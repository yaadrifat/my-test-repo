<%@ taglib prefix="s"  uri="/struts-tags"%>
<script>

$j(document).ready(function(){
	$j("#acknowledgeOrdersViewparent").attr('style','display:none');
	$j("#acknowledgediv").attr('style','display:none');
	$j("#colNames").attr("disabled","true");
	$j("#filterData").attr("disabled","true");
	$j("#filterDataDiv").hide();
});

function getSelectedColData(colName){
	if(colName!=null || colName!=""){
		$j("#filterData").removeAttr('disabled');
		$j("#filterDataDiv").show();	
		refreshDiv('getDataforSelectedCol?colName='+colName,'filterDataDiv','acknowledgeOrdersForm');
	}	
}
function constructTable(){
	 $j('#searchResults').dataTable();
}
function showOrders(){
	var var_orderType=$j("#acknowledgeOrdersForm input[type='radio']:checked").val();
	var var_filterData=$j("#filterData").val();
	var var_filterCol=$j("#colNames").val();
	var var_pkOrderType=$j("#pkOrderType").val();
	$j("#colNames").removeAttr('disabled');
	$j("#acknowledgeOrdersViewparent").attr('style','display:block');
	$j("#acknowledgediv").attr('style','display:block');
	refreshDiv('getResultsForCancelFilter?orderType='+var_orderType+'&filterData='+var_filterData+'&filterCol='+var_filterCol+'&pkOrderType='+var_pkOrderType,'acknowledgeOrdersViewparent','acknowledgeOrdersForm');
}

function getOrderDetails(orderId,cbuId,orderType,orderStatus,reqDate,lstUpdDate){
	$j("#orderId").val(orderId);
	$j("#cbuId").val(cbuId);
	$j("#orderType").val(orderType);
	$j("#orderStatus").val(orderStatus);
	$j("#reqDate").val(reqDate);
	$j("#lstUpdDate").val(lstUpdDate);
	document.forms["acknowledgeOrdersForm"].submit();
	//window.location="getOrderDetails?orderId="+orderId+"&cbuId="+cbuId+"&";
}

function getCheck(val1,val2,This){

if(This.checked){
		var tempnul;
		var checkval=$j("#twoids").val();
		//alert("First....."+checkval);
		if(checkval=="" || checkval== null){
			va1="0";
			va2="0";
			tempnul=va1+","+va2;
			}
		if(tempnul!=null){
		var newtwoId = tempnul+ ","+ val1 + "," + val2 +",";
		$j("#twoids").val(newtwoId);
		}
		else if(checkval=="0,0"){
		var newtwoId = checkval+ ","+ val1 + "," + val2 +",";
		$j("#twoids").val(newtwoId);
		}
		else{
			var newtwoId = checkval+ val1 + "," + val2+",";
			$j("#twoids").val(newtwoId);
		}
		$j("#twoids").val(newtwoId);
		//alert("Inside if....."+newtwoId);
	}
	else{
		 var checkIfRated = $j("#twoids").val();
		 //alert("Inside else....."+checkIfRated);
			if(checkIfRated.indexOf(val1) && checkIfRated.indexOf(val2)){
				//alert("B4 : "+checkIfRated);
				//checkIfRated.replace("0,0,","")
				checkIfRated = checkIfRated.replace(","+val1+","+val2+"","");
				$j("#twoids").val(checkIfRated);
			}
		//alert("Inside else....."+checkIfRated);
		}
}

function checkSelectedUser(){

	var userList = document.getElementsByName('checkbox');
	//alert(userList);
	var count=0;
	for(var i=0;i<userList.length;i++){
		if(userList[i].checked){
			count++;
			}
		}
		if(count>0){
			return true;
		}else{
			return false;
		}
}
function acknowledgeselection(){
	    <%System.out.print("inside function");%>
		var isUserSelect = checkSelectedUser(); 
		if(isUserSelect){
		var logId = $j('#twoids').val();
		logId = logId.replace("0,0,","");
		var loginIds = logId.split(",");
		showModal('Update Acknowledgement','updateAcknowledgement?acknow=false&cbulocalIds='+loginIds,'200','350');
		}
		else{
			document.getElementById("label1").innerHTML="<s:text name="garuda.cancelorders.pf.checkValue"/>";
		}
		//alert("Inside else....."+loginIds);
		var reset="";
		$j("#twoids").val(reset);
		showOrders();
		return false;
		
}

</script>
<div class="col_100 maincontainer"><div class="col_100">
<div class="column">
<form id="acknowledgeOrdersForm" method="post" action="getOrderDetails">
<s:hidden name="pkOrderType"></s:hidden>
<s:hidden name="orderId"></s:hidden>
<s:hidden name="cbuId"></s:hidden>
<s:hidden name="orderType"></s:hidden>
<s:hidden name="orderStatus"></s:hidden>
<s:hidden name="reqDate"></s:hidden>
<s:hidden name="lstUpdDate" ></s:hidden>
<s:hidden name="pkids" id="pkids" />
<s:hidden name="cbulocalIds" id="cbulocalIds" />
<s:hidden name="twoids" id="twoids" />
	<table cellpadding="0" cellspacing="0" border="0" class="col_100">
		<tr>
			<td>
				<s:div class="portlet" id="acknowledgeOrdersSearchparent">
					<div id="cdrsearch" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ><div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
					<span class="ui-icon ui-icon-minusthick"></span><!--<span class="ui-icon ui-icon-close"></span>--><s:text name="garuda.cancelorders.pf.cancelOrderSearch"/></div>
							<table cellpadding="0" cellspacing="0" border="0" class="col_100" width="100%">
								<tr>
									<td width="35%">
										<s:radio name="orderType" list="orderTypeList" listKey="pkCodeId" listValue="description" onchange="showOrders();"/>
									</td>
									<td width="15%" align="right">
										<s:text name="garuda.cbu.quickNote.filter"/> :
									</td>
									<td width="25%">
										<s:select name="colNames" id="colNames"	list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@COL_NAMES]"
											listKey="subType" listValue="description" headerKey=""
											headerValue="Select" onchange="getSelectedColData(this.value);"/>
									</td>
									<td width="25%">
										<div id="filterDataDiv">
										<s:if test="colValuesList!=null && colValuesList.size()>0">
											<s:select list="colValuesList" headerKey="" headerValue="Select" listKey="subType" listValue="description" name="filterData" id="filterData" onchange="showOrders();"/>

										</s:if>
										<s:else>
											<s:select list="#{'':'Select'}" name="filterData"></s:select>
										</s:else>
										</div>
									</td>
								</tr>
							</table>
					</div>
				</s:div>
			</td>
		</tr>
		<tr>
		</tr>
		<tr>
			<td colspan="3">
			<s:div id="acknowledgediv">
			<table><tr><td width="100%">
			<input type="button" onclick="acknowledgeselection();" value="<s:text name="garuda.cancelorders.pf.acknowledgementButton"/></input>"/><label id="label1" style="color:#FF0000;" ></label>
			</td></tr></table>	
			</s:div>
			</td>
		</tr>
		<tr>
			<td>
			<div id="resultdiv">
				<s:div class="portlet" id="acknowledgeOrdersViewparent">
					<div id="openOrderView" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
							<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
							<span class="ui-icon ui-icon-minusthick"></span><!--<span class="ui-icon ui-icon-close"></span>--><s:text name="garuda.cancelorders.pf.searchResults"/></div>
							<div class="portlet-content">
							<div id="searchTble">
							<table border="0" align="left" cellpadding="0" cellspacing="0"
								class="displaycdr" id="searchResults">
								<thead>
									<tr>
										<th><s:text name="garuda.cbuentry.label.localcbuid"/></th>
										<th><s:text name="garuda.pendingorderpage.label.ordertype"/></th>
										<th><s:text name="garuda.cancelorders.pf.orderStatus"/></th>
										<th><s:text name="garuda.cancelorders.pf.acknowledge "/></th>
										<th><s:text name="garuda.cancelorders.label.requestedDate"/></th>
										<th><s:text name="garuda.currentReqProgress.label.assignedto"/></th>
										<th><s:text name="garuda.cancelorders.label.task"/></th>
										<th><s:text name="garuda.cancelorders.label.lastUpdated"/></th>
									</tr>
								</thead>
								<tbody>
								<s:if test="searchDataList!=null && searchDataList.size()>0">
								<s:iterator value="searchDataList" var="rowVal">
									<tr>
										<td><s:property value="%{#rowVal[1]}"/></td>
										<td><a href="#" onclick="getOrderDetails('<s:property value="%{#rowVal[0]}"/>','<s:property value="%{#rowVal[1]}"/>','<s:property value="%{#rowVal[2]}"/>','<s:property value="%{#rowVal[4]}"/>','<s:property value="%{#rowVal[3]}"/>','<s:property value="%{#rowVal[9]}"/>')"><s:property value="%{#rowVal[2]}"/></a></td>
										<td><s:property value="%{#rowVal[4]}"/></td>
										<td><s:checkbox name="checkbox" id="checkbox" onclick="getCheck('%{#rowVal[0]}','%{#rowVal[1]}',this)" /></td>
										<td><s:property value="%{#rowVal[3]}"/></td>
										<td><s:property value="%{#rowVal[6]}"/></td>
										<td><s:property value="%{#rowVal[7]}"/></td>
										<td><s:property value="%{#rowVal[9]}"/></td>
									</tr>
								</s:iterator>
								</s:if>
								<s:else>
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
								</s:else>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="4"></td>
									</tr>
								</tfoot>
							</table>
						</div>
						</div>
							
					</div>
				</s:div>
				</div>
			</td>
		</tr>
	</table>
</form>
</div>
</div></div>