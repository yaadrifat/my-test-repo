<%@taglib prefix="s" uri="/struts-tags"%>
<script>
function displayAppMsg(id){
	   var vicon='icon_div';
	  // $j("#"+vicon).find(".ui-icon").each(function(){
	       /*if($j("#"+vicon).is(".ui-icon-minusthick") || $j("#"+vicon).is(".ui-icon-plusthick"))
	       {
	           if($j("#"+vicon).is(".ui-icon-minusthick")){
	               $j('#'+id).toggle("slow");
	               $j("#"+vicon).removeClass("ui-icon-minusthick").addClass("ui-icon-plusthick");
	           }
	           else if($j("#"+vicon).is(".ui-icon-plusthick")){
	               $j('#'+id).toggle("slow");
	               $j("#"+vicon).removeClass("ui-icon-plusthick").addClass("ui-icon-minusthick");
	           }
	       }*/
	  // });   
		  $j("#"+vicon).toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick");
		  $j("#"+id).toggle();
	   }

$j(function(){
	var innerHidden = document.getElementById('innerHidden');
	//var innerHidden_Val = null;
	if(innerHidden!=null){
		$j('#appMagsDiv').show();
	}else{
		$j('#appMagsDiv').hide();
	}
});
</script>
<style>
.app-msg ul {
    list-style: none outside none;
    margin: 0;
    padding: 0 0 0px 35px;
}
.app-msg ul li {
    background: url("./images/nmdpbullets.jpg") no-repeat scroll left 8px transparent;
    color: #000000;
    line-height: 24px;
    margin: 0;
    padding: 0 0 0 15px;
    text-align: left;
}
.btnAlign{
margin: 5px 0 5px 30px;
}
</style>
<s:if test="#application.appMsgsList.size()>0 && #request.pageId == 29">
	<div id="appMagsDiv">
	<span onclick="displayAppMsg('appMsgs')"><span class="ui-icon ui-icon-plusthick" id="icon_div" ></span><b><s:text name="garuda.applMessge.label.applicationMsg"></s:text></b></span>
	<div class="app-msg" id="appMsgs" style="display: none;">
	<s:iterator value="#application.appMsgsList" var="appMessages" status="row">
	<s:if test="%{(msgStartDt==null && msgEndDt==null) || ((#application.pkInActiveMsg!=msgStatus) && (msgStartDt.before(#application.currentDate) || msgStartDt.equals(#application.currentDate)))}">
	<s:hidden name="innerHidden" id="innerHidden" ></s:hidden>
	<ul>
	<li>
	<s:property	value="msgDesc" />
	</li>
	</ul>
	</s:if>
	</s:iterator>
	</div>
	</div>
</s:if>