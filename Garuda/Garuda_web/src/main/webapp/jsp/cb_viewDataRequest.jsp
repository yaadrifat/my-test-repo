<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="paginateSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<%
HttpSession tSession = request.getSession(true);
String accId = null;
String logUsr = null;
if (sessionmaint.isValidSession(tSession))
{
    accId = (String) tSession.getValue("accountId");
    logUsr = (String) tSession.getValue("userId");
    request.setAttribute("username",logUsr);
}
%>
<script type="text/javascript">
$j(document).ready(function(){
	dataTblOnLoad();
});
function dataTblOnLoad(){
	

	var viewRequestTbl = $j('#searchTbl').dataTable({"iDisplayLength":1000,"bRetrieve":true,"bDestroy":true});
		/* Add a select menu for each TH element in the table head */
		$j("#searchTbl thead tr").each( function ( i ) {
		if(i!=0){
			$j(this).find('th').each ( function(j) {
				
						this.innerHTML = fnCreateSelect( viewRequestTbl.fnGetColumnData(j),j);
					 	$j('select', this).change( function () {
					 		viewRequestTbl.fnFilter( $j(this).val(), j );
						} );
			});

		}
		});

	$j('#searchTbl_info').hide();
	$j('#searchTbl_paginate').hide();
	$j('#searchTbl_length').empty();
	$j('#searchTbl_length').replaceWith('Show <select name="showsRow" id="showsRowsearchTbl" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="<s:property value="paginateSearch.getiTotalRows()"/>">All</option></select> Entries');
	if($j('#entries').val()!=null || $j('#entries').val()!=""){
		$j('#showsRowsearchTbl').val($j('#entries').val());
		}
	$j('#showsRowsearchTbl').change(function(){
		paginationFooter(0,"viewDataReq,showsRowsearchTbl,temp,viewDataRequestForm,viewDataRequestTbl,dataTblOnLoad");
	});
	var temp=150;
	$j('#searchTbl').tableScroll({"height":temp});
}

function fnCreateSelect( aData,j)
{
		var r='<select id="HeaderSelectOrd'+j+'"><option value="">-Select-</option>', i, iLen=aData.length;
		for ( i=0 ; i<iLen ; i++ )
		{
			r += '<option value="'+aData[i]+'">'+aData[i]+'</option>';
		}
	return r+'</select>';
}


(function($j) {
	$j.fn.dataTableExt.oApi.fnGetColumnData = function ( oSettings, iColumn, bUnique, bFiltered, bIgnoreEmpty ) {
	
	 if ( typeof iColumn == "undefined" ) return new Array();
	 if ( typeof bUnique == "undefined" ) bUnique = true;
	 if ( typeof bFiltered == "undefined" ) bFiltered = true;
     if ( typeof bIgnoreEmpty == "undefined" ) bIgnoreEmpty = true;
     var aiRows;
	
	if (bFiltered == true) aiRows = oSettings.aiDisplay; 
	else aiRows = oSettings.aiDisplayMaster; // all row numbers
	var asResultData = new Array();
	var sValue;
	for (var i=0,c=aiRows.length; i<c; i++) {
		iRow = aiRows[i];
		var aData = this.fnGetData(iRow);
		sValue = aData[iColumn];
		if (bIgnoreEmpty == true && sValue.length == 0) continue;
		else if (bUnique == true && jQuery.inArray(sValue, asResultData) > -1) continue;
		else asResultData.push(sValue);
	}
	return asResultData;
}}($j));


function getDetails(pkval,status){

	if(status=='Approved' || status=='Pending'){
		    refreshDiv('getRequestDetails?datarequestpojo.pkdatarequest='+pkval,'viewDataRequestTblDiv','viewDataRequestForm');
			$j('#tablediv').css('display','none');
			$j('#viewDataRequestTblDiv').css('display','block');
			//$j('#RequestAssign').css('padding-left','25%');
			$j('#requestDetail').width('50%');
			$j('#viewEsign').width('65%');
			$j('#appDropdownTbl').width('40%');
	}
	
}
function updateReqStatus(){
	var pkval=$j('#pkvalue').val();
	var status=$j('#reqApprovalDd1').val();
	if(status==null || status==""){
		showError();
	}
	else{
		//window.location="updateDMRequestStatus?datarequestpojo.pkdatarequest="+pkval+"&statusval="+status;
		submitpost('updateDMRequestStatus',{'datarequestpojo.pkdatarequest':pkval,'statusval':status});
	}
	
}
function showError(){
	var status=$j('#reqApprovalDd1').val();
	if(status==null || status==""){
		$j('#showerror').css('display','block');
	}
	else{
		$j('#showerror').css('display','none');
	}
}
function fnCancelUpdate(){
	window.location="viewDataReq";
}
function constructTable(){
	dataTblOnLoad();
}
function fnResetFilter(){
    var i=1;
   	var OrdTabCols = $j("#searchTbl").find('tr')[0].cells.length;
	while(i<OrdTabCols){
		var temp='#HeaderSelectOrd'+i;
		$j(temp).val('');
		var activeOrdTable = $j('#searchTbl').dataTable();
		activeOrdTable.fnFilter('', i );
		i++;
	}
	
} 
</script>
<div class="maincontainer">




<s:form id="viewDataRequestForm">

<div class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" id="historydiv" style="height: 100%;">
						<div style="text-align: center;" class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
								<span onclick="clickheretoprint()" class="ui-icon ui-icon-print"></span>
								<span class="ui-icon ui-icon-newwin"></span>
								<span class="ui-icon ui-icon-minusthick"></span>
								<s:text name="garuda.viewData.label.viewDataModification" />
						</div>
		<div class="portlet-content">
		<div id="tablediv">
		<div style="float: right;"><button id="resetFilter" name="resetFilter" onclick="fnResetFilter()" type="button"><s:text name="garuda.viewData.label.resetFilterButton"/></button></div>
		<div id="viewDataRequestTbl" style="width: 100%;">
						<input type="hidden" name="entries" id="entries" value=<%=paginateSearch.getiShowRows() %> />
						<table class="displaycdr" id="searchTbl">
												<thead>
													<tr>
														<th><s:text name="garuda.dmrequest.label.requestno"/></th>
														<th><s:text name="garuda.dmrequest.label.requesttype"/></th>
														<th><s:text name="garuda.dmrequest.label.requestdate"/></th>
														<th><s:text name="garuda.dmrequest.label.cbbcode"/></th>
														<th><s:text name="garuda.dmrequest.label.username"/></th>
														<th><s:text name="garuda.dmrequest.label.requestdue"/></th>
														<th><s:text name="garuda.dmrequest.label.requestttile"/></th>
														<th><s:text name="garuda.dmrequest.label.requeststatus"/></th>
													</tr>
													<tr>
																	<th></th>
																	<th></th>
																	<th></th>
																	<th></th>
																	<th></th>
																	<th></th>
																	<th></th>
																	<th></th>
													</tr>
												</thead>
												<tbody>
													<s:if test="dataModificationLst!=null && dataModificationLst.size()>0">
													<s:iterator value="dataModificationLst" var="rowVal" status="row">
													<tr 
													onclick="getDetails('<s:property value="%{#rowVal[0]}"/>','<s:property value="%{#rowVal[7]}"/>');"
													id="<s:property	value="%{#row.index}"/>" onMouseOver="highlight('<s:property	value="%{#row.index}"/>')" onmouseout="removeHighlight('<s:property	value="%{#row.index}"/>')">
														<td><s:property value="%{#rowVal[0]}"/></td>
														<td><s:property value="%{#rowVal[1]}"/></td>
														<td><s:property value="%{#rowVal[2]}"/></td>
														<td><s:property value="%{#rowVal[3]}"/></td>
														<td>
														<s:if test="%{#rowVal[4]!=null}">
														<s:set name="username" value="%{#rowVal[4]}" scope="request"/>
																		<%
																			String cellValue1 = request.getAttribute("username").toString();
																			UserJB userB = new UserJB();
																			userB.setUserId(EJBUtil.stringToNum(cellValue1));
																			userB.getUserDetails();													
																		%>		
																		<%=userB.getUserLastName()+" "+userB.getUserFirstName()%>
																		
														</s:if>
														</td>
														<td><s:property value="%{#rowVal[5]}"/></td>
														<td><s:property value="%{#rowVal[6]}"/></td>
														<td><s:property value="%{#rowVal[7]}"/></td>
													</tr>
													</s:iterator>
													</s:if>
													<s:else><tr><td colspan="8"><s:text name="garuda.viewData.label.noRecordFound"/></td></tr></s:else>
													
												</tbody>
												<tfoot>
													<tr>
														<td colspan="8">
															<jsp:include page="paginationFooter.jsp">
																		  	<jsp:param value="viewDataRequestTbl" name="divName"/>
																		  	<jsp:param value="viewDataRequestForm" name="formName"/>
																		  	<jsp:param value="showsRowsearchTbl" name="showsRecordId"/>
																		  	<jsp:param value="viewDataReq" name="url"/>
																		  	<jsp:param value="dataTblOnLoad" name="bodyOnloadFn"/>
															 </jsp:include>
														</td>
													</tr>
												</tfoot>
						</table>
				</div>
		</div>
		<div id="viewDataRequestTblDiv" style="width: 100%; display: none;">
						<br/><br/><br/>
						<table id="requestDetail" class="displaycdr" align="center" >
								<tbody>
									<tr>
										<td><s:text name="garuda.dmrequest.label.requestttile"/></td>
										<td>
											<s:property value="datarequestpojo.title"/>
											<input type="hidden" value=<s:property value="datarequestpojo.pkdatarequest"/> id="pkvalue"/>
										</td>
									</tr>
									<tr>
										<td colspan="2"><br/><br/></td>
									</tr>
									<tr>
										<td><s:text name="garuda.dmrequest.label.requestdue"/></td>
										<td>
										<s:if test="%{datarequestpojo.requestDuedate!=null}">
											<s:text name="collection.date">
												<s:param name="value" value="datarequestpojo.requestDuedate" />
											</s:text>
										</s:if>
										</td>
									</tr>
									<tr>
										<td colspan="2"><br/><br/></td>
									</tr>
									<tr>
										<td><s:text name="garuda.dmrequest.label.datamodificationdescri"/></td>
										<td><s:property value="datarequestpojo.dataModiDescription"/></td>
									</tr>
									<tr>
										<td colspan="2"><br/><br/></td>
									</tr>
									<tr>
										<td><s:text name="garuda.dmrequest.label.requestreason"/></td>
										<td><s:property value="datarequestpojo.requestReason"/></td>
									</tr>
									<tr>
										<td colspan="2"><br/><br/></td>
									</tr>
									<tr>
										<td><s:text name="garuda.dmrequest.label.affectedfields"/></td>
										<td><s:property value="datarequestpojo.affectedFields"/></td>
									</tr>
									<tr>
										<td colspan="2"><br/><br/></td>
									</tr>
									<tr>
										<td><s:text name="garuda.dmrequest.label.cbbprocesschange"/></td>
										<td><s:property value="datarequestpojo.cbbProcessChange"/></td>
									</tr>
								</tbody>
						</table>
						<br/><br/>
						<table id="RequestAssign">
									<tr>
										<td>
											<table id="appDropdownTbl" align="center"><tr>
											<td width="50%"><s:text name="garuda.viewData.label.requestApproval"/><span class="error">*</span></td>
											<td  width="50%">
											<s:select name="reqApprovalDd" list="{'Approved','Completed','Rejected'}" id="reqApprovalDd1" headerValue="Select" headerKey="" onchange="showError()"/>
											<span class="error" id="showerror" style="display:none"><s:text name="garuda.common.value.selectValue"/></span> 
											</td>
											</tr></table>
										</td>
									</tr>
									<!--
									<tr>
										<td><s:text name="Request Approval" id="ReqApproval"/></td>
										<td><s:select name="reqApprovalDd" list="{'Completed','Rejected'}" id="reqApprovalDd2" headerValue="Select" headerKey=""/></td>
									</tr>
									  -->
									<tr>
										<td>
										<br/><br/>
										<table bgcolor="#cccccc" style="padding-top: 5px;" id="viewEsign" align="center">
											<tr bgcolor="#cccccc" valign="baseline">
											<td width="70%"><jsp:include page="cb_esignature.jsp" flush="true"></jsp:include></td>
										    <td width="15%" align="right">
										    <input type="button" id="submit" disabled= "disabled" onclick="updateReqStatus()" value="<s:text name="garuda.unitreport.label.button.submit"/>" />
										    </td>
										    <td width="15%"><input type="button"  onclick="fnCancelUpdate()" value="<s:text name="garuda.unitreport.label.button.cancel"/>" /></td>
										    </tr>	
										</table>
										</td>
									</tr>
						</table>
						<br/><br/><br/>
		</div>	
		</div>			
</div>
</s:form>




</div>