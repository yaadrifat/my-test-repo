
<%@page import="com.velos.ordercomponent.business.util.VelosMidConstants"%>
<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="paginateSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginationSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginationSearch1" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>

<% String contextpath=request.getContextPath();%>
<%
HttpSession tSession = request.getSession(true);
GrpRightsJB grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
String accId = null;
String logUsr = null;
if (sessionmaint.isValidSession(tSession))
{
    accId = (String) tSession.getValue("accountId");
    logUsr = (String) tSession.getValue("userId");
    request.setAttribute("username",logUsr);
}

String cellValue1="";
if(request.getAttribute("username")!=null && !request.getAttribute("username").equals("")){
	cellValue1 = request.getAttribute("username").toString();
}
UserJB userB = new UserJB();
userB.setUserId(EJBUtil.stringToNum(cellValue1));
userB.getUserDetails();			
String lname=userB.getUserDetails().getUserLastName();
String fname=userB.getUserDetails().getUserFirstName();
String logname=userB.getUserDetails().getUserLoginName();

int assignOrders = 0;
int viewFunding = 0;
int actReqst = 0;
int othrReqst = 0;
int notAvailCbu = 0;
int svdinPrgrss = 0;
int viewCTResolution = 0;
int viewORResolution = 0;
int viewHEResol= 0;

if(grpRights.getFtrRightsByValue("CB_PFPENDINGW")!=null && !grpRights.getFtrRightsByValue("CB_PFPENDINGW").equals(""))
{	assignOrders = Integer.parseInt(grpRights.getFtrRightsByValue("CB_PFPENDINGW"));}
else
	assignOrders = 4;

if(grpRights.getFtrRightsByValue("CB_FUNDING")!=null && !grpRights.getFtrRightsByValue("CB_FUNDING").equals(""))
{	viewFunding = Integer.parseInt(grpRights.getFtrRightsByValue("CB_FUNDING"));}
else
	viewFunding = 4;

if(grpRights.getFtrRightsByValue("CB_ACTIVEREQ")!=null && !grpRights.getFtrRightsByValue("CB_ACTIVEREQ").equals(""))
{	actReqst = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ACTIVEREQ"));}
else
	actReqst = 4;

if(grpRights.getFtrRightsByValue("CB_OTHREQ")!=null && !grpRights.getFtrRightsByValue("CB_OTHREQ").equals(""))
{	othrReqst = Integer.parseInt(grpRights.getFtrRightsByValue("CB_OTHREQ"));}
else
	othrReqst = 4;
if(grpRights.getFtrRightsByValue("CB_NOTAVAIL")!=null && !grpRights.getFtrRightsByValue("CB_NOTAVAIL").equals(""))
{	notAvailCbu = Integer.parseInt(grpRights.getFtrRightsByValue("CB_NOTAVAIL"));}
else
	notAvailCbu = 4;
if(grpRights.getFtrRightsByValue("CB_INPROGCBU")!=null && !grpRights.getFtrRightsByValue("CB_INPROGCBU").equals(""))
{	svdinPrgrss = Integer.parseInt(grpRights.getFtrRightsByValue("CB_INPROGCBU"));}
else
	svdinPrgrss = 4;

if(grpRights.getFtrRightsByValue("CB_ORRESOLW")!=null && !grpRights.getFtrRightsByValue("CB_ORRESOLW").equals(""))
{        viewORResolution = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ORRESOLW"));}
else
        viewORResolution = 4;
if(grpRights.getFtrRightsByValue("CB_CTRESOLTNW")!=null && !grpRights.getFtrRightsByValue("CB_CTRESOLTNW").equals(""))
{        viewCTResolution = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CTRESOLTNW"));}
else
        viewCTResolution = 4;
if(grpRights.getFtrRightsByValue("CB_HERESOLW")!=null && !grpRights.getFtrRightsByValue("CB_HERESOLW").equals(""))
{        viewHEResol= Integer.parseInt(grpRights.getFtrRightsByValue("CB_HERESOLW"));}
else
        viewHEResol = 4;

request.setAttribute("assignOrders",assignOrders);
request.setAttribute("viewFunding",viewFunding);
request.setAttribute("actReqst",actReqst);
request.setAttribute("othrReqst",othrReqst);
request.setAttribute("notAvailCbu",notAvailCbu);
request.setAttribute("svdinPrgrss",svdinPrgrss);
request.setAttribute("ctResolRht",viewCTResolution);
request.setAttribute("heResolRht",viewHEResol);
request.setAttribute("orResolRht",viewORResolution);
%>

<script type="text/javascript">

$j(window).unload(function(){
	  var actTabId=$j(".highlight-tab").find("a").attr("id");
	  $j.cookie('activeTab', actTabId);
	  var activeShowEntries=$j("#showsRowtablelookup").val();
	  var naCBUShowEntries=$j("#showsRowNA-Cbu-tbl").val();
	  var cordEntryShowEntries=$j("#showsRowcordentrytbl").val();
	  var activeShowEntriestxt=$j("#showsRowtablelookup :selected").html();
	  var naCBUShowEntriestxt=$j("#showsRowNA-Cbu-tbl :selected").html();
	  var cordEntryShowEntriestxt=$j("#showsRowcordentrytbl :selected").html();
	 
	  var tbl1_site_filter=$j('#allparams').val();
	  var tbl2_site_filter=$j('#allparamsNaCBU').val();
	  var tbl3_site_filter=$j('#allparamSIP').val();
	  var param3=tbl3_site_filter.replace(/&/gi,'<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@REPLACE_STR"/>');
	  var str=tbl1_site_filter.indexOf("&reqType1");
	  var param1="";
	  //alert("fil1:"+tbl1_site_filter+"fil2:::::"+tbl2_site_filter+"fil3:::"+tbl3_site_filter);
	  if(str!=-1){
		  var len=tbl1_site_filter.length;
		  var removedStr1=tbl1_site_filter.substring(str,len);
		  param1=removedStr1.replace(/&/gi,'<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@REPLACE_STR"/>');
		  if(param1==""){
			  param1=tbl1_site_filter.replace(/&/gi,'<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@REPLACE_STR"/>');
		  }
	  }
	  
	  var str2=tbl2_site_filter.indexOf("&nastatus");
	  var param2="";
	  if(str2!=-1){
		  var len2=tbl2_site_filter.length;
		  var removedStr2=tbl2_site_filter.substring(str2,len2);
		  param2=removedStr2.replace(/&/gi,'<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@REPLACE_STR"/>');
		  if(param2==""){
			  param2=tbl2_site_filter.replace(/&/gi,'<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@REPLACE_STR"/>');
		  }
		  //alert("param2 in unload"+param2);
	  }
	  
	  var lastSelectCBB=$j('#cbbdrpdwn').val();
	  //alert("For orders TBL::::::::CBB:"+lastSelectCBB+"activeShowEntries:::::"+activeShowEntries+"activeShowEntriestxt:::"+activeShowEntriestxt);
	  //alert("For NACBU TBL::::::::CBB:"+lastSelectCBB+"naShowEntries:::::"+naCBUShowEntries+"activeShowEntriestxt:::"+naCBUShowEntriestxt);
	  //alert("For SIP TBL::::::::CBB:"+lastSelectCBB+"sipShowEntries:::::"+cordEntryShowEntries+"activeShowEntriestxt:::"+cordEntryShowEntriestxt);
	  var ord_sort_col=$j('#ord_sortParam').val();
	  var ord_sort_type=$j('#ord_sort_type').val();
	  var nacbu_sort_col=$j('#naCBU_sortParam').val();
	  var nacbu_sort_type=$j('#naCBU_sort_type').val();
	  var sip_sort_col=$j('#sip_sortParam').val();
	  var sip_sort_type=$j('#sip_sort_type').val();
	  
	  var urltemp='setSessionDataPS?actOrd_SE='+activeShowEntries+'&naCBU_SE='+naCBUShowEntries+'&SIP_SE='+cordEntryShowEntries+'&actOrd_fval='+param1+'&naCBU_fval='+param2+'&SIP_fval='+param3+'&last_cbb='+lastSelectCBB+'&actOrd_SE_txt='+activeShowEntriestxt+'&naCBU_SE_txt='+naCBUShowEntriestxt+'&SIP_SE_txt='+cordEntryShowEntriestxt+'&Ord_Sort_Col='+ord_sort_col+'&Ord_Sort_Typ='+ord_sort_type+'&NACBU_Sort_Col='+nacbu_sort_col+'&NACBU_Sort_Typ='+nacbu_sort_type+'&SIP_Sort_Col='+sip_sort_col+'&SIP_Sort_Typ='+sip_sort_type;
	  setValueInSession(urltemp);
}); 

$j(document).ready(function() {
	
	maximizePage();
	
	ShowEntries_Sort_Data();
	var sv_last_cbb='<%=session.getAttribute("lastCBB")%>';
	$j('.showHide').live('click', function(){
		var cbb=$j('#cbbdrpdwn').val();
		if(cbb==null || cbb==''){ // || !$j(this).hasClass('otherRequestWid')
			$j(this).triggerHandler('click');
			alert("Please select any CBB");
		}
	});
	
	
	$j('.firstLoad').live('click', function(){
		removingCSS();
		if($j('#not-available-cbu').is(":visible")){
			
			var ord=$j('#NA-Cbu-tbl').dataTable();
			if ( ord.length > 0 ) {
				//alert("inside calling alignment notavail")
				ord.fnAdjustColumnSizing(false);
			}	
		}
		if($j('#cord-entry').is(":visible")){
				//alert("inside calling alignment sip live");
				var ord=$j('#cordentrytbl').dataTable();
				if ( ord.length > 0 ) {
					ord.fnAdjustColumnSizing(false);
				}
		}		
			
	});
		
	$j("#tabs").tabs({
		"show": function(event, ui) {
			var oTable = $j('div.dataTables_scrollBody> #cordentrytbl', ui.panel).dataTable();
			if ( oTable.length > 0 ) {
				oTable.fnAdjustColumnSizing(false);
			}
		}
	});
	
	var fromLanding=$j('#pendingFilter').val();
	var primaryorg=$j('#primaryOrg').val();

	if(fromLanding!=null && fromLanding!='' && fromLanding=='1'){
			siteIdval=$j('#pfSiteId').val();
			$j('#cbbdrpdwn').val(siteIdval);
			$j('.activeOrder').triggerHandler('click');
	}
	else if((fromLanding==null || fromLanding=='') && sv_last_cbb!=null && sv_last_cbb!='' && sv_last_cbb!='null'){
		
		var j=0;
		if(sv_last_cbb.indexOf(",")!=-1){
			splitedStr=sv_last_cbb.split(",");
			$j('#cbbdrpdwn').find('option').each(function(i){
				if($j(this).val()==splitedStr[j]){
					$j(this).attr('selected','selected');
					j=j+1;
				}
			});
			
		}else{
			$j('#cbbdrpdwn').val(sv_last_cbb);	
		}
		getCBBdetails(sv_last_cbb);
		
	}
	else if(primaryorg!='' && (fromLanding==null || fromLanding=='') && (sv_last_cbb==null || sv_last_cbb=='' || sv_last_cbb=='null')){
		
		$j('#cbbdrpdwn').val(primaryorg);
		getCBBdetails(primaryorg);
	}
	
	getFilterDataForOtherReq();
	
	hideDataDiv();
	$j('#cbbdrpdwn').filterByText($j('#Searchbox'), true , "cbbdrpdwn");
});
</script>
<style type="text/css">
.assignsel,.ackdyn{
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid #DBDAD9;
    height: 20px !important;
    margin-bottom: 0 !important;
    padding: 0 !important;
}
</style>
<div id="noNameDiv" style="display: none;"></div>
<input type="hidden" id="printingdiv" value="NA-Cbu-tbl_wrapper" >
<input type="hidden" id="printingdivTitle" value="Not Available CBUs">
<s:hidden name="new_Ord_filterFlag" id="new_Ord_filterFlag"/>
<s:hidden name="new_Other_filterFlag" id="new_Other_filterFlag"/>
<s:hidden name="ord_sortParam" id="ord_sortParam"/>
<s:hidden name="ord_sort_type" id="ord_sort_type"/>
<s:hidden name="ord_header" id="ord_header"/>
<s:hidden name="naCBU_sortParam" id="naCBU_sortParam"/>
<s:hidden name="naCBU_sort_type" id="naCBU_sort_type"/>
<s:hidden name="naCBU_header" id="naCBU_header"/>
<s:hidden name="sip_sortParam" id="sip_sortParam"/>
<s:hidden name="sip_sort_type" id="sip_sort_type"/>
<s:hidden name="sip_header" id="sip_header"/>
<s:hidden name="SE_Pending" id="SE_Pending"></s:hidden>
<s:hidden name="SE_SIP" id="SE_SIP"></s:hidden>
<s:hidden name="SE_NACBU" id="SE_NACBU"></s:hidden>
<s:hidden name="SE_Pending_txt" id="SE_Pending_txt"></s:hidden>
<s:hidden name="SE_SIP_txt" id="SE_SIP_txt"></s:hidden>
<s:hidden name="SE_NACBU_txt" id="SE_NACBU_txt"></s:hidden>
<s:hidden name="ord_allentries_flag" id="ord_allentries_flag"></s:hidden>
<s:hidden name="nacbu_allentries_flag" id="nacbu_allentries_flag"></s:hidden>
<s:hidden name="sip_allentries_flag" id="sip_allentries_flag"></s:hidden>
<s:hidden name="is_cbb_selected" id="is_cbb_selected"></s:hidden>
<s:hidden name="ord_filter_value" id="ord_filter_value"></s:hidden>
<s:hidden name="nacbu_filter_value" id="nacbu_filter_value"></s:hidden>
<s:hidden name="sip_filter_value" id="sip_filter_value"></s:hidden>
<s:form id="pendingOrderForm">
<s:hidden name="pkOrderType"></s:hidden>
<s:hidden name="orderId" id="orderId"></s:hidden>
<s:hidden name="pkcordId" id="pkcordId"></s:hidden>
<s:hidden name="allparams" id="allparams"></s:hidden>
<s:hidden name="allparamsNaCBU" id="allparamsNaCBU"></s:hidden>
<s:hidden name="allparamSIP" id="allparamSIP"></s:hidden>
<s:hidden name="activeFilterFlag" id="activeFilterFlag"/>
<s:hidden name="cordFilterFlag" id="cordFilterFlag"/>
<s:hidden name="naCBUFilterFlag" id="naCBUFilterFlag"/>
<s:hidden name="activeStatFilterFlag" id="activeStatFilterFlag"/>
<s:hidden name="cordStatFilterFlag" id="cordStatFilterFlag"/>
<s:hidden name="naCBUStatFilterFlag" id="naCBUStatFilterFlag"/>
<s:hidden name="fcol" id="fcol"></s:hidden>
<s:hidden name="fval" id="fval"></s:hidden>
<s:hidden name="pfOrdertype" id="pfOrdertype"></s:hidden>
<s:hidden name="pendingFilter" id="pendingFilter"></s:hidden>
<s:hidden name="pfSiteId" id="pfSiteId"></s:hidden>
<s:hidden name="pfOrdertype" id="pfOrdertype"></s:hidden>
<s:hidden name="pfSampleAtlab" id="pfSampleAtlab"></s:hidden>
<s:hidden name="userPrimaryOrg" id="primaryOrg"></s:hidden>
<s:hidden name="lastFilterValnaCBU" id="lastFilterValnaCBU" />
<s:hidden name="lastFilterValSIP" id="lastFilterValSIP" />
<s:hidden name="countVal" id="countVal" />


<div class="maincontainer">
	<div class="portlet" style="margin-top: 10px;">
			<div class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
					<span class="ui-icon ui-icon-minusthick-lp" onclick="togglecbbIdDiv(this)"></span>
					<s:text name="garuda.fundingNMDP.label.selectCBB" />
			</div>
			<div class="portlet-content">
			<div id="cbbIdDiv" style="float: left;">
					<table width="auto"><tr>
					<td><strong><s:text name="garuda.fundingNMDP.label.selectCBB" />:</strong></td>
					<td>
					<s:if test="#request.CbbList!=null">
						<%-- <s:select list="#request.CbbList" listKey="siteId" listValue="siteIdentifier" name="cbbdrpdwn"  id="cbbdrpdwn" multiple="true" cssStyle="height:80px;width:300px;" onchange="getCBBdetails(this.value);" /> --%>
							<s:textfield id="Searchbox" name="Searchbox" value="Enter Any CBB"></s:textfield><br>
								<select id="cbbdrpdwn" name="cbbdrpdwn" style="height:auto;width:300px;" onchange="getCBBdetails(this.value);" multiple="true">
								<s:iterator value="#request.CbbList">
								<option value=<s:property value='siteId'/> ><s:property value="siteIdentifier"/> - <s:property value="siteName"/></option>
								</s:iterator>
								<option value="<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@ALL_SITE"/>"><s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@ALL_OPTION"/></option>
		    					</select>
					</s:if>
					</td>
					</tr></table>
			</div>
			</div>
			</div>
	</div>	

<div class="clearDiv" style="clear: both;"><br/></div>
<div id="wrapperpending1">
			<div class="portlet" id="activeOrdDivparent">
           <s:if test="hasViewPermission(#request.actReqst)==true">			
			<div id="activeOrdDiv" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
					 <span class="ui-icon ui-icon-print" onclick="showContent(this,'activeOrders','flag_activeOrders');printWidgets('activeOrdDiv','allActiveContent')"></span>
					<%--<span class="ui-icon ui-icon-newwin"></span>
					<span class="ui-icon ui-icon-minusthick"></span> --%>
					<span class="ui-icon ui-icon-plusthick showHide activeOrder" onclick="showContent(this,'activeOrders','flag_activeOrders');"></span>
					<s:text name="garuda.pendingorderpage.portletname.orderrequests" />
			</div>
			<div class="portlet-content hide-data" id="allActiveContent">
									<div class="portletpending-content" id="pendingTblDiv">
									<div id="filterDiv1" class="pending-leftdiv" style="display: inline-block;">
									<a  href="#" name="createFilter" name="createFilter" id="createFilter" onclick="createActivefilter();"><span><s:text name="garuda.pendingorderpage.label.createcustomfilter"/></span></a>&nbsp;&nbsp;
									<a  href="#" id="executeFilter1" onclick="fn_FilterOrders()" ><s:text name="garuda.pendingorderpage.label.executeFilter"/></a>&nbsp;&nbsp;
									<a  href="#" name="resetFilter" id="resetFilter" onclick="fnResetOrdFilter();"><span><s:text name="garuda.pendingorderpage.label.resetallfilter"/></span></a>&nbsp;&nbsp;
									<a  href="#" name="resetSortOrder" id="resetSortOrder"><span><s:text name="garuda.pendingorderpage.label.resetsort"/></span></a>&nbsp;&nbsp;
									<div id="noOfdays_date" style="display: inline-block;"></div>
									<div id="newFilter" style="display: inline-block;"><s:if test="customFilter!=null && customFilter.size()>0"><s:select name="customFilters" id="customFilters" list="customFilter" onchange="ExecuteCustomFilterOrd(this.value)" listKey="filter_paraameter" listValue="filter_name" headerKey="" headerValue="-Custom Filter-"/></s:if></div>
									</div>
									<div style="clear: both;"></div>
									
									<br/>
									<s:hidden name="pStartDtVal" id="pStartDtVal" value="%{getpStartDt()}" />
									<s:hidden name="pEndDtVal" id="pEndDtVal" value="%{getpEndDt()}" />						
									<s:hidden name="cancelled_ack_orders" id="canackorders"></s:hidden>
									<s:hidden name="pendingFilter" id="filterflag"></s:hidden>
									<s:hidden name="userIdval" id="userIdval" value="#request.userId"></s:hidden>
                                    <s:hidden name="pfOrdertype" id="fordertype"></s:hidden>
                                    <s:hidden name="pfSiteId" id="fsiteid"></s:hidden>
                                    <div id="psOrders">
												<input type="hidden" name="entries" id="pendingEntries" value=<%=paginateSearch.getiShowRows()%> />
												<input type="hidden" name="tentries" id="tpendingEntries" value=<%=paginateSearch.getiTotalRows()%> />
												<table border="0" align="left" cellpadding="0" cellspacing="0" id="tablelookup" class="displaycdr">
															<thead id="pendingThead">
																<tr id="pendingTheadTr">
																	<th id="thSelectColumn">
																		<div class="cmDiv">
																		<ul class="clickMenu" id="ulSelectColumn">
																			<li class="main hover">
																				<img src="images/selectcol.png" alt='<s:text name="garuda.common.label.selectcolumns"/>' title=<s:text name="garuda.common.label.selectcolumns"/>/>
																					<div style="position: absolute; display: block;" class="outerbox inner">
																						<div class="shadowbox1"></div>
																						<div class="shadowbox2"></div>
																						<div class="shadowbox3"></div>
																						<ul class="innerBox" id="targetall"></ul>
																					</div>
																			</li>
																		</ul>
																		<div class="clear"></div>
																		</div>
																	</th>
																	<th class="sorting ord_cbbId postionth" onclick="fn_setSort_ord('ord_cbbId',this)"><s:text name="garuda.pendingorderpage.label.cddid" /></th>
																	<th class="sorting ord_cbbSate postionth" onclick="fn_setSort_ord('ord_cbbSate',this)"><s:text name="garuda.pendingorderpage.label.cbbsattellite" /></th>
																	<th class="sorting ord_regId postionth" onclick="fn_setSort_ord('ord_regId',this)"><s:text name="garuda.pendingorderpage.label.cburegid" /></th>
																	<th class="sorting ord_locCbuId postionth" onclick="fn_setSort_ord('ord_locCbuId',this)"><s:text name="garuda.pendingorderpage.label.localcbuid" /></th>
																	<th class="sorting ord_reqType postionth" onclick="fn_setSort_ord('ord_reqType',this)"><s:text name="garuda.pendingorderpage.label.requesttype" /></th>
																	<th class="sorting ord_priority postionth" onclick="fn_setSort_ord('ord_priority',this)"><s:text name="garuda.pendingorderpage.label.priority" /></th>
																	<th class="sorting ord_reqDt postionth" onclick="fn_setSort_ord('ord_reqDt',this)"><s:text name="garuda.cbuentry.label.requestDate" /></th>
																	<th class="sorting ord_noofdays postionth" onclick="fn_setSort_ord('ord_noofdays',this)"><s:text name="garuda.pendingorderpage.label.noofdaystable" /></th>
																	<th class="sorting ord_status postionth" onclick="fn_setSort_ord('ord_status',this)"><s:text name="garuda.pendingorderpage.label.status" /></th>
																	<th class="sorting ord_assignTo postionth" onclick="fn_setSort_ord('ord_assignTo',this)"><s:text name="garuda.pendingorderpage.label.assignedto" /></th>
																	<th class="sorting ord_ackRes postionth" onclick="fn_setSort_ord('ord_ackRes',this)"><s:text name="garuda.pendingorderpage.label.ackresolution" /></th>
																	<th class="sorting ord_closOrdRsn postionth" onclick="fn_setSort_ord('ord_closOrdRsn',this)"><s:text name="garuda.pendingorderpage.label.closeorderreason" /></th>
																	<th class="sorting ord_resTc postionth" onclick="fn_setSort_ord('ord_resTc',this)"><s:text name="garuda.pendingorderpage.label.resolutionfromtc" /></th>
																	<th class="sorting ord_reviewBy postionth" onclick="fn_setSort_ord('ord_reviewBy',this)"><s:text name="garuda.pendingorderpage.label.reviewedby" /></th>
																</tr>
																<tr>
																	<th><!--Th for column picker  --><s:checkbox id="sib" name="sib"></s:checkbox></th>
																	<th><s:textfield name="siteSeq" id="siteSeq1" placeholder="Enter Site ID" /></th>
																	<th><s:textfield name="satellite" id="satellite1" placeholder="Enter CBB Satellite"  /></th>
																	<th><s:textfield name="regId" id="regId1" placeholder="Enter CBU Registry ID"/></th>
																	<th><s:textfield name="locId" id="locId1" placeholder="Enter Local CBU ID" /></th>
																	<th>
																	<s:hidden name="sampleAtlab"/>
																	<s:hidden name="reqType"/>
																	<select id="orderType1" name="orderType1" style="width:auto">
																		<option value="">Select</option>
																			<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@ORDER_TYPE]">
																			<s:if test="subType=='CT' || subType=='HE' || subType=='OR' || subType=='ALL'">
																						<s:if test="subType=='CT'" >
																							<option value="<s:property value='pkCodeId' />" <s:if test='sampleAtlab=="Y"'>selected="selected"</s:if>><%=VelosMidConstants.CT_LAB_ORDER%></option>
																							<option value="<s:property value='pkCodeId'/>" <s:if test='sampleAtlab=="N"'>selected="selected"</s:if>><%=VelosMidConstants.CT_SHIP_ORDER%></option>
																						</s:if>
																						<s:if test="subType=='ALL'" >
																							<option value="ALL" <s:if test="reqType=='ALL'">selected="selected"</s:if>><s:property value='description'/></option>
																						</s:if>
																						<s:if test="subType=='HE' || subType=='OR'">
																							<option value="<s:property value='pkCodeId'/>" <s:if test="reqType==pkCodeId">selected="selected"</s:if> ><s:property value='description'/></option>
																						</s:if>
																			</s:if>
																			</s:iterator>
																	</select>
																	</th>
																	<th>
																		<select id="orderPriority1" name="orderPriority1" style="width:auto">
																			<option value="">Select</option>
																			<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@ORDER_PRIORITY]">
																			<option value=<s:property value='pkCodeId'/>  <s:if test="%{priority==pkCodeId}">selected="selected"</s:if>><s:property value="description"/></option>
																			</s:iterator>
																		</select>
																	</th>
																	<th>
																		<input type="button" id="ordRange" value="<s:text name='garuda.pendingorder.button.dateRange'/>" onclick='showDiv(this,"dateDiv","dateStart","dateEnd"); resetValidForm("dummyForm");' />
																	</th>
																	<th>
																		<input type="button" id="noOfdays" value="<s:text name='garuda.pendingorderpage.label.noofdays' />" onclick='showNoDiv(this,"noDiv","noOfdaystxt")' />
																	</th>
																	<th>
																		<select id="orderStatus1" name="orderStatus1" style="width:auto">
																			<option value="">Select</option>
																			<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@ORDER_STATUS]">
																			<s:if test="subType!='close_ordr' && subType!='CANCELLED'">
																			<s:if test="%{pkCodeId==status}">
																			<option value="<s:property value='pkCodeId'/>" selected="selected" ><s:property value="description"/></option>
																			</s:if>
																			<s:else>
																			<option value="<s:property value='pkCodeId'/>"> <s:property value="description"/></option>
																			</s:else>
																			</s:if>
																			</s:iterator>
																		</select>
																	</th>
																	<th id="assignToTh" align="center">
																	<s:if test="assignUsrLst!=null && assignUsrLst.size()>0">
																	<s:hidden name="assignLstSize" id="assignLstSize" value="%{assignUsrLst.size()}"/>
																	</s:if>
																	<select id="assignTo1" name="assignTo1" style="width:auto" class="searchableSelect" >
																			<option value="">Select</option>
																			<s:if test="labelList!=null && labelList.size()>0">
																			<s:iterator value="labelList" var="rowIndex">
																			<s:if test="%{#rowIndex[0]==#request.username}">
																			<option value='<s:property value="%{#rowIndex[0]}"/>' <s:if test="%{assignto==#rowIndex[0]}">selected="selected"</s:if>  >Me</option>
																			</s:if>
																			</s:iterator>
																			<option value="ALL" <s:if test="%{assignto=='ALL'}">selected="selected"</s:if>>ALL</option>
																			<option value="Unassigned" <s:if test="%{assignto=='Unassigned'}">selected="selected"</s:if>>Unassigned</option>
																			<s:iterator value="labelList" var="rowIndex">
																			<s:if test="%{#rowIndex[0]!=#request.username}">
																			<option value='<s:property value="%{#rowIndex[0]}"/>' <s:if test="%{assignto==#rowIndex[0]}">selected="selected"</s:if>><s:property value="%{#rowIndex[2]}"/></option>
																			</s:if>
																			</s:iterator>
																			</s:if>
																	</select>
																	</th>
																	<th><%--AcknowledgeResolution --%><s:select name="ackresolution" id="ackresolution1" list="{'Y'}" headerKey="" headerValue="Select"/></th>
																	<th>
																	<select id="closeReason1" name="closeReason1" style="width:auto">
																			<option value="">Select</option>
																			<s:iterator value="getCBUStatusValues(@com.velos.ordercomponent.business.util.VelosMidConstants@CLOSE_REASON)">
																			<s:if test="%{pkcbustatus==cancelreason}">
																			<option value="<s:property value='pkcbustatus'/>" selected="selected" ><s:property value="cbuStatusDesc"/></option>
																			</s:if>
																			<s:else>
																			<option value="<s:property value='pkcbustatus'/>"> <s:property value="cbuStatusDesc"/></option>
																			</s:else>
																			</s:iterator>
																	</select>
																	</th>
																	<th>
																	<select id="resolution1" name="resolution1" style="width:auto">
																			<option value="">Select</option>
																			<s:iterator value="getCBUStatusValues(@com.velos.ordercomponent.business.util.VelosMidConstants@RESOLUTION)">
																			<s:if test="%{pkcbustatus==resolution}">
																			<option value="<s:property value='pkcbustatus'/>" selected="selected" ><s:property value="cbuStatusDesc"/></option>
																			</s:if>
																			<s:else>
																			<option value="<s:property value='pkcbustatus'/>"> <s:property value="cbuStatusDesc"/></option>
																			</s:else>
																			</s:iterator>
																	</select>
																	</th>
																	<th>
																		<s:if test="labelList!=null && labelList.size()>0">
																			<select name="reviewby" id="reviewby">
																			<option value="">Select</option>
																			<s:iterator value="labelList" var="rowIndex">
																			<option value='<s:property value="%{#rowIndex[0]}"/>' <s:if test="%{reviewby==#rowIndex[0]}">selected="selected"</s:if>><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
																			</select>
																		</s:if>		
																		<s:else>
																			<s:select name="reviewby" headerKey="" headerValue="Select" list="{}" id="reviewby"></s:select>														
																		</s:else>
																	</th>
																</tr>
															</thead>
															<tbody id="tableLookupTbody">
															<s:if test="orderDetailsList!=null && orderDetailsList.size()>0">
															<s:hidden name="visibleRecCount" value="%{orderDetailsList.size()}" id="visibleRecCount"/>
															<s:iterator value="orderDetailsList" var="rowVal" status="row">
																<tr>
																	<td><s:checkbox name="selectrow%{#row.index}" id="selectrow%{#row.index}" fieldValue="%{#rowVal[12]}"/></td>
																	<td><div style="display: none;" class="printHideDiv"><s:property value="%{removeSymbol(#rowVal[0],'-')}"/></div><a href="#" onclick="getSiteDetails('<s:property value="%{#rowVal[14]}"/>')" ><s:property value="%{#rowVal[0]}" /></a><s:hidden name="hpkcordId%{#row.index}" value="%{#rowVal[13]}" id="hpkcordId%{#row.index}"/></td>										
																	<td><div style="display: none;" class="printHideDiv"><s:property value="%{removeSymbol(#rowVal[1],'-')}"/></div><s:property value="%{#rowVal[1]}"  /></td>						
																	<td><div style="display: none;" class="printHideDiv"><s:property value="%{removeSymbol(#rowVal[2],'-')}"/></div><a href="#" onclick="getCordDetails('<s:property value="%{#rowVal[13]}"/>')"><s:property value="%{#rowVal[2]}"  /></a></td>						
																	<td><div style="display: none;" class="printHideDiv"><s:property value="%{removeSymbol(#rowVal[3],'-')}"/></div><s:property value="%{#rowVal[3]}"  /></td><s:hidden name="hReqType%{#row.index}" value="%{#rowVal[18]}" id="hReqType%{#row.index}"/>						
																	<td><div style="display: none;" class="printHideDiv"><s:property value="%{removeSymbol(#rowVal[4],'-')}"/></div><s:property value="%{#rowVal[4]}"/></td>						
																	<td><s:property value="%{#rowVal[15]}"/></td>				
																	<td><s:property value="%{#rowVal[5]}"  /></td>						
																	<td><s:property value="%{#rowVal[6]}"/></td>	
																	<td><div style="display: none;" class="printHideDiv"><s:property value="%{#rowVal[9]}"/></div><a href="#" onclick="loadGroup('<s:property value="%{#rowVal[12]}"/>','<s:property value="%{#rowVal[4]}"  />','<s:property value="%{#rowVal[13]}"/>','<s:property value="%{#rowVal[14]}"/>');"><s:property value="%{#rowVal[9]}"  /></a></td>
																	<td>
																	<s:if test="assignUsrLst!=null && assignUsrLst.size()>0">
																		<s:if test="hasEditPermission(#request.assignOrders)==true">
																			<select name="assigntodataTbl<s:property value="%{#row.index}"/>" id="assigntodataTbl<s:property value="%{#row.index}"/>" onchange="setAssignAction(this)" >
																			<option value="">Unassigned</option>
																			<s:iterator value="assignUsrLst" var="rowIndex">
																			<s:if test="%{#rowIndex[1]==#rowVal[14]}">
																			<option value='<s:property value="%{#rowIndex[0]}"/>' <s:if test="%{#rowVal[10]==#rowIndex[0]}">selected="selected"</s:if>><s:property value="%{#rowIndex[2]}"/></option>
																			</s:if>
																			</s:iterator>
																			</select>
																			<s:hidden name="hassignTo%{#row.index}" value="%{#rowVal[10]}" id="hassignTo%{#row.index}"/>
																		</s:if>
																		<s:elseif test="hasViewPermission(#request.assignOrders)==true">
																				<!--<s:select listKey="userId"  listValue="loginId" list="assignUsrLst" name="assigntodataTbl%{#row.index}" id="assigntodataTbl%{#row.index}" headerKey="" headerValue="Unassigned" disabled="true" value="%{#rowVal[10]}"/>-->
																					<select name="assigntodataTbl<s:property value="%{#row.index}"/>" id="assigntodataTbl<s:property value="%{#row.index}"/>" disabled="disabled">
																					<option value="">Unassigned</option>
																					<s:iterator value="assignUsrLst" var="rowIndex">
																					<s:if test="%{#rowIndex[1]==#rowVal[14]}">
																					<option value='<s:property value="%{#rowIndex[0]}"/>' <s:if test="%{#rowVal[10]==#rowIndex[0]}">selected="selected"</s:if>><s:property value="%{#rowIndex[2]}"/></option>
																					</s:if>
																					</s:iterator>
																					</select>
																		</s:elseif>																		
																	</s:if>
																	<!--<s:property value="%{#rowVal[10]}"  />-->
																	</td>						
																	<td><div style="display: none;" class="printHideDiv"><s:property value="%{#rowVal[7]}"/></div>
																		<s:if test="%{#rowVal[8]!=null}">
																		<%--<s:property value="%{#rowVal[7]}"  /> --%>
																		<s:select name="ackResult%{#row.index}" id="ackResult%{#row.index}"  value="%{#rowVal[7]}" list="{'Y'}" headerKey="" headerValue="-select-" onchange="setAckAction(this)"/>
																		</s:if>
																	</td>
																	<td><s:property value="%{#rowVal[17]}"  /></td>
																	<td><s:property value="%{#rowVal[8]}"  /></td>						
																	<td><s:property value="%{#rowVal[16]}"  /></td>	
																</tr>
																</s:iterator>
																</s:if>
																</tbody>		
															<tfoot>
																<tr id="ordTblFoot">
																	<td colspan="15" width="100%">
																		<jsp:include page="paginationFooter.jsp">
																		  	<jsp:param value="pendingTblDiv" name="divName"/>
																		  	<jsp:param value="pendingOrderForm" name="formName"/>
																		  	<jsp:param value="showsRowtablelookup" name="showsRecordId"/>
																		  	<jsp:param value="getPSOrders" name="url"/>
																		  	<jsp:param value="workflow" name="paginateSearch"/>
																		  	<jsp:param value="pendingFilter" name="filterflag"/>
																		  	<jsp:param value="pfOrdertype" name="ordertype"/>
																		  	<jsp:param value="pfSiteId" name="siteId"/>
																		  	<jsp:param value="allparams" name="dropdownfilter"/>
																		  	<jsp:param value="ord_sortParam" name="ord_sortParam"/>
																		  	<jsp:param value="ord_allentries_flag" name="allentries"/>
																		  	<jsp:param value="1" name="idparam"/>
																		  	<jsp:param value="pendingOrdersOnLoad" name="bodyOnloadFn"/>
																	   </jsp:include>
																	</td>
																</tr>
															</tfoot>		
														</table>
														<div align="center"><span id="activeStatFilterFlagTxt"></span></div>
										</div>
										<div id="mainpending-assignbar" class="quicklinkpanel pending-assingnbar">
	
											<table cellpadding="0" cellspacing="0" style="width:100%;">
												<tr bordercolor="black">
													<th style="text-align: left;">
													<s:text name="garuda.pendingorderpage.label.assignedto"></s:text>
													</th>
													<th>
													<s:if test="commonUsrLst!=null && commonUsrLst.size()>0">
													<s:if test="hasEditPermission(#request.assignOrders)==true">
															<%-- <s:select listKey="userId"  listValue="loginId" list="assignUsrLst" name="assigntodata" id="assignto" headerKey="" headerValue="-select- " /> --%>
															<select name="assigntodata" id="assignto" style="width:auto">
															<option value="">Select</option>
															<s:iterator value="commonUsrLst" var="rowIndex">
															<option value='<s:property value="%{#rowIndex[0]}"/>'><s:property value="%{#rowIndex[2]}"/></option>
															</s:iterator>
															</select>
													</s:if>
													<s:elseif test="hasViewPermission(#request.assignOrders)==true">
															<%-- <s:select listKey="userId"  listValue="loginId" list="assignUsrLst" name="assigntodata" id="assignto" headerKey="" headerValue="-select- " disabled="true"/> --%>
															<select name="assigntodata" id="assignto" disabled="disabled" style="width:auto">
															<option value="">Select</option>
															<s:iterator value="commonUsrLst" var="rowIndex">
															<option value='<s:property value="%{#rowIndex[2]}"/>'><s:property value="%{#rowIndex[2]}"/></option>
															</s:iterator>
															</select>
													</s:elseif>																		
													</s:if>
													</th>
													<th style="text-align: left;">
													<s:text name="garuda.pendingorderpage.label.ackresolution"></s:text>
													</th>
													<th>
													<s:select name="assign-ackResult" id="assign-ackResult" list="{'Y'}" headerKey="" headerValue="-select-" />
													</th>
													<th>
													<table bgcolor="#cccccc" style="padding-top: 5px;" id="esignTbl">
																		<tr bgcolor="#cccccc" valign="baseline">
																			<td width="70%">
																					<jsp:include page="cb_esignature.jsp" flush="true">
																						<jsp:param value="pendingAsignsubmit" name="submitId"/>
																						<jsp:param value="pendingAsigninvalid" name="invalid" />
																						<jsp:param value="pendingAsignminimum" name="minimum" />
																						<jsp:param value="pendingAsignpass" name="pass" />
																					</jsp:include>
																			</td>
							    											<td width="30%" align="right">
							    												<input type="button" id="pendingAsignsubmit" disabled= "disabled" onclick="submitAssingnTo()" value="<s:text name="garuda.unitreport.label.button.submit"/>" />
							    											</td>
						    											</tr>	
						  							</table>
													</th>
												</tr>
											</table>
										</div>
										<div>   
											<br/>
											<br/>
											<br/>
											<br/>
											<br/>
											<br/>
											<br/>
										</div>
									</div>
							<!-- </fieldset> -->
				<br/>
				<br/>
				<br/>
	
	   	
	   	</div></div>
	   	</s:if>
	   	</div>
	   
	     			

</div>



	
<div style="clear: both;"><br/><br/></div>

<div id="wrapperpending2">	
			<!-- <fieldset id="OthrReqfieldset"><legend><s:text name="garuda.pendingorderpage.portletname.otherrequests" /></legend> -->
			<div class="column">
			<div class="portlet" id="otherOrdDivparent">
		<s:if test="hasViewPermission(#request.othrReqst)==true">				
			<div id="otherOrdDiv" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
					<span class="ui-icon ui-icon-print" onclick="printWidgets('otherOrdDiv','printOtherDiv')"></span>
					<%-- <span class="ui-icon ui-icon-newwin"></span>
					<span class="ui-icon ui-icon-minusthick"></span> --%>
					<span class="ui-icon ui-icon-plusthick showHide otherRequestWid" id="otherSpan" onclick="showContent(this,'otherOrders','flag_otherOrders');"></span>
					<s:text name="garuda.pendingorderpage.portletname.otherrequests" />
			</div>
			<div class="portlet-content hide-data" id="printOtherDiv">
							<div id="leftpending2" class="pending-leftdiv" style="display: inline-block;">
								<a  href="#" name="createFilter1" id="createFilter" onclick="createOtherfilter();" ><span><s:text name="garuda.pendingorderpage.label.createcustomfilter"/></span></a>&nbsp;&nbsp;
								<a  href="#" id="executeFilter2" onclick="fn_FilterOthers()" ><s:text name="garuda.pendingorderpage.label.executeFilter"/></a>&nbsp;&nbsp;
								<a  href="#" name="resetFilter1"  id="resetFilter" onclick="fnResetCordEntryFilter();"><span><s:text name="garuda.pendingorderpage.label.resetallfilter"/></span></a>&nbsp;&nbsp;
								<a  href="#" name="resetOtherNA" id="resetOtherNA" style="display: none"><span><s:text name="garuda.pendingorderpage.label.resetsort"/></span></a>&nbsp;&nbsp;
								<a  href="#" name="resetOtherSIP" id="resetOtherSIP" style="display: none"><span><s:text name="garuda.pendingorderpage.label.resetsort"/></span></a>&nbsp;&nbsp;
								<div id="noOfdays_date1" style="display: inline-block;"></div>
								<div id="newCordFilter" style="display: inline-block;">
								<div id="newFilterSIP" style="display: inline-block;">
									<s:if test="customFiltersip!=null && customFiltersip.size()>0"><s:select name="customFilterssip" id="customFilterssip" list="customFiltersip" onchange="ExecuteCustomFilterSIP(this.value)" listKey="filter_paraameter" listValue="filter_name" headerKey="" headerValue="-Custom Filter-"/></s:if>
								</div>
								<div id="newFilterNACBU" style="display: inline-block;">
									<s:if test="customFilternacbu!=null && customFilternacbu.size()>0"><s:select name="customFiltersNACBU" id="customFiltersNACBU" list="customFilternacbu"  listKey="filter_paraameter" listValue="filter_name" headerKey="" headerValue="-Custom Filter-" onchange="ExecuteCustomFilterNA(this.value)"/></s:if>
								</div>
								</div>
								
							</div>
						
							<br/>
							<br/>
						
								<div id="" class="demo">
											<div id="tabs" class="pendingtabs">
											    <ul class="pendingtabsul">
											        <s:if test="hasViewPermission(#request.notAvailCbu)==true">
											    	   <li><a href="#not-available-cbu" onclick="hideTabDiv('not-available-cbu');highlight_tab('not-avail-anchor');printPendingTab('NA-Cbu-tbl_wrapper','Not Available CBUs');" id="not-avail-anchor"><span><s:text name="garuda.pendingorderpage.label.notavailcbu" /></span></a></li>
											        </s:if>
											        <s:if test="hasViewPermission(#request.svdinPrgrss)==true">
											           <li><a href="#cord-entry" onclick="hideTabDiv('cord-entry');highlight_tab('cord-entry-anchor');printPendingTab('cordentrytbl_wrapper','Saved in Progress CBUs');" id="cord-entry-anchor"><span><s:text name="garuda.pendingorderpage.label.cordentryprogress" /></span></a></li>
											        </s:if>
											       <s:if test="hasViewPermission(#request.viewFunding)==true">
											        <li><a href="#funding"  onclick="hideTabDiv('funding');highlight_tab('funding-anchor');printPendingTab('funding','Funding Requested');" id="funding-anchor"><span><s:text name="garuda.pendingorderpage.label.funding" /></span></a></li>
											       </s:if>
											    </ul>
											    <!--  -->
											    <div id="not-available-cbu" style="width: 96%;" >
											     	<input type="hidden" name="entries" id="naCBUEntries" value=<%=paginationSearch1.getiShowRows() %> />
											     	<input type="hidden" name="tentries" id="tnaCBUEntries" value=<%=paginationSearch1.getiTotalRows()%> />
											        <table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="NA-Cbu-tbl">
														<thead id="naCBUThead">
															<tr id="naCBUTheadTr">
																<th id="thSelectColumn2">
																<div class="cmDiv">
																<ul class="clickMenu" id="ulSelectColumn2">
																	<li class="main hover"><img src="images/selectcol.png"
																		alt="select columns" title="select columns" />
																	<div style="position: absolute; display: block;"
																		class="outerbox inner">
																	<div class="shadowbox1"></div>
																	<div class="shadowbox2"></div>
																	<div class="shadowbox3"></div>
																	<ul class="innerBox" id="targetall2"></ul>
																	</div>
																	</li>
																</ul>
																<div class="clear"></div>
																</div>
																</th>
																<th class="sorting naCBU_cbbId postionth" onclick="fn_setSort_naCBU('naCBU_cbbId',this)"><s:text name="garuda.pendingorderpage.label.cddid" /></th>
																<th class="sorting naCBU_regId postionth" onclick="fn_setSort_naCBU('naCBU_regId',this)"><s:text name="garuda.pendingorderpage.label.cburegid" /></th>
																<th class="sorting naCBU_cbuId postionth" onclick="fn_setSort_naCBU('naCBU_cbuId',this)"><s:text name="garuda.pendingorderpage.label.localcbuid" /></th>
																<th class="sorting naCBU_cbuStat postionth" onclick="fn_setSort_naCBU('naCBU_cbuStat',this)"><s:text name="garuda.pendingorderpage.label.cbustatus" /></th>
																<th class="sorting naCBU_reas postionth" onclick="fn_setSort_naCBU('naCBU_reas',this)"><s:text name="garuda.pendingorderpage.label.reason" /></th>
																<th class="sorting naCBU_removeBy postionth" onclick="fn_setSort_naCBU('naCBU_removeBy',this)"><s:text name="garuda.pendingorderpage.label.removedby" /></th>
															</tr>
															<tr>
																<th></th>
																<th><!-- CBB id  --><s:textfield name="nasiteId" id="nasiteId1" placeholder="Enter Site ID"/></th>
																<th><s:textfield name="naregId" id="naregId1" placeholder="Enter CBU Registry ID"/></th>
																<th><!-- CBU Loc id --><s:textfield name="nalocId" id="nalocId1"  placeholder="Enter Local CBU ID" /></th>
																<th>
																<select id="cbuStatus1" name="cbuStatus1">
																	<option value="">Select</option>
																	<s:iterator value="getCBUStatusValues(@com.velos.ordercomponent.business.util.VelosMidConstants@NATIONAL_STATUS)">
																	<s:if test="%{pkcbustatus==nastatus}">
																	<option value="<s:property value='pkcbustatus'/>" selected="selected" ><s:property value="cbuStatusDesc"/></option>
																	</s:if>
																	<s:else>
																	<option value="<s:property value='pkcbustatus'/>"> <s:property value="cbuStatusDesc"/></option>
																	</s:else>
																	</s:iterator>
																</select>
																</th>
																<th>
																<select id="cbuReason1" name="cbuReason1">
																	<option value="">Select</option>
																	<s:iterator value="getCBUStatusValues(@com.velos.ordercomponent.business.util.VelosMidConstants@DEFRED_CORD)">
																	<s:if test="%{pkcbustatus==nareason}">
																	<option value="<s:property value='pkcbustatus'/>" selected="selected" ><s:property value="cbuStatusDesc"/></option>
																	</s:if>
																	<s:else>
																	<option value="<s:property value='pkcbustatus'/>"> <s:property value="cbuStatusDesc"/></option>
																	</s:else>
																	</s:iterator>
																</select>
																
																</th>
																<th>
																<s:if test="assignUsrLst!=null && assignUsrLst.size()>0">
																			<select name="assigntodata" id="reviewby1"  name="naremovedby">
																			<option value="">Select</option>
																			<s:iterator value="assignUsrLst" var="rowIndex">
																			<option value='<s:property value="%{#rowIndex[0]}"/>'><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
																			</select>
																</s:if>		
																<s:else>
																			<s:select name="empty" headerKey="" headerValue="Select" list="{}" id="reviewby1"></s:select>														
																</s:else>
																</th>
															</tr>
														</thead>
														<tbody id="naCBUTbody">
															<s:if test="naCbuList!=null && naCbuList.size()>0">
															<s:iterator value="naCbuList" var="rowIndex" status="row">
																	<tr>
																		<td></td>
																		<td>
																			<div style="display: none;" class="printHideDiv"><s:property value="%{removeSymbol(#rowIndex[0],'-')}"/></div><a href="#" onclick="getSiteDetails('<s:property value="%{#rowIndex[7]}"/>');" ><s:property value="%{#rowIndex[0]}" /></a>
																		</td>
														  <td>
																<!--<div style="display: none;"><s:property value="%{removeSymbol(#rowIndex[1],'-')}"/></div>
															    <a href="#" onclick="getCordDetails('<s:property value="%{#rowIndex[6]}"/>');"><s:property value="%{#rowIndex[1]}"  /></a>				
															     -->
															<s:if test="%{#rowIndex[11]!=null && #rowIndex[11]==0}">
															     <div style="display: none;" class="printHideDiv"><s:property value="%{removeSymbol(#rowIndex[1],'-')}"/></div><a href="#" onclick="submitpost('cordEditDataEntry',{'cdrCbuPojo.cordID':'<s:property value="%{#rowIndex[6]}" />'});" ><s:property value="%{#rowIndex[1]}"  /></a>
															</s:if>
															<s:elseif test="%{#rowIndex[11]!=null && #rowIndex[11]==1}">
																  <div style="display: none;" class="printHideDiv"><s:property value="%{removeSymbol(#rowIndex[1],'-')}"/></div><a href="#" onclick="getCordDetails('<s:property value="%{#rowIndex[6]}"/>');"><s:property value="%{#rowIndex[1]}"  /></a>
												             </s:elseif>	
												           <s:else>
												                  <s:property value="%{#rowIndex[1]}" />
												            </s:else>
												            </td>
																		<td><div style="display: none;" class="printHideDiv"><s:property value="%{removeSymbol(#rowIndex[2],'-')}"/></div><s:property value="%{#rowIndex[2]}"/></td>
																		<td><s:property value="%{#rowIndex[3]}"/></td>
																		<td><s:property value="%{#rowIndex[4]}"/></td>
																		<td><s:property value="%{#rowIndex[5]}"/>
																		</td>
																	</tr>
															</s:iterator>
															</s:if>
														</tbody>
														<tfoot>
																<tr id="naTblFoot">
																	<td colspan="7" width="100%">
																						<jsp:include page="paginationFooter.jsp">
																						  	<jsp:param value="not-available-cbu" name="divName"/>
																						  	<jsp:param value="pendingOrderForm" name="formName"/>
																						  	<jsp:param value="showsRowNA-Cbu-tbl" name="showsRecordId"/>
																						  	<jsp:param value="getPSNACBU" name="url"/>
																						  	<jsp:param value="naAvailCBU" name="paginateSearch"/>
																						  	<jsp:param value="notAvailCbuTblOnLoad" name="bodyOnloadFn"/>
																						  	<jsp:param value="allparamsNaCBU" name="naCBUparams"/>
																						  	<jsp:param value="naCBUflag" name="naCBUflag"/>
																						  	<jsp:param value="naCBU_sortParam" name="ord_sortParam"/>
																						  	<jsp:param value="nacbu_allentries_flag" name="allentries"/>
																						  	<jsp:param value="2" name="idparam"/>
																					   </jsp:include>
																	</td>
																</tr>
														</tfoot>		
													</table>
													<div align="center" id="naCBUStatFilter"><span id="naCBUStatFilterFlagTxt"></span></div>
											    </div>
											    <div id="cord-entry" style="width:96%;">
											    	  <s:hidden name="sipStartDt" id="sippStartDtVal"/>
												      <s:hidden name="sipEndDt" id="sippEndDtVal" />
											  		  <input type="hidden" name="entries" id="cordEntries" value=<%=paginationSearch.getiShowRows() %> />
											  		  <input type="hidden" name="tentries" id="tcordEntries" value=<%=paginationSearch.getiTotalRows()%> />
													  <table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="cordentrytbl" bordercolor="#000">
															<thead id="cordThead">
																<tr id="cordTheadTr">
																	<th id="thSelectColumn1">
																		<div class="cmDiv">
																		<ul class="clickMenu" id="ulSelectColumn1">
																			<li class="main hover">
																				<img src="images/selectcol.png" alt="select columns" title="select columns"/>
																					<div style="position: absolute; display:block;" class="outerbox inner">
																						<div class="shadowbox1"></div>
																						<div class="shadowbox2"></div>
																						<div class="shadowbox3"></div>
																						<ul class="innerBox" id="targetall1"></ul>
																					</div>
																			</li>
																		</ul>
																		<div class="clear"></div>
																		</div>
																	</th>
																	<th class="sorting sip_cbbid postionth" onclick="fn_setSort_sip('sip_cbbid',this)"><s:text name="garuda.pendingorderpage.label.cddid" /></th>
																	<th class="sorting sip_cbuRegId postionth" onclick="fn_setSort_sip('sip_cbuRegId',this)"><s:text name="garuda.pendingorderpage.label.cburegid" /></th>
																	<th class="sorting sip_loccbuid postionth" onclick="fn_setSort_sip('sip_loccbuid',this)"><s:text name="garuda.pendingorderpage.label.localcbuid" /></th>
																	<th class="sorting sip_doe postionth" onclick="fn_setSort_sip('sip_doe',this)"><s:text name="garuda.pendingorderpage.label.dateofentry" /></th>
																	<th class="sorting sip_noofdays postionth" onclick="fn_setSort_sip('sip_noofdays',this)"><s:text name="garuda.pendingorderpage.label.noofdaystable" /></th>
																	<th class="sorting sip_percent postionth" onclick="fn_setSort_sip('sip_percent',this)"><s:text name="garuda.pendingorderpage.label.percentagecom" /></th>
																</tr>
																<tr>
																	<th></th>
																	<th><s:textfield name="sipsiteId" id="sipsiteId1" placeholder="Enter Site ID" /></th>
																	<th><s:textfield name="sipregId" id="sipregId1" placeholder="Enter CBU Registry ID" /></th>
																	<th><s:textfield name="siplocId" id="siplocId1" placeholder="Enter Local CBU ID" /></th>
																	<th><input type="button" id="cordRange" value="<s:text name="garuda.pendingorder.button.dateRange"/>" onclick="showDiv(this,'dateDiv1','dateStart1','dateEnd1'); resetValidForm('dummyForm');" /></th>
																	<th><input type="button" id="noOfdays1" value="<s:text name='garuda.pendingorderpage.label.noofdays'/>" onclick="showNoDiv(this,'noDiv1','noOfdaystxt1')" /></th>
																	<th><input type="button" id="percentage" value="<s:text name='garuda.pendingorderpage.label.percentcompgreater' />" onclick="showNoDiv(this,'perCentageDiv','sippercentval')" /></th>
																</tr>
															</thead>
															<tbody id="sipTbody">
																<s:if test="cordentryprogress!=null && cordentryprogress.size()>0">
																<s:iterator value="cordentryprogress" var="rowIndex" status="row">
																	<tr>
																		<td></td>
																		<td><div style="display: none;" class="printHideDiv"><s:property value="%{removeSymbol(#rowIndex[6],'-')}"/></div><a href="#" onclick="getSiteDetails('<s:property value="%{#rowIndex[7]}"/>');" ><s:property value="%{#rowIndex[6]}"/></a></td>
																		<td>
																			<div style="display: none;" class="printHideDiv"><s:property value="%{removeSymbol(#rowIndex[0],'-')}"/></div>
																			<a href="#" onclick="getCordEntry('<s:property value="%{#rowIndex[5]}"/>');">
																				<s:property value="%{#rowIndex[0]}"/>
																			</a>
																		</td>
																		<td><div style="display: none;" class="printHideDiv"><s:property value="%{removeSymbol(#rowIndex[1],'-')}"/></div><s:property value="%{#rowIndex[1]}"/></td>
																		<td><s:property value="%{#rowIndex[2]}"/></td>
																		<td><s:property value="%{#rowIndex[3]}"/></td>
																		<td><s:property value="%{#rowIndex[4]}"/></td>
																   </tr>
																</s:iterator>
																</s:if>
															</tbody>
															<tfoot>
																<tr id="sipTblFoot">
																	<td colspan="7" width="100%">
																						<jsp:include page="paginationFooter.jsp">
																						  	<jsp:param value="cord-entry" name="divName"/>
																						  	<jsp:param value="pendingOrderForm" name="formName"/>
																						  	<jsp:param value="showsRowcordentrytbl" name="showsRecordId"/>
																						  	<jsp:param value="getPSSIP" name="url"/>
																						  	<jsp:param value="orders" name="paginateSearch"/>
																						  	<jsp:param value="cordTblOnLoad" name="bodyOnloadFn"/>
																						  	<jsp:param value="allparamSIP" name="sipparams"/>
																						  	<jsp:param value="sip_sortParam" name="ord_sortParam"/>
																						  	<jsp:param value="sipflag" name="sipflag"/>
																						  	<jsp:param value="sip_allentries_flag" name="allentries"/>
																						  	<jsp:param value="3" name="idparam"/>
																					   </jsp:include>
																	</td>
																</tr>
															</tfoot>		
														</table>
															 <div align="center" id="cordStatFilter"><span id="cordStatFilterFlagTxt"></span></div>
											    </div>
											    <div id="funding" style="width: 96%;display: none;">
											         <input type="hidden" name="entries" id="fundingEntries" value=<%=paginationSearch.getiShowRows() %> />
													  <table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="fundingtbl" bordercolor="#000">
															<thead>
																<tr>
																	<th><s:text name="garuda.pendingorderpage.label.cburegid" /></th>
																	<th><s:text name="garuda.pendingorderpage.label.localcbuid" /></th>
																	<th><s:text name="garuda.pendingorderpage.label.nationalregisstatus" /></th>
																	<th><s:text name="garuda.pendingorderpage.label.reasoncode" /></th>
																	<th><s:text name="garuda.pendingorderpage.label.birthdate" /></th>
																	<th><s:text name="garuda.pendingorderpage.label.cburegistration" /></th>
																	<th><s:text name="garuda.pendingorderpage.label.tnc" /></th>
																	<th><s:text name="garuda.pendingorderpage.label.funded" /></th>
																	<th><s:text name="garuda.pendingorderpage.label.hrsaracerollup" /></th>
																	<th><s:text name="garuda.pendingorderpage.label.fundingcategory" /><s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@FUNDING_CATEGORY]">
																		<s:checkbox name = "fundCat" value ="description"/><s:property value="description"/>
																	</s:iterator></th>
																	<th><s:text name="garuda.pendingorderpage.label.requestfunding" /><s:checkbox name="" id="checkbox"></s:checkbox></th>
																	<!--<th><s:text name="Registry Status" /></th>
																	<th><s:text name="Funding Requested" /></th>
																	-->
																</tr>
																<tr>
																	
																	<th></th>
																	<th></th>
																	<th></th>
																	<th></th>
																	<th></th>
																	<th></th>
																	<th></th>
																	<th></th>
																	<th></th>
																	<th></th>
																	<th></th>
																	<!--<th></th>
																	<th></th>
																--></tr>
															</thead>
															<tbody id="">
																	
																<%-- <s:if test="fundedCbu!=null && fundedCbu.size()>0">
																
																<s:iterator value="fundedCbu" var="rowIndex" status="row">
																	<tr>
																		<td><s:property value="%{#rowIndex[2]}"/></td>
																		<td><s:property value="%{#rowIndex[3]}"/></td>
																		<td></td>
																		<td></td>
																		<td><s:property value="%{#rowIndex[4]}"/></td>
																		<td></td>
																		<td><s:property value="%{#rowIndex[0]}"/></td>
																		<td><s:property value="%{#rowIndex[1]}"/></td>
																		<td></td>
																		<td><s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@FUNDING_CATEGORY]">
																		<s:checkbox name = "fundCat" value ="description"/></s:iterator></td>
																		<td><s:checkbox name="" id="checkbox"></s:checkbox></td>
																		<!--<td><s:property value="%{#rowIndex[5]}"/></td>
																		<td><s:property value="%{#rowIndex[6]}"/></td>
																	--></tr>
																</s:iterator>
																</s:if> --%>
																<s:else>
																	<tr><td colspan="9" width="100%"><s:text name="garuda.pendingorder.label.noRecordFound"/></td></tr>
																</s:else>
															</tbody>
															<tfoot>
																<tr>
																	
																</tr>
															</tfoot>		
														</table>
														<s:text name = "garuda.pendingorder.label.noRecordFound"/>
														<jsp:include page="cb_esignature.jsp" flush="true"></jsp:include>
											    			<button type="button" id="save" onclick="" ><s:text
																name="garuda.common.saveforlater" ></s:text></button>
										
														<table border="0">
															<tbody>	
																<tr>
																		<th><s:text name="garuda.pendingorderpage.label.cburegid" /></th>
																		<th><s:text name="garuda.pendingorderpage.label.localcbuid" /></th>
																</tr>
																<tr>
																		<th></th>
																		<th></th>
																</tr>
															</tbody>
														</table>
											    </div>
											    
										    </div>	
								</div>
							<!--  </fieldset>-->
						   
	</div>
	</div>
	</s:if>
	</div></div></div>
	
</div>
</s:form>
<s:form id="dummyForm">
								<div style="display: none;" id="dateDiv" class="dataEnteringDiv">
											<div class="closeDiv" onclick="$j('#dateDiv').hide()" ><img src="images/cross.png" /></div>
											<table align="center">
												<tr align="center">
												<td>
													<s:text name="garuda.pendingorderpage.label.fromdt"/> <s:date name="dateStart" id="dateStart" format="M dd,yy" /><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateStart" id="dateStart"  placeholder='<s:text name="garuda.pendingorderpage.label.enterstartdate"/>' readonly="readonly" />
												</td>
												<td>
													<s:text name="garuda.pendingorderpage.label.todt"/> <s:date name="dateEnd" id="dateEnd" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateEnd" id="dateEnd" placeholder='<s:text name="garuda.pendingorderpage.label.enterenddate"/>' readonly="readonly" />
												</td>
												<td>
													<input  id="clear" type="button" onclick="fnclear('dateStart','dateEnd');" value='<s:text name="garuda.pendingorderpage.label.clear"/>' />
												</td>
												</tr>
												<tr align="center">
												<td colspan="3">
												<input  id="dateStartbt" type="button" onclick="$j('#dateDiv').hide();fn_FilterOrders();" value='<s:text name="garuda.pendingorderpage.label.filter"/>' /> 
												<input type="button" id="datebtclse" onclick="$j('#dateDiv').hide();" value='<s:text name="garuda.pendingorderpage.label.close"/>' />
												</td>
												</tr>
											</table>
										 
									</div>
										<div style="display: none;" id="noDiv" class="dataEnteringDiv">
										<div class="closeDiv" onclick="$j('#noDiv').hide()" ><img src="images/cross.png" /></div>
											<table align="center">
												<tr align="center">
												<td>
													<s:text name="garuda.landingpage.label.noofdaystofilter"/>
												</td>
												<td>
													<s:textfield name="noofdays" id="noOfdaystxt" placeholder="Enter No. Of Days" />
												</td>
												</tr>
												<tr align="center">
												<td colspan="3">
												<input  id="noFilterbt" type="button" onclick="$j('#noDiv').hide();fn_FilterOrders();" value='<s:text name="garuda.pendingorderpage.label.filter"/>' /> 
												<input  type="button" id="" onclick="$j('#noDiv').hide();" value='<s:text name="garuda.pendingorderpage.label.close"/>' />
												<input type="button" id="clear" onclick="$j('#noOfdaystxt').val('');" value='<s:text name="garuda.pendingorderpage.label.clear"/>' />
												</td>
												</tr>
											</table>
										 
									</div>
									  <div style="display: none;" id="dateDiv1" class="dataEnteringDiv">
									  								<div class="closeDiv" onclick="$j('#dateDiv1').hide()" ><img src="images/cross.png" /></div>
																	<table align="center">
																		<tr align="center">
																		<td>
																		<s:text name="garuda.pendingorderpage.label.fromdt"/> <s:date name="dateStart1" id="dateStart1" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateStart1" id="dateStart1" placeholder='<s:text name="garuda.pendingorderpage.label.enterstartdate"/>' readonly="readonly" />
																		</td>
																		<td>
																		<s:text name="garuda.pendingorderpage.label.todt"/> <s:date name="dateEnd1" id="dateEnd1" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateEnd1" id="dateEnd1" placeholder='<s:text name="garuda.pendingorderpage.label.enterenddate"/>' readonly="readonly" />
																		</td>
																		<td>
																		<input type="button" id="clear" onclick="fnclear('dateStart1','dateEnd1');fnDrawCord();" value='<s:text name="garuda.pendingorderpage.label.clear"/>' />
																		</td>
																		</tr>
																		<tr align="center">
																		<td colspan="3">
																		<input type="button" id="dateStartbt1" onclick="$j('#dateDiv1').hide();fn_FilterOthers();" value='<s:text name="garuda.pendingorderpage.label.filter"/>' /> 
																		<input type="button" id="closedt1" onclick="$j('#dateDiv1').hide()" value='<s:text name="garuda.pendingorderpage.label.close"/>' />
																		</td>
																		</tr>
																	</table>
															 </div>
															<div style="display: none;" id="noDiv1" class="dataEnteringDiv">
															<div class="closeDiv" onclick="$j('#noDiv1').hide()" ><img src="images/cross.png" /></div>
																	<table align="center">
																		<tr align="center">
																		<td>
																		<s:text name="garuda.landingpage.label.noofdaystofilter"/>
																		</td>
																		<td>
																		<s:textfield name="sipnoofdays" id="noOfdaystxt1" placeholder="Enter No. Of Days" />
																		</td>
																		</tr>
																		<tr align="center">
																		<td colspan="3">
																		<input type="button"  id="noFilterbt1" onclick="$j('#noDiv1').hide();fn_FilterOthers();" value='<s:text name="garuda.pendingorderpage.label.filter"/>' />
																		<input type="button" id="closebt1" onclick="$j('#noDiv1').hide()" value='<s:text name="garuda.pendingorderpage.label.close" />' />
																		<input type="button" id="clear" onclick="$j('#noOfdaystxt1').val('');" value='<s:text name="garuda.pendingorderpage.label.clear"/>' />
																		</td>
																		</tr>
																	</table>
															</div>
															<div style="display: none;" id="perCentageDiv"  class="dataEnteringDiv">
															<div class="closeDiv" onclick="$j('#perCentageDiv').hide()" ><img src="images/cross.png" /></div>
																	<table align="center">
																		<tr align="center">
																		<td>
																		<s:text name="garuda.pendingorderpage.label.percentage"/>
																		</td>
																		<td>
																		<s:textfield name="sippercent" id="sippercentval" placeholder="Enter Percentage"  />
																		</td>
																		</tr>
																		<tr align="center">
																		<td colspan="3">
																		<input  id="noPerFilterbt" type="button" onclick="$j('#perCentageDiv').hide();fn_FilterOthers();" value='<s:text name="garuda.pendingorderpage.label.filter"/>' /> 
																		<input type="button" id="closePercent" onclick="$j('#perCentageDiv').hide();" value='<s:text name="garuda.pendingorderpage.label.close"/>'  />
																		<input type="button" id="clear" onclick="$j('#sippercentval').val('');" value='<s:text name="garuda.pendingorderpage.label.clear"/>' />
																		</td>
																		</tr>
																	</table>
															</div>
															<div style="display: none;" id="customFilterDiv" class="centered" >
															<div class="closeDiv" onclick="$j('#customFilterDiv').hide();" ><img src="images/cross.png" /></div>
															<table>
																<tr align="center">
																<td>
																	<s:text name="garuda.pendingorderpage.label.createactfilter"/>
																</td>
																<td>
																	<s:textfield name="filtername" id="filtername" />
																</td>
																</tr>
																<tr align="center">
																<td colspan="2">
																<input  id="filterbt" type="button" onclick="calling();$j('#customFilterDiv').hide();"  value='<s:text name="garuda.pendingorderpage.label.Ok"/>' />
																<input type="button" id="filterclose" onclick="cancelCustomFilterOrd();"  value='<s:text name="garuda.pendingorderpage.label.cancel"/>' />
																</td>
																</tr>
															</table>
						   									</div>
						   									<div style="display: none;" id="customOFilterDiv" class="centered dateDivClass" >
						   									<div class="closeDiv" onclick="$j('#customOFilterDiv').hide()" ><img src="images/cross.png" /></div>
															<table>
																<tr align="center">
																<td>
																	<s:text name="garuda.pendingorderpage.label.createotherfilter"/>
																</td>
																<td>
																	<s:textfield name="filtername1" id="filtername1" />
																</td>
																</tr>
																<tr align="center">
																<td colspan="2">
																<input  id="filterbt1" type="button" onclick="callingOther();$j('#customOFilterDiv').hide();" value='<s:text name="garuda.pendingorderpage.label.Ok"/>'/>
																<input type="button" id="" onclick="cancelCustomFilter();" value='<s:text name="garuda.pendingorderpage.label.cancel"/>'/>
																</td>
																</tr>
															</table>
														 
						   </div>
</s:form>

<div id="usersitegrpsdropdown">
			<s:hidden value="#request.grpList.size()" />
	        <s:if test="#request.grpList.size()>1">
				<jsp:include page="pf_userSiteGroups.jsp"></jsp:include>
			</s:if>
</div>