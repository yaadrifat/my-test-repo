<%@ taglib prefix="s"  uri="/struts-tags"%>
<%
    String buttonid = request.getParameter("submitId");
	String invalid = "";
	String minimum = "";
	String pass = "";
	String esign = "";
	if(request.getParameter("invalid") != "" && request.getParameter("invalid") != null){
		invalid = request.getParameter("invalid");
	}else{
		invalid = "invalid";
	}
	if(request.getParameter("minimum") != "" && request.getParameter("minimum") != null){
		minimum = request.getParameter("minimum");
	}else{
		minimum = "minimum";
	}
	if(request.getParameter("pass") != "" && request.getParameter("pass") != null){
		pass = request.getParameter("pass");
	}else{
		pass = "pass";
	}
	if(request.getParameter("esign") != "" && request.getParameter("esign") != null){
		esign = request.getParameter("esign");
	}else{
		esign = "esign";
	}
	request.setAttribute("invalid",invalid);
	request.setAttribute("minimum",minimum);
	request.setAttribute("pass",pass);
    request.setAttribute("submitId",buttonid);
    request.setAttribute("esign",esign);

%>
<table width="98%" border="0" cellspacing="0" cellpadding="0" bgcolor="#cccccc">
	<tr valign="baseline" bgcolor="#cccccc">
	    <td>
		<span id="<s:property value="#request.invalid"/>" style="display: none;" class="validation-fail"><s:text name="garuda.common.esign.invalid"/></span>
		<span id="<s:property value="#request.minimum"/>" style="display: none;" class="validation-fail"><s:text name="garuda.common.esign.minimumLength"/></span>
		<span id="<s:property value="#request.pass"/>" style="display: none;" class="validation-pass"><s:text name="garuda.common.esign.valid"/></span>
		</td>
		<td nowrap="nowrap">
		<s:text name="garuda.esignature.lable.esign" /><span style="color:red;">*</span></td>
		<td><s:password name="userPojo.signature" id="%{#request.esign}" size="10" style="font-style:bold; color:#000000;" onkeyup="validateSign(this.value,'%{#request.submitId}','%{#request.invalid}','%{#request.minimum}','%{#request.pass}')" onkeypress="return validateScreen('%{#request.pass}',event);" cssStyle="width:100px"></s:password>
		</td>		
	</tr>
</table>