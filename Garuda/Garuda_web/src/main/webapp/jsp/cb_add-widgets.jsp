<%@taglib prefix="s" uri="/struts-tags" %>
<%@ page import="com.velos.ordercomponent.business.domain.Widget" %>
<%@ page import="com.velos.ordercomponent.business.domain.WidgetInstance" %>
<%@ page import="com.velos.ordercomponent.business.domain.User,java.util.List,java.util.ArrayList,java.util.Map,java.util.HashMap" %>

<% 
Map<Long,List<Widget>> widgetInfo = (Map<Long,List<Widget>>)application.getAttribute("widgetInfoMap");
Long pageid = Long.parseLong(request.getParameter("pageId"));
List<Widget> widgetList =  widgetInfo.get(pageid);
request.setAttribute("widgetlist",widgetList);
%>
<script>

function showWidget(){
	
		$j('input:checkbox:checked').each(function(){
		    var id = $j(this).val();
		    $j('#'+id).parent().css('display','block');
		});		
		jQuery("#modelPopup1").dialog("destroy");    
}

function closeAddWidget(){
	jQuery("#modelPopup1").dialog("destroy");
}
</script>
<div id="addWidget">
	<s:form id="widgetForm" method="post"><div id="widgetTble">
       <table border="0" align="center" cellpadding="0" cellspacing="0" class="display">
            <tr >
                    <th class="th1" scope="col" ><s:text name="garuda.widget.widgets" /></th>
                    <th class="th1" scope="col" ><s:text name="garuda.widget.select" /></th>
                    
           </tr>                
			<s:iterator value="#request.widgetlist">
			<tr>
			<td><s:property value="widgetName" /></td><td><input type="checkbox" name="widgets" value="<s:property value="widgetDivId" />" id="<s:property value="widgetDivId" />" /></td>
			</tr>
			
			</s:iterator>	         	
		<tr>
		    <td align="right"><button type="button" onclick="closeModal();"><s:text name="garuda.common.close" /></button></td>			
			<td ><button type="button" onclick="showWidget();"><s:text name="garuda.widget.add" /></button></td>
		</tr>
		</table></div>  </s:form>
		
</div>