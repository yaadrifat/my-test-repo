<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:useBean id ="paginateSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginationSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id="grpRights" scope="session" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="userLB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<script>
	var map = new Object(); 	

	function checkDup(k) {
	    return map[k];
	}

	<s:if test="#session.duplCords!=null && #session.duplCords.size()>0 && #request.paginateModule!=null && #request.paginateModule!=@com.velos.ordercomponent.util.VelosGarudaConstants@NMDP_CBU_QUICKSUMMARY_REPORT">
	   <s:iterator value="#session.duplCords" >	      
	         map[<s:property value="key"/>] = <s:property value="value"/>;	       
	   </s:iterator>
    </s:if>
    <s:if test="#session.orDuplCords!=null && #session.orDuplCords.size()>0">
	   <s:iterator value="#session.orDuplCords" >	      
	         map[<s:property value="key"/>] = <s:property value="value"/>;	       
	   </s:iterator>
   </s:if>
    
var ExpoCSV={
		
		
	
	 expoCSVData:function(){
		    var id1=$j('#tablelookup1_wrapper').find('.sorting_asc').attr('id');
			var id2=$j('#tablelookup1_wrapper').find('.sorting_desc').attr('id');
			var id="";


			if(id1!='null' && id1!='undefined' && $j.trim(id1)!=''){
				id=id1;
			}

			if(id2!='null' && id2!='undefined' && $j.trim(id2)!=''){
				id=id2;
			}	
	         window.location='pdfReportsForQueryBuilder?inelig_incomple_flag=<s:property value="#request.inelig_incomple_flag"/>&unlicensed_flag=<s:property value="#request.unlicensed_flag"/>&selectedSiteId=<s:property value="selectedSiteId"/>&ReportType=csv&paginateModule=<s:property value="paginateModule"/>&isMultipleCBBSelected=<s:property value="#request.isMultipleCBBSelected"/>&isWorkflowActivitySelected=<s:property value="#request.isWorkflowActivitySelected"/>'+'&sortCol='+id+'&sortTyp='+$j('#lookup_tbl_sort_type').val()+'&qinelig_flag='+$j('#v_qinelig_flag').val()+'&qincomp_flag='+$j('#v_qincomp_flag').val();
     },
     expoExcelData:function(){
    	 
    	var id1=$j('#tablelookup1_wrapper').find('.sorting_asc').attr('id');
 		var id2=$j('#tablelookup1_wrapper').find('.sorting_desc').attr('id');
 		var id="";


 		if(id1!='null' && id1!='undefined' && $j.trim(id1)!=''){
 			id=id1;
 		}

 		if(id2!='null' && id2!='undefined' && $j.trim(id2)!=''){
 			id=id2;
 		}	
         window.location='pdfReportsForQueryBuilder?inelig_incomple_flag=<s:property value="#request.inelig_incomple_flag"/>&unlicensed_flag=<s:property value="#request.unlicensed_flag"/>&selectedSiteId=<s:property value="selectedSiteId"/>&ReportType=excel&paginateModule=<s:property value="paginateModule"/>&isMultipleCBBSelected=<s:property value="#request.isMultipleCBBSelected"/>&isWorkflowActivitySelected=<s:property value="#request.isWorkflowActivitySelected"/>'+'&sortCol='+id+'&sortTyp='+$j('#lookup_tbl_sort_type').val()+'&qinelig_flag=<s:property value="#request.qinelig_flag"/>&qincomp_flag=<s:property value="#request.qincomp_flag"/>';
     }		
}

function f_modified(url,param1,param2){
	var id1=$j('#tablelookup1_wrapper').find('.sorting_asc').attr('id');
	var id2=$j('#tablelookup1_wrapper').find('.sorting_desc').attr('id');
	var id="";
	
	
	if(id1!='null' && id1!='undefined' && $j.trim(id1)!=''){
		id=id1;
	}
	
	if(id2!='null' && id2!='undefined' && $j.trim(id2)!=''){
		id=id2;
	}
	
	
	
	url=url+'&sortCol='+id+'&sortTyp='+$j('#lookup_tbl_sort_type').val();
	//alert(url);
	window.open(url,param1,param2); 
}

function loadAntigenReport(){	
    
	ProgBarController.stop();
	setSearchText();
}
var ProgBarController={
	show:function(){
	   showProgressOverlayMsg();
   },
   stop:function(){
	   closeProgressOverlayMsg();
   }
};

function loadFilteredHlaData(filterText){
	asyncLoadPageByGetRequset('getSimpleQueryPaginationData?filterTxt='+filterText+'&inelig_incomple_flag=<s:property value="#request.inelig_incomple_flag"/>&unlicensed_flag=<s:property value="#request.unlicensed_flag"/>&selSiteIdentifier=<s:property value="#request.selSiteIdentifier"/>&selectedSiteId=<s:property value="#request.selectedSiteId"/>&cbuCount=&iShowRows=10&iPageNo=0&paginateModule=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@NMDP_CBU_ANTIGEN_REPORT"/>&isMultipleCBBSelected='+$j("#isMultipleCBBSelected").val()+'&isWorkflowActivitySelected='+$j("#isWorkflowActivitySelected").val()+'&unlicensed_flag='+$j('#v_unlicensed_flag').val()+'&inelig_incomple_flag='+$j('#v_inelig_incomple_flag').val()+'&qinelig_flag='+$j('#v_qinelig_flag').val()+'&qincomp_flag='+$j('#v_qincomp_flag').val(),'searchTbleReport','<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@NMDP_CBU_ANTIGEN_REPORT"/>');
}

function setSearchText(){
	<s:if test="#request.filterTxt!=null && #request.filterTxt!=''">  
        $j("#lookup_search_txt").val('<s:property value="#request.filterTxt"/>');     
	</s:if>
	<s:else>
	    $j("#lookup_search_txt").val('');
	</s:else>
}


</script>

<table width="100%">
         <tr>
	  		<td align = "center">
	  		    <input type="hidden" name="entries" id="entries" value=<%=paginateSearch.getiShowRows() %> />
                <input type="hidden" name="tentries" id="tentries" value=<%=paginateSearch.getiTotalRows() %> />

					<s:if test = "%{#request.selSiteIdentifier.length()>0}">
						<strong><s:property value = "getCodeListDescById(getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@REPORT_CATEGORY,#request.paginateModule))"/> For CBB
						<s:property value="%{#request.selSiteIdentifier}"/></strong>
					</s:if>
					<s:else>
						<strong><s:property value = "getCodeListDescById(getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@REPORT_CATEGORY,#request.paginateModule))"/></strong>				
					</s:else>
			</td>		  		
		</tr>
</table>
<s:if test="#request.paginateModule!=null && #request.paginateModule==@com.velos.ordercomponent.util.VelosGarudaConstants@NMDP_CBU_ANTIGEN_REPORT">
  <input type="hidden" name="showsRow" value="10" />  
  <div align="right"><a href="#" onclick="loadFilteredHlaData('')"  id="resetSortLookup"><s:text name="garuda.pendingorderpage.label.resetsort"/></a></div><br>
   <table width="100%">
        <tr>
          <td align="left" width="25%"></td>
          <td align="right" width="75%">
              <button onclick="window.open('pdfReportsForQueryBuilder?selectedSiteId=<s:property value="selectedSiteId"/>&paginateModule=<s:property value="#request.paginateModule"/>&isMultipleCBBSelected=<s:property value="#request.isMultipleCBBSelected"/>&ReportType=pdf&isWorkflowActivitySelected=<s:property value="#request.isWorkflowActivitySelected"/>&repId=189&amp;repName=NMDP CBU Antigens Report&amp;selDate=&amp;params=','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');">Print as PDF</button>&nbsp;&nbsp;&nbsp;&nbsp;
              <button onclick="ExpoCSV.expoCSVData();">Export to CSV</button>&nbsp;&nbsp;&nbsp;&nbsp;
              <button onclick="ExpoCSV.expoExcelData();">Export to Excel</button>
           </td>
        </tr>
        <tr>
           <td align="left" width="25%">
                Show <select name="showsRow" id="showsRow" style="width:auto; height:25px;">
                     <option <s:if test="paginateSearch.getiShowRows()!=null && paginateSearch.getiShowRows()==10">selected="selected"</s:if> value="10">10</option>
                     <option <s:if test="paginateSearch.getiShowRows()!=null && paginateSearch.getiShowRows()==25">selected="selected"</s:if> value="25">25</option>
                     <option <s:if test="paginateSearch.getiShowRows()!=null && paginateSearch.getiShowRows()==50">selected="selected"</s:if> value="50">50</option>
                     </select> Entries
           </td>           
           <td align="right" width="75%">
              Search:<input type="text" name="filterTxt" id="filterTxt" <s:if test="#request.filterTxt!=null && #request.filterTxt!=''">value="<s:property value="#request.filterTxt"/>"</s:if> />
              <script>
              setSearchText();
              
               $j('#showsRow').change(function(){
       	          var selectedText = $j('#showsRow :selected').html();
       	        //  $j('#lookup_tbl_sentries').val(selectedText);
       	           //paginationFooter(1,"getSimpleQueryPaginationData,showsRow,inputs,Tableform,searchTbleReport,f_CBU_Sumary_Grid",'<s:property value="cbuCount"/>','<s:property value="paginateModule"/>');
       	           paginationFooter(1,'getSimpleQueryPaginationData,showsRow,inputs,Tableform,searchTbleReport,loadAntigenReport','<s:property value="cbuCount"/>','<s:property value="paginateModule"/>','<s:property value="%{#request.selSiteIdentifier}"/>');
       	       }); 

               $j('#filterTxt').change(function(){
        	          var selectedText = $j('#filterTxt').val();
        	          loadFilteredHlaData(selectedText);
        	       }); 
               </script>
           </td>
        </tr>
     </table>     
    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="displaycdr searchResults" id="searchResults">
		<thead>
		</thead>
		<tbody id="simple_reportTblTBody">		        
                <s:set name="cStatus" value="getCbuStatusByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@AC).getPkcbustatus().toString()"/>						           
		        <s:iterator value="cordInfoList" var="rowVal" status="row">	
		        <s:set name="cbuHlaKey" value="%{#rowVal[0]}" />	
		  		<tr>		  		
		  		</tr>
		          <tr>
		              <td>
		                  <table width="100%" cellpadding="0" cellspacing="0" border="1">
		              <s:if test="isMultipleCBBSelected == true">
		             <tr>
					   <td >
					      <strong><s:text name="garuda.queryBuilder.label.cbbid" /></strong>
					   </td>
					   <td align="left">
					      <s:property value="%{#rowVal[6]}"  />
					   </td>
					   <td colspan="2"></td>		
					</tr>
					</s:if>  	        
					 <tr>
					   <td >
					      <strong><s:text name="garuda.cbuentry.label.registrycbuid" /></strong>
					   </td>
					   <td align="left">
					      <s:property value="%{#rowVal[1]}"  />
					   </td>
					   <td >
					      <strong><s:text name="garuda.cbuentry.label.registrymaternalid" /></strong>
					   </td>
					   <td align="left">
					      <s:property value="%{#rowVal[2]}"  />
					   </td>
					</tr>
					<tr>
					   <td >
					      <strong><s:text name="garuda.cbuentry.label.localcbuid" /></strong>
					   </td>
					   <td align="left">
					      <s:property value="%{#rowVal[3]}"  />
					   </td>
					   <td >
					      <strong><s:text name="garuda.cbuentry.label.maternallocalid" /></strong>
					   </td>
					   <td align="left">
					      <s:property value="%{#rowVal[4]}"  />
					   </td>
					</tr>
					<tr>
					   <td >
					      <strong><s:text name="garuda.cbuentry.label.idbag" /></strong>
					   </td>
					   <td align="left" >
					      <s:property value="%{#rowVal[8]}"  />
					   </td>
					    <td >
					      <strong><s:text name="garuda.querybuilderreport.label.activeforpatient" /></strong>
					   </td>
					   <td align="left" >
					       <s:set name="cordStatus" value="%{#rowVal[11].toString()}"/>						  
						   <!--<s:if test="%{#cordStatus==#cStatus}">
						      <s:property value="%{#rowVal[7]}"  />	
						   </s:if>			
						   	   --><s:property value="%{#rowVal[7]}"  />	   
					   </td>
					</tr>
					<tr>
					   <td >
					      <strong><s:text name="garuda.idsReport.isbt" /></strong>
					   </td>
					   <td align="left">
					      <s:property value="%{#rowVal[5]}"  />
					   </td>
					   <td >
					      <strong><s:text name="garuda.queryBuilder.label.cbucollectiondate" /></strong>
					   </td>
					   <td align="left">
					      <s:property value="%{#rowVal[10]}"  />
					   </td>
					</tr>
					<tr>
					   <td >
					      <strong><s:text name="garuda.querybuilderreport.label.historiclocalid" /></strong>
					   </td>
					   <td align="left">
					     <s:property value="%{#rowVal[9]}"  />
					   </td>
					   <td colspan="2"></td>
  	

					</tr>	
					<tr>
					   <td colspan="4">
					      <!--<s:iterator value="#request.hlas" >
					         <s:set name="cbuHlaMapKey" value="key" />
					             <s:if test="#cbuHlaMapKey==#cbuHlaKey">
					                <s:set name="cbuHlaKey" value="key"/>					                
					             </s:if>
					      </s:iterator>
					     --><table align="left" cellpadding="0" cellspacing="0" border="0" class="displaycdr" >
						 <thead>
						   <tr>
						      <th width="28%"></th>			   			   
							   <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
							       <th width="8%">
							          <s:property value="description.replace('HLA-','')"/>
							       </th>				     
							   </s:iterator>
						   </tr>
						 </thead>			 
						 <tbody>
						    <tr>
							    <td width="28%">
									 <s:text name="garuda.cbu.label.besthla"/>								            
								</td>								    
								  <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
							            <s:iterator value="#request.bestHlasMap[#cbuHlaKey.longValue()]" var="bhla">
									             <s:if test="pkCodeId==fkHlaCodeId">
									                 <s:set name="bhlaCheck" value="%{'true'}" />
										             <td width="8%">
											            <table>
										                    <tr>
										                        <td>
										                            <s:property value="hlaType1" />
										                        </td>
										                    </tr>
										                     <tr>
										                        <td>
										                            <s:property value="hlaType2" />
										                        </td>
										                    </tr>
										                </table>	  
												     </td>	
											      </s:if>				      										      	        	
									        </s:iterator> 
									        <s:if test="%{#bhlaCheck!='true'}">
											         <td width="8%"></td>
											</s:if>	
											<s:set name="bhlaCheck" value="%{'false'}" />
								  </s:iterator>
							</tr>
						   </tbody>
						   <tfoot>
							    <tr>
									<td colspan="13"></td>
								</tr>
							</tfoot>
						</table>
						<table width="100%" style="padding-top: 50px;">
						      <tr>
						         <td ></td>
						      </tr>
						</table>
					       <table align="left" cellpadding="0" cellspacing="0" border="0" class="displaycdr datatablewithoutsearch" id="searchResults10">
							 <thead>
							   <tr>
							      <th width="10%"><s:text name="garuda.cbuentry.label.typingdate"/></th>	
							      <th width="9%"><s:text name="garuda.queryBuilder.label.recieveddate"/></th>
							      <th width="9%"><s:text name="garuda.cbuentry.label.source"/></th>		   			   
								   <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
								       <th width="8%">
								          <s:property value="description.replace('HLA-','')"/>
								       </th>				     
								   </s:iterator>
							   </tr>
							 </thead>			 
							 <tbody>
							 <s:iterator value="#request.hlas[#cbuHlaKey]">		
							      <s:set name="cbuHlaSequenceMapKey" value="key" />				    						        
							      <tr>				
							        <td width="10%">
							            <s:if test="#request.hlas[#cbuHlaKey][#cbuHlaSequenceMapKey][0].hlaTypingDate !=null">
							                <s:set name="cbuhlatypedate" value="#request.hlas[#cbuHlaKey][#cbuHlaSequenceMapKey][0].hlaTypingDate" />
									        <s:date name="cbuhlatypedate" id="cbuhlatypedate" format="MMM dd, yyyy" />
									        <s:property value="%{#request.cbuhlatypedate}"/>									       
							            </s:if>									        
							        </td>
							        <td width="9%">
							            <s:if test="#request.hlas[#cbuHlaKey][#cbuHlaSequenceMapKey][0].hlaRecievedDate !=null">
									        <s:set name="cbuhlarecievedate" value="#request.hlas[#cbuHlaKey][#cbuHlaSequenceMapKey][0].hlaRecievedDate" />
									        <s:date name="cbuhlarecievedate" id="cbuhlarecievedate" format="MMM dd, yyyy" />
									        <s:property value="%{#request.cbuhlarecievedate}"/>									        
									    </s:if>
							        </td>
							        <td width="9%">
							           <!--<s:if test="#request.cbuHlaMap[#cbuHlaKey][0].fkSource !=null && #request.cbuHlaMap[#cbuHlaKey][0].fkSource!=''">
							               <s:set name="cbuhlasource" value="#request.cbuHlaMap[#cbuHlaKey][0].fkSource" />
								           <s:property value="getCodeListDescById(#cbuhlasource)"/>
								       </s:if>
								       -->
								       
								   <s:if test="#request.hlas[#cbuHlaKey][#cbuHlaSequenceMapKey][0].lastModifiedByUser !=null && #request.hlas[#cbuHlaKey][#cbuHlaSequenceMapKey][0].lastModifiedByUser!=''">
                     				
                  					 <s:property value="#request.hlas[#cbuHlaKey][#cbuHlaSequenceMapKey][0].lastModifiedByUser"/>
            					   </s:if>
								       
								       <s:elseif test="#request.hlas[#cbuHlaKey][#cbuHlaSequenceMapKey][0].getuName() !=null && #request.hlas[#cbuHlaKey][#cbuHlaSequenceMapKey][0].getuName()!=''">
							               
								           <s:property value="#request.hlas[#cbuHlaKey][#cbuHlaSequenceMapKey][0].getuName()"/>
								       </s:elseif>
							        </td>
							        <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">					       
								            <s:iterator value="#request.hlas[#cbuHlaKey.longValue()][#cbuHlaSequenceMapKey]" var="bhla">
									             <s:if test="pkCodeId==fkHlaCodeId">
									                 <s:set name="hlaCheck" value="%{'true'}" />
										             <td width="8%">
											            <table>
										                    <tr>
										                        <td>
										                            <s:property value="hlaType1" />
										                        </td>
										                    </tr>
										                     <tr>
										                        <td>
										                            <s:property value="hlaType2" />
										                        </td>
										                    </tr>
										                </table>	  
												     </td>	
											      </s:if>				      										      	        	
									        </s:iterator> 
									        <s:if test="%{#hlaCheck!='true'}">
											         <td width="8%"></td>
											</s:if>	
											<s:set name="hlaCheck" value="%{'false'}" />	    								          
									 </s:iterator> 
									 </tr>	
							  </s:iterator>							 
						   </tbody>
						   <tfoot>
							    <tr>
									<td colspan="13"></td>
								</tr>
						   </tfoot>
			             </table>
					  </td>
				</tr>
				</table>				
		              </td>
		          </tr>
		         <tr>
		            <td >
		                <table width="100%" style="background-color: black;" cellpadding="0" cellspacing="0">
		                   <tr>
		                      <td></td>
		                   </tr>
		                </table>
		            </td>
		         </tr>			
			</s:iterator>
		</tbody>
		<tfoot>
			<tr><td colspan="8"></td></tr>			
			<tr>
			    <td colspan="3">
		              <jsp:include page="paginationFooter.jsp">
					  	<jsp:param value="searchTbleReport" name="divName"/>
					  	<jsp:param value="Tableform" name="formName"/>
					  	<jsp:param value="showsRow" name="showsRecordId"/>
					  	<jsp:param value="getSimpleQueryPaginationData" name="url"/>
					  	<jsp:param value="inputs" name="cbuid"/>
					  	<jsp:param value="workflow" name="paginateSearch"/>
					    <jsp:param value="1" name="idparam"/>
					  	<jsp:param value="loadAntigenReport" name="bodyOnloadFn"/>
				    </jsp:include>
               </td>
               <td colspan="5"></td>
            </tr>		
		</tfoot>	
     </table>
     <table width="100%">
        <tr>           
           <td colspan="2">                 
           </td>           
           <td align="right" colspan="2">
              <button onclick="window.open('pdfReportsForQueryBuilder?selectedSiteId=<s:property value="selectedSiteId"/>&paginateModule=<s:property value="#request.paginateModule"/>&isMultipleCBBSelected=<s:property value="#request.isMultipleCBBSelected"/>&ReportType=pdf&isWorkflowActivitySelected=<s:property value="#request.isWorkflowActivitySelected"/>&repId=189&amp;repName=NMDP CBU Antigens Report&amp;selDate=&amp;params=','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');">Print as PDF</button>&nbsp;&nbsp;&nbsp;&nbsp;
              <button onclick="ExpoCSV.expoCSVData();">Export to CSV</button>&nbsp;&nbsp;&nbsp;&nbsp;
              <button onclick="ExpoCSV.expoExcelData();">Export to Excel</button>
           </td>
        </tr>
     </table>     
     <script>
      $j('#query_builder_tbl').val('cbuAntigen');
     </script>
  </s:if>
  <s:elseif test="#request.paginateModule!=null && #request.paginateModule==@com.velos.ordercomponent.util.VelosGarudaConstants@NMDP_CBU_IDSUMMARY_REPORT">
  <div align="right"><a href="#" onclick="fn_ResetLookupSort()"  id="resetSortLookup"><s:text name="garuda.pendingorderpage.label.resetsort"/></a></div><br>
     
          <table width="100%">
        		<tr>
           			<td colspan="2">           
           			</td>
           			<td align="right" colspan="2">
             			 <button onclick="f_modified('pdfReportsForQueryBuilder?selectedSiteId=<s:property value="selectedSiteId"/>&paginateModule=<s:property value="#request.paginateModule"/>&isMultipleCBBSelected=<s:property value="#request.isMultipleCBBSelected"/>&ReportType=pdf&isWorkflowActivitySelected=<s:property value="#request.isWorkflowActivitySelected"/>','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');" >Print as PDF</button>&nbsp;&nbsp;&nbsp;&nbsp;
             			 <button onclick="ExpoCSV.expoCSVData();">Export to CSV</button>&nbsp;&nbsp;&nbsp;&nbsp;
              			  <button onclick="ExpoCSV.expoExcelData();">Export to Excel</button>
           			</td>
       			 </tr>
     	</table> 
     
      <table width="100%" cellpadding="0" cellspacing="0" border="0" class="displaycdr searchResults" id="tablelookup1">
		 <thead>		 
			<tr id="lookupTblThead">
			    <s:if test="isMultipleCBBSelected == true">
				   <th id="cbbid"><s:text name="garuda.queryBuilder.label.cbbid" /></th>
				</s:if>
				<th id="regid"><s:text name="garuda.cbuentry.label.registrycbuid" /></th>
				<th id="loccbuid"><s:text name="garuda.cbuentry.label.localcbuid" /></th>
				<th id="idonbag"><s:text name="garuda.cdrcbuview.label.id_on_bag" /></th>
				<th id="isbt"><s:text name="garuda.shippedcbu.report.label.isbt" /></th>
				<th id="histcbuid"><s:text name="garuda.cdrcbuview.label.historic_cbu_lcl_id"/></th>
				<th id="matregid"><s:text name="garuda.cbuentry.label.registrymaternalid" /></th>
				<th id="matlocid"><s:text name="garuda.cbuentry.label.localmaternalid" /></th>
				<!--<s:if test="isWorkflowActivitySelected==true">
				   <th id="workflowCbuId"><s:text name="garuda.shippedcbu.report.label.wactivity"/></th>
				</s:if>
				<th id="ctshipdate"><s:text name="garuda.idsReport.ctshipdate"/></th>
				<th id="orshipdate"><s:text name="garuda.idsReport.orshipdate"/></th>
				<s:if test="isWorkflowActivitySelected==true">
				   <th id="reqdate"><s:text name="garuda.idsReport.reqdate"/></th>
				</s:if>
			--></tr>

		</thead>
		<tbody id="tableBody">
		      <s:iterator value="cordInfoList" var="rowVal" status="row">	
					<tr id="<s:property	value="%{#row.index}"/>" class="hotspot" >
					    <s:if test="isMultipleCBBSelected==true">
						  <td><s:property value="%{#rowVal[1]}"  /></td>
						</s:if>
						<td ><s:property value="%{#rowVal[2]}"  /></td>						
						<td><s:property value="%{#rowVal[3]}"  /></td>
						<td><s:property value="%{#rowVal[4]}"  /></td>
						<td><s:property value="%{#rowVal[5]}"  /></td>
						<td><s:property value="%{#rowVal[6]}"  /></td>						  
						<td><s:property value="%{#rowVal[7]}"  /></td>
						<td><s:property value="%{#rowVal[8]}"  /></td>
						<!--<s:if test="isWorkflowActivitySelected==true">
						 <td><s:property value="%{#rowVal[9]}"  /></td>
						</s:if>
						<td>
						    <s:if test="getCodeListDescById(%{#rowVal[12]})==@com.velos.ordercomponent.util.VelosGarudaConstants@CT">
						       <s:property value="%{#rowVal[10]}"  />
						    </s:if>
						</td>
						<td>
						    <s:if test="getCodeListDescById(%{#rowVal[12]})==@com.velos.ordercomponent.util.VelosGarudaConstants@OR">
						       <s:property value="%{#rowVal[10]}"  />
						    </s:if>
						</td>
						<s:if test="isWorkflowActivitySelected==true">
						  <td  ><s:property value="%{#rowVal[11]}"  /></td>
						  </s:if>
					--></tr>
			</s:iterator>
		</tbody>
		<tfoot>
			<tr><td colspan="8">
		              <jsp:include page="paginationFooter.jsp">
					  	<jsp:param value="searchTbleReport" name="divName"/>
					  	<jsp:param value="Tableform" name="formName"/>
					  	<jsp:param value="showsRow" name="showsRecordId"/>
					  	<jsp:param value="getSimpleQueryPaginationData" name="url"/>
					  	<jsp:param value="inputs" name="cbuid"/>
					  	<jsp:param value="workflow" name="paginateSearch"/>
					    <jsp:param value="1" name="idparam"/>
					  	<jsp:param value="f_CBU_Sumary_Grid" name="bodyOnloadFn"/>
				    </jsp:include>				    
               </td>
            </tr>
		</tfoot>	
     </table>
     <table width="100%">
        <tr>
           <td colspan="2">           
           </td>
           <td align="right" colspan="2">
              <button onclick="f_modified('pdfReportsForQueryBuilder?selectedSiteId=<s:property value="selectedSiteId"/>&paginateModule=<s:property value="#request.paginateModule"/>&isMultipleCBBSelected=<s:property value="#request.isMultipleCBBSelected"/>&ReportType=pdf&isWorkflowActivitySelected=<s:property value="#request.isWorkflowActivitySelected"/>&repId=188&amp;repName=Detail CBU Report&amp;selDate=&amp;params=','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');" >Print as PDF</button>&nbsp;&nbsp;&nbsp;&nbsp;
              <button onclick="ExpoCSV.expoCSVData();">Export to CSV</button>&nbsp;&nbsp;&nbsp;&nbsp;
              <button onclick="ExpoCSV.expoExcelData();">Export to Excel</button>
           </td>
        </tr>
     </table>     
     <script>
     var i=0;
     function f_CBU_Sumary_Grid(){ 		
    	   
    	    
    		var showEntriesTxt='Show <select name="showsRow" id="showsRow" style="width:auto; height:25px;"><option value="10">10</option><option value="25">25</option><option value="50">50</option></select> Entries';
    		var is_all_entries=$j('#showsRow').val();
    		var pageNo=$j('#paginateWrapper1').find('.currentPage').text();
    		var showEntries=$j('#lookup_tbl_sentries').val();
    		
    		var totalEntries=$j('#tentries').val();
    		var sort=0;
    		var sortDir="asc";
    		var SortParm=$j('#lookup_tbl_sort_col').val();
    		var seachTxt="";
    		var preTxt=$j('#lookup_search_txt').val();
    		var otable;
    		
    		if((preTxt!=null && $j.trim(preTxt)!="")){
    			seachTxt=preTxt;
    		}
    		 $j('#query_builder_tbl').val('cbuSummary');

    		if(SortParm!=undefined && SortParm!=null && SortParm!=""){
    			sort=$j('#lookup_tbl_sort_col').val();
    			sortDir=$j('#lookup_tbl_sort_type').val();
    		}	
    		
    		if((showEntries==null || showEntries==undefined || showEntries=="undefined" || $j.trim(showEntries)=="")){
    			is_all_entries='10';
    			$j('#lookup_tbl_sentries').val(is_all_entries);
    			showEntries=$j('#lookup_tbl_sentries').val();
    		}
    		
    		
    		if(showEntries=='All'){
    			
    			 otable =  $j('#tablelookup1').dataTable({
    				"sScrollY": "100px", 
    		        "sDom": "frtiS",
    		        "sAjaxSource": 'getJsonPagination.action?TblFind=lookup&currentpage=cbuSummary&allEntries=ALL&paginateModule=<s:property value="paginateModule"/>&cbuCount=<s:property value="cbuCount"/>&cbuid='+$j("#inputs").val(),
    		        "fnServerData": function ( sSource, aoData, fnCallback ) {
    		        	xhr= $j.getJSON( sSource, aoData, function (json) { 
    		                fnCallback(json);
    		               // ProgBarController.stop();
    		            } );
    		        },"fnInitComplete": function(oSettings, json) {
    		        	ProgBarController.stop();
   		        	 //console.warn( 'DataTables has finished its initialisation.' );
   		            },
    		        "bServerSide": true,
    		       // "bProcessing": true,
    		        "bRetrieve" : true,
    		        "bDestroy": true,
    		        "aaSorting": [[ sort, sortDir ]],
    		        "oSearch": {"sSearch": seachTxt},
    		        "bDeferRender": true,
    		        "bAutoWidth": false,
    		        "sAjaxDataProp": "aaData",
    		        "oLanguage": {
    		        	"sEmptyTable": "<s:text name="garuda.common.message.noMatchingRecords"/>"
    				},
    		        "aoColumnDefs": [   		          /*CBB ID*/
    		     					    		         <s:if test="isMultipleCBBSelected==true">
    		        		                             {    		                        	  
    		        		                              "aTargets": [i++],"iDataSort": 0,"mDataProp": function ( source, type, val ) {
    		        				                        	var str="";
    		        			                          	    if(source[1]!='null' && source[1]!=null){
    		        			                          	    str = "<span><div>"+source[1]+"</div></span>";
    		        			                          	    } 
    		        			                              	return str;
    		        		                             }},
    		        		                             </s:if>
    		        		                            {
    		        		                            	/*CBU Registry ID*/
    		        		                                    "aTargets": [i++],"iDataSort": 1, "mDataProp": function ( source, type, val ) {
    		        		                                     var str="";
    		        		                                     if(source[2]!='null' && source[2]!=null){        		        		                                     	 
    		        		                                    	 //str = checkDup(source[0])!=undefined ? "<span class='duplicate'><div>"+source[2]+"</div></span>" : "<span><div>"+source[2]+"</div></span>"; 
    		        		                                    	 str = "<span><div>"+source[2]+"</div></span>";
    		        		                                     }        		        		                                         
    		        		                                     return str;
    		        		                             }},
    		        		                          	{
    		        		                            	 /*CBU Local ID*/
    		        		                                 "aTargets": [i++],"iDataSort": 2, "mDataProp": function ( source, type, val ) {
    		        		                                	var str="";
    		        		                                	if(source[4]!='null' && source[4]!=null){
    		        		                                		str="<span><div>"+source[4]+"</div></span>";	
    		        		                                	}
    		        		                                    return str;
    		        		                             }},
    		        		                             {
    		        		                            	 /*Unique Product Identity on Bag*/
    		        		                                 "aTargets": [i++],"iDataSort": 3, "mDataProp": function ( source, type, val ) {
    		        		                                	var str="";
    		        		                                	if(source[7]!='null' && source[7]!=null)
    		        		                                	str="<span><div>"+source[7]+"</div></span>"; 
    		        		                                    return str;
    		        		                             }},
    		        		                             {
    		        		                            	 /*ISBT */
    		        		                                 "aTargets": [i++],"iDataSort": 4, "mDataProp": function ( source, type, val ) {
    		        		                                	var str="";
    		        		                                	if(source[6]!='null' && source[6]!=null)
    		        		                                	str="<span><div>"+source[6]+"</div></span>"; 
    		        		                                    return str;
    		        		                             }},    		                             
    		        		                             {
    		        		                            	 /*Historic Local CBU ID */
    		        		                                 "aTargets": [i++],"iDataSort": 5, "mDataProp": function ( source, type, val ) {
    		        		                                	var str="";
    		        		                                	if(source[8]!='null' && source[8]!=null)
    		        		                                	str="<span><div>"+source[8]+"</div></span>"; 
    		        		                                    return str;
    		        		                             }},    		                             
    		        		                             {
    		        		                            	 /*Maternal Registry ID*/
    		        		                                 "aTargets": [i++],"iDataSort": 6, "mDataProp": function ( source, type, val ) {
    		        		                                	var str="";
    		        		                                	if(source[3]!='null' && source[3]!=null)
    		        		                                	str="<span><div>"+source[3]+"</div></span>"; 
    		        		                                    return str;
    		        		                             }},
    		        		                             {
    		        		                            	 /*Maternal Local ID*/
    		        		                                 "aTargets": [i++],"iDataSort": 7, "mDataProp": function ( source, type, val ) {
    		        		                                	var str="";
    		        		                                	if(source[5]!='null' && source[5]!=null)
    		        		                                	str="<span><div>"+source[5]+"</div></span>"; 
    		        		                                    return str;
    		        		                             }}                  
    		                             
    		                             
    		                        ],
    		                        "fnDrawCallback": function() {
    		                            $j('#tablelookup1_wrapper').find('.dataTables_scrollFoot').hide();
    		                            $j('#tablelookup1_info').show();
    		                            $j("#showsRow option:contains('All')").attr('selected', 'selected');
    		                            $j('#tablelookup1_wrapper').find('.dataTables_scrollHeadInner').find('table').css('position','');
    		                            //var inputs = $j('#inputs').val();	                        	
    		                        	//$j('.lookupmenu :text').removeAttr("disabled");
    		                        	setSortData('lookupTblThead','lookup_tbl_sort_col','lookup_tbl_sort_type');
    		                        	$j(".duplicate").each(function(){
   		                 	    	  	  $j(this).parent().css("background-color","red");	
   		                 	    	    });
    		                        }
    		    });
    			$j('#tablelookup1_filter').before(showEntriesTxt); 
    			
    		}else{
    			otable =  $j('#tablelookup1').dataTable({
    		        "sAjaxSource": 'getJsonPagination.action?TblFind=lookup&allEntries=ALL&currentpage=cbuSummary&paginateModule=<s:property value="paginateModule"/>&cbuCount=<s:property value="cbuCount"/>&iDisplayStart=0&iDisplayLength='+is_all_entries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries,
    		        "fnServerData": function ( sSource, aoData, fnCallback ) {
    		        	xhr=$j.getJSON( sSource, aoData, function (json) { 
    		                fnCallback(json);
    		               // ProgBarController.stop(); 		               
    		            } );    		        	
    		        },"fnInitComplete": function(oSettings, json) {
    		        	ProgBarController.stop();
   		        	 //console.warn( 'DataTables has finished its initialisation.' );
   		            },
    		        "bServerSide": true,
    		       // "bProcessing": true,
    		        "bRetrieve" : true,
    		        "bDestroy": true,
    		        "aaSorting": [[ sort, sortDir ]],
    		        "oSearch": {"sSearch": seachTxt},
    		        "bDeferRender": true,
    		        "bAutoWidth": false,
    		        "sAjaxDataProp": "aaData",
    		        "oLanguage": {
    		        	"sEmptyTable": "<s:text name="garuda.common.message.noMatchingRecords"/>"
    				},
					    		        "aoColumnDefs": [
					    		          /*CBB ID*/
					    		         <s:if test="isMultipleCBBSelected==true">
    		                             {    		                        	  
    		                              "aTargets": [i++],"iDataSort": 0,"mDataProp": function ( source, type, val ) {
    				                        	var str="";
    			                          	    if(source[1]!='null' && source[1]!=null){
    			                          	    str = "<span><div>"+source[1]+"</div></span>";
    			                          	    } 
    			                              	return str;
    		                             }},
    		                             </s:if>
    		                            {
    		                            	/*CBU Registry ID*/
    		                                    "aTargets": [i++],"iDataSort": 1, "mDataProp": function ( source, type, val ) {
    		                                     var str="";
    		                                     if(source[2]!='null' && source[2]!=null){
    		                                    	 //str = checkDup(source[0])!=undefined ? "<span class='duplicate'><div>"+source[2]+"</div></span>" : "<span><div>"+source[2]+"</div></span>";
    		                                    	 str = "<span><div>"+source[2]+"</div></span>";
    		                                     } 
    		                                     return str;
    		                             }},
    		                          	{
    		                            	 /*CBU Local ID*/
    		                                 "aTargets": [i++],"iDataSort": 2, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[4]!='null' && source[4]!=null){
    		                                		str="<span><div>"+source[4]+"</div></span>";	
    		                                	}
    		                                    return str;
    		                             }},
    		                             {
    		                            	 /*Unique Product Identity on Bag*/
    		                                 "aTargets": [i++],"iDataSort": 3, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[7]!='null' && source[7]!=null)
    		                                	str="<span><div>"+source[7]+"</div></span>"; 
    		                                    return str;
    		                             }},
    		                             {
    		                            	 /*ISBT */
    		                                 "aTargets": [i++],"iDataSort": 4, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[6]!='null' && source[6]!=null)
    		                                	str="<span><div>"+source[6]+"</div></span>"; 
    		                                    return str;
    		                             }},    		                             
    		                             {
    		                            	 /*Historic Local CBU ID */
    		                                 "aTargets": [i++],"iDataSort": 5, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[8]!='null' && source[8]!=null)
    		                                	str="<span><div>"+source[8]+"</div></span>"; 
    		                                    return str;
    		                             }},    		                             
    		                             {
    		                            	 /*Maternal Registry ID*/
    		                                 "aTargets": [i++],"iDataSort": 6, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[3]!='null' && source[3]!=null)
    		                                	str="<span><div>"+source[3]+"</div></span>"; 
    		                                    return str;
    		                             }},
    		                             {
    		                            	 /*Maternal Local ID*/
    		                                 "aTargets": [i++],"iDataSort": 7, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[5]!='null' && source[5]!=null)
    		                                	str="<span><div>"+source[5]+"</div></span>"; 
    		                                    return str;
    		                             }}
    		                        ],
    		                        "fnDrawCallback": function() {
    		                            $j('#tablelookup1_wrapper').find('.dataTables_scrollHeadInner').find('table').css('position','');
    		                            //$j('.lookupmenu :text').removeAttr("disabled");
    		                        	setSortData('lookupTblThead','lookup_tbl_sort_col','lookup_tbl_sort_type');
    		                        	$j('#tablelookup1_info').hide();
    		                 			$j('#tablelookup1_paginate').hide();
    		                 			$j(".duplicate").each(function(){
    		                 	    	  	  $j(this).parent().css("background-color","red");	
    		                 	    	    });
    		                        }
    		    });
    			$j('#tablelookup1_length').replaceWith(showEntriesTxt);
    			
    		} 
    	    
    	    $j('#showsRow').change(function(){
    	        var selectedText = $j('#showsRow :selected').html();
    	        //alert("value:::::::::::::::::"+selectedText)
    	        $j('#lookup_tbl_sentries').val(selectedText);
    	        paginationFooter(0,"getSimpleQueryPaginationData,showsRow,inputs,Tableform,searchTbleReport,f_CBU_Sumary_Grid",'<s:property value="cbuCount"/>','<s:property value="paginateModule"/>','<s:property value="%{#request.selSiteIdentifier}"/>');
    	    }); 
    	    
    	    $j('#tablelookup1_filter').find(".searching1").keyup(function(e){
    			  var val = $j(this).val();
    			  val=$j.trim(val);
    			  $j('#lookup_search_txt').val(val);
    			  if(e.keyCode==13)
    			  {	  showProgressOverlayMsg();
				      setTimeout('idSummaryPagination()', 500);
    				 
    			  }
    		});    		   	    
    	}
     function fn_ResetLookupSort(){
    	 if($j('#lookup_tbl_sort_col').val()!=undefined && $j('#lookup_tbl_sort_col').val()!=null && $j('#lookup_tbl_sort_col').val()!=""){
    			
    			$j('#lookup_tbl_sort_col').val('');
    			$j('#lookup_tbl_sort_type').val('');
    			var oTable = $j('#tablelookup1').dataTable();
    			oTable.fnSort( [ [0,'asc'] ] );
    			oTable.fnDraw();
    		}
         }
     function idSummaryPagination(){
		  paginationFooter(0,"getSimpleQueryPaginationData,showsRow,inputs,Tableform,searchTbleReport,f_CBU_Sumary_Grid","0",'<s:property value="paginateModule"/>','<s:property value="%{#request.selSiteIdentifier}"/>');
	  }
     </script>
  </s:elseif> 
  <s:elseif test="#request.paginateModule!=null && #request.paginateModule==@com.velos.ordercomponent.util.VelosGarudaConstants@NMDP_CBU_LICENSEANDELIGIBILTY_REPORT">
  <div align="right"><a href="#" onclick="fn_ResetLookupSort()"  id="resetSortLookup"><s:text name="garuda.pendingorderpage.label.resetsort"/></a></div><br>
    
    <table width="100%">
        <tr>
           <td colspan="2">           
           </td>
           <td align="right" colspan="2">
              <button onclick="f_modified('pdfReportsForQueryBuilder?selectedSiteId=<s:property value="selectedSiteId"/>&paginateModule=<s:property value="#request.paginateModule"/>&isMultipleCBBSelected=<s:property value="#request.isMultipleCBBSelected"/>&ReportType=pdf&isWorkflowActivitySelected=<s:property value="#request.isWorkflowActivitySelected"/>&repId=190&amp;repName=NMDP CBU Licensure and Eligibility Deteremination Report&amp;selDate=&amp;params='+'&unlicensed_flag=<s:property value="#request.unlicensed_flag"/>&qinelig_flag=<s:property value="qinelig_flag"/>&qincomp_flag=<s:property value="qincomp_flag"/>','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');">Print as PDF</button>&nbsp;&nbsp;&nbsp;&nbsp;
              <button onclick="ExpoCSV.expoCSVData();">Export to CSV</button>&nbsp;&nbsp;&nbsp;&nbsp;
              <button onclick="ExpoCSV.expoExcelData();">Export to Excel</button>
           </td>
        </tr>
     </table>   
    
      <table width="100%" cellpadding="0" cellspacing="0" border="0" class="displaycdr searchResults" id="tablelookup1">
		 <thead>
			<tr id="lookupTblThead">
				<s:if test="isMultipleCBBSelected==true">
				<th id="cbbid"><s:text name="garuda.queryBuilder.label.cbbid" /></th>
				</s:if>
				<th id="regid"><s:text name="garuda.cbuentry.label.registrycbuid" /></th>
				<th id="loccbuid"><s:text name="garuda.cbuentry.label.localcbuid" /></th>
				<th id="idonbag"><s:text name="garuda.cbuentry.label.idbag" /></th>
				<th id="cbucollectdt"><s:text name="garuda.queryBuilder.label.cbucollectiondate" /></th>
				<th id="regdt"><s:text name="garuda.queryBuilder.nmdpregistrationDates"/></th>
				<th id="cbustat"><s:text name="garuda.cbuentry.label.cbu_status"/></th>
				<th id="reason"><s:text name="garuda.clinicalnote.label.reason"/></th>
				<th id="licstat"><s:text name="garuda.cdrcbuview.label.licensure_status"/></th>
				<th id="unlicreas"><s:text name="garuda.evaluHistory.report.unlicensedReason"/></th>
				<th id="eligstat"><s:text name="garuda.cdrcbuview.label.eligibility_determination"/></th>
				<th id="inelireason"><s:text name="garuda.queryBuilder.ineligiblereason"/></th>
				<th id="incomreason"><s:text name="garuda.queryBuilder.incompletereason"/></th>
				<%-- <th><s:text name="garuda.queryBuilder.incompletereason"/></th> --%>
				<th id="babybirdt"><s:text name="garuda.queryBuilder.babyBirthDate"/></th>
				<th id="race"><s:text name="garuda.queryBuilder.race"/></th>
				<th id="racesumm"><s:text name="garuda.cordentry.label.nmdpbroadrace"/></th>
				<th id="funded"><s:text name="garuda.pendingorderpage.label.funded"/></th>
				<th id="fundingcat"><s:text name="garuda.queryBuilder.fundingcategory"/></th>
				<th id="bacterialres"><s:text name="garuda.cbuentry.label.bcresult"/></th>
				<th id="fungalres"><s:text name="garuda.cbuentry.label.fcresult"/></th>
				<th id="hemoglobinopathy"><s:text name="garuda.minimumcriteria.label.hemoglobin"/></th>
				<th id="procesingproc"><s:text name="garuda.cbbprocedures.label.processingprocedure"/></th>
				<th id="totcbunucl"><s:text name="garuda.queryBuilder.label.totalcbunucleatedcount"/></th>
				<th id="totcbunuclun"><s:text name="garuda.queryBuilder.label.totalcbunucleatedcountun"/></th>
				<th id="totcd34"><s:text name="garuda.queryBuilder.totalcd34"/></th>
				<th id="viability"><s:text name="garuda.queryBuilder.viabilitycount"/></th>
				<th id="viabmethod"><s:text name="garuda.common.lable.viability"/></th>
				<th id="finalprod"><s:text name="garuda.productinsert.label.finalProdVol"/></th>
				<th id="ctshipdate"><s:text name="garuda.idsReport.ctshipdate"/></th>
				<th id="orshipdate"><s:text name="garuda.idsReport.orshipdate"/></th>
				<%-- <s:if test="isWorkflowActivitySelected==true"> --%>
<th id="workflowCbuchar"><s:text name="garuda.queryBuilder.label.requestType"/></th>
				<%-- </s:if>
				<s:if test="isWorkflowActivitySelected==true"> --%>
				   <th id="reqdate"><s:text name="garuda.idsReport.reqdate"/></th>
				<%-- </s:if>	 --%>
			</tr>

		</thead>
		<tbody id="simple_reportTblTBody">
		      <s:iterator value="cordInfoList" var="rowVal" status="row">	
					<tr id="<s:property	value="%{#row.index}"/>" class="hotspot" >
					    <td ><s:property value="%{#rowVal[0]}"  /></td>	
						<td ><s:property value="%{#rowVal[1]}"  /></td>						
						<td><s:property value="%{#rowVal[2]}"  /></td>
						<td><s:property value="%{#rowVal[3]}"  /></td>
						<td><s:property value="%{#rowVal[4]}"  /></td>
						<td><s:property value="%{#rowVal[5]}"  /></td>
						<td><s:property value="%{#rowVal[28]}"  /></td>
						<td><s:property value="%{#rowVal[6]}"  /></td>
						<td><s:property value="%{#rowVal[7]}"  /></td>
						<td><s:property value="%{#rowVal[8]}"  /></td>
						<td><s:property value="%{#rowVal[9]}"  /></td>
						<td ><s:property value="%{#rowVal[10]}"  /></td>	
						<td ><s:property value="%{#rowVal[11]}"  /></td>	
						<td ><s:property value="%{#rowVal[12]}"  /></td>	
						<td ><s:property value="%{#rowVal[13]}"  /></td>	
						<td ><s:property value="%{#rowVal[14]}"  /></td>	
						<td ><s:property value="%{#rowVal[15]}"  /></td>	
						<td ><s:property value="%{#rowVal[16]}"  /></td>	
						<td ><s:property value="%{#rowVal[17]}"  /></td>	
						<td ><s:property value="%{#rowVal[18]}"  /></td>	
						<td ><s:property value="%{#rowVal[19]}"  /></td>	
						<td ><s:property value="%{#rowVal[20]}"  /></td>	
						<td ><s:property value="%{#rowVal[21]}"  /></td>
						<td ><s:property value="%{#rowVal[22]}"  /></td>
						<td ><s:property value="%{#rowVal[23]}"  /></td>						
						<td><s:property value="%{#rowVal[27]}"  /></td>
						<td>
						    <s:if test="getCodeListDescById(%{#rowVal[25]})==@com.velos.ordercomponent.util.VelosGarudaConstants@CT">
						       <s:property value="%{#rowVal[24]}"  />
						    </s:if>
						</td>
						<td>`
						    <s:if test="getCodeListDescById(%{#rowVal[25]})==@com.velos.ordercomponent.util.VelosGarudaConstants@OR">
						       <s:property value="%{#rowVal[24]}"  />
						    </s:if>
						</td>
						 <td><s:property value="%{#rowVal[26]}"  /></td>
						
					</tr>
			</s:iterator>
		</tbody>
		<tfoot>
			<tr><td colspan="12">
		              <jsp:include page="paginationFooter.jsp">
					  	<jsp:param value="searchTbleReport" name="divName"/>
					  	<jsp:param value="Tableform" name="formName"/>
					  	<jsp:param value="showsRow" name="showsRecordId"/>
					  	<jsp:param value="getSimpleQueryPaginationData" name="url"/>
					  	<jsp:param value="inputs" name="cbuid"/>
					  	<jsp:param value="workflow" name="paginateSearch"/>
					    <jsp:param value="1" name="idparam"/>
					  	<jsp:param value="f_Lic_Elig_grid" name="bodyOnloadFn"/>
				    </jsp:include>
              </td>
            </tr>
		</tfoot>	
     </table>
     <table width="100%">
        <tr>
           <td colspan="2">           
           </td>
           <td align="right" colspan="2">
              <button onclick="f_modified('pdfReportsForQueryBuilder?selectedSiteId=<s:property value="selectedSiteId"/>&paginateModule=<s:property value="#request.paginateModule"/>&isMultipleCBBSelected=<s:property value="#request.isMultipleCBBSelected"/>&ReportType=pdf&isWorkflowActivitySelected=<s:property value="#request.isWorkflowActivitySelected"/>&repId=190&amp;repName=NMDP CBU Licensure and Eligibility Deteremination Report&amp;selDate=&amp;params=','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');">Print as PDF</button>&nbsp;&nbsp;&nbsp;&nbsp;
              <button onclick="ExpoCSV.expoCSVData();">Export to CSV</button>&nbsp;&nbsp;&nbsp;&nbsp;
              <button onclick="ExpoCSV.expoExcelData();">Export to Excel</button>
           </td>
        </tr>
     </table>     
     <script>
     var i=0;
     function f_Lic_Elig_grid(){
    	 
 		
    		//var showEntriesTxt='Show <select name="showsRow" id="showsRow" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="<s:property value="paginateSearch.getiTotalRows()"/>" selected="selected">All</option></select> Entries';
    		var showEntriesTxt='Show <select name="showsRow" id="showsRow" style="width:auto; height:25px;"><option value="10">10</option><option value="25">25</option><option value="50">50</option></select> Entries';
    		var is_all_entries=$j('#showsRow').val();
    		var pageNo=$j('#paginateWrapper1').find('.currentPage').text();
    		var showEntries=$j('#lookup_tbl_sentries').val();
    		var totalEntries=$j('#tentries').val();
    		var sort=0;
    		var sortDir="asc";
    		var SortParm=$j('#lookup_tbl_sort_col').val();
    		var seachTxt="";
    		var preTxt=$j('#lookup_search_txt').val();
    		
    		if((preTxt!=null && $j.trim(preTxt)!="")){
    			seachTxt=preTxt;
    		}
    		
    		 $j('#query_builder_tbl').val('licElig');
    		
    		

    		if(SortParm!=undefined && SortParm!=null && SortParm!=""){
    			sort=$j('#lookup_tbl_sort_col').val();
    			sortDir=$j('#lookup_tbl_sort_type').val();
    		}	
    		

    		if((showEntries==null || showEntries==undefined || showEntries=="undefined" || $j.trim(showEntries)=="")){
    			is_all_entries='10';
    			$j('#lookup_tbl_sentries').val(is_all_entries);
    			showEntries=$j('#lookup_tbl_sentries').val();
    		}
    		
    		
    		if(showEntries=='All'){
    			
    			 otable =  $j('#tablelookup1').dataTable({
    				"sScrollY": "100px", 
    		        "sDom": "frtiS",
    		        "sAjaxSource": 'getJsonPagination.action?TblFind=lookup&currentpage=licElig&allEntries=ALL&paginateModule=<s:property value="paginateModule"/>&cbuCount=<s:property value="cbuCount"/>&cbuid='+$j("#inputs").val(),
    		        "fnServerData": function ( sSource, aoData, fnCallback ) {
    		        	xhr=$j.getJSON( sSource, aoData, function (json) { 
    		                fnCallback(json);
    		               // ProgBarController.stop();
    		                
    		            } );
    		        },
    		        "fnInitComplete": function(oSettings, json) {
    		        	ProgBarController.stop();
    		        	 //console.warn( 'DataTables has finished its initialisation.' );
    		         },
    		        "bServerSide": true,
    		       // "bProcessing": true,
    		        "bRetrieve" : true,
    		        "bDestroy": true,
    		        "aaSorting": [[ sort, sortDir ]],
    		        "oSearch": {"sSearch": seachTxt},
    		        "bDeferRender": true,
    		        "bAutoWidth": false,
    		        "sAjaxDataProp": "aaData",
    		        "oLanguage": {
    		        	"sEmptyTable": "<s:text name="garuda.common.message.noMatchingRecords"/>"
    				},
    		        "aoColumnDefs": [
							<s:if test="isMultipleCBBSelected==true">
							{
								/*CBB ID*/
							  "aTargets": [i++],"iDataSort": 0,"mDataProp": function ( source, type, val ) {
							    	var str="";
							  	    if(source[1]!='null' && source[1]!=null){
							  	    str = "<span><div>"+source[1]+"</div></span>";
							  	    } 
							      	return str;
							 }},
							 </s:if> 
							{
								/*CBU REG ID ID*/
							        "aTargets": [i++],"iDataSort": 1, "mDataProp": function ( source, type, val ) {
							         var str="";
							         if(source[2]!='null' && source[2]!=null){
							        	 str = checkDup(source[0])!=undefined ? "<span class='duplicate'><div>"+source[2]+"</div></span>" : "<span><div>"+source[2]+"</div></span>"; 	 
							         }
							         return str;
							 }},
								{
								 /*CBU Local ID*/
							     "aTargets": [i++],"iDataSort": 2, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[3]!='null' && source[3]!=null){
							    		str="<span><div>"+source[3]+"</div></span>";	
							    	}
							        return str;
							 }},
							 {
								 /*Unique Product Identity on Bag*/
							     "aTargets": [i++],"iDataSort": 3, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[4]!='null' && source[4]!=null)
							    	str="<span><div>"+source[4]+"</div></span>"; 
							        return str;
							 }},
							 {
								 /*CBU Collection Date*/
							     "aTargets": [i++],"iDataSort": 4, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[5]!='null' && source[5]!=null)
							    	str="<span><div>"+source[5]+"</div></span>"; 
							        return str;
							 }},
							 {
								 /*NMDP Registration Date*/
							     "aTargets": [i++],"iDataSort": 5, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[6]!='null' && source[6]!=null)
							    	str="<span><div>"+source[6]+"</div></span>"; 
							        return str;
							 }},
							 {
								 /*CBU Status*/
							     "aTargets": [i++],"iDataSort": 6, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[7]!='null' && source[7]!=null)
							    	str="<span><div>"+source[7]+"</div></span>"; 
							        return str;
							 }},
							 {
								 /*Reason*/
							     "aTargets": [i++],"iDataSort": 7, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[8]!='null' && source[8]!=null)
							    	str="<span><div>"+source[8]+"</div></span>"; 
							        return str;
							 }},
							 {
								 /*Licensure Status*/
							     "aTargets": [i++],"iDataSort": 8, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[9]!='null' && source[9]!=null)
							    	str="<span><div>"+source[9]+"</div></span>"; 
							        return str;
							 }},
							 {
								 /*Unlicensed Reason*/
							     "aTargets": [i++],"iDataSort": 9, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[10]!='null' && source[10]!=null)
							    	str="<span><div>"+source[10]+"</div></span>";
							        return str;
							 }},
							 {
								 /*Eligibility Determination*/
							     "aTargets": [i++],"iDataSort": 10, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[11]!='null' && source[11]!=null ){
							    		str="<span><div>"+source[11]+"</div></span>";
							    	}
							    	else{
								    	str="Not Yet Determined";
								    } 
							        return str;
							 }},
							 {
                            	 /*Ineligible Reason*/
                                 "aTargets": [i++],"iDataSort": 11, "mDataProp": function ( source, type, val ) {
                                	var str="";
                                	if(source[12]!='null' && source[12]!=null && source[11]!='null' && source[11]!=null && source[11]=='Ineligible')
                                	str="<span><div>"+source[12]+"</div></span>"; 
                                    return str;
                             }},
                             {
    		                      /*Incomplete Reason*/
    		                      "aTargets": [i++],"iDataSort": 11, "mDataProp": function ( source, type, val ) {
	    		                      var str="";
	    		                      if(source[12]!='null' && source[12]!=null && source[11]!='null' && source[11]!=null && source[11]=='Incomplete' )
	    		                      str="<span><div>"+source[12]+"</div></span>"; 
	    		                 	  return str;
    		                 }},
    		                 {
								 /*Baby birth date*/
							     "aTargets": [i++],"iDataSort": 12, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[13]!='null' && source[13]!=null)
							    	str="<span><div>"+source[13]+"</div></span>"; 
							        return str;
							 }},
							 {
								 /*Race*/
							     "aTargets": [i++],"iDataSort": 13, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[14]!='null' && source[14]!=null)
							    	str="<span><div>"+source[14]+"</div></span>"; 
							        return str;
							 }},
							 {
								 /*Race Summary*/
							     "aTargets": [i++],"iDataSort": 14, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[15]!='null' && source[15]!=null)
							    	str="<span><div>"+source[15]+"</div></span>"; 
							        return str;
							 }},
							 {
								 /*Funded*/
							     "aTargets": [i++],"iDataSort": 15, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[16]!='null' && source[16]!=null)
								    	{
								    	if(source[16]=='0')
									    	{
								    		str="<span><div>"+"No"+"</div></span>";
									    	}
								    	else if(source[16]=='1'){
								    		str="<span><div>"+"Yes"+"</div></span>";
									    	}
								    	} 
							        return str;
							 }},
							 {
								 /*Funding Category*/
							     "aTargets": [i++],"iDataSort": 29, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[17]!='null' && source[17]!=null)
							    	str="<span><div>"+source[17]+"</div></span>"; 
							        return str;
							 }},
							 {
								 /*Bacterial culture result*/
							     "aTargets": [i++],"iDataSort": 16, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[18]!='null' && source[18]!=null)
							    	str="<span><div>"+source[18]+"</div></span>"; 
							        return str;
							 }},
							 {
								 /*Fungal culture result*/
							     "aTargets": [i++],"iDataSort": 17, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[19]!='null' && source[19]!=null)
							    	str="<span><div>"+source[19]+"</div></span>"; 
							        return str;
							 }},
							 {
								 /*Hemoglobinopathy Testing*/
							     "aTargets": [i++],"iDataSort": 18, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[20]!='null' && source[20]!=null)
							    	str="<span><div>"+source[20]+"</div></span>"; 
							        return str;
							 }},
							 {
								 /*Processing procedure*/
							     "aTargets": [i++],"iDataSort": 19, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[21]!='null' && source[21]!=null)
							    	str="<span><div>"+source[21]+"</div></span>"; 
							        return str;
							 }},
							 {
								 /*Total CBU Nucleated cell count (uncorrected)*/
							     "aTargets": [i++],"iDataSort": 20, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[22]!='null' && source[22]!=null)
							    	str="<span><div>"+source[22]+"</div></span>"; 
							        return str;
							 }},
							 {
								 /*Total CBU Nucleated cell count (unknown if uncorrected)*/
							     "aTargets": [i++],"iDataSort": 30, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[23]!='null' && source[23]!=null)
							    	str="<span><div>"+source[23]+"</div></span>"; 
							        return str;
							 }},
							 {
								 /*Total CD34+ cell count*/
							     "aTargets": [i++],"iDataSort": 21, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[24]!='null' && source[24]!=null)
							    	str="<span><div>"+source[24]+"</div></span>"; 
							        return str;
							 }},
							 {
								 /*Viability count*/
							     "aTargets": [i++],"iDataSort": 22, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[25]!='null' && source[25]!=null)
							    	str="<span><div>"+source[25]+"</div></span>"; 
							        return str;
							 }},
							 {
								 /*Viability Method*/
							     "aTargets": [i++],"iDataSort": 23, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[26]!='null' && source[26]!=null)
							    	str="<span><div>"+source[26]+"</div></span>"; 
							        return str;
							 }},
							 {
								 /*Final Product Volume*/
							     "aTargets": [i++],"iDataSort": 24, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[27]!='null' && source[27]!=null)
							    	str="<span><div>"+source[27]+"</div></span>"; 
							        return str;
							 }},
							 {
								 /*CT ship date*/
							     "aTargets": [i++],"iDataSort": 25, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[28]!='null' && source[28]!=null && source[30]!=null && source[30]=='CT - Ship Sample')
							    	str="<span><div>"+source[28]+"</div></span>"; 
							        return str;
							 }},
							 {
								 /*OR ship date*/
							     "aTargets": [i++],"iDataSort": 26, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[29]!='null' && source[29]!=null && source[30]!='null' && source[30]!=null && source[30]=='OR')
							    	str="<span><div>"+source[29]+"</div></span>"; 
							        return str;
							 }},
							 {
								 /*Current Workflow Activity Stage*/
							     "aTargets": [i++],"iDataSort": 27, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[30]!='null' && source[30]!=null)
							    	str="<span><div>"+source[30]+"</div></span>"; 
							        return str;
							 }},
							 {
								 /*Request Date*/
							     "aTargets": [i++],"iDataSort": 28, "mDataProp": function ( source, type, val ) {
							    	var str="";
							    	if(source[31]!='null' && source[31]!=null)
							    	str="<span><div>"+source[31]+"</div></span>"; 
							        return str;
							 }}
    		                             
    		                        ],
    		                        "fnDrawCallback": function() {
    		                            $j('#tablelookup1_wrapper').find('.dataTables_scrollFoot').hide();
    		                            $j('#tablelookup1_info').show();
    		                            $j("#showsRow option:contains('All')").attr('selected', 'selected');
    		                            $j('#tablelookup1_wrapper').find('.dataTables_scrollHeadInner').find('table').css('position','');
    		                            //var inputs = $j('#inputs').val();	                        	
    		                        	//$j('.lookupmenu :text').removeAttr("disabled");
    		                        	setSortData('lookupTblThead','lookup_tbl_sort_col','lookup_tbl_sort_type');
    		                        	$j(".duplicate").each(function(){
   		                 	    	  	  $j(this).parent().css("background-color","red");	
   		                 	    	    });
    		                        }
    		    });
    			$j('#tablelookup1_filter').before(showEntriesTxt); 
    			
    		}else{
    			otable =  $j('#tablelookup1').dataTable({
    		        "sAjaxSource": 'getJsonPagination.action?TblFind=lookup&allEntries=ALL&currentpage=licElig&paginateModule=<s:property value="paginateModule"/>&cbuCount=<s:property value="cbuCount"/>&iDisplayStart=0&iDisplayLength='+is_all_entries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries,
    		        "fnServerData": function ( sSource, aoData, fnCallback ) {
    		        	xhr=$j.getJSON( sSource, aoData, function (json) { 
    		                fnCallback(json);
    		               // ProgBarController.stop();
    		              //  console.warn('ProgBarController.stop');
    		            } );
    		        },
    		        "fnInitComplete": function(oSettings, json) {
    		        	ProgBarController.stop();
    		        	 //console.warn( 'DataTables has finished its initialisation.' );
    		        },
    		        "bServerSide": true,
    		        //"bProcessing": true,
    		        "bRetrieve" : true,
    		        "bDestroy": true,
    		        "aaSorting": [[ sort, sortDir ]],
    		        "oSearch": {"sSearch": seachTxt},
    		        "bDeferRender": true,
    		        "bAutoWidth": false,
    		        "sAjaxDataProp": "aaData",
    		        "oLanguage": {
    		        	"sEmptyTable": "<s:text name="garuda.common.message.noMatchingRecords"/>"
    				},
    		        "aoColumnDefs": [
										<s:if test="isMultipleCBBSelected==true">
    		                            {
    		                            	/*CBB ID*/
    		                              "aTargets": [i++],"iDataSort": 0,"mDataProp": function ( source, type, val ) {
    				                        	var str="";
    			                          	    if(source[1]!='null' && source[1]!=null){
    			                          	    str = "<span><div>"+source[1]+"</div></span>";
    			                          	    } 
    			                              	return str;
    		                             }},
    		                             </s:if> 
    		                            {
    		                            	/*CBU REG ID ID*/
    		                                    "aTargets": [i++],"iDataSort": 1, "mDataProp": function ( source, type, val ) {
    		                                     var str="";
    		                                     if(source[2]!='null' && source[2]!=null){
    		                                    	 str = checkDup(source[0])!=undefined ? "<span class='duplicate'><div>"+source[2]+"</div></span>" : "<span><div>"+source[2]+"</div></span>"; 	 
    		                                     }
    		                                     return str;
    		                             }},
    		                          	{
    		                            	 /*CBU Local ID*/
    		                                 "aTargets": [i++],"iDataSort": 2, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[3]!='null' && source[3]!=null){
    		                                		str="<span><div>"+source[3]+"</div></span>";	
    		                                	}
    		                                    return str;
    		                             }},
    		                             {
    		                            	 /*Unique Product Identity on Bag*/
    		                                 "aTargets": [i++],"iDataSort": 3, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[4]!='null' && source[4]!=null)
    		                                	str="<span><div>"+source[4]+"</div></span>"; 
    		                                    return str;
    		                             }},
    		                             {
    		                            	 /*CBU Collection Date*/
    		                                 "aTargets": [i++],"iDataSort": 4, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[5]!='null' && source[5]!=null)
    		                                	str="<span><div>"+source[5]+"</div></span>"; 
    		                                    return str;
    		                             }},
    		                             {
    		                            	 /*NMDP Registration Date*/
    		                                 "aTargets": [i++],"iDataSort": 5, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[6]!='null' && source[6]!=null)
    		                                	str="<span><div>"+source[6]+"</div></span>"; 
    		                                    return str;
    		                             }},
    		                             {
    		                            	 /*CBU Status*/
    		                                 "aTargets": [i++],"iDataSort": 6, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[7]!='null' && source[7]!=null)
    		                                	str="<span><div>"+source[7]+"</div></span>"; 
    		                                    return str;
    		                             }},
    		                             {
    		                            	 /*Reason*/
    		                                 "aTargets": [i++],"iDataSort": 7, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[8]!='null' && source[8]!=null)
    		                                	str="<span><div>"+source[8]+"</div></span>"; 
    		                                    return str;
    		                             }},
    		                             {
    		                            	 /*Licensure Status*/
    		                                 "aTargets": [i++],"iDataSort": 8, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[9]!='null' && source[9]!=null)
    		                                	str="<span><div>"+source[9]+"</div></span>"; 
    		                                    return str;
    		                             }},
    		                             
    		                             {
    		                            	 /*Unlicensed Reason*/
    		                                 "aTargets": [i++],"iDataSort": 9, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[10]!='null' && source[10]!=null)
    		                                	str="<span><div>"+source[10]+"</div></span>";
    		                                    return str;
    		                             }},
    		                             
    		                             {
    		                            	 /*Eligibility Determination*/
    		                                 "aTargets": [i++],"iDataSort": 10, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[11]!='null' && source[11]!=null ){
    		                                		str="<span><div>"+source[11]+"</div></span>";
    		                                	}
    		                                	else{
    										    	str="Not Yet Determined";
    										    } 
    		                                    return str;
    		                             }},
    		                             
    		                             {
    		                            	 /*Ineligible Reason*/
    		                                 "aTargets": [i++],"iDataSort": 11, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[12]!='null' && source[12]!=null && source[11]!='null' && source[11]!=null && source[11]=='Ineligible')
    		                                	str="<span><div>"+source[12]+"</div></span>"; 
    		                                    return str;
    		                             }},
    		                             
    		                             
    		                             {
    		                            	 /*Incomplete Reason*/
    		                                 "aTargets": [i++],"iDataSort": 11, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[12]!='null' && source[12]!=null && source[11]!='null' && source[11]!=null && source[11]=='Incomplete' )
    		                                	str="<span><div>"+source[12]+"</div></span>"; 
    		                                    return str;
    		                             }},
    		                             
    		                             {
    		                            	 /*Baby birth date*/
    		                                 "aTargets": [i++],"iDataSort": 12, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[13]!='null' && source[13]!=null)
    		                                	str="<span><div>"+source[13]+"</div></span>"; 
    		                                    return str;
    		                             }},
    		                             {
    		                            	 /*Race*/
    		                                 "aTargets": [i++],"iDataSort": 13, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[14]!='null' && source[14]!=null)
    		                                	str="<span><div>"+source[14]+"</div></span>"; 
    		                                    return str;
    		                             }},
    		                             {
    		                            	 /*Race Summary*/
    		                                 "aTargets": [i++],"iDataSort": 14, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[15]!='null' && source[15]!=null)
    		                                	str="<span><div>"+source[15]+"</div></span>"; 
    		                                    return str;
    		                             }},
    		                             {
    		                            	 /*Funded*/
    		                                 "aTargets": [i++],"iDataSort": 15, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[16]!='null' && source[16]!=null)
    									    	{
    									    	if(source[16]=='0')
    										    	{
    									    		str="<span><div>"+"No"+"</div></span>";
    										    	}
    									    	else if(source[16]=='1'){
    									    		str="<span><div>"+"Yes"+"</div></span>";
    										    	}
    									    	}  
    		                                    return str;
    		                             }},
    		                             {
											 /*Funding Category*/
										     "aTargets": [i++],"iDataSort": 29, "mDataProp": function ( source, type, val ) {
										    	var str="";
										    	if(source[17]!='null' && source[17]!=null)
										    	str="<span><div>"+source[17]+"</div></span>"; 
										        return str;
										 }},
    		                             {
    		                            	 /*Bacterial culture result*/
    		                                 "aTargets": [i++],"iDataSort": 16, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[18]!='null' && source[18]!=null)
    		                                	str="<span><div>"+source[18]+"</div></span>"; 
    		                                    return str;
    		                             }},
    		                             {
    		                            	 /*Fungal culture result*/
    		                                 "aTargets": [i++],"iDataSort": 17, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[19]!='null' && source[19]!=null)
    		                                	str="<span><div>"+source[19]+"</div></span>"; 
    		                                    return str;
    		                             }},
    		                             {
    		                            	 /*Hemoglobinopathy Testing*/
    		                                 "aTargets": [i++],"iDataSort": 18, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[20]!='null' && source[20]!=null)
    		                                	str="<span><div>"+source[20]+"</div></span>"; 
    		                                    return str;
    		                             }},
    		                             {
    		                            	 /*Processing procedure*/
    		                                 "aTargets": [i++],"iDataSort": 19, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[21]!='null' && source[21]!=null)
    		                                	str="<span><div>"+source[21]+"</div></span>"; 
    		                                    return str;
    		                             }},
    		                             {
    		                            	 /*Total CBU Nucleated cell count (uncorrected)*/
    		                                 "aTargets": [i++],"iDataSort": 20, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[22]!='null' && source[22]!=null)
    		                                	str="<span><div>"+source[22]+"</div></span>"; 
    		                                    return str;
    		                             }},
    		                             {
											 /*Total CBU Nucleated cell count (unknown if uncorrected)*/
										     "aTargets": [i++],"iDataSort": 30, "mDataProp": function ( source, type, val ) {
										    	var str="";
										    	if(source[23]!='null' && source[23]!=null)
										    	str="<span><div>"+source[23]+"</div></span>"; 
										        return str;
										 }},
    		                             {
    		                            	 /*Total CD34+ cell count*/
    		                                 "aTargets": [i++],"iDataSort": 21, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[24]!='null' && source[24]!=null)
    		                                	str="<span><div>"+source[24]+"</div></span>"; 
    		                                    return str;
    		                             }},
    		                             {
    		                            	 /*Viability count*/
    		                                 "aTargets": [i++],"iDataSort": 22, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[25]!='null' && source[25]!=null)
    		                                	str="<span><div>"+source[25]+"</div></span>"; 
    		                                    return str;
    		                             }},
    		                             {
    		                            	 /*Viability Method*/
    		                                 "aTargets": [i++],"iDataSort": 23, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[26]!='null' && source[26]!=null)
    		                                	str="<span><div>"+source[26]+"</div></span>"; 
    		                                    return str;
    		                             }},
    		                             {
    		                            	 /*Final Product Volume*/
    		                                 "aTargets": [i++],"iDataSort": 24, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[27]!='null' && source[27]!=null)
    		                                	str="<span><div>"+source[27]+"</div></span>"; 
    		                                    return str;
    		                             }},
    		                             {
    		                            	 /*CT ship date*/
    		                                 "aTargets": [i++],"iDataSort": 25, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[28]!='null' && source[28]!=null && source[30]!=null && source[30]=='CT - Ship Sample')
    		                                	str="<span><div>"+source[28]+"</div></span>"; 
    		                                    return str;
    		                             }},
    		                             {
    		                            	 /*OR ship date*/
    		                                 "aTargets": [i++],"iDataSort": 26, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[29]!='null' && source[29]!=null && source[30]!='null' && source[30]!=null && source[30]=='OR')
    		                                	str="<span><div>"+source[29]+"</div></span>"; 
    		                                    return str;
    		                             }},
    		                             {
    		                            	 /*Current Workflow Activity Stage*/
    		                                 "aTargets": [i++],"iDataSort": 27, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[30]!='null' && source[30]!=null)
    		                                	str="<span><div>"+source[30]+"</div></span>"; 
    		                                    return str;
    		                             }},
    		                             {
    		                            	 /*Request Date*/
    		                                 "aTargets": [i++],"iDataSort": 28, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[31]!='null' && source[31]!=null)
    		                                	str="<span><div>"+source[31]+"</div></span>"; 
    		                                    return str;
    		                             }}
    		                        ],
    		                        "fnDrawCallback": function() {
    		                            $j('#tablelookup1_wrapper').find('.dataTables_scrollHeadInner').find('table').css('position','');
    		                            //$j('.lookupmenu :text').removeAttr("disabled");
    		                        	setSortData('lookupTblThead','lookup_tbl_sort_col','lookup_tbl_sort_type');
    		                        	$j('#tablelookup1_info').hide();
    		                 			$j('#tablelookup1_paginate').hide();
    		                 			$j(".duplicate").each(function(){
   		                 	    	  	  $j(this).parent().css("background-color","red");	
   		                 	    	    });
    		                        }
    		    });
    			$j('#tablelookup1_length').replaceWith(showEntriesTxt);
    			
    		} 
    	    
    	    $j('#showsRow').change(function(){
    	        var selectedText = $j('#showsRow :selected').html();
    	        //alert("value:::::::::::::::::"+selectedText)
    	        $j('#lookup_tbl_sentries').val(selectedText);
    	        $j('#query_builder_tbl').val('licElig');
    	        paginationFooter(0,"getSimpleQueryPaginationData,showsRow,inputs,Tableform,searchTbleReport,f_Lic_Elig_grid",'<s:property value="cbuCount"/>','<s:property value="paginateModule"/>','<s:property value="%{#request.selSiteIdentifier}"/>');
    	    }); 
    	    
    	    $j('#tablelookup1_filter').find(".searching1").keyup(function(e){
    			  var val = $j(this).val();
    			  val=$j.trim(val);
    			  $j('#lookup_search_txt').val(val);
    			  if(e.keyCode==13)
    			  {	 
        			   showProgressOverlayMsg();
        			   setTimeout('characteristicPagination()', 500);    			  
    				  
        			  }
    		});    		
    	}
 	function characteristicPagination(){
 		paginationFooter(0,"getSimpleQueryPaginationData,showsRow,inputs,Tableform,searchTbleReport,f_Lic_Elig_grid","0",'<s:property value="paginateModule"/>','<s:property value="%{#request.selSiteIdentifier}"/>');
 	}
     </script>
  </s:elseif>
  <s:elseif test="#request.paginateModule!=null && #request.paginateModule==@com.velos.ordercomponent.util.VelosGarudaConstants@NMDP_CBU_QUICKSUMMARY_REPORT">
  <div align="right"><a href="#" onclick="fn_ResetLookupSort()"  id="resetSortLookup"><s:text name="garuda.pendingorderpage.label.resetsort"/></a></div><br>
      
      <table width="100%">
        <tr>
           <td colspan="2">           
           </td>
           <td align="right" colspan="2">
              <!--<button onclick="printGrid()">Print as PDF</button>&nbsp;&nbsp;&nbsp;&nbsp;
              -->
              <button onclick="f_modified('pdfReportsForQueryBuilder?selectedSiteId=<s:property value="selectedSiteId"/>&paginateModule=<s:property value="#request.paginateModule"/>&isMultipleCBBSelected=<s:property value="#request.isMultipleCBBSelected"/>&ReportType=pdf&isWorkflowActivitySelected=<s:property value="#request.isWorkflowActivitySelected"/>&repId=188&amp;repName=Detail CBU Report&amp;selDate=&amp;params=','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');">Print as PDF</button>&nbsp;&nbsp;&nbsp;&nbsp;
              <button onclick="ExpoCSV.expoCSVData();">Export to CSV</button>&nbsp;&nbsp;&nbsp;&nbsp;
              <button onclick="ExpoCSV.expoExcelData();">Export to Excel</button>
           </td>
        </tr>
     </table>
      
      <table width="100%" cellpadding="0" cellspacing="0" border="0" class="displaycdr searchResults" id="tablelookup1">
		 <thead>
			<tr id="lookupTblThead">
			    <s:if test="isMultipleCBBSelected==true">
				   <th id="cbbid"><s:text name="garuda.queryBuilder.label.cbbid" /></th>
				</s:if>
				<th id="regid"><s:text name="garuda.cbuentry.label.registrycbuid" /></th>				
				<th id="loccbuid"><s:text name="garuda.cbuentry.label.localcbuid" /></th>
				<th id="idonbag"><s:text name="garuda.cbuentry.label.idbag" /></th>
				<th id="isbt"><s:text name="garuda.shippedcbu.report.label.isbt" /></th>
				<%-- <s:if test="isWorkflowActivitySelected==true"> --%>
				<th id="workflowCbuSumm"><s:text name="garuda.cbuentry.label.requestType"/></th>
				<th id="reqdate"><s:text name="garuda.idsReport.reqdate"/></th>
				<%-- </s:if> --%>
				<th id="orshipdate"><s:text name="garuda.idsReport.orshipdate"/></th>
				<th id="infuseddate"><s:text name="garuda.shippedcbu.report.label.orinfuseddt"/></th>
				<th id="patientid"><s:text name="garuda.recipient&tcInfo.label.recipientid" /></th>
				<th id="pdiagnosis"><s:text name="garuda.shippedcbu.report.label.patientdiag" /></th>
				<th id="tccode"><s:text name="garuda.shippedcbu.report.label.tccode"/></th>
				<th id="tcname"><s:text name="garuda.shippedcbu.report.label.tcname"/></th>
			</tr>

		</thead>
		<tbody id="simple_reportTblTBody">
		      <s:iterator value="cordInfoList" var="rowVal" status="row">	
					<tr id="<s:property	value="%{#row.index}"/>" class="hotspot" >
					    <s:if test="isMultipleCBBSelected == true">
					        <td><s:property value="%{#rowVal[6]}"  /></td>
					    </s:if>
						<td ><s:property value="%{#rowVal[1]}"  /></td>
						<td><s:property value="%{#rowVal[3]}"  /></td>
						<td><s:property value="%{#rowVal[8]}"  /></td>
						<td><s:property value="%{#rowVal[5]}"  /></td>	
						<s:if test="isWorkflowActivitySelected==true">
						   <td><s:property value="%{#rowVal[19]}"  /></td>
						   <td><s:property value="%{#rowVal[13]}"  /></td>
						</s:if>
						
						<td>
							 <s:if test="getCodeListDescById(%{#rowVal[10]})==@com.velos.ordercomponent.util.VelosGarudaConstants@OR">
							    <s:property value="%{#rowVal[12]}"  />
							 </s:if>						    
						</td>
						<td>
							<s:if test="getCodeListDescById(%{#rowVal[10]})==@com.velos.ordercomponent.util.VelosGarudaConstants@OR">
							   <s:property value="%{#rowVal[15]}"  />
							</s:if>
						</td>
						<td><s:property value="%{#rowVal[7]}" /></td>
						<td><s:property value="%{#rowVal[16]}"  /></td>
						<td><s:property value="%{#rowVal[17]}"  /></td>
						<td><s:property value="%{#rowVal[18]}"  /></td>
					</tr>
			</s:iterator>
		</tbody>
		<tfoot>
			<tr><td colspan="12">
		              <jsp:include page="paginationFooter.jsp">
					  	<jsp:param value="searchTbleReport" name="divName"/>
					  	<jsp:param value="Tableform" name="formName"/>
					  	<jsp:param value="showsRow" name="showsRecordId"/>
					  	<jsp:param value="getSimpleQueryPaginationData" name="url"/>
					  	<jsp:param value="inputs" name="cbuid"/>
					  	<jsp:param value="workflow" name="paginateSearch"/>
					    <jsp:param value="1" name="idparam"/>
					  	<jsp:param value="f_Shipped_CBU_Grid" name="bodyOnloadFn"/>
				    </jsp:include>
              </td>
            </tr>
		</tfoot>	
     </table>
     <table width="100%">
        <tr>
           <td colspan="2">           
           </td>
           <td align="right" colspan="2">
              <!--<button onclick="printGrid()">Print as PDF</button>&nbsp;&nbsp;&nbsp;&nbsp;
              -->
              <button onclick="f_modified('pdfReportsForQueryBuilder?selectedSiteId=<s:property value="selectedSiteId"/>&paginateModule=<s:property value="#request.paginateModule"/>&isMultipleCBBSelected=<s:property value="#request.isMultipleCBBSelected"/>&ReportType=pdf&isWorkflowActivitySelected=<s:property value="#request.isWorkflowActivitySelected"/>&repId=188&amp;repName=Detail CBU Report&amp;selDate=&amp;params=','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');">Print as PDF</button>&nbsp;&nbsp;&nbsp;&nbsp;
              <button onclick="ExpoCSV.expoCSVData();">Export to CSV</button>&nbsp;&nbsp;&nbsp;&nbsp;
              <button onclick="ExpoCSV.expoExcelData();">Export to Excel</button>
           </td>
        </tr>
     </table>
     <script>
     var i = 0;
     function f_Shipped_CBU_Grid(){
    	 
    	 
 		    
    		//var showEntriesTxt='Show <select name="showsRow" id="showsRow" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="<s:property value="paginateSearch.getiTotalRows()"/>" selected="selected">All</option></select> Entries';
    		var showEntriesTxt='Show <select name="showsRow" id="showsRow" style="width:auto; height:25px;"><option value="10">10</option><option value="25">25</option><option value="50">50</option></select> Entries';
    		var is_all_entries=$j('#showsRow').val();
    		var pageNo=$j('#paginateWrapper1').find('.currentPage').text();
    		var showEntries=$j('#lookup_tbl_sentries').val();
    		var totalEntries=$j('#tentries').val();
    		var sort=0;
    		var sortDir="asc";
    		var SortParm=$j('#lookup_tbl_sort_col').val();
    		var seachTxt="";
    		var preTxt=$j('#lookup_search_txt').val();
    		
    		if((preTxt!=null && $j.trim(preTxt)!="")){
    			seachTxt=preTxt;
    		}
    		
    		$j('#query_builder_tbl').val('shippedcbu');
    		

    		if(SortParm!=undefined && SortParm!=null && SortParm!=""){
        		
    			sort=$j('#lookup_tbl_sort_col').val();
    			sortDir=$j('#lookup_tbl_sort_type').val();
    		}	

    		if((showEntries==null || showEntries==undefined || showEntries=="undefined" || $j.trim(showEntries)=="")){
    			is_all_entries='10';
    			$j('#lookup_tbl_sentries').val(is_all_entries);
    			showEntries=$j('#lookup_tbl_sentries').val();
    		}
    		
    		
    		if(showEntries=='All'){
    			
    			 otable =  $j('#tablelookup1').dataTable({
    				"sScrollY": "100px", 
    		        "sDom": "frtiS",
    		        "sAjaxSource": 'getJsonPagination.action?TblFind=lookup&currentpage=shippedcbu&allEntries=ALL&paginateModule=<s:property value="paginateModule"/>&cbuCount=<s:property value="cbuCount"/>&cbuid='+$j("#inputs").val(),
    		        "fnServerData": function ( sSource, aoData, fnCallback ) {
    		        	xhr=$j.getJSON( sSource, aoData, function (json) { 
    		                fnCallback(json);
    		               // ProgBarController.stop();
    		            } );
    		        },
    		        "fnInitComplete": function(oSettings, json) {
    		        	ProgBarController.stop();
    		        	 //console.warn( 'DataTables has finished its initialisation.' );
    		         },
    		        "bServerSide": true,
    		       // "bProcessing": true,
    		        "bRetrieve" : true,
    		        "bDestroy": true,
    		        "aaSorting": [[ sort, sortDir ]],
    		        "oSearch": {"sSearch": seachTxt},
    		        "bDeferRender": true,
    		        "bAutoWidth": false,
    		        "sAjaxDataProp": "aaData",
    		        "oLanguage": {
    		        	"sEmptyTable": "<s:text name="garuda.common.message.noMatchingRecords"/>"
    				},
    		        "aoColumnDefs": [
    		                         /*CBB ID*/
                                   <s:if test="isMultipleCBBSelected == true">
    		                          {
    		                              "aTargets": [i++],"iDataSort": 0,"mDataProp": function ( source, type, val ) {
    				                        	var str="";
    			                          	    if(source[1]!='null' && source[1]!=null){
    			                          	    str = "<span><div>"+source[1]+"</div></span>";
    			                          	    } 
    			                          	    return str;
    		                             }},
    		                        </s:if>
    		                             /*Registry Id*/
    		                            {
    		                                    "aTargets": [i++],"iDataSort": 1, "mDataProp": function ( source, type, val ) {
    		                                     var str="";
    		                                     if(source[2]!='null' && source[2]!=null){
    		                                    	 str = checkDup(source[0])!=undefined ? "<span class='duplicate'><div>"+source[2]+"</div></span>" : "<span><div>"+source[2]+"</div></span>"; 	 
    		                                     }
    		                                     return str;
    		                             }},
    		                             /*Local CBU Id*/
    		                          	{
    		                                 "aTargets": [i++],"iDataSort": 2, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[3]!='null' && source[3]!=null){
    		                                		str="<span><div>"+source[3]+"</div></span>";	
    		                                	}
    		                                	return str;
    		                             }},
    		                             /*Unique Product Id*/
    		                             {
    		                                 "aTargets": [i++],"iDataSort": 3, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[4]!='null' && source[4]!=null)
    		                                	str="<span><div>"+source[4]+"</div></span>"; 
    		                                	return str;
    		                             }},
    		                             /*ISBT*/
    		                             {
    		                                 "aTargets": [i++],"iDataSort": 4, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[5]!='null' && source[5]!=null)
    		                                	str="<span><div>"+source[5]+"</div></span>"; 
    		                                	return str;
    		                             }},
    		                             /*Workflow*/
    		                             {
    		                                 "aTargets": [i++],"iDataSort": 5, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[9]!='null' && source[9]!=null)
    		                                	str="<span><div>"+source[9]+"</div></span>";
    		                                	return str;
    		                             }},
    		                             /*Workflow request date*/
    		                             {
    		                                 "aTargets": [i++],"iDataSort": 6, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[10]!='null' && source[10]!=null)
    		                                	str="<span><div>"+source[10]+"</div></span>"; 
    		                                	return str;
    		                             }},
    		                             /*OR Ship date*/
    		                             {
    		                                 "aTargets": [i++],"iDataSort": 7, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[11]!='null' && source[11]!=null && source[9]!='null' && source[9]!=null  && source[9]=='OR')
    		                                	str="<span><div>"+source[11]+"</div></span>"; 
    		                                	return str;
    		                             }},
    		                             /*OR Infused date*/
    		                             {
    		                                 "aTargets": [i++],"iDataSort": 8, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[12]!='null' && source[12]!=null )
    		                                	str="<span><div>"+source[12]+"</div></span>";
    		                                	return str;
    		                             }},
    		                             /*Patient ID*/
    		                             {
    		                                 "aTargets": [i++],"iDataSort": 9, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[13]!='null' && source[13]!=null )
    		                                	str="<span><div>"+source[13]+"</div></span>";
    		                                	return str;
    		                             }},
    		                             /*Patient diagnosis*/
    		                             {
    		                                 "aTargets": [i++],"iDataSort": 10, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[14]!='null' && source[14]!=null)
    		                                	str="<span><div>"+source[14]+"</div></span>"; 
    		                                	return str;
    		                             }},
    		                             /*TC Code*/
    		                             {
    		                                 "aTargets": [i++],"iDataSort": 11, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[15]!='null' && source[15]!=null)
    		                                	str="<span><div>"+source[15]+"</div></span>"; 
    		                                	return str;
    		                             }},
    		                             /*TC Name*/
    		                             {
    		                                 "aTargets": [i++],"iDataSort": 12, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[16]!='null' && source[16]!=null)
    		                                	str="<span><div>"+source[16]+"</div></span>"; 
    		                                	return str;
    		                             }}
    		                             
    		                             
    		                        ],
    		                        "fnDrawCallback": function() {
    		                            $j('#tablelookup1_wrapper').find('.dataTables_scrollFoot').hide();
    		                            $j('#tablelookup1_info').show();
    		                            $j("#showsRow option:contains('All')").attr('selected', 'selected');
    		                            $j('#tablelookup1_wrapper').find('.dataTables_scrollHeadInner').find('table').css('position','');
    		                            //var inputs = $j('#inputs').val();	                        	
    		                        	//$j('.lookupmenu :text').removeAttr("disabled");
    		                        	setSortData('lookupTblThead','lookup_tbl_sort_col','lookup_tbl_sort_type');
    		                        	$j(".duplicate").each(function(){
   		                 	    	  	  $j(this).parent().css("background-color","red");	
   		                 	    	    });
    		                        }
    		    });
    			$j('#tablelookup1_filter').before(showEntriesTxt); 
    			
    		}else{
    			otable =  $j('#tablelookup1').dataTable({
    		        "sAjaxSource": 'getJsonPagination.action?TblFind=lookup&allEntries=ALL&currentpage=shippedcbu&paginateModule=<s:property value="paginateModule"/>&cbuCount=<s:property value="cbuCount"/>&iDisplayStart=0&iDisplayLength='+is_all_entries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries,
    		        "fnServerData": function ( sSource, aoData, fnCallback ) {
    		        	xhr=$j.getJSON( sSource, aoData, function (json) { 
    		                fnCallback(json);
    		                //ProgBarController.stop();
    		            } );
    		        },
    		        "fnInitComplete": function(oSettings, json) {
    		        	ProgBarController.stop();
    		        	 //console.warn( 'DataTables has finished its initialisation.' );
    		        },
    		        "bServerSide": true,
    		       // "bProcessing": true,
    		        "bRetrieve" : true,
    		        "bDestroy": true,
    		        "aaSorting": [[ sort, sortDir ]],
    		        "oSearch": {"sSearch": seachTxt},
    		        "bDeferRender": true,
    		        "bAutoWidth": false,
    		        "sAjaxDataProp": "aaData",
    		        "oLanguage": {
    		        	"sEmptyTable": "<s:text name="garuda.common.message.noMatchingRecords"/>"
    				},
    		        "aoColumnDefs": [
								   /*CBB ID*/
                                   <s:if test="isMultipleCBBSelected == true">
    		                          {
    		                              "aTargets": [i++],"iDataSort": 0,"mDataProp": function ( source, type, val ) {
    				                        	var str="";
    			                          	    if(source[1]!='null' && source[1]!=null){
    			                          	    str = "<span><div>"+source[1]+"</div></span>";
    			                          	    } 
    			                          	    return str;
    		                             }},
    		                        </s:if>
    		                             /*Registry Id*/
    		                            {
    		                                    "aTargets": [i++],"iDataSort": 1, "mDataProp": function ( source, type, val ) {
    		                                     var str="";
    		                                     if(source[2]!='null' && source[2]!=null){
    		                                    	 str = checkDup(source[0])!=undefined ? "<span class='duplicate'><div>"+source[2]+"</div></span>" : "<span><div>"+source[2]+"</div></span>"; 	 
    		                                     }
    		                                     return str;
    		                             }},
    		                             /*Local CBU Id*/
    		                          	{
    		                                 "aTargets": [i++],"iDataSort": 2, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[3]!='null' && source[3]!=null){
    		                                		str="<span><div>"+source[3]+"</div></span>";	
    		                                	}
    		                                	return str;
    		                             }},
    		                             /*Unique Product Id*/
    		                             {
    		                                 "aTargets": [i++],"iDataSort": 3, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[4]!='null' && source[4]!=null)
    		                                	str="<span><div>"+source[4]+"</div></span>"; 
    		                                	return str;
    		                             }},
    		                             /*ISBT*/
    		                             {
    		                                 "aTargets": [i++],"iDataSort": 4, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[5]!='null' && source[5]!=null)
    		                                	str="<span><div>"+source[5]+"</div></span>"; 
    		                                	return str;
    		                             }},
    		                             /*Workflow*/
    		                             {
    		                                 "aTargets": [i++],"iDataSort": 5, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[9]!='null' && source[9]!=null)
    		                                	str="<span><div>"+source[9]+"</div></span>";
    		                                	return str;
    		                             }},
    		                             /*Workflow request date*/
    		                             {
    		                                 "aTargets": [i++],"iDataSort": 6, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[10]!='null' && source[10]!=null)
    		                                	str="<span><div>"+source[10]+"</div></span>"; 
    		                                	return str;
    		                             }},
    		                             /*OR Ship date*/
    		                             {
    		                                 "aTargets": [i++],"iDataSort": 7, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[11]!='null' && source[11]!=null && source[9]!='null' && source[9]!=null  && source[9]=='OR')
    		                                	str="<span><div>"+source[11]+"</div></span>"; 
    		                                	return str;
    		                             }},
    		                             /*OR Infused date*/
    		                             {
    		                                 "aTargets": [i++],"iDataSort": 8, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[12]!='null' && source[12]!=null )
    		                                	str="<span><div>"+source[12]+"</div></span>";
    		                                	return str;
    		                             }},
    		                             /*Patient ID*/
    		                             {
    		                                 "aTargets": [i++],"iDataSort": 9, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[13]!='null' && source[13]!=null )
    		                                	str="<span><div>"+source[13]+"</div></span>";
    		                                	return str;
    		                             }},
    		                             /*Patient diagnosis*/
    		                             {
    		                                 "aTargets": [i++],"iDataSort": 10, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[14]!='null' && source[14]!=null)
    		                                	str="<span><div>"+source[14]+"</div></span>"; 
    		                                	return str;
    		                             }},
    		                             /*TC Code*/
    		                             {
    		                                 "aTargets": [i++],"iDataSort": 11, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[15]!='null' && source[15]!=null)
    		                                	str="<span><div>"+source[15]+"</div></span>"; 
    		                                	return str;
    		                             }},
    		                             /*TC Name*/
    		                             {
    		                                 "aTargets": [i++],"iDataSort": 12, "mDataProp": function ( source, type, val ) {
    		                                	var str="";
    		                                	if(source[16]!='null' && source[16]!=null)
    		                                	str="<span><div>"+source[16]+"</div></span>"; 
    		                                	return str;
    		                             }}
   
    		                        ],
    		                        "fnDrawCallback": function() {
    		                            $j('#tablelookup1_wrapper').find('.dataTables_scrollHeadInner').find('table').css('position','');
    		                            //$j('.lookupmenu :text').removeAttr("disabled");
    		                        	setSortData('lookupTblThead','lookup_tbl_sort_col','lookup_tbl_sort_type');
    		                        	$j('#tablelookup1_info').hide();
    		                 			$j('#tablelookup1_paginate').hide();
    		                 			 $j(".duplicate").each(function(){
   		                 	    	  	  $j(this).parent().css("background-color","red");	
   		                 	    	    });
    		                        }
    		    });
    			$j('#tablelookup1_length').replaceWith(showEntriesTxt);
    			
    		} 
    	    
    	    $j('#showsRow').change(function(){
    	        var selectedText = $j('#showsRow :selected').html();
    	        $j('#lookup_tbl_sentries').val(selectedText);
    	        paginationFooter(0,"getSimpleQueryPaginationData,showsRow,inputs,Tableform,searchTbleReport,f_Shipped_CBU_Grid",'<s:property value="cbuCount"/>','<s:property value="paginateModule"/>','<s:property value="%{#request.selSiteIdentifier}"/>');
    	    }); 
    	    
    	    $j('#tablelookup1_filter').find(".searching1").keyup(function(e){
    			  var val = $j(this).val();
    			  val=$j.trim(val);
    			  $j('#lookup_search_txt').val(val);
    			  if(e.keyCode==13)
    			  {
    				  showProgressOverlayMsg();
    				  setTimeout('quickSummaryPagination()', 500);
    			  }
    		});     		
    	}
 	function quickSummaryPagination(){
		  paginationFooter(0,"getSimpleQueryPaginationData,showsRow,inputs,Tableform,searchTbleReport,f_Shipped_CBU_Grid","0",'<s:property value="paginateModule"/>','<s:property value="%{#request.selSiteIdentifier}"/>');
	 	 	
 	}
     </script>
  </s:elseif>