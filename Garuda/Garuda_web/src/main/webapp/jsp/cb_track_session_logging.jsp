<jsp:include page="sessionlogging.jsp">
  <jsp:param name="entityId" value="<%=request.getAttribute(\"moduleEntityId\")%>"></jsp:param>
  <jsp:param name="entityType" value="<%=request.getAttribute(\"moduleEntityType\")%>" />
  <jsp:param name="entityIdentifier" value="<%=request.getAttribute(\"moduleEntityIdentifier\")%>" />
  <jsp:param name="eventCode" value="<%=request.getAttribute(\"moduleEntityEventCode\")%>" />
</jsp:include>