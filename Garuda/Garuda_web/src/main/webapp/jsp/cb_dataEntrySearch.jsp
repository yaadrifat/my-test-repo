<%@ taglib prefix="s"  uri="/struts-tags"%>
<%String contextpath=request.getContextPath()+"/jsp/"; %>
<script>

$j(function() {
	$j( "#datepicker" ).datepicker({changeMonth: true,
		changeYear: true});
});


function constructTable() {

		 $j('#searchResults').dataTable();
		 var tdObj = $j('#searchResults').find('tbody').find("tr:eq(0)").find("td:eq(0)");
			if($j(tdObj).hasClass('dataTables_empty')){
				$j('#searchResults_info').hide();
				$j('#searchResults_paginate').hide();
			}

		}

	function dataEntryScreen(formname,cdrcbuId,attchid,pkcordinfoid,pkhlaext){
		$j('#cdrcbuId').val(cdrcbuId);
		$j('#attchid').val(attchid);
		$j('#pkcordinfoid').val(pkcordinfoid);
		$j('#pkhlaext').val(pkhlaext);
		submitform(formname);
	
		
		}
	var cotextpath3 = "<%=contextpath%>";
	//var path=cotextpath3;
	
	function showDocument(attachmentId,dcmsAttachmentId){
		
		if(dcmsAttachmentId==null||dcmsAttachmentId==""){
			var url = cotextpath3+"/viewdocument.action?attachmentId="+attachmentId;
			window.open( url);	
		}
		else{
			var insertUrl=null;
			var userName=null;
			var pass=null;

			 //start reading xml file
            var dcmsXml = readXml1("../xml/dcmsAttachment.xml");
            $j(dcmsXml).find("dcms").each(function() {
            	insertUrl=$j(this).find("dcmsIpAddress").text();
				userName=$j(this).find("userName").text();
				pass=$j(this).find("password").text();
				
			});

			
			refreshDiv('fetchDcmsLog?fetchId='+dcmsAttachmentId,'dcmsRetreiveDiv','searchCordInfo');
			//var url = 'http://'+insertUrl+'/FileData-war/DcmsRetrieveServlet?userName='+userName+'&password='+pass+'&docId='+dcmsAttachmentId;
			var signed='true';
			var url='getAttachmentFromDcms?docId='+dcmsAttachmentId+'&signed='+signed;
			window.open( url);


			
			
		}

	}

	$j(document).ready(function(){
		
		});
	
	 function showDiv()
	 {
				$j('#searchresultparent').css('display','block');
				refreshDiv('getDataDetails','searchTble','searchCordInfo');
	 }				
		
	 function showAddWidget(url){
		 url = url + "?pageId=1" ; 
		 showAddWidgetModal(url);
	}
		
</script>

<div class="col_100 maincontainer ">
<div class="col_100">
<table>
<tr><td>
	<div class="column">
	<div class="portlet" id="cdrsearchparent">
		<div id="cdrsearch" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
			<span class="ui-icon ui-icon-minusthick"></span>
			<span class="ui-icon ui-icon-close" ></span>
			<s:text name="garuda.cbuentry.label.cdrcbusearch" />
			</div>
		<div class="portlet-content">
		<div id="studyDashDiv1" > <s:hidden name="pkcordinfoid" id="pkcordinfoid"></s:hidden>
  
				
	        <s:form id="searchCordInfo" onsubmit="showDiv(); return false;" method="post">
	        <s:hidden name="dcmsTask" value="FETCH"></s:hidden>
			         <table cellpadding="0" cellspacing="0" border="0"
					style="width: 100%; height: auto;" class="col_100 ">         
		         	<tr>
					   <td ><s:text name="garuda.cbuentry.label.cbuid" /></td>
					   <td ><s:textfield  name="cdrCbuPojo.cdrCbuId" placeholder="CBU ID" /></td> 
					    <td><button type="button" onclick="showDiv();"><s:text name="garuda.opentask.label.search"/></button> </td>
					</tr>
					</table>
	        	</s:form>
	        	
		</div></div></div></div></div>
</td></tr>

        
			
<tr><td>
<div class="column">

<div class="portlet" id="searchresultparent" style="display: none;"><div id="searchresult" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ><div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"><span class="ui-icon ui-icon-minusthick"></span><span class="ui-icon ui-icon-close" ></span><s:text name="garuda.cbuentry.label.cdrcbusearchresult" /></div><div class="portlet-content">
<s:form id="search" name="search" action="dataEntryScreen">
<s:hidden name="cdrcbuId" id="cdrcbuId"></s:hidden>
<s:hidden name="attchid" id="attchid"></s:hidden>
<s:hidden name="pkcordinfoid" id="pkcordinfoid"></s:hidden>
<s:hidden name="pkhlaext" id="pkhlaext"></s:hidden>
<div id="searchTble">

<table style="height:100%"  border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="searchResults" >
       
	
                <thead>
                      <tr>
                        
                        <th class="th2"  scope="col"><s:text name="garuda.entry.label.orderid" /></th>
                        <th class="th3"  scope="col"><s:text name="garuda.entry.label.cbuid" /></th>
                        <th class="th4"  scope="col"><s:text name="garuda.entry.label.entryreq" /></th>
                        <th class="th5"  scope="col"><s:text name="garuda.entry.label.view" /></th>
						                                             
                      </tr>
			         </thead>  
			  <tbody>     
			  
			<s:iterator value="cordInfoList" var="rowVal" status="row">
			        
				<tr>
					<td></td>		
					<td><a href="#" onclick="javaScript:return dataEntryScreen('search','<s:property value="%{#rowVal[0]}" />','<s:property value="%{#rowVal[4]}" />','<s:property value="%{#rowVal[8]}" />','<s:property value="%{#rowVal[9]}" />')" ><s:property value="%{#rowVal[0]}" /></a></td>
					<td><a href="#" onclick="javaScript:return dataEntryScreen('search','<s:property value="%{#rowVal[0]}" />','<s:property value="%{#rowVal[4]}" />','<s:property value="%{#rowVal[8]}" />','<s:property value="%{#rowVal[9]}" />')"></a></td>
					<td><a href="#" onclick="javascript: return showDocument('<s:property value="%{#rowVal[4]}" />','<s:property value="%{#rowVal[10]}" />')" ><s:property value="%{#rowVal[7]}" /></a>
						<s:hidden id="fetchId" value="%{#rowVal[10]}"></s:hidden>
					</td>
				</tr>
			 </s:iterator>
						
				      
              </tbody>  
               
                
				  <tfoot>
					<tr><td colspan="4"></td></tr>
				  </tfoot>
                
                </table>
                <s:div id="dcmsRetreiveDiv"></s:div>
	
        </div>
        </s:form>
		</div>
       
         </div>

</div>
</div>
</td></tr>     
       
 </table>       
</div>
</div>