
<%@taglib prefix="s" uri="/struts-tags"%>

<% 
String contextpath=request.getContextPath();
%>
 <script>
 $j(document).ready(function(){ 
		 var contextpath = "<%=contextpath%>";
		 $j.ajax({
		        type: "POST",
		        url: contextpath+"/jsp/getLoginPageDetails.action",
		        async:false,
		        success: function (result){
		        	$j("#versiontd1").text(result.message);
		        },
		        error: function (request, status, error) {
		        	alert("Error " + error);
		            alert(request.responseText);
		        }

			});
	 });</script>
	   	<div class="footer">
	   
	   	<span id="copyright" class="left" style="margin-top: -8px; margin-left: 10px  " ><br> <s:text name="garuda.common.footer.copyright"/> &copy; <a href="http://velos.com" target="_blank"><s:text name="garuda.common.footer.velos"/></a>, <s:text name="garuda.common.footer.inc"/><br />
	   	<span class="left" id="versiontd1" style="padding-top: 4px; margin-left: 10px"></span>
	   	 </span>
	 	<span id ="fimg"  class="hidden float_right marginright20px">
	   		 <a href="http://velos.com" target="_blank"><img style="margin-top:1px;" src="images/velos-footer-logo.gif" alt="<s:text name="garuda.common.footer.velos"/>"  /></a> 
	   	</span>  	   		   
	   	
	   </div>
	   	 
 