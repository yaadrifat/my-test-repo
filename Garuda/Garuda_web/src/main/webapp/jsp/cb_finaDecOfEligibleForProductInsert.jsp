<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%String contextpath = request.getContextPath()+"/jsp/modal/"; %>
<s:if test="cbuFinalReviewPojo.eligibleConfirmFlag == 1">
									<table width="100%" cellspacing="0" cellpadding="0">
									    <tr>
									      <td>								         					
										       	<div align="center"><b><s:text name="garuda.cdrcbuview.label.final_decl_eligibility" /></b></div>
												<div id="finadecelig">
												         <div class="accordion">
																<h3><a href="#"><s:text name="garuda.common.lable.sectionone"/></a></h3>
																<div>
																	<table width="100%" cellspacing="0" cellpadding="0">
																	  <tr>
																	     <td><s:text name="garuda.fdoe.level.cbb" />:</td>
																	     <td>
																	        <s:if test="#request.sites!=null">
																			  <%--  <s:select name="cdrCbuPojo.fkCbbId" listKey="siteId" listValue="siteName" list="#request.sites" disabled="true"></s:select> --%>
																				<s:iterator value="#request.sites" var="cbuunitreport" status="row">
																					<s:if test="siteId==cdrCbuPojo.fkCbbId">
																					<s:property value="siteName" />
																					</s:if>
																				</s:iterator>
																			</s:if>
																	     </td>																     
																	  </tr>
																	  <tr>																  
																	      <td>
																	         <s:text name="garuda.productinsert.label.address" />
																	      </td>
																	      <td>
																	         <s:property value="cbbAddress.address1" />
																	      </td>																      
																	  </tr>
																	  <tr>
																	     <td>
																	         <s:text name="garuda.cbbdefaults.lable.country" />
																	      </td>
																	      <td>
																	         <s:property value="cbbAddress.country" />
																	      </td>																  
																	  </tr>
																	  <tr>
																	     <td>
																	         <s:text name="garuda.fdoe.level.eligible" />
																	      </td>
																	      <td>
																	         <input type="checkbox" disabled="disabled" name="eligibledeclfdoe" <s:if test="cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.fkCordCbuEligibleReason">checked="checked"</s:if> ></input>
																	      </td>		
																	  </tr>
																	  <tr>
																	     <td>
																	        <s:text name="garuda.fdoe.level.incomplete" />
																	     </td>
																	     <td>
																	        <input type="checkbox" disabled="disabled" name="incompletedeclfdoe" <s:if test="cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.incompleteReasonId">checked="checked"</s:if> ></input>
																	     </td>
																	  </tr>
																	  <tr>
																	     <td>
																	        <s:text name="garuda.fdoe.level.incomplete_ques1" />
																	     </td>
																	     <td>
																	        <input type="checkbox" disabled="disabled" name="incompletedeclfdoe" <s:if test="cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.incompleteReasonId">checked="checked"</s:if> ></input>
																	     </td>
																	  </tr>
																	  <tr>
																	     <td>
																	        <s:text name="garuda.fdoe.level.incomplete_ques2" />
																	     </td>
																	     <td>
																	        <input type="checkbox" disabled="disabled" name="incompletedeclfdoe" <s:if test="cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.incompleteReasonId">checked="checked"</s:if> ></input>
																	     </td>
																	  </tr>
																	  <tr>
																	     <td>
																	         <s:text name="garuda.fdoe.level.ineligible" />
																	      </td>
																	      <td>
																	         <input type="checkbox" disabled="disabled" name="ineligibledeclfdoe" <s:if test="declPojo.mrqQues1b==false">checked="checked"</s:if> ></input>
																	      </td>		
																	  </tr>
																	  <tr>
																	     <td>
																	        <s:text name="garuda.fdoe.level.notapplicable" />
																	     </td>
																	     <td>
																	        <input type="checkbox" disabled="disabled" name="notapplideclfdoe" <s:if test="declPojo.mrqQues1b==true">checked="checked"</s:if> ></input>
																	     </td>
																	  </tr>
																	</table>
																</div>	
																<h3><a href="#"><s:text name="garuda.common.lable.sectiontwo"/></a></h3>
																<div>
																	<fieldset >
									   									<legend> 
									   										<s:text name="garuda.fdoe.level.sectionformname1"></s:text>
																		</legend>
																	    <table width="100%" cellspacing="0" cellpadding="0">
																	       <tr>
																		       <td>
																		         <s:text name="garuda.fdoe.level.sectionformname1_ques1" />
																		       </td>
																		       <td>
																		         <input type="checkbox" disabled="disabled" name="sectionformname1_ques1" <s:if test="declPojo.idmQues7a==false">checked="checked"</s:if> ></input>
																		       </td>
																	       </tr>
																	       <tr>
																		       <td>
																		         <s:text name="garuda.fdoe.level.sectionformname1_ques2" />
																		       </td>
																		       <td>
																		         <input type="checkbox" disabled="disabled" name="sectionformname1_ques2" <s:if test="declPojo.idmQues6==false">checked="checked"</s:if> ></input>
																		       </td>
																	       </tr>
																	       <tr>
																		       <td>
																		         <s:text name="garuda.fdoe.level.sectionformname1_ques3" />
																		       </td>
																		       <td>
																		         <input type="checkbox" disabled="disabled" name="sectionformname1_ques3" <s:if test="declPojo.idmQues8==true">checked="checked"</s:if> ></input>
																		       </td>
																	       </tr>
																	       <tr>
																		       <td>
																		         <s:text name="garuda.fdoe.level.sectionformname1_ques4" />
																		       </td>
																		       <td>
																		         <input type="checkbox" disabled="disabled" name="sectionformname1_ques4" ></input>
																		       </td>
																	       </tr>
																	    </table>
																	</fieldset>
																	<fieldset >
									   									<legend> 
									   										<s:text name="garuda.fdoe.level.sectionformname2"></s:text>
																		</legend>
																	    <table width="100%" cellspacing="0" cellpadding="0">
																	       <tr>
																		       <td>
																		         <s:text name="garuda.fdoe.level.sectionformname2_ques1" />
																		       </td>
																		       <td>
																		         <input type="checkbox" disabled="disabled" name="sectionformname2_ques1" <s:if test="declPojo.mrqQues2==true">checked="checked"</s:if> ></input>
																		       </td>
																	       </tr>
																	       <tr>
																		       <td>
																		         <s:text name="garuda.fdoe.level.sectionformname2_ques2" />
																		       </td>
																		       <td>
																		         <input type="checkbox" disabled="disabled" name="sectionformname2_ques2" <s:if test="declPojo.phyFindQues5==true">checked="checked"</s:if> ></input>
																		       </td>
																	       </tr>
																	       <tr>
																		       <td>
																		         <s:text name="garuda.fdoe.level.sectionformname2_ques3" />
																		       </td>
																		       <td>
																		         <input type="checkbox" disabled="disabled" name="sectionformname2_ques3" <s:if test="declPojo.phyFindQues4==true">checked="checked"</s:if> ></input>
																		       </td>
																	       </tr>
																	       <tr>
																		       <td>
																		         <s:text name="garuda.fdoe.level.sectionformname2_ques4" />
																		       </td>
																		       <td>
																		         <input type="checkbox" disabled="disabled" name="sectionformname2_ques4" <s:if test="declPojo.mrqQues2==false">checked="checked"</s:if> ></input>
																		       </td>
																	       </tr>
																	       <tr>
																		       <td>
																		         <s:text name="garuda.fdoe.level.sectionformname2_ques5" />
																		       </td>
																		       <td>
																		         <input type="checkbox" disabled="disabled" name="sectionformname2_ques5" ></input>
																		       </td>
																	       </tr>
																	    </table>
																	</fieldset>
																</div>	
																<h3><a href="#"><s:text name="garuda.common.lable.sectionthree"/></a></h3>
																<div>
																	 <fieldset>
									   									<legend> 
									   										<s:text name="garuda.fdoe.level.sectionformname3"></s:text>
																		</legend>
																	    <table width="100%" cellspacing="0" cellpadding="0">
																	     <tr>
																	         <td><s:text name="garuda.fdoe.level.name" /></td>
																	         <td></td>
																	         <td><s:text name="garuda.fdoe.level.signature" /></td>
																	         <td></td>
																	         <td><s:text name="garuda.fdoe.level.date" /></td>
																	         <td></td>
																	     </tr>
																	     <tr>
																	        <td colspan="6">
																	        </td>
																	     </tr>
																	     <tr>
																	        <td colspan="3">
																	           <fieldset>
												   									<legend> 
												   										<s:text name="garuda.fdoe.level.req"></s:text>
																					</legend>
																				    <table width="100%" cellspacing="0" cellpadding="0">
																				       <tr>
																				           <td>
																				              <s:text name="garuda.fdoe.level.estab"></s:text>
																				           </td>
																				           <td>
																				              <input type="checkbox" disabled="disabled" name="estabfdoe" />
																				           </td>
																				       </tr>
																				       <tr>
																				           <td>
																				              <s:text name="garuda.fdoe.level.compestab"></s:text>
																				           </td>
																				           <td>
																				              <input type="checkbox" disabled="disabled" name="compestabfdoe" />
																				           </td>
																				       </tr>
																				       <tr>
																				           <td>
																				              <s:text name="garuda.fdoe.level.nmdpadd"></s:text>
																				           </td>
																				           <td>
																				              <input type="checkbox" disabled="disabled" name="nmdpaddfdoe" />
																				           </td>
																				       </tr>
																				    </table>
																			   </fieldset>
																	        </td>
																	        <td colspan="3">
																	           <table width="100%" cellspacing="0" cellpadding="0">
																	              <tr>
																	                  <td><s:text name="garuda.fdoe.level.estabname" /></td>
																	              </tr>
																	              <tr>
																	                  <td></td>
																	              </tr>
																	              <tr>
																	                  <td><s:text name="garuda.fdoe.level.estabadd" /></td>
																	              </tr>
																	           </table>
																	        </td>
																	     </tr>
																	    </table>
																	 </fieldset>
																</div>													
														  </div>													 
												</div>
									        </td>
									    </tr>
									  </table>
								</s:if>