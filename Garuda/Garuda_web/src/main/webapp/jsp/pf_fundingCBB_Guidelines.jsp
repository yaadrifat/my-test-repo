<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:useBean id ="paginateSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginationSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<script>
$j(function(){
	
	$j("#cbuFundingGuidelines").validate({
	rules:{
		"cbbId":{
				minlength:3
			},
			"cbbName":{
				minlength:3
			}
		},
	 messages:{
		"cbbId":{            
		    minlength : "<s:text name="garuda.cbu.cordentry.inputIdminlength"/>"
			},
		"cbbName":{
            minlength : "<s:text name="garuda.cbu.cordentry.inputIdminlength"/>"
			}
	 	}
	});
	
});


</script>
<s:form id="cbuFundingGuidelines" action="" method="post">
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
					  <tr>
						<td >
							<s:textfield name="cbbId" id="cbbIdForFund" placeholder="CBB ID" size="20px"
							onkeypress="return entersRefreshdiv(event);" />
					    </td>
					    <td >
						 	<s:textfield name="cbbName" id="cbbNameForFund" placeholder="CBB Name" size="20px"
							onkeypress="return entersRefreshdiv(event);" />
						</td>
						<td>
							<input type="button" onclick="javascript:showCBB();" value="<s:text name="garuda.opentask.label.search" />"/>
						</td>
					 </tr>
					  <tr>
					      <td colspan="3">		
							   <div id="cbbdefaultparent" class="portlet-content" style="display: none">
							   	<input type="hidden" name="entries" id="entries" value=<%=paginateSearch.getiShowRows() %> />					
										<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displaycdr" id="cbbguidlinesdt1" >
											<thead>
												<tr>
												    <th ><s:text name="garuda.pendingorderpage.label.cddid"/></th>
													<th ><s:text name="garuda.cbbdefaults.lable.cbbname"/></th>			
												</tr>
											</thead>
											<tbody>
												<s:iterator value="#request.CbbList" status="row">
													<tr id="<s:property	value="%{#row.index}"/>" onclick="showSelectedCBBGuidlines('<s:property value="siteId"/>','<s:property	value="%{#row.index}"/>');" onMouseOver="highlight('<s:property	value="%{#row.index}"/>')" onmouseout="removeHighlight('<s:property	value="%{#row.index}"/>')">
													    <td><s:property	value="siteIdentifier" /></td>
										                <td><s:property	value="siteName" /></td>               
										           </tr>
												</s:iterator>
												<s:if test="#request.CbbList.size()==1">
													<script>
														showSelectedCBBGuidlines('<s:property value="#request.CbbList[0].siteId"/>','0');
													</script>
												</s:if>
											</tbody>
											<tfoot>
												<tr>
													<td colspan="2"></td>
												</tr>
												<tr>
												   <td>
												    <jsp:include page="paginationFooter.jsp">
													  	<jsp:param value="cbbdefaultparent" name="divName"/>
													  	<jsp:param value="cbuFundingGuidelines" name="formName"/>
													  	<jsp:param value="showsRow" name="showsRecordId"/>
													  	<jsp:param value="getCbbDetailsForFunding" name="url"/>
													  	<jsp:param value="temp" name="cbuid"/>
													  	<jsp:param value="fundingPaginateTable" name="bodyOnloadFn"/>
													  	<jsp:param value="cbbIdForFund" name="cbbIdForFund"/>
													  	<jsp:param value="cbbNameForFund" name="cbbNameForFund"/>
													  	<jsp:param value="parmsForFund" name="flag"/>
												    </jsp:include>
									               </td>
									               <td></td>
									            </tr>
											</tfoot>
										</table>
							    </div>							
					     </td>
					 </tr>
					 <tr>
					   <td colspan="3">
					       	<div id="addFundingGuidlines" style="display: none">
					  			<jsp:include page="pf_funding_guidelines.jsp"/>
					  		</div>
					  </td>
				</tr>
		  </table></s:form>