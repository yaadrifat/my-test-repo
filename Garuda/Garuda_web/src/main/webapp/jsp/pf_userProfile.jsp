<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="paginateSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<%
HttpSession tSession = request.getSession(true);
String accId = null;
String logUsr = null;
if (sessionmaint.isValidSession(tSession))
{
	accId = (String) tSession.getValue("accountId");
}
%>
<script>

	function constructTable() {
 		$j('#searchResults').dataTable({
 			"bRetrieve": true,
 			"iDisplayLength": 1000
 		});
 		 $j('#searchResults_info').hide();
 		$j('#searchResults_paginate').hide();
 		//$j('#searchResults_length').hide();
 		$j('#searchResults_length').empty();
 		$j('#searchResults_length').replaceWith('Show <select name="showsRow" id="showsRow" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+$j("#totalNoRows").val()+'">All</option></select> Entries');
 		if($j('#entries').val()!=null || $j('#entries').val()!=""){
			$j('#showsRow').val($j('#entries').val());
			}
 		$j('#showsRow').change(function(){
 			paginationFooter(0,"getUserDetails,showsRow,temp,searchUserInfo,searchTble,constructTable");
 		});
	} 

 function showUserDetails(formname,userId){
	$j('#userId').val(userId);
	document.usersearch.submit();
		}
		
	 function showDiv(val)
	 {
		$j('#usersearchresultparent').css('display','block');
		refreshDiv('getUserDetails?iPageNo='+val+'&iShowRows='+$j('#showsRow').val(),'searchTble','searchUserInfo');
	 }	

	 $j(document).ready(function(){
		 constructTable();
		 $j('#fkAccout').val($j('#accId').val());
		 
		 })
		 
	 
</script>

<div class="col_100 maincontainer "><div class="col_100">
<table width="100%" id="container1">
	<tr>
		<td>
		<div class="column">
		<div class="portlet" id="usersearchparent">
		<div id="usersearch"
			class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
		<div
			class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
		<span class="ui-icon ui-icon-minusthick"></span><!--<span
			class="ui-icon ui-icon-close"></span>--><s:text
			name="garuda.userprofile.label.usersearch" /></div>
		<div class="portlet-content">
		<div id="studyDashDiv1"><s:form id="searchUserInfo"
			method="post">
			<table cellpadding="0" cellspacing="0" border="0"
				style="width: 100%; height: auto;" class="col_100 ">
				<tr>
					<td><s:text name="garuda.assigntask.label.loginid" />:</td>
					<input type="hidden" name="" id="accId" value=<%=accId%>>
					<s:hidden name="UserPojo.fkAccount" id="fkAccout"></s:hidden>
					<td><s:textfield name="UserPojo.loginId" placeholder="LOGIN ID" onkeypress="return enter2Refreshdiv(event);" /></td>
					<td>
					<button type="button" id="search" onclick="showDiv('0');"><s:text
						name="garuda.uploaddoc.label.search" /></button>
					</td>
				</tr>
			</table>
		</s:form></div>
		</div>
		</div>
		</div>
		</div>
		</td>
	</tr>


	<tr>
		<td>
		<div class="column">

		<div class="portlet" id="usersearchresultparent"
			style="display: none;">
		<div id="usersearchresult"
			class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
		<div
			class="portlet-header ui-widget-header ui-widget-content ui-corner-all"><span
			class="ui-icon ui-icon-minusthick"></span><!--<span
			class="ui-icon ui-icon-close"></span>--><s:text
			name="garuda.cbuentry.label.cdrcbusearchresult" /></div>
		<div class="portlet-content">
<s:form id="usersearch" name="usersearch" action="showUserDetails">
<s:hidden name="userId" id="userId"></s:hidden>
		<div id="searchTble">
<input type="hidden" name="entries" id="entries" value=<%=paginateSearch.getiShowRows() %> />
		<table style="height: 100%" border="0" align="left" cellpadding="0"
			cellspacing="0" class="displaycdr" id="searchResults">


			<thead>
				<tr>
					<!--  <th class="th2" scope="col"><s:text
						name="garuda.unitreport.label.User_id" /></th>-->
					<th class="th3" scope="col"><s:text
						name="garuda.assigntask.label.loginid" /></th>
					<th class="th4" scope="col"><s:text
						name="garuda.assigntask.label.name" /></th>
					<th class="th5" scope="col"><s:text
						name="garuda.assigntask.label.mailid" />
						<input type="hidden" name="" id="totalNoRows" value=<%=paginateSearch.getiTotalRows() %>>
						</th>
				</tr>
			</thead>
			<tbody>
				<s:iterator value="lstObject" var="rowVal" status="row">
					<tr id="<s:property	value="%{#row.index}"/>" onMouseOver="highlight('<s:property	value="%{#row.index}"/>')" onmouseout="removeHighlight('<s:property	value="%{#row.index}"/>')" onclick="javaScript:return showUserDetails('usersearch','<s:property value="%{#rowVal[0]}"/>')">
						<!-- <td><s:property	value="userId" /></td> -->
						<td><s:property	value="%{#rowVal[1]}" /></td>
						<td><s:property value="%{#rowVal[2]}" />&nbsp;<s:property value="%{#rowVal[3]}" /></td>
						<td><s:property	value="%{#rowVal[4]}" /></td>
					</tr>

				</s:iterator>
			</tbody>
			<tfoot>
				<tr><td colspan="3">
				
		  <jsp:include page="paginationFooter.jsp">
		  	<jsp:param value="searchTble" name="divName"/>
		  	<jsp:param value="searchUserInfo" name="formName"/>
		  	<jsp:param value="showsRow" name="showsRecordId"/>
		  	<jsp:param value="getUserDetails" name="url"/>
		  	<jsp:param value="constructTable" name="bodyOnloadFn"/>
		  </jsp:include>
    </td>
    
    </tr>
			</tfoot>
			
		</table>

		</div>
		</s:form>
		</div>

		</div>

		</div>
		</div>
		</td>
	</tr>

</table>

</div></div>