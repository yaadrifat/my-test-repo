<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:useBean id ="paginateSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginationSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>

<script>
$j(document).ready(function(){
	$j( "input:submit, button", ".column" ).button();   	
	manageProfile();
   	if($j('#vflag').val()!=null && $j('#vflag').val()!="" ){
   	   	var pkid=$j('#vpksite').val();
   	 	$j("#cbbdefaultparent").css('display','none');
   	 	loadPageByGetRequset('showCBBDefaults?cbbDefault='+pkid,'cbbform');     
   	}
});  
function fn_ResetCbbSort(){
if($j('#prof_tbl_sort_col').val()!=undefined && $j('#prof_tbl_sort_col').val()!=null && $j('#prof_tbl_sort_col').val()!=""){
		
		$j('#prof_tbl_sort_col').val('');
		$j('#prof_tbl_sort_type').val('');
		var oTable = $j('#searchResults').dataTable();
		oTable.fnSort( [ [0,'asc'] ] );
		oTable.fnDraw();
	}
}
    
  function showSelectedCBB(id,rowid){
	  $j("#"+rowid).css('background-color','blue');
	  loadPageByGetRequset('showCBBDefaults?cbbDefault='+id,'cbbform');      
  }  

  function showSelectedCBBByRefresh(id){
	 loadPageByGetRequset('showCBBDefaults?cbbDefault='+id,'cbbform');      
  }  

  function showAddWidget(url){
		 url = url + "?pageId=20" ; 
		 showAddWidgetModal(url);
	 }

  function printCbbDefaults(id){
	  var url = cotextpath+"/jsp"+"/printCBBDefaults.action?site.siteId="+id;
	 	window.open(url);
  } 

  function manageProfile(){
			
			var is_all_entries=$j('#showsRow').val();
			var pageNo=$j('#paginateWrapper1').find('.currentPage').text();
			var showEntries=$j('#prof_tbl_sentries').val();
			var tEntries=$j('#tentries').val();
			var sort=0;
			var sortDir="asc";
			var SortParm=$j('#prof_tbl_sort_col').val();
			var seachTxt="";
			var preTxt=$j('#prof_search_txt').val();
			
			if((preTxt!=null && $j.trim(preTxt)!="")){
				seachTxt=preTxt;
			}
			
			
			//alert("showEntries::::"+showEntries)

			if(SortParm!=undefined && SortParm!=null && SortParm!=""){
				sort=$j('#prof_tbl_sort_col').val();
				sortDir=$j('#prof_tbl_sort_type').val();
			}	
			

			if((showEntries==null || showEntries==undefined || showEntries=="undefined" || $j.trim(showEntries)=="")){
				is_all_entries='5';
				$j('#prof_tbl_sentries').val(is_all_entries);
				showEntries=$j('#prof_tbl_sentries').val();
			}
			
			showEntriesTxt='Show <select name="showsRow" id="showsRow" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+tEntries+'" >ALL</option></select> Entries';
			
		    if(showEntries=="ALL"){
		    	
		    	$j('#searchResults').dataTable({
		            "sScrollY": "150",
		            "sDom": "frtiS",
		            "sAjaxSource": 'getJsonPagination.action?TblFind=cbbProfile&allEntries=ALL',
		     		"bServerSide": true,
		     		"bProcessing": true, 
		     		"aaSorting": [[ sort, sortDir ]],
		     		"oSearch": {"sSearch": seachTxt},
		     		"bRetrieve" : true,
		     		"bDeferRender": true,
		     		"bDestroy": true,
		     		"sAjaxDataProp": "cbbList",
		     		"aoColumnDefs": [
									{		
										  "aTargets": [0],"bSortable": false,"mDataProp": function ( source, type, val ) {
										   return "<input type='hidden' name='hpksite' class='pksiteHid' value='"+source.siteId+"' id='hpksite'></input>"+"<input type='hidden' name='hsiteName' class='siteNameHid' value='"+source.siteName+"' id='hsiteName'></input>";
									}},
			  						{		
			  							  "aTargets": [1],"mDataProp": function ( source, type, val ) {
			  		                	   return source.siteIdentifier;
			  		                }},
			  		                {
			  		                 	   "aTargets": [2],"mDataProp": function ( source, type, val ) {
										    return source.siteName;
					                }}
		  						],
		  						"fnDrawCallback": function() { 
		  							$j('#searchResults_wrapper').find('.dataTables_scrollFoot').hide();
								    $j('#searchResults_info').show();
								    $j("#showsRowsearchResults option:contains('ALL')").attr('selected', 'selected'); 	
								    setSortData('profTblThead','prof_tbl_sort_col','prof_tbl_sort_type');
								    frameOnclick();
		  						}
				});
		    	$j('#searchResults_filter').before(showEntriesTxt);    	
		    	
		    }else{
		    	
		    	$j('#searchResults').dataTable({
		            "sScrollY": "150",
		            "sAjaxSource": 'getJsonPagination.action?TblFind=cbbProfile&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries,
		            "bServerSide": true,
		            "bProcessing": true, 
		     		"bRetrieve" : true,
					"bDestroy": true,
					"oSearch": {"sSearch": seachTxt},
		     		"aaSorting": [[ sort, sortDir ]],
		     		"bDeferRender": true,
		     		"sAjaxDataProp": "cbbList",
		     		"aoColumnDefs": [
										{		
											  "aTargets": [0],"bSortable": false,"mDataProp": function ( source, type, val ) {
											   return "<input type='hidden' name='hpksite' class='pksiteHid' value='"+source.siteId+"' id='hpksite'></input>"+"<input type='hidden' name='hsiteName' class='siteNameHid' value='"+source.siteName+"' id='hsiteName'></input>";
										}},
										{		
											  "aTargets": [1],"mDataProp": function ( source, type, val ) {
											   return source.siteIdentifier;
										}},
										{
												"aTargets": [2],"mDataProp": function ( source, type, val ) {
												return source.siteName;
										}}
		  						],
		  						"fnDrawCallback": function() { 
		  							setSortData('profTblThead','prof_tbl_sort_col','prof_tbl_sort_type');
		  							frameOnclick();
		  						}
				});
		    	
		    }
			
			
			$j('#searchResults_info').hide();
			$j('#searchResults_paginate').hide();
			$j('#searchResults_length').empty();
			$j('#searchResults_length').replaceWith(showEntriesTxt);

			$j('#showsRow').change(function(){
				var textval=$j('#showsRow :selected').html();
				$j('#prof_tbl_sentries').val(textval);
				paginationFooter(0,"getCbbDetails,showsRow,inputs,Tableform,searchTble,manageProfile,prof_search_txt");
			});	
			
			$j('#searchResults_filter').find(".searching1").keyup(function(e){
				  var val = $j(this).val();
				  val=$j.trim(val);
				  $j('#prof_search_txt').val(val);
				  if(e.keyCode==13)
				  {
						  paginationFooter(0,"getCbbDetails,showsRow,inputs,Tableform,searchTble,manageProfile,prof_search_txt");
				  }
			});

	}
   function frameOnclick(){
	   
	   
	   var chk_arr1 = document.getElementsByName("hpksite");
	   var chklength1 = chk_arr1.length;  
	   for(k=0;k< chklength1;k++)
	   {
	       chk_arr1[k].id = 'hpksite'+k;
	   } 
	   
	   var chk_arr2 = document.getElementsByName("hsiteName");
	   var chklength2 = chk_arr2.length;  
	   for(k=0;k< chklength2;k++)
	   {
	       chk_arr2[k].id = 'hsiteName'+k;
	   } 
	   
	   

	   var table=document.getElementById('searchResults');
	   var rows = document.getElementById('profTblTBody').getElementsByTagName('TR');
	   var chklength3 = rows.length;  

	   for (k=0;k< chklength3;k++) {
		   rows[k].id = chk_arr1[k].value;
	   }
	   
	   
	   var tTxt1='<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBB" />';
	   var tTxt2='<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_CBB" />';
	   var tempval1="";
	   var tempval2="";
	   var index="";
	   var url="";
	   
	   $j('#profTblTBody').find('tr').each(function(i){
		   				
		             $j(this).bind("click",function(){
		            	tempval1=$j('#hpksite'+i).val();
		     		    tempval2=$j('#hsiteName'+i).val();
		     		    index=i;
		     		    url=tempval1+"&moduleEntityId="+tempval1+"&moduleEntityType="+tTxt1+"&moduleEntityIdentifier="+tempval2+"&moduleEntityEventCode="+tTxt2;
		            	showSelectedCBB(url,index);
		             });
		             $j(this).mouseover(function(i,el){
		            	 var temp=$j(this).attr('id');
		            	 highlight(temp); 
		             });
		             $j(this).mouseout(function(i,el){
		            	 var temp1=$j(this).attr('id');
		            	 removeHighlight(temp1); 
		             });
	   });
	   
	   
	   
   }
  function constructTable() {
  }

  function fn_setSort_ord(label,el){
		var sort_to="";
		var sorted=false;
		$j("#ord_header").val(label);
		var pagno=$j('#ordTblFoot div:first').find('b').find('.currentPage').text();
		if($j(el).hasClass('sorting') || $j(el).hasClass('sorting_desc')){
			sort_to='asc';
			var temp_sort_Param='&orderBy='+label+' '+sort_to;
			$j('#ord_sortParam').val(temp_sort_Param);
			$j("#ord_sort_type").val('sorting_asc');
			paginationFooter(0,"getCbbDetails,showsRow,inputs,Tableform,searchTble,manageProfile,ord_sortParam");
			sorted=true;
		}
		if($j(el).hasClass('sorting_asc') && !sorted){
			sort_to='desc';
			var temp_sort_Param='&orderBy='+label+' '+sort_to;
			$j('#ord_sortParam').val(temp_sort_Param);
			$j("#ord_sort_type").val('sorting_desc');
			paginationFooter(0,"getCbbDetails,showsRow,inputs,Tableform,searchTble,manageProfile,ord_sortParam");
			sorted=true;
		}
	}
</script>

<s:hidden name="prof_search_txt" id="prof_search_txt"/>
<s:hidden name="prof_tbl_sentries" id="prof_tbl_sentries"/>
<s:hidden name="prof_tbl_sort_col" id="prof_tbl_sort_col"/>
<s:hidden name="prof_tbl_sort_type" id="prof_tbl_sort_type"/>
<s:hidden name="ord_sortParam" id="ord_sortParam"/>
<s:hidden name="ord_sort_type" id="ord_sort_type"/>
<s:hidden name="ord_header" id="ord_header"/>
<s:form name="Tableform" id="Tableform">
</s:form>
<s:hidden name="flag" id="vflag"></s:hidden>
<s:hidden name="pksite" id="vpksite"></s:hidden>
<div class="column">
	<div class="portlet" id="cbbdefaultparent">
	<div id="cbbdefault" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ><div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"><span class="ui-icon ui-icon-minusthick"></span><!--<span class="ui-icon ui-icon-close" ></span>--><s:text name="garuda.cbbdefaults.lable.cbbprofile" /></div><div class="portlet-content">
	<div id="searchTble">
	<input type="hidden" name="entries" id="entries" value=<%=paginateSearch.getiShowRows() %>   />
	<input type="hidden" name="tentries" id="tentries" value=<%=paginateSearch.getiTotalRows() %> />
	<div align="right"><a href="#" onclick="fn_ResetCbbSort()"  id="resetSortLookup"><s:text name="garuda.pendingorderpage.label.resetsort"/></a></div><br>
<table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="searchResults" >
	<thead>
		<tr id="profTblThead">
		   <th width="0%"></th>
		    <th  width="35%" class="sorting siteIdentifier" ><s:text name="garuda.pendingorderpage.label.cddid"/></th>
			<th width="65%" class="sorting siteName"><s:text name="garuda.cbbdefaults.lable.cbbname"/></th>			
		</tr>
	</thead>
	<tbody id="profTblTBody">
		<s:iterator value="#request.CbbList" status="row">
			<tr id="<s:property	value="%{#row.index}"/>" onclick="showSelectedCBB('<s:property value="siteId"/>&moduleEntityId=<s:property value="siteId"/>&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBB" />&moduleEntityIdentifier=<s:property value="%{siteName}" escapeJavaScript="true"/>&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_CBB" />','<s:property	value="%{#row.index}"/>');" onMouseOver="highlight('<s:property	value="%{#row.index}"/>')" onmouseout="removeHighlight('<s:property	value="%{#row.index}"/>')">
			   <td></td>
			    <td><s:property	value="%{siteIdentifier}" /></td>
                <td><s:property	value="%{siteName}" /></td>               
           </tr>
		</s:iterator>
	</tbody>
	<tfoot>
			<tr id="ordTblFoot">
			   <td colspan="3">
			    <jsp:include page="paginationFooter.jsp">
				  	<jsp:param value="searchTble" name="divName"/>
				  	<jsp:param value="Tableform" name="formName"/>
				  	<jsp:param value="showsRow" name="showsRecordId"/>
				  	<jsp:param value="getCbbDetails" name="url"/>
				  	<jsp:param value="inputs" name="cbuid"/>
				  	<jsp:param value="workflow" name="paginateSearch"/>
				  	<jsp:param value="manageProfile" name="bodyOnloadFn"/>
				  	<jsp:param value="1" name="idparam"/>
				  	<jsp:param value="prof_search_txt" name="prof_search_txt"/>
			    </jsp:include>
               </td>
            </tr>
		</tfoot>
</table>
</div></div></div></div></div>
<div id="cbbform">
   <jsp:include page="cb_show-cbb-defaults.jsp"></jsp:include>
</div>