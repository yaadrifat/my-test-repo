<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<script>
$j(document).ready(function(){
		var idmgrp=$j('#pkidmgrp').val();
		$j('#labgrp').val(idmgrp);
		getlabtest(idmgrp);
	});
function checkUnique(obj,tdId,id2){
	var val=$j(obj).val().toLowerCase();
	var txtid=$j(obj).attr('id');
	$j('#'+id2+' select option').each(function(index,el) {
		if($j(el).text().trim().toLowerCase()==val){
			var error='<span class="error" id="'+tdId+'_err"><s:text name="garuda.cbu.label.labtesterror1"/></span>';
			$j('#'+txtid).val("");
			$j('#'+tdId).append(error);
			return false;
		}else{
			$j('#'+tdId+'_err').remove();
		}
	});
}
function checknull(){
	var labtestname=$j('#labtestName').val();
	var shrtName=$j('#shrtName').val();
	if(labtestname!=null && labtestname!="" && shrtName!=null && shrtName!=""){
		$j('#testName_err').hide();
		$j('#shrtName_err').hide();
		return true;
	}else{
			if(labtestname==null || labtestname==""){
				$j('#testName_err').show();
			}else{
				$j('#testName_err').hide();
			}
			if(shrtName==null || shrtName==""){
				$j('#shrtName_err').show();
			}else{
				$j('#shrtName_err').hide();
			}
		}
	return false;
}
function saveLabTest(){
	if(checknull()){
		refreshDiv('savelabTest','lTName','labTest');
		$j('#updatelabtest').show();
		$j('#addLTest').hide();
		$j('#labTestDiv').hide();
	}
}
//var ltestlist1 = new Array();
var flag=0;
function getMultiple(ob) {
	var idmnewTest=document.getElementsByName('idmnewTest');
	var selectedArray = new Array();
	var selectedId = new Array();
	var selObj =ob;
	var i;
	var count = 0;
	for (i=0; i<selObj.options.length; i++) {
	  if (selObj.options[i].selected) {
	    selectedArray[count] = selObj.options[i].value;
	    selectedId[count]=selObj.options[i].text;
	    count++;
	    selObj.options[i].selected=true;
	  }
	}
	//  ltestlist1 = selectedArray;
	//alert("selectedArray: "+selectedArray);
//	alert("selectedArray: "+idmnewTest.length);
	for(var i=0;i<idmnewTest.length;i++){
	//	alert("check1");
	//	alert("idmnewTest[ "+i+" ] = "+idmnewTest[i].value);
		for(var j=0;j<selectedArray.length;j++){
		//	alert("check2");
			//alert("selectedArray[ "+j+" ] = "+selectedArray[j]);
			if(selectedArray[j]==idmnewTest[i].value){
				var error=selectedId[j]+" <s:text name="garuda.cbu.label.labtesterror2"/>";
				$j("#Testerror").html(error);
				flag=1;
				return false;
			}else{
				$j("#Testerror").html("");
				flag=0;
			}
		}
	}
}

function closeaddTest(){
	var val=$j('#pkidmgrp').val();
	getlabtest(val);
	$j('#updatelabtest').hide();
	$j('#labTestDiv').show();
	$j('#lButtons').show();
}
function getlabtest(val){
	var val=$j('#pkidmgrp').val();
	refreshDiv('getlabTest','lTName','labTest');
	$j('#lTName').show();
}
function addLabTest(){
	//var idmlist=
	if(flag==0){
		submitLabTest('labTest','addnewlabtest','addittest');
		//callDate();
	}
//	alert("Selected val : "+ltestlist1.toString());  
}
function constructTable(){
	
}
function submitLabTest(formid,url,divname){
	if($j("#"+formid).valid()){
		showprogressMgs();
		/*var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'><s:text name="garuda.common.message.wait "/><img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
		var processingSaveDialog = document.createElement('div');
		processingSaveDialog.innerHTML=progressMsg;		
		$j(processingSaveDialog).dialog({autoOpen: false,
			resizable: false, width:400, height:90,closeOnEscape: false,
			modal: true}).siblings('.ui-dialog-titlebar').remove();		
		$j(processingSaveDialog).dialog("open");*/		
			$j.ajax({
		        type: "POST",
		        url: url,
		        data : $j("#"+formid).serialize(),
		        success: function (result){
		        	$j('.ui-datepicker').html("");
		        	$j('.ui-datepicker').html("");
		        	
		            var response=$j(result).find('#addittest');
		           // alert("html : "+$j(response).html());
		            var resultval=$j(response).html();
	            	$j("#"+divname).html(resultval);
	            	$j("#updatetest").css('display','block');
	            	$j("#teststatus").css('display','none');
	            	callDate();
		        },
		        error: function (request, status, error) {
		        	closeprogressMsg();
		        	alert("Error " + error);
		            alert(request.responseText);
		        }

			});	
			closeprogressMsg();
    }
}

function addLTest(){
	$j('#labtestName').val("");
	$j('#shrtName').val("");
	$j('#lTName').hide();
	$j('#lButtons').hide();
	$j('#addLTest').show();
}
function closeTest(){
	 $j("#showaddtestmodal").dialog("close");
	 $j("#showaddtestmodal").dialog("destroy");
}
</script>
<s:form id="labTest" name="labTest">
	<s:hidden name="pkidmgrp" id="pkidmgrp"></s:hidden>
	<s:hidden name="cdrCbuPojo.fkSpecimenId" id="fkSpecimenId" value="%{fkSpecimenId}"></s:hidden>

<s:if test="%{labtestlist1!=null && labtestlist1.size()>0}">
	<s:iterator value="labtestlist1" var="IdmTest" status="row">
		<s:hidden name="idmnewTest" value="%{IdmTest}" id="idmTest_%{#row.index}"></s:hidden>
	</s:iterator>
</s:if>

<div id="updatetest" style="display: none;" >
<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr >
		<td colspan="2">
			<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
			<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
			<strong><s:text name="garuda.common.lable.addnewtest" /> </strong></p>
			</div>
		</td>
	</tr>
	<tr><td></td>
		<td>
			<input type="button" onclick="closeModal()" value="<s:text name="garuda.common.close"/>" />
		</td>
	</tr>
</table>
</div>
<div id="teststatus">
	<div id="labTestDiv">
		<table width="100%">
			<tr>
			<td width="40%">
				<s:text name="garuda.cbu.label.TestGroup"></s:text>
			</td>
			<td width="60%">
				<select name="labgrp" id="labgrp" onchange="getlabtest(this.value);">
					<s:iterator value="%{labtestgrp}" var="row">
						<option value="<s:property value="%{#row[0]}"/>">
							<s:property value="%{#row[1]}"/>
						</option>
					</s:iterator>
				</select>
			</td>
			</tr>
			<tr id="lTName" style="display: none;">
			<td width="40%">
				<s:text name="Test Name :"></s:text>
			</td>
			<td id="testname" width="60%">
				<select name="labtestlist1" id="labtestlist1" multiple="multiple" style="height:150px" onblur='getMultiple(document.labTest.labtestlist1);'>
					<s:iterator value="%{labtestlist}" var="row">
						<option value="<s:property value="%{#row[0]}"/>">
							<s:property value="%{#row[1]}"/>
						</option>
					</s:iterator>
				</select>
				<span class="error" id="Testerror"></span>
			</td>
			<td style="visibility: hidden;" id="sname">
				<select name="labtestsname" id="labtestsname" style="display:none">
					<s:iterator value="%{labtestlist}" var="row">
						<option value="<s:property value="%{#row[0]}"/>">
							<s:property value="%{#row[2]}"/>
						</option>
					</s:iterator>
				</select>
			</td>
			</tr>
			<tr id="addLTest" style="display: none;">
				<td colspan="2">
					<table width="100%">
						<tr>
							<td width="40%"><s:text name="Test Name :"/></td>
							<td id="lname" width="60%">
								<s:textfield name="labtestName" id="labtestName" onchange="checkUnique(this,'lname','testname');"/>
								<br/><span class="error" id="testName_err" style="display:none"><s:text name="Please Enter the Test Name"/></span>
							</td>
						</tr>
						<tr>
							<td width="40%"><s:text name="Short Name :"/></td>
							<td id="ltshname" width="60%">
								<s:textfield name="shrtName" id="shrtName" onchange="checkUnique(this,'ltshname','sname');"/>
								<br/><span class="error" id="shrtName_err" style="display:none"><s:text name="Please Enter the Test Short Name"/></span>
							</td>
						</tr>
						<tr>
							<td colspan="2">
							<table width="100%" bgcolor="#cccccc">
							<tr>
								<td width="70%"><jsp:include page="./cb_esignature.jsp" flush="true"></jsp:include></td>
								<td><button type="button" id="submit" disabled="disabled" onclick="saveLabTest();"><s:text name="garuda.common.save"/></button></td>
								<td><button type="button" id="cancel" onclick="closeaddTest()"><s:text name="garuda.common.lable.cancel"/></button></td>
							</tr>
							</table>
							</td>
						</tr>
						
					</table>
				</td>
			</tr>
		</table>
	</div>
	<div id="lButtons">
		<table width="100%">
			<tr>
			<td rowspan="2">
				<button type="button" onclick="addLabTest();">
					<s:text name="garuda.label.dynamicform.addtest"></s:text>
				</button>
			</td>
			<td>
				<button type="button" onclick="addLTest();">
					<s:text name="garuda.cbu.label.Add_New_LabTest"></s:text>
				</button>
			</td>
			</tr>			
		</table>
	</div>
	<div id="updatelabtest" style="display: none;">
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr >
			<td colspan="2">
				<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
				<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
				<strong><s:text name="garuda.common.lable.addnewtest" /> </strong></p>
				</div>
			</td>
		</tr>
		<tr>
		<td>
		</td>
		<td>
			<input type="button" onclick="closeaddTest()" value="<s:text name="garuda.common.close"/>" />
		</td>
		</tr>
		</table>
	</div>
	</div>
</s:form>
