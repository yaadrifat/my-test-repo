<%@taglib prefix="s" uri="/struts-tags"%>
<script>
function formSubmit(){
	if($j("#mailform").valid()){ 
		modalFormSubmitRefreshDiv('mailform','sendMail','mail');
		$j(".mailupdate").show();
		$j(".mailtable").hide();
    }
}
function formAssessmentSubmit(){
	if($j("#mailform").valid()){ 
		modalFormSubmitRefreshDiv('mailform','sentCbuAssessmentMail','mail');
		$j(".mailupdate").show();
		$j(".mailtable").hide();
    }
}
$j(function(){
	$j("#mailform").validate({
			
		rules:{			
				"notificationPojo.notifyTo":"required"
			},
			messages:{
				"notificationPojo.notifyTo":"<s:text name="garuda.common.validation.mail"/>"
			}
		});
	});
</script>
<div id="mail">
<form id="mailform">
<s:hidden name="notificationPojo.notifyIsSent" value="false"></s:hidden>
<s:hidden name="notificationPojo.deletedFlag" value="false"></s:hidden>
<s:hidden name="notificationPojo.notifyId" ></s:hidden>
<s:hidden name="mailCode" ></s:hidden>
<s:hidden name="repId" ></s:hidden>
<s:hidden name="filterKeys" ></s:hidden>
<s:hidden name="filterValues" ></s:hidden>
<s:hidden name="cdrCbuPojo.cordID"></s:hidden>
<s:hidden name="cdrCbuPojo.fkCordCbuEligibleReason" ></s:hidden>
  <div id="update" style="display: none;" class="mailupdate">
		  <jsp:include page="cb_update.jsp">
		    <jsp:param value="Message Sent Successfully" name="message"/>
		  </jsp:include>	   
	</div>
    <table width="100%" cellspacing="0" cellpadding="0" border="0" id="status" class="mailtable">
    <tr>
      <td style="width: 250px;"><s:text name="garuda.entryTask.label.from"/>:</td>
      <td style="width: 300px;"><s:textfield name="notificationPojo.notifyFrom" ></s:textfield> </td>
    </tr>
    <tr>
      <td style="width: 250px;"><s:text name="garuda.entryTask.label.to"/>:</td>
      <td ><s:textfield name="notificationPojo.notifyTo" id="to"></s:textfield> </td>
     </tr>
    <tr>
      <td colspan="2"></td>
    </tr>
    <tr>
      <td style="width: 250px;"><s:text name="garuda.entryTask.label.subject"/>:</td>
      <td ><s:textfield name="notificationPojo.notifySubject" ></s:textfield> </td>
    </tr>
     <tr>
     <td style="width: 250px; vertical-align: top;"><s:text name="garuda.entryTask.label.content"/>:</td>
      <td ><s:textarea cols="50" rows="10" name="notificationPojo.notifyContent" escape="false"></s:textarea> </td>
    </tr>
    <tr>
      <td ></td>
      <s:if test="#request.module==@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_CBU_ASSESSMENT">
          <td><input type="button" onclick="formAssessmentSubmit()" value="<s:text name="garuda.entryTask.label.sendmail"/>" /></td>
      </s:if>
      <s:else>
          <td><input type="button" onclick="formSubmit()" value="<s:text name="garuda.entryTask.label.sendmail"/>" /></td>
      </s:else>      
    </tr>
  </table>
 </form>
</div>
<s:if test="sentMail==true">
  <script>
    $j(".mailupdate").show();
    $j(".mailtable").hide();
  </script>
</s:if>