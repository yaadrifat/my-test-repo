	<%@page contentType="text/html; charset=UTF-8"%>
	<%@taglib prefix="s" uri="/struts-tags"%>
	
<html>
<script type="text/javascript">

function saveCBUUnitReport(){
	$j.ajax({
		type : "POST",
		async : false,
		url : 'saveCBUUnitReport',
		data : $j("#CBUUnitRptForm").serialize(),
		success : function(data) {
			//showUnitReport("close");
		}
		
	});
} 
$j(function() {
	$j('#userSelection').html("");
	$j( "#datepicker" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
		changeYear: true});
	$j( "#datepicker1" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
		changeYear: true});
	
});

</script>
<body>
<div id="unitTable"  class="tabledisplay">
	<s:form action="" id="CBUUnitRptForm" name="CBUUnitRptForm" method="post" enctype="multipart/form-data" >
	<div >
  <table class="tabledisplay" align="center" border="0">
    <tbody>
      <tr>
        <td><h2><s:text name="garuda.entry.label.datasaved" /> </h2></td>
      </tr>
    </tbody>
  </table>
  </div>
</s:form></div>
</body>
</html>