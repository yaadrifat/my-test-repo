<%@ taglib prefix="s"  uri="/struts-tags"%>
<script>
$j(function() {
	/*$j( "#datepicker" ).datepicker({changeMonth: true,
		changeYear: true});*/
	getDatePic();
});


function constructTable() {
	  // alert("ready Volunteer")
		 $j('#searchResults').dataTable();
		 var tdObj = $j('#searchResults').find('tbody').find("tr:eq(0)").find("td:eq(0)");
			if($j(tdObj).hasClass('dataTables_empty')){
				$j('#searchResults_info').hide();
				$j('#searchResults_paginate').hide();
			}
	}

$j(document).ready(function(){

});

function checkSelectedUser(){
	
	var userList = document.getElementsByName('checkbox');
	var count=0;
	for(var i=0;i<userList.length;i++){
		if(userList[i].checked){
			count++;
			}
		}
		if(count==2){
			return true;
		}else{
			return false;
		}
}




 function gettingResult()
 {
	 var codeId=$j('#codeId').val();
	 var info=$j('#pkextinfo').val();
	 var isUserSelect = checkSelectedUser(); 
	if(isUserSelect){	
	// updateBreadcrumb('Data Specialists','loadpage(\'showtaskcdrId\')');
	 
	 //alert($j("#cdrcbuid").val());
	 var logId = $j('#loginIds').val();
	// alert("b4 : "+logId)
	 logId = logId.replace("'0',","");
	// alert("Af : "+logId)
	var loginIds = logId.split(",");
	
	//updateBreadcrumb('New Study','loadpage(\'getstudy\')');loadPage('getstudy');
	//updateBreadcrumb('Assign Task','loadPage("showtaskcdrId?loginIds="+loginIds+"&cdrcbuide="+$j(\'#cdrcbuid\').val())');
	//alert("loginIds : "+loginIds)
	//alert("false"); 
	//window.location="showtaskcdrId?loginIds="+loginIds+"&pkextinfo="+$j('#pkextinfo').val()+"&cdrcbuide="+$j('#cdrcbuid').val()+"&codeId="+codeId;
	submitpost('showtaskcdrId',{'loginIds':loginIds,'pkextinfo':$j('#pkextinfo').val(),'cdrcbuide':$j('#cdrcbuid').val(),'codeId':codeId});
	}else{
		document.getElementById("lbl1").innerHTML="<s:text name="garuda.cbu.label.cbb_signature"/>";
		//alert("Please select only two users...");
		}
 }

	
function getCheck(val,This){
//alert("----"+This.checked);
if(This.checked){
	
		var val1 = $j("#loginIds").val();
		if(val1=="" || val1==null){
			val1="'0'";
			}
		var newVal = val1+",'"+val+"'";
		//alert("newVal :: "+newVal);
		$j("#loginIds").val(newVal);
	}else{
		//alert("fsgg"+$j("#loginIds").val());
		 var checkIfRated = $j("#loginIds").val();
			if(checkIfRated.indexOf(val)){
				//alert("B4 : "+checkIfRated);
				//alert("true");
				checkIfRated = checkIfRated.replace(",'"+val+"'","");
				//alert("final : "+checkIfRated);
				$j("#loginIds").val(checkIfRated);
			}
		}
			

	}

	function showAddWidget(url){
		 url = url + "?pageId=10" ; 
		 showAddWidgetModal(url);
	}
</script>
<div class="col_100 maincontainer ">
<div class="col_100">
	<div class="portlet" id="taskresultparent">
		<div id="taskresult" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
				<span class="ui-icon ui-icon-minusthick"></span>
				<!--<span class="ui-icon ui-icon-close" ></span>-->
				<s:text name="garuda.assigntask.label.assignTask" />
			</div>
		<div class="portlet-content">
		<div id="cbuDashDiv1">
		
<div id="studyDashDiv1" class="col_35 left">
<form id="searchCordInfo" name="searchCordInfo" action="showtaskcdrId"><input type="hidden" id="loginIds" />
<s:hidden name="cdrcbuid" id="cdrcbuid" />
<s:hidden name="codeId" id="codeId" value="%{codeId}" />
----<s:property value="pkextinfo"/>
<s:hidden name="pkextinfo" id="pkextinfo" value="%{pkextinfo}" />
<div>
<fieldset>
 <div id="searchTble">
   <table border="0" align="center" cellpadding="0" cellspacing="0" class="display" id="searchResults">
	<thead>
		<tr>
			<th align="center"  style="width: 200px;"> </th>
			<th align="center"  style="width: 200px;"><s:text name="garuda.assigntask.label.name" /></th>
			<th align="center"  style="width: 200px;"><s:text name="garuda.assigntask.label.loginid" /></th>
			<th align="center"  style="width: 200px;"><s:text name="garuda.assigntask.label.mailid" /></th>

		</tr>
	</thead>
	<tbody>
	
	<s:iterator value="lstUser" var="rowVal" status="row">

		<tr >

			<td align="left"><s:checkbox name="checkbox" id="checkbox" onclick="getCheck('%{#rowVal[1]}',this)" /></td>
			<s:iterator value="rowVal" var="cellValue" status="col">
				<s:if test="%{#col.index == 0}">
					<td style="width: 200px;" align="left"><s:property /></td>
				</s:if>
				<s:if test="%{#col.index == 1}">
					<td style="width: 200px;" align="left"><s:property /></td>
				</s:if>
				<s:if test="%{#col.index == 2}">
					<td style="width: 200px;" align="left"><s:property /></td>
				</s:if>

			</s:iterator>
		</tr>

	</s:iterator>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="4"></td>
		</tr>
	</tfoot>
</table>
</div>

</fieldset>
</div>

<table cellpadding="0" cellspacing="0" align="center">
 <tr>
 <td>
  <label id="lbl1" style="color:#FF0000;"></label>
 </td>
 </tr>
</table>

<fieldset>
<button type="button" onclick="javascript:submitform('searchCordInfo')"><s:text name="garuda.assigntask.label.submit" /></button>
</fieldset>
</form>
</div>
	
</div></div></div></div>

</div>
</div>