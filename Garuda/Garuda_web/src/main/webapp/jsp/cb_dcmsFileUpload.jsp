<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript">
function getFileSelect(fileName){
	refreshDiv('saveAttachmentFile?attCodeType=ATTACHMENT_TYPE&attCodeSubType=UNIT_REPORT&codeEntityType=ENTITY_TYPE&codeEntitySubType=CBU&fileName='+fileName,'getAttachmentIdDiv','testDcmsForm');
	alert("after div refresh"+$j("#firstField").val());
	var attid=$j("#firstField").val();
	if(attid==null||attid==""){
	}
	else{ 
	onFileSelect('fileToUpload','testDcmsForm','firstField','FileId1');
	}	
}

function refreshDiv(url,divname,formId){
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data : $j("#"+formId).serialize(),
        success: function (result){
        	$j('.ui-datepicker').html("");
            var $response=$j(result);
            var errorpage = $jresponse.find('#errorForm').html();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
            	$j("#"+divname).replaceWith($j('#'+divname, $j(result)));
            }
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	

}

</script>
<s:form method="POST" id="testDcmsForm" name="testDcmsForm" enctype="multipart/form-data">
	<s:hidden name="targetFolder" value="1=0"></s:hidden>
	<s:hidden name="UploadFlags" value="1#"></s:hidden>	
	<s:div id="getAttachmentIdDiv">	
		<s:hidden name="garudaAttachmentId" id="firstField"></s:hidden>
		*******<s:property value="garudaAttachmentId"/>*******
	</s:div>
		<s:file id="fileToUpload" size="20" name="fileToUpload" onchange="getFileSelect(this.value);"></s:file>
		<s:hidden id="FileId1"></s:hidden>	
</s:form>