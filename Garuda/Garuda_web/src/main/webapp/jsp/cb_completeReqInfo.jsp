<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<jsp:useBean id="modCtlDao" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:include page="./cb_track_session_logging.jsp" />
<%
	HttpSession tSession = request.getSession(true); 
	GrpRightsJB grpRights =null;
	if (tSession.getAttribute("LocalGRights")!=null){
	 grpRights = (GrpRightsJB) tSession.getAttribute("LocalGRights");
	}else
	{
	 grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
	}
	int updateHistry_Mrq=0;
	int updateHistry_FMHQ=0;
	int cmpltReqInfo = 0;
	
	if(grpRights.getFtrRightsByValue("CB_MRQW")!=null && !grpRights.getFtrRightsByValue("CB_MRQW").equals(""))
	{	updateHistry_Mrq = Integer.parseInt(grpRights.getFtrRightsByValue("CB_MRQW")); }
	else
		updateHistry_Mrq = 4;
	
	if(grpRights.getFtrRightsByValue("CB_FMHQW")!=null && !grpRights.getFtrRightsByValue("CB_FMHQW").equals(""))
	{	updateHistry_FMHQ = Integer.parseInt(grpRights.getFtrRightsByValue("CB_FMHQW")); }
	else
		updateHistry_FMHQ = 4;
	if(grpRights.getFtrRightsByValue("CB_COMPREQINFO")!=null && !grpRights.getFtrRightsByValue("CB_COMPREQINFO").equals(""))
	{	cmpltReqInfo = Integer.parseInt(grpRights.getFtrRightsByValue("CB_COMPREQINFO"));}
	else
		cmpltReqInfo = 4;
	
	request.setAttribute("cmpltReqInfo",cmpltReqInfo);
	request.setAttribute("updateHistry_Mrq",updateHistry_Mrq);
	request.setAttribute("updateHistry_FMHQ",updateHistry_FMHQ);
%>
<%String contextpath = request.getContextPath()+"/jsp/"; %>
<script type="text/javascript">

function showrightdiv(val,functionName,divname,action,refreshingDiv){
	showprogressMgs();
	hideDiv();
	setTimeout(function() {
		$j.ajax({
			type: "POST",
			url: action, 
			async: false,
			success: function(result) {
				setTimeout(function() {
				$j('#'+refreshingDiv).html(result);
				},0);
				setTimeout(function() {
					window[functionName]();
					if(divname=='cbuInfoTargetDiv'){
						$j('#finalreviewmodel input[name=characteristicsEdit]').hide();
					}
					$j('#'+divname).css('display','block');
				},0);	
			},
			error: function(request, status, error) {
				closeprogressMsg();
				alert('Error calling Complete Required Information: '+error);
				alert(request.responseText);
			}
		});
	},0);
	closeprogressMsg();
}
function refreshPageDiv(url,divname,formId){
	if($j("#isCDRUser").val()=="true"){
		if($j('#idsFlag').val()==""){
			$j('#idsFlag').val("0");
		}
		if($j('#cbuInfoFlag').val()==""){
			$j('#cbuInfoFlag').val("0");
		}
		if($j('#licensureFlag').val()==""){
			$j('#licensureFlag').val("0");
		}
		if($j('#cbuProcessInfoFlag').val()==""){
			$j('#cbuProcessInfoFlag').val("0");
		}
		if($j('#labSummaryFlag').val()==""){
			$j('#labSummaryFlag').val("0");
		}
		if($j('#mrqFlag').val()==""){
			$j('#mrqFlag').val("0");
		}
		if($j('#idmFlag').val()==""){
			$j('#idmFlag').val("0");
		}
		//alert();
		if($j('#idsFlag').val()=="1" || $j('#licensureFlag').val()=="1" || $j('#cbuInfoFlag').val()=="1" || $j('#cbuProcessInfoFlag').val()=="1" || $j('#labSummaryFlag').val()=="1" || $j('#mrqFlag').val()=="1" || $j('#idmFlag').val()=="1"){
			submitCRI(url,divname,formId);
		}
	}
	else if($j("#isCDRUser").val()=="false"){
		if($j('#idsFlag').val()==""){
			$j('#idsFlag').val("0");
		}
		if($j('#licensureFlag').val()==""){
			$j('#licensureFlag').val("0");
		}
		//if($j('#eligibleFlag').val()==""){
			//$j('#eligibleFlag').val("0");
		//}

		if($j('#idsFlag').val()=="1" || $j('#licensureFlag').val()=="1"){
			submitCRI(url,divname,formId);
		}
	}
	
}

function submitCRI(url,divname,formId){
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data : $j("#"+formId).serialize(),
        success: function (result){
        	$j('.ui-datepicker').html("");
            var $response=$j(result);
            var errorpage = $response.find('#errorForm').html();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
            	$j("#"+divname).replaceWith($j('#'+divname, $j(result)));
			hideDiv();
			reloadingMainDiv(url);
            }
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }
	});
}
function reloadingMainDiv(){
	//var innerUrl=url.split("?");
	var orderId = $j("#orderId").val();
	var orderType = $j("#orderType").val();

	if(typeof(orderId)=="undefined"){
		$j("#orderId").val("");
		orderId="";
	}else if(orderId!=null && orderId.indexOf(",")!=-1){
		$j("#orderId").val("");
		orderId="";
	}
	if(typeof(orderType)=="undefined"){
		$j("#orderType").val("");
		orderType="";
	}else if(orderType!=null && orderType.indexOf(",")!=-1){
		$j("#orderType").val("");
		orderType="";
	}
	if(orderId!="" && orderType!=""){
		var mainUrl='loadOrderPageDivs?cdrCbuPojo.cordID='+$j("#cordId").val()+'&orderId='+orderId+'&orderType='+orderType+'&pkcordId='+$j("#cordId").val();
		//window.location=mainUrl;
		loadMoredivs(mainUrl,'viewcbutoppanel,quicklinksDiv,taskCompleteFlagsDiv,ctReqClinInfoContentDiv,chistory,cReqhistory','cbuIdInfo','update','status');
		//refreshDiv(mainUrl,'main','cbuCordInformationForm');
		//closeModals('dialogForCRI');
		setpfprogressbar();
		getprogressbarcolor();
		historyTblOnLoad();
	}else{
		//var mainUrl='cdrcbuView?'+innerUrl[1];
		var url = "loadClinicalData?cdrCbuPojo.cordID="+$j("#cordId").val();
		loadPageByGetRequset(url,'cordSelectedData');
	}
	//refreshDiv(mainUrl,'main','cbuCordInformationForm');
}

function submitReqClinInfo(){
	var orderId = $j("#orderId").val();
	var orderType = $j("#orderType").val();

	if(typeof(orderId)=="undefined"){
		$j("#orderId").val("");
		orderId="";
	}else if(orderId!=null && orderId.indexOf(",")!=-1){
		$j("#orderId").val("");
		orderId="";
	}
	if(typeof(orderType)=="undefined"){
		$j("#orderType").val("");
		orderType="";
	}else if(orderType!=null && orderType.indexOf(",")!=-1){
		$j("#orderType").val("");
		orderType="";
	}
	refreshPageDiv('submitCBUReqInfo?cdrCbuPojo.cordID='+$j("#cordId").val()+'&orderId='+orderId+'&orderType='+orderType+'&pkcordId='+$j("#cordId").val(),'finalreview','completeReqInfoForm');
}


function addIdsMandatory()
{
	
if($j('#sizess').val() > 0){
	$j('#additionalsTR').show();
}	
}
</script>
<script>
function cbuInfoMandatory(){
	 $j('#dialogForCRI .cbuInfoMandatory').each(function(){
		 $j(this).css("border", "1px solid #DBDAD9");
		 var fieldVal = $j(this).val();
		 fieldVal = $j.trim(fieldVal);
		 if(fieldVal==null || fieldVal=="" || fieldVal=="-1" ){
			 $j(this).css("border", "1px solid red");
			 $j('#cbuInfoFlag').val("0");
			}
		 else $j('#cbuInfoFlag').val("1");			 
		  });
	 $j('td.cbuProcMandatoryTD').each(function(){
		 if($j.trim($j(this).text())==""){
			 $j(this).css("border", "1px solid red");
			 $j('#cbuInfoFlag').val("0");
			 }
		 else $j('#cbuInfoFlag').val("1");
	 });
}

function cbuProcMandatory(){
	 $j('#dialogForCRI .cbuProcMandatory').each(function(){
		 $j(this).css("border", "1px solid #DBDAD9");
		 var fieldVal = $j(this).val();
		 fieldVal = $j.trim(fieldVal);
		 if(fieldVal==null || fieldVal=="" || fieldVal=="-1" ){
			 $j(this).css("border", "1px solid red");
			 $j('#cbuProcessInfoFlag').val("0");
			}
		 else $j('#cbuProcessInfoFlag').val("1");
		  });
}
function cbuLabSummMandatory(){
	 $j('#dialogForCRI .cbuLabSummMandatory').each(function(){
		 $j(this).css("border", "1px solid #DBDAD9");
		 var fieldVal = $j(this).val();
		 fieldVal = $j.trim(fieldVal);
		 if(fieldVal==null || fieldVal=="" || fieldVal=="-1" ){
			 $j(this).css("border", "1px solid red");
			 $j('#labSummaryFlag').val("0");
			}
		 else $j('#labSummaryFlag').val("1");
		  });
}


function formsMRQ(){
	$j('#mrqTargetDiv .isReqPriorToShip').each(function(){
		var id=$j(this).parent().parent().attr('id');
		if(id!="" && id!=null && id!='undefined'){
			if($j('#'+id).is(':visible')){
				var fieldVal = $j(this).val();
				 fieldVal = $j.trim(fieldVal);
				 if(fieldVal==null || fieldVal=="" || fieldVal=="-1" ){
					 $j('#mrqFlag').val("0");
				}else
					 $j('#mrqFlag').val("1");
			}	 
		}
	});
	if($j('#mrqFlag').val()==1){
		var len=$j('#dialogForCRI .assesReqPriorToShip').length;
			if(len!=0){
				$j('#mrqFlag').val("0");
			}else{
				$j('#mrqFlag').val("1");
			}
		}
}

function formsFMHQ(){
	$j('#fmhqTargetDiv .isReqPriorToShip').each(function(){
		var id=$j(this).parent().parent().attr('id');
		if(id!="" && id!=null && id!='undefined'){
			if($j('#'+id).is(':visible')){
				var fieldVal = $j(this).val();
				 fieldVal = $j.trim(fieldVal);
				 if(fieldVal==null || fieldVal=="" || fieldVal=="-1" ){
					 $j('#fmhqFlag').val("0");
				}else
					 $j('#fmhqFlag').val("1");
			}	 
		}
	});
	if($j('#fmhqFlag').val()==1){
		var len=$j('#dialogForCRI .assesReqPriorToShip').length;
			if(len!=0){
				$j('#fmhqFlag').val("0");
			}else{
				$j('#fmhqFlag').val("1");
			}
		}
}

function cbuIDM(){
	$j('#idmTargetDiv').find('.isReqPriorToShip').each(function(){
		var id=$j(this).parent().parent().attr('id');
		if(id!="" && id!=null && id!='undefined'){
			if($j('#'+id).is(':visible')){
				var fieldVal = $j(this).val();
				 fieldVal = $j.trim(fieldVal);
				 if(fieldVal==null || fieldVal=="" || fieldVal=="-1" ){
					 $j('#idmFlag').val("0");
				}else
					 $j('#idmFlag').val("1");
			}
		}
	});
	if($j('#idmFlag').val()==1){
		var len=$j('#dialogForCRI .assesReqPriorToShip').length;
		if(len!=0){
			$j('#idmFlag').val("0");
		}else{
			$j('#idmFlag').val("1");
		}
	}
}

function iDs(){
		if($j('#sizess').val() > 0){
			$j('#additionalsTR').show();
		}
	 $j('#dialogForCRI .idsMandatory').each(function(){
		 $j(this).css("border", "1px solid #DBDAD9");
	
		 var fieldVal = $j(this).val();
		 fieldVal = $j.trim(fieldVal);
		 if(fieldVal==null || fieldVal=="" || fieldVal=="-1" ){
			 $j(this).css("border", "1px solid red");
			 $j('#idsFlag').val("0");
			}
		 else $j('#idsFlag').val("1");
		  });
	  	
}
function licensure(){
	if($j('#cbuLicStatus').val()==""){
		$j('#cbuLicStatus').css("border", "1px solid red");
		$j('#licensureFlag').val("0");
	}else if($j('#cbuLicStatus').val()=='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@LICENCE_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@UNLICENSE_REASON)"/>'){
		if($j('#fkCordCbuUnlicenseReason').val() == null){
			$j('#fkCordCbuUnlicenseReason').css("border", "1px solid red");
			$j('#licensureFlag').val("0");
			}
		else{
			$j('#licensureFlag').val("1");
			}
	}else{
		$j('#licensureFlag').val("1");
	}
}
function CRIeligible(){
	if($j('#fkCordCbuEligible').val()==""){
		$j('#fkCordCbuEligible').css("border", "1px solid red");
		$j('#eligibleFlag').val("0");
	}else if($j('#fkCordCbuEligible').val()=='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@INELIGIBLE_REASON)"/>'||$j('#fkCordCbuEligible').val()=='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@INCOMPLETE_REASON)"/>'){
		if($j('#fkCordCbuEligibleReason').val() == null){
			$j('#fkCordCbuEligibleReason').css("border", "1px solid red");
			$j('#eligibleFlag').val("0");
			}
		else{
			$j('#eligibleFlag').val("1");
			}
	}else{
		$j('#eligibleFlag').val("1");
	}
}

function getFormInModal(title,url,popupDivName,divId,maximize,minimize){

	var orderId = $j('#orderId').val();
	var orderType = $j('#orderType').val();
	if(orderId=='undefined'){
		$j("#orderId").val("");
		orderId="";
	}else if(orderId!=null && orderId.indexOf(",")!=-1){
		$j("#orderId").val("");
		orderId="";
	}
	
	if(orderType=='undefined'){
		$j("#orderType").val("");
		orderType="";
	}else if(orderType!=null && orderType.indexOf(",")!=-1){
		$j("#orderType").val("");
		orderType="";
	}
	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	title=title+patient_data;
	showModals(title,url,'500','950',popupDivName,maximize,minimize);
	//$j('#'+divId+'Div').html("");
	$j('#'+divId+'Parent button').hide();
	$j('#'+divId+'Parent').hide();
	divId = divId+'Div';
	document.getElementById(divId).innerHTML="";
	//window[functionName]();
}

function hideSomeMoreDivs(divname){
	$j('#'+divname).hide();
}
</script>
<s:set name="tempVal" value="cbuFinalReviewPojo.finalReviewConfirmStatus"></s:set>
<div class="portlet" id="finalreviewmodel">
<div
	class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">

<div class="finalreviewmodel" style="height: auto;">
<div class="portlet-content" style="height: auto;">
<div class="finalreview" id="finalreview" style="height: auto;">

<div class="mainbox">

<div class="left_box" id="completeTd">
		<s:form id="completeReqInfoForm" action="">
			<s:hidden name="licenceUpdate" id="licenceUpdate" value="false"></s:hidden>
			<s:hidden name="cdrCbuPojo.cordID" id="cordId" />
			<s:hidden name="criChangeFlag" id="criChangeFlag"/>
			<s:hidden name="changeFlag" id="formFlag"/>
			<s:hidden name="cbuCompleteReqInfoPojo.cbuInfoFlag" id="cbuInfoFlag" />
			<s:hidden name="cbuCompleteReqInfoPojo.cbuProcessInfoFlag" id="cbuProcessInfoFlag" />
			<s:hidden name="cbuCompleteReqInfoPojo.labSummaryFlag" id="labSummaryFlag" />
			<s:hidden name="cbuCompleteReqInfoPojo.mrqFlag" id="mrqFlag" />
			<s:hidden name="cbuCompleteReqInfoPojo.fmhqFlag" id="fmhqFlag" />
			<s:hidden name="cbuCompleteReqInfoPojo.idmFlag" id="idmFlag" />
			<s:hidden name="cbuCompleteReqInfoPojo.idsFlag" id="idsFlag" />
			<s:hidden name="cbuCompleteReqInfoPojo.licensureFlag" id="licensureFlag" />
			<s:hidden name="cbuCompleteReqInfoPojo.eligibleFlag" id="eligibleFlag" />
			<s:hidden name="cdrCbuPojo.localCbuId" id="cbulocalid"></s:hidden>
			<s:hidden name="cdrCbuPojo.historicLocalCbuId" id="historicalcbulocalid"></s:hidden>			
			<s:hidden name="orderId" id="criOrderId"></s:hidden>
			<s:hidden name="orderType" id="criOrderType"></s:hidden>
			<s:hidden name="isCBBMem" id="isCBBMem" value="%{cdrCbuPojo.site.isCDRUser()}"></s:hidden>
			<s:hidden name="isDomestic" id="isDomestic" value="%{cdrCbuPojo.getIsSiteDomestic()}"></s:hidden>
			<s:hidden name="isCDRUser" id="isCDRUser" value="%{cdrCbuPojo.site.isCDRUser()}"></s:hidden>
			<input type="hidden" value="<s:property value='getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@LICENCE_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@UNLICENSE_REASON)'/>" id="unLicensedPk"/>
			<s:hidden name="cdrCbuPojo.cbuLicStatus" id="licenseid"></s:hidden>
			<s:text name="garuda.completeReqInfo.label.plscompfollreqinfo" />:
			<div id="iDs">
			<table>
				<tr><td><input type="checkbox" id="idschb" name="idschb" style="display: none;" /></td>
				<td id="idsTD">
				<a href="#" onclick="javascript:showrightdiv('<s:property value="cbuCompleteReqInfoPojo.idsFlag"/>','iDs','idsTargetDiv','getViewIdsInfo?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CRI_VIEW_IDS" />','idrefreshDiv');"><s:text name="garuda.completeReqInfo.label.ids"/></a>
				<s:if test="cbuCompleteReqInfoPojo.idsFlag==1">
					<img src="images/tick_mark.gif"/></s:if>
				</td>	
				</tr>
			</table>
			</div>
			
			<div id="licensure">
			<table>
				<tr><td><input type="checkbox" id="licensurechb" name="licensurechb" style="display: none;" /></td>
				<td id="licensureTD">
				<a href="#" onclick="getRighSideForm('viewFinalLicense?licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&esignFlag=modal&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CRI_VIEW_LICENSURE" />','licensureTargetDiv','licensureTargetParent','licensure','licensureTable');"><s:text name="garuda.completeReqInfo.label.licensure"/></a>
				<s:if test="cbuCompleteReqInfoPojo.licensureFlag==1">
					<img src="images/tick_mark.gif"/></s:if>
				</td>
				</tr>
			</table>
			</div>
			<!-- <div id="eligible">
			<table>
				<tr><td><input type="checkbox" id="eligiblechb" name="eligiblechb" style="display: none;" /></td>
				<td id="eligibleTD">
				<a href="#" onclick="javascript:getRighSideForm('viewFinalEligibility?licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&esignFlag=modal&orderId=<s:property value="orderId"/>&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CRI_VIEW_ELIGIBILITY" />','eligibleTargetDiv','eligibleTargetParent','eligible','eligibleTable');"><s:text name="garuda.completeReqInfo.label.eligible"/></a>
				<s:if test="cbuCompleteReqInfoPojo.eligibleFlag==1">
					<img src="images/tick_mark.gif"/></s:if>
				</td>	
				</tr>
			</table>
			</div> -->
			<s:if test="cdrCbuPojo.site.isCDRUser()==true">
			<s:if test="%{#request.isNewTaskReq==true}">
			<div id="CRIeligible">
			<table>
				<tr><td><input type="checkbox" id="eligiblechb" name="eligiblechb" style="display: none;" /></td>
				<td id="eligibleTD">
				<a href="#" onclick="javascript:getRighSideForm('cbuFinalEligibilityStatus?esignFlag=reviewCriView&status=eligble&licenceUpdate=false&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId=<s:property value="orderId"/>&orderType=<s:property value="orderType"/>&pkcordId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CRI_VIEW_ELIGIBILITY" />','eligibleTargetDiv','eligibleTargetParent','CRIeligible','eligibleTable');"><s:text name="garuda.completeReqInfo.label.eligible"/></a>
				 
				<s:if test="%{cbuCompleteReqInfoPojo.eligibleFlag==1}">
					<img src="images/tick_mark.gif"/></s:if>
				</td>	
				</tr>
			</table>
			</div>	
			</s:if>
			<div id="cbuinformationDiv">
			<table>
				<tr>
					<td>
					<input type="checkbox" id="cbuinfochb" name="cbuinfochb" style="display: none;" />
					</td>
					<td class="cbuinfoTD">
					<a href="#"
						onclick="javascript:showrightdiv('<s:property value="cbuCompleteReqInfoPojo.cbuInfoFlag"/>','cbuInfoMandatory','cbuInfoTargetDiv','getViewCbuInfo?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CRI_VIEW_CBU_INFO" />','cbuRefreshDiv');">
					<s:text name="garuda.completeReqInfo.label.cbuInfo" /></a>
					<s:if test="cbuCompleteReqInfoPojo.cbuInfoFlag == 1" >
					<img src="images/tick_mark.gif"/></s:if>
					</td>
				</tr>
			</table>
			</div>
			<div id="processingInformationDiv">
			<table>
				<tr><td>
					<input type="checkbox" id="procinfochb" name="procinfochb" style="display: none;" />
					</td>
					<td class="procinfoTD">
					<a href="#" onclick="getRighSideForm('getProcedureInfo?entityId=<s:property value="cdrCbuPojo.cordID" />&esginFlag=review&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CRI_VIEW_PROC_PRD" />','processInfoTargetDiv','processInfoTargetParent','cbuProcMandatory','eSignTable');">
					<s:text name="garuda.completeReqInfo.label.proceinfo" /></a>
					<s:if test="cbuCompleteReqInfoPojo.cbuProcessInfoFlag == 1" >
					<img src="images/tick_mark.gif"/></s:if>
					</td>
				</tr>
			</table>
			</div>
			<div id="labSummaryDiv">
			<table>
				<tr>
					<td>
					<input type="checkbox" id="labSummarychb" name="labSummarychb" style="display: none;" />
					</td>
					<td id="labSummaryTD">
					<a href="#"
						onclick="getRighSideForm('getLabSummary?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&esignFlag=review&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CRI_VIEW_LAB_SUMM" />','labSumTargetDiv','labSumTargetParent','cbuLabSummMandatory','eSignTable');">
					<s:text name="garuda.completeReqInfo.label.labsummery" /></a>
					<s:if test="cbuCompleteReqInfoPojo.labSummaryFlag == 1" >
					<img src="images/tick_mark.gif"/></s:if>
					</td>
				</tr>
			</table>
			</div>
			<s:if test="cdrCbuPojo.cbuLicStatus==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@LICENCE_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@UNLICENSE_REASON)">
			<s:if test="hasViewPermission(#request.updateHistry_Mrq)==true">
			<div id="mrqDiv" class="formsClass">
			<table>
				<tr><td>
					<input type="checkbox" id="mrqchb" name="mrqchb" style="display: none;" />
					</td>
					<td id="mrqTD">
					<a href="#" onclick="getRighSideForm('getDynamicFrm?formId=MRQ&iseditable=1&isCompInfo=1&entityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CRI_VIEW_MRQ" />','mrqTargetDiv','mrqTargetParent','formsMRQ','eSignTable');">
					<!-- onclick="showModal('MRQ Form','getDynamicFrm?formId=MRQ&isCompInfo=1&entityId=<s:property value="cdrCbuPojo.cordID" />','500','950');" -->
					<s:text name="garuda.completeReqInfo.label.mrq_infull" /></a>
					<s:if test="cbuCompleteReqInfoPojo.mrqFlag==1">
					<img src="images/tick_mark.gif"/></s:if>
					</td>
				</tr>
			</table>
			</div>
			</s:if>
			</s:if>
			<div id="idmDiv">
			<table>
				<tr><td>
					<input type="checkbox" id="idmchb" name="idmchb" style="display: none;" />
					</td>
					<td id="idmTD">
					<a href="#" onclick="getRighSideForm('getDynamicFrm?formId=IDM&iseditable=1&isCompInfo=1&entityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CRI_VIEW_IDM" />','idmTargetDiv','idmTargetParent','cbuIDM','eSignTable');">
					<s:text name="garuda.completeReqInfo.label.idm_infull" /></a>
					<s:if test="cbuCompleteReqInfoPojo.idmFlag==1">
					<img src="images/tick_mark.gif"/></s:if>
					</td>
				</tr>
			</table>
			</div>
			<s:if test="cdrCbuPojo.cbuLicStatus==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@LICENCE_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@UNLICENSE_REASON)">
			<s:if test="hasViewPermission(#request.updateHistry_FMHQ)==true">
			<s:if test="%{#request.isNewTaskReq==true}">
			<div id="fmhqDiv" class="formsClass">
			<table>
				<tr><td>
					<input type="checkbox" id="fmhqchb" name="fmhqchb" style="display: none;" />
					</td>
					<td id="fmhqTD">
					<a href="#" onclick="getRighSideForm('getDynamicFrm?formId=FMHQ&iseditable=1&isCompInfo=1&entityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CRI_VIEW_FMHQ" />','fmhqTargetDiv','fmhqTargetParent','formsFMHQ','eSignTable');">
					<!-- onclick="showModal('MRQ Form','getDynamicFrm?formId=MRQ&isCompInfo=1&entityId=<s:property value="cdrCbuPojo.cordID" />','500','950');" -->
					<s:text name="garuda.completeReqInfo.label.fmhq_infull" /></a>
					<s:if test="cbuCompleteReqInfoPojo.fmhqFlag==1">
					<img src="images/tick_mark.gif"/></s:if>
					</td>
				</tr>
			</table>
			</div>
			</s:if>
			</s:if>
			</s:if>
			</s:if>
			<s:if test="cdrCbuPojo.site.isCDRUser()==true">
			<s:if test="(#request.isNewTaskReq==true && cdrCbuPojo.cbuLicStatus == getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@LICENCE_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@UNLICENSE_REASON)) && (cbuCompleteReqInfoPojo != null && cbuCompleteReqInfoPojo.completeReqinfoFlag!=null && cbuCompleteReqInfoPojo.completeReqinfoFlag==1 && cbuCompleteReqInfoPojo.idsFlag==1 && cbuCompleteReqInfoPojo.licensureFlag==1 && cbuCompleteReqInfoPojo.idmFlag==1 && cbuCompleteReqInfoPojo.mrqFlag==1 && cbuCompleteReqInfoPojo.labSummaryFlag==1 && cbuCompleteReqInfoPojo.cbuProcessInfoFlag==1 && cbuCompleteReqInfoPojo.cbuInfoFlag==1 && cbuCompleteReqInfoPojo.fmhqFlag==1 && cbuCompleteReqInfoPojo.eligibleFlag==1)">
			<center>
			<s:text name="garuda.completeReqInfo.label.completedDesc"/>
				 <s:if test="cbuCompleteReqInfoPojo.lastModifiedOn ==null">
				 <s:date name="%{cbuCompleteReqInfoPojo.createdOn}" id="cdate" format="MMM dd, yyyy" /><s:property value="%{cdate}"/>
				 </s:if>
				 <s:else>
				 <s:date name="%{cbuCompleteReqInfoPojo.lastModifiedOn}" id="cdate" format="MMM dd, yyyy" /><s:property value="%{cdate}"/>
				 </s:else>
				<br/>
			</center>
			</s:if>
			<s:elseif test="((#request.isNewTaskReq==null || #request.isNewTaskReq==false) && cdrCbuPojo.cbuLicStatus == getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@LICENCE_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@UNLICENSE_REASON)) && (cbuCompleteReqInfoPojo != null && cbuCompleteReqInfoPojo.completeReqinfoFlag!=null && cbuCompleteReqInfoPojo.completeReqinfoFlag==1 && cbuCompleteReqInfoPojo.idsFlag==1 && cbuCompleteReqInfoPojo.licensureFlag==1 && cbuCompleteReqInfoPojo.idmFlag==1 && cbuCompleteReqInfoPojo.mrqFlag==1 && cbuCompleteReqInfoPojo.labSummaryFlag==1 && cbuCompleteReqInfoPojo.cbuProcessInfoFlag==1 && cbuCompleteReqInfoPojo.cbuInfoFlag==1)">
			<center>
			<s:text name="garuda.completeReqInfo.label.completedDesc"/>
				 <s:if test="cbuCompleteReqInfoPojo.lastModifiedOn ==null">
				 <s:date name="%{cbuCompleteReqInfoPojo.createdOn}" id="cdate" format="MMM dd, yyyy" /><s:property value="%{cdate}"/>
				 </s:if>
				 <s:else>
				 <s:date name="%{cbuCompleteReqInfoPojo.lastModifiedOn}" id="cdate" format="MMM dd, yyyy" /><s:property value="%{cdate}"/>
				 </s:else>
				<br/>
			</center>
			
			</s:elseif>
			<s:elseif test="(#request.isNewTaskReq==true && cdrCbuPojo.cbuLicStatus == getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@LICENCE_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@LICENCE)) && (cbuCompleteReqInfoPojo != null && cbuCompleteReqInfoPojo.completeReqinfoFlag!=null && cbuCompleteReqInfoPojo.completeReqinfoFlag==1 && cbuCompleteReqInfoPojo.idsFlag==1 && cbuCompleteReqInfoPojo.licensureFlag==1 && cbuCompleteReqInfoPojo.idmFlag==1  && cbuCompleteReqInfoPojo.labSummaryFlag==1 && cbuCompleteReqInfoPojo.cbuProcessInfoFlag==1 && cbuCompleteReqInfoPojo.cbuInfoFlag==1 && cbuCompleteReqInfoPojo.eligibleFlag==1)">
			<center>
			<s:text name="garuda.completeReqInfo.label.completedDesc"/>
				 <s:if test="cbuCompleteReqInfoPojo.lastModifiedOn ==null">
				 <s:date name="%{cbuCompleteReqInfoPojo.createdOn}" id="cdate" format="MMM dd, yyyy" /><s:property value="%{cdate}"/>
				 </s:if>
				 <s:else>
				 <s:date name="%{cbuCompleteReqInfoPojo.lastModifiedOn}" id="cdate" format="MMM dd, yyyy" /><s:property value="%{cdate}"/>
				 </s:else>
				<br/>
			</center>
			</s:elseif>
			
			<s:elseif test="((#request.isNewTaskReq==null || #request.isNewTaskReq==false) && cdrCbuPojo.cbuLicStatus == getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@LICENCE_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@LICENCE)) && (cbuCompleteReqInfoPojo != null && cbuCompleteReqInfoPojo.completeReqinfoFlag!=null && cbuCompleteReqInfoPojo.completeReqinfoFlag==1 && cbuCompleteReqInfoPojo.idsFlag==1 && cbuCompleteReqInfoPojo.licensureFlag==1 && cbuCompleteReqInfoPojo.idmFlag==1  && cbuCompleteReqInfoPojo.labSummaryFlag==1 && cbuCompleteReqInfoPojo.cbuProcessInfoFlag==1 && cbuCompleteReqInfoPojo.cbuInfoFlag==1)">
			<center>
			<s:text name="garuda.completeReqInfo.label.completedDesc"/>
				 <s:if test="cbuCompleteReqInfoPojo.lastModifiedOn ==null">
				 <s:date name="%{cbuCompleteReqInfoPojo.createdOn}" id="cdate" format="MMM dd, yyyy" /><s:property value="%{cdate}"/>
				 </s:if>
				 <s:else>
				 <s:date name="%{cbuCompleteReqInfoPojo.lastModifiedOn}" id="cdate" format="MMM dd, yyyy" /><s:property value="%{cdate}"/>
				 </s:else>
				<br/>
			</center>
			</s:elseif>
			</s:if>
			<s:elseif test="(cdrCbuPojo.site.isCDRUser()==false && cbuCompleteReqInfoPojo != null && cbuCompleteReqInfoPojo.completeReqinfoFlag!=null && cbuCompleteReqInfoPojo.completeReqinfoFlag==1 && cbuCompleteReqInfoPojo.idsFlag==1 && cbuCompleteReqInfoPojo.licensureFlag==1)">
			<center>
			<s:text name="garuda.completeReqInfo.label.completedDesc"/> 
				 <s:if test="cbuCompleteReqInfoPojo.lastModifiedOn ==null">
				 <s:date name="%{cbuCompleteReqInfoPojo.createdOn}" id="cdate" format="MMM dd, yyyy" /><s:property value="%{cdate}"/>
				 </s:if>
				 <s:else>
				 <s:date name="%{cbuCompleteReqInfoPojo.lastModifiedOn}" id="cdate" format="MMM dd, yyyy" /><s:property value="%{cdate}"/>
				 </s:else>
				<br/>
			</center>
			</s:elseif>
			<div id="SubmitAllDiv">
				<table bgcolor="#cccccc" width="100%">
					<tr bgcolor="#cccccc">
						<td valign="baseline" width="100%" align="center">   
						   <input type="button" onclick="closeModals('dialogForCRI');"  value="<s:text name="garuda.pendingorderpage.label.close"/>" />
						</td> 		 
					</tr>
				</table>
			</div>
		</s:form>
		</div>
		<div class="right_box">
		
		<div id="finalresult" class="finalresult">
		
	<s:if test="%{cdrCbuPojo.site.isCDRUser()==true}">	
		<!--starts CBU info widget div -->
		<div id="cbuInfoTargetDiv" style="display: none;width: 100%;overflow: auto;">
		<div id="cbuinfocontent" class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="text-align: center;">
					<span class="ui-dialog-title"></span>
					<s:text	name="garuda.completeReqInfo.label.cbuInfo" />
				</div>
		<s:form>
		<div style="width: 100%;overflow: auto;" class="rightcont">
		<fieldset style="border: 1px solid #D2D2D2;">
		<div id="cbuRefreshDiv">
		<!-- cb_viewcbuinfo.jsp -->
		</div>
				
				</fieldset>
		</div>
		<s:if test="hasEditPermission(#request.cmpltReqInfo)==true">
		<table>
				<tr align="right">
					 <s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
    </s:if>
    <s:else>
					<td colspan="4" align="right" style="vertical-align: right">
					<input type="button" name="cbuInfoEdit"
						value='<s:text name="garuda.common.lable.edit"></s:text>'
						onclick="hideSomeMoreDivs('cbuInfoTargetDiv');getModalforEdit('<s:text name="garuda.completeReqInfo.label.editCbuInfo"></s:text>'+' <s:text name="garuda.message.modal.headerregid"/>:<s:property value="cdrCbuPojo.registryId" />','updateCharacteristics?esignFlag=review&licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CRI_EDIT_CBU_INFO" />','<s:property value="orderId"/>','<s:property value="orderType"/>');">
					</td>
					</s:else>
				</tr>
			</table>
			</s:if>
		</s:form>
		</div>
		<!--ends CBU info widget div -->

		<!--starts CBU Proccessing info widget div -->
		<div id="processInfoTargetParent" style="display: none;width: 100%;overflow: auto;">
		<div id="procInfocontent" class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="text-align: center;">
					<span class="ui-dialog-title"></span>
					<s:text	name="garuda.completeReqInfo.label.proceinfo" />
				</div>
		<div id="processInfoTargetDiv" style="width: 100%;overflow: auto;" class="rightcont"></div>
		<br/>
			<s:if test="hasEditPermission(#request.cmpltReqInfo)==true">
			<div>
			 <s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
    		</s:if>
   			 <s:else>
				<s:form><button type="button"
						name="procedureEdit"
						onclick="hideSomeMoreDivs('processInfoTargetParent');getFormInModal('<s:text name="garuda.completeReqInfo.label.editproceinfo"></s:text>'+' <s:text name="garuda.message.modal.headerregid"/>:<s:property value="cdrCbuPojo.registryId" />','getProcedureInfo?entityId=<s:property value="cdrCbuPojo.cordID" />&esginFlag=modal&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CRI_EDIT_PROC_PRD" />','ProcedureModalForm','processInfoTarget');">
						<s:text name="garuda.common.lable.edit"></s:text>
						</button>
				</s:form>
				</s:else>
			</div>
			</s:if>
		</div>
		<!--ends CBU Proccessing info widget div -->
		
		<!--starts MRQ widget div -->
		<div id="mrqTargetParent" style="display: none;width: 100%;overflow: auto;">
		<div id="mrqcontent" class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="text-align: center;">
					<span class="ui-dialog-title"></span>
					<s:text	name="garuda.completeReqInfo.label.mrq_infull" />
				</div>
			<div id="mrqTargetDiv" style="width: 100%;overflow: auto;" class="rightcont"></div>
			<br/>
			<s:if test="hasEditPermission(#request.cmpltReqInfo)==true">
			<div>
			 <s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
  		  </s:if>
   			 <s:else>
				<s:form><button name="editMRQForm" type="button" id="editMRQForm" onclick="hideSomeMoreDivs('mrqTargetParent');getFormInModal('<s:text name="garuda.completeReqInfo.label.editMrqForm"></s:text>'+' <s:text name="garuda.message.modal.headerregid"/>:<s:property value="cdrCbuPojo.registryId" />','getDynamicFrm?formId=MRQ&isCompInfo=1&entityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CRI_EDIT_MRQ" />','MRQModalForm','mrqTarget',true,true);">
					<s:text name="garuda.common.lable.edit"></s:text>	
				</button></s:form>
			</s:else>
			</div>
			
			</s:if>
		</div>
		<!--ends MRQ widget div -->
		
		<!--starts FMHQ widget div -->
		<div id="fmhqTargetParent" style="display: none;width: 100%;overflow: auto;">
		<div id="fmhqcontent" class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="text-align: center;">
					<span class="ui-dialog-title"></span>
					<s:text	name="garuda.completeReqInfo.label.fmhq_infull" />
				</div>
			<s:form><div id="fmhqTargetDiv" style="width: 100%;overflow: auto;" class="rightcont"></div></s:form>	
			<br/>
			<s:if test="hasEditPermission(#request.cmpltReqInfo)==true">
			<div>
			  <s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
    </s:if>
    <s:else>
				<s:form><button name="editFMHQForm" type="button" id="editFMHQForm" onclick="hideSomeMoreDivs('fmhqTargetParent');getFormInModal('<s:text name="garuda.completeReqInfo.label.editFmhqForm"></s:text>'+' <s:text name="garuda.message.modal.headerregid"/>:<s:property value="cdrCbuPojo.registryId" />','getDynamicFrm?formId=FMHQ&isCompInfo=1&entityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CRI_EDIT_FMHQ" />','FMHQModalForm','fmhqTarget',true,true);">
						<s:text name="garuda.common.lable.edit"></s:text>	
				</button></s:form>
			</s:else>
			</div>
			</s:if>		
		</div>
		<!--ends FMHQ widget div -->	
		
		<!--starts IDM widget div -->
		<div id="idmTargetParent" style="display: none;width: 100%;overflow: auto;">
		<div id="idmcontent" class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="text-align: center;">
					<span class="ui-dialog-title"></span>
					<s:text	name="garuda.completeReqInfo.label.idm_infull" />
				</div>
			<s:form><div id="idmTargetDiv" style="width: 100%;overflow: auto;" class="rightcont"></div></s:form>	
			<br/>
			<s:if test="hasEditPermission(#request.cmpltReqInfo)==true">
			<div>
			  <s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
    </s:if>
    <s:else>
				<s:form><button name="editIDMForm" type="button" id="editIDMForm" onclick="hideSomeMoreDivs('idmTargetParent');getFormInModal('<s:text name="garuda.completeReqInfo.label.editIdmForm"></s:text>'+' <s:text name="garuda.message.modal.headerregid"/>:<s:property value="cdrCbuPojo.registryId" />','getDynamicFrm?formId=IDM&isCompInfo=1&entityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CRI_EDIT_IDM" />','IDMModalForm','idmTarget',true,true);">
						<s:text name="garuda.common.lable.edit"></s:text>	
				</button></s:form>
			</s:else>
			</div>
			</s:if>		
		</div>
		<!--ends IDM widget div -->		
		
		
		<!--starts Lab Summery widget div -->
		<div id="labSumTargetParent" style="display: none;width: 100%;overflow: auto;">
		<div id="labSumcontent" class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="text-align: center;">
					<span class="ui-dialog-title"></span>
					<s:text	name="garuda.cordentry.label.labsummary" />
				</div>
			<div id="labSumTargetDiv" style="width: 100%;overflow: auto;" class="rightcont"></div>	
			<br/>
			<s:if test="hasEditPermission(#request.cmpltReqInfo)==true">
			<div>
			 <s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
 		   </s:if>
   			 <s:else>
				<s:form><button name="editLABForm" type="button" id="editLABForm" onclick="forLabSummary('labSumTarget');forLabSummaryEdit();">
						<s:text name="garuda.common.lable.edit"></s:text>	
				</button></s:form>
			</s:else>
			</div>
			</s:if>
		</div>
		<!--ends Lab Summery widget div -->
		
		<div id="eligibleTargetParent" style="display: none;width: 100%;overflow: auto;">
		<div id="eligiblecontent" 
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><span
				class="ui-icon ui-icon-triangle-1-s"></span> <s:text
				name="garuda.cdrcbuview.label.eligibility_determination" /></div>
			<div id="eligibleTargetDiv" style="width: 100%;overflow: auto;" class="rightcont"></div>
			<br/>
			<s:if test="hasEditPermission(#request.cmpltReqInfo)==true">
			<s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
 			</s:if>
 			<s:else>
			<div>
				<s:form>
				<button name="editEligibleForm" type="button" id="editEligibleForm" onclick="getFormInModal('Edit Eligibility Determination <s:text name="garuda.message.modal.headerregid"/>:<s:property value="cdrCbuPojo.registryId" />','cbuFinalEligibilityStatus?esignFlag=reviewCriEdit&status=eligble&licenceUpdate=false&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId=<s:property value="orderId"/>&orderType=<s:property value="orderType"/>&pkcordId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CRI_VIEW_ELIGIBILITY" />','modelPopup1','eligibleTarget',false,false);">
					<s:text name="garuda.common.lable.edit"></s:text>
				</button>
				</s:form>
			</div>
			</s:else>
			</s:if>
		</div>
	</s:if>	
	
	<div id="idsTargetDiv" style="display: none;width: 100%;overflow: auto;">
	<div id="idscontent" class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="text-align: center;">
					<span class="ui-dialog-title"></span>
					<s:text	name="garuda.cdrcbuview.label.ids" />
				</div>
				<div id="idrefreshDiv">
				<!-- cb_viewidsinfo.jsp -->
				</div>
		<s:form>
		 <s:if test="hasEditPermission(#request.cmpltReqInfo)==true">
			<table>
				<tr align="right">
				 <s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
 				   </s:if>
 				   <s:else>
					<td colspan="4">
					    <input type="button"
						name="idssEdit" value="<s:text name="garuda.common.lable.edit"/>"
						onclick="hideSomeMoreDivs('idsTargetDiv');getModalforEdit('Edit CBU IDs'+' <s:text name="garuda.message.modal.headerregid"/>:<s:property value="cdrCbuPojo.registryId" />','updateIdInfo?licenceUpdate=False&cordIdStr=<s:property value="cdrCbuPojo.cordID" />&esignFlag=review&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CRI_EDIT_IDS" />','<s:property value="orderId"/>','<s:property value="orderType"/>');">
					</td>
					</s:else>
				</tr>
			</table>
		</s:if>
		</s:form>	
		</div>
	
		<div id="licensureTargetParent" style="display: none;width: 100%;overflow: auto;">
				<div id="licensurecontent" class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="text-align: center;">
					<span class="ui-dialog-title"></span>
					<s:text	name="garuda.cdrcbuview.label.licensure_status" />
				</div>
			<fieldset style="border: 1px solid #D2D2D2;">
			<div id="licensureTargetDiv" style="width: 100%;overflow: auto;" class="rightcont"></div></fieldset>
			<br/>
		    <s:if test="hasEditPermission(#request.cmpltReqInfo)==true">
			<div>
		 <s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
    </s:if>
    <s:else>
			<s:form><button name="editLicensureForm" type="button" id="editLicensureForm" onclick="hideSomeMoreDivs('licensureTargetParent');getFormInModal('Edit Licensure Status'+' <s:text name="garuda.message.modal.headerregid"/>:<s:property value="cdrCbuPojo.registryId" />','updateStatusLicense?licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&esignFlag=reviewCRIview&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CRI_EDIT_LICENSURE" />','modelPopup1','licensureTarget',false,false);">
					<s:text name="garuda.common.lable.edit"></s:text>	
			</button></s:form>
			</s:else>
			</div>
			</s:if>
		</div>
		
		
		</div>
		</div>
</div>
</div>
</div>
</div>
</div>
<script>
function forLabSummary(val){
	$j('#'+val+'Parent').hide();
	$j('#'+val+'Div').empty();
}
function getModalforEdit(title,url,orderId,orderType){
	
	if(typeof(orderId)=="undefined"){
		$j("#orderId").val("");
		orderId="";
	}else if(orderId!=null && orderId.indexOf(",")!=-1){
		$j("#orderId").val("");
		orderId="";
	}
	
	if(typeof(orderType)=="undefined"){
		$j("#orderType").val("");
		orderType="";
	}else if(orderType!=null && orderType.indexOf(",")!=-1){
		$j("#orderType").val("");
		orderType="";
	}
	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	title=title+patient_data;
	url = url+'&orderId='+orderId+'&orderType='+orderType;
	showModal(title,url,'500','850');
}
function forLabSummaryEdit(){
	var orderId = $j("#orderId").val();
	var orderType = $j("#orderType").val();

	if(typeof(orderId)=="undefined"){
		$j("#orderId").val("");
		orderId="";
	}else if(orderId!=null && orderId.indexOf(",")!=-1){
		$j("#orderId").val("");
		orderId="";
	}
	
	if(typeof(orderType)=="undefined"){
		$j("#orderType").val("");
		orderType="";
	}else if(orderType!=null && orderType.indexOf(",")!=-1){
		$j("#orderType").val("");
		orderType="";
	}
	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	showModals('<s:text name="garuda.completeReqInfo.label.editLabSummaryForm"></s:text>'+' <s:text name="garuda.message.modal.headerregid"/>:<s:property value="cdrCbuPojo.registryId" />'+patient_data,'getLabSummary?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&esignFlag=modal&orderId='+orderId+'&orderType='+orderType+'&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CRI_EDIT_LAB_SUMM" />','500','900','labSummaryForComplete','','','')
}
</script>
<script language="javascript">
function getRighSideForm(url,divId,divId2,functionName,hideSign){
	showprogressMgs();
	hideDiv();
	$j('#'+divId2+' button').show();
		$j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        success: function (result){
	            $j("#"+divId).html(result);
	            $j("#"+divId2).show();
	            $j("#"+divId).show();
	            if(divId=='fmhqTargetDiv'){
	            	if(($j("#"+divId).find("#FMHQformversion").val()!=$j("#"+divId).find("#currentFMHQV").val()) || $j("#isFormavail").val()!="true"){
	            		$j("#editFMHQForm").hide();
		            }else{
		            	$j("#editFMHQForm").show();
			        }
		        }
	     	}
		});
		//setTimeout(function(){
			
			if(divId!="idmTargetDiv" && divId!="mrqTargetDiv" && divId!="fmhqTargetDiv"){
				window[functionName]();
				$j('#'+hideSign).hide();
				$j("#"+divId+" select").attr('disabled',true);
				$j('#'+divId+' button').hide();
				$j('#'+divId+' input[type=button]').hide();
				$j("#"+divId+" input[type=text]").attr('disabled',true);
				$j('#'+divId+' a').hide();
				$j('#labSumTargetDiv'+' a').show();
			}
			
			if(hideSign=="licensureTable"){
				$j('#licensurecontent').show();
				$j('#liensehistroyTR').hide();
			}
			
			else if(hideSign=="eligibleTable"){
				$j('#eligiblecontent').show();
				$j('#eligiblehistory').empty();
			}
		//},0);
		closeprogressMsg();
}

function hideDiv(){
	if(document.getElementById("modelPopup1")!=null){
	document.getElementById("modelPopup1").innerHTML="";
	}
	if($j('#isCDRUser').val()=="true"){
		//$j('.idmteststr').show();
		$j('#idsTargetDiv').css('display','none');
		$j('#licensureTargetParent').css('display','none');
		//$j('#licensureTargetDiv').empty();
		document.getElementById("licensureTargetDiv").innerHTML="";
		document.getElementById("cbuRefreshDiv").innerHTML="";
		document.getElementById("idrefreshDiv").innerHTML="";
		$j('#cbuInfoTargetDiv').css('display','none');
		$j('#processInfoTargetParent').css('display','none');
		$j('#processInfoTargetDiv').css('display','none');
		$j('#labSumTargetParent').css('display','none');
		//$j('#labSumTargetDiv').empty();
		document.getElementById("labSumTargetDiv").innerHTML="";
		$j('#eligibleTargetParent').css('display','none');
		$j('#eligibleTargetDiv').empty();
		if($j('#licenseid').val()==$j('#unLicensedPk').val()){
			$j('#mrqTargetParent').css('display','none');
			//$j('#mrqTargetDiv').empty();
			document.getElementById("mrqTargetDiv").innerHTML="";
			$j('#fmhqTargetParent').css('display','none');
			//$j('#fmhqTargetDiv').empty();
			document.getElementById("fmhqTargetDiv").innerHTML="";
		}
		$j('#idmTargetParent').css('display','none');
		//$j('#idmTargetDiv').empty();
		document.getElementById("idmTargetDiv").innerHTML="";
	}
	if($j('#isCDRUser').val()=="false"){
		$j('#idsTargetDiv').css('display','none');
		$j('#licensureTargetParent').css('display','none');
		//$j('#licensureTargetDiv').empty();
		document.getElementById("licensureTargetDiv").innerHTML="";
		document.getElementById("idrefreshDiv").innerHTML="";
	}
	/*$j("#esign").val("");
	$j("#submitId").attr("disabled", true); 
	$j("#pass").css('display','none'); 
	$j("#minimum").css('display','none');
	$j("#invalid").css('display','none');*/
	$j('.rightcont').each(function(){
		var dialogheight = $j('#dialogForCRI').height();
		var idclass = $j(this).attr("id");
		if(idclass!=""){
    	$j(this).height(dialogheight - 130);
		}
    });
	
}
$j(function(){

	$j('#dialogForCRI').bind( "dialogresize", function(event, ui) {
    	var dialogheight = $j('#dialogForCRI').height();
        $j('.rightcont').each(function(){
        	$j(this).height(dialogheight - 130);
        });
    });
    	
});

$j(document).ready(function(){
	
	//hideDiv();
	addIdsMandatory();
	getHiddedValues();
	if($j('#licenseid').val()==$j("#unLicensedPk").val()){
		$j('.formsClass').show();
	}else{
		$j('.formsClass').hide();
	}
	var orderId = $j("#orderId").val();
	var orderType = $j("#orderType").val();

	if(typeof(orderId)=="undefined"){
		$j("#orderId").val("");
		orderId="";
	}else if(orderId!=null && orderId.indexOf(",")!=-1){
		$j("#orderId").val("");
		orderId="";
	}
	if(typeof(orderType)=="undefined"){
		$j("#orderType").val("");
		orderType="";
	}else if(orderType!=null && orderType.indexOf(",")!=-1){
		$j("#orderType").val("");
		orderType="";
	}
	$j("#criOrderId").val(orderId);
	$j("#criOrderType").val(orderType);
});
</script>
</div>