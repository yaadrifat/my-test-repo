<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<jsp:useBean id="modCtlDao" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<%
	HttpSession tSession = request.getSession(true); 
	GrpRightsJB grpRights =null;
	if (tSession.getAttribute("LocalGRights")!=null){
	 grpRights = (GrpRightsJB) tSession.getAttribute("LocalGRights");
	}else
	{
	 grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
	}
	int updateINDstat = 0;
	int updateCBUstatus = 0;
	int updateEligStat=0;
	int updateLicStat=0;
	int updateCliStat=0;
	int updateUploadDoc=0;
	int updateUnitRepStat=0;
	int updateMinCriteria=0;
	int updateCBUAssment=0;
	int updateCBUReport=0;
	int updateAuditTrail=0;
	int updateFinalCBURev=0;
	int updateFlaggedItem=0;
	int updateShipmentItern=0;
	int updateProductInsert=0;
	int updateQuickLnkPanel=0;
	int updateCBUInfowidget=0;
	int updateIDwidget=0;
	int updateProInfoWidget=0;
	int updateInfectDisease=0;
	int updateidmForm =0;
	int updateCbuHLA=0;
	int updateHLA_CBU=0;
	int updateHLA_Matrnl=0;
	int updateHLA_Patient=0;
	int updateHlthHis=0;
	int updateHistry_Mrq=0;
	int updateHistry_FMHQ=0;
	int updateLabSmmry=0;
	int updateOthrTstRslt=0;
	int updateClnclNotes=0;
	int updateElgblty=0;
	int vClnclNote=0;
	int viewNote =0;
	int crtePckngSlip =0;
	int viewPatienttc = 0;
	int vPatientAntgn = 0;
	int CaseMngrDir = 0;
	int viewCbuAvailCon = 0;
	int viewRqstCBUhis = 0;
	int viewreqclincinfo = 0;
	int viewNotes = 0;
	int viewHEResol= 0;
	int vPrgrssNote = 0;
	int applyResotoCT = 0;
	int viewFnlCBURvw = 0;
	int viewTCOrdrDetail = 0;
	int viewORResolution = 0;
	int viewPrgrssNotes = 0;
	int viewCBUShipmnt = 0;
	int viewAddTypbyCBB = 0;
	int viewCTResolution = 0;
	int viewCTshipmnt= 0;
	int vCTinstns = 0;
	int assignOrders=0;
	int conElgble=0;

	if(grpRights.getFtrRightsByValue("CB_INSTAT")!=null && !grpRights.getFtrRightsByValue("CB_INSTAT").equals(""))
	{updateINDstat = Integer.parseInt(grpRights.getFtrRightsByValue("CB_INSTAT"));}
	else
		updateINDstat = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CBUSTATUS")!=null && !grpRights.getFtrRightsByValue("CB_CBUSTATUS").equals(""))
	{updateCBUstatus = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBUSTATUS"));}
	else
		updateCBUstatus = 4;
	
	if(grpRights.getFtrRightsByValue("CB_ELSTAT")!=null && !grpRights.getFtrRightsByValue("CB_ELSTAT").equals(""))
	{updateEligStat = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ELSTAT"));}
	else
		updateEligStat = 4;
	
	if(grpRights.getFtrRightsByValue("CB_LICSTAT")!=null && !grpRights.getFtrRightsByValue("CB_LICSTAT").equals(""))
	{updateLicStat = Integer.parseInt(grpRights.getFtrRightsByValue("CB_LICSTAT"));}
	else
		updateLicStat = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CLSTAT")!=null && !grpRights.getFtrRightsByValue("CB_CLSTAT").equals(""))
	{updateCliStat = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CLSTAT"));}
	else
		updateCliStat = 4;
	
	if(grpRights.getFtrRightsByValue("CB_UPLDOC")!=null && !grpRights.getFtrRightsByValue("CB_UPLDOC").equals(""))
	{	updateUploadDoc = Integer.parseInt(grpRights.getFtrRightsByValue("CB_UPLDOC"));}
	else
		updateUploadDoc = 4;
	
	if(grpRights.getFtrRightsByValue("CB_ATREP")!=null && !grpRights.getFtrRightsByValue("CB_ATREP").equals(""))
	{	updateUnitRepStat = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ATREP"));}
	else
		updateUnitRepStat = 4;
	
	if(grpRights.getFtrRightsByValue("CB_REVMIN")!=null && !grpRights.getFtrRightsByValue("CB_REVMIN").equals(""))
	{	updateMinCriteria = Integer.parseInt(grpRights.getFtrRightsByValue("CB_REVMIN"));}
	else
		updateMinCriteria = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CBUASSMNT")!=null && !grpRights.getFtrRightsByValue("CB_CBUASSMNT").equals(""))
	{updateCBUAssment = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBUASSMNT"));}
	else
		updateCBUAssment = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CBUREPORT")!=null && !grpRights.getFtrRightsByValue("CB_CBUREPORT").equals(""))
	{updateCBUReport = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBUREPORT"));}
	else
		updateCBUReport = 4;
	
	if(grpRights.getFtrRightsByValue("CB_AUDIT")!=null && !grpRights.getFtrRightsByValue("CB_AUDIT").equals(""))
	{updateAuditTrail = Integer.parseInt(grpRights.getFtrRightsByValue("CB_AUDIT"));}
	else
		updateAuditTrail = 4;
	
	if(grpRights.getFtrRightsByValue("CB_FINALREVIEW")!=null && !grpRights.getFtrRightsByValue("CB_FINALREVIEW").equals(""))
	{updateFinalCBURev = Integer.parseInt(grpRights.getFtrRightsByValue("CB_FINALREVIEW"));}
	else
		updateFinalCBURev = 4;
	
	if(grpRights.getFtrRightsByValue("CB_FLAGGEDITEM")!=null && !grpRights.getFtrRightsByValue("CB_FLAGGEDITEM").equals(""))
	{updateFlaggedItem = Integer.parseInt(grpRights.getFtrRightsByValue("CB_FLAGGEDITEM")); }
	else
		updateFlaggedItem = 4;
	
	if(grpRights.getFtrRightsByValue("CB_SHPMNTITERN")!=null && !grpRights.getFtrRightsByValue("CB_SHPMNTITERN").equals(""))
	{updateShipmentItern = Integer.parseInt(grpRights.getFtrRightsByValue("CB_SHPMNTITERN")); }
	else
		updateFlaggedItem = 4;
	
	if(grpRights.getFtrRightsByValue("CB_PRODUCTINSRT")!=null && !grpRights.getFtrRightsByValue("CB_PRODUCTINSRT").equals(""))
	{updateProductInsert = Integer.parseInt(grpRights.getFtrRightsByValue("CB_PRODUCTINSRT")); }
	else
		updateProductInsert = 4;
	
	if(grpRights.getFtrRightsByValue("CB_QUICKLNKPANL")!=null && !grpRights.getFtrRightsByValue("CB_QUICKLNKPANL").equals(""))
	{updateQuickLnkPanel = Integer.parseInt(grpRights.getFtrRightsByValue("CB_QUICKLNKPANL")); }
	else
		updateQuickLnkPanel = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CBUINFOW")!=null && !grpRights.getFtrRightsByValue("CB_CBUINFOW").equals(""))
	{updateCBUInfowidget = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBUINFOW")); }
	else
		updateCBUInfowidget = 4;
	
	if(grpRights.getFtrRightsByValue("CB_IDW")!=null && !grpRights.getFtrRightsByValue("CB_IDW").equals(""))
	{updateIDwidget = Integer.parseInt(grpRights.getFtrRightsByValue("CB_IDW")); }
	else
		updateIDwidget = 4;
	
	if(grpRights.getFtrRightsByValue("CB_PROINFOW")!=null && !grpRights.getFtrRightsByValue("CB_PROINFOW").equals(""))
	{	updateProInfoWidget = Integer.parseInt(grpRights.getFtrRightsByValue("CB_PROINFOW")); }
	else
		updateProInfoWidget = 4;
	
	if(grpRights.getFtrRightsByValue("CB_IDMW")!=null && !grpRights.getFtrRightsByValue("CB_IDMW").equals(""))
	{	updateInfectDisease = Integer.parseInt(grpRights.getFtrRightsByValue("CB_IDMW")); }
	else
		updateInfectDisease = 4;
	
	if(grpRights.getFtrRightsByValue("CB_IDMFORM")!=null && !grpRights.getFtrRightsByValue("CB_IDMFORM").equals(""))
	{	updateidmForm = Integer.parseInt(grpRights.getFtrRightsByValue("CB_IDMFORM")); }
	else
		updateidmForm = 4;
	
	if(grpRights.getFtrRightsByValue("CB_HLAW")!=null && !grpRights.getFtrRightsByValue("CB_HLAW").equals(""))
	{	updateCbuHLA = Integer.parseInt(grpRights.getFtrRightsByValue("CB_HLAW")); }
	else
		updateCbuHLA = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CBUHLAW")!=null && !grpRights.getFtrRightsByValue("CB_CBUHLAW").equals(""))
	{	updateHLA_CBU = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBUHLAW")); }
	else
		updateHLA_CBU = 4;
	
	if(grpRights.getFtrRightsByValue("CB_MATERNALHLA")!=null && !grpRights.getFtrRightsByValue("CB_MATERNALHLA").equals(""))
	{	updateHLA_Matrnl = Integer.parseInt(grpRights.getFtrRightsByValue("CB_MATERNALHLA")); }
	else
		updateHLA_Matrnl = 4;
	
	if(grpRights.getFtrRightsByValue("CB_PATIENTHLA")!=null && !grpRights.getFtrRightsByValue("CB_PATIENTHLA").equals(""))
	{	updateHLA_Patient = Integer.parseInt(grpRights.getFtrRightsByValue("CB_PATIENTHLA")); }
	else
		updateHLA_Patient = 4;
	
	if(grpRights.getFtrRightsByValue("CB_HLTHISCRNW")!=null && !grpRights.getFtrRightsByValue("CB_HLTHISCRNW").equals(""))
	{	updateHlthHis = Integer.parseInt(grpRights.getFtrRightsByValue("CB_HLTHISCRNW")); }
	else
		updateHlthHis = 4;
			
	if(grpRights.getFtrRightsByValue("CB_MRQW")!=null && !grpRights.getFtrRightsByValue("CB_MRQW").equals(""))
	{	updateHistry_Mrq = Integer.parseInt(grpRights.getFtrRightsByValue("CB_MRQW")); }
	else
		updateHistry_Mrq = 4;
	
	if(grpRights.getFtrRightsByValue("CB_FMHQW")!=null && !grpRights.getFtrRightsByValue("CB_FMHQW").equals(""))
	{	updateHistry_FMHQ = Integer.parseInt(grpRights.getFtrRightsByValue("CB_FMHQW")); }
	else
		updateHistry_FMHQ = 4;
	
	if(grpRights.getFtrRightsByValue("CB_LABSUMW")!=null && !grpRights.getFtrRightsByValue("CB_LABSUMW").equals(""))
	{	updateLabSmmry = Integer.parseInt(grpRights.getFtrRightsByValue("CB_LABSUMW")); }
	else
		updateLabSmmry = 4;
	
	if(grpRights.getFtrRightsByValue("CB_OTHERW")!=null && !grpRights.getFtrRightsByValue("CB_OTHERW").equals(""))
	{	updateOthrTstRslt = Integer.parseInt(grpRights.getFtrRightsByValue("CB_OTHERW")); }
	else
		updateOthrTstRslt = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CLNCLNOTESW")!=null && !grpRights.getFtrRightsByValue("CB_CLNCLNOTESW").equals(""))
	{	updateClnclNotes = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CLNCLNOTESW")); }
	else
		updateClnclNotes = 4;
	
	if(grpRights.getFtrRightsByValue("CB_ELIGIBLEW")!=null && !grpRights.getFtrRightsByValue("CB_ELIGIBLEW").equals(""))
	{	updateElgblty = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ELIGIBLEW")); }
	else
		updateElgblty = 4;
	if(grpRights.getFtrRightsByValue("CB_NOTESB")!=null && !grpRights.getFtrRightsByValue("CB_NOTESB").equals(""))
	{	viewNote = Integer.parseInt(grpRights.getFtrRightsByValue("CB_NOTESB"));}
	else
		viewNote = 4;	
	if(grpRights.getFtrRightsByValue("CB_NOTES")!=null && !grpRights.getFtrRightsByValue("CB_NOTES").equals(""))
	{	vClnclNote = Integer.parseInt(grpRights.getFtrRightsByValue("CB_NOTES"));}
	else
		vClnclNote = 4;
	
	if(grpRights.getFtrRightsByValue("CB_PCKNGSLIP")!=null && !grpRights.getFtrRightsByValue("CB_PCKNGSLIP").equals(""))
	{	crtePckngSlip = Integer.parseInt(grpRights.getFtrRightsByValue("CB_PCKNGSLIP"));}
	else
		crtePckngSlip = 4;
		
	if(grpRights.getFtrRightsByValue("CB_PATCENTRECMW")!=null && !grpRights.getFtrRightsByValue("CB_PATCENTRECMW").equals(""))
	{	viewPatienttc = Integer.parseInt(grpRights.getFtrRightsByValue("CB_PATCENTRECMW"));}
	else
		viewPatienttc = 4;
	
	if(grpRights.getFtrRightsByValue("CB_VPATANTIGENT")!=null && !grpRights.getFtrRightsByValue("CB_VPATANTIGENT").equals(""))
	{	vPatientAntgn = Integer.parseInt(grpRights.getFtrRightsByValue("CB_VPATANTIGENT"));}
	else
		vPatientAntgn = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CASEMNGRDIR")!=null && !grpRights.getFtrRightsByValue("CB_CASEMNGRDIR").equals(""))
	{	CaseMngrDir = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CASEMNGRDIR"));}
	else
		CaseMngrDir = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CBUAVAILCONW")!=null && !grpRights.getFtrRightsByValue("CB_CBUAVAILCONW").equals(""))
	{	viewCbuAvailCon = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBUAVAILCONW"));}
	else
		viewCbuAvailCon = 4;
	
	if(grpRights.getFtrRightsByValue("CB_REQCBUHISTW")!=null && !grpRights.getFtrRightsByValue("CB_REQCBUHISTW").equals(""))
	{	viewRqstCBUhis= Integer.parseInt(grpRights.getFtrRightsByValue("CB_REQCBUHISTW"));}
	else
		viewRqstCBUhis = 4;
	
	if(grpRights.getFtrRightsByValue("CB_REQCLNINFOW")!=null && !grpRights.getFtrRightsByValue("CB_REQCLNINFOW").equals(""))
	{	viewreqclincinfo= Integer.parseInt(grpRights.getFtrRightsByValue("CB_REQCLNINFOW"));}
	else
		viewreqclincinfo = 4;
	
	if(grpRights.getFtrRightsByValue("CB_PROGNOTESW")!=null && !grpRights.getFtrRightsByValue("CB_PROGNOTESW").equals(""))
	{	viewNotes  = Integer.parseInt(grpRights.getFtrRightsByValue("CB_PROGNOTESW"));}
	else
		viewNotes = 4;
	
	if(grpRights.getFtrRightsByValue("CB_HERESOLW")!=null && !grpRights.getFtrRightsByValue("CB_HERESOLW").equals(""))
	{	viewHEResol= Integer.parseInt(grpRights.getFtrRightsByValue("CB_HERESOLW"));}
	else
		viewHEResol = 4;
	
	if(grpRights.getFtrRightsByValue("CB_PROGRESNOTE")!=null && !grpRights.getFtrRightsByValue("CB_PROGRESNOTE").equals(""))
	{	vPrgrssNote = Integer.parseInt(grpRights.getFtrRightsByValue("CB_PROGRESNOTE"));}
	else
		vPrgrssNote = 4;
	
	if(grpRights.getFtrRightsByValue("CB_APLYRESOCT")!=null && !grpRights.getFtrRightsByValue("CB_APLYRESOCT").equals(""))
	{	applyResotoCT = Integer.parseInt(grpRights.getFtrRightsByValue("CB_APLYRESOCT"));}
	else
		applyResotoCT = 4;
	
	if(grpRights.getFtrRightsByValue("CB_FINALCBUREVW")!=null && !grpRights.getFtrRightsByValue("CB_FINALCBUREVW").equals(""))
	{	viewFnlCBURvw= Integer.parseInt(grpRights.getFtrRightsByValue("CB_FINALCBUREVW"));}
	else
		viewFnlCBURvw = 4;
	
	if(grpRights.getFtrRightsByValue("CB_TCORDRDETALW")!=null && !grpRights.getFtrRightsByValue("CB_TCORDRDETALW").equals(""))
	{	viewTCOrdrDetail = Integer.parseInt(grpRights.getFtrRightsByValue("CB_TCORDRDETALW"));}
	else
		viewTCOrdrDetail = 4;
	
	if(grpRights.getFtrRightsByValue("CB_ORRESOLW")!=null && !grpRights.getFtrRightsByValue("CB_ORRESOLW").equals(""))
	{	viewORResolution = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ORRESOLW"));}
	else
		viewORResolution = 4;
	
	if(grpRights.getFtrRightsByValue("CB_PROGNOTESW")!=null && !grpRights.getFtrRightsByValue("CB_PROGNOTESW").equals(""))
	{	viewPrgrssNotes  = Integer.parseInt(grpRights.getFtrRightsByValue("CB_PROGNOTESW"));}
	else
		viewPrgrssNotes = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CBUSPMNTINFW")!=null && !grpRights.getFtrRightsByValue("CB_CBUSPMNTINFW").equals(""))
	{viewCBUShipmnt  = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBUSPMNTINFW"));}
	else
		viewCBUShipmnt = 4;
	
	if(grpRights.getFtrRightsByValue("CB_ADCBUHLATYPW")!=null && !grpRights.getFtrRightsByValue("CB_ADCBUHLATYPW").equals(""))
	{	viewAddTypbyCBB = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ADCBUHLATYPW"));}
	else
		viewAddTypbyCBB = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CTRESOLTNW")!=null && !grpRights.getFtrRightsByValue("CB_CTRESOLTNW").equals(""))
	{	viewCTResolution = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CTRESOLTNW"));}
	else
		viewCTResolution = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CTSPMNTINFOW")!=null && !grpRights.getFtrRightsByValue("CB_CTSPMNTINFOW").equals(""))
	{	viewCTshipmnt= Integer.parseInt(grpRights.getFtrRightsByValue("CB_CTSPMNTINFOW"));}
	else
		viewCTshipmnt = 4;
	
	if(grpRights.getFtrRightsByValue("CB_VCTINS")!=null && !grpRights.getFtrRightsByValue("CB_VCTINS").equals(""))
	{	vCTinstns = Integer.parseInt(grpRights.getFtrRightsByValue("CB_VCTINS"));}
	else
		vCTinstns = 4;
	if(grpRights.getFtrRightsByValue("CB_PFPENDINGW")!=null && !grpRights.getFtrRightsByValue("CB_PFPENDINGW").equals(""))
	{	assignOrders = Integer.parseInt(grpRights.getFtrRightsByValue("CB_PFPENDINGW"));}
	else
		assignOrders = 4;
	if(grpRights.getFtrRightsByValue("CB_FINALELIGBLE")!=null && !grpRights.getFtrRightsByValue("CB_FINALELIGBLE").equals(""))
	{	conElgble = Integer.parseInt(grpRights.getFtrRightsByValue("CB_FINALELIGBLE"));}
	else
		conElgble = 4;
	/* --------View Upload Category document---------------*/
	int viewUploadCat=0;
	int viewUnitRepCat=0;
	int viewCbuInfocat=0;
	int viewLabSumCat=0;
	int viewProcesInfoCat=0;
	int viewIDMCat=0;
	int viewHealthHisCat=0;
	int viewHlaCat=0;
	int viewEligbCat=0;
	int viewOtherCat=0;
	int viewAssmentCat=0;
	int viewShipmentCat=0;
	int viewPostShipCat=0;
	int overRideDeferred=0;
	
	if(grpRights.getFtrRightsByValue("CB_UPLOADCAT")!=null && !grpRights.getFtrRightsByValue("CB_UPLOADCAT").equals(""))
	{	viewUploadCat = Integer.parseInt(grpRights.getFtrRightsByValue("CB_UPLOADCAT"));}
	else
		viewUploadCat = 4;
	if(grpRights.getFtrRightsByValue("CB_UNITCAT")!=null && !grpRights.getFtrRightsByValue("CB_UNITCAT").equals(""))
	{	viewUnitRepCat = Integer.parseInt(grpRights.getFtrRightsByValue("CB_UNITCAT"));}
	else
		viewUnitRepCat = 4;
	if(grpRights.getFtrRightsByValue("CB_CBUINFOCAT")!=null && !grpRights.getFtrRightsByValue("CB_CBUINFOCAT").equals(""))
	{	viewCbuInfocat = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBUINFOCAT"));}
	else
		viewCbuInfocat = 4;
	if(grpRights.getFtrRightsByValue("CB_LABSUMCAT")!=null && !grpRights.getFtrRightsByValue("CB_LABSUMCAT").equals(""))
	{	viewLabSumCat = Integer.parseInt(grpRights.getFtrRightsByValue("CB_LABSUMCAT"));}
	else
		viewLabSumCat = 4;
	if(grpRights.getFtrRightsByValue("CB_PROINFOCAT")!=null && !grpRights.getFtrRightsByValue("CB_PROINFOCAT").equals(""))
	{	viewProcesInfoCat = Integer.parseInt(grpRights.getFtrRightsByValue("CB_PROINFOCAT"));}
	else
		viewProcesInfoCat = 4;
	
	
	if(grpRights.getFtrRightsByValue("CB_IDMCAT")!=null && !grpRights.getFtrRightsByValue("CB_IDMCAT").equals(""))
	{	viewIDMCat = Integer.parseInt(grpRights.getFtrRightsByValue("CB_IDMCAT"));}
	else
		viewIDMCat = 4;
	if(grpRights.getFtrRightsByValue("CB_HEALTHCAT")!=null && !grpRights.getFtrRightsByValue("CB_HEALTHCAT").equals(""))
	{	viewHealthHisCat = Integer.parseInt(grpRights.getFtrRightsByValue("CB_HEALTHCAT"));}
	else
		viewHealthHisCat = 4;
	if(grpRights.getFtrRightsByValue("CB_HLACAT")!=null && !grpRights.getFtrRightsByValue("CB_HLACAT").equals(""))
	{	viewHlaCat = Integer.parseInt(grpRights.getFtrRightsByValue("CB_HLACAT"));}
	else
		viewHlaCat = 4;
	if(grpRights.getFtrRightsByValue("CB_ELIGCAT")!=null && !grpRights.getFtrRightsByValue("CB_ELIGCAT").equals(""))
	{	viewEligbCat = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ELIGCAT"));}
	else
		viewEligbCat = 4;
	if(grpRights.getFtrRightsByValue("CB_OTHRCAT")!=null && !grpRights.getFtrRightsByValue("CB_OTHRCAT").equals(""))
	{	viewOtherCat = Integer.parseInt(grpRights.getFtrRightsByValue("CB_OTHRCAT"));}
	else
		viewOtherCat = 4;
	if(grpRights.getFtrRightsByValue("CB_ASSMNTCAT")!=null && !grpRights.getFtrRightsByValue("CB_ASSMNTCAT").equals(""))
	{	viewAssmentCat = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ASSMNTCAT"));}
	else
		viewAssmentCat = 4;
	if(grpRights.getFtrRightsByValue("CB_SHPMNTCAT")!=null && !grpRights.getFtrRightsByValue("CB_SHPMNTCAT").equals(""))
	{	viewShipmentCat = Integer.parseInt(grpRights.getFtrRightsByValue("CB_SHPMNTCAT"));}
	else
		viewShipmentCat = 4;
	if(grpRights.getFtrRightsByValue("CB_POSTSHIPCAT")!=null && !grpRights.getFtrRightsByValue("CB_POSTSHIPCAT").equals(""))
	{	viewPostShipCat = Integer.parseInt(grpRights.getFtrRightsByValue("CB_POSTSHIPCAT"));}
	else
		viewPostShipCat = 4;
	if(grpRights.getFtrRightsByValue("CB_OVRIDEDEFR")!=null && !grpRights.getFtrRightsByValue("CB_OVRIDEDEFR").equals(""))
	 {	overRideDeferred = Integer.parseInt(grpRights.getFtrRightsByValue("CB_OVRIDEDEFR"));}
	else
		overRideDeferred = 4;
	
	request.setAttribute("viewUploadCat",viewUploadCat);
	request.setAttribute("viewUnitRepCat",viewUnitRepCat);
	request.setAttribute("viewCbuInfocat",viewCbuInfocat);
	request.setAttribute("viewLabSumCat",viewLabSumCat);
	request.setAttribute("viewProcesInfoCat",viewProcesInfoCat);
	request.setAttribute("viewIDMCat",viewIDMCat);
	request.setAttribute("viewHealthHisCat",viewHealthHisCat);
	request.setAttribute("viewHlaCat",viewHlaCat);
	request.setAttribute("viewEligbCat",viewEligbCat);
	request.setAttribute("viewOtherCat",viewOtherCat);
	request.setAttribute("viewAssmentCat",viewAssmentCat);
	request.setAttribute("viewShipmentCat",viewShipmentCat);
	request.setAttribute("viewPostShipCat",viewPostShipCat);
	/*-----------------------------------End------------*/
	request.setAttribute("updateINDstat",updateINDstat);
	request.setAttribute("updateCBUstatus",updateCBUstatus);
	request.setAttribute("updateEligStat",updateEligStat);
	request.setAttribute("updateLicStat",updateLicStat);
	request.setAttribute("updateCliStat",updateCliStat);
	request.setAttribute("updateUploadDoc",updateUploadDoc);
	request.setAttribute("updateUnitRepStat",updateUnitRepStat);	
	request.setAttribute("updateMinCriteria",updateMinCriteria);
	request.setAttribute("updateCBUAssment",updateCBUAssment);
	request.setAttribute("updateCBUReport",updateCBUReport);
	request.setAttribute("updateFlaggedItem",updateFlaggedItem);
	request.setAttribute("updateAuditTrail",updateAuditTrail);
	request.setAttribute("updateFinalCBURev",updateFinalCBURev);
	request.setAttribute("updateFlaggedItem",updateFlaggedItem);
	request.setAttribute("updateShipmentItern",updateShipmentItern);
	request.setAttribute("updateProductInsert",updateProductInsert);
	request.setAttribute("updateQuickLnkPanel",updateQuickLnkPanel);
	request.setAttribute("updateCBUInfowidget",updateCBUInfowidget);
	request.setAttribute("updateIDwidget",updateIDwidget);
	request.setAttribute("updateProInfoWidget",updateProInfoWidget);
	request.setAttribute("updateInfectDisease",updateInfectDisease);
	request.setAttribute("updateidmForm",updateidmForm);
	request.setAttribute("updateCbuHLA",updateCbuHLA);
	request.setAttribute("updateHLA_CBU",updateHLA_CBU);
	request.setAttribute("updateHLA_Matrnl",updateHLA_Matrnl);
	request.setAttribute("updateHLA_Patient",updateHLA_Patient);
	request.setAttribute("updateHlthHis",updateHlthHis);	
	request.setAttribute("updateHistry_Mrq",updateHistry_Mrq);
	request.setAttribute("updateHistry_FMHQ",updateHistry_FMHQ);
	request.setAttribute("updateLabSmmry",updateLabSmmry);
	request.setAttribute("updateOthrTstRslt",updateOthrTstRslt);
	request.setAttribute("updateClnclNotes",updateClnclNotes);
	request.setAttribute("updateElgblty",updateElgblty);
	request.setAttribute("viewNote",viewNote);
	request.setAttribute("vClnclNote",vClnclNote);
	request.setAttribute("crtePckngSlip",crtePckngSlip);
	request.setAttribute("viewPatienttc",viewPatienttc);
	request.setAttribute("vPatientAntgn",vPatientAntgn);
	request.setAttribute("CaseMngrDir",CaseMngrDir);
	request.setAttribute("viewCbuAvailCon",viewCbuAvailCon);
	request.setAttribute("viewRqstCBUhis",viewRqstCBUhis);
	request.setAttribute("viewreqclincinfo",viewreqclincinfo);
	request.setAttribute("viewNotes",viewNotes);
	request.setAttribute("viewHEResol",viewHEResol);
	request.setAttribute("vPrgrssNote",vPrgrssNote);
	request.setAttribute("applyResotoCT",applyResotoCT);
	request.setAttribute("viewFnlCBURvw",viewFnlCBURvw);
	request.setAttribute("viewTCOrdrDetail",viewTCOrdrDetail);
	request.setAttribute("viewORResolution",viewORResolution);
	request.setAttribute("viewPrgrssNotes",viewPrgrssNotes);
	request.setAttribute("viewCBUShipmnt",viewCBUShipmnt);
	request.setAttribute("viewCTResolution",viewCTResolution);
	request.setAttribute("viewAddTypbyCBB",viewAddTypbyCBB);
	request.setAttribute("viewCTshipmnt",viewCTshipmnt);
	request.setAttribute("vCTinstns",vCTinstns);
	request.setAttribute("assignOrders",assignOrders);
	request.setAttribute("conElgble",conElgble);
	request.setAttribute("overRideDeferred",overRideDeferred);
	/*--------------------Static Panel----------------*/
	int cmpltReqInfo = 0;

int updateCBUInfoS=0;
int staicPanl =0;
if(grpRights.getFtrRightsByValue("CB_COMPREQINFO")!=null && !grpRights.getFtrRightsByValue("CB_COMPREQINFO").equals(""))
{	cmpltReqInfo = Integer.parseInt(grpRights.getFtrRightsByValue("CB_COMPREQINFO"));}
else
	cmpltReqInfo = 4;
if(grpRights.getFtrRightsByValue("CB_CBUINFOS")!=null && !grpRights.getFtrRightsByValue("CB_CBUINFOS").equals(""))
{updateCBUInfoS = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBUINFOS")); }
else
	updateCBUInfoS = 4;
if(grpRights.getFtrRightsByValue("CB_STATPANEL")!=null && !grpRights.getFtrRightsByValue("CB_STATPANEL").equals(""))
{	staicPanl = Integer.parseInt(grpRights.getFtrRightsByValue("CB_STATPANEL"));}
else
	staicPanl = 4;	

request.setAttribute("cmpltReqInfo",cmpltReqInfo);
request.setAttribute("updateCBUInfoS",updateCBUInfoS);
request.setAttribute("staicPanl",staicPanl);
/*-----------------------End------------------*/
/*--------------------------*/
int editCBURght = 0;
	int viewCbuDetailRep = 0;
    int updateCbuRegid = 0;
	int updateCbuMaternalid = 0;
	
	if(grpRights.getFtrRightsByValue("CB_DETAILCBU")!=null && !grpRights.getFtrRightsByValue("CB_DETAILCBU").equals(""))
	{
		viewCbuDetailRep = Integer.parseInt(grpRights.getFtrRightsByValue("CB_DETAILCBU"));
	}
	else
		viewCbuDetailRep = 4;
	
	if(grpRights.getFtrRightsByValue("CB_ADD")!=null && !grpRights.getFtrRightsByValue("CB_ADD").equals(""))
	 {editCBURght = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ADD"));}
	else
		editCBURght = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CBUREGID")!=null && !grpRights.getFtrRightsByValue("CB_CBUREGID").equals(""))
	{
		updateCbuRegid = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBUREGID"));
	}
	else
		updateCbuRegid = 4;
	if(grpRights.getFtrRightsByValue("CB_MATREGID")!=null && !grpRights.getFtrRightsByValue("CB_MATREGID").equals(""))
	{
		updateCbuMaternalid = Integer.parseInt(grpRights.getFtrRightsByValue("CB_MATREGID"));
	}
	else
		updateCbuMaternalid = 4;
	
	
	request.setAttribute("editCBURght",editCBURght);
	request.setAttribute("viewCbuDetailRep",viewCbuDetailRep);
    request.setAttribute("updateCbuRegid",updateCbuRegid);
    request.setAttribute("updateCbuMaternalid",updateCbuMaternalid);
    
    //cb_show-cbb-defaults.jsp
    
    int editCbbPrfle = 0;
int viewCbbSetup = 0;
int veiwCbbInfo = 0;
int viewCbbpick = 0;
int viewDryShip = 0;
int ViewCbbDeflt = 0;
int ViewLastmodby = 0;
if(grpRights.getFtrRightsByValue("CB_CBBPROFILE")!=null && !grpRights.getFtrRightsByValue("CB_CBBPROFILE").equals(""))
	{	editCbbPrfle = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBBPROFILE"));}
else
	editCbbPrfle = 4;
if(grpRights.getFtrRightsByValue("CB_CBBSETUP")!=null && !grpRights.getFtrRightsByValue("CB_CBBSETUP").equals(""))
{	viewCbbSetup = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBBSETUP"));}
else
viewCbbSetup = 4;
if(grpRights.getFtrRightsByValue("CB_CBBINFO")!=null && !grpRights.getFtrRightsByValue("CB_CBBINFO").equals(""))
{	veiwCbbInfo = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBBINFO"));}
else
	veiwCbbInfo = 4;
if(grpRights.getFtrRightsByValue("CB_PICKUPADD")!=null && !grpRights.getFtrRightsByValue("CB_PICKUPADD").equals(""))
{	viewCbbpick = Integer.parseInt(grpRights.getFtrRightsByValue("CB_PICKUPADD"));}
else
	viewCbbpick = 4;
if(grpRights.getFtrRightsByValue("CB_SHIPRETADD")!=null && !grpRights.getFtrRightsByValue("CB_SHIPRETADD").equals(""))
{	viewDryShip = Integer.parseInt(grpRights.getFtrRightsByValue("CB_SHIPRETADD"));}
else
	viewDryShip = 4;
if(grpRights.getFtrRightsByValue("CB_VIEWCBBDFLTW")!=null && !grpRights.getFtrRightsByValue("CB_VIEWCBBDFLTW").equals(""))
{	ViewCbbDeflt = Integer.parseInt(grpRights.getFtrRightsByValue("CB_VIEWCBBDFLTW"));}
else
	ViewCbbDeflt = 4;
if(grpRights.getFtrRightsByValue("CB_LASTMODBY")!=null && !grpRights.getFtrRightsByValue("CB_LASTMODBY").equals(""))
{	ViewLastmodby = Integer.parseInt(grpRights.getFtrRightsByValue("CB_LASTMODBY"));}
else
	ViewLastmodby = 4;
request.setAttribute("editCbbPrfle",editCbbPrfle);
request.setAttribute("viewCbbSetup",viewCbbSetup);
request.setAttribute("veiwCbbInfo",veiwCbbInfo);
request.setAttribute("viewCbbpick",viewCbbpick);
request.setAttribute("viewDryShip",viewDryShip);
request.setAttribute("ViewCbbDeflt",ViewCbbDeflt);
request.setAttribute("ViewLastmodby",ViewLastmodby);



//cb_cord_entry_in_progress

	int newCBU = 0;
	int saveinprg = 0;
	int notavilCbu = 0;
	int cordImprt = 0;
	int ImprtXmlfle = 0;
	int cbuImprtHis = 0;
	
	if(grpRights.getFtrRightsByValue("CB_ADD")!=null && !grpRights.getFtrRightsByValue("CB_ADD").equals(""))
	 {newCBU = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ADD"));}
	else
		newCBU = 4;
	if(grpRights.getFtrRightsByValue("CB_CORDIMPORT")!=null && !grpRights.getFtrRightsByValue("CB_CORDIMPORT").equals(""))
	{cordImprt =Integer.parseInt(grpRights.getFtrRightsByValue("CB_CORDIMPORT"));}
	else
		cordImprt =4;	
	if(grpRights.getFtrRightsByValue("CB_SAVEINCBU")!=null && !grpRights.getFtrRightsByValue("CB_SAVEINCBU").equals(""))
	{saveinprg =Integer.parseInt(grpRights.getFtrRightsByValue("CB_SAVEINCBU"));}
	else
		saveinprg =4;
	if(grpRights.getFtrRightsByValue("CB_NOTAVAILCBU")!=null && !grpRights.getFtrRightsByValue("CB_NOTAVAILCBU").equals(""))
	{notavilCbu =Integer.parseInt(grpRights.getFtrRightsByValue("CB_NOTAVAILCBU"));}
	else
		notavilCbu =4;
	if(grpRights.getFtrRightsByValue("CB_IMPRTXML")!=null && !grpRights.getFtrRightsByValue("CB_IMPRTXML").equals(""))
	{ImprtXmlfle =Integer.parseInt(grpRights.getFtrRightsByValue("CB_IMPRTXML"));}
	else
		ImprtXmlfle =4;
	if(grpRights.getFtrRightsByValue("CB_IMPRTHIST")!=null && !grpRights.getFtrRightsByValue("CB_IMPRTHIST").equals(""))
	{cbuImprtHis =Integer.parseInt(grpRights.getFtrRightsByValue("CB_IMPRTHIST"));}
	else
		cbuImprtHis =4;
	request.setAttribute("newCBU",newCBU);
	request.setAttribute("cordImprt",cordImprt);
	request.setAttribute("saveinprg",saveinprg);
	request.setAttribute("notavilCbu",notavilCbu);
	request.setAttribute("ImprtXmlfle",ImprtXmlfle);
    request.setAttribute("cbuImprtHis",cbuImprtHis);
    
    
    int viewCTShipReport=0;
	int viewCTSampReport=0;
	int viewHEReport=0;
	int viewORReport=0;
	
	
	
	if(grpRights.getFtrRightsByValue("CB_CTSHPREPORT")!=null && !grpRights.getFtrRightsByValue("CB_CTSHPREPORT").equals(""))
	{
		viewCTShipReport = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CTSHPREPORT"));
	}
	else
		viewCTShipReport = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CTLABREPORT")!=null && !grpRights.getFtrRightsByValue("CB_CTLABREPORT").equals(""))
	{
		viewCTSampReport = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CTLABREPORT"));
	}
	else
		viewCTSampReport = 4;
	
	
	if(grpRights.getFtrRightsByValue("CB_HEREPORT")!=null && !grpRights.getFtrRightsByValue("CB_HEREPORT").equals(""))
	{
		viewHEReport = Integer.parseInt(grpRights.getFtrRightsByValue("CB_HEREPORT"));
	}
	else
		viewHEReport = 4;
	
	if(grpRights.getFtrRightsByValue("CB_ORREPORT")!=null && !grpRights.getFtrRightsByValue("CB_ORREPORT").equals(""))
	{
		viewORReport = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ORREPORT"));
	}
	else
		viewORReport = 4;
	
	
	request.setAttribute("viewCTShipReport",viewCTShipReport);
	request.setAttribute("viewCTSampReport",viewCTSampReport);
	request.setAttribute("viewHEReport",viewHEReport);
	request.setAttribute("viewORReport",viewORReport);
	
	int eligbleSumQues=0;
	if(grpRights.getFtrRightsByValue("CB_ELIGSUMQUEST")!=null && !grpRights.getFtrRightsByValue("CB_ELIGSUMQUEST").equals(""))
	{
		eligbleSumQues = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ELIGSUMQUEST"));
	}
	else
		eligbleSumQues = 4;
	
	request.setAttribute("eligbleSumQues",eligbleSumQues);
	//System.out.println("eligbleSumQues--------->"+eligbleSumQues);
%>