<%@ taglib prefix="s"  uri="/struts-tags"%>
<script>
  jQuery.validator.addMethod("validateRadioRequired", function(value, element,params) {
	var flag = false;
	$j('[name='+params[0]+']').each(function(){
	   if($j(this).attr('checked'))
	    {
		   flag = true;                    
	    }                        
	});
	return (true && flag);		 
	}, "<s:text name="garuda.cbu.cordentry.question"/>");

var new_elig_quest_validtaion_rules =  {"declNewPojo.mrqNewQues1":{validateRadioRequired:["declNewPojo.mrqNewQues1"]},
		"declNewPojo.mrqNewQues1a":{validateRadioRequired:{
			                param:["declNewPojo.mrqNewQues1a"],
							depends: function(element) {
                                var flag = false;
								$j('[name=declNewPojo.mrqNewQues1]').each(function(){
					                if($j(this).attr('checked'))
					                {
                                        if($j(this).val()=='false')
                                        flag = true;     
					                                
					                }                        
					            });
							    return flag;
							 }
						   }
		                  },
	  "declNewPojo.mrqNewQues1AddDetail":{required:{
				                depends: function(element) {
		                        var flag = false;
								$j('[name=declNewPojo.mrqNewQues1]').each(function(){
					                if($j(this).attr('checked'))
					                {
					                	if($j(this).val()=='false')
							            flag = true;     
					                                
					                }                        
					            });
							    return flag;
							 }
						   }
		                 },									                  
		"declNewPojo.mrqNewQues2":{validateRadioRequired:["declNewPojo.mrqNewQues2"]},
		"declNewPojo.mrqNewQues2AddDetail":{required:{
					                depends: function(element) {
                                        var flag = false;
										$j('[name=declNewPojo.mrqNewQues2]').each(function(){
							                if($j(this).attr('checked'))
							                {
							                	if($j(this).val()=='false')
									            flag = true;     
							                                
							                }                        
							            });
									    return flag;
									 }
								   }
				                  },
		
	     "declNewPojo.mrqNewQues3":{validateRadioRequired:["declNewPojo.mrqNewQues3"]},
		 "declNewPojo.mrqNewQues3AddDetail":{required:{
								     depends: function(element) {
								       var flag = false;
								     $j('[name=declNewPojo.mrqNewQues3]').each(function(){
									   if($j(this).attr('checked'))
									   {
									     	if($j(this).val()=='true')
									            flag = true;     
									   }                        
									});
									return flag;
									}
									}
						        },
	
		"declNewPojo.phyNewFindQues4":{validateRadioRequired:["declNewPojo.phyNewFindQues4"]},
		"declNewPojo.phyNewFindQues4a":{validateRadioRequired:{
																param:["declNewPojo.phyNewFindQues4a"],
																depends: function(element) {
												                 var flag = false;
																 $j('[name=declNewPojo.phyNewFindQues4]').each(function(){
																		if($j(this).attr('checked'))
																			  {
												                                    if($j(this).val()=='false')
												                                                flag = true;     
																			                                
																			                }                        
																			            });
																					    return flag;
																					 }
																				   }
																                  },
		"declNewPojo.phyNewFindQues4AddDetail":{required:{
														depends: function(element) {
														var flag = false;
														$j('[name=declNewPojo.phyNewFindQues4]').each(function(){
															if($j(this).attr('checked'))
																			   {
																			      if($j(this).val()=='false')
																					flag = true;     
																			                                
																			                }                        
																			            });
																					    return flag;
																					 }
																				   }
																                 },			
  "declNewPojo.phyNewFindQues5":{validateRadioRequired:["declNewPojo.phyNewFindQues5"]},
  "declNewPojo.phyNewFindQues5AddDetail":{required:{
							                depends: function(element) {
						                      var flag = false;
												$j('[name=declNewPojo.phyNewFindQues5]').each(function(){
									                if($j(this).attr('checked'))
									                {
									                	if($j(this).val()=='true')
											            flag = true;     
									                                
									                }                        
									            });
											    return flag;
											 }
										   }
						                },
"declNewPojo.phyNewFindQues6":{validateRadioRequired:["declNewPojo.phyNewFindQues6"]},
"declNewPojo.phyNewFindQues6a":{validateRadioRequired:{
							                param:["declNewPojo.phyNewFindQues6a"],
											depends: function(element) {
							                   var flag = false;
												$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
									                if($j(this).attr('checked'))
									                {
							                           if($j(this).val()=='true')
							                           flag = true;     
									                                
									                }                        
									            });
											    return flag;
											 }
										   }
							              },
"declNewPojo.phyNewFindQues6b":{validateRadioRequired:{
								                param:["declNewPojo.phyNewFindQues6b"],
												depends: function(element) {
								                   var flag = false;
													$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
										                if($j(this).attr('checked'))
										                {
								                           if($j(this).val()=='true')
								                           flag = true;     
										                                
										                }                        
										            });
												    return flag;
												 }
											   }
								              },
"declNewPojo.phyNewFindQues6c":{validateRadioRequired:{
									                param:["declNewPojo.phyNewFindQues6c"],
													depends: function(element) {
									                   var flag = false;
														$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
											                if($j(this).attr('checked'))
											                {
									                           if($j(this).val()=='true')
									                           flag = true;     
											                                
											                }                        
											            });
													    return flag;
													 }
												   }
									              },
"declNewPojo.phyNewFindQues6d":{validateRadioRequired:{
										                param:["declNewPojo.phyNewFindQues6d"],
														depends: function(element) {
										                   var flag = false;
															$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
												                if($j(this).attr('checked'))
												                {
										                           if($j(this).val()=='true')
										                           flag = true;     
												                                
												                }                        
												            });
														    return flag;
														 }
													   }
										              },
"declNewPojo.phyNewFindQues6e":{validateRadioRequired:{
											                param:["declNewPojo.phyNewFindQues6e"],
															depends: function(element) {
											                   var flag = false;
																$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
													                if($j(this).attr('checked'))
													                {
											                           if($j(this).val()=='true')
											                           flag = true;     
													                                
													                }                        
													            });
															    return flag;
															 }
														   }
											              },
"declNewPojo.phyNewFindQues6f":{validateRadioRequired:{
												                param:["declNewPojo.phyNewFindQues6f"],
																depends: function(element) {
												                   var flag = false;
																	$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
														                if($j(this).attr('checked'))
														                {
												                           if($j(this).val()=='true')
												                           flag = true;     
														                                
														                }                        
														            });
																    return flag;
																 }
															   }
												              },
"declNewPojo.phyNewFindQues6g":{validateRadioRequired:{
													                param:["declNewPojo.phyNewFindQues6g"],
																	depends: function(element) {
													                   var flag = false;
																		$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
															                if($j(this).attr('checked'))
															                {
													                           if($j(this).val()=='true')
													                           flag = true;     
															                                
															                }                        
															            });
																	    return flag;
																	 }
																   }
													              },
"declNewPojo.phyNewFindQues6h":{validateRadioRequired:{
										                param:["declNewPojo.phyNewFindQues6h"],
														depends: function(element) {
										                   var flag = false;
															$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
												                if($j(this).attr('checked'))
												                {
										                           if($j(this).val()=='true')
										                           flag = true;     
												                                
												                }                        
												            });
														    return flag;
														 }
													   }
										              },
"declNewPojo.phyNewFindQues6i":{validateRadioRequired:{
											                param:["declNewPojo.phyNewFindQues6i"],
															depends: function(element) {
											                   var flag = false;
																$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
													                if($j(this).attr('checked'))
													                {
											                           if($j(this).val()=='true')
											                           flag = true;     
													                                
													                }                        
													            });
															    return flag;
															 }
														   }
											              },
"declNewPojo.phyNewFindQues6AddDetail":{required:{
								                depends: function(element) {
							                      var flag = false;
													$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
										                if($j(this).attr('checked'))
										                {
										                	if($j(this).val()=='true')
												            flag = true;     
										                                
										                }                        
										            });
												    return flag;
												 }
											   }
							                },

	
"declNewPojo.idmNewQues7":{validateRadioRequired:["declNewPojo.idmNewQues7"]},
"declNewPojo.idmNewQues7AddDetail":{required:{
					                depends: function(element) {
			                      var flag = false;
									$j('[name=declNewPojo.idmNewQues7]').each(function(){
						                if($j(this).attr('checked'))
						                {
						                	if($j(this).val()=='false')
								            flag = true;
						                }                        
						            });
								    return flag;
								 }
							   }
				                										             						                													                
},									             
"declNewPojo.idmNewQues7a":{validateRadioRequired:{
										               param:["declNewPojo.idmNewQues7a"],
														depends: function(element) {
										                  var flag = false;
															$j('[name=declNewPojo.idmNewQues7]').each(function(){
												                if($j(this).attr('checked'))
												                {
										                          if($j(this).val()=='false')
										                          flag = true;     
												                                
												                }                        
												            });
														    return flag;
														 }
													   }
										             },
	"declNewPojo.idmNewQues8":{validateRadioRequired:["declNewPojo.idmNewQues8"]},		
	"declNewPojo.idmNewQues8a":{validateRadioRequired:{
           param:["declNewPojo.idmNewQues8a"],
			depends: function(element) {
              var flag = false;
				$j('[name=declNewPojo.idmNewQues8]').each(function(){
	                if($j(this).attr('checked'))
	                {
                      if($j(this).val()=='false')
                      flag = true;     
	                                
	                }                        
	            });
			    return flag;
			 }
		   }
         },
	"declNewPojo.idmNewQues8AddDetail":{required:{
											                depends: function(element) {
										                      var flag = false;
																$j('[name=declNewPojo.idmNewQues8]').each(function(){
													                if($j(this).attr('checked'))
													                {
													                	if($j(this).val()=='false')
															            flag = true;     
													                                
													                }                        
													            });
															    return flag;
															 }
														   }
										                }		,
	"declNewPojo.idmNewQues9":{validateRadioRequired:["declNewPojo.idmNewQues9"]},		
	"declNewPojo.idmNewQues9a":{validateRadioRequired:{
																param:["declNewPojo.idmNewQues9a"],
										 						depends: function(element) {
										 				        var flag = false;
										 						$j('[name=declNewPojo.idmNewQues9]').each(function(){
										 				        if($j(this).attr('checked'))
										 				        {
										 				             if($j(this).val()=='false')
										 				              flag = true;     
										 				        }                        
										 				});
										 				return flag;
										 				}
										 				}
										 				},
	"declNewPojo.idmNewQues9AddDetail":{required:{
        depends: function(element) {
          var flag = false;
			$j('[name=declNewPojo.idmNewQues9]').each(function(){
                if($j(this).attr('checked'))
                {
                	if($j(this).val()=='true')
		            flag = true;     
                                
                }                        
            });
		    return flag;
		 }
	   }
    }};
	
var new_cord_entry_elig_quest_validtaion_rules =  {
		"declNewPojo.mrqNewQues1a":{validateRadioRequired:{
			                param:["declNewPojo.mrqNewQues1a"],
							depends: function(element) {
                                var flag = false;
								if($j("#cordSearchable").val()=='0'){	
							
                                    return false;
                  		        }
								$j('[name=declNewPojo.mrqNewQues1]').each(function(){
					                if($j(this).attr('checked'))
					                {
                                        if($j(this).val()=='false')
                                        flag = true;     
					                    
					                }                        
					            });
							    return flag;
							 }
						   }
		                  },
	  "declNewPojo.mrqNewQues1AddDetail":{required:{
				                depends: function(element) {
		                        var flag = false;
								if($j("#cordSearchable").val()=='0'){	
							
                                    return false;
                  		        }
								$j('[name=declNewPojo.mrqNewQues1]').each(function(){
					                if($j(this).attr('checked'))
					                {
					                	if($j(this).val()=='false')
							            flag = true;
							            
					                                
					                }                        
					            });
							    return flag;
							 }
						   }
		                 },									                  
		"declNewPojo.mrqNewQues2AddDetail":{required:{
					                depends: function(element) {
                                        var flag = false;
										if($j("#cordSearchable").val()=='0'){	
							
                                    return false;
                  		        }
										$j('[name=declNewPojo.mrqNewQues2]').each(function(){
							                if($j(this).attr('checked'))
							                {
							                	if($j(this).val()=='false')
									            flag = true;     
							                                
							                }                        
							            });
									    return flag;
									 }
								   }
				                  },
		
		"declNewPojo.mrqNewQues3AddDetail":{required:{
												     depends: function(element) {
							                          var flag = false;
													  if($j("#cordSearchable").val()=='0'){	
							
                                    return false;
                  		        }
													  $j('[name=declNewPojo.mrqNewQues3]').each(function(){
														                if($j(this).attr('checked'))
														                {
														                	if($j(this).val()=='true')
																            flag = true;     
														                                
														                }                        
														            });
																    return flag;
																 }
															   }
											                  },
	
		"declNewPojo.phyNewFindQues4A":{validateRadioRequired:{
																param:["declNewPojo.phyNewFindQues4A"],
																depends: function(element) {
												                 var flag = false;
																 if($j("#cordSearchable").val()=='0'){	
							
                                    return false;
                  		        }
																 $j('[name=declNewPojo.phyNewFindQues4]').each(function(){
																		if($j(this).attr('checked'))
																			  {
												                                    if($j(this).val()=='false')
												                                                flag = true;     
																			                                
																			                }                        
																			            });
																					    return flag;
																					 }
																				   }
																                  },
		"declNewPojo.phyNewFindQues4AddDetail":{required:{
														depends: function(element) {
														var flag = false;
														if($j("#cordSearchable").val()=='0'){	
							
                                    return false;
                  		        }
														$j('[name=declNewPojo.phyNewFindQues4]').each(function(){
															if($j(this).attr('checked'))
																			   {
																			      if($j(this).val()=='false')
																					flag = true;     
																			                                
																			                }                        
																			            });
																					    return flag;
																					 }
																				   }
																                 },			
  "declNewPojo.phyNewFindQues5AddDetail":{required:{
							                depends: function(element) {
						                      var flag = false;
											  if($j("#cordSearchable").val()=='0'){	
							
                                    return false;
                  		        }
												$j('[name=declNewPojo.phyNewFindQues5]').each(function(){
									                if($j(this).attr('checked'))
									                {
									                	if($j(this).val()=='true')
											            flag = true;     
									                                
									                }                        
									            });
											    return flag;
											 }
										   }
						                },
"declNewPojo.phyNewFindQues6a":{validateRadioRequired:{
							                param:["declNewPojo.phyNewFindQues6a"],
											depends: function(element) {
							                   var flag = false;
											   if($j("#cordSearchable").val()=='0'){	
							
                                    return false;
                  		        }
												$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
									                if($j(this).attr('checked'))
									                {
							                           if($j(this).val()=='true')
							                           flag = true;     
									                                
									                }                        
									            });
											    return flag;
											 }
										   }
							              },
"declNewPojo.phyNewFindQues6b":{validateRadioRequired:{
								                param:["declNewPojo.phyNewFindQues6b"],
												depends: function(element) {
								                   var flag = false;
												   if($j("#cordSearchable").val()=='0'){	
							
                                    return false;
                  		        }
													$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
										                if($j(this).attr('checked'))
										                {
								                           if($j(this).val()=='true')
								                           flag = true;     
										                                
										                }                        
										            });
												    return flag;
												 }
											   }
								              },
"declNewPojo.phyNewFindQues6c":{validateRadioRequired:{
									                param:["declNewPojo.phyNewFindQues6c"],
													depends: function(element) {
									                   var flag = false;
													   if($j("#cordSearchable").val()=='0'){	
							
                                    return false;
                  		        }
														$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
											                if($j(this).attr('checked'))
											                {
									                           if($j(this).val()=='true')
									                           flag = true;     
											                                
											                }                        
											            });
													    return flag;
													 }
												   }
									              },
"declNewPojo.phyNewFindQues6d":{validateRadioRequired:{
										                param:["declNewPojo.phyNewFindQues6d"],
														depends: function(element) {
										                   var flag = false;
														   if($j("#cordSearchable").val()=='0'){	
							
                                    return false;
                  		        }
															$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
												                if($j(this).attr('checked'))
												                {
										                           if($j(this).val()=='true')
										                           flag = true;     
												                                
												                }                        
												            });
														    return flag;
														 }
													   }
										              },
"declNewPojo.phyNewFindQues6e":{validateRadioRequired:{
											                param:["declNewPojo.phyNewFindQues6e"],
															depends: function(element) {
											                   var flag = false;
															   if($j("#cordSearchable").val()=='0'){	
							
                                    return false;
                  		        }
																$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
													                if($j(this).attr('checked'))
													                {
											                           if($j(this).val()=='true')
											                           flag = true;     
													                                
													                }                        
													            });
															    return flag;
															 }
														   }
											              },
"declNewPojo.phyNewFindQues6f":{validateRadioRequired:{
												                param:["declNewPojo.phyNewFindQues6f"],
																depends: function(element) {
												                   var flag = false;
																   if($j("#cordSearchable").val()=='0'){	
							
                                    return false;
                  		        }
																	$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
														                if($j(this).attr('checked'))
														                {
												                           if($j(this).val()=='true')
												                           flag = true;     
														                                
														                }                        
														            });
																    return flag;
																 }
															   }
												              },
"declNewPojo.phyNewFindQues6g":{validateRadioRequired:{
													                param:["declNewPojo.phyNewFindQues6g"],
																	depends: function(element) {
													                   var flag = false;
																	   if($j("#cordSearchable").val()=='0'){	
							
                                    return false;
                  		        }
																		$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
															                if($j(this).attr('checked'))
															                {
													                           if($j(this).val()=='true')
													                           flag = true;     
															                                
															                }                        
															            });
																	    return flag;
																	 }
																   }
													              },
"declNewPojo.phyNewFindQues6h":{validateRadioRequired:{
										                param:["declNewPojo.phyNewFindQues6h"],
														depends: function(element) {
										                   var flag = false;
														   if($j("#cordSearchable").val()=='0'){	
							
                                    return false;
                  		        }
															$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
												                if($j(this).attr('checked'))
												                {
										                           if($j(this).val()=='true')
										                           flag = true;     
												                                
												                }                        
												            });
														    return flag;
														 }
													   }
										              },
"declNewPojo.phyNewFindQues6i":{validateRadioRequired:{
											                param:["declNewPojo.phyNewFindQues6i"],
															depends: function(element) {
											                   var flag = false;
															   if($j("#cordSearchable").val()=='0'){	
							
                                    return false;
                  		        }
																$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
													                if($j(this).attr('checked'))
													                {
											                           if($j(this).val()=='true')
											                           flag = true;     
													                                
													                }                        
													            });
															    return flag;
															 }
														   }
											              },
"declNewPojo.phyNewFindQues6AddDetail":{required:{
								                depends: function(element) {
							                      var flag = false;
												  if($j("#cordSearchable").val()=='0'){	
							
                                    return false;
                  		        }
													$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
										                if($j(this).attr('checked'))
										                {
										                	if($j(this).val()=='true')
												            flag = true;     
										                                
										                }                        
										            });
												    return flag;
												 }
											   }
							                },

	
"declNewPojo.idmNewQues7AddDetail":{required:{
					                depends: function(element) {
			                      var flag = false;
								  if($j("#cordSearchable").val()=='0'){	
							
                                    return false;
                  		        }
									$j('[name=declNewPojo.idmNewQues7]').each(function(){
						                if($j(this).attr('checked'))
						                {
						                	if($j(this).val()=='false')
								            flag = true;
						                }                        
						            });
								    return flag;
								 }
							   }
				                										             						                													                
},									             
"declNewPojo.idmNewQues7a":{validateRadioRequired:{
										               param:["declNewPojo.idmNewQues7a"],
														depends: function(element) {
										                  var flag = false;
														  if($j("#cordSearchable").val()=='0'){	
							
                                    return false;
                  		        }
															$j('[name=declNewPojo.idmNewQues7]').each(function(){
												                if($j(this).attr('checked'))
												                {
										                          if($j(this).val()=='false')
										                          flag = true;     
												                                
												                }                        
												            });
														    return flag;
														 }
													   }
										             },
	"declNewPojo.idmNewQues8a":{validateRadioRequired:{
           param:["declNewPojo.idmNewQues8a"],
			depends: function(element) {
              var flag = false;
			  if($j("#cordSearchable").val()=='0'){	
							
                                    return false;
                  		        }
				$j('[name=declNewPojo.idmNewQues8]').each(function(){
	                if($j(this).attr('checked'))
	                {
                      if($j(this).val()=='false')
                      flag = true;     
	                                
	                }                        
	            });
			    return flag;
			 }
		   }
         },
	"declNewPojo.idmNewQues8AddDetail":{required:{
											                depends: function(element) {
										                      var flag = false;
															  if($j("#cordSearchable").val()=='0'){	
							
                                    return false;
                  		        }
																$j('[name=declNewPojo.idmNewQues8]').each(function(){
													                if($j(this).attr('checked'))
													                {
													                	if($j(this).val()=='false')
															            flag = true;     
													                                
													                }                        
													            });
															    return flag;
															 }
														   }
										                }		,
		
	"declNewPojo.idmNewQues9AddDetail":{required:{
        depends: function(element) {
          var flag = false;
		  if($j("#cordSearchable").val()=='0'){	
							
                                    return false;
                  		        }
			$j('[name=declNewPojo.idmNewQues9]').each(function(){
                if($j(this).attr('checked'))
                {
                	if($j(this).val()=='true')
		            flag = true;     
                                
                }                        
            });
		    return flag;
		 }
	   }
    }};


var old_elig_quest_validtaion_rules = {"declPojo.mrqQues1":{validateRadioRequired:["declPojo.mrqQues1"]},
		"declPojo.mrqQues1a":{validateRadioRequired:{
			                param:["declPojo.mrqQues1a"],
							depends: function(element) {
                                var flag = false;
								$j('[name=declPojo.mrqQues1]').each(function(){
					                if($j(this).attr('checked'))
					                {
                                        if($j(this).val()=='false')
                                        flag = true;     
					                                
					                }                        
					            });
							    return flag;
							 }
						   }
		                  },
	  "declPojo.mrqQues1AddDetail":{required:{
				                depends: function(element) {
		                        var flag = false;
								$j('[name=declPojo.mrqQues1]').each(function(){
					                if($j(this).attr('checked'))
					                {
					                	if($j(this).val()=='false')
							            flag = true;     
					                                
					                }                        
					            });
							    return flag;
							 }
						   }
		                 },									                  
		"declPojo.mrqQues1b":{validateRadioRequired:{
				                param:["declPojo.mrqQues1b"],
								depends: function(element) {
                                    var flag = false;
									$j('[name=declPojo.mrqQues1a]').each(function(){
						                if($j(this).attr('checked'))
						                {
						                	if($j(this).val()=='false')
								            flag = true;     
						                                
						                }                        
						            });
								    return flag;
								 }
							   }
			                  },
		"declPojo.mrqQues2AddDetail":{required:{
					                depends: function(element) {
                                        var flag = false;
										$j('[name=declPojo.mrqQues2]').each(function(){
							                if($j(this).attr('checked'))
							                {
							                	if($j(this).val()=='true')
									            flag = true;     
							                                
							                }                        
							            });
									    return flag;
									 }
								   }
				                  },
	   "declPojo.phyFindQues3":{validateRadioRequired:["declPojo.mrqQues1"]},
	   "declPojo.phyFindQues3a":{validateRadioRequired:{
					                param:["declPojo.phyFindQues3a"],
									depends: function(element) {
			                           var flag = false;
										$j('[name=declPojo.phyFindQues3]').each(function(){
							                if($j(this).attr('checked'))
							                {
			                                   if($j(this).val()=='false')
			                                   flag = true;     
							                                
							                }                        
							            });
									    return flag;
									 }
								   }
				                  },
	"declPojo.phyFindQues3c":{validateRadioRequired:{
						                param:["declPojo.phyFindQues3c"],
										depends: function(element) {
				                           var flag = false;
											$j('[name=declPojo.phyFindQues3]').each(function(){
								                if($j(this).attr('checked'))
								                {
				                                   if($j(this).val()=='false')
				                                   flag = true;     
								                                
								                }                        
								            });
										    return flag;
										 }
									   }
					                  },
	"declPojo.phyFindQues3AddDetail":{required:{
							                depends: function(element) {
		                                        var flag = false;
												$j('[name=declPojo.phyFindQues3]').each(function(){
									                if($j(this).attr('checked'))
									                {
									                	if($j(this).val()=='false')
											            flag = true;     
									                                
									                }                        
									            });
											    return flag;
											 }
										   }
						                  },
	"declPojo.phyFindQues3b":{validateRadioRequired:{
								                param:["declPojo.phyFindQues3b"],
												depends: function(element) {
						                           var flag = false;
													$j('[name=declPojo.phyFindQues3c]').each(function(){
										                if($j(this).attr('checked'))
										                {
						                                   if($j(this).val()=='false')
						                                   flag = true;     
										                                
										                }                        
										            });
												    return flag;
												 }
											   }
							                 },
	"declPojo.phyFindQues3d":{validateRadioRequired:{
									                param:["declPojo.phyFindQues3d"],
													depends: function(element) {
							                           var flag = false;
														$j('[name=declPojo.phyFindQues3c]').each(function(){
											                if($j(this).attr('checked'))
											                {
							                                   if($j(this).val()=='false')
							                                   flag = true;     
											                                
											                }                        
											            });
													    return flag;
													 }
												   }
								                  },
  "declPojo.phyFindQues4":{validateRadioRequired:["declPojo.phyFindQues4"]},
  "declPojo.phyFindQues4AddDetail":{required:{
							                depends: function(element) {
						                      var flag = false;
												$j('[name=declPojo.phyFindQues4]').each(function(){
									                if($j(this).attr('checked'))
									                {
									                	if($j(this).val()=='true')
											            flag = true;     
									                                
									                }                        
									            });
											    return flag;
											 }
										   }
						                },
"declPojo.phyFindQues5":{validateRadioRequired:["declPojo.phyFindQues5"]},
"declPojo.phyFindQues5a":{validateRadioRequired:{
							                param:["declPojo.phyFindQues5a"],
											depends: function(element) {
							                   var flag = false;
												$j('[name=declPojo.phyFindQues5]').each(function(){
									                if($j(this).attr('checked'))
									                {
							                           if($j(this).val()=='true')
							                           flag = true;     
									                                
									                }                        
									            });
											    return flag;
											 }
										   }
							              },
"declPojo.phyFindQues5b":{validateRadioRequired:{
								                param:["declPojo.phyFindQues5b"],
												depends: function(element) {
								                   var flag = false;
													$j('[name=declPojo.phyFindQues5]').each(function(){
										                if($j(this).attr('checked'))
										                {
								                           if($j(this).val()=='true')
								                           flag = true;     
										                                
										                }                        
										            });
												    return flag;
												 }
											   }
								              },
"declPojo.phyFindQues5c":{validateRadioRequired:{
									                param:["declPojo.phyFindQues5c"],
													depends: function(element) {
									                   var flag = false;
														$j('[name=declPojo.phyFindQues5]').each(function(){
											                if($j(this).attr('checked'))
											                {
									                           if($j(this).val()=='true')
									                           flag = true;     
											                                
											                }                        
											            });
													    return flag;
													 }
												   }
									              },
"declPojo.phyFindQues5d":{validateRadioRequired:{
										                param:["declPojo.phyFindQues5d"],
														depends: function(element) {
										                   var flag = false;
															$j('[name=declPojo.phyFindQues5]').each(function(){
												                if($j(this).attr('checked'))
												                {
										                           if($j(this).val()=='true')
										                           flag = true;     
												                                
												                }                        
												            });
														    return flag;
														 }
													   }
										              },
"declPojo.phyFindQues5e":{validateRadioRequired:{
											                param:["declPojo.phyFindQues5e"],
															depends: function(element) {
											                   var flag = false;
																$j('[name=declPojo.phyFindQues5]').each(function(){
													                if($j(this).attr('checked'))
													                {
											                           if($j(this).val()=='true')
											                           flag = true;     
													                                
													                }                        
													            });
															    return flag;
															 }
														   }
											              },
"declPojo.phyFindQues5f":{validateRadioRequired:{
												                param:["declPojo.phyFindQues5f"],
																depends: function(element) {
												                   var flag = false;
																	$j('[name=declPojo.phyFindQues5]').each(function(){
														                if($j(this).attr('checked'))
														                {
												                           if($j(this).val()=='true')
												                           flag = true;     
														                                
														                }                        
														            });
																    return flag;
																 }
															   }
												              },
"declPojo.phyFindQues5g":{validateRadioRequired:{
													                param:["declPojo.phyFindQues5g"],
																	depends: function(element) {
													                   var flag = false;
																		$j('[name=declPojo.phyFindQues5]').each(function(){
															                if($j(this).attr('checked'))
															                {
													                           if($j(this).val()=='true')
													                           flag = true;     
															                                
															                }                        
															            });
																	    return flag;
																	 }
																   }
													              },
"declPojo.phyFindQues5h":{validateRadioRequired:{
										                param:["declPojo.phyFindQues5h"],
														depends: function(element) {
										                   var flag = false;
															$j('[name=declPojo.phyFindQues5]').each(function(){
												                if($j(this).attr('checked'))
												                {
										                           if($j(this).val()=='true')
										                           flag = true;     
												                                
												                }                        
												            });
														    return flag;
														 }
													   }
										              },
"declPojo.phyFindQues5i":{validateRadioRequired:{
											                param:["declPojo.phyFindQues5i"],
															depends: function(element) {
											                   var flag = false;
																$j('[name=declPojo.phyFindQues5]').each(function(){
													                if($j(this).attr('checked'))
													                {
											                           if($j(this).val()=='true')
											                           flag = true;     
													                                
													                }                        
													            });
															    return flag;
															 }
														   }
											              },
"declPojo.phyFindQues5AddDetail":{required:{
								                depends: function(element) {
							                      var flag = false;
													$j('[name=declPojo.phyFindQues5]').each(function(){
										                if($j(this).attr('checked'))
										                {
										                	if($j(this).val()=='true')
												            flag = true;     
										                                
										                }                        
										            });
												    return flag;
												 }
											   }
							                },
"declPojo.idmQues6":{validateRadioRequired:["declPojo.idmQues6"]},
"declPojo.idmQues6a":{validateRadioRequired:{
							               param:["declPojo.idmQues6a"],
											depends: function(element) {
							                  var flag = false;
												$j('[name=declPojo.idmQues6]').each(function(){
									                if($j(this).attr('checked'))
									                {
							                          if($j(this).val()=='false')
							                          flag = true;     
									                                
									                }                        
									            });
											    return flag;
											 }
										   }
							             },
		"declPojo.idmQues6AddDetail":{required:{
							                depends: function(element) {
						                      var flag = false;
												$j('[name=declPojo.idmQues6]').each(function(){
									                if($j(this).attr('checked'))
									                {
									                	if($j(this).val()=='false')
											            flag = true;     
									                                
									                }                        
									            });
											    return flag;
											 }
										   }
						                },
	"declPojo.idmQues6b":{validateRadioRequired:{
								               param:["declPojo.idmQues6b"],
												depends: function(element) {
								                  var flag = false;
													$j('[name=declPojo.idmQues6a]').each(function(){
										                if($j(this).attr('checked'))
										                {
								                          if($j(this).val()=='false')
								                          flag = true;     
										                                
										                }                        
										            });
												    return flag;
												 }
											   }
								             },
"declPojo.idmQues7a":{validateRadioRequired:{
									               param:["declPojo.idmQues7a"],
													depends: function(element) {
									                  var flag = false;
														$j('[name=declPojo.idmQues7]').each(function(){
											                if($j(this).attr('checked'))
											                {
									                          if($j(this).val()=='false')
									                          flag = true;     
											                                
											                }                        
											            });
													    return flag;
													 }
												   }
									             },
"declPojo.idmQues7AddDetail":{required:{
					                depends: function(element) {
			                      var flag = false;
									$j('[name=declPojo.idmQues7]').each(function(){
						                if($j(this).attr('checked'))
						                {
						                	if($j(this).val()=='false')
								            flag = true;
						                }                        
						            });
								    return flag;
								 }
							   }
				                										             						                													                
},									             
"declPojo.idmQues7b":{validateRadioRequired:{
										               param:["declPojo.idmQues7b"],
														depends: function(element) {
										                  var flag = false;
															$j('[name=declPojo.idmQues7a]').each(function(){
												                if($j(this).attr('checked'))
												                {
										                          if($j(this).val()=='false')
										                          flag = true;     
												                                
												                }                        
												            });
														    return flag;
														 }
													   }
										             },
	"declPojo.idmQues8AddDetail":{required:{
											                depends: function(element) {
										                      var flag = false;
																$j('[name=declPojo.idmQues8]').each(function(){
													                if($j(this).attr('checked'))
													                {
													                	if($j(this).val()=='true')
															            flag = true;     
													                                
													                }                        
													            });
															    return flag;
															 }
														   }
										                }};
													
function addRules(rulesObj){
  	for (var item in rulesObj){
		$j('[name='+item+']').each(function() {
		    $j(this).rules('add', rulesObj[item]);
		});	 
	} 	
}
</script>