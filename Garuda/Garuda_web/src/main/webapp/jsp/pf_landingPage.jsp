<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import="com.velos.ordercomponent.business.util.VelosMidConstants"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<%@page import="com.velos.ordercomponent.util.VelosGarudaConstants"%>
<%@page import="com.velos.ordercomponent.util.GetResourceBundleData"%>
<% 
GetResourceBundleData propData = new GetResourceBundleData();
%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="paginateSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginationSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginationSearch2" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<%
HttpSession tSession = request.getSession(true);
GrpRightsJB grpRights =null;
String accId = null;
String logUsr = null;
if (tSession.getAttribute("LocalGRights")!=null){
 grpRights = (GrpRightsJB) tSession.getAttribute("LocalGRights");
}else
{
 grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
}
if (sessionmaint.isValidSession(tSession))
{
    accId = (String) tSession.getValue("accountId");
    logUsr = (String) tSession.getValue("userId");
    request.setAttribute("username",logUsr);
}

String cellValue1="";
if(request.getAttribute("username")!=null && !request.getAttribute("username").equals("")){
	cellValue1 = request.getAttribute("username").toString();
}

int viewAlertNotiWidget = 0;
int viewWorkflowNotiWidget=0;
int viewOrdersWidget=0;
int viewCBBSummaryWidget=0;
int viewCordShippedWidget=0;

if(grpRights.getFtrRightsByValue("CB_ALERTNOTE")!=null && !grpRights.getFtrRightsByValue("CB_ALERTNOTE").equals(""))
{viewAlertNotiWidget = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ALERTNOTE"));}
else
	viewAlertNotiWidget = 4;
if(grpRights.getFtrRightsByValue("CB_WORKNOTE")!=null && !grpRights.getFtrRightsByValue("CB_WORKNOTE").equals(""))
{viewWorkflowNotiWidget = Integer.parseInt(grpRights.getFtrRightsByValue("CB_WORKNOTE"));}
else
	viewWorkflowNotiWidget = 4;
if(grpRights.getFtrRightsByValue("CB_ORDERS")!=null && !grpRights.getFtrRightsByValue("CB_ORDERS").equals(""))
{viewOrdersWidget = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ORDERS"));}
else
	viewOrdersWidget = 4;
if(grpRights.getFtrRightsByValue("CB_CBBSUM")!=null && !grpRights.getFtrRightsByValue("CB_CBBSUM").equals(""))
{viewCBBSummaryWidget = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBBSUM"));}
else
	viewCBBSummaryWidget = 4;
if(grpRights.getFtrRightsByValue("CB_CORDSHIP")!=null && !grpRights.getFtrRightsByValue("CB_CORDSHIP").equals(""))
{viewCordShippedWidget = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CORDSHIP"));}
else
	viewCordShippedWidget = 4;
request.setAttribute("viewAlertNotiWidget",viewAlertNotiWidget);
request.setAttribute("viewWorkflowNotiWidget",viewWorkflowNotiWidget);
request.setAttribute("viewOrdersWidget",viewOrdersWidget);
request.setAttribute("viewCBBSummaryWidget",viewCBBSummaryWidget);
request.setAttribute("viewCordShippedWidget",viewCordShippedWidget);
%>

<script type="text/javascript">

function Maximize()
{
window.resizeTo(screen.availWidth, screen.availHeight);
}
Maximize();



$j(document).ready(function(){
	maximizePage();
	ShowEntries_Sort_Data();
	hideDataDiv();
	$j('.firstLoad').live('click', function(){
		
		if($j('#shipmentCalenderDiv').is(":visible")){
			
			if(!$j('#shipmentCalenderDiv').hasClass('shipmentLoad')){
				getCEvents();
				$j('#shipmentCalenderDiv').addClass('shipmentLoad');
			}
			
		}
	});
	$j('.showHide').live('click', function(){
		var cbb=$j('#cbbdrpdwn').val();
		if(cbb==null || cbb==''){
			$j(this).triggerHandler('click');
			alert("Please select any CBB");
		}
		
	});

	
	var lastCBB='<%=session.getAttribute("lastCBBlp")%>';
	
	var primaryorg=$j('#primaryOrg').val();
	
	if(primaryorg!=null && primaryorg!="" && (lastCBB=='null' || lastCBB=='')){
		$j('#cbbdrpdwn').val(primaryorg);
		getCBBdetails(primaryorg);
	} 
	
	if(lastCBB!='null' && lastCBB!=''){
		
		if(lastCBB.indexOf(",")!=-1){
			splitedStr=lastCBB.split(",");
			var j=0;
			$j('#cbbdrpdwn').find('option').each(function(i){
				if($j(this).val()==splitedStr[j]){
					$j(this).attr('selected','selected');
					j=j+1;
				}
			});
			
		}else{
			$j('#cbbdrpdwn').val(lastCBB);	
		}
		getCBBdetails(lastCBB);
		
		
	}
	
	$j('#buttonSpan').removeClass('ui-button-text').addClass('ui-button-text-only');
	$j('#cbbdrpdwn').filterByText($j('#Searchbox'), true , "cbbdrpdwn");
});
$j(window).unload(function(){
	lastFunc();
});
</script>
<style>
#arrowDiv{
     border-width:6px 6px 6px 6px;
     border-style:outset;
     border-color:transparent #A1A09F transparent transparent;
     height:1px;
     width:1px;
}

#wrkflowTbody td{
	padding: 2px 10px !important;
    white-space: nowrap; 
    
}
</style>
<s:hidden name="ord_header" id="ord_header"></s:hidden>
<s:hidden name="ord_sort_type" id="ord_sort_type"></s:hidden>
<s:hidden name="wrkflw_header" id="wrkflw_header"></s:hidden>
<s:hidden name="wrkflw_sort_type" id="wrkflw_sort_type"></s:hidden>
<s:hidden name="alert_header" id="alert_header"></s:hidden>
<s:hidden name="alert_sort_type" id="alert_sort_type"></s:hidden>
<s:hidden name="shipDtArry" id="shipDtArry"/>
<s:hidden name="shipRegIdArry" id="shipRegIdArry"/>
<s:hidden name="shippkCordArry" id="shippkCordArry"/>
<s:hidden name="shippkOrdArry" id="shippkOrdArry"/>
<s:hidden name="ord_sortParam" id="ord_sortParam"/>
<s:hidden name="wrkflow_sortParam" id="wrkflow_sortParam"/>
<s:hidden name="alert_sortParam" id="alert_sortParam"/>
<s:hidden name="LP_OrdEntries" id="LP_OrdEntries"/>
<s:hidden name="LP_WflwEntries" id="LP_WflwEntries"/>
<s:hidden name="LP_TaskEntries" id="LP_TaskEntries"/>
<s:hidden name="LP_OrdEntries_txt" id="LP_OrdEntries_txt"/>
<s:hidden name="LP_WflwEntries_txt" id="LP_WflwEntries_txt"/>
<s:hidden name="LP_TaskEntries_txt" id="LP_TaskEntries_txt"/>
<s:hidden name="ord_allEntries_flag" id="ord_allEntries_flag"/>
<s:hidden name="wrkflw_allEntries_flag" id="wrkflw_allEntries_flag"/>
<s:hidden name="alrt_allEntries_flag" id="alrt_allEntries_flag"/>
<s:hidden name="ord_filter_value" id="ord_filter_value"/>
<s:hidden name="workflow_filter_value" id="workflow_filter_value"/>
<s:hidden name="alrt_filter_value" id="alrt_filter_value"/>
<s:hidden name="is_cbb_selected" id="is_cbb_selected"></s:hidden>
<s:form id="quicknoteform">
<s:hidden name="vflag" id="flag"/>
<s:hidden name="pksite" id="siteIdval"/>
<s:hidden name="resetOrdFlag" id="resetOrdFilt" />
<s:hidden name="ordParam" id="ordParams"/>
<s:hidden name="workflowParam" id="workflowParam"/>
<s:hidden name="alertParam" id="alertParam"/>
<s:hidden name="ordFilterFlag" id="ordFilterFlag"/>
<s:hidden name="workflowFilterFlag" id="workflowFilterFlag"/>
<s:hidden name="alertFilterFlag" id="alertFilterFlag"/>
<s:hidden name="ordStatFilterFlag" id="ordStatFilterFlag"/>
<s:hidden name="wrkflowStatFilterFlag" id="wrkflowStatFilterFlag"/>
<s:hidden name="alertStatFilterFlag" id="alertStatFilterFlag"/>
<s:hidden name="userPrimaryOrg" id="primaryOrg"></s:hidden>
<div class="maincontainer">
<table><tr><td colspan="2">
	<div class="portlet" style="margin-top: 10px;">
			<div class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
					<span class="ui-icon ui-icon-minusthick-lp" onclick="togglecbbIdDiv(this)"></span>
					<%=propData.getPropertiesData("garuda.cdrcbuview.label.selectcbb") %>
			</div>
			<div class="portlet-content">
	<div id="cbbIdDiv" style="float: left;">
	<table width="auto"><tr>
	<td><strong><%=propData.getPropertiesData("garuda.cdrcbuview.label.selectcbb") %></strong> :</td>
	<td>
	<s:if test="#request.CbbList!=null">
			<s:textfield id="Searchbox" name="Searchbox" value="Enter Any CBB"></s:textfield><br>
			<select id="cbbdrpdwn" name="cbbdrpdwn" style="width:300px;height:auto;overflow:visible;" onchange="getCBBdetails(this.value);" multiple="true">
								<s:iterator value="#request.CbbList">
								<option value=<s:property value='siteId'/> ><s:property value="siteIdentifier"/> - <s:property value="siteName"/></option>
								</s:iterator>
								<option value="<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@ALL_SITE"/>"><s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@ALL_OPTION"/></option>
		    </select>
	</s:if>
	</td>
	</tr></table>
	</div>
</div></div></div>	
	</td>
	</tr>
	<tr>
	<td valign="top" width="20%">
	<div class="fdiv">

			<s:if test="hasViewPermission(#request.viewCBBSummaryWidget)==true">
			<div class="portlet" id="cbbProfDivparent">
			<div id="cbbProfDiv" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
					<span class="ui-icon ui-icon-print" onclick="showContent(this,'cbbprofiletbl','flag_cbbsummary');printWidgets('cbbProfDiv','printcbbProfDiv')"></span>
					<span class="ui-icon ui-icon-plusthick cbbSummary showHide" onclick="showContent(this,'cbbprofiletbl','flag_cbbsummary');"></span>
					<%=propData.getPropertiesData("garuda.landingpage.label.cbusummary") %>
			</div>
			<div class="portlet-content hide-data" id="printcbbProfDiv">
			
			<br/>
			<br/>
			
			<div id="cbbprofiletbl" style="vertical-align: middle;">
			<table border="0" align="left" cellpadding="0" cellspacing="0"
				class="displaycdr cbbproftblcls" id="cbbprofile">
				<thead>
					<tr>
						<!-- <th></th> -->
						<th><%=propData.getPropertiesData("garuda.landingpage.label.requesttype") %></th>
						<th><%=propData.getPropertiesData("garuda.landingpage.label.totcount") %></th>
						<th><%=propData.getPropertiesData("garuda.landingpage.label.new") %></th>
					</tr>
				</thead>
				<tbody>
					<s:if test="cbbProfileLst.size()>0 && cbbProfileLst!=null">
						<s:iterator value="cbbProfileLst" var="rowVal" status="row">
							<tr>
								<td><s:property value="%{#rowVal[0]}" /></td>
								<td><s:property value="%{#rowVal[1]}" /></td>
								<td><a href="#" onclick="clickHere('<s:property value="%{#row.index}"/>','<s:property value="%{#rowVal[0]}" />');" ><s:property value="%{#rowVal[2]}"/></a></td>
							</tr>
						</s:iterator>
					</s:if>
					
				</tbody>
				<tfoot>
					<tr>
						<td colspan="4"></td>
					</tr>
				</tfoot>
			</table>
			<br/>
			<br/>
			<br/>
			</div>
			</div></div></div>
			</s:if>
			</div>
			
			
			<div class="fdiv">
			<s:if test="hasViewPermission(#request.viewCordShippedWidget)==true">
			<div class="portlet" id="shipmentDivparent">
			<div id="shipmentDiv" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
					<span class="ui-icon ui-icon-print" onclick="showContent(this,'shipmentCalenderDiv','flag_shipment');printWidgets('shipmentDiv','printShipmentDiv')"></span>
					<span class="ui-icon ui-icon-plusthick showHide shipmentPortlet" id="shipmentSpan" onclick="showContent(this,'shipmentCalenderDiv','flag_shipment');"></span>
					<%=propData.getPropertiesData("garuda.landingpage.label.cordshipments") %>
			</div>
			<div class="portlet-content hide-data shipmentCalen" id="printShipmentDiv">
				<div id="shipmentCalenderDiv">
						<div id="calendar"></div>
						<div id="helpmsg" style="position:fixed;">
						</div>
				</div>
			</div></div></div>
			</s:if>
			</div>
	</td>
	<td width="80%" valign="top">
	<div class="sdiv">
	<s:if test="hasViewPermission(#request.viewOrdersWidget)==true">
			<div class="portlet" id="allOrdersDivparent">
			<div id="allOrdersDiv" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
					<span class="ui-icon ui-icon-print" onclick="showContent(this,'orderdiv','flag_orders');printWidgets('allOrdersDiv','printAllOrdersDiv')"></span>
					<span class="ui-icon ui-icon-plusthick lporders showHide" onclick="showContent(this,'orderdiv','flag_orders');"></span>
					<%=propData.getPropertiesData("garuda.pendingorderpage.label.orders") %>
			</div>
			<div class="portlet-content hide-data" id="printAllOrdersDiv">
			<table align="right">
				<tr align="right" style="vertical-align: top;">
						<td>
						<a href="#" onclick="fn_FilterOrders()" ><%=propData.getPropertiesData("garuda.pendingorderpage.label.executeFilter") %></a>
						&nbsp;&nbsp;&nbsp;
						<a href="#" onclick="fnResetFilterOrders()" ><%=propData.getPropertiesData("garuda.landingpage.button.label.reset") %></a>
						&nbsp;&nbsp;&nbsp;
						<a href="#"  id="resetSortOrd"><span><%=propData.getPropertiesData("garuda.pendingorderpage.label.resetsort") %></span></a>
					    </td>
				</tr>
			</table>
			
			<div id="orderdiv">
			<s:hidden name="lpdateStart" id="lpdateStartord"/>
			<s:hidden name="lpdateEnd" id="lpdateEndord" />
			<input type="hidden" name="entries" id="entriesOrder" value=<%=paginationSearch.getiShowRows() %> />
			<input type="hidden" name="tentries" id="tentriesOrder" value=<%=paginationSearch.getiTotalRows() %> />
			<table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="orders">
				<thead id="ordThead">
					<tr id="ordTheadTr">
						<th id="thSelectColumn2">
						<div class="cmDiv">
						<ul class="clickMenu" id="ulSelectColumn2">
							<li class="main hover"><img src="images/selectcol.png"
								alt="<s:text name="garuda.common.label.selectcolumns"/>" title="<s:text name="garuda.common.label.selectcolumns"/>" />
							<div style="position: absolute; display: block;"
								class="outerbox inner">
							<div class="shadowbox1"></div>
							<div class="shadowbox2"></div>
							<div class="shadowbox3"></div>
							<ul class="innerBox" id="targetall2"></ul>
							</div>
							</li>
						</ul>
						<div class="clear"></div>
						</div>
						</th>
						<th class="sorting ord_regId postionth" ><%=propData.getPropertiesData("garuda.cbufinalreview.label.regId") %></th>
						<th class="sorting ord_reqtype postionth" ><%=propData.getPropertiesData("garuda.landingpage.label.requesttype") %></th>
						<th class="sorting ord_reqDate postionth" ><%=propData.getPropertiesData("garuda.landingpage.label.requestdate") %></th>
						<th class="sorting ord_reqStat postionth" ><%=propData.getPropertiesData("garuda.landingpage.label.requeststatus") %></th>
						<th class="sorting ord_assign postionth" ><%=propData.getPropertiesData("garuda.landingpage.label.assignedto") %></th>
					</tr>
					<tr id="ordTheadTr2">
						<th><!--Column for column picker  --></th>
						<th><s:textfield name="lpRegId1" id="lpRegId1" placeholder="Enter CBU Registry ID" /></th>
						<th>
							<s:hidden name="lporderType1"></s:hidden>
							<s:hidden name="lpsampleatLab1"></s:hidden>
							<select id="orderType1" name="orderType1">
								<option value="">Select</option>
								<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@ORDER_TYPE]">
								<s:if test="subType=='CT' || subType=='HE' || subType=='OR' || subType=='ALL'">
											<s:if test="subType=='CT'" >
												<option value="<s:property value='pkCodeId' />" <s:if test='lpsampleatLab1=="Y"'>selected="selected"</s:if>><%=VelosMidConstants.CT_LAB_ORDER%></option>
												<option value="<s:property value='pkCodeId'/>" <s:if test='lpsampleatLab1=="N"'>selected="selected"</s:if>><%=VelosMidConstants.CT_SHIP_ORDER%></option>
											</s:if>
											<s:if test="subType=='ALL'" >
												<option value="ALL" <s:if test="lporderType1=='ALL'">selected="selected"</s:if>><s:property value='description'/></option>
											</s:if>
											<s:if test="subType=='HE' || subType=='OR'">
												<option value="<s:property value='pkCodeId'/>" <s:if test="lporderType1==pkCodeId">selected="selected"</s:if> ><s:property value='description'/></option>
											</s:if>
								</s:if>
								</s:iterator>
							</select>
							
						</th>
						<th>
							
							<button type="button" id="dateRange1" onclick="showDateDiv(this,'dateDiv','dateStart','dateEnd'); resetValidForm('dummyform');"
								 class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false">
							  	<span class="ui-button-text" id="buttonSpan"><%=propData.getPropertiesData("garuda.pendingorder.button.dateRange") %></span>
							  </button>
						</th>
						<th>
							<select id="orderStatus1" name="orderStatus1">
								<option value="">Select</option>
								<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@ORDER_STATUS]">
								<option value=<s:property value='pkCodeId'/>  <s:if test="%{lporderStatus1==pkCodeId}">selected="selected"</s:if>><s:property value="description"/></option>
								</s:iterator>
							</select>
							
						</th>
						<th id="assignToTh" align="center">
							<s:if test="lstUsers!=null && lstUsers.size()>0">
							<s:hidden name="assignLstSize" id="assignLstSize" value="%{lstUsers.size()}"/>
							<select id="AssignTo1" name="AssignTo1" class="searchableSelect">
							<option value="">Select</option>
							<s:iterator value="lstUsers" var="rowIndex">
							<s:if test="%{#rowIndex[0]==#request.username}">
							<option value='<s:property value="%{#rowIndex[0]}"/>' <s:if test="%{assignTo1==#rowIndex[0]}">selected="selected"</s:if>  >Me</option>
							</s:if>
							</s:iterator>
							<option value="ALL" <s:if test="%{assignTo1=='ALL'}">selected="selected"</s:if>>ALL</option>
							<option value="Unassigned" <s:if test="%{assignTo1=='Unassigned'}">selected="selected"</s:if>>Unassigned</option>
							<s:iterator value="lstUsers" var="rowIndex">
							<s:if test="%{#rowIndex[0]!=#request.username}">
							<option value='<s:property value="%{#rowIndex[0]}"/>' <s:if test="%{assignTo1==#rowIndex[0]}">selected="selected"</s:if>><s:property value="%{#rowIndex[2]}"/></option>
							</s:if>
							</s:iterator>
							</select>																															
							</s:if>		
							<s:else>
								<s:select name="empty" headerKey="" headerValue="Select" list="{}"></s:select>														
							</s:else>
						</th>


					</tr>
				</thead>
				<tbody id="ordTbody">
					<tr>
					<td colspan="6"></td>
					</tr>
				</tbody> 
				<tfoot>
					<tr id="ordTblFoot">
						<td colspan="6" width="100%">
						<jsp:include page="paginationFooter.jsp">
						  	<jsp:param value="orderdiv" name="divName"/>
						  	<jsp:param value="quicknoteform" name="formName"/>
						  	<jsp:param value="showsRoworder" name="showsRecordId"/>
						  	<jsp:param value="showLPOrders" name="url"/>
						  	<jsp:param value="ordParams" name="ordParams"/>
						  	<jsp:param value="orders" name="paginateSearch"/>
						  	<jsp:param value="ord_allEntries_flag" name="allentries"/>
						  	<jsp:param value="ord_sortParam" name="ord_sortParam"/>
						  	<jsp:param value="forOrdersBodyOnloads" name="bodyOnloadFn"/>
						  	<jsp:param value="1" name="idparam"/>
						</jsp:include>
						</td>
					</tr>
				</tfoot>
			</table>
			<div align="center"><span id="orderFiltStatusTxt"></span></div>
			</div>
			</div></div></div></s:if></div>
			
			<div class="sdiv">
			<s:if test="hasViewPermission(#request.viewWorkflowNotiWidget)==true">
			<div class="portlet" id="workflowOrdDivparent">
			<div id="workflowOrdDiv" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
					<span class="ui-icon ui-icon-print" onclick="showContent(this,'workflowdiv','flag_workflow');printWidgets('workflowOrdDiv','printWorkflowDiv')"></span>
					<span class="ui-icon ui-icon-plusthick showHide lpworkflow" onclick="showContent(this,'workflowdiv','flag_workflow');"></span>
					<%=propData.getPropertiesData("garuda.pf.label.WorkflowNotifications") %>
			</div>
			<div class="portlet-content hide-data" id="printWorkflowDiv">
								
								<table align="right">
								<tr align="right" style="vertical-align: top;"><td>
								<a href="#" onclick="fn_FilterWorkflow()" ><%=propData.getPropertiesData("garuda.pendingorderpage.label.executeFilter") %></a>&nbsp;&nbsp;&nbsp;
								<a href="#" onclick="fnResetFilterWorkFlow()" ><%=propData.getPropertiesData("garuda.landingpage.button.label.reset") %></a>
								&nbsp;&nbsp;&nbsp;
						        <a href="#"  id="resetSortWorkflow"><span><%=propData.getPropertiesData("garuda.pendingorderpage.label.resetsort") %></span></a>
								</td></tr>
								</table>
								<div id="workflowdiv">
								<input type="hidden" name="entries" id="entriesWorkflow" value=<%=paginateSearch.getiShowRows() %> />
								<input type="hidden" name="tentries" id="tentriesWorkflow" value=<%=paginateSearch.getiTotalRows() %> />
								<s:hidden name="lpdateStart2" id="lpdateStartwork"/>
								<s:hidden name="lpdateEnd2" id="lpdateEndwork" />
								<table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="workflow">
									<thead id="wrkflowThead">
										<tr id="wrkflowTheadTr">
											<th id="thSelectColumn">
												<div class="cmDiv">
												<ul class="clickMenu" id="ulSelectColumn">
													<li class="main hover">
														<img src="images/selectcol.png" alt="<s:text name="garuda.common.label.selectcolumns"/>" title="<s:text name="garuda.common.label.selectcolumns"/>"/>
															<div style="position: absolute; display: block;" class="outerbox inner">
																<div class="shadowbox1"></div>
																<div class="shadowbox2"></div>
																<div class="shadowbox3"></div>
																<ul class="innerBox" id="targetall"></ul>
															</div>
													</li>
												</ul>
												<div class="clear"></div>
												</div>
											</th>
											<th class="sorting wrkflw_reqType postionth" ><%=propData.getPropertiesData("garuda.landingpage.label.requesttype") %></th>
											<th class="sorting wrkflw_regId postionth" ><%=propData.getPropertiesData("garuda.cbufinalreview.label.regId") %></th>
											<th class="sorting wrkflw_reqDt postionth" ><%=propData.getPropertiesData("garuda.cbuhistory.label.requestdate") %></th>
											<th class="sorting wrkflw_reqStat postionth"><%=propData.getPropertiesData("garuda.currentReqProgress.label.requeststatus") %></th>
											
											
										</tr>
										<tr id="wrkflowTheadTr1">
											<th><!--Column for column picker  --></th>
											<!-- <th></th> -->
											<th>
												<s:hidden name="lporderType2"></s:hidden>
												<s:hidden name="lpsampleatLab2"></s:hidden>
												<select id="orderType2" name="orderType2">
													<option value="">Select</option>
													<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@ORDER_TYPE]">
													<s:if test="subType=='CT' || subType=='HE' || subType=='OR' || subType=='ALL'">
														<s:if test="subType=='CT'" >
																<option value="<s:property value='pkCodeId' />"  <s:if test='lpsampleatLab2=="Y"'>selected="selected"</s:if>><%=VelosMidConstants.CT_LAB_ORDER%></option>
																<option value="<s:property value='pkCodeId'/>"   <s:if test='lpsampleatLab2=="N"'>selected="selected"</s:if>><%=VelosMidConstants.CT_SHIP_ORDER%></option>
														</s:if>
														<s:if test="subType=='ALL'" >
																<option value="ALL"  <s:if test="lporderType2=='ALL'">selected="selected"</s:if>><s:property value='description'/></option>
														</s:if>
														<s:if test="subType=='HE' || subType=='OR'">
															<option value=<s:property value='pkCodeId'/> <s:if test="lporderType2==pkCodeId">selected="selected"</s:if>><s:property value='description'/></option>
														</s:if>
													</s:if>
													</s:iterator>
											    </select>
											</th>
											<th><s:textfield name="lpRegId2" id="lpRegId2" placeholder="Enter CBU Registry ID" /></th>
											<th>
											<button id="dateRange2" type="button" onclick="showDateDiv(this,'dateDiv1','dateStart1','dateEnd1'); resetValidForm('dummyform');"
									    	class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"> 

											<span class="ui-button-text"><s:text name="garuda.pendingorder.button.dateRange"/></span>
											</button>
											</th>
											<th>
											<select id="orderStatus2" name="orderStatus2">
												<option value="">Select</option>
												<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@ORDER_STATUS]">
												<s:if test="subType!='close_ordr' && subType!='CANCELLED'">
												<s:if test="%{pkCodeId==lporderStatus2}">
												<option value="<s:property value='pkCodeId'/>" selected="selected" ><s:property value="description"/></option>
												</s:if>
												<s:else>
												<option value="<s:property value='pkCodeId'/>"> <s:property value="description"/></option>
												</s:else>
												</s:if>
												</s:iterator>
											</select>
											</th>
											
											
										</tr>
									</thead>
									<tbody id="wrkflowTbody">
									<tr>
									<td colspan="5"></td>
									</tr>
									</tbody>			
									<tfoot>
										<tr id="wrkFlwTblFoot">
											<td colspan="5" width="100%">
											  <jsp:include page="paginationFooter.jsp">
											  	<jsp:param value="workflowdiv" name="divName"/>
											  	<jsp:param value="quicknoteform" name="formName"/>
											  	<jsp:param value="showsRowNotifications" name="showsRecordId"/>
											  	<jsp:param value="showLPWorkflow" name="url"/>
											  	<jsp:param value="workflow" name="paginateSearch"/>
											  	<jsp:param value="forWorkFlowBodyOnloads" name="bodyOnloadFn"/>
											  	<jsp:param value="ord_allEntries_flag" name="allentries"/>
											  	<jsp:param value="wrkflow_sortParam" name="ord_sortParam"/>
											  	<jsp:param value="workflowParam" name="ordParams"/>
											  	<jsp:param value="2" name="idparam"/>
											  </jsp:include>
											</td>
										</tr>
									</tfoot>		
								</table> 	
								<div align="center"><span id="wrkflowFiltStatusTxt"></span></div>
								</div>
					</div></div></div></s:if>
					</div>
					
					<div class="tdiv">
					<s:if test="hasViewPermission(#request.viewAlertNotiWidget)==true">
					<div class="portlet" id="taskLstDivparent">
					<div id="taskLstDiv" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
					<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
							<span class="ui-icon ui-icon-print" onclick="showContent(this,'tasksdiv','flag_alerts');showContent(this,'tasksdiv','flag_alerts');printWidgets('taskLstDiv','printTaskLstDiv')"></span>
							<span class="ui-icon ui-icon-plusthick showHide lpalerts" onclick="showContent(this,'tasksdiv','flag_alerts');"></span>
							<%=propData.getPropertiesData("garuda.landingpage.label.alertnotifications") %>
					</div>
					<div class="portlet-content hide-data" id="printTaskLstDiv">
								<table align="right">
								<tr align="right" style="vertical-align: top;"><td>
								<a href="#" onclick="fn_FilterAlerts()" ><%=propData.getPropertiesData("garuda.pendingorderpage.label.executeFilter") %></a>&nbsp;&nbsp;&nbsp;
								<a href="#" onclick="fnResetFilterAlerts()" ><%=propData.getPropertiesData("garuda.landingpage.button.label.reset") %></a>
								&nbsp;&nbsp;&nbsp;
						        <a href="#"  id="resetSortAlert"><span><%=propData.getPropertiesData("garuda.pendingorderpage.label.resetsort") %></span></a>
								</td></tr>
								</table>
								<div id="tasksdiv">
								<input type="hidden" name="entries" id="entriestasks" value=<%=paginationSearch2.getiShowRows() %> />
								<input type="hidden" name="tentries" id="tentriestasks" value=<%=paginationSearch2.getiTotalRows() %> />
								<s:hidden name="lpdateStart3" id="lpdateStartalrt"/>
								<s:hidden name="lpdateEnd3" id="lpdateEndalrt" />
								<s:hidden name="lpdateStart4" id="lpdateStartalrtStr"/>
								<s:hidden name="lpdateEnd4" id="lpdateEndalrtStr" />
								<table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="tasks">
									<thead>
										<tr id="taskTheadTr">
											<th id="thSelectCol">
												<div class="cmDiv">
												<ul class="clickMenu" id="ulSelectCol">
													<li class="main hover">
														<img src="images/selectcol.png" alt="<s:text name="garuda.common.label.selectcolumns"/>" title="<s:text name="garuda.common.label.selectcolumns"/>"/>
															<div style="position: absolute; display: block;" class="outerbox inner">
																<div class="shadowbox1"></div>
																<div class="shadowbox2"></div>
																<div class="shadowbox3"></div>
																<ul class="innerBox" id="targetUl"></ul>
															</div>
													</li>
												</ul>
												<div class="clear"></div>
												</div>
											</th>
											<th class="sorting alert_reqType postionth" ><%=propData.getPropertiesData("garuda.landingpage.label.requesttype") %></th>
											<th class="sorting alert_reqId postionth" ><%=propData.getPropertiesData("garuda.cbufinalreview.label.regId") %></th>
											<th class="sorting alert_reqDt postionth" ><%=propData.getPropertiesData("garuda.cbuhistory.label.requestdate") %></th>
											<th class="sorting alert_reqStat postionth" ><%=propData.getPropertiesData("garuda.currentReqProgress.label.requeststatus") %></th>
											<th class="sorting alert_alrDt postionth" ><%=propData.getPropertiesData("garuda.landingpage.label.alertdate") %></th>
										</tr>
										<tr id="taskTheadTr1">
											<th><!--Column for column picker  --></th>
											<!-- <th></th> -->
											<th>				
											<select id="orderType3" name="orderType3">
													<option value="">Select</option>
													<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@ORDER_TYPE]">
													<s:if test="subType=='CT' || subType=='HE' || subType=='OR' || subType=='ALL'">
														<s:if test="subType=='CT'" >
																<option value="<s:property value='pkCodeId' />"  <s:if test='lpsampleatLab3=="Y"'>selected="selected"</s:if>><%=VelosMidConstants.CT_LAB_ORDER%></option>
																<option value="<s:property value='pkCodeId'/>"  <s:if test='lpsampleatLab3=="N"'>selected="selected"</s:if>><%=VelosMidConstants.CT_SHIP_ORDER%></option>
														</s:if>
														<s:if test="subType=='ALL'" >
																<option value='ALL' <s:if test="lporderType3=='ALL'">selected="selected"</s:if>><s:property value='description'/></option>
														</s:if>
														<s:if test="subType=='HE' || subType=='OR'">
															<option value=<s:property value='pkCodeId'/> <s:if test="lporderType3==pkCodeId">selected="selected"</s:if>><s:property value='description'/></option>
														</s:if>
													</s:if>
													</s:iterator>
										    </select>
										    </th>
										    <th><s:textfield name="lpRegId3" id="lpRegId3" placeholder="Enter CBU Registry ID" /></th>
										    <th>
										    <button id="dateRange3" type="button" onclick="showDateDiv(this,'dateDiv2','dateStart2','dateEnd2'); resetValidForm('dummyform');" 
										    	class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"> 
										    	<span class="ui-button-text"><%=propData.getPropertiesData("garuda.pendingorder.button.dateRange") %></span>
										    </button>
										    </th>
										    <th>
										    <select id="orderStatus3" name="orderStatus3">
												<option value="">Select</option>
												<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@ORDER_STATUS]">
												<s:if test="subType!='close_ordr' && subType!='CANCELLED'">
												<option value="<s:property value='pkCodeId'/>" <s:if test="lporderStatus3==pkCodeId">selected="selected"</s:if>><s:property value="description"/></option>
												</s:if>
												</s:iterator>
											</select>
										    </th>
										    <th>
										    <button id="dateRange4" type="button" onclick="showDateDiv(this,'dateDiv3','dateStart3','dateEnd3'); resetValidForm('dummyform');" 
										    	class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"> 
										    	<span class="ui-button-text"><%=propData.getPropertiesData("garuda.pendingorder.button.dateRange") %></span>
										    </button>
										    </th>
										</tr>
									</thead>
									<tbody>
									<tr>
									<td colspan="6"></td>
									</tr>
									</tbody>			
									<tfoot>
										<tr id="taskTblFoot">
											<td colspan="6" width="100%">
											  <jsp:include page="paginationFooter.jsp">
											  	<jsp:param value="tasksdiv" name="divName"/>
											  	<jsp:param value="quicknoteform" name="formName"/>
											  	<jsp:param value="showsRowtasks" name="showsRecordId"/>
											  	<jsp:param value="showLPAlerts" name="url"/>
											  	<jsp:param value="alerts" name="paginateSearch"/>
											  	<jsp:param value="alertParam" name="ordParams"/>
											  	<jsp:param value="alrt_allEntries_flag" name="allentries"/>
											  	<jsp:param value="alert_sortParam" name="ord_sortParam"/>
											  	<jsp:param value="forAlertsOnloads" name="bodyOnloadFn"/>
											  	<jsp:param value="3" name="idparam"/>
											  </jsp:include>
											</td>
										</tr>
									</tfoot>
								</table> 
								<div align="center"><span id="alertFiltStatusTxt"></span></div>	
								</div>
								
					<!-- </fieldset> -->
					</div>
					</div></div></s:if></div>
	
	
	</td>
	</tr>
	</table>
</div>
</s:form>
<s:form id="dummyform">
<div style="display: none;" id="dateDiv" class="dataEnteringDiv">
<div class="closeDiv" onclick="$j('#dateDiv').hide()" ><img src="images/cross.png" /></div>
											<table align="center">
												<tr align="center">
												<td>
													<%=propData.getPropertiesData("garuda.pendingorderpage.label.fromdt") %><s:date name="dateStart" id="dateStart" format="M dd,yy" /><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateStart" id="dateStart" placeholder='<%=propData.getPropertiesData("garuda.pendingorderpage.label.enterstartdate") %>' readonly="readonly" />
												</td>
												<td>
													<%=propData.getPropertiesData("garuda.pendingorderpage.label.todt") %><s:date name="dateEnd" id="dateEnd" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateEnd" id="dateEnd" placeholder='<%=propData.getPropertiesData("garuda.pendingorderpage.label.enterenddate") %>' readonly="readonly" />
												</td>
												<td align="center">
													<button  id="clear" type="button" onclick="fnclear('dateStart','dateEnd')"> 
													<%=propData.getPropertiesData("garuda.pendingorderpage.label.clear") %></button>
												</td>
												</tr>
												<tr align="center">
												<td colspan="3">
												<button  id="dateStartbt" type="button" onclick="$j('#dateDiv').hide();fn_FilterOrders();" ><%=propData.getPropertiesData("garuda.pendingorderpage.label.filter") %></button> 
												<button type="button" id="datebtclse" onclick="$j('#dateDiv').hide();" ><%=propData.getPropertiesData("garuda.pendingorderpage.label.close") %></button> 
												</td>
												</tr>
											</table>
										 
</div>
<div style="display: none;" id="dateDiv3" class="dataEnteringDiv">
<div class="closeDiv" onclick="$j('#dateDiv3').hide()" ><img src="images/cross.png" /></div>
																	<table align="center">
																		<tr align="center">
																		<td>
																		<%=propData.getPropertiesData("garuda.pendingorderpage.label.fromdt") %> <s:date name="dateStart3" id="dateStart3" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateStart3" id="dateStart3" placeholder='<%=propData.getPropertiesData("garuda.pendingorderpage.label.enterstartdate") %>' readonly="readonly" />
																		</td>
																		<td>
																		<%=propData.getPropertiesData("garuda.pendingorderpage.label.todt") %> <s:date name="dateEnd3" id="dateEnd3" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateEnd3" id="dateEnd3" placeholder='<%=propData.getPropertiesData("garuda.pendingorderpage.label.enterenddate") %>' readonly="readonly" />
																		</td>
																		<td align="center">
																		<button type="button" id="clear" onclick="fnclear('dateStart3','dateEnd3')" ><%=propData.getPropertiesData("garuda.pendingorderpage.label.clear") %></button>
																		</td>
																		</tr>
																		<tr align="center">
																		<td colspan="3">
																		<button type="button" id="dateStartbt2" onclick="$j('#dateDiv3').hide();fn_FilterAlerts();" ><%=propData.getPropertiesData("garuda.pendingorderpage.label.filter") %></button>
																		<button type="button" id="closedt1" onclick="$j('#dateDiv3').hide()"><%=propData.getPropertiesData("garuda.pendingorderpage.label.close") %></button>
																		</td>
																		</tr>
																	</table>
</div>
<div style="display: none;" id="dateDiv2" class="dataEnteringDiv">
<div class="closeDiv" onclick="$j('#dateDiv2').hide()" ><img src="images/cross.png" /></div>
																	<table align="center">
																		<tr align="center">
																		<td>
																		<%=propData.getPropertiesData("garuda.pendingorderpage.label.fromdt") %> <s:date name="dateStart2" id="dateStart2" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateStart2" id="dateStart2" placeholder='<%=propData.getPropertiesData("garuda.pendingorderpage.label.enterstartdate") %>' readonly="readonly" />
																		</td>
																		<td>
																		<%=propData.getPropertiesData("garuda.pendingorderpage.label.todt") %> <s:date name="dateEnd2" id="dateEnd2" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateEnd2" id="dateEnd2" placeholder='<%=propData.getPropertiesData("garuda.pendingorderpage.label.enterenddate") %>' readonly="readonly" />
																		</td>
																		<td align="center">
																		<button type="button" id="clear" onclick="fnclear('dateStart2','dateEnd2')" ><%=propData.getPropertiesData("garuda.pendingorderpage.label.clear") %></button>
																		</td>
																		</tr>
																		<tr align="center">
																		<td colspan="3">
																		<button type="button" id="dateStartbt1" onclick="$j('#dateDiv2').hide();fn_FilterAlerts();" ><%=propData.getPropertiesData("garuda.pendingorderpage.label.filter") %></button>
																		<button type="button" id="closedt1" onclick="$j('#dateDiv2').hide()"><%=propData.getPropertiesData("garuda.pendingorderpage.label.close") %></button>
																		</td>
																		</tr>
																	</table>
</div>
<div style="display: none;" id="dateDiv1" class="dataEnteringDiv">
<div class="closeDiv" onclick="$j('#dateDiv1').hide()" ><img src="images/cross.png" /></div>
																	<table align="center">
																		<tr align="center">
																		<td>
																		<%=propData.getPropertiesData("garuda.pendingorderpage.label.fromdt") %> <s:date name="dateStart1" id="dateStart1" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateStart1" id="dateStart1" placeholder='<%=propData.getPropertiesData("garuda.pendingorderpage.label.enterstartdate") %>' readonly="readonly" />
																		</td>
																		<td>
																		<%=propData.getPropertiesData("garuda.pendingorderpage.label.todt") %> <s:date name="dateEnd1" id="dateEnd1" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateEnd1" id="dateEnd1" placeholder='<%=propData.getPropertiesData("garuda.pendingorderpage.label.enterenddate") %>' readonly="readonly" />
																		</td>
																		<td align="center">
																		<button type="button" id="clear" onclick="fnclear('dateStart1','dateEnd1')" >
																		<%=propData.getPropertiesData("garuda.pendingorderpage.label.clear") %></button>
																		</td>
																		</tr>
																		<tr align="center">
																		<td colspan="3">
																		<button type="button" id="dateStartbt1" onclick="$j('#dateDiv1').hide();fn_FilterWorkflow();"><%=propData.getPropertiesData("garuda.pendingorderpage.label.filter") %></button>
																		<button type="button" id="closedt1" onclick="$j('#dateDiv1').hide()"><%=propData.getPropertiesData("garuda.pendingorderpage.label.close") %></button>
																		</td>
																		</tr>
																	</table>
</div>
</s:form>
<div id="usersitegrpsdropdown">
			<s:hidden value="#request.grpList.size()" />
	        <s:if test="#request.grpList.size()>1">
				<jsp:include page="pf_userSiteGroups.jsp"></jsp:include>
			</s:if>
</div>