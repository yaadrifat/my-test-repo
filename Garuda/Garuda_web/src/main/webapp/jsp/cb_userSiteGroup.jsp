<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.velos.eres.web.site.SiteJB" %>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.eres.web.address.AddressJB" %>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<script>

function loadRightsByGrpId(grpsId){
	var url="getUserRightForGrp?grpId="+grpsId+"&cordId="+$j("#cordIdentifier").val()+"&dynamicPage=show";
	$j('#recordDiv1').css('display','none');
	refreshDiv(url,"searchTbles2","form1");
	$j('#recordDiv').css('display','block');
	mainonload();
	settingwidth();
	
	if($j('#completeReqInfoFlag').val()=="1"){
		$j('#finalReviewBtn').removeAttr('disabled');
	}
	else{
		$j('#finalReviewBtn').attr('disabled', true);
		}
}
function loadRightsByGrpIdByParam(){
	var url="getUserRightForGrpByParam?siteId="+$j("#siteIdentifier").val()+"&cordId="+$j("#cordIdentifier").val()+"&userId="+$j("#userIdentifier").val()+"&dynamicPage=show";
	$j('#recordDiv1').css('display','none');
	refreshDiv(url,"searchTbles2","form1");
	$j('#recordDiv').css('display','block');
	mainonload();
	settingwidth();
	
	if($j('#completeReqInfoFlag').val()=="1"){
		$j('#finalReviewBtn').removeAttr('disabled');
	}
	else{
		$j('#finalReviewBtn').attr('disabled', true);
		}
}

$j(function(){
	var grpLstSize= document.getElementById("grpLst").value;
	
	 if(grpLstSize==0)
	{
		confirm("<s:text name="garuda.cbu.detail.groupConfirm"/>  ");
	}
});



</script>
<div>
	<table>
		<tr>			
			<td>	
			<s:hidden name="#request.grpList.size()" id="grpLst"></s:hidden>
			<input type="hidden" name="cordIdentifier" id="cordIdentifier" value="<s:property value="#request.cordId"/>" />
			<input type="hidden" name="siteIdentifier" id="siteIdentifier" value="<s:property value="#request.siteId"/>" />
			<input type="hidden" name="userIdentifier" id="userIdentifier" value="<s:property value="#request.userId"/>" />
			<s:if test="#request.grpList.size()==0">
			</s:if>	
			<s:elseif test="#request.grpList!=null && #request.grpList.size()==1">
				<script>
				loadRightsByGrpId('<s:property value="#request.grpList[0].pkGrp" />');
				</script>
			</s:elseif>
			<s:elseif test="#request.grpList!=null && #request.grpList.size()>1">
				<script>
				loadRightsByGrpIdByParam();
				</script>
			</s:elseif>	
				 			 
		  </td>
		</tr>
	</table>
</div>