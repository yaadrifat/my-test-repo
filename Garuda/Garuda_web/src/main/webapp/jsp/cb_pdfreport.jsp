<%@taglib prefix="s" uri="/struts-tags" %>
<%@ page import="java.text.DecimalFormat"%>
<%@ page import="com.velos.eres.service.util.Security"%>
<%
    String sessId = session.getId();
    if (sessId.length()>8) { sessId = sessId.substring(0,8); } // Keep the leading digits only
    sessId = Security.encrypt(sessId);
    char[] chs = sessId.toCharArray();
    StringBuffer sb = new StringBuffer();
    DecimalFormat df = new DecimalFormat("000");
    for (int iX=0; iX<chs.length; iX++) {
        sb.append(df.format((int)chs[iX]));
    }
    String keyValue = "&key="+sb.toString();
%>
<script>
function callAction(){
document.reportForm.action=document.getElementById('path').value;
document.reportForm.submit();
}
</script>
<s:form id="reportForm" method="POST">
<body onload="callAction();">
<input type="hidden" name="path" id="path" value=<%=session.getAttribute("pdfPath")+keyValue%>/>
<input type="hidden" name="sessionId" id="sessionId" value=<%=session.getId()%>/>
</body>
</s:form>
