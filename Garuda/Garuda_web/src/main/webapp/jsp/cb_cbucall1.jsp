<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page import="com.velos.ordercomponent.business.domain.Widget"%>
<%@ page
	import="com.velos.ordercomponent.business.domain.WidgetInstance"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>


<% 
Map<Long,List<Widget>> widgetInfo = (Map<Long,List<Widget>>)application.getAttribute("widgetInfoMap");
List<Widget> widgetList =  widgetInfo.get(2l);
request.setAttribute("widgetlist",widgetList);
String contextpath=request.getContextPath();
String contextpath1=request.getContextPath()+"/jsp/";
%>
<!DOCTYPE html>
<html lang="en">
<meta charset=utf-8>
<title><s:text name="garuda.cbu.title.Velos" /></title>
<head>
<!-- JQuery Styles Scripts Starts -->

<link href="<%=contextpath%>/jsp/styles/jquery-ui-1.8.7.custom.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/js/jquery/themes/nmdp/jquery.ui.all.css">
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery-ui-1.8.7.custom.min.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.ui.core.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.validate.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/ui/jquery.ui.mouse.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/ui/jquery.ui.sortable.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/ui/jquery.ui.resizable.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery-latest.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery-tootltip.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.columnmanager.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.columnmanager.min.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.columnmanager.pack.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.clickmenu.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.clickmenu.pack.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.dataTables.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.numeric.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.decimalMask.1.1.1.min.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery-fieldselection.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.jclock.js"></script>
<!-- JQuery Styles Scripts Ends -->



<!--  end of table search -->
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery.jclock.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/dateFormat.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/table2csv.js"></script>

<script type="text/javascript">
var cotextpath = "<%=contextpath1%>";
function newfileupload(){
	//$j("#newdocpop").dialog(mode);
	var url = cotextpath+"/openfileupload.action?attCodeType=ATTACHMENT_TYPE&attCodeSubType=UNIT_REPORT&codeEntityType=ENTITY_TYPE&codeEntitySubType=CBU&entityID=<s:property value='cdrCbuPojo.cordID' />";
	window.open( url);				
 }
 
/* $j(function() {
	  $j( "#addWidget").dialog(
	   {autoOpen: false,
	   	title: 'Add Widgets',
	    modal: true, width:500, height:300,
	    close : function(){
	    	jQuery("#"+div).dialog("destroy");
			//jquery("#respondentDiv").dialog("destroy");
			  
	      }
	    }
	   );
	 // jQuery("#respondentDiv").dialog("destroy");
	 }); 
	 function showpopup(){
		 $j("#addWidget").dialog("open");
	}*/

	 
	 
	 
function saveCBUUnitReport(){

	/* if($j('#completedby').val()==null || $j('#completedby').val()==""){
     alert("Please enter Completed By!");
     $j('#completedby').focus();
    }else { */     
		if($j("#CBUUnitRptForm").valid()){ 	
			$j.ajax({
				type : "POST",
				async : false,
				url : 'saveCBUUnitReport',
				data : $j("#CBUUnitRptForm").serialize(),
				success : function(result) {
					//showUnitReport("close");
					$j('#main').html(result);
				}
				
			});
	    }
}

$j(function(){
	$j("#CBUUnitRptForm").validate({
			
		rules:{			
				//"cbuUnitReportPojo.cordViabPostProcess" : {lessThanHundredOne : true },
				"cbuUnitReportPojo.cordViabPostProcess" : {greaterThanZero : true },
				//"cbuUnitReportPojo.cordCfuPostProcessCount" : {lessThanHundredOne : true },
				"cbuUnitReportPojo.cordCfuPostProcessCount" : {greaterThanZero : true },
				//"cbuUnitReportPojo.cordCbuPostProcessCount" : {lessThanHundredOne : true },
				"cbuUnitReportPojo.cordCbuPostProcessCount" : {greaterThanZero : true },
				"cbuUnitReportPojo.cordCompletedBy":"required",
				"attachmentFlag":{required:{
											depends: function(element){
														return $j("#browseFlag").val()==1;
													}
											}
								}
			},
			messages:{
				"cbuUnitReportPojo.cordViabPostProcess":"<s:text name="garuda.cbu.unitreport.viabpostprocesbetween0to100"/>",
				"cbuUnitReportPojo.cordCfuPostProcessCount":"<s:text name="garuda.cbu.unitreport.cfupostprocesscountbetween0to100"/>",
				"cbuUnitReportPojo.cordCbuPostProcessCount":"<s:text name="garuda.cbu.unitreport.CbuPostProcessCountbetween0to100"/>",
				"cbuUnitReportPojo.cordCompletedBy":"<s:text name="garuda.cbu.unitreport.completedby"/>"
			}
		});
	});

$j(function() {
	var today = new Date();
	
	var d = today.getDate();
	var m = today.getMonth();
	var y = today.getFullYear();

	var h=today.getHours();
	var mn=today.getMinutes()+1;


	$j( "#datepicker1" ).datepicker({
			dateFormat: 'M dd, yy',
			maxDate: new Date(y, m, d),changeMonth: true,
			changeYear: true
		});
	
	$j( "#datepicker2" ).datepicker({dateFormat: 'M dd, yy',maxDate: new Date(y, m, d),changeMonth: true,
		changeYear: true});
	$j( "#datepicker3" ).datepicker({dateFormat: 'M dd, yy',maxDate: new Date(y, m, d),changeMonth: true,
		changeYear: true});
	//$j( "#datepicker4" ).datepicker({dateFormat: 'M dd, yy'});
	
});
function show(checkboxName,divName) {
	if(checkboxName.checked == true) {
		document.getElementById(divName).style.display='block';
	} else {
		document.getElementById(divName).style.display='none';
	}
}
function checkBx1(){
	
	var checkBox=document.getElementById("cfuStatusCheckbox");
	
	  if(checkBox.checked==true){
		
	   document.getElementById("isCfu").value=1;
	   document.getElementById("cfuMethod").value="";
	   document.getElementById("datepicker2").value="";
	  }
	   else{
		   document.getElementById("isCfu").value=0;
		   }
	  show(checkBox,"viab2");
}
function checkBx2(){
	
	var checkBox1=document.getElementById("viabStatusCheckbox");
	
	  if(checkBox1.checked==true){
		 
	   document.getElementById("isViab").value=1;
	   document.getElementById("viabMethod").value="";
	   document.getElementById("datepicker1").value="";
	  }
	   else{
		   document.getElementById("isViab").value=0;
		   }
	  show(checkBox1,"viab");
}
function checkBx3(){
	
	var checkBox2=document.getElementById("cbuStatusCheckbox");
	
	  if(checkBox2.checked==true){
		//  alert(checkBox2.checked);
	   document.getElementById("isCbu").value=1;
	   document.getElementById("datepicker3").value="";
	   
	  }
	   else{
		   document.getElementById("isCbu").value=0;
		   }
	  show(checkBox2,"viab3");
}

$j(document).ready(function(){
	
	//$j("#cfuStatusCheckbox").attr("checked",false);
	//$j("#viabStatusCheckbox").attr("checked",false);
//$j("#cbuStatusCheckbox").attr("checked",false);
	checkBx2();
	checkBx1();
	checkBx3();
	var flag =true;
	var flag1 = true;
	$j( ".column" ).sortable({
		connectWith: ".column"
	});
	$j( ".column" ).sortable({ distance: 200 });
	<s:iterator value="#request.widgetlist" >
	// $j('#'+'<s:property value="widgetParentDivId" />').offset({ top: 100, left: 300 });
	// $j('#'+'<s:property value="widgetParentDivId" />').css('width': 700px, 'height': 500px);
	
	 if('<s:property value="isMinimizable" />'=='true')
	 {
		 var parentDiv = '<s:property value="widgetDivId" />'+'parent';
		 var widgetDiv = '<s:property value="widgetDivId" />';
		$j('#'+'<s:property value="widgetDivId" />').find(".portlet-header .ui-icon").bind("click",function() {

			if('<s:property value="isClosable" />'=='true')
			 {
		    	if($j(this).is(".ui-icon-close"))
				{
						
				$j(this).parent().parent().parent().css('display','none');
					
				}
			 }
			if($j(this).is(".ui-icon-circle-plus"))
			{
				var maxId = $j(this).parent().parent().attr("id");
				//alert(maxId);
				$j("#"+maxId).dialog({
     				modal: true, width:1024, height:1024,
     				close: function() {
     					//$j(".ui-dialog-content").html("");
     					//jQuery("#subeditpop").attr("id","subeditpop_old");
     					$j("#"+maxId).dialog("destroy");
     			    }
     			   });			
			}		
			
			if($j(this).is(".ui-icon-minusthick") || $j(this).is(".ui-icon-plusthick"))
			{
				$j(this).toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick");
				
				$j(this).parents(".portlet:first").find(".portlet-content").toggle();
				
				var divid = $j(this).parent().parent().attr("id");
				if(flag)
				divHeight=document.getElementById(divid).style.height;
				if(divHeight=='100%')
				{
					divHeight = 'auto';
					}	
			    $j(this).parents(".portlet:first").css('height',divHeight);
				
			}
			
			flag=false;			
			});
	 }

	 if('<s:property value="isResizable" />'=='true')
	 {
		  var minHeight = '<s:property value="resizeMinHeight" />';
		  var minWidth = '<s:property value="resizeMinWidth" />';
		  var maxHeight = '<s:property value="resizeMaxHeight" />';
		  var maxWidth = '<s:property value="resizeMaxWidth" />';
		  $j('#'+parentDiv).resizable({helper: "ui-resizable-helper"});		  
		  $j('#'+parentDiv).resizable({minHeight: minHeight});
		  $j('#'+parentDiv).resizable({minWidth: minWidth});	
		  $j('#'+parentDiv).resizable({maxHeight: maxHeight});
		  $j('#'+parentDiv).resizable({maxWidth: maxWidth});			 
		 // $j('#'+parentDiv).resizable("option", "minWidth",minWidth );		  
	 }

	 if('<s:property value="isDragable" />'=='true')
	 {
	  //$j('#'+parentDiv).draggable();
	 }		
	 //$j('#'+parentDiv).droppable({
	 //     drop: function() { alert('dropped'); }
	//    });
	
	//for Setting the Height on the div
	$j('#'+'<s:property value="widgetDivId" />').css('height','100%');
	$j('#'+'<s:property value="widgetDivId" />').find('.portlet-content').each(function(){
          $j(this).css('height','100%');
		});		

	</s:iterator>	
	
	});
	
/* jQuery.validator.addMethod("lessThanHundredOne", function(value, element) {
    return this.optional(element) || (parseFloat(value) < 101);
}, "Percentage must be less than Hundred"); */

jQuery.validator.addMethod("greaterThanZero", function(value, element) {
    return this.optional(element) || (parseFloat(value) <= 100.00);
}, "<s:text name="garuda.cbu.cbucall.alert"/>");

function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : event.keyCode
   if ((charCode > 31 && (charCode < 46 || charCode > 57)) || charCode==45)
      return false;

   return true;
}

function showAddWidget(url){
	
	 url = url + "?pageId=2" ; 
	 showAddWidgetModal(url);
}
</script>


</head>


<body>



<div id="wrapper"><div class="headerStyle row" id="header"></div>
<div class="nav" id="nav"></div>
<div class="breadcrumbStyle hidden" id="breadcrumb"></div>

<div>
<table>
	<tr>
		<td class="align_right"><a href="javascript:void(0);"
			id="widgetId" onclick="showAddWidget('addWidgets');"><s:text name="garuda.widget.add"/></a></td>

	</tr>
</table>

</div>

<div style="padding-bottom: 20px">
<div class="mainStyle" id="main">
<div id="unitTable"><s:form action="" id="CBUUnitRptForm"
	name="CBUUnitRptForm">
	<s:hidden name="cdrCbuPojo.cordID"></s:hidden>
	<table border="0" align="center" cellpadding="0" cellspacing="0"
		id="unitreportrequest" class="tabledisplay">
		<thead>
			<tr>
				<th align="center"><s:text
					name="garuda.unitreport.label.heading" /></th>

			</tr>
		</thead>
	</table>

	<div id="fileupload" class="tabledisplay">
	<table class="tabledisplay" align="center" border="0">
		<tbody>
			<tr>
				<td><s:text name="garuda.unitreport.label.sublabel" /><s:property
					value="cdrCbuPojo.cdrCbuId" /></td>
			</tr>
		</tbody>
	</table>
	</div>
	<div class="portlet" id="cbuinfoparent">
	<div id="cbuinfo"
		class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
	<div
		class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
	<span class="ui-icon ui-icon-minusthick"></span><!--<span
		class="ui-icon ui-icon-close"></span>--><s:text
		name="garuda.unitreport.label.cbuinfo.subheading" /></div>
	<div class="portlet-content">
	<div id="cbuinfo1">
	<table width="100%" border="0" align="center" cellpadding="0"
		cellspacing="2" class="col_100 ">


		<tr>
			<td><s:text name="garuda.unitreport.label.regunitid" /></td>
			<td><s:textfield disabled="true" id="regid"
				name="cdrCbuPojo.registryId" /></td>

			<td><s:text name="garuda.unitreport.label.idnocbubag" /></td>
			<td><s:textfield disabled="true"
				name="cdrCbuPojo.numberOnCbuBag" id="cbubagid" /></td>
		</tr>
		<tr>
			<td><s:text name="garuda.unitreport.label.localcbuid" /></td>
			<td><s:textfield disabled="true" name="cdrCbuPojo.localCbuId"
				id="cbuid" /></td>

			<td><s:text name="garuda.unitreport.label.collectiondate" /></td>
			<td><s:date name="cdrCbuPojo.specimen.specCollDate"
				id="colletionDate" format="MMM dd, yyyy" /> <s:textfield
				name="colletionDate" disabled="true" id="cordCollectionDate"
				value="%{colletionDate}" /></td>
		</tr>

	</table>
	</div>
	</div>
	</div>
	</div>

	<div class="portlet" id="productinfoparent">
	<div id="productinfo"
		class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
	<div
		class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
	<span class="ui-icon ui-icon-minusthick"></span><!--<span
		class="ui-icon ui-icon-close"></span>--><s:text
		name="garuda.unitreport.label.prodinfo.subheading" /></div>
	<div class="portlet-content">
	<div id="productinfo1">
	<table border="0" cellpadding="0" cellspacing="0" width="1000px"
		class="col_100 ">
		<tbody>
			<tr>
				<td width="150px"><s:text
					name="garuda.unitreport.label.cellviability" /></td>
				<td width="100px"><s:textfield id="cordViabPostProcess"
					name="cbuUnitReportPojo.cordViabPostProcess" size="10"
					onKeyPress="return isNumberKey(event)" /></td>
				<td width="10px">%</td>
				<td width="100px"><s:text name="garuda.unitreport.label.method" /></td>
				<td width="150px"><s:select id="viabMethod"
					name="cbuUnitReportPojo.cordViabMethod" list="listViabMethod"
					listKey="pkCodeId" listValue="description" headerKey=""
					headerValue="Select" /></td>
				<td width="100px"><s:text
					name="garuda.unitreport.label.testdate" /></td>
				<td width="100px"><s:textfield
					name="cbuUnitReportPojo.strcordViabPostTestDate" class="datepic"
					id="datepicker1" /></td>

				<td width="100px"><s:text name="garuda.unitreport.label.or" /></td>
				<td width="100px"><s:text
					name="garuda.unitreport.label.notdone" /></td>


				<td width="90px"><s:checkbox name="cordViabPostStatus"
					id="viabStatusCheckbox" onclick="checkBx2()" /> <s:hidden
					name="cbuUnitReportPojo.cordViabPostStatus" id="isViab" /></td>
			</tr>
	</table>

	<div id="viab" style="display: none">
	<table border="0" cellpadding="0" cellspacing="0" width="100%"
		class="col_100 ">

		<tr>
			<td width="40%"><s:text
				name="garuda.unitreport.question.priortest" /></td>
			<td width="10%"><s:radio
				name="cbuUnitReportPojo.cordViabPostQues1"
				list="#{'true':'Yes','false':'No'}" /></td>
			<td width="40%"><s:text
				name="garuda.unitreport.question2.priortest2" /></td>
			<td width="10%"><s:radio
				name="cbuUnitReportPojo.cordViabPostQues2"
				list="#{'true':'Yes','false':'No'}" /></td>

		</tr>
	</table>
	</div>
	<table border="0" cellpadding="0" cellspacing="0" width="100%"
		class="col_100 ">
		<tbody>
			<tr>
				<td width="150px"><s:text
					name="garuda.unitreport.label.cfupostprocessingcount" /></td>
				<td width="100px"><s:textfield id="cordCfuPostProcessCount"
					name="cbuUnitReportPojo.cordCfuPostProcessCount" size="10"
					onKeyPress="return isNumberKey(event)" /></td>
				<td width="10px"></td>
				<td width="100px"><s:text name="garuda.unitreport.label.method" /></td>
				<td width="150px"><s:select id="cfuMethod"
					name="cbuUnitReportPojo.cordCfuPostMethod" list="listCfuPostMethod"
					listKey="pkCodeId" listValue="description" headerKey=""
					headerValue="Select" /></td>
				<td width="100px"><s:text
					name="garuda.unitreport.label.testdate" /></td>
				<td width="100px"><s:textfield
					name="cbuUnitReportPojo.strcordCfuPostTestDate" id="datepicker2" /></td>

				<td width="100px"><s:text name="garuda.unitreport.label.or" /></td>
				<td width="100px"><s:text
					name="garuda.unitreport.label.notdone" /></td>

				<td width="90px"><s:checkbox name="cordCfuPostStatus"
					id="cfuStatusCheckbox" onclick="checkBx1()" /> <s:hidden
					name="cbuUnitReportPojo.cordCfuPostStatus" id="isCfu" /></td>
			</tr>
		</tbody>
	</table>

	<div id="viab2" style="display: none">
	<table border="0" cellpadding="0" cellspacing="0" width="100%"
		class="col_100 ">

		<tr>
			<td width="40%"><s:text
				name="garuda.unitreport.question.priortest" /></td>

			<td width="10%"><s:radio
				name="cbuUnitReportPojo.cordCfuPostQues1"
				list="#{'true':'Yes','false':'No'}" /></td>

			<td width="40%"><s:text
				name="garuda.unitreport.question2.priortest2" /></td>
			<td width="10%"><s:radio
				name="cbuUnitReportPojo.cordCfuPostQues2"
				list="#{'true':'Yes','false':'No'}" /></td>

		</tr>
	</table>
	</div>
	<table border="0" cellpadding="0" cellspacing="0" width="100%"
		class="col_100 ">
		<tbody>
			<tr>
				<td width="150px"><s:text
					name="garuda.unitreport.label.cbupostprocessingnrbc" /></td>
				<td width="100px"><s:textfield
					name="cbuUnitReportPojo.cordCbuPostProcessCount" size="10"
					onKeyPress="return isNumberKey(event)" /></td>
				<td width="10px"></td>

				<td width="100px"><s:text
					name="garuda.unitreport.label.testdate" /></td>
				<td width="150px"><s:textfield
					name="cbuUnitReportPojo.strcordCbuPostTestDate" id="datepicker3" /></td>
				<td width="100px"><s:text name="garuda.unitreport.label.or" /></td>
				<td width="100px"><s:text
					name="garuda.unitreport.label.notdone" /></td>
				<td width="100px"><s:checkbox name="cordCbuPostStatus"
					id="cbuStatusCheckbox" onclick="checkBx3()" /> <s:hidden
					name="cbuUnitReportPojo.cordCbuPostStatus" id="isCbu" /></td>
				<td width="100px"></td>
				<td width="90px"></td>
			</tr>
		</tbody>
	</table>

	<div id="viab3" style="display: none">
	<table border="0" cellpadding="0" cellspacing="0" width="100%"
		class="col_100 ">
		<tr>
			<td width="40%"><s:text
				name="garuda.unitreport.question.priortest" /></td>

			<td width="10%"><s:radio
				name="cbuUnitReportPojo.cordCbuPostQues1"
				list="#{'true':'Yes','false':'No'}" /></td>

			<td width="40%"><s:text
				name="garuda.unitreport.question2.priortest2" /></td>
			<td width="10%"><s:radio
				name="cbuUnitReportPojo.cordCbuPostQues2"
				list="#{'true':'Yes','false':'No'}" /></td>

		</tr>
	</table>
	</div>
	<table border="0" cellpadding="0" cellspacing="0" width="100%"
		class="col_100 ">
		<tr>
			<td width="150px"><s:text
				name="garuda.unitreport.label.bacterialculture" /></td>
			<td width="100px"><s:select
				name="cbuUnitReportPojo.cordBacterialCul" list="listBacterial"
				listKey="pkCodeId" listValue="description" headerKey=""
				headerValue="Select" /></td>
			<td width="10px"></td>
			<td width="100px"><s:text
				name="garuda.unitreport.label.fungalculture" />&nbsp;&nbsp;</td>
			<td width="150px"><s:select
				name="cbuUnitReportPojo.cordFungalCul" list="listFungal"
				listKey="pkCodeId" listValue="description" headerKey=""
				headerValue="Select" /></td>
			<td width="100px"></td>
			<td width="100px"></td>
			<td width="100px"></td>
			<td width="100px"></td>
			<td width="90px"></td>
		</tr>
		<tr>
			<td><s:text name="garuda.unitreport.label.hemoglobinopathyscr" /></td>
			<td><s:select name="cbuUnitReportPojo.cordHemogScreen"
				list="listHemoPathyScr" listKey="pkCodeId" listValue="description"
				headerKey="" headerValue="Select" /></td>

		</tr>


	</table>
	</div>
	</div>
	</div>
	</div>
	<div class="portlet" id="hlainfoparent">
	<div id="hlainfo"
		class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
	<div
		class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
	<span class="ui-icon ui-icon-minusthick"></span><!--<span
		class="ui-icon ui-icon-close"></span>--> <br />
	</div>
	<table width="100%" border="0" cellpadding="0" cellspacing="0"
		class="col_100 ">
		<tr>
			<td>
			<div
				class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
			<div
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all">

			<s:text name="garuda.unitreport.label.hlainfo.subheading" /></div>
			<div class="portlet-content">
			<div id="hlainfo1">




			<table width="100%" border="0" cellpadding="0" cellspacing="0"
				class="col_100 ">


				<tr>
					<td width="75%"><s:text
						name="garuda.unitreport.question.hlatype" /></td>
					<td align=right width="25%"><s:radio
						name="cordHlaExtPojo.cordHlaTwice" list="#{'1':'Yes','0':'No'}" /></td>
				</tr>


				<tr>
					<td width="75%"><s:text
						name="garuda.unitreport.question.hlasamples" /></td>
					<td align=right width="25%"><s:radio
						name="cordHlaExtPojo.cordIndHlaSamples"
						list="#{'1':'Yes','0':'No'}" /></td>
				</tr>

				<tr>
					<td width="75%"><s:text
						name="garuda.unitreport.question.hlalab" /></td>
					<td align=right width="25%"><s:radio
						name="cordHlaExtPojo.cordHlaSecondLab"
						list="#{'1':'Yes','0':'No'}" /></td>
				</tr>
				<tr>
					<td width="75%"><s:text
						name="garuda.unitreport.question.hlacontguoussegment" /></td>
					<td align=right width="25%"><s:radio
						name="cordHlaExtPojo.cordHlaContiSeg" list="#{'1':'Yes','0':'No'}" /></td>
				</tr>
				<tr>
					<td width="75%"><s:text
						name="garuda.unitreport.question.hlarelease" /></td>
					<td align=right width="25%"><s:radio
						name="cordHlaExtPojo.cordHlaContiSegBefRel"
						list="#{'1':'Yes','0':'No'}" /></td>


				</tr>
				<tr>
					<td width="75%"><s:text
						name="garuda.unitreport.question.hlabeforerelease" /></td>
					<td align=right width="25%"><s:radio
						name="cordHlaExtPojo.cordHlaIndBefRel"
						list="#{'1':'Yes','0':'No'}" /></td>

				</tr>

				<tr>
					<td width="75%"><s:text
						name="garuda.unitreport.question.unitrequestedshipment" /></td>
					<td align=right width="25%"><s:select
						name="cordHlaExtPojo.CordHlaContiSegNum" list="listNumOfSegments"
						listKey="pkCodeId" listValue="description" headerKey=""
						headerValue="Select" /></td>
				</tr>

			</table>

			<br />
			<br />
			<br />
			<br />
			<br />
			<br />
			</div>
			</div>
			</div>

			</td>
			<td>

			<div
				class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
			<div
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all">

			<s:text name="garuda.unitreport.label.processinginfo.subheading" /></div>
			<div class="portlet-content">
			<div id="processinginfo1">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"
				class="col_100 ">


				<tr>
					<td><s:text name="garuda.unitreport.label.processingmethod" /></td>
					<td><s:select name="cbuUnitReportPojo.cordProcMethod"
						list="listProcessMethod" listKey="pkCodeId"
						listValue="description" headerKey="" headerValue="Select" /></td>
				</tr>
				<tr>

					<td><s:text name="garuda.unitreport.label.typeofbagused" /></td>
					<td><s:select name="cbuUnitReportPojo.cordTypeBag"
						list="listBagType" listKey="pkCodeId" listValue="description"
						headerKey="" headerValue="Select" /></td>

				</tr>

				<tr>

					<td><s:text name="garuda.unitreport.label.productmodification" /></td>
					<td><s:select name="cbuUnitReportPojo.cordProdModification"
						list="listProductModify" listKey="pkCodeId"
						listValue="description" headerKey="" headerValue="Select" /></td>

				</tr>


				<tr>
					<td style="vertical-align: middle"><s:text
						name="garuda.unitreport.label.plsexplain" /></td>
					<td><textarea rows="2" cols="38"
						name="cbuUnitReportPojo.cordExplainNotes"></textarea></td>
				</tr>


				<tr>
					<td style="vertical-align: middle"><s:text
						name="garuda.unitreport.label.additionalnotes" /></td>
					<td><textarea rows="2" cols="38"
						name="cbuUnitReportPojo.cordAddNotes"></textarea></td>
				</tr>


			</table>

			</div>
			</div>
			</div>
			</td>
		</tr>
	</table>
	</div>
	</div>



	<div class="portlet" id="attachmentparent">
	<div id="attachment"
		class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
	<div
		class="portlet-header ui-widget-header ui-widget-content ui-corner-all">

	<br />
	</div>


	<div id="attachment1">
	<table width="100%" border="0" cellspacing="0" cellpadding="0"
		class="col_100 ">
		<tr>
			
			<!--<td width="250px"><b><a href="#" 
								onclick="javascript:newfileupload();"><s:text
								name="garuda.unitreport.label.attachtxt" /></a></b></td>
							
						-->
			<td colspan="4" width="100%">
				<iframe id="frame1" width="100%" height="70px" src="cb_fileattach.jsp?attCodeType=attach_type&attCodeSubType=unit_rprt&codeEntityType=entity_type&codeEntitySubType=CBU">
				</iframe>
				<s:hidden id="attachmentFlag" name="attachmentFlag" value=""></s:hidden>
				<s:hidden id="browseFlag" name="browseFlag" value="0"></s:hidden>
			</td>
			
		</tr>
		<tr>
			<td width="150px"><s:text
				name="garuda.unitreport.label.completedby" /></td>
			<td width="220px"><s:textfield
				name="cbuUnitReportPojo.cordCompletedBy" id="completedby" /></td>
			<td width="100px"><s:text
				name="garuda.unitreport.label.completedstatus" /></td>
			<td width="195px"><s:select
				name="cbuUnitReportPojo.cordCompletedStatus" list="listCompStatus"
				listKey="pkCodeId" listValue="description" headerKey=""
				headerValue="Select" /></td>

		</tr>

	</table>
	</div>
	</div>
	</div>

	<table width="100%" cellspacing="0" cellpadding="0" class="col_100 ">

		<tr align="center">
			<td></td>
			<td><s:hidden name="cbuUnitReportPojo.attachmentId"
				id="attachmentId" /> <s:hidden name="timestamp" id="attachId" />
			<button type="button" onclick="javascript:saveCBUUnitReport();"><s:text
				name="garuda.unitreport.label.button.submit" /></button>
				
			<%-- <button type="button"><s:text
				name="garuda.unitreport.label.button.export" /></button> --%>
			<button type="button"
				onClick=" alert('<s:text name="garuda.cbu.cbucall.submitalert"/>');">
			<s:text name="garuda.unitreport.label.button.cancel" /></button>
			</td>
		</tr>
	</table>
	<br />
	<br />


</s:form></div>
</div>
</div>


<div class="footerStyle" id="footer"></div></div>


<div id="modelPopup2"></div>
<div id="modelPopup1"></div>
</html>