<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<jsp:include page="cb_user_rights.jsp"></jsp:include>
<s:if test="#request.moduleEntityEventCode=='View Processing Procedure From Complete Required Informaion'">
<jsp:include page="./cb_track_session_logging.jsp" />
</s:if>
		<div id="procinfo">
		<s:form>
			<table cellpadding="0" cellspacing="0" width="100%">
			    <tr align="center">
			       <td width="20%">
			          <s:text name="garuda.cbbprocedures.label.procname" />:
			       </td>
			       <td colspan="3">
			          <s:if test="#request.cbbProceduresList!=null">
			              <s:select disabled="true" id="cbbProcessProc" cssClass="cbuProcMandatory" name="cdrCbuPojo.fkCbbProcedure" listKey="pkProcId" listValue="procName" list="#request.cbbProceduresList" headerKey="-1" headerValue="Select" value="%{cdrCbuPojo.fkCbbProcedure}"></s:select>
			           </s:if>     
					</td>
			    </tr>
			</table>
			<s:if test="cdrCbuPojo.fkCbbProcedure!=null && cdrCbuPojo.fkCbbProcedure!=-1">
			<table>
				<tr align="right">
					<td colspan="4">
					     <jsp:include page="cord-entry/cb_procedure_refresh.jsp" >
					        <jsp:param value="true" name="sampInventReadOnly"/>
					     </jsp:include>
					</td>
				</tr>
			</table>
			</s:if>
		</s:form>
		</div>	 
  	