<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<jsp:include page="cb_user_rights.jsp"></jsp:include>
			
			<div>
				<s:if test="hasViewPermission(#request.updateHistry_Mrq)==true">
					<div id="mrqinfocontent" >
					<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;" onclick="toggleDiv('mrqinfo');" >
						<span class="ui-icon ui-icon-triangle-1-s" ></span>
					
						<span class="ui-icon ui-icon-print" onclick="window.open('cbuPdfReports?repId=178&repName=MRQ Report&selDate=&params=<s:property value="cdrCbuPojo.cordID" />:MRQ','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');toggleDiv('mrqinfo');toggleDiv('mrqinfo');">
						</span> 
						<s:text name="garuda.cordentry.label.mrq" />
					</div><table><tr><td>
					<div id="mrqinfo">
						<jsp:include page="./cb_MRQformVersion.jsp"></jsp:include>
					</div>
					</td></tr></table>
					</div>
				</s:if>
		    </div>
		    
		     <div>
		     	<s:if test="hasViewPermission(#request.updateHistry_FMHQ)==true">
					<div id="fmhqinfocontent">
					<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;" onclick="toggleDiv('fmhqinfo')">
						<span class="ui-icon ui-icon-triangle-1-s"></span>
						<span class="ui-icon ui-icon-print" onclick="window.open('cbuPdfReports?repId=179&repName=FMHQ Report&selDate=&params=<s:property value="cdrCbuPojo.cordID" />:FMHQ','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');toggleDiv('fmhqinfo');toggleDiv('fmhqinfo');"></span>
						<s:text name="garuda.cordentry.label.fmhq" />
					</div><table><tr><td>
					<div id="fmhqinfo">
						<jsp:include page="./cb_FMHQformVersion.jsp"></jsp:include>
					</div></td></tr></table>
					</div>
				</s:if>
		    </div>
		    
				<div id="healthhistorynotecontent" onclick="toggleDiv('healthhistorynote')"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><span
				class="ui-icon ui-icon-triangle-1-s"></span> <s:text
				name="garuda.cdrcbuview.label.button.healthHistoryclinicalnotes" /></div>
			<table cellpadding="0" cellspacing="0" width="100%">
			<tr><td>				
				<div id="healthhistorynote" style=" width: 463px; height: 100%;">
					<div id="loadHealthHistoryDiv">
							<s:set name="cbuCateg" value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@CLINIC_CATEGORY,@com.velos.ordercomponent.util.VelosGarudaConstants@HLT_NOTE_CATEGORY_CODESUBTYPE) " />
							<jsp:include page="modal/cb_load_clinicalNote.jsp"></jsp:include>
					</div>
			<s:if test="hasNewPermission(#request.vClnclNote)==true">
			<s:if test='(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)) '>
			<button type="button" disabled="disabled"><s:text
				name="garuda.cdrcbuview.label.button.addnotes" /></button>
				  </s:if>
				  <s:else>
				  		<button type="button"
				onclick="fn_showModalMRQForNotes('Clinical Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','clinicalNotesCategory?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&cat=HLT_NOTE_CATEGORY_CODESUBTYPE','550','850','notesModalsDiv');"><s:text
				name="garuda.cdrcbuview.label.button.addnotes" /></button>
				  </s:else>
				  </s:if>
				  </div></td></tr></table>
				  <div id="loadHealthHistroyDocInfoDiv">
			<s:if test="(#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@HEALTH_HISt_SCR_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@HEALTH_HISt_SCR_CODESUBTYPE].size>0 && hasViewPermission(#request.viewHealthHisCat)==true)">
				<div id="loadHealthHistroyDocInfocontent" onclick="toggleDiv('loadHealthHistroyDocInfo')"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;">
				<span class="ui-icon ui-icon-triangle-1-s"></span> 
				<s:text name="garuda.uploaddoc.label.button.healthHistoryuploaddocument" />
				</div>
			<div id="loadHealthHistroyDocInfo">
			<table><tr><td>
				<s:set name="cbuDocCateg" value="@com.velos.ordercomponent.util.VelosGarudaConstants@HEALTH_HISt_SCR_CODESUBTYPE" scope="request" />
				<div id="loadHealthHisRevokDoc">
						<jsp:include page="modal/cb_load_upload_document.jsp">
							<jsp:param value="<%=request.getAttribute(\"cbuDocCateg\")%>" name="cbuDocCateg"/>
							<jsp:param value="loadHealthHisRevokDoc" name="replaceDivId"/>
						</jsp:include>
				</div>
				 					
			</td></tr></table>
			</div>
		 </s:if>	
		 </div>
		 <script>
		 function fn_showModalMRQForNotes(title,url,hight,width,id){
			 	var patientId = $j('#patientId').val();
				var patient_data = '';
				if(patientId!="" && patientId != null && patientId != "undefined"){
					patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
				}
				title=title+patient_data;
				showModals(title,url,hight,width,id);
			}
		 </script>