<%@ taglib prefix="s"  uri="/struts-tags"%>
<script>
var documentId ='<s:property value="timestamp"/>';

if(documentId != "" && documentId!=null){
	parent.document.getElementById("timestamp").value = documentId;
}
function trackFile(){
	document.forms["cordimp"].submit();	
}

function changeUploadFlag(){
	var fileName = document.getElementById("xmlFileUpload").value;
	if(fileName!= undefined && fileName!=null && fileName!=""){
		var arr = fileName.split("/");
		if(arr.length >0){
	        var fileExt = arr[arr.length-1];
	        var extArr =  fileExt.split('.');
	        if(extArr.length>0){
	        	var ext = extArr[extArr.length-1];
	        	if(ext.toUpperCase()=='XML'){
		        	trackFile();	
		        	//$j("#xmlalert").css('display','none'); 
		        	document.getElementById("xmlalert").style.display = 'none';
			    }else{
                   //$j("#selFileName").css('display','none');
                     document.getElementById("xmlalert").style.display = 'block';
                   document.getElementById("selFileName").style.display = 'none';
                  
			    }
		    }
	    }
	}
  	  	
 }
</script>
<form action="cordImport" id="cordimp" method="POST" enctype="multipart/form-data" name="cordimp">	 
           
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
		     <s:if test="#request.fileUploadFileName!=null">		
				<tr id="selFileName">
						<td align="center" colspan="2"><strong>Selected File is <s:property value="#request.fileUploadFileName" /> </strong></td>
			    </tr>
	        </s:if>
			<tr>
				<td align="right">
					<s:text name="garuda.cordimport.label.xmlfiletoimport"></s:text>:
				</td>
				<td align="left">
					<input type="file" id="xmlFileUpload" name="fileUpload" onchange="changeUploadFlag();" />
				</td>
			</tr>
			<tr style="display: none;" id="xmlalert">
			   <td colspan="2" align="center">
			         <div style="margin-top: 0px;" class="ui-state-error ui-corner-all"> 
						<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-alert"></span>
						<strong><s:text name="garuda.cbu.label.xmlalert"/></strong></p>
					</div>
			   </td>
			</tr>							   
		</table>						
</form>		