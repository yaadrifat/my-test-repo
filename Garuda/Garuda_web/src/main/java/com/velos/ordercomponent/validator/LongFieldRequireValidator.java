package com.velos.ordercomponent.validator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.CompoundRoot;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

/**
 * @author Mohiuddin Ali Ahmed
 * @author Anurag Upadhyay
 * @version 1.0
 * 
 */

public class LongFieldRequireValidator extends FieldValidatorSupport {
	 private boolean searchableCheck;	

	public boolean isSearchableCheck() {
		return searchableCheck;
	}

	public void setSearchableCheck(boolean searchableCheck) {
		this.searchableCheck = searchableCheck;
	}
	@Override
	public void validate(Object object) throws ValidationException {
		// TODO Auto-generated method stub
		//try{
		String fieldName = getFieldName();
		Object obj = getFieldValue(fieldName, object);
		Long value=(obj instanceof String)?Long.valueOf(obj.toString()):(obj==null)?null:(Long)obj;	
		 CompoundRoot cRoot= ActionContext.getContext().getValueStack().getRoot();
		 long cordSearchable = (Long) getFieldValue("cordSearchable", cRoot); 
		 
		if(searchableCheck && (value==null || value==-1) && cordSearchable==1){
			   addFieldError(fieldName, object);
		       return;
		}
		/*}catch(Exception e){
			System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\fieldName :" +getFieldName()+"\n\n\n\n\n\n\n\n\n");
			
		}*/
		
	}

}
