package com.velos.ordercomponent.controller;

import java.util.List;
import com.velos.ordercomponent.business.pojoobjects.AppMessagesPojo;
import com.velos.ordercomponent.service.VelosService;
import com.velos.ordercomponent.service.impl.VelosServiceImpl;

/**
 * @author jamarendra
 *
 */
public class AppMessagesController {
	VelosService service;
	public AppMessagesPojo updateAppMessages(AppMessagesPojo appMessagesPojo) throws Exception{
		service = VelosServiceImpl.getService();
		return service.updateAppMessages(appMessagesPojo);
	}
	public AppMessagesPojo getAppMessagesById(AppMessagesPojo appMessagesPojo) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getAppMessagesById(appMessagesPojo);
	}
	public List getGroupsBySiteId(Long siteId)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getGroupsBySiteId(siteId);
	}
}
