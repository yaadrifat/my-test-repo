package com.velos.ordercomponent.action;

import com.velos.ordercomponent.business.util.VelosMidConstants;

public class ServerOrderBy extends VelosBaseAction{

	
	
	public String FetchOrders_OrderBy(String ColumnIndex, String Type){
		
		String orderBycondition="";
		
		String tableColumnNames[]={"0","1","2","3","4","5"};
		String columnNames[]={"","crd.cord_registry_id","CASE WHEN f_codelst_desc(ord.order_type)='"+VelosMidConstants.CT_ORDER+"' THEN decode(ord.order_sample_at_lab,'Y','"+VelosMidConstants.CT_LAB_ORDER+"',"+
			                "'N','"+VelosMidConstants.CT_SHIP_ORDER+"') WHEN f_codelst_desc(ord.order_type)!='"+VelosMidConstants.CT_ORDER+"' THEN f_codelst_desc(ord.order_type) END","ordhdr.order_open_date","f_codelst_desc(ORD.ORDER_STATUS)","usr.usr_logname"};
		
		orderBycondition=" ORDER BY "+columnNames[Integer.parseInt(ColumnIndex)]+" "+Type;
		
		if(ColumnIndex.equals("5")){
			orderBycondition=" ORDER BY "+columnNames[Integer.parseInt(ColumnIndex)]+" "+Type +" nulls last";
		}
		
		//System.out.println("order by condition for orders..........."+orderBycondition);
		
		return  orderBycondition;
		
		
		
	}
	
	public String FetchWorkflow_OrderBy(String ColumnIndex, String Type){
		
		String orderBycondition="";
		
		String tableColumnNames[]={"0","1","2","3","4"};
		String columnNames[]={"","CASE WHEN f_codelst_desc(ord.order_type)='"+VelosMidConstants.CT_ORDER+"' THEN decode(ord.order_sample_at_lab,'Y','"+VelosMidConstants.CT_LAB_ORDER+"',"+
			                "'N','"+VelosMidConstants.CT_SHIP_ORDER+"') WHEN f_codelst_desc(ord.order_type)!='"+VelosMidConstants.CT_ORDER+"' THEN f_codelst_desc(ord.order_type) END","crd.cord_registry_id","ordhdr.order_open_date","f_codelst_desc(ORD.ORDER_STATUS)"};
		
		orderBycondition=" ORDER BY "+columnNames[Integer.parseInt(ColumnIndex)]+" "+Type;
		
		
		//System.out.println("order by condition workflow..........."+orderBycondition);
		
		return  orderBycondition;
		
		
		
	}
	public String FetchAlerts_OrderBy(String ColumnIndex, String Type){
		
		String orderBycondition="";
		
		String tableColumnNames[]={"0","1","2","3","4"};
		String columnNames[]={"","CASE WHEN f_codelst_desc(ord.order_type)='"+VelosMidConstants.CT_ORDER+"' THEN decode(ord.order_sample_at_lab,'Y','"+VelosMidConstants.CT_LAB_ORDER+"',"+
			                "'N','"+VelosMidConstants.CT_SHIP_ORDER+"') WHEN f_codelst_desc(ord.order_type)!='"+VelosMidConstants.CT_ORDER+"' THEN f_codelst_desc(ord.order_type) END","crd.cord_registry_id","ordhdr.order_open_date","f_codelst_desc(ORD.ORDER_STATUS)","ains.CREATED_ON"};
		
		orderBycondition=" ORDER BY "+columnNames[Integer.parseInt(ColumnIndex)]+" "+Type;
		
		
		//System.out.println("order by condition alerts..........."+orderBycondition);
		
		return  orderBycondition;
		
	}
    public String FetchPSOrders_OrderBy(String ColumnIndex, String Type){
		
		String orderBycondition="";
		
		String tableColumnNames[]={"0","1","2","3","4"};
		String columnNames[]={"","site.site_id","ADR.ADD_CITY","CORD.CORD_REGISTRY_ID","CORD.CORD_LOCAL_CBU_ID","CASE WHEN f_codelst_desc(ord.order_type)='"+VelosMidConstants.CT_ORDER+"' THEN decode(ord.order_sample_at_lab,'Y','"+VelosMidConstants.CT_LAB_ORDER+"',"+
			                "'N','"+VelosMidConstants.CT_SHIP_ORDER+"') WHEN f_codelst_desc(ord.order_type)!='"+VelosMidConstants.CT_ORDER+"' THEN f_codelst_desc(ord.order_type) END","f_codelst_desc(ORD.ORDER_PRIORITY)","ordhdr.order_open_date","ceil(SYSDATE-ordhdr.order_open_date)","f_codelst_desc(ORD.ORDER_STATUS)","usr.usr_logname","ORD.ORDER_ACK_FLAG","cbustat1.cbu_status_desc","cbustat.cbu_status_desc","usr1.usr_logname"};
		
		orderBycondition=" "+columnNames[Integer.parseInt(ColumnIndex)]+" "+Type;
		
		if(ColumnIndex.equals("10")){
			orderBycondition=" "+columnNames[Integer.parseInt(ColumnIndex)]+" "+Type +" nulls last";
		}
		//System.out.println("order by condition alerts..........."+orderBycondition);
		
		return  orderBycondition;
		
	}
    public String FetchSavedInProgress_OrderBy(String ColumnIndex, String Type){
		
		String orderBycondition="";
		//System.out.println("ColumnIndex:::"+ColumnIndex);
		String tableColumnNames[]={"0","1","2","3"};
		String columnNames[]={"","creationdt","C.CORD_REGISTRY_ID","C.CORD_ENTRY_PROGRESS"};
		
		orderBycondition=" ORDER BY "+columnNames[Integer.parseInt(ColumnIndex)]+" "+Type;
		
		//System.out.println("order by condition NotAvailCbus..........."+orderBycondition);
		
		return  orderBycondition;
		
	}
    public String FetchLookup_OrderBy(String ColumnIndex, String Type){
		
		String orderBycondition="";
		//System.out.println("ColumnIndex:::"+ColumnIndex);
		String tableColumnNames[]={"0","1","2","3"};
		String columnNames[]={"C.CORD_REGISTRY_ID","C.registry_maternal_id","C.CORD_LOCAL_CBU_ID","C.maternal_local_id","C.CORD_ISBI_DIN_CODE","site.SITE_NAME","pat.PATIENTID"};
		
		orderBycondition=" ORDER BY "+columnNames[Integer.parseInt(ColumnIndex)]+" "+Type+" nulls last";
		
	//	System.out.println("order by condition NotAvailCbus..........."+orderBycondition);
		
		return  orderBycondition;
		
	}
    public String FetchUploadDoc_OrderBy(String ColumnIndex, String Type){
		
		String orderBycondition="";
		//System.out.println("ColumnIndex:::"+ColumnIndex);
		String tableColumnNames[]={"0","1","2","3"};
		String columnNames[]={"","C.CORD_REGISTRY_ID","C.registry_maternal_id","C.CORD_LOCAL_CBU_ID","C.maternal_local_id","C.CORD_ISBI_DIN_CODE","C.CORD_ID_NUMBER_ON_CBU_BAG"};
		
		orderBycondition=" ORDER BY "+columnNames[Integer.parseInt(ColumnIndex)]+" "+Type+" nulls last";
		
		//System.out.println("order by condition UploadDoc..........."+orderBycondition);
		
		return  orderBycondition;
		
	}
    
    public String FetchNotAvailCbus_OrderBy(String ColumnIndex, String Type){
		
		String orderBycondition="";
		//System.out.println("ColumnIndex:::"+ColumnIndex);
		String tableColumnNames[]={"0","1","2"};
		String columnNames[]={"C.CORD_REGISTRY_ID","CS.CBU_STATUS_DESC","CORD_CREATION_DATE"};
		
		if(ColumnIndex.equals("2")){
			orderBycondition=" ORDER BY "+columnNames[Integer.parseInt(ColumnIndex)]+" "+Type+" nulls last";
		}else{
			orderBycondition=" ORDER BY "+columnNames[Integer.parseInt(ColumnIndex)]+" "+Type;
		}
		
		
		
		
		//System.out.println("order by condition NotAvailCbus..........."+orderBycondition);
		
		return  orderBycondition;
		
	}
    public String FetchPSNotAvail_OrderBy(String ColumnIndex, String Type){
    	
    	String orderBycondition="";
		
		String tableColumnNames[]={"0","1","2","3","4"};
		String columnNames[]={"","site.site_id","cord.cord_registry_id","cord.cord_local_cbu_id","nmdpstat.cbu_status_desc","loclstat.cbu_status_desc","usr.usr_logname"};
		
		orderBycondition=" ORDER BY "+columnNames[Integer.parseInt(ColumnIndex)]+" "+Type;
		
		
		//System.out.println("order by condition alerts..........."+orderBycondition);
		
		return  orderBycondition;
    	
    }
    
    public String FetchPSSavInProg_OrderBy(String ColumnIndex, String Type){
    	
    	String orderBycondition="";
		
		String tableColumnNames[]={"0","1","2","3","4"};
		String columnNames[]={"","site.site_id","cord.cord_registry_id","cord.cord_local_cbu_id","cord.created_on","ceil(sysdate-cord.created_on)","cord.cord_entry_progress"};
		
		orderBycondition=" ORDER BY "+columnNames[Integer.parseInt(ColumnIndex)]+" "+Type;
		
		
		//System.out.println("order by condition alerts..........."+orderBycondition);
		
		return  orderBycondition;
    	
    }
    public String FetchHistory_OrderBy(String ColumnIndex, String Type){
    	
    	String orderBycondition="";
		
		String tableColumnNames[]={"0","1","2","3","4"};
		String columnNames[]={"","HIST.HISTORYDATE","HIST.DESC1,HIST.DESC2, HIST.DESC3","HIST.USERNAME","HIST.ORDERTYPE"};
		
		orderBycondition=" ORDER BY "+columnNames[Integer.parseInt(ColumnIndex)]+" "+Type;
		
		//System.out.println("order by History..........."+orderBycondition);
		
		return  orderBycondition;
    	
    }
    public String FetchQBuilder_OrderBy(String ColumnIndex, String Type){
    	
    	String orderBycondition="";
		
		String tableColumnNames[]={"0","1","2","3","4"};
		String columnNames[]={"c.SITE_ID","c.REGISTRY_ID","c.cord_local_cbu_id","c.cord_id_number_on_cbu_bag","c.cord_isbi_din_code","10","c.ORDER_OPEN_DATE","c.SHIP_DATE","c.INFUSION_DATE","c.PATIENT_ID","c.CURRENT_DIAGNOSIS","c.TRANS_SITE_ID","c.TRANS_SITE_NAME"};
		
		//fix done for sorting records. Amit
		if(ColumnIndex.equals("0")){
			orderBycondition=" ORDER BY c.SITE_ID, c.REGISTRY_ID " +Type+ ", DECODE(req_type,(SELECT CODELST_DESC FROM ERES.ER_CODELST WHERE CODELST_TYPE='order_type' AND CODELST_SUBTYP='OR'),C.ORDER_OPEN_DATE) "+Type+" nulls last";
		}
		else if(ColumnIndex.equals("1")){
			orderBycondition=" ORDER BY c.REGISTRY_ID " +Type+ ", DECODE(req_type,(SELECT CODELST_DESC FROM ERES.ER_CODELST WHERE CODELST_TYPE='order_type' AND CODELST_SUBTYP='OR'),C.ORDER_OPEN_DATE) "+Type+" nulls last";
		}
		else{
			orderBycondition=" ORDER BY "+columnNames[Integer.parseInt(ColumnIndex)]+" "+Type;
		}
		//System.out.println("order by Query Builder..........."+orderBycondition);
		
		return  orderBycondition;
    	
    }
    public String FetchQBuilder_CBUID_OrderBy(String ColumnIndex, String Type){
    	
        String orderBycondition="";
		
		String tableColumnNames[]={"0","1","2","3","4"};
		String columnNames[]={"c.SITE_ID","c.REGISTRY_ID","c.cord_local_cbu_id","c.cord_id_number_on_cbu_bag","c.cord_isbi_din_code","C.CORD_HISTORIC_CBU_ID","C.registry_maternal_id","C.maternal_local_id","10","c.SHIP_DATE","c.SHIP_DATE","c.ORDER_OPEN_DATE"};
		
		orderBycondition=" ORDER BY "+columnNames[Integer.parseInt(ColumnIndex)]+" "+Type+" nulls last";
		
		if(ColumnIndex.equals("0")){
			orderBycondition=" ORDER BY c.SITE_ID, c.REGISTRY_ID nulls last";
		}
		//System.out.println("order by Query Builder..........."+orderBycondition);
		
		return  orderBycondition;
    	
    }
    public String FetchQBuilder_LICElig_OrderBy(String ColumnIndex, String Type){
    	
    	String orderBycondition="";
		
		String tableColumnNames[]={"0","1","2","3","4"};
		String columnNames[]={"c.SITE_ID",/*0*/
							  "c.REGISTRY_ID",/*1*/
							  "c.cord_local_cbu_id",/*2*/
							  "c.cord_id_number_on_cbu_bag",/*3*/
							  "C.SPEC_COLLECTION_DATE",/*4*/
							  "C.CORD_REGISTRATION_DATE",/*5*/
							  "ERES.F_GET_CBU_STATUS(C.CORD_NMDP_STATUS)",/*6*/
							  "ERES.F_GET_CBU_STATUS(C.FK_CORD_CBU_STATUS)",/*7*/
							  "ERES.f_codelst_desc(C.FK_CORD_CBU_LIC_STATUS)",/*8*/
							  "13",/*9*/
							  "ERES.f_codelst_desc(C.fk_cord_cbu_eligible_status)",/*10*/
							  "15",/*11*/
							  "C.CORD_BABY_BIRTH_DATE",/*12*/
							  "16",/*13*/
							  "ERES.f_codelst_desc(C.NMDP_RACE_ID)",/*14*/
							  "C.funded_cbu",/*15*/
							  "ERES.f_codelst_desc(C.FK_CORD_BACT_CUL_RESULT)",/*16*/
							  "ERES.f_codelst_desc(C.FK_CORD_FUNGAL_CUL_RESULT)",/*17*/
							  "ERES.f_codelst_desc(C.HEMOGLOBIN_SCRN)",/*18*/
							  "PROC.PROC_NAME",/*19*/
							  "Nucl_cell_count.resultdata",/*20*/
							  "CD34_cell_count.resultdata",/*21*/
							  "Viability_Method.resultdata",/*22*/
							  "ERES.f_codelst_desc(Viability_Method.fkTestMethod)",/*23*/
							  "Final_Prod_Vol.resultdata",/*24*/
							  "C.SHIP_DATE",/*25*/
							  "C.SHIP_DATE",/*26*/
							  "31",/*27*/
							  "C.ORDER_OPEN_DATE", /*28*/
							  "ERES.f_codelst_desc(C.FUNDING_CATEGORY)",/*29*/
							  "Nucl_cell_count_unknown.resultdata"/*30*/
							  };
		//fix done for sorting records. Amit
		if(ColumnIndex.equals("0")){
			orderBycondition=" ORDER BY c.SITE_ID, c.REGISTRY_ID "+ Type +", C.ORDER_OPEN_DATE "+ Type +" nulls last";
		}
		else if(ColumnIndex.equals("1")){
			orderBycondition=" ORDER BY c.REGISTRY_ID "+ Type +", C.ORDER_OPEN_DATE "+ Type +" nulls last";
		}
		else{
			orderBycondition=" ORDER BY "+columnNames[Integer.parseInt(ColumnIndex)]+" "+Type;
		}
			
		
		//System.out.println("order by Query Builder..........."+orderBycondition);
		
		return  orderBycondition;
    	
    }
    public String FetchCordImport_OrderBy(String ColumnIndex, String Type){
    	
    	String orderBycondition="";
		
		String tableColumnNames[]={"0","1","2","3","4","5"};
		String columnNames[]={"","h.PK_CORD_IMPORT_HISTORY","h.START_TIME", "h.END_TIME","h.IMPORT_FILE_NAME","u.USR_FIRSTNAME||' '||u.USR_LASTNAME","h.IMPORT_HISTORY_DESC"};
		
		orderBycondition=" ORDER BY "+columnNames[Integer.parseInt(ColumnIndex)]+" "+Type;
		
		//System.out.println("\n\n\n\norder by Import History..........."+orderBycondition);
		
		return  orderBycondition;
    	
    }
    public String FetchProcedure_OrderBy(String ColumnIndex, String Type){
    	
    	String orderBycondition="";
		
		String tableColumnNames[]={"0","1","2","3","4"};
		String columnNames[]={"","","procName","procStartDate","procTermiDate"};
		
		orderBycondition=" ORDER BY "+columnNames[Integer.parseInt(ColumnIndex)]+" "+Type;
		
		
		if(ColumnIndex.equals("3") || ColumnIndex.equals("4")){
			//System.out.println("Inside appending nulls last.................");
			orderBycondition=orderBycondition+" NULLS LAST";
		}
		
		
		//System.out.println("order by Procedure..........."+orderBycondition);
		
		return  orderBycondition;
    	
    }
    public String FetchCBBProfile_OrderBy(String ColumnIndex, String Type){
    	
    	String orderBycondition="";
		
		String tableColumnNames[]={"0","1","2"};
		String columnNames[]={"","siteIdentifier","siteName"};
		
		orderBycondition=" ORDER BY "+columnNames[Integer.parseInt(ColumnIndex)]+" "+Type;
		
		//System.out.println("order by Procedure..........."+orderBycondition);
		
		return  orderBycondition;
    	
    }
    
public String fetchDocInfoAudit_OrderBy(String ColumnIndex, String Type){
    	String orderBycondition="";
		String tableColumnNames[]={"0","1","2","3","4"};
		String columnNames[]={"category","subcategory","user_id","timestamp","action"};
		orderBycondition=" ORDER BY "+columnNames[Integer.parseInt(ColumnIndex)]+" "+Type;
		return  orderBycondition;
}    
    
    
public String fetchCbuInfoAudit_OrderBy(String ColumnIndex, String Type){
    	String orderBycondition="";
		String tableColumnNames[]={"0","1","2","3","4","5"};
		String columnNames[]={"column_display_name","user_id","timestamp","action","old_value","new_value"/*,"PK_COLUMN_ID"*/};
		orderBycondition=" ORDER BY "+columnNames[Integer.parseInt(ColumnIndex)]+" "+Type;
		return  orderBycondition;
}

public String fetchCbuInformationAudit_OrderBy(String ColumnIndex, String Type){
	String orderBycondition="";
	String tableColumnNames[]={"0","1","2","3","4","5"};
	String columnNames[]={"column_display_name","user_id","timestamp desc, PK_COLUMN_ID ","action","old_value","new_value"/*,"PK_COLUMN_ID"*/};
	orderBycondition=" ORDER BY "+columnNames[Integer.parseInt(ColumnIndex)]+" "+Type;
	return  orderBycondition;
}

   
public String fetchFormInfoAudit_OrderBy(String ColumnIndex, String Type){
	String orderBycondition="";
	String tableColumnNames[]={"0","1","2","3"};
	String columnNames[]={"formname","fversion","user_id","created_on"};
	orderBycondition=" ORDER BY "+columnNames[Integer.parseInt(ColumnIndex)]+" "+Type;
	return  orderBycondition;
}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
}
