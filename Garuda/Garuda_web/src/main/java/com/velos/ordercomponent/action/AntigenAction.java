package com.velos.ordercomponent.action;

import com.velos.ordercomponent.business.pojoobjects.AntigenPojo;
import com.velos.ordercomponent.controller.CBUController;

public class AntigenAction extends VelosBaseAction {

	private Boolean dbValue;
	private String entityName;
	private String entityField;
	private String entityValue;	
	private String entityField1;
	private String entityValue1;
	private CBUController cbuController;
	
	
	
	public Boolean getDbValue() {
		return dbValue;
	}

	public void setDbValue(Boolean dbValue) {
		this.dbValue = dbValue;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getEntityField() {
		return entityField;
	}

	public void setEntityField(String entityField) {
		this.entityField = entityField;
	}

	public String getEntityValue() {
		return entityValue;
	}

	public void setEntityValue(String entityValue) {
		this.entityValue = entityValue;
	}

	public String getEntityField1() {
		return entityField1;
	}

	public void setEntityField1(String entityField1) {
		this.entityField1 = entityField1;
	}

	public String getEntityValue1() {
		return entityValue1;
	}

	public void setEntityValue1(String entityValue1) {
		this.entityValue1 = entityValue1;
	}

	public AntigenAction(){
		cbuController = new CBUController();
	}
	
	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String validateAntigen() throws NumberFormatException, Exception{
		AntigenPojo antigenPojo = null;
		if(getEntityValue()!=null && !getEntityValue().equals("") && !getEntityValue().equals("null")){
			antigenPojo = cbuController.getLatestAntigen(Long.parseLong(getEntityValue()));
			if(antigenPojo.getPkAntigen()==Long.parseLong(getEntityValue())){
				setDbValue(true);
			}else{
				setDbValue(false);
			}
		}else{
			setDbValue(false);
		}
		return SUCCESS;
	}

}
