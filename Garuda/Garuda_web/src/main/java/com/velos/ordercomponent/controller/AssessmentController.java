package com.velos.ordercomponent.controller;

import java.util.List;

import com.velos.ordercomponent.business.pojoobjects.AssessmentPojo;
import com.velos.ordercomponent.business.pojoobjects.SearchPojo;
import com.velos.ordercomponent.service.VelosService;
import com.velos.ordercomponent.service.impl.VelosServiceImpl;

/**
 * @author jamarendra
 *
 */
public class AssessmentController {
	VelosService service;
	public List<AssessmentPojo> saveAssessfromsession(List<AssessmentPojo> assessmentPojo,List patLabsPojos) throws Exception{
		service = VelosServiceImpl.getService();
		return service.saveUpdateAssessfromsession(assessmentPojo,patLabsPojos);
	}
	
	public AssessmentPojo saveOrUpdateAssessment(AssessmentPojo assessmentPojo) throws Exception{
		service=VelosServiceImpl.getService();
		return service.saveorUpdateAssessment(assessmentPojo);
	}
	
	public AssessmentPojo getAssesmentPojo(AssessmentPojo assessmentPojo)throws Exception{
		service=VelosServiceImpl.getService();
		return service.getAssessmentPojoById(assessmentPojo);
	}
	public List<AssessmentPojo> saveOrUpdateAssessmentList(List<AssessmentPojo> assessmentPojoList)throws Exception{
		service=VelosServiceImpl.getService();
		return service.saveOrUpdateAssessmentList(assessmentPojoList);
	}
	
}