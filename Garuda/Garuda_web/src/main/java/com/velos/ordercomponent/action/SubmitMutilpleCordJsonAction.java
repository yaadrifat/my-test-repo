package com.velos.ordercomponent.action;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.velos.ordercomponent.business.domain.CdrCbu;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.EntitySamplesPojo;
import com.velos.ordercomponent.business.pojoobjects.PatLabsPojo;
import com.velos.ordercomponent.controller.CBUController;
import com.velos.ordercomponent.util.VelosGarudaConstants;


public class SubmitMutilpleCordJsonAction extends VelosBaseAction{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CdrCbuPojo cdrCbuPojo;
	private Boolean successFlag;
	private List<PatLabsPojo> postTestList;
	private EntitySamplesPojo entitySamplesPojo;

	public List<PatLabsPojo> getPostTestList() {
		return postTestList;
	}

	public void setPostTestList(List<PatLabsPojo> postTestList) {
		this.postTestList = postTestList;
	}

	public EntitySamplesPojo getEntitySamplesPojo() {
		return entitySamplesPojo;
	}

	public void setEntitySamplesPojo(EntitySamplesPojo entitySamplesPojo) {
		this.entitySamplesPojo = entitySamplesPojo;
	}

	public CdrCbuPojo getCdrCbuPojo() {
		return cdrCbuPojo;
	}

	public void setCdrCbuPojo(CdrCbuPojo cdrCbuPojo) {
		this.cdrCbuPojo = cdrCbuPojo;
	}

	public Boolean getSuccessFlag() {
		return successFlag;
	}

	public void setSuccessFlag(Boolean successFlag) {
		this.successFlag = successFlag;
	}

	public String saveCordsForSearch() {		
		CBUAction action = new CBUAction();
		CBUController cbuController = new CBUController();
		   successFlag = false;
				   /*updateCord = cbuController.saveCordListForSearch(cordListForSearch);*/
				    	  /* try{
							   CdrCbuPojo cdrCbuPojo = new CdrCbuPojo();
							   long cordId = Long.valueOf(request.getParameter("cordId"));
							   cdrCbuPojo.setCordID(cordId);
							   cdrCbuPojo = new CBUController().getCordInfoById(cdrCbuPojo);
							   setCdrCbuPojo(cdrCbuPojo);
							   
							   Boolean flag = new CBUAction().deferStatus(getCdrCbuPojo());
							   System.out.println("Flag is inside saveCordFOrsearch!!!!!"+cdrCbuPojo.getCordID());
							   if(!flag){ 
								   System.out.println("No for autodefer@@@@@@@@@@@@@@@@@");
								      getCdrCbuPojo().setFkCordCbuStatus(getCbuStatusPkByCode(AV));
								      getCdrCbuPojo().setCordSearchable(1l);
								      new CBUAction().setGuid();
								      
								      autoDeferFlag="false";
				                      new CBUController().saveOrUpdateCordInfo(getCdrCbuPojo());
									  updateCord++;
							   }
							   else
							   {
								   System.out.println("auto defer!!!@@@@@@@@@@@@@2");
								   autoDeferFlag="true";
								   setAutoDeferFlag(autoDeferFlag);
								   
							   }
				    	   }catch (Exception e) {
							e.printStackTrace();
							
						   }	*/	
	       try{
			   CdrCbuPojo cdrCbuPojo = new CdrCbuPojo();			
			   cdrCbuPojo = cbuController.getCordInfoById(getCdrCbuPojo());
			   setCdrCbuPojo(cdrCbuPojo);
			  // System.out.println("CordId is!!!!!!!!!!!!!"+getCdrCbuPojo().getCordID());
			   Boolean flag = false;
			   Long valuePostFkTimingTest = getCodeListPkByTypeAndSubtype(VelosGarudaConstants.TIMING_OF_TEST, VelosGarudaConstants.POST_TEST);
			   postTestList = cbuController.getPatLabsPojoInfo(getCdrCbuPojo().getFkSpecimenId(),valuePostFkTimingTest);
			   setPostTestList(postTestList);		
			   entitySamplesPojo = cbuController.getEntitySampleInfo(getCdrCbuPojo().getCordID());
			   setEntitySamplesPojo(entitySamplesPojo);
			   //flag = action.setAutoDeferStatusForCord(getPostTestList(),getCdrCbuPojo(),getEntitySamplesPojo());
			   /*if(!flag){ 
				   if(action.getCdrCbuPojo().getFkCordCbuStatus()==null)
				       getCdrCbuPojo().setFkCordCbuStatus(getCbuStatusPkByCode(AV)); 
				   
			   }*/
			   //if(action.getCdrCbuPojo()!=null && action.getCdrCbuPojo().getFkCordCbuStatus()!=null && action.getCdrCbuPojo().getFkCordCbuStatus().equals(getCbuStatusPkByCode(AV))){
			      getCdrCbuPojo().setCordSearchable(1l);
			      setGuid();
			      int matCount = cbuController.getCordByCountByMaternalRegistry(getCdrCbuPojo().getRegistryMaternalId());
			      getCdrCbuPojo().setPregRefNumber(Long.parseLong(Integer.toString(matCount)));
			      cbuController.saveOrUpdateCordInfo(getCdrCbuPojo());
			  // }			   
			   successFlag = true;
    	   }catch (Exception e) {
			e.printStackTrace();
			successFlag = false;
		   }
			setSuccessFlag(successFlag);		
		   return "success";
	   }
	
	public void setGuid(){
		   try{
			   CBUController cbuController = new CBUController();
			   if(StringUtils.isEmpty(getCdrCbuPojo().getGuid())){
				    String guid = cbuController.generateGuid();
					if(guid!=null){
					  getCdrCbuPojo().setGuid(guid);
					}
			    }
			   if(StringUtils.isEmpty(getCdrCbuPojo().getMatGuid())){
					String matGuid = null;
					String criteria1 = "and registryMaternalId='"+cdrCbuPojo.getRegistryMaternalId()+"' and cordSearchable=1 ";
					List<Object> regData = cbuController.getCBUCordInfoList(CDRCBU_OBJECTNAME, criteria1,null);
					if(regData!=null && regData.size()>0){
				       CdrCbu cbuPojo = (CdrCbu)regData.get(0);
				       if(cbuPojo!=null && !StringUtils.isEmpty(cbuPojo.getMatGuid())){
				    	   matGuid = cbuPojo.getMatGuid();
				       }
					}		
					if(matGuid==null){
						  matGuid = cbuController.generateGuid();
						  getCdrCbuPojo().setMatGuid(matGuid);
					}else{
						getCdrCbuPojo().setMatGuid(matGuid);
					}
			   }
		   }catch (Exception e) {
			e.printStackTrace();
		   }
	   }

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
