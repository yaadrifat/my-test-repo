/**
 * 
 */
package com.velos.ordercomponent.form;

import java.io.PrintWriter;
import java.io.StringWriter;

//import com.velos.p1v.exception.P1VBaseException;

/**
 * @author akajeera
 *
 */
public class ErrorForm {
	//private P1VBaseException exception;
	private String errorcode;
	private String message;
	private String exceptionEnable;
	
	
	/**
	 * @return Returns the errorcode.
	 */
	public String getErrorcode() {
		return errorcode;
	}
	/**
	 * @param errorcode The errorcode to set.
	 */
	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}
	/**
	 * @return Returns the exception.
	 */
//	public P1VBaseException getException() {
//		return exception;
//	}
//	/**
//	 * @param exception The exception to set.
//	 */
//	public void setException(P1VBaseException exception) {
//		this.exception = exception;
//	}
	/**
	 * @return Returns the message.
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message The message to set.
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
	/**
	 * Method used for getting ErrorStack
	 * @return String
	 */
	public String getErrorStack(){
		
		String retVal="No Stack";
		StringWriter sw=new StringWriter();
		PrintWriter pw=new PrintWriter(sw);
		
//		if(exception!=null && getException().getCause()!=null && getException().getExceptionData().getCause() !=null){
//			getException().getExceptionData().getCause().printStackTrace(pw);
//			pw.flush();
//			retVal=sw.toString();
//		}else if(getException().getStackTrace()!=null){
//			getException().printStackTrace(pw);
//			pw.flush();
//			retVal=sw.toString();
//		}
		
		return retVal;
		
	}
	public String getExceptionEnable() {
		return exceptionEnable;
	}
	public void setExceptionEnable(String exceptionEnable) {
		this.exceptionEnable = exceptionEnable;
	}
}
