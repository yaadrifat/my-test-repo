package com.velos.ordercomponent.validator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.CompoundRoot;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;

/**
 * @author Mohiuddin Ali Ahmed
 * @author Anurag Upadhyay
 * @version 1.0
 * 
 */

public class ISBTRequireValidator extends FieldValidatorSupport{
	private String cdrUserField="";
	
	public String getCdrUserField() {
		return cdrUserField;
	}
	public void setCdrUserField(String cdrUserField) {
		this.cdrUserField = cdrUserField;
	}
	@Override
	public void validate(Object object) throws ValidationException {
		 String fieldName = getFieldName();
	     String value =(String) getFieldValue(fieldName, object);
	     
	     CompoundRoot cRoot= ActionContext.getContext().getValueStack().getRoot();
		 boolean isCDRUser=(Boolean) getFieldValue(cdrUserField, cRoot);     
		 CdrCbuPojo cdrCbuPojo=(CdrCbuPojo) object;
       if (isCDRUser && ("".equals(cdrCbuPojo.getLocalCbuId()) && (value==null ||value.equals("")))){
	       addFieldError(fieldName, object);
	       return;
       }
		
	}

}
