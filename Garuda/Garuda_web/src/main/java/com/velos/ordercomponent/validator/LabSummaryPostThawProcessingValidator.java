package com.velos.ordercomponent.validator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;
import com.velos.ordercomponent.business.domain.CodeList;
import com.velos.ordercomponent.business.pojoobjects.PatLabsPojo;

/**
 * @author Tarun Sondhi
 * @author Anurag Upadhyay
 * @version 1.0
 * 
 */

public class LabSummaryPostThawProcessingValidator extends FieldValidatorSupport{
	private boolean searchableCheck;	
	
	public boolean isSearchableCheck() {
		return searchableCheck;
	}

	public void setSearchableCheck(boolean searchableCheck) {
		this.searchableCheck = searchableCheck;
	}
	@SuppressWarnings({ "unchecked"})
	@Override
	public void validate(Object object) throws ValidationException {
		// TODO Auto-generated method stub
		ServletContext context = ServletActionContext.getServletContext();
		Map<Long,CodeList> codeListMapByIds = null;	
		codeListMapByIds = (Map<Long,CodeList>)context.getAttribute("codeListMapByIds");
		String fieldName=getFieldName();
		 int i=0;
		 List<PatLabsPojo> savePostThawTestList=(List<PatLabsPojo>) getFieldValue(fieldName, object);
		 long cordSearchable = (Long) getFieldValue("cordSearchable", object);
		 HashMap<Long, String> processingTestMap=(HashMap<Long, String>) getFieldValue("processingTestMap", object);
			if(savePostThawTestList!=null){ 
		 	for(PatLabsPojo plp: savePostThawTestList){
		 		if(plp!=null){
		     String testresult=plp.getTestresult();
		     Long testSpecimanType=plp.getFktestspecimen();
		     String testReason=plp.getFktestreason();
		     String testReasonDesc= plp.getReasonTestDesc();
		     Long testMethod=plp.getFktestmethod();
		     String testMethodDesc=plp.getTestMthdDesc();
		     switch(LabTests.valueOf(processingTestMap.get(plp.getFktestid())==null?"DEF":processingTestMap.get(plp.getFktestid()))){
		    	case CBU_VOL:
		    		/*
		    		if(searchableCheck  && cordSearchable==1 && (testresult==null || testresult.equals(""))){
		    			 setMessageKey("garuda.cbu.cordentry.inputId");
		    			addFieldError(fieldName+"["+i+"].testresult", object);
		    		}else if(testresult!=null && (Float.parseFloat(testresult)<40 || Float.parseFloat(testresult)>500)){
		    			 setMessageKey("garuda.common.range.testval40to500");
		    			 addFieldError(fieldName+"["+i+"].testresult", object);
		    		}*/
		    		break;
		    	case TCNCC:	  
		    	case UNCRCT:
		    	case TCBUUN:
		    		 if((testresult!=null && !(testresult.equals(""))) && (Float.parseFloat(testresult)<12 || Float.parseFloat(testresult)>999)){
		    			 setMessageKey("garuda.common.range.testval12to999");
		    			 addFieldError(fieldName+"["+i+"].testresult", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null && !(testresult.equals(""))) && (testReason==null || testReason.equals("-1"))){
		    			setMessageKey("garuda.cbu.cordentry.inputId");
		    			addFieldError(fieldName+"["+i+"].fktestreason", object);
		    		}else if(searchableCheck  && cordSearchable==1 && (testReason.equals(getCodeListPkByTypeAndSubtype(com.velos.ordercomponent.util.VelosGarudaConstants.TEST_REASON,com.velos.ordercomponent.util.VelosGarudaConstants.TEST_REASON_OTHER,codeListMapByIds).toString()))&& (testReasonDesc==null || testReasonDesc.equals(""))){
		    			 	setMessageKey("garuda.cbu.cordentry.inputId");
			    			addFieldError(fieldName+"["+i+"].reasonTestDesc", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null &&  !(testresult.equals(""))) && (testSpecimanType==null || testSpecimanType==-1)){
		    			 setMessageKey("garuda.cbu.cordentry.inputId");
		    			 addFieldError(fieldName+"["+i+"].fktestspecimen", object);
		    		}
		    		break;
		    	case TCDAD:
		    		if((testresult!=null &&  !(testresult.equals(""))) && (Float.parseFloat(testresult)<0.0 || Float.parseFloat(testresult)>999.9)){
		    			 setMessageKey("garuda.common.range.val0to999");
		    			 addFieldError(fieldName+"["+i+"].testresult", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null &&  !(testresult.equals(""))) && (testReason==null || testReason.equals("-1"))){
		    			setMessageKey("garuda.cbu.cordentry.inputId");
		    			addFieldError(fieldName+"["+i+"].fktestreason", object);
		    		}else if(searchableCheck  && cordSearchable==1 && (testReason.equals(getCodeListPkByTypeAndSubtype(com.velos.ordercomponent.util.VelosGarudaConstants.TEST_REASON,com.velos.ordercomponent.util.VelosGarudaConstants.TEST_REASON_OTHER,codeListMapByIds).toString()))&& (testReasonDesc==null || testReasonDesc.equals(""))){
		    			 	setMessageKey("garuda.cbu.cordentry.inputId");
			    			addFieldError(fieldName+"["+i+"].reasonTestDesc", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null &&  !(testresult.equals(""))) && (testSpecimanType==null || testSpecimanType==-1)){
		    			 setMessageKey("garuda.cbu.cordentry.inputId");
		    			 addFieldError(fieldName+"["+i+"].fktestspecimen", object);
		    		}
		    		break;
		    	case PERNRBC:
		    		if((testresult!=null &&  !(testresult.equals(""))) && (Float.parseFloat(testresult)<0 || Float.parseFloat(testresult)>100)){
		    			 setMessageKey("garuda.common.range.val0to100");
		    			 addFieldError(fieldName+"["+i+"].testresult", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null &&  !(testresult.equals(""))) && (testReason==null || testReason.equals("-1"))){
		    			setMessageKey("garuda.cbu.cordentry.inputId");
		    			addFieldError(fieldName+"["+i+"].fktestreason", object);
		    		}else if(searchableCheck  && cordSearchable==1 && (testReason.equals(getCodeListPkByTypeAndSubtype(com.velos.ordercomponent.util.VelosGarudaConstants.TEST_REASON,com.velos.ordercomponent.util.VelosGarudaConstants.TEST_REASON_OTHER,codeListMapByIds).toString()))&& (testReasonDesc==null || testReasonDesc.equals(""))){
		    			 	setMessageKey("garuda.cbu.cordentry.inputId");
			    			addFieldError(fieldName+"["+i+"].reasonTestDesc", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null &&  !(testresult.equals(""))) && (testSpecimanType==null || testSpecimanType==-1)){
		    			 setMessageKey("garuda.cbu.cordentry.inputId");
		    			 addFieldError(fieldName+"["+i+"].fktestspecimen", object);
		    		}
		    		break;
		    	case PERTNUC:
		    		if((testresult!=null &&  !(testresult.equals(""))) && (Float.parseFloat(testresult)<20.0 || Float.parseFloat(testresult)>99.9)){
		    			 setMessageKey("garuda.common.range.val20to99");
		    			 addFieldError(fieldName+"["+i+"].testresult", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null && !(testresult.equals(""))) && (testReason==null || testReason.equals("-1"))){
		    			setMessageKey("garuda.cbu.cordentry.inputId");
		    			addFieldError(fieldName+"["+i+"].fktestreason", object);
		    		}else if(searchableCheck  && cordSearchable==1 && (testReason.equals(getCodeListPkByTypeAndSubtype(com.velos.ordercomponent.util.VelosGarudaConstants.TEST_REASON,com.velos.ordercomponent.util.VelosGarudaConstants.TEST_REASON_OTHER,codeListMapByIds).toString()))&& (testReasonDesc==null || testReasonDesc.equals(""))){
		    			 	setMessageKey("garuda.cbu.cordentry.inputId");
			    			addFieldError(fieldName+"["+i+"].reasonTestDesc", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null && !(testresult.equals(""))) && (testSpecimanType==null || testSpecimanType==-1)){
		    			 setMessageKey("garuda.cbu.cordentry.inputId");
		    			 addFieldError(fieldName+"["+i+"].fktestspecimen", object);
		    		}
		    		break;
		    	case PERTMON:
		    		if((testresult!=null && !(testresult.equals(""))) && (Float.parseFloat(testresult)<0.0 || Float.parseFloat(testresult)>10.0)){
		    			 setMessageKey("garuda.common.range.testrange0to10");
		    			 addFieldError(fieldName+"["+i+"].testresult", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null && !(testresult.equals(""))) && (testReason==null || testReason.equals("-1"))){
		    			setMessageKey("garuda.cbu.cordentry.inputId");
		    			addFieldError(fieldName+"["+i+"].fktestreason", object);
		    		}else if(searchableCheck  && cordSearchable==1 && (testReason.equals(getCodeListPkByTypeAndSubtype(com.velos.ordercomponent.util.VelosGarudaConstants.TEST_REASON,com.velos.ordercomponent.util.VelosGarudaConstants.TEST_REASON_OTHER,codeListMapByIds).toString()))&& (testReasonDesc==null || testReasonDesc.equals(""))){
		    			 	setMessageKey("garuda.cbu.cordentry.inputId");
			    			addFieldError(fieldName+"["+i+"].reasonTestDesc", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null && !(testresult.equals(""))) && (testSpecimanType==null || testSpecimanType==-1)){
		    			 setMessageKey("garuda.cbu.cordentry.inputId");
		    			 addFieldError(fieldName+"["+i+"].fktestspecimen", object);
		    		}
		    		break;
		    	case CNRBC:
		    		if((testresult!=null && !(testresult.equals(""))) && (Float.parseFloat(testresult)<0 || Float.parseFloat(testresult)>9999)){
		    			 setMessageKey("garuda.common.range.val0to9999");
		    			 addFieldError(fieldName+"["+i+"].testresult", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null && !(testresult.equals(""))) && (testReason==null || testReason.equals("-1"))){
		    			setMessageKey("garuda.cbu.cordentry.inputId");
		    			addFieldError(fieldName+"["+i+"].fktestreason", object);
		    		}else if(searchableCheck  && cordSearchable==1 && (testReason.equals(getCodeListPkByTypeAndSubtype(com.velos.ordercomponent.util.VelosGarudaConstants.TEST_REASON,com.velos.ordercomponent.util.VelosGarudaConstants.TEST_REASON_OTHER,codeListMapByIds).toString()))&& (testReasonDesc==null || testReasonDesc.equals(""))){
		    			 	setMessageKey("garuda.cbu.cordentry.inputId");
			    			addFieldError(fieldName+"["+i+"].reasonTestDesc", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null && !(testresult.equals(""))) && (testSpecimanType==null || testSpecimanType==-1)){
		    			 setMessageKey("garuda.cbu.cordentry.inputId");
		    			 addFieldError(fieldName+"["+i+"].fktestspecimen", object);
		    		}
		    		break;
		    	case PERCD3:
		    		if((testresult!=null && !(testresult.equals(""))) && (Float.parseFloat(testresult)<0 || Float.parseFloat(testresult)>99.9)){
		    			 setMessageKey("garuda.common.range.val0to99");
		    			 addFieldError(fieldName+"["+i+"].testresult", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null && !(testresult.equals(""))) && (testReason==null || testReason.equals("-1"))){
		    			setMessageKey("garuda.cbu.cordentry.inputId");
		    			addFieldError(fieldName+"["+i+"].fktestreason", object);
		    		}else if(searchableCheck  && cordSearchable==1 && (testReason.equals(getCodeListPkByTypeAndSubtype(com.velos.ordercomponent.util.VelosGarudaConstants.TEST_REASON,com.velos.ordercomponent.util.VelosGarudaConstants.TEST_REASON_OTHER,codeListMapByIds).toString()))&& (testReasonDesc==null || testReasonDesc.equals(""))){
		    			 	setMessageKey("garuda.cbu.cordentry.inputId");
			    			addFieldError(fieldName+"["+i+"].reasonTestDesc", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null && !(testresult.equals(""))) && (testSpecimanType==null || testSpecimanType==-1)){
		    			 setMessageKey("garuda.cbu.cordentry.inputId");
		    			 addFieldError(fieldName+"["+i+"].fktestspecimen", object);
		    		}
		    		break;
		    	case CFUCNT:
		    		if((testresult!=null && !(testresult.equals(""))) && (Float.parseFloat(testresult)<0 || Float.parseFloat(testresult)>99.9)){
		    			 setMessageKey("garuda.common.range.val0to99");
		    			 addFieldError(fieldName+"["+i+"].testresult", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null && !(testresult.equals(""))) && (testReason==null || testReason.equals("-1") )){
		    			setMessageKey("garuda.cbu.cordentry.inputId");
		    			addFieldError(fieldName+"["+i+"].fktestreason", object);
		    		}else if(searchableCheck  && cordSearchable==1 && (testReason.equals(getCodeListPkByTypeAndSubtype(com.velos.ordercomponent.util.VelosGarudaConstants.TEST_REASON,com.velos.ordercomponent.util.VelosGarudaConstants.TEST_REASON_OTHER,codeListMapByIds).toString()))&& (testReasonDesc==null || testReasonDesc.equals(""))){
		    			 	setMessageKey("garuda.cbu.cordentry.inputId");
			    			addFieldError(fieldName+"["+i+"].reasonTestDesc", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null && !(testresult.equals(""))) && (testSpecimanType==null || testSpecimanType==-1)){
		    			 setMessageKey("garuda.cbu.cordentry.inputId");
		    			 addFieldError(fieldName+"["+i+"].fktestspecimen", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null && !(testresult.equals(""))) && ( testMethod==null || testMethod==-1)){
		    			 setMessageKey("garuda.cbu.cordentry.inputId");
			    			addFieldError(fieldName+"["+i+"].fktestmethod", object);
		    		}else if(searchableCheck  && cordSearchable==1 && testMethod==getCodeListPkByTypeAndSubtype(com.velos.ordercomponent.util.VelosGarudaConstants.CFU_COUNT,com.velos.ordercomponent.util.VelosGarudaConstants.CFU_OTHER,codeListMapByIds).longValue() && (testMethodDesc==null || testMethodDesc.equals(""))){
		    			setMessageKey("garuda.cbu.cordentry.inputId");
		    			addFieldError(fieldName+"["+i+"].testMthdDesc", object);
		    		}
		    		break;
		    	case CBUNCCC:
		    		if((testresult!=null && !(testresult.equals(""))) && (Float.parseFloat(testresult)<5000 || Float.parseFloat(testresult)>200000)){
		    			 setMessageKey("garuda.common.range.testval5000to200000");
		    			 addFieldError(fieldName+"["+i+"].testresult", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null && !(testresult.equals(""))) && (testReason==null || testReason.equals("-1"))){
		    			setMessageKey("garuda.cbu.cordentry.inputId");
		    			addFieldError(fieldName+"["+i+"].fktestreason", object);
		    		}else if(searchableCheck  && cordSearchable==1 && (testReason.equals(getCodeListPkByTypeAndSubtype(com.velos.ordercomponent.util.VelosGarudaConstants.TEST_REASON,com.velos.ordercomponent.util.VelosGarudaConstants.TEST_REASON_OTHER,codeListMapByIds).toString()))&& (testReasonDesc==null || testReasonDesc.equals(""))){
		    			 	setMessageKey("garuda.cbu.cordentry.inputId");
			    			addFieldError(fieldName+"["+i+"].reasonTestDesc", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null && !(testresult.equals(""))) && (testSpecimanType==null || testSpecimanType==-1)){
		    			 setMessageKey("garuda.cbu.cordentry.inputId");
		    			 addFieldError(fieldName+"["+i+"].fktestspecimen", object);
		    		}
		    		break;
		    	case TOTCD:
		    		if((testresult!=null && !(testresult.equals(""))) && (Float.parseFloat(testresult)<0 || Float.parseFloat(testresult)>999)){
		    			 setMessageKey("garuda.common.range.val0to999");
		    			 addFieldError(fieldName+"["+i+"].testresult", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null && !(testresult.equals(""))) && (testReason==null || testReason.equals("-1"))){
		    			setMessageKey("garuda.cbu.cordentry.inputId");
		    			addFieldError(fieldName+"["+i+"].fktestreason", object);
		    		}else if(searchableCheck  && cordSearchable==1 && (testReason.equals(getCodeListPkByTypeAndSubtype(com.velos.ordercomponent.util.VelosGarudaConstants.TEST_REASON,com.velos.ordercomponent.util.VelosGarudaConstants.TEST_REASON_OTHER,codeListMapByIds).toString()))&& (testReasonDesc==null || testReasonDesc.equals(""))){
		    			 	setMessageKey("garuda.cbu.cordentry.inputId");
			    			addFieldError(fieldName+"["+i+"].reasonTestDesc", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null && !(testresult.equals(""))) && (testSpecimanType==null || testSpecimanType==-1)){
		    			 setMessageKey("garuda.cbu.cordentry.inputId");
		    			 addFieldError(fieldName+"["+i+"].fktestspecimen", object);
		    		}
		    		break;
		    	case FNPV:
		    		if((testresult!=null && !(testresult.equals(""))) && (Float.parseFloat(testresult)<10 || Float.parseFloat(testresult)>600)){
		    			 setMessageKey("garuda.common.range.testval10to600");
		    			 addFieldError(fieldName+"["+i+"].testresult", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null && !(testresult.equals(""))) && (testReason==null || testReason.equals("-1"))){
		    			setMessageKey("garuda.cbu.cordentry.inputId");
		    			addFieldError(fieldName+"["+i+"].fktestreason", object);
		    		}else if(searchableCheck  && cordSearchable==1 && (testReason.equals(getCodeListPkByTypeAndSubtype(com.velos.ordercomponent.util.VelosGarudaConstants.TEST_REASON,com.velos.ordercomponent.util.VelosGarudaConstants.TEST_REASON_OTHER,codeListMapByIds).toString()))&& (testReasonDesc==null || testReasonDesc.equals(""))){
		    			 	setMessageKey("garuda.cbu.cordentry.inputId");
			    			addFieldError(fieldName+"["+i+"].reasonTestDesc", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null && !(testresult.equals(""))) && (testSpecimanType==null || testSpecimanType==-1)){
		    			 setMessageKey("garuda.cbu.cordentry.inputId");
		    			 addFieldError(fieldName+"["+i+"].fktestspecimen", object);
		    		}
		    		break;
		    	case VIAB:
		    		if((testresult!=null && !(testresult.equals(""))) && (Float.parseFloat(testresult)<0 || Float.parseFloat(testresult)>99.9)){
		    			 setMessageKey("garuda.common.range.val0to99");
		    			 addFieldError(fieldName+"["+i+"].testresult", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null && !(testresult.equals(""))) && (testReason==null || testReason.equals("-1"))){
		    			setMessageKey("garuda.cbu.cordentry.inputId");
		    			addFieldError(fieldName+"["+i+"].fktestreason", object);
		    		}else if(searchableCheck  && cordSearchable==1 && (testReason.equals(getCodeListPkByTypeAndSubtype(com.velos.ordercomponent.util.VelosGarudaConstants.TEST_REASON,com.velos.ordercomponent.util.VelosGarudaConstants.TEST_REASON_OTHER,codeListMapByIds).toString()))&& (testReasonDesc==null || testReasonDesc.equals(""))){
		    			 	setMessageKey("garuda.cbu.cordentry.inputId");
			    			addFieldError(fieldName+"["+i+"].reasonTestDesc", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null && !(testresult.equals(""))) && (testSpecimanType==null || testSpecimanType==-1)){
		    			 setMessageKey("garuda.cbu.cordentry.inputId");
		    			 addFieldError(fieldName+"["+i+"].fktestspecimen", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null && !(testresult.equals(""))) && (testMethod==null || testMethod==-1)){
		    			 setMessageKey("garuda.cbu.cordentry.inputId");
			    			addFieldError(fieldName+"["+i+"].fktestmethod", object);
		    		}else if(searchableCheck  && cordSearchable==1 && testMethod==getCodeListPkByTypeAndSubtype(com.velos.ordercomponent.util.VelosGarudaConstants.VAIBILITY,com.velos.ordercomponent.util.VelosGarudaConstants.VAIBILITY_OTHER,codeListMapByIds).longValue() && (testMethodDesc==null || testMethodDesc.equals(""))){
		    			setMessageKey("garuda.cbu.cordentry.inputId");
		    			addFieldError(fieldName+"["+i+"].testMthdDesc", object);
		    		}
		    		break;
		    	case PERCD:
		    		if((testresult!=null && !(testresult.equals(""))) && (Float.parseFloat(testresult)<0 || Float.parseFloat(testresult)>100)){
		    			 setMessageKey("garuda.common.range.val0to100");
		    			 addFieldError(fieldName+"["+i+"].testresult", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null && !(testresult.equals(""))) && (testReason==null || testReason.equals("-1"))){
		    			setMessageKey("garuda.cbu.cordentry.inputId");
		    			addFieldError(fieldName+"["+i+"].fktestreason", object);
		    		}else if(searchableCheck  && cordSearchable==1 && (testReason.equals(getCodeListPkByTypeAndSubtype(com.velos.ordercomponent.util.VelosGarudaConstants.TEST_REASON,com.velos.ordercomponent.util.VelosGarudaConstants.TEST_REASON_OTHER,codeListMapByIds).toString()))&& (testReasonDesc==null || testReasonDesc.equals(""))){
		    			 	setMessageKey("garuda.cbu.cordentry.inputId");
			    			addFieldError(fieldName+"["+i+"].reasonTestDesc", object);
		    		}
		    		if(searchableCheck  && cordSearchable==1 && (testresult!=null && !(testresult.equals(""))) && (testSpecimanType==null || testSpecimanType==-1)){
		    			 setMessageKey("garuda.cbu.cordentry.inputId");
		    			 addFieldError(fieldName+"["+i+"].fktestspecimen", object);
		    		}
		    		break;
		    	default:
		    		if(searchableCheck  && cordSearchable==1 && (testresult==null || "".equals(testresult))){
		    			 setMessageKey("garuda.cbu.cordentry.inputId");
		    			 addFieldError(fieldName+"["+i+"].testresult", object);
		    		}
	    			if(searchableCheck  && cordSearchable==1 && (testSpecimanType==null || testSpecimanType==-1)){
		    			 setMessageKey("garuda.cbu.cordentry.inputId");
		    			 addFieldError(fieldName+"["+i+"].fktestspecimen", object);
		    		}
		     }
		     i++;
		}
	   }
	 }
	}
	public Long getCodeListPkByTypeAndSubtype(String type, String subType,Map<Long,CodeList> codeListMapByIds){
		  Long pk = null;
		     for(Map.Entry<Long, CodeList> me : codeListMapByIds.entrySet()){
		    	 if((type!=null && (type.equals(me.getValue().getType()) || type==me.getValue().getType())) 
		    			 && (subType!=null && (subType.equals(me.getValue().getSubType()) || subType==me.getValue().getSubType()))){
		    		 pk = me.getKey();
		    	 }
		     }
		  return pk;
		}
	 private static enum LabTests
	  {
		 UNCRCT, TCDAD, PERNRBC, PERTNUC, CBU_VOL, PERTMON, CNRBC, PERCD3, CFUCNT, CBUNCCC, TOTCD, FNPV, TCNCC, TCBUUN, VIAB, PERCD, DEF;
	  }

}
