package com.velos.ordercomponent.action;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import com.opensymphony.xwork2.ActionSupport;
import com.velos.ordercomponent.business.domain.CodeList;
import com.velos.ordercomponent.business.pojoobjects.AdditionalIdsPojo;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.ClinicalNotePojo;
import com.velos.ordercomponent.business.pojoobjects.EligibilityNewDeclPojo;
import com.velos.ordercomponent.business.pojoobjects.EntitySamplesPojo;
import com.velos.ordercomponent.business.pojoobjects.LabTestPojo;
import com.velos.ordercomponent.business.pojoobjects.PatLabsPojo;
import com.velos.ordercomponent.controller.CBUController;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.ValidationParameter;
import com.opensymphony.xwork2.validator.annotations.VisitorFieldValidator;

import com.opensymphony.xwork2.validator.annotations.*;

/**
 * @author Mohiuddin Ali Ahmed
 * @author Anurag Upadhyay
 * @version 1.0
 * 
 */

/*@Validations(expressions = {
		
  @ExpressionValidator(expression = "hlaPercentage.equals(100) == 100", message = "HLA Required .......")

})*/
@SuppressWarnings("unchecked")
public class CordEntryValidatorAction extends ActionSupport{
	
	private static final long serialVersionUID = 25275896460619500L;

	private static final Log log = LogFactory.getLog(CordEntryValidatorAction.class);	
	
	 public static Map<Long,CodeList> codeListMapByIds = null;	
	 
	 private CdrCbuPojo cdrCbuPojo;
	 private Boolean isCDRUser;
     private String defaultRegId;
     private String defaultMatId;
     private String defaultLocCbuId;
     private String defaultMatLocId;
     private String defaultISBTCode;
     
     private Long cbuOnBag;
     private List<AdditionalIdsPojo> additionalIds;
     private Long cbuLicStatus;
     private Date birthDateStr;
     private Date cbuCollectionDateStr;
     private Long[] race;
     private Long cordSearchable;
     private EntitySamplesPojo entitySamplesPojo;
     private Date prcsngStartDateStr;
     private Date frzDateStr;
     private List<PatLabsPojo> savePreTestList;
     private HashMap<Long, String> processingTestMap;
     private List<PatLabsPojo> savePostTestList;
     private List<PatLabsPojo> saveOtherPostTestList1;
     private List<PatLabsPojo> saveOtherPostTestList2;
     private List<PatLabsPojo> saveOtherPostTestList3;
     private List<PatLabsPojo> saveOtherPostTestList4;
     private List<PatLabsPojo> saveOtherPostTestList5;
     private List<PatLabsPojo> saveOtherPostTestList6;
     private Long fkCordCbuEligible;
     private Long unLicensedPk;
     private Byte idmFormVersion;
     private Byte fmhqFormVersion;
     private Byte mrqFormVersion;
     private Short hlaPercentage;
     private EligibilityNewDeclPojo declNewPojo;
     private Long[] reasons;
     private String cordAdditionalInfo;
     private Date procStartDate;
     private Date procTermiDate;
     
     private Date fungCultDateStr;
 	 private Date bactCultDateStr;
     private Date preTestDateStrn;
 	 private Date postTestDateStrn;
	 private Date otherTestDateStrn1=new Date(1970);
	 private Date otherTestDateStrn2=new Date(1970);
	 private Date otherTestDateStrn3=new Date(1970);
	 private Date otherTestDateStrn4=new Date(1970);
	 private Date otherTestDateStrn5=new Date(1970);
	 private Date otherTestDateStrn6=new Date(1970);
     private Date todayDate;
     private ClinicalNotePojo clinicalNotePojo;
     private List<ClinicalNotePojo> multipleClinicalNotes;
     private Boolean cordAutoDeferStatus=false;
     private Long cfuCountPk;
     private Long viabilityPk;
     public Boolean isCordSubmitted=false;
     
	@VisitorFieldValidator(message="",context="action alias")
	public CdrCbuPojo getCdrCbuPojo() {
		return cdrCbuPojo;
	}

	public void setCdrCbuPojo(CdrCbuPojo cdrCbuPojo) {
		this.cdrCbuPojo = cdrCbuPojo;
	}

	public Boolean getIsCDRUser() {
		return isCDRUser;
	}

	public void setIsCDRUser(Boolean isCDRUser) {
		this.isCDRUser = isCDRUser;
	}

	public String getDefaultRegId() {
		return defaultRegId;
	}

	public void setDefaultRegId(String defaultRegId) {
		this.defaultRegId = defaultRegId;
	}

	public String getDefaultMatId() {
		return defaultMatId;
	}

	public void setDefaultMatId(String defaultMatId) {
		this.defaultMatId = defaultMatId;
	}

	public String getDefaultLocCbuId() {
		return defaultLocCbuId;
	}

	public void setDefaultLocCbuId(String defaultLocCbuId) {
		this.defaultLocCbuId = defaultLocCbuId;
	}

	public String getDefaultMatLocId() {
		return defaultMatLocId;
	}

	public void setDefaultMatLocId(String defaultMatLocId) {
		this.defaultMatLocId = defaultMatLocId;
	}

	public String getDefaultISBTCode() {
		return defaultISBTCode;
	}

	public void setDefaultISBTCode(String defaultISBTCode) {
		this.defaultISBTCode = defaultISBTCode;
	}

	//@RequiredFieldValidator(key = "garuda.common.validation.value", shortCircuit = true)
	@CustomValidator(
			  type ="longfieldrequire",
			  fieldName = "cbuOnBag",				 
			  key="garuda.common.validation.value",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )})
	public Long getCbuOnBag() {
		return cbuOnBag;
	}

	public void setCbuOnBag(Long cbuOnBag) {
		this.cbuOnBag = cbuOnBag;
	}
	@VisitorFieldValidator(message="",context="action alias")
	public List<AdditionalIdsPojo> getAdditionalIds() {
		return additionalIds;
	}

	public void setAdditionalIds(List<AdditionalIdsPojo> additionalIds) {
		this.additionalIds = additionalIds;
	}
	 @CustomValidator(
			  type ="longfieldrequire",
			  fieldName = "cbuLicStatus",				 
			  key="garuda.cbu.license.licensurestatus",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )})
	public Long getCbuLicStatus() {
		return cbuLicStatus;
	}

	public void setCbuLicStatus(Long cbuLicStatus) {
		this.cbuLicStatus = cbuLicStatus;
	}
	@Validations(customValidators={
	@CustomValidator(
			  type ="requirefield",
			  fieldName = "birthDateStr",				 
			  key="garuda.cbu.cordentry.birthdate",
			  shortCircuit=true
			  /*parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}*/),
			  @CustomValidator(
					  type ="maxdaterangefield",
					  fieldName = "birthDateStr",				 
					  key="garuda.common.message.validateDateMax",
					  shortCircuit=true,
					  parameters = { @ValidationParameter( name = "invokingFieldTime", value = "cdrCbuPojo.babyBirthTime" ),
							         @ValidationParameter( name = "compareFieldName", value = "cbuCollectionDateStr" ),
							         @ValidationParameter( name = "compareFieldTime", value = "cdrCbuPojo.cbuCollectionTime" )
					               }
					  )
	})
	public Date getBirthDateStr() {
		return birthDateStr;
	}

	public void setBirthDateStr(Date birthDateStr) {
		this.birthDateStr = birthDateStr;
	}
	@Validations(customValidators={
			@CustomValidator(
					  type ="requirefield",
					  fieldName = "cbuCollectionDateStr",				 
					  key="garuda.cbu.cordentry.collectiondate",
					  shortCircuit=true
					  /*parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}*/),
					  
			@CustomValidator(
							  type ="datetimerangefieldvalidator",
							  fieldName = "cbuCollectionDateStr",				 
							  key="garuda.common.message.validatedateminmax",
							  shortCircuit=true,
							  parameters = { @ValidationParameter( name = "timeRange", value = "48"),
									         @ValidationParameter( name = "invokingFieldTime", value = "cdrCbuPojo.cbuCollectionTime"),
									         @ValidationParameter( name = "compareFieldName", value = "birthDateStr"),
									         @ValidationParameter( name = "compareFieldTime", value = "cdrCbuPojo.babyBirthTime")
							               }
							  ),
			@CustomValidator(
									  type ="daterangefieldvalidator",
									  fieldName = "cbuCollectionDateStr",				 
									  key="garuda.cbu.cordentry.prcssngdtwrtproc",
									  shortCircuit=true,
									  parameters = { @ValidationParameter( name = "startDateFieldName", value = "procStartDate"),
											         @ValidationParameter( name = "terminDateFieldName", value = "procTermiDate")
									               }
									  )
			}) 
	public Date getCbuCollectionDateStr() {
		return cbuCollectionDateStr;
	}

	public void setCbuCollectionDateStr(Date cbuCollectionDateStr) {
		this.cbuCollectionDateStr = cbuCollectionDateStr;
	}
	@CustomValidator(
			  type ="longarrayfieldrequire",
			  fieldName = "race",				 
			  key="garuda.cbu.cordentry.race",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )})
	public Long[] getRace() {
		return race;
	}

	public void setRace(Long[] race) {
		this.race = race;
	}

	public Long getCordSearchable() {
		return cordSearchable;
	}

	public void setCordSearchable(Long cordSearchable) {
		this.cordSearchable = cordSearchable;
	}
	@VisitorFieldValidator(message="",context="action alias")
	public EntitySamplesPojo getEntitySamplesPojo() {
		return entitySamplesPojo;
	}

	public void setEntitySamplesPojo(EntitySamplesPojo entitySamplesPojo) {
		this.entitySamplesPojo = entitySamplesPojo;
	}
	@Validations(customValidators={
			@CustomValidator(
					  type ="requirefield",
					  fieldName = "prcsngStartDateStr",				 
					  key="garuda.cbu.cordentry.procstartdate",
					  shortCircuit=true
					  /*parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}*/),
					  @CustomValidator(
							  type ="maxdaterangefield",
							  fieldName = "prcsngStartDateStr",				 
							  key="garuda.cbu.cordentry.procbeforefreezdate",
							  shortCircuit=true,
							  parameters = { @ValidationParameter( name = "invokingFieldTime", value = "cdrCbuPojo.processingTime"),
									         @ValidationParameter( name = "compareFieldName", value = "frzDateStr"),
									         @ValidationParameter( name = "compareFieldTime", value = "cdrCbuPojo.freezeTime")
							               }
							  ),
			@CustomValidator(
							  type ="datetimerangefieldvalidator",
							  fieldName = "prcsngStartDateStr",				 
							  key="garuda.common.message.validatedateminmax",
							  shortCircuit=true,
							  parameters = { @ValidationParameter( name = "timeRange", value = "48"),
									  @ValidationParameter( name = "invokingFieldTime", value = "cdrCbuPojo.processingTime"),
									         @ValidationParameter( name = "compareFieldName", value = "cbuCollectionDateStr"),
									         @ValidationParameter( name = "compareFieldTime", value = "cdrCbuPojo.cbuCollectionTime")
							               }
							  ),
			@CustomValidator(
							  type ="daterangefieldvalidator",
							  fieldName = "prcsngStartDateStr",				 
							  key="garuda.cbu.cordentry.prcssngdtwrtproc",
							  shortCircuit=true,
							  parameters = { @ValidationParameter( name = "startDateFieldName", value = "procStartDate"),
									         @ValidationParameter( name = "terminDateFieldName", value = "procTermiDate")
							               }
							  )
			})
	public Date getPrcsngStartDateStr() {
		return prcsngStartDateStr;
	}

	public void setPrcsngStartDateStr(Date prcsngStartDateStr) {
		this.prcsngStartDateStr = prcsngStartDateStr;
	}
	@Validations(customValidators={
			@CustomValidator(
					  type ="requirefield",
					  fieldName = "frzDateStr",				 
					  key="garuda.cbu.cordentry.freezedate",
					  shortCircuit=true
					 /*parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}*/),
					 @CustomValidator(
							  type ="mindaterangefield",
							  fieldName = "birthDateStr",				 
							  key="garuda.cbu.cordentry.frzdateafterproc",
							  shortCircuit=false,
							  parameters = { @ValidationParameter( name = "invokingFieldTime", value = "cdrCbuPojo.freezeTime" ),
									         @ValidationParameter( name = "compareFieldName", value = "prcsngStartDateStr" ),
									         @ValidationParameter( name = "compareFieldTime", value = "cdrCbuPojo.processingTime" )
							               }
							  ),					
			@CustomValidator(
							  type ="datetimerangefieldvalidator",
							  fieldName = "frzDateStr",				 
							  key="garuda.common.message.validatedateminmax",
							  shortCircuit=true,
							  parameters = { @ValidationParameter( name = "timeRange", value = "48"),
									         @ValidationParameter( name = "invokingFieldTime", value = "cdrCbuPojo.processingTime"),
									         @ValidationParameter( name = "compareFieldName", value = "cbuCollectionDateStr"),
									         @ValidationParameter( name = "compareFieldTime", value = "cdrCbuPojo.cbuCollectionTime")
							               }
							  ),
			@CustomValidator(
							  type ="daterangefieldvalidator",
							  fieldName = "frzDateStr",				 
							  key="garuda.cbu.cordentry.freezedtwrtproc",
							  shortCircuit=true,
							  parameters = { @ValidationParameter( name = "startDateFieldName", value = "procStartDate"),
									         @ValidationParameter( name = "terminDateFieldName", value = "procTermiDate")
							               }
							  )
			})
	public Date getFrzDateStr() {
		return frzDateStr;
	}

	public void setFrzDateStr(Date frzDateStr) {
		this.frzDateStr = frzDateStr;
	}

	@CustomValidator(
				  type ="labsumpreprocvalidator",
				  fieldName = "savePreTestList",				 
				  key="Lab Summary Pre Test List Validation",
				  shortCircuit=true,
				  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )						        
				               }
				  )
	public List<PatLabsPojo> getSavePreTestList() {
		return savePreTestList;
	}

	public void setSavePreTestList(List<PatLabsPojo> savePreTestList) throws Exception {
		this.savePreTestList = savePreTestList;
		setProcessingTestMap(processingTestMap);
	}

	public  HashMap<Long, String> getProcessingTestMap() {
		return processingTestMap;
	}

	public void setProcessingTestMap(HashMap<Long, String> processingTestMap)throws Exception {
		HashMap<Long, String> processingTestMapTemp = new HashMap<Long, String>();
		for (LabTestPojo processingTestTemp : new CBUController().getProcessingTestInfo(com.velos.ordercomponent.util.VelosGarudaConstants.LAB_GROUP_TYPE)) {
			processingTestMapTemp.put(processingTestTemp.getPkLabtest(), processingTestTemp.getShrtName());
		}
		this.processingTestMap = processingTestMapTemp;
	}
	
	@CustomValidator(
			  type ="labsumpostprocvalidator",
			  fieldName = "savePostTestList",				 
			  key="Lab Summary Post Test List Validation",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )						        
			               }
			  )
	public List<PatLabsPojo> getSavePostTestList() {
		return savePostTestList;
	}

	public void setSavePostTestList(List<PatLabsPojo> savePostTestList) {
		this.savePostTestList = savePostTestList;
	}

	@CustomValidator(
			  type ="labsumpostthawprocvalidator",
			  fieldName = "saveOtherPostTestList1",				 
			  key="Lab Summary Post Test List Validation",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )						        
			               }
			  )
	public List<PatLabsPojo> getSaveOtherPostTestList1() {
		return saveOtherPostTestList1;
	}

	public void setSaveOtherPostTestList1(List<PatLabsPojo> saveOtherPostTestList1) {
		this.saveOtherPostTestList1 = saveOtherPostTestList1;
	}

	@CustomValidator(
			  type ="labsumpostthawprocvalidator",
			  fieldName = "saveOtherPostTestList2",				 
			  key="Lab Summary Post Test List Validation",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )						        
			               }
			  )
	public List<PatLabsPojo> getSaveOtherPostTestList2() {
		return saveOtherPostTestList2;
	}

	public void setSaveOtherPostTestList2(List<PatLabsPojo> saveOtherPostTestList2) {
		this.saveOtherPostTestList2 = saveOtherPostTestList2;
	}

	@CustomValidator(
			  type ="labsumpostthawprocvalidator",
			  fieldName = "saveOtherPostTestList3",				 
			  key="Lab Summary Post Test List Validation",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )						        
			               }
			  )
	public List<PatLabsPojo> getSaveOtherPostTestList3() {
		return saveOtherPostTestList3;
	}

	public void setSaveOtherPostTestList3(List<PatLabsPojo> saveOtherPostTestList3) {
		this.saveOtherPostTestList3 = saveOtherPostTestList3;
	}

	@CustomValidator(
			  type ="labsumpostthawprocvalidator",
			  fieldName = "saveOtherPostTestList4",				 
			  key="Lab Summary Post Test List Validation",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )						        
			               }
			  )
	public List<PatLabsPojo> getSaveOtherPostTestList4() {
		return saveOtherPostTestList4;
	}

	public void setSaveOtherPostTestList4(List<PatLabsPojo> saveOtherPostTestList4) {
		this.saveOtherPostTestList4 = saveOtherPostTestList4;
	}

	@CustomValidator(
			  type ="labsumpostthawprocvalidator",
			  fieldName = "saveOtherPostTestList5",				 
			  key="Lab Summary Post Test List Validation",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )						        
			               }
			  )
	public List<PatLabsPojo> getSaveOtherPostTestList5() {
		return saveOtherPostTestList5;
	}

	public void setSaveOtherPostTestList5(List<PatLabsPojo> saveOtherPostTestList5) {
		this.saveOtherPostTestList5 = saveOtherPostTestList5;
	}

	@CustomValidator(
			  type ="labsumpostthawprocvalidator",
			  fieldName = "saveOtherPostTestList6",				 
			  key="Lab Summary Post Test List Validation",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )						        
			               }
			  )
	public List<PatLabsPojo> getSaveOtherPostTestList6() {
		return saveOtherPostTestList6;
	}

	public void setSaveOtherPostTestList6(List<PatLabsPojo> saveOtherPostTestList6) {
		this.saveOtherPostTestList6 = saveOtherPostTestList6;
	}
	@CustomValidator(
			  type ="longfieldrequire",
			  fieldName = "fkCordCbuEligible",				 
			  key="garuda.cbu.cordentry.cordeligible",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}) 
	public Long getFkCordCbuEligible() {
		return fkCordCbuEligible;
	}

	public void setFkCordCbuEligible(Long fkCordCbuEligible) {
		this.fkCordCbuEligible = fkCordCbuEligible;
	}

	public Long getUnLicensedPk() {
		return unLicensedPk;
	}

	public void setUnLicensedPk(Long unLicensedPk) {
		this.unLicensedPk = unLicensedPk;
	}
	@CustomValidator(
			  type ="requireformvalidator",
			  fieldName = "idmFormVersion",				 
			  key="garuda.label.forms.idmvalidationmsg",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
					         @ValidationParameter( name = "dependencyField1", value = "cbuLicStatus" ),
					         @ValidationParameter( name = "dependencyField2", value = "unLicensedPk" )
			               }
			  )
	public Byte getIdmFormVersion() {
		return idmFormVersion;
	}

	public void setIdmFormVersion(Byte idmFormVersion) {
		this.idmFormVersion = idmFormVersion;
	}
	@CustomValidator(
			  type ="requireformvalidator",
			  fieldName = "fmhqFormVersion",				 
			  key="garuda.label.forms.fmhqvalidationmsg",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
					         @ValidationParameter( name = "dependencyField1", value = "cbuLicStatus" ),
					         @ValidationParameter( name = "dependencyField2", value = "unLicensedPk" )
			               }
			  )
	public Byte getFmhqFormVersion() {
		return fmhqFormVersion;
	}

	public void setFmhqFormVersion(Byte fmhqFormVersion) {
		this.fmhqFormVersion = fmhqFormVersion;
	}
	@CustomValidator(
			  type ="requireformvalidator",
			  fieldName = "mrqFormVersion",				 
			  key="garuda.label.forms.mrqvalidationmsg",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
					         @ValidationParameter( name = "dependencyField1", value = "cbuLicStatus" ),
					         @ValidationParameter( name = "dependencyField2", value = "unLicensedPk" )
			               }
			  )
	public Byte getMrqFormVersion() {
		return mrqFormVersion;
	}

	public void setMrqFormVersion(Byte mrqFormVersion) {
		this.mrqFormVersion = mrqFormVersion;
	}
	@CustomValidator(
			  type ="cordentrynoncustomvalidator",
			  fieldName = "hlaPercentage",				 
			  key="garuda.label.message.cbuhla",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )
					        })
	public Short getHlaPercentage() {
		return hlaPercentage;
	}

	public void setHlaPercentage(Short hlaPercentage) {
		this.hlaPercentage = hlaPercentage;
	}
	@VisitorFieldValidator(message="",context="action alias")
	public EligibilityNewDeclPojo getDeclNewPojo() {
		return declNewPojo;
	}

	public void setDeclNewPojo(EligibilityNewDeclPojo declNewPojo) {
		this.declNewPojo = declNewPojo;
	}
	@CustomValidator(
			  type ="cordentrynoncustomvalidator",
			  fieldName = "reasons",				 
			  key="garuda.cbu.clinicalnote.reason",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
					         @ValidationParameter( name = "fields", value = "fkCordCbuEligible,cdrCbuPojo.inEligiblePkid,cdrCbuPojo.incompleteReasonId" )})
	public Long[] getReasons() {
		return reasons;
	}

	public void setReasons(Long[] reasons) {
		this.reasons = reasons;
	}
	@CustomValidator(
			  type ="cordentrynoncustomvalidator",
			  fieldName = "cordAdditionalInfo",				 
			  key="garuda.cbu.cordentry.cordadditionalinfo",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
					         @ValidationParameter( name = "fields", value = "fkCordCbuEligible,cdrCbuPojo.notCollectedToPriorReasonId" )})
	@StringLengthFieldValidator(key = "garuda.cbu.cordentry.maxchar", shortCircuit = true, trim = false, minLength = "1",  maxLength = "300")				         
	public String getCordAdditionalInfo() {
		return cordAdditionalInfo;
	}

	public void setCordAdditionalInfo(String cordAdditionalInfo) {
		this.cordAdditionalInfo = cordAdditionalInfo;
	}

	public Date getProcStartDate() {
		return procStartDate;
	}

	public void setProcStartDate(Date procStartDate) {
		this.procStartDate = procStartDate;
	}

	public Date getProcTermiDate() {
		return procTermiDate;
	}

	public void setProcTermiDate(Date procTermiDate) {
		this.procTermiDate = procTermiDate;
	}
	@Validations(customValidators={
	@CustomValidator(
			  type ="cordentrynoncustomvalidator",
			  fieldName = "fungCultDateStr",				 
			  key="garuda.cbu.cordentry.bactculstartdate",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
					  		 @ValidationParameter( name = "fields", value = "9763,cdrCbuPojo.bacterialResult")					       			          
					        }),
	@CustomValidator(
			  type ="daterangefieldvalidator",
			  fieldName = "fungCultDateStr",				 
			  key="garuda.common.message.validatedateminmax",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "startDateFieldName", value = "cbuCollectionDateStr"),
					         @ValidationParameter( name = "terminDateFieldName", value = "todayDate")
			               }
			  )
	})
	public Date getFungCultDateStr() {
		return fungCultDateStr;
	}

	public void setFungCultDateStr(Date fungCultDateStr) {
		this.fungCultDateStr = fungCultDateStr;
	}
	@Validations(customValidators={
			@CustomValidator(
					  type ="cordentrynoncustomvalidator",
					  fieldName = "bactCultDateStr",				 
					  key="garuda.cbu.cordentry.fungalstartdate",
					  shortCircuit=true,
					  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
							  		 @ValidationParameter( name = "fields", value = "9763,cdrCbuPojo.bacterialResult")
							        }),
			@CustomValidator(
					  type ="daterangefieldvalidator",
					  fieldName = "bactCultDateStr",				 
					  key="garuda.common.message.validatedateminmax",
					  shortCircuit=true,
					  parameters = { @ValidationParameter( name = "startDateFieldName", value = "cbuCollectionDateStr"),
							         @ValidationParameter( name = "terminDateFieldName", value = "todayDate")
					               }
					  )
			})
	public Date getBactCultDateStr() {
		return bactCultDateStr;
	}

	public void setBactCultDateStr(Date bactCultDateStr) {
		this.bactCultDateStr = bactCultDateStr;
	}
	@Validations(customValidators={
			@CustomValidator(
					  type ="requirefield",
					  fieldName = "preTestDateStrn",				 
					  key="garuda.cbu.cordentry.inputId",
					  shortCircuit=true
					  /*parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}*/),
			@CustomValidator(
					  type ="daterangefieldvalidator",
					  fieldName = "preTestDateStrn",				 
					  key="garuda.common.message.validatedateminmax",
					  shortCircuit=true,
					  parameters = { @ValidationParameter( name = "startDateFieldName", value = "cbuCollectionDateStr"),
							         @ValidationParameter( name = "terminDateFieldName", value = "prcsngStartDateStr")
					               }
					  )
			})
	public Date getPreTestDateStrn() {
		return preTestDateStrn;
	}

	public void setPreTestDateStrn(Date preTestDateStrn) {
		this.preTestDateStrn = preTestDateStrn;
	}
	@Validations(customValidators={
			@CustomValidator(
					  type ="requirefield",
					  fieldName = "postTestDateStrn",				 
					  key="garuda.cbu.cordentry.inputId",
					  shortCircuit=true
					  /*parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}*/),
			@CustomValidator(
					  type ="daterangefieldvalidator",
					  fieldName = "postTestDateStrn",				 
					  key="garuda.common.message.validatedateminmax",
					  shortCircuit=true,
					  parameters = { @ValidationParameter( name = "startDateFieldName", value = "cbuCollectionDateStr"),
							         @ValidationParameter( name = "terminDateFieldName", value = "prcsngStartDateStr"),
							         @ValidationParameter( name = "todayDate", value = "todayDate")
					               }
					  )
			})
	public Date getPostTestDateStrn() {
		return postTestDateStrn;
	}

	public void setPostTestDateStrn(Date postTestDateStrn) {
		this.postTestDateStrn = postTestDateStrn;
	}
	@Validations(customValidators={
			@CustomValidator(
					  type ="requirefield",
					  fieldName = "otherTestDateStrn1",				 
					  key="garuda.cbu.cordentry.inputId",
					  shortCircuit=true
					  /*parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}*/),
			@CustomValidator(
					  type ="daterangefieldvalidator",
					  fieldName = "otherTestDateStrn1",				 
					  key="garuda.common.message.validatedateminmax",
					  shortCircuit=true,
					  parameters = { @ValidationParameter( name = "startDateFieldName", value = "cbuCollectionDateStr"),
							         @ValidationParameter( name = "terminDateFieldName", value = "prcsngStartDateStr"),
							         @ValidationParameter( name = "todayDate", value = "todayDate")
					               }
					  )
			})
	public Date getOtherTestDateStrn1() {
		return otherTestDateStrn1;
	}
    
	public void setOtherTestDateStrn1(Date otherTestDateStrn1) {
		this.otherTestDateStrn1 = otherTestDateStrn1;
	}
	@Validations(customValidators={
			@CustomValidator(
					  type ="requirefield",
					  fieldName = "otherTestDateStrn2",				 
					  key="garuda.cbu.cordentry.inputId",
					  shortCircuit=true
					  /*parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}*/),
			@CustomValidator(
					  type ="daterangefieldvalidator",
					  fieldName = "otherTestDateStrn2",				 
					  key="garuda.common.message.validatedateminmax",
					  shortCircuit=true,
					  parameters = { @ValidationParameter( name = "startDateFieldName", value = "cbuCollectionDateStr"),
							         @ValidationParameter( name = "terminDateFieldName", value = "prcsngStartDateStr"),
							         @ValidationParameter( name = "todayDate", value = "todayDate")
					               }
					  )
			})
	public Date getOtherTestDateStrn2() {
		return otherTestDateStrn2;
	}

	public void setOtherTestDateStrn2(Date otherTestDateStrn2) {
		this.otherTestDateStrn2 = otherTestDateStrn2;
	}
	@Validations(customValidators={
			@CustomValidator(
					  type ="requirefield",
					  fieldName = "otherTestDateStrn3",				 
					  key="garuda.cbu.cordentry.inputId",
					  shortCircuit=true
					  /*parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}*/),
			@CustomValidator(
					  type ="daterangefieldvalidator",
					  fieldName = "otherTestDateStrn3",				 
					  key="garuda.common.message.validatedateminmax",
					  shortCircuit=true,
					  parameters = { @ValidationParameter( name = "startDateFieldName", value = "cbuCollectionDateStr"),
							         @ValidationParameter( name = "terminDateFieldName", value = "prcsngStartDateStr"),
							         @ValidationParameter( name = "todayDate", value = "todayDate")
					               }
					  )
			})
	public Date getOtherTestDateStrn3() {
		return otherTestDateStrn3;
	}

	public void setOtherTestDateStrn3(Date otherTestDateStrn3) {
		this.otherTestDateStrn3 = otherTestDateStrn3;
	}
	@Validations(customValidators={
			@CustomValidator(
					  type ="requirefield",
					  fieldName = "otherTestDateStrn4",				 
					  key="garuda.cbu.cordentry.inputId",
					  shortCircuit=true
					  /*parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}*/),
			@CustomValidator(
					  type ="daterangefieldvalidator",
					  fieldName = "otherTestDateStrn4",				 
					  key="garuda.common.message.validatedateminmax",
					  shortCircuit=true,
					  parameters = { @ValidationParameter( name = "startDateFieldName", value = "cbuCollectionDateStr"),
							         @ValidationParameter( name = "terminDateFieldName", value = "prcsngStartDateStr"),
							         @ValidationParameter( name = "todayDate", value = "todayDate")
					               }
					  )
			})
	public Date getOtherTestDateStrn4() {
		return otherTestDateStrn4;
	}

	public void setOtherTestDateStrn4(Date otherTestDateStrn4) {
		this.otherTestDateStrn4 = otherTestDateStrn4;
	}
	@Validations(customValidators={
			@CustomValidator(
					  type ="requirefield",
					  fieldName = "otherTestDateStrn5",				 
					  key="garuda.cbu.cordentry.inputId",
					  shortCircuit=true
					  /*parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}*/),
			@CustomValidator(
					  type ="daterangefieldvalidator",
					  fieldName = "otherTestDateStrn5",				 
					  key="garuda.common.message.validatedateminmax",
					  shortCircuit=true,
					  parameters = { @ValidationParameter( name = "startDateFieldName", value = "cbuCollectionDateStr"),
							         @ValidationParameter( name = "terminDateFieldName", value = "prcsngStartDateStr"),
							         @ValidationParameter( name = "todayDate", value = "todayDate")
					               }
					  )
			})
	public Date getOtherTestDateStrn5() {
		return otherTestDateStrn5;
	}

	public void setOtherTestDateStrn5(Date otherTestDateStrn5) {
		this.otherTestDateStrn5 = otherTestDateStrn5;
	}
	@Validations(customValidators={
			@CustomValidator(
					  type ="requirefield",
					  fieldName = "otherTestDateStrn6",				 
					  key="garuda.cbu.cordentry.inputId",
					  shortCircuit=true
					  /*parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}*/),
			@CustomValidator(
					  type ="daterangefieldvalidator",
					  fieldName = "otherTestDateStrn6",				 
					  key="garuda.common.message.validatedateminmax",
					  shortCircuit=true,
					  parameters = { @ValidationParameter( name = "startDateFieldName", value = "cbuCollectionDateStr"),
							         @ValidationParameter( name = "terminDateFieldName", value = "prcsngStartDateStr"),
							         @ValidationParameter( name = "todayDate", value = "todayDate")
					               }
					  )
			})
	public Date getOtherTestDateStrn6() {
		return otherTestDateStrn6;
	}

	public void setOtherTestDateStrn6(Date otherTestDateStrn6) {
		this.otherTestDateStrn6 = otherTestDateStrn6;
	}
	
	public Date getTodayDate() {
		return todayDate;
	}

	public void setTodayDate(Date todayDate) {
		this.todayDate = new Date();
	}
	@VisitorFieldValidator(message="",context="action alias")
	public ClinicalNotePojo getClinicalNotePojo() {
		return clinicalNotePojo;
	}

	public void setClinicalNotePojo(ClinicalNotePojo clinicalNotePojo) {
		this.clinicalNotePojo = clinicalNotePojo;
	}
   
	public List<ClinicalNotePojo> getMultipleClinicalNotes() {
		return multipleClinicalNotes;
	}

	public void setMultipleClinicalNotes(
			List<ClinicalNotePojo> multipleClinicalNotes) {
		this.multipleClinicalNotes = multipleClinicalNotes;
	}
	@CustomValidator(
			  type ="autodefervalidator",
			  fieldName = "cordAutoDeferStatus",				 
			  key="true",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "fields", value = "cfuCountPk,viabilityPk,savePostTestList,cdrCbuPojo.cordID,cdrCbuPojo.fungalResult,cdrCbuPojo.bacterialResult,cdrCbuPojo.hemoglobinScrn,cdrCbuPojo.fkMultipleBirth,entitySamplesPojo.noSegAvail,cdrCbuPojo.fkCbbProcedure" )
					        })
	public Boolean getCordAutoDeferStatus() {
		return cordAutoDeferStatus;
	}

	public void setCordAutoDeferStatus(Boolean cordAutoDeferStatus) {
		this.cordAutoDeferStatus = cordAutoDeferStatus;
	}

	public Long getCfuCountPk() {
		return cfuCountPk;
	}

	public void setCfuCountPk(Long cfuCountPk) {
		this.cfuCountPk = cfuCountPk;
	}

	public Long getViabilityPk() {
		return viabilityPk;
	}

	public void setViabilityPk(Long viabilityPk) {
		this.viabilityPk = viabilityPk;
	}
	@CustomValidator(
			  type ="checkdataexist",
			  fieldName = "isCordSubmitted",				 
			  key="true",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "fields", value = "cdrCbuPojo.cordID" )
				            }
   )	
	public Boolean getIsCordSubmitted() {
		return isCordSubmitted;
	}

	public void setIsCordSubmitted(Boolean isCordSubmitted) {
		this.isCordSubmitted = isCordSubmitted;
	}

	public String execute()throws Exception
	  {	   
	    log.info("Validation Executed Successfull......................");	     
	    return  NONE;
	  }
	
    public static enum EligibilityFields{
		MRQNEWQUES1A, PHYNEWFINDQUES4A, IDMNEWQUES7A, IDMNEWQUES8A, MRQNEWQUES3ADDDETAIL, PHYNEWFINDQUES5ADDDETAIL, PHYNEWFINDQUES6ADDDETAIL, IDMNEWQUES9ADDDETAIL, DEF;
	}
    static{
    	ServletContext context = ServletActionContext.getServletContext();   	 	
   	  codeListMapByIds = (Map<Long,CodeList>)context.getAttribute("codeListMapByIds");    	
    }
    
    public static  Long getCodeListPkByTypeAndSubtype(String type, String subType,Map<Long,CodeList> codeListMapByIds){
		  Long pk = null;
		     for(Map.Entry<Long, CodeList> me : codeListMapByIds.entrySet()){
		    	 if((type!=null && (type.equals(me.getValue().getType()) || type==me.getValue().getType())) 
		    			 && (subType!=null && (subType.equals(me.getValue().getSubType()) || subType==me.getValue().getSubType()))){
		    		 pk = me.getKey();
		    	 }
		     }
		  return pk;
		}
    
    public static Long getCodeListPkByTypeAndSubtype(String type, String subType){
  	  Long pk = null;
  	     for(Map.Entry<Long, CodeList> me : codeListMapByIds.entrySet()){
  	    	 if((type!=null && (type.equals(me.getValue().getType()) || type==me.getValue().getType())) 
  	    			 && (subType!=null && (subType.equals(me.getValue().getSubType()) || subType==me.getValue().getSubType()))){
  	    		 pk = me.getKey();
  	    	 }
  	     }
  	  return pk;
  	}

}
