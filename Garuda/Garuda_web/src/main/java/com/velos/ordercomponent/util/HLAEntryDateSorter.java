package com.velos.ordercomponent.util;

import com.velos.ordercomponent.business.pojoobjects.HLAPojo;
import java.util.Comparator;
import java.util.List;

public class HLAEntryDateSorter implements Comparator<List<HLAPojo>> {

	@Override
	public int compare(List<HLAPojo> lh1, List<HLAPojo> lh2) {	
		
		
		return  ((HLAPojo)lh2.get(0)).getHlaRecievedDate().compareTo(((HLAPojo)lh1.get(0)).getHlaRecievedDate());
	}

}
