package com.velos.ordercomponent.action;

import java.awt.Color;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.extensions.XSSFCellBorder.BorderSide;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.util.CellRangeAddress;

import com.opensymphony.xwork2.ActionSupport;
import com.velos.ordercomponent.business.pojoobjects.CordFilterPojo;
import com.velos.ordercomponent.business.pojoobjects.HLAFilterPojo;
import com.velos.ordercomponent.util.VelosGarudaConstants;

public class AntigenExcel extends ActionSupport {
	
	 public final int BLACKROWCOUNT = 1;
	 public final int GRAYROWCOUNT = 1;
	 private Boolean isMultipleCBBSelected = false;
	 
	public Workbook antigenExcel(List<CordFilterPojo> cdrObjects, Map<Long, HLAFilterPojo> bestHlasMap, Map<Long, Map<Long, HLAFilterPojo>> cbuHlas, Workbook wb){
		
		 
		 XSSFCellStyle grayStyle = grayStyle(wb);
		 XSSFCellStyle blackStyle = blackStyle(wb);
		 XSSFCellStyle headStyle = headStyle(wb);
		 XSSFCellStyle whiteTopGrayStyle = whiteTopGrayStyle(wb);
		 XSSFCellStyle leftGrayStyle = leftGrayStyle(wb);
		 XSSFCellStyle leftGrayBackgroungStyle = leftGrayBackgroungStyle(wb);
		 XSSFCellStyle whiteStyle = whiteStyle(wb);
		
		 XSSFCellStyle dateCellStyle = dateStyle(wb);		
		 Row row;
		 Cell cell;
		 int rowNum = 1;
		 short col = 0;
		 HLAFilterPojo hla = null;
		 Map<Long, HLAFilterPojo> cbuHla;
		 short hlaColindex;
		 int firstRow;
		 int lastRow;
		 int firstCol;
		 int lastCol;		
		 int cdrSegment=0;
		 int sheetNo = 1;
		 int cbuHLASize ;
		 int index = 0;
		 Sheet sheet = sheet(wb, getText("garuda.querybuilder.report.antigen"));
		 int noOfCell = createHeader(sheet, headStyle);
		 
		 /*long startTime=System.currentTimeMillis();*/
		 
		 for(CordFilterPojo cdrObj : cdrObjects){
		
	
	
		 
			 hla 	=  bestHlasMap.get(cdrObj.cord);
			 cbuHla = cbuHlas.get(cdrObj.cord);
			 cbuHLASize = cbuHla == null ? 0: cbuHla.size();
			 bestHlasMap.remove(cdrObj.cord);//remove map reference
			 cbuHlas.remove(cdrObj.cord);//remove map reference
			 
			 if(rowNum + BLACKROWCOUNT + GRAYROWCOUNT + cbuHLASize > VelosGarudaConstants.MAXROW_XLSX ){
				 
				 sheet = sheet(wb,getText("garuda.querybuilder.report.antigen")+" "+(++sheetNo));
				 createHeader(sheet, headStyle);
				 rowNum = 1;
			 }
			 
			  row = sheet.createRow(rowNum++);
			  col = 0;
			  firstCol = 0;
			  if(isMultipleCBBSelected)
				  row.createCell(col++).setCellValue(cdrObj.site); // Site Is Conditional --- if multiple CBB is selected then Site Column Have to Include
			  row.createCell(col++).setCellValue(cdrObj.registry);
			  row.createCell(col++).setCellValue(cdrObj.localCbu);
			  row.createCell(col++).setCellValue(cdrObj.numberOnBag);
			  row.createCell(col++).setCellValue(cdrObj.isbtCode);
			  row.createCell(col++).setCellValue(cdrObj.historic);
			  row.createCell(col++).setCellValue(cdrObj.maternal);
			  row.createCell(col++).setCellValue(cdrObj.localMat);
			  row.createCell(col++).setCellValue(cdrObj.patient);
			  cell = row.createCell(col++);
			  if(cdrObj.collectDate != null)//cdrObj.collectDate
				  cell.setCellValue(cdrObj.collectDate);
			  cell.setCellStyle(dateCellStyle);
			  
			  hlaColindex = col;
			  cdrSegment = lastCol = col-1;
			  
			  /*********** Best HLA ********************/
			  
			  if(hla != null){
				  
				  row.createCell(col++).setCellValue("Summary of HLA");
				  
				  row.createCell(col++).setCellStyle(grayStyle);/*setCellValue(hla.typingDate);*///hla.typingDate
				  row.createCell(col++).setCellStyle(grayStyle);/*setCellValue(hla.receivedDate);*///hla.receivedDate
				  row.createCell(col++).setCellStyle(grayStyle);/*setCellValue(hla.source);*/// source
				  
				  row.createCell(col++).setCellValue(hla.aType1);
				  row.createCell(col++).setCellValue(hla.aType2);
				  
				  row.createCell(col++).setCellValue(hla.bType1);
				  row.createCell(col++).setCellValue(hla.bType2);
				  
				  row.createCell(col++).setCellValue(hla.cType1);
				  row.createCell(col++).setCellValue(hla.cType2);
				  
				  row.createCell(col++).setCellValue(hla.drb1Type1);
				  row.createCell(col++).setCellValue(hla.drb1Type2);
				  
				  row.createCell(col++).setCellValue(hla.drb3Type1);
				  row.createCell(col++).setCellValue(hla.drb3Type2);
				  
				  row.createCell(col++).setCellValue(hla.drb4Type1);
				  row.createCell(col++).setCellValue(hla.drb4Type2);
				  
				  row.createCell(col++).setCellValue(hla.drb5Type1);
				  row.createCell(col++).setCellValue(hla.drb5Type2);
				  
				  row.createCell(col++).setCellValue(hla.dpb1Type1);
				  row.createCell(col++).setCellValue(hla.dpb1Type2);
				  
				  row.createCell(col++).setCellValue(hla.dqb1Type1);
				  row.createCell(col++).setCellValue(hla.dqb1Type2);
				  
			  }else{
				  
				  row.createCell(col++).setCellValue("Summary of HLA");
			  }
			  
			  firstRow = rowNum;
			  row = sheet.createRow(rowNum++); // Create Blank Row 
			  //row.setRowStyle(whiteStyle);///////////////////////////
			  
			  for(int i=0; i<=cdrSegment;i++){//					  
				  
				  
				  row.createCell(i).setCellStyle(whiteTopGrayStyle);
			  }
			  
			  row.createCell(cdrSegment+1).setCellStyle(leftGrayBackgroungStyle);
			  
			  for(int i = cdrSegment+2; i<=noOfCell; i++){//
				 
				 /* if(i>cdrSegment){*/
					 cell =  row.createCell(i);cell.setCellStyle(grayStyle);
				 /* }else
					  row.createCell(i).setCellStyle(whiteTopGrayStyle);*/
			  }
			  
			  
			 // System.out.println("-----> Row Number :"+rowNum);
			  /***************** HLA *********************/
			
			  if(cbuHla != null){
			  for(Map.Entry<Long, HLAFilterPojo> hp:cbuHla.entrySet()){
				 
				  row = sheet.createRow(rowNum++); // HLA Row
				  hla = hp.getValue();
				  col = hlaColindex;
				  
				  for(int i=0; i<=cdrSegment;i++){//					  
					  
					  
					      row.createCell(i).setCellStyle(whiteStyle);
				  }
				  
				  cell =  row.createCell(col++);cell.setCellValue("History");cell.setCellStyle(leftGrayStyle);
				  
				  cell = row.createCell(col++);// hla.typingDate
				  if(hla.typingDate != null)
					  cell.setCellValue(hla.typingDate);
				  cell.setCellStyle(dateCellStyle);
				 
				  cell = row.createCell(col++);//hla.receivedDate
				  if(hla.receivedDate != null)
					  cell.setCellValue(hla.receivedDate);
				  cell.setCellStyle(dateCellStyle);
				  
				  row.createCell(col++).setCellValue(hla.user);// User
				  
				  row.createCell(col++).setCellValue(hla.aType1);
				  row.createCell(col++).setCellValue(hla.aType2);
				  
				  row.createCell(col++).setCellValue(hla.bType1);
				  row.createCell(col++).setCellValue(hla.bType2);
				  
				  row.createCell(col++).setCellValue(hla.cType1);
				  row.createCell(col++).setCellValue(hla.cType2);
				  
				  row.createCell(col++).setCellValue(hla.drb1Type1);
				  row.createCell(col++).setCellValue(hla.drb1Type2);
				  
				  row.createCell(col++).setCellValue(hla.drb3Type1);
				  row.createCell(col++).setCellValue(hla.drb3Type2);
				  
				  row.createCell(col++).setCellValue(hla.drb4Type1);
				  row.createCell(col++).setCellValue(hla.drb4Type2);
				  
				  row.createCell(col++).setCellValue(hla.drb5Type1);
				  row.createCell(col++).setCellValue(hla.drb5Type2);
				  
				  row.createCell(col++).setCellValue(hla.dpb1Type1);
				  row.createCell(col++).setCellValue(hla.dpb1Type2);
				  
				  row.createCell(col++).setCellValue(hla.dqb1Type1);
				  row.createCell(col++).setCellValue(hla.dqb1Type2);
				  
			   }
			  }else{
				  
				  row = sheet.createRow(rowNum++);
				  col = hlaColindex;
				  
				  for(int i=0; i<=cdrSegment;i++){
					  row.createCell(i).setCellStyle(whiteStyle);;
				  }
				  
				  cell = row.createCell(col++);cell.setCellValue("History");cell.setCellStyle(leftGrayStyle);
				  
				/*  for(int i=col; i<=noOfCell; i++){
					  row.createCell(i);
				  }*/
			  }
             lastRow = rowNum-1;
             
             /*sheet.addMergedRegion(new CellRangeAddress( firstRow,
									                     lastRow,
									                     firstCol,
									                     lastCol));*/
             
			  row = sheet.createRow(rowNum++); // Create Blank Row 
			  //row.setRowStyle(blackStyle);
			  
			  for(int i=0; i<=noOfCell; i++){				
					  row.createCell(i).setCellStyle(blackStyle);				
			  }
			  
			 /* if(rowNum%1000 == 0){
			   long endTime=System.currentTimeMillis();
		         System.out.println();
		         System.out.println((endTime-startTime)/1000+" : "+(endTime-startTime)%1000+" MM:SS\n\n");
		         
		         startTime=System.currentTimeMillis();
			  }*/
			  
		 }
		 
		 return wb;
	}
	
	
	
	
	private int createHeader(Sheet sheet, XSSFCellStyle blackStyle){
		
		 int r = 0, c = 0;
		 Cell cell;
		 Row row;
		 row = sheet.createRow(r);
		// row.setRowStyle(blackStyle);
	
     
		 row.setHeight((short)500);
    
		  if(isMultipleCBBSelected){
			  cell=row.createCell(c++);cell.setCellValue("CBB");cell.setCellStyle(blackStyle);
		  }	
		  
		cell=row.createCell(c++);cell.setCellValue(getText("garuda.cbuentry.label.registrycbuid"));cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue(getText("garuda.cbuentry.label.localcbuid"));cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue(getText("garuda.cdrcbuview.label.id_on_bag"));cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue(getText("garuda.shippedcbu.report.label.isbt"));cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue(getText("garuda.querybuilderreport.label.historiclocalid"));cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue(getText("garuda.cbuentry.label.maternalregistryid"));cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue(getText("garuda.cordentry.label.localmaternalid"));cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue(getText("garuda.querybuilderreport.label.activeforpatient"));cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue(getText("garuda.queryBuilder.label.cbucollectiondate"));cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue(getText("garuda.cdrcbuview.label.type"));cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue("Typing Date");cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue("Received Date");cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue("Source");cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue("A Type1");cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue("A Type2");cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue("B Type1");cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue("B Type2");cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue("C Type1");cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue("C Type2");cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue("DRB1 Type1");cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue("DRB1 Type2");cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue("DRB3 Type1");cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue("DRB3 Type2");cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue("DRB4 Type1");cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue("DRB4 Type2");cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue("DRB5 Type1");cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue("DRB5 Type2");cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue("DQB1 Type1");cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue("DQB1 Type2");cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue("DPB1 Type1");cell.setCellStyle(blackStyle);
		cell=row.createCell(c);cell.setCellValue("DPB1 Type2");cell.setCellStyle(blackStyle);
		
		return c;
	}
	
	public void genExcel(Sheet sheet){
		
		
		
	}
	
	
public void generateExcel(){
		
		Workbook wb = workBook();
		Sheet sheet = sheet(wb, "HLA");
		  //  createHeader(sheet);
		   // antigenExcel();
	}

private Workbook workBook(){
	
	Workbook wb = new SXSSFWorkbook(1000);
	 
	 return wb;
}

private Sheet sheet(Workbook wb, String sheetName){
	//wb =   new SXSSFWorkbook(100);
	 Sheet sheet = wb.createSheet(sheetName); 
	 
	 return sheet;
}

private XSSFCellStyle grayStyle(Workbook wb){
	
	XSSFCellStyle style = (XSSFCellStyle) wb.createCellStyle();

    style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
    style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
    
    return style;
}

private XSSFCellStyle leftGrayBackgroungStyle(Workbook wb){
	
	XSSFCellStyle style = (XSSFCellStyle) wb.createCellStyle();

    style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
    style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
    style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
    style.setBorderColor(BorderSide.LEFT, new XSSFColor(Color.GRAY));
    
    return style;
}


private XSSFCellStyle blackStyle(Workbook wb){
	
	XSSFCellStyle style = (XSSFCellStyle) wb.createCellStyle();

    style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
    style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
    
   
    return style;
}

private XSSFCellStyle whiteTopGrayStyle(Workbook wb){
	
	XSSFCellStyle style = (XSSFCellStyle) wb.createCellStyle();

    style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
    style.setFillPattern(FillPatternType.SOLID_FOREGROUND);   
    style.setBorderTop(XSSFCellStyle.BORDER_THIN);
    style.setBorderColor(BorderSide.TOP, new XSSFColor(Color.GRAY));
   
    return style;
}

private XSSFCellStyle whiteStyle(Workbook wb){
	
	XSSFCellStyle style = (XSSFCellStyle) wb.createCellStyle();

    style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
    style.setFillPattern(FillPatternType.SOLID_FOREGROUND);   
   // style.setBorderTop(XSSFCellStyle.BORDER_THIN);
   // style.setBorderColor(BorderSide.TOP, new XSSFColor(Color.GRAY));
  
    return style;
}

private XSSFCellStyle leftGrayStyle(Workbook wb){
	
	XSSFCellStyle style = (XSSFCellStyle) wb.createCellStyle();

    //style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
   // style.setFillPattern(FillPatternType.SOLID_FOREGROUND);   
    style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
    style.setBorderColor(BorderSide.LEFT, new XSSFColor(Color.GRAY));
  
    return style;
}


private XSSFCellStyle dateStyle(Workbook wb){
	
	XSSFCellStyle style = (XSSFCellStyle) wb.createCellStyle();
	CreationHelper createHelper = wb.getCreationHelper();
	
	style.setDataFormat(
		        createHelper.createDataFormat().getFormat("MM/dd/yyyy"));
    
    return style;
}

private XSSFCellStyle headStyle(Workbook wb){
	
	XSSFCellStyle style = (XSSFCellStyle) wb.createCellStyle();

    style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
    style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
    style.setWrapText(true);
    
    Font font = wb.createFont();
    font.setColor(IndexedColors.WHITE.getIndex());
    font.setBoldweight(Font.BOLDWEIGHT_BOLD);
    style.setFont(font);
   // Row.setHeight((short)500);
    
    return style;
}




public Boolean getIsMultipleCBBSelected() {
	return isMultipleCBBSelected;
}




public void setIsMultipleCBBSelected(Boolean isMultipleCBBSelected) {
	this.isMultipleCBBSelected = isMultipleCBBSelected;
}

}
