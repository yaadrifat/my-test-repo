package com.velos.ordercomponent.validator;

import java.util.HashMap;
import java.util.List;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

import com.velos.ordercomponent.business.pojoobjects.PatLabsPojo;

/**
 * @author Tarun Sondhi
 * @author Anurag Upadhyay
 * @version 1.0
 * 
 */

public class LabSummaryPreProcessingValidator extends FieldValidatorSupport{
	private boolean searchableCheck;	

	public boolean isSearchableCheck() {
		return searchableCheck;
	}

	public void setSearchableCheck(boolean searchableCheck) {
		this.searchableCheck = searchableCheck;
	}
	@SuppressWarnings("unchecked")
	@Override
	public void validate(Object object) throws ValidationException {
		// TODO Auto-generated method stub
	 String fieldName=getFieldName();
	 int i=0;
	 List<PatLabsPojo> savePreTestList=(List<PatLabsPojo>) getFieldValue(fieldName, object);
	 long cordSearchable = (Long) getFieldValue("cordSearchable", object);
	 HashMap<Long, String> processingTestMap=(HashMap<Long, String>) getFieldValue("processingTestMap", object);	
	 for(PatLabsPojo plp: savePreTestList){
		 if(plp!=null){
		     String testresult=plp.getTestresult();
		     Long testSpecimanType=plp.getFktestspecimen();
		    switch(LabTests.valueOf(processingTestMap.get(plp.getFktestid())==null?"DEF":processingTestMap.get(plp.getFktestid()))){
		    case CBU_VOL:
	    		
	    		if(searchableCheck  && cordSearchable==1 && (testresult==null || testresult.equals(""))){
	    			 setMessageKey("garuda.cbu.cordentry.inputId");
	    			addFieldError(fieldName+"["+i+"].testresult", object);
	    		}else if(testresult!=null && !testresult.equals("") && (Float.parseFloat(testresult)<40 || Float.parseFloat(testresult)>500)){
	    			 setMessageKey("garuda.common.range.testval40to500");
	    			 addFieldError(fieldName+"["+i+"].testresult", object);
	    		}
	    		break;
	    	case TCNCC:	  
	    	case UNCRCT:
	    	case TCBUUN:
	    		if(testresult!=null && !testresult.equals("") && (Float.parseFloat(testresult)<12 || Float.parseFloat(testresult)>999)){
	    			 setMessageKey("garuda.common.range.testval12to999");
	    			 addFieldError(fieldName+"["+i+"].testresult", object);
	    		}
	    		break;
	    	case CBUNCCC:
	    		if(testresult!=null && !testresult.equals("") && (Float.parseFloat(testresult)<1000 || Float.parseFloat(testresult)>50000)){
	    			 setMessageKey("garuda.common.range.testval1000to50000");
	    			 addFieldError(fieldName+"["+i+"].testresult", object);
	    		}
	    		break;
	    	case TCDAD:
	    	case PERNRBC:
	    	case PERTNUC:
	    	case PERTMON:
	    	case CNRBC:
	    	case PERCD3:
	    	case CFUCNT:
	    	case TOTCD:
	    	case FNPV:
	    	case VIAB:
	    	case PERCD:
	    		break;
	    		default:   		
	    			if(searchableCheck  && cordSearchable==1 && (testresult==null || testresult.equals(""))){
		    			 setMessageKey("garuda.cbu.cordentry.inputId");
		    			 addFieldError(fieldName+"["+i+"].testresult", object);
		    		}
	    			if(searchableCheck  && cordSearchable==1 && (testSpecimanType==null || testSpecimanType==-1)){
		    			 setMessageKey("garuda.cbu.cordentry.inputId");
		    			 addFieldError(fieldName+"["+i+"].fktestspecimen", object);
		    		}
	    	}
	    	i++;
	    }
	  }
	}
	 private static enum LabTests
	  {
		 UNCRCT, TCDAD, PERNRBC, PERTNUC, CBU_VOL, PERTMON, CNRBC, PERCD3, CFUCNT, CBUNCCC, TOTCD, FNPV, TCNCC, TCBUUN, VIAB, PERCD,DEF;
	  }

}
