package com.velos.ordercomponent.util;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class GarudaContextListner implements ServletContextListener {

	private ServletContext context = null;
	public static final Log log=LogFactory.getLog(GarudaContextListner.class);
	
	@Override
	public void contextInitialized(ServletContextEvent contextEvent) {
		// TODO Auto-generated method stub
		//log.debug("Context Event Initialized");
		GarudaConfig.initializeOthers(contextEvent.getServletContext());
		setContext(contextEvent.getServletContext());
		
	}

	
	
	@Override
	public void contextDestroyed(ServletContextEvent contextEvent) {
		// TODO Auto-generated method stub
		//log.debug("Context Event destroyed");
		
	}

	public ServletContext getContext() {
		return context;
	}

	public void setContext(ServletContext context) {
		this.context = context;
	}

	
}
