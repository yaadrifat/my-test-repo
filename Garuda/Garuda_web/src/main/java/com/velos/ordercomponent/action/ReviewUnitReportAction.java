package com.velos.ordercomponent.action;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.ordercomponent.business.domain.CBUUnitReport;
import com.velos.ordercomponent.business.domain.CdrCbu;
import com.velos.ordercomponent.business.domain.CordHlaExt;
import com.velos.ordercomponent.business.pojoobjects.CBUUnitReportTempPojo;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.CordHlaExtTempPojo;
import com.velos.ordercomponent.business.pojoobjects.SearchPojo;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.controller.CBUUnitReportController;
import com.velos.ordercomponent.controller.CodelstController;
import com.velos.ordercomponent.controller.ReviewUnitReportController;
import com.velos.ordercomponent.controller.VelosSearchController;
import com.velos.ordercomponent.helper.velosHelper;

public class ReviewUnitReportAction extends VelosBaseAction {

	public static final Log log = LogFactory
			.getLog(ReviewUnitReportAction.class);

	
	private String cbuId;
	private List lstObject;
	private Map codelstParam;
	private CodelstController codelstController;
	private VelosSearchController searchController;
	private CBUUnitReportController linkController;
	private ReviewUnitReportController reviewUnitReportController;
	private SearchPojo searchPojo;
	private CBUUnitReportTempPojo cbuUnitReportTempPojo;
	private CBUUnitReportTempPojo cbuUnitReportTempPojo1;
	private CBUUnitReportTempPojo cbuUnitReportTempPojo2;
	private CdrCbu cdrCbu;
	private CdrCbuPojo cdrCbuPojo;
	private List cordInfoList;
	private CordHlaExtTempPojo cordHlaExtTempPojo;
	private CordHlaExtTempPojo cordHlaExtTempPojo1;
	private CordHlaExtTempPojo cordHlaExtTempPojo2;
	private CBUUnitReport cbuUnitReport;
	private CordHlaExt cordHlaExt;

	public CBUUnitReportController getLinkController() {
		return linkController;
	}

	public void setLinkController(CBUUnitReportController linkController) {
		this.linkController = linkController;
	}

	public List getCordInfoList() {
		return cordInfoList;
	}

	public void setCordInfoList(List cordInfoList) {
		this.cordInfoList = cordInfoList;
	}

	public CdrCbu getCdrCbu() {
		return cdrCbu;
	}

	public void setCdrCbu(CdrCbu cdrCbu) {
		this.cdrCbu = cdrCbu;
	}

	public CdrCbuPojo getCdrCbuPojo() {
		return cdrCbuPojo;
	}

	public void setCdrCbuPojo(CdrCbuPojo cdrCbuPojo) {
		this.cdrCbuPojo = cdrCbuPojo;
	}

	public List getLstObject() {
		return lstObject;
	}

	public void setLstObject(List lstObject) {
		this.lstObject = lstObject;
	}


	public String getCbuId() {
		return cbuId;
	}

	public void setCbuId(String cbuId) {
		this.cbuId = cbuId;
	}

	public Map getCodelstParam() {
		return codelstParam;
	}

	public void setCodelstParam(Map codelstParam) {
		this.codelstParam = codelstParam;
	}

	public CodelstController getCodelstController() {
		return codelstController;
	}

	public void setCodelstController(CodelstController codelstController) {
		this.codelstController = codelstController;
	}

	public VelosSearchController getSearchController() {
		return searchController;
	}

	public void setSearchController(VelosSearchController searchController) {
		this.searchController = searchController;
	}

	public ReviewUnitReportController getReviewUnitReportController() {
		return reviewUnitReportController;
	}

	public void setReviewUnitReportController(
			ReviewUnitReportController reviewUnitReportController) {
		this.reviewUnitReportController = reviewUnitReportController;
	}

	public ReviewUnitReportAction() {
		codelstController = new CodelstController();
		searchController = new VelosSearchController();
		reviewUnitReportController = new ReviewUnitReportController();

	}

	public SearchPojo getSearchPojo() {
		return searchPojo;
	}

	public void setSearchPojo(SearchPojo searchPojo) {
		this.searchPojo = searchPojo;
	}

	public CBUUnitReportTempPojo getCbuUnitReportTempPojo() {
		return cbuUnitReportTempPojo;
	}

	public void setCbuUnitReportTempPojo(
			CBUUnitReportTempPojo cbuUnitReportTempPojo) {
		this.cbuUnitReportTempPojo = cbuUnitReportTempPojo;
	}

	public CBUUnitReportTempPojo getCbuUnitReportTempPojo1() {
		return cbuUnitReportTempPojo1;
	}

	public void setCbuUnitReportTempPojo1(
			CBUUnitReportTempPojo cbuUnitReportTempPojo1) {
		this.cbuUnitReportTempPojo1 = cbuUnitReportTempPojo1;
	}

	public CBUUnitReportTempPojo getCbuUnitReportTempPojo2() {
		return cbuUnitReportTempPojo2;
	}

	public void setCbuUnitReportTempPojo2(
			CBUUnitReportTempPojo cbuUnitReportTempPojo2) {
		this.cbuUnitReportTempPojo2 = cbuUnitReportTempPojo2;
	}

	public CordHlaExtTempPojo getCordHlaExtTempPojo() {
		return cordHlaExtTempPojo;
	}

	public void setCordHlaExtTempPojo(CordHlaExtTempPojo cordHlaExtTempPojo) {
		this.cordHlaExtTempPojo = cordHlaExtTempPojo;
	}

	public CordHlaExtTempPojo getCordHlaExtTempPojo1() {
		return cordHlaExtTempPojo1;
	}

	public void setCordHlaExtTempPojo1(CordHlaExtTempPojo cordHlaExtTempPojo1) {
		this.cordHlaExtTempPojo1 = cordHlaExtTempPojo1;
	}

	public CordHlaExtTempPojo getCordHlaExtTempPojo2() {
		return cordHlaExtTempPojo2;
	}

	public void setCordHlaExtTempPojo2(CordHlaExtTempPojo cordHlaExtTempPojo2) {
		this.cordHlaExtTempPojo2 = cordHlaExtTempPojo2;
	}

	public CBUUnitReport getCbuUnitReport() {
		return cbuUnitReport;
	}

	public void setCbuUnitReport(CBUUnitReport cbuUnitReport) {
		this.cbuUnitReport = cbuUnitReport;
	}

	public CordHlaExt getCordHlaExt() {
		return cordHlaExt;
	}

	public void setCordHlaExt(CordHlaExt cordHlaExt) {
		this.cordHlaExt = cordHlaExt;
	}

	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	public String test() throws Exception {
		setCordInfoList(getCbuAttachInfo(cdrCbuPojo.getCdrCbuId()));
		return "test";
	}



	public String getReviewData() throws Exception {

		setCordInfoList(getCbuAttachInfo(cdrCbuPojo.getCdrCbuId()));

		//log.debug("LIST VAlues ******" + getCordInfoList());
		return "reviewData";
	}

	public String getReviewScreen() {

		return "success";
	}

	public List getCbuAttachInfo(String cbuid) throws Exception {
		return reviewUnitReportController.getCbuAttachInfo(cbuid);

	}

	public void getCbuInfo() throws Exception {
				
		if (getCbuId() != null && getCbuId() != "") {
			cbuId = getCbuId();
		}
		if (cbuId != null || cbuId != "") {
			try {

				cdrCbu = (CdrCbu) linkController.getCbuInfo(CDRCBU_OBJECTNAME,
						cbuId, CORD_CDR_CBU_ID);
				
				setCdrCbuPojo((CdrCbuPojo) ObjectTransfer.transferObjects(
						cdrCbu, cdrCbuPojo));
				

			} catch (Exception e) {
				log.error(e.getMessage());
				e.printStackTrace();
			}
		}

	}

	public String getReviewUnitReportHome() {
		try{
		Long pkCordExtInfo = Long.parseLong(request
				.getParameter("pkCordExtInfo"));
		Long pkCordHlaExt = Long
				.parseLong(request.getParameter("pkCordHlaExt"));
		List<CBUUnitReportTempPojo> list = reviewUnitReportController
				.getCordExtendedTempInfoData(pkCordExtInfo);
		List<CordHlaExtTempPojo> hlaTempList = reviewUnitReportController
				.getCordExtendedHlaTempData(pkCordHlaExt);

		setCordHlaExtTempPojo1(hlaTempList.get(0));
		setCordHlaExtTempPojo2(hlaTempList.get(1));
		setCbuUnitReportTempPojo1(list.get(0));
		setCbuUnitReportTempPojo2(list.get(1));
		
		}catch(Exception e){
			//log.debug("Exception for-->"+e);
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String updateReviewUnitReportHome() throws Exception {

		CBUUnitReportTempPojo cbuUnitReportTempPojo = new CBUUnitReportTempPojo();
		CordHlaExtTempPojo cordHlaExtPojo = new CordHlaExtTempPojo();

		cbuUnitReportTempPojo1 = getCbuUnitReportTempPojo1();
		cbuUnitReportTempPojo2 = getCbuUnitReportTempPojo2();
		cordHlaExtTempPojo1 = getCordHlaExtTempPojo1();
		cordHlaExtTempPojo2 = getCordHlaExtTempPojo2();
		
		if (cbuUnitReportTempPojo1.getCordViabPostProcess() == cbuUnitReportTempPojo2
				.getCordViabPostProcess()) {
			cbuUnitReportTempPojo.setCordViabPostProcess(cbuUnitReportTempPojo1
					.getCordViabPostProcess());
		}

		if (cbuUnitReportTempPojo1.getCordFungalCul() == cbuUnitReportTempPojo2
				.getCordFungalCul()) {
			cbuUnitReportTempPojo.setCordFungalCul(cbuUnitReportTempPojo1
					.getCordFungalCul());
		}
		if (cbuUnitReportTempPojo1.getCordBacterialCul() == cbuUnitReportTempPojo2
				.getCordBacterialCul()) {
			cbuUnitReportTempPojo.setCordBacterialCul(cbuUnitReportTempPojo1
					.getCordBacterialCul());
		}

		if (cbuUnitReportTempPojo1.getCordHemogScreen() == cbuUnitReportTempPojo2
				.getCordHemogScreen()) {
			cbuUnitReportTempPojo.setCordHemogScreen(cbuUnitReportTempPojo1
					.getCordHemogScreen());
		}

		if (cbuUnitReportTempPojo1.getCordProcMethod() == cbuUnitReportTempPojo2
				.getCordProcMethod()) {
			cbuUnitReportTempPojo.setCordProcMethod(cbuUnitReportTempPojo1
					.getCordProcMethod());
		}

		if (cbuUnitReportTempPojo1.getCordTypeBag() == cbuUnitReportTempPojo2
				.getCordTypeBag()) {
			cbuUnitReportTempPojo.setCordTypeBag(cbuUnitReportTempPojo1
					.getCordTypeBag());
		}

		if (cbuUnitReportTempPojo1.getCordProdModification() == cbuUnitReportTempPojo2
				.getCordProdModification()) {
			cbuUnitReportTempPojo
					.setCordProdModification(cbuUnitReportTempPojo1
							.getCordProdModification());
		}

		if (cbuUnitReportTempPojo1.getCordAddNotes().equalsIgnoreCase(cbuUnitReportTempPojo2
				.getCordAddNotes())) {
			cbuUnitReportTempPojo.setCordAddNotes(cbuUnitReportTempPojo1
					.getCordAddNotes());
		}

		if (cordHlaExtTempPojo1.getCordHlaTwice().equalsIgnoreCase(cordHlaExtTempPojo2
				.getCordHlaTwice())) {
			cordHlaExtPojo.setCordHlaTwice(cordHlaExtTempPojo1
					.getCordHlaTwice());
			
			
		}
		if (cordHlaExtTempPojo1.getCordHlaContiSeg().equalsIgnoreCase(cordHlaExtTempPojo2
				.getCordHlaContiSeg())) {
			cordHlaExtPojo.setCordHlaContiSeg(cordHlaExtTempPojo1
					.getCordHlaContiSeg());
		}
		if (cordHlaExtTempPojo1.getCordHlaContiSegBefRel().equalsIgnoreCase(cordHlaExtTempPojo2
				.getCordHlaContiSegBefRel())) {
			cordHlaExtPojo.setCordHlaContiSegBefRel(cordHlaExtTempPojo1
					.getCordHlaContiSegBefRel());
	
		}
		if (cordHlaExtTempPojo1.getCordHlaIndBefRel().equalsIgnoreCase(cordHlaExtTempPojo2
				.getCordHlaIndBefRel())) {
			cordHlaExtPojo.setCordHlaIndBefRel(cordHlaExtTempPojo1
					.getCordHlaIndBefRel());
			
		}
		if (cordHlaExtTempPojo1.getCordHlaContiSegNum() == cordHlaExtTempPojo2
				.getCordHlaContiSegNum()) {
			cordHlaExtPojo.setCordHlaContiSegNum(cordHlaExtTempPojo1
					.getCordHlaContiSegNum());
			
		}
		
		String username = request.getParameter("username");
		cbuUnitReportTempPojo.setCordCompletedBy(username);
		cbuUnitReportTempPojo.setFkCordExtInfo(cbuUnitReportTempPojo1.getFkCordExtInfo());
		cordHlaExtPojo.setFkCordHlaExt(cordHlaExtTempPojo1.getFkCordHlaExt());

		cbuUnitReportTempPojo = reviewUnitReportController
				.addReviewUnitReport(cbuUnitReportTempPojo);
		
		cordHlaExtPojo = reviewUnitReportController
		.addReviewHlaUnitReport(cordHlaExtPojo);
		
		return SUCCESS;

	}

}
