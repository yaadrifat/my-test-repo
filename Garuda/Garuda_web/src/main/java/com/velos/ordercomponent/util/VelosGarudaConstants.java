package com.velos.ordercomponent.util;

public interface VelosGarudaConstants {
	
	//properties files
	public static final String VELOS_PROPERTIES_FILE_NAME = "";
	public static final String VELOS_GARUDA_URL = "http://172.16.2.60:8080";
	public static final String VELOS_GARUDA_DEFAULT_THEME = "theme";
	
	//-------------------------------------------------------------
	
	public static final String VELOS_EMTRAX_VERSION_TYPE = "VersionDate";
	public static final String VELOS_EMTRAX_VERSION_SUBTYPE = "Version1.3";
	public static final String VELOS_EMTRAX_VERSION1_3_DATE_FORMAT = "dd-MMM-yy";
	
	
	// for ESB messages
	public static final String EMAVEN_USER_LOGNAME="emaven";
	public static final String EMTRAX_USER_LOGNAME="emtrax";
	//for Garuda
	public static final String CORD_INFO_OBJECTNAME="CdrCbu";
	public static final String MAIL_CONFIG_OBJECTNAME = "MailConfig";
	public static final String MAIL_NOTIFICATION_OBJECTNAME = "Notification";
	public static final String GARUDA_USER = "currentUser";
	public static final String APPLICATIONVERSION_PARENTCODE = "appl_ver";
	public static final String USER_ALERT = "alert";
	public static final String USER_ALERT_RESOL="alert_resol";
	public static final String CLINICAL_NOTE_OBJ = "ClinicalNote";
	public static final String ALERT_RESOL_CUST="alert_28";
	public static final String ALERT_RESOL_ALL="alert_27";
	public static final String FORM_RESPONSE_OBJ="FormResponses";
	public static final String FORM_QUESTION_OBJ="FormQuestions";

	//-------------------------------------------------
	
	//Cbu Unit Report
	public static final String CBUUNITREPORT_OBJECT ="CBUUnitReport";
	public static final String CORDHLAEXT_OBJECT ="CordHlaExt";
	//-------------------------------------------------
	//for cdr cbu
	public static final String CDRCBU_OBJECTNAME ="CdrCbu";
	public static final String CORD_CDR_CBU_ID ="cdrCbuId";
	public static final String CORD_CDR_REGISTERY_ID ="registryId";
	public static final String CDRCBU_DOMAINOBJECT = "com.velos.ordercomponent.business.domain.CdrCbu";
	public static final String BABY_GENDER = "gender";
	public static final String CLINICAL_STATUS = "clini_stat";
	public static final String BACTERIAL_STATUS ="bact_cul";
	public static final String FUNGAL_STATUS ="fung_cul";
	public static final String POSITIVE = "posi";
	public static final String NEGATIVE = "negi";
	public static final String NOTDONE = "not_done";
	public static final String RH_TYPE = "rh_type";
	public static final String RH_TYPE_OTHER = "oth";
	public static final String BLOOD_GROUP = "bloodgr";
	public static final String LICENCE_STATUS = "licence";
	public static final String ELIGIBILITY_STATUS = "eligibility";
	public static final String INELIGIBLE_REASON = "ineligible";
	public static final String INCOMPLETE_REASON = "incomplete";
	public static final String INCOMPLETE_STR = "Incomplete";
	public static final String INELIGIBLE_STR = "Ineligible";
	public static final String UNLICENSE_STR = "Unlicensed";
	public static final String NOT_COLLECTED_TO_PRIOR_REASON = "not_appli_prior";
	public static final String NOT_APPLICABLE = "Not Applicable";
	public static final String NOT_APPLICABLE_PRIOR_DATE = "Not Applicable - collected prior to 05/25/2005";
	public static final String INELIGIBLE_MATERNAL_REASON = "mtrn_rsk_ques";
	public static final String INELIGIBLE_PHYSICAL_REASON = "phy_find_ass";
	public static final String INELIGIBLE_INFECTIOUS_REASON = "infe_desc_mrk";
	public static final String INELIGIBLE_OTHER_REASON = "oth_med_rec";
	public static final String UNLICENSE_REASON = "unlicensed";
	public static final String UNLICENSE_REASON_INTERNATIONAL = "international";
	public static final String UNLICENSE_REASON_US_PRIOR = "u.s_prior";
	public static final String UNLICENSE_REASON_DONOR_SCREEN = "u.s_inel_scr";
	public static final String UNLICENSE_REASON_DONOR_TEST = "u.s_inel_test";
	public static final String UNLICENSE_REASON_PRE_LIC = "pre_lice";
	public static final String UNLICENSE_REASON_POST_LIC = "post_lice";
	public static final String UNLICENSE_REASON_INCOMPLETE = "incomplete";
	public static final String UNLICENSE_REASON_NOT_APP = "not_applicable";
	public static final String ELIGIBLE = "eligible";
	public static final String LICENCE = "licensed";
	public static final String CLINICAL_STATUS_DEFFERED = "defer";
	public static final String CLINICAL_STATUS_TEMP_UNAVAIL = "accept";
	public static final String SEARCH_IDS = "search_ids";
	public static final String START = "start";
	public static final String END = "end";
	public static final String CONTAINS = "contain";
	public static final String EXACT = "exact";
	public static final String CBU_IDS = "cbu_ids";
	public static final String CBU_REG_ID = "cbu_reg_id";
	public static final String CBU_LOC_ID = "cbu_loc_id";
	public static final String ADD_ID = "add_id";
	public static final String CBU_ONBAG_ID = "cbu_idonbag";
	public static final String CBU_ISBT_DIN = "cbu_isbtdin";
	public static final String MATERNAL_IDS = "maternal_ids";
	public static final String MAT_REG_ID = "mat_reg_id";
	public static final String MAT_LOC_ID = "mat_loc_id";
	public static final String MAT_ADD_ID = "mat_add_id";
	public static final String CORD_ACCEPTABLE = "cord_acceptance";
	public static final String ACCEPTABLE = "acceptable";
	public static final String CBU_COLLECTION_TYPE = "cbu_col_type";
	public static final String CBU_COLLECTION_TYPE_IU = "cbu_col_inutero";
	public static final String CBU_COLLECTION_TYPE_EU = "cbu_col_exutero";
	public static final String CBU_COLLECTION_TYPE_B = "cbu_both";
	public static final String CBU_COLLECTION_TYPE_U = "cbu_unknown";
	public static final String CBU_DELIV_TYPE = "cbu_del_type";
	public static final String CBU_DELIV_TYPE_V = "cbu_vaginal";
	public static final String CBU_DELIV_TYPE_C = "cbu_c_section";
	public static final String CBU_DELIV_TYPE_U = "cbu_unknwon_del";
	public static final String NOT_ACCEPTABLE = "not_acceptable";
	public static final String TIMING_OF_TEST="timing_of_test";
	//NMDP & OTHERS
	public static final String IND_SPONSOR_NMDP = "ind_spon";
	public static final String IND_SPONSOR_OTHER = "ind_spon_other";
	public static final String IND_SPONSOR_NAME = "ind_spon_name";
	public static final String IND_SPONSOR_NUMBER = "ind_spon_number";
	public static final String OTHER = "other";
	public static final String NMDP = "NMDP";
	public static final String NOT_ACTIVE = "not_act";
	public static final String ACTIVE = "act";
	public static final String CLINIC_CATEGORY = "note_cat";
	public static final String NOTE_ASSESS ="note_assess";
	public static final String ASSESS_NCD ="no_cause";
	public static final String PF_NOTE_CAT = "note_cat_pf";
	public static final String NOTE_REASON = "note_reason";
	public static final String FUNDING_STATUS = "funding_status";
	public static final String FUNDING_STATUS_SUBTYPE_ACT = "active";
	public static final String FUNDING_CATEGORY="fund_cat_code";
	//CBB Defaults
	public static final String SHIPPER = "shipper";
	public static final String FEDEX="fedex";
	public static final String UPS="ups";
	public static final String QUICK="quick";
	public static final String WORLD_COUR="world_cour";
	public static final String LOCAL_ASSIGNMENT_ID = "local_id_assign";
	public static final String NMDP_LOCAL_ASSIGNMENT_ID = "nmdp_cbu_id";
	public static final String USER_DEFINED_LOCAL_ASSIGNMENT_ID = "user_defined";
	public static final String AUTOMATIC_SEQUENCING_LOCAL_ASSIGNMENT_ID = "auto_sequencing";
	public static final String DEFAULT_SHIPPING_PAPER_WRK_LOC = "shping_pwrk_loc";
	public static final String TIME_FORMAT = "time_format"; 
	public static final String DATE_FORMAT = "date_format";
	public static final String  ACCREDITITAON = "accreditation";
	public static final String  CBU_STORAGE = "cbu_storage";
	public static final String  CBU_COLLECTION = "cbu_collection";
	public static final String  SITE_TYPE = "site_type";
	public static final String  CBU = "CBU";
	
	//unit report
	public static final String CORD_VIAB_METHOD ="viab_meth";
	public static final String CFU_POST_METHOD ="cfu_post_meth";
	public static final String HEMO_PATHYSCREEN ="hem_path_scrn";
	public static final String PROCESSING_METHOD="pro_meth";
	public static final String TYPE_OF_BAG="bag_type";
	public static final String PRODUCT_MODIFICATION="product_modi";
	public static final String NUM_OF_SEGMENTS="ship_seg";
	public static final String COMPLETED_STATUS="complete_status";
	public static final String HEMO_PATHY_NOTDONE = "not_done";
	public static final String HEMO_PATHY_BETA = "beta_thalmia";
	public static final String HEMO_PATHY_SICKLE = "sickle_beta";
	public static final String HEMO_PATHY_HEMOSICK = "hemo_sick_dis";
	public static final String HEMO_PATHY_ALPHA = "alp_thal_sev";
	public static final String HEMO_PATHY_MULT = "mult_trait";
	public static final String HEMO_PATHY_UNKNOWN = "unknown";
	public static final String HEMO_ALPHA_TRAIT = "alp_thal_trait";
	public static final String HEMO_NO_DISEASE = "homo_no_dis";
	public static final String HEMO_ALPHA_THAL_SIL = "alp_thal_sil";
	public static final String HEMO_TRAIT = "hemo_trait";
	public static final String HEMO_NORMAL = "normal";
	//Constant For Attachment
	public static final String ENTITY_CBU_CODE_TYPE = "entity_type";
	public static final String ENTITY_CBU_CODE_SUB_TYPE = "CBU";
	public static final String SUBENTITY_CODE_TYPE = "sub_entity_type";
	public static final String SUBENTITY_CODE_SUB_TYPE = "IDM";
	public static final String SUBENTITY_CODE_SUB_TYPE_LAB_SUM = "LAB_SUM";
	public static final String ENTITY_CBB = "CBB";
	public static final String ENTITY_ORDER_CODE_SUB_TYPE = "ORDER";
	public static final String ENTITY_USER_CODE_SUB_TYPE = "USER";
	public static final String ASSESSMENT ="note_assess";
	public static final String NOTES = "NOTES";
	public static final String ATTACHMENT_CODE_TYPE = "attach_type";
	public static final String ATTACHMENT_CODE_SUB_TYPE = "unit_rprt";
	public static final String ATTACHMENT_CODE_SUB_TYPE_TST_REPORT= "test_rprt";
	
	//for open orders
	public static final String ORDER_STATUS_CODE_SUB_TYPE = "open_ordr";
	public static final String CATEGORY_TYPE_CODE_SUB_TYPE ="LAB";
	public static final String VELOSORDERS_OBJECTNAME ="Orders";
	public static final String ORDER_PKORDERID="pk_OrderId";
	public static final String CATEGORY= "doc_categ";	
	public static final String ORDER_PRIORITY= "order_priority";	
	
	public static final String ORDER_ID_PARAM= "ORDERID";	
	public static final String GET_ALL_USER_PARAM= "ALLUSERS";	
	public static final String GET_ALL_FOR_CBB= "CBBLIST";	
	public static final String GET_DATA= "GET_DATA";	
	public static final String GET_COUNT= "GET_COUNT";	
	public static final String PACKAGE_SLIP_ACTIVITY= "genpackageslipbar";	
	
	
	// for doc_category
	
	public static final String UNIT_REPORT_CODESUBTYPE = "unit_rprt_cat";
	public static final String CBU_INFORMATION_CODESUBTYPE = "cbu_info_cat";

	public static final String  LAB_SUMMARY_CODESUBTYPE = "lab_sum_cat";
	public static final String  PRODUCT_TESTING_CODESUBTYPE =" product_test";
	public static final String SAMPLE_TESTING = "sample_test";
	public static final String  OTHER_CODESUBTYPE= "other";
	public static final String  DOC_MODIFY= "U";
	public static final String  DOC_REVOKE= "R";
	
	public static final String  PROCESSING_INFO_CODESUBTYPE = "process_info";

	public static final String INFECT_DIS_MARK_CODESUBTYPE ="infect_mark";
	
	public static final String HEALTH_HISt_SCR_CODESUBTYPE="hlth_scren";
	public static final String MRQ_CODESUBTYPE="mrq";
	public static final String FMHQ_CODESUBTYPE="fmhq";
	public static final String OTHER_CODESUBTYPE_HLTH="other";
	public static final String DOMAIN_OBJ_ACCOUNTFORM = "AccountForms";
	
	public static final String HLA_CODESUBTYPE="hla";
	public static final String CBU_CODESUBTYPE="cbu";
	public static final String MATERNAL_CODESUBTYPE="maternal";
	public static final String PATIENT_CODESUBTYPE="patient";
	
	public static final String ELIGIBILITY_CODESUBTYPE="eligibile_cat";
	public static final String URG_MED_NEED_CODESUBTYPE="ur_medi_need";
	public static final String FINAL_DEC_OF_ELIG_CODESUBTYPE="f_dec_of_elig";
	public static final String OTHER_ELIG_CODESUBTYPE="other";
	public static final String MATERNAL_RISK_QUESTIONAAIRE="mrq_attach";
	public static final String ATTACHMENTS = "attach";
	public static final String CBUASSESSMENT_CODESUBTYPE="cbu_asmnt_cat";
	
	public static final String AC_ELIGIBILITY_CODESUBTYPE = "ac";
	public static final String BC_ELIGIBILITY_CODESUBTYPE = "bc";
	public static final String CC_ELIGIBILITY_CODESUBTYPE = "cc";
	public static final String DC_ELIGIBILITY_CODESUBTYPE = "dc";
	public static final String NOTIFI_ASSESSMENT_ELIGIBILITY_CODESUBTYPE="cbu_assessment";
	public static final String URG_MED_NEED_ELIGIBILITY_CODESUBTYPE="ur_medi_need";
	public static final String OTHER_REC_CODESUBTYPE="other_rec_cat";
	public static final String OR_RELE_MED_REC_CODESUBTYPE="o_rel_med_rec";
	public static final String PHY_ASSMNT_CODESUBTYPE="phy_assess";
	public static final String OTHER_RECORD_CODESUBTYPE="other";
	
	//New Category added for document upload
	public static final String DOMAIN_CBU_UPLOADINFO="CBUploadInfo";
	public static final String CBU_ASMNT_CODESUBTYPE="cbu_asmnt_cat";
	public static final String SHIPMENT_ITER_CODESUBTYPE="shpmnt_iter_cat";
	public static final String POST_SHPMNT_CODESUBTYPE="post_shpmnt";
	public static final String CBU_RECEIPT_CODESUBTYPE="cbu_receipt";
	public static final String POST_SHPMNT_OTHER_CODESUBTYPE="other";
	//for Document Upload
	public static final String ENTITY_CODE_TYPE="entity_type";
	public static final String ENTITY_CODE_SUB_TYPE="CBU";
	public static final String ATTACHMENT_CODETYPE="attach_type";
	public static final String ATTACHMENT_CODE_SUBTYPE ="test_rprt";
	public static final String CATEGORY_NAME ="IDMs and OtherTest Results";
	public static final String CATEGORY_CODE_IDM = "infect_mark";
	///----------

	
	//user profile
	public static final String USER_OBJECTNAME ="UserDomain";
	
	public static final String USER_OBJECT = "userDetailsObject";
	
	public static final String CBU_FINAL_REVIEW_OBJECT = "CBUFinalReview";
	
	public static final String CBU_COMPLETE_REQ_INFO_OBJECT = "CBUCompleteReqInfo";
	public static final String TEST_OUTCOMES = "test_outcome";
	public static final String TEST_OUTCOMES1 = "test_outcome1";
	public static final String TEST_SOURCES = "test_source";
	public static final String TEST_NOT_DONE = "not_done";
	public static final String TEST_POSI = "posi";
	public static final String TEST_NEGI = "negi";
	public static final String TEST_Indeter = "indeter";
	
	//-------------------------------------------------
	
	
	public static final String SESSION_BOUND_NAME="P1VUSER";
	public static final String LOGIN_CONCURRENT_CONNECT="C";
	public static final String LOGIN_CONCURRENT_DISCONNECT="D";
	
	public static final String SECURITY_ENABLE_KEY = "SECURITYSERVICE_ENABLE";
	public static final String EXCEPTION_ENABLE_KEY = "EXCEPTIONSTACKTRACE_ENABLE";
	
	public static final String ERROR_FORMNAME = "errorForm";
	// For Study
	public static final String STUDY_OBJECTNAME ="Study";
	public static final String STUDY_DOMAINOBJECT = "com.velos.p1v.business.domain.Study";
	public static final String PERSON_DOMAINOBJECT = "com.velos.p1v.business.domain.Person";
	public static final String ADDRESS_DOMAINOBJECT = "com.velos.p1v.business.domain.Address";
	public static final String DOCUMENTS_DOMAINOBJECT = "com.velos.p1v.business.domain.Documents";
	public static final String RESPONDENT_OBJECTNAME="Person";
	public static final String RESPONDENT_ID="personid";
	
	public static final String ADDRESS_OBJECTNAME="Address";
	public static final String ADDRESS_ID = "addressId"; 
	public static final String DOCUMENT_OBJECTNAME ="Attachment";
	public static final String RECSTATUS_OBJECTNAME ="RecStatus";
	public static final String STUDY_ID ="studyId";	
	public static final String DOCUMENT_ID="attachmentId";
	//Parent Code
	public final String RECSTATUS_PARENTCODE = "RECSTATUS";
	public final String DOCTYPE_PARENTCODE = "DOCTYPE";
	public final String AGEGROUP_PARENTCODE = "AGEGROUP";
	public final String STUDYSTATUS_PARENTCODE = "STUDYSTATUS";
	public final String IRBRECORD_PARENTCODE ="IRBRECORD";
	
	// For Volunteer
	public static final String VOLUNTEER_OBJECTNAME ="Person";
	public static final String PERSON_ID ="personid";
		

	public static final String GUARDIAN_OBJECTNAME ="Guardian";
	public static final String GUARDIAN_ID = "guardianId"; 
	public static final String PERGUARDIAN_ID = "personGuardianId"; 
	public static final String GUARDIANPERSON_ID = "guardPersonId"; 
	
	public static final String MEDICALCONDN_OBJECTNAME ="MedicalCondition";
	public static final String MEDICALCOND_ID = "medicalCondnId"; 
	public static final String DONOTCONTACT_OBJECTNAME ="DonotContact";
	
	
	public static final String VOLUNTEER_PERSONTYPE = "personType";
	public static final String VOLUNTEER_TYPE = "volunteer";
	
	//Parent Code
	public final String RACE_PARENTCODE = "race";
	public final String GENDER_PARENTCODE = "gender";
	public final String MALE = "male";
	public final String FEMALE = "female";
	
	public final String ETHNICITY_PARENTCODE = "ethnicity";	
	public final String CHILDBEARING_PARENTCODE = "CHILDBEARING";
	public final String SMOKER_PARENTCODE = "SMOKER";
	public final String BLACKLISTSTATUS_PARENTCODE = "BLACKSTATUS";
	public final String BLACKTEMP = "BLACKTEMP";
	public final String BLACKPERM = "BLACKPERM";
	
	
	public final String CONSENTMTHD_PARENTCODE = "CONSENTMETHOD";
	public final String RELATIONSHIP_PARENTCODE = "RELATIONSHIP";
	
	public final String MEDICALCONDN_PARENTCODE = "MEDICALCOND";
	public final String REPORTEDSOURCE_PARENTCODE = "REPORTSOURCE";
	
	public final String STATE_PARENTCODE = "STATE";
	public final String PREFCONTACT_PARENTCODE = "PREFCONTACT";
	// end of Volunteer
	//For Respondent
	
	public static final String RESPONDENT_CONTACTACTIVITY="Contactactivity";
	public static final String RESPONDENT_PERSTUDY="Perstudy";
	//Parent Code
	public final String ACTIVITY_PARENTCODE = "ACTIVITY"; 
	public final String REFERRALSOURCE_PARENTCODE = "REFSOURCE";
	public final String PRESCREENOUTCOME_PARENTCODE = "PRESCREENOUTCOME";
	public final String PRESCREENOUTCOME_NOTINTERESTCODE= "NOTINTEREST";
	public final String PRESCREENOUTCOME_INTERESTCODE = "INTEREST";
	public final String PREGNANT_PARENTCODE = "PREGNANT";
	public final String MEDICATIONS_PARENTCODE = "MEDICATIONS";
	public final String FUTURERESEARCH_PARENTCODE = "FUTURERESEARCH";
	//for appointment
	public static final String APPOINTMENT_DOMAINNAME="Appointment";
	public static final String RECRUITACTIVITY="Recruitactivity";
	public static final String CODELIST="CodeList";
	
	//for CBB Procedures
	public static final String STORAGE_METHOD="storage_meth";
	public static final String FREEZER_MANUFACTURER="freezer_manu";
	public static final String BAG_TYPE="bag_type";
	public static final String NUM_OF_BAGS ="num_of_bags";
	public static final String ONE_BAG="1_bag";
	public static final String TWO_BAG="2_bags";
	public static final String OTHER_BAG="other";
	public static final String FROZEN_IN="frozen_in";
	public static final String FROZEN_IN_VIALS="vials";
	public static final String FROZEN_IN_TUBES="tubes";
	public static final String STORAGE_TEMPERATURE="storage_temp";
	public static final String STORAGE_TEMPERATURE_LTONETHIRTYFIVE = ">-135<=80";
	public static final String STORAGE_TEMPERATURE_GTMINUSONETHIRTYFIVE = ">-135";
	public static final String CONTR_RATE_FREEZ="contr_freez";
	public static final String PROCESSING="processing";
	public static final String IF_AUTOMATED="if_automated";
	public static final String PRODUCT_MODI="prod_modi";
	public static final String AUTOMATED="automated";
	public static final String BAGS="bags";	
	public static final String DCMSFLAG="Y";
	public static final String PKDCMSFLAG="Y";
	public static final String ISSYSGENCORDYESRESP="yes";
	//for CORD ENTRY
	public static final String ALL_IDS = "all_ids";
	public static final String CBU_REGIS_ID = "cbu_reg_id";
	public static final String CBU_LOCAL_ID = "cbu_local_id";
	public static final String REGIS_MATER_ID = "regis_mat_id";
	public static final String MATER_LOCAL_ID = "mat_local_id";
	public static final String ISBT_DIN_ID = "isbt_din_id";
	public static final String ADDITIONAL_ID = "additional_id";
	public static final String ETHNICITY = "ethnicity";
	public static final String ETH_HIS = "hispanic";
	public static final String ETH_NHIS = "nonhispanic";
	public static final String ETH_NA = "notreported";
	public static final String RACE ="race";
	public static final String RACE_AFRICA_AME = "AAFA";
	public static final String RACE_AFRICA = "AFB";
	public static final String RACE_SOUTH_ASIAN = "AINDI";
	public static final String RACE_AMERICAN_INDIAN_SO_CENTRAL = "AISC";
	public static final String RACE_ALASKA = "ALANAM";
	public static final String RACE_AMERICAN_INDIAN = "AMIND";
	public static final String RACE_CARIBEAN_BLACK = "CARB";
	public static final String RACE_CARIBEAN_INDIAN = "CARIBI";	
	public static final String RACE_OTHER_WHITE = "CAU";
	public static final String RACE_DECLINES_NO_RACE_SEL = "DEC";
	public static final String RACE_EASTERN_EUROPE = "EEURO";	
	public static final String RACE_FILIPINO = "FILII";
	public static final String RACE_GUAMANIAN = "GUAMAN";
	public static final String RACE_HAWAIAN = "HAWAII";
	public static final String RACE_JAPANESE = "JAPI";	
	public static final String RACE_KOREAN = "KORI";
	public static final String RACE_MEDITERREAN = "MEDIT";	
	public static final String RACE_MIDDLE_EASTERN = "MIDEAS";
	public static final String RACE_NORTH_AMERICA = "NAMER";	
	public static final String RACE_NORTH_COAST_AFRICA = "NCAFRI";
	public static final String RACE_CHINESE = "NCHI";	
	public static final String RACE_NORTH_EUROPE = "NEURO";
	public static final String RACE_OTHER_PACIFIC_ISLAND = "OPI";	
	public static final String RACE_SAMOAN = "SAMOAN";
	public static final String RACE_BLACK_CENT_AMER = "SCAMB";	
	public static final String RACE_SOUTH_EAST_ASIAN = "SCSEAI";
	public static final String RACE_VIETNAMESE = "VIET";
	public static final String RACE_WHITE_CARRIBEAN = "WCARIB";
	public static final String RACE_WESTERN_EUROPE = "WEURO";
	public static final String RACE_WHITE_CENT_AME = "WSCA";
	public static final String RACE_UNSPECIFIED_BLACK = "AFA";
	public static final String RACE_UNSPEC_ASAINPACIFIC_ISLAND = "API";
	public static final String RACE_CARIBBEAN_HISPANIC = "CARHIS";
	public static final String RACE_NORTH_AMERICAN_OR_EUROPEAN = "EURWRC";
	public static final String RACE_HAWAIIAN_OR_PACIFIC_ISLANDER="HAWI";
	public static final String RACE_MIDEAST_NO_COAST_OF_AFRICA="MENAFC";
	public static final String RACE_MEXICAN_OR_CHICANO="MSWHIS";
	public static final String RACE_UNSPECIFIED_NATIVE_AMERICAN="NAM";
	public static final String RACE_SOUTH_CNTRL_AMER_HISP="SCAHIS";
	public static final String RACE_UNKNOWN_QUESTION_NOT_ASKED="UNK";
	public static final String RACE_HISPANIC="HIS";
	public static final String RACE_OTHER_RETIRED="OTH";
	
	public static final String ISBT_CODE = "isbt_code";
	public static final String SPEC_TYPE = "specimen_type";
	public static final String CBU_SPEC_TYPE = "cbu_spec_type";
	public static final String MAT_SPEC_TYPE = "mat_spec_type";
	public static final String SPEC_SUB_TYPE = "cbu";
	//for HLA
	public static final String HLA_LOCUS = "hla_locus";
	public static final String HLA_METHOD = "hla_meth";
	public static final String HLA_A = "HLA-A";
	public static final String HLA_B = "HLA-B";
	public static final String HLA_DRB1  = "HLA-DRB1";
	public static final String HLA_A_ST = "hla_a";
	public static final String HLA_B_ST = "hla_b";
	public static final String HLA_DRB1_ST  = "hla_drb1";
	public static final String CORD_ENTITY_TYPE = "entity_type";
	public static final String CORD_ENTITY_SUB_TYPE = "CBU";
	public static final String PATIENT_ENTITY_SUB_TYPE = "PATIENT";
	public static final String HLA_C_ST = "hla_c";
	public static final String HLA_DRB3_ST = "hla_drb3";
	public static final String HLA_DRB4_ST = "hla_drb4";
	public static final String HLA_DRB5_ST = "hla_drb5";
	public static final String HLA_DQB1_ST = "hla_dqb1";
	public static final String HLA_DPB1_ST = "hla_dpb1";
	public static final String HLA_DPA1_ST = "hla_dpa1";
	public static final String HLA_HLA1_ST = "hla_dqa1";
	//for PendingOrders
	public static final String ORDER_TYPE_NEW="order_type";
	public static final String COL_NAMES="er_order_col";
	public static final String ALL_ORDERS="ALL";
	public static final String ORDER_DATE="ORDER_DATE";
	public static final String CORD_STATUS="cord_status";	
	public static final String CANCELLED="CANCELLED";
	public static final String OR_ORDER="OR";
	public static final String ORDERS_OBJECTNAME="Orders";
	public static final String CTORDER="CT";
	public static final String HEORDER="HE";
	public static final String ORORDER="OR";
	public static final String ORDER_TYPE_ALL="order_type_al";
	
	public static final String RESOLUTION_CODE="resolution_code";
	public static final String TEMPORARILY_UNAVAILABALE="Temporarily Unavailable";
	public static final String TU_SUBTYPE="TU";
	public static final String CA_SUBTYPE="CA";
	public static final String CBU_SAMPLE_TYPE="cbu_sample_type";
	public static final String ALIQUOTS_TYPE="aliquots_type";
	public static final String ALIQUOTS="ALQT";	
	public static final String ORDERSPOJO_OBJECTNAME="OrderPojo";
	public static final String SHIPMENTPOJO_OBJECTNAME="ShipmentPojo";
	public static final String RECIPIENTINFO_OBJECTNAME="RecipientInfoPojo";
	public static final String RECIPIENTINFO_DOMAIN_OBJECT="RecipientInfo";
	public static final String REQUEST_OBJECTNAME="RequestPojo";
	public static final String RECIPIENT_ENTITY_SUB_TYPE="RECEIPANT";
	public static final String RESERVED_FOR_NMDP="RSN";
	public static final String RESERVED="RESERVED";
	public static final String SHIPPING_CONTAINER="ship_cont";
	public static final String TEMP_MONITOR="temp_moniter";
	public static final String SHIPMENT_OBJECTNAME="Shipment";
	public static final String SHIPMENT_TYPE="shipment_type";
	public static final String LAB_CODE="lab_code";
	
	//Order status list	
	public static final String ORDER_STATUS="order_status";
	public static final String NEW="NEW";
	public static final String PENDING="PENDING";
	public static final String SHIP_SCH="SHIP_SCH";
	public static final String AWA_HLA_RES="AWA_HLA_RES";
	public static final String HLA_RES_REC="HLA_RES_REC";
	public static final String RESOLVED="resolved";
	public static final String CLOSED="close_ordr";
	public static final String CORD_SHIPPED="cordshipped";
	public static final String AVAILABLE="AV";
	public static final String NOT_AVAILABLE="NA";
	public static final String FORM_TYPE = "form";
	public static final String FILE_TYPE = "file";
	public static final String LAB_NOTE_CATEGORY_CODESUBTYPE= "lab_sum";
	public static final String HLT_NOTE_CATEGORY_CODESUBTYPE= "hlt_his_scrn";
	public static final String POST_NOTE_CATEGORY_CODESUBTYPE= "post_ship";
	public static final String ADHOC_NOTE_CATEGORY_CODESUBTYPE= "adhoc";
	public static final String ID_CATEGORY_CODESUBTYPE= "id";
	public static final String OTHER_NOTE_CATEGORY_CODESUBTYPE= "other_rec";
	public static final String HLA_NOTE_CATEGORY_CODESUBTYPE="hla";
	public static final String ELIGIBLE_NOTE_CATEGORY_CODESUBTYPE= "eligiblity";
	public static final String PROCESS_NOTE_CATEGORY_CODESUBTYPE= "process_info";
	public static final String INFCT_NOTE_CATEGORY_CODESUBTYPE= "infct_dis_mark";
	public static final String CBU_NOTE_CATEGORY_CODESUBTYPE= "cbu_info";
	public static final String AVAIL_CNFRM_CODESUBTYPE= "avail_cnfrm";
	public static final String CT_SHHIPMENT_CODESUBTYPE = "ct_ship";
	public static final String CBU_SHHIPMENT_CODESUBTYPE ="cbu_ship";
	public static final String RESOLUTION_CODESUBTYPE = "resolution";
	public static final String CBU_STATUS_CODESUBTYPE = "cbu_stat";
	public static final String RESEARCH_SMP_CODESUBTYPE = "research_smp";
	public static final String OTHER_PF_CODESUBTYPE ="other";
	public static final String NOTE_TYPE= "note_type";
	public static final String NOTE_CAT= "note_cat";
	public static final String POST_SHIP= "post_ship";
	public static final String ORDER_TYPE= "order_type";
	public static final String NOTE_TYPE_CODE_SUB_TYPE_CLINIC= "clinic";
	public static final String NOTE_TYPE_CODE_SUB_TYPE_NON_CLINIC= "non_clinic";
	public static final String NOTE_TYPE_CODE_SUB_TYPE_PROGRESS = "progress";
	public static final String MILE_STONE="order_milestone";
	
	public static final String MS_CORDSHIPPED="cordshipped";
	public static final String MS_NEW="new";
	public static final String MS_RESERVED="reserved";
	public static final String MS_SHIPMENTSCH="shipmentsch";
	public static final String MS_SAMPLESHIPPED="sampleshipped";
	public static final String MS_AWAITING_HLA_RES="awaithlares";
	public static final String MS_HLA_RES_RECE="hlaresreceived";
	public static final String MS_RESOLVED="Resolved";
	public static final String MS_COMPLETE="Complete";
	public static final String TSK_CBUAVAILCONFIRM="cbuavailconfbar";
	public static final String TSK_REQ_CLINICAL_INFO="reqclincinfobar";
	public static final String TSK_FINAL_CBU_REVIEW="finalcbureviewbar";
	public static final String TSK_SHIPMENT_INFO="ctshipmentbar";
	public static final String TSK_GEN_PACKAGE_SLIP="genpackageslipbar";
	public static final String TSK_ADDITI_TYPING="additityingandtest";
	public static final String TSK_SHIPMENT_CONFIRM="cbushipmentbar";
	public static final String TSK_SAMPLE_SHIPMENT="shipsamplebar";
	public static final String TSK_COMPLETESCTOD="completesctodbar";
	public static final String TSK_ACKNOWLEDGE_RESOL="ctresolutionbar";
	public static final String TSK_SHIPSAMPLE="shipsamplebar";
	public static final String CORD_UNAVAIL_REASON="cord_unavail_reason";
	public static final String CORD_UNAVAIL_RSN="cord_unavailrsn";
	public static final String PAPERWORK_LOCATION="paperwork_loc";
	
	//for IDM Test
	public static final String IDM_VALUES="test_outcome";
	public static final String IDM_SUBQ_RES="test_outcome2";
	public static final String ER_PATLABS="PatLabs";
	public static final String IDMTEST_SUBQUES="idmtest_subques";
	public static final String IDMTEST_SUBQUES_YES="yes";
	public static final String IDMTEST_SUBQ_N="no";
	public static final String IDMTEST_SUBQUES_NO="none";
	public static final String IDMTEST_SUBQUES_SOME="some";
	public static final String IDMTEST_QUES="idm_sub_ques";
	public static final String IDMTEST_SUBQUES1="subques1";
	public static final String IDMTEST_SUBQUES2="subques2";
	//for Minimum Criteria
	public static final String MINIMUM_CRITERIA_OBJECTNAME="CBUMinimumCriteria";
	public static final String MINIMUM_CRITERIA_TEMP_OBJECTNAME="CBUMinimumCriteriaTemp";
	public static final String MINIMUM_CRITERIA_DECLARATION="minimum_decl";
	public static final String MINIMUM_DECL_SUBTYPE="doesntmeets";
	public static final String MINIMUM_DECL_SUBTYPE1="meets";
	public static final String MINIMUM_FIELDS="minimum_field";
	public static final String BACTERIAL_STATUS_NT ="not_done";
	public static final String BACTERIAL_STATUS_POS ="posi";
	public static final String FUNGAL_STATUS_POS="posi";
	public static final String HEMO_GLOBI_HOMO ="homozygous";
	public static final String HEMO_GLOBI_HETRO ="hetrozygous";
	public static final String HEMO_GLOBI_BETA_THALMIA="beta_thalmia";
	public static final String HEMO_GLOBI_SICKLE_BETA="sickle_beta";
	public static final String HEMO_GLOBI_SICK_DIS="hemo_sick_dis";
	public static final String HEMO_GLOBI_ALP_THAL_SEV="alp_thal_sev";
	public static final String HEMO_GLOBI_MULT_TRAIT="mult_trait";
	public static final String HEMO_GLOBI_NEGI="negi";
	public static final String IDM_POS="posi";
	public static final String MINICRITE_STATUS="no";
	public static final String VAIBILITY_SHN ="VIAB";
	public static final String TCNCC_SHN ="TCNCC";
	public static final String FNPV_SHN ="FNPV";
	public static final String UNCRCT_SHN ="UNCRCT";
	public static final String UNIFUNCRCT_SHN="TCBUUN";
	public static final String VIAB_DECLARE="viab_dec";
	
	//FORM RESPONSES
	public static final String DROPDOWN_FIELD="dropdown";
	public static final String TEXT_FIELD="textfield";
	public static final String MULTISELECT_FIELD="multiselect";
	public static final String LABEL="label";
	public static final String MRQ_RES1="mrq_res1";
	public static final String FMHQ_RES1="fmhq_res1";
	public static final String FMHQ_RES3="fmhq_res3";
	public static final String FMHQ_RES2="fmhq_res2";
	public static final String FMHQ_CNCR_TYPE="cncr_type";
	public static final String FMHQ_OTSKINCNCR="otskincncr";
	public static final String FMHQ_METAB_DIS="metab_dis";
	public static final String FMHQ_METAB_OTHER="storgdis";
	
	public static final String IDM_TEST_OUTCOME1="test_outcome1";
	public static final String IDM_TEST_OUTCOME="test_outcome";	
	//eResearch Forms
	public static final String FORM_LAB_SUMMARY = "labSummary";
	public static final String FORM_MRQ = "MRQ";
	public static final String FORM_PHYS_FIND_ASSESS = "physicalFindingAssessment";
	public static final String FORM_INF_DIS_MARK = "infecDeseaseMarkers";
	public static final String FORM_OTH_MED_REC = "otherMedRec"; 
	public static final String FORM_CBU_ASSESSMENT = "cbuAssessment";
	public static final String FORM_FMHQ = "FMHQ";
	public static final String FORM_IDM = "IDM";
	public static final String TEST_DATE = "Test Date";
	public static final String EMPTY_STRING = "";

	public static final String TEST_REASON="test_reason";
	public static final String TEST_REASON_OTHER="other";
	public static final String TEST_METHOD="test_method";	
	
	//For multiple values related to entity
	public static final String MULTIPLE_RACE = "race";
	public static final String MULTIPLE_CBU_ON_BAG = "all_ids";
	//For other Lab tests
	public static final String OTHER_LAB_TESTS = "Other";
	public static final String LAB_GROUP_TYPE = "PC";
	
	//for application messages
	public static final String APPMESSAGES_OBJECTNAME = "AppMessages";
	public static final String APPMSG_STATUS = "app_msg_status";
	public static final String APPMSG_STATUS_INACTIVE = "inactive";
	public static final String APPMSG_STATUS_ACTIVE_ALLWAYS = "active_always";
	public static final String DATE_FORMAT_MMM_dd_YYYY = "MMM dd,yyyy";
	public static final String PAGE_TYPE = "page_type";
	public static final String LOGIN_PAGE_TYPE = "login_page";
	public static final String LANDING_PAGE_TYPE = "landing_page";
	
	//for Assessment 
	public static final String ASSESSMENT_OBJECTNAME = "Assessment";
	
	//For Race Roll Up(HRSA)
	public static final String RACE_ROLLUP = "race_roll_up";
	//For NMDP Broad Race
	public static final String NMDP_BROAD_RACE = "nmdp_broad_race";
	
	public static final String ORDER_TYPE_ADVLOOKUP = "order_type_al";
	public static final String ORDER_TYPE_CT_SHIP = "CT - Ship Sample";
	public static final String ORDER_TYPE_CT_LAB = "CT - Sample at Lab";
	public static final String CBB_CT = "CBB_CT";
	public static final String CBB_HE = "CBB_HE";
	public static final String CBB_OR = "CBB_OR";
	public static final String CBUSTATUS_OBJECTNAME = "CBUStatus";
	public static final String NOTE_VISIBILITY = "note_visible";
	public static final String CODE_TYPE = "code_type";
	public static final String CBU_STATUS_CODE = "cbu_status_code";
	public static final String RESOLUTION = "Resolution";
	public static final String RESPONSE = "Response";
	public static final String CLOSEREASON = "CloseReason";
	public static final String RO = "RO";
	public static final String AV = "AV";
	public static final String AG = "AG";
	public static final String CA = "CA";
	public static final String DT = "DT";
	public static final String NR = "NR";
	public static final String CT = "CT";
	public static final String CT_SHIP_SAMPLE_AT_LAB = "CT - Sample at Lab";
	public static final String CT_SHIP_SAMPLE = "CT - Ship Sample";
	public static final String HE = "HE";
	public static final String OR = "OR";
	public static final String SA = "SA";
	public static final String SB = "SB";
	public static final String SC = "SC";
	public static final String SD = "SD";
	public static final String SE = "SE";
	public static final String NA = "NA";
	public static final String AC = "AC";
	public static final String DD = "DD";
	public static final String QR = "QR";
	public static final String CD = "CD";
	public static final String OT = "OT";
	public static final String SO = "SO";	
	public static final String DS = "DS";
	public static final String LS = "LS";
	public static final String TS = "TS";
	public static final String CB = "CB";
	public static final String RESOLUTION_CBB = "resolution_cbb";
	public static final String RESOLUTION_TC = "resolution_tc";
	public static final String SH="SH";
	public static final String RSN="RSN";
	public static final String IN="IN";
	public static final String RSO = "RSO";
	//FOR DATA MODIFICATION REQUEST 
	
	
	public static final String REQUEST_TYPE="request_type";
	public static final String DATA_MODI_REQ="data_modi_req";
	public static final String APPROVAL_STATUS="approval_status";
	public static final String PENDING_STATUS="pending";
	public static final String COMPLETE_STATUS="completed";
	public static final String APPROVED_STATUS="approved";
	public static final String REJECTED_STATUS="rejected";
	
	// Done By Gaurav
	public static final String PRE_TEST  ="pre_procesing";
	public static final String POST_TEST ="post_procesing";
	public static final String OTHER_TEST ="post_proc_thaw";
	public static final String NEW_TEST_DATE ="new_test_date";
	
	public static final String OTHERFIRST_TEST ="1";
	public static final String OTHERSECOND_TEST ="2";
	public static final String OTHERTHIRD_TEST ="3";
	public static final String OTHERFOURTH_TEST ="4";
	public static final String OTHERFIFTH_TEST ="5";
	public static final String OTHERZEROETH_TEST="0";
	
	public static final String IS_LOCAL_REG="IS_LOCAL_REG";
	
	//FOR THE VALUE IN DROP DOWN
	public static final String CFU_COUNT ="cfu_cnt";
	public static final String CFU_TOTAL ="total";
	public static final String CFU_GM ="gm";
	public static final String CFU_OTHER ="other";
	
	public static final String VAIBILITY ="vaibility";
	public static final String VAIBILITY_7AADT ="7aadt";
	public static final String VAIBILITY_ACRI_PROP ="acri_prop";
	public static final String VAIBILITY_EHI_BROM ="ehi_brom";
	public static final String VAIBILITY_TRY_BLUE  ="try_blue";
	public static final String VAIBILITY_OTHER  ="other";
	
	
	
	//FOR THE VALUE SAVED
	public static final String CFU_COUNT_VAL ="CFU Count";
	public static final String VAIBILITY_VAL ="Viability";
	
	public static final String CFU_COUNT_SUBTYPE ="other";
	public static final String VIABILITY_SUBTYPE ="other";
	public static final String ORDERSERVICES_OBJECTNAME="OrderServices";
	
	public static final String OTHER_CFU_COUNT_VAL ="Other CFU Count";
	
	//FOR LAB GROUP
	public static final String IOG="IOG";
	public static final String OLBT="OLBT";
	
	//For Set the name of view 
	public static final String CORD_ROW_LEVEL = "cordrowlevel";
	public static final String ENTITY_STATUS_ROW_LEVEL = "entitystatusrowlevel";
	public static final String ENTITY_STATUS_REASON_ROW_LEVEL = "entitystatusreasonrowlevel";
	public static final String MULTIPLE_VALUE_ROW_LEVEL = "multiplevaluerowlevel";
	public static final String CORD_COLUMN_LEVEL = "cordcolumnlevel";
	public static final String ENTITY_STATUS_COLUMN_LEVEL = "entitystatuscolumnlevel";
	public static final String ENTITY_STATUS_REASON_COLUMN_LEVEL = "entitystatusreasoncolumnlevel";
	public static final String MULTIPLE_VALUE_COLUMN_LEVEL = "multiplevaluecolumnlevel";
	public static final String REVIEW_ROW_LEVEL ="finalcbureviewrow";
	public static final String HLA_ROW_LEVEL ="hlarow";
	public static final String PATHLA_ROW_LEVEL ="pathlarow"; 
	public static final String LABSUMMARY_ROW_LEVEL ="labsummaryrow";
	public static final String NOTES_ROW_LEVEL ="notesrow";
	public static final String FORMS_ROW_LEVEL_IDM ="formsrow";
	public static final String FORMS_ROW_LEVEL_HHS ="formsrowhhs";
	public static final String ID_ROW_LEVEL = "idrow";
	public static final String PROC_INFO_ROW_LEVEL = "procinforow";
	public static final String MIN_CRIT_INFO_ROW_LEVEL ="mininforow";
	public static final String PF_ROW_LEVEL ="pfrow";
	public static final String BEST_HLA_ROW_LEVEL ="besthlarow";
	public static final String MODULE_CBBPROCEDURE ="PROCEDURE";
	public static final String MODULE_PENDING ="PENDING";
	public static final String MODULE_LANDING ="LANDING";
	public static final String FILTER_MODULE ="filter_module";
	public static final String PENDING_SCREEN ="pending_screen";
	public static final String SUBMODULE_PENDING ="ps_submodule";
	public static final String SUBMODULE_PENDING_ORDERS ="active_request";
	public static final String SUBMODULE_PENDING_NACBU ="not_avail_cbu";
	public static final String SUBMODULE_PENDING_SIP ="saved_in_prog";
	public static final String SUBMODULE_LANDING ="LANDING";
	public static final String LANDING_PAGE ="landing_page";
	public static final String FILTER_TYPE ="filter_type";
	public static final String CUSTOM_FILTER ="custom_filter";
	public static final String REPLACE_STR ="----";
	public static final String REPLACE_STR2 ="&";
	
	
	public static final String SAMP_TYPE = "samp_type";
	public static final String SWITCH_WORFLOW_RSN="switchwf_rsn";
	public static final String SWITCH_TO="switch_to";
	public static final String SWITCH_TO_LAB="lab";
	public static final String SWITCH_TO_SHIP="ship";
	public static final String IND_PROTOCOL="ind_protocol";
	public static final String IND_PROTOCOL_NO="no";
	
	public static final String CORD_IMPORT_ERROR = "Error";
	public static final String CORD_IMPORT_WARNING = "Warning";
	public static final String CORD_IMPORT_INFORMATION = "Information";
	public static final String CORD_IMPORT_FAILURE = "Failure";
	public static final String CORD_IMPORT_ZERO_LEVEL_SUCCESS = "CBU was imported successfully!";
	public static final String CORD_IMPORT_ZERO_LEVEL_ERROR = "CBU not imported!";
	
	public static final String SORT_ASC="SORT_ASC";
	public static final String SORT_DES="SORT_DES";
	
	public static final Integer MESSAGE_LEVEL_ZERO = 0;
	public static final Integer MESSAGE_LEVEL_ONE = 1;
	
	public static final Integer CORD_IMPORT_STATUS_INSERT = 1;
	public static final Integer CORD_IMPORT_STATUS_UPDATE = 2;
	public static final Integer CORD_IMPORT_STATUS_NOTSAVE = 3;
	
	public static final String ISBT_REF_CODE = "001";
	
	// START For Lab Summary 
	//public static final String CBUVOLUME_CBUVOL="CBU_VOL";  Viability
	public static final String CBU_VOL ="CBU_VOL";
	public static final String TOT_CBUNUCL ="TCNCC";
	public static final String CBUNUC_CECNT="CBUNCCC";
	public static final String FIN_VOL="FNPV";
	public static final String CBU_UNKNWN ="TCBUUN";
	public static final String CBU_UNCRCT="UNCRCT";
	public static final String TOTCD34_CECNT="TCDAD";
	public static final String PERCD34="PERCD";
	public static final String PERCD34_MONO="PERTMON";
	public static final String PERMONO_TOTNUC="PERTNUC";
	public static final String TOTCD3="TOTCD";
	public static final String PERCD3="PERCD3";
	public static final String CBUNRBC_ABS="CNRBC";
	public static final String PERCBUNRBC ="PERNRBC"; 
	public static final String  CFU_CNT="CFUCNT";
	public static final String  VIABILITY="VIAB";
	public static final String MAX_30="30";
	public static final String MAX_100="100";
	//END For Lab Summary 
	
	//Pagination Module Constants
	public static final String MODULE_VIEW_CLINICAL = "viewClinical";
	public static final String MODULE_VIEW_LOOKUP = "advanceLookUp";
	public static final String MODULE_SAVE_IN_PROGRESS = "saveInProgress";
	public static final String MODULE_NOT_AVAILABLE_CBUS = "notAvailableCbus";
	public static final String TNCFROZEN = "Total CBU Nucleated Cell Count (unknown if uncorrected)";
	public static final String TNCFROZENNXT ="Total CBU Nucleated Cell Count (uncorrected)";
	public static final String TNCTEST="Total CBU Nucleated Cell Count";
	public static final String VIABILITYTEST="Viability";
	public static final String MODULE_QUERY_BUILDER = "queryBuilder";
	//Div name to refersh particular div on notes and document
	public static final String ID_DIV = "loadIdClinicalNoteDiv";
	public static final String CBUCHARACTERISTIC_DIV = "loadCbuCharacteristicClinicalNoteDiv";
	public static final String LABSUMMARY_DIV = "loadLabSummaryClinicalNoteDiv";
	public static final String ELIGIBILITY_DIV = "loadEligibilityClinicalNoteDiv";
	public static final String IDM_DIV = "loadIdmClinicalNoteDiv";
	public static final String OTHERS_DIV = "loadOthersClinicalNoteDiv";
	public static final String PROCESSING_DIV = "loadProcessingProcedureClinicalNoteDiv";
	public static final String HLA_DIV = "loadHlaClinicalNoteDiv";
	public static final String HEALTH_HISTORY ="loadHealthHistoryDiv";
	public static final String FORMS_DIV = "loadFormsDataDiv";
	public static final String WHOLENOTES_DIV = "loadWholeNotesDiv";
	public static final String CORD_MODULE= "CORD_PAGE";
	public static final String ORDER_MODULE= "ORDER_PAGE";
	public static final String UPDATE_MODULE= "UPDATE_MODULE";
	
	//DOC Div name to refersh particular div on notes and document
	public static final String DOC_CBUCHARACTERISTIC_DIV = "loadCbuDocInfo";
	public static final String DOC_LABSUMMARY_DIV = "loadLabSummaryDocInfo";
	public static final String DOC_ELIGIBILITY_DIV = "loadEligibilityDocInfo";
	public static final String DOC_IDM_DIV = "loadIdmDocInfo";
	public static final String DOC_OTHERS_DIV = "loadOthersDataDiv";
	public static final String DOC_PROCESSING_DIV = "loadProcessingDocInfo";
	public static final String DOC_HLA_DIV = "loadHlaDocInfo";
	public static final String DOC_FORMS_DIV = "loadHealthHistroyDocInfo";
	public static final String ALL_SITE = "ALLSITE";
	public static final String ALL_OPTION = "ALL";
	
	
	//back to search results
	
	public static final String BACKTOSEARCHRES="backToSearchResFlag";
	
	//Report Number and Version Number
	public static final String CBUSUMMARY_REPNO = "CBUSummaryReportNo";
	public static final String CBUSUMMARY_VERNO = "CBUSummaryVersionNo";
	public static final String DETAILCBU_REPNO = "DetailCBUReportNo";
	public static final String DETAILCBU_VERNO = "DetailCBUVersionNo";
	public static final String CBUCOMP_REPNO = "CBUComprehensiveReportNo";
	public static final String CBUCOMP_VERNO = "CBUComprehensiveVersionNo";
	public static final String CBUEVALHIST_REPNO = "CBUEvaluationHistoryReportNo";
	public static final String CBUEVALHIST_VERNO = "CBUEvaluationHistoryVersionNo";
	public static final String FDOE_REPNO = "FDOEReportNo";
	public static final String FDOE_VERNO = "FDOEVersionNo";
	public static final String DUMN_REPNO = "DUMNReportNo";
	public static final String DUMN_VERNO = "DUMNVersionNo";
	public static final String CBUASSESSMENT_REPNO = "CBUAssessmentReportNo";
	public static final String CBUASSESSMENT_VERNO = "CBUAssessmentVersionNo";
	public static final String NOTIFYCBUASSESSMENT_REPNO = "NotifyCBUAssessmentReportNo";
	public static final String NOTIFYCBUASSESSMENT_VERNO = "NotifyCBUAssessmentVersionNo";
	public static final String PRODUCTTAG_REPNO = "ProductTagReportNo";
	public static final String PRODUCTTAG_REPNO_VERNO = "ProductTagVersionNo";
	public static final String CBUShipmentPack_REPNO = "CBUEShipmentPacketReportNo";
	public static final String CBUShipmentPack_VERNO = "CBUEShipmentPacketVersionNo";
	public static final String PRODUCTINSERT_REPNO="ProductInsertReportNo";
	public static final String PRODUCTINSERT_VERNO="ProductInsertVersionNo";
	public static final String SHIPMENTPKTIDM_REPNO="ShipPacketIdmReportNo";
	public static final String SHIPMENTPKTIDM_VERNO="ShipPacketIdmVersionNo";
	public static final String SHIPPACKETCBURECIPT_REPNO="ShipPacketCBUReciptReportNo";
	public static final String SHIPPACKETCBURECIPT_VERNO="ShipPacketCBUReciptVersionNo";
	
	//for Pdf documents
	public static final String PDF_PRODUCT_TAG = "productTag";
	public static final String PDF_CBU_ASSESSMENT = "cbuAssessment";
	public static final String PDF_FDOE = "fdoe";
	public static final String PDF_DUMN = "dumn";
	public static final String PDF_NOTIFI_CBU_ASSESS = "notifiCbuAssessment";
	
	public static final String SEQ_CB_HLA_ORDER = "SEQ_CB_HLA_ORDER";
	public static final String SEQ_CB_BEST_HLA_ORDER = "SEQ_CB_BEST_HLA_ORDER";
	
	//Query Builder
	public static final String DATE_RANGE = "date_range";
	public static final String COMPLETE_REQ_INFO="comp_req_info";
	public static final String WORKFLOW_ACTIVITY_STAGE= "workflow_act";
	public static final String NMDP_CBU_QUICKSUMMARY_REPORT= "quicksummaryrpt";
	public static final String NMDP_CBU_IDSUMMARY_REPORT= "idsummaryrpt";
	public static final String NMDP_CBU_ANTIGEN_REPORT= "antigenrpt";
	public static final String NMDP_CBU_LICENSEANDELIGIBILTY_REPORT= "licenselgblerpt";
	public static final String REPORT_CATEGORY ="qury_build_rprt";
	public static final String REPORT_CATEGORY_CBU_SHIP ="quicksummaryrpt";
	public static final String ANTIGEN_REPORT ="ANTIGEN";
	public static final String ID_SUMMARY_REPORT ="ID";
	public static final String QUICKSUMMARY ="QUICKSUMMARY";
	public static final String LICENSELGBLERPT ="LICENSELGBLERPT";
	//Cord Entry WIth Server SIde Validation
	public static final String CORD_ENTRY_EDIT_MODE = "EDIT";
	public static final String CORD_ENTRY_UPDATE_MODE = "UPDATE";
	public static final String CORD_ENTRY_SAVE_MODE = "SAVE";
	public static final String QUERY_BUILD_REPORT = "query_report";
	public static final String QUERY_BUILD_REPORT_SUBMODULE ="rpt_submodule";
	public static final String CBU_QUERIES ="cbu_queries";
	
	//CSV Import
	public static final String NEW_LINE="\r\n";
	public static final String SEPRATOR=",";
	public static final String EMPTY_VALUE="\"\"";
	public static final String WRAPPER="\"";
	public static final String EMPTY="";
	public static final String ASSET_SUBTYPE ="na";
	public static final int MAXROW_XLSX = 1048576;
	
}