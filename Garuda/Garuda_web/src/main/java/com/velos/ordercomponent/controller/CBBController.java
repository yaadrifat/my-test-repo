package com.velos.ordercomponent.controller;


import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.eres.web.user.UserJB;
import com.velos.ordercomponent.business.domain.CBB;
import com.velos.ordercomponent.business.domain.Site;
import com.velos.ordercomponent.business.pojoobjects.AdditionalIdsPojo;
import com.velos.ordercomponent.business.pojoobjects.CBBPojo;
import com.velos.ordercomponent.business.pojoobjects.CBBProcedureInfoPojo;
import com.velos.ordercomponent.business.pojoobjects.CBBProcedurePojo;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.FundingGuidelinesPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.pojoobjects.FundingSchedulePojo;
import com.velos.ordercomponent.business.pojoobjects.GrpsPojo;
import com.velos.ordercomponent.business.pojoobjects.SitePojo;
import com.velos.ordercomponent.service.PendingOrdersService;
import com.velos.ordercomponent.service.VelosService;
import com.velos.ordercomponent.service.impl.PendingOrdersServiceImpl;
import com.velos.ordercomponent.service.impl.VelosServiceImpl;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.domain.CodeList;

/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class CBBController {
	public static final Log log = LogFactory.getLog(CBBController.class);
	VelosService service;
	PendingOrdersService ordersService;
	
	public Map<Long,CBB> getCBBMapWithIds() throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCBBMapWithIds();
	}
	
	public List<SitePojo> getCBBDetails(Long id) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCBBDetails(id);
	}
	
	public List<SitePojo> getCBBDetailsByIds(Long[] ids) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCBBDetailsByIds(ids);
	}
	
	public CBBPojo saveCBBDefaults(CBBPojo cbbPojo ) throws Exception{
		service = VelosServiceImpl.getService();
		return service.saveCBBDefaults(cbbPojo);
	}
	
	public List<CBBProcedurePojo> getCBBProcedure(Long id,String siteIds,PaginateSearch paginateSearch,int i) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCBBProcedure(id,siteIds,paginateSearch,i);
	}
	
	public List<CBBProcedurePojo> getCBBProcedureSort(Long id,String siteIds,PaginateSearch paginateSearch,int i,String filterCondtion,String orderBy) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCBBProcedureSort(id,siteIds,paginateSearch,i,filterCondtion,orderBy);
	}
	
	public List<CBBProcedureInfoPojo> getCBBProcedureInfo(String id) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCBBProcedureInfo(id);
	}
	
	public CBBProcedureInfoPojo saveCBBProcedureDetails(CBBProcedureInfoPojo cbbProcedureInfoPojo ) throws Exception{
		//System.out.println("in cbb controller");
		service = VelosServiceImpl.getService();
		return service.saveCBBProcedureDetails(cbbProcedureInfoPojo);
	}
	public CBBProcedurePojo saveCBBProcedure(CBBProcedurePojo cbbProcedurePojo ) throws Exception{
		//System.out.println("in cbb controller");
		service = VelosServiceImpl.getService();
		return service.saveCBBProcedure(cbbProcedurePojo);
	}
	
	public CBBProcedureInfoPojo updateCbbProcedureInfo(CBBProcedureInfoPojo cbbProcedureInfoPojo, CBBProcedurePojo cbbProcedurePojo) throws Exception{
		
		service = VelosServiceImpl.getService();
		return service.updateCbbProcedureInfo(cbbProcedureInfoPojo,cbbProcedurePojo);
	}
	
	public Long getCodeListPkIds(String parentCode, String code)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCodeListPkIds(parentCode, code);
	}
	
	public List<SitePojo> getSites() throws Exception{
		service = VelosServiceImpl.getService();
		return service.getSites();
	}
	public List<GrpsPojo> getGroups(Long siteId) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getGroups(siteId);
	}
	
	public List getCBBProcedureCount(String siteIds,String procedureCondi,int i) throws Exception{
		service= VelosServiceImpl.getService();
		return service.getCBBProcedureCount(siteIds,procedureCondi,i);
	}
	
	public List getSitesCount(PaginateSearch paginateSearch,int i,String condition) throws Exception{
		service= VelosServiceImpl.getService();
		return service.getSitesCount(paginateSearch,i,condition);
	}
		
	public List<SitePojo> getSitesWithPagination(PaginateSearch paginateSearch,int i) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getSitesWithPagination(paginateSearch,i);
	}
	
	public List<SitePojo> getSitesWithPaginationSort(PaginateSearch paginateSearch,int i, String condition, String orderby) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getSitesWithPaginationSort(paginateSearch,i,condition,orderby);
	}
	
	public List<SitePojo> getSitesByCriteria(String criteria,PaginateSearch paginateSearch,int i) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getSitesByCriteria(criteria, paginateSearch, i);
	}
	
	public CBB getCBBBySiteId(Long siteId) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCBBBySiteId(siteId);
	}
	
	public Long getdefaultshipper(Long orderId,String orderType) throws Exception{
		ordersService=PendingOrdersServiceImpl.getService();
		return ordersService.getdefaultshipper(orderId,orderType);
	}

	public SitePojo getSiteBySiteId(Long siteId) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getSiteBySiteId(siteId);
	}
	
	public SitePojo saveSite(SitePojo sitePojo) throws Exception{
		service = VelosServiceImpl.getService();
		return service.saveSite(sitePojo);
	}
	
	public List<CBBProcedurePojo> getCBBProcedures(Long id) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCBBProcedures(id);
	}
	
	public String checkForProcAssignment(String procId) throws Exception{
		service=VelosServiceImpl.getService();
		return service.checkForProcAssignment(procId);
	}	
	
	public String getProcCordsMaxCollectionDt(String procId) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getProcCordsMaxCollectionDt(procId);
	}	
	
	public List<FundingGuidelinesPojo> getCBBGuidlinesByid(String siteId) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCBBGuidlinesByid(siteId);
	}
	
	public List<FundingGuidelinesPojo> updateCBBGuidlines(List<FundingGuidelinesPojo> fundingGuidelinesPojoLst) throws Exception{
		service = VelosServiceImpl.getService();
		return service.updateCBBGuidlines(fundingGuidelinesPojoLst);
	}
	
	public List<FundingGuidelinesPojo> getValidfundingguidelinesdate(String birthDate,String Category,String cbbId) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getValidfundingguidelinesdate(birthDate,Category,cbbId);
	}
	
	public List<SitePojo> getCBBListForSchAvail() throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCBBListForSchAvail();
	}
	
	public List<FundingSchedulePojo> getScheduledFundingRepLst(List<FundingSchedulePojo> fundingSchedulePojoLst,PaginateSearch paginateSearch,int i) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getScheduledFundingRepLst(fundingSchedulePojoLst,paginateSearch,i);
	}
	
	public List<FundingSchedulePojo> getfundingSchedule(Long scheduledFundingId) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getfundingSchedule(scheduledFundingId);
	}
	
	public String getSiteInfoBySiteId(String siteColName,Long siteID) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getSiteInfoBySiteId(siteColName,siteID);
	}
	
	public String checkForProcName(String procName, String cbbId) throws Exception{
		service=VelosServiceImpl.getService();
		return service.checkForProcName(procName, cbbId);
	}
	 public CodeList getAddChangeEmail(String type,  String subtype) {
		 try{
		 service=VelosServiceImpl.getService();
		 }catch(Exception e)
		
		 { 
			 log.error(e.getMessage());
			 e.printStackTrace();		
		 }
			return service.getCodeListByCode(type,subtype);
	 }
	 
	 public String getCbbType(String siteId)
	 {
		 try{
			 service=VelosServiceImpl.getService();
			 }catch(Exception e)
			 { 
				 log.error(e.getMessage());
				 e.printStackTrace();		
			 }
		 return service.getCbbType(siteId);
	 }
	 
	 public SitePojo getSiteBySiteName(String siteName,UserJB userObject){
		 try{
			 service=VelosServiceImpl.getService();
		}catch(Exception e) { 
			 log.error(e.getMessage());
			e.printStackTrace();		
		}
		return service.getSiteBySiteName(siteName,userObject);
	 }
	 
	 public List<SitePojo> getMultipleValuesByErSite(Long entityId,
				String entityType, String entitySubType,long isActive){
		 try{
			 service=VelosServiceImpl.getService();
		}catch(Exception e) { 
			 log.error(e.getMessage());
			e.printStackTrace();		
		}
		return service.getMultipleValuesByErSite( entityId,
				entityType, entitySubType, isActive);
	 }
	 
	 public List<SitePojo> getMultipleValuesByErSites(Long entityId,
				String entityType, String entitySubType){
		 try{
			 service=VelosServiceImpl.getService();
		}catch(Exception e) { 
			 log.error(e.getMessage());
			e.printStackTrace();		
		}
		return service.getMultipleValuesByErSites( entityId,
				entityType, entitySubType);
	 }
	 
	 
	 public Boolean isSiteExist(String siteName){
		 try{
			 service=VelosServiceImpl.getService();
		}catch(Exception e) { 
			 log.error(e.getMessage());
			e.printStackTrace();		
		}
		if(service.isSiteExist(siteName).size()>0)
		{
			return false;
		}
		else{
			return true;
		}
		
	 }
	 
	 
	 public Boolean isCbbIdExist(String cbbId){
		 try{
			 service=VelosServiceImpl.getService();
		}catch(Exception e) { 
			e.printStackTrace();		
		}
		if(service.isCbbIdExist(cbbId).size()>0)
		{
			return false;
		}
		else{
			return true;
		}
		
	 }
	 
	 public CodeList getCodeList(String type,  String subtype) {
		 try{
		 service=VelosServiceImpl.getService();
		 }catch(Exception e)
		 { 
			 log.error(e.getMessage());
			 e.printStackTrace();		
		 }
			return service.getCodeListByCode(type,subtype);
	 }
	 
	 public String getCbbInfoBySiteId(String CBBColName,Long siteID){
		 try{
			 service=VelosServiceImpl.getService();
		 }catch(Exception e){
			 log.error(e.getMessage());
			 e.printStackTrace();
		 }
		 return service.getCbbInfoBySiteId(CBBColName, siteID);
	 }
	 
	 public Map<Long,List<GrpsPojo>> getGroupsOfAllUserBySiteId(Long siteId){
		 try{
			 service=VelosServiceImpl.getService();
		 }catch(Exception e){
			 log.error(e.getMessage());
			 e.printStackTrace();
		 }
		 return service.getGroupsOfAllUserBySiteId(siteId);
	 }
	 public String getSiteName(Long siteId) throws Exception{
			service=VelosServiceImpl.getService();
			return service.getSiteName(siteId);
		}	
	 public String getSiteID(Long siteId) throws Exception{
			service=VelosServiceImpl.getService();
			return service.getSiteID(siteId);
		}	
	 public List<SitePojo> getSitesForQueryBuilder() throws Exception{
			service = VelosServiceImpl.getService();
			return service.getSitesForQueryBuilder();
		}
}
