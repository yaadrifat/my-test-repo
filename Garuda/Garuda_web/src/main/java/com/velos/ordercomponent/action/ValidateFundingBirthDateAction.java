package com.velos.ordercomponent.action;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.velos.ordercomponent.business.pojoobjects.FundingGuidelinesPojo;
import com.velos.ordercomponent.controller.CBBController;


public class ValidateFundingBirthDateAction extends VelosBaseAction {

	private Boolean dbValue;
	private String entityName;
	private String entityField;
	private String entityValue;	
	private String entityField1;
	private String entityValue1;
	private String entityValue2;
	private List<FundingGuidelinesPojo> fundingGuidelinespojoLst;	
	
	public String getEntityField1() {
		return entityField1;
	}

	public void setEntityField1(String entityField1) {
		this.entityField1 = entityField1;
	}

	public String getEntityValue1() {
		return entityValue1;
	}

	public void setEntityValue1(String entityValue1) {
		this.entityValue1 = entityValue1;
	}
	
	public String getEntityValue2() {
		return entityValue2;
	}

	public void setEntityValue2(String entityValue2) {
		this.entityValue2 = entityValue2;
	}

	public Boolean getDbValue() {
		return dbValue;
	}

	public void setDbValue(Boolean dbValue) {
		this.dbValue = dbValue;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getEntityField() {
		return entityField;
	}

	public void setEntityField(String entityField) {
		this.entityField = entityField;
	}

	public String getEntityValue() {
		return entityValue;
	}

	public void setEntityValue(String entityValue) {
		this.entityValue = entityValue;
	}

	public ValidateFundingBirthDateAction(){
		
		
	}
	
	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}
	

	
	
	public String getValidfundingguidelinesdate() throws Exception{
		fundingGuidelinespojoLst = new ArrayList<FundingGuidelinesPojo>();
		if(!getEntityName().equals(null) && !getEntityName().equals("") || !getEntityName().equals("null")){
		   if(getEntityValue2() != null && !getEntityValue2().equals("") || !getEntityValue2().equals("null")){
			   if(getEntityValue1() != null && !getEntityValue1().equals("") || !getEntityValue1().equals("null")){
				   if(getEntityValue() != null && !getEntityValue().equals("") || !getEntityValue().equals("null")){
					   fundingGuidelinespojoLst = new CBBController().getValidfundingguidelinesdate(getEntityValue(),getEntityValue1(),getEntityValue2());
					   if(fundingGuidelinespojoLst.size()>0){
						   setDbValue(false);
					   }
					   else{
						   setDbValue(true);
					   }
				   }
			   	}
			  }
		}
		return SUCCESS;
		}
	
}
