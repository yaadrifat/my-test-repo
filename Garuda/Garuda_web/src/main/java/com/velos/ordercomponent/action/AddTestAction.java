package com.velos.ordercomponent.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.ordercomponent.business.domain.CodeList;
import com.velos.ordercomponent.util.GarudaConfig;
	
	public class AddTestAction extends VelosBaseAction {
	public static final Log log = LogFactory.getLog(AddTestAction.class);
	private String htmlTest;

    public String getHtmlTest() {
		return htmlTest;
	}
    
	public void setHtmlTest(String htmlTest) {
		this.htmlTest = htmlTest;
	}
	
	public String toPullDropDown() throws Exception{
		boolean valueFound =false;
		String type = request.getParameter("codeType");
		String listName = request.getParameter("listName");
		String hideDivId= request.getParameter("hideDivId");
		String dbValue=request.getParameter("dbValue");
		String labClass="labChanges";
		String id = listName.replace("[","");
		String otherPkVal=null;
		id = id.replace("]","");
		id = id.replace(".","");
		  try{
			      StringBuffer popUpDropDown = new StringBuffer();
			      List<CodeList> tempList = GarudaConfig.getCodeListMap().get(type);
					 /*for(Map.Entry<Long, CodeList> me : codeListMapByIds.entrySet()){
					    	 if((type!=null && (type.equals(me.getValue().getType()) || type==me.getValue().getType()))){
					    		popUpDropDown.append("<OPTION value ='").append(me.getKey()).append("'>").append(me.getValue().getDescription()).append("</OPTION>"); 
					         }
		              }*/
					  if(tempList!=null && tempList.size()>0){
						  for(CodeList l : tempList){
							  if(l.getSubType().equals(OTHER)) {
								  otherPkVal=l.getPkCodeId().toString(); 
							  }
						  }
					  }
				  popUpDropDown.append("<SELECT NAME='"+listName+"' id='"+id+"' onchange=showDescField('"+hideDivId+"','"+otherPkVal+"',this.value,this.id); isChangeDone(this,'"+dbValue+"','"+labClass+"');>");
				  popUpDropDown.append("<OPTION VALUE='-1' SELECTED>Select</OPTION>");
				  List<CodeList> list = GarudaConfig.getCodeListMap().get(type);
				 /*for(Map.Entry<Long, CodeList> me : codeListMapByIds.entrySet()){
				    	 if((type!=null && (type.equals(me.getValue().getType()) || type==me.getValue().getType()))){
				    		popUpDropDown.append("<OPTION value ='").append(me.getKey()).append("'>").append(me.getValue().getDescription()).append("</OPTION>"); 
				         }
	              }*/
				  if(list!=null && list.size()>0){
					  for(CodeList l : list){
						  popUpDropDown.append("<OPTION value ='").append(l.getPkCodeId()).append("'>").append(l.getDescription()).append("</OPTION>"); 
					  }
				  }
			  popUpDropDown.append("</SELECT>");
 			  setHtmlTest(popUpDropDown.toString());
		}catch (Exception e) {
		  log.error(e.getMessage());
		  e.printStackTrace();
		 
		}
		 return "success";
	}
	
	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}
}
