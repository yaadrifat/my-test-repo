package com.velos.ordercomponent.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.ordercomponent.business.pojoobjects.IdmTestPojo;
import com.velos.ordercomponent.business.pojoobjects.LabTestGrpPojo;
import com.velos.ordercomponent.business.pojoobjects.LabTestPojo;
import com.velos.ordercomponent.controller.LabTestcontroller;

public class LabTestAction extends VelosBaseAction{
	public static final Log log = LogFactory.getLog(LabTestAction.class);
	private List labtestgrp=new ArrayList();
	private Long labgrp;
	private List labtestlist=new ArrayList();
	private Long pkidmgrp;
	private String labtestName;
	private String shrtName;
	private LabTestPojo labTestPojo=null;
	private LabTestGrpPojo labTestGrpPojo=null;
	private LabTestcontroller labTestcontroller;
	private List labtestlist1=null;
	private Long fkSpecimenId;


	public LabTestAction(){
		labTestcontroller= new LabTestcontroller();
	}
	
	public List getLabtestgrp() {
		return labtestgrp;
	}

	public void setLabtestgrp(List labtestgrp) {
		this.labtestgrp = labtestgrp;
	}

	public Long getLabgrp() {
		return labgrp;
	}

	public void setLabgrp(Long labgrp) {
		this.labgrp = labgrp;
	}
	public String getLabtestName() {
		return labtestName;
	}

	public void setLabtestName(String labtestName) {
		this.labtestName = labtestName;
	}
	public String getShrtName() {
		return shrtName;
	}

	public void setShrtName(String shrtName) {
		this.shrtName = shrtName;
	}
	public List getLabtestlist() {
		return labtestlist;
	}

	public void setLabtestlist(List labtestlist) {
		this.labtestlist = labtestlist;
	}
	public Long getPkidmgrp() {
		return pkidmgrp;
	}

	public void setPkidmgrp(Long pkidmgrp) {
		this.pkidmgrp = pkidmgrp;
	}
	
	public List getLabtestlist1() {
		return labtestlist1;
	}

	public void setLabtestlist1(List labtestlist1) {
		this.labtestlist1 = labtestlist1;
	}
	
	public Long getFkSpecimenId() {
		return fkSpecimenId;
	}

	public void setFkSpecimenId(Long fkSpecimenId) {
		this.fkSpecimenId = fkSpecimenId;
	}
	
	public String getLabGroup()throws Exception{
		try{
			List labgroup=labTestcontroller.getLabTestgrp();
			if(labgroup!=null){
				setLabtestgrp(labgroup);
			}
			getlabgrppks();
			//log.debug("getlabtestlist1 : "+getLabtestlist1());
			//log.debug("getlabtestlist1 size : "+getLabtestlist1().size());
		}catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage());
			e.printStackTrace();
			//log.debug("Exceprion in getLabGroup : "+e);
		}
		
		return "success";
	}
	
	public String getlabTest()throws Exception{
		try{
			//log.debug("getLabgrp : "+getLabgrp());
			if(getLabgrp()!=null){
				List listlabtest=labTestcontroller.getLabTestList(getLabgrp());
				if(listlabtest!=null){
					setLabtestlist(listlabtest);
				}
			}
			//log.debug("labtest list 1 : "+getLabtestlist1());
		}catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage());
			e.printStackTrace();
			//log.debug("Exceprion in getLabGroup : "+e);
		}
		return "success";
	}
	
	public String addLabOthertest()throws Exception{
		try{
			Long otherLabgrppk=labTestcontroller.getLabgrppks(OLBT);
			List labgroup=labTestcontroller.getLabTestgrp();
			List OthLabGrp = new ArrayList();
			if(labgroup!=null){
				for(Object a : labgroup){
					Object[] arr = (Object[])a;
					if(((BigDecimal)arr[0]).longValue()==otherLabgrppk){
						OthLabGrp.add(a);
						setLabtestgrp(OthLabGrp);
						}
				}
			}
			
			if(otherLabgrppk!=null){
				setLabgrp(otherLabgrppk);
			}
			//log.debug("getlabtestlist1 : "+getLabtestlist1());
			//log.debug("getlabtestlist1 size : "+getLabtestlist1().size());
		}catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage());
			e.printStackTrace();
			//log.debug("Exceprion in getLabGroup : "+e);
		}
		
		return "success";
	}
	
	public void getlabgrppks()throws Exception{
		try{
			Long idmgrppk=labTestcontroller.getLabgrppks("IOG");
			if(idmgrppk!=null){
				setPkidmgrp(idmgrppk);
			}
		}catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage());
			e.printStackTrace();
			//log.debug("Exceprion in getlabgrppks : "+e);
		}
	}
	public String savelabTest() throws Exception{
		try{
			//log.debug("getLabgrp : "+getLabgrp());
			//log.debug("labTestPojo.labtestName : "+getLabtestName());
			//log.debug("labTestPojo.shrtName : "+getShrtName());
			labTestPojo=new LabTestPojo();
			labTestGrpPojo=new LabTestGrpPojo();
			labTestPojo.setLabtestName(getLabtestName());
			labTestPojo.setShrtName(getShrtName());
			labTestGrpPojo.setFklabgrp(getLabgrp());
			labTestGrpPojo=labTestcontroller.saveorupdateLTestgrp(labTestGrpPojo, labTestPojo);
		}catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage());
			e.printStackTrace();
			//log.debug("Exception in savelabtest : "+e);
		}
		return "success";
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
}
