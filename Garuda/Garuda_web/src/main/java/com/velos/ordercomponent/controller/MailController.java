package com.velos.ordercomponent.controller;

import java.util.List;

import com.velos.ordercomponent.business.pojoobjects.EmailAttachmentPojo;
import com.velos.ordercomponent.business.pojoobjects.NotificationPojo;
import com.velos.ordercomponent.service.VelosService;
import com.velos.ordercomponent.service.impl.VelosServiceImpl;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class MailController {
	VelosService service;
	
	public Object getMailConfigurationByCode(String code ) throws Exception
	{
		service = VelosServiceImpl.getService();
		return service.getMailConfigurationByCode(code);
	}
	
	public NotificationPojo sendEmail(NotificationPojo notificationPojo) throws Exception{
		service = VelosServiceImpl.getService();
		return service.sendEmail(notificationPojo,true);
	}
	
	public NotificationPojo sendEmail(NotificationPojo notificationPojo,Boolean fromUI) throws Exception{
		service = VelosServiceImpl.getService();
		return service.sendEmail(notificationPojo,fromUI);
	}
	
	public List<EmailAttachmentPojo> saveEmailDocuments(List<EmailAttachmentPojo> pojos) throws Exception{
		service = VelosServiceImpl.getService();
		return service.saveEmailDocuments(pojos);
	}
}
