package com.velos.ordercomponent.controller;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.ordercomponent.business.pojoobjects.FormResponsesPojo;
import com.velos.ordercomponent.business.pojoobjects.FormVersionPojo;
import com.velos.ordercomponent.service.VelosService;
import com.velos.ordercomponent.service.impl.VelosServiceImpl;

public class DynaFormController {
	public static final Log log = LogFactory.getLog(DynaFormController.class);
	VelosService service;
	
	public List getFormData(Long cordId,String formName,String formvers,Long fkfrmversion) throws Exception{
		service = VelosServiceImpl.getService();
		//log.debug("Inside the Dynamic Form Controller");
		return service.getDynaFormLst(cordId,formName,formvers,fkfrmversion);
	}
	public List getIdmDeferCondition(String formName) throws Exception{
		service = VelosServiceImpl.getService();
		//log.debug("Inside the Dynamic Form Controller:getIdmDeferCondition()");
		return service.getIdmDeferCondition(formName);
	}
	public List getIdmResponseList(Long entityId) throws Exception{
		service = VelosServiceImpl.getService();
		//log.debug("Inside the Dynamic Form Controller:getIdmResponseList()");
		return service.getIdmResponseList(entityId);
	}
	public Long getFormseq(Long cordId,String formName,String formVers) throws Exception{
		service = VelosServiceImpl.getService();
		//log.debug("Inside the Dynamic Form Controller");
		return service.getFormseq(cordId,formName,formVers);
	}
	public FormVersionPojo getFormVerPojo(Long cordId,String formName,String formVers,Long formseq) throws Exception{
		service = VelosServiceImpl.getService();
		//log.debug("Inside the Dynamic Form Controller");
		return service.getFormVerPojo(cordId,formName,formVers,formseq);
	}
	
	public Object getformvmax(Long cordId,String formName) throws Exception{
		service = VelosServiceImpl.getService();
		//log.debug("Inside the Dynamic Form Controller");
		return service.getformvmax(cordId,formName);
	}
	
	public Long getformvmaxPk(Long cordId,String formName,String formvers) throws Exception{
		service = VelosServiceImpl.getService();
		//log.debug("Inside the Dynamic Form Controller");
		return service.getformvmaxPk(cordId,formName,formvers);
	}
	
	public String getLatestFormVer(String formName) throws Exception{
		service = VelosServiceImpl.getService();
		//log.debug("Inside the Dynamic Form Controller");
		return service.getLatestFormVer(formName);
	}
	
	public List getFormQues(Long cordId,String formName) throws Exception{
		service = VelosServiceImpl.getService();
		//log.debug("Inside the Dynamic Form Controller");
		return service.getDynaQuesLst(cordId,formName);
	}
	
	public void getOldFormVersion(Long entityId,String formName, boolean formUI) throws Exception{
		service = VelosServiceImpl.getService();
		//log.debug("Inside the Dynamic Form Controller:getOldFormVersion");
		service.getOldFormVersion(entityId,formName,formUI);
	}
	
	/*public List getChildQuest(Long pkQuestion,String formName) throws Exception{
		service = VelosServiceImpl.getService();
		//log.debug("Inside the Dynamic Form Controller Method : getChildQuest");
		return service.getChildQuest(pkQuestion,formName);
	}*/
	
	public List saveorupdateDynaForm(List<FormResponsesPojo> formResponsesPojos,boolean formUI) throws Exception{
		service = VelosServiceImpl.getService();
		//log.debug("Inside the Dynamic Form Controller Method : saveorupdateDynaForm");
		return service.saveorupdateDynaForms(formResponsesPojos,formUI);
	}
	
	public List getFormVersionLst(Long cordId,String formName) throws Exception{
		service = VelosServiceImpl.getService();
		//log.debug("Inside the Dynamic Form Controller Method : getFormVersionLst");
		return service.getFormVersionLst(cordId,formName);
	}
	
	public FormVersionPojo saveFormVersion(FormVersionPojo formVersionPojo,boolean formUI) throws Exception{
		service = VelosServiceImpl.getService();
		//log.debug("Inside the Dynamic Form Controller Method : saveFormVersion");
		return service.saveFormVersion(formVersionPojo,formUI);
	}
	
	public List getQuestionGroup(String formName,String formvers) throws Exception{
		service = VelosServiceImpl.getService();
		//log.debug("Inside the Dynamic Form Controller Method : getQuestionGroup");
		return service.getQuestionGroup(formName,formvers);
	}
	
	public List<Object> getPrevAssessData(Long entityId,Long fkformVersion) throws Exception{
		service = VelosServiceImpl.getService();
		//log.debug("Inside the Dynamic Form Controller Method : getQuestionGroup");
		return service.getPrevAssessData(entityId,fkformVersion);
	}
	public List getQuestionsByFormId(Long formId) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getQuestionsByFormId(formId);
	}
	public Long getfkCbbid(Long entityId) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getfkCbbid(entityId);
	}
	public String getidmdefervalue(Long entityId) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getidmdefervalue(entityId);
	}
	public String getAssessmentDetail(Long pkassessment) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getAssessmentDetail(pkassessment);
	}
	public Date getFormCreatedDate(Long cordId,String formName,String formVers) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getFormCreatedDate(cordId,formName,formVers);
	}
	public FormResponsesPojo saveFormData(FormResponsesPojo formobjects,Long entityId,String formName,boolean formUI) throws Exception{
	//	System.out.println("saveFormData in dynaFormController");
		service = VelosServiceImpl.getService();
		return service.saveFormData(formobjects,entityId,formName,formUI);
	}
}
