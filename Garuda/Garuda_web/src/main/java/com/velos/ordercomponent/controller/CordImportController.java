package com.velos.ordercomponent.controller;

import java.util.List;

import com.velos.eres.web.user.UserJB;
import com.velos.ordercomponent.business.pojoobjects.CordImportHistoryPojo;
import com.velos.ordercomponent.business.pojoobjects.CordImportProcessMessagesPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.pojoobjects.SearchPojo;
import com.velos.ordercomponent.service.VelosService;
import com.velos.ordercomponent.service.impl.VelosServiceImpl;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class CordImportController {

	VelosService service;
	
	public List<CordImportProcessMessagesPojo> getCordImportErrorMessagesByHistoryId(
			Long importHistoryId,Integer level,String registryId) throws Exception {
		service = VelosServiceImpl.getService();
		return service.getCordImportErrorMessagesByHistoryId(importHistoryId,level,registryId);
	}
	
	public List<CordImportHistoryPojo> getCordImportHistory(Long pkCordimportHistory,Long userPk,PaginateSearch paginateSearch) throws Exception {
		service = VelosServiceImpl.getService();
		return service.getCordImportHistory(pkCordimportHistory,userPk,paginateSearch);
	}
	
	public List getCordImportHistoryJson(Long pkCordimportHistory,Long userPk,PaginateSearch paginateSearch,String condition,String orderby) throws Exception {
		service = VelosServiceImpl.getService();
		return service.getCordImportHistoryJson(pkCordimportHistory,userPk,paginateSearch,condition,orderby);
	}
	
	public List getCordImportHistoryCount(Long userPk,String condition) throws Exception {
		service = VelosServiceImpl.getService();
		return service.getCordImportHistoryCount(userPk,condition);
	}
	
	public List<CordImportProcessMessagesPojo> saveCordImportErrorMessages(
			List<CordImportProcessMessagesPojo> messagesPojos) throws Exception {
		service = VelosServiceImpl.getService();
		return service.saveCordImportErrorMessages(messagesPojos);
	}
	
	public CordImportHistoryPojo saveCordImportHistory(
			CordImportHistoryPojo historyPojo) throws Exception {
		service = VelosServiceImpl.getService();
		return service.saveCordImportHistory(historyPojo);
	}
	public SearchPojo getFormsByType(SearchPojo searchPojo) throws Exception {
		service = VelosServiceImpl.getService();
		return service.velosSearch(searchPojo);
	}
	
	public void insertCordEntryHlaEsbRef(Long fkCord,UserJB user) throws Exception{
		service = VelosServiceImpl.getService();
		service.insertCordEntryHlaEsbRef(fkCord, user);
	}
	
	public String getFmhqQuestionResp(Long pkcord)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getFmhqQuestionResp(pkcord);
	}
	
	
}
