package com.velos.ordercomponent.validator;

import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

/**
 * @author Mohiuddin Ali Ahmed
 * @author Anurag Upadhyay
 * @version 1.0
 * 
 */

public class RequiredFormValidator extends FieldValidatorSupport{
	    private boolean searchableCheck;
	    private String dependencyField1;
	    private String dependencyField2;

		public boolean isSearchableCheck() {
			return searchableCheck;
		}

		public void setSearchableCheck(boolean searchableCheck) {
			this.searchableCheck = searchableCheck;
		}

	   public String getDependencyField1() {
			return dependencyField1;
		}

		public void setDependencyField1(String dependencyField1) {
			this.dependencyField1 = dependencyField1;
		}

		public String getDependencyField2() {
			return dependencyField2;
		}

		public void setDependencyField2(String dependencyField2) {
			this.dependencyField2 = dependencyField2;
		}

	@Override
	public void validate(Object object) throws ValidationException {
		// TODO Auto-generated method stub
		String fieldName = getFieldName();
		Byte byt = (Byte) getFieldValue(fieldName, object);
		Long df1=(Long) getFieldValue(dependencyField1, object);
		Long df2=(Long) getFieldValue(dependencyField2, object);
		long cordSearchable = (Long) getFieldValue("cordSearchable", object);
		if(searchableCheck && cordSearchable==1 && fieldName.equals("idmFormVersion") && (byt.equals(0) || byt<=0)){
			   addFieldError(fieldName, object);
		       return;			
		}else if(searchableCheck && cordSearchable==1 && df1!=null &&  df2!=null && byt!=null &&  df1.equals(df2) && (byt.equals(0)||byt<=0) ){
			   addFieldError(fieldName, object);
		       return;
		}
		
	}

}
