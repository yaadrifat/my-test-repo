package com.velos.ordercomponent.action;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Chapter;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Section;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import com.lowagie.text.pdf.PdfWriter;
import com.velos.eres.service.util.Configuration;
import com.velos.eres.web.user.UserJB;
import com.velos.ordercomponent.business.domain.RecipientInfo;
import com.velos.ordercomponent.business.pojoobjects.CBBPojo;
import com.velos.ordercomponent.business.pojoobjects.CBBProcedureInfoPojo;
import com.velos.ordercomponent.business.pojoobjects.CBBProcedurePojo;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.EligibilityDeclPojo;
import com.velos.ordercomponent.business.pojoobjects.FormVersionPojo;
import com.velos.ordercomponent.business.pojoobjects.LabTestPojo;
import com.velos.ordercomponent.business.pojoobjects.OrderPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.pojoobjects.PatLabsPojo;
import com.velos.ordercomponent.business.pojoobjects.SearchPojo;
import com.velos.ordercomponent.business.pojoobjects.SitePojo;
import com.velos.ordercomponent.controller.CBBController;
import com.velos.ordercomponent.controller.CBUController;
import com.velos.ordercomponent.controller.DynaFormController;
import com.velos.ordercomponent.controller.OpenOrderController;
import com.velos.ordercomponent.controller.VelosSearchController;
import com.velos.ordercomponent.util.VelosGarudaConstants;

public class ProductInsertAction extends CBUAction{
	
	private CBBPojo cbbPojo;
	private CBUController cbuController;
	private CBBProcedureInfoPojo cbbProcedureInfoPojo;
	private CBBProcedurePojo cbbProcedurePojo;
	private PaginateSearch paginateSearch;
	private List<CBBProcedureInfoPojo> cbbProcedureInfoPojoLst;
	private Long labGrpPkByGrpType;
	private CdrCbuPojo cdrCbuPojo;
	private String cdrCbuId;
	private String orderId;
	private Long tcnccPk;
	private Long fnpvPk;
	private EligibilityDeclPojo declPojo;
	private List<PatLabsPojo> postTestList;
	private OrderPojo orderPojo;
	private OpenOrderController openOrderController;
	private List quesGrpPrd = new ArrayList();
	private List mrqFormListPrd = new ArrayList();
	private String formvers;
	private Long seqnumber;
	private String totlCbuNclCntUncr;
	private String totlCbuNclCntUnkIfUncr;
	private String finalProdVol;
	FormVersionPojo formversion=new FormVersionPojo();
	RecipientInfo recipientInfo=new RecipientInfo();
    private SitePojo tcSite;
	/*private static Font headerFont = FontFactory.getFont("Arial Narrow", 14);
	private static Font bodyFont = FontFactory.getFont("Arial Narrow", 11);
	private static Font bodyBold = FontFactory.getFont("Arial Narrow", 11,Font.BOLD);
	private static Font footerFont = FontFactory.getFont("Arial Narrow", 8);*/
	private static Font headerFont = null;
	private static Font bodyFont = null;
	private static Font bodyBold = null;
	private static Font footerFont = null;
	private static Font footerSupFont = null;
	Calendar cal = Calendar.getInstance();
	int curYear = cal.get(Calendar.YEAR);
	
	public PaginateSearch getPaginateSearch() {
		return paginateSearch;
	}

	public void setPaginateSearch(PaginateSearch paginateSearch) {
		this.paginateSearch = paginateSearch;
	}

	public CBBProcedureInfoPojo getCbbProcedureInfoPojo() {
		return cbbProcedureInfoPojo;
	}

	public void setCbbProcedureInfoPojo(CBBProcedureInfoPojo cbbProcedureInfoPojo) {
		this.cbbProcedureInfoPojo = cbbProcedureInfoPojo;
	}
	
	public void setDeclPojo(EligibilityDeclPojo declPojo) {
		this.declPojo = declPojo;
	}
	public EligibilityDeclPojo getDeclPojo() {
		return declPojo;
	}
	
	public String getCdrCbuId() {
		return cdrCbuId;
	}
	public void setCdrCbuId(String cdrCbuId) {
		this.cdrCbuId = cdrCbuId;
	}
	
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	

	public Long getTcnccPk() {
		return tcnccPk;
	}

	public void setTcnccPk(Long tcnccPk) {
		this.tcnccPk = tcnccPk;
	}

	public Long getFnpvPk() {
		return fnpvPk;
	}

	public void setFnpvPk(Long fnpvPk) {
		this.fnpvPk = fnpvPk;
	}

	public Long getLabGrpPkByGrpType() {
		return labGrpPkByGrpType;
	}

	public void setLabGrpPkByGrpType(Long labGrpPkByGrpType) {
		this.labGrpPkByGrpType = labGrpPkByGrpType;
	}

	public List<CBBProcedureInfoPojo> getCbbProcedureInfoPojoLst() {
		return cbbProcedureInfoPojoLst;
	}

	public void setCbbProcedureInfoPojoLst(
			List<CBBProcedureInfoPojo> cbbProcedureInfoPojoLst) {
		this.cbbProcedureInfoPojoLst = cbbProcedureInfoPojoLst;
	}
	

	public CBBProcedurePojo getCbbProcedurePojo() {
		return cbbProcedurePojo;
	}

	public void setCbbProcedurePojo(CBBProcedurePojo cbbProcedurePojo) {
		this.cbbProcedurePojo = cbbProcedurePojo;
	}

	public List<PatLabsPojo> getPostTestList() {
		return postTestList;
	}

	public void setPostTestList(List<PatLabsPojo> postTestList) {
		this.postTestList = postTestList;
	}
	

	public List getQuesGrpPrd() {
		return quesGrpPrd;
	}

	public void setQuesGrpPrd(List quesGrpPrd) {
		this.quesGrpPrd = quesGrpPrd;
	}

	public List getMrqFormListPrd() {
		return mrqFormListPrd;
	}

	public void setMrqFormListPrd(List mrqFormListPrd) {
		this.mrqFormListPrd = mrqFormListPrd;
	}

	public String getFormvers() {
		return formvers;
	}

	public void setFormvers(String formvers) {
		this.formvers = formvers;
	}

	public FormVersionPojo getFormversion() {
		return formversion;
	}

	public void setFormversion(FormVersionPojo formversion) {
		this.formversion = formversion;
	}
	
	public String getTotlCbuNclCntUncr() {
		return totlCbuNclCntUncr;
	}

	public void setTotlCbuNclCntUncr(String totlCbuNclCntUncr) {
		this.totlCbuNclCntUncr = totlCbuNclCntUncr;
	}

	public String getTotlCbuNclCntUnkIfUncr() {
		return totlCbuNclCntUnkIfUncr;
	}

	public void setTotlCbuNclCntUnkIfUncr(String totlCbuNclCntUnkIfUncr) {
		this.totlCbuNclCntUnkIfUncr = totlCbuNclCntUnkIfUncr;
	}
	
	public RecipientInfo getRecipientInfo() {
		return recipientInfo;
	}

	public void setRecipientInfo(RecipientInfo recipientInfo) {
		this.recipientInfo = recipientInfo;
	}
	
	public String getFinalProdVol() {
		return finalProdVol;
	}

	public void setFinalProdVol(String finalProdVol) {
		this.finalProdVol = finalProdVol;
	}

	public SitePojo getTcSite() {
		return tcSite;
	}

	public void setTcSite(SitePojo tcSite) {
		this.tcSite = tcSite;
	}

	public ProductInsertAction(){
		
		CBUController cbuController = new CBUController();
		CBBController cBBController =new CBBController();
		paginateSearch =new PaginateSearch();
		orderPojo=new OrderPojo();
		openOrderController=new OpenOrderController();
	}
	
	
	
	public CBBPojo getCbbPojo() {
		return cbbPojo;
	}

	public void setCbbPojo(CBBPojo cbbPojo) {
		this.cbbPojo = cbbPojo;
	}

	public String productInsert() throws Exception{
		Long cordId=(Long.parseLong(request.getParameter("cordID")));
		UserJB user = (UserJB)getSession(getRequest()).getAttribute(VelosGarudaConstants.GARUDA_USER);
		getCdrCbuPojo().setCordID(cordId);
		Map codelstParam = new HashMap<String, String>();
		SearchPojo searchPojo = new SearchPojo();
		searchPojo.setCodelstParam(codelstParam);
		searchPojo.setSearchType(CDRCBU_OBJECTNAME);
		searchPojo.setCriteria(" cordID = '"+getCdrCbuPojo().getCordID()+"'");			
		searchPojo = new VelosSearchController().velosSearch(searchPojo);
		setLstObjectOne(searchPojo.getResultlst());
		setPkCodes(getLstObjectOne());
		setCdrCbuPojo(getCdrCbuPojo());
		//cdrCbuPojo = new CdrCbuPojo();
		try{
			//String idmCreateIdm = getIdmPdfReportFrShpPckt();
			Long valuePostFkTimingTest = getCodeListPkByTypeAndSubtype(VelosGarudaConstants.TIMING_OF_TEST, VelosGarudaConstants.POST_TEST);
			postTestList = new CBUController().getPatLabsPojoInfo(getCdrCbuPojo().getFkSpecimenId(),valuePostFkTimingTest);
			setPostTestList(postTestList);
			setTcnccPk(new CBUController().getLabTestpkbyshname(UNCRCT_SHN));
			setFnpvPk(new CBUController().getLabTestpkbyshname(FNPV_SHN));
			//cdrCbuPojo.setCordID(getCdrCbuPojo().getCordID());
			for(PatLabsPojo test : getPostTestList()){
				if(test!=null && (test.getFktestid()==new CBUController().getLabTestpkbyshname(UNCRCT_SHN)||test.getFktestid().equals(new CBUController().getLabTestpkbyshname(UNCRCT_SHN)))){
					if(test.getTestresult()!=null)
					{
						setTotlCbuNclCntUncr(test.getTestresult());
					}
				}
				if(test!=null && (test.getFktestid()==new CBUController().getLabTestpkbyshname(UNIFUNCRCT_SHN)||test.getFktestid().equals(new CBUController().getLabTestpkbyshname(UNIFUNCRCT_SHN)))){
					if(test.getTestresult()!=null)
					{
					setTotlCbuNclCntUnkIfUncr(test.getTestresult());
					}
				}
				if(test!=null && (test.getFktestid()==new CBUController().getLabTestpkbyshname(FNPV_SHN)||test.getFktestid().equals(new CBUController().getLabTestpkbyshname(FNPV_SHN)))){
					if(test.getTestresult()!=null)
					{
						setFinalProdVol(test.getTestresult());
					}
				}
		    }
			cbuFinalReviewPojo = getCbufinalReviewObj(getCdrCbuPojo(), getOrderId());
			setDumnPojo(new CBUController().getDumnQuestionsByEntity(getCdrCbuPojo().getCordID(), getCodeListPkByTypeAndSubtype(ENTITY_CBU_CODE_TYPE, ENTITY_CBU_CODE_SUB_TYPE),null));
		
	
			setLabGrpPkByGrpType(new CBUController().getLabGrpPkByGrpType("IOG"));
			//getIDMTestList(getCdrCbuPojo().getFkSpecimenId());
			CBBProcedurePojo cbbProcedurePojo = new CBBProcedurePojo();
			setCbbProcedurePojo(cbbProcedurePojo);
			CBBProcedureInfoPojo cbbProcedureInfoPojo = new CBBProcedureInfoPojo();
			setCbbProcedureInfoPojo(cbbProcedureInfoPojo);
				if(getCdrCbuPojo().getFkCbbProcedure()!=null){
			
					 List<SitePojo>	sites = new CBBController().getSites();
					 setSites(sites);   
					 String siteIds="'0'";
					 Iterator<SitePojo> itr =getSites().iterator();
					 while(itr.hasNext()){	        	
					    siteIds=siteIds+",'"+itr.next().getSiteId()+"'"; 
					 }
					 if( new CBBController().getCBBProcedure(getCdrCbuPojo().getFkCbbProcedure(),siteIds,paginateSearch,0)!=null &&  new CBBController().getCBBProcedure(getCdrCbuPojo().getFkCbbProcedure(),siteIds,paginateSearch,0).size()>0){
					 for(CBBProcedurePojo CbbpojoTemp : new CBBController().getCBBProcedure(getCdrCbuPojo().getFkCbbProcedure(),siteIds,paginateSearch,0)){
						 if(CbbpojoTemp.getPkProcId()==getCdrCbuPojo().getFkCbbProcedure() || (CbbpojoTemp.getPkProcId()).equals(getCdrCbuPojo().getFkCbbProcedure()) ){
							 cbbProcedurePojo=CbbpojoTemp;
						 }
					 }
						// cbbProcedurePojo = new CBBController().getCBBProcedure(getCdrCbuPojo().getFkCbbProcedure(),siteIds,paginateSearch,0).get(0);
					 }
					 cbbProcedureInfoPojoLst = new CBBController().getCBBProcedureInfo(getCdrCbuPojo().getFkCbbProcedure().toString());
					 if(cbbProcedurePojo != null){
						 setCbbProcedurePojo(cbbProcedurePojo);
						}
					 if(cbbProcedureInfoPojoLst.size()>0){
					 cbbProcedureInfoPojo=cbbProcedureInfoPojoLst.get(0);
					 setCbbProcedureInfoPojo(cbbProcedureInfoPojo);
					 }
			 }
			 List<SitePojo> cbbList = new CBBController().getCBBDetails(getCdrCbuPojo().getFkCbbId());
			 if (cbbList.size() > 0) {
					setCbbPojo(cbbList.get(0).getCbb());
					setSite(cbbList.get(0));
				}
			 if(request.getParameter("orderId")!=null && request.getParameter("orderId") != "" && request.getParameter("orderType")!=null && request.getParameter("orderType") != ""){
				setOrderId(request.getParameter("orderId"));
				orderPojo.setPk_OrderId(Long.parseLong(request.getParameter("orderId").toString()));
				orderPojo.setPackageSlipFlag("Y");
				orderPojo=openOrderController.updateOrderDetails(orderPojo);
				new CBUAction().workflowTasks(request.getParameter("orderId").toString(),getCodeListPkByTypeAndSubtype(ORDER_TYPE_NEW, request.getParameter("orderType").toString()),user);
				setCtOrderDetailsList(new OpenOrderController().getCtOrderDetails(orderPojo.getPk_OrderId().toString()));
				}
			 	EligibilityDeclPojo declPojo1=new EligibilityDeclPojo();
			 	setDeclPojo(declPojo1);
				declPojo1=new CBUController().getEligibilityDeclQuestionsByEntity(getCdrCbuPojo().getCordID(), getCodeListPkByTypeAndSubtype(ENTITY_CBU_CODE_TYPE, ENTITY_CBU_CODE_SUB_TYPE),cbuFinalReviewPojo.getFkFinalEligQuesId());
				if(declPojo1!=null){
				setDeclPojo(declPojo1);
				}
				DynaFormAction dynaAction = new DynaFormAction();
				DynaFormController dynaFrmController= new DynaFormController();
				dynaAction.setEntityId(getCdrCbuPojo().getCordID());
				dynaAction.setFormId(VelosGarudaConstants.FORM_IDM);
				dynaAction.setIsMinimumCriteria("1");
				dynaAction.getForm();
				quesGrpPrd=dynaAction.getQuesGrp();
				setQuesGrpPrd(quesGrpPrd);
				mrqFormListPrd=dynaAction.getMrqFormList();
				setMrqFormListPrd(mrqFormListPrd);
				formvers=dynaFrmController.getLatestFormVer(VelosGarudaConstants.FORM_IDM);
				setFormvers(formvers);
				seqnumber=dynaFrmController.getFormseq(getCdrCbuPojo().getCordID(),VelosGarudaConstants.FORM_IDM,getFormvers());
				formversion=dynaFrmController.getFormVerPojo(getCdrCbuPojo().getCordID(),VelosGarudaConstants.FORM_IDM,getFormvers(),seqnumber);
				setFormversion(formversion);
				//setPatientInfoLst(new OpenOrderController().getPatientInfo(new OpenOrderController().getOrderIdFromCordId(getCdrCbuPojo().getCordID())));
				Map<Long,RecipientInfo> mapRecipient = new HashMap<Long, RecipientInfo>();
				 RecipientInfo recipientInfo = null;
				 try{
				 List<Long> cordList = new ArrayList<Long>();
			     cordList.add(getCdrCbuPojo().getCordID());
				    if(cordList!=null && cordList.size()>0){
			    		mapRecipient = new CBUController().getRecipientInfoMap(cordList);
			    	}
				    if(mapRecipient!=null && mapRecipient.containsKey(getCdrCbuPojo().getCordID())){
				    	recipientInfo = mapRecipient.get(getCdrCbuPojo().getCordID());
				    	setRecipientInfo(recipientInfo);
			    	}
				    if(recipientInfo!=null && recipientInfo.getFkSecTransCenterId()!=null){
				    	if(new CBBController().getCBBDetails(recipientInfo.getFkSecTransCenterId())!=null && new CBBController().getCBBDetails(recipientInfo.getFkSecTransCenterId()).size()>0){
					    	setTcSite(new CBBController().getCBBDetails(recipientInfo.getFkSecTransCenterId()).get(0));
				    	}
				    }
				 }catch (Exception e) {
					e.printStackTrace();
				 }
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			try {
				Configuration.readAppendixParam(Configuration.ERES_HOME + "eresearch.xml");
				com.aithent.file.uploadDownload.Configuration.readSettings("eres");
				com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "study");	
				String fileDnPath = com.aithent.file.uploadDownload.Configuration.DOWNLOADSERVLET ;
				Configuration.readReportParam(Configuration.ERES_HOME + "eresearch.xml");
				String filePath = Configuration.REPDWNLDPATH;
				String pdfFileName="productInsertReport"+"["+ System.currentTimeMillis() + "]"+(user!=null?user.getUserId():"")+".pdf";
				String pdfFile=filePath + "/" + pdfFileName ;
				createShipmentPdf(pdfFile,1);
				pdfFile=addPageStamper(pdfFile,1);
				List<InputStream> pdfs = new ArrayList<InputStream>();
				pdfs.add(new FileInputStream(pdfFile));
				request.setAttribute("genForShpPackt","true");
				setPdfDocument(PDF_FDOE);
				getPdfObject();
				if(request.getAttribute("fileGenForPrd")!=null && !request.getAttribute("fileGenForPrd").equals("")){
					pdfs.add(new FileInputStream(request.getAttribute("fileGenForPrd").toString()));
				}
				setPdfDocument(PDF_PRODUCT_TAG);
				getPdfObject();
				if(request.getAttribute("fileGenForPrd")!=null && !request.getAttribute("fileGenForPrd").equals("")){
					pdfs.add(new FileInputStream(request.getAttribute("fileGenForPrd").toString()));
				}
				request.setAttribute("repId","172");
				request.setAttribute("repName","IDM");
				request.setAttribute("params",getCdrCbuPojo().getCordID()+":IDM");
				String idmCreateIdm = getDetailCbuPdfReports();
				if(getRequest().getSession().getAttribute("filePath")!=null && !getRequest().getSession().getAttribute("filePath").equals("")){
					pdfs.add(new FileInputStream(getRequest().getSession().getAttribute("filePath").toString()));
				}
				/*pdfFileName="idmForShpPack"+"["+ System.currentTimeMillis() + "].pdf";
				pdfFile=filePath + "/" + pdfFileName ;
				createShipmentPdf(pdfFile,2);
				pdfFile=addPageStamper(pdfFile,2);
				pdfs.add(new FileInputStream(pdfFile));*/
				pdfFileName="reciptOfCbu"+"["+ System.currentTimeMillis() + "]"+(user!=null?user.getUserId():"")+".pdf";
				pdfFile=filePath + "/" + pdfFileName ;
				createShipmentPdf(pdfFile,3);
				pdfFile=addPageStamper(pdfFile,3);
				pdfs.add(new FileInputStream(pdfFile));
				/*if(request.getAttribute("idmPdfFileName")!=null && !request.getAttribute("idmPdfFileName").equals("")){
					pdfs.add(new FileInputStream(request.getAttribute("idmPdfFileName").toString()));
				}*/
				pdfs.add(new FileInputStream(getContext().getRealPath("")+"/jsp/pdf/COI_2013.pdf"));
				String mergedshppkt="mergedshipmentPkt"+"["+ System.currentTimeMillis() + "]"+(user!=null?user.getUserId():"")+".pdf";
				OutputStream output = new FileOutputStream(filePath+"/"+mergedshppkt);
				concatPDFsOfShip(pdfs, output, false);
				String path=fileDnPath + "?file=" + mergedshppkt +"&mod=R";
				
				getRequest().getSession().setAttribute("pdfPath",path);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		/*try{
			if(getCdrCbuPojo().getCordID()!=null){
				CdrCbuPojo cdrCbuPojo = new CdrCbuPojo();
				cdrCbuPojo.setCordID(getCdrCbuPojo().getCordID());
				cdrCbuPojo =  new CBUController().getCordInfoById(cdrCbuPojo);			
				setCdrCbuPojo(cdrCbuPojo);
			
				}		
			}*/
		/*catch(Exception e)
		{e.printStackTrace();}*/
			 /*Document document = new Document();
			    try{
			        response.setContentType("application/pdf");
			        PdfWriter.getInstance(document, response.getOutputStream());
			        document.open();
			        
			      }catch(Exception e){
			        e.printStackTrace();
			    }
			    document.close();*/
	return SUCCESS;
	}
	
	public void concatPDFsOfShip(List<InputStream> streamOfPDFFiles,
            OutputStream outputStream, boolean paginate) {
		String repNo = "";
		String verNo = "";
		String temp=" ";
		String tempTime=" ";
		String[] dateString=null;
		repNo = getXMLTagValue(VelosGarudaConstants.CBUShipmentPack_REPNO);
		verNo = getXMLTagValue(VelosGarudaConstants.CBUShipmentPack_VERNO);
        Document document = new Document();
        try {
            List<InputStream> pdfs = streamOfPDFFiles;
            List<PdfReader> readers = new ArrayList<PdfReader>();
            int totalPages = 0;
            Iterator<InputStream> iteratorPDFs = pdfs.iterator();
 
            // Create Readers for the pdfs.
            while (iteratorPDFs.hasNext()) {
                InputStream pdf = iteratorPDFs.next();
                PdfReader pdfReader = new PdfReader(pdf);
                readers.add(pdfReader);
                totalPages += pdfReader.getNumberOfPages();
            }
            // Create a writer for the outputstream
            PdfWriter writer = PdfWriter.getInstance(document, outputStream);
 
            document.open();
            BaseFont bf = BaseFont.createFont(getContext().getRealPath("")+"/jsp/font/arial_narrow.TTF",  
	                 BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            PdfContentByte cb = writer.getDirectContent(); // Holds the PDF
            PdfContentByte ov = writer.getDirectContent(); 
            // data
 
            PdfImportedPage page;
            int currentPageNumber = 0;
            int pageOfCurrentReaderPDF = 0;
            Iterator<PdfReader> iteratorPDFReader = readers.iterator();
 
            // Loop through the PDF files and add to the output.
            while (iteratorPDFReader.hasNext()) {
                PdfReader pdfReader = iteratorPDFReader.next();
 
                // Create a new page in the target for each source page.
                while (pageOfCurrentReaderPDF < pdfReader.getNumberOfPages()) {
                    document.newPage();
                    pageOfCurrentReaderPDF++;
                    currentPageNumber++;
                    
 
                    // Code for pagination.
                    if (paginate) {
                        
                        ov.beginText();
                        ov.setFontAndSize(bf, 8f);
                        Chunk copyChunk = new Chunk(Character.toString ((char) 169),FontFactory.getFont("arial_narrow", 8,Font.NORMAL));
            			Chunk regChunk = new Chunk(Character.toString ((char) 174),FontFactory.getFont("arial_narrow", 8,Font.NORMAL));
            			regChunk.setTextRise(5.0f);
            			copyChunk.setTextRise(5.0f);
            			
            			Phrase p1=new Phrase(copyChunk);
            			String temp1=p1.get(0).toString();
            			
            			Phrase p2=new Phrase(regChunk);
            			String temp2=p2.get(0).toString();
            			
            			ov.beginText();
            			BaseFont bf1 = BaseFont.createFont(getContext().getRealPath("")+"/jsp/font/arial_narrow.TTF",  
            	                 BaseFont.CP1252, BaseFont.NOT_EMBEDDED); 
            			ov.setFontAndSize(bf1 , 8f);
            			
            			
            			
                        dateString = new Date().toLocaleString().split(" ");
            			temp=dateString[0]+" "+dateString[1]+" "+dateString[2];
            			tempTime=dateString[3]+" "+dateString[4];
                        ov.showTextAligned(PdfContentByte.ALIGN_LEFT, "Report Date: "+temp , 20, 820,0); 
                        ov.showTextAligned(PdfContentByte.ALIGN_LEFT, "Report Time: "+tempTime , 20, 810,0);
                        
                        ov.endText();
                        cb.beginText();
                        cb.setFontAndSize(bf, 8f);
                        cb.showTextAligned(PdfContentByte.ALIGN_LEFT, "Report: "+repNo , 20f, 10f,0f); 
                        cb.showTextAligned(PdfContentByte.ALIGN_LEFT, "Version: "+verNo, 20f, 2f,0f);  
                        cb.showTextAligned(PdfContentByte.ALIGN_RIGHT, "Page "
                                + currentPageNumber + " of " + totalPages, 530,
                                15, 0);
                        cb.showTextAligned(PdfContentByte.ALIGN_CENTER, getText("garuda.pdf.all.copyright1a")+" "+curYear+" "+temp1+" "+getText("garuda.pdf.all.copyright1b")+temp2+". "+getText("garuda.pdf.all.copyright1c") , 280f, 10f,0f); 
                        cb.showTextAligned(PdfContentByte.ALIGN_CENTER, getText("garuda.pdf.all.copyright2a")+temp2+" "+getText("garuda.pdf.all.copyright2b"), 280f,  
            		               2f,0f);
                        cb.endText();
                    }
                    page = writer.getImportedPage(pdfReader,
                            pageOfCurrentReaderPDF);
                    cb.addTemplate(page, 0, 0);
                }
                pageOfCurrentReaderPDF = 0;
            }
            outputStream.flush();
            document.close();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (document.isOpen())
                document.close();
            try {
                if (outputStream != null)
                    outputStream.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }
	public String addPageStamper(String filePath,int reportGen) throws Exception{
		String repNo = "";
		String verNo = "";
		if(reportGen==1){
			repNo = getXMLTagValue(VelosGarudaConstants.PRODUCTINSERT_REPNO);
			verNo = getXMLTagValue(VelosGarudaConstants.PRODUCTINSERT_VERNO);
		}
		else if(reportGen==2){
			repNo = getXMLTagValue(VelosGarudaConstants.SHIPMENTPKTIDM_REPNO);
			verNo = getXMLTagValue(VelosGarudaConstants.SHIPMENTPKTIDM_VERNO);
		}
		else if(reportGen==3){
			repNo = getXMLTagValue(VelosGarudaConstants.SHIPPACKETCBURECIPT_REPNO);
			verNo = getXMLTagValue(VelosGarudaConstants.SHIPPACKETCBURECIPT_VERNO);
		}
		 PdfReader originalPdf = new PdfReader(filePath);
		 String outputFilePath = "";
		 outputFilePath=filePath+"New";
		PdfStamper stamper = new PdfStamper(originalPdf, new FileOutputStream(outputFilePath));
		PdfContentByte under = null;
		PdfContentByte over = null;
		int totalPages = originalPdf.getNumberOfPages();
		String repDt="";
		String repTime = "";
		Date dt = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat ("MMM dd, yyyy");
		repDt = sdf.format(dt) ;
		SimpleDateFormat sd = new SimpleDateFormat ("kk:mm" );
		repTime = sd.format(dt);
		for (int pge = 1; pge <= totalPages; pge++) {
			under = stamper.getUnderContent(pge);
			over = stamper.getOverContent(pge);
			under = stamper.getUnderContent(pge);
			Chunk copyChunk = new Chunk(Character.toString ((char) 169),FontFactory.getFont("arial_narrow", 8,Font.NORMAL));
			Chunk regChunk = new Chunk(Character.toString ((char) 174),FontFactory.getFont("arial_narrow", 8,Font.NORMAL));
			regChunk.setTextRise(5.0f);
			copyChunk.setTextRise(5.0f);
			
			Phrase p1=new Phrase(copyChunk);
			String temp1=p1.get(0).toString();
			
			Phrase p2=new Phrase(regChunk);
			String temp2=p2.get(0).toString();
			
			over.beginText();
			BaseFont bf1 = BaseFont.createFont(getContext().getRealPath("")+"/jsp/font/arial_narrow.TTF",  
	                 BaseFont.CP1252, BaseFont.NOT_EMBEDDED); 
			over.setFontAndSize(bf1 , 8f);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, "Report Date: "+ repDt, 20f,   830f,0f);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, "Report Time: "+ repTime, 20f,   815f,0f);
			//over.showTextAligned(PdfContentByte.ALIGN_RIGHT, "Page " + pge + " of " + totalPages, 550f,830f,0f);
			over.endText();
			
			under.beginText();
			under.setFontAndSize(bf1 , 8f);
			//BaseFont bf = BaseFont.createFont(getContext().getRealPath("")+"/jsp/font/arial_narrow.TTF",BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
			under.showTextAligned(PdfContentByte.ALIGN_CENTER, getText("garuda.pdf.all.copyright1a")+" "+curYear+" "+temp1+" "+getText("garuda.pdf.all.copyright1b")+temp2+". "+getText("garuda.pdf.all.copyright1c") , 280f, 10f,0f); 
			under.showTextAligned(PdfContentByte.ALIGN_CENTER, getText("garuda.pdf.all.copyright2a")+temp2+" "+getText("garuda.pdf.all.copyright2b"), 280f,  
		               2f,0f);
			under.showTextAligned(PdfContentByte.ALIGN_LEFT, "Report: "+repNo , 20f, 10f,0f); 
			under.showTextAligned(PdfContentByte.ALIGN_LEFT, "Version: "+verNo, 20f, 2f,0f);  
			under.showTextAligned(PdfContentByte.ALIGN_RIGHT, "Page " + pge + " of " + totalPages, 580f,2f,0f);
			under.endText(); 
		}
		
		originalPdf.close();
		stamper.close();
		return outputFilePath;
	}
	
	
	
	public void createShipmentPdf(String pdfFile,int reportGen) throws Exception{
		Document document = new Document();
		Paragraph prea = new Paragraph();
		String temp=" ";
		String tempTime=" ";
		String[] dateString=null;
		
		PdfWriter.getInstance(document, new FileOutputStream(pdfFile)).setPdfVersion(PdfWriter.VERSION_1_7);
		document.open();
		try{
			document.addTitle("Shipment Packet");
			PdfPTable headTable = new PdfPTable(3);
			headTable.setWidthPercentage(100); 
			headTable.getDefaultCell().setBorderWidth(0);
			dateString = new Date().toLocaleString().split(" ");
			temp=dateString[0]+" "+dateString[1]+" "+dateString[2];
			tempTime=dateString[3]+" "+dateString[4];
			HeaderFooter footer = new HeaderFooter(new Phrase(getText("garuda.productinsert.label.footer1"), FontFactory.getFont("Arial Narrow",8)),false);
			//HeaderFooter topHeader = new HeaderFooter(new Phrase(getText("Print Date: ")+temp+"\n"+getText("Print Time: ")+tempTime, FontFactory.getFont("Arial Narrow",8)), false);
			//topHeader.setBorder(0);
			footer.setBorder(Rectangle.NO_BORDER);
			footer.setAlignment(Element.ALIGN_CENTER);
			//document.setHeader(topHeader);
			//document.setFooter(footer);
			/*prea.add(new Paragraph(getText("garuda.productinsert.label.productinsertreportnmdp")));
			addEmptyLine(prea, 1);
			prea.add(new Paragraph(getText("garuda.productinsert.label.productinsertreporthdrtxtnmdp")));
			addEmptyLine(prea, 1);
			document.add(prea);*/
			addContent(document,reportGen);
			document.close();
			//createTable(document);
			}catch(Exception e){e.printStackTrace();}
	}
	private void addEmptyLine(PdfPTable table, int numberRows , int numberCols) {
		for (int i = 0; i < numberRows; i++) {
			for(int j=0; j<numberCols;j++){
			table.addCell(" ");
			}
		}
	}
	
	private void addContent(Document document,int reportGen) throws DocumentException ,IOException{
		//Anchor anchor = new Anchor("First Chapter", catFont);
		//anchor.setName("First Chapter");

		// Second parameter is the number of the chapter
		Chapter catPart = new Chapter(0);
		catPart.setNumberDepth(0);
		//Paragraph subPara = new Paragraph(" ");
		Section subCatPart = catPart.addSection("");
		subCatPart.setNumberDepth(0);
		//Section subCatPart= new;
		/*subCatPart = catPart.addSection(subPara);
		subCatPart.add(new Paragraph("Paragraph 1"));
		subCatPart.add(new Paragraph("Paragraph 2"));
		subCatPart.add(new Paragraph("Paragraph 3"));
*/
		// Add a list
		/*createList(subCatPart);
		Paragraph paragraph = new Paragraph();
		addEmptyLine(paragraph, 5);
		subCatPart.add(paragraph);

		// Add a table
*/		createTable(subCatPart, reportGen);

		// Now add all this to the document
		document.add(subCatPart);

		// Next section
		/*anchor = new Anchor("Second Chapter", catFont);
		anchor.setName("Second Chapter");*/

		// Second parameter is the number of the chapter
		/*catPart = new Chapter(new Paragraph(anchor), 1);

		subPara = new Paragraph("Subcategory", subFont);*/
		/*subCatPart = catPart.addSection(subPara);
		subCatPart.add(new Paragraph("This is a very important message"));*/

		// Now add all this to the document

	}

	
	private void createTable(Section subCatPart,int reportGen)throws BadElementException ,IOException,DocumentException{
		BaseFont bf1 = BaseFont.createFont(getContext().getRealPath("")+"/jsp/font/arial_narrow.TTF",  
	            BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
		headerFont=new Font(bf1, 14);
		bodyFont = new Font(bf1, 11);
		bodyBold = new Font(bf1, 11,Font.BOLD);
		footerFont =new Font(bf1, 8);
		footerSupFont=new Font(bf1, 6);
		String temp=" ";
		String[] dateString=null;
		float[] columnWidths=null;
		PdfPTable table = new PdfPTable(6);
		PdfPTable tableWidBrd = new PdfPTable(1);
		PdfPCell cel = new PdfPCell();
		PdfPCell celWidBrd = new PdfPCell();
		Chunk header = new Chunk();
		Image checkBoxChecked=null;
		Image checkBoxUnChecked=null;
		Image section1Bar=null;
		Image section2Bar=null;
		Image section3Bar=null;
		Image dotImage=null;
		Image tag1Image=null;
		Image tag2Image=null;
		Image tag3Image=null;
		Image LineImage=null;
		Image Line1Image=null;
		Image radioButtonUnChecked=null;
		try{
			checkBoxUnChecked = Image.getInstance(getContext().getRealPath("")+"/jsp/images/CHECKBOX_UNCHECKED1.bmp");
			checkBoxChecked = Image.getInstance(getContext().getRealPath("")+"/jsp/images/CHECKBOX_CHECKED.bmp");
			radioButtonUnChecked = Image.getInstance(getContext().getRealPath("")+"/jsp/images/RADIOBUTTON_UNCHECKED.bmp");
			section1Bar=Image.getInstance(getContext().getRealPath("")+"/jsp/images/SECTION1BAR.bmp");
			section2Bar=Image.getInstance(getContext().getRealPath("")+"/jsp/images/SECTION2BAR.bmp");
			section3Bar=Image.getInstance(getContext().getRealPath("")+"/jsp/images/SECTION3BAR.bmp");
			dotImage=Image.getInstance(getContext().getRealPath("")+"/jsp/images/dot.bmp");
			tag1Image=Image.getInstance(getContext().getRealPath("")+"/jsp/images/tag11.bmp");
			tag2Image=Image.getInstance(getContext().getRealPath("")+"/jsp/images/tag12.bmp");
			tag3Image=Image.getInstance(getContext().getRealPath("")+"/jsp/images/tag13.bmp");
			LineImage=Image.getInstance(getContext().getRealPath("")+"/jsp/images/Line.bmp");
			Line1Image=Image.getInstance(getContext().getRealPath("")+"/jsp/images/Line1.bmp");
		}catch(Exception e){
			e.printStackTrace();
		}
		/*-----------------------------NMDP Cord Blood Unit - Product Insert Report------------*/
		if(reportGen==1){
		table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		Phrase p1 = new Phrase();
		Chunk superScrChnk=new Chunk(getText("garuda.nmdp.productinsert.headingSupScrpPrt"),bodyFont);
		superScrChnk.setTextRise(5f);
		p1.add(new Chunk(getText("garuda.nmdp.productinsert.heading"),headerFont));
		p1.add(superScrChnk);
		cel = new PdfPCell(p1);
		//cel.addElement(p);
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.productinsertreportnmdp"),headerFont));
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.productinsertreporthdrtxtnmdp"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		subCatPart.add(table);
		
		table = new PdfPTable(6);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		
		Phrase pCel = new Phrase();
		pCel.add(new Chunk(getText("garuda.recipient&tcInfo.label.patientid")+"  ",bodyBold));
		/*cel.setBorder(0);
		table.addCell(cel);*/
		//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
		 /*if(recipientInfo!=null && !StringUtils.isEmpty(recipientInfo.getRecipientId())){
				pCel.add(new Chunk(recipientInfo.getRecipientId(),bodyFont));
				}*/
			if(getCtOrderDetailsList()!=null && getCtOrderDetailsList().size()>0){
				Object[] obj = (Object[]) getCtOrderDetailsList().get(0);
				if(obj[13]!=null && !(obj[13].toString().equals("Not Provided"))){
					pCel.add(new Chunk(obj[13].toString(),bodyFont));
				}
				else{
					pCel.add(new Chunk("",bodyFont));
				}
			}
			else{
				pCel.add(new Chunk(" ",bodyFont));
			}
			cel = new PdfPCell(pCel);
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table.addCell(cel);
			table.addCell("");
		
		//Bold = new Phrase(getText("garuda.unitreport.label.localcbuid"),subFont);
		pCel = new Phrase();
		pCel.add(new Chunk("          "+getText("garuda.cbbdefaults.lable.cbbregistryname")+":  ",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		/*cel.setBorder(0);
		table.addCell(cel);*/
		//Bold = new Phrase(getCdrCbuPojo().getLocalCbuId(),smallBold);
		if(getSite()!=null && getSite().getSiteName()!=null)
		{
			pCel.add(new Chunk(getSite().getSiteName(),bodyFont));
			//cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		}
		else
		{
			pCel.add(new Chunk(" ",bodyFont));
		}
		cel = new PdfPCell(pCel);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		table.addCell("");
		table.addCell("");
		
		pCel = new Phrase();
		pCel.add(new Chunk(getText("garuda.idsReport.cburegids")+"  ",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		/*cel.setBorder(0);
		table.addCell(cel);*/
		//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
		if(getCdrCbuPojo()!=null && getCdrCbuPojo().getRegistryId()!=null)
		{
			pCel.add(new Chunk(getCdrCbuPojo().getRegistryId(),bodyFont));
		//cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		}
		else
		{
			pCel.add(new Chunk(" ",bodyFont));
		}
		cel = new PdfPCell(pCel);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		//Bold = new Phrase(getText("garuda.unitreport.label.localcbuid"),subFont);
		pCel = new Phrase();
		pCel.add(new Chunk("          "+getText("garuda.cbuentry.label.idbag")+":  ",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		/*cel.setBorder(0);
		table.addCell(cel);*/
		//Bold = new Phrase(getCdrCbuPojo().getLocalCbuId(),smallBold);
		if(getCdrCbuPojo()!=null && getCdrCbuPojo().getNumberOnCbuBag()!=null)
		{
			pCel.add(new Chunk(getCdrCbuPojo().getNumberOnCbuBag(),bodyFont));
		/*cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);*/
		}
		else
		{
			pCel.add(new Chunk(" ",bodyFont));
			/*cel.setBorder(0);*/
		}
		cel = new PdfPCell(pCel);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		table.addCell("");
		table.addCell("");
		
		//Bold = new Phrase(getText("garuda.cdrcbuview.label.collection_date" ),subFont);

		pCel = new Phrase();
		pCel.add(new Chunk(getText("garuda.cdrcbuview.label.collection_date")+":  ",bodyBold));
		//cel = new PdfPCell(new Phrase(getText("garuda.cdrcbuview.label.collection_date" )+":",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		/*cel.setBorder(0);
		table.addCell(cel);*/
		//Bold = new Phrase(getCdrCbuPojo().getSpecimen().getSpecCollDate().toString(),smallBold);
		
		if(getCdrCbuPojo().getSpecimen().getSpecCollDate()!=null){
		dateString = getCdrCbuPojo().getSpecimen().getSpecCollDate().toLocaleString().split(" ");
		temp=dateString[0]+" "+dateString[1]+" "+dateString[2];
		}
		pCel.add(new Chunk(temp,bodyFont));
		//cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel = new PdfPCell(pCel);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		
		pCel = new Phrase();
		pCel.add(new Chunk(getText("garuda.productinsert.label.time")+":  ",bodyBold));
		//cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.time")+":",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		/*cel.setBorder(0);
		table.addCell(cel);
		*/
		if(getCdrCbuPojo()!=null && getCdrCbuPojo().getCbuCollectionTime()!=null)
		{
			pCel.add(new Chunk(getCdrCbuPojo().getCbuCollectionTime(),bodyFont));
		}
		else
		{
			pCel.add(new Chunk(" ",bodyFont));
		}
		//cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		//cel = new PdfPCell(pCel);
		//cel.setBorder(0);
		//table.addCell(cel);
		//table.addCell("");
		
		//pCel = new Phrase();
		pCel.add(new Chunk("                         "+getText("garuda.productinsert.label.productcode")+":  ",bodyBold));
		//cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.productcode")+":",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		/*cel.setBorder(0);
		table.addCell(cel);*/
		
		/*if(getCbbProcedurePojo()!=null && getCbbProcedurePojo().getProductCode()!=null)
		{
			pCel.add(new Chunk(getCbbProcedurePojo().getProductCode(),bodyFont));

		}
		else
			{*/
			pCel.add(new Chunk("57MH-01",bodyFont));
			/*}*/
		//cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel = new PdfPCell(pCel);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		table.addCell("");
		table.addCell("");

		pCel = new Phrase();
		pCel.add(new Chunk(getText("garuda.cdrcbuview.label.licensure_status")+":  ",bodyBold));
		//cel = new PdfPCell(new Phrase(getText("garuda.cdrcbuview.label.licensure_status")+":",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		/*cel.setBorder(0);
		table.addCell(cel);*/
		if(getCdrCbuPojo()!=null && getCdrCbuPojo().getLicense()!=null)
		{
			pCel.add(new Chunk(getCdrCbuPojo().getLicense(),bodyFont));
		}
		else
		{
			pCel.add(new Chunk(" ",bodyFont));
		}
		cel = new PdfPCell(pCel);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell(" ");
		table.addCell(" ");
		table.addCell(" ");
		table.addCell(" ");
		table.addCell(" ");
		
		header = new Chunk(getText("garuda.productinsert.label.unlicensedcordbloodunitheader"),bodyFont);
		header.setUnderline(0.1f, -1f);
		cel = new PdfPCell(new Phrase(header));
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		cel.setColspan(6);
		table.addCell(cel);
		/*set width of table cells*/
		
		 columnWidths = new float[] {38f, 2f, 60f, 0f, 0f,0f};
	       try{
			table.setWidths(columnWidths);
	       }
	       catch(Exception e){e.printStackTrace();}
		tableWidBrd = new PdfPTable(1);
		tableWidBrd.setWidthPercentage(100);
		tableWidBrd.setHorizontalAlignment(0);
		tableWidBrd.getDefaultCell().setBorder(0);
		tableWidBrd.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		celWidBrd = new PdfPCell(table);
		celWidBrd.setHorizontalAlignment(Element.ALIGN_LEFT);
		celWidBrd.setBorder(Rectangle.BOX);
		tableWidBrd.addCell(celWidBrd);
		subCatPart.add(tableWidBrd);
		//subCatPart.add(header);
		
		PdfPTable table1 = new PdfPTable(3);
		table1.setWidthPercentage(100);
		table1.setHorizontalAlignment(0);
		table1.getDefaultCell().setBorder(0);
		table1.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		
		pCel = new Phrase();
		pCel.add(new Chunk(getText("garuda.productinsert.label.productname")+":  "+getText("garuda.productinsert.label.productnameReportStcContent"),bodyBold));
		//cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.productname")+":",bodyBold));
		//cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		//cel.setBorder(0);
		//table1.addCell(cel);
		/*if(getCbbProcedurePojo()!=null && getCbbProcedurePojo().getProcName()!=null)
		{
			pCel.add(new Chunk(getCbbProcedurePojo().getProcName(),bodyFont));
		}
		else
		{
			pCel.add(new Chunk(" ",bodyFont));
		}*/
		cel = new PdfPCell(pCel);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		cel.setColspan(3);
		table1.addCell(cel);
		/*table1.addCell(" ");
		table1.addCell(" ");
		table1.addCell(" ");
		table1.addCell(" ");*/
		
		pCel = new Phrase();
		pCel.add(new Chunk(getText("garuda.productinsert.label.finalProdVol")+":  ",bodyBold));
		//cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.finalProdVol")+":",bodyBold));
		//cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		//cel.setBorder(0);
		//table1.addCell(cel);
		if(getFinalProdVol()!=null)
		{
			pCel.add(new Chunk(getFinalProdVol()+" "+getText("garuda.processingrep.ml"),bodyFont));
		}
		else
		{
			pCel.add(new Chunk(" ",bodyFont));
		}
		cel = new PdfPCell(pCel);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		
		pCel = new Phrase();
		pCel.add(new Chunk(getText("garuda.cordentry.label.bloodtype")+":  ",bodyBold));
		//cel = new PdfPCell(new Phrase(getText("garuda.cordentry.label.bloodtype")+":",bodyBold));
		//cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		//cel.setBorder(0);
		//table1.addCell(cel);
		
		if(getCdrCbuPojo()!=null && getCdrCbuPojo().getBloodGpDes()!=null)
		{
			pCel.add(new Chunk(getCdrCbuPojo().getBloodGpDes(),bodyFont));
		}
		else
		{
			pCel.add(new Chunk(" ",bodyFont));
		}
		cel = new PdfPCell(pCel);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		
		pCel = new Phrase();
		pCel.add(new Chunk(getText("garuda.productinsert.label.rhType")+":  ",bodyBold));		
		//cel = new PdfPCell(new Phrase(getText("garuda.cbuentry.label.rhtype")+":",bodyBold));
		//cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		//cel.setBorder(0);
		//table1.addCell(cel);
		
		if(getCdrCbuPojo()!=null && getCdrCbuPojo().getRhTypeDes()!=null)
		{
			pCel.add(new Chunk(getCdrCbuPojo().getRhTypeDes(),bodyFont));
		}
		else
		{
			pCel.add(new Chunk(" ",bodyFont));
		}
		cel = new PdfPCell(pCel);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		
		pCel = new Phrase();
		pCel.add(new Chunk(getText("garuda.productinsert.label.totalCBUnuclcellcnt")+":  ",bodyBold));
		//cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.totalCBUnuclcellcnt")+":",bodyBold));
		//cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		//cel.setBorder(0);
		//table1.addCell(cel);
		if(getCdrCbuPojo()!=null && getCdrCbuPojo().getIsSystemCord()!=null && getCdrCbuPojo().getIsSystemCord().equals("no"))
		{
			if(getTotlCbuNclCntUnkIfUncr()!=null){
				pCel.add(new Chunk(getTotlCbuNclCntUnkIfUncr(),bodyFont));
			}
			else if(getTotlCbuNclCntUncr()!=null){
				pCel.add(new Chunk(getTotlCbuNclCntUncr(),bodyFont));
			}
			else{
				pCel.add(new Chunk("",bodyFont));
			}
		}
		else if(getCdrCbuPojo()!=null && (getCdrCbuPojo().getIsSystemCord()==null || getCdrCbuPojo().getIsSystemCord().equals("yes")))
		{
			if(getTotlCbuNclCntUncr()!=null){
				pCel.add(new Chunk(getTotlCbuNclCntUncr(),bodyFont));
			}
			else{
				pCel.add(new Chunk("",bodyFont));
			}
		}
		else{
			pCel.add(new Chunk("",bodyFont));
		}
		Chunk superScrChnk1=new Chunk("7",footerFont);
		superScrChnk1.setTextRise(4f);
		pCel.add(new Chunk(" *10",bodyFont));
		pCel.add(superScrChnk1);
		cel = new PdfPCell(pCel);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		
		pCel = new Phrase();
		pCel.add(new Chunk(getText("garuda.cordentry.label.prostartdate")+":  ",bodyBold));
		//cel = new PdfPCell(new Phrase(getText("garuda.cordentry.label.prostartdate")+":",bodyBold));
		//cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		//cel.setBorder(0);
		//table1.addCell(pCel);
		if(getCdrCbuPojo().getProcessingDate()!=null){
		dateString = getCdrCbuPojo().getProcessingDate().toLocaleString().split(" ");
		temp=dateString[0]+" "+dateString[1]+" "+dateString[2];
		}
		pCel.add(new Chunk(temp,bodyFont));
		cel = new PdfPCell(pCel);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		cel.setColspan(2);
		table1.addCell(cel);
		/*table1.addCell(" ");
		table1.addCell(" ");*/
		
		pCel = new Phrase();
		pCel.add(new Chunk(getText("garuda.productinsert.label.productmodification")+":  ",bodyBold));
		//cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.productmodification")+":",bodyBold));
		//cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		//cel.setBorder(0);
		//table1.addCell(cel);
		if(getCbbProcedureInfoPojo()!=null && getCbbProcedureInfoPojo().getFkProductModification()!=null)
		{
			pCel.add(new Chunk(getCodeListDescById(getCbbProcedureInfoPojo().getFkProductModification()),bodyFont));
		}
		else
		{
			pCel.add(new Chunk(" ",bodyFont));
		}
		cel = new PdfPCell(pCel);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		cel.setColspan(3);
		table1.addCell(cel);
		addEmptyLine(table1, 1, 3);
		/*table1.addCell(" ");
		table1.addCell(" ");
		table1.addCell(" ");
		table1.addCell(" ");*/
		
		columnWidths = new float[] {40f,40f,20f};
	       try{
			table1.setWidths(columnWidths);
	       }
	       catch(Exception e){e.printStackTrace();}
		subCatPart.add(table1);
		
		//subCatPart.add(header);
		table1 = new PdfPTable(2);
		table1.setWidthPercentage(100);
		table1.setHorizontalAlignment(0);
		table1.getDefaultCell().setBorder(0);
		table1.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		header = new Chunk(getText("garuda.productinsert.label.additivesvolredheader"),bodyBold);
		header.setUnderline(0.1f, -2f);
		cel = new PdfPCell(new Phrase(header));
		cel.setBorder(0);
		table1.addCell(cel);
		table1.addCell("");
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.typeofanticoagulant")+":",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.volume/percent")+":",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		temp=" ";
		if(getCbbProcedureInfoPojo().getThouUnitPerMlHeparin()!=null || getCbbProcedureInfoPojo().getThouUnitPerMlHeparinper()!=null){
			cel = new PdfPCell(new Phrase(getText("garuda.cbbprocedures.label.thouunitpermlheparin"),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
			if(getCbbProcedureInfoPojo().getThouUnitPerMlHeparin()!=null){
				temp=temp+getCbbProcedureInfoPojo().getThouUnitPerMlHeparin().toString();
				temp=temp+" "+getText("garuda.processingrep.ml");
			}
			if(getCbbProcedureInfoPojo().getThouUnitPerMlHeparinper()!=null){
				if(temp.length()>1){
					temp=temp+"/";
				}
				temp=temp+getCbbProcedureInfoPojo().getThouUnitPerMlHeparinper().toString();
				temp=temp+" "+getText("garuda.processingrep.percentage");
			}
			cel = new PdfPCell(new Phrase(temp,bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
		}
		temp=" ";
		if(getCbbProcedureInfoPojo().getFiveUnitPerMlHeparin()!=null || getCbbProcedureInfoPojo().getFiveUnitPerMlHeparinper()!=null){
			cel = new PdfPCell(new Phrase(getText("garuda.cbbprocedures.label.fiveunitpermlheparin"),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
			if(getCbbProcedureInfoPojo().getFiveUnitPerMlHeparin()!=null){
				temp=temp+getCbbProcedureInfoPojo().getFiveUnitPerMlHeparin().toString();
				temp=temp+" "+getText("garuda.processingrep.ml");
			}
			if(getCbbProcedureInfoPojo().getFiveUnitPerMlHeparinper()!=null){
				if(temp.length()>1){
					temp=temp+"/";
				}
				temp=temp+getCbbProcedureInfoPojo().getFiveUnitPerMlHeparinper().toString();
				temp=temp+" "+getText("garuda.processingrep.percentage");
			}
			cel = new PdfPCell(new Phrase(temp,bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
		}
		temp=" ";
		if(getCbbProcedureInfoPojo().getTenUnitPerMlHeparin()!=null || getCbbProcedureInfoPojo().getTenUnitPerMlHeparinper()!=null){
			cel = new PdfPCell(new Phrase(getText("garuda.cbbprocedures.label.tenunitpermlheparin"),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
			if(getCbbProcedureInfoPojo().getTenUnitPerMlHeparin()!=null){
				temp=temp+getCbbProcedureInfoPojo().getTenUnitPerMlHeparin().toString();
				temp=temp+" "+getText("garuda.processingrep.ml");
			}
			if(getCbbProcedureInfoPojo().getTenUnitPerMlHeparinper()!=null){
				if(temp.length()>1){
					temp=temp+"/";
				}
				temp=temp+getCbbProcedureInfoPojo().getTenUnitPerMlHeparinper().toString();
				temp=temp+" "+getText("garuda.processingrep.percentage");
			}
			cel = new PdfPCell(new Phrase(temp,bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
		}
		temp=" ";
		if(getCbbProcedureInfoPojo().getSixPerHydroxyethylStarch()!=null || getCbbProcedureInfoPojo().getSixPerHydroxyethylStarchper()!=null){
			cel = new PdfPCell(new Phrase(getText("garuda.cbbprocedures.label.sixperhydroxyethylstarch"),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
			if(getCbbProcedureInfoPojo().getSixPerHydroxyethylStarch()!=null){
				temp=temp+getCbbProcedureInfoPojo().getSixPerHydroxyethylStarch().toString();
				temp=temp+" "+getText("garuda.processingrep.ml");
			}
			if(getCbbProcedureInfoPojo().getSixPerHydroxyethylStarchper()!=null){
				if(temp.length()>1){
					temp=temp+"/";
				}
				temp=temp+getCbbProcedureInfoPojo().getSixPerHydroxyethylStarchper().toString();
				temp=temp+" "+getText("garuda.processingrep.percentage");
			}
			cel = new PdfPCell(new Phrase(temp,bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
		}
		temp=" ";
		if(getCbbProcedureInfoPojo().getCpda()!=null || getCbbProcedureInfoPojo().getCpdaper()!=null){
			cel = new PdfPCell(new Phrase(getText("garuda.cbbprocedures.label.cpda"),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
			if(getCbbProcedureInfoPojo().getCpda()!=null){
				temp=temp+getCbbProcedureInfoPojo().getCpda().toString();
				temp=temp+" "+getText("garuda.processingrep.ml");
			}
			if(getCbbProcedureInfoPojo().getCpdaper()!=null){
				if(temp.length()>1){
					temp=temp+"/";
				}
				temp=temp+getCbbProcedureInfoPojo().getCpdaper().toString();
				temp=temp+" "+getText("garuda.processingrep.percentage");
			}
			cel = new PdfPCell(new Phrase(temp,bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
		}
		temp=" ";
		if(getCbbProcedureInfoPojo().getCpd()!=null || getCbbProcedureInfoPojo().getCpdper()!=null){
			cel = new PdfPCell(new Phrase(getText("garuda.cbbprocedures.label.cpd"),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
			if(getCbbProcedureInfoPojo().getCpd()!=null){
				temp=temp+getCbbProcedureInfoPojo().getCpd().toString();
				temp=temp+" "+getText("garuda.processingrep.ml");
			}
			if(getCbbProcedureInfoPojo().getCpdper()!=null){
				if(temp.length()>1){
					temp=temp+"/";
				}
				temp=temp+getCbbProcedureInfoPojo().getCpdper().toString();
				temp=temp+" "+getText("garuda.processingrep.percentage");
			}
			cel = new PdfPCell(new Phrase(temp,bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
		}
		temp=" ";
		if(getCbbProcedureInfoPojo().getAcd()!=null || getCbbProcedureInfoPojo().getAcdper()!=null){
			cel = new PdfPCell(new Phrase(getText("garuda.cbbprocedures.label.acd"),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
			if(getCbbProcedureInfoPojo().getAcd()!=null){
				temp=temp+getCbbProcedureInfoPojo().getAcd().toString();
				temp=temp+" "+getText("garuda.processingrep.ml");
			}
			if(getCbbProcedureInfoPojo().getAcdper()!=null){
				if(temp.length()>1){
					temp=temp+"/";
				}
				temp=temp+getCbbProcedureInfoPojo().getAcdper().toString();
				temp=temp+" "+getText("garuda.processingrep.percentage");
			}
			cel = new PdfPCell(new Phrase(temp,bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
		}
		temp=" ";
		if(getCbbProcedureInfoPojo().getOtherAnticoagulant()!=null || getCbbProcedureInfoPojo().getOtherAnticoagulantper()!=null){
			cel = new PdfPCell(new Phrase(getText("garuda.cbbprocedures.label.otheranticoagulant"),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
			table1.addCell("");
			if(getCbbProcedureInfoPojo().getSpecifyOthAnti()!=null){
				cel = new PdfPCell(new Phrase(getCbbProcedureInfoPojo().getSpecifyOthAnti().toString(),bodyFont));
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table1.addCell(cel);
			if(getCbbProcedureInfoPojo().getOtherAnticoagulant()!=null){
				temp=temp+getCbbProcedureInfoPojo().getOtherAnticoagulant().toString();
				temp=temp+" "+getText("garuda.processingrep.ml");
			}
			if(getCbbProcedureInfoPojo().getOtherAnticoagulantper()!=null){
				if(temp.length()>1){
					temp=temp+"/";
				}
				temp=temp+getCbbProcedureInfoPojo().getOtherAnticoagulantper().toString();
				temp=temp+" "+getText("garuda.processingrep.percentage");
			}
			cel = new PdfPCell(new Phrase(temp,bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
			}
		}
		addEmptyLine(table1, 1, 2);
		temp=" ";
		columnWidths = new float[] {50f, 30f};
	       try{
			table1.setWidths(columnWidths);
	       }
	       catch(Exception e){e.printStackTrace();}
		subCatPart.add(table1);
		//subCatPart.add(header);
		table1 = new PdfPTable(2);
		table1.setWidthPercentage(100);
		table1.setHorizontalAlignment(0);
		table1.getDefaultCell().setBorder(0);
		table1.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		header = new Chunk(getText("garuda.productinsert.label.cryoprotectantsotheradditives"),bodyBold);
		header.setUnderline(0.1f, -2f);
		cel = new PdfPCell(new Phrase(header));
		cel.setBorder(0);
		table1.addCell(cel);
		table1.addCell("");
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.typeofcryoprotectant")+":",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.volume/percent")+":",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		if(getCbbProcedureInfoPojo().getHunPerDmso()!=null || getCbbProcedureInfoPojo().getHunPerDmsoper()!=null){
			cel = new PdfPCell(new Phrase(getText("garuda.cbbprocedures.label.hunperdmso"),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
			if(getCbbProcedureInfoPojo().getHunPerDmso()!=null){
				temp=temp+getCbbProcedureInfoPojo().getHunPerDmso().toString();
				temp=temp+" "+getText("garuda.processingrep.ml");
			}
			if(getCbbProcedureInfoPojo().getHunPerDmsoper()!=null){
				if(temp.length()>1){
					temp=temp+"/";
				}
				temp=temp+getCbbProcedureInfoPojo().getHunPerDmsoper().toString();
				temp=temp+" "+getText("garuda.processingrep.percentage");
			}
			cel = new PdfPCell(new Phrase(temp,bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
		}
		temp=" ";
		if(getCbbProcedureInfoPojo().getHunPerGlycerol()!=null || getCbbProcedureInfoPojo().getHunPerGlycerolper()!=null){
			cel = new PdfPCell(new Phrase(getText("garuda.cbbprocedures.label.hunperglycerol"),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
			if(getCbbProcedureInfoPojo().getHunPerGlycerol()!=null){
				temp=temp+getCbbProcedureInfoPojo().getHunPerGlycerol().toString();
				temp=temp+" "+getText("garuda.processingrep.ml");
			}
			if(getCbbProcedureInfoPojo().getHunPerGlycerolper()!=null){
				if(temp.length()>1){
					temp=temp+"/";
				}
				temp=temp+getCbbProcedureInfoPojo().getHunPerGlycerolper().toString();
				temp=temp+" "+getText("garuda.processingrep.percentage");
			}
			cel = new PdfPCell(new Phrase(temp,bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
		}
		temp=" ";
		if(getCbbProcedureInfoPojo().getTenPerDextran_40()!=null || getCbbProcedureInfoPojo().getTenPerDextran_40per()!=null){
			cel = new PdfPCell(new Phrase(getText("garuda.cbbprocedures.label.tenperdextran40"),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
			if(getCbbProcedureInfoPojo().getTenPerDextran_40()!=null){
				temp=temp+getCbbProcedureInfoPojo().getTenPerDextran_40().toString();
				temp=temp+" "+getText("garuda.processingrep.ml");
			}
			if(getCbbProcedureInfoPojo().getTenPerDextran_40per()!=null){
				if(temp.length()>1){
					temp=temp+"/";
				}
				temp=temp+getCbbProcedureInfoPojo().getTenPerDextran_40per().toString();
				temp=temp+" "+getText("garuda.processingrep.percentage");
			}
			cel = new PdfPCell(new Phrase(temp,bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
		}
		temp=" ";
		if(getCbbProcedureInfoPojo().getFivePerHumanAlbu()!=null || getCbbProcedureInfoPojo().getFivePerHumanAlbuper()!=null){
			cel = new PdfPCell(new Phrase(getText("garuda.cbbprocedures.label.fiveperhumanalbu"),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
			if(getCbbProcedureInfoPojo().getFivePerHumanAlbu()!=null){
				temp=temp+getCbbProcedureInfoPojo().getFivePerHumanAlbu().toString();
				temp=temp+" "+getText("garuda.processingrep.ml");
			}
			if(getCbbProcedureInfoPojo().getFivePerHumanAlbuper()!=null){
				if(temp.length()>1){
					temp=temp+"/";
				}
				temp=temp+getCbbProcedureInfoPojo().getFivePerHumanAlbuper().toString();
				temp=temp+" "+getText("garuda.processingrep.percentage");
			}
			cel = new PdfPCell(new Phrase(temp,bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
		}
		temp=" ";
		if(getCbbProcedureInfoPojo().getTwenFiveHumAlbu()!=null || getCbbProcedureInfoPojo().getTwenFiveHumAlbuper()!=null){
			cel = new PdfPCell(new Phrase(getText("garuda.cbbprocedures.label.twenfivehumalbu"),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
			if(getCbbProcedureInfoPojo().getTwenFiveHumAlbu()!=null){
				temp=temp+getCbbProcedureInfoPojo().getTwenFiveHumAlbu().toString();
				temp=temp+" "+getText("garuda.processingrep.ml");
			}
			if(getCbbProcedureInfoPojo().getTwenFiveHumAlbuper()!=null){
				if(temp.length()>1){
					temp=temp+"/";
				}
				temp=temp+getCbbProcedureInfoPojo().getTwenFiveHumAlbuper().toString();
				temp=temp+" "+getText("garuda.processingrep.percentage");
			}
			cel = new PdfPCell(new Phrase(temp,bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
		}
		temp=" ";
		if(getCbbProcedureInfoPojo().getPlasmalyte()!=null || getCbbProcedureInfoPojo().getPlasmalyteper()!=null){
			cel = new PdfPCell(new Phrase(getText("garuda.cbbprocedures.label.plasmalyte"),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
			if(getCbbProcedureInfoPojo().getPlasmalyte()!=null){
				temp=temp+getCbbProcedureInfoPojo().getPlasmalyte().toString();
				temp=temp+" "+getText("garuda.processingrep.ml");
			}
			if(getCbbProcedureInfoPojo().getPlasmalyteper()!=null){
				if(temp.length()>1){
					temp=temp+"/";
				}
				temp=temp+getCbbProcedureInfoPojo().getPlasmalyteper().toString();
				temp=temp+" "+getText("garuda.processingrep.percentage");
			}
			cel = new PdfPCell(new Phrase(temp,bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
		}
		temp=" ";
		if(getCbbProcedureInfoPojo().getOthCryoprotectant()!=null || getCbbProcedureInfoPojo().getOthCryoprotectantper()!=null){
			cel = new PdfPCell(new Phrase(getText("garuda.cbbprocedures.label.othcryoprotectant"),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
			table1.addCell("");
			if(getCbbProcedureInfoPojo().getSpecOthCryopro()!=null){
				cel = new PdfPCell(new Phrase(getCbbProcedureInfoPojo().getSpecOthCryopro().toString(),bodyFont));
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table1.addCell(cel);
			if(getCbbProcedureInfoPojo().getOthCryoprotectant()!=null){
				temp=temp+getCbbProcedureInfoPojo().getOthCryoprotectant().toString();
				temp=temp+" "+getText("garuda.processingrep.ml");
			}
			if(getCbbProcedureInfoPojo().getOthCryoprotectantper()!=null){
				if(temp.length()>1){
					temp=temp+"/";
				}
				temp=temp+getCbbProcedureInfoPojo().getOthCryoprotectantper().toString();
				temp=temp+" "+getText("garuda.processingrep.percentage");
			}
			cel = new PdfPCell(new Phrase(temp,bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
			}
		}
		temp=" ";
		addEmptyLine(table1, 1, 2);
		columnWidths = new float[] {50f, 30f};
	       try{
			table1.setWidths(columnWidths);
	       }
	       catch(Exception e){e.printStackTrace();}
		subCatPart.add(table1);
		//subCatPart.add(header);
		table1 = new PdfPTable(2);
		table1.setWidthPercentage(100);
		table1.setHorizontalAlignment(0);
		table1.getDefaultCell().setBorder(0);
		table1.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		header = new Chunk(getText("garuda.productinsert.label.diluents"),bodyBold);
		header.setUnderline(0.1f, -2f);
		cel = new PdfPCell(new Phrase(header));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		table1.addCell("");
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.otheraddtiveshedr")+":",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.volume/percent")+":",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		if(getCbbProcedureInfoPojo().getFivePerDextrose()!=null || getCbbProcedureInfoPojo().getFivePerDextroseper()!=null){
			cel = new PdfPCell(new Phrase(getText("garuda.cbbprocedures.label.fiveperdextrose"),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
			if(getCbbProcedureInfoPojo().getFivePerDextrose()!=null){
				temp=temp+getCbbProcedureInfoPojo().getFivePerDextrose().toString();
				temp=temp+" "+getText("garuda.processingrep.ml");
			}
			if(getCbbProcedureInfoPojo().getFivePerDextroseper()!=null){
				if(temp.length()>1){
					temp=temp+"/";
				}
				temp=temp+getCbbProcedureInfoPojo().getFivePerDextroseper().toString();
				temp=temp+" "+getText("garuda.processingrep.percentage");
			}
			cel = new PdfPCell(new Phrase(temp,bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
		}
		temp=" ";
		if(getCbbProcedureInfoPojo().getPointNinePerNacl()!=null || getCbbProcedureInfoPojo().getPointNinePerNaclper()!=null){
			cel = new PdfPCell(new Phrase(getText("garuda.cbbprocedures.label.pointninepernacl"),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
			if(getCbbProcedureInfoPojo().getPointNinePerNacl()!=null){
				temp=temp+getCbbProcedureInfoPojo().getPointNinePerNacl().toString();
				temp=temp+" "+getText("garuda.processingrep.ml");
			}
			if(getCbbProcedureInfoPojo().getPointNinePerNaclper()!=null){
				if(temp.length()>1){
					temp=temp+"/";
				}
				temp=temp+getCbbProcedureInfoPojo().getPointNinePerNaclper().toString();
				temp=temp+" "+getText("garuda.processingrep.percentage");
			}
			cel = new PdfPCell(new Phrase(temp,bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
		}
		temp=" ";
		if(getCbbProcedureInfoPojo().getOthDiluents()!=null || getCbbProcedureInfoPojo().getOthDiluentsper()!=null){
			cel = new PdfPCell(new Phrase(getText("garuda.cbbprocedures.label.otherdiluents"),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
			table1.addCell("");
			if(getCbbProcedureInfoPojo().getSpecOthDiluents()!=null){
				cel = new PdfPCell(new Phrase(getCbbProcedureInfoPojo().getSpecOthDiluents().toString(),bodyFont));
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table1.addCell(cel);
			if(getCbbProcedureInfoPojo().getOthDiluents()!=null){
				temp=temp+getCbbProcedureInfoPojo().getOthDiluents().toString();
				temp=temp+" "+getText("garuda.processingrep.ml");
			}
			if(getCbbProcedureInfoPojo().getOthDiluentsper()!=null){
				if(temp.length()>1){
					temp=temp+"/";
				}
				temp=temp+getCbbProcedureInfoPojo().getOthDiluentsper().toString();
				temp=temp+" "+getText("garuda.processingrep.percentage");
			}
			cel = new PdfPCell(new Phrase(temp,bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table1.addCell(cel);
			}
		}
		temp=" ";
		columnWidths = new float[] {50f, 30f};
	       try{
			table1.setWidths(columnWidths);
	       }
	       catch(Exception e){e.printStackTrace();}
		subCatPart.add(table1);
		table1 = new PdfPTable(3);
		table1.setWidthPercentage(100);
		table1.setHorizontalAlignment(0);
		table1.getDefaultCell().setBorder(0);
		table1.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		addEmptyLine(table1, 1, 3);
		header = new Chunk(getText("garuda.productinsert.label.recentinfectiousdiseasetesting")+":",bodyBold);
		//header.setUnderline(0.1f, -2f);
		cel = new PdfPCell(new Phrase(header));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		if(getFormversion()!=null && getFormversion().getDynaFormDateStr()!=null)
		{
			cel = new PdfPCell(new Phrase(getFormversion().getDynaFormDateStr()+"    (results attached)",bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
			cel.setBorder(0);
		}
		table1.addCell(cel);
		table1.addCell(" ");
		columnWidths = new float[] {46f,54f , 10f};
	       try{
			table1.setWidths(columnWidths);
	       }
	       catch(Exception e){e.printStackTrace();}
		subCatPart.add(table1);
		
		table1 = new PdfPTable(1);
		table1.setWidthPercentage(100);
		table1.setHorizontalAlignment(0);
		table1.getDefaultCell().setBorder(0);
		table1.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		addEmptyLine(table1, 1, 1);
		Phrase p = new Phrase();
		 p.add(new Chunk(getText("garuda.productinsert.label.warningtext"),bodyFont));
		 p.add(new Chunk(getText("garuda.productinsert.label.warningtext1"),bodyFont).setUnderline(0.1f, -2f));
		 cel = new PdfPCell(p);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		table1.addCell(" ");
		subCatPart.add(table1);
		
		table1 = new PdfPTable(1);
		table1.setWidthPercentage(100);
		table1.setHorizontalAlignment(0);
		table1.getDefaultCell().setBorder(0);
		table1.getDefaultCell().setBorder(Rectangle.NO_BORDER);		
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.requiredstoragetemp"),bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		table1.addCell(" ");		
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.estblprovdcordbldunit"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		table1.addCell(" ");		
		subCatPart.add(table1);
		
		table1 = new PdfPTable(4);
		table1.setWidthPercentage(100);
		table1.setHorizontalAlignment(0);
		table1.getDefaultCell().setBorder(0);
		table1.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		cel = new PdfPCell(new Phrase(getText("garuda.assigntask.label.name")+":",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		
		if(getSite()!=null && getSite().getSiteName()!=null)
		{
			cel = new PdfPCell(new Phrase(getSite().getSiteName(),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}
		cel.setBorder(0);
		table1.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.cbbcode")+":",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		
		if(getSite()!=null && getSite().getSiteIdentifier()!=null)
		{
			cel = new PdfPCell(new Phrase(getSite().getSiteIdentifier(),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}
		cel.setBorder(0);
		table1.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.address")+":",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		
		if(getCbbAddress()!=null && !StringUtils.isEmpty(getCbbAddress().getAddress1()))
		{	
			
			pCel = new Phrase();
			pCel.add(new Chunk(getCbbAddress().getAddress1()+", ",bodyFont));
			 if((!StringUtils.isEmpty(getCbbAddress().getCity()))){
				 pCel.add(new Chunk(getCbbAddress().getCity()+", ",bodyFont));
			 }
			 if((!StringUtils.isEmpty(getCbbAddress().getState()))){
				 pCel.add(new Chunk(getCbbAddress().getState()+", ",bodyFont));
			 }
			 if((!StringUtils.isEmpty(getCbbAddress().getZip()))){
				 pCel.add(new Chunk(getCbbAddress().getZip(),bodyFont));
			 }
			cel = new PdfPCell(pCel);
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}
		cel.setBorder(0);
		table1.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.contactname")+":",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		
		if(getSite()!=null && getSite().getCbb()!=null && getSite().getCbb().getPerson()!=null && (getSite().getCbb().getPerson().getPersonFname()!= null || getSite().getCbb().getPerson().getPersonLname()!= null))
		{	
			pCel = new Phrase();
			if(getSite().getCbb().getPerson().getPersonFname()!= null){
				pCel.add(new Chunk(getSite().getCbb().getPerson().getPersonFname()+" ",bodyFont));
			}
			if(getSite().getCbb().getPerson().getPersonLname()!= null){
				pCel.add(new Chunk(getSite().getCbb().getPerson().getPersonLname(),bodyFont));
			}
			cel = new PdfPCell(pCel);
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}cel.setBorder(0);
		table1.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.cbbdefaults.lable.country")+":",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		
		if(getSite()!=null && getSite().getAddress()!=null && getSite().getAddress().getCountry()!=null)
		{
			cel = new PdfPCell(new Phrase(getSite().getAddress().getCountry(),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}cel.setBorder(0);
		table1.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.contactphone")+":",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		
		if(getSite()!=null && getSite().getAddress()!=null && getSite().getAddress().getPhone()!=null)
		{
			cel = new PdfPCell(new Phrase(getSite().getAddress().getPhone(),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}cel.setBorder(0);
		table1.addCell(cel);		
		columnWidths = new float[] {8f,42f,13f,39f};
	       try{
			table1.setWidths(columnWidths);
	       }
	       catch(Exception e){e.printStackTrace();}
		subCatPart.add(table1);
		
		/*table1 = new PdfPTable(2);
		table1.setWidthPercentage(100);
		table1.setHorizontalAlignment(0);
		table1.getDefaultCell().setBorder(0);
		table1.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.bottomtxt"),smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.notirradiatewarning"),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		
		
		columnWidths = new float[] {70f, 30f};
	       try{
			table1.setWidths(columnWidths);
	       }
	       catch(Exception e){e.printStackTrace();}
		subCatPart.add(table1);*/
		
		table1 = new PdfPTable(3);
		table1.setWidthPercentage(100);
		table1.setHorizontalAlignment(0);
		table1.getDefaultCell().setBorder(0);
		table1.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		addEmptyLine(table1, 1, 3);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.bottomtxtwhole"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		table1.addCell(" ");
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.notirradiatewarning"),bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		Phrase pB = new Phrase();
		pB.add(new Chunk(getText("garuda.productinsert.label.cautiontxt")+"",bodyBold));
		pB.add(new Chunk(getText("garuda.productinsert.label.bottomtxt1"),bodyFont));
		cel = new PdfPCell(pB);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		table1.addCell(" ");
		table1.addCell(" ");
		/*cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.notxRaywarning"),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);*/
		columnWidths = new float[] {68f,7f,25f};
	       try{
			table1.setWidths(columnWidths);
	       }
	       catch(Exception e){e.printStackTrace();}
		subCatPart.add(table1);
		
	/*	table1 = new PdfPTable(1);
		table1.setWidthPercentage(100);
		table1.setHorizontalAlignment(0);
		table1.getDefaultCell().setBorder(0);
		table1.getDefaultCell().setBorder(Rectangle.NO_BORDER);	
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.bottomtxt2"),smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);		
		columnWidths = new float[] {100f};
	       try{
			table1.setWidths(columnWidths);
	       }
	       catch(Exception e){e.printStackTrace();}
		subCatPart.add(table1);		*/
		
		table1 = new PdfPTable(3);
		table1.setWidthPercentage(100);
		table1.setHorizontalAlignment(0);
		table1.getDefaultCell().setBorder(0);
		table1.getDefaultCell().setBorder(Rectangle.NO_BORDER);			
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.bottomstatementtxt"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		table1.addCell("");
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.notxRaywarning"),bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table1.addCell(cel);
		
		columnWidths = new float[] {68f,7f,25f};
	       try{
			table1.setWidths(columnWidths);
	       }
	       catch(Exception e){e.printStackTrace();}
		subCatPart.add(table1);
		table1 = new PdfPTable(3);
		table1.setWidthPercentage(100);
		table1.setHorizontalAlignment(0);
		table1.getDefaultCell().setBorder(0);
		table1.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table1.addCell(" ");
		subCatPart.add(table1);
		subCatPart.newPage();
		table = new PdfPTable(6);
		table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		/*Phrase p2 = new Phrase();
		Chunk superScrChnk1=new Chunk(getText("garuda.nmdp.productinsert.headingSupScrpPrt"),bodyFont);
		superScrChnk1.setTextRise(5f);
		p1.add(new Chunk(getText("garuda.nmdp.productinsert.heading"),headerFont));
		p1.add(superScrChnk);*/
		cel = new PdfPCell(p1);
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.productinsertreportnmdp"),headerFont));
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.productinsertreporthdrtxtnmdp"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell(" ");
		subCatPart.add(table);
		
		table = new PdfPTable(4);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		
		pCel = new Phrase();
		pCel.add(new Chunk(getText("garuda.recipient&tcInfo.label.patientid")+"  ",bodyBold));
		//cel = new PdfPCell(new Phrase(getText("garuda.recipient&tcInfo.label.patientid" ),bodyBold));
		//cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		//cel.setBorder(0);
		//table.addCell(cel);
		 /*if(recipientInfo!=null && !StringUtils.isEmpty(recipientInfo.getRecipientId())){
				pCel.add(new Chunk(recipientInfo.getRecipientId(),bodyFont));
				}*/
			if(getCtOrderDetailsList()!=null && getCtOrderDetailsList().size()>0){
				Object[] obj = (Object[]) getCtOrderDetailsList().get(0);
				if(obj[13]!=null && !(obj[13].toString().equals("Not Provided"))){
					pCel.add(new Chunk(obj[13].toString(),bodyFont));
				}
				else{
					pCel.add(new Chunk("",bodyFont));
				}
			}
			else{
				pCel.add(new Chunk(" ",bodyFont));
			}
			cel = new PdfPCell(pCel);
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table.addCell(cel);
			table.addCell("");
			pCel = new Phrase();
			pCel.add(new Chunk(getText("garuda.productinsert.label.intendedrecpname")+":  ",bodyBold));
		//cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.intendedrecpname" )+":",bodyBold));
		//cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		//cel.setBorder(0);
		//table.addCell(cel);
			if(getCtOrderDetailsList()!=null && getCtOrderDetailsList().size()>0){
				Object[] obj = (Object[]) getCtOrderDetailsList().get(0);
				if(obj[16]!=null && !(obj[16].toString().equals("Not Provided"))){
					pCel.add(new Chunk(obj[16].toString(),bodyFont));
				}
				else{
					pCel.add(new Chunk("",bodyFont));
				}
			}else{
				pCel.add(new Chunk("",bodyFont));
			}
		cel = new PdfPCell(pCel);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		pCel = new Phrase();
		pCel.add(new Chunk(getText("garuda.nmdp.label.cburegid")+"  ",bodyBold));
		//cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.cburegid")+":",bodyBold));
		//cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		//cel.setBorder(0);
		//table.addCell(cel);
		//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
		
		if(getCdrCbuPojo()!=null && getCdrCbuPojo().getRegistryId()!=null)
		{
			pCel.add(new Chunk(getCdrCbuPojo().getRegistryId(),bodyFont));
		}
		else
		{
			pCel.add(new Chunk(" ",bodyFont));
		}
		cel = new PdfPCell(pCel);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		pCel = new Phrase();
		pCel.add(new Chunk(getText("garuda.cbuentry.label.idbag")+":  ",bodyBold));
		//cel = new PdfPCell(new Phrase(getText("garuda.evaluHistory.report.uniqueProdIdonBag")+":",bodyBold));
		//cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		//cel.setBorder(0);
		//table.addCell(cel);
		
		if(getCdrCbuPojo()!=null && getCdrCbuPojo().getNumberOnCbuBag()!=null)
		{
			pCel.add(new Chunk(getCdrCbuPojo().getNumberOnCbuBag(),bodyFont));	
		}
		else
		{
			pCel.add(new Chunk(" ",bodyFont));
		}
		cel = new PdfPCell(pCel);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);	
		table.addCell(cel);
		table.addCell("");
		columnWidths = new float[] {45f,5f,45f,5f};
	       try{
			table.setWidths(columnWidths);
	       }
	       catch(Exception e){e.printStackTrace();}
		
		tableWidBrd = new PdfPTable(1);
		tableWidBrd.setWidthPercentage(100);
		tableWidBrd.setHorizontalAlignment(0);
		tableWidBrd.getDefaultCell().setBorder(0);
		tableWidBrd.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		celWidBrd = new PdfPCell(table);
		celWidBrd.setHorizontalAlignment(Element.ALIGN_LEFT);
		celWidBrd.setBorder(Rectangle.BOX);
		tableWidBrd.addCell(celWidBrd);
		subCatPart.add(tableWidBrd);
		
		table = new PdfPTable(4);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);			
		
		pCel = new Phrase();
		pCel.add(new Chunk(getText("garuda.productinsert.label.shipto")+":  ",bodyFont));
		/*cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.shipto" )+":",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);*/
		if(getCtOrderDetailsList()!=null && getCtOrderDetailsList().size()>0){
			Object[] obj = (Object[]) getCtOrderDetailsList().get(0);
			if(obj[84]!=null && !(obj[84].toString().equals("Not Provided"))){
				pCel.add(new Chunk(obj[84].toString(),bodyFont));
			}
			else{
				pCel.add(new Chunk("",bodyFont));
			}
		}else{
			pCel.add(new Chunk("",bodyFont));
		}
		cel = new PdfPCell(pCel);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell(" ");
		table.addCell(" ");
		table.addCell(" ");
		
		pCel = new Phrase();
		pCel.add(new Chunk(getText("garuda.tcorderdetails.label.centername")+":  ",bodyFont));
		/*cel = new PdfPCell(new Phrase(getText("garuda.tcorderdetails.label.centername" )+":",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);*/
		if(getCtOrderDetailsList()!=null && getCtOrderDetailsList().size()>0){
			Object[] obj = (Object[]) getCtOrderDetailsList().get(0);
			if(obj[28]!=null && !(obj[28].toString().equals("Not Provided"))){
				pCel.add(new Chunk(obj[28].toString(),bodyFont));
			}
			else{
				pCel.add(new Chunk("",bodyFont));
			}
		}else{
			pCel.add(new Chunk("",bodyFont));
		}
		cel = new PdfPCell(pCel);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell(" ");
		table.addCell(" ");
		table.addCell(" ");
		
		pCel = new Phrase();
		pCel.add(new Chunk(getText("garuda.productinsert.label.address")+":  ",bodyFont));
		/*cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.address" )+":",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);*/
		if(getCtOrderDetailsList()!=null && getCtOrderDetailsList().size()>0){
			Object[] obj = (Object[]) getCtOrderDetailsList().get(0);
			String addressTc="";
			if(obj[29]!=null && !(obj[29].toString().equals("Not Provided"))){
				addressTc+=obj[29].toString()+"\n";
			}
			if(obj[30]!=null && !(obj[30].toString().equals("Not Provided"))){
				addressTc+="                "+obj[30].toString()+"\n";
			}
			if(obj[31]!=null && !(obj[31].toString().equals("Not Provided"))){
				addressTc+="                "+obj[31].toString()+"\n";
			}
			if(obj[34]!=null && !(obj[34].toString().equals("Not Provided"))){
				addressTc+="                "+obj[34].toString();
			}
			/*if(!StringUtils.isEmpty(getTcSite().getAddress().getCity())){
				addressTc+="                 "+getTcSite().getAddress().getCity()+", ";
			}
			if(!StringUtils.isEmpty(getTcSite().getAddress().getState())){
				addressTc+=getTcSite().getAddress().getState()+", ";
			}
			if(!StringUtils.isEmpty(getTcSite().getAddress().getZip())){
				addressTc+=getTcSite().getAddress().getZip()+"\n";
			}
			if(!StringUtils.isEmpty(getTcSite().getAddress().getCountry())){
				addressTc+="                 "+getTcSite().getAddress().getCountry();
			}*/
			pCel.add(new Chunk(addressTc,bodyFont));
		}else{
			pCel.add(new Chunk("",bodyFont));
		}
		cel = new PdfPCell(pCel);
		//cel = new PdfPCell(new Phrase(temp,bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell(" ");
		table.addCell(" ");
		table.addCell(" ");
		columnWidths = new float[] {85f,5f,5f,5f};
		try
		{
			table.setWidths(columnWidths);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		subCatPart.add(table);
		
		table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		addEmptyLine(table, 1, 2);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.attention"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("________________________________________________________");
		columnWidths = new float[] {10f,90f};
		try
		{
			table.setWidths(columnWidths);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		subCatPart.add(table);
		table = new PdfPTable(6);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.addCell(" ");
		table.addCell(" ");
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.persntoacceptcordship"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell(" ");
		table.addCell(" ");
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.phonenum"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {5f,5f,35f,10f,10f,30f};
		try
		{
			table.setWidths(columnWidths);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		subCatPart.add(table);
		table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		addEmptyLine(table, 1, 2);
		cel = new PdfPCell(new Phrase(getText("garuda.esignature.label.sign")+":",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("_________________________/___________");
		columnWidths = new float[] {10f,90f};
		try
		{
			table.setWidths(columnWidths);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		subCatPart.add(table);
		table = new PdfPTable(6);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.addCell(" ");
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.percompform"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.cbuhistory.label.date"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell(" ");
		table.addCell(" ");
		table.addCell(" ");
		columnWidths = new float[] {10f,35f,35f,10f,5f,5f};
		try
		{
			table.setWidths(columnWidths);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		subCatPart.add(table);
		table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		addEmptyLine(table, 1, 2);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.verby"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("_________________________/___________");
		columnWidths = new float[] {10f,90f};
		try
		{
			table.setWidths(columnWidths);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		subCatPart.add(table);
		table = new PdfPTable(6);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.addCell(" ");
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.percompform"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.cbuhistory.label.date"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell(" ");
		table.addCell(" ");
		table.addCell(" ");
		columnWidths = new float[] {10f,35f,35f,10f,5f,5f};
		try
		{
			table.setWidths(columnWidths);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		subCatPart.add(table);
		subCatPart.newPage();
		}
			
		/*-----------------------------Final decl of eligibility REPORT------------*/
		
		/*
		table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		cel = new PdfPCell(new Phrase(getText("garuda.idsReport.heading"),headerFont));
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.fodecordbloodunit"),headerFont));
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.fodedocmstaccproduct"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell(" ");
		subCatPart.add(table);
		
		
		table = new PdfPTable(4);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.nmdpintendedrecpid")+":",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		if(getCdrCbuPojo()!=null && getCdrCbuPojo().getRegistryId()!=null)
		{
			cel = new PdfPCell(new Phrase(getCdrCbuPojo().getRegistryId(),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);	
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.cbuentry.label.cordcbucollecdate")+":",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		if(getCdrCbuPojo().getSpecimen().getSpecCollDate()!=null)
		{
		dateString = getCdrCbuPojo().getSpecimen().getSpecCollDate().toLocaleString().split(" ");
		temp=dateString[0]+dateString[1]+dateString[2];
		cel = new PdfPCell(new Phrase(temp,bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.nmdpcbuid")+":",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
		
		if(getCdrCbuPojo()!=null && getCdrCbuPojo().getRegistryId()!=null)
		{
			cel = new PdfPCell(new Phrase(getCdrCbuPojo().getRegistryId(),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.unitreport.label.localcbuid")+":",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		if(getCdrCbuPojo()!=null && getCdrCbuPojo().getLocalCbuId()!=null)
		{
			cel = new PdfPCell(new Phrase(getCdrCbuPojo().getLocalCbuId(),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}
		table.addCell(cel);
		tableWidBrd = new PdfPTable(1);
		tableWidBrd.setWidthPercentage(100);
		tableWidBrd.setHorizontalAlignment(0);
		tableWidBrd.getDefaultCell().setBorder(0);
		tableWidBrd.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		celWidBrd = new PdfPCell(table);
		celWidBrd.setHorizontalAlignment(Element.ALIGN_LEFT);
		celWidBrd.setBorder(Rectangle.BOX);
		tableWidBrd.addCell(celWidBrd);
		subCatPart.add(tableWidBrd);
				
		table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.impnoticetotc"),footerFont));
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.impnoticetotctxt"),footerFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {40f , 60f};
		try
		{
			table.setWidths(columnWidths);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		subCatPart.add(table);
		
		
		table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.fdacondionaltxt"),footerFont));
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		header = new Chunk(getText("SECTION ONE"),subFont);
		header.setUnderline(0.1f, -2f);
		cel = new PdfPCell(new Phrase(header));
		addEmptyLine(table, 1 , 1);
		cel = new PdfPCell(section1Bar);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		subCatPart.add(table);
			
		
		table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.cbb"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		if(getSites()!=null && getSites().size()>0)
		{
		for(SitePojo s : getSites()){
		if(getCdrCbuPojo()!=null && getCdrCbuPojo().getFkCbbId()!=null)
		{
			if(s.getSiteId()==getCdrCbuPojo().getFkCbbId()){
				cel = new PdfPCell(new Phrase(s.getSiteName(),bodyFont));
			}
			else{
				cel = new PdfPCell(new Phrase(" ",bodyFont));
			}
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}
		
		}
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		subCatPart.add(table);
			
		
		table = new PdfPTable(4);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.address"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		if(getCbbAddress()!=null && getCbbAddress().getAddress1()!=null)
		{
			cel = new PdfPCell(new Phrase(getCbbAddress().getAddress1(),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.cbbdefaults.lable.country"),bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		if(getCbbAddress()!=null && getCbbAddress().getCountry()!=null)
		{
			cel = new PdfPCell(new Phrase(getCbbAddress().getCountry(),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}cel.setBorder(0);
		table.addCell(cel);
		subCatPart.add(table);
		
		table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.cbuDetermination"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		subCatPart.add(table);
		
		
		table = new PdfPTable(3);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		if(getCdrCbuPojo()!=null && getCdrCbuPojo().getFkCordCbuEligible()!=null && getCdrCbuPojo().getFkCordCbuEligibleReason()!=null)
		{
			if(getCdrCbuPojo().getFkCordCbuEligible()== getCdrCbuPojo().getFkCordCbuEligibleReason() || getCdrCbuPojo().getFkCordCbuEligible().equals(getCdrCbuPojo().getFkCordCbuEligibleReason())){
				cel = new PdfPCell(checkBoxChecked);
			}
			else{
				cel = new PdfPCell(checkBoxUnChecked);
			}
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}
		table.addCell(cel);		
				
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.eligible"),bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText(""),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		if(getCdrCbuPojo()!=null && getCdrCbuPojo().getFkCordCbuEligible()!=null && String.valueOf(getCdrCbuPojo().getIncompleteReasonId())!=null)
		{
			if(getCdrCbuPojo().getFkCordCbuEligible()== getCdrCbuPojo().getIncompleteReasonId() || getCdrCbuPojo().getFkCordCbuEligible().equals(getCdrCbuPojo().getIncompleteReasonId())){
				cel = new PdfPCell(checkBoxChecked);
			}
			else{
				cel = new PdfPCell(checkBoxUnChecked);
			}
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.incomplete"),bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText(""),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		columnWidths = new float[] {3f,30f,67f};
		try
		{
			table.setWidths(columnWidths);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		subCatPart.add(table);
		
		table = new PdfPTable(3);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		cel = new PdfPCell(new Phrase(getText(""),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		
		if(getCdrCbuPojo()!=null && getCdrCbuPojo().getFkCordCbuEligible()!=null && String.valueOf(getCdrCbuPojo().getIncompleteReasonId())!=null)
		{
			if(getCdrCbuPojo().getFkCordCbuEligible()== getCdrCbuPojo().getIncompleteReasonId() || getCdrCbuPojo().getFkCordCbuEligible().equals(getCdrCbuPojo().getIncompleteReasonId())){
				cel = new PdfPCell(checkBoxChecked);
			}
			else{
				cel = new PdfPCell(checkBoxUnChecked);
			}
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);

		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.incomplete_ques1"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText(""),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		
		if(getCdrCbuPojo()!=null && getCdrCbuPojo().getFkCordCbuEligible()!=null && String.valueOf(getCdrCbuPojo().getIncompleteReasonId())!=null)
		{
			if(getCdrCbuPojo().getFkCordCbuEligible()== getCdrCbuPojo().getIncompleteReasonId()){
				cel = new PdfPCell(checkBoxChecked);
			}
			else{
				cel = new PdfPCell(checkBoxUnChecked);
			}
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}
		
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.incomplete_ques2"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		columnWidths = new float[] {3f,3f,94f};
		try
		{
			table.setWidths(columnWidths);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		subCatPart.add(table);
		
		
		
		table = new PdfPTable(3);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		if(getCdrCbuPojo()!=null && getCdrCbuPojo().getFkCordCbuEligible()!=null && String.valueOf(getCdrCbuPojo().getInEligiblePkid())!=null)
		{
			if(getCdrCbuPojo().getFkCordCbuEligible()== getCdrCbuPojo().getInEligiblePkid() || getCdrCbuPojo().getFkCordCbuEligible().equals(getCdrCbuPojo().getInEligiblePkid())){
				cel = new PdfPCell(checkBoxChecked);
			}
			else{
				cel = new PdfPCell(checkBoxUnChecked);
			}
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.ineligible"),bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText(""),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		if(getCdrCbuPojo()!=null && getCdrCbuPojo().getFkCordCbuEligible()!=null && String.valueOf(getCdrCbuPojo().getNotCollectedToPriorReasonId())!=null)
		{
			if(getCdrCbuPojo().getFkCordCbuEligible()== getCdrCbuPojo().getNotCollectedToPriorReasonId() || getCdrCbuPojo().getFkCordCbuEligible().equals(getCdrCbuPojo().getNotCollectedToPriorReasonId())){
				cel = new PdfPCell(checkBoxChecked);
			}
			else{
				cel = new PdfPCell(checkBoxUnChecked);
			}
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}
		
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.notapplicable"),bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText(""),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		columnWidths = new float[] {3f,30f,67f};
		try
		{
			table.setWidths(columnWidths);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		subCatPart.add(table);
		
		
		
		
		table = new PdfPTable(3);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		cel = new PdfPCell(new Phrase(" ",smallBold));
		if(getDeclPojo().getMrqQues1b()!=null){
			if(getDeclPojo().getMrqQues1b()== false){
				cel = new PdfPCell(checkBoxChecked);
			}
			else{
				cel = new PdfPCell(checkBoxUnChecked);
			}
		}
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.notapplicable"),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		if(getDeclPojo().getMrqQues1b()!=null){
			if(getDeclPojo().getMrqQues1b()== true){
				cel = new PdfPCell(checkBoxChecked);
			}
			else{
				cel = new PdfPCell(checkBoxUnChecked);
			}
		}
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		subCatPart.add(table);
		
		
		
		
		table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		header = new Chunk(getText("SECTION TWO"),subFont);
		header.setUnderline(0.1f, -2f);
		cel = new PdfPCell(new Phrase(header));
		addEmptyLine(table, 1 , 1);
		cel = new PdfPCell(section2Bar);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.urgentTransplantationNeed"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.detailDonorScreening"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.reasonForIncompleteCbu"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		addEmptyLine(table, 1, 1);
		header = new Chunk(getText("garuda.fdoe.level.sectionformname1"),bodyBold);
		header.setUnderline(0.1f, -2f);
		cel = new PdfPCell(new Phrase(header));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		subCatPart.add(table);
		
		
		table = new PdfPTable(3);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.addCell("");
		cel = new PdfPCell(new Phrase(" ",bodyFont));
		if(getDeclPojo().getIdmQues7a()!=null){	
			if(getDeclPojo().getIdmQues7a()==false){
				cel = new PdfPCell(checkBoxChecked);
			}
			else{
				cel = new PdfPCell(checkBoxUnChecked);
			}
		}
		else{
			cel = new PdfPCell(checkBoxUnChecked);
		}
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.sectionformname1_ques1"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		table.addCell("");
		cel = new PdfPCell(new Phrase(" ",bodyFont));
		if(getDeclPojo().getIdmQues6()!=null){	
			if(getDeclPojo().getIdmQues6()==false){
				cel = new PdfPCell(checkBoxChecked);
			}
			else{
				cel = new PdfPCell(checkBoxUnChecked);
			}
		}
		else{
			cel = new PdfPCell(checkBoxUnChecked);
		}
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.sectionformname1_ques2"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		table.addCell("");
		cel = new PdfPCell(new Phrase(" ",bodyFont));
		if(getDeclPojo().getIdmQues8()!=null){	
			if(getDeclPojo().getIdmQues8()==true){
				cel = new PdfPCell(checkBoxChecked);
			}
			else{
				cel = new PdfPCell(checkBoxUnChecked);
			}
		}
		else{
			cel = new PdfPCell(checkBoxUnChecked);
		}
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.sectionformname1_ques3"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {3f,3f,94f};
		try
		{
			table.setWidths(columnWidths);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		subCatPart.add(table);
		
		
		
		table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		cel = new PdfPCell(new Phrase(" ",bodyFont));
		if(getDeclPojo().getIdmQues8()!=null){
			if(getDeclPojo().getIdmQues8()==false){
				cel = new PdfPCell(new Phrase(" ",bodyFont));
			}
			else{
				cel = new PdfPCell(new Phrase(" ",bodyFont));
			}
		}
		else{
			cel = new PdfPCell(checkBoxUnChecked);
		}
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.sectionformname1_ques4"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {3f,97f};
		try
		{
			table.setWidths(columnWidths);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		subCatPart.add(table);
		
		
		
		table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		addEmptyLine(table, 1, 1);
		header = new Chunk(getText("garuda.fdoe.level.sectionformname2"),bodyBold);
		header.setUnderline(0.1f, -2f);
		cel = new PdfPCell(new Phrase(header));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		subCatPart.add(table);
		
		
		table = new PdfPTable(3);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.addCell("");
		cel = new PdfPCell(new Phrase(" ",bodyFont));
		if(getDeclPojo().getMrqQues2()!=null){
			if(getDeclPojo().getMrqQues2()==true){
				cel = new PdfPCell(checkBoxChecked);
			}
			else{
				cel = new PdfPCell(checkBoxUnChecked);
			}
		}
		else{
			cel = new PdfPCell(checkBoxUnChecked);
		}
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.sectionformname2_ques1"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		table.addCell("");
		cel = new PdfPCell(new Phrase(" ",bodyFont));
		if(getDeclPojo().getPhyFindQues5()!=null){
			if(getDeclPojo().getPhyFindQues5()==true){
				cel = new PdfPCell(checkBoxChecked);
			}
			else{
				cel = new PdfPCell(checkBoxUnChecked);
			}
		}
		else{
			cel = new PdfPCell(checkBoxUnChecked);
		}
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.sectionformname2_ques2"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		table.addCell("");
		cel = new PdfPCell(new Phrase(" ",bodyFont));
		if(getDeclPojo().getPhyFindQues4()!=null){
			if(getDeclPojo().getPhyFindQues4()==true){
				cel = new PdfPCell(checkBoxChecked);
			}
			else{
				cel = new PdfPCell(checkBoxUnChecked);
			}
		}
		else{
			cel = new PdfPCell(checkBoxUnChecked);
		}
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.sectionformname2_ques3"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		table.addCell("");
		cel = new PdfPCell(new Phrase(" ",bodyFont));
		if(getDeclPojo().getMrqQues2()!=null){
			if(getDeclPojo().getMrqQues2()== false){
				cel = new PdfPCell(checkBoxChecked);
			}
			else{
				cel = new PdfPCell(checkBoxUnChecked);
			}
		}
		else{
			cel = new PdfPCell(checkBoxUnChecked);
		}
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.sectionformname2_ques4"),bodyFont));;
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {3f,3f,94f};
		try
		{
			table.setWidths(columnWidths);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		subCatPart.add(table);
		
		
		
		table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		cel = new PdfPCell(new Phrase(" ",bodyFont));
		if(getDeclPojo().getMrqQues2()!=null){
			if(getDeclPojo().getMrqQues2()== false){
				cel = new PdfPCell(new Phrase(" ",bodyFont));
			}
			else{
				cel = new PdfPCell(new Phrase(" ",bodyFont));
			}
		}
		else{
			cel = new PdfPCell(checkBoxUnChecked);
		}
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.sectionformname2_ques5"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		columnWidths = new float[] {3f,97f};
		try
		{
			table.setWidths(columnWidths);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		subCatPart.add(table);
		subCatPart.newPage();
			
		
				
		table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		header = new Chunk(getText("SECTION THREE"),subFont);
		header.setUnderline(0.1f, -2f);
		cel = new PdfPCell(new Phrase(header));
		table.addCell("");
		table.addCell("");
		table.addCell("");
		cel = new PdfPCell(new Phrase(getText("garuda.idsReport.heading"),headerFont));
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(section3Bar);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		header = new Chunk(getText("garuda.fdoe.level.sectionformname3"),bodyBold);
		header.setUnderline(0.1f, -2f);
		cel = new PdfPCell(new Phrase(header));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		subCatPart.add(table);
		
		
		table = new PdfPTable(3);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		cel = new PdfPCell(new Phrase(getText("-------------------"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("-------------------"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("-------------------"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.name"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.signature"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.date"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		subCatPart.add(table);
				
		table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		addEmptyLine(table, 1, 1);
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.req"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		subCatPart.add(table);
		
		
		table = new PdfPTable(4);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		cel = new PdfPCell(new Phrase(" ",bodyFont));	
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.estab"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(" ",bodyFont));
		if(getDeclPojo().getIdmQues7a()!=null){
			if(getDeclPojo().getIdmQues7a()==false){
				cel = new PdfPCell(new Phrase(" ",bodyFont));
			}
			else{
				cel = new PdfPCell(new Phrase(" ",bodyFont));
			}
		}
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		
		
		table.addCell("-------------------");
		
		
		cel = new PdfPCell(new Phrase(" ",bodyFont));
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.compestab"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(" ",bodyFont));
		if(getDeclPojo().getIdmQues7a()!=null){
			if(getDeclPojo().getIdmQues7a()==false){
				cel = new PdfPCell(new Phrase(" ",bodyFont));
			}
			else{
				cel = new PdfPCell(new Phrase(" ",bodyFont));
			}
		}
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.estabname"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(" ",bodyFont));
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.nmdpadd"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(" ",bodyFont));
		if(getDeclPojo().getIdmQues7a()!=null){
			if(getDeclPojo().getIdmQues7a()==false){
				cel = new PdfPCell(new Phrase(" ",bodyFont));
			}
			else{
				cel = new PdfPCell(new Phrase(" ",bodyFont));
			}
		}
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		table.addCell("-------------------");
		
		table.addCell(" ");
		table.addCell(" ");
		table.addCell(" ");
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.estabadd"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		columnWidths = new float[] {3f,63f,17f,20f};
		try
		{
			table.setWidths(columnWidths);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		tableWidBrd = new PdfPTable(1);
		tableWidBrd.setWidthPercentage(100);
		tableWidBrd.setHorizontalAlignment(0);
		tableWidBrd.getDefaultCell().setBorder(0);
		tableWidBrd.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		celWidBrd = new PdfPCell(table);
		celWidBrd.setHorizontalAlignment(Element.ALIGN_LEFT);
		celWidBrd.setBorder(Rectangle.BOX);
		tableWidBrd.addCell(celWidBrd);
		subCatPart.add(tableWidBrd);
		
		subCatPart.newPage();
		
		-----------------------------Product Tag------------
		
		table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		cel = new PdfPCell(new Phrase(getText("garuda.pdf.nmdp.producttag"),headerFont));
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		subCatPart.add(table);
		
		
		table = new PdfPTable(4);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		
		cel = new PdfPCell(new Phrase(getText("garuda.evaluHistory.report.patientid"),bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		if(getCtOrderDetailsList()!=null && getCtOrderDetailsList().size()>13){
			if(getCtOrderDetailsList().get(13)!=null){
				cel = new PdfPCell(new Phrase(getCtOrderDetailsList().get(13).toString(),bodyFont));
				}
			else{
				cel = new PdfPCell(new Phrase(" ",bodyFont));
			}
			}
			else{
				cel = new PdfPCell(new Phrase(" ",bodyFont));
			}
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
			
		cel = new PdfPCell(new Phrase(getText("garuda.cbbdefaults.lable.cbbregistryname")+":",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
			
		if(getSite()!=null && getSite().getSiteName()!=null)
		{
				cel = new PdfPCell(new Phrase(getSite().getSiteName(),bodyFont));
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}
		table.addCell(cel);
			
		cel = new PdfPCell(new Phrase(getText("garuda.cbuentry.label.registrycbuid")+":",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
			
		if(getCdrCbuPojo().getRegistryId()!= null)
		{
				cel = new PdfPCell(new Phrase(getCdrCbuPojo().getRegistryId(),bodyFont));
			}
		else{
					cel = new PdfPCell(new Phrase(" ",bodyFont));
			}
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		table.addCell("");
			
		cel = new PdfPCell(new Phrase(getText("garuda.evaluHistory.report.uniqueProdIdonBag"),bodyBold));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table.addCell(cel);
			//Bold = new Phrase(getCdrCbuPojo().getLocalCbuId(),smallBold);
			if(getCdrCbuPojo()!=null && getCdrCbuPojo().getNumberOnCbuBag()!=null)
			{
			cel = new PdfPCell(new Phrase(getCdrCbuPojo().getNumberOnCbuBag(),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			}
			else
			{
				cel = new PdfPCell(new Phrase(" ",bodyFont));
				cel.setBorder(0);
			}
			table.addCell(cel);
			table.addCell("");
			table.addCell("");
			tableWidBrd = new PdfPTable(1);
			tableWidBrd.setWidthPercentage(100);
			tableWidBrd.setHorizontalAlignment(0);
			tableWidBrd.getDefaultCell().setBorder(0);
			tableWidBrd.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			celWidBrd = new PdfPCell(table);
			celWidBrd.setHorizontalAlignment(Element.ALIGN_LEFT);
			celWidBrd.setBorder(Rectangle.BOX);
			tableWidBrd.addCell(celWidBrd);
			subCatPart.add(tableWidBrd);
		addEmptyLine(table,1,1);
		
		table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		cel = new PdfPCell(new Phrase(getText("garuda.pdf.producttag.tag1"),bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		subCatPart.add(table);
		
		table = new PdfPTable(4);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		if(getDeclPojo().getIdmQues7a()!=null && getDeclPojo().getIdmQues6()!=null && getDeclPojo().getMrqQues2()!=null){
			if(getDeclPojo().getIdmQues7a()== false || getDeclPojo().getIdmQues6() == false || getDeclPojo().getMrqQues2() == false){
				cel = new PdfPCell(checkBoxChecked);
			}
			else{
				cel = new PdfPCell(checkBoxUnChecked);
			}
		}
		else{
			cel = new PdfPCell(checkBoxUnChecked);
		}
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.applicable_hpc"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		if(getDeclPojo().getIdmQues7a()!=null && getDeclPojo().getIdmQues6()!=null && getDeclPojo().getMrqQues2()!=null){
			if(getDeclPojo().getIdmQues7a()== false || getDeclPojo().getIdmQues6() == false || getDeclPojo().getMrqQues2() == false){
				cel = new PdfPCell(checkBoxUnChecked);
			}
			else{
				cel = new PdfPCell(checkBoxChecked);
			}
		}
		else{
			cel = new PdfPCell(checkBoxChecked);
		}
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.nonapplicable_hpc"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {4f,46f,4f,46f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
		subCatPart.add(table);
		
		table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		cel = new PdfPCell(tag1Image);
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setVerticalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		subCatPart.add(table);
		
		table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		cel = new PdfPCell(new Phrase(getText("garuda.pdf.producttag.tag2"),bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		subCatPart.add(table);
		table = new PdfPTable(4);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		if(getDeclPojo().getIdmQues7a()!=null && getDeclPojo().getIdmQues6()!=null && getDeclPojo().getMrqQues2()!=null){
			if(getDeclPojo().getIdmQues7a()== false || getDeclPojo().getIdmQues6() == false || getDeclPojo().getMrqQues2() == false){
				cel = new PdfPCell(checkBoxChecked);
			}
			else{
				cel = new PdfPCell(checkBoxUnChecked);
			}
		}
		else{
			cel = new PdfPCell(checkBoxUnChecked);
		}
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.applicable_hpc"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		if(getDeclPojo().getIdmQues7a()!=null && getDeclPojo().getIdmQues6()!=null && getDeclPojo().getMrqQues2()!=null){
			if(getDeclPojo().getIdmQues7a()== false || getDeclPojo().getIdmQues6() == false || getDeclPojo().getMrqQues2() == false){
				cel = new PdfPCell(checkBoxUnChecked);
			}
			else{
				cel = new PdfPCell(checkBoxChecked);
			}
		}
		else{
			cel = new PdfPCell(checkBoxChecked);
		}
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.nonapplicable_hpc"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {4f,46f,4f,46f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
		subCatPart.add(table);
		table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		cel = new PdfPCell(tag2Image);
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setVerticalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		subCatPart.add(table);
		
		table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		cel = new PdfPCell(new Phrase(getText("garuda.pdf.producttag.tag3"),bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		subCatPart.add(table);
		table = new PdfPTable(4);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		if(getDeclPojo().getIdmQues7a()!=null && getDeclPojo().getIdmQues6()!=null && getDeclPojo().getMrqQues2()!=null){
			if(getDeclPojo().getIdmQues7a()== false || getDeclPojo().getIdmQues6() == false || getDeclPojo().getMrqQues2() == false){
				cel = new PdfPCell(checkBoxChecked);
			}
			else{
				cel = new PdfPCell(checkBoxUnChecked);
			}
		}
		else{
			cel = new PdfPCell(checkBoxUnChecked);
		}
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.applicable_hpc"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		if(getDeclPojo().getIdmQues7a()!=null && getDeclPojo().getIdmQues6()!=null && getDeclPojo().getMrqQues2()!=null){
			if(getDeclPojo().getIdmQues7a()== false || getDeclPojo().getIdmQues6() == false || getDeclPojo().getMrqQues2() == false){
				cel = new PdfPCell(checkBoxUnChecked);
			}
			else{
				cel = new PdfPCell(checkBoxChecked);
			}
		}
		else{
			cel = new PdfPCell(checkBoxChecked);
		}
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.fdoe.level.nonapplicable_hpc"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {4f,46f,4f,46f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
		subCatPart.add(table);
		table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		cel = new PdfPCell(tag3Image);
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setVerticalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		subCatPart.add(table);

		
		subCatPart.newPage();*/
		else if(reportGen==2){
		/*-----------------------IDM Report-----------------*/
		table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		cel = new PdfPCell(new Phrase(getText("garuda.pdf.nmdp.prodInsertCont")+":",footerFont));
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.productinsertreportIDM"),headerFont));
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		subCatPart.add(table);
		
		
		table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		cel = new PdfPCell(new Phrase(getText("garuda.comprehensivereport.cbuinfo.cbb"),headerFont));
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		if(getSite()!=null && getSite().getSiteIdentifier()!=null)
		{
			cel = new PdfPCell(new Phrase(getSite().getSiteIdentifier(),bodyFont));
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {60f,40f};
	       try{
			table.setWidths(columnWidths);
	       }
	       catch(Exception e){e.printStackTrace();}
		subCatPart.add(table);
		
		table = new PdfPTable(6);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.nmdpcbuid")+":",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		if(getCdrCbuPojo()!=null && getCdrCbuPojo().getRegistryId()!=null)
		{
			cel = new PdfPCell(new Phrase(getCdrCbuPojo().getRegistryId(),bodyFont));
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		table.addCell("");
		cel = new PdfPCell(new Phrase(getText("garuda.unitreport.label.localcbuid")+":",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		if(getCdrCbuPojo()!=null && getCdrCbuPojo().getLocalCbuId()!=null)
		{
			cel = new PdfPCell(new Phrase(getCdrCbuPojo().getLocalCbuId(),bodyFont));
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.nmdpmaternalid")+":",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		if(getCdrCbuPojo()!=null && getCdrCbuPojo().getNmdpMaternalId()!=null)
		{
			cel = new PdfPCell(new Phrase(getCdrCbuPojo().getNmdpMaternalId(),bodyFont));
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.formnum")+":",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		cel = new PdfPCell(new Phrase(getText("garuda.unitreport.label.localmaternalid")+":",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		if(getCdrCbuPojo()!=null && getCdrCbuPojo().getLocalMaternalId()!=null)
		{
			cel = new PdfPCell(new Phrase(getCdrCbuPojo().getLocalMaternalId(),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.validationprtrule")+":",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.donrefnumber")+":",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		cel = new PdfPCell(new Phrase(getText("garuda.label.dynamicform.bldcollectdate")+":",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		if(getFormversion()!=null && getFormversion().getDynaFormDateStr()!=null)
		{
			cel = new PdfPCell(new Phrase(getFormversion().getDynaFormDateStr(),bodyFont));
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		addEmptyLine(table,1,4);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.localCbuFull"),bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		addEmptyLine(table,1,4);
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.localMaternalFull"),bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		addEmptyLine(table,1,4);
		cel = new PdfPCell(new Phrase(getText("garuda.ctShipmentInfo.label.isbtid")+":",bodyBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		if(getCdrCbuPojo()!=null && getCdrCbuPojo().getCordIsbiDinCode()!=null)
		{
			cel = new PdfPCell(new Phrase(getCdrCbuPojo().getCordIsbiDinCode(),bodyFont));
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",bodyFont));
		}
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {20f,13f,18f,9f,25f,15f};
	       try{
			table.setWidths(columnWidths);
	       }
	       catch(Exception e){e.printStackTrace();}
	       tableWidBrd = new PdfPTable(1);
			tableWidBrd.setWidthPercentage(100);
			tableWidBrd.setHorizontalAlignment(0);
			tableWidBrd.getDefaultCell().setBorder(0);
			tableWidBrd.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			celWidBrd = new PdfPCell(table);
			celWidBrd.setHorizontalAlignment(Element.ALIGN_LEFT);
			celWidBrd.setBorder(Rectangle.BOX);
			tableWidBrd.addCell(celWidBrd);
			subCatPart.add(tableWidBrd);
		
		
			table = new PdfPTable(3);
			table.setWidthPercentage(100);
			table.setHorizontalAlignment(0);
			table.getDefaultCell().setBorder(0);
			cel = new PdfPCell(new Phrase(getText("garuda.cbufinalreview.label.flaggedItems.question"),bodyBold));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table.addCell(cel);
			cel = new PdfPCell(new Phrase(getText("garuda.cbuentry.label.status"),bodyBold));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table.addCell(cel);
			table.addCell("");
			//List a= new ArrayList();
			if(getMrqFormListPrd()!=null && getMrqFormListPrd().size()>1){
				for(int i=0;i<2;i++){
				Object a = getMrqFormListPrd().get(i);
				Object[] b=(Object[]) a;
				if((b[15])!=null && (b[1])!=null){
					Object a1 = getMrqFormListPrd().get(i+1);
					Object[] b1=(Object[]) a1;
				cel = new PdfPCell(new Phrase(b[1].toString(),bodyFont));
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table.addCell(cel);
				cel = new PdfPCell(new Phrase(getCodeListDescById(Long.parseLong(b[15].toString())),bodyFont));
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table.addCell(cel);
				cel = new PdfPCell(new Phrase("",bodyFont));
				cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cel.setBorder(0);
				table.addCell(cel);
				}}}
			columnWidths = new float[] {55f,25f,20f};
		       try{
				table.setWidths(columnWidths);
		       }
		       catch(Exception e){e.printStackTrace();}
			subCatPart.add(table);
			
			table = new PdfPTable(3);
			table.setWidthPercentage(100);
			table.setHorizontalAlignment(0);
			table.getDefaultCell().setBorder(0);
			if(getQuesGrpPrd()!=null && getQuesGrpPrd().size()>1){
			Object[] titles =(Object[]) getQuesGrpPrd().get(1);
			if(titles[1]!=null){
			cel = new PdfPCell(new Phrase(titles[1].toString(),bodyBold));
			}
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table.addCell(cel);
			cel = new PdfPCell(new Phrase(getText("garuda.unitreport.label.completedstatus"),bodyBold));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table.addCell(cel);
			cel = new PdfPCell(new Phrase(getText("garuda.unitreport.label.testdate"),bodyBold));
			cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cel.setBorder(0);
			table.addCell(cel);
			}
			for(int i=0;i<getMrqFormListPrd().size()-1;i++){
			Object a = getMrqFormListPrd().get(i);
			Object[] b=(Object[]) a;
			if((b[15])!=null && (b[20])!=null && (b[8].toString()).equals("test_outcome1")){
				Object a1 = getMrqFormListPrd().get(i+1);
				Object[] b1=(Object[]) a1;
			cel = new PdfPCell(new Phrase(b[1].toString(),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table.addCell(cel);
			cel = new PdfPCell(new Phrase(getCodeListDescById(Long.parseLong(b[15].toString())),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table.addCell(cel);
			if((b1[21])!=null){
				cel = new PdfPCell(new Phrase(b1[21].toString(),bodyFont));
			}
			else{
				cel = new PdfPCell(new Phrase("",bodyFont));
			}
			cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cel.setBorder(0);
			table.addCell(cel);
			}
			}
			columnWidths = new float[] {55f,25f,20f};
		       try{
				table.setWidths(columnWidths);
		       }
		       catch(Exception e){e.printStackTrace();}
			subCatPart.add(table);
			
			
			table = new PdfPTable(3);
			table.setWidthPercentage(100);
			table.setHorizontalAlignment(0);
			table.getDefaultCell().setBorder(0);
			//List a= new ArrayList();
			
			if(getQuesGrpPrd()!=null && getQuesGrpPrd().size()>2){
				Object[] titles =(Object[]) getQuesGrpPrd().get(2);
				if(titles[1]!=null){
				cel = new PdfPCell(new Phrase(titles[1].toString(),bodyBold));
				}
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table.addCell(cel);
				cel = new PdfPCell(new Phrase(getText("garuda.unitreport.label.completedstatus"),bodyBold));
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table.addCell(cel);
				cel = new PdfPCell(new Phrase(getText("garuda.unitreport.label.testdate"),bodyBold));
				cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cel.setBorder(0);
				table.addCell(cel);
				}
			for(int i=0;i<getMrqFormListPrd().size()-1;i++){
			Object a = getMrqFormListPrd().get(i);
			Object[] b=(Object[]) a;
			if((b[15])!=null && (b[1])!=null  && (b[20])!=null && (b[8].toString()).equals("test_outcome") && !(b[20].toString()).contains("other_idm")){
				Object a1 = getMrqFormListPrd().get(i+1);
				Object[] b1=(Object[]) a1;
			cel = new PdfPCell(new Phrase(b[1].toString(),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table.addCell(cel);
			cel = new PdfPCell(new Phrase(getCodeListDescById(Long.parseLong(b[15].toString())),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table.addCell(cel);
			if((b1[21])!=null){
				cel = new PdfPCell(new Phrase(b1[21].toString(),bodyFont));
			}
			else{
				cel = new PdfPCell(new Phrase("",bodyFont));
			}
			cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cel.setBorder(0);
			table.addCell(cel);
			}
			}
			columnWidths = new float[] {55f,25f,20f};
		       try{
				table.setWidths(columnWidths);
		       }
		       catch(Exception e){e.printStackTrace();}
			subCatPart.add(table);
			
			table = new PdfPTable(3);
			table.setWidthPercentage(100);
			table.setHorizontalAlignment(0);
			table.getDefaultCell().setBorder(0);
			//List a= new ArrayList();
			
			if(getQuesGrpPrd()!=null && getQuesGrpPrd().size()>3){
				Object[] titles =(Object[]) getQuesGrpPrd().get(3);
				if(titles[1]!=null){
				cel = new PdfPCell(new Phrase(titles[1].toString(),bodyBold));
				}
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table.addCell(cel);
				cel = new PdfPCell(new Phrase(getText("garuda.unitreport.label.completedstatus"),bodyBold));
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table.addCell(cel);
				cel = new PdfPCell(new Phrase(getText("garuda.unitreport.label.testdate"),bodyBold));
				cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cel.setBorder(0);
				table.addCell(cel);
				}
			for(int i=0;i<getMrqFormListPrd().size()-1;i++){
			Object a = getMrqFormListPrd().get(i);
			Object[] b=(Object[]) a;
			if((b[16])!=null && (b[15])!=null  && (b[20])!=null && (b[8].toString()).equals("test_outcome") && (b[20].toString()).contains("other_idm")){
				Object a1 = getMrqFormListPrd().get(i+1);
				Object[] b1=(Object[]) a1;
			cel = new PdfPCell(new Phrase(b[16].toString(),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table.addCell(cel);
			cel = new PdfPCell(new Phrase(getCodeListDescById(Long.parseLong(b[15].toString())),bodyFont));
			cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cel.setBorder(0);
			table.addCell(cel);
			if((b1[21])!=null){
				cel = new PdfPCell(new Phrase(b1[21].toString(),bodyFont));
			}
			else{
				cel = new PdfPCell(new Phrase("",bodyFont));
			}
			cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cel.setBorder(0);
			table.addCell(cel);
			}
			}
			columnWidths = new float[] {55f,25f,20f};
	       try{
			table.setWidths(columnWidths);
	       }
	       catch(Exception e){e.printStackTrace();}
		subCatPart.add(table);
		subCatPart.newPage();
		}
		else if(reportGen==3){
		
		/*-----------------------------CBU RECEIPT------------*/
		table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		Phrase p1 = new Phrase();
		Chunk superScrChnk2=new Chunk(getText("garuda.nmdp.productinsert.headingSupScrpPrt"),bodyFont);
		superScrChnk2.setTextRise(5f);
		p1.add(new Chunk(getText("garuda.nmdp.productinsert.heading"),headerFont));
		p1.add(superScrChnk2);
		p1.add(new Chunk("(NMDP)",headerFont));
		cel = new PdfPCell(p1);		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.nmdp"),headerFont));
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.reciptunit"),headerFont));
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		addEmptyLine(table, 1, 1);
		subCatPart.add(table);
		
		
		table = new PdfPTable(4);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		
		Phrase pCel = new Phrase();
		pCel.add(new Chunk(getText("garuda.nmdp.label.patntid")+":  ",bodyFont));
		//cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.patntid")+":",bodyFont));
		//cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		//cel.setBorder(0);
		//table.addCell(cel);
		//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
		
		 /*if(recipientInfo!=null && !StringUtils.isEmpty(recipientInfo.getRecipientId())){
				pCel.add(new Chunk(recipientInfo.getRecipientId(),bodyFont));
				}*/
			if(getCtOrderDetailsList()!=null && getCtOrderDetailsList().size()>0){
				Object[] obj = (Object[]) getCtOrderDetailsList().get(0);
				if(obj[13]!=null && !(obj[13].toString().equals("Not Provided"))){
					pCel.add(new Chunk(obj[13].toString(),bodyFont));
				}
				else{
					pCel.add(new Chunk("",bodyFont));
				}
			}
			else{
				pCel.add(new Chunk(" ",bodyFont));
			}
			cel = new PdfPCell(pCel);
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table.addCell(cel);
			table.addCell("");
			
			pCel = new Phrase();
			pCel.add(new Chunk(getText("garuda.cbuentry.label.idbag")+":  ",bodyFont));
		//cel = new PdfPCell(new Phrase(getText("garuda.evaluHistory.report.uniqueProdIdonBag")+":",bodyFont));
		//cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		//cel.setBorder(0);
		//table.addCell(cel);
		
		if(getCdrCbuPojo()!=null && getCdrCbuPojo().getNumberOnCbuBag()!=null)
		{
			pCel.add(new Chunk(getCdrCbuPojo().getNumberOnCbuBag(),bodyFont));
		//cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		//cel.setBorder(0);
		}
		else
		{
			pCel.add(new Chunk(" ",bodyFont));
			//cel.setBorder(0);
		}
		cel = new PdfPCell(pCel);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		pCel = new Phrase();
		pCel.add(new Chunk(getText("garuda.nmdp.label.TcId")+":  ",bodyFont));
		/*cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.TcId")+":",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);*/
		if(getCtOrderDetailsList()!=null && getCtOrderDetailsList().size()>0){
			Object[] obj = (Object[]) getCtOrderDetailsList().get(0);
			if(obj[8]!=null && !(obj[8].toString().equals("Not Provided"))){
				pCel.add(new Chunk(obj[8].toString(),bodyFont));
			}
			else{
				pCel.add(new Chunk("",bodyFont));
			}
		}else{
			pCel.add(new Chunk("",bodyFont));
		}
			cel = new PdfPCell(pCel);
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table.addCell(cel);
			table.addCell("");
			pCel = new Phrase();
			pCel.add(new Chunk(getText("garuda.nmdp.label.cburegid")+"  ",bodyFont));
		/*cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.cburegid" ),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);*/
		
		if(getCdrCbuPojo().getRegistryId()!=null){
			pCel.add(new Chunk(getCdrCbuPojo().getRegistryId(),bodyFont));
		}
		else{
			pCel.add(new Chunk(" ",bodyFont));
		}
		cel = new PdfPCell(pCel);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		columnWidths = new float[] {45f,5f,45f,5f};
	       try{
			table.setWidths(columnWidths);
	       }
	       catch(Exception e){e.printStackTrace();}
	    subCatPart.add(table);
	           
	    table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		pCel = new Phrase();
		pCel.add(new Chunk(getText("garuda.cbbdefaults.lable.cbbregistryname")+":  ",bodyFont));
		/*cel = new PdfPCell(new Phrase(getText("garuda.cbbdefaults.lable.cbbregistryname")+":",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);*/
		
		if(getSite()!=null && getSite().getSiteName()!=null)
		{
			pCel.add(new Chunk(getSite().getSiteName(),bodyFont));
		}
		else
		{
			pCel.add(new Chunk(" ",bodyFont));
		}
		cel = new PdfPCell(pCel);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		
		pCel = new Phrase();
		pCel.add(new Chunk(getText("garuda.advancelookup.label.licstus")+":  ",bodyFont));
		/*cel = new PdfPCell(new Phrase(getText("garuda.advancelookup.label.licstus")+":",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);*/
		
		if(getCdrCbuPojo()!=null && getCdrCbuPojo().getLicense()!=null)
		{
			pCel.add(new Chunk(getCdrCbuPojo().getLicense(),bodyFont));
		}
		else
		{
			pCel.add(new Chunk(" ",bodyFont));
		}
		cel = new PdfPCell(pCel);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		
		columnWidths = new float[] {80f,20f};
	       try
	       {
			table.setWidths(columnWidths);
	       }
	       catch(Exception e){e.printStackTrace();}
	       subCatPart.add(table);
		
		    
	    
	    table = new PdfPTable(5);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		table.addCell(" ");
		
		pCel = new Phrase();
		pCel.add(new Chunk(getText("garuda.nmdp.label.unlic_spon")+":  ",bodyFont));
		/*cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.unlic_spon")+":",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);*/
		if(getCtOrderDetailsList()!=null && getCtOrderDetailsList().size()>0){
			Object[] obj = (Object[]) getCtOrderDetailsList().get(0);
			if(obj[69]!=null && !(obj[69].toString().equals("Not Provided")) && getCdrCbuPojo().getLicense().equals("Unlicensed")){
				pCel.add(new Chunk(obj[69].toString(),bodyFont));
			}
			else{
				pCel.add(new Chunk("",bodyFont));
			}
		}else{
			pCel.add(new Chunk("",bodyFont));
		}
			cel = new PdfPCell(pCel);
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table.addCell(cel);
			table.addCell("");
			
			pCel = new Phrase();
			pCel.add(new Chunk(getText("garuda.nmdp.label.indNumber")+":  ",bodyFont));
		/*cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.indNumber")+":",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);*/
			if(getCtOrderDetailsList()!=null && getCtOrderDetailsList().size()>0){
				Object[] obj = (Object[]) getCtOrderDetailsList().get(0);
				if(obj[70]!=null && !(obj[70].toString().equals("Not Provided")) && getCdrCbuPojo().getLicense().equals("Unlicensed")){
					pCel.add(new Chunk(obj[70].toString(),bodyFont));
				}
				else{
					pCel.add(new Chunk("",bodyFont));
				}
			}else{
				pCel.add(new Chunk("",bodyFont));
			}
			cel = new PdfPCell(pCel);
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table.addCell(cel);
			table.addCell("");
			
		columnWidths = new float[] {5f,45f,3f,45f,2f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    subCatPart.add(table);
	    
	    table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		cel = new PdfPCell(LineImage);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		cel.setPadding(0);
		table.addCell(cel);
		table.addCell("");
		columnWidths = new float[] {90f,10f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
		subCatPart.add(table);
		
	    table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
				
		header = new Chunk(getText("garuda.nmdp.label.receiptInspection"));
		header.setUnderline(0.1f, -2f);
		//subCatPart.add(table);
		subCatPart.add(header);
		
		table.addCell("");
		subCatPart.add(table);
		
		table = new PdfPTable(10);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.dateRec")+":",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase("_____________",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.time")+":",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase("__________",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		table.addCell("");

		cel = new PdfPCell(new Phrase("",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setVerticalAlignment(Element.ALIGN_BOTTOM);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.am"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setVerticalAlignment(Element.ALIGN_BOTTOM);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.pm"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		columnWidths = new float[] {14f,16f,6f,12f,1f,5f,3f,5f,3f,35f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    subCatPart.add(table);
	    
	    table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.addCell("");
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.date"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {15f,85f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
		subCatPart.add(table);
		
		table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.dataLogger")+"____________________",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.dryShipper")+":",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		subCatPart.add(table);
		
		table = new PdfPTable(4);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		addEmptyLine(table, 1, 4);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setVerticalAlignment(Element.ALIGN_BOTTOM);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.accept"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setVerticalAlignment(Element.ALIGN_BOTTOM);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.notAccept"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		columnWidths = new float[] {3f,28f,3f,66f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
		subCatPart.add(table);
		
		table = new PdfPTable(3);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.addCell("");
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setVerticalAlignment(Element.ALIGN_BOTTOM);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.open"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setVerticalAlignment(Element.ALIGN_BOTTOM);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.temp"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		columnWidths = new float[] {41f,4f,55f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    subCatPart.add(table);
	    
	    table = new PdfPTable(3);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.addCell("");
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.probe")+"_____________________",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {50f,48f,2f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    subCatPart.add(table);
	    
	    table = new PdfPTable(3);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.addCell("");
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setVerticalAlignment(Element.ALIGN_BOTTOM);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.DataLogrIndAlrmCnd"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setVerticalAlignment(Element.ALIGN_BOTTOM);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.cbuThawed"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {41f,4f,55f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    subCatPart.add(table);
	    
	    table = new PdfPTable(3);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.addCell("");
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.probe")+"_____________________",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {50f,48f,2f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    subCatPart.add(table);
	    
	    table = new PdfPTable(3);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.addCell("");
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setVerticalAlignment(Element.ALIGN_BOTTOM);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.cbuIdNotMatch"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {41f,4f,55f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    subCatPart.add(table);
	    
	    table = new PdfPTable(4);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.addCell("");
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setVerticalAlignment(Element.ALIGN_BOTTOM);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.otherSpecify")+"______________________________",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		addEmptyLine(table, 1, 4);
		columnWidths = new float[] {41f,4f,54f,1f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    subCatPart.add(table);
			    
	    table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		header = new Chunk(getText("garuda.nmdp.receipt.reqDocumentsIncluded"),bodyBold);
		header.setUnderline(0.1f, -2f);
		subCatPart.add(header);
		//addEmptyLine(table, 1, 1);
		//subCatPart.add(table);
		
		table = new PdfPTable(7);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		table.addCell("");
		
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setVerticalAlignment(Element.ALIGN_BOTTOM);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.clinicalnote.label.yes"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setVerticalAlignment(Element.ALIGN_BOTTOM);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.clinicalnote.label.no")+"**",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setVerticalAlignment(Element.ALIGN_BOTTOM);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.naLicensedProduct"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {5f,4f,12f,4f,28f,3f,44f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    subCatPart.add(table);
	    
	    
	    
	    
	    table = new PdfPTable(6);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		table.addCell("");
		
		cel = new PdfPCell(dotImage);
		cel.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		cel.setVerticalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
				
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.productInsert"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("    ");
		cel = new PdfPCell(dotImage);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setVerticalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase("    "+getText("garuda.nmdp.receipt.licensedProduct"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		table.addCell("");
		
		cel = new PdfPCell(dotImage);
		cel.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		cel.setVerticalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.nmdpFinal"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("    ");
		cel = new PdfPCell(dotImage);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setVerticalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase("    "+getText("garuda.nmdp.receipt.nonNmdp"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		columnWidths = new float[] {5f,4f,31f,10f,6f,44f};
		try
		{
		  	table.setWidths(columnWidths);
		}
		catch(Exception e)
		{
		  	e.printStackTrace();
		}
		subCatPart.add(table);

		table = new PdfPTable(6);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		table.addCell("");
		cel = new PdfPCell(dotImage);
		cel.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		cel.setVerticalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		

		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.nmdpCordBlood"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		table.addCell("");
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.unitsNotFac"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		table.addCell("");
		
		cel = new PdfPCell(dotImage);
		cel.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		cel.setVerticalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
	
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.idmTestResult"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		table.addCell("");
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.submitProductComplaintShpprt2")+".",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		table.addCell("");
		cel = new PdfPCell(dotImage);
		cel.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		cel.setVerticalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.cbbThawInstructions"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		table.addCell("");
		cel = new PdfPCell(new Phrase(getText(""),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		
		table.addCell("");
		cel = new PdfPCell(dotImage);
		cel.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		cel.setVerticalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.CrculrInfo"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		table.addCell("");
		/*cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.submitProductComplaint"),bold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);*/
		table.addCell("");
		columnWidths = new float[] {5f,4f,31f,7f,6f,47f};
		try
		{
		  	table.setWidths(columnWidths);
		}
		catch(Exception e)
		{
		  	e.printStackTrace();
		}
		subCatPart.add(table);
		
		table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		addEmptyLine(table, 1, 2);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.CbuIns")+"_____________________________________________________________________",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {99f,1f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    subCatPart.add(table);
	    
		table = new PdfPTable(7);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		table.addCell("");
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setVerticalAlignment(Element.ALIGN_BOTTOM);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.tcCoordinator"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setVerticalAlignment(Element.ALIGN_BOTTOM);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.stemCell"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setVerticalAlignment(Element.ALIGN_BOTTOM);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.other")+"_______________________",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {13f,3f,17f,3f,33f,3f,28f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    subCatPart.add(table);
	    
		table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		addEmptyLine(table, 1, 2);
		cel = new PdfPCell(new Phrase(getText("garuda.clinicalnote.label.comments")+":",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("__________________________________________________________________________________________",bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {12f,88f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    subCatPart.add(table);
		
		table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		addEmptyLine(table, 1, 1);		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.faxCompleted"),bodyFont));
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		/*table.addCell("");
		columnWidths = new float[] {60f,40f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }*/
		subCatPart.add(table);
		
		table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		cel = new PdfPCell(LineImage);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		cel.setPadding(0);
		table.addCell(cel);
		table.addCell("");
		columnWidths = new float[] {90f,10f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
		subCatPart.add(table);
		
		table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		Phrase p2 = new Phrase();
		Chunk superScrChnk1=new Chunk(getText("garuda.nmdp.receipt.supTm"),footerSupFont);
		superScrChnk1.setTextRise(5f);
		p2.add(new Chunk("          **="+getText("garuda.nmdp.receipt.submitProductComplaintShp"),footerFont));
		p2.add(superScrChnk1);
		p2.add(new Chunk(getText("garuda.nmdp.receipt.nmdpCaseManager0")+"\n          ",footerFont));
		p2.add(new Chunk(getText("garuda.nmdp.receipt.nmdpCaseManager"),footerFont));
		cel = new PdfPCell(p2);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		/*
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);*/
		subCatPart.add(table);
		
		}
		
				
		
		/*table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.nmdp"),head));
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.reciptunit"),sub));
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		subCatPart.add(table);
		
		
		table = new PdfPTable(4);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.patntid")+":",largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
		
		if(getCtOrderDetailsList()!=null && getCtOrderDetailsList().size()>13){
			if(getCtOrderDetailsList().get(13)!=null){
				cel = new PdfPCell(new Phrase(getCtOrderDetailsList().get(13).toString(),largeFont));
				}
			else{
				cel = new PdfPCell(new Phrase(" ",largeFont));
			}
			}
			else{
				cel = new PdfPCell(new Phrase(" ",largeFont));
			}
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.evaluHistory.report.uniqueProdIdonBag"),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		if(getCtOrderDetailsList()!=null && getCtOrderDetailsList().size()>13)
		{
			if(getCtOrderDetailsList().get(13)!=null)
			{
				cel = new PdfPCell(new Phrase(getCtOrderDetailsList().get(13).toString(),largeFont));
			}
			else
			{
				cel = new PdfPCell(new Phrase(" ",largeFont));
			}
		}
		else
		{
				cel = new PdfPCell(new Phrase(" ",largeFont));
		}
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.TcId")+":",largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		if(getCtOrderDetailsList()!=null && getCtOrderDetailsList().size()>8)
		{
			if(getCtOrderDetailsList().get(8)!=null)
			{
				cel = new PdfPCell(new Phrase(getCtOrderDetailsList().get(8).toString(),largeFont));
			}
			else
			{
				cel = new PdfPCell(new Phrase(" ",largeFont));
			}
		}
		else
		{
				cel = new PdfPCell(new Phrase(" ",largeFont));
		}
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.cburegid" ),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		if(getCdrCbuPojo().getRegistryId()!=null){
		cel = new PdfPCell(new Phrase(getCdrCbuPojo().getRegistryId(),largeFont));
		}
		else{
			cel = new PdfPCell(new Phrase(" ",largeFont));
		}
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		columnWidths = new float[] {12f,38f,33f,17f};
	       try{
			table.setWidths(columnWidths);
	       }
	       catch(Exception e){e.printStackTrace();}
	    subCatPart.add(table);
	           
	    table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		cel = new PdfPCell(new Phrase(getText("garuda.cbbdefaults.lable.cbbregistryname")+":",largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		if(getSite()!=null && getSite().getSiteName()!=null)
		{
			cel = new PdfPCell(new Phrase(getSite().getSiteName(),smallBold));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",smallBold));
		}
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.advancelookup.label.licstus")+":",largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		if(getCdrCbuPojo()!=null && getCdrCbuPojo().getLicense()!=null)
		{
			cel = new PdfPCell(new Phrase(getCdrCbuPojo().getLicense(),smallBold));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
		}
		else
		{
			cel = new PdfPCell(new Phrase(" ",smallBold));
		}
		table.addCell(cel);
		
		columnWidths = new float[] {20f,80f};
	       try
	       {
			table.setWidths(columnWidths);
	       }
	       catch(Exception e){e.printStackTrace();}
	       subCatPart.add(table);
		
		    
	    
	    table = new PdfPTable(5);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		table.addCell(" ");
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.unlic_spon")+":",largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
				
		table.addCell(" ");
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.indNumber")+":",largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		table.addCell(" ");
		
		columnWidths = new float[] {5f,30f,30f,15f,20f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    subCatPart.add(table);
	    
	    table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		cel = new PdfPCell(LineImage);
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		columnWidths = new float[] {90f,10f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
		subCatPart.add(table);
		
	    table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
				
		header = new Chunk(getText("garuda.nmdp.label.receiptInspection"));
		header.setUnderline(0.1f, -2f);
		subCatPart.add(table);
		subCatPart.add(header);
		
		table.addCell("");
		subCatPart.add(table);
		
		table = new PdfPTable(10);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.dateRec")+":",largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase("-------------------",largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.productinsert.label.time")+":",largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase("---",largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		table.addCell(":");

		cel = new PdfPCell(new Phrase("---",largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.am"),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.pm"),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		columnWidths = new float[] {15f,15f,8f,3f,2f,5f,3f,5f,3f,41f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    subCatPart.add(table);
	    
	    table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.addCell("");
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.date"),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {15f,85f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
		subCatPart.add(table);
		
		table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.dryShipper")+":",largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		subCatPart.add(table);
		
		table = new PdfPTable(4);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.accept")+":",largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.notAccept")+":",largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		columnWidths = new float[] {6f,25f,6f,63};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
		subCatPart.add(table);
		
		table = new PdfPTable(3);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.addCell("");
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.open"),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.temp"),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		columnWidths = new float[] {41f,4f,55f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    subCatPart.add(table);
	    
	    table = new PdfPTable(3);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.addCell("");
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.probe"),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("-------------------",largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {50f,18f,32f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    subCatPart.add(table);
	    
	    table = new PdfPTable(3);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.addCell("");
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.cbuThawed"),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {41f,4f,55f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    subCatPart.add(table);
	    
	    table = new PdfPTable(3);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.addCell("");
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.probe"),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("-------------------",largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {50f,18f,32f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    subCatPart.add(table);
	    
	    table = new PdfPTable(3);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.addCell("");
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.cbuIdNotMatch"),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {41f,4f,55f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    subCatPart.add(table);
	    
	    table = new PdfPTable(4);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.addCell("");
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.otherSpecify"),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("-------------------------------------------",largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {41f,4f,15f,40f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    subCatPart.add(table);
			    
	    table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		header = new Chunk(getText("garuda.nmdp.receipt.reqDocumentsIncluded"));
		header.setUnderline(0.1f, -2f);
		subCatPart.add(header);
		table.addCell("");
		subCatPart.add(table);
		
		table = new PdfPTable(7);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		table.addCell("");
		
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.clinicalnote.label.yes"),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.clinicalnote.label.no")+"**",largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.naLicensedProduct"),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {5f,4f,12f,4f,31f,3f,41f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    subCatPart.add(table);
	    
	    
	    
	    
	    table = new PdfPTable(5);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		table.addCell("");
		
		cel = new PdfPCell(dotImage);
		cel.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		cel.setVerticalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
				
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.CrculrInfo"),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(dotImage);
		cel.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		cel.setVerticalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.licensedProduct"),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		table.addCell("");
		
		cel = new PdfPCell(dotImage);
		cel.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		cel.setVerticalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.productInsert"),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(dotImage);
		cel.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		cel.setVerticalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.nonNmdp"),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		table.addCell("");
		cel = new PdfPCell(dotImage);
		cel.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		cel.setVerticalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.nmdpFinal"),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.unitsNotFac"),bold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		table.addCell("");
		
		cel = new PdfPCell(dotImage);
		cel.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		cel.setVerticalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.nmdpCordBlood"),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.labelReq"),bold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		table.addCell("");
		cel = new PdfPCell(dotImage);
		cel.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		cel.setVerticalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.idmTestResult"),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		table.addCell("");
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.sponReq"),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		
		table.addCell("");
		cel = new PdfPCell(dotImage);
		cel.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		cel.setVerticalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.cbbThawInstructions"),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		table.addCell("");
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.submitProductComplaint"),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {5f,4f,46f,4f,41f};
		try
		{
		  	table.setWidths(columnWidths);
		}
		catch(Exception e)
		{
		  	e.printStackTrace();
		}
		subCatPart.add(table);
		
		table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.addCell("");
		table.addCell("");
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.CbuIns"),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("---------------------------------------------------------------------------",largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {40f,60f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    subCatPart.add(table);
	    
		table = new PdfPTable(7);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		table.addCell("");
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.tcCoordinator"),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.stemCell"),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.other"),largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {35f,3f,17f,3f,33f,3f,6f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    subCatPart.add(table);
	    
		table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		
		cel = new PdfPCell(new Phrase(getText("garuda.clinicalnote.label.comments")+":",largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("----------------------------------------------------------------------------------------------------------------------------",largeFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] {11f,87f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    subCatPart.add(table);
		
		table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		addEmptyLine(table, 2 , 1);
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.receipt.faxCompleted"),largeBold));
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		subCatPart.add(table);
		
		table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		cel = new PdfPCell(LineImage);
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		columnWidths = new float[] {90f,10f};
	    try
	    {
	    	table.setWidths(columnWidths);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
		subCatPart.add(table);
		
		table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.nmdp"),headerFont));
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.reciptunit"),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_CENTER);
		cel.setBorder(0);
		table.addCell(cel);
		subCatPart.add(table);
		
		
		table = new PdfPTable(4);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.patntid")+":",subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
		if(getCtOrderDetailsList()!=null && getCtOrderDetailsList().size()>13){
		if(getCtOrderDetailsList().get(13)!=null){
			cel = new PdfPCell(new Phrase(getCtOrderDetailsList().get(13).toString(),smallBold));
			}
		else{
			cel = new PdfPCell(new Phrase(" ",smallBold));
		}
		}
		else{
			cel = new PdfPCell(new Phrase(" ",smallBold));
		}
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		//Bold = new Phrase(getText("garuda.unitreport.label.localcbuid"),subFont);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.patnLcltid"),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		//Bold = new Phrase(getCdrCbuPojo().getLocalCbuId(),smallBold);
		if(getCtOrderDetailsList()!=null && getCtOrderDetailsList().size()>13){
			if(getCtOrderDetailsList().get(13)!=null){
				cel = new PdfPCell(new Phrase(getCtOrderDetailsList().get(13).toString(),smallBold));
				}
			else{
				cel = new PdfPCell(new Phrase(" ",smallBold));
			}
			}
			else{
				cel = new PdfPCell(new Phrase(" ",smallBold));
			}
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		//Bold = new Phrase(getText("garuda.cdrcbuview.label.collection_date" ),subFont);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.cburegid" ),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		if(getCdrCbuPojo().getRegistryId()!=null){
		cel = new PdfPCell(new Phrase(getCdrCbuPojo().getRegistryId(),smallBold));
		}
		else{
			cel = new PdfPCell(new Phrase(" ",smallBold));
		}
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.cbuidBag")+":",subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		if(getCdrCbuPojo().getNumberOnCbuBag()!=null){
		cel = new PdfPCell(new Phrase(getCdrCbuPojo().getNumberOnCbuBag(),smallBold));
		}
		else{
			cel = new PdfPCell(new Phrase(" ",smallBold));
		}
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.TcId"),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		if(getCtOrderDetailsList()!=null && getCtOrderDetailsList().size()>8){
			if(getCtOrderDetailsList().get(8)!=null){
				cel = new PdfPCell(new Phrase(getCtOrderDetailsList().get(8).toString(),smallBold));
				}
			else{
				cel = new PdfPCell(new Phrase(" ",smallBold));
			}
			}
			else{
				cel = new PdfPCell(new Phrase(" ",smallBold));
			}
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.CbbReg"),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		if(getSites()!=null && getSites().size()>0){
			for(SitePojo s : getSites()){
				if(getCdrCbuPojo()!=null && getCdrCbuPojo().getFkCbbId()!=null){
						if(s.getSiteId()==getCdrCbuPojo().getFkCbbId()){
							cel = new PdfPCell(new Phrase(s.getSiteName(),smallBold));
						}
						else{
							cel = new PdfPCell(new Phrase(" ",smallBold));
						}
				}else{
					cel = new PdfPCell(new Phrase(" ",smallBold));
				}
			}
		}
		else{
			cel = new PdfPCell(new Phrase(" ",smallBold));
		}
		
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		//set width of table cells
		header = new Chunk(getText("garuda.nmdp.label.DryShipper"));
		header.setUnderline(0.1f, -2f);
		subCatPart.add(table);
		subCatPart.add(header);
		
		table = new PdfPTable(14);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		cel = new PdfPCell(new Phrase(getText("garuda.cbuHistory.label.date")+":",subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
		cel = new PdfPCell(new Phrase("---------",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("Time")+":",subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("---------",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
	
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("a.m.",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("p.m.",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.DryShippintct"),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText(" "),smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase("Yes",smallBold));		
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);

		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase("No",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell("");
		cel = new PdfPCell(new Phrase("(mm/dd/yyyy)",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		cel.setColspan(13);
		table.addCell(cel);
		
			columnWidths = new float[] {5f, 12f, 6f, 13f, 4f, 6f, 4f, 6f, 22f , 5f, 3f, 5f,5f, 4f};
	       try{
			table.setWidths(columnWidths);
	       }
	       catch(Exception e){e.printStackTrace();}
	       subCatPart.add(table);
		/*--------------------------new Line---------------------------*/
	       	/*table = new PdfPTable(4);
			table.setWidthPercentage(100);
			table.setHorizontalAlignment(0);
			table.getDefaultCell().setBorder(0);
			table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.DryShppRcvd"),subFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table.addCell(cel);
			//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
			cel = new PdfPCell(new Phrase("----------------   ",smallBold));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table.addCell(cel);
			cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.SealId"),subFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table.addCell(cel); 
			cel = new PdfPCell(new Phrase("----------------",smallBold));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table.addCell(cel);
			subCatPart.add(table);
			/*--------------------------new Line---------------------------*/
		/*header = new Chunk(getText("garuda.nmdp.label.OpningTC"));
		header.setUnderline(0.1f, -2f);
		subCatPart.add(header);
		
		table = new PdfPTable(10);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		cel = new PdfPCell(new Phrase(getText("garuda.cbuHistory.label.date")+":",subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
		cel = new PdfPCell(new Phrase("---------",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("Time")+":",subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("---------",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
	
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("a.m.",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("p.m.",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.DryShprUnpck"),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("--------------"),smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
		/*cel = new PdfPCell(new Phrase(getText("---"),smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("--",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);

		cel = new PdfPCell(new Phrase("---",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("--",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);*/
		
		
		/*columnWidths = new float[] {5f, 12f, 6f, 13f, 4f, 6f, 4f, 6f, 22f , 22f };
	       try{
			table.setWidths(columnWidths);
	       }
	       catch(Exception e){e.printStackTrace();}
	       subCatPart.add(table);
		/*--------------------------new line----------------------------*/
	      /* 	table = new PdfPTable(4);
			table.setWidthPercentage(100);
			table.setHorizontalAlignment(0);
			table.getDefaultCell().setBorder(0);
			table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.IndcteTmp"),subFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table.addCell(cel);
			//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
			cel = new PdfPCell(new Phrase(" ",smallBold));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table.addCell(cel);
			cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.WghtDryShpr"),subFont));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table.addCell(cel);
			cel = new PdfPCell(new Phrase(getText("---------KG"),smallBold));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table.addCell(cel);
			
			columnWidths = new float[] {45f, 11f, 30f,14f};
		       try{
				table.setWidths(columnWidths);
		       }
		       catch(Exception e){e.printStackTrace();}
		       subCatPart.add(table);
			/*--------------------------new line----------------------------*/
		       
		    /*   	table = new PdfPTable(8);
				table.setWidthPercentage(100);
				table.setHorizontalAlignment(0);
				table.getDefaultCell().setBorder(0);
				table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
				
				cel = new PdfPCell(checkBoxUnChecked);
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table.addCell(cel);
				
				cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.IntrnlData"),subFont));
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table.addCell(cel);
				//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
				cel = new PdfPCell(new Phrase(" ",smallBold));
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table.addCell(cel);
				table.addCell(" ");
				cel = new PdfPCell(checkBoxUnChecked);
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table.addCell(cel);
				cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.OthrTemp"),subFont));
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table.addCell(cel);
				table.addCell(" ");
				table.addCell(" ");			

				columnWidths = new float[] {3f, 35f, 3f,17f,3f,36f,3f,2f};
			       try{
					table.setWidths(columnWidths);
			       }
			       catch(Exception e){e.printStackTrace();}
			       subCatPart.add(table);
		    /*--------------------------new line----------------------------*/	

		/*table = new PdfPTable(5);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.addCell(" ");
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.IndctReadng"),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		// Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
		cel = new PdfPCell(new Phrase("----------", smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("Specify:"), subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("---------------------------", smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		columnWidths = new float[] { 3f, 30f, 27f, 8f, 32f };
		try {
			table.setWidths(columnWidths);
		} catch (Exception e) {
			e.printStackTrace();
		}
		subCatPart.add(table);
		    /*--------------------------new line----------------------------*/
				
				

			/*    table = new PdfPTable(8);
				table.setWidthPercentage(100);
			    table.setHorizontalAlignment(0);
				table.getDefaultCell().setBorder(0);
				table.getDefaultCell().setBorder(Rectangle.NO_BORDER);				
				table.addCell(" ");
				cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.AlrmMode"),subFont));
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table.addCell(cel);
				//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
				cel = new PdfPCell(checkBoxUnChecked);
				cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cel.setBorder(0);
				table.addCell(cel);
				cel = new PdfPCell(new Phrase("Yes**",smallBold));
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table.addCell(cel);
				cel = new PdfPCell(checkBoxUnChecked);
				cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cel.setBorder(0);
				table.addCell(cel);
				cel = new PdfPCell(new Phrase("No",smallBold));
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table.addCell(cel);
				table.addCell(" ");
				table.addCell(" ");
				
				
				table.addCell(" ");
				cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.TmpWrmr"),subFont));
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table.addCell(cel);
				//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
				cel = new PdfPCell(checkBoxUnChecked);
				cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cel.setBorder(0);
				table.addCell(cel);
				cel = new PdfPCell(new Phrase("Yes**",smallBold));
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table.addCell(cel);
				cel = new PdfPCell(checkBoxUnChecked);
				cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cel.setBorder(0);
				table.addCell(cel);
				cel = new PdfPCell(new Phrase("No",smallBold));
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table.addCell(cel);
				table.addCell(" ");
				table.addCell(" ");
				columnWidths = new float[] {3f, 35f, 3f,7f,3f,36f,5f,8f};
			       try{
					table.setWidths(columnWidths);
			       }
			       catch(Exception e){e.printStackTrace();}
			       subCatPart.add(table);
		    /*--------------------------new line----------------------------*/ 
				
				
				

			    /*table = new PdfPTable(8);
				table.setWidthPercentage(100);
				table.setHorizontalAlignment(0);
				table.getDefaultCell().setBorder(0);
				table.getDefaultCell().setBorder(Rectangle.NO_BORDER);					
				cel = new PdfPCell(checkBoxUnChecked);
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table.addCell(cel);
				cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.CryoIndctr"),subFont));
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table.addCell(cel);
				//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
				cel = new PdfPCell(new Phrase(" ",smallBold));
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table.addCell(cel);
				table.addCell(" ");
				cel = new PdfPCell(checkBoxUnChecked);
				cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cel.setBorder(0);
				table.addCell(cel);
				cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.NoTmpMontrng"),subFont));
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table.addCell(cel);
				table.addCell(" ");
				table.addCell(" ");
				columnWidths = new float[] {3f, 35f, 3f,17f,3f,36f,2f,1f};
			       try{
					table.setWidths(columnWidths);
			       }
			       catch(Exception e){e.printStackTrace();}
			       subCatPart.add(table);
		    /*--------------------------new line----------------------------*/
				
				
				

		       	/*table = new PdfPTable(8);
				table.setWidthPercentage(100);
				table.setHorizontalAlignment(0);
				table.getDefaultCell().setBorder(0);
				table.getDefaultCell().setBorder(Rectangle.NO_BORDER);					
				table.addCell(" ");
				cel = new PdfPCell(new Phrase(getText("Indicate Color: "),subFont));
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table.addCell(cel);
				//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
				cel = new PdfPCell(checkBoxUnChecked);
				cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cel.setBorder(0);
				table.addCell(cel);
				cel = new PdfPCell(new Phrase("Green",smallBold));
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table.addCell(cel);
				cel = new PdfPCell(checkBoxUnChecked);
				cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cel.setBorder(0);
				table.addCell(cel);
				cel = new PdfPCell(new Phrase("Red**",smallBold));
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table.addCell(cel);
				table.addCell(" ");
				cel = new PdfPCell(new Phrase(getText(" "),subFont));
				cel.setHorizontalAlignment(Element.ALIGN_LEFT);
				cel.setBorder(0);
				table.addCell(cel);		
				
				columnWidths = new float[] {3f, 35f, 3f,7f,3f,36f,5f,8f};
			       try{
					table.setWidths(columnWidths);
			       }
			       catch(Exception e){e.printStackTrace();}
			       subCatPart.add(table);
		    /*--------------------------new line----------------------------*/
			       
		/*table = new PdfPTable(4);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);		
		//table = new PdfPTable(1);
		//table.setWidthPercentage(100);
		//table.setHorizontalAlignment(0);
		//table.getDefaultCell().setBorder(0);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.RcordProbe"),smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		cel.setColspan(4);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("Probe temperature: "),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
		cel = new PdfPCell(new Phrase("----------------C",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.PrbeThrmtr"),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("----------------",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);		
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.TmpReadBy"),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
		cel = new PdfPCell(new Phrase("----------------",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText(" "),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell(" ");
	     subCatPart.add(table);
	       
	       /*--------------------------new line----------------------------*/   
	    /*table = new PdfPTable(6);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);		
		header = new Chunk(getText("garuda.nmdp.label.InspctionCBU"));
		header.setUnderline(0.1f, -2f);
		header.setHorizontalScaling(Element.ALIGN_CENTER);
		subCatPart.add(header);		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.AprnceCBUbag"),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("Yes",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("No",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell(" ");

		
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.CBUIdmtch"),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("Yes",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("No",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell(" ");
		
		columnWidths = new float[] {40f, 4f,5f,3f,5f,43f};
	       try{
			table.setWidths(columnWidths);
	       }
	       catch(Exception e){e.printStackTrace();}
	       subCatPart.add(table);
		
		
	       /*--------------------------new line----------------------------*/   	
	       
	    /*table = new PdfPTable(12);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);			
	    header = new Chunk(getText("garuda.nmdp.label.RvwLablng"));
		header.setUnderline(0.1f, -2f);
		header.setHorizontalScaling(Element.ALIGN_CENTER);
		subCatPart.add(header);			
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.CrculrInfo"),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("Yes",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("No**",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(" ",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(" ",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.IDMRslt"),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("Yes",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("No**",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.PrdctInsrt"),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("Yes",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("No**",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(" ",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(" ",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.ThwInstrn"),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("Yes",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("No**",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.FnlElgblty"),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("Yes",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("No**",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(" ",smallBold));
		table.addCell(" ");
		table.addCell(" ");
		table.addCell(" ");
		table.addCell(" ");
		table.addCell(" ");
		table.addCell(" ");
		table.addCell(" ");
		
		columnWidths = new float[] {20f,3f,5f,3f,6f,3f,24f,19f,3f,5f,3f,6f};
	       try{
			table.setWidths(columnWidths);
	       }
	       catch(Exception e){e.printStackTrace();}
	       subCatPart.add(table);
	       
/*--------------------------new line----------------------------*/  
		
	    /*table = new PdfPTable(7);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(0);
		table.getDefaultCell().setBorder(0);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.NMDPtag"),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("Yes",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("No**",smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		//Bold = new Phrase(getCdrCbuPojo().getRegistryId(),smallBold);
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.NotAppProdct"),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);		
		columnWidths = new float[] {20f,3f,5f,3f,6f,3f,60f};
	       try{
			table.setWidths(columnWidths);
	       }
	       catch(Exception e){e.printStackTrace();}
	       subCatPart.add(table);
	       
  /*--------------------------new line----------------------------*/   	
	       
	    /*table = new PdfPTable(3);
	    table.setWidthPercentage(100);
	    table.setHorizontalAlignment(0);
	    table.getDefaultCell().setBorder(0);
	    table.getDefaultCell().setBorder(Rectangle.NO_BORDER);		
		cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.CbuIns"),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell(":");
		cel = new PdfPCell(new Phrase(getText("---------------------------------------------------------"),smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		columnWidths = new float[] {32f,1f,67f};
	       try{
			table.setWidths(columnWidths);
	       }
	       catch(Exception e){e.printStackTrace();}
	       subCatPart.add(table);
		
	       /*--------------------------new line----------------------------*/ 
		
	    /* table = new PdfPTable(7);
		 table.setWidthPercentage(100);
		 table.setHorizontalAlignment(0);
		 table.getDefaultCell().setBorder(0);
		 table.getDefaultCell().setBorder(Rectangle.NO_BORDER); 		
		table.addCell(" ");
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase("TC Coordinator",subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("Stem Cell Lab or Blood Bank Staff"),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(checkBoxUnChecked);
		cel.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cel.setBorder(0);
		table.addCell(cel);
		cel = new PdfPCell(new Phrase(getText("Other"),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		
		columnWidths = new float[] {34f,3f,16f,3f,32f,3f,9f};
	       try{
			table.setWidths(columnWidths);
	       }
	       catch(Exception e){e.printStackTrace();}
	       subCatPart.add(table);
		
	       /*--------------------------new line----------------------------*/ 
		
	      /* table = new PdfPTable(3);
		   table.setWidthPercentage(100);
		   table.setHorizontalAlignment(0);
		   table.getDefaultCell().setBorder(0);
		   table.getDefaultCell().setBorder(Rectangle.NO_BORDER);		
		cel = new PdfPCell(new Phrase(getText("Comments"),subFont));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		table.addCell(":");
		cel = new PdfPCell(new Phrase(getText("----------------------------------------------------------------------------"),smallBold));
		cel.setHorizontalAlignment(Element.ALIGN_LEFT);
		cel.setBorder(0);
		table.addCell(cel);
		
		columnWidths = new float[] {10f,1f,89f};
	       try{
			table.setWidths(columnWidths);
	       }
	       catch(Exception e){e.printStackTrace();}
	       subCatPart.add(table);
	       /*--------------------------new line----------------------------*/  
	       
	      /* table = new PdfPTable(1);
			table.setWidthPercentage(100);
			table.setHorizontalAlignment(0);
			table.getDefaultCell().setBorder(0);
			table.getDefaultCell().setBorder(Rectangle.NO_BORDER); 	       
			cel = new PdfPCell(new Phrase(getText("--------------------------------------------------------------------------------------"),smallBold));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);
			table.addCell(cel);
			columnWidths = new float[] {100f};
		       try{
				table.setWidths(columnWidths);
		       }
		       catch(Exception e){e.printStackTrace();}
		       subCatPart.add(table);
		       /*--------------------------new line----------------------------*/  
	       
	        /*table = new PdfPTable(1);
			table.setWidthPercentage(100);
			table.setHorizontalAlignment(0);
			table.getDefaultCell().setBorder(0);
			table.getDefaultCell().setBorder(Rectangle.NO_BORDER);			
			cel = new PdfPCell(new Phrase(getText("garuda.nmdp.label.Suggested"),smallBold));
			cel.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel.setBorder(0);			
			table.addCell(cel); 	       
			columnWidths = new float[] {100f};
		       try{
				table.setWidths(columnWidths);
		       }
		       catch(Exception e){e.printStackTrace();}
		       subCatPart.add(table); 
	       
	    */   
		
		

		

		


		
		/*-----------------------------IDM REPORT------------*/
		
		
		
		
	}	
}