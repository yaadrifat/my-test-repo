package com.velos.ordercomponent.action;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import com.velos.ordercomponent.business.pojoobjects.CordFilterPojo;
import com.velos.ordercomponent.business.pojoobjects.HLAFilterPojo;
import com.velos.ordercomponent.util.VelosGarudaConstants;

public class IDSummaryCSV implements VelosGarudaConstants {
	
	private Boolean isMultipleCBBSelected = false;
	
	public File idSummaryCSV(List<Object> cdrObjects, String path) {
		
		
		 int bufferSize = 10240;
		 FileWriter fr = null;
		 BufferedWriter br = null;
		
		 File file = null;
		 Map<Long, HLAFilterPojo> cbuHla = null;
		 StringBuilder content = new StringBuilder();
		
		 
		try {
			
			file = new File(path);
			fr = new FileWriter(file);		
            br = new BufferedWriter(fr, bufferSize);
        
		
		 
		 
		 
		content = createHeader(content);
		br.write(content.toString());
		content.setLength(0);
		
		for(Object obj : cdrObjects){		 
			
			if(obj!=null){
				
       	        Object[] objArr = (Object[])obj;			   		
			  
			    if(isMultipleCBBSelected){
				  content.append(WRAPPER).append(objArr[1]!=null?objArr[1].toString():"").append(WRAPPER).append(SEPRATOR); 
			    }
				  content.append(WRAPPER).append(objArr[2]!=null?objArr[2]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[3]!=null?objArr[3]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[4]!=null?objArr[4]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[5]!=null?objArr[5]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[6]!=null?objArr[6]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[7]!=null?objArr[7]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[8]!=null?objArr[8]:"").append(WRAPPER).append(SEPRATOR);
				 
				  content.append(NEW_LINE); 
				  
				  br.write(content.toString());
				  content.setLength(0);
			}
			
		 }
		 } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
           try {
               br.close();
               fr.close();
           } catch (IOException e) {
               e.printStackTrace();
           }
       }
		return file;
	}
	
private StringBuilder createHeader(StringBuilder content){
		
	if(isMultipleCBBSelected){
		   content.append(WRAPPER).append("CBB").append(WRAPPER).append(SEPRATOR);
	}
		   content.append(WRAPPER).append("CBU Registry ID").append(WRAPPER).append(SEPRATOR);              
		   content.append(WRAPPER).append("CBU Local ID").append(WRAPPER).append(SEPRATOR);                   
		   content.append(WRAPPER).append("Unique Product Identity on Bag").append(WRAPPER).append(SEPRATOR); 
		   content.append(WRAPPER).append("ISBT").append(WRAPPER).append(SEPRATOR);                           
		   content.append(WRAPPER).append("Historic Local CBU ID").append(WRAPPER).append(SEPRATOR);		  
		   content.append(WRAPPER).append("Maternal Registry ID").append(WRAPPER).append(SEPRATOR);           
		   content.append(WRAPPER).append("Maternal Local ID").append(WRAPPER).append(SEPRATOR);
		   content.append(NEW_LINE);
			
		   return content;
	}

public void setIsMultipleCBBSelected(Boolean isMultipleCBBSelected) {
	this.isMultipleCBBSelected = isMultipleCBBSelected;
}

}
