package com.velos.ordercomponent.action;

import java.io.ByteArrayOutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;
import org.hibernate.Hibernate;

import com.velos.eres.business.common.ReportDaoNew;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.user.UserJB;
import com.velos.ordercomponent.business.domain.EmailAttachment;
import com.velos.ordercomponent.business.pojoobjects.AddressPojo;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.EmailAttachmentPojo;
import com.velos.ordercomponent.business.pojoobjects.EntityStatusReasonPojo;
import com.velos.ordercomponent.business.pojoobjects.MailConfigPojo;
import com.velos.ordercomponent.business.pojoobjects.NotificationPojo;
import com.velos.ordercomponent.business.pojoobjects.SearchPojo;
import com.velos.ordercomponent.business.pojoobjects.UserPojo;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.controller.CBUController;
import com.velos.ordercomponent.controller.MailController;
import com.velos.ordercomponent.controller.UserAlertController;
import com.velos.ordercomponent.controller.VelosSearchController;
import com.velos.ordercomponent.util.EresForms;
import com.velos.ordercomponent.util.VelosSystemServices;
import com.velos.ordercomponent.util.VelosGarudaConstants;
import com.velos.ordercomponent.util.VelosMailConstants;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class MailAction extends VelosBaseAction {

	public static final Log log = LogFactory.getLog(MailAction.class);
	
	private VelosSearchController searchController;
	private MailController mailController;
	private SearchPojo searchPojo;
	private MailConfigPojo mailConfigPojo;
	private NotificationPojo notificationPojo;
	private String cbuID;
	private Boolean sentMail;
	private String mailCode;
	private CdrCbuPojo cdrCbuPojo;
	private String formname;
	private String notifyTo;
	private UserPojo userPojo;
	private AddressPojo addressPojo;
	private UserAlertController userAlertController;
	private Object[] filterKeys;
    private Object[] filterValues;
    private String repId;
	

	public Object[] getFilterKeys() {
		return filterKeys;
	}
	public void setFilterKeys(Object[] filterKeys) {
		this.filterKeys = filterKeys;
	}
	public Object[] getFilterValues() {
		return filterValues;
	}
	public void setFilterValues(Object[] filterValues) {
		this.filterValues = filterValues;
	}
	public String getRepId() {
		return repId;
	}
	public void setRepId(String repId) {
		this.repId = repId;
	}
	public CdrCbuPojo getCdrCbuPojo() {
		return cdrCbuPojo;
	}
	public void setCdrCbuPojo(CdrCbuPojo cdrCbuPojo) {
		this.cdrCbuPojo = cdrCbuPojo;
	}
	public String getMailCode() {
		return mailCode;
	}
	public void setMailCode(String mailCode) {
		this.mailCode = mailCode;
	}
	public Boolean getSentMail() {
		return sentMail;
	}
	public void setSentMail(Boolean sentMail) {
		this.sentMail = sentMail;
	}
	public SearchPojo getSearchPojo() {
		return searchPojo;
	}
	public void setSearchPojo(SearchPojo searchPojo) {
		this.searchPojo = searchPojo;
	}	
	public NotificationPojo getNotificationPojo() {
		return notificationPojo;
	}
	public void setNotificationPojo(NotificationPojo notificationPojo) {
		this.notificationPojo = notificationPojo;
	}
	public MailConfigPojo getMailConfigPojo() {
		return mailConfigPojo;
	}
	public void setMailConfigPojo(MailConfigPojo mailConfigPojo) {
		this.mailConfigPojo = mailConfigPojo;
	}  
	public String getCbuID() {
		return cbuID;
	}
	public void setCbuID(String cbuID) {
		this.cbuID = cbuID;
	}
	public String getFormname() {
		return formname;
	}
	public void setFormname(String formname) {
		this.formname = formname;
	}
	public String getNotifyTo() {
		return notifyTo;
	}
	public void setNotifyTo(String notifyTo) {
		this.notifyTo = notifyTo;
	}
	public MailAction()
	{
		searchController = new VelosSearchController();
		mailController = new MailController();
		userPojo=new UserPojo();
		addressPojo=new AddressPojo();
		userAlertController=new UserAlertController();
	}
	
	
	
	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public static void main(String str[]){
		
		String s1="request on your CBU  [CBUID] for NMDP";
		String s2=	VelosMailConstants.CBUID;
		String s3 ="KK";
		//log.debug(s1.replace(s2,s3));
		
		}

	public String getMailHome() throws Exception
	{
		//log.debug("get Mail Home");
		commonMailContent();
		return "success";
	}
	
	public String sentMailFromCbuAssessment() throws Exception{
		try{
		List<EmailAttachmentPojo> list = new ArrayList<EmailAttachmentPojo>();
		EmailAttachmentPojo pojo = null;
		/*commonMailContent();
		getNotificationPojo().setNotifySubject("CBU Assessment Form");
		getNotificationPojo().setNotifyTo("mahmed@del.aithent.com");	
		Integer[] formIds = new Integer[5];
		formIds[0] = new EresForms().getFormId(VelosGarudaConstants.FORM_PHYS_FIND_ASSESS);
		formIds[1] = new EresForms().getFormId(VelosGarudaConstants.FORM_INF_DIS_MARK);
		formIds[2] = new EresForms().getFormId(VelosGarudaConstants.FORM_MRQ);
		formIds[3] = new EresForms().getFormId(VelosGarudaConstants.FORM_CBU_ASSESSMENT);
		for(Integer id : formIds){
			if(id != null){
				pojo = new EmailAttachmentPojo();
				pojo.setEmailDocName("Form");
				pojo.setEmailDocType("application/pdf");
				pojo.setEntityId(getCdrCbuPojo().getCordID());
				pojo.setEntityType(FORM_TYPE);
				pojo.setEmailDoc(id.longValue());
				list.add(pojo);
			}
		}
		setAttachmentQuery(list);*/
		if(getRepId()!=null && getFilterKeys()!=null && getFilterKeys().length>0 && getFilterValues()!=null && getFilterValues().length>0){
			ByteArrayOutputStream out = getXsltoPdfStream();
			ByteArrayOutputStream out1 = new ByteArrayOutputStream();			
			out1 = new DynaFormAction().generateTempFileForPdfGen(FORM_MRQ, getCdrCbuPojo().getCordID());			
			if(out!=null){
			  byte[] carr = out.toByteArray();
			  Blob blob = null;
			  if(out1!=null){
					  byte[] ar = out1.toByteArray();
					  byte[] darr = new byte[carr.length+ar.length];					  
					  darr = ArrayUtils.addAll(ar, carr);
					  blob = Hibernate.createBlob(darr);
			  }else{
				  blob = Hibernate.createBlob(carr);
			  }
			  
			  pojo = new EmailAttachmentPojo();
			  pojo.setEmailDocName("CBU Assessment");
			  pojo.setEmailDocType("application/pdf");
			  pojo.setEntityId(getCdrCbuPojo().getCordID());
			  pojo.setEmailDocFile(blob);
			  list.add(pojo);
			}
			if(list!=null && list.size()>0){
				setAttachmentQueryForPDF(list);
			}			
		}
		setNotificationPojo(mailController.sendEmail(getNotificationPojo()));
		setSentMail(true);
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String sentMail() throws Exception
	{
		if(getMailCode()!=null && (getMailCode().equals(VelosMailConstants.TRANSPLANT_CENTER_CODE)|| getMailCode()==VelosMailConstants.TRANSPLANT_CENTER_CODE)){
			if(getCdrCbuPojo()!=null && getCdrCbuPojo().getCordID()!=null 
					&& getCdrCbuPojo().getFkCordCbuEligibleReason()!=null && getCdrCbuPojo().getFkCordCbuEligibleReason()!= getCodeListPkByTypeAndSubtype(ELIGIBILITY_STATUS, ELIGIBLE)){
				List<EntityStatusReasonPojo> reasonPojos =  new	CBUController().getReasonsByEntityStatusId(getCdrCbuPojo().getFkCordCbuEligibleReason());
				List<EmailAttachmentPojo> list = new ArrayList<EmailAttachmentPojo>();
				if(reasonPojos!=null){
					for(EntityStatusReasonPojo r : reasonPojos){
						if(r.getFkFormId()!=null){
							EmailAttachmentPojo pojo = new EmailAttachmentPojo();
							pojo.setEmailDoc(r.getFkFormId());
							pojo.setEmailDocName("Form");
							pojo.setEmailDocType("application/pdf");
							pojo.setEntityId(getCdrCbuPojo().getCordID());
							pojo.setEntityType(FORM_TYPE);
							list.add(pojo);
						}
						if(r.getFkAttachmentId()!=null){
							EmailAttachmentPojo pojo = new EmailAttachmentPojo();
							Long id = getDCMSAttachId(r.getFkAttachmentId());
							pojo.setEmailDoc(id);
							pojo.setEmailDocName("File");
							pojo.setEmailDocType("application/pdf");
							pojo.setEntityId(getCdrCbuPojo().getCordID());
							pojo.setEntityType(FILE_TYPE);
							list.add(pojo);
						}
					}
				}
				setAttachmentQuery(list);
			}
		}
		setNotificationPojo(mailController.sendEmail(getNotificationPojo()));
		getRequest().setAttribute("mailSent", true);
		setSentMail(true);
		return "success";
	}
	
	public void cbbAddressChangeMail(String sendTo, String siteName, String siteId)
	{
		NotificationPojo notification = null;
		try{		
		setNotifyTo(sendTo);
		String cbbAddChg = VelosMailConstants.CBB_ADDRESS_CHANGE_CODE;
		setMailConfigPojo((MailConfigPojo)mailController.getMailConfigurationByCode(cbbAddChg));
		if(siteId!=null && getMailConfigPojo()!=null)
		{
			String subject = getMailConfigPojo().getMailConfigSubject();
			String content = getMailConfigPojo().getMailConfigContent();
	
				if(subject.indexOf(VelosMailConstants.SITEID)!=-1)
				{
					if(siteId!=null && siteId!=""){
					  subject = subject.replace(VelosMailConstants.SITEID, siteId);
					}								  
					getMailConfigPojo().setMailConfigSubject(subject);				
				}
				if(content.indexOf(VelosMailConstants.SITEID)!=-1){
					content = content.replace(VelosMailConstants.SITEID, siteId);
					content = content.replace(VelosMailConstants.CBBNAME, siteName);					
					getMailConfigPojo().setMailConfigContent(content);				
				}
		
		}	
		if(getMailConfigPojo()!=null)
		{
			notification = new NotificationPojo();
			notification.setNotifySubject(getMailConfigPojo().getMailConfigSubject());
			notification.setNotifyContent(getMailConfigPojo().getMailConfigContent());
			notification.setNotifyFrom(getMailConfigPojo().getMailConfigFrom());
			notification.setNotifyBCC(getMailConfigPojo().getMailConfigBCC());
			notification.setNotifyCC(getMailConfigPojo().getMailConfigCC());
			if(getNotifyTo()!=null && !getNotifyTo().equals("")){
				notification.setNotifyTo(getNotifyTo());
			}
			setNotificationPojo(notification);		
			setNotificationPojo(mailController.sendEmail(getNotificationPojo()));
			setSentMail(true);			
		}		
		}
		catch(Exception e)
		{
			log.error(e.getMessage());
			e.printStackTrace();
		}
		
	}
	
	public void setAttachmentQueryForPDF(List<EmailAttachmentPojo> list) throws Exception{
		List<EmailAttachmentPojo> listPojo = new ArrayList<EmailAttachmentPojo>();
		Long[] ids;		
		Integer count =0;
		if(list!=null && list.size()>0){
			listPojo = mailController.saveEmailDocuments(list);		
			//log.debug("list pojo size"+listPojo.size()+listPojo.get(0).getEmailDocumentId());
		}		
		if(listPojo!=null && listPojo.size()>0){
			ids = new Long[listPojo.size()];
			for(EmailAttachmentPojo e : listPojo){
				if(e.getEmailDocumentId()!=null){
					ids[count] = e.getEmailDocumentId();
					count++;
				}
			}
			if(ids.length>0){
				String sql = "select DOCUMENT_FILENAME,DOCUMENT_FILES,DOCUMENT_CONTENTTYPE,ENTITY_ID,ENTITY_TYPE,ATTACHMENT_TYPE from CB_MAIL_DOCUMENTS " +
						"where PK_MAIL_DOCUMENTS in  (";
				Integer len = ids.length;
				Integer idCount = 0;
				for(Long id : ids){
					sql += id ;
					idCount++;
					if(idCount==len){
						sql += ")";
						break;
					}else{
						sql += ",";
					}
				}
				//log.debug("sql attachment query"+sql);
				getNotificationPojo().setAttachementQuery(sql);
			}
		}
	}
	
	public void setAttachmentQuery(List<EmailAttachmentPojo> list) throws Exception{
		List<EmailAttachmentPojo> listPojo = new ArrayList<EmailAttachmentPojo>();
		Long[] ids;		
		Integer count =0;
		if(list!=null && list.size()>0){
			listPojo = mailController.saveEmailDocuments(list);		
			//log.debug("list pojo size"+listPojo.size()+listPojo.get(0).getEmailDocumentId());
		}		
		if(listPojo!=null && listPojo.size()>0){
			ids = new Long[listPojo.size()];
			for(EmailAttachmentPojo e : listPojo){
				if(e.getEmailDocumentId()!=null){
					ids[count] = e.getEmailDocumentId();
					count++;
				}
			}
			if(ids.length>0){
				String sql = "select DOCUMENT_FILE,DOCUMENT_CONTENTTYPE,DOCUMENT_FILENAME,ENTITY_ID,ENTITY_TYPE,ATTACHMENT_TYPE from CB_MAIL_DOCUMENTS " +
						"where PK_MAIL_DOCUMENTS in  (";
				Integer len = ids.length;
				Integer idCount = 0;
				for(Long id : ids){
					sql += id ;
					idCount++;
					if(idCount==len){
						sql += ")";
						break;
					}else{
						sql += ",";
					}
				}
				//log.debug("sql attachment query"+sql);
				getNotificationPojo().setAttachementQuery(sql);
			}
		}
	}
	
	public void commonMailContent() throws Exception{
		//log.debug("Inside mail content!!!!!!!!!!!!!!!");
		NotificationPojo notification = null;
		String uId =null;
		setMailConfigPojo((MailConfigPojo)mailController.getMailConfigurationByCode(getMailCode()));
		//System.out.print(getMailConfigPojo().getMailConfigCC()+getMailConfigPojo().getMailConfigCode()+getMailConfigPojo().getMailConfigContent());
		System.out.print("ID"+getCbuID());
		//log.debug("MAil "+getNotifyTo());
		String user = "Velos";
		String cm = "Aithent";
		try
		{
			if(request.getParameter("userId")!=null && request.getParameter("userId")!="")
			{
		Long userId=Long.parseLong(request.getParameter("userId").toString());
		//log.debug("User Id from request : "+userId);
		userPojo.setUserId(userId);
		//log.debug("From User Pojo : "+userPojo.getUserId());
		if(userPojo.getUserId()!=null){
			userPojo = userAlertController.getUserById(userPojo);
			if(userPojo.getAddress()!=null && userPojo.getAddress().getAddressId()!=null){
				searchPojo = new SearchPojo();	
				searchPojo.setCodelstParam(null);
				searchPojo.setSearchType(ADDRESS_OBJECTNAME);
				searchPojo.setCriteria(" addressId = '"+userPojo.getAddress().getAddressId()+"'");
				searchPojo = searchController.velosSearch(searchPojo);
				if(searchPojo.getResultlst()!=null && searchPojo.getResultlst().size()>0){
					addressPojo = (AddressPojo)ObjectTransfer.transferObjects(searchPojo.getResultlst().get(0), addressPojo);
				}
			}
			if(addressPojo!=null){
				//log.debug("Email Id is!!!!!!!!!!!!!!!"+addressPojo.getEmail());
				setNotifyTo(addressPojo.getEmail());
			}
		}
	}
		}
		catch(Exception e)
		{
			log.error(e.getMessage());
			e.printStackTrace();
		}
		
		if(getCbuID()!=null && getMailConfigPojo()!=null)
		{
			String subject = getMailConfigPojo().getMailConfigSubject();
			String content = getMailConfigPojo().getMailConfigContent();
			String cbuid = getCbuID();	
			String formname=getFormname();
			
			
			String url = VelosGarudaConstants.VELOS_GARUDA_URL+VelosMailConstants.UNIT_REPORT_URL_SENT;			
			try{
			if(subject.indexOf(VelosMailConstants.CBUID)!=-1)
			{
				if(cbuid!=null && cbuid!=""){
				  subject = subject.replace(VelosMailConstants.CBUID, cbuid);
				}
				if(formname!=null && formname!=""){
					subject = subject.replace(VelosMailConstants.FORMNAME, formname);	
				}				  
				getMailConfigPojo().setMailConfigSubject(subject);				
			}
			if(content.indexOf(VelosMailConstants.CBUID)!=-1){
				content = content.replace(VelosMailConstants.UNIT_REPORT_URL, url);
				content = content.replace(VelosMailConstants.CBUID, cbuid);
				content = content.replace(VelosMailConstants.VELOS_USER, user);
				content = content.replace(VelosMailConstants.CM, cm);
				if(formname!=null && formname!=""){
				  content = content.replace(VelosMailConstants.FORMNAME, formname);
				}
				System.out.print("content"+content);				
				getMailConfigPojo().setMailConfigContent(content);				
			}
			}
			catch(Exception e)
			{
				log.error(e.getMessage());
				e.printStackTrace();
				}
						
		}
		if(getMailConfigPojo()!=null)
		{
			notification = new NotificationPojo();
			notification.setNotifySubject(getMailConfigPojo().getMailConfigSubject());
			notification.setNotifyContent(getMailConfigPojo().getMailConfigContent());
			notification.setNotifyFrom(getMailConfigPojo().getMailConfigFrom());
			notification.setNotifyBCC(getMailConfigPojo().getMailConfigBCC());
			notification.setNotifyCC(getMailConfigPojo().getMailConfigCC());
			if(getNotifyTo()!=null && !getNotifyTo().equals("")){
				notification.setNotifyTo(getNotifyTo());
			}
			setNotificationPojo(notification);
			
			//System.out.print("get notification pojo"+getNotificationPojo());
		}
	}
	public String mailforcbuAssessment(){
		try{
			//UserPojo userPojo1=null;
			Long userId=Long.parseLong(request.getParameter("userid").toString());
			//log.debug("User Id from request : "+userId);
			userPojo.setUserId(userId);
			//log.debug("From User Pojo : "+userPojo.getUserId());
			if(userPojo.getUserId()!=null){
				userPojo = userAlertController.getUserById(userPojo);
				if(userPojo.getAddress()!=null && userPojo.getAddress().getAddressId()!=null){
					searchPojo = new SearchPojo();	
					searchPojo.setCodelstParam(null);
					searchPojo.setSearchType(ADDRESS_OBJECTNAME);
					searchPojo.setCriteria(" addressId = '"+userPojo.getAddress().getAddressId()+"'");
					searchPojo = searchController.velosSearch(searchPojo);
					if(searchPojo.getResultlst()!=null && searchPojo.getResultlst().size()>0){
						addressPojo = (AddressPojo)ObjectTransfer.transferObjects(searchPojo.getResultlst().get(0), addressPojo);
					}
				}
				if(addressPojo!=null){
					setNotifyTo(addressPojo.getEmail());
				}
			}
			commonMailContent();
	}catch (Exception e) {
		// TODO: handle exception
		log.error(e.getMessage());
		e.printStackTrace();
		//log.debug("Exception In getting user Mail id : "+e);
	}
		return "success";
	}
	
	public ByteArrayOutputStream getXsltoPdfStream() throws Exception{
		ByteArrayOutputStream out = null;
		try {
			
			int repId = StringUtil.stringToNum(getRepId());

			ReportDaoNew rDao = new ReportDaoNew();
			if(getFilterKeys()!=null && getFilterKeys().length>0 && getFilterValues()!=null && getFilterValues().length>0){
				rDao.setFilterKey(getFilterKeys());
				rDao.setFilterValue(getFilterValues());
			}
			rDao.createReport(repId, 0);

			ArrayList repXml = rDao.getRepXml();
			String xml = null;
			if (repXml != null && repXml.size() > 0) {
				xml = (String)repXml.get(0);
				if (xml == null) { xml = ""; }
				xml = StringUtil.replace(xml,"&amp;#","&#");
			} else {
				//log.debug("Error in getting XML");
			}
			rDao.getFoXsl(repId);
			ArrayList xslList = rDao.getXsls();
			if (xslList == null || xslList.size() == 0) { return null; }

			String foText = transformXML(xml, ((String)xslList.get(0)));
            
			try {
				FopFactory fopFactory = FopFactory.newInstance();
				TransformerFactory tFactory = TransformerFactory.newInstance();
				out = new ByteArrayOutputStream();
				Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, out); 
				Transformer transformer = tFactory.newTransformer();
    			
				Source src = new StreamSource(new StringReader(foText));
				Result res = new SAXResult(fop.getDefaultHandler());
				transformer.transform(src, res);				
			}catch (Exception ex) {
				//log.debug("Exception pdf::::"+ex);
				log.error(ex.getMessage());
				ex.printStackTrace();
			}
		} catch(Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			throw e;
		}
		return out;
		
	}
	
	private String transformXML(String xml, String xsl)
	throws Exception {
		Reader mR = new StringReader(xml);
		Reader sR = new StringReader(xsl);
		Source xmlSource = new StreamSource(mR);
		Source xslSource = new StreamSource(sR);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		TransformerFactory tFactory = TransformerFactory.newInstance();
		Transformer transformer = tFactory.newTransformer(xslSource);
		
		String repDate="";
		UserJB user = (UserJB)getRequest().getSession().getAttribute(VelosGarudaConstants.GARUDA_USER);
		String uName = user.getUserFirstName() +" "+ user.getUserLastName();	 
		repDate = DateUtil.getCurrentDateTime();
		String repName = request.getParameter("repName");
		if(repName!=null)
		transformer.setParameter("repName",repName);
		transformer.setParameter("repBy",uName);
		transformer.setParameter("repDate",repDate);
		transformer.setOutputProperty("encoding", "UTF-8");
		transformer.transform(xmlSource, new StreamResult(out));
		return out.toString("UTF-8");
	}
}
