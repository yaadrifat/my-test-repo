package com.velos.ordercomponent.controller;

import com.velos.ordercomponent.business.pojoobjects.AddressPojo;
import com.velos.ordercomponent.service.VelosService;
import com.velos.ordercomponent.service.impl.VelosServiceImpl;

public class AddressController {
	VelosService service;
	
	public AddressPojo saveAddress(AddressPojo addressPojo) throws Exception {
		service = VelosServiceImpl.getService();
		return service.saveAddress(addressPojo);
	}
	
	public AddressPojo getAddress(AddressPojo addressPojo) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getAddress(addressPojo);
	}
}
