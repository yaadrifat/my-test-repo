package com.velos.ordercomponent.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.service.VelosVDAService;
import com.velos.ordercomponent.service.impl.VelosVDAServiceImpl;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class VDAController {
    
	public VelosVDAService service;
	
	public List<Object> getQueryBuilderViewData(String sql,PaginateSearch paginateSearch) throws Exception{
		service=VelosVDAServiceImpl.getVDAService();
		return service.getQueryBuilderViewData(sql, paginateSearch);
	}
	
	public List getQueryBuilderViewData(String sql) throws Exception{
		service=VelosVDAServiceImpl.getVDAService();
		return service.getQueryBuilderViewData(sql);
	}
	
	public Map getQueryBuilderRecordDetails(String sql) throws Exception{
		service=VelosVDAServiceImpl.getVDAService();
		return service.getQueryBuilderRecordDetails(sql);
	}
	
	public Map getQueryBuilderHlaData(String sql) throws Exception{
		service=VelosVDAServiceImpl.getVDAService();
		return service.getQueryBuilderHlaData(sql);
	}
	
	public Map getQueryBuilderBestHlaData(String sql) throws Exception{
		service=VelosVDAServiceImpl.getVDAService();
		return service.getQueryBuilderBestHlaData(sql);
	}
	
	public Map getQueryBuilderBestHlaFilterData(String sql,  Map<Long, String> cdlst) throws Exception{
		service=VelosVDAServiceImpl.getVDAService();
		return service.getQueryBuilderBestHlaFilterData(sql, cdlst);
	}
	
	public List getQueryBuilderCordHlaData(String sql) throws Exception{
		service=VelosVDAServiceImpl.getVDAService();
		return service.getQueryBuilderCordHlaData(sql);
	}
	
	public Map getQueryBuilderHlaFilterData(String sql, Map<Long, String> cdlst) throws Exception{
		service=VelosVDAServiceImpl.getVDAService();
		return service.getQueryBuilderHlaFilterData(sql, cdlst);
	}
	
	public List getQueryBuilderListObject(String sql) throws Exception{
		service=VelosVDAServiceImpl.getVDAService();
		return service.getQueryBuilderListObject(sql);
	}
	
	public List getQueryBuilderCordIdObject(String sql) throws Exception{
		service=VelosVDAServiceImpl.getVDAService();
		return service.getQueryBuilderCordIdObject(sql);
	}
	
	public Map getQueryBuilderMapObject(String sql) throws Exception{
		service=VelosVDAServiceImpl.getVDAService();
		return service.getQueryBuilderMapObject(sql);
	}
	
	public Map getLicEligMapObject(String sql, Map<Long, Object []> cord, Map<Long, ArrayList<Long>> reason) throws Exception{
		service=VelosVDAServiceImpl.getVDAService();
		return service.getLicEligMapObject(sql,  cord,  reason);
	}
	
	public Map getQueryBuilderMapLstObject(String sql) throws Exception{
		service=VelosVDAServiceImpl.getVDAService();
		return service.getQueryBuilderMapLstObject(sql);
	}
}
