package com.velos.ordercomponent.controller;

import java.util.List;

import com.velos.ordercomponent.business.pojoobjects.DataModificationRequestPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.service.VelosService;
import com.velos.ordercomponent.service.impl.VelosServiceImpl;

public class DataModificationRequestController {
	VelosService service;
	public void createDataRequest(DataModificationRequestPojo datarequestpojo)throws Exception{
		service = VelosServiceImpl.getService();
		service.createDataRequest(datarequestpojo);
	}
	public List viewDataRequest(Long pkDatarequest,PaginateSearch paginateSearch,int i)throws Exception{
		service = VelosServiceImpl.getService();
		return service.viewDataRequest(pkDatarequest,paginateSearch,i);
	}
	public DataModificationRequestPojo getRequestDetails(DataModificationRequestPojo datarequestpojo)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getRequestDetails(datarequestpojo);
	}
	public void updateDMRequestStatus(DataModificationRequestPojo datarequestpojo)throws Exception{
		service = VelosServiceImpl.getService();
		service.updateDMRequestStatus(datarequestpojo);
	}

}
