package com.velos.ordercomponent.action;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.eres.web.user.UserJB;
import com.velos.ordercomponent.business.domain.CodeList;
import com.velos.ordercomponent.business.domain.User;
import com.velos.ordercomponent.business.pojoobjects.UserPojo;
import com.velos.ordercomponent.controller.CodelstController;
import com.velos.ordercomponent.util.VelosGarudaConstants;

/**
 * @author Mohiuddin Ali Ahmed
 *
 */

public class LoginAction extends VelosBaseAction {

	public static final Log log = LogFactory.getLog(LoginAction.class);
	User user = null;	
	private String username;
	private String password;
	private String errorMessage;
	private CodelstController codelstController;
	private String message;
	private UserPojo userPojo;
	

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getPassword() {
		return password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return SUCCESS;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String getHome()
	{
		//log.debug("getHome");
		return "home";
	}
	
	public String login()
	{
		//log.debug("login");
		return "login";
	}
	
	public LoginAction(){
		//log.debug("Login Action ");
		//session=request.getSession(true);
		//session.setAttribute("sessionInitial", "true");
		userPojo = new UserPojo();
		codelstController = new CodelstController();
		user = new User();
		
	}
	

	public String validateUser()
	{
		//log.debug("validate user");
		
		if(getUsername().equalsIgnoreCase("pfuser")&& getPassword().equalsIgnoreCase("velos123"))
		{
			user.setFirstName("Product Fullfillment");
			user.setLastName("User");
			user.setLoginName("Product Fullfillment User");
			user.setLastLogindt(new Date().toString());
			user.setRoleRights("pfrights");
			user.setSignature("pfsign");
			
		}else if(getUsername().equalsIgnoreCase("admin")&& getPassword().equalsIgnoreCase("velos123"))
		{
			user.setFirstName("Admin");
			user.setLastName("User");
			user.setLoginName("Admin User");
			user.setLastLogindt(new Date().toString());
			user.setRoleRights("adminrights");
			user.setSignature("adminsign");
		}
		else if(getUsername().equalsIgnoreCase("cdruser")&& getPassword().equalsIgnoreCase("velos123"))
		{
			user.setFirstName("CDR");
			user.setLastName("User");
			user.setLoginName("CDR User");
			user.setLastLogindt(new Date().toString());
			user.setRoleRights("cdrrights");
			user.setSignature("cdrsign");
		}else if(getUsername().equalsIgnoreCase("reviewuser")&& getPassword().equalsIgnoreCase("velos123"))
		{
			user.setFirstName("Reviewer");
			user.setLastName("User");
			user.setLoginName("Reviewer User");
			user.setLastLogindt(new Date().toString());
			user.setRoleRights("reviewrights");
			user.setSignature("reviewsign");
		}else if(getUsername().equalsIgnoreCase("dataentryuser")&& getPassword().equalsIgnoreCase("velos123"))
		{
			user.setFirstName("Data Entry Specialist");
			user.setLastName("User");
			user.setLoginName("Data Entry Specialist User");
			user.setLastLogindt(new Date().toString());
			user.setRoleRights("dataentryrights");
			user.setSignature("datasign");
		}
		else if(getUsername().equalsIgnoreCase("")&& getPassword().equalsIgnoreCase("")){
			setErrorMessage("Please Provide UserName and Password");
			return "failure";
		}else{
			setErrorMessage("Please Provide Valid UserDetails !");
			return "failure";
		}/*else if(getUserName().equalsIgnoreCase("cbbuser")&& getPassword().equalsIgnoreCase("velos123"))
		{
		user.setFirstName("CBB");
		user.setLastName("User");
		user.setLoginName("CBB User");
		user.setLastLogindt(new Date().toString());
		user.setRoleRights("cbbrights");
	}*/
		if(user!=null)
		{
	    getSession(getRequest()).setAttribute(VelosGarudaConstants.GARUDA_USER, user);
		}
		
		return SUCCESS;
	}
	
	public String submitEsign(){		
		//userPojo = getUserPojo();
		 //log.debug("Checking Function");
		 String esign = getUserPojo().getSignature();
		 //log.debug("Value of esignature is " +esign);	
		UserJB user = (UserJB)getSession(getRequest()).getAttribute(VelosGarudaConstants.GARUDA_USER);		
		
		if(user.getUserESign().equals(esign)){
			setMessage("true");			
		}else{
			setMessage("false");
		}
	
		
			return SUCCESS;
	}
	
	

	public UserPojo getUserPojo() {
		return userPojo;
	}

	public void setUserPojo(UserPojo userPojo) {
		this.userPojo = userPojo;
	}

	public String loginPageDetails()throws Exception {
		//log.debug("LoginAction : loginPageDetails Method Start ");
		try{
			List<CodeList> codelstList = codelstController.getCodelst(VelosGarudaConstants.APPLICATIONVERSION_PARENTCODE);
			if(codelstList != null && !codelstList.isEmpty()){
				CodeList codelst = (CodeList)codelstList.get(0);
				message = codelst.getDescription();
			}
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}
		//log.debug("LoginAction : loginPageDetails Method End ");
		return SUCCESS;
	}		
	
	public String addWidgets(){
		return SUCCESS;
	}

}
