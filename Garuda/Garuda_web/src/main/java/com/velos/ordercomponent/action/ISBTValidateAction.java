package com.velos.ordercomponent.action;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ISBTValidateAction extends VelosBaseAction {
	public static final Log log = LogFactory.getLog(ISBTValidateAction.class);
	private Boolean dbValue;
	private String codeval;
	private String refnum;
	private Integer valmatch;
	public Boolean getDbValue() {
		return dbValue;
	}

	public void setDbValue(Boolean dbValue) {
		this.dbValue = dbValue;
	}
	
    public String getCodeval() {
		return codeval;
	}

	public void setCodeval(String codeval) {
		this.codeval = codeval;
	}

	public String getRefnum() {
		return refnum;
	}

	public void setRefnum(String refnum) {
		this.refnum = refnum;
	}

	public Integer getValmatch() {
		return valmatch;
	}

	public void setValmatch(Integer valmatch) {
		this.valmatch = valmatch;
	}

	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void validateIsbtCodeMeth( String codeval,String val,String position,String length){
		//log.debug("codelength "+codeval.length()+" xmllength "+Integer.valueOf(length));
		/*if(refnum.equals("025")){
			if(i>=6 && i<codeval.length()){
				//log.debug(posindex);
				posindex=7;
				//log.debug(posindex);}
			if(codeval.length()==i){//log.debug("i"+i);
			posindex=codeval.length();}
			
			if(val.contains("-")){
				String[] preval=val.split("-");
				//log.debug("codevalascii"+(int)(codeval.charAt(Integer.valueOf(position)-1)));
				for(int valbet=Integer.valueOf(preval[0]); valbet>=Integer.valueOf(preval[0]) && valbet<=Integer.valueOf(preval[1]); valbet++){
					//log.debug("multivalue"+valbet);
					if((int)(codeval.charAt(i))== valbet)
					{
						valmatch=1;
						//log.debug("valmatch"+valmatch);
					}
				}
			}
			else{
				int valbet=Integer.valueOf(val);
				//log.debug("singlevalue"+valbet);
				//log.debug("codevalascii"+(int)(codeval.charAt(Integer.valueOf(position)-1)));
				if((int)(codeval.charAt(Integer.valueOf(position)-1))== valbet)
				{
					valmatch=1;
					//log.debug("valmatch"+valmatch);
				}
			}
		}
		else{*/
		int minLength;
		int MaxLength;
		if(length.contains("-")){
			minLength=Integer.valueOf(length.split("-")[0]);
			MaxLength=Integer.valueOf(length.split("-")[1]);
		}
		else{
			minLength=Integer.valueOf(length);
			MaxLength=Integer.valueOf(length);
			}
		if(codeval.length()>=minLength && codeval.length()<=MaxLength)
		{
			//log.debug("codelength"+codeval.length()+"xmllength"+Integer.valueOf(length));
		if(val.contains("-")){
			String[] preval=val.split("-");
			//log.debug("codevalascii"+(int)(codeval.charAt(Integer.valueOf(position)-1)));
			for(int valbet=Integer.valueOf(preval[0]); valbet>=Integer.valueOf(preval[0]) && valbet<=Integer.valueOf(preval[1]); valbet++){
				//log.debug("multivalue"+valbet);
				if((int)(codeval.charAt(Integer.valueOf(position)-1))== valbet)
				{
					setValmatch(1);
					//log.debug("valmatch"+getValmatch());
				}
			}
		}
		else{
			int valbet=Integer.valueOf(val);
			//log.debug("singlevalue"+valbet);
			//log.debug("codevalascii"+(int)(codeval.charAt(Integer.valueOf(position)-1)));
			if((int)(codeval.charAt(Integer.valueOf(position)-1))== valbet)
			{
				setValmatch(1);
				//log.debug("valmatch"+getValmatch());
			}
		}
		}
		/*}*/
		
	}
	
	public String validateIsbtCode() {
		if(true){
			setDbValue(codeval.length()>=1 && codeval.length()<=16 && codeval.charAt(0)!=' ' && codeval.charAt(codeval.length()-1)!=' '? true : false);
			return SUCCESS;
		}
		else{
			/************************************use later*********************************/
			int posindex;
			int i = 0;		
			String codeval1 = getRequest().getParameter("codeval");
			String refnum1 = getRequest().getParameter("refnum");
			//log.debug("codeval="+codeval1);
			//log.debug("refnum="+refnum1);		
		  try {
		  File file = new File(getContext().getRealPath("WEB-INF/classes/isbtMap.xml"));
		  DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		  DocumentBuilder db = dbf.newDocumentBuilder();
		  Document doc = db.parse(file);
		  doc.getDocumentElement().normalize();
		  //log.debug("Root element " + doc.getDocumentElement().getNodeName());
		  NodeList nodeLst = doc.getElementsByTagName("refnode");
		  //log.debug("Information of all codes");
		
		  for (int s = 0; s < nodeLst.getLength(); s++) {
			  //log.debug("----++++"+nodeLst.getLength());
		    Node fstNode = nodeLst.item(s);
		    
		    if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
		  
		           Element fstElmnt = (Element) fstNode;
		      NodeList refLst=fstElmnt.getElementsByTagName("refnum");
		      Element refElmnt=(Element) refLst.item(0);
		      NodeList refNumLst=refElmnt.getChildNodes();
		      
		      //log.debug("Refrence Number : "  + ((Node) refNumLst.item(0)).getNodeValue());
		      if(refnum.equals(((Node) refNumLst.item(0)).getNodeValue()))
		      {
		    	  NodeList fldNmLst=fstElmnt.getElementsByTagName("field-name");
		    	  Element fldNmElmnt=(Element) fldNmLst.item(0);
		    	  NodeList fldDnmLst=fldNmElmnt.getChildNodes();
		    	  //log.debug("Field Name : "  + ((Node) fldDnmLst.item(0)).getNodeValue());
		    	  NodeList lngthLst=fstElmnt.getElementsByTagName("length");
		    	  Element lngthElmnt=(Element) lngthLst.item(0);
		    	  NodeList lngthnumLst=lngthElmnt.getChildNodes();
		    	  //log.debug("Length : "  + ((Node) lngthnumLst.item(0)).getNodeValue());
		    	  NodeList fldLst=fstElmnt.getElementsByTagName("position");
		    	  //log.debug("----++++"+fldLst.getLength());
		    	  int smallerFld =(fldLst.getLength()<=codeval.length() ? fldLst.getLength() : codeval.length());
		    	  
		    	  for(posindex=0;posindex<smallerFld;posindex++)
		      		{
		    	  		
		    	  		setValmatch(0);
		    	  		Node posNode=fldLst.item(posindex);
		    	  		Element posElmnt = (Element) posNode;
		    	  		NodeList posNoLst = posElmnt.getElementsByTagName("number");
		    	  		Element posNo=(Element) posNoLst.item(0);
		    	  		NodeList posLst=posNo.getChildNodes();
		    	  		//log.debug("position : "  + ((Node) posLst.item(0)).getNodeValue());
		    	  		NodeList valueElmntLst = posElmnt.getElementsByTagName("value");
		    	  	for(int innermst=0;innermst < valueElmntLst.getLength() ; innermst++)
		    	  	{
		    	  		Element valElmnt = (Element) valueElmntLst.item(innermst);
		    	  		NodeList val = valElmnt.getChildNodes();
		    	  		validateIsbtCodeMeth(  codeval, ((Node) val.item(0)).getNodeValue(), ((Node) posLst.item(0)).getNodeValue(), ((Node) lngthnumLst.item(0)).getNodeValue());
		    	  		//log.debug("value : " + ((Node) val.item(0)).getNodeValue());
		    	  		if(getValmatch()==1)
		    	  			break;
		    	  	}
		    	  		if(getValmatch()==0)
		    	  		{ 	//log.debug("code invalid");
		    	  		setDbValue(false);	
		    	  		return SUCCESS;
		    	  		}	    	  		
		    	  	}
		    	  	//log.debug("code valid");
		    	  	setDbValue(true);
		    	  	return SUCCESS;
		    	 }
		    }
		    
		  }
		  
		  } catch (Exception e) {
			  log.error(e.getMessage());
		    e.printStackTrace();	    
		  }
		 return SUCCESS;
		}
	  
	 }

	public Boolean checkIsbt(String refNum,String codeVal){
		Boolean flag = true;
		int posindex;
		int i = 0;		
		String codeval1 = codeVal;
		String refnum1 = refNum;
		setRefnum(refNum);
		//log.debug("codeval="+codeval1);
		//log.debug("refnum="+refnum1);		
	  try {
	  File file = new File(getContext().getRealPath("WEB-INF/classes/isbtMap.xml"));
	  DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	  DocumentBuilder db = dbf.newDocumentBuilder();
	  Document doc = db.parse(file);
	  doc.getDocumentElement().normalize();
	  //log.debug("Root element " + doc.getDocumentElement().getNodeName());
	  NodeList nodeLst = doc.getElementsByTagName("refnode");
	  //log.debug("Information of all codes");
	
	  for (int s = 0; s < nodeLst.getLength(); s++) {
		  //log.debug("----++++"+nodeLst.getLength());
	    Node fstNode = nodeLst.item(s);
	    
	    if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
	  
	           Element fstElmnt = (Element) fstNode;
	      NodeList refLst=fstElmnt.getElementsByTagName("refnum");
	      Element refElmnt=(Element) refLst.item(0);
	      NodeList refNumLst=refElmnt.getChildNodes();
	      
	      //log.debug("Refrence Number : "  + ((Node) refNumLst.item(0)).getNodeValue());
	      if(refnum.equals(((Node) refNumLst.item(0)).getNodeValue()))
	      {
	    	  NodeList fldNmLst=fstElmnt.getElementsByTagName("field-name");
	    	  Element fldNmElmnt=(Element) fldNmLst.item(0);
	    	  NodeList fldDnmLst=fldNmElmnt.getChildNodes();
	    	  //log.debug("Field Name : "  + ((Node) fldDnmLst.item(0)).getNodeValue());
	    	  NodeList lngthLst=fstElmnt.getElementsByTagName("length");
	    	  Element lngthElmnt=(Element) lngthLst.item(0);
	    	  NodeList lngthnumLst=lngthElmnt.getChildNodes();
	    	  //log.debug("Length : "  + ((Node) lngthnumLst.item(0)).getNodeValue());
	    	  NodeList fldLst=fstElmnt.getElementsByTagName("position");
	    	  //log.debug("----++++"+fldLst.getLength());
	    	  for(posindex=0;posindex<fldLst.getLength();posindex++)
	      		{
	    	  		
	    	  		setValmatch(0);
	    	  		Node posNode=fldLst.item(posindex);
	    	  		Element posElmnt = (Element) posNode;
	    	  		NodeList posNoLst = posElmnt.getElementsByTagName("number");
	    	  		Element posNo=(Element) posNoLst.item(0);
	    	  		NodeList posLst=posNo.getChildNodes();
	    	  		//log.debug("position : "  + ((Node) posLst.item(0)).getNodeValue());
	    	  		NodeList valueElmntLst = posElmnt.getElementsByTagName("value");
	    	  	for(int innermst=0;innermst < valueElmntLst.getLength() ; innermst++)
	    	  	{
	    	  		Element valElmnt = (Element) valueElmntLst.item(innermst);
	    	  		NodeList val = valElmnt.getChildNodes();
	    	  		validateIsbtCodeMeth(  codeval, ((Node) val.item(0)).getNodeValue(), ((Node) posLst.item(0)).getNodeValue(), ((Node) lngthnumLst.item(0)).getNodeValue());
	    	  		//log.debug("value : " + ((Node) val.item(0)).getNodeValue());
	    	  		if(getValmatch()==1)
	    	  			break;
	    	  	}
	    	  		if(getValmatch()==0)
	    	  		{ 	
	    	  			//log.debug("code invalid");
	    	  		    flag = false;
	    	  		    return flag;
	    	  		}	    	  		
	    	  	}
	    	  	//log.debug("code valid");
	    	  	flag = true;
	    	  	return flag;
	    	 }
	    }
	    
	  }
	  
	  } catch (Exception e) {
		  log.error(e.getMessage());
	    e.printStackTrace();	    
	  }
	  return flag;
	}

}
