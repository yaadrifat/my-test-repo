package com.velos.ordercomponent.action;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Blob;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Hibernate;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.nexoko.SignedUrl.Utils.URLSigning;
import com.nexoko.signing.constatns.RequestSigningConstants;
import com.velos.eres.web.user.UserJB;
import com.velos.ordercomponent.business.pojoobjects.AttachmentPojo;
import com.velos.ordercomponent.business.pojoobjects.DcmsLogPojo;
import com.velos.ordercomponent.business.util.Utilities;
import com.velos.ordercomponent.business.util.VelosUtil;
import com.velos.ordercomponent.controller.AttachmentController;
import com.velos.ordercomponent.util.VelosGarudaConstants;
import com.velos.ordercomponent.util.VelosSystemServices;

/**
 * @author Mohiuddin Ali Ahmed
 * 
 */
public class AttachmentAction extends VelosBaseAction {
	public static final Log log = LogFactory.getLog(AttachmentAction.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<File> fileUpload;
	private List<String> fileUploadContentType;
	private List<String> fileUploadFileName;
	private InputStream fileInputStream;
	private AttachmentPojo attachmentPojo;
	private AttachmentController attachmentController;
	private String attCodeType;
	private String attCodeSubType;
	private String codeEntityType;
	private String codeEntitySubType;
	private Long cordID;
	private String exportData;
	private String attachmentRemark;
	// private Long dcmsAttachmentId;
	private Long dcmsattachmentId;
	private String dcmsFlag;
	private String pkDcmsFlag;
	private String dcmsTask;
	DcmsLogPojo dcmsLogPojo;
	private String filepath;
	private List<List> urlList=null;
	String url;
	String urlDesc;
	String linkId;

	public AttachmentAction() {
		attachmentController = new AttachmentController();
		dcmsLogPojo = new DcmsLogPojo();
	}

	public AttachmentPojo getAttachmentPojo() {
		return attachmentPojo;
	}

	public void setAttachmentPojo(AttachmentPojo attachmentPojo) {
		this.attachmentPojo = attachmentPojo;
	}

	public List<File> getFileUpload() {
		return fileUpload;
	}

	public void setFileUpload(List<File> fileUpload) {
		this.fileUpload = fileUpload;
	}

	public List<String> getFileUploadContentType() {
		return fileUploadContentType;
	}

	public void setFileUploadContentType(List<String> fileUploadContentType) {
		this.fileUploadContentType = fileUploadContentType;
	}

	public List<String> getFileUploadFileName() {
		return fileUploadFileName;
	}

	public void setFileUploadFileName(List<String> fileUploadFileName) {
		this.fileUploadFileName = fileUploadFileName;
	}

	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}

	public String getAttCodeType() {
		return attCodeType;
	}

	public void setAttCodeType(String attCodeType) {
		this.attCodeType = attCodeType;
	}

	public String getAttCodeSubType() {
		return attCodeSubType;
	}

	public void setAttCodeSubType(String attCodeSubType) {
		this.attCodeSubType = attCodeSubType;
	}

	public String getCodeEntityType() {
		return codeEntityType;
	}

	public void setCodeEntityType(String codeEntityType) {
		this.codeEntityType = codeEntityType;
	}

	public String getCodeEntitySubType() {
		return codeEntitySubType;
	}

	public void setCodeEntitySubType(String codeEntitySubType) {
		this.codeEntitySubType = codeEntitySubType;
	}

	public Long getCordID() {
		return cordID;
	}

	public void setCordID(Long cordID) {
		this.cordID = cordID;
	}

	public String getExportData() {
		return exportData;
	}

	public void setExportData(String exportData) {
		this.exportData = exportData;
	}

	public String getAttachmentRemark() {
		return attachmentRemark;
	}

	public void setAttachmentRemark(String attachmentRemark) {
		this.attachmentRemark = attachmentRemark;
	}

	/*
	 * public Long getDcmsAttachmentId() { return dcmsAttachmentId; }
	 * 
	 * public void setDcmsAttachmentId(Long dcmsAttachmentId) {
	 * this.dcmsAttachmentId = dcmsAttachmentId; }
	 */

	public Long getDcmsattachmentId() {
		return dcmsattachmentId;
	}

	public void setDcmsattachmentId(Long dcmsattachmentId) {
		this.dcmsattachmentId = dcmsattachmentId;
	}

	public String getDcmsFlag() {
		return dcmsFlag;
	}

	public void setDcmsFlag(String dcmsFlag) {
		this.dcmsFlag = dcmsFlag;
	}

	public String getPkDcmsFlag() {
		return pkDcmsFlag;
	}

	public void setPkDcmsFlag(String pkDcmsFlag) {
		this.pkDcmsFlag = pkDcmsFlag;
	}

	public String getDcmsTask() {
		return dcmsTask;
	}

	public void setDcmsTask(String dcmsTask) {
		this.dcmsTask = dcmsTask;
	}

	public DcmsLogPojo getDcmsLogPojo() {
		return dcmsLogPojo;
	}

	public void setDcmsLogPojo(DcmsLogPojo dcmsLogPojo) {
		this.dcmsLogPojo = dcmsLogPojo;
	}
	
	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrlDesc() {
		return urlDesc;
	}

	public void setUrlDesc(String urlDesc) {
		this.urlDesc = urlDesc;
	}

	public String getLinkId() {
		return linkId;
	}

	public void setLinkId(String linkId) {
		this.linkId = linkId;
	}

	public List<List> getUrlList() {
		if(urlList==null){
			urlList=new ArrayList<List>();
		}
		return urlList;
	}

	public void setUrlList(List<List> urlList) {
		this.urlList = urlList;
	}


	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		//log.debug("Values from file upload " + getAttCodeType() + "--"+ getAttCodeSubType() + "--" + getCodeEntityType());
		return SUCCESS;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	public String saveAttachment() throws Exception {
		Long attachmentTypeID = new VelosSystemServices().getCodeListId(
				getAttCodeType(), getAttCodeSubType());
		Long entityId = getCordID();
		Long entityTypeId = new VelosSystemServices().getCodeListId(
				getCodeEntityType(), getCodeEntitySubType());

		AttachmentPojo attachmentPojo = new AttachmentPojo();
		attachmentPojo.setGarudaAttachmentType(attachmentTypeID);
		attachmentPojo.setGarudaEntityId(entityId);
		attachmentPojo.setGarudaEntityType(entityTypeId);
		attachmentPojo.setDocFile(getFileUpload().get(0));
		attachmentPojo.setFileName(getFileUploadFileName().get(0));
		attachmentPojo.setDocumentType(getFileUploadContentType().get(0));
		attachmentPojo.setGarudaAttachmentsTypeRem(getAttachmentRemark());
		attachmentPojo.setDcmsAttachmentId(getDcmsattachmentId());
		attachmentPojo = (AttachmentPojo) new VelosUtil()
				.setAuditableInfo(attachmentPojo);

		attachmentPojo = attachmentController.addAttachments(attachmentPojo);
		getRequest().setAttribute("fileUpload", true);
		setAttachmentPojo(attachmentPojo);
		return SUCCESS;
	}

	public String attachFile() throws Exception {
		// log.debug(getAttCodeSubType() + "---" + getAttCodeType()+ "---" + getCodeEntitySubType() + "---" + getCodeEntityType()+ "---" + getCordID());
		Long attachmentTypeID = new VelosSystemServices().getCodeListId(
				getAttCodeType(), getAttCodeSubType());
		// log.debug("attachment type id is" + attachmentTypeID);
		List<AttachmentPojo> attachments = new ArrayList<AttachmentPojo>();
		Long entityTypeId = new VelosSystemServices().getCodeListId(
				getCodeEntityType(), getCodeEntitySubType());
		Date date = new Date();
		Timestamp timestamp = new Timestamp(date.getTime());
		setTimestamp(timestamp);
		// log.debug("time stamp is :::" + getTimestamp());
		int count = 0;
		AttachmentPojo attachmentPojo = null;
		for (File f : getFileUpload()) {
			attachmentPojo = new AttachmentPojo();
			attachmentPojo.setGarudaAttachmentType(attachmentTypeID);
			// attachmentPojo.setGarudaEntityId(getCordID());
			attachmentPojo.setGarudaEntityType(entityTypeId);
			if (getDcmsFlag().equalsIgnoreCase(getPkDcmsFlag())) {
				attachmentPojo.setDcmsAttachmentId(getDcmsattachmentId());
			} else {
				InputStream fis = new FileInputStream(f);
				Blob blob = Hibernate.createBlob(fis);
				attachmentPojo.setDocumentFile(blob);
				attachmentPojo.setDcmsAttachmentId(null);
			}
			// attachmentPojo.setDocFile(getFileUpload());
			attachmentPojo.setFileName(getFileUploadFileName().get(count));
			attachmentPojo.setDocumentType(getFileUploadContentType()
					.get(count));
			attachmentPojo.setGarudaAttachmentsTypeRem(getAttachmentRemark());
			attachmentPojo = (AttachmentPojo) new VelosUtil()
					.setAuditableInfo(attachmentPojo);
			attachmentPojo.setTimestamp(timestamp);
			attachments.add(attachmentPojo);
			count++;
		}
		getSession(getRequest()).setAttribute("attachmentList", attachments);
		setAttachmentPojo(attachmentPojo);
		getRequest().setAttribute("fileUpload", true);
		getRequest().setAttribute("path", getFileUploadFileName());
		return SUCCESS;
	}

	public String exportData() throws Exception {
		try {
			byte[] res;
			response.setContentType("application/vnd.ms-excel;charset=utf8");
			res = getExportData().getBytes();
			response.setHeader("Content-Type", "application/charset=utf8");
			response.setHeader("Content-Disposition",
					"attachment;filename=export.xls");
			ByteArrayInputStream bis = new ByteArrayInputStream(res);
			PrintWriter pw = response.getWriter();
			int c = -1;
			while ((c = bis.read()) != -1) {
				pw.print((char) c);
			}
			bis.close();
			pw.flush();
			pw = null;
			// fileUploadContentType = "application/vnd.ms-excel";
			// fileUploadFileName = "export.xls";
			// fileInputStream = bis;
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return SUCCESS;
	}

	public String gettDcmsAttachmentId() throws Exception {
		dcmsattachmentId = attachmentController.getDcmsAttachmentId();
		setDcmsattachmentId(dcmsattachmentId);
		// setDcmsAttachmentId(dcmsattachmentId());
		setDcmsFlag(DCMSFLAG);
		setDcmsFlag(getDcmsFlag());
		setPkDcmsFlag(attachmentController.getDcmsFlagMethod(PKDCMSFLAG));
		setPkDcmsFlag(getPkDcmsFlag());
		//log.debug("pkdcmsflag isssssss::::" + getPkDcmsFlag());
		return "success";
	}

	public String dcmsLogEntry() {

		try {
			//log.debug("action issssssss-------" + getDcmsTask());
			//log.debug("getDcmsAttachmentId---" + getDcmsattachmentId());
			dcmsLogPojo.setTask(getDcmsTask());
			dcmsLogPojo.setTaskDate(new Date());
			if (request.getParameter("fetchId") == null
					|| request.getParameter("fetchId").toString().equals("")) {
				dcmsLogPojo.setAttachmentId(getDcmsattachmentId());
			} else if (getDcmsattachmentId() == null
					|| getDcmsattachmentId().toString().equals("")) {
				dcmsLogPojo.setAttachmentId(Long.parseLong(request
						.getParameter("fetchId")));
			}
			//log.debug("dcmsLogPojo.getAction-------"+ dcmsLogPojo.getTask());
			setDcmsLogPojo(dcmsLogPojo);
			setDcmsLogPojo(getDcmsLogPojo());
			dcmsLogPojo = attachmentController.addDcmsLog(getDcmsLogPojo());
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return "success";
	}

	public void getSignedUrl() {

		response.setContentType("text/html;charset=UTF-8");
		//log.debug("in getsigned url method");
		String signedURL = null;
		String docId = null;
		boolean isRequiredCrypto = false;
		PrintWriter out = null;
		docId = request.getParameter("docId");
		//log.debug("document id is::" + docId);
		isRequiredCrypto = Boolean.parseBoolean(request
				.getParameter("CryptoReq"));
		try {
			out = response.getWriter();
			signedURL = getSignedURL(docId, isRequiredCrypto);

			//log.debug("signed url iss::::" + signedURL);
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			JsonArray jsonArray = new JsonArray();
			JsonObject jsonObject = new JsonObject();
			jsonObject.addProperty("signedUrl", signedURL);
			jsonObject.addProperty("docId", docId);
			jsonArray.add(jsonObject);
			out.print(jsonArray);
			out.close();
		}
	}

	private String getSignedURL(String docId, boolean isEncyptReq) {
		UserJB user = (UserJB) getRequest().getSession().getAttribute(
				VelosGarudaConstants.GARUDA_USER);
		//log.debug("document id is::" + docId);
		String signKey = getDatafromXml("signedKey");
		String apiUser = getDatafromXml("apiUser");
		String cryptoKey = getDatafromXml("cryptoKey");
		String initVectorVal = getDatafromXml("initVectorVal");
		String activeUser = "activeUser";
		StringBuilder signedUrlParameters = new StringBuilder();
		Map<String, String> urlParams = new TreeMap<String, String>();
		Map<String, String> signedUrlParams = new TreeMap<String, String>();
		//log.debug("doc id sign key  -->" + signKey);
		//log.debug("doc id apiUser  -->" + apiUser);
		try {
			urlParams.put(RequestSigningConstants.DOC_ID, docId);
			urlParams.put(RequestSigningConstants.API_USER, apiUser);
			urlParams.put(RequestSigningConstants.Enc_REQ_TYPE,
					Boolean.toString(isEncyptReq));
			// ACTIVE_USER_VALUE should be replaced with logged in user.
			urlParams.put(activeUser, user.getUserDetails().getUserId()
					.toString());
			if (isEncyptReq) {
				urlParams.put(RequestSigningConstants.CRYPT_KEY, cryptoKey);
				urlParams.put(RequestSigningConstants.INITIAL_VECTOR,
						initVectorVal);
			}
			try {
				signedUrlParams.putAll(URLSigning.getSignedURLParameters(
						urlParams, signKey));
			} catch (Exception e) {
				log.error(e.getMessage());
				e.printStackTrace();
			}
			//log.debug("after getting signed url parameters");

			for (Map.Entry<String, String> entry : signedUrlParams.entrySet()) {
				signedUrlParameters.append(entry.getKey());
				signedUrlParameters.append("=");
				signedUrlParameters.append(entry.getValue());
				signedUrlParameters.append("&");
			}
			// removing last & from urlParameters
			signedUrlParameters.deleteCharAt(signedUrlParameters
					.lastIndexOf("&"));
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		//log.debug("before returning signed url");
		return signedUrlParameters.toString();

	}

	public void getAttachmentFromDcms() {
		response.setContentType("text/html;charset=UTF-8");
		boolean isEncryptRequest = false;
		String ipAddress = getDatafromXml("dcmsIpAddress");
		String getDoc=request.getParameter("getDoc");
		String filetype=request.getParameter("ftype");
		String initialUrl = ipAddress
				+ "/FileData-war/DcmsRetrieveServlet?";
		String docIdStr = request.getParameter("docId");
		String encStr = request.getParameter("encrypted");

		if (encStr != null) {
			isEncryptRequest = encStr.equals("true") ? true : false;
		}
		try {
			String urlParameters = getSignedURL(docIdStr, isEncryptRequest);
			//log.debug("urlParameters:::::" + urlParameters);

			URL url = new URL(initialUrl + urlParameters);
			//log.debug("url::::" + url);
			if(Utilities.removesqlconstants(url.toString())){
				HttpURLConnection huc = (HttpURLConnection) url.openConnection();
			huc.setRequestMethod("GET");
			if (huc.getDoOutput() == false) {
				huc.setDoOutput(true);
			}
			huc.connect();
			int code = huc.getResponseCode();
			int errorCode = 404;
			if (code == errorCode) {
				if(getDoc!=null && !getDoc.equals("") && getDoc.equals("true")){
					setFilepath(null);
				}else{
					response.sendRedirect("cb_errorPage.jsp");
				}
			} else {
				if(getDoc!=null && !getDoc.equals("") && getDoc.equals("true")){
					String fileName;
					//log.debug("Inside if");
					@SuppressWarnings("deprecation")
					String tempDir=request.getRealPath("")+"/jsp";
				//	log.debug("tempDir :"+tempDir);
					tempDir=tempDir+"/temp";
				//	log.debug("tempDir : "+tempDir);
					File file = new File(tempDir);
					if(!file.exists()){
						file.mkdir();
					}
					String filePath=file.getAbsolutePath();
					Calendar now = Calendar.getInstance();
					if(filetype!=null && !filetype.equals("")){
						 fileName="temphtml"+"["+ now.get(now.DAY_OF_MONTH) + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)+ now.get(now.HOUR_OF_DAY)+ now.get(now.MINUTE)+ now.get(now.SECOND) + "]."+filetype;
					}else{
						 fileName="temphtml"+"["+ now.get(now.DAY_OF_MONTH) + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)+ now.get(now.HOUR_OF_DAY)+ now.get(now.MINUTE)+ now.get(now.SECOND) + "]";
					}
					fileUrl(initialUrl + urlParameters,fileName,filePath);
				/*	log.debug("filepath"+filepath);
					filePath=filepath+"/"+fileName;
					setFilepath(filePath);
					getFilepath();*/
				}else{
					//log.debug("Inside else");
					response.sendRedirect(initialUrl + urlParameters);
				}
			}
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
	}

	////
	public void getAttachmentFromDcms(String getDoc,String filetype,String docIdStr,String encStr) {
		response.setContentType("text/html;charset=UTF-8");
		boolean isEncryptRequest = false;
		String ipAddress = getDatafromXml("dcmsIpAddress");
//		String getDoc=request.getParameter("getDoc");
//		String filetype=request.getParameter("ftype");
		String initialUrl = ipAddress
				+ "/FileData-war/DcmsRetrieveServlet?";
//		String docIdStr = request.getParameter("docId");
//		String encStr = request.getParameter("encrypted");

		if (encStr != null) {
			isEncryptRequest = encStr.equals("true") ? true : false;
		}
		try {
			String urlParameters = getSignedURL(docIdStr, isEncryptRequest);
			//log.debug("urlParameters:::::" + urlParameters);

			URL url = new URL(initialUrl + urlParameters);
			//log.debug("url::::" + url);
			if(Utilities.removesqlconstants(url.toString())){
				HttpURLConnection huc = (HttpURLConnection) url.openConnection();
			huc.setRequestMethod("GET");
			if (huc.getDoOutput() == false) {
				huc.setDoOutput(true);
			}
			huc.connect();
			int code = huc.getResponseCode();
			int errorCode = 404;
			if (code == errorCode) {
				if(getDoc!=null && !getDoc.equals("") && getDoc.equals("true")){
					setFilepath(null);
				}else{
					response.sendRedirect("cb_errorPage.jsp");
				}
			} else {
				if(getDoc!=null && !getDoc.equals("") && getDoc.equals("true")){
					String fileName;
					//log.debug("Inside if");
					@SuppressWarnings("deprecation")
					String tempDir=request.getRealPath("")+"/jsp";
					//	log.debug("tempDir :"+tempDir);
					tempDir=tempDir+"/temp";
					//log.debug("tempDir : "+tempDir);
					File file = new File(tempDir);
					if(!file.exists()){
						file.mkdir();
					}
					String filePath=file.getAbsolutePath();
					Calendar now = Calendar.getInstance();
					if(filetype!=null && !filetype.equals("")){
						 fileName="temphtml"+"["+ now.get(now.DAY_OF_MONTH) + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)+ now.get(now.HOUR_OF_DAY)+ now.get(now.MINUTE)+ now.get(now.SECOND) + "]."+filetype;
					}else{
						 fileName="temphtml"+"["+ now.get(now.DAY_OF_MONTH) + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)+ now.get(now.HOUR_OF_DAY)+ now.get(now.MINUTE)+ now.get(now.SECOND) + "]";
					}
			
					fileUrl(initialUrl + urlParameters,fileName,filePath);
					request.setAttribute("filePath",filePath);
					request.setAttribute("fileName",fileName);
				/*	log.debug("filepath"+filepath);
					filePath=filepath+"/"+fileName;
					setFilepath(filePath);
					getFilepath();*/
				}else{
					//log.debug("Inside else");
					response.sendRedirect(initialUrl + urlParameters);
				}
			}
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
	}
	
	////
	
	
	public void fileUrl(String fAddress, String localFileName,String destinationDir) {
		//log.debug("localFileName : "+localFileName);
		//log.debug("destinationDir : "+destinationDir);
		OutputStream outStream = null;
		URLConnection  uCon = null;
		InputStream is = null;
		final int size=1024;
		try {
			URL Url;
			byte[] buf;
			int ByteRead,ByteWritten=0;
			Url= new URL(fAddress);
			outStream = new BufferedOutputStream(new FileOutputStream(destinationDir+"/"+localFileName));
			uCon = Url.openConnection();
			is = uCon.getInputStream();
			buf = new byte[size];
			while ((ByteRead = is.read(buf)) != -1) {
				outStream.write(buf, 0, ByteRead);
				ByteWritten += ByteRead;
			}
			//String pdfFileName=cbuAction.convertDOCtoImage(localFileName,destinationDir);
			setFilepath(localFileName);
		//	log.debug(getFilepath());
			//log.debug("Downloaded Successfully.");
			//log.debug("File name:\""+localFileName+ "\"\nNo ofbytes :" + ByteWritten);
		}
		catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		finally {
			try {
				is.close();
				outStream.close();
			}
			catch (IOException e) {
				log.error(e.getMessage());
				e.printStackTrace();
			}
		}
	}
	
	public String getAttachedDoc()throws Exception{
		try{
			getAttachmentFromDcms();
		}catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return "success";
	}
	
	
	
	public String getnmdpLink(){
		List<String> linklist;
        linklist=new ArrayList<String>();
		try {
	        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
	        @SuppressWarnings("deprecation")
			Document doc = docBuilder.parse (new File(request.getRealPath("") + "/jsp/xml/NMDP-Links.xml")); // normalize text representation
	        doc.getDocumentElement ().normalize ();
	        NodeList listofLinks = doc.getElementsByTagName("links");
	        
	        for(int s=0; s<listofLinks.getLength() ; s++){

	            Node firstnode = listofLinks.item(s);
	            if(firstnode.getNodeType() == Node.ELEMENT_NODE){
	            	
	                Element firstElement = (Element)firstnode; //——-
	                
	                linkId=firstElement.getAttribute("id");
	               
	                NodeList nmdpurlList = firstElement.getElementsByTagName("url");
	            if((Element)nmdpurlList.item(0) !=null){
	                Element urlElement = (Element)nmdpurlList.item(0);
	             
	                NodeList textFNList = urlElement.getChildNodes();
	                if((Node)textFNList.item(0)!=null){
	                	url=((Node)textFNList.item(0)).getNodeValue().trim();
	                	}else{
	                		url = null;
	                	}
	             	}
	                NodeList lastNameList = firstElement.getElementsByTagName("desc");
	                Element lastNameElement = (Element)lastNameList.item(0); 
	                NodeList textLNList = lastNameElement.getChildNodes();
	                urlDesc=((Node)textLNList.item(0)).getNodeValue().trim();
	                
	                linklist=new ArrayList<String>();
	                linklist.add(linkId);
	                linklist.add(urlDesc);
	                //if(url !=null || url !=""){
	                linklist.add(url);
	               // }
	                
					urlList=getUrlList();
					urlList.add(linklist);
	            }
	        }
	        System.out.println(urlList);
	        setUrlList(urlList);
	    }
		 catch (SAXException e) {
				// TODO Auto-generated catch block
			 	log.error(e.getMessage());
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				log.error(e.getMessage());
				e.printStackTrace();
			}
		 catch (ParserConfigurationException e1) {
			// TODO Auto-generated catch block
			 log.error(e1.getMessage());
			e1.printStackTrace();
		}
		 catch (Exception e) {
			// TODO: handle exception
			 log.error(e.getMessage());
			 e.printStackTrace();
		}
		return "success";
	}
	
	public String updatenmdpLink(){
		List<String> linklist;
        linklist=new ArrayList<String>();
		try {
	        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
	        @SuppressWarnings("deprecation")
	        String filepath=request.getRealPath("") + "/jsp/xml/NMDP-Links.xml";
			Document doc = docBuilder.parse (new File(filepath)); // normalize text representation
	        doc.getDocumentElement ().normalize ();
	        NodeList listofLinks = doc.getElementsByTagName("links");
	        
	        for(int s=0; s<listofLinks.getLength() ; s++){

	            Node firstnode = listofLinks.item(s);
	            if(firstnode.getNodeType() == Node.ELEMENT_NODE){
	            	
	                Element firstElement = (Element)firstnode; //——-
	                if(firstElement.getAttribute("id").equals(getLinkId())){
		                
	                	firstElement.setAttribute("id", getLinkId());
		                
		                NodeList nmdpurlList = firstElement.getElementsByTagName("url");
		                Element urlElement = (Element)nmdpurlList.item(0);      
		                urlElement.setTextContent(getUrl());
		                
		               
		                NodeList urlDesc = firstElement.getElementsByTagName("desc");
		                Element urlDescElem = (Element)urlDesc.item(0);
		                urlDescElem.setTextContent(getUrlDesc());
		                
		                TransformerFactory transformerFactory = TransformerFactory.newInstance();
		        		Transformer transformer = transformerFactory.newTransformer();
		        		DOMSource source = new DOMSource(doc);
		        		StreamResult result = new StreamResult(new File(filepath));
		        		transformer.transform(source, result);
		         
		        		//log.debug("Done");
		        		
	                }
	            }
	        }
	        getnmdpLink();
	    }
		 catch (SAXException e) {
				// TODO Auto-generated catch block
			 	log.error(e.getMessage());
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				log.error(e.getMessage());
				e.printStackTrace();
			}
		 catch (ParserConfigurationException e1) {
			// TODO Auto-generated catch block
			 log.error(e1.getMessage());
			e1.printStackTrace();
		}
		 catch (Exception e) {
			// TODO: handle exception
			 log.error(e.getMessage());
			 e.printStackTrace();
		}
		return "success";
	}
	
public List<String> getnmdpLinkFrCbbProfile(){		
		
		List<String> linklist;
        linklist=new ArrayList<String>();
		try {
	        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
	        @SuppressWarnings("deprecation")
			Document doc = docBuilder.parse (new File(request.getRealPath("") + "/jsp/xml/NMDP-Links.xml")); // normalize text representation
	        doc.getDocumentElement ().normalize ();
	        NodeList listofLinks = doc.getElementsByTagName("links");
	        
	        for(int s=0; s<listofLinks.getLength() ; s++){

	            Node firstnode = listofLinks.item(s);
	            if(firstnode.getNodeType() == Node.ELEMENT_NODE){
	            	
	                Element firstElement = (Element)firstnode;
	                
	                linkId=firstElement.getAttribute("id");
	                
	                NodeList nmdpurlList = firstElement.getElementsByTagName("url");
	                Element urlElement = (Element)nmdpurlList.item(0); NodeList textFNList = urlElement.getChildNodes();
	                url=((Node)textFNList.item(0)).getNodeValue().trim();
	               
	                NodeList lastNameList = firstElement.getElementsByTagName("desc");
	                Element lastNameElement = (Element)lastNameList.item(0); NodeList textLNList = lastNameElement.getChildNodes();
	                urlDesc=((Node)textLNList.item(0)).getNodeValue().trim();
	                 if(linkId.equals("6"))
	                 {
	                	 linklist.add(urlDesc);
	                	 linklist.add(url);
	                	 
	                 }
	               
	            }
	        }	        
	    }
		 catch (SAXException e) {
				// TODO Auto-generated catch block
			 log.error(e.getMessage());
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				log.error(e.getMessage());
				e.printStackTrace();
			}
		 catch (ParserConfigurationException e1) {
			// TODO Auto-generated catch block
			 log.error(e1.getMessage());
			e1.printStackTrace();
		}
		 catch (Exception e) {
			// TODO: handle exception
			 log.error(e.getMessage());
			 e.printStackTrace();
		}
		return linklist;
	}


}