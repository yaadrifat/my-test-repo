package com.velos.ordercomponent.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;

import com.velos.ordercomponent.business.domain.AppMessages;
import com.velos.ordercomponent.business.pojoobjects.AppMessagesPojo;
import com.velos.ordercomponent.business.pojoobjects.GrpsPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.pojoobjects.SearchPojo;
import com.velos.ordercomponent.business.pojoobjects.SitePojo;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.Utilities;
import com.velos.ordercomponent.controller.AppMessagesController;
import com.velos.ordercomponent.controller.CBBController;
import com.velos.ordercomponent.controller.VelosSearchController;
import com.velos.ordercomponent.util.GarudaConfig;

public class AppMessagesAction extends VelosBaseAction {
	public static final Log log = LogFactory.getLog(AppMessagesAction.class);
	private Boolean addNewMsg;
	private String esignFlag;
	private AppMessagesPojo appMessagesPojo;
	private SearchPojo searchPojo;
	private VelosSearchController velosSearchController;
	private List appMsgList;
	private Long pkActiveMsg;
	private Long pkInActiveMsg;
	private Long pkPendingMsg;
	private Long pkActiveAllways;
	private Long pkLandingPage;
	private Long pkLoginPage;
	private Long pkAppMsgs;
	private String applicationMsgDesc;
	private Date appMsgStartDt;
	private String appMsgStartDtStr;
	private Date appMsgEndDt;
	private String appMsgEndDtStr;
	private Long appMsgDispAt;
	private Long appMsgStatus;
	private Long appMsgfkSite;
	private Long appMsgfkGrps;
	private AppMessagesController appMessagesController;
	private Long pkLogOn = null;
	private CBBController cbbController;
	private PaginateSearch paginateSearch;
	
	
	public Long getPkAppMsgs() {
		return pkAppMsgs;
	}

	public void setPkAppMsgs(Long pkAppMsgs) {
		this.pkAppMsgs = pkAppMsgs;
	}

	public Long getPkActiveAllways() {
		return pkActiveAllways;
	}

	public void setPkActiveAllways(Long pkActiveAllways) {
		this.pkActiveAllways = pkActiveAllways;
	}

	public Long getPkLandingPage() {
		return pkLandingPage;
	}

	public void setPkLandingPage(Long pkLandingPage) {
		this.pkLandingPage = pkLandingPage;
	}

	public Long getPkLoginPage() {
		return pkLoginPage;
	}

	public void setPkLoginPage(Long pkLoginPage) {
		this.pkLoginPage = pkLoginPage;
	}

	public Long getPkActiveMsg() {
		return pkActiveMsg;
	}

	public void setPkActiveMsg(Long pkActiveMsg) {
		this.pkActiveMsg = pkActiveMsg;
	}

	public Long getPkInActiveMsg() {
		return pkInActiveMsg;
	}

	public void setPkInActiveMsg(Long pkInActiveMsg) {
		this.pkInActiveMsg = pkInActiveMsg;
	}

	public Long getPkPendingMsg() {
		return pkPendingMsg;
	}

	public void setPkPendingMsg(Long pkPendingMsg) {
		this.pkPendingMsg = pkPendingMsg;
	}

	public List getAppMsgList() {
		return appMsgList;
	}

	public void setAppMsgList(List appMsgList) {
		this.appMsgList = appMsgList;
	}

	public AppMessagesPojo getAppMessagesPojo() {
		return appMessagesPojo;
	}

	public void setAppMessagesPojo(AppMessagesPojo appMessagesPojo) {
		this.appMessagesPojo = appMessagesPojo;
	}

	public Boolean getAddNewMsg() {
		return addNewMsg;
	}

	public void setAddNewMsg(Boolean addNewMsg) {
		this.addNewMsg = addNewMsg;
	}

	public String getEsignFlag() {
		return esignFlag;
	}

	public void setEsignFlag(String esignFlag) {
		this.esignFlag = esignFlag;
	}

	public String getApplicationMsgDesc() {
		return applicationMsgDesc;
	}

	public void setApplicationMsgDesc(String applicationMsgDesc) {
		this.applicationMsgDesc = applicationMsgDesc;
	}

	public Date getAppMsgStartDt() {
		return appMsgStartDt;
	}

	public void setAppMsgStartDt(Date appMsgStartDt) {
		this.appMsgStartDt = appMsgStartDt;
	}

	public String getAppMsgStartDtStr() {
		return appMsgStartDtStr;
	}

	public void setAppMsgStartDtStr(String appMsgStartDtStr) {
		this.appMsgStartDtStr = appMsgStartDtStr;
	}

	public Date getAppMsgEndDt() {
		return appMsgEndDt;
	}

	public void setAppMsgEndDt(Date appMsgEndDt) {
		this.appMsgEndDt = appMsgEndDt;
	}

	public String getAppMsgEndDtStr() {
		return appMsgEndDtStr;
	}

	public void setAppMsgEndDtStr(String appMsgEndDtStr) {
		this.appMsgEndDtStr = appMsgEndDtStr;
	}

	public Long getAppMsgDispAt() {
		return appMsgDispAt;
	}

	public void setAppMsgDispAt(Long appMsgDispAt) {
		this.appMsgDispAt = appMsgDispAt;
	}

	public Long getAppMsgStatus() {
		return appMsgStatus;
	}

	public void setAppMsgStatus(Long appMsgStatus) {
		this.appMsgStatus = appMsgStatus;
	}

	public Long getAppMsgfkSite() {
		return appMsgfkSite;
	}

	public void setAppMsgfkSite(Long appMsgfkSite) {
		this.appMsgfkSite = appMsgfkSite;
	}

	public Long getAppMsgfkGrps() {
		return appMsgfkGrps;
	}

	public void setAppMsgfkGrps(Long appMsgfkGrps) {
		this.appMsgfkGrps = appMsgfkGrps;
	}
	private List<GrpsPojo> grpPojoList = null;
	
	public AppMessagesAction(){
		appMessagesPojo = new AppMessagesPojo();
		velosSearchController = new VelosSearchController();
		appMessagesController = new AppMessagesController();
		cbbController = new CBBController();
		paginateSearch = new PaginateSearch();
		grpPojoList = new ArrayList<GrpsPojo>();
	}

	@Override
	public Object getModel() {
		return null;
	}

	@Override
	public String executeAction() throws Exception {
		return null;
	}
	
	public String getAppMessages(){
		
		try {
			setAppMsgList(getApplicationMsgList());
			getAlltypeMsgLists(getAppMsgList());
			getPkAllTypePks();
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return "success";
	}
	
	public void getAlltypeMsgLists(List msgList){
		List activeMsgList = new ArrayList();
		List pendingMsgList = new ArrayList();
		List inactiveMsgList = new ArrayList();
		pkInActiveMsg = getCodeListPkByTypeAndSubtype(APPMSG_STATUS,APPMSG_STATUS_INACTIVE);
		setPkInActiveMsg(pkInActiveMsg);
		Iterator  it = msgList.iterator();
		while(it.hasNext()){
			appMessagesPojo = new AppMessagesPojo();
			appMessagesPojo = (AppMessagesPojo)it.next();
			if(appMessagesPojo.getMsgStatus()!=null && !appMessagesPojo.getMsgStatus().equals(getPkInActiveMsg())){
				if((appMessagesPojo.getMsgStartDt()==null && appMessagesPojo.getMsgEndDt()==null) || (appMessagesPojo.getMsgStartDt()!=null && (appMessagesPojo.getMsgStartDt().before(new Date()) || appMessagesPojo.getMsgStartDt().equals(new Date())))){
					activeMsgList.add(appMessagesPojo);	
				}
				if(appMessagesPojo.getMsgStartDt()!=null && appMessagesPojo.getMsgStartDt().after(new Date())){
					pendingMsgList.add(appMessagesPojo);
				}
			}
			if(appMessagesPojo.getMsgStatus()!=null && appMessagesPojo.getMsgStatus().equals(getPkInActiveMsg())){
				inactiveMsgList.add(appMessagesPojo);
			}
		}
		request.setAttribute("activeMsgList", activeMsgList);
		request.setAttribute("pendingMsgList", pendingMsgList);
		request.setAttribute("inactiveMsgList", inactiveMsgList);
		
	}
	private void getPkAllTypePks(){
		try{
		pkInActiveMsg = getCodeListPkByTypeAndSubtype(APPMSG_STATUS,APPMSG_STATUS_INACTIVE);
		setPkInActiveMsg(pkInActiveMsg);
		pkActiveAllways = getCodeListPkByTypeAndSubtype(APPMSG_STATUS,APPMSG_STATUS_ACTIVE_ALLWAYS);
		setPkActiveAllways(pkActiveAllways);
		pkLandingPage = getCodeListPkByTypeAndSubtype(PAGE_TYPE,LANDING_PAGE_TYPE);
		setPkLandingPage(pkLandingPage);
		pkLoginPage = getCodeListPkByTypeAndSubtype(PAGE_TYPE,LOGIN_PAGE_TYPE);
		setPkLoginPage(pkLoginPage);
		List<SitePojo> cbbList = cbbController.getSitesWithPagination(paginateSearch,0);
		getRequest().setAttribute("CbbList", cbbList);
		getRequest().setAttribute("grpList", grpPojoList);
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}
	}
	
	private List getApplicationMsgList(){
		List list = null;
		try{
			list = new ArrayList();
			searchPojo = new SearchPojo();	
			searchPojo.setSearchType(APPMESSAGES_OBJECTNAME);
			searchPojo = velosSearchController.velosSearch(searchPojo);
			appMsgList = searchPojo.getResultlst();
			Iterator it = appMsgList.iterator();
			AppMessagesPojo appMsgPojo = null;
			while(it.hasNext()){
				AppMessages appMessages = new AppMessages();
				appMsgPojo = new AppMessagesPojo();
				appMessages = (AppMessages)it.next();
				appMsgPojo = (AppMessagesPojo) ObjectTransfer.transferObjects(appMessages,
						appMsgPojo);
				list.add(appMsgPojo);
			}
		
		}catch(Exception e){
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return list;
	}
	public String saveAppMessages(){
		getPkAllTypePks();
		if(!getAddNewMsg()){
			try {
				setEsignFlag("add");
				setAppMessagesPojo(new AppMessagesPojo());
			} catch (Exception e) {
				e.printStackTrace();
				log.error(e.getMessage());
			}
		}
		if(getAddNewMsg()){
			try {
				appMessagesPojo = getAppMessagesPojo();
				appMessagesPojo.setMsgStatus(getAppMsgStatus());
				appMessagesPojo.setMsgDesc(getApplicationMsgDesc());
				appMessagesPojo.setMsgDispAt(getAppMsgDispAt());
				if(appMessagesPojo.getMsgStatus()!=null && appMessagesPojo.getMsgStatus().equals(pkActiveAllways)){
					appMessagesPojo.setMsgStartDt(null);
					appMessagesPojo.setMsgEndDt(null);
				}else{
					if(getAppMsgStartDtStr()!=null && !getAppMsgStartDtStr().equals("")){
						appMessagesPojo.setMsgStartDt(Utilities.getDate(getAppMsgStartDtStr(), DATE_FORMAT_MMM_dd_YYYY));
					}
					if(getAppMsgEndDtStr()!=null && !getAppMsgEndDtStr().equals("")){
						appMessagesPojo.setMsgEndDt(Utilities.getDate(getAppMsgEndDtStr(), DATE_FORMAT_MMM_dd_YYYY));
					}	
				}
				if(appMessagesPojo.getMsgDispAt()!=null && appMessagesPojo.getMsgDispAt().equals(pkLandingPage)){
					appMessagesPojo.setFkSite(null);
					appMessagesPojo.setFkGrps(null);
				}
				else{
					appMessagesPojo.setFkSite(getAppMsgfkSite());
					appMessagesPojo.setFkGrps(getAppMsgfkGrps());
				}
				
				appMessagesPojo = appMessagesController.updateAppMessages(appMessagesPojo);
				pkLogOn = getCodeListPkByTypeAndSubtype(PAGE_TYPE, LOGIN_PAGE_TYPE);
				if(appMessagesPojo!=null && pkLogOn!=null && appMessagesPojo.getMsgDispAt()!=null && appMessagesPojo.getMsgDispAt().equals(pkLogOn)){
					GarudaConfig.initializeLoginAppMessages(ServletActionContext.getServletContext());
				}
				setEsignFlag("add");
			} catch (Exception e) {
				e.printStackTrace();
				log.error(e.getMessage());
			}
		}
		setAppMsgList(getApplicationMsgList());
		getAlltypeMsgLists(getAppMsgList());
		return "success";
	}
	
	public String deleteAppMessages(){
		if(getAppMessagesPojo().getPkAppMsg()!=null){
			try {
			appMessagesPojo = appMessagesController.getAppMessagesById(getAppMessagesPojo());
			}catch (Exception e) {
				e.printStackTrace();
				log.error(e.getMessage());
			}
		}	
		if(!getAddNewMsg()){
				try {
					setAppMessagesPojo(appMessagesPojo);
					setPkAppMsgs(getAppMessagesPojo().getPkAppMsg());
					setEsignFlag("delete");
				} catch (Exception e) {
					e.printStackTrace();
					log.error(e.getMessage());
				}
		}
		if(getAddNewMsg()){
			try {
				appMessagesPojo.setDeletedFlag("1");
				appMessagesPojo = appMessagesController.updateAppMessages(appMessagesPojo);
				pkLogOn = getCodeListPkByTypeAndSubtype(PAGE_TYPE, LOGIN_PAGE_TYPE);
				if(appMessagesPojo!=null && pkLogOn!=null && appMessagesPojo.getMsgDispAt()!=null && appMessagesPojo.getMsgDispAt().equals(pkLogOn)){
					GarudaConfig.initializeLoginAppMessages(ServletActionContext.getServletContext());
				}
				setEsignFlag("delete");
			} catch (Exception e) {
				e.printStackTrace();
				log.error(e.getMessage());
			}
	}
			setAppMsgList(getApplicationMsgList());
			getAlltypeMsgLists(getAppMsgList());
			getPkAllTypePks();
		return "success";
	}

	public String updateAppMessages(){
		getPkAllTypePks();
		if(getAppMessagesPojo().getPkAppMsg()!=null){
			try {
				appMessagesPojo = appMessagesController.getAppMessagesById(getAppMessagesPojo());
			}catch (Exception e) {
				e.printStackTrace();
				log.error(e.getMessage());
			}
		}
		if(!getAddNewMsg()){
			try {
					setPkAppMsgs(getAppMessagesPojo().getPkAppMsg());
					setApplicationMsgDesc(appMessagesPojo.getMsgDesc());
					setAppMsgStartDt(appMessagesPojo.getMsgStartDt());
					setAppMsgEndDt(appMessagesPojo.getMsgEndDt());
					setAppMsgDispAt(appMessagesPojo.getMsgDispAt());
					setAppMsgStatus(appMessagesPojo.getMsgStatus());
					setAppMsgfkSite(appMessagesPojo.getFkSite());
					setAppMsgfkGrps(appMessagesPojo.getFkGrps());
					setAppMessagesPojo(appMessagesPojo);
					setEsignFlag("edit");
					Long siteId = getAppMsgfkSite();
					if(siteId==null || siteId.equals("")){
						siteId = 0l;
					}
					grpPojoList = getGrpPojoList(siteId);
					getRequest().setAttribute("grpList", grpPojoList);
			} catch (Exception e) {
				e.printStackTrace();
				log.error(e.getMessage());
			}
			
		}
		if(getAddNewMsg()){
			try {
				if(appMessagesPojo.getPkAppMsg()!=null){
					appMessagesPojo.setMsgDispAt(getAppMsgDispAt());
					appMessagesPojo.setMsgStatus(getAppMsgStatus());
					appMessagesPojo.setMsgDesc(getApplicationMsgDesc());
					if(appMessagesPojo.getMsgStatus()!=null && appMessagesPojo.getMsgStatus().equals(pkActiveAllways)){
						appMessagesPojo.setMsgStartDt(null);
						appMessagesPojo.setMsgEndDt(null);
					}else{
						if(getAppMsgStartDtStr()!=null && !getAppMsgStartDtStr().equals("")){
							appMessagesPojo.setMsgStartDt(Utilities.getDate(getAppMsgStartDtStr(), DATE_FORMAT_MMM_dd_YYYY));
						}
						if(getAppMsgEndDtStr()!=null && !getAppMsgEndDtStr().equals("")){
							appMessagesPojo.setMsgEndDt(Utilities.getDate(getAppMsgEndDtStr(), DATE_FORMAT_MMM_dd_YYYY));
						}	
					}
					if(appMessagesPojo.getMsgDispAt()!=null && appMessagesPojo.getMsgDispAt().equals(pkLandingPage)){
						appMessagesPojo.setFkSite(null);
						appMessagesPojo.setFkGrps(null);
					}
					else{
						appMessagesPojo.setFkSite(getAppMsgfkSite());
						appMessagesPojo.setFkGrps(getAppMsgfkGrps());
					}
					appMessagesPojo = appMessagesController.updateAppMessages(appMessagesPojo);
					pkLogOn = getCodeListPkByTypeAndSubtype(PAGE_TYPE, LOGIN_PAGE_TYPE);
					if(appMessagesPojo!=null && pkLogOn!=null && appMessagesPojo.getMsgDispAt()!=null && appMessagesPojo.getMsgDispAt().equals(pkLogOn)){
						GarudaConfig.initializeLoginAppMessages(ServletActionContext.getServletContext());
					}
					setEsignFlag("edit");
				}
			}catch (Exception e) {
				e.printStackTrace();
				log.error(e.getMessage());
			}
		}
		setAppMsgList(getApplicationMsgList());
		getAlltypeMsgLists(getAppMsgList());
		return "success";
	}
	
	public String showGrpsBySiteId(){
		try{
		appMessagesPojo = appMessagesController.getAppMessagesById(getAppMessagesPojo());
		setApplicationMsgDesc(appMessagesPojo.getMsgDesc());
		setAppMsgStartDt(appMessagesPojo.getMsgStartDt());
		setAppMsgEndDt(appMessagesPojo.getMsgEndDt());
		setAppMsgDispAt(appMessagesPojo.getMsgDispAt());
		setAppMsgStatus(appMessagesPojo.getMsgStatus());
		setAppMsgfkSite(appMessagesPojo.getFkSite());
		setAppMsgfkGrps(appMessagesPojo.getFkGrps());
		setAppMessagesPojo(appMessagesPojo);
		setEsignFlag(getEsignFlag());
		String fkSite = "";
		if(getRequest()!=null){
		 fkSite= getRequest().getParameter("appMsgfkSite");
		}
		Long siteId = 0l;
		if(!fkSite.equals("")){
			siteId = Long.parseLong(fkSite);	
		}
		grpPojoList = getGrpPojoList(siteId);
		List<SitePojo> cbbList = cbbController.getSitesWithPagination(paginateSearch,0);
		getRequest().setAttribute("CbbList", cbbList);
		getRequest().setAttribute("grpList", grpPojoList);
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return "success";
	}
	
	public List<GrpsPojo> getGrpPojoList(Long siteId){
		try{
			List grpList = appMessagesController.getGroupsBySiteId(siteId);
			GrpsPojo grpsPojo = null;
			if(grpList!=null && grpList.size()>0){
				for(int i=0;i<grpList.size();i++){
					grpsPojo = new GrpsPojo();
					Object[] obj = null;
					obj = (Object[])grpList.get(i);
					grpsPojo.setPkGrp(Long.parseLong(obj[0]+""));
					grpsPojo.setGrpName(""+obj[1]);
					grpPojoList.add(grpsPojo);
				}
				
			}
		}catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return grpPojoList;
	}

}
