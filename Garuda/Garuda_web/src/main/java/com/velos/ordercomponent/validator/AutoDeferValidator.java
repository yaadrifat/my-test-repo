package com.velos.ordercomponent.validator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;
import com.velos.ordercomponent.business.pojoobjects.PatLabsPojo;
import com.velos.ordercomponent.controller.CBUController;
import com.velos.ordercomponent.controller.DynaFormController;
import com.velos.ordercomponent.util.ObjectFactory;
import com.velos.ordercomponent.util.VelosGarudaConstants;

import static com.velos.ordercomponent.action.CordEntryValidatorAction.getCodeListPkByTypeAndSubtype;
import static com.velos.ordercomponent.util.VelosGarudaConstants.FUNGAL_STATUS;
import static com.velos.ordercomponent.util.VelosGarudaConstants.POSITIVE;
import static com.velos.ordercomponent.util.VelosGarudaConstants.BACTERIAL_STATUS;
import static com.velos.ordercomponent.util.VelosGarudaConstants.NOTDONE;
import static com.velos.ordercomponent.util.VelosGarudaConstants.HEMO_PATHYSCREEN;
import static com.velos.ordercomponent.util.VelosGarudaConstants.HEMO_PATHY_BETA;
import static com.velos.ordercomponent.util.VelosGarudaConstants.HEMO_PATHY_SICKLE;
import static com.velos.ordercomponent.util.VelosGarudaConstants.HEMO_PATHY_HEMOSICK;
import static com.velos.ordercomponent.util.VelosGarudaConstants.HEMO_PATHY_ALPHA;
import static com.velos.ordercomponent.util.VelosGarudaConstants.STORAGE_TEMPERATURE;
import static com.velos.ordercomponent.util.VelosGarudaConstants.STORAGE_TEMPERATURE_GTMINUSONETHIRTYFIVE;
import static com.velos.ordercomponent.util.VelosGarudaConstants.FROZEN_IN_VIALS;
import static com.velos.ordercomponent.util.VelosGarudaConstants.FROZEN_IN_TUBES;

/**
 * @author Mohiuddin Ali Ahmed
 * @author Anurag Upadhyay
 * @version 1.0
 * 
 */

public class AutoDeferValidator extends FieldValidatorSupport {
	private String fields="";
	
	public String getFields() {
		return fields;
	}
	public void setFields(String fields) {
		this.fields = fields;
	}
	@SuppressWarnings("unchecked")
	@Override
	public void validate(Object object) throws ValidationException {
		ValueStack stack = ActionContext.getContext().getValueStack();
		 Map<String, List<String>> errorMap = (Map<String, List<String>>)stack.findValue("fieldErrors");
		 if (errorMap == null || errorMap.size() == 0) { 
			 
		 String fieldName = getFieldName();
		 String []fields=this.fields.split(",");		 
		 
		 Long cfuCountPk= (Long)getFieldValue(fields[0], object);
		 Long viabilityPk= (Long)getFieldValue(fields[1], object);		
		 List<PatLabsPojo> savePostTestList=(List<PatLabsPojo>) getFieldValue(fields[2], object);
		 Long cordID= (Long)getFieldValue(fields[3], object);
		 Long fungulResult= (Long)getFieldValue(fields[4], object);
		 Long bactResult= (Long)getFieldValue(fields[5], object);
		 Long hemoglobinScrn= (Long)getFieldValue(fields[6], object);
		 Long fkMultipleBirth= (Long)getFieldValue(fields[7], object);
		 Long noSegAvail= (Long)getFieldValue(fields[8], object);
		 Long fkCbbProcedure=(Long)getFieldValue(fields[9], object);
	  	//CFU COUNT AUTO DEFER			
		     if(cfuCountPk!=null &&((cfuCountPk!=null && savePostTestList.get(12).getPkpatlabs().equals(cfuCountPk)&& savePostTestList.get(12).getTestresult()!=null && !savePostTestList.get(12).getTestresult().equals("") && Long.parseLong((savePostTestList.get(12).getTestresult()).trim())==0)
		    		             ||(viabilityPk!=null && savePostTestList.get(13).getPkpatlabs().equals(viabilityPk) &&  savePostTestList.get(13).getTestresult()!=null && !savePostTestList.get(13).getTestresult().equals("") && (Long.parseLong((savePostTestList.get(13).getTestresult()).trim())>=0 && Long.parseLong((savePostTestList.get(13).getTestresult()).trim())<=84))
		    		             ||(fungulResult!=null && fungulResult.equals(getCodeListPkByTypeAndSubtype(FUNGAL_STATUS, POSITIVE)))
		    		             ||(bactResult!=null &&( bactResult.equals(getCodeListPkByTypeAndSubtype(BACTERIAL_STATUS, POSITIVE)) || bactResult.equals(getCodeListPkByTypeAndSubtype(BACTERIAL_STATUS, NOTDONE))))
		    		             ||((hemoglobinScrn!=null) &&(( hemoglobinScrn.equals(getCodeListPkByTypeAndSubtype(HEMO_PATHYSCREEN, HEMO_PATHY_BETA)))
						                 ||  hemoglobinScrn.equals(getCodeListPkByTypeAndSubtype(HEMO_PATHYSCREEN, HEMO_PATHY_SICKLE))
						                 ||  hemoglobinScrn.equals(getCodeListPkByTypeAndSubtype(HEMO_PATHYSCREEN, HEMO_PATHY_HEMOSICK))
						                 ||  hemoglobinScrn.equals(getCodeListPkByTypeAndSubtype(HEMO_PATHYSCREEN, HEMO_PATHY_ALPHA))))
						         ||(fkMultipleBirth!=null && fkMultipleBirth.equals(1l)) 
						         ||(noSegAvail!=null && (noSegAvail.equals(1l) || noSegAvail.equals(0l)))
		    		             ||(IDMDeferStatus(cordID))
		    		             ||(CBBAutoDeferStatus(fkCbbProcedure))
		     )){
			   addFieldError(fieldName, object);
		       return;	
		     }
	}

  }
	private boolean IDMDeferStatus(Long cordID){
		String deferstatus;
		try {
			deferstatus = new DynaFormController().getidmdefervalue(cordID);
			if(deferstatus!=null && !deferstatus.equals("") && deferstatus.equals("1")){
				
			       return true;
			}			
		} catch (Exception e) {				
			e.printStackTrace();
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	private boolean CBBAutoDeferStatus(Long fkCbbProcedure){
	if(fkCbbProcedure!=null && fkCbbProcedure!=-1){
		 StringBuilder sql=new StringBuilder();
		 sql.append("select FK_STOR_TEMP,FK_FROZEN_IN from CBB_PROCESSING_PROCEDURES_INFO where FK_PROCESSING_ID=").append(fkCbbProcedure).append("order by FK_PROCESSING_ID desc");
		 CBUController cbuController = ((CBUController)ObjectFactory.MakeObjects.getFactoryObject(CBUController.class.getName()));

	       try {
			 ArrayList<Object>idCountLst=(ArrayList<Object>)cbuController.validateDataExistence(sql);
			 if(idCountLst!=null && idCountLst.size()>0 &&(((BigDecimal)((Object[])idCountLst.get(0))[0]).equals(getCodeListPkByTypeAndSubtype(STORAGE_TEMPERATURE, STORAGE_TEMPERATURE_GTMINUSONETHIRTYFIVE))
					 || ((BigDecimal)((Object[])idCountLst.get(0))[1]).equals(getCodeListPkByTypeAndSubtype(VelosGarudaConstants.FROZEN_IN, FROZEN_IN_VIALS))
					 || ((BigDecimal)((Object[])idCountLst.get(0))[1]).equals(getCodeListPkByTypeAndSubtype(VelosGarudaConstants.FROZEN_IN, FROZEN_IN_TUBES))
					 )){
				     return true;
			 }
		
		} catch (Exception e) {						
			e.printStackTrace();
		}	
		}
		return false;
		
	}
}
