package com.velos.ordercomponent.controller;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.ordercomponent.business.domain.AntigenEncoding;
import com.velos.ordercomponent.business.domain.RecipientInfo;
import com.velos.ordercomponent.business.pojoobjects.AdditionalIdsPojo;
import com.velos.ordercomponent.business.pojoobjects.AntigenPojo;
import com.velos.ordercomponent.business.pojoobjects.BestHLAPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUCompleteReqInfoPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUFinalReviewPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUMinimumCriteriaPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUMinimumCriteriaTempPojo;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.DumnPojo;
import com.velos.ordercomponent.business.pojoobjects.EligibilityDeclPojo;
import com.velos.ordercomponent.business.pojoobjects.EligibilityNewDeclPojo;
import com.velos.ordercomponent.business.pojoobjects.EntityMultipleValuesPojo;
import com.velos.ordercomponent.business.pojoobjects.EntitySamplesPojo;
import com.velos.ordercomponent.business.pojoobjects.EntityStatusPojo;
import com.velos.ordercomponent.business.pojoobjects.EntityStatusReasonPojo;
import com.velos.ordercomponent.business.pojoobjects.FinalRevUploadInfoPojo;
import com.velos.ordercomponent.business.pojoobjects.HLAPojo;
import com.velos.ordercomponent.business.pojoobjects.IDMPojo;
import com.velos.ordercomponent.business.pojoobjects.LabTestPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.pojoobjects.PatLabsPojo;
import com.velos.ordercomponent.business.pojoobjects.PatientHLAPojo;
import com.velos.ordercomponent.business.pojoobjects.SitePojo;
import com.velos.ordercomponent.business.pojoobjects.TempHLAPojo;
import com.velos.ordercomponent.service.VelosService;
import com.velos.ordercomponent.service.impl.VelosServiceImpl;
import com.velos.ordercomponent.business.domain.FinalRevUploadInfo;

/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class CBUController {
	VelosService service;
	public static final Log log = LogFactory.getLog(CBUController.class);
	public List<Object> getCBUCordInfoList(String objectName, String criteria,PaginateSearch paginateSearch) throws Exception {
		service = VelosServiceImpl.getService();
		return service.getCBUCordInfoList(objectName, criteria,paginateSearch);
	}
	
	public Long getCodeListPkIds(String parentCode, String code)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCodeListPkIds(parentCode, code);
	}
	
	public CdrCbuPojo updateCodeListStatus(CdrCbuPojo cdrcbuPojo) throws Exception{
	
		service = VelosServiceImpl.getService();
		return service.updateCodeListStatus(cdrcbuPojo);
	}
	
	public EntityStatusPojo updateEntityStatus(EntityStatusPojo entityStatusPojo) throws Exception{
		service = VelosServiceImpl.getService();
		return service.updateEntityStatus(entityStatusPojo,true);
	}
	
	public EntityStatusReasonPojo updateEntityStatusReason(EntityStatusReasonPojo entityStatusReasonPojo) throws Exception{
		service = VelosServiceImpl.getService();
		return service.updateEntityStatusReason(entityStatusReasonPojo,true);
	}
	
	public EntityStatusPojo updateEntityStatus(EntityStatusPojo entityStatusPojo,Boolean fromUI) throws Exception{
		service = VelosServiceImpl.getService();
		return service.updateEntityStatus(entityStatusPojo,fromUI);
	}
	
	public EntityStatusReasonPojo updateEntityStatusReason(EntityStatusReasonPojo entityStatusReasonPojo,Boolean fromUI) throws Exception{
		service = VelosServiceImpl.getService();
		return service.updateEntityStatusReason(entityStatusReasonPojo,fromUI);
	}
	
	public CdrCbuPojo getCordInfoById(CdrCbuPojo cdrCbuPojo) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCordInfoById(cdrCbuPojo);
	}

	public List<Object> getCbuinfo(String criteria,PaginateSearch paginateSearch,int i) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getCbuinfo(criteria.trim(),paginateSearch,i);
	}
	
	public List<Object> getCbuInfoById(String entityType, String entityValue) throws Exception {
		service = VelosServiceImpl.getService();
		return service.getCbuInfoById(entityType, entityValue);
	}
	
	public List<Object> getListCdrCBB(String criteria,PaginateSearch paginateSearch,int i) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getListCdrCBB(criteria, paginateSearch, i);
	}
	
	public List<Object> getMultipleIneligibleReasons(Long pkEntityStatus,PaginateSearch paginateSearch,int i) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getMultipleIneligibleReasons(pkEntityStatus, paginateSearch, i);
	}
	
	public List<Object> getHla(Long cordId,PaginateSearch paginateSearch,int i) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getHla(cordId, paginateSearch, i);
	}
	public List<Object> getStatusHistory(Long entityID, Long entityType,String statusTypeCode,PaginateSearch paginateSearch,int i) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getStatusHistory(entityID, entityType,statusTypeCode, paginateSearch, i);
	}
	
	public List<AdditionalIdsPojo> getCordInfoAdditionalIds(Long cordId) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getCordInfoAdditionalIds(cordId);
	}
	
	public List<AdditionalIdsPojo> saveCordInfoAdditionalIds(List<AdditionalIdsPojo> additionalIdsPojo) throws Exception{
		service=VelosServiceImpl.getService();
		return service.saveCordInfoAdditionalIds(additionalIdsPojo,false);
	}
	
	public List<AdditionalIdsPojo> saveCordInfoAdditionalIds(List<AdditionalIdsPojo> additionalIdsPojo,Boolean fromUI) throws Exception{
		service=VelosServiceImpl.getService();
		return service.saveCordInfoAdditionalIds(additionalIdsPojo,fromUI);
	}
	
	public CdrCbuPojo saveOrUpdateCordInfo(CdrCbuPojo cdrCbuPojo) throws Exception{
		service=VelosServiceImpl.getService();
		return service.saveOrUpdateCordInfo(cdrCbuPojo,true);
	}
	
	public CdrCbuPojo saveOrUpdateCordInfo(CdrCbuPojo cdrCbuPojo,Boolean fromUI) throws Exception{
		service=VelosServiceImpl.getService();
		return service.saveOrUpdateCordInfo(cdrCbuPojo,fromUI);
	}
	
	public List<HLAPojo> saveOrUpdateHlas(List<HLAPojo> hlaPojos) throws Exception {
		service=VelosServiceImpl.getService();
		return service.saveOrUpdateHlas(hlaPojos,true);
	}
	
	public List<HLAPojo> saveOrUpdateHlas(List<HLAPojo> hlaPojos,Boolean fromUI) throws Exception {
		service=VelosServiceImpl.getService();
		return service.saveOrUpdateHlas(hlaPojos,fromUI);
	}
	
	public List<CdrCbuPojo> getCordsInProgress(Long cordId) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getCordsInProgress(cordId);
	}
	
	public List<HLAPojo> getHlasByCord(Long cordId, Long entityType, String criteria) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getHlasByCord(cordId, entityType, criteria);
	}

	public CBUMinimumCriteriaPojo saveOrConfirmCriteria(CBUMinimumCriteriaPojo cbuMinimumCriteriaPojo)throws Exception {
		service=VelosServiceImpl.getService();
		return service.saveOrConfirmCriteria(cbuMinimumCriteriaPojo);
	}
	public CBUMinimumCriteriaTempPojo saveMinimumCriteriaTemp(CBUMinimumCriteriaTempPojo minimumCriteriaTempPojo)throws Exception {
		service=VelosServiceImpl.getService();
		return service.saveMinimumCriteriaTemp(minimumCriteriaTempPojo);
	}
	
	public List getPkOrders(Long cordId) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getPkOrders(cordId);
	}
	
	public IDMPojo saveIDMValues(IDMPojo idmPojo)throws Exception {
		service=VelosServiceImpl.getService();
		return service.saveIDMValues(idmPojo);
	}
	
	public List<EntityStatusReasonPojo> getReasonsByEntityStatusId(Long entityStatusId) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getReasonsByEntityStatusId(entityStatusId);
	}
	
	public CBUFinalReviewPojo saveCBUFinalReview(CBUFinalReviewPojo cbuFinalReviewPojo)throws Exception {
		service=VelosServiceImpl.getService();
		return service.saveCBUFinalReview(cbuFinalReviewPojo);
	}
	
	public EntityStatusReasonPojo loadEntityStatusReason(EntityStatusReasonPojo entityStatusReasonPojo) throws Exception{
		service=VelosServiceImpl.getService();
		return service.loadEntityStatusReason(entityStatusReasonPojo);
	}
	
	public Map<String,List> getCordUploadDocument(Long attachmentType,Long entityId,Long entityType) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getCordUploadDocument(attachmentType, entityId, entityType);
	}
	
	public List getIDMTestList(Long fkSpecId) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getTestsForIDM(fkSpecId);
	}
	
	public List getdataFromIDM(Long fkSpecId) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getdataFromIDM(fkSpecId);
	}
	
	public  Long getLabTestpkbyshname(String shname) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getLabTestpkbyshname(shname);
	}
	
	public List getpatlabpklist(Long fkSpecId) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getpatlabpklist(fkSpecId);
	}
	
	public List getIDMTests() throws Exception{
		service=VelosServiceImpl.getService();
		return service.getIDMTests();
	}
	
	public Long getIDMTestcount(Long fkSpecId) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getIDMTestcount(fkSpecId);
	}	
	
	public List getCbuStatus(String cbustatusin) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getCbuStatus(cbustatusin);
	}
	
	public EntitySamplesPojo getEntitySampleInfo(Long entityId) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getEntitySampleInfo(entityId);
	}
	
	public EntitySamplesPojo saveOrUpdateSampInfo(EntitySamplesPojo entitySamplesPojo) throws Exception{
		service=VelosServiceImpl.getService();
		return service.saveOrUpdateSampInfo(entitySamplesPojo,true);
	}
	
	public EntitySamplesPojo saveOrUpdateSampInfo(EntitySamplesPojo entitySamplesPojo,Boolean fromUI) throws Exception{
		service=VelosServiceImpl.getService();
		return service.saveOrUpdateSampInfo(entitySamplesPojo,fromUI);
	}
	
	public CBUCompleteReqInfoPojo saveCBUCompleteReqInfo(CBUCompleteReqInfoPojo cbuCompleteReqInfoPojo) throws Exception{
		service=VelosServiceImpl.getService();
		return service.saveCBUCompleteReqInfo(cbuCompleteReqInfoPojo);
	}	
	
	public List<PatLabsPojo> saveOrUpdatePatLabs(List<PatLabsPojo> patLabsPojos) throws Exception {
		//log.debug("sava or update patlabs :cbu controller .....");
		service=VelosServiceImpl.getService();
		return service.saveOrUpdatePatLabs(patLabsPojos);
	}
	
	public List<PatLabsPojo> savePatLabs(List<PatLabsPojo> patLabsPojos) throws Exception {
		service=VelosServiceImpl.getService();
		return service.savePatLabs(patLabsPojos);
	}
	public List<PatLabsPojo> getOtherTestLst(Long specimenId,Long testId) throws Exception {
		//log.debug("get other lab tests .....");
		service=VelosServiceImpl.getService();
		return service.getOtherTestLst(specimenId,testId);
	}
	public PatLabsPojo getPatLabsPojoById(Long specimenId,Long pkPatLabs) throws Exception {
		//log.debug("get other lab tests .....");
		service=VelosServiceImpl.getService();
		return service.getPatLabsPojoById(specimenId,pkPatLabs);
	}
	public List<EntityMultipleValuesPojo> getMultipleValuesByEntityId(Long entityId, String entityType,String entityMultipleType) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getMultipleValuesByEntityId(entityId, entityType,entityMultipleType);
	}
	
	public List<EntityMultipleValuesPojo> saveMultipleEntityValue(List<EntityMultipleValuesPojo> entityMultipleValuesPojos) throws Exception{
		service=VelosServiceImpl.getService();
		return service.saveMultipleEntityValue(entityMultipleValuesPojos,true);
	}
	
	public List<EntityStatusReasonPojo> saveMultipleReasonValue(List<EntityStatusReasonPojo> entityStatusReasonPojo, Boolean fromUI) throws Exception{
		service=VelosServiceImpl.getService();
		return service.saveMultipleReasonValue(entityStatusReasonPojo,fromUI);
	}
	
	public List<EntityMultipleValuesPojo> saveMultipleEntityValue(List<EntityMultipleValuesPojo> entityMultipleValuesPojos,Boolean fromUI) throws Exception{
		service=VelosServiceImpl.getService();
		return service.saveMultipleEntityValue(entityMultipleValuesPojos,fromUI);
	}
	
	public void transientMultipleValues(List<Long> ids,Long entityId) throws Exception{
		service=VelosServiceImpl.getService();
		service.transientMultipleValues(ids,entityId);
	}
	
	public void transientMultipleReasonValues(List<Long> ids,Long entityStatus) throws Exception{
		service=VelosServiceImpl.getService();
		service.transientMultipleReasonValues(ids,entityStatus);
	}
	
	public String generateGuid() throws Exception{
		service=VelosServiceImpl.getService();
		return service.generateGuid();
	}
	
	public AntigenPojo getAntigen(String genomicFormat,String locus,String typingMethod) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getAntigen(genomicFormat, locus, typingMethod);
	}
	
	public List<TempHLAPojo> getTempHla(Long entityType,String registryId) throws Exception{
    	service=VelosServiceImpl.getService();
		return service.getTempHla(entityType, registryId);
    }
	
	public List<BestHLAPojo> getBestHla(Long entityType,Long entityId) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getBestHla(entityType,entityId);
	}
	
	public AntigenPojo getLatestAntigen(Long antigenId) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getLatestAntigen(antigenId);
	}
	
	public Long getTestIdByName(String testName) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getTestIdByName(testName);
	}
	public void LCStoResolution(String pkresol,String ordertype,String pkorderId) throws Exception{
		service=VelosServiceImpl.getService();
		service.LCStoResolution(pkresol,ordertype,pkorderId);
		
	}
	
	public List<SitePojo> getMultipleValuesByErSite(Long entityId, String entityType,String entitySubType) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getMultipleValuesByErSite(entityId, entityType,entitySubType);
	}
	
    public EligibilityDeclPojo getEligibilityDeclQuestionsByEntity(Long entityId,Long entityType,Long pkEligQues) throws Exception{
    	service=VelosServiceImpl.getService();
		return service.getEligibilityDeclQuestionsByEntity(entityId, entityType,pkEligQues);
    }
	
	public DumnPojo getDumnQuestionsByEntity(Long entityId,Long entityType,Long pkEligQues) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getDumnQuestionsByEntity(entityId,entityType,pkEligQues);
	}
	
	public EligibilityDeclPojo saveOrUpdateFinalEligibilityQuestions(EligibilityDeclPojo eDeclPojo) throws Exception{
		service=VelosServiceImpl.getService();
		return service.saveOrUpdateFinalEligibilityQuestions(eDeclPojo);
	}
	
	public DumnPojo saveOrUpdateDumnQuestions(DumnPojo dumnPojo) throws Exception{
		service=VelosServiceImpl.getService();
		return service.saveOrUpdateDumnQuestions(dumnPojo);
	}
	
	public List<PatientHLAPojo> getPatientHla(Long entityType,Long entityId) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getPatientHla(entityType, entityId);
	}
	
	public List<LabTestPojo> getProcessingTestInfo(String groupType) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getProcessingTestInfo(groupType);
	}
	
	public List<LabTestPojo> getOtherProcessingTestInfo(Long specimenId ,String groupType) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getOtherProcessingTestInfo(specimenId,groupType);
	}
	public LabTestPojo getOtrProcingTstInfoByTestId(Long testId) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getOtrProcingTstInfoByTestId(testId);
	}
	
	/*public List<PatLabsPojo> getPreTestListInfo(String preGroup,Long specimenId) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getPreTestListInfo(preGroup,specimenId);
	}*/
	
	public List<PatLabsPojo> getvalueSavePreTestList(List<PatLabsPojo> alist) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getvalueSavePreTestList(alist,true);
	}
	
	public List<PatLabsPojo> getvalueSavePreTestList(List<PatLabsPojo> alist,Boolean fromUI) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getvalueSavePreTestList(alist,fromUI);
	}
	
	public List<PatLabsPojo> getvalueSavePostTestList(List<PatLabsPojo> blist) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getvalueSavePostTestList(blist,true);
	}
	
	public List<PatLabsPojo> getvalueSavePostTestList(List<PatLabsPojo> blist,Boolean fromUI) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getvalueSavePostTestList(blist,fromUI);
	}

	
	
	public List<PatLabsPojo> getPatLabsPojoInfo(Long specimenId,Long testValueType) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getPatLabsPojoInfo(specimenId,testValueType);
	}
	public List<CdrCbuPojo> getCordsByCbuStatus(Long cordId,Long cbuStatus) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCordsByCbuStatus(cordId, cbuStatus);
	}
	public List getcbuFlaggedItems(Long cordId) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getcbuFlaggedItems(cordId);
	}
	
	
	public List saveOrUpdateAssessments(List assessmentPojos) throws Exception {
		service=VelosServiceImpl.getService();
		return service.saveOrUpdateAssessments(assessmentPojos);
	}
	
	public List<PatLabsPojo> getvalueSaveOtherTestList(List<PatLabsPojo> clist) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getvalueSaveOtherTestList(clist);
	}

	public List getAttachmentByEntityId(Long entityId , Long categoryId, Long subCategoryId) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getAttachmentByEntityId(entityId , categoryId, subCategoryId);
	}
	
	public List getAttachmentByEntityId(Long entityId , Long categoryId, Long revokedFlag, int i) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getAttachmentByEntityId(entityId , categoryId, revokedFlag, i);
	}
	
	public Long getLabGrpPkByGrpType(String grpType)throws Exception{
		service=VelosServiceImpl.getService();
		return service.getLabGrpPkByGrpType(grpType);
	}
	
	public Integer getCordByCountByMaternalRegistry(String maternalRegistryId) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getCordByCountByMaternalRegistry(maternalRegistryId);
	}
	
	public void updateCordSearchable(Long cordSearchable,Long entityId) throws Exception{
		service=VelosServiceImpl.getService();
		service.updateCordSearchable(cordSearchable, entityId);
	}
	
	public void updateCordProgress(Long cordSearchable,Long entityId) throws Exception{
		service=VelosServiceImpl.getService();
		service.updateCordProgress(cordSearchable, entityId);
	}
	
	public Integer getDomainCount(String objectName,
			String criteria,String pkName) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getDomainCount(objectName, criteria, pkName);
	}
	
	public Integer saveCordListForSearch(List<Long> cordListForSearch) throws Exception{
		service=VelosServiceImpl.getService();
		return service.saveCordListForSearch(cordListForSearch);
	}
	
	public Map<Long,RecipientInfo> getRecipientInfoMap(List<Long> cordList) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getRecipientInfoMap(cordList);
	}
	
	public Long getSequence(String seqName) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getSequence(seqName);
	}
	
	public List<AntigenEncoding> getEncodingList(List<Long> pkAntigenList) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getEncodingList(pkAntigenList);
	}
	
	public String getLookUpFilterData(String filterValue) throws Exception {
		service=VelosServiceImpl.getService();
		return service.getLookUpFilterData(filterValue);
	}
	
	public List<Object> getCordListAsobject(String sql,PaginateSearch paginateSearch) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getCordListAsobject(sql, paginateSearch);
	}

	
	public List<PatLabsPojo> getTestResultByTestNames(Long specimenId,Long fkTimingOfTest,List<String> testNames) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getTestResultByTestNames(specimenId, fkTimingOfTest, testNames);
	}

	
	public Long getMC_flag(Long cordId) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getMC_flag(cordId);
	}
	
	public Long getFCR_flag(Long cordId) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getFCR_flag(cordId);
	}
	
	public Long getCBUAssessment_flag(Long cordId) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getCBUAssessment_flag(cordId);
	}

	
	public boolean getAssDetailsForLabSum(Long CordId,Long responceId,Long subEntityType,String questionNo) throws Exception{
		service=VelosServiceImpl.getService();
		return service.getAssDetailsForLabSum(CordId,responceId,subEntityType,questionNo);
	}
	
	public void deleteHla(Long entityType, Long entityId) throws Exception {
		service=VelosServiceImpl.getService();
		service.deleteHla(entityType, entityId);
		
	}

	public void deleteTempHla(Long entityType, String registryId) throws Exception {
		service=VelosServiceImpl.getService();
		service.deleteTempHla(entityType, registryId);
		
	}
	
	public EntityStatusPojo loadLatestEntityStatus(Long entityId,Long entityType,String status) throws Exception{
		service=VelosServiceImpl.getService();
		return service.loadLatestEntityStatus(entityId, entityType, status);
	}
		public EligibilityNewDeclPojo saveOrUpdateNewFinalEligibilityQuestions(EligibilityNewDeclPojo eDeclPojo) throws Exception{
		service=VelosServiceImpl.getService();
		return service.saveOrUpdateNewFinalEligibilityQuestions(eDeclPojo);
	}
	
	public EligibilityNewDeclPojo getEligibilityNewDeclQuestionsByEntity(Long entityId,Long entityType,Long pkEligQues) throws Exception{
    	service=VelosServiceImpl.getService();
		return service.getEligibilityNewDeclQuestionsByEntity(entityId, entityType,pkEligQues);
    }	
	
    public void deleteFinalReviewDocs(Long entityType,Long entityId) throws Exception{
    	service=VelosServiceImpl.getService();
		service.deleteFinalReviewDocs(entityType, entityId);
    }
	
	public List<FinalRevUploadInfoPojo> saveFinalReviewDocs(List<FinalRevUploadInfoPojo> reviewPojos,Boolean fromUI) throws Exception{
		service=VelosServiceImpl.getService();
		return service.saveFinalReviewDocs(reviewPojos, fromUI);
	}
	public List<FinalRevUploadInfo> getFinalReviewUploadDocLst(Long cordID,Long entityType) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getFinalReviewUploadDocLst(cordID,entityType);
	}

	public Long getEligTaskFlag(Long cordID, int i) throws Exception {
		service = VelosServiceImpl.getService();
		return service.getEligTaskFlag(cordID,i);
	}

	public List<String> getRaceOrderByValue(String qStr) throws Exception {
		service = VelosServiceImpl.getService();
		return service.getRaceOrderByValue(qStr);
	}
	public List<Object> updateCdrCbuObject (Object obj) throws Exception {
		service = VelosServiceImpl.getService();
		return service.updateCdrCbuObject(obj);
	}
	
	public List validateDataExistence(StringBuilder sqlQuery)throws Exception {			
		service = VelosServiceImpl.getService();
		return service.validateDataExistence(sqlQuery);		
	}
		
}