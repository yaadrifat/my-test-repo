package com.velos.ordercomponent.action;

import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.velos.ordercomponent.business.pojoobjects.CBUStatusPojo;
import com.velos.ordercomponent.business.pojoobjects.SitePojo;
import com.velos.ordercomponent.controller.CBBController;
import com.velos.ordercomponent.controller.CBUController;
import com.velos.ordercomponent.util.VelosGarudaConstants;

public class CbuReportAction extends VelosBaseAction{
	private CBBController cbbController;
	private CBUController cbuController;
	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public ServletContext getContext() {
		return context;
	}

	public void setContext(ServletContext context) {
		this.context = context;
	}
	public CbuReportAction() throws Exception
	{
		cbbController =new CBBController();
		cbuController = new CBUController();
		
	}
	 public String getSelection() throws Exception
	 {
		 List<SitePojo> cbbList = cbbController.getSites();
			getRequest().setAttribute("CbbList", cbbList);
			getRequest().setAttribute("CbbDefaultList", null);
			List<CBUStatusPojo> reasonList =  cbuController.getCbuStatus(VelosGarudaConstants.IS_LOCAL_REG);
			getRequest().setAttribute("reasonList", reasonList);
			return "success";
	 }

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	
}
