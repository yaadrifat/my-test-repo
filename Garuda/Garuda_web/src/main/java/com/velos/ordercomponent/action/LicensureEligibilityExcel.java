package com.velos.ordercomponent.action;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;

import com.opensymphony.xwork2.ActionSupport;
import com.velos.ordercomponent.util.VelosGarudaConstants;
import com.opensymphony.xwork2.ActionSupport;

public class LicensureEligibilityExcel extends ActionSupport implements VelosGarudaConstants{
	
	private Boolean isMultipleCBBSelected = false;
	
	public Workbook quickSummaryExcel(List<Object> cdrObjects, Map<Long,Long> duplCords, Workbook wb) throws ParseException{		 
		
		XSSFCellStyle headStyle = headStyle(wb);
		XSSFCellStyle hightLightStyle = redStyle(wb);
		XSSFCellStyle dateCellStyle = dateStyle(wb);
		
		 Row row;
		 Cell cell;
		 SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
		 int rowNum = 1;
		 short col = 0;	
		 int sheetNo = 1;	
		 Sheet sheet = sheet(wb, getText("garuda.querybuilder.report.licandeligibiltiy"));
		 createHeader(sheet, headStyle);		 
		
		 
		 for(Object cdrObj : cdrObjects){			
			
			 if(cdrObj!=null){					
	       	        Object[] objArr = (Object[])cdrObj;
		
			 if(rowNum  > VelosGarudaConstants.MAXROW_XLSX ){
				 
				 sheet = sheet(wb,getText("garuda.querybuilder.report.licandeligibiltiy")+" "+(++sheetNo));
				 createHeader(sheet, headStyle);
				 rowNum = 1;
			 }
			 
			  row = sheet.createRow(rowNum++);
			  col = 0;			 
				
				  
				  if(isMultipleCBBSelected){
					  row.createCell(col++).setCellValue(objArr[1]!=null?objArr[1].toString():""); //cbb id
				  }
				 
				  cell = row.createCell(col++);cell.setCellValue(objArr[2]!=null?objArr[2].toString():"");cell.setCellStyle((duplCords !=null && duplCords.get(Long.valueOf(objArr[0].toString())) !=null)?hightLightStyle:null);// registryid
				   row.createCell(col++).setCellValue(objArr[3]!=null?objArr[3].toString():"");// cbu local id
				  row.createCell(col++).setCellValue(objArr[4]!=null?objArr[4].toString():"");// Unique product identity on bag
				  row.createCell(col++).setCellValue(objArr[5]!=null?objArr[5].toString():"");// ISBT
				  row.createCell(col++).setCellValue(objArr[6]!=null?objArr[6].toString():"");
				  row.createCell(col++).setCellValue(objArr[7]!=null?objArr[7].toString():"");
				  row.createCell(col++).setCellValue(objArr[8]!=null?objArr[8].toString():"");
				  row.createCell(col++).setCellValue(objArr[9]!=null?objArr[9].toString():"");
				  row.createCell(col++).setCellValue(objArr[10]!=null?objArr[10].toString():"");				
				  row.createCell(col++).setCellValue(objArr[11]!=null?objArr[11].toString():getText("garuda.queryBuilder.label.eligibilityNotDetermined"));
				  row.createCell(col++).setCellValue(objArr[12]!=null && (objArr[11]!=null && objArr[11].toString().equals(INELIGIBLE_STR))?objArr[12].toString():"");
				  row.createCell(col++).setCellValue(objArr[12]!=null && (objArr[11]!=null && objArr[11].toString().equals(INCOMPLETE_STR))?objArr[12].toString():"");
				  				  
				  cell = row.createCell(col++);
				  if(objArr[13]!=null){
	        			cell.setCellStyle(dateCellStyle);    	        		        		
    	        		cell.setCellValue(format.parse(objArr[13].toString()));
	        		}			  
				  row.createCell(col++).setCellValue(objArr[14]!=null?objArr[14].toString():"");
				  row.createCell(col++).setCellValue(objArr[15]!=null?objArr[15].toString():"");
				  row.createCell(col++).setCellValue(objArr[16]!=null?objArr[16].toString():"");
				  row.createCell(col++).setCellValue(objArr[17]!=null?objArr[17].toString():"");
				  row.createCell(col++).setCellValue(objArr[18]!=null?objArr[18].toString():"");
				  row.createCell(col++).setCellValue(objArr[19]!=null?objArr[19].toString():"");
				  row.createCell(col++).setCellValue(objArr[20]!=null?objArr[20].toString():"");
				  row.createCell(col++).setCellValue(objArr[21]!=null?objArr[21].toString():"");
				  row.createCell(col++).setCellValue(objArr[22]!=null?objArr[22].toString():"");
				  row.createCell(col++).setCellValue(objArr[23]!=null?objArr[23].toString():"");
				  row.createCell(col++).setCellValue(objArr[24]!=null?objArr[24].toString():"");
				  row.createCell(col++).setCellValue(objArr[25]!=null?objArr[25].toString():"");
				  row.createCell(col++).setCellValue(objArr[26]!=null?objArr[26].toString():"");
				  row.createCell(col++).setCellValue(objArr[27]!=null?objArr[27].toString():"");
				  				 
				  cell = row.createCell(col++);
				  if(objArr[28]!=null && objArr[30]!=null && objArr[30].toString().trim().equals(ORDER_TYPE_CT_SHIP)){
	        			cell.setCellStyle(dateCellStyle);    	        		        		
    	        		cell.setCellValue(format.parse(objArr[28].toString()));
	        		}				 
				  
				  cell = row.createCell(col++);
				  if(objArr[29]!=null && objArr[30]!=null && objArr[30].toString().trim().equals(OR)){
	        			cell.setCellStyle(dateCellStyle);    	        		        		
    	        		cell.setCellValue(format.parse(objArr[29].toString()));
	        		}
				  
				  row.createCell(col++).setCellValue(objArr[30]!=null?objArr[30].toString():"");
				  				  				 
				  cell = row.createCell(col++);
				  if(objArr[31]!=null){
	        			cell.setCellStyle(dateCellStyle);    	        		        		
    	        		cell.setCellValue(format.parse(objArr[31].toString()));
	        		}		 
			   }
		 }
		 
		 return wb;
	}
	
	private Sheet sheet(Workbook wb, String sheetName){
		
		 Sheet sheet = wb.createSheet(sheetName); 
		 
		 return sheet;
	}
	
	private int createHeader(Sheet sheet, XSSFCellStyle blackStyle){
		
		 int r = 0, column = 0;
		 Cell cell;
		 Row row;
		 row = sheet.createRow(r);    
		 row.setHeight((short)500);  
		 
		 if(isMultipleCBBSelected){
			 cell=row.createCell(column++);cell.setCellValue(getText("garuda.queryBuilder.label.cbbid"));cell.setCellStyle(blackStyle);
		 }		
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.cbuentry.label.registrycbuid"));cell.setCellStyle(blackStyle);              
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.cbuentry.label.localcbuid"));cell.setCellStyle(blackStyle);                   
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.cbuentry.label.idbag"));cell.setCellStyle(blackStyle); 
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.queryBuilder.label.cbucollectiondate"));cell.setCellStyle(blackStyle);                           
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.queryBuilder.nmdpregistrationDates"));cell.setCellStyle(blackStyle);		  
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.cbuentry.label.cbu_status"));cell.setCellStyle(blackStyle);           
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.clinicalnote.label.reason"));cell.setCellStyle(blackStyle);	
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.cdrcbuview.label.licensure_status"));cell.setCellStyle(blackStyle);
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.evaluHistory.report.unlicensedReason"));cell.setCellStyle(blackStyle);
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.cdrcbuview.label.eligibility_determination"));cell.setCellStyle(blackStyle);
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.queryBuilder.ineligiblereason"));cell.setCellStyle(blackStyle);
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.queryBuilder.incompletereason"));cell.setCellStyle(blackStyle);
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.queryBuilder.babyBirthDate"));cell.setCellStyle(blackStyle);
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.queryBuilder.race"));cell.setCellStyle(blackStyle);
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.cordentry.label.nmdpbroadrace"));cell.setCellStyle(blackStyle);
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.pendingorderpage.label.funded"));cell.setCellStyle(blackStyle);
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.queryBuilder.fundingcategory"));cell.setCellStyle(blackStyle);
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.cbuentry.label.bcresult"));cell.setCellStyle(blackStyle);
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.cbuentry.label.fcresult"));cell.setCellStyle(blackStyle);
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.minimumcriteria.label.hemoglobin"));cell.setCellStyle(blackStyle);
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.cbbprocedures.label.processingprocedure"));cell.setCellStyle(blackStyle);
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.queryBuilder.label.totalcbunucleatedcount"));cell.setCellStyle(blackStyle);
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.queryBuilder.label.totalcbunucleatedcountun"));cell.setCellStyle(blackStyle);
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.queryBuilder.totalcd34"));cell.setCellStyle(blackStyle);
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.queryBuilder.viabilitycount"));cell.setCellStyle(blackStyle);
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.common.lable.viability"));cell.setCellStyle(blackStyle);
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.productinsert.label.finalProdVol"));cell.setCellStyle(blackStyle);
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.idsReport.ctshipdate"));cell.setCellStyle(blackStyle);
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.idsReport.orshipdate"));cell.setCellStyle(blackStyle);
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.detailreport.cbuhistory.requesttype"));cell.setCellStyle(blackStyle);
		   cell=row.createCell(column++);cell.setCellValue(getText("garuda.idsReport.reqdate"));cell.setCellStyle(blackStyle);	
			 
		
		
		
		return column;
	}

private XSSFCellStyle dateStyle(Workbook wb){
		
		XSSFCellStyle style = (XSSFCellStyle) wb.createCellStyle();
		CreationHelper createHelper = wb.getCreationHelper();
		
		style.setDataFormat(
			        createHelper.createDataFormat().getFormat("mm/dd/yyyy"));
	    
	    return style;
	}
	
private XSSFCellStyle headStyle(Workbook wb){
		
		XSSFCellStyle style = (XSSFCellStyle) wb.createCellStyle();

	    style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
	    style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    style.setWrapText(false);
	    
	    Font font = wb.createFont();
	    font.setColor(IndexedColors.WHITE.getIndex());
	    font.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    style.setFont(font);
	   // Row.setHeight((short)500);
	    
	    return style;
	}


private XSSFCellStyle redStyle(Workbook wb){
	
	XSSFCellStyle style = (XSSFCellStyle) wb.createCellStyle();   
    Font font = wb.createFont();  
    style.setFillForegroundColor(IndexedColors.RED.getIndex());
    style.setFillPattern(CellStyle.SOLID_FOREGROUND);
    style.setWrapText(false);
    style.setFont(font);
    
    return style;
}
	public void setIsMultipleCBBSelected(Boolean isMultipleCBBSelected) {
		this.isMultipleCBBSelected = isMultipleCBBSelected;
	}

}
