package com.velos.ordercomponent.validator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.CompoundRoot;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

/**
 * @author Mohiuddin Ali Ahmed
 * @author Anurag Upadhyay
 * @version 1.0
 * 
 */

public class DateRangeFieldValidation extends FieldValidatorSupport{
      private String startDateFieldName;
      private String terminDateFieldName;
      private String todayDate;
      private String min;
      private String max;
      
	public String getStartDateFieldName() {
		return startDateFieldName;
	}
	
	public void setStartDateFieldName(String startDateFieldName) {
		this.startDateFieldName = startDateFieldName;
	}

	public String getTerminDateFieldName() {
		return terminDateFieldName;
	}

	public void setTerminDateFieldName(String terminDateFieldName) {
		this.terminDateFieldName = terminDateFieldName;
	}

	public String getTodayDate() {
		return todayDate;
	}

	public void setTodayDate(String todayDate) {
		this.todayDate = todayDate;
	}

	public String getMin() {
		return min;
	}

	public void setMin(String min) {
		this.min = min;
	}

	public String getMax() {
		return max;
	}

	public void setMax(String max) {
		this.max = max;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void validate(Object object) throws ValidationException {
		// TODO Auto-generated method stub
		
			String fieldName=getFieldName();
			Object obj=getFieldValue(fieldName, object);
			Date invokedDateValue= obj==null?null:(Date)obj;
			invokedDateValue= (invokedDateValue!=null && invokedDateValue.equals(new Date(1970)))?null:invokedDateValue;		
		    CompoundRoot cRoot= ActionContext.getContext().getValueStack().getRoot();
		    DateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
		    
		    Date startDate=(Date) getFieldValue(startDateFieldName, cRoot);//collection date also
		    Date terminDate=(Date) getFieldValue(terminDateFieldName, cRoot);// Today date ,Processing Start Date also,
		    
		    
		    setMin(((startDate!=null)?dateFormat.format(startDate):""));
		    setMax(((terminDate!=null)?dateFormat.format(terminDate):""));
		    
		    Comparable  procStartComp = (Comparable)startDate;//collection Date
		    Comparable  procTerminComp = (Comparable)terminDate;// Processing Start Date
		    Comparable  invokedDateComp = (Comparable)invokedDateValue;
		    if(fieldName.equals("postTestDateStrn") || fieldName.equals("otherTestDateStrn1")){
		    	Date todayDt=(Date)getFieldValue(todayDate, cRoot);                          // collection date                                                   // processing date
		    	if(procStartComp!=null && invokedDateComp!=null && (invokedDateComp.compareTo(procStartComp)<0 ||(procTerminComp!=null && invokedDateComp.compareTo(procTerminComp)<0 ) ||(invokedDateComp.compareTo(todayDt)>0))){
		    		 addFieldError(getFieldName(), object);
			         return;		    		
		    	}		    	
		    }else if(procStartComp!=null && invokedDateComp!=null && (invokedDateComp.compareTo(procStartComp)< 0 ||(procTerminComp!=null && invokedDateComp.compareTo(procTerminComp)>0))){
		    	 addFieldError(getFieldName(), object);
		         return;
		    }
		
	}

}
