package com.velos.ordercomponent.action;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;



import javax.print.DocFlavor.STRING;
import javax.servlet.http.HttpServletResponse;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.PageSize;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Section;
import com.lowagie.text.html.WebColors;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.html.simpleparser.StyleSheet;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfGState;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import com.lowagie.text.pdf.PdfWriter;
import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.user.UserJB;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.velos.ordercomponent.business.domain.CBUStatus;
import com.velos.ordercomponent.business.domain.CdrCbu;
import com.velos.ordercomponent.business.domain.CodeList;
import com.velos.ordercomponent.business.domain.RecipientInfo;
import com.velos.ordercomponent.business.domain.Specimen;
import com.velos.ordercomponent.business.pojoobjects.AdditionalIdsPojo;
import com.velos.ordercomponent.business.pojoobjects.BestHLAPojo;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.ClinicalNotePojo;
import com.velos.ordercomponent.business.pojoobjects.CordFilterPojo;
import com.velos.ordercomponent.business.pojoobjects.DumnPojo;
import com.velos.ordercomponent.business.pojoobjects.EligibilityDeclPojo;
import com.velos.ordercomponent.business.pojoobjects.EligibilityNewDeclPojo;
import com.velos.ordercomponent.business.pojoobjects.EntitySamplesPojo;
import com.velos.ordercomponent.business.pojoobjects.FilterDataPojo;
import com.velos.ordercomponent.business.pojoobjects.HLAFilterPojo;
import com.velos.ordercomponent.business.pojoobjects.HLAPojo;
import com.velos.ordercomponent.business.pojoobjects.LabTestPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.pojoobjects.PatLabsPojo;
import com.velos.ordercomponent.business.pojoobjects.SitePojo;
import com.velos.ordercomponent.business.pojoobjects.TempHLAPojo;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.Utilities;
import com.velos.ordercomponent.business.util.VelosUtil;
import com.velos.ordercomponent.controller.CBBController;
import com.velos.ordercomponent.controller.CBUController;
import com.velos.ordercomponent.controller.ClinicalNoteController;
import com.velos.ordercomponent.controller.DynaFormController;
import com.velos.ordercomponent.controller.LabTestcontroller;
import com.velos.ordercomponent.controller.OpenOrderController;
import com.velos.ordercomponent.controller.SpecimenController;
import com.velos.ordercomponent.controller.VDAController;
import com.velos.ordercomponent.util.GarudaConfig;
import com.velos.ordercomponent.queryObject.LicEligCDRObj;
import com.velos.ordercomponent.queryObject.LicEligFilterCordObj;
import com.velos.ordercomponent.queryObject.LicEligFilterReasonObj;
import com.velos.ordercomponent.queryObject.LicEligMapObj;
import com.velos.ordercomponent.queryObject.LicEligRaceObj;
import com.velos.ordercomponent.util.ObjectFactory;
import com.velos.ordercomponent.util.VelosGarudaConstants;
import com.velos.ordercomponent.business.pojoobjects.FinalRevUploadInfoPojo;

/**
 * @author Mohiuddin Ali Ahmed
 * @version 1.0
 * 
 */
public class CordEntryAction extends VelosBaseAction {

	
	private static final long serialVersionUID = 1L;

	DateFormat dat=new SimpleDateFormat("MMM dd, yyyy");
	
	private String openMode;	
	private CdrCbuPojo cdrCbuPojo;
	private String cbuCollectionDateStr;
	private Long[] cbuOnBag;
	private Long[] race;
	private List<AdditionalIdsPojo> additionalIds;
	CBUController cbuController = ((CBUController)ObjectFactory.MakeObjects.getFactoryObject(CBUController.class.getName()));
	SpecimenController specimenController = (SpecimenController)ObjectFactory.MakeObjects.getFactoryObject(SpecimenController.class.getName());
	CBBController cbbController = (CBBController)ObjectFactory.MakeObjects.getFactoryObject(CBBController.class.getName());
	OpenOrderController linkController = (OpenOrderController)ObjectFactory.MakeObjects.getFactoryObject(OpenOrderController.class.getName());
	ClinicalNoteController clinicalNoteController = (ClinicalNoteController)ObjectFactory.MakeObjects.getFactoryObject(ClinicalNoteController.class.getName());
	VDAController vdaController = (VDAController)ObjectFactory.MakeObjects.getFactoryObject(VDAController.class.getName());
	private Long[] reasons;
	private Long[] inEligibleReasons;
	private Long[] inCompReasons;
	private Long[] licenseReasons;
	private List cbuStrLocationList;
	private List cbuCollectionList;
	private Long cbuLicStatus;
	private Long fkCordCbuUnlicenseReason;
    private Long fkCordCbuEligible;
    private Long fkCordCbuEligibleReason;
    private PaginateSearch paginateSearch;
    private CBUAction action = null;
    private String cordAdditionalInfo;
    private EntitySamplesPojo entitySamplesPojo;
    private ClinicalNotePojo clinicalNotePojo;
    private List mrqformversionlst= new ArrayList();
	private List fmhqformversionlst= new ArrayList();
	private List idmformversionlst= new ArrayList();
	private String mrqversion;
	private String fmhqversion;
	private String idmversion;
	private Integer formDataToBeLoaded;
	private Map<Long,List<HLAPojo>> cbuHlaMap;
	private Map<Long,List<HLAPojo>> matHlaMap;
	private Map<Long,List<HLAPojo>> patHlaMap;
	private List<HLAPojo> editHlas;
	private List<HLAPojo> maternalHlas ;
	private List<HLAPojo> editMaternalHlas;
	private Boolean isCordEntryPage=false;
	
	/*****************Lab data***************/
	private List<PatLabsPojo> otherPostTestList;
	private List<PatLabsPojo> otherPostTestListDbStored;
    private List<PatLabsPojo> saveOtherPostTestList1;
    private PatLabsPojo otherPatLabsPojo;
 
    private List<PatLabsPojo> otherPostTestListFirst;
    private List<PatLabsPojo> otherPostTestListFirstDbStored;
    private List<PatLabsPojo> saveOtherPostTestListFirst;
    private List<PatLabsPojo> saveOtherPostTestList2;
    
  
    private List<PatLabsPojo> otherPostTestListSecond;
    private List<PatLabsPojo> otherPostTestListSecondDbStroed;
    private List<PatLabsPojo> saveOtherPostTestListSecond;
    private List<PatLabsPojo> saveOtherPostTestList3;
    
    
    private List<PatLabsPojo> otherPostTestListThird;
    private List<PatLabsPojo> otherPostTestListThirdDbStored;
    private List<PatLabsPojo> saveOtherPostTestListThird;
    private List<PatLabsPojo> saveOtherPostTestList4;
     
    private List<PatLabsPojo> otherPostTestListFourth;
    private List<PatLabsPojo> otherPostTestListFourthDbStored;
    private List<PatLabsPojo> saveOtherPostTestListFourth;
    private List<PatLabsPojo> saveOtherPostTestList5;
     
    private List<PatLabsPojo> otherPostTestListFifth;
    private List<PatLabsPojo> otherPostTestListFifthDbStored;
    private List<PatLabsPojo> saveOtherPostTestListFifth;
    private List<PatLabsPojo> saveOtherPostTestList6;
    private String otherTestDateStrn1;
    private String otherTestDateStrn2;
    private String otherTestDateStrn3;
    private String otherTestDateStrn4;
    private String otherTestDateStrn5;
    private String otherTestDateStrn6;
    
    private PatLabsPojo otherPatLabsPojoFirst;
    private PatLabsPojo otherPatLabsPojoSecond;
    private PatLabsPojo otherPatLabsPojoThird;
    private PatLabsPojo otherPatLabsPojoFourth;
    private PatLabsPojo otherPatLabsPojoFive;
    private List<PatLabsPojo> preTestList;
	private List<PatLabsPojo> postTestList;
	private List<PatLabsPojo> othTestList;
	private Long pkCfuOther;
	private Long pkViabilityOther;
	private String otherLabTstsLstId;
	private List<PatLabsPojo> patLabs;
	private List<LabTestPojo> processingTest;
	private PatLabsPojo prePatLabsPojo;
	private PatLabsPojo postPatLabsPojo;
	private int labGroupTypTests;
	private Long hemoTraitPkVal;
	private Long hemozygousNoDiesasePkVal;
	private Long hemoAlphaThalassmiaPkVal;
	private Long alphaThalismiaTraitPkVal;
	private Long hemozygousNoDonePkVal;
	private Long hemozygousUnknownPkVal;
	private Long fungalCulpostive;
	private Long fungalCulnotdone;
	private Long subentitytype;
	private List<ClinicalNotePojo> clinicalNotelist;
	private String reportType;
	List<CBUStatus> defferedReason;
	private Integer cbuCount;
	//private List<FilterDataPojo> customFilter;
	private List cordInfoList;
	private Map<Long,Map<Long,List<HLAPojo>>> hlas;
	private Map<Long,List<BestHLAPojo>> bestHlasMap;
	private EligibilityDeclPojo declPojo;
	private Boolean isMultipleCBBSelected;
    private Boolean isWorkflowActivitySelected;
    private List<BestHLAPojo> bestHlas;
    private EligibilityNewDeclPojo declNewPojo;
	private Long[] finalReviewDocs; 
    private Boolean finReviewAssoDocsFlag=false;
    private Integer hlaPercentage;
    private Integer hlaMadatoryField;
    private Integer hlaTotaFiledCount;
     private List<FinalRevUploadInfoPojo> finRevUploadInfo;
 	private Boolean unlicensed_flag;
	private Boolean inelig_incomple_flag;
	List<Object> cdrObjects = new ArrayList<Object>();
    private Long selectedSiteId;
    private String[] cbbdrpdwn;
	private Boolean qinelig_flag;
	private Boolean qincomp_flag;
	private List<CordFilterPojo> cdrObj;
	private Map<Long, HLAFilterPojo> cbuBestHlas;
	private Map<Long, Map<Long, HLAFilterPojo>> cbuHlas;
	private Map<Long, String> cdlst;
	private List<Object> cdrObjs;  
	
	
	public Boolean getQinelig_flag() {
		return qinelig_flag;
	}
	public Boolean getQincomp_flag() {
		return qincomp_flag;
	}
	public void setQinelig_flag(Boolean qinelig_flag) {
		this.qinelig_flag = qinelig_flag;
	}
	public void setQincomp_flag(Boolean qincomp_flag) {
		this.qincomp_flag = qincomp_flag;
	}
	public String[] getCbbdrpdwn() {
		return cbbdrpdwn;
	}
	public void setCbbdrpdwn(String[] cbbdrpdwn) {
		this.cbbdrpdwn = cbbdrpdwn;
	}
	public Long getSelectedSiteId() {
		return selectedSiteId;
	}
	public void setSelectedSiteId(Long selectedSiteId) {
		this.selectedSiteId = selectedSiteId;
	}
    
    public List<Object> getCdrObjects() {
		return cdrObjects;
	}
	public void setCdrObjects(List<Object> cdrObjects) {
		this.cdrObjects = cdrObjects;
	}
	public Boolean getUnlicensed_flag() {
		return unlicensed_flag;
	}
	public Boolean getInelig_incomple_flag() {
		return inelig_incomple_flag;
	}
	public void setUnlicensed_flag(Boolean unlicensed_flag) {
		this.unlicensed_flag = unlicensed_flag;
	}
	public void setInelig_incomple_flag(Boolean inelig_incomple_flag) {
		this.inelig_incomple_flag = inelig_incomple_flag;
	}
	public List<FinalRevUploadInfoPojo> getFinRevUploadInfo() {
		return finRevUploadInfo;
	}
	public void setFinRevUploadInfo(List<FinalRevUploadInfoPojo> finRevUploadInfo) {
		this.finRevUploadInfo = finRevUploadInfo;
    }    
	public Integer getHlaTotaFiledCount() {
		return hlaTotaFiledCount;
	}
	public void setHlaTotaFiledCount(Integer hlaTotaFiledCount) {
		this.hlaTotaFiledCount = hlaTotaFiledCount;
	}
	public Integer getHlaMadatoryField() {
		return hlaMadatoryField;
	}
	public void setHlaMadatoryField(Integer hlaMadatoryField) {
		this.hlaMadatoryField = hlaMadatoryField;
	}
	public Integer getHlaPercentage() {
		return hlaPercentage;
	}
	public void setHlaPercentage(Integer hlaPercentage) {
		this.hlaPercentage = hlaPercentage;
	}
	public Boolean getFinReviewAssoDocsFlag() {
		return finReviewAssoDocsFlag;
	}
	public void setFinReviewAssoDocsFlag(Boolean finReviewAssoDocsFlag) {
		this.finReviewAssoDocsFlag = finReviewAssoDocsFlag;
	}
	public Long[] getFinalReviewDocs() {
		return finalReviewDocs;
	}
	public void setFinalReviewDocs(Long[] finalReviewDocs) {
		this.finalReviewDocs = finalReviewDocs;
	}	
	    
	public EligibilityNewDeclPojo getDeclNewPojo() {
		return declNewPojo;
	}
	public void setDeclNewPojo(EligibilityNewDeclPojo declNewPojo) {
		this.declNewPojo = declNewPojo;
	}
	public void setBestHlas(List<BestHLAPojo> bestHlas) {
		this.bestHlas = bestHlas;
	}
	
	public Boolean getIsMultipleCBBSelected() {
		return isMultipleCBBSelected;
	}
	public void setIsMultipleCBBSelected(Boolean isMultipleCBBSelected) {
		this.isMultipleCBBSelected = isMultipleCBBSelected;
	}
	public Boolean getIsWorkflowActivitySelected() {
		return isWorkflowActivitySelected;
	}
	public void setIsWorkflowActivitySelected(Boolean isWorkflowActivitySelected) {
		this.isWorkflowActivitySelected = isWorkflowActivitySelected;
	}
	public EligibilityDeclPojo getDeclPojo() {
		return declPojo;
	}
	public void setDeclPojo(EligibilityDeclPojo declPojo) {
		this.declPojo = declPojo;
	}
	public Map<Long, Map<Long,List<HLAPojo>>> getHlas() {
		return hlas;
	}
	public void setHlas(Map<Long, Map<Long,List<HLAPojo>>> hlas) {
		this.hlas = hlas;
	}
	
	public Map<Long, List<BestHLAPojo>> getBestHlasMap() {
		return bestHlasMap;
	}
	public void setBestHlasMap(Map<Long, List<BestHLAPojo>> bestHlasMap) {
		this.bestHlasMap = bestHlasMap;
	}
	public Integer getCbuCount() {
		return cbuCount;
	}
	public void setCbuCount(Integer cbuCount) {
		this.cbuCount = cbuCount;
	}
	public List getCordInfoList() {
		return cordInfoList;
	}
	public void setCordInfoList(List cordInfoList) {
		this.cordInfoList = cordInfoList;
	}
	/*public List<FilterDataPojo> getCustomFilter() {
		return customFilter;
	}
	public void setCustomFilter(List<FilterDataPojo> customFilter) {
		this.customFilter = customFilter;
	}*/
	public List<CBUStatus> getDefferedReason() {
		return defferedReason;
	}
	public void setDefferedReason(List<CBUStatus> defferedReason) {
		this.defferedReason = defferedReason;
	}
	public String getReportType() {
		return reportType;
	}
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}
	public List<ClinicalNotePojo> getClinicalNotelist() {
		return clinicalNotelist;
	}
	public void setClinicalNotelist(List<ClinicalNotePojo> clinicalNotelist) {
		this.clinicalNotelist = clinicalNotelist;
	}
	public Long getSubentitytype() {
		return subentitytype;
	}
	public void setSubentitytype(Long subentitytype) {
		this.subentitytype = subentitytype;
	}
	public Long getAlphaThalismiaTraitPkVal() {
		return alphaThalismiaTraitPkVal;
	}
	public void setAlphaThalismiaTraitPkVal(Long alphaThalismiaTraitPkVal) {
		this.alphaThalismiaTraitPkVal = alphaThalismiaTraitPkVal;
	}
	public Long getHemoTraitPkVal() {
		return hemoTraitPkVal;
	}
	public void setHemoTraitPkVal(Long hemoTraitPkVal) {
		this.hemoTraitPkVal = hemoTraitPkVal;
	}
	public Long getHemozygousNoDiesasePkVal() {
		return hemozygousNoDiesasePkVal;
	}
	public void setHemozygousNoDiesasePkVal(Long hemozygousNoDiesasePkVal) {
		this.hemozygousNoDiesasePkVal = hemozygousNoDiesasePkVal;
	}
	public Long getHemoAlphaThalassmiaPkVal() {
		return hemoAlphaThalassmiaPkVal;
	}
	public void setHemoAlphaThalassmiaPkVal(Long hemoAlphaThalassmiaPkVal) {
		this.hemoAlphaThalassmiaPkVal = hemoAlphaThalassmiaPkVal;
	}
	public Long getHemozygousNoDonePkVal() {
		return hemozygousNoDonePkVal;
	}
	public void setHemozygousNoDonePkVal(Long hemozygousNoDonePkVal) {
		this.hemozygousNoDonePkVal = hemozygousNoDonePkVal;
	}
	public Long getHemozygousUnknownPkVal() {
		return hemozygousUnknownPkVal;
	}
	public void setHemozygousUnknownPkVal(Long hemozygousUnknownPkVal) {
		this.hemozygousUnknownPkVal = hemozygousUnknownPkVal;
	}
	public Long getFungalCulpostive() {
		return fungalCulpostive;
	}
	public void setFungalCulpostive(Long fungalCulpostive) {
		this.fungalCulpostive = fungalCulpostive;
	}
	public Long getFungalCulnotdone() {
		return fungalCulnotdone;
	}
	public void setFungalCulnotdone(Long fungalCulnotdone) {
		this.fungalCulnotdone = fungalCulnotdone;
	}
	public int getLabGroupTypTests() {
		return labGroupTypTests;
	}
	public void setLabGroupTypTests(int labGroupTypTests) {
		this.labGroupTypTests = labGroupTypTests;
	}
	public PatLabsPojo getPrePatLabsPojo() {
		return prePatLabsPojo;
	}
	public void setPrePatLabsPojo(PatLabsPojo prePatLabsPojo) {
		this.prePatLabsPojo = prePatLabsPojo;
	}
	public PatLabsPojo getPostPatLabsPojo() {
		return postPatLabsPojo;
	}
	public void setPostPatLabsPojo(PatLabsPojo postPatLabsPojo) {
		this.postPatLabsPojo = postPatLabsPojo;
	}
	public List<LabTestPojo> getProcessingTest() {
		return processingTest;
	}
	public void setProcessingTest(List<LabTestPojo> processingTest) {
		this.processingTest = processingTest;
	}
	public List<PatLabsPojo> getPatLabs() {
		return patLabs;
	}
	public void setPatLabs(List<PatLabsPojo> patLabs) {
		this.patLabs = patLabs;
	}
	public String getOtherLabTstsLstId() {
		return otherLabTstsLstId;
	}
	public void setOtherLabTstsLstId(String otherLabTstsLstId) {
		this.otherLabTstsLstId = otherLabTstsLstId;
	}
	public Long getPkCfuOther() {
		return pkCfuOther;
	}
	public void setPkCfuOther(Long pkCfuOther) {
		this.pkCfuOther = pkCfuOther;
	}
	public Long getPkViabilityOther() {
		return pkViabilityOther;
	}
	public void setPkViabilityOther(Long pkViabilityOther) {
		this.pkViabilityOther = pkViabilityOther;
	}
	public List<PatLabsPojo> getPreTestList() {
		return preTestList;
	}
	public void setPreTestList(List<PatLabsPojo> preTestList) {
		this.preTestList = preTestList;
	}
	public List<PatLabsPojo> getPostTestList() {
		return postTestList;
	}
	public void setPostTestList(List<PatLabsPojo> postTestList) {
		this.postTestList = postTestList;
	}
	public List<PatLabsPojo> getOthTestList() {
		return othTestList;
	}
	public void setOthTestList(List<PatLabsPojo> othTestList) {
		this.othTestList = othTestList;
	}
	public List<PatLabsPojo> getOtherPostTestList() {
		return otherPostTestList;
	}
	public void setOtherPostTestList(List<PatLabsPojo> otherPostTestList) {
		this.otherPostTestList = otherPostTestList;
	}
	public List<PatLabsPojo> getOtherPostTestListDbStored() {
		return otherPostTestListDbStored;
	}
	public void setOtherPostTestListDbStored(
			List<PatLabsPojo> otherPostTestListDbStored) {
		this.otherPostTestListDbStored = otherPostTestListDbStored;
	}
	public PatLabsPojo getOtherPatLabsPojo() {
		return otherPatLabsPojo;
	}
	public void setOtherPatLabsPojo(PatLabsPojo otherPatLabsPojo) {
		this.otherPatLabsPojo = otherPatLabsPojo;
	}
	public List<PatLabsPojo> getOtherPostTestListFirst() {
		return otherPostTestListFirst;
	}
	public void setOtherPostTestListFirst(List<PatLabsPojo> otherPostTestListFirst) {
		this.otherPostTestListFirst = otherPostTestListFirst;
	}
	public List<PatLabsPojo> getOtherPostTestListFirstDbStored() {
		return otherPostTestListFirstDbStored;
	}
	public void setOtherPostTestListFirstDbStored(
			List<PatLabsPojo> otherPostTestListFirstDbStored) {
		this.otherPostTestListFirstDbStored = otherPostTestListFirstDbStored;
	}
	public List<PatLabsPojo> getOtherPostTestListSecond() {
		return otherPostTestListSecond;
	}
	public void setOtherPostTestListSecond(List<PatLabsPojo> otherPostTestListSecond) {
		this.otherPostTestListSecond = otherPostTestListSecond;
	}
	public List<PatLabsPojo> getOtherPostTestListSecondDbStroed() {
		return otherPostTestListSecondDbStroed;
	}
	public void setOtherPostTestListSecondDbStroed(
			List<PatLabsPojo> otherPostTestListSecondDbStroed) {
		this.otherPostTestListSecondDbStroed = otherPostTestListSecondDbStroed;
	}
	public List<PatLabsPojo> getOtherPostTestListThird() {
		return otherPostTestListThird;
	}
	public void setOtherPostTestListThird(List<PatLabsPojo> otherPostTestListThird) {
		this.otherPostTestListThird = otherPostTestListThird;
	}
	public List<PatLabsPojo> getOtherPostTestListThirdDbStored() {
		return otherPostTestListThirdDbStored;
	}
	public void setOtherPostTestListThirdDbStored(
			List<PatLabsPojo> otherPostTestListThirdDbStored) {
		this.otherPostTestListThirdDbStored = otherPostTestListThirdDbStored;
	}
	public List<PatLabsPojo> getOtherPostTestListFourth() {
		return otherPostTestListFourth;
	}
	public void setOtherPostTestListFourth(List<PatLabsPojo> otherPostTestListFourth) {
		this.otherPostTestListFourth = otherPostTestListFourth;
	}
	public List<PatLabsPojo> getOtherPostTestListFourthDbStored() {
		return otherPostTestListFourthDbStored;
	}
	public void setOtherPostTestListFourthDbStored(
			List<PatLabsPojo> otherPostTestListFourthDbStored) {
		this.otherPostTestListFourthDbStored = otherPostTestListFourthDbStored;
	}
	public List<PatLabsPojo> getOtherPostTestListFifth() {
		return otherPostTestListFifth;
	}
	public void setOtherPostTestListFifth(List<PatLabsPojo> otherPostTestListFifth) {
		this.otherPostTestListFifth = otherPostTestListFifth;
	}
	public List<PatLabsPojo> getOtherPostTestListFifthDbStored() {
		return otherPostTestListFifthDbStored;
	}
	public void setOtherPostTestListFifthDbStored(
			List<PatLabsPojo> otherPostTestListFifthDbStored) {
		this.otherPostTestListFifthDbStored = otherPostTestListFifthDbStored;
	}
	public String getOtherTestDateStrn1() {
		return otherTestDateStrn1;
	}
	public void setOtherTestDateStrn1(String otherTestDateStrn1) {
		this.otherTestDateStrn1 = otherTestDateStrn1;
	}
	public String getOtherTestDateStrn2() {
		return otherTestDateStrn2;
	}
	public void setOtherTestDateStrn2(String otherTestDateStrn2) {
		this.otherTestDateStrn2 = otherTestDateStrn2;
	}
	public String getOtherTestDateStrn3() {
		return otherTestDateStrn3;
	}
	public void setOtherTestDateStrn3(String otherTestDateStrn3) {
		this.otherTestDateStrn3 = otherTestDateStrn3;
	}
	public String getOtherTestDateStrn4() {
		return otherTestDateStrn4;
	}
	public void setOtherTestDateStrn4(String otherTestDateStrn4) {
		this.otherTestDateStrn4 = otherTestDateStrn4;
	}
	public String getOtherTestDateStrn5() {
		return otherTestDateStrn5;
	}
	public void setOtherTestDateStrn5(String otherTestDateStrn5) {
		this.otherTestDateStrn5 = otherTestDateStrn5;
	}
	public String getOtherTestDateStrn6() {
		return otherTestDateStrn6;
	}
	public void setOtherTestDateStrn6(String otherTestDateStrn6) {
		this.otherTestDateStrn6 = otherTestDateStrn6;
	}
	public PatLabsPojo getOtherPatLabsPojoFirst() {
		return otherPatLabsPojoFirst;
	}
	public void setOtherPatLabsPojoFirst(PatLabsPojo otherPatLabsPojoFirst) {
		this.otherPatLabsPojoFirst = otherPatLabsPojoFirst;
	}
	public PatLabsPojo getOtherPatLabsPojoSecond() {
		return otherPatLabsPojoSecond;
	}
	public void setOtherPatLabsPojoSecond(PatLabsPojo otherPatLabsPojoSecond) {
		this.otherPatLabsPojoSecond = otherPatLabsPojoSecond;
	}
	public PatLabsPojo getOtherPatLabsPojoThird() {
		return otherPatLabsPojoThird;
	}
	public void setOtherPatLabsPojoThird(PatLabsPojo otherPatLabsPojoThird) {
		this.otherPatLabsPojoThird = otherPatLabsPojoThird;
	}
	public PatLabsPojo getOtherPatLabsPojoFourth() {
		return otherPatLabsPojoFourth;
	}
	public void setOtherPatLabsPojoFourth(PatLabsPojo otherPatLabsPojoFourth) {
		this.otherPatLabsPojoFourth = otherPatLabsPojoFourth;
	}
	public PatLabsPojo getOtherPatLabsPojoFive() {
		return otherPatLabsPojoFive;
	}
	public void setOtherPatLabsPojoFive(PatLabsPojo otherPatLabsPojoFive) {
		this.otherPatLabsPojoFive = otherPatLabsPojoFive;
	}
	/*********************/
	
	
	public Map<Long, List<HLAPojo>> getCbuHlaMap() {
		return cbuHlaMap;
	}
	public Long[] getRace() {
		return race;
	}
	public void setRace(Long[] race) {
		this.race = race;
	}
	public List getMrqformversionlst() {
		return mrqformversionlst;
	}
	public void setMrqformversionlst(List mrqformversionlst) {
		this.mrqformversionlst = mrqformversionlst;
	}
	public List getFmhqformversionlst() {
		return fmhqformversionlst;
	}
	public void setFmhqformversionlst(List fmhqformversionlst) {
		this.fmhqformversionlst = fmhqformversionlst;
	}
	public List getIdmformversionlst() {
		return idmformversionlst;
	}
	public void setIdmformversionlst(List idmformversionlst) {
		this.idmformversionlst = idmformversionlst;
	}
	public Boolean getIsCordEntryPage() {
		return isCordEntryPage;
	}
	public void setIsCordEntryPage(Boolean isCordEntryPage) {
		this.isCordEntryPage = isCordEntryPage;
	}
	public List<HLAPojo> getEditMaternalHlas() {
		return editMaternalHlas;
	}
	public void setEditMaternalHlas(List<HLAPojo> editMaternalHlas) {
		this.editMaternalHlas = editMaternalHlas;
	}
	public List<HLAPojo> getEditHlas() {
		return editHlas;
	}
	public void setEditHlas(List<HLAPojo> editHlas) {
		this.editHlas = editHlas;
	}
	public List<HLAPojo> getMaternalHlas() {
		return maternalHlas;
	}
	public void setMaternalHlas(List<HLAPojo> maternalHlas) {
		this.maternalHlas = maternalHlas;
	}
	public void setCbuHlaMap(Map<Long, List<HLAPojo>> cbuHlaMap) {
		this.cbuHlaMap = cbuHlaMap;
	}
	public Map<Long, List<HLAPojo>> getMatHlaMap() {
		return matHlaMap;
	}
	public void setMatHlaMap(Map<Long, List<HLAPojo>> matHlaMap) {
		this.matHlaMap = matHlaMap;
	}
	public Map<Long, List<HLAPojo>> getPatHlaMap() {
		return patHlaMap;
	}
	public void setPatHlaMap(Map<Long, List<HLAPojo>> patHlaMap) {
		this.patHlaMap = patHlaMap;
	}
	public Integer getFormDataToBeLoaded() {
		return formDataToBeLoaded;
	}
	public void setFormDataToBeLoaded(Integer formDataToBeLoaded) {
		this.formDataToBeLoaded = formDataToBeLoaded;
	}
	public String getMrqversion() {
		return mrqversion;
	}
	public void setMrqversion(String mrqversion) {
		this.mrqversion = mrqversion;
	}
	public String getFmhqversion() {
		return fmhqversion;
	}
	public void setFmhqversion(String fmhqversion) {
		this.fmhqversion = fmhqversion;
	}
	public String getIdmversion() {
		return idmversion;
	}
	public void setIdmversion(String idmversion) {
		this.idmversion = idmversion;
	}
	public ClinicalNotePojo getClinicalNotePojo() {
		return clinicalNotePojo;
	}
	public void setClinicalNotePojo(ClinicalNotePojo clinicalNotePojo) {
		this.clinicalNotePojo = clinicalNotePojo;
	}
	public EntitySamplesPojo getEntitySamplesPojo() {
		return entitySamplesPojo;
	}
	public void setEntitySamplesPojo(EntitySamplesPojo entitySamplesPojo) {
		this.entitySamplesPojo = entitySamplesPojo;
	}
	public String getCordAdditionalInfo() {
		return cordAdditionalInfo;
	}
	public void setCordAdditionalInfo(String cordAdditionalInfo) {
		this.cordAdditionalInfo = cordAdditionalInfo;
	}
	public PaginateSearch getPaginateSearch() {
		return paginateSearch;
	}
	public void setPaginateSearch(PaginateSearch paginateSearch) {
		this.paginateSearch = paginateSearch;
	}
	public Long getFkCordCbuUnlicenseReason() {
		return fkCordCbuUnlicenseReason;
	}
	public void setFkCordCbuUnlicenseReason(Long fkCordCbuUnlicenseReason) {
		this.fkCordCbuUnlicenseReason = fkCordCbuUnlicenseReason;
	}
	public Long getFkCordCbuEligible() {
		return fkCordCbuEligible;
	}
	public void setFkCordCbuEligible(Long fkCordCbuEligible) {
		this.fkCordCbuEligible = fkCordCbuEligible;
	}
	public Long getFkCordCbuEligibleReason() {
		return fkCordCbuEligibleReason;
	}
	public void setFkCordCbuEligibleReason(Long fkCordCbuEligibleReason) {
		this.fkCordCbuEligibleReason = fkCordCbuEligibleReason;
	}
	public List getCbuStrLocationList() {
		return cbuStrLocationList;
	}
	public void setCbuStrLocationList(List cbuStrLocationList) {
		this.cbuStrLocationList = cbuStrLocationList;
	}
	public List getCbuCollectionList() {
		return cbuCollectionList;
	}
	public void setCbuCollectionList(List cbuCollectionList) {
		this.cbuCollectionList = cbuCollectionList;
	}
	public Long[] getReasons() {
		return reasons;
	}
	public void setReasons(Long[] reasons) {
		this.reasons = reasons;
	}
	public Long[] getInEligibleReasons() {
		return inEligibleReasons;
	}
	public void setInEligibleReasons(Long[] inEligibleReasons) {
		this.inEligibleReasons = inEligibleReasons;
	}
	public Long[] getInCompReasons() {
		return inCompReasons;
	}
	public void setInCompReasons(Long[] inCompReasons) {
		this.inCompReasons = inCompReasons;
	}
	public Long[] getLicenseReasons() {
		return licenseReasons;
	}
	public void setLicenseReasons(Long[] licenseReasons) {
		this.licenseReasons = licenseReasons;
	}
	public Long getCbuLicStatus() {
		return cbuLicStatus;
	}
	public void setCbuLicStatus(Long cbuLicStatus) {
		this.cbuLicStatus = cbuLicStatus;
	}
	public Long[] getCbuOnBag() {
		return cbuOnBag;
	}
	public void setCbuOnBag(Long[] cbuOnBag) {
		this.cbuOnBag = cbuOnBag;
	}
	public List<AdditionalIdsPojo> getAdditionalIds() {
		return additionalIds;
	}
	public void setAdditionalIds(List<AdditionalIdsPojo> additionalIds) {
		this.additionalIds = additionalIds;
	}
	public String getCbuCollectionDateStr() {
		return cbuCollectionDateStr;
	}
	public void setCbuCollectionDateStr(String cbuCollectionDateStr) {
		this.cbuCollectionDateStr = cbuCollectionDateStr;
	}
	public CdrCbuPojo getCdrCbuPojo() {
		return cdrCbuPojo;
	}
	public void setCdrCbuPojo(CdrCbuPojo cdrCbuPojo) {
		this.cdrCbuPojo = cdrCbuPojo;
	}
	public String getOpenMode() {
		return openMode;
	}
	public void setOpenMode(String openMode) {
		this.openMode = openMode;
	}
	
	public CordEntryAction(){
		
		action = new CBUAction(0);
		action.cbuController = cbuController;
		action.specimenController = specimenController;
		action.cbbController = cbbController;	
		action.linkController = linkController;
		action.clinicalNoteController = clinicalNoteController;
	}
	public CordEntryAction(int X){
	
	}

	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String simpleCbuReports()
	{
		try {
			List<CBUStatus> defferedReason = new ArrayList<CBUStatus>();  
			List<SitePojo> cbbList = cbbController.getSitesForQueryBuilder();
			getRequest().setAttribute("CbbList", cbbList);
			
			UserJB user = (UserJB)getRequest().getSession().getAttribute(VelosGarudaConstants.GARUDA_USER);
			Long userId = (long)user.getUserId();
			
			request.setAttribute("userId", userId);
			String primaryOrg = user.getUserSiteId();
			String []primaryOrgArr = new String[] {primaryOrg};

		    String primaryOrgName = getSiteName(Long.valueOf(primaryOrg));
		    String primaryOrgSiteId = getSiteID(Long.valueOf(primaryOrg));
		    getRequest().setAttribute("primaryOrg", primaryOrg);
		    setCbbdrpdwn(primaryOrgArr);
		    getRequest().setAttribute("primaryOrgName", primaryOrgName);
		    getRequest().setAttribute("primaryOrgSiteId", primaryOrgSiteId);
		   // Long module = getCodeListPkByTypeAndSubtype(FILTER_MODULE,QUERY_BUILD_REPORT);
		//	Long subModule = getCodeListPkByTypeAndSubtype(QUERY_BUILD_REPORT_SUBMODULE,CBU_QUERIES);
		 //   setCustomFilter(linkController.getCustomFilterData((long)user.getUserId(),module,subModule));
		    
		    defferedReason.add(getCbuStatusByCode(VelosGarudaConstants.AG));//Aliquots Gone
		    defferedReason.add(getCbuStatusByCode(VelosGarudaConstants.RSN));//Reserved for NMDP 
		    defferedReason.add(getCbuStatusByCode(VelosGarudaConstants.CD));//Cord Destroyed or Damaged
		    defferedReason.add(getCbuStatusByCode(VelosGarudaConstants.IN));//Cord Infused
		   
		    defferedReason.add(getCbuStatusByCode(VelosGarudaConstants.RSO));//Reserved for Other
		    defferedReason.add(getCbuStatusByCode(VelosGarudaConstants.SO));//Cord Shipped for Other
		    defferedReason.add(getCbuStatusByCode(VelosGarudaConstants.AV));//Available
		    defferedReason.add(getCbuStatusByCode(VelosGarudaConstants.QR));//Cord quarantined
		    defferedReason.add(getCbuStatusByCode(VelosGarudaConstants.DD));//Permanently Medically Deferred
		    defferedReason.add(getCbuStatusByCode(VelosGarudaConstants.SH));//Cord Shipped
		    defferedReason.add(getCbuStatusByCode(VelosGarudaConstants.OT));//Cord Unavailable - Other Reasons
		   
			
			
			setDefferedReason(defferedReason);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "success";
	}
	
	
	public String cordWholeEntry() {
		String returnFlow = "";	
		try{
			if(getSession(getRequest()).getAttribute("cordEntryOnceToken")!=null && getRequest().getParameter("cordEntryOnceToken")!=null 
					&& getSession(getRequest()).getAttribute("cordEntryOnceToken").toString().equalsIgnoreCase(getRequest().getParameter("cordEntryOnceToken"))){
				try{
					cdrCbuPojo = getCdrCbuPojo();
					cdrCbuPojo.setCreationDate(new Date());
					cdrCbuPojo.setIsSystemCord(ISSYSGENCORDYESRESP);
					cdrCbuPojo.setCordSearchable(0l);
					Specimen specimen = new Specimen();
					specimen.setSpecType(getCodeListPkByTypeAndSubtype(VelosGarudaConstants.SPEC_TYPE, VelosGarudaConstants.SPEC_SUB_TYPE));
					cdrCbuPojo.setSpecimen(specimen);
					specimen = specimenController.saveOrUpdateSpecimen(cdrCbuPojo.getSpecimen());	
					cdrCbuPojo.setFkSpecimenId(specimen.getPkSpecimen());
					
					setCdrCbuPojo(cbuController.saveOrUpdateCordInfo(cdrCbuPojo));
					if(getCbuOnBag()!=null && getCbuOnBag().length>0 && getCbuOnBag()[0]!=null){
						 action.saveEntityMultipleType(VelosGarudaConstants.ENTITY_CBU_CODE_SUB_TYPE, VelosGarudaConstants.ALL_IDS, getCdrCbuPojo().getCordID(), getCbuOnBag());
					}
				    if(getAdditionalIds() != null && getAdditionalIds().size()>0){
						List<AdditionalIdsPojo> additionalIdsPojo = getAdditionalIds();
						if(additionalIdsPojo!=null){			
							additionalIdsPojo = action.saveCordAdditionalIds(additionalIdsPojo, getCdrCbuPojo());
							additionalIdsPojo = cbuController.saveCordInfoAdditionalIds(additionalIdsPojo);
							setAdditionalIds( action.getAdditionalIdValueType(additionalIdsPojo));
						}			
					}
				    setDomestic(cdrCbuPojo);				    
				    Date date = new Date();
					Timestamp timestamp = new Timestamp(date.getTime());
					getSession(getRequest()).setAttribute("cordEditEntryToken", timestamp.toString());
				}catch (Exception e) {
					e.printStackTrace();
				}
				getSession(getRequest()).setAttribute("cordEntryOnceToken", null);
				getTempHlaData(cdrCbuPojo);
				returnFlow = SUCCESS;
			}else {
				try{
					if(cdrCbuPojo.getCordID()!=null){
						cdrCbuPojo = cbuController.getCordInfoById(cdrCbuPojo);
						cdrCbuPojo = action.setTimeForCdrCbu(cdrCbuPojo);
						setCdrCbuPojo(cdrCbuPojo);
						setLicensureData(cdrCbuPojo);
						setEligibleData(cdrCbuPojo);
						
						
						
					}else{
						String criteria = "and replace(lower(registryId),'-','')=lower('"+Utilities.removeDelimeters(cdrCbuPojo.getRegistryId(),"-")+"')" +
						" and fkCbbId="+cdrCbuPojo.getFkCbbId()+" and localCbuId ='"+cdrCbuPojo.getLocalCbuId()+"'" +
								" and replace(lower(registryMaternalId),'-','')=lower('"+Utilities.removeDelimeters(cdrCbuPojo.getRegistryMaternalId(),"-")+"')";
						List<Object> list = cbuController.getCBUCordInfoList(CDRCBU_OBJECTNAME, criteria,null);
						CdrCbu object = null;
						if(list!=null && list.size()==1){
							object = (CdrCbu)list.get(0);
							cdrCbuPojo = new CdrCbuPojo();
							if(object.getSpecimen()!=null){
								Specimen specimen = new Specimen();
								specimen = (Specimen)ObjectTransfer.transferObjects(object.getSpecimen(), specimen);
								cdrCbuPojo.setSpecimen(specimen);					
							}
							if(object!=null){
								cdrCbuPojo = (CdrCbuPojo)ObjectTransfer.transferObjects(object, cdrCbuPojo);												
							}
							cdrCbuPojo.setCordID(object.getCordID());
							setCdrCbuPojo(cdrCbuPojo);						
						}
						/*******************set licence and licence reason to defalult if cbb is international(Tarun)***********/
						setDomestic(cdrCbuPojo);
					}
					List<AdditionalIdsPojo> additionalIdsPojo = cbuController.getCordInfoAdditionalIds(cdrCbuPojo.getCordID());
					if(additionalIdsPojo!=null){			
						setAdditionalIds( action.getAdditionalIdValueType(additionalIdsPojo));					
					}										 
				}catch(Exception ex){
					ex.printStackTrace();
				}
				Date date = new Date();
				Timestamp timestamp = new Timestamp(date.getTime());
				getSession(getRequest()).setAttribute("cordEditEntryToken", timestamp.toString());
				returnFlow = SUCCESS;
			}	
			action.setCdrCbuPojo(getCdrCbuPojo());
			cbuStrLocationList=(List<SitePojo>) action.setMultipleValues(VelosGarudaConstants.SITE_TYPE,getCdrCbuPojo().getFkCbbId(),VelosGarudaConstants.CBU_STORAGE);
			setCbuStrLocationList(cbuStrLocationList);			
			cbuCollectionList=(List<SitePojo>) action.setMultipleValues(VelosGarudaConstants.SITE_TYPE,getCdrCbuPojo().getFkCbbId(),VelosGarudaConstants.CBU_COLLECTION);
			setCbuCollectionList(cbuCollectionList);
			Long fkCBBId = getCdrCbuPojo().getFkCbbId();
			action.getCdrCbuPojo().setFkCbbId(null);
			action.getMultipleValuesData();
			action.getCdrCbuPojo().setFkCbbId(fkCBBId);
			setCbuOnBag(action.getCbuOnBag());
			setRace(action.getRace());
			action.prepareData();
			action.setCodeListData(getCdrCbuPojo());
			setPopulatedData(action.getPopulatedData());
			cdrCbuPojo.setInEligiblePkid(getCodeListPkByTypeAndSubtype(ELIGIBILITY_STATUS, INELIGIBLE_REASON).intValue());
			cdrCbuPojo.setIncompleteReasonId(getCodeListPkByTypeAndSubtype(ELIGIBILITY_STATUS, INCOMPLETE_REASON).intValue());
			cdrCbuPojo.setNotCollectedToPriorReasonId(getCodeListPkByTypeAndSubtype(ELIGIBILITY_STATUS, NOT_COLLECTED_TO_PRIOR_REASON).intValue());
			cdrCbuPojo.setUnLicesePkid(getCodeListPkByTypeAndSubtype(LICENCE_STATUS, UNLICENSE_REASON).intValue());
			Boolean shipMentFlag = isTasksRequired(getCdrCbuPojo().getCordID());
			request.setAttribute("shipMentFlag", shipMentFlag);
			if(shipMentFlag==true)
			{
				setDeclNewPojo(cbuController.getEligibilityNewDeclQuestionsByEntity(getCdrCbuPojo().getCordID(), getCodeListPkByTypeAndSubtype(ENTITY_CBU_CODE_TYPE, ENTITY_CBU_CODE_SUB_TYPE),null));
			}
			else
			{
				setDeclPojo(cbuController.getEligibilityDeclQuestionsByEntity(getCdrCbuPojo().getCordID(), getCodeListPkByTypeAndSubtype(ENTITY_CBU_CODE_TYPE, ENTITY_CBU_CODE_SUB_TYPE),null));
				
			}
			request.setAttribute("sampAuto","cordEntryAuto");
			loadNotesData();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return returnFlow;
	}
	
	public void setDomestic(CdrCbuPojo cdrCbuPojo){
		/*******************set licence and licence reason to default if cbb is international(Tarun)***********/
		if(cdrCbuPojo.getIsSiteDomestic()!=null && cdrCbuPojo.getIsSiteDomestic()==false){
			 Long unlicensePK = getCodeListPkByTypeAndSubtype(LICENCE_STATUS, UNLICENSE_REASON);
			 long internationalReasonPK = getCodeListPkByTypeAndSubtype(UNLICENSE_REASON, UNLICENSE_REASON_INTERNATIONAL);
			 Long[] reasonArr = new Long[1];
			 reasonArr[0]=internationalReasonPK;
			 setCbuLicStatus(unlicensePK);
			 cdrCbuPojo.setCbuLicStatus(unlicensePK);
			 cdrCbuPojo.setUnLicesePkid(unlicensePK.intValue());
			 setLicenseReasons(reasonArr);
		 }
	}
	
	/*public Object getFactoryObject(String className){
		Object obj = null;
		if(CBUController.class.getName().equalsIgnoreCase(className)){
			if(cbuController==null){
				cbuController = new CBUController();
				obj = cbuController;
			}else{
				obj = cbuController;				
			}
		}
		if(CBBController.class.getName().equalsIgnoreCase(className)){
			if(cbbController==null){
				cbbController = new CBBController();
				obj = cbbController;
			}else{
				obj = cbbController;				
			}
		}
		return obj;
	}*/
	
	public CdrCbuPojo setLicensureData(CdrCbuPojo cdrCbuPojo) {
		try{
			Long licensePK = getCodeListPkByTypeAndSubtype(LICENCE_STATUS, LICENCE);			
			if(cdrCbuPojo.getCbuLicStatus()!=null && licensePK!=null && cdrCbuPojo.getCbuLicStatus()==licensePK){
					setCbuLicStatus(cdrCbuPojo.getCbuLicStatus());
					setFkCordCbuUnlicenseReason(cdrCbuPojo.getCbuLicStatus());
					/*cdrCbuPojo.setFkCordCbuUnlicenseReason(cdrCbuPojo.getCbuLicStatus());*/
			}else{
				   setCbuLicStatus(cdrCbuPojo.getCbuLicStatus());
				   /*if(cdrCbuPojo.getFkCordCbuUnlicenseReason()!=null){
						List<Object> reasonIds = action.getMultipleReasons(cdrCbuPojo.getFkCordCbuUnlicenseReason(), paginateSearch, 0);
						Long[] reasonArr = new Long[reasonIds.size()];
						Integer count = 0;
						for(Object o : reasonIds){
							if(o!=null){
								reasonArr[count] = ((BigDecimal)o).longValue();	
								count++;
							}
						}
						if(reasonArr.length>0){
							setLicenseReasons(reasonArr);						
						}
					}
					setFkCordCbuUnlicenseReason(cdrCbuPojo.getFkCordCbuUnlicenseReason());*/
					request.setAttribute("unlicensed", true);						
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return cdrCbuPojo;
	}
	
	public CdrCbuPojo setEligibleData(CdrCbuPojo cdrCbuPojo){
		try{
			long eligiblePK = getCodeListPkByTypeAndSubtype(ELIGIBILITY_STATUS, ELIGIBLE);
			long notCollectedPriorToPk = getCodeListPkByTypeAndSubtype(ELIGIBILITY_STATUS, NOT_COLLECTED_TO_PRIOR_REASON);
			if(cdrCbuPojo.getFkCordCbuEligible()!=null && (cdrCbuPojo.getFkCordCbuEligible()==eligiblePK || cdrCbuPojo.getFkCordCbuEligible()==notCollectedPriorToPk)){
				setFkCordCbuEligible(cdrCbuPojo.getFkCordCbuEligible());
				setFkCordCbuEligibleReason(cdrCbuPojo.getFkCordCbuEligible());
				cdrCbuPojo.setFkCordCbuEligibleReason(cdrCbuPojo.getFkCordCbuEligibleReason());
				setCordAdditionalInfo(cdrCbuPojo.getCord_Add_info());
			}else{
				setFkCordCbuEligible(cdrCbuPojo.getFkCordCbuEligible());
				if(cdrCbuPojo.getFkCordCbuEligibleReason()!=null){
					List<Object> reasonIds = action.getMultipleReasons(cdrCbuPojo.getFkCordCbuEligibleReason(), paginateSearch,0);
					Long[] reasonArr = new Long[reasonIds.size()];
					Integer count = 0;
					for(Object o : reasonIds){
						if(o!=null){
						reasonArr[count] = ((BigDecimal)o).longValue();	
						count++;
						}
					}
					if(reasonArr.length>0){
						setReasons(reasonArr);
					}
					setFkCordCbuEligibleReason(cdrCbuPojo.getFkCordCbuEligibleReason());
				}				
				setCordAdditionalInfo(cdrCbuPojo.getCord_Add_info());
		    }	
		}catch (Exception e) {
			e.printStackTrace();
		}
		return cdrCbuPojo;
	}
	
	public String loadProcessingProcedureData(){
		try{
			action.setCdrCbuPojo(cdrCbuPojo);
			setUserSite();			
			if(getSitesByUser()!=null)
			  action.setSitesByUser(getSitesByUser());
			action.setSiteAndProcedure();
			action.getProcedureData();
			if(action.getEntitySamplesPojo()!=null)
			  setEntitySamplesPojo(action.getEntitySamplesPojo());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String loadLabSummaryData(){
		LabTestcontroller labTestController = (LabTestcontroller)ObjectFactory.MakeObjects.getFactoryObject(LabTestcontroller.class.getName());
		action.labTestcontroller = labTestController;
		try{
			if(getCdrCbuPojo().getCordID()!=null){
				//cdrCbuPojo = cbuController.getCordInfoById(getCdrCbuPojo());
				action.setLabSummeryEssnData();
				action.setCdrCbuPojo(cdrCbuPojo);
				action.getOtherTests();
				long pkOtherType= getCodeListPkByTypeAndSubtype(VelosGarudaConstants.CFU_COUNT, VelosGarudaConstants.CFU_COUNT_SUBTYPE);
				setPkCfuOther(pkOtherType);
				long pkViaOtherType = getCodeListPkByTypeAndSubtype(VelosGarudaConstants.VAIBILITY, VelosGarudaConstants.VIABILITY_SUBTYPE);
				setPkViabilityOther(pkViaOtherType);
				setOtherLabTstsLstId("");
				//if(cdrCbuPojo.getFkSpecimenId()!=null)
				//   setPatLabs(action.showOtherTests(cdrCbuPojo.getFkSpecimenId()));
				setPatLabs(action.getPatLabs());
				setProcessingTest(action.getProcessingTest());
				setPreTestList(action.getPreTestList());
				if (getPreTestList() != null && getPreTestList().size() > 0) {
				   for(PatLabsPojo p : getPreTestList()){
			    		if(p.getTestdate()!=null){
			    			setPrePatLabsPojo(p);
			    		}
			    	}
				}
				setPostTestList(action.getPostTestList());
				if (getPostTestList() != null && getPostTestList().size() > 0) {
				   for(PatLabsPojo g : getPostTestList()){
			    		if(g.getTestdate()!=null){
			    			setPostPatLabsPojo(g);
			    		}
			    	}
				}
				setOtherPostTestList(action.getOtherPostTestList());
				if(getOtherPostTestList()!= null && getOtherPostTestList().size() > 0){
			    	for(PatLabsPojo other : getOtherPostTestList()){
			    		if(other.getTestdate()!=null){
				    	setOtherPatLabsPojo(other);
			    		}
			    	}
			    }
				setOtherPostTestListFirst(action.getOtherPostTestListFirst());
				if(getOtherPostTestListFirst()!= null && getOtherPostTestListFirst().size() > 0){
				      PatLabsPojo otherFirst = (PatLabsPojo)getOtherPostTestListFirst().get(0);
				      setOtherPatLabsPojoFirst(otherFirst);
				}
				setOtherPostTestListSecond(action.getOtherPostTestListSecond());
				if(getOtherPostTestListSecond()!= null && getOtherPostTestListSecond().size() > 0){
				      PatLabsPojo otherSecond = (PatLabsPojo)getOtherPostTestListSecond().get(0);
				      setOtherPatLabsPojoSecond(otherSecond);
				}
				setOtherPostTestListThird(action.getOtherPostTestListThird());
				if(getOtherPostTestListThird()!= null && getOtherPostTestListThird().size() > 0){
				      PatLabsPojo otherThird = (PatLabsPojo)getOtherPostTestListThird().get(0);
				      setOtherPatLabsPojoThird(otherThird);
				}
				setOtherPostTestListFourth(action.getOtherPostTestListFourth());
				if(getOtherPostTestListFourth()!= null && getOtherPostTestListFourth().size() > 0){
				      PatLabsPojo otherFourth = (PatLabsPojo)getOtherPostTestListFourth().get(0);
				      setOtherPatLabsPojoFourth(otherFourth);
				}
				setOtherPostTestListFifth(action.getOtherPostTestListFifth());				  
				if(getOtherPostTestListFifth()!= null && getOtherPostTestListFifth().size() > 0){
				      PatLabsPojo otherFifth = (PatLabsPojo)getOtherPostTestListFifth().get(0);
				      setOtherPatLabsPojoFive(otherFifth);
				}
				Long tncFrozenId = null;
				for(LabTestPojo test : getProcessingTest()){
					//if(test.getLabtestName().equals("Total CBU Nucleated Cell Count (unknown if uncorrected)")){
					if(test.getLabtestName().equals(VelosGarudaConstants.TNCFROZEN)){
						tncFrozenId=test.getPkLabtest();
					}
				}
				for(LabTestPojo test : getProcessingTest()){
					if(test.getLabtestName().equals(VelosGarudaConstants.TNCFROZENNXT)){
						tncFrozenId = test.getPkLabtest();
				  }	
				}	
				if(getProcessingTest()!=null)
				   setLabGroupTypTests(action.getLabGroupTypTests());				
				
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String loadHlaData(){
		try{
			action.setCdrCbuPojo(cdrCbuPojo);
			action.setCbuAndMaternalHla(null);
			action.setHlaInfoForCord();
			setCbuHlaMap(action.getCbuHlaMap());
			setMatHlaMap(action.getMatHlaMap());
			setPatHlaMap(action.getPatHlaMap());
			setEditHlas(action.getEditHlas());
			setEditMaternalHlas(action.getEditMaternalHlas());
			
			/* Tarun Sondhi*/
			 sortHlaInfo( getCbuHlaMap());
			 sortHlaInfo( getMatHlaMap());					
			List<BestHLAPojo> bhlas = null;			
			/****set best hla*****/
			if(getCdrCbuPojo().getCordID()!=null && !getCdrCbuPojo().getCordID().equals("") && !getCdrCbuPojo().getCordID().equals("undefined")){
				bhlas = setBestHlaByEntityType(getCodeListPkByTypeAndSubtype(ENTITY_CBU_CODE_TYPE, ENTITY_CBU_CODE_SUB_TYPE),getCdrCbuPojo().getCordID());
			}
			if(bhlas!=null && bhlas.size()>0)
			 setBestHlas(bhlas);
			calculateHlaPercentage();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public void sortHlaInfo(Map<Long, List<HLAPojo>> cbuHlaMap){
		for(Map.Entry<Long, List<HLAPojo>> hlp: cbuHlaMap.entrySet()){				
			
			List<HLAPojo> hlaPojoList = hlp.getValue();
			Date latestUpdateDate= null;
			Long latestUpdater = null;
			
			for(HLAPojo hp: hlp.getValue()){
				if(hp != null){
					
					if(latestUpdateDate == null || latestUpdateDate.compareTo(hp.getCreatedOn()) <0){
						
					      latestUpdateDate = hp.getCreatedOn();
					      latestUpdater = hp.getCreatedBy();
					}
						
						if(hp.getLastModifiedOn() != null && latestUpdateDate.compareTo( hp.getLastModifiedOn())< 0){
							
							//if(latestUpdateDate == null){
								
								latestUpdateDate = hp.getLastModifiedOn();
								latestUpdater = hp.getLastModifiedBy();								
							
						/*}else{
							
							if( latestUpdateDate.compareTo( hp.getLastModifiedOn())< 0){
								
								latestUpdateDate = hp.getLastModifiedOn();
								latestUpdater = hp.getLastModifiedBy();
								
							}*/
						
					}			
										
				  }					
			}
			
			if(hlaPojoList != null && !hlaPojoList.isEmpty() && latestUpdateDate !=null){
				
				HLAPojo hp = hlaPojoList.get(0);
						hp.setHlaRecievedDate(latestUpdateDate);
						hp.setCreatedBy(latestUpdater);
			}
			
			
		}
	}
	
	public String loadFormData(){
		try{
			DynaFormController formController = (DynaFormController)ObjectFactory.MakeObjects.getFactoryObject(DynaFormController.class.getName());
			action.formController = formController;
			action.setFormDataToBeLoaded(getFormDataToBeLoaded());
			action.setCdrCbuPojo(getCdrCbuPojo());
			action.getFormsData();
			mrqformversionlst = action.mrqformversionlst;
			setMrqformversionlst(mrqformversionlst);
			setFmhqformversionlst(fmhqformversionlst);
			setIdmformversionlst(idmformversionlst);
			fmhqformversionlst = action.fmhqformversionlst;
			idmformversionlst = action.idmformversionlst;
			setMrqversion(action.getMrqversion());
			setFmhqversion(action.getFmhqversion());
			setIdmversion(action.getIdmversion());
			setIsCordEntryPage(true);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String loadNotesData(){
		try{
			 ClinicalNoteController clinicalNoteController = (ClinicalNoteController)ObjectFactory.MakeObjects.getFactoryObject(ClinicalNoteController.class.getName());
			 action.clinicalNoteController = clinicalNoteController;
			 clinicalNotePojo = new ClinicalNotePojo();
		// setClinicalNotelist(clinicalNoteController.clinicalNoteHistory(getCdrCbuPojo().getCordID(),clinicalNotePojo.getFkNotesType(),clinicalNotePojo.getRequestType()));
		     /* if(listnotes!=null && listnotes.size()>0){
				  setClinicalNotePojo(listnotes.get(0));
				  String assess = null;
				  assess = clinicalNotePojo.getNoteAssessment();
				  request.setAttribute("assess", assess);
			  }*/
			 	Long noteCategory = null;
				noteCategory =getCodeListPkByTypeAndSubtype(NOTE_TYPE,NOTE_TYPE_CODE_SUB_TYPE_CLINIC);
				setClinicalNotelist(action.clinicalNoteController.clinicalNoteHistory(getCdrCbuPojo().getCordID(),noteCategory,null));
				
		}catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	public String generatePdfReport(){
		if(StringUtils.equalsIgnoreCase(getReportType(),NMDP_CBU_QUICKSUMMARY_REPORT))
		{}
		
		return SUCCESS;
	}
	
	
	public void getTempHlaData(CdrCbuPojo cdrCbuPojo){
		Boolean guidReset = false;
		try{
		List<TempHLAPojo> listTempHla = cbuController.getTempHla(getCodeListPkByTypeAndSubtype(VelosGarudaConstants.CORD_ENTITY_TYPE, VelosGarudaConstants.CORD_ENTITY_SUB_TYPE), cdrCbuPojo.getRegistryId());
		List<HLAPojo> listHlaTemp = new ArrayList<HLAPojo>();		
		Long cbuHlaOrderSeq = null;
			if(listTempHla!=null && listTempHla.size()>0){
				if(listTempHla.get(0)!=null && !StringUtils.isEmpty(listTempHla.get(0).getGuid())){
					getCdrCbuPojo().setGuid(listTempHla.get(0).getGuid());
					guidReset = true;	
					
				}
				cbuHlaOrderSeq = cbuController.getSequence(SEQ_CB_HLA_ORDER);
				for(TempHLAPojo t : listTempHla){
					HLAPojo hlaPojo = new HLAPojo();
					hlaPojo = (HLAPojo)ObjectTransfer.transferObjects(t, hlaPojo);
					hlaPojo.setPkHla(null);
					hlaPojo.setPkcordId(null);
					hlaPojo.setFkentityCordId(cdrCbuPojo.getCordID());
					hlaPojo.setHlaOrderSeq(cbuHlaOrderSeq);
					listHlaTemp.add(hlaPojo);
				}
				if(listHlaTemp!=null && listHlaTemp.size()>0){
					//cbuController.deleteHla(getCodeListPkByTypeAndSubtype(VelosGarudaConstants.CORD_ENTITY_TYPE, VelosGarudaConstants.CORD_ENTITY_SUB_TYPE), cdrCbuPojo.getCordID());
					cbuController.saveOrUpdateHlas(listHlaTemp,false);
					cbuController.deleteTempHla(getCodeListPkByTypeAndSubtype(VelosGarudaConstants.CORD_ENTITY_TYPE, VelosGarudaConstants.CORD_ENTITY_SUB_TYPE), cdrCbuPojo.getRegistryId());
				}
			}
			if(guidReset)
				setCdrCbuPojo(cbuController.saveOrUpdateCordInfo(cdrCbuPojo));
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String setPaginationReportData(){
		   try{
			   int iTotalRows = 0;
			   CdrCbuPojo cdrCbuPojo = null;
			   UserJB user = (UserJB)getSession(getRequest()).getAttribute(VelosGarudaConstants.GARUDA_USER);
			   if(user!=null){
				   Long userSiteId = Long.parseLong(user.getUserSiteId().toString());
				   request.setAttribute("userSiteId", userSiteId);
				   Long userId=(long)user.getUserId();
				   request.setAttribute("userId", userId);
			   }
			   paginateSearch = new PaginateSearch();
			   paginationReqScopeData(paginateSearch);			   
			   List<Object> cdrObjects = new ArrayList<Object>();
			   String sqlHead = "";	
			   String qBuilderTbl="";
			   String condition="";
			   String searchTxt="";
			   String selSiteIdentifier="";
			   selSiteIdentifier = getRequest().getParameter("selSiteIdentifier");
			   ServerGlobalSearch serverglobalsearch=new ServerGlobalSearch();
			   
			   
			   if(request.getParameter("filterTxt")!= null && !request.getParameter("filterTxt").equals("") && request.getParameter("currentpage")!=null && !request.getParameter("currentpage").equals("")){
				   qBuilderTbl=request.getParameter("currentpage");
				   searchTxt=request.getParameter("filterTxt").toString();
				   
				   if(qBuilderTbl.equals("shippedcbu")){
					   condition = serverglobalsearch.FetchGlobalSearchQBuilder(searchTxt);
				   }else if(qBuilderTbl.equals("cbuSummary")){
					   condition = serverglobalsearch.FetchGlobalSearchQBuilder_CBUID(searchTxt);
				   }else if(qBuilderTbl.equals("licElig")){
					   condition = serverglobalsearch.FetchGlobalSearchQBuilder_LICELig(searchTxt);
				   }
			   }
			   
			   
			   if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_ANTIGEN_REPORT)){
				   sqlHead += "SELECT Distinct C.PK_CORD,C.REGISTRY_ID,C.REGISTRY_MATERNAL_ID,C.CORD_LOCAL_CBU_ID,C.MATERNAL_LOCAL_ID,C.CORD_ISBI_DIN_CODE,C.SITE_ID,C.PATIENT_ID,C.CORD_ID_NUMBER_ON_CBU_BAG" +
				   " , C.CORD_HISTORIC_CBU_ID ,"+
				    /*" CASE "+
		            " WHEN ERES.f_codelst_desc(C.order_type)='CT' "+
		            " AND C.order_sample_at_lab        ='Y'"+
		            " THEN 'CT - Sample at Lab' "+
		            " WHEN ERES.f_codelst_desc(C.order_type)='CT'"+
		            " AND C.order_sample_at_lab        ='N'"+
		            " THEN 'CT - Ship Sample' "+
		            " ELSE ERES.f_codelst_desc(C.order_type) "+*/
		           " TO_CHAR(C.SPEC_COLLECTION_DATE,'Mon DD YYYY') as CBU_COLLECTION_DATE ,C.CORD_NMDP_STATUS as CORD_NMDP_STATUS  ";
				   sqlHead += getSession(getRequest()).getAttribute("queryBuilderHlaSql");				   
		           if(request.getParameter("filterTxt")!= null && !request.getParameter("filterTxt").equals("")){
					   searchTxt=request.getParameter("filterTxt").toString();
					   condition = serverglobalsearch.FetchGlobalSearchQBuilder_CBUID(searchTxt);
					   sqlHead += condition;
					   request.setAttribute("filterTxt", searchTxt);
				   }
				   sqlHead += " ORDER BY C.SITE_ID,C.REGISTRY_ID ";
				   
			   }/*else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_LICENSEANDELIGIBILTY_REPORT)){
				   sqlHead += "select C.site_id as cbb_id,C.REGISTRY_ID as cbu_registry_id,C.CORD_LOCAL_CBU_ID as cbu_local_id,C.CORD_ID_NUMBER_ON_CBU_BAG as uniqueidonbag,to_char(C.SPEC_COLLECTION_DATE,'Mon DD, YYYY') as cbu_collection_date," +
			   		" to_char(C.CORD_REGISTRATION_DATE,'Mon DD, YYYY') as nmdp_reg_date,ERES.F_GET_CBU_STATUS(C.FK_CORD_CBU_STATUS) as CBU_Status,ERES.f_codelst_desc(C.FK_CORD_CBU_LIC_STATUS) as Licensure_Status," +
			   		" c.UNLICENSED_DESC, " +
			   		" ERES.f_codelst_desc(C.fk_cord_cbu_eligible_status) AS Elig_Deter, c.INELIG_INCOMP_DESC, " +
			   		" to_char(C.CORD_BABY_BIRTH_DATE,'Mon DD, YYYY') Baby_birth_date, " +
			   		" c.RACE_DESC AS Race," +
			   		" C.funded_cbu as Funded ,ERES.f_codelst_desc(C.NMDP_RACE_ID) AS Race_Summary ,ERES.f_codelst_desc(C.FK_CORD_BACT_CUL_RESULT) AS Bact_cultr_rst, ERES.f_codelst_desc(C.FK_CORD_FUNGAL_CUL_RESULT) AS Fungal_cultr_rst," +
			   		" ERES.f_codelst_desc(C.HEMOGLOBIN_SCRN) AS Hemoglobin_Testing, ERES.f_codelst_desc(C.FK_CBB_PROCEDURE) AS Processing_procedure  , (select to_number(test_result) from "+'"'+"VDA"+'"'+"."+'"'+"ETVDA_QUERY_BUILDER_PAT_LAB"+'"'+" where fk_testid in (select pk_labtest from "+'"'+"VDA"+'"'+"."+'"'+"ETVDA_QUERY_BUILDER_LAB_TEST"+'"'+" where LABTEST_SHORTNAME = 'UNCRCT') " +
			   		" and fk_specimen = C.FK_SPECIMEN_ID and FK_TIMING_OF_TEST in (select pk_codelst from eres.er_codelst where codelst_type = 'timing_of_test' and CODELST_SUBTYP='pre_procesing')) as Nucl_cell_count , " +
			   		" (select to_number(test_result) from "+'"'+"VDA"+'"'+"."+'"'+"ETVDA_QUERY_BUILDER_PAT_LAB"+'"'+" where fk_testid in (select pk_labtest from "+'"'+"VDA"+'"'+"."+'"'+"ETVDA_QUERY_BUILDER_LAB_TEST"+'"'+" where LABTEST_SHORTNAME = 'TCDAD') and fk_specimen = C.FK_SPECIMEN_ID and FK_TIMING_OF_TEST in (select pk_codelst from eres.er_codelst where codelst_type = " +
			   		" 'timing_of_test' and CODELST_SUBTYP='post_procesing')) as CD34_cell_count , (select to_number(test_result) from "+'"'+"VDA"+'"'+"."+'"'+"ETVDA_QUERY_BUILDER_PAT_LAB"+'"'+" where fk_testid in (select pk_labtest from "+'"'+"VDA"+'"'+"."+'"'+"ETVDA_QUERY_BUILDER_LAB_TEST"+'"'+" where LABTEST_SHORTNAME = 'VIAB') and fk_specimen = C.FK_SPECIMEN_ID " +
			   		" and FK_TIMING_OF_TEST in (select pk_codelst from eres.er_codelst where codelst_type = 'timing_of_test' and CODELST_SUBTYP='post_procesing')) as Viability_count ,ERES.f_codelst_desc((select FK_TEST_METHOD from "+'"'+"VDA"+'"'+"."+'"'+"ETVDA_QUERY_BUILDER_PAT_LAB"+'"'+" where fk_testid in (select pk_labtest from "+'"'+"VDA"+'"'+"."+'"'+"ETVDA_QUERY_BUILDER_LAB_TEST"+'"'+" where LABTEST_SHORTNAME = 'VIAB') " +
			   		" and fk_specimen = C.FK_SPECIMEN_ID and FK_TIMING_OF_TEST in (select pk_codelst from eres.er_codelst where codelst_type = 'timing_of_test' and CODELST_SUBTYP='post_procesing'))) as Viability_Method ,(select to_number(test_result) from "+'"'+"VDA"+'"'+"."+'"'+"ETVDA_QUERY_BUILDER_PAT_LAB"+'"'+" where fk_testid in (select pk_labtest from "+'"'+"VDA"+'"'+"."+'"'+"ETVDA_QUERY_BUILDER_LAB_TEST"+'"'+" where LABTEST_SHORTNAME = 'FNPV') " +
			   		" and fk_specimen = C.FK_SPECIMEN_ID and FK_TIMING_OF_TEST in (select pk_codelst from eres.er_codelst where codelst_type = 'timing_of_test' and CODELST_SUBTYP='post_procesing')) as Final_Prod_Vol ,to_char(C.SHIP_DATE,'Mon DD, YYYY') as SHIP_DATE ,C.ORDER_TYPE as ORDER_TYPE ,to_char(C.ORDER_OPEN_DATE,'Mon DD, YYYY') as Request_Date,"+
				   " CASE "+
		            " WHEN ERES.f_codelst_desc(C.order_type)='CT' "+
		            " AND C.order_sample_at_lab        ='Y'"+
		            " THEN 'CT - Sample at Lab' "+
		            " WHEN ERES.f_codelst_desc(C.order_type)='CT'"+
		            " AND C.order_sample_at_lab        ='N'"+
		            " THEN 'CT - Ship Sample' "+
		            " ELSE ERES.f_codelst_desc(C.order_type) "+
		           " END req_type,ERES.F_GET_CBU_STATUS(C.CORD_NMDP_STATUS) as nmdpstatus";
				   sqlHead += getSession(getRequest()).getAttribute("queryBuilderSql");
		           sqlHead += " ORDER BY C.REGISTRY_ID";
			   }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_QUICKSUMMARY_REPORT)){
				   sqlHead += "SELECT Distinct(C.PK_CORD),C.REGISTRY_ID,C.REGISTRY_MATERNAL_ID,C.CORD_LOCAL_CBU_ID,C.MATERNAL_LOCAL_ID,C.CORD_ISBI_DIN_CODE,C.SITE_ID,C.PATIENT_ID,C.CORD_ID_NUMBER_ON_CBU_BAG" +
				   " , C.CORD_HISTORIC_CBU_ID,C.ORDER_TYPE,C.ORDER_SAMPLE_AT_LAB,TO_CHAR(C.SHIP_DATE,'Mon DD, YYYY') SHIP_DATE,TO_CHAR(C.ORDER_OPEN_DATE,'Mon DD, YYYY') ORDER_OPEN_DATE,C.FK_SHIPMENT_TYPE,TO_CHAR(C.INFUSION_DATE,'Mon DD, YYYY') INFUSION_DATE ,C.CURRENT_DIAGNOSIS,C.TRANS_SITE_NAME,C.TRANS_SITE_ID,"+
				   " CASE "+
		            " WHEN ERES.f_codelst_desc(C.order_type)='CT' "+
		            " AND C.order_sample_at_lab        ='Y'"+
		            " THEN 'CT - Sample at Lab' "+
		            " WHEN ERES.f_codelst_desc(C.order_type)='CT'"+
		            " AND C.order_sample_at_lab        ='N'"+
		            " THEN 'CT - Ship Sample' "+
		            " ELSE ERES.f_codelst_desc(C.order_type) "+
		           " END req_type ";
				   sqlHead += getSession(getRequest()).getAttribute("queryBuilderSql");
		          	sqlHead += " ORDER BY C.REGISTRY_ID";
			   }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_IDSUMMARY_REPORT)){
				   sqlHead += "SELECT Distinct(C.PK_CORD),C.SITE_ID,C.REGISTRY_ID,C.REGISTRY_MATERNAL_ID,C.CORD_LOCAL_CBU_ID,C.MATERNAL_LOCAL_ID,C.CORD_ISBI_DIN_CODE,C.CORD_ID_NUMBER_ON_CBU_BAG" +
				   " , C.CORD_HISTORIC_CBU_ID,"+
				   " CASE "+
		            " WHEN ERES.f_codelst_desc(C.order_type)='CT' "+
		            " AND C.order_sample_at_lab        ='Y'"+
		            " THEN 'CT - Sample at Lab' "+
		            " WHEN ERES.f_codelst_desc(C.order_type)='CT'"+
		            " AND C.order_sample_at_lab        ='N'"+
		            " THEN 'CT - Ship Sample' "+
		            " ELSE ERES.f_codelst_desc(C.order_type) "+
		           " END req_type ,TO_CHAR(C.SHIP_DATE,'Mon DD, YYYY') SHIP_DATE,TO_CHAR(C.ORDER_OPEN_DATE,'Mon DD, YYYY') ORDER_OPEN_DATE,C.ORDER_TYPE";
				   sqlHead += getSession(getRequest()).getAttribute("queryBuilderSql");
		          	sqlHead += " ORDER BY C.REGISTRY_ID";
			   }*/
			   if(!sqlHead.equals("") && getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_ANTIGEN_REPORT)){
				   //cdrObjects = vdaController.getQueryBuilderViewData(sqlHead, paginateSearch);
				   cdrObjects =  hlaAntigenDataUserInterface(sqlHead);
			   }
			    /*if(cdrObjects!=null && cdrObjects.size()>0){
			    	setCordInfoList(cdrObjects);
			    	if(getCbuCount()==null || getCbuCount()==0)
			    	{
			    		List<Object> list = vdaController.getQueryBuilderViewData("SELECT COUNT(*) FROM ( "+sqlHead+" )", null);
			    		//cbuCount = list!=null && list.size()>0?list.size():0;
			    		cbuCount = list !=null && list.get(0) != null ?((BigDecimal)list.get(0)).intValue():0;
			    		if(cbuCount>0){
			    			setCbuCount(cbuCount);
			    		}
			    	}                   
			    }*/
			   /* if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_ANTIGEN_REPORT) && cdrObjects!=null && cdrObjects.size()>0){
			    	Map<Long,Map<Long,List<HLAPojo>>> hlas = new HashMap<Long, Map<Long,List<HLAPojo>>>();
			    	Map<Long,List<BestHLAPojo>> bestHlas = new HashMap<Long, List<BestHLAPojo>>();
			    	for(Object o : cdrObjects){
			    		Object[] oArr = (Object[])o;
			    		BigDecimal pkCord  = (BigDecimal)oArr[0];
			    		Map<Long,List<HLAPojo>> mapHla = setHlaMapBYEntityId(pkCord.longValue(),getCodeListPkByTypeAndSubtype(VelosGarudaConstants.CORD_ENTITY_TYPE, VelosGarudaConstants.CORD_ENTITY_SUB_TYPE));
			    		List<HLAPojo> cbuhla = mapHla.get(getCodeListPkByTypeAndSubtype(VelosGarudaConstants.TEST_SOURCES, VelosGarudaConstants.CBU_CODESUBTYPE));
			    		hlas.put(Long.parseLong(pkCord.toString()), setHlaMapForAntigenReport(cbuhla));
			    		List<BestHLAPojo> bhla = setBestHlaByEntityType(getCodeListPkByTypeAndSubtype(ENTITY_CBU_CODE_TYPE, ENTITY_CBU_CODE_SUB_TYPE),pkCord.longValue());
			    		bestHlas.put(Long.parseLong(pkCord.toString()), bhla);			    		
			    	}
			    	setHlas(hlas);
		    		setBestHlas(bestHlas);
			    	//hlaAntigenDataUserInterface(sqlHead, paginateSearch); //commented for NMDP Reported Extra(Without Filter) Query Execution during HLA UI report Search
			    }*/
			    
			    long q1leng=0l;
			    long q2leng=0l;
			    Boolean countflag = false;
			    
			    if(getSession(getRequest()).getAttribute("queryBuilderSql")!=null && getSession(getRequest()).getAttribute("queryBuilderORSql")!=null){
			    	
			    	q1leng=getSession(getRequest()).getAttribute("queryBuilderSql").toString().length();
			    	q2leng=getSession(getRequest()).getAttribute("queryBuilderORSql").toString().length();
			    	if(q1leng!=q2leng){
			    		countflag=true;
			    	}
			    	//System.out.println("\n\nLength1::"+q1leng);
			    	//System.out.println("Length2::"+q2leng);
			    	//System.out.println("flag1::"+countflag);
			    	//System.out.println("flag2::"+(getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_QUICKSUMMARY_REPORT) && countflag==true));
			    }
			    
			    if(getCbuCount()==null || getCbuCount()==0 || (getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_QUICKSUMMARY_REPORT) && countflag==true)){
			    	  sqlHead = "SELECT c.pk_cord ";
			    	  
			    	  if(qBuilderTbl.equals("licElig") || getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_LICENSEANDELIGIBILTY_REPORT)){
			    		  sqlHead += getSession(getRequest()).getAttribute("queryBuilderLESql");			    		  
			    	  }else if(qBuilderTbl.equals("cbuSummary") || getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_QUICKSUMMARY_REPORT)){
			    		  sqlHead += getSession(getRequest()).getAttribute("queryBuilderORSql");		
			    	  }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_ANTIGEN_REPORT)){
			    		  sqlHead += getSession(getRequest()).getAttribute("queryBuilderHlaSql");
			    	  }else{			    	
			    		  sqlHead += getSession(getRequest()).getAttribute("queryBuilderSql");
			    	  }
			
		          	  sqlHead +=" "+condition;
		          	  
		          	//List<Object> countObj = vdaController.getQueryBuilderViewData(sqlHead, null);
		          	Map<String,Object> counterDetails = vdaController.getQueryBuilderRecordDetails(sqlHead);
		          	/* Set<Long> uniqueCords = new LinkedHashSet<Long>();
		             Map<Long,Long> duplCords = new HashMap<Long,Long>();*/
			        /* for(Object o : countObj){
			         	   Long l = ((BigDecimal)o).longValue();
			         	   if(!uniqueCords.add(l)){
			         		   duplCords.put(l, l);
			         	   }
			         }*/
			         
			         Set<Long> uniqueCords = (LinkedHashSet<Long>)counterDetails.get("uniqueCords");
			         Map<Long,Long> duplCords = (Map<Long,Long>)counterDetails.get("duplCords");
			         long countObj = (Long)counterDetails.get("totalRecord");
			         
			        /* if(countObj!=null && countObj.size()>0){
			       		 iTotalRows = countObj.size();
			       		 setCbuCount(iTotalRows);			       		
		       	     }*/
			         
			         if(countObj >0 ){
			    		 iTotalRows = (int)countObj;
			    		 setCbuCount(iTotalRows);
			    		
			    	  }
			         
		     	    getSession(getRequest()).setAttribute("orDuplCords", duplCords);		     	    
			    }else{
			    		iTotalRows = getCbuCount();
			    }
			    if(getCbuCount()==null)
			    	iTotalRows = getCbuCount();
				paginateSearch.setiTotalRows(iTotalRows);
				setPaginateSearch(VelosUtil.getListTotalCount(paginateSearch));
				request.setAttribute("paginateSearch", paginateSearch);
				request.setAttribute("selSiteIdentifier", selSiteIdentifier);	
				if(getSession(getRequest()).getAttribute("selectedSiteId")!=null)
					request.setAttribute("selectedSiteId", getSession(getRequest()).getAttribute("selectedSiteId"));	
		   }catch(Exception ex){
			   ex.printStackTrace();
		   }
		return SUCCESS;
	   }
	
	private Map<Long,List<HLAPojo>> setHlaMapBYEntityId(Long entityId,Long entityType) throws Exception{
		Map<Long,List<HLAPojo>> mapHla = new TreeMap<Long, List<HLAPojo>>();
		List<HLAPojo> wholeHlaList = null;
		if(entityId!=null && entityType!=null){
		  wholeHlaList = cbuController.getHlasByCord(entityId,entityType, null);		 	  
		}
		if(wholeHlaList!=null && wholeHlaList.size()>0){
			for(HLAPojo h : wholeHlaList){
				if(h.getFkSource()!=null){
					if(mapHla.containsKey(h.getFkSource())){
						List<HLAPojo> listH = mapHla.get(h.getFkSource());
						listH.add(h);
					}else{
						List<HLAPojo> listH = new ArrayList<HLAPojo>();
						listH.add(h);
						mapHla.put(h.getFkSource(), listH);
					}
				}
			}
		}
		return mapHla;
	}
	
	public List<BestHLAPojo> setBestHlaByEntityType(Long entityType,Long entityId) throws Exception{
		List<BestHLAPojo> bestHLAPojos = new ArrayList<BestHLAPojo>();
		bestHLAPojos = cbuController.getBestHla(entityType,entityId);		
	 return bestHLAPojos;		
   }
	
	public Map<Long,List<HLAPojo>> setHlaMapForAntigenReport(List<HLAPojo> hlas){
		List<HLAPojo> bhlas = new ArrayList<HLAPojo>();
		Map<Long,List<HLAPojo>> bHlaMap = new LinkedHashMap<Long, List<HLAPojo>>();
		if(hlas != null && hlas.size()>0){
			for(HLAPojo h : hlas){
				Long hlaOrderSeq = h.getHlaOrderSeq();
				if(bHlaMap.containsKey(hlaOrderSeq)){
						bhlas = bHlaMap.get(hlaOrderSeq);
						bhlas.add(h);
					}else{
						bhlas = new ArrayList<HLAPojo>();
						bhlas.add(h);
						if(hlaOrderSeq!=null){
						  bHlaMap.put(hlaOrderSeq, bhlas);
						}
				}
			}
				
			
		}			
		return bHlaMap;			
	}
	
	/**
	 * This Method Use to create CSV Document Content
	 */
	private static enum QueryBuilderResolver{
		IDSUMMARYRPT ,ANTIGENRPT,LICENSELGBLERPT,QUICKSUMMARYRPT;		
	}
	
	public StringBuilder generateAntigenRepCSVContent(List<Object> cdrObjects, String reportName){
		     StringBuilder content= new StringBuilder("");		
		     // Generate Report Body
		     content= generateRepBody(cdrObjects,content,reportName);
		     return content;
	}    
	
	@SuppressWarnings("unchecked")
	private StringBuilder generateRepBody(List<Object> cdrObjects,StringBuilder content,String reportName){
		
		switch(QueryBuilderResolver.valueOf(reportName.toUpperCase())){
		          case IDSUMMARYRPT:
		        	  
		        	  content.append(isMultipleCBBSelected==true?getText("garuda.queryBuilder.label.cbbid")+",":"").append(getText("garuda.cbuentry.label.registrycbuid")+",").append(getText("garuda.cbuentry.label.localcbuid")+",").append(getText("garuda.cdrcbuview.label.id_on_bag")+",").append(getText("garuda.shippedcbu.report.label.isbt")+",").append(getText("garuda.cdrcbuview.label.historic_cbu_lcl_id")+",").append(getText("garuda.cbuentry.label.registrymaternalid")+",")
		        	  .append(getText("garuda.cbuentry.label.localmaternalid")+",").append(NEW_LINE);
		                  
		        	  for(Object obj:cdrObjects){
			        		
	                	  if(obj!=null){
	                	     Object[] objArr = (Object[])obj;
	                	        for(int i=1; i< objArr.length; i++){
	                	        	switch(i){
	                	        	case 1:
	                	        		
	                	        		content.append(isMultipleCBBSelected == true?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?",":"\""+objArr[i].toString()+"\",":"");
	                	        		break;
	                	        	/*case 9:
	                	        		
	                	        	   content.append(isWorkflowActivitySelected==true?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?",":"\""+objArr[i].toString()+"\",":"");
	                	        	   	break;
	                	        	case 10:
	                	        		String ct="";
	                	        		if(objArr[9]!=null && objArr[9].toString().trim().equals(ORDER_TYPE_CT_SHIP)){
	                	        			if(objArr[i]!=null && !StringUtils.isEmpty(objArr[i].toString())){
	                  	        				ct=objArr[i].toString();
	                	        			//content.append(objArr[9]!=null && objArr[9].equals(ORDER_TYPE_CT_SHIP)?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"\"\", \"\" ":objArr[i].toString()+", \"\" ":(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"\"\", \"\" ":"\"\", "+objArr[i].toString()).append(", ");
	                  	        				content.append("\"").append(ct).append("\",");
	                	        			}else{
	                	        				content.append(",");
	                	        			}
	                	        		}else{
	                	        			content.append(",");
	                	        		}
	                	        	    break;
	                	        		case 11:
	                	        			String or="";
		                	        		if(objArr[9]!=null && objArr[9].toString().trim().equals(OR)){
		                	        			if(objArr[i]!=null && !StringUtils.isEmpty(objArr[i].toString())){
		                  	        				or=objArr[i].toString();
		                	        			//content.append(objArr[9]!=null && objArr[9].equals(ORDER_TYPE_CT_SHIP)?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"\"\", \"\" ":objArr[i].toString()+", \"\" ":(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"\"\", \"\" ":"\"\", "+objArr[i].toString()).append(", ");
		                  	        				content.append("\"").append(or).append("\",");
		                	        			}else{
		                	        				content.append(",");
		                	        			}
		                	        		}else{
		                	        			content.append(",");
		                	        		}
	                	        		break;
	                	        	     
	                	        	case 12:          	        	
	                	        		content.append(isWorkflowActivitySelected==true?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?",":"\""+objArr[i].toString()+"\",":"");
	                	        	    break;
	                	        	case 13:
	                	        		break;
	                	        	case 14:
	                	        		break;*/
	                	        	default:
	                	        			content.append((objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"":"\""+objArr[i].toString()+"\"").append(",");
	                	        	}
	                	        	content.append(objArr.length==i+1?NEW_LINE:"");
	                	       }           		  
	                	  }
	                  }	
		                  break;
		          case  ANTIGENRPT:
		        	   content.append(isMultipleCBBSelected==true?"\"CBB\",":"").append("\"CBU Registry ID\",").append("\"CBU Local ID\",").append("\"Unique Product Identity on Bag\",").append("\"ISBT\",").append("\"Historic Local CBU ID\",").append("\"Maternal Registry ID\",")
			          .append("\"Maternal Local ID\",").append("\"Active for Patient (ID)\",").append("\"CBU Collection Date\",").append("\"Type\",").append("\"Typing Date\",").append("\"Received Date\",").append("\"Source\",").append("\"A Type1\",").append("\"A Type2\",")
			          .append("\"B Type1\",").append("\"B Type2\",").append("\"C Type1\",").append("\"C Type2\",").append("\"DRB1 Type1\",").append("\"DRB1 Type2\",").append("\"DRB3 Type1\",").append("\"DRB3 Type2\",")
			          .append("\"DRB4 Type1\",").append("\"DRB4 Type2\",").append("\"DRB5 Type1\",").append("\"DRB5 Type2\",").append("\"DQB1 Type1\",").append("\"DQB1 Type2\",").append("\"DPB1 Type1\",").append("\"DPB1 Type2\",").append(NEW_LINE);
		        	   
		        	   String HLA_A=getCodeListDescByTypeAndSubtype(HLA_LOCUS, HLA_A_ST);   
		        	    String HLA_B=getCodeListDescByTypeAndSubtype(HLA_LOCUS, HLA_B_ST);
		        	    String HLA_C=getCodeListDescByTypeAndSubtype(HLA_LOCUS, HLA_C_ST);
		        	    String HLA_DRB1=getCodeListDescByTypeAndSubtype(HLA_LOCUS, HLA_DRB1_ST);
		        	    String HLA_DRB3=getCodeListDescByTypeAndSubtype(HLA_LOCUS, HLA_DRB3_ST);
		        	    String HLA_DRB4=getCodeListDescByTypeAndSubtype(HLA_LOCUS, HLA_DRB4_ST);
		        	    String HLA_DRB5=getCodeListDescByTypeAndSubtype(HLA_LOCUS, HLA_DRB5_ST);
		        	    String HLA_DQB1=getCodeListDescByTypeAndSubtype(HLA_LOCUS, HLA_DQB1_ST);
		        	    String HLA_DPB1=getCodeListDescByTypeAndSubtype(HLA_LOCUS, HLA_DPB1_ST);

		        	  ArrayList<String> keys=new ArrayList<String>(30);
		        	  if(isMultipleCBBSelected==true)
		        	    keys.add("SITE_ID");		        	  
		        	    keys.add("REGISTRY_ID");
		        	    keys.add("CORD_LOCAL_CBU_ID");
		        	    keys.add("CORD_ID_NUMBER_ON_CBU_BAG");
		        	    keys.add("CORD_ISBI_DIN_CODE");
		        	    keys.add("CORD_HISTORIC_CBU_ID");
		        	    keys.add("REGISTRY_MATERNAL_ID");
		        	    keys.add("MATERNAL_LOCAL_ID");
		        	    keys.add("PATIENT_ID");
		        	    keys.add("CBU_COLLECTION_DATE");
		        	    keys.add("TYPE");
		        	    keys.add("HLA_TYPING_DATE");
		        	    keys.add("HLA_RECEIVED_DATE");
		        	    keys.add("HLA_USER");
		        	    keys.add("A TYPE1");
		        	    keys.add("A TYPE2");
		        	    keys.add("B TYPE1");
		        	    keys.add("B TYPE2");
		        	    keys.add("C TYPE1");
		        	    keys.add("C TYPE2");
		        	    keys.add("DRB1 TYPE1");
						keys.add("DRB1 TYPE2");
						keys.add("DRB3 TYPE1");
						keys.add("DRB3 TYPE2");
						keys.add("DRB4 TYPE1");
						keys.add("DRB4 TYPE2");
						keys.add("DRB5 TYPE1");
						keys.add("DRB5 TYPE2");
						keys.add("DQB1 TYPE1");
						keys.add("DQB1 TYPE2");
						keys.add("DPB1 TYPE1");
						keys.add("DPB1 TYPE2");
  
		        	  String regId=null;
		        	  String patientId=null;

		        	  ArrayList lst;
		        	  //ArrayList templst;//for Summary of Hla
		        	  HashMap<String, Object> hmp;
		        	  HashMap<String, Object> summHLA;
		        	  int columnCount= (isMultipleCBBSelected==true?32:31);
		        	  //int defaultHeaderCount=isMultipleCBBSelected==true?9:8;
		        	  boolean firstRowStatus=false;
		        	  for (int j = 0; j < cdrObjects.size(); j++) { 
	            
		                      regId=(((HashMap) cdrObjects.get(j)).get("REGISTRY_ID")!=null?((HashMap) cdrObjects.get(j)).get("REGISTRY_ID").toString():"");
		                      patientId= (((HashMap) cdrObjects.get(j)).get("PATIENT_ID")!=null?((HashMap) cdrObjects.get(j)).get("PATIENT_ID").toString():"");
		                      
		                      //lastCell=lastCellPostion; 
		                      lst=new ArrayList();
		                      hmp=new HashMap<String, Object>();
		                      summHLA= new HashMap<String, Object>();
		                   			 outer: for(int i=j;i<cdrObjects.size();i++){
		                   				if(regId.equals(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID")) && (patientId.equals((((HashMap) cdrObjects.get(j)).get("PATIENT_ID")!=null?((HashMap) cdrObjects.get(j)).get("PATIENT_ID").toString():"")))){
		                   					if("0".equals(((HashMap) cdrObjects.get(i)).get("CBHLA").toString())){//summary of HLA
		                   						if(summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID")) !=null){
		                   							HashMap o= (HashMap) cdrObjects.get(i);
		                   							
		                   									if(HLA_A.equals(o.get("FK_HLA_CODE_ID"))){
		                   										((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("A TYPE1", o.get("HLA_TYPE1"));
		                   										((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("A TYPE2", o.get("HLA_TYPE2"));
		                   									} else if(HLA_B.equals(o.get("FK_HLA_CODE_ID"))){
		                              	        				((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("B TYPE1", o.get("HLA_TYPE1"));
		                              	        				((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("B TYPE2", o.get("HLA_TYPE2"));
		                   									}else if(HLA_C.equals(o.get("FK_HLA_CODE_ID"))){
		                             	        		    	((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("C TYPE1", o.get("HLA_TYPE1"));
		                             	        		    	((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("C TYPE2", o.get("HLA_TYPE2"));
		                   									}else if(HLA_DRB1.equals(o.get("FK_HLA_CODE_ID"))){
		                              	        					((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("DRB1 TYPE1", o.get("HLA_TYPE1"));
		                              	        					((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("DRB1 TYPE2", o.get("HLA_TYPE2"));
		                                	        		}else if(HLA_DRB3.equals(o.get("FK_HLA_CODE_ID"))){
		                                	        				((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("DRB3 TYPE1", o.get("HLA_TYPE1"));
		                                	        				((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("DRB3 TYPE2", o.get("HLA_TYPE2"));
		                                	        		}else if(HLA_DRB4.equals(o.get("FK_HLA_CODE_ID"))){
		                                	        				((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("DRB4 TYPE1", o.get("HLA_TYPE1"));
	                                	        					((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("DRB4 TYPE2", o.get("HLA_TYPE2"));
		                                	        		}else if(HLA_DRB5.equals(o.get("FK_HLA_CODE_ID"))){
		                                	        				((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("DRB5 TYPE1", o.get("HLA_TYPE1"));
	                                	        					((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("DRB5 TYPE2", o.get("HLA_TYPE2"));
		                                	        		}else if(HLA_DQB1.equals(o.get("FK_HLA_CODE_ID"))){
		                                	        				((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("DQB1 TYPE1", o.get("HLA_TYPE1"));
		                                	        				((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("DQB1 TYPE2", o.get("HLA_TYPE2"));
		                                	        		}else if(HLA_DPB1.equals(o.get("FK_HLA_CODE_ID"))){
		                                	        				((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("DPB1 TYPE1", o.get("HLA_TYPE1"));
		                                	        				((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("DPB1 TYPE2", o.get("HLA_TYPE2"));
		                                	        		}
		                   						}else{

		                   							if(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID")!=null){
		                   								HashMap o= (HashMap) cdrObjects.get(i);
		                   									if(HLA_A.equals(o.get("FK_HLA_CODE_ID"))){
		                   							    		o.put("A TYPE1", o.get("HLA_TYPE1"));
		                   							    		o.put("A TYPE2", o.get("HLA_TYPE2"));
		                   									}else if(HLA_B.equals(o.get("FK_HLA_CODE_ID"))){
		      	                          	        			o.put("B TYPE1", o.get("HLA_TYPE1"));
		      	                          	        			o.put("B TYPE2", o.get("HLA_TYPE2"));
		      	                       	        		    }else if(HLA_C.equals(o.get("FK_HLA_CODE_ID"))){
		      	                       	        			   o.put("C TYPE1", o.get("HLA_TYPE1"));
		      	                       	        			   o.put("C TYPE2", o.get("HLA_TYPE2"));
		      	                         	        		}else if(HLA_DRB1.equals(o.get("FK_HLA_CODE_ID"))){
		      	                        	        			o.put("DRB1 TYPE1", o.get("HLA_TYPE1"));
		      	                        	        			o.put("DRB1 TYPE2", o.get("HLA_TYPE2"));
		      	                          	        		}else if(HLA_DRB3.equals(o.get("FK_HLA_CODE_ID"))){
		      	                        	        			o.put("DRB3 TYPE1", o.get("HLA_TYPE1"));
		      	                        	        			o.put("DRB3 TYPE2", o.get("HLA_TYPE2"));
		      	                          	        		}else if(HLA_DRB4.equals(o.get("FK_HLA_CODE_ID"))){
		      	                        	        			o.put("DRB4 TYPE1", o.get("HLA_TYPE1"));
		      	                        	        			o.put("DRB4 TYPE2", o.get("HLA_TYPE2"));
		      	                          	        		}else if(HLA_DRB5.equals(o.get("FK_HLA_CODE_ID"))){
		      	                        	        			o.put("DRB5 TYPE1", o.get("HLA_TYPE1"));
		      	                        	        			o.put("DRB5 TYPE2", o.get("HLA_TYPE2"));
		      	                          	        		}else if(HLA_DQB1.equals(o.get("FK_HLA_CODE_ID"))){
		      	                        	        			o.put("DQB1 TYPE1", o.get("HLA_TYPE1"));
		      	                        	        			o.put("DQB1 TYPE2", o.get("HLA_TYPE2"));
		      	                          	        		}else if(HLA_DPB1.equals(o.get("FK_HLA_CODE_ID"))){
		      	                        	        			o.put("DPB1 TYPE1", o.get("HLA_TYPE1"));
		      	                        	        			o.put("DPB1 TYPE2", o.get("HLA_TYPE2"));
		      	                          	        		}		                   								
		      	                       	        		   summHLA.put(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString(), cdrObjects.get(i));	  
		                   							}
		                   							
		                   						}
		                   						                 						
		                   					}else{//History
		                   						if(hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ")!=null?((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString():"")!=null){
		                   							HashMap o= (HashMap) cdrObjects.get(i);
		                   							
		                   									if(HLA_A.equals(o.get("FK_HLA_CODE_ID"))){
		                   										((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("A TYPE1", o.get("HLA_TYPE1"));
		                   										((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("A TYPE2", o.get("HLA_TYPE2"));  
		                   									}else if(HLA_B.equals(o.get("FK_HLA_CODE_ID"))){
		                              	        				((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("B TYPE1", o.get("HLA_TYPE1"));
		                              	        				((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("B TYPE2", o.get("HLA_TYPE2"));
		                                	        		}else if(HLA_C.equals(o.get("FK_HLA_CODE_ID"))){
		                             	        			   ((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("C TYPE1", o.get("HLA_TYPE1"));
		                             	        				((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("C TYPE2", o.get("HLA_TYPE2"));
		                                	        		}else if(HLA_DRB1.equals(o.get("FK_HLA_CODE_ID"))){
		                              	        				((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("DRB1 TYPE1", o.get("HLA_TYPE1"));
		                              	        				((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("DRB1 TYPE2", o.get("HLA_TYPE2"));
		                                	        		}else if(HLA_DRB3.equals(o.get("FK_HLA_CODE_ID"))){
		                                	        			((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("DRB3 TYPE1", o.get("HLA_TYPE1"));
		                                	        			((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("DRB3 TYPE2", o.get("HLA_TYPE2"));
		                                	        		}else if(HLA_DRB4.equals(o.get("FK_HLA_CODE_ID"))){
		                                	        			((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("DRB4 TYPE1", o.get("HLA_TYPE1"));
		                                	        			((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("DRB4 TYPE2", o.get("HLA_TYPE2"));
		                                	        		}else if(HLA_DRB5.equals(o.get("FK_HLA_CODE_ID"))){
		                                	        			((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("DRB5 TYPE1", o.get("HLA_TYPE1"));
		                                	        			((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("DRB5 TYPE2", o.get("HLA_TYPE2"));
		                                	        		}else if(HLA_DQB1.equals(o.get("FK_HLA_CODE_ID"))){
		                                	        			((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("DQB1 TYPE1", o.get("HLA_TYPE1"));
		                                	        			((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("DQB1 TYPE2", o.get("HLA_TYPE2"));
		                                	        		}else if(HLA_DPB1.equals(o.get("FK_HLA_CODE_ID"))){
		                                	        			((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("DPB1 TYPE1", o.get("HLA_TYPE1"));
		                                	        			((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("DPB1 TYPE2", o.get("HLA_TYPE2"));
		                                	        		}
		                   							
		                   						}else{ 
		      	             							if(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ")!=null /*&& regId.equals(((HashMap) cdrObjects.get(i+1)).get("REGISTRY_ID"))*/){
		      	             								HashMap o= (HashMap) cdrObjects.get(i);
		      	             									if(HLA_A.equals(o.get("FK_HLA_CODE_ID"))){
		      	             									  	o.put("A TYPE1", o.get("HLA_TYPE1"));	
		      	             									  	o.put("A TYPE2", o.get("HLA_TYPE2"));		
		      	             									}else if(HLA_B.equals(o.get("FK_HLA_CODE_ID"))){
		      		                          	        			o.put("B TYPE1", o.get("HLA_TYPE1"));
		      		                          	        		    o.put("B TYPE2", o.get("HLA_TYPE2"));	
		      		                       	        		    }else if(HLA_C.equals(o.get("FK_HLA_CODE_ID"))){
		      			                       	        			o.put("C TYPE1", o.get("HLA_TYPE1"));
		      			                       	        			o.put("C TYPE2", o.get("HLA_TYPE2"));
		      		                         	        		}else if(HLA_DRB1.equals(o.get("FK_HLA_CODE_ID"))){
		      		                        	        			o.put("DRB1 TYPE1", o.get("HLA_TYPE1"));
		      		                        	        			o.put("DRB1 TYPE2", o.get("HLA_TYPE2"));
		      		                          	        		}else if(HLA_DRB3.equals(o.get("FK_HLA_CODE_ID"))){
		      		                        	        			o.put("DRB3 TYPE1", o.get("HLA_TYPE1"));
		      		                        	        			o.put("DRB3 TYPE2", o.get("HLA_TYPE2"));
		      		                          	        		}else if(HLA_DRB4.equals(o.get("FK_HLA_CODE_ID"))){
		      		                        	        			o.put("DRB4 TYPE1", o.get("HLA_TYPE1"));
		      		                        	        			o.put("DRB4 TYPE2", o.get("HLA_TYPE2"));
		      		                          	        		}else if(HLA_DRB5.equals(o.get("FK_HLA_CODE_ID"))){
		      		                        	        			o.put("DRB5 TYPE1", o.get("HLA_TYPE1"));
		      		                        	        			o.put("DRB5 TYPE2", o.get("HLA_TYPE2"));
		      		                          	        		}else if(HLA_DQB1.equals(o.get("FK_HLA_CODE_ID"))){
		      		                        	        			o.put("DQB1 TYPE1", o.get("HLA_TYPE1"));
		      		                        	        			o.put("DQB1 TYPE2", o.get("HLA_TYPE2"));
		      		                          	        		}else if(HLA_DPB1.equals(o.get("FK_HLA_CODE_ID"))){
		      		                        	        			o.put("DPB1 TYPE1", o.get("HLA_TYPE1"));
		      		                        	        			o.put("DPB1 TYPE2", o.get("HLA_TYPE2"));
		      		                          	        		}
		      	             							      hmp.put(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString(), cdrObjects.get(i));	             							
		                   							}
		                   						}
		                   					}
		                   					j++;
		                   				}else{break outer;}    				  
		                   			  }
		                                 
		      			                if(summHLA.size()>0 || hmp.size()>0){
		      			                	 j--;
		      			 					firstRowStatus=false;			 					
		      			 					//Summary of hla
		      			 					for(Map.Entry<String, Object> entry: summHLA.entrySet()) {             				       
		      			 				        //System.out.println(entry.getValue());
		      			 				       lst.add(entry.getValue());
		      			 				    }   
		      			 					// History
		      			 					for(Map.Entry<String, Object> entry: hmp.entrySet()) {             				       
		      			 				        //System.out.println(entry.getValue());
		      			 				       lst.add(entry.getValue());
		      			 				    }  
		      			 					if(lst.size()==1){// to manage history if no avalable in list
		      			 						HashMap<String, Object> o=new HashMap<String, Object>();
		      			 						o.put("CBHLA", "History");
		      			 						lst.add(o);
		      			 					} 					
		      			 				}         
		                   		
		                            for(int i=0;i<lst.size();i++){ 		                            	
		                            	if(i==1){
		                            		content.append(NEW_LINE);
		                            	}
		                            HashMap o = (HashMap) lst.get(i);	
		                	        for(short k=0; k< columnCount; k++){
		                	        	 
		                	        	 String cellVal=(String)(keys.get(k)!=null?keys.get(k):"");
		               	        		if(cellVal!=null && firstRowStatus && (cellVal.equals("SITE_ID") 
		               	        				                           || cellVal.equals("REGISTRY_ID")
		               	        				                           || cellVal.equals("CORD_LOCAL_CBU_ID")
		               	        				                           || cellVal.equals("CORD_ID_NUMBER_ON_CBU_BAG")
		               	        				                           || cellVal.equals("CORD_ISBI_DIN_CODE")
		               	        				                           || cellVal.equals("CORD_HISTORIC_CBU_ID")
		               	        				                           || cellVal.equals("REGISTRY_MATERNAL_ID")
		               	        				                           || cellVal.equals("MATERNAL_LOCAL_ID")
		               	        				                           || cellVal.equals("PATIENT_ID")
		               	        				                           || cellVal.equals("CBU_COLLECTION_DATE"))){
		               	        			
		               	        			content.append("\"\"");
		               	        		}else if(!firstRowStatus && cellVal.equals("PATIENT_ID")){
		               	        			content.append("\"").append(o.get(keys.get(k)!=null?keys.get(k).toString():"")!=null?o.get(keys.get(k)!=null?keys.get(k).toString():""):"").append("\"");
		               	        			/*if(o.get("CORD_NMDP_STATUS")!=null && (getCbuStatusByCode(AC).getPkcbustatus().equals(((BigDecimal)o.get("CORD_NMDP_STATUS")).longValue()))){
		               	        				content.append("\"").append(o.get(keys.get(k)!=null?keys.get(k).toString():"")!=null?o.get(keys.get(k)!=null?keys.get(k).toString():""):"").append("\"");
		               	        			}else{
		               	        				content.append("\"").append("").append("\"");
		               	        			}*/
		               	        		}else if(cellVal!=null && cellVal.equals("TYPE")){          	        			
		               	        			content.append("\"").append((o.get("CBHLA")!=null && ("0").equals(o.get("CBHLA").toString())?"Summary of HLA":"History")).append("\"");

		               	        		}else if(!firstRowStatus && (cellVal.equals("HLA_TYPING_DATE") || cellVal.equals("HLA_RECEIVED_DATE") || cellVal.equals("HLA_USER"))){
		               	        			
		               	        			content.append("\"").append("").append("\"");
		          	        			}else{
		               	        			content.append("\"").append(o.get(keys.get(k)!=null?keys.get(k).toString():"")!=null?o.get(keys.get(k)!=null?keys.get(k).toString():""):"").append("\"");          	        		
		               	        		}
		               	        		content.append(",");
		                	        	}
		                	        	content.append(NEW_LINE);
		                	        	firstRowStatus=true;
		                	         }
		                	       
		                	     /*  for (short k=0; k< columnCount; k++) {  
		   	                    	content.append("");               
		   	                    } */
		                	       content.append(NEW_LINE);
		        	    }
		        	  break;
		          case LICENSELGBLERPT:
		        	  
		        	   content.append(isMultipleCBBSelected==true?getText("garuda.queryBuilder.label.cbbid")+",":"").append(getText("garuda.cbuentry.label.registrycbuid")+",").append(getText("garuda.cbuentry.label.localcbuid")+",").append(getText("garuda.cbuentry.label.idbag")+",").append(getText("garuda.queryBuilder.label.cbucollectiondate")+",")
			        	  .append(getText("garuda.queryBuilder.nmdpregistrationDates")+",").append(getText("garuda.cbuentry.label.cbu_status")+",").append(getText("garuda.clinicalnote.label.reason")+",").append(getText("garuda.cdrcbuview.label.licensure_status")+",").append(getText("garuda.evaluHistory.report.unlicensedReason")+",").append(getText("garuda.cdrcbuview.label.eligibility_determination")+",").append(getText("garuda.queryBuilder.ineligiblereason")+",").append(getText("garuda.queryBuilder.incompletereason")+",")
			        	  .append(getText("garuda.queryBuilder.babyBirthDate")+",").append(getText("garuda.queryBuilder.race")+",").append(getText("garuda.cordentry.label.nmdpbroadrace")+",").append(getText("garuda.pendingorderpage.label.funded")+",").append(getText("garuda.queryBuilder.fundingcategory")+",").append(getText("garuda.cbuentry.label.bcresult")+",").append(getText("garuda.cbuentry.label.fcresult")+",").append(getText("garuda.minimumcriteria.label.hemoglobin")+",").append(getText("garuda.cbbprocedures.label.processingprocedure")+",")
			        	  .append(getText("garuda.queryBuilder.label.totalcbunucleatedcount")+",").append(getText("garuda.queryBuilder.label.totalcbunucleatedcountun")+",").append(getText("garuda.queryBuilder.totalcd34")+",").append(getText("garuda.queryBuilder.viabilitycount")+",").append(getText("garuda.common.lable.viability")+",").append(getText("garuda.productinsert.label.finalProdVol")+",").append(getText("garuda.idsReport.ctshipdate")+",").append(getText("garuda.idsReport.orshipdate")+",")
			        	  .append(getText("garuda.detailreport.cbuhistory.requesttype")+",").append(getText("garuda.idsReport.reqdate")+",").append(NEW_LINE);
		        	 
		        	 /* for(Object obj:cdrObjects){
	                	  if(obj!=null){
	                	     Object[] objArr = (Object[])obj;
	                	        for(int i=1; i< objArr.length; i++){
	                	        	switch(i){
	                	        	case 1:
	                	        		content.append(isMultipleCBBSelected == true?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"\"\", ":objArr[i].toString()+", ":"");
	                	        		break;
	                	        	case 9:
	                	        	   content.append(unlicensed_flag==true?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"\"\", ":objArr[i].toString()+", ":"");
	                	        	   	break;
	                	        	case 11:		                	        		
	                	        	    content.append(inelig_incomple_flag==true?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"\"\", ":objArr[i].toString()+", ":"");
	                	        	    break;
	                	        	case 25:
	                	        		content.append(objArr[26]!=null && objArr[26].equals(ORDER_TYPE_CT_SHIP)?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"\"\", \"\" ":objArr[i].toString()+", \"\" ":(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"\"\", \"\" ":"\"\", "+objArr[i].toString()).append(", ");
	                	        	    break;
	                	        	case 26:		                	        		
	                	        	    content.append("");
	                	        	    break;
	                	        	case 27:		                	        		
	                	        	    content.append(isWorkflowActivitySelected==true?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"\"\", ":objArr[i].toString()+", ":"");
	                	        	    break;
	                	        	case 28:		                	        		
	                	        	    content.append(isWorkflowActivitySelected==true?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"\"\", ":objArr[i].toString()+", ":"");
	                	        	    break;
	                	        	case 29:		                	        		
	                	        	    content.append("");
	                	        	    break;
	                	        	case 31:		                	        		
	                	        	    content.append("");
	                	        	    break;
	                	        	default:
	                	        	    content.append((objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"\"\" ":objArr[i].toString()).append(", ");
	                	        	}
	                	        	content.append(objArr.length==i+1?NEW_LINE:"");
	                	       }           		  
	                	  }
	                  }	*/
		        		  for(Object obj:cdrObjects){
		                	  if(obj!=null){
		                	     Object[] objArr = (Object[])obj;
		                	        for(int i=1; i< objArr.length; i++){
		                	        	switch(i){
		                	        	case 1:
		                	        		content.append(isMultipleCBBSelected == true?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"":"\""+objArr[i].toString()+"\",":"");
		                	        		break;
		                	        	/*case 10:
		                	        	   content.append(unlicensed_flag==true?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?",":"\""+objArr[i].toString()+"\",":"");
		                	        	   	break;*/
		                	        	case 12:		                	        		
		                	        	    //content.append(inelig_incomple_flag==true?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"\"\", ":objArr[i].toString()+", ":"");
		                	        		if(objArr[11]!=null && objArr[11].toString().equals(INELIGIBLE_STR) && objArr[i]!=null && !objArr[i].toString().equals("")){
		                	        			content.append("\"").append(objArr[i].toString()).append("\",");
		                	        		}else{
	                	        				content.append(",");
	                	        			}
		                	        		if(objArr[11]!=null && objArr[11].toString().equals(INCOMPLETE_STR) && objArr[i]!=null && !objArr[i].toString().equals("")){
		                	        			content.append("\"").append(objArr[i].toString()).append("\",");
		                	        		}else{
		                	        			//log.debug("Inside adding comma incomple reason::::::::::::"+objArr[i]);
	                	        				content.append(",");
	                	        			}
	                	        		/*else if((qinelig_flag==true && qincomp_flag==false)|| (qinelig_flag==false && qincomp_flag==true)){		                	        			
	                	        			
	                	        			if(qinelig_flag!=null && qinelig_flag==true && objArr[11]!=null){
		                	        			content.append("\"").append(objArr[i].toString()).append("\",");
		                	        		}
	                	        			if(qincomp_flag!=null && qincomp_flag==true && objArr[11]!=null){
		                	        			content.append("\"").append(objArr[i].toString()).append("\",");
		                	        		}
	                	        		}*/
	                	        	    break;
		                	        	case 28:
		                	        		String ct="";
		                	        		if(objArr[30]!=null && objArr[30].toString().trim().equals(ORDER_TYPE_CT_SHIP)){
		                	        			if(objArr[i]!=null && !StringUtils.isEmpty(objArr[i].toString())){
		                  	        				ct=objArr[i].toString();
		                	        			//content.append(objArr[9]!=null && objArr[9].equals(ORDER_TYPE_CT_SHIP)?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"\"\", \"\" ":objArr[i].toString()+", \"\" ":(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"\"\", \"\" ":"\"\", "+objArr[i].toString()).append(", ");
		                  	        				content.append("\"").append(ct).append("\",");
		                	        			}else{
		                	        				content.append(",");
		                	        			}
		                	        		}else{
		                	        			content.append(",");
		                	        		}
		               	        		    break;
		                  	        	case 29:
		                  	        		String or="";
		                	        		if(objArr[30]!=null && objArr[30].toString().trim().equals(OR)){
		                	        			if(objArr[i]!=null && !StringUtils.isEmpty(objArr[i].toString())){
		                  	        				or=objArr[i].toString();
		                	        			//content.append(objArr[9]!=null && objArr[9].equals(ORDER_TYPE_CT_SHIP)?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"\"\", \"\" ":objArr[i].toString()+", \"\" ":(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"\"\", \"\" ":"\"\", "+objArr[i].toString()).append(", ");
		                  	        				content.append("\"").append(or).append("\",");
		                	        			}else{
		                	        				content.append(",");
		                	        			}
		                	        		}else{
		                	        			content.append(",");
		                	        		}
		                  	        		break;
		                  	        /*	case 30:		                	        		
		                  	        		 content.append(isWorkflowActivitySelected==true?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?",":"\""+objArr[i].toString()+"\",":"");
		                  	        	    break;
		                  	        	case 31:		                	        		
		                  	        		 content.append(isWorkflowActivitySelected==true?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?",":"\""+objArr[i].toString()+"\",":"");
		                  	        	    break;*/
		                  	        	case 32:
		                  	        		break;
		                  	        	case 33:
		                  	        		break;
		                  	        	case 34:
		                  	        		break;
		                  	        	case 35:
		                  	        		break;
		                  	        	case 36:
		                  	        		break;
		                  	        	case 37:
		                  	        		break;
		                  	        	case 38:
		                  	        		break;
		                	        	default:
		                	        	    content.append((objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"":"\""+objArr[i].toString()+"\"").append(",");
		                	        	}
		                	        	content.append(objArr.length==i+1?NEW_LINE:"");
		                	       }           		  
		                	  }
		                  }	
		        	  break;
		          case QUICKSUMMARYRPT:
		        	  
		        	  content.append(isMultipleCBBSelected==true?getText("garuda.queryBuilder.label.cbbid")+",":"").append(getText("garuda.cbuentry.label.registrycbuid")+",").append(getText("garuda.cbuentry.label.localcbuid")+",").append(getText("garuda.cbuentry.label.idbag")+",").append(getText("garuda.shippedcbu.report.label.isbt")+",")
		        	  .append(getText("garuda.cbuentry.label.requestType")+",").append(getText("garuda.idsReport.reqdate")+",").append(getText("garuda.idsReport.orshipdate")+",").append(getText("garuda.shippedcbu.report.label.orinfuseddt")+",").append(getText("garuda.recipient&tcInfo.label.recipientid")+",").append(getText("garuda.shippedcbu.report.label.patientdiag")+",").append(getText("garuda.shippedcbu.report.label.tccode")+",").append(getText("garuda.shippedcbu.report.label.tcname")+",").append(NEW_LINE);
		        	  
		        	  for(Object obj:cdrObjects){
	                	  if(obj!=null){
	                	     Object[] objArr = (Object[])obj;
	                	        for(int i=1; i< objArr.length; i++){
	                	        	switch(i){
	                	        	case 1:
	                	        		content.append(isMultipleCBBSelected == true?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?",":"\""+objArr[i].toString()+"\",":"");
	                	        		break;
	                	        	case 6:		                	        		
	                	        	    content.append("");
	                	        	    break;
	                	        	
	                	        	case 7:		                	        		
	                	        	    content.append("");
	                	        	    break;
	                	        	case 8:		                	        		
	                	        	    content.append("");
	                	        	    break;
	                	        	/*case 9:
	                	        	   content.append(isWorkflowActivitySelected==true?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?",":"\""+objArr[i].toString()+"\",":"");
	                	        	   	break;	                	        	
	                	        	case 10:
	                	        		content.append(isWorkflowActivitySelected==true?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?",":"\""+objArr[i].toString()+"\",":"");
	                	        	    break;*/
	                	        	case 11:
	                	        		String or="";
	                  	        		if(objArr[9]!=null && objArr[9].toString().trim().equals(OR)){
	                  	        			
	                  	        			if(objArr[i]!=null && !StringUtils.isEmpty(objArr[i].toString())){
	                  	        				or=objArr[i].toString();
	                  	        				content.append("\"").append(or).append("\",");
	                  	        			}else{
		                	        			content.append(",");}	                  	        			
	                  	        		}else{
	                	        			content.append(",");}
	                  	        		
	                	        		break;
	                	        	case 17:		                	        		
	                  	        	    break;
	                  	        	case 18:		                	        		
	                  	        	    break;
	                  	        	case 19:		                	        		
	                  	        	    break;
	                	        	default:
	                	        	    content.append((objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"":"\""+objArr[i].toString()+"\"").append(",");
	                	        	}
	                	        	content.append(objArr.length==i+1?NEW_LINE:"");
	                	       }           		  
	                	  }
	                  }	
		        	  break;
		    }
		return content;		
	}
	
	@SuppressWarnings("unchecked")
	public String pdfReportsForQueryBuilder(){
		long st = System.currentTimeMillis();
		    UserJB user = null;
			try{
				
			    user = (UserJB)getRequest().getSession().getAttribute(VelosGarudaConstants.GARUDA_USER);
			  
			    Long userId = (long)user.getUserId();				
				request.setAttribute("userId", userId);
				
			}catch (Exception e) {
				e.printStackTrace();
			}
		   String result = "";
		   String orderBy="";
		   try{
			   
			   //System.out.println("\n\n\n\n\n\n\n\n\n\n\n\nvalue of sort type:::::::::::::::::::::::::::"+request.getParameter("sortTyp"));
			   //System.out.println("value of sort col:::::::::::::::::::::::::::"+request.getParameter("sortCol"));
			   //System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n");
			   
			   Map<String ,String> column=new HashMap<String, String>();
			   column.put("cbbid", "C.SITE_ID");
			   column.put("regid", "C.REGISTRY_ID");
			   column.put("loccbuid", "C.CORD_LOCAL_CBU_ID");
			   column.put("idonbag", "C.CORD_ID_NUMBER_ON_CBU_BAG");
			   column.put("isbt", "C.CORD_ISBI_DIN_CODE");
			   column.put("histcbuid", "C.CORD_HISTORIC_CBU_ID");
			   column.put("matregid", "C.REGISTRY_MATERNAL_ID");
			   column.put("matlocid", "C.MATERNAL_LOCAL_ID");
			   column.put("workflowCbuId", "10");
			   column.put("ctshipdate", "C.SHIP_DATE");
			   column.put("orshipdate", "C.SHIP_DATE");
			   column.put("reqdate", "C.ORDER_OPEN_DATE");
			   column.put("workflowCbuSumm", "10");
			   column.put("infuseddate", "C.INFUSION_DATE");
			   column.put("tccode", "C.TRANS_SITE_ID");
			   column.put("tcname", "C.TRANS_SITE_NAME");
			   column.put("patientid", "C.PATIENT_ID");
			   column.put("pdiagnosis", "C.CURRENT_DIAGNOSIS");
			   column.put("cbucollectdt", "C.SPEC_COLLECTION_DATE");
			   column.put("regdt", "C.CORD_REGISTRATION_DATE");
			   column.put("cbustat", "ERES.F_GET_CBU_STATUS(C.CORD_NMDP_STATUS)");
			   column.put("reason", "ERES.F_GET_CBU_STATUS(C.FK_CORD_CBU_STATUS)");
			   column.put("licstat", "ERES.f_codelst_desc(C.FK_CORD_CBU_LIC_STATUS)");
			   column.put("unlicreas", "c.UNLICENSED_DESC");
			   column.put("eligstat", "ERES.f_codelst_desc(C.fk_cord_cbu_eligible_status)");
			   column.put("inelireason", "c.INELIG_INCOMP_DESC");
			   column.put("incomreason", "c.INELIG_INCOMP_DESC");
			   column.put("babybirdt", "C.CORD_BABY_BIRTH_DATE");
			   column.put("race", "ERES.f_codelst_desc(C.NMDP_RACE_ID)");
			   column.put("racesumm", "c.RACE_DESC");
			   column.put("funded", "C.funded_cbu");
			   column.put("fundingcat", "ERES.f_codelst_desc(C.FUNDING_CATEGORY)");
			   column.put("bacterialres", "ERES.f_codelst_desc(C.FK_CORD_BACT_CUL_RESULT)");
			   column.put("fungalres", "ERES.f_codelst_desc(C.FK_CORD_FUNGAL_CUL_RESULT)");
			   column.put("hemoglobinopathy", "C.HEMOGLOBIN_SCRN");
			   column.put("procesingproc", "PROC.PROC_NAME");
			   column.put("totcbunucl", "Nucl_cell_count.resultdata");
			   column.put("totcbunuclun", "Nucl_cell_count_unknown.resultdata");
			   column.put("totcd34", "CD34_cell_count.resultdata");
			   column.put("viability", "Viability_count.resultdata");
			   column.put("viabmethod", "ERES.f_codelst_desc(Viability_Method.resultdata)");
			   column.put("finalprod", "Final_Prod_Vol.resultdata");
			   column.put("workflowCbuchar", "31");
			   
			   
			   
			   String t1="";
			   String t2="";
			   String kRegid="regid";
			   if(request.getParameter("sortCol")!=null && !request.getParameter("sortCol").toString().trim().equals("") && request.getParameter("sortTyp")!=null && !request.getParameter("sortTyp").toString().trim().equals("")){
				   t1=request.getParameter("sortCol").toString();
				   t2=request.getParameter("sortTyp").toString();
				   if(getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_QUICKSUMMARY_REPORT)){
				   if(t1.equals("cbbid")){
					   orderBy=" order by "+column.get(t1)+" "+t2+", C.REGISTRY_ID, DECODE(req_type,(SELECT CODELST_DESC FROM ERES.ER_CODELST WHERE CODELST_TYPE='order_type' AND CODELST_SUBTYP='OR'),request_dt) DESC nulls last";
				   }
				   else if(t1.equals("regid")){
					   orderBy=" order by "+column.get(t1)+" "+t2+", DECODE(req_type,(SELECT CODELST_DESC FROM ERES.ER_CODELST WHERE CODELST_TYPE='order_type' AND CODELST_SUBTYP='OR'),request_dt) DESC nulls last";
				   }
				   else{
					   orderBy=" order by "+column.get(t1)+" "+t2;
				   }
				   }
				   else if(getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_LICENSEANDELIGIBILTY_REPORT)){
					   if(t1.equals("cbbid")){
						   orderBy=" order by "+column.get(t1)+" "+t2+", C.REGISTRY_ID, C.ORDER_OPEN_DATE DESC nulls last";
					   }
					   else if(t1.equals("regid")){
						   orderBy=" order by "+column.get(t1)+" "+t2+", C.ORDER_OPEN_DATE DESC nulls last";
					   }
					   else{
						   orderBy=" order by "+column.get(t1)+" "+t2;
					   }
				   }
				   else{
					   if(t1.equals("cbbid")){
						   orderBy=" order by "+column.get(t1)+" "+t2+", C.REGISTRY_ID nulls last";
					   }
					   else{
						   orderBy=" order by "+column.get(t1)+" "+t2+" nulls last";
					   }
				   }
				   
				   
				   
			   }else{
				   if(getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_QUICKSUMMARY_REPORT)){
					   orderBy=" order by C.SITE_ID,"+column.get(kRegid)+" asc, DECODE(req_type,(SELECT CODELST_DESC FROM ERES.ER_CODELST WHERE CODELST_TYPE='order_type' AND CODELST_SUBTYP='OR'),request_dt) DESC nulls last";
				   }
				   else if(getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_LICENSEANDELIGIBILTY_REPORT)){
					   orderBy=" order by C.SITE_ID,"+column.get(kRegid)+" asc, C.ORDER_OPEN_DATE DESC nulls last";
				   }
				   else{
					   orderBy=" order by C.SITE_ID,"+column.get(kRegid)+" asc nulls last ";
				   }
				   
			   }
			   
			   
			   
			   
			   
			   
			    vdaController = new VDAController();
			   String params = "";
			   Integer count = 0;
			   String sqlHead = "";			  
			   if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_ANTIGEN_REPORT)){
	        	   sqlHead += "SELECT Distinct C.PK_CORD,C.SITE_ID,C.REGISTRY_ID,C.CORD_LOCAL_CBU_ID,C.CORD_ID_NUMBER_ON_CBU_BAG,C.CORD_ISBI_DIN_CODE,C.CORD_HISTORIC_CBU_ID,C.REGISTRY_MATERNAL_ID,C.MATERNAL_LOCAL_ID" +
				   " , C.PATIENT_ID ,"+
				   /*" CASE "+
		            " WHEN ERES.f_codelst_desc(C.order_type)='CT' "+
		            " AND C.order_sample_at_lab        ='Y'"+
		            " THEN 'CT - Sample at Lab' "+
		            " WHEN ERES.f_codelst_desc(C.order_type)='CT'"+
		            " AND C.order_sample_at_lab        ='N'"+
		            " THEN 'CT - Ship Sample' "+
		            " ELSE ERES.f_codelst_desc(C.order_type) "+*/
		           " TO_CHAR(C.SPEC_COLLECTION_DATE,'MM/DD/YYYY') as CBU_COLLECTION_DATE ,C.CORD_NMDP_STATUS as CORD_NMDP_STATUS " ;
	        	   sqlHead += getSession(getRequest()).getAttribute("queryBuilderHlaSql");
		           sqlHead += " ORDER BY C.SITE_ID,C.REGISTRY_ID";
			   }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_LICENSEANDELIGIBILTY_REPORT)){
				   sqlHead += "SELECT DISTINCT(C.PK_CORD), " +
				   "  C.site_id                                        AS cbb_id, " +
				   "  C.REGISTRY_ID                                    AS cbu_registry_id, " +
				   "  C.CORD_LOCAL_CBU_ID                              AS cbu_local_id, " +
				   "  C.CORD_ID_NUMBER_ON_CBU_BAG                      AS uniqueidonbag, " +
				   "  TO_CHAR(C.SPEC_COLLECTION_DATE, 'MM/DD/YYYY')   AS cbu_collection_date, " +
				   "  TO_CHAR(C.CORD_REGISTRATION_DATE, 'MM/DD/YYYY') AS nmdp_reg_date, " +
				   "  C.CORD_NMDP_STATUS                      		   As CBU_Status, " +
				   "  C.FK_CORD_CBU_STATUS                             AS Reason, " +				   
				   "  C.FK_CORD_CBU_LIC_STATUS                         AS Licensure_Status, " +
				   "  c.UNLICENSED_DESC, " +
				   "  C.fk_cord_cbu_eligible_status AS Elig_Deter, " +
				   "  c.INELIG_INCOMP_DESC, " +
				   "  TO_CHAR(C.CORD_BABY_BIRTH_DATE, 'MM/DD/YYYY') Baby_birth_date, " +
				   "  c.RACE_DESC                              AS Race, " +
				   "  C.NMDP_RACE_ID                           AS Race_Summary , " +
				   "  CASE "+
				   "  WHEN C.funded_cbu = '0'  THEN 'No'"+
				   "  When C.funded_cbu = '1'  THEN 'Yes'"+
				   "  END"+
				   "  as Funded, " +
				   "  C.FUNDING_CATEGORY, "+
				   "  C.FK_CORD_BACT_CUL_RESULT                AS Bact_cultr_rst, " +
				   "  C.FK_CORD_FUNGAL_CUL_RESULT              AS Fungal_cultr_rst, " +
				   "  C.HEMOGLOBIN_SCRN                        AS Hemoglobin_Testing, " +
				   "  PROC.PROC_NAME                           AS Processing_procedure , " +
				   "  Nucl_cell_count.resultdata               AS nucl_cell_countdata, " +
				   "  Nucl_cell_count_unknown.resultdata,"+
				   "  CD34_cell_count.resultdata               AS cd34_cell_countdata , " +
				   "  Viability_Method.resultdata               AS Viability_countdata, " +
				   "  Viability_Method.fkTestMethod              AS Viability_Methoddata, " +
				   "  Final_Prod_Vol.resultdata                AS Final_Prod_Voldata, " +
				   "  TO_CHAR(C.SHIP_DATE,'MM/DD/YYYY')       AS SHIP_DATE1, " +
				   "  TO_CHAR(C.SHIP_DATE,'MM/DD/YYYY')       AS SHIP_DATE, " +
				   "  C.ORDER_TYPE                             AS req_type, " +
				   "  TO_CHAR(C.ORDER_OPEN_DATE,'MM/DD/YYYY') AS Request_Date, " +
				   "  C.order_sample_at_lab,C.SPEC_COLLECTION_DATE as d1,C.CORD_REGISTRATION_DATE as d2,C.CORD_BABY_BIRTH_DATE as d3,C.ORDER_OPEN_DATE as d4,C.SHIP_DATE as d5, TO_CHAR(c.act_ship_date, 'MM/DD/YYYY') AS ACT_SHIP_DATE, ROW_NUMBER () OVER(ORDER BY C.site_id)";
			       sqlHead += getSession(getRequest()).getAttribute("queryBuilderLESql");
		           sqlHead += orderBy;		           			   
		       }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_QUICKSUMMARY_REPORT)){
				   sqlHead += "SELECT Distinct(C.PK_CORD),C.SITE_ID,C.REGISTRY_ID,C.CORD_LOCAL_CBU_ID,C.CORD_ID_NUMBER_ON_CBU_BAG,C.CORD_ISBI_DIN_CODE,C.REGISTRY_MATERNAL_ID,C.MATERNAL_LOCAL_ID," +
				   " C.CORD_HISTORIC_CBU_ID,"+
				   " CASE "+
		            " WHEN ERES.f_codelst_desc(C.order_type)='CT' "+
		            " AND C.order_sample_at_lab        ='Y'"+
		            " THEN 'CT - Sample at Lab' "+
		            " WHEN ERES.f_codelst_desc(C.order_type)='CT'"+
		            " AND C.order_sample_at_lab        ='N'"+
		            " THEN 'CT - Ship Sample' "+
		            " ELSE ERES.f_codelst_desc(C.order_type) "+
		           " END req_type,TO_CHAR(C.ORDER_OPEN_DATE,'MM/DD/YYYY') ORDER_OPEN_DATE,TO_CHAR(C.SHIP_DATE,'MM/DD/YYYY') SHIP_DATE,TO_CHAR(C.INFUSION_DATE,'MM/DD/YYYY') INFUSION_DATE ,C.PATIENT_ID,C.CURRENT_DIAGNOSIS,C.TRANS_SITE_ID,C.TRANS_SITE_NAME,C.ORDER_OPEN_DATE AS request_dt";
				   sqlHead += getSession(getRequest()).getAttribute("queryBuilderORSql");
				   sqlHead += orderBy;		      	           		           			      	
			   }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_IDSUMMARY_REPORT)){
				   /*sqlHead += "SELECT Distinct(C.PK_CORD),C.SITE_ID,C.REGISTRY_ID, C.CORD_LOCAL_CBU_ID, C.CORD_ID_NUMBER_ON_CBU_BAG, C.CORD_ISBI_DIN_CODE,C.CORD_HISTORIC_CBU_ID,C.REGISTRY_MATERNAL_ID,C.MATERNAL_LOCAL_ID," +
   				   " CASE "+
   		            " WHEN ERES.f_codelst_desc(C.order_type)='CT' "+
   		            " AND C.order_sample_at_lab        ='Y'"+
   		            " THEN 'CT - Sample at Lab' "+
   		            " WHEN ERES.f_codelst_desc(C.order_type)='CT'"+
   		            " AND C.order_sample_at_lab        ='N'"+
   		            " THEN 'CT - Ship Sample' "+
   		            " ELSE ERES.f_codelst_desc(C.order_type) "+
   		           " END req_type,TO_CHAR(C.SHIP_DATE,'MM/DD/YYYY') SHIP_DATE,TO_CHAR(C.SHIP_DATE,'MM/DD/YYYY') SHIP_DATE1,TO_CHAR(C.ORDER_OPEN_DATE,'MM/DD/YYYY'),ORDER_OPEN_DATE as c4 ,C.SHIP_DATE as c3";*/
				   sqlHead += "SELECT Distinct(C.PK_CORD),C.SITE_ID,C.REGISTRY_ID, C.CORD_LOCAL_CBU_ID, C.CORD_ID_NUMBER_ON_CBU_BAG, C.CORD_ISBI_DIN_CODE,C.CORD_HISTORIC_CBU_ID,C.REGISTRY_MATERNAL_ID,C.MATERNAL_LOCAL_ID";
   				   sqlHead += getSession(getRequest()).getAttribute("queryBuilderSql");
   				   sqlHead += " ORDER BY C.SITE_ID,C.REGISTRY_ID";		      
}
			   if(getReportType()!=null && getReportType().equalsIgnoreCase("pdf")){
				  
				   if(!sqlHead.equals("")){
					   cdrObjects = vdaController.getQueryBuilderViewData(sqlHead, null);  
				   }
				  
				   
		           if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_LICENSEANDELIGIBILTY_REPORT)){
					   
		        	   Map<BigDecimal, String> raceMap = null;
					   Map<Long,ArrayList<Long>> licensureMap = null;
					   Map<Long,ArrayList<Long>> eligMap = null;
					   Map<Long, Object []> filterCDRObj = null;
					   Map<Long, ArrayList<Long>> filterReasonObj =null;
					   Map<String, Map<Long,ArrayList<Long>>> entityMap = null;
					   
					   LicEligCDRObj cdr =  new LicEligCDRObj(sqlHead, vdaController);
					   Thread cdrWorker = new Thread(cdr);
				   		  cdrWorker.start();
					   LicEligRaceObj race = new LicEligRaceObj(raceSelectedCordQuery().toString(), vdaController);
					   Thread raceWorker = new Thread(race);
					   		  raceWorker.start();
							
					   		
					   		  
					   		LicEligFilterCordObj fcdr = new LicEligFilterCordObj(filterCordQuery(), vdaController);
					   		Thread fcdrWorker = new Thread(fcdr);
					   		fcdrWorker.start();
					   		
					   		LicEligFilterReasonObj fReason = new LicEligFilterReasonObj(filterReasonQuery(), vdaController);					   		
					   		Thread fReasonWorker = new Thread(fReason);
					   		fReasonWorker.start();
					   
					   		
					   		fReasonWorker.join();
					   		fcdrWorker.join();
					   		
					   		filterCDRObj = fcdr.getCdr();
					   		filterReasonObj = fReason.getReason();
					   		
					   				LicEligMapObj lem = new LicEligMapObj(filterStatusQuery(), vdaController, filterCDRObj, filterReasonObj);
					   				Thread lemWorker = new Thread(lem);
					   				lemWorker.start();
					   				
					   				
					   				
					   				raceWorker.join();
					   				lemWorker.join();
					   				cdrWorker.join();
							   		
					   				/**** Setting Object *****/		
					   				
					   				raceMap = objToMap(race.getRace());
					   				cdrObjects = cdr.getCdrObj();
					   				entityMap = lem.getLicEligMap();
					   				
							     long endTimee=System.currentTimeMillis();
						         System.out.println();
						         System.out.println(" Time ------>"+(endTimee-st)/1000+" : "+(endTimee-st)%1000+" MM:SS\n\n");
							 
							 System.out.println("|||||||||||||||||||||||||||||||||||||||||||||||||||||||");						

							 
						if(entityMap != null){
							
									licensureMap =entityMap.get("licence") == null ? new HashMap<Long,ArrayList<Long>>() : entityMap.get("licence");
									eligMap      = entityMap.get("eligibility") == null ? new HashMap<Long,ArrayList<Long>>() : entityMap.get("eligibility");
						 
						}					
							
						if(cdrObjects!=null){							
							   modifyLstObj(licensureMap, raceMap, eligMap);
							   
						   }
				   }			   
				
				   result = getPdfObject(cdrObjects);
			   }else{// Report Type CSV and Excel
				   
				   
				   if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_ANTIGEN_REPORT)){
					   
					  /*  String sqlHlaHead= "";
					    sqlHlaHead += "select Distinct(C.PK_CORD), C.SITE_ID,C.REGISTRY_ID,C.CORD_LOCAL_CBU_ID,C.CORD_ID_NUMBER_ON_CBU_BAG,C.CORD_ISBI_DIN_CODE,C.CORD_HISTORIC_CBU_ID,C.REGISTRY_MATERNAL_ID,C.MATERNAL_LOCAL_ID,eres.F_CODELST_DESC(h.FK_HLA_CODE_ID) AS FK_HLA_CODE_ID,eres.F_CODELST_DESC(h.FK_HLA_METHOD_ID) AS FK_HLA_METHOD_ID,TO_CHAR(h.HLA_RECEIVED_DATE,'MM/DD/YYYY') as HLA_RECEIVED_DATE, TO_CHAR(h.HLA_TYPING_DATE,'MM/DD/YYYY') as HLA_TYPING_DATE,h.HLA_TYPE1,h.HLA_TYPE2,eres.F_CODELST_DESC(h.FK_SOURCE) AS FK_SOURCE,h.HLA_USER,h.CREATED_ON,h.CB_HLA_ORDER_SEQ,C.PATIENT_ID,CBU_COLLECTION_DATE , 1 as CBHLA,C.CORD_NMDP_STATUS as CORD_NMDP_STATUS " +
						" from "+'"'+"VDA"+'"'+"."+'"'+"ETVDA_QUERY_BUILDER_HLA_DATA"+'"'+" h right outer JOIN (" +
						" SELECT Distinct(C.PK_CORD) AS PK_CORD,C.SITE_ID AS SITE_ID,C.REGISTRY_ID AS REGISTRY_ID,C.REGISTRY_MATERNAL_ID AS REGISTRY_MATERNAL_ID,C.CORD_LOCAL_CBU_ID AS CORD_LOCAL_CBU_ID,C.MATERNAL_LOCAL_ID AS MATERNAL_LOCAL_ID,C.CORD_ISBI_DIN_CODE AS CORD_ISBI_DIN_CODE, C.Cord_Historic_Cbu_Id as Cord_Historic_Cbu_Id,C.CORD_ID_NUMBER_ON_CBU_BAG AS CORD_ID_NUMBER_ON_CBU_BAG,C.PATIENT_ID AS PATIENT_ID, TO_CHAR(C.SPEC_COLLECTION_DATE,'MM/DD/YYYY') as CBU_COLLECTION_DATE,C.CORD_NMDP_STATUS as CORD_NMDP_STATUS" ;
					    sqlHlaHead += getSession(getRequest()).getAttribute("queryBuilderHlaSql");
					    sqlHlaHead +=	" ) C on h.entity_id=c.PK_CORD and h.ENTITY_TYPE="+getCodeListPkByTypeAndSubtype(ENTITY_CBU_CODE_TYPE, ENTITY_CBU_CODE_SUB_TYPE) +" UNION ";

					    sqlHlaHead += " select Distinct(C.PK_CORD), C.SITE_ID,C.REGISTRY_ID,C.CORD_LOCAL_CBU_ID,C.CORD_ID_NUMBER_ON_CBU_BAG,C.CORD_ISBI_DIN_CODE,C.CORD_HISTORIC_CBU_ID,C.REGISTRY_MATERNAL_ID,C.MATERNAL_LOCAL_ID,eres.F_CODELST_DESC(bh.FK_HLA_CODE_ID) AS FK_HLA_CODE_ID,eres.F_CODELST_DESC(bh.FK_HLA_METHOD_ID) AS FK_HLA_METHOD_ID,TO_CHAR(bh.HLA_RECEIVED_DATE,'MM/DD/YYYY') as HLA_RECEIVED_DATE, TO_CHAR(bh.HLA_TYPING_DATE,'MM/DD/YYYY') as HLA_TYPING_DATE,bh.HLA_TYPE1,bh.HLA_TYPE2,eres.F_CODELST_DESC(bh.FK_SOURCE) AS FK_SOURCE,bh.HLA_USER,bh.CREATED_ON,bh.CB_BEST_HLA_ORDER_SEQ as CB_HLA_ORDER_SEQ,C.PATIENT_ID,CBU_COLLECTION_DATE , 0 as BHLA,C.CORD_NMDP_STATUS as CORD_NMDP_STATUS " +
						" from "+'"'+"VDA"+'"'+"."+'"'+"ETVDA_QUERY_BUILDER_BHLA_DATA"+'"'+" bh right outer JOIN (" +
						" SELECT Distinct(C.PK_CORD) AS PK_CORD,C.SITE_ID AS SITE_ID,C.REGISTRY_ID AS REGISTRY_ID,C.REGISTRY_MATERNAL_ID AS REGISTRY_MATERNAL_ID,C.CORD_LOCAL_CBU_ID AS CORD_LOCAL_CBU_ID,C.MATERNAL_LOCAL_ID AS MATERNAL_LOCAL_ID,C.CORD_ISBI_DIN_CODE AS CORD_ISBI_DIN_CODE, C.Cord_Historic_Cbu_Id as Cord_Historic_Cbu_Id,C.CORD_ID_NUMBER_ON_CBU_BAG AS CORD_ID_NUMBER_ON_CBU_BAG,C.PATIENT_ID AS PATIENT_ID, TO_CHAR(C.SPEC_COLLECTION_DATE,'MM/DD/YYYY') as CBU_COLLECTION_DATE,C.CORD_NMDP_STATUS as CORD_NMDP_STATUS" ;
					    sqlHlaHead += getSession(getRequest()).getAttribute("queryBuilderHlaSql");
					    sqlHlaHead +=	") C on bh.entity_id=c.PK_CORD and bh.ENTITY_TYPE="+getCodeListPkByTypeAndSubtype(ENTITY_CBU_CODE_TYPE, ENTITY_CBU_CODE_SUB_TYPE)+" order by SITE_ID,REGISTRY_ID,PATIENT_ID";
					    
					    */
					    
					    if(getReportType()!=null && getReportType().equalsIgnoreCase("excel")/*||getReportType().equalsIgnoreCase("csv"))&& getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_ANTIGEN_REPORT)*/)
					    {						    	
					    	 
					                 hlaAntigenDataExcelCSV((String) getSession(getRequest()).getAttribute("queryBuilderHlaSql"));
					        
					         
					      
							    ExcelCSVGenerator eg = new  ExcelCSVGenerator(cdrObj, cbuBestHlas, cbuHlas, (Map<Long,Long>)getSession(getRequest()).getAttribute("orDuplCords"));
								         eg.setIsMultipleCBBSelected(getIsMultipleCBBSelected());
								         Workbook wb =    eg.generateExcel(ANTIGEN_REPORT);
								         String contentType = "application/vnd.ms-excel";								        
								         request.setAttribute("onlyFileName", true);
								         String fileName = Utilities.file("CBU_HLA_Report", ".xlsx", request);
								         
								         servletResponseHeader(response, fileName, contentType);
								        
								         excelServletResponseOutStream(response, wb);
								        
								         
								 ((SXSSFWorkbook)wb).dispose();//  // deleting temporary data on disk
								 
								 long endTime=System.currentTimeMillis();
						         System.out.println();
						         System.out.println(" EXCEL TIME ------>"+(endTime-st)/1000+" : "+(endTime-st)%1000+" MM:SS\n\n");
								 
					         return "CSVEXPORT";
					         
					    }else if(getReportType()!=null && getReportType().equalsIgnoreCase("csv")){
					    	
					    	 hlaAntigenDataExcelCSV((String) getSession(getRequest()).getAttribute("queryBuilderHlaSql"));
					    	// String  fName="CBU_HLA_Report"+"["+ System.currentTimeMillis() + "]"+(user!=null?user.getUserId():"");
					    	  ExcelCSVGenerator eg = new  ExcelCSVGenerator(cdrObj, cbuBestHlas, cbuHlas, (Map<Long,Long>)getSession(getRequest()).getAttribute("orDuplCords"));
					    	  					eg.setIsMultipleCBBSelected(getIsMultipleCBBSelected());
						         File content = eg.generateCSV(filePath("CBU_HLA_Report" , ".csv", user.getUserId()), ANTIGEN_REPORT);
						         String contentType = "text/csv";						      
						         servletResponseHeader(response, content.getName(), contentType);						       
						         csvServletResponseOutStream(response, content);
						         
						         long endTime=System.currentTimeMillis();
						         System.out.println();
						         System.out.println((endTime-st)/1000+" : "+(endTime-st)%1000+" MM:SS\n\n");
						         
					    	 return "CSVEXPORT";
					    	
					    } /*else
					    {
					    cdrObjects = vdaController.getQueryBuilderViewData(sqlHlaHead, null);
					    }*/
						
				   }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_IDSUMMARY_REPORT)){
					   
				    	
				    	
				    	 if(getReportType()!=null && getReportType().equalsIgnoreCase("excel")/*||getReportType().equalsIgnoreCase("csv"))&& getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_ANTIGEN_REPORT)*/)
						    {		 	
						    	 
				    		 
						        
						         
						      
								    ExcelCSVGenerator eg = new  ExcelCSVGenerator(vdaController.getQueryBuilderListObject(sqlHead), (Map<Long,Long>)getSession(getRequest()).getAttribute("orDuplCords"));
								    				  eg.setIsMultipleCBBSelected(getIsMultipleCBBSelected());
									         Workbook wb =    eg.generateExcel(ID_SUMMARY_REPORT);
									         String contentType = "application/vnd.ms-excel";								        
									         request.setAttribute("onlyFileName", true);
									         
									         String fileName = Utilities.file("CBU_ID_Summary", ".xlsx", request);									         
									         servletResponseHeader(response, fileName, contentType);
									        
									         excelServletResponseOutStream(response, wb);	        
									         
									          ((SXSSFWorkbook)wb).dispose();//  // deleting temporary data on disk
									 
									 long endTime=System.currentTimeMillis();
							         System.out.println();
							         System.out.println(" EXCEL TIME ------>"+(endTime-st)/1000+" : "+(endTime-st)%1000+" MM:SS\n\n");
									 
						         return "CSVEXPORT";
						         
						    }else if(getReportType()!=null && getReportType().equalsIgnoreCase("csv")){
						    	
						 
						    	 
						    	 	 ExcelCSVGenerator eg = new  ExcelCSVGenerator(vdaController.getQueryBuilderListObject(sqlHead), (Map<Long,Long>)getSession(getRequest()).getAttribute("orDuplCords"));	
						    	 	 				   eg.setIsMultipleCBBSelected(getIsMultipleCBBSelected());
							         File content = eg.generateCSV(filePath("CBU_ID_Summary", ".csv", user.getUserId()), ID_SUMMARY_REPORT);	        
							         String contentType = "text/csv";							         
							         servletResponseHeader(response, content.getName(), contentType);						       
							         csvServletResponseOutStream(response, content);
							        
							         long endTime=System.currentTimeMillis();
							         System.out.println();
							         System.out.println((endTime-st)/1000+" : "+(endTime-st)%1000+" MM:SS\n\n");
							         
						    	 return "CSVEXPORT";
						    	
						    } 
						
				    }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_QUICKSUMMARY_REPORT)){
				    	
				    	
				    	 if(getReportType()!=null && getReportType().equalsIgnoreCase("excel")/*||getReportType().equalsIgnoreCase("csv"))&& getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_ANTIGEN_REPORT)*/)
						    {
				    	

						    ExcelCSVGenerator eg = new  ExcelCSVGenerator(vdaController.getQueryBuilderListObject(sqlHead), (Map<Long,Long>)getSession(getRequest()).getAttribute("orDuplCords"));
						    				  eg.setIsMultipleCBBSelected(getIsMultipleCBBSelected());
						    				  
							         Workbook wb =    eg.generateExcel(QUICKSUMMARY);
							         
							         String contentType = "application/vnd.ms-excel";								        
							         request.setAttribute("onlyFileName", true);
							         
							         String fileName = Utilities.file("OR_Request_and_Shipment_Details_Report", ".xlsx", request);									         
							         servletResponseHeader(response, fileName, contentType);
							        
							         excelServletResponseOutStream(response, wb);	        
							         
							          ((SXSSFWorkbook)wb).dispose();//  // deleting temporary data on disk
							          
							          long endTime=System.currentTimeMillis();
								         System.out.println();
								         System.out.println(" EXCEL TIME ------>"+(endTime-st)/1000+" : "+(endTime-st)%1000+" MM:SS\n\n");
							          
							          return "CSVEXPORT";
							          
						    }else if(getReportType()!=null && getReportType().equalsIgnoreCase("csv")){
						    	
						    	     ExcelCSVGenerator eg = new  ExcelCSVGenerator(vdaController.getQueryBuilderListObject(sqlHead), (Map<Long,Long>)getSession(getRequest()).getAttribute("orDuplCords"));	
	    	 	 				     eg.setIsMultipleCBBSelected(getIsMultipleCBBSelected());
							         File content = eg.generateCSV(filePath("OR_Request_and_Shipment_Details_Report", ".csv", user.getUserId()), QUICKSUMMARY);	        
							         String contentType = "text/csv";							         
							         servletResponseHeader(response, content.getName(), contentType);						       
							         csvServletResponseOutStream(response, content);
							        
							         long endTime=System.currentTimeMillis();
							         System.out.println();
							         System.out.println((endTime-st)/1000+" : "+(endTime-st)%1000+" MM:SS\n\n");
							         
						    	 return "CSVEXPORT";
						    	
						    }
			       
			         
						
				    }else{// This  Report
			           if(!sqlHead.equals("") && !getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_LICENSEANDELIGIBILTY_REPORT)){
						   cdrObjects = vdaController.getQueryBuilderListObject(sqlHead); 
					   }	
			       }
				   
				   
				   if(!sqlHead.equals("") && getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_LICENSEANDELIGIBILTY_REPORT)){
					   
					   Map<BigDecimal, String> raceMap = null;
					   Map<Long,ArrayList<Long>> licensureMap = null;
					   Map<Long,ArrayList<Long>> eligMap = null;
					   Map<Long, Object []> filterCDRObj = null;
					   Map<Long, ArrayList<Long>> filterReasonObj =null;
					   Map<String, Map<Long,ArrayList<Long>>> entityMap = null;
					   
					   LicEligCDRObj cdr =  new LicEligCDRObj(sqlHead, vdaController);
					   Thread cdrWorker = new Thread(cdr);
				   		  cdrWorker.start();
					   LicEligRaceObj race = new LicEligRaceObj(raceSelectedCordQuery().toString(), vdaController);
					   Thread raceWorker = new Thread(race);
					   		  raceWorker.start();
							
					   		
					   		  
					   		LicEligFilterCordObj fcdr = new LicEligFilterCordObj(filterCordQuery(), vdaController);
					   		Thread fcdrWorker = new Thread(fcdr);
					   		fcdrWorker.start();
					   		
					   		LicEligFilterReasonObj fReason = new LicEligFilterReasonObj(filterReasonQuery(), vdaController);					   		
					   		Thread fReasonWorker = new Thread(fReason);
					   		fReasonWorker.start();
					   
					   		
					   		fReasonWorker.join();
					   		fcdrWorker.join();
					   		
					   		filterCDRObj = fcdr.getCdr();
					   		filterReasonObj = fReason.getReason();
					   		
					   				LicEligMapObj lem = new LicEligMapObj(filterStatusQuery(), vdaController, filterCDRObj, filterReasonObj);
					   				Thread lemWorker = new Thread(lem);
					   				lemWorker.start();
					   				
					   				
					   				
					   				raceWorker.join();
					   				lemWorker.join();
					   				cdrWorker.join();
							   		
					   				/**** Setting Object *****/		
					   				
					   				raceMap = objToMap(race.getRace());
					   				cdrObjects = cdr.getCdrObj();
					   				entityMap = lem.getLicEligMap();
					   				
							     long endTimee=System.currentTimeMillis();
						         System.out.println();
						         System.out.println(" Time ------>"+(endTimee-st)/1000+" : "+(endTimee-st)%1000+" MM:SS\n\n");
							 
							 System.out.println("|||||||||||||||||||||||||||||||||||||||||||||||||||||||");					
						
						

						// Map<String, Map<BigDecimal, String>> hmp = objsToMaps(null/*vdaController.getQueryBuilderListObject(licensureSelectedCordQuery().toString())*/);	
							// Map<Long,Long> licensureMap =new HashMap<Long, Long>();
							// Map<Long,Long> eligMap = new HashMap<Long, Long>();
							 
						if(entityMap != null){
							
									licensureMap =entityMap.get("licence") == null ? new HashMap<Long,ArrayList<Long>>() : entityMap.get("licence");
									eligMap      = entityMap.get("eligibility") == null ? new HashMap<Long,ArrayList<Long>>() : entityMap.get("eligibility");
						 
						}
						 // raceMap=objToMap(vdaController.getQueryBuilderListObject(raceSelectedCordQuery().toString()));
							
						if(cdrObjects!=null){
							  // setCdrObjects(cdrObjects);
							   modifyLstObj(licensureMap, raceMap, eligMap);
							   
						   }
						
						if(getReportType()!=null && getReportType().equalsIgnoreCase("excel"))
					    {
			    	

						    ExcelCSVGenerator eg = new  ExcelCSVGenerator(cdrObjects, (Map<Long,Long>)getSession(getRequest()).getAttribute("orDuplCords"));
						    				  eg.setIsMultipleCBBSelected(getIsMultipleCBBSelected());
						    				  
							         Workbook wb =    eg.generateExcel(LICENSELGBLERPT);
							         
							         String contentType = "application/vnd.ms-excel";								        
							         request.setAttribute("onlyFileName", true);
							         
							         String fileName = Utilities.file("CBU_Characteristics_Report", ".xlsx", request);									         
							         servletResponseHeader(response, fileName, contentType);
							        
							         excelServletResponseOutStream(response, wb);	        
							         
							          ((SXSSFWorkbook)wb).dispose();//  // deleting temporary data on disk
							          
							          long endTime=System.currentTimeMillis();
								         System.out.println();
								         System.out.println(" EXCEL TIME ------>"+(endTime-st)/1000+" : "+(endTime-st)%1000+" MM:SS\n\n");
						          
						          return "CSVEXPORT";
						          
					    }else if(getReportType()!=null && getReportType().equalsIgnoreCase("csv")){
					    	
					    	     ExcelCSVGenerator eg = new  ExcelCSVGenerator(cdrObjects, (Map<Long,Long>)getSession(getRequest()).getAttribute("orDuplCords"));	
    	 	 				     eg.setIsMultipleCBBSelected(getIsMultipleCBBSelected());
						         File content = eg.generateCSV(filePath("CBU_Characteristics_Report", ".csv", user.getUserId()), LICENSELGBLERPT);	        
						         String contentType = "text/csv";							         
						         servletResponseHeader(response, content.getName(), contentType);						       
						         csvServletResponseOutStream(response, content);
						        
						         long endTime=System.currentTimeMillis();
						         System.out.println();
						         System.out.println((endTime-st)/1000+" : "+(endTime-st)%1000+" MM:SS\n\n");
						         
					    	 return "CSVEXPORT";
					    	
					    }
					}
				   
				    String fName = "";
				    if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_ANTIGEN_REPORT)){
				    	fName="CBU_HLA_Report"+"["+ System.currentTimeMillis() + "]"+(user!=null?user.getUserId():"");
					    
				    }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_LICENSEANDELIGIBILTY_REPORT)){
				    	fName="CBU_Characteristics_Report"+"["+ System.currentTimeMillis() + "]"+(user!=null?user.getUserId():"");
				    	 
				    }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_QUICKSUMMARY_REPORT)){
				    	fName="OR_Request_and_Shipment_Details_Report"+"["+ System.currentTimeMillis() + "]"+(user!=null?user.getUserId():"");
						
				    }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_IDSUMMARY_REPORT)){
				    	fName="cbu_id_summary"+"["+ System.currentTimeMillis() + "]"+(user!=null?user.getUserId():"");
						
				    }
				   
				    
				   if(getReportType()!=null && getReportType().equalsIgnoreCase("excel")){	
					   
					   String excelType = getText("garuda.querybuilderreport.excel.type");
			             //setResponse Header
			            response.setHeader("Expires", "0");
						response.setHeader("Cache-Control",
						"must-revalidate, post-check=0, pre-check=0");
						response.setHeader("Pragma", "public");
						response.setContentType("application/vnd.ms-excel");
						if(excelType!=null && excelType.equals("xls"))
						  response.setHeader("Content-Disposition","attachment;filename="+fName+".xls");
						else
							response.setHeader("Content-Disposition","attachment;filename="+fName+".xlsx");						
						ExcelGeneratorAction action = new ExcelGeneratorAction();
					    if(getIsMultipleCBBSelected()!=null)
					    	action.setIsMultipleCBBSelected(getIsMultipleCBBSelected());
					    if(getIsWorkflowActivitySelected()!=null)
					    	action.setIsWorkflowActivitySelected(getIsWorkflowActivitySelected());
					    if(getUnlicensed_flag()!=null)
					    	action.setUnlicensed_flag(getUnlicensed_flag());
					    if(getQinelig_flag()!=null)
					    	action.setQinelig_flag(getQinelig_flag());
					    if(getQincomp_flag()!=null)
					    	action.setQincomp_flag(getQincomp_flag());
					    if(getInelig_incomple_flag()!=null)
					    	action.setInelig_incomple_flag(getInelig_incomple_flag());
						OutputStream out = action.generateExcel(cdrObjects, response, getPaginateModule(),excelType);
						out.close();
						out.flush();
						
				   }else{			       
			             //setResponse Header

					   StringBuilder content=generateAntigenRepCSVContent(cdrObjects,getPaginateModule());
			            response.setHeader("Expires", "0");
						response.setHeader("Cache-Control",
						"must-revalidate, post-check=0, pre-check=0");
						response.setHeader("Pragma", "public");
						response.setContentType("text/csv");
						response.setContentLength(content.length());
						response.setHeader("Content-Disposition",
						"attachment;filename="+fName+".csv");
						response.getOutputStream().write(content.toString().getBytes());
						response.getOutputStream().flush();
						
				   }
		           
		           result = "CSVEXPORT";
			   }
			   
		   }catch (Exception e) {
			e.printStackTrace();
		   }
		   return result;
	   }

	private void servletResponseHeader(HttpServletResponse response, String fName, String contentType){
		
		    response.setHeader("Expires", "0");
			response.setHeader("Cache-Control","must-revalidate, post-check=0, pre-check=0");
			response.setHeader("Pragma", "public");
			response.setContentType(contentType);
			response.setHeader("Content-Disposition","attachment;filename="+fName);	
		
	}
	
	private void excelServletResponseOutStream(HttpServletResponse response, Workbook wb) throws IOException{
		
		    OutputStream out = response.getOutputStream();		         	
			           ((SXSSFWorkbook)wb).write(out, request);			         
			             out.close();
			             out.flush();
			 
			
		
	}
	
	private void csvServletResponseOutStream(HttpServletResponse response, File content) throws IOException{
	
		OutputStream out = null;
		FileInputStream fis = null;
		try{
			response.setContentLength((int)content.length());
			out = response.getOutputStream();		
			fis = new FileInputStream(content); 		
			IOUtils.copy(fis, out);		
			out.close();		
			fis.close();		
			content.delete(); 
			
		}catch(IOException e){
			
			e.printStackTrace();
		}finally{
			
            try {
            	//out.close();
            	fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
		 
	
}
public StringBuilder licensureSelectedCordQuery(){
		 StringBuilder querySB=new StringBuilder();
		
		 querySB.append(" SELECT c.pk_cord, codelst.CODELST_DESC ,STAT.STATUS_TYPE_CODE FROM ERES.CB_ENTITY_STATUS STAT INNER JOIN eres.cb_cord c ON c.pk_cord =stat.entity_id AND ( c.fk_cord_cbu_unlicensed_reason=STAT.PK_ENTITY_STATUS or c.fk_cord_cbu_ineligible_reason=STAT.PK_ENTITY_STATUS) INNER JOIN ERES.CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS INNER JOIN ERES.ER_CODELST codelst on REASON.FK_REASON_ID=codelst.PK_CODELST ");
			 querySB.append(" JOIN ( SELECT Distinct(C.PK_CORD) AS PK_CORD ").append(getSession(getRequest()).getAttribute("queryBuilderSql")).append(") d on c.pk_cord = d.pk_cord ") ;
			/* querySB.append(" UNION ");
			 querySB.append(" SELECT c.pk_cord, ERES.F_CODELST_DESC(REASON.FK_REASON_ID),STAT.STATUS_TYPE_CODE FROM ERES.CB_ENTITY_STATUS STAT INNER JOIN eres.cb_cord c ON c.pk_cord =stat.entity_id AND c.fk_cord_cbu_ineligible_reason=STAT.PK_ENTITY_STATUS INNER JOIN ERES.CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS ");
			 querySB.append(" JOIN ( SELECT Distinct(C.PK_CORD) AS PK_CORD ").append(getSession(getRequest()).getAttribute("queryBuilderLESql")).append(") d on c.pk_cord = d.pk_cord ") ;*/
			
		return querySB;
		
	}	
	
public StringBuilder raceSelectedCordQuery(){
		 StringBuilder querySB=new StringBuilder();
		
			 querySB.append(" select cord.pk_cord, codelst.CODELST_DESC as Description FROM eres.CB_CORD cord join eres.CB_MULTIPLE_VALUES race on cord.pk_cord = race.entity_id AND race.FK_CODELST_TYPE='race' INNER JOIN ERES.ER_CODELST CODELST ON race.FK_CODELST=codelst.PK_CODELST ");
			 querySB.append(" JOIN ( SELECT Distinct(C.PK_CORD) AS PK_CORD ").append(getSession(getRequest()).getAttribute("queryBuilderSql")).append(") d on cord.pk_cord = d.pk_cord order by codelst.CODELST_DESC") ;
				 
		return querySB;		
	}

private String filterCordQuery(){
	 String query = "";	 
	         query = "SELECT CORD.PK_CORD, CORD.FK_CORD_CBU_UNLICENSED_REASON,  /*ERES.F_IS_NEW_TASK_NEEDED(CORD.PK_CORD),*/  CORD.FK_CORD_CBU_INELIGIBLE_REASON FROM ERES.CB_CORD CORD  JOIN ( SELECT DISTINCT(C.PK_CORD) AS PK_CORD "+getSession(getRequest()).getAttribute("queryBuilderSql")+") D ON CORD.PK_CORD = D.PK_CORD";
	 return query;
}

private String filterStatusQuery(){
	 String query = "";
	 		query = "SELECT STAT.ENTITY_ID, STAT.PK_ENTITY_STATUS, STAT.STATUS_TYPE_CODE FROM ERES.CB_ENTITY_STATUS STAT  JOIN ( SELECT Distinct(C.PK_CORD) AS PK_CORD "+getSession(getRequest()).getAttribute("queryBuilderSql")+") D ON STAT.ENTITY_ID = D.PK_CORD";	 
	 return query;
}

private String filterReasonQuery(){
	 String query = "";
	 		query = "SELECT REASON.FK_ENTITY_STATUS, REASON.FK_REASON_ID  FROM ERES.CB_ENTITY_STATUS_REASON REASON";	 
	 return query;
}

/*private String filterNewTaskQuery(){
	 String query = "";
	 		query = "SELECT CORD.PK_CORD,ERES.F_IS_NEW_TASK_NEEDED(CORD.PK_CORD) FROM ERES.CB_CORD CORD JOIN ( SELECT DISTINCT(C.PK_CORD) AS PK_CORD "+getSession(getRequest()).getAttribute("queryBuilderSql")+") D ON CORD.PK_CORD = D.PK_CORD";	 
	 return query;
}*/

public Map<String, Map<BigDecimal, String>> objsToMaps(List<Object> object) throws Exception{
	
	Map<String, Map<BigDecimal, String>> hmp =new HashMap<String, Map<BigDecimal, String>>();    
	Map<BigDecimal,String> licMap=new HashMap<BigDecimal, String>();
	Map<BigDecimal,String> eligMap=new HashMap<BigDecimal, String>();
	hmp.put("licence", licMap);
	hmp.put("eligibility", eligMap);
	Object []o;
	BigDecimal key;
	StringBuilder description=new StringBuilder();
	StringBuilder str=new StringBuilder();
	for(Object obj:object){
		o=(Object[])obj;
		key=(BigDecimal)o[0];
		description.append(o[1]);
		if(LICENCE_STATUS.equals(o[2].toString())){//Licenser
			if(licMap.containsKey(key)){
				str.append(licMap.get(key)).append(", ");
				str.append(description);
				licMap.put(key, str.toString());
			}else{
				licMap.put(key, description.toString());
			}
		}else{//Eligibility
			if(eligMap.containsKey(key)){
				str.append(eligMap.get(key)).append(", ");
				str.append(description);
				eligMap.put(key, str.toString());
			}else{
				eligMap.put(key, description.toString());
			}
			
		}
		description.delete(0, description.length());
		str.delete(0, str.length());
	}
	
	return hmp;
}
public Map<BigDecimal, String> objToMap(List<Object> object){
	Map<BigDecimal,String> objMap = new HashMap<BigDecimal,String>();
	
	Object []o;
	BigDecimal key;
	StringBuilder description=new StringBuilder();
	List<BigDecimal> exemptionList = new ArrayList<BigDecimal>();
	String exemptionStr = "Declines/No Race Selected";
	String str=new String();
	for(Object obj:object){//filter same RegistryId description
		o=(Object[])obj;
		key=(BigDecimal)o[0];
		description.append(o[1]);
		if(objMap.containsKey(key)){
			if(!description.toString().equals(exemptionStr)){
				str=objMap.get(key)+", ";
				str +=description.toString();
				objMap.put(key, str);
			}
			else{
				exemptionList.add(key);
			}
			
		}else{
			
			objMap.put(key, description.toString());
		}
		
		description.delete(0, description.length());
	}
	/*if(exemption.equals(exemptionStr)){
		str +=
	}*/
	for(int i=0;i<exemptionList.size();i++){
		if(objMap.containsKey(exemptionList.get(i))){
			String temp = objMap.get(exemptionList.get(i));
			temp += ", "+exemptionStr;
			//System.out.println("\n\n\n Inside If Temp:::"+temp+"::::Key:::"+exemptionList.get(i));
			objMap.put(exemptionList.get(i), temp);
		}
	}
	return objMap;
	
}
	private void modifyLstObj(Map<Long, ArrayList<Long>> licensureMap, Map<BigDecimal,String> raceMap, Map<Long, ArrayList<Long>> eligibilityMap){
		
		String dateStr = null;		
		Date codeLstDate = null;
		Date schshipdate = null;
		Date actshipdate = null;
		Long entityId = null;
		
		long pk_unlicensed=getCodeListPkByTypeAndSubtype(LICENCE_STATUS, UNLICENSE_REASON);
		long pk_ineligible=getCodeListPkByTypeAndSubtype(ELIGIBILITY_STATUS, INELIGIBLE_REASON);
		long pk_incomplete=getCodeListPkByTypeAndSubtype(ELIGIBILITY_STATUS, INCOMPLETE_REASON);
		long eliDescription=getCodeListPkByTypeAndSubtype(ELIGIBILITY_STATUS, NOT_COLLECTED_TO_PRIOR_REASON);
		long pk_ctReq=getCodeListPkByTypeAndSubtype(ORDER_TYPE, CT);		
		SimpleDateFormat df = new SimpleDateFormat("mm/dd/yyyy");
		StringBuilder str = new StringBuilder();
		   
		try{
			
			
			dateStr = getCodeListDescByTypeAndSubtype(VELOS_EMTRAX_VERSION_TYPE,VELOS_EMTRAX_VERSION_SUBTYPE);
			if(dateStr!=null){
				codeLstDate = Utilities.getDate(dateStr, VELOS_EMTRAX_VERSION1_3_DATE_FORMAT);
			}
					
		
	}catch (Exception e) {
		e.printStackTrace();
	}
		
		 for(int k=0;k<cdrObjects.size();k++){
			   
			   Object[] Obj=(Object[]) cdrObjects.get(k);
			   entityId = ((BigDecimal)Obj[0]).longValue();
			   
			   /***************************For NMDP CBU Status*********************************/
			   
			   String nmdpStatus="";
			  
			   if(Obj[7]!=null && !Obj[7].toString().equals("")){
				   nmdpStatus=Obj[7].toString();
				   Obj[7]=getCbuStatusDescByPk(Long.parseLong(nmdpStatus));
				       
			   }
			   /******************************************************************************/
			   
			   /***************************For CBU Status*********************************/
			   
			   String cbuStatus="";
			  
			   if(Obj[8]!=null && !Obj[8].toString().equals("")){
				   cbuStatus=Obj[8].toString();
				   Obj[8]=getCbuStatusDescByPk(Long.parseLong(cbuStatus));
				       
			   }
			   /******************************************************************************/
			   
			   /***************************For Race Values*********************************/
			   
			   String raceType="";
			  
			   if(Obj[15]!=null && !Obj[15].toString().equals("")){
				   raceType=Obj[15].toString();
				   Obj[15]=getCodeListDescByPk(Long.parseLong(raceType));
				       
			   }
			   /******************************************************************************/
			   /**************************For Bacterial Cultural Result***********************/
			   
			   String bactCultRes="";
			  
			   if(Obj[18]!=null && !Obj[18].toString().equals("")){
				   bactCultRes=Obj[18].toString();
				   Obj[18]=getCodeListDescByPk(Long.parseLong(bactCultRes));
				       
			   }
			   /******************************************************************************/
			   
			   /**************************For Fungal Cultural Result***********************/
			   
			   String fungCultRes="";
			  
			   if(Obj[19]!=null && !Obj[19].toString().equals("")){
				   fungCultRes=Obj[19].toString();
				   Obj[19]=getCodeListDescByPk(Long.parseLong(fungCultRes));
				       
			   }
			   /******************************************************************************/						   
			   
			   /*******************************Hemoglobinopathy Result***********************/
			   
			   String hemoGlobinopathytRes="";
			  
			   if(Obj[20]!=null && !Obj[20].toString().equals("")){
				   hemoGlobinopathytRes=Obj[20].toString();
				   Obj[20]=getCodeListDescByPk(Long.parseLong(hemoGlobinopathytRes));
				       
			   }
			   /******************************************************************************/
			   
			   /*******************************Viability Result***********************/
			   
			   String viablility="";
			  
			   if(Obj[26]!=null && !Obj[26].toString().equals("")){
				   viablility=Obj[26].toString();
				   Obj[26]=getCodeListDescByPk(Long.parseLong(viablility));
				       
			   }
			   /******************************************************************************/
			   
		        //************************For Unlicensed Reason***********************************//
			   		
			   long tempSt=0l;
			   if(Obj[9]!=null && !Obj[9].toString().equals("")){
				   tempSt=Long.parseLong(Obj[9].toString());
			   }
			   
			   
			   long temp=0l;
			   
			   if(Obj[10]!=null && !Obj[10].toString().equals("")){
				   temp=Long.parseLong(Obj[10].toString());
			   }
			long pkval=0l;
			if((Obj[28]!=null || Obj[38]!=null)){
				
				try {
					if(Obj[28]!=null)
						schshipdate = df.parse(Obj[28].toString());
					if(Obj[38]!=null)
						actshipdate = df.parse(Obj[38].toString());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(tempSt==pk_unlicensed){ 
				if(checkOldCord(schshipdate,actshipdate,codeLstDate)){
				   	   String unlicensReason="";
					   unlicensReason=licensureMap!=null?prepCommaSeprateStringByDesc(licensureMap.get(entityId)):fetchCommaSeperatedString(temp,LICENCE_STATUS);
					   Obj[10]=unlicensReason;
					   //System.out.println("\n\n\n\nModified Str:::::::::::::"+Obj[8]);
			   }
			   else{
				   Obj[10]="";
			   }
			}else{
				Obj[10]="";
			}
			   //******************************************************************************//
			   
			   /***************************For Licensure Status*********************************/
			   
			   String licStatus="";
			  
			   if(Obj[9]!=null && !Obj[9].toString().equals("")){
				   licStatus=Obj[9].toString();
				   Obj[9]=getCodeListDescByPk(Long.parseLong(licStatus));
				       
			   }
			   /******************************************************************************/
			   
			   //************************For Ineligible Reason***********************************//*
			   
			   long eligSt=0l;
			   temp=0l;
			   
			   if(Obj[11]!=null && !Obj[11].toString().equals("")){
				   eligSt=Long.parseLong(Obj[11].toString());
			   }
			   
			 

			   if(Obj[12]!=null && !Obj[12].toString().equals("")){
				   temp=Long.parseLong(Obj[12].toString());
			   }
			   
			   pkval=0l;
			   
			   

			   if(eligSt==pk_ineligible || eligSt==pk_incomplete){
				   String inEligReason="";
				   inEligReason=eligibilityMap!=null?prepCommaSeprateStringByDesc(eligibilityMap.get(entityId)):fetchCommaSeperatedString(temp,ELIGIBILITY_STATUS);
				   Obj[12]=inEligReason;
				   //System.out.println("\n\n\n\nModified Str:::::::::::::"+Obj[10]);
			   }else{
				   Obj[12]="";
			   }
			   //******************************************************************************//
			   
			   /***************************For Eligible Status*********************************/
			   
			   long EligStatus=0l;
			   
			   if(Obj[11]!=null && !Obj[11].toString().equals("")){
				   EligStatus=Long.parseLong(Obj[11].toString());
  
				   if(EligStatus==eliDescription){
					   
					   if(checkOldCord(schshipdate,actshipdate,codeLstDate)){
						   Obj[11]=NOT_APPLICABLE_PRIOR_DATE;
						   //System.out.println("Text Changed");
					   }else{
						   Obj[11]=getCodeListDescByPk(EligStatus);
					   }
				   }else{
					   Obj[11]=getCodeListDescByPk(EligStatus);
				   }
			   }
			   else{
				   Obj[11]="Not Yet Determined";
			   }
			   /******************************************************************************/

			   //*****************************Race Description*********************************//
			   
			   String pkcord="";
			   long cordid=0l;
			   String raceDesc="";
			   
			   if(Obj[0]!=null && !Obj[0].toString().equals("")){
				   
				   pkcord=Obj[0].toString();
				   cordid=Long.parseLong(pkcord);
				   raceDesc=raceMap!=null?raceMap.get(BigDecimal.valueOf(entityId)):fetchCommaSeperatedString(cordid,RACE);
				   Obj[14]=raceDesc;
				   
			   }
			   /******************************************************************************/
			   
			   
			   
			   
			   /***************************For Request Type*********************************/
			   
			   long reqType=0l;
			 
			   String samplAtlab="";
			  
			   if(Obj[30]!=null && !Obj[30].toString().equals("")){
				   reqType=Long.parseLong(Obj[30].toString());
				   
				   if(reqType==pk_ctReq){
					   
					   if(Obj[32]!=null && !Obj[32].toString().equals("")){
						   samplAtlab=Obj[32].toString();
						   
						   if(samplAtlab.equals("Y")){
							   Obj[30]=CT_SHIP_SAMPLE_AT_LAB;
						   }else if (samplAtlab.equals("N")) {
							   Obj[30]=CT_SHIP_SAMPLE;
						   } 
					   }
				   }else{
					   Obj[30]=getCodeListDescByPk(reqType);
				   }
				       
			   }
			   /******************************************************************************/
			   
			   /***************************For Funding Category*********************************/
			   
			   String fundCat="";
			  
			   if(Obj[17]!=null && !Obj[17].toString().equals("")){
				   fundCat=Obj[17].toString();
				   Obj[17]=getCodeListDescByPk(Long.parseLong(fundCat));
				       
			   }			   
			   
		   }		
	}
	
	private String prepCommaSeprateStringByDesc(ArrayList<Long> lst){
		StringBuilder sb = new StringBuilder();
		
		if(lst != null)
				for(Long l: lst){
					
					sb.append(getCodeListDescByPk(l)) ;
					sb.append(SEPRATOR);
				}
		
		sb.setLength(sb.length() > 0 ? sb.length() - 1 : 0);		
		return sb.toString();
	}
	
	private String getPdfObject(List<Object> cdrObjects){	
		UserJB user = null;
		try{	 
		      user = (UserJB)getRequest().getSession().getAttribute(VelosGarudaConstants.GARUDA_USER);		  
		}catch (Exception e) {
			e.printStackTrace();
		}
		try{			
			CBUAction action = new CBUAction(0);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			Configuration.readAppendixParam(Configuration.ERES_HOME + "eresearch.xml");
			com.aithent.file.uploadDownload.Configuration.readSettings("eres");
			com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "study");	
			String fileDnPath = com.aithent.file.uploadDownload.Configuration.DOWNLOADSERVLET ;
			Configuration.readReportParam(Configuration.ERES_HOME + "eresearch.xml");
			String path = Configuration.REPDWNLDPATH;
			//String path = "C:\\";
			String pdfFileName= "";
			String temppdfFileName="";
			String reportNo="";
			String versionNo="";
			StringBuffer title = new StringBuffer();
			if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_ANTIGEN_REPORT)){
				pdfFileName="cbu_hla_report"+"["+ System.currentTimeMillis() + "]"+(user!=null?user.getUserId():"")+".pdf";
			    //reportNo = action.getXMLTagValue(VelosGarudaConstants.PRODUCTTAG_REPNO);
				//versionNo = action.getXMLTagValue(VelosGarudaConstants.PRODUCTTAG_REPNO_VERNO);
		    }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_LICENSEANDELIGIBILTY_REPORT)){
		    	pdfFileName="CBU_Characteristics_Report"+"["+ System.currentTimeMillis() + "]"+(user!=null?user.getUserId():"")+".pdf";
		    	temppdfFileName = "CBU_Characteristics_Report1"+"["+ System.currentTimeMillis() + "]"+(user!=null?user.getUserId():"")+".pdf";
				//reportNo = action.getXMLTagValue(VelosGarudaConstants.FDOE_REPNO);
				//versionNo = action.getXMLTagValue(VelosGarudaConstants.FDOE_VERNO);  
		    }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_QUICKSUMMARY_REPORT)){
		    	pdfFileName="OR_Request_and_Shipment_Details_Report"+"["+ System.currentTimeMillis() + "]"+(user!=null?user.getUserId():"")+".pdf";
				//reportNo = action.getXMLTagValue(VelosGarudaConstants.DUMN_REPNO);
				//versionNo = action.getXMLTagValue(VelosGarudaConstants.DUMN_VERNO);
		    }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_IDSUMMARY_REPORT)){
		    	pdfFileName="cbu_id_summary"+"["+ System.currentTimeMillis() + "]"+(user!=null?user.getUserId():"")+".pdf";
				//reportNo = action.getXMLTagValue(VelosGarudaConstants.CBUASSESSMENT_REPNO);
				//versionNo = action.getXMLTagValue(VelosGarudaConstants.CBUASSESSMENT_VERNO);
		    }			
			String filePath = path + File.separator + pdfFileName ;
			String tempfilePath = path + File.separator + temppdfFileName ;
			FontFactory.register(getContext().getRealPath("")+"/jsp/font/arial_narrow.TTF");
			Font titleFont = FontFactory.getFont("arial_narrow", 14,
			      Font.BOLD);
			Font bodyFont = FontFactory.getFont("arial_narrow", 11);
			Font bodyFontBold = FontFactory.getFont("arial_narrow", 11,Font.BOLD);
			Font bodyFontItalic = FontFactory.getFont("arial_narrow", 11,Font.ITALIC);
			Font bodyFontBoldItalic = FontFactory.getFont("arial_narrow", 11,Font.BOLDITALIC);
			Font footerFont = FontFactory.getFont("arial_narrow", 8,
			      Font.NORMAL);
			Document document = new Document();
			if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_LICENSEANDELIGIBILTY_REPORT)){
				document = new Document(PageSize.A4, 25, 25, 50, 40);
				document.setPageSize(PageSize.A4.rotate());
			}
			else{
				document = new Document();
			}
			PdfWriter writer=PdfWriter.getInstance(document, new FileOutputStream(new File(filePath)));
			document.open();
			
		    addTitlePage(document,titleFont,bodyFontBold,bodyFont,cdrObjects,title,writer);
		    document = addContent(document, bodyFontBold, bodyFont,bodyFontItalic,bodyFontBoldItalic,cdrObjects,writer,pdfFileName,tempfilePath);
		    document.close();		    
		    
		    /////
		    String outputFilePath = "";
		    PdfReader originalPdf = new PdfReader(filePath);
            
            if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_ANTIGEN_REPORT)){
            	outputFilePath= path + File.separator + "cbu_hla_report"+"["+ System.currentTimeMillis() + "]"+"_temp"+(user!=null?user.getUserId():"")+".pdf";
		    }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_LICENSEANDELIGIBILTY_REPORT)){
		    	outputFilePath= path + File.separator + "CBU_Characteristics_Report"+"["+ System.currentTimeMillis() + "]"+"_temp"+(user!=null?user.getUserId():"")+".pdf";  
		    }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_QUICKSUMMARY_REPORT)){
		    	outputFilePath= path + File.separator + "OR_Request_and_Shipment_Details_Report"+"["+ System.currentTimeMillis() + "]"+"_temp"+(user!=null?user.getUserId():"")+".pdf";
		    }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_IDSUMMARY_REPORT)){
		    	outputFilePath= path + File.separator + "cbu_id_summary"+"["+ System.currentTimeMillis() + "]"+"_temp"+(user!=null?user.getUserId():"")+".pdf";
		    }	           
            
			Calendar cal = Calendar.getInstance();
			int curYear = cal.get(Calendar.YEAR);
            PdfStamper stamper = new PdfStamper(originalPdf, new FileOutputStream(outputFilePath));
			PdfContentByte under = null;
			PdfContentByte over = null;
			PdfContentByte fover = null;
			int totalPages = originalPdf.getNumberOfPages();
			String repDt="";
			String repTime = "";
			Date dt = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat ("MMM dd, yyyy");
			repDt = sdf.format(dt) ;
			SimpleDateFormat sd = new SimpleDateFormat ("kk:mm" );
			repTime = sd.format(dt);
			for (int pge = 1; pge <= totalPages; pge++) {
				under = stamper.getUnderContent(pge);
				over = stamper.getOverContent(pge);
				under = stamper.getUnderContent(pge);
				fover = stamper.getOverContent(pge);
				over.beginText();
				BaseFont bf1 = BaseFont.createFont(getContext().getRealPath("")+"/jsp/font/arial_narrow.TTF",  
		                 BaseFont.CP1252, BaseFont.NOT_EMBEDDED); 
				over.setFontAndSize(bf1 , 8f);
				if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_LICENSEANDELIGIBILTY_REPORT)){
					over.showTextAligned(PdfContentByte.ALIGN_LEFT, "Report Date: "+ repDt, 20f,   575f,0f);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT, "Report Time: "+ repTime, 20f,   560f,0f);
				}
				else{
					over.showTextAligned(PdfContentByte.ALIGN_LEFT, "Report Date: "+ repDt, 20f,   830f,0f);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT, "Report Time: "+ repTime, 20f,   815f,0f);
				}
				over.endText();
				if(pge>1){
					fover.beginText();
					if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_LICENSEANDELIGIBILTY_REPORT)){
						BaseFont bf2 = BaseFont.createFont(getContext().getRealPath("")+"/jsp/font/arial_narrow_bold.ttf", 
	                            BaseFont.CP1252, BaseFont.EMBEDDED);
	                   fover.setFontAndSize(bf2 , 14f);
	                   fover.setCharacterSpacing(0.70f);
						fover.showTextAligned(PdfContentByte.ALIGN_CENTER, title.toString(), 420f,560f,0f);
						}
						else{
							BaseFont bf2 = BaseFont.createFont(getContext().getRealPath("")+"/jsp/font/arial_narrow_bold.ttf", 
		                            BaseFont.CP1252, BaseFont.EMBEDDED);
		                   fover.setFontAndSize(bf2 , 14f);
		                   fover.setCharacterSpacing(0.70f);
							fover.showTextAligned(PdfContentByte.ALIGN_CENTER, title.toString(), 280f,815f,0f);
						}
					fover.endText();
				}
				
				under.beginText();
				//BaseFont bf = BaseFont.createFont(getContext().getRealPath("")+"/jsp/font/arial_narrow.TTF",BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
				Chunk copyChunk = new Chunk(Character.toString ((char) 169),FontFactory.getFont("arial_narrow", 8,Font.NORMAL));
				Chunk regChunk = new Chunk(Character.toString ((char) 174),FontFactory.getFont("arial_narrow", 8,Font.NORMAL));
				regChunk.setTextRise(5.0f);
				copyChunk.setTextRise(5.0f);
				
				Phrase p1=new Phrase(copyChunk);
				String temp1=p1.get(0).toString();
				
				Phrase p2=new Phrase(regChunk);
				String temp2=p2.get(0).toString();
				
				under.setFontAndSize(bf1 , 8f);
				if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_LICENSEANDELIGIBILTY_REPORT)){
					under.showTextAligned(PdfContentByte.ALIGN_CENTER, getText("garuda.pdf.all.copyright1a")+" "+curYear+" "+temp1+" "+getText("garuda.pdf.all.copyright1b")+temp2+". "+getText("garuda.pdf.all.copyright1c") , 420f, 10f,0f); 
					under.showTextAligned(PdfContentByte.ALIGN_CENTER, getText("garuda.pdf.all.copyright2a")+temp2+" "+getText("garuda.pdf.all.copyright2b"), 420f, 2f,0f);
					under.showTextAligned(PdfContentByte.ALIGN_RIGHT, "Page " + pge + " of " + totalPages, 820f,2f,0f);
if(pge==2){
						
						BaseFont bf = BaseFont.createFont(getContext().getRealPath("")+"/jsp/font/arial_narrow_bold.ttf", 
	                            BaseFont.CP1252, BaseFont.EMBEDDED);
				        PdfGState gs = new PdfGState();
				        gs.setFillOpacity(0.8f);
				        under.setGState(gs);
				        under.setFontAndSize(bf, 14f);
				        under.setColorFill(Color.BLACK);
				        under.showTextAligned(Element.ALIGN_CENTER, getText("garuda.querybuilder.report.blankpage"), 420f, 340f, 0);
				        
					}
				}
				else{
					under.showTextAligned(PdfContentByte.ALIGN_CENTER, getText("garuda.pdf.all.copyright1a")+" "+curYear+" "+temp1+" "+getText("garuda.pdf.all.copyright1b")+temp2+". "+getText("garuda.pdf.all.copyright1c") , 380f, 10f,0f); 
					under.showTextAligned(PdfContentByte.ALIGN_CENTER, getText("garuda.pdf.all.copyright2a")+temp2+" "+getText("garuda.pdf.all.copyright2b"), 380f, 2f,0f);
					under.showTextAligned(PdfContentByte.ALIGN_RIGHT, "Page " + pge + " of " + totalPages, 580f,2f,0f);	
				}
				under.endText();				
			}
			originalPdf.close();
			stamper.close();
			InputStream in = new FileInputStream(new File(outputFilePath));
		    byte[] res ;		    
			response.setContentType("application/pdf;charset=utf8");
			res = IOUtils.toByteArray(in);
			response.setHeader("Content-Type", "application/pdf");
			response.setHeader("Content-Disposition",
					"attachment;filename="+pdfFileName);
			ByteArrayInputStream bis = new ByteArrayInputStream(res);			
			OutputStream out = response.getOutputStream();
			IOUtils.copy(bis,out);
			out.flush();
			out.close();
			bis = null;
			in = null;
			out = null;					
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	private void addEmptyLine(Paragraph paragraph, int number) {
	    for (int i = 0; i < number; i++) {
	      paragraph.add(new Paragraph(" "));
	    }
	 }	
	
	private void addTitlePage(Document document,Font titleFont,Font bodyFontBold,Font bodyFont,List<Object> cdrObjects,StringBuffer title,PdfWriter writer)
	   throws Exception {
	      try{
	    	  
	    	  if(getSelectedSiteId()==null && getSession(getRequest()).getAttribute("selectedSiteId")!=null)
					setSelectedSiteId(Long.parseLong(getSession(getRequest()).getAttribute("selectedSiteId").toString()));
	    	  
		  }catch(Exception e){e.printStackTrace();}
		  Image line = null;
		  PdfPTable table =  null;
		  PdfPCell cell = null;
		  Paragraph preface = null;
		  preface = new Paragraph();
		  float[] columnWidths=null;		  
		  addEmptyLine(preface, 1); 
		  Phrase p = new Phrase();
		  p.add(new Chunk(getText("garuda.pdf.common.nmdp"), titleFont));
		  Chunk regChunk = new Chunk(Character.toString ((char) 174),FontFactory.getFont("arial_narrow", 8,Font.NORMAL));
		  regChunk.setTextRise(5.0f);
		  p.add(regChunk);
		  preface.add(new Paragraph(p));
		  preface.setAlignment(Element.ALIGN_CENTER);
		  addEmptyLine(preface, 1);
		  addEmptyLine(preface, 1);
		  document.add(preface);
		  
		  table = createTable(1,0);
		  columnWidths = new float[] {100f};
		  table.setWidths(columnWidths);	
		  table.setWidthPercentage(100);
		  table.setHorizontalAlignment(0);
		  table.getDefaultCell().setBorder(0);		  
		  
		  
		  p = new Phrase();
		  p.add(new Chunk(getText("garuda.queryBuilder.label.selectionCriteria"),bodyFontBold));
		  if(getSession(getRequest()).getAttribute("selectedFilterCriteriaRep")!=null){
			  String htmlCriteria = getSession(getRequest()).getAttribute("selectedFilterCriteriaRep").toString();
			  InputStream is = new ByteArrayInputStream(htmlCriteria.getBytes());
			  Reader reader = new BufferedReader(new InputStreamReader(is));
			  StyleSheet styles = new StyleSheet();	         			
	          ArrayList arrayElementList = HTMLWorker.parseToList(reader, styles);	       
			  for (int i1 = 0; i1 < arrayElementList.size(); ++i1) {
					 Element e = (Element) arrayElementList.get(i1);
					 p.add(e);
		      }		    
	      }
		  cell = new PdfPCell(p); 
		  cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		  cell.setBorder(0);
		  table.setSplitLate(false);
		  table.setSplitRows(true);
		  table.addCell(cell);
		  
		  p = new Phrase();
		  p.add(new Chunk(getText("garuda.querybuilder.report.totalrecords")+" ",bodyFontBold));
		  Set<Long> uCords =  (Set<Long>)getSession(getRequest()).getAttribute("uniqueCords");
		  if(uCords!=null && uCords.size()>0){
			Integer cnt = uCords.size();
			p.add(new Chunk(cnt.toString(),bodyFontBold));
		  }		  
		  cell = new PdfPCell(p); 
		  cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		  cell.setBorder(0);
		  table.addCell(cell);
		  
		  document.add(table);
		  
		  preface = new Paragraph();
		  addEmptyLine(preface, 1);
		  document.add(preface);
		  
		  
		  preface = new Paragraph();
		  if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_ANTIGEN_REPORT)){
			  title.append(getText("garuda.querybuilder.report.antigen"));
			  title.append(getIsMultipleCBBSelected()!=null && getIsMultipleCBBSelected()==false?getSelectedSiteId()!=null?" For CBB "+getSiteID(getSelectedSiteId()):"":"");
			  preface.add(new Paragraph(title.toString(), titleFont));
	      }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_LICENSEANDELIGIBILTY_REPORT)){
	    	  title.append(getText("garuda.querybuilder.report.licandeligibiltiy"));
			  title.append(getIsMultipleCBBSelected()!=null && getIsMultipleCBBSelected()==false?getSelectedSiteId()!=null?" For CBB "+getSiteID(getSelectedSiteId()):"":"");
			  preface.add(new Paragraph(title.toString(), titleFont));  
	      }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_QUICKSUMMARY_REPORT)){
	    	  title.append(getText("garuda.querybuilder.report.shippedcbus"));
			  title.append(getIsMultipleCBBSelected()!=null && getIsMultipleCBBSelected()==false?getSelectedSiteId()!=null?" For CBB "+getSiteID(getSelectedSiteId()):"":"");
			  preface.add(new Paragraph(title.toString(), titleFont));	    	 
	      }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_IDSUMMARY_REPORT)){
	    	  title.append(getText("garuda.querybuilder.report.cbuids"));
			  title.append(getIsMultipleCBBSelected()!=null && getIsMultipleCBBSelected()==false?getSelectedSiteId()!=null?" For CBB "+getSiteID(getSelectedSiteId()):"":"");
			  preface.add(new Paragraph(title.toString(), titleFont));	    	  
	      }
		  preface.setAlignment(Element.ALIGN_CENTER);
		  addEmptyLine(preface, 1);
		  addEmptyLine(preface, 1);
		  
		 
		  document.add(preface);
		  
		  if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_LICENSEANDELIGIBILTY_REPORT)){
			  //For Water Mark text in second page of CBU Characteristics report
			  document.newPage();
			  writer.setPageEmpty(false);
			  document.newPage();
		  }
		 }
	 
	private Document addContent(Document document,Font bodyFontBold,Font bodyFont,Font bodyFontItalic,Font bodyFontBoldItalic, List<Object> cdrObjects,PdfWriter writer, String pdfFileName, String filePath) throws Exception {
			 PdfPTable table =  null;
			 PdfPTable table1 =  null;
			 PdfPCell cell = null;
			 PdfPCell cell1 = null;
			 PdfPTable tableWidBrd = null;
			 PdfPCell cellWidBrd = null;
			 Paragraph preface = null;
			 Section section = null;
			 Phrase phrase = null;
			 Chunk chunk = null;
			 Image line = null;
			 float[] columnWidths=null;
			 float[] columnWidths1=null;
			 int index=0;
			 int tableCounter=1;
			 String remainingVal = "";
			 String remainingVal1 = "";
			 List<Float> exceedingIndices = new ArrayList<Float>();
			 int exceedLimit = 280;
			 Boolean exceedFlag = false;
			 Boolean allowOnce = false;
			 int exceedIndex = 0;
			 try{
					line =Image.getInstance(getContext().getRealPath("")+"/jsp/images/Line1.bmp");
			 }catch(Exception e){
					e.printStackTrace();
			  }
			 SimpleDateFormat dateFormat = new SimpleDateFormat ("MMM dd, yyyy");
			 if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_ANTIGEN_REPORT)){
				 if(cdrObjects!=null && cdrObjects.size()>0){
				    	/*Map<Long,Map<Long,List<HLAPojo>>> hlas = new HashMap<Long, Map<Long,List<HLAPojo>>>();
				    	Map<Long,List<BestHLAPojo>> bestHlas = new HashMap<Long, List<BestHLAPojo>>();
				    	for(Object o : cdrObjects){
				    		Object[] oArr = (Object[])o;
				    		BigDecimal pkCord  = (BigDecimal)oArr[0];
				    		Map<Long,List<HLAPojo>> mapHla = setHlaMapBYEntityId(pkCord.longValue(),getCodeListPkByTypeAndSubtype(VelosGarudaConstants.CORD_ENTITY_TYPE, VelosGarudaConstants.CORD_ENTITY_SUB_TYPE));
				    		List<HLAPojo> cbuhla = mapHla.get(getCodeListPkByTypeAndSubtype(VelosGarudaConstants.TEST_SOURCES, VelosGarudaConstants.CBU_CODESUBTYPE));
				    		hlas.put(Long.parseLong(pkCord.toString()), setHlaMapForAntigenReport(cbuhla));
				    		List<BestHLAPojo> bhla = setBestHlaByEntityType(getCodeListPkByTypeAndSubtype(ENTITY_CBU_CODE_TYPE, ENTITY_CBU_CODE_SUB_TYPE),pkCord.longValue());
				    		bestHlas.put(Long.parseLong(pkCord.toString()), bhla);			    		
				    	}
				    	setHlas(hlas);
			    		setBestHlas(bestHlas);*/
					    setHlaAntigenDataForPdf();
				 }
				 if(cdrObjects!=null){
				 for(Object o : cdrObjects){
					 Object[] oArr = (Object[])o;
					 PdfPTable outerTable = createTable(1,0);
					 outerTable.setKeepTogether(true);
					 columnWidths = new float[] {100f};
					 outerTable.setWidths(columnWidths);	
					 outerTable.setWidthPercentage(100);
					 outerTable.setHorizontalAlignment(0);
					 outerTable.getDefaultCell().setBorder(0);
					 
					 table = createTable(4,0);
					 columnWidths = new float[] {30f,20f,30f,20f};
					 table.setWidths(columnWidths);	
					 table.setWidthPercentage(100);
					 table.setHorizontalAlignment(0);
					 table.getDefaultCell().setBorder(0);
					 
					 if(getIsMultipleCBBSelected()!=null && getIsMultipleCBBSelected()==true){					 
						 cell = createTableCell(getText("garuda.queryBuilder.label.cbbid"), 0, bodyFontBold, 0) ;
						 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
						 cell.setBorder(Rectangle.BOX);
						 table.addCell(cell);
						 
						 cell = createTableCell(oArr[1]!=null?oArr[1].toString():"", 0, bodyFont, 0) ;
						 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
						 cell.setBorder(Rectangle.BOX);
						 table.addCell(cell);
						 cell = createTableCell(getText(""), 0, bodyFontBold, 0) ;
						 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
						 cell.setBorder(Rectangle.BOX);
						 table.addCell(cell);
						 
						 cell = createTableCell(getText(""), 0, bodyFont, 0) ;
						 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
						 cell.setBorder(Rectangle.BOX);
						 table.addCell(cell);
					 }					 
					 cell = createTableCell(getText("garuda.cbuentry.label.registrycbuid"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(oArr[2]!=null?oArr[2].toString():"", 0, bodyFont, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.cbuentry.label.registrymaternalid"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(oArr[7]!=null?oArr[7].toString():"", 0, bodyFont, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.cbuentry.label.localcbuid"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(oArr[3]!=null?oArr[3].toString():"", 0, bodyFont, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 cell = createTableCell(getText("garuda.cbuentry.label.maternallocalid"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(oArr[8]!=null?oArr[8].toString():"", 0, bodyFont, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.cbuentry.label.idbag"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(oArr[4]!=null?oArr[4].toString():"", 0, bodyFont, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.querybuilderreport.label.activeforpatient"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					// cell = createTableCell(oArr[9]!=null && (oArr[11]!=null && ((BigDecimal)oArr[11]).toString().equals(getCbuStatusByCode(AC).getPkcbustatus().toString()))?oArr[9].toString():"", 0, bodyFont, 0) ;
					 cell = createTableCell(oArr[9]!=null?oArr[9].toString():"", 0, bodyFont, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.idsReport.isbt"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(oArr[5]!=null?oArr[5].toString():"", 0, bodyFont, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.queryBuilder.label.cbucollectiondate"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(oArr[10]!=null?oArr[10].toString():"", 0, bodyFont, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 					 
					 cell = createTableCell(getText("garuda.querybuilderreport.label.historiclocalid"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(oArr[6]!=null?oArr[6].toString():"", 0, bodyFont, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText(""), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText(""), 0, bodyFont, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					
					 
					 //document.add(table);
					 
					 cell = new PdfPCell();
					 cell.setBorder(0);
					 cell.addElement(table);
					 outerTable.addCell(cell);
					 
					 
					 preface = new Paragraph();
					 addEmptyLine(preface, 1);					 
					 //document.add(preface);
					 
					 cell = new PdfPCell();
					 cell.setBorder(0);
					 cell.addElement(preface);
					 outerTable.addCell(cell);
					 
					 List<String> headers = new ArrayList<String>();
					 for(CodeList c : GarudaConfig.getCodeListMap().get(HLA_LOCUS)){
						 headers.add(c.getDescription().replace("HLA-",""));
					 }
					 table = createTable(headers.size()+1, 0);
					 columnWidths = new float[] {28f,8f,8f,8f,8f,8f,8f,8f,8f,8f};
					 table.setWidths(columnWidths);	
					 table.setWidthPercentage(100);
					 table.setHorizontalAlignment(0);
					 table.getDefaultCell().setBorder(0);
					 cell = createTableCell("", Element.ALIGN_CENTER, bodyFontBold, 0);
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 table = createTableHeader(table, headers, Element.ALIGN_CENTER, bodyFontBold, 0);
					 if(getBestHlasMap()!=null && oArr[0]!=null && getBestHlasMap().get(Long.parseLong(oArr[0].toString()))!=null){
						
						 cell = createTableCell(getText("garuda.cbu.label.besthla"), Element.ALIGN_CENTER, bodyFont, 0);
						 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
						 cell.setBorder(Rectangle.BOX);
						 table.addCell(cell);
						 
						 for(CodeList c : GarudaConfig.getCodeListMap().get(HLA_LOCUS)){
							 Boolean traverse = false;
							 for(BestHLAPojo h : getBestHlasMap().get(Long.parseLong(oArr[0].toString()))){
								 if(c.getPkCodeId()==h.getFkHlaCodeId() || c.getPkCodeId().equals(h.getFkHlaCodeId())){
									 traverse = true;
									 phrase = new Phrase();									 
									 phrase.add(new Chunk(h!=null && h.getHlaType1()!=null?h.getHlaType1()+"\n":"",bodyFont));
									 if(!StringUtils.isEmpty(h.getHlaType2()))
									   phrase.add(new Chunk(h.getHlaType2()+"\n",bodyFont));
									 cell = createTableCell(phrase, Element.ALIGN_CENTER, bodyFont, 0);
									 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
									 cell.setBorder(Rectangle.BOX);
									 table.addCell(cell);
								 }
							 }
							 if(!traverse){
								 cell = createTableCell("", Element.ALIGN_CENTER, bodyFont, 0);
								 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
								 cell.setBorder(Rectangle.BOX);
								 table.addCell(cell);
							 }
						 } 
					 }else{// For Bug no 20884
						 cell = createTableCell(getText("garuda.cbu.label.besthla"), Element.ALIGN_CENTER, bodyFont, 0);
						 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
						 cell.setBorder(Rectangle.BOX);
						 cell.setColspan(headers.size()+1);
						 table.addCell(cell);
						 
					 }
					 
					 //document.add(table);
					 
					 cell = new PdfPCell();
					 cell.setBorder(0);
					 cell.addElement(table);
					 outerTable.addCell(cell);
					 
					 preface = new Paragraph();
					 addEmptyLine(preface, 1);
					 //document.add(preface);
					 
					 cell = new PdfPCell();
					 cell.setBorder(0);
					 cell.addElement(preface);
					 outerTable.addCell(cell);
					 
					 table = createTable(headers.size()+3, 0);
					 columnWidths = new float[] {10f,9f,9f,8f,8f,8f,8f,8f,8f,8f,8f,8f};
					 table.setWidths(columnWidths);
					 table.setWidthPercentage(100);
					 table.setHorizontalAlignment(0);
					 table.getDefaultCell().setBorder(0);
					 
					 cell = createTableCell(getText("garuda.cbuentry.label.typingdate"), Element.ALIGN_CENTER, bodyFontBold, 0);
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.queryBuilder.label.recieveddate"), Element.ALIGN_CENTER, bodyFontBold, 0);
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 
					 cell = createTableCell(getText("garuda.cbuentry.label.source"), Element.ALIGN_CENTER, bodyFontBold, 0);
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 table = createTableHeader(table, headers, Element.ALIGN_CENTER, bodyFontBold, 0);
					 
					 if(getHlas()!=null && oArr[0]!=null && getHlas().get(Long.parseLong(oArr[0].toString()))!=null){
						 for(Long  l : getHlas().get(Long.parseLong(oArr[0].toString())).keySet()){
							 
							 List<HLAPojo> hlaList =  getHlas().get(Long.parseLong(oArr[0].toString())).get(l);
							 if(hlaList!=null){
								 cell = createTableCell(hlaList.get(0).getHlaTypingDate()!=null?dateFormat.format(hlaList.get(0).getHlaTypingDate()):"", Element.ALIGN_CENTER, bodyFont, 0);
								 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
								 cell.setBorder(Rectangle.BOX);
								 table.addCell(cell);
								 
								 cell = createTableCell(hlaList.get(0).getHlaRecievedDate()!=null?dateFormat.format(hlaList.get(0).getHlaRecievedDate()):"", Element.ALIGN_CENTER, bodyFont, 0);
								 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
								 cell.setBorder(Rectangle.BOX);
								 table.addCell(cell);
								 
								 
								 if(hlaList.get(0).getLastModifiedByUser()!=null && hlaList.get(0).getLastModifiedByUser().toString()!=""){
									 cell = createTableCell(hlaList.get(0).getLastModifiedByUser(), Element.ALIGN_CENTER, bodyFont, 0);
								 }else if(hlaList.get(0).getuName()!=null && hlaList.get(0).getuName()!=""){
									 cell = createTableCell(hlaList.get(0).getuName(), Element.ALIGN_CENTER, bodyFont, 0);
								 }
								 
								 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
								 cell.setBorder(Rectangle.BOX);
								 table.addCell(cell);
								 
								 for(CodeList c : GarudaConfig.getCodeListMap().get(HLA_LOCUS)){
									 Boolean traverse = false;
									 for(HLAPojo h : hlaList){
										 if(c.getPkCodeId()==h.getFkHlaCodeId() || c.getPkCodeId().equals(h.getFkHlaCodeId())){
											 traverse = true;
											 phrase = new Phrase();		
											 if(!StringUtils.isEmpty(h.getHlaType1()))
											  phrase.add(new Chunk(h.getHlaType1()+"\n",bodyFont));
											 if(!StringUtils.isEmpty(h.getHlaType2()))
											   phrase.add(new Chunk(h.getHlaType2()+"\n",bodyFont));
											 cell = createTableCell(phrase, Element.ALIGN_CENTER, bodyFont, 0);
											 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
											 cell.setBorder(Rectangle.BOX);
											 table.addCell(cell);
										 }
									 }
									 if(!traverse){
										 cell = createTableCell("", Element.ALIGN_CENTER, bodyFont, 0);
										 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
										 cell.setBorder(Rectangle.BOX);
										 table.addCell(cell);
									 }
								 } 
							 }
						 }
					 }
					// document.add(table);
					 
					 cell = new PdfPCell();
					 cell.setBorder(0);
					 cell.addElement(table);
					 outerTable.addCell(cell);
					 
					 preface = new Paragraph();
					 addEmptyLine(preface, 1);
					 //document.add(preface);
					 
					 cell = new PdfPCell();
					 cell.setBorder(0);
					 cell.addElement(preface);
					 outerTable.addCell(cell);
					 
					 table =  createTable(1,0);
					 table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
					 cell = new PdfPCell();
					 cell = createTableCell(line, Element.ALIGN_LEFT,bodyFont,0);
					 table.addCell(cell);
					 //document.add(table);
					 cell = new PdfPCell();
					 cell.setBorder(0);
					 cell.addElement(table);
					 outerTable.addCell(cell);
					 
					document.add(outerTable);
					 
				 }
				}				 
		      }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_LICENSEANDELIGIBILTY_REPORT)){
		    	 
		    	  if(cdrObjects!=null){
		    		  int i=0;
		    		  int j=0;
		    		  int incrementCounter=0;
		    		  while(i<cdrObjects.size()){
		    			  int t1 = cdrObjects.size()-i;
		    			  if(t1>5){
		    				  i=i+5;
		    			  }
		    			  else{
		    				  i=cdrObjects.size();
		    			  }
		    			for(j=index;j<cdrObjects.size();j++){
		    				  if(getIsMultipleCBBSelected()!=null && getIsMultipleCBBSelected())
				    		  {
				    			   	 	 table = createTable(16,0);								 
				    					 columnWidths = new float[] {4f,6f,6f,6f,4f,5f,5f,7f,8f,11f,7f,9f,9f,5f,8f,8f};
				    					 table1 = createTable(16,0);
				    					 columnWidths1 = new float[] {6f,6f,6f,6f,9f,8f,6f,6f,5f,6f,6f,6f,4f,4f,6f,4f};
				    		 }else{
				    			  		table = createTable(15,0);								 
				    			  		columnWidths = new float[] {4f,6f,6f,7f,6f,6f,5f,7f,11f,8f,9f,9f,5f,8f,8f};
				    		 	 		table1 = createTable(16,0);
				    		 	 		columnWidths1 = new float[] {6f,6f,6f,6f,9f,8f,6f,6f,5f,6f,6f,6f,4f,4f,6f,4f};
				    		 }								 
					 table.setTotalWidth(columnWidths);
					 table.setHorizontalAlignment(0);
					 table.getDefaultCell().setBorder(0);
					 //table.setSplitRows(true);
					 //table.setSplitLate(false);
					 //table.setHeaderRows(1);
					 table1.setTotalWidth(columnWidths1);
					 table1.setHorizontalAlignment(0);
					 table1.getDefaultCell().setBorder(0);
					 //table1.setSplitRows(true);
					 //table1.setSplitLate(false);
					 //table1.setHeaderRows(1);
					 float titleFixedHeight=111.2f;
					 
					 if(getIsMultipleCBBSelected()!=null && getIsMultipleCBBSelected()==true){					 
						 cell = createTableCell(getText("garuda.queryBuilder.label.cbbid"), 0, bodyFontBold, 0);
						 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
						 cell.setBorder(Rectangle.BOX);
						 cell.setFixedHeight(titleFixedHeight);
						 table.addCell(cell);
					 }
					 
					 cell = createTableCell(getText("garuda.cbuentry.label.registrycbuid"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 cell.setFixedHeight(titleFixedHeight);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.cbuentry.label.localcbuid"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 cell.setFixedHeight(titleFixedHeight);
					 table.addCell(cell);
					
					 
					 cell = createTableCell(getText("garuda.cbuentry.label.idbag"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 cell.setMinimumHeight(titleFixedHeight);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.queryBuilder.label.cbucollectiondate"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 cell.setFixedHeight(titleFixedHeight);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.queryBuilder.nmdpregistrationDates"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 cell.setFixedHeight(titleFixedHeight);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.cbuentry.label.cbu_status"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 cell.setFixedHeight(titleFixedHeight);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.clinicalnote.label.reason"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 cell.setFixedHeight(titleFixedHeight);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.cdrcbuview.label.licensure_status"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 cell.setFixedHeight(titleFixedHeight);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.evaluHistory.report.unlicensedReason"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 cell.setFixedHeight(titleFixedHeight);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.cdrcbuview.label.eligibility_determination"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 cell.setFixedHeight(titleFixedHeight);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.queryBuilder.ineligiblereason"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 cell.setFixedHeight(titleFixedHeight);
					 table.addCell(cell);
					 				 
					 cell = createTableCell(getText("garuda.queryBuilder.incompletereason"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 cell.setFixedHeight(titleFixedHeight);
					 table.addCell(cell);
					 					 
					 cell = createTableCell(getText("garuda.queryBuilder.babyBirthDate"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 cell.setFixedHeight(titleFixedHeight);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.queryBuilder.race"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 cell.setFixedHeight(titleFixedHeight);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.cordentry.label.nmdpbroadrace"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 cell.setFixedHeight(titleFixedHeight);
					 table.addCell(cell);
					 
					 cell1 = createTableCell(getText("garuda.pendingorderpage.label.funded"), 0, bodyFontBold, 0) ;
					 cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell1.setBorder(Rectangle.BOX);
					 cell1.setFixedHeight(titleFixedHeight);
					 table1.addCell(cell1);
					 
					 cell1 = createTableCell(getText("garuda.queryBuilder.fundingcategory"), 0, bodyFontBold, 0) ;
					 cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell1.setBorder(Rectangle.BOX);
					 cell1.setFixedHeight(titleFixedHeight);
					 table1.addCell(cell1);
					 
					 cell1 = createTableCell(getText("garuda.cbuentry.label.bcresult"), 0, bodyFontBold, 0) ;
					 cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell1.setBorder(Rectangle.BOX);
					 cell1.setFixedHeight(titleFixedHeight);
					 table1.addCell(cell1);
					 
					 cell1 = createTableCell(getText("garuda.cbuentry.label.fcresult"), 0, bodyFontBold, 0) ;
					 cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell1.setBorder(Rectangle.BOX);
					 cell1.setFixedHeight(titleFixedHeight);
					 table1.addCell(cell1);
					 
					 cell1 = createTableCell(getText("garuda.minimumcriteria.label.hemoglobin"), 0, bodyFontBold, 0) ;
					 cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell1.setBorder(Rectangle.BOX);
					 cell1.setFixedHeight(titleFixedHeight);
					 table1.addCell(cell1);
					 
					 cell1 = createTableCell(getText("garuda.cbbprocedures.label.processingprocedure"), 0, bodyFontBold, 0) ;
					 cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell1.setBorder(Rectangle.BOX);
					 cell1.setFixedHeight(titleFixedHeight);
					 table1.addCell(cell1);
					 
					 cell1 = createTableCell(getText("garuda.queryBuilder.label.totalcbunucleatedcount"), 0, bodyFontBold, 0) ;
					 cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell1.setBorder(Rectangle.BOX);
					 cell1.setFixedHeight(titleFixedHeight);
					 table1.addCell(cell1);
					 
					 cell1 = createTableCell(getText("garuda.queryBuilder.label.totalcbunucleatedcountun"), 0, bodyFontBold, 0) ;
					 cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell1.setBorder(Rectangle.BOX);
					 cell1.setFixedHeight(titleFixedHeight);
					 table1.addCell(cell1);
					 
					 cell1 = createTableCell(getText("garuda.queryBuilder.totalcd34"), 0, bodyFontBold, 0) ;
					 cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell1.setBorder(Rectangle.BOX);
					 cell1.setFixedHeight(titleFixedHeight);
					 table1.addCell(cell1);
					 
					 cell1 = createTableCell(getText("garuda.queryBuilder.viabilitycount"), 0, bodyFontBold, 0) ;
					 cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell1.setBorder(Rectangle.BOX);
					 cell1.setFixedHeight(titleFixedHeight);
					 table1.addCell(cell1);
					 
					 cell1 = createTableCell(getText("garuda.common.lable.viability"), 0, bodyFontBold, 0) ;
					 cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell1.setBorder(Rectangle.BOX);
					 cell1.setFixedHeight(titleFixedHeight);
					 table1.addCell(cell1);
					 
					 cell1 = createTableCell(getText("garuda.productinsert.label.finalProdVol"), 0, bodyFontBold, 0) ;
					 cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell1.setBorder(Rectangle.BOX);
					 cell1.setFixedHeight(titleFixedHeight);
					 table1.addCell(cell1);
					 
					 cell1 = createTableCell(getText("garuda.idsReport.ctshipdate"), 0, bodyFontBold, 0) ;
					 cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell1.setBorder(Rectangle.BOX);
					 cell1.setFixedHeight(titleFixedHeight);
					 table1.addCell(cell1);
					 
					 cell1 = createTableCell(getText("garuda.idsReport.orshipdate"), 0, bodyFontBold, 0) ;
					 cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell1.setBorder(Rectangle.BOX);
					 cell1.setFixedHeight(titleFixedHeight);
					 table1.addCell(cell1);
					 
					 cell1 = createTableCell(getText("garuda.queryBuilder.label.requestType"), 0, bodyFontBold, 0) ;
					 cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell1.setBorder(Rectangle.BOX);
					 cell1.setFixedHeight(titleFixedHeight);
					 table1.addCell(cell1);
						 
					 cell1 = createTableCell(getText("garuda.idsReport.reqdate"), 0, bodyFontBold, 0) ;
					 cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell1.setBorder(Rectangle.BOX);
					 cell1.setFixedHeight(titleFixedHeight);
					 table1.addCell(cell1);
					
					 float totColumns = 0l;
					 float noI =1l;
					 String higherIndex = "";
					 if(!allowOnce){
					 if(exceedFlag && (remainingVal!="" || remainingVal1!="") ){
						 float minHeight = 59.42857f;
						if(remainingVal!=""){
						 
							 higherIndex = remainingVal;
						 //System.out.println("\n\n Inside Else If:::"+higherIndex);
						 if(getIsMultipleCBBSelected()!=null){
							 totColumns = getIsMultipleCBBSelected()?32:31;
						 }
						 for(float ind=1l; ind<=totColumns;ind++){
							 String tempString = "";
								 if(exceedingIndices.get(0)!=null && ind == exceedingIndices.get(0)){
									 tempString = remainingVal.substring(0, remainingVal.length()>exceedLimit?remainingVal.substring(0,exceedLimit).lastIndexOf(','):remainingVal.length());
								 }
								 if(tempString!=""){
									 PdfPCell tempCell = createTableCell(tempString!=null?tempString:"", 0, bodyFont, 0) ;
									 tempCell.setHorizontalAlignment(Element.ALIGN_LEFT);
									 tempCell.setBorder(Rectangle.BOX);
									 tempCell.setMinimumHeight(minHeight);
									 if((totColumns==32 && ind<=16)){
										 table.addCell(tempCell);
									 }
									 else if((totColumns==31 && ind<=15)){
										 table.addCell(tempCell);
									}
									 else{
										 table1.addCell(tempCell);
									}
								 }
								 
								 else{
									 PdfPCell tempCell = createTableCell("", 0, bodyFont, 0) ;
									 tempCell.setHorizontalAlignment(Element.ALIGN_LEFT);
									 tempCell.setBorder(Rectangle.BOX);
									 tempCell.setMinimumHeight(minHeight);
									 if((totColumns==32 && ind<=16)){
										 table.addCell(tempCell);
									 }
									 else if((totColumns==31 && ind<=15)){
										 table.addCell(tempCell);
									 }
									 else{
										 table1.addCell(tempCell);
									 }
								 }
							 }
						 if(remainingVal!=""){
							 if(remainingVal.length()>exceedLimit){
								 String temprace = remainingVal.substring(0,exceedLimit);
								 String temp1=remainingVal.substring(temprace.lastIndexOf(','),remainingVal.length());
								 remainingVal= temp1;
							 }
							 else{
								 remainingVal="";
							 }
						 }
						 //System.out.println("\n\n remainingVal.size()::::"+remainingVal.length()+"remainingVal::::"+remainingVal);
						 if(remainingVal==""){
							 //System.out.println("Remain val empty exceedIndex"+exceedIndex);
							 //System.out.println("");
							 if(cdrObjects.size()>1){
								 if(index!=(cdrObjects.size()-1)){
									 exceedFlag=false;
									 allowOnce=false;
									 index = exceedIndex+1;
								 }
								 else{
									 incrementCounter = exceedIndex+1;
									 
								 }
							 }
							 if(j==(cdrObjects.size()-1)){
								 incrementCounter = cdrObjects.size();
							 }
							 exceedingIndices.clear();
						 }
						 else{
							 incrementCounter = index+1;
							 
						 }
						 //System.out.println("Inside else in remain val:::"+incrementCounter);
					 }
				 }
				}	 
				if(!exceedFlag || allowOnce){
					 if(cdrObjects!=null){
						 
						 int loopIndex=0;
						 
							for(Object o : cdrObjects){
									 Object[] oArr = (Object[])o;
									 incrementCounter=incrementCounter+1;
									 //System.out.println("Index:::"+index+"Increment Counter:::"+incrementCounter);									 
									 if((index!=0) && (incrementCounter<=index)){
										 continue;
									 }
									 
									 loopIndex=loopIndex+1;
									 
									 if(loopIndex==6){
										 break;
									 }
									
									 float minHeight = 59.42857f;
									 
									 PdfPCell pcell1=null;							 
									 /*CBB ID*/
									 if(getIsMultipleCBBSelected()!=null && getIsMultipleCBBSelected()==true){					 
										 pcell1 = createTableCell(oArr[1]!=null?oArr[1].toString():"", 0, bodyFont, 0) ;
										 pcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
										 pcell1.setBorder(Rectangle.BOX);
										 pcell1.setMinimumHeight(minHeight);
										 table.addCell(pcell1);
										 
									 }
									 
									 /*CBU Reg ID*/
									 PdfPCell pcell2 = createTableCell(oArr[2]!=null?oArr[2].toString():"", 0, bodyFont, 0) ;
									 pcell2.setHorizontalAlignment(Element.ALIGN_LEFT);
									 pcell2.setBorder(Rectangle.BOX);
									 if(getSession(getRequest()).getAttribute("orDuplCords")!=null && oArr[0]!=null && ((Map<Long,Long>)getSession(getRequest()).getAttribute("orDuplCords")).get(((BigDecimal)oArr[0]).longValue())!=null)
										 pcell2.setBackgroundColor(WebColors.getRGBColor("#FF0000"));
									 pcell2.setMinimumHeight(minHeight);
									 table.addCell(pcell2);
									 
									 
									 /*CBU Local ID*/
									 PdfPCell pcell3 = createTableCell(oArr[3]!=null?oArr[3].toString():"", 0, bodyFont, 0) ;
									 pcell3.setHorizontalAlignment(Element.ALIGN_LEFT);
									 pcell3.setBorder(Rectangle.BOX);
									 pcell3.setMinimumHeight(minHeight);
									 table.addCell(pcell3);
									 
									 /*Unique Prod ID*/
									 PdfPCell pcell4 = createTableCell(oArr[4]!=null?oArr[4].toString():"", 0, bodyFont, 0) ;
									 pcell4.setHorizontalAlignment(Element.ALIGN_LEFT);
									 pcell4.setBorder(Rectangle.BOX);
									 pcell4.setMinimumHeight(minHeight);
									 table.addCell(pcell4);
									 
									 
									 /*Collection Dt*/
									 PdfPCell pcell5 = createTableCell(oArr[5]!=null?oArr[5].toString():"", 0, bodyFont, 0) ;
									 pcell5.setHorizontalAlignment(Element.ALIGN_LEFT);
									 pcell5.setBorder(Rectangle.BOX);
									 pcell5.setMinimumHeight(minHeight);
									 table.addCell(pcell5);
									 
									 
									 /*NMDP Registration Dt*/
									 PdfPCell pcell6 = createTableCell(oArr[6]!=null?oArr[6].toString():"", 0, bodyFont, 0) ;
									 pcell6.setHorizontalAlignment(Element.ALIGN_LEFT);
									 pcell6.setBorder(Rectangle.BOX);
									 pcell6.setMinimumHeight(minHeight);
									 table.addCell(pcell6);
									 
									 
									 /*CBU Status*/
									 PdfPCell pcell7 = createTableCell(oArr[7]!=null?oArr[7].toString():"", 0, bodyFont, 0) ;
									 pcell7.setHorizontalAlignment(Element.ALIGN_LEFT);
									 pcell7.setBorder(Rectangle.BOX);
									 pcell7.setMinimumHeight(minHeight);
									 table.addCell(pcell7);
									 
									 
									 /*Reason*/
									 PdfPCell pcell8 = createTableCell(oArr[8]!=null?oArr[8].toString():"", 0, bodyFont, 0) ;
									 pcell8.setHorizontalAlignment(Element.ALIGN_LEFT);
									 pcell8.setBorder(Rectangle.BOX);
									 pcell8.setMinimumHeight(minHeight);
									 table.addCell(pcell8);
									 
									 
									 /*Licensure Status*/
									 PdfPCell pcell9 = createTableCell(oArr[9]!=null?oArr[9].toString():"", 0, bodyFont, 0) ;
									 pcell9.setHorizontalAlignment(Element.ALIGN_LEFT);
									 pcell9.setBorder(Rectangle.BOX);
									 pcell9.setMinimumHeight(minHeight);
									 table.addCell(pcell9);
									 
									 
									 /*Unlicensed Reason*/
									 PdfPCell pcell10 = createTableCell(oArr[10]!=null?oArr[10].toString():"", 0, bodyFont, 0) ;
									 pcell10.setHorizontalAlignment(Element.ALIGN_LEFT);
									 pcell10.setBorder(Rectangle.BOX);
									 pcell10.setMinimumHeight(minHeight);
									 table.addCell(pcell10);
																		 
									 /*Eligibility Determination*/
									 PdfPCell pcell11 = createTableCell(oArr[11]!=null?oArr[11].toString():"Not Yet Determined", 0, bodyFont, 0) ;
									 pcell11.setHorizontalAlignment(Element.ALIGN_LEFT);
									 pcell11.setBorder(Rectangle.BOX);
									 pcell11.setMinimumHeight(minHeight);
									 table.addCell(pcell11);
									 
									 
									 /*InEligible Reasons*/
									PdfPCell pcell12 = createTableCell((oArr[11]!=null && oArr[11].toString().equals(INELIGIBLE_STR)) && oArr[12]!=null?oArr[12].toString():"", 0, bodyFont, 0) ;
										 	 pcell12.setHorizontalAlignment(Element.ALIGN_LEFT);
										 	 pcell12.setBorder(Rectangle.BOX);
										 	 pcell12.setMinimumHeight(minHeight);
										 	 table.addCell(pcell12);
										
										 
			            	        			              	        		
			          	        	 /*Incomplete Reasons*/
										 	 PdfPCell pcell13 = createTableCell((oArr[11]!=null && oArr[11].toString().equals(INCOMPLETE_STR)) && oArr[12]!=null?oArr[12].toString():"", 0, bodyFont, 0) ;
											 pcell13.setHorizontalAlignment(Element.ALIGN_LEFT);
											 pcell13.setBorder(Rectangle.BOX);
											 pcell13.setMinimumHeight(minHeight);
											 table.addCell(pcell13);
									 
										
									 
									 /*BirthDate*/
									 PdfPCell pcell14 = createTableCell(oArr[13]!=null?oArr[13].toString():"", 0, bodyFont, 0) ;
									 pcell14.setHorizontalAlignment(Element.ALIGN_LEFT);
									 pcell14.setBorder(Rectangle.BOX);
									 pcell14.setMinimumHeight(minHeight);
									 table.addCell(pcell14);
									 
									 
									 /*Race*/
									 String race = "";
									 int startIndex=0;
									 if(oArr[14]!=null && oArr[14].toString().length()>exceedLimit){
										 
										 if(allowOnce || cdrObjects.size()==1){
											 String temprace = oArr[14].toString().substring(0,exceedLimit);
											 race=oArr[14].toString().substring(0,temprace.lastIndexOf(','));
											 if(temprace.length()!=race.length()){
												 for(int in=0;in<temprace.length();in++){
													 race += "";
												 }
											 }
											 //System.out.println("\n\n\n Race Value endup by comma"+race);
											 startIndex = temprace.lastIndexOf(',');
											 //System.out.println("Index of Comma"+temprace.lastIndexOf(','));
										 }
										 else{
											 race = oArr[14].toString();
										 }
										 //System.out.println("\n\n\n allowOnce:::"+allowOnce+"Race::::"+race);
										 
										 exceedingIndices.add(0,(!getIsMultipleCBBSelected())?(float) 14:(float) 15);
										 remainingVal = oArr[14].toString().substring(startIndex,oArr[14].toString().length());
										 exceedFlag=true;
									 }
									 else if(oArr[14]!=null){
										 race = oArr[14].toString();
									 }
									 //System.out.println("Race::::::::::"+race+"Inc Counter"+incrementCounter);
									 PdfPCell pcell15 = createTableCell(race, 0, bodyFont, 0) ;
									 pcell15.setHorizontalAlignment(Element.ALIGN_LEFT);
									 pcell15.setBorder(Rectangle.BOX);
									 pcell15.setMinimumHeight(minHeight);
									 table.addCell(pcell15);
									 
									 
									 
									 /*Race Summary*/
									 String raceSumm = "";
									 int startIndex1=0;
									 if(oArr[15]!=null && oArr[15].toString().length()>exceedLimit){
										 if(allowOnce || cdrObjects.size()==1){
											 String temprace = oArr[14].toString().substring(0,exceedLimit);
											 raceSumm=oArr[14].toString().substring(0,temprace.lastIndexOf(','));
											 if(temprace.length()!=raceSumm.length()){
												 for(int in=0;in<temprace.length();in++){
													 raceSumm += "";
												 }
											 }
											 startIndex1 = temprace.lastIndexOf(',');
											 //System.out.println("\n\n\n raceSumm Value endup by comma"+raceSumm);
										 }
										 else{
											 raceSumm = oArr[15].toString();
										 }
										 //System.out.println("\n\n\n allowOnce:::"+allowOnce+"RaceSumm::::"+raceSumm);
										 if(!exceedFlag){
											 exceedingIndices.add(0,(!getIsMultipleCBBSelected())?(float) 15:(float) 16);
											 remainingVal = oArr[15].toString().substring(startIndex1,oArr[15].toString().length());
										 }
										 else{
											 exceedingIndices.add(1,(!getIsMultipleCBBSelected())?(float) 15:(float) 16);
											 remainingVal1 = oArr[15].toString().substring(startIndex1,oArr[15].toString().length());
										 }
										 exceedFlag=true;
									 }
									 else if(oArr[15]!=null){
										 raceSumm = oArr[15].toString();
									 }
									 PdfPCell pcell16 = createTableCell(raceSumm, 0, bodyFont, 0) ;
									 pcell16.setHorizontalAlignment(Element.ALIGN_LEFT);
									 pcell16.setBorder(Rectangle.BOX);
									 pcell16.setMinimumHeight(minHeight);
									 table.addCell(pcell16);
									 
									 /*Funded*/
									 PdfPCell pcell17 = createTableCell(oArr[16]!=null?oArr[16].toString():"", 0, bodyFont, 0) ;
									 pcell17.setHorizontalAlignment(Element.ALIGN_LEFT);
									 pcell17.setBorder(Rectangle.BOX);
									 pcell17.setMinimumHeight(minHeight);
									 table1.addCell(pcell17);
									 
									 /*Funding Category*/
									 PdfPCell pcell18 = createTableCell(oArr[17]!=null?oArr[17].toString():"", 0, bodyFont, 0) ;
									 pcell18.setHorizontalAlignment(Element.ALIGN_LEFT);
									 pcell18.setBorder(Rectangle.BOX);
									 pcell18.setMinimumHeight(minHeight);
									 table1.addCell(pcell18);
									 
									 /*Bacterial Culture Dt*/
									 PdfPCell pcell19 = createTableCell(oArr[18]!=null?oArr[18].toString():"", 0, bodyFont, 0) ;
									 pcell19.setHorizontalAlignment(Element.ALIGN_LEFT);
									 pcell19.setBorder(Rectangle.BOX);
									 pcell19.setMinimumHeight(minHeight);
									 table1.addCell(pcell19);
									 
									 /*Fungal Culture Dt*/
									 PdfPCell pcell20 = createTableCell(oArr[19]!=null?oArr[19].toString():"", 0, bodyFont, 0) ;
									 pcell20.setHorizontalAlignment(Element.ALIGN_LEFT);
									 pcell20.setBorder(Rectangle.BOX);
									 pcell20.setMinimumHeight(minHeight);
									 table1.addCell(pcell20);
									 
									 /*Hemoglobinopathy testing*/
									 PdfPCell pcell21 = createTableCell(oArr[20]!=null?oArr[20].toString():"", 0, bodyFont, 0) ;
									 pcell21.setHorizontalAlignment(Element.ALIGN_LEFT);
									 pcell21.setBorder(Rectangle.BOX);
									 pcell21.setMinimumHeight(minHeight);
									 table1.addCell(pcell21);
									 
									 /*Processing Procedure*/
									 PdfPCell pcell22 = createTableCell(oArr[21]!=null?oArr[21].toString():"", 0, bodyFont, 0) ;
									 pcell22.setHorizontalAlignment(Element.ALIGN_LEFT);
									 pcell22.setBorder(Rectangle.BOX);
									 pcell22.setMinimumHeight(minHeight);
									 table1.addCell(pcell22);
									 
									 /*TNCC*/
									 PdfPCell pcell23 = createTableCell(oArr[22]!=null?oArr[22].toString():"", 0, bodyFont, 0) ;
									 pcell23.setHorizontalAlignment(Element.ALIGN_LEFT);
									 pcell23.setBorder(Rectangle.BOX);
									 pcell23.setMinimumHeight(minHeight);
									// cell1.setFixedHeight(rowHeight);
									 table1.addCell(pcell23);
									 
									 /*TNCC(Unknown if uncorrected)*/
									 PdfPCell pcell24 = createTableCell(oArr[23]!=null?oArr[23].toString():"", 0, bodyFont, 0) ;
									 pcell24.setHorizontalAlignment(Element.ALIGN_LEFT);
									 pcell24.setBorder(Rectangle.BOX);
									 pcell24.setMinimumHeight(minHeight);
									 //cell1.setFixedHeight(rowHeight);
									 table1.addCell(pcell24);
									 
									 /*TN CD34 CC*/
									 PdfPCell pcell25 = createTableCell(oArr[24]!=null?oArr[24].toString():"", 0, bodyFont, 0) ;
									 pcell25.setHorizontalAlignment(Element.ALIGN_LEFT);
									 pcell25.setBorder(Rectangle.BOX);
									 pcell25.setMinimumHeight(minHeight);
									 table1.addCell(pcell25);
									 
									 /*Viability Count*/
									 PdfPCell pcell26 = createTableCell(oArr[25]!=null?oArr[25].toString():"", 0, bodyFont, 0) ;
									 pcell26.setHorizontalAlignment(Element.ALIGN_LEFT);
									 pcell26.setBorder(Rectangle.BOX);
									 pcell26.setMinimumHeight(minHeight);
									 table1.addCell(pcell26);
									 
									 /*Viability Method*/
									 PdfPCell pcell27 = createTableCell(oArr[26]!=null?oArr[26].toString():"", 0, bodyFont, 0) ;
									 pcell27.setHorizontalAlignment(Element.ALIGN_LEFT);
									 pcell27.setBorder(Rectangle.BOX);
									 pcell27.setMinimumHeight(minHeight);
									 table1.addCell(pcell27);
									 
									 /*fnpv*/	 
									 PdfPCell pcell28 = createTableCell(oArr[27]!=null?oArr[27].toString():"", 0, bodyFont, 0) ;
									 pcell28.setHorizontalAlignment(Element.ALIGN_LEFT);
									 pcell28.setBorder(Rectangle.BOX);
									 pcell28.setMinimumHeight(minHeight);
									 table1.addCell(pcell28);
									 
									 /*CT Ship Date*/
									 PdfPCell pcell29 = createTableCell(oArr[30]!=null && oArr[28]!=null && (oArr[30].toString().trim().equals(ORDER_TYPE_CT_SHIP))?oArr[28].toString():"", 0, bodyFont, 0) ;
									 pcell29.setHorizontalAlignment(Element.ALIGN_LEFT);
									 pcell29.setBorder(Rectangle.BOX);
									 pcell29.setMinimumHeight(minHeight);
									 table1.addCell(pcell29);
									 
									 /*OR Ship Date*/
									 PdfPCell pcell30 = createTableCell(oArr[30]!=null && oArr[29]!=null && (oArr[30].toString().trim().equals(OR))?oArr[29].toString():"", 0, bodyFont, 0) ;
									 pcell30.setHorizontalAlignment(Element.ALIGN_LEFT);
									 pcell30.setBorder(Rectangle.BOX);
									 pcell30.setMinimumHeight(minHeight);
									 table1.addCell(pcell30);
									 
									 /*Workflow*/
										 PdfPCell pcell31 = createTableCell(oArr[30]!=null?oArr[30].toString():"", 0, bodyFont, 0) ;
										 pcell31.setHorizontalAlignment(Element.ALIGN_LEFT);
										 pcell31.setBorder(Rectangle.BOX);
										 pcell31.setMinimumHeight(minHeight);
										 //cell1.setFixedHeight(rowHeight);
										 table1.addCell(pcell31);
										 
										 /*Request Dt*/
										 PdfPCell pcell32 = createTableCell(oArr[31]!=null?oArr[31].toString():"", 0, bodyFont, 0) ;
										 pcell32.setHorizontalAlignment(Element.ALIGN_LEFT);
										 pcell32.setBorder(Rectangle.BOX);
										 pcell32.setMinimumHeight(minHeight);
										 //cell1.setFixedHeight(rowHeight);
										 table1.addCell(pcell32);
										 
									 Boolean allowOnceTemp=false;	 
									 if(exceedFlag){
										 //System.out.println("incrementCounter::::"+incrementCounter+"Size:::"+cdrObjects.size());
										 if(allowOnce==false && cdrObjects.size()!=1){
											 allowOnceTemp= true;
											 exceedIndex=incrementCounter-1;
											 //System.out.println("Inside If allowOnce::::"+allowOnce);
										 }
										 else{
											 allowOnce= false;
											 exceedIndex=incrementCounter-1;
											 /*exceedIndex=incrementCounter-1;
											 if(cdrObjects.size()==1 || incrementCounter==cdrObjects.size()){
												 exceedIndex -= 1;
												 System.out.println("Inside If :::"+exceedIndex);
											 }*/
											 //System.out.println("exceedIndex::::"+exceedIndex);
											 //System.out.println("Inside Else allowOnce::::"+allowOnce);
											 
										 }
										 
										if(allowOnceTemp){
											
											allowOnce = true;
										}
										if((incrementCounter+1)<cdrObjects.size()){
											incrementCounter = incrementCounter + 1;
										}
										break;
									 }
									 
									 //float rowHeight1=(table1.getRowHeight(loopIndex)/7f)-4f;
							}
													
				    	 }
					 }
				if(remainingVal=="" && (cdrObjects.size()==1 || index==(cdrObjects.size()-1))){
					exceedFlag = false;
				}
				
					 		 int currIndex=1;
				    	     int indextemp=incrementCounter;
				    	     int sum=0;
				    	     int maxTableLimit=500;
				    	     Boolean deletionFlag = false;
				    	     int tempCurrIndex=0;
				    	     int totalRows = 0;
				    	     if(incrementCounter!=0){
				    	    		 	    	 
				    	    	 Document tempdocument = new Document(PageSize.A4, 25, 25, 50, 40);
				    	    	 File f = new File(filePath);
				    	    	 FileOutputStream fos =new FileOutputStream(f);
				    	    	 PdfWriter tempwriter=PdfWriter.getInstance(tempdocument, fos);
								 tempdocument.setPageSize(PageSize.A4.rotate());
								 tempdocument.open();
								 tempdocument.add(table);
								 totalRows=table.getRows().size();
								 int noOfRows = totalRows;
								 //System.out.println("\n\n\nTotal Rows::::"+totalRows+"\n\n\n");
								 while(currIndex<totalRows){
				    	    		 
				    	    		 PdfPCell cell145[]=table1.getRow(currIndex).getCells();
				    	    		 cell145[0].setMinimumHeight(table.getRowHeight(currIndex));
				    	    		 sum +=table.getRowHeight(currIndex);
				    	    		 //System.out.println("Sum Value::"+sum);
				    	    		 float totalHeight = sum+titleFixedHeight;
				    	    		 //System.out.println("\n\n\n\n\ntotalHeight Value:::::"+totalHeight+"\t\t\t\t currIndex:::::"+currIndex);
				    	    		 //System.out.println("Condition Values::::"+table.getRowHeight(currIndex)+"==="+(maxTableLimit-titleFixedHeight));
				    	    		 if((totalHeight > maxTableLimit) || deletionFlag){
				    	    			 //System.out.println("tempCurrIndex::::"+tempCurrIndex+"Current Index:::"+currIndex);
			    	    					 table.deleteRow(currIndex);
				    	    				 table1.deleteRow(currIndex);
				    	    				 if(!deletionFlag){
				    	    					tempCurrIndex=currIndex;
				    	    				 }
				    	    				 if(currIndex==(totalRows-1)){
				    	    					 if(tempCurrIndex==1){
				    	    						 table.flushContent();
				    	    						 table1.flushContent();
				    	    						 //System.out.println("None::::::"+tempCurrIndex);
				    	    					 }
				    	    					 incrementCounter=incrementCounter-(noOfRows - tempCurrIndex);
				    	    					 //System.out.println("\n\n\n\nIncrement Counter Value::::"+incrementCounter+"\n\n\n");
				    	    				 }
				    	    				 totalRows -= 1;
				    	    				 currIndex -= 1;
				    	    				 deletionFlag=true;
				    	    			}
				    	    		 currIndex +=1;
				    	    		 //System.out.println("after Current Index:::"+currIndex);
				    	    	 }
				    	    	tempdocument.close();
								tempwriter.close();
								fos.close();
								f.delete();
							}
				    	     
				    	     //table.setComplete(true);
				    	    if(tempCurrIndex!=1){
				    	    	document.newPage();
				    	    	document.add(table);
				    	    	//table1.setComplete(true);
				    	    	document.newPage();
				    	    	document.add(table1);
				    	     }
			    	     
			    	     
				    	    	
				    	    if((index+(totalRows-1)) != cdrObjects.size()){
								incrementCounter -= 1;
								//System.out.println("\n\nInside If inc counter::::"+incrementCounter);
							}
	    	    	 		
			    	    	 	
			    	    	 	if(exceedFlag){
			    	    	 		i = exceedIndex;
			    	    	 		index = exceedIndex;
			    	    	 		//System.out.println("Current Index in exceed::"+i);
			    	    	 	}
			    	    	 	else{
			    	    	 		i = indextemp-(indextemp-incrementCounter);
			    	    	 		index = indextemp-(indextemp-incrementCounter);
			    	    	 	}
			    	    	 	incrementCounter=0;
			    	    	 	//System.out.println("\n\n\nFinal I value::::"+i+"indextemp:::"+indextemp+"index:::"+index);
								break;
							/*}*/
			    	     
		    			  }
		    		  }
		    	  }
		    	  
		      }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_QUICKSUMMARY_REPORT)){
		    	    /* if((getIsMultipleCBBSelected()!=null && getIsMultipleCBBSelected())
		    			  && (getIsWorkflowActivitySelected()!=null && getIsWorkflowActivitySelected())){
						 table = createTable(13,0);								 
						 columnWidths = new float[] {7f,7f,7f,7f,8f,8f,8f,8f,8f,8f,8f,8f,8f};								 
					 }else if((getIsMultipleCBBSelected()!=null && getIsMultipleCBBSelected())
			    			  && (getIsWorkflowActivitySelected()!=null && !getIsWorkflowActivitySelected())){
						 table = createTable(11,0);								 
						 columnWidths = new float[] {9f,9f,9f,9f,8f,8f,8f,8f,8f,8f,8f};								 
					 }else if((getIsMultipleCBBSelected()!=null && !getIsMultipleCBBSelected())
			    			  && (getIsWorkflowActivitySelected()!=null && getIsWorkflowActivitySelected())){
						 table = createTable(12,0);								 
						 columnWidths = new float[] {9f,9f,9f,9f,8f,8f,8f,8f,8f,8f,8f,8f};						 
					 }else{
						 table = createTable(10,0);								 
						 columnWidths = new float[] {9f,9f,9f,9f,9f,9f,9f,9f,9f,9f};
					 }		*/		
		    	  if((getIsMultipleCBBSelected()!=null && getIsMultipleCBBSelected())){
		    		  //System.out.println("Insisde multiple");
						 table = createTable(13,0);								 
						 columnWidths = new float[] {7f,7f,7f,7f,8f,8f,8f,8f,8f,8f,8f,8f,8f};								 
					 }
		    	  else{
		    		  //System.out.println("Insisde multiple not selected");
						 table = createTable(12,0);								 
						 columnWidths = new float[] {9f,9f,9f,9f,9f,9f,9f,9f,9f,9f,9f,9f};
					 }	
		    	  
					 table.setWidths(columnWidths);	
					 table.setWidthPercentage(100);
					 table.setHorizontalAlignment(0);
					 table.getDefaultCell().setBorder(0);
					 if(getIsMultipleCBBSelected()!=null && getIsMultipleCBBSelected()==true){					 
						 cell = createTableCell(getText("garuda.queryBuilder.label.cbbid"), 0, bodyFontBold, 0) ;
						 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
						 cell.setBorder(Rectangle.BOX);
						 table.addCell(cell);
					 }
					 
					 cell = createTableCell(getText("garuda.cbuentry.label.registrycbuid"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.cbuentry.label.localcbuid"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.cdrcbuview.label.id_on_bag"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.shippedcbu.report.label.isbt"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 					 
						 cell = createTableCell(getText("garuda.cbuentry.label.requestType"), 0, bodyFontBold, 0) ;
						 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
						 cell.setBorder(Rectangle.BOX);
						 table.addCell(cell);
					
					 
					
					 cell = createTableCell(getText("garuda.idsReport.reqdate"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.idsReport.orshipdate"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.shippedcbu.report.label.orinfuseddt"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.recipient&tcInfo.label.recipientid"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.shippedcbu.report.label.patientdiag"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.shippedcbu.report.label.tccode"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.shippedcbu.report.label.tcname"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 if(cdrObjects!=null){
							for(Object o : cdrObjects){
									 Object[] oArr = (Object[])o;
									 if(getIsMultipleCBBSelected()!=null && getIsMultipleCBBSelected()==true){					 
										 cell = createTableCell(oArr[1]!=null?oArr[1].toString():"", 0, bodyFont, 0) ;
										 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
										 cell.setBorder(Rectangle.BOX);
										 table.addCell(cell);
									 }
									 
									 cell = createTableCell(oArr[2]!=null?oArr[2].toString():"", 0, bodyFont, 0) ;
									 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
									 cell.setBorder(Rectangle.BOX);
									 if(getSession(getRequest()).getAttribute("orDuplCords")!=null && oArr[0]!=null && ((Map<Long,Long>)getSession(getRequest()).getAttribute("orDuplCords")).get(((BigDecimal)oArr[0]).longValue())!=null)
										 cell.setBackgroundColor(WebColors.getRGBColor("#FF0000"));
									 table.addCell(cell);
									 
									 cell = createTableCell(oArr[3]!=null?oArr[3].toString():"", 0, bodyFont, 0) ;
									 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
									 cell.setBorder(Rectangle.BOX);
									 table.addCell(cell);
									 
									 cell = createTableCell(oArr[4]!=null?oArr[4].toString():"", 0, bodyFont, 0) ;
									 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
									 cell.setBorder(Rectangle.BOX);
									 table.addCell(cell);
									 
									 cell = createTableCell(oArr[5]!=null?oArr[5].toString():"", 0, bodyFont, 0) ;
									 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
									 cell.setBorder(Rectangle.BOX);
									 table.addCell(cell);
									 
									 
										 cell = createTableCell(oArr[9]!=null?oArr[9].toString():"", 0, bodyFont, 0) ;
										 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
										 cell.setBorder(Rectangle.BOX);
										 table.addCell(cell);
									
									 
									
									 cell = createTableCell(oArr[9]!=null && oArr[10]!=null && (oArr[9].toString().equals(OR) || oArr[9].toString().equals(CT_SHIP_SAMPLE_AT_LAB) || oArr[9].toString().equals(CT_SHIP_SAMPLE) || oArr[9].toString().equals(HE))?oArr[10].toString():"", 0, bodyFont, 0) ;
									 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
									 cell.setBorder(Rectangle.BOX);
									 table.addCell(cell);
									
									 
									 cell = createTableCell(oArr[9]!=null && oArr[11]!=null && (oArr[9].toString().equals(OR))?oArr[11].toString():"", 0, bodyFont, 0) ;
									 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
									 cell.setBorder(Rectangle.BOX);
									 table.addCell(cell);
									 
									 cell = createTableCell(oArr[9]!=null && oArr[12]!=null && (oArr[9].toString().equals(OR))?oArr[12].toString():"", 0, bodyFont, 0) ;
									 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
									 cell.setBorder(Rectangle.BOX);
									 table.addCell(cell);
									 
									 cell = createTableCell(oArr[13]!=null?oArr[13].toString():"", 0, bodyFont, 0) ;
									 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
									 cell.setBorder(Rectangle.BOX);
									 table.addCell(cell);
									 
									 cell = createTableCell(oArr[14]!=null?oArr[14].toString():"", 0, bodyFont, 0) ;
									 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
									 cell.setBorder(Rectangle.BOX);
									 table.addCell(cell);
									 
									 cell = createTableCell(oArr[15]!=null?oArr[15].toString():"", 0, bodyFont, 0) ;
									 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
									 cell.setBorder(Rectangle.BOX);
									 table.addCell(cell);
						
									 cell = createTableCell(oArr[16]!=null?oArr[16].toString():"", 0, bodyFont, 0) ;
									 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
									 cell.setBorder(Rectangle.BOX);
									 table.addCell(cell);			 
							}
				    	 }
			    	     document.add(table);
					 
		      }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_IDSUMMARY_REPORT)){
		    	  /*if((getIsMultipleCBBSelected()!=null && getIsMultipleCBBSelected())
		    			  && (getIsWorkflowActivitySelected()!=null && getIsWorkflowActivitySelected())){
						 table = createTable(12,0);								 
						 columnWidths = new float[] {9f,9f,9f,9f,8f,8f,8f,8f,8f,8f,8f,8f};								 
					 }else if((getIsMultipleCBBSelected()!=null && getIsMultipleCBBSelected())
			    			  && (getIsWorkflowActivitySelected()!=null && !getIsWorkflowActivitySelected())){
						 table = createTable(10,0);								 
						 columnWidths = new float[] {10f,10f,10f,10f,10f,10f,10f,10f,10f,10f};								 
					 }else if((getIsMultipleCBBSelected()!=null && !getIsMultipleCBBSelected())
			    			  && (getIsWorkflowActivitySelected()!=null && getIsWorkflowActivitySelected())){
						 table = createTable(11,0);								 
						 columnWidths = new float[] {9f,9f,9f,9f,9f,9f,9f,9f,9f,9f,10f};						 
					 }else{
						 table = createTable(9,0);								 
						 columnWidths = new float[] {11f,11f,11f,11f,11f,11f,11f,11f,12f};
					 }*/
		    	    if((getIsMultipleCBBSelected()!=null && getIsMultipleCBBSelected())){
		    	    	table = createTable(8,0);								 
					    columnWidths = new float[] {12f,12f,12f,12f,13f,13f,13f,13f};								 
					 }else{	
		    	        table = createTable(7,0);								 
					    columnWidths = new float[] {14f,14f,14f,14f,14f,15f,15f};
					 }
					 table.setWidths(columnWidths);	
					 table.setWidthPercentage(100);
					 table.setHorizontalAlignment(0);
					 table.getDefaultCell().setBorder(0);
					 
					 if(getIsMultipleCBBSelected()!=null && getIsMultipleCBBSelected()==true){					 
						 cell = createTableCell(getText("garuda.queryBuilder.label.cbbid"), 0, bodyFontBold, 0) ;
						 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
						 cell.setBorder(Rectangle.BOX);
						 table.addCell(cell);
					 }
					 
					 cell = createTableCell(getText("garuda.cbuentry.label.registrycbuid"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.cbuentry.label.localcbuid"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.cdrcbuview.label.id_on_bag"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.shippedcbu.report.label.isbt"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.cdrcbuview.label.historic_cbu_lcl_id"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.cbuentry.label.registrymaternalid"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					 cell = createTableCell(getText("garuda.cbuentry.label.localmaternalid"), 0, bodyFontBold, 0) ;
					 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					 cell.setBorder(Rectangle.BOX);
					 table.addCell(cell);
					 
					/* if(getIsWorkflowActivitySelected()!=null && getIsWorkflowActivitySelected()==true){					 
						 cell = createTableCell(getText("garuda.shippedcbu.report.label.wactivity"), 0, bodyFontBold, 0) ;
						 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
						 cell.setBorder(Rectangle.BOX);
						 table.addCell(cell);
					 }
					 
					
					cell = createTableCell(getText("garuda.idsReport.ctshipdate"), 0, bodyFontBold, 0);
					cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					cell.setBorder(Rectangle.BOX);
					table.addCell(cell);
					 
					cell = createTableCell(getText("garuda.idsReport.orshipdate"), 0, bodyFontBold, 0);
					cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					cell.setBorder(Rectangle.BOX);
					table.addCell(cell);
					
					if(getIsWorkflowActivitySelected()!=null && getIsWorkflowActivitySelected()==true){
						 cell = createTableCell(getText("garuda.idsReport.reqdate"), 0, bodyFontBold, 0) ;
						 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
						 cell.setBorder(Rectangle.BOX);
						 table.addCell(cell);
					}*/
					 
					 
					 
					 
					 
			    	 if(cdrObjects!=null){
						for(Object o : cdrObjects){
								 Object[] oArr = (Object[])o;
								 if(getIsMultipleCBBSelected()!=null && getIsMultipleCBBSelected()==true){					 
									 cell = createTableCell(oArr[1]!=null?oArr[1].toString():"", 0, bodyFont, 0) ;
									 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
									 cell.setBorder(Rectangle.BOX);
									 table.addCell(cell);
								 }
								 
								 cell = createTableCell(oArr[2]!=null?oArr[2].toString():"", 0, bodyFont, 0) ;
								 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
								 cell.setBorder(Rectangle.BOX);
								 /*if(getSession(getRequest()).getAttribute("duplCords")!=null && oArr[0]!=null && ((Map<Long,Long>)getSession(getRequest()).getAttribute("duplCords")).get(((BigDecimal)oArr[0]).longValue())!=null)
									 cell.setBackgroundColor(WebColors.getRGBColor("#FF0000"));*/
								 table.addCell(cell);
								 
								 cell = createTableCell(oArr[3]!=null?oArr[3].toString():"", 0, bodyFont, 0) ;
								 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
								 cell.setBorder(Rectangle.BOX);
								 table.addCell(cell);
								 
								 cell = createTableCell(oArr[4]!=null?oArr[4].toString():"", 0, bodyFont, 0) ;
								 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
								 cell.setBorder(Rectangle.BOX);
								 table.addCell(cell);
								 
								 cell = createTableCell(oArr[5]!=null?oArr[5].toString():"", 0, bodyFont, 0) ;
								 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
								 cell.setBorder(Rectangle.BOX);
								 table.addCell(cell);
								 
								 cell = createTableCell(oArr[6]!=null?oArr[6].toString():"", 0, bodyFont, 0) ;
								 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
								 cell.setBorder(Rectangle.BOX);
								 table.addCell(cell);
								 
								 cell = createTableCell(oArr[7]!=null?oArr[7].toString():"", 0, bodyFont, 0) ;
								 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
								 cell.setBorder(Rectangle.BOX);
								 table.addCell(cell);
								 
								 cell = createTableCell(oArr[8]!=null?oArr[8].toString():"", 0, bodyFont, 0) ;
								 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
								 cell.setBorder(Rectangle.BOX);
								 table.addCell(cell);
								 
								 /*if(getIsWorkflowActivitySelected()!=null && getIsWorkflowActivitySelected()==true){					 
									 cell = createTableCell(oArr[9]!=null?oArr[9].toString():"", 0, bodyFont, 0) ;
									 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
									 cell.setBorder(Rectangle.BOX);
									 table.addCell(cell);
								 }
								 
								 
								 
								 cell = createTableCell(oArr[9]!=null && oArr[10]!=null && (oArr[9].toString().equals(CT_SHIP_SAMPLE_AT_LAB) || oArr[9].toString().equals(CT_SHIP_SAMPLE))?oArr[10].toString():"", 0, bodyFont, 0) ;
								 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
								 cell.setBorder(Rectangle.BOX);
								 table.addCell(cell);
								 
								 cell = createTableCell(oArr[9]!=null && oArr[11]!=null && (oArr[9].toString().equals(OR))?oArr[11].toString():"", 0, bodyFont, 0) ;
								 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
								 cell.setBorder(Rectangle.BOX);
								 table.addCell(cell);
								 
								 if(getIsWorkflowActivitySelected()!=null && getIsWorkflowActivitySelected()==true){
									 cell = createTableCell(oArr[12]!=null?oArr[12].toString():"", 0, bodyFont, 0) ;
									 cell.setHorizontalAlignment(Element.ALIGN_LEFT);
									 cell.setBorder(Rectangle.BOX);
									 table.addCell(cell);
								 }*/
								 
								 
						}
			    	 }
		    	     document.add(table);
		      }
			 return document;
			 
		}
		 
		 private PdfPTable createTable(Integer numberOfCells,int border)
		    throws BadElementException {
				   PdfPTable table = new PdfPTable(numberOfCells);
				   table.setWidthPercentage(100);
				   table.setHorizontalAlignment(Element.ALIGN_CENTER);
				   return table;
		    }
			 
		 private PdfPTable createTableHeader(PdfPTable table,List<String> headers,int alignment,Font font,int border){
			for(String s : headers){
				PdfPCell cell = createTableCell(s, alignment,font,border);
				cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				cell.setBorder(Rectangle.BOX);
				table.addCell(cell);
			}
			//table.setHeaderRows(1);
			return table;
		} 
		 
		private PdfPCell createTableCell(String cellText,int alignment,Font font,int border){
			PdfPCell c1 = null;
			if(!StringUtils.isEmpty(cellText)){
			  c1 = new PdfPCell(new Phrase(cellText,font));
			}else{
			  c1 = new PdfPCell(new Phrase("",font));	
			} 
			c1.setHorizontalAlignment(alignment);
			c1.setBorder(border);
			return c1;
		}
		
		private PdfPCell createTableCell(Image cellImage,int alignment,Font font,int border){
			PdfPCell c1 = null;
			if(cellImage!=null){
			   c1 = new PdfPCell(cellImage);
			}else{
				c1 = new PdfPCell();	
			}
			c1.setHorizontalAlignment(alignment);
			c1.setBorder(border);
			return c1;
		}	
		private PdfPCell createTableCell(Phrase phrase,int alignment,Font font,int border){
			PdfPCell c1 = null;
			if(phrase!=null){
			   c1 = new PdfPCell(phrase);
			}else{
				c1 = new PdfPCell();	
			}
			c1.setHorizontalAlignment(alignment);
			c1.setBorder(border);
			return c1;
		}
		
		private String PrepareFilter(String filter, PaginateSearch paginateSearch){
			
			if(paginateSearch !=null){
				
				if(paginateSearch.getiShowRows()!=null && paginateSearch.getiPageNo()!=null){
					
					filter = "SELECT * FROM (SELECT row_.*,rownum rownum_  FROM ("+filter+")row_  )WHERE rownum_ <= "+((paginateSearch.getiPageNo())+(paginateSearch.getiShowRows()))+" AND rownum_    > "+paginateSearch.getiPageNo();
					
				}else if(paginateSearch.getiShowRows() != null){
					
					filter = "SELECT *FROM ("+filter+")WHERE rownum <="+paginateSearch.getiShowRows();
				}
				
				
				
			}
			
			return filter;
		}
		
		
		private void setHlaAntigenDataForPdf(){		

			try{			
				
				Map<Long, Map<Long,List<HLAPojo>>> cbuHlas;
				HLAPojo hlaPojo = null;
				BestHLAPojo bhlaPojo = null;
				String sqlHlaHead= "";
				sqlHlaHead += "select h.PK_HLA,h.FK_HLA_CODE_ID,h.FK_HLA_METHOD_ID,h.HLA_RECEIVED_DATE,h.HLA_TYPING_DATE,h.ENTITY_ID,h.ENTITY_TYPE,h.HLA_TYPE1,h.HLA_TYPE2,h.FK_SOURCE,h.HLA_USER,h.CREATED_ON,h.CB_HLA_ORDER_SEQ,h.LAST_MODIFIED_BY " +
						" from "+'"'+"VDA"+'"'+"."+'"'+"ETVDA_QUERY_BUILDER_HLA_DATA"+'"'+" h JOIN (" +
						" SELECT DISTINCT(C.PK_CORD) as pk_cord " ;
				sqlHlaHead += getSession(getRequest()).getAttribute("queryBuilderSql");
				sqlHlaHead +=	" ) C on h.entity_id=c.pk_cord where h.FK_SOURCE="+getCodeListPkByTypeAndSubtype(TEST_SOURCES, CBU_CODESUBTYPE);
				
				if(!sqlHlaHead.equals("")){
					cbuHlas = vdaController.getQueryBuilderHlaData(sqlHlaHead);
					if(cbuHlas != null){				
						setHlas(cbuHlas);
					}
				}							
				
				
				String bestHlaHead = "";
				bestHlaHead += "select h.PK_BEST_HLA,h.FK_HLA_CODE_ID,h.FK_HLA_METHOD_ID,h.HLA_RECEIVED_DATE,h.HLA_TYPING_DATE,h.ENTITY_ID,h.ENTITY_TYPE,h.HLA_TYPE1,h.HLA_TYPE2,h.FK_SOURCE,h.HLA_USER,h.CREATED_ON,h.CB_BEST_HLA_ORDER_SEQ " +
				" from "+'"'+"VDA"+'"'+"."+'"'+"ETVDA_QUERY_BUILDER_BHLA_DATA"+'"'+" h JOIN (" +
				" SELECT DISTINCT(C.PK_CORD) as pk_cord " ;
				bestHlaHead += getSession(getRequest()).getAttribute("queryBuilderSql");
				bestHlaHead +=	" ) C on h.entity_id=c.pk_cord and h.ENTITY_TYPE="+getCodeListPkByTypeAndSubtype(ENTITY_CBU_CODE_TYPE, ENTITY_CBU_CODE_SUB_TYPE);
				
				if(!bestHlaHead.equals("")){
					bestHlasMap = vdaController.getQueryBuilderBestHlaData(bestHlaHead);  
					if(bestHlasMap!=null){
						setBestHlasMap(bestHlasMap);
					}
				}			
				
		   }catch (Exception e) {
			e.printStackTrace();
		  }			
		
			
		}
		
		
		private List<Object>  hlaAntigenDataUserInterface( final String sqlFilter){
			
			Map<Long, Map<Long,List<HLAPojo>>> cbuHlas;			
			HLAPojo hlaPojo = null;
			BestHLAPojo bhlaPojo = null;
			
			
			
			
			try{
				
				Runnable th1 = new Runnable() {
					
					@Override
					public void run() {
						
							try {	
								
							  cdrObjs =	vdaController.getQueryBuilderViewData(sqlFilter, paginateSearch);
							
							if(cdrObjs!=null && cdrObjs.size()>0){
						    	setCordInfoList(cdrObjs);
						    	if(getCbuCount()==null || getCbuCount()==0)
						    	{
						    		List<Object> list;
									
										list = vdaController.getQueryBuilderViewData("SELECT COUNT(*) FROM ( "+sqlFilter+" )", null);
									
						    		//cbuCount = list!=null && list.size()>0?list.size():0;
						    		cbuCount = list !=null && list.get(0) != null ?((BigDecimal)list.get(0)).intValue():0;
						    		
						    	}                   
						    }
						
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				   }
				};
				
				Thread cdrWorker=new Thread(th1);
					cdrWorker.start();
					
				Runnable th2 = new Runnable() {
					
					@Override
					public void run() {
						String bestHlaHead = "";
								bestHlaHead += "select h.PK_BEST_HLA,h.FK_HLA_CODE_ID,h.FK_HLA_METHOD_ID,h.HLA_RECEIVED_DATE,h.HLA_TYPING_DATE,h.ENTITY_ID,h.ENTITY_TYPE,h.HLA_TYPE1,h.HLA_TYPE2,h.FK_SOURCE,h.HLA_USER,h.CREATED_ON,h.CB_BEST_HLA_ORDER_SEQ " +
										" from "+'"'+"VDA"+'"'+"."+'"'+"ETVDA_QUERY_BUILDER_BHLA_DATA"+'"'+" h JOIN (" ;				
								bestHlaHead += PrepareFilter(sqlFilter, paginateSearch);
								bestHlaHead +=	") C on h.entity_id=c.pk_cord and h.ENTITY_TYPE="+getCodeListPkByTypeAndSubtype(ENTITY_CBU_CODE_TYPE, ENTITY_CBU_CODE_SUB_TYPE);
																
							try {
								bestHlasMap = vdaController.getQueryBuilderBestHlaData(bestHlaHead);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							
							
							
					}
				};
				
				Thread bestHLAWorker=new Thread(th2);
					   bestHLAWorker.start();
				
				Runnable th3 = new Runnable() {
					
					@Override
					public void run() {
						
					/*String	sqlHlaHead = "select h.PK_HLA,h.FK_HLA_CODE_ID,h.FK_HLA_METHOD_ID,h.HLA_RECEIVED_DATE,h.HLA_TYPING_DATE,h.ENTITY_ID,h.ENTITY_TYPE,h.HLA_TYPE1,h.HLA_TYPE2,h.FK_SOURCE,h.HLA_USER,h.CREATED_ON,h.CB_HLA_ORDER_SEQ " +
						                 " from "+'"'+"VDA"+'"'+"."+'"'+"ETVDA_QUERY_BUILDER_HLA_DATA"+'"'+" h JOIN ( ";	
					*/
					
						String	sqlHlaHead = "select h.PK_HLA,h.FK_HLA_CODE_ID,h.FK_HLA_METHOD_ID,h.HLA_RECEIVED_DATE,h.HLA_TYPING_DATE,h.ENTITY_ID,h.ENTITY_TYPE,h.HLA_TYPE1,h.HLA_TYPE2,h.FK_SOURCE,h.HLA_USER,h.CREATED_ON,h.CB_HLA_ORDER_SEQ,h.LAST_MODIFIED_BY" +
		                 " from "+'"'+"VDA"+'"'+"."+'"'+"ETVDA_QUERY_BUILDER_HLA_DATA"+'"'+" h JOIN ( ";	
						
				           sqlHlaHead += PrepareFilter(sqlFilter, paginateSearch);
				           sqlHlaHead +=	" ) C on h.entity_id=c.pk_cord where h.FK_SOURCE="+getCodeListPkByTypeAndSubtype(TEST_SOURCES, CBU_CODESUBTYPE);
								
						try {
							hlas = vdaController.getQueryBuilderHlaData(sqlHlaHead);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
				
						
					}
				};
													
				Thread hlaWorker=new Thread(th3);
						hlaWorker.start();
						
						cdrWorker.join();
						bestHLAWorker.join();
						hlaWorker.join();
				
				
		   }catch (Exception e) {
			e.printStackTrace();
		  }	
		   
		   return cdrObjs;
		}
		
		
		
		private void hlaAntigenDataExcelCSV(String sqlFilter) throws InterruptedException{
			String desc="";
			//List<CordFilterPojo> cord;
				
			
				
				  cdlst = new HashMap<Long, String>();
				  
				 for(CodeList c : GarudaConfig.getCodeListMap().get(HLA_LOCUS)){
					 //System.out.println(c.getDescription());
					 desc = c.getDescription();
					 if("HLA-A".equals(desc))
						 cdlst.put(c.getPkCodeId(), "A");
					 else if("HLA-B".equals(desc))
						 cdlst.put(c.getPkCodeId(), "B");
					 else if("HLA-C".equals(desc))
						 cdlst.put(c.getPkCodeId(), "C");
					 else if("HLA-DRB1".equals(desc))
						 cdlst.put(c.getPkCodeId(), "DRB1");
					 else if("HLA-DRB3".equals(desc))
						 cdlst.put(c.getPkCodeId(), "DRB3");
					 else if("HLA-DRB4".equals(desc))
						 cdlst.put(c.getPkCodeId(), "DRB4");
					 else if("HLA-DRB5".equals(desc))
						 cdlst.put(c.getPkCodeId(), "DRB5");
					 else if("HLA-DQB1".equals(desc))
						 cdlst.put(c.getPkCodeId(), "DQB1");
					 else if("HLA-DPB1".equals(desc))
						 cdlst.put(c.getPkCodeId(), "DPB1");
				 }
					
	 
				 
				 
				 Runnable  hlaThread1 = new Runnable() {
						
						@Override
						public void run() {
							
							String bestHlaHead = "";
							bestHlaHead += "select h.PK_BEST_HLA,h.FK_HLA_CODE_ID,h.FK_HLA_METHOD_ID,h.HLA_RECEIVED_DATE,h.HLA_TYPING_DATE,h.ENTITY_ID,h.ENTITY_TYPE,h.HLA_TYPE1,h.HLA_TYPE2,h.FK_SOURCE,h.HLA_USER,h.CREATED_ON,h.CB_BEST_HLA_ORDER_SEQ " +
							" from "+'"'+"VDA"+'"'+"."+'"'+"ETVDA_QUERY_BUILDER_BHLA_DATA"+'"'+" h JOIN (" +
							" SELECT DISTINCT(C.PK_CORD) as pk_cord " ;
							bestHlaHead += getSession(getRequest()).getAttribute("queryBuilderSql");
							bestHlaHead +=	" ) C on h.entity_id=c.pk_cord and h.ENTITY_TYPE="+getCodeListPkByTypeAndSubtype(ENTITY_CBU_CODE_TYPE, ENTITY_CBU_CODE_SUB_TYPE);
							
							if(!bestHlaHead.equals("")){
								
								try {
									cbuBestHlas = vdaController.getQueryBuilderBestHlaFilterData(bestHlaHead, cdlst);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}  
								
							}			
						}
					};			
					
					Thread hlaWorker1=new Thread(hlaThread1);
					hlaWorker1.start();
					
				 Runnable  hlaThread2= 	new Runnable() {
					
					@Override
					public void run() {
						
						String cordSql = null;
						 
					 	cordSql = "SELECT Distinct C.PK_CORD,C.SITE_ID,C.REGISTRY_ID,C.CORD_LOCAL_CBU_ID,C.CORD_ID_NUMBER_ON_CBU_BAG,C.CORD_ISBI_DIN_CODE,C.CORD_HISTORIC_CBU_ID,C.REGISTRY_MATERNAL_ID,C.MATERNAL_LOCAL_ID" +
					 			   " , C.PATIENT_ID ,"+" C.SPEC_COLLECTION_DATE ,C.CORD_NMDP_STATUS as CORD_NMDP_STATUS " ;
					 	cordSql += getSession(getRequest()).getAttribute("queryBuilderHlaSql");
					 	cordSql += " ORDER BY C.SITE_ID,C.REGISTRY_ID";
					 
					 	String sqlHlaHead= "";
						sqlHlaHead += "select h.PK_HLA,h.FK_HLA_CODE_ID,h.FK_HLA_METHOD_ID,h.HLA_RECEIVED_DATE,h.HLA_TYPING_DATE,h.ENTITY_ID,h.ENTITY_TYPE,h.HLA_TYPE1,h.HLA_TYPE2,h.FK_SOURCE,h.HLA_USER,h.CREATED_ON,h.CB_HLA_ORDER_SEQ " +
								" from "+'"'+"VDA"+'"'+"."+'"'+"ETVDA_QUERY_BUILDER_HLA_DATA"+'"'+" h JOIN (" +
								" SELECT DISTINCT(C.PK_CORD) as pk_cord " ;
						sqlHlaHead += getSession(getRequest()).getAttribute("queryBuilderSql");
						sqlHlaHead +=	" ) C on h.entity_id=c.pk_cord where h.FK_SOURCE="+getCodeListPkByTypeAndSubtype(TEST_SOURCES, CBU_CODESUBTYPE);
						
					
					 
					 	
					 		try {
								cdrObj = vdaController.getQueryBuilderCordHlaData(cordSql);
								
								cbuHlas = vdaController.getQueryBuilderHlaFilterData(sqlHlaHead, cdlst);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}			
						 
					}
				};
	    
				
				Thread hlaWorker2=new Thread(hlaThread2);
				hlaWorker2.start();
				
				hlaWorker1.join();
				hlaWorker2.join();
			
			System.out.println(" HLA Executor finished");
		}
		
	/***
	 * This method use to Update Final CBU Review Selected Document to final attachment
	 * @author Anurag Upadhyay
	 *@param Use IMPLICITLY  finalReviewDocs, cordID, CordFinalReview primary key value
	 * @ return  IMPLICITLY set the Boolean value for Field 'finReviewAssoDocsFlag'
	 */
	public String finalReviewAssociatedDocs(){
		if(finRevUploadInfo!=null){
		   List<FinalRevUploadInfoPojo> finRevSelectDocInfo = new ArrayList<FinalRevUploadInfoPojo>() ;
		   Long entityType=getCodeListPkByTypeAndSubtype(ENTITY_CBU_CODE_TYPE, ENTITY_CBU_CODE_SUB_TYPE);
			   for(FinalRevUploadInfoPojo finRevDocs : finRevUploadInfo){
				   if(finRevDocs !=null && finRevDocs.getFkCbuUploadInfo()!=null){
				     finRevDocs.setEntityId(cdrCbuPojo.getCordID());
				     finRevDocs.setEntityType(entityType);
				     finRevDocs.setFkFinalCbuReview(Long.parseLong(request.getParameter("pkCordFinalReview")));
				     finRevSelectDocInfo.add(finRevDocs);
				   }
			   }
		   try {
			        cbuController.deleteFinalReviewDocs(entityType,cdrCbuPojo.getCordID());
			        finRevUploadInfo=cbuController.saveFinalReviewDocs(finRevSelectDocInfo,true);
			        setFinReviewAssoDocsFlag(true);
		} catch (Exception e) {
			// TODO Auto-generated catch block			
			e.printStackTrace();
		}
	 }
		return SUCCESS;
  }
	
	public void calculateHlaPercentage() throws Exception{
		Integer totalFieldcount = 6;
		Integer mandatoryFiledCount = 0;
		Integer hlaPercentage = 0;		
		Boolean hla_a_exist = true;
		Boolean hla_b_exist = true;
		Boolean hla_drb1_exist = true;
		Map<Long,List<HLAPojo>> mapHla = setHlaMapBYEntityId(cdrCbuPojo.getCordID(),getCodeListPkByTypeAndSubtype(VelosGarudaConstants.CORD_ENTITY_TYPE, VelosGarudaConstants.CORD_ENTITY_SUB_TYPE));
		List<HLAPojo> hlas = mapHla.get(getCodeListPkByTypeAndSubtype(VelosGarudaConstants.TEST_SOURCES, VelosGarudaConstants.CBU_CODESUBTYPE));
		if(hlas!=null){
			for(HLAPojo h : hlas){
				if((h.getFkHlaCodeId()==getCodeListPkByTypeAndSubtype(VelosGarudaConstants.HLA_LOCUS, VelosGarudaConstants.HLA_A_ST)
						|| h.getFkHlaCodeId().equals(getCodeListPkByTypeAndSubtype(VelosGarudaConstants.HLA_LOCUS, VelosGarudaConstants.HLA_A_ST))) && hla_a_exist){
					if(h.getFkHlaMethodId()!=null && h.getFkHlaMethodId()!=-1l)
						mandatoryFiledCount++;
					if(h.getFkHlaAntigen1()!=null)
						mandatoryFiledCount++;
					hla_a_exist = false;
				}
				if((h.getFkHlaCodeId()==getCodeListPkByTypeAndSubtype(VelosGarudaConstants.HLA_LOCUS, VelosGarudaConstants.HLA_B_ST)
						|| h.getFkHlaCodeId().equals(getCodeListPkByTypeAndSubtype(VelosGarudaConstants.HLA_LOCUS, VelosGarudaConstants.HLA_B_ST))) && hla_b_exist){
					if(h.getFkHlaMethodId()!=null && h.getFkHlaMethodId()!=-1l)
						mandatoryFiledCount++;
					if(h.getFkHlaAntigen1()!=null)
						mandatoryFiledCount++;
					hla_b_exist = false;
				}
				if((h.getFkHlaCodeId()==getCodeListPkByTypeAndSubtype(VelosGarudaConstants.HLA_LOCUS, VelosGarudaConstants.HLA_DRB1_ST)
						|| h.getFkHlaCodeId().equals(getCodeListPkByTypeAndSubtype(VelosGarudaConstants.HLA_LOCUS, VelosGarudaConstants.HLA_DRB1_ST))) && hla_drb1_exist){
					if(h.getFkHlaMethodId()!=null && h.getFkHlaMethodId()!=-1l)
						mandatoryFiledCount++;
					if(h.getFkHlaAntigen1()!=null)
						mandatoryFiledCount++;
					hla_drb1_exist = false;
				}
			}
		}
		setHlaTotaFiledCount(totalFieldcount);
		setHlaMadatoryField(mandatoryFiledCount);
		hlaPercentage = (mandatoryFiledCount*100)/totalFieldcount;
		setHlaPercentage(hlaPercentage);
	}
	
	private String filePath(String fName, String fType, int userId){
		
		String  fpath; 
		Configuration.readAppendixParam(Configuration.ERES_HOME + "eresearch.xml");
		com.aithent.file.uploadDownload.Configuration.readSettings("eres");
		com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "study");	
		String fileDnPath = com.aithent.file.uploadDownload.Configuration.DOWNLOADSERVLET ;
		Configuration.readReportParam(Configuration.ERES_HOME + "eresearch.xml");		
		
		fpath = Configuration.REPDWNLDPATH;
		fpath = fpath.replace("\\", "/");
		
		 fpath += "/";
		 fName  = fName+"["+ System.nanoTime()+ "]"+ userId;	
		 fName += fType;
		  
		 fpath += fName;		
		  
		return  fpath;
	}
	
}