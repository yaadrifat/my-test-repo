package com.velos.ordercomponent.action;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.velos.ordercomponent.controller.CodelstController;

public class JsonAction extends VelosBaseAction {
	public static final Log log = LogFactory.getLog(JsonAction.class);
	private Boolean dbValue;
	private String entityName;
	private String entityField;
	private String entityValue;	
	private String entityField1;
	private String entityValue1;
	private String entityField2;
	private String entityValue2;
	
	private CodelstController codelstController;	
	
	
	public String getEntityField2() {
		return entityField2;
	}

	public void setEntityField2(String entityField2) {
		this.entityField2 = entityField2;
	}

	public String getEntityValue2() {
		return entityValue2;
	}

	public void setEntityValue2(String entityValue2) {
		this.entityValue2 = entityValue2;
	}

	public String getEntityField1() {
		return entityField1;
	}

	public void setEntityField1(String entityField1) {
		this.entityField1 = entityField1;
	}

	public String getEntityValue1() {
		return entityValue1;
	}

	public void setEntityValue1(String entityValue1) {
		this.entityValue1 = entityValue1;
	}

	public Boolean getDbValue() {
		return dbValue;
	}

	public void setDbValue(Boolean dbValue) {
		this.dbValue = dbValue;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getEntityField() {
		return entityField;
	}

	public void setEntityField(String entityField) {
		this.entityField = entityField;
	}

	public String getEntityValue() {
		return entityValue;
	}

	public void setEntityValue(String entityValue) {
		this.entityValue = entityValue;
	}

	public JsonAction(){
		codelstController = new CodelstController();
		
	}
	
	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String getValidateDbData() throws Exception{
		//log.debug("url3");		
		if(!getEntityName().equals(null) || !getEntityName().equals("null")){
			 //log.debug("url4"+getEntityField1()+getEntityValue1());
		   if(StringUtils.isEmpty(getEntityField1()) || getEntityField1().equals("null")){
			   //log.debug("url1");	
			   if(getEntityValue()!=null && getEntityValue().indexOf("-")!=-1)
			   {
				 /*  while(getEntityValue().indexOf("-")>0){
					   String val = getEntityValue();
					   int index =  val.indexOf("-");
					   val = val.substring(0,index)+val.substring(index+1,val.length());
					   setEntityValue(val);
				   }*/
				   setEntityValue(getEntityValue().replaceAll("-", ""));
			   }
			   //log.debug("value"+getEntityValue());	
			   setDbValue(codelstController.getValidateDbData(getEntityName(), getEntityField(), getEntityValue(), null , null));			   
		   }else {
			   //log.debug("url2");	
			   if(getEntityValue()!=null && getEntityValue().indexOf("-")!=-1)
			   {
				  /* while(getEntityValue().indexOf("-")>0){
					   String val = getEntityValue();
					   int index =  val.indexOf("-");
					   val = val.substring(0,index)+val.substring(index+1,val.length());
					   setEntityValue(val);
				   }*/
				   setEntityValue(getEntityValue().replaceAll("-", ""));
			   }
			   //log.debug("value"+getEntityValue());
			   if(getEntityValue1()!=null && getEntityValue1().indexOf("-")!=-1)
			   {
				  /* while(getEntityValue1().indexOf("-")>0){
					   String val = getEntityValue1();
					   int index =  val.indexOf("-");
					   val = val.substring(0,index)+val.substring(index+1,val.length());
					   setEntityValue1(val);
				   }*/
				   setEntityValue1(getEntityValue1().replaceAll("-", ""));
			   }
			   //log.debug("value"+getEntityValue1());
			   if(!StringUtils.isEmpty(getEntityField2()) && !getEntityField2().equals("null")){
				   if(getEntityValue2()!=null && getEntityValue2().indexOf("-")!=-1)
				   {
					  /* while(getEntityValue2().indexOf("-")>0){
						   String val = getEntityValue2();
						   int index =  val.indexOf("-");
						   val = val.substring(0,index)+val.substring(index+1,val.length());
						   setEntityValue2(val);
					   }*/
					   setEntityValue2(getEntityValue2().replaceAll("-", ""));
				   }
				   //log.debug("Check Db Data of two fields against CBB "+getEntityValue2());
				   setDbValue(codelstController.getValidateDbLongDataWithAdditionalField(getEntityName(), getEntityField(), getEntityValue(), getEntityField2(), getEntityValue2(), getEntityField1() ,Long.parseLong(getEntityValue1())));
			   }else{
			      setDbValue(codelstController.getValidateDbData(getEntityName(), getEntityField(), getEntityValue(), getEntityField1() ,getEntityValue1()));
			   }
		   }
		}
		return SUCCESS;
	}
	 
}
