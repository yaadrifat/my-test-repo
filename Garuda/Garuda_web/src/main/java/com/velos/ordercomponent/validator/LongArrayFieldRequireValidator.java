package com.velos.ordercomponent.validator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.CompoundRoot;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

/**
 * @author Mohiuddin Ali Ahmed
 * @author Anurag Upadhyay
 * @version 1.0
 * 
 */

public class LongArrayFieldRequireValidator extends FieldValidatorSupport{
		private boolean searchableCheck;	

		public boolean isSearchableCheck() {
			return searchableCheck;
		}

		public void setSearchableCheck(boolean searchableCheck) {
			this.searchableCheck = searchableCheck;
		}
		
	@Override
	public void validate(Object object) throws ValidationException {
		// TODO Auto-generated method stub
		 String fieldName = getFieldName();
		 Long[]  value = (Long[]) getFieldValue(fieldName, object);
		 
		 CompoundRoot cRoot= ActionContext.getContext().getValueStack().getRoot();
		 long cordSearchable = (Long) getFieldValue("cordSearchable", cRoot); 
		 
		if(searchableCheck && (value.length<=0) && cordSearchable==1){
			   addFieldError(fieldName, object);
		       return;
		}
		
	}

}
