/**
 * 
 */
package com.velos.ordercomponent.controller;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.velos.ordercomponent.business.pojoobjects.SearchPojo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionContext;

import com.velos.ordercomponent.service.VelosService;
import com.velos.ordercomponent.service.impl.VelosServiceImpl;

/**
 * @author akajeera
 *
 */
public class VelosSearchController {
	public static final Log log = LogFactory.getLog(VelosSearchController.class);
	VelosService service;
	SearchPojo searchPojo = new SearchPojo();
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);

	public SearchPojo velosSearch(SearchPojo searchPojo) throws Exception {
		//log.debug( " contact manager");
		service = VelosServiceImpl.getService();
		try{
		Map paramMap = new HashMap();
		Enumeration paramNames = request.getParameterNames();
		while(paramNames.hasMoreElements()) {
			String paramName = (String)paramNames.nextElement();
			String[] paramValues = request.getParameterValues(paramName);
			//log.debug("Keuy " + paramName +" Value " + paramValues[0]);
			paramMap.put(paramName, paramValues[0]);
		}	
		searchPojo.setParamMap(paramMap);
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return service.velosSearch(searchPojo);
	}
}
