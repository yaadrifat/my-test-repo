package com.velos.ordercomponent.action;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.velos.ordercomponent.util.VelosGarudaConstants;



public class ExcelGeneratorAction extends VelosBaseAction {

	private Boolean unlicensed_flag;
	private Boolean inelig_incomple_flag;
	private Boolean isMultipleCBBSelected;
    private Boolean isWorkflowActivitySelected;
	private Boolean qinelig_flag;
	private Boolean qincomp_flag;
    private short columnCount;
    
    
	
	public OutputStream generateExcel(List<Object> cdrObjects, HttpServletResponse response, String paginateModule, String excelType) throws ParseException {
		Workbook workBook = null;
		workBook = (Workbook) (excelType!=null && excelType.equals("xls") ? new HSSFWorkbook() : new XSSFWorkbook());
        OutputStream out  = null;       
        try {
        	workBook = createSheet(cdrObjects , workBook, paginateModule);            
        	out = response.getOutputStream();
            workBook.write(out);            
        } catch (IOException e) {
            e.printStackTrace();
        }
        return out;
    }
	
	/**
	 * This Method Use to create CSV Document Content
	 */
	private static enum QueryBuilderResolver{
		IDSUMMARYRPT ,ANTIGENRPT,LICENSELGBLERPT,QUICKSUMMARYRPT;		
	}
	
	private Row createHeader(List<Object> cdrObjects, String paginateModule,Row Row,Workbook workBook){
		short column = 0;
		if(paginateModule!=null && paginateModule.equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_ANTIGEN_REPORT)){
			CellStyle cellStyle = workBook.createCellStyle();
	        cellStyle.setFillForegroundColor(IndexedColors.BLACK.getIndex());
	        cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
	        cellStyle.setWrapText(true);
	        
	        Font font = workBook.createFont();
	        font.setColor(IndexedColors.WHITE.getIndex());
	        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
	        cellStyle.setFont(font);
	        Row.setHeight((short)500);
            Cell hsc;
			if(getIsMultipleCBBSelected()!=null && getIsMultipleCBBSelected()==true){
				hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue("CBB");}	
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue(getText("garuda.cbuentry.label.registrycbuid"));
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue(getText("garuda.cbuentry.label.localcbuid"));
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue(getText("garuda.cdrcbuview.label.id_on_bag"));
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue(getText("garuda.shippedcbu.report.label.isbt"));
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue(getText("garuda.querybuilderreport.label.historiclocalid"));
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue(getText("garuda.cbuentry.label.maternalregistryid"));
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue(getText("garuda.cordentry.label.localmaternalid"));
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue(getText("garuda.querybuilderreport.label.activeforpatient"));
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue(getText("garuda.queryBuilder.label.cbucollectiondate"));
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue("Type");
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue("Typing Date");
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue("Received Date");
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue("Source");
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue("A Type1");
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue("A Type2");
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue("B Type1");
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue("B Type2");
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue("C Type1");
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue("C Type2");
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue("DRB1 Type1");
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue("DRB1 Type2");
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue("DRB3 Type1");
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue("DRB3 Type2");
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue("DRB4 Type1");
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue("DRB4 Type2");
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue("DRB5 Type1");
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue("DRB5 Type2");
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue("DQB1 Type1");
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue("DQB1 Type2");
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue("DPB1 Type1");
			hsc=Row.createCell(column++);hsc.setCellStyle(cellStyle);hsc.setCellValue("DPB1 Type2");
			/*Row.createCell(column++).setCellValue("DPA1 TYPE1");
			Row.createCell(column++).setCellValue("DPA1 TYPE2");
			Row.createCell(column++).setCellValue("DQA1 TYPE1");
			Row.createCell(column++).setCellValue("DQA1 TYPE2");*/
			columnCount=column;
			
		}else if(paginateModule!=null && paginateModule.equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_LICENSEANDELIGIBILTY_REPORT)){
			if(getIsMultipleCBBSelected()!=null && getIsMultipleCBBSelected()==true)
			      Row.createCell(column++).setCellValue(getText("garuda.queryBuilder.label.cbbid"));
			/*Row.createCell(column++).setCellValue("CORD REGISTRY ID");
			Row.createCell(column++).setCellValue("CORD LOCAL CBU ID");
			Row.createCell(column++).setCellValue("UNIQUE ID ON BAG");
			Row.createCell(column++).setCellValue("CBU COLLECTION DATE");
			Row.createCell(column++).setCellValue("NMDP REGISTRATION DATE");
			Row.createCell(column++).setCellValue("REASON");
			Row.createCell(column++).setCellValue("LICENSURE STATUS");
			if(getUnlicensed_flag()!=null && getUnlicensed_flag()==true)
			      Row.createCell(column++).setCellValue("UNLICENSED DESC");
			Row.createCell(column++).setCellValue("ELIGIBILITY DETERMINATION");
			if(getInelig_incomple_flag()!=null && getInelig_incomple_flag()==true)
			      Row.createCell(column++).setCellValue("INELIG INCOMP DESC");
			Row.createCell(column++).setCellValue("BABY BIRTH DATE");
			Row.createCell(column++).setCellValue("RACE");
			Row.createCell(column++).setCellValue("FUNDED");
			Row.createCell(column++).setCellValue("RACE SUMMARY");
			Row.createCell(column++).setCellValue("BACTERIAL CULTURE RESULT");
			Row.createCell(column++).setCellValue("FUNGAL CULTURE RESULT");
			Row.createCell(column++).setCellValue("HEMOGLOBIN TESTING");			
			Row.createCell(column++).setCellValue("PROCESSING PROCEDURE");
			Row.createCell(column++).setCellValue("NUCLEATED CELL COUNT");
			Row.createCell(column++).setCellValue("CD34 CELL COUNT");
			Row.createCell(column++).setCellValue("VIABILITY COUNT");
			Row.createCell(column++).setCellValue("VIABILITY METHOD");
			Row.createCell(column++).setCellValue("FINAL PRODUCT VOLUME");
			Row.createCell(column++).setCellValue("CT SHIP DATE");
			Row.createCell(column++).setCellValue("OR SHIP DATE");			
			if(getIsWorkflowActivitySelected()!=null && getIsWorkflowActivitySelected()==true)
			      Row.createCell(column++).setCellValue("REQUEST DATE");
			if(getIsWorkflowActivitySelected()!=null && getIsWorkflowActivitySelected()==true)
			      Row.createCell(column++).setCellValue("REQUEST TYPE");
			Row.createCell(column++).setCellValue("NMDP STATUS");*/
			Row.createCell(column++).setCellValue(getText("garuda.cbuentry.label.registrycbuid"));
			Row.createCell(column++).setCellValue(getText("garuda.cbuentry.label.localcbuid"));
			Row.createCell(column++).setCellValue(getText("garuda.cdrcbuview.label.id_on_bag"));
			Row.createCell(column++).setCellValue(getText("garuda.queryBuilder.label.cbucollectiondate"));
			Row.createCell(column++).setCellValue(getText("garuda.queryBuilder.nmdpregistrationDates"));
			Row.createCell(column++).setCellValue(getText("garuda.cbuentry.label.cbu_status"));
			Row.createCell(column++).setCellValue(getText("garuda.clinicalnote.label.reason"));
			Row.createCell(column++).setCellValue(getText("garuda.cdrcbuview.label.licensure_status"));
			Row.createCell(column++).setCellValue(getText("garuda.evaluHistory.report.unlicensedReason"));
			Row.createCell(column++).setCellValue(getText("garuda.cdrcbuview.label.eligibility_determination"));
			Row.createCell(column++).setCellValue(getText("garuda.queryBuilder.ineligiblereason"));
			Row.createCell(column++).setCellValue(getText("garuda.queryBuilder.incompletereason"));
			Row.createCell(column++).setCellValue(getText("garuda.queryBuilder.babyBirthDate"));
			Row.createCell(column++).setCellValue(getText("garuda.queryBuilder.race"));
			Row.createCell(column++).setCellValue(getText("garuda.cordentry.label.nmdpbroadrace"));
			Row.createCell(column++).setCellValue(getText("garuda.pendingorderpage.label.funded"));
			Row.createCell(column++).setCellValue(getText("garuda.queryBuilder.fundingcategory"));
			Row.createCell(column++).setCellValue(getText("garuda.cbuentry.label.bcresult"));
			Row.createCell(column++).setCellValue(getText("garuda.cbuentry.label.fcresult"));
			Row.createCell(column++).setCellValue(getText("garuda.minimumcriteria.label.hemoglobin"));
			Row.createCell(column++).setCellValue(getText("garuda.cbbprocedures.label.processingprocedure"));
			Row.createCell(column++).setCellValue(getText("garuda.queryBuilder.label.totalcbunucleatedcount"));
			Row.createCell(column++).setCellValue(getText("garuda.queryBuilder.label.totalcbunucleatedcountun"));
			Row.createCell(column++).setCellValue(getText("garuda.queryBuilder.totalcd34"));
			Row.createCell(column++).setCellValue(getText("garuda.queryBuilder.viabilitycount"));
			Row.createCell(column++).setCellValue(getText("garuda.common.lable.viability"));
			Row.createCell(column++).setCellValue(getText("garuda.productinsert.label.finalProdVol"));
			Row.createCell(column++).setCellValue(getText("garuda.idsReport.ctshipdate"));
			Row.createCell(column++).setCellValue(getText("garuda.idsReport.orshipdate"));
			//if(getIsWorkflowActivitySelected()!=null && getIsWorkflowActivitySelected()==true)
			Row.createCell(column++).setCellValue(getText("garuda.detailreport.cbuhistory.requesttype"));
		//	if(getIsWorkflowActivitySelected()!=null && getIsWorkflowActivitySelected()==true)
			Row.createCell(column++).setCellValue(getText("garuda.idsReport.reqdate"));
			
		}else if(paginateModule!=null && paginateModule.equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_QUICKSUMMARY_REPORT)){
			if(getIsMultipleCBBSelected()!=null && getIsMultipleCBBSelected()==true)
			      Row.createCell(column++).setCellValue(getText("garuda.queryBuilder.label.cbbid"));
			/*Row.createCell(column++).setCellValue("CORD REGISTRY ID");
			Row.createCell(column++).setCellValue("CORD LOCAL CBU ID");
			Row.createCell(column++).setCellValue("CORD ISBT DIN CODE");
			Row.createCell(column++).setCellValue("UNIQUE ID ON BAG");
			Row.createCell(column++).setCellValue("PATIENT ID");
			Row.createCell(column++).setCellValue("HISTORIC CBU ID");
			if(getIsWorkflowActivitySelected()!=null && getIsWorkflowActivitySelected()==true)
			     Row.createCell(column++).setCellValue("REQUEST TYPE");
			Row.createCell(column++).setCellValue("SHIPMENT DATE");
			if(getIsWorkflowActivitySelected()!=null && getIsWorkflowActivitySelected()==true)
			     Row.createCell(column++).setCellValue("ORDER OPEN DATE");
			Row.createCell(column++).setCellValue("INFUSION DATE");
			Row.createCell(column++).setCellValue("PATIENT DIAGNOSIS");
			Row.createCell(column++).setCellValue("TC NAME");
			Row.createCell(column++).setCellValue("TC CODE");*/
			Row.createCell(column++).setCellValue(getText("garuda.cbuentry.label.registrycbuid"));
			Row.createCell(column++).setCellValue(getText("garuda.cbuentry.label.localcbuid"));
			Row.createCell(column++).setCellValue(getText("garuda.cdrcbuview.label.id_on_bag"));
			Row.createCell(column++).setCellValue(getText("garuda.shippedcbu.report.label.isbt"));
			//if(getIsWorkflowActivitySelected()!=null && getIsWorkflowActivitySelected()==true)
			Row.createCell(column++).setCellValue(getText("garuda.cbuentry.label.requestType"));
			//if(getIsWorkflowActivitySelected()!=null && getIsWorkflowActivitySelected()==true)
			Row.createCell(column++).setCellValue(getText("garuda.idsReport.reqdate"));
			Row.createCell(column++).setCellValue(getText("garuda.idsReport.orshipdate"));
			Row.createCell(column++).setCellValue(getText("garuda.shippedcbu.report.label.orinfuseddt"));
			Row.createCell(column++).setCellValue(getText("garuda.recipient&tcInfo.label.recipientid"));
			Row.createCell(column++).setCellValue(getText("garuda.shippedcbu.report.label.patientdiag"));
			Row.createCell(column++).setCellValue(getText("garuda.shippedcbu.report.label.tccode"));
			Row.createCell(column++).setCellValue(getText("garuda.shippedcbu.report.label.tcname"));
			
				
		}else if(paginateModule!=null && paginateModule.equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_IDSUMMARY_REPORT)){
			if(getIsMultipleCBBSelected()!=null && getIsMultipleCBBSelected()==true)
			      Row.createCell(column++).setCellValue(getText("garuda.queryBuilder.label.cbbid"));
/*			Row.createCell(column++).setCellValue("CORD REGISTRY ID");
			Row.createCell(column++).setCellValue("REGISTRY MATERNAL ID");
			Row.createCell(column++).setCellValue("CORD LOCAL CBU ID");
			Row.createCell(column++).setCellValue("MATERNAL LOCAL ID");
			Row.createCell(column++).setCellValue("CORD ISBT DIN CODE");
			Row.createCell(column++).setCellValue("UNIQUE ID ON BAG");
			Row.createCell(column++).setCellValue("HISTORIC CBU ID");
			if(getIsWorkflowActivitySelected()!=null && getIsWorkflowActivitySelected()==true)
			      Row.createCell(column++).setCellValue("ORDER TYPE");
			Row.createCell(column++).setCellValue("CT SHIPMENT DATE");
			Row.createCell(column++).setCellValue("OR SHIPMENT DATE");
			if(getIsWorkflowActivitySelected()!=null && getIsWorkflowActivitySelected()==true)
			      Row.createCell(column++).setCellValue("ORDER OPEN DATE");*/
			Row.createCell(column++).setCellValue(getText("garuda.cbuentry.label.registrycbuid"));
			Row.createCell(column++).setCellValue(getText("garuda.cbuentry.label.localcbuid"));
			Row.createCell(column++).setCellValue(getText("garuda.cdrcbuview.label.id_on_bag"));
			Row.createCell(column++).setCellValue(getText("garuda.shippedcbu.report.label.isbt"));
			Row.createCell(column++).setCellValue(getText("garuda.querybuilderreport.label.historiclocalid"));
			Row.createCell(column++).setCellValue(getText("garuda.cbuentry.label.maternalregistryid"));
			Row.createCell(column++).setCellValue(getText("garuda.cordentry.label.localmaternalid"));
			/*if(getIsWorkflowActivitySelected()!=null && getIsWorkflowActivitySelected()==true)
		      Row.createCell(column++).setCellValue(getText("garuda.shippedcbu.report.label.wactivity"));
		Row.createCell(column++).setCellValue(getText("garuda.ctShipmentInfo.label.ctshipdate"));
		Row.createCell(column++).setCellValue(getText("garuda.shippedcbu.report.label.orshipdt"));
		if(getIsWorkflowActivitySelected()!=null && getIsWorkflowActivitySelected()==true)
		      Row.createCell(column++).setCellValue(getText("garuda.idsReport.reqdate"));*/
		}
		return Row;
	}
	
	private Sheet createSheet(String paginateModule, Workbook workBook,int sheetNo){
		Sheet sheet = null;
		if(paginateModule!=null && paginateModule.equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_ANTIGEN_REPORT)){
			sheet = workBook.createSheet(getText("garuda.querybuilder.report.antigen")+(sheetNo==1 ?"":sheetNo)) ;
		}else if(paginateModule!=null && paginateModule.equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_LICENSEANDELIGIBILTY_REPORT)){
			sheet = workBook.createSheet(getText("garuda.querybuilder.report.licandeligibiltiyexcel")+(sheetNo==1 ?"":sheetNo));
		}else if(paginateModule!=null && paginateModule.equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_QUICKSUMMARY_REPORT)){
			sheet = workBook.createSheet(getText("garuda.querybuilder.report.shippedcbus1")+(sheetNo==1 ?"":sheetNo));
		}else if(paginateModule!=null && paginateModule.equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_IDSUMMARY_REPORT)){
			sheet = workBook.createSheet(getText("garuda.querybuilder.report.cbuids")+(sheetNo==1 ?"":sheetNo));	 
		}
		return sheet;
	}

	private Workbook createSheet(List<Object> cdrObjects, Workbook workBook, String paginateModule) throws ParseException {
        int maxLimit = Integer.parseInt(getText("garuda.querybuilderreport.excel.size"));
        if(cdrObjects !=null && cdrObjects.size() > maxLimit){
        	int limit  = (cdrObjects.size()/maxLimit) + 1;
        	int length = cdrObjects.size();
        	for(int i=1 ; i <= limit ;i++){
        		List<Object> list = null;
        		if(i==1)      			
        		    list = cdrObjects.subList(0, (maxLimit*i));
        		else if(i==limit)
        			list = cdrObjects.subList((maxLimit*(i-1)), length);
        		else
        			list = cdrObjects.subList((maxLimit*(i-1)), (maxLimit*i));
        		
        		workBook = createSheets(list, workBook, paginateModule,i);
        	}
        }else{
        	workBook = createSheets(cdrObjects, workBook, paginateModule,1);
        }
		return workBook;
    }

	@SuppressWarnings("unchecked")
	private Workbook createSheets(List<Object> cdrObjects, Workbook workBook, String paginateModule, int sheetNo) throws ParseException {
		Sheet sheet = createSheet(paginateModule,workBook,sheetNo);
        int rowNum = 0;
        Row rowHeader = sheet.createRow(rowNum++);
        rowHeader = createHeader(cdrObjects, paginateModule, rowHeader, workBook); 
        
        CellStyle cellStyle = workBook.createCellStyle();
        CreationHelper createHelper = workBook.getCreationHelper();        
        CellStyle dateCellStyle = workBook.createCellStyle();
        
        dateCellStyle.setDataFormat(
        		 createHelper.createDataFormat().getFormat("mm/dd/yyyy"));
        
        cellStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
        cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
        cellStyle.setWrapText(false);
        Font font = workBook.createFont();
        font.setColor(IndexedColors.BLACK.getIndex());
        cellStyle.setFont(font);        
        Date date;
        Cell cell;
        switch(QueryBuilderResolver.valueOf(paginateModule.toUpperCase())){
        case IDSUMMARYRPT: 
        	 for (int j = 0; j < cdrObjects.size(); j++) {
                 Row innerRowContent = sheet.createRow(rowNum++);
                 Object o = cdrObjects.get(j);
              	  if(o!=null){
              	     Object[] objArr = (Object[])o;
              	        for(short i=1,k=0; i< objArr.length; i++){
              	        	
              	        	//System.out.println("Iteration:::::::::"+i);
              	        	
              	        	switch(i){
              	        	case 1:
              	        		String cbb = (isMultipleCBBSelected == true?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"":objArr[i].toString():"");
              	        		if(!StringUtils.isEmpty(cbb)){
              	        			innerRowContent.createCell(k).setCellValue(cbb);
              	        			k++;
              	        		}
              	        		break;
              	        	case 2:
              	        		 cell = innerRowContent.createCell(k);
              	        		 cell.setCellValue(objArr[i]!=null?objArr[i].toString():"");
              	        		/*if(getSession(getRequest()).getAttribute("duplCords")!=null && objArr[0]!=null && ((Map<Long,Long>)getSession(getRequest()).getAttribute("duplCords")).get(((BigDecimal)objArr[0]).longValue())!=null)
              	        			cell.setCellStyle(cellStyle);*/
              	        		k++;
              	        		break;
              	        	/*case 9:
              	        	   String workflow = (isWorkflowActivitySelected==true?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"":objArr[i].toString():"");
              	        	   if(!StringUtils.isEmpty(workflow)){
           	        			   innerRowContent.createCell(k).setCellValue(workflow);
           	        			   k++;
              	        	   }
              	        	   break;
              	        	
              	        	case 10:		                	        		
              	        		String ct = "";
              	        		
              	        		if(objArr[9]!=null && objArr[9].toString().trim().equals(ORDER_TYPE_CT_SHIP)){
              	        			
              	        			if(objArr[i]!=null && !StringUtils.isEmpty(objArr[i].toString())){
              	        				ct=objArr[i].toString();
              	        				innerRowContent.createCell(k).setCellValue(ct);
              	        				//System.out.println("Inside getting "+objArr[9]+"::"+ct);
              	        			}
              	        			
              	        		}
              	        		k++;
               	        	break;
              	        	case 11:		                	        		
              	        		String or = "";
              	        		if(objArr[9]!=null && objArr[9].toString().trim().equals(OR)){
              	        			
              	        			if(objArr[i]!=null && !StringUtils.isEmpty(objArr[i].toString())){
              	        				or=objArr[i].toString();
              	        				innerRowContent.createCell(k).setCellValue(or);
              	        				//System.out.println("Inside getting "+objArr[9]+"::"+or);
              	        			}
              	        			
              	        		}
              	        		k++;
               	        	   break;
              	        	case 12:
              	        		String workflowReq = (isWorkflowActivitySelected==true?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"":objArr[i].toString():"");
              	        		if(!StringUtils.isEmpty(workflowReq)){
            	        			   innerRowContent.createCell(k).setCellValue(workflowReq);
            	        			   k++;
               	        	   }
              	        	   break;
              	        	case 13:
        	        		break;
              	        	case 14:
            	        		break;*/
              	        	default:
              	        		innerRowContent.createCell(k).setCellValue(objArr[i]!=null?objArr[i].toString():"");
              	        		k++;
              	        	}              	        	
              	       }           		  
              	  }
                }	
                break;
        case  ANTIGENRPT:
	      String HLA_A=getCodeListDescByTypeAndSubtype(HLA_LOCUS, HLA_A_ST);   
		  String HLA_B=getCodeListDescByTypeAndSubtype(HLA_LOCUS, HLA_B_ST);
		  String HLA_C=getCodeListDescByTypeAndSubtype(HLA_LOCUS, HLA_C_ST);
		  String HLA_DRB1=getCodeListDescByTypeAndSubtype(HLA_LOCUS, HLA_DRB1_ST);
		  String HLA_DRB3=getCodeListDescByTypeAndSubtype(HLA_LOCUS, HLA_DRB3_ST);
		  String HLA_DRB4=getCodeListDescByTypeAndSubtype(HLA_LOCUS, HLA_DRB4_ST);
		  String HLA_DRB5=getCodeListDescByTypeAndSubtype(HLA_LOCUS, HLA_DRB5_ST);
		  String HLA_DQB1=getCodeListDescByTypeAndSubtype(HLA_LOCUS, HLA_DQB1_ST);
		  String HLA_DPB1=getCodeListDescByTypeAndSubtype(HLA_LOCUS, HLA_DPB1_ST);	
		  
      	  HashMap <String, String> defaultKeySet = new HashMap<String, String>();
      	  defaultKeySet.put("CBB", "SITE_ID");
      	  defaultKeySet.put(getText("garuda.cbuentry.label.registrycbuid"), "REGISTRY_ID");
      	  defaultKeySet.put(getText("garuda.cbuentry.label.localcbuid"), "CORD_LOCAL_CBU_ID");
      	  defaultKeySet.put(getText("garuda.cdrcbuview.label.id_on_bag"), "CORD_ID_NUMBER_ON_CBU_BAG");
      	  defaultKeySet.put(getText("garuda.shippedcbu.report.label.isbt"), "CORD_ISBI_DIN_CODE");
      	  defaultKeySet.put(getText("garuda.querybuilderreport.label.historiclocalid"), "CORD_HISTORIC_CBU_ID");
      	  defaultKeySet.put(getText("garuda.cbuentry.label.maternalregistryid"), "REGISTRY_MATERNAL_ID");
      	  defaultKeySet.put(getText("garuda.cordentry.label.localmaternalid"), "MATERNAL_LOCAL_ID");
      	  defaultKeySet.put(getText("garuda.querybuilderreport.label.activeforpatient"), "PATIENT_ID");
      	  defaultKeySet.put(getText("garuda.queryBuilder.label.cbucollectiondate"), "CBU_COLLECTION_DATE");
      	  defaultKeySet.put("Typing Date", "HLA_TYPING_DATE");
    	  defaultKeySet.put("Received Date", "HLA_RECEIVED_DATE");
    	  defaultKeySet.put("Source", "HLA_USER");
    	  defaultKeySet.put("A Type1", "A TYPE1");
    	  defaultKeySet.put("A Type2", "A TYPE2");
    	  defaultKeySet.put("B Type1", "B TYPE1");
    	  defaultKeySet.put("B Type2", "B TYPE2");
    	  defaultKeySet.put("C Type1", "C TYPE1");
    	  defaultKeySet.put("C Type2", "C TYPE2");
    	  defaultKeySet.put("DRB1 Type1", "DRB1 TYPE1");
    	  defaultKeySet.put("DRB1 Type2", "DRB1 TYPE2");
    	  defaultKeySet.put("DRB3 Type1", "DRB3 TYPE1");
    	  defaultKeySet.put("DRB3 Type2", "DRB3 TYPE2");
    	  defaultKeySet.put("DRB4 Type1", "DRB4 TYPE1");
    	  defaultKeySet.put("DRB4 Type2", "DRB4 TYPE2");
    	  defaultKeySet.put("DRB5 Type1", "DRB5 TYPE1");
    	  defaultKeySet.put("DRB5 Type2", "DRB5 TYPE2");
    	  defaultKeySet.put("DQB1 Type1", "DQB1 TYPE1");
    	  defaultKeySet.put("DQB1 Type2", "DQB1 TYPE2");
    	  defaultKeySet.put("DPB1 Type1", "DPB1 TYPE1");
    	  defaultKeySet.put("DPB1 Type2", "DPB1 TYPE2");
    	  String regId=null;
    	  String patientId=null;
    	  ArrayList lst;
    	  ArrayList templst;//for Summary of Hla
    	  HashMap<String, Object> hmp;
    	  HashMap<String, Object> summHLA;
    	  boolean firstRowStatus=false;
    	  int firstRow=0;
    	  int lastRow=0;
    	  short firstCell=0;
    	  short lastCell=8;
    	  Row grayRow = null;
    	  String hlaCodeId;
    	  short grayRowCellCount=(short)("CBB".equals(rowHeader.getCell((short)0).getStringCellValue().toString())?9:8);
    	  HashMap<String, Object> blankHistRow=new HashMap<String, Object>();
    	  blankHistRow.put("CBHLA", "History");
    	  CellStyle cs = workBook.createCellStyle();
          cs.setFillForegroundColor(IndexedColors.BLACK.getIndex());
          cs.setFillPattern(CellStyle.SOLID_FOREGROUND);
          
          CellStyle gr = workBook.createCellStyle();
          gr.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
          gr.setFillPattern(CellStyle.SOLID_FOREGROUND);
         
          
        	for (int j = 0; j < cdrObjects.size(); j++) { 
        	    	/*System.out.println("Init size : "+cdrObjects.size());
        	    	System.out.println("Counter : "+j);*/

        		short lastCellPostion=(short)("CBB".equals(rowHeader.getCell((short)0).getStringCellValue().toString())?9:8); 
                regId=(((HashMap) cdrObjects.get(j)).get("REGISTRY_ID")!=null?((HashMap) cdrObjects.get(j)).get("REGISTRY_ID").toString():"");
                patientId= (((HashMap) cdrObjects.get(j)).get("PATIENT_ID")!=null?((HashMap) cdrObjects.get(j)).get("PATIENT_ID").toString():"");
                firstRow=0;
          	    lastRow=0;
                firstCell=0;
                lastCell=lastCellPostion; 
                lst=new ArrayList();
                hmp=new HashMap<String, Object>();
                summHLA= new HashMap<String, Object>();
             			 outer: for(int i=j;i<cdrObjects.size();i++){
                				if(regId.equals((((HashMap) cdrObjects.get(i)).get("REGISTRY_ID"))) && (patientId.equals((((HashMap) cdrObjects.get(j)).get("PATIENT_ID")!=null?((HashMap) cdrObjects.get(j)).get("PATIENT_ID").toString():"")))){
                   					if("0".equals(((HashMap) cdrObjects.get(i)).get("CBHLA").toString())){//summary of HLA
                   						if(summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID")) !=null){
                   							HashMap o= (HashMap) cdrObjects.get(i);
                   							hlaCodeId=(String) o.get("FK_HLA_CODE_ID");
                   									if(HLA_A.equals(hlaCodeId)){
                   											((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("A TYPE1", o.get("HLA_TYPE1"));
                   											((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("A TYPE2", o.get("HLA_TYPE2"));
                   									} else if(HLA_B.equals(hlaCodeId)){
                   											((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("B TYPE1", o.get("HLA_TYPE1"));
                              	        					((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("B TYPE2", o.get("HLA_TYPE2"));
                                	        		}else if(HLA_C.equals(hlaCodeId)){
                             	        		    		((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("C TYPE1", o.get("HLA_TYPE1"));
                             	        		    		((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("C TYPE2", o.get("HLA_TYPE2"));
                                	        		}else if(HLA_DRB1.equals(hlaCodeId)){
                              	        					((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("DRB1 TYPE1", o.get("HLA_TYPE1"));
                              	        					((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("DRB1 TYPE2", o.get("HLA_TYPE2"));
                                	        		} else if(HLA_DRB3.equals(hlaCodeId)){
                                	        				((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("DRB3 TYPE1", o.get("HLA_TYPE1"));
                                	        				((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("DRB3 TYPE2", o.get("HLA_TYPE2"));
                                	        		} else if(HLA_DRB4.equals(hlaCodeId)){
                                	        				((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("DRB4 TYPE1", o.get("HLA_TYPE1"));
                            	        					((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("DRB4 TYPE2", o.get("HLA_TYPE2"));
                                	        		}else if(HLA_DRB5.equals(hlaCodeId)){
                                	        				((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("DRB5 TYPE1", o.get("HLA_TYPE1"));
                            	        					((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("DRB5 TYPE2", o.get("HLA_TYPE2"));
                                	        		} else if(HLA_DQB1.equals(hlaCodeId)){
                                	        				((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("DQB1 TYPE1", o.get("HLA_TYPE1"));
                                	        				((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("DQB1 TYPE2", o.get("HLA_TYPE2"));
                                	        		} else if(HLA_DPB1.equals(hlaCodeId)){
                                	        				((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("DPB1 TYPE1", o.get("HLA_TYPE1"));
                                	        				((HashMap) summHLA.get(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString())).put("DPB1 TYPE2", o.get("HLA_TYPE2"));
                                	        		} 
                   						}else{

                   							if(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID")!=null){
                   								HashMap o= (HashMap) cdrObjects.get(i);
                   								hlaCodeId=(String) o.get("FK_HLA_CODE_ID");
                   									if(HLA_A.equals(hlaCodeId)){
                   							    		o.put("A TYPE1", o.get("HLA_TYPE1"));
                   							    		o.put("A TYPE2", o.get("HLA_TYPE2"));
      	                      	        		  }	else if(HLA_B.equals(hlaCodeId)){
      	                          	        			o.put("B TYPE1", o.get("HLA_TYPE1"));
      	                          	        			o.put("B TYPE2", o.get("HLA_TYPE2"));
      	                       	        		   }else if(HLA_C.equals(hlaCodeId)){
      	                       	        			   o.put("C TYPE1", o.get("HLA_TYPE1"));
      	                       	        			   o.put("C TYPE2", o.get("HLA_TYPE2"));
      	                         	        		} else if(HLA_DRB1.equals(hlaCodeId)){
      	                        	        			o.put("DRB1 TYPE1", o.get("HLA_TYPE1"));
      	                        	        			o.put("DRB1 TYPE2", o.get("HLA_TYPE2"));
      	                          	        		}else if(HLA_DRB3.equals(hlaCodeId)){
      	                        	        			o.put("DRB3 TYPE1", o.get("HLA_TYPE1"));
      	                        	        			o.put("DRB3 TYPE2", o.get("HLA_TYPE2"));
      	                          	        		} else if(HLA_DRB4.equals(hlaCodeId)){
      	                        	        			o.put("DRB4 TYPE1", o.get("HLA_TYPE1"));
      	                        	        			o.put("DRB4 TYPE2", o.get("HLA_TYPE2"));
      	                          	        		} else if(HLA_DRB5.equals(hlaCodeId)){
      	                        	        			o.put("DRB5 TYPE1", o.get("HLA_TYPE1"));
      	                        	        			o.put("DRB5 TYPE2", o.get("HLA_TYPE2"));
      	                          	        		}else if(HLA_DQB1.equals(hlaCodeId)){
      	                        	        			o.put("DQB1 TYPE1", o.get("HLA_TYPE1"));
      	                        	        			o.put("DQB1 TYPE2", o.get("HLA_TYPE2"));
      	                          	        		} else if(HLA_DPB1.equals(hlaCodeId)){
      	                        	        			o.put("DPB1 TYPE1", o.get("HLA_TYPE1"));
      	                        	        			o.put("DPB1 TYPE2", o.get("HLA_TYPE2"));
      	                          	        		} 
                   								
      	                       	        		   summHLA.put(((HashMap) cdrObjects.get(i)).get("REGISTRY_ID").toString(), cdrObjects.get(i));	                  							
               							   }               						
                   							
                   						}
                   						
                   					}else{//History
                   						if(hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ")!=null?((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString():"")!=null){
                   							HashMap o= (HashMap) cdrObjects.get(i);
                   							hlaCodeId=(String) o.get("FK_HLA_CODE_ID");
                   									if(HLA_A.equals(hlaCodeId)){
                   										((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("A TYPE1", o.get("HLA_TYPE1"));
                   										((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("A TYPE2", o.get("HLA_TYPE2"));  
                   									}   else if(HLA_B.equals(hlaCodeId)){
                              	        				((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("B TYPE1", o.get("HLA_TYPE1"));
                              	        				((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("B TYPE2", o.get("HLA_TYPE2"));
                                	        		} else if(HLA_C.equals(hlaCodeId)){
                             	        			   ((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("C TYPE1", o.get("HLA_TYPE1"));
                             	        				((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("C TYPE2", o.get("HLA_TYPE2"));
                                	        		} else if(HLA_DRB1.equals(hlaCodeId)){
                              	        				((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("DRB1 TYPE1", o.get("HLA_TYPE1"));
                              	        				((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("DRB1 TYPE2", o.get("HLA_TYPE2"));
                                	        		} else if(HLA_DRB3.equals(hlaCodeId)){
                                	        			((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("DRB3 TYPE1", o.get("HLA_TYPE1"));
                                	        			((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("DRB3 TYPE2", o.get("HLA_TYPE2"));
                                	        		} else if(HLA_DRB4.equals(hlaCodeId)){
                                	        			((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("DRB4 TYPE1", o.get("HLA_TYPE1"));
                                	        			((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("DRB4 TYPE2", o.get("HLA_TYPE2"));
                                	        		} else if(HLA_DRB5.equals(hlaCodeId)){
                                	        			((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("DRB5 TYPE1", o.get("HLA_TYPE1"));
                                	        			((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("DRB5 TYPE2", o.get("HLA_TYPE2"));
                                	        		} else if(HLA_DQB1.equals(hlaCodeId)){
                                	        			((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("DQB1 TYPE1", o.get("HLA_TYPE1"));
                                	        			((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("DQB1 TYPE2", o.get("HLA_TYPE2"));
                                	        		} else if(HLA_DPB1.equals(hlaCodeId)){
                                	        			((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("DPB1 TYPE1", o.get("HLA_TYPE1"));
                                	        			((HashMap) hmp.get(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString())).put("DPB1 TYPE2", o.get("HLA_TYPE2"));
                                	        		}
                   							
                   						}else{ 
      	             							if(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ")!=null /*&& regId.equals(((HashMap) cdrObjects.get(i+1)).get("REGISTRY_ID"))*/){
      	             								HashMap o= (HashMap) cdrObjects.get(i);
      	             								hlaCodeId=(String) o.get("FK_HLA_CODE_ID");
      	             								
      	             									if(HLA_A.equals(hlaCodeId)){
      	             									  	o.put("A TYPE1", o.get("HLA_TYPE1"));	
      	             									  	o.put("A TYPE2", o.get("HLA_TYPE2"));		
      		                      	        		  } else if(HLA_B.equals(hlaCodeId)){
      		                          	        			o.put("B TYPE1", o.get("HLA_TYPE1"));
      		                          	        		    o.put("B TYPE2", o.get("HLA_TYPE2"));	
      		                       	        		   }else if(HLA_C.equals(hlaCodeId)){
      			                       	        			o.put("C TYPE1", o.get("HLA_TYPE1"));
      			                       	        			o.put("C TYPE2", o.get("HLA_TYPE2"));
      		                         	        		}else if(HLA_DRB1.equals(hlaCodeId)){
      		                        	        			o.put("DRB1 TYPE1", o.get("HLA_TYPE1"));
      		                        	        			o.put("DRB1 TYPE2", o.get("HLA_TYPE2"));
      		                          	        		}else if(HLA_DRB3.equals(hlaCodeId)){
      		                        	        			o.put("DRB3 TYPE1", o.get("HLA_TYPE1"));
      		                        	        			o.put("DRB3 TYPE2", o.get("HLA_TYPE2"));
      		                          	        		}else if(HLA_DRB4.equals(hlaCodeId)){
      		                        	        			o.put("DRB4 TYPE1", o.get("HLA_TYPE1"));
      		                        	        			o.put("DRB4 TYPE2", o.get("HLA_TYPE2"));
      		                          	        		}else if(HLA_DRB5.equals(hlaCodeId)){
      		                        	        			o.put("DRB5 TYPE1", o.get("HLA_TYPE1"));
      		                        	        			o.put("DRB5 TYPE2", o.get("HLA_TYPE2"));
      		                          	        		}else if(HLA_DQB1.equals(hlaCodeId)){
      		                        	        			o.put("DQB1 TYPE1", o.get("HLA_TYPE1"));
      		                        	        			o.put("DQB1 TYPE2", o.get("HLA_TYPE2"));
      		                          	        		}else if(HLA_DPB1.equals(hlaCodeId)){
      		                        	        			o.put("DPB1 TYPE1", o.get("HLA_TYPE1"));
      		                        	        			o.put("DPB1 TYPE2", o.get("HLA_TYPE2"));
      		                          	        		}
      	             							      hmp.put(((HashMap) cdrObjects.get(i)).get("CB_HLA_ORDER_SEQ").toString(), cdrObjects.get(i));	  	             							
                   							}
                   						}
                   					}
                   					j++;
                   				}else{break outer;}    				  
                   			  }
                            firstRow =rowNum+1;
			                if(summHLA.size()>0 || hmp.size()>0){
			                	 j--;
			 					firstRowStatus=false;			 					
			 					//Summary of hla
			 					for(Map.Entry<String, Object> entry: summHLA.entrySet()) {             				       
			 				       lst.add(entry.getValue());
			 				    }   
			 					// History
			 					for(Map.Entry<String, Object> entry: hmp.entrySet()) {             				       
			 				       lst.add(entry.getValue());
			 				    }  
			 					if(lst.size()==1){// to manage history if no avalable in list		 						
			 						lst.add(blankHistRow);
			 					} 					
			 				}         
             		
                      for(int i=0;i<lst.size();i++){ 
                    	if(i==1){
                    		 grayRow=sheet.createRow(rowNum++);                   		
                    	}
                    	Row innerRowContent = sheet.createRow(rowNum++);
                    	HashMap o = (HashMap) lst.get(i);	
          	         for(short k=0; k< columnCount; k++){          	        	
          	        	 	cell = innerRowContent.createCell(k); 
          	        		String cellVal=rowHeader.getCell(k).getStringCellValue()!=null?rowHeader.getCell(k).getStringCellValue().toString():"";
          	        		if(cellVal!=null && firstRowStatus && (cellVal.equals("CBB") 
          	        				                           || cellVal.equals("CBU Registry ID")
          	        				                           || cellVal.equals("CBU Local ID")
          	        				                           || cellVal.equals("Unique Product Identity on Bag")
          	        				                           || cellVal.equals("ISBT")
          	        				                           || cellVal.equals("Historic Local CBU ID")
          	        				                           || cellVal.equals("Maternal Registry ID")
          	        				                           || cellVal.equals("Maternal Local ID")
          	        				                           || cellVal.equals("Active for Patient (ID)")
          	        				                           || cellVal.equals("CBU Collection Date"))){      	        			             	        		
          	        		
          	        					cell.setCellValue("");          	        			
          	        			}else if(!firstRowStatus && cellVal.equals("Active for Patient (ID)")){
          	        				cell.setCellValue(o.get(defaultKeySet.get(rowHeader.getCell(k).getStringCellValue()))!=null?o.get(defaultKeySet.get(rowHeader.getCell(k).getStringCellValue())).toString():"");
          	        				/*if(o.get("CORD_NMDP_STATUS")!=null && (getCbuStatusByCode(AC).getPkcbustatus().equals(((BigDecimal)o.get("CORD_NMDP_STATUS")).longValue()))){
          	        					cell.setCellValue(o.get(defaultKeySet.get(rowHeader.getCell(k).getStringCellValue()))!=null?o.get(defaultKeySet.get(rowHeader.getCell(k).getStringCellValue())).toString():"");
          	        				}else{
          	        					cell.setCellValue("");
          	        				} */         	        
          	        			}else if(!firstRowStatus && cellVal.equals("CBU Collection Date")){
          	        				if( o.get(defaultKeySet.get(rowHeader.getCell(k).getStringCellValue()))!=null && !((String)o.get(defaultKeySet.get(rowHeader.getCell(k).getStringCellValue()))).isEmpty()){               	        		
			          	        		date = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH).parse((String)o.get(defaultKeySet.get(rowHeader.getCell(k).getStringCellValue())));          	        		
			          	        		cell.setCellStyle(dateCellStyle);
			          	        		cell.setCellValue(date);          	        				
          	        				}	
          	        			}else if(cellVal!=null && cellVal.equals("Type")){          	        			
          	        					cell.setCellValue((o.get("CBHLA")!=null && ("0").equals(o.get("CBHLA").toString())?"Summary of HLA":i==1?"History":""));

          	        			}else if(!firstRowStatus && (cellVal.equals("Typing Date") || cellVal.equals("Received Date") || cellVal.equals("Source"))){
          	        					cell.setCellStyle(gr);
          	        			}else if(firstRowStatus && (cellVal.equals("Typing Date") || cellVal.equals("Received Date"))){
          	        			if(o.get(defaultKeySet.get(rowHeader.getCell(k).getStringCellValue()))!=null && !((String)o.get(defaultKeySet.get(rowHeader.getCell(k).getStringCellValue()))).isEmpty()){
          	        					date = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH).parse((String)o.get(defaultKeySet.get(rowHeader.getCell(k).getStringCellValue())));          	        		
          	        					cell.setCellStyle(dateCellStyle);
          	        					cell.setCellValue(date);   
          	        			}          	        			       			
          	        		}else{
          	        					cell.setCellValue(o.get(defaultKeySet.get(rowHeader.getCell(k).getStringCellValue()))!=null?o.get(defaultKeySet.get(rowHeader.getCell(k).getStringCellValue())).toString():"");          	        		
          	        		}
          	        	}      	        	
          	       
          	       firstRowStatus=true;
             	  }
                    if(lst.size()>0){
                    		 lastRow=rowNum-1;
     	                    //Excel Formating
     	                    
     	                    sheet.addMergedRegion(new CellRangeAddress(//white area merging
     	                    		firstRow,
     	                    		lastRow,
     	                    		firstCell,	                    		
     	                    		lastCell                    		
     	                    ));
     	                    sheet.addMergedRegion(new CellRangeAddress(//grey area merging
     	                    		firstRow-1,
     	                    		firstRow-1,
     	                    		lastCell+2,	                    		
     	                    		lastCell+3                    		
     	                    ));
     	                    sheet.addMergedRegion(new CellRangeAddress(//grey area merging
     	                    		firstRow,
     	                    		firstRow,
     	                    		lastCell+1,	                    		
     	                    		lastCell+22                    		
     	                    ));
     	                    Row blankRow = sheet.createRow(rowNum++); 
     	                     for (short k=0; k< columnCount; k++) {  
     	                    	blankRow.createCell(k).setCellStyle(cs); 
     	                    	if(k>=grayRowCellCount)
     	                    		grayRow.createCell(k).setCellStyle(gr);
     	                    } 
     	                    firstRow=lastRow=rowNum-1;
     	                    firstCell=0;
     	                    lastCell=(short)(columnCount-1);
     	                    sheet.addMergedRegion(new CellRangeAddress( // black area merging
     	                    		firstRow,
     	                    		lastRow,
     	                    		firstCell,	                    		
     	                    		lastCell                    		
     	                    ));
                  }
          	  }
      	  break;
        case LICENSELGBLERPT:
      	  
        	for (int j = 0; j < cdrObjects.size(); j++) {
                Row innerRowContent = sheet.createRow(rowNum++);
                Object o = cdrObjects.get(j);
             	  if(o!=null){
             	     Object[] objArr = (Object[])o;
          	        for(short i=1,k=0; i< objArr.length; i++){
          	        	date=null;cell=null;
          	        	switch(i){
          	        	case 1:
          	        		String cbb = (isMultipleCBBSelected == true?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"":objArr[i].toString():"");
          	        		if(!StringUtils.isEmpty(cbb)){
          	        			innerRowContent.createCell(k).setCellValue(cbb);
          	        			k++;
          	        		}
          	        		break;
          	        	case 2:
          	        		cell = innerRowContent.createCell(k);
          	        		cell.setCellValue(objArr[i]!=null?objArr[i].toString():"");
          	        		if(getSession(getRequest()).getAttribute("orDuplCords")!=null && objArr[0]!=null && ((Map<Long,Long>)getSession(getRequest()).getAttribute("orDuplCords")).get(((BigDecimal)objArr[0]).longValue())!=null)
          	        			cell.setCellStyle(cellStyle);
          	        		k++;
          	        		break;	
          	        	case 5:
          	        	case 6:
          	        	case 13:          	        	
          	        		cell = innerRowContent.createCell(k);
          	        		cell.setCellStyle(dateCellStyle);
          	        		if(objArr[i]!=null && !((String)objArr[i]).isEmpty()){
	          	        		date = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH).parse((String)objArr[i]);          	        		
	          	        		cell.setCellValue(date);
          	        		}else{
          	        			cell.setCellValue("");
          	        		}
          	        		k++;             	        	    
          	        	    break;
          	        	case 11:
          	        		if(objArr[11]==null || objArr[11].toString().equals("")){
          	        			objArr[11]="Not Yet Determined";
          	        			innerRowContent.createCell(k).setCellValue(objArr[i].toString());
          	        			k++;
          	        		}
          	        		else{
          	        			innerRowContent.createCell(k).setCellValue(objArr[i].toString());
          	        			k++;
          	        		}
          	        		break;
          	        		
          	        	case 12:		                	        		
          	        		/*String inelig_incomple = (inelig_incomple_flag==true?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"":objArr[i].toString():"");
          	        		if(!StringUtils.isEmpty(inelig_incomple)){
         	        			  innerRowContent.createCell(k).setCellValue(inelig_incomple);
         	        			  k++;
         	        		}*/
          	        		
          	        		if(qinelig_flag!=null && qincomp_flag!=null){
          	        			
          	        			if(objArr[11]!=null && objArr[11].toString().equals(INELIGIBLE_STR) && objArr[i]!=null && !objArr[i].toString().equals("")){
              	        			innerRowContent.createCell(k).setCellValue(objArr[i].toString());
              	        			k++;
            	        		}else{
            	        			innerRowContent.createCell(k).setCellValue("");
            	        			k++;
            	        		}
          	        		    if(objArr[11]!=null && objArr[11].toString().equals(INCOMPLETE_STR) && objArr[i]!=null && !objArr[i].toString().equals("")){
              	        			innerRowContent.createCell(k).setCellValue(objArr[i].toString());
              	        			k++;
            	        		}else{
            	        			innerRowContent.createCell(k).setCellValue("");
            	        			k++;
            	        		}
          	        			
          	        		}/*else if((qinelig_flag==true && qincomp_flag==false)|| (qinelig_flag==false && qincomp_flag==true)){
          	        			if(qinelig_flag!=null && qinelig_flag==true && objArr[11]!=null){
              	        			innerRowContent.createCell(k).setCellValue(objArr[i].toString());
              	        			k++;
            	        		}
          	        			if(qincomp_flag!=null && qincomp_flag==true && objArr[11]!=null){
              	        			innerRowContent.createCell(k).setCellValue(objArr[i].toString());
              	        			k++;
            	        		}
          	        		}*/
          	        		break;
          	        	case 28:
          	        		cell = innerRowContent.createCell(k); 
          	        		if(objArr[30]!=null && objArr[30].toString().trim().equals(ORDER_TYPE_CT_SHIP)){
          	        			
          	        			if(objArr[i]!=null && !StringUtils.isEmpty(objArr[i].toString())){
          	        				date = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH).parse(objArr[i].toString());          	        		
    	          	        		cell.setCellStyle(dateCellStyle);
    	          	        		cell.setCellValue(date);   
          	        				//ct=objArr[i].toString();
          	        				//innerRowContent.createCell(k).setCellValue(ct);
          	        			}
          	        		}
          	        		k++;
       	        		    break;
          	        	case 29:
          	        		cell = innerRowContent.createCell(k); 
          	        		if(objArr[30]!=null && objArr[30].toString().trim().equals(OR)){
          	        			
          	        			if(objArr[i]!=null && !StringUtils.isEmpty(objArr[i].toString())){
          	        				date = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH).parse(objArr[i].toString());          	        		
    	          	        		cell.setCellStyle(dateCellStyle);
    	          	        		cell.setCellValue(date);   
          	        				//or=objArr[i].toString();
          	        				//innerRowContent.createCell(k).setCellValue(or);
          	        			}
          	        		}
          	        		k++;
       	        		    break;
          	        	/*case 30:		                	        		
          	        	    String work = (isWorkflowActivitySelected==true?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"":objArr[i].toString():"");
          	        	    if(!StringUtils.isEmpty(work)){
       	        			  innerRowContent.createCell(k).setCellValue(work);
       	        			  k++;
       	        		    }
          	        	    break;
          	        	case 31:		                	        		
          	        	    String workdate = (isWorkflowActivitySelected==true?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"":objArr[i].toString():"");
          	        	    if(!StringUtils.isEmpty(workdate)){
       	        			  innerRowContent.createCell(k).setCellValue(workdate);
       	        			  k++;
       	        		    }
          	        	    break;*/
          	        	case 31:
          	        		cell = innerRowContent.createCell(k); 
          	        		if(objArr[i]!=null && !((String)objArr[i]).isEmpty()){
	          	        		date = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH).parse((String)objArr[i]);
	          	        		cell.setCellStyle(dateCellStyle);
	          	        		cell.setCellValue(date);
          	        		}
          	        		k++;
          	        		break;
          	        	case 32:
          	        		break;
          	        	case 33:
          	        		break;
          	        	case 34:
          	        		break;
          	        	case 35:
          	        		break;
          	        	case 36:
          	        		break;
          	        	case 37:
          	        		break;
         	        	case 38:
          	        		break;
          	        	default:
          	        		innerRowContent.createCell(k).setCellValue(objArr[i]!=null?objArr[i].toString():"");
          	        		k++;
          	        	}          	        	
          	       }           		  
          	  }
            }	
      	  break;
        case QUICKSUMMARYRPT:
        	for (int j = 0; j < cdrObjects.size(); j++) {
                Row innerRowContent = sheet.createRow(rowNum++);
                Object o = cdrObjects.get(j);
             	  if(o!=null){
             	     Object[] objArr = (Object[])o;
          	        for(short i=1,k=0; i< objArr.length; i++){
          	        	date=null;cell=null;
          	        	switch(i){
          	        	case 1:
          	        		String cbb = (isMultipleCBBSelected == true?(objArr[i]==null) || (objArr[i]!=null && StringUtils.isEmpty(objArr[i].toString()))?"":objArr[i].toString():"");
          	        		if(!StringUtils.isEmpty(cbb)){
          	        			innerRowContent.createCell(k).setCellValue(cbb);
          	        			k++;
          	        		}break;
          	        	case 2:
          	        		 cell = innerRowContent.createCell(k);
          	        		cell.setCellValue(objArr[i]!=null?objArr[i].toString():"");
          	        		if(getSession(getRequest()).getAttribute("orDuplCords")!=null && objArr[0]!=null && ((Map<Long,Long>)getSession(getRequest()).getAttribute("orDuplCords")).get(((BigDecimal)objArr[0]).longValue())!=null)
          	        			cell.setCellStyle(cellStyle);
          	        		k++;
          	        		break;
          	        	case 6:		                	        		
          	        	    break;
          	        	case 7:		                	        		
          	        	    break;
          	        	case 8:		                	        		
          	        	    break;
          	        	/*case 10:
          	        		cell = innerRowContent.createCell(k);
          	        		cell.setCellStyle(dateCellStyle);
          	        		if(objArr[i]!=null && !((String)objArr[i]).isEmpty()){
	          	        		date = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH).parse((String)objArr[i]);          	        		
	          	        		cell.setCellValue(date);
          	        		}else{
          	        			cell.setCellValue("");
          	        		}
          	        		k++;  */        	        
          	        	case 11:
          	        		cell = innerRowContent.createCell(k);          	        		
          	        		if(objArr[9]!=null && objArr[9].toString().trim().equals(OR) && objArr[i]!=null && !StringUtils.isEmpty(objArr[i].toString())){ 
          	        				cell.setCellStyle(dateCellStyle);
          	        				date = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH).parse((String)objArr[i]);          	        		
    	          	        		cell.setCellValue(date);
          	        			
          	        		}        	        		
          	        		k++;
          	        	    break;
          	        	case 10:          	        	
          	        	case 12:
          	        		cell = innerRowContent.createCell(k);          	        		
          	        		if(objArr[i]!=null && !((String)objArr[i]).isEmpty()){
          	        			cell.setCellStyle(dateCellStyle);
	          	        		date = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH).parse((String)objArr[i]);          	        		
	          	        		cell.setCellValue(date);
          	        		}
          	        		k++;
          	        		break;
          	        	case 17:		                	        		
          	        	    break;
          	        	case 18:		                	        		
          	        	    break;
          	        	case 19:		                	        		
          	        	    break;
          	        	default:
          	        		innerRowContent.createCell(k).setCellValue(objArr[i]!=null?objArr[i].toString():"");
          	        		k++;
          	        	}          	        	
          	       }           		  
          	  }
            }	
      	    break;
         }
        Row blankRow2 = sheet.createRow(rowNum++);
        blankRow2.createCell((short) 0).setCellValue("");   
        Row blankRow3 = sheet.createRow(rowNum++);
        blankRow3.createCell((short) 0).setCellValue("");
        return workBook;
        
	}

	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Boolean getUnlicensed_flag() {
		return unlicensed_flag;
	}

	public void setUnlicensed_flag(Boolean unlicensedFlag) {
		unlicensed_flag = unlicensedFlag;
	}

	public Boolean getInelig_incomple_flag() {
		return inelig_incomple_flag;
	}

	public void setInelig_incomple_flag(Boolean ineligIncompleFlag) {
		inelig_incomple_flag = ineligIncompleFlag;
	}

	public Boolean getIsMultipleCBBSelected() {
		return isMultipleCBBSelected;
	}

	public void setIsMultipleCBBSelected(Boolean isMultipleCBBSelected) {
		this.isMultipleCBBSelected = isMultipleCBBSelected;
	}

	public Boolean getIsWorkflowActivitySelected() {
		return isWorkflowActivitySelected;
	}

	public void setIsWorkflowActivitySelected(Boolean isWorkflowActivitySelected) {
		this.isWorkflowActivitySelected = isWorkflowActivitySelected;
	}
	public Boolean getQinelig_flag() {
		return qinelig_flag;
	}
	public Boolean getQincomp_flag() {
		return qincomp_flag;
	}
	public void setQinelig_flag(Boolean qinelig_flag) {
		this.qinelig_flag = qinelig_flag;
	}
	public void setQincomp_flag(Boolean qincomp_flag) {
		this.qincomp_flag = qincomp_flag;
	}
}
