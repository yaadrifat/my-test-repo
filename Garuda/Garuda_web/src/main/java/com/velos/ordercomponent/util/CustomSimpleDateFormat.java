package com.velos.ordercomponent.util;

import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
 * @author Anurag Upadhyay
 * for handling the different date format parsing
 * 
 * 
 * */
public class CustomSimpleDateFormat extends SimpleDateFormat{
	
	//DateFormat dat = null;// =new SimpleDateFormat("MMM dd, yyyy");
	DateFormat dat1 = null; //=new SimpleDateFormat("MM/dd/yyyy");
	
	public CustomSimpleDateFormat() {
		
		super("MMM dd, yyyy");		
		//this.dat =new SimpleDateFormat("MMM dd, yyyy");
	    this.dat1 =new SimpleDateFormat("MM/dd/yyyy");
	}
	
	 public Date parse(String source) throws ParseException
	    {
		  Date result = null;
		  
		  if(source !=null && !source.isEmpty()){
				       
			  	if(source.contains("/")){
			  		
			  		result = dat1.parse(source);
			  		return result;
			  	}
			  	result = super.parse(source);
		  }
	        return result;
	    }

}
