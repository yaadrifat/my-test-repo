package com.velos.ordercomponent.controller;


import java.util.List;

import com.velos.ordercomponent.business.domain.CBUploadInfo;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.LabMaintPojo;
import com.velos.ordercomponent.business.pojoobjects.OrderAttachmentPojo;
import com.velos.ordercomponent.business.pojoobjects.PatientLabPojo;

import com.velos.ordercomponent.service.VelosService;
import com.velos.ordercomponent.service.impl.VelosServiceImpl;


public class UploadDocController {
	
	VelosService service;
	
	public String getAttachments(Long entityId,Long entityTypeId,Long attachmentType,String categoryName) throws Exception {
		service = VelosServiceImpl.getService();
		return service.getAttachments(entityId,entityTypeId,attachmentType,categoryName);
		
	}
	
	public List<CBUploadInfo> getCbUploadInfoLst(Long attachmentId) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCbUploadInfoLst(attachmentId);
	}
	
	public String getOtherTestAttachInfo(Long entityId,Long entityTypeId,Long attachmentType,String categoryName) throws Exception {
		service = VelosServiceImpl.getService();
		return service.getOtherTestAttachInfo(entityId,entityTypeId,attachmentType,categoryName);
		
	}
	public String getHealthHistoryInfo(Long entityId,Long entityTypeId,Long attachmentType,String categoryName) throws Exception {
		service = VelosServiceImpl.getService();
		return service.getHealthHistoryInfo(entityId,entityTypeId,attachmentType,categoryName);
		
	}
	public String getEligibilityAttachInfo(Long entityId,Long entityTypeId,Long attachmentType,String categoryName) throws Exception {
		service = VelosServiceImpl.getService();
		return service.getEligibilityAttachInfo(entityId,entityTypeId,attachmentType,categoryName);
		
	}
	
	
	public String getEntityDocuments(Long entityId,Long entityTypeId,Long attachmentType) throws Exception {
		service = VelosServiceImpl.getService();
		return service.getEntityDocuments(entityId,entityTypeId,attachmentType);
		
	}
	public Long getCodeListPkIds(String parentCode, String code)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCodeListPkIds(parentCode, code);
	}
	public Long getCbuPkCordId(CdrCbuPojo cdrCbuPojo)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCbuPkCordId(cdrCbuPojo);
	}
	
}
