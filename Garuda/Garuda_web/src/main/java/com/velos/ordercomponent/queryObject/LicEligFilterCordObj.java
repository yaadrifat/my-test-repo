package com.velos.ordercomponent.queryObject;

import java.util.Map;
import java.util.concurrent.Callable;

import com.velos.ordercomponent.controller.VDAController;

public class LicEligFilterCordObj implements Runnable{
	
	private VDAController vdaController;
	private String sqlQuery;
	private Map<Long, Object[]> cdr;
	
	public LicEligFilterCordObj(String sqlQuery, VDAController vdaController) {

		this.vdaController = vdaController;
		this.sqlQuery =  sqlQuery;
	}

/*	@Override
	public Map<Long, Object[]> call() throws Exception {
		
		return vdaController.getQueryBuilderMapObject(sqlQuery);
	}*/

	@Override
	public void run() {
		
		try {
			cdr = vdaController.getQueryBuilderMapObject(sqlQuery);
			
			System.out.println(" LIC & ELIG Finished ----------------------- >");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	

	/**
	 * @return the cdr
	 */
	public Map<Long, Object[]> getCdr() {
		return cdr;
	}

}
