package com.velos.ordercomponent.validator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.CompoundRoot;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

/**
 * @author Mohiuddin Ali Ahmed
 * @author Anurag Upadhyay
 * @version 1.0
 * 
 */

public class CheckLength extends FieldValidatorSupport{
	private boolean cdrUser;
	private String cdrUserField="";
	
	public boolean isCdrUser() {
		return cdrUser;
	}
	public void setCdrUser(boolean cdrUser) {
		this.cdrUser = cdrUser;
	}
	public String getCdrUserField() {
		return cdrUserField;
	}
	public void setCdrUserField(String cdrUserField) {
		this.cdrUserField = cdrUserField;
	}
	@Override
	public void validate(Object object) throws ValidationException {
		// TODO Auto-generated method stub
			String fieldName= getFieldName();		
			String value=(String) getFieldValue(fieldName, object);
		if(cdrUser){
			CompoundRoot cRoot= ActionContext.getContext().getValueStack().getRoot();
			boolean isCDRUser=(Boolean) getFieldValue(cdrUserField, cRoot);			
			if( isCDRUser && value!=null && value.replaceAll("-", "").length()!=9){			
				 addFieldError(fieldName, object);
			}else if(!isCDRUser && value != null && value.length()>30){//Non System User
				addFieldError(fieldName, object);
			}
		}/*else if(value!=null && value.replaceAll("-", "").length()!=9){
			addFieldError(fieldName, object);
		}*/
	}

}
