package com.velos.ordercomponent.validator;

import com.opensymphony.xwork2.ActionContext;

import com.opensymphony.xwork2.util.CompoundRoot;

import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

/**
 * @author Mohiuddin Ali Ahmed
 * @author Anurag Upadhyay
 * @version 1.0
 * 
 */

public class CheckFormat extends FieldValidatorSupport {
	private boolean cdrUser;
	private String cdrUserField="";     
	private String defaultFieldValue="";
     
	public boolean isCdrUser() {
		return cdrUser;
	}

	public void setCdrUser(boolean cdrUser) {
		this.cdrUser = cdrUser;
	}	

	public String getCdrUserField() {
		return cdrUserField;
	}

	public void setCdrUserField(String cdrUserField) {
		this.cdrUserField = cdrUserField;
	}

	public String getDefaultFieldValue() {
		return defaultFieldValue;
	}

	public void setDefaultFieldValue(String defaultFieldValue) {
		this.defaultFieldValue = defaultFieldValue;
	}

	@Override
	public void validate(Object object) throws ValidationException {
		// TODO Auto-generated method stub
		
		String fieldName= getFieldName();
		String value=(String) getFieldValue(fieldName, object);
		
		if(cdrUser){
			 CompoundRoot cRoot= ActionContext.getContext().getValueStack().getRoot();
			 String defaultValue = (String) getFieldValue(defaultFieldValue, cRoot);
		     boolean isCDRUser=(Boolean) getFieldValue(cdrUserField, cRoot);
				
	    if(value!=null && defaultValue!=null && !(value.equals(defaultValue)) && isCDRUser && (  value.length()!=11 || value.charAt(4)!='-' || value.charAt(9)!='-')){			
			 addFieldError(fieldName, object);
		}else if(!isCDRUser && value == null){
			addFieldError(fieldName, object);
		}
	  }else if(value!=null && ( value.length()!=11 || value.charAt(4)!='-' || value.charAt(9)!='-')){
		    addFieldError(fieldName, object);
	  }/*else{//if value==null
		  addFieldError(fieldName, object);
	  }*/
	}

}
