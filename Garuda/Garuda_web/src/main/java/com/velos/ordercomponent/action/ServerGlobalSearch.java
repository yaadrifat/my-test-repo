package com.velos.ordercomponent.action;

import com.velos.ordercomponent.business.util.Utilities;

public class ServerGlobalSearch extends VelosBaseAction {

	
	
	public String FetchGlobalSearchOrders(String inputTxt){
		
		String condition="";
		String columnNames[]={"lower(F_CODELST_DESC(ORD.ORDER_STATUS))","crd.cord_registry_id","lower(usr.usr_logname)","lower(to_char(ordhdr.order_open_date,'Mon DD, YYYY'))","lower(F_CODELST_DESC(ORD.ORDER_TYPE))"};
		inputTxt=Utilities.removeDelimeters(inputTxt.toLowerCase(), "-");
		String text1="";
		text1=Utilities.removeDelimeters(inputTxt, " ");
		
		
		for(int i=0;i<columnNames.length;i++){
			
			if(i==0){
				 condition="  and ("+columnNames[i]+" LIKE '%"+inputTxt+"%'";
			
			}else if(i==1){
				 condition=condition+"  or replace(lower("+columnNames[i]+"),'-','') like lower('%"+ Utilities.removeDelimeters(inputTxt, "-") + "%')";
			
			}else if(i==4){
				
				if(text1.contains("ctship") || text1.contains("ct  ship sample")){
					condition=condition+"  or ( "+columnNames[i]+" ='ct' and ord.order_sample_at_lab='N' ) ";
				}else if(text1.contains("ctsamp") || text1.contains("ct  sample at lab")){
					condition=condition+"  or ( "+columnNames[i]+" ='ct'  and ord.order_sample_at_lab='Y' ) ";
				}else{
					 condition=condition+"  or "+columnNames[i]+" LIKE '%"+inputTxt+"%'";
				}
			}
			else{
				 condition=condition+"  or "+columnNames[i]+" LIKE '%"+inputTxt+"%'";
			}
			 
	
		}
		condition=condition+")";
		
		//System.out.println("\n\n\n\n\n\n\n\n\n\nGlobal Search:::::::::::"+condition);
		
		
		return condition;
	}
	public String FetchGlobalSearchWorflow(String inputTxt){
		
		String condition="";
		String columnNames[]={"lower(f_codelst_desc(ORD.ORDER_TYPE))","crd.cord_registry_id","lower(to_char(ordhdr.order_open_date,'Mon DD, YYYY'))","lower(f_codelst_desc(ORD.ORDER_STATUS))"};
		inputTxt=Utilities.removeDelimeters(inputTxt.toLowerCase(), "-");
		String text1="";
		text1=Utilities.removeDelimeters(inputTxt, " ");
		
		for(int i=0;i<columnNames.length;i++){
			
			if(i==0){
				 //condition="  and ("+columnNames[i]+" LIKE '%"+inputTxt+"%'";
						if(text1.contains("ctship") || text1.contains("ct  ship sample")){
							condition="  and (( "+columnNames[i]+" ='ct' and ord.order_sample_at_lab='N' ) ";
						}else if(text1.contains("ctsamp") || text1.contains("ct  sample at lab")){
							condition="  and (( "+columnNames[i]+" ='ct'  and ord.order_sample_at_lab='Y' ) ";
						}else{
							 condition="  and ( "+columnNames[i]+" LIKE '%"+inputTxt+"%'";
						}
				
			
			}else if(i==1){
				 condition=condition+"  or replace(lower("+columnNames[i]+"),'-','') like lower('%"+ Utilities.removeDelimeters(inputTxt, "-") + "%')";
			
			}else{
				 condition=condition+"  or "+columnNames[i]+" LIKE '%"+inputTxt+"%'";
			}
			 
	
		}
		condition=condition+")";
		
		//System.out.println("\n\n\n\n\n\n\n\n\n\nGlobal Search:::::::::::"+condition);
		
		
		return condition;
	}
	public String FetchGlobalSearchAlerts(String inputTxt){
		
		String condition="";
		String columnNames[]={"lower(f_codelst_desc(ORD.ORDER_TYPE))","crd.cord_registry_id","lower(to_char(ordhdr.order_open_date,'Mon DD, YYYY'))","lower(f_codelst_desc(ORD.ORDER_STATUS))"};
		inputTxt=Utilities.removeDelimeters(inputTxt.toLowerCase(), "-");
		String text1="";
		text1=Utilities.removeDelimeters(inputTxt, " ");
		
		for(int i=0;i<columnNames.length;i++){
			
			if(i==0){
				
				if(text1.contains("ctship")  || text1.contains("ct  ship sample")){
					condition="  and(( "+columnNames[i]+" ='ct' and ord.order_sample_at_lab='N' ) ";
				}else if(text1.contains("ctsamp") || text1.contains("ct  sample at lab")){
					condition="  and (( "+columnNames[i]+" ='ct'  and ord.order_sample_at_lab='Y' ) ";
				}else{
					 condition="  and ("+columnNames[i]+" LIKE '%"+inputTxt+"%'";
				}
			
			}else if(i==1){
				 condition=condition+"  or replace(lower("+columnNames[i]+"),'-','') like lower('%"+ Utilities.removeDelimeters(inputTxt, "-") + "%')";
			
			}else{
				 condition=condition+"  or "+columnNames[i]+" LIKE '%"+inputTxt+"%'";
			}
			 
	
		}
		condition=condition+")";
		
		//System.out.println("\n\n\n\n\n\n\n\n\n\nGlobal Search alerts:::::::::::"+condition);
		
		
		return condition;
	}
    public String FetchGlobalSearchQBuilder(String inputTxt){
		
		String condition="";
		String columnNames[]={"c.SITE_ID","lower(c.REGISTRY_ID)","lower(c.cord_local_cbu_id)","lower(c.cord_id_number_on_cbu_bag)","lower(c.cord_isbi_din_code)","lower(c.PATIENT_ID)","lower(c.CURRENT_DIAGNOSIS)","lower(c.TRANS_SITE_NAME)","lower(c.TRANS_SITE_ID)" ,"lower(eres.f_codelst_desc(c.ORDER_TYPE))","lower(to_char(c.ORDER_OPEN_DATE,'Mon DD, YYYY'))","lower(to_char(c.ORDER_OPEN_DATE,'Mon DD, YYYY'))","lower(to_char(c.INFUSION_DATE,'Mon DD, YYYY'))"};
		
		
		inputTxt=Utilities.removeDelimeters(inputTxt.toLowerCase(), "-");
		condition="  and (";
		
		for(int i=0;i<columnNames.length;i++){
			
			if(i==0){
				condition=condition+"  replace("+columnNames[i]+",'-','') like lower('%"+ inputTxt+ "%')";
				
			}else if(i==1 || i==2 || i==3 || i==5 || i==6 || i==7){
				 condition=condition+"  or replace("+columnNames[i]+",'-','') like lower('%"+ inputTxt+ "%')";
			
			}else if(i==9){
				 //condition="  and ("+columnNames[i]+" LIKE '%"+inputTxt+"%'";
						if(inputTxt.contains("ctship")  || inputTxt.contains("ct  ship sample")){
							condition=condition+"or ( "+columnNames[i]+" ='ct' and c.order_sample_at_lab='N' ) ";
						}else if(inputTxt.contains("ctsamp") || inputTxt.contains("ct  sample at lab")){
							condition=condition+"or  ( "+columnNames[i]+" ='ct'  and c.order_sample_at_lab='Y' ) ";
						}else{
							 condition=condition+"or "+columnNames[i]+" LIKE '%"+inputTxt+"%'";
						}
				
			
			}else{
				 condition=condition+"  or "+columnNames[i]+" LIKE '%"+inputTxt+"%'";
			}
			 
	
		}
		condition=condition+")";
		
		//System.out.println("\n\n\n\n\n\n\n\n\n\nGlobal Search for Query Builder:::::::::::"+condition);
		
		
		return condition;
	}
    public String FetchGlobalSearchQBuilder_CBUID(String inputTxt){
    	String condition="";
    	String columnNames[]={"c.SITE_ID","lower(c.REGISTRY_ID)","lower(C.registry_maternal_id)","lower(c.cord_local_cbu_id)","lower(C.maternal_local_id)","lower(c.cord_isbi_din_code)","lower(c.cord_id_number_on_cbu_bag)","lower(C.CORD_HISTORIC_CBU_ID)"/*,"c.ORDER_TYPE","lower(to_char(c.SHIP_DATE, 'Mon DD, YYYY'))","lower(to_char(c.ORDER_OPEN_DATE,'Mon DD, YYYY'))"*/};
		inputTxt=Utilities.removeDelimeters(inputTxt.toLowerCase(), "-");
		
		
		for(int i=0;i<columnNames.length;i++){
			
			if(i==0){
				 condition="  and ("+columnNames[i]+" LIKE '%"+inputTxt+"%'";
			
			}else if(i==8){
				
				if(inputTxt.contains("ctship")){
					condition=condition+"  or ( lower(eres.f_codelst_desc("+columnNames[i]+")) ='ct' and c.order_sample_at_lab='N' ) ";
				}else if(inputTxt.contains("ctsamp")){
					condition=condition+"  or ( lower(eres.f_codelst_desc("+columnNames[i]+")) ='ct'  and c.order_sample_at_lab='Y' ) ";
				}else{
					 condition=condition+"  or lower(eres.f_codelst_desc("+columnNames[i]+")) LIKE '%"+inputTxt+"%'";
				}
			
			}else if(i==1 || i==2 || i==3 || i==4 || i==5 || i==6 || i==7){
				 condition=condition+"  or replace("+columnNames[i]+",'-','') like lower('%"+ inputTxt+ "%')";
			
			}else{
				 condition=condition+"  or "+columnNames[i]+" LIKE '%"+inputTxt+"%'";
			}
			 
	
		}
		condition=condition+")";
		
		//System.out.println("\n\n\n\n\n\n\n\n\n\nGlobal PS Orders:::::::::::"+condition);
		
		
		return condition;
    }
    
    public String FetchGlobalSearchQBuilder_LICELig(String inputTxt){
    	String condition="";
    	String columnNames[]={"c.SITE_ID",
    			              "lower(c.REGISTRY_ID)",
    			              "lower(c.cord_local_cbu_id)",
    			              "lower(c.cord_id_number_on_cbu_bag)",
    			              "c.ORDER_TYPE",
    			              "lower(to_char(C.SPEC_COLLECTION_DATE,'Mon DD, YYYY'))",
    			              "lower(to_char(C.CORD_REGISTRATION_DATE,'Mon DD, YYYY'))",
    			              "lower(ERES.F_GET_CBU_STATUS(C.FK_CORD_CBU_STATUS))",
    			              "lower(ERES.f_codelst_desc(C.FK_CORD_CBU_LIC_STATUS))",
    			              "c.UNLICENSED_DESC",
    			              "lower(ERES.f_codelst_desc(C.fk_cord_cbu_eligible_status))",
    			              "c.INELIG_INCOMP_DESC",
    			              "lower(to_char(C.CORD_BABY_BIRTH_DATE,'Mon DD, YYYY'))",
    			              "c.RACE_DESC",
    			              "lower(C.funded_cbu)",
    			              "lower(C.NMDP_RACE_ID)",
    			              "lower(ERES.f_codelst_desc(C.FK_CORD_BACT_CUL_RESULT))",
    			              "lower(ERES.f_codelst_desc(C.FK_CORD_FUNGAL_CUL_RESULT))",
    			              "lower(ERES.f_codelst_desc(C.HEMOGLOBIN_SCRN))",
    			              "lower(PROC.PROC_NAME)",
    			              "Nucl_cell_count.resultdata",
    			              "CD34_cell_count.resultdata",
    			              "ERES.f_codelst_desc(Viability_Method.fkTestMethod)",
    			              "Final_Prod_Vol.resultdata",
    			              "Viability_Method.resultdata",
    			              "lower(to_char(C.SHIP_DATE,'Mon DD, YYYY'))",
    			              "lower(to_char(C.ORDER_OPEN_DATE,'Mon DD, YYYY'))",
    			              "lower(ERES.F_GET_CBU_STATUS(C.CORD_NMDP_STATUS))",
    			              "lower(ERES.f_codelst_desc(C.FUNDING_CATEGORY))",
							  "Nucl_cell_count_unknown.resultdata"};

		inputTxt=Utilities.removeDelimeters(inputTxt.toLowerCase(), "-");
		String text1="";
		text1=Utilities.removeDelimeters(inputTxt, " ");
		
		
		for(int i=0;i<columnNames.length;i++){
			
			if(i==0){
				 condition="  and ("+columnNames[i]+" LIKE '%"+inputTxt+"%'";
			
			}else if(i==1 || i==2 || i==3){
				 condition=condition+"  or replace("+columnNames[i]+",'-','') like lower('%"+ inputTxt+ "%')";
			
			}else if(i==4){
				
				if(text1.contains("ctship") || text1.contains("ct  ship sample")){
					condition=condition+"  or ( lower(eres.f_codelst_desc("+columnNames[i]+")) ='ct' and c.order_sample_at_lab='N' ) ";
				}else if(text1.contains("ctsamp") || text1.contains("ct  sample at lab")){
					condition=condition+"  or ( lower(eres.f_codelst_desc("+columnNames[i]+")) ='ct'  and c.order_sample_at_lab='Y' ) ";
				}else{
					 condition=condition+"  or lower(eres.f_codelst_desc("+columnNames[i]+")) LIKE '%"+inputTxt+"%'";
				}
			
			}else{
				 condition=condition+"  or "+columnNames[i]+" LIKE '%"+inputTxt+"%'";
			}
			 
	
		}
		condition=condition+")";
		
		//System.out.println("\n\n\n\n\n\n\n\n\n\nGlobal PS Orders:::::::::::"+condition);
		
		
		return condition;
    }
    
    public String FetchGlobalSearchPsOrders(String inputTxt){
		
		String condition="";
		String columnNames[]={"site.site_id","lower(ADR.ADD_CITY)","CORD.CORD_REGISTRY_ID","lower(CORD.CORD_LOCAL_CBU_ID)","lower(f_codelst_desc(ORD.ORDER_TYPE))","lower(f_codelst_desc(ORD.ORDER_PRIORITY))","lower(f_codelst_desc(ORD.ORDER_STATUS))","lower(ORD.ORDER_ACK_FLAG)","lower(usr.usr_logname)","lower(cbustat1.cbu_status_desc)","lower(cbustat.cbu_status_desc)","lower(usr1.usr_logname)","lower(to_char(ordhdr.order_open_date,'Mon DD, YYYY'))"};
		inputTxt=Utilities.removeDelimeters(inputTxt.toLowerCase(), "-");
		String text1="";
		text1=Utilities.removeDelimeters(inputTxt, " ");
		
		
		for(int i=0;i<columnNames.length;i++){
			
			if(i==0){
				 condition="  and ("+columnNames[i]+" LIKE '%"+inputTxt+"%'";
			
			}else if(i==4){
				
				if(text1.contains("ctship")  || text1.contains("ct  ship sample")){
					condition=condition+"  or ( "+columnNames[i]+" ='ct' and ord.order_sample_at_lab='N' ) ";
				}else if(text1.contains("ctsamp") || text1.contains("ct  sample at lab")){
					condition=condition+"  or ( "+columnNames[i]+" ='ct'  and ord.order_sample_at_lab='Y' ) ";
				}else{
					 condition=condition+"  or "+columnNames[i]+" LIKE '%"+inputTxt+"%'";
				}
			
			}else if(i==2){
				 condition=condition+"  or replace(lower("+columnNames[i]+"),'-','') like lower('%"+ Utilities.removeDelimeters(inputTxt, "-") + "%')";
			
			}else{
				 condition=condition+"  or "+columnNames[i]+" LIKE '%"+inputTxt+"%'";
			}
			 
	
		}
		condition=condition+")";
		
		//System.out.println("\n\n\n\n\n\n\n\n\n\nGlobal PS Orders:::::::::::"+condition);
		
		
		return condition;
	}
    public String FetchGlobalSearchSavedInProgress(String inputTxt){
		
		String condition="";
		String columnNames[]={"LOWER(TO_CHAR(C.created_on,'Mon DD, YYYY'))","C.CORD_REGISTRY_ID","C.CORD_ENTRY_PROGRESS"};
		inputTxt=Utilities.removeDelimeters(inputTxt.toLowerCase(), "-");
		
		for(int i=0;i<columnNames.length;i++){
			if(i == 0){
				//	System.out.println("i=="+i+"condition:::"+condition);
					condition = condition+" and ("+columnNames[i]+" LIKE lower('%"+inputTxt+"%')";
			}else if(i == 1){
			//	System.out.println("i=="+i+"condition:::"+condition);
				condition = condition+" or replace(lower("+columnNames[i]+"),'-','') like lower('%"+ Utilities.removeDelimeters(inputTxt, "-") + "%')";
			}else{
			//	System.out.println("i=="+i+"condition:::"+condition);
				condition = condition+"  or "+columnNames[i]+" LIKE lower('%"+inputTxt+"%')";
			}
		}
		condition = condition +")";
		
	//System.out.println("\n\n\n\n\n\n\n\n\n\nGlobal Search alerts:::::::::::"+condition);
		
		
		return condition;
	}
    public String FetchGlobalSearchLookup(String inputTxt){
		
		String condition="";
		String columnNames[]={"lower(C.CORD_REGISTRY_ID)","lower(C.registry_maternal_id)","lower(C.CORD_LOCAL_CBU_ID)","lower(C.maternal_local_id)","lower(C.CORD_ISBI_DIN_CODE)","lower(site.SITE_NAME)","lower(pat.PATIENTID)"};
		inputTxt=Utilities.removeDelimeters(inputTxt.toLowerCase(), "-");
		
		condition=" and (";
		for(int i=0;i<columnNames.length;i++){
			if(i == 0){
				//	System.out.println("i=="+i+"condition:::"+condition);
					condition =condition+" replace("+columnNames[i]+",'-','') like '%"+ inputTxt+ "%'";
			}else if(i == 1){
			//	System.out.println("i=="+i+"condition:::"+condition);
				condition = condition+" or replace("+columnNames[i]+",'-','') like '%"+ inputTxt + "%'";
			}else{
			//	System.out.println("i=="+i+"condition:::"+condition);
				condition = condition+"  or "+columnNames[i]+" LIKE '%"+inputTxt+"%'";
			}
		}
		condition = condition +")";
		
	   //System.out.println("\n\n\n\n\n\n\n\n\n\nGlobal Search lookup:::::::::::"+condition);
		
		
		return condition;
	}
    public String FetchGlobalSearchUploadDoc(String inputTxt){
		
		String condition="";
		String columnNames[]={"lower(C.CORD_REGISTRY_ID)","lower(C.registry_maternal_id)","lower(C.CORD_LOCAL_CBU_ID)","lower(C.maternal_local_id)","lower(C.CORD_ISBI_DIN_CODE)","lower(C.CORD_ID_NUMBER_ON_CBU_BAG)"};
		inputTxt=Utilities.removeDelimeters(inputTxt.toLowerCase(), "-");
		
		condition=" and (";
		for(int i=0;i<columnNames.length;i++){
			if(i == 0){
				//	System.out.println("i=="+i+"condition:::"+condition);
					condition =condition+" replace("+columnNames[i]+",'-','') like '%"+ inputTxt+ "%'";
			}else if(i == 1){
			//	System.out.println("i=="+i+"condition:::"+condition);
				condition = condition+" or replace("+columnNames[i]+",'-','') like '%"+ inputTxt + "%'";
			}else{
			//	System.out.println("i=="+i+"condition:::"+condition);
				condition = condition+"  or "+columnNames[i]+" LIKE '%"+inputTxt+"%'";
			}
		}
		condition = condition +")";
		
	   //System.out.println("\n\n\n\n\n\n\n\n\n\nGlobal Search lookup:::::::::::"+condition);
		
		
		return condition;
	}
    public String FetchGlobalSearchNotAvailCbu(String inputTxt){
		
		String condition="";
		String columnNames[]={"C.CORD_REGISTRY_ID","lower(CS.CBU_STATUS_DESC)","LOWER(TO_CHAR(C.CORD_CREATION_DATE,'Mon DD, YYYY'))"};
		inputTxt=Utilities.removeDelimeters(inputTxt.toLowerCase(), "-");
		
		for(int i=0;i<columnNames.length;i++){
			if(i == 0){
			//	System.out.println("i=="+i+"condition:::"+condition);
				condition = condition+"  and (replace(lower("+columnNames[i]+"),'-','') like lower('%"+ Utilities.removeDelimeters(inputTxt, "-") + "%')";
			}else if(i == 1){
			//	System.out.println("i=="+i+"condition:::"+condition);
				condition = condition+"  or "+columnNames[i]+" LIKE lower('%"+inputTxt+"%')";
			}else{
			//	System.out.println("i=="+i+"condition:::"+condition);
				condition = condition+"  or "+columnNames[i]+" LIKE lower('%"+inputTxt+"%')";
			}
		}
		condition = condition +")";
		
	//System.out.println("\n\n\n\n\n\n\n\n\n\nGlobal Search alerts:::::::::::"+condition);
		
		
		return condition;
	}
    public String FetchGlobalSearchPSNotAvailCbu(String inputTxt){
		
		String condition="";
		String columnNames[]={"site.site_id","cord.cord_registry_id","lower(cord.cord_local_cbu_id)","lower(nmdpstat.cbu_status_desc)","lower(loclstat.cbu_status_desc)","lower(usr.usr_logname)"};
		inputTxt=Utilities.removeDelimeters(inputTxt.toLowerCase(), "-");
		
		for(int i=0;i<columnNames.length;i++){
			if(i == 0){
				condition = condition+"  and ("+columnNames[i]+" LIKE '%"+inputTxt+"%'";
			}
			else if(i == 1){
				condition = condition+"  or replace(lower("+columnNames[i]+"),'-','') like lower('%"+ Utilities.removeDelimeters(inputTxt, "-") + "%')";
			}else{
				condition = condition+"  or "+columnNames[i]+" LIKE '%"+inputTxt+"%'";
			}
		}
		condition = condition +")";
		
        //System.out.println("\n\n\n\n\n\n\n\n\n\nGlobal Search Not Avail CBU PS:::::::::::"+condition);
		
		
		return condition;
	}
    
   public String FetchGlobalSearchPSSavInProg(String inputTxt){
		
		String condition="";
		
		String columnNames[]={"site.site_id","cord.cord_registry_id","lower(cord.cord_local_cbu_id)","cord.cord_entry_progress","ceil(sysdate-cord.created_on)","lower(to_char(cord.created_on,'Mon DD, YYYY'))"};
		inputTxt=Utilities.removeDelimeters(inputTxt.toLowerCase(), "-");
		
		for(int i=0;i<columnNames.length;i++){
			if(i == 0){
				condition = condition+"  and ("+columnNames[i]+" LIKE '%"+inputTxt+"%'";
			}
			else if(i == 1){
				condition = condition+"  or replace(lower("+columnNames[i]+"),'-','') like lower('%"+ Utilities.removeDelimeters(inputTxt, "-") + "%')";
			}else{
				condition = condition+"  or "+columnNames[i]+" LIKE '%"+inputTxt+"%'";
			}
		}
		condition = condition +")";
		
        //System.out.println("\n\n\n\n\n\n\n\n\n\nGlobal Search Sip PS:::::::::::"+condition);
		
		
		return condition;
	}
   public String FetchGlobalSearchHistory(String inputTxt){
		
		String condition="";
		inputTxt=inputTxt.toLowerCase();
		
		String columnNames[]={"lower(HIST.DATEVAL)","lower(HIST.DESC1)","lower(HIST.DESC2)","lower(HIST.DESC3)","lower(HIST.USERNAME)","lower(HIST.ORDERTYPE)","lower(HIST.DESC1||HIST.DESC2||HIST.DESC3)"};
		inputTxt=Utilities.removeDelimeters(inputTxt.toLowerCase(), "-");
		String text1="";
		text1=Utilities.removeDelimeters(inputTxt, " ");
		
		 condition =" Where ( ";
		
		for(int i=0;i<columnNames.length;i++){
			
			    if(i==0){
			    	condition =condition + columnNames[i]+" LIKE '%"+inputTxt+"%'";
			    }else{
			    	
			    	if(i==5){
			    		inputTxt=Utilities.removeDelimeters(inputTxt, "-");
			    		
			    		if(text1.contains("ctship") || text1.contains("ct  ship sample")){
							condition=condition+"  or ( "+columnNames[i]+" ='ct - ship sample') ";
						}else if(text1.contains("ctsamp") || text1.contains("ct  sample at lab")){
							condition=condition+"  or ( "+columnNames[i]+" ='ct - sample at lab') ";
						}else{
							 condition=condition+"  or "+columnNames[i]+" LIKE '%"+inputTxt+"%'";
						}
			    	}else{
			    		     condition = condition+"  or "+columnNames[i]+" LIKE '%"+inputTxt+"%'";
			    	}
			    	
			    }
		}
		condition = condition +" ) ";
		
        //System.out.println("\n\n\n\n\n\n\n\n\n\nGlobal Search HistoryTable:::::::::::"+condition);
		
		
		return condition;
	}
    public String FetchGlobalSearchCordImport(String inputTxt){
		
		String condition="";
		
		String columnNames[]={"h.PK_CORD_IMPORT_HISTORY","lower(to_char(h.START_TIME,'Mon DD, YYYY HH:MM:SS AM'))", "lower(to_char(h.END_TIME,'Mon DD, YYYY HH:MM:SS AM'))","lower(h.IMPORT_FILE_NAME)","lower(u.USR_FIRSTNAME)","lower(u.USR_LASTNAME)","lower(h.IMPORT_HISTORY_DESC)"};
		inputTxt=Utilities.removeDelimeters(inputTxt.toLowerCase(), "-");
		
		 condition ="and ( ";
		
		for(int i=0;i<columnNames.length;i++){
			if(i == 0){
				condition=condition+" "+columnNames[i]+" LIKE '%"+inputTxt+"%'";
			}
			if(i != 0){
				condition=condition+"  or "+columnNames[i]+" LIKE '%"+inputTxt+"%'";
			}
		}
		condition = condition +" )";
		
        //System.out.println("\n\n\n\n\n\n\n\n\n\nGlobal Search CordImport:::::::::::"+condition);
		return condition;
	}
    public String FetchGlobalSearchProcedure(String inputTxt){
		
		String condition="";
		
		String columnNames[]={"lower(procName)","lower(to_char(procStartDate,'Mon DD, YYYY'))","lower(to_char(procTermiDate,'Mon DD, YYYY'))"};
		inputTxt=Utilities.removeDelimeters(inputTxt.toLowerCase(), "-");
		
		 condition ="and ( ";
		
		for(int i=0;i<columnNames.length;i++){
			if(i == 0){
				condition=condition+" "+columnNames[i]+" LIKE '%"+inputTxt+"%'";
			}
			else{
				condition=condition+"  or "+columnNames[i]+" LIKE '%"+inputTxt+"%'";
			}
		}
		condition = condition +" )";
		
        //System.out.println("\n\n\n\n\n\n\n\n\n\nGlobal Search Procedures:::::::::::"+condition);
		return condition;
	}
    public String FetchGlobalSearchCBBProfile(String inputTxt){
		
		String condition="";
		
		String columnNames[]={"lower(siteIdentifier)","lower(siteName)"};
		inputTxt=inputTxt.toLowerCase();
		
		 condition =" and ( ";
		
		for(int i=0;i<columnNames.length;i++){
			if(i == 0){
				condition = condition+"  "+columnNames[i]+" LIKE '%"+inputTxt+"%'";
			}else{
				condition=condition+"  or "+columnNames[i]+" LIKE '%"+inputTxt+"%'";
			}
			
		}
		condition = condition +" )";
		
        //System.out.println("\n\n\n\n\n\n\n\n\n\nGlobal Search CBBProfile:::::::::::"+condition);
		return condition;
	}
    
    public String fetchGlobalSearch_docInfo(String inputTxt) {
		String condition = "";
		String columnNames[] = { "category","subcategory", "user_id", "tstamp",
				"action"};
		inputTxt = Utilities.removeDelimeters(inputTxt.toLowerCase(), "-");
		//condition = " where ";
		for (int i = 0; i < columnNames.length; i++) {
			if (i == 0) {
				condition = condition + " ( replace(lower(" + columnNames[i]
						+ "),'-','') like lower('%"
						+ Utilities.removeDelimeters(inputTxt, "-") + "%')";
			} else {
				condition = condition + "  or " + "replace(lower("
						+ columnNames[i] + "),'-','') LIKE lower('%"
						+ Utilities.removeDelimeters(inputTxt, "-") + "%')  ";
			}
		}
		condition = condition + " ) ";
		return condition;
	}
    
    public String fetchGlobalSearch_cbuInfo(String inputTxt) {
		String condition = "";
		String columnNames[] = { "column_display_name", "user_id", "tstamp",
				"action", "old_value", "new_value" };
		inputTxt = Utilities.removeDelimeters(inputTxt.toLowerCase(), "-");
		//condition = " where ";
		for (int i = 0; i < columnNames.length; i++) {
			if (i == 0) {
				condition = condition + " ( replace(lower(" + columnNames[i]
						+ "),'-','') like lower('%"
						+ Utilities.removeDelimeters(inputTxt, "-") + "%')";
			} else {
				condition = condition + "  or " + "replace(lower("
						+ columnNames[i] + "),'-','') LIKE lower('%"
						+ Utilities.removeDelimeters(inputTxt, "-") + "%')  ";
			}
		}
		condition = condition + " ) ";
		return condition;
	}
	
	public String fetchGlobalSearch_formInfo(String inputTxt) {
		String condition = "";
		String columnNames[] = { "formname", "fversion", "user_id",
				"created_on"};
		inputTxt = Utilities.removeDelimeters(inputTxt.toLowerCase(), "-");
		//condition = " where ";
		for (int i = 0; i < columnNames.length; i++) {
			if (i == 0) {
				condition = condition + " ( replace(lower(" + columnNames[i]
						+ "),'-','') like lower('%"
						+ Utilities.removeDelimeters(inputTxt, "-") + "%')";
			} else {
				condition = condition + "  or " + "replace(lower("
						+ columnNames[i] + "),'-','') LIKE lower('%"
						+ Utilities.removeDelimeters(inputTxt, "-") + "%')  ";
			}
		}
		condition = condition + " ) ";
		return condition;
	}
	

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
}
