package com.velos.ordercomponent.action;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;

import com.velos.ordercomponent.business.pojoobjects.CordFilterPojo;
import com.velos.ordercomponent.business.pojoobjects.HLAFilterPojo;
import com.velos.ordercomponent.util.VelosGarudaConstants;
import com.opensymphony.xwork2.ActionSupport;

public class IDSummaryExcel extends ActionSupport {

	private Boolean isMultipleCBBSelected = false;
	
	public Workbook idSummaryExcel(List<Object> cdrObjects, Workbook wb, Map<Long,Long> duplCords){		 
		
		XSSFCellStyle headStyle = headStyle(wb);
		XSSFCellStyle hightLightStyle = redStyle(wb);
		 Row row;
		 Cell cell;
		 int rowNum = 1;
		 short col = 0;	
		 int sheetNo = 1;	
		 Sheet sheet = sheet(wb, getText("garuda.querybuilder.report.cbuids"));
		 createHeader(sheet, headStyle);		 
		
		 
		 for(Object cdrObj : cdrObjects){			
			
			 if(cdrObj!=null){					
	       	        Object[] objArr = (Object[])cdrObj;
		
			 if(rowNum  > VelosGarudaConstants.MAXROW_XLSX ){
				 
				 sheet = sheet(wb,getText("garuda.querybuilder.report.cbuids")+" "+(++sheetNo));
				 createHeader(sheet, headStyle);
				 rowNum = 1;
			 }
			 
			  row = sheet.createRow(rowNum++);
			  col = 0;
			 
				/*  if(isMultipleCBBSelected){
					  row.createCell(col++).setCellValue(cdrObj.site); 
				  }
				  row.createCell(col++).setCellValue(cdrObj.registry);
				  row.createCell(col++).setCellValue(cdrObj.localCbu);
				  row.createCell(col++).setCellValue(cdrObj.numberOnBag);
				  row.createCell(col++).setCellValue(cdrObj.isbtCode);
				  row.createCell(col++).setCellValue(cdrObj.historic);
				  row.createCell(col++).setCellValue(cdrObj.maternal);
				  row.createCell(col++).setCellValue(cdrObj.localMat);*/
				  
				  if(isMultipleCBBSelected){
					  row.createCell(col++).setCellValue(objArr[1]!=null?objArr[1].toString():""); 
				  }
				  cell = row.createCell(col++);cell.setCellValue(objArr[2]!=null?objArr[2].toString():"");cell.setCellStyle((duplCords !=null && duplCords.get(Long.valueOf(objArr[0].toString())) !=null)?hightLightStyle:null);
				  row.createCell(col++).setCellValue(objArr[3]!=null?objArr[3].toString():"");
				  row.createCell(col++).setCellValue(objArr[4]!=null?objArr[4].toString():"");
				  row.createCell(col++).setCellValue(objArr[5]!=null?objArr[5].toString():"");
				  row.createCell(col++).setCellValue(objArr[6]!=null?objArr[6].toString():"");
				  row.createCell(col++).setCellValue(objArr[7]!=null?objArr[7].toString():"");
				  row.createCell(col++).setCellValue(objArr[8]!=null?objArr[8].toString():"");
			 
			 }
		 }
		 
		 return wb;
	}
	
	private Sheet sheet(Workbook wb, String sheetName){
		
		 Sheet sheet = wb.createSheet(sheetName); 
		 
		 return sheet;
	}
	
	private int createHeader(Sheet sheet, XSSFCellStyle blackStyle){
		
		 int r = 0, c = 0;
		 Cell cell;
		 Row row;
		 row = sheet.createRow(r);    
		 row.setHeight((short)500);  
		 
		 if(isMultipleCBBSelected){
			 cell=row.createCell(c++);cell.setCellValue(getText("garuda.queryBuilder.label.cbbid"));cell.setCellStyle(blackStyle);
		 }
		cell=row.createCell(c++);cell.setCellValue(getText("garuda.cbuentry.label.registrycbuid"));cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue(getText("garuda.cbuentry.label.localcbuid"));cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue(getText("garuda.cdrcbuview.label.id_on_bag"));cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue(getText("garuda.shippedcbu.report.label.isbt"));cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue(getText("garuda.querybuilderreport.label.historiclocalid"));cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue(getText("garuda.cbuentry.label.maternalregistryid"));cell.setCellStyle(blackStyle);
		cell=row.createCell(c++);cell.setCellValue(getText("garuda.cordentry.label.localmaternalid"));cell.setCellStyle(blackStyle);
		
		
		return c;
	}
	
	private XSSFCellStyle headStyle(Workbook wb){
		
		XSSFCellStyle style = (XSSFCellStyle) wb.createCellStyle();

	    style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
	    style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    style.setWrapText(false);
	    
	    Font font = wb.createFont();
	    font.setColor(IndexedColors.WHITE.getIndex());
	    font.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    style.setFont(font);
	   // Row.setHeight((short)500);
	    
	    return style;
	}
	
	private XSSFCellStyle redStyle(Workbook wb){
		
		XSSFCellStyle style = (XSSFCellStyle) wb.createCellStyle();   
	    Font font = wb.createFont();  
	    style.setFillForegroundColor(IndexedColors.RED.getIndex());
	    style.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    style.setWrapText(false);
	    style.setFont(font);
	    
	    return style;
	}
	
	public void setIsMultipleCBBSelected(Boolean isMultipleCBBSelected) {
		this.isMultipleCBBSelected = isMultipleCBBSelected;
	}
}
