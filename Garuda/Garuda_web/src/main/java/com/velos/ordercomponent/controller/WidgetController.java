package com.velos.ordercomponent.controller;

import java.util.List;

import com.velos.ordercomponent.business.domain.Page;
import com.velos.ordercomponent.business.domain.WidgetInstance;
import com.velos.ordercomponent.service.VelosService;
import com.velos.ordercomponent.service.impl.VelosServiceImpl;

public class WidgetController {
	VelosService service;
	
	public List<Page> getWidgetInfo() throws Exception{
		service = VelosServiceImpl.getService();
		return service.getWidgetInfo();
	}
}
