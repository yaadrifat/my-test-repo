/**
 * 
 */
package com.velos.ordercomponent.controller;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.ordercomponent.business.domain.CBUStatus;
import com.velos.ordercomponent.business.domain.CodeList;
import com.velos.ordercomponent.service.VelosService;
import com.velos.ordercomponent.service.impl.VelosServiceImpl;



/**
 * @author akajeera
 *
 */
public class CodelstController {
	public static final Log log = LogFactory.getLog(CodelstController.class);
	VelosService service;
	
	public List<CodeList> getCodelst(String parentcode) throws Exception {
		//log.debug( " contact manager");
		service = VelosServiceImpl.getService();
 
		return service.getCodelst(parentcode);
	}
	
	public Map getCodelistValues(Map codelstParams) throws Exception{
         //log.debug("CodeLIst Controller getCodelist valuse"+codelstParams);
		service = VelosServiceImpl.getService();
		//log.debug("CodeLIst Controller getCodelist service"+service);
		
		 //log.debug("inside getcode controller");
		return service.getCodelistValues(codelstParams);
	}
	
	public CodeList getCodeListByCode(String codetype,String codesubtype ) throws Exception{
		 //log.debug("CodeLIst Controller getMailConfigurationByCode");
			service = VelosServiceImpl.getService();
			return service.getCodeListByCode(codetype, codesubtype);
	}
	
	public Boolean getValidateDbData(String entityName, String entityFiled, String entityValue, String entityField1, String entityValue1) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getValidateDbData(entityName, entityFiled, entityValue , entityField1, entityValue1);
	}
	
	public Boolean getValidateDbLongData(String entityName, String entityFiled, String entityValue, String entityField1, Long entityValue1) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getValidateDbLongData(entityName, entityFiled, entityValue , entityField1, entityValue1);
	}
	
	public Boolean getValidateDbLongDataWithAdditionalField(String entityName, String entityFiled, String entityValue,
			String entityFiled2, String entityValue2, String entityField1, Long entityValue1) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getValidateDbLongDataWithAdditionalField(entityName, entityFiled,
				entityValue, entityFiled2, entityValue2, entityField1, entityValue1);
	}
	
	public List<String> getCodelstCustCols(Long pkCodelst){
		try{
		//log.debug("in controller");
		service=VelosServiceImpl.getService();
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
			//log.debug("codelstcontroller exception in getcodelstcustcols---------"+e);
		}
		return service.getCodelstCustCols(pkCodelst);
	}
	
	public List<CBUStatus> getCBUStatusValues(String codeSubType){
		try{
			//log.debug("in controller");
			service=VelosServiceImpl.getService();
			}catch(Exception e){
				log.error(e.getMessage());
				e.printStackTrace();
				//log.debug("codelstcontroller exception in getcodelstcustcols---------"+e);
			}
			return service.getCBUStatusValues(codeSubType);
	}
}
