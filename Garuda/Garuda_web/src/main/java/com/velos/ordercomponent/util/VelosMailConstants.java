package com.velos.ordercomponent.util;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class VelosMailConstants {

	//Constants For mail Config Code
	public static final String UNIT_REPORT_CODE = "UnitReport";
	public static final String DATA_ENTRY_CODE = "DataEntry";
	public static final String TRANSPLANT_CENTER_CODE = "TranCenter";
	public static final String ASSESSMENT_CODE = "Assessment";
	public static final String CBU_ASSESSMENT_CODE = "CBUAssess";
	public static final String CBB_ADDRESS_CHANGE_CODE = "CBBAddChg";
	public static final String CORD_IMPORT_NOTIFI_CODE = "IMPORTNOTI";	
	
	//Constants For mail Config replacement id used in text
	public static final String CBUID = "[CBUID]";
	public static final String CBBID = "[CBBID]";
	public static final String UNIT_REPORT_URL = "[URL]";
	public static final String UNIT_REPORT_URL_SENT = "/velos/jsp/nmdplogin.jsp";
	public static final String APPLICATION_URL = "garuda.application.url";
	public static final String VELOS_USER = "[USER]";
	public static final String CM = "[CM]";
	public static final String FORMNAME = "[FORMNAME]";
	public static final String CLINICAL_NOTES = "Clinical";
	public static final String SITEID = "[SITEID]";
	public static final String CBBNAME = "[CBBNAME]";
	public static final String CORD_IMPORT_JOB_ID = "[ID]";
	public static final String CORD_IMPORT_COMPLETION_DATE = "[DATE]";
	public static final String CORD_IMPORT_COMPLETION_TIME = "[COMPLETIONTIME]";
	public static final String CORD_IMPORT_START_TIME = "[STARTTIME]";
	public static final String CORD_IMPORT_STATUS = "[STATUS]";
} 