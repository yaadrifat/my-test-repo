package com.velos.ordercomponent.validator;

import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

/**
 * @author Mohiuddin Ali Ahmed
 * @author Anurag Upadhyay
 * @version 1.0
 * 
 */

public class StringLengthFieldValidator extends FieldValidatorSupport{
	  private int maxLength=-1;
	  private int minLength=-1;
	  private boolean doTrim=true;
	  
	  public int getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	public int getMinLength() {
		return minLength;
	}

	public void setMinLength(int minLength) {
		this.minLength = minLength;
	}

	public boolean isDoTrim() {
		return doTrim;
	}

	public void setDoTrim(boolean doTrim) {
		this.doTrim = doTrim;
	}

	public void validate(Object obj)throws ValidationException
	  {
		  String fieldName= getFieldName();
		  //String fieldMsg= getMessageKey();
		  //String [] fieldmsg= getMessageParameters();
		  String val= (String) getFieldValue(fieldName, obj);
		  if(doTrim){
			  val=val.trim();
		  }
		  if((minLength > -1) && (val.length() < minLength)){
			  addFieldError(fieldName, obj);
		  }
		  else if ((maxLength > -1) && (val.length() > maxLength)) {
			  addFieldError(fieldName, obj);
			
		}
		  
	  }
	

}
