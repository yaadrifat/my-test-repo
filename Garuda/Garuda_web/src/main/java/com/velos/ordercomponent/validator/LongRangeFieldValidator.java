package com.velos.ordercomponent.validator;

import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

/**
 * @author Mohiuddin Ali Ahmed
 * @author Anurag Upadhyay
 * @version 1.0
 * 
 */

public class LongRangeFieldValidator  extends FieldValidatorSupport{
	 private Long max = null;
	 private Long min = null;
	 private boolean searchableCheck;	
		
	public Long getMax() {
		return max;
	}
	public void setMax(Long max) {
		this.max = max;
	}
	public Long getMin() {
		return min;
	}
	public void setMin(Long min) {
		this.min = min;
	}
	public boolean isSearchableCheck() {
		return searchableCheck;
	}

	public void setSearchableCheck(boolean searchableCheck) {
		this.searchableCheck = searchableCheck;
	}
	@Override
	public void validate(Object object) throws ValidationException {
		// TODO Auto-generated method stub
	Long value = (Long) getFieldValue(getFieldName(), object);
		
	 if( value!=null && ( value < min || value > max )){
		addFieldError(getFieldName(), object);
		return;		
	}
	    
	}
}
