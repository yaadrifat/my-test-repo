package com.velos.ordercomponent.controller;

import java.util.List;

import com.velos.ordercomponent.business.pojoobjects.AddressPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.pojoobjects.UserAlertsPojo;
import com.velos.ordercomponent.business.pojoobjects.UserPojo;
import com.velos.ordercomponent.service.VelosService;
import com.velos.ordercomponent.service.impl.VelosServiceImpl;

public class UserAlertController {
VelosService service;
	
	public List<UserAlertsPojo> saveUserAlert(List<UserAlertsPojo> userAlertsLst) throws Exception {
		service = VelosServiceImpl.getService();
		return service.saveUserAlert(userAlertsLst);
	}
	
	public List getUserAlerts(Long userid,String alertType) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getUserAlerts(userid,alertType);
	}
	
	public UserPojo getUserById(UserPojo userPojo) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getUserById(userPojo);
	}
	
	public String getLogNameByUserId(Long userId) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getLogNameByUserId(userId);
	}
	
	public AddressPojo updateUserMailId(AddressPojo addressPojo)throws Exception{
		service = VelosServiceImpl.getService();
		return service.upadateuserMailId(addressPojo);
	}
	
	public List getusersById(String criteria,PaginateSearch paginateSearch, int i)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getusersById(criteria,paginateSearch,i);
	}
}
