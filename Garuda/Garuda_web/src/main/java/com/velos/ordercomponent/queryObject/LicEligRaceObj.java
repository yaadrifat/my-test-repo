package com.velos.ordercomponent.queryObject;

import java.util.List;
import java.util.concurrent.Callable;



import com.velos.ordercomponent.controller.VDAController;

public class LicEligRaceObj implements Runnable{
	
	private VDAController vdaController;
	private String sqlQuery;
	private List<Object> race;
	
	public LicEligRaceObj(String sqlQuery, VDAController vdaController) {
		
		this.vdaController = vdaController;
		this.sqlQuery =  sqlQuery;		
	}
	
	/*@Override
	public List<Object> call() throws Exception {
	
		return vdaController.getQueryBuilderListObject(sqlQuery);
	}*/

	@Override
	public void run() {
		
		try {
			race = vdaController.getQueryBuilderListObject(sqlQuery);
			
			System.out.println(" RACE Finished ----------------------- >");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	/**
	 * @return the race
	 */
	public List<Object> getRace() {
		return race;
	}

}
