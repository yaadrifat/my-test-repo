package com.velos.ordercomponent.validator;

import java.util.HashMap;
import java.util.List;


import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

import com.velos.ordercomponent.business.pojoobjects.PatLabsPojo;
import static com.velos.ordercomponent.action.CordEntryValidatorAction.getCodeListPkByTypeAndSubtype;
import static com.velos.ordercomponent.action.CordEntryValidatorAction.codeListMapByIds;

/**
 * @author Tarun Sondhi
 * @author Anurag Upadhyay
 * @version 1.0
 * 
 */

public class LabSummaryPostProcessingValidator extends FieldValidatorSupport{
	private boolean searchableCheck;	
	
	public boolean isSearchableCheck() {
		return searchableCheck;
	}

	public void setSearchableCheck(boolean searchableCheck) {
		this.searchableCheck = searchableCheck;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void validate(Object object) throws ValidationException {
		// TODO Auto-generated method stub
		
		String fieldName=getFieldName();
		 int i=0;
		 List<PatLabsPojo> savePostTestList=(List<PatLabsPojo>) getFieldValue(fieldName, object);
		 long cordSearchable = (Long) getFieldValue("cordSearchable", object);
		 HashMap<Long, String> processingTestMap=(HashMap<Long, String>) getFieldValue("processingTestMap", object);
			for(PatLabsPojo plp: savePostTestList){
			 if(plp!=null){
		     String testresult=plp.getTestresult()!=null?plp.getTestresult():"";
		     Long testSpecimanType=plp.getFktestspecimen()!=null?plp.getFktestspecimen():-1l;
		     Long testMethod=plp.getFktestmethod()!=null?plp.getFktestmethod():-1l;
		     String testMethodDesc=plp.getTestMthdDesc()!=null?plp.getTestMthdDesc():"";
		     switch(LabTests.valueOf(processingTestMap.get(plp.getFktestid())==null?"DEF":processingTestMap.get(plp.getFktestid()))){
		     case CBU_VOL:
	    		/*
	    		if(searchableCheck  && cordSearchable==1 && (testresult==null || testresult.equals(""))){
	    			 setMessageKey("garuda.cbu.cordentry.inputId");
	    			addFieldError(fieldName+"["+i+"].testresult", object);
	    		}else if(testresult!=null && (Float.parseFloat(testresult)<40 || Float.parseFloat(testresult)>500)){
	    			 setMessageKey("garuda.common.range.testval40to500");
	    			 addFieldError(fieldName+"["+i+"].testresult", object);
	    		}*/
	    		break;
	    	case TCNCC:	  
	    	case UNCRCT:
	    	case TCBUUN:
	    		if(searchableCheck  && cordSearchable==1 && (testresult==null || testresult.equals(""))){
	    			 setMessageKey("garuda.cbu.cordentry.inputId");
	    			addFieldError(fieldName+"["+i+"].testresult", object);
	    		}else if(testresult!=null && !testresult.equals("") && (Float.parseFloat(testresult)<12 || Float.parseFloat(testresult)>999)){
	    			 setMessageKey("garuda.common.range.testval12to999");
	    			 addFieldError(fieldName+"["+i+"].testresult", object);
	    		}
	    		break;
	    	case CBUNCCC:
	    		if(testresult!=null && !testresult.equals("") && (Float.parseFloat(testresult)<5000 || Float.parseFloat(testresult)>200000)){
	    			 setMessageKey("garuda.common.range.testval5000to200000");
	    			 addFieldError(fieldName+"["+i+"].testresult", object);
	    		}
	    		break;
	    	case TCDAD:
	    		if(searchableCheck  && cordSearchable==1 && (testresult==null || testresult.equals(""))){
	    			 setMessageKey("garuda.cbu.cordentry.inputId");
	    			addFieldError(fieldName+"["+i+"].testresult", object);
	    		}else if(testresult!=null && !testresult.equals("") && (Float.parseFloat(testresult)<0.0 || Float.parseFloat(testresult)>999.9)){
	    			 setMessageKey("garuda.common.range.val0to999");
	    			 addFieldError(fieldName+"["+i+"].testresult", object);
	    		}
	    		break;
	    	case PERNRBC:
	    		if(searchableCheck  && cordSearchable==1 && (testresult==null || testresult.equals(""))){
	    			 setMessageKey("garuda.cbu.cordentry.inputId");
	    			addFieldError(fieldName+"["+i+"].testresult", object);
	    		}else if(testresult!=null && !testresult.equals("") && (Float.parseFloat(testresult)<0 || Float.parseFloat(testresult)>100)){
	    			 setMessageKey("garuda.common.range.val0to100");
	    			 addFieldError(fieldName+"["+i+"].testresult", object);
	    		}
	    		break;
	    	case PERTNUC:
	    		if(testresult!=null && !testresult.equals("") && (Float.parseFloat(testresult)<20.0 || Float.parseFloat(testresult)>99.9)){
	    			 setMessageKey("garuda.common.range.val20to99");
	    			 addFieldError(fieldName+"["+i+"].testresult", object);
	    		}
	    		break;
	    	case PERTMON:
	    		if(testresult!=null && !testresult.equals("") && (Float.parseFloat(testresult)<0.0 || Float.parseFloat(testresult)>10.0)){
	    			 setMessageKey("garuda.common.range.testrange0to10");
	    			 addFieldError(fieldName+"["+i+"].testresult", object);
	    		}
	    		break;
	    	case CNRBC:
	    		if(searchableCheck  && cordSearchable==1 && (testresult==null || testresult.equals(""))){
	    			 setMessageKey("garuda.cbu.cordentry.inputId");
	    			addFieldError(fieldName+"["+i+"].testresult", object);
	    		}else if(testresult!=null && !testresult.equals("") && (Float.parseFloat(testresult)<0 || Float.parseFloat(testresult)>9999)){
	    			 setMessageKey("garuda.common.range.val0to9999");
	    			 addFieldError(fieldName+"["+i+"].testresult", object);
	    		}
	    		break;
	    	case PERCD3:
	    		if(testresult!=null && !testresult.equals("") && (Float.parseFloat(testresult)<0 || Float.parseFloat(testresult)>99.9)){
	    			 setMessageKey("garuda.common.range.val0to99");
	    			 addFieldError(fieldName+"["+i+"].testresult", object);
	    		}
	    		break;
	    	case CFUCNT:
	    		if(searchableCheck  && cordSearchable==1 && (testresult==null || testresult.equals(""))){
	    			 setMessageKey("garuda.cbu.cordentry.inputId");
	    			addFieldError(fieldName+"["+i+"].testresult", object);
	    		}else if(testresult!=null && !testresult.equals("") && (Float.parseFloat(testresult)<0 || Float.parseFloat(testresult)>999)){
	    			 setMessageKey("garuda.common.range.testrange0to999");
	    			 addFieldError(fieldName+"["+i+"].testresult", object);
	    		}
	    		if(searchableCheck  && cordSearchable==1 && testMethod==-1){
	    			 setMessageKey("garuda.cbu.cordentry.inputId");
		    			addFieldError(fieldName+"["+i+"].fktestmethod", object);
	    		}else if(searchableCheck  && cordSearchable==1 && testMethod==getCodeListPkByTypeAndSubtype(com.velos.ordercomponent.util.VelosGarudaConstants.CFU_COUNT,com.velos.ordercomponent.util.VelosGarudaConstants.CFU_OTHER,codeListMapByIds).longValue() && (testMethodDesc==null || testMethodDesc.equals(""))){
	    			setMessageKey("garuda.cbu.cordentry.inputId");
	    			addFieldError(fieldName+"["+i+"].testMthdDesc", object);
	    		}
	    		break;
	    	case TOTCD:
	    		if(testresult!=null && !testresult.equals("") && (Float.parseFloat(testresult)<0 || Float.parseFloat(testresult)>999)){
	    			 setMessageKey("garuda.common.range.val0to999");
	    			 addFieldError(fieldName+"["+i+"].testresult", object);
	    		}
	    		break;
	    	case FNPV:
	    		if(testresult!=null && !testresult.equals("") && (Float.parseFloat(testresult)<10 || Float.parseFloat(testresult)>600)){
	    			 setMessageKey("garuda.common.range.testval10to600");
	    			 addFieldError(fieldName+"["+i+"].testresult", object);
	    		}
	    		break;
	    	case VIAB:
	    		if(searchableCheck  && cordSearchable==1 && (testresult==null || testresult.equals(""))){
	    			 setMessageKey("garuda.cbu.cordentry.inputId");
	    			addFieldError(fieldName+"["+i+"].testresult", object);
	    		}else if(testresult!=null && !testresult.equals("") && (Float.parseFloat(testresult)<0 || Float.parseFloat(testresult)>100)){
	    			 setMessageKey("garuda.common.range.val0to100");
	    			 addFieldError(fieldName+"["+i+"].testresult", object);
	    		}
	    		if(searchableCheck  && cordSearchable==1 && testMethod==-1){
	    			 setMessageKey("garuda.cbu.cordentry.inputId");
		    			addFieldError(fieldName+"["+i+"].fktestmethod", object);
	    		}else if(searchableCheck  && cordSearchable==1 && testMethod==getCodeListPkByTypeAndSubtype(com.velos.ordercomponent.util.VelosGarudaConstants.VAIBILITY,com.velos.ordercomponent.util.VelosGarudaConstants.VAIBILITY_OTHER,codeListMapByIds).longValue() && (testMethodDesc==null || testMethodDesc.equals(""))){
	    			setMessageKey("garuda.cbu.cordentry.inputId");
	    			addFieldError(fieldName+"["+i+"].testMthdDesc", object);
	    		}
	    		break;
	    	case PERCD:
	    		if(testresult!=null && !testresult.equals("") && (Float.parseFloat(testresult)<0.0 || Float.parseFloat(testresult)>100)){
	    			 setMessageKey("garuda.common.range.val0to100");
	    			 addFieldError(fieldName+"["+i+"].testresult", object);
	    		}
	    		break;
	    		default:   		
	    			if(searchableCheck  && cordSearchable==1 && (testresult==null || testresult.equals(""))){
		    			 setMessageKey("garuda.cbu.cordentry.inputId");
		    			 addFieldError(fieldName+"["+i+"].testresult", object);
		    		}
	    			if(searchableCheck  && cordSearchable==1 && (testSpecimanType==-1 || testSpecimanType==null)){
		    			 setMessageKey("garuda.cbu.cordentry.inputId");
		    			 addFieldError(fieldName+"["+i+"].fktestspecimen", object);
		    		}
	    		
	    	}
	    	i++;
	    }
	  }
		
	}
	
	 private static enum LabTests
	  {
		 UNCRCT, TCDAD, PERNRBC, PERTNUC, CBU_VOL, PERTMON, CNRBC, PERCD3, CFUCNT, CBUNCCC, TOTCD, FNPV, TCNCC, TCBUUN, VIAB, PERCD, DEF;
	  }
}
