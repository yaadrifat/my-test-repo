package com.velos.ordercomponent.validator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.CompoundRoot;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

/**
 * @author Mohiuddin Ali Ahmed
 * @author Anurag Upadhyay
 * @version 1.0
 * 
 */

public class RequiredFieldValidator extends FieldValidatorSupport
{
	private boolean cdrUser;
	private String cdrUserField="";

	
  public boolean isCdrUser() {
		return cdrUser;
	}

	public void setCdrUser(boolean cdrUser) {
		this.cdrUser = cdrUser;
	}

	public String getCdrUserField() {
		return cdrUserField;
	}

	public void setCdrUserField(String cdrUserField) {
		this.cdrUserField = cdrUserField;
	}

public void validate(Object object) throws ValidationException
  {
	 String fieldName = getFieldName();
     Object value = getFieldValue(fieldName, object);
     value=(value instanceof String && value.equals(""))?null:value;
     CompoundRoot cRoot= ActionContext.getContext().getValueStack().getRoot();
	 long cordSearchable = (Long) getFieldValue("cordSearchable", cRoot);     
	if(cdrUser){		
		 boolean isCDRUser=(Boolean) getFieldValue(cdrUserField, cRoot);     

       if (isCDRUser && value == null){
	       addFieldError(fieldName, object);
	       return;
       }
     }else if( (value == null) && cordSearchable==1 ){//for non cdr user check
    	   addFieldError(fieldName, object);
	       return;
     }
   }
}
