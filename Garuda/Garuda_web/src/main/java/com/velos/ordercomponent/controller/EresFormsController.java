package com.velos.ordercomponent.controller;

import java.util.List;

import com.velos.ordercomponent.service.VelosService;
import com.velos.ordercomponent.service.impl.VelosServiceImpl;

public class EresFormsController {
	VelosService service;
	
	public List getEresFormHtml(Long formId, Long cordId) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getEresFormHtml(formId, cordId);
	}
	
	public void updateSpecimenFormResponse(Long specimenId, List<Long> pkAcctForm) throws Exception{
		service = VelosServiceImpl.getService();
		service.updateSpecimenFormResponse(specimenId, pkAcctForm);
	}
}
