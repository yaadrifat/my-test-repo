package com.velos.ordercomponent.validator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.CompoundRoot;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;
import static com.velos.ordercomponent.action.CordEntryValidatorAction.EligibilityFields;

/**
 * @author Mohiuddin Ali Ahmed
 * @author Anurag Upadhyay
 * @version 1.0
 * 
 */

public class EligibilityRadioFieldValidator extends FieldValidatorSupport {
	private boolean searchableCheck;
	private String dependencyField1="";
	private String dependencyField2="";
	
	public void setSearchableCheck(boolean searchableCheck) {
		this.searchableCheck = searchableCheck;
	}
	public boolean isSearchableCheck() {
		return searchableCheck;
	}
	public String getDependencyField1() {
		return dependencyField1;
	}
	public void setDependencyField1(String dependencyField1) {
		this.dependencyField1 = dependencyField1;
	}
	public String getDependencyField2() {
		return dependencyField2;
	}
	public void setDependencyField2(String dependencyField2) {
		this.dependencyField2 = dependencyField2;
	}
	@Override
	public void validate(Object object) throws ValidationException {

		 Boolean value = (Boolean) getFieldValue(getFieldName(), object);
		 
		 CompoundRoot cRoot= ActionContext.getContext().getValueStack().getRoot();
		 long cordSearchable = (Long) getFieldValue("cordSearchable", cRoot);
		 
		 Boolean dependentF1=(Boolean) getFieldValue(dependencyField1, object);
		 String FieldName=getFieldName().toUpperCase();
		 boolean exist=FieldName.equals("MRQNEWQUES1A") || FieldName.equals("PHYNEWFINDQUES4A") || FieldName.equals("IDMNEWQUES7A") || FieldName.equals("IDMNEWQUES8A");
		 boolean flag=true;
		 switch(EligibilityFields.valueOf(exist?FieldName:"DEF")){
			 case MRQNEWQUES1A:
			 case PHYNEWFINDQUES4A:
			 case IDMNEWQUES7A:
			 case IDMNEWQUES8A:
				 flag=false;
				 break;
		 }
		    /* if( searchableCheck && cordSearchable==1 && !(dependencyField1.equals("")) && value==null ){
				addFieldError(getFieldName(), object);
				return;			
	}else*/ if(searchableCheck && cordSearchable==1 && dependentF1!=null && dependentF1.equals(flag) && value==null){
				addFieldError(getFieldName(), object);
				return;
			}
  }
	

}
