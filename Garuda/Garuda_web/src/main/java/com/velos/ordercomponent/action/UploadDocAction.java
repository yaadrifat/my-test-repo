package com.velos.ordercomponent.action;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.ordercomponent.business.domain.CBUUnitReport;
import com.velos.ordercomponent.business.domain.CdrCbu;
import com.velos.ordercomponent.business.pojoobjects.AttachmentPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUUnitReportPojo;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.SearchPojo;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.controller.AttachmentController;
import com.velos.ordercomponent.controller.CodelstController;
import com.velos.ordercomponent.controller.UploadDocController;
import com.velos.ordercomponent.controller.VelosSearchController;
import com.velos.ordercomponent.helper.velosHelper;

public class UploadDocAction extends VelosBaseAction {
	public static final Log log = LogFactory
			.getLog(UploadDocAction.class);
	private CdrCbuPojo cdrCbuPojo;
	private CdrCbu cdrCbu;
	private SearchPojo searchPojo;
	private VelosSearchController velosSearchController;
	private List cordInfoList;
	 AttachmentPojo attachmentPojo;
	 AttachmentController attachmentController;
	private UploadDocController uploadDocController;
	private CodelstController codelstController;
	private File fileUpload;
	private String fileUploadContentType;
	private String fileUploadFileName;
	private InputStream fileInputStream;
	private Long orderId;
	private String orderType;
	
	
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
	
	
	
	
	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public CdrCbuPojo getCdrCbuPojo() {
		return cdrCbuPojo;
	}

	public void setCdrCbuPojo(CdrCbuPojo cdrCbuPojo) {
		this.cdrCbuPojo = cdrCbuPojo;
	}

	public SearchPojo getSearchPojo() {
		return searchPojo;
	}

	public void setSearchPojo(SearchPojo searchPojo) {
		this.searchPojo = searchPojo;
	}

	public AttachmentPojo getAttachmentPojo() {
		return attachmentPojo;
	}

	public void setAttachmentPojo(AttachmentPojo attachmentPojo) {
		this.attachmentPojo = attachmentPojo;
	}

	public List getCordInfoList() {
		return cordInfoList;
	}

	public void setCordInfoList(List cordInfoList) {
		this.cordInfoList = cordInfoList;
	}

	public File getFileUpload() {
		return fileUpload;
	}

	public void setFileUpload(File fileUpload) {
		this.fileUpload = fileUpload;
	}

	public String getFileUploadContentType() {
		return fileUploadContentType;
	}

	public void setFileUploadContentType(String fileUploadContentType) {
		this.fileUploadContentType = fileUploadContentType;
	}

	public String getFileUploadFileName() {
		return fileUploadFileName;
	}

	public void setFileUploadFileName(String fileUploadFileName) {
		this.fileUploadFileName = fileUploadFileName;
	}

	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public UploadDocAction(){
		
		velosSearchController = new VelosSearchController();
		uploadDocController=new UploadDocController();
		cdrCbuPojo = new CdrCbuPojo();
		searchPojo = new SearchPojo();
		attachmentPojo=new AttachmentPojo();
		attachmentController = new AttachmentController();
		codelstController=new CodelstController();
		
	}

	public String getUploadDoc() throws Exception {
		return "success";
	}
	
	public String getCbuDetails() throws Exception {
		
		
		//String cbuid=cdrCbuPojo.getCdrCbuId();
		//log.debug("CBUID.............." +cbuid);

		setCordInfoList(getEntityDocuments());
		return "data";
	}

/*	public List getCbuAttachInfo(){
		cdrCbuPojo = getCdrCbuPojo();
		String criteria = null;
		try {
			criteria = uploadDocController.getCbuAttachDetails(entityId,entityType,attachmentType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return velosHelper.getObjectList(criteria);

	
	}*/
	
	public List getEntityDocuments(){
		//log.debug("i m hereeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
		cdrCbuPojo = getCdrCbuPojo();
		Long entityId=null;
		Long entityTypeId=null;
		Long attachmentType=null;;
	
		//log.debug("CBUID......"+getCdrCbuPojo().getCdrCbuId());
		//entityId=getCdrCbuPojo().getCordID();
		
		String criteria = null;
		try {
			entityId=uploadDocController.getCbuPkCordId(cdrCbuPojo);
			//log.debug("entityId:::::::::"+entityId);
			entityTypeId=uploadDocController.getCodeListPkIds(ENTITY_CODE_TYPE, ENTITY_CODE_SUB_TYPE);
			attachmentType=uploadDocController.getCodeListPkIds(ATTACHMENT_CODETYPE,ATTACHMENT_CODE_SUBTYPE);
			criteria = uploadDocController.getEntityDocuments(entityId,entityTypeId,attachmentType);
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return velosHelper.getObjectList(criteria);

	
	}

	public String getDocuments() throws Exception{
		
		
		String attachmentId = request.getParameter("attachmentId");
		
		
		try{
			//log.debug("inside getdocuments method in UploadDocAction------"+attachmentId);
			
			Object obj = attachmentController.getAttachment(DOCUMENT_OBJECTNAME,attachmentId, DOCUMENT_ID);
			//log.debug("attachment--->"+obj);
			
			setAttachmentPojo(new AttachmentPojo());
			//log.debug("after new attachmentpojo");
			attachmentPojo = (AttachmentPojo)ObjectTransfer.transferObjects(obj, attachmentPojo);
			//log.debug("document type is:::"+attachmentPojo.getDocumentType());
			fileUploadContentType = attachmentPojo.getDocumentType();
			fileUploadFileName = attachmentPojo.getFileName();
			fileInputStream = attachmentPojo.getDocumentFile().getBinaryStream();
		}catch(Exception e){
			//log.debug("-------------------"+e);
			log.error(e.getMessage());
			e.printStackTrace();
			
			
		}
	
		return SUCCESS;}
	
	
}
