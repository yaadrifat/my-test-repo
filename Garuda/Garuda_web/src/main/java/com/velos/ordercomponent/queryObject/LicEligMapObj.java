package com.velos.ordercomponent.queryObject;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.Callable;

import com.velos.ordercomponent.controller.VDAController;

public class LicEligMapObj implements Runnable{
	
	private VDAController vdaController;
	private String sqlQuery;
	private Map<Long, Object []> filterCDRObj;
	private Map<Long, ArrayList<Long>> filterReasonObj;
	
	private Map<String, Map<Long,ArrayList<Long>>> licEligMap;
	
	public LicEligMapObj(String sqlQuery, VDAController vdaController, Map<Long, Object []> filterCDRObj, Map<Long, ArrayList<Long>> filterReasonObj) {

		this.vdaController = vdaController;
		this.sqlQuery =  sqlQuery;
		this.filterCDRObj = filterCDRObj;
		this.filterReasonObj = filterReasonObj;
		
	}
/*
	@Override
	public Map<String, Map<Long, Long>> call() throws Exception {
		
		if(filterCDRObj != null && filterReasonObj != null)
			return null;
		
		return vdaController.getLicEligMapObject(sqlQuery, filterCDRObj, filterReasonObj);
	}*/

	@Override
	public void run() {
		
		try {
			if(filterCDRObj != null && filterReasonObj != null)				
				licEligMap = vdaController.getLicEligMapObject(sqlQuery, filterCDRObj, filterReasonObj);
			
			System.out.println(" STATUS Finished ----------------------- >");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	/**
	 * @return the licEligMap
	 */
	public Map<String, Map<Long,ArrayList<Long>>> getLicEligMap() {
		return licEligMap;
	}

}
