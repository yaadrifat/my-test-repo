package com.velos.ordercomponent.controller;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.ordercomponent.business.pojoobjects.AdditionalIdsPojo;
import com.velos.ordercomponent.business.pojoobjects.CBBProcedureInfoPojo;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.ClinicalNotePojo;
import com.velos.ordercomponent.business.pojoobjects.SitePojo;
import com.velos.ordercomponent.service.VelosService;
import com.velos.ordercomponent.service.impl.VelosServiceImpl;

public class ClinicalNoteController {
	public static final Log log = LogFactory.getLog(ClinicalNoteController.class);
	VelosService service;
	public ClinicalNotePojo saveClinicalNotes(ClinicalNotePojo clinicalNotePojo) throws Exception{
		service=VelosServiceImpl.getService();
		return service.saveClinicalNotes(clinicalNotePojo);
	}
	
	public List<ClinicalNotePojo> updateClinicalNotes(List<ClinicalNotePojo> cnPojos) throws Exception {
		service=VelosServiceImpl.getService();
		return service.updateClinicalNotes(cnPojos);
	}
	
	/*public List<ClinicalNotePojo> clinicalNoteHistory(Long cordId) throws Exception{
		service=VelosServiceImpl.getService();		
		return service.clinicalNoteHistory(cordId);
	}*/
	public List<ClinicalNotePojo> amendNotes(Long pkNotes) throws Exception{
		//log.debug("For revoke!!!!!!!!!!!!!!!!!!!!!!!");
		service = VelosServiceImpl.getService();
		return service.amendNotes(pkNotes);
		
	}
	
	public Long getCodeListPkIds(String parentCode, String code)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCodeListPkIds(parentCode, code);
	}
	

	public String getNotesByCategory(Long entityId,Long entityTypeId,Long categoryTypeID,String categoryName) throws Exception {
		service = VelosServiceImpl.getService();
		return service.getNotesByCategory(entityId,entityTypeId,categoryTypeID,categoryName);
		
	}
	public ClinicalNotePojo viewClinicalNotes(Long pkNotes) throws Exception{
		service=VelosServiceImpl.getService();
		//log.debug("Inside view clinical notes controller!!!!!!!!!!!");
		return service.viewClinicalNotes(pkNotes);
	}
	
	
	
	public Map<Long,List> getNotes(Long entityId) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getNotes(entityId);
	}
	public Map<Long,List> getPfNotes(Long entityId, Long requestType) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getPfNotes(entityId,requestType);
	}
	
	public List<ClinicalNotePojo> clinicalFilter(String keyword, Long type, Long cordId) throws Exception{
		service=VelosServiceImpl.getService();		
		return service.clinicalfilter(keyword,type,cordId);
	}
	
	public List<ClinicalNotePojo> clinicalNoteHistory(Long cordId, Long noteCategory, Long requestId) throws Exception{
		service=VelosServiceImpl.getService();	     
		return service.clinicalNoteHistory(cordId,noteCategory,requestId);
	}
	
	/*public String getRequestInfo(Long cordId) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getRequestInfo(cordId);
		
	}*/
	
	public String populateUserValues(Long cordId) throws Exception{
		service = VelosServiceImpl.getService();
		return service.populateUserValues(cordId);
		
	}
	

	public List<ClinicalNotePojo> saveCordMultipleNote(List<ClinicalNotePojo> clinicalNotePojo,Long entityId, Long noteCategory) throws Exception{
		service=VelosServiceImpl.getService();
		return service.saveCordMultipleNote(clinicalNotePojo,false,entityId,noteCategory);
	}
	
	public String lastCBUStatus(Long entityId) throws Exception {
		service = VelosServiceImpl.getService();
		return service.lastCBUStatus(entityId);
		
	}
	
/*	public ClinicalNotePojo saveClinicalNotesAssessment(ClinicalNotePojo clinicalNotePojo) throws Exception{
		service=VelosServiceImpl.getService();
		return service.saveClinicalNotesAssessment(clinicalNotePojo);
	}*/
	
	/*public String finalReviewStatus(Long cordId) throws Exception{
		service = VelosServiceImpl.getService();
		return service.finalReviewStatus(cordId);
		
	}*/
	
	/*public List getFlaggedCNoteByPkAsses(Long pkAssessment) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getFlaggedCNoteByPkAsses(pkAssessment);
		
	}*/
	
}
