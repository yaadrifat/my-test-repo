package com.velos.ordercomponent.controller;

import java.util.List;

import com.velos.ordercomponent.business.domain.TaskList;
import com.velos.ordercomponent.service.VelosService;
import com.velos.ordercomponent.service.impl.VelosServiceImpl;

public class TaskListController {

	VelosService service;
	
	public List addTaskList(TaskList taskList1,TaskList taskList2) throws Exception {
		service = VelosServiceImpl.getService();
		return service.addTaskList(taskList1, taskList2);
	}
}
