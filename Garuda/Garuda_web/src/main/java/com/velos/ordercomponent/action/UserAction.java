package com.velos.ordercomponent.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.velos.ordercomponent.business.domain.CodeList;
import com.velos.ordercomponent.business.domain.UserAlerts;
import com.velos.ordercomponent.business.pojoobjects.AddressPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.pojoobjects.SearchPojo;
import com.velos.ordercomponent.business.pojoobjects.SitePojo;
import com.velos.ordercomponent.business.pojoobjects.UserAlertsPojo;
import com.velos.ordercomponent.business.pojoobjects.UserPojo;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.VelosUtil;
import com.velos.ordercomponent.controller.UserAlertController;
import com.velos.ordercomponent.controller.VelosSearchController;
import com.velos.eres.business.common.UserSiteDao;
import com.velos.eres.web.userSite.UserSiteJB;
import com.velos.ordercomponent.controller.CBBController;
import com.velos.ordercomponent.util.GarudaConfig;

public class UserAction extends VelosBaseAction{

	public static final Log log = LogFactory.getLog(UserAction.class);
	 private Map codelstParam;
	 private SearchPojo searchPojo;
	 private List lstObject;
	 private List list;
	 private VelosSearchController velosSearchController;
	 private CodeList codeList;
	 private UserPojo userPojo;
	 private AddressPojo addressPojo;
	 private UserAlertsPojo userAlertsPojo;
	 private UserAlertController userAlertController;
	 private UserAlerts userAlerts;
	 private String message;
	 private List<String> leftListAlerts = new ArrayList<String>();
	 private List<String> rightListAlerts = new ArrayList<String>();
	 private Boolean mailUpdate;
	 private String emailId;
	 private Long pkresolCustAlert;
	 private PaginateSearch paginateSearch;
	 private String leftalert;
	 private String rightalert;
	 private List userAlertsLst;
	 private List userResolAlertsLst;
	 private Long pkresolAllAlert;
	 private List<UserAlertsPojo> userAlertList;
	 private List<UserAlertsPojo> userResolAlertList;
	 private List userSitesLst;
	 private List userSiteRightLst;
	 private List userSiteIdLst;
	 CBBController cbbController;

	 
	public List getUserSiteIdLst() {
		return userSiteIdLst;
	}

	public void setUserSiteIdLst(List userSiteIdLst) {
		this.userSiteIdLst = userSiteIdLst;
	}

	public List getUserSiteRightLst() {
		return userSiteRightLst;
	}

	public void setUserSiteRightLst(List userSiteRightLst) {
		this.userSiteRightLst = userSiteRightLst;
	}

		public List getUserSitesLst() {
			return userSitesLst;
		}
	
		public void setUserSitesLst(List userSitesLst) {
			
			this.userSitesLst = userSitesLst;
		}

		public List<UserAlertsPojo> getUserResolAlertList() {
			return userResolAlertList;
		}

		public void setUserResolAlertList(List<UserAlertsPojo> userResolAlertList) {
			this.userResolAlertList = userResolAlertList;
		}

		public List<UserAlertsPojo> getUserAlertList() {
			return userAlertList;
		}

		public void setUserAlertList(List<UserAlertsPojo> userAlertList) {
			this.userAlertList = userAlertList;
		}

		public Long getPkresolAllAlert() {
			return pkresolAllAlert;
		}

		public void setPkresolAllAlert(Long pkresolAllAlert) {
			this.pkresolAllAlert = pkresolAllAlert;
		}

		public List getUserResolAlertsLst() {
			return userResolAlertsLst;
		}

		public void setUserResolAlertsLst(List userResolAlertsLst) {
			this.userResolAlertsLst = userResolAlertsLst;
		}

		public List getUserAlertsLst() {
			return userAlertsLst;
		}

		public void setUserAlertsLst(List userAlertsLst) {
			this.userAlertsLst = userAlertsLst;
		}

		public Long getPkresolCustAlert() {
			return pkresolCustAlert;
		}

		public void setPkresolCustAlert(Long pkresolCustAlert) {
			this.pkresolCustAlert = pkresolCustAlert;
		}

		public PaginateSearch getPaginateSearch() {
		return paginateSearch;
		}

		public void setPaginateSearch(PaginateSearch paginateSearch) {
		this.paginateSearch = paginateSearch;
		}

		public String getEmailId() {
			return emailId;
		}

		public void setEmailId(String emailId) {
			this.emailId = emailId;
		}

		public Boolean getMailUpdate() {
			return mailUpdate;
		}

		public void setMailUpdate(Boolean mailUpdate) {
			this.mailUpdate = mailUpdate;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public List<String> getLeftListAlerts() {
			return leftListAlerts;
		}

		public void setLeftListAlerts(List<String> leftListAlerts) {
			this.leftListAlerts = leftListAlerts;
		}

		public List<String> getRightListAlerts() {
			return rightListAlerts;
		}

		public void setRightListAlerts(List<String> rightListAlerts) {
			this.rightListAlerts = rightListAlerts;
		}

		public String getLeftalert() {
			return leftalert;
		}

		public void setLeftalert(String leftalert) {
			this.leftalert = leftalert;
		}
		
		
		public String getRightalert() {
			return rightalert;
		}
		
		public void setRightalert(String rightalert) {
			this.rightalert = rightalert;
		}

		
	public UserPojo getUserPojo() {
		return userPojo;
	}

	public void setUserPojo(UserPojo userPojo) {
		this.userPojo = userPojo;
	}

	public Map getCodelstParam() {
		return codelstParam;
	}

	public void setCodelstParam(Map codelstParam) {
		this.codelstParam = codelstParam;
	}

	public SearchPojo getSearchPojo() {
		return searchPojo;
	}

	public void setSearchPojo(SearchPojo searchPojo) {
		this.searchPojo = searchPojo;
	}

	public List getLstObject() {
		return lstObject;
	}

	public void setLstObject(List lstObject) {
		this.lstObject = lstObject;
	}

	public VelosSearchController getVelosSearchController() {
		return velosSearchController;
	}

	public void setVelosSearchController(VelosSearchController velosSearchController) {
		this.velosSearchController = velosSearchController;
	}

	public CodeList getCodeList() {
		return codeList;
	}

	public void setCodeList(CodeList codeList) {
		this.codeList = codeList;
	}
	
	public AddressPojo getAddressPojo() {
		return addressPojo;
	}

	public void setAddressPojo(AddressPojo addressPojo) {
		this.addressPojo = addressPojo;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	public UserAction(){
		velosSearchController = new VelosSearchController();
		codeList = new CodeList();
		userPojo = new UserPojo();
		userAlertController = new UserAlertController();
		userAlertsPojo = new UserAlertsPojo();
		userAlerts = new UserAlerts();
		addressPojo = new AddressPojo();
		paginateSearch = new PaginateSearch();
		userAlertsLst=new ArrayList();
		userAlertList=new ArrayList<UserAlertsPojo>();
		userResolAlertList=new ArrayList<UserAlertsPojo>();
		cbbController = new CBBController();
	}
	
	/*
	 * getUserProfile method for showing the search screen for 
	 * searching the user id. 
	 */
	public String getUserProfile(){
		//log.debug("inside user profile");
		return "success";
	}
	
	/*
	 * getUserDetails method for showing the searched results in the 
	 * the grid 
	 */	
	public String getUserDetails(){
		try {
			//log.debug("before criteria construction");
			String criteria = constructCriteria();
			
			//pagination code
			paginationReqScopeData(paginateSearch);
			//-----------end------------

			lstObject = userAlertController.getusersById(criteria,paginateSearch,1);
		
			// calculate next record start record  and end record 
			int iTotalRows = userAlertController.getusersById(criteria,paginateSearch,0).size();
			paginateSearch.setiTotalRows(iTotalRows);
            setPaginateSearch(VelosUtil.getListTotalCount(paginateSearch));
            //------------end--------------
		
			} catch (Exception exArg) {
				log.error(exArg.getMessage());
				exArg.printStackTrace();
			}
			setLstObject(lstObject);
			return "success";
	}

/*
 * submitUserAlerts method for submiting the user alerts
 */
public String submitUserAlerts() throws Exception{
	setMessage("");
	try{
		for(UserAlertsPojo usPojo1 : getUserResolAlertList()){
			  if(usPojo1.getFkCodelstId()!=null){
				  if(!usPojo1.getEmailFlag()){
					  continue;
				  }else{
					  userAlertList.add(usPojo1);
				  }
			  }
		   }
		setUserAlertList(userAlertController.saveUserAlert(getUserAlertList()));
		setMessage("Alerts Updated Successfully!");
		showUserDetails();
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}
	return SUCCESS;
}

/*
 * showUserDetails method for showing the perticuler user and having alerts
 */
public String showUserDetails() {
	
	try{
		codelstParam = new HashMap<String, String>();
		searchPojo = new SearchPojo();
		searchPojo.setCodelstParam(codelstParam);
		searchPojo.setSearchType(USER_OBJECTNAME);
		//log.debug("before criteria construction");
		String userid = request.getParameter("userId");
		searchPojo.setCriteria(" userId = '"+Long.parseLong(userid)+"'");
		searchPojo = velosSearchController.velosSearch(searchPojo);
		if(searchPojo.getResultlst()!=null && searchPojo.getResultlst().size()>0){
		userPojo = (UserPojo)ObjectTransfer.transferObjects(searchPojo.getResultlst().get(0), userPojo);
		}
		setUserPojo(userPojo);
		setPkresolCustAlert(getCodeListPkByTypeAndSubtype(USER_ALERT, ALERT_RESOL_CUST));
		setPkresolAllAlert(getCodeListPkByTypeAndSubtype(USER_ALERT, ALERT_RESOL_ALL));
		setUserAlertsLst(userAlertController.getUserAlerts(Long.parseLong(userid), USER_ALERT));
		setUserResolAlertsLst(userAlertController.getUserAlerts(Long.parseLong(userid), USER_ALERT_RESOL));
		UserSiteJB userSiteB = new UserSiteJB();
		UserSiteDao userSiteDao = userSiteB.getUserSiteTree(userPojo.getFkAccount().intValue(),(int)Long.parseLong(userid));
		setUserSitesLst(userSiteDao.getUserSiteNames());
		setUserSiteRightLst(userSiteDao.getUserSiteRights());
		setUserSiteIdLst(userSiteDao.getUserSiteIds());
		paginationReqScopeData(paginateSearch);
		List<SitePojo> cbbList = cbbController.getSites();
		getRequest().setAttribute("CbbList", cbbList);
	}
	catch (Exception e) {
		log.error(e.getMessage());
		e.printStackTrace();
	}
	return SUCCESS;
}

/*
 * updateUserMailid method for showing the mail id in the model window and 
 * updating the mailid 
 */
public String updateUserMailid(){
	try{
		if(userPojo.getUserId()!=null){
			userPojo = userAlertController.getUserById(userPojo);
			if(userPojo.getAddress()!=null && userPojo.getAddress().getAddressId()!=null){
				searchPojo = new SearchPojo();	
				searchPojo.setCodelstParam(null);
				searchPojo.setSearchType(ADDRESS_OBJECTNAME);
				searchPojo.setCriteria(" addressId = '"+userPojo.getAddress().getAddressId()+"'");
				searchPojo = velosSearchController.velosSearch(searchPojo);
				if(searchPojo.getResultlst()!=null && searchPojo.getResultlst().size()>0){
					addressPojo = (AddressPojo)ObjectTransfer.transferObjects(searchPojo.getResultlst().get(0), addressPojo);
					}
			}
			if(!getMailUpdate()){
				setEmailId(addressPojo.getEmail());
			}
			if(getMailUpdate()){
				addressPojo.setEmail(getEmailId());
				userAlertController.updateUserMailId(addressPojo);
			    getRequest().setAttribute("mailUpdate", true);
			}
			
			SearchPojo searchPojo1 = new SearchPojo();
			searchPojo1.setSearchType(USER_OBJECTNAME);
			searchPojo1.setCriteria(" userId = "+userPojo.getUserId());
			searchPojo1 = velosSearchController.velosSearch(searchPojo1);
			if(searchPojo1.getResultlst()!=null && searchPojo1.getResultlst().size()>0){
				userPojo = (UserPojo)ObjectTransfer.transferObjects(searchPojo1.getResultlst().get(0), userPojo);
			}
			List list = getLeftRightLists(userPojo.getUserId());
			setLeftListAlerts((List)list.get(0));
			setRightListAlerts((List)list.get(1));
			
		}
	}catch(Exception e){
		log.error(e.getMessage());
		e.printStackTrace();
	}
	return "success";
}

/*
 * search criteria for searching the user id return type string	
 */
	private String constructCriteria() {
		String criteria = " ";
		//log.debug("inside constructCriteria");
		userPojo = getUserPojo();

		if (userPojo != null) {

			if (userPojo.getLoginId() != null
					&& !userPojo.getLoginId().trim().equals("")) {
				criteria += "and lower(userd.usr_logname) like lower('"
						+ userPojo.getLoginId() + "')";
			}
			
			if(!criteria.trim().equals("")){
				criteria = criteria +" and userd.fk_account = "+userPojo.getFkAccount();	
			}
			else
				criteria = criteria +"and userd.fk_account = "+userPojo.getFkAccount();
			// criteria = " where (deletedFlag = 0 or deletedFlag is null) " +
			// criteria;
		}
		//log.debug("Criteria *****" + criteria);
		return criteria;
	}

	/*
	 * getLeftRightLists method return type is list of alerts and parameter is Long type 
	 * that is user id . here we are setting the user alerts to the list
	 */
	private List getLeftRightLists(Long userid){
		try{
		list = new ArrayList();
		List fkcodelist = userAlertController.getUserAlerts(userid,USER_ALERT);
		//log.debug("fkcodelist size ==:: "+fkcodelist.size());
		List<String> duplicateList = new ArrayList<String>();
		List userAlertList = GarudaConfig.getCodeListMap().get(USER_ALERT);
		if(fkcodelist.size() != 0){
			try{
			for (Iterator it = fkcodelist.iterator(); it.hasNext();) {
			
			Long code =(Long)it.next();
			
			//log.debug("code ::: "+code);
			if(userAlertList != null){
				for (Iterator iterator = userAlertList.iterator(); iterator.hasNext();) {
					codeList = (CodeList) iterator.next();
					
					//log.debug(code+"=="+codeList.getPkCodeId());
					int comp = code.compareTo(codeList.getPkCodeId());
					//log.debug("code :: "+code);
					if(comp == 0){
						rightListAlerts.add(codeList.getPkCodeId()+"-"+codeList.getDescription());
						duplicateList.add(codeList.getPkCodeId()+"-"+codeList.getDescription());
					}
					else{
						if(!leftListAlerts.contains(codeList.getPkCodeId()+"-"+codeList.getDescription())){
							leftListAlerts.add(codeList.getPkCodeId()+"-"+codeList.getDescription());
						}
						
					}
				}
			}
		}
		leftListAlerts.removeAll(duplicateList);
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}
		}
		else{
			try{
				if(userAlertList != null){
					for (Iterator iterator = userAlertList.iterator(); iterator.hasNext();) {
						codeList = (CodeList) iterator.next();
						leftListAlerts.add(codeList.getPkCodeId()+"-"+codeList.getDescription());
					}
				}	
			}catch (Exception e) {
				log.error(e.getMessage());
				e.printStackTrace();
				}	
			}
		
		list.add(leftListAlerts);
		list.add(rightListAlerts);
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	
}
