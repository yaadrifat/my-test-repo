package com.velos.ordercomponent.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.ordercomponent.business.pojoobjects.CBUUnitReportPojo;
import com.velos.ordercomponent.business.domain.CBUUnitReport;
import com.velos.ordercomponent.business.pojoobjects.CBUUnitReportTempPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUploadInfoPojo;
import com.velos.ordercomponent.business.pojoobjects.CordHlaExtPojo;
import com.velos.ordercomponent.business.pojoobjects.CordHlaExtTempPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.service.VelosService;
import com.velos.ordercomponent.service.impl.VelosServiceImpl;


public class CBUUnitReportController {
	public static final Log log = LogFactory.getLog(CBUUnitReportController.class);
	VelosService service;
	public CBUUnitReportPojo saveCBUUnitReport(CBUUnitReportPojo cbuUnitReportPojo) throws Exception{
		//log.debug("IN CBUUnitReportController");
		service = VelosServiceImpl.getService();
		return service.saveCBUUnitReport(cbuUnitReportPojo);
	}
	
	public CBUploadInfoPojo saveUploadInfo(CBUploadInfoPojo cbUploadInfoPojo) throws Exception{
		
		service = VelosServiceImpl.getService();
		return service.saveCatUploadInfo(cbUploadInfoPojo);
	}
	
	public CordHlaExtPojo saveHlaInfo(CordHlaExtPojo cordHlaExtPojo) throws Exception{
		//log.debug("IN HLA CBUUnitReportController");
		service = VelosServiceImpl.getService();
		return service.saveHlaInfo(cordHlaExtPojo);
	}
	
	public List getRevokeUnitReportLst(String cordID,PaginateSearch paginateSearch,int i,String condition, String orderBy) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getRevokeUnitReportLst(cordID,paginateSearch,i,condition,orderBy);
	}
	
	public CBUUnitReportTempPojo saveCBUUnitTempReport(CBUUnitReportTempPojo cbuUnitReportTempPojo) throws Exception{
		//log.debug("IN CBUUnitReportController--------");
		service = VelosServiceImpl.getService();
		return service.saveCBUUnitTempReport(cbuUnitReportTempPojo);
	}
	
	public CordHlaExtTempPojo saveHlaTempInfo(CordHlaExtTempPojo cordHlaExtTempPojo) throws Exception{
		//log.debug("IN HLA CBUUnitReportController");
		service = VelosServiceImpl.getService();
		return service.saveHlaTempInfo(cordHlaExtTempPojo);
	}
	
	public List<CBUUnitReport> getCbuUnitReportLst(Long cordId, Long attachmentId)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCbuUnitReportLst(cordId,attachmentId);
	}
	
	public Object getCbuInfo(String objectName,String id,String fieldName) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCbuInfo(objectName, id,fieldName);
	}
	
	public String getcordinfopk() throws Exception{
		service = VelosServiceImpl.getService();
		return service.getcordinfopk();
	}
	public List getUploadedDocumentLst(String cordID) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getUploadedDocumentLst(cordID);
	}

}
