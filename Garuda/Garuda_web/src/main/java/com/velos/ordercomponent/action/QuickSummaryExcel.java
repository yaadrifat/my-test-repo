package com.velos.ordercomponent.action;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;

import com.opensymphony.xwork2.ActionSupport;
import com.velos.ordercomponent.util.VelosGarudaConstants;
import com.opensymphony.xwork2.ActionSupport;

public class QuickSummaryExcel extends ActionSupport implements VelosGarudaConstants{
	
	private Boolean isMultipleCBBSelected = false;
	
	public Workbook quickSummaryExcel(List<Object> cdrObjects, Map<Long,Long> duplCords, Workbook wb) throws ParseException{		 
		
		XSSFCellStyle headStyle = headStyle(wb);
		XSSFCellStyle hightLightStyle = redStyle(wb);
		XSSFCellStyle dateCellStyle = dateStyle(wb);
		
		 Row row;
		 Cell cell;
		 SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
		 int rowNum = 1;
		 short col = 0;	
		 int sheetNo = 1;	
		 Sheet sheet = sheet(wb, getText("garuda.querybuilder.report.shippedcbus1"));
		 createHeader(sheet, headStyle);		 
		
		 
		 for(Object cdrObj : cdrObjects){			
			
			 if(cdrObj!=null){					
	       	        Object[] objArr = (Object[])cdrObj;
		
			 if(rowNum  > VelosGarudaConstants.MAXROW_XLSX ){
				 
				 sheet = sheet(wb,getText("garuda.querybuilder.report.shippedcbus1")+" "+(++sheetNo));
				 createHeader(sheet, headStyle);
				 rowNum = 1;
			 }
			 
			  row = sheet.createRow(rowNum++);
			  col = 0;			 
				
				  
				  if(isMultipleCBBSelected){
					  row.createCell(col++).setCellValue(objArr[1]!=null?objArr[1].toString():""); //cbb id
				  }
				 
				  cell = row.createCell(col++);cell.setCellValue(objArr[2]!=null?objArr[2].toString():"");cell.setCellStyle((duplCords !=null && duplCords.get(Long.valueOf(objArr[0].toString())) !=null)?hightLightStyle:null);// registryid
				 
				  row.createCell(col++).setCellValue(objArr[3]!=null?objArr[3].toString():"");// cbu local id
				  row.createCell(col++).setCellValue(objArr[4]!=null?objArr[4].toString():"");// Unique product identity on bag
				  row.createCell(col++).setCellValue(objArr[5]!=null?objArr[5].toString():"");// ISBT
				  row.createCell(col++).setCellValue(objArr[9]!=null?objArr[9].toString():"");//Request Type
				  
				  cell = row.createCell(col++);
					  if(objArr[10] != null){// Request Date
						  cell.setCellValue(format.parse(objArr[10].toString()));
						  cell.setCellStyle(dateCellStyle);
					  }
				 
				  
				  cell = row.createCell(col++);// Request Date		  
				  if(objArr[9]!=null && objArr[9].toString().trim().equals(OR) && objArr[11]!=null && !StringUtils.isEmpty(objArr[11].toString())){ 
        				cell.setCellStyle(dateCellStyle);        				       	        		
    	        		cell.setCellValue(format.parse(objArr[11].toString()));
        			
        		  }  
				  
				  cell = row.createCell(col++);// Infusion Date
				  if(objArr[12]!=null && !((String)objArr[12]).isEmpty()){
	        			cell.setCellStyle(dateCellStyle);    	        		        		
    	        		cell.setCellValue(format.parse(objArr[12].toString()));
	        		}
				  
				  row.createCell(col++).setCellValue(objArr[13]!=null?objArr[13].toString():"");// Patient id
				  row.createCell(col++).setCellValue(objArr[14]!=null?objArr[14].toString():"");// patient Diagnosis
				  row.createCell(col++).setCellValue(objArr[15]!=null?objArr[15].toString():"");// Tc Code
				  row.createCell(col++).setCellValue(objArr[16]!=null?objArr[16].toString():"");// TC Name
			 
			 }
		 }
		 
		 return wb;
	}
	
	private Sheet sheet(Workbook wb, String sheetName){
		
		 Sheet sheet = wb.createSheet(sheetName); 
		 
		 return sheet;
	}
	
	private int createHeader(Sheet sheet, XSSFCellStyle blackStyle){
		
		 int r = 0, column = 0;
		 Cell cell;
		 Row row;
		 row = sheet.createRow(r);    
		 row.setHeight((short)500);  
		 
		 if(isMultipleCBBSelected){
			 cell=row.createCell(column++);cell.setCellValue(getText("garuda.queryBuilder.label.cbbid"));cell.setCellStyle(blackStyle);
		 }		
		cell=row.createCell(column++);cell.setCellValue(getText("garuda.cbuentry.label.registrycbuid"));cell.setCellStyle(blackStyle);
		cell=row.createCell(column++);cell.setCellValue(getText("garuda.cbuentry.label.localcbuid"));cell.setCellStyle(blackStyle);
		cell=row.createCell(column++);cell.setCellValue(getText("garuda.cdrcbuview.label.id_on_bag"));cell.setCellStyle(blackStyle);
		cell=row.createCell(column++);cell.setCellValue(getText("garuda.shippedcbu.report.label.isbt"));cell.setCellStyle(blackStyle);
		cell=row.createCell(column++);cell.setCellValue(getText("garuda.cbuentry.label.requestType"));cell.setCellStyle(blackStyle);
		cell=row.createCell(column++);cell.setCellValue(getText("garuda.idsReport.reqdate"));cell.setCellStyle(blackStyle);
		cell=row.createCell(column++);cell.setCellValue(getText("garuda.idsReport.orshipdate"));cell.setCellStyle(blackStyle);
		cell=row.createCell(column++);cell.setCellValue(getText("garuda.shippedcbu.report.label.orinfuseddt"));cell.setCellStyle(blackStyle);
		cell=row.createCell(column++);cell.setCellValue(getText("garuda.recipient&tcInfo.label.recipientid"));cell.setCellStyle(blackStyle);
		cell=row.createCell(column++);cell.setCellValue(getText("garuda.shippedcbu.report.label.patientdiag"));cell.setCellStyle(blackStyle);
		cell=row.createCell(column++);cell.setCellValue(getText("garuda.shippedcbu.report.label.tccode"));cell.setCellStyle(blackStyle);
		cell=row.createCell(column++);cell.setCellValue(getText("garuda.shippedcbu.report.label.tcname"));cell.setCellStyle(blackStyle);
		
		
		
		return column;
	}

private XSSFCellStyle dateStyle(Workbook wb){
		
		XSSFCellStyle style = (XSSFCellStyle) wb.createCellStyle();
		CreationHelper createHelper = wb.getCreationHelper();
		
		style.setDataFormat(
			        createHelper.createDataFormat().getFormat("mm/dd/yyyy"));
	    
	    return style;
	}
	
private XSSFCellStyle headStyle(Workbook wb){
		
		XSSFCellStyle style = (XSSFCellStyle) wb.createCellStyle();

	    style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
	    style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    style.setWrapText(false);
	    
	    Font font = wb.createFont();
	    font.setColor(IndexedColors.WHITE.getIndex());
	    font.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    style.setFont(font);
	   // Row.setHeight((short)500);
	    
	    return style;
	}


private XSSFCellStyle redStyle(Workbook wb){
	
	XSSFCellStyle style = (XSSFCellStyle) wb.createCellStyle();   
    Font font = wb.createFont();  
    style.setFillForegroundColor(IndexedColors.RED.getIndex());
    style.setFillPattern(CellStyle.SOLID_FOREGROUND);
    style.setWrapText(false);
    style.setFont(font);
    
    return style;
}
	public void setIsMultipleCBBSelected(Boolean isMultipleCBBSelected) {
		this.isMultipleCBBSelected = isMultipleCBBSelected;
	}

}
