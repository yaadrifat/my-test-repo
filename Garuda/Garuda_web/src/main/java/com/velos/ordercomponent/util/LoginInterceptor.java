package com.velos.ordercomponent.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.StrutsStatics;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class LoginInterceptor extends AbstractInterceptor implements StrutsStatics {

	public static final Log log=LogFactory.getLog(LoginInterceptor.class);
	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		// TODO Auto-generated method stub
		
		final ActionContext context = invocation.getInvocationContext().getContext();
		HttpServletRequest request = (HttpServletRequest)context.get(HTTP_REQUEST);
		HttpSession session = request.getSession(true);
		Integer userNumId = 0;
		String userId = (String) session.getAttribute("userId");
		 if (userId == null || userId.equals(""))
	            return "sessionTimeOut";
		
		try{
		 userNumId = Integer.parseInt(userId);
		 	
		}catch(NumberFormatException e){
			log.error(e.getMessage());
			e.printStackTrace();
		}

        // if valid user return true else false
		if (userNumId > 0) {
            return invocation.invoke(); 
        } else {
            return "sessionTimeOut";
        }	
	}

}
