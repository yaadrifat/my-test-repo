package com.velos.ordercomponent.action;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.opensymphony.xwork2.ActionSupport;
import com.velos.ordercomponent.business.pojoobjects.HLAFilterPojo;
import com.velos.ordercomponent.util.VelosGarudaConstants;

public class QuickSummaryCSV extends ActionSupport implements VelosGarudaConstants{
	
	private Boolean isMultipleCBBSelected = false;
	
	
	public File quickSummaryCSV(List<Object> cdrObjects, String path){
		
		
		 int bufferSize = 10240;
		 FileWriter fr = null;
		 BufferedWriter br = null;
		
		 File file = null;
		 Map<Long, HLAFilterPojo> cbuHla = null;
		 StringBuilder content = new StringBuilder();
		
		 
		try {
			
			file = new File(path);
			fr = new FileWriter(file);		
           br = new BufferedWriter(fr, bufferSize);
       
		
		 
		 
		 
		content = createHeader(content);
		br.write(content.toString());				
	    content.setLength(0);
	    
		for(Object obj : cdrObjects){		 
			
			if(obj!=null){
				
      	        Object[] objArr = (Object[])obj;			
			  
			    if(isMultipleCBBSelected){
				  content.append(WRAPPER).append(objArr[1]!=null?objArr[1].toString():"").append(WRAPPER).append(SEPRATOR); 
			    }
				  content.append(WRAPPER).append(objArr[2]!=null?objArr[2]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[3]!=null?objArr[3]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[4]!=null?objArr[4]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[5]!=null?objArr[5]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[9]!=null?objArr[9]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[10]!=null?objArr[10]:"").append(WRAPPER).append(SEPRATOR);
				  
				  if(objArr[9]!=null && objArr[9].toString().trim().equals(OR) && objArr[11]!=null && !StringUtils.isEmpty(objArr[11].toString())) 
					  content.append(WRAPPER).append(objArr[11]!=null?objArr[11]:"").append(WRAPPER).append(SEPRATOR);
				  else	 
					  content.append(EMPTY_VALUE).append(SEPRATOR);
				  
				  content.append(WRAPPER).append(objArr[12]!=null?objArr[12]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[13]!=null?objArr[13]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[14]!=null?objArr[14]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[15]!=null?objArr[15]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[16]!=null?objArr[16]:"").append(WRAPPER).append(SEPRATOR);
				 
				  content.append(NEW_LINE); 
				  
				  br.write(content.toString());				
				  content.setLength(0);
			}
				
		 }
		 } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
          try {
              br.close();
              fr.close();
          } catch (IOException e) {
              e.printStackTrace();
          }
      }
		return file;
	}
	
	
private StringBuilder createHeader(StringBuilder content){
	
	
	
	if(isMultipleCBBSelected){
		   content.append(WRAPPER).append(getText("garuda.queryBuilder.label.cbbid")).append(WRAPPER).append(SEPRATOR);
	}
		   content.append(WRAPPER).append(getText("garuda.cbuentry.label.registrycbuid")).append(WRAPPER).append(SEPRATOR);              
		   content.append(WRAPPER).append(getText("garuda.cbuentry.label.localcbuid")).append(WRAPPER).append(SEPRATOR);                   
		   content.append(WRAPPER).append(getText("garuda.cbuentry.label.idbag")).append(WRAPPER).append(SEPRATOR); 
		   content.append(WRAPPER).append(getText("garuda.shippedcbu.report.label.isbt")).append(WRAPPER).append(SEPRATOR);                           
		   content.append(WRAPPER).append(getText("garuda.cbuentry.label.requestType")).append(WRAPPER).append(SEPRATOR);		  
		   content.append(WRAPPER).append(getText("garuda.idsReport.reqdate")).append(WRAPPER).append(SEPRATOR);           
		   content.append(WRAPPER).append(getText("garuda.idsReport.orshipdate")).append(WRAPPER).append(SEPRATOR);	
		   content.append(WRAPPER).append(getText("garuda.shippedcbu.report.label.orinfuseddt")).append(WRAPPER).append(SEPRATOR);
		   content.append(WRAPPER).append(getText("garuda.recipient&tcInfo.label.recipientid")).append(WRAPPER).append(SEPRATOR);
		   content.append(WRAPPER).append(getText("garuda.shippedcbu.report.label.patientdiag")).append(WRAPPER).append(SEPRATOR);
		   content.append(WRAPPER).append(getText("garuda.shippedcbu.report.label.tccode")).append(WRAPPER).append(SEPRATOR);
		   content.append(WRAPPER).append(getText("garuda.shippedcbu.report.label.tcname")).append(WRAPPER).append(SEPRATOR);
		   content.append(NEW_LINE);
			
		   return content;
	}

public void setIsMultipleCBBSelected(Boolean isMultipleCBBSelected) {
	this.isMultipleCBBSelected = isMultipleCBBSelected;
}

}
