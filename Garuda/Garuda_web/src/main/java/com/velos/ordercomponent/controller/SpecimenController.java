package com.velos.ordercomponent.controller;

import com.velos.ordercomponent.business.domain.Specimen;
import com.velos.ordercomponent.service.VelosService;
import com.velos.ordercomponent.service.impl.VelosServiceImpl;

public class SpecimenController {

	VelosService service;
	
	public Specimen saveOrUpdateSpecimen(Specimen specimenPojo) throws Exception{
		service = VelosServiceImpl.getService();
		return service.saveOrUpdateSpecimen(specimenPojo,true);
	}
	
	public Specimen saveOrUpdateSpecimen(Specimen specimenPojo,Boolean fromUI) throws Exception{
		service = VelosServiceImpl.getService();
		return service.saveOrUpdateSpecimen(specimenPojo,fromUI);
	}
}
