package com.velos.ordercomponent.action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.eres.web.grpRights.GrpRightsJB;
import com.velos.eres.web.user.UserJB;
import com.velos.ordercomponent.business.domain.Attachment;
import com.velos.ordercomponent.business.domain.CBUFinalReview;
import com.velos.ordercomponent.business.domain.CBUUnitReport;
import com.velos.ordercomponent.business.domain.CBUploadInfo;
import com.velos.ordercomponent.business.domain.CdrCbu;
import com.velos.ordercomponent.business.domain.CodeList;
import com.velos.ordercomponent.business.domain.EntityStatusReason;
import com.velos.ordercomponent.business.domain.LabCategory;
import com.velos.ordercomponent.business.domain.LabMaint;
import com.velos.ordercomponent.business.domain.Orders;
import com.velos.ordercomponent.business.domain.PatientLab;
import com.velos.ordercomponent.business.pojoobjects.ActiveOrderPojo;
import com.velos.ordercomponent.business.pojoobjects.AttachmentPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUCompleteReqInfoPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUFinalReviewPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUUnitReportPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUploadInfoPojo;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.ClinicalNotePojo;
import com.velos.ordercomponent.business.pojoobjects.EligibilityDeclPojo;
import com.velos.ordercomponent.business.pojoobjects.EntityStatusPojo;
import com.velos.ordercomponent.business.pojoobjects.EntityStatusReasonPojo;
import com.velos.ordercomponent.business.pojoobjects.FilterDataPojo;
import com.velos.ordercomponent.business.pojoobjects.LabCategoryPojo;
import com.velos.ordercomponent.business.pojoobjects.LabMaintPojo;
import com.velos.ordercomponent.business.pojoobjects.OrderAttachmentPojo;
import com.velos.ordercomponent.business.pojoobjects.OrderPojo;
import com.velos.ordercomponent.business.pojoobjects.OtherOrdersPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.pojoobjects.PatientLabPojo;
import com.velos.ordercomponent.business.pojoobjects.PendingOrdersData;
import com.velos.ordercomponent.business.pojoobjects.SearchPojo;
import com.velos.ordercomponent.business.pojoobjects.SitePojo;
import com.velos.ordercomponent.business.util.CommonDao;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.Utilities;
import com.velos.ordercomponent.business.util.VelosUtil;
import com.velos.ordercomponent.controller.AttachmentController;
import com.velos.ordercomponent.controller.CBBController;
import com.velos.ordercomponent.controller.CBUController;
import com.velos.ordercomponent.controller.CBUUnitReportController;
import com.velos.ordercomponent.controller.CodelstController;
import com.velos.ordercomponent.controller.OpenOrderController;
import com.velos.ordercomponent.controller.UploadDocController;
import com.velos.ordercomponent.controller.VelosSearchController;
import com.velos.ordercomponent.exception.MidErrorConstants;
import com.velos.ordercomponent.helper.CBUUnitReportHelper;
import com.velos.ordercomponent.helper.velosHelper;
import com.velos.ordercomponent.util.VelosGarudaConstants;


public class OpenOrderAction extends VelosBaseAction {
	public static final Log log = LogFactory.getLog(OpenOrderAction.class);
	private VelosSearchController velosSearchController;

	private final String CATEGORY_LISTNAME = "categoryList";

	private final String CLINICAL_TEST_LISTNAME = "clinicalTestList";
	private CodelstController codelstController;
	private CBUUnitReportAction CBUUnitReportAction;
	
	private List openOrderList;

	private List<LabCategory> categoryList;

	private List clinicalTestList;

	private OrderAttachmentPojo orderAttachmentPojo;

	private AttachmentPojo attachmentPojo;
	
	private List lstObject;
	
	private LabCategory labCategory;
	
	private Map codelstParam;

	private LabMaint labMaint;
	
	private CodeList codeList;
	
	private LabMaintPojo labMaintPojo;
	
	private SearchPojo searchPojo;
	
	private CBUUnitReportPojo cbuUnitReportPojo;
	
	private LabCategoryPojo labCategoryPojo;

	private OpenOrderController linkController;

	private CBUUnitReportController linkControllerCbu;
	
	private CdrCbuPojo cdrCbuPojo;

	private CdrCbu cdrCbu;

	private List<CodeList> testNameList;

	private Orders orders;

	private OrderPojo orderPojo;

	private PatientLab patientLab;

	private PatientLabPojo patientLabPojo;
	
	private CBUploadInfo cbUploadInfo;
	
	private CBUploadInfoPojo cbUploadInfoPojo;
	private Long cbuCheck;
	
	private Long[] checkbox;	
	
	
	private List<PendingOrdersData> colValuesList;
	
	private List searchDataList;
	
	private String pkOrderType;
	
	private String subtype;
	private CBBController cbbController;
	
	private String orderType;
	private String orderStatus;
	private String reqDate;
	private String lstUpdDate;
	private String orderId;
	private String cbuId;
	private List<CodeList> orderTypeList;
	private List orderDetailsList;
	private List ctOrderDetailsList;
	private List unitReportList;

	private String statReason;
	private String pkcord;
	private String pkTempUnavail;
	private String nmdpcbuid;
	private String iscordavailfornmdp;
	private Long categoryName;
	private Long labTestName;	
    private List antigensList;	
    private String pkCanceled;
    private EntityStatusReasonPojo reasonPojo;
    private Boolean licenceUpdate;
    private List<EntityStatusReasonPojo> reasonPojos;    
    private String modifyUnitrpt;
    private String modifyDoc;
    private Long cbuCordId;
    private Long attachmentId;
    private String revokeToId;
    private String cbuRegisteryId;
    private List<Attachment> attachmentInfoLst; 
    private List<CBUUnitReport> cbuUnitReportLst;
    private String msgShipDt;
    private String pkresolved;
    private String pkavailable;
    private List cordentryprogress;
    private List naCbuList;
    private String pknotavailable;
    private CBUController cbuController;
    private String registryId;
    private PaginateSearch paginateSearch;
    private PaginateSearch paginationSearch;
    private PaginateSearch paginationSearch1;
	//    private List<TaskListPojo> workFlowNotiLst;
//    private List cbbProfileLst;
//    private List ordersLst;  
    private EligibilityDeclPojo declPojo;
    private List assigntoLst;
    private UserJB userObject;
    private EntityStatusPojo entityStatuspojo;
    private String cancelled_ack_orders;
    private CBUAction cbuaction;
    private String pfOrdertype;
	private String pendingFilter;
    private String pfSiteId;
    private List<ActiveOrderPojo> activeOrderPojoLst; 
    private List<ActiveOrderPojo> pcbbidACP;
    private List<ActiveOrderPojo> pcbbsateACP;
	private List<ActiveOrderPojo> pcburegACP;
	private List<ActiveOrderPojo> pcbulocACP;
	private List<ActiveOrderPojo> prequesttypeACP;
	private List<ActiveOrderPojo> ppriorityACP;
	private List<ActiveOrderPojo> prequestdateACP;
	private List<ActiveOrderPojo> pnoofdaysACP;
	private List<ActiveOrderPojo> pstatusACP;
	private List<ActiveOrderPojo> packresolACP;
	private List<ActiveOrderPojo> presolutionACP;
	private List<ActiveOrderPojo> passigntoACP;
	private List<ActiveOrderPojo> previewedbyACP;
	private List<OtherOrdersPojo> otherNACbuPojoLst;
	private List<OtherOrdersPojo> otherSvdInProgLst;
	private String siteSeq;
	private String satellite;
	private String regId;
	private String locId;
	private String reqType;
	private String priority;
	private String status;
	private String ackresolution;
	private String assignto;
	private String resolution;
	private String reviewby;
	private String noofdays;
	private String cancelreason;
	private String sampleAtlab;
	private List<OtherOrdersPojo> naSiteLst;
	private List<OtherOrdersPojo> naregidLst;
	private List<OtherOrdersPojo> nalocidLst;
	private List<OtherOrdersPojo> nastatusLst;
	private List<OtherOrdersPojo> nareasonLst;
	private List<OtherOrdersPojo> naremovedByLst;
	private List<OtherOrdersPojo> sipRegidLst;
	private List<OtherOrdersPojo> sipLocidLst;
	private List<OtherOrdersPojo> sipSiteidLst;
	private List<OtherOrdersPojo> sipPercentageLst;
	private String nasiteId;
	private String naregId;
	private String nalocId;
	private String nastatus;
	private String nareason;
	private String naremovedby;
	private String sipsiteId;
	private String sipregId;
	private String siplocId;
	private String sippercent;
	private String sipnoofdays;
	private String pStartDt;
	private String pEndDt;
	private String sipStartDt;
	private String sipEndDt;
    private List<CodeList> docCategWithRights;
    private String userPrimaryOrg;
    private FilterDataPojo filterdatapojo;
    private List<FilterDataPojo> customFilter;
    private List<FilterDataPojo> customFiltersip;
    private List<FilterDataPojo> customFilternacbu;
    private List assignUsrLst;
    private String pfSampleAtlab;
    private Boolean foruploadDoc = false;
    private Long FCRCompletedFlag;
    private Long MCCompletedFlag;
    private Long CBUAssessmentFlag;
    private OrderPojo orderpPojo;
    private CBUFinalReviewPojo cbuFinalReviewPojo;
    private CBUCompleteReqInfoPojo cbuCompleteReqInfoPojo;
    private List commonUsrLst;
    private List labelList;
    

	public List getCommonUsrLst() {
		return commonUsrLst;
	}

	public void setCommonUsrLst(List commonUsrLst) {
		this.commonUsrLst = commonUsrLst;
	}

       
    public CBUFinalReviewPojo getCbuFinalReviewPojo() {
		return cbuFinalReviewPojo;
	}


	public void setCbuFinalReviewPojo(CBUFinalReviewPojo cbuFinalReviewPojo) {
		this.cbuFinalReviewPojo = cbuFinalReviewPojo;
	}


	public CBUCompleteReqInfoPojo getCbuCompleteReqInfoPojo() {
		return cbuCompleteReqInfoPojo;
	}


	public void setCbuCompleteReqInfoPojo(CBUCompleteReqInfoPojo cbuCompleteReqInfoPojo) {
		this.cbuCompleteReqInfoPojo = cbuCompleteReqInfoPojo;
	}


	public OrderPojo getOrderpPojo() {
		return orderpPojo;
	}


	public List getLabelList() {
		return labelList;
	}

	public void setLabelList(List labelList) {
		this.labelList = labelList;
	}

	public void setOrderpPojo(OrderPojo orderpPojo) {
		this.orderpPojo = orderpPojo;
	}


	public Long getFCRCompletedFlag() {
		return FCRCompletedFlag;
	}


	public void setFCRCompletedFlag(Long fCRCompletedFlag) {
		FCRCompletedFlag = fCRCompletedFlag;
	}


	public Long getMCCompletedFlag() {
		return MCCompletedFlag;
	}


	public void setMCCompletedFlag(Long mCCompletedFlag) {
		MCCompletedFlag = mCCompletedFlag;
	}


	public Long getCBUAssessmentFlag() {
		return CBUAssessmentFlag;
	}


	public void setCBUAssessmentFlag(Long cBUAssessmentFlag) {
		CBUAssessmentFlag = cBUAssessmentFlag;
	}


	public Boolean getForuploadDoc() {
		return foruploadDoc;
	}


	public void setForuploadDoc(Boolean foruploadDoc) {
		this.foruploadDoc = foruploadDoc;
	}


	public String getPfSampleAtlab() {
		return pfSampleAtlab;
	}


	public void setPfSampleAtlab(String pfSampleAtlab) {
		this.pfSampleAtlab = pfSampleAtlab;
	}


	public List getAssignUsrLst() {
		return assignUsrLst;
	}


	public void setAssignUsrLst(List assignUsrLst) {
		this.assignUsrLst = assignUsrLst;
	}


	public List<FilterDataPojo> getCustomFiltersip() {
		return customFiltersip;
	}


	public List<FilterDataPojo> getCustomFilternacbu() {
		return customFilternacbu;
	}


	public void setCustomFiltersip(List<FilterDataPojo> customFiltersip) {
		this.customFiltersip = customFiltersip;
	}


	public void setCustomFilternacbu(List<FilterDataPojo> customFilternacbu) {
		this.customFilternacbu = customFilternacbu;
	}

	
    
    
    public List<FilterDataPojo> getCustomFilter() {
		return customFilter;
	}


	public void setCustomFilter(List<FilterDataPojo> customFilter) {
		this.customFilter = customFilter;
	}


	public FilterDataPojo getFilterdatapojo() {
		return filterdatapojo;
	}


	public void setFilterdatapojo(FilterDataPojo filterdatapojo) {
		this.filterdatapojo = filterdatapojo;
	}


	public String getUserPrimaryOrg() {
		return userPrimaryOrg;
	}


	public void setUserPrimaryOrg(String userPrimaryOrg) {
		this.userPrimaryOrg = userPrimaryOrg;
	}
	
	
	public List<CodeList> getDocCategWithRights() {
		return docCategWithRights;
	}

	public void setDocCategWithRights(List<CodeList> docCategWithRights) {
		this.docCategWithRights = docCategWithRights;
	}

	public String getSipStartDt() {
		return sipStartDt;
	}

	public String getSipEndDt() {
		return sipEndDt;
	}

	public void setSipStartDt(String sipStartDt) {
		this.sipStartDt = sipStartDt;
	}

	public void setSipEndDt(String sipEndDt) {
		this.sipEndDt = sipEndDt;
	}
	public String getpEndDt() {
		return pEndDt;
	}

	public void setpEndDt(String pEndDt) {
		this.pEndDt = pEndDt;
	}

	public String getpStartDt() {
		return pStartDt;
	}

	public void setpStartDt(String pStartDt) {
		this.pStartDt = pStartDt;
	}

		
	public String getCancelreason() {
		return cancelreason;
	}

	public void setCancelreason(String cancelreason) {
		this.cancelreason = cancelreason;
	}

	public String getSipnoofdays() {
		return sipnoofdays;
	}

	public String getSampleAtlab() {
		return sampleAtlab;
	}

	public void setSampleAtlab(String sampleAtlab) {
		this.sampleAtlab = sampleAtlab;
	}

	public void setSipnoofdays(String sipnoofdays) {
		this.sipnoofdays = sipnoofdays;
	}

	public String getNasiteId() {
		return nasiteId;
	}

	public String getNaregId() {
		return naregId;
	}

	public String getNalocId() {
		return nalocId;
	}

	public String getNastatus() {
		return nastatus;
	}

	public String getNareason() {
		return nareason;
	}

	public String getNaremovedby() {
		return naremovedby;
	}

	public String getSipsiteId() {
		return sipsiteId;
	}

	public String getSipregId() {
		return sipregId;
	}

	public String getSiplocId() {
		return siplocId;
	}

	public String getSippercent() {
		return sippercent;
	}

	public void setNasiteId(String nasiteId) {
		this.nasiteId = nasiteId;
	}

	public void setNaregId(String naregId) {
		this.naregId = naregId;
	}

	public void setNalocId(String nalocId) {
		this.nalocId = nalocId;
	}

	public void setNastatus(String nastatus) {
		this.nastatus = nastatus;
	}

	public void setNareason(String nareason) {
		this.nareason = nareason;
	}

	public void setNaremovedby(String naremovedby) {
		this.naremovedby = naremovedby;
	}

	public void setSipsiteId(String sipsiteId) {
		this.sipsiteId = sipsiteId;
	}

	public void setSipregId(String sipregId) {
		this.sipregId = sipregId;
	}

	public void setSiplocId(String siplocId) {
		this.siplocId = siplocId;
	}

	public void setSippercent(String sippercent) {
		this.sippercent = sippercent;
	}

	public String getNoofdays() {
		return noofdays;
	}

	public void setNoofdays(String noofdays) {
		this.noofdays = noofdays;
	}

	public String getSiteSeq() {
		return siteSeq;
	}

	public void setSiteSeq(String siteSeq) {
		this.siteSeq = siteSeq;
	}

	public String getSatellite() {
		return satellite;
	}

	public void setSatellite(String satellite) {
		this.satellite = satellite;
	}

	public String getRegId() {
		return regId;
	}

	public void setRegId(String regId) {
		this.regId = regId;
	}

	public String getLocId() {
		return locId;
	}

	public void setLocId(String locId) {
		this.locId = locId;
	}

	public String getReqType() {
		return reqType;
	}

	public void setReqType(String reqType) {
		this.reqType = reqType;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAckresolution() {
		return ackresolution;
	}

	public void setAckresolution(String ackresolution) {
		this.ackresolution = ackresolution;
	}

	public String getAssignto() {
		return assignto;
	}

	public void setAssignto(String assignto) {
		this.assignto = assignto;
	}

	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	public String getReviewby() {
		return reviewby;
	}

	public void setReviewby(String reviewby) {
		this.reviewby = reviewby;
	}

	public List<ActiveOrderPojo> getPcbbidACP() {
		return pcbbidACP;
	}

	public void setPcbbidACP(List<ActiveOrderPojo> pcbbidACP) {
		this.pcbbidACP = pcbbidACP;
	}

	public List<ActiveOrderPojo> getPcbbsateACP() {
		return pcbbsateACP;
	}

	public void setPcbbsateACP(List<ActiveOrderPojo> pcbbsateACP) {
		this.pcbbsateACP = pcbbsateACP;
	}

	public List<ActiveOrderPojo> getPcburegACP() {
		return pcburegACP;
	}

	public void setPcburegACP(List<ActiveOrderPojo> pcburegACP) {
		this.pcburegACP = pcburegACP;
	}

	public List<ActiveOrderPojo> getPcbulocACP() {
		return pcbulocACP;
	}

	public void setPcbulocACP(List<ActiveOrderPojo> pcbulocACP) {
		this.pcbulocACP = pcbulocACP;
	}

	public List<ActiveOrderPojo> getPrequesttypeACP() {
		return prequesttypeACP;
	}

	public void setPrequesttypeACP(List<ActiveOrderPojo> prequesttypeACP) {
		this.prequesttypeACP = prequesttypeACP;
	}

	public List<ActiveOrderPojo> getPpriorityACP() {
		return ppriorityACP;
	}

	public void setPpriorityACP(List<ActiveOrderPojo> ppriorityACP) {
		this.ppriorityACP = ppriorityACP;
	}

	public List<ActiveOrderPojo> getPrequestdateACP() {
		return prequestdateACP;
	}

	public void setPrequestdateACP(List<ActiveOrderPojo> prequestdateACP) {
		this.prequestdateACP = prequestdateACP;
	}

	public List<ActiveOrderPojo> getPnoofdaysACP() {
		return pnoofdaysACP;
	}

	public void setPnoofdaysACP(List<ActiveOrderPojo> pnoofdaysACP) {
		this.pnoofdaysACP = pnoofdaysACP;
	}

	public List<ActiveOrderPojo> getPstatusACP() {
		return pstatusACP;
	}

	public void setPstatusACP(List<ActiveOrderPojo> pstatusACP) {
		this.pstatusACP = pstatusACP;
	}

	public List<ActiveOrderPojo> getPackresolACP() {
		return packresolACP;
	}

	public void setPackresolACP(List<ActiveOrderPojo> packresolACP) {
		this.packresolACP = packresolACP;
	}

	public List<ActiveOrderPojo> getPresolutionACP() {
		return presolutionACP;
	}

	public void setPresolutionACP(List<ActiveOrderPojo> presolutionACP) {
		this.presolutionACP = presolutionACP;
	}

	public List<ActiveOrderPojo> getPassigntoACP() {
		return passigntoACP;
	}

	public void setPassigntoACP(List<ActiveOrderPojo> passigntoACP) {
		this.passigntoACP = passigntoACP;
	}

	public List<ActiveOrderPojo> getPreviewedbyACP() {
		return previewedbyACP;
	}

	public void setPreviewedbyACP(List<ActiveOrderPojo> previewedbyACP) {
		this.previewedbyACP = previewedbyACP;
	}

	public List<ActiveOrderPojo> getActiveOrderPojoLst() {
		return activeOrderPojoLst;
	}

	public void setActiveOrderPojoLst(List<ActiveOrderPojo> activeOrderPojoLst) {
		this.activeOrderPojoLst = activeOrderPojoLst;
	}

	public String getPfOrdertype() {
		return pfOrdertype;
	}

	public void setPfOrdertype(String pfOrdertype) {
		this.pfOrdertype = pfOrdertype;
	}

	public String getPendingFilter() {
		return pendingFilter;
	}

	public void setPendingFilter(String pendingFilter) {
		this.pendingFilter = pendingFilter;
	}

	public String getPfSiteId() {
		return pfSiteId;
	}

	public void setPfSiteId(String pfSiteId) {
		this.pfSiteId = pfSiteId;
	}
    
    
    public CBUAction getCbuaction() {
		return cbuaction;
	}

	public void setCbuaction(CBUAction cbuaction) {
		this.cbuaction = cbuaction;
	}

	public String getCancelled_ack_orders() {
		return cancelled_ack_orders;
	}

	public void setCancelled_ack_orders(String cancelled_ack_orders) {
		this.cancelled_ack_orders = cancelled_ack_orders;
	}

	public EntityStatusPojo getEntityStatuspojo() {
		return entityStatuspojo;
	}

	public void setEntityStatuspojo(EntityStatusPojo entityStatuspojo) {
		this.entityStatuspojo = entityStatuspojo;
	}

	public List getAssigntoLst() {
		return assigntoLst;
	}

	public void setAssigntoLst(List assigntoLst) {
		this.assigntoLst = assigntoLst;
	}

	public PaginateSearch getPaginationSearch1() {
		return paginationSearch1;
	}

	public void setPaginationSearch1(PaginateSearch paginationSearch1) {
		this.paginationSearch1 = paginationSearch1;
	}
    public PaginateSearch getPaginationSearch() {
		return paginationSearch;
	}

	public void setPaginationSearch(PaginateSearch paginationSearch) {
		this.paginationSearch = paginationSearch;
	}
    
	public PaginateSearch getPaginateSearch() {
		return paginateSearch;
	}

	public void setPaginateSearch(PaginateSearch paginateSearch) {
		this.paginateSearch = paginateSearch;
	}

	public EligibilityDeclPojo getDeclPojo() {
		return declPojo;
	}

	public void setDeclPojo(EligibilityDeclPojo declPojo) {
		this.declPojo = declPojo;
	}

	public String getPknotavailable() {
		return pknotavailable;
	}

	public void setPknotavailable(String pknotavailable) {
		this.pknotavailable = pknotavailable;
	}

	public List getNaCbuList() {
		return naCbuList;
	}

	public void setNaCbuList(List naCbuList) {
		this.naCbuList = naCbuList;
	}

	public List getCordentryprogress() {
		return cordentryprogress;
	}

	public void setCordentryprogress(List cordentryprogress) {
		this.cordentryprogress = cordentryprogress;
	}

	/*public List getCbbProfileLst() {
		return cbbProfileLst;
	}

	public void setCbbProfileLst(List cbbProfileLst) {
		this.cbbProfileLst = cbbProfileLst;
	}

	public List getOrdersLst() {
		return ordersLst;
	}

	public void setOrdersLst(List ordersLst) {
		this.ordersLst = ordersLst;
	}*/
    public String getMsgShipDt() {
		return msgShipDt;
	}

	public void setMsgShipDt(String msgShipDt) {
		this.msgShipDt = msgShipDt;
	}
    public String getPkresolved() {
		return pkresolved;
	}

	public void setPkresolved(String pkresolved) {
		this.pkresolved = pkresolved;
	}

	public String getPkavailable() {
		return pkavailable;
	}

	public void setPkavailable(String pkavailable) {
		this.pkavailable = pkavailable;
	}

	public String getCbuRegisteryId() {
		return cbuRegisteryId;
	}

	public void setCbuRegisteryId(String cbuRegisteryId) {
		this.cbuRegisteryId = cbuRegisteryId;
	}

	public String getRevokeToId() {
		return revokeToId;
	}

	public void setRevokeToId(String revokeToId) {
		this.revokeToId = revokeToId;
	}

	public Long getCbuCordId() {
		return cbuCordId;
	}

	public void setCbuCordId(Long cbuCordId) {
		this.cbuCordId = cbuCordId;
	}

	public Long getAttachmentId() {
		return attachmentId;
	}

	public void setAttachmentId(Long attachmentId) {
		this.attachmentId = attachmentId;
	}

	public String getModifyUnitrpt() {
		return modifyUnitrpt;
	}

	public void setModifyUnitrpt(String modifyUnitrpt) {
		this.modifyUnitrpt = modifyUnitrpt;
	}

	public List<EntityStatusReasonPojo> getReasonPojos() {
		return reasonPojos;
	}

	public void setReasonPojos(List<EntityStatusReasonPojo> reasonPojos) {
		this.reasonPojos = reasonPojos;
	}

	public Boolean getLicenceUpdate() {
		return licenceUpdate;
	}

	public void setLicenceUpdate(Boolean licenceUpdate) {
		this.licenceUpdate = licenceUpdate;
	}
	
	public void setUnitReportList(List list) {
		// TODO Auto-generated method stub
		this.unitReportList = list;
	}
	public List getUnitReportList() {
		// TODO Auto-generated method stub
		return unitReportList;
	}
	

	public EntityStatusReasonPojo getReasonPojo() {
		return reasonPojo;
	}

	public void setReasonPojo(EntityStatusReasonPojo reasonPojo) {
		this.reasonPojo = reasonPojo;
	}

	public String getPkCanceled() {
		return pkCanceled;
	}

	public void setPkCanceled(String pkCanceled) {
		this.pkCanceled = pkCanceled;
	}

	public List getAntigensList() {
		return antigensList;
	}

	public void setAntigensList(List antigensList) {
		this.antigensList = antigensList;
	}

	public String getPkTempUnavail() {
		return pkTempUnavail;
	}

	public void setPkTempUnavail(String pkTempUnavail) {
		this.pkTempUnavail = pkTempUnavail;
	}
    
	public Long getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(Long categoryName) {
		this.categoryName = categoryName;
	}

	public Long getLabTestName() {
		return labTestName;
	}

	public void setLabTestName(Long labTestName) {
		this.labTestName = labTestName;
	}

	private Map<String, String> cancelids;
	
	public Map<String, String> getCancelids() {
		return cancelids;
	}

	public void setCancelids(Map<String, String> cancelids) {
		this.cancelids = cancelids;
	}

	public CBUUnitReportPojo getCbuUnitReportPojo() {
		return cbuUnitReportPojo;
	}
	public void setCbuUnitReportPojo(CBUUnitReportPojo cbuUnitReportPojo) {
		this.cbuUnitReportPojo = cbuUnitReportPojo;
	}
	
	
	
	public String getIscordavailfornmdp() {
		return iscordavailfornmdp;
	}

	public void setIscordavailfornmdp(String iscordavailfornmdp) {
		this.iscordavailfornmdp = iscordavailfornmdp;
	}
	
	public List getCtOrderDetailsList() {
		return ctOrderDetailsList;
	}

	public void setCtOrderDetailsList(List ctOrderDetailsList) {
		this.ctOrderDetailsList = ctOrderDetailsList;
	}

	public List getOrderDetailsList() {
		return orderDetailsList;
	}

	public void setOrderDetailsList(List orderDetailsList) {
		this.orderDetailsList = orderDetailsList;
	}

	public List<CodeList> getOrderTypeList() {
		return orderTypeList;
	}

	public void setOrderTypeList(List<CodeList> orderTypeList) {
		this.orderTypeList = orderTypeList;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setCbuId(String cbuId) {
		this.cbuId = cbuId;
	}

	public String getCbuId() {
		return cbuId;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getReqDate() {
		return reqDate;
	}

	public void setReqDate(String reqDate) {
		this.reqDate = reqDate;
	}

	public String getLstUpdDate() {
		return lstUpdDate;
	}

	public void setLstUpdDate(String lstUpdDate) {
		this.lstUpdDate = lstUpdDate;
	}

	public String getPkOrderType() {
		return pkOrderType;
	}

	public void setPkOrderType(String pkOrderType) {
		this.pkOrderType = pkOrderType;
	}

	public List getSearchDataList() {
		return searchDataList;
	}

	public void setSearchDataList(List searchDataList) {
		this.searchDataList = searchDataList;
	}

	public List<PendingOrdersData> getColValuesList() {
		return colValuesList;
	}

	public void setColValuesList(List<PendingOrdersData> colValuesList) {
		this.colValuesList = colValuesList;
	}

	public Long getCbuCheck() {
		return cbuCheck;
	}

	public void setCbuCheck(Long cbuCheck) {
		this.cbuCheck = cbuCheck;
	}

	public Long[] getCheckbox() {
		return checkbox;
	}

	public void setCheckbox(Long[] checkbox) {
		this.checkbox = checkbox;
	}
	
	
	

	

	/*public List<TaskListPojo> getWorkFlowNotiLst() {
		if(workFlowNotiLst==null){
			workFlowNotiLst = new ArrayList<TaskListPojo>();
		}
		return workFlowNotiLst;
	}

	public void setWorkFlowNotiLst(List<TaskListPojo> workFlowNotiLst) {
		this.workFlowNotiLst = workFlowNotiLst;
	}*/

	public String getStatReason() {
		return statReason;
	}

	public void setStatReason(String statReason) {
		this.statReason = statReason;
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getOpenOrder() throws Exception {
		return "success";
	}


	public OpenOrderAction() {
		if(null != getSession(getRequest())){
		velosSearchController = new VelosSearchController();
		cdrCbuPojo = new CdrCbuPojo();
		orderAttachmentPojo = new OrderAttachmentPojo();
		codelstController=new CodelstController();
		labCategoryPojo = new LabCategoryPojo();
		labCategory = new LabCategory();
		labMaint = new LabMaint();
		orders = new Orders();
		cdrCbu =new CdrCbu();
		codeList=new CodeList();
		orderPojo = new OrderPojo();
		searchPojo=new SearchPojo();
		patientLabPojo = new PatientLabPojo();
		patientLab = new PatientLab();
		filterdatapojo=new FilterDataPojo();
		linkController = new OpenOrderController();
		linkControllerCbu = new CBUUnitReportController();
		cbUploadInfo=new CBUploadInfo();
		cbUploadInfoPojo=new CBUploadInfoPojo();
		cbbController =new CBBController();
		cbuUnitReportPojo = new CBUUnitReportPojo();
		setCbuController(new CBUController());
		paginateSearch = new PaginateSearch();
		paginationSearch=new PaginateSearch();
		paginationSearch1=new PaginateSearch();
		activeOrderPojoLst=new ArrayList<ActiveOrderPojo>();
		entityStatuspojo=new EntityStatusPojo();
		cbuaction=new CBUAction();
		pcbbidACP=new ArrayList<ActiveOrderPojo>();
		pcbbsateACP=new ArrayList<ActiveOrderPojo>();
		pcburegACP=new ArrayList<ActiveOrderPojo>();
		pcbulocACP=new ArrayList<ActiveOrderPojo>();
		prequesttypeACP=new ArrayList<ActiveOrderPojo>();
		ppriorityACP=new ArrayList<ActiveOrderPojo>();
		prequestdateACP=new ArrayList<ActiveOrderPojo>();
		pnoofdaysACP=new ArrayList<ActiveOrderPojo>();
		packresolACP=new ArrayList<ActiveOrderPojo>();
		presolutionACP=new ArrayList<ActiveOrderPojo>();
		passigntoACP=new ArrayList<ActiveOrderPojo>();
		previewedbyACP=new ArrayList<ActiveOrderPojo>();
		pstatusACP=new ArrayList<ActiveOrderPojo>();
		otherSvdInProgLst=new ArrayList<OtherOrdersPojo>();
		otherNACbuPojoLst=new ArrayList<OtherOrdersPojo>();
		naSiteLst=new ArrayList<OtherOrdersPojo>();
		naregidLst=new ArrayList<OtherOrdersPojo>();
		nalocidLst=new ArrayList<OtherOrdersPojo>();
		nastatusLst=new ArrayList<OtherOrdersPojo>();
		nareasonLst=new ArrayList<OtherOrdersPojo>();
		naremovedByLst=new ArrayList<OtherOrdersPojo>();
		sipLocidLst=new ArrayList<OtherOrdersPojo>();
		sipPercentageLst=new ArrayList<OtherOrdersPojo>();
		sipRegidLst=new ArrayList<OtherOrdersPojo>();
		sipSiteidLst=new ArrayList<OtherOrdersPojo>();
		categoryDivMap.put(getCodeListPkByTypeAndSubtype(CATEGORY, LAB_SUMMARY_CODESUBTYPE), DOC_LABSUMMARY_DIV);
	    categoryDivMap.put(getCodeListPkByTypeAndSubtype(CATEGORY, HEALTH_HISt_SCR_CODESUBTYPE), DOC_FORMS_DIV);
	    categoryDivMap.put(getCodeListPkByTypeAndSubtype(CATEGORY, OTHER_REC_CODESUBTYPE), DOC_OTHERS_DIV);
	    categoryDivMap.put(getCodeListPkByTypeAndSubtype(CATEGORY, HLA_NOTE_CATEGORY_CODESUBTYPE), DOC_HLA_DIV);
	    categoryDivMap.put(getCodeListPkByTypeAndSubtype(CATEGORY, ELIGIBILITY_CODESUBTYPE), DOC_ELIGIBILITY_DIV);
	    categoryDivMap.put(getCodeListPkByTypeAndSubtype(CATEGORY, PROCESS_NOTE_CATEGORY_CODESUBTYPE), DOC_PROCESSING_DIV);
	    categoryDivMap.put(getCodeListPkByTypeAndSubtype(CATEGORY, INFECT_DIS_MARK_CODESUBTYPE), DOC_IDM_DIV);
	    categoryDivMap.put(getCodeListPkByTypeAndSubtype(CATEGORY, CBU_INFORMATION_CODESUBTYPE), DOC_CBUCHARACTERISTIC_DIV);
	    orderPojo=new OrderPojo();
	    cbuFinalReviewPojo=new CBUFinalReviewPojo();
	    cbuCompleteReqInfoPojo=new CBUCompleteReqInfoPojo();
		}
	}

	public List<OtherOrdersPojo> getSipRegidLst() {
		return sipRegidLst;
	}

	public List<OtherOrdersPojo> getSipLocidLst() {
		return sipLocidLst;
	}

	public List<OtherOrdersPojo> getSipSiteidLst() {
		return sipSiteidLst;
	}

	public List<OtherOrdersPojo> getSipPercentageLst() {
		return sipPercentageLst;
	}

	public void setSipRegidLst(List<OtherOrdersPojo> sipRegidLst) {
		this.sipRegidLst = sipRegidLst;
	}

	public void setSipLocidLst(List<OtherOrdersPojo> sipLocidLst) {
		this.sipLocidLst = sipLocidLst;
	}

	public void setSipSiteidLst(List<OtherOrdersPojo> sipSiteidLst) {
		this.sipSiteidLst = sipSiteidLst;
	}

	public void setSipPercentageLst(List<OtherOrdersPojo> sipPercentageLst) {
		this.sipPercentageLst = sipPercentageLst;
	}

	public List<OtherOrdersPojo> getOtherSvdInProgLst() {
		return otherSvdInProgLst;
	}

	public void setOtherSvdInProgLst(List<OtherOrdersPojo> otherSvdInProgLst) {
		this.otherSvdInProgLst = otherSvdInProgLst;
	}
	


	public List<OtherOrdersPojo> getNaSiteLst() {
		return naSiteLst;
	}

	public List<OtherOrdersPojo> getNaregidLst() {
		return naregidLst;
	}

	public List<OtherOrdersPojo> getNalocidLst() {
		return nalocidLst;
	}

	public List<OtherOrdersPojo> getNastatusLst() {
		return nastatusLst;
	}

	public List<OtherOrdersPojo> getNareasonLst() {
		return nareasonLst;
	}

	public List<OtherOrdersPojo> getNaremovedByLst() {
		return naremovedByLst;
	}

	public void setNaSiteLst(List<OtherOrdersPojo> naSiteLst) {
		this.naSiteLst = naSiteLst;
	}

	public void setNaregidLst(List<OtherOrdersPojo> naregidLst) {
		this.naregidLst = naregidLst;
	}

	public void setNalocidLst(List<OtherOrdersPojo> nalocidLst) {
		this.nalocidLst = nalocidLst;
	}

	public void setNastatusLst(List<OtherOrdersPojo> nastatusLst) {
		this.nastatusLst = nastatusLst;
	}

	public void setNareasonLst(List<OtherOrdersPojo> nareasonLst) {
		this.nareasonLst = nareasonLst;
	}

	public void setNaremovedByLst(List<OtherOrdersPojo> naremovedByLst) {
		this.naremovedByLst = naremovedByLst;
	}

	public List<OtherOrdersPojo> getOtherNACbuPojoLst() {
		return otherNACbuPojoLst;
	}

	public void setOtherNACbuPojoLst(List<OtherOrdersPojo> otherNACbuPojoLst) {
		this.otherNACbuPojoLst = otherNACbuPojoLst;
	}


	
	public List getTestNameList() {
		return testNameList;
	}
	
	public void setTestNameList(List testNameList) {
		this.testNameList = testNameList;
	}

	public OrderAttachmentPojo getOrderAttachmentPojo() {
		return orderAttachmentPojo;
	}

	public void setOrderAttachmentPojo(OrderAttachmentPojo orderAttachmentPojo) {
		this.orderAttachmentPojo = orderAttachmentPojo;
	}

	public LabCategoryPojo getLabCategoryPojo() {
		return labCategoryPojo;
	}

	public void setLabCategoryPojo(LabCategoryPojo labCategoryPojo) {
		this.labCategoryPojo = labCategoryPojo;
	}

	public LabMaintPojo getLabMaintPojo() {
		return labMaintPojo;
	}

	public void setLabMaintPojo(LabMaintPojo labMaintPojo) {
		this.labMaintPojo = labMaintPojo;
	}

	public Map getCodelstParam() {
		return codelstParam;
	}

	public void setCodelstParam(Map codelstParam) {
		this.codelstParam = codelstParam;
	}

	public VelosSearchController getVelosSearchController() {
		return velosSearchController;
	}

	public void setVelosSearchController(
			VelosSearchController velosSearchController) {
		this.velosSearchController = velosSearchController;
	}

	public PatientLabPojo getPatientLabPojo() {
		return patientLabPojo;
	}

	public void setPatientLabPojo(PatientLabPojo patienLabPojo) {
		this.patientLabPojo = patientLabPojo;
	}

	public List getClinicalTestList() {
		return clinicalTestList;
	}

	public void setClinicalTestList(List clinicalTestList) {
		this.clinicalTestList = clinicalTestList;
	}

	public CdrCbuPojo getCdrCbuPojo() {
		return cdrCbuPojo;
	}

	public List getLstObject() {
		return lstObject;
	}

	public void setLstObject(List lstObject) {
		this.lstObject = lstObject;
	}

	public SearchPojo getSearchPojo() {
		return searchPojo;
	}

	public void setSearchPojo(SearchPojo searchPojo) {
		this.searchPojo = searchPojo;
	}

	public void setCdrCbuPojo(CdrCbuPojo cdrCbuPojo) {
		this.cdrCbuPojo = cdrCbuPojo;
	}

	public List getOpenOrderList() {
		return openOrderList;
	}

	public List getCategoryList() {
		return categoryList;
	}

	public CBUploadInfoPojo getCbUploadInfoPojo() {
		return cbUploadInfoPojo;
	}

	public void setCbUploadInfoPojo(CBUploadInfoPojo cbUploadInfoPojo) {
		this.cbUploadInfoPojo = cbUploadInfoPojo;
	}

	public void setCategoryList(List categoryList) {
		this.categoryList = categoryList;
	}

	public void setOpenOrderList(List openOrderList) {
		this.openOrderList = openOrderList;
	}

	public AttachmentPojo getAttachmentPojo() {
		return attachmentPojo;
	}

	public String getSubtype() {
		return subtype;
	}

	public void setSubtype(String subtype) {
		this.subtype = subtype;
		//log.debug("Subtype::::"+subtype);
	}

	public void setAttachmentPojo(AttachmentPojo attachmentPojo) {
		this.attachmentPojo = attachmentPojo;
	}

	public OrderPojo getOrderPojo() {
		return orderPojo;
	}

	public void setOrderPojo(OrderPojo orderPojo) {
		this.orderPojo = orderPojo;
	}
	
	
	
	
	public String getModifyDoc() {
		return modifyDoc;
	}

	public void setModifyDoc(String modifyDoc) {
		this.modifyDoc = modifyDoc;
	}

	public String getPkcord() {
		return pkcord;
	}

	public void setPkcord(String pkcord) {
		this.pkcord = pkcord;
	}
	
	public String getDescriptionUploadInfo(Long attachmentID) throws Exception{
		return CBUUnitReportHelper.getDescriptionUploadInfo(attachmentID);
	}
	
	

	public String getNmdpcbuid() {
		return nmdpcbuid;
	}

	public void setNmdpcbuid(String nmdpcbuid) {
		this.nmdpcbuid = nmdpcbuid;
	}

	public String searchCbuDetails() throws Exception {

		codelstParam = new HashMap<String, String>();
		searchPojo = new SearchPojo();
		searchPojo.setCodelstParam(codelstParam);
		searchPojo.setSearchType(CDRCBU_OBJECTNAME);
		////log.debug("before criteria construction");
		String criteria = constructCriteria();
		////log.debug("after criteria construction---1" + criteria);
		searchPojo.setCriteria(criteria);
		// //log.debug("after criteria---2");
		try {
			
			searchPojo = velosSearchController.velosSearch(searchPojo);
			 ////log.debug("searchpojo--->>>>>" + searchPojo.getResultlst());
		} catch (Exception exArg) {
			log.error(exArg.getMessage());
			exArg.printStackTrace();
		}
		
		setLstObject(searchPojo.getResultlst());
		
	
		return "success";
	}
	private String constructCriteria() {
		String criteria = " ";
		//log.debug("inside constructCriteria");
		cdrCbuPojo = getCdrCbuPojo();
		UserJB userObject=(UserJB)getRequest().getSession().getAttribute(MidErrorConstants.GARUDA_USER);
		String filterVal = Utilities.removeDelimeters(cdrCbuPojo.getCdrCbuId().trim(),"-");
		
		if (cdrCbuPojo != null) {
			
			if (cdrCbuPojo.getCdrCbuId() != null
					&& !cdrCbuPojo.getCdrCbuId().equals("")) {
				criteria += " or replace(lower(registryId),'-','' ) like lower('%"
						+ filterVal + "%')";
			
				criteria += " or lower(numberOnCbuBag) like lower('%"
						+ filterVal + "%')";
		
				criteria += " or lower(localCbuId) like lower('%"
						+ filterVal + "%')";
				
				criteria += " or replace(lower(registryMaternalId),'-','') like lower('%"
					+ filterVal + "%')";
				criteria += " or lower(localMaternalId) like lower('%"
					+ filterVal + "%')";
				criteria += " or lower(cordIsbiDinCode) like lower('%"
					+ filterVal + "%')";
				//log.debug("criteria cbuid");
			}
			
			
			int index = criteria.indexOf("or");
			if (index != -1) {
				criteria = criteria.substring(index + 2);
				criteria = "("+criteria+")";
			}
			
			
			// criteria = " where (deletedFlag = 0 or deletedFlag is null) " +
			// criteria;
		}
		if (cdrCbuPojo.getCdrCbuId() != null
				&& !cdrCbuPojo.getCdrCbuId().equals("")){
		    criteria += " and ( cordSearchable = 1 or cordSearchable IS NULL )";
		    criteria +="and (fkCbbId in(select fkSite From UserSite where fkUser="+((Integer)userObject.getUserId()).longValue()+" and userRight <> 0)))";
		    
		    
		}else{
			criteria += "  ( cordSearchable = 1 or cordSearchable IS NULL )"; 
			criteria +="and (fkCbbId in(select fkSite From UserSite where fkUser="+((Integer)userObject.getUserId()).longValue()+" and userRight <> 0)))";
			
		}
		
		//log.debug("Criteria *****" + criteria);
		return criteria;
	}

	public String searchOpenOrderDetails() throws Exception {
		String registryID=null;
		setOpenOrderList(queryForOpenOrderList());
		 if(cdrCbuPojo.getCordID()!=null){
		     
				cdrCbuPojo = cbuController.getCordInfoById(cdrCbuPojo);
			}
	       
	         registryID = cdrCbuPojo.getRegistryId();
	         request.setAttribute("registryID", registryID);
	         setDocCategWithRights(setCategoriesWithGroupRights());
	       	 //log.debug("Registry id inside search!!!!!!!!!!!!" +registryID);
		//log.debug("CATEGORY list" + queryForCategoryList());
		//setCategoryList(queryForCategoryList());
		//log.debug("quey list" + getCategoryList().get(0).toString());

		return "success";
	}

	public String populateTestNames() throws Exception {

		setTestNameList(populateDropdownValues());

		//log.debug("query sub catlist" + getTestNameList().get(0).toString());
		return "success";
	}
	
	

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	

	public String saveOrderDetails() throws Exception {
		// getCordInfo();

		Long attachmentId = null;
		Long testEntityId = null;
		Long testEntityType = null;
		//String cbuId1 = getRequest().getParameter("cbuId");
		Long cbuId1=Long.parseLong(getRequest().getParameter("cbuCheck"));
		//log.debug("-----------Cord:::::::::::-------"+cbuId1);	
		getCdrCbuPojo().setCordID(cbuId1);        
		Long cordId = getCdrCbuPojo().getCordID();
		
        //log.debug("-----------cordId-------"+cordId);
      
        List<AttachmentPojo> attachments = null;
		// Get the attachment Id from attachment POJO
		if(getSession(getRequest()).getAttribute("attachmentList")!=null){
				
				attachments = (List<AttachmentPojo>)getSession(getRequest()).getAttribute("attachmentList");
				//log.debug("Attachment Open Order Action"+attachments.size());
				if(attachments.size()>0){
					//log.debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
					for(AttachmentPojo a : attachments){
						//log.debug("**********************************************************");
						a.setGarudaEntityId(getCdrCbuPojo().getCordID());
						
					}
					attachments = new AttachmentController().addMultipleAttachments(attachments);
					//getCbuUnitReportPojo().setAttachmentId(attachments.get(0).getAttachmentId());
					 if(getRequest().getParameter("categoryId").equals(cbbController.getCodeListPkIds(CATEGORY, UNIT_REPORT_CODESUBTYPE).toString())){
					    	getCbuUnitReportPojo().setAttachmentId(attachments.get(0).getAttachmentId());
					    	if(getCdrCbuPojo().getCordID()!=null)
							{
								getCbuUnitReportPojo().setFkCordCdrCbuId(getCdrCbuPojo().getCordID());
								//getCordHlaExtPojo().setFkCordCdrCbuId(getCdrCbuPojo().getCordID());
							}
					    	cbuUnitReportPojo = getCbuUnitReportPojo();
					    	try{
					    		if(getCbuUnitReportPojo().getAttachmentId()!=null){
					    		linkControllerCbu.saveCBUUnitReport(cbuUnitReportPojo);
					    		}
					    		}catch(Exception e){
					    			log.error(e.getMessage());
					    			e.printStackTrace();
					    		}
					    		getRequest().setAttribute("submit", "unitreport");
					    }
			    } 
					
				
		}
				
		
		//String testName = getLabMaintPojo().getLabTestName();
		//log.debug("Test Name:::::::::::::" + testName);
		
		String orderIds = getRequest().getParameter("orderid");
		//log.debug("In save order IDs method"+orderIds);
		
		String subCatIds = getRequest().getParameter("subcatid");
		//log.debug("In save method subCat "+ subCatIds);
		
		String categoryId = getRequest().getParameter("categoryId");
		//log.debug("In save method category "+ categoryId);
		getSession(getRequest()).setAttribute("categoryId", categoryId);
		// attachmentId = attachmentPojo.getAttachmentId();

		// Get the order Ids from JSP
		if ((orderIds != null && orderIds.length() > 0) && (categoryId == null || categoryId.equals(""))) {

			List orderList = new ArrayList();
			// orderAttachmentPojo.setFk_attachmentid(attachmentId);
			String orderids[] = orderIds.split(","); // 1,2,3 && attchId=100
			orderList = Arrays.asList(orderids);

			for (int i = 0; i < orderList.size(); i++) {
	
				getOrderAttachmentPojo().setFk_orderid(
						Long.parseLong(orderList.get(i).toString()));
				
				//Get the attachment information for every order Id
				if(attachments!=null) {
					for(int j=0;j<attachments.size();j++) {
						attachmentId = attachments.get(j).getAttachmentId();
						
						//log.debug("attachmentId::::::::::"+attachmentId);
							getOrderAttachmentPojo().setFk_attachmentId(attachmentId);
							
							orderAttachmentPojo = getOrderAttachmentPojo();
							try {
								linkController.saveOrderAttachment(orderAttachmentPojo);
								// to do set dropdown values
							} catch (Exception e) {
								log.error(e.getMessage());
								e.printStackTrace();
							}
						}
					}
				
				}
	    	}

		

		

if(!categoryId.equals("")) {
			

			//log.debug("IN Save CBUPLOAD INfo::::::::::;;;111111");
			getCbUploadInfoPojo().setFk_CategoryId(Long.parseLong(categoryId.toString()));
			if(subCatIds != null && !subCatIds.equals("")){
				getCbUploadInfoPojo().setFk_SubCategoryId(Long.parseLong(subCatIds.toString()));
			}
				
				if(getCbUploadInfoPojo().getStrcompletionDate() != null && !getCbUploadInfoPojo().getStrcompletionDate().equals("")) {
					getCbUploadInfoPojo().setCompletionDate(new Date(Utilities.getFormattedDate(new Date(cbUploadInfoPojo.getStrcompletionDate()))));
					}
				if(getCbUploadInfoPojo().getStrtestDate() != null && !getCbUploadInfoPojo().getStrtestDate().equals("")) {
					getCbUploadInfoPojo().setTestDate(new Date(Utilities.getFormattedDate(new Date(cbUploadInfoPojo.getStrtestDate()))));
					}
				if(getCbUploadInfoPojo().getStrprocessDate() != null && !getCbUploadInfoPojo().getStrprocessDate().equals("")) {
					getCbUploadInfoPojo().setProcessDate(new Date(Utilities.getFormattedDate(new Date(cbUploadInfoPojo.getStrprocessDate()))));
					}
				if(getCbUploadInfoPojo().getStrreceivedDate() != null && !getCbUploadInfoPojo().getStrreceivedDate().equals("")) {
					getCbUploadInfoPojo().setReceivedDate(new Date(Utilities.getFormattedDate(new Date(cbUploadInfoPojo.getStrreceivedDate()))));
					}
				if(getCbUploadInfoPojo().getStrreportDate()!= null && !getCbUploadInfoPojo().getStrreportDate().equals("")){
					getCbUploadInfoPojo().setReportDate(new Date(Utilities.getFormattedDate(new Date(cbUploadInfoPojo.getStrreportDate()))));
				}
				
				//Get the attachment information for every lab test and save for every test id
			if(attachments!=null) {
					for(int j=0;j<attachments.size();j++) {
						//log.debug(":::::::;attachments.size()"+attachments.size());
						attachmentId = attachments.get(j).getAttachmentId();
						testEntityId = attachments.get(j).getGarudaEntityId();
						testEntityType = attachments.get(j).getGarudaEntityType();
						
						getCbUploadInfoPojo().setFk_AttachmentId(attachmentId);
						
						//getPatientLabPojo().setTestEntityId(testEntityId);
						//getPatientLabPojo().setTestEntityType(testEntityType);
						cbUploadInfoPojo = getCbUploadInfoPojo();
						
						
						try {
							linkController.saveCatUploadInfo(cbUploadInfoPojo);
							
							// to do set dropdown values
						} catch (Exception e) {
							log.error(e.getMessage());
							e.printStackTrace();
						}
						
				}
				}
	}


/*if (testName != null && testName.length() > 0) {

	String categoryId = getRequest().getParameter("categoryId");
	//log.debug("categoryId....." +categoryId);
	
	getLabMaintPojo().setLabTestName(testName);
	getLabMaintPojo().setFk_CategroyId(Long.parseLong(categoryId.toString()));
	labMaintPojo = getLabMaintPojo();
	
	try {
		linkController.saveNewLabTestInfo(labMaintPojo);
		//log.debug("Lab test id  after save"+getLabMaintPojo().getPk_LabTestId());
		Long labTestid = getLabMaintPojo().getPk_LabTestId();
		getPatientLabPojo().setFk_LabTestId(labTestid);
		
		
		//	Get the attachment information for every lab test and save for every test name
		if(attachments!=null) {
			for(int j=0;j<attachments.size();j++) {
				attachmentId = attachments.get(j).getAttachmentId();
				testEntityId = attachments.get(j).getGarudaEntityId();
				testEntityType = attachments.get(j).getGarudaEntityType();
				
				getPatientLabPojo().setFk_AttachmentId(attachmentId);
				getPatientLabPojo().setTestEntityId(testEntityId);
				getPatientLabPojo().setTestEntityType(testEntityType);
		
				patientLabPojo = getPatientLabPojo();
				linkController.savePatientTestInfo(patientLabPojo);
			}
		}
		
		
		
		
		
	} catch (Exception e) {
		e.printStackTrace();
	}*/
	
//}

getRequest().setAttribute("update", true);
getCdrCbuPojo().setCdrCbuId(null);
//searchCbuDetails();
return "success";
}
	
public String saveCategoryDetails() throws Exception{
	setForuploadDoc(true);
	saveDocumentWithCategory();
	//log.debug("after saveDocumentWithCategory #######################################################################################:::"+cdrCbuPojo.getCordID());
	if(request.getParameter("orderId")!=null && !request.getParameter("orderId").equals("") && !request.getParameter("orderId").equals("undefined") && request.getParameter("orderType")!=null && !request.getParameter("orderType").equals("") && !request.getParameter("orderType").equals("undefined")){
		cbuaction.viewOrderDetails();
		//log.debug("after viewOrderDetails #######################################################################################");
	}else{
		//log.debug("before getClinicalDataFromPF #######################################################################################");
		cbuaction.setCordDataWithRights();
		//log.debug("after getClinicalDataFromPF #######################################################################################");
	}
	//log.debug("getFCRCompletedFlag()::::::::::"+cbuaction.getFCRCompletedFlag());
	setFCRCompletedFlag(cbuaction.getFCRCompletedFlag());
	//log.debug("getFCRCompletedFlag()::::::::::"+getFCRCompletedFlag());
	//log.debug("getMCCompletedFlag():::::::::::"+cbuaction.getMCCompletedFlag());
	setMCCompletedFlag(cbuaction.getMCCompletedFlag());
	//log.debug("getMCCompletedFlag():::::::::::"+getMCCompletedFlag());
	//log.debug("getCBUAssessmentFlag():::::::::"+cbuaction.getCBUAssessmentFlag());
	setCBUAssessmentFlag(cbuaction.getCBUAssessmentFlag());
	//log.debug("getCBUAssessmentFlag():::::::::"+getCBUAssessmentFlag());
	//log.debug("getOrderPojo().getPk_OrderId():"+cbuaction.getOrderPojo().getPk_OrderId());
	setOrderPojo(cbuaction.getOrderPojo());
	//log.debug("getOrderPojo().getPk_OrderId():"+getOrderPojo().getPk_OrderId());
	//log.debug("cbuaction.getCbuFinalReviewPojo():::::::"+cbuaction.getCbuFinalReviewPojo());
	setCbuFinalReviewPojo(cbuaction.getCbuFinalReviewPojo());
	//log.debug("getCbuFinalReviewPojo():::::::"+getCbuFinalReviewPojo());
	//log.debug("cbuaction.getCbuCompleteReqInfoPojo():::::::"+cbuaction.getCbuCompleteReqInfoPojo());
	setCbuCompleteReqInfoPojo(cbuaction.getCbuCompleteReqInfoPojo());
	//log.debug("getCbuCompleteReqInfoPojo():::::::"+getCbuCompleteReqInfoPojo());
	return "success";	
}


	/**
	 * 
	 * @return
	 */
	private List queryForOpenOrderList() {
		
		//String CbuId = getRequest().getParameter("cdrCbuId");
		//log.debug("-----------CBU  ids--------"+Long.parseLong(CbuId.toString()));
		cdrCbuPojo = getCdrCbuPojo();
		String[] linesArr=null;
		//Long cbuId1=(Long) getRequest().getAttribute("cbuCheck");
		//log.debug("-----------Cord:::::::::::-------"+cbuId1);
		String criteria = null;
		try {
			criteria = linkController.getQueryForOpenOrderList(cdrCbuPojo);
			List lst=codelstController.getCodelst(VelosGarudaConstants.CATEGORY);
			/*GrpRightsJB grpRights = (GrpRightsJB)getRequest().getSession().getAttribute("GRights");
			int cbuCordEntryRight = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ADD"));
			
			Iterator it1=lst.iterator();
			 	//log.debug("outside loop"+lst.size());
			    int flagval1=0;
			   int i  = 0;
			   for (Iterator iterator = lst.iterator(); iterator.hasNext();) {
				   codeList = (CodeList) iterator.next();
				   linesArr[i] =codeList.getSubType();
				   i++;
			    	}*/
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return velosHelper.getObjectList(criteria);

	}

	/**
	 * 
	 * @return
	 */
	/*private List queryForCategoryList() {
		String criteria = null;
		try {
			criteria = linkController.queryForCategoryList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return velosHelper.getObjectList(criteria);
	}*/

	/**
	 * 
	 * @return
	 */
	public List populateDropdownValues() {
		String criteria = null;

		String selectedValue = getRequest().getParameter("opt");
		//log.debug("selectedValue from request = " + selectedValue);

		Long selVal = Long.parseLong(selectedValue);
		
		try {
			criteria = linkController.populateDropdownValues(selVal);
			//searchOpenOrderDetails();
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return velosHelper.getObjectList(criteria);
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	public String setSessionDataPS(){
		try{
			
			/*****PENDING SCREEN PLACING VALUES IN SESSION******/
			
			/*****SETING SHOW ENTRIES VALUE IN SESSION******/
			
			//log.debug("\n\n\n\n\n\n\n\n\n\n\n\n\n\nInside setSessionDataPS---------------------------------------------------------------------------------------------");
			
			if(request.getParameter("actOrd_SE")!=null){
				
				String ordse=request.getParameter("actOrd_SE").toString();
				getRequest().getSession().setAttribute("v_actOrd_SE", ordse);
				//log.debug("inside setting of value in session activeOrder showEntries::::::::::::"+request.getAttribute("v_actOrd_SE"));
				
			}
			if(request.getParameter("naCBU_SE")!=null){
				
				String nacbuse=request.getParameter("naCBU_SE").toString();
				getRequest().getSession().setAttribute("v_naCBU_SE", nacbuse);
				//log.debug("inside setting of value in session NACBU showEntries::::::::::::"+request.getAttribute("v_naCBU_SE"));
				
			}
			if(request.getParameter("SIP_SE")!=null){
				
				String sipcbuse=request.getParameter("SIP_SE").toString();
				getRequest().getSession().setAttribute("v_SIP_SE", sipcbuse);
				//log.debug("inside setting of value in session SIP showEntries::::::::::::"+request.getAttribute("v_SIP_SE"));
				
			}
			
			
			
			/*****SETING SHOW ENTRIES TEXT IN SESSION******/
			
			//log.debug("\n\n\n\n\n\n\n\n\n\n\n\n\n\nInside setSessionDataPS---------------------------------------------------------------------------------------------");
			
			if(request.getParameter("actOrd_SE_txt")!=null){
				
				String ordse=request.getParameter("actOrd_SE_txt").toString();
				getRequest().getSession().setAttribute("v_actOrd_SE_txt", ordse);
				//log.debug("inside setting of value in session activeOrder showEntries::::::::::::"+request.getSession().getValue("v_actOrd_SE_txt"));
				
			}
			if(request.getParameter("naCBU_SE_txt")!=null){
				
				String nacbuse=request.getParameter("naCBU_SE_txt").toString();
				getRequest().getSession().setAttribute("v_naCBU_SE_txt", nacbuse);
				//log.debug("inside setting of value in session NACBU showEntries::::::::::::"+request.getSession().getValue("v_naCBU_SE_txt"));
				
			}
			if(request.getParameter("SIP_SE_txt")!=null){
				
				String sipcbuse=request.getParameter("SIP_SE_txt").toString();
				getRequest().getSession().setAttribute("v_SIP_SE_txt", sipcbuse);
				//log.debug("inside setting of value in session SIP showEntries::::::::::::"+request.getSession().getValue("SIP_SE_txt"));
				
			}
			
			/*****SETING LAST FILTER VALUE IN SESSION******/
			
			if(request.getParameter("actOrd_fval")!=null){
				
				String ordTblfval=request.getParameter("actOrd_fval").toString();
				getRequest().getSession().setAttribute("v_actOrd_fval", ordTblfval);
				//log.debug("inside setting of value in session filtervaluesOrders::::::::::::"+request.getAttribute("v_actOrd_fval"));
				
			}
			if(request.getParameter("naCBU_fval")!=null){
				
				String nacbufval=request.getParameter("naCBU_fval").toString();
				getRequest().getSession().setAttribute("v_naCBU_fval", nacbufval);
				//log.debug("inside setting of value in session filtervaluesNACBU::::::::::::"+request.getAttribute("v_naCBU_fval"));
				
			}
			if(request.getParameter("SIP_fval")!=null){
				
				String sipfval=request.getParameter("SIP_fval").toString();
				getRequest().getSession().setAttribute("v_SIP_fval", sipfval);
				//log.debug("inside setting of value in session filtervaluesSIP"+request.getAttribute("v_SIP_fval"));
				
			}
			
			
			
			/*****SETING LAST SORT VALUES IN SESSION******/
			
			if(request.getParameter("Ord_Sort_Col")!=null && request.getParameter("Ord_Sort_Typ")!=null){
				
				String ordSortCol=request.getParameter("Ord_Sort_Col").toString();
				String ordSortType=request.getParameter("Ord_Sort_Typ").toString();
				getRequest().getSession().setAttribute("v_actOrd_SC", ordSortCol);
				getRequest().getSession().setAttribute("v_actOrd_ST", ordSortType);
				//log.debug("inside setting of value in session Order sort values::::::::::::"+request.getSession().getValue("v_actOrd_SC")+":"+request.getSession().getValue("v_actOrd_ST"));
				
			}
			if(request.getParameter("NACBU_Sort_Col")!=null && request.getParameter("NACBU_Sort_Typ")!=null){
				
				String nacbuSortCol=request.getParameter("NACBU_Sort_Col").toString();
				String nacbuSortType=request.getParameter("NACBU_Sort_Typ").toString();
				getRequest().getSession().setAttribute("v_NACBU_SC", nacbuSortCol);
				getRequest().getSession().setAttribute("v_NACBU_ST", nacbuSortType);
				//log.debug("inside setting of value in session NACBU sort values::::::::::::"+request.getSession().getValue("v_NACBU_SC")+":"+request.getSession().getValue("v_NACBU_ST"));
				
			}
			if(request.getParameter("SIP_Sort_Col")!=null && request.getParameter("SIP_Sort_Typ")!=null){
				
				String sipSortCol=request.getParameter("SIP_Sort_Col").toString();
				String sipSortType=request.getParameter("SIP_Sort_Typ").toString();
				getRequest().getSession().setAttribute("v_SIP_SC", sipSortCol);
				getRequest().getSession().setAttribute("v_SIP_ST", sipSortType);
				//log.debug("inside setting of value in session SIP sort values::::::::::::"+request.getSession().getValue("v_SIP_SC")+":"+request.getSession().getValue("v_SIP_ST"));
				
			}
			
			/*****SETING CBB ID IN SESSION******/
			if(request.getParameter("last_cbb")!=null){
				String fksiteId=request.getParameter("last_cbb").toString();
				getRequest().getSession().setAttribute("lastCBB", fksiteId);
				//log.debug("inside setting of value in session"+request.getAttribute("lastCBB"));
				
			}
			
			//log.debug("\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
			
			
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public String PendingScreen(){
		
		try{
			List<SitePojo> cbbList = cbbController.getSites();
			getRequest().setAttribute("CbbList", cbbList);
			initializeAppMessages(context);
			UserJB user = (UserJB)getRequest().getSession().getAttribute(VelosGarudaConstants.GARUDA_USER);
			Long userId = (long)user.getUserId();
			request.setAttribute("userId", userId);
			String primaryOrg = user.getUserSiteId();
			setUserPrimaryOrg(primaryOrg);
			
			//log.debug("Primary Organization::::::"+getUserPrimaryOrg());
			
			Iterator<SitePojo> ie=cbbList.iterator();
			String siteval="";
			int count=0;
			while(ie.hasNext()){
				SitePojo temp=ie.next();
				if(count==0){
					siteval=temp.getSiteId().toString();
					count=1;
				}else{
					siteval=siteval+","+temp.getSiteId();
				}
			}
			//log.debug("siteval"+siteval);
			String fromLandingPage=request.getParameter("filterPending");
			if(fromLandingPage!=null && fromLandingPage!="" && fromLandingPage.equals("1")){
				setPendingFilter(fromLandingPage);
				String ordertypeval=request.getParameter("fkordertype").toString();
				setPfOrdertype(ordertypeval);
				String fksiteIdval=request.getParameter("pksiteid");
				//log.debug("fksiteid:::"+fksiteIdval);
				setPfSiteId(fksiteIdval);
				String sampleAtlab=request.getParameter("sampleAtLab");
				setPfSampleAtlab(sampleAtlab);
			}
			
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}
		
		/****************Calling Orders part in load itself******************/
		getPendingScreenOrders();
		/************************************/
		
		return "success";
	}
	@SuppressWarnings({ "unchecked", "deprecation" })
	public String getPendingScreenOrders(){
		
		//log.debug("\n\n\n\n\n\n\n\n\nPendingScreen orders start At..................."+new Date());
		try{
			
			String fksiteId="";
			String ordertypeval="";
			String filterflag="0";
			String temp1=request.getParameter("filterPending");
			String condition="";
			String orderBy="";
			UserJB user = (UserJB)getRequest().getSession().getAttribute(VelosGarudaConstants.GARUDA_USER);
			Long userId = (long)user.getUserId();
			fksiteId="";
			int allEntriesflag=0;
			List tempList=new ArrayList();
			int showEntriesSize=0;
			int refreshAction=0;
			String countStr="";
			
			
			if(request.getParameter("pkSiteId")!=null){
				fksiteId=request.getParameter("pkSiteId").toString();
				refreshAction=1;
			}else if(request.getSession().getValue("lastCBB")!=null && !request.getSession().getValue("lastCBB").equals("")){
				fksiteId=request.getSession().getValue("lastCBB").toString();
			}else{
				fksiteId=getUserPrimaryOrg();
			}
			
			
			if(request.getParameter("allEntries")!=null && !request.getParameter("allEntries").toString().equals("")){
				if(request.getParameter("allEntries").toString().equals("ALL")){
					allEntriesflag=1;
				}
				
			}else if(request.getSession().getValue("v_actOrd_SE_txt")!=null && !request.getSession().getValue("v_actOrd_SE_txt").equals("")){	
				
				if(request.getSession().getValue("v_actOrd_SE_txt").equals("ALL")){
					allEntriesflag=1;
					showEntriesSize=100;
				}else{
					if(request.getSession().getValue("v_actOrd_SE_txt")!=null && !request.getSession().getValue("v_actOrd_SE_txt").equals("") && !request.getSession().getValue("v_actOrd_SE_txt").equals("null"))
					showEntriesSize=Integer.parseInt(request.getSession().getValue("v_actOrd_SE_txt").toString());
				}
				
			}
			
			condition=getPSOrdersCondition();
			//orderBy=getPSOrders_OrderBy();
			
			if(refreshAction==0 && condition.equals("") && request.getSession().getValue("v_actOrd_fval")!=null && !request.getSession().getValue("v_actOrd_fval").equals("")){
				String temp=request.getSession().getValue("v_actOrd_fval").toString();
				temp=temp.replace("----", "&");
				condition=getPSOrdersConditionFromSession();
			}
			
			if(temp1!=null && temp1!="" && temp1.equals("1")){
				filterflag=request.getParameter("filterPending");
				setPendingFilter(filterflag);
				ordertypeval=request.getParameter("fkordertype").toString();
				setPfOrdertype(ordertypeval);
				fksiteId=request.getParameter("pksiteid");
				setPfSiteId(fksiteId);
				condition=getPSOrdersCondition();
			}
			
			
			if(fksiteId.equals(ALL_SITE)){
				
				fksiteId=getUserSiteString(userId,GET_DATA);
				countStr=getUserSiteString(userId,GET_COUNT);
				
			}else{
				int charCount = (fksiteId.length() - fksiteId.replaceAll("\\,", "").length())+1;
				countStr=Integer.toString(charCount);
			}
			
			int iTotalSize=0;
			
			setSessionDataPS();
			
			if(request.getParameter("filterTxt")!=null && !request.getParameter("filterTxt").trim().equals("")){
				ServerGlobalSearch serverGlobalSearch=new ServerGlobalSearch();
				String conditionnew=serverGlobalSearch.FetchGlobalSearchPsOrders(request.getParameter("filterTxt").trim());
				condition=condition+conditionnew;
			}
			
			
			paginationSessionData(paginateSearch,showEntriesSize,MODULE_PENDING);
			if(allEntriesflag==1){
				tempList=linkController.getallOrderDetails(paginateSearch,2,filterflag,ordertypeval,fksiteId,userId,condition,orderBy);
				Object obj=tempList.get(0);
				iTotalSize=Integer.parseInt(obj.toString());
				/*setOrderDetailsList(tempList);
				paginateSearch.setiShowRows(tempList.size());
				iTotalSize=tempList.size();*/
				//log.debug("\n\n\n\n\nallentries flag 1....."+allEntriesflag);
				
			}else{
				
				/*if((showEntriesSize>50 || request.getParameter("iShowRows")!=null) && (request.getParameter("iShowRows").toString().equals("05") || request.getParameter("iShowRows").toString().equals("10")  || request.getParameter("iShowRows").toString().equals("25") || request.getParameter("iShowRows").toString().equals("50"))){
					setOrderDetailsList(linkController.getallOrderDetails(paginateSearch,1,filterflag,ordertypeval,fksiteId,userId,condition,orderBy));	
				}
				if(request.getParameter("iShowRows")==null){
					setOrderDetailsList(linkController.getallOrderDetails(paginateSearch,1,filterflag,ordertypeval,fksiteId,userId,condition,orderBy));
				}*/
				
				tempList=linkController.getallOrderDetails(paginateSearch,2,filterflag,ordertypeval,fksiteId,userId,condition,orderBy);
				Object obj=tempList.get(0);
				iTotalSize=Integer.parseInt(obj.toString());
				//log.debug("\n\n\n\n\nallentries flag ! 1....."+allEntriesflag);
				
			}
			
			setAssignUsrLst(userList(GET_ALL_FOR_CBB,fksiteId));
			
			paginateSearch.setiTotalRows(iTotalSize);
			setPaginateSearch(VelosUtil.getListTotalCount(paginateSearch));
			request.setAttribute("paginateSearch", paginateSearch);
			Long module=getCodeListPkByTypeAndSubtype(FILTER_MODULE, PENDING_SCREEN);
			Long sub_module=getCodeListPkByTypeAndSubtype(SUBMODULE_PENDING, SUBMODULE_PENDING_ORDERS);
			setCustomFilter(linkController.getCustomFilterData(userId,module,sub_module));
			
		    setLabelList(linkController.getLabelAssignLst(fksiteId));
			setCommonUsrLst(linkController.getCommonUsrLst(fksiteId,countStr));
			
			
			//log.debug("\n\n\n\nstartdate"+pStartDt+"----enddate---"+pEndDt);
			
			//log.debug("\n\n\n\n\nPendingScreen orders end At..................."+new Date());
			
		}catch (Exception e) {
			//log.debug("Inside Errors in Orders::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::;");
			log.error(e.getMessage());
			e.printStackTrace();
		}
		
		
		
		return "success";
	}
	public String getPSOrdersConditionFromSession(){
		String condition="";

		String temp=request.getSession().getValue("v_actOrd_fval").toString();
		temp=temp.replace("----", "&");
		temp=temp.concat("&");
		String getStr1="";
		String getStr2="";
		String dropDownNames[]={"siteSeq=","satellite=","regId=","locId=","reqType1=","orderPriority1=","orderStatus1=","ackresolution=","assignTo1=","resolution1=","reviewBy1=","noOfdays1=","dateStart1=","dateEnd1=","sampleAtlab1=","closeReason1="};
		String columnNames[]={"site.site_id","ADR.ADD_CITY","CORD.CORD_REGISTRY_ID","CORD.CORD_LOCAL_CBU_ID","ORD.ORDER_TYPE","ORD.ORDER_PRIORITY","ORD.ORDER_STATUS","ORD.ORDER_ACK_FLAG","ORD.ASSIGNED_TO","ORD.FK_ORDER_RESOL_BY_TC","ORD.ORDER_LAST_VIEWED_BY","trunc(SYSDATE-ordhdr.order_open_date)","ordhdr.order_open_date","ordhdr.order_open_date","ord.order_sample_at_lab","cbustat1.PK_CBU_STATUS"};
		List dropDownValues=new ArrayList();
		//log.debug("Raw ....."+temp);
		for(int i=0;i<dropDownNames.length;i++){
		     int tempIndex1=temp.indexOf(dropDownNames[i]);	
		     getStr2="";
		     if(tempIndex1>0){
		    	 getStr1=temp.substring(tempIndex1);
			     //log.debug("String from keyword....."+getStr1);
			     int tempIndex2=getStr1.indexOf("&");
			     int keywordSize=dropDownNames[i].length();
			     //log.debug("second String index::::"+tempIndex2);
			     if(tempIndex2>0){
			    	 getStr2=getStr1.substring(keywordSize,tempIndex2);
			     }
			     //log.debug("value for the keyword....."+getStr2);
			     
		     }
		     dropDownValues.add(getStr2);
		     //System.out.println("value for the keyword....."+getStr2);
		     
		}
		
		Object[] temptttt=dropDownValues.toArray();	
		
		
		
		
		for(int i=0;i<dropDownNames.length;i++){
			
			
			if(temptttt[i]!=null && !temptttt[i].equals("")){
							
				if(i==11){
					condition=condition+" and "+columnNames[i]+">='"+temptttt[i]+"'";
				}else if(i==13){
					String val1=temptttt[12].toString();
					String val2=temptttt[13].toString();
					if(val1.equals(val2)){
						condition=condition+" and to_char("+columnNames[i]+",'DD-Mon-YYYY')='"+temptttt[12]+"'";
					}else{
						condition=condition+" and (trunc("+columnNames[i]+") between to_date('"+temptttt[12]+"','DD-Mon-YYYY') and to_date('"+temptttt[13]+"','DD-Mon-YYYY'))";
					}
					
				}else if(i==8){
						String tempval=temptttt[8].toString();
						
						if(tempval.equals("Unassigned")){
							condition=condition+" and "+columnNames[i]+" IS NULL";						
						}
						else if(tempval.equals("ALL")){
							/*No cha*/
						}else{
							condition=condition+" and "+columnNames[i]+"='"+temptttt[i]+"'";
						}
				}else if(i!=11 && i!=12 && i!=13 && i!=8 && i!=0 && i!=1 && i!=2 && i!=3){
					if(i==4){
						String tempval=temptttt[i].toString();
						if(!tempval.equals("ALL")){
							condition=condition+" and "+columnNames[i]+"='"+temptttt[i]+"'";	
						}
						
					}else{
						condition=condition+" and "+columnNames[i]+"='"+temptttt[i]+"'";
						
					}
					
				}if(i==0 || i==1 || i==2 || i==3){
					condition=condition+" and replace(lower("+columnNames[i]+"),'-','') like lower('%"+ Utilities.removeDelimeters(temptttt[i].toString(), "-") + "%')";
				}
				
				if(i==0)
					setSiteSeq(temptttt[i].toString());
				if(i==1)
					setSatellite(temptttt[i].toString());
				if(i==2)
					setRegId(temptttt[i].toString());
				if(i==3)
					setLocId(temptttt[i].toString());
				if(i==4)
					setReqType(temptttt[i].toString());
				if(i==5)
					setPriority(temptttt[i].toString());
				if(i==6)
					setStatus(temptttt[i].toString());
				if(i==7)
					setAckresolution(temptttt[i].toString());
				if(i==8)
					setAssignto(temptttt[i].toString());
				if(i==9)
					setResolution(temptttt[i].toString());
				if(i==10)
					setReviewby(temptttt[i].toString());
				if(i==11)
					setNoofdays(temptttt[i].toString());
				if(i==12)
					setpStartDt(Utilities.getOrdDateFormatDate(temptttt[i].toString()));
				if(i==13)
					setpEndDt(Utilities.getOrdDateFormatDate(temptttt[i].toString()));
				if(i==14)
					setSampleAtlab(temptttt[i].toString());
				if(i==15)
					setCancelreason(temptttt[i].toString());
				
			}
			
		}
		
		//log.debug("\n\n\n\n\n\nCondition in session order by in pending screen orders:::::::::::::::::::::"+condition);
		
		return condition;
	}
	@SuppressWarnings("unchecked")
	public String pendingScreenNACBU(){
		
		try{
			UserJB user = (UserJB)getRequest().getSession().getAttribute(VelosGarudaConstants.GARUDA_USER);
			Long userId = (long)user.getUserId();
			String nacbuCondition="";
			String nacbuOrderBy="";
			String fksiteId=request.getParameter("pkSiteId").toString();
			int allEntriesflag=0;
			List naCBULst=new ArrayList();
			
			if(request.getParameter("allEntries")!=null && !request.getParameter("allEntries").toString().equals("") && request.getParameter("allEntries").toString().equals("ALL")){
				//log.debug("\n\n\n\n\n\n\n\n\n allEntriesflag"+request.getParameter("allEntries").toString());
				allEntriesflag=1;
			}
			
			if(fksiteId.equals(ALL_SITE)){
				
				fksiteId=getUserSiteString(userId,GET_DATA);
				
			}
			
			nacbuCondition=getNACBUCondition();
			//nacbuOrderBy=getNACBU_OrderBy();
			
			paginationReqScopeData(paginationSearch1);
			int iTotalRows2 =0;
			
			
			if(request.getParameter("filterTxt")!=null && !request.getParameter("filterTxt").trim().equals("")){
				ServerGlobalSearch serverGlobalSearch=new ServerGlobalSearch();
				String conditionnew=serverGlobalSearch.FetchGlobalSearchPSNotAvailCbu(request.getParameter("filterTxt").trim());
				nacbuCondition=nacbuCondition+conditionnew;
			}
			
			if(allEntriesflag==1){
				naCBULst=linkController.notAvailCbu(paginationSearch1,nacbuCondition,2,fksiteId,nacbuOrderBy,userId);
				Object obj=naCBULst.get(0);
				iTotalRows2 = Integer.parseInt(obj.toString());
				//System.out.println("inside calling all");
				/*setNaCbuList(naCBULst);
				paginationSearch1.setiShowRows(naCBULst.size());
				iTotalRows2 = naCBULst.size();*/
				
			}else{
				/*setNaCbuList(linkController.notAvailCbu(paginationSearch1,nacbuCondition,1,fksiteId,nacbuOrderBy,userId));*/
				naCBULst=linkController.notAvailCbu(paginationSearch1,nacbuCondition,2,fksiteId,nacbuOrderBy,userId);
				Object obj=naCBULst.get(0);
				iTotalRows2 = Integer.parseInt(obj.toString());
				
			}
		
			paginationSearch1.setiTotalRows(iTotalRows2);
			setPaginationSearch1(VelosUtil.getListTotalCount(paginationSearch1));
			request.setAttribute("paginationSearch1", paginationSearch1);
			Long module=getCodeListPkByTypeAndSubtype(FILTER_MODULE, PENDING_SCREEN);
			Long sub_module=getCodeListPkByTypeAndSubtype(SUBMODULE_PENDING, SUBMODULE_PENDING_NACBU);
			setCustomFilternacbu(linkController.getCustomFilterData(userId,module,sub_module));
			
			setAssignUsrLst(linkController.getLabelAssignLst(fksiteId));
			
		}catch(Exception e){
			//log.debug("Inside Errors in Not Available CBUS::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::;");
			log.error(e.getMessage());
			e.printStackTrace();
		}
		
		
		return "success";
	}
	@SuppressWarnings("unchecked")
	public String pendingScreenSIP(){
		try{
			UserJB user = (UserJB)getRequest().getSession().getAttribute(VelosGarudaConstants.GARUDA_USER);
			Long userId = (long)user.getUserId();
			String SIPCondition="";
			String sipOrderBy="";
			String fksiteId=request.getParameter("pkSiteId").toString();
			int allEntriesflag=0;
			List savedInprogLst=new ArrayList();
			
			if(request.getParameter("allEntries")!=null && !request.getParameter("allEntries").toString().equals("") && request.getParameter("allEntries").toString().equals("ALL")){
				//log.debug("\n\n\n\n\n\n\n\n\n allEntriesflag"+request.getParameter("allEntries").toString());
				allEntriesflag=1;
			}
			
			if(fksiteId.equals(ALL_SITE)){
				
				fksiteId=getUserSiteString(userId,GET_DATA);
				
			}
			SIPCondition=getSIPCondition();
			//sipOrderBy=getSIPOrderBy();
			paginationReqScopeData(paginationSearch);
			
			int iTotalRows1 = 0;
			
			if(request.getParameter("filterTxt")!=null && !request.getParameter("filterTxt").trim().equals("")){
				ServerGlobalSearch serverGlobalSearch=new ServerGlobalSearch();
				String conditionnew=serverGlobalSearch.FetchGlobalSearchPSSavInProg(request.getParameter("filterTxt").trim());
				SIPCondition=SIPCondition+conditionnew;
			}
			
			
			if(allEntriesflag==1){
				savedInprogLst=linkController.savedInprogress(paginationSearch,SIPCondition,userId,fksiteId,sipOrderBy,2);
				Object obj=savedInprogLst.get(0);
				iTotalRows1=Integer.parseInt(obj.toString());
				/*setCordentryprogress(savedInprogLst);
				paginationSearch.setiShowRows(savedInprogLst.size());
				iTotalRows1 = savedInprogLst.size();*/
				
			}else{
				/*setCordentryprogress(linkController.savedInprogress(paginationSearch,SIPCondition,userId,fksiteId,sipOrderBy,1));*/
				savedInprogLst=linkController.savedInprogress(paginationSearch,SIPCondition,userId,fksiteId,sipOrderBy,2);
				Object obj=savedInprogLst.get(0);
				iTotalRows1=Integer.parseInt(obj.toString());
				
			}

			
			paginationSearch.setiTotalRows(iTotalRows1);
			setPaginationSearch(VelosUtil.getListTotalCount(paginationSearch));
			request.setAttribute("paginationSearch", paginationSearch);
			Long module=getCodeListPkByTypeAndSubtype(FILTER_MODULE, PENDING_SCREEN);
			Long sub_module=getCodeListPkByTypeAndSubtype(SUBMODULE_PENDING, SUBMODULE_PENDING_SIP);
			setCustomFiltersip(linkController.getCustomFilterData(userId,module,sub_module));
			
		}catch (Exception e) {
			//log.debug("Inside Errors in Saved In Progress::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::;");
			log.error(e.getMessage());
			e.printStackTrace();
		}
		
		return "success";
	}
	
	public String getNACBUCondition(){
		String condition="";
		
		String dropDownNames[]={"nasiteId","naregId","nalocId","nastatus","nareason","naremovedby"};
		String columnNames[]={"site.site_id","cord.cord_registry_id","cord.cord_local_cbu_id","cord.cord_nmdp_status","cord.FK_CORD_CBU_STATUS","cord.LAST_MODIFIED_BY"};
		
		for(int i=0;i<dropDownNames.length;i++){
			
			
			if(request.getParameter(dropDownNames[i])!=null && !request.getParameter(dropDownNames[i]).equals("") && !request.getParameter(dropDownNames[i]).equals("undefined")){
				if(i==0 || i==1 || i==2){
					condition=condition+" and replace(lower("+columnNames[i]+"),'-','') like lower('%"+ Utilities.removeDelimeters(request.getParameter(dropDownNames[i]).toString(), "-") + "%')";
				}else{
					condition=condition+" and "+columnNames[i]+"='"+request.getParameter(dropDownNames[i])+"'";
				}
				
				
				if(i==0)
					setNasiteId(request.getParameter(dropDownNames[i]).toString());
				if(i==1)
					setNaregId(request.getParameter(dropDownNames[i]).toString());
				if(i==2)
					setNalocId(request.getParameter(dropDownNames[i]).toString());
				if(i==3)
					setNastatus(request.getParameter(dropDownNames[i]).toString());
				if(i==4){
					setNareason(request.getParameter(dropDownNames[i]).toString());
				}
				if(i==5)
					setNaremovedby(request.getParameter(dropDownNames[i]).toString());
				
					
			}
			
		}
		//log.debug("condition in na:::::::::::::::"+condition);
		
		
		return condition;
		
	}
	public String getNACBU_OrderBy(){
		
		String orderBycondition="";
		
		String tableColumnNames[]={"naCBU_cbbId","naCBU_regId","naCBU_cbuId","naCBU_cbuStat","naCBU_reas","naCBU_removeBy"};
		String columnNames[]={"site.site_id","cord.cord_registry_id","cord.cord_local_cbu_id","nmdpstat.cbu_status_desc","loclstat.cbu_status_desc","usr.usr_logname"};
		
		for(int i=0;i<tableColumnNames.length;i++){
			
			
			if(request.getParameter(tableColumnNames[i])!=null && !request.getParameter(tableColumnNames[i]).equals("")){
				
				String tempVariable=request.getParameter(tableColumnNames[i]).toString();
				
				if(tempVariable.equals(SORT_ASC)){
					orderBycondition="ORDER BY "+columnNames[i]+" ASC";
				}
				if(tempVariable.equals(SORT_DES)){
					orderBycondition="ORDER BY "+columnNames[i]+" DESC";
				}
					
				
			}
		}
		
		//log.debug("\n\n\n\n\n\n\n\n\nOrder by condition in NA CBU orderBY::::::::::::::::::::::::"+orderBycondition);
		
		return  orderBycondition;
		
	}
	public String getSIPCondition(){
		String condition="";
		String dropDownNames[]={"sipsiteId","sipregId","siplocId","sippercent","sipnoofdays","sipStartDt","sipEndDt"};
		String columnNames[]={"site.site_id","cord.cord_registry_id","cord.cord_local_cbu_id","cord.cord_entry_progress","ceil(sysdate-cord.created_on)","cord.created_on","cord.created_on"};
		
		for(int i=0;i<dropDownNames.length;i++){
			
			
			if(request.getParameter(dropDownNames[i])!=null && !request.getParameter(dropDownNames[i]).equals("") && !request.getParameter(dropDownNames[i]).equals("undefined")){
				if(i==6){
					String val3=request.getParameter(dropDownNames[5]).toString();
					String val4=request.getParameter(dropDownNames[6]).toString();
					if(val3.equals(val4)){
						condition=condition+" and to_char("+columnNames[5]+",'DD-Mon-YYYY')='"+request.getParameter(dropDownNames[5])+"'";
					}else{
						condition=condition+" and (trunc("+columnNames[5]+") between to_date('"+request.getParameter(dropDownNames[5])+"','DD-Mon-YYYY') and to_date('"+request.getParameter(dropDownNames[6])+"','DD-Mon-YYYY'))";
					}
					
				}
				else if(i==3){
					condition=condition+" and "+columnNames[i]+">='"+request.getParameter(dropDownNames[i])+"'";
				}
				else if(i==4){
					condition=condition+" and "+columnNames[i]+">='"+request.getParameter(dropDownNames[i])+"'";
				}else if(i==0 || i==1 || i==2){
					//condition=condition+" and "+columnNames[i]+"='"+request.getParameter(dropDownNames[i])+"'";
					condition=condition+" and replace(lower("+columnNames[i]+"),'-','') like lower('%"+ Utilities.removeDelimeters(request.getParameter(dropDownNames[i]).toString(), "-") + "%')";
				}
				if(i==0)
					setSipsiteId(request.getParameter(dropDownNames[i]).toString());
				if(i==1)
					setSipregId(request.getParameter(dropDownNames[i]).toString());
				if(i==2)
					setSiplocId(request.getParameter(dropDownNames[i]).toString());
				if(i==3)
					setSippercent(request.getParameter(dropDownNames[i]).toString());
				if(i==4)
					setSipnoofdays(request.getParameter(dropDownNames[i]).toString());		
				if(i==5)
					setSipStartDt(Utilities.getOrdDateFormatDate(request.getParameter(dropDownNames[i]).toString()));		
				if(i==6)
					setSipEndDt(Utilities.getOrdDateFormatDate(request.getParameter(dropDownNames[i]).toString()));		
			}
			
		}
		//log.debug("Sip condition:::::::::::"+condition);
		return condition;
	}
	public String getSIPOrderBy(){
		
		String orderBycondition="";
		String tableColumnNames[]={"sip_cbbid","sip_cbuRegId","sip_loccbuid","sip_doe","sip_noofdays","sip_percent"};
		String columnNames[]={"site.site_id","cord.cord_registry_id","cord.cord_local_cbu_id","cord.created_on","ceil(sysdate-cord.created_on)","cord.cord_entry_progress"};
		
		for(int i=0;i<tableColumnNames.length;i++){
			
			
			if(request.getParameter(tableColumnNames[i])!=null && !request.getParameter(tableColumnNames[i]).equals("")){
				
				String tempVariable=request.getParameter(tableColumnNames[i]).toString();
				
				if(tempVariable.equals(SORT_ASC)){
					orderBycondition="ORDER BY "+columnNames[i]+" ASC";
				}
				if(tempVariable.equals(SORT_DES)){
					orderBycondition="ORDER BY "+columnNames[i]+" DESC";
				}
					
				
			}
		}
		
		//log.debug("\n\n\n\n\n\n\n\n\nOrder by condition in SIP orderBY::::::::::::::::::::::::"+orderBycondition);
		
		return  orderBycondition;
		
	}
	public String getPSOrdersCondition(){
		
		String condition="";
		
		
		String dropDownNames[]={"siteSeq","satellite","regId","locId","reqType1","orderPriority1","orderStatus1","ackresolution","assignTo1","resolution1","reviewBy1","noOfdays1","dateStart1","dateEnd1","sampleAtlab1","closeReason1"};
		String columnNames[]={"site.site_id","ADR.ADD_CITY","CORD.CORD_REGISTRY_ID","CORD.CORD_LOCAL_CBU_ID","ORD.ORDER_TYPE","ORD.ORDER_PRIORITY","ORD.ORDER_STATUS","ORD.ORDER_ACK_FLAG","ORD.ASSIGNED_TO","ORD.FK_ORDER_RESOL_BY_TC","ORD.ORDER_LAST_VIEWED_BY","trunc(SYSDATE-ordhdr.order_open_date)","ordhdr.order_open_date","ordhdr.order_open_date","ord.order_sample_at_lab","cbustat1.PK_CBU_STATUS"};
		
		
		for(int i=0;i<dropDownNames.length;i++){
			
			
			if(request.getParameter(dropDownNames[i])!=null && !request.getParameter(dropDownNames[i]).equals("") && !request.getParameter(dropDownNames[i]).equals("undefined")){
							
				if(i==11){
					condition=condition+" and "+columnNames[i]+">='"+request.getParameter(dropDownNames[i])+"'";
				}else if(i==13){
					String val1=request.getParameter(dropDownNames[12]).toString();
					String val2=request.getParameter(dropDownNames[13]).toString();
					if(val1.equals(val2)){
						condition=condition+" and to_char("+columnNames[i]+",'DD-Mon-YYYY')='"+request.getParameter(dropDownNames[12])+"'";
					}else{
						condition=condition+" and (trunc("+columnNames[i]+") between to_date('"+request.getParameter(dropDownNames[12])+"','DD-Mon-YYYY') and to_date('"+request.getParameter(dropDownNames[13])+"','DD-Mon-YYYY'))";
					}
					
				}else if(i==8){
						String tempval=request.getParameter(dropDownNames[8]).toString();
						
						if(tempval.equals("Unassigned")){
							condition=condition+" and "+columnNames[i]+" IS NULL";						
						}
						else if(tempval.equals("ALL")){
							/*No cha*/
						}else{
							condition=condition+" and "+columnNames[i]+"='"+request.getParameter(dropDownNames[i])+"'";
						}
				}else if(i!=11 && i!=12 && i!=13 && i!=8 && i!=0 && i!=1 && i!=2 && i!=3){
					if(i==4){
						String tempval="";
						if(request.getParameter("reqTypeAllFlag")!=null){
							tempval=request.getParameter("reqTypeAllFlag").toString();
						}
						if(!tempval.equals("1")){
							condition=condition+" and "+columnNames[i]+"='"+request.getParameter(dropDownNames[i])+"'";	
						}
						
					}else{
						condition=condition+" and "+columnNames[i]+"='"+request.getParameter(dropDownNames[i])+"'";
						
					}
					
				}if(i==0 || i==1 || i==2 || i==3){
					condition=condition+" and replace(lower("+columnNames[i]+"),'-','') like lower('%"+ Utilities.removeDelimeters(request.getParameter(dropDownNames[i]).toString(), "-") + "%')";
				}
				
				if(i==0)
					setSiteSeq(request.getParameter(dropDownNames[i]).toString());
				if(i==1)
					setSatellite(request.getParameter(dropDownNames[i]).toString());
				if(i==2)
					setRegId(request.getParameter(dropDownNames[i]).toString());
				if(i==3)
					setLocId(request.getParameter(dropDownNames[i]).toString());
				if(i==4)
					setReqType(request.getParameter(dropDownNames[i]).toString());
				if(i==5)
					setPriority(request.getParameter(dropDownNames[i]).toString());
				if(i==6)
					setStatus(request.getParameter(dropDownNames[i]).toString());
				if(i==7)
					setAckresolution(request.getParameter(dropDownNames[i]).toString());
				if(i==8)
					setAssignto(request.getParameter(dropDownNames[i]).toString());
				if(i==9)
					setResolution(request.getParameter(dropDownNames[i]).toString());
				if(i==10)
					setReviewby(request.getParameter(dropDownNames[i]).toString());
				if(i==11)
					setNoofdays(request.getParameter(dropDownNames[i]).toString());
				if(i==12)
					setpStartDt(Utilities.getOrdDateFormatDate(request.getParameter(dropDownNames[i]).toString()));
				if(i==13)
					setpEndDt(Utilities.getOrdDateFormatDate(request.getParameter(dropDownNames[i]).toString()));
				if(i==14)
					setSampleAtlab(request.getParameter(dropDownNames[i]).toString());
				if(i==15)
					setCancelreason(request.getParameter(dropDownNames[i]).toString());
				
			}
			
		}
		
		//log.debug("Condition in orders:::::::::::::::::::::"+condition);
		
		return condition;
	}
	public String getPSOrders_OrderBy(){
		
		String orderBycondition="";
		
		String tableColumnNames[]={"ord_cbbId","ord_cbbSate","ord_regId","ord_locCbuId","ord_reqType","ord_priority","ord_reqDt","ord_noofdays","ord_status","ord_ackRes","ord_closOrdRsn","ord_resTc","ord_assignTo","ord_reviewBy"};
		String columnNames[]={"site.site_id","ADR.ADD_CITY","CORD.CORD_REGISTRY_ID","CORD.CORD_LOCAL_CBU_ID","ORD.ORDER_TYPE","f_codelst_desc(ORD.ORDER_PRIORITY)","ordhdr.order_open_date","ceil(SYSDATE-ordhdr.order_open_date)","f_codelst_desc(ORD.ORDER_STATUS)","ORD.ORDER_ACK_FLAG","cbustat1.cbu_status_desc","cbustat.cbu_status_desc","usr.usr_logname","usr1.usr_logname"};
		
		for(int i=0;i<tableColumnNames.length;i++){
			
			
			if(request.getParameter(tableColumnNames[i])!=null && !request.getParameter(tableColumnNames[i]).equals("")){
				
				String tempVariable=request.getParameter(tableColumnNames[i]).toString();
				
				if(tempVariable.equals(SORT_ASC)){
					orderBycondition=columnNames[i]+" ASC";
				}
				if(tempVariable.equals(SORT_DES)){
					orderBycondition=columnNames[i]+" DESC";
				}
					
				
			}
		}
		
		//log.debug("\n\n\n\n\n\n\n\n\nOrder by condition in Pending Screen Orders::::::::::::::::::::::::"+orderBycondition);
		
		return  orderBycondition;
		
	}
	public String customFilterInsert(){
		try{
			if(request.getParameter("modulevalue")!=null){
				String module=request.getParameter("modulevalue").toString();
				if(module.equals(MODULE_PENDING)){
					Long fkmodule=getCodeListPkByTypeAndSubtype(FILTER_MODULE,PENDING_SCREEN);
					filterdatapojo.setFk_module(fkmodule);
				}
				Long customfil=getCodeListPkByTypeAndSubtype(FILTER_TYPE,CUSTOM_FILTER);
				filterdatapojo.setFk_filter_type(customfil);
			}
			if(request.getParameter("fparam")!=null){
				String temp=request.getParameter("fparam").toString();
				String temp1=temp.replaceAll(REPLACE_STR, "&");
				filterdatapojo.setFilter_paraameter(temp1);
			}
			if(request.getParameter("fkuser")!=null){
				String userId=request.getParameter("fkuser");
				filterdatapojo.setFk_user(Long.parseLong(userId));
				filterdatapojo.setCreated_by(Long.parseLong(userId));
				filterdatapojo.setCreated_on(new Date());
			}
			if(request.getParameter("filname")!=null){
				String temp=request.getParameter("filname").toString();
				
				filterdatapojo.setFilter_name(temp);
			}
			if(request.getParameter("subModule")!=null){
				String temp=request.getParameter("subModule").toString();
				filterdatapojo.setFk_sub_module(Long.parseLong(temp));
			}
			
			customFilter=linkController.customFilterInsert(filterdatapojo);
			//log.debug("filterdatapojo.getFk_sub_module():::::::::::::::::::::"+filterdatapojo.getFk_sub_module());
			
			if(filterdatapojo.getFk_sub_module()==getCodeListPkByTypeAndSubtype(SUBMODULE_PENDING, SUBMODULE_PENDING_ORDERS)){
				if(customFilter!=null && customFilter.size()>0){
					//log.debug("\n\n\n\n\n\n\n\nOrders customFilter size in action"+customFilter.size());
					setCustomFilter(customFilter);
				}
			}
			if(filterdatapojo.getFk_sub_module()==getCodeListPkByTypeAndSubtype(SUBMODULE_PENDING, SUBMODULE_PENDING_NACBU)){
				if(customFilter!=null && customFilter.size()>0){
					//log.debug("\n\n\n\n\n\n\n\nNA CBU customFilter size in action"+customFilter.size());
					setCustomFilternacbu(customFilter);
				}
			}
			
			if(filterdatapojo.getFk_sub_module()==getCodeListPkByTypeAndSubtype(SUBMODULE_PENDING, SUBMODULE_PENDING_SIP)){
				if(customFilter!=null && customFilter.size()>0){
					//log.debug("\n\n\n\n\n\n\n\nSIP customFilter size in action"+customFilter.size());
					setCustomFiltersip(customFilter);
				}
			}
			
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	public String getDataforSelectedCol(){
		//log.debug("******************************************"+request.getParameter("colName"));
		String colName=request.getParameter("colName");
		String orderType=request.getParameter("orderType");
		//log.debug("selected column isssssssssss;;"+colName);
		//log.debug("selected column isssssssssss;;"+orderType);
		if(colName==null || colName.equals("")){
			
		}
		else{
			//log.debug("colName : "+colName);
			setColValuesList(velosHelper.getColValues(colName,orderType));
			setOrderTypeList(velosHelper.getOrdersList());
		}
		//log.debug("returnned lis object is"+getColValuesList());
		return "success";
	}
	
	public String getResultsForFilter(){
		String orderType=request.getParameter("orderType");
		String colData=request.getParameter("filterData");
		String colName=request.getParameter("filterCol");
		String pkOrderType=request.getParameter("pkOrderType");
		//log.debug(orderType+"*****"+colData+"*****"+colName+"********"+pkOrderType);
		//log.debug(orderType+"*****"+colData+"*****"+colName+"********"+pkOrderType);
		try {
			setSearchDataList(linkController.getResultsForFilter(orderType, colName, colData, pkOrderType, PENDING,ORDER_STATUS));
			setOrderTypeList(velosHelper.getOrdersList());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return "success";
	}
	
	
	public String getOrderDetails(){
		//"orderId="+orderId+"&cbuId="+cbuId+"&orderType="+orderType+"&orderStatus="+orderStatus+"&reqDate="+reqDate+"&lstUpdDate"+lstUpdDate
		setOrderId(request.getParameter("orderId"));
		setOrderType(request.getParameter("orderType"));
		setPkcord(request.getParameter("pkcordId"));
		//log.debug(getOrderId()+"************"+getPkcord()+"*********"+getOrderType());
		try {
			setOrderDetailsList(linkController.getOrderDetails(getPkcord()));
			setCtOrderDetailsList(linkController.getCtOrderDetails(getOrderId()));
			String tempUnavail = cbbController.getCodeListPkIds(RESOLUTION_CODE, TU_SUBTYPE).toString();
			String canceled = cbbController.getCodeListPkIds(RESOLUTION_CODE, CA_SUBTYPE).toString();
			//log.debug("tempUnavail value"+tempUnavail);
			//log.debug("cancelled value"+canceled);
			setPkTempUnavail(tempUnavail);
			setPkCanceled(canceled);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
			e.printStackTrace();
		}
		
		return "success";
	}
	
	public String updateCordStatus(){
		//log.debug("request value is:::"+request.getParameter("orderId").toString());
		cdrCbuPojo.setCordID(Long.parseLong(request.getParameter("orderId").toString()));
		try {
			setCdrCbuPojo(new CBUController().getCordInfoById(getCdrCbuPojo()));
			//log.debug("order id is:::"+cdrCbuPojo.getCordID());
			if(cdrCbuPojo.getCordID()!=null){
				//log.debug("in cordId not null");
			setOrderId(cdrCbuPojo.getCordID().toString());
			}
			else{
				setOrderId("");
			}
			if(cdrCbuPojo.getFkCordCbuStatus()!=null){
				//log.debug("in cordstatus not null");
			setOrderStatus(cdrCbuPojo.getFkCordCbuStatus().toString());
			}else{
				setOrderStatus("");
			}
			//setStatReason(cdrCbuPojo.getCordUnavailReason());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
			e.printStackTrace();
		}
		
		return "success";
	}

	public String uploadDocument()throws Exception{
		if(request.getParameter("orderId")!=null && !request.getParameter("orderId").equals("")){
			setOrderId(request.getParameter("orderId"));
		}
		if(request.getParameter("orderType")!=null && !request.getParameter("orderType").equals("")){
			setOrderType(request.getParameter("orderType"));
		}
		
		if(cdrCbuPojo.getCordID()!=null){
			cdrCbuPojo = cbuController.getCordInfoById(cdrCbuPojo);
			setCdrCbuPojo(cdrCbuPojo);
			//log.debug("-------------------"+cdrCbuPojo.getCordID());
		}
		setDocCategWithRights(setCategoriesWithGroupRights());
		return "success";
	}
	
	public String uploadFinalDocument(){
		setTestNameList(populateDropdownValues());
		getRequest().setAttribute("subcatid", getLabTestName());
		getRequest().setAttribute("categoryId", getCategoryName());
		return SUCCESS;
	}
	
	public String modifyUnitReport()throws Exception{
		String cordID=request.getParameter("cbuCordId");
		getCdrCbuPojo().setCordID(Long.parseLong(cordID));
		setDocCategWithRights(setCategoriesWithGroupRights());
		//log.debug("attachmentid==========================="+attachmentId);
		//log.debug("modifyunitreport==========================="+modifyUnitrpt);
		//log.debug("cordId==========================="+cbuCordId);
		return SUCCESS;
	}
	
	public String revokeUnitrptDoc() throws Exception{
		Long cbuCordId = (Long.parseLong(request.getParameter("cbuCordId")));
		Date todayDate = new Date();
		HttpSession session = request.getSession(true);
		userObject=(UserJB)session.getAttribute(MidErrorConstants.GARUDA_USER);
		Long attachmentId=null;
		try{
		 attachmentId = (Long.parseLong(request.getParameter("attachmentId")));
			}
			catch(Exception e)
			{
				log.error(e.getMessage());
			e.printStackTrace();
			}
		if(attachmentId!=null){
			Attachment revokeAttachment = new Attachment();
			try{
			List<Attachment> attachmentInfoLst = new AttachmentController().getAttachmentInfoLst(attachmentId);
			if(attachmentInfoLst!=null)
			{
			
				revokeAttachment = attachmentInfoLst.get(0);
			}
			attachmentPojo = new AttachmentPojo();
			attachmentPojo = (AttachmentPojo) ObjectTransfer.transferObjects(revokeAttachment, attachmentPojo);
			//log.debug("object============="+attachmentPojo);
			attachmentPojo.setAttachmentId(attachmentPojo.getAttachmentId());
			attachmentPojo.setLastModifiedOn(todayDate);
			attachmentPojo.setLastModifiedBy((long)userObject.getUserId());
			attachmentPojo.setActionFlag(DOC_REVOKE);
			attachmentPojo.setDeletedFlag(true);
			new AttachmentController().addAttachments(attachmentPojo);
			CBUUnitReport cbuUnitReport = new CBUUnitReport();
			List<CBUUnitReport> cbuUnitReportLst = new CBUUnitReportController().getCbuUnitReportLst(cbuCordId,attachmentId);
			if(cbuUnitReportLst!=null)
			{
				if(cbuUnitReportLst.size() > 0 )
					cbuUnitReport= cbuUnitReportLst.get(0);
			
			cbuUnitReportPojo = new CBUUnitReportPojo();
			cbuUnitReportPojo = (CBUUnitReportPojo)ObjectTransfer.transferObjects(cbuUnitReport, cbuUnitReportPojo);
			cbuUnitReportPojo.setPkCordExtInfo(cbuUnitReportPojo.getPkCordExtInfo());
			cbuUnitReportPojo.setLastModifiedBy((long)userObject.getUserId());
			cbuUnitReportPojo.setLastModifiedOn(todayDate);
			cbuUnitReportPojo.setActionFlag(DOC_REVOKE);
			cbuUnitReportPojo.setDeletedFlag(true); 
			new CBUUnitReportController().saveCBUUnitReport(cbuUnitReportPojo);
			}
			}
			catch(Exception e){
				log.error(e.getMessage());
				e.printStackTrace();
				}
			}
		getRequest().setAttribute("submit", "revokeunitrpt");
		try {
			getCdrCbuPojo().setCordID(cbuCordId);
			cdrCbuPojo.setInEligiblePkid(getCodeListPkByTypeAndSubtype(ELIGIBILITY_STATUS, INELIGIBLE_REASON).intValue());
			cdrCbuPojo.setIncompleteReasonId(getCodeListPkByTypeAndSubtype(ELIGIBILITY_STATUS, INCOMPLETE_REASON).intValue());
			setCdrCbuPojo(new CBUController().getCordInfoById(getCdrCbuPojo()));
			List list = velosHelper.getObjectList(CBUUNITREPORT_OBJECT," fkCordCdrCbuId = '"+cbuCordId+"' and attachmentId != null");
			setUnitReportList(list);
			//getUploadedDocumentData();
			//new CBUAction().refreshMain(cbuCordId);
			}catch(Exception e){
				log.error(e.getMessage());
				e.printStackTrace();
			}
			setForuploadDoc(true);
			//log.debug("after saveDocumentWithCategory #######################################################################################:::"+cdrCbuPojo.getCordID());
			if(request.getParameter("orderId")!=null && !request.getParameter("orderId").equals("") && !request.getParameter("orderId").equals("undefined") && request.getParameter("orderType")!=null && !request.getParameter("orderType").equals("") && !request.getParameter("orderType").equals("undefined")){
				cbuaction.viewOrderDetails();
				//log.debug("after viewOrderDetails #######################################################################################");
			}else{
				cbuaction.setCordDataWithRights();
				//log.debug("after setCordDataWithRights #######################################################################################");
			}
			
			//-------------------------------
			List fcrList = null;
			if(getCdrCbuPojo()!=null && getCdrCbuPojo().getCordID()!=null){
				fcrList = velosHelper.getObjectList(CBU_FINAL_REVIEW_OBJECT," fkCordId ="+cdrCbuPojo.getCordID()+" order by pkCordFinalReview desc"); 
			}
			CBUFinalReview cbuFinalReview = null;
			if(fcrList!=null && fcrList.size()!=0){
				cbuFinalReview = (CBUFinalReview)fcrList.get(0);
			}
			if(cbuFinalReview!=null){
				cbuFinalReviewPojo = (CBUFinalReviewPojo)ObjectTransfer.transferObjects(cbuFinalReview, cbuFinalReviewPojo);
			}
			//log.debug("cbuFinalReviewPojo::::::::::"+cbuFinalReviewPojo);
			
			if(getCbuFinalReviewPojo()!=null && getCbuFinalReviewPojo().getFinalReviewConfirmStatus()!=null && getCbuFinalReviewPojo().getFinalReviewConfirmStatus().equals("Y") && getCdrCbuPojo()!=null && getCdrCbuPojo().getSite()!=null && !getCdrCbuPojo().getSite().isCDRUser()){
				List list = getAttachments(getCdrCbuPojo().getCordID(), getCodeListPkByTypeAndSubtype(CATEGORY, CBU_ASMNT_CODESUBTYPE),null);
				if(list!=null && list.size()==0){
					log.debug("List size:::"+list.size());
					log.debug("only invoking for non system cords when the FCR is completed and there is no document for "+CBU_ASMNT_CODESUBTYPE+" category.");
					Date dd = new Date();
					Long useridd  = (long)userObject.getUserId();
					cbuFinalReviewPojo.setFkCordId(cdrCbuPojo.getCordID());
					cbuFinalReviewPojo.setConfirmCBBSignatureFlag(0l);
					cbuFinalReviewPojo.setLicensureConfirmFlag(0l);
					cbuFinalReviewPojo.setEligibleConfirmFlag(0l);
					cbuFinalReviewPojo.setReviewCordAcceptance(null);
					cbuFinalReviewPojo.setFinalReviewConfirmStatus("N");
					cbuFinalReviewPojo.setConfirmCBBSignCreatedOn(dd);
					cbuFinalReviewPojo.setConfirmCBBSignCreator(useridd);
					cbuFinalReviewPojo.setLicCreatedOn(dd);
					cbuFinalReviewPojo.setLicCreatedBy(useridd);
					cbuFinalReviewPojo.setEligCreatedOn(dd);
					cbuFinalReviewPojo.setEligCreatedBy(useridd);
					cbuFinalReviewPojo = cbuController.saveCBUFinalReview(cbuFinalReviewPojo);
					setCbuFinalReviewPojo(cbuFinalReviewPojo);
				}
			}
			//-------------------------------
			//log.debug("getFCRCompletedFlag()::::::::::"+cbuaction.getFCRCompletedFlag());
			setFCRCompletedFlag(cbuaction.getFCRCompletedFlag());
			//log.debug("getFCRCompletedFlag()::::::::::"+getFCRCompletedFlag());
			//log.debug("getMCCompletedFlag():::::::::::"+cbuaction.getMCCompletedFlag());
			setMCCompletedFlag(cbuaction.getMCCompletedFlag());
			//log.debug("getMCCompletedFlag():::::::::::"+getMCCompletedFlag());
			//log.debug("getCBUAssessmentFlag():::::::::"+cbuaction.getCBUAssessmentFlag());
			setCBUAssessmentFlag(cbuaction.getCBUAssessmentFlag());
			//log.debug("getCBUAssessmentFlag():::::::::"+getCBUAssessmentFlag());
			//log.debug("getOrderPojo().getPk_OrderId():"+cbuaction.getOrderPojo().getPk_OrderId());
			setOrderPojo(cbuaction.getOrderPojo());
			//log.debug("getOrderPojo().getPk_OrderId():"+getOrderPojo().getPk_OrderId());
			//log.debug("cbuaction.getCbuFinalReviewPojo():::::::"+cbuaction.getCbuFinalReviewPojo());
			setCbuFinalReviewPojo(cbuFinalReviewPojo);
			//log.debug("getCbuFinalReviewPojo():::::::"+getCbuFinalReviewPojo());
			//log.debug("cbuaction.getCbuCompleteReqInfoPojo():::::::"+cbuaction.getCbuCompleteReqInfoPojo());
			setCbuCompleteReqInfoPojo(cbuaction.getCbuCompleteReqInfoPojo());
			//log.debug("getCbuCompleteReqInfoPojo():::::::"+getCbuCompleteReqInfoPojo());
		return SUCCESS;
	}
	
	public void modifyDocumentWithCategory(Long cordId,Long cbuCordId){
		Date todayDate = new Date();
		HttpSession session = request.getSession(true);
		userObject=(UserJB)session.getAttribute(MidErrorConstants.GARUDA_USER);
		if(attachmentId!=null){
			Attachment modifyAttachment = new Attachment();
			try{
			List<Attachment> attachmentInfoLst = new AttachmentController().getAttachmentInfoLst(attachmentId);
			if(attachmentInfoLst!=null)
			{
			
				modifyAttachment = attachmentInfoLst.get(0);
			}
			attachmentPojo = new AttachmentPojo();
			attachmentPojo = (AttachmentPojo) ObjectTransfer.transferObjects(modifyAttachment, attachmentPojo);
			//log.debug("object============="+attachmentPojo);
			attachmentPojo.setAttachmentId(attachmentPojo.getAttachmentId());
			attachmentPojo.setLastModifiedOn(todayDate);
			attachmentPojo.setLastModifiedBy((long)userObject.getUserId());
			attachmentPojo.setActionFlag(DOC_MODIFY);
			attachmentPojo.setDeletedFlag(true);
			new AttachmentController().addAttachments(attachmentPojo);
			AttachmentPojo attachmentPojo1 = new AttachmentPojo();
			attachmentPojo1 = (AttachmentPojo) ObjectTransfer.transferObjects(modifyAttachment, attachmentPojo1);
			attachmentPojo1.setAttachmentId(null);
			attachmentPojo1.setGarudaEntityId(cordId);
			attachmentPojo1 = new AttachmentController().addAttachments(attachmentPojo1);
			CBUUnitReport cbuUnitReport = new CBUUnitReport();
			List<CBUUnitReport> cbuUnitReportLst = new CBUUnitReportController().getCbuUnitReportLst(cbuCordId,attachmentId);
			if(cbuUnitReportLst!=null)
			{
				if(cbuUnitReportLst.size() > 0)
					cbuUnitReport= cbuUnitReportLst.get(0);
			
			
			cbuUnitReportPojo = new CBUUnitReportPojo();
			cbuUnitReportPojo = (CBUUnitReportPojo)ObjectTransfer.transferObjects(cbuUnitReport, cbuUnitReportPojo);
			cbuUnitReportPojo.setPkCordExtInfo(cbuUnitReportPojo.getPkCordExtInfo());
			cbuUnitReportPojo.setLastModifiedOn(todayDate);
			cbuUnitReportPojo.setLastModifiedBy((long)userObject.getUserId());
			cbuUnitReportPojo.setActionFlag(DOC_MODIFY);
			cbuUnitReportPojo.setDeletedFlag(true); 
			new CBUUnitReportController().saveCBUUnitReport(cbuUnitReportPojo);
			}
			if(getRequest().getParameter("categoryId").equals(cbbController.getCodeListPkIds(CATEGORY, UNIT_REPORT_CODESUBTYPE).toString())){
			CBUUnitReportPojo cbuUnitReportPojo1 = new CBUUnitReportPojo();
			 cbuUnitReportPojo1 = (CBUUnitReportPojo)ObjectTransfer.transferObjects(cbuUnitReport, cbuUnitReportPojo1);
			 
			 //cbuUnitReportPojo = getCbuUnitReportPojo();
			 cbuUnitReportPojo1.setPkCordExtInfo(null);
			 cbuUnitReportPojo1.setDeletedFlag(null);
			 cbuUnitReportPojo1.setFkCordCdrCbuId(cordId);
			 cbuUnitReportPojo1.setAttachmentId(attachmentPojo1.getAttachmentId());
			// cbuUnitReportPojo1 = getCbuUnitReportPojo();
			 cbuUnitReportPojo1 = new CBUUnitReportController().saveCBUUnitReport(cbuUnitReportPojo1);
			}
			 CBUploadInfo cbUploadInfo1 = new CBUploadInfo();
			 List<CBUploadInfo> cbUploadInfoLst =  new UploadDocController().getCbUploadInfoLst(attachmentId);
			 if(cbUploadInfoLst!=null)
				{
					if(cbUploadInfoLst.size() > 0)
						cbUploadInfo1 = cbUploadInfoLst.get(0);
				}
			 CBUploadInfoPojo cbUploadInfoPojo1 = new CBUploadInfoPojo();
			 cbUploadInfoPojo1 = (CBUploadInfoPojo)ObjectTransfer.transferObjects(cbUploadInfo1, cbUploadInfoPojo1);
			 //setCbUploadInfoPojo(cbUploadInfoPojo);
			 cbUploadInfoPojo1.setPk_UploadInfo(cbUploadInfoPojo1.getPk_UploadInfo());
			 cbUploadInfoPojo1.setDeletedFlag(true);
			 linkController.saveCatUploadInfo(cbUploadInfoPojo1);
			 
			 
			 String subCatIds = getRequest().getParameter("subcatid");
				//log.debug("In save method subCat "+ subCatIds);
				
				String categoryId = getRequest().getParameter("categoryId");
				//log.debug("In save method category "+ categoryId);
				getSession(getRequest()).setAttribute("categoryId", categoryId);
				
				// attachmentId = attachmentPojo.getAttachmentId();

				// Get the order Ids from JSP
				
				
			if(!categoryId.equals("")) {
					
					//CBUploadInfoPojo cbUploadInfoPojo = new CBUploadInfoPojo();
					//setCbUploadInfoPojo(cbUploadInfoPojo);
					getCbUploadInfoPojo().setDescription(getCbUploadInfoPojo().getDescription());
					//log.debug("IN Save CBUPLOAD INfo::::::::::;;;22222222222222");
					getCbUploadInfoPojo().setFk_CategoryId(Long.parseLong(categoryId.toString()));
					if(subCatIds != null && !subCatIds.equals("")){
						getCbUploadInfoPojo().setFk_SubCategoryId(Long.parseLong(subCatIds.toString()));
					}
						
						if(getCbUploadInfoPojo().getStrcompletionDate() != null && !getCbUploadInfoPojo().getStrcompletionDate().equals("")) {
							getCbUploadInfoPojo().setCompletionDate(new Date(Utilities.getFormattedDate(new Date(cbUploadInfoPojo.getStrcompletionDate()))));
							}
						if(getCbUploadInfoPojo().getStrtestDate() != null && !getCbUploadInfoPojo().getStrtestDate().equals("")) {
							getCbUploadInfoPojo().setTestDate(new Date(Utilities.getFormattedDate(new Date(cbUploadInfoPojo.getStrtestDate()))));
							}
						if(getCbUploadInfoPojo().getStrprocessDate() != null && !getCbUploadInfoPojo().getStrprocessDate().equals("")) {
							getCbUploadInfoPojo().setProcessDate(new Date(Utilities.getFormattedDate(new Date(cbUploadInfoPojo.getStrprocessDate()))));
							}
						if(getCbUploadInfoPojo().getStrreceivedDate() != null && !getCbUploadInfoPojo().getStrreceivedDate().equals("")) {
							getCbUploadInfoPojo().setReceivedDate(new Date(Utilities.getFormattedDate(new Date(cbUploadInfoPojo.getStrreceivedDate()))));
							}
						if(getCbUploadInfoPojo().getStrreportDate()!= null && !getCbUploadInfoPojo().getStrreportDate().equals("")){
							getCbUploadInfoPojo().setReportDate(new Date(Utilities.getFormattedDate(new Date(cbUploadInfoPojo.getStrreportDate()))));
						}
						//Get the attachment information for every lab test and save for every test id
								
								getCbUploadInfoPojo().setFk_AttachmentId(attachmentPojo1.getAttachmentId());
								
								//getPatientLabPojo().setTestEntityId(testEntityId);
								//getPatientLabPojo().setTestEntityType(testEntityType);
								cbUploadInfoPojo = getCbUploadInfoPojo();
								linkController.saveCatUploadInfo(cbUploadInfoPojo);
								/*request.setAttribute("cbuCordId", cbuCordId);
								new CBUAction().getMain2CbuDetails();*/
				}
			 
			}catch(Exception e){
				log.error(e.getMessage());
				e.printStackTrace();
			}
			}
			
			
		}
	
	
	public String saveFinalCBUCategoryDetails() throws Exception{
		//log.debug("modify document=========================="+getModifyUnitrpt());
		if(getModifyUnitrpt()!=null && getModifyUnitrpt().equals("True")){
			
			List cbuInfoLst = new CBUUnitReportAction().getCbuInfoById(CORD_CDR_REGISTERY_ID, revokeToId);
			if(cbuInfoLst!=null){
				if(cbuInfoLst.size() > 0)
				{
					cdrCbu =(CdrCbu) cbuInfoLst.get(0);
				}
			getCdrCbuPojo().setCordID(cdrCbu.getCordID());
			//log.debug("returncordId=========================================="+cdrCbu.getCordID());
			}
			
			modifyDocumentWithCategory(cdrCbu.getCordID(),cbuCordId);			
			getRequest().setAttribute("submit", "modifyunitrpt");
			try {
				getCdrCbuPojo().setCordID(cbuCordId);
				cdrCbuPojo.setInEligiblePkid(getCodeListPkByTypeAndSubtype(ELIGIBILITY_STATUS, INELIGIBLE_REASON).intValue());
				cdrCbuPojo.setIncompleteReasonId(getCodeListPkByTypeAndSubtype(ELIGIBILITY_STATUS, INCOMPLETE_REASON).intValue());
				setCdrCbuPojo(new CBUController().getCordInfoById(getCdrCbuPojo()));
				List list = velosHelper.getObjectList(CBUUNITREPORT_OBJECT," fkCordCdrCbuId = '"+cbuCordId+"'and attachmentId != null");
				setUnitReportList(list);
				}catch(Exception e){
					log.error(e.getMessage());
					e.printStackTrace();
				}				
		}
		else{
		saveDocumentWithCategory();
		try {
			if(cdrCbuPojo.getCordID()!=null){
			cdrCbuPojo = new CBUController().getCordInfoById(cdrCbuPojo);
			}
			long inEligiblePkCode = new CBUController().getCodeListPkIds(ELIGIBILITY_STATUS, INELIGIBLE_REASON);
			cdrCbuPojo.setInEligiblePkid((int)inEligiblePkCode);
			cdrCbuPojo.setIncompleteReasonId(new CBUController().getCodeListPkIds(ELIGIBILITY_STATUS, INCOMPLETE_REASON).intValue());
			cdrCbuPojo.setNotCollectedToPriorReasonId(new CBUController().getCodeListPkIds(ELIGIBILITY_STATUS, NOT_COLLECTED_TO_PRIOR_REASON).intValue());		
			if(cdrCbuPojo.getFkCordCbuEligible()!=null){
				cdrCbuPojo.setEligibilitydes(getCodeListDescById(cdrCbuPojo.getFkCordCbuEligible()));				
			}
			setReasonPojos(new CBUController().getReasonsByEntityStatusId(cdrCbuPojo.getFkCordCbuEligibleReason()));	
			List<ClinicalNotePojo> assessment = new CBUAction().getNotesByAssessmnetAndNoteId(getCdrCbuPojo().getCordID(), com.velos.ordercomponent.util.VelosGarudaConstants.INELIGIBLE_REASON);
		    getRequest().setAttribute("assessment", assessment);
		    CBUFinalReviewPojo cbuFinalReviewPojo = new CBUFinalReviewPojo();
		    cbuFinalReviewPojo = new CBUAction().getCbufinalReviewObj(cdrCbuPojo, getOrderId());
		    setDeclPojo(cbuController.getEligibilityDeclQuestionsByEntity(getCdrCbuPojo().getCordID(), getCodeListPkByTypeAndSubtype(ENTITY_CBU_CODE_TYPE, ENTITY_CBU_CODE_SUB_TYPE),cbuFinalReviewPojo.getFkFinalEligQuesId()));
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		
		}
		setForuploadDoc(true);
		//log.debug("after saveDocumentWithCategory #######################################################################################:::"+cdrCbuPojo.getCordID());
		if(request.getParameter("orderId")!=null && !request.getParameter("orderId").equals("") && !request.getParameter("orderId").equals("undefined") && request.getParameter("orderType")!=null && !request.getParameter("orderType").equals("") && !request.getParameter("orderType").equals("undefined")){
			cbuaction.viewOrderDetails();
			//log.debug("after viewOrderDetails #######################################################################################");
		}else{
			cbuaction.setCordDataWithRights();
			//log.debug("after setCordDataWithRights #######################################################################################");
		}
		//log.debug("getFCRCompletedFlag()::::::::::"+cbuaction.getFCRCompletedFlag());
		setFCRCompletedFlag(cbuaction.getFCRCompletedFlag());
		//log.debug("getFCRCompletedFlag()::::::::::"+getFCRCompletedFlag());
		//log.debug("getMCCompletedFlag():::::::::::"+cbuaction.getMCCompletedFlag());
		setMCCompletedFlag(cbuaction.getMCCompletedFlag());
		//log.debug("getMCCompletedFlag():::::::::::"+getMCCompletedFlag());
		//log.debug("getCBUAssessmentFlag():::::::::"+cbuaction.getCBUAssessmentFlag());
		setCBUAssessmentFlag(cbuaction.getCBUAssessmentFlag());
		//log.debug("getCBUAssessmentFlag():::::::::"+getCBUAssessmentFlag());
		//log.debug("getOrderPojo().getPk_OrderId():"+cbuaction.getOrderPojo().getPk_OrderId());
		setOrderPojo(cbuaction.getOrderPojo());
		//log.debug("getOrderPojo().getPk_OrderId():"+getOrderPojo().getPk_OrderId());
		//log.debug("cbuaction.getCbuFinalReviewPojo():::::::"+cbuaction.getCbuFinalReviewPojo());
		setCbuFinalReviewPojo(cbuaction.getCbuFinalReviewPojo());
		//log.debug("getCbuFinalReviewPojo():::::::"+getCbuFinalReviewPojo());
		//log.debug("cbuaction.getCbuCompleteReqInfoPojo():::::::"+cbuaction.getCbuCompleteReqInfoPojo());
		setCbuCompleteReqInfoPojo(cbuaction.getCbuCompleteReqInfoPojo());
		//log.debug("getCbuCompleteReqInfoPojo():::::::"+getCbuCompleteReqInfoPojo());
		return SUCCESS;
	}
	
	private void saveDocumentWithCategory() throws Exception{
		Long attachmentId = null;
		Long testEntityId = null;
		Long testEntityType = null;
		if(request.getParameter("orderId")!=null && !request.getParameter("orderId").equals("")){
			setOrderId(request.getParameter("orderId"));
			//log.debug("getOrderId()::::::::::::"+getOrderId());
		}
		if(request.getParameter("orderType")!=null && !request.getParameter("orderType").equals("")){
			setOrderType(request.getParameter("orderType"));
			//log.debug("getOrderType():::::::::::::::::"+getOrderType());
		}
		//String cbuId1 = getRequest().getParameter("cbuId");
		Long cbuId1=Long.parseLong(getRequest().getParameter("cbuCheck"));
		//log.debug("-----------Cord:::::::::::-------"+cbuId1);		
		getCdrCbuPojo().setCordID(cbuId1);        
		Long cordId = getCdrCbuPojo().getCordID();
	    //log.debug("-----------cordId-------"+cordId);
	    
	    List<AttachmentPojo> attachments = null;
		// Get the attachment Id from attachment POJO
		if(getSession(getRequest()).getAttribute("attachmentList")!=null){
				
				attachments = (List<AttachmentPojo>)getSession(getRequest()).getAttribute("attachmentList");
				//log.debug("Attachment Open Order Action"+attachments.size());
				if(attachments.size()>0){
					//log.debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
					for(AttachmentPojo a : attachments){
						//log.debug("**********************************************************");
						a.setGarudaEntityId(getCdrCbuPojo().getCordID());
						
					}
					attachments = new AttachmentController().addMultipleAttachments(attachments);
				    if(getRequest().getParameter("categoryId").equals(cbbController.getCodeListPkIds(CATEGORY, UNIT_REPORT_CODESUBTYPE).toString())){
				    	getCbuUnitReportPojo().setAttachmentId(attachments.get(0).getAttachmentId());
				    	if(getCdrCbuPojo().getCordID()!=null)
						{
							getCbuUnitReportPojo().setFkCordCdrCbuId(getCdrCbuPojo().getCordID());
							//getCordHlaExtPojo().setFkCordCdrCbuId(getCdrCbuPojo().getCordID());
						}
				    	cbuUnitReportPojo = getCbuUnitReportPojo();
				    	try{
				    		if(getCbuUnitReportPojo().getAttachmentId()!=null){
				    		linkControllerCbu.saveCBUUnitReport(cbuUnitReportPojo);
				    		}
				    		}catch(Exception e){
				    			log.error(e.getMessage());
				    			e.printStackTrace();
				    		}
				    		getRequest().setAttribute("submit", "unitreport");
				    }
					//getCbuUnitReportPojo().setAttachmentId(attachments.get(0).getAttachmentId());
			    } 
					
				
		}
				
		
		//String testName = getLabMaintPojo().getLabTestName();
		//log.debug("Test Name:::::::::::::" + testName);
		
		String subCatIds = getRequest().getParameter("subcatid");
		//log.debug("In save method subCat "+ subCatIds);
		
		String categoryId = getRequest().getParameter("categoryId");
		//log.debug("In save method category "+ categoryId);
		getSession(getRequest()).setAttribute("categoryId", categoryId);
		// attachmentId = attachmentPojo.getAttachmentId();

		// Get the order Ids from JSP
		

	if(!categoryId.equals("")) {			
            
			//log.debug("IN Save CBUPLOAD INfo::::::::::;;;333333333333");
			getCbUploadInfoPojo().setFk_CategoryId(Long.parseLong(categoryId.toString()));
			if(getCbUploadInfoPojo().getFk_CategoryId()!=null && StringUtils.isEmpty(getLoadDivName())){
				setLoadDivName(categoryDivMap.get(getCbUploadInfoPojo().getFk_CategoryId()));
				getSession(getRequest()).setAttribute("loadDivName", getLoadDivName());
				request.setAttribute("cbuDocCateg", getCodeListSubTypeById(cbUploadInfoPojo.getFk_CategoryId()));
			}else if(!StringUtils.isEmpty(getLoadDivName())){
				getSession(getRequest()).setAttribute("loadDivName", getLoadDivName());
				request.setAttribute("cbuDocCateg", getCodeListSubTypeById(cbUploadInfoPojo.getFk_CategoryId()));
			}
			if(subCatIds != null && !subCatIds.equals("")){
				getCbUploadInfoPojo().setFk_SubCategoryId(Long.parseLong(subCatIds.toString()));
			}
				
				if(getCbUploadInfoPojo().getStrcompletionDate() != null && !getCbUploadInfoPojo().getStrcompletionDate().equals("")) {
					getCbUploadInfoPojo().setCompletionDate(new Date(Utilities.getFormattedDate(new Date(cbUploadInfoPojo.getStrcompletionDate()))));
					}
				if(getCbUploadInfoPojo().getStrtestDate() != null && !getCbUploadInfoPojo().getStrtestDate().equals("")) {
					getCbUploadInfoPojo().setTestDate(new Date(Utilities.getFormattedDate(new Date(cbUploadInfoPojo.getStrtestDate()))));
					}
				if(getCbUploadInfoPojo().getStrprocessDate() != null && !getCbUploadInfoPojo().getStrprocessDate().equals("")) {
					getCbUploadInfoPojo().setProcessDate(new Date(Utilities.getFormattedDate(new Date(cbUploadInfoPojo.getStrprocessDate()))));
					}
				if(getCbUploadInfoPojo().getStrreceivedDate() != null && !getCbUploadInfoPojo().getStrreceivedDate().equals("")) {
					getCbUploadInfoPojo().setReceivedDate(new Date(Utilities.getFormattedDate(new Date(cbUploadInfoPojo.getStrreceivedDate()))));
					}
				if(getCbUploadInfoPojo().getStrreportDate()!= null && !getCbUploadInfoPojo().getStrreportDate().equals("")){
					getCbUploadInfoPojo().setReportDate(new Date(Utilities.getFormattedDate(new Date(cbUploadInfoPojo.getStrreportDate()))));
				}
				//Get the attachment information for every lab test and save for every test id
			if(attachments!=null) {
					for(int j=0;j<attachments.size();j++) {
						//log.debug(":::::::;attachments.size()"+attachments.size());
						attachmentId = attachments.get(j).getAttachmentId();
						testEntityId = attachments.get(j).getGarudaEntityId();
						testEntityType = attachments.get(j).getGarudaEntityType();
						
						getCbUploadInfoPojo().setFk_AttachmentId(attachmentId);
						
						//getPatientLabPojo().setTestEntityId(testEntityId);
						//getPatientLabPojo().setTestEntityType(testEntityType);
						cbUploadInfoPojo = getCbUploadInfoPojo();
						
						
						try {
							linkController.saveCatUploadInfo(cbUploadInfoPojo);
							
							// to do set dropdown values
						} catch (Exception e) {
							log.error(e.getMessage());
							e.printStackTrace();
						}
						if(getReasonPojo()!=null){
							if(getReasonPojo().getPkEntityStatusReason()!=null){
								CommonDao<EntityStatusReason> commonDao = new CommonDao<EntityStatusReason>(EntityStatusReason.class);
								EntityStatusReason reason = commonDao.findById(getReasonPojo().getPkEntityStatusReason());
								EntityStatusReasonPojo entityStatusReasonPojo = new EntityStatusReasonPojo();
								entityStatusReasonPojo = (EntityStatusReasonPojo)ObjectTransfer.transferObjects(reason, entityStatusReasonPojo);
								entityStatusReasonPojo.setFkAttachmentId(attachmentId);
								setReasonPojo(new CBUController().updateEntityStatusReason(entityStatusReasonPojo));
							}
						}
						
				}
				}
	}


	/*if (testName != null && testName.length() > 0) {

	String categoryId = getRequest().getParameter("categoryId");
	//log.debug("categoryId....." +categoryId);

	getLabMaintPojo().setLabTestName(testName);
	getLabMaintPojo().setFk_CategroyId(Long.parseLong(categoryId.toString()));
	labMaintPojo = getLabMaintPojo();

	try {
		linkController.saveNewLabTestInfo(labMaintPojo);
		//log.debug("Lab test id  after save"+getLabMaintPojo().getPk_LabTestId());
		Long labTestid = getLabMaintPojo().getPk_LabTestId();
		getPatientLabPojo().setFk_LabTestId(labTestid);
		
		
		//	Get the attachment information for every lab test and save for every test name
		if(attachments!=null) {
			for(int j=0;j<attachments.size();j++) {
				attachmentId = attachments.get(j).getAttachmentId();
				testEntityId = attachments.get(j).getGarudaEntityId();
				testEntityType = attachments.get(j).getGarudaEntityType();
				
				getPatientLabPojo().setFk_AttachmentId(attachmentId);
				getPatientLabPojo().setTestEntityId(testEntityId);
				getPatientLabPojo().setTestEntityType(testEntityType);
		
				patientLabPojo = getPatientLabPojo();
				linkController.savePatientTestInfo(patientLabPojo);
			}
		}
		
		
		
		
		
	} catch (Exception e) {
		e.printStackTrace();
	}*/

	//}

	getRequest().setAttribute("update", true);
	getCdrCbuPojo().setCdrCbuId(null);
	//searchCbuDetails();
	getCdrCbuPojo().setCordID(cordId);
	cdrCbuPojo.setInEligiblePkid(getCodeListPkByTypeAndSubtype(ELIGIBILITY_STATUS, INELIGIBLE_REASON).intValue());
	cdrCbuPojo.setIncompleteReasonId(getCodeListPkByTypeAndSubtype(ELIGIBILITY_STATUS, INCOMPLETE_REASON).intValue());
	setCdrCbuPojo(new CBUController().getCordInfoById(getCdrCbuPojo()));
	getUploadedDocumentData();
	request.setAttribute("cbuDocCateg", getCodeListSubTypeById(cbUploadInfoPojo.getFk_CategoryId()));
	
	}
	
	
	public String submitCancelOrder() throws Exception{
		//log.debug("---------inside submitCancelOrder-------------");
		String shedshipdate="";
		String vorderid=request.getParameter("orderid");
		String vcbuid=request.getParameter("cbuid");
		String cancelResult=request.getParameter("cancelRes");
		String pkresolved=cbbController.getCodeListPkIds(ORDER_STATUS, RESOLVED).toString();
		String pkavailable=cbbController.getCodeListPkIds(CORD_STATUS, AVAILABLE).toString();
		/*log.debug("\n cbuId "+vcbuid+
				"\n orderid "+vorderid+
				"\n pkresolved "+pkresolved+
				"\n pkavailable"+pkavailable+
				"\n cancelResult"+cancelResult);*/
		if(cancelResult.equals("N") || cancelResult.equals("Y")){
			UserJB user = (UserJB)getRequest().getSession().getAttribute(VelosGarudaConstants.GARUDA_USER);
			Long loggedIn_userId = (long) user.getUserId();
			if(cancelResult.equals("N")){
				orderPojo.setOrderCancellationAcceptFlag("N");
			}else if(cancelResult.equals("Y")){
				orderPojo.setOrderCancellationAcceptFlag("Y");	
			}
			//log.debug("Cancellation No"+cancelResult);
			orderPojo.setPk_OrderId(Long.parseLong(vorderid));
			orderPojo.setOrderCancelledBy(loggedIn_userId);
			orderPojo.setOrderStatus(Long.parseLong(pkresolved));
			orderPojo.setOrderCancellationConfirmDate(new Date());
			//order table update
			linkController.updateAcknowledgement(orderPojo);
			//cord table update
			shedshipdate=linkController.checkShedShipDt(vorderid,vcbuid,pkavailable); 
			//log.debug("shed shipdate"+shedshipdate);
			
			//Entity Status update
			if(cancelResult.equals("Y")){
				String entityType=cbbController.getCodeListPkIds(ENTITY_CBU_CODE_TYPE, ENTITY_ORDER_CODE_SUB_TYPE).toString();
				entityStatuspojo.setEntityId(Long.parseLong(vorderid));
				entityStatuspojo.setEntityType(Long.parseLong(entityType));
				entityStatuspojo.setStatusTypeCode(ORDER_STATUS);
				entityStatuspojo.setStatusValue(Long.parseLong(pkresolved));
				entityStatuspojo.setStatusDate(new Date());
				entityStatuspojo = cbuController.updateEntityStatus(entityStatuspojo);	
			}
		}
			
			//log.debug("After Committing no cancellation");
		if(cancelResult.equals("Y")){
			//log.debug("Cancellation Yes");
		}
		
		if(shedshipdate!=null){
			String tempstring="Cord is sheduled to ship on "+shedshipdate+" Order cannot be cancelled";
			setMsgShipDt(tempstring);
		}
		return "success";
	}
	public CBUController getCbuController() {
		return cbuController;
	}
	public void setCbuController(CBUController cbuController) {
		this.cbuController = cbuController;
	}

	public String getRegistryId() {
		return registryId;
	}

	public void setRegistryId(String registryId) {
		this.registryId = registryId;
	}
	
	public String updateAssignTo(){
		
		//log.debug("inside updateAssignTo");
		String ordId=request.getParameter("ordIdval");
		String assignto=request.getParameter("assigntoval");
		String flagval=request.getParameter("uflag");
		String cordIdval=request.getParameter("cordpk");
		UserJB user = (UserJB)getRequest().getSession().getAttribute(VelosGarudaConstants.GARUDA_USER);
		Long loggedIn_userId = (long) user.getUserId();
		
	    
	    	try {
				setCancelled_ack_orders(linkController.updateAssignTo(ordId,assignto,flagval,loggedIn_userId,cordIdval));
				log.debug("\n\n\n\n\n\ngetcancelled_ack_orders"+getCancelled_ack_orders());
				//System.out.println("\n\n\n\nIn action class cancelled_ack_ords:"+getCancelled_ack_orders());
				
				if(flagval.equals("acknowledge") || flagval.equals("assign_acknowledge")){
				
							if(getCancelled_ack_orders().equals("")){
								if(ordId.contains(",")){
									String[] ordId_arry=ordId.split(",");
									for(int i=0;i<ordId_arry.length;i++){	
										updateEntries(ordId_arry[i]);
									}
								}else{
									updateEntries(ordId);
								}
							}else{
								 
								String flag_cancel="0";
								if(getCancelled_ack_orders().contains(",")){
									String[] ordId_arry=ordId.split(",");
									String[] cancelled_ord=cancelled_ack_orders.split(",");
									
									if(ordId_arry.length!=cancelled_ord.length){
										
										for(int i=0;i<ordId_arry.length;i++){
											int k=0;
											flag_cancel="0";
											while(k<cancelled_ord.length){
												if(ordId_arry[i].equals(cancelled_ord[k]))
													flag_cancel="1";
												k++;
											}
											if(flag_cancel.equals("0"))
											updateEntries(ordId_arry[i]);
										}
										
									}
								}else{
									if(ordId.contains(",")){
										String[] ordId_arry=ordId.split(",");
										for(int i=0;i<ordId_arry.length;i++){
											if(!ordId_arry[i].equals(getCancelled_ack_orders()))
												updateEntries(ordId_arry[i]);
										}
									}else{
										if(!ordId.equals(getCancelled_ack_orders()))
											updateEntries(ordId);
									}
								}
								
								
							}
			 }
			} catch (Exception e) {
				// TODO Auto-generated catch block
				log.error(e.getMessage());
				e.printStackTrace();
			}
	    
		getPendingScreenOrders();
		
		return "success";
	}
	public void updateEntries(String orderId){
		
		try{
			UserJB user = (UserJB)getSession(getRequest()).getAttribute(VelosGarudaConstants.GARUDA_USER);	
		//System.out.println("orderIdvalue:::::::::::"+orderId);
    	long entityType= cbuController.getCodeListPkIds(ENTITY_CBU_CODE_TYPE, ENTITY_ORDER_CODE_SUB_TYPE);
    	entityStatuspojo.setEntityId(Long.parseLong(orderId));
    	entityStatuspojo.setEntityType(entityType);
    	entityStatuspojo.setStatusTypeCode(ORDER_STATUS);
    	entityStatuspojo.setStatusValue(cbuController.getCodeListPkIds(ORDER_STATUS,CLOSED));
    	entityStatuspojo.setStatusDate(new Date());
    	entityStatuspojo = cbuController.updateEntityStatus(entityStatuspojo);
    	Long pkval=linkController.getOrderType(Long.parseLong(orderId));
    	//System.out.println("pkvalue of order type in action class::::::"+pkval);
		cbuaction.workflowTasks(orderId,pkval,user);
		}catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
			e.printStackTrace();
		}
		
	}
	
	public List<CodeList> setCategoriesWithGroupRights()throws Exception{
		UserJB user = (UserJB)getSession(getRequest()).getAttribute(VelosGarudaConstants.GARUDA_USER);
		//Long siteId=Long.parseLong(user.getUserSiteId());
		Long cordID=getCdrCbuPojo().getCordID();
		if(cordID!=null){
			getCdrCbuPojo().setCordID(cordID);
			setCdrCbuPojo(new CBUController().getCordInfoById(getCdrCbuPojo()));
		}
		Long siteId= getCdrCbuPojo().getFkCbbId();
		List<CodeList> list = new ArrayList<CodeList>();
		Map<String,Long> map = new LinkedHashMap<String, Long>();
		List<CodeList> codelistV = (List<CodeList>)((Map<Long,CodeList>)getContext().getAttribute("codeListValues")).get(CATEGORY);
		Map<Long,CodeList> listMap = new HashMap<Long, CodeList>();
		if(codelistV!=null){
		    for(CodeList c : codelistV){
		    	listMap.put(c.getPkCodeId(), c);
			}
		}
		new CBUAction().setUserRightsForClinicalData(siteId);
		HttpSession tSession = request.getSession(true); 
		GrpRightsJB grpRights =null;
		if (tSession.getAttribute("LocalGRights")!=null){
		 grpRights = (GrpRightsJB) tSession.getAttribute("LocalGRights");
		}else
		{
		 grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
		}
		if((cdrCbuPojo.getSite().stausUnitReport()!= null )&&(cdrCbuPojo.getSite().stausUnitReport()== true )){
		map.put("CB_UNITCAT", getCodeListPkByTypeAndSubtype(CATEGORY, UNIT_REPORT_CODESUBTYPE));
		}
		else{
				map.remove("CB_UNITCAT");
		}
		map.put("CB_CBUINFOCAT", getCodeListPkByTypeAndSubtype(CATEGORY, CBU_INFORMATION_CODESUBTYPE));
		map.put("CB_LABSUMCAT", getCodeListPkByTypeAndSubtype(CATEGORY, LAB_SUMMARY_CODESUBTYPE));
		map.put("CB_PROINFOCAT", getCodeListPkByTypeAndSubtype(CATEGORY, PROCESSING_INFO_CODESUBTYPE));
		map.put("CB_IDMCAT", getCodeListPkByTypeAndSubtype(CATEGORY, INFECT_DIS_MARK_CODESUBTYPE));
		map.put("CB_HEALTHCAT", getCodeListPkByTypeAndSubtype(CATEGORY, HEALTH_HISt_SCR_CODESUBTYPE));
		map.put("CB_HLACAT", getCodeListPkByTypeAndSubtype(CATEGORY, HLA_CODESUBTYPE));
		map.put("CB_ELIGCAT", getCodeListPkByTypeAndSubtype(CATEGORY, ELIGIBILITY_CODESUBTYPE));
		map.put("CB_OTHRCAT", getCodeListPkByTypeAndSubtype(CATEGORY, OTHER_REC_CODESUBTYPE));
		map.put("CB_ASSMNTCAT", getCodeListPkByTypeAndSubtype(CATEGORY, CBU_ASMNT_CODESUBTYPE));
		map.put("CB_SHPMNTCAT", getCodeListPkByTypeAndSubtype(CATEGORY, SHIPMENT_ITER_CODESUBTYPE));
		map.put("CB_POSTSHIPCAT", getCodeListPkByTypeAndSubtype(CATEGORY, POST_SHPMNT_CODESUBTYPE));
		if(getCdrCbuPojo().getFkCordCbuStatus()!=null && ((getCdrCbuPojo().getFkCordCbuStatus().equals(getCbuStatusPkByCode(VelosBaseAction.SH))) || (getCdrCbuPojo().getFkCordCbuStatus().equals(getCbuStatusPkByCode(VelosBaseAction.IN))))){
			map.remove("CB_UNITCAT");
			map.remove("CB_CBUINFOCAT");
			map.remove("CB_LABSUMCAT");
			map.remove("CB_PROINFOCAT");
			map.remove("CB_IDMCAT");
			map.remove("CB_HEALTHCAT");
			map.remove("CB_HLACAT");
			map.remove("CB_ELIGCAT");
			map.remove("CB_OTHRCAT");
			map.remove("CB_ASSMNTCAT");
			map.remove("CB_SHPMNTCAT");
			//map.put("CB_POSTSHIPCAT", getCodeListPkByTypeAndSubtype(CATEGORY, POST_SHPMNT_CODESUBTYPE));
		}
		if(map!=null && grpRights!=null && listMap!=null){
			for(Map.Entry<String,Long> me : map.entrySet()){
					if(grpRights.getFtrRightsByValue(me.getKey())!=null && !(grpRights.getFtrRightsByValue(me.getKey()).equals(""))){
						Integer right = Integer.parseInt(grpRights.getFtrRightsByValue(me.getKey()));
						if((right>=5)&&(right!=6)){
							list.add(listMap.get(me.getValue()));
						}
				}
			}
		}		
		return list;
	}

	public void getUploadedDocumentData(){
		  try{
			 
		     Map map = cbuController.getCordUploadDocument(getCodeListPkByTypeAndSubtype(ATTACHMENT_CODE_TYPE,ATTACHMENT_CODE_SUB_TYPE_TST_REPORT), getCdrCbuPojo().getCordID(), getCodeListPkByTypeAndSubtype(ENTITY_CODE_TYPE, ENTITY_CODE_SUB_TYPE));
			 if(map!=null){
				 getRequest().setAttribute("categoryMap", map);
			 }
		  }catch(Exception e){
			  log.error(e.getMessage());
			  e.printStackTrace();
		  }		  
	  }
	
	public List<CodeList> setCategoriesWithGroupRightsForViewDoument()throws Exception{
		UserJB user = (UserJB)getSession(getRequest()).getAttribute(VelosGarudaConstants.GARUDA_USER);
		String cordID=request.getParameter("cordID");
		if(cordID!=null){
			getCdrCbuPojo().setCordID(Long.parseLong(cordID));
			setCdrCbuPojo(new CBUController().getCordInfoById(getCdrCbuPojo()));
		}
		//Long siteId=Long.parseLong(user.getUserSiteId());
		Long siteId= getCdrCbuPojo().getFkCbbId();
		List<CodeList> list = new ArrayList<CodeList>();
		Map<String,Long> map = new LinkedHashMap<String, Long>();
		List<CodeList> codelistV = (List<CodeList>)((Map<Long,CodeList>)getContext().getAttribute("codeListValues")).get(CATEGORY);
		Map<Long,CodeList> listMap = new HashMap<Long, CodeList>();
		if(codelistV!=null){
		    for(CodeList c : codelistV){
		    	listMap.put(c.getPkCodeId(), c);
			}
		}
		new CBUAction().setUserRightsForClinicalData(siteId);
		HttpSession tSession = request.getSession(true); 
		GrpRightsJB grpRights =null;
		if (tSession.getAttribute("LocalGRights")!=null){
		 grpRights = (GrpRightsJB) tSession.getAttribute("LocalGRights");
		}else
		{
		 grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
		}
		//log.debug("Unit Report------>"+getCdrCbuPojo().getSite().stausUnitReport());
		

		if((getCdrCbuPojo().getSite().stausUnitReport()!=null)&&(getCdrCbuPojo().getSite().stausUnitReport()== true)){
			map.put("CB_UNITCAT", getCodeListPkByTypeAndSubtype(CATEGORY, UNIT_REPORT_CODESUBTYPE));
			}
			else{
					map.remove("CB_UNITCAT");
			}
			map.put("CB_CBUINFOCAT", getCodeListPkByTypeAndSubtype(CATEGORY, CBU_INFORMATION_CODESUBTYPE));
			map.put("CB_LABSUMCAT", getCodeListPkByTypeAndSubtype(CATEGORY, LAB_SUMMARY_CODESUBTYPE));
			map.put("CB_PROINFOCAT", getCodeListPkByTypeAndSubtype(CATEGORY, PROCESSING_INFO_CODESUBTYPE));
			map.put("CB_IDMCAT", getCodeListPkByTypeAndSubtype(CATEGORY, INFECT_DIS_MARK_CODESUBTYPE));
			map.put("CB_HEALTHCAT", getCodeListPkByTypeAndSubtype(CATEGORY, HEALTH_HISt_SCR_CODESUBTYPE));
			map.put("CB_HLACAT", getCodeListPkByTypeAndSubtype(CATEGORY, HLA_CODESUBTYPE));
			map.put("CB_ELIGCAT", getCodeListPkByTypeAndSubtype(CATEGORY, ELIGIBILITY_CODESUBTYPE));
			map.put("CB_OTHRCAT", getCodeListPkByTypeAndSubtype(CATEGORY, OTHER_REC_CODESUBTYPE));
			map.put("CB_ASSMNTCAT", getCodeListPkByTypeAndSubtype(CATEGORY, CBU_ASMNT_CODESUBTYPE));
			map.put("CB_SHPMNTCAT", getCodeListPkByTypeAndSubtype(CATEGORY, SHIPMENT_ITER_CODESUBTYPE));
			map.put("CB_POSTSHIPCAT", getCodeListPkByTypeAndSubtype(CATEGORY, POST_SHPMNT_CODESUBTYPE));
		if(map!=null && grpRights!=null && listMap!=null){
			for(Map.Entry<String,Long> me : map.entrySet()){
				if(grpRights.getFtrRightsByValue(me.getKey())!=null && !(grpRights.getFtrRightsByValue(me.getKey()).equals(""))){
					Integer right = Integer.parseInt(grpRights.getFtrRightsByValue(me.getKey()));
					if(right>=4){
						list.add(listMap.get(me.getValue()));
					}
				}
			}
		}		
		return list;
	}

}