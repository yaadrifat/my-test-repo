package com.velos.ordercomponent.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.ordercomponent.business.domain.AppMessages;
import com.velos.ordercomponent.business.domain.CBUStatus;
import com.velos.ordercomponent.business.domain.CodeList;
import com.velos.ordercomponent.business.domain.Page;
import com.velos.ordercomponent.business.domain.Widget;
import com.velos.ordercomponent.business.pojoobjects.SearchPojo;
import com.velos.ordercomponent.helper.CodelstHelper;
import com.velos.ordercomponent.helper.OrderAttachmentHelper;
import com.velos.ordercomponent.helper.WidgetHelper;
import com.velos.ordercomponent.helper.velosHelper;

public class GarudaConfig {
	public static final Log log=LogFactory.getLog(GarudaConfig.class);
	public static ServletContext context = null;
    private static boolean initializeothers = false;
    private static Map<Long,CodeList> codeListMapByIds = null;	
    private static Map<String,List<CodeList>> codeListMap = null;
    private static Hashtable<String,StringBuffer> sessionAppMap = null;
    private static Map<Long,CBUStatus> cbuStatusValues = null;
	private static Map<String,CodeList> codeListTypeSubType = null;

	public static Map<String,List<CodeList>> getCodeListMap() {
		return codeListMap;
	}

	public static void setCodeListMap(Map<String,List<CodeList>> codeListMap) {
		GarudaConfig.codeListMap = codeListMap;
	}
	public static Map<Long, CodeList> getCodeListMapByIds() {
		return codeListMapByIds;
	}
 
	public static void setCodeListMapByIds(Map<Long, CodeList> codeListMapByIds) {
		GarudaConfig.codeListMapByIds = codeListMapByIds;
	}

	public static Hashtable<String,StringBuffer> getSessionAppMap() {
		return sessionAppMap;
	}

	public static void setSessionAppMap(Hashtable<String,StringBuffer> sessionAppMap) {
		GarudaConfig.sessionAppMap = sessionAppMap;
	}

	public static Map<Long,CBUStatus> getCbuStatusValues() {
		return cbuStatusValues;
	}

	public static void setCbuStatusValues(Map<Long,CBUStatus> cbuStatusValues) {
		GarudaConfig.cbuStatusValues = cbuStatusValues;
	}

	public static Map<String,CodeList> getCodeListTypeSubType() {
		return codeListTypeSubType;
	}

	public static void setCodeListTypeSubType(Map<String,CodeList> codeListTypeSubType) {
		GarudaConfig.codeListTypeSubType = codeListTypeSubType;
	}

	public static void initializeOthers(ServletContext context) {
			  
		if(!initializeothers)
		{
			initializeWidgets(context);
			initializeTheme(context);
			initializeCodeList(context);
			initializeCodeListMapWithIds(context);
			initializeSessionMap(context);
			initializeothers = true;
			initializeLoginAppMessages(context);
			initializeCbuStatus(context);
			initializeCodelistTypeSubType(context);
			ObjectFactory.getInstance();
		}
	}
	
	public static void initializeTheme(ServletContext context){
		//List<Theme> themes = null;
		//themes  = WidgetHelper.getThemes(null);
		//context.setAttribute("themes", themes);
	}
	
	public static void initializeWidgets(ServletContext context)
	{
		List<Page> pages = null;
		List<Widget> widgetList = new ArrayList<Widget>();
		List<Widget> widgetListByPageCode = null;
		Map<Long,List<Widget>> widgetInfo = new HashMap<Long,List<Widget>>();
		Page page = new Page();		
		widgetInfo = WidgetHelper.getWidgetInfoDetailsByPageCode(null);
		/*if(pages.size()>0 && pages!=null)
		{
			for(Page p : pages)
			{
				if(p.getWidget()!=null)
				{
					widgetList.add(p.getWidget());
				}
			}			
		}*/
		
		/*if(pages.size()>0 && pages!=null){
			for(Page pi : pages)
			{
				if(widgetList.size()>0)
				{
					for(Widget wg : widgetList){
					   if(pi.getPageCode()==wg.getId() || wi.getId().equals(wg.getId())){
						   widgetListByPageId = new ArrayList<Widget>();
							if(widgetInfo.containsKey(wi.getPageId())){
								widgetListByPageId = widgetInfo.get(wi.getPageId());
								widgetListByPageId.add(wg);
													
							}else{
								widgetListByPageId.add(wg);
								widgetInfo.put(wi.getPageId(), widgetListByPageId);
							}
						}
					}
				}
			}
		}*/
		//for(Map.Entry<Long,List<Widget>> m : widgetInfo.entrySet()){
		//	System.out.print("Map Info"+m.getKey()+"map value"+((List<Widget>)m.getValue()).get(0).getWidgetDivId());
		//	for(Widget w : (List<Widget>)m.getValue()){
		//		System.out.print("div value"+w.getWidgetDivId());
		//	}
		//}
		context.setAttribute("widgetInfoMap", widgetInfo);		
		
	}
	
	public static void initializeCodeList(ServletContext context)
	{
		codeListMap = new GarudaConfig().getCodeListValues();
		//for(Map.Entry<String,List<CodeList>> m : codeListMap.entrySet()){
		//	System.out.print("Map Info"+m.getKey()+"map value"+((List<CodeList>)m.getValue()).get(0).getType());
		//	for(CodeList w : (List<CodeList>)m.getValue()){
		//		System.out.print("Code List value"+w.getSubType());
		//	}
		//}
		Map<String,List<CodeList>> invalidCodeListMap = new GarudaConfig().getCodeListInvalidValues();
		context.setAttribute("codeListInvalidValues", invalidCodeListMap);
		context.setAttribute("codeListValues", codeListMap);
	}	
	
	public static void initializeCodeListMapWithIds(ServletContext context)
	{
		List<CodeList> codeLists = null;
		Map<Long,CodeList> codeListMap = new HashMap<Long, CodeList>();
		codeLists = new CodelstHelper().getCodeListForIds();
		if(codeLists!=null && codeLists.size()>0){
			for(CodeList c : codeLists){
				codeListMap.put(c.getPkCodeId(), c);
			}
	    }
		setCodeListMapByIds(codeListMap);
		context.setAttribute("codeListMapByIds", codeListMap);
	}
		
	public static void initializeLoginAppMessages(ServletContext context){
		//log.debug("******Intialized application messages for login page********");
		
		List<AppMessages> appMsgsList = null;
		List<Object> resultlst = new ArrayList<Object>();
		SearchPojo searchPojo = new SearchPojo();
		searchPojo.setSearchType(VelosGarudaConstants.APPMESSAGES_OBJECTNAME);
		Long pkLogOn = getCodeListPkByTypeAndSubtype(VelosGarudaConstants.PAGE_TYPE,VelosGarudaConstants.LOGIN_PAGE_TYPE,getCodeListMapByIds());
		Long pkInActiveMsg = getCodeListPkByTypeAndSubtype(VelosGarudaConstants.APPMSG_STATUS,VelosGarudaConstants.APPMSG_STATUS_INACTIVE,getCodeListMapByIds());
		context.setAttribute("pkInActiveMsg", pkInActiveMsg);
		if(pkLogOn!=null){
		searchPojo.setCriteria(" msgDispAt = '"+pkLogOn+"'");
		try{
			resultlst = velosHelper.getObjectList(searchPojo.getSearchType(), searchPojo.getCriteria());
			searchPojo.setResultlst(resultlst);
			appMsgsList = searchPojo.getResultlst();
			//log.debug("appMsgsList::::"+appMsgsList);
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}
		context.setAttribute("loginAppMsgsList", appMsgsList);
		}
	}
	
	public static Long getCodeListPkByTypeAndSubtype(String type, String subType,Map<Long,CodeList> codeListMapByIds){
		  Long pk = null;
		     for(Map.Entry<Long, CodeList> me : codeListMapByIds.entrySet()){
		    	 if((type.equals(me.getValue().getType()) || type==me.getValue().getType()) 
		    			 && (subType.equals(me.getValue().getSubType()) || subType==me.getValue().getSubType())){
		    		 pk = me.getKey();
		    	 }
		     }
		  return pk;
		}

	public static void initializeSessionMap(ServletContext context){
		Hashtable<String,StringBuffer> sessionMap = new Hashtable<String, StringBuffer>();
		setSessionAppMap(sessionMap);
		context.setAttribute("sessionAppMap", sessionMap);
	}
	
	public static void initializeCbuStatus(ServletContext context){
		Map<Long,CBUStatus> map = new HashMap<Long, CBUStatus>();
		List<CBUStatus> list = OrderAttachmentHelper.getAllCbuStatus();
		CBUStatus cbuStatus = null;
		for(CBUStatus c : list){
			map.put(c.getPkcbustatus(),c);
		}
		setCbuStatusValues(map);
		context.setAttribute("cbuStatusValues",map);
	}
	
    public Map<String,List<CodeList>> getCodeListValues(){
    	List<CodeList> codeLists = null;
		List<CodeList> codeListVal = null;
		CodeList codeList = null;
		Map<String,List<CodeList>> codeListMap = new HashMap<String, List<CodeList>>();
		codeLists = CodelstHelper.getCodeListValues();
		//log.debug("list size"+codeLists.size());
		for(CodeList c : codeLists)
		{
			codeList = new CodeList();
			if(codeListMap.containsKey(c.getType()))
			{
				codeListVal = codeListMap.get(c.getType());
				codeListVal.add(c);
			}else{
				codeListVal = new ArrayList<CodeList>();
				codeListVal.add(c);
				codeListMap.put(c.getType(), codeListVal);
			}
		}
		return codeListMap;
	}
    
    private static void initializeCodelistTypeSubType(ServletContext context) {
    	
		List<CodeList> codeLists = null;
		Map<String,CodeList> codeListMap = new HashMap<String, CodeList>();
		codeLists = new CodelstHelper().getCodeListForIds();
		if(codeLists!=null && codeLists.size()>0){
			for(CodeList c : codeLists){
				codeListMap.put((c.getType()+"-"+c.getSubType()), c);
			}
	    }
		setCodeListTypeSubType(codeListMap);
	}
    
    public Map<String,List<CodeList>> getCodeListInvalidValues(){
    	List<CodeList> codeLists = null;
		List<CodeList> codeListVal = null;
		CodeList codeList = null;
		Map<String,List<CodeList>> codeListMap = new HashMap<String, List<CodeList>>();
		codeLists = CodelstHelper.getCodeListInvalidValues();
		//log.debug("list size"+codeLists.size());
		for(CodeList c : codeLists)
		{
			codeList = new CodeList();
			if(codeListMap.containsKey(c.getType()))
			{
				codeListVal = codeListMap.get(c.getType());
				codeListVal.add(c);
			}else{
				codeListVal = new ArrayList<CodeList>();
				codeListVal.add(c);
				codeListMap.put(c.getType(), codeListVal);
			}
		}
		return codeListMap;
	}
}
