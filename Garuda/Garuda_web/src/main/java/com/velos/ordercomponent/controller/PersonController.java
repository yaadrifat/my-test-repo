package com.velos.ordercomponent.controller;

import com.velos.ordercomponent.business.pojoobjects.AddressPojo;
import com.velos.ordercomponent.business.pojoobjects.PersonPojo;
import com.velos.ordercomponent.service.VelosService;
import com.velos.ordercomponent.service.impl.VelosServiceImpl;

public class PersonController {
	VelosService service;
	
	public PersonPojo savePerson(PersonPojo personPojo) throws Exception{
		service = VelosServiceImpl.getService();
		return service.savePerson(personPojo);
	}
	
	public PersonPojo getPersonById(PersonPojo personPojo) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getPersonById(personPojo);
	}
}
