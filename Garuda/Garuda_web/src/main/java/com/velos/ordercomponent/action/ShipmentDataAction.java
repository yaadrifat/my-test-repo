package com.velos.ordercomponent.action;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.eres.web.user.UserJB;
import com.velos.ordercomponent.controller.CodelstController;
import com.velos.ordercomponent.controller.OpenOrderController;
import com.velos.ordercomponent.util.VelosGarudaConstants;

public class ShipmentDataAction extends VelosBaseAction {
	public static final Log log = LogFactory.getLog(ShipmentDataAction.class);
	private OpenOrderController linkController;
	private List shedShipDtLst=new ArrayList();
	private CodelstController codelstController;
	private List codeLstCustColLst;
	private String tableName;
	private Long sampleCount;
	private Long pkworldcourier;
	private Long completedFlag;
	
	public Long getCompletedFlag() {
		return completedFlag;
	}
	public void setCompletedFlag(Long completedFlag) {
		this.completedFlag = completedFlag;
	}
	public Long getPkworldcourier() {
		return pkworldcourier;
	}
	public void setPkworldcourier(Long pkworldcourier) {
		this.pkworldcourier = pkworldcourier;
	}
	public Long getSampleCount() {
		return sampleCount;
	}
	public void setSampleCount(Long sampleCount) {
		this.sampleCount = sampleCount;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	private String columnName;
	
	public List getCodeLstCustColLst() {
		return codeLstCustColLst;
	}
	public void setCodeLstCustColLst(List codeLstCustColLst) {
		this.codeLstCustColLst = codeLstCustColLst;
	}
	
	public ShipmentDataAction(){
		linkController = new OpenOrderController();
		codelstController = new CodelstController();
	}
	public List getShedShipDtLst() {
		return shedShipDtLst;
	}
	public void setShedShipDtLst(List shedShipDtLst) {
		this.shedShipDtLst = shedShipDtLst;
	}
	
	public String fatchLandingPageShipment(){
		List s = new ArrayList();
		try{
			log.debug("Inside shipment data action");
			String siteid=request.getParameter("pksiteId");
			log.debug("site id "+siteid);
			UserJB user = (UserJB)getRequest().getSession().getAttribute(VelosGarudaConstants.GARUDA_USER);
			Long userId = (long)user.getUserId();
			if(siteid.equals(ALL_SITE)){
				
				siteid=getUserSiteString(userId,GET_DATA);
				
			}
			s = linkController.getShedShipDts(null,siteid);
			setShedShipDtLst(s);
			log.debug("shedshipdtlist size:::::::::"+getShedShipDtLst().size());
			
			//request.setAttribute("cordShipment", shedShipDtLst);
		}catch(Exception e){
			//System.out.println("Error in cord shipment Part.................................................");
			e.printStackTrace();
		}
		//System.out.println("After Execution function");
	return SUCCESS;
		
	}
	public String fatchCodelstCustCol(){
		try {
				
				if(request.getParameter("pkCodelst")!=null && !request.getParameter("pkCodelst").equals("")){
					setCodeLstCustColLst(codelstController.getCodelstCustCols(Long.parseLong(request.getParameter("pkCodelst"))));
					
					for(int l=0;l<getCodeLstCustColLst().size();l++){
						Object [] obj = (Object[])getCodeLstCustColLst().get(l);
						
						if(obj[0]!=null && !obj[0].equals("")){
							setTableName(obj[0].toString());
						}
						if(obj[1]!=null && !obj[1].equals("")){
							setColumnName(obj[1].toString());
						}
					}
					if(getTableName()!=null && !getTableName().equals("") && getColumnName()!=null && !getColumnName().equals("") && request.getParameter("pkcordId")!=null){
						List lst=linkController.getDataFromCodelstCustCol(getTableName(), getColumnName(), " where ENTITY_ID="+request.getParameter("pkcordId")+" and ENTITY_TYPE="+getCodeListPkByTypeAndSubtype(ENTITY_CBU_CODE_TYPE, ENTITY_CBU_CODE_SUB_TYPE));
						if(lst.size()>0 && lst.get(0)!=null){
							
							Long obj = Long.parseLong(lst.get(0).toString());
							//System.out.println("obj:::"+obj);
							if(obj!=null && !obj.equals("")){
								setSampleCount(obj);
							}
						}else{
							setSampleCount(0l);
						}
					}
					setPkworldcourier(getCodeListPkByTypeAndSubtype(SHIPPER, WORLD_COUR));
					log.debug("getPkworldcourier():::::::::"+getPkworldcourier());
					//System.out.println("Table Name="+getTableName());
					//System.out.println("Column Name="+getColumnName());
					//cdrCbuViewDetails();
				}
			
			} catch (Exception e) {
				//System.out.println("error in getcodelstcustcol is:::"+e.getMessage());
				e.printStackTrace();
				
			}
		
		return "success";
	}
	
	public String FetchCRICompleteFlag(){
		Long pkCord=Long.parseLong(request.getParameter("pkcordId").toString());
		try {
			setCompletedFlag(linkController.getCRI_CompletedFlag(pkCord));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
