package com.velos.ordercomponent.action;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;



import com.velos.ordercomponent.business.pojoobjects.CordFilterPojo;
import com.velos.ordercomponent.business.pojoobjects.HLAFilterPojo;
import com.velos.ordercomponent.util.VelosGarudaConstants;

public class AntigenCSV implements VelosGarudaConstants{
	
	 private Boolean isMultipleCBBSelected = false;
	 private int noOfCDRCounter; 

	public File antigenCSV(List<CordFilterPojo> cdrObjects, Map<Long, HLAFilterPojo> bestHlasMap, Map<Long, Map<Long, HLAFilterPojo>> cbuHlas, String path) {
		
		
		 int bufferSize = 10240;
		 FileWriter fr = null;
		 BufferedWriter br = null;
		 HLAFilterPojo hla = null;
		 File file = null;
		 Map<Long, HLAFilterPojo> cbuHla = null;
		 StringBuilder content = new StringBuilder();
		 SimpleDateFormat formator = new SimpleDateFormat("MM/dd/yyyy");
		 
		try {
			
			file = new File(path);
			fr = new FileWriter(file);		
            br = new BufferedWriter(fr, bufferSize);
         
		
		 
		 
		 
		content = createHeader(content);
		br.write(content.toString());		
		content.setLength(0);
		
		for(CordFilterPojo cdrObj : cdrObjects){		
			
			 
			 hla =  bestHlasMap.get(cdrObj.cord);
			 cbuHla = cbuHlas.get(cdrObj.cord);
			 bestHlasMap.remove(cdrObj.cord);//remove map reference
			 cbuHlas.remove(cdrObj.cord);//remove map reference			 
			 
			
			 
			  if(isMultipleCBBSelected)
				  content.append(WRAPPER).append(cdrObj.site == null ? EMPTY : cdrObj.site ).append(WRAPPER).append(SEPRATOR); // Site Is Conditional --- if multiple CBB is selected then Site Column Have to Include
			  content.append(WRAPPER).append(cdrObj.registry == null ? EMPTY : cdrObj.registry).append(WRAPPER).append(SEPRATOR);
			  content.append(WRAPPER).append(cdrObj.localCbu == null ? EMPTY : cdrObj.localCbu).append(WRAPPER).append(SEPRATOR);
			  content.append(WRAPPER).append(cdrObj.numberOnBag == null ? EMPTY : cdrObj.numberOnBag).append(WRAPPER).append(SEPRATOR);
			  content.append(WRAPPER).append(cdrObj.isbtCode == null ? EMPTY : cdrObj.isbtCode).append(WRAPPER).append(SEPRATOR);
			  content.append(WRAPPER).append(cdrObj.historic == null ? EMPTY : cdrObj.historic).append(WRAPPER).append(SEPRATOR);
			  content.append(WRAPPER).append(cdrObj.maternal == null ? EMPTY : cdrObj.maternal).append(WRAPPER).append(SEPRATOR);
			  content.append(WRAPPER).append(cdrObj.localMat == null ? EMPTY : cdrObj.localMat).append(WRAPPER).append(SEPRATOR);
			  content.append(WRAPPER).append(cdrObj.patient == null ? EMPTY : cdrObj.patient).append(WRAPPER).append(SEPRATOR);
			  content.append(WRAPPER).append(cdrObj.collectDate == null ? EMPTY : formator.format(cdrObj.collectDate)).append(WRAPPER).append(SEPRATOR);
			
			  
			
			  
			  /*********** Best HLA ********************/
			  
			  if(hla != null){
				  
				  content.append(WRAPPER).append("Summary of HLA").append(WRAPPER).append(SEPRATOR);				  
				  content.append(EMPTY_VALUE).append(SEPRATOR); //hla.typingDate
				  content.append(EMPTY_VALUE).append(SEPRATOR); //hla.receivedDate
				  content.append(EMPTY_VALUE).append(SEPRATOR); // source				  
				  setHLAs(hla, content);
				  
			  }else{
				  
				  content.append(WRAPPER).append("Summary of HLA").append(WRAPPER).append(SEPRATOR);
			  }			  
			
			   content.append(NEW_LINE); // Create Blank Row  to separate 	BEST-HLA and HLA  		  
			
			  /***************** HLA *********************/
			
			  if(cbuHla != null){
			  for(Map.Entry<Long, HLAFilterPojo> hp:cbuHla.entrySet()){
				 
				  content.append(NEW_LINE); // HLA Row
				  hla = hp.getValue();
				 
				  setblankCDR(content, noOfCDRCounter);// Blank CDR
				 
				  
				  content.append(WRAPPER).append("History").append(WRAPPER).append(SEPRATOR); 				
				  content.append(WRAPPER).append(hla.typingDate == null ? EMPTY : formator.format(hla.typingDate)).append(WRAPPER).append(SEPRATOR);// hla.typingDate				
				  content.append(WRAPPER).append(hla.receivedDate == null ? EMPTY : formator.format(hla.receivedDate)).append(WRAPPER).append(SEPRATOR);//hla.receivedDate			  
				  content.append(WRAPPER).append(hla.user == null ? EMPTY : hla.user).append(WRAPPER).append(SEPRATOR);// Source
				  
				  setHLAs(hla, content);
				
				  
			   }
			  }else{
				  
				  content.append(NEW_LINE);
				  setblankCDR(content, noOfCDRCounter);// Blank CDR				  
				  content.append(WRAPPER).append("History").append(WRAPPER).append(SEPRATOR);
				  
				
			  }           
            
			  content.append(NEW_LINE); // Create Blank Row 
			  content.append(NEW_LINE);
			  
			 
			  br.write(content.toString());		
			  content.setLength(0);  
		 }
		 } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
            try {
                br.close();
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
		
		return file;
	}
	
	private StringBuilder setHLAs(HLAFilterPojo hla, StringBuilder content){
		
		  content.append(WRAPPER).append(hla.aType1 == null ? EMPTY : hla.aType1).append(WRAPPER).append(SEPRATOR);
		  content.append(WRAPPER).append(hla.aType2 == null ? EMPTY : hla.aType2).append(WRAPPER).append(SEPRATOR);
		  
		  content.append(WRAPPER).append(hla.bType1 == null ? EMPTY : hla.bType1).append(WRAPPER).append(SEPRATOR);
		  content.append(WRAPPER).append(hla.bType2 == null ? EMPTY : hla.bType2).append(WRAPPER).append(SEPRATOR);
		  
		  content.append(WRAPPER).append(hla.cType1 == null ? EMPTY : hla.cType1).append(WRAPPER).append(SEPRATOR);
		  content.append(WRAPPER).append(hla.cType2 == null ? EMPTY : hla.cType2).append(WRAPPER).append(SEPRATOR);
		  
		  content.append(WRAPPER).append(hla.drb1Type1 == null ? EMPTY : hla.drb1Type1).append(WRAPPER).append(SEPRATOR);
		  content.append(WRAPPER).append(hla.drb1Type2 == null ? EMPTY : hla.drb1Type2).append(WRAPPER).append(SEPRATOR);
		  
		  content.append(WRAPPER).append(hla.drb3Type1 == null ? EMPTY : hla.drb3Type1).append(WRAPPER).append(SEPRATOR);
		  content.append(WRAPPER).append(hla.drb3Type2 == null ? EMPTY : hla.drb3Type2).append(WRAPPER).append(SEPRATOR);
		  
		  content.append(WRAPPER).append(hla.drb4Type1 == null ? EMPTY : hla.drb4Type1).append(WRAPPER).append(SEPRATOR);
		  content.append(WRAPPER).append(hla.drb4Type2 == null ? EMPTY : hla.drb4Type2).append(WRAPPER).append(SEPRATOR);
		  
		  content.append(WRAPPER).append(hla.drb5Type1 == null ? EMPTY : hla.drb5Type1).append(WRAPPER).append(SEPRATOR);
		  content.append(WRAPPER).append(hla.drb5Type2 == null ? EMPTY : hla.drb5Type2).append(WRAPPER).append(SEPRATOR);
		  
		  content.append(WRAPPER).append(hla.dpb1Type1 == null ? EMPTY : hla.dpb1Type1).append(WRAPPER).append(SEPRATOR);
		  content.append(WRAPPER).append(hla.dpb1Type2 == null ? EMPTY : hla.dpb1Type2).append(WRAPPER).append(SEPRATOR);
		  
		  content.append(WRAPPER).append(hla.dqb1Type1 == null ? EMPTY : hla.dqb1Type1).append(WRAPPER).append(SEPRATOR);
		  content.append(WRAPPER).append(hla.dqb1Type2 == null ? EMPTY : hla.dqb1Type2).append(WRAPPER)/*.append(SEPRATOR)*/;
		  
		return content;
	}
	
	private StringBuilder setblankCDR(StringBuilder content, int noOfBlankColumn){
		
		for(int i=0;i<noOfBlankColumn;i++){
			content.append(EMPTY_VALUE).append(SEPRATOR);
		}
		
		return content;
	}
	
	private StringBuilder createHeader(StringBuilder content){
		
		if(isMultipleCBBSelected){
		   content.append(WRAPPER).append("CBB").append(WRAPPER).append(SEPRATOR);
		   noOfCDRCounter++;
		}
		   content.append(WRAPPER).append("CBU Registry ID").append(WRAPPER).append(SEPRATOR);                noOfCDRCounter++;
		   content.append(WRAPPER).append("CBU Local ID").append(WRAPPER).append(SEPRATOR);                   noOfCDRCounter++;
		   content.append(WRAPPER).append("Unique Product Identity on Bag").append(WRAPPER).append(SEPRATOR); noOfCDRCounter++;
		   content.append(WRAPPER).append("ISBT").append(WRAPPER).append(SEPRATOR);                           noOfCDRCounter++;
		   content.append(WRAPPER).append("Historic Local CBU ID").append(WRAPPER).append(SEPRATOR);		  noOfCDRCounter++;
		   content.append(WRAPPER).append("Maternal Registry ID").append(WRAPPER).append(SEPRATOR);           noOfCDRCounter++;
		   content.append(WRAPPER).append("Maternal Local ID").append(WRAPPER).append(SEPRATOR);              noOfCDRCounter++;
		   content.append(WRAPPER).append("Active for Patient (ID)").append(WRAPPER).append(SEPRATOR);        noOfCDRCounter++;
		   content.append(WRAPPER).append("CBU Collection Date").append(WRAPPER).append(SEPRATOR);            noOfCDRCounter++;
		   content.append(WRAPPER).append("Type").append(WRAPPER).append(SEPRATOR)
			      .append(WRAPPER).append("Typing Date").append(WRAPPER).append(SEPRATOR)
			      .append(WRAPPER).append("Received Date").append(WRAPPER).append(SEPRATOR)
			      .append(WRAPPER).append("Source").append(WRAPPER).append(SEPRATOR)
			      .append(WRAPPER).append("A Type1").append(WRAPPER).append(SEPRATOR)
			      .append(WRAPPER).append("A Type2").append(WRAPPER).append(SEPRATOR)
			      .append(WRAPPER).append("B Type1").append(WRAPPER).append(SEPRATOR)
			      .append(WRAPPER).append("B Type2").append(WRAPPER).append(SEPRATOR)
			      .append(WRAPPER).append("C Type1").append(WRAPPER).append(SEPRATOR)
			      .append(WRAPPER).append("C Type2").append(WRAPPER).append(SEPRATOR)
			      .append(WRAPPER).append("DRB1 Type1").append(WRAPPER).append(SEPRATOR)
			      .append(WRAPPER).append("DRB1 Type2").append(WRAPPER).append(SEPRATOR)
			      .append(WRAPPER).append("DRB3 Type1").append(WRAPPER).append(SEPRATOR)
			      .append(WRAPPER).append("DRB3 Type2").append(WRAPPER).append(SEPRATOR)
			      .append(WRAPPER).append("DRB4 Type1").append(WRAPPER).append(SEPRATOR)
			      .append(WRAPPER).append("DRB4 Type2").append(WRAPPER).append(SEPRATOR)
			      .append(WRAPPER).append("DRB5 Type1").append(WRAPPER).append(SEPRATOR)
			      .append(WRAPPER).append("DRB5 Type2").append(WRAPPER).append(SEPRATOR)
			      .append(WRAPPER).append("DQB1 Type1").append(WRAPPER).append(SEPRATOR)
			      .append(WRAPPER).append("DQB1 Type2").append(WRAPPER).append(SEPRATOR)
			      .append(WRAPPER).append("DPB1 Type1").append(WRAPPER).append(SEPRATOR)
			      .append(WRAPPER).append("DPB1 Type2").append(WRAPPER)
			      .append(NEW_LINE);
			
				return content;
	}
	public Boolean getIsMultipleCBBSelected() {
		return isMultipleCBBSelected;
	}

	public void setIsMultipleCBBSelected(Boolean isMultipleCBBSelected) {
		this.isMultipleCBBSelected = isMultipleCBBSelected;
	}
}
