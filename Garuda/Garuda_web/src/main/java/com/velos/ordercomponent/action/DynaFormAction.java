package com.velos.ordercomponent.action;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpSession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.html.simpleparser.StyleSheet;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import com.lowagie.text.pdf.PdfWriter;
import com.velos.ordercomponent.business.domain.CBUCompleteReqInfo;
import com.velos.ordercomponent.business.domain.CodeList;
import com.velos.ordercomponent.business.pojoobjects.AssessmentPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUCompleteReqInfoPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUFinalReviewPojo;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.FormResponsesPojo;
import com.velos.ordercomponent.business.pojoobjects.FormVersionPojo;
import com.velos.ordercomponent.business.pojoobjects.SearchPojo;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.Utilities;
import com.velos.ordercomponent.business.util.VelosUtil;
import com.velos.ordercomponent.controller.AssessmentController;
import com.velos.ordercomponent.controller.CBUController;
import com.velos.ordercomponent.controller.DynaFormController;
import com.velos.ordercomponent.controller.OpenOrderController;
import com.velos.ordercomponent.controller.VelosSearchController;
import com.velos.ordercomponent.util.VelosGarudaConstants;
import com.velos.ordercomponent.helper.CodelstHelper;

public class DynaFormAction extends VelosBaseAction {
	
	public static final Log log = LogFactory.getLog(DynaFormAction.class);
	private DynaFormController dynaFrmController;
	DateFormat dat=new SimpleDateFormat("MMM dd,yyyy");
	HttpSession session=request.getSession(true);
	private Long entityId;
	private String formId;
	private Long pkQuestion;
	private Long rowIndex;
	private List childquestions;
	private Long pk_cord;
	private List<FormResponsesPojo> MRQ_Question;
	private List<FormResponsesPojo> FMHQ_Question;
	private List<FormResponsesPojo> IDM_Question;
	private List<FormResponsesPojo> AssessmentList;
	private FormVersionPojo formVersionPojo;
	private FormVersionPojo formVersionPojo1;
	private Long subentityType;
	private AssessmentController assessmentController;
	private CBUController cbuController;
	private String dynaFormDate;
	private List quesGrp = new ArrayList();
	private List mrqFormList = new ArrayList();
	private String isCordEntry;
	private Boolean iscdrMember;
	private String filepath;
	private String isCompInfo;
	private String esignFlag;
	private CdrCbuPojo cdrCbuPojo;
	private CBUCompleteReqInfoPojo cbuCompleteReqInfoPojo;
	private VelosSearchController velosSearchController;
	private Long vnumber;
	private Long fkfrmversion;
	private Long seqnumber;
	private Long fkform;
	private List formversionlst = new ArrayList();
	private List mrqformversionlst = new ArrayList();
	private List fmhqformversionlst = new ArrayList();
	private List idmformversionlst = new ArrayList();
	private String iseditable;
	private List oldassessList = new ArrayList();
	private String formvers;
	FormVersionPojo formversion=new FormVersionPojo();
	private String isMinimumCriteria;
	private Long pkformversion;
	private String mrqversion;
	private String fmhqversion;
	private String idmversion;
	private Long siteId;
	private OpenOrderController linkController = new OpenOrderController();
	private String cbustatusDesc;
	private String statusCode;
	private Long notdonepk;
	private Long notdonepk1;
	private CBUFinalReviewPojo cbuFinalReviewPojo;
	private Boolean isCordEntryPage;
	private Boolean autoDeferFlag = false;
	private Long[] race;
	private CBUAction cbuaction;
	private CBUDeferStatusAction cbudeferstatusaction;
	private Boolean fromdefer = false;
	private FormResponsesPojo formResponsesPojoObjs;
	private String collectionDate;
	private String latestFormVersion;
	private Boolean isFormavail = true; 
	private String editFlag = "0";
	private Boolean changeFlag = true;
	private Boolean isFMHQNewForm = false;
	private Boolean criChangeFlag;
	private Boolean flagVal=false;
	public Boolean getCriChangeFlag() {
		return criChangeFlag;
	}

	public void setCriChangeFlag(Boolean criChangeFlag) {
		this.criChangeFlag = criChangeFlag;
	}

	public Boolean getIsFMHQNewForm() {
		return isFMHQNewForm;
	}

	public void setIsFMHQNewForm(Boolean isFMHQNewForm) {
		this.isFMHQNewForm = isFMHQNewForm;
	}

	public Boolean getChangeFlag() {
		return changeFlag;
	}

	public void setChangeFlag(Boolean changeFlag) {
		this.changeFlag = changeFlag;
	}

	public String getEditFlag() {
		return editFlag;
	}

	public void setEditFlag(String editFlag) {
		this.editFlag = editFlag;
	}

	public Boolean getIsFormavail() {
		return isFormavail;
	}

	public void setIsFormavail(Boolean isFormavail) {
		this.isFormavail = isFormavail;
	}

	public String getLatestFormVersion() {
		return latestFormVersion;
	}

	public void setLatestFormVersion(String latestFormVersion) {
		this.latestFormVersion = latestFormVersion;
	}

	public String getCollectionDate() {
		return collectionDate;
	}

	public void setCollectionDate(String collectionDate) {
		this.collectionDate = collectionDate;
	}

	public FormResponsesPojo getFormResponsesPojoObjs() {
		return formResponsesPojoObjs;
	}

	public void setFormResponsesPojoObjs(FormResponsesPojo formResponsesPojoObjs) {
		this.formResponsesPojoObjs = formResponsesPojoObjs;
	}

	public Boolean getFromdefer() {
		return fromdefer;
	}

	public void setFromdefer(Boolean fromdefer) {
		this.fromdefer = fromdefer;
	}

	public Boolean getAutoDeferFlag() {
		return autoDeferFlag;
	}

	public void setAutoDeferFlag(Boolean autoDeferFlag) {
		this.autoDeferFlag = autoDeferFlag;
	}
	
	public Long[] getRace() {
		return race;
	}

	public void setRace(Long[] race) {
		this.race = race;
	}

	public Boolean getIsCordEntryPage() {
		return isCordEntryPage;
	}

	public void setIsCordEntryPage(Boolean isCordEntryPage) {
		this.isCordEntryPage = isCordEntryPage;
	}
	
	public CBUFinalReviewPojo getCbuFinalReviewPojo() {
		return cbuFinalReviewPojo;
	}

	public void setCbuFinalReviewPojo(CBUFinalReviewPojo cbuFinalReviewPojo) {
		this.cbuFinalReviewPojo = cbuFinalReviewPojo;
	}

	public Long getNotdonepk() {
		return notdonepk;
	}

	public void setNotdonepk(Long notdonepk) {
		this.notdonepk = notdonepk;
	}

	public Long getNotdonepk1() {
		return notdonepk1;
	}

	public void setNotdonepk1(Long notdonepk1) {
		this.notdonepk1 = notdonepk1;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getCbustatusDesc() {
		return cbustatusDesc;
	}

	public void setCbustatusDesc(String cbustatusDesc) {
		this.cbustatusDesc = cbustatusDesc;
	}

	public Long getSiteId() {
		return siteId;
	}

	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}

	public Boolean getIscdrMember() {
		return iscdrMember;
	}

	public void setIscdrMember(Boolean iscdrMember) {
		this.iscdrMember = iscdrMember;
	}
	public String getMrqversion() {
		return mrqversion;
	}

	public void setMrqversion(String mrqversion) {
		this.mrqversion = mrqversion;
	}

	public String getFmhqversion() {
		return fmhqversion;
	}

	public void setFmhqversion(String fmhqversion) {
		this.fmhqversion = fmhqversion;
	}

	public String getIdmversion() {
		return idmversion;
	}

	public void setIdmversion(String idmversion) {
		this.idmversion = idmversion;
	}

	public Long getPkformversion() {
		return pkformversion;
	}

	public void setPkformversion(Long pkformversion) {
		this.pkformversion = pkformversion;
	}

	public String getIsMinimumCriteria() {
		return isMinimumCriteria;
	}

	public void setIsMinimumCriteria(String isMinimumCriteria) {
		this.isMinimumCriteria = isMinimumCriteria;
	}

	public FormVersionPojo getFormversion() {
		return formversion;
	}

	public void setFormversion(FormVersionPojo formversion) {
		this.formversion = formversion;
	}

	public String getFormvers() {
		return formvers;
	}

	public void setFormvers(String formvers) {
		this.formvers = formvers;
	}

	public List getOldassessList() {
		return oldassessList;
	}

	public void setOldassessList(List oldassessList) {
		this.oldassessList = oldassessList;
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}
	public String getIseditable() {
		return iseditable;
	}

	public void setIseditable(String iseditable) {
		this.iseditable = iseditable;
	}

	public List getMrqformversionlst() {
		return mrqformversionlst;
	}

	public void setMrqformversionlst(List mrqformversionlst) {
		this.mrqformversionlst = mrqformversionlst;
	}
	public List getIdmformversionlst() {
		return idmformversionlst;
	}

	public void setIdmformversionlst(List idmformversionlst) {
		this.idmformversionlst = idmformversionlst;
	}
	public List getFmhqformversionlst() {
		return fmhqformversionlst;
	}

	public void setFmhqformversionlst(List fmhqformversionlst) {
		this.fmhqformversionlst = fmhqformversionlst;
	}
	public List getFormversionlst() {
		return formversionlst;
	}

	public void setFormversionlst(List formversionlst) {
		this.formversionlst = formversionlst;
	}
	public FormVersionPojo getFormVersionPojo1() {
		return formVersionPojo1;
	}

	public void setFormVersionPojo1(FormVersionPojo formVersionPojo1) {
		this.formVersionPojo1 = formVersionPojo1;
	}
	public Long getFkform() {
		return fkform;
	}

	public void setFkform(Long fkform) {
		this.fkform = fkform;
	}

	public FormVersionPojo getFormVersionPojo() {
		return formVersionPojo;
	}

	public void setFormVersionPojo(FormVersionPojo formVersionPojo) {
		this.formVersionPojo = formVersionPojo;
	}
	
	public Long getSeqnumber() {
		return seqnumber;
	}

	public void setSeqnumber(Long seqnumber) {
		this.seqnumber = seqnumber;
	}

	public Long getFkfrmversion() {
		return fkfrmversion;
	}

	public void setFkfrmversion(Long fkfrmversion) {
		this.fkfrmversion = fkfrmversion;
	}

	public Long getVnumber() {
		return vnumber;
	}

	public void setVnumber(Long vnumber) {
		this.vnumber = vnumber;
	}

	public CdrCbuPojo getCdrCbuPojo() {
		return cdrCbuPojo;
	}

	public void setCdrCbuPojo(CdrCbuPojo cdrCbuPojo) {
		this.cdrCbuPojo = cdrCbuPojo;
	}

	public CBUCompleteReqInfoPojo getCbuCompleteReqInfoPojo() {
		return cbuCompleteReqInfoPojo;
	}

	public void setCbuCompleteReqInfoPojo(
			CBUCompleteReqInfoPojo cbuCompleteReqInfoPojo) {
		this.cbuCompleteReqInfoPojo = cbuCompleteReqInfoPojo;
	}

	public String getEsignFlag() {
		return esignFlag;
	}

	public void setEsignFlag(String esignFlag) {
		this.esignFlag = esignFlag;
	}
	
	public String getIsCompInfo() {
		return isCompInfo;
	}

	public void setIsCompInfo(String isCompInfo) {
		this.isCompInfo = isCompInfo;
	}

	public String getDynaFormDate() {
		return dynaFormDate;
	}

	public void setDynaFormDate(String dynaFormDate) {
		this.dynaFormDate = dynaFormDate;
	}

	public Long getSubentityType() {
		return subentityType;
	}

	public void setSubentityType(Long subentityType) {
		this.subentityType = subentityType;
	}

	public List getChildquestions() {
		return childquestions;
	}

	public void setChildquestions(List childquestions) {
		this.childquestions = childquestions;
	}

	public Long getRowIndex() {
		return rowIndex;
	}

	public void setRowIndex(Long rowIndex) {
		this.rowIndex = rowIndex;
	}

	public Long getPkQuestion() {
		return pkQuestion;
	}

	public void setPkQuestion(Long pkQuestion) {
		this.pkQuestion = pkQuestion;
	}

	public String getFormId() {
		return formId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}
	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	
	public DynaFormAction(){
		cbuaction = new CBUAction();
		cbudeferstatusaction = new CBUDeferStatusAction();
		dynaFrmController = new DynaFormController();
		assessmentController = new AssessmentController();
		cbuController = new CBUController();
		cdrCbuPojo = new CdrCbuPojo();
		cbuCompleteReqInfoPojo = new CBUCompleteReqInfoPojo();
		cbuFinalReviewPojo = new CBUFinalReviewPojo();
		formVersionPojo = new FormVersionPojo();
		formVersionPojo1 = new FormVersionPojo();
		formResponsesPojoObjs = new FormResponsesPojo();
		velosSearchController = new VelosSearchController();
	}
	
	public List getMrqFormList() {
		return mrqFormList;
	}
	
	public void setMrqFormList(List mrqFormList) {
		this.mrqFormList = mrqFormList;
	}
	
	public List<FormResponsesPojo> getMRQ_Question() {
		if(MRQ_Question==null){
			MRQ_Question=new ArrayList<FormResponsesPojo>();
		}
		return MRQ_Question;
	}

	public void setMRQ_Question(List<FormResponsesPojo> mRQ_Question) {
		MRQ_Question = mRQ_Question;
	}
	public List<FormResponsesPojo> getIDM_Question() {
		return IDM_Question;
	}

	public void setIDM_Question(List<FormResponsesPojo> iDM_Question) {
		IDM_Question = iDM_Question;
	}
	public List<FormResponsesPojo> getFMHQ_Question() {
		if(FMHQ_Question==null){
			FMHQ_Question=new ArrayList<FormResponsesPojo>();
		}
		return FMHQ_Question;
	}

	public void setFMHQ_Question(List<FormResponsesPojo> fMHQ_Question) {
		FMHQ_Question = fMHQ_Question;
	}

	public List getQuesGrp() {
		return quesGrp;
	}

	public void setQuesGrp(List quesGrp) {
		this.quesGrp = quesGrp;
	}
	
	public String getIsCordEntry() {
		return isCordEntry;
	}

	public void setIsCordEntry(String isCordEntry) {
		this.isCordEntry = isCordEntry;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String getDynaForm(){
		return "success";
	}
	
	public String getForm()throws Exception{
		String returnType = SUCCESS;
		try{
			if(session.getAttribute("aList")!=null){
				session.setAttribute("aList", null);
			}

			if(getSiteId()!=null && !getSiteId().equals("")){
			}else{
				if(getEntityId()!=null && !EMPTY_STRING.equals(getEntityId())){
					siteId=dynaFrmController.getfkCbbid(getEntityId());
					setSiteId(siteId);
				}
			}
			if(siteId!=null && !EMPTY_STRING.equals(siteId)){
				Boolean flag= new VelosUtil().isCdrUser(getSiteId());
				setIscdrMember(flag);
			}
		
			if(getFormId()!=null && !EMPTY_STRING.equals(getFormId()) && FORM_IDM.equals(getFormId())){
				notdonepk = getCodeListPkByTypeAndSubtype(TEST_OUTCOMES1, TEST_NOT_DONE);
				setNotdonepk(notdonepk);
				notdonepk1 = getCodeListPkByTypeAndSubtype(TEST_OUTCOMES, TEST_NOT_DONE);
				setNotdonepk1(notdonepk1);
				returnType="idmform";
			}
			latestFormVersion = dynaFrmController.getLatestFormVer(getFormId());
			setLatestFormVersion(latestFormVersion);
			if(getFormvers()!=null && !EMPTY_STRING.equals(getFormvers())){
				
			}else{
					formvers = dynaFrmController.getLatestFormVer(getFormId());
					setFormvers(formvers);
			}
			long subentityType=getCodeListPkByTypeAndSubtype(SUBENTITY_CODE_TYPE, getFormId());
			setSubentityType(subentityType);
			if(getEntityId()!=null && !EMPTY_STRING.equals(getEntityId()) && getFormId()!=null && !EMPTY_STRING.equals(getFormId())){
				cdrCbuPojo.setCordID(getEntityId());
				cdrCbuPojo = cbuController.getCordInfoById(cdrCbuPojo);
				if(getCollectionDate()!=null && !EMPTY_STRING.equals(getCollectionDate())){
					cdrCbuPojo.getSpecimen().setSpecCollDate(dat.parse(collectionDate));
				}
				setCdrCbuPojo(cdrCbuPojo);
				//log.debug("getFormId : "+getFormId());
				//log.debug("Inside the getForm");
				if(getFormId()!=null && !EMPTY_STRING.equals(getFormId()) && !FORM_IDM.equals(getFormId())){
					Long assess_ncd= getCodeListPkByTypeAndSubtype(NOTE_ASSESS, ASSESS_NCD);
					if(assess_ncd!=null && !EMPTY_STRING.equals(assess_ncd)){
						request.setAttribute("assess_ncd", assess_ncd);
					}
				}
				if(getFormId()!=null && !EMPTY_STRING.equals(getFormId()) && FORM_IDM.equals(getFormId())){
					cbuaction.getNotdonepk();
				}
				if((getIsCompInfo()!=null && ("1").equals(getIsCompInfo())) || (getIsMinimumCriteria()!=null && ("1").equals(getIsMinimumCriteria()))){
					if(FORM_FMHQ.equals(getFormId())){
						Object [] obj = null;
						obj = (Object[]) dynaFrmController.getformvmax(getEntityId(), getFormId());
						if(obj!=null && !EMPTY_STRING.equals(obj)){
								if(obj[0]!=null && !EMPTY_STRING.equals(obj[0])){
									fkfrmversion = Long.parseLong(obj[0].toString());
									}
								if(obj[1]!=null && !EMPTY_STRING.equals(obj[1])){
									formvers = obj[1].toString();
									setFormvers(formvers);
								}
							}else{
								setFormvers(latestFormVersion);
							}
						log.debug("Form version pk::"+fkfrmversion+":::Form Version::"+formvers);
						}else{
							fkfrmversion=dynaFrmController.getformvmaxPk(getEntityId(), getFormId(),getFormvers());
						}
					if(fkfrmversion!=null && !EMPTY_STRING.equals(fkfrmversion)){
						setFkfrmversion(fkfrmversion);
						setIsFormavail(true);
						setEditFlag("1");
					}else{
						if(FORM_FMHQ.equals(getFormId())){
							setIsFormavail(false);
						}else{
							setIsFormavail(true);
							setEditFlag("1");
						}
					}
				}
				if(seqnumber!=null && !EMPTY_STRING.equals(seqnumber) && !seqnumber.equals("undefined")){
						
					}else{
						seqnumber=dynaFrmController.getFormseq(getEntityId(),getFormId(),getFormvers());
					}
				
				//System.out.println("seqnumber::"+seqnumber);
				if(seqnumber!=null && !EMPTY_STRING.equals(seqnumber)){
					formversion=dynaFrmController.getFormVerPojo(getEntityId(),getFormId(),getFormvers(),seqnumber);
				}
				mrqFormList=dynaFrmController.getFormData(getEntityId(),getFormId(),getFormvers(),getFkfrmversion());
				quesGrp=dynaFrmController.getQuestionGroup(getFormId(),getFormvers());
				//log.debug("mrqList:"+mrqFormList);
				if(FORM_IDM.equals(getFormId()) || FORM_FMHQ.equals(getFormId())){
					Date formCreatedDate = dynaFrmController.getFormCreatedDate(getEntityId(), getFormId(), getFormvers());
					String codeListDateStr = getCodeListDescByTypeAndSubtype(VELOS_EMTRAX_VERSION_TYPE, VELOS_EMTRAX_VERSION_SUBTYPE);
					Date codeListDate = Utilities.getDate(codeListDateStr, VELOS_EMTRAX_VERSION1_3_DATE_FORMAT);
					if(seqnumber!=null){
						//System.out.println("Form is avail");
						if(formCreatedDate!=null){
							if(formCreatedDate.after(codeListDate)){
								setIsFMHQNewForm(true);
							}
						}else{
							//System.out.println("NewForm");
							setIsFMHQNewForm(true);
						}
					}else{
						//System.out.println("NewForm");
						setIsFMHQNewForm(true);
					}
				}
			}
			if(mrqFormList!=null && !EMPTY_STRING.equals(mrqFormList)){
				setMrqFormList(mrqFormList);
			}
			if(quesGrp!=null && !EMPTY_STRING.equals(quesGrp)){
				setQuesGrp(quesGrp);
			}
		}catch (Exception e) {
			// TODO: handle exception
			log.error("Execption in getForm:"+e);
			e.printStackTrace();
		}
		return returnType;
	}
	
	public String getFormVersion()throws Exception{
		try{
			if(getEntityId()!=null && !EMPTY_STRING.equals(getEntityId()) && getFormId()!=null && !EMPTY_STRING.equals(getFormId())){
				//log.debug("getEntiryId: "+getEntityId());
				//log.debug("getFormId : "+getFormId());
				//log.debug("Inside the getFormVersion");
				formversionlst=dynaFrmController.getFormVersionLst(getEntityId(), getFormId());
				formvers=dynaFrmController.getLatestFormVer(getFormId());
				setFormvers(formvers);
				//log.debug("formversionlst:------:"+formversionlst);
				if(formversionlst!=null && !EMPTY_STRING.equals(formversionlst)){
					if(FORM_MRQ.equals(getFormId())){
						setMrqformversionlst(formversionlst);
						setMrqversion(getFormvers());
					}else if(FORM_FMHQ.equals(getFormId())){
						setFmhqformversionlst(formversionlst);
						setFmhqversion(getFormvers());
					}else if(FORM_IDM.equals(getFormId())){
						setIdmformversionlst(formversionlst);
						setIdmversion(getFormvers());
					}
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
			log.error("Exception:"+e);
			e.printStackTrace();
		}
		return getFormId();
	}
	public String saveFormResponses()throws Exception{
		String returnType = SUCCESS;
		log.debug("--------------------Save Form Response--------------------------------");
		 String autoDeferExecFlag = request.getParameter("autoDeferExecFlag");
		 String statusCode = request.getParameter("cordCbuStatus");
		
		try{
			Long entityType=null;
			if(!FORM_IDM.equals(getFormId())){
				entityType= getCodeListPkByTypeAndSubtype(TEST_SOURCES, MATERNAL_CODESUBTYPE);
			}else if(FORM_IDM.equals(getFormId())){
				String entiryStr=request.getParameter("formversion.entityType");
				if(entiryStr!=null && !EMPTY_STRING.equals(entiryStr)){
					entityType=Long.parseLong(entiryStr);
				}else{
					entityType= getCodeListPkByTypeAndSubtype(TEST_SOURCES, MATERNAL_CODESUBTYPE);
				}
			}
			List<FormResponsesPojo> formresponsepojo = new ArrayList<FormResponsesPojo>();
			/**For Audit Trail
				List<FormResponsesPojo> formresponsepojotemp = new ArrayList<FormResponsesPojo>();
			*/
			if(getFMHQ_Question()!=null && getFMHQ_Question().size()>0 && !EMPTY_STRING.equals(getFMHQ_Question())){
				setMRQ_Question(getFMHQ_Question());
			}
			if(getIDM_Question()!=null && getIDM_Question().size()>0 && !EMPTY_STRING.equals(getIDM_Question())){
				setMRQ_Question(getIDM_Question());
			}
			
			if(isCordEntry!=null && !EMPTY_STRING.equals(isCordEntry) && !("0").equals(isCordEntry)){
				setIsCordEntryPage(true);
			}
			Long pkformVersion = getPkformversion();
		/*	if(getSeqnumber()!=null && !getSeqnumber().equals("") && !getSeqnumber().equals(1l)){
				dynaFrmController.getOldFormVersion(getEntityId(),getFormId(),true);
			}*/
			formVersionPojo.setPkformversion(null);
			if(getEntityId()!=null && !EMPTY_STRING.equals(getEntityId())){
				formVersionPojo.setEntityId(getEntityId());
			}
			if(entityType!=null && !EMPTY_STRING.equals(entityType)){
				formVersionPojo.setEntityType(entityType);
			}
			if(getDynaFormDate()!=null && !EMPTY_STRING.equals(getDynaFormDate())){
				formVersionPojo.setDynaformDate(dat.parse(getDynaFormDate()));
			}
			if(getSeqnumber()!=null && !EMPTY_STRING.equals(getSeqnumber())){
				formVersionPojo.setFormseq(getSeqnumber());
			}
			if(getFkform()!=null && !EMPTY_STRING.equals(getFkform())){
				formVersionPojo.setFkform(getFkform());
			}
			
			//formVersionPojo = dynaFrmController.saveFormVersion(formVersionPojo,true);
			if(getMRQ_Question()!=null && !EMPTY_STRING.equals(getMRQ_Question())){
				for(FormResponsesPojo formresponse : getMRQ_Question()){
					if(FORM_IDM.equals(getFormId())){
						if(formresponse.getDynaFormDateStr()!=null && !EMPTY_STRING.equals(formresponse.getDynaFormDateStr())){
							formresponse.setResponsevalue(formresponse.getDynaFormDateStr());
						}
					}
					if((formresponse.getResponsecode()==null || EMPTY_STRING.equals(formresponse.getResponsecode()))&&(formresponse.getResponsevalue()==null || EMPTY_STRING.equals(formresponse.getResponsevalue()))){
						/**For Audit Trail Temp table
						if(getEntityId()!=null && !getEntityId().equals("")){
							formresponse.setEntityId(getEntityId());
						}
						if(entityType!=null && !entityType.equals("")){
							formresponse.setEntityType(entityType);
						}
						if(getFormId().equals(VelosGarudaConstants.FORM_IDM)){
							if(formresponse.getDynaFormDateStr()!=null && !formresponse.getDynaFormDateStr().equals("")){
								formresponse.setDynaFormDate(dat.parse(formresponse.getDynaFormDateStr()));
								formresponse.setResponsevalue("");
							}
						}
						formresponsepojotemp.add(formresponse);
						*/
					}else{
						if(getEntityId()!=null && !EMPTY_STRING.equals(getEntityId())){
							formresponse.setEntityId(getEntityId());
						}
						if(entityType!=null && !EMPTY_STRING.equals(entityType)){
							formresponse.setEntityType(entityType);
						}
						if(FORM_IDM.equals(getFormId())){
							if(formresponse.getDynaFormDateStr()!=null && !EMPTY_STRING.equals(formresponse.getDynaFormDateStr())){
								formresponse.setDynaFormDate(dat.parse(formresponse.getDynaFormDateStr()));
								formresponse.setResponsevalue(EMPTY_STRING);
							}
						}
						//formresponse.setFkformversion(formVersionPojo.getPkformversion());
						formresponsepojo.add(formresponse);
						/**For Audit Trail
							formresponsepojotemp.add(formresponse);
						*/
					}
					
				}
			}
			formResponsesPojoObjs.setFormVersionPojo(formVersionPojo);
			formResponsesPojoObjs.setFormresponsepojoList(formresponsepojo);
			
			/** For Audit Trail Temp table
				formResponsesPojoObjs.setFormresponseTemppojoList(formresponsepojotemp); 
			*/
			
			log.debug("Change Flag:::"+changeFlag);
			if(getChangeFlag()){
				log.debug("inside save the form");
				AssessmentList = new ArrayList<FormResponsesPojo>();
				FormResponsesPojo finalSavedObjects = dynaFrmController.saveFormData(formResponsesPojoObjs, getEntityId(), getFormId(),true);
				formVersionPojo = finalSavedObjects.getFormVersionPojo();
				AssessmentList = finalSavedObjects.getFormresponsepojoList();
				log.debug("--------------------After save the form----------------");
				if(formVersionPojo.getFormseq()!=null && formVersionPojo.getFormseq()!=1l){
					List<Object> oldassess = new ArrayList<Object>();
					oldassess = dynaFrmController.getPrevAssessData(getEntityId(),pkformVersion);
					setAssessmentInSession(oldassess);
				}
				log.debug("--------------------After prev Assessment data----------------");
				appendAssessment(AssessmentList);
				log.debug("--------------------After append Assessment----------------");
			}
			pk_cord=getEntityId();
			cdrCbuPojo.setCordID(getEntityId());
			cdrCbuPojo = cbuController.getCordInfoById(cdrCbuPojo);	
			setCdrCbuPojo(cdrCbuPojo);
			String flagStr=dynaFrmController.getidmdefervalue(getEntityId());
			if(getEntityId()!=null && !getEntityId().equals("")){
				
				if(flagStr!=null && (flagStr.equals("1") || flagStr.equals("2"))){
					flagVal = true;
				}
				else{
					flagVal = false;
				}
			}			
			if(FORM_IDM.equals(getFormId())){
				
					
				if(flagVal && !flagStr.equals("2")){
					String pkstatus = linkController.getPkCbuStatus(null, RESPONSE, DD);
					 if(!"false".equals(autoDeferExecFlag)){
						//System.out.println("\n\nInside if flag:::"+flagVal);						
							getCdrCbuPojo().setFkCordCbuStatus(Long.parseLong(pkstatus));
							setCdrCbuPojo(cbuController.saveOrUpdateCordInfo(getCdrCbuPojo()));
								
								if("DD".equals(statusCode) || "OT".equals(statusCode)){
							//getCdrCbuPojo().setFkCordCbuStatus(getCbuStatusPkByCode(statusCode));
								new CBUAction(getCdrCbuPojo()).rollBackAutoDefer(statusCode);
								setCdrCbuPojo((CdrCbuPojo)cbuController.getCordInfoById(getCdrCbuPojo()));
							}
							
						}
						
						if(pkstatus!=null && !EMPTY_STRING.equals(pkstatus)){
							setCbustatusDesc(getCbuStatusDescByPk(Long.parseLong(pkstatus)));
							setStatusCode(statusCode);
						}
						setFromdefer(true);
					}
				}
				log.debug("--------------------After Auto Defer----------------");
			
			//if(getIsCompInfo()!=null && !getIsCompInfo().equals("1")){
				if(cdrCbuPojo.getCordID()!=null && getFormId()!=null){
					linkController.autoCompleteCRI(cdrCbuPojo.getCordID());
				}
				
				CBUCompleteReqInfoPojo cbuCompleteReqInfoPojo = new CBUCompleteReqInfoPojo();
				cbuCompleteReqInfoPojo = cbuaction.getCbuComplReqInfoObj(cdrCbuPojo, null);
				setCbuCompleteReqInfoPojo(cbuCompleteReqInfoPojo);
				if(getChangeFlag()){
					log.debug("inside reset the final Review");
					CBUFinalReviewPojo cbuFinalReviewPojo = new CBUFinalReviewPojo();
					cbuFinalReviewPojo = cbuaction.getCbufinalReviewObj(cdrCbuPojo, null);
					setCbuFinalReviewPojo(cbuaction.resetFinalCBUReview(cbuFinalReviewPojo));
				}
			//}
			if(getIsCompInfo()!=null && ("1").equals(getIsCompInfo()) && autoDeferFlag==false){
				returnType = "completeReqInfo";
			}else{
				returnType = getFormVersion();
			}
			log.debug("--------------------After return----------------");
			request.setAttribute("assess_Flg", "true");
			//System.out.println("CORDID"+pk_cord+"getCdrCbuPojo().cordId"+getCdrCbuPojo().getCordID());
			request.setAttribute("cordIdfrm", String.valueOf(pk_cord));
			//System.out.println("\n\n\n CBU Status value:::"+getCdrCbuPojo().getFkCordCbuStatus());
		}catch (Exception e) {
			log.error("Exception in SaveFormResponses :"+e);
			e.printStackTrace();
		}
		
		return returnType;
	}
	public CBUCompleteReqInfoPojo f_getCRIpojoValues(Long pkCordId){
		CBUCompleteReqInfo cbuCompleteReqInfo = null;
		CBUCompleteReqInfoPojo cbuCRInfoPojo = null;
		try{
			SearchPojo searchPojoComp = new SearchPojo();
			searchPojoComp.setSearchType(CBU_COMPLETE_REQ_INFO_OBJECT);
			searchPojoComp.setCriteria(" fkCordId ="+pkCordId+" order by pkCbuCompleteReqInfo desc");
			
			searchPojoComp = velosSearchController.velosSearch(searchPojoComp);
			Boolean isNewTaskReq = isTasksRequired(cdrCbuPojo.getCordID());
			request.setAttribute("isNewTaskReq", isNewTaskReq);
			if(searchPojoComp!=null && searchPojoComp.getResultlst()!=null && searchPojoComp.getResultlst().size() != 0){
				log.debug("searchPojoComp.getResultlst().size()::::::::::::::::::::::::::::"+searchPojoComp.getResultlst().size());
				cbuCompleteReqInfo = (CBUCompleteReqInfo)searchPojoComp.getResultlst().get(0);
				}
			if(cbuCompleteReqInfo!=null){
				cbuCRInfoPojo = new CBUCompleteReqInfoPojo();
				cbuCRInfoPojo = (CBUCompleteReqInfoPojo)ObjectTransfer.transferObjects(cbuCompleteReqInfo, cbuCRInfoPojo);
			}
		}catch (Exception e) {
			System.out.println("error in f_getCRIpojoValues::");
			e.printStackTrace();
		}
		return cbuCRInfoPojo;
	}
	public void setAssessmentInSession(List<Object> assessList){
		List<AssessmentPojo> assessmentList = new ArrayList<AssessmentPojo>();
		Boolean updateflag = true;
		try{
			Object[] obj = null;
			if(session.getAttribute("aList")!=null){
				List l = (List)session.getAttribute("aList");
				if(l!=null && !l.isEmpty()){
					assessmentList.addAll(l);
				}
			}
			for(int i=0;i<assessList.size();i++){
				AssessmentPojo assessmentPojo=new AssessmentPojo();
				obj=(Object[]) assessList.get(i);
				updateflag = true;
				for(AssessmentPojo asp:assessmentList){
					String oldRef="";
					Long oldResCode=null;
					String oldResValue="";
					if(obj[10]!=null && !EMPTY_STRING.equals(obj[10])){
						oldRef = obj[10].toString();
					}
					if(obj[4]!=null && !EMPTY_STRING.equals(obj[4])){
						oldResCode = Long.parseLong(obj[4].toString());
						//System.out.println("oldResCode:::"+oldResCode);
					}
					if(obj[5]!=null && !EMPTY_STRING.equals(obj[5])){
						oldResValue = obj[5].toString();
						//System.out.println("oldResValue:::"+oldResValue);
					}
					//System.out.println("colRef:::"+asp.getColumnReference());
					if(asp.getColumnReference()!=null && !EMPTY_STRING.equals(asp.getColumnReference()) && asp.getColumnReference().toString().equals(oldRef.toString())){
						if(asp.getAssessmentReason()!=null && !EMPTY_STRING.equals(asp.getAssessmentReason()) && asp.getAssessmentReason().equals(oldResCode)){
							updateflag = false;	
						}else if(asp.getReasonRemarks()!=null && !EMPTY_STRING.equals(asp.getReasonRemarks()) && asp.getReasonRemarks().equals(oldResValue)){
							updateflag = false;
						}
					}
				}
				if(updateflag){
					if(obj[0]!=null && !EMPTY_STRING.equals(obj[0])){
						assessmentPojo.setAssessmentRemarks(obj[0].toString());
					}
					if(obj[1]!=null && !EMPTY_STRING.equals(obj[1])){
						if(("1").equals(obj[1].toString())){
							assessmentPojo.setFlagforLater(true);
						}else if(("0").equals(obj[1].toString())){
							assessmentPojo.setFlagforLater(false);
						}
					}
					if(obj[2]!=null && !EMPTY_STRING.equals(obj[2])){
						assessmentPojo.setAssessmentResp(Long.parseLong((obj[2].toString())));
					}
					if(obj[3]!=null && !EMPTY_STRING.equals(obj[3])){
						if(("1").equals(obj[3].toString())){
							assessmentPojo.setVisibleTCFlag(true);
						}else if(("0").equals(obj[3].toString())){
							assessmentPojo.setVisibleTCFlag(false);
						}
					}
					if(obj[4]!=null && !EMPTY_STRING.equals(obj[4])){
						assessmentPojo.setAssessmentReason(Long.parseLong((obj[4].toString())));
					}
					if(obj[5]!=null && !EMPTY_STRING.equals(obj[5])){
						assessmentPojo.setReasonRemarks(obj[5].toString());
					}
					if(obj[6]!=null && !EMPTY_STRING.equals(obj[6])){
						assessmentPojo.setAvailableDate((Date) obj[6]);
					}
					if(obj[7]!=null && !EMPTY_STRING.equals(obj[7])){
						if(("1").equals(obj[7].toString())){
							assessmentPojo.setSendReviewFlag(true);
						}else if(("0").equals(obj[7].toString())){
							assessmentPojo.setSendReviewFlag(false);
						}
					}
					if(obj[8]!=null && !EMPTY_STRING.equals(obj[8])){
						if(("1").equals(obj[8].toString())){
							assessmentPojo.setConsultFlag(true);
						}else if(("0").equals(obj[8].toString())){
							assessmentPojo.setConsultFlag(false);
						}
					}
					if(obj[9]!=null && !EMPTY_STRING.equals(obj[9])){
						assessmentPojo.setAssessmentReviewed(obj[9].toString());
					}
					if(obj[10]!=null && !EMPTY_STRING.equals(obj[10])){
						assessmentPojo.setColumnReference(obj[10].toString());
					}
					if(obj[11]!=null && !EMPTY_STRING.equals(obj[11])){
						assessmentPojo.setAssessmentpostedby(Long.parseLong((obj[11].toString())));
					}
					if(obj[12]!=null && !EMPTY_STRING.equals(obj[12])){
						assessmentPojo.setAssessmentpostedon((Date) obj[12]);
					}
					if(obj[13]!=null && !EMPTY_STRING.equals(obj[13])){
						assessmentPojo.setAssessmentconsultby(Long.parseLong((obj[13].toString())));
					}
					if(obj[14]!=null && !EMPTY_STRING.equals(obj[14])){
						assessmentPojo.setAssessmentconsulton((Date) obj[14]);
					}
					if(obj[15]!=null && !EMPTY_STRING.equals(obj[15])){
						assessmentPojo.setFkTestId(Long.parseLong((obj[15].toString())));
					}
					if(obj[16]!=null && !EMPTY_STRING.equals(obj[16])){
						assessmentPojo.setAssessmentFlagComment((obj[16].toString()));
					}
					assessmentList.add(assessmentPojo);
				}
			}
			if(session!=null){
				session.setAttribute("aList",assessmentList);
			}
		}catch (Exception e) {
			// TODO: handle exception
			log.error("Exception in setAssessmentInSession:"+e);
			e.printStackTrace();
		}
	}
	
	public String appendAssessment(List<FormResponsesPojo> AssessmentList){
		try{
			List<AssessmentPojo> assessList = Collections.synchronizedList(new ArrayList<AssessmentPojo>());
			AssessmentPojo assessmentPojoList = new AssessmentPojo();
			List<AssessmentPojo> assessmentList1 = new ArrayList<AssessmentPojo>();
			Long fktestid = null;
			Long pkresponse = null;
			List assessResponse = new ArrayList();
			if(session.getAttribute("aList")!=null){
				List l = (List)session.getAttribute("aList");
				if(l!=null && !l.isEmpty()){
					assessList.addAll(l);
				}
				session.setAttribute("aList", null);
			}
			Iterator<AssessmentPojo> assessmentPojo = assessList.iterator();
			while (assessmentPojo.hasNext()) {
				assessmentPojoList = assessmentPojo.next();
				assessmentPojoList.setPkAssessment(null);
				for(int l=0;l<AssessmentList.size();l++){
					FormResponsesPojo formrespojo = (FormResponsesPojo)AssessmentList.get(l);
					if(formrespojo.getPkresponse()!=null && !EMPTY_STRING.equals(formrespojo.getPkresponse())){
						pkresponse = formrespojo.getPkresponse();
					}
					if(formrespojo.getFkquestion()!=null && !EMPTY_STRING.equals(formrespojo.getFkquestion())){
						fktestid = formrespojo.getFkquestion();
					}
					
					if(formrespojo.getResponsecode()!=null && !EMPTY_STRING.equals(formrespojo.getResponsecode())){
						assessResponse.add(formrespojo.getResponsecode());
					}
					//log.debug("Sub Entity Id:"+formrespojo.getPkresponse());
					//log.debug("getResponsevalue:"+formrespojo.getResponsevalue());
					if(formrespojo.getResponsevalue()!=null && !EMPTY_STRING.equals(formrespojo.getResponsevalue())){
						//log.debug("Inside If");
						Long[] resVal = getCommaSeparatedData(formrespojo.getResponsevalue());
						//log.debug("Array len:"+resVal.length);
						for (Long longVal : resVal) {
							//log.debug("Inside Loop");
							if(longVal!=null && !EMPTY_STRING.equals(longVal)){
								//log.debug("inside Loop If");
								//log.debug("Long val:"+longVal);
								assessResponse.add(longVal);
							}else{
								//log.debug("else part:"+longVal);
								assessResponse.add(formrespojo.getResponsevalue());
							}
						}
					}
					//log.debug("fktestid:::"+fktestid);
					//log.debug("assessmentPojoList.getFkTestId():::"+assessmentPojoList.getFkTestId());
					if(fktestid.equals(assessmentPojoList.getFkTestId())){
							//log.debug("Responsecode:"+assessResponse.toString());
							//log.debug("assessmentPojoList.getAssessmentReason():"+assessmentPojoList.getAssessmentReason());
							//log.debug("assessmentPojoList.getAssessmentRemarks():"+assessmentPojoList.getReasonRemarks());
							if(assessmentPojoList.getAssessmentReason()!=null && !EMPTY_STRING.equals(assessmentPojoList.getAssessmentReason())){
								if(assessResponse.contains(assessmentPojoList.getAssessmentReason())){
									//log.debug("inside getAssessmentReason");
									if(pkresponse!=null && pkresponse>0){
										assessmentPojoList.setEntityId(formrespojo.getEntityId());
										assessmentPojoList.setEntityType(formrespojo.getEntityType());
										assessmentPojoList.setSubEntityId(pkresponse);
										assessmentPojoList.setSubEntityType(getSubentityType());
										assessmentList1.add(assessmentPojoList);
									}
								}
							}else if(assessmentPojoList.getReasonRemarks()!=null && !EMPTY_STRING.equals(assessmentPojoList.getReasonRemarks())){
								if(assessResponse.contains(assessmentPojoList.getReasonRemarks())){
									//log.debug("inside getReasonRemarks");
									if(pkresponse!=null && pkresponse>0){
										assessmentPojoList.setEntityId(formrespojo.getEntityId());
										assessmentPojoList.setEntityType(formrespojo.getEntityType());
										assessmentPojoList.setSubEntityId(pkresponse);
										assessmentPojoList.setSubEntityType(getSubentityType());
										assessmentList1.add(assessmentPojoList);
									}
								}
							}
					}
					assessResponse.clear();
				}
			}
			assessmentController.saveOrUpdateAssessmentList(assessmentList1);
		}catch (Exception e) {
			log.error("Exception in appendAssessment:"+e);
			e.printStackTrace();
		}
		return "success";
	}
	
	public Long[] getCommaSeparatedData(String Strdata){
		Long[] longdata=null;
		try{
			if(Strdata!=null && !EMPTY_STRING.equals(Strdata)){
				String[] starr=Strdata.split(",");
				longdata = new Long[starr.length];
				int count=0;
				for(String data:starr){
					if(data!=null && !EMPTY_STRING.equals(data)){
						data=data.trim();
						Pattern integerPattern = Pattern.compile("^\\d*$");
						Matcher matchesInteger = integerPattern.matcher(data);
						boolean isInteger = matchesInteger.matches();
						//log.debug("isInteger::"+isInteger);
						if(isInteger){
							longdata[count]= Long.parseLong(data);
							count++;
						}
					}
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
			log.error("Ex:"+e);
			e.printStackTrace();
		}
		return longdata;
	}
	public String fmhqPdfGenerator() throws Exception{
		try{		
		   //generateTempFileForPdfGen(getFormId(), getEntityId());
			getFormsInPdf(getFormId(),getEntityId());
		}catch(Exception e){
			e.printStackTrace();
		}
	return "success";
}
	
	
	
	public ByteArrayOutputStream generateTempFileForPdfGen(String formId,Long entityId){
		String filePath=null;
		String filePath1=null;
		String fileName=null;
		//log.debug("Enter to the DynaFormAction Class :::: ");
		List<CodeList> lstcodeList = new ArrayList<CodeList>();
		try{
		String tempDir=request.getRealPath("")+"/jsp";
		//	//log.debug("tempDir :"+tempDir);
			tempDir=tempDir+"/temp";
		//	//log.debug("tempDir : "+tempDir);
		File file = new File(tempDir);
		if(!file.exists()){
			file.mkdir();
		}
		filePath=file.getAbsolutePath();
		filePath1=file.getAbsolutePath();
		//log.debug("Fiel absolute Path :::: "+filePath);
		Calendar now = Calendar.getInstance();
		fileName="/"+formId+"temphtml"+"["+ now.get(now.DAY_OF_MONTH) + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)+ now.get(now.HOUR_OF_DAY)+ now.get(now.MINUTE)+ now.get(now.SECOND) + "]."+"html";
		filePath=filePath+fileName;
		//log.debug("generated file name :::: "+fileName);
		mrqFormList=dynaFrmController.getFormQues(entityId,formId);
		try{
	//		cdrCbuPojo = cbuController.getCordInfoById(cdrCbuPojo);
//			cdrCbuPojo.
			//log.debug("getEntityId :::: "+entityId);
			if(entityId!=null && !entityId.equals("")){
				//CdrCbuPojo cbuPojo=new CdrCbuPojo();
				cdrCbuPojo.setCordID(entityId);
				cdrCbuPojo = cbuController.getCordInfoById(cdrCbuPojo);
			}
			
			
//			cdrCbuPojo.get
			PrintWriter pw = new PrintWriter(new FileWriter(filePath));
			pw.println("<html><head><title></title></head><body>");
			pw.print("<h3 class='nmdpHead'>National Marrow Donnor Program</h3><h3 class='nmdpHead'>Maternal Risk Questionnaire Report for HCP</h3></center>");
			pw.print("<table class='headTable'>" +
					"<tr><td class='trLeft' style='align:left'>Cord Blood Unit Registry Id : "+ cdrCbuPojo.getRegistryId() +"</td>" +
					"<td class='trRight' style='align:right'>Cord Blood Bank ID : "+ cdrCbuPojo.getSite().getSiteSequence() +"</td></tr>" +
					"<tr><td class='trLeft' style='align:left'>CBB ID on the Bag : "); 
					if(cdrCbuPojo.getNumberOnCbuBag()!=null && !cdrCbuPojo.getNumberOnCbuBag().equals("")){
						pw.print(cdrCbuPojo.getNumberOnCbuBag());
					}
					
			pw.print("</td><td class='trRight' style='align:right'>Cord Blood Bank Name : "+ cdrCbuPojo.getSite().getSiteName() +"</td></tr></table><br><hr>");
	        pw.println("<TABLE class='tableCss'><TR ><TH class='Qseq' >Quest No.	Question Description</TH></TR>");
	        Iterator iterator=mrqFormList.iterator();
	        int i=1;
	       while(iterator.hasNext()){
	    	   Object [] obj = (Object[])iterator.next();
	    	   String fieldType = "";
	    	   if(obj[2]!=null && !obj[2].equals("")){
	    		   fieldType = getCodeListDescByPk(Long.parseLong(obj[2].toString()));
	    	   }
	    	   if(obj[8]!=null && !obj[8].equals("")){
	    		   lstcodeList=CodelstHelper.getCodelst(obj[8].toString());
	    	   }
	    	   ////log.debug(iterator.next());
	    	   pw.println("<TR><TD class='Qseq' >" + obj[16] +". "+obj[1]);
	    	   if(fieldType.equals(VelosGarudaConstants.TEXT_FIELD)){
	    		   pw.print("------------------------------------");
	    	   }
	    	   pw.print("</TD></TR>");
	    	   if(fieldType.equals(VelosGarudaConstants.DROPDOWN_FIELD)){
	    		   pw.println("<tr><td class='Qseq' >");
	    		   if(lstcodeList!=null && lstcodeList.size()>0){
		    		   Iterator<CodeList> codelstIterator1=lstcodeList.iterator();
		    		   //pw.print("<ul>");
		    		   while(codelstIterator1.hasNext()){
		    			   CodeList c= codelstIterator1.next();
		    			   //pw.print("<li class='options'>"+ c.getDescription()+"</li>");   
		    			   pw.print("<input type='checkbox' class='options'> "+ c.getDescription()+"<br>");
		    		   }
		    		   //pw.println("</ul>");
	    		   }
	    		   pw.println("</td></tr>");
	    	   }
	    	   
	    	   if(fieldType.equals(VelosGarudaConstants.MULTISELECT_FIELD)){
	    		   pw.println("<tr><td class='Qseq' >");
	    		   if(lstcodeList!=null && lstcodeList.size()>0){
		    		   Iterator<CodeList> codelstIterator2=lstcodeList.iterator();
		    		   pw.print("<ul>");
		    		   while(codelstIterator2.hasNext()){
		    			   CodeList c= codelstIterator2.next();
		    			   pw.print("<li class='options'>"+ c.getDescription()+"</li>");   
		    		   }
		    		   pw.println("</ul>");
	    		   }
	    		   pw.println("</td></tr>");
	    	   }
	    	   
	       }
	        pw.println("</TABLE>" +
	        		"<table><tr><td class='trLeft'>Name of the Person Completint the Form : __________________</td>" +
	        		"<td class='trRight'>Signature of the Person Completint the Form : __________________</td></tr></table></body></html>");
	        pw.close();
	        //log.debug("HTML file created :::: ");
		}catch(Exception e){
			//log.debug("Exception occured :::: "+e);
		}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return dynaFormPdfGenerator(filePath1,filePath,formId);
	}
	
	public ByteArrayOutputStream dynaFormPdfGenerator(String filePath,String htmlFile,String formId){
		Calendar now1 = Calendar.getInstance();
		String pdfFileName=formId+"reportpdf"+"["+ System.currentTimeMillis() + "].pdf" ;
		String pdfFile=filePath + "/" + pdfFileName ;
		String pdfFileNameNew ="";
		ByteArrayOutputStream baos = new ByteArrayOutputStream();       
		
	try
	{	
		
		File fin=new File(htmlFile);
		Document pdfDocument = new Document(PageSize.A3, 50, 50, 50, 50);
        Reader htmlreader = new BufferedReader(new InputStreamReader(
                                 new FileInputStream(fin)
                                ));
        PdfWriter writer = PdfWriter.getInstance(pdfDocument, baos);
        //log.debug("Pdf writer instance :::: ");
		/*
		Phrase header;
		PdfPTable footer;
		header = new Phrase("**** THIS IS HEADER PART OF THIS PDF ****");
		footer = new PdfPTable(1);
		footer.setTotalWidth(300);
		footer.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		footer.addCell(new Phrase(new Chunk("**** THIS IS FOOTER PART OF THIS PDF ****")
		.setAction(new PdfAction(PdfAction.FIRSTPAGE))));
		*/
		//
        
        pdfDocument.open();
        //StyleSheet styles = new StyleSheet();
        HashMap<String , String> QseqHM=new HashMap<String , String>();
        QseqHM.put("width", "10%");
       // QseqHM.put("vertical-align", "top");
        QseqHM.put("align", "left");
        QseqHM.put("color", "red");
        
        HashMap<String , String> QdescHM=new HashMap<String , String>();
        QdescHM.put("width", "90%");
       // QdescHM.put("vertical-align", "top");
        QdescHM.put("align", "right");
        QdescHM.put("color", "green");
        
        HashMap<String , String> liHM=new HashMap<String , String>();
        liHM.put("display", "inline");
        liHM.put("list-style-type", "circle");
//        liHM.put("padding-right", "20px");
        liHM.put("color", "blue");
        
        HashMap<String , String> tableCssHM=new HashMap<String , String>();
        tableCssHM.put("width", "900px");
        tableCssHM.put("cellpadding", "5");
        tableCssHM.put("cellspacing", "10");
        tableCssHM.put("border", "0");
        StyleSheet styles = new StyleSheet();
        //styles.loadTagStyle("body", "font", "Bitstream Vera Sans");
//        styles.loadTagStyle("li", "display","inline");
//        styles.loadTagStyle("li", "list-style-type","none");
//        styles.loadTagStyle("li", "padding-right","20px");
//        styles.loadTagStyle("li", "padding-left","20px");
//        styles.loadTagStyle("li", "color", "red");
//        styles.loadStyle("tableCss", tableCssHM);
        //styles.applyStyle("Qseq"	, QseqHM);
        
        styles.loadStyle("Qseq", "align", "left");
        styles.loadStyle("Qseq", "width", "20em");
        //styles.loadStyle("Qdesc", QdescHM);
       // styles.loadStyle("Qdesc", "width","80em");
        //styles.loadStyle("Qdesc", "align");
        //styles.loadStyle("options", liHM);
        //styles.loadStyle("Qseq", QseqHM);
        styles.loadStyle("tr1Left", "align", "left");
        styles.loadStyle("tr2Left", "align", "left");
        styles.loadStyle("tr1Right", "align", "right");
        styles.loadStyle("tr2Right", "align", "right");
        styles.loadStyle("nmdpHead", "align", "center");
        styles.loadTagStyle("li", "display", "inline");
        styles.loadTagStyle("li", "list-style-type", "none");
        styles.loadTagStyle("li", "padding-right", "2pt");
        
        
       // styles.loadStyle("Qseq", "align", "left");
//        styles.loadTagStyle("td", "color","green");
//        styles.loadTagStyle("td", "background-color","red");
//        styles.loadStyle("td.Qseq", "color"	, "red");
//        styles.loadStyle("td.Qdesc", "color"	, "green");
        //styles.loadStyle("", arg1, arg2);
        
        ArrayList arrayElementList = HTMLWorker.parseToList(htmlreader, styles);
		for (int i1 = 0; i1 < arrayElementList.size(); ++i1) {
            Element e = (Element) arrayElementList.get(i1);
            //log.debug("Element :::: "+e.toString());
            pdfDocument.add(e);
        }
		//log.debug("Before closing the pdfDocument :::: ");
		//
		//public void onEndPage(PdfWriter writer, Document document) {
		//PdfContentByte cb = writer.getDirectContent();
		////log.debug("pdfDocument.getPageNumber()::::"+pdfDocument.getPageNumber());
		//if (pdfDocument.getPageNumber() > 1) {
		
		//ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, header,(pdfDocument.right() - pdfDocument.left()) / 2+ pdfDocument.leftMargin(), pdfDocument.top() + 10, 0);

		//footer.writeSelectedRows(0, -1,(pdfDocument.right() - pdfDocument.left() - 300) / 2+ pdfDocument.leftMargin(), pdfDocument.bottom() - 10, cb);
		//}
		//}
		//
		//log.debug("style sheet :::: "+pdfDocument.getClass());
        pdfDocument.close();
        //log.debug("style sheet :::: "+pdfDocument.getHtmlStyleClass());
        byte[] bs = baos.toByteArray();
        //String pdfBase64 = Base64.encodeBytes(bs); //output
        FileOutputStream outPdf = new FileOutputStream(pdfFile);
        outPdf.write(bs);
        outPdf.close();

		pdfFileNameNew=formId+"reportpdfnew"+"["+ System.currentTimeMillis() + "].pdf" ;
		String pdfFileNew=filePath + "/" + pdfFileNameNew ;
        
        PdfReader originalPdf = new PdfReader(pdfFile);
		PdfStamper stamper = new PdfStamper(originalPdf, new FileOutputStream(pdfFileNew));
		PdfContentByte under = null;
		PdfContentByte over = null;
		int totalPages = originalPdf.getNumberOfPages();
		
		String repDt="";
		String repTime = "";
		Date dt = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat ("MMM dd, yyyy");
		repDt = sdf.format(dt) ;
		SimpleDateFormat sd = new SimpleDateFormat ("hh:mm a" );
		repTime = sd.format(dt);
		
		for (int pge = 1; pge <= totalPages; pge++) {
		
			under = stamper.getUnderContent(pge);
			over = stamper.getOverContent(pge);
			
			//header date, time and page number part.
			over.beginText();
			BaseFont bf1 = BaseFont.createFont(BaseFont.COURIER, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);  
			over.setFontAndSize(bf1 , 9f);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, "Report Date: "+ repDt, 20f,   1150f,0f);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, "Report Time: "+ repTime, 20f,   1135f,0f);
			over.showTextAligned(PdfContentByte.ALIGN_RIGHT, "Page " + pge + " of " + totalPages, 720f,  1150f,0f);  
			over.endText();
			
			//footer copyright part.
			under.beginText();
			BaseFont bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);  
			under.setFontAndSize(bf , 9f);
			under.showTextAligned(PdfContentByte.ALIGN_CENTER, "Copyright 2011 National Morrow Donor Program. All rights reserved" , 420f, 10f,0f); 
			under.showTextAligned(PdfContentByte.ALIGN_CENTER, "National Morrow Donor Program Proprietary and Confidential Information. " 
					, 420f, 2f,0f);  
			under.endText();  
		
		}
		pdfDocument.open();
		//log.debug("style sheet :::: "+pdfDocument.getHtmlStyleClass());
		pdfDocument.close();
		originalPdf.close();
		stamper.close();
		filepath="/velos/jsp/temp/"+pdfFileNameNew;
		//log.debug("path :::: "+ filepath);
		setFilepath(filepath);
		}
		catch (Exception e)
		{
			//log.debug("Error in Creation of PDF file" + e + e.getMessage());
		}
	return baos;
	
//		String path=fileDnPath + "?file=" + pdfFileNameNew +"&mod=R";
//		
//		getRequest().getSession().setAttribute("pdfPath",path);
		
		
	}
	public String getFormsInPdf(String formId,Long entityId){
		String filePath=null;
		String fileName=null;

		List<CodeList> lstcodeList = new ArrayList<CodeList>();
		
		String finalFilePath=null;
		try{
			String tempDir = request.getRealPath("")+File.separator+"jsp";
			tempDir = tempDir+File.separator+"temp";
			File file = new File(tempDir);
			if(!file.exists()){
				file.mkdir();
			}

			filePath = file.getAbsolutePath();
			Calendar now = Calendar.getInstance();
			fileName=File.separator+formId+"pdf"+"["+ now.get(now.DAY_OF_MONTH) + now.get(Calendar.MONTH) +
				(now.get(now.YEAR) - 1900)+ now.get(now.HOUR_OF_DAY)+ now.get(now.MINUTE)+ now.get(now.SECOND) + "]."+"pdf";
			filePath = filePath+fileName;
			
			mrqFormList = dynaFrmController.getFormQues(entityId,formId);
			if(entityId!=null && !entityId.equals("")){
				cdrCbuPojo.setCordID(entityId);
				cdrCbuPojo = cbuController.getCordInfoById(cdrCbuPojo);
			}

			String reportNo = new CBUAction().getXMLTagValue(VelosGarudaConstants.CBUASSESSMENT_REPNO);
			String versionNo = new CBUAction().getXMLTagValue(VelosGarudaConstants.CBUASSESSMENT_VERNO);
			
			FontFactory.register(getContext().getRealPath("")+"/jsp/font/arial_narrow.TTF");
			Font footerFont = FontFactory.getFont("arial_narrow", 8, Font.NORMAL);

			Document document=new Document();
			PdfWriter.getInstance(document,new FileOutputStream(filePath));
			document.open();

			/*Date dt = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat ("MMM dd, yyyy");
			String repDt = "";
			String repTime = "";
			repDt = sdf.format(dt) ;
			SimpleDateFormat sd = new SimpleDateFormat ("hh:mm a" );
			repTime = sd.format(dt);

			HeaderFooter footer = new HeaderFooter(new Phrase(getText("Report Number: ")+reportNo+"\n"+getText("Version Number: ")+versionNo, footerFont), false);
			HeaderFooter topHeader = new HeaderFooter(new Phrase(getText("Print Date: ")+repDt+"\n"+getText("Print Time: ")+repTime, footerFont), false);
			topHeader.setBorder(0);
			
			footer.setBorder(Rectangle.NO_BORDER);
			footer.setAlignment(Element.ALIGN_LEFT);
			document.setHeader(topHeader);
			document.setFooter(footer);*/

			Paragraph nmdpPara = new Paragraph("National Marrow Donnor Program");
			nmdpPara.setAlignment(Element.ALIGN_CENTER);
			nmdpPara.setFont(new Font(Font.BOLD));
			document.add(nmdpPara);
			
			if(formId.equals(VelosGarudaConstants.FORM_MRQ)){
				nmdpPara = new Paragraph("Maternal Risk Questionnaire Report for HCP");
				nmdpPara.setAlignment(Element.ALIGN_CENTER);
				nmdpPara.setFont(new Font(Font.BOLD));
				document.add(nmdpPara);
			}else if(formId.equals(VelosGarudaConstants.FORM_FMHQ)){
				nmdpPara = new Paragraph("Family Medical History Questionnaire Report for HCP");
				nmdpPara.setAlignment(Element.ALIGN_CENTER);
				nmdpPara.setFont(new Font(Font.BOLD));
				document.add(nmdpPara);
			}
			
			PdfPTable checkBox = new PdfPTable(1);
			checkBox.setWidthPercentage(2);
			checkBox.getDefaultCell().setBorder(PdfPCell.BOX);
			PdfPCell checkBoxCell = new PdfPCell();
			checkBox.addCell(checkBoxCell);
			
			PdfPTable cbbInfoTable = new PdfPTable(3);
			cbbInfoTable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
			cbbInfoTable.setWidthPercentage(100);
			
			PdfPCell cell1 = new PdfPCell(new Phrase("Cord Blood Unit Registry Id : "+cdrCbuPojo.getRegistryId()));
			cell1.setColspan(2);
			cell1.setBorder(PdfPCell.NO_BORDER);
			cbbInfoTable.addCell(cell1);
			cbbInfoTable.addCell("Cord Blood Bank ID : "+cdrCbuPojo.getSite().getSiteSequence()); 
			
			String numOnBang = null;
			if(cdrCbuPojo.getNumberOnCbuBag() != null && !cdrCbuPojo.getNumberOnCbuBag().equals("")){
				numOnBang=cdrCbuPojo.getNumberOnCbuBag();
			}
			else{
				numOnBang="";
			}
			
			cell1 = new PdfPCell(new Phrase("CBB ID on the Bag : " + numOnBang));
			cell1.setColspan(2);
			cell1.setBorder(PdfPCell.NO_BORDER);
			
			cbbInfoTable.addCell(cell1);
			cbbInfoTable.addCell("Cord Blood Bank Name : " + cdrCbuPojo.getSite().getSiteName());

			PdfPTable QuestTable = new PdfPTable(15);
			QuestTable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
			QuestTable.setWidthPercentage(100);
			
			PdfPCell cell = new PdfPCell(new Paragraph("Question No."));
			cell.setBorder(PdfPCell.NO_BORDER);
			cell.setColspan(3);
			QuestTable.addCell(cell);
			
			cell = new PdfPCell(new Paragraph("Question Description"));
			cell.setBorder(PdfPCell.NO_BORDER);
			cell.setColspan(12);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			QuestTable.addCell(cell);
			
			PdfPTable optTable = new PdfPTable(2);
			optTable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
			optTable.setWidthPercentage(100);
			Iterator iterator = mrqFormList.iterator();

			while(iterator.hasNext()){
				Object [] obj = (Object[])iterator.next();
		    	   if(obj[8] != null && !obj[8].equals("")){
		    		   lstcodeList = CodelstHelper.getCodelst(obj[8].toString());
		    	}
		    	String fieldType = "";
		    	if(obj[2]!=null && !obj[2].equals("")){
		    		   fieldType = getCodeListDescByPk(Long.parseLong(obj[2].toString()));
		    	}
		    	if(obj[16].toString().contains(".")){
		    		QuestTable.addCell("");
			    	QuestTable.addCell(obj[16]+".");
					if(fieldType.equals(VelosGarudaConstants.TEXT_FIELD)){
						cell = new PdfPCell(new Paragraph(obj[1]+" __ __ __ __ __ __ __ __ __ __ __ __ __ __ __"));
						cell.setColspan(13);
						cell.setBorder(PdfPCell.NO_BORDER);
						QuestTable.addCell(cell);
			    	}else {
			    		cell = new PdfPCell(new Paragraph(obj[1].toString()));
						cell.setColspan(13);
						cell.setBorder(PdfPCell.NO_BORDER);
						QuestTable.addCell(cell);   
			    	}
					if(fieldType.equals(VelosGarudaConstants.DROPDOWN_FIELD)){
						if(lstcodeList!=null && lstcodeList.size()>0){
							Iterator<CodeList> codelstIterator1=lstcodeList.iterator();
							optTable = new PdfPTable(2);
							optTable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
					   		optTable.setWidthPercentage(100);
					   		PdfPTable oneRowTable=new PdfPTable(10);
					   		while(codelstIterator1.hasNext()){
					   			CodeList c= codelstIterator1.next();
					   			oneRowTable=new PdfPTable(10);
					   			cell=new PdfPCell(checkBox);
					   			cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
					   			cell.setBorder(PdfPCell.NO_BORDER);
					   			oneRowTable.addCell(cell);
					   			cell=new PdfPCell(new Phrase(c.getDescription()));
					   			cell.setBorder(PdfPCell.NO_BORDER);
					   			cell.setColspan(9);
					   			oneRowTable.addCell(cell);
					   			optTable.addCell(oneRowTable);
					   		}
					   		cell = new PdfPCell(optTable);
					   		cell.setBorder(PdfPCell.NO_BORDER);
					   		cell.setColspan(13);
			    			QuestTable.addCell("");
			    			QuestTable.addCell("");
			    			QuestTable.addCell(cell);
			    		}
					}
					if(fieldType.equals(VelosGarudaConstants.MULTISELECT_FIELD)){
						if(lstcodeList!=null && lstcodeList.size()>0){
							Iterator<CodeList> codelstIterator2=lstcodeList.iterator();
				    		optTable = new PdfPTable(2);
				   			optTable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
				   			optTable.setWidthPercentage(100);
				    		while(codelstIterator2.hasNext()){
				    			CodeList c= codelstIterator2.next();				    			   
				    			cell = new PdfPCell(new Paragraph(c.getDescription()));
				    			cell.setBorder(PdfPCell.NO_BORDER);
				    			optTable.addCell(cell);
				    		}
				    		cell = new PdfPCell(optTable);
				    		cell.setBorder(PdfPCell.NO_BORDER);
				    		cell.setColspan(13);
				    		QuestTable.addCell("");
				    		QuestTable.addCell("");
				    		QuestTable.addCell(cell);
				    	}
					}
		    		/**/
		    	}else{
		    		/*
		    		*/
		    		QuestTable.addCell(obj[16]+".");
					if(fieldType.equals(VelosGarudaConstants.TEXT_FIELD)){
						cell = new PdfPCell(new Paragraph(obj[1]+" __ __ __ __ __ __ __ __ __ __ __ __ __ __ __"));
						cell.setColspan(14); 
						cell.setBorder(PdfPCell.NO_BORDER);
						QuestTable.addCell(cell);
					}else {
			    		cell = new PdfPCell(new Paragraph(obj[1].toString()));
						cell.setColspan(14);
						cell.setBorder(PdfPCell.NO_BORDER);
						QuestTable.addCell(cell);
					}
					if(fieldType.equals(VelosGarudaConstants.DROPDOWN_FIELD)){
			    		   if(lstcodeList!=null && lstcodeList.size()>0){
			    			   	Iterator<CodeList> codelstIterator1=lstcodeList.iterator();
				    		   	optTable = new PdfPTable(2);
				   				optTable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
				   				optTable.setWidthPercentage(100);
				    		   while(codelstIterator1.hasNext()){
				    			   CodeList c= codelstIterator1.next();
				    			   cell = new PdfPCell(new Paragraph(c.getDescription()));
				    			   cell.setBorder(PdfPCell.NO_BORDER);
				    			   optTable.addCell(cell);
				    		   }
				    		   cell=new PdfPCell(optTable);
			    			   cell.setBorder(PdfPCell.NO_BORDER);
			    			   cell.setColspan(14);
			    			   QuestTable.addCell("");
			    			   QuestTable.addCell(cell);
			    		   }
			    	   }
					if(fieldType.equals(VelosGarudaConstants.MULTISELECT_FIELD)){
			    		   if(lstcodeList!=null && lstcodeList.size()>0){
				    		   	Iterator<CodeList> codelstIterator2=lstcodeList.iterator();
				    		   	optTable = new PdfPTable(2);
				   				optTable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
				   				optTable.setWidthPercentage(100);
				    		   while(codelstIterator2.hasNext()){
				    			   CodeList c= codelstIterator2.next();
				    			   cell = new PdfPCell(new Paragraph(c.getDescription()));
				    			   cell.setBorder(PdfPCell.NO_BORDER);
				    			   optTable.addCell(cell);
				    		   }
				    		   cell=new PdfPCell(optTable);
			    			   cell.setBorder(PdfPCell.NO_BORDER);
			    			   cell.setColspan(14);
			    			   QuestTable.addCell("");
			    			   QuestTable.addCell(cell);
			    		   }
			    	   }
		    	}
				cell=new PdfPCell();
		    	cell.setBorder(PdfPCell.NO_BORDER);
		    	cell.setColspan(15);
		    	QuestTable.addCell(cell);
			}
			
			PdfPTable endTable=new PdfPTable(5);
			endTable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
			endTable.setWidthPercentage(100);
			endTable.addCell("");
			
			cell= new PdfPCell(new Paragraph("Name of Person Completing Form :"));
			cell.setBorder(PdfPCell.NO_BORDER);
			cell.setColspan(4);
			endTable.addCell(cell);
			
			cell=null;
			endTable.addCell("");
			
			cell=new PdfPCell(new Paragraph("E-SIGN PROVIDED :"));
			cell.setBorder(PdfPCell.NO_BORDER);
			cell.setColspan(4);
			endTable.addCell(cell);
			
			cell=null;
			endTable.addCell("");
			
			cell=new PdfPCell(new Paragraph("Actual Date of Completion of Form :"));
			cell.setBorder(PdfPCell.NO_BORDER);
			cell.setColspan(4);
			endTable.addCell(cell);
			
			document.add(cbbInfoTable);
			Paragraph linePara=new Paragraph("___________________________________________________________________________");
			document.add(linePara);
			linePara=new Paragraph();
			document.add(linePara);
			document.add(QuestTable);
			linePara=new Paragraph();
			document.add(linePara);
			document.add(endTable);
			document.close();
			request.setAttribute("MRQFilePath", fileName);
			finalFilePath="temp/"+fileName;

		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return finalFilePath;
	}
	
}