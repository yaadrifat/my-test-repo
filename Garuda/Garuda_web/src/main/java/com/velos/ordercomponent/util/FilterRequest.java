package com.velos.ordercomponent.util;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FilterRequest implements Filter {

	private FilterConfig filterConfig;
	
	@Override
	public void destroy() {		
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {		
		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse res = (HttpServletResponse)response;
		String url = req.getRequestURI();
		if(url.indexOf("?")!=-1){
			Enumeration enumeration = req.getParameterNames();
			while(enumeration.hasMoreElements()){
				String paramName = (String)enumeration.nextElement();
				String[] values;
				values = req.getParameterValues(paramName);
				if(values.length==1){
					req.setAttribute(paramName,values[0]);
				}else{
					req.setAttribute(paramName, values);
				}
			}
			url = url.substring(0, url.indexOf("?"));
			RequestDispatcher dispatcher = req.getRequestDispatcher(url);
			dispatcher.forward(req,res);
		}else{		
		chain.doFilter(req,res);
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;		
	}

}
