package com.velos.ordercomponent.queryObject;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.Callable;

import com.velos.ordercomponent.controller.VDAController;

public class LicEligFilterReasonObj implements Runnable{
	
	private VDAController vdaController;
	private String sqlQuery;
	private Map<Long, ArrayList<Long>> reason;
	
	public LicEligFilterReasonObj(String sqlQuery, VDAController vdaController) {
		
		this.vdaController = vdaController;
		this.sqlQuery =  sqlQuery;
		
	}

	/*@Override
	public Map<Long, Object[]> call() throws Exception {
		// TODO Auto-generated method stub
		return vdaController.getQueryBuilderMapObject(sqlQuery);
	}*/

	@Override
	public void run() {
		
		try {
			
			reason = vdaController.getQueryBuilderMapLstObject(sqlQuery);
			System.out.println(" REASON Finished ----------------------- >");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * @return the reason
	 */
	public Map<Long, ArrayList<Long>> getReason() {
		return reason;
	}

}
