package com.velos.ordercomponent.util;

import java.util.Date;

import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.StrutsStatics;
 
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
 
/**
 * Class used to clear the catch in Http response Header.
 *
 * @author nganesavel
 * @version 1.0
 */

public class ClearCacheInterceptor  extends AbstractInterceptor{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
    public String intercept(ActionInvocation invocation) throws Exception {
        final ActionContext context = (ActionContext)invocation.getInvocationContext();
        
        HttpServletResponse response = (HttpServletResponse)context.get(StrutsStatics.HTTP_RESPONSE);
        
        if(response!=null){
	        response.setHeader("Cache-Control", "must-revalidate,private,post-check=0,pre-check=0,max-age=0,proxy-revalidate");
	        response.setHeader("Pragma", "no-cache");
	        response.setHeader("Expires", new Date().toString());
        }

        return invocation.invoke();
    }
}