package com.velos.ordercomponent.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.ordercomponent.business.domain.CdrCbu;
import com.velos.ordercomponent.business.pojoobjects.CBBProcedureInfoPojo;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.EntitySamplesPojo;
import com.velos.ordercomponent.business.pojoobjects.PatLabsPojo;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.controller.CBBController;
import com.velos.ordercomponent.controller.CBUController;
import com.velos.ordercomponent.controller.DynaFormController;
import com.velos.ordercomponent.util.VelosGarudaConstants;

public class CBUDeferStatusAction extends VelosBaseAction {
	public static final Log log = LogFactory.getLog(CBUDeferStatusAction.class);
	private CdrCbuPojo cdrCbuPojo;
	private List<PatLabsPojo> postTestList;
	private EntitySamplesPojo entitySamplesPojo;
	private CBBProcedureInfoPojo cbbProcedureInfoPojo;
	private Boolean defer;
	private List<CBBProcedureInfoPojo> cbbProdList;
	private List<PatLabsPojo> savePostTestList;
	private Boolean submitExternal;
	private String autoDeferRegistryId;
	private Boolean submitCord;
	private Boolean cordCheck;
	public DynaFormController formController;
	private Boolean assessflag;
	private String deferstatus;
	private Long cordStatus;
	private CBBController cbbController;
	DateFormat dat = new SimpleDateFormat("MMM dd, yyyy");
	private String procStartDateStrAdd;
	private String procTermiDateStrAdd;
	private String checkDefer;
	
	
	
	public Boolean getCordCheck() {
		return cordCheck;
	}

	public void setCordCheck(Boolean cordCheck) {
		this.cordCheck = cordCheck;
	}

	public Boolean getSubmitCord() {
		return submitCord;
	}

	public void setSubmitCord(Boolean submitCord) {
		this.submitCord = submitCord;
	}

	public String getAutoDeferRegistryId() {
		return autoDeferRegistryId;
	}

	public void setAutoDeferRegistryId(String autoDeferRegistryId) {
		this.autoDeferRegistryId = autoDeferRegistryId;
	}

	public Boolean getSubmitExternal() {
		return submitExternal;
	}

	public void setSubmitExternal(Boolean submitExternal) {
		this.submitExternal = submitExternal;
	}

	public List<PatLabsPojo> getSavePostTestList() {
		return savePostTestList;
	}

	public void setSavePostTestList(List<PatLabsPojo> savePostTestList) {
		this.savePostTestList = savePostTestList;
	}

	public List<CBBProcedureInfoPojo> getCbbProdList() {
		return cbbProdList;
	}

	public void setCbbProdList(List<CBBProcedureInfoPojo> cbbProdList) {
		this.cbbProdList = cbbProdList;
	}

	public Boolean getDefer() {
		return defer;
	}

	public void setDefer(Boolean defer) {
		this.defer = defer;
	}

	public List<PatLabsPojo> getPostTestList() {
		return postTestList;
	}

	public void setPostTestList(List<PatLabsPojo> postTestList) {
		this.postTestList = postTestList;
	}

	public EntitySamplesPojo getEntitySamplesPojo() {
		return entitySamplesPojo;
	}

	public void setEntitySamplesPojo(EntitySamplesPojo entitySamplesPojo) {
		this.entitySamplesPojo = entitySamplesPojo;
	}

	public CdrCbuPojo getCdrCbuPojo() {
		return cdrCbuPojo;
	}

	public void setCdrCbuPojo(CdrCbuPojo cdrCbuPojo) {
		this.cdrCbuPojo = cdrCbuPojo;
	}	
	

	public CBBProcedureInfoPojo getCbbProcedureInfoPojo() {
		return cbbProcedureInfoPojo;
	}

	public void setCbbProcedureInfoPojo(CBBProcedureInfoPojo cbbProcedureInfoPojo) {
		this.cbbProcedureInfoPojo = cbbProcedureInfoPojo;
	}

	public CBUDeferStatusAction(){	
		cbbController = new CBBController();
	}

	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void getPkCodeIds() {
		try {
			cbbProcedureInfoPojo.setPkProcMethodId(cbbController.getCodeListPkIds(PROCESSING, AUTOMATED));
			cbbProcedureInfoPojo.setPkIfAutomatedOther(cbbController.getCodeListPkIds(IF_AUTOMATED, OTHER));
			cbbProcedureInfoPojo.setPkProdModiOther(cbbController.getCodeListPkIds(PRODUCT_MODI, OTHER));
			cbbProcedureInfoPojo.setPkFreezManuOther(cbbController.getCodeListPkIds(FREEZER_MANUFACTURER, OTHER));
			cbbProcedureInfoPojo.setPkFrozenInBag(cbbController.getCodeListPkIds(FROZEN_IN, BAGS));
			cbbProcedureInfoPojo.setPkFrozenInOther(cbbController.getCodeListPkIds(FROZEN_IN, OTHER));
			cbbProcedureInfoPojo.setPkOtherBagType(cbbController.getCodeListPkIds(BAG_TYPE, OTHER));
			cbbProcedureInfoPojo.setPkBag1Type(cbbController.getCodeListPkIds(NUM_OF_BAGS, ONE_BAG));
			cbbProcedureInfoPojo.setPkBag2Type(cbbController.getCodeListPkIds(NUM_OF_BAGS, TWO_BAG));
			cbbProcedureInfoPojo.setPkNoOfBagsOthers(cbbController.getCodeListPkIds(NUM_OF_BAGS, OTHER_BAG)); 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public String f_getDeferProc() throws Exception{
		  try{
		  String id = request.getParameter("procPk");
		  long storTemp =0;
		  long frozenIn= 0;
		  long noOfSeg = 2;
		  String procStartDt = "";
		  String procTermiDt = "";
		  List<CBBProcedureInfoPojo> lstProc =new ArrayList<CBBProcedureInfoPojo>();
			List<CBBProcedureInfoPojo> lstProc1 =new ArrayList<CBBProcedureInfoPojo>();
			lstProc= cbbController.getCBBProcedureInfo(id);
			Iterator<CBBProcedureInfoPojo> itr1=lstProc.iterator();
			cbbProcedureInfoPojo = new CBBProcedureInfoPojo();
			getPkCodeIds();
			while(itr1.hasNext()){
				CBBProcedureInfoPojo cbbproinfo=new CBBProcedureInfoPojo();
				cbbproinfo=itr1.next();
				cbbproinfo=(CBBProcedureInfoPojo)ObjectTransfer.transferObjects(cbbProcedureInfoPojo,cbbproinfo);
				lstProc1.add(cbbproinfo);
				if(cbbproinfo!=null){
					storTemp = cbbproinfo.getFkStorTemp();
					frozenIn = cbbproinfo.getFkFrozenIn();
					noOfSeg = cbbproinfo.getNoOfSegments();
					if( cbbproinfo.getCbbProcedures().getProcStartDate() != null)
						procStartDt = dat.format(cbbproinfo.getCbbProcedures().getProcStartDate());
					
					if( cbbproinfo.getCbbProcedures().getProcTermiDate() != null)
						procTermiDt = dat.format(cbbproinfo.getCbbProcedures().getProcTermiDate());
				}
			}
			
			//log.debug("storTemp:::"+storTemp);
			//log.debug("frozenIn:::"+frozenIn);
			//log.debug("tubes:::"+getCodeListPkByTypeAndSubtype(FROZEN_IN,FROZEN_IN_TUBES));
			//log.debug("vials:::"+getCodeListPkByTypeAndSubtype(FROZEN_IN,FROZEN_IN_VIALS));
			//log.debug("storTemp1:::"+getCodeListPkByTypeAndSubtype(STORAGE_TEMPERATURE,STORAGE_TEMPERATURE_GTMINUSONETHIRTYFIVE));
			
			setProcStartDateStrAdd(procStartDt);
			setProcTermiDateStrAdd(procTermiDt);
			
			if ((storTemp == getCodeListPkByTypeAndSubtype(STORAGE_TEMPERATURE,STORAGE_TEMPERATURE_GTMINUSONETHIRTYFIVE))
			|| (frozenIn == getCodeListPkByTypeAndSubtype(FROZEN_IN,FROZEN_IN_VIALS) || frozenIn == getCodeListPkByTypeAndSubtype(FROZEN_IN,FROZEN_IN_TUBES))
			 )
			{
				setCheckDefer("true");
			}
			else
			{
				setCheckDefer("false");
			}
		  }catch (Exception e) {
			// TODO: handle exception
				log.error(e.getMessage());
			  e.printStackTrace();
		}
		  return SUCCESS;
		  
  }
	
	public String f_getDeferStatus(){
		Boolean flag = false;
		String registryId="";
		setSubmitCord(false);
		CBUAction cbuAction = new CBUAction();
		//System.out.println("getDeferStatus@@@@@@@@@@@@@@@@@");
		try{
			if(getSubmitExternal()!=null && getSubmitExternal()==true){
			//	System.out.println("Inisde if of getDeferStatus!!!!!!!!!!!!!!!!!");
				CdrCbuPojo cdrCbuPojo = new CdrCbuPojo();			   
				cdrCbuPojo = new CBUController().getCordInfoById(getCdrCbuPojo());
				setCdrCbuPojo(cdrCbuPojo);
				 registryId = cdrCbuPojo.getRegistryId();
				//System.out.println("Inisde if of getDeferStatus!!!!!!!!!!!!!!!!!"+cdrCbuPojo.getCordID());
				Long valuePostFkTimingTest = getCodeListPkByTypeAndSubtype(VelosGarudaConstants.TIMING_OF_TEST, VelosGarudaConstants.POST_TEST);
				postTestList = new CBUController().getPatLabsPojoInfo(getCdrCbuPojo().getFkSpecimenId(),valuePostFkTimingTest);
				setPostTestList(postTestList);		
				entitySamplesPojo = new CBUController().getEntitySampleInfo(getCdrCbuPojo().getCordID());
				setEntitySamplesPojo(entitySamplesPojo);
				flag = cbuAction.setAutoDeferStatusForCord(getPostTestList(),getCdrCbuPojo(),getEntitySamplesPojo());
				if(flag==false)
				{
					if(cbuAction.getCdrCbuPojo()!=null && cbuAction.getCdrCbuPojo().getFkCordCbuStatus()!=null && cbuAction.getCdrCbuPojo().getFkCordCbuStatus().equals(getCbuStatusPkByCode(AV))){
					   getCdrCbuPojo().setCordSearchable(1l);
					   setGuid();
					   int matCount = new CBUController().getCordByCountByMaternalRegistry(getCdrCbuPojo().getRegistryMaternalId());
					   getCdrCbuPojo().setPregRefNumber(Long.parseLong(Integer.toString(matCount)));
					   if(getCordCheck()==null)
					     new CBUController().saveOrUpdateCordInfo(getCdrCbuPojo());
					}
				}
				if(getCdrCbuPojo().getCordSearchable()!=null && getCdrCbuPojo().getCordSearchable()==1l){
					setSubmitCord(true);
				}
			}else{
				//System.out.println("Else is called!!!!!!!!!!!1");
				CdrCbuPojo cdrCbuPojo = new CdrCbuPojo();
				if(request.getAttribute("cordIdfrm")!=null ){
					Long cordID = Long.parseLong(request.getAttribute("cordIdfrm").toString());
					//System.out.println("cordID"+cordID);
					cdrCbuPojo.setCordID(cordID);
					setCdrCbuPojo(cdrCbuPojo);
				}
				
				List<PatLabsPojo> blist = (List<PatLabsPojo>) getSavePostTestList();
				flag = new CBUAction().setAutoDeferStatusForCord(blist,getCdrCbuPojo(),getEntitySamplesPojo());
				cdrCbuPojo = new CBUController().getCordInfoById(getCdrCbuPojo());
				setCdrCbuPojo(cdrCbuPojo);
				if(getCdrCbuPojo().getCordSearchable()!=null && getCdrCbuPojo().getCordSearchable()==1l){
					setSubmitCord(true);
				}
			}
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		if(flag==true){
			formController= new DynaFormController();
			assessflag = cbuAction.getIDMDeferStatus(getCdrCbuPojo().getCordID());
			if(assessflag==true){
				try {
					deferstatus = formController.getidmdefervalue(getCdrCbuPojo().getCordID());
					if(deferstatus.equals("2")){
						//System.out.println("Inside If"+deferstatus);
						flag=false;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
						
			}
		}
		setDefer(flag);
		setAutoDeferRegistryId(registryId);
		String returnval="";
		if(request.getAttribute("assess_Flg")!=null ){
			returnval=returnval+flag;
		}
		else{
			returnval="success";
		}
		//System.out.println("ReturnVal:::"+returnval);
		return returnval;
	}
	
	public void setGuid(){
		   try{
			   CBUController cbuController = new CBUController();
			   if(StringUtils.isEmpty(getCdrCbuPojo().getGuid())){
				    String guid = cbuController.generateGuid();
					if(guid!=null){
					  getCdrCbuPojo().setGuid(guid);
					}
			    }
			   if(StringUtils.isEmpty(getCdrCbuPojo().getMatGuid())){
					String matGuid = null;
					String criteria1 = "and registryMaternalId='"+cdrCbuPojo.getRegistryMaternalId()+"' and cordSearchable=1 ";
					List<Object> regData = cbuController.getCBUCordInfoList(CDRCBU_OBJECTNAME, criteria1,null);
					if(regData!=null && regData.size()>0){
				       CdrCbu cbuPojo = (CdrCbu)regData.get(0);
				       if(cbuPojo!=null && !StringUtils.isEmpty(cbuPojo.getMatGuid())){
				    	   matGuid = cbuPojo.getMatGuid();
				       }
					}		
					if(matGuid==null){
						  matGuid = cbuController.generateGuid();
						  getCdrCbuPojo().setMatGuid(matGuid);
					}else{
						getCdrCbuPojo().setMatGuid(matGuid);
					}
			   }
		   }catch (Exception e) {
			e.printStackTrace();
		   }
	   }

	public DynaFormController getFormController() {
		return formController;
	}

	public void setFormController(DynaFormController formController) {
		this.formController = formController;
	}

	public Boolean getAssessflag() {
		return assessflag;
	}

	public void setAssessflag(Boolean assessflag) {
		this.assessflag = assessflag;
	}

	public String getDeferstatus() {
		return deferstatus;
	}

	public void setDeferstatus(String deferstatus) {
		this.deferstatus = deferstatus;
	}

	public Long getCordStatus() {
		return cordStatus;
	}

	public void setCordStatus(Long cordStatus) {
		this.cordStatus = cordStatus;
	}

	public DateFormat getDat() {
		return dat;
	}

	public void setDat(DateFormat dat) {
		this.dat = dat;
	}

	public String getProcStartDateStrAdd() {
		return procStartDateStrAdd;
	}

	public void setProcStartDateStrAdd(String procStartDateStrAdd) {
		this.procStartDateStrAdd = procStartDateStrAdd;
	}

	public String getProcTermiDateStrAdd() {
		return procTermiDateStrAdd;
	}

	public void setProcTermiDateStrAdd(String procTermiDateStrAdd) {
		this.procTermiDateStrAdd = procTermiDateStrAdd;
	}

	public String getCheckDefer() {
		return checkDefer;
	}

	public void setCheckDefer(String checkDefer) {
		this.checkDefer = checkDefer;
	}

}
