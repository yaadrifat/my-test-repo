package com.velos.ordercomponent.controller;

import java.util.List;

import com.velos.ordercomponent.business.pojoobjects.LabTestGrpPojo;
import com.velos.ordercomponent.business.pojoobjects.LabTestPojo;
import com.velos.ordercomponent.service.VelosService;
import com.velos.ordercomponent.service.impl.VelosServiceImpl;

/**
 * @author jamarendra
 *
 */

public class LabTestcontroller {
	VelosService service;
	
	public List getLabTestgrp() throws Exception {
		service = VelosServiceImpl.getService();
		return service.getLabTestgrp();
	}
	
	public List getLabTestList(Long pklabgrp) throws Exception {
		service = VelosServiceImpl.getService();
		return service.getLatTestlist(pklabgrp);
	}
	
	public Long getLabgrppks(String grptype) throws Exception {
		service = VelosServiceImpl.getService();
		return service.getLabgrppks(grptype);
	}
	
	public LabTestGrpPojo saveorupdateLTestgrp(LabTestGrpPojo labTestGrpPojo, LabTestPojo labTestPojo) throws Exception {
		service = VelosServiceImpl.getService();
		return service.saveorupdateLTestgrp(labTestGrpPojo,labTestPojo);
	}
	
	public Long getLabTestInfoByName(String testName) throws Exception {
		service = VelosServiceImpl.getService();
		return service.getLabTestInfoByName(testName);
	}
	
	public Long getLabTestInfoByName(String testName , String testShrtName) throws Exception {
		service = VelosServiceImpl.getService();
		return service.getLabTestInfoByName(testName,testShrtName);
	}
}
