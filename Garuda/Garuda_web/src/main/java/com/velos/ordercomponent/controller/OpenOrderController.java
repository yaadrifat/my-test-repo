package com.velos.ordercomponent.controller;

import java.util.List;

//import com.aithent.gems.objects.WorkFlowProcessObject;
import com.aithent.gems.objects.WorkFlowProcessObject;
import com.velos.ordercomponent.business.domain.CBUStatus;
import com.velos.ordercomponent.business.pojoobjects.CBUploadInfoPojo;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.FilterDataPojo;
import com.velos.ordercomponent.business.pojoobjects.HLAPojo;
import com.velos.ordercomponent.business.pojoobjects.LabMaintPojo;
import com.velos.ordercomponent.business.pojoobjects.OrderAttachmentPojo;
import com.velos.ordercomponent.business.pojoobjects.OrderPojo;
import com.velos.ordercomponent.business.pojoobjects.PackingSlipPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.pojoobjects.ShipmentPojo;
import com.velos.ordercomponent.business.pojoobjects.TaskListPojo;
import com.velos.ordercomponent.service.PendingOrdersService;
import com.velos.ordercomponent.service.VelosService;
import com.velos.ordercomponent.service.impl.PendingOrdersServiceImpl;
import com.velos.ordercomponent.service.impl.VelosServiceImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class OpenOrderController {
	public static final Log log = LogFactory.getLog(OpenOrderController.class);
	VelosService service;
	PendingOrdersService pendingOrdersService;
	public OrderAttachmentPojo saveOrderAttachment(OrderAttachmentPojo orderAttachmentPojo) throws Exception{
		//log.debug("IN OrderAttachmentController saveorderattachment");
		service = VelosServiceImpl.getService();
		return service.saveOrderAttachment(orderAttachmentPojo);
	}
	
	public String getCBUStatusCode(String pkcbustatus) throws Exception {
		service = VelosServiceImpl.getService();
		return service.getCBUStatusCode(pkcbustatus);
		
	}
	
	public String getQueryForOpenOrderList(CdrCbuPojo cdrCbuPojo) throws Exception {
		service = VelosServiceImpl.getService();
		return service.getQueryForOpenOrderList(cdrCbuPojo);
		 
	}
	public Long getCRI_CompletedFlag(Long pk_cord) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getCRI_CompletedFlag(pk_cord);
	}
	
	public String queryForCategoryList() throws Exception  {
		service = VelosServiceImpl.getService();
		return service.queryForCategoryList();
		
	}
	public String populateDropdownValues(Long selVal) throws Exception{
		service = VelosServiceImpl.getService();
		return service.populateDropdownValues(selVal);
		
	}
	
	public Object getCordId(String objectName,String id,String fieldName) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCordId(objectName,id,fieldName);
	}
	
	public CBUploadInfoPojo saveCatUploadInfo(CBUploadInfoPojo cbUploadInfoPojo) throws Exception{
		//log.debug("IN OrderAttachmentController saveCBUploadInfoPojo");
		service = VelosServiceImpl.getService();
		return service.saveCatUploadInfo(cbUploadInfoPojo);
	}
	
	public LabMaintPojo saveNewLabTestInfo(LabMaintPojo labMaintPojo) throws Exception{
		//log.debug("IN OrderAttachmentController saveNewLabTestInfo");
		service = VelosServiceImpl.getService();
		return service.saveNewLabTestInfo(labMaintPojo);
	}
	
	public List getResultsForFilter(String orderType,String colName,String colData,String pkOrderType, String pending,String order_status) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		
		//return service.getResultsForFilter(orderType, colName, colData, pkOrderType, pending,order_status);
		return pendingOrdersService.getResultsForCancelFilter(orderType, colName, colData, pkOrderType, pending, order_status);
	}
	
	public List getOrderDetails(String orderId) throws Exception{
		//service=VelosServiceImpl.getService();
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		//return service.getOrderDetails(orderId);
		return pendingOrdersService.getOrderDetails(orderId);
	}
	
	public List getCtOrderDetails(String orderId) throws Exception{
		//service=VelosServiceImpl.getService();
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		//return service.getCtOrderDetails(orderId);
		return pendingOrdersService.getCtOrderDetails(orderId);
	}
	
	public void autoCompleteCRI(Long pkcord) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		pendingOrdersService.autoCompleteCRI(pkcord);
	}
	
	public CdrCbuPojo submitCordStatus(CdrCbuPojo cdrCbuPojo) throws Exception{
		//service=VelosServiceImpl.getService();
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		//return service.submitCordStatus(cdrCbuPojo);
		return pendingOrdersService.submitCordStatus(cdrCbuPojo);
	}

	public List getResultsForCancelFilter(String orderType, String colName,	String colData, String pkOrderType, String cancelled,String order_status) throws Exception {
		//service=VelosServiceImpl.getService();
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		//return service.getResultsForCancelFilter(orderType, colName, colData, pkOrderType, cancelled,order_status);
		return pendingOrdersService.getResultsForCancelFilter(orderType, colName, colData, pkOrderType, cancelled, order_status);
	}
	
	public OrderPojo updateAcknowledgement(OrderPojo orderpojo) throws Exception{
		service=VelosServiceImpl.getService();
		return service.updateAcknowledgement(orderpojo);
		
	}
	public void submitCtResolution(String pkorderId,String availDt,String resolbyCbb,String resolbyTc,String ackTcResolution,String acceptToCancel,String pkTempunavail,String pkcordId,String rejectDt,String rejectBy,String userID)throws Exception{
		service=VelosServiceImpl.getService();
		service.submitCtResolution(pkorderId,availDt,resolbyCbb,resolbyTc,ackTcResolution,acceptToCancel,pkTempunavail,pkcordId,rejectDt,rejectBy,userID);
	}
	public void submitHeResolution(String pkorderId,String availDt,String resolbyCbb,String resolbyTc,String ackTcResolution,String pkTempunavail)throws Exception{
		service=VelosServiceImpl.getService();
		service.submitHeResolution(pkorderId,availDt,resolbyCbb,resolbyTc,ackTcResolution,pkTempunavail);
	}

	
	
	public List getAntigensForReceipant(String receipantId,String entityType) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getAntigensForReceipant(receipantId,entityType);
		
	}
	
	
	public ShipmentPojo saveShipmentInfo(ShipmentPojo shipmentPojo) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.saveShipmentInfo(shipmentPojo);
	}
	
	public OrderPojo updateOrderDetails(OrderPojo orderPojo) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.updateOrderDetails(orderPojo);
	}
	
	public ShipmentPojo getshipmentDetails(ShipmentPojo shipmentPojo) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getshipmentDetails(shipmentPojo);
	}
	
	
	public  HLAPojo saveCurrentHlaTyping(HLAPojo hlaPojo) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.saveCurrentHlaTyping(hlaPojo);
	}
	
	public List getWorkFlowNotification(PaginateSearch paginateSearch,int i,Long userId,String siteId,String condition,String OrdByCondition)throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		List<TaskListPojo> l = null;
		try{
			l =	pendingOrdersService.getWorkFlowNotification(paginateSearch,i,userId,siteId,condition,OrdByCondition);
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return l;
	}
	
	public List getAlertNotifications(Long userId,String siteId,String criteria,String orderBy,PaginateSearch paginateSearch,int i) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getAlertNotifications(userId,siteId,criteria,orderBy,paginateSearch,i);
		
	}
			
	public Long getOrderType(Long orderId) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getOrderType(orderId);
	}
	
	public void createAlertInstances(Long orderId) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		pendingOrdersService.createAlertInstances(orderId);
	}
	
	public List getCbbProfile(String siteId,Long userId)throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getCbbProfile(siteId,userId);
	}
	
	public List getCommonUsrLst(String siteId,String count)throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getCommonUsrLst(siteId,count);
	}
	
	public List getLabelAssignLst(String siteId)throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getLabelAssignLst(siteId);
	}
	
	public List getUserslistData(String siteId)throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getUserslistData(siteId);
	}
	
	public Long getPkActivity(String activityname)throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getPkActivity(activityname);
	}
	
	public List getOrders(PaginateSearch paginateSearch,int i,Long userId,String siteId,String condition,String orderByCondition)throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getOrders(paginateSearch,i,userId,siteId,condition,orderByCondition);
	}

	public List getTasksListForOrder(Long orderId) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getTasksListForOrder(orderId);
	}
	public  String checkShedShipDt(String orderid,String cbuid,String pkavailable) throws Exception{
		service=VelosServiceImpl.getService();
		return service.checkShedShipDt(orderid,cbuid,pkavailable);
	}
	public List getallOrderDetails(PaginateSearch paginateSearch,int i,String filterflag,String ordertypeval,String fksiteId,Long userId,String codition,String orderBy) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getallOrderDetails(paginateSearch,i,filterflag,ordertypeval,fksiteId,userId,codition,orderBy);
	}
	
	public int updateAlertStatus(Long alertInstanceId) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.updateAlertStatus(alertInstanceId);
	}
	
	public List getShipmentInfo(Long orderId,Long shipmentType) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getShipmentInfo(orderId,shipmentType);
		
	}
	
	public List getCustomFilterData(Long userId,Long module,Long submodule) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getCustomFilterData(userId,module,submodule);
		
	}
	
	
	public List getLatestHlaInfo(Long orderId,Long entityId,String entityType) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getLatestHlaInfo(orderId, entityId,entityType);
	}
	
	public List getcordAvailConfirm(Long orderId) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getcordAvailConfirm(orderId);
	}
	
	
	public void updateOrderLastReviewedBy(Long orderId,Long user_id) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		pendingOrdersService.updateOrderLastReviewedBy(orderId,user_id);
	}
	
	public List getOrderResol(Long orderId) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getOrderResol(orderId);
	}
	
	public List fetchNextTask(WorkFlowProcessObject objWorkFlow) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.fetchNextTask(objWorkFlow);
	}
	
	public List savedInprogress(PaginateSearch paginationSearch,String SIPCondition,Long userId,String fksiteId,String orderBy,int i)throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.savedInprogress(paginationSearch,SIPCondition,userId,fksiteId,orderBy,i);
	}
	
	public List notAvailCbu(PaginateSearch paginationSearch,String nacbuCondition,int i,String fksiteId,String nacbuOrderBy,Long userId)throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.notAvailCbu(paginationSearch,nacbuCondition,i,fksiteId,nacbuOrderBy,userId);
	}
	
	public String getClinicChkStatus(Long cordId) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getClinicChkStatus(cordId);
	}
	
	public List getfinalReviewStatus(Long cordId,Long orderId) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getfinalReviewStatus(cordId,orderId);
	}
	
	public String getMinimumCriteriaStat(Long orderId) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getMinimumCriteriaStat(orderId);
	}
	
	public String getMinimumCriteriaFlagByCordId(Long cordId) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getMinimumCriteriaFlagByCordId(cordId);
	}
	
	public List getpackingSlipDetails(Long pkPackingSlip) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getpackingSlipDetails(pkPackingSlip);
	}

	public String getPkCbuStatus(String ordertype,String codetype,String statuscode)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getPkCbuStatus(ordertype,codetype,statuscode);
		
	}
	public String getTermialStatusByPkOrDesc(Long pkValue, String desc)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getTermialStatusByPkOrDesc(pkValue,desc);
	}
	public List maincbuRequestHistory(String pkcord, String orderId)throws Exception{
		service = VelosServiceImpl.getService();
		return service.maincbuRequestHistory(pkcord,orderId);
		
	}
	public List cbuRequestHistory(String pkcord,String pkorder,String cdrUser)throws Exception{
		service = VelosServiceImpl.getService();
		return service.cbuRequestHistory(pkcord,pkorder,cdrUser);
		
	}
	public List cbuHistory(String pkcord,String orderId,PaginateSearch paginationSearch,int i,String cdrUser,String condition,String orderBy)throws Exception{
		service = VelosServiceImpl.getService();
		return service.cbuHistory(pkcord,orderId,paginationSearch,i,cdrUser,condition,orderBy);
	}
	public List cbbDetailPopup(String pksite,String ordType,String sampleAtlab)throws Exception{
		service = VelosServiceImpl.getService();
		return service.cbbDetailPopup(pksite,ordType,sampleAtlab);
		
	}
	public String getTcResol(String orderId)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getTcResol(orderId);
		
	}
	public List getShedShipDts(String tshipDate,String siteId)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getShedShipDts(tshipDate,siteId);
		
	}
	
	public List getDataFromCodelstCustCol(String tableName,String columnName,String criteria) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getDataFromCodelstCustCol(tableName, columnName, criteria);
	}
	
	public void updateOrderCompleteReqInfoflag(Long cordId) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		pendingOrdersService.updateOrderCompleteReqInfoflag(cordId);
	}
	
	public void updateDataFromCodelstCustCol(String tableName,String columnName,Long value,String criteria,String operation) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		pendingOrdersService.updateDataFromCodelstCustCol(tableName, columnName, value, criteria,operation);
	}
	
	public PackingSlipPojo updatePackingSlipDet(PackingSlipPojo packingSlipPojo) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.updatePackingSlipDet(packingSlipPojo);
	}
	
	public String updateAssignTo(String ordId,String assignto,String flagval,Long loggedIn_userId,String cordIdval)throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.updateAssignTo(ordId,assignto,flagval,loggedIn_userId,cordIdval);
	}
	
	public List getpatientList(Long fkorderid,Long fkcordid)throws Exception{
		service=VelosServiceImpl.getService();
		return service.getpatientList(fkorderid, fkcordid);
	}
	
	public List getShipmentDetails(Long fkorderid,Long fkcordid)throws Exception{
		service=VelosServiceImpl.getService();
		return service.getShipmentDetails(fkorderid, fkcordid);
	}
	
	public List getPatientInfo(Long orderId) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getPatientInfo(orderId);
	}
	
	public List<CBUStatus> getAllCbuStatus() throws Exception{
		service = VelosServiceImpl.getService();
		return service.getAllCbuStatus();
	}
	
	public String getNMDPResearchSample(Long siteId,Long cordId) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getNMDPResearchSample(siteId,cordId);
	}
	
	public Long getReceipientId(Long cordId) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getReceipientId(cordId);
	}
	
	public String getAcknowledgeCancelledOrderFlag(Long orderId,Long cordId) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getAcknowledgeCancelledOrderFlag(orderId, cordId);
	}
	
	public List<FilterDataPojo> customFilterInsert(FilterDataPojo filterDataPojo) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.customFilterInsert(filterDataPojo);
	}
	
	public OrderPojo getRequestDetails(Long orderId) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getRequestDetails(orderId);
	}
	
	public List getQBuilderList(String sql, String orderBy)throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getQBuilderList(sql,orderBy);
	}
	
	public Long getOrderIdFromCordId(Long cordId) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getOrderIdFromCordId(cordId);
	}
	
	public String getFlagForTaskCheck(Long cordId) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getFlagForTaskCheck(cordId);
	}
	
	public Long getDefaultPaperLoc(Long orderId) throws Exception{
		pendingOrdersService=PendingOrdersServiceImpl.getService();
		return pendingOrdersService.getDefaultPaperLoc(orderId);
		
	}
}
