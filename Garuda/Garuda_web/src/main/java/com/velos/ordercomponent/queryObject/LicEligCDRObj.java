package com.velos.ordercomponent.queryObject;
import java.util.List;
import com.velos.ordercomponent.controller.VDAController;

public class LicEligCDRObj implements Runnable {
	
	private VDAController vdaController;
	private String sqlQuery;
	private List<Object> cdrObj;
	
	public LicEligCDRObj(String sqlQuery, VDAController vdaController) {
		
		this.vdaController = vdaController;
		this.sqlQuery =  sqlQuery;
	}

	/*@Override
	public List<Object> call() throws Exception {		 
		
		return vdaController.getQueryBuilderListObject(sqlQuery);
		
	}*/

	@Override
	public void run() {
		
		try {
			
			cdrObj = vdaController.getQueryBuilderListObject(sqlQuery);
			System.out.println(" CDR Finished ----------------------- >");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	/**
	 * @return the cdrObj
	 */
	public List<Object> getCdrObj() {
		return cdrObj;
	}

}
