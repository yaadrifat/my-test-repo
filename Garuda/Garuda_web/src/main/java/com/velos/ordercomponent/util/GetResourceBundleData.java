package com.velos.ordercomponent.util;

import java.io.FileInputStream;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.velos.ordercomponent.action.VelosBaseAction;

import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GetResourceBundleData extends VelosBaseAction {
	Properties props = new Properties();
	ActionContext ac = ActionContext.getContext();
	public static final Log log=LogFactory.getLog(GetResourceBundleData.class);
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
                                
                    public GetResourceBundleData() {
                        try {
                        	
                        	@SuppressWarnings("deprecation")
							String filepath=request.getRealPath("")+"/WEB-INF/classes/ApplicationResources.properties";
                        	String filepathValidProp=request.getRealPath("")+"/WEB-INF/classes/ValidationMessages.properties";
                        	props.load(new FileInputStream(filepath));
                            props.load(new FileInputStream(filepathValidProp));
                            // log.debug(filepath);
                            
                        } 
                        catch(Exception e){
            				//log.debug("inside error");
                        	log.error(e.getMessage());
            				e.printStackTrace();
            			}
            	    	
                    }
                    @SuppressWarnings("deprecation")
					public String getImagePath(String path){
                    	String imgPath="";
                    	try{
                    		imgPath=request.getRealPath(path);
                        	props.load(new FileInputStream(imgPath));
                          	   }
                    	catch(Exception e)
                    	{
                    		log.error(e.getMessage());
                            e.printStackTrace();
                    	}
                    	return imgPath;
                    }
                    public String getPropertiesData(String propName){
                        String retProp=new String("");
                        try {
                            retProp = props.getProperty(propName);
                        } catch(Exception e) {
                        	log.error(e.getMessage());
                            e.printStackTrace();
                        }
                        return retProp;
                    }
                    
                    public String getOrderReportDate(){
                    	String dateNow = "";  
                    	try {
	                    	  //Calendar currentDate = Calendar.getInstance();
	                    	  SimpleDateFormat formatter= 
	                    	  new SimpleDateFormat("MMMM dd, yyyy 'at' hh:mm a");
	                    	  dateNow = formatter.format(new Date());
                    	  } catch(Exception e) {
                              e.printStackTrace();
                          }
                    	  
                    	  return dateNow;
                    }
					@Override
					public Object getModel() {
						// TODO Auto-generated method stub
						return null;
					}
					@Override
					public String executeAction() throws Exception {
						// TODO Auto-generated method stub
						return null;
					}
                   

}
