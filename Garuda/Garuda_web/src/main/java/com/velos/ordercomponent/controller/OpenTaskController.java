package com.velos.ordercomponent.controller;

import java.util.List;

import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.service.VelosService;
import com.velos.ordercomponent.service.impl.VelosServiceImpl;

public class OpenTaskController {
	VelosService service;

	public List<Object> queryForCbuList(CdrCbuPojo cdrCbuPojo) throws Exception {
		service = VelosServiceImpl.getService();
		return service.queryForCbuList(cdrCbuPojo);
	}

	public List getUserId(String loginId) throws Exception {
		service = VelosServiceImpl.getService();
		return service.getUserId(loginId);
	}

	public List getUser() throws Exception {
		service = VelosServiceImpl.getService();
		return service.getUser();
	}
	
	public List<Object> getTask(String loginId, String listid) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getTask(loginId, listid);
	}

}
