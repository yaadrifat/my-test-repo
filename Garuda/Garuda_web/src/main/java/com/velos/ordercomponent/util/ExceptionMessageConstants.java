package com.velos.ordercomponent.util;

public interface ExceptionMessageConstants {
    public static final String BUNDLE_NAME = "ValidationMessages";
    public static final String APP_RESOURCE_BUNDLE_NAME = "ApplicationResources";
    public static final String CUSTOMER_BUNDLE_NAME = "CustomerValidationMessages";
	public static final String REQUIRED = "garuda.exception.message.required";
	public static final String FORCORDENTRY = "garuda.exception.message.forcordentry";
	public static final String REQ_CBU_REG_ID = "garuda.cbu.cordentry.reqcburegid";
	public static final String COMMON_REQ_ALPHANUM = "garuda.common.validation.alphanumeric";
}
