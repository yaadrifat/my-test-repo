package com.velos.ordercomponent.validator;

import java.util.Calendar;
import java.util.Date;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.CompoundRoot;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

/**
 * @author Mohiuddin Ali Ahmed
 * @author Anurag Upadhyay
 * @version 1.0
 * 
 */

public class MaxTimeRangeFieldValidator extends FieldValidatorSupport {
	private int timeRange;
	private String invokingRespDateFieldName;
	private String compareTimeFieldName;
	private String compareRespDateFieldName;
	public int getTimeRange() {
		return timeRange;
	}
	public void setTimeRange(int timeRange) {
		this.timeRange = timeRange;
	}
	public String getInvokingRespDateFieldName() {
		return invokingRespDateFieldName;
	}
	public void setInvokingRespDateFieldName(String invokingRespDateFieldName) {
		this.invokingRespDateFieldName = invokingRespDateFieldName;
	}
	public String getCompareTimeFieldName() {
		return compareTimeFieldName;
	}
	public void setCompareTimeFieldName(String compareTimeFieldName) {
		this.compareTimeFieldName = compareTimeFieldName;
	}
	public String getCompareRespDateFieldName() {
		return compareRespDateFieldName;
	}
	public void setCompareRespDateFieldName(String compareRespDateFieldName) {
		this.compareRespDateFieldName = compareRespDateFieldName;
	}
	@SuppressWarnings("unchecked")
	@Override
	public void validate(Object object) throws ValidationException {
		// TODO Auto-generated method stub
		 String fieldName=getFieldName();
		 String value= (String)getFieldValue(fieldName, object);
		 
		 CompoundRoot cRoot= ActionContext.getContext().getValueStack().getRoot();
		 Date invokedRespFieldDate=(Date)getFieldValue(invokingRespDateFieldName, cRoot);
		 Date compareRespFieldDate=(Date)getFieldValue(compareRespDateFieldName, cRoot);
		 String compareFieldTime=(String)getFieldValue(compareTimeFieldName, cRoot);
		 
		 Calendar cal = Calendar.getInstance();	
		 if(compareRespFieldDate!=null && value!=null && invokedRespFieldDate!=null && ( value!=null && !value.equals("") ) &&(compareFieldTime!=null && !compareFieldTime.equals(""))&& value.matches("([0-2][0-3]:[0-5][0-9])|([0-2][0-3]:[6][0])|([0-1][0-9]:[0-5][0-9])") && compareFieldTime.matches("([0-2][0-3]:[0-5][0-9])|([0-2][0-3]:[6][0])|([0-1][0-9]:[0-5][0-9])")){
		      String [] invokFieldTimes=value.split(":");
		      String [] comFieldTimes=compareFieldTime.split(":");
		      cal.setTime(invokedRespFieldDate);
		      cal.add(Calendar.HOUR, Integer.parseInt(invokFieldTimes[0])); // adds  hour
		      cal.add(Calendar.MINUTE, Integer.parseInt(invokFieldTimes[1]));// adds  minute
		      invokedRespFieldDate=cal.getTime();		      		   
		      
		      cal.setTime(compareRespFieldDate);
		      cal.add(Calendar.HOUR, Integer.parseInt(comFieldTimes[0])); // adds  hour
		      cal.add(Calendar.MINUTE, Integer.parseInt(comFieldTimes[1]));
		     // Date initDateWithTime=compareRespFieldDate=cal.getTime();
		      cal.add(Calendar.HOUR, timeRange);// adds  minute
		      compareRespFieldDate=cal.getTime();
		      Comparable valuex=(Comparable)invokedRespFieldDate;
			  Comparable valuey =(Comparable)compareRespFieldDate;
			  if(valuex!=null && valuey!=null && valuex.compareTo(valuey) > 0){
			    	 addFieldError(getFieldName(), object);
			         return;
			     }	     
	      }	    
		
	}
}
