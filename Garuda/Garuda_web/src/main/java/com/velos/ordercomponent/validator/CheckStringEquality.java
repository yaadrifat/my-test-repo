package com.velos.ordercomponent.validator;

import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

/**
 * @author Mohiuddin Ali Ahmed
 * @author Anurag Upadhyay
 * @version 1.0
 * 
 */

public class CheckStringEquality extends FieldValidatorSupport{
	private String fieldName2="";	

	public String getFieldName2() {
		return fieldName2;
	}

	public void setFieldName2(String fieldName2) {
		this.fieldName2 = fieldName2;
	}

	@Override
	public void validate(Object object) throws ValidationException {
		
		String fieldName1= getFieldName();
		String value1=(String) getFieldValue(fieldName1, object);
		String value2=(String) getFieldValue(fieldName2, object);
		if(value1!=null && value2!=null && (!value1.equals("") && !value2.equals("")) && value1.equals(value2)){
			addFieldError(fieldName1, object);
		}
	}

}
