package com.velos.ordercomponent.controller;

import java.util.List;

import com.velos.ordercomponent.business.pojoobjects.CBUUnitReportTempPojo;
import com.velos.ordercomponent.business.pojoobjects.CordHlaExtTempPojo;
import com.velos.ordercomponent.service.VelosService;
import com.velos.ordercomponent.service.impl.VelosServiceImpl;

public class ReviewUnitReportController {
	
	VelosService service;
	
	public List<CBUUnitReportTempPojo> getCordExtendedTempInfoData(Long extInfoId) throws Exception
	{
		service = VelosServiceImpl.getService();
		return service.getCordExtendedTempInfoData(extInfoId);
	}
	public List<CordHlaExtTempPojo> getCordExtendedHlaTempData(Long hlaExtId) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCordExtendedHlaTempData(hlaExtId);
	}

	public CBUUnitReportTempPojo addReviewUnitReport(CBUUnitReportTempPojo cbuUnitReportTempPojo1) throws Exception {
		service = VelosServiceImpl.getService();
		return service.addReviewUnitReport(cbuUnitReportTempPojo1);
	}
	
	public CordHlaExtTempPojo addReviewHlaUnitReport(
			CordHlaExtTempPojo cordHlaExtPojo) {
		// TODO Auto-generated method stub
		return service.addReviewHlaUnitReport(cordHlaExtPojo);
	}
	public List<Object> getCbuAttachInfo(String cbuid) throws Exception {
	service=VelosServiceImpl.getService();
	return service.getCbuAttachInfo(cbuid);
	}

}
