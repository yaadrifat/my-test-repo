package com.velos.ordercomponent.action;

import java.util.List;

import com.velos.ordercomponent.business.pojoobjects.FundingGuidelinesPojo;
import com.velos.ordercomponent.business.pojoobjects.FundingSchedulePojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.pojoobjects.SearchPojo;
import com.velos.ordercomponent.business.pojoobjects.SitePojo;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.ordercomponent.business.pojoobjects.CBBPojo;
import com.velos.ordercomponent.business.util.VelosUtil;
import com.velos.ordercomponent.controller.CBBController;
import com.velos.ordercomponent.helper.CBUUnitReportHelper;

import com.velos.ordercomponent.action.CBUAction;
import com.velos.ordercomponent.action.CBBAction;



public class FundingReportAction extends VelosBaseAction{
	
	
	
	/**
	 * 
	 */
	public static final Log log = LogFactory.getLog(FundingReportAction.class);
	private static final long serialVersionUID = 1L;
	CBBController cbbController;
	private CBBPojo cbbPojo;
	private SitePojo site;
	private String pksite;
	private List<SitePojo> sites;
	private SearchPojo searchPojo;
	private FundingGuidelinesPojo fundingGuidelinespojo;
	private FundingSchedulePojo fundingSchedulePojo;
	private List<FundingSchedulePojo> fundingSchedulePojo1;
	private List<FundingSchedulePojo> fundingSchedulePojoLst;
	private List<FundingSchedulePojo> fundingSchedulePojoLst1;
	private List<FundingGuidelinesPojo> fundingGuidelinespojoLst;
	private List<FundingGuidelinesPojo> fundingActiveGuidelinespojoLst;
	private String editGuidlinesFlag;
	private Long cbbId;
	private List<SitePojo> cbbAvailForSchLst;
	private Date dateToGenerateJ;
	private Date cbbRegistrationDateJ;
	private String message;
	//private Long[] notificationBefore;
	private Long[] cbbforSchedule;
	private List<FundingSchedulePojo> scheduledFundingLst;
	private PaginateSearch paginateSearch;
	
	
	public FundingReportAction(){
		
		cbbController =new CBBController();
		fundingGuidelinespojo = new FundingGuidelinesPojo();
		paginateSearch = new PaginateSearch();
	}
	public CBBPojo getCbbPojo() {
		return cbbPojo;
	}

	public void setCbbPojo(CBBPojo cbbPojo) {
		this.cbbPojo = cbbPojo;
	}
	
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<FundingSchedulePojo> getFundingSchedulePojo1() {
		return fundingSchedulePojo1;
	}
	public void setFundingSchedulePojo1(
			List<FundingSchedulePojo> fundingSchedulePojo1) {
		this.fundingSchedulePojo1 = fundingSchedulePojo1;
	}
	public String getEditGuidlinesFlag() {
		return editGuidlinesFlag;
	}
	public void setEditGuidlinesFlag(String editGuidlinesFlag) {
		this.editGuidlinesFlag = editGuidlinesFlag;
	}
	public List<SitePojo> getSites() {
		return sites;
	}
	
	public SearchPojo getSearchPojo() {
		return searchPojo;
	}

	public void setSearchPojo(SearchPojo searchPojo) {
		this.searchPojo = searchPojo;
	}

	
	public void setSites(List<SitePojo> sites) {
		this.sites = sites;
	}

	public List<SitePojo> getCbbAvailForSchLst() {
		return cbbAvailForSchLst;
	}
	
	public void setCbbAvailForSchLst(List<SitePojo> cbbAvailForSchLst) {
		this.cbbAvailForSchLst = cbbAvailForSchLst;
	}
	public Long getCbbId() {
		return cbbId;
	}
	public void setCbbId(Long cbbId) {
		this.cbbId = cbbId;
	}

	public FundingGuidelinesPojo getFundingGuidelinespojo() {
		return fundingGuidelinespojo;
	}
	public void setFundingGuidelinespojo(FundingGuidelinesPojo fundingGuidelinespojo) {
		this.fundingGuidelinespojo = fundingGuidelinespojo;
	}
	public List<FundingGuidelinesPojo> getFundingGuidelinespojoLst() {
		return fundingGuidelinespojoLst;
	}
	public void setFundingGuidelinespojoLst(
			List<FundingGuidelinesPojo> fundingGuidelinespojoLst) {
		this.fundingGuidelinespojoLst = fundingGuidelinespojoLst;
	}
	public SitePojo getSite() {
		return site;
	}
	
	public void setSite(SitePojo site) {
		this.site = site;
	}

	
	public String getPksite() {
		return pksite;
	}
	public void setPksite(String pksite) {
		this.pksite = pksite;
	}
	
	public Date getDateToGenerateJ() {
		return dateToGenerateJ;
	}
	public void setDateToGenerateJ(Date dateToGenerateJ) {
		this.dateToGenerateJ = dateToGenerateJ;
	}
	public Date getCbbRegistrationDateJ() {
		return cbbRegistrationDateJ;
	}
	public void setCbbRegistrationDateJ(Date cbbRegistrationDateJ) {
		this.cbbRegistrationDateJ = cbbRegistrationDateJ;
	}
	/*public Long[] getNotificationBefore() {
		return notificationBefore;
	}
	
	public void setNotificationBefore(Long[] notificationBefore) {
		this.notificationBefore = notificationBefore;
	}*/
		
	public Long[] getCbbforSchedule() {
		return cbbforSchedule;
	}
	
	public void setCbbforSchedule(Long[] cbbforSchedule) {
		this.cbbforSchedule = cbbforSchedule;
	}
	
	public List<FundingSchedulePojo> getScheduledFundingLst() {
		return scheduledFundingLst;
	}
	
	public void setScheduledFundingLst(List<FundingSchedulePojo> scheduledFundingLst) {
		this.scheduledFundingLst = scheduledFundingLst;
	}
	
	public PaginateSearch getPaginateSearch() {
		return paginateSearch;
	}
	public void setPaginateSearch(PaginateSearch paginateSearch) {
		this.paginateSearch = paginateSearch;
	}
	
	public String getCBB() throws Exception {
		CBBAction cbb = new CBBAction();
		
		return SUCCESS;
		
	}
	
	public List<FundingGuidelinesPojo> getFundingActiveGuidelinespojoLst() {
		return fundingActiveGuidelinespojoLst;
	}
	
	public void setFundingActiveGuidelinespojoLst(
			List<FundingGuidelinesPojo> fundingActiveGuidelinespojoLst) {
		this.fundingActiveGuidelinespojoLst = fundingActiveGuidelinespojoLst;
	}
	public String fundingCBB() throws Exception {
		setLsitofCBBavailforsch();
		return "success";
	}
	
	public void setLsitofCBBavailforsch()throws Exception{
		paginationReqScopeData(paginateSearch);
		cbbAvailForSchLst=cbbController.getCBBListForSchAvail();
		setCbbAvailForSchLst(cbbAvailForSchLst);
	}
	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	DateFormat dat=new SimpleDateFormat("MMM dd,yyyy");
	DateFormat dat1=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	
	public String getCbbDetailsForFunding() throws Exception{
		//log.debug("Inside CBBDetails Funding---->");
		try{
			paginationReqScopeData(paginateSearch);
			}
		catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
			}
		if((request.getParameter("cbbId")==null ||request.getParameter("cbbId")=="") && (request.getParameter("cbbName")==null || request.getParameter("cbbName")==""))
		{
			
			List<SitePojo> cbbList = new CBBController().getSitesWithPagination(paginateSearch,1);
			int iTotalRows = cbbController.getSitesWithPagination(paginateSearch,0).size();
			paginateSearch.setiTotalRows(iTotalRows);
			setPaginateSearch(VelosUtil.getListTotalCount(paginateSearch));
			request.setAttribute("paginateSearch", paginateSearch);
			getRequest().setAttribute("CbbList", cbbList);
		}
		else{
			String criteria = constructCriteriaNew();
			List<SitePojo> cbbList = new CBBController().getSitesByCriteria(criteria,paginateSearch,1);
			int iTotalRows = new CBBController().getSitesByCriteria(criteria,paginateSearch,0).size();
			paginateSearch.setiTotalRows(iTotalRows);
			setPaginateSearch(VelosUtil.getListTotalCount(paginateSearch));
			request.setAttribute("paginateSearch", paginateSearch);
			getRequest().setAttribute("CbbList", cbbList);
		}
		setLsitofCBBavailforsch();
		return SUCCESS;
	}
	
	private String constructCriteriaNew() {
		String criteria = " ";
		
	
		if (request.getParameter("cbbId") != null && request.getParameter("cbbId")!="") {

			if (request.getParameter("cbbName") != null && request.getParameter("cbbName")!=""  ) {
				criteria += "lower(siteIdentifier) like lower('%"
						+ request.getParameter("cbbId") + "%')";
 
				criteria += " or lower(siteName) like lower('%"
						+ request.getParameter("cbbName") + "%')";
		
				
			}
			else{
				criteria += "lower(siteIdentifier) like lower('%"
					+ request.getParameter("cbbId") + "%')";
			}
			}
			else{
				if (request.getParameter("cbbName") != null && request.getParameter("cbbName")!="" ){
				
				criteria += "lower(siteName) like lower('%"
					+ request.getParameter("cbbName") + "%')";
				}
				
			}
			//log.debug(criteria);		
		return criteria;
	}
	
	public String showSelectedCBBGuidlines() throws Exception{
		if(request.getParameter("siteId")!=null && request.getParameter("siteId")!=""){
			fundingGuidelinespojoLst=cbbController.getCBBGuidlinesByid(request.getParameter("siteId"));
			if(fundingGuidelinespojoLst!=null && fundingGuidelinespojoLst.size()>0){
				setFundingGuidelinespojoLst(fundingGuidelinespojoLst);
				
			}
			if(fundingGuidelinespojoLst.size()>0 || fundingGuidelinespojoLst!=null){
				fundingActiveGuidelinespojoLst = new ArrayList<FundingGuidelinesPojo>();
				for(FundingGuidelinesPojo f : fundingGuidelinespojoLst){
					if(Long.parseLong(f.getFundingStatus().trim())== getCodeListPkByTypeAndSubtype(FUNDING_STATUS, FUNDING_STATUS_SUBTYPE_ACT)){
						fundingActiveGuidelinespojoLst.add(f);
					}
					}
				setFundingActiveGuidelinespojoLst(fundingActiveGuidelinespojoLst);
				}
			setCbbId(Long.parseLong(request.getParameter("siteId")));
		}
		setCbbId(Long.parseLong(request.getParameter("siteId")));
		setLsitofCBBavailforsch();
		setEditGuidlinesFlag("false");
		return SUCCESS;
	}
	
	public String editCBBGuidlines() throws Exception{
		if(request.getParameter("siteId")!=null && request.getParameter("siteId")!=""){
			fundingGuidelinespojoLst=cbbController.getCBBGuidlinesByid(request.getParameter("siteId"));
			if(fundingGuidelinespojoLst!=null && fundingGuidelinespojoLst.size()>0){
				setFundingGuidelinespojoLst(fundingGuidelinespojoLst);
			}
			if(fundingGuidelinespojoLst.size()>0 || fundingGuidelinespojoLst!=null){
				fundingActiveGuidelinespojoLst = new ArrayList<FundingGuidelinesPojo>();
				for(FundingGuidelinesPojo f : fundingGuidelinespojoLst){
					if(Long.parseLong(f.getFundingStatus().trim())== getCodeListPkByTypeAndSubtype(FUNDING_STATUS, FUNDING_STATUS_SUBTYPE_ACT)){
						fundingActiveGuidelinespojoLst.add(f);
					}
					}
				setFundingActiveGuidelinespojoLst(fundingActiveGuidelinespojoLst);
				}
			setCbbId(Long.parseLong(request.getParameter("siteId")));
		}
		setLsitofCBBavailforsch();
		setEditGuidlinesFlag("true");
		return SUCCESS;
	}
	
	/*public String editFundingStatus() throws Exception{
		if(request.getParameter("rowIndex")!=null && request.getParameter("rowIndex")!=""){
			if(request.getParameter("status")!=null && request.getParameter("status")!=""){
			getFundingGuidLinespojoLst().get((Integer.parseInt(request.getParameter("rowIndex")))).setFundingStatus(request.getParameter("status"));
			setFundingGuidLinespojoLst(getFundingGuidLinespojoLst());
			
				}
			}
		setEditGuidlinesFlag("true");
		return SUCCESS;
	}*/
	
	public String addCBBGuidlines(){
		try{
		if(request.getParameter("cbbId")!=null && request.getParameter("cbbId")!=""){
			fundingGuidelinespojo=new FundingGuidelinesPojo();
			fundingGuidelinespojoLst=cbbController.getCBBGuidlinesByid(request.getParameter("cbbId"));
			if(fundingGuidelinespojoLst!=null&&fundingGuidelinespojoLst.size()>0){
				Long higherKey=0L;
					for(FundingGuidelinesPojo f:fundingGuidelinespojoLst){
						if(higherKey<f.getPkFundingGuidelin())
							higherKey=f.getPkFundingGuidelin();
					}
					for(FundingGuidelinesPojo f:fundingGuidelinespojoLst){
							if(higherKey==f.getPkFundingGuidelin()){
								String fundingId=f.getFundingId();
								String[]fundingId1=new String[2];
								fundingId1[0]=fundingId.substring(0,2);
								fundingId1[1]=fundingId.substring(2);
								int fundingId2=Integer.parseInt(fundingId1[1]);
								fundingId2++;
								if(fundingId2<10){
									fundingId1[1]="00"+Integer.toString(fundingId2);
								}
								else if(fundingId2<100){
									fundingId1[1]="0"+Integer.toString(fundingId2);
								}
								else{
									fundingId1[1]=Integer.toString(fundingId2);
								}
								fundingGuidelinespojo.setFundingId(fundingId1[0]+fundingId1[1]);
							}
						}
					}
			else{
				fundingGuidelinespojo.setFundingId("FG001");
				}
			fundingGuidelinespojo.setFkCbbId(Long.parseLong(request.getParameter("cbbId")));
			setCbbId(Long.parseLong(request.getParameter("cbbId")));
			setFundingGuidelinespojo(fundingGuidelinespojo);
		}
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
			}
		return SUCCESS;
	}
	public String updateCBBGuidlines()throws Exception{
		fundingGuidelinespojoLst=getFundingGuidelinespojoLst();
		if(fundingGuidelinespojoLst.size()>0 || fundingGuidelinespojoLst!=null){
			int activeStatus=0;
			for(FundingGuidelinesPojo f : fundingGuidelinespojoLst){
					if(Long.parseLong(f.getFundingStatus().trim())== getCodeListPkByTypeAndSubtype(FUNDING_STATUS, FUNDING_STATUS_SUBTYPE_ACT)){
						activeStatus++;
							if(activeStatus>10)
							{
								setCbbId(fundingGuidelinespojoLst.get(0).getFkCbbId());
								setLsitofCBBavailforsch();
								setFundingGuidelinespojoLst(cbbController.getCBBGuidlinesByid(f.getFkCbbId().toString()));
								setEditGuidlinesFlag("true");
								setMessage("You can not add more than 10 active Guidelines");
								//setFundingActiveGuidelinespojoLst(fundingActiveGuidelinespojoLst);
								return SUCCESS;
							}
					}
				}
				
			}
		fundingGuidelinespojoLst=cbbController.updateCBBGuidlines(getFundingGuidelinespojoLst());
		if(fundingGuidelinespojoLst.size()>0 || fundingGuidelinespojoLst!=null){
			fundingActiveGuidelinespojoLst = new ArrayList<FundingGuidelinesPojo>();
			for(FundingGuidelinesPojo f : fundingGuidelinespojoLst){
				if(Long.parseLong(f.getFundingStatus().trim())== getCodeListPkByTypeAndSubtype(FUNDING_STATUS, FUNDING_STATUS_SUBTYPE_ACT)){
					fundingActiveGuidelinespojoLst.add(f);
				}
				}
			setFundingActiveGuidelinespojoLst(fundingActiveGuidelinespojoLst);
			}
		setCbbId(fundingGuidelinespojoLst.get(0).getFkCbbId());
		setLsitofCBBavailforsch();
		setEditGuidlinesFlag("false");
		return SUCCESS;
	}
	
	public String addcbbGuidelines(){
		try{
			fundingGuidelinespojoLst = new ArrayList<FundingGuidelinesPojo>();	 
			fundingGuidelinespojoLst.add(getFundingGuidelinespojo());
			fundingGuidelinespojoLst=cbbController.updateCBBGuidlines(fundingGuidelinespojoLst);
			if(fundingGuidelinespojoLst!=null && fundingGuidelinespojoLst.size()>0){
				fundingGuidelinespojo.setFkCbbId(fundingGuidelinespojoLst.get(0).getFkCbbId());
				setCbbId(fundingGuidelinespojoLst.get(0).getFkCbbId());
				setFundingGuidelinespojoLst(fundingGuidelinespojoLst);
			}
			if(fundingGuidelinespojoLst.size()>0 || fundingGuidelinespojoLst!=null){
				fundingActiveGuidelinespojoLst = new ArrayList<FundingGuidelinesPojo>();
				for(FundingGuidelinesPojo f : fundingGuidelinespojoLst){
					if(Long.parseLong(f.getFundingStatus().trim())== getCodeListPkByTypeAndSubtype(FUNDING_STATUS, FUNDING_STATUS_SUBTYPE_ACT)){
						fundingActiveGuidelinespojoLst.add(f);
					}
					}
				setFundingActiveGuidelinespojoLst(fundingActiveGuidelinespojoLst);
				}
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
			}
		return SUCCESS;
		}
	
	public String getcbbforSchedule()throws Exception{
		cbbAvailForSchLst=cbbController.getCBBListForSchAvail();
		return SUCCESS;
	}
	
	public String addFundingReportSchedule(){
		try{
			fundingSchedulePojoLst = new ArrayList<FundingSchedulePojo>();	
			if(cbbforSchedule!=null){
				if(cbbforSchedule.length>0){
						if(fundingSchedulePojo1!=null && fundingSchedulePojo1.size()>0 )
						{	
							for(Long l : cbbforSchedule){
									for(FundingSchedulePojo f: fundingSchedulePojo1 ){
										fundingSchedulePojo = new FundingSchedulePojo();
										if(f.getPkFundingSchedule()!=null){
											fundingSchedulePojo.setPkFundingSchedule(f.getPkFundingSchedule());
										}
										fundingSchedulePojo.setFkCbbId(l);
										fundingSchedulePojo.setDateToGenerate(f.getDateToGenerate());
										fundingSchedulePojo.setCbbRegistrationDate(f.getCbbRegistrationDate());
										fundingSchedulePojo.setNotificationBefore(f.getNotificationBefore());
										fundingSchedulePojo.setGeneratedStatus("0");
										fundingSchedulePojoLst.add(fundingSchedulePojo);	
									}
								}
							}
						request.setAttribute("scheduleFlag","false");
						try{
							paginationReqScopeData(paginateSearch);
							}
						catch(Exception e){
							log.error(e.getMessage());
							e.printStackTrace();
							}
						fundingSchedulePojoLst = cbbController.getScheduledFundingRepLst(fundingSchedulePojoLst,paginateSearch,1);
						int iTotalRows = cbbController.getScheduledFundingRepLst(fundingSchedulePojoLst,paginateSearch,0).size();
						paginateSearch.setiTotalRows(iTotalRows);
						setPaginateSearch(VelosUtil.getListTotalCount(paginateSearch));
						request.setAttribute("paginateSearch", paginateSearch);
						setScheduledFundingLst(fundingSchedulePojoLst);}
					
				}
			
		}
		catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public String getLstFundingReportSchedule()throws Exception{
		request.setAttribute("scheduleFlag","false");
		try{
			paginationReqScopeData(paginateSearch);
			}
		catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
			}
		fundingSchedulePojoLst = new ArrayList<FundingSchedulePojo>();
		fundingSchedulePojoLst = cbbController.getScheduledFundingRepLst(fundingSchedulePojoLst,paginateSearch,1);
		int iTotalRows = cbbController.getScheduledFundingRepLst(fundingSchedulePojoLst,paginateSearch,0).size();
		paginateSearch.setiTotalRows(iTotalRows);
		setPaginateSearch(VelosUtil.getListTotalCount(paginateSearch));
		request.setAttribute("paginateSearch", paginateSearch);
		setScheduledFundingLst(fundingSchedulePojoLst);
		return SUCCESS;
	}
	
	public String getLstScheduleFundingReport()throws Exception{
		request.setAttribute("scheduleFlag","true");
		request.setAttribute("editscheduleFlag","true");
		setLsitofCBBavailforsch();
		return SUCCESS;
	}
	
	public String getSiteInfoBySiteId(String siteColName,Long siteID) throws Exception{
		return cbbController.getSiteInfoBySiteId(siteColName,siteID);
	}
	
	public String editScheduledFundingRep()throws Exception{
		request.setAttribute("scheduleFlag","true");
		request.setAttribute("editscheduleFlag","true");
		setLsitofCBBavailforsch();
		try{
			if(request.getParameter("scheduledFundingId")!= null && request.getParameter("scheduledFundingId")!=""){
				fundingSchedulePojo1=cbbController.getfundingSchedule(Long.parseLong(request.getParameter("scheduledFundingId")));
				if(fundingSchedulePojo1!=null && fundingSchedulePojo1.size()>0){
					Long[] CbbforSchedulearr = new Long[fundingSchedulePojo1.size()];
					CbbforSchedulearr[0]=fundingSchedulePojo1.get(0).getFkCbbId();
					setCbbforSchedule(CbbforSchedulearr);
					setDateToGenerateJ(dat1.parse(fundingSchedulePojo1.get(0).getDateToGenerate().toString()));
					setCbbRegistrationDateJ(dat1.parse(fundingSchedulePojo1.get(0).getCbbRegistrationDate().toString()));
					//fundingSchedulePojo1.get(0).setDateToGenerate(dat.parse(fundingSchedulePojo1.get(0).getDateToGenerate().toString()));
					//setFundingSchedulePojo1(fundingSchedulePojo1);
				}
			}
		}
		catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
			}
		return SUCCESS;
	}
	
	
	
}
