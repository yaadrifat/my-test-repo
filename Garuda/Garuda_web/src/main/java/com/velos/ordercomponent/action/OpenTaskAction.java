	package com.velos.ordercomponent.action;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.ordercomponent.business.domain.CdrCbu;
import com.velos.ordercomponent.business.domain.CodeList;
import com.velos.ordercomponent.business.domain.TaskList;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.MailConfigPojo;
import com.velos.ordercomponent.business.pojoobjects.NotificationPojo;
import com.velos.ordercomponent.business.pojoobjects.SearchPojo;
import com.velos.ordercomponent.business.util.VelosUtil;
import com.velos.ordercomponent.controller.MailController;
import com.velos.ordercomponent.controller.OpenTaskController;
import com.velos.ordercomponent.controller.TaskListController;
import com.velos.ordercomponent.controller.VelosSearchController;
import com.velos.ordercomponent.helper.velosHelper;
import com.velos.ordercomponent.util.VelosGarudaConstants;
import com.velos.ordercomponent.util.VelosMailConstants;

public class OpenTaskAction extends VelosBaseAction {

	// Document Type
	// private final String DOCTYPE_LISTNAME = "lstDocumentType";

	private VelosSearchController velosSearchController;
	public static final Log log = LogFactory.getLog(OpenTaskAction.class);
	private List lstObject;
	private SearchPojo searchPojo;
	private Map codelstParam;
	private CodeList codeList;
	private List cordInfoList;
	private List lstUser;
	private List lstTask;
	
	private String cdrcbuid;
	private CdrCbuPojo cdrCbuPojo;
	private CdrCbu cdrCbu;
	private String codeId;
	private String pkextinfo;
	private TaskList tasklist;
	private TaskList tasklist1;
	
	private MailController mailController;
	
	private MailConfigPojo mailConfigPojo;
	private NotificationPojo notificationPojo;
	private String cbuID;
	private Boolean sentMail;
	private TaskListController taskListController;
	private OpenTaskController opentaskcontroller;
	
	
	
	public String getCodeId() {
		return codeId;
	}

	public void setCodeId(String codeId) {
		this.codeId = codeId;
	}

	public TaskList getTasklist() {
		return tasklist;
	}

	public void setTasklist(TaskList tasklist) {
		this.tasklist = tasklist;
	}

	public TaskList getTasklist1() {
		return tasklist1;
	}

	public void setTasklist1(TaskList tasklist1) {
		this.tasklist1 = tasklist1;
	}

	public String getPkextinfo() {
		return pkextinfo;
	}

	public void setPkextinfo(String pkextinfo) {
		this.pkextinfo = pkextinfo;
	}

	public MailController getMailController() {
		return mailController;
	}

	public void setMailController(MailController mailController) {
		this.mailController = mailController;
	}

	public MailConfigPojo getMailConfigPojo() {
		return mailConfigPojo;
	}

	public void setMailConfigPojo(MailConfigPojo mailConfigPojo) {
		this.mailConfigPojo = mailConfigPojo;
	}

	public NotificationPojo getNotificationPojo() {
		
		return notificationPojo;
	}

	public void setNotificationPojo(NotificationPojo notificationPojo) {
		this.notificationPojo = notificationPojo;
	}

	public String getCbuID() {
		return cbuID;
	}

	public void setCbuID(String cbuID) {
		this.cbuID = cbuID;
	}

	public Boolean getSentMail() {
		return sentMail;
	}

	public void setSentMail(Boolean sentMail) {
		this.sentMail = sentMail;
	}

	public CdrCbuPojo getCdrCbuPojo() {
		if(cdrCbuPojo==null){
			cdrCbuPojo = new CdrCbuPojo();
		}
		return cdrCbuPojo;
	}

	public void setCdrCbuPojo(CdrCbuPojo cdrCbuPojo) {
		this.cdrCbuPojo = cdrCbuPojo;
	}

	public CdrCbu getCdrCbu() {
		return cdrCbu;
	}

	public void setCdrCbu(CdrCbu cdrCbu) {
		this.cdrCbu = cdrCbu;
	}

	public String getCdrcbuid() {
		return cdrcbuid;
	}

	public void setCdrcbuid(String cdrcbuid) {
		this.cdrcbuid = cdrcbuid;
	}

	public List getLstObject() {
		return lstObject;
	}

	public void setLstObject(List lstObject) {
		this.lstObject = lstObject;
	}

	public SearchPojo getSearchPojo() {
		return searchPojo;
	}

	public void setSearchPojo(SearchPojo searchPojo) {
		this.searchPojo = searchPojo;
	}

	public Map getCodelstParam() {
		return codelstParam;
	}

	public void setCodelstParam(Map codelstParam) {
		this.codelstParam = codelstParam;
	}

	public CodeList getCodeList() {
		return codeList;
	}

	public void setCodeList(CodeList codeList) {
		this.codeList = codeList;
	}

	public List getCordInfoList() {
		return cordInfoList;
	}

	public void setCordInfoList(List cordInfoList) {
		this.cordInfoList = cordInfoList;
	}

	public List getLstUser() {
		return lstUser;
	}

	public void setLstUser(List lstUser) {
		this.lstUser = lstUser;
	}

	public List getLstTask() {
		return lstTask;
	}

	public void setLstTask(List lstTask) {
		this.lstTask = lstTask;
	}


	// constru
	public OpenTaskAction() {
		mailController = new MailController();
		velosSearchController = new VelosSearchController();
		cdrCbuPojo = new CdrCbuPojo();
		tasklist = new TaskList();
		tasklist1 = new TaskList();
		taskListController = new TaskListController();
		opentaskcontroller = new OpenTaskController();
	}

	public VelosSearchController getVelosSearchController() {
		if (velosSearchController == null) {
			velosSearchController = new VelosSearchController();
		}
		return velosSearchController;
	}

	public void setVelosSearchController(
			VelosSearchController velosSearchController) {
		this.velosSearchController = velosSearchController;
	}

	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		//log.debug("In Cbu Action :  ");
		return null;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getOpenTask() throws Exception {

		//log.debug("inside getOpenTask");

		return "taskhome";
	}

	public String searchTaskDetails() throws Exception {

	/*	searchPojo = new SearchPojo();
		searchPojo.setCodelstParam(null);
		searchPojo.setSearchType(CORD_INFO_OBJECTNAME);
		String criteria = constructCriteria();
		//log.debug("criteria"+criteria);
		searchPojo.setCriteria(criteria);
		searchPojo = velosSearchController.velosSearch(searchPojo);
		setCordInfoList(searchPojo.getResultlst());*/
		
		setCordInfoList(queryForCbuList());
		

		return "success";
	}
	
	
	private List<Object> queryForCbuList()throws Exception{
		cdrCbuPojo = getCdrCbuPojo();
		
		return opentaskcontroller.queryForCbuList(cdrCbuPojo);
		
	}

	

	
	public String taskView() throws Exception {
		setLstUser(getUser());
		String cdrcbuid = request.getParameter("cdrcbuId");
		pkextinfo = request.getParameter("pkextinfo");
		codeId = request.getParameter("codeId");
		setCodeId(codeId);
		//log.debug("cdrcbuId.1.1.->" + cdrcbuid);
		setCdrcbuid(cdrcbuid);
		setPkextinfo(pkextinfo);

		return "success";
	}

	public String showtaskView(){
		//log.debug("request.getParameter(vloginIds) :: "+ request.getParameter("loginIds"));
		String loginIds = request.getParameter("loginIds");
		Long pkextinfo =Long.parseLong(request.getParameter("pkextinfo"));
//		String values[] = request.getParameter("loginIds").split(",");
		try{
		List userList = getUserId(loginIds);
		codeId = request.getParameter("codeId");
		tasklist1.setTaskType(Long.parseLong(codeId));
		tasklist1.setTaskAssignTo(Long.parseLong(userList.get(0).toString()));
		tasklist1.setDomainId(pkextinfo);
		tasklist1=(TaskList)  new VelosUtil().setAuditableInfo( tasklist1);
		
		
		tasklist.setTaskType(Long.parseLong(codeId));
		tasklist.setTaskAssignTo(Long.parseLong(userList.get(1).toString()));
		tasklist.setDomainId(pkextinfo);
		tasklist=(TaskList)  new VelosUtil().setAuditableInfo( tasklist);
		
//		List l = linkController.saveCBUUnitTempReport(cbuUnitReporttempPojo);
		
		List l = taskListController.addTaskList(tasklist, tasklist1);
		tasklist = (TaskList)l.get(0);
		tasklist1 = (TaskList)l.get(1);
		String listid = ""+tasklist.getPkTaskId()+","+tasklist1.getPkTaskId();
		
		//log.debug("values  :: "+loginIds);
		String cdrcbuid1 = null;
		
//		tasklist.setTaskAssignTo(Long.parseLong(loginIds));
		
		
		List tlst = getTask(loginIds,listid);
		
		setLstTask(getTask(loginIds,listid));
		cdrcbuid1 = request.getParameter("cdrcbuide");
		setCdrcbuid(cdrcbuid1);
		//log.debug("cdrcbuid-->>"+cdrcbuid1);
		}
		catch(Exception e){
log.error(e.getMessage());
e.printStackTrace();
			//log.debug("Exception for -->"+e);
		}
		
/*
		NotificationPojo notification = null;
		setMailConfigPojo((MailConfigPojo)mailController.getMailConfigurationByCode(VelosMailConstants.DATA_ENTRY_CODE));
		setCbuID(cdrcbuid1);
		//System.out.print(getMailConfigPojo().getMailConfigCC()+getMailConfigPojo().getMailConfigCode()+getMailConfigPojo().getMailConfigContent());
		//log.debug("ID-->>"+getCbuID());
		//log.debug("getMailConfigPojo()"+getMailConfigPojo());

		if(getCbuID()!=null && getMailConfigPojo()!=null)
		{
			String subject = getMailConfigPojo().getMailConfigSubject();
			String content = getMailConfigPojo().getMailConfigContent();
			if(subject.indexOf(VelosMailConstants.CBUID)!=-1)
			{
				
				String cbuid = getCbuID();
				subject = subject.replace(VelosMailConstants.CBUID, cbuid);	
				String url = VelosGarudaConstants.VELOS_GARUDA_URL+VelosMailConstants.UNIT_REPORT_URL_SENT;
				System.out.print("url"+url);
				content = content.replace(VelosMailConstants.UNIT_REPORT_URL, url);
				content = content.replace(VelosMailConstants.CBUID, cbuid);
				System.out.print("content"+content);
				getMailConfigPojo().setMailConfigSubject(subject);
				getMailConfigPojo().setMailConfigContent(content);
				
			}
						
		}
		if(getMailConfigPojo()!=null)
		{
			notification = new NotificationPojo();
			notification.setNotifySubject(getMailConfigPojo().getMailConfigSubject());
			notification.setNotifyContent(getMailConfigPojo().getMailConfigContent());
			notification.setNotifyFrom(getMailConfigPojo().getMailConfigFrom());
			notification.setNotifyBCC(getMailConfigPojo().getMailConfigBCC());
			notification.setNotifyCC(getMailConfigPojo().getMailConfigCC());
			setNotificationPojo(notification);
			//System.out.print("get notification pojo"+getNotificationPojo());
		}*/
			
		/*List list=new ArrayList();
		for (int i = 0; i < values.length; i++) {
			if (!values[i].equals("")) {
				String loginids = values[i];
				Long ids = Long.parseLong(loginids);
				list.add(getTask(ids));
				//log.debug("values--->>>" + values[i]);
			}
			cdrcbuid1 = request.getParameter("cdrcbuide");
			// //log.debug("cdrcbuid-->>"+cdrcbuid1);
		}	*/

		return "success";
	}
	
	public String getMail() throws Exception
	{
		NotificationPojo notification = null;
		setMailConfigPojo((MailConfigPojo)mailController.getMailConfigurationByCode(VelosMailConstants.DATA_ENTRY_CODE));
		setCbuID(request.getParameter("cbuID"));
		//System.out.print(getMailConfigPojo().getMailConfigCC()+getMailConfigPojo().getMailConfigCode()+getMailConfigPojo().getMailConfigContent());
		//log.debug("ID-->>"+getCbuID());
		//log.debug("getMailConfigPojo()"+getMailConfigPojo());

		if(getCbuID()!=null && getMailConfigPojo()!=null)
		{
			String subject = getMailConfigPojo().getMailConfigSubject();
			String content = getMailConfigPojo().getMailConfigContent();
			if(subject.indexOf(VelosMailConstants.CBUID)!=-1)
			{
				
				String cbuid = getCbuID();
				subject = subject.replace(VelosMailConstants.CBUID, cbuid);	
				String url = VelosGarudaConstants.VELOS_GARUDA_URL+VelosMailConstants.UNIT_REPORT_URL_SENT;
				System.out.print("url"+url);
				content = content.replace(VelosMailConstants.UNIT_REPORT_URL, url);
				content = content.replace(VelosMailConstants.CBUID, cbuid);
				System.out.print("content"+content);
				getMailConfigPojo().setMailConfigSubject(subject);
				getMailConfigPojo().setMailConfigContent(content);
				getMailConfigPojo().setMailConfigTo(request.getParameter("mailid"));
				
			}
						
		}
		if(getMailConfigPojo()!=null)
		{
			notification = new NotificationPojo();
			notification.setNotifySubject(getMailConfigPojo().getMailConfigSubject());
			notification.setNotifyContent(getMailConfigPojo().getMailConfigContent());
			notification.setNotifyFrom(getMailConfigPojo().getMailConfigFrom());
			notification.setNotifyTo(getMailConfigPojo().getMailConfigTo());
			notification.setNotifyBCC(getMailConfigPojo().getMailConfigBCC());
			notification.setNotifyCC(getMailConfigPojo().getMailConfigCC());
			setNotificationPojo(notification);
			//log.debug("get notification pojo"+getNotificationPojo());
		}
		return "success";
	}

	public List getUser() throws Exception {
				
		return opentaskcontroller.getUser();
	}

	
	private List getUserId(String loginId) throws Exception{
		return opentaskcontroller.getUserId(loginId);
	}
	private List getTask(String loginId, String listid) throws Exception {
//		String query = "from TaskList where taskAssignTo in (select userId from UserDomain" +
//				" where loginId  in ("+loginId+"))"
//				+ " and domainId in (select pkCordExtInfo from CBUUnitReport)";

       
		return opentaskcontroller.getTask(loginId, listid);
	}

	/*public String sentMail() throws Exception
	{
		
		
		System.out.print("Email Sent--"+getNotificationPojo());
		try{
		setNotificationPojo(mailController.sendEmail(getNotificationPojo()));
		}catch(Exception e){
			//log.debug("Eccccc--"+e);
			e.printStackTrace();
		}
		//log.debug("b4 succ--"+mailController.sendEmail(getNotificationPojo()));
		getRequest().setAttribute("mailSent", true);
		setSentMail(true);
		//log.debug("after succ--"+sentMail);
		
		return "success";
	}*/

}
