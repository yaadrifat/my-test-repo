package com.velos.ordercomponent.validator;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.CompoundRoot;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

/**
 * @author Mohiuddin Ali Ahmed
 * @author Anurag Upadhyay
 * @version 1.0
 * 
 */

public class DateTimeRangeFieldValidator  extends FieldValidatorSupport{
	private int timeRange;
	private String invokingFieldTime;
	private String compareFieldTime;
	private String compareFieldName;
	private String min;
	private String max;
	
	public int getTimeRange() {
		return timeRange;
	}
	public void setTimeRange(int timeRange) {
		this.timeRange = timeRange;
	}
	public String getInvokingFieldTime() {
		return invokingFieldTime;
	}
	public void setInvokingFieldTime(String invokingFieldTime) {
		this.invokingFieldTime = invokingFieldTime;
	}
	public String getCompareFieldTime() {
		return compareFieldTime;
	}
	public void setCompareFieldTime(String compareFieldTime) {
		this.compareFieldTime = compareFieldTime;
	}
	public String getCompareFieldName() {
		return compareFieldName;
	}
	public void setCompareFieldName(String compareFieldName) {
		this.compareFieldName = compareFieldName;
	}
	
	public String getMin() {
		return min;
	}
	public void setMin(String min) {
		this.min = min;
	}
	public String getMax() {
		return max;
	}
	public void setMax(String max) {
		this.max = max;
	}
	@SuppressWarnings({ "unchecked" })
	@Override
	public void validate(Object object) throws  ValidationException {
		// TODO Auto-generated method stub
		 Calendar cal = Calendar.getInstance();
		 CompoundRoot cRoot= ActionContext.getContext().getValueStack().getRoot();
		 String fieldName=getFieldName();
		
		  Comparable valuex; 
	      Comparable valuey ;
	      Comparable initVal=null;		
	      
	      Date value= (Date)getFieldValue(fieldName, object);//invoking field
		  Date compareValue=(Date) getFieldValue(compareFieldName, object);
	     
	     String invokedFieldTime= getFieldValue(invokingFieldTime, cRoot).toString();
	     String comaredFieldTime=getFieldValue(compareFieldTime, cRoot).toString();
	      
	     SimpleDateFormat dateFormat = new SimpleDateFormat( "MMM dd, yyyy" );
	     
	     if(compareValue!=null && value!=null && invokedFieldTime.matches("([0-2][0-3]:[0-5][0-9])|([0-2][0-3]:[6][0])|([0-1][0-9]:[0-5][0-9])") && comaredFieldTime.matches("([0-2][0-3]:[0-5][0-9])|([0-2][0-3]:[6][0])|([0-1][0-9]:[0-5][0-9])")){
		      String [] invokFieldTimes=invokedFieldTime.split(":");
		      String [] comFieldTimes=comaredFieldTime.split(":");
		      cal.setTime(value);
		      cal.add(Calendar.HOUR_OF_DAY, Integer.parseInt(invokFieldTimes[0])); // adds  hour
		      cal.add(Calendar.MINUTE, Integer.parseInt(invokFieldTimes[1]));// adds  minute
		      value=cal.getTime();
		     
		      		   
		      
		      cal.setTime(compareValue);
		      cal.add(Calendar.HOUR_OF_DAY, Integer.parseInt(comFieldTimes[0])); // adds  hour
		      cal.add(Calendar.MINUTE, Integer.parseInt(comFieldTimes[1]));// adds  minute
		      compareValue=cal.getTime();
		      setMin(dateFormat.format(compareValue));//collection date   
	     
	      }	 
	     // add time constraints	   
	          
	          if(compareValue!=null){
	        	initVal=(Comparable)compareValue;
	            cal.setTime(compareValue);
	          }
	          if(compareValue!=null &&(fieldName.equals("prcsngStartDateStr") || fieldName.equals("frzDateStr"))){
	        	  Date dt;
	        	  Date today;
	        	  cal.setTime(compareValue);
	        	  cal.add(Calendar.HOUR, 48);//for two days
	        	  dt= cal.getTime();
	        	  today=new Date();
	        	  if(((Comparable)dt).compareTo(today)<0){
	        		  compareValue=cal.getTime();
	        	  }
	        	  cal.setTime(compareValue);
	        	  cal.add(Calendar.HOUR, 24);//for one days
	        	  dt= cal.getTime();
	        	  today=new Date();
	        	  if(((Comparable)dt).compareTo(today)<0){
	        		  compareValue=cal.getTime();
	        	  }else{
	        		cal.setTime(compareValue);
	        		cal.add(Calendar.HOUR, timeRange);
	  	            compareValue=cal.getTime();	
	        	  }
	        	  
	          }else{
	            cal.add(Calendar.HOUR, timeRange);
	            compareValue=cal.getTime();	
	          }
		        
		      setMax(dateFormat.format(compareValue));
			 
		      valuex = (Comparable)value;//freeze date
		      valuey = (Comparable)compareValue;//collection date
		
    
       if ((valuex != null) && (valuey !=null) && initVal!=null &&((initVal.compareTo(valuex)>0) || (valuey.compareTo(valuex) < 0))) {
         addFieldError(getFieldName(), object);
         return;
         }
		 
	}

}
