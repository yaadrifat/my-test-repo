package com.velos.ordercomponent.util;

import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.velos.eres.web.user.UserJB;
import com.velos.ordercomponent.business.domain.CBB;
import com.velos.ordercomponent.business.domain.CodeList;
import com.velos.ordercomponent.business.domain.Widget;
import com.velos.ordercomponent.controller.CBBController;
import com.velos.ordercomponent.controller.CBUController;
import com.velos.ordercomponent.controller.CodelstController;
import com.velos.ordercomponent.helper.velosHelper;


public class VelosSystemServices {

	public static final Log log=LogFactory.getLog(VelosSystemServices.class);
	ActionContext ac = ActionContext.getContext();
	ServletContext c = ServletActionContext.getServletContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	//ResourceBundle resource = ResourceBundle.getBundle(VelosGarudaConstants.VELOS_PROPERTIES_FILE_NAME);
	 public Long getCodeListId(String codetype,String codesubtype) throws Exception
	 {
		 CodeList codeList = new CodelstController().getCodeListByCode(codetype, codesubtype);
		 return codeList.getPkCodeId();
	 }
	 
	 public String getCodeListDescription(String codetype,String codesubtype) throws Exception
	 {
		 CodeList codeList = new CodelstController().getCodeListByCode(codetype, codesubtype);
		 return codeList.getDescription();
	 }
	 
	 public List<Widget> getAllWidgetsByPageCodeId(Long pageCode){
		 Map<Long,List<Widget>> widgetInfo = (Map<Long,List<Widget>>)c.getAttribute("widgetInfoMap");
		 List<Widget> widgets = null;
		 if(widgetInfo!=null)
		 {
			 for(Map.Entry<Long,List<Widget>> m : widgetInfo.entrySet()){
			    if(m.getKey().equals(pageCode) || m.getKey()==pageCode)
			    {
			    	widgets = m.getValue();
			    }
			}
		 }
		 return widgets;
	 }
	 
	 public Boolean isCurrentCdrUser(){
		 Boolean flag = false;
		 try{
		 UserJB user = (UserJB)request.getSession(false).getAttribute(VelosGarudaConstants.GARUDA_USER);
		 CBB cbb = new CBBController().getCBBBySiteId(Long.parseLong(user.getUserSiteId()));
			 if(cbb!=null){
				 if(cbb.getCdrUser()!=null && !cbb.getCdrUser().equals("")){
					 flag = cbb.getCdrUser();
				 }else
				 flag = false;
			 }		 	 
		 }catch(Exception e){
			 log.error(e.getMessage());
			 e.printStackTrace();
		 }
		 return flag;
	 }
	 
	 public Boolean isCurrentUserMember(){
		 Boolean flag = false;
		 try{
		 UserJB user = (UserJB)request.getSession(false).getAttribute(VelosGarudaConstants.GARUDA_USER);
		 CBB cbb = new CBBController().getCBBBySiteId(Long.parseLong(user.getUserSiteId()));
			 if(cbb!=null){
				 if(cbb.getAllowMember()!=null && cbb.getAllowMember()==1l){
					 flag=true;
				 }else
				 flag = false;
			 }		 	 
		 }catch(Exception e){
			 log.error(e.getMessage());
			 e.printStackTrace();
		 }
		 return flag;
	 } 	
	 
	 
	/* public String getValueByKey(String key)
	 {
		 return resource.getString(key);
	 }*/
}
