package com.velos.ordercomponent.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.ordercomponent.action.VelosBaseAction;
import com.velos.ordercomponent.business.domain.Attachment;
import com.velos.ordercomponent.business.pojoobjects.AttachmentPojo;
import com.velos.ordercomponent.business.pojoobjects.DcmsLogPojo;
import com.velos.ordercomponent.service.VelosService;
import com.velos.ordercomponent.service.impl.VelosServiceImpl;

public class AttachmentController {
	VelosService service;
	public static final Log log = LogFactory.getLog(AttachmentController.class);
	public AttachmentPojo addAttachments(AttachmentPojo attachmentPojo) throws Exception
	{
		service = VelosServiceImpl.getService();
		return service.addAttachments(attachmentPojo);
	}
	
	public List<Attachment> getAttachmentInfoLst(Long attachmentId) throws Exception{
		service =VelosServiceImpl.getService();
		return service.getAttachmentInfoLst(attachmentId);
	}
	
	public Object getAttachment(String objectName, String cdrcbuId, String keyName)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getObject(objectName, cdrcbuId,keyName);
	}
	
	public List<AttachmentPojo> addMultipleAttachments(List<AttachmentPojo> attachmentpojos) throws Exception
	{
		service = VelosServiceImpl.getService();
		return service.addMultipleAttachments(attachmentpojos);
	}
	public Long getDcmsAttachmentId() throws Exception{
		//log.debug("in controllerrrrrrrrrRRRRRRRRRRRRRRRRRRR");
		service = VelosServiceImpl.getService();
		return service.getDcmsAttachmentId();
	}
	
	public String getDcmsFlagMethod(String flagValue) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getDcmsFlagMethod(flagValue);
	}
	
	public DcmsLogPojo addDcmsLog(DcmsLogPojo dcmsLogPojo) throws Exception{
		service = VelosServiceImpl.getService();
		return service.addDcmsLog(dcmsLogPojo);
	}
}
