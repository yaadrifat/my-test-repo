package com.velos.ordercomponent.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.ordercomponent.business.domain.Assessment;
import com.velos.ordercomponent.business.pojoobjects.AssessmentPojo;
import com.velos.ordercomponent.business.pojoobjects.SearchPojo;
import com.velos.ordercomponent.business.pojoobjects.UserPojo;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.controller.AssessmentController;
import com.velos.ordercomponent.controller.VelosSearchController;

/**
 * @author jamarendra
 *
 */
public class AssessmentAction extends VelosBaseAction {
	
	public static final Log log = LogFactory.getLog(AssessmentAction.class);
	DateFormat dat=new SimpleDateFormat("MMM dd,yyyy");
	HttpSession session=request.getSession(true);
	private VelosSearchController velosSearchController;
	private Assessment assessment;
	private AssessmentPojo assessmentPojo;
	private AssessmentController assessmentController;
	private SearchPojo searchPojo;
	private Map<String,String> codelstParam;
	private Long pkassessment;
	private Long assessmentresponse;
	private Boolean flagforLater;
	private String assessmentRemarks;
	private String columnReference;
	private Boolean visibleTCFlag;
	private List<AssessmentPojo> assessmentList;
	private Long fkTestid;
	private Long entityId;
	private Long subEntityId;
	private Long subentitytype;
	private String shrtName;
	private String questionNo;
	private Long assessmentpostedby;
	private Date assessmentpostedon;
	private Long assessmentconsultby;
	private Date assessmentconsulton;
	private String availableDate;
	private List<UserPojo> userLst;
	private String email;
	private Boolean consultFlag;
	private Long entityType;
	private String responsestr;
	private String assessDivName;
	private String responseval;
	private String formflag;
	private Boolean textflag = false;
	private String assessmentFlagComment;
	
	public String getAssessmentFlagComment() {
		return assessmentFlagComment;
	}

	public void setAssessmentFlagComment(String assessmentFlagComment) {
		this.assessmentFlagComment = assessmentFlagComment;
	}

	public Boolean getTextflag() {
		return textflag;
	}

	public void setTextflag(Boolean textflag) {
		this.textflag = textflag;
	}

	public String getFormflag() {
		if(formflag!=null && !formflag.equals("")){
		}else{
			formflag = "false";
		}
		return formflag;
	}

	public void setFormflag(String formflag) {
		this.formflag = formflag;
	}

	public String getResponseval() {
		return responseval;
	}

	public void setResponseval(String responseval) {
		this.responseval = responseval;
	}

	public String getAssessDivName() {
		return assessDivName;
	}

	public void setAssessDivName(String assessDivName) {
		this.assessDivName = assessDivName;
	}

	
	public String getResponsestr() {
		return responsestr;
	}

	public void setResponsestr(String responsestr) {
		this.responsestr = responsestr;
	}

	public Long getEntityType() {
		return entityType;
	}

	public void setEntityType(Long entityType) {
		this.entityType = entityType;
	}

	public Boolean getFlagforLater() {
		return flagforLater;
	}

	public void setFlagforLater(Boolean flagforLater) {
		this.flagforLater = flagforLater;
	}

	public String getColumnReference() {
		return columnReference;
	}

	public void setColumnReference(String columnReference) {
		this.columnReference = columnReference;
	}

	public Boolean getVisibleTCFlag() {
		return visibleTCFlag;
	}

	public void setVisibleTCFlag(Boolean visibleTCFlag) {
		this.visibleTCFlag = visibleTCFlag;
	}

	public Boolean getConsultFlag() {
		return consultFlag;
	}

	public String getQuestionNo() {
		return questionNo;
	}

	public void setQuestionNo(String questionNo) {
		this.questionNo = questionNo;
	}

	public void setConsultFlag(Boolean consultFlag) {
		this.consultFlag = consultFlag;
	}

	public Long getPkassessment() {
		return pkassessment;
	}

	public void setPkassessment(Long pkassessment) {
		this.pkassessment = pkassessment;
	}
	public String getAvailableDate() {
		return availableDate;
	}

	public void setAvailableDate(String availableDate) {
		this.availableDate = availableDate;
	}


	public String getShrtName() {
		return shrtName;
	}

	public void setShrtName(String shrtName) {
		this.shrtName = shrtName;
	}
	
	public Long getSubEntityId() {
		return subEntityId;
	}

	public void setSubEntityId(Long subEntityId) {
		this.subEntityId = subEntityId;
	}

	public Long getSubentitytype() {
		return subentitytype;
	}

	public void setSubentitytype(Long subentitytype) {
		this.subentitytype = subentitytype;
	}
	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public Long getFkTestid() {
		return fkTestid;
	}

	public void setFkTestid(Long fkTestid) {
		this.fkTestid = fkTestid;
	}

	public List<AssessmentPojo> getAssessmentList() {
		return assessmentList;
	}

	public void setAssessmentList(List<AssessmentPojo> assessmentList) {
		this.assessmentList = assessmentList;
	}

	public AssessmentPojo getAssessmentPojo() {
		return assessmentPojo;
	}

	public void setAssessmentPojo(AssessmentPojo assessmentPojo) {
		this.assessmentPojo = assessmentPojo;
	}
	

	@Override
	public Object getModel() {
		return null;
	}

	@Override
	public String executeAction() throws Exception {
		return null;
	}
	public AssessmentAction(){
		assessment=new Assessment();
		searchPojo =new SearchPojo();
		assessmentPojo = new AssessmentPojo();
		velosSearchController = new VelosSearchController();
		assessmentController = new AssessmentController();
	}
	
	public Long getAssessmentresponse() {
		return assessmentresponse;
	}

	public void setAssessmentresponse(Long assessmentresponse) {
		this.assessmentresponse = assessmentresponse;
	}

	

	public String getAssessmentRemarks() {
		return assessmentRemarks;
	}

	public void setAssessmentRemarks(String assessmentRemarks) {
		this.assessmentRemarks = assessmentRemarks;
	}


	public Long getAssessmentpostedby() {
		return assessmentpostedby;
	}

	public void setAssessmentpostedby(Long assessmentpostedby) {
		this.assessmentpostedby = assessmentpostedby;
	}

	public Date getAssessmentpostedon() {
		return assessmentpostedon;
	}

	public void setAssessmentpostedon(Date assessmentpostedon) {
		this.assessmentpostedon = assessmentpostedon;
	}

	public Long getAssessmentconsultby() {
		return assessmentconsultby;
	}

	public void setAssessmentconsultby(Long assessmentconsultby) {
		this.assessmentconsultby = assessmentconsultby;
	}

	public Date getAssessmentconsulton() {
		return assessmentconsulton;
	}

	public void setAssessmentconsulton(Date assessmentconsulton) {
		this.assessmentconsulton = assessmentconsulton;
	}
	public Map<String,String> getCodelstParam() {
		return codelstParam;
	}

	public void setCodelstParam(Map<String,String> codelstParam) {
		this.codelstParam = codelstParam;
	}
	
	public List<UserPojo> getUserLst() {
		return userLst;
	}

	public void setUserLst(List<UserPojo> userLst) {
		this.userLst = userLst;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String saveAssessment() throws Exception{
		try{
			assessmentList = new ArrayList<AssessmentPojo>();
			if(getEntityType()!=null && !getEntityType().equals("")){
				setEntityType(getEntityType());
			}else{
				entityType= getCodeListPkByTypeAndSubtype(ENTITY_CBU_CODE_TYPE, ENTITY_CBU_CODE_SUB_TYPE);
			}
			assessmentPojo.setPkAssessment(getPkassessment());
			assessmentPojo.setEntityId(getEntityId());
			assessmentPojo.setEntityType(entityType);
			assessmentPojo.setSubEntityType(getSubentitytype());
			//log.debug("Commnet -- "+getAssessmentRemarks());
			assessmentPojo.setAssessmentRemarks(getAssessmentRemarks());
			log.debug("comment from pojo -- "+assessmentPojo.getAssessmentRemarks());
			assessmentPojo.setFlagforLater(getFlagforLater());
			assessmentPojo.setColumnReference(getColumnReference());
			setQuestionNo(getColumnReference());
			assessmentPojo.setAssessmentResp(getAssessmentresponse());
			if(getAvailableDate()!=null && !getAvailableDate().equals("")){
				assessmentPojo.setAvailableDate(dat.parse(getAvailableDate()));
			}else{
				assessmentPojo.setAvailableDate(null);
			}
			assessmentPojo.setVisibleTCFlag(getVisibleTCFlag());
			assessmentPojo.setFkTestId(getFkTestid());
			assessmentPojo.setConsultFlag(getConsultFlag());
			assessmentPojo.setAssessmentpostedby(getAssessmentpostedby());
			assessmentPojo.setAssessmentpostedon(new Date());
			assessmentPojo.setAssessmentconsultby(getAssessmentconsultby());
			assessmentPojo.setAssessmentconsulton(new Date());
			assessmentPojo.setAssessmentFlagComment(getAssessmentFlagComment());
			if(!getTextflag()){
				if(getResponseval()!=null && !getResponseval().equals("") && !getResponseval().equals("undefined")){
					assessmentPojo.setAssessmentReason(Long.parseLong(getResponseval()));
				}
			}else{
				if(getResponsestr()!=null && !getResponsestr().equals("") && !getResponsestr().equals("undefined"))
					assessmentPojo.setReasonRemarks(getResponsestr());
			}
			if(getResponseval()!=null && !getResponseval().equals("") && !getResponseval().equals("undefined")){
				if(getResponseval().contains("QqQqQ")){
					setResponseval(getResponseval().replace("QqQqQ", ""));
					//log.debug("Inside If:::::"+getResponseval());
				}
			}
			assessmentPojo.setSubEntityId(getSubEntityId());
			setAssessmentPojo(assessmentPojo);
			if(getSubEntityId()!=null && getFormflag()!=null && !getFormflag().equals("true")){
				log.debug("save Assessment method");
				setAssessmentPojo(assessmentPojo);
				assessmentController.saveOrUpdateAssessment(getAssessmentPojo());
				getAssessmentdata();
			}else{
				assessmentList.add(assessmentPojo);
				if(session!=null){
					if(session.getAttribute("aList")!=null){
						List l = (List)session.getAttribute("aList");
						if(l!=null && !l.isEmpty()){
							for(int i=0;i<l.size();i++){
								AssessmentPojo assesspojo =(AssessmentPojo) l.get(i);
								log.debug("assesspojo.getColumnReference():::"+assesspojo.getColumnReference());
								if(assesspojo.getColumnReference().toString().equals(assessmentPojo.getColumnReference().toString())){
									if(!getTextflag()){
										log.debug("assesspojo.getAssessmentReason::"+assesspojo.getAssessmentReason());
										if(!assesspojo.getAssessmentReason().equals(assessmentPojo.getAssessmentReason())){
											assessmentList.add(assesspojo);
										}
									}else{
										if(!assesspojo.getReasonRemarks().equals(assessmentPojo.getReasonRemarks())){
											assessmentList.add(assesspojo);
										}
									}
								}else{
									assessmentList.add(assesspojo);
								}
							}
							//assessmentList.addAll(l);
						}	
					}
					session.setAttribute("aList",assessmentList);
					log.debug("session value : --- "+session.getAttribute("aList"));
				}
			}
			log.debug("shnaem:::"+getShrtName());
			//userList();
		}catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return "success";
	}
	public String saveAssessmentfromsession(List patLabsPojosNew)throws Exception{
		try{
			//log.debug("save assessment from session"+session.getAttribute("aList"));
			if(session.getAttribute("aList")!=null){
				List l = (List)session.getAttribute("aList");
				if(l!=null && !l.isEmpty()){
					List<AssessmentPojo> assessList = Collections.synchronizedList(new ArrayList<AssessmentPojo>());
					assessList.addAll(l);
					//log.debug("save assessment from session");
					assessmentController.saveAssessfromsession(assessList,patLabsPojosNew);
				}	
			}
			session.setAttribute("aList", null);
			
		}catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return "success";
	}
	public String getAssessmentdata() throws Exception{
		try{
			//log.debug("form bean : entity id = "+getEntityId()+" Sub entity Id = "+getSubEntityId()+"Response Value = "+getResponseval());
			if(getSubEntityId()!=null && !getSubEntityId().equals("")){
				assessmentPojo.setEntityId(getEntityId());
				assessmentPojo.setSubEntityId(getSubEntityId());
				assessmentPojo.setColumnReference(getQuestionNo());
				if(!getTextflag()){
					if(getResponseval()!=null && !getResponseval().equals("") && !getResponseval().equals("undefined")){
						assessmentPojo.setAssessmentReason(Long.parseLong(getResponseval()));
					}
				}else{
					assessmentPojo.setReasonRemarks(getResponsestr());
				}
				if(getResponseval()!=null && !getResponseval().equals("") && !getResponseval().equals("undefined")){
					if(getResponseval().contains("QqQqQ")){
						setResponseval(getResponseval().replace("QqQqQ", ""));
						//log.debug("Inside If:::::"+getResponseval());
					}
				}
				setAssessmentPojo(assessmentPojo);
				assessment=getAssessmentObject(getAssessmentPojo());
				assessmentPojo=(AssessmentPojo)ObjectTransfer.transferObjects(assessment, assessmentPojo);
				setAssessmentPojo(assessmentPojo);
				//log.debug("response STR:"+getResponsestr());
				
				//log.debug("responseList:"+responseList.size());
			}
			//userList();
		}catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return "success";
	}
	public String getAssessmentHemo() throws Exception{
		try{
			//log.debug("form bean : entity id = "+getEntityId()+" Sub entity Id = "+getSubEntityId()+"Sub entity Id = "+getSubentitytype());
			if(getSubEntityId()!=null && !getSubEntityId().equals("")){
				assessmentPojo= new AssessmentPojo();
				assessmentPojo.setEntityId(getEntityId());
				assessmentPojo.setSubEntityId(getSubEntityId());
				if(getQuestionNo()!=null && !getQuestionNo().equals("") && !getQuestionNo().equals("undefined"))
					assessmentPojo.setColumnReference(getQuestionNo());
				if(getResponseval()!=null && !getResponseval().equals("") && !getResponseval().equals("undefined"))
					assessmentPojo.setAssessmentReason(Long.parseLong(getResponseval()));
				setAssessmentPojo(assessmentPojo);
				if( request.getParameter("assesmentCheckFlag")!=null && request.getParameter("assesmentCheckFlag").equals("true") ){
					assessment=getAssessmentObject(getAssessmentPojo());
					assessmentPojo=(AssessmentPojo)ObjectTransfer.transferObjects(assessment, assessmentPojo);
					setAssessmentPojo(assessmentPojo);
				}
			}
			//userList();
		}catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return "success";
	}
	
	public Assessment getAssessmentObject(AssessmentPojo assessmentPojo){
		try{
		    searchPojo.setSearchType(ASSESSMENT_OBJECTNAME);
		    if(assessmentPojo.getAssessmentReason()!=null && !assessmentPojo.getAssessmentReason().equals("")){
		    	searchPojo.setCriteria(" entityId ="+assessmentPojo.getEntityId()+" and subEntityId=" + assessmentPojo.getSubEntityId()+" and columnReference='"+assessmentPojo.getColumnReference()+"' and assessmentReason="+assessmentPojo.getAssessmentReason()+" order by pkAssessment desc");
		    }else if(getTextflag()){
		    	searchPojo.setCriteria(" entityId ="+assessmentPojo.getEntityId()+" and subEntityId=" + assessmentPojo.getSubEntityId()+" and columnReference='"+assessmentPojo.getColumnReference()+"' and reasonRemarks='"+assessmentPojo.getReasonRemarks().replaceAll("'", "''")+"' order by pkAssessment desc");
		    }else{
		    	searchPojo.setCriteria(" entityId ="+assessmentPojo.getEntityId()+" and subEntityId=" + assessmentPojo.getSubEntityId()+" and columnReference='"+assessmentPojo.getColumnReference()+"' order by pkAssessment desc");
		    }
	        searchPojo = velosSearchController.velosSearch(searchPojo);
		    if(searchPojo.getResultlst().size() != 0){
		        assessment = (Assessment)searchPojo.getResultlst().get(0);
		    }
	    } catch (Exception e) {
	        e.printStackTrace();
	        log.error(e.getMessage());
	    }
	    return assessment;
	}
	
}
