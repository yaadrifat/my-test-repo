package com.velos.ordercomponent.action;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;


import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import com.opensymphony.xwork2.ActionSupport;
import com.velos.ordercomponent.business.pojoobjects.CordFilterPojo;
import com.velos.ordercomponent.business.pojoobjects.HLAFilterPojo;
import com.velos.ordercomponent.report.AntigenBestHla.HLA;
import com.velos.ordercomponent.util.ReportNames;


public class ExcelCSVGenerator extends ActionSupport {
	
	private List<CordFilterPojo> cdrObjects;
	private List<Object> cdrObj;
	private Map<Long, HLAFilterPojo> bestHlasMap;
	private Map<Long, Map<Long, HLAFilterPojo>> cbuHlas;
	private Boolean isMultipleCBBSelected;
	Map<Long,Long> duplCords;
	
	public ExcelCSVGenerator(List<CordFilterPojo> cdrObjects, Map<Long, HLAFilterPojo> bestHlasMap, Map<Long, Map<Long, HLAFilterPojo>> cbuHlas, Map<Long,Long> dupId) {

			this.cdrObjects = cdrObjects;
			this.bestHlasMap = bestHlasMap;
			this.cbuHlas = cbuHlas;
			this.duplCords = dupId;
			
			
	}
	
	public ExcelCSVGenerator( List<Object> cdrObj, Map<Long,Long> dupId){
		
		this.cdrObj = cdrObj;
		this.duplCords = dupId;
		
		
	}
	
	public Workbook generateExcel(String ReportName){
		 long s = System.currentTimeMillis();
		 Workbook wb = workBook();		
		
		 
		 switch (ReportNames.valueOf(ReportName)) {
		 
		 case ANTIGEN:
			 
			 AntigenExcel  ae =	new AntigenExcel();	
			 			   ae.setIsMultipleCBBSelected(isMultipleCBBSelected);
			 			   wb = ae.antigenExcel(cdrObjects, bestHlasMap, cbuHlas, workBook());
			 
		  break;
		  
		 case ID:
			 
			 IDSummaryExcel ids = new IDSummaryExcel();
			 				ids.setIsMultipleCBBSelected(isMultipleCBBSelected);
			 				wb  = ids.idSummaryExcel(cdrObj, wb,  duplCords);
			 break;
		
		 case QUICKSUMMARY:
			 
			  QuickSummaryExcel qse = new QuickSummaryExcel();
			  					qse.setIsMultipleCBBSelected(isMultipleCBBSelected);
					try {
						wb  = qse.quickSummaryExcel(cdrObj, duplCords, wb);
					} catch (ParseException e1) {
						
						e1.printStackTrace();
					}			 
			 break;
			 
		 case LICENSELGBLERPT:
			 
			 LicensureEligibilityExcel lee = new LicensureEligibilityExcel();
			  					lee.setIsMultipleCBBSelected(isMultipleCBBSelected);
					try {
						wb  = lee.quickSummaryExcel(cdrObj, duplCords, wb);
					} catch (ParseException e1) {
						
						e1.printStackTrace();
					}			 
			 break;
			 
		 }
		 long e = System.currentTimeMillis();
		 System.out.println(" Before writing to disk : " +(e-s)/1000+" : "+(e-s)%1000+" MM:SS\n\n");
		
		 return wb; 
		
	}
	
	
	public File generateCSV(String csvFileName, String ReportName){
		
		long s = System.currentTimeMillis();
		
		Workbook wb ;
		HttpServletResponse response =null;
		File content = null;
		
		 switch (ReportNames.valueOf(ReportName)) {
		 
		 case ANTIGEN:
		 		AntigenCSV ae;
		 		ae =	new AntigenCSV();	
		 		ae.setIsMultipleCBBSelected(isMultipleCBBSelected);		 
		 		content = ae.antigenCSV(cdrObjects, bestHlasMap, cbuHlas, csvFileName);
		 		break;	
		 		
		 case ID:			 
			 IDSummaryCSV id = new IDSummaryCSV();
			 id.setIsMultipleCBBSelected(isMultipleCBBSelected);	
			 content = id.idSummaryCSV(cdrObj, csvFileName);			 
			 break;
			 
		case QUICKSUMMARY:			
			QuickSummaryCSV qs = new QuickSummaryCSV();			
					qs.setIsMultipleCBBSelected(isMultipleCBBSelected);
					content = qs.quickSummaryCSV(cdrObj, csvFileName);			
			break;
			
		case LICENSELGBLERPT:			
			LicensureEligibilityCSV le = new LicensureEligibilityCSV();			
					le.setIsMultipleCBBSelected(isMultipleCBBSelected);
					content = le.licEligCSV(cdrObj, csvFileName);			
			break;
			 
			
	
	}
		 long e = System.currentTimeMillis();
		 System.out.println(" Before writing to disk : " +(e-s)/1000+" : "+(e-s)%1000+" MM:SS\n\n");
		
		 return content;
	}
	
	private Workbook workBook(){
		
		Workbook wb = new SXSSFWorkbook(200);		
		 return wb;
	}
	/*
	private Sheet sheet(Workbook wb, String sheetName){
		
		 Sheet sheet = wb.createSheet(sheetName); 
		 
		 return sheet;
	}*/

	public Boolean getIsMultipleCBBSelected() {
		return isMultipleCBBSelected;
	}

	public void setIsMultipleCBBSelected(Boolean isMultipleCBBSelected) {
		this.isMultipleCBBSelected = isMultipleCBBSelected;
	}
	/*
	private enum REPORTNAME{
		
		ANTIGEN, ID, QUICKSUMMARY, LICENSELGBLERPT 
		
	}*/
	
}
