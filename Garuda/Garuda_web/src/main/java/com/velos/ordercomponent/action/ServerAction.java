package com.velos.ordercomponent.action;


import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import org.apache.commons.lang.StringUtils;

import com.velos.eres.web.user.UserJB;
import com.velos.ordercomponent.controller.AuditTrailController;
import com.velos.ordercomponent.controller.CBUUnitReportController;
import com.velos.ordercomponent.controller.CBBController;
import com.velos.ordercomponent.controller.CBUController;
import com.velos.ordercomponent.controller.CordImportController;
import com.velos.ordercomponent.controller.OpenOrderController;
import com.velos.ordercomponent.controller.VDAController;
import com.velos.ordercomponent.util.VelosGarudaConstants;
import com.velos.ordercomponent.business.pojoobjects.AlertsListPojo;
import com.velos.ordercomponent.business.pojoobjects.CBBProcedurePojo;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.pojoobjects.SitePojo;
import com.velos.ordercomponent.business.pojoobjects.TaskListPojo;
import com.velos.ordercomponent.business.util.Utilities;
import com.velos.ordercomponent.business.util.VelosUtil;


public class ServerAction extends VelosBaseAction {
	
   
   
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Long iDisplayStart;
	Long iDisplayLength;
	Long sEcho;
	Long iSortCol_0;
	Long iTotalRecords;
	Long iTotalDisplayRecords;
	List aaData=new ArrayList();
	String sSearch;
	String sdir;
	String sSortDir_0;
	Long pagLstSize=0l;
	Long unpagLstSize=0l;
	String pagLstSizeStr="";
	String unpagLstSizeStr="";
	int sStart1;
    int sAmount1;
	private String cbuid;
	private String inputId;
	private Long[] cordCbuEliStatus;
	private Long[] cordCbuLicStatus;
	private Long[] cordStatus;
	private Long[] workflow;
	String otherthanAll="0";
	String orderBy="";
    String condition="";
    ServerOrderBy serverOrderBy=new ServerOrderBy();
    ServerGlobalSearch sergloGlobalSearch=new ServerGlobalSearch();
    CBUAction cbuaction=new CBUAction();
    CBBAction cbbaction=new CBBAction();
    CBUController cbuController = new CBUController();
    OpenOrderAction openorderaction=new OpenOrderAction();
	OpenOrderController linkController=new OpenOrderController();
	CBBController cbbcontroller=new CBBController();
	PaginateSearch paginationSearch=new PaginateSearch();
	public List<TaskListPojo> workFlowNotiLst=new ArrayList<TaskListPojo>();
	private List<AlertsListPojo> alertNotificationsLst=new ArrayList<AlertsListPojo>();
	public List<CBBProcedurePojo> procedure=new ArrayList<CBBProcedurePojo>();
	private List<SitePojo> cbbList = null;
	AuditTrailController audittrailcontroller=new AuditTrailController();
	CBUUnitReportController cbuunitcontroller = new CBUUnitReportController();
	

	List tempLst1=new ArrayList();
	List tempLst2=new ArrayList();
	List tempLst3=new ArrayList();
	private List assignUsrLst;
	private List labelList;
	private String isSavedInProgress = "0";
	private Integer cbuCount;
	public List listCordImportHistory=new ArrayList();
	
	
	private String fprocStrDt1;
	private String fprocStrDt2;
	
	
	public String getFprocStrDt1() {
		return fprocStrDt1;
	}

	public String getFprocStrDt2() {
		return fprocStrDt2;
	}

	public void setFprocStrDt1(String fprocStrDt1) {
		this.fprocStrDt1 = fprocStrDt1;
	}
	
	public void setFprocStrDt2(String fprocStrDt2) {
		this.fprocStrDt2 = fprocStrDt2;
	}
	
	
    public String getOtherthanAll() {
		return otherthanAll;
	}
	public void setOtherthanAll(String otherthanAll) {
		this.otherthanAll = otherthanAll;
	}
	public Integer getCbuCount() {
		return cbuCount;
	}
	public void setCbuCount(Integer cbuCount) {
		this.cbuCount = cbuCount;
	}
	public String getIsSavedInProgress() {
		return isSavedInProgress;
	}
	public void setIsSavedInProgress(String isSavedInProgress) {
		this.isSavedInProgress = isSavedInProgress;
	}
	
	public List<CBBProcedurePojo> getProcedure() {
		return procedure;
	}
	public void setProcedure(List<CBBProcedurePojo> procedure) {
		this.procedure = procedure;
	}
	
	public List<SitePojo> getCbbList() {
		return cbbList;
	}

	public void setCbbList(List<SitePojo> cbbList) {
		this.cbbList = cbbList;
	}
	
	public String getSdir() {
 		return sdir;
 	}
 	public void setSdir(String sdir) {
 		this.sdir = sdir;
 	}
    public int getsStart1() {
 		return sStart1;
 	}
 	public int getsAmount1() {
 		return sAmount1;
 	}
 	
    public String getsSortDir_0() {
		return sSortDir_0;
	}
	public void setsSortDir_0(String sSortDir_0) {
		this.sSortDir_0 = sSortDir_0;
	}
	public Long[] getWorkflow() {
		return workflow;
	}
	public void setWorkflow(Long[] workflow) {
		this.workflow = workflow;
	}
	public Long[] getCordStatus() {
		return cordStatus;
	}
	public void setCordStatus(Long[] cordStatus) {
		this.cordStatus = cordStatus;
	}
	public Long[] getCordCbuEliStatus() {
		return cordCbuEliStatus;
	}
	public void setCordCbuEliStatus(Long[] cordCbuEliStatus) {
		this.cordCbuEliStatus = cordCbuEliStatus;
	}
	public Long[] getCordCbuLicStatus() {
		return cordCbuLicStatus;
	}
	public void setCordCbuLicStatus(Long[] cordCbuLicStatus) {
		this.cordCbuLicStatus = cordCbuLicStatus;
	}
	public String getInputId() {
		return inputId;
	}
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	public String getCbuid() {
		return cbuid;
	}
	public void setCbuid(String cbuid) {
		this.cbuid = cbuid;
	}
 	public void setsStart1(int sStart1) {
 		this.sStart1 = sStart1;
 	}
 	public void setsAmount1(int sAmount1) {
 		this.sAmount1 = sAmount1;
 	}
	
	public String getsSearch() {
		return sSearch;
	}
	public void setsSearch(String sSearch) {
		this.sSearch = sSearch;
	}
	public Long getiDisplayStart() {
		return iDisplayStart;
	}
	public Long getiDisplayLength() {
		return iDisplayLength;
	}
	public Long getsEcho() {
		return sEcho;
	}
	public Long getiSortCol_0() {
		return iSortCol_0;
	}
	public Long getiTotalRecords() {
		return iTotalRecords;
	}
	public Long getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}
	public void setiDisplayStart(Long iDisplayStart) {
		this.iDisplayStart = iDisplayStart;
	}
	public void setiDisplayLength(Long iDisplayLength) {
		this.iDisplayLength = iDisplayLength;
	}
	public void setsEcho(Long sEcho) {
		this.sEcho = sEcho;
	}
	public void setiSortCol_0(Long iSortCol_0) {
		this.iSortCol_0 = iSortCol_0;
	}
	public void setiTotalRecords(Long iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}
	public void setiTotalDisplayRecords(Long iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}
	
	public List getAaData() {
		return aaData;
	}
	public void setAaData(List aaData) {
		this.aaData = aaData;
	}
	public List<TaskListPojo> getWorkFlowNotiLst() {
		if(workFlowNotiLst==null){
			workFlowNotiLst = new ArrayList<TaskListPojo>();
		}
		return workFlowNotiLst;
	}

	public void setWorkFlowNotiLst(List<TaskListPojo> workFlowNotiLst) {
		this.workFlowNotiLst = workFlowNotiLst;
	}
	public List<AlertsListPojo> getAlertNotificationsLst() {
		return alertNotificationsLst;
	}

	public void setAlertNotificationsLst(List<AlertsListPojo> alertNotificationsLst) {
		this.alertNotificationsLst = alertNotificationsLst;
	}
   
	public List getAssignUsrLst() {
		return assignUsrLst;
	}
	public List getLabelList() {
		return labelList;
	}
	public void setAssignUsrLst(List assignUsrLst) {
		this.assignUsrLst = assignUsrLst;
	}
	public void setLabelList(List labelList) {
		this.labelList = labelList;
	}
	public List getListCordImportHistory() {
		return listCordImportHistory;
	}

	public void setListCordImportHistory(
			List listCordImportHistory) {
		this.listCordImportHistory = listCordImportHistory;
	}
	
    public String FetchResultSet(){          //getJsonPagination action called  ......y execute again ...result already rendered
    	//System.out.println("Inside correct action................."+request);        
    	try{
    		Long sStart = 0l;
    		Long sAmount=0l;
    		Long sEcho1 =0l;
    		int sCol = 0;
    		String sdir = "";
    		String searchTxt = "";
    		String findTab = "";
    		if(request.getParameter("iDisplayStart")!=null && !request.getParameter("iDisplayStart").equals("") && !request.getParameter("iDisplayStart").equals("undefined")){
    	        sStart = Long.parseLong(request.getParameter("iDisplayStart").toString());
    		}
    		if(request.getParameter("iDisplayLength")!=null && !request.getParameter("iDisplayLength").equals("") && !request.getParameter("iDisplayLength").equals("undefined")){
    	        sAmount =Long.parseLong(request.getParameter("iDisplayLength").toString());
    		}
    		if(request.getParameter("sEcho")!=null && !request.getParameter("sEcho").equals("") && !request.getParameter("sEcho").equals("undefined")){
    	        sEcho1 = Long.parseLong(request.getParameter("sEcho").toString());
    		}
    		if(request.getParameter("iSortCol_0")!=null && !request.getParameter("iSortCol_0").equals("") && !request.getParameter("iSortCol_0").equals("undefined")){
    	        sCol = Integer.parseInt(request.getParameter("iSortCol_0").toString());
    		}
    		if(request.getParameter("sSortDir_0")!=null && !request.getParameter("sSortDir_0").equals("") && !request.getParameter("sSortDir_0").equals("undefined")){
    	        sdir = request.getParameter("sSortDir_0").toString();
    		}
    		if(request.getParameter("sSearch")!=null && !request.getParameter("sSearch").equals("") && !request.getParameter("sSearch").equals("undefined")){
    	        searchTxt = request.getParameter("sSearch").toString().trim();
    		}
    		if(request.getParameter("iDisplayStart")!=null && !request.getParameter("iDisplayStart").equals("") && !request.getParameter("iDisplayStart").equals("undefined")){
    	        sStart1 = Integer.parseInt(request.getParameter("iDisplayStart").toString());
    		}
    		if(request.getParameter("iDisplayLength")!=null && !request.getParameter("iDisplayLength").equals("") && !request.getParameter("iDisplayLength").equals("undefined")){
    	        sAmount1 =Integer.parseInt(request.getParameter("iDisplayLength").toString());
    		}
    		if(request.getParameter("TblFind")!=null && !request.getParameter("TblFind").equals("") && !request.getParameter("TblFind").equals("undefined")){
    	        findTab = request.getParameter("TblFind").toString();
    		}
    	        
    	        setsStart1(sStart1);
    	        setsAmount1(sAmount1);
    	        setiDisplayStart(sStart);
    	        setsEcho(sEcho1);
    	        setSdir(sdir);
    	        
    	        //System.out.println("\n\n\n\n\n\n\n\nsStart::"+sStart+"::sAmount::"+sAmount+":"+":sCol:"+sCol+":sdir:"+sdir+"\n\n\n");
    	        if(findTab.equals("historytbl")){
    	        	setHistoryData();
    	        }
    	        if(findTab.equals("lporders")){
    	        	setOrdersLp();
    	        }
    	        if(findTab.equals("lpworkflow")){
    	        	setWorkflowLp();
    	        }
    	        if(findTab.equals("lpalerts")){
    	        	setAlertsLp();
    	        }
    	        if(findTab.equals("psOrders")){
    	        	setOrdersPs();
    	        }
    	        if(findTab.equals("psNotavail")){
    	        	setNotAvailPs();
    	        }
    	        if(findTab.equals("psSavInProg")){
    	        	setSavInProgPs();
    	        }
    	        if(findTab.equals("notAvailCBUs")){
    	        	setNotAvailCBUs();
    	        }
    	        if(findTab.equals("lookup")){
    	        	setLookupData();
    	        }
    	        if(findTab.equals("cordImportHist")){
    	        	setCordImportData();
    	        }
    	        if(findTab.equals("pProcedure")){
    	        	setProcedureData();
    	        }
    	        if(findTab.equals("cbbProfile")){
    	        	setCBBProfieData();
    	        }

    	        if(findTab.equals("cbuInfo")){
    	        	
    	        	cbuInformationAuditData(VelosGarudaConstants.CORD_ROW_LEVEL,1);
    	        }
    	        
    	        if(findTab.equals("idInfo")){
    	        	setCbuInfoAuditData(VelosGarudaConstants.ID_ROW_LEVEL,2);
    	        }
    	        
    	        if(findTab.equals("procInfo")){
    	        	setCbuInfoAuditData(VelosGarudaConstants.PROC_INFO_ROW_LEVEL,3);
    	        }
    	        
    	        if(findTab.equals("licInfo")){
    	        	setCbuInfoAuditData(VelosGarudaConstants.ENTITY_STATUS_ROW_LEVEL,4);
    	        }

    	        if(findTab.equals("eligInfo")){
    	        	setCbuInfoAuditData(VelosGarudaConstants.MULTIPLE_VALUE_ROW_LEVEL,5);
    	        }
    	        
    	        if(findTab.equals("revInfo")){
    	        	setCbuInfoAuditData(VelosGarudaConstants.REVIEW_ROW_LEVEL,6);
    	        }
    	        
    	        if(findTab.equals("hlaInfo")){
    	        	setCbuInfoAuditData(VelosGarudaConstants.HLA_ROW_LEVEL,7);
    	        }
    	        
    	        if(findTab.equals("labInfo")){
    	        	setCbuInfoAuditData(VelosGarudaConstants.LABSUMMARY_ROW_LEVEL,8);
    	        }
    	        
    	        if(findTab.equals("cnInfo")){
    	        	setCbuInfoAuditData(VelosGarudaConstants.NOTES_ROW_LEVEL,9);
    	        }
    	        if(findTab.equals("mcInfo")){
    	        	setCbuInfoAuditData(VelosGarudaConstants.MIN_CRIT_INFO_ROW_LEVEL,10);
    	        }
    	        if(findTab.equals("orderInfo")){
    	        	setCbuInfoAuditData(VelosGarudaConstants.PF_ROW_LEVEL,11);
    	        }
    	        
    	        if(findTab.equals("idmInfo")){
    	        	setFormInfoAuditData(VelosGarudaConstants.FORMS_ROW_LEVEL_IDM,12);
    	        }
    	        
    	        if(findTab.equals("hhsInfo")){
    	        	setFormInfoAuditData(VelosGarudaConstants.FORMS_ROW_LEVEL_HHS,13);
    	        }
    	        
    	        if(findTab.equals("pathlaInfo")){
    	        	setCbuInfoAuditData(VelosGarudaConstants.PATHLA_ROW_LEVEL,12);
    	        }
    	        
    	        if(findTab.equals("docInfo")){
    	        	setDocInfoAuditData();
    	        }
    	        
    	    } catch (Exception e) {
    	         e.printStackTrace();
    	    }
    	    return "SUCCESS";
    }
    public void setCBBProfieData(){
    	
    	int i;
    	
    	//System.out.println("Inside setProcedureData");
    	
    	if(request.getParameter("iSortCol_0")!=null && !request.getParameter("iSortCol_0").toString().equals("") &&  !request.getParameter("iSortCol_0").toString().equals("0")){
        	orderBy=serverOrderBy.FetchCBBProfile_OrderBy(request.getParameter("iSortCol_0").toString(),getSdir());
        }
    	
    	if(request.getParameter("sSearch")!=null && !request.getParameter("sSearch").toString().trim().equals("")){
        	condition=sergloGlobalSearch.FetchGlobalSearchCBBProfile(request.getParameter("sSearch").toString());
        }
    	
    	
    	/*******************Getting Paginated Whole List***********************/
    	i=0;
    	cbbList=FetchCBBProfileData(condition,orderBy,i,sStart1,sAmount1);
    	
    	//System.out.println("tempList1.size.."+tempLst1.size());
    	
    	/**********************************************************************/
    	
    	
    	if(request.getParameter("otherThanAll")!=null && request.getParameter("otherThanAll").equals("1")){
    		setOtherthanAll(request.getParameter("otherThanAll").toString());
    	}
    	
    	
    	if(!getOtherthanAll().equals("1")){
		    	
    			/*******************Getting Paginated List Size***********************/
		    	i=1;
		    	tempLst3=FetchCBBProfileData(condition,orderBy,i,sStart1,sAmount1);
		    	//BigDecimal totCount1 = (BigDecimal)tempLst3.get(0);
		    	pagLstSizeStr = tempLst3.get(0).toString();
		    	pagLstSize=Long.parseLong(pagLstSizeStr);
		    	//System.out.println("\n\n\n\n\ntempList3.size..Procedure Table:"+tempLst3.size()+"size1..."+pagLstSize);
		    	/**********************************************************************/
		    	
		    	
		    	/*******************Getting Whole List Size without pagination***********************/
		    	i=2;
		    	condition="";
		    	tempLst2=FetchCBBProfileData(condition,orderBy,i,sStart1,sAmount1);
		    	//BigDecimal totCount2 = (BigDecimal)tempLst2.get(0);
		    	unpagLstSizeStr=tempLst2.get(0).toString();
		    	unpagLstSize=Long.parseLong(unpagLstSizeStr);
		    	//System.out.println("tempList2.size..Procedure Table:"+tempLst2.size()+"size2..."+unpagLstSize);
		    	/************************************************************************************/
		    	
		    	
		    	setCbbList(cbbList);
		        setiTotalRecords(unpagLstSize);
				setiTotalDisplayRecords(pagLstSize);
		
    	}
		
        if(getOtherthanAll().equals("1")){
    		
    		/*************************Execute Only for Other than All Option******************/
        	setCbbList(cbbList);
    		setiTotalRecords(Long.parseLong(request.getParameter("iShowRows").toString()));
    		setiTotalDisplayRecords(Long.parseLong(request.getParameter("iShowRows").toString()));
    	}
    	
    } 
    public void setHistoryData(){
    	
    	int i;
    	
    	//System.out.println("Inside setProcedureData");
    	
    	if(request.getParameter("iSortCol_0")!=null && !request.getParameter("iSortCol_0").toString().equals("") &&  !request.getParameter("iSortCol_0").toString().equals("0")){
        	orderBy=serverOrderBy.FetchHistory_OrderBy(request.getParameter("iSortCol_0").toString(),getSdir());
        }
    	
    	if(request.getParameter("sSearch")!=null && !request.getParameter("sSearch").toString().trim().equals("")){
        	condition=sergloGlobalSearch.FetchGlobalSearchHistory(request.getParameter("sSearch").toString());
        }
    	
    	
    	/*******************Getting Paginated Whole List***********************/
    	i=0;
    	tempLst1=FetchHistoryData(condition,orderBy,i,sStart1,sAmount1);
    	
    	//System.out.println("tempList1.size.."+tempLst1.size());
    	
    	/**********************************************************************/
    	
    	
    	if(request.getParameter("otherThanAll")!=null && request.getParameter("otherThanAll").equals("1")){
    		setOtherthanAll(request.getParameter("otherThanAll").toString());
    	}
    	
    	
    	if(!getOtherthanAll().equals("1")){
		    	
    			/*******************Getting Paginated List Size***********************/
		    	i=1;
		    	tempLst3=FetchHistoryData(condition,orderBy,i,sStart1,sAmount1);
		    	//BigDecimal totCount1 = (BigDecimal)tempLst3.get(0);
		    	pagLstSizeStr = tempLst3.get(0).toString();
		    	pagLstSize=Long.parseLong(pagLstSizeStr);
		    	//System.out.println("\n\n\n\n\ntempList3.size..Procedure Table:"+tempLst3.size()+"size1..."+pagLstSize);
		    	/**********************************************************************/
		    	
		    	
		    	/*******************Getting Whole List Size without pagination***********************/
		    	i=2;
		    	condition="";
		    	tempLst2=FetchHistoryData(condition,orderBy,i,sStart1,sAmount1);
		    	//BigDecimal totCount2 = (BigDecimal)tempLst2.get(0);
		    	unpagLstSizeStr=tempLst2.get(0).toString();
		    	unpagLstSize=Long.parseLong(unpagLstSizeStr);
		    	//System.out.println("tempList2.size..Procedure Table:"+tempLst2.size()+"size2..."+unpagLstSize);
		    	/************************************************************************************/
		    	
		    	
		    	setAaData(tempLst1);
		        setiTotalRecords(unpagLstSize);
				setiTotalDisplayRecords(pagLstSize);
		
    	}
		
        if(getOtherthanAll().equals("1")){
    		
    		/*************************Execute Only for Other than All Option******************/
        	setAaData(tempLst1);
    		setiTotalRecords(Long.parseLong(request.getParameter("iShowRows").toString()));
    		setiTotalDisplayRecords(Long.parseLong(request.getParameter("iShowRows").toString()));
    	}
    	
    } 
    public String getDocInfoAuditCondition(){
		String condition="";
		String dropDownNames[]={"fromDocDt","toDocDt","doccat","docsubcat","docusrid","docact"};
		String columnNames[]={"timestamp","timestamp","category","subcategory","user_id","action"};
		for(int i=0;i<dropDownNames.length;i++){
			if(request.getParameter(dropDownNames[i])!=null && !request.getParameter(dropDownNames[i]).equals("") && !request.getParameter(dropDownNames[i]).equals("undefined")){
				if(i==0){
					String val1=request.getParameter(dropDownNames[0]).toString();
					String val2=request.getParameter(dropDownNames[1]).toString();
					if(val1.equals(val2)){
						condition=condition+" to_char("+columnNames[i]+",'DD-Mon-YYYY')='"+request.getParameter(dropDownNames[0])+"'";
					}else{
						condition=condition+"  (trunc("+columnNames[i]+") between to_date('"+request.getParameter(dropDownNames[0])+"','DD-Mon-YYYY') and to_date('"+request.getParameter(dropDownNames[1])+"','DD-Mon-YYYY'))";
					}
				}
				if(i==0){
					setFprocStrDt1(Utilities.getOrdDateFormatDate(request.getParameter(dropDownNames[0]).toString()));
					setFprocStrDt2(Utilities.getOrdDateFormatDate(request.getParameter(dropDownNames[1]).toString()));
				}
				if (i==2) {
					if (condition != "")
						condition = condition + " and ";
					String categ = request.getParameter(dropDownNames[i]);
					condition = condition + columnNames[i] +" = '" + categ +"' ";
				}
				
				if (i==3) {
					if (condition != "")
						condition = condition + " and ";
					String subcat = request.getParameter(dropDownNames[i]);
					condition = condition +  columnNames[i] +" = '" + subcat+"' ";
				}
	
				
				if (i==4) {
					if (condition != "")
						condition = condition + " and ";
					String userId = request.getParameter(dropDownNames[i]);
					condition = condition + columnNames[i] +" = '" + userId+"' ";
				}
				
				
				if (i==5) {
					if (condition != "")
						condition = condition + " and ";
					String action = request.getParameter(dropDownNames[i]);
					condition = condition +  columnNames[i] +" = '" + action+"' ";
				}
			}
		}
		return condition;
	}		

    
    public String getFormInfoAuditCondition(int k){
		String condition="";
		String[] dropDownNames = new String[5];
		dropDownNames [0] = "fromDt"+k;
		dropDownNames [1] = "toDt"+k;
		dropDownNames [2] = "formname"+k;
		dropDownNames [3] = "fversion"+k;
		dropDownNames [4] = "fuserid"+k;
		
		String columnNames[]={"created_on","created_on","formname","fversion","user_id"};

		for(int i=0;i<dropDownNames.length;i++){
			if(request.getParameter(dropDownNames[i])!=null && !request.getParameter(dropDownNames[i]).equals("") && !request.getParameter(dropDownNames[i]).equals("undefined")){
				if(i==0){
					String val1=request.getParameter(dropDownNames[0]).toString();
					String val2=request.getParameter(dropDownNames[1]).toString();
					if(val1.equals(val2)){
						condition=condition+" to_char("+columnNames[i]+",'DD-Mon-YYYY')='"+request.getParameter(dropDownNames[0])+"'";
					}else{
						condition=condition+"  (trunc("+columnNames[i]+") between to_date('"+request.getParameter(dropDownNames[0])+"','DD-Mon-YYYY') and to_date('"+request.getParameter(dropDownNames[1])+"','DD-Mon-YYYY'))";
					}
				}
				if(i==0){
					setFprocStrDt1(Utilities.getOrdDateFormatDate(request.getParameter(dropDownNames[0]).toString()));
					setFprocStrDt2(Utilities.getOrdDateFormatDate(request.getParameter(dropDownNames[1]).toString()));
				}
				
				if (i==2) {
					if (condition != "")
						condition = condition + " and ";
					String fName = request.getParameter(dropDownNames[i]);
					condition = condition + columnNames[i] +" = '" + fName +"' ";
				}
				
				if (i==3) {
					if (condition != "")
						condition = condition + " and ";
					String userId = request.getParameter(dropDownNames[i]);
					condition = condition + columnNames[i] +" = '" + userId+"' ";
				}
				
				if (i==4) {
					if (condition != "")
						condition = condition + " and ";
					String action = request.getParameter(dropDownNames[i]);
					condition = condition +  columnNames[i] +" = '" + action+"' ";
				}
			}
		}
		return condition;
	}	
    
    
    
    public String getCbuInfoAuditCondition(int k){
		String condition="";
		String[] dropDownNames = new String[7];
		dropDownNames [0] = "fromDt"+k;
		dropDownNames [1] = "toDt"+k;
		dropDownNames [2] = "fieldName"+k;
		dropDownNames [3] = "userId"+k;
		dropDownNames [4] = "actTaken"+k;
		dropDownNames [5] = "prevValue"+k;
		dropDownNames [6] = "newValue"+k;
		
		String columnNames[]={"timestamp","timestamp","column_display_name","user_id","action","old_value","new_value"};

		for(int i=0;i<dropDownNames.length;i++){
			if(request.getParameter(dropDownNames[i])!=null && !request.getParameter(dropDownNames[i]).equals("") && !request.getParameter(dropDownNames[i]).equals("undefined")){
				if(i==0){
					String val1=request.getParameter(dropDownNames[0]).toString();
					String val2=request.getParameter(dropDownNames[1]).toString();
					if(val1.equals(val2)){
						condition=condition+" to_char("+columnNames[i]+",'DD-Mon-YYYY')='"+request.getParameter(dropDownNames[0])+"'";
					}else{
						condition=condition+"  (trunc("+columnNames[i]+") between to_date('"+request.getParameter(dropDownNames[0])+"','DD-Mon-YYYY') and to_date('"+request.getParameter(dropDownNames[1])+"','DD-Mon-YYYY'))";
					}
				}
				if(i==0){
					setFprocStrDt1(Utilities.getOrdDateFormatDate(request.getParameter(dropDownNames[0]).toString()));
					setFprocStrDt2(Utilities.getOrdDateFormatDate(request.getParameter(dropDownNames[1]).toString()));
				}
				
				if (i==2) {
					if (condition != "")
						condition = condition + " and ";
					String fName = request.getParameter(dropDownNames[i]);
					condition = condition + columnNames[i] +" = '" + fName +"' ";
				}
				
				if (i==3) {
					if (condition != "")
						condition = condition + " and ";
					String userId = request.getParameter(dropDownNames[i]);
					condition = condition + columnNames[i] +" = '" + userId+"' ";
				}
				
				if (i==4) {
					if (condition != "")
						condition = condition + " and ";
					String action = request.getParameter(dropDownNames[i]);
					condition = condition +  columnNames[i] +" = '" + action+"' ";
				}
				
				if (i==5) {
					if (condition != "")
						condition = condition + " and ";
					String prevVal = request.getParameter(dropDownNames[i]);
					condition = condition +  columnNames[i] +" = '" + prevVal+"' ";
				}

				if (i==6) {
					if (condition != "")
						condition = condition + " and ";
					String newVal = request.getParameter(dropDownNames[i]);
					condition = condition +  columnNames[i] +" = '" + newVal+"' ";
				}
				
			}
		}
		return condition;
	}
    
    public void setDocInfoAuditData() {
		int i;
		if (request.getParameter("iSortCol_0") != null
				&& !request.getParameter("iSortCol_0").toString().equals("")) {
			orderBy = serverOrderBy.fetchDocInfoAudit_OrderBy(request
					.getParameter("iSortCol_0").toString(), getSdir());
		}
		if (request.getParameter("sSearch") != null
				&& !request.getParameter("sSearch").toString().trim()
						.equals("")) {
			condition = sergloGlobalSearch.fetchGlobalSearch_docInfo(request
					.getParameter("sSearch").toString());
		}
		
		if (condition != "" ) { 
			condition = " where "+ condition; 
			if (getDocInfoAuditCondition() != "")
				condition = condition + " and "+ getDocInfoAuditCondition();
		}
		else
		{
			if (getDocInfoAuditCondition() != "")
				condition = " where " + getDocInfoAuditCondition();
		}	

		/******************* Getting Paginated Whole List ***********************/
		
		i = 0;
		aaData = FetchDocInfoAudit(condition, orderBy, i, sStart1, sAmount1);
		/**********************************************************************/

		if (request.getParameter("otherThanAll") != null
				&& request.getParameter("otherThanAll").equals("1")) {
			setOtherthanAll(request.getParameter("otherThanAll").toString());
		}

		if (!getOtherthanAll().equals("1")) {

			/******************* Getting Paginated List Size ***********************/
			i = 1;
			tempLst3 = FetchDocInfoAudit(condition, orderBy, i, sStart1,
					sAmount1);
			pagLstSizeStr = tempLst3.get(0).toString();
			pagLstSize = Long.parseLong(pagLstSizeStr);
			/******************* Getting Whole List Size without pagination ***********************/
			i = 2;
			condition = "";
			tempLst2 = FetchDocInfoAudit(condition, orderBy, i, sStart1,
					sAmount1);
			unpagLstSizeStr = tempLst2.get(0).toString();
			unpagLstSize = Long.parseLong(unpagLstSizeStr);
			setAaData(aaData);
			setiTotalRecords(unpagLstSize);
			setiTotalDisplayRecords(pagLstSize);
		}
		if (getOtherthanAll().equals("1")) {

			/************************* Execute Only for Other than All Option ******************/
			setAaData(aaData);
			setiTotalRecords(Long.parseLong(request.getParameter("iShowRows")
					.toString()));
			setiTotalDisplayRecords(Long.parseLong(request.getParameter(
					"iShowRows").toString()));
		}
	}
    
    
    
    
    public void setFormInfoAuditData(String whichWidget, int j) {
		int i;
		if (request.getParameter("iSortCol_0") != null
				&& !request.getParameter("iSortCol_0").toString().equals("")) {
			orderBy = serverOrderBy.fetchFormInfoAudit_OrderBy(request
					.getParameter("iSortCol_0").toString(), getSdir());
		}
		if (request.getParameter("sSearch") != null
				&& !request.getParameter("sSearch").toString().trim()
						.equals("")) {
			condition = sergloGlobalSearch.fetchGlobalSearch_formInfo(request
					.getParameter("sSearch").toString());
		}
		
		if (condition != "" ) { 
			condition = " where "+ condition; 
			if (getFormInfoAuditCondition(j) != "")
				condition = condition + " and "+ getFormInfoAuditCondition(j);
		}
		else
		{
			if (getFormInfoAuditCondition(j) != "")
				condition = " where " + getFormInfoAuditCondition(j);
		}	
		/******************* Getting Paginated Whole List ***********************/

		i = 0;
		aaData = FetchCbuInfoAudit(condition, orderBy, i, sStart1, sAmount1, whichWidget);


		/**********************************************************************/

		if (request.getParameter("otherThanAll") != null
				&& request.getParameter("otherThanAll").equals("1")) {
			setOtherthanAll(request.getParameter("otherThanAll").toString());
		}

		if (!getOtherthanAll().equals("1")) {

			/******************* Getting Paginated List Size ***********************/
			i = 1;
			tempLst3 = FetchCbuInfoAudit(condition, orderBy, i, sStart1,
					sAmount1,whichWidget);
			pagLstSizeStr = tempLst3.get(0).toString();
			pagLstSize = Long.parseLong(pagLstSizeStr);
			/******************* Getting Whole List Size without pagination ***********************/
			i = 2;
			condition = "";
			tempLst2 = FetchCbuInfoAudit(condition, orderBy, i, sStart1,
					sAmount1,whichWidget);
			unpagLstSizeStr = tempLst2.get(0).toString();
			unpagLstSize = Long.parseLong(unpagLstSizeStr);
			setAaData(aaData);
			setiTotalRecords(unpagLstSize);
			setiTotalDisplayRecords(pagLstSize);
		}
		if (getOtherthanAll().equals("1")) {

			/************************* Execute Only for Other than All Option ******************/
			setAaData(aaData);
			setiTotalRecords(Long.parseLong(request.getParameter("iShowRows")
					.toString()));
			setiTotalDisplayRecords(Long.parseLong(request.getParameter(
					"iShowRows").toString()));
		}
	}

    
    public void setCbuInfoAuditData(String whichWidget, int j) {
		int i;
		if (request.getParameter("iSortCol_0") != null
				&& !request.getParameter("iSortCol_0").toString().equals("")) {
			orderBy = serverOrderBy.fetchCbuInfoAudit_OrderBy(request
					.getParameter("iSortCol_0").toString(), getSdir());
		}
		if (request.getParameter("sSearch") != null
				&& !request.getParameter("sSearch").toString().trim()
						.equals("")) {
			condition = sergloGlobalSearch.fetchGlobalSearch_cbuInfo(request
					.getParameter("sSearch").toString());
			
		}
		
		if (condition != "" ) { 
			condition = " where "+ condition; 
			if (getCbuInfoAuditCondition(j) != "")
				condition = condition + " and "+ getCbuInfoAuditCondition(j);
		}
		else
		{
			if (getCbuInfoAuditCondition(j) != "")
				condition = " where " + getCbuInfoAuditCondition(j);
		}	

		/******************* Getting Paginated Whole List ***********************/
		
		i = 0;
		aaData = FetchCbuInfoAudit(condition, orderBy, i, sStart1, sAmount1, whichWidget);

		/**********************************************************************/

		if (request.getParameter("otherThanAll") != null
				&& request.getParameter("otherThanAll").equals("1")) {
			setOtherthanAll(request.getParameter("otherThanAll").toString());
		}
		if (!getOtherthanAll().equals("1")) {

			/******************* Getting Paginated List Size ***********************/
			i = 1;
			tempLst3 = FetchCbuInfoAudit(condition, orderBy, i, sStart1,
					sAmount1,whichWidget);
			pagLstSizeStr = tempLst3.get(0).toString();
			pagLstSize = Long.parseLong(pagLstSizeStr);
			/******************* Getting Whole List Size without pagination ***********************/
			i = 2;
			condition = "";
			tempLst2 = FetchCbuInfoAudit(condition, orderBy, i, sStart1,
					sAmount1,whichWidget);
			unpagLstSizeStr = tempLst2.get(0).toString();
			unpagLstSize = Long.parseLong(unpagLstSizeStr);
			setAaData(aaData);
			setiTotalRecords(unpagLstSize);
			setiTotalDisplayRecords(pagLstSize);
		}
		if (getOtherthanAll().equals("1")) {
			/************************* Execute Only for Other than All Option ******************/
			setAaData(aaData);
			setiTotalRecords(Long.parseLong(request.getParameter("iShowRows")
					.toString()));
			setiTotalDisplayRecords(Long.parseLong(request.getParameter(
					"iShowRows").toString()));
		}
	}
    
    public void cbuInformationAuditData(String whichWidget, int j) {
		int i;
		if (request.getParameter("iSortCol_0") != null
				&& !request.getParameter("iSortCol_0").toString().equals("")) {
			orderBy = serverOrderBy.fetchCbuInformationAudit_OrderBy(request
					.getParameter("iSortCol_0").toString(), getSdir());
		}
		if (request.getParameter("sSearch") != null
				&& !request.getParameter("sSearch").toString().trim()
						.equals("")) {
			condition = sergloGlobalSearch.fetchGlobalSearch_cbuInfo(request
					.getParameter("sSearch").toString());
			
		}
		
		if (condition != "" ) { 
			condition = " where "+ condition; 
			if (getCbuInfoAuditCondition(j) != "")
				condition = condition + " and "+ getCbuInfoAuditCondition(j);
		}
		else
		{
			if (getCbuInfoAuditCondition(j) != "")
				condition = " where " + getCbuInfoAuditCondition(j);
		}	

		/******************* Getting Paginated Whole List ***********************/
		
		i = 0;
		aaData = FetchCbuInfoAudit(condition, orderBy, i, sStart1, sAmount1, whichWidget);

		/**********************************************************************/

		if (request.getParameter("otherThanAll") != null
				&& request.getParameter("otherThanAll").equals("1")) {
			setOtherthanAll(request.getParameter("otherThanAll").toString());
		}
		if (!getOtherthanAll().equals("1")) {

			/******************* Getting Paginated List Size ***********************/
			i = 1;
			tempLst3 = FetchCbuInfoAudit(condition, orderBy, i, sStart1,
					sAmount1,whichWidget);
			pagLstSizeStr = tempLst3.get(0).toString();
			pagLstSize = Long.parseLong(pagLstSizeStr);
			/******************* Getting Whole List Size without pagination ***********************/
			i = 2;
			condition = "";
			tempLst2 = FetchCbuInfoAudit(condition, orderBy, i, sStart1,
					sAmount1,whichWidget);
			unpagLstSizeStr = tempLst2.get(0).toString();
			unpagLstSize = Long.parseLong(unpagLstSizeStr);
			setAaData(aaData);
			setiTotalRecords(unpagLstSize);
			setiTotalDisplayRecords(pagLstSize);
		}
		if (getOtherthanAll().equals("1")) {
			/************************* Execute Only for Other than All Option ******************/
			setAaData(aaData);
			setiTotalRecords(Long.parseLong(request.getParameter("iShowRows")
					.toString()));
			setiTotalDisplayRecords(Long.parseLong(request.getParameter(
					"iShowRows").toString()));
		}
	}
 
    public void setCbuInfoAuditData() {
		int i;
		if (request.getParameter("iSortCol_0") != null
				&& !request.getParameter("iSortCol_0").toString().equals("")) {
			orderBy = serverOrderBy.fetchCbuInfoAudit_OrderBy(request
					.getParameter("iSortCol_0").toString(), getSdir());
		}
		if (request.getParameter("sSearch") != null
				&& !request.getParameter("sSearch").toString().trim()
						.equals("")) {
			condition = sergloGlobalSearch.fetchGlobalSearch_cbuInfo(request
					.getParameter("sSearch").toString());
		}
		
		if (condition != "" ) { 
			condition = " where "+ condition; 
			if (getCbuInfoAuditCondition(1) != "")
				condition = condition + " and "+ getCbuInfoAuditCondition(1);
		}
		else
		{
			if (getCbuInfoAuditCondition(1) != "")
				condition = " where " + getCbuInfoAuditCondition(1);
		}	
		

		/******************* Getting Paginated Whole List ***********************/
		

		i = 0;
		aaData = FetchCbuInfoAudit(condition, orderBy, i, sStart1, sAmount1);

		/**********************************************************************/

		if (request.getParameter("otherThanAll") != null
				&& request.getParameter("otherThanAll").equals("1")) {
			setOtherthanAll(request.getParameter("otherThanAll").toString());
		}

		if (!getOtherthanAll().equals("1")) {

			/******************* Getting Paginated List Size ***********************/
			i = 1;
			tempLst3 = FetchCbuInfoAudit(condition, orderBy, i, sStart1,
					sAmount1);
			pagLstSizeStr = tempLst3.get(0).toString();
			pagLstSize = Long.parseLong(pagLstSizeStr);
			/******************* Getting Whole List Size without pagination ***********************/
			i = 2;
			condition = "";
			tempLst2 = FetchCbuInfoAudit(condition, orderBy, i, sStart1,
					sAmount1);
			unpagLstSizeStr = tempLst2.get(0).toString();
			unpagLstSize = Long.parseLong(unpagLstSizeStr);
			setAaData(aaData);
			setiTotalRecords(unpagLstSize);
			setiTotalDisplayRecords(pagLstSize);
		}
		if (getOtherthanAll().equals("1")) {

			/************************* Execute Only for Other than All Option ******************/
			setAaData(aaData);
			setiTotalRecords(Long.parseLong(request.getParameter("iShowRows")
					.toString()));
			setiTotalDisplayRecords(Long.parseLong(request.getParameter(
					"iShowRows").toString()));
		}
	}
    
    
    public void setIdInfoAuditData() {
		int i;
		if (request.getParameter("iSortCol_0") != null
				&& !request.getParameter("iSortCol_0").toString().equals("")) {
			orderBy = serverOrderBy.fetchCbuInfoAudit_OrderBy(request
					.getParameter("iSortCol_0").toString(), getSdir());
		}
		if (request.getParameter("sSearch") != null
				&& !request.getParameter("sSearch").toString().trim()
						.equals("")) {
			condition = sergloGlobalSearch.fetchGlobalSearch_cbuInfo(request
					.getParameter("sSearch").toString());
		}
		
		if (condition != "" ) { 
			condition = " where "+ condition; 
			if (getCbuInfoAuditCondition(2) != "")
				condition = condition + " and "+ getCbuInfoAuditCondition(2);
		}
		else
		{
			if (getCbuInfoAuditCondition(2) != "")
				condition = " where " + getCbuInfoAuditCondition(2);
		}	
		
		/******************* Getting Paginated Whole List ***********************/
		i = 0;

		aaData = FetchIdInfoAudit(condition, orderBy, i, sStart1, sAmount1);

		/**********************************************************************/

		if (request.getParameter("otherThanAll") != null
				&& request.getParameter("otherThanAll").equals("1")) {
			setOtherthanAll(request.getParameter("otherThanAll").toString());
		}
		if (!getOtherthanAll().equals("1")) {

			/******************* Getting Paginated List Size ***********************/
			i = 1;
			tempLst3 = FetchIdInfoAudit(condition, orderBy, i, sStart1,
					sAmount1);
			pagLstSizeStr = tempLst3.get(0).toString();
			pagLstSize = Long.parseLong(pagLstSizeStr);
			
			/******************* Getting Whole List Size without pagination ***********************/
			i = 2;
			condition = "";
			tempLst2 = FetchIdInfoAudit(condition, orderBy, i, sStart1,
					sAmount1);
			unpagLstSizeStr = tempLst2.get(0).toString();
			unpagLstSize = Long.parseLong(unpagLstSizeStr);
			setAaData(aaData);
			setiTotalRecords(unpagLstSize);
			setiTotalDisplayRecords(pagLstSize);
		}
		if (getOtherthanAll().equals("1")) {

			/************************* Execute Only for Other than All Option ******************/
			setAaData(aaData);
			setiTotalRecords(Long.parseLong(request.getParameter("iShowRows")
					.toString()));
			setiTotalDisplayRecords(Long.parseLong(request.getParameter(
					"iShowRows").toString()));
		}
	}
    
    
	public void setProcedureData() {

		int i;

		// System.out.println("Inside setProcedureData");

		if (request.getParameter("iSortCol_0") != null
				&& !request.getParameter("iSortCol_0").toString().equals("")
				&& !request.getParameter("iSortCol_0").toString().equals("0")) {
			orderBy = serverOrderBy.FetchProcedure_OrderBy(request
					.getParameter("iSortCol_0").toString(), getSdir());
		}

		if (request.getParameter("sSearch") != null
				&& !request.getParameter("sSearch").toString().trim()
						.equals("")) {
			condition = sergloGlobalSearch.FetchGlobalSearchProcedure(request
					.getParameter("sSearch").toString());
		}

		/******************* Getting Paginated Whole List ***********************/
		i = 0;
		procedure = FetchProcedureData(condition, orderBy, i, sStart1, sAmount1);

		// System.out.println("tempList1.size.."+tempLst1.size());

		/**********************************************************************/

		if (request.getParameter("otherThanAll") != null
				&& request.getParameter("otherThanAll").equals("1")) {
			setOtherthanAll(request.getParameter("otherThanAll").toString());
		}

		if (!getOtherthanAll().equals("1")) {

			/******************* Getting Paginated List Size ***********************/
			i = 1;
			tempLst3 = FetchProcedureData(condition, orderBy, i, sStart1,
					sAmount1);
			// BigDecimal totCount1 = (BigDecimal)tempLst3.get(0);
			pagLstSizeStr = tempLst3.get(0).toString();
			pagLstSize = Long.parseLong(pagLstSizeStr);
			// System.out.println("\n\n\n\n\ntempList3.size..Procedure Table:"+tempLst3.size()+"size1..."+pagLstSize);
			/**********************************************************************/

			/******************* Getting Whole List Size without pagination ***********************/
			i = 2;
			condition = "";
			tempLst2 = FetchProcedureData(condition, orderBy, i, sStart1,
					sAmount1);
			// BigDecimal totCount2 = (BigDecimal)tempLst2.get(0);
			unpagLstSizeStr = tempLst2.get(0).toString();
			unpagLstSize = Long.parseLong(unpagLstSizeStr);
			// System.out.println("tempList2.size..Procedure Table:"+tempLst2.size()+"size2..."+unpagLstSize);
			/************************************************************************************/

			setProcedure(procedure);
			setiTotalRecords(unpagLstSize);
			setiTotalDisplayRecords(pagLstSize);

		}

		if (getOtherthanAll().equals("1")) {

			/************************* Execute Only for Other than All Option ******************/
			setProcedure(procedure);
			setiTotalRecords(Long.parseLong(request.getParameter("iShowRows")
					.toString()));
			setiTotalDisplayRecords(Long.parseLong(request.getParameter(
					"iShowRows").toString()));
		}

	}

    public void setSavInProgPs(){
       
    	
    	
    	int i;
    	
    	//System.out.println("Inside setPendingOrders");
    	
    	if(request.getParameter("iSortCol_0")!=null && !request.getParameter("iSortCol_0").toString().equals("") &&  !request.getParameter("iSortCol_0").toString().equals("0")){
        	orderBy=serverOrderBy.FetchPSSavInProg_OrderBy(request.getParameter("iSortCol_0").toString(),getSdir());
        }
    	
    	if(request.getParameter("sSearch")!=null && !request.getParameter("sSearch").toString().trim().equals("")){
        	condition=sergloGlobalSearch.FetchGlobalSearchPSSavInProg(request.getParameter("sSearch").toString());
        }
    	
    	
    	/*******************Getting Paginated Whole List***********************/
    	i=0;
    	tempLst1=FetchPSSIPData(condition,orderBy,i,sStart1,sAmount1);
    	
    	//System.out.println("tempList1.size.."+tempLst1.size());
    	
    	/**********************************************************************/
    	
    	
    	if(request.getParameter("otherThanAll")!=null && request.getParameter("otherThanAll").equals("1")){
    		setOtherthanAll(request.getParameter("otherThanAll").toString());
    	}
    	
    	
    	if(!getOtherthanAll().equals("1")){
		    	
    			/*******************Getting Paginated List Size***********************/
		    	i=1;
		    	tempLst3=FetchPSSIPData(condition,orderBy,i,sStart1,sAmount1);
		    	BigDecimal totCount1 = (BigDecimal)tempLst3.get(0);
		    	pagLstSizeStr = totCount1.toString();
		    	pagLstSize=Long.parseLong(pagLstSizeStr);
		    	//System.out.println("tempList3.size..SIP ps:"+tempLst3.size()+"size1..."+pagLstSize);
		    	/**********************************************************************/
		    	
		    	
		    	/*******************Getting Whole List Size without pagination***********************/
		    	i=2;
		    	condition="";
		    	tempLst2=FetchPSSIPData(condition,orderBy,i,sStart1,sAmount1);
		    	BigDecimal totCount2 = (BigDecimal)tempLst2.get(0);
		    	unpagLstSizeStr = totCount2.toString();
		    	unpagLstSize=Long.parseLong(unpagLstSizeStr);
		    	//System.out.println("tempList2.size..SIP ps:"+tempLst2.size()+"size2..."+unpagLstSize);
		    	/************************************************************************************/
		    	
		    	
		    	setAaData(tempLst1);
		        setiTotalRecords(unpagLstSize);
				setiTotalDisplayRecords(pagLstSize);
		
    	}
		
        if(getOtherthanAll().equals("1")){
    		
    		/*************************Execute Only for Other than All Option******************/
    		setAaData(tempLst1);
    		setiTotalRecords(Long.parseLong(request.getParameter("iShowRows").toString()));
    		setiTotalDisplayRecords(Long.parseLong(request.getParameter("iShowRows").toString()));
    	}
    	
    	
    }
    public void setNotAvailPs(){
    	
    	
    	int i;
    	

    	
    	if(request.getParameter("iSortCol_0")!=null && !request.getParameter("iSortCol_0").toString().equals("") &&  !request.getParameter("iSortCol_0").toString().equals("0")){
        	orderBy=serverOrderBy.FetchPSNotAvail_OrderBy(request.getParameter("iSortCol_0").toString(),getSdir());
        }
    	
    	if(request.getParameter("sSearch")!=null && !request.getParameter("sSearch").toString().trim().equals("")){
        	condition=sergloGlobalSearch.FetchGlobalSearchPSNotAvailCbu(request.getParameter("sSearch").toString());
        }
    	
    	if(request.getParameter("otherThanAll")!=null && request.getParameter("otherThanAll").equals("1")){
    		setOtherthanAll(request.getParameter("otherThanAll").toString());
    	}
    	
    	//System.out.println("start value:::::"+sStart1+"end value"+sAmount1);
    	
    	/*******************Getting Paginated Whole List***********************/
    	i=0;
    	tempLst1=FetchPsNotAvailData(condition,orderBy,i,sStart1,sAmount1);
    	
    	//System.out.println("Not avail cbu tempList1.size.."+tempLst1.size());
    	
    	/**********************************************************************/
    	
    	
    	if(!getOtherthanAll().equals("1")){
			    	
    		
    				/*******************Getting Paginated List Size***********************/
			    	i=1;
			    	tempLst3=FetchPsNotAvailData(condition,orderBy,i,sStart1,sAmount1);
			    	BigDecimal totCount1 = (BigDecimal)tempLst3.get(0);
			    	pagLstSizeStr = totCount1.toString();
			    	pagLstSize=Long.parseLong(pagLstSizeStr);
			    	//System.out.println("tempList3.size.. not avail:"+tempLst3.size()+"size1..."+pagLstSize);
			    	/**********************************************************************/
			    	
			    	
			    	/*******************Getting Whole List Size without pagination***********************/
			    	i=2;
			    	condition="";
			    	tempLst2=FetchPsNotAvailData(condition,orderBy,i,sStart1,sAmount1);
			    	BigDecimal totCount2 = (BigDecimal)tempLst2.get(0);
			    	unpagLstSizeStr = totCount2.toString();
			    	unpagLstSize=Long.parseLong(unpagLstSizeStr);
			    	//System.out.println("tempList2.size..not avail:"+tempLst2.size()+"size2..."+unpagLstSize);
			    	/************************************************************************************/
			    	
			    	setAaData(tempLst1);
			        setiTotalRecords(unpagLstSize);
					setiTotalDisplayRecords(pagLstSize);
    	
    	}
    	
        if(getOtherthanAll().equals("1")){
    		
    		/*************************Execute Only for Other than All Option******************/
    		setAaData(tempLst1);
    		setiTotalRecords(Long.parseLong(request.getParameter("iShowRows").toString()));
    		setiTotalDisplayRecords(Long.parseLong(request.getParameter("iShowRows").toString()));
    	}
       
    	
    	
    }
    
    public void setNotAvailCBUs(){
    	int i;
    	
    	//System.out.println("Inside setNotAvailCBUs");
    	
    	if(request.getParameter("otherThanAll")!=null && request.getParameter("otherThanAll").equals("1")){
    		setOtherthanAll(request.getParameter("otherThanAll").toString());
    	}
    	
    	
    	if(getIsSavedInProgress()!=null && getIsSavedInProgress().equals("1")){
    		if(request.getParameter("iSortCol_0") != null && !request.getParameter("iSortCol_0").toString().equals("") && !request.getParameter("iSortCol_0").toString().equals("0")){
            	orderBy = serverOrderBy.FetchSavedInProgress_OrderBy(request.getParameter("iSortCol_0").toString(),getSdir());
            }
        	
        	if(request.getParameter("sSearch") != null && !request.getParameter("sSearch").toString().trim().equals("")){
            	condition = sergloGlobalSearch.FetchGlobalSearchSavedInProgress(request.getParameter("sSearch").toString());
            }
    	}else{
    		if(request.getParameter("iSortCol_0") != null && !request.getParameter("iSortCol_0").toString().equals("")){
            	orderBy = serverOrderBy.FetchNotAvailCbus_OrderBy(request.getParameter("iSortCol_0").toString(),getSdir());
            }
        	
        	if(request.getParameter("sSearch") != null && !request.getParameter("sSearch").toString().trim().equals("")){
            	condition = sergloGlobalSearch.FetchGlobalSearchNotAvailCbu(request.getParameter("sSearch").toString());
            }
    	}
    	
    	/*******************Getting Paginated Whole List***********************/
    	i = 1;
    	tempLst1 = FetchNotAvailCBUsData(condition,orderBy,i,sStart1,sAmount1);
    	
    	//System.out.println("tempList1.size.."+tempLst1.size());
    	
    	if(!getOtherthanAll().equals("1")){
    		
    		
    		i = 2;
        	/*******************Getting Paginated List Size***********************/
        	tempLst3 = FetchNotAvailCBUsData(condition,orderBy,i,sStart1,sAmount1);
        	BigDecimal totCount1 = (BigDecimal)tempLst3.get(0);
        	pagLstSizeStr = totCount1.toString();
        	pagLstSize = Long.parseLong(pagLstSizeStr);
        	//System.out.println("tempList3.size.. alert"+tempLst3.size()+"size1..."+pagLstSize);
        	/**********************************************************************/
        	
        	/*******************Getting Whole List Size without pagination***********************/
        	i = 3;
        	condition = "";
        	tempLst2 = FetchNotAvailCBUsData(condition,orderBy,i,sStart1,sAmount1);
        	BigDecimal totCount2 = (BigDecimal)tempLst2.get(0);
        	unpagLstSizeStr = totCount2.toString();
        	unpagLstSize = Long.parseLong(unpagLstSizeStr);
        	//System.out.println("tempList2.size..alert"+tempLst2.size()+"size2..."+unpagLstSize);
        	/************************************************************************************/
        	
        	setAaData(tempLst1);
            setiTotalRecords(unpagLstSize);
    		setiTotalDisplayRecords(pagLstSize);
    		
    	}
    	
    	if(getOtherthanAll().equals("1")){
        	setAaData(tempLst1);
            setiTotalRecords(Long.parseLong(request.getParameter("iShowRows").toString()));
    		setiTotalDisplayRecords(Long.parseLong(request.getParameter("iShowRows").toString()));
    	}
    	
    	
    	/**********************************************************************/
    	
    	
    }
    public List FetchNotAvailCBUsData(String condition,String orderBy,int i,int Start,int End){
    	List<Object> cdrObjects = new ArrayList<Object>();
		//System.out.println("condition:::"+condition);
		//System.out.println(":::orderBy:::"+orderBy);
  	   try{
  		  //System.out.println("INside FetchNotAvailCBUsData");
  		   UserJB user = (UserJB)getSession(getRequest()).getAttribute(VelosGarudaConstants.GARUDA_USER);
  		   if(user!=null){
  			   Long userSiteId = Long.parseLong(user.getUserSiteId().toString());
  			   request.setAttribute("userSiteId", userSiteId);
  			   Long userId=(long)user.getUserId();
  			   request.setAttribute("userId", userId);
  		   }
  		 	StringBuilder sqlHead = new StringBuilder();		   
		    if(i==1){
				//System.out.println("\n\n\n\n\ninside i=0...");
				if(getIsSavedInProgress()!=null && getIsSavedInProgress().equals("1")){
					sqlHead.append( "SELECT Distinct(C.PK_CORD),C.CORD_REGISTRY_ID,C.CORD_ENTRY_PROGRESS,to_char(C.created_on,'Mon DD, YYYY') createdon,C.created_on creationdt");
					sqlHead.append( cbuaction.constructSqlsForSearch(user, null)+condition);
				}else{
					sqlHead.append( "SELECT Distinct(C.PK_CORD),C.CORD_REGISTRY_ID,CS.CBU_STATUS_DESC,to_char(C.CORD_CREATION_DATE,'Mon DD, YYYY') createdon,C.CORD_SEARCHABLE,C.CORD_CREATION_DATE ");
					sqlHead.append( cbuaction.constructSqlsForSearch(user, MODULE_NOT_AVAILABLE_CBUS)+condition);
				}
				if(!orderBy.equals("")){
					sqlHead.append( orderBy);
				}else{
					
					if(getIsSavedInProgress()!=null && getIsSavedInProgress().equals("1")){
						sqlHead.append( " order by createdon asc");
						
					}else{
						sqlHead.append( " order by CORD_CREATION_DATE asc");
					}
					
				}
				
				//System.out.println("\n\n\n\n\nSQl for cord entry tables:::::::::::::::::::::::::::::..."+sqlHead);
				
				
				if(request.getParameter("otherThanAll")!=null && request.getParameter("otherThanAll").equals("1")){
					paginationReqScopeData(paginationSearch);
		    	}else{
		    		paginationSearch.setiPageNo(Start);
					paginationSearch.setiShowRows(End);
		    		
		    	}
				
				cdrObjects = cbuController.getCordListAsobject(sqlHead.toString(), paginationSearch);
				//System.out.println("\n\n\n\n\nend of i=0...");
			}
		    if(i==2){
				//System.out.println("\n\n\n\n\ninside i=1...");
				sqlHead  = new StringBuilder("SELECT count(Distinct(c.pk_cord)) ");
				if(getIsSavedInProgress()!=null && getIsSavedInProgress().equals("1")){
					sqlHead.append( cbuaction.constructSqlsForSearch(user, null)+condition);
				}else{
					sqlHead.append( cbuaction.constructSqlsForSearch(user, MODULE_NOT_AVAILABLE_CBUS)+condition);
				}
	          	cdrObjects = cbuController.getCordListAsobject(sqlHead.toString(), null);
				//System.out.println("\n\n\n\n\nend of i=1...");
			}
			if(i==3){
				//System.out.println("\n\n\n\n\ninside i=1...");
				sqlHead  = new StringBuilder("SELECT count(Distinct(c.pk_cord)) ");
				if(getIsSavedInProgress()!=null && getIsSavedInProgress().equals("1")){
					sqlHead.append( cbuaction.constructSqlsForSearch(user, null)+condition);
				}else{
					sqlHead.append( cbuaction.constructSqlsForSearch(user, MODULE_NOT_AVAILABLE_CBUS));
				}
	          	cdrObjects = cbuController.getCordListAsobject(sqlHead.toString(), null);
				//System.out.println("\n\n\n\n\nend of i=1...");
			}
			if(getIsSavedInProgress()!=null && !getIsSavedInProgress().equals("1")){
				getRequest().setAttribute("cbuDeferlist",cdrObjects);
			}
			request.setAttribute("paginateSearch", paginationSearch);
  	   }catch(Exception ex){
  		   ex.printStackTrace();
  	   }
  	return cdrObjects;
     }
    
       
    public void setLookupData(){
    	int i;    	
    	List<Object> cdrObjects = new ArrayList<Object>();
    	int UploadDoc=0;
    	int queryBuilder=0;
    	
    	//System.out.println("\n\n\n\n\n\n\n\nInside calling server action...........\n\n");
    	
    	if(request.getParameter("pageFlag") != null && request.getParameter("pageFlag").toString().equals("uploadDoc")){
    		UploadDoc=1;
        }
    	
    	if(getPaginateModule()!=null && (getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_ANTIGEN_REPORT) || getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_LICENSEANDELIGIBILTY_REPORT) || getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_QUICKSUMMARY_REPORT) || getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_IDSUMMARY_REPORT)) ){
    		 queryBuilder=1;
    	}
    	
		if(request.getParameter("iSortCol_0") != null && !request.getParameter("iSortCol_0").toString().equals("")){
			
			if(UploadDoc==1 && !request.getParameter("iSortCol_0").toString().equals("0")){
				//System.out.println("\n\n\n\n\n\nInside UploadDoc order By");
				orderBy = serverOrderBy.FetchUploadDoc_OrderBy(request.getParameter("iSortCol_0").toString(),getSdir());
			}else if(queryBuilder==1 && request.getParameter("currentpage")!=null 
					&& request.getParameter("currentpage").equals("shippedcbu")){
				
				orderBy= serverOrderBy.FetchQBuilder_OrderBy(request.getParameter("iSortCol_0").toString(),getSdir());
				
			}else if(queryBuilder==1 && request.getParameter("currentpage")!=null 
					&& request.getParameter("currentpage").equals("cbuSummary")){
				
				orderBy= serverOrderBy.FetchQBuilder_CBUID_OrderBy(request.getParameter("iSortCol_0").toString(),getSdir());
				
			}else if(queryBuilder==1 && request.getParameter("currentpage")!=null 
					&& request.getParameter("currentpage").equals("licElig")){
				
				 orderBy= serverOrderBy.FetchQBuilder_LICElig_OrderBy(request.getParameter("iSortCol_0").toString(),getSdir());
				 
			}else{
				orderBy = serverOrderBy.FetchLookup_OrderBy(request.getParameter("iSortCol_0").toString(),getSdir());
			}
        	
        }
    	
    	if(request.getParameter("sSearch") != null && !request.getParameter("sSearch").toString().trim().equals("")){
    		
    		if(UploadDoc==1 && !request.getParameter("iSortCol_0").toString().equals("0")){
				//System.out.println("\n\n\n\n\n\nInside UploadDoc condition");
				 condition = sergloGlobalSearch.FetchGlobalSearchUploadDoc(request.getParameter("sSearch").toString());
			}else if(queryBuilder==1 && request.getParameter("currentpage")!=null 
					&& request.getParameter("currentpage").equals("shippedcbu")){
				 condition = sergloGlobalSearch.FetchGlobalSearchQBuilder(request.getParameter("sSearch").toString());
			}else if(queryBuilder==1 && request.getParameter("currentpage")!=null 
					&& request.getParameter("currentpage").equals("cbuSummary")){
				 condition = sergloGlobalSearch.FetchGlobalSearchQBuilder_CBUID(request.getParameter("sSearch").toString());
			}else if(queryBuilder==1 && request.getParameter("currentpage")!=null 
					&& request.getParameter("currentpage").equals("licElig")){
				 condition = sergloGlobalSearch.FetchGlobalSearchQBuilder_LICELig(request.getParameter("sSearch").toString());
			}else{
        	    condition = sergloGlobalSearch.FetchGlobalSearchLookup(request.getParameter("sSearch").toString());
			}
        }
    	/********************************/
    	cbuaction.setPaginateModule(getPaginateModule());
		if(getCbuid()!=null){
			   setInputId(getCbuid());
			   cbuaction.setInputId(getInputId());
			   CdrCbuPojo cdrCbuPojo = new CdrCbuPojo();
			   cdrCbuPojo.setCdrCbuId(getCbuid());
			   cbuaction.setCdrCbuPojo(cdrCbuPojo);			   
		}
		if(getCbuCount()!=null){
			cbuaction.setCbuCount(getCbuCount());
		}
		try{
	  		   UserJB user = (UserJB)getSession(getRequest()).getAttribute(VelosGarudaConstants.GARUDA_USER);
	  		   if(user!=null){
	  			   Long userSiteId = Long.parseLong(user.getUserSiteId().toString());
	  			   request.setAttribute("userSiteId", userSiteId);
	  			   Long userId=(long)user.getUserId();
	  			   request.setAttribute("userId", userId);
	  		   }
	  		 	StringBuilder sqlHead = new StringBuilder();
				if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(MODULE_VIEW_CLINICAL)){
		          	sqlHead.append( "SELECT Distinct(C.PK_CORD),C.CORD_REGISTRY_ID,C.registry_maternal_id,C.CORD_LOCAL_CBU_ID,C.maternal_local_id,C.CORD_ISBI_DIN_CODE,SITE.SITE_NAME,PAT.PATIENTID,C.CORD_ID_NUMBER_ON_CBU_BAG" );
		          	sqlHead.append(cbuaction.constructSqlsForSearch(user, null)+condition);
		          	if(!StringUtils.isEmpty(orderBy))
						sqlHead.append(" " + orderBy);
					else
						sqlHead.append(" ORDER BY C.CORD_REGISTRY_ID");
		          	
		          	/*paginationSearch.setiPageNo(sStart1);
					paginationSearch.setiShowRows(sAmount1);
					   
		          	 tempLst1 = cbuController.getCordListAsobject(sqlHead, paginationSearch);
		          	 String countRecSql = new String("Select count(*) from ( "+sqlHead+" )");
					 tempLst2 = cbuController.getCordListAsobject(countRecSql, null);
					 
				   		if(tempLst1!=null){
				   			setAaData(tempLst1);
				   			getRequest().setAttribute("cordInfoList",tempLst1);
				   }
				   if(tempLst2!=null && tempLst2.size()>0)
					  setiTotalDisplayRecords(Long.parseLong(String.valueOf(tempLst2.size())));
				   if(getCbuCount()!=null)
					   setiTotalRecords(getCbuCount().longValue());
				   request.setAttribute("paginateSearch", paginationSearch);
				   
				   return;*/
				   
			   }else if(getPaginateModule().equalsIgnoreCase(MODULE_VIEW_LOOKUP)){
				   sqlHead.append("SELECT Distinct(C.PK_CORD),C.CORD_REGISTRY_ID,C.registry_maternal_id,C.CORD_LOCAL_CBU_ID,C.maternal_local_id,C.CORD_ISBI_DIN_CODE,site.SITE_NAME,PAT.PATIENTID");
				   sqlHead.append(cbuaction.constructSqlsForSearch(user, null)+condition);
				   if(!StringUtils.isEmpty(orderBy))
					   sqlHead.append( " " + orderBy);
				   else
				       sqlHead.append( " ORDER BY C.CORD_REGISTRY_ID");
				   
				   //System.out.println("\n\n\n\n\n\n\n\nSQL 2  in Server Action:::"+sqlHead);
				   //System.out.println("\n\n\n\ncase 2:::::::::::");
			   }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(MODULE_QUERY_BUILDER)){
					sqlHead.append( "SELECT Distinct(C.PK_CORD),C.CORD_REGISTRY_ID,C.registry_maternal_id,C.CORD_LOCAL_CBU_ID,C.maternal_local_id,C.CORD_ISBI_DIN_CODE,site.SITE_ID,CB_REC_INFO.RECEIPANT_ID,C.CORD_ID_NUMBER_ON_CBU_BAG" +
			           " , C.CORD_HISTORIC_CBU_ID,ORD.ORDER_TYPE,ORD.ORDER_SAMPLE_AT_LAB,SHIPMENT.SCH_SHIPMENT_DATE,HDR.ORDER_OPEN_DATE,SHIPMENT.FK_SHIPMENT_TYPE");
			        sqlHead.append( getSession(getRequest()).getAttribute("queryBuilderSql")+condition);
			        if(!StringUtils.isEmpty(orderBy))
						sqlHead.append( " " + orderBy);
					else
					    sqlHead.append( " ORDER BY site.SITE_ID,C.CORD_REGISTRY_ID,HDR.ORDER_OPEN_DATE");
			        //System.out.println("\n\n\n\ncase 4:::::::::::");
			        
		      }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_ANTIGEN_REPORT)){
		    	  /*sqlHead += "SELECT Distinct(C.PK_CORD),C.REGISTRY_ID,C.REGISTRY_MATERNAL_ID,C.CORD_LOCAL_CBU_ID,C.MATERNAL_LOCAL_ID,C.CORD_ISBI_DIN_CODE,C.SITE_ID,C.PATIENT_ID,C.CORD_ID_NUMBER_ON_CBU_BAG" +
				   " , C.CORD_HISTORIC_CBU_ID,C.ORDER_TYPE,C.ORDER_SAMPLE_AT_LAB,TO_CHAR(C.SHIP_DATE,'Mon DD, YYYY') SHIP_DATE,TO_CHAR(C.ORDER_OPEN_DATE,'Mon DD, YYYY') ORDER_OPEN_DATE,C.FK_SHIPMENT_TYPE ,"+
				    " CASE "+
		            " WHEN ERES.f_codelst_desc(C.order_type)='CT' "+
		            " AND C.order_sample_at_lab        ='Y'"+
		            " THEN 'CT - Sample at Lab' "+
		            " WHEN ERES.f_codelst_desc(C.order_type)='CT'"+
		            " AND C.order_sample_at_lab        ='N'"+
		            " THEN 'CT - Ship Sample' "+
		            " ELSE ERES.f_codelst_desc(C.order_type) "+
		           " END req_type ";*/
		    	  sqlHead.append( "SELECT Distinct(C.PK_CORD),C.REGISTRY_ID,C.REGISTRY_MATERNAL_ID,C.CORD_LOCAL_CBU_ID,C.MATERNAL_LOCAL_ID,C.CORD_ISBI_DIN_CODE,C.SITE_ID,C.PATIENT_ID,C.CORD_ID_NUMBER_ON_CBU_BAG" +
				   " , C.CORD_HISTORIC_CBU_ID ");
		    	  sqlHead.append( getSession(getRequest()).getAttribute("queryBuilderSql")+condition);		          	
			   }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_LICENSEANDELIGIBILTY_REPORT)){
				   sqlHead.append( "SELECT DISTINCT(C.PK_CORD), " 
					   +"  C.site_id                                        AS cbb_id, " 
					   +"  C.REGISTRY_ID                                    AS cbu_registry_id, " 
					   +"  C.CORD_LOCAL_CBU_ID                              AS cbu_local_id, " 
					   +"  C.CORD_ID_NUMBER_ON_CBU_BAG                      AS uniqueidonbag, " 
					   +"  TO_CHAR(C.SPEC_COLLECTION_DATE, 'Mon DD YYYY')   AS cbu_collection_date, " 
					   +"  TO_CHAR(C.CORD_REGISTRATION_DATE, 'Mon DD YYYY') AS nmdp_reg_date, " 
					   +"  C.CORD_NMDP_STATUS                               AS CBU_Status, " 
					   +"  C.FK_CORD_CBU_STATUS                             AS Reason, " 
					   +"  C.FK_CORD_CBU_LIC_STATUS                         AS Licensure_Status, " 
					   +"  c.UNLICENSED_DESC, "
					   +"  C.fk_cord_cbu_eligible_status AS Elig_Deter, " 
					   +"  c.INELIG_INCOMP_DESC, " 
					   +"  TO_CHAR(C.CORD_BABY_BIRTH_DATE, 'Mon DD YYYY') Baby_birth_date, " 
					   +"  c.RACE_DESC    AS Race, " 
					   +"  C.NMDP_RACE_ID AS Race_Summary , " 
					   +"  C.funded_cbu   AS Funded , " 
					   +"  C.FUNDING_CATEGORY, " 
					   +"  C.FK_CORD_BACT_CUL_RESULT   AS Bact_cultr_rst, " 
					   +"  C.FK_CORD_FUNGAL_CUL_RESULT AS Fungal_cultr_rst, " 
					   +"  C.HEMOGLOBIN_SCRN           AS Hemoglobin_Testing, " 
					   +"  PROC.PROC_NAME              AS Processing_procedure , " 
					   +"  Nucl_cell_count.resultdata  AS nucl_cell_countdata, " 
					   +"  Nucl_cell_count_unknown.resultdata, " 
					   +"  CD34_cell_count.resultdata                AS cd34_cell_countdata , " 
					   +"  Viability_Method.resultdata                AS Viability_countdata, " 
					   +"  Viability_Method.fkTestMethod               AS Viability_Methoddata, " 
					   +"  Final_Prod_Vol.resultdata                 AS Final_Prod_Voldata, " 
					   +"  TO_CHAR(C.SHIP_DATE, 'Mon DD YYYY')       AS SHIP_DATE, " 
					   +"  TO_CHAR(C.SHIP_DATE, 'Mon DD YYYY')       AS SHIP_DATE1, " 
					   +"  C.ORDER_TYPE                              AS req_type, " 
					   +"  TO_CHAR(C.ORDER_OPEN_DATE, 'Mon DD YYYY') AS Request_Date, " 
					   +"  C.order_sample_at_lab, " 
					   +"  C.SPEC_COLLECTION_DATE   AS d1, " 
					   +"  C.CORD_REGISTRATION_DATE AS d2, " 
					   +"  C.CORD_BABY_BIRTH_DATE   AS d3, " 
					   +"  C.ORDER_OPEN_DATE        AS d4, " 
					   +"  C.SHIP_DATE              AS d5, TO_CHAR(c.act_ship_date, 'Mon DD YYYY') AS ACT_SHIP_DATE, ROW_NUMBER () OVER(ORDER BY C.site_id) "); 
				   sqlHead.append( getSession(getRequest()).getAttribute("queryBuilderLESql")+condition);	
				   if(!StringUtils.isEmpty(orderBy))
						sqlHead.append( " " + orderBy);
					else
					    sqlHead.append( " ORDER BY site.SITE_ID,C.CORD_REGISTRY_ID,HDR.ORDER_OPEN_DATE desc ");
			   }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_QUICKSUMMARY_REPORT)){
				   sqlHead.append( "SELECT Distinct(C.PK_CORD),"+
						        "C.SITE_ID,"+
						        "C.REGISTRY_ID,"+
						        "C.CORD_LOCAL_CBU_ID,"+
						        "C.CORD_ID_NUMBER_ON_CBU_BAG,"+
						        "C.CORD_ISBI_DIN_CODE,"+
						        "C.REGISTRY_MATERNAL_ID,"+
						        "C.MATERNAL_LOCAL_ID,"+
						        "C.CORD_HISTORIC_CBU_ID,"+
						        "CASE  "+
						            "WHEN ERES.f_codelst_desc(C.order_type)='CT'"+  
						            "AND C.order_sample_at_lab        ='Y' THEN 'CT - Sample at Lab'"+  
						            "WHEN ERES.f_codelst_desc(C.order_type)='CT' "+
						            "AND C.order_sample_at_lab        ='N' THEN 'CT - Ship Sample'"+  
						            "ELSE ERES.f_codelst_desc(C.order_type)  "+
						        "END req_type,"+
						        "TO_CHAR(C.ORDER_OPEN_DATE,"+
						        "'Mon DD YYYY') ORDER_OPEN_DATE,"+
						        "TO_CHAR(C.SHIP_DATE,"+
						        "'Mon DD YYYY') SHIP_DATE,"+
						        "TO_CHAR(C.INFUSION_DATE,"+
						        "'Mon DD YYYY') INFUSION_DATE ,"+
						        "C.PATIENT_ID,"+
						        "C.CURRENT_DIAGNOSIS,"+
						        "C.TRANS_SITE_ID,"+
						        "C.TRANS_SITE_NAME,"+
						        "C.ORDER_OPEN_DATE AS d1,"+ 
							    "C.SHIP_DATE AS d2,"+
							    "C.INFUSION_DATE as d3");

				   sqlHead.append( getSession(getRequest()).getAttribute("queryBuilderORSql")+condition);	
				   if(!StringUtils.isEmpty(orderBy))
						sqlHead.append( " " + orderBy);
					else
					    sqlHead.append( " ORDER BY site.SITE_ID,C.CORD_REGISTRY_ID,HDR.ORDER_OPEN_DATE desc ");
			   }else if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_IDSUMMARY_REPORT)){
				   /*sqlHead += "SELECT Distinct(C.PK_CORD),C.SITE_ID,C.REGISTRY_ID,C.REGISTRY_MATERNAL_ID,C.CORD_LOCAL_CBU_ID,C.MATERNAL_LOCAL_ID,C.CORD_ISBI_DIN_CODE,C.CORD_ID_NUMBER_ON_CBU_BAG" +
				   " , C.CORD_HISTORIC_CBU_ID,"+
				   " CASE "+
		            " WHEN ERES.f_codelst_desc(C.order_type)='CT' "+
		            " AND C.order_sample_at_lab        ='Y'"+
		            " THEN 'CT - Sample at Lab' "+
		            " WHEN ERES.f_codelst_desc(C.order_type)='CT'"+
		            " AND C.order_sample_at_lab        ='N'"+
		            " THEN 'CT - Ship Sample' "+
		            " ELSE ERES.f_codelst_desc(C.order_type) "+
		           " END req_type ,TO_CHAR(C.SHIP_DATE,'Mon DD, YYYY') SHIP_DATE,TO_CHAR(C.ORDER_OPEN_DATE,'Mon DD, YYYY') ORDER_OPEN_DATE,C.ORDER_TYPE,"+
		           "  c.ORDER_OPEN_DATE as opendt,"+
				   "  c.SHIP_DATE as shipdt";*/
				   sqlHead.append( "SELECT Distinct(C.PK_CORD),C.SITE_ID,C.REGISTRY_ID,C.REGISTRY_MATERNAL_ID,C.CORD_LOCAL_CBU_ID,C.MATERNAL_LOCAL_ID,C.CORD_ISBI_DIN_CODE,C.CORD_ID_NUMBER_ON_CBU_BAG" +
				   " , C.CORD_HISTORIC_CBU_ID");
				   sqlHead.append( getSession(getRequest()).getAttribute("queryBuilderSql")+condition);	
				   if(!StringUtils.isEmpty(orderBy))
						sqlHead.append( " " + orderBy);
					else
					    sqlHead.append( " ORDER BY site.SITE_ID,C.CORD_REGISTRY_ID ");
			   }else{
				   sqlHead.append( "SELECT Distinct(C.PK_CORD),C.CORD_REGISTRY_ID,C.CORD_ENTRY_PROGRESS,C.created_on");
				   sqlHead.append( cbuaction.constructSqlsForSearch(user, null)+condition);
				   if(!StringUtils.isEmpty(orderBy))
					   sqlHead.append( " " + orderBy);
				   else
					   sqlHead.append( " order by C.created_on");
				   //System.out.println("\n\n\n\ncase 5:::::::::::");
				   //System.out.println("\n\n\n\n\n\n\n\nSQL 3 in Server Action:::"+sqlHead);
			   }
			   //System.out.println("\n\n\n\nsql for lookup:::::::::::"+sqlHead);
			   
			   if(request.getParameter("otherThanAll")!=null && !request.getParameter("otherThanAll").equals("") && request.getParameter("otherThanAll").equals("1")){
				   paginationSessionData(paginationSearch,0,MODULE_CBBPROCEDURE);
				   
			   }else{
				   paginationSearch.setiPageNo(sStart1);
				   paginationSearch.setiShowRows(sAmount1);
			   }
			   if(getPaginateModule()!=null && (getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_ANTIGEN_REPORT)
					   || getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_LICENSEANDELIGIBILTY_REPORT)
					   || getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_QUICKSUMMARY_REPORT)
					   || getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_IDSUMMARY_REPORT))){
			       VDAController vdaController = new VDAController();
				   tempLst1 = vdaController.getQueryBuilderViewData(sqlHead.toString(), paginationSearch);	
				   if(getCbuCount()!=null)
					   setiTotalDisplayRecords(Long.parseLong(String.valueOf(tempLst2.size())));
				   
				   if(tempLst1!=null && getPaginateModule().equalsIgnoreCase(VelosGarudaConstants.NMDP_CBU_LICENSEANDELIGIBILTY_REPORT)){
					    String dateStr = null;		
						Date codeLstDate = null;
						Date schshipdate = null;
						Date actshipdate = null;
						try{
							
							
							dateStr = getCodeListDescByTypeAndSubtype(VELOS_EMTRAX_VERSION_TYPE,VELOS_EMTRAX_VERSION_SUBTYPE);
							if(dateStr!=null){
								codeLstDate = Utilities.getDate(dateStr, VELOS_EMTRAX_VERSION1_3_DATE_FORMAT);
							}
									
						
						}catch (Exception e) {
							System.out.println(e);
						}
					   for(int k=0;k<tempLst1.size();k++){
						   
						   Object[] Obj=(Object[]) tempLst1.get(k);
						   
						   
						   /***************************For NMDP CBU Status*********************************/
						   
						   String nmdpStatus="";
						  
						   if(Obj[7]!=null && !Obj[7].toString().equals("")){
							   nmdpStatus=Obj[7].toString();
							   Obj[7]=getCbuStatusDescByPk(Long.parseLong(nmdpStatus));
							       
						   }
						   /******************************************************************************/
						   
						   /***************************For CBU Status*********************************/
						   
						   String cbuStatus="";
						  
						   if(Obj[8]!=null && !Obj[8].toString().equals("")){
							   cbuStatus=Obj[8].toString();
							   Obj[8]=getCbuStatusDescByPk(Long.parseLong(cbuStatus));
							       
						   }
						   /******************************************************************************/
						   
						   /***************************For Race Values*********************************/
						   
						   String raceType="";
						  
						   if(Obj[15]!=null && !Obj[15].toString().equals("")){
							   raceType=Obj[15].toString();
							   Obj[15]=getCodeListDescByPk(Long.parseLong(raceType));
							       
						   }
						   /******************************************************************************/
						   /**************************For Bacterial Cultural Result***********************/
						   
						   String bactCultRes="";
						  
						   if(Obj[18]!=null && !Obj[18].toString().equals("")){
							   bactCultRes=Obj[18].toString();
							   Obj[18]=getCodeListDescByPk(Long.parseLong(bactCultRes));
							       
						   }
						   /******************************************************************************/
						   
						   /**************************For Fungal Cultural Result***********************/
						   
						   String fungCultRes="";
						  
						   if(Obj[19]!=null && !Obj[19].toString().equals("")){
							   fungCultRes=Obj[19].toString();
							   Obj[19]=getCodeListDescByPk(Long.parseLong(fungCultRes));
							       
						   }
						   /******************************************************************************/						   
						   
						   /*******************************Hemoglobinopathy Result***********************/
						   
						   String hemoGlobinopathytRes="";
						  
						   if(Obj[20]!=null && !Obj[20].toString().equals("")){
							   hemoGlobinopathytRes=Obj[20].toString();
							   Obj[20]=getCodeListDescByPk(Long.parseLong(hemoGlobinopathytRes));
							       
						   }
						   /******************************************************************************/
						   
						   /*******************************Viability Result***********************/
						   
						   String viablility="";
						  
						   if(Obj[26]!=null && !Obj[26].toString().equals("")){
							   viablility=Obj[26].toString();
							   Obj[26]=getCodeListDescByPk(Long.parseLong(viablility));
							       
						   }
						   /******************************************************************************/
						   
					        //************************For Unlicensed Reason***********************************//
						   long tempSt=0l;
						   if(Obj[9]!=null && !Obj[9].toString().equals("")){
							   tempSt=Long.parseLong(Obj[9].toString());
						   }
						   
						   long pk_unlicensed=getCodeListPkByTypeAndSubtype(LICENCE_STATUS, UNLICENSE_REASON);
						   long temp=0l;
						   
						   if(Obj[10]!=null && !Obj[10].toString().equals("")){
							   temp=Long.parseLong(Obj[10].toString());
						   }
						
						   long pkval=0l;

						   if((Obj[28]!=null || Obj[38]!=null)){
							   SimpleDateFormat df = new SimpleDateFormat("MMM dd yyyy");
							   try {
								   if(Obj[28]!=null)
									   schshipdate = df.parse(Obj[28].toString());
								   if(Obj[38]!=null)
									   actshipdate = df.parse(Obj[38].toString());
							   } catch (ParseException e) {
								   // TODO Auto-generated catch block
								   e.printStackTrace();
							   }
						   }
						   if(tempSt==pk_unlicensed){
							   if(checkOldCord(schshipdate,actshipdate,codeLstDate)){
							   	   String unlicensReason="";
								   unlicensReason=fetchCommaSeperatedString(temp,LICENCE_STATUS);
								   Obj[10]=unlicensReason;
								   //System.out.println("\n\n\n\nModified Str:::::::::::::"+Obj[8]);
							   }else{
							   Obj[10]="";
							   }
						   }else{
							   Obj[10]="";
						   }
						   //******************************************************************************//
						   
						   /***************************For Licensure Status*********************************/
						   
						   String licStatus="";
						  
						   if(Obj[9]!=null && !Obj[9].toString().equals("")){
							   licStatus=Obj[9].toString();
							   Obj[9]=getCodeListDescByPk(Long.parseLong(licStatus));
							       
						   }
						   /******************************************************************************/
						   
						   //************************For Ineligible Reason***********************************//*
						   
						   long eligSt=0l;
						   temp=0l;
						   
						   if(Obj[11]!=null && !Obj[11].toString().equals("")){
							   eligSt=Long.parseLong(Obj[11].toString());
						   }
						   
						   long pk_ineligible=getCodeListPkByTypeAndSubtype(ELIGIBILITY_STATUS, INELIGIBLE_REASON);
						   long pk_incomplete=getCodeListPkByTypeAndSubtype(ELIGIBILITY_STATUS, INCOMPLETE_REASON);

						   if(Obj[12]!=null && !Obj[12].toString().equals("")){
							   temp=Long.parseLong(Obj[12].toString());
						   }
						   
						   pkval=0l;
						   
						   

						   if(eligSt==pk_ineligible || eligSt==pk_incomplete){
							   String inEligReason="";
							   inEligReason=fetchCommaSeperatedString(temp,ELIGIBILITY_STATUS);
							   Obj[12]=inEligReason;
							   //System.out.println("\n\n\n\nModified Str:::::::::::::"+Obj[10]);
						   }else{
							   Obj[12]="";
						   }
						   //******************************************************************************//*
						   
						   /***************************For Eligible Status*********************************/
						   
						   long EligStatus=0l;
						   
						   if(Obj[11]!=null && !Obj[11].toString().equals("")){
							   EligStatus=Long.parseLong(Obj[11].toString());
							   long eliDescription=getCodeListPkByTypeAndSubtype(ELIGIBILITY_STATUS, NOT_COLLECTED_TO_PRIOR_REASON);
							   long cordIdval=0l;
							   Boolean flag;
							   
							   //System.out.println("EligibStat::::"+EligStatus);
							   
							   if(EligStatus==eliDescription){
								   
								   if(checkOldCord(schshipdate,actshipdate,codeLstDate)){
									   Obj[11]=NOT_APPLICABLE_PRIOR_DATE;
									   //System.out.println("Text Changed");
								   }else{
									   Obj[11]=getCodeListDescByPk(EligStatus);
								   }
							   }else{
								   Obj[11]=getCodeListDescByPk(EligStatus);
							   }
						   }
						   /******************************************************************************/

						   //*****************************Race Description*********************************//
						   
						   String pkcord="";
						   long cordid=0l;
						   String raceDesc="";
						   
						   if(Obj[0]!=null && !Obj[0].toString().equals("")){
							   
							   pkcord=Obj[0].toString();
							   cordid=Long.parseLong(pkcord);
							   raceDesc=fetchCommaSeperatedString(cordid,RACE);
							   Obj[14]=raceDesc;
							   
						   }
						   /******************************************************************************/
						   
						   
						   
						   
						   /***************************For Request Type*********************************/
						   
						   long reqType=0l;
						   long pk_ctReq=getCodeListPkByTypeAndSubtype(ORDER_TYPE, CT);
						   String samplAtlab="";
						  
						   if(Obj[30]!=null && !Obj[30].toString().equals("")){
							   reqType=Long.parseLong(Obj[30].toString());
							   
							   if(reqType==pk_ctReq){
								   
								   if(Obj[32]!=null && !Obj[32].toString().equals("")){
									   samplAtlab=Obj[32].toString();
									   
									   if(samplAtlab.equals("Y")){
										   Obj[30]=CT_SHIP_SAMPLE_AT_LAB;
									   }else if (samplAtlab.equals("N")) {
										   Obj[30]=CT_SHIP_SAMPLE;
									   } 
								   }
							   }else{
								   Obj[30]=getCodeListDescByPk(reqType);
							   }
							       
						   }
						   /******************************************************************************/
						   
						   /***************************For Funding Category*********************************/
						   
						   String fundCat="";
						  
						   if(Obj[17]!=null && !Obj[17].toString().equals("")){
							   fundCat=Obj[17].toString();
							   Obj[17]=getCodeListDescByPk(Long.parseLong(fundCat));
							       
						   }
						   /******************************************************************************/
						   
						   
						   //System.out.println("\n\n\n\n\n\n\n\n\nModified List::::::::::::::::::::::::::::::::::"+Obj);
						   
					   }
					   
					   
					   
				   }
				   
				   
				   
			       //tempLst2 = vdaController.getQueryBuilderViewData(sqlHead, null);
			   }else{
				   
				   if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(MODULE_VIEW_CLINICAL)){//here
					   
					   tempLst1 = cbuController.getCordListAsobject(sqlHead.toString(), paginationSearch);
			          	 StringBuilder countRecSql = new StringBuilder("Select count(*) from ( "+sqlHead.toString()+" )");
						 tempLst2 = cbuController.getCordListAsobject(countRecSql.toString(), null);
						 
					   		if(tempLst1!=null){
					   			setAaData(tempLst1);
					   			getRequest().setAttribute("cordInfoList",tempLst1);
					   }
					   if(tempLst2!=null && tempLst2.size()>0)
						  setiTotalDisplayRecords(Long.parseLong(String.valueOf(tempLst2.size())));
					   if(getCbuCount()!=null)
						   setiTotalRecords(getCbuCount().longValue());
					   request.setAttribute("paginateSearch", paginationSearch);
					   
					   return;
					   
				   }
				   tempLst1 = cbuController.getCordListAsobject(sqlHead.toString(), paginationSearch);	
				   tempLst2 = cbuController.getCordListAsobject(sqlHead.toString(), null);
			   }
			   if(tempLst1!=null){
			     setAaData(tempLst1);
			     getRequest().setAttribute("cordInfoList",tempLst1);
			   }
			   if(tempLst2!=null && tempLst2.size()>0)
				  setiTotalDisplayRecords(Long.parseLong(String.valueOf(tempLst2.size())));
			   if(getCbuCount()!=null)
				   setiTotalRecords(getCbuCount().longValue());
			   request.setAttribute("paginateSearch", paginationSearch);
		}catch (Exception e) {
			e.printStackTrace();
		}
    	/*******************Getting Paginated Whole List***********************//*
    	i = 1;
    	tempLst1 = fetchLookupData(condition,orderBy,i,sStart1,sAmount1);
    	
    	System.out.println("setLookupData  ::: tempList1.size.."+tempLst1.size());
    	
    	*//**********************************************************************//*
    	i = 2;
    	*//*******************Getting Paginated List Size***********************//*
    	tempLst3 = fetchLookupData(condition,orderBy,i,sStart1,sAmount1);
    	BigDecimal totCount1 = (BigDecimal)tempLst3.get(0);
    	pagLstSizeStr = totCount1.toString();
    	pagLstSize = Long.parseLong(pagLstSizeStr);
    	System.out.println("setLookupData  ::: tempList3.size.. alert "+tempLst3.size()+" size1..."+pagLstSize);
    	*//**********************************************************************//*
    	
    	*//*******************Getting Whole List Size without pagination***********************//*
    	i = 3;
    	condition = "";
    	tempLst2 = fetchLookupData(condition,orderBy,i,sStart1,sAmount1);
    	BigDecimal totCount2 = (BigDecimal)tempLst2.get(0);
    	unpagLstSizeStr = totCount2.toString();
    	unpagLstSize = Long.parseLong(unpagLstSizeStr);
    	System.out.println("setLookupData   :::::: tempList2.size..alert::::::"+tempLst2.size()+"::::::size2..."+unpagLstSize);
    	*//************************************************************************************//*
    	
    	setAaData(tempLst1);
        setiTotalRecords(unpagLstSize);
		setiTotalDisplayRecords(pagLstSize);*/
    }
    
    @SuppressWarnings("unchecked")
	public void setCordImportData(){
    	
    	
    	int i;  
    	List list = new ArrayList();
    	String index="1";
    	String dir="desc";
    	if(request.getParameter("iSortCol_0") != null && !request.getParameter("iSortCol_0").toString().equals("")){
    		if(!request.getParameter("iSortCol_0").toString().equals("0")){
    			index=request.getParameter("iSortCol_0").toString();
    	    	dir=getSdir();
    		}
        	orderBy = serverOrderBy.FetchCordImport_OrderBy(index,dir);
        }
    	
    	if(request.getParameter("sSearch") != null && !request.getParameter("sSearch").toString().trim().equals("")){
        	condition = sergloGlobalSearch.FetchGlobalSearchCordImport(request.getParameter("sSearch").toString());
        }
    	
    	if(request.getParameter("otherThanAll")!=null && request.getParameter("otherThanAll").equals("1")){
    		setOtherthanAll(request.getParameter("otherThanAll").toString());
    	}
    	
    	
    	/*******************Getting Paginated Whole List***********************/
    	i = 1;
    	list = fetchCordImportData(condition,orderBy,i,sStart1,sAmount1);
    	setListCordImportHistory(list);
    	
    	//System.out.println("fetchCordImportData  ::: tempList1.size.."+tempLst1.size());
    	
    	/**********************************************************************/
    	
    	if(!getOtherthanAll().equals("1")){
		    	
    			i = 2;
		    	/*******************Getting Paginated List Size***********************/
		    	tempLst3 = fetchCordImportData(condition,orderBy,i,sStart1,sAmount1);
		    	BigDecimal totCount1 = (BigDecimal)tempLst3.get(0);
		    	pagLstSizeStr = totCount1.toString();
		    	pagLstSize= Long.parseLong(pagLstSizeStr);

		    	//System.out.println("fetchCordImportData  ::: tempList3.size..  "+tempLst3.size()+" size1..."+pagLstSize);
		    	/**********************************************************************/
		    	
		    	
		    	/*******************Getting Whole List Size without pagination***********************/
		    	i = 3;
		    	condition = "";
		    	tempLst2 = fetchCordImportData(condition,orderBy,i,sStart1,sAmount1);
		    	BigDecimal totCount2 = (BigDecimal)tempLst2.get(0);
		    	unpagLstSizeStr = totCount2.toString();
		    	unpagLstSize = Long.parseLong(unpagLstSizeStr);
		    	//System.out.println("fetchCordImportData   :::::: tempList2.size..::::::"+tempLst2.size()+"::::::size2..."+unpagLstSize);
		    	/************************************************************************************/
		    	
		    	setiTotalRecords(unpagLstSize);
				setiTotalDisplayRecords(pagLstSize);
    	}
    	
    	if(request.getParameter("otherThanAll")!=null && request.getParameter("otherThanAll").equals("1")){
	    	setiTotalRecords(Long.parseLong(request.getParameter("iShowRows").toString()));
			setiTotalDisplayRecords(Long.parseLong(request.getParameter("iShowRows").toString()));
    	}
    	
    	
    	//System.out.println("\n\n\n\n\n\n\n\n\nlast line of the function....................................");
    	
        
    	
    	
    }
    public List fetchCordImportData(String condition,String orderBy,int i,int Start,int End){
    	
    	UserJB user = (UserJB)getSession(getRequest()).getAttribute(VelosGarudaConstants.GARUDA_USER);
	    if(user!=null){
			   Long userSiteId = Long.parseLong(user.getUserSiteId().toString());
			   request.setAttribute("userSiteId", userSiteId);
			   Long userId=(long)user.getUserId();
			   request.setAttribute("userId", userId);
		}

	    List tlistCordImportHistory=new ArrayList();

    	try{
		   
		   
		   if(i==1){
			   
			   if(request.getParameter("otherThanAll")!=null && request.getParameter("otherThanAll").equals("1")){
				   paginationReqScopeData(paginationSearch);
			   }else{
				   paginationSearch.setiPageNo(Start);
				   paginationSearch.setiShowRows(End);
			   }

			   tlistCordImportHistory = new CordImportController().getCordImportHistoryJson(null,((Integer)user.getUserId()).longValue(), paginationSearch,condition,orderBy);
		   }
		   
		  if(i==2){
			    tlistCordImportHistory = new CordImportController().getCordImportHistoryCount(((Integer)user.getUserId()).longValue(),condition);
		   }
		   if(i==3){
			   condition="";
			   tlistCordImportHistory = new CordImportController().getCordImportHistoryCount(((Integer)user.getUserId()).longValue(),condition);
		   }
		   
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return tlistCordImportHistory;
    	
    }
    
    public List fetchLookupData(String condition,String orderBy,int i,int Start,int End){
    	List<Object> cdrObjects = new ArrayList<Object>();
		//System.out.println(" fetchLookupData::::::::condition:::"+condition);
		//System.out.println("fetchLookupData:::orderBy:::"+orderBy);
		//System.out.println("getPaginateModule:::::::"+getPaginateModule());
		
		//System.out.println("getCbuid:::::::::::::::::::"+getCbuid());
		//System.out.println("cbuaction.getCdrCbuPojo().getCdrCbuId()::::::::::::::::::::::::"+cbuaction.getCdrCbuPojo().getCdrCbuId());
  	   try{
  		  //System.out.println("INside fetchLookupData");
  		   UserJB user = (UserJB)getSession(getRequest()).getAttribute(VelosGarudaConstants.GARUDA_USER);
  		   if(user!=null){
  			   Long userSiteId = Long.parseLong(user.getUserSiteId().toString());
  			   request.setAttribute("userSiteId", userSiteId);
  			   Long userId=(long)user.getUserId();
  			   request.setAttribute("userId", userId);
  		   }
  		 	StringBuilder sqlHead = new StringBuilder();		   
		    if(i==1){
				//System.out.println("\n\n\n\n\ninside i=0...");
				if(getPaginateModule()!=null && getPaginateModule().equalsIgnoreCase(MODULE_VIEW_CLINICAL)){
				    sqlHead.append( "SELECT Distinct(C.PK_CORD),C.CORD_REGISTRY_ID,C.registry_maternal_id,C.CORD_LOCAL_CBU_ID,C.maternal_local_id,C.CORD_ISBI_DIN_CODE,SITE.SITE_NAME,PAT.PATIENTID" );
		          	sqlHead.append( cbuaction.constructSqlsForSearch(user, null)+condition);
				}else if(getPaginateModule().equalsIgnoreCase(MODULE_VIEW_LOOKUP)){
				   sqlHead.append( "SELECT Distinct(C.PK_CORD),C.CORD_REGISTRY_ID,C.registry_maternal_id,C.CORD_LOCAL_CBU_ID,C.maternal_local_id,C.CORD_ISBI_DIN_CODE,site.SITE_NAME,pat.RECEIPANT_ID");
				   sqlHead.append( cbuaction.constructSqlsForSearch(user, null)+condition);
				}else{
				   sqlHead.append( "SELECT Distinct(C.PK_CORD),C.CORD_REGISTRY_ID,C.CORD_ENTRY_PROGRESS,C.created_on");
				   sqlHead.append( cbuaction.constructSqlsForSearch(user, null)+condition);
				}
				
				if(!orderBy.equals("")){
					//sqlHead += orderBy;
				}else{
					//sqlHead	+= " order by C.CORD_REGISTRY_ID asc";
				}
				paginationSearch.setiPageNo(Start);
				paginationSearch.setiShowRows(End);
				cdrObjects = cbuController.getCordListAsobject(sqlHead.toString(), paginationSearch);
				//System.out.println("\n\n\n\n\nend of i=0...");
			}
		    if(i==2){
				//System.out.println("\n\n\n\n\ninside i=1...");
				sqlHead = new StringBuilder("SELECT count(Distinct(c.pk_cord)) ");
	          	sqlHead.append( cbuaction.constructSqlsForSearch(user, null)+condition);
	          	cdrObjects = cbuController.getCordListAsobject(sqlHead.toString(), null);
				//System.out.println("\n\n\n\n\nend of i=1...");
			}
			if(i==3){
				//System.out.println("\n\n\n\n\ninside i=1...");
				sqlHead = new StringBuilder("SELECT count(Distinct(c.pk_cord)) ");
	          	sqlHead.append( cbuaction.constructSqlsForSearch(user, null)+condition);
	          	cdrObjects = cbuController.getCordListAsobject(sqlHead.toString(), null);
				//System.out.println("\n\n\n\n\nend of i=1...");
			}
			if(cdrObjects!=null && cdrObjects.size()>0){
  		    	getRequest().setAttribute("cbuDeferlist",cdrObjects);
  		    }
  	   }catch(Exception ex){
  		   ex.printStackTrace();
  	   }
  	return cdrObjects;
     }
    
    public void setOrdersPs(){
    	
    	int i;
    	
    	//System.out.println("Inside setPendingOrders");
    	
    	if(request.getParameter("iSortCol_0")!=null && !request.getParameter("iSortCol_0").toString().equals("") &&  !request.getParameter("iSortCol_0").toString().equals("0")){
        	orderBy=serverOrderBy.FetchPSOrders_OrderBy(request.getParameter("iSortCol_0").toString(),getSdir());
        }
    	
    	if(request.getParameter("sSearch")!=null && !request.getParameter("sSearch").toString().trim().equals("")){
        	condition=sergloGlobalSearch.FetchGlobalSearchPsOrders(request.getParameter("sSearch").toString());
        }
    	
    	
    	/*******************Getting Paginated Whole List***********************/
    	i=0;
    	tempLst1=FetchPSOrdersData(condition,orderBy,i,sStart1,sAmount1);
    	
    	//System.out.println("tempList1.size.."+tempLst1.size());
    	
    	/**********************************************************************/
    	
    	
    	if(request.getParameter("otherThanAll")!=null && request.getParameter("otherThanAll").equals("1")){
    		setOtherthanAll(request.getParameter("otherThanAll").toString());
    	}
    	
    	
    	if(!getOtherthanAll().equals("1")){
    		
			    	/*************************Execute Only for All Option******************/
    		
    				/*******************Getting Paginated List Size***********************/
			    	i=1;
			    	tempLst3=FetchPSOrdersData(condition,orderBy,i,sStart1,sAmount1);
			    	BigDecimal totCount1 = (BigDecimal)tempLst3.get(0);
			    	pagLstSizeStr = totCount1.toString();
			    	pagLstSize=Long.parseLong(pagLstSizeStr);
			    	//System.out.println("tempList3.size.. alert"+tempLst3.size()+"size1..."+pagLstSize);
			    	/**********************************************************************/
			    	
			    	
			    	/*******************Getting Whole List Size without pagination***********************/
			    	i=2;
			    	condition="";
			    	tempLst2=FetchPSOrdersData(condition,orderBy,i,sStart1,sAmount1);
			    	BigDecimal totCount2 = (BigDecimal)tempLst2.get(0);
			    	unpagLstSizeStr = totCount2.toString();
			    	unpagLstSize=Long.parseLong(unpagLstSizeStr);
			    	//System.out.println("tempList2.size..alert"+tempLst2.size()+"size2..."+unpagLstSize);
			    	/************************************************************************************/
    	
    	
    	setAaData(tempLst1);
        setiTotalRecords(unpagLstSize);
		setiTotalDisplayRecords(pagLstSize);
		
    	}
    	
    	if(getOtherthanAll().equals("1")){
    		
    		/*************************Execute Only for Other than All Option******************/
    		setAaData(tempLst1);
    		setiTotalRecords(Long.parseLong(request.getParameter("iShowRows").toString()));
    		setiTotalDisplayRecords(Long.parseLong(request.getParameter("iShowRows").toString()));
    	}
    	
    }
    
   public void setAlertsLp(){
    	
    	int i;
    	
    	//System.out.println("Inside setAlertsLp");
    	
    	if(request.getParameter("iSortCol_0")!=null && !request.getParameter("iSortCol_0").toString().equals("") &&  !request.getParameter("iSortCol_0").toString().equals("0")){
        	orderBy=serverOrderBy.FetchAlerts_OrderBy(request.getParameter("iSortCol_0").toString(),getSdir());
        }
    	
    	if(request.getParameter("sSearch")!=null && !request.getParameter("sSearch").toString().trim().equals("")){
        	condition=sergloGlobalSearch.FetchGlobalSearchAlerts(request.getParameter("sSearch").toString());
        }
    	
    	if(request.getParameter("otherThanAll")!=null && request.getParameter("otherThanAll").equals("1")){
    		setOtherthanAll(request.getParameter("otherThanAll").toString());
    	}
    	
    	
    	/*******************Getting Paginated Whole List***********************/
    	i=0;
    	tempLst1=FetchAlertsData(condition,orderBy,i,sStart1,sAmount1);
    	
    	//System.out.println("tempList1.size.."+tempLst1.size());
    	
    	/**********************************************************************/
    	
    	
    	if(!getOtherthanAll().equals("1")){
    	/*******************Getting Paginated List Size***********************/
    	i=1;
    	tempLst3=FetchAlertsData(condition,orderBy,i,sStart1,sAmount1);
    	BigDecimal totCount1 = (BigDecimal)tempLst3.get(0);
    	pagLstSizeStr = totCount1.toString();
    	pagLstSize=Long.parseLong(pagLstSizeStr);
    	//System.out.println("tempList3.size.. alert"+tempLst3.size()+"size1..."+pagLstSize);
    	/**********************************************************************/
    	
    	
    	/*******************Getting Whole List Size without pagination***********************/
    	i=2;
    	condition="";
    	tempLst2=FetchAlertsData(condition,orderBy,i,sStart1,sAmount1);
    	BigDecimal totCount2 = (BigDecimal)tempLst2.get(0);
    	unpagLstSizeStr = totCount2.toString();
    	unpagLstSize=Long.parseLong(unpagLstSizeStr);
    	//System.out.println("tempList2.size..alert"+tempLst2.size()+"size2..."+unpagLstSize);
    	/************************************************************************************/
    	
    	
    	//setAaData(tempLst1);
        setiTotalRecords(unpagLstSize);
		setiTotalDisplayRecords(pagLstSize);
		
    	}
    	if(getOtherthanAll().equals("1")){
    		
    		setiTotalRecords(Long.parseLong(request.getParameter("iShowRows").toString()));
    		setiTotalDisplayRecords(Long.parseLong(request.getParameter("iShowRows").toString()));
    	}
    	
    	
    }
    public void setWorkflowLp(){
    	
    	int i;
    	
    	if(request.getParameter("iSortCol_0")!=null && !request.getParameter("iSortCol_0").toString().equals("") &&  !request.getParameter("iSortCol_0").toString().equals("0")){
        	orderBy=serverOrderBy.FetchWorkflow_OrderBy(request.getParameter("iSortCol_0").toString(),getSdir());
        }
    	
    	if(request.getParameter("sSearch")!=null && !request.getParameter("sSearch").toString().trim().equals("")){
        	condition=sergloGlobalSearch.FetchGlobalSearchWorflow(request.getParameter("sSearch").toString());
        }
    	
    	if(request.getParameter("otherThanAll")!=null && request.getParameter("otherThanAll").equals("1")){
    		setOtherthanAll(request.getParameter("otherThanAll").toString());
    	}
    	
    	/*******************Getting Paginated Whole List***********************/
    	i=0;
    	tempLst1=FetchWorkflowData(condition,orderBy,i,sStart1,sAmount1);
    	
    	//System.out.println("tempList1.size.."+tempLst1.size());
    	
    	/**********************************************************************/
    	
    	
    	if(!getOtherthanAll().equals("1")){
    	/*******************Getting Paginated List Size***********************/
    	i=1;
    	tempLst3=FetchWorkflowData(condition,orderBy,i,sStart1,sAmount1);
    	BigDecimal totCount1 = (BigDecimal)tempLst3.get(0);
    	pagLstSizeStr = totCount1.toString();
    	pagLstSize=Long.parseLong(pagLstSizeStr);
    	//System.out.println("tempList3.size.."+tempLst3.size()+"size1..."+pagLstSize);
    	/**********************************************************************/
    	
    	
    	/*******************Getting Whole List Size without pagination***********************/
    	i=2;
    	condition="";
    	tempLst2=FetchWorkflowData(condition,orderBy,i,sStart1,sAmount1);
    	BigDecimal totCount2 = (BigDecimal)tempLst2.get(0);
    	unpagLstSizeStr = totCount2.toString();
    	unpagLstSize=Long.parseLong(unpagLstSizeStr);
    	//System.out.println("tempList2.size.."+tempLst2.size()+"size2..."+unpagLstSize);
    	/************************************************************************************/
    	
    	
    	//setAaData(tempLst1);
        setiTotalRecords(unpagLstSize);
		setiTotalDisplayRecords(pagLstSize);
    	}
    	
    	
    	if(getOtherthanAll().equals("1")){
    		setiTotalRecords(Long.parseLong(request.getParameter("iShowRows").toString()));
    		setiTotalDisplayRecords(Long.parseLong(request.getParameter("iShowRows").toString()));
    	}
    	
    	
    }
    public void setOrdersLp(){
    	
    	int i;
    	
    	if(request.getParameter("iSortCol_0")!=null && !request.getParameter("iSortCol_0").toString().equals("") &&  !request.getParameter("iSortCol_0").toString().equals("0")){
        	orderBy=serverOrderBy.FetchOrders_OrderBy(request.getParameter("iSortCol_0").toString(),getSdir());
        }
    	
    	if(request.getParameter("sSearch")!=null && !request.getParameter("sSearch").toString().trim().equals("")){
        	condition=sergloGlobalSearch.FetchGlobalSearchOrders(request.getParameter("sSearch").toString());
        }
    	
    	if(request.getParameter("otherThanAll")!=null && request.getParameter("otherThanAll").equals("1")){
    		setOtherthanAll(request.getParameter("otherThanAll").toString());
    	}
    	
    	
    	/*******************Getting Paginated Whole List***********************/
    	i=0;
    	tempLst1=FetchOrdersData(condition,orderBy,i,sStart1,sAmount1);
    	
    	//System.out.println("tempList1.size.."+tempLst1.size());
    	
    	/**********************************************************************/
    	
    	if(!getOtherthanAll().equals("1")){
    	
    	/*******************Getting Paginated List Size***********************/
    	i=1;
    	tempLst3=FetchOrdersData(condition,orderBy,i,sStart1,sAmount1);
    	BigDecimal totCount1 = (BigDecimal)tempLst3.get(0);
    	pagLstSizeStr = totCount1.toString();
    	pagLstSize=Long.parseLong(pagLstSizeStr);
    	//System.out.println("tempList3.size.."+tempLst3.size()+"size1..."+pagLstSize);
    	/**********************************************************************/
    	
    	
    	/*******************Getting Whole List Size without pagination***********************/
    	i=2;
    	condition="";
    	tempLst2=FetchOrdersData(condition,orderBy,i,sStart1,sAmount1);
    	BigDecimal totCount2 = (BigDecimal)tempLst2.get(0);
    	unpagLstSizeStr = totCount2.toString();
    	unpagLstSize=Long.parseLong(unpagLstSizeStr);
    	//System.out.println("tempList2.size.."+tempLst2.size()+"size2..."+unpagLstSize);
    	/************************************************************************************/
    	
    	setAaData(tempLst1);
        setiTotalRecords(unpagLstSize);
		setiTotalDisplayRecords(pagLstSize);
		
    	}
    	if(getOtherthanAll().equals("1")){
    		
    		/*************************Execute Only for Other than All Option******************/
    		setAaData(tempLst1);
    		setiTotalRecords(Long.parseLong(request.getParameter("iShowRows").toString()));
    		setiTotalDisplayRecords(Long.parseLong(request.getParameter("iShowRows").toString()));
    	}
    	
    }
    public List FetchPsNotAvailData(String condition,String orderBy,int i,int Start,int End){
    	
    	
    	UserJB user = (UserJB)getRequest().getSession().getAttribute(VelosGarudaConstants.GARUDA_USER);
		Long userId = (long)user.getUserId();
		String nacbuCondition="";
		String nacbuOrderBy="";
		String condi1="";
		String fksiteId=request.getParameter("pkSiteId").toString();
		int allEntriesflag=0;
		List naCBULst=new ArrayList();
		
		
		try{
		
					if(request.getParameter("allEntries")!=null && !request.getParameter("allEntries").toString().equals("") && request.getParameter("allEntries").toString().equals("ALL")){
						//log.debug("\n\n\n\n\n\n\n\n\n allEntriesflag"+request.getParameter("allEntries").toString());
						allEntriesflag=1;
					}
					
					if(fksiteId.equals(ALL_SITE)){
						
						fksiteId=getUserSiteString(userId,GET_DATA);
						
					}
					
					condi1=openorderaction.getNACBUCondition();
					
			
					
					if(allEntriesflag==1){
						
						if(i==0){
							//System.out.println("\n\n\n\n\ninside i=0..."+Start+":Start:"+End+":End");
							if(request.getParameter("otherThanAll")!=null && request.getParameter("otherThanAll").equals("1")){
								//System.out.println("inside expected....");
								paginationReqScopeData(paginationSearch);
							}else{
								//System.out.println("For all....");
								paginationSearch.setiPageNo(Start);
								paginationSearch.setiShowRows(End);
							}
							nacbuCondition=condi1+condition;
							nacbuOrderBy=orderBy;
							naCBULst=linkController.notAvailCbu(paginationSearch,nacbuCondition,1,fksiteId,nacbuOrderBy,userId);
							//System.out.println("\n\n\n\n\nend of i=0...nacbu list size"+naCBULst.size());
						}
						if(i==1){
							//System.out.println("\n\n\n\n\ninside i=1...");
							nacbuCondition=condi1+condition;
							naCBULst=linkController.notAvailCbu(paginationSearch,nacbuCondition,2,fksiteId,nacbuOrderBy,userId);
							//System.out.println("\n\n\n\n\nend of i=1...");
							
						}
						if(i==2){
							//System.out.println("\n\n\n\n\ninside i=2...");
							nacbuCondition=condi1;
							naCBULst=linkController.notAvailCbu(paginationSearch,nacbuCondition,2,fksiteId,nacbuOrderBy,userId);
							//System.out.println("\n\n\n\n\nend of i=2...");
							
						}
					
						
					}
					//setAssignUsrLst(userList(GET_ALL_FOR_CBB,fksiteId));
		
	}catch(Exception e){
		e.printStackTrace();
	}
	return naCBULst;
	
    }
    public List FetchOrdersData(String condition,String orderBy,int i,int Start,int End){
    	List tempOrd=new ArrayList();
    	

    		
    		try{
    			UserJB user = (UserJB)getRequest().getSession().getAttribute(VelosGarudaConstants.GARUDA_USER);
    			Long userId = (long)user.getUserId();
    			request.setAttribute("userId", userId);
    			String primaryOrg = user.getUserSiteId();
 
    			//setUserPrimaryOrg(primaryOrg);
    			String site="";
    			String ordersCondition="";
    			String orderByCondition="";
    			orderByCondition=orderBy;
    			
    			List tempLst=new ArrayList();
    			int allEntriesflag=0;
    			int showEntriesSize=0;
    			int refreshAction=0;
    			int iTotalRows1;
    			
    			
    			
    			if(request.getParameter("pksiteId")!=null){
    				site=request.getParameter("pksiteId").toString();
    				refreshAction=1;
    			}else if(request.getSession().getValue("lastCBBlp")!=null && !request.getSession().getValue("lastCBBlp").equals("")){
    				site=request.getSession().getValue("lastCBBlp").toString();
    			}else{
    				site=user.getUserSiteId();
    			}
    			
    			
    			
    			
    			
    			if(refreshAction==0  && request.getSession().getValue("v_Ord_fval")!=null && !request.getSession().getValue("v_Ord_fval").equals("")){
    				cbuaction.resetOldData();
    				ordersCondition=cbuaction.getOrdersConditionFromSession();
    			}
    			
    			//System.out.println("\n\n\n\n\norder condition in landing page....."+request.getParameter("allEntries"));
    			
    			if(request.getParameter("allEntries")!=null && !request.getParameter("allEntries").toString().equals("")){
    				if(request.getParameter("allEntries").toString().equals("ALL")){
    					allEntriesflag=1;
    				}
    				
    			}else if(request.getSession().getValue("v_Ord_SE_txt")!=null && !request.getSession().getValue("v_Ord_SE_txt").equals("")){	
    				
    				if(request.getSession().getValue("v_Ord_SE_txt").equals("ALL")){
    					allEntriesflag=1;
    					showEntriesSize=100;
    				}else{
    					showEntriesSize=Integer.parseInt(request.getSession().getValue("v_Ord_SE_txt").toString());
    				}
    				
    			}
    			
    			
    			//System.out.println("\n\n\n\n\nallEntriesflag....."+allEntriesflag);
    			
    			if(site.equals(ALL_SITE)){
    				
    				site=getUserSiteString(userId,GET_DATA);
    				
    			}
    			
    			//System.out.println("pksite::::"+site);
    			
    			if(orderByCondition.equals("")){
    				//orderByCondition=" order by decode(f_codelst_desc(ord.order_status),'New','A','Awaiting HLA Results','B','Cord Shipped','C','HLA Results Received','D','Reserved','E','Resolved','F','Shipment Scheduled','G','Completed','H') ";
    			}
    			
    			//System.out.println("orderByCondition::::"+orderByCondition);
    			
    			if(allEntriesflag==1){
    				
    				if(i==0){
    					//System.out.println("\n\n\n\n\ninside i=0...");
    					if(request.getParameter("otherThanAll")!=null && request.getParameter("otherThanAll").equals("1")){
    						paginationSessionData(paginationSearch,0,MODULE_PENDING);
    					}else{
    						paginationSearch.setiPageNo(Start);
    						paginationSearch.setiShowRows(End);
    					}
    					ordersCondition=cbuaction.getOrdersCondition()+condition;
    					tempOrd=linkController.getOrders(paginationSearch,1,userId,site,ordersCondition,orderByCondition);
    					//System.out.println("\n\n\n\n\nend of i=0...");
    				}
    				if(i==1){
    					//System.out.println("\n\n\n\n\ninside i=1...");
    					ordersCondition=cbuaction.getOrdersCondition()+condition;
    					tempOrd=linkController.getOrders(paginationSearch,2,userId,site,ordersCondition,orderByCondition);
    					//System.out.println("\n\n\n\n\nend of i=1...");
    					
    				}
    				if(i==2){
    					//System.out.println("\n\n\n\n\ninside i=2...");
    					ordersCondition=cbuaction.getOrdersCondition();
    					tempOrd=linkController.getOrders(paginationSearch,2,userId,site,ordersCondition,orderByCondition);
    					//System.out.println("\n\n\n\n\nend of i=2...");
    					
    				}
    				
    				
    			}else{
    				tempOrd=linkController.getOrders(paginationSearch,2,userId,site,ordersCondition,orderByCondition);
    				Object obj=tempLst.get(0);
    				//log.debug("value of list size:::::::::::::::"+obj.toString());
    				iTotalRows1=Integer.parseInt(obj.toString());
    				//log.debug("size ....."+iTotalRows1);
    				setAaData(linkController.getOrders(paginationSearch,1,userId,site,ordersCondition,orderByCondition));
    			}
    			
   			
    			cbuaction.setSessionDataLP();

    			
    			//log.debug("landing page orders End time::::::::::::::::::"+new Date());
    						
    		}catch(Exception e){
    			//log.debug("Error in orders Part.................................................");
    			e.printStackTrace();
    		}
    		
    		return tempOrd;
    		
    	}
    
    
    
    @SuppressWarnings("unchecked")
	public List FetchDocInfoAudit(String condition,String orderBy,int i,int Start,int End){
    	int allEntriesflag=0;
    	String pkCord= "";
    	List tempData=new ArrayList();
    	try{
    		
    		if(request.getParameter("allEntries")!=null && !request.getParameter("allEntries").toString().equals("") && request.getParameter("allEntries").toString().equals("ALL")){
    			allEntriesflag=1;
    		}
    		
    		if(request.getParameter("cordpk")!=null && !request.getParameter("cordpk").toString().equals("")){
    			pkCord=request.getParameter("cordpk").toString();
    		}
    		if(allEntriesflag==1){
				if(i==0){
					if(request.getParameter("otherThanAll")!=null && request.getParameter("otherThanAll").equals("1")){
						paginationReqScopeData(paginationSearch);
					}else{
						paginationSearch.setiPageNo(Start);
						paginationSearch.setiShowRows(End);
					}
					tempData=cbuunitcontroller.getRevokeUnitReportLst(pkCord,paginationSearch,1,condition,orderBy);
				}
				if(i==1){
					tempData=cbuunitcontroller.getRevokeUnitReportLst(pkCord,paginationSearch,0,condition,orderBy);
				}
				if(i==2){
					tempData=cbuunitcontroller.getRevokeUnitReportLst(pkCord,paginationSearch,0,condition,orderBy);
				}
			}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	return tempData;
    }
    
    
    
    
    @SuppressWarnings("unchecked")
	public List FetchCbuInfoAudit(String condition,String orderBy,int i,int Start,int End, String whichWidget){
    	int allEntriesflag=0;
    	Long pkCord=0l;
    	List tempData=new ArrayList();
    	try{
    		
    		if(request.getParameter("allEntries")!=null && !request.getParameter("allEntries").toString().equals("") && request.getParameter("allEntries").toString().equals("ALL")){
    			allEntriesflag=1;
    		}
    		
    		if(request.getParameter("cordpk")!=null && !request.getParameter("cordpk").toString().equals("")){
    			pkCord=Long.parseLong(request.getParameter("cordpk").toString());
    		}
    		
    		if(allEntriesflag==1){
				if(i==0){
					if(request.getParameter("otherThanAll")!=null && request.getParameter("otherThanAll").equals("1")){
						paginationReqScopeData(paginationSearch);
					}else{
						paginationSearch.setiPageNo(Start);
						paginationSearch.setiShowRows(End);
					}
					tempData=audittrailcontroller.getAuditRowDataByEntityId(pkCord,whichWidget,paginationSearch,1,condition,orderBy);
				}
				if(i==1){
					tempData=audittrailcontroller.getAuditRowDataByEntityId(pkCord,whichWidget,paginationSearch,0,condition,orderBy);
				}
				if(i==2){
					tempData=audittrailcontroller.getAuditRowDataByEntityId(pkCord,whichWidget,paginationSearch,0,condition,orderBy);
				}
			}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	return tempData;
    }
    
    
    @SuppressWarnings("unchecked")
	public List FetchCbuInfoAudit(String condition,String orderBy,int i,int Start,int End){
    	int allEntriesflag=0;
    	Long pkCord=0l;
    	List tempData=new ArrayList();
    	try{
    		
    		if(request.getParameter("allEntries")!=null && !request.getParameter("allEntries").toString().equals("") && request.getParameter("allEntries").toString().equals("ALL")){
    			allEntriesflag=1;
    		}
    		
    		if(request.getParameter("cordpk")!=null && !request.getParameter("cordpk").toString().equals("")){
    			pkCord=Long.parseLong(request.getParameter("cordpk").toString());
    		}
    		
    		if(allEntriesflag==1){
				if(i==0){
					if(request.getParameter("otherThanAll")!=null && request.getParameter("otherThanAll").equals("1")){
						paginationReqScopeData(paginationSearch);
					}else{
						paginationSearch.setiPageNo(Start);
						paginationSearch.setiShowRows(End);
					}
					tempData=audittrailcontroller.getAuditRowDataByEntityId(pkCord,VelosGarudaConstants.CORD_ROW_LEVEL,paginationSearch,1,condition,orderBy);
				}
				if(i==1){
					tempData=audittrailcontroller.getAuditRowDataByEntityId(pkCord,VelosGarudaConstants.CORD_ROW_LEVEL,paginationSearch,0,condition,orderBy);
				}
				if(i==2){
					tempData=audittrailcontroller.getAuditRowDataByEntityId(pkCord,VelosGarudaConstants.CORD_ROW_LEVEL,paginationSearch,0,condition,orderBy);
				}
			}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	return tempData;
    }
    
    @SuppressWarnings("unchecked")
	public List FetchIdInfoAudit(String condition,String orderBy,int i,int Start,int End){
    	int allEntriesflag=0;
    	Long pkCord=0l;
    	List tempData=new ArrayList();
    	try{
    		
    		if(request.getParameter("allEntries")!=null && !request.getParameter("allEntries").toString().equals("") && request.getParameter("allEntries").toString().equals("ALL")){
    			allEntriesflag=1;
    		}
    		
    		if(request.getParameter("cordpk")!=null && !request.getParameter("cordpk").toString().equals("")){
    			pkCord=Long.parseLong(request.getParameter("cordpk").toString());
    		}
    		
    		if(allEntriesflag==1){
				if(i==0){
					if(request.getParameter("otherThanAll")!=null && request.getParameter("otherThanAll").equals("1")){
						paginationReqScopeData(paginationSearch);
					}else{
						paginationSearch.setiPageNo(Start);
						paginationSearch.setiShowRows(End);
					}
					tempData=audittrailcontroller.getAuditRowDataByEntityId(pkCord,VelosGarudaConstants.ID_ROW_LEVEL,paginationSearch,1,condition,orderBy);
				}
				if(i==1){
					tempData=audittrailcontroller.getAuditRowDataByEntityId(pkCord,VelosGarudaConstants.ID_ROW_LEVEL,paginationSearch,0,condition,orderBy);
				}
				if(i==2){
					tempData=audittrailcontroller.getAuditRowDataByEntityId(pkCord,VelosGarudaConstants.ID_ROW_LEVEL,paginationSearch,0,condition,orderBy);
				}
			}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	return tempData;
    }
    
    
    
    @SuppressWarnings("unchecked")
	public List FetchProcedureData(String condition,String orderBy,int i,int Start,int End){
    	
    	List<CBBProcedurePojo> cbbList = new ArrayList<CBBProcedurePojo>();
    	int allEntriesflag=0;
    	String procedureCondi="";
    	List<SitePojo> sites=new ArrayList<SitePojo>();
    	
    	
    	try{
    		
    		if(request.getParameter("allEntries")!=null && !request.getParameter("allEntries").toString().equals("") && request.getParameter("allEntries").toString().equals("ALL")){
    			//log.debug("\n\n\n\n\n\n\n\n\n allEntriesflag"+request.getParameter("allEntries").toString());
    			allEntriesflag=1;
    		}
    		
    		sites = cbbcontroller.getSites();
            String siteIds="'0'";
            Iterator<SitePojo> itr =getSites().iterator();
            while(itr.hasNext()){
            		siteIds=siteIds+",'"+itr.next().getSiteId()+"'";
            }
    		
    		
    		if(allEntriesflag==1){
				
				if(i==0){
					//System.out.println("\n\n\n\n\n\n\n\n\n\n\n\ninside i=0...");
					if(request.getParameter("otherThanAll")!=null && request.getParameter("otherThanAll").equals("1")){
						paginationReqScopeData(paginationSearch);
					}else{
						paginationSearch.setiPageNo(Start);
						paginationSearch.setiShowRows(End);
					}
					procedureCondi=cbbaction.getProcessingProcedureCondition()+condition;
					//System.out.println("\n\n\n\n\n\n\n\n\n\n\n\nCondition string in i=0..."+procedureCondi);
					cbbList=cbbcontroller.getCBBProcedureSort(null,siteIds,paginationSearch,1,procedureCondi,orderBy);
					//System.out.println("\n\n\n\n\nend of i=0...");
				}
				if(i==1){
					//System.out.println("\n\n\n\n\ninside i=1...");
					procedureCondi=cbbaction.getProcessingProcedureCondition()+condition;
					cbbList=cbbcontroller.getCBBProcedureCount(siteIds,procedureCondi,i);
					//System.out.println("\n\n\n\n\nend of i=1...");
					
				}
				if(i==2){
					//System.out.println("\n\n\n\n\ninside i=2...");
					procedureCondi=cbbaction.getProcessingProcedureCondition();
					cbbList=cbbcontroller.getCBBProcedureCount(siteIds,procedureCondi,i);
					//System.out.println("\n\n\n\n\nend of i=2...");
					
				}
				
				
			}/*else{
				tempOrd=cbbcontroller.getCBBProcedureSort(null,siteIds,paginationSearch,2,procedureCondi,orderBy);
				Object obj=tempLst.get(0);
				//log.debug("value of list size:::::::::::::::"+obj.toString());
				iTotalRows1=Integer.parseInt(obj.toString());
				//log.debug("size ....."+iTotalRows1);
				setAaData(linkController.getOrders(paginationSearch,1,userId,site,ordersCondition,orderByCondition));
			}*/
    		
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	
    	
    	return cbbList;
    	
    }
    public List FetchCBBProfileData(String condition,String orderBy,int i,int Start,int End){
    	
    	List<SitePojo> cbbList = null;
    	int allEntriesflag=0;
    	String procedureCondi="";
    	List<SitePojo> sites=new ArrayList<SitePojo>();
    	
    	
    	try{
    		
    		if(request.getParameter("allEntries")!=null && !request.getParameter("allEntries").toString().equals("") && request.getParameter("allEntries").toString().equals("ALL")){
    			//log.debug("\n\n\n\n\n\n\n\n\n allEntriesflag"+request.getParameter("allEntries").toString());
    			allEntriesflag=1;
    		}
    		
    		
    		if(allEntriesflag==1){
				
				if(i==0){
					//System.out.println("\n\n\n\n\n\n\n\n\n\n\n\ninside i=0...");
					if(request.getParameter("otherThanAll")!=null && request.getParameter("otherThanAll").equals("1")){
						paginationReqScopeData(paginationSearch);
					}else{
						paginationSearch.setiPageNo(Start);
						paginationSearch.setiShowRows(End);
					}
					//System.out.println("\n\n\n\n\n\n\n\n\n\n\n\nCondition string in i=0..."+condition+":orderBy:"+orderBy);
					cbbList=cbbcontroller.getSitesWithPaginationSort(paginationSearch,1,condition,orderBy);
					//System.out.println("\n\n\n\n\nend of i=0...");
				}
				if(i==1){
					//System.out.println("\n\n\n\n\ninside i=1...");
					cbbList=cbbcontroller.getSitesCount(paginationSearch,1,condition);
					//System.out.println("\n\n\n\n\nend of i=1...");
					
				}
				if(i==2){
					//System.out.println("\n\n\n\n\ninside i=2...");
					condition="";
					cbbList=cbbcontroller.getSitesCount(paginationSearch,1,condition);
					//System.out.println("\n\n\n\n\nend of i=2...");
					
				}
				
				
			}
    		
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	
    	
    	return cbbList;
    	
    }
    @SuppressWarnings("unchecked")
	public List FetchHistoryData(String condition,String orderBy,int i,int Start,int End){
    	
    	List tempLst = new ArrayList();
    	int allEntriesflag=0;
    	String cordId="";
    	String orderId="";
    	String cdruser="";
    	CdrCbuPojo cdrcbupojo=new CdrCbuPojo();
    	
    	
    	try{
    		
    		if(request.getParameter("allEntries")!=null && !request.getParameter("allEntries").toString().equals("") && request.getParameter("allEntries").toString().equals("ALL")){
    			//log.debug("\n\n\n\n\n\n\n\n\n allEntriesflag"+request.getParameter("allEntries").toString());
    			allEntriesflag=1;
    		}
    		
    		if(request.getParameter("pkcordId")!=null && !request.getParameter("pkcordId").equals("") && !request.getParameter("pkcordId").equals("undefined")){
    			cdrcbupojo.setCordID(Long.parseLong(request.getParameter("pkcordId")));
    			cdrcbupojo = cbuController.getCordInfoById(cdrcbupojo);
    			cordId=request.getParameter("pkcordId").toString();
			}	
    		
    		if(request.getParameter("pkorderid")!=null && !request.getParameter("pkorderid").equals("") && !request.getParameter("pkorderid").equals("undefined")){
				   orderId = request.getParameter("pkorderid").toString();
			 }
    		
    		cdruser=cbbcontroller.getCbbInfoBySiteId("USING_CDR", cdrcbupojo.getFkCbbId());
    		//String pkorder=request.getParameter("pkorderid");
    		
    		if(allEntriesflag==1){
				
				if(i==0){
					//System.out.println("\n\n\n\n\n\n\n\n\n\n\n\ninside i=0...");
					if(request.getParameter("otherThanAll")!=null && request.getParameter("otherThanAll").equals("1")){
						paginationReqScopeData(paginationSearch);
					}else{
						paginationSearch.setiPageNo(Start);
						paginationSearch.setiShowRows(End);
					}
					
					//System.out.println("\n\n\n\n\n\n\n\n\n\n\n\nCondition string in i=0..."+procedureCondi);
					tempLst=linkController.cbuHistory( cordId,orderId,paginationSearch,1,cdruser,condition,orderBy);
					//System.out.println("\n\n\n\n\nend of i=0...");
				}
				if(i==1){
					//System.out.println("\n\n\n\n\ninside i=1...");
					
					tempLst=linkController.cbuHistory( cordId,orderId,paginationSearch,0,cdruser,condition,orderBy);
					//System.out.println("\n\n\n\n\nend of i=1...");
					
				}
				if(i==2){
					//System.out.println("\n\n\n\n\ninside i=2...");
					condition="";
					tempLst=linkController.cbuHistory( cordId,orderId,paginationSearch,0,cdruser,condition,orderBy);
					//System.out.println("\n\n\n\n\nend of i=2...");
					
				}
				
				
			}
    		
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	
    	
    	return tempLst;
    	
    }
    public List FetchPSSIPData(String condition,String orderBy,int k,int Start,int End){
    	
    	List savedInprogLst=new ArrayList();
    	try{
    		
    		UserJB user = (UserJB)getRequest().getSession().getAttribute(VelosGarudaConstants.GARUDA_USER);
    		Long userId = (long)user.getUserId();
    		String SIPCondition="";
    		String sipOrderBy="";
    		String fksiteId=request.getParameter("pkSiteId").toString();
    		int allEntriesflag=0;
    		
    		
    		if(request.getParameter("allEntries")!=null && !request.getParameter("allEntries").toString().equals("") && request.getParameter("allEntries").toString().equals("ALL")){
    			//log.debug("\n\n\n\n\n\n\n\n\n allEntriesflag"+request.getParameter("allEntries").toString());
    			allEntriesflag=1;
    		}
    		
    		if(fksiteId.equals(ALL_SITE)){
    			
    			fksiteId=getUserSiteString(userId,GET_DATA);
    			
    		}
    		SIPCondition=openorderaction.getSIPCondition();
    	
    		
    		
    		if(allEntriesflag==1){
    			
    			if(k==0){
    				//System.out.println("\n\n\n\n\ninside i=0...");
    				if(request.getParameter("otherThanAll")!=null && request.getParameter("otherThanAll").equals("1")){
						//System.out.println("inside expected....");
						paginationReqScopeData(paginationSearch);
					}else{
						//System.out.println("For all....");
						paginationSearch.setiPageNo(Start);
						paginationSearch.setiShowRows(End);
					}
    				SIPCondition=SIPCondition+condition;
    				sipOrderBy=orderBy;
    				savedInprogLst=linkController.savedInprogress(paginationSearch,SIPCondition,userId,fksiteId,sipOrderBy,1);
    				//System.out.println("\n\n\n\n\nend of i=0...");
    			}
    			if(k==1){
    				//System.out.println("\n\n\n\n\ninside i=1...");
    				SIPCondition=SIPCondition+condition;
    				savedInprogLst=linkController.savedInprogress(paginationSearch,SIPCondition,userId,fksiteId,sipOrderBy,2);
    				//System.out.println("\n\n\n\n\nend of i=1...");
    				
    			}
    			if(k==2){
    				//System.out.println("\n\n\n\n\ninside i=2...");
    				SIPCondition=SIPCondition+condition;
    				savedInprogLst=linkController.savedInprogress(paginationSearch,SIPCondition,userId,fksiteId,sipOrderBy,2);
    				//System.out.println("\n\n\n\n\nend of i=2...");
    				
    			}
    			
    			
    		}
    		
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	
		
		return savedInprogLst;

    }
    public List FetchWorkflowData(String condition,String orderBy,int k,int Start,int End){
    	List tempOrd=new ArrayList();
    	List templst=new ArrayList();
    	String tmpordertype="";
		String tmporderid="";
		String tmppkOrder="";
		String tmpActivityName="";
		String tmpSampleAtLab="";
		List<TaskListPojo> li1=new ArrayList<TaskListPojo>();
    	int allEntriesflag=0;
    	List litemp=new ArrayList();

    		
    		try{
    			UserJB user = (UserJB)getRequest().getSession().getAttribute(VelosGarudaConstants.GARUDA_USER);
    			Long userId = (long)user.getUserId();
    			request.setAttribute("userId", userId);
    			String primaryOrg = user.getUserSiteId();
    			request.setAttribute("userId", userId);
    			
    			String site=request.getParameter("pksiteId").toString();
    			if(site.equals(ALL_SITE)){
    				
    				site=getUserSiteString(userId,GET_DATA);
    				
    			}
    			String wrkflw_condition="";
    			String wrkflw_OrdBy_Condition=orderBy;
    			
    			if(request.getParameter("allEntries")!=null && !request.getParameter("allEntries").toString().equals("") && request.getParameter("allEntries").toString().equals("ALL")){
    				allEntriesflag=1;
    			}
    			int iTotalRows =0;
    			
    			if(allEntriesflag==1){
    				
    				
    				if(k==0){
    					if(request.getParameter("otherThanAll")!=null && request.getParameter("otherThanAll").equals("1")){
    						paginationReqScopeData(paginationSearch);
    					}else{
    						paginationSearch.setiPageNo(Start);
    						paginationSearch.setiShowRows(End);
    					}
    					wrkflw_condition=cbuaction.getWorkflowConditions()+condition;
    					tempOrd=linkController.getWorkFlowNotification(paginationSearch,1,userId,site,wrkflw_condition,wrkflw_OrdBy_Condition);
    					//System.out.println("\n\n\n\n\nend of i=0...workflow");
    					
    					if(tempOrd!=null && tempOrd.size()>0){
    				        	
    				        	for(int l=0;l<tempOrd.size();l++){
    				    			
    								Object[] obj = (Object[])tempOrd.get(l);
    								TaskListPojo taskListPojo=new TaskListPojo();
    								if(obj[0]!=null && !obj[0].equals("")){
    									taskListPojo.setOrderType(obj[0].toString());
    								}
    								if(obj[1]!=null && !obj[1].equals("")){
    								taskListPojo.setOrderNumber(obj[1].toString());
    								}
    								if(obj[2]!=null && !obj[2].equals("")){
    									taskListPojo.setCordRegistryId(obj[2].toString());
    									taskListPojo.setRegId1(obj[2].toString());
    								}
    								if(obj[3]!=null && !obj[3].equals("")){
    								taskListPojo.setOrderDate(obj[3].toString());
    								}
    								if(obj[4]!=null && !obj[4].equals("")){
    								taskListPojo.setOrderStatus(obj[4].toString());
    								}
    								if(obj[5]!=null && !obj[5].equals("")){
    								taskListPojo.setActivityDesc(obj[5].toString());
    								}
    								if(obj[6]!=null && !obj[6].equals("")){
    								taskListPojo.setTaskCompflag(Long.parseLong(obj[6].toString()));
    								}
    								if(obj[7]!=null && !obj[7].equals("")){
    								taskListPojo.setPkOrder(Long.parseLong(obj[7].toString()));
    								}
    								if(obj[8]!=null && !obj[8].equals("")){
    								taskListPojo.setFkentityId(Long.parseLong(obj[8].toString()));
    								}
    								if(obj[9]!=null && !obj[9].equals("")){
    								taskListPojo.setActivityName(obj[9].toString());
    								}
    								if(obj[10]!=null && !obj[10].equals("")){
    									taskListPojo.setSampleAtLab(obj[10].toString());
    								}
    								if(obj[13]!=null && !obj[13].equals("")){
    									taskListPojo.setFksiteid(obj[13].toString());
    								}
    								workFlowNotiLst = getWorkFlowNotiLst();
    								workFlowNotiLst.add(taskListPojo);
    				        	}
    						
    							if(workFlowNotiLst!=null){
    									if(workFlowNotiLst.size()>0){
    										li1.add(workFlowNotiLst.get(0));
    										tmporderid=workFlowNotiLst.get(0).getOrderNumber();
    										tmpordertype=workFlowNotiLst.get(0).getOrderType();
    										tmppkOrder=workFlowNotiLst.get(0).getPkOrder().toString();
    										tmpActivityName=workFlowNotiLst.get(0).getActivityName();
    									}
    				        	
    							}
    							for(int i=0;i<workFlowNotiLst.size();i++){
    								TaskListPojo listPojo=new TaskListPojo();
    								if(workFlowNotiLst.get(i).getPkOrder().toString().equals(tmppkOrder) && workFlowNotiLst.get(i).getOrderType().equals(tmpordertype)){
    									listPojo.setOrderNumber("-");
    									listPojo.setOrderType(workFlowNotiLst.get(i).getOrderType());
    									listPojo.setActivityName(workFlowNotiLst.get(i).getActivityName());
    									listPojo.setCordRegistryId("-");
    									listPojo.setRegId1(workFlowNotiLst.get(i).getRegId1());
    									listPojo.setFkentityId(workFlowNotiLst.get(i).getFkentityId());
    									listPojo.setOrderDate("-");
    									listPojo.setOrderStatus("-");
    									listPojo.setPkOrder(workFlowNotiLst.get(i).getPkOrder());
    									listPojo.setTaskCompflag(workFlowNotiLst.get(i).getTaskCompflag());
    									listPojo.setFksiteid(workFlowNotiLst.get(i).getFksiteid());
    									if(tmpordertype.equals("OR") && workFlowNotiLst.get(i).getActivityName().equals("genpackageslipbar")){
    										listPojo.setActivityDesc("Create Shipment Packet");
    									}else{
    										listPojo.setActivityDesc(workFlowNotiLst.get(i).getActivityDesc());
    									}
    									tmporderid=workFlowNotiLst.get(i).getOrderNumber();
    									tmpordertype=workFlowNotiLst.get(i).getOrderType();
    									if(workFlowNotiLst.get(i).getSampleAtLab()!=null && workFlowNotiLst.get(i).getSampleAtLab().toString().equalsIgnoreCase("Y") && (workFlowNotiLst.get(i).getActivityName().toString().equalsIgnoreCase(TSK_SHIPMENT_INFO) || workFlowNotiLst.get(i).getActivityName().toString().equalsIgnoreCase(TSK_GEN_PACKAGE_SLIP))){
    										
    									}else{
    										li1.add(listPojo);
    									}
    									
    								}
    								else{
    									listPojo.setOrderNumber(workFlowNotiLst.get(i).getOrderNumber());
    									listPojo.setOrderType(workFlowNotiLst.get(i).getOrderType());
    									listPojo.setActivityName(workFlowNotiLst.get(i).getActivityName());
    									listPojo.setCordRegistryId(workFlowNotiLst.get(i).getCordRegistryId());
    									listPojo.setRegId1(workFlowNotiLst.get(i).getRegId1());
    									listPojo.setFkentityId(workFlowNotiLst.get(i).getFkentityId());
    									listPojo.setOrderDate(workFlowNotiLst.get(i).getOrderDate());
    									listPojo.setOrderStatus(workFlowNotiLst.get(i).getOrderStatus());
    									listPojo.setPkOrder(workFlowNotiLst.get(i).getPkOrder());
    									listPojo.setTaskCompflag(workFlowNotiLst.get(i).getTaskCompflag());
    									listPojo.setActivityDesc(workFlowNotiLst.get(i).getActivityDesc());
    									listPojo.setFksiteid(workFlowNotiLst.get(i).getFksiteid());
    									tmporderid=workFlowNotiLst.get(i).getOrderNumber();
    									tmpordertype=workFlowNotiLst.get(i).getOrderType();
    									tmppkOrder=workFlowNotiLst.get(i).getPkOrder().toString();
    									tmpActivityName=workFlowNotiLst.get(i).getActivityName();
    									if(workFlowNotiLst.get(i).getSampleAtLab()!=null && workFlowNotiLst.get(i).getSampleAtLab().toString().equalsIgnoreCase("Y") && (workFlowNotiLst.get(i).getActivityName().toString().equalsIgnoreCase(TSK_SHIPMENT_INFO) || workFlowNotiLst.get(i).getActivityName().toString().equalsIgnoreCase(TSK_GEN_PACKAGE_SLIP))){
    										
    									}else{
    										li1.add(listPojo);
    									}
    									i=i-1;
    									
    								}
    							 }
    	    				setWorkFlowNotiLst(li1);
    	    			}
    				}
    				if(k==1){
    					//System.out.println("\n\n\n\n\ninside i=1 workflow...");
    					wrkflw_condition=cbuaction.getWorkflowConditions()+condition;
    					wrkflw_OrdBy_Condition="";
    					tempOrd=linkController.getWorkFlowNotification(paginationSearch,2,userId,site,wrkflw_condition,wrkflw_OrdBy_Condition);
    					//System.out.println("\n\n\n\n\nend of i=1...");
    					
    					
    				}
    				if(k==2){
    					//System.out.println("\n\n\n\n\ninside i=2 workflow...");
    					wrkflw_condition=cbuaction.getWorkflowConditions();
    					wrkflw_OrdBy_Condition="";
    					tempOrd=linkController.getWorkFlowNotification(paginationSearch,2,userId,site,wrkflw_condition,wrkflw_OrdBy_Condition);
    					//System.out.println("\n\n\n\n\nend of i=2 workflow...");
    					
    				}
    				
    			}else{
    				paginationSearch.setiPageNo(Start);
					paginationSearch.setiShowRows(End);
					tempOrd = linkController.getWorkFlowNotification(paginationSearch,1,userId,site,wrkflw_condition,wrkflw_OrdBy_Condition);
    				templst=linkController.getWorkFlowNotification(paginationSearch,2,userId,site,wrkflw_condition,wrkflw_OrdBy_Condition);
    				Object obj=templst.get(0);
    				//log.debug("value of list size notification table:::::::::::::::"+obj.toString());
    				iTotalRows=Integer.parseInt(obj.toString());
    				//log.debug("size ....."+iTotalRows);
    			}
    			
    			
    						
    		}catch(Exception e){
    			//log.debug("Error in orders Part.................................................");
    			e.printStackTrace();
    		}
    		
    		return tempOrd;
    		
    	}
    

    public List FetchPSOrdersData(String condition,String orderBy,int k,int Start,int End){
    	
    	
    	String fksiteId="";
		String ordertypeval="";
		String filterflag="0";
		String temp1=request.getParameter("filterPending");
		String pscondition="";
		String psorderBy="";
		UserJB user = (UserJB)getRequest().getSession().getAttribute(VelosGarudaConstants.GARUDA_USER);
		Long userId = (long)user.getUserId();
		fksiteId="";
		int allEntriesflag=0;
		List tempList=new ArrayList();
		int showEntriesSize=0;
		int refreshAction=0;
		String countStr="";
		
		try{
			if(request.getParameter("pkSiteId")!=null){
				fksiteId=request.getParameter("pkSiteId").toString();
				refreshAction=1;
			}else if(request.getSession().getValue("lastCBB")!=null && !request.getSession().getValue("lastCBB").equals("")){
				fksiteId=request.getSession().getValue("lastCBB").toString();
			}else{
				fksiteId=user.getUserSiteId();
			}
			
			
			if(request.getParameter("allEntries")!=null && !request.getParameter("allEntries").toString().equals("")){
				if(request.getParameter("allEntries").toString().equals("ALL")){
					allEntriesflag=1;
				}
				
			}else if(request.getSession().getValue("v_actOrd_SE_txt")!=null && !request.getSession().getValue("v_actOrd_SE_txt").equals("")){	
				
				if(request.getSession().getValue("v_actOrd_SE_txt").equals("ALL")){
					allEntriesflag=1;
					showEntriesSize=100;
				}else{
					showEntriesSize=Integer.parseInt(request.getSession().getValue("v_actOrd_SE_txt").toString());
				}
				
			}
			
			pscondition=openorderaction.getPSOrdersCondition();
			
			if(refreshAction==0 && condition.equals("") && request.getSession().getValue("v_actOrd_fval")!=null && !request.getSession().getValue("v_actOrd_fval").equals("")){
				String temp=request.getSession().getValue("v_actOrd_fval").toString();
				temp=temp.replace("----", "&");
				pscondition=openorderaction.getPSOrdersConditionFromSession();
			}
			
			if(temp1!=null && temp1!="" && temp1.equals("1")){
				filterflag=request.getParameter("filterPending");
				//setPendingFilter(filterflag);
				ordertypeval=request.getParameter("fkordertype").toString();
				//setPfOrdertype(ordertypeval);
				fksiteId=request.getParameter("pksiteid");
				//setPfSiteId(fksiteId);
				pscondition=openorderaction.getPSOrdersCondition();
			}
			
			
			if(fksiteId.equals(ALL_SITE)){
				
				fksiteId=getUserSiteString(userId,GET_DATA);
				countStr=getUserSiteString(userId,GET_COUNT);
				
			}else{
				int charCount = (fksiteId.length() - fksiteId.replaceAll("\\,", "").length())+1;
				countStr=Integer.toString(charCount);
			}
			
			
			
			openorderaction.setSessionDataPS();
			
			paginationSessionData(paginationSearch,showEntriesSize,MODULE_PENDING);
			if(allEntriesflag==1){
				
				if(k==0){
					//System.out.println("\n\n\n\n\ninside i=0...");
					if(request.getParameter("otherThanAll")!=null && request.getParameter("otherThanAll").equals("1")){
						//System.out.println("inside expected....");
						paginationSessionData(paginationSearch,0,MODULE_PENDING);
					}else{
						//System.out.println("For all....");
						paginationSearch.setiPageNo(Start);
						paginationSearch.setiShowRows(End);
					}
					
					condition=pscondition+condition;
					tempList=linkController.getallOrderDetails(paginationSearch,1,filterflag,ordertypeval,fksiteId,userId,condition,orderBy);
					//System.out.println("\n\n\n\n\nend of i=0...");
				}
				if(k==1){
					//System.out.println("\n\n\n\n\ninside i=1...");
					condition=pscondition+condition;
					tempList=linkController.getallOrderDetails(paginationSearch,2,filterflag,ordertypeval,fksiteId,userId,condition,orderBy);
					//System.out.println("\n\n\n\n\nend of i=1...");
					
				}
				if(k==2){
					//System.out.println("\n\n\n\n\ninside i=2...");
					condition=pscondition;
					tempList=linkController.getallOrderDetails(paginationSearch,2,filterflag,ordertypeval,fksiteId,userId,condition,orderBy);
					//System.out.println("\n\n\n\n\nend of i=2...");
					
				}
				/*if(k==3){
				condition=pscondition+condition;
				tempList=linkController.getallOrderDetails(paginationSearch,0,filterflag,ordertypeval,fksiteId,userId,condition,orderBy);
			    }*/
				
				
				
			}
			
			
		    //setLabelList(linkController.getLabelAssignLst(fksiteId));
		    
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		
    	
    	return tempList;
    }
    
    public String FetchAssignToLst() throws Exception{
    	
        String fksiteId="";
        UserJB user = (UserJB)getRequest().getSession().getAttribute(VelosGarudaConstants.GARUDA_USER);
        Long userId = (long)user.getUserId();
        
        
    	if(request.getParameter("pkSiteId")!=null){
			fksiteId=request.getParameter("pkSiteId").toString();
		}else if(request.getSession().getValue("lastCBB")!=null && !request.getSession().getValue("lastCBB").equals("")){
			fksiteId=request.getSession().getValue("lastCBB").toString();
		}else{
			fksiteId=user.getUserSiteId();
		}
    	
    	if(fksiteId.equals(ALL_SITE)){
    		fksiteId=getUserSiteString(userId,GET_DATA);
		}
    	setAssignUsrLst(userList(GET_ALL_FOR_CBB,fksiteId));
    	
    	return "SUCCESS";
    }
    
    public List FetchAlertsData(String condition,String orderBy,int k,int Start,int End){
    	
		UserJB user = (UserJB)getRequest().getSession().getAttribute(VelosGarudaConstants.GARUDA_USER);
		Long userId = (long)user.getUserId();
		request.setAttribute("userId", userId);
		String site=request.getParameter("pksiteId").toString();
		int allEntriesflag=0;
		List li2=new ArrayList();
		List tempLst=new ArrayList();
		List tempLst2=new ArrayList();
		String alerts_condition="";
		String alerts_orderBy="";
		alerts_orderBy=orderBy;
		
		
		if(request.getParameter("allEntries")!=null && !request.getParameter("allEntries").toString().equals("") && request.getParameter("allEntries").toString().equals("ALL")){
			allEntriesflag=1;
		}
		
		//System.out.println("allEntries Flag...."+allEntriesflag);
		
		try{
			
			
			if(site.equals(ALL_SITE)){
				site=getUserSiteString(userId,GET_DATA);
			}
			
			if(allEntriesflag==1){
				
				if(k==0){
					//System.out.println("\n\n\n\n\ninside i=0 alerts...");
					alerts_condition=cbuaction.getAlertsCondtion()+condition;
					
					if(request.getParameter("otherThanAll")!=null && request.getParameter("otherThanAll").equals("1")){
						paginationReqScopeData(paginationSearch);
					}else{
						paginationSearch.setiPageNo(Start);
						paginationSearch.setiShowRows(End);
					}
					String tmpordertype1="";
					String tmppkOrder1="";
					li2 =  linkController.getAlertNotifications(userId,site,alerts_condition,orderBy,paginationSearch,1);
					
					for(int l=0;l<li2.size();l++){
						//log.debug("in for loop||||||||||||||||||||");
						Object[] obj = (Object[])li2.get(l);
						AlertsListPojo alertsListPojo=new AlertsListPojo();
						//log.debug("after declaring alertsListPojo");
							if(obj[0]!=null && !obj[0].equals("")){
								alertsListPojo.setAlertInstanceId(Long.parseLong(obj[0].toString()));
							}
							if(obj[1]!=null && !obj[1].equals("")){
							alertsListPojo.setAlertTitle(obj[1].toString());
							}
							if(obj[2]!=null && !obj[2].equals("")){
								alertsListPojo.setAlertWording(obj[2].toString());
							}
							if(obj[3]!=null && !obj[3].equals("")){
							alertsListPojo.setAlertCloseFlag(obj[3].toString());
							}
							if(obj[4]!=null && !obj[4].equals("")){
							alertsListPojo.setOrderType(obj[4].toString());
							}
							if(obj[5]!=null && !obj[5].equals("")){
							alertsListPojo.setOrderNum(obj[5].toString());
							}
							if(obj[6]!=null && !obj[6].equals("")){
							alertsListPojo.setCordRegistryId(obj[6].toString());
							alertsListPojo.setRegId1(obj[6].toString());
							
							}
							if(obj[7]!=null && !obj[7].equals("")){
							alertsListPojo.setOrderStatus(obj[7].toString());
							}
							if(obj[8]!=null && !obj[8].equals("")){
							alertsListPojo.setOrderDate(obj[8].toString());
							}
							if(obj[9]!=null && !obj[9].equals("")){
								alertsListPojo.setCordId(Long.parseLong(obj[9].toString()));
							}
							if(obj[10]!=null && !obj[10].equals("")){
								alertsListPojo.setOrderId(Long.parseLong(obj[10].toString()));
							}
							if(obj[11]!=null && !obj[11].equals("")){
								alertsListPojo.setOrderStatusId(Long.parseLong(obj[11].toString()));
							}
							if(obj[12]!=null && !obj[12].equals("")){
								alertsListPojo.setOrdertypeId(Long.parseLong(obj[12].toString()));
							}
							if(obj[16]!=null && !obj[16].equals("")){
								alertsListPojo.setFksiteid(Long.parseLong(obj[16].toString()));
							}
							if(obj[17]!=null && !obj[17].equals("")){
								alertsListPojo.setAlertCreatedOn(obj[17].toString());
								//System.out.println("alertsListPojo.getAlertCreatedOn()::::::::::::::::::::"+alertsListPojo.getAlertCreatedOn());
							}
							//log.debug("before adding alertslistPojo to alertnotificationLst");
							alertNotificationsLst=getAlertNotificationsLst();
							alertNotificationsLst.add(alertsListPojo);
							
					}
					
					List<AlertsListPojo> li3=new ArrayList<AlertsListPojo>();
					if(getAlertNotificationsLst().size()>0){
						
						li3.add(alertNotificationsLst.get(0));
						tmpordertype1=alertNotificationsLst.get(0).getOrderType();
						tmppkOrder1=alertNotificationsLst.get(0).getOrderId().toString();
					}
					
					for(int i=0;i<alertNotificationsLst.size();i++){
						AlertsListPojo alPojo=new AlertsListPojo();
						AlertsListPojo alPojoReqType=new AlertsListPojo();
						AlertsListPojo alPojoReqDate=new AlertsListPojo();
						AlertsListPojo alPojoReqStat=new AlertsListPojo();
						AlertsListPojo alPojoCordId=new AlertsListPojo();
						AlertsListPojo alPojoOrdNum=new AlertsListPojo();
						
						
						if(alertNotificationsLst.get(i).getOrderId().toString().trim().equals(tmppkOrder1.trim()) && alertNotificationsLst.get(i).getOrderType().toString().trim().equals(tmpordertype1.trim())){
							//log.debug("in if condition");
							alPojo.setAlertInstanceId(alertNotificationsLst.get(i).getAlertInstanceId());
							alPojo.setOrderNum("-");
							alPojo.setOrderType(alertNotificationsLst.get(i).getOrderType());
							alPojo.setCordRegistryId("-");
							alPojo.setOrderDate("-");
							alPojo.setOrderStatus("-");
							alPojo.setRegId1(alertNotificationsLst.get(i).getRegId1());
							alPojo.setOrderId(alertNotificationsLst.get(i).getOrderId());
							alPojo.setAlertCloseFlag(alertNotificationsLst.get(i).getAlertCloseFlag());
							alPojo.setAlertWording(alertNotificationsLst.get(i).getAlertWording());
							alPojo.setAlertTitle(alertNotificationsLst.get(i).getAlertTitle());
							alPojo.setAlertCloseFlag(alertNotificationsLst.get(i).getAlertCloseFlag());
							alPojo.setOrderStatusId(alertNotificationsLst.get(i).getOrderStatusId());
							alPojo.setOrdertypeId(alertNotificationsLst.get(i).getOrdertypeId());
							alPojo.setCordId(alertNotificationsLst.get(i).getCordId());
							alPojo.setOrderStatus(alertNotificationsLst.get(i).getOrderStatus());
							alPojo.setFksiteid(alertNotificationsLst.get(i).getFksiteid());
							tmpordertype1=alertNotificationsLst.get(i).getOrderType();
							tmppkOrder1=alertNotificationsLst.get(i).getOrderId().toString();
							alPojo.setAlertCreatedOn(alertNotificationsLst.get(i).getAlertCreatedOn());
							//System.out.println("alPojo.getAlertCreatedOn() in if::::::::::::::"+alPojo.getAlertCreatedOn());
							li3.add(alPojo);
							
						}
						else{
							//log.debug("in else condition");
							alPojo.setAlertInstanceId(alertNotificationsLst.get(i).getAlertInstanceId());
							alPojo.setOrderNum(alertNotificationsLst.get(i).getOrderNum());
							alPojo.setOrderType(alertNotificationsLst.get(i).getOrderType());
							alPojo.setCordRegistryId(alertNotificationsLst.get(i).getCordRegistryId());
							alPojo.setOrderDate(alertNotificationsLst.get(i).getOrderDate());
							alPojo.setOrderStatus(alertNotificationsLst.get(i).getOrderStatus());
							alPojo.setOrderId(alertNotificationsLst.get(i).getOrderId());
							alPojo.setRegId1(alertNotificationsLst.get(i).getRegId1());
							alPojo.setAlertCloseFlag(alertNotificationsLst.get(i).getAlertCloseFlag());
							alPojo.setAlertWording(alertNotificationsLst.get(i).getAlertWording());
							alPojo.setAlertTitle(alertNotificationsLst.get(i).getAlertTitle());
							alPojo.setAlertCloseFlag(alertNotificationsLst.get(i).getAlertCloseFlag());
							alPojo.setOrderStatusId(alertNotificationsLst.get(i).getOrderStatusId());
							alPojo.setCordId(alertNotificationsLst.get(i).getCordId());
							alPojo.setOrderStatus(alertNotificationsLst.get(i).getOrderStatus());
							alPojo.setFksiteid(alertNotificationsLst.get(i).getFksiteid());
							tmpordertype1=alertNotificationsLst.get(i).getOrderType();
							tmppkOrder1=alertNotificationsLst.get(i).getOrderId().toString();
							alPojo.setOrdertypeId(alertNotificationsLst.get(i).getOrdertypeId());
							alPojo.setAlertCreatedOn(alertNotificationsLst.get(i).getAlertCreatedOn());
							//System.out.println("alPojo.getAlertCreatedOn() in else::::::::::::::"+alPojo.getAlertCreatedOn());
							li3.add(alPojo);
							
							i=i-1;
							
						}
					}
					setAlertNotificationsLst(li3);
					
					//System.out.println("list size....."+getAlertNotificationsLst().size());
				}
				if(k==1){
					alerts_condition=cbuaction.getAlertsCondtion()+condition;
					orderBy="";
					tempLst =  linkController.getAlertNotifications(userId,site,alerts_condition,orderBy,paginationSearch,2);
				}
				if(k==2){
					alerts_condition=cbuaction.getAlertsCondtion();
					orderBy="";
					tempLst =  linkController.getAlertNotifications(userId,site,alerts_condition,orderBy,paginationSearch,2);
					
				}
				
			}else{
				li2=linkController.getAlertNotifications(userId,site,alerts_condition,alerts_orderBy,paginationSearch,1);
				tempLst =  linkController.getAlertNotifications(userId,site,alerts_condition,alerts_orderBy,paginationSearch,2);
				Object obj=tempLst.get(0);
				
			}
			
		}catch(Exception e){
    			e.printStackTrace();
    	}
    		
    	return tempLst;
    		
    	}
    	
    

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
}
