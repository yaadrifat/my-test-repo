package com.velos.ordercomponent.action;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.opensymphony.xwork2.ActionSupport;
import com.velos.ordercomponent.business.pojoobjects.HLAFilterPojo;
import com.velos.ordercomponent.util.VelosGarudaConstants;

public class LicensureEligibilityCSV extends ActionSupport implements VelosGarudaConstants{
	
	private Boolean isMultipleCBBSelected = false;
	
	
	public File licEligCSV(List<Object> cdrObjects, String path){
		
		
		 int bufferSize = 10240;
		 FileWriter fr = null;
		 BufferedWriter br = null;
		
		 File file = null;
		 Map<Long, HLAFilterPojo> cbuHla = null;
		 StringBuilder content = new StringBuilder();
		
		 
		try {
			
			file = new File(path);
			fr = new FileWriter(file);		
            br = new BufferedWriter(fr, bufferSize);
       
		
		 
		 
		 
		content = createHeader(content);
		br.write(content.toString());				
	    content.setLength(0);		
	    
		for(Object obj : cdrObjects){		 
			
			if(obj!=null){
				
      	        Object[] objArr = (Object[])obj;
      	        
				
			 
			  
			
			  
			    if(isMultipleCBBSelected){
				  content.append(WRAPPER).append(objArr[1]!=null?objArr[1].toString():"").append(WRAPPER).append(SEPRATOR); 
			    }
				  content.append(WRAPPER).append(objArr[2]!=null?objArr[2]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[3]!=null?objArr[3]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[4]!=null?objArr[4]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[5]!=null?objArr[5]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[6]!=null?objArr[6]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[7]!=null?objArr[7]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[8]!=null?objArr[8]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[9]!=null?objArr[9]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[10]!=null?objArr[10]:"").append(WRAPPER).append(SEPRATOR);			
				  content.append(WRAPPER).append(objArr[11]!=null?objArr[11]:"").append(WRAPPER).append(SEPRATOR);				
				  content.append(WRAPPER).append(objArr[12]!=null && (objArr[11]!=null && objArr[11].toString().equals(INELIGIBLE_STR))?objArr[12]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[12]!=null && (objArr[11]!=null && objArr[11].toString().equals(INCOMPLETE_STR))?objArr[12]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[13]!=null?objArr[13]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[14]!=null?objArr[14]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[15]!=null?objArr[15]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[16]!=null?objArr[16]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[17]!=null?objArr[17]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[18]!=null?objArr[18]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[19]!=null?objArr[19]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[20]!=null?objArr[20]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[21]!=null?objArr[21]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[22]!=null?objArr[22]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[23]!=null?objArr[23]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[24]!=null?objArr[24]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[25]!=null?objArr[25]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[26]!=null?objArr[26]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[27]!=null?objArr[27]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[28]!=null && (objArr[30]!=null && objArr[30].toString().trim().equals(ORDER_TYPE_CT_SHIP)) ? objArr[28]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[29]!=null && (objArr[30]!=null && objArr[30].toString().trim().equals(OR)) ? objArr[29]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[30]!=null?objArr[30]:"").append(WRAPPER).append(SEPRATOR);
				  content.append(WRAPPER).append(objArr[31]!=null?objArr[31]:"").append(WRAPPER).append(SEPRATOR);
				 
				  content.append(NEW_LINE);
				  
				  br.write(content.toString());				
				  content.setLength(0);		
			}
			
		 }
		 } catch (IOException e) {
			
			e.printStackTrace();
		}finally{
          try {
              br.close();
              fr.close();
          } catch (IOException e) {
              e.printStackTrace();
          }
      }
		return file;
	}
	
	
private StringBuilder createHeader(StringBuilder content){
	
	if(isMultipleCBBSelected){
		   content.append(WRAPPER).append(getText("garuda.queryBuilder.label.cbbid")).append(WRAPPER).append(SEPRATOR);
	}
		   content.append(WRAPPER).append(getText("garuda.cbuentry.label.registrycbuid")).append(WRAPPER).append(SEPRATOR);              
		   content.append(WRAPPER).append(getText("garuda.cbuentry.label.localcbuid")).append(WRAPPER).append(SEPRATOR);                   
		   content.append(WRAPPER).append(getText("garuda.cbuentry.label.idbag")).append(WRAPPER).append(SEPRATOR); 
		   content.append(WRAPPER).append(getText("garuda.queryBuilder.label.cbucollectiondate")).append(WRAPPER).append(SEPRATOR);                           
		   content.append(WRAPPER).append(getText("garuda.queryBuilder.nmdpregistrationDates")).append(WRAPPER).append(SEPRATOR);		  
		   content.append(WRAPPER).append(getText("garuda.cbuentry.label.cbu_status")).append(WRAPPER).append(SEPRATOR);           
		   content.append(WRAPPER).append(getText("garuda.clinicalnote.label.reason")).append(WRAPPER).append(SEPRATOR);	
		   content.append(WRAPPER).append(getText("garuda.cdrcbuview.label.licensure_status")).append(WRAPPER).append(SEPRATOR);
		   content.append(WRAPPER).append(getText("garuda.evaluHistory.report.unlicensedReason")).append(WRAPPER).append(SEPRATOR);
		   content.append(WRAPPER).append(getText("garuda.cdrcbuview.label.eligibility_determination")).append(WRAPPER).append(SEPRATOR);
		   content.append(WRAPPER).append(getText("garuda.queryBuilder.ineligiblereason")).append(WRAPPER).append(SEPRATOR);
		   content.append(WRAPPER).append(getText("garuda.queryBuilder.incompletereason")).append(WRAPPER).append(SEPRATOR);
		   content.append(WRAPPER).append(getText("garuda.queryBuilder.babyBirthDate")).append(WRAPPER).append(SEPRATOR);
		   content.append(WRAPPER).append(getText("garuda.queryBuilder.race")).append(WRAPPER).append(SEPRATOR);
		   content.append(WRAPPER).append(getText("garuda.cordentry.label.nmdpbroadrace")).append(WRAPPER).append(SEPRATOR);
		   content.append(WRAPPER).append(getText("garuda.pendingorderpage.label.funded")).append(WRAPPER).append(SEPRATOR);
		   content.append(WRAPPER).append(getText("garuda.queryBuilder.fundingcategory")).append(WRAPPER).append(SEPRATOR);
		   content.append(WRAPPER).append(getText("garuda.cbuentry.label.bcresult")).append(WRAPPER).append(SEPRATOR);
		   content.append(WRAPPER).append(getText("garuda.cbuentry.label.fcresult")).append(WRAPPER).append(SEPRATOR);
		   content.append(WRAPPER).append(getText("garuda.minimumcriteria.label.hemoglobin")).append(WRAPPER).append(SEPRATOR);
		   content.append(WRAPPER).append(getText("garuda.cbbprocedures.label.processingprocedure")).append(WRAPPER).append(SEPRATOR);
		   content.append(WRAPPER).append(getText("garuda.queryBuilder.label.totalcbunucleatedcount")).append(WRAPPER).append(SEPRATOR);
		   content.append(WRAPPER).append(getText("garuda.queryBuilder.label.totalcbunucleatedcountun")).append(WRAPPER).append(SEPRATOR);
		   content.append(WRAPPER).append(getText("garuda.queryBuilder.totalcd34")).append(WRAPPER).append(SEPRATOR);
		   content.append(WRAPPER).append(getText("garuda.queryBuilder.viabilitycount")).append(WRAPPER).append(SEPRATOR);
		   content.append(WRAPPER).append(getText("garuda.common.lable.viability")).append(WRAPPER).append(SEPRATOR);
		   content.append(WRAPPER).append(getText("garuda.productinsert.label.finalProdVol")).append(WRAPPER).append(SEPRATOR);
		   content.append(WRAPPER).append(getText("garuda.idsReport.ctshipdate")).append(WRAPPER).append(SEPRATOR);
		   content.append(WRAPPER).append(getText("garuda.idsReport.orshipdate")).append(WRAPPER).append(SEPRATOR);
		   content.append(WRAPPER).append(getText("garuda.detailreport.cbuhistory.requesttype")).append(WRAPPER).append(SEPRATOR);
		   content.append(WRAPPER).append(getText("garuda.idsReport.reqdate")).append(WRAPPER).append(SEPRATOR);		  
		   content.append(NEW_LINE);
			
		   return content;
		   
	
	
	}

public void setIsMultipleCBBSelected(Boolean isMultipleCBBSelected) {
	this.isMultipleCBBSelected = isMultipleCBBSelected;
}

}
