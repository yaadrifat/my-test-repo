package com.velos.ordercomponent.util;

public interface SessionLoggingConstants {

	//Event Code
	public static final String MODULE_EC_VIEW_CBU = "View CBU";
	public static final String MODULE_EC_EDIT_CBU = "Edit CBU";
	public static final String MODULE_EC_DELETE_CBU = "Delete";
	public static final String MODULE_EC_ADD_CBU = "Add CBU";
	public static final String MODULE_EC_EDIT_ELIGIBILITY = "Edit Eligibility";	
	public static final String MODULE_EC_EDIT_LICENSE = "Edit License";
	public static final String MODULE_EC_EDIT_ORDER = "Edit ORDER";
	public static final String MODULE_EC_EDIT_CBB = "Edit CBB";
	public static final String MODULE_EC_EDIT_CBU_STATUS = "Edit CBU Status Reason";
	public static final String MODULE_EC_EDIT_FINAL_ELIGIBILITY = "Edit Final Eligibility";	
	public static final String MODULE_EC_EDIT_FINAL_LICENSE = "Edit Final License";
	//Processing Procedure Event Code
	public static final String MODULE_EC_VIEW_PROS_PROC = "View Procedure";
	public static final String MODULE_EC_EDIT_PROS_PROC= "Edit Procedure";
	public static final String MODULE_EC_PRINT_PROS_PROC= "Print Procedure";
	public static final String MODULE_EC_ADD_PROS_PROC= "Add Procedure";
	public static final String MODULE_EC_COPY_PROS_PROC= "Copy Procedure";
	//For MRQ/FMHQ/IDM Form and Minimum Criteria
	public static final String MODULE_EC_ADD_FORM = "Add@FORM";
	public static final String MODULE_EC_VIEW_FORM = "View@FORM";
	public static final String MODULE_EC_EDIT_FORM = "Edit@FORM";
	public static final String MODULE_EC_MINIMUMCRITERIA = "MINIMUM CRITERIA";
	public static final String MODULE_EC_MINI_LABSUMMARY = "View Lab Summary From MINIMUM CRITERIA";
	public static final String MODULE_EC_MINI_IDM = "View IDM Form From MINIMUM CRITERIA";
	public static final String MODULE_EC_MINI_HLA = "View HLA Data From MINIMUM CRITERIA";
	//Final cbu review Event code
	public static final String MODULE_EC_FCR = "Final CBU Review";
	public static final String MODULE_EC_FCR_VIEW_LICENSURE = "View Licensure From Final CBU Review";
	public static final String MODULE_EC_FCR_VIEW_ELIGIBLE = "View Eligibility From Final CBU Review";
	public static final String MODULE_EC_FCR_EDIT_LICENSURE = "Edit Licensure From Final CBU Review";
	public static final String MODULE_EC_FCR_EDIT_ELIGIBLE = "Edit Eligibility From Final CBU Review";
	//Complete Required Information Event code
	public static final String MODULE_EC_CRI_VIEW_CRI = "View Complete Required Informaion";
	public static final String MODULE_EC_CRI_VIEW_LICENSURE = "View Licensure From Complete Required Informaion";
	public static final String MODULE_EC_CRI_EDIT_LICENSURE = "Edit Licensure From Complete Required Informaion";
	public static final String MODULE_EC_CRI_VIEW_ELIGIBILITY = "View Eligibility From Complete Required Informaion";
	public static final String MODULE_EC_CRI_EDIT_ELIGIBILITY = "Edit Eligibility From Complete Required Informaion";
	public static final String MODULE_EC_CRI_VIEW_IDS = "View IDs From Complete Required Informaion";
	public static final String MODULE_EC_CRI_EDIT_IDS = "Edit IDs From Complete Required Informaion";
	public static final String MODULE_EC_CRI_VIEW_CBU_INFO = "View CBU Information From Complete Required Informaion";
	public static final String MODULE_EC_CRI_EDIT_CBU_INFO = "Edit CBU Information From Complete Required Informaion";
	public static final String MODULE_EC_CRI_VIEW_LAB_SUMM = "View Lab summary From Complete Required Informaion";
	public static final String MODULE_EC_CRI_EDIT_LAB_SUMM = "Edit Lab summary From Complete Required Informaion";
	public static final String MODULE_EC_CRI_VIEW_PROC_PRD = "View Processing Procedure From Complete Required Informaion";
	public static final String MODULE_EC_CRI_EDIT_PROC_PRD = "Edit Processing Procedure From Complete Required Informaion";
	public static final String MODULE_EC_CRI_VIEW_IDM = "View IDM From Complete Required Informaion";
	public static final String MODULE_EC_CRI_EDIT_IDM = "Edit IDM From Complete Required Informaion";
	public static final String MODULE_EC_CRI_VIEW_MRQ = "View MRQ From Complete Required Informaion";
	public static final String MODULE_EC_CRI_EDIT_MRQ = "Edit MRQ From Complete Required Informaion";
	public static final String MODULE_EC_CRI_VIEW_FMHQ = "View FMHQ From Complete Required Informaion";
	public static final String MODULE_EC_CRI_EDIT_FMHQ = "Edit FMHQ From Complete Required Informaion";
	public static final String MODULE_EC_VIEW_ORDER = "View Order";
	public static final String MODULE_EC_EDIT_CORD_AVAIL_CONFIRM = "Edit Cord Availability Confirmation";
	public static final String MODULE_EC_EDIT_ADDIT_HLA_TYPING = "Edit Additional HLA Typing";
	public static final String MODULE_EC_EDIT_CT_SHIPMENT = "Edit CT Shipment Information";
	public static final String MODULE_EC_EDIT_CBU_SHIPMENT = "Edit CBU Shipment Information";
	public static final String MODULE_EC_EDIT_NMDP_SHIPMENT = "Edit NMDP Research Sample Shipment Information";
	public static final String MODULE_EC_EDIT_RESOLUTION = "Edit Resolution";
	public static final String MODULE_EC_VIEW_CBB = "View CBB";
	//View Clinical 'Clinical Record' Constants
	public static final String MODULE_EC_CLINICAL_RECORD_CBU_INFO = "Edit CBU Information";
	public static final String MODULE_EC_CLINICAL_RECORD_ID="Edit Id";
	public static final String MODULE_EC_CLINICAL_RECORD_LAB_SUM="Edit Lab Summary";
	public static final String MODULE_EC_CLINICAL_RECORD_ELIGIBILITY="Edit Eligibility";
	public static final String MODULE_EC_CLINICAL_RECORD_HLA_CBU="Edit CBU HLA";
	public static final String MODULE_EC_CLINICAL_RECORD_HLA_MATERNAL="Edit Edit CBU Maternal";
	public static final String MODULE_EC_CLINICAL_RECORD_UPLOAD_DOC="Upload Document";
	public static final String MODULE_EC_CLINICAL_RECORD_VIE_UPLOADED_DOC="View Upload Document";
	
	//Entity Identifier
	public static final String MODULE_EI_ELIGIBILITY = "Eligibility";
	
	//Clinical Notes
	
	public static final String MODULE_EC_ADD_NOTE = "Add Note";
	public static final String MODULE_EC_VIEW_NOTE = "View Note";
	public static final String MODULE_EC_REVOKE_NOTE = "Revoke Note";
	public static final String MODULE_EC_REPLY_NOTE = "Reply Note";
	
	//Entity Type
	public static final String MODULE_ET_CBU = "CBU";
	public static final String MODULE_ET_ORDER = "ORDER";
	public static final String MODULE_ET_CBB = "CBB";
	public static final String MODULE_ET_PROCESSING_PROCEDURE = "PROCESSING PROCEDURE";
	public static final String MODULE_ET_CLINICAL_NOTES= "CLINICAL NOTES";
	
	//Processing Information
	public static final String MODULE_EC_PROC_INFO ="EDIT CBU and Maternal Samples";
	
	
}
