package com.velos.ordercomponent.validator;

import java.util.Calendar;
import java.util.Date;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.CompoundRoot;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

/**
 * @author Mohiuddin Ali Ahmed
 * @author Anurag Upadhyay
 * @version 1.0
 * 
 */

public class MinDateRangeFieldValidator extends FieldValidatorSupport {
	String invokingFieldTime;
	 String compareFieldTime;
	 String compareFieldName;

	public String getInvokingFieldTime() {
		return invokingFieldTime;
	}

	public void setInvokingFieldTime(String invokingFieldTime) {
		this.invokingFieldTime = invokingFieldTime;
	}

	public String getCompareFieldTime() {
		return compareFieldTime;
	}

	public void setCompareFieldTime(String compareFieldTime) {
		this.compareFieldTime = compareFieldTime;
	}

	public String getCompareFieldName() {
		return compareFieldName;
	}

	public void setCompareFieldName(String compareFieldName) {
		this.compareFieldName = compareFieldName;
	}
	@SuppressWarnings({ "unchecked"})
	@Override
	public void validate(Object object) throws ValidationException {
		 
		 Calendar cal = Calendar.getInstance();
		 CompoundRoot cRoot= ActionContext.getContext().getValueStack().getRoot();
		 String fieldName = getFieldName();
	   
		 Date value= (Date)getFieldValue(fieldName, object);
		 Date compareValue=(Date) getFieldValue(compareFieldName, object);
		 
	     String invokedFieldTime= getFieldValue(invokingFieldTime, cRoot).toString();
	     String comaredFieldTime=getFieldValue(invokingFieldTime, cRoot).toString();
	     
	     if(compareValue!=null && value!=null && invokedFieldTime.matches("([0-2][0-3]:[0-5][0-9])|([0-2][0-3]:[6][0])|([0-1][0-9]:[0-5][0-9])") && comaredFieldTime.matches("([0-2][0-3]:[0-5][0-9])|([0-2][0-3]:[6][0])|([0-1][0-9]:[0-5][0-9])")){
	     
	    	 String [] invokFieldTimes=invokedFieldTime.split(":");
		     String [] comFieldTimes=comaredFieldTime.split(":");
		     
		      cal.setTime(value);
		      cal.add(Calendar.HOUR, Integer.parseInt(invokFieldTimes[0])); // adds  hour
		      cal.add(Calendar.MINUTE, Integer.parseInt(invokFieldTimes[1]));// adds  minute
		      value=cal.getTime();
		      
		      cal.setTime(compareValue);
		      cal.add(Calendar.HOUR, Integer.parseInt(comFieldTimes[0])); // adds  hour
		      cal.add(Calendar.MINUTE, Integer.parseInt(comFieldTimes[1]));// adds  minute
		      compareValue=cal.getTime();	
	     
	     }
	     
	     Comparable valuex = (Comparable)value;
	     Comparable valuey = (Comparable)compareValue;
	     if ((value!=null && valuey!=null && valuex.compareTo(valuey) < 0)) {
	         addFieldError(getFieldName(), object);
	         return;
	         }
		
	}

}
