package com.velos.ordercomponent.controller;

import java.util.List;

import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.service.VelosService;
import com.velos.ordercomponent.service.impl.VelosServiceImpl;

public class AuditTrailController {

	VelosService service;
	
	public List<Object> getAuditRowDataByEntityId(Long entityId,String viewName,PaginateSearch paginateSearch,int i,String condition,String orderBy) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getAuditRowDataByEntityId(entityId,viewName,paginateSearch,i,condition,orderBy);
	}
	
	public List<Object> getAuditColumnDataByEntityId(Long entityId,String viewName) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getAuditColumnDataByEntityId(entityId, viewName);
	}
}
