package com.velos.ordercomponent.action;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.velos.eres.web.user.UserJB;
import com.velos.ordercomponent.business.domain.AppMessages;
import com.velos.ordercomponent.business.domain.CBUStatus;
import com.velos.ordercomponent.business.domain.CodeList;
import com.velos.ordercomponent.business.domain.Shipment;
import com.velos.ordercomponent.business.pojoobjects.CodeListPopulatedData;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.pojoobjects.SearchPojo;
import com.velos.ordercomponent.business.pojoobjects.SitePojo;
import com.velos.ordercomponent.business.util.Utilities;
import com.velos.ordercomponent.business.util.VelosUtil;
import com.velos.ordercomponent.controller.CBBController;
import com.velos.ordercomponent.controller.CBUController;
import com.velos.ordercomponent.controller.CodelstController;
import com.velos.ordercomponent.controller.OpenOrderController;
import com.velos.ordercomponent.controller.VDAController;
import com.velos.ordercomponent.controller.VelosSearchController;
import com.velos.ordercomponent.helper.AttachmentHelper;
import com.velos.ordercomponent.helper.velosHelper;
import com.velos.ordercomponent.util.GarudaConfig;
import com.velos.ordercomponent.util.SessionLoggingConstants;
import com.velos.ordercomponent.util.VelosGarudaConstants;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public abstract class VelosBaseAction extends ActionSupport implements ModelDriven,VelosGarudaConstants,SessionLoggingConstants {
	
	/*
	public Long id;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	} 
*/
    	public static final Log log = LogFactory.getLog(VelosBaseAction.class);
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	HttpServletResponse response=(HttpServletResponse)ac.get(ServletActionContext.HTTP_RESPONSE);
	ServletContext context = ServletActionContext.getServletContext();
	private HttpSession session =null;
	/*Map<Long,CodeList> codeListMapByIds = null;	
	Map<String,List<CodeList>> codeListMap = null;
	Hashtable<String,StringBuffer> sessionAppMap = null;
	Map<Long,CBUStatus> cbuStatusValues = null;*/
	private Timestamp timestamp;	
	private Long pageId;
	private String module;
	private String formCode;
	private Long source;
	private SearchPojo searchPojo;
	private VelosSearchController velosSearchController;
	private List userLst1;
	private String moduleEntityType;
	private String moduleEntityIdentifier;
	private String moduleEntityEventCode;
	private Long moduleEntityId;
	private Long moduleId;
	private List<SitePojo> sitesByUser;
	private CodeListPopulatedData populatedData;	
	List<SitePojo> sites;
	private String paginateModule;
	private String loadDivName;
	protected Map<Long,String> categoryDivMap;
	public static final String INPUT = "input";
	public static final String EDIT = "edit";
	public static final String UPDATE = "update";
	
	public String getLoadDivName() {
		return loadDivName;
	}

	public void setLoadDivName(String loadDivName) {
		this.loadDivName = loadDivName;
	}

	public Map<Long, String> getCategoryDivMap() {
		return categoryDivMap;
	}

	public void setCategoryDivMap(Map<Long, String> categoryDivMap) {
		this.categoryDivMap = categoryDivMap;
	}

	public String getPaginateModule() {
		return paginateModule;
	}

	public void setPaginateModule(String paginateModule) {
		this.paginateModule = paginateModule;
	}

	public List<SitePojo> getSites() {
		return sites;
	}

	public void setSites(List<SitePojo> sites) {
		this.sites = sites;
	}

	public CodeListPopulatedData getPopulatedData() {
		return populatedData;
	}

	public void setPopulatedData(CodeListPopulatedData populatedData) {
		this.populatedData = populatedData;
	}

	public VelosSearchController getVelosSearchController() {
		return velosSearchController;
	}

	public void setVelosSearchController(VelosSearchController velosSearchController) {
		this.velosSearchController = velosSearchController;
	}

	public SearchPojo getSearchPojo() {
		return searchPojo;
	}

	public void setSearchPojo(SearchPojo searchPojo) {
		this.searchPojo = searchPojo;
	}
	public ActionContext getAc() {
		return ac;
	}
	public VelosBaseAction() {
		// TODO Auto-generated constructor stub
		searchPojo=new SearchPojo();
		velosSearchController=new VelosSearchController();
		try{
			session = getSession(request);			
			if(session != null){
				//userObject=(UserDetailsObject)session.getAttribute(USER_OBJECT);
				/*codeListMapByIds = (Map<Long,CodeList>)getContext().getAttribute("codeListMapByIds");
				codeListMap = (Map<String,List<CodeList>>)getContext().getAttribute("codeListValues");
				sessionAppMap = (Hashtable<String,StringBuffer>)getContext().getAttribute("sessionAppMap");
				cbuStatusValues = (Map<Long, CBUStatus>)getContext().getAttribute("cbuStatusValues");*/
				String sessId = getSession(request).getId();
				StringBuffer sessBuff = new StringBuffer();
				if(!GarudaConfig.getSessionAppMap().containsKey(sessId))
					GarudaConfig.getSessionAppMap().put(sessId, sessBuff);
				setUserSite();
				categoryDivMap = new HashMap<Long, String>();	
				session.setAttribute("AppUrl",getText("garuda.application.url"));
			}
			//System.out.println("\n\n\n\n In Base Action :  \n\n\n\n");
			
		}catch(Exception e){
			
			e.printStackTrace();
		}
	}
	
	public String execute() throws Exception {
		//System.out.println( " Base action execute");
		return executeAction();
	}
	public String fetchCommaSeperatedString(long entitypk,String key){
    	
	 	   StringBuilder sbStr=new StringBuilder("");
	 	   List<Object> unLic=null;//=new ArrayList<Object>();
	 	   PaginateSearch paginationSearch1=null;
	 	   VDAController vdaController = new VDAController();
	 	   String exemption ="";
	 	   String exemptionStr = "Declines/No Race Selected";
	 	   //String temp=Long.toString(entitypk);
	 	   String qStr="";
	 	   
	 	   
	 	   if(LICENCE_STATUS.equals(key) || ELIGIBILITY_STATUS.equals(key)){
	 		   
	 		   qStr="SELECT ERES.F_CODELST_DESC(REASON.FK_REASON_ID) FROM ERES.CB_ENTITY_STATUS STAT INNER JOIN  ERES.CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS  WHERE  STAT.PK_ENTITY_STATUS=";
	 		   qStr += Long.toString(entitypk);
	 	   }
	 	   
	 	   if(RACE.equals(key)){
	 		   
	 		   qStr="SELECT ERES.f_codelst_Desc(race.FK_CODELST) raceDesc FROM eres.CB_CORD cord, eres.CB_MULTIPLE_VALUES race WHERE cord.pk_cord  = race.entity_id and race.FK_CODELST_TYPE='race' and cord.pk_cord =";
	 		   qStr += Long.toString(entitypk);
	 		   qStr +=" order by raceDesc";
	 	   }  
	 	   
			    try{
			    	unLic=vdaController.getQueryBuilderViewData(qStr, paginationSearch1);
				} catch (Exception e) {
					e.printStackTrace();
				}	
			   
			   if(unLic!=null){
				   int length=unLic.size();
				   for(int j=0;j<length;j++){
					 /*  if(j==0){
						  // returnStr=unLic.get(j)!=null?unLic.get(j).toString():"";
						   unLic.add(j, unLic.get(j)!=null?unLic.get(j).toString():"");
					   }else{
						   //returnStr+=unLic.get(j)!=null?unLic.get(j).toString():"";
					   }*/
					   if(!unLic.get(j).toString().equals(exemptionStr) || (unLic.size()==1 && unLic.get(j).toString().equals(exemptionStr))){
						   sbStr.append(unLic.get(j)!=null?(unLic.get(j).toString()):"").append(length-1==j?"":", ");
					   }
					   else{
						   
						   exemption = exemptionStr;
						   
					   }
				   }
				   
			   }
			   
	           			   
			   if(exemption.equals(exemptionStr)){
				   sbStr.append(", "+exemptionStr);
				  
			   }
			   //returnStr=returnStr.replace(',', ' ');
			   
	 	return sbStr.toString();
	 }
	
	public abstract String executeAction() throws Exception;
	
	public HttpSession getSession(HttpServletRequest request) {
		HttpSession session=request.getSession(false);
		//Check The Session Object and Throw Exception if The Session is null
		if( session!=null && session.getAttribute(GARUDA_USER)==null){			
			//throw new P1VBaseException(ErrorConstants.SESSION_EXPIRED_ERR,ErrorConstants.SESSION_EXPIRED_MSG);
		}
		return session;

	}
	public String getUserSiteString(Long userId,String reqData){
		
		String allSite="";
		String userIdToString="";
		
		if(userId!=null){
			userIdToString=userId.toString();
			//allSite="select fk_site From er_usersite where (fk_user="+userIdToString+" and (usersite_right>0 or usersite_right<0) ) ";
			if(reqData.equals(GET_DATA)){
				//allSite="select us.fk_site from er_usersite us,er_site s,cbb c where us.fk_site=s.pk_site and c.fk_site=s.pk_site and us.fk_user="+userIdToString+" and (usersite_right>0 or usersite_right<0) and c.using_cdr=1";
				allSite="select us.fk_site from cbb c,er_usersite us,er_site s where c.using_cdr='1' and us.fk_user='"+userId+"' AND us.usersite_right<>0 and c.fk_site=s.pk_site and  us.fk_site=s.pk_site";
			}
			if(reqData.equals(GET_COUNT)){
				//allSite="(select count(*) from er_usersite us,er_site s,cbb c where us.fk_site=s.pk_site and c.fk_site=s.pk_site and us.fk_user="+userIdToString+" and (usersite_right>0 or usersite_right<0) and c.using_cdr=1)";
				allSite="select count(*) from cbb c,er_usersite us,er_site s where c.using_cdr='1' and us.fk_user='"+userId+"' AND us.usersite_right<>0 and c.fk_site=s.pk_site and  us.fk_site=s.pk_site";
			}
		}		
		
		return allSite;
	}
	
	
	public void cleanSession()throws Exception{
		if (session!=null){
			session.removeAttribute(SESSION_BOUND_NAME);
			session.removeAttribute(GARUDA_USER);
			session.removeAttribute(SECURITY_ENABLE_KEY);
			session.removeAttribute(VELOS_GARUDA_DEFAULT_THEME);
			
		}	
	}
	public List getuserLst1() {
		return userLst1;
	}
	public void setuserLst1(List userLst1) {
		this.userLst1 = userLst1;
	}
	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public ServletContext getContext() {
		return context;
	}

	public void setContext(ServletContext context) {
		this.context = context;
	}
	
	public String getCodeListDescById(Long Id){
		return GarudaConfig.getCodeListMapByIds().get(Id).getDescription();	
	}
	
	public String getCodeListSubTypeById(Long Id){
		return GarudaConfig.getCodeListMapByIds().get(Id).getSubType();
	}
	
	public Long getCodeListPkByTypeAndSubtype(String type, String subType){
	  Long pk = null;
	     /*for(Map.Entry<Long, CodeList> me : GarudaConfig.getCodeListMapByIds().entrySet()){
	    	 if((type!=null && (type.equals(me.getValue().getType()) || type==me.getValue().getType())) 
	    			 && (subType!=null && (subType.equals(me.getValue().getSubType()) || subType==me.getValue().getSubType()))){
	    		 pk = me.getKey();
	    	 }
	     }*/
	     String key = type+"-"+subType;
		 CodeList cl =  GarudaConfig.getCodeListTypeSubType().get(key);
			  if(cl != null){
				  
				  pk = cl.getPkCodeId();
			  }
			  //System.out.println("PK Value returned in getCodeListPkByTypeAndSubtype:::"+pk+"for Key Value::::"+key);
	  return pk;
	}
	
	public String getCodeListDescByTypeAndSubtype(String type, String subType){
		  String desc = "";
		     /*for(Map.Entry<Long, CodeList> me : GarudaConfig.getCodeListMapByIds().entrySet()){
		    	 if((type!=null && (type.equals(me.getValue().getType()) || type==me.getValue().getType())) 
		    			 && (subType!=null && (subType.equals(me.getValue().getSubType()) || subType==me.getValue().getSubType()))){
		    		 desc = me.getValue().getDescription();
		    	 }
		     }*/
		  String key = type+"-"+subType;
		  CodeList cl =  GarudaConfig.getCodeListTypeSubType().get(key);
			  if(cl != null){
				  
				  desc = cl.getDescription();
			  }
			  //System.out.println("desc Value returned in getCodeListDescByTypeAndSubtype:::"+desc+"for Key Value::::"+key);
		  return desc;
	}
	
	public Boolean isTasksRequired(Long cordId){
		Boolean flag = false;
		int tempflag = 0;
		OpenOrderController linkController = new OpenOrderController();
		try{
			if(cordId!=null){
				tempflag = Integer.parseInt(linkController.getFlagForTaskCheck(cordId));
			}
		}catch (Exception e) {
			System.out.println(e);
		}
		if(tempflag==1){
			flag=true;
		}
		return flag;
	 }
	
	public Long getCodeListPkByTypeAndDesc(String type, String desc){
		  Long pk = null;
		     for(Map.Entry<Long, CodeList> me : GarudaConfig.getCodeListMapByIds().entrySet()){
		    	 if((type!=null && (type.equals(me.getValue().getType()) || type==me.getValue().getType())) 
		    			 && (desc!=null && (desc.equalsIgnoreCase(me.getValue().getDescription())))){
		    		 pk = me.getKey();
		    	 }
		     }
		  return pk;
	}
	
	public String getCodeListDescByPk(Long pkCodeList){
		  String desc = "";
		     /*for(Map.Entry<Long, CodeList> me : codeListMapByIds.entrySet()){
		    	 if(me.getKey()==pkCodeList || me.getKey().equals(pkCodeList)){
		    		 desc = me.getValue().getDescription();
		    	 }
		     }*/
		  CodeList cl =  GarudaConfig.getCodeListMapByIds().get(pkCodeList);
		  
		  if(cl != null){
			  
			  desc = cl.getDescription();
		  }
		  
		  return desc;
	}
	
	public String getCodeListDescByPk(Long pkCodeList,Long cordId){
		  String desc = "";
		  Boolean flag = isTasksRequired(cordId);
		     for(Map.Entry<Long, CodeList> me : GarudaConfig.getCodeListMapByIds().entrySet()){
		    	 if(me.getKey()==pkCodeList || me.getKey().equals(pkCodeList)){
		    		 if(me.getValue().getSubType().equals(NOT_COLLECTED_TO_PRIOR_REASON)){
		    			 if(!flag){
		    				 desc = NOT_APPLICABLE_PRIOR_DATE;
		    			 }else if(flag){
			    			 desc = getCodeListDescByTypeAndSubtype(ELIGIBILITY_STATUS , NOT_COLLECTED_TO_PRIOR_REASON);
			    		 }else{
			    			 desc = me.getValue().getDescription();
			    		 }
		    		 }
		    		 else{
		    			 desc = me.getValue().getDescription();
		    		 }
		    	 }
		     }
		  return desc;
	}
	
	public String getCodeListSubTypeByPk(Long pkCodeList){
		  String subType = "";
		     for(Map.Entry<Long, CodeList> me : GarudaConfig.getCodeListMapByIds().entrySet()){
		    	 if(me.getKey()==pkCodeList || me.getKey().equals(pkCodeList)){
		    		 subType = me.getValue().getSubType();
		    	 }
		     }
		  return subType;
	}
	
	public String getCbuStatusDescByPk(Long pkCbuStatus){
		String desc="";
		/*for(Map.Entry<Long,CBUStatus> me : cbuStatusValues.entrySet()){
			if(me.getKey()==pkCbuStatus || me.getKey().equals(pkCbuStatus)){
				desc = me.getValue().getCbuStatusDesc();
			}
		}*/
		
		CBUStatus cs = GarudaConfig.getCbuStatusValues().get(pkCbuStatus);
		if(cs != null)
			desc = cs.getCbuStatusDesc();
		
		return desc;
	}
	
	public Long getCbuStatusPkByCode(String code){
		String type = "Response";
		Long pk = null;
		for(Map.Entry<Long,CBUStatus> me : GarudaConfig.getCbuStatusValues().entrySet()){
			if(!StringUtils.isEmpty(code) && !StringUtils.isEmpty(type) && StringUtils.equalsIgnoreCase(me.getValue().getCbuStatusCode(), code)
					&& StringUtils.equalsIgnoreCase(me.getValue().getCodeType(), type)){
				pk = me.getKey();
			}
		}
		return pk;
	}
	
	public CBUStatus getCbuStatusByCode(String code){
		String type = "Response";
		CBUStatus cbuStatus = null;
		for(Map.Entry<Long,CBUStatus> me : GarudaConfig.getCbuStatusValues().entrySet()){
			if(!StringUtils.isEmpty(code) && !StringUtils.isEmpty(type) && StringUtils.equalsIgnoreCase(me.getValue().getCbuStatusCode(), code)
					&& StringUtils.equalsIgnoreCase(me.getValue().getCodeType(), type)){
				cbuStatus = me.getValue();
			}
		}
		return cbuStatus;
	}
	
	public List<CBUStatus> getCbuStatusListByType(String type){
		List<CBUStatus> cbuStatusList = new ArrayList<CBUStatus>();
		for(Map.Entry<Long,CBUStatus> me : GarudaConfig.getCbuStatusValues().entrySet()){
			if(!StringUtils.isEmpty(type) && StringUtils.equalsIgnoreCase(me.getValue().getCodeType(), type)){
				cbuStatusList.add(me.getValue());
			}
		}
		return cbuStatusList;
	}
	
	public Long getCbuStatusPkByCode(String code,String type){
		//String type = "Resolution";
		Long pk = null;
		for(Map.Entry<Long,CBUStatus> me : GarudaConfig.getCbuStatusValues().entrySet()){
			if(!StringUtils.isEmpty(code) && !StringUtils.isEmpty(type) && StringUtils.equalsIgnoreCase(me.getValue().getCbuStatusCode(), code)
					&& StringUtils.equalsIgnoreCase(me.getValue().getCodeType(), type)){
				pk = me.getKey();
			}
		}
		return pk;
	}
	
	public List<CBUStatus> getCBUStatusValues(String codeType){
		List<CBUStatus> cbuStatus = null;
		if(codeType!=null && !codeType.equals("")){
			try{
			cbuStatus = new CodelstController().getCBUStatusValues(codeType);
			}catch (Exception e) {
				//System.out.println(e);
				e.printStackTrace();
			}
		}
		return cbuStatus;
	}
	
	public List<CodeList> getCodeListObjs(List<CodeList> clist){
		List<CodeList> list2 = new ArrayList<CodeList>();
		Iterator<CodeList> it = clist.iterator();
		CodeList codelst = new CodeList();
		CodeList codelst1 = new CodeList();
		try{
		while(it.hasNext()){
			codelst = it.next();
			codelst1 = (CodeList)codelst.clone();
			list2.add(codelst1);
		}
		}catch(Exception e){
			System.out.println(e);
		}
		return list2;
	}
	
	public List<CodeList> getListOfCodeLstObj(String codeType,Long cordId){
		
		Boolean flag = isTasksRequired(cordId);
		CodeList code = null;
		
		List<CodeList> list = new ArrayList<CodeList>();
		
		for (Object key : GarudaConfig.getCodeListMap().keySet()) {
			if(codeType.equals(key.toString())){
				list = GarudaConfig.getCodeListMap().get(key);
			}
		}
		
		List<CodeList> list1 = getCodeListObjs(list);
		
		if(!flag){
			Iterator<CodeList> it = list1.iterator();
			while(it.hasNext()){
				code = it.next();
				if(code.getSubType().equals(NOT_COLLECTED_TO_PRIOR_REASON)){
					code.setDescription(NOT_APPLICABLE_PRIOR_DATE);
				}
			}
		}
		else{
			Iterator<CodeList> it = list1.iterator();
			while(it.hasNext()){
				code = it.next();
				if(code.getSubType().equals(NOT_COLLECTED_TO_PRIOR_REASON)){
					code.setDescription(getCodeListDescByTypeAndSubtype(ELIGIBILITY_STATUS , NOT_COLLECTED_TO_PRIOR_REASON));
				}
			}	
		}
		
		return list1;
	}
	
	public Long getTermialStatusByPkOrDesc(Long pkValue, String desc){
		Long pk = null;
		try{
			if(pkValue!=null || desc!=null){
			pk = Long.parseLong(new OpenOrderController().getTermialStatusByPkOrDesc(pkValue,desc));
			}
		}catch (Exception e) {
				//System.out.println(e);
				e.printStackTrace();
		}
		return pk;
	}
	public Timestamp getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public Long getPageId() {
		return pageId;
	}

	public void setPageId(Long pageId) {
		this.pageId = pageId;
	}

	public static final int PRIVILEGE_VIEW = 4;
	public static final int PRIVILEGE_EDIT = 2;
	public static final int PRIVILEGE_NEW = 1;

	public boolean hasViewPermission(Integer permissionFlags) {
		if (permissionFlags == null || permissionFlags == 0) return false;
		return (permissionFlags & PRIVILEGE_VIEW) == PRIVILEGE_VIEW;
	}
	
	public boolean hasEditPermission(Integer permissionFlags) {
		if (permissionFlags == null || permissionFlags == 0) return false;
		return (permissionFlags & PRIVILEGE_EDIT) == PRIVILEGE_EDIT;
	}
	
	public boolean hasNewPermission(Integer permissionFlags) {
		if (permissionFlags == null || permissionFlags == 0) return false;
		return (permissionFlags & PRIVILEGE_NEW) == PRIVILEGE_NEW;
	}
	
	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public Long getDCMSAttachId(Long attachmentID){
		return AttachmentHelper.getDcmsFileAttchId(attachmentID);
	}
	
	public String getDCMSAttachType(Long attachmentID){
		return AttachmentHelper.getDcmsFileDocType(attachmentID);
	}
	
	public String getDCMSAttachFileName(Long attachmentID){
		return AttachmentHelper.getDcmsAttchFileName(attachmentID);
	}

	public String getFormCode() {
		return formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	public Long getSource() {
		return source;
	}

	public void setSource(Long source) {
		this.source = source;
	}
	
	
	public String getModuleEntityType() {
		return moduleEntityType;
	}

	public void setModuleEntityType(String moduleEntityType) {
		this.moduleEntityType = moduleEntityType;
	}	

	public Long getModuleEntityId() {
		return moduleEntityId;
	}

	public void setModuleEntityId(Long moduleEntityId) {
		this.moduleEntityId = moduleEntityId;
	}
	
	public String getModuleEntityIdentifier() {
		return moduleEntityIdentifier;
	}

	public void setModuleEntityIdentifier(String moduleEntityIdentifier) {
		this.moduleEntityIdentifier = moduleEntityIdentifier;
	}

	public String getModuleEntityEventCode() {
		return moduleEntityEventCode;
	}

	public void setModuleEntityEventCode(String moduleEntityEventCode) {
		this.moduleEntityEventCode = moduleEntityEventCode;
	}

	public Long getModuleId() {
		return moduleId;
	}

	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}

	public void paginationReqScopeData(PaginateSearch paginateSearch){
		//code for pagination 
		try{
			paginateSearch.setiPageNo(Utilities.nullIntconv(request.getParameter("iPageNo")));
			paginateSearch.setiShowRows(Utilities.nullIntconv(request.getParameter("iShowRows")));
			if(paginateSearch.getiShowRows()==0){
				paginateSearch.setiShowRows(5);
			}
			paginateSearch = VelosUtil.getPageNoShowRows(paginateSearch);
		}catch (Exception e) {
			//System.out.println("--------------"+e);
			e.printStackTrace();
		}
		//-----------end------------
	}
	
	public void paginationSessionData(PaginateSearch paginateSearch,int size, String module){
		//code for pagination 
		try{
			
			if(size==0){
				paginateSearch.setiPageNo(Utilities.nullIntconv(request.getParameter("iPageNo")));
				paginateSearch.setiShowRows(Utilities.nullIntconv(request.getParameter("iShowRows")));
				
				if(paginateSearch.getiShowRows()==0 && (module.equals(MODULE_PENDING) || module.equals(MODULE_LANDING))){
					//System.out.println("\n\n\n\n\n\nInside condition of setting 10 entries module pending.................");
					paginateSearch.setiShowRows(10);
				}
				if(paginateSearch.getiShowRows()==0 && module.equals(MODULE_CBBPROCEDURE)){
						//System.out.println("\n\n\n\n\n\nInside condition of setting 5 entries cbb procedure.................");
						paginateSearch.setiShowRows(5);
				}
				
			}else{
				//System.out.println("\n\n\n\n\n\nInside condition of setting else porition entries................."+size);
				paginateSearch.setiShowRows(size);
				paginateSearch.setiPageNo(0);
			}
			
			paginateSearch = VelosUtil.getPageNoShowRows(paginateSearch);
		}catch (Exception e) {
			//System.out.println("--------------"+e);
			e.printStackTrace();
		}
		//-----------end------------
	}
	
	public void initializeAppMessages(ServletContext context){
		List<AppMessages> appMsgsList = null;
		List<Object> resultlst = new ArrayList<Object>();
		SearchPojo searchPojo = new SearchPojo();
		searchPojo.setSearchType(VelosGarudaConstants.APPMESSAGES_OBJECTNAME);
		Long pkLandingPage = getCodeListPkByTypeAndSubtype(PAGE_TYPE,LANDING_PAGE_TYPE);
		searchPojo.setCriteria(" msgDispAt = '"+pkLandingPage+"'");
		Long pkInActiveMsg = getCodeListPkByTypeAndSubtype(APPMSG_STATUS,APPMSG_STATUS_INACTIVE);
		context.setAttribute("pkInActiveMsg", pkInActiveMsg);
		context.setAttribute("currentDate", new Date());
		try{
			resultlst = velosHelper.getObjectList(searchPojo.getSearchType(), searchPojo.getCriteria());
			searchPojo.setResultlst(resultlst);
			appMsgsList = searchPojo.getResultlst();
			//System.out.println("appMsgsList---------"+appMsgsList);
		}catch(Exception e){
			e.printStackTrace();
		}
		context.setAttribute("appMsgsList", appMsgsList);
	}
	public List userList(String parmtype,String Id) throws Exception{
		List userlst=new ArrayList();
		try{
			String fksite="";
			
			
			if(parmtype.equals(ORDER_ID_PARAM)){
				fksite="select crd.fk_cbb_id from er_order ord left outer join er_order_header hdr on (hdr.pk_order_header=ord.fk_order_header) left outer join cb_cord  crd on (hdr.order_entityid=crd.pk_cord) where ord.pk_order="+Id;
				userlst=(new OpenOrderController().getUserslistData(fksite));
			}
			
			if(parmtype.equals(GET_ALL_FOR_CBB)){
				fksite=Id;
				userlst=(new OpenOrderController().getUserslistData(fksite));
				
			}
		
			
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return userlst;
	}
	
	public List getAttachemetLists(Long entityId , Long categoryId , Long subCategoryId){
		List list = new ArrayList();
		try {
			list = new CBUController().getAttachmentByEntityId(entityId,categoryId,subCategoryId);
		}
		catch(Exception e){
			//System.out.println("Exception in CBUAction at getCBUAssessmentAttachemets method::"+e);
			e.printStackTrace();
			}
		return list;
	}
	
	public List getAttachments(Long entityId , Long categoryId , Long revokedFlag){
		List list = new ArrayList();
		try {
			list = new CBUController().getAttachmentByEntityId(entityId,categoryId,revokedFlag,0);
		}
		catch(Exception e){
			//System.out.println("Exception in velosbaseaction method getAttachmentLists method::"+e);
			e.printStackTrace();
			}
		return list;
	}
	public Long getCRI_CompletedFlag(Long pk_cord) throws Exception{
		Long completedFlag=(long)0;
		
		if(pk_cord!=null){
			completedFlag=new OpenOrderController().getCRI_CompletedFlag(pk_cord);
		}
		
		return completedFlag;
	}
	
	public String getSiteName(Long siteId) throws Exception{
		String siteName = null;
		
		if(siteId!=null){
			siteName=new CBBController().getSiteName(siteId);
		}
		
		return siteName;
	}
	
	public String getSiteID(Long siteId) throws Exception{
		String siteName = null;
		
		if(siteId!=null){
			siteName=new CBBController().getSiteID(siteId);
		}
		
		return siteName;
	}

	public List<SitePojo> getSitesByUser() {
		return sitesByUser;
	}

	public void setSitesByUser(List<SitePojo> sitesByUser) {
		this.sitesByUser = sitesByUser;
	}
	
	public void setUserSite(){
		try{
		UserJB user = (UserJB)getRequest().getSession().getAttribute(VelosGarudaConstants.GARUDA_USER);
		List<SitePojo> sites = new ArrayList<SitePojo>();
		sites = new CBBController().getSites();
		setSites(sites);
		setSitesByUser(sites); 
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<CodeList> getInvalidCodeListData(String type,Long[] values){
		List<CodeList> codeListIn = new ArrayList<CodeList>();
		Map<String,List<CodeList>> invalidCodeListMap = ((Map<String,List<CodeList>>)getContext().getAttribute("codeListInvalidValues"));
		if(invalidCodeListMap!=null && invalidCodeListMap.size()>0){
			if(!ArrayUtils.isEmpty(values)){
				List<CodeList> invalidCodeList = invalidCodeListMap.get(type);
				for(Long l : values){					
					if(invalidCodeList!=null)
					{
						for(CodeList c : invalidCodeList){
							if(c.getPkCodeId()==l || c.getPkCodeId().equals(l)){
								codeListIn.add(c);
							}
						}
					}
				}
			}			
		}	
		return codeListIn;
	}
	public String removeSymbol(String value,String symbol){
		
		String resultString="";
		
		if(value!=null && value!="" && !value.equals("undefined")){
			value=value.trim();
			resultString=value.replace(symbol, "");
			resultString=resultString.replace(" ", "");
		}
		//System.out.println("Resultant String::::::::::::::::::::::::::::::::::::::::::::"+resultString);
		return resultString;
		
	}
	
	public UserJB getUserDetails(Integer userId){
		UserJB user = null;
		try{
			user = new UserJB();
			user.setUserId(userId);
			user.getUserDetails(); 
		}catch (Exception e) {
				e.printStackTrace();
		}
		return user;
	}
	
	public String getUserNameById(Integer userId){
		String username="";
		UserJB user = getUserDetails(userId);
		if(user!=null)
			username += user.getUserLastName()+" "+user.getUserFirstName();
		return username;
	}
	
	public String insertHyphenIntoRegistryId(String regId){
		Integer index = 0;
		Integer length = 0;
		String changeRegId = "";
		if(!StringUtils.isEmpty(regId))
		{
			index = regId.indexOf("-");	
			length = regId.length();
			if(index==0 || index==-1){
				if(length==9){
					changeRegId = regId.substring(0,4)+"-"+regId.substring(4,8)+"-"+regId.substring(8,length);
				}else{
					changeRegId = regId;
				}
			}else{
				changeRegId = regId;
			}
		}
		return changeRegId;
	}
	
	public Boolean checkTokenExistence(String data,String token){
		Boolean flag = false;
		String resultString = "";
		String symbol = "-";
		if(!StringUtils.isEmpty(data) && !StringUtils.isEmpty(token)){
			resultString=data.replace(symbol, "");
			resultString=resultString.replace(" ", "");
			if(!StringUtils.isEmpty(resultString) && resultString.equals(token)){
				flag = true;
			}
		}
		return flag;
	}
	/* @Anurag 
	 * This method take two parameter 
	 * 1: List
	 * 2: Long 
	 * return the description if long value is matched with list objects corresponding value
	 * this is used to get data for Gender and Race field 
	 * */
	public String getCodeListDesc(List<CodeList> codelist , Long pkcodeid){		
        String desc="";
         if(codelist != null && !codelist.isEmpty()){
         for(CodeList cl : codelist){
        	 if(cl != null){
        	 if(cl.getPkCodeId()== pkcodeid || cl.getPkCodeId().equals(pkcodeid)){
             desc = cl.getDescription();
             break;
        	 }
        	}
           }
         } 
         return desc;
       }
	
	public String getRaceDescOrderBy(Long pkcordId)	{
		StringBuilder race=new StringBuilder();
		String qStr = "";
		String exemptionStr = "Declines/No Race Selected";
		String excemption = "";
		List<String> raceList = new ArrayList<String>();
		qStr="SELECT ERES.f_codelst_Desc(race.FK_CODELST) raceDesc FROM eres.CB_CORD cord, eres.CB_MULTIPLE_VALUES race WHERE cord.pk_cord  = race.entity_id and race.FK_CODELST_TYPE='race' and cord.pk_cord =";
		qStr += Long.toString(pkcordId);
		qStr +=" order by raceDesc";
		try{
			raceList.addAll(new CBUController().getRaceOrderByValue(qStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(raceList!=null){
			   int length=raceList.size();
			   for(int j=0;j<length;j++){
				   if(!raceList.get(j).toString().equals(exemptionStr) || (length==1 && raceList.get(j).toString().equals(exemptionStr))){
					   race.append(raceList.get(j)!=null?(raceList.get(j).toString()):"").append(length-1==j?"":", ");
				   }
				   else{
					   excemption = exemptionStr;
				   }
			   }
			   
		   }
		if(excemption.equals(exemptionStr)){
			race.append(", "+exemptionStr);
		}
		return race.toString();
	}
	/* This method return List for any collection*/
	
	public <E> List<E> collectionToList(Collection<E> collection)
	   {
	       List<E> list;
	       if (collection instanceof List)
	       {
	           list = (List<E>) collection;
	       }
	       else
	       {
	           list = new ArrayList<E>(collection);
	       }
	       return list;
	   }
	
	public String getDatafromXml(String tagname) {
		String data = "";
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder docBuilder;
		try {
			docBuilder = docBuilderFactory.newDocumentBuilder();
			try {
			    //System.out.println("ERES_HOMEEEE path:::::"+com.velos.eres.service.util.Configuration.ERES_HOME);
				Document doc = docBuilder.parse(new File(com.velos.eres.service.util.Configuration.ERES_HOME + "../server/eresearch/deploy/dcmsAttachment.xml"));
				//System.out.println("doc::::::::::::::::"+doc.getDocumentURI());
				doc.getDocumentElement().normalize();
				NodeList dcms = doc.getElementsByTagName("dcms");
				//System.out.println("dcms::::::::"+dcms.getLength());
				for (int s = 0; s < dcms.getLength(); s++) {

					Node dcmsNode = dcms.item(s);
					if (dcmsNode.getNodeType() == Node.ELEMENT_NODE) {

						Element dcmsElement = (Element) dcmsNode;

						// -------
						NodeList ipAddrList = dcmsElement
								.getElementsByTagName(tagname);
						Element ipAddrElement = (Element) ipAddrList.item(0);

						NodeList textFNList = ipAddrElement.getChildNodes();
						data = (String) ((Node) textFNList.item(0))
								.getNodeValue().trim();
						/*System.out
							.println(tagname + " is: " + data);*/

					}// end of if clause

				}// end of for loop with s var

			} catch (SAXException e) {
				// TODO Auto-generated catch block
				log.error(e.getMessage());
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				log.error(e.getMessage());
				e.printStackTrace();
			}
		} catch (ParserConfigurationException e1) {
			// TODO Auto-generated catch block
			log.error(e1.getMessage());
			e1.printStackTrace();
		}
		return data;
	}
	
	public String getDCMSIPAddress(){
	    System.out.println("dcmsIpAddress is:::::::::"+getDatafromXml("dcmsIpAddress"));
	    return getDatafromXml("dcmsIpAddress");
	}
	
	public Boolean checkContantDate(String checkDateS){
		Boolean flag = false;
		SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
		//Date date = df.parse(consDateS);
		try {
		    String consDateS = getCodeListDescByTypeAndSubtype(VELOS_EMTRAX_VERSION_TYPE,VELOS_EMTRAX_VERSION_SUBTYPE);		
		    Date consDate = !StringUtils.isEmpty(consDateS) ? df.parse(consDateS):null;
			Date checkDate = !StringUtils.isEmpty(checkDateS) ? df.parse(checkDateS)  :null;
			flag = consDate!=null && checkDate!=null ? compareDates(checkDate, consDate):false;
			//boolean x=	checkDate.compareTo(consDate)==-1?true:false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}
	
	private Boolean compareDates(Date d1,Date d2){
		Boolean flag = false;
		try{
			flag = (d1.getYear()==d2.getYear() && d1.getMonth()==d2.getMonth() && d1.getDay()==d2.getDay())?true:false;
			if(d1.before(d2) || (d1.equals(d2)) || flag){
				flag = true;
			}
		}catch(Exception ex){			
			ex.printStackTrace();
		}
		return flag;
	}
	
	public Boolean checkOldCord(Date schShipDate, Date actShipDate, Date codeLstDate){
		Boolean flag = false; 
		
		if(actShipDate==null && schShipDate==null){
			flag = false;
		}else{
			if(actShipDate!=null){
				if((actShipDate.compareTo(codeLstDate)>=0)){
					flag = false;
				}else if(actShipDate.before(codeLstDate)){
					flag = true;
				}
			}else if(schShipDate!=null){
				if((schShipDate.compareTo(codeLstDate)>=0)){
					flag = false;
				}else if(schShipDate.before(codeLstDate)){
					flag = true;
				}
			}
		}
		return flag;
	 }
}
