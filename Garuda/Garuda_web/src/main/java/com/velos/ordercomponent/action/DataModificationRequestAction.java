package com.velos.ordercomponent.action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.ordercomponent.business.pojoobjects.DataModificationRequestPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.util.VelosUtil;
import com.velos.ordercomponent.controller.DataModificationRequestController;
import com.velos.ordercomponent.helper.DataModificationRequestHelper;
import com.velos.ordercomponent.util.VelosGarudaConstants;
import com.aithent.audittrail.reports.EJBUtil;
import com.velos.eres.web.user.UserJB;

public class DataModificationRequestAction extends VelosBaseAction{
	
	/**
	 * 
	 */
	public static final Log log = LogFactory.getLog(DataModificationRequestAction.class);
	private static final long serialVersionUID = 5235130144143853980L;
	private DataModificationRequestPojo datarequestpojo;
	private List<DataModificationRequestPojo> DataModificationLst;
	private DataModificationRequestController requestController;
	private UserJB user;
	private PaginateSearch paginateSearch;
	
	
	public PaginateSearch getPaginateSearch() {
		return paginateSearch;
	}

	public void setPaginateSearch(PaginateSearch paginateSearch) {
		this.paginateSearch = paginateSearch;
	}

	public List<DataModificationRequestPojo> getDataModificationLst() {
		return DataModificationLst;
	}

	public void setDataModificationLst(List<DataModificationRequestPojo> dataModificationLst) {
		DataModificationLst = dataModificationLst;
	}

	public DataModificationRequestPojo getDatarequestpojo() {
		return datarequestpojo;
	}

	public void setDatarequestpojo(DataModificationRequestPojo datarequestpojo) {
		this.datarequestpojo = datarequestpojo;
	}

	public DataModificationRequestAction(){
		
		datarequestpojo=new DataModificationRequestPojo();
		requestController= new DataModificationRequestController();
		user=new UserJB();
		paginateSearch=new PaginateSearch();
	}

	public String createDataReq(){
		
		if(datarequestpojo.getTitle()!=null){
			
			try {
				Date date = new Date();
				UserJB user = (UserJB)getRequest().getSession().getAttribute(VelosGarudaConstants.GARUDA_USER);
				int userId = user.getUserId();
				Long siteid=Long.valueOf(user.getUserDetails().getUserSiteId());
				datarequestpojo.setRequesttype(getCodeListPkByTypeAndSubtype(REQUEST_TYPE, DATA_MODI_REQ));
				datarequestpojo.setCreatedon(date);
				datarequestpojo.setCreater((long) userId);
				datarequestpojo.setCbbcode(siteid);
				datarequestpojo.setRequestStatus(getCodeListPkByTypeAndSubtype(APPROVAL_STATUS, PENDING_STATUS));
				requestController.createDataRequest(datarequestpojo);
				datarequestpojo.setTitle("");
				datarequestpojo.setDataModiDescription("");
				datarequestpojo.setRequestDuedate(null);
				datarequestpojo.setCbbProcessChange("");
				datarequestpojo.setCbbcode(null);
				datarequestpojo.setAffectedFields("");
				datarequestpojo.setRequestReason("");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				log.error(e.getMessage());
				e.printStackTrace();
			}
			
		}
		return "success";
	}
	@SuppressWarnings("unchecked")
	public String viewDataReq(){
		try {
			
			
			//------------pagination code
			paginationReqScopeData(paginateSearch);
			//-----------------------end-----------------
			
			setDataModificationLst(requestController.viewDataRequest( getCodeListPkByTypeAndSubtype(REQUEST_TYPE, DATA_MODI_REQ),paginateSearch,1 ));
			// calculate next record start record  and end record 
			int iTotalRows = requestController.viewDataRequest( getCodeListPkByTypeAndSubtype(REQUEST_TYPE, DATA_MODI_REQ),paginateSearch,0).size();
			paginateSearch.setiTotalRows(iTotalRows);
			setPaginateSearch(VelosUtil.getListTotalCount(paginateSearch));
			
			
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return "success";
	}
	public String getRequestDetails(){
		try {
			setDatarequestpojo( requestController.getRequestDetails(datarequestpojo));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
			e.printStackTrace();
		}
		//viewDataReq();
		return "success";
	}
	public String updateDMRequestStatus(){
		
		try{
			String statusval=request.getParameter("statusval");
			
			if(statusval.equals("Approved")){
				datarequestpojo.setRequestStatus(getCodeListPkByTypeAndSubtype(APPROVAL_STATUS, APPROVED_STATUS));
			}
			else if(statusval.equals("Rejected")){
				datarequestpojo.setRequestStatus(getCodeListPkByTypeAndSubtype(APPROVAL_STATUS, REJECTED_STATUS));
			}
			else{
				datarequestpojo.setRequestStatus(getCodeListPkByTypeAndSubtype(APPROVAL_STATUS, COMPLETE_STATUS));
			}
			requestController.updateDMRequestStatus(datarequestpojo);
			viewDataReq();
			
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}

		return "success";
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
}
