package com.velos.ordercomponent.action;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.ordercomponent.business.domain.CodeList;
import com.velos.ordercomponent.business.pojoobjects.AdditionalIdsPojo;
import com.velos.ordercomponent.util.GarudaConfig;
import com.velos.ordercomponent.util.VelosGarudaConstants;
	
	public class UniqueIdOnBagAction extends VelosBaseAction {
		public static final Log log = LogFactory
				.getLog(UniqueIdOnBagAction.class);
	private String htmlStr;	  
	private String regId;
	private String isbt;
	private String locId;	
	private String data;
	private List<AdditionalIdsPojo> additionalIds;
	
	public List<AdditionalIdsPojo> getAdditionalIds() {
		return additionalIds;
	}

	public void setAdditionalIds(List<AdditionalIdsPojo> additionalIds) {
		this.additionalIds = additionalIds;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getRegId() {
		return regId;
	}

	public void setRegId(String regId) {
		this.regId = regId;
	}

	public String getIsbt() {
		return isbt;
	}

	public void setIsbt(String isbt) {
		this.isbt = isbt;
	}

	public String getLocId() {
		return locId;
	}

	public void setLocId(String locId) {
		this.locId = locId;
	}
	
	public String getHtmlStr() {
		return htmlStr;
	}

	public void setHtmlStr(String htmlStr) {
		this.htmlStr = htmlStr;
	}

	public String toPullDropDown() throws Exception{
		StringBuffer popUpDropDown = new StringBuffer();
		String uniqueDropDown="";
		Boolean flag = false;
		Boolean select = false;
		try{
			  popUpDropDown.append("<SELECT NAME='cbuOnBag' id='cbuOnBag' onchange='uniqueIdOnBagVal(this.value)'>"); 
			  popUpDropDown.append("<OPTION VALUE='-1' SELECTED>Select</OPTION>");
			  List<CodeList> list =  GarudaConfig.getCodeListMap().get(VelosGarudaConstants.ALL_IDS);
			  for(CodeList l : list){
				  if(l.getSubType().equals(VelosGarudaConstants.CBU_REGIS_ID) && getRegId()!=null && getRegId()!="" && !getRegId().equals("") && getRegId()!="undefined"){
					  flag = true;
					  uniqueDropDown = getRegId(); 
					  if(getData().equals(getRegId())){
						  select = true;
					  }
				  }else if(l.getSubType().equals(VelosGarudaConstants.CBU_LOCAL_ID) && getLocId()!=null && getLocId()!="" && !getLocId().equals("") && getLocId()!="undefined"){
					  flag = true;
					  uniqueDropDown = getLocId();
					  if(getData().equals(getLocId())){
						  select = true;
					  }
				  }else if(l.getSubType().equals(VelosGarudaConstants.ISBT_DIN_ID) && getIsbt()!=null && getIsbt()!="" && !getIsbt().equals("") && getIsbt()!="undefined"){
					  flag = true;
					  uniqueDropDown = getIsbt();
					  if(getData().equals(getIsbt())){
						  select = true;
					  }
				  }else if(l.getSubType().equals(VelosGarudaConstants.ADDITIONAL_ID) && additionalIds!=null && !additionalIds.isEmpty()){
					  for(AdditionalIdsPojo ap : additionalIds) {
						  if(ap!=null && ap.getAdditionalIdDesc()!=null && !ap.getAdditionalIdDesc().equals("undefined") &&  ap.getAdditionalIdDesc()!="" &&  !ap.getAdditionalIdDesc().equals("") && ap.getAdditionalIdValueType().equals("cbu")&& ap.getAdditionalId() != null && !ap.getAdditionalId().equals("undefined") && ap.getAdditionalId()!="" && !ap.getAdditionalId().equals("")) {
							  String str = getData().equals(ap.getAdditionalId())? "<OPTION selected='selected' value ='"+l.getPkCodeId()+"'>"+ap.getAdditionalId()+"</OPTION>" : "<OPTION value ='"+l.getPkCodeId()+"'>"+ap.getAdditionalId()+"</OPTION>" ;
							  popUpDropDown.append(str);
						  }
						  else {
							  popUpDropDown.append("");
						  }
						 
					  }
					 
				  }
				  if(flag){
					  if(select){
						  popUpDropDown.append("<OPTION selected='selected' value ='").append(l.getPkCodeId()).append("'>").append(uniqueDropDown).append("</OPTION>");
					  }else{
						  popUpDropDown.append("<OPTION value ='").append(l.getPkCodeId()).append("'>").append(uniqueDropDown).append("</OPTION>");
					  }					  					  
				  }
				  flag = false;
				  select = false;
			  }
			  popUpDropDown.append("</SELECT>");
			  setHtmlStr(popUpDropDown.toString());
		}catch (Exception e) {
			log.error(e.getMessage());
		  e.printStackTrace();
		 
		}
		 return "success";
	}
	
	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}
}
