package com.velos.ordercomponent.validator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.CompoundRoot;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

/**
 * @author Mohiuddin Ali Ahmed
 * @author Anurag Upadhyay
 * @version 1.0
 * 
 */

public class ISBTFormatValidator extends FieldValidatorSupport{	
	 private String defaultFieldValue="";

	public String getDefaultFieldValue() {
		return defaultFieldValue;
	}

	public void setDefaultFieldValue(String defaultFieldValue) {
		this.defaultFieldValue = defaultFieldValue;
	}

	@Override
	public void validate(Object object) throws ValidationException {
		// TODO Auto-generated method stub
		 String fieldName= getFieldName();
		 String value=(String) getFieldValue(fieldName, object);
		 CompoundRoot cRoot= ActionContext.getContext().getValueStack().getRoot();
		 String defaultValue = (String) getFieldValue(defaultFieldValue, cRoot);
		if(value!=null && defaultValue!=null && !(value.equals(defaultValue)) && !(value.length()>=1 && value.length()<=16 && value.charAt(0)!=' ' && value.charAt(value.length()-1)!=' ')){
			  addFieldError(fieldName, object);
			  return;
		}
	}

}
