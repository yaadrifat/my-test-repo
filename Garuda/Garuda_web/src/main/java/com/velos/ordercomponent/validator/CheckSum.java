package com.velos.ordercomponent.validator;

import org.apache.commons.lang.StringUtils;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.CompoundRoot;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

/**
 * @author Mohiuddin Ali Ahmed
 * @author Anurag Upadhyay
 * @version 1.0
 * 
 */

public class CheckSum extends FieldValidatorSupport {
	private boolean cdrUser;
	private String cdrUserField="";     
	private String defaultFieldValue="";
     
	public boolean isCdrUser() {
		return cdrUser;
	}

	public void setCdrUser(boolean cdrUser) {
		this.cdrUser = cdrUser;
	}	

	public String getCdrUserField() {
		return cdrUserField;
	}

	public void setCdrUserField(String cdrUserField) {
		this.cdrUserField = cdrUserField;
	}

	public String getDefaultFieldValue() {
		return defaultFieldValue;
	}

	public void setDefaultFieldValue(String defaultFieldValue) {
		this.defaultFieldValue = defaultFieldValue;
	}
	

	@Override
	public void validate(Object object) throws ValidationException {
		String fieldName= getFieldName();
		String value=(String) getFieldValue(fieldName, object);
		if(cdrUser){
			 CompoundRoot cRoot= ActionContext.getContext().getValueStack().getRoot();
			 String defaultValue = (String) getFieldValue(defaultFieldValue, cRoot);
		     boolean isCDRUser=(Boolean) getFieldValue(cdrUserField, cRoot);
		     
		     if(value!=null && defaultValue!=null && !(value.equals(defaultValue)) && isCDRUser && !checkSum(value)){			
				 addFieldError(fieldName, object);
			}else if(isCDRUser && value == null){
				addFieldError(fieldName, object);
			}
		}else if(value!=null && !checkSum(value)){
		    addFieldError(fieldName, object);
		}/*else{//if value==null
			  addFieldError(fieldName, object);
		 }*/
		
	}

	
	private Boolean checkSum(String org_id)
	{
		    if(StringUtils.isEmpty(org_id)){
		    	return false;
		    }
		    int[] chkarray = new int [9];
		  
			int chkdex; 
			int chkadd=0;
			double chksum=0; 
			double nxthighest=0;
			int idlength=0;
			int num;
			/*if(org_id.indexOf("-") !=-1){
				org_id=
			}*/
			while (org_id.indexOf("-") !=-1) {
			
			int index = org_id.indexOf("-");
			 org_id = org_id.substring(0,index)+org_id.substring(index+1,org_id.length()); 
			}
			
			int i = 0;
			//boolean flag = true;
			int j = 0;
			int len = org_id.length();
			while(j!=len){
				int charCode = (int)org_id.charAt(i);
				if((charCode>64 && charCode<91) || (charCode>96 && charCode<123) 
						|| (charCode>32 && charCode<48) || (charCode>57 && charCode<65) || (charCode>90 && charCode<97) || (charCode>172 && charCode<=126)){
					int	index = org_id.indexOf(org_id.charAt(i));
						org_id = org_id.substring(0,index)+org_id.substring(index+1,org_id.length());							
				}else{
					i++;
				}
				j++;
			}
			idlength=org_id.length();
			if(idlength==0){
				return false;
			}else{
			
			for (chkdex = 0; chkdex < idlength; chkdex++) { 
				chkarray[chkdex] = Integer.valueOf(org_id.substring(chkdex,chkdex+1));
				if((chkdex>=1)&&(chkdex%2!=0))
				{
				chkarray[chkdex] = 2 * chkarray[chkdex];				
				}
			} 
			
			chkadd = 0; 
			for(chkdex=0;chkdex<idlength-1;chkdex++)
			{
				if(chkarray[chkdex]>=10)
				{
					num=chkarray[chkdex];
					chkadd=chkadd+ (num%10);
					chkadd= chkadd + Integer.valueOf(num/10);				
					
				}
				else
				{
				chkadd=chkadd+chkarray[chkdex];
				}
			}
			
			nxthighest = (Math.floor(chkadd/10)+1)*10; 
			
			chksum = nxthighest - chkadd; 
			if (chksum == 10) {
			  chksum = 1;
			}else if (chksum == 9)
			{
			  chksum = 0;
			}else {
			  chksum = chksum + 1;
			}
			
			if (chksum == chkarray[idlength-1])
			{
				return true;				 
			}else{	
				return false;
			}
		}//end of else block
	}
}

