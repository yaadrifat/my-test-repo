package com.velos.ordercomponent.validator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.CompoundRoot;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;
import static com.velos.ordercomponent.action.CordEntryValidatorAction.EligibilityFields;

/**
 * @author Mohiuddin Ali Ahmed
 * @author Anurag Upadhyay
 * @version 1.0
 * 
 */

public class EligibilityFieldValidator extends FieldValidatorSupport{
	private boolean searchableCheck;
	private String dependencyField1="";
	
	public boolean isSearchableCheck() {
		return searchableCheck;
	}

	public void setSearchableCheck(boolean searchableCheck) {
		this.searchableCheck = searchableCheck;
	}

	public String getDependencyField1() {
		return dependencyField1;
	}

	public void setDependencyField1(String dependencyField1) {
		this.dependencyField1 = dependencyField1;
	}


	@Override
	public void validate(Object object) throws ValidationException {
		
		String value = (String) getFieldValue(getFieldName(), object);
		
		 CompoundRoot cRoot= ActionContext.getContext().getValueStack().getRoot();
		 long cordSearchable = (Long) getFieldValue("cordSearchable", cRoot); 		
		 Object obj=  getFieldValue(dependencyField1, object);
		 boolean flag=false;
		 String FieldName=getFieldName().toUpperCase();
		 boolean exist=FieldName.equals("MRQNEWQUES3ADDDETAIL") || FieldName.equals("PHYNEWFINDQUES5ADDDETAIL") || FieldName.equals("PHYNEWFINDQUES6ADDDETAIL") || FieldName.equals("IDMNEWQUES9ADDDETAIL");
		 switch(EligibilityFields.valueOf(exist?FieldName:"DEF")){
			  case MRQNEWQUES3ADDDETAIL:				
			  case PHYNEWFINDQUES5ADDDETAIL:				
			  case PHYNEWFINDQUES6ADDDETAIL:			
			  case IDMNEWQUES9ADDDETAIL:
				  flag=true;
				  break;		 
		 }
		 
		if(obj instanceof String){
		 
		 if( searchableCheck && cordSearchable==1 && (obj!=null && ((String)obj).equals("")) && (value==null || value.equals("")) ){
				addFieldError(getFieldName(), object);
				return;			
	    }
		}else if(searchableCheck && cordSearchable==1 && (obj!=null && ((Boolean)obj).equals(flag) && (value==null || value.equals("")))){//It will be instance of Boolean
			addFieldError(getFieldName(), object);
			return;
		}
		 
		
	}	

}
