package com.velos.ordercomponent.action;

public class JsonRuntimeDivAction extends VelosBaseAction {

	private String divName;
	
	public String getDivName() {
		return divName;
	}

	public void setDivName(String divName) {
		this.divName = divName;
	}

	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String getDivNameFromServer(){
		if(getSession(getRequest()).getAttribute("loadDivName")!=null){
			setDivName(getSession(request).getAttribute("loadDivName").toString());
		}
		return "success";
   }

}
