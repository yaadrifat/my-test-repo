package com.velos.ordercomponent.action;

import java.io.File;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.ordercomponent.business.domain.CdrCbu;
import com.velos.ordercomponent.business.pojoobjects.AttachmentPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUUnitReportTempPojo;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.CordHlaExtTempPojo;
import com.velos.ordercomponent.business.pojoobjects.SearchPojo;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.VelosUtil;
import com.velos.ordercomponent.controller.AttachmentController;
import com.velos.ordercomponent.controller.CBUUnitReportController;
import com.velos.ordercomponent.controller.CodelstController;
import com.velos.ordercomponent.controller.DataEntryController;
import com.velos.ordercomponent.controller.VelosSearchController;

public class DataEntryAction extends VelosBaseAction {

	public static final Log log = LogFactory.getLog(DataEntryAction.class);
	private List listViabMethod;
	private List listCfuPostMethod;
	private List listHemoPathyScr;
	private List listProcessMethod;
	private List listBagType;
	private List listProductModify;
	private List listNumOfSegments;
	private List listBacterial;
	private List listFungal;
	private List listCompStatus;
	private String cbuId;
	private List lstObject;
	private String attachmentid;
	
	private String pkcordextinfoid;
	private String pkhlaextinfo;
		
	
	private CodelstController codelstController;
	private CBUUnitReportController linkController;
	private CBUUnitReportTempPojo cbuUnitReporttempPojo;
	private CordHlaExtTempPojo cordHlaExttempPojo;
	private CdrCbuPojo cdrCbuPojo;
	private CdrCbu cdrCbu;
	private SearchPojo searchPojo;
	private VelosSearchController velosSearchController;
	@SuppressWarnings("rawtypes")
	private List cordInfoList;
	private AttachmentPojo attachmentPojo;
	private AttachmentController attachmentController;

	private File fileUpload;
	private String fileUploadContentType;
	private String fileUploadFileName;
	
	private InputStream fileInputStream;
	private DataEntryController dataentrycontrol;
	
	
	public InputStream getFileInputStream() {
		return fileInputStream;
	}
	public File getFileUpload() {
		return fileUpload;
	}

	public void setFileUpload(File fileUpload) {
		this.fileUpload = fileUpload;
	}

	public String getFileUploadContentType() {
		return fileUploadContentType;
	}

	public void setFileUploadContentType(String fileUploadContentType) {
		this.fileUploadContentType = fileUploadContentType;
	}

	public String getFileUploadFileName() {
		return fileUploadFileName;
	}
	public void setFileUploadFileName(String fileUploadFileName) {
		this.fileUploadFileName = fileUploadFileName;
	}
	
	public List getListViabMethod() {
		return listViabMethod;
	}

	public void setListViabMethod(List listViabMethod) {
		this.listViabMethod = listViabMethod;
	}

	public List getListCfuPostMethod() {
		return listCfuPostMethod;
	}

	public void setListCfuPostMethod(List listCfuPostMethod) {
		this.listCfuPostMethod = listCfuPostMethod;
	}

	public List getListHemoPathyScr() {
		return listHemoPathyScr;
	}

	public void setListHemoPathyScr(List listHemoPathyScr) {
		this.listHemoPathyScr = listHemoPathyScr;
	}

	public List getListProcessMethod() {
		return listProcessMethod;
	}

	public void setListProcessMethod(List listProcessMethod) {
		this.listProcessMethod = listProcessMethod;
	}

	public List getListBagType() {
		return listBagType;
	}

	public void setListBagType(List listBagType) {
		this.listBagType = listBagType;
	}

	public List getListProductModify() {
		return listProductModify;
	}

	public void setListProductModify(List listProductModify) {
		this.listProductModify = listProductModify;
	}

	public List getListNumOfSegments() {
		return listNumOfSegments;
	}

	public void setListNumOfSegments(List listNumOfSegments) {
		this.listNumOfSegments = listNumOfSegments;
	}

	public List getListBacterial() {
		return listBacterial;
	}

	public void setListBacterial(List listBacterial) {
		this.listBacterial = listBacterial;
	}

	public List getListFungal() {
		return listFungal;
	}

	public void setListFungal(List listFungal) {
		this.listFungal = listFungal;
	}

	public List getListCompStatus() {
		return listCompStatus;
	}

	public void setListCompStatus(List listCompStatus) {
		this.listCompStatus = listCompStatus;
	}

	public String getCbuId() {
		return cbuId;
	}

	public void setCbuId(String cbuId) {
		this.cbuId = cbuId;
	}

	public CodelstController getCodelstController() {
		return codelstController;
	}

	public void setCodelstController(CodelstController codelstController) {
		this.codelstController = codelstController;
	}

	public CBUUnitReportController getLinkController() {
		return linkController;
	}

	public void setLinkController(CBUUnitReportController linkController) {
		this.linkController = linkController;
	}

	public String getAttachmentid() {
		return attachmentid;
	}
	public void setAttachmentid(String attachmentid) {
		this.attachmentid = attachmentid;
	}
	public String getPkcordextinfoid() {
		return pkcordextinfoid;
	}
	public void setPkcordextinfoid(String pkcordextinfoid) {
		this.pkcordextinfoid = pkcordextinfoid;
	}

	public String getPkhlaextinfo() {
		return pkhlaextinfo;
	}
	public void setPkhlaextinfo(String pkhlaextinfo) {
		this.pkhlaextinfo = pkhlaextinfo;
	}

	public CBUUnitReportTempPojo getCbuUnitReporttempPojo() {
		return cbuUnitReporttempPojo;
	}

	public void setCbuUnitReporttempPojo(CBUUnitReportTempPojo cbuUnitReporttempPojo) {
		this.cbuUnitReporttempPojo = cbuUnitReporttempPojo;
	}

	public CordHlaExtTempPojo getCordHlaExttempPojo() {
		return cordHlaExttempPojo;
	}

	public void setCordHlaExttempPojo(CordHlaExtTempPojo cordHlaExttempPojo) {
		this.cordHlaExttempPojo = cordHlaExttempPojo;
	}

	public CdrCbu getCdrCbu() {
		return cdrCbu;
	}

	public void setCdrCbu(CdrCbu cdrCbu) {
		this.cdrCbu = cdrCbu;
	}

	public List getCordInfoList() {
		return cordInfoList;
	}

	public void setCordInfoList(List cordInfoList) {
		this.cordInfoList = cordInfoList;
	}

	public SearchPojo getSearchPojo() {
		return searchPojo;
	}

	public void setSearchPojo(SearchPojo searchPojo) {
		this.searchPojo = searchPojo;
	}

	public VelosSearchController getVelosSearchController() {
		return velosSearchController;
	}

	public void setVelosSearchController(
			VelosSearchController velosSearchController) {
		this.velosSearchController = velosSearchController;
	}

	public CdrCbuPojo getCdrCbuPojo() {
		return cdrCbuPojo;
	}

	public void setCdrCbuPojo(CdrCbuPojo cdrCbuPojo) {
		this.cdrCbuPojo = cdrCbuPojo;
	}

	public List getLstObject() {
		return lstObject;
	}

	public void setLstObject(List lstObject) {
		this.lstObject = lstObject;
	}

	public DataEntryAction() {
		codelstController = new CodelstController();
		linkController = new CBUUnitReportController();
		velosSearchController = new VelosSearchController();
		cbuUnitReporttempPojo = new CBUUnitReportTempPojo();
		cordHlaExttempPojo = new CordHlaExtTempPojo();
		cdrCbuPojo = new CdrCbuPojo();
		searchPojo = new SearchPojo();
		attachmentController = new AttachmentController();
		dataentrycontrol = new DataEntryController();
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String getDataScreen() {

		return "success";
	}

	public String getDetailData() throws Exception {

		setCordInfoList(getdataentryCbuAttachInfo(cdrCbuPojo.getCdrCbuId()));
		return "data";
	}

	public List<Object>  getdataentryCbuAttachInfo(String cbuid)throws Exception{
			
		return dataentrycontrol. getdataentryCbuAttachInfo(cbuid);
		

	}
		
	

	public String getDataEntryScreen() throws Exception {
		try{
		
		String cdrcbuId = request.getParameter("cdrcbuId");
		attachmentid = request.getParameter("attchmentid");
		pkcordextinfoid = request.getParameter("pkcordinfoid");
		pkhlaextinfo = request.getParameter("pkhlaextinfo");
		setPkcordextinfoid(pkcordextinfoid);
		setAttachmentid(attachmentid);
		String appendQuery = " cdrCbuId='" + cdrcbuId + "'";
		searchPojo.setSearchType(CDRCBU_OBJECTNAME);
		searchPojo.setCriteria(appendQuery);
		searchPojo = velosSearchController.velosSearch(searchPojo);

		setLstObject(searchPojo.getResultlst());
		List i = getLstObject();
		Iterator<CdrCbu> it = i.iterator();

		while (it.hasNext()) {
			cdrCbu = it.next();
			
		}

		Object object = ObjectTransfer.transferObjects(cdrCbu, getCdrCbuPojo());
		cdrCbuPojo = (CdrCbuPojo) object;
		setCdrCbuPojo(cdrCbuPojo);
		//getObjectList(cdrCbu.getCordID());
		//getCbuInfo();
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
			//log.debug("Exception for -->"+e);
		}
		return "success";
	}
	
	/*private List getObjectList(Long cbuId){
		String query="select "
			
			select pk_cord_ext_info,fk_attachment_id from garuda_cord_ext_info where fk_cord_cdr_cbu_id='4'
		return null;
		
	}*/

	

	public void getCbuInfo() throws Exception {
		// String cbuId="cbu001";
		// Long cbuId=Long.parseLong(cdrcbuId);
		// String cbuId=request.getParameter("cbuId");
		// if(cbuId!=null && !cbuId.equals("")){
		// cbuId = Long.parseLong(request.getParameter("cbuId"));
		// }
		System.out.print("get cbu info cbu id" + getCdrCbuPojo().getCdrCbuId());
		
		// if(getCdrCbuPojo()!=null)
		// {
		// cbuId = getCdrCbuPojo().getCdrCbuId();
		// }
		//log.debug("getCbuId()--"+getCbuId());
		if (getCbuId() != null && getCbuId() != "") {
			cbuId = getCbuId();
		}
		if (cbuId != null || cbuId != "") {
			try {

				cdrCbu = (CdrCbu) linkController.getCbuInfo(CDRCBU_OBJECTNAME,
						cbuId, CORD_CDR_CBU_ID);
				/*
				 * if(cdrCbu.getCbuCollectionDate()!=null &&
				 * !cdrCbu.getCbuCollectionDate().equals("")) { String
				 * entitydate=dateFormat.format(comm.getEntitydate());
				 * 
				 * //log.debug("entityDate.....:::"+entitydate); }
				 */
				setCdrCbuPojo((CdrCbuPojo) ObjectTransfer.transferObjects(
						cdrCbu, cdrCbuPojo));
				// //log.debug("CdrCbuPojo In"+cdrCbuPojo.getLocalCbuId());

			} catch (Exception e) {
				log.error(e.getMessage());
				e.printStackTrace();
			}
		}

	}
	
	public String saveCBUUnitReport() throws Exception{
		//log.debug("save unit Report called....");
		
		
		if(getCdrCbuPojo().getCordID()!=null)
		{
			getCbuUnitReporttempPojo().setFkCordCdrCbuId(getCdrCbuPojo().getCordID());
			getCordHlaExttempPojo().setFkCordCdrCbuId(getCdrCbuPojo().getCordID());
		}//CBUUnitReportTempPojo.cordViabMethod
		//log.debug("--getCordId--"+getCdrCbuPojo().getCordID());
		//log.debug("11"+getCbuUnitReporttempPojo().getCordViabMethod());
		//log.debug("22"+getCbuUnitReporttempPojo().getCordCfuPostMethod());
		
		//cordHlaExttempPojo.cordHlaTwice
		//log.debug("request.getParameter(attachmentid)---"+request.getParameter("attachmentid"));
		//log.debug("request.getParameter(pkcordextinfoid)"+request.getParameter("pkcordextinfoid"));
		
		//log.debug(cbuUnitReporttempPojo.getCordCompletedStatus());
		cbuUnitReporttempPojo.setAttachmentId(Long.parseLong(request.getParameter("attachmentid")));
		cbuUnitReporttempPojo.setFkCordExtInfo(Long.parseLong(request.getParameter("pkcordextinfoid")));
		cordHlaExttempPojo.setFkCordHlaExt(Long.parseLong(request.getParameter("pkhlaextinfo")));
		//CBUUnitReportTempPojo
		cbuUnitReporttempPojo = getCbuUnitReporttempPojo();
		cbuUnitReporttempPojo=(CBUUnitReportTempPojo)  new VelosUtil().setAuditableInfo( cbuUnitReporttempPojo);
		cordHlaExttempPojo = getCordHlaExttempPojo();
		cordHlaExttempPojo = (CordHlaExtTempPojo) new VelosUtil().setAuditableInfo(cordHlaExttempPojo);
		//Study study = null;
		//getCodelstDetails();
		try{
			this.attachmentid="";
			this.pkcordextinfoid="";
			this.pkhlaextinfo="";
		cbuUnitReporttempPojo = linkController.saveCBUUnitTempReport(cbuUnitReporttempPojo);
		this.attachmentid=cbuUnitReporttempPojo.getAttachmentId()+"";
		this.pkcordextinfoid=cbuUnitReporttempPojo.getFkCordExtInfo()+"";
		this.pkhlaextinfo=cordHlaExttempPojo.getFkCordHlaExt()+"";
		//log.debug("============================================================="+cbuUnitReporttempPojo.getFkCordExtInfo());
		
		//log.debug("cbuUnitReporttempPojo.getPkCordExtInfoTemp()------->"+cbuUnitReporttempPojo.getPkCordExtInfoTemp());
		cordHlaExttempPojo.setFkCordExtInfoTemp(cbuUnitReporttempPojo.getPkCordExtInfoTemp());
		//log.debug("---before save hla---");
		linkController.saveHlaTempInfo(cordHlaExttempPojo);
		
		// to do set dropdown values
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}
		//-----------
		
		String cdrcbuId = request.getParameter("cdrcbuId");

		String appendQuery = " cdrCbuId='" + cdrcbuId + "'";
		// //log.debug("appendQuery-->"+appendQuery);
		searchPojo.setSearchType(CDRCBU_OBJECTNAME);
		searchPojo.setCriteria(appendQuery);
		searchPojo = velosSearchController.velosSearch(searchPojo);

		setLstObject(searchPojo.getResultlst());
		List i = getLstObject();
		Iterator<CdrCbu> it = i.iterator();

		while (it.hasNext()) {
			cdrCbu = it.next();
		}

		Object object = ObjectTransfer.transferObjects(cdrCbu, getCdrCbuPojo());
		cdrCbuPojo = (CdrCbuPojo) object;
		setCdrCbuPojo(cdrCbuPojo);
		//---------------
		return SUCCESS;
	}
	
	public String getDocuments() throws Exception{
		
		
		String attachmentId = request.getParameter("attachmentId");
		
		try{
			//log.debug("inside getdocuments method in dataentryAction------"+attachmentId);
			
			Object obj = attachmentController.getAttachment(DOCUMENT_OBJECTNAME, attachmentId, DOCUMENT_ID);
			//log.debug("attachment--->"+obj);
			attachmentPojo = new AttachmentPojo();
			attachmentPojo = (AttachmentPojo)ObjectTransfer.transferObjects(obj, attachmentPojo);
			fileUploadContentType = attachmentPojo.getDocumentType();
			fileUploadFileName = attachmentPojo.getFileName();
			fileInputStream = attachmentPojo.getDocumentFile().getBinaryStream();
		}catch(Exception e){
			//log.debug("-------------------"+e);
			log.error(e.getMessage());
			e.printStackTrace();
			
			
		}
	
		return SUCCESS;
		
		
		
		
		/*String cdrcbuId = request.getParameter("cdrcbuId");
		List list=getPdfList(cdrcbuId);
		
		Iterator iterator=list.iterator();
		
		while(iterator.hasNext()){
			Object[] object = (Object[])iterator.next();
			//log.debug("objectobject--"+object.length);
			for (int i = 0; i < object.length; i++) {
				String contentType = object[0].toString();
				Blob b = (Blob) object[1];
				String filename = object[2].toString();
				
				//log.debug(object[i]);
				byte arr[]=new byte[(int)b.length()];
				arr=b.getBytes(1,(int)b.length());
				//log.debug("myImage"+"=-------=="+request.getContextPath());
				
				//log.debug("path---"+request.getRealPath("/")+"uploadedFiles"+File.separator+filename);
				
				File document = new File(request.getRealPath("/")+"pages/images/files"+File.separator+filename);
				FileOutputStream fos = new FileOutputStream(document);
				fos.write(arr);
				fos.close();
				
				
			}
			
			
		}
		  
	    
		return "pdf";*/
	}
	
/*	public List getPdfList(String cdrcbuId){
		String appendQuery="select att.documentType,att.documentFile,att.garudaAttachmentsTypeRem from Attachment att, CBUUnitReportTemp extemp, CdrCbu cordinfo " +
		"where att.attachmentId = extemp.attachmentId and extemp.fkCordCdrCbuId = cordinfo.cdrCbuId and lower(cordinfo.cdrCbuId) like lower('%"+ cdrcbuId + "%')" ;
		return velosHelper.getObjectList(appendQuery);
	}
	*/
	

}
