package com.velos.ordercomponent.util;

import com.velos.ordercomponent.controller.CBBController;
import com.velos.ordercomponent.controller.CBUController;
import com.velos.ordercomponent.controller.ClinicalNoteController;
import com.velos.ordercomponent.controller.DynaFormController;
import com.velos.ordercomponent.controller.LabTestcontroller;
import com.velos.ordercomponent.controller.OpenOrderController;
import com.velos.ordercomponent.controller.SpecimenController;
import com.velos.ordercomponent.controller.VDAController;

public class ObjectFactory {
	
	public static MakeObjects obj = getInstance();

	public static class MakeObjects{
		
		static CBUController cbuController;
		static CBBController cbbController;
		static SpecimenController specimenController;
		static DynaFormController dynaFormController;
		static ClinicalNoteController clinicalNoteController;
		static LabTestcontroller labTestcontroller;
		static OpenOrderController linkController;
		static VDAController vdaController;
		
		private MakeObjects() {
			if(cbuController==null)
			  cbuController = new CBUController();
			if(cbbController==null)
				cbbController = new CBBController();
			if(specimenController==null)
				specimenController = new SpecimenController();
			if(dynaFormController==null)
				dynaFormController = new DynaFormController();
			if(clinicalNoteController==null)
				clinicalNoteController = new ClinicalNoteController();
			if(labTestcontroller==null)
				labTestcontroller = new LabTestcontroller();
			if(linkController==null)
				linkController = new OpenOrderController();
			if(vdaController==null)
				vdaController = new VDAController();
			
		}
		
		public static Object getFactoryObject(String className){
			Object obj = null;
			if(CBUController.class.getName().equalsIgnoreCase(className)){				
					obj = cbuController;
			}
			if(CBBController.class.getName().equalsIgnoreCase(className)){
				obj = cbbController;
			}
			if(SpecimenController.class.getName().equalsIgnoreCase(className)){
				obj = specimenController;
			}
			if(DynaFormController.class.getName().equalsIgnoreCase(className)){
				obj = dynaFormController;
			}
			if(ClinicalNoteController.class.getName().equalsIgnoreCase(className)){
				obj = clinicalNoteController;
			}
			if(LabTestcontroller.class.getName().equalsIgnoreCase(className)){
				obj = labTestcontroller;
			}
			if(OpenOrderController.class.getName().equalsIgnoreCase(className))
			{
				obj = linkController;
			}
			if(VDAController.class.getName().equalsIgnoreCase(className))
			{
				obj = vdaController;
			}
			return obj;
		}	
	}
	
	public static MakeObjects getInstance(){
		return new MakeObjects();
	}
}