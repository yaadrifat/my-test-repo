package com.velos.ordercomponent.action;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.eres.web.user.UserJB;
import com.velos.ordercomponent.business.domain.CBBProcedures;
import com.velos.ordercomponent.business.domain.Site;
import com.velos.ordercomponent.business.pojoobjects.AddressPojo;
import com.velos.ordercomponent.business.pojoobjects.CBBPojo;
import com.velos.ordercomponent.business.pojoobjects.CBBProcedureInfoPojo;
import com.velos.ordercomponent.business.pojoobjects.CBBProcedurePojo;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.EntitySamplesPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.pojoobjects.PersonPojo;
import com.velos.ordercomponent.business.pojoobjects.SitePojo;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.Utilities;
import com.velos.ordercomponent.business.util.VelosUtil;
import com.velos.ordercomponent.controller.AddressController;
import com.velos.ordercomponent.controller.CBBController;
import com.velos.ordercomponent.controller.PersonController;
import com.velos.ordercomponent.util.VelosGarudaConstants;
import com.velos.ordercomponent.business.domain.CodeList;
import com.velos.ordercomponent.action.MailAction;

/**
 * @author Mohiuddin Ali Ahmed
 * 
 */
public class CBBAction extends VelosBaseAction {

	/**
	 * 
	 */
	public static final Log log = LogFactory.getLog(CBBAction.class);
	private static final long serialVersionUID = 1L;
	CBBController cbbController;
	private CBBPojo cbbPojo;
	private List<CBBProcedurePojo> cbbList;
	private CBBProcedurePojo cbbProcedurePojo;
	private Long[] cbbDefault;
	private String firstName;
	private String lastName;
	private PersonController personController;
	private AddressController addressController;
	private List<CBBProcedureInfoPojo> cbbProdList;
	private CBBProcedureInfoPojo cbbProcedureInfoPojo;
	private String sopStrtDateStrAdd;
	private String sopTermiDateStrAdd;
	private String procStartDateStrAdd;
	private String procTermiDateStrAdd;
	private String sopStrtDateStr;
	private String sopTermiDateStr;
	private String procStartDateStr;
	private String procTermiDateStr;
	private CBBProcedures cbbProcedures;
	DateFormat dat = new SimpleDateFormat("MMM dd, yyyy");
	private Long fkProcessingId;
	private Boolean statusUpdate;
	private String fkProcId;
	private String rcntPkProcId;
	private List list;
	private Long selectCbb;
	private SitePojo site;
	private String flag;
	private String pksite;
	private CdrCbuPojo cdrCbuPojo;
	private List<SitePojo> sites;
	private Long nmdpCbuIdLocalAssgnId;
	private Long autoMaticSequencingLocalAssgnId;
	private Long userDefindeLocalAssgnId;
	private Long[] fkAccreditation;	
	private List<SitePojo> cbuStrLocationList;
	private List<SitePojo> cbuCollectionList;
	private Long fkCbuStorage;
	private Long fkCbuCollection;
	private Long[] cbuOnBag; 
	private EntitySamplesPojo entitySamplesPojo;
	private String procProcAssignToCordFlag;
	private PaginateSearch paginateSearch;
	private String assignedToCordFlag;
	private String procNameExistsFlag;
	private String diableCBBEditAndPrintFlag;
	private String cbbType;
	private String linkDesc;
	private String linkUrl;
	private String esignFlag;
	private List<SitePojo> sitePojoArr;
	private List<SitePojo> CollSitePojoArr;
	private String siteExist;
	private String cbuCollectionPro;
	private String checkDefer;
	private String procCordsMaxCollDt;
	private String cbbIdExists;
	private List<CBBProcedurePojo> cbbProcList;
	private String fprocedureName;
	private String fprocStrDt1;
	private String fprocStrDt2;
	private String fprocTerDt1;
	private String fprocTerDt2;
	private ServerOrderBy serverorderby;
	private ServerGlobalSearch serverglobalsearch;
	
	
	
	public String getFprocStrDt1() {
		return fprocStrDt1;
	}

	public void setFprocStrDt1(String fprocStrDt1) {
		this.fprocStrDt1 = fprocStrDt1;
	}

	
	public String getFprocedureName() {
		return fprocedureName;
	}

	public String getFprocStrDt2() {
		return fprocStrDt2;
	}

	public String getFprocTerDt1() {
		return fprocTerDt1;
	}

	public String getFprocTerDt2() {
		return fprocTerDt2;
	}

	public void setFprocedureName(String fprocedureName) {
		this.fprocedureName = fprocedureName;
	}

	public void setFprocStrDt2(String fprocStrDt2) {
		this.fprocStrDt2 = fprocStrDt2;
	}

	public void setFprocTerDt1(String fprocTerDt1) {
		this.fprocTerDt1 = fprocTerDt1;
	}

	public void setFprocTerDt2(String fprocTerDt2) {
		this.fprocTerDt2 = fprocTerDt2;
	}

	public List<CBBProcedurePojo> getCbbProcList() {
		return cbbProcList;
	}

	public void setCbbProcList(List<CBBProcedurePojo> cbbProcList) {
		this.cbbProcList = cbbProcList;
	}

	public String getCbbIdExists() {
		return cbbIdExists;
	}

	public void setCbbIdExists(String cbbIdExists) {
		this.cbbIdExists = cbbIdExists;
	}

	public String getProcCordsMaxCollDt() {
		return procCordsMaxCollDt;
	}

	public void setProcCordsMaxCollDt(String procCordsMaxCollDt) {
		this.procCordsMaxCollDt = procCordsMaxCollDt;
	}

	public String getCheckDefer() {
		return checkDefer;
	}

	public void setCheckDefer(String checkDefer) {
		this.checkDefer = checkDefer;
	}

	public List<SitePojo> getCollSitePojoArr() {
		return CollSitePojoArr;
	}

	public void setCollSitePojoArr(List<SitePojo> collSitePojoArr) {
		CollSitePojoArr = collSitePojoArr;
	}

	public String getCbuCollectionPro() {
		return cbuCollectionPro;
	}

	public void setCbuCollectionPro(String cbuCollectionPro) {
		this.cbuCollectionPro = cbuCollectionPro;
	}

	public String getSiteExist() {
		return siteExist;
	}

	public void setSiteExist(String siteExist) {
		this.siteExist = siteExist;
	}

	public String getEsignFlag() {
		return esignFlag;
	}

	public void setEsignFlag(String esignFlag) {
		this.esignFlag = esignFlag;
	}

	public String getLinkDesc() {
		return linkDesc;
	}

	public void setLinkDesc(String linkDesc) {
		this.linkDesc = linkDesc;
	}

	public String getLinkUrl() {
		return linkUrl;
	}

	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}

	public String getCbbType() {
		return cbbType;
	}

	public void setCbbType(String cbbType) {
		this.cbbType = cbbType;
	}

	public String getDiableCBBEditAndPrintFlag() {
		return diableCBBEditAndPrintFlag;
	}

	public void setDiableCBBEditAndPrintFlag(String diableCBBEditAndPrintFlag) {
		this.diableCBBEditAndPrintFlag = diableCBBEditAndPrintFlag;
	}

	public String getProcNameExistsFlag() {
		return procNameExistsFlag;
	}

	public void setProcNameExistsFlag(String procNameExistsFlag) {
		this.procNameExistsFlag = procNameExistsFlag;
	}

	public String getAssignedToCordFlag() {
		return assignedToCordFlag;
	}

	public void setAssignedToCordFlag(String assignedToCordFlag) {
		this.assignedToCordFlag = assignedToCordFlag;
	}

	public PaginateSearch getPaginateSearch() {
		return paginateSearch;
	}

	public void setPaginateSearch(PaginateSearch paginateSearch) {
		this.paginateSearch = paginateSearch;
	}

	public String getProcProcAssignToCordFlag() {
		return procProcAssignToCordFlag;
	}

	public void setProcProcAssignToCordFlag(String procProcAssignToCordFlag) {
		this.procProcAssignToCordFlag = procProcAssignToCordFlag;
	}

	public EntitySamplesPojo getEntitySamplesPojo() {
		return entitySamplesPojo;
	}

	public void setEntitySamplesPojo(EntitySamplesPojo entitySamplesPojo) {
		this.entitySamplesPojo = entitySamplesPojo;
	}

	public Long[] getCbuOnBag() {
		return cbuOnBag;
	}

	public void setCbuOnBag(Long[] cbuOnBag) {
		this.cbuOnBag = cbuOnBag;
	}

	public List getCbuStrLocationList() {
		return cbuStrLocationList;
	}

	public void setCbuStrLocationList(List cbuStrLocationList) {
		this.cbuStrLocationList = cbuStrLocationList;
	}

	public List getCbuCollectionList() {
		return cbuCollectionList;
	}

	public void setCbuCollectionList(List cbuCollectionList) {
		this.cbuCollectionList = cbuCollectionList;
	}

	public Long getFkCbuStorage() {
		return fkCbuStorage;
	}

	public void setFkCbuStorage(Long fkCbuStorage) {
		this.fkCbuStorage = fkCbuStorage;
	}

	public Long getFkCbuCollection() {
		return fkCbuCollection;
	}

	public void setFkCbuCollection(Long fkCbuCollection) {
		this.fkCbuCollection = fkCbuCollection;
	}

	
	public CdrCbuPojo getCdrCbuPojo() {
		if(cdrCbuPojo!=null && !cdrCbuPojo.equals("")){
			
		}else{
			cdrCbuPojo = new CdrCbuPojo();
		}
		return cdrCbuPojo;
	}

	public void setCdrCbuPojo(CdrCbuPojo cdrCbuPojo) {
		this.cdrCbuPojo = cdrCbuPojo;
	}

	public String getPksite() {
		return pksite;
	}

	public void setPksite(String pksite) {
		this.pksite = pksite;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public Long getSelectCbb() {
		return selectCbb;
	}

	public void setSelectCbb(Long selectCbb) {
		this.selectCbb = selectCbb;
	}

	public String getFkProcId() {
		return fkProcId;
	}

	public void setFkProcId(String fkProcId) {
		this.fkProcId = fkProcId;
	}

	public Boolean getStatusUpdate() {
		return statusUpdate;
	}

	public void setStatusUpdate(Boolean statusUpdate) {
		this.statusUpdate = statusUpdate;
	}

	public String getSopStrtDateStrAdd() {
		return sopStrtDateStrAdd;
	}

	public void setSopStrtDateStrAdd(String sopStrtDateStrAdd) {
		this.sopStrtDateStrAdd = sopStrtDateStrAdd;
	}

	public String getSopTermiDateStrAdd() {
		return sopTermiDateStrAdd;
	}

	public void setSopTermiDateStrAdd(String sopTermiDateStrAdd) {
		this.sopTermiDateStrAdd = sopTermiDateStrAdd;
	}

	public String getProcStartDateStrAdd() {
		return procStartDateStrAdd;
	}

	public void setProcStartDateStrAdd(String procStartDateStrAdd) {
		this.procStartDateStrAdd = procStartDateStrAdd;
	}

	public String getProcTermiDateStrAdd() {
		return procTermiDateStrAdd;
	}

	public void setProcTermiDateStrAdd(String procTermiDateStrAdd) {
		this.procTermiDateStrAdd = procTermiDateStrAdd;
	}

	public Long getFkProcessingId() {
		return fkProcessingId;
	}

	public void setFkProcessingId(Long fkProcessingId) {
		this.fkProcessingId = fkProcessingId;
	}

	public CBBProcedures getCbbProcedures() {
		return cbbProcedures;
	}

	public void setCbbProcedures(CBBProcedures cbbProcedures) {
		this.cbbProcedures = cbbProcedures;
	}

	public CBBProcedureInfoPojo getCbbProcedureInfoPojo() {
		return cbbProcedureInfoPojo;
	}

	public void setCbbProcedureInfoPojo(
			CBBProcedureInfoPojo cbbProcedureInfoPojo) {
		this.cbbProcedureInfoPojo = cbbProcedureInfoPojo;
	}

	public List<CBBProcedureInfoPojo> getCbbProdList() {
		return cbbProdList;
	}

	public void setCbbProdList(List<CBBProcedureInfoPojo> cbbProdList) {
		this.cbbProdList = cbbProdList;
	}

	public CBBProcedurePojo getCbbProcedurePojo() {
		return cbbProcedurePojo;
	}

	public void setCbbProcedurePojo(CBBProcedurePojo cbbProcedurePojo) {
		this.cbbProcedurePojo = cbbProcedurePojo;
	}

	public CBBPojo getCbbPojo() {
		return cbbPojo;
	}

	public void setCbbPojo(CBBPojo cbbPojo) {
		this.cbbPojo = cbbPojo;
	}

	public List<CBBProcedurePojo> getCbbList() {
		return cbbList;
	}

	public void setCbbList(List<CBBProcedurePojo> cbbList) {
		this.cbbList = cbbList;
	}
	
	public List<SitePojo> getSites() {
		return sites;
	}

	public void setSites(List<SitePojo> sites) {
		this.sites = sites;
	}
	
	public CBBAction() {
		cbbController = new CBBController();
		personController = new PersonController();
		addressController = new AddressController();
		cbbProcedures = new CBBProcedures();
		cbbProcedureInfoPojo = new CBBProcedureInfoPojo();
		cbbProcedurePojo = new CBBProcedurePojo();
		paginateSearch = new PaginateSearch();
		serverorderby=new ServerOrderBy();
		serverglobalsearch=new ServerGlobalSearch();
		list = new ArrayList();
	}

	public Long[] getCbbDefault() {
		return cbbDefault;
	}

	public void setCbbDefault(Long[] cbbDefault) {
		this.cbbDefault = cbbDefault;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}	

	public SitePojo getSite() {
		return site;
	}

	public void setSite(SitePojo site) {
		this.site = site;
	}
	
	public Long getNmdpCbuIdLocalAssgnId() {
		return nmdpCbuIdLocalAssgnId;
	}

	public void setNmdpCbuIdLocalAssgnId(Long nmdpCbuIdLocalAssgnId) {
		this.nmdpCbuIdLocalAssgnId = nmdpCbuIdLocalAssgnId;
	}

	public Long getAutoMaticSequencingLocalAssgnId() {
		return autoMaticSequencingLocalAssgnId;
	}

	public void setAutoMaticSequencingLocalAssgnId(
			Long autoMaticSequencingLocalAssgnId) {
		this.autoMaticSequencingLocalAssgnId = autoMaticSequencingLocalAssgnId;
	}

	public Long getUserDefindeLocalAssgnId() {
		return userDefindeLocalAssgnId;
	}

	public void setUserDefindeLocalAssgnId(Long userDefindeLocalAssgnId) {
		this.userDefindeLocalAssgnId = userDefindeLocalAssgnId;
	}

	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getSopStrtDateStr() {
		return sopStrtDateStr;
	}

	public void setSopStrtDateStr(String sopStrtDateStr) {
		this.sopStrtDateStr = sopStrtDateStr;
	}

	public String getSopTermiDateStr() {
		return sopTermiDateStr;
	}

	public void setSopTermiDateStr(String sopTermiDateStr) {
		this.sopTermiDateStr = sopTermiDateStr;
	}

	public String getProcStartDateStr() {
		return procStartDateStr;
	}

	public void setProcStartDateStr(String procStartDateStr) {
		this.procStartDateStr = procStartDateStr;
	}

	public String getProcTermiDateStr() {
		return procTermiDateStr;
	}

	public void setProcTermiDateStr(String procTermiDateStr) {
		this.procTermiDateStr = procTermiDateStr;
	}

	public String getRcntPkProcId() {
		return rcntPkProcId;
	}

	public void setRcntPkProcId(String rcntPkProcId) {
		this.rcntPkProcId = rcntPkProcId;
	}

	public List getList() {
		return list;
	}

	public void setList(List list) {
		this.list = list;
	}
	
	

	public Long[] getFkAccreditation() {
		return fkAccreditation;
	}

	public void setFkAccreditation(Long[] fkAccreditation) {
		this.fkAccreditation = fkAccreditation;
	}
	
	public List<SitePojo> getSitePojoArr() {
		return sitePojoArr;
	}

	public void setSitePojoArr(List<SitePojo> sitePojoArr) {
		this.sitePojoArr = sitePojoArr;
	}

	public String getCBBDetails() throws Exception {
		String orderBy = "";
		int iTotalRows = 0;
		List<SitePojo> cbbList = null;
		List countVal=new ArrayList();
		setFlag(request.getParameter("flagval"));
		setPksite(request.getParameter("pksiteid"));
		//log.debug("Flagval--------------"+getFlag()+"-pkcbb id--------"+getPksite());
		orderBy = request.getParameter("orderBy");
		if(orderBy == null || orderBy == "")
			orderBy = "";
		paginationReqScopeData(paginateSearch);
		String condition="";
		
		if(request.getParameter("filterTxt")!=null && !request.getParameter("filterTxt").trim().equals("")){
			String inputTxt=request.getParameter("filterTxt");
			condition=serverglobalsearch.FetchGlobalSearchCBBProfile(inputTxt);
		}
		
		
		
		countVal=cbbController.getSitesCount(paginateSearch,1,condition);
		
		if(countVal!=null && countVal.size()>0){
			String size=countVal.get(0).toString();
			iTotalRows=Integer.parseInt(size);
		}
		paginateSearch.setiTotalRows(iTotalRows);
		setPaginateSearch(VelosUtil.getListTotalCount(paginateSearch));
		request.setAttribute("paginateSearch", paginateSearch);
		getRequest().setAttribute("CbbList", cbbList);
		getRequest().setAttribute("CbbDefaultList", null);
		return SUCCESS;
	}

	public String showCbbDefaults() throws Exception {
	List<SitePojo> sitePojoArr= null;
			sitePojoArr = new ArrayList<SitePojo>();
			
	List<SitePojo> CollSitePojoArr= null;
		CollSitePojoArr = new ArrayList<SitePojo>();		
			
		
		List<SitePojo> cbbList1 = cbbController.getSites();
		getRequest().setAttribute("CbbList", cbbList1);
		List<SitePojo> cbbList = cbbController
				.getCBBDetailsByIds(getCbbDefault());
		getRequest().setAttribute("CbbDefaultList", cbbList);
		setSelectCbb(getCbbDefault()[0]);
		setCbbDefault(getCbbDefault());
		if(cbbList.size() > 0)
		{
			Map<String, List<Long>> map = new CBUAction().setMultipleValues(VelosGarudaConstants.ENTITY_CBB,cbbList.get(0).getCbb().getPkCbbId());
			List<Long> fkAccreditationArr = map.get(VelosGarudaConstants.ACCREDITITAON);
				if(fkAccreditationArr!=null && fkAccreditationArr.size()>0){			
				 setFkAccreditation(fkAccreditationArr.toArray(new Long[0]));
				}
		}
		if(cbbList.get(0).getSiteCodelstType()!= null){
		setCbbType(getCBBType(cbbList.get(0).getSiteCodelstType().toString()));
		}
		setAutoMaticSequencingLocalAssgnId(getCodeListPkByTypeAndSubtype(LOCAL_ASSIGNMENT_ID, AUTOMATIC_SEQUENCING_LOCAL_ASSIGNMENT_ID));

		cbuStrLocationList=(List<SitePojo>) new CBUAction().setMultipleValues(VelosGarudaConstants.SITE_TYPE,getCbbDefault()[0],VelosGarudaConstants.CBU_STORAGE);
		setCbuStrLocationList(cbuStrLocationList);	
		
		cbuCollectionList=(List<SitePojo>) new CBUAction().setMultipleValues(VelosGarudaConstants.SITE_TYPE,getCbbDefault()[0],VelosGarudaConstants.CBU_COLLECTION);
		setCbuCollectionList(cbuCollectionList);
		
		
		sitePojoArr = cbbController.getMultipleValuesByErSite(getCbbDefault()[0],VelosGarudaConstants.SITE_TYPE,VelosGarudaConstants.CBU_STORAGE,1);
		setSitePojoArr(sitePojoArr);
		
		CollSitePojoArr = cbbController.getMultipleValuesByErSite(getCbbDefault()[0],VelosGarudaConstants.SITE_TYPE,VelosGarudaConstants.CBU_COLLECTION,1);
		setCollSitePojoArr(CollSitePojoArr);	
							
		if(request.getParameter("diableCBBEditAndPrintFlag")!=null && !request.getParameter("diableCBBEditAndPrintFlag").equals("")){
			setDiableCBBEditAndPrintFlag(request.getParameter("diableCBBEditAndPrintFlag"));
		}
		
		if(cbbList.size()>0){
			if(cbbList.get(0).getCbb()!=null){
				setFkCbuStorage(cbbList.get(0).getCbb().getFkCbuStorage());
				setFkCbuCollection(cbbList.get(0).getCbb().getFkCbuCollection());
			}
		}
			Map<String, List<Long>> multipleMap = new CBUAction().setMultipleValues(VelosGarudaConstants.ENTITY_CBB,getCbbDefault()[0]);
			List<Long> cbuOnBagArr = multipleMap.get(VelosGarudaConstants.ALL_IDS);
			
			
			if(cbuOnBagArr!=null && cbuOnBagArr.size()>0){			
			  setCbuOnBag(cbuOnBagArr.toArray(new Long[0]));
			  new CBUAction().setIdsOnBag(getCbuOnBag());
			}			
		
		return SUCCESS;
	}

	public String editCBBDefaults() throws Exception {
		List<SitePojo> cbbList = cbbController.getCBBDetails(getSite().getSiteId());
		List<String> linkList = null;
		
		List<SitePojo> sitePojoArr= null;
		sitePojoArr = new ArrayList<SitePojo>();
		
		List<SitePojo> CollSitePojoArr= null;
		CollSitePojoArr = new ArrayList<SitePojo>();
		
		
		setNmdpCbuIdLocalAssgnId(getCodeListPkByTypeAndSubtype(LOCAL_ASSIGNMENT_ID, NMDP_LOCAL_ASSIGNMENT_ID));
		setUserDefindeLocalAssgnId(getCodeListPkByTypeAndSubtype(LOCAL_ASSIGNMENT_ID, USER_DEFINED_LOCAL_ASSIGNMENT_ID));
		setAutoMaticSequencingLocalAssgnId(getCodeListPkByTypeAndSubtype(LOCAL_ASSIGNMENT_ID, AUTOMATIC_SEQUENCING_LOCAL_ASSIGNMENT_ID));
		
		if (cbbList.size() > 0) {
			setCbbPojo(cbbList.get(0).getCbb());
			setSite(cbbList.get(0));
		}
		if(getSite().getSiteCodelstType()!= null){		
		setCbbType(getCBBType(getSite().getSiteCodelstType().toString()));
		}
		AttachmentAction attAction = new AttachmentAction();		
		linkList= attAction.getnmdpLinkFrCbbProfile();	
		if(linkList!= null){
			setLinkDesc(linkList.get(0));
			setLinkUrl(linkList.get(1));
		}
		Map<String, List<Long>> map = new CBUAction().setMultipleValues(VelosGarudaConstants.ENTITY_CBB,cbbList.get(0).getCbb().getPkCbbId());
		List<Long> fkAccreditationArr = map.get(VelosGarudaConstants.ACCREDITITAON);
		if(fkAccreditationArr!=null && fkAccreditationArr.size()>0){			
		 setFkAccreditation(fkAccreditationArr.toArray(new Long[0]));
		}
		
		cbuStrLocationList=(List<SitePojo>) new CBUAction().setMultipleValues(VelosGarudaConstants.SITE_TYPE,getSite().getSiteId(),VelosGarudaConstants.CBU_STORAGE);
		setCbuStrLocationList(cbuStrLocationList);
		
		cbuCollectionList=(List<SitePojo>) new CBUAction().setMultipleValues(VelosGarudaConstants.SITE_TYPE,getSite().getSiteId(),VelosGarudaConstants.CBU_COLLECTION);
		setCbuCollectionList(cbuCollectionList);
		
		
		sitePojoArr = cbbController.getMultipleValuesByErSite(getSite().getSiteId(),VelosGarudaConstants.SITE_TYPE,VelosGarudaConstants.CBU_STORAGE,1);
		setSitePojoArr(sitePojoArr);
		
		CollSitePojoArr = cbbController.getMultipleValuesByErSite(getSite().getSiteId(),VelosGarudaConstants.SITE_TYPE,VelosGarudaConstants.CBU_COLLECTION,1);
		setCollSitePojoArr(CollSitePojoArr);
			Map<String, List<Long>> multipleMap = new CBUAction().setMultipleValues(VelosGarudaConstants.ENTITY_CBB,getSite().getSiteId());
			List<Long> cbuOnBagArr = multipleMap.get(VelosGarudaConstants.ALL_IDS);
			
			
			if(cbuOnBagArr!=null && cbuOnBagArr.size()>0){			
			  setCbuOnBag(cbuOnBagArr.toArray(new Long[0]));
			  new CBUAction().setIdsOnBag(getCbuOnBag());
			}					
				
		setFkCbuStorage(cbbList.get(0).getCbb().getFkCbuStorage());
		request.setAttribute("fkCbuStorage", getFkCbuStorage());
		setFkCbuCollection(cbbList.get(0).getCbb().getFkCbuCollection());
		request.setAttribute("fkCbuCollection", getFkCbuCollection());		
		return SUCCESS;
	}
	
	public void sessionProcTbl(){
		
		
		/********************************Set Session Values for CBB Procedure Table**********************************/
		
		
		try{
			/**********************Setting ShowEntries Data*******************************/
			if(request.getParameter("l_proc_SE")!=null){
				
				String procse=request.getParameter("l_proc_SE").toString();
				getRequest().getSession().setAttribute("V_PROC_SE", procse);
				
			}
			/****************************************************************************/
			
			/**********************Setting Sorting Data*******************************/
			if(request.getParameter("l_proc_sort_col")!=null){
				
				String lastSortColInd=request.getParameter("l_proc_sort_col").toString();
				getRequest().getSession().setAttribute("V_SORT_COL_IND", lastSortColInd);
				
			}
			if(request.getParameter("l_proc_sort_typ")!=null){
				
				String lastSortColTyp=request.getParameter("l_proc_sort_typ").toString();
				getRequest().getSession().setAttribute("V_SORT_COL_TYP", lastSortColTyp);
				
			}
			/****************************************************************************/
			
			
			/**********************Setting Filter values Data*******************************/
			if(request.getParameter("l_proc_fval")!=null){
				
				String lastSortColInd=request.getParameter("l_proc_fval").toString();
				getRequest().getSession().setAttribute("V_FILTER_VALUES", lastSortColInd);
				
			}
			/****************************************************************************/
			if(request.getParameter("l_proc_rcntIds")!=null){
				
				String lastprocrcntIds=request.getParameter("l_proc_rcntIds").toString();
				getRequest().getSession().setAttribute("V_RCNT_ID_VALUES", lastprocrcntIds);
				
			}
			//log.debug("inside setting of value in session for showEntries::::::::::::"+request.getSession().getValue("V_PROC_SE"));
			//log.debug("inside setting of value in session for V_SORT_COL_IND::::::::::::"+request.getSession().getValue("V_SORT_COL_IND"));
			//log.debug("inside setting of value in session for V_SORT_COL_TYP::::::::::::"+request.getSession().getValue("V_SORT_COL_TYP"));
			//log.debug("inside setting of value in session for V_FILTER_VALUES::::::::::::"+request.getSession().getValue("V_FILTER_VALUES"));
			
		}catch(Exception e){
			log.debug("\n\n\nException in Setting Session Data..........");
			e.printStackTrace();
		}
		
		
		/************************************************************************************************************/
		
	}
	
	public String getCBBType(String siteId)
	{
		return new CBBController().getCbbType(siteId);
	}
	
	public String setCBBInfo() throws Exception{
		String returnFlag = "success";
		String flag = request.getParameter("flag");
		if(flag!=null && flag.equals("1")){
			setSite(new CBBController().getSiteBySiteId(getSite().getSiteId()));
			getRequest().setAttribute("sitePerAdd", getSite().getSitePerAdd());
			cbuStrLocationList=(List<SitePojo>) new CBUAction().setMultipleValues(VelosGarudaConstants.SITE_TYPE,getSite().getSiteId(),VelosGarudaConstants.CBU_STORAGE);
			setCbuStrLocationList(cbuStrLocationList);			
			cbuCollectionList=(List<SitePojo>) new CBUAction().setMultipleValues(VelosGarudaConstants.SITE_TYPE,getSite().getSiteId(),VelosGarudaConstants.CBU_COLLECTION);
			setCbuCollectionList(cbuCollectionList);
			if(getSite()!=null){
				Site s  = new Site();
				s = (Site)ObjectTransfer.transferObjects(getSite(),s);
				CdrCbuPojo cdrCbuPojo = new CdrCbuPojo();
				cdrCbuPojo.setSite(s);
				setCdrCbuPojo(cdrCbuPojo);
			}
		}
		else if(flag!=null && flag.equals("0")){
			returnFlag = "cbbProcedure";
			getRequest().setAttribute("cbbProceduresList", cbbController.getCBBProcedure(null,getSite().getSiteId()+"",paginateSearch,0));
		}
		return returnFlag;
	}

	public String updateCBBDefaults() throws Exception {
		UserJB user = (UserJB)getRequest().getSession().getAttribute(VelosGarudaConstants.GARUDA_USER);
		if (getCbbPojo() != null) {
			AddressPojo address = getSite().getAddress();
			AddressPojo address1 = getCbbPojo().getAddress();
			AddressPojo addressDryShipper = getCbbPojo().getAddressDryShipper();			
			address = addressController.saveAddress(address);
			if(request.getParameter("ispickupAddUpdated").toString().equals("true"))
			{
				address1 = addressController.saveAddress(address1);
				CodeList codeList= cbbController.getAddChangeEmail("ADD_CHG_EMAIL","PICK_UP");
			//	//log.debug("codeList =    "+codeList.getDescription());
				MailAction mailAction = new MailAction();
				if(codeList.getDescription()!=null && getSite().getSiteName()!= null && getSite().getSiteId()!= null)
				{
					mailAction.cbbAddressChangeMail(codeList.getDescription().toString(),getSite().getSiteName(),getSite().getSiteId().toString());
				}
				
			}
			if(request.getParameter("isDryShipperAddUpdated").toString().equals("true"))
			{
				addressDryShipper = addressController.saveAddress(addressDryShipper);
			}
			PersonPojo personPojo = new PersonPojo();
			personPojo.setPersonId(getCbbPojo().getPersonId());
			personPojo = personController.getPersonById(personPojo);
			personPojo.setPersonFname(getFirstName());
			personPojo.setPersonLname(getLastName());
			personPojo.setPersonAccount(Long.valueOf(user.getUserAccountId()));
			personPojo.setPersonCode("CBU Contact");
			personPojo = personController.savePerson(personPojo);
			//getSite().setSitePerAdd(address.getAddressId());
			getCbbPojo().setPersonId(personPojo.getPersonId());
			getCbbPojo().setSiteId(getSite().getSiteId());
			getCbbPojo().setFkCbuPickUpAddress(address1.getAddressId());
			getCbbPojo().setFkDryshipperAdd(addressDryShipper.getAddressId());			
			//getCbbPojo().setFkCbuStorage(getFkCbuStorage());
			if(request.getParameter("fkCbuStorage")!=null)
			getCbbPojo().setFkCbuStorage(new Long(request.getParameter("fkCbuStorage")));
			if(request.getParameter("fkCbuCollection")!=null)
			getCbbPojo().setFkCbuCollection(new Long(request.getParameter("fkCbuCollection")));
			setCbbPojo(cbbController.saveCBBDefaults(getCbbPojo()));
			String sName = getSite().getSiteName();
			String sId = getSite().getSiteIdentifier();
			setSite(cbbController.getSiteBySiteId(getSite().getSiteId()));
			getSite().setSiteName(sName);
			getSite().setSiteIdentifier(sId);
			getSite().setSitePerAdd(address.getAddressId());
			setSite(cbbController.saveSite(getSite()));
			List<SitePojo> cbbList = cbbController.getCBBDetails(getSite().getSiteId());
			getRequest().setAttribute("CbbDefaultList", cbbList);		
			setAutoMaticSequencingLocalAssgnId(getCodeListPkByTypeAndSubtype(LOCAL_ASSIGNMENT_ID, AUTOMATIC_SEQUENCING_LOCAL_ASSIGNMENT_ID));
			/*if(getFkAccreditation()==null)
			{
				Long[] val = new Long[1];
				val[0]= new Long(-1);
				setFkAccreditation(val);
			}
			*/
			if(getFkAccreditation()!=null && getFkAccreditation().length>0)
			{
				 new CBUAction().saveEntityMultipleType(VelosGarudaConstants.ENTITY_CBB, VelosGarudaConstants.ACCREDITITAON, getCbbPojo().getPkCbbId(),getFkAccreditation());
			}
			/*	Id on bag is removed			
			if(getCbuOnBag()!=null && getCbuOnBag().length>0 && getCbuOnBag()[0]!=null){
				new CBUAction().saveEntityMultipleType(VelosGarudaConstants.ENTITY_CBB, VelosGarudaConstants.ALL_IDS, getSite().getSiteId(), getCbuOnBag());
			}
			*/			
			cbuStrLocationList=(List<SitePojo>) new CBUAction().setMultipleValues(VelosGarudaConstants.SITE_TYPE,getSite().getSiteId(),VelosGarudaConstants.CBU_STORAGE);
			setCbuStrLocationList(cbuStrLocationList);
			
			cbuCollectionList=(List<SitePojo>) new CBUAction().setMultipleValues(VelosGarudaConstants.SITE_TYPE,getSite().getSiteId(),VelosGarudaConstants.CBU_COLLECTION);
			setCbuCollectionList(cbuCollectionList);
			
		
		}
		return SUCCESS;
	}

	public String f_getCBBProcedure() {
		try {
			
			UserJB user = (UserJB)getRequest().getSession().getAttribute(VelosGarudaConstants.GARUDA_USER);
			List<CBBProcedurePojo> cbbList = null;
			List countLst=new ArrayList();
			String orderBy = "";
			String orderByStr = "";
			String id = "";
			String filterCondtion="";
			int iTotalRows = 0;
	        int RefreshFlag=0;
	        int size=0;
	        String colIndex="";
	        String colSortTyp="";
	        
			sites = new CBBController().getSites();
	        setSites(sites);
	        String siteIds="'0'";
	        Iterator<SitePojo> itr =getSites().iterator();
	        while(itr.hasNext()){
	        		siteIds=siteIds+",'"+itr.next().getSiteId()+"'";
	        }
	        
	        
	        if(request.getParameter("iShowRows")!=null){
	        	RefreshFlag=1;
	        	String tempStr=request.getParameter("iShowRows").toString();
	        	//System.out.println("\n\n\n\n\nInside refresh action show entries\n\n\n\n\n"+request.getParameter("iShowRows"));
	        }else if(RefreshFlag==0 && request.getParameter("iShowRows")==null && (request.getSession().getValue("V_PROC_SE")!=null)){
	        	
	        	   String tempStr=request.getSession().getValue("V_PROC_SE").toString();
	        	   if(!tempStr.equals("ALL")){
	        		   size=Integer.parseInt(tempStr);   
	        	   }else{
	        		   size=5;
	        	   }
	        	//System.out.println("\n\n\n\n\nInside getting session data for show entries\n\n\n\n\n"+size);
	        }
	        
	        
	        if(RefreshFlag==0 && request.getSession().getValue("V_SORT_COL_IND")!=null){
	        	colIndex=request.getSession().getValue("V_SORT_COL_IND").toString();
	        	colSortTyp=request.getSession().getValue("V_SORT_COL_TYP").toString();
	        	orderByStr=serverorderby.FetchProcedure_OrderBy(colIndex, colSortTyp);
	        	//System.out.println("Order By condition from Session................."+orderByStr);
	        }
	        
	        filterCondtion=getProcessingProcedureCondition();
	        
	        if(RefreshFlag==0 && filterCondtion.equals("") && request.getSession().getValue("V_FILTER_VALUES")!=null){
	        	filterCondtion=f_getSProcessingProcedureCondition();
	        }
	        
	        if(request.getParameter("filterTxt")!=null && !request.getParameter("filterTxt").trim().equals("")){
				ServerGlobalSearch serverGlobalSearch=new ServerGlobalSearch();
				String conditionnew=serverGlobalSearch.FetchGlobalSearchProcedure(request.getParameter("filterTxt").trim());
				filterCondtion=filterCondtion+conditionnew;
			}
	        
	        
	        
			paginationSessionData(paginateSearch, size,MODULE_CBBPROCEDURE);
			cbbList = cbbController.getCBBProcedureSort(null,siteIds,paginateSearch,1,filterCondtion,orderByStr);
			List<CBBProcedurePojo> cbnewList =new ArrayList<CBBProcedurePojo>();
			countLst = cbbController.getCBBProcedureCount(siteIds, filterCondtion, 0);
			Object obj=countLst.get(0);
			int iTotalSize=Integer.parseInt(obj.toString());
			paginateSearch.setiTotalRows(iTotalSize);
			request.setAttribute("paginationSearch", paginateSearch);
			setPaginateSearch(VelosUtil.getListTotalCount(paginateSearch));
			
			//System.out.println("\n\n\n\n\nShow Entries Data in action........\n\n\n\n\n"+paginateSearch.getiShowRows());

			Iterator<CBBProcedurePojo> it = cbbList.iterator();
			if(it.hasNext()){
				id = it.next().getPkProcId().toString();
			}
			List<CBBProcedureInfoPojo> lstProc =new ArrayList<CBBProcedureInfoPojo>();
			List<CBBProcedureInfoPojo> lstProc1 =new ArrayList<CBBProcedureInfoPojo>();
			if(id!=null && !id.equals("")){
				lstProc=cbbController.getCBBProcedureInfo(id);
				Iterator<CBBProcedureInfoPojo> itr1=lstProc.iterator();
				
				getPkCodeIds();
				while(itr1.hasNext()){
					CBBProcedureInfoPojo cbbproinfo=new CBBProcedureInfoPojo();
					cbbproinfo=itr1.next();
					cbbproinfo=(CBBProcedureInfoPojo)ObjectTransfer.transferObjects(cbbProcedureInfoPojo,cbbproinfo);
					lstProc1.add(cbbproinfo);
				}
			}
			
			setCbbProdList(lstProc1);
			setRcntPkProcId(id);
			
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return SUCCESS;
	} 
	
	public String f_getSProcessingProcedureCondition(){
		String condition="";
		
		String temp=request.getSession().getValue("V_FILTER_VALUES").toString();
		temp=temp.replace("----", "&");
		temp=temp.concat("&");
		String getStr1;
		String getStr2;
		String dropDownNames[]={"procedureName=","proStrDt1=","proStrDt2=","procTermDt1=","procTermDt2="};
		String columnNames[]={"procName","procStartDate","procStartDate","procTermiDate","procTermiDate"};
		List dropDownValues=new ArrayList();
		
		for(int i=0;i<dropDownNames.length;i++){
		     int tempIndex1=temp.indexOf(dropDownNames[i]);	
		     getStr2="";
		     if(tempIndex1>0){
		    	 getStr1=temp.substring(tempIndex1);
			     int tempIndex2=getStr1.indexOf("&");
			     int keywordSize=dropDownNames[i].length();
			     if(tempIndex2>0){
			    	 getStr2=getStr1.substring(keywordSize,tempIndex2);
			     }
		     }
		     dropDownValues.add(getStr2);
		}
		
		Object[] temptttt=dropDownValues.toArray();	
		
		
		for(int i=0;i<dropDownNames.length;i++){
			
			
			if(temptttt[i]!=null && !temptttt[i].equals("")){
				
				if(i==0){
					condition=condition+" and replace(lower("+columnNames[i]+"),'-','') like lower('%"+ Utilities.removeDelimeters(temptttt[i].toString(), "-") + "%')";
				}
				
				if(i==1){
					String val1=temptttt[1].toString();
					String val2=temptttt[2].toString();
					if(val1.equals(val2)){
						condition=condition+" and to_char("+columnNames[i]+",'DD-Mon-YYYY')='"+temptttt[1]+"'";
					}else{
						condition=condition+" and (trunc("+columnNames[i]+") between to_date('"+temptttt[1]+"','DD-Mon-YYYY') and to_date('"+temptttt[2]+"','DD-Mon-YYYY'))";
					}
					
				}
				if(i==3){
					String val1=temptttt[3].toString();
					String val2=temptttt[4].toString();
					if(val1.equals(val2)){
						condition=condition+" and to_char("+columnNames[i]+",'DD-Mon-YYYY')='"+temptttt[3]+"'";
					}else{
						condition=condition+" and (trunc("+columnNames[i]+") between to_date('"+temptttt[3]+"','DD-Mon-YYYY') and to_date('"+temptttt[4]+"','DD-Mon-YYYY'))";
					}
					
				}
				
				if(i==0){
					setFprocedureName(temptttt[i].toString());
				}
				if(i==1){
					setFprocStrDt1(Utilities.getOrdDateFormatDate(temptttt[1].toString()));
					setFprocStrDt2(Utilities.getOrdDateFormatDate(temptttt[2].toString()));
				}
				if(i==3){
					setFprocTerDt1(Utilities.getOrdDateFormatDate(temptttt[3].toString()));
					setFprocTerDt2(Utilities.getOrdDateFormatDate(temptttt[4].toString()));
				}
			}
			
		
		}
		
		//System.out.println("Condition in session::::::::::::::::::::::"+condition);
		
		return condition;
	}
	
	
	public String getProcessingProcedureCondition(){
		String condition="";
		
		String dropDownNames[]={"procedureName","proStrDt1","proStrDt2","procTermDt1","procTermDt2"};
		String columnNames[]={"procName","procStartDate","procStartDate","procTermiDate","procTermiDate"};
		
		
		for(int i=0;i<dropDownNames.length;i++){
			
			
			if(request.getParameter(dropDownNames[i])!=null && !request.getParameter(dropDownNames[i]).equals("") && !request.getParameter(dropDownNames[i]).equals("undefined")){
				
				if(i==0){
					condition=condition+" and replace(lower("+columnNames[i]+"),'-','') like lower('%"+ Utilities.removeDelimeters(request.getParameter(dropDownNames[i]).toString(), "-") + "%')";
				}
				
				if(i==1){
					String val1=request.getParameter(dropDownNames[1]).toString();
					String val2=request.getParameter(dropDownNames[2]).toString();
					if(val1.equals(val2)){
						condition=condition+" and to_char("+columnNames[i]+",'DD-Mon-YYYY')='"+request.getParameter(dropDownNames[1])+"'";
					}else{
						condition=condition+" and (trunc("+columnNames[i]+") between to_date('"+request.getParameter(dropDownNames[1])+"','DD-Mon-YYYY') and to_date('"+request.getParameter(dropDownNames[2])+"','DD-Mon-YYYY'))";
					}
					
				}
				if(i==3){
					String val1=request.getParameter(dropDownNames[3]).toString();
					String val2=request.getParameter(dropDownNames[4]).toString();
					if(val1.equals(val2)){
						condition=condition+" and to_char("+columnNames[i]+",'DD-Mon-YYYY')='"+request.getParameter(dropDownNames[3])+"'";
					}else{
						condition=condition+" and (trunc("+columnNames[i]+") between to_date('"+request.getParameter(dropDownNames[3])+"','DD-Mon-YYYY') and to_date('"+request.getParameter(dropDownNames[4])+"','DD-Mon-YYYY'))";
					}
					
				}
				
				if(i==0){
					setFprocedureName(request.getParameter(dropDownNames[i]).toString());
				}
				if(i==1){
					setFprocStrDt1(Utilities.getOrdDateFormatDate(request.getParameter(dropDownNames[1]).toString()));
					setFprocStrDt2(Utilities.getOrdDateFormatDate(request.getParameter(dropDownNames[2]).toString()));
				}
				if(i==3){
					setFprocTerDt1(Utilities.getOrdDateFormatDate(request.getParameter(dropDownNames[3]).toString()));
					setFprocTerDt2(Utilities.getOrdDateFormatDate(request.getParameter(dropDownNames[4]).toString()));
				}
			}
			
		
		}
		
		//System.out.println("Condition::::::::::::::::::::::"+condition);
		
		return condition;
	}
	public String getDeferProc() throws Exception{
		  try{
		  String id = request.getParameter("procPk");
		  long storTemp =0;
		  long frozenIn= 0;
		  long noOfSeg = 2;
		  String procStartDt = "";
		  String procTermiDt = "";
		  List<CBBProcedureInfoPojo> lstProc =new ArrayList<CBBProcedureInfoPojo>();
			List<CBBProcedureInfoPojo> lstProc1 =new ArrayList<CBBProcedureInfoPojo>();
			lstProc= cbbController.getCBBProcedureInfo(id);
			Iterator<CBBProcedureInfoPojo> itr1=lstProc.iterator();
			getPkCodeIds();
			while(itr1.hasNext()){
				CBBProcedureInfoPojo cbbproinfo=new CBBProcedureInfoPojo();
				cbbproinfo=itr1.next();
				cbbproinfo=(CBBProcedureInfoPojo)ObjectTransfer.transferObjects(cbbProcedureInfoPojo,cbbproinfo);
				lstProc1.add(cbbproinfo);
				if(cbbproinfo!=null){
					storTemp = cbbproinfo.getFkStorTemp();
					frozenIn = cbbproinfo.getFkFrozenIn();
					noOfSeg = cbbproinfo.getNoOfSegments();
					if( cbbproinfo.getCbbProcedures().getProcStartDate() != null)
						procStartDt = dat.format(cbbproinfo.getCbbProcedures().getProcStartDate());
					
					if( cbbproinfo.getCbbProcedures().getProcTermiDate() != null)
						procTermiDt = dat.format(cbbproinfo.getCbbProcedures().getProcTermiDate());
				}
			}
			
			//log.debug("storTemp:::"+storTemp);
			//log.debug("frozenIn:::"+frozenIn);
			//log.debug("tubes:::"+getCodeListPkByTypeAndSubtype(FROZEN_IN,FROZEN_IN_TUBES));
			//log.debug("vials:::"+getCodeListPkByTypeAndSubtype(FROZEN_IN,FROZEN_IN_VIALS));
			//log.debug("storTemp1:::"+getCodeListPkByTypeAndSubtype(STORAGE_TEMPERATURE,STORAGE_TEMPERATURE_GTMINUSONETHIRTYFIVE));
			
			setProcStartDateStrAdd(procStartDt);
			setProcTermiDateStrAdd(procTermiDt);
			
			if ((storTemp == getCodeListPkByTypeAndSubtype(STORAGE_TEMPERATURE,STORAGE_TEMPERATURE_GTMINUSONETHIRTYFIVE))
			|| (frozenIn == getCodeListPkByTypeAndSubtype(FROZEN_IN,FROZEN_IN_VIALS) || frozenIn == getCodeListPkByTypeAndSubtype(FROZEN_IN,FROZEN_IN_TUBES))
			 )
			{
				setCheckDefer("true");
			}
			else
			{
				setCheckDefer("false");
			}
		  }catch (Exception e) {
			// TODO: handle exception
				log.error(e.getMessage());
			  e.printStackTrace();
		}
		  return SUCCESS;
		  
    }
	public String getCBBProcedureDetails() {
		try {
			sites = new CBBController().getSites();
	        setSites(sites);  
			String loginIds = request.getParameter("loginIds");
			List<CBBProcedureInfoPojo> lstProc =new ArrayList<CBBProcedureInfoPojo>();
			List<CBBProcedureInfoPojo> lstProc1 =new ArrayList<CBBProcedureInfoPojo>();
			lstProc=cbbController.getCBBProcedureInfo(loginIds);
			Iterator<CBBProcedureInfoPojo> itr=lstProc.iterator();
			getPkCodeIds();
			while(itr.hasNext()){
				CBBProcedureInfoPojo cbbproinfo=new CBBProcedureInfoPojo();
				cbbproinfo=itr.next();
				cbbproinfo=(CBBProcedureInfoPojo)ObjectTransfer.transferObjects(cbbProcedureInfoPojo,cbbproinfo);
				//log.debug("cbbproinfo.getPkProcInfoId::::::::::::::"+cbbproinfo.getPkProcInfoId());
				//log.debug("cbbproinfo.getFkBag1Type::::::"+cbbproinfo.getFkBag1Type());
				lstProc1.add(cbbproinfo);
			}
			setCbbProdList(lstProc1);
			setSopStrtDateStrAdd(getSopStrtDateStrAdd());
			setSopTermiDateStrAdd(getSopTermiDateStrAdd());
			setProcStartDateStrAdd(getProcStartDateStrAdd());
			setProcTermiDateStrAdd(getProcTermiDateStrAdd());
			setFkProcessingId(getFkProcessingId());
			setFkProcId(loginIds);
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public void getPkCodeIds() {
		try {
			cbbProcedureInfoPojo.setPkProcMethodId(cbbController.getCodeListPkIds(PROCESSING, AUTOMATED));
			cbbProcedureInfoPojo.setPkIfAutomatedOther(cbbController.getCodeListPkIds(IF_AUTOMATED, OTHER));
			cbbProcedureInfoPojo.setPkProdModiOther(cbbController.getCodeListPkIds(PRODUCT_MODI, OTHER));
			cbbProcedureInfoPojo.setPkFreezManuOther(cbbController.getCodeListPkIds(FREEZER_MANUFACTURER, OTHER));
			cbbProcedureInfoPojo.setPkFrozenInBag(cbbController.getCodeListPkIds(FROZEN_IN, BAGS));
			cbbProcedureInfoPojo.setPkFrozenInOther(cbbController.getCodeListPkIds(FROZEN_IN, OTHER));
			cbbProcedureInfoPojo.setPkOtherBagType(cbbController.getCodeListPkIds(BAG_TYPE, OTHER));
			cbbProcedureInfoPojo.setPkBag1Type(cbbController.getCodeListPkIds(NUM_OF_BAGS, ONE_BAG));
			cbbProcedureInfoPojo.setPkBag2Type(cbbController.getCodeListPkIds(NUM_OF_BAGS, TWO_BAG));
			cbbProcedureInfoPojo.setPkNoOfBagsOthers(cbbController.getCodeListPkIds(NUM_OF_BAGS, OTHER_BAG)); 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public String saveCBBProcedureDetails() {
		try {
			UserJB user = (UserJB)getRequest().getSession().getAttribute(VelosGarudaConstants.GARUDA_USER);
			sites = new CBBController().getSites();
	        setSites(sites);
	        String siteIds="'0'";
	        Iterator<SitePojo> itr =getSites().iterator();
	        while(itr.hasNext()){
	        		siteIds=siteIds+",'"+itr.next().getSiteId()+"'";
	        }
			if (!getStatusUpdate()) {
				getPkCodeIds();
			}
			if (getStatusUpdate()) {
				setCbbProcedurePojo(getCbbProcedurePojo());
				if (getProcStartDateStrAdd() != null
						&& !getProcStartDateStrAdd().equals("")) {

					cbbProcedurePojo.setProcStartDate(dat
							.parse(getProcStartDateStrAdd()));
				}
				if (getProcTermiDateStrAdd() != null
						&& !getProcTermiDateStrAdd().equals("")) {
					cbbProcedurePojo.setProcTermiDate(dat
							.parse(getProcTermiDateStrAdd()));
				}
				setCbbProcedureInfoPojo(getCbbProcedureInfoPojo());
				if (getSopStrtDateStrAdd() != null
						&& !getSopStrtDateStrAdd().equals("")) {
					cbbProcedureInfoPojo.setSopStrtDate(dat
							.parse(getSopStrtDateStrAdd()));
				}
				if (getSopTermiDateStrAdd() != null
						&& !getSopTermiDateStrAdd().equals("")) {
					cbbProcedureInfoPojo.setSopTermiDate(dat
							.parse(getSopTermiDateStrAdd()));
				}
				/*log.debug("user.getUserDetails().getUserSiteId()::::::"+user.getUserDetails().getUserSiteId());
				cbbProcedurePojo.setFkSite(Long.parseLong(user.getUserDetails().getUserSiteId()));
				
				log.debug("cbbProcedurePojo.getFkSite():::::"+cbbProcedurePojo.getFkSite());*/
				
				setCbbProcedurePojo(cbbController
						.saveCBBProcedure(getCbbProcedurePojo()));
				cbbProcedureInfoPojo.setFkProcessingId(cbbProcedurePojo
						.getPkProcId());
				
				setCbbProcedureInfoPojo(cbbController
						.saveCBBProcedureDetails(getCbbProcedureInfoPojo()));
				
				
				paginationReqScopeData(paginateSearch);
				//-----------------------end-----------------
				
				cbbList = cbbController.getCBBProcedure(null,siteIds,paginateSearch,1);
				int iTotalRows = cbbController.getCBBProcedure(null,siteIds,paginateSearch,0).size();
				paginateSearch.setiTotalRows(iTotalRows);
				setPaginateSearch(VelosUtil.getListTotalCount(paginateSearch));
				setCbbList(getCbbList());
	
				getPkCodeIds();
				setRcntPkProcId(cbbProcedurePojo.getPkProcId().toString());
				List<CBBProcedureInfoPojo> lstProc =new ArrayList<CBBProcedureInfoPojo>();
				List<CBBProcedureInfoPojo> lstProc1 =new ArrayList<CBBProcedureInfoPojo>();
				lstProc=cbbController.getCBBProcedureInfo(cbbProcedurePojo.getPkProcId()
						.toString());
				Iterator<CBBProcedureInfoPojo> itr1=lstProc.iterator();
				getPkCodeIds();
				while(itr1.hasNext()){
					CBBProcedureInfoPojo cbbproinfo=new CBBProcedureInfoPojo();
					cbbproinfo=itr1.next();
					cbbproinfo=(CBBProcedureInfoPojo)ObjectTransfer.transferObjects(cbbProcedureInfoPojo,cbbproinfo);
					//log.debug("cbbproinfo.getPkProcInfoId::::::::::::::"+cbbproinfo.getPkProcInfoId());
					//log.debug("cbbproinfo.getFkBag1Type::::::"+cbbproinfo.getFkBag1Type());
					lstProc1.add(cbbproinfo);
				}
				setCbbProdList(lstProc1);
				setFkProcId("0,"+cbbProcedurePojo.getPkProcId().toString());
				//log.debug("getFkProcId():::::::::::::"+getFkProcId());
				request.setAttribute("statusUpdate", true);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String copyCbbProcedure(){
		try {
				UserJB user = (UserJB)getRequest().getSession().getAttribute(VelosGarudaConstants.GARUDA_USER);
				sites = new CBBController().getSites();
		        setSites(sites);
		        String siteIds="'0'";
		        Iterator<SitePojo> itr =getSites().iterator();
		        //log.debug("sites size::::::::::::::::::::::::"+getSites().size());
		        while(itr.hasNext()){
		        		siteIds=siteIds+",'"+itr.next().getSiteId()+"'";
		        }
		        //log.debug("sites areeeeeeeeeeeee"+siteIds);
				fkProcId = "";
				fkProcId = request.getParameter("loginIds");
				if (request.getParameter("pkProcId") != null) {
					
						List<CBBProcedureInfoPojo> lstProc =new ArrayList<CBBProcedureInfoPojo>();
						List<CBBProcedureInfoPojo> lstProc1 =new ArrayList<CBBProcedureInfoPojo>();
						lstProc=cbbController.getCBBProcedureInfo(request.getParameter("pkProcId"));
						Iterator<CBBProcedureInfoPojo> itr1=lstProc.iterator();
						getPkCodeIds();
						while(itr1.hasNext()){
							CBBProcedureInfoPojo cbbproinfo=new CBBProcedureInfoPojo();
							cbbproinfo=itr1.next();
							cbbproinfo=(CBBProcedureInfoPojo)ObjectTransfer.transferObjects(cbbProcedureInfoPojo,cbbproinfo);
							//log.debug("cbbproinfo.getPkProcInfoId::::::::::::::"+cbbproinfo.getPkProcInfoId());
							//log.debug("cbbproinfo.getFkBag1Type::::::"+cbbproinfo.getFkBag1Type());
							lstProc1.add(cbbproinfo);
						}
						setCbbProdList(lstProc1);
						Iterator<CBBProcedureInfoPojo> it = cbbProdList.iterator();
						while (it.hasNext()) {
							cbbProcedureInfoPojo = it.next();
							cbbProcedures = getCbbProcedureInfoPojo()
									.getCbbProcedures();
							cbbProcedures.setProcName("");
							cbbProcedures.setProductCode("");
							cbbProcedures.setProcStartDate(null);
							cbbProcedures.setProcTermiDate(null);
							cbbProcedurePojo = (CBBProcedurePojo) ObjectTransfer
									.transferObjects(cbbProcedures,
											cbbProcedurePojo);
							setFkProcId(fkProcId);
						}
						
						setCbbProcedureInfoPojo(cbbProcedureInfoPojo);
						setCbbProcedurePojo(cbbProcedurePojo);
						setCbbProcedures(cbbProcedures);
						getPkCodeIds();
						if(request.getParameter("assignedToCordFlag")!=null && !request.getParameter("assignedToCordFlag").equals("0") && !request.getParameter("assignedToCordFlag").equals("")){
							setAssignedToCordFlag(request.getParameter("assignedToCordFlag"));
						}else{
							setAssignedToCordFlag("0");
						}
				}
			}catch(Exception e){
				log.error(e.getMessage());
				e.printStackTrace();
			}
			return "success";
		
	}

	public String updateCBBInformation() {
		try {
			UserJB user = (UserJB)getRequest().getSession().getAttribute(VelosGarudaConstants.GARUDA_USER);
			sites = new CBBController().getSites();
	        setSites(sites);
	        String siteIds="'0'";
	        Iterator<SitePojo> itr =getSites().iterator();
	        Boolean shipMentFlag = isTasksRequired(getCdrCbuPojo().getCordID());
			request.setAttribute("shipMentFlag", shipMentFlag);
	        //log.debug("sites size::::::::::::::::::::::::"+getSites().size());
	        while(itr.hasNext()){
	        		siteIds=siteIds+",'"+itr.next().getSiteId()+"'";
	        }
	       // //log.debug("sites areeeeeeeeeeeee"+siteIds);
			fkProcId = "";
			fkProcId = request.getParameter("loginIds");
			if (request.getParameter("pkProcId") != null) {
				if (!getStatusUpdate()) {
					List<CBBProcedureInfoPojo> lstProc =new ArrayList<CBBProcedureInfoPojo>();
					List<CBBProcedureInfoPojo> lstProc1 =new ArrayList<CBBProcedureInfoPojo>();
					lstProc=cbbController.getCBBProcedureInfo(request.getParameter("pkProcId"));
					Iterator<CBBProcedureInfoPojo> itr1=lstProc.iterator();
					getPkCodeIds();
					while(itr1.hasNext()){
						CBBProcedureInfoPojo cbbproinfo=new CBBProcedureInfoPojo();
						cbbproinfo=itr1.next();
						cbbproinfo=(CBBProcedureInfoPojo)ObjectTransfer.transferObjects(cbbProcedureInfoPojo,cbbproinfo);
						//log.debug("cbbproinfo.getPkProcInfoId::::::::::::::"+cbbproinfo.getPkProcInfoId());
						//log.debug("cbbproinfo.getFkBag1Type::::::"+cbbproinfo.getFkBag1Type());
						lstProc1.add(cbbproinfo);
					}
					setCbbProdList(lstProc1);
					Iterator<CBBProcedureInfoPojo> it = cbbProdList.iterator();
					while (it.hasNext()) {
						cbbProcedureInfoPojo = it.next();
						cbbProcedures = getCbbProcedureInfoPojo()
								.getCbbProcedures();
						cbbProcedurePojo = (CBBProcedurePojo) ObjectTransfer
								.transferObjects(cbbProcedures,
										cbbProcedurePojo);
						setFkProcId(fkProcId);
					}
					setCbbProcedureInfoPojo(cbbProcedureInfoPojo);
					setCbbProcedurePojo(cbbProcedurePojo);
					getPkCodeIds();
					if(request.getParameter("assignedToCordFlag")!=null && !request.getParameter("assignedToCordFlag").equals("")){
						setAssignedToCordFlag(request.getParameter("assignedToCordFlag"));
					}else{
						setAssignedToCordFlag("0");
					}
					
				}
				if (getStatusUpdate()) {
					if (cbbProcedurePojo.getProcStartDateStr() != null
							&& !cbbProcedurePojo.getProcStartDateStr().equals(
									"")) {
						cbbProcedurePojo.setProcStartDate(dat
								.parse(cbbProcedurePojo.getProcStartDateStr()));
					} else
						cbbProcedurePojo.setProcStartDate(null);

					if (cbbProcedurePojo.getProcTermiDateStr() != null
							&& !cbbProcedurePojo.getProcTermiDateStr().equals(
									"")) {
						cbbProcedurePojo.setProcTermiDate(dat
								.parse(cbbProcedurePojo.getProcTermiDateStr()));
					} else
						cbbProcedurePojo.setProcTermiDate(null);

					if (cbbProcedureInfoPojo.getSopStrtDateStr() != null
							&& !cbbProcedureInfoPojo.getSopStrtDateStr()
									.equals("")) {
						cbbProcedureInfoPojo
								.setSopStrtDate(dat.parse(cbbProcedureInfoPojo
										.getSopStrtDateStr()));
					} else
						cbbProcedureInfoPojo.setSopStrtDate(null);

					if (cbbProcedureInfoPojo.getSopTermiDateStr() != null
							&& !cbbProcedureInfoPojo.getSopTermiDateStr()
									.equals("")) {
						cbbProcedureInfoPojo.setSopTermiDate(dat
								.parse(cbbProcedureInfoPojo
										.getSopTermiDateStr()));
					} else
						cbbProcedureInfoPojo.setSopTermiDate(null);
					/*log.debug("user.getUserDetails().getUserSiteId()::::::"+user.getUserDetails().getUserSiteId());
					cbbProcedurePojo.setFkSite(Long.parseLong(user.getUserDetails().getUserSiteId()));
					
					log.debug("cbbProcedurePojo.getFkSite():::::"+cbbProcedurePojo.getFkSite());*/
					//log.debug("request.getParameter(assignedToCordFlag)::::"+request.getParameter("assignedToCordFlag"));
					if(request.getParameter("assignedToCordFlag")!=null && !request.getParameter("assignedToCordFlag").equals("") && !request.getParameter("assignedToCordFlag").equals("0")){
						//log.debug("in if assignToCordFlag=1");
						List<CBBProcedureInfoPojo> lstProc =new ArrayList<CBBProcedureInfoPojo>();
						lstProc=cbbController.getCBBProcedureInfo(request.getParameter("pkProcId"));
						Iterator<CBBProcedureInfoPojo> itr1=lstProc.iterator();
						while(itr1.hasNext()){
							CBBProcedureInfoPojo cbbproinfo=new CBBProcedureInfoPojo();
							cbbproinfo=itr1.next();
							cbbproinfo.getCbbProcedures().setProcTermiDate(getCbbProcedurePojo().getProcTermiDate());
							CBBProcedurePojo cbbpro=new CBBProcedurePojo();
							cbbpro=(CBBProcedurePojo)ObjectTransfer.transferObjects(cbbproinfo.getCbbProcedures(), cbbpro);
							cbbProcedureInfoPojo = cbbController.updateCbbProcedureInfo(cbbproinfo,cbbpro);
							
						}
					}else{
					cbbProcedureInfoPojo = cbbController
							.updateCbbProcedureInfo(getCbbProcedureInfoPojo(),
									getCbbProcedurePojo());
					}
					setFkProcId(fkProcId);

					String[] token = fkProcId.split(",");
					if (token.length != 0) {
						for (int i = 0; i < token.length; i++) {
							list.add("" + token[i]);
						}
					}
					setList(list);

					request.setAttribute("statusUpdate", true);
				}

			}
			paginationReqScopeData(paginateSearch);
			//-----------------------end-----------------
			
			cbbList = cbbController.getCBBProcedure(null,siteIds,paginateSearch,1);
			int iTotalRows = cbbController.getCBBProcedure(null,siteIds,paginateSearch,0).size();
			paginateSearch.setiTotalRows(iTotalRows);
			setPaginateSearch(VelosUtil.getListTotalCount(paginateSearch));
			setCbbList(getCbbList());
			setRcntPkProcId(cbbProcedurePojo.getPkProcId().toString());
			List<CBBProcedureInfoPojo> lstProc =new ArrayList<CBBProcedureInfoPojo>();
			List<CBBProcedureInfoPojo> lstProc1 =new ArrayList<CBBProcedureInfoPojo>();
			lstProc=cbbController.getCBBProcedureInfo(getFkProcId());
			Iterator<CBBProcedureInfoPojo> itr1=lstProc.iterator();
			getPkCodeIds();
			while(itr1.hasNext()){
				CBBProcedureInfoPojo cbbproinfo=new CBBProcedureInfoPojo();
				cbbproinfo=itr1.next();
				cbbproinfo=(CBBProcedureInfoPojo)ObjectTransfer.transferObjects(cbbProcedureInfoPojo,cbbproinfo);
				//log.debug("cbbproinfo.getPkProcInfoId::::::::::::::"+cbbproinfo.getPkProcInfoId());
				//log.debug("cbbproinfo.getFkBag1Type::::::"+cbbproinfo.getFkBag1Type());
				lstProc1.add(cbbproinfo);
			}
			setCbbProdList(lstProc1);
			setSopStrtDateStr(cbbProcedureInfoPojo.getSopStrtDateStr());
			setSopTermiDateStr(cbbProcedureInfoPojo.getSopTermiDateStr());
			setProcStartDateStr(cbbProcedurePojo.getProcStartDateStr());
			setProcTermiDateStr(cbbProcedurePojo.getProcTermiDateStr());
			setFkProcessingId(getFkProcessingId());

		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return "success";
	}
	
	public String setCordProcedureInfo() throws Exception{
		if(getCbbProcedures().getPkProcId()!=null){
			setEsignFlag(getEsignFlag());
			//CdrCbuPojo cdrCbuPojo = new CdrCbuPojo();
			getCdrCbuPojo();
			EntitySamplesPojo entitySamplesPojo = new EntitySamplesPojo();
			CBBProcedureInfoPojo cbbProcedureInfoPojo = new CBBProcedureInfoPojo();
			List<CBBProcedureInfoPojo> lstProc =new ArrayList<CBBProcedureInfoPojo>();
			List<CBBProcedureInfoPojo> lstProc1 =new ArrayList<CBBProcedureInfoPojo>();
			lstProc=cbbController.getCBBProcedureInfo(getCbbProcedures().getPkProcId().toString());
			Iterator<CBBProcedureInfoPojo> itr1=lstProc.iterator();
			getPkCodeIds();
			while(itr1.hasNext()){
				CBBProcedureInfoPojo cbbproinfo=new CBBProcedureInfoPojo();
				cbbproinfo=itr1.next();
				cbbproinfo=(CBBProcedureInfoPojo)ObjectTransfer.transferObjects(cbbProcedureInfoPojo,cbbproinfo);
				cbbproinfo.setPkProcMethodId(cbbController.getCodeListPkIds(PROCESSING, AUTOMATED));
				cbbproinfo.setPkIfAutomatedOther(cbbController.getCodeListPkIds(IF_AUTOMATED, OTHER));
				cbbproinfo.setPkProdModiOther(cbbController.getCodeListPkIds(PRODUCT_MODI, OTHER));
				cbbproinfo.setPkFreezManuOther(cbbController.getCodeListPkIds(FREEZER_MANUFACTURER, OTHER));
				cbbproinfo.setPkFrozenInBag(cbbController.getCodeListPkIds(FROZEN_IN, BAGS));
				cbbproinfo.setPkFrozenInOther(cbbController.getCodeListPkIds(FROZEN_IN, OTHER));
				cbbproinfo.setPkOtherBagType(cbbController.getCodeListPkIds(BAG_TYPE, OTHER));
				cbbproinfo.setPkBag1Type(cbbController.getCodeListPkIds(NUM_OF_BAGS, ONE_BAG));
				cbbproinfo.setPkBag2Type(cbbController.getCodeListPkIds(NUM_OF_BAGS, TWO_BAG));
				cbbproinfo.setPkNoOfBagsOthers(cbbController.getCodeListPkIds(NUM_OF_BAGS, OTHER_BAG)); 
				lstProc1.add(cbbproinfo);
			}
			setCbbProdList(lstProc1);
			if(getCbbProdList()!=null && getCbbProdList().size()>0){
				cbbProcedureInfoPojo = getCbbProdList().get(0);
				cdrCbuPojo.setProcVersion(cbbProcedureInfoPojo.getCbbProcedures().getProcVersion());
				cdrCbuPojo.setProcTermiDate(cbbProcedureInfoPojo.getCbbProcedures().getProcTermiDate());
				cdrCbuPojo.setProcStartDate(cbbProcedureInfoPojo.getCbbProcedures().getProcStartDate());
				cdrCbuPojo.setProductCode(cbbProcedureInfoPojo.getCbbProcedures().getProductCode());
				setCdrCbuPojo(cdrCbuPojo);
			    entitySamplesPojo.setFiltPap(cbbProcedureInfoPojo.getFilterPaper());
			    entitySamplesPojo.setRbcPel(cbbProcedureInfoPojo.getRbcPallets());
			    entitySamplesPojo.setNoSerAli(cbbProcedureInfoPojo.getNoOfSerumAliquots());
			    entitySamplesPojo.setNoPlasAli(cbbProcedureInfoPojo.getNoOfPlasmaAliquots());
			    entitySamplesPojo.setNonViaAli(cbbProcedureInfoPojo.getNoOfNonviableAliquots());
			    entitySamplesPojo.setNoSegAvail(cbbProcedureInfoPojo.getNoOfSegments());
			    entitySamplesPojo.setViaCelAli(cbbProcedureInfoPojo.getNoOfViableSampleNotRep());
			    entitySamplesPojo.setCbuOthRepConFin(cbbProcedureInfoPojo.getNoOfOthRepAliqProd());
			    entitySamplesPojo.setCbuRepAltCon(cbbProcedureInfoPojo.getNoOfOthRepAliqAltCond());
			    entitySamplesPojo.setSerMatAli(cbbProcedureInfoPojo.getNoOfSerumMaterAliquots());
			    entitySamplesPojo.setPlasMatAli(cbbProcedureInfoPojo.getNoOfPlasmaMaterAliquots());
			    entitySamplesPojo.setExtDnaAli(cbbProcedureInfoPojo.getNoOfExtrDnaAliquots());
			    entitySamplesPojo.setNoMiscMat(cbbProcedureInfoPojo.getNoOfCellMaterAliquots());	
			    entitySamplesPojo.setExtDnaMat(cbbProcedureInfoPojo.getNoOfExtrDnaMaterAliquots());
			    setEntitySamplesPojo(entitySamplesPojo);
				sites = new CBBController().getSites();
		        setSites(sites);  
				request.setAttribute("showProc", true);
			}   			
		}
		return SUCCESS;		
	}
	
	public String openOrganisation() throws Exception{		
		request.setAttribute("srcmenu","tdMenuBarItem2");
		return SUCCESS;	
	}
	
	public String checkForProcProcedure(){
		try{
			if(request.getParameter("procId")!=null && !request.getParameter("procId").equals("")){
				setProcProcAssignToCordFlag(cbbController.checkForProcAssignment(request.getParameter("procId")));
				//log.debug("count is:::::::::::::"+getProcProcAssignToCordFlag());
				setProcCordsMaxCollDt(cbbController.getProcCordsMaxCollectionDt(request.getParameter("procId")));
			}
		}catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage());
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	public String checkForProcName(){
		try{
			//log.debug("request.getParameter(ProcName)::::::::"+request.getParameter("ProcName"));
			if(request.getParameter("ProcName")!=null && !request.getParameter("ProcName").equals("")){
				String cbbId = request.getParameter("cbbId");
				setProcNameExistsFlag(cbbController.checkForProcName(request.getParameter("ProcName"),cbbId));
				//log.debug("getProcNameExistsFlag()::::::::::"+getProcNameExistsFlag());
			}
			saveCBBProcedureDetails();
		}catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String addStorageLocation(){
		List<SitePojo> sitePojoArr= null;
		
		try{
			sitePojoArr = new ArrayList<SitePojo>();
			
			setCbuCollectionPro(request.getParameter("cbuCollectionPro"));
			
			
			//log.debug("cbuCollectioncbuCollection = "+cbuCollectionPro);
			
			//log.debug("getSite().getSiteId() ==  "+getSite().getSiteId());
			//log.debug("getSite().getSiteId() ==  "+getSite().getSiteParent());
			
			
			cbuStrLocationList=(List<SitePojo>) new CBUAction().setMultipleValues(VelosGarudaConstants.SITE_TYPE,getSite().getSiteId(),VelosGarudaConstants.CBU_STORAGE);
			setCbuStrLocationList(cbuStrLocationList);
			
			
			if(getCbuCollectionPro().equalsIgnoreCase("true"))
			{
				sitePojoArr = cbbController.getMultipleValuesByErSites(getSite().getSiteId(),VelosGarudaConstants.SITE_TYPE,VelosGarudaConstants.CBU_COLLECTION);
			}
			else{
				sitePojoArr = cbbController.getMultipleValuesByErSites(getSite().getSiteId(),VelosGarudaConstants.SITE_TYPE,VelosGarudaConstants.CBU_STORAGE);
			}	
			setSitePojoArr(sitePojoArr);	
			
			cbuCollectionList=(List<SitePojo>) new CBUAction().setMultipleValues(VelosGarudaConstants.SITE_TYPE,getSite().getSiteId(),VelosGarudaConstants.CBU_COLLECTION);
			setCbuCollectionList(cbuCollectionList);
			
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			
			log.error(e.getMessage());
		}
		
		return SUCCESS;
	}
	
	
	public String saveStorageLocation(){
		UserJB user = (UserJB)getRequest().getSession().getAttribute(VelosGarudaConstants.GARUDA_USER);
		SitePojo  sitePojoNew =null;
		String siteName= null;
		CodeList codeListStrLocation= null;
		CodeList codeListCollSite= null;
		boolean dupFlag = false;
		
		
		try{				
			
			setCbuCollectionPro(request.getParameter("cbuCollectionPro"));
			if(getCbuCollectionPro().equalsIgnoreCase("true"))
			{
				//cbuStrLocationList=(List<SitePojo>) new CBUAction().setMultipleValues(VelosGarudaConstants.SITE_TYPE,getSite().getSiteId(),VelosGarudaConstants.CBU_COLLECTION);
				cbuStrLocationList=(List<SitePojo>) cbbController.getMultipleValuesByErSites(getSite().getSiteId(),VelosGarudaConstants.SITE_TYPE,VelosGarudaConstants.CBU_COLLECTION);
			}
			else{
				//cbuStrLocationList=(List<SitePojo>) new CBUAction().setMultipleValues(VelosGarudaConstants.SITE_TYPE,getSite().getSiteId(),VelosGarudaConstants.CBU_STORAGE);
				cbuStrLocationList=(List<SitePojo>) cbbController.getMultipleValuesByErSites(getSite().getSiteId(),VelosGarudaConstants.SITE_TYPE,VelosGarudaConstants.CBU_STORAGE);
			}
			
			setCbuStrLocationList(cbuStrLocationList);

			
			
			for(int index=0; index< cbuStrLocationList.size(); index++)
			{
					//log.debug("getSitePojoArr "+index+"  =  "+getSitePojoArr().get(index).getSiteHidden());
					cbuStrLocationList.get(index).setSiteHidden(getSitePojoArr().get(index).getSiteHidden());
					cbbController.saveSite(cbuStrLocationList.get(index));
					
			}
			
			if(getSite().getSiteName()!=null && !getSite().getSiteName().trim().equals(""))					
			{
				siteName = getSite().getSiteName().trim();
				
				for(int index=0; index< cbuStrLocationList.size(); index++)
				{
					if(siteName.equals(cbuStrLocationList.get(index).getSiteName().trim()))
					{
						dupFlag =true;
						break;
					}
					
				}
				if(!dupFlag)
				{	
					if(getCbuCollectionPro().equalsIgnoreCase("true"))
					{
						codeListStrLocation = cbbController.getCodeList(VelosGarudaConstants.SITE_TYPE,VelosGarudaConstants.CBU_COLLECTION);
					}else
					{
						codeListStrLocation = cbbController.getCodeList(VelosGarudaConstants.SITE_TYPE,VelosGarudaConstants.CBU_STORAGE);
					}
					//codeListStrLocation.getPkCodeId();
					//log.debug("siteCodelstType  codeListStrLocation.getPkCodeId() = "+codeListStrLocation.getPkCodeId());
					//log.debug("parent Site ID ==  "+getSite().getSiteId());
					//log.debug(" Site Name ==  "+getSite().getSiteName());
					sitePojoNew = new SitePojo();
					sitePojoNew.setSiteName(getSite().getSiteName().trim());
					sitePojoNew.setSiteParent(getSite().getSiteId());
					sitePojoNew.setSiteHidden("1");								
					sitePojoNew.setSiteAccountId(Long.parseLong(user.getUserAccountId()));				
					sitePojoNew.setSiteCodelstType(codeListStrLocation.getPkCodeId());							
					sitePojoNew=cbbController.saveSite(sitePojoNew);	
				}
			}
			
			cbuCollectionList=(List<SitePojo>) new CBUAction().setMultipleValues(VelosGarudaConstants.SITE_TYPE,getSite().getSiteId(),VelosGarudaConstants.CBU_COLLECTION);
			setCbuCollectionList(cbuCollectionList);
			
			/// Show section Start//////////
			/*
			List<SitePojo> cbbList = cbbController.getCBBDetails(getSite().getSiteId());
			List<String> linkList = null;
			
			List<SitePojo> sitePojoArr= null;
			sitePojoArr = new ArrayList<SitePojo>();
			
			List<SitePojo> CollSitePojoArr= null;
			CollSitePojoArr = new ArrayList<SitePojo>();
			setNmdpCbuIdLocalAssgnId(getCodeListPkByTypeAndSubtype(LOCAL_ASSIGNMENT_ID, NMDP_LOCAL_ASSIGNMENT_ID));
			setUserDefindeLocalAssgnId(getCodeListPkByTypeAndSubtype(LOCAL_ASSIGNMENT_ID, USER_DEFINED_LOCAL_ASSIGNMENT_ID));
			setAutoMaticSequencingLocalAssgnId(getCodeListPkByTypeAndSubtype(LOCAL_ASSIGNMENT_ID, AUTOMATIC_SEQUENCING_LOCAL_ASSIGNMENT_ID));
			
			if (cbbList.size() > 0) {
				setCbbPojo(cbbList.get(0).getCbb());
				setSite(cbbList.get(0));
			}
			if(getSite().getSiteCodelstType()!= null){		
			setCbbType(getCBBType(getSite().getSiteCodelstType().toString()));
			}
			AttachmentAction attAction = new AttachmentAction();		
			linkList= attAction.getnmdpLinkFrCbbProfile();	
			if(linkList!= null){
				setLinkDesc(linkList.get(0));
				setLinkUrl(linkList.get(1));
			}
			Map<String, List<Long>> map = new CBUAction().setMultipleValues(VelosGarudaConstants.ENTITY_CBB,cbbList.get(0).getCbb().getPkCbbId());
			List<Long> fkAccreditationArr = map.get(VelosGarudaConstants.ACCREDITITAON);
			if(fkAccreditationArr!=null && fkAccreditationArr.size()>0){			
			 setFkAccreditation(fkAccreditationArr.toArray(new Long[0]));
			}
			
			cbuStrLocationList=(List<SitePojo>) new CBUAction().setMultipleValues(VelosGarudaConstants.SITE_TYPE,getSite().getSiteId(),VelosGarudaConstants.CBU_STORAGE);
			setCbuStrLocationList(cbuStrLocationList);
			
			cbuCollectionList=(List<SitePojo>) new CBUAction().setMultipleValues(VelosGarudaConstants.SITE_TYPE,getSite().getSiteId(),VelosGarudaConstants.CBU_COLLECTION);
			setCbuCollectionList(cbuCollectionList);
			
			sitePojoArr = cbbController.getMultipleValuesByErSite(getSite().getSiteId(),VelosGarudaConstants.SITE_TYPE,VelosGarudaConstants.CBU_STORAGE,1);
			setSitePojoArr(sitePojoArr);
			
			CollSitePojoArr = cbbController.getMultipleValuesByErSite(getSite().getSiteId(),VelosGarudaConstants.SITE_TYPE,VelosGarudaConstants.CBU_COLLECTION,1);
			setCollSitePojoArr(CollSitePojoArr);
			
				Map<String, List<Long>> multipleMap = new CBUAction().setMultipleValues(VelosGarudaConstants.ENTITY_CBB,getSite().getSiteId());
				List<Long> cbuOnBagArr = multipleMap.get(VelosGarudaConstants.ALL_IDS);
				
				
				if(cbuOnBagArr!=null && cbuOnBagArr.size()>0){			
				  setCbuOnBag(cbuOnBagArr.toArray(new Long[0]));
				  new CBUAction().setIdsOnBag(getCbuOnBag());
				}					
					
			setFkCbuStorage(cbbList.get(0).getCbb().getFkCbuStorage());
			setFkCbuCollection(cbbList.get(0).getCbb().getFkCbuCollection());
		*/
			
		
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e.getMessage());
			
		}
		
		return SUCCESS;
	}

	
	public String siteExist(){
		Boolean isSite = false;
		String siteName = null;
		Long siteId = 0L;
		String cbuCollectionPro=null;
		List<SitePojo> sitePojoArr= null;
		sitePojoArr = new ArrayList<SitePojo>();
		if (request.getParameter("siteName") != null) {
			siteName = request.getParameter("siteName");
		}
		if (request.getParameter("siteId") != null) {
			siteId = new Long(request.getParameter("siteId"));
		}
		if (request.getParameter("cbuCollectionPro") != null) {
			cbuCollectionPro = request.getParameter("cbuCollectionPro");
		}
		
		if(cbuCollectionPro.equalsIgnoreCase("true"))
		{
			sitePojoArr = cbbController.getMultipleValuesByErSites(siteId,VelosGarudaConstants.SITE_TYPE,VelosGarudaConstants.CBU_COLLECTION);
		}
		else{
			sitePojoArr = cbbController.getMultipleValuesByErSites(siteId,VelosGarudaConstants.SITE_TYPE,VelosGarudaConstants.CBU_STORAGE);
		}	
		
		for(int index=0;index<sitePojoArr.size();index++){
			if(siteName.trim().equals(sitePojoArr.get(index).getSiteName())){
				isSite=true;
				break;
			}
			
		}
		if(!isSite){
			
			setSiteExist("NO");
			return SUCCESS;
		}else{
			setSiteExist("Yes");
			return ERROR;
		}
		
	}
	
	
	public String cbbIdExist(){
		
		String cbbId = null;		
		if (request.getParameter("cbbId") != null) {
			cbbId = request.getParameter("cbbId").toString().trim();
		}
		
	
		if(cbbController.isCbbIdExist(cbbId)){
			
			setCbbIdExists("NO");
			return SUCCESS;
		}else{
			setCbbIdExists("Yes");
			return ERROR;
		}
		
	}
	
	public String refreshStrLocationCollSite(){
			
		List<SitePojo> strLocationList = new ArrayList<SitePojo>();
			Long siteId=null;
			try{
			if (request.getParameter("SiteType") != null && request.getParameter("siteId") !=null ) {
				siteId= new Long(request.getParameter("siteId").toString());
				//log.debug(" siteId id is  " + siteId);
			}
				if(request.getParameter("SiteType").toString().equalsIgnoreCase("strCol"))
				{
					strLocationList=(List<SitePojo>) cbbController.getMultipleValuesByErSite(siteId,VelosGarudaConstants.SITE_TYPE,VelosGarudaConstants.CBU_COLLECTION,1);
					setSitePojoArr(strLocationList);
				}
				else{
					strLocationList=(List<SitePojo>)cbbController.getMultipleValuesByErSite(siteId,VelosGarudaConstants.SITE_TYPE,VelosGarudaConstants.CBU_STORAGE,1);
					setSitePojoArr(strLocationList);
				}
				
			}
			catch(Exception ex)
			{
				log.error(ex.getMessage());
				ex.printStackTrace();
				return ERROR;
			}
			
				return SUCCESS;
			
		}
	}
