package com.velos.ordercomponent.util;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class GarudaMessages implements ExceptionMessageConstants {
	public static final Log log=LogFactory.getLog(GarudaMessages.class);
	public static String getMessageByKey(String key){
		String message = "";
		Boolean flag = false;
		try{
			ResourceBundle customerBundle = ResourceBundle.getBundle(CUSTOMER_BUNDLE_NAME);
			message = customerBundle.getString(key);	
			flag = true;
		}catch (NullPointerException ne) {
			log.error(ne.getMessage());
			ne.printStackTrace();
		}catch(ClassCastException ce){
			log.error(ce.getMessage());
			ce.printStackTrace();
		}catch (MissingResourceException me) {
			log.error(me.getMessage());
			me.printStackTrace();			
		}
		if(!flag){
			try{
				ResourceBundle bundle = ResourceBundle.getBundle(BUNDLE_NAME);
				message = bundle.getString(key);
				}catch (NullPointerException ne) {
					log.error(ne.getMessage());
					ne.printStackTrace();
				}catch(ClassCastException ce){
					log.error(ce.getMessage());
					ce.printStackTrace();
				}catch (MissingResourceException me) {
					log.error(me.getMessage());
					me.printStackTrace();
				}
		}		
		return message;
	}
	
	public static String getApplicationResourceMessageByKey(String key){
		String message = "";
		try{
		ResourceBundle bundle = ResourceBundle.getBundle(APP_RESOURCE_BUNDLE_NAME);
		message = bundle.getString(key);
		}catch (NullPointerException ne) {
			log.error(ne.getMessage());
			ne.printStackTrace();
		}catch(ClassCastException ce){
			log.error(ce.getMessage());
			ce.printStackTrace();
		}catch (MissingResourceException me) {
			log.error(me.getMessage());
						me.printStackTrace();
		}
		return message;
	}

	public static final String REQUIRED_MSG = getMessageByKey(REQUIRED); 
	
	public static final String CORDENTRY_MSG = getMessageByKey(FORCORDENTRY); 
	
	public static final String REQ_CBU_REG_ID_MSG = getMessageByKey(REQ_CBU_REG_ID);
	
	public static final String COMMON_REQ_ALPHANUM_MSG = getMessageByKey(COMMON_REQ_ALPHANUM);
}
