package com.velos.ordercomponent.action;



import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.ordercomponent.business.domain.CBUUnitReport;
import com.velos.ordercomponent.business.domain.CdrCbu;
import com.velos.ordercomponent.business.domain.CodeList;
import com.velos.ordercomponent.business.domain.CordHlaExt;
import com.velos.ordercomponent.business.pojoobjects.AttachmentPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUUnitReportPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUploadInfoPojo;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.CordHlaExtPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.pojoobjects.SearchPojo;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.Utilities;
import com.velos.ordercomponent.business.util.VelosUtil;
import com.velos.ordercomponent.controller.AttachmentController;
import com.velos.ordercomponent.controller.AuditTrailController;
import com.velos.ordercomponent.controller.CBBController;
import com.velos.ordercomponent.controller.CBUController;
import com.velos.ordercomponent.controller.CBUUnitReportController;
import com.velos.ordercomponent.controller.CodelstController;
import com.velos.ordercomponent.controller.VelosSearchController;
import com.velos.ordercomponent.helper.AttachmentHelper;
import com.velos.ordercomponent.helper.CBUUnitReportHelper;
import com.velos.ordercomponent.helper.velosHelper;
import com.velos.ordercomponent.util.VelosGarudaConstants;

public class CBUUnitReportAction extends OpenOrderAction {
	

	
	private final String CORD_VIAB_METHOD_LISTNAME = "listViabMethod";
	private final String CFU_POST_METHOD_LISTNAME="listCfuPostMethod";
	private final String HEMO_PATHYSCREEN_LISTNAME="listHemoPathyScr";
	private final String PROCESSING_METHOD_LISTNAME="listProcessMethod";
	private final String TYPE_OF_BAG_LISTNAME="listBagType";
	private final String PRODUCT_MODIFICATION_LISTNAME="listProductModify";
	private final String NUM_OF_SEGMENTS_LISTNAME="listNumOfSegments";
	private final String BACTERIAL_STATUS_LISTNAME="listBacterial";
	private final String FUNGAL_STATUS_LISTNAME="listFungal";
	private final String COMPLETED_STATUS_LISTNAME="listCompStatus";
	public static final Log log = LogFactory.getLog(CBUUnitReportAction.class);
	private List listViabMethod;
	private List listCfuPostMethod;
	private List listHemoPathyScr;
	private List listProcessMethod;
	private List listBagType;
	private List listProductModify;
	private List listNumOfSegments;
	private List listBacterial;
	private List listFungal;
	private List listCompStatus;
	private List lstObjecthla;
	private List lstObjectOne;
	private List lstObjecttwo;
	private List unitReportList;
	private PaginateSearch paginateSearch;
	private PaginateSearch paginationSearch1;
	private PaginateSearch paginationSearch2;
	private PaginateSearch paginationSearch3;
	private PaginateSearch paginationSearch4;
	private PaginateSearch paginationSearch5;
	private PaginateSearch paginationSearch6;
	private PaginateSearch paginationSearch7;
	private PaginateSearch paginationSearch8;
	private PaginateSearch paginationSearch9;
	private PaginateSearch paginationSearch10;
	private PaginateSearch paginationSearch11;
	private PaginateSearch paginationSearch12;
	private PaginateSearch paginationSearch13;
	private PaginateSearch paginationSearch14;
	private PaginateSearch paginationSearch15;
	private PaginateSearch paginationSearch16;


	private List revokedUnitReportList;
    private String cbuId;
	private Boolean modifyUnitrpt;
	private Map codelstParam;
	private Map codelstParam1;
	private Map codelstParam2;
	private CodelstController codelstController;
	private CBUUnitReportController linkController;
	private VelosSearchController velosSearchController;
	private CBBController cBBController;
	private CBUUnitReportPojo cbuUnitReportPojo;
	private CordHlaExtPojo cordHlaExtPojo;
	private CBUploadInfoPojo cbUploadInfoPojo;
	private CdrCbuPojo cdrCbuPojo;
	private CdrCbu cdrCbu;
	private CordHlaExt CordHlaExt;
	private CBUUnitReport cbuUnitReport;
	private SearchPojo searchPojo;
	private SearchPojo searchPojo1;
	private SearchPojo searchPojo2;
	private Long cordID;	
	private Long dcmsFileAttchId;
    private List<Object> auditTrailRowData;
    private List<Object> auditTrailAllData;
    private List<Object> auditTrailColumnData;
    private Long auditRowPk;   
    private Long entityId;
	private String entityType;
	private String entitySubtype;
	private List uploadedDocumentLst;
    private List<Object> auditTrailEntityStatusRowData;
	private List<Object> auditTrailEntityStatusReasonRowData;
	private List<Object> auditTrailMultipleValuesRowData;
	private List<Object> auditTrailReviewRowData;
	private List<Object> auditTrailHlaRowData;
	private List<Object> auditTrailPatHlaRowData;
	private List<Object> auditTrailLabSummaryRowData;
	private List<Object> auditTrailBestHlaRowData;
	private List<Object> auditTrailPfRowData;
	private List<Object> auditTrailIdRowData;
	private List<Object> auditTrailProcInfoRowData;
	private List<Object> auditTrailMinCritInfoRowData;
	private String fprocStrDt1;
	private String fprocStrDt2;
	
	
	public String getFprocStrDt1() {
		return fprocStrDt1;
	}

	public String getFprocStrDt2() {
		return fprocStrDt2;
	}

	public void setFprocStrDt1(String fprocStrDt1) {
		this.fprocStrDt1 = fprocStrDt1;
	}
	
	public void setFprocStrDt2(String fprocStrDt2) {
		this.fprocStrDt2 = fprocStrDt2;
	}

	
	public List<Object> getAuditTrailMinCritInfoRowData() {
		return auditTrailMinCritInfoRowData;
	}

	public void setAuditTrailMinCritInfoRowData(
			List<Object> auditTrailMinCritInfoRowData) {
		this.auditTrailMinCritInfoRowData = auditTrailMinCritInfoRowData;
	}

	public List<Object> getAuditTrailProcInfoRowData() {
		return auditTrailProcInfoRowData;
	}

	public void setAuditTrailProcInfoRowData(List<Object> auditTrailProcInfoRowData) {
		this.auditTrailProcInfoRowData = auditTrailProcInfoRowData;
	}

	public List<Object> getAuditTrailIdRowData() {
		return auditTrailIdRowData;
	}

	public void setAuditTrailIdRowData(List<Object> auditTrailIdRowData) {
		this.auditTrailIdRowData = auditTrailIdRowData;
	}

	public List<Object> getAuditTrailPfRowData() {
		return auditTrailPfRowData;
	}

	public void setAuditTrailPfRowData(List<Object> auditTrailPfRowData) {
		this.auditTrailPfRowData = auditTrailPfRowData;
	}

	public List<Object> getAuditTrailBestHlaRowData() {
		return auditTrailBestHlaRowData;
	}

	public void setAuditTrailBestHlaRowData(List<Object> auditTrailBestHlaRowData) {
		this.auditTrailBestHlaRowData = auditTrailBestHlaRowData;
	}

	public List<Object> getAuditTrailLabSummaryRowData() {
		return auditTrailLabSummaryRowData;
	}

	public void setAuditTrailLabSummaryRowData(
			List<Object> auditTrailLabSummaryRowData) {
		this.auditTrailLabSummaryRowData = auditTrailLabSummaryRowData;
	}


	private List<Object> auditTrailNotesRowData;
	public List<Object> getAuditTrailNotesRowData() {
		return auditTrailNotesRowData;
	}

	public void setAuditTrailNotesRowData(List<Object> auditTrailNotesRowData) {
		this.auditTrailNotesRowData = auditTrailNotesRowData;
	}


	private List<Object> auditTrailFormsRowData;
	
   
	public List<Object> getAuditTrailFormsRowData() {
		return auditTrailFormsRowData;
	}

	public void setAuditTrailFormsRowData(List<Object> auditTrailFormsRowData) {
		this.auditTrailFormsRowData = auditTrailFormsRowData;
	}
	
	private List<Object> auditTrailFormshssRowData;
	
	   
	public List<Object> getAuditTrailFormshssRowData() {
		return auditTrailFormshssRowData;
	}

	public void setAuditTrailFormshssRowData(List<Object> auditTrailFormshssRowData) {
		this.auditTrailFormshssRowData = auditTrailFormshssRowData;
	}

	public List<Object> getAuditTrailEntityStatusRowData() {
		return auditTrailEntityStatusRowData;
	}

	public void setAuditTrailEntityStatusRowData(
			List<Object> auditTrailEntityStatusRowData) {
		this.auditTrailEntityStatusRowData = auditTrailEntityStatusRowData;
	}

	public List<Object> getAuditTrailEntityStatusReasonRowData() {
		return auditTrailEntityStatusReasonRowData;
	}

	public void setAuditTrailEntityStatusReasonRowData(
			List<Object> auditTrailEntityStatusReasonRowData) {
		this.auditTrailEntityStatusReasonRowData = auditTrailEntityStatusReasonRowData;
	}

	public List<Object> getAuditTrailMultipleValuesRowData() {
		return auditTrailMultipleValuesRowData;
	}

	public void setAuditTrailMultipleValuesRowData(
			List<Object> auditTrailMultipleValuesRowData) {
		
		this.auditTrailMultipleValuesRowData = auditTrailMultipleValuesRowData;
	}
	
	public List<Object> getAuditTrailReviewRowData() {
		return auditTrailReviewRowData;
	}

	public void setAuditTrailReviewRowData(List<Object> auditTrailReviewRowData) {
		this.auditTrailReviewRowData = auditTrailReviewRowData;
	}

	 
	public List<Object> getAuditTrailHlaRowData() {
		return auditTrailHlaRowData;
	}

	public void setAuditTrailHlaRowData(List<Object> auditTrailHlaRowData) {
		this.auditTrailHlaRowData = auditTrailHlaRowData;
	}
	public List<Object> getAuditTrailPatHlaRowData() {
		return auditTrailPatHlaRowData;
	}

	public void setAuditTrailPatHlaRowData(List<Object> auditTrailPatHlaRowData) {
		this.auditTrailPatHlaRowData = auditTrailPatHlaRowData;
	}


	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public String getEntitySubtype() {
		return entitySubtype;
	}

	public void setEntitySubtype(String entitySubtype) {
		this.entitySubtype = entitySubtype;
	}

	public List<Object> getAuditTrailRowData() {
		return auditTrailRowData;
	}

	public void setAuditTrailRowData(List<Object> auditTrailRowData) {
		this.auditTrailRowData = auditTrailRowData;
	}
	
	public List<Object> getAuditTrailAllData() {
		return auditTrailAllData;
	}

	public void setAuditTrailAllData(List<Object> auditTrailAllData) {
		this.auditTrailAllData = auditTrailAllData;
	}
	

	public List<Object> getAuditTrailColumnData() {
		return auditTrailColumnData;
	}

	public void setAuditTrailColumnData(List<Object> auditTrailColumnData) {
		this.auditTrailColumnData = auditTrailColumnData;
	}

	public Long getAuditRowPk() {
		return auditRowPk;
	}

	public void setAuditRowPk(Long auditRowPk) {
		this.auditRowPk = auditRowPk;
	}
	public Long getCordID() {
		return cordID;
	}

	public void setCordID(Long cordID) {
		this.cordID = cordID;
	}
	
	public PaginateSearch getPaginateSearch() {
		return paginateSearch;
	}

	public void setPaginateSearch(PaginateSearch paginateSearch) {
		this.paginateSearch = paginateSearch;
	}

	public PaginateSearch getPaginationSearch1() {
		return paginationSearch1;
	}

	public void setPaginationSearch1(PaginateSearch paginationSearch1) {
		this.paginationSearch1 = paginationSearch1;
	}
	
	
	public PaginateSearch getPaginationSearch2() {
		return paginationSearch2;
	}

	public void setPaginationSearch2(PaginateSearch paginationSearch2) {
		this.paginationSearch2 = paginationSearch2;
	}
	
	
	public PaginateSearch getPaginationSearch3() {
		return paginationSearch3;
	}

	public void setPaginationSearch3(PaginateSearch paginationSearch3) {
		this.paginationSearch3 = paginationSearch3;
	}
	
	public PaginateSearch getPaginationSearch4() {
		return paginationSearch4;
	}

	public void setPaginationSearch4(PaginateSearch paginationSearch4) {
		this.paginationSearch4 = paginationSearch4;
	}
	
	public PaginateSearch getPaginationSearch5() {
		return paginationSearch5;
	}

	public void setPaginationSearch5(PaginateSearch paginationSearch5) {
		this.paginationSearch5 = paginationSearch5;
	}
	
	public PaginateSearch getPaginationSearch6() {
		return paginationSearch6;
	}

	public void setPaginationSearch6(PaginateSearch paginationSearch6) {
		this.paginationSearch6 = paginationSearch6;
	}
	
	public PaginateSearch getPaginationSearch7() {
		return paginationSearch7;
	}

	public void setPaginationSearch7(PaginateSearch paginationSearch7) {
		this.paginationSearch7 = paginationSearch7;
	}
	
	public PaginateSearch getPaginationSearch8() {
		return paginationSearch8;
	}

	public void setPaginationSearch8(PaginateSearch paginationSearch8) {
		this.paginationSearch8 = paginationSearch8;
	}
	
	public PaginateSearch getPaginationSearch9() {
		return paginationSearch9;
	}

	public void setPaginationSearch9(PaginateSearch paginationSearch9) {
		this.paginationSearch9 = paginationSearch9;
	}
	
	public PaginateSearch getPaginationSearch10() {
		return paginationSearch10;
	}

	public void setPaginationSearch10(PaginateSearch paginationSearch10) {
		this.paginationSearch10 = paginationSearch10;
	}
	
	public PaginateSearch getPaginationSearch11() {
		return paginationSearch11;
	}

	public void setPaginationSearch11(PaginateSearch paginationSearch11) {
		this.paginationSearch11 = paginationSearch11;
	}
	
	public PaginateSearch getPaginationSearch12() {
		return paginationSearch12;
	}

	public void setPaginationSearch12(PaginateSearch paginationSearch12) {
		this.paginationSearch12 = paginationSearch12;
	}
	
	public PaginateSearch getPaginationSearch13() {
		return paginationSearch13;
	}

	public void setPaginationSearch13(PaginateSearch paginationSearch13) {
		this.paginationSearch13 = paginationSearch13;
	}
	
	public PaginateSearch getPaginationSearch14() {
		return paginationSearch14;
	}

	public void setPaginationSearch14(PaginateSearch paginationSearch14) {
		this.paginationSearch14 = paginationSearch14;
	}
	
	public String getDescriptionUploadInfo(Long attachmentID) throws Exception{
		return CBUUnitReportHelper.getDescriptionUploadInfo(attachmentID);
	}
	
	public Date getCompltionDateUploadInfo(Long attachmentID) throws Exception{
		return CBUUnitReportHelper.getCompltionDateUploadInfo(attachmentID);
	}
	
	public Long getDcmsFileAttchId(Long attachmentID) throws Exception{
		return AttachmentHelper.getDcmsFileAttchId(attachmentID);
	}
	
	public String getDcmsFileAttchType(Long attachmentID)throws Exception{
		return AttachmentHelper.getDcmsFileDocType(attachmentID);
	}
	public String getDcmsAttchFileName(Long attachmentID)throws Exception{
		return AttachmentHelper.getDcmsAttchFileName(attachmentID);
	}
	
	

	public CBUUnitReportAction(){
		codelstController = new CodelstController();
		linkController = new CBUUnitReportController();
		cdrCbuPojo=new CdrCbuPojo();
		cbUploadInfoPojo= new CBUploadInfoPojo() ;
		cBBController = new CBBController() ;
		cbuUnitReportPojo=new CBUUnitReportPojo();
		cordHlaExtPojo=new CordHlaExtPojo();
		paginateSearch = new PaginateSearch();
		paginationSearch1=new PaginateSearch();
		paginationSearch2=new PaginateSearch();
		paginationSearch3=new PaginateSearch();
		paginationSearch4=new PaginateSearch();
		paginationSearch5=new PaginateSearch();
		paginationSearch6=new PaginateSearch();
		paginationSearch7=new PaginateSearch();
		paginationSearch8=new PaginateSearch();
		paginationSearch9=new PaginateSearch();
		paginationSearch10=new PaginateSearch();
		paginationSearch11=new PaginateSearch();
		paginationSearch12=new PaginateSearch();
		paginationSearch13=new PaginateSearch();
		paginationSearch14=new PaginateSearch();
		paginationSearch15=new PaginateSearch();
		paginationSearch16=new PaginateSearch();
	}	

	public String getCbuId() {
		return cbuId;
	}
	public void setCbuId(String cbuId) {
		this.cbuId = cbuId;
	}
	public CBUUnitReportPojo getCbuUnitReportPojo() {
		return cbuUnitReportPojo;
	}
	public void setCbuUnitReportPojo(CBUUnitReportPojo cbuUnitReportPojo) {
		this.cbuUnitReportPojo = cbuUnitReportPojo;
	}
	public String getUnitReport()throws Exception {
		getCodelstDetails();
		getCdrCbuPojo().setCordID((Long.parseLong(request.getParameter("cordID"))));
		getCbuInfo();
		//log.debug("inside getCbuHome"+getListViabMethod());
		return "success";
	}
	
	public VelosSearchController getVelosSearchController() {
		if (velosSearchController == null) {
			velosSearchController = new VelosSearchController();
		}
		return velosSearchController;
	}

	public void setVelosSearchController(
			VelosSearchController velosSearchController) {
		this.velosSearchController = velosSearchController;
	}

	public CBUUnitReportController getLinkController() {
		return linkController;
	}

	public void setLinkController(CBUUnitReportController linkController) {
		this.linkController = linkController;
	}

	public List getListViabMethod() {
		return listViabMethod;
	}
	
	
	
	public List getRevokedUnitReportList() {
		return revokedUnitReportList;
	}

	public void setRevokedUnitReportList(List revokedUnitReportList) {
		this.revokedUnitReportList = revokedUnitReportList;
	}

	public CBUploadInfoPojo getCbUploadInfoPojo() {
		return cbUploadInfoPojo;
	}

	public void setCbUploadInfoPojo(CBUploadInfoPojo cbUploadInfoPojo) {
		this.cbUploadInfoPojo = cbUploadInfoPojo;
	}

	public CordHlaExtPojo getCordHlaExtPojo() {
		return cordHlaExtPojo;
	}
	
	public List getLstObjecthla() {
		return lstObjecthla;
	}

	public void setLstObjecthla(List lstObjecthla) {
		this.lstObjecthla = lstObjecthla;
	}

	public void setCordHlaExtPojo(CordHlaExtPojo cordHlaExtPojo) {
		this.cordHlaExtPojo = cordHlaExtPojo;
	}
	public List getLstObjecttwo() {
		return lstObjecttwo;
	}
	public List getLstObjectOne() {
		return lstObjectOne;
	}
	
	public SearchPojo getSearchPojo() {
		return searchPojo;
	}

	public void setSearchPojo(SearchPojo searchPojo) {
		this.searchPojo = searchPojo;
	}
	
	public void setLstObjectOne(List lstObjectOne) {
		this.lstObjectOne = lstObjectOne;
	}
	public void setLstObjecttwo(List lstObjecttwo) {
		this.lstObjecttwo = lstObjecttwo;
	}

	public void setListViabMethod(List listViabMethod) {
		this.listViabMethod = listViabMethod;
	}

	public List getListCfuPostMethod() {
		return listCfuPostMethod;
	}
	public void setListCfuPostMethod(List listCfuPostMethod) {
		this.listCfuPostMethod = listCfuPostMethod;
	}
	public List getListHemoPathyScr() {
		return listHemoPathyScr;
	}
	public void setListHemoPathyScr(List listHemoPathyScr) {
		this.listHemoPathyScr = listHemoPathyScr;
	}
	public List getListProcessMethod() {
		return listProcessMethod;
	}
	public void setListProcessMethod(List listProcessMethod) {
		this.listProcessMethod = listProcessMethod;
	}
	public List getListBagType() {
		return listBagType;
	}
	public void setListBagType(List listBagType) {
		this.listBagType = listBagType;
	}
	public List getListProductModify() {
		return listProductModify;
	}
	public void setListProductModify(List listProductModify) {
		this.listProductModify = listProductModify;
	}
	public List getListNumOfSegments() {
		return listNumOfSegments;
	}
	public void setListNumOfSegments(List listNumOfSegments) {
		this.listNumOfSegments = listNumOfSegments;
	}
	public List getListBacterial() {
		return listBacterial;
	}
	public void setListBacterial(List listBacterial) {
		this.listBacterial = listBacterial;
	}
	public List getListFungal() {
		return listFungal;
	}
	public void setListFungal(List listFungal) {
		this.listFungal = listFungal;
	}
	public Map getCodelstParam() {
		return codelstParam;
	}
	
	public void setCodelstParam(Map codelstParam) {
		this.codelstParam = codelstParam;
	}
	
	public CdrCbuPojo getCdrCbuPojo() {
		if(cdrCbuPojo==null){
			cdrCbuPojo=new CdrCbuPojo();
		}
		//log.debug("Inside getter- : ");
		return cdrCbuPojo;
		
	}
	public void setUnitReportList(List list) {
		// TODO Auto-generated method stub
		this.unitReportList = list;
	}
	public List getUnitReportList() {
		// TODO Auto-generated method stub
		return unitReportList;
	}
	public List getUploadedDocumentLst() {
		return uploadedDocumentLst;
	}

	public void setUploadedDocumentLst(List uploadedDocumentLst) {
		this.uploadedDocumentLst = uploadedDocumentLst;
	}


	public void setCdrCbuPojo(CdrCbuPojo cdrCbuPojo) {
		this.cdrCbuPojo = cdrCbuPojo;
	}
	
	public CodelstController getCodelstController() {
		return codelstController;
	}

	public PaginateSearch getPaginationSearch15() {
		return paginationSearch15;
	}

	public void setPaginationSearch15(PaginateSearch paginationSearch15) {
		this.paginationSearch15 = paginationSearch15;
	}
	
	public PaginateSearch getPaginationSearch16() {
		return paginationSearch16;
	}

	public void setPaginationSearch16(PaginateSearch paginationSearch16) {
		this.paginationSearch16 = paginationSearch16;
	}
	
	public void setCodelstController(CodelstController codelstController) {
		this.codelstController = codelstController;
	}

	public void getCodelstDetails() throws Exception {
		//log.debug("inside code list details");
		
		codelstParam = new HashMap<String, String>();
		codelstParam.put(CORD_VIAB_METHOD_LISTNAME,CORD_VIAB_METHOD);
		codelstParam.put(CFU_POST_METHOD_LISTNAME,CFU_POST_METHOD);
		codelstParam.put(HEMO_PATHYSCREEN_LISTNAME,HEMO_PATHYSCREEN);
		codelstParam.put(PROCESSING_METHOD_LISTNAME,PROCESSING_METHOD);
		codelstParam.put(TYPE_OF_BAG_LISTNAME,TYPE_OF_BAG);
		codelstParam.put(PRODUCT_MODIFICATION_LISTNAME,PRODUCT_MODIFICATION);
		codelstParam.put(NUM_OF_SEGMENTS_LISTNAME,NUM_OF_SEGMENTS);
		codelstParam.put(BACTERIAL_STATUS_LISTNAME,BACTERIAL_STATUS);
		codelstParam.put(FUNGAL_STATUS_LISTNAME,FUNGAL_STATUS);
		codelstParam.put(COMPLETED_STATUS_LISTNAME,COMPLETED_STATUS);
		
		
		//log.debug("codelstParam" + codelstParam);
		
		Map codeListMap = null;
		try {
			
			codeListMap = codelstController.getCodelistValues(codelstParam);
						
			
			
		} catch (Exception e) {
			//log.debug(e);
log.error(e.getMessage());
			e.printStackTrace();
		}
		//log.debug("codeListMap"+codeListMap);
		setListViabMethod((List) codeListMap.get(CORD_VIAB_METHOD_LISTNAME));
		setListCfuPostMethod((List) codeListMap.get(CFU_POST_METHOD_LISTNAME));
		setListHemoPathyScr((List) codeListMap.get(HEMO_PATHYSCREEN_LISTNAME));
		setListProcessMethod((List) codeListMap.get(PROCESSING_METHOD_LISTNAME));
		setListBagType((List) codeListMap.get(TYPE_OF_BAG_LISTNAME));
		setListProductModify((List) codeListMap.get(PRODUCT_MODIFICATION_LISTNAME));
		setListNumOfSegments((List) codeListMap.get(NUM_OF_SEGMENTS_LISTNAME));
		setListBacterial((List) codeListMap.get(BACTERIAL_STATUS_LISTNAME));
		setListFungal((List) codeListMap.get(FUNGAL_STATUS_LISTNAME));
		setListCompStatus((List) codeListMap.get(COMPLETED_STATUS_LISTNAME));
		
		
	}

	public List getListCompStatus() {
		return listCompStatus;
	}

	public void setListCompStatus(List listCompStatus) {
		this.listCompStatus = listCompStatus;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String executeAction() throws Exception {
		// TODO Auto-generated method stub
		return SUCCESS;
	}
	public List<Object> getCbuInfoById(String entityType, String entityValue)throws Exception{
		
		return new CBUController().getCbuInfoById(entityType,entityValue);
		
		}
	
	public void getCbuInfo() throws Exception{
	if(getCordID()!=null)
		{
		  getCdrCbuPojo().setCordID(getCordID());
			try{			
				setCdrCbuPojo(new CBUController().getCordInfoById(getCdrCbuPojo()));				
			}catch(Exception e){
				log.error(e.getMessage());
				e.printStackTrace();
			
				
			}
		}		
	}
	
	/*public String saveCBUUnitReport() throws Exception{
		//log.debug("Time Stamp in CBB Unit Report Action"+getTimestamp()+getRequest().getContextPath());
		if(getTimestamp()!=null){
			List<AttachmentPojo> attachments = (List<AttachmentPojo>)getSession(getRequest()).getAttribute(getTimestamp().toString());
			//log.debug("Attachment Pojo in CBB Unit Report Action"+attachments);	
			if(attachments.size()>0){
				for(AttachmentPojo a : attachments){
					a.setGarudaEntityId(getCdrCbuPojo().getCordID());
				}
				attachments = new AttachmentController().addMultipleAttachments(attachments);
			    getCbuUnitReportPojo().setAttachmentId(attachments.get(0).getAttachmentId());
		    } 
		}
		if(getCdrCbuPojo().getCordID()!=null)
		{
			getCbuUnitReportPojo().setFkCordCdrCbuId(getCdrCbuPojo().getCordID());
			getCordHlaExtPojo().setFkCordCdrCbuId(getCdrCbuPojo().getCordID());
		}
		
		cbuUnitReportPojo = getCbuUnitReportPojo() ;
		
		//Study study = null;
		//getCodelstDetails();
		try{
		linkController.saveCBUUnitReport(cbuUnitReportPojo);
		getCordHlaExtPojo().setFkCordExtInfo(getCbuUnitReportPk());
		cordHlaExtPojo=getCordHlaExtPojo();
		linkController.saveHlaInfo(cordHlaExtPojo);
		
		}catch(Exception e){
			e.printStackTrace();
		}
		getRequest().setAttribute("submit", "unitreport");
		return SUCCESS;
	}*/
	
	public String saveCBUUnitReport() throws Exception{
		//log.debug("Time Stamp in CBB Unit Report Action"+getTimestamp()+getRequest().getContextPath());
		if(getSession(getRequest()).getAttribute("attachmentList")!=null){
			List<AttachmentPojo> attachments = (List<AttachmentPojo>)getSession(getRequest()).getAttribute("attachmentList");
			//log.debug("Attachment Pojo in CBB Unit Report Action"+attachments);	
			if(attachments.size()>0){
				for(AttachmentPojo a : attachments){
					a.setGarudaEntityId(getCdrCbuPojo().getCordID());
				}
				attachments = new AttachmentController().addMultipleAttachments(attachments);
			    getCbuUnitReportPojo().setAttachmentId(attachments.get(0).getAttachmentId());
			    getCbUploadInfoPojo().setFk_AttachmentId(getCbuUnitReportPojo().getAttachmentId());
			} 
		}
	
		if(getCdrCbuPojo().getCordID()!=null)
		{
			getCbuUnitReportPojo().setFkCordCdrCbuId(getCdrCbuPojo().getCordID());
			//getCordHlaExtPojo().setFkCordCdrCbuId(getCdrCbuPojo().getCordID());
		}
		if(getCbUploadInfoPojo().getStrcompletionDate() != null && !getCbUploadInfoPojo().getStrcompletionDate().equals("")) {
			getCbUploadInfoPojo().setCompletionDate(new Date(Utilities.getFormattedDate(new Date(cbUploadInfoPojo.getStrcompletionDate()))));
			}
		getCbUploadInfoPojo().setFk_CategoryId(cBBController.getCodeListPkIds(CATEGORY, UNIT_REPORT_CODESUBTYPE));
		cbUploadInfoPojo = getCbUploadInfoPojo();
		cbuUnitReportPojo = getCbuUnitReportPojo();
		
		//Study study = null;
		//getCodelstDetails();
		try{
			if(getCbuUnitReportPojo().getAttachmentId()!=null){
			cbuUnitReportPojo=linkController.saveCBUUnitReport(cbuUnitReportPojo);
			linkController.saveUploadInfo(cbUploadInfoPojo);
			}
		//getCordHlaExtPojo().setFkCordExtInfo(getCbuUnitReportPk());
		//cordHlaExtPojo=getCordHlaExtPojo();
		//linkController.saveHlaInfo(cordHlaExtPojo);
		//linkController.saveHlaInfo(cbUploadInfoPojo);
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}
		getRequest().setAttribute("submit", "unitreport");
		return SUCCESS;
		}
		
		
	
	
	public String cbuSearchForUnitReport()
	{
		
		return SUCCESS;
	}
	public List getCordInfoExtPkId() throws Exception {
		
		String query = linkController.getcordinfopk();
				
		return velosHelper.getObjectList(query);			
				
}
	
		public Long getCbuUnitReportPk() throws Exception {
		
		
		Long pkCordExtInfo = null;
		
		
			try{
				
				List list = (List)getCordInfoExtPkId(); 
				if(list.size()>1) {
					cbuUnitReport = (CBUUnitReport)list.get(0);
					setCbuUnitReportPojo((CBUUnitReportPojo)ObjectTransfer.transferObjects(cbuUnitReport, cbuUnitReportPojo));
				}
				
				//setCdrCbuPojo((CdrCbuPojo)ObjectTransfer.transferObjects(cdrCbu, cbuUnitPojo));
				//log.debug("CdrCbuPojo In"+cdrCbuPojo.getLocalCbuId());
				
				pkCordExtInfo = cbuUnitReportPojo.getPkCordExtInfo();
				}catch(Exception e){
					log.error(e.getMessage());
					e.printStackTrace();
			}
			
			return pkCordExtInfo;
		}
		public String cdrCbuviewUnitReport(){
			String cordID=request.getParameter("cordID");
			try {
			List list = velosHelper.getObjectList(CBUUNITREPORT_OBJECT," fkCordCdrCbuId = '"+cordID+"'and attachmentId != null");
			getCdrCbuPojo().setCordID(Long.parseLong(cordID));
			try{			
				setCdrCbuPojo(new CBUController().getCordInfoById(getCdrCbuPojo()));				
			}catch(Exception e){
				log.error(e.getMessage());
				e.printStackTrace();
			}
			setUnitReportList(list);
			//log.debug("list"+getUnitReportList());
			//cdrCbu = (CdrCbu)list.get(0);
			}
			catch(Exception e){//log.debug(e);
				log.error(e.getMessage());
				e.printStackTrace();
				}
			
			return "success";
		}
		
		public String cdrCbucreateUnitReport(){
			
			return "success";
		}
		
		public String revokedUnitReports() throws Exception{
			String cordID =null;
			if(request.getParameter("cordID")!=null && !request.getParameter("cordID").equals("")){
				cordID = request.getParameter("cordID");
			}
			try{
				getCdrCbuPojo().setCordID(Long.parseLong(cordID));
				setCdrCbuPojo(new CBUController().getCordInfoById(getCdrCbuPojo()));
			}
			catch(Exception e){
				log.error(e.getMessage());
				e.printStackTrace();
			}
			
			return "success";
		}
		
		public String getDocInfoAuditCondition(){
			String condition="";
			String dropDownNames[]={"fromDocDt","toDocDt","doccat","docsubcat","docusrid","docact"};
			String columnNames[]={"timestamp","timestamp","category","subcategory","user_id","action"};
			for(int i=0;i<dropDownNames.length;i++){
				if(request.getParameter(dropDownNames[i])!=null && !request.getParameter(dropDownNames[i]).equals("") && !request.getParameter(dropDownNames[i]).equals("undefined")){
					if(i==0){
						String val1=request.getParameter(dropDownNames[0]).toString();
						String val2=request.getParameter(dropDownNames[1]).toString();
						if(val1.equals(val2)){
							condition=condition+" to_char("+columnNames[i]+",'DD-Mon-YYYY')='"+request.getParameter(dropDownNames[0])+"'";
						}else{
							condition=condition+"  (trunc("+columnNames[i]+") between to_date('"+request.getParameter(dropDownNames[0])+"','DD-Mon-YYYY') and to_date('"+request.getParameter(dropDownNames[1])+"','DD-Mon-YYYY'))";
						}
					}
					if(i==0){
						setFprocStrDt1(Utilities.getOrdDateFormatDate(request.getParameter(dropDownNames[0]).toString()));
						setFprocStrDt2(Utilities.getOrdDateFormatDate(request.getParameter(dropDownNames[1]).toString()));
					}
					if (i==2) {
						if (condition != "")
							condition = condition + " and ";
						String categ = request.getParameter(dropDownNames[i]);
						condition = condition + columnNames[i] +" = '" + categ +"' ";
					}
					
					if (i==3) {
						if (condition != "")
							condition = condition + " and ";
						String subcat = request.getParameter(dropDownNames[i]);
						condition = condition +  columnNames[i] +" = '" + subcat+"' ";
					}
		
					
					if (i==4) {
						if (condition != "")
							condition = condition + " and ";
						String userId = request.getParameter(dropDownNames[i]);
						condition = condition + columnNames[i] +" = '" + userId+"' ";
					}
					
					
					if (i==5) {
						if (condition != "")
							condition = condition + " and ";
						String action = request.getParameter(dropDownNames[i]);
						condition = condition +  columnNames[i] +" = '" + action+"' ";
					}
				}
			}
			return condition;
		}		
		
		 public String getFormInfoAuditCondition(int k){
				String condition="";
				String[] dropDownNames = new String[5];
				dropDownNames [0] = "fromDt"+k;
				dropDownNames [1] = "toDt"+k;
				dropDownNames [2] = "formname"+k;
				dropDownNames [3] = "fversion"+k;
				dropDownNames [4] = "fuserid"+k;
				
				String columnNames[]={"created_on","created_on","formname","fversion","user_id"};

				for(int i=0;i<dropDownNames.length;i++){
					if(request.getParameter(dropDownNames[i])!=null && !request.getParameter(dropDownNames[i]).equals("") && !request.getParameter(dropDownNames[i]).equals("undefined")){
						if(i==0){
							String val1=request.getParameter(dropDownNames[0]).toString();
							String val2=request.getParameter(dropDownNames[1]).toString();
							if(val1.equals(val2)){
								condition=condition+" to_char("+columnNames[i]+",'DD-Mon-YYYY')='"+request.getParameter(dropDownNames[0])+"'";
							}else{
								condition=condition+"  (trunc("+columnNames[i]+") between to_date('"+request.getParameter(dropDownNames[0])+"','DD-Mon-YYYY') and to_date('"+request.getParameter(dropDownNames[1])+"','DD-Mon-YYYY'))";
							}
						}
						if(i==0){
							setFprocStrDt1(Utilities.getOrdDateFormatDate(request.getParameter(dropDownNames[0]).toString()));
							setFprocStrDt2(Utilities.getOrdDateFormatDate(request.getParameter(dropDownNames[1]).toString()));
						}
						
						if (i==2) {
							if (condition != "")
								condition = condition + " and ";
							String fName = request.getParameter(dropDownNames[i]);
							condition = condition + columnNames[i] +" = '" + fName +"' ";
						}
						
						if (i==3) {
							if (condition != "")
								condition = condition + " and ";
							String userId = request.getParameter(dropDownNames[i]);
							condition = condition + columnNames[i] +" = '" + userId+"' ";
						}
						
						if (i==4) {
							if (condition != "")
								condition = condition + " and ";
							String action = request.getParameter(dropDownNames[i]);
							condition = condition +  columnNames[i] +" = '" + action+"' ";
						}
					}
				}
				return condition;
			}	
		    
		
		
		public String getCbuInfoAuditCondition(int k){
			String condition="";
			String[] dropDownNames = new String[7];
			dropDownNames [0] = "fromDt"+k;
			dropDownNames [1] = "toDt"+k;
			dropDownNames [2] = "fieldName"+k;
			dropDownNames [3] = "userId"+k;
			dropDownNames [4] = "actTaken"+k;
			dropDownNames [5] = "prevValue"+k;
			dropDownNames [6] = "newValue"+k;
			//dropDownNames [7] =  {"proStrDt1","proStrDt2","fieldName","userId","actTaken","prevValue","newValue"};
			String columnNames[]={"timestamp","timestamp","column_display_name","user_id","action","old_value","new_value"};
			for(int i=0;i<dropDownNames.length;i++){
				if(request.getParameter(dropDownNames[i])!=null && !request.getParameter(dropDownNames[i]).equals("") && !request.getParameter(dropDownNames[i]).equals("undefined")){
					if(i==0){
						String val1=request.getParameter(dropDownNames[0]).toString();
						String val2=request.getParameter(dropDownNames[1]).toString();
						if(val1.equals(val2)){
							condition=condition+"  to_char("+columnNames[i]+",'DD-Mon-YYYY')='"+request.getParameter(dropDownNames[0])+"'";
						}else{
							condition=condition+"  (trunc("+columnNames[i]+") between to_date('"+request.getParameter(dropDownNames[0])+"','DD-Mon-YYYY') and to_date('"+request.getParameter(dropDownNames[1])+"','DD-Mon-YYYY'))";
						}
					}
					if(i==0){
						setFprocStrDt1(Utilities.getOrdDateFormatDate(request.getParameter(dropDownNames[0]).toString()));
						setFprocStrDt2(Utilities.getOrdDateFormatDate(request.getParameter(dropDownNames[1]).toString()));
					}
					
					if (i==2) {
						if (condition != "")
							condition = condition + " and ";
						String fName = request.getParameter(dropDownNames[i]);
						condition = condition + columnNames[i] +" = '" + fName +"' ";
					}
					
					if (i==3) {
						if (condition != "")
							condition = condition + " and ";
						String userId = request.getParameter(dropDownNames[i]);
						condition = condition + columnNames[i] +" = '" + userId+"' ";
					}
					
					if (i==4) {
						if (condition != "")
							condition = condition + " and ";
						String action = request.getParameter(dropDownNames[i]);
						condition = condition +  columnNames[i] +" = '" + action+"' ";
					}
					
					if (i==5) {
						if (condition != "")
							condition = condition + " and ";
						String prevVal = request.getParameter(dropDownNames[i]);
						condition = condition +  columnNames[i] +" = '" + prevVal+"' ";
					}

					if (i==6) {
						if (condition != "")
							condition = condition + " and ";
						String newVal = request.getParameter(dropDownNames[i]);
						condition = condition +  columnNames[i] +" = '" + newVal+"' ";
					}
				}
			}
			return condition;
		}
		
		public String revokedUnitReportsWidget() throws Exception{
			
			String cordID =null;
			String size="";
			String valStr="";
			String condition="";
			ServerGlobalSearch globalSearch=new ServerGlobalSearch();
			
			if(request.getParameter("cordID")!=null && !request.getParameter("cordID").equals("")){
				cordID = request.getParameter("cordID");
			}
			String widgetId = request.getParameter("widget");
			try{
				//List revokeLst = new CBUUnitReportController().getRevokeUnitReportLst(cordID);
				//setRevokedUnitReportList(revokeLst);
				if(widgetId.equals("cordaudit")){
					getCdrCbuPojo().setCordID(Long.parseLong(cordID));
					setCdrCbuPojo(new CBUController().getCordInfoById(getCdrCbuPojo()));
					paginationReqScopeData(paginateSearch);
					
					if(request.getParameter("gsearchTxt")!=null && !request.getParameter("gsearchTxt").equals("")){
						valStr=request.getParameter("gsearchTxt").toString();
						condition=globalSearch.fetchGlobalSearch_cbuInfo(valStr);
					}
					if (condition != "") {
						condition = " where "+ condition; 
						if (getCbuInfoAuditCondition(1) != "")
							condition = condition + " and "+ getCbuInfoAuditCondition(1);
					}
					else
					{
						if (getCbuInfoAuditCondition(1) != "")
							condition = " where " + getCbuInfoAuditCondition(1);
					}
					
					setAuditTrailAllData(new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.CORD_ROW_LEVEL,null,1,"",""));
					List tempLst=new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.CORD_ROW_LEVEL,paginateSearch,0,condition,"");
					if(tempLst!=null){
						size=tempLst.get(0).toString();
					}
					int iTotalRows = Integer.parseInt(size);
					paginateSearch.setiTotalRows(iTotalRows);
					setPaginateSearch(VelosUtil.getListTotalCount(paginateSearch));
					request.setAttribute("paginateSearch", paginateSearch);
				} else if(widgetId.equals("entitystat")){
					paginationReqScopeData(paginationSearch1);
					if (condition != "") {
						condition = " where "+ condition; 
						if (getCbuInfoAuditCondition(4) != "")
							condition = condition + " and "+ getCbuInfoAuditCondition(4);
					}
					else
					{
						if (getCbuInfoAuditCondition(4) != "")
							condition = " where " + getCbuInfoAuditCondition(4);
					}
					
					setAuditTrailEntityStatusRowData(new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.ENTITY_STATUS_ROW_LEVEL,null,1,"",""));
					List tempLst = new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.ENTITY_STATUS_ROW_LEVEL,paginationSearch1,0,condition,"");
					if(tempLst!=null){
						size=tempLst.get(0).toString();
					}
					int iTotalRows1=Integer.parseInt(size);
					paginationSearch1.setiTotalRows(iTotalRows1);
					setPaginationSearch1(VelosUtil.getListTotalCount(paginationSearch1));
					request.setAttribute("paginateSearch1", paginationSearch1);
				} else if(widgetId.equals("statrsn")){
					paginationReqScopeData(paginationSearch2);
					setAuditTrailEntityStatusReasonRowData(new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.ENTITY_STATUS_REASON_ROW_LEVEL,paginationSearch2,1,"",""));
					int iTotalRows2 = new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.ENTITY_STATUS_REASON_ROW_LEVEL,paginationSearch2,0,"","").size();
					paginationSearch2.setiTotalRows(iTotalRows2);
					setPaginationSearch2(VelosUtil.getListTotalCount(paginationSearch2));
					request.setAttribute("paginateSearch2", paginationSearch2);
				} else if(widgetId.equals("multval")){	
					paginationReqScopeData(paginationSearch3);
					if (condition != "") {
						condition = " where "+ condition; 
						if (getCbuInfoAuditCondition(5) != "")
							condition = condition + " and "+ getCbuInfoAuditCondition(5);
					}
					else
					{
						if (getCbuInfoAuditCondition(5) != "")
							condition = " where " + getCbuInfoAuditCondition(5);
					}
					setAuditTrailMultipleValuesRowData(new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.MULTIPLE_VALUE_ROW_LEVEL,null,1,"",""));
					List tempLst = new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.MULTIPLE_VALUE_ROW_LEVEL,paginationSearch3,0,condition,"");
					if(tempLst!=null){
						size=tempLst.get(0).toString();
					}
					int iTotalRows3=Integer.parseInt(size);
					paginationSearch3.setiTotalRows(iTotalRows3);
					setPaginationSearch3(VelosUtil.getListTotalCount(paginationSearch3));
					request.setAttribute("paginateSearch3", paginationSearch3);
				} else if(widgetId.equals("docaudit")){	
					paginationReqScopeData(paginationSearch4);
					if (condition != "") {
						condition = " where "+ condition; 
						if (getDocInfoAuditCondition() != "")
							condition = condition + " and "+ getDocInfoAuditCondition();
					}
					else
					{
						if (getDocInfoAuditCondition() != "")
							condition = " where " + getDocInfoAuditCondition();
					}
					setRevokedUnitReportList(new CBUUnitReportController().getRevokeUnitReportLst(cordID,paginationSearch4,1,"",""));
					List tempLst = new CBUUnitReportController().getRevokeUnitReportLst(cordID,paginationSearch4,0,condition,"");
					 if(tempLst!=null) {
						size=tempLst.get(0).toString();
					} 
					int iTotalRows4 =Integer.parseInt(size);
					paginationSearch4.setiTotalRows(iTotalRows4);
					setPaginationSearch4(VelosUtil.getListTotalCount(paginationSearch4));
					request.setAttribute("paginateSearch4", paginationSearch4);
				} else if(widgetId.equals("review")){
					paginationReqScopeData(paginationSearch5);
					if (condition != "") {
						condition = " where "+ condition; 
						if (getCbuInfoAuditCondition(6) != "")
							condition = condition + " and "+ getCbuInfoAuditCondition(6);
					}
					else
					{
						if (getCbuInfoAuditCondition(6) != "")
							condition = " where " + getCbuInfoAuditCondition(6);
					}
					 setAuditTrailReviewRowData(new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.REVIEW_ROW_LEVEL,null,1,"",""));
					 List tempLst =new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.REVIEW_ROW_LEVEL,paginationSearch5,0,condition,"");
					 if(tempLst!=null){
							size=tempLst.get(0).toString();
						}
					 int iTotalRows5 =Integer.parseInt(size);
					paginationSearch5.setiTotalRows(iTotalRows5);
					setPaginationSearch5(VelosUtil.getListTotalCount(paginationSearch5));
					request.setAttribute("paginateSearch5", paginationSearch5);
				} else if(widgetId.equals("hla")){	
					paginationReqScopeData(paginationSearch6);
					if (condition != "") {
						condition = " where "+ condition; 
						if (getCbuInfoAuditCondition(7) != "")
							condition = condition + " and "+ getCbuInfoAuditCondition(7);
					}
					else
					{
						if (getCbuInfoAuditCondition(7) != "")
							condition = " where " + getCbuInfoAuditCondition(7);
					}
					setAuditTrailHlaRowData(new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.HLA_ROW_LEVEL,null,1,"",""));
					List tempLst = new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.HLA_ROW_LEVEL,paginationSearch6,0,condition,"");
					if(tempLst!=null){
						size=tempLst.get(0).toString();
					}
					int iTotalRows6 = Integer.parseInt(size);
					paginationSearch6.setiTotalRows(iTotalRows6);
					setPaginationSearch6(VelosUtil.getListTotalCount(paginationSearch6));
					request.setAttribute("paginateSearch6", paginationSearch6);
				}else if(widgetId.equals("pathla")){	
					paginationReqScopeData(paginationSearch16);
					if (condition != "") {
						condition = " where "+ condition; 
						if (getCbuInfoAuditCondition(12) != "")
							condition = condition + " and "+ getCbuInfoAuditCondition(12);
					}
					else
					{
						if (getCbuInfoAuditCondition(12) != "")
							condition = " where " + getCbuInfoAuditCondition(12); 
					}
					setAuditTrailPatHlaRowData(new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.PATHLA_ROW_LEVEL,null,1,"",""));
					List tempLst = new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.PATHLA_ROW_LEVEL,paginationSearch16,0,condition,"");
					if(tempLst!=null){
						size=tempLst.get(0).toString();
					}
					int iTotalRows16 = Integer.parseInt(size);
					paginationSearch16.setiTotalRows(iTotalRows16);
					setPaginationSearch16(VelosUtil.getListTotalCount(paginationSearch16));
					request.setAttribute("paginateSearch16", paginationSearch16);
				}
				else if(widgetId.equals("labsummary")){	
					paginationReqScopeData(paginationSearch7);
					if (condition != "") {
						condition = " where "+ condition; 
						if (getCbuInfoAuditCondition(8) != "")
							condition = condition + " and "+ getCbuInfoAuditCondition(8);
					}
					else
					{
						if (getCbuInfoAuditCondition(8) != "")
							condition = " where " + getCbuInfoAuditCondition(8);
					}
					setAuditTrailLabSummaryRowData(new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.LABSUMMARY_ROW_LEVEL,null,1,"",""));
					List tempLst = new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.LABSUMMARY_ROW_LEVEL,paginationSearch7,0,condition,"");
					if(tempLst!=null){
						size=tempLst.get(0).toString();
					}
					int iTotalRows7 =Integer.parseInt(size);
					paginationSearch7.setiTotalRows(iTotalRows7);
					setPaginationSearch7(VelosUtil.getListTotalCount(paginationSearch7));
					request.setAttribute("paginateSearch7", paginationSearch7);
				}else if(widgetId.equals("cbnotes")){	
					paginationReqScopeData(paginationSearch8);
					if (condition != "") {
						condition = " where "+ condition; 
						if (getCbuInfoAuditCondition(9) != "")
							condition = condition + " and "+ getCbuInfoAuditCondition(9);
					}
					else
					{
						if (getCbuInfoAuditCondition(9) != "")
							condition = " where " + getCbuInfoAuditCondition(9);
					}
					setAuditTrailNotesRowData(new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.NOTES_ROW_LEVEL,null,1,"",""));
					List tempLst = new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.NOTES_ROW_LEVEL,paginationSearch8,0,condition,"");
					if(tempLst!=null){
						size=tempLst.get(0).toString();
					}
					int iTotalRows8 =Integer.parseInt(size);
					paginationSearch8.setiTotalRows(iTotalRows8);
					setPaginationSearch8(VelosUtil.getListTotalCount(paginationSearch8));
					request.setAttribute("paginateSearch8", paginationSearch8);
				}else if(widgetId.equals("forms")){	
					paginationReqScopeData(paginationSearch9);
					if (condition != "") {
						condition = " where "+ condition; 
						if (getFormInfoAuditCondition(1) != "")
							condition = condition + " and "+ getFormInfoAuditCondition(1);
					}
					else
					{
						if (getFormInfoAuditCondition(1) != "")
							condition = " where " + getFormInfoAuditCondition(1);
					}
					setAuditTrailFormsRowData(new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.FORMS_ROW_LEVEL_IDM,null,1,"",""));
					List tempLst = new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.FORMS_ROW_LEVEL_IDM,paginationSearch9,0,condition,"");
					if(tempLst!=null){
						size=tempLst.get(0).toString();
					}
					int iTotalRows9 =Integer.parseInt(size);
					paginationSearch9.setiTotalRows(iTotalRows9);
					setPaginationSearch9(VelosUtil.getListTotalCount(paginationSearch9));
					request.setAttribute("paginateSearch9", paginationSearch9);
				} else if(widgetId.equals("besthla")){	
					paginationReqScopeData(paginationSearch10);
					setAuditTrailBestHlaRowData(new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.BEST_HLA_ROW_LEVEL,paginationSearch10,1,"",""));
					int iTotalRows10 = new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.BEST_HLA_ROW_LEVEL,paginationSearch10,0,"","").size();
					paginationSearch10.setiTotalRows(iTotalRows10);
					setPaginationSearch10(VelosUtil.getListTotalCount(paginationSearch10));
					request.setAttribute("paginateSearch10", paginationSearch10);
				}else if(widgetId.equals("pf")){	
					paginationReqScopeData(paginationSearch11);
					if (condition != "") {
						condition = " where "+ condition; 
						if (getCbuInfoAuditCondition(11) != "")
							condition = condition + " and "+ getCbuInfoAuditCondition(11);
					}
					else
					{
						if (getCbuInfoAuditCondition(11) != "")
							condition = " where " + getCbuInfoAuditCondition(11);
					}
					setAuditTrailPfRowData(new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.PF_ROW_LEVEL,null,1,"",""));
					List tempLst =  new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.PF_ROW_LEVEL,paginationSearch11,0,condition,"");
					if(tempLst!=null){
						size=tempLst.get(0).toString();
					}
					int iTotalRows11 = Integer.parseInt(size);
					paginationSearch11.setiTotalRows(iTotalRows11);
					
					if(request.getParameter("iShowRows")==null&& request.getParameter("flag")==null){
						      
						paginationSearch11.setiEndResultNo(iTotalRows11); 
						setPaginationSearch11(VelosUtil.getListTotalCount1(paginationSearch11));
						
					}
					else{
					
					setPaginationSearch11(VelosUtil.getListTotalCount(paginationSearch11));
					
					}
					
					request.setAttribute("paginateSearch11", paginationSearch11);
				}else if(widgetId.equals("formshhs")){	
					paginationReqScopeData(paginationSearch12);
					if (condition != "") {
						condition = " where "+ condition; 
						if (getFormInfoAuditCondition(2) != "")
							condition = condition + " and "+ getFormInfoAuditCondition(2);
					}
					else
					{
						if (getFormInfoAuditCondition(2) != "")
							condition = " where " + getFormInfoAuditCondition(2);
					}
					setAuditTrailFormshssRowData(new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.FORMS_ROW_LEVEL_HHS,null,1,"",""));
					List tempLst =  new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.FORMS_ROW_LEVEL_HHS,paginationSearch12,0,condition,"");
					if(tempLst!=null){
						size=tempLst.get(0).toString();
					}
					int iTotalRows12 =Integer.parseInt(size);
					paginationSearch12.setiTotalRows(iTotalRows12);
					setPaginationSearch12(VelosUtil.getListTotalCount(paginationSearch12));
					request.setAttribute("paginateSearch12", paginationSearch12);
				}else if(widgetId.equals("id")){	
					paginationReqScopeData(paginationSearch13);
					
					if(request.getParameter("gsearchTxt")!=null && !request.getParameter("gsearchTxt").equals("")){
						valStr=request.getParameter("gsearchTxt").toString();
						condition=globalSearch.fetchGlobalSearch_cbuInfo(valStr);
					}
					if (condition != "") {
						condition = " where "+ condition; 
						if (getCbuInfoAuditCondition(2) != "")
							condition = condition + " and "+ getCbuInfoAuditCondition(2);
					}
					else
					{
						if (getCbuInfoAuditCondition(2) != "")
							condition = " where " + getCbuInfoAuditCondition(2);
					}
					setAuditTrailIdRowData(new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.ID_ROW_LEVEL,null,1,"",""));
					
					List tempLst = new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.ID_ROW_LEVEL,paginationSearch13,0,condition,"");
					if(tempLst!=null){
						size=tempLst.get(0).toString();
					}
					int iTotalRows13 = Integer.parseInt(size);
					paginationSearch13.setiTotalRows(iTotalRows13);
					setPaginationSearch13(VelosUtil.getListTotalCount(paginationSearch13));
					request.setAttribute("paginateSearch13", paginationSearch13);
				}else if(widgetId.equals("procInfo")){	
					
					if (condition != "") {
						condition = " where "+ condition; 
						if (getCbuInfoAuditCondition(3) != "")
							condition = condition + " and "+ getCbuInfoAuditCondition(3);
					}
					else
					{
						if (getCbuInfoAuditCondition(3) != "")
							condition = " where " + getCbuInfoAuditCondition(3);
					}
					
					paginationReqScopeData(paginationSearch14);
					setAuditTrailProcInfoRowData(new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.PROC_INFO_ROW_LEVEL,null,1,"",""));
					List tempLst =new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.PROC_INFO_ROW_LEVEL,paginationSearch14,0,condition,"");
					if(tempLst!=null){
						size=tempLst.get(0).toString();
					}
					int iTotalRows14 = Integer.parseInt(size);
					paginationSearch14.setiTotalRows(iTotalRows14);
					setPaginationSearch14(VelosUtil.getListTotalCount(paginationSearch14));
					request.setAttribute("paginateSearch14", paginationSearch14);
				}
				else if(widgetId.equals("mc")){	
					paginationReqScopeData(paginationSearch15);
					if (condition != "") {
						condition = " where "+ condition; 
						if (getCbuInfoAuditCondition(10) != "")
							condition = condition + " and "+ getCbuInfoAuditCondition(10);
					}
					else
					{
						if (getCbuInfoAuditCondition(10) != "")
							condition = " where " + getCbuInfoAuditCondition(10);
					}
					setAuditTrailMinCritInfoRowData(new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.MIN_CRIT_INFO_ROW_LEVEL,null,1,"",""));
					List tempLst = new AuditTrailController().getAuditRowDataByEntityId(getEntityId(),VelosGarudaConstants.MIN_CRIT_INFO_ROW_LEVEL,paginationSearch15,0,condition,"");
					if(tempLst!=null){
						size=tempLst.get(0).toString();
					}
					int iTotalRows15 = Integer.parseInt(size);
					paginationSearch15.setiTotalRows(iTotalRows15);
					setPaginationSearch15(VelosUtil.getListTotalCount(paginationSearch15));
					request.setAttribute("paginateSearch15", paginationSearch15);
				}
			}
			catch(Exception e){
				log.error(e.getMessage());
				e.printStackTrace();
			}
			
			return SUCCESS;
		}
		
		
		
		public String uploadedDocuments(){
			String cordID=request.getParameter("cordID");
			try{
				List uploadedLst1=new ArrayList();
				List uploadedLst = new CBUUnitReportController().getUploadedDocumentLst(cordID);
				List<CodeList> docRightList = setCategoriesWithGroupRightsForViewDoument();
				if(uploadedLst!=null){
					for(Object a:uploadedLst){
						Object[] objArr = (Object[])a;
						for(CodeList c:docRightList){
							if(objArr[4]==c.getPkCodeId()||objArr[4].equals(c.getPkCodeId())){
								uploadedLst1.add(a);
							}
						}
						
					}
				}
				setUploadedDocumentLst(uploadedLst1);
				getCdrCbuPojo().setCordID(Long.parseLong(cordID));
				setCdrCbuPojo(new CBUController().getCordInfoById(getCdrCbuPojo()));
			}
			catch(Exception e){
				log.error(e.getMessage());
				e.printStackTrace();
			}			
			return SUCCESS;
		}


		public String cdrCbuUnitReport() throws Exception{
			
			codelstParam = new HashMap<String, String>();
			codelstParam1 = new HashMap<String, String>();
			codelstParam2 = new HashMap<String, String>();
			searchPojo = new SearchPojo();
			searchPojo1 =new SearchPojo();
			searchPojo2 =new SearchPojo();
			searchPojo.setCodelstParam(codelstParam);
			searchPojo1.setCodelstParam(codelstParam1);
			searchPojo2.setCodelstParam(codelstParam2);
			String cordID = request.getParameter("cordID");
			String pkCordExtInfo=request.getParameter("pkCordExtInfo");
			searchPojo.setSearchType(CDRCBU_OBJECTNAME);
			searchPojo1.setSearchType(CBUUNITREPORT_OBJECT);
			searchPojo2.setSearchType(CORDHLAEXT_OBJECT);
							
			
			/*String cbbname = request.getParameter("cbbname");
			String elegdeter = request.getParameter("elegdeter");
			String status = request.getParameter("status");*/
			try {
				searchPojo.setCriteria(" cordID = '"+cordID+"'");
				searchPojo = getVelosSearchController().velosSearch(searchPojo);
				
			setLstObjectOne(searchPojo.getResultlst());
			
			List list = getLstObjectOne();
			
			
			Iterator<CdrCbu> it = list.iterator();
			
			 
			while (it.hasNext()) {
				cdrCbu = it.next();
			}

			Object object = ObjectTransfer.transferObjects(cdrCbu, getCdrCbuPojo());
			//log.debug("object-->"+object);
			cdrCbuPojo = (CdrCbuPojo) object;
			//log.debug("CdrCbuPojo-->"+cdrCbuPojo);
			setCdrCbuPojo(cdrCbuPojo);
				
			
			//setLstObject(getListCdrCBB(cdrCbuId));
			
			} catch (Exception e) {
				log.error(e.getMessage());
				e.printStackTrace();
				//log.debug("Exception for -------->"+e);
			}
			
						
			try {
				searchPojo1.setCriteria("pkCordExtInfo ="+pkCordExtInfo);
				searchPojo1= getVelosSearchController().velosSearch(searchPojo1);
			
			setLstObjecttwo(searchPojo1.getResultlst());
		
			List list1 = getLstObjecttwo();
			
		
			Iterator<CBUUnitReport> it1 = list1.iterator();
			 
			

			while (it1.hasNext()) {
				cbuUnitReport = it1.next();
			}

			Object object1 = ObjectTransfer.transferObjects(cbuUnitReport, getCbuUnitReportPojo());
			//log.debug("object-->"+object);
			cbuUnitReportPojo = (CBUUnitReportPojo) object1;
			//log.debug("CdrCbuPojo-->"+cdrCbuPojo);
		
			//log.debug(CBUUnitReportPojo.getCordCompletedBy());
			
			//setLstObject(getListCdrCBB(cdrCbuId));
			
			} catch (Exception e) {
				log.error(e.getMessage());
				e.printStackTrace();
				//log.debug("Exception for -------->"+e);
			}
			
			try {
				searchPojo2.setCriteria("fkCordExtInfo ="+pkCordExtInfo);
				searchPojo2= getVelosSearchController().velosSearch(searchPojo2);
			
			setLstObjecthla(searchPojo2.getResultlst());
		
			List list2 = getLstObjecthla();
			
		
			Iterator<CordHlaExt> it2 = list2.iterator();
			 
			

			while (it2.hasNext()) {
				CordHlaExt = it2.next();
			}

			Object object2 = ObjectTransfer.transferObjects(CordHlaExt, getCordHlaExtPojo());
			//log.debug("object-->"+object);
			cordHlaExtPojo = (CordHlaExtPojo) object2;
			//log.debug("CdrCbuPojo-->"+cdrCbuPojo);
			
			
			
			//setLstObject(getListCdrCBB(cdrCbuId));
			
			} catch (Exception e) {
				//log.debug("Exception for -------->"+e);
				e.printStackTrace();
				log.error(e.getMessage());
			}
			//String id = getCodeListDescById(CBUUnitReportPojo.getCordCfuPostMethod());
			
			
			
			return "success";
		}
}