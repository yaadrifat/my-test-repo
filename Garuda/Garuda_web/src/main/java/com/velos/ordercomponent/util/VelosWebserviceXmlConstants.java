package com.velos.ordercomponent.util;

public interface VelosWebserviceXmlConstants {

 //for COrdInfo Xml Constants
	
	public static final String VELOS_CORDINFO_XML_ROOT = "CBUDATA";
	public static final String VELOS_CORDINFO_ROOT = "CORDINFO";
	public static final String VELOS_HLAINFO_ROOT = "HLAINFO";
	public static final String VELOS_HLA_ROOT = "HLA";
	public static final String VELOS_CORDINFO_CBUID = "CBUID";
	public static final String VELOS_CORDINFO_EXTERNALCBUID = "EXTERNALCBUID";
	public static final String VELOS_CORDINFO_TNC_FROZEN = "TNC-FROZEN";
	public static final String VELOS_CORDINFO_CD3CELLCOUNT = "CD3CELLCOUNT";
	public static final String VELOS_CORDINFO_TNC_FROZEN_PATIENT_WEIGHT = "TNC-FROZEN-PATIENT-WEIGHT>";
	public static final String VELOS_CORDINFO_BABYBIRTHDATE = "BABYBIRTHDATE";
	public static final String VELOS_CORDINFO_CBUCOLLECTIONDATE = "CBUCOLLECTIONDATE";
	public static final String VELOS_CORDINFO_REGISTRYID = "REGISTRYID";
	public static final String VELOS_CORDINFO_LOCALCBUID = "LOCALCBUID";	
	public static final String VELOS_CORDINFO_NUMBERONCBUBAG = "NUMBERONCBUBAG";
	public static final String VELOS_CORDINFO_CORDCREATIONDATE = "CORDCREATIONDATE";
	public static final String VELOS_CORDINFO_BACTERIALRESULT = "BACTERIALRESULT";
	public static final String VELOS_CORDINFO_FUNGALRESULT = "FUNGALRESULT";
	public static final String VELOS_CORDINFO_BLOODTYPE = "BLOODTYPE";
	public static final String VELOS_CORDINFO_RHTYPE = "RHTYPE";	
	public static final String VELOS_CORDINFO_BABYGENDER = "BABYGENDER";
	public static final String VELOS_CORDINFO_CLINICALSTATUS = "CLINICALSTATUS";
	public static final String VELOS_CORDINFO_LICENSESTATUS = "LICENSESTATUS";	
	public static final String VELOS_CORDINFO_ADDITIONALINFORECQUIRED = "ADDITIONALINFORECQUIRED";
	public static final String VELOS_CORDINFO_ISBIDINCODE = "ISBIDINCODE";
	public static final String VELOS_CORDINFO_ISITPRODUCTCODE = "ISITPRODUCTCODE";	
	public static final String VELOS_CORDINFO_ELIGIBLEADDITIONALINFO = "ELIGIBLEADDITIONALINFO";	
	public static final String VELOS_CORDINFO_CBBNAME = "CBBNAME";
	public static final String VELOS_CORDINFO_ELIGIBLESTATUS = "ELIGIBLESTATUS";
	public static final String VELOS_HLAINFO_HLA_HLACODE = "HLACODE";	
	public static final String VELOS_HLAINFO_HLA_METHOD = "METHOD";
	public static final String VELOS_HLAINFO_HLA_TYPE1 = "TYPE1";
	public static final String VELOS_HLAINFO_HLA_TYPE2 = "TYPE2";	
	
 ////////////////////////////
	public static final String LICENSE_REASON_L="LIC";
	public static final String LICENSE_REASON_U="UNL";
	public static final String XSD_UNLICENSED_REASON_INT = "INT";
	public static final String XSD_UNLICENSED_REASON_USPRE2005 = "USPRE2005";
	public static final String XSD_UNLICENSED_REASON_USIEDNRSCR = "USIEDNRSCR";
	public static final String XSD_UNLICENSED_REASON_USIEDNRTST = "USIEDNRTST";
	public static final String XSD_UNLICENSED_REASON_USINCL = "USINCL";
	public static final String XSD_UNLICENSED_REASON_ELIGPRELIC = "ELIGPRELIC";
	public static final String XSD_UNLICENSED_REASON_ELIGPSTLIC = "ELIGPSTLIC";
	public static final String XSD_UNLICENSED_REASON_NONNMDPIND = "NONNMDPIND";
	public static final String XSD_MALE = "M";
	public static final String XSD_FEMALE = "F";
	public static final String XSD_REACTIVE = "R";
	public static final String XSD_NONREACTIVE = "N";
	public static final String XSD_NOTDONE = "D";
	public static final String XSD_INDETERMINATE = "I";
	public static final String YES = "Y";
	public static final String NO = "N";
	public static final String XSD_NONE = "N";
	public static final String SOME = "S";
	public static final String COLLECTION_TYPE_IU = "IU";
	public static final String COLLECTION_TYPE_EU = "EU";
	public static final String COLLECTION_TYPE_B = "B";
	public static final String COLLECTION_TYPE_U = "U";
	public static final String DELIVERY_TYPE_C = "C";
	public static final String DELIVERY_TYPE_V = "V";
	public static final String DELIVERY_TYPE_U = "U";
	public static final String XSD_NEGATIVE = "N";
	public static final String XSD_NOT_DONE = "D";
	public static final String XSD_POSITIVE = "P";
	public static final String XSD_Y_POSITIVE = "Y";
	public static final String XSD_OTHER = "O";
	public static final String ELIGIBILITY_E ="E";
	public static final String ELIGIBILITY_I ="I";
	public static final String ELIGIBILITY_P ="P";
	public static final String ELIGIBILITY_NA ="NA";
	public static final String ELIGIBILITY_REASON_MRQ_DESC= "MRQ";
	public static final String ELIGIBILITY_REASON_IDM_DESC= "IDMTEST";
	public static final String ELIGIBILITY_REASON_OTHER_DESC= "OTHMEDREC";
	public static final String ELIGIBILITY_REASON_PHY_DESC= "PFA";
	public static final String CBU_STATUS_AG = "AG";
	public static final String CBU_STATUS_CD = "CD";
	public static final String CBU_STATUS_DD = "DD";
	public static final String CBU_STATUS_OT = "OT";
	public static final String CBU_STATUS_QR = "QR";
	public static final String CBU_STATUS_RO = "RO";
	public static final String CBU_STATUS_SO = "SO";
	public static final String CBU_STATUS_AV = "AV";
	public static final String ETHNICITY_HIS = "HIS";
	public static final String ETHNICITY_NHIS = "NHIS";
	public static final String ETHNICITY_NA = "NA";
	public static final String XSD_RACE_AFRICA_AME = "AAFA";
	public static final String XSD_RACE_AFRICA = "AFB";
	public static final String XSD_RACE_SOUTH_ASIAN = "AINDI";
	public static final String XSD_RACE_AMERICAN_INDIAN_SO_CENTRAL = "AISC";
	public static final String XSD_RACE_ALASKA = "ALANAM";
	public static final String XSD_RACE_AMERICAN_INDIAN = "AMIND";
	public static final String XSD_RACE_CARIBEAN_BLACK = "CARB";
	public static final String XSD_RACE_CARIBEAN_INDIAN = "CARIBI";	
	public static final String XSD_RACE_OTHER_WHITE = "CAU";
	public static final String XSD_RACE_DECLINES_NO_RACE_SEL = "DEC";
	public static final String XSD_RACE_EASTERN_EUROPE = "EEURO";	
	public static final String XSD_RACE_FILIPINO = "FILII";
	public static final String XSD_RACE_GUAMANIAN = "GUAMAN";
	public static final String XSD_RACE_HAWAIAN = "HAWAII";
	public static final String XSD_RACE_JAPANESE = "JAPI";	
	public static final String XSD_RACE_KOREAN = "KORI";
	public static final String XSD_RACE_MEDITERREAN = "MEDIT";	
	public static final String XSD_RACE_MIDDLE_EASTERN = "MIDEAS";
	public static final String XSD_RACE_NORTH_AMERICA = "NAMER";	
	public static final String XSD_RACE_NORTH_COAST_AFRICA = "NCAFRI";
	public static final String XSD_RACE_CHINESE = "NCHI";	
	public static final String XSD_RACE_NORTH_EUROPE = "NEURO";
	public static final String XSD_RACE_OTHER_PACIFIC_ISLAND = "OPI";	
	public static final String XSD_RACE_SAMOAN = "SAMOAN";
	public static final String XSD_RACE_BLACK_CENT_AMER = "SCAMB";	
	public static final String XSD_RACE_SOUTH_EAST_ASIAN = "SCSEAI";
	public static final String XSD_RACE_VIETNAMESE = "VIET";
	public static final String XSD_RACE_WHITE_CARRIBEAN = "WCARIB";
	public static final String XSD_RACE_WESTERN_EUROPE = "WEURO";
	public static final String XSD_RACE_WHITE_CENT_AME = "WSCA";
	public static final String XSD_RACE_UNSPECIFIED_BLACK  = "AFA";
	public static final String XSD_RACE_UNSPEC_ASAINPACIFIC_ISLAND  = "API";
	public static final String XSD_RACE_CARIBBEAN_HISPANIC = "CARHIS";
	public static final String XSD_RACE_NORTH_AMERICAN_OR_EUROPEAN = "EURWRC";
	public static final String XSD_RACE_HAWAIIAN_OR_PACIFIC_ISLANDER="HAWI";
	public static final String XSD_RACE_MIDEAST_NO_COAST_OF_AFRICA="MENAFC";
	public static final String XSD_RACE_MEXICAN_OR_CHICANO="MSWHIS";
	public static final String XSD_RACE_UNSPECIFIED_NATIVE_AMERICAN="NAM";
	public static final String XSD_RACE_SOUTH_CNTRL_AMER_HISP="SCAHIS";
	public static final String XSD_RACE_UNKNOWN_QUESTION_NOT_ASKED="UNK";
	public static final String XSD_RACE_UNSPECIFIED_HISPANIC="HIS";
	public static final String XSD_RACE_OTHER_RETIRED="OTH";
	public static final String XSD_ADDITIONAL_IDS_CORD = "C";
	public static final String XSD_ADDITIONAL_IDS_MOM = "M";
	///////////////////////////////////////////////////////////
	public static final String YES_DESC = "yes";
	public static final String NO_DESC = "no";
	public static final String NONE_DESC = "none";
	public static final String SOME_DESC ="some";
	public static final String MOM_NOT_ANS = "M";
	public static final String MOM_NOT_ANS_DESC = "momdntans";
	public static final String BANK_DIDNT_ASK = "B";
	public static final String BANK_DIDNT_ASK_DESC = "bankdntask";
	public static final String UNKNOWN ="U";
	public static final String UNKNOWN_DESC ="unknown";
	public static final String NOT_ASKED ="NT";
	public static final String NOT_ASKED_DESC ="notask";
	
	public static final String BABYMOM ="BM";
	public static final String BABYMOM_DESC ="babymom";
	public static final String BABYDAD ="BF";
	public static final String BABYDAD_DESC ="babydad";
	public static final String BABYSIB ="BS";
	public static final String BABYSIB_DESC ="babysib";
	public static final String BABYGP ="BGP";
	public static final String BABYGP_DESC ="babygpa";
	public static final String BABYMOMSIB ="BMS";
	public static final String BABYMOMSIB_DESC ="babymomsib";
	public static final String BABYDADSIB ="BFS";
	public static final String BABYDADSIB_DESC ="babydadsib";
	
	
	//cancer disease////////////////////
	public static final String FMHQ_CANCER = "cncr_type";
	public static final String FMHQ_ACLLEUK_CANCER = "aclleuk";
	public static final String ALL = "ALL";
	public static final String FMHQ_ACMLEUK_CANCER = "acmleuk";
	public static final String AML = "AML";
	public static final String FMHQ_BONE_CANCER = "brncncr";
	public static final String BONEJOI = "BONEJOI";
	public static final String FMHQ_BRNEV_CANCER = "brnevcncr";
	public static final String BRAONSC = "BRAONSC";
	public static final String FMHQ_HODGKIN_CANCER = "hodgcncr";
	public static final String HL = "HL";
	public static final String FMHQ_KDNY_CANCER = "kdycncr";
	public static final String KIDREN = "KIDREN";
	public static final String FMHQ_NON_HODGKIN_CANCER = "nonhodgcncr";
	public static final String NHL = "NHL";
	public static final String FMHQ_SKIN_CANCER = "skincncr";
	public static final String SKIN = "SKIN";
	public static final String FMHQ_THYROID_CANCER = "thycncr";
	public static final String THYR = "THYR";
	public static final String FMHQ_OTSKIN_CANCER = "otskincncr";
	public static final String OTHCL = "OTHCL";
	/////////////////////////////////////////////////////////
	
	/////////////RED BLOOD CELLS///////////////////////////
	public static final String FMHQ_RBC_DIABLSYN = "diablsyn";
	public static final String DBS = "DBS";
	public static final String FMHQ_RBC_ELLIPTO = "ellipto";
	public static final String ELL = "ELL";
	public static final String FMHQ_RBC_GPDRC = "gpdrc";
	public static final String G6PD = "G6PD";
	public static final String FMHQ_RBC_SPHERO = "sphero";
	public static final String SPH = "SPH";
	
	/////////////////////////////////////////////////////////
	
	/////////////WHITE BLOOD CELLS///////////////////////////
	public static final String FMHQ_RES_WBC_CHONICDIS = "chonicdis";
	public static final String CGD = "CGD";
	public static final String FMHQ_RES_WBC_KOSTSYN = "kostsyn";
	public static final String KOSTS = "KOSTS";
	public static final String FMHQ_RES_WBC_LAD = "lad";
	public static final String LAD = "LAD";
	public static final String FMHQ_RES_WBC_SCHDIASYN = "schdiasyn";
	public static final String SCHDIAS ="SCHDIAS";
	
	/////////////////////////////////////////////////////////////
	
	////////////////////immune_def/////////////////////////////
	
	public static final String FMHQ_RES_IMMUNE_ADAORP = "adaorp";
	public static final String ADAPNP = "ADAPNP";
	public static final String FMHQ_RES_IMMUNE_DIGSYN = "digsyn";
	public static final String DIGRE = "DIGRE";
	public static final String FMHQ_RES_IMMUNE_HYPOGLOB = "hypoglob";
	public static final String HYPOGLOB = "HYPOGLOB";
	public static final String FMHQ_RES_IMMUNE_NEZELHOFF = "nezelhoff";
	public static final String NEZEL = "NEZEL";
	public static final String FMHQ_RES_IMMUNE_SCID = "scid";
	public static final String SCID = "SCID";
	public static final String FMHQ_RES_IMMUNE_CIS = "cis";
	public static final String CID = "CID";
	public static final String FMHQ_RES_IMMUNE_HLEINFEL = "hleInfel";
	public static final String HLHFEL = "HLHFEL";
	public static final String FMHQ_RES_IMMUNE_WISALD = "wisAld";
	public static final String WAS = "WAS";
	public static final String FMHQ_RES_IMMUNE_CVID = "cViD";
	public static final String CVID = "CVID";
	
	////////////////////////////////////////////////////////////
	
	//////////////////////plate_dis/////////////////////////////
	public static final String FMHQ_RES_PLAT_AMEGA = "amega";
	public static final String AMTH = "AMTH";
	public static final String FMHQ_RES_PLAT_GLANZ = "glanz";
	public static final String GLTROM = "GLTROM";
	public static final String FMHQ_RES_PLAT_HEREDI = "heredi";
	public static final String HTHROM = "HTHROM";
	public static final String FMHQ_RES_PLAT_PSPDIS = "pspdis";
	public static final String PLTSPD = "PLTSPD";
	public static final String FMHQ_RES_PLAT_TAR = "tar";
	public static final String TAR = "TAR";
	public static final String FMHQ_RES_PLAT_ATATELAN = "ataTelan";
	public static final String AT = "AT";
	public static final String FMHQ_RES_PLAT_FANANE = "fanAne";
	public static final String FA = "FA";
	
	///////////////////////////////////////////////////////////////////
	
	///////////////////////////metab_dis////////////////////////
	
	public static final String FMHQ_RES_METAB_MPS1 = "mps1";
	public static final String HURLER = "HURLER";
	public static final String FMHQ_RES_METAB_MPS2 = "mps2";
	public static final String HSS = "HSS";
	public static final String FMHQ_RES_METAB_HUNSYN = "hunSYn";
	public static final String HUNTER = "HUNTER";
	public static final String FMHQ_RES_METAB_MPS3 = "mps3";
	public static final String SANFIL = "SANFIL";
	public static final String FMHQ_RES_METAB_MPS4 = "mps4";
	public static final String MSPIII = "MSPIII";
	public static final String FMHQ_RES_METAB_MPS6 = "mps6";
	public static final String MSPVI = "MSPVI";
	public static final String FMHQ_RES_METAB_MPS7 = "mps7";
	public static final String MSPVII = "MSPVII";
	public static final String FMHQ_RES_METAB_ICELL = "icell";
	public static final String ICELL = "ICELL";
	public static final String FMHQ_RES_METAB_GLOBOID = "globoid";
	public static final String GLOMLD = "GLOMLD";
	public static final String FMHQ_RES_METAB_MLD = "mld";
	public static final String METMLD = "METMLD";
	public static final String FMHQ_RES_METAB_ALD = "ald";
	public static final String ALD = "ALD";
	public static final String FMHQ_RES_METAB_SANDHOFF = "sandhoff";
	public static final String SAND = "SAND";
	public static final String FMHQ_RES_METAB_TAYSACHS = "taysachs";
	public static final String TAYSACH = "TAYSACH";
	public static final String FMHQ_RES_METAB_GAUCHER = "gaucher";
	public static final String GAUCHER = "GAUCHER";
	public static final String FMHQ_RES_METAB_NIEMANN = "niemann";
	public static final String NEIPICK = "NEIPICK";
	public static final String FMHQ_RES_METAB_PORPHY = "porphy";
	public static final String POR = "POR";
	public static final String FMHQ_RES_METAB_STORGDIS = "storgdis";
	public static final String OTHMSD = "OTHMSD";
		
	//////////////////////////////////////////////////////////////////////
	
	public static final String FMHQ_RES_SEVERE_CROHNDIS = "crohndis";
	public static final String CROHN = "CROHN";
	public static final String FMHQ_RES_SEVERE_LUPUS = "lupus";
	public static final String SLE = "SLE";
	public static final String FMHQ_RES_SEVERE_MS = "ms";
	public static final String MS = "MS";
	public static final String FMHQ_RES_SEVERE_RHEUMATOID = "rheumatoid";
	public static final String RA = "RA";
	///////////////////////////////////////////////////////////////////
	
	
	
	//////form version for both mrq and fmhq forms//////////////
	public static final String XSD_CBB_VAL_PROTOCOL_USED = "cbb_val_protocol_used";
	
	/////////////////////////////////mrq////////
	
	public static final String XSD_AT_RISK_FRM_CMPLT_DATE = "at_risk_frm_cmplt_date";
	public static final String XSD_DONATE_CB_PREV_IND = "donate_cb_prev_ind";
	public static final String XSD_REFUSED_BLD_DNR_IND="refused_bld_dnr_ind";
	public static final String XSD_REFUSED_BLD_DESC="refused_bld_desc";
	public static final String DB_REFUSED_BLD_DESC="refused_bld_dnr_ind_desc";
	public static final String XSD_TKN_MEDICATIONS_IND="tkn_medications_ind";
	public static final String XSD_BOVINE_INSULIN_SINCE_1980_IND="bovine_insulin_since_1980_ind";
	public static final String XSD_EVR_TKN_PI_GH_IND="evr_tkn_pit_gh_ind";
	public static final String XSD_RABIES_VACC_PST_YEAR_IND="rabies_vacc_pst_year_ind";
	public static final String XSD_SHOTS_VACC_PST_8WK_IND="shots_vacc_pst_8wk_ind";
	public static final String XSD_SHOTS_VACC_PST_8WK_DESC="shots_vacc_pst_8wk_desc";
	public static final String XSD_CNTC_SMALLPOX_VACCINE_12WK_IND="cntc_smallpox_vaccine_12wk_ind";
	public static final String XSD_PST_4MTH_ILLNESS_SYMPTOM_IND="pst_4mth_illness_symptom_ind";
	public static final String XSD_SYMPTOM_DESC="symptom_desc";
	public static final String XSD_CNCR_IND="cncr_ind";
	public static final String XSD_BLD_PRBLM_5YR_IND="bld_prblm_5yr_ind";
	public static final String XSD_WNV_PREG_DIAG_PST_TEST_IND="wnv_preg_diag_pos_test_ind";
	public static final String XSD_VIRAL_HEPATITIS_AGE_11_IND="viral_hepatitis_age_11_ind";
	public static final String XSD_PARA_CHAGAS_BABESIOSIS_LEISH_IND="para_chagas_babesiosis_leish_ind";
	public static final String XSD_CJD_DIAG_NEURO_UNK_IND="cjd_diag_neuro_unk_ind";
	public static final String XSD_BLD_RLT_DIAG_RSK_CJD="bld_rlt_diag_rsk_cjd";
	public static final String XSD_DURA_MATER_IND="dura_mater_ind";
	public static final String XSD_RECV_XENO_TX_MED_PROCEDURE_IND="recv_xeno_tx_med_procedure_ind";
	public static final String XSD_LIV_CNTC_XENO_TX_MED_PROC_IND="liv_cntc_xeno_tx_med_proc_ind";
	public static final String XSD_MALARIA_ANTIMALR_DRUG_3YR_IND="malaria_antimalr_drug_3yr_ind";
	public static final String XSD_OUTSIDE_US_OR_CANADA_3YR_IND="outside_us_or_canada_3yr_ind";
	public static final String XSD_OUTSIDE_CANADA_US_DESC="outside_canada_us_desc";
	public static final String XSD_BLOOD_TRANSF_B4COLL_12M_IND="blood_transf_b4coll_12m_ind";
	public static final String XSD_TX_STEMCELL_12M_OTH_SELF_IND="tx_stemcell_12m_oth_self_ind";
	public static final String XSD_TATTOO_EAR_SKIN_PIERCE_12M_IND="tattoo_ear_skin_pierce_12m_ind";
	public static final String XSD_TATTOO_SHARED_NONSTERILE_12M_IND="tattoo_shared_nonsterile_12m_ind";
	public static final String XSD_ACC_NS_STK_CUT_BLD_12M_IND="acc_ns_stk_cut_bld_12m_ind";
	public static final String XSD_HAD_TREAT_SYPH_12M_IND="had_treat_syph_12m_ind";
	public static final String XSD_GIVEN_MN_DR_FOR_SEX_IND="given_mn_dr_for_sex_ind";
	public static final String XSD_SEX_W_TK_MN_DR_PMT_SEX_12M_5YR_IND="sex_w_tk_mn_dr_pmt_sex_12m_5yr_ind";
	public static final String XSD_SEX_CNTC_LIV_HEP_B_C_IND="sex_cntc_liv_hep_b_c_ind";
	public static final String XSD_SEX_W_IV_DR_INF_PST_5YR_12M_IND="sex_w_iv_dr_in_pst_5yr_12m_ind";
	public static final String XSD_SEX_W_MAN_OTH_PST_5YR_12M_IND="sex_w_man_oth_pst_5yr_12m_ind";
	public static final String XSD_SEX_W_CLOT_FACT_BLD_12M_IND="sex_w_clot_fact_bld_12m_ind";
	public static final String XSD_SEX_W_HIV_AIDS_12M_IND="sex_w_hiv_aids_12m_ind";
	public static final String XSD_PRISON_JAIN_CONTIN_72HRS_IND="prison_jail_contin_72hrs_ind";
	public static final String XSD_PST_5YR_TK_MN_DR_SEX_IND="pst_5yr_tk_mn_dr_sex_ind";
	public static final String XSD_PST_5YR_IV_DRUG_IND="pst_5yr_iv_drug_ind";
	public static final String XSD_AIDS_HIV_SCREEN_TEST_IND="aids_hiv_screen_test_ind";
	
	public static final String XSD_UNEXPLAINED_SYMPTOM_IND="unexplained_symptom_ind";

	public static final String XSD_UNEXPLN_NIGHT_SWEAT="unexpln_night_sweat";
	public static final String XSD_UNEXPLN_BLUE_PRPL_SPOT_KAPOSI_IND="unexpln_blue_prpl_spot_kaposi_ind";
	public static final String XSD_UNEXPLN_WEIGHT_LOSS="unexpln_weight_loss";
	public static final String XSD_UNEXPLN_PERSIST_DIARRHEA="unexpln_persist_diarrhea";
	public static final String XSD_UNEXPLN_COUGH_SHORT_BREATH="unexpln_cough_short_breath";
	public static final String XSD_UNEXPLN_TEMP_OVER_TEN_DAYS="unexpln_temp_over_ten_days";
	public static final String XSD_UNEXPLN_MOUTH_SORES_WHT_SPT="unexpln_mouth_sores_wht_spt";
	public static final String XSD_UNEXPLN_1M_LUMP_NK_APIT_GRN_MULT_IND="unexpln_1m_lump_nk_apit_grn_mult_ind";
	public static final String XSD_INF_DUR_PREG_IND="inf_dur_preg_ind";
	public static final String XSD_HTLV_INCL_SCREEN_TEST_PARA_IND="htlv_incl_screen_test_para_ind";
	public static final String XSD_HIV_CONTAG_PERSON_UNDERSTAND_IND="hiv_contag_person_understand_ind";
	public static final String XSD_CJD_LIVE_TRAVEL_COUNTRY_IND="cjd_live_travel_country_ind";
	public static final String XSD_SPENT_UK_GE_3M_IND="spent_uk_ge_3m_ind";
	public static final String XSD_RECV_TRNSFSN_UK_FRANCE_IND="recv_trnsfsn_uk_france_ind";
	public static final String XSD_SPENT_EUROPE_GE_5Y_CJD_CNT_IND="spent_europe_ge_5y_cjd_cnt_ind";
	public static final String XSD_US_MILITARY_DEP_IND="us_military_dep_ind";
	public static final String XSD_MIL_BASE_EUROPE_1_GE_6M_IND="mil_base_europe_1_ge_6m_ind";
	public static final String XSD_MIL_BASE_EUROPE_2_GE_6M_IND="mil_base_europe_2_ge_6m_ind";
	public static final String XSD_HIV_1_2_O_PERFORMED_IND="hiv_1_2_o_performed_ind";
	public static final String XSD_BORN_LIVE_CAM_AFR_NIG_IND="born_live_cam_afr_nig_ind";
	public static final String XSD_TRAV_CTRY_REC_BLD_OR_MED_IND="trav_ctry_rec_bld_or_med_ind";
	public static final String XSD_SEX_W_BORN_LIVE_IN_CTRY_IND="sex_w_born_live_in_ctry_ind";
	
	
	///////////////////////////////fmhq////////////////////////////////////////
	
	public static final String XSD_HLTH_HIST_FRM_CMPLT_DATE = "hlth_hist_frm_cmplt_date";
	public static final String XSD_M_OR_F_ADOPTED_IND = "m_or_f_adopted_ind";
	public static final String XSD_M_OR_F_ADOPTED_MEDHIST_IND = "m_or_f_adopted_medhist_ind"; 
	public static final String XSD_BABY_MOTH_FATH_BLD_REL_IND = "baby_moth_fath_bld_rel_ind";
	public static final String XSD_PREG_USE_DNR_EGG_SPERM_IND = "preg_use_dnr_egg_sperm_ind";
	public static final String XSD_DNR_EGG_SPERM_FMHQ_AVAIL_IND ="dnr_egg_sperm_fmhq_avail_ind";
	public static final String XSD_ABN_PRENAT_TEST_RSLT_IND ="abn_prenat_test_rslt_ind";
	public static final String XSD_ABN_PRENAT_TEST_RSLT_TESTNAME ="abn_prenat_test_rslt_testname";
	public static final String XSD_ABN_PRENAT_TEST_ABORT_DESC ="abn_prenat_test_abort_desc";
	public static final String XSD_ABN_PRENAT_TEST_RSLT_DIAG_IND ="abn_prenat_test_rslt_diag_ind";
	public static final String XSD_ABN_PRENAT_TEST_RSLT_DIAG_DESC ="abn_prenat_test_rslt_diag_desc";
	public static final String XSD_CHILD_DIE_AGE_10_IND ="child_die_age_10_ind";
	public static final String XSD_CHILD_DIE_AGE_10_DESC ="child_die_age_10_desc";
	public static final String XSD_STILLBORN_IND ="stillborn_ind";
	public static final String XSD_STILLBORN_CAUSE ="stillborn_cause";
	public static final String XSD_ANSW_BOTH_MOTH_FATH_IND ="answ_both_moth_fath_ind";
	
	public static final String XSD_CAN_LEUK_IND ="can_leuk_ind";
	public static final String XSD_INHER_CL_OTHER_DISES_IND ="inher_cl_other_dises_ind";
	public static final String DB_CANCER_LEUK_REL_TYPE1 = "cancer_leuk_rel_type1";
	public static final String DB_CANCER_LEUK_REL_TYPE_DESC1 = "cancer_leuk_rel_type_desc1";
	public static final String DB_CANCER_LEUK_REL_TYPE2 = "cancer_leuk_rel_type2";
	public static final String DB_CANCER_LEUK_REL_TYPE_DESC2 = "cancer_leuk_rel_type_desc2";
	public static final String DB_CANCER_LEUK_REL_TYPE3 = "cancer_leuk_rel_type3";
	public static final String DB_CANCER_LEUK_REL_TYPE_DESC3 = "cancer_leuk_rel_type_desc3";
		
	
	public static final String XSD_INHER_RBC_DISES_IND ="inher_rbc_dises_ind";
	public static final String DB_RED_BLD_CELL_DES_REL = "red_bld_cell_dis_rel";
	public static final String DB_RED_BLD_CELL_DIS_TYPE1 = "red_bld_cell_dis_type1";
	public static final String DB_RED_BLD_CELL_DIS_TYPE2 = "red_bld_cell_dis_type2";
	public static final String DB_RED_BLD_CELL_DIS_TYPE3 = "red_bld_cell_dis_type3";
	public static final String DB_RED_BLD_CELL_DIS_TYPE4 = "red_bld_cell_dis_type4";
	public static final String DB_RED_BLD_CELL_DIS_TYPE5 = "red_bld_cell_dis_type5";
	public static final String DB_RED_BLD_CELL_DIS_TYPE6 = "red_bld_cell_dis_type6";
	
	public static final String XSD_INHER_WBC_DISES_IND ="inher_wbc_dises_ind";
	public static final String DB_WHITE_BLD_CELL_DIS_REL ="while_bld_cell_dis_rel";
	public static final String DB_WHITE_BLD_CELL_DIS_TYPE1 ="while_bld_cell_dis_type1";
	public static final String DB_WHITE_BLD_CELL_DIS_TYPE2 ="while_bld_cell_dis_type2";
	public static final String DB_WHITE_BLD_CELL_DIS_TYPE3 ="while_bld_cell_dis_type3";
	public static final String DB_WHITE_BLD_CELL_DIS_TYPE4 ="while_bld_cell_dis_type4";
	public static final String DB_WHITE_BLD_CELL_DIS_TYPE5 ="while_bld_cell_dis_type5";
	public static final String DB_WHITE_BLD_CELL_DIS_TYPE6 ="while_bld_cell_dis_type6";

	
	public static final String XSD_INHER_IMM_DFC_DISORDR_IND ="inher_imm_dfc_disordr_ind";
	public static final String DB_IMMUNE_DEF_IND_REL ="imune_def_ind_rel";
	public static final String DB_IMMUNE_DEF_IND_TYPE1 ="imune_def_ind_type1";
	public static final String DB_IMMUNE_DEF_IND_TYPE2 ="imune_def_ind_type2";
	public static final String DB_IMMUNE_DEF_IND_TYPE3 ="imune_def_ind_type3";
	public static final String DB_IMMUNE_DEF_IND_TYPE4 ="imune_def_ind_type4";
	public static final String DB_IMMUNE_DEF_IND_TYPE5 ="imune_def_ind_type5";
	public static final String DB_IMMUNE_DEF_IND_TYPE6 ="imune_def_ind_type6";

	
	public static final String XSD_INHER_PLTLT_DISES_IND ="inher_pltlt_dises_ind";
	public static final String DB_PLATELET_DIS_IND_REL ="platelet_dis_ind_rel";
	public static final String DB_PLATELET_DIS_IND_TYPE1 ="platelet_dis_ind_type1";
	public static final String DB_PLATELET_DIS_IND_TYPE2 ="platelet_dis_ind_type2";
	public static final String DB_PLATELET_DIS_IND_TYPE3 ="platelet_dis_ind_type3";
	public static final String DB_PLATELET_DIS_IND_TYPE4 ="platelet_dis_ind_type4";
	public static final String DB_PLATELET_DIS_IND_TYPE5 ="platelet_dis_ind_type5";
	public static final String DB_PLATELET_DIS_IND_TYPE6 ="platelet_dis_ind_type6";

	
	public static final String XSD_INHER_OTHER_BLD_DISES_IND ="inher_other_bld_dises_ind";
	public static final String DB_INHER_OTHER_BLD_DISES_REL ="inher_other_bld_dises_rel";
	public static final String DB_INHER_OTHER_BLD_DISES_DESC1 ="inher_other_bld_dises_desc1";
	public static final String DB_INHER_OTHER_BLD_DISES_DESC2 ="inher_other_bld_dises_desc2";
	public static final String DB_INHER_OTHER_BLD_DISES_DESC3 ="inher_other_bld_dises_desc3";
	public static final String DB_INHER_OTHER_BLD_DISES_DESC4 ="inher_other_bld_dises_desc4";
	public static final String DB_INHER_OTHER_BLD_DISES_DESC5 ="inher_other_bld_dises_desc5";
	public static final String DB_INHER_OTHER_BLD_DISES_DESC6 ="inher_other_bld_dises_desc6";

	
	public static final String XSD_INHER_THALASSEMIA_IND ="inher_thalassemia_ind";
	public static final String DB_INHER_THALASSEMIA_REL = "inher_thalassemia_rel";
	
	public static final String XSD_INHER_SICKLE_CELL_IND ="inher_sickle_cell_ind";
	public static final String DB_INHER_SICKLE_CELL_REL = "inher_sickle_cell_rel";
	
	public static final String XSD_INHER_MET_STG_DISES_IND="inher_met_stg_dises_ind";
	public static final String DB_INHER_MET_STG_DISES_REL ="inher_met_stg_dises_rel";
	public static final String DB_INHER_MET_STG_DISES_TYPE1 ="inher_met_stg_dises_type1";
	public static final String DB_INHER_MET_STG_DISES_DESC1 ="inher_met_stg_dises_desc1";
	public static final String DB_INHER_MET_STG_DISES_TYPE2 ="inher_met_stg_dises_type2";
	public static final String DB_INHER_MET_STG_DISES_DESC2 ="inher_met_stg_dises_desc2";
	public static final String DB_INHER_MET_STG_DISES_TYPE3 ="inher_met_stg_dises_type3";
	public static final String DB_INHER_MET_STG_DISES_DESC3 ="inher_met_stg_dises_desc3";
	public static final String DB_INHER_MET_STG_DISES_TYPE4 ="inher_met_stg_dises_type4";
	public static final String DB_INHER_MET_STG_DISES_DESC4 ="inher_met_stg_dises_desc4";
	public static final String DB_INHER_MET_STG_DISES_TYPE5 ="inher_met_stg_dises_type5";
	public static final String DB_INHER_MET_STG_DISES_DESC5 ="inher_met_stg_dises_desc5";
	public static final String DB_INHER_MET_STG_DISES_TYPE6 ="inher_met_stg_dises_type6";
	public static final String DB_INHER_MET_STG_DISES_DESC6 ="inher_met_stg_dises_desc6";
	
	
	
	public static final String XSD_HIV_AIDS_IND ="hiv_aids_ind";
	public static final String DB_HIV_AIDS_IND_REL = "hiv_aids_ind_rel";
	
	public static final String XSD_SEVERE_AUT_IMM_SYS_DISORDR_IND ="severe_aut_imm_sys_disordr_ind";
	public static final String DB_SEVERE_AUT_IMM_SYS_DISORDR_REL ="severe_aut_imm_sys_disordr_rel";                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
	public static final String DB_SEVERE_AUT_IMM_SYS_DISORDR_TYPE1 ="severe_aut_imm_sys_disordr_type1";                                                                                                                                                                                                                                                                                                                                                                                                                                                               
	public static final String DB_SEVERE_AUT_IMM_SYS_DISORDR_TYPE2 ="severe_aut_imm_sys_disordr_type2";                                                                                                                                                                                                                                                                                                                                                                                                                                                               
	public static final String DB_SEVERE_AUT_IMM_SYS_DISORDR_TYPE3 ="severe_aut_imm_sys_disordr_type3";
	
	
	public static final String XSD_INHER_OTH_UNK_IMM_SYS_DIS_IND ="inher_oth_unk_imm_sys_dis_ind";
	public static final String DB_OTH_IMMUNE_DIS = "oth_immune_dis";
	public static final String DB_OTH_IMMUNE_DIS_REL = "oth_immune_dis_rel";
	public static final String DB_OTH_IMMUNE_DIS_DESC1 = "oth_immune_dis_desc1";
	public static final String DB_OTH_IMMUNE_DIS_DESC2 = "oth_immune_dis_desc2";
	public static final String DB_OTH_IMMUNE_DIS_DESC3 = "oth_immune_dis_desc3";		

	public static final String XSD_CHRON_BLD_TRANF_IND ="chron_bld_tranf_ind";
	public static final String DB_CHRON_BLD_TRANS_IND_REL ="chron_bld_trans_ind_rel";
	
	public static final String XSD_HEMOLYTIC_ANEMIA_IND ="hemolytic_anemia_ind";
	public static final String DB_HEMOLYTIC_ANEMIA_REL ="hemolytic_anemia_rel";
	
	
	public static final String XSD_SPLEEN_REMOVED_IND ="spleen_removed_ind";
	public static final String DB_SPLEEN_REMOVED_REL ="spleen_removed_rel";
	
	
	public static final String XSD_GALLBLADDER_REMOVED_IND ="gallbladder_removed_ind";
	public static final String DB_GALLBLADDER_REMOVED_REL ="gallbladder_removed_rel";
	
	
	public static final String XSD_CRUETZ_JAKOB_DIS_IND ="cruetz_jakob_dis_ind";
	public static final String DB_CRUETZ_JAKOB_DIS_REL ="cruetz_jakob_dis_rel";
	
	public static final String XSD_INHER_OTHER_DISES_IND ="inher_other_dises_ind";
	public static final String DB_INHER_OTHER_DISES_REL ="inher_other_dises_rel";                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
	public static final String DB_INHER_OTHER_DISES_DESC1 ="inher_other_dises_desc1";                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
	public static final String DB_INHER_OTHER_DISES_DESC2 ="inher_other_dises_desc2";                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
	public static final String DB_INHER_OTHER_DISES_DESC3 ="inher_other_dises_desc3";                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
	public static final String DB_INHER_OTHER_DISES_DESC4 ="inher_other_dises_desc4";                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
	public static final String DB_INHER_OTHER_DISES_DESC5 ="inher_other_dises_desc5";                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
	public static final String DB_INHER_OTHER_DISES_DESC6 ="inher_other_dises_desc6";                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
	
	
	public static final String XSD_DISEASE_CODE = "Code";
	
	public static final String XSD_DISEASE = "Disease";
	public static final String XSD_DISEASES = "Diseases";
	public static final String XSD_DISEASE_DESC = "OtherDescription";
	public static final String XSD_CANCER_DISEASE_DESC = "OtherCancerLeukDescription";
	public static final String XSD_RELATION = "Relation";
	public static final String XSD_RELATIONS = "Relations";
	
	///////////////////////////mat IDM////////////////////////////////////
	
	public static final String XSD_BLD_COLL_DATE ="bld_coll_date";
	public static final String XSD_CMS_CERT_LAB_IND ="cms_cert_lab_ind";
	public static final String XSD_FDA_LIC_MFG_INST_IND ="fda_lic_mfg_inst_ind";
	public static final String XSD_HBSAG_REACT_IND ="hbsag_react_ind";
	public static final String XSD_HBSAG_DATE ="hbsag_date";
	public static final String XSD_HBSAG_CMS = "hbsag_cms";
	public static final String XSD_HBSAG_FDA = "hbsag_fda";
	
	public static final String XSD_ANTI_HBC_REACT_IND ="anti_hbc_react_ind";
	public static final String XSD_ANTI_HBC_DATE ="anti_hbc_date";
	public static final String XSD_ANTI_HBC_CMS = "anti_hbc_cms";
	public static final String XSD_ANTI_HBC_FDA = "anti_hbc_fda";
	
	public static final String XSD_ANTI_HCV_REACT_IND ="anti_hcv_react_ind";
	public static final String XSD_ANTI_HCV_DATE ="anti_hcv_date";
	public static final String XSD_ANTI_HCV_CMS = "anti_hcv_cms";
	public static final String XSD_ANTI_HCV_FDA = "anti_hcv_fda";
	
	public static final String XSD_ANTI_HIV_1_2_O_IND ="anti_hiv_1_2_o_ind";
	public static final String XSD_ANTI_HIV_1_2_O_DATE ="anti_hiv_1_2_o_date";
	public static final String XSD_ANTI_HIV_1_2_O_CMS = "anti_hiv_1_2_o_cms";
	public static final String XSD_ANTI_HIV_1_2_O_FDA = "anti_hiv_1_2_o_fda";
	
	public static final String XSD_ANTI_HIV_1_2_IND ="anti_hiv_1_2_ind";
	public static final String XSD_ANTI_HIV_1_2_DATE ="anti_hiv_1_2_date";
	public static final String XSD_ANTI_HIV_1_2_CMS = "anti_hiv_1_2_cms";
	public static final String XSD_ANTI_HIV_1_2_FDA = "anti_hiv_1_2_fda";
	
	public static final String XSD_ANTI_HTLV1_REACT_IND ="anti_htlv1_react_ind";
	public static final String XSD_ANTI_HTLV1_DATE ="anti_htlv1_date";
	public static final String XSD_ANTI_HTLV1_CMS = "anti_htlv1_cms";
	public static final String XSD_ANTI_HTLV1_FDA = "anti_htlv1_fda";
	
	public static final String XSD_CMV_TOTAL_IND ="cmv_total_ind";
	public static final String XSD_CMV_TOTAL_DATE ="cmv_total_date";
	public static final String XSD_CMV_TOTAL_CMS = "cmv_total_cms";
	public static final String XSD_CMV_TOTAL_FDA = "cmv_total_fda";
	
	public static final String XSD_NAT_HIV_1_HCV_HBV_MPX_IND ="nat_hiv_1_hcv_hbv_mpx_ind";
	public static final String XSD_NAT_HIV_1_HCV_HBV_MPX_DATE ="nat_hiv_1_hcv_hbv_mpx_date";
	public static final String XSD_NAT_HIV_1_HCV_HBV_MPX_CMS = "nat_hiv_1_hcv_hbv_mpx_cms";
	public static final String XSD_NAT_HIV_1_HCV_HBV_MPX_FDA = "nat_hiv_1_hcv_hbv_mpx_fda";
	
	public static final String XSD_NAT_HBV_REACT_IND ="nat_hbv_react_ind";
	public static final String XSD_NAT_HBV_REACT_DATE ="nat_hbv_react_date";
	public static final String XSD_NAT_HBV_REACT_CMS = "nat_hbv_react_cms";
	public static final String XSD_NAT_HBV_REACT_FDA = "nat_hbv_react_fda";
	
	public static final String XSD_NAT_HCV_REACT_IND ="nat_hcv_react_ind";
	public static final String XSD_NAT_HCV_REACT_DATE ="nat_hcv_react_date";
	public static final String XSD_NAT_HCV_REACT_CMS = "nat_hcv_react_cms";
	public static final String XSD_NAT_HCV_REACT_FDA = "nat_hcv_react_fda";
	
	public static final String XSD_NAT_HIV_REACT_IND ="nat_hiv_react_ind";
	public static final String XSD_NAT_HIV_REACT_DATE ="nat_hiv_react_date";
	public static final String XSD_NAT_HIV_REACT_CMS = "nat_hiv_react_cms";
	public static final String XSD_NAT_HIV_REACT_FDA = "nat_hiv_react_fda";
	
	public static final String XSD_HIV_ANTIGEN_RSLT ="hiv_antigen_rslt";
	public static final String XSD_HIV_ANTIGEN_DATE ="hiv_antigen_date";
	public static final String XSD_HIV_ANTIGEN_CMS = "hiv_antigen_cms";
	public static final String XSD_HIV_ANTIGEN_FDA ="hiv_antigen_fda";
	
	public static final String XSD_SYPHILIS_REACT_IND ="syphilis_react_ind";
	public static final String XSD_SYPHILIS_REACT_DATE ="syphilis_react_date";
	public static final String XSD_SYPHILIS_REACT_CMS = "syphilis_react_cms";
	public static final String XSD_SYPHILIS_REACT_FDA = "syphilis_react_fda";
	
	public static final String XSD_NAT_WEST_NILE_REACT_IND ="nat_west_nile_react_ind";
	public static final String XSD_NAT_WEST_NILE_REACT_DATE ="nat_west_nile_react_date";
	public static final String XSD_NAT_WEST_NILE_REACT_CMS = "nat_west_nile_react_cms";
	public static final String XSD_NAT_WEST_NILE_REACT_FDA = "nat_west_nile_react_fda";
	
	public static final String XSD_CHAGAS_REACT_IND ="chagas_react_ind";
	public static final String XSD_CHAGAS_REACT_DATE ="chagas_react_date";
	public static final String XSD_CHAGAS_REACT_CMS = "chagas_react_cms";
	public static final String XSD_CHAGAS_REACT_FDA = "chagas_react_fda";
	
	public static final String XSD_SYPHILIS_CT_SUP_REACT_IND ="syphilis_ct_sup_react_ind";
	public static final String XSD_SYPHILIS_CT_SUP_REACT_DATE ="syphilis_ct_sup_react_date";
	public static final String XSD_SYPHILIS_CT_SUP_REACT_CMS = "syphilis_ct_sup_react_cms";
	
	public static final String XSD_TREPONEMAL_CT_IND ="treponemal_ct_ind";
	public static final String XSD_OTHER_IDM_1_REACT_IND ="other_idm_1_react_ind";
	public static final String XSD_OTHER_IDM_1_REACT_TEST ="other_idm_1_react_test";
	public static final String XSD_OTHER_IDM_1_REACT_DATE ="other_idm_1_react_date";
	public static final String XSD_OTHER_IDM_2_REACT_IND ="other_idm_2_react_ind";
	public static final String XSD_OTHER_IDM_2_REACT_TEST ="other_idm_2_react_test";
	public static final String XSD_OTHER_IDM_2_REACT_DATE ="other_idm_2_react_date";
	public static final String XSD_OTHER_IDM_3_REACT_IND ="other_idm_3_react_ind";
	public static final String XSD_OTHER_IDM_3_REACT_TEST ="other_idm_3_react_test";
	public static final String XSD_OTHER_IDM_3_REACT_DATE ="other_idm_3_react_date";
	public static final String XSD_OTHER_IDM_4_REACT_IND ="other_idm_4_react_ind";
	public static final String XSD_OTHER_IDM_4_REACT_TEST ="other_idm_4_react_test";
	public static final String XSD_OTHER_IDM_4_REACT_DATE ="other_idm_4_react_date";
	public static final String XSD_OTHER_IDM_5_REACT_IND ="other_idm_5_react_ind";
	public static final String XSD_OTHER_IDM_5_REACT_TEST ="other_idm_5_react_test";
	public static final String XSD_OTHER_IDM_5_REACT_DATE ="other_idm_5_react_date";
	public static final String XSD_OTHER_IDM_6_REACT_IND ="other_idm_6_react_ind";
	public static final String XSD_OTHER_IDM_6_REACT_TEST ="other_idm_6_react_test";
	public static final String XSD_OTHER_IDM_6_REACT_DATE ="other_idm_6_react_date";
	
	/////////////////////////////////////////



	//Cord Import Start for Lab Summary
	
	
	//PRE PROCEESING TEST DECLARATION
	//CBU volume (without anticoagulant/additives)
	public static final String XSD_LABSUMMARY_CBUVOLUME ="cbu_vol_pre_prcsng";
	//Total CBU Nucleated Cell Count
	public static final String XSD_LABSUMMARY_TOTNUCL_CNT="nucl_cell_cnt_strt_prcsng";
	//CBU Nucleated Cell Count Concentration
	public static final String XSD_LABSUMMARY_CBUNUCL_CNCTRN ="nucl_cell_cnctrn_strt_prcsng";
	
  //POST PROCESSING TEST DECLARATION
	//CBU Nucleated Cell Count Concentration
	public static final String XSD_LABSUMMARY_PST_CBUNUCL_CNCTRN = "nucl_cell_cnctrn_strt_prcsng";
	
    //Final Product Volume
	public static final String XSD_LABSUMMARY_CBUVOL_FRZN ="cbu_vol_frzn";
	//Total CBU Nucleated Cell Count (unknown if uncorrected)==?
	public static final String XSD_LABSUMMARY_CBUNUCL_CELCNT_UNCORCT  ="nucl_cell_cnctrn_frzn";
	//Total CBU Nucleated Cell Count (uncorrected)
	public static final String XSD_LABSUMMARY_TOTCBU_NUCL_UNCORRECT ="nucl_cell_cnt_frzn_uncorrect"; 
	//Total CD34+ Cell Count
	public static final String XSD_LABSUMMARY_CD34FRZN ="cd34_cell_cnt_frzn";
	//% of CD34+  Viable
	public static final String XSD_LABSUMMARY_PERCD34_VIAB ="prcnt_cd34_viable";
	//% of CD34+ Cells in Total Monunuclear Cell Count  
	public static final String XSD_LABSUMMARY_PERCD34_MONOCEL="prcnt_cd34_frzn_mono_cell_cn";
	// % of Mononuclear Cells in Total Nucleated Cell Count 
	public static final String XSD_LABSUMMARY_PERMONO_CELNUCL  ="prcnt_mono_frzn_nucl_cell_cn";
	//Total CD3 Cell Count  
	public static final String XSD_LABSUMMARY_TOTCD3_CELCNT="cd3_cell_cnt_frzn";
	//% CD3 Cells in Total Mononuclear Cell Count  
	public static final String XSD_LABSUMMARY_PERCD3CEL_TOTMONO ="prcnt_cd3_frzn_mono_cell_cn";
	//CBU nRBC Absolute Number  
	public static final String XSD_LABSUMMARY_CBUN_ABSNUM ="post_prcsng_nucl_rbc_cnt";
	//CBU nRBC %															
	public static final String XSD_LABSUMMARY_CBUNRBC_PER ="pct_post_prcsng_nucl_rbc_cnt";
	//CFU Count
	public static final String XSD_LABSUMMARY_CFUCNT ="cfu_post_prcsng_cnt";
	//Viability										 	
	public static final String XSD_LABSUMMARY_VIAB ="post_prcsng_viability_cnt";
	
	//Mapping The Short Name With The Map
	public static final String XSD_LABSUMMARY_SHRT_CBUVOLUME ="CBU_VOL";
	//Total CBU Nucleated Cell Count
	public static final String XSD_LABSUMMARY_SHRT_TOTNUCL_CNT ="TCNCC";
	//CBU Nucleated Cell Count Concentration
	public static final String XSD_LABSUMMARY_SHRT_CBUNUCL_CNCTRN ="CBUNCCC";
	
	//POST PROCESSING TEST DECLARATION
	//CBU Nucleated Cell Count Concentration
	public static final String XSD_LABSUMMARY_PST_SHRT_CBUNUCL_CNCTRN ="CBUNCCC";
	
    //Final Product Volume
	public static final String XSD_LABSUMMARY_SHRT_CBUVOL_FRZN ="FNPV";
	//Total CBU Nucleated Cell Count (unknown if uncorrected)==?
	public static final String XSD_LABSUMMARY_SHRT_CBUNUCL_CELCNT_UNCORCT ="TCBUUN"; 
	//Total CBU Nucleated Cell Count (uncorrected)
	public static final String XSD_LABSUMMARY_SHRT_TOTCBU_NUCL_UNCORRECT ="UNCRCT";
	//Total CD34+ Cell Count
	public static final String XSD_LABSUMMARY_SHRT_CD34FRZN ="TCDAD";
	//% of CD34+  Viable
	public static final String XSD_LABSUMMARY_SHRT_PERCD34_VIAB ="PERCD";
	//% of CD34+ Cells in Total Monunuclear Cell Count  
	public static final String XSD_LABSUMMARY_SHRT_PERCD34_MONOCEL ="PERTMON";
	// % of Mononuclear Cells in Total Nucleated Cell Count 
	public static final String XSD_LABSUMMARY_SHRT_PERMONO_CELNUCL ="PERTNUC";
	//Total CD3 Cell Count  
	public static final String XSD_LABSUMMARY_SHRT_TOTCD3_CELCNT ="TOTCD";
	//% CD3 Cells in Total Mononuclear Cell Count  
	public static final String XSD_LABSUMMARY_SHRT_PERCD3CEL_TOTMONO ="PERCD3";
	//CBU nRBC Absolute Number  
	public static final String XSD_LABSUMMARY_SHRT_CBUN_ABSNUM ="CNRBC";
	//CBU nRBC %															
	public static final String XSD_LABSUMMARY_SHRT_CBUNRBC_PER ="PERNRBC";
	//CFU Count
	public static final String XSD_LABSUMMARY_SHRT_CFUCNT ="CFUCNT";
	//Viability										 	
	public static final String XSD_LABSUMMARY_SHRT_VIAB ="VIAB";
	
	
	//CFU Count dropdown
	public static final String XSD_LABSUMMARY_TOTAL ="T";
	public static final String XSD_LABSUMMARY_GM ="G";
	public static final String XSD_LABSUMMARY_OTHER ="O";
	
	public static final String XSD_LABSUMMARY_AADT ="7A";
	public static final String XSD_LABSUMMARY_EB ="EB";
	public static final String XSD_LABSUMMARY_TB ="TB";
	public static final String XSD_LABSUMMARY_AP ="AP";
	 
	public static final String XSD_LABSUMMARY_BTT ="BTT";
	public static final String XSD_LABSUMMARY_H ="H";
	public static final String XSD_LABSUMMARY_SBT="SBT";
	public static final String XSD_LABSUMMARY_HD="HD";
	public static final String XSD_LABSUMMARY_HT="HT";
	public static final String XSD_LABSUMMARY_NM="NM";
	public static final String XSD_LABSUMMARY_ASC="ASC";
	public static final String XSD_LABSUMMARY_ATT="ATT";
	public static final String XSD_LABSUMMARY_ASD="ASD";
	public static final String XSD_LABSUMMARY_U ="U";
	public static final String XSD_LABSUMMARY_D ="D";
	public static final String XSD_LABSUMMARY_MT ="MT";
	////Cord Import End for Lab Summary
	

	
	/////////////////////////////////
	public static final String MESSAGE_DATA_OVERRIDE = "Required fields cannot be overridden once the Cord is submitted for search.";
	public static final String MESSAGE_CBU_REGISTRY_ID = "CBU Registry ID <nmdp_cbuid>";
	public static final String MESSAGE_MATERNAL_REGISTRY_ID = "Maternal Registry ID <nmdp_matrnl_cbuid>";
	public static final String MESSAGE_COMMON_CORRUPT_DATA_CHECK_LENGTH = "ID length must be 9 characters";
	public static final String MESSAGE_COMMON_CORRUPT_DATA_CHECK_FORMAT = "ID must be in correct format.";
	public static final String MESSAGE_COMMON_CORRUPT_DATA_CHECK_SPECIAL_CHAR = "ID cannot have special characters.";
	public static final String MESSAGE_COMMON_CORRUPT_DATA_CHECKSUM = "Checksum failed.";
	public static final String MESSAGE_CBB = "Registry ID is not valid for this CBB.";
	public static final String MESSAGE_CBB_EXIST = "Either this CBB does not exist in the database or is not assigned to the current user.";
	public static final String MESSAGE_CORRUPT_DATA_CBU_REG_ID = "CBU Registry ID is missing. <nmdp_cbuid>";
	public static final String MESSAGE_CORRUPT_DATA_CBU_LOCAL_ID = "CBU Local ID is missing. <EXLCL>";
	public static final String MESSAGE_CORRUPT_DATA_CBU_ISBT_ID = "ISBT Donation Identification Number is missing. <ISBT>";
	public static final String MESSAGE_CORRUPT_DATA_MAT_REG_ID = "Maternal Registry ID is missing. <nmdp_matrnl_cbuid>";
	public static final String MESSAGE_CORRUPT_DATA_NOT_MATCH_CBU_MAT_REG_ID = "CBU Registry ID And Maternal Registry ID can not be equal <nmdp_matrnl_cbuid>";
	public static final String MESSAGE_CORRUPT_DATA_CBU_REG_ID_ASSIGN = "CBU Registry ID already exists. <nmdp_cbuid>";
	public static final String MESSAGE_CORRUPT_DATA_MAT_REG_ID_ASSIGN = "Maternal Registry ID already exists. <nmdp_matrnl_cbuid>";
	public static final String MESSAGE_CORRUPT_DATA_CBU_REG_ID_ASSIGN_AS_MATREG = "CBU Registry ID is already used as a Maternal Registry ID. <nmdp_cbuid>";
	public static final String MESSAGE_CORRUPT_DATA_MAT_REG_ID_ASSIGN_AS_CBUREG = "Maternal Registry ID is already used as a CBU Registry ID. <nmdp_matrnl_cbuid>";
	public static final String MESSAGE_CORRUPT_DATA_MAT_DEFER_REG_ID= "Maternal Registry ID is already saved with a Deferred Cord status. nmdp_matrnl_cbuid";
	public static final String MESSAGE_CORRUPT_DATA_MAT_LOCAL_REG_ID= "Maternal Local Id already exists for this Maternal Registry ID. <EXMCL>";
	public static final String MESSAGE_CORRUPT_DATA_MAT_LOCAL_ID= "Maternal Local ID already exists. <EXMCL>";
	public static final String MESSAGE_CORRUPT_DATA_MAT_LOCAL_CBB_ID= "Maternal Local ID is associated with a different Maternal Registry ID for this CBB. <EXMCL>";
	public static final String MESSAGE_CORRUPT_DATA_MAT_LOCAL_CBU_ID= "Maternal Local ID already exists as a Local CBU ID. for this CBB. <EXMCL>";
	public static final String MESSAGE_CORRUPT_DATA_MAT_LOCAL_CBU_ID_LENGTH= "Maternal Local ID can only contain 30 characters. <EXMCL>";
	public static final String MESSAGE_CORRUPT_DATA_NOT_MATCH_CBU_MATERNAL_LOCAL_ID = "Maternal Local ID And CBU Local ID cannot be equal. <EXMCL>";
	public static final String MESSAGE_CORRUPT_DATA_CBU_LOCAL_CBB_ID= "CBU Local ID already exists for this CBB. <EXLCL>";
	public static final String MESSAGE_CORRUPT_DATA_CBU_LOCAL_CBB_ID_LENGTH= "CBU Local ID can only contain 30 characters. <EXLCL>";
	public static final String MESSAGE_CORRUPT_DATA_CBU_LOCAL_MAT_ID= "CBU Local ID already exists as a Maternal Local ID for this CBB. <EXLCL>";
	public static final String MESSAGE_CORRUPT_DATA_ISBT_ID= "ISBT Donation Identification Number already exists. <ISBT>";
	public static final String MESSAGE_CORRUPT_DATA_ISBT_ID_VALID= "ISBT Donation Identification Number is not valid. <ISBT>";
	public static final String MESSAGE_CORRUPT_DATA_LICENSE_STATUS_AVAIL= "License Status is an invalid value. <licensure_status>";
	public static final String MESSAGE_CORRUPT_DATA_LICENSE_STATUS_OMMIT_REASON_AVAIl= "Licensure Status response UNL is required when Unlicensed Reason response exists <licensure_status>..";
	public static final String MESSAGE_CORRUPT_DATA_UNLICENSE_REASON_AVAIL= "Unlicensed Reason response required if Licensure Status response is 'Unlicensed'.<unlicensed_reason>";
	public static final String MESSAGE_CORRUPT_DATA_UNLICENSE_REASON_DUPLICATE= "UnLicensed reason should not be dupliate. <unlicensed_reason>";
	public static final String MESSAGE_CORRUPT_DATA_UNLICENSE_REASON_AVAIL_WITH_LICENSED= "Unlicensed Reason response not required if Licensure Status response is 'Licensed'. <unlicensed_reason>";
	public static final String MESSAGE_CORRUPT_DATA_CBB_PROCEDURE_AVAIL= "Processing Procedure is an invalid value.";
	public static final String MESSAGE_CORRUPT_DATA_CBB_PROCEDURE_AVAIL_NOT_AVAIL_COLL_DATE= "Collection Date is outside the processing procedure start and termination date <coll_dte>.";
	public static final String MESSAGE_CORRUPT_DATA_CBU_DELIV_TYPE_AVAIL= "CBU Delivery Type is an invalid value. <typeofdelivery>";
	public static final String MESSAGE_CORRUPT_DATA_CBU_COLL_TYPE_AVAIL= "CBU Collection Type is an invalid value. <typeofcollection>";
	public static final String MESSAGE_CORRUPT_DATA_MULTIPLE_BIRTH_AVAIL= "Multiple Birth Indicator is an invalid value. <multiplebirth>";
	public static final String MESSAGE_CORRUPT_DATA_BIRTHDATE_AVAIL= "Baby Birth Date is an invalid value. <b_dte>";
	public static final String MESSAGE_CORRUPT_DATA_BABY_GENDER_AVAIL= "Baby Gender is not available. <sex>";
	public static final String MESSAGE_CORRUPT_DATA_BABY_GENDER_IN_CORRECT= "Baby Gender is an invalid value. <sex>";
	public static final String MESSAGE_CORRUPT_DATA_ETHNICITY_AVAIL= "Ethnicity is not available <ethnicity>";
	public static final String MESSAGE_CORRUPT_DATA_ETHNICITY_INVALID= "Ethnicity is an invalid value <ethnicity>";
	public static final String MESSAGE_CORRUPT_DATA_RACE_AVAIL= "Race is missing. <Races>";
	public static final String MESSAGE_CORRUPT_DATA_INVALID_RACE= "Race is an invalid value. <Races>";
	public static final String MESSAGE_CORRUPT_DATA_DEPRECATED_RACE= "Race is a deprecated. <Races>";
	public static final String MESSAGE_CORRUPT_DATA_COLLECTION_DATE_AVAIL= "CBU Collection Date is an invalid value. <coll_dte>";
	public static final String MESSAGE_CORRUPT_DATA_PROC_START_DATE_AVAIL= "Processing Start Date is an invalid value. <prcsng_strt_dte>";
	public static final String MESSAGE_CORRUPT_DATA_FREEZE_DATE_AVAIL= "Freeze Date is an invalid value. <frz_dte>";
	public static final String MESSAGE_CORRUPT_DATA_BACTERIAL_AVAIL= "Bacterial Culture is an invalid value. <bact_cult_sts>";
	public static final String MESSAGE_CORRUPT_DATA_FUNGAL_AVAIL= "Fungal Culture value is an invalid value. <fung_cult_sts>";
	public static final String MESSAGE_CORRUPT_DATA_BACTERIAL_DATE_AVAIL= "Bacterial Culture Start Date is an invalid value. <bact_cult_start_date>";
	public static final String MESSAGE_CORRUPT_DATA_FUNGAL_DATE_AVAIL= "Fungal Culture Start Date is an invalid value. <fung_cult_start_date>";
	public static final String MESSAGE_CORRUPT_DATA_BLOODTYPE_AVAIL= "Blood Type is an invalid value. <abo_bld_typ>";
	public static final String MESSAGE_CORRUPT_DATA_HEMOGLOBINOPATHY_AVAIL= "Hemoglobinopathy Screening is an invalid value. <hemoglobin_screening>";
	public static final String MESSAGE_CORRUPT_DATA_HEMOGLOBINOPATHY_INVALID= "Hemoglobinopathy Screening value is invalid. <hemoglobin_screening>";
	public static final String MESSAGE_CORRUPT_DATA_RHTYPE_AVAIL= "RH Type is an invalid value. <rh_typ>";
	public static final String MESSAGE_CORRUPT_DATA_RHTYPE_INVALID= "RH Type value is invalid. <rh_typ>";
	public static final String MESSAGE_CORRUPT_DATA_RHTYPE_AVAIL_OTHER= "Please Specify response is required if RH Type response  is 'O'.<rh_specifyother>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_AVAIL= "CBU Sample(s) is(are) an invalid value(s). <CBUSamples>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_FILT_PAP_SAMP_AVAIL= "Number of Filter Paper Samples is an invalid value. <filter_paper_avail>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_NO_OF_RBC_PELLETES_AVAIL= "Number of RBC Pellets is an invalid value. <rbc_pellets_avail>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_NO_OF_EXTRACTED_DNA_AVAIL= "Number of Extracted DNA is an invalid value. <dna_aliquots_avail>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_NO_OF_SERUM_SAMPLES_AVAIL= "Number of Serum Samples is an invalid value. <cbu_serum_aliquots_avail>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_NO_OF_PLASMA_SAMPLES_AVAIL="Number of Plasma Samples is an invalid value. <plasma_aliquots_avail>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_NO_OF_NON_VIABLE_SAMPLES_AVAIL="Number of Non-Viable Cell Samples is an invalid value. <cell_aliquots_avail>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_NO_OF_VIABLE_SAMPLES_FINAL_PRODUCT_AVAIL="Number of Viable Samples Not Representative of the Final Product is an invalid value. <viable_sampes_not_rep_final_prod>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_NO_OF_SEGMENTS_AVAIL="Number of Segments is an invalid value. <segments_avail>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_NO_OF_OTH_REP_ALI_CON_FINAL_PRODUCT_AVAIL="Number of Other Representative Aliquots Stored Under Conditions Consistent with Those of the Final Product is an invalid value. <number_other_rep_aliquots_consist>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_NO_OF_OTH_REP_ALI_ALT_FINAL_PRODUCT_AVAIL="Number of Other Representative Aliquots Stored Under Alternate Conditions is an invalid value. <number_other_rep_aliquots_alt>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_NO_OF_MAT_SERUM_SAMPLES_AVAIL="Number of Maternal Serum Samples is an invalid value.  <matrnl_serum_aliquots_avail>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_NO_OF_MAT_PLASMA_SAMPLES_AVAIL="Number of Maternal Plasma Samples is an invalid value. <matrnl_plasma_aliquots_avail>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_NO_OF_MAT_EXT_DNA_SAMPLES_AVAIL="Number of Maternal Extracted DNA Samples is an invalid value. <matrnl_dna_aliquots_avail>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_NO_OF_MISC_MAT_SAMPLES_AVAIL="Number of Miscellaneous Maternal Samples is an invalid value. <matrnl_cell_aliquots_avail>";
	public static final String MESSAGE_CORRUPT_DATA_ELIGIBILITY_DETER_AVAIL= "Eligibility Determination is an invalid value. <eligibilityCode>";
	public static final String MESSAGE_CORRUPT_DATA_ELIGIBILITY_REASON_AVAIL_WITHOUT_STATUS= "If the Ineligible and Incomplete Reasonis provided in XML, then Eligibility Code must be provided.<eligibilityCode>";
	public static final String MESSAGE_CORRUPT_DATA_ELIGIBILITY_DETER_ELIG_OR_INC= "If the Ineligible Reason is provided then Eligibility Determination should be 'I' for Ineligible or 'P' for Incomplete. <eligibilityCode>";
	public static final String MESSAGE_CORRUPT_DATA_ELIGIBILITY_DETER_NOTAPP_PRIOR_DESC_AVAIL= "Not Applicable Prior Description is an invalid value. <eligibilityCode>";
	public static final String MESSAGE_CORRUPT_DATA_ELIGIBILITY_DETER_NOTAPP_PRIOR_DESC_LENGTH_CHECK= "Not Applicable Prior Description should not exceed 300 characters. <IneligibleandIncompleteReasons>";
	public static final String MESSAGE_CORRUPT_DATA_ELIGIBILITY_DETER_REASON_AVAIL= "Ineligible Reason is required when Eligibility response is 'P' or 'I'. <IneligibleandIncompleteReasons>";
	public static final String MESSAGE_CORRUPT_DATA_ELIGIBILITY_DETER_REASON_DUPLICATE= "Eligibility Reason should not be duplicate. <IneligibleandIncompleteReasons>";
	public static final String MESSAGE_CORRUPT_DATA_FORM_NOT_AVAILABLE = "Form data is not available. <Forms>";
	public static final String MESSAGE_CORRUPT_DATA_NOT_AVAILABLE= "Response is an invalid value";
	public static final String MESSAGE_CORRUPT_FORM_DATA_NOT_AVAILABLE= "Response not available for";
	public static final String MESSAGE_CORRUPT_FORM_DATA_MANDATORY_PLZ_PROVIDE = " is mandatory. Please provide a valid response."; 
	public static final String MESSAGE_CORRUPT_FORM_DATA_NOT_MATCH= "Response doesn't match with";
	public static final String MESSAGE_CORRUPT_FORM_DATA_NOT_CORRECT= "Response incorrect for";
	public static final String MESSAGE_CORRUPT_DATA_MATERNAL_REG_RANGE_MONTH = "New CBU Birth Date cannot be less than 5 months from previous CBU Birth Dates. <nmdp_matrnl_cbuid>";
	public static final String MESSAGE_CORRUPT_DATA_SHIPPED_CORD = "This cord has been already shipped. <nmdp_cbuid>";
	public static final String HISTORY_CORD_IMPORT_FAILS = "Cord Import Fails Due to Invalid XML Format";
	public static final String HISTORY_CORD_IMPORT_SUCCESS = "Cord Import Successfull";
	public static final String HISTORY_CORD_IMPORT_START = "Cord Import XML Start";
	public static final String HISTORY_CORD_IMPORTED = "Cord(s) Imported";
	public static final String HISTORY_CORD_PROCESSED = "Cord(s) Processed Out Of ";
	

	public static final String MESSAGE_CORRUPT_NOT_FUTUREDATE = "Form Completion Date should not be future date";
	public static final String MESSAGE_CORRUPT_IDM_NOT_FUTUREDATE = "Form Blood Collection Date should not be future date";
	public static final String MESSAGE_CORRUPT_DATENOT_FUTUREDATE = "Form Test Date should not be future date";
	public static final String MESSAGE_CORRUPT_TEST_DATENOT_FUTUREDATE = " Test Date should not be future date";
	public static final String MESSAGE_CORRUPT_DATENOT_BEFOREDATE = " Test Date should not be before blood collection date";
	public static final String MESSAGE_CORRUPT_ISNOT_REQ_WHEN = "is not required when";
	public static final String MESSAGE_CORRUPT_RESPONSE = "response";
	public static final String MESSAGE_CORRUPT_RESPONSE_BF = "Baby’s Father is not a valid choice";
	public static final String MESSAGE_CORRUPT_RESPONSE_BS = "Baby's Sibling is not a valid choice";
	public static final String MESSAGE_CORRUPT_RESPONSE_IS = "response is";
	public static final String MESSAGE_IN_CORRUPT_RESPONSE_IS = "response is incorrect";
	public static final String MESSAGE_CORRUPT_NOT_AVAILABLE = "not available";
	public static final String MESSAGE_CORRUPT_RESPONSE_FORTHIS_DIEASES="response for this dieases";
	public static final String MESSAGE_CORRUPT_RESPONSE_MAX_30="Response text maximum length is 30 characters";
	public static final String MESSAGE_CORRUPT_RESPONSE_MAX_100="Response text maximum length is 100 characters";
	public static final String MESSAGE_CORRUPT_FORM_NOT_REQ = "form is not required when the cord licensure status response is not provided";
	public static final String MESSAGE_CORRUPT_FORM_REQ_FOR_LICSTATUS = "Form is not required when the Licensure Status response is Licensed";
	public static final String MESSAGE_CORRUPT_FORM_DATE_REQ = "Form Completion Date is required";
	public static final String MESSAGE_CORRUPT_IDM_FORM_DATE_REQ = "Form Blood Collection Date is required";
	
	public static final String MESSAGE_CORRUPT_AND ="and";
	public static final String MESSAGE_CORRUPT_FORM_IS ="Form is";
	public static final String MESSAGE_CORRUPT_FORMS_ARE ="Forms are";
	public static final String MESSAGE_CORRUPT_BOTH_FORM_REJECTED = "rejected while import since Licensure status is not provided. Please provide the valid Licensure status.";

	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_PRE_CBU_RANGE_ERROR = "CBU Volume Test Result out of range from 40 to 500 <cbu_vol_pre_prcsng>";
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_PRE_MANDATORY_ERROR = "CBU Volume Test Result required <cbu_vol_pre_prcsng>";
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_PRE_TOTNUCL_CNT_RANGE_ERROR ="Total CBU Nucleated Cell Count out of range from 12 to 999 <nucl_cell_cnt_strt_prcsng>";
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_PRE_CBUNUCL_CNCTRN_RANGE_ERROR ="CBU Nucleated Cell Count Concentration out of range from 1000 to 50000 <nucl_cell_cnctrn_strt_prcsng>";
	
	//For the Post Processing List 
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_POST_CBUNUCL_CNCTRN_RANGE_ERROR= "CBU Nucleated Cell Count Concentration Test Result out of range from 5000 to 200000 <nucl_cell_cnctrn_frzn>";
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_POST_CBUNUCL_CNCTRN_MANDATORY_ERROR= "CBU Nucleated Cell Count Concentration Test Required <nucl_cell_cnctrn_frzn>";
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_POST_CBUVOL_FRZN_RANGE_ERROR= "Final Product Volume Test Result out of range from 10 to 600 <cbu_vol_frzn >";
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_POST_CBUNUCL_CELCNT_UNCORCT_RANGE_ERROR= "Total CBU Nucleated Cell Count(unknown if uncorrected) out of range from 12 to 999 <nucl_cell_cnctrn_frzn>";
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_POST_UNK_IF_UNCRCT_MANDATORY_ERROR="Total CBU Nucleated Cell Count(unknown if uncorrected) Test Result Required <nucl_cell_cnctrn_frzn>";
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_POST_TOTCBU_NUCL_UNCORRECT_RANGE_ERROR= "Total CBU Nucleated Cell Count (uncorrected) out of range from 12 to 999 <nucl_cell_cnt_frzn_uncorrect>";
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_POST_UNKOWN_MANDATORY_ERROR ="Total CBU Nucleated Cell Count(unknown) Test Result required  <nucl_cell_cnt_frzn_uncorrect>";
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_POST_CD34FRZN_RANGE_ERROR= "Total CD34+ Cell Count out of range from 0 to 999.9 <cd34_cell_cnt_frzn>";
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_POST_TOTCD34_ERROR ="Total CD34+ Cell Count Test Result required <cd34_cell_cnt_frzn>";
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_POST_PERCD34_VIAB_RANGE_ERROR= "% of CD34+  Viable out of range from 0.0 to 100 <prcnt_cd34_viable>";
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_POST_PERCD34_MONOCEL_RANGE_ERROR= "% of CD34+ Cells in Total Monunuclear Cell Count out of range from 0.0 to 10.0 <prcnt_cd34_frzn_mono_cell_cn>";
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_POST_PERMONO_CELNUCL_RANGE_ERROR= "% of Mononuclear Cells in Total Nucleated Cell Count out of range from 20.0 to 99.9 <prcnt_mono_frzn_nucl_cell_cn>";
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_POST_TOTCD3_CELCNT_RANGE_ERROR= "Total CD3 Cell Count out of range from 0 to 999.9 <cd3_cell_cnt_frzn>";
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_POST_PERCD3CEL_TOTMONO_RANGE_ERROR= "% CD3 Cells in Total Mononuclear Cell Count out of range from 0 to 99.9 <prcnt_cd3_frzn_mono_cell_cn>";
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_POST_CBUN_ABSNUM_RANGE_ERROR= "CBU nRBC Absolute Number out of range from 0 to 9999 <post_prcsng_nucl_rbc_cnt>";
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_POST_CBUN_ABSNUM_MANDATORY_ERROR= "CBU nRBC Absolute Number Test Result required <post_prcsng_nucl_rbc_cnt>";
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_POST_CBUNRBC_PER_RANGE_ERROR= "CBU nRBC % out of range from 0 to 100 <pct_post_prcsng_nucl_rbc_cnt>";
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_POST_CBUNRBC_PER_MANDATORY_ERROR ="CBU nRBC % Test Result required <pct_post_prcsng_nucl_rbc_cnt>";
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_POST_CFUCNT_RANGE_ERROR= "CFU Count Test Result out of range from 0 to 9999 <cfu_post_prcsng_cnt>";
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_POST_CFUCNT_MANDATORY_ERROR ="CFU Count Test Result required <cfu_post_prcsng_cnt>";
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_POST_CFUCNT_METHOD_MANDATORY_ERROR ="CFU Count Test Method required <cfu_post_prcsng_test>";
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_POST_LABSUMMARY_VIAB_RANGE_ERROR= "Viability Test Method required<post_prcsng_viability_test>";
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_POST_LABSUMMARY_VIAB_MANDATORY_ERROR_RANGE ="Viability Test Result out of range from 0 to 100 <post_prcsng_viability_cnt>";
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_POST_LABSUMMARY_VIAB_MANDATORY_ERROR ="Viability Test Result required <post_prcsng_viability_cnt>";
	public static final String MESSAGE_CORRUPT_DATA_LAB_SUMMARY_POST_LABSUMMARY_VIAB_METHOD_MANDATORY_ERROR ="Viability Test Method required <post_prcsng_viability_test>";
	public static final String MESSAGE_CORRUPT_DATA_NUMBER_ON_CBU_BAG = "Invalid Unique Product Identity On Bag <IdOnBag>";
	public static final String MESSAGE_CORRUPT_DATA_CBU_STORAGE_LOCATION_INVALID = "Invalid CBU Storage Location<cbu_location>";
	public static final String MESSAGE_CORRUPT_DATA_BABY_BIRTH_COMP_SPEC_COLL_DATE = "Baby Birth Date must be on or before Collection Date <b_dte>";
	public static final String MESSAGE_CORRUPT_DATA_BABY_BIRTH_COMP_PROC_DATE = "Baby Birth Date must be on or before Processing Start Date <b_dte>";
	public static final String MESSAGE_CORRUPT_DATA_BABY_BIRTH_COMP_FRZ_DATE = "Baby Birth Date must be on or before Freeze Date <b_dte>";
	public static final String MESSAGE_CORRUPT_DATA_PROC_START_COMP_SPEC_COLL_DATE = "Processing Start Date cannot be less than Collection Date <prcsng_strt_dte>";
	public static final String MESSAGE_CORRUPT_DATA_FRZ_COMP_SPEC_COLL_DATE = "Collection Date should be less than Freeze Date <coll_dte>";
	public static final String MESSAGE_CORRUPT_DATA_FRZ_COMP_PROC_DATE = "Processing Start Date should be less than Freeze Date  <prcsng_strt_dte>";
	public static final String MESSAGE_CORRUPT_DATA_FRZ_COMP_PROC_DATE_HOUR = "Processing Start Date and Freeze Date difference should be less than or equal to 48 hours <prcsng_strt_dte>";
	public static final String MESSAGE_CORRUPT_DATA_FUTURE_PROC_DATE = "Processing Start Date can not be future Date  <prcsng_strt_dte>";
	public static final String MESSAGE_CORRUPT_DATA_FRZ_COMP_PROC_TERMINATION_DATE = "Freeze Date should be within the Processing Procedure's Activation and Termination Date range <frz_dte>";
	
	public static final String MESSAGE_CORRUPT_DATA_PROCNG_DATE = "Processing start date should be within the processing procedure�s activation and term dates <prcsng_strt_dte>";
	
	public static final String MESSAGE_CORRUPT_DATA_FRZ_COMP_PROC_START_DATE = "Freeze Date should be on or after the Processing Start Date and within the Processing Procedure's Activation and Termination Date range <frz_dte>";
	public static final String MESSAGE_CORRUPT_DATA_FRZ_COMP_HOUR_SPEC_COLL_DATE = "Freeze Date and Collection Date difference should be less than or equal to 48 hours  <frz_dte>";
	public static final String MESSAGE_CORRUPT_DATA_BABY_BIRTH_FUTURE_DATE = "Baby Birth Date cannot be a future date <b_dte>";
	public static final String MESSAGE_CORRUPT_DATA_FUNGAL_FUTURE_DATE = "Fungal Culture Start Date cannot be a future date  <fung_cult_start_date>";
	public static final String MESSAGE_CORRUPT_DATA_COLLECTION_FUTURE_DATE = "Collection Date cannot be a future date  <coll_dte>";
	public static final String MESSAGE_CORRUPT_DATA_BACTERIAL_FUTURE_DATE = "Bacterial Culture Start Date cannot be a future date  <bact_cult_start_date>";
	public static final String MESSAGE_CORRUPT_DATA_PRE_PROCESSING_START_DATE = "Pre Processing Start Date cannot be a future date <PreProcessingStartDate>";
	public static final String MESSAGE_CORRUPT_DATA_POST_PROCESSING_START_DATE = "Post Processing Start Date cannot be a future date <PreCryoStartDate>";
	public static final String MESSAGE_CORRUPT_DATA_FUNGAL_FUTURE_DATE_CHILD ="Fungal Culture Start Date response is required if Fungal Culture response is 'N' or 'Y'<fung_cult_start_date>";
	public static final String MESSAGE_CORRUPT_DATA_BACTERIAL_FUTURE_DATE_CHILD ="Bacterial Culture Start Date response is required if Bacterial Culture response is 'N' or 'Y' <bact_cult_start_date>" ; 
	public static final String MESSAGE_CORRUPT_DATA_CFUCOUNT_AVAIL_OTHER= "Describe is only required if CFU Count Test Method  is 'Other'.<cfu_post_prcsng_oth_describe>";
	public static final String MESSAGE_CORRUPT_DATA_VIABILITY_AVAIL_OTHER= "Describe is only required if Viability Test Method  is 'Other'.<post_prcsng_viability_oth_describe>";
	public static final String MESSAGE_CORRUPT_DATA_NRBC_PERNRBCE_EXIST_TOGETHER = "NRBC and PERNRBC should not be exist together in the XML File";
	//Pagination Module Constants
	public static final String MODULE_CORD_IMPORT = "cordFileUpload";
	public static final String CORD_IMP_OBJECTNAME ="CordImportHistory";
	public static final String MODULE_NOT_AVAILABLE_FILES = "notAvailablefiles";
	public static final String MESSAGE_CORRUPT_DATA_ETHNICITY_AVAIL_INVAL ="Invalid Ethnicity .<ethnicity>";
	
	public static final String MESSAGE_CBU_REGISTRY_ID_DEFERED = "CBU Registry ID already permanently medically deferred <nmdp_cbuid>";
	public static final String MESSAGE_CORRUPT_ELIGIBILITY_AGAINST_LICENSE = "The selected Eligibility status will reset Licensure status of the CBU to Unlicensed <eligibilityCode>";
	public static final String MESSAGE_CORRUPT_DATA_ELIGIBILITY_DETER_AVAIL_REA_NTAVAIL ="Eligibility Code response is required when Reason Code response exists <eligibilityCode>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_FILT_PAP_SAMP_AVAIL_LENGTH ="Number of Filter Paper Samples value will be between 0 to 99. <filter_paper_avail>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_NO_OF_RBC_PELLETES_AVAIL_LENGTH = " Number of RBC Pellets value will be between 0 to 99. <rbc_pellets_avail>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_NO_OF_EXTRACTED_DNA_AVAIL_LENGTH = "Number of Extracted DNA value will be between 0 to 99. <dna_aliquots_avail>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_NO_OF_SERUM_SAMPLES_AVAIL_LENGTH = "Number of Serum Samples value will be between 0 to 99. <cbu_serum_aliquots_avail>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_NO_OF_PLASMA_SAMPLES_AVAIL_LENGTH = "Number of Plasma Samples value will be between 0 to 99. <plasma_aliquots_avail>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_NO_OF_NON_VIABLE_SAMPLES_AVAIL_LENGTH ="Number of Non-Viable Cell Samples value will be between 0 to 99. <cell_aliquots_avail>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_NO_OF_VIABLE_SAMPLES_FINAL_PRODUCT_AVAIL_LENGTH="Number of Viable Samples Not Representative of the Final Product value will be between 0 to 99. <viable_sampes_not_rep_final_prod>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_NO_OF_SEGMENTS_AVAIL_LENGTH="Number of Segments value will be between 0 to 99. <segments_avail>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_NO_OF_OTH_REP_ALI_CON_FINAL_PRODUCT_AVAIL_LENGTH="Number of Other Representative Aliquots Stored Under Conditions Consistent with Those of the Final Product value will be between 0 to 99. <number_other_rep_aliquots_consist>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_NO_OF_OTH_REP_ALI_ALT_FINAL_PRODUCT_AVAIL_LENGTH="Number of Other Representative Aliquots Stored Under Alternate Conditions value will be between 0 to 99. <number_other_rep_aliquots_alt>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_NO_OF_MAT_SERUM_SAMPLES_AVAIL_LENGTH="Number of Maternal Serum Samples value will be between 0 to 99.  <matrnl_serum_aliquots_avail>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_NO_OF_MAT_EXT_DNA_SAMPLES_AVAIL_LENGTH="Number of Maternal Extracted DNA Samples value will be between 0 to 99. <matrnl_dna_aliquots_avail>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_NO_OF_MISC_MAT_SAMPLES_AVAIL_LENGTH="Number of Miscellaneous Maternal Samples value will be between 0 to 99. <matrnl_cell_aliquots_avail>";
	public static final String MESSAGE_CORRUPT_DATA_CBUSAMPLES_NO_OF_MAT_PLASMA_SAMPLES_AVAIL_LENGTH = "Number of Maternal Plasma Samples value will be between 0 to 99. <matrnl_plasma_aliquots_avail>";
	public static final String MESSAGE_CORRUPT_DATA_UNLICENSE_REASON_AVAIL_WITH_UNLICENSED= "Unlicensed Reason response is required if Licensure Status response is 'UnLicensed'. <unlicensed_reason>";
	
	
	public static final String MESSAGE_CORRUPT_DATA_BACT_CULT_DATE_VALIDATE = "Bacterial Culture Date must be on or after Collection Date <bact_cult_sts>";
	public static final String MESSAGE_CORRUPT_DATA_FUNG_CULT_DATE_VALIDATE = "Fungal Culture Date must be on or after Collection Date <fung_cult_start_date>";
}