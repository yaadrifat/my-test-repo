package com.velos.ordercomponent.validator;

import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

/**
 * @author Mohiuddin Ali Ahmed
 * @author Anurag Upadhyay
 * @version 1.0
 * 
 */

public class CordEntryNonCustomFieldValidator extends FieldValidatorSupport {
	private boolean searchableCheck;
	
	private String fields="";
	
	public boolean isSearchableCheck() {
		return searchableCheck;
	}

	public void setSearchableCheck(boolean searchableCheck) {
		this.searchableCheck = searchableCheck;
	}
	public String getFields() {
		return fields;
	}

	public void setFields(String fields) {
		this.fields = fields;
	}

	@Override
	public void validate(Object object) throws ValidationException {
		// TODO Auto-generated method stub
		 String fieldName = getFieldName();
		 String []fields=this.fields.split(",");
		 String notesTxt;
		 long cordSearchable = (Long) getFieldValue("cordSearchable", object);

		switch(CordEntryNonFields.valueOf(fieldName.toUpperCase())){
		
		case REASONS:/* This is Long Arrays Fields*/
			 Long[]  fieldValue = (Long[]) getFieldValue(fieldName, object);
			 long fkCordCbuEligible= (Long) getFieldValue(fields[0], object);
			 int inEligiblePkid=(Integer) getFieldValue(fields[1], object);
			 int incompleteReasonId=(Integer) getFieldValue(fields[2], object);
			 if(searchableCheck && cordSearchable==1 && (fieldValue.length<=0) && (fkCordCbuEligible==inEligiblePkid || fkCordCbuEligible==incompleteReasonId) ){
				   addFieldError(fieldName, object);
			       return;
			}
			break;
			
		case HLAPERCENTAGE:
			 Short hlaPercentage = (Short) getFieldValue(fieldName, object);
			 if(hlaPercentage!=null && searchableCheck && cordSearchable==1 && hlaPercentage !=100 ){
				   addFieldError(fieldName, object);
			       return;
			}
			break;
			
		case CORDADDITIONALINFO:
			 String content= getFieldValue(fieldName, object).toString();
			 long fkCordCbuEligible1= (Long) getFieldValue(fields[0], object);
			 int notCollectedToPriorReasonId= (Integer) getFieldValue(fields[1], object);
			 
			 if(searchableCheck && cordSearchable==1 && (content==null || content.equals(""))&& fkCordCbuEligible1==notCollectedToPriorReasonId){
				   addFieldError(fieldName, object);
			       return;
			 }
			break;
		case BACTCULTDATESTR:
		case FUNGCULTDATESTR:
		case SPECRHTYPEOTHER:
			Object dt= getFieldValue(fieldName, object);
			dt=(dt instanceof String && ((String)dt).equals(""))?null:dt;
			long value1=Long.parseLong(fields[0]);
			long value2=(Long) getFieldValue(fields[1], object);
			if(searchableCheck && cordSearchable==1 && (dt==null)&& value1==value2){
				addFieldError(fieldName, object);
			       return;
			    }
			break;
		//case FLAGFORLATER:
		case NOTEASSESSMENT:
			  String value=(String) getFieldValue(fieldName, object);
			  notesTxt=(String) getFieldValue(fields[0], object);// text editor value
			  notesTxt=(notesTxt!=null && notesTxt.equals(""))?null:notesTxt;
			  Long   cont2=(Long)getFieldValue(fields[1], object);
			  if((notesTxt!=null || cont2!=null) && (value==null || value.equals(""))){
				   addFieldError(fieldName, object);
			       return;
			  }
			break;
		case FLAGFORLATER:
			  String val=(String) getFieldValue(fieldName, object);
			  Boolean flgFrLater=(Boolean) getFieldValue(fields[0], object);// text editor value
			  if(flgFrLater.equals(true) && val==null || val.equals("") ){
				   addFieldError(fieldName, object);
			       return;
			  }
			break;
		case FKNOTESCATEGORY:
			Long  fkNoteCat=(Long) getFieldValue(fieldName, object);
			 notesTxt=(String) getFieldValue(fields[0], object);
			 notesTxt=(notesTxt!=null && notesTxt.equals(""))?null:notesTxt;
			String assesment=(String) getFieldValue(fields[1], object);
			if(fkNoteCat==null  && (notesTxt!=null  || assesment!=null)){
				   addFieldError(fieldName, object);
			       return;				
			}
			break;
		case NOTES:
			notesTxt=(String) getFieldValue(fieldName, object);
			String asses=(String) getFieldValue(fields[0], object);
			Long fkNoteCate=(Long) getFieldValue(fields[1], object);
			if((notesTxt==null || notesTxt.equals("")) && (asses!=null || fkNoteCate!=null)){
				 addFieldError(fieldName, object);
			       return;		
			}			
			break;
		case COMMENTS:
			String comments=(String) getFieldValue(fieldName, object);
			Boolean flagForLater=(Boolean) getFieldValue(fields[0], object);
			if((comments==null || comments.equals("")) && flagForLater.equals(true) ){
				   addFieldError(fieldName, object);
			       return;				
			}
			break;
		/*case CORDAUTODEFERSTATUS:
			ValueStack stack = ActionContext.getContext().getValueStack();
			 Map<String, List<String>> errorMap = (Map<String, List<String>>)stack.findValue("fieldErrors");
			 if (errorMap == null || errorMap.size() == 0) { 
			
			Long cfuCountPk= (Long)getFieldValue(fields[0], object);
			Long viabilityPk= (Long)getFieldValue(fields[1], object);
			 List<PatLabsPojo> savePostTestList=(List<PatLabsPojo>) getFieldValue(fields[1], object);
			 Long cfuCount=Long.parseLong((savePostTestList.get(12).getTestresult()).trim());
			//CFU COUNT AUTO DEFER			
			     if(cfuCountPk!=null && savePostTestList.get(12).getPkpatlabs().equals(cfuCountPk)&& savePostTestList.get(12).getTestresult()!=null && !savePostTestList.get(12).getTestresult().equals("") && Long.parseLong((savePostTestList.get(12).getTestresult()).trim())==0){
				   addFieldError(fieldName, object);
			       return;	
			   }
			 if(viabilityPk!=null && savePostTestList.get(13).getPkpatlabs().equals(cfuCountPk) &&  savePostTestList.get(13).getTestresult()!=null && !savePostTestList.get(13).getTestresult().equals("") && (Long.parseLong((savePostTestList.get(13).getTestresult()).trim())>=0 || Long.parseLong((savePostTestList.get(13).getTestresult()).trim())<=84)){
				   addFieldError(fieldName, object);
			       return;	
			}
			 }
			break;	*/
		}
	}
	private static enum CordEntryNonFields
	  {
		REASONS, HLAPERCENTAGE, CORDADDITIONALINFO, BACTCULTDATESTR, FUNGCULTDATESTR, SPECRHTYPEOTHER, NOTEASSESSMENT, FLAGFORLATER, FKNOTESCATEGORY, NOTES, COMMENTS;
	  }
}
