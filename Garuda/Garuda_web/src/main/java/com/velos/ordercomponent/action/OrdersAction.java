package com.velos.ordercomponent.action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.ordercomponent.business.domain.CBUploadInfo;
import com.velos.ordercomponent.business.domain.CdrCbu;
import com.velos.ordercomponent.business.domain.CodeList;
import com.velos.ordercomponent.business.domain.LabCategory;
import com.velos.ordercomponent.business.domain.LabMaint;
import com.velos.ordercomponent.business.domain.Orders;
import com.velos.ordercomponent.business.domain.PatientLab;
import com.velos.ordercomponent.business.pojoobjects.AttachmentPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUploadInfoPojo;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.LabCategoryPojo;
import com.velos.ordercomponent.business.pojoobjects.LabMaintPojo;
import com.velos.ordercomponent.business.pojoobjects.OrderAttachmentPojo;
import com.velos.ordercomponent.business.pojoobjects.OrderPojo;
import com.velos.ordercomponent.business.pojoobjects.PatientLabPojo;
import com.velos.ordercomponent.business.pojoobjects.SearchPojo;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.Utilities;
import com.velos.ordercomponent.controller.AttachmentController;
import com.velos.ordercomponent.controller.CBBController;
import com.velos.ordercomponent.controller.OpenOrderController;
import com.velos.ordercomponent.controller.VelosSearchController;
import com.velos.ordercomponent.helper.velosHelper;


public class OrdersAction extends VelosBaseAction {
	public static final Log log = LogFactory.getLog(OrdersAction.class);

	
	public String getSubtype() {
		return subtype;
	}


	public void setSubtype(String subtype) {
		this.subtype = subtype;
	}


	private String subtype;


	public String getSubType(){
		
		String selectedValue = getRequest().getParameter("catPkCodeId");
		//log.debug("CategoryID from request = " + selectedValue);
		Long catId = Long.parseLong(selectedValue);
		String subtype=getCodeListSubTypeById(catId);
		setSubtype(subtype);
		return "success" ;
	}


@Override
public Object getModel() {
	// TODO Auto-generated method stub
	return null;
}


@Override
public String executeAction() throws Exception {
	// TODO Auto-generated method stub
	return null;
}

}
