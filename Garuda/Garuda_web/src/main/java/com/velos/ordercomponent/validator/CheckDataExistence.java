package com.velos.ordercomponent.validator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.CompoundRoot;
import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.controller.CBUController;
import com.velos.ordercomponent.util.ObjectFactory;

import java.math.BigDecimal;
import java.util.*;
/**
 * @author Mohiuddin Ali Ahmed
 * @author Anurag Upadhyay
 * @version 1.0
 * 
 */

public class CheckDataExistence extends FieldValidatorSupport {
	private boolean cdrUser;
	private String cdrUserField="";     
	private String defaultFieldValue="";
    private String fields;
    
	public boolean isCdrUser() {
		return cdrUser;
	}

	public void setCdrUser(boolean cdrUser) {
		this.cdrUser = cdrUser;
	}	

	public String getCdrUserField() {
		return cdrUserField;
	}

	public void setCdrUserField(String cdrUserField) {
		this.cdrUserField = cdrUserField;
	}

	public String getDefaultFieldValue() {
		return defaultFieldValue;
	}

	public void setDefaultFieldValue(String defaultFieldValue) {
		this.defaultFieldValue = defaultFieldValue;
	}
	
	public String getFields() {
		return fields;
	}

	public void setFields(String fields) {
		this.fields = fields;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void validate(Object object) throws ValidationException{

		String fieldName= getFieldName();
		Object obj= getFieldValue(fieldName, object);
		String value=(obj instanceof String)?(String) getFieldValue(fieldName, object):null;
	    ArrayList<Object> idCountLst=null;		    	
        StringBuilder sb=new StringBuilder();
        CompoundRoot cRoot= ActionContext.getContext().getValueStack().getRoot();
		 String defaultValue = (String) getFieldValue(defaultFieldValue, cRoot);
   	    CBUController cbuController = ((CBUController)ObjectFactory.MakeObjects.getFactoryObject(CBUController.class.getName()));
        CdrCbuPojo cdrCbuPojo=(CdrCbuPojo) getFieldValue("cdrCbuPojo", cRoot);
		switch(ExecutionResolver.valueOf(fieldName.toUpperCase())){
		
		case REGISTRYID:
		if(cdrUser){//require to check cdruser constraint ** apply for cord entry screen **			 
		     boolean isCDRUser=(Boolean) getFieldValue(cdrUserField, cRoot);			
		     if(value!=null && defaultValue!=null && !(value.equals(defaultValue)) && isCDRUser){
		    	        sb.append("select 'RegId', count(1) as count from cb_cord where Replace(CORD_REGISTRY_ID,'-','')='").append(value.replaceAll("-", "")).append("'");
		    	        sb.append(" UNION ");
		    	        sb.append("select 'RegMatId', count(1) as count from cb_cord where Replace(REGISTRY_MATERNAL_ID,'-','') ='").append(value.replaceAll("-", "")).append("' order by 1");
		    	 
		    	       try {
						 idCountLst=(ArrayList<Object>)cbuController.validateDataExistence(sb);
					
					} catch (Exception e) {						
						e.printStackTrace();
					}		
					if((((BigDecimal)((Object[])idCountLst.get(1))[1]).longValue())>0 || (((BigDecimal)((Object[])idCountLst.get(0))[1]).longValue())>0){
						setMessageKey("garuda.common.id.exist");
						  addFieldError(fieldName, object);
						  return;
					}			
		       }else if(value!=null && defaultValue!=null && !(value.equals(defaultValue)) && !isCDRUser){
		    	   sb.append("select 'RegId', count(1) as count from cb_cord where Replace(CORD_REGISTRY_ID,'-','')='").append(value.replaceAll("-", "")).append("' and Replace(FK_CBB_ID,'-','')=").append(cdrCbuPojo.getFkCbbId());
		    	   sb.append(" UNION ");
		    	   sb.append("select 'RegMatId', count(1) as count from cb_cord where Replace(REGISTRY_MATERNAL_ID,'-','') ='").append(value.replaceAll("-", "")).append("' and Replace(FK_CBB_ID,'-','')=").append(cdrCbuPojo.getFkCbbId()).append(" order by 1");
		    	   try {
						 idCountLst=(ArrayList<Object>)cbuController.validateDataExistence(sb);					
					 } catch (Exception e) {						
						e.printStackTrace();
					  }
					 // for registry id
					 if((((BigDecimal)((Object[])idCountLst.get(1))[1]).longValue())>0 ){
						 setMessageKey("garuda.cbu.cordentry.cburegid");
						  addFieldError(fieldName, object);
						  return;
					  }else if((((BigDecimal)((Object[])idCountLst.get(0))[1]).longValue())>0){
						  //for maternal registry id
						  setMessageKey("garuda.cbu.cordentry.cburegidasmatregid");
						  addFieldError(fieldName, object);
						  return;
					  }
		      
		       }
	      }/*else if(){
	      If User is CDR User  
	      }*/
		
		break;
		case REGISTRYMATERNALID:
			if(cdrUser){//require to check cdruser constraint ** apply for cord entry screen **				
			     boolean isCDRUser=(Boolean) getFieldValue(cdrUserField, cRoot);				
			     if(value!=null && defaultValue!=null && !(value.equals(defaultValue)) && isCDRUser){
			    	 
			    	        sb.append("select 'RegId', count(1) as count from cb_cord where Replace(CORD_REGISTRY_ID,'-','') ='").append(value.replaceAll("-", "")).append("'");
			    	        sb.append(" UNION ");
			    	        sb.append("select 'RegMatId', count(1) as count from cb_cord where Replace(REGISTRY_MATERNAL_ID,'-','') ='").append(value.replaceAll("-", "")).append("'");
			    	        sb.append(" UNION ");
			    	        sb.append("select 'RegMatIdWithinCbb', count(1) as count from cb_cord where Replace(REGISTRY_MATERNAL_ID,'-','')='").append(value.replaceAll("-", "")).append("' and Replace(FK_CBB_ID,'-','')=").append(cdrCbuPojo.getFkCbbId()).append(" order by 1");
			    	       try {
							 idCountLst=(ArrayList<Object>)cbuController.validateDataExistence(sb);
						
						} catch (Exception e) {						
							e.printStackTrace();
						}		
						if(((((BigDecimal)((Object[])idCountLst.get(1))[1]).longValue())>0) && ((((BigDecimal)((Object[])idCountLst.get(2))[1]).longValue())==0)){
							  setMessageKey("garuda.cbu.cordentry.samematregid");
							  addFieldError(fieldName, object);
							  return;
						}
						else if((((BigDecimal)((Object[])idCountLst.get(0))[1]).longValue())>0){
							  setMessageKey("garuda.cbu.cordentry.samematregidcbuid");
							  addFieldError(fieldName, object);
							  return;
							
						}
			       }else if(value!=null && defaultValue!=null && !(value.equals(defaultValue)) && !isCDRUser){
			    	   sb.append("select 'RegId', count(1) as count from cb_cord where Replace(CORD_REGISTRY_ID,'-','')='").append(value.replaceAll("-", "")).append("' and Replace(FK_CBB_ID,'-','')=").append(cdrCbuPojo.getFkCbbId());
			    	   sb.append(" UNION ");
			    	   sb.append("select 'RegMatId', count(1) as count from cb_cord where Replace(REGISTRY_MATERNAL_ID,'-','') ='").append(value.replaceAll("-", "")).append("' and Replace(FK_CBB_ID,'-','')=").append(cdrCbuPojo.getFkCbbId()).append(" order by 1");
			    	   try {
							 idCountLst=(ArrayList<Object>)cbuController.validateDataExistence(sb);					
						 } catch (Exception e) {						
							e.printStackTrace();
						  }
						 // for registry id
						 if((((BigDecimal)((Object[])idCountLst.get(1))[1]).longValue())>0 ){
							 setMessageKey("garuda.cbu.cordentry.matregid");
							  addFieldError(fieldName, object);
							  return;
						  }else if((((BigDecimal)((Object[])idCountLst.get(0))[1]).longValue())>0){
							  //for maternal registry id
							  setMessageKey("garuda.cbu.cordentry.cburegidasmatregid");
							  addFieldError(fieldName, object);
							  return;
						  }
			      
			       }
		      }
			break;
			
		case LOCALCBUID:			
			if(value!=null && defaultValue!=null && !(value.equals(defaultValue))){
				sb.append("select 'LocId', count(1) as count from cb_cord where Replace(CORD_LOCAL_CBU_ID,'-','')='").append(value.replaceAll("-", "")).append("' and Replace(FK_CBB_ID,'-','')=").append(cdrCbuPojo.getFkCbbId());
				sb.append(" UNION ");
				sb.append("select 'MatLocId', count(1) as count from cb_cord where Replace(MATERNAL_LOCAL_ID,'-','') ='").append(value.replaceAll("-", "")).append("' and Replace(FK_CBB_ID,'-','')=").append(cdrCbuPojo.getFkCbbId()).append(" order by 1");
    	        
    	       try {
				 idCountLst=(ArrayList<Object>)cbuController.validateDataExistence(sb);
			
			} catch (Exception e) {						
				e.printStackTrace();
			}		
			if((((BigDecimal)((Object[])idCountLst.get(1))[1]).longValue())>0 ){
				  setMessageKey("garuda.cbu.cordentry.localCbuIdexistasMatId");
				  addFieldError(fieldName, object);
				  return;
			}else if((((BigDecimal)((Object[])idCountLst.get(0))[1]).longValue())>0){
				  setMessageKey("garuda.cbu.cordentry.localCbuIdexist");
				  addFieldError(fieldName, object);
				  return;
				
			}
       }
			
			break;
		case LOCALMATERNALID:
			if(value!=null && defaultValue!=null && !(value.equals(defaultValue))){
				sb.append("select 'LocId', count(1) as count from cb_cord where Replace(CORD_LOCAL_CBU_ID,'-','') ='").append(value.replaceAll("-", "")).append("' and Replace(FK_CBB_ID,'-','')=").append(cdrCbuPojo.getFkCbbId());
    	        sb.append(" UNION ");
    	        sb.append("select 'MatLocId', count(1) as count from cb_cord where Replace(MATERNAL_LOCAL_ID,'-','') ='").append(value.replaceAll("-", "")).append("' and Replace(FK_CBB_ID,'-','')=").append(cdrCbuPojo.getFkCbbId());
    	        sb.append(" UNION ");
    	        sb.append("select 'RegMatWithMatLocWithinCbb', count(1) as count from cb_cord where Replace(MATERNAL_LOCAL_ID,'-','')='").append(value.replaceAll("-", "")).append("' and Replace(REGISTRY_MATERNAL_ID,'-','')='").append(cdrCbuPojo.getRegistryMaternalId().replaceAll("-", "")).append("' and Replace(FK_CBB_ID,'-','')=").append(cdrCbuPojo.getFkCbbId()).append(" order by 1");
    	        
    	       try {
				 idCountLst=(ArrayList<Object>)cbuController.validateDataExistence(sb);
			
			} catch (Exception e) {						
				e.printStackTrace();
			}		
			if((((BigDecimal)((Object[])idCountLst.get(2))[1]).longValue())==0 && (((BigDecimal)((Object[])idCountLst.get(1))[1]).longValue())>0){
				  setMessageKey("garuda.cbu.cordentry.cbumatlocexistdiffmatid");
				  addFieldError(fieldName, object);
				  return;
			}else if((((BigDecimal)((Object[])idCountLst.get(0))[1]).longValue())>0){
				  setMessageKey("garuda.cbu.cordentry.locmatidexistascblocid");
				  addFieldError(fieldName, object);
				  return;
				
			}
       }
			break;
		case CORDISBIDINCODE:
			if(value!=null && defaultValue!=null && !(value.equals(defaultValue))){
				sb.append("select 'LocId', count(1) as count from cb_cord where Replace(CORD_ISBI_DIN_CODE,'-','') ='").append(value.replaceAll("-", "")).append("' and Replace(FK_CBB_ID,'-','')=").append(cdrCbuPojo.getFkCbbId());   	       
    	      try {
				 idCountLst=(ArrayList<Object>)cbuController.validateDataExistence(sb);			
			} catch (Exception e) {						
				e.printStackTrace();
			}
			 if((((BigDecimal)((Object[])idCountLst.get(0))[1]).longValue())>0){
				  setMessageKey("garuda.common.id.exist");
				  addFieldError(fieldName, object);
				  return;			
			}
         }
			break;
		case ISCORDSUBMITTED:
			 ValueStack stack = ActionContext.getContext().getValueStack();
			 Map<String, List<String>> errorMap = (Map<String, List<String>>)stack.findValue("fieldErrors");
			 if (errorMap == null || errorMap.size() == 0) {  
			String [] fieldNames=getFields().split(",");
		    long cordPk=(Long)getFieldValue(fieldNames[0], cRoot);
			sb.append("select CORD_SEARCHABLE from cb_cord where PK_CORD=").append(cordPk);
			 try {
				 idCountLst=(ArrayList<Object>)cbuController.validateDataExistence(sb);			
			} catch (Exception e) {						
				e.printStackTrace();
			}
			if(Long.parseLong(idCountLst.get(0).toString())==1l){
				 addFieldError(fieldName, object);
				  return;
			}
			 }
			break;
	 }	
	}
	
	public static enum ExecutionResolver
	  {
		REGISTRYID, REGISTRYMATERNALID, LOCALCBUID, LOCALMATERNALID, CORDISBIDINCODE, ISCORDSUBMITTED;
	  }

}
