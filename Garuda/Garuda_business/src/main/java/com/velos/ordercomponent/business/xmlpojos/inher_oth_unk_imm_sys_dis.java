package com.velos.ordercomponent.business.xmlpojos;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class inher_oth_unk_imm_sys_dis {
	private String inher_oth_unk_imm_sys_dis_ind;
	private List<Relations> Relations;
	
	@XmlElement(name="inher_oth_unk_imm_sys_dis_ind")
	public String getInher_oth_unk_imm_sys_dis_ind() {
		return inher_oth_unk_imm_sys_dis_ind;
	}
	public void setInher_oth_unk_imm_sys_dis_ind(
			String inher_oth_unk_imm_sys_dis_ind) {
		this.inher_oth_unk_imm_sys_dis_ind = inher_oth_unk_imm_sys_dis_ind;
	}
	@XmlElement(name="Relations")
	public List<Relations> getRelations() {
		return Relations;
	}
	public void setRelations(List<Relations> relations) {
		Relations = relations;
	}
	
}
