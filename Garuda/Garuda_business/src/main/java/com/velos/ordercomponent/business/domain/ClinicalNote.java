package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CB_NOTES")
@SequenceGenerator(sequenceName="SEQ_CB_NOTES",name="SEQ_CB_NOTES",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class ClinicalNote extends Auditable {
	/**
	 * @author Ruchi
	 */
	
	private Long pkNotes;
	private Long entityId;
	private Long entityType;
	private Long fkNotesCategory;
	private Long fkNotesType ;
	private String notes;
	private String noteAssessment;
	private Date availableDate;
	private String explaination;
	private String keyword;
	private String sentTo;
	private Boolean visibility;
	private Long fkReason;
	private Long fkCbuStatus;
	private Boolean flagForLater;
	private Boolean amended;
	private String deletedFlag;
	private String comments;
	private String subject;
	private Date requestDate;
	private Long requestType;
	private String explainationTu;
	private Boolean reply;
	private String noteVisible;
	private Long fkOrderId;
	private Boolean informTo;
	private Boolean consultTo;
	private String noteSeq;
	private Boolean tagNote;
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_NOTES")
	@Column(name="PK_NOTES")
	public Long getPkNotes() {
		return pkNotes;
	}
	public void setPkNotes(Long pkNotes) {
		this.pkNotes = pkNotes;
	}
	@Column(name="ENTITY_ID")
	public Long getEntityId() {
		return entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	@Column(name="COMMENTS")
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	@Column(name="ENTITY_TYPE")
	public Long getEntityType() {
		return entityType;
	}
	public void setEntityType(Long entityType) {
		this.entityType = entityType;
	}
	@Column(name="FK_NOTES_CATEGORY")
	public Long getFkNotesCategory() {
		return fkNotesCategory;
	}
	public void setFkNotesCategory(Long fkNotesCategory) {
		this.fkNotesCategory = fkNotesCategory;
	}
	@Column(name="FK_NOTES_TYPE")
	public Long getFkNotesType() {
		return fkNotesType;
	}
	public void setFkNotesType(Long fkNotesType) {
		this.fkNotesType = fkNotesType;
	}
	@Column(name="NOTES") 
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	@Column(name="NOTE_ASSESSMENT") 
	public String getNoteAssessment() {
		return noteAssessment;
	}
	public void setNoteAssessment(String noteAssessment) {
		this.noteAssessment = noteAssessment;
	}
	@Column(name="FK_REASON") 
	public Long getFkReason() {
		return fkReason;
	}
	public void setFkReason(Long fkReason) {
		this.fkReason = fkReason;
	}
	@Column(name="AVAILABLE_DATE") 
	public Date getAvailableDate() {
		return availableDate;
	}
	public void setAvailableDate(Date availableDate) {
		this.availableDate = availableDate;
	}
	@Column(name="EXPLAINATION") 
	public String getExplaination() {
		return explaination;
	}
	public void setExplaination(String explaination) {
		this.explaination = explaination;
	}
	@Column(name="KEYWORD") 
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	@Column(name="SEND_TO") 
	public String getSentTo() {
		return sentTo;
	}
	public void setSentTo(String sentTo) {
		this.sentTo = sentTo;
	}
	
	@Column(name="FK_CBU_STATUS") 
	public Long getFkCbuStatus() {
		return fkCbuStatus;
	}
	public void setFkCbuStatus(Long fkCbuStatus) {
		this.fkCbuStatus = fkCbuStatus;
	}
	@Column(name="FLAG_FOR_LATER") 
	public Boolean getFlagForLater() {
		return flagForLater;
	}
	public void setFlagForLater(Boolean flagForLater) {
		this.flagForLater = flagForLater;
	}
	
	@Column(name="AMENDED") 
	public Boolean getAmended() {
		return amended;
	}
	public void setAmended(Boolean amended) {
		this.amended = amended;
	}
	
	
	@Column(name="DELETEDFLAG") 
	public String getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(String deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	@Column(name="SUBJECT")
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	@Column(name="REQUEST_DATE")
	public Date getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	@Column(name="REQUEST_TYPE")
	public Long getRequestType() {
		return requestType;
	}
	public void setRequestType(Long requestType) {
		this.requestType = requestType;
	}
	@Column(name="EXPLAINATION_TU")
	public String getExplainationTu() {
		return explainationTu;
	}
	public void setExplainationTu(String explainationTu) {
		this.explainationTu = explainationTu;
	}
	@Column(name="REPLY")
	public Boolean getReply() {
		return reply;
	}
	public void setReply(Boolean reply) {
		this.reply = reply;
	}
	@Column(name="VISIBILITY")
	public Boolean getVisibility() {
		return visibility;
	}
	public void setVisibility(Boolean visibility) {
		this.visibility = visibility;
	}
	
	@Column(name="NOTE_VISIBLE")
	public String getNoteVisible() {
		return noteVisible;
	}
	public void setNoteVisible(String noteVisible) {
		this.noteVisible = noteVisible;
	}
	
	@Column(name="FK_ORDER_ID")
	public Long getFkOrderId() {
		return fkOrderId;
	}
	public void setFkOrderId(Long fkOrderId) {
		this.fkOrderId = fkOrderId;
	}
	@Column(name="INFORM_TO")
	public Boolean getInformTo() {
		return informTo;
	}
	public void setInformTo(Boolean informTo) {
		this.informTo = informTo;
	}
	@Column(name="CONSULT_TO")
	public Boolean getConsultTo() {
		return consultTo;
	}
	public void setConsultTo(Boolean consultTo) {
		this.consultTo = consultTo;
	}
	@Column(name="NOTE_SEQ")
	public String getNoteSeq() {
		return noteSeq;
	}
	public void setNoteSeq(String noteSeq) {
		this.noteSeq = noteSeq;
	}
	@Column(name="TAG_NOTE")
	public Boolean getTagNote() {
		return tagNote;
	}
	public void setTagNote(Boolean tagNote) {
		this.tagNote = tagNote;
	}
	
	
	
	

}
