package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

import com.velos.ordercomponent.business.domain.CBBProcedures;

public class CBBProcedureInfoPojo extends AuditablePojo{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long pkProcInfoId;
	private Long fkProcessingId;
	private Long fkStorMethod;
	private Long fkFreezManufac;
	private String otherFreezManufac;
	private Long fkStorTemp;
	private Long fkContrlRateFreezing;
	private Long fkFrozenIn;
	private String otherFrozenCont;
	private Long noOfIndiFrac;
	private Long fkBagType;
	private String cryoBagManufac;
	private Long maxValue;
	private Long fkProcMethId;
	private Long fkIfAutomated;
	private String otherProcessing;
	private Long fkProductModification;
	private String otherProdModi;
	private Long noOfSegments;
	private Long filterPaper;
	private Long rbcPallets;
	private Long noOfExtrDnaAliquots;
	private Long noOfNonviableAliquots;
	private Long noOfSerumAliquots;
	private Long noOfPlasmaAliquots;
	private Long noOfViableCellAliquots;
	private Long totCbuAliquots;
	private Long noOfExtrDnaMaterAliquots;
	private Long noOfCellMaterAliquots;
	private Long noOfSerumMaterAliquots;
	private Long noOfPlasmaMaterAliquots;
	private Long totMaterAliquots;
	private String sopRefNo;
	private String sopTitle;
	private Date sopStrtDate;
	private Date sopTermiDate;
	private Double thouUnitPerMlHeparin;
	private Double fiveUnitPerMlHeparin;
	private Double tenUnitPerMlHeparin;
	private Double sixPerHydroxyethylStarch;
	private Double cpda;
	private Double cpd;
	private Double acd;
	private Double otherAnticoagulant;
	private String specifyOthAnti;
	private Double hunPerDmso;
	private Double hunPerGlycerol;
	private Double tenPerDextran_40;
	private Double fivePerHumanAlbu;
	private Double twenFiveHumAlbu;
	private Double plasmalyte;
	private Double othCryoprotectant;
	private String specOthCryopro;
	private Double fivePerDextrose;
	private Double pointNinePerNacl;
	private Double othDiluents;
	private String specOthDiluents;
	private CBBProcedures cbbProcedures;
	private String sopStrtDateStr;
	private String sopTermiDateStr;
	private Long pkProcMethodId;
	private Long pkIfAutomatedOther;
	private Long pkProdModiOther;
	private Long pkFreezManuOther;
	private Long pkFrozenInOther;
	private Long pkFrozenInBag;
	private String otherBagType;
	private Long pkOtherBagType;
	private Double thouUnitPerMlHeparinper;
	private Double fiveUnitPerMlHeparinper;
	private Double tenUnitPerMlHeparinper;
	private Double sixPerHydroxyethylStarchper;
	private Double cpdaper;
	private Double cpdper;
	private Double acdper;
	private Double otherAnticoagulantper;
	private Double hunPerDmsoper;
	private Double hunPerGlycerolper;
	private Double tenPerDextran_40per;
	private Double fivePerHumanAlbuper;
	private Double twenFiveHumAlbuper;
	private Double plasmalyteper;
	private Double othCryoprotectantper;
	private Double fivePerDextroseper;
	private Double pointNinePerNaclper;
	private Double othDiluentsper;

	private Long noOfBags;
	private Long noOfBagsOthers;
	private Long fkBag1Type;
	private Long fkBag2Type;
	private Long noOfViableSampleNotRep;
	private Long noOfOthRepAliqProd;
	private Long noOfOthRepAliqAltCond;
	private Long pkBag1Type;
	private Long pkBag2Type;
	private Long pkNoOfBagsOthers;
	private String cryoBagManufac2;
	private Long maxValue2;
	private String otherBagType2;
	private String otherBagType1;
	

	public String getSopStrtDateStr() {
		return sopStrtDateStr;
	}

	public void setSopStrtDateStr(String sopStrtDateStr) {
		this.sopStrtDateStr = sopStrtDateStr;
	}

	public String getSopTermiDateStr() {
		return sopTermiDateStr;
	}

	public void setSopTermiDateStr(String sopTermiDateStr) {
		this.sopTermiDateStr = sopTermiDateStr;
	}

	public CBBProcedures getCbbProcedures() {
		return cbbProcedures;
	}
	
	public void setCbbProcedures(CBBProcedures cbbProcedures) {
		this.cbbProcedures = cbbProcedures;
	}
	public Long getFkProcMethId() {
		return fkProcMethId;
	}
	public void setFkProcMethId(Long fkProcMethId) {
		this.fkProcMethId = fkProcMethId;
	}
	public Long getPkProcInfoId() {
		return pkProcInfoId;
	}
	public void setPkProcInfoId(Long pkProcInfoId) {
		this.pkProcInfoId = pkProcInfoId;
	}
	public Long getFkStorMethod() {
		return fkStorMethod;
	}
	public void setFkStorMethod(Long fkStorMethod) {
		this.fkStorMethod = fkStorMethod;
	}
	public Long getFkFreezManufac() {
		return fkFreezManufac;
	}
	public void setFkFreezManufac(Long fkFreezManufac) {
		this.fkFreezManufac = fkFreezManufac;
	}
	public String getOtherFreezManufac() {
		return otherFreezManufac;
	}
	public void setOtherFreezManufac(String otherFreezManufac) {
		this.otherFreezManufac = otherFreezManufac;
	}
	public Long getFkStorTemp() {
		return fkStorTemp;
	}
	public void setFkStorTemp(Long fkStorTemp) {
		this.fkStorTemp = fkStorTemp;
	}
	public Long getFkContrlRateFreezing() {
		return fkContrlRateFreezing;
	}
	public void setFkContrlRateFreezing(Long fkContrlRateFreezing) {
		this.fkContrlRateFreezing = fkContrlRateFreezing;
	}
	public Long getFkFrozenIn() {
		return fkFrozenIn;
	}
	public void setFkFrozenIn(Long fkFrozenIn) {
		this.fkFrozenIn = fkFrozenIn;
	}
	public String getOtherFrozenCont() {
		return otherFrozenCont;
	}
	public void setOtherFrozenCont(String otherFrozenCont) {
		this.otherFrozenCont = otherFrozenCont;
	}
	public Long getNoOfIndiFrac() {
		return noOfIndiFrac;
	}
	public void setNoOfIndiFrac(Long noOfIndiFrac) {
		this.noOfIndiFrac = noOfIndiFrac;
	}
	public Long getFkBagType() {
		return fkBagType;
	}
	public void setFkBagType(Long fkBagType) {
		this.fkBagType = fkBagType;
	}
	public String getCryoBagManufac() {
		return cryoBagManufac;
	}
	public void setCryoBagManufac(String cryoBagManufac) {
		this.cryoBagManufac = cryoBagManufac;
	}
	public Long getMaxValue() {
		return maxValue;
	}
	public void setMaxValue(Long maxValue) {
		this.maxValue = maxValue;
	}
	public Long getFkProcessingId() {
		return fkProcessingId;
	}
	public void setFkProcessingId(Long fkProcessingId) {
		this.fkProcessingId = fkProcessingId;
	}
	public Long getFkIfAutomated() {
		return fkIfAutomated;
	}
	public void setFkIfAutomated(Long fkIfAutomated) {
		this.fkIfAutomated = fkIfAutomated;
	}
	public String getOtherProcessing() {
		return otherProcessing;
	}
	public void setOtherProcessing(String otherProcessing) {
		this.otherProcessing = otherProcessing;
	}
	public Long getFkProductModification() {
		return fkProductModification;
	}
	public void setFkProductModification(Long fkProductModification) {
		this.fkProductModification = fkProductModification;
	}
	public String getOtherProdModi() {
		return otherProdModi;
	}
	public void setOtherProdModi(String otherProdModi) {
		this.otherProdModi = otherProdModi;
	}
	public Long getNoOfSegments() {
		return noOfSegments;
	}
	public void setNoOfSegments(Long noOfSegments) {
		this.noOfSegments = noOfSegments;
	}
	public Long getFilterPaper() {
		return filterPaper;
	}
	public void setFilterPaper(Long filterPaper) {
		this.filterPaper = filterPaper;
	}
	public Long getRbcPallets() {
		return rbcPallets;
	}
	public void setRbcPallets(Long rbcPallets) {
		this.rbcPallets = rbcPallets;
	}
	public Long getNoOfExtrDnaAliquots() {
		return noOfExtrDnaAliquots;
	}
	public void setNoOfExtrDnaAliquots(Long noOfExtrDnaAliquots) {
		this.noOfExtrDnaAliquots = noOfExtrDnaAliquots;
	}
	public Long getNoOfNonviableAliquots() {
		return noOfNonviableAliquots;
	}
	public void setNoOfNonviableAliquots(Long noOfNonviableAliquots) {
		this.noOfNonviableAliquots = noOfNonviableAliquots;
	}
	public Long getNoOfSerumAliquots() {
		return noOfSerumAliquots;
	}
	public void setNoOfSerumAliquots(Long noOfSerumAliquots) {
		this.noOfSerumAliquots = noOfSerumAliquots;
	}
	public Long getNoOfPlasmaAliquots() {
		return noOfPlasmaAliquots;
	}
	public void setNoOfPlasmaAliquots(Long noOfPlasmaAliquots) {
		this.noOfPlasmaAliquots = noOfPlasmaAliquots;
	}
	public Long getNoOfViableCellAliquots() {
		return noOfViableCellAliquots;
	}
	public void setNoOfViableCellAliquots(Long noOfViableCellAliquots) {
		this.noOfViableCellAliquots = noOfViableCellAliquots;
	}
	public Long getTotCbuAliquots() {
		return totCbuAliquots;
	}
	public void setTotCbuAliquots(Long totCbuAliquots) {
		this.totCbuAliquots = totCbuAliquots;
	}
	public Long getNoOfExtrDnaMaterAliquots() {
		return noOfExtrDnaMaterAliquots;
	}
	public void setNoOfExtrDnaMaterAliquots(Long noOfExtrDnaMaterAliquots) {
		this.noOfExtrDnaMaterAliquots = noOfExtrDnaMaterAliquots;
	}
	public Long getNoOfCellMaterAliquots() {
		return noOfCellMaterAliquots;
	}
	public void setNoOfCellMaterAliquots(Long noOfCellMaterAliquots) {
		this.noOfCellMaterAliquots = noOfCellMaterAliquots;
	}
	public Long getNoOfSerumMaterAliquots() {
		return noOfSerumMaterAliquots;
	}
	public void setNoOfSerumMaterAliquots(Long noOfSerumMaterAliquots) {
		this.noOfSerumMaterAliquots = noOfSerumMaterAliquots;
	}
	public Long getNoOfPlasmaMaterAliquots() {
		return noOfPlasmaMaterAliquots;
	}
	public void setNoOfPlasmaMaterAliquots(Long noOfPlasmaMaterAliquots) {
		this.noOfPlasmaMaterAliquots = noOfPlasmaMaterAliquots;
	}
	public Long getTotMaterAliquots() {
		return totMaterAliquots;
	}
	public void setTotMaterAliquots(Long totMaterAliquots) {
		this.totMaterAliquots = totMaterAliquots;
	}
	public String getSopRefNo() {
		return sopRefNo;
	}
	public void setSopRefNo(String sopRefNo) {
		this.sopRefNo = sopRefNo;
	}
	public String getSopTitle() {
		return sopTitle;
	}
	public void setSopTitle(String sopTitle) {
		this.sopTitle = sopTitle;
	}
	public Date getSopStrtDate() {
		return sopStrtDate;
	}
	public void setSopStrtDate(Date sopStrtDate) {
		this.sopStrtDate = sopStrtDate;
	}
	public Date getSopTermiDate() {
		return sopTermiDate;
	}
	public void setSopTermiDate(Date sopTermiDate) {
		this.sopTermiDate = sopTermiDate;
	}
	public Double getThouUnitPerMlHeparin() {
		return thouUnitPerMlHeparin;
	}
	public void setThouUnitPerMlHeparin(Double thouUnitPerMlHeparin) {
		this.thouUnitPerMlHeparin = thouUnitPerMlHeparin;
	}
	public Double getFiveUnitPerMlHeparin() {
		return fiveUnitPerMlHeparin;
	}
	public void setFiveUnitPerMlHeparin(Double fiveUnitPerMlHeparin) {
		this.fiveUnitPerMlHeparin = fiveUnitPerMlHeparin;
	}
	public Double getTenUnitPerMlHeparin() {
		return tenUnitPerMlHeparin;
	}
	public void setTenUnitPerMlHeparin(Double tenUnitPerMlHeparin) {
		this.tenUnitPerMlHeparin = tenUnitPerMlHeparin;
	}
	public Double getSixPerHydroxyethylStarch() {
		return sixPerHydroxyethylStarch;
	}
	public void setSixPerHydroxyethylStarch(Double sixPerHydroxyethylStarch) {
		this.sixPerHydroxyethylStarch = sixPerHydroxyethylStarch;
	}
	public Double getCpda() {
		return cpda;
	}
	public void setCpda(Double cpda) {
		this.cpda = cpda;
	}
	public Double getCpd() {
		return cpd;
	}
	public void setCpd(Double cpd) {
		this.cpd = cpd;
	}
	public Double getAcd() {
		return acd;
	}
	public void setAcd(Double acd) {
		this.acd = acd;
	}
	public Double getOtherAnticoagulant() {
		return otherAnticoagulant;
	}
	public void setOtherAnticoagulant(Double otherAnticoagulant) {
		this.otherAnticoagulant = otherAnticoagulant;
	}
	public String getSpecifyOthAnti() {
		return specifyOthAnti;
	}
	public void setSpecifyOthAnti(String specifyOthAnti) {
		this.specifyOthAnti = specifyOthAnti;
	}
	public Double getHunPerDmso() {
		return hunPerDmso;
	}
	public void setHunPerDmso(Double hunPerDmso) {
		this.hunPerDmso = hunPerDmso;
	}
	public Double getHunPerGlycerol() {
		return hunPerGlycerol;
	}
	public void setHunPerGlycerol(Double hunPerGlycerol) {
		this.hunPerGlycerol = hunPerGlycerol;
	}
	public Double getTenPerDextran_40() {
		return tenPerDextran_40;
	}
	public void setTenPerDextran_40(Double tenPerDextran_40) {
		this.tenPerDextran_40 = tenPerDextran_40;
	}
	public Double getFivePerHumanAlbu() {
		return fivePerHumanAlbu;
	}
	public void setFivePerHumanAlbu(Double fivePerHumanAlbu) {
		this.fivePerHumanAlbu = fivePerHumanAlbu;
	}
	public Double getTwenFiveHumAlbu() {
		return twenFiveHumAlbu;
	}
	public void setTwenFiveHumAlbu(Double twenFiveHumAlbu) {
		this.twenFiveHumAlbu = twenFiveHumAlbu;
	}
	public Double getPlasmalyte() {
		return plasmalyte;
	}
	public void setPlasmalyte(Double plasmalyte) {
		this.plasmalyte = plasmalyte;
	}
	public Double getOthCryoprotectant() {
		return othCryoprotectant;
	}
	public void setOthCryoprotectant(Double othCryoprotectant) {
		this.othCryoprotectant = othCryoprotectant;
	}
	public String getSpecOthCryopro() {
		return specOthCryopro;
	}
	public void setSpecOthCryopro(String specOthCryopro) {
		this.specOthCryopro = specOthCryopro;
	}
	public Double getFivePerDextrose() {
		return fivePerDextrose;
	}
	public void setFivePerDextrose(Double fivePerDextrose) {
		this.fivePerDextrose = fivePerDextrose;
	}
	public Double getPointNinePerNacl() {
		return pointNinePerNacl;
	}
	public void setPointNinePerNacl(Double pointNinePerNacl) {
		this.pointNinePerNacl = pointNinePerNacl;
	}
	public Double getOthDiluents() {
		return othDiluents;
	}
	public void setOthDiluents(Double othDiluents) {
		this.othDiluents = othDiluents;
	}
	public String getSpecOthDiluents() {
		return specOthDiluents;
	}
	public void setSpecOthDiluents(String specOthDiluents) {
		this.specOthDiluents = specOthDiluents;
	}
	public Long getPkProcMethodId() {
		return pkProcMethodId;
	}

	public void setPkProcMethodId(Long pkProcMethodId) {
		this.pkProcMethodId = pkProcMethodId;
	}

	public Long getPkIfAutomatedOther() {
		return pkIfAutomatedOther;
	}

	public void setPkIfAutomatedOther(Long pkIfAutomatedOther) {
		this.pkIfAutomatedOther = pkIfAutomatedOther;
	}

	public Long getPkProdModiOther() {
		return pkProdModiOther;
	}

	public void setPkProdModiOther(Long pkProdModiOther) {
		this.pkProdModiOther = pkProdModiOther;
	}

	public Long getPkFreezManuOther() {
		return pkFreezManuOther;
	}

	public void setPkFreezManuOther(Long pkFreezManuOther) {
		this.pkFreezManuOther = pkFreezManuOther;
	}

	public Long getPkFrozenInOther() {
		return pkFrozenInOther;
	}

	public void setPkFrozenInOther(Long pkFrozenInOther) {
		this.pkFrozenInOther = pkFrozenInOther;
	}

	public Long getPkFrozenInBag() {
		return pkFrozenInBag;
	}

	public void setPkFrozenInBag(Long pkFrozenInBag) {
		this.pkFrozenInBag = pkFrozenInBag;
	}

	public String getOtherBagType() {
		return otherBagType;
	}

	public void setOtherBagType(String otherBagType) {
		this.otherBagType = otherBagType;
	}

	public Long getPkOtherBagType() {
		return pkOtherBagType;
	}

	public void setPkOtherBagType(Long pkOtherBagType) {
		this.pkOtherBagType = pkOtherBagType;
	}

	public Double getThouUnitPerMlHeparinper() {
		return thouUnitPerMlHeparinper;
	}

	public void setThouUnitPerMlHeparinper(Double thouUnitPerMlHeparinper) {
		this.thouUnitPerMlHeparinper = thouUnitPerMlHeparinper;
	}

	public Double getFiveUnitPerMlHeparinper() {
		return fiveUnitPerMlHeparinper;
	}

	public void setFiveUnitPerMlHeparinper(Double fiveUnitPerMlHeparinper) {
		this.fiveUnitPerMlHeparinper = fiveUnitPerMlHeparinper;
	}

	public Double getTenUnitPerMlHeparinper() {
		return tenUnitPerMlHeparinper;
	}

	public void setTenUnitPerMlHeparinper(Double tenUnitPerMlHeparinper) {
		this.tenUnitPerMlHeparinper = tenUnitPerMlHeparinper;
	}

	public Double getSixPerHydroxyethylStarchper() {
		return sixPerHydroxyethylStarchper;
	}

	public void setSixPerHydroxyethylStarchper(Double sixPerHydroxyethylStarchper) {
		this.sixPerHydroxyethylStarchper = sixPerHydroxyethylStarchper;
	}

	public Double getCpdaper() {
		return cpdaper;
	}

	public void setCpdaper(Double cpdaper) {
		this.cpdaper = cpdaper;
	}

	public Double getCpdper() {
		return cpdper;
	}

	public void setCpdper(Double cpdper) {
		this.cpdper = cpdper;
	}

	public Double getAcdper() {
		return acdper;
	}

	public void setAcdper(Double acdper) {
		this.acdper = acdper;
	}

	public Double getOtherAnticoagulantper() {
		return otherAnticoagulantper;
	}

	public void setOtherAnticoagulantper(Double otherAnticoagulantper) {
		this.otherAnticoagulantper = otherAnticoagulantper;
	}

	public Double getHunPerDmsoper() {
		return hunPerDmsoper;
	}

	public void setHunPerDmsoper(Double hunPerDmsoper) {
		this.hunPerDmsoper = hunPerDmsoper;
	}

	public Double getHunPerGlycerolper() {
		return hunPerGlycerolper;
	}

	public void setHunPerGlycerolper(Double hunPerGlycerolper) {
		this.hunPerGlycerolper = hunPerGlycerolper;
	}

	public Double getTenPerDextran_40per() {
		return tenPerDextran_40per;
	}

	public void setTenPerDextran_40per(Double tenPerDextran_40per) {
		this.tenPerDextran_40per = tenPerDextran_40per;
	}

	public Double getFivePerHumanAlbuper() {
		return fivePerHumanAlbuper;
	}

	public void setFivePerHumanAlbuper(Double fivePerHumanAlbuper) {
		this.fivePerHumanAlbuper = fivePerHumanAlbuper;
	}

	public Double getTwenFiveHumAlbuper() {
		return twenFiveHumAlbuper;
	}

	public void setTwenFiveHumAlbuper(Double twenFiveHumAlbuper) {
		this.twenFiveHumAlbuper = twenFiveHumAlbuper;
	}

	public Double getPlasmalyteper() {
		return plasmalyteper;
	}

	public void setPlasmalyteper(Double plasmalyteper) {
		this.plasmalyteper = plasmalyteper;
	}

	public Double getOthCryoprotectantper() {
		return othCryoprotectantper;
	}

	public void setOthCryoprotectantper(Double othCryoprotectantper) {
		this.othCryoprotectantper = othCryoprotectantper;
	}

	public Double getFivePerDextroseper() {
		return fivePerDextroseper;
	}

	public void setFivePerDextroseper(Double fivePerDextroseper) {
		this.fivePerDextroseper = fivePerDextroseper;
	}

	public Double getPointNinePerNaclper() {
		return pointNinePerNaclper;
	}

	public void setPointNinePerNaclper(Double pointNinePerNaclper) {
		this.pointNinePerNaclper = pointNinePerNaclper;
	}

	public Double getOthDiluentsper() {
		return othDiluentsper;
	}

	public void setOthDiluentsper(Double othDiluentsper) {
		this.othDiluentsper = othDiluentsper;
	}
	
	public Long getNoOfBags() {
		return noOfBags;
	}

	public void setNoOfBags(Long noOfBags) {
		this.noOfBags = noOfBags;
	}
	
	public Long getNoOfViableSampleNotRep() {
		return noOfViableSampleNotRep;
	}

	public void setNoOfViableSampleNotRep(Long noOfViableSampleNotRep) {
		this.noOfViableSampleNotRep = noOfViableSampleNotRep;
	}

	public Long getNoOfOthRepAliqProd() {
		return noOfOthRepAliqProd;
	}

	public void setNoOfOthRepAliqProd(Long noOfOthRepAliqProd) {
		this.noOfOthRepAliqProd = noOfOthRepAliqProd;
	}

	public Long getNoOfOthRepAliqAltCond() {
		return noOfOthRepAliqAltCond;
	}

	public void setNoOfOthRepAliqAltCond(Long noOfOthRepAliqAltCond) {
		this.noOfOthRepAliqAltCond = noOfOthRepAliqAltCond;
	}

	public Long getFkBag1Type() {
		return fkBag1Type;
	}

	public void setFkBag1Type(Long fkBag1Type) {
		this.fkBag1Type = fkBag1Type;
	}

	public Long getFkBag2Type() {
		return fkBag2Type;
	}

	public void setFkBag2Type(Long fkBag2Type) {
		this.fkBag2Type = fkBag2Type;
	}

	public Long getNoOfBagsOthers() {
		return noOfBagsOthers;
	}

	public void setNoOfBagsOthers(Long noOfBagsOthers) {
		this.noOfBagsOthers = noOfBagsOthers;
	}

	public Long getPkBag1Type() {
		return pkBag1Type;
	}

	public void setPkBag1Type(Long pkBag1Type) {
		this.pkBag1Type = pkBag1Type;
	}

	public Long getPkBag2Type() {
		return pkBag2Type;
	}

	public void setPkBag2Type(Long pkBag2Type) {
		this.pkBag2Type = pkBag2Type;
	}

	public Long getPkNoOfBagsOthers() {
		return pkNoOfBagsOthers;
	}

	public void setPkNoOfBagsOthers(Long pkNoOfBagsOthers) {
		this.pkNoOfBagsOthers = pkNoOfBagsOthers;
	}

	public String getCryoBagManufac2() {
		return cryoBagManufac2;
	}

	public void setCryoBagManufac2(String cryoBagManufac2) {
		this.cryoBagManufac2 = cryoBagManufac2;
	}

	public Long getMaxValue2() {
		return maxValue2;
	}

	public void setMaxValue2(Long maxValue2) {
		this.maxValue2 = maxValue2;
	}

	public String getOtherBagType2() {
		return otherBagType2;
	}

	public void setOtherBagType2(String otherBagType2) {
		this.otherBagType2 = otherBagType2;
	}
	
	public String getOtherBagType1() {
		return otherBagType1;
	}

	public void setOtherBagType1(String otherBagType1) {
		this.otherBagType1 = otherBagType1;
	}

	
}
