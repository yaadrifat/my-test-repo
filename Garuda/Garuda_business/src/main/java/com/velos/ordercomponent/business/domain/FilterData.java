package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CB_FILTER_DATA")
@SequenceGenerator(sequenceName="SEQ_CB_FILTER_DATA",name="SEQ_CB_FILTER_DATA",allocationSize=1)
public class FilterData {
	
	public long pk_filter;
	public String filter_name;
	public String filter_paraameter;
	public long fk_user;
	public long created_by;
	public Date created_on;
	public long fk_module;
	public long fk_filter_type;
	public long fk_sub_module;
	/*public String selected_param;*/

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_FILTER_DATA")
	@Column(name="PK_FILTER_DATA")
	public long getPk_filter() {
		return pk_filter;
	}
	@Column(name="FILTER_NAME")
	public String getFilter_name() {
		return filter_name;
	}
	@Column(name="FILTER_PARAMS")
	public String getFilter_paraameter() {
		return filter_paraameter;
	}
	@Column(name="FK_USER_ID")
	public long getFk_user() {
		return fk_user;
	}
	@Column(name="CREATED_BY")
	public long getCreated_by() {
		return created_by;
	}
	@Column(name="CREATED_ON")
	public Date getCreated_on() {
		return created_on;
	}
	@Column(name="FILTER_MODULE")
	public long getFk_module() {
		return fk_module;
	}
	@Column(name="FK_FILTER_TYPE")
	public long getFk_filter_type() {
		return fk_filter_type;
	}
	@Column(name="FK_SUB_MODULE")
	public long getFk_sub_module() {
		return fk_sub_module;
	}
	public void setFk_sub_module(long fk_sub_module) {
		this.fk_sub_module = fk_sub_module;
	}
	
	public void setPk_filter(long pk_filter) {
		this.pk_filter = pk_filter;
	}
	public void setFilter_name(String filter_name) {
		this.filter_name = filter_name;
	}
	public void setFilter_paraameter(String filter_paraameter) {
		this.filter_paraameter = filter_paraameter;
	}
	public void setFk_user(long fk_user) {
		this.fk_user = fk_user;
	}
	public void setCreated_by(long created_by) {
		this.created_by = created_by;
	}
	public void setCreated_on(Date created_on) {
		this.created_on = created_on;
	}
	public void setFk_module(long fk_module) {
		this.fk_module = fk_module;
	}
	public void setFk_filter_type(long fk_filter_type) {
		this.fk_filter_type = fk_filter_type;
	}
	/*@Column(name="SELECTED_PARAM")
	public String getSelected_param() {
		return selected_param;
	}
	public void setSelected_param(String selected_param) {
		this.selected_param = selected_param;
	}
	*/
	

	
	
	
}
