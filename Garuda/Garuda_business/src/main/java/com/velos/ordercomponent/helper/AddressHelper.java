package com.velos.ordercomponent.helper;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;

import com.velos.ordercomponent.business.domain.Address;
import com.velos.ordercomponent.business.pojoobjects.AddressPojo;
import com.velos.ordercomponent.business.util.HibernateUtil;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.VelosUtil;

public class AddressHelper {
	public static final Log log=LogFactory.getLog(AddressHelper.class);
	
	public static AddressPojo saveAddress(AddressPojo addressPojo){
		return saveAddress(addressPojo,null);
	}
	
    public static AddressPojo saveAddress(AddressPojo addressPojo , Session session){
    	//log.debug("saveAddress with session");
		//log.debug("AddressHelper : saveAddress Start ");
		Address address = new Address();
		boolean sessionFlag = false;
		try{
		 // check session 
		if(session== null){
			// create new session
		  session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}
		address = (Address)ObjectTransfer.transferObjects(addressPojo, address);
		address.setAddressEffectiveDate(new Date());
		/* addCreatedBy is set via from user  address.setAddCreatedBy(addCreatedBy);*/
		address = (Address)new VelosUtil().setAuditableInfo(address);
		if(address.getAddressId()!=null)
		{session.merge(address);
		}else{
			session.save(address);
		}
		addressPojo = (AddressPojo)ObjectTransfer.transferObjects(address, addressPojo);
		if(sessionFlag){
			// close session, if session created in this method
			session.getTransaction().commit();
		}
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}
		finally{
			if(session.isOpen()){
				session.close();
			}
		}
		//log.debug("AddressHelper : saveAddress Method End ");
		return addressPojo;
	}
    
    public static AddressPojo getAddress(AddressPojo addressPojo){
		return getAddress(addressPojo,null);
	}
	
    public static AddressPojo getAddress(AddressPojo addressPojo , Session session){
    	//log.debug("getAddress with session");
		//log.debug("AddressHelper : getAddress Start ");
		Address address = new Address();
		boolean sessionFlag = false;
		try{
		 // check session 
		if(session== null){
			// create new session
		  session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}
		address = (Address)ObjectTransfer.transferObjects(addressPojo, address);		
		/*address = (Address)new VelosUtil().setAuditableInfo(address);*/
		if(address.getAddressId()!=null)
		{
			String sql = "From Address where addressId="+address.getAddressId();
			List<Address> list = (List<Address>)session.createQuery(sql).list();
			if(list!=null){
				address = list.get(0);
			}
		}
		addressPojo = (AddressPojo)ObjectTransfer.transferObjects(address, addressPojo);
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}
		finally{
			if(session.isOpen()){
				session.close();
			}
		}
		//log.debug("AddressHelper : getAddress Method End ");
		return addressPojo;
	}
}
