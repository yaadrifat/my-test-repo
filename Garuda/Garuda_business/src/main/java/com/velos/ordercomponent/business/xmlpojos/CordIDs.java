package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


public class CordIDs {

	private String nmdp_cbuid;
	private String ISBT;
	private String EXLCL;
	@XmlElement(name="nmdp_cbuid",required=true)
	public String getNmdp_cbuid() {
		return nmdp_cbuid;
	}
	public void setNmdp_cbuid(String nmdpCbuid) {
		nmdp_cbuid = nmdpCbuid;
	}
	@XmlElement
	public String getISBT() {
		return ISBT;
	}
	public void setISBT(String iSBT) {
		ISBT = iSBT;
	}
	@XmlElement(name="EXLCL",required=true)
	public String getEXLCL() {
		return EXLCL;
	}
	public void setEXLCL(String eXLCL) {
		EXLCL = eXLCL;
	}
	
	
}
