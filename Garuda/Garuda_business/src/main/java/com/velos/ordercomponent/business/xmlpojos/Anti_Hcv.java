package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlJavaTypeAdapter(type=XMLGregorianCalendar.class,value=XMLGeorgarianCalanderAdapter.class)
public class Anti_Hcv {
    private String anti_hcv_react_ind;
    private XMLGregorianCalendar anti_hcv_date;
    private String cms_cert_lab_ind;
    private String fda_lic_mfg_inst_ind;
    
    @XmlElement(name="anti_hcv_react_ind")
	public String getAnti_hcv_react_ind() {
		return anti_hcv_react_ind;
	}
	public void setAnti_hcv_react_ind(String antiHcvReactInd) {
		anti_hcv_react_ind = antiHcvReactInd;
	}
	@XmlElement(name="anti_hcv_date")
	public XMLGregorianCalendar getAnti_hcv_date() {
		return anti_hcv_date;
	}
	public void setAnti_hcv_date(XMLGregorianCalendar antiHcvDate) {
		anti_hcv_date = antiHcvDate;
	}
	@XmlElement(name="cms_cert_lab_ind")
	public String getCms_cert_lab_ind() {
		return cms_cert_lab_ind;
	}
	public void setCms_cert_lab_ind(String cmsCertLabInd) {
		cms_cert_lab_ind = cmsCertLabInd;
	}
	@XmlElement(name="fda_lic_mfg_inst_ind")
	public String getFda_lic_mfg_inst_ind() {
		return fda_lic_mfg_inst_ind;
	}
	public void setFda_lic_mfg_inst_ind(String fdaLicMfgInstInd) {
		fda_lic_mfg_inst_ind = fdaLicMfgInstInd;
	}
  
  
}
