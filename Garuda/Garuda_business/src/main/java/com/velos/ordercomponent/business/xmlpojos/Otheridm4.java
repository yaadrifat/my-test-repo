package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlJavaTypeAdapter(type=XMLGregorianCalendar.class,value=XMLGeorgarianCalanderAdapter.class)
public class Otheridm4 {
	private String other_idm_4_react_ind;
	private String other_idm_4_react_test;
	private XMLGregorianCalendar other_idm_4_react_date;
	@XmlElement(name="other_idm_4_react_ind")	
	public String getOther_idm_4_react_ind() {
		return other_idm_4_react_ind;
	}
	public void setOther_idm_4_react_ind(String otherIdm_4ReactInd) {
		other_idm_4_react_ind = otherIdm_4ReactInd;
	}
	@XmlElement(name="other_idm_4_react_test")	
	public String getOther_idm_4_react_test() {
		return other_idm_4_react_test;
	}
	public void setOther_idm_4_react_test(String otherIdm_4ReactTest) {
		other_idm_4_react_test = otherIdm_4ReactTest;
	}
	@XmlElement(name="other_idm_4_react_date")	
	@XmlJavaTypeAdapter(type=XMLGregorianCalendar.class,value=XMLGeorgarianCalanderAdapter.class)
	public XMLGregorianCalendar getOther_idm_4_react_date() {
		return other_idm_4_react_date;
	}
	public void setOther_idm_4_react_date(XMLGregorianCalendar otherIdm_4ReactDate) {
		other_idm_4_react_date = otherIdm_4ReactDate;
	}

	
}

