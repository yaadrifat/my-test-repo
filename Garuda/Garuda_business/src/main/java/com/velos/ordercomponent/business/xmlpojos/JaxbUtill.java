package com.velos.ordercomponent.business.xmlpojos;

import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public class JaxbUtill {

	public static XMLGregorianCalendar getDate(Date date){
		if(date == null)
            return null;
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(date);
        try {
            XMLGregorianCalendar dt = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
            dt.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
            return dt;
        } catch (DatatypeConfigurationException e) {
           //System.out.println("error in setting the date for xml");
        	e.printStackTrace();
        }
        return null;
	}
	
	public static Date getDate(XMLGregorianCalendar cal){
		if(cal == null)
            return null;
        try {
            Date dt = cal.toGregorianCalendar().getTime();
            return dt;
        } catch (Exception e) {
           //System.out.println("error in setting the date for xml");
        	e.printStackTrace();
        }
        return null;
	}
}
