package com.velos.ordercomponent.business.domain;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
@Entity
@Table(name = "ER_SPECIMEN")
@SequenceGenerator(name = "SEQ_ER_SPECIMEN", sequenceName = "SEQ_ER_SPECIMEN",allocationSize=1)    
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class Specimen extends Auditable {

	public Long pkSpecimen;
    public String specimenId;
    public String specAltId;
    public String specDesc;
    public Long specOrgQnty;
    public Long spectQuntyUnits;
    public Long  specType;
    public Long  fkStudy;
    public Date specCollDate;
    public Long fkPer;
    public Long specOwnerFkUser;
    public Long fkStorage;
    public Long fkSite;
    public Long fkVisit;
    public Long fkSchEvents;
    public String specNote;
    public Long fkSpec;
    public Long specStorageTmp;
    public Long specStorageTmpUnit;    
    public Long fkAccount;
    public Long specProcType;
    public Long fkStrgKitComp;
    public String specUserPathologist;
    public String specUserSurgeon;
    public String specUserCollectingTech;
    public String specUserProcessingTech;
    public Date specRemovalDatetime;
    public Date specFreezeDatetime;
    public Long specAnatomicSite;
    public Long specTissueSide;
    public Long specPathologyStat;
    public Long fkStorageId;
    public Long specExpectQuant;
    public Long specBaseOrigQuant;
    public Long specExpectedQUnit;
    public Long specBaseOrigQUnit;
    public Long specDisposition;
    public String specEnvtCons;
    /* private CdrCbu cdrCbu;*/


    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_ER_SPECIMEN") 
    @Column(name = "PK_SPECIMEN")
    public Long getPkSpecimen(){
    	return this.pkSpecimen;
    }
    public void setPkSpecimen( Long pkSpecimen){
      	 this.pkSpecimen = pkSpecimen;
     }

    public void setSpecimenId(String specimenId){
    	this.specimenId = specimenId;
    }
    @Column(name = "SPEC_ID")
	public String getSpecimenId(){
		return this.specimenId;
	}

	public void setSpecAltId(String specAltId){
		this.specAltId = specAltId;
	}
	@Column(name = "SPEC_ALTERNATE_ID")
	public String getSpecAltId(){
		return this.specAltId;
	}

    public void setSpecDesc(String specDesc){
    	this.specDesc = specDesc;
    }
	@Column(name = "SPEC_DESCRIPTION")
    public String getSpecDesc(){
		return this.specDesc ;
	}
    public void setSpecOrgQnty(Long specOrgQnty){
    	this.specOrgQnty = specOrgQnty;
    }
    
	@Column(name = "SPEC_ORIGINAL_QUANTITY")
    public Long getSpecOrgQnty(){
		return this.specOrgQnty;

	}

    public void setSpectQuntyUnits(Long spectQuntyUnits){
    	this.spectQuntyUnits = spectQuntyUnits;
    }
	@Column(name = "SPEC_QUANTITY_UNITS")
    public Long getSpectQuntyUnits(){
		return this.spectQuntyUnits;
	}

    public void setSpecType(Long specType){
    	this.specType = specType;
    }
	@Column(name = "SPEC_TYPE")
    public Long getSpecType(){
		return this.specType;
	}

    public void setFkStudy(Long fkStudy){
    	this.fkStudy = fkStudy;
    }
	@Column(name = "FK_STUDY")
    public Long getFkStudy(){
		return this.fkStudy;
	}

	@Column(name = "SPEC_COLLECTION_DATE")
    public Date getSpecCollDate(){
		return this.specCollDate;
	}

	public void setSpecCollDate(Date specCollectionDate) {

	        this.specCollDate = specCollectionDate;
	}

    public void setFkPer(Long fkPer) {
    	this.fkPer = fkPer;
    }
	@Column(name = "FK_PER")
	public Long getFkPer() {
		return this.fkPer;
	}

    public void setSpecOwnerFkUser(Long specOwnerUser){
    	this.specOwnerFkUser = specOwnerUser;
    }
	@Column(name = "SPEC_OWNER_FK_USER")
	public Long getSpecOwnerFkUser(){
		return this.specOwnerFkUser;
	}

    public void setFkStorage(Long fkStorage){
    	this.fkStorage= fkStorage;
    }
	@Column(name = "FK_STORAGE")
	public Long getFkStorage(){
		return this.fkStorage;
	}

    public void setFkSite( Long fkSite){
    	this.fkSite= fkSite;
    }
	@Column(name = "FK_SITE")
	public Long getFkSite(){
		return this.fkSite;
	}

    public void setFkVisit(Long fkVisit){
    	this.fkVisit= fkVisit;
    }
	@Column(name = "FK_VISIT")
	public Long getFkVisit(){
		return this.fkVisit;
	}

    public void setFkSchEvents(Long fkSchEvents){
    	this.fkSchEvents= fkSchEvents;
    }
	@Column(name = "FK_SCH_EVENTS1")
	public Long getFkSchEvents(){
		return this.fkSchEvents;
	}

	public void setSpecNote(String specNote){
    	this.specNote = specNote;
    }

	@Column(name = "SPEC_NOTES")
	public String getSpecNote(){
		return this.specNote;
	}


    public void setFkSpec(Long fkSpec){
    	this.fkSpec= fkSpec;
    }
	@Column(name = "FK_SPECIMEN")
	public Long getFkSpec(){
		return this.fkSpec;
	}

    public void setSpecStorageTmp(Long specStorageTmp){
    	this.specStorageTmp= specStorageTmp;
    }
	@Column(name = "SPEC_STORAGE_TEMP")
	public Long getSpecStorageTmp(){
		return this.specStorageTmp;
	}

    public void setSpecStorageTmpUnit(Long specStorageTmpUnit){
    	this.specStorageTmpUnit= specStorageTmpUnit;
    }
	@Column(name = "SPEC_STORAGE_TEMP_UNIT")
    public Long getSpecStorageTmpUnit(){
		return this.specStorageTmpUnit;
	}

    /*
	@Column(name = "SPECIMEN_STATUS")
    public String getSpecimenStatus() {
        return this.specimenStat;
    }

    public void setSpecimenStatus(String specimenStat) {
        this.specimenStat = specimenStat;
    }*/



   /**
    * @return Fk Account
    */
  @Column(name = "fk_account")
   public Long getFkAccount() {
       return this.fkAccount;
   }

   /**
    * @param fkAccount
    *            The Fk Account of the logged in user
    */
  public void setFkAccount(Long fkAccount) {
       this.fkAccount = fkAccount;
   }

  //KM:Added for child specimens.


   public void setSpecProcType(Long specProcType) {
	   this.specProcType = specProcType;
   }

   @Column(name ="SPEC_PROCESSING_TYPE")
   public Long getSpecProcType(){
	   return this.specProcType;
   }



   public void setFkStrgKitComp(Long fkStrgKitComp) {
	   this.fkStrgKitComp = fkStrgKitComp;
   }

   @Column(name ="FK_STORAGE_KIT_COMPONENT")
   public Long getFkStrgKitComp(){
	   return this.fkStrgKitComp;
   }

    @Column(name = "SPEC_FREEZE_TIME")
    public Date getSpecFreezeDatetime() {
    	return specFreezeDatetime;
    }
    public void setSpecFreezeDatetime(Date specFreezeDatetime) {
    	this.specFreezeDatetime = specFreezeDatetime;
    }


    @Column(name = "SPEC_REMOVAL_TIME")
    public Date getSpecRemovalDatetime() {
    	return specRemovalDatetime;
    }
    public void setSpecRemovalDatetime(Date specRemovalDatetime) {
    	this.specRemovalDatetime = specRemovalDatetime;
    }

    @Column(name = "FK_USER_COLLTECH")
    public String getSpecUserCollectingTech() {
    	return specUserCollectingTech;
    }
    public void setSpecUserCollectingTech(String specUserCollectingTech) {
    	this.specUserCollectingTech = specUserCollectingTech;
    }

    @Column(name = "FK_USER_PATH")
    public String getSpecUserPathologist() {
    	return specUserPathologist;
    }
    public void setSpecUserPathologist(String specUserPathologist) {
    	this.specUserPathologist = specUserPathologist;
    }

    @Column(name = "FK_USER_PROCTECH")
    public String getSpecUserProcessingTech() {
    	return specUserProcessingTech;
    }
    public void setSpecUserProcessingTech(String specUserProcessingTech) {
    	this.specUserProcessingTech = specUserProcessingTech;
    }

    @Column(name = "FK_USER_SURG")
    public String getSpecUserSurgeon() {
    	return specUserSurgeon;
    }
    public void setSpecUserSurgeon(String specUserSurgeon) {
    	this.specUserSurgeon = specUserSurgeon;
    }

    @Column(name = "SPEC_ANATOMIC_SITE")
    public Long getSpecAnatomicSite() {
    	return this.specAnatomicSite;
    }
    public void setSpecAnatomicSite(Long specAnatomicSite) {

    	this.specAnatomicSite = specAnatomicSite;
    }


    @Column(name ="SPEC_TISSUE_SIDE")
    public Long getSpecTissueSide(){
 	   return this.specTissueSide;
    }
    public void setSpecTissueSide(Long specTissueSide) {
 	   this.specTissueSide = specTissueSide;
    }


    @Column(name ="SPEC_PATHOLOGY_STAT")
    public Long getSpecPathologyStat(){
 	   return this.specPathologyStat;
    }
    public void setSpecPathologyStat(Long specPathologyStat) {
 	   this.specPathologyStat = specPathologyStat;
    }

	@Column(name ="FK_STORAGE_KIT")
    public Long getFkStorageId(){
 	   return this.fkStorageId;
    }
    public void setFkStorageId(Long fkStorageId) {
 	   this.fkStorageId = fkStorageId;
    }

    @Column(name = "SPEC_EXPECTED_QUANTITY")
    public Long getSpecExpectQuant(){
		return this.specExpectQuant;
	}
    public void setSpecExpectQuant(Long specExpectQuant){
    	this.specExpectQuant = specExpectQuant;
    }


    @Column(name = "spec_base_orig_quantity")
    public Long getSpecBaseOrigQuant(){
		return this.specBaseOrigQuant;
	}
    public void setSpecBaseOrigQuant(Long specBaseOrigQuant){
    	this.specBaseOrigQuant = specBaseOrigQuant;
    }


    @Column(name = "spec_expectedq_units")
    public Long getSpecExpectedQUnit(){
  	   return this.specExpectedQUnit;
    }
    public void setSpecExpectedQUnit(Long specExpectedQUnit) {
  	   this.specExpectedQUnit = specExpectedQUnit;
    }


    @Column(name = "spec_base_origq_units")
    public Long getSpecBaseOrigQUnit(){
   	   return this.specBaseOrigQUnit;
    }
    public void setSpecBaseOrigQUnit(Long specBaseOrigQUnit) {
   	   this.specBaseOrigQUnit = specBaseOrigQUnit;
    }
    
    @Column(name = "SPEC_DISPOSITION")
    public Long getSpecDisposition() {
    	return this.specDisposition;
    }

    public void setSpecDisposition(Long specDisposition) {
        this.specDisposition = specDisposition;
    }
    
    @Column(name = "SPEC_ENVT_CONS")
    public String getSpecEnvtCons() {
    	return this.specEnvtCons;
    }

    public void setSpecEnvtCons(String specEnvtCons) {
        this.specEnvtCons = specEnvtCons;
    }
   
	
	/*@OneToOne(fetch = FetchType.LAZY)
	@PrimaryKeyJoinColumn
	public CdrCbu getCdrCbu() {
		return cdrCbu;
	}
	public void setCdrCbu(CdrCbu cdrCbu) {
		this.cdrCbu = cdrCbu;
	}*/


}