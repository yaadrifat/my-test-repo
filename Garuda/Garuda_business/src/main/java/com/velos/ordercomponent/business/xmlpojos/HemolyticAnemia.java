package com.velos.ordercomponent.business.xmlpojos;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class HemolyticAnemia {

	private String hemolytic_anemia_ind;
	/*private Relations relations;
	@XmlElement(name="Relations")
	public Relations getRelations() {
		return relations;
	}
	public void setRelations(Relations relations) {
		this.relations = relations;
	}*/
	@XmlElement(name="hemolytic_anemia_ind")
	public String getHemolytic_anemia_ind() {
		return hemolytic_anemia_ind;
	}
	public void setHemolytic_anemia_ind(String hemolyticAnemiaInd) {
		hemolytic_anemia_ind = hemolyticAnemiaInd;
	}
	private List<Relations> Relations;
	@XmlElement(name="Relations")
	public List<Relations> getRelations() {
		return Relations;
	}
	public void setRelations(List<Relations> relations) {
		Relations = relations;
	}
}
