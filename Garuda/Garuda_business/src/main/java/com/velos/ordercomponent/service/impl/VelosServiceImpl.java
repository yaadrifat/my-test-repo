package com.velos.ordercomponent.service.impl;

/**mk*/
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.eres.web.user.UserJB;
import com.velos.ordercomponent.business.domain.AntigenEncoding;
import com.velos.ordercomponent.business.domain.Attachment;
import com.velos.ordercomponent.business.domain.CBB;
import com.velos.ordercomponent.business.domain.CBUStatus;
import com.velos.ordercomponent.business.domain.CBUUnitReport;
import com.velos.ordercomponent.business.domain.CBUploadInfo;
import com.velos.ordercomponent.business.domain.CodeList;
import com.velos.ordercomponent.business.domain.Page;
import com.velos.ordercomponent.business.domain.RecipientInfo;
import com.velos.ordercomponent.business.domain.Specimen;
import com.velos.ordercomponent.business.domain.TaskList;
import com.velos.ordercomponent.business.pojoobjects.AdditionalIdsPojo;
import com.velos.ordercomponent.business.pojoobjects.AddressPojo;
import com.velos.ordercomponent.business.pojoobjects.AntigenPojo;
import com.velos.ordercomponent.business.pojoobjects.AppMessagesPojo;
import com.velos.ordercomponent.business.pojoobjects.AssessmentPojo;
import com.velos.ordercomponent.business.pojoobjects.AttachmentPojo;
import com.velos.ordercomponent.business.pojoobjects.BestHLAPojo;
import com.velos.ordercomponent.business.pojoobjects.CBBPojo;
import com.velos.ordercomponent.business.pojoobjects.CBBProcedureInfoPojo;
import com.velos.ordercomponent.business.pojoobjects.CBBProcedurePojo;
import com.velos.ordercomponent.business.pojoobjects.CBUCompleteReqInfoPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUFinalReviewPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUMinimumCriteriaPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUMinimumCriteriaTempPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUUnitReportPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUUnitReportTempPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUploadInfoPojo;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.ClinicalNotePojo;
import com.velos.ordercomponent.business.pojoobjects.CordHlaExtPojo;
import com.velos.ordercomponent.business.pojoobjects.CordHlaExtTempPojo;
import com.velos.ordercomponent.business.pojoobjects.CordImportHistoryPojo;
import com.velos.ordercomponent.business.pojoobjects.CordImportProcessMessagesPojo;
import com.velos.ordercomponent.business.pojoobjects.DataModificationRequestPojo;
import com.velos.ordercomponent.business.pojoobjects.DcmsLogPojo;
import com.velos.ordercomponent.business.pojoobjects.DumnPojo;
import com.velos.ordercomponent.business.pojoobjects.EligibilityDeclPojo;
import com.velos.ordercomponent.business.pojoobjects.EligibilityNewDeclPojo;
import com.velos.ordercomponent.business.pojoobjects.EmailAttachmentPojo;
import com.velos.ordercomponent.business.pojoobjects.EntityMultipleValuesPojo;
import com.velos.ordercomponent.business.pojoobjects.EntitySamplesPojo;
import com.velos.ordercomponent.business.pojoobjects.EntityStatusPojo;
import com.velos.ordercomponent.business.pojoobjects.EntityStatusReasonPojo;
import com.velos.ordercomponent.business.pojoobjects.FinalRevUploadInfoPojo;
import com.velos.ordercomponent.business.pojoobjects.FormResponsesPojo;
import com.velos.ordercomponent.business.pojoobjects.FormVersionPojo;
import com.velos.ordercomponent.business.pojoobjects.FundingGuidelinesPojo;
import com.velos.ordercomponent.business.pojoobjects.FundingSchedulePojo;
import com.velos.ordercomponent.business.pojoobjects.GrpsPojo;
import com.velos.ordercomponent.business.pojoobjects.HLAPojo;
import com.velos.ordercomponent.business.pojoobjects.IDMPojo;
import com.velos.ordercomponent.business.pojoobjects.LabMaintPojo;
import com.velos.ordercomponent.business.pojoobjects.LabTestGrpPojo;
import com.velos.ordercomponent.business.pojoobjects.LabTestPojo;
import com.velos.ordercomponent.business.pojoobjects.NotificationPojo;
import com.velos.ordercomponent.business.pojoobjects.OrderAttachmentPojo;
import com.velos.ordercomponent.business.pojoobjects.OrderPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.pojoobjects.PatLabsPojo;
import com.velos.ordercomponent.business.pojoobjects.PatientHLAPojo;
import com.velos.ordercomponent.business.pojoobjects.PersonPojo;
import com.velos.ordercomponent.business.pojoobjects.SearchPojo;
import com.velos.ordercomponent.business.pojoobjects.SitePojo;
import com.velos.ordercomponent.business.pojoobjects.TempHLAPojo;
import com.velos.ordercomponent.business.pojoobjects.UserAlertsPojo;
import com.velos.ordercomponent.business.pojoobjects.UserPojo;
import com.velos.ordercomponent.helper.AddressHelper;
import com.velos.ordercomponent.helper.AppMessagesHelper;
import com.velos.ordercomponent.helper.AssessmentHelper;
import com.velos.ordercomponent.helper.AttachmentHelper;
import com.velos.ordercomponent.helper.AuditTrailHelper;
import com.velos.ordercomponent.helper.CBBHelper;
import com.velos.ordercomponent.helper.CBUHelper;
import com.velos.ordercomponent.helper.CBUUnitReportHelper;
import com.velos.ordercomponent.helper.ClinicalNoteHelper;
import com.velos.ordercomponent.helper.CodelstHelper;
import com.velos.ordercomponent.helper.CordImportHelper;
import com.velos.ordercomponent.helper.DataEntryHelper;
import com.velos.ordercomponent.helper.DataModificationRequestHelper;
import com.velos.ordercomponent.helper.DynaFormHelper;
import com.velos.ordercomponent.helper.EresFormsHelper;
import com.velos.ordercomponent.helper.LabTestHelper;
import com.velos.ordercomponent.helper.MailHelper;
import com.velos.ordercomponent.helper.OpenTaskHelper;
import com.velos.ordercomponent.helper.OrderAttachmentHelper;
import com.velos.ordercomponent.helper.PersonHelper;
import com.velos.ordercomponent.helper.ReviewUnitReportHelper;
import com.velos.ordercomponent.helper.SampleInventHelper;
import com.velos.ordercomponent.helper.SpecimenHelper;
import com.velos.ordercomponent.helper.TaskListHelper;
import com.velos.ordercomponent.helper.UploadDocHelper;
import com.velos.ordercomponent.helper.UserAlertsHelper;
import com.velos.ordercomponent.helper.VelosSearchHelper;
import com.velos.ordercomponent.helper.WidgetHelper;
import com.velos.ordercomponent.helper.velosHelper;
import com.velos.ordercomponent.service.VelosService;
import com.velos.ordercomponent.business.domain.FinalRevUploadInfo;

public class VelosServiceImpl implements VelosService {
	public static VelosService velosService;
	public static final Log log = LogFactory.getLog(VelosServiceImpl.class);

	private VelosServiceImpl() {

	}

	public static VelosService getService() throws Exception {
		log.debug(" get service ");
		try {
			if (velosService == null) {
				velosService = (VelosService) new VelosServiceImpl();
			}
		} catch (Exception e) {
			throw new RuntimeException();
		}
		return velosService;
	}

	public SearchPojo velosSearch(SearchPojo searchPojo) {
		return VelosSearchHelper.getSearchlst(searchPojo); 
	}
	
	public List getRevokeUnitReportLst(String cordID,PaginateSearch paginateSearch,int i, String condition, String orderBy){
		return CBUUnitReportHelper.getRevokeUnitReportLst(cordID,paginateSearch,i, condition, orderBy);
	}
	public List getUploadedDocumentLst(String cordID){
		return CBUUnitReportHelper.getUploadedDocumentLst(cordID);
	}

	public List<Object> getCBUCordInfoList(String objectName, String criteria,PaginateSearch paginateSearch) {
		return CBUHelper.getCBUCordInfoList(objectName, criteria,paginateSearch);
	}

	public Object getMailConfigurationByCode(String code) {
		return MailHelper.getMailConfigurationByCode(code);
	}

	public NotificationPojo sendEmail(NotificationPojo notificationPojo,Boolean fromUI) {
		return MailHelper.sendEmail(notificationPojo,fromUI);
	}

	@Override
	public List<CodeList> getCodelst(String parentcode) {
		// TODO Auto-generated method stub
		return CodelstHelper.getCodelst(parentcode);
	}

	@Override
	public Map getCodelistValues(Map codelstParams) {
		log.debug("getCodelistValues-VelosServiceImpl");
		return CodelstHelper.getCodelistValues(codelstParams);
	}

	public CBUUnitReportPojo saveCBUUnitReport(
			CBUUnitReportPojo cbuUnitReportPojo) {
		return CBUUnitReportHelper.saveCBUUnitReport(cbuUnitReportPojo);
	}

	public CordHlaExtPojo saveHlaInfo(CordHlaExtPojo cordHlaExtPojo) {
		return CBUUnitReportHelper.saveHlaInfo(cordHlaExtPojo);

	}

	public CBUUnitReportTempPojo saveCBUUnitTempReport(
			CBUUnitReportTempPojo cbuUnitReportTempPojo) {
		return CBUUnitReportHelper.saveCBUUnitTempReport(cbuUnitReportTempPojo);
	}

	public CordHlaExtTempPojo saveHlaTempInfo(
			CordHlaExtTempPojo cordHlaExtTempPojo) {
		return CBUUnitReportHelper.saveHlaTempInfo(cordHlaExtTempPojo);

	}
	@Override
	public String getCBUStatusCode(String pkcbustatus){
		return OrderAttachmentHelper.getCBUStatusCode(pkcbustatus);
	}

	public Object getObject(String objectName, String id, String fieldName)
			throws Exception {
		int i = 0;
		return velosHelper.getObject(objectName, id, fieldName, i);
	}

	public Object getCbuInfo(String objectName, String id, String fieldName)
			throws Exception {

		return CBUUnitReportHelper.getCbuInfo(objectName, id, fieldName);

	}

	public AttachmentPojo addAttachments(AttachmentPojo attachmentPojo) {
		return AttachmentHelper.addAttachments(attachmentPojo);
	}

	public List<AttachmentPojo> addMultipleAttachments(
			List<AttachmentPojo> attachmentpojos) {
		return AttachmentHelper.addMultipleAttachments(attachmentpojos);
	}

	public CodeList getCodeListByCode(String codetype, String codesubtype) {
		return CodelstHelper.getCodeListByCode(codetype, codesubtype);
	}

	public List<CBUUnitReportTempPojo> getCordExtendedTempInfoData(
			Long extInfoId) {
		return ReviewUnitReportHelper.getCordExtendedTempInfoData(extInfoId);
	}

	public List<CordHlaExtTempPojo> getCordExtendedHlaTempData(Long hlaExtId) {
		return ReviewUnitReportHelper.getCordExtendedHlaTempData(hlaExtId);
	}

	public List<Page> getWidgetInfo() {
		return WidgetHelper.getWidgetInfo();
	}

	@Override
	public CBUUnitReportTempPojo addReviewUnitReport(
			CBUUnitReportTempPojo cbuUnitReportTempPojo1) throws Exception {
		// TODO Auto-generated method stub
		return ReviewUnitReportHelper
				.addReviewUnitReport(cbuUnitReportTempPojo1);
	}

	@Override
	public CordHlaExtTempPojo addReviewHlaUnitReport(
			CordHlaExtTempPojo cordHlaExtPojo) {
		// TODO Auto-generated method stub
		return ReviewUnitReportHelper.addReviewHlaUnitReport(cordHlaExtPojo);
	}

	public List addTaskList(TaskList taskList1, TaskList taskList2) {

		return TaskListHelper.getTaskLists(taskList1, taskList2);
	}

	public OrderAttachmentPojo saveOrderAttachment(
			OrderAttachmentPojo orderAttachmentPojo) {

		return OrderAttachmentHelper.saveOrderAttachment(orderAttachmentPojo);
	}

	public Long getCodeListPkIds(String parentCode, String code)
			throws Exception {
		return CodelstHelper.getCodelstPrimaryId(parentCode, code);
	}

	public CdrCbuPojo updateCodeListStatus(CdrCbuPojo cdrCbuPojo)
			throws Exception {
		return CBUHelper.updateCodeList(cdrCbuPojo);
	}
	
	public List<Object> getMultipleIneligibleReasons(Long pkEntityStatus,PaginateSearch paginateSearch,int i) {
		return CBUHelper.getMultipleIneligibleReasons(pkEntityStatus,paginateSearch,i);
	}
	
	public EntityStatusPojo updateEntityStatus(EntityStatusPojo entityStatusPojo,Boolean fromUI) throws Exception{
		return CBUHelper.saveOrUpdateEntityStatus(entityStatusPojo,fromUI);
	}
	
	public EntityStatusReasonPojo updateEntityStatusReason(EntityStatusReasonPojo entityStatusReasonPojo,Boolean fromUI) throws Exception{
		return CBUHelper.saveOrUpdateEntityStatusReason(entityStatusReasonPojo,fromUI);
	}

	@Override
	public Map<Long, CBB> getCBBMapWithIds() {
		// TODO Auto-generated method stub
		return CBBHelper.getCBBMapWithIds();
	}

	public List<SitePojo> getCBBDetailsByIds(Long[] ids) {
		return CBBHelper.getCBBDetailsByIds(ids);
	}

	@Override
	public CdrCbuPojo getCordInfoById(CdrCbuPojo cdrCbuPojo) {
		// TODO Auto-generated method stub
		return CBUHelper.getCordInfoById(cdrCbuPojo);
	}

	public String getQueryForOpenOrderList(CdrCbuPojo cdrCbuPojo)
			throws Exception {

		return OrderAttachmentHelper.getQueryForOpenOrderList(cdrCbuPojo);
	}

	public String queryForCategoryList() throws Exception {
		return OrderAttachmentHelper.queryForCategoryList();

	}

	public String populateDropdownValues(Long selVal) throws Exception {
		return OrderAttachmentHelper.populateDropdownValues(selVal);
	}

	public Object getCordId(String objectName, String id, String fieldName)
			throws Exception {

		return OrderAttachmentHelper.getCordId(objectName, id, fieldName);

	}

	public CBUploadInfoPojo  saveCatUploadInfo(CBUploadInfoPojo cbUploadInfoPojo) {

		return OrderAttachmentHelper.saveCatUploadInfo(cbUploadInfoPojo);
	}

	public LabMaintPojo saveNewLabTestInfo(LabMaintPojo labMaintPojo) {

		return OrderAttachmentHelper.saveNewLabTestInfo(labMaintPojo);
	}

	public List<UserAlertsPojo> saveUserAlert(List<UserAlertsPojo> userAlertsLst) {

		return UserAlertsHelper
				.insertUserAlerts(userAlertsLst);
	}

	public List getUserAlerts(Long userid,String alertType) {

		return UserAlertsHelper.getUserAlerts(userid,alertType);
	}

	@Override
	public List<SitePojo> getCBBDetails(Long id) {
		// TODO Auto-generated method stub
		return CBBHelper.getCBBDetails(id);
	}

	@Override
	public AddressPojo saveAddress(AddressPojo addressPojo) {
		// TODO Auto-generated method stub
		return AddressHelper.saveAddress(addressPojo);
	}

	@Override
	public PersonPojo savePerson(PersonPojo personPojo) {
		// TODO Auto-generated method stub
		return PersonHelper.savePerson(personPojo);
	}

	@Override
	public PersonPojo getPersonById(PersonPojo personPojo) {
		// TODO Auto-generated method stub
		return PersonHelper.getPersonById(personPojo);
	}

	@Override
	public CBBPojo saveCBBDefaults(CBBPojo cbbPojo) {
		// TODO Auto-generated method stub
		return CBBHelper.saveCBBDefaults(cbbPojo);
	}
	@Override
	public List getcbuFlaggedItems(Long cordId){
		return CBUHelper.getcbuFlaggedItems(cordId);
	}

	public String getAttachments(Long entityId, Long entityTypeId,
			Long attachmentType, String categoryName) throws Exception {

		return UploadDocHelper.getAttachments(entityId, entityTypeId,
				attachmentType, categoryName);
	}

	public String getEntityDocuments(Long entityId, Long entityTypeId,
			Long attachmentType) throws Exception {

		return UploadDocHelper.getEntityDocuments(entityId, entityTypeId,
				attachmentType);
	}

	public Long getCbuPkCordId(CdrCbuPojo cdrCbuPojo) throws Exception {

		return UploadDocHelper.getCbuPkCordId(cdrCbuPojo);
	}

	public String getOtherTestAttachInfo(Long entityId, Long entityTypeId,
			Long attachmentType, String categoryName) throws Exception {

		return UploadDocHelper.getOtherTestAttachInfo(entityId, entityTypeId,
				attachmentType, categoryName);
	}
	public String getHealthHistoryInfo(Long entityId, Long entityTypeId,
			Long attachmentType, String categoryName) throws Exception {
		return UploadDocHelper.getHealthHistoryInfo(entityId, entityTypeId,
				attachmentType, categoryName);
	}
	
	public String getEligibilityAttachInfo(Long entityId, Long entityTypeId,
			Long attachmentType, String categoryName) throws Exception {

		return UploadDocHelper.getHealthHistoryInfo(entityId, entityTypeId,
				attachmentType, categoryName);
	}
	

	public List<CBBProcedurePojo> getCBBProcedure(Long id,String siteIds,PaginateSearch paginateSearch,int i) {
		// TODO Auto-generated method stub
		return CBBHelper.getCBBProcedure(id,siteIds,paginateSearch,i);
	}
	
	public List<CBBProcedurePojo> getCBBProcedureSort(Long id,String siteIds,PaginateSearch paginateSearch,int i,String filterCondtion,String orderBy) {
		// TODO Auto-generated method stub
		return CBBHelper.getCBBProcedureSort(id,siteIds,paginateSearch,i,filterCondtion,orderBy);
	}

	public List<CBBProcedureInfoPojo> getCBBProcedureInfo(String id) {
		// TODO Auto-generated method stub
		return CBBHelper.getCBBProcedureInfo(id);
	}
	
	public List getCBBProcedureCount(String siteIds,String procedureCondi,int i){
		return CBBHelper.getCBBProcedureCount(siteIds, procedureCondi, i);
	}

	public CBBProcedureInfoPojo saveCBBProcedureDetails(
			CBBProcedureInfoPojo cbbProcedureInfoPojo) {
		// TODO Auto-generated method stub
		log.debug("in cbb service");
		return CBBHelper.saveCBBProcedureDetails(cbbProcedureInfoPojo);
	}

	public CBBProcedurePojo saveCBBProcedure(CBBProcedurePojo cbbProcedurePojo) {
		// TODO Auto-generated method stub
		log.debug("in cbb service");
		return CBBHelper.saveCBBProcedure(cbbProcedurePojo);
	}

	public List<Object> getCbuinfo(String criteria,PaginateSearch paginateSearch,int i) {
		return CBUHelper.getCbuinfo(criteria,paginateSearch,i);
	}
	public List<Object> getCbuInfoById(String entityType, String entityValue){
		return CBUHelper.getCbuInfoById(entityType,entityValue);
	}

	public List<Object> getListCdrCBB(String criteria,PaginateSearch paginateSearch,int i) {
		return CBUHelper.getListCdrCBB(criteria,paginateSearch,i);
	}

	public List<Object> getHla(Long cordId,PaginateSearch paginateSearch,int i) {
		return CBUHelper.getHla(cordId,paginateSearch,i);
	}
	
	public List<Object>	getStatusHistory(Long entityID,Long entityType,String statusTypeCode,PaginateSearch paginateSearch,int i){
		return CBUHelper.getStatusHistory(entityID,entityType,statusTypeCode,paginateSearch,i);
	}

	public List<Object> getCbuAttachInfo(String cbuid) {
		return ReviewUnitReportHelper.getCbuAttachInfo(cbuid);
	}

	public String getcordinfopk() throws Exception {
		return CBUUnitReportHelper.getcordinfopk();

	}

	public CBBProcedureInfoPojo updateCbbProcedureInfo(
			CBBProcedureInfoPojo cbbProcedureInfoPojo,
			CBBProcedurePojo cbbProcedurePojo) {
		return CBBHelper.updateCBBProcedureInfo(cbbProcedureInfoPojo,
				cbbProcedurePojo);
	}

	public List<Object> getdataentryCbuAttachInfo(String cbuid) {
		return DataEntryHelper.getdataentryCbuAttachInfo(cbuid);

	}

	public List<Object> queryForCbuList(CdrCbuPojo cdrCbuPojo) {
		return OpenTaskHelper.queryForCbuList(cdrCbuPojo);
	}

	public List getUserId(String loginId) {
		return OpenTaskHelper.getUserId(loginId);

	}

	@SuppressWarnings("unchecked")
	public List<Object> getUser() {
		return OpenTaskHelper.getUser();
	}

	public List<Object> getTask(String loginId, String listid) {
		return OpenTaskHelper.getTask(loginId, listid);
	}

	public UserPojo getUserById(UserPojo userPojo) {
		return UserAlertsHelper.getUserByid(userPojo);
	}
	
	public String getLogNameByUserId(Long userId) {
		return UserAlertsHelper.getLogNameByUserId(userId);
	}
	
	public AddressPojo upadateuserMailId(AddressPojo addressPojo) {
		return UserAlertsHelper.updateUserMailId(addressPojo);
	}
	
	public Long getDcmsAttachmentId() {
		log.debug("in serviceImplllllllllllllLLLLLLLLLLLLLLLLL");
		return AttachmentHelper.getDcmsAttachmentId();
	}
	
	public String getDcmsFlagMethod(String flagValue) {
		log.debug("in serviceImplllllllllllllLLLLLLLLLLLLLLLLL");
		return AttachmentHelper.getDcmsFlagMethod(flagValue);
	}
	
	public DcmsLogPojo addDcmsLog(DcmsLogPojo dcmsLogPojo){
		return AttachmentHelper.addDcmsLog(dcmsLogPojo);
	}

	@Override
	public List<AdditionalIdsPojo> getCordInfoAdditionalIds(
			Long cordId) {
		return CBUHelper.getCordInfoAdditionalIds(cordId);
	}

	@Override
	public List<AdditionalIdsPojo> saveCordInfoAdditionalIds(
			List<AdditionalIdsPojo> additionalIdsPojo,Boolean fromUI) {
		return CBUHelper.saveCordInfoAdditionalIds(additionalIdsPojo,fromUI);
	}

	@Override
	public CdrCbuPojo saveOrUpdateCordInfo(CdrCbuPojo cdrCbuPojo,Boolean fromUI) {
		return CBUHelper.saveOrUpdateCordInfo(cdrCbuPojo,fromUI);
	}

	@Override
	public List<HLAPojo> saveOrUpdateHlas(List<HLAPojo> hlaPojos,Boolean fromUI) {
		return CBUHelper.saveOrUpdateHlas(hlaPojos,fromUI);
	}
	
	@Override
	public List getResultsForFilter(String orderType,String colName,String colData,String pkOrderType,String pending,String order_status){
		return OrderAttachmentHelper.getResultsForFilter(orderType, colName, colData,pkOrderType,pending,order_status);
	}

	@Override
	public Specimen saveOrUpdateSpecimen(Specimen specimenPojo,Boolean fromUI) {
		return SpecimenHelper.saveOrUpdateSpecimen(specimenPojo,fromUI) ;
	}

	@Override
	public Boolean getValidateDbData(String entityName, String entityFiled,
			String entityValue , String entityField1, String entityValue1) {
		return CodelstHelper.getValidateDbData(entityName, entityFiled, entityValue , entityField1, entityValue1);
	}
	
	@Override
	public Boolean getValidateDbLongData(String entityName, String entityFiled,
			String entityValue , String entityField1, Long entityValue1) {
		return CodelstHelper.getValidateDbLongData(entityName, entityFiled, entityValue , entityField1, entityValue1);
	}

	@Override
	public List<SitePojo> getSites() {
		return CBBHelper.getSites();
	}
	
	@Override
	public List<SitePojo> getSitesWithPagination(PaginateSearch paginateSearch,int i) {
		return CBBHelper.getSitesWithPagination(paginateSearch,i);
	}
	
	@Override
	public List<SitePojo> getSitesWithPaginationSort(PaginateSearch paginateSearch,int i, String condition,String orderby) {
		return CBBHelper.getSitesWithPaginationSort(paginateSearch,i,condition,orderby);
	}
	
	@Override
	public List<SitePojo> getSitesByCriteria(String criteria,PaginateSearch paginateSearch,int i) {
		return CBBHelper.getSitesByCriteria(criteria, paginateSearch, i);
	}
	
	public List getSitesCount(PaginateSearch paginateSearch,int i,String condition){
		return CBBHelper.getSitesCount(paginateSearch,i,condition);
	}
	
	@Override
	public List<GrpsPojo> getGroups(Long siteId) {
		return CBBHelper.getGroups(siteId);
	}
	
	@Override
	public List<CdrCbuPojo> getCordsInProgress(Long cordId) {
		return CBUHelper.getCordsInProgress(cordId);
	}

	@Override
	public List<HLAPojo> getHlasByCord(Long cordId, Long entityType, String criteria) {
		return CBUHelper.getHlasByCord(cordId,entityType, criteria);
	}	
	
	public List getOrderDetails(String orderId){
		return OrderAttachmentHelper.getOrderDetails(orderId);
	}
	
	public List getCtOrderDetails(String orderId){
		return OrderAttachmentHelper.getCtOrderDetails(orderId);
	}

	public ClinicalNotePojo saveClinicalNotes(
			ClinicalNotePojo clinicalNotePojo) {
		log.debug("IN Service implementation !!!!!!!!!!!");
		return ClinicalNoteHelper.saveClinicalNotes(clinicalNotePojo);
	}
	public CdrCbuPojo submitCordStatus(CdrCbuPojo cdrCbuPojo){
		return OrderAttachmentHelper.submitCordStatus(cdrCbuPojo);
	}
	public List getResultsForCancelFilter(String orderType,String colName,String colData,String pkOrderType,String cancelled,String order_status){
		return OrderAttachmentHelper.getResultsForCancelFilter(orderType, colName, colData,pkOrderType,cancelled,order_status);
	}

	@Override
	/*public List<ClinicalNotePojo> clinicalNoteHistory(Long cordId) {
		// TODO Auto-generated method stub
		return ClinicalNoteHelper.clinicalNoteHistory(cordId);
	}*/
	
	public  List<ClinicalNotePojo> amendNotes(Long pkNotes)  {
		log.debug("Inside implementation for revoke!!!!!!!!!!");
		return ClinicalNoteHelper.amendNotes(pkNotes);
	}
	
	@Override
	public CBB getCBBBySiteId(Long siteId) {
		System.out.println("INside service implementation");
		return CBBHelper.getCBBBySiteId(siteId);
	}

	@Override
	public CBUMinimumCriteriaPojo saveOrConfirmCriteria(CBUMinimumCriteriaPojo cbuMinimumCriteriaPojo)
	{
		return CBUHelper.saveOrConfirmCriteria(cbuMinimumCriteriaPojo);
	}
	@Override
	public CBUMinimumCriteriaTempPojo saveMinimumCriteriaTemp(CBUMinimumCriteriaTempPojo minimumCriteriaTempPojo){
		return CBUHelper.saveMinimumCriteriaTemp(minimumCriteriaTempPojo);
	}
	@Override
	public List getPkOrders(Long cordId){
		return OrderAttachmentHelper.getPkOrders(cordId);
	}
	@Override
	public List getpatientList(Long fkorderid,Long fkcordid)
	{
		return OrderAttachmentHelper.getpatientList(fkorderid, fkcordid);
	}
	@Override
	public List getShipmentDetails(Long fkorderid,Long fkcordid)
	{
		return OrderAttachmentHelper.getShipmentDetails(fkorderid, fkcordid);
	}
	
	@Override
	public IDMPojo saveIDMValues(IDMPojo idmPojo){
		return CBUHelper.saveIDMValues(idmPojo);
	}
	
	public OrderPojo updateAcknowledgement(OrderPojo orderpojo){
		return OrderAttachmentHelper.updateAcknowledgement(orderpojo);
	}
	
	public void submitCtResolution(String pkorderId,String availDt,String resolbyCbb,String resolbyTc,String ackTcResolution,String acceptToCancel,String pkTempunavail,String pkcordId,String rejectDt,String rejectBy,String userID){
		OrderAttachmentHelper.submitCtResolution(pkorderId,availDt,resolbyCbb,resolbyTc,ackTcResolution,acceptToCancel,pkTempunavail,pkcordId,rejectDt,rejectBy,userID);
	}
		
	public void submitHeResolution(String pkorderId,String availDt,String resolbyCbb,String resolbyTc,String ackTcResolution,String pkTempunavail){
		OrderAttachmentHelper.submitHeResolution(pkorderId,availDt,resolbyCbb,resolbyTc,ackTcResolution,pkTempunavail);
	}

	@Override
	public List<EntityStatusReasonPojo> getReasonsByEntityStatusId(
			Long entityStatusId) {
		return CBUHelper.getReasonsByEntityStatusId(entityStatusId);
	}
	

	@Override
	public List getEresFormHtml(Long formId, Long cordId) {
		return EresFormsHelper.getEresFormHtml(formId, cordId);
	}
	
	public List<Attachment> getAttachmentInfoLst(Long attachmentId){
		return AttachmentHelper.getAttachmentInfoLst(attachmentId);
	}
	public List<CBUUnitReport> getCbuUnitReportLst( Long cordId, Long attachmentId){
		return CBUUnitReportHelper.getCbuUnitReportLst(cordId,attachmentId);
	}
	
	public List<CBUploadInfo> getCbUploadInfoLst(Long attachmentId){
		return UploadDocHelper.getCbUploadInfoLst(attachmentId);
	}
	
	
	public String checkShedShipDt(String orderid,String cbuid,String pkavailable) {
		return OrderAttachmentHelper.checkShedShipDt(orderid,cbuid,pkavailable);
	}	
	
	public SitePojo getSiteBySiteId(Long siteId){
		return CBBHelper.getSiteBySiteId(siteId);
	}

	@Override
	public SitePojo saveSite(SitePojo sitePojo) {
		return CBBHelper.saveSite(sitePojo);
	}

	@Override
	public CBUFinalReviewPojo saveCBUFinalReview(
			CBUFinalReviewPojo cbuFinalReviewPojo) {
		return CBUHelper.saveCbuFinalReview(cbuFinalReviewPojo);
	}

	@Override
	public List<EmailAttachmentPojo> saveEmailDocuments(
			List<EmailAttachmentPojo> pojos) {
		return MailHelper.saveEmailDocuments(pojos);
	}

	@Override
	public EntityStatusReasonPojo loadEntityStatusReason(
			EntityStatusReasonPojo entityStatusReasonPojo) {
		return CBUHelper.loadEntityStatusReason(entityStatusReasonPojo);
	}

	@Override
	public Map<String, List> getCordUploadDocument(Long attachmentType,
			Long entityId, Long entityType) {
		return CBUHelper.getCordUploadDocument(attachmentType, entityId, entityType);
	}
	
	public EntitySamplesPojo getEntitySampleInfo(Long entityId){
		return SampleInventHelper.getEntitySampleInfo(entityId);
	}
	
	public EntitySamplesPojo saveOrUpdateSampInfo(EntitySamplesPojo entitySamplesPojo,Boolean fromUI) {
		return SampleInventHelper.saveOrUpdateSampInfo(entitySamplesPojo,fromUI);
	}
	
	public String getNotesByCategory(Long entityId, Long entityTypeId,
			Long categoryTypeID, String categoryName) throws Exception {

		return ClinicalNoteHelper.getNotesByCategory(entityId, entityTypeId,
				categoryTypeID, categoryName);
	}
	public ClinicalNotePojo viewClinicalNotes(
			Long pkNotes) {
		log.debug("IN view clinical notes Service implementation !!!!!!!!!!!");
		return ClinicalNoteHelper.viewClinicalNotes(pkNotes);
	}

	@Override
	public List getTestsForIDM(Long fkSpecId) {
		return CBUHelper.getTestsForIDM(fkSpecId);
	}
	
	@Override
	public List getDynaFormLst(Long cordId,String formName,String formvers,Long fkfrmversion) {
		return DynaFormHelper.getDynaFormLst(cordId,formName,formvers,fkfrmversion);
	}
	@Override
	public void getOldFormVersion(Long entityId,String formName,boolean formUI){
		DynaFormHelper.getOldFormVersion(entityId,formName,formUI);
	}
	@Override
	public List<Object> getPrevAssessData(Long entityId,Long fkformversion){
		return DynaFormHelper.getPrevAssessData(entityId,fkformversion);
	}
	
	@Override
	public List getFormVersionLst(Long cordId,String formName){
		return DynaFormHelper.getFormVersionList(cordId,formName);
	}
	
	@Override
	public String getLatestFormVer(String formName){
		return DynaFormHelper.getLatestFormVer(formName);
	}
	@Override
	public FormVersionPojo saveFormVersion(FormVersionPojo formVersionPojo,boolean formUI){
		return DynaFormHelper.saveFormVersion(formVersionPojo,formUI);
	}
	
	@Override
	public List getIdmDeferCondition(String formName) {
		return DynaFormHelper.getIdmDeferCondition(formName);
	}
	
	@Override
	public Long getFormseq(Long cordId,String formName,String formVers) {
		return DynaFormHelper.getFormseq(cordId,formName,formVers);
	}
	@Override
	public List getIdmResponseList(Long entityId){
		return DynaFormHelper.getIdmResponseList(entityId);
	}
	
	@Override
	public FormVersionPojo getFormVerPojo(Long cordId,String formName,String formVers,Long formseq) {
		return DynaFormHelper.getFormVerPojo(cordId,formName,formVers,formseq);
	}
	
	@Override
	public Long getformvmaxPk(Long cordId,String formName,String formvers){
		return DynaFormHelper.getformvmaxPk(cordId, formName,formvers);
	}
	
	@Override
	public Object getformvmax(Long cordId,String formName){
		return DynaFormHelper.getformvmax(cordId, formName);
	}
	
	@Override
	public List getDynaQuesLst(Long cordId,String formName) {
		return DynaFormHelper.getDynaQuesLst(cordId,formName);
	}
	
	@Override
	public List getQuestionGroup(String formName,String formVers){
		return DynaFormHelper.getQuestionGroup(formName,formVers);
	}
	
	@Override
	public List saveorupdateDynaForms(List<FormResponsesPojo> formResponsesPojo,boolean formUI){
		log.debug("saveorupdateDynaForms in ServiceIMpl");
		return DynaFormHelper.saveorupdateDynaForm(formResponsesPojo,formUI);
	}
	@Override
	public FormResponsesPojo saveFormData(FormResponsesPojo formobjects,Long entityId,String formName,boolean formUI){
		log.debug("saveFormData in ServiceIMpl");
		return DynaFormHelper.saveFormData(formobjects,entityId,formName,formUI);
	}
	@Override
	public Date getFormCreatedDate(Long cordId,String formName,String formVers){
		log.debug("saveFormData in ServiceIMpl");
		return DynaFormHelper.getFormCreatedDate(cordId,formName,formVers);
	}
	
	/*@Override
	public List getChildQuest(Long pkQuestion,String formName) {
		return DynaFormHelper.getChildQuest(pkQuestion,formName);
	}*/
	
	@Override
	public List getdataFromIDM(Long fkSpecId) {
		return CBUHelper.getdataFromIDM(fkSpecId);
	}
	
	@Override
	public Long getLabTestpkbyshname(String shname){
		return CBUHelper.getLabTestpkbyshname(shname);
	}
	
	@Override
	public List getpatlabpklist(Long fkSpecId) {
		return CBUHelper.getpatlabpklist(fkSpecId);
	}
	
	@Override
	public Long getLabgrppks(String grptype) {
		return LabTestHelper.getLabgrppks(grptype);
	}
	
	@Override
	public LabTestGrpPojo saveorupdateLTestgrp(LabTestGrpPojo labTestGrpPojo,LabTestPojo labTestPojo){
		log.debug("saveorupdateLTestgrp in service impl");
		return LabTestHelper.saveorupdateLTestgrp(labTestGrpPojo,labTestPojo);
	}
	
	public Long getLabTestInfoByName(String testName){
		log.debug("saveorupdateLTestgrp in service impl");
		return LabTestHelper.getLabTestInfoByName(testName);
	}
	
	public Long getLabTestInfoByName(String testName, String testShrtName){
		log.debug("saveorupdateLTestgrp in service impl");
		return LabTestHelper.getLabTestInfoByName(testName,testShrtName);
	}
	
	@Override
	public List getLabTestgrp(){
		log.debug("get lab Test Group in Service impl");
		return LabTestHelper.getLabtestgroup();
	}
	
	@Override
	public List getLatTestlist(Long pklabgrp){
		log.debug("get lab Test List in Service impl");
		return LabTestHelper.getLabtestlist(pklabgrp);
	}
	@Override
	public List getIDMTests() {
		return CBUHelper.getIDMTests();
	}
	
	@Override
	public Long getIDMTestcount(Long fkSpecId){
		return CBUHelper.getIDMTestcount(fkSpecId);
	}
	@Override
	public List getCbuStatus(String cbustatusin) {
		return CBUHelper.getCbuStatus(cbustatusin);
	}
	
	@Override
	public CBUCompleteReqInfoPojo saveCBUCompleteReqInfo(
			CBUCompleteReqInfoPojo cbuCompleteReqInfoPojo) {
		
		return CBUHelper.saveCBUCompleteReqInfo(cbuCompleteReqInfoPojo);
	}

	@Override
	public Map<Long, List> getNotes(Long entityId) {
		return ClinicalNoteHelper.getNotes(entityId);
	}
	
	@Override
	public Map<Long, List> getPfNotes(Long entityId,Long requestType) {
		return ClinicalNoteHelper.getPfNotes(entityId,requestType);
	}
	
	@Override
	public List<CBBProcedurePojo> getCBBProcedures(Long id) {
		return CBBHelper.getCBBProcedures(id);
	}

	@Override
	public void updateSpecimenFormResponse(Long specimenId, List<Long> pkAcctForm) {
		EresFormsHelper.updateSpecimenFormResponse(specimenId, pkAcctForm);		
	}
	
	@Override
	public List<PatLabsPojo> saveOrUpdatePatLabs(List<PatLabsPojo> patLabsPojos) {
		log.debug("save or update velos service impl ....");
		return CBUHelper.saveOrUpdatePatLabs(patLabsPojos);
	}
	
	@Override
	public List<PatLabsPojo> savePatLabs(List<PatLabsPojo> patLabsPojos) {
		return CBUHelper.savePatLabs(patLabsPojos);
	}
	public PatLabsPojo getPatLabsPojoById(Long specimenId,Long pkPatLabs) {
		log.debug("save or update velos service impl ....");
		return CBUHelper.getPatLabsPojoById(specimenId,pkPatLabs);
	}
	
	@Override
	public List<AssessmentPojo> saveUpdateAssessfromsession(List<AssessmentPojo> assessmentPojo,List patLabsPojos) {
		return AssessmentHelper.saveUpdateAssessfromsession(assessmentPojo,patLabsPojos);
	}
	@Override
	public List<AssessmentPojo> saveOrUpdateAssessmentList(List<AssessmentPojo> assessmentPojoList){
		return AssessmentHelper.saveOrUpdateAssessmentList(assessmentPojoList);
	}
	@Override
	public AssessmentPojo saveorUpdateAssessment(AssessmentPojo assessmentPojo) {
		return AssessmentHelper.saveorUpdateAssessment(assessmentPojo);
	}

	public List<PatLabsPojo> getOtherTestLst(Long specimenId, Long testId) {
		log.debug("get other lab tests in service impl ....");
		return CBUHelper.getOtherTestLst(specimenId,testId);
	}
	
	
	public List<ClinicalNotePojo> clinicalfilter(String keyword, Long type, Long cordId) {
		// TODO Auto-generated method stub
		return ClinicalNoteHelper.clinicalfilter(keyword,type,cordId);
	}

	@Override
	public List<ClinicalNotePojo> updateClinicalNotes(
			List<ClinicalNotePojo> cnPojos) {
		// TODO Auto-generated method stub
		return ClinicalNoteHelper.updateClinicalNotes(cnPojos);
	}

	@Override
	public List<EntityMultipleValuesPojo> getMultipleValuesByEntityId(
			Long entityId, String entityType,String entityMultipleType) {
		return CBUHelper.getMultipleValuesByEntityId(entityId, entityType,entityMultipleType);
	}

	@Override
	public List<EntityMultipleValuesPojo> saveMultipleEntityValue(
			List<EntityMultipleValuesPojo> entityMultipleValuesPojos,Boolean fromUI) {
		return CBUHelper.saveMultipleEntityValue(entityMultipleValuesPojos,fromUI);
	}

	@Override
	public List<EntityStatusReasonPojo> saveMultipleReasonValue(
			List<EntityStatusReasonPojo> entityStatusReasonPojo,Boolean fromUI) {
		return CBUHelper.saveMultipleReasonValue(entityStatusReasonPojo,fromUI);
	}
	
	@Override
	public void transientMultipleValues(List<Long> ids,Long entityId) {
		CBUHelper.transientMultipleValues(ids,entityId);		
	}

	@Override
	public void transientMultipleReasonValues(List<Long> ids,Long entityStatus) {
		CBUHelper.transientMultipleReasonValues(ids,entityStatus);		
	}
	
	@Override
	public String generateGuid() {
		return CBUHelper.generateGuid();
	}
	
	public Long getTestIdByName(String testName){
		return CBUHelper.getTestIdByName(testName);
	}

	@Override
	public AntigenPojo getAntigen(String genomicFormat, String locus,
			String typingMethod) {
		return CBUHelper.getAntigen(genomicFormat, locus, typingMethod);
	}
	

	@Override
	public AntigenPojo getLatestAntigen(Long antigenId) {
		return CBUHelper.getLatestAntigen(antigenId);
	}
	
	@Override
	public List getusersById(String criteria,PaginateSearch paginateSearch,int i) {
		return UserAlertsHelper.getUsersByid(criteria,paginateSearch,i);
	}
	
	
	@Override
	public List<BestHLAPojo> getBestHla(Long entityType,Long entityId) {
		return CBUHelper.getBestHla(entityType,entityId);
	}


	@Override
	public AppMessagesPojo updateAppMessages(AppMessagesPojo appMessagesPojo) {
		return AppMessagesHelper.updateAppMessages(appMessagesPojo);
	}

	@Override
	public AppMessagesPojo getAppMessagesById(AppMessagesPojo appMessagesPojo) {
		return AppMessagesHelper.getAppMessagesById(appMessagesPojo);
	}

	@Override
	public List<TempHLAPojo> getTempHla(Long entityType, String registryId) {
		return CBUHelper.getTempHla(entityType, registryId);
	}
	
	
	public void LCStoResolution(String pkresol,String ordertype,String pkorderId){
		OrderAttachmentHelper.LCStoResolution(pkresol,ordertype,pkorderId);
	}
	
	public String getPkCbuStatus(String ordertye,String codetype,String statuscode){
		return OrderAttachmentHelper.getPkCbuStatus(ordertye,codetype,statuscode);
	}
	
	@Override
	public List<ClinicalNotePojo> clinicalNoteHistory(Long cordId, Long noteCategory, Long requestId) {
		// TODO Auto-generated method stub
		return ClinicalNoteHelper.clinicalNoteHistory(cordId, noteCategory,requestId);
	}
	
	/*public String getRequestInfo(Long cordId) throws Exception {
		return ClinicalNoteHelper.getRequestInfo(cordId);
	}*/
	
	
	@Override	
	public List<SitePojo> getMultipleValuesByErSite(
			Long entityId, String entityType,String entitySubType) {
		return CBUHelper.getMultipleValuesByErSite(entityId, entityType,entitySubType);
	}
	
	public List maincbuRequestHistory(String pkcord,String pkorderId) {
		return OrderAttachmentHelper.maincbuRequestHistory(pkcord,pkorderId);
	}
	
	public List cbuRequestHistory(String pkcord,String pkorder,String cdrUser){
		return OrderAttachmentHelper.cbuRequestHistory(pkcord,pkorder,cdrUser);
	}
	
	public List cbuHistory(String pkcord,String pkorder,PaginateSearch paginationSearch,int i,String cdrUser,String condition,String orderBy){
		return OrderAttachmentHelper.cbuHistory(pkcord,pkorder,paginationSearch,i,cdrUser,condition,orderBy);
	}

	@Override
	public DumnPojo getDumnQuestionsByEntity(Long entityId, Long entityType,Long pkEligQues) {
		return CBUHelper.getDumnQuestionsByEntity(entityId, entityType,pkEligQues);
	}
	
	public String populateUserValues(Long cordId) throws Exception {
		return ClinicalNoteHelper.populateUserValues(cordId);
	}
	
	
	@Override
	public EligibilityDeclPojo getEligibilityDeclQuestionsByEntity(
			Long entityId, Long entityType,Long pkEligQues) {
		return CBUHelper.getEligibilityDeclQuestionsByEntity(entityId, entityType,pkEligQues);
	}

	@Override
	public DumnPojo saveOrUpdateDumnQuestions(DumnPojo dumnPojo) {
		return CBUHelper.saveOrUpdateDumnQuestions(dumnPojo);
	}

	@Override
	public EligibilityDeclPojo saveOrUpdateFinalEligibilityQuestions(
			EligibilityDeclPojo eDeclPojo) {
		return CBUHelper.saveOrUpdateFinalEligibilityQuestions(eDeclPojo);
	}
	
	@Override
	public AddressPojo getAddress(AddressPojo addressPojo) {
		return AddressHelper.getAddress(addressPojo);
	}

    @Override
	public List<PatientHLAPojo> getPatientHla(Long entityType, Long entityId) {
		return CBUHelper.getPatientHla(entityType, entityId);
	}
	
	public List cbbDetailPopup(String pksite,String ordType,String sampleAtlab){
		return OrderAttachmentHelper.cbbDetailPopup(pksite,ordType,sampleAtlab);
	}
	
	public String getTcResol(String orderId){
		return OrderAttachmentHelper.getTcResol(orderId);
	}
	
	public String checkForProcAssignment(String procId){
		return CBBHelper.checkForProcAssignment(procId);
	}
	
	public String getProcCordsMaxCollectionDt(String procId){
		return CBBHelper.getProcCordsMaxCollectionDt(procId);
	}
	
	
	public List getShedShipDts(String tshipDate,String siteId){
		return OrderAttachmentHelper.getShedShipDts(tshipDate,siteId);
	}
	
	public List<FundingGuidelinesPojo> getCBBGuidlinesByid(String siteId){
		return CBBHelper.getCBBGuidlinesByid(siteId);
	}
	
	public List<FundingGuidelinesPojo> updateCBBGuidlines(List<FundingGuidelinesPojo> fundingGuidelinesPojoLst){
		return CBBHelper.updateCBBGuidlines(fundingGuidelinesPojoLst);
	}
	
	public List<FundingGuidelinesPojo> getValidfundingguidelinesdate(String birthDate,String Category,String cbbId){
		return CBBHelper.getValidfundingguidelinesdate(birthDate,Category,cbbId);
	}

	public List<String> getCodelstCustCols(Long pkCodelst){
		log.debug("in service impl");
		return CodelstHelper.getCodelstCustCols(pkCodelst);
	}
	public void createDataRequest(DataModificationRequestPojo datarequestpojo){
		DataModificationRequestHelper.createDataRequest(datarequestpojo);
	}
	
	public List viewDataRequest(Long pkDatarequest,PaginateSearch paginateSearch,int i){
		return DataModificationRequestHelper.viewDataRequest(pkDatarequest,paginateSearch,i);
	}
	
	public DataModificationRequestPojo getRequestDetails(DataModificationRequestPojo datarequestpojo){
		return DataModificationRequestHelper.getRequestDetails(datarequestpojo);
		
	}
	
	public void updateDMRequestStatus(DataModificationRequestPojo datarequestpojo){
		DataModificationRequestHelper.updateDMRequestStatus(datarequestpojo);		
	}
	
	public List<LabTestPojo> getProcessingTestInfo(String groupType){
		return SampleInventHelper.getProcessingTestInfo(groupType);
	}
	
	public List<LabTestPojo> getOtherProcessingTestInfo(Long specimenId,String groupType){
		return SampleInventHelper.getOtherProcessingTestInfo(specimenId,groupType);
	}
	
	public LabTestPojo getOtrProcingTstInfoByTestId(Long testId){
		return SampleInventHelper.getOtrProcingTstInfoByTestId(testId);
	}
	
	/*public List<PatLabsPojo> getPreTestListInfo(String preGroup,Long specimenId){
		return SampleInventHelper.getPreTestListInfo(preGroup,specimenId);
	}*/
	
	public List<PatLabsPojo> getvalueSavePreTestList(List<PatLabsPojo> alist,Boolean fromUI){
		return SampleInventHelper.getvalueSavePreTestList(alist,fromUI);
	}
	
	/*public List<PatLabsPojo> getvalueSavePostTestList(List<PatLabsPojo> blist){
		return SampleInventHelper.getvalueSavePostTestList(blist);
	}*/
	
	public List<PatLabsPojo> getvalueSavePostTestList(List<PatLabsPojo> blist,Boolean fromUI){
		return SampleInventHelper.getvalueSavePostTestList(blist,fromUI);
	}

	
	public List<PatLabsPojo> getPatLabsPojoInfo(Long specimenId,Long testValueType){
		return SampleInventHelper.getPatLabsPojoInfo(specimenId,testValueType);
	}
	
	public List<SitePojo> getCBBListForSchAvail(){
		return CBBHelper.getCBBListForSchAvail();
	}
	
	public List<FundingSchedulePojo> getScheduledFundingRepLst(List<FundingSchedulePojo> fundingSchedulePojoLst,PaginateSearch paginateSearch,int i){
		return CBBHelper.getScheduledFundingRepLst(fundingSchedulePojoLst,paginateSearch,i);
	}
	
	public List<FundingSchedulePojo> getfundingSchedule(Long scheduledFundingId){
		return CBBHelper.getfundingSchedule(scheduledFundingId);
	}

	@Override
	public AssessmentPojo getAssessmentPojoById(AssessmentPojo assessmentPojo) {
		return AssessmentHelper.getAssessmentPojoById(assessmentPojo);
	}

	public String getSiteInfoBySiteId(String siteColName,Long siteID){
		return CBBHelper.getSiteInfoBySiteId(siteColName,siteID);
	}

	@Override
	public List<CdrCbuPojo> getCordsByCbuStatus(Long cordId, Long cbuStatus) {
		return CBUHelper.getCordsByCbuStatus(cordId, cbuStatus);
	}

	@Override
	public List saveOrUpdateAssessments(List AssessmentPojos) {
		return AssessmentHelper.saveOrUpdateAssessmentList(AssessmentPojos);
	}
	
	public List<PatLabsPojo> getvalueSaveOtherTestList(List<PatLabsPojo> clist){
		return SampleInventHelper.getvalueSaveOtherTestList(clist);
	}

	@Override
	public List getAttachmentByEntityId(Long entityId, Long categoryId,Long subCategoryId) {
		return CBUHelper.getAttachmentList(entityId, categoryId,subCategoryId,null,0);
	}
	
	public List getAttachmentByEntityId(Long entityId, Long categoryId, Long revokedFlag, int i) {
		return CBUHelper.getAttachmentList(entityId, categoryId,i,null,0,revokedFlag);
	}
	
	public String checkForProcName(String procName, String cbbId){
		return CBBHelper.checkForProcName(procName, cbbId);
	}

	@Override
	public List<Object> getAuditRowDataByEntityId(Long entityId,String viewName,PaginateSearch paginateSearch,int i,String condition,String orderby) {
		return AuditTrailHelper.getAuditRowDataByEntityId(entityId,viewName,paginateSearch,i,condition,orderby);
	}

	@Override
	public List<Object> getAuditColumnDataByEntityId(Long entityId,
			String viewName) {
		return AuditTrailHelper.getAuditColumnDataByEntityId(entityId, viewName);
	}

	public Long getLabGrpPkByGrpType(String grpType){
		return CBUHelper.getLabGrpPkByGrpType(grpType);
	}

	public List getGroupsBySiteId(Long siteId) {
		return AppMessagesHelper.getGroupsBySiteId(siteId);
	}

	@Override
	public Integer getCordByCountByMaternalRegistry(String maternalRegistryId) {
		return CBUHelper.getCordByCountByMaternalRegistry(maternalRegistryId);
	}
	
	public String  getCbbType(String Siteid)
	{
		return CBBHelper.getCbbType(Siteid);
	}

	@Override
	public List<CBUStatus> getAllCbuStatus() {
		return OrderAttachmentHelper.getAllCbuStatus();
	}

	@Override
	public SitePojo getSiteBySiteName(String siteName,UserJB userObject) {
		// TODO Auto-generated method stub
		return CBBHelper.getSiteBySiteName(siteName,userObject);
	}
	
	
	public List<SitePojo> getMultipleValuesByErSite(Long entityId,
			String entityType, String entitySubType,long isActive) {
		// TODO Auto-generated method stub
		return CBBHelper.getMultipleValuesByErSite( entityId,
				entityType, entitySubType, isActive);
	}
	
	
	public List<SitePojo> getMultipleValuesByErSites(Long entityId,
			String entityType, String entitySubType) {
		// TODO Auto-generated method stub
		return CBBHelper.getMultipleValuesByErSites( entityId,
				entityType, entitySubType);
	}
	
	
	
	public List<SitePojo> isSiteExist(String siteName) {
		// TODO Auto-generated method stub
		return CBBHelper.isSiteExist(siteName);
	}
	
	public List<SitePojo> isCbbIdExist(String cbbId) {
		// TODO Auto-generated method stub
		return CBBHelper.isCbbIdExist(cbbId);
	}
	
	@Override
	public List<CordImportProcessMessagesPojo> getCordImportErrorMessagesByHistoryId(
			Long importHistoryId,Integer level,String registryId) {
		return CordImportHelper.getCordImportErrorMessagesByHistoryId(importHistoryId,level,registryId);
	}

	@Override
	public List<CordImportHistoryPojo> getCordImportHistory(Long pkCordimportHistory,Long userPk,PaginateSearch paginateSearch) {
		return CordImportHelper.getCordImportHistory(pkCordimportHistory,userPk,paginateSearch);
	}
	
	@Override
	public List getCordImportHistoryJson(Long pkCordimportHistory,Long userPk,PaginateSearch paginateSearch,String condtion,String orderby) {
		return CordImportHelper.getCordImportHistoryJson(pkCordimportHistory,userPk,paginateSearch,condtion,orderby);
	}

	@Override
	public List<CordImportProcessMessagesPojo> saveCordImportErrorMessages(
			List<CordImportProcessMessagesPojo> messagesPojos) {
		return CordImportHelper.saveCordImportErrorMessages(messagesPojos);
	}

	@Override
	public CordImportHistoryPojo saveCordImportHistory(
			CordImportHistoryPojo historyPojo) {
		return CordImportHelper.saveCordImportHistory(historyPojo);
	}

	@Override
	public void updateCordSearchable(Long cordSearchable, Long entityId) {
		CBUHelper.updateCordSearchable(cordSearchable, entityId);
	}
	
	public void updateCordProgress(Long cordSearchable,Long entityId){
		CBUHelper.updateCordProgress(cordSearchable, entityId);
	}

	@Override
	public List getQuestionsByFormId(Long formId) {
		return DynaFormHelper.getQuestionsByFormId(formId);
	}
    @Override
	public Long getfkCbbid(Long entityId){
		return DynaFormHelper.getfkCbbid(entityId);
	}
    @Override
	public String getidmdefervalue(Long entityId){
		return DynaFormHelper.getidmdefervalue(entityId);
	}
    @Override
    public String getAssessmentDetail(Long pkassessment){
    	return DynaFormHelper.getAssessmentDetail(pkassessment);
    }
	@Override
	public Integer getDomainCount(String objectName, String criteria,
			String pkName) {
		return CBUHelper.getDomainCount(objectName, criteria, pkName);
	}
	
	public List getCordImportHistoryCount(Long userPk,String condition){
		return CordImportHelper.getCordImportHistoryCount(userPk,condition);
	}

	@Override
	public String getTermialStatusByPkOrDesc(Long pkValue, String desc) {
		return OrderAttachmentHelper.getTermialStatusByPkOrDesc(pkValue,desc);
	}

	@Override
	public List<CBUStatus> getCBUStatusValues(String codeSubType) {
		return CodelstHelper.getCBUStatusValues(codeSubType);
	}

	@Override
	public Integer saveCordListForSearch(List<Long> cordListForSearch) {
		return CBUHelper.saveCordListForSearch(cordListForSearch);
	}

	@Override
	public Map<Long, RecipientInfo> getRecipientInfoMap(List<Long> cordList) {
		return CBUHelper.getRecipientInfoMap(cordList);
	}

	@Override
	public Boolean getValidateDbLongDataWithAdditionalField(String entityName,
			String entityFiled, String entityValue, String entityFiled2,
			String entityValue2, String entityField1, Long entityValue1) {
		return CodelstHelper.getValidateDbLongDataWithAdditionalField(entityName, entityFiled,
				entityValue, entityFiled2, entityValue2, entityField1, entityValue1);
	}

	@Override
	public Long getSequence(String seqName) {
		return CBUHelper.getSequence(seqName);
	}

	@Override
	public List<AntigenEncoding> getEncodingList(List<Long> pkAntigenList) {
		return CBUHelper.getEncodingList(pkAntigenList);
	}

	@Override
	public String getLookUpFilterData(String filterValue) {
		return CBUHelper.getLookUpFilterData(filterValue);
	}

	@Override
	public List<Object> getCordListAsobject(String sql,
			PaginateSearch paginateSearch) {
		return CBUHelper.getCordListAsobject(sql, paginateSearch);
	}
	
	public String getCbbInfoBySiteId(String CBBColName,Long siteID){
		return CBBHelper.getCbbInfoBySiteId(CBBColName, siteID);
	}
	
	public Long getFCR_flag(Long cordId){
		return OrderAttachmentHelper.getFCR_flag(cordId);
	}
	
	public Long getMC_flag(Long cordId){
		return OrderAttachmentHelper.getMC_flag(cordId);
	}
	
	public Long getCBUAssessment_flag(Long cordId){
		return OrderAttachmentHelper.getCBUAssessment_flag(cordId);
	}
	@Override
	public List<PatLabsPojo> getTestResultByTestNames(Long specimenId,
			Long fkTimingOfTest, List<String> testNames) {
		return SampleInventHelper.getTestResultByTestNames(specimenId, fkTimingOfTest, testNames);
	}
	
	public boolean getAssDetailsForLabSum(Long CordId,Long responceId,Long subEntityType,String questionNo){
		return CBUHelper.getAssDetailsForLabSum(CordId,responceId,subEntityType,questionNo);
	}

	@Override
	public void deleteHla(Long entityType, Long entityId) {
		CBUHelper.deleteHla(entityType, entityId);
		
	}

	@Override
	public void deleteTempHla(Long entityType, String registryId) {
		CBUHelper.deleteTempHla(entityType, registryId);
		
	}

	@Override
	public Map<Long, List<GrpsPojo>> getGroupsOfAllUserBySiteId(Long siteId) {
		return CBBHelper.getGroupsOfAllUserBySiteId(siteId);
	}

	@Override
	public EntityStatusPojo loadLatestEntityStatus(Long entityId,
			Long entityType, String status) {
		return CBUHelper.loadLatestEntityStatus(entityId, entityType, status);
	}

	@Override
	public List<ClinicalNotePojo> saveCordMultipleNote(
			List<ClinicalNotePojo> clinicalNotePojo,Boolean fromUI,Long entityId, Long noteCategory) {
		return ClinicalNoteHelper.saveCordMultipleNote(clinicalNotePojo,fromUI,entityId,noteCategory);
	}

	@Override
	public String getSiteName(Long siteId) {
		return CBBHelper.getSiteName(siteId);
	}
	
	@Override
	public String getSiteID(Long siteId) {
		return CBBHelper.getSiteID(siteId);
	}
		
	@Override
	public EligibilityNewDeclPojo saveOrUpdateNewFinalEligibilityQuestions(
			EligibilityNewDeclPojo eDeclPojo) {
		return CBUHelper.saveOrUpdateNewFinalEligibilityQuestions(eDeclPojo);
	}
	
	@Override
	public EligibilityNewDeclPojo getEligibilityNewDeclQuestionsByEntity(
			Long entityId, Long entityType,Long pkEligQues) {
		return CBUHelper.getEligibilityNewDeclQuestionsByEntity(entityId, entityType,pkEligQues);
	}
	@Override
	public void insertCordEntryHlaEsbRef(Long fkCord, UserJB user) {
		CordImportHelper.insertCordEntryHlaEsbRef(fkCord, user);
	}
	
	@Override
	public void deleteFinalReviewDocs(Long entityType, Long entityId) {
		CBUHelper.deleteFinalReviewDocs(entityType, entityId);
	}

	@Override
	public List<FinalRevUploadInfoPojo> saveFinalReviewDocs(
			List<FinalRevUploadInfoPojo> reviewPojos, Boolean fromUI) {
		return CBUHelper.saveFinalReviewDocs(reviewPojos, fromUI);
	}
	
	public List<FinalRevUploadInfo> getFinalReviewUploadDocLst(Long cordID,Long entityType){
		return CBUHelper.getFinalReviewUploadDocLst(cordID,entityType);
	}
	
	@Override
	public List<SitePojo> getSitesForQueryBuilder() {
		return CBBHelper.getSitesForQueryBuilder();
	}

	@Override
	public List validateDataExistence(StringBuilder sqlQuery) {		
		return CBUHelper.validateDataExistence(sqlQuery);
	}
	
	@Override
	public Long getEligTaskFlag(Long cordID, int i) {
		return OrderAttachmentHelper.getEligTaskFlag(cordID,i);
	}
	
	@Override
	public String lastCBUStatus(Long cordID) throws Exception {
		return ClinicalNoteHelper.lastCBUStatus(cordID);
	}
	
	@Override
	public String getFmhqQuestionResp(Long cordID) throws Exception {
		return CordImportHelper.getFmhqQuestionResp(cordID);
	}
	
	@Override
	public List<String> getRaceOrderByValue(String qStr) throws Exception {
		return OrderAttachmentHelper.getRaceOrderByValue(qStr);
	}
	
	@Override
	public List<Object> updateCdrCbuObject(Object obj) throws Exception {
		return CBUHelper.updateCdrCbuObject(obj);
	}
}
