package com.velos.ordercomponent.business.xmlpojos;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class ImmuneDeficiencies {

	private String inher_imm_dfc_disordr_ind;
	/*private Relations relations;
	@XmlElement(name="Relations")
	public Relations getRelations() {
		return relations;
	}
	public void setRelations(Relations relations) {
		this.relations = relations;
	}*/
	@XmlElement(name="inher_imm_dfc_disordr_ind")
	public String getInher_imm_dfc_disordr_ind() {
		return inher_imm_dfc_disordr_ind;
	}
	public void setInher_imm_dfc_disordr_ind(String inherImmDfcDisordrInd) {
		inher_imm_dfc_disordr_ind = inherImmDfcDisordrInd;
	}
	private List<Relations> Relations;
	@XmlElement(name="Relations")
	public List<Relations> getRelations() {
		return Relations;
	}
	public void setRelations(List<Relations> relations) {
		Relations = relations;
	}
	
}
