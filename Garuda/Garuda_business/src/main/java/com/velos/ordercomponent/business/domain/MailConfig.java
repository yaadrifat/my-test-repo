package com.velos.ordercomponent.business.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
@Entity
@Table(name="ER_MAILCONFIG")
@SequenceGenerator(sequenceName="SEQ_ER_MAILCONFIG",name="SEQ_ER_MAILCONFIG",allocationSize=1)
public class MailConfig extends Auditable implements Serializable {

private Long mailConfigID;
	
	
	private String mailConfigCode;
	private String mailConfigSubject;
	private String mailConfigContent;
	private String mailConfigContentType;
	private String mailConfigFrom;
	private String mailConfigBCC;
	private String mailConfigCC;
	private Boolean mailConfigHasAttachment;
	private Long mailConfigCodeListOrgId;

	/*private Auditable auditable;*/

	@Id
	@Column(name="PK_MAILCONFIG")
	@GeneratedValue(strategy=GenerationType.SEQUENCE , generator="SEQ_ER_MAILCONFIG")
	public Long getMailConfigID() {
		return mailConfigID;
	}
	public void setMailConfigID(Long mailConfigID) {
		this.mailConfigID = mailConfigID;
	}
	
	@Column(name="MAILCONFIG_MAILCODE")
	public String getMailConfigCode() {
		return mailConfigCode;
	}
	public void setMailConfigCode(String mailConfigCode) {
		this.mailConfigCode = mailConfigCode;
	}
	
	@Column(name="MAILCONFIG_SUBJECT")
	public String getMailConfigSubject() {
		return mailConfigSubject;
	}
	public void setMailConfigSubject(String mailConfigSubject) {
		this.mailConfigSubject = mailConfigSubject;
	}
	
	@Column(name="MAILCONFIG_CONTENT")
	public String getMailConfigContent() {
		return mailConfigContent;
	}
	public void setMailConfigContent(String mailConfigContent) {
		this.mailConfigContent = mailConfigContent;
	}
	
	@Column(name="MAILCONFIG_CONTENTTYPE")
	public String getMailConfigContentType() {
		return mailConfigContentType;
	}
	public void setMailConfigContentType(String mailConfigContentType) {
		this.mailConfigContentType = mailConfigContentType;
	}
	
	@Column(name="MAILCONFIG_FROM")
	public String getMailConfigFrom() {
		return mailConfigFrom;
	}
	public void setMailConfigFrom(String mailConfigFrom) {
		this.mailConfigFrom = mailConfigFrom;
	}
	
	@Column(name="MAILCONFIG_BCC")
	public String getMailConfigBCC() {
		return mailConfigBCC;
	}
	public void setMailConfigBCC(String mailConfigBCC) {
		this.mailConfigBCC = mailConfigBCC;
	}
	
	@Column(name="MAILCONFIG_CC")
	public String getMailConfigCC() {
		return mailConfigCC;
	}
	public void setMailConfigCC(String mailConfigCC) {
		this.mailConfigCC = mailConfigCC;
	}
	
	@Column(name="MAILCONFIG_HASATTACHEMENT")
	public Boolean getMailConfigHasAttachment() {
		return mailConfigHasAttachment;
	}
	public void setMailConfigHasAttachment(Boolean mailConfigHasAttachment) {
		this.mailConfigHasAttachment = mailConfigHasAttachment;
	}
	
	@Column(name="FK_CODELSTORGID")
	public Long getMailConfigCodeListOrgId() {
		return mailConfigCodeListOrgId;
	}
	public void setMailConfigCodeListOrgId(Long mailConfigCodeListOrgId) {
		this.mailConfigCodeListOrgId = mailConfigCodeListOrgId;
	}
	
	
	/*@Embedded
	public Auditable getAuditable() {
		return auditable;
	}
	public void setAuditable(Auditable auditable) {
		this.auditable = auditable;
	}*/
	
	private Boolean deletedFlag;
	
	@Column(name="deletedflag")
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	
}