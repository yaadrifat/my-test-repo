package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;

public class SevereAutoImmune {
	
	private severe_aut_imm_sys_disordr autImmSysDisordr;
	private inher_oth_unk_imm_sys_dis inherOthUnkImmSysDis;
	
	
	@XmlElement(name="severe_aut_imm_sys_disordr")
	public severe_aut_imm_sys_disordr getAutImmSysDisordr() {
		return autImmSysDisordr;
	}

	public void setAutImmSysDisordr(severe_aut_imm_sys_disordr autImmSysDisordr) {
		this.autImmSysDisordr = autImmSysDisordr;
	}
	

	@XmlElement(name="inher_oth_unk_imm_sys_dis")
	public inher_oth_unk_imm_sys_dis getInherOthUnkImmSysDis() {
		return inherOthUnkImmSysDis;
	}

	public void setInherOthUnkImmSysDis(
			inher_oth_unk_imm_sys_dis inherOthUnkImmSysDis) {
		this.inherOthUnkImmSysDis = inherOthUnkImmSysDis;
	}

	
}
