package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


public class cbu {
	private Identifiers identifiers;
	private Status status;
	private CBUDemographics cbuDemographics;
	private LabSummary labSummary;
	private CBUSamples cbuSamples;
	private Forms forms;
	private Eligibility eligibility;

	@XmlElement(name="Identifiers")
	public Identifiers getIdentifiers() {
		return identifiers;
	}
	
	public void setIdentifiers(Identifiers identifiers) {
		this.identifiers = identifiers;
	}

	@XmlElement(name="Status")
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@XmlElement(name="CBUDemographics")
	public CBUDemographics getCbuDemographics() {
		return cbuDemographics;
	}

	public void setCbuDemographics(CBUDemographics cbuDemographics) {
		this.cbuDemographics = cbuDemographics;
	}

	@XmlElement(name="LabSummary")
	public LabSummary getLabSummary() {
		return labSummary;
	}

	public void setLabSummary(LabSummary labSummary) {
		this.labSummary = labSummary;
	}

	@XmlElement(name="CBUSamples")
	public CBUSamples getCbuSamples() {
		return cbuSamples;
	}

	public void setCbuSamples(CBUSamples cbuSamples) {
		this.cbuSamples = cbuSamples;
	}

	@XmlElement(name="Forms")
	public Forms getForms() {
		return forms;
	}

	public void setForms(Forms forms) {
		this.forms = forms;
	}

	@XmlElement(name="Eligibility")
	public Eligibility getEligibility() {
		return eligibility;
	}

	public void setEligibility(Eligibility eligibility) {
		this.eligibility = eligibility;
	} 
	
	
 
}
