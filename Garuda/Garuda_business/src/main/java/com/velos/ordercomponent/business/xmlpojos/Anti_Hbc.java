package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


public class Anti_Hbc {
	private String anti_hbc_react_ind;
	private XMLGregorianCalendar anti_hbc_date;
	private String cms_cert_lab_ind;
	private String fda_lic_mfg_inst_ind;
	
	@XmlElement(name="anti_hbc_react_ind")
	public String getAnti_hbc_react_ind() {
		return anti_hbc_react_ind;
	}
	public void setAnti_hbc_react_ind(String antiHbcReactInd) {
		anti_hbc_react_ind = antiHbcReactInd;
	}
	@XmlElement(name="anti_hbc_date")
	@XmlJavaTypeAdapter(type=XMLGregorianCalendar.class,value=XMLGeorgarianCalanderAdapter.class)
	public XMLGregorianCalendar getAnti_hbc_date() {
		return anti_hbc_date;
	}
	public void setAnti_hbc_date(XMLGregorianCalendar antiHbcDate) {
		anti_hbc_date = antiHbcDate;
	}
	@XmlElement(name="cms_cert_lab_ind")
	public String getCms_cert_lab_ind() {
		return cms_cert_lab_ind;
	}
	public void setCms_cert_lab_ind(String cmsCertLabInd) {
		cms_cert_lab_ind = cmsCertLabInd;
	}
	@XmlElement(name="fda_lic_mfg_inst_ind")
	public String getFda_lic_mfg_inst_ind() {
		return fda_lic_mfg_inst_ind;
	}
	public void setFda_lic_mfg_inst_ind(String fdaLicMfgInstInd) {
		fda_lic_mfg_inst_ind = fdaLicMfgInstInd;
	}
	
    
}
