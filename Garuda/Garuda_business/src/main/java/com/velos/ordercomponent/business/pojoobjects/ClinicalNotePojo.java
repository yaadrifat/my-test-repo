package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.ValidationParameter;
import com.velos.ordercomponent.business.domain.Auditable;

public class ClinicalNotePojo extends Auditable {
	/**
	 * @author Ruchi
	 */
	private Long pkNotes;
	private Long entityId;
	private Long entityType;
	private Long fkNotesCategory;
	private Long fkNotesType ;
	private String notes;
	private String noteAssessment;
	private Date availableDate;
	private String explaination;
	private String keyword;
	private String sentTo ;
	private Long fkReason;
	private Long fkCbuStatus;
	private Boolean flagForLater;
	private String noteSeq;
	private Boolean amended;
	private String deletedFlag;
	private String comments;
	private String subject;
	private Date requestDate;
	private Long requestType;
	private String explainationTu;
	private Long reasonTu;
	private Boolean reply;
	private String userName;
	private Boolean visibility;
	private String noteVisible;
	private Long fkOrderId;
	private Boolean informTo;
	private Boolean consultTo;
	private Boolean reviewStatus;
	private String orderType;
	private Boolean tagNote;
	
	
	
	public Boolean getTagNote() {
		return tagNote;
	}
	public void setTagNote(Boolean tagNote) {
		this.tagNote = tagNote;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public Boolean getReviewStatus() {
		return reviewStatus;
	}
	public void setReviewStatus(Boolean reviewStatus) {
		this.reviewStatus = reviewStatus;
	}
	public Boolean getConsultTo() {
		return consultTo;
	}
	public void setConsultTo(Boolean consultTo) {
		this.consultTo = consultTo;
	}
	public Boolean getInformTo() {
		return informTo;
	}
	public void setInformTo(Boolean informTo) {
		this.informTo = informTo;
	}
	public Long getFkOrderId() {
		return fkOrderId;
	}
	public void setFkOrderId(Long fkOrderId) {
		this.fkOrderId = fkOrderId;
	}
	public String getNoteVisible() {
		return noteVisible;
	}
	public void setNoteVisible(String noteVisible) {
		this.noteVisible = noteVisible;
	}
	public Boolean getReply() {
		return reply;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public void setReply(Boolean reply) {
		this.reply = reply;
	}
	public Long getReasonTu() {
		return reasonTu;
	}
	public void setReasonTu(Long reasonTu) {
		this.reasonTu = reasonTu;
	}
	@CustomValidator(
			  type ="cordentrynoncustomvalidator",
			  fieldName = "comments",				 
			  key="garuda.common.validation.comment",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
					         @ValidationParameter( name = "fields", value = "flagForLater")				       
			}) 
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Boolean getAmended() {
		return amended;
	}
	public void setAmended(Boolean amended) {
		this.amended = amended;
	}	     
	public Boolean getFlagForLater() {
		return flagForLater;
	}
	public void setFlagForLater(Boolean flagForLater) {
		this.flagForLater = flagForLater;
	}
	public Long getPkNotes() {
		return pkNotes;
	}
	public void setPkNotes(Long pkNotes) {
		this.pkNotes = pkNotes;
	}
	public Long getEntityId() {
		return entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	public Long getEntityType() {
		return entityType;
	}
	public void setEntityType(Long entityType) {
		this.entityType = entityType;
	}
	@CustomValidator(
			  type ="cordentrynoncustomvalidator",
			  fieldName = "fkNotesCategory",				 
			  key="garuda.cbu.clinicalnote.notescategory",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
					         @ValidationParameter( name = "fields", value = "notes,noteAssessment")				       
			}) 
	public Long getFkNotesCategory() {
		return fkNotesCategory;
	}
	public void setFkNotesCategory(Long fkNotesCategory) {
		this.fkNotesCategory = fkNotesCategory;
	}
	public Long getFkNotesType() {
		return fkNotesType;
	}
	public void setFkNotesType(Long fkNotesType) {
		this.fkNotesType = fkNotesType;
	}
	@CustomValidator(
			  type ="cordentrynoncustomvalidator",
			  fieldName = "notes",				 
			  key="garuda.cbu.cordentry.reqnotes",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
					         @ValidationParameter( name = "fields", value = "noteAssessment,fkNotesCategory")				       
			}) 
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	@CustomValidator(
			  type ="cordentrynoncustomvalidator",
			  fieldName = "noteAssessment",				 
			  key="garuda.cbu.assessment.selectAssess",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
					         @ValidationParameter( name = "fields", value = "notes,fkNotesCategory")					        
			})
	public String getNoteAssessment() {
		return noteAssessment;
	}
	public void setNoteAssessment(String noteAssessment) {
		this.noteAssessment = noteAssessment;
	}
	public Date getAvailableDate() {
		return availableDate;
	}
	public void setAvailableDate(Date availableDate) {
		this.availableDate = availableDate;
	}
	public String getExplaination() {
		return explaination;
	}
	public void setExplaination(String explaination) {
		this.explaination = explaination;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getSentTo() {
		return sentTo;
	}
	public void setSentTo(String sentTo) {
		this.sentTo = sentTo;
	}
	public Long getFkReason() {
		return fkReason;
	}
	public void setFkReason(Long fkReason) {
		this.fkReason = fkReason;
	}
	public Long getFkCbuStatus() {
		return fkCbuStatus;
	}
	public void setFkCbuStatus(Long fkCbuStatus) {
		this.fkCbuStatus = fkCbuStatus;
	}

	public String getNoteSeq() {
		return noteSeq;
	}
	public void setNoteSeq(String noteSeq) {
		this.noteSeq = noteSeq;
	}
	public String getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(String deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public Date getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	public Long getRequestType() {
		return requestType;
	}
	public void setRequestType(Long requestType) {
		this.requestType = requestType;
	}
	public String getExplainationTu() {
		return explainationTu;
	}
	public void setExplainationTu(String explainationTu) {
		this.explainationTu = explainationTu;
	}
	public Boolean getVisibility() {
		return visibility;
	}
	public void setVisibility(Boolean visibility) {
		this.visibility = visibility;
	}
	
	
	
}
