package com.velos.ordercomponent.helper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;

import com.velos.ordercomponent.business.domain.TaskList;
import com.velos.ordercomponent.business.util.HibernateUtil;

public class TaskListHelper {

	public static final Log log=LogFactory.getLog(TaskListHelper.class);
	
	public static List getTaskLists(TaskList taskList1,TaskList taskList2){

		return getTaskLists(taskList1, taskList2, null);
	}
	
	public static List getTaskLists(TaskList taskList1,TaskList taskList2, Session session) {
		
		List list = new ArrayList();
		list.add(taskList1);
		list.add(taskList2);
		try {
			
			boolean sessionFlag = false;
			// check session
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			for(int i=0;i<list.size();i++){
				TaskList tasklist = new TaskList();
				tasklist = (TaskList)list.get(i);
				
				if (tasklist.getPkTaskId() != null
						&& tasklist.getPkTaskId() > 0) {
					session.update(tasklist);
				} else {
					session.save(tasklist);
				}
			}
			if (sessionFlag) {
				// close session, if session created in this method
				session.getTransaction().commit();
			}

		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			// throw new P1VBaseException();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	
		return list;
	}
}
