package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="CBB_PROCESSING_PROCEDURES")
@SequenceGenerator(sequenceName="SEQ_CBB_PROCESSING_PROCEDURES",name="SEQ_CBB_PROCESSING_PROCEDURES",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class CBBProcedures extends Auditable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long pkProcId;
	private Double procNo;
	private String procName;
	private Date procStartDate;
	private Date procTermiDate;
	private String procVersion;
	private CBBProceduresInfo cbbProceduresInfo;
	private Long fkSite;
	private String productCode;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CBB_PROCESSING_PROCEDURES")
	@Column(name="PK_PROC")
	public Long getPkProcId() {
		return pkProcId;
	}
	public void setPkProcId(Long pkProcId) {
		this.pkProcId = pkProcId;
	}
	@Column(name="PROC_NO")
	public Double getProcNo() {
		return procNo;
	}
	public void setProcNo(Double procNo) {
		this.procNo = procNo;
	}
	@Column(name="PROC_NAME")
	public String getProcName() {
		return procName;
	}
	public void setProcName(String procName) {
		this.procName = procName;
	}
	@Column(name="PROC_START_DATE")
	public Date getProcStartDate() {
		return procStartDate;
	}
	public void setProcStartDate(Date procStartDate) {
		this.procStartDate = procStartDate;
	}
	@Column(name="PROC_TERMI_DATE")
	public Date getProcTermiDate() {
		return procTermiDate;
	}
	public void setProcTermiDate(Date procTermiDate) {
		this.procTermiDate = procTermiDate;
	}
	@Column(name="PROC_VERSION")
	public String getProcVersion() {
		return procVersion;
	}
	public void setProcVersion(String procVersion) {
		this.procVersion = procVersion;
	}
	
	@OneToOne(fetch = FetchType.LAZY)
	@PrimaryKeyJoinColumn
	public CBBProceduresInfo getCbbProceduresInfo() {
		return cbbProceduresInfo;
	}
	public void setCbbProceduresInfo(CBBProceduresInfo cbbProceduresInfo) {
		this.cbbProceduresInfo = cbbProceduresInfo;
	}
	
	@Column(name="FK_SITE")
	public Long getFkSite() {
		return fkSite;
	}
	public void setFkSite(Long fkSite) {
		this.fkSite = fkSite;
	}
	
	@Column(name="PRODUCT_CODE")
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	
	
	
}
