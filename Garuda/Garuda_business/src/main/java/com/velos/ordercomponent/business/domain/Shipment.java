package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="CB_SHIPMENT")
@SequenceGenerator(sequenceName="SEQ_CB_SHIPMENT",name="SEQ_CB_SHIPMENT",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class Shipment extends Auditable{

	private Long shipmentId;
	private Long orderId;
	private Date shipmentDate;
	private Date addiShipmentDdate;
	private String shipmentTrackingNo;
	private Long addiShipmentTrackingNo;
	private Long cbbId;
	private Long fkCordId;
	private String attnName;
	private Long fkDeliveryAddress;
	private Long fkShippingContainer;
	private Long fkTempMoniter;
	private Date expectingArriveDate;
	private String shipmentFlag;
	private Long shipmentConfirmBy;
	private Date shipmentConfirmDate;
	private Date propShipmentDate;
	private Date schShipmentDate;
	private String addiInfo;
	
	
	private Long fkshipingCompanyId;
	
	private String shipmentTime;
	private String padlockCombination;
	private Long paperworkLoc;
	private String additiShiiperDet;
	private Long shipmentType;
	private String nmdpShipExcuse;
	private String dataModifiedFlag;
	private Long excuseFormSubmittedBy;
	private Date excuseFormSubmittedDate;
	
	
	
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_SHIPMENT")
	@Column(name="PK_SHIPMENT")
	public Long getShipmentId() {
		return shipmentId;
	}
	public void setShipmentId(Long shipmentId) {
		this.shipmentId = shipmentId;
	}
	
	@Column(name="FK_ORDER_ID")
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	
	
	@Column(name="SHIPMENT_DATE")
	public Date getShipmentDate() {
		return shipmentDate;
	}
	public void setShipmentDate(Date shipmentDate) {
		this.shipmentDate = shipmentDate;
	}
	
	@Column(name="ADDI_SHIPMENT_DATE")
	public Date getAddiShipmentDdate() {
		return addiShipmentDdate;
	}
	public void setAddiShipmentDdate(Date addiShipmentDdate) {
		this.addiShipmentDdate = addiShipmentDdate;
	}
	
	
	@Column(name="SHIPMENT_TRACKING_NO")
	public String getShipmentTrackingNo() {
		return shipmentTrackingNo;
	}
	public void setShipmentTrackingNo(String shipmentTrackingNo) {
		this.shipmentTrackingNo = shipmentTrackingNo;
	}
	
	
	@Column(name="ADDI_SHIPMENT_TRACKING_NO")
	public Long getAddiShipmentTrackingNo() {
		return addiShipmentTrackingNo;
	}
	public void setAddiShipmentTrackingNo(Long addiShipmentTrackingNo) {
		this.addiShipmentTrackingNo = addiShipmentTrackingNo;
	}
	
	@Column(name="CBB_ID")
	public Long getCbbId() {
		return cbbId;
	}
	public void setCbbId(Long cbbId) {
		this.cbbId = cbbId;
	}
	
	
	@Column(name="FK_CORD_ID")
	public Long getFkCordId() {
		return fkCordId;
	}
	public void setFkCordId(Long fkCordId) {
		this.fkCordId = fkCordId;
	}
	
	
	@Column(name="ATTN_NAME")
	public String getAttnName() {
		return attnName;
	}
	public void setAttnName(String attnName) {
		this.attnName = attnName;
	}
	
	
	@Column(name="FK_DELIVERY_ADD")
	public Long getFkDeliveryAddress() {
		return fkDeliveryAddress;
	}
	public void setFkDeliveryAddress(Long fkDeliveryAddress) {
		this.fkDeliveryAddress = fkDeliveryAddress;
	}
	
	
	@Column(name="FK_SHIPPING_CONT")
	public Long getFkShippingContainer() {
		return fkShippingContainer;
	}
	public void setFkShippingContainer(Long fkShippingContainer) {
		this.fkShippingContainer = fkShippingContainer;
	}
	
	@Column(name="FK_TEMPERATURE_MONITER")
	public Long getFkTempMoniter() {
		return fkTempMoniter;
	}
	public void setFkTempMoniter(Long fkTempMoniter) {
		this.fkTempMoniter = fkTempMoniter;
	}
	
	
	@Column(name="EXPECT_ARR_DATE")
	public Date getExpectingArriveDate() {
		return expectingArriveDate;
	}
	public void setExpectingArriveDate(Date expectingArriveDate) {
		this.expectingArriveDate = expectingArriveDate;
	}
	
	
	@Column(name="SHIPMENT_CONFIRM")
	public String getShipmentFlag() {
		return shipmentFlag;
	}
	public void setShipmentFlag(String shipmentFlag) {
		this.shipmentFlag = shipmentFlag;
	}
	
	
	@Column(name="SHIPMENT_CONFIRM_BY")
	public Long getShipmentConfirmBy() {
		return shipmentConfirmBy;
	}
	public void setShipmentConfirmBy(Long shipmentConfirmBy) {
		this.shipmentConfirmBy = shipmentConfirmBy;
	}
	
	@Column(name="SHIPMENT_CONFIRM_ON")
	public Date getShipmentConfirmDate() {
		return shipmentConfirmDate;
	}
	public void setShipmentConfirmDate(Date shipmentConfirmDate) {
		this.shipmentConfirmDate = shipmentConfirmDate;
	}
	
	
	@Column(name="PROP_SHIPMENT_DATE")
	public Date getPropShipmentDate() {
		return propShipmentDate;
	}
	public void setPropShipmentDate(Date propShipmentDate) {
		this.propShipmentDate = propShipmentDate;
	}
	
	
	@Column(name="SCH_SHIPMENT_DATE")
	public Date getSchShipmentDate() {
		return schShipmentDate;
	}
	public void setSchShipmentDate(Date schShipmentDate) {
		this.schShipmentDate = schShipmentDate;
	}
	
	
	@Column(name="ADDI_INFO")
	public String getAddiInfo() {
		return addiInfo;
	}
	public void setAddiInfo(String addiInfo) {
		this.addiInfo = addiInfo;
	}
	
	
	
	
	@Column(name="FK_SHIP_COMPANY_ID")
	public Long getFkshipingCompanyId() {
		return fkshipingCompanyId;
	}
	public void setFkshipingCompanyId(Long fkshipingCompanyId) {
		this.fkshipingCompanyId = fkshipingCompanyId;
	}
	
	
	
	
	
	@Column(name="SCH_SHIPMENT_TIME")
	public String getShipmentTime() {
		return shipmentTime;
	}
	public void setShipmentTime(String shipmentTime) {
		this.shipmentTime = shipmentTime;
	}
	
	@Column(name="PADLOCK_COMBINATION")
	public String getPadlockCombination() {
		return padlockCombination;
	}
	public void setPadlockCombination(String padlockCombination) {
		this.padlockCombination = padlockCombination;
	}
	
	@Column(name="PAPERWORK_LOCATION")
	public Long getPaperworkLoc() {
		return paperworkLoc;
	}
	public void setPaperworkLoc(Long paperworkLoc) {
		this.paperworkLoc = paperworkLoc;
	}
	
	@Column(name="ADDITI_SHIPPER_DET")
	public String getAdditiShiiperDet() {
		return additiShiiperDet;
	}
	public void setAdditiShiiperDet(String additiShiiperDet) {
		this.additiShiiperDet = additiShiiperDet;
	}
	
	
	@Column(name="FK_SHIPMENT_TYPE")
	public Long getShipmentType() {
		return shipmentType;
	}
	public void setShipmentType(Long shipmentType) {
		this.shipmentType = shipmentType;
	}
	
	@Column(name="NMDP_SHIP_EXCUSE_FLAG")
	public String getNmdpShipExcuse() {
		return nmdpShipExcuse;
	}
	public void setNmdpShipExcuse(String nmdpShipExcuse) {
		this.nmdpShipExcuse = nmdpShipExcuse;
	}
	
	
	@Column(name="DATA_MODIFIED_FLAG")
	public String getDataModifiedFlag() {
		return dataModifiedFlag;
	}
	public void setDataModifiedFlag(String dataModifiedFlag) {
		this.dataModifiedFlag = dataModifiedFlag;
	}
	
	@Column(name="NMDP_SHIP_EXCUSE_SUBMITTED_BY")
	public Long getExcuseFormSubmittedBy() {
		return excuseFormSubmittedBy;
	}
	public void setExcuseFormSubmittedBy(Long excuseFormSubmittedBy) {
		this.excuseFormSubmittedBy = excuseFormSubmittedBy;
	}
	
	@Column(name="NMDP_SHIP_EXCUSE_SUBMITTED_ON")
	public Date getExcuseFormSubmittedDate() {
		return excuseFormSubmittedDate;
	}
	public void setExcuseFormSubmittedDate(Date excuseFormSubmittedDate) {
		this.excuseFormSubmittedDate = excuseFormSubmittedDate;
	}
	
	
	

}
