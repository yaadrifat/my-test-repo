package com.velos.ordercomponent.queryconstants;

public interface AuditTrailQueryConstants {
	
	//String AUDIT_TRAIL_ROW_CORD_DATA = "SELECT PK_ROW_ID,CORD_REGISTRY_ID,CORD_LOCAL_CBU_ID,REGISTRY_MATERNAL_ID,MATERNAL_LOCAL_ID,CREATED_ON,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,MODULE_ID from REP_CB_CORD_VIEW where module_id=?";
	/*String AUDIT_TRAIL_ROW_CORD_DATA = "select acm.column_name, acm.column_display_name, (select usr_logname from er_user where pk_user = arm.user_id), " +
			" arm.timestamp, decode(arm.action,'I','Add','U','Update','-'), acm.old_value, acm.new_value, arm.table_name from audit_column_module acm, audit_row_module arm " +
			" where acm.fk_row_id = arm.pk_row_id and ( arm.module_id in (?) or arm.module_id in (select pk_multiple_values from cb_multiple_values where entity_id = ?) " +
			" or arm.module_id in (select pk_entity_status from cb_entity_status where entity_id = ?) " +
			" or arm.module_id in (select pk_entity_status_reason from cb_entity_status_reason where fk_entity_status in (select pk_entity_status from cb_entity_status  where entity_id = ?)) " +
			" or arm.module_id in (select pk_entity_samples from cb_entity_samples where entity_id = ?) " +
			" or arm.module_id in (select pk_notes from cb_notes where entity_id = ?) " +
			" or arm.module_id in (select pk_assessment from cb_assessment where entity_id = ?) "+
			" or arm.module_id in (select pk_hla from cb_hla where entity_id = ?) "+
			" or arm.module_id in (select pk_patlabs from er_patlabs where fk_specimen = (select fk_specimen_id from cb_cord where pk_cord=?)) " +
			" ) order by arm.timestamp desc";      
	*/
	String AUDIT_TRAIL_ROW_CORD_DATA$ = " select * from (select acm.column_name, acm.column_display_name, (select usr_logname from er_user where pk_user = arm.user_id) user_id, " +
			" to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, decode(arm.action,'I','Add','U','Update','-') action, acm.old_value, acm.new_value, arm.table_name,arm.timestamp, acm.PK_COLUMN_ID " +
			" from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id and (arm.module_id = ? " +
			" and arm.table_name = 'CB_CORD') and acm.column_name  in ('FK_CORD_CBU_STATUS','FK_CODELST_ETHNICITY','MULTIPLE_BIRTH','CB_ADDITIONAL_ID','FK_CORD_BABY_GENDER_ID', 'CORD_BABY_BIRTH_DATE','FK_CBU_STOR_LOC','FK_CBU_COLL_SITE','FUNDED_CBU','FK_FUND_CATEG','FK_CBU_COLL_TYPE','FK_CBU_DEL_TYPE','NMDP_RACE_ID','FK_CBB_ID','CORD_NMDP_STATUS','CORD_SEARCHABLE') union all " +
			" select acm.column_name, acm.column_display_name, (select usr_logname from er_user where pk_user = arm.user_id) user_id,  to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, " +
			" decode(arm.action,'I','Add','U','Update','-') action, acm.old_value, acm.new_value, arm.table_name,arm.timestamp, acm.PK_COLUMN_ID from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id "+ 
			" and (arm.module_id in (select pk_multiple_values from cb_multiple_values where entity_id = ? and fk_codelst_type = 'race') and arm.table_name = 'CB_MULTIPLE_VALUES') and acm.column_name in ('FK_CODELST') " +
			" union all  select acm.column_name, acm.column_display_name, (select usr_logname from er_user where pk_user = arm.user_id) user_id, " +
			" to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, decode(arm.action,'I','Add','U','Update','-') action, acm.old_value, " +
			" acm.new_value, arm.table_name,arm.timestamp, acm.PK_COLUMN_ID  from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id and (arm.module_id in (select FK_SPECIMEN_ID from CB_CORD where PK_CORD = ?)  " +
			" and arm.table_name = 'ER_SPECIMEN') and acm.column_name  in  ('SPEC_COLLECTION_DATE')) "; 
	
	String AUDIT_TRAIL_ROW_CORD_DATA_COUNT = "select count(*) from ("+AUDIT_TRAIL_ROW_CORD_DATA$+")";
	
	String AUDIT_TRAIL_COLUMN_CORD_DATA = "SELECT FK_ROW_ID,CORD_REGISTRY_ID,CORD_LOCAL_CBU_ID,REGISTRY_MATERNAL_ID,MATERNAL_LOCAL_ID,CREATED_ON,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,COLUMN_NAME,OLD_VALUE,NEW_VALUE,REASON,REMARKS,COLUMN_DISPLAY_NAME FROM REP_CB_CORD_AUDT_VIEW WHERE FK_ROW_ID=?";
	
	
	String AUDIT_TRAIL_ROW_ENTITY_STATUS_DATA = "select * from (select acm.column_name, acm.column_display_name, (select usr_logname from er_user where pk_user = arm.user_id) user_id, " +
	" to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, decode(arm.action,'I','Add','U','Update','-') action, acm.old_value, acm.new_value, arm.table_name,arm.timestamp, acm.pk_column_id " +
	" from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id " +
	" and (arm.module_id in (select pk_entity_status from cb_entity_status where entity_id = ? and status_type_code = 'licence') and arm.table_name = 'CB_ENTITY_STATUS') " +
	" and acm.column_name not in ('RID','IP_ADD','CREATED_ON','CREATOR','DELETEDFLAG','PK_ENTITY_STATUS','ENTITY_ID','IS_CORD_ENTRY_DATA','ORDER_ID','ORDER_FK_RESOL','STATUS_DATE','FK_ENTITY_TYPE','STATUS_TYPE_CODE','ADD_OR_UPDATE') union all select acm.column_name, acm.column_display_name, (select usr_logname from er_user where pk_user = arm.user_id) user_id, "+  
	" to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, decode(arm.action,'I','Add','U','Update','-') action, acm.old_value, acm.new_value, arm.table_name,arm.timestamp, acm.pk_column_id from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id and "+ 
	" (arm.module_id in (select pk_entity_status_reason from cb_entity_status_reason where fk_entity_status in (select pk_entity_status from cb_entity_status where entity_id = ? and status_type_code= 'licence') and arm.table_name = 'CB_ENTITY_STATUS_REASON')) "+  
	" and acm.column_name not in ('RID','IP_ADD','CREATED_ON','CREATOR','PK_ENTITY_STATUS_REASON','DELETEDFLAG','FK_ATTACHMENTID','FK_FORMID','NOT_APP_PRIOR_FLAG','FK_ENTITY_STATUS') )"; 

	

	String AUDIT_TRAIL_ROW_ENTITY_STATUS_DATA_COUNT = "select count(*) from ("+AUDIT_TRAIL_ROW_ENTITY_STATUS_DATA+")";
	
	String AUDIT_TRAIL_COLUMN_ENTITY_STATUS_DATA = "SELECT FK_ROW_ID,CORD_REGISTRY_ID,CORD_LOCAL_CBU_ID,REGISTRY_MATERNAL_ID,MATERNAL_LOCAL_ID,CREATED_ON,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,COLUMN_NAME,OLD_VALUE,NEW_VALUE,REASON,REMARKS,COLUMN_DISPLAY_NAME FROM REP_ENTY_STUS_AUDT_VIEW WHERE FK_ROW_ID=?";
	
	String AUDIT_TRAIL_ROW_ENTITY_STATUS_REASON_DATA = " select acm.column_name, acm.column_display_name, (select usr_logname from er_user where pk_user = arm.user_id) user_id,  to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, decode(arm.action,'I','Add','U','Update','-') action, acm.old_value, acm.new_value, arm.table_name " +
		" from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id and (arm.module_id in (select pk_entity_status_reason from cb_entity_status_reason where fk_entity_status in (select pk_entity_status from cb_entity_status where entity_id = ?) and arm.table_name = 'CB_ENTITY_STATUS_REASON')) " + 
		" and acm.column_name not in ('RID','IP_ADD','CREATED_ON','CREATOR','PK_ENTITY_STATUS_REASON','DELETEDFLAG','FK_ATTACHMENTID','FK_FORMID','NOT_APP_PRIOR_FLAG','FK_ENTITY_STATUS') order by arm.timestamp desc ";
	
	String AUDIT_TRAIL_COLUMN_ENTITY_STATUS_REASON_DATA = "SELECT FK_ROW_ID,CORD_REGISTRY_ID,CORD_LOCAL_CBU_ID,REGISTRY_MATERNAL_ID,MATERNAL_LOCAL_ID,CREATED_ON,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,COLUMN_NAME,OLD_VALUE,NEW_VALUE,REASON,REMARKS,COLUMN_DISPLAY_NAME FROM REP_ENT_STUS_RSN_AUDT_VIEW WHERE FK_ROW_ID=?";
	
	
	String AUDIT_TRAIL_ROW_MULTIPLE_VALUES_DATA = "select * from (select acm.column_name, acm.column_display_name, (select usr_logname from er_user where pk_user = arm.user_id) user_id, " +
	" to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, decode(arm.action,'I','Add','U','Update','-') action, acm.old_value, acm.new_value, arm.table_name,arm.timestamp " +
	" from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id " +
	" and (arm.module_id in (select pk_entity_status from cb_entity_status where entity_id = ? and status_type_code = 'eligibility') and arm.table_name = 'CB_ENTITY_STATUS') " +
	" and acm.column_name not in ('RID','IP_ADD','CREATED_ON','CREATOR','PK_ENTITY_STATUS','ENTITY_ID','IS_CORD_ENTRY_DATA','ORDER_ID','ORDER_FK_RESOL','STATUS_DATE','FK_ENTITY_TYPE','STATUS_TYPE_CODE','ADD_OR_UPDATE') union all select acm.column_name, acm.column_display_name, (select usr_logname from er_user where pk_user = arm.user_id) user_id, "+   
	" to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, decode(arm.action,'I','Add','U','Update','-') action, acm.old_value, acm.new_value, arm.table_name,arm.timestamp "+ 
	" from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id and (arm.module_id in (select pk_entity_status_reason from cb_entity_status_reason where fk_entity_status in "+ 
	" (select pk_entity_status from cb_entity_status where entity_id = ? and status_type_code= 'eligibility') and arm.table_name = 'CB_ENTITY_STATUS_REASON')) and acm.column_name not in ('RID','IP_ADD','CREATED_ON','CREATOR','PK_ENTITY_STATUS_REASON','DELETEDFLAG','FK_ATTACHMENTID','FK_FORMID','NOT_APP_PRIOR_FLAG','FK_ENTITY_STATUS')  " +
	"  )";
	
	
	String AUDIT_TRAIL_ROW_MULTIPLE_VALUES_DATA_COUNT ="select count(*) from ("+AUDIT_TRAIL_ROW_MULTIPLE_VALUES_DATA+")";
	
	 
	String AUDIT_TRAIL_COLUMN_MULTIPLE_VALUES_DATA = "SELECT FK_ROW_ID,CORD_REGISTRY_ID,CORD_LOCAL_CBU_ID,REGISTRY_MATERNAL_ID,MATERNAL_LOCAL_ID,CREATED_ON,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,COLUMN_NAME,OLD_VALUE,NEW_VALUE,REASON,REMARKS,COLUMN_DISPLAY_NAME FROM REP_MUL_RSN_AUDT_VIEW WHERE FK_ROW_ID=?";
	
	
	String AUDIT_TRAIL_ROW_FINALREVIEW_DATA = " select * from (select acm.column_name, acm.column_display_name, (select usr_logname from er_user where pk_user = arm.user_id) user_id,  to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, decode(arm.action,'I','Add','U','Update','-') action, "+
	" acm.old_value, acm.new_value, arm.table_name, arm.timestamp from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id and " +
	" (arm.module_id in (select pk_cord_final_review from cb_cord_final_review where fk_cord_id = ?) and arm.table_name = 'CB_CORD_FINAL_REVIEW') " +
	" and acm.column_name in ('LICENSURE_CONFIRM_FLAG','ELIGIBLE_CONFIRM_FLAG','REVIEW_CORD_ACCEPTABLE_FLAG','CONFIRM_CBB_SIGN_FLAG'))  ";
	
	String AUDIT_TRAIL_ROW_FINALREVIEW_DATA_COUNT ="select count(*) from ("+AUDIT_TRAIL_ROW_FINALREVIEW_DATA+")";
	
	String AUDIT_TRAIL_ROW_HLA_DATA ="select * from ( select acm.column_name, acm.column_display_name,  (select usr_logname from er_user where pk_user = arm.user_id) user_id,  to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, decode(arm.action,'I','Add','U','Update','-') action, "+
	" acm.old_value, acm.new_value, arm.table_name,arm.timestamp from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id and " +
	" (arm.module_id in (select pk_hla from cb_hla where entity_id = ? and  entity_type = (select pk_codelst from er_codelst where lower(codelst_type) ='entity_type' and lower(codelst_subtyp) = 'cbu')) and arm.table_name = 'CB_HLA') " +
	" and acm.column_name not in ('RID','IP_ADD','CREATED_ON','CREATOR','PK_HLA','ENTITY_ID','DELETEDFLAG','HLA_RECEIVED_DATE', 'FK_HLA_CODE_ID','FK_HLA_ANTIGENEID1','FK_HLA_ANTIGENEID2','FK_HLA_OVERCLS_CODE','HLA_CRIT_OVER_DATE','ENTITY_TYPE','LAST_MODIFIED_BY','LAST_MODIFIED_DATE','HLA_OVER_IND_ANTGN1','HLA_OVER_IND_ANTGN2','FK_HLA_PRIM_DNA_ID','FK_HLA_PROT_DIPLOID','FK_HLA_REPORTING_LAB','FK_HLA_REPORT_METHOD','FK_ORDER_ID','FK_CORD_CT_STATUS','FK_SPEC_TYPE','FK_CORD_CDR_CBU_ID','HLA_TYPING_SEQ','CB_HLA_ORDER_SEQ','HLA_REPORT_METHOD_CODE') " +
	" union  SELECT ac.column_name,ac.column_display_name,(SELECT usr_logname FROM er_user WHERE pk_user = ar.user_id) user_id ,TO_CHAR(ar.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp ,DECODE(ar.action,'D','Delete','-') action,ac.old_value,ac.new_value,ar.table_name,ar.timestamp FROM audit_column_module ac,audit_row_module ar WHERE ar.pk_row_id= ac.fk_row_id AND ar.action='D' AND ar.table_name ='CB_HLA' AND ac.fk_row_id IN(SELECT a.fk_row_id FROM(SELECT ac.fk_row_id fk_row_id FROM audit_column_module ac,audit_row_module ar WHERE ar.pk_row_id= ac.fk_row_id AND ar.action='D' AND ar.table_name ='CB_HLA' AND ac.old_value  = ? ) a,(SELECT ac.fk_row_id fk_row_id FROM audit_column_module ac,audit_row_module ar WHERE ar.pk_row_id= ac.fk_row_id AND ar.action ='D' AND ar.table_name ='CB_HLA' AND ac.old_value='CBU') b WHERE a.fk_row_id=b.fk_row_id) AND ac.column_name NOT IN ('RID','IP_ADD','CREATED_ON','CREATOR','PK_HLA','ENTITY_ID','DELETEDFLAG','HLA_RECEIVED_DATE','FK_HLA_ANTIGENEID1', 'FK_HLA_ANTIGENEID2','FK_HLA_OVERCLS_CODE','HLA_CRIT_OVER_DATE','ENTITY_TYPE','LAST_MODIFIED_BY','LAST_MODIFIED_DATE', 'FK_HLA_CODE_ID', 'HLA_OVER_IND_ANTGN1','HLA_OVER_IND_ANTGN2','FK_HLA_PRIM_DNA_ID','FK_HLA_PROT_DIPLOID','FK_HLA_REPORTING_LAB', 'FK_HLA_REPORT_METHOD','FK_ORDER_ID','FK_CORD_CT_STATUS','FK_SPEC_TYPE','FK_CORD_CDR_CBU_ID','HLA_TYPING_SEQ','CB_HLA_ORDER_SEQ','HLA_REPORT_METHOD_CODE'))";

	String AUDIT_TRAIL_ROW_HLA_DATA_COUNT ="select count(*) from ("+AUDIT_TRAIL_ROW_HLA_DATA+")";
	

	String AUDIT_TRAIL_ROW_PATHLA_DATA ="select * from ( select acm.column_name, acm.column_display_name,  (select usr_logname from er_user where pk_user = arm.user_id) user_id,  to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, decode(arm.action,'I','Add','U','Update','-') action, "+
	" acm.old_value, acm.new_value, arm.table_name,arm.timestamp from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id and " +
	" (arm.module_id in (select pk_hla from cb_hla where entity_id = ? and  entity_type = (select pk_codelst from er_codelst where lower(codelst_type) ='entity_type' and lower(codelst_subtyp) = 'patient')) and arm.table_name = 'CB_HLA') " +
	" and acm.column_name not in ('RID','IP_ADD','CREATED_ON','CREATOR','PK_HLA','ENTITY_ID','DELETEDFLAG','HLA_RECEIVED_DATE','FK_HLA_ANTIGENEID1','FK_HLA_ANTIGENEID2','FK_HLA_OVERCLS_CODE','HLA_CRIT_OVER_DATE','ENTITY_TYPE','LAST_MODIFIED_BY','LAST_MODIFIED_DATE','HLA_OVER_IND_ANTGN1','HLA_OVER_IND_ANTGN2','FK_HLA_PRIM_DNA_ID','FK_HLA_PROT_DIPLOID','FK_HLA_REPORTING_LAB','FK_HLA_REPORT_METHOD','FK_ORDER_ID','FK_CORD_CT_STATUS','FK_SPEC_TYPE','FK_CORD_CDR_CBU_ID','HLA_TYPING_SEQ','CB_HLA_ORDER_SEQ','HLA_REPORT_METHOD_CODE') " +
	" union SELECT ac.column_name,ac.column_display_name,(SELECT usr_logname FROM er_user WHERE pk_user = ar.user_id) user_id ,TO_CHAR(ar.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp ,DECODE(ar.action,'D','Delete','-') action,ac.old_value,ac.new_value,ar.table_name,ar.timestamp FROM audit_column_module ac,audit_row_module ar WHERE ar.pk_row_id= ac.fk_row_id AND ar.action='D' AND ar.table_name ='CB_HLA' AND ac.fk_row_id IN(SELECT a.fk_row_id FROM(SELECT ac.fk_row_id fk_row_id FROM audit_column_module ac,audit_row_module ar WHERE ar.pk_row_id= ac.fk_row_id AND ar.action='D' AND ar.table_name ='CB_HLA' AND ac.old_value  = ? ) a,(SELECT ac.fk_row_id fk_row_id FROM audit_column_module ac,audit_row_module ar WHERE ar.pk_row_id= ac.fk_row_id AND ar.action ='D' AND ar.table_name ='CB_HLA' AND ac.old_value='PATIENT') b WHERE a.fk_row_id=b.fk_row_id) AND ac.column_name NOT IN ('RID','IP_ADD','CREATED_ON','CREATOR','PK_HLA','ENTITY_ID','DELETEDFLAG','HLA_RECEIVED_DATE','FK_HLA_ANTIGENEID1', 'FK_HLA_ANTIGENEID2','FK_HLA_OVERCLS_CODE','HLA_CRIT_OVER_DATE','ENTITY_TYPE','LAST_MODIFIED_BY','LAST_MODIFIED_DATE', 'HLA_OVER_IND_ANTGN1','HLA_OVER_IND_ANTGN2','FK_HLA_PRIM_DNA_ID','FK_HLA_PROT_DIPLOID','FK_HLA_REPORTING_LAB', 'FK_HLA_REPORT_METHOD','FK_ORDER_ID','FK_CORD_CT_STATUS','FK_SPEC_TYPE','FK_CORD_CDR_CBU_ID','HLA_TYPING_SEQ','CB_HLA_ORDER_SEQ','HLA_REPORT_METHOD_CODE'))";
	String AUDIT_TRAIL_ROW_PATHLA_DATA_COUNT ="select count(*) from ("+AUDIT_TRAIL_ROW_PATHLA_DATA+")"; 

	
		
	String AUDIT_TRAIL_ROW_FORMS_IDM =" select * from ( select pk_form_version, (select forms_desc from cb_forms where pk_form = ver.fk_form and forms_desc ='IDM') formname, "+ 
		" (select version from cb_forms where pk_form = ver.fk_form and forms_desc ='IDM') fversion, (select usr_logname from er_user where pk_user = ver.creator) user_id, to_char(created_on,'Mon DD, YYYY HH:MI:SS AM'), created_on, " +
		" last_modified_by, last_modified_date from cb_form_version ver  where entity_id = ? and fk_form in (select pk_form from cb_forms where pk_form = ver.fk_form and forms_desc = 'IDM') order by pk_form_version desc) ";
	
	String AUDIT_TRAIL_ROW_FORMS_IDM_COUNT  = "select count(*) from ("+AUDIT_TRAIL_ROW_FORMS_IDM+")";

	String AUDIT_TRAIL_ROW_FORMS_HHS = "select * from (select pk_form_version, (select forms_desc from cb_forms where pk_form = ver.fk_form and forms_desc in ('FMHQ','MRQ')) formname, (select version from cb_forms where pk_form = ver.fk_form and forms_desc in ('FMHQ','MRQ')) fversion, "+ 
		" (select usr_logname from er_user where pk_user = ver.creator) user_id, to_char(created_on,'Mon DD, YYYY HH:MI:SS AM'), created_on, last_modified_by, last_modified_date from cb_form_version ver  where entity_id = ? and fk_form in (select pk_form from cb_forms where pk_form = ver.fk_form and forms_desc in ('FMHQ','MRQ')) order by pk_form_version desc) ";
	
	String AUDIT_TRAIL_ROW_FORMS_HHS_COUNT = "select count(*) from ("+AUDIT_TRAIL_ROW_FORMS_HHS+")";
	
	String AUDIT_TRAIL_ROW_LABSUMMARY_DATA = " select * from (select acm.column_name, acm.column_display_name, (select usr_logname from er_user where pk_user = arm.user_id) user_id, " +
			" to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, decode(arm.action,'I','Add','U','Update','-') action, acm.old_value, acm.new_value, arm.table_name,arm.timestamp,acm.pk_column_id from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id " + 
			" and (arm.module_id in (select pk_patlabs from er_patlabs where fk_specimen in (select fk_specimen_id from cb_cord where pk_cord = ?) and arm.table_name = 'ER_PATLABS')) "+ 
			" and acm.column_name not in ('RID','IP_ADD','CREATED_ON','CREATOR','PK_PATLABS','LAST_MODIFIED_DATE','LAST_MODIFIED_BY','FK_SPECIMEN','CUSTOM006','FK_PATPROT') union all " +
			" select acm.column_name, acm.column_display_name, (select usr_logname from er_user where pk_user = arm.user_id) user_id, to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, decode(arm.action,'I','Add','U','Update','-') action, acm.old_value, "+ 
			" acm.new_value, arm.table_name,arm.timestamp,acm.pk_column_id  from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id and (arm.module_id = ? and arm.table_name = 'CB_CORD') and acm.column_name  in " +
			" ('PRCSNG_START_DATE','FRZ_DATE','BACT_CULT_DATE','FK_CORD_BACT_CUL_RESULT','FK_CORD_FUNGAL_CUL_RESULT','FUNG_CULT_DATE','FK_CORD_ABO_BLOOD_TYPE','FK_CORD_RH_TYPE','HEMOGLOBIN_SCRN','CB_RHTYPE_OTHER_SPEC','BACT_COMMENT','FUNG_COMMENT') union all " +
			" select acm.column_name, acm.column_display_name, (select usr_logname from er_user where pk_user = arm.user_id) user_id, " + 
			" to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, decode(arm.action,'I','Add','U','Update','-') action, acm.old_value, acm.new_value, arm.table_name,arm.timestamp,acm.pk_column_id  from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id "+ 
			" and (arm.module_id in (select pk_assessment from cb_assessment where entity_id = ? and entity_type in (f_codelst_id('entity_type','CBU')) and sub_entity_id in (f_codelst_id('fung_cul','not_done'),f_codelst_id('hem_path_scrn','alp_thal_sil'),f_codelst_id('hem_path_scrn','not_done'), "+
			" f_codelst_id('hem_path_scrn','hemo_trait'),f_codelst_id('hem_path_scrn','homo_no_dis'),f_codelst_id('hem_path_scrn','alp_thal_trait'),f_codelst_id('hem_path_scrn','unknown'))) and arm.table_name = 'CB_ASSESSMENT') and acm.column_name  in "+ 
			" ('ASSESSMENT_REMARKS','ASSESSMENT_FOR_RESPONSE','TC_VISIBILITY_FLAG','ASSESSMENT_REASON','FLAG_FOR_LATER','ASSESSMENT_FLAG_COMMENT') )";
	
	String AUDIT_TRAIL_ROW_LABSUMMARY_DATA_COUNT ="select count(*) from ("+AUDIT_TRAIL_ROW_LABSUMMARY_DATA+")";
	 
	
	String AUDIT_TRAIL_ROW_NOTES_DATA = " select * from (select acm.column_name, acm.column_display_name, (select usr_logname from er_user where pk_user = arm.user_id) user_id, " +
	" to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, decode(arm.action,'I','Add','U','Update','-') action, acm.old_value, acm.new_value, arm.table_name, arm.timestamp from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id " + 
	" and (arm.module_id in (select pk_notes from cb_notes where entity_id = ? and fk_notes_type = (select pk_codelst from er_codelst where codelst_type ='note_type' and codelst_subtyp = 'clinic' )) and arm.table_name = 'CB_NOTES') and acm.column_name "+ 
	" in ('NOTES_CLINIC','VISIBILITY','KEYWORD','FLAG_FOR_LATER','FK_NOTES_TYPE','FK_NOTES_CATEGORY','COMMENTS','NOTE_ASSESSMENT','AMENDED','NOTE_SEQ','REPLY')) ";

	
	
	String AUDIT_TRAIL_ROW_NOTES_DATA_COUNT ="select count(*) from ("+AUDIT_TRAIL_ROW_NOTES_DATA+")";
	 
	String AUDIT_TRAIL_PF_DATA =" select * from (select acm.column_name, acm.column_display_name, (select usr_logname from er_user where pk_user = arm.user_id) user_id, " +
	" to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, decode(arm.action,'I','Add','U','Update','-') action, acm.old_value, acm.new_value, arm.table_name,arm.timestamp from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id " + 
	" and (arm.module_id in (select pk_order_header from er_order_header where order_entityid = ?) and arm.table_name = 'ER_ORDER_HEADER') and acm.column_name in ('ORDER_OPEN_DATE','FK_SITE_ID') "+ 
	" union all select acm.column_name, acm.column_display_name, (select usr_logname from er_user where pk_user = arm.user_id) user_id, "+
	" to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, decode(arm.action,'I','Add','U','Update','-') action, acm.old_value, acm.new_value, arm.table_name,arm.timestamp from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id "+ 
	" and (arm.module_id in (select pk_order from er_order where fk_order_header in (select pk_order_header from er_order_header where order_entityid = ?) and arm.table_name = 'ER_ORDER')) and acm.column_name not in ('RID','CREATED_ON','CREATOR','LAST_MODIFIED_DATE','LAST_MODIFIED_BY','IP_ADD','PK_ORDER','ORDER_TYPE','FK_ORDER_HEADER'," +
	" 'CBU_AVAIL_CONFIRM_FLAG','FK_ATTACHMENT','TASK_ID','TASK_NAME','DELETEDFLAG','CORD_AVAILABILITY_CONFORMEDON'," +
	" 'CLINIC_INFO_CHECKLIST_STAT','ADDI_TEST_RESULT_AVAIL','CANCEL_CONFORM_DATE','CANCELED_BY','ACCPT_TO_CANCEL_REQ','FK_CASE_MANAGER','ORDER_NUMBER','ORDER_ENTITYID','ORDER_ENTITYTYPE','FK_ORDER_STATUS','ORDER_REMARK','ORDER_BY','FK_ORDER_ITEM_ID','FK_ORDER_TYPE','DATA_MODIFIED_FLAG','FINAL_REVIEW_TASK_FLAG','COMPLETE_REQ_INFO_TASK_FLAG') "+ 
	" union all select acm.column_name, acm.column_display_name, (select usr_logname from er_user where pk_user = arm.user_id) user_id, to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, decode(arm.action,'I','Add','U','Update','-') action, acm.old_value, acm.new_value, arm.table_name,arm.timestamp "+
	" from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id and (arm.module_id in (select pk_shipment from cb_shipment where fk_cord_id = ?) and arm.table_name = 'CB_SHIPMENT') and acm.column_name "+ 
	" not in ('PK_SHIPMENT','FK_ORDER_ID','FK_CORD_ID','RID','IP_ADD','CREATED_ON','CREATOR','LAST_MODIFIED_DATE','LAST_MODIFIED_BY','DATA_MODIFIED_FLAG','FK_SHIPMENT_TYPE','CBB_ID','FK_CORD_ID','DELETEDFLAG') union all select acm.column_name, acm.column_display_name, (select usr_logname from er_user where pk_user = arm.user_id) user_id, to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, "+ 
	" decode(arm.action,'I','Add','U','Update','-') action, acm.old_value, acm.new_value, arm.table_name,arm.timestamp from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id and (arm.module_id in (select pk_order_receipant_info from er_order_receipant_info where fk_cord_id = ?) and arm.table_name = 'ER_ORDER_RECEIPANT_INFO') and acm.column_name "+ 
	" not in ('PK_ORDER_RECEIPANT_INFO','FK_RECEIPANT','FK_ORDER_ID','FK_CORD_ID','RID','ENTITY_ID','CREATED_ON','CREATOR','LAST_MODIFIED_DATE','LAST_MODIFIED_BY','IP_ADD','NMDP_RID','NMDP_ID','LOCAL_ID','COOP_REF','TC_CDE','SCU_ACTIVATED_DATE','FK_SITE_ID') union all " +
	" select acm.column_name, acm.column_display_name, (select usr_logname from er_user where pk_user = arm.user_id) user_id, to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM'), decode(arm.action,'I','Add','U','Update','-') action, acm.old_value, acm.new_value, "+
	" arm.table_name,arm.timestamp from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id and (arm.module_id in (select pk_notes from cb_notes where entity_id = ? and fk_notes_type = (select pk_codelst from er_codelst where codelst_type ='note_type' and codelst_subtyp = 'progress' )) and arm.table_name = 'CB_NOTES') and acm.column_name "+ 
	" in ('SUBJECT','KEYWORD','TAG_NOTE','NOTES_PROGRESS','FK_NOTES_CATEGORY','NOTE_VISIBLE','REQUEST_TYPE','REQUEST_DATE','AMENDED','NOTE_SEQ') union all " +
	" select acm.column_name, acm.column_display_name, (select usr_logname from er_user where pk_user = arm.user_id) user_id, to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, decode(arm.action,'I','Add','U','Update','-') action, "+
	" acm.old_value, acm.new_value, arm.table_name,arm.timestamp from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id and (arm.module_id in (select order_requested_by from er_order_header where order_entityid = ?) and arm.table_name = 'ER_ORDER_USERS') and acm.column_name "+ 
	" not in ('RID','DELETEDFLAG','USER_COMMENTS','PK_ORDER_USER','IP_ADD','CREATED_ON','CREATOR','LAST_MODIFIED_DATE','LAST_MODIFIED_BY') " +
	" union all select acm.column_name, acm.column_display_name, (select usr_logname from er_user where pk_user = arm.user_id) user_id, to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, decode(arm.action,'I','Add','U','Update','-') action, "+ 
	" acm.old_value, acm.new_value, arm.table_name,arm.timestamp from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id and (arm.module_id in (select FK_RECEIPANT from er_order_receipant_info where FK_CORD_ID= ?) and arm.table_name = 'CB_RECEIPANT_INFO') and acm.column_name "+  
	" not in ('PK_RECEIPANT','FK_PERSON_ID','REC_WEIGHT','ENTRY_DATE','GUID','REC_HEIGHT','RID','DELETEDFLAG','IP_ADD','CREATED_ON','CREATOR','LAST_MODIFIED_DATE','LAST_MODIFIED_BY')  " +
	" union all select acm.column_name, acm.column_display_name, nvl((select usr_logname from er_user where pk_user = arm.user_id),'System') user_id, to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, decode(arm.action,'I','Add','U','Update','-') action,	acm.old_value, acm.new_value, arm.table_name,arm.timestamp from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id and (arm.module_id in (select PK_CORD_COMPLETE_REQINFO from CB_CORD_COMPLETE_REQ_INFO where FK_CORD_ID = ?) and arm.table_name = 'CB_CORD_COMPLETE_REQ_INFO') and acm.column_name "+  
	" in ('COMPLETE_REQ_INFO_FLAG') ) ";
	
	String AUDIT_TRAIL_PF_DATA_COUNT ="select count(*) from ("+AUDIT_TRAIL_PF_DATA+")";
	
	String AUDIT_TRAIL_BEST_HLA = " select acm.column_name, acm.column_display_name, (select usr_logname from er_user where pk_user = arm.user_id) user_id, "+
			" to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, decode(arm.action,'I','Add','U','Update','-') action, acm.old_value, acm.new_value, arm.table_name from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id "+ 
			" and (arm.module_id in (select pk_best_hla from cb_best_hla where entity_id = ?) and arm.table_name = 'CB_BEST_HLA') and acm.column_name "+ 
			" not in ('RID','IP_ADD','ENTITY_ID','ENTITY_TYPE','CREATED_ON','CREATOR','PK_BEST_HLA','LAST_MODIFIED_DATE','LAST_MODIFIED_BY') order by tstamp desc ";
	
	
	String AUDIT_TRAIL_ID_DATA = " select * from ( select  acm.column_name, acm.column_display_name, (select usr_logname from er_user where pk_user = arm.user_id) user_id, "+ 
	 " to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, decode(arm.action,'I','Add','U','Update','-') action, acm.old_value, acm.new_value, arm.table_name,arm.timestamp,acm.pk_column_id  from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id and (arm.module_id = ? and arm.table_name = 'CB_CORD') and acm.column_name  in "+ 
	 " ('CORD_REGISTRY_ID','CORD_LOCAL_CBU_ID','CORD_ID_NUMBER_ON_CBU_BAG','CORD_ISBI_DIN_CODE','MATERNAL_LOCAL_ID','REGISTRY_MATERNAL_ID','CORD_HISTORIC_CBU_ID','CB_IDS_CHANGE_REASON') union all "+
	 " select  acm.column_name, acm.column_display_name, (select usr_logname from er_user where pk_user = arm.user_id) user_id, to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, decode(arm.action,'I','Add','U','Update','-') action, acm.old_value, "+
	 " acm.new_value, arm.table_name,arm.timestamp,acm.pk_column_id  from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id and (arm.module_id in (select PK_CB_ADDITIONAL_IDS from CB_ADDITIONAL_IDS where entity_id = ?) and arm.table_name = 'CB_ADDITIONAL_IDS') and acm.column_name  in "+ 
	 " ('CB_ADDITIONAL_ID_DESC','FK_CB_ADDITIONAL_ID_TYPE','CB_ADDITIONAL_ID') ) ";
	
	String AUDIT_TRAIL_ID_DATA_COUNT  = "select count(*) from ("+AUDIT_TRAIL_ID_DATA+")";
	
	
	String AUDIT_TRAIL_PROCINFO_DATA ="select * from ( select acm.column_name, acm.column_display_name, (select usr_logname from er_user where pk_user = arm.user_id) user_id, to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, decode(arm.action,'I','Add','U','Update','-') action, "+  
	" acm.old_value, acm.new_value, arm.table_name,arm.timestamp from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id and (arm.module_id in (select pk_entity_samples from cb_entity_samples where entity_id = ?) "+
	" and arm.table_name = 'CB_ENTITY_SAMPLES') and acm.column_name not in ('PK_ENTITY_SAMPLES','ENTITY_ID','ENTITY_TYPE','CREATOR','CREATED_ON','RID','IP_ADD','LAST_MODIFIED_DATE','LAST_MODIFIED_BY','FK_SPECIMEN','DELETEDFLAG','FK_SAMPLE_TYPE','FK_SAMPLE_STATUS','SEQUENCE') union all select acm.column_name, acm.column_display_name, (select usr_logname from er_user where pk_user = arm.user_id) user_id, "+ 
	" to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, decode(arm.action,'I','Add','U','Update','-') action, acm.old_value, acm.new_value, arm.table_name,arm.timestamp  from audit_column_module acm, audit_row_module arm where acm.fk_row_id = arm.pk_row_id and (arm.module_id = ? and arm.table_name = 'CB_CORD') and acm.column_name  in ('FK_CBB_PROCEDURE') ) ";
	
	String AUDIT_TRAIL_PROCINFO_DATA_COUNT = "select count(*) from ("+AUDIT_TRAIL_PROCINFO_DATA+")";
	
	String AUDIT_TRAIL_MIN_CRITERIA_DATA=" select * from (SELECT ACM.COLUMN_NAME,ACM.COLUMN_DISPLAY_NAME, (SELECT USR_LOGNAME FROM ER_USER WHERE " +
	" PK_USER = ARM.USER_ID  ) USER_ID, TO_CHAR(ARM.TIMESTAMP,'Mon DD, YYYY HH:MI:SS AM') TSTAMP," +
	" DECODE(ARM.ACTION,'I','Add','U','Update','-') ACTION,  ACM.OLD_VALUE,ACM.NEW_VALUE, ARM.TABLE_NAME,ARM.TIMESTAMP " +
	" FROM AUDIT_ROW_MODULE ARM ,  AUDIT_COLUMN_MODULE ACM WHERE ACM.FK_ROW_ID  = ARM.PK_ROW_ID AND (ARM.MODULE_ID   " +
	"  in (SELECT PK_CORD_MINIMUM_CRITERIA FROM CB_CORD_MINIMUM_CRITERIA  WHERE FK_CORD_ID=? ) AND ARM.TABLE_NAME   " +
	" = 'CB_CORD_MINIMUM_CRITERIA') AND ACM.COLUMN_NAME IN ('ANTIHIV_II_HIVP24_HIVNAT_COMP','HBSAG_COMP','DECLARATION_RESULT','ANTIHCV_COMP','REVIEWED_BY','SECOND_REVIEW_BY', 'HLA_CNFM','ANTIGEN_MATCH','HIVPNAT_COMP','HLACHILD', 'CTCOMPLETED','CTCOMPLETED_CHILD','ANTIGENCHILD', 'UNITRECIPIENT','UNITRECIPIENT_CHILD','CBUPLANNED', 'CBUPLANNED_CHILD','PATIENTTYPING','PATIENTTYPING_CHILD','POSTPROCESS_CHILD','VIABILITY_CHILD') UNION ALL SELECT acm.column_name," +
	"  acm.column_display_name,  (SELECT usr_logname  FROM er_user  WHERE pk_user = arm.user_id  ) user_id," +
	"  TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp,  DECODE(arm.action,'I','Add','U','Update','-') action," +
	"  acm.old_value,  acm.new_value,  arm.table_name,  arm.timestamp FROM audit_column_module acm," +
	"  audit_row_module arm WHERE ACM.FK_ROW_ID = ARM.PK_ROW_ID AND (ARM.MODULE_ID IN " +
	" ( SELECT  PK_MINIMUM_CRITERIA_FIELD  FROM CB_CORD_TEMP_MINIMUM_CRITERIA" +
	"  WHERE FK_MINIMUM_CRITERIA=(select PK_CORD_MINIMUM_CRITERIA from CB_CORD_MINIMUM_CRITERIA where FK_CORD_ID=? ) ) " +
	" AND ARM.TABLE_NAME   = 'CB_CORD_TEMP_MINIMUM_CRITERIA')AND ACM.COLUMN_NAME IN ('BACTERIAL_RESULT','FUNGAL_RESULT'," +
	" 'HEMOGLOB_RESULT','HIVRESULT','REVIEWED_BY','SECOND_REVIEW_BY','HIVPRESULT','HBSAG_RESULT','HBVNAT_RESULT','HCV_RESULT','T_CRUZI_CHAGAS_RESULT'," +
	" 'WNV_RESULT','TNCKG_PT_WEIGHT_RESULT','VIABILITY_RESULT') ) ";
	
	String AUDIT_TRAIL_MIN_CRITERIA_DATA_COUNT = "select count(*) from ("+AUDIT_TRAIL_MIN_CRITERIA_DATA+")";
		 
	String CORD_ROW_LEVEL = "cordrowlevel";  
	String ENTITY_STATUS_ROW_LEVEL = "entitystatusrowlevel";
	String ENTITY_STATUS_REASON_ROW_LEVEL = "entitystatusreasonrowlevel"; 
	String MULTIPLE_VALUE_ROW_LEVEL = "multiplevaluerowlevel";
	String CORD_COLUMN_LEVEL = "cordcolumnlevel";
	String ENTITY_STATUS_COLUMN_LEVEL = "entitystatuscolumnlevel";
	String ENTITY_STATUS_REASON_COLUMN_LEVEL = "entitystatusreasoncolumnlevel";
	String MULTIPLE_VALUE_COLUMN_LEVEL = "multiplevaluecolumnlevel";
	String REVIEW_ROW_LEVEL = "finalcbureviewrow";
	String HLA_ROW_LEVEL = "hlarow";
	String PATHLA_ROW_LEVEL = "pathlarow";
	String LABSUMMARY_ROW_LEVEL = "labsummaryrow";
	String NOTES_ROW_LEVEL = "notesrow";
	String FORMS_ROW_LEVEL_IDM = "formsrow";
	String FORMS_ROW_LEVEL_HHS = "formsrowhhs";
	String PF_ROW_LEVEL = "pfrow";
	String BEST_HLA_ROW_LEVEL = "besthlarow";
	String ID_ROW_LEVEL = "idrow";
	String PROC_INFO_ROW_LEVEL = "procinforow";
	String MIN_INFO_ROW_LEVEL = "mininforow";
	
	
	
}