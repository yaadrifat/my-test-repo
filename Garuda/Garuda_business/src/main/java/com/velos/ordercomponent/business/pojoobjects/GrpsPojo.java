package com.velos.ordercomponent.business.pojoobjects;

public class GrpsPojo extends AuditablePojo{
	
	private Long pkGrp;
	private String grpName;
	private String grpDesc;
	private Long fkAccount;
	private String grpRights;
	private String grpSupUsrRights;
	private Long grpSupUsrFlag;
	private Long grpHidden;
	
	public Long getPkGrp() {
		return pkGrp;
	}
	public void setPkGrp(Long pkGrp) {
		this.pkGrp = pkGrp;
	}
	public String getGrpName() {
		return grpName;
	}
	public void setGrpName(String grpName) {
		this.grpName = grpName;
	}
	public String getGrpDesc() {
		return grpDesc;
	}
	public void setGrpDesc(String grpDesc) {
		this.grpDesc = grpDesc;
	}
	public Long getFkAccount() {
		return fkAccount;
	}
	public void setFkAccount(Long fkAccount) {
		this.fkAccount = fkAccount;
	}
	public String getGrpRights() {
		return grpRights;
	}
	public void setGrpRights(String grpRights) {
		this.grpRights = grpRights;
	}
	public String getGrpSupUsrRights() {
		return grpSupUsrRights;
	}
	public void setGrpSupUsrRights(String grpSupUsrRights) {
		this.grpSupUsrRights = grpSupUsrRights;
	}
	public Long getGrpSupUsrFlag() {
		return grpSupUsrFlag;
	}
	public void setGrpSupUsrFlag(Long grpSupUsrFlag) {
		this.grpSupUsrFlag = grpSupUsrFlag;
	}
	public Long getGrpHidden() {
		return grpHidden;
	}
	public void setGrpHidden(Long grpHidden) {
		this.grpHidden = grpHidden;
	}
	

}
