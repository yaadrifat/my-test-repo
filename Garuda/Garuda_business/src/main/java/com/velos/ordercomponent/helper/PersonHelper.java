package com.velos.ordercomponent.helper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;

import com.velos.ordercomponent.business.domain.Address;
import com.velos.ordercomponent.business.domain.Person;
import com.velos.ordercomponent.business.pojoobjects.AddressPojo;
import com.velos.ordercomponent.business.pojoobjects.PersonPojo;
import com.velos.ordercomponent.business.util.HibernateUtil;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.VelosUtil;

public class PersonHelper {
	public static final Log log=LogFactory.getLog(PersonHelper.class);
	
	public static PersonPojo savePerson(PersonPojo personPojo){
		return savePerson(personPojo, null);
	}
	
    public static PersonPojo savePerson(PersonPojo personPojo,Session session){
    	//log.debug("savePerson with session");
		//log.debug("PersonHelper : savePerson Start ");
		Person person = new Person();
		boolean sessionFlag = false;
		try{
		 // check session 
		if(session== null){
			// create new session
		  session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}
		person = (Person)ObjectTransfer.transferObjects(personPojo, person);
		person = (Person)new VelosUtil().setAuditableInfo(person);
		if(person.getPersonId()!=null)
		{session.merge(person);
		}else{
			session.save(person);
		}
		personPojo = (PersonPojo)ObjectTransfer.transferObjects(person, personPojo);
		if(sessionFlag){
			// close session, if session created in this method
			session.getTransaction().commit();
		}
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		//log.debug("PersonHelper : savePerson Method End ");
		return personPojo;
	}
    
    public static PersonPojo getPersonById(PersonPojo personPojo)
    {
    	return getPersonById(personPojo, null);
    }
    
    public static PersonPojo getPersonById(PersonPojo personPojo,Session session){
    	//log.debug("getPersonById with session");
		//log.debug("PersonHelper : getPersonById Start ");
		Person person = new Person();
		boolean sessionFlag = false;
		try{
		 // check session 
		if(session== null){
			// create new session
		  session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}
		person = (Person)ObjectTransfer.transferObjects(personPojo, person);		
		if(person.getPersonId()!=null)
		{
			person = (Person)session.load(Person.class, person.getPersonId());
		}
		if(person !=null){
		personPojo = (PersonPojo)ObjectTransfer.transferObjects(person, personPojo);
		}
		
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		//log.debug("PersonHelper : getPersonById Method End ");
		return personPojo;
	} 
}
