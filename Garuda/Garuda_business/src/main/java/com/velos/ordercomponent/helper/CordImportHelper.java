package com.velos.ordercomponent.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.hibernate.SQLQuery;
import org.hibernate.classic.Session;

import com.opensymphony.xwork2.ActionContext;
import com.velos.eres.web.user.UserJB;
import com.velos.ordercomponent.business.domain.CordImportHistory;
import com.velos.ordercomponent.business.domain.CordImportProcessMessages;
import com.velos.ordercomponent.business.domain.UserDomain;
import com.velos.ordercomponent.business.pojoobjects.CordImportHistoryPojo;
import com.velos.ordercomponent.business.pojoobjects.CordImportProcessMessagesPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.util.HibernateUtil;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.VelosMidConstants;
import com.velos.ordercomponent.business.util.VelosUtil;
import com.velos.ordercomponent.exception.MidErrorConstants;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class CordImportHelper {

	public static final Log log = LogFactory.getLog(CordImportHelper.class);
	
	public static CordImportHistoryPojo saveCordImportHistory(CordImportHistoryPojo historyPojo){
		return saveCordImportHistory(historyPojo,null) ;
	}
	
	public static CordImportHistoryPojo saveCordImportHistory(CordImportHistoryPojo historyPojo,Session session){
		boolean sessionFlag = false;
		CordImportHistory history =  new CordImportHistory();
		history = (CordImportHistory)ObjectTransfer.transferObjects(historyPojo,history);		
		try {
			if (session == null) {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if(history.getPkCordimportHistory()!=null){
			  session.merge(history);
			}else{
				session.save(history);
			}
			historyPojo = (CordImportHistoryPojo)ObjectTransfer.transferObjects(history, historyPojo);			
		} catch (Exception e) {
			//log.debug(" Exception in saveCordImportHistory " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			
			 try {
				session.getTransaction().commit();
			}catch (Exception e) {
				log.error(e.getMessage());
			    e.printStackTrace();
			 }finally{
				if(session.isOpen()){
				    session.close();
				}
			}
	  }
		return historyPojo;
	}
	
	public static List<CordImportHistoryPojo> getCordImportHistory(Long pkCordImportHistory,Long userPK,PaginateSearch paginateSearch){
		return getCordImportHistory(pkCordImportHistory,userPK,paginateSearch,null) ;
	}
	
	public static List<CordImportHistoryPojo> getCordImportHistory(Long pkCordImportHistory,Long userPk,PaginateSearch paginateSearch,Session session){
		List<CordImportHistoryPojo> historyPojos = new ArrayList<CordImportHistoryPojo>();
		CordImportHistory history =  null;
		UserDomain user = null;
		List<Object> listObj = new ArrayList<Object>();
		CordImportHistoryPojo historyPojo = null;
		boolean sessionFlag = false;
		UserJB userObject = null;
		try {
			if (session == null) {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "select {h.*},{u.*} from CB_CORD_IMPORT_HISTORY h left join ER_USER u on h.CREATOR=u.PK_USER";
			if(pkCordImportHistory!=null){
				sql += " where h.PK_CORD_IMPORT_HISTORY=?";
			}else{
			sql += " where h.FK_SITE_ID IN  " +
					" (SELECT fk_site  FROM er_usersite  WHERE er_usersite.fk_user ="+userPk+"  AND er_usersite.usersite_right<>0  )" +
							" or u.PK_USER ="+userPk;
			}
			sql += " order by h.PK_CORD_IMPORT_HISTORY desc";
			List<CordImportHistory> list = new ArrayList<CordImportHistory>();
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity("h",CordImportHistory.class);
			query.addEntity("u",UserDomain.class);
			if(pkCordImportHistory!=null){
				query.setLong(0,pkCordImportHistory);
			}
			if(paginateSearch!=null){
				query.setFirstResult(paginateSearch.getiPageNo());
				query.setMaxResults(paginateSearch.getiShowRows());
			}
			listObj = (List<Object>)query.list();
			for(Object o : listObj){
				Object[] oArr = (Object[])o;
				history = new CordImportHistory();
				user = new UserDomain();
				history = (CordImportHistory)oArr[0];
				user = (UserDomain)oArr[1];
				historyPojo = new CordImportHistoryPojo();
				historyPojo = (CordImportHistoryPojo)ObjectTransfer.transferObjects(history, historyPojo);
				historyPojo.setUserDomain(user);		
				historyPojos.add(historyPojo);
			}	
			
		} catch (Exception e) {
			//log.debug(" Exception in getCordImportHistory " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			
			 try {
				session.getTransaction().commit();
			}catch (Exception e) {
				log.error(e.getMessage());
			    e.printStackTrace();
			 }finally{
				if(session.isOpen()){
				    session.close();
				}
			}
	  }
		return historyPojos;
	}
	
	
	public static List getCordImportHistoryJson(Long pkCordImportHistory,Long userPK,PaginateSearch paginateSearch,String condition, String orderby){
		return getCordImportHistoryJson(pkCordImportHistory,userPK,paginateSearch,condition,orderby,null) ;
	}
	
	@SuppressWarnings("unchecked")
	public static List getCordImportHistoryJson(Long pkCordImportHistory,Long userPk,PaginateSearch paginateSearch,String condition, String orderby,Session session){
		List<CordImportHistoryPojo> historyPojos = new ArrayList<CordImportHistoryPojo>();
		CordImportHistory history =  null;
		UserDomain user = null;
		List<Object> listObj = new ArrayList<Object>();
		CordImportHistoryPojo historyPojo = null;
		boolean sessionFlag = false;
		UserJB userObject = null;
		try {
			if (session == null) {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "select h.PK_CORD_IMPORT_HISTORY,to_char(h.START_TIME,'Mon DD, YYYY HH:MM:SS AM') Starttime,h.IMPORT_FILE_NAME,to_char(h.END_TIME,'Mon DD, YYYY HH:MM:SS AM') endtime,h.IMPORT_HISTORY_DESC,u.USR_FIRSTNAME,u.USR_LASTNAME from CB_CORD_IMPORT_HISTORY h left join ER_USER u on h.CREATOR=u.PK_USER";
			if(pkCordImportHistory!=null){
				sql += " where h.PK_CORD_IMPORT_HISTORY=?"+condition+orderby;
			}else{
			sql += " where (h.FK_SITE_ID IN  " +
					" (SELECT fk_site  FROM er_usersite  WHERE er_usersite.fk_user ="+userPk+"  AND er_usersite.usersite_right<>0  )" +
							" or u.PK_USER ="+userPk+") "+condition+orderby;
			}
	
			//System.out.println("\n\n\n\n\n\n\n\n\nsql::::::::::::::::::::::"+sql);
			List<CordImportHistory> list = new ArrayList<CordImportHistory>();
			SQLQuery query = session.createSQLQuery(sql);
		
			if(pkCordImportHistory!=null){
				query.setLong(0,pkCordImportHistory);
			}
			if(paginateSearch!=null){
				query.setFirstResult(paginateSearch.getiPageNo());
				query.setMaxResults(paginateSearch.getiShowRows());
			}
			
			
			listObj = query.list();
			//System.out.println("\n\n\n\n\n\n\n\n\nSize in helper::::::::::::::::::::::"+listObj.size());
			
		} catch (Exception e) {
			//log.debug(" Exception in getCordImportHistory " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			
			 try {
				session.getTransaction().commit();
			}catch (Exception e) {
				log.error(e.getMessage());
			    e.printStackTrace();
			 }finally{
				if(session.isOpen()){
				    session.close();
				}
			}
	  }
		return listObj;
	}
	public static List getCordImportHistoryCount(Long userPK,String condition){
		return getCordImportHistoryCount(userPK,condition,null) ;
	}
	
	public static List getCordImportHistoryCount(Long userPk,String condition,Session session){

		UserDomain user = null;
		List<Object> listObj = new ArrayList<Object>();
		boolean sessionFlag = false;
		try {
			if (session == null) {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "select count(*) from CB_CORD_IMPORT_HISTORY h left join ER_USER u on h.CREATOR=u.PK_USER";
			sql += " where (h.FK_SITE_ID IN  " +
					" (SELECT fk_site  FROM er_usersite  WHERE er_usersite.fk_user ="+userPk+"  AND er_usersite.usersite_right<>0  )" +
							" or u.PK_USER ="+userPk+") "+condition;
	
			//System.out.println("\n\n\n\n\n\n\n\n\n In count query sql::::::::::::::::::::::"+sql);
			List<CordImportHistory> list = new ArrayList<CordImportHistory>();
			SQLQuery query = session.createSQLQuery(sql);
		
			
			listObj = query.list();
			//System.out.println("\n\n\n\n\n\n\n\n\nSize in helper::::::::::::::::::::::"+listObj.get(0).toString());
			
		} catch (Exception e) {
			//log.debug(" Exception in getCordImportHistory " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			
			 try {
				session.getTransaction().commit();
			}catch (Exception e) {
				log.error(e.getMessage());
			    e.printStackTrace();
			 }finally{
				if(session.isOpen()){
				    session.close();
				}
			}
	  }
		return listObj;
	}
	
	public static List<CordImportProcessMessagesPojo> saveCordImportErrorMessages(List<CordImportProcessMessagesPojo> messagesPojos){
		return saveCordImportErrorMessages(messagesPojos, null);
	}
	
   public static List<CordImportProcessMessagesPojo> saveCordImportErrorMessages(List<CordImportProcessMessagesPojo> messagesPojos,Session session){
	   List<CordImportProcessMessages> messages = new ArrayList<CordImportProcessMessages>();
	   CordImportProcessMessages importProcessMessages = null;
	   CordImportProcessMessagesPojo messagesPojo = null;
	   for(CordImportProcessMessagesPojo mp : messagesPojos){
		   importProcessMessages = new CordImportProcessMessages();
		   importProcessMessages = (CordImportProcessMessages)ObjectTransfer.transferObjects(mp,importProcessMessages);
		   messages.add(importProcessMessages);
	   }
	   messagesPojos.clear();
	   for(CordImportProcessMessages m : messages){
		 //  m = (CordImportProcessMessages)new VelosUtil().setAuditableInfo(m);
		   m = saveCordImportProcessMessage(m);
		   messagesPojo = (CordImportProcessMessagesPojo)ObjectTransfer.transferObjects(m,messagesPojo);
		   messagesPojos.add(messagesPojo);
	   }	   
	   return messagesPojos;
	}
   
   public static CordImportProcessMessages saveCordImportProcessMessage(CordImportProcessMessages processMessage){
	   return saveCordImportProcessMessage(processMessage,null);
   }
   
   public static CordImportProcessMessages saveCordImportProcessMessage(CordImportProcessMessages processMessage,Session session){
	   boolean sessionFlag = false;
		try {
			if (session == null) {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if(processMessage.getPkCordImportProcessMessage()!=null){
				session.merge(processMessage);
			}else{
				session.save(processMessage);
			}
			
		} catch (Exception e) {
			//log.debug(" Exception in saveCordImportProcessMessage " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			
			 try {
				session.getTransaction().commit();
			}catch (Exception e) {
				log.error(e.getMessage());
			    e.printStackTrace();
			 }finally{
				if(session.isOpen()){
				    session.close();
				}
			}
	  }
		return processMessage;
   }
   
   public static List<CordImportProcessMessagesPojo> getCordImportErrorMessagesByHistoryId(Long importHistoryId,Integer level,String registryId){
		return getCordImportErrorMessagesByHistoryId(importHistoryId,level,registryId,null);
	}
	
   public static List<CordImportProcessMessagesPojo> getCordImportErrorMessagesByHistoryId(Long importHistoryId,Integer level,String registryId,Session session){
	  List<CordImportProcessMessagesPojo> processMessagesPojos = new ArrayList<CordImportProcessMessagesPojo>();
	  List<CordImportProcessMessages> messages = new ArrayList<CordImportProcessMessages>();
	  CordImportProcessMessagesPojo messagesPojo = null;
	  boolean sessionFlag = false;
	  boolean whereFlag = false;
		try {
			if (session == null) {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "From CordImportProcessMessages";
			if(importHistoryId!=null){
				sql += " where fkCordImportHistory="+importHistoryId;
				whereFlag = true;
			}
			if(level!=null){
				if(whereFlag){
					sql += " and level="+level;
				}else{
					sql += " where level="+level;
					whereFlag = true;
				}
			}
			if(!StringUtils.isEmpty(registryId)){
				if(whereFlag){
					sql += " and cbuRegistryId='"+registryId+"'";
				}else{
					sql += " where cbuRegistryId='"+registryId+"'";
					whereFlag = true;
				}
			}
			sql += " order by messageId";
			messages = (List<CordImportProcessMessages>)session.createQuery(sql).list();
			for(CordImportProcessMessages m : messages){
				messagesPojo = new CordImportProcessMessagesPojo();
			    messagesPojo = (CordImportProcessMessagesPojo)ObjectTransfer.transferObjects(m,messagesPojo);
			    processMessagesPojos.add(messagesPojo);
			}	
		} catch (Exception e) {
			//log.debug(" Exception in getCordImportErrorMessagesByHistoryId " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			
			 try {
				session.getTransaction().commit();
			}catch (Exception e) {
				log.error(e.getMessage());
			    e.printStackTrace();
			 }finally{
				if(session.isOpen()){
				    session.close();
				}
			}
	  }
		return processMessagesPojos;
	}
   
   public static String getFmhqQuestionResp(Long pkCord){
	   return getFmhqQuestionResp(pkCord, null);
   }
   
   public static String getFmhqQuestionResp(Long fkCord, Session session){
	   boolean sessionFlag = false;
	   String response = null;
	   List respList = null;
		try {
			if (session == null) {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if(fkCord!=null && !fkCord.equals("")){
				String sql = "select response_value from cb_form_responses where fk_form=(select pk_form from cb_forms where forms_desc='FMHQ' and is_current_flag=1) and fk_form_version=(select max(pk_form_version) from cb_form_version where fk_form =(select pk_form from cb_forms where forms_desc='FMHQ' and is_current_flag=1) and entity_id='"+fkCord+"') and entity_id='"+fkCord+"' and fk_question=(select pk_questions from cb_questions where ques_code='severe_aut_imm_sys_disordr_rel')";
			    SQLQuery query = session.createSQLQuery(sql);
			    respList = query.list();
			    
			}
			if(respList!=null && respList.size()!=0){
				response = respList.get(0)+"";
			}
			
		} catch (Exception e) {
			//log.debug(" Exception in saveCordImportProcessMessage " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			
			 try {
				session.getTransaction().commit();
			}catch (Exception e) {
				log.error(e.getMessage());
			    e.printStackTrace();
			 }finally{
				if(session.isOpen()){
				    session.close();
				}
			}
	  }
		return response;
   }
   
   public static void insertCordEntryHlaEsbRef(Long fkCord,UserJB user){
	   insertCordEntryHlaEsbRef(fkCord, user, null);
   }
   
   public static void insertCordEntryHlaEsbRef(Long fkCord,UserJB user, Session session){
	   boolean sessionFlag = false;
		try {
			if (session == null) {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if(fkCord!=null){
				String sql = "INSERT INTO CB_CORD_REF_HLA_ESB_DATA(PK_CORD_REF,ENTERED_DATE,FK_CORD,ENTERED_BY,STATUS) VALUES(SEQ_CB_CORD_REF_HLA_ESB_DATA.NEXTVAL,SYSDATE,"+fkCord+","+user.getUserId()+",0)";
			    SQLQuery query = session.createSQLQuery(sql);
			    int i = query.executeUpdate();
			}else{
				
			}
			
		} catch (Exception e) {
			//log.debug(" Exception in saveCordImportProcessMessage " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			
			 try {
				session.getTransaction().commit();
			}catch (Exception e) {
				log.error(e.getMessage());
			    e.printStackTrace();
			 }finally{
				if(session.isOpen()){
				    session.close();
				}
			}
	  }
   }
}
