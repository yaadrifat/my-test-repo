package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

public class PatLabsPojo extends AuditablePojo{
	
	private Long pkpatlabs;
	private Long fkper;
	private Long fktestid;
	private Date testdate;
	private String testDateStr;
	private String testresult;
	private Long fksite;
	private Long fktestgroup;
	private Long fkspecimen;
	private Long fktestoutcome;
	private Long cmsapprovedlab;
	private Long fdalicensedlab;
	private String fktestreason;
	private Long fktestmethod;
	private Long fttestsource;
	private Long fktestspecimen;
	private Long fkTimingOfTest;
	private String testDescription;
	private Long fkidmmaster;
	private String testcomments;
	private Long viabilityMthd;
	private Long testcount;
	private String otherListValue;
	
	
	private String notes;
	private String reasonTestDesc;
	private String testMthdDesc;
    private Long[] fkTestReasonArr;
	

	
	public Long getPkpatlabs() {
		return pkpatlabs;
	}
	public void setPkpatlabs(Long pkpatlabs) {
		this.pkpatlabs = pkpatlabs;
	}
	public Long getFkper() {
		return fkper;
	}
	public void setFkper(Long fkper) {
		this.fkper = fkper;
	}
	public Long getFktestid() {
		return fktestid;
	}
	public void setFktestid(Long fktestid) {
		this.fktestid = fktestid;
	}
	public Date getTestdate() {
		return testdate;
	}
	public void setTestdate(Date testdate) {
		this.testdate = testdate;
	}
	public String getTestDateStr() {
		return testDateStr;
	}
	public void setTestDateStr(String testDateStr) {
		this.testDateStr = testDateStr;
	}
	public String getTestresult() {
		return testresult;
	}
	public void setTestresult(String testresult) {
		this.testresult = testresult;
	}
	public Long getFksite() {
		return fksite;
	}
	public void setFksite(Long fksite) {
		this.fksite = fksite;
	}
	public Long getFktestgroup() {
		return fktestgroup;
	}
	public void setFktestgroup(Long fktestgroup) {
		this.fktestgroup = fktestgroup;
	}
	
	public Long getFkspecimen() {
		return fkspecimen;
	}
	public void setFkspecimen(Long fkspecimen) {
		this.fkspecimen = fkspecimen;
	}
	
	public Long getFktestoutcome() {
		return fktestoutcome;
	}
	public void setFktestoutcome(Long fktestoutcome) {
		this.fktestoutcome = fktestoutcome;
	}
	public Long getCmsapprovedlab() {
		return cmsapprovedlab;
	}
	public void setCmsapprovedlab(Long cmsapprovedlab) {
		this.cmsapprovedlab = cmsapprovedlab;
	}
	public Long getFdalicensedlab() {
		return fdalicensedlab;
	}
	public void setFdalicensedlab(Long fdalicensedlab) {
		this.fdalicensedlab = fdalicensedlab;
	}
	public String getFktestreason() {
		return fktestreason;
	}
	public void setFktestreason(String fktestreason) {
		this.fktestreason = fktestreason;
	}
	public Long getFktestmethod() {
		return fktestmethod;
	}
	public void setFktestmethod(Long fktestmethod) {
		this.fktestmethod = fktestmethod;
	}
	public Long getFttestsource() {
		return fttestsource;
	}
	public void setFttestsource(Long fttestsource) {
		this.fttestsource = fttestsource;
	}
	public Long getFkidmmaster() {
		return fkidmmaster;
	}
	public void setFkidmmaster(Long fkidmmaster) {
		this.fkidmmaster = fkidmmaster;
	}
	public Long getFktestspecimen() {
		return fktestspecimen;
	}
	public void setFktestspecimen(Long fktestspecimen) {
		this.fktestspecimen = fktestspecimen;
	}
	public Long getFkTimingOfTest() {
		return fkTimingOfTest;
	}
	public void setFkTimingOfTest(Long fkTimingOfTest) {
		this.fkTimingOfTest = fkTimingOfTest;
	}
	public String getTestDescription() {
		return testDescription;
	}
	public void setTestDescription(String testDescription) {
		this.testDescription = testDescription;
	}
	public String getTestcomments() {
		return testcomments;
	}
	public void setTestcomments(String testcomments) {
		this.testcomments = testcomments;
	}
	public Long getViabilityMthd() {
		return viabilityMthd;
	}
	public void setViabilityMthd(Long viabilityMthd) {
		this.viabilityMthd = viabilityMthd;
	}
	public Long getTestcount() {
		return testcount;
	}
	public void setTestcount(Long testcount) {
		this.testcount = testcount;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getReasonTestDesc() {
		return reasonTestDesc;
	}
	public void setReasonTestDesc(String reasonTestDesc) {
		this.reasonTestDesc = reasonTestDesc;
	}
	public String getTestMthdDesc() {
		return testMthdDesc;
	}
	public void setTestMthdDesc(String testMthdDesc) {
		this.testMthdDesc = testMthdDesc;
	}
	public String getOtherListValue() {
		return otherListValue;
	}
	public void setOtherListValue(String otherListValue) {
		this.otherListValue = otherListValue;
	}
	public Long[] getFkTestReasonArr() {
		return fkTestReasonArr;
	}
	public void setFkTestReasonArr(Long[] fkTestReasonArr) {
		this.fkTestReasonArr = fkTestReasonArr;
	}
	
	
}
