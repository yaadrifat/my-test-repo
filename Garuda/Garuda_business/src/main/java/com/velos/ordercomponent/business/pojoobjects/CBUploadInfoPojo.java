package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

public class CBUploadInfoPojo extends AuditablePojo{
	private Long pk_UploadInfo;
	private String description;
	private Date completionDate; 
	private String strcompletionDate;
	private Date testDate; 
	private String strtestDate;
	private Date processDate; 
	private String strprocessDate;
	private String veficationTyping;
	private Date receivedDate; 
	private String strreceivedDate;
	private Long fk_CategoryId;
	private Long fk_SubCategoryId;
	private Long fk_AttachmentId;
	private Boolean deletedFlag;
	private Date reportDate;
	private String strreportDate;
	
	
	public Long getPk_UploadInfo() {
		return pk_UploadInfo;
	}
	public void setPk_UploadInfo(Long pk_UploadInfo) {
		this.pk_UploadInfo = pk_UploadInfo;
	}
	public String getDescription() {
		return description;
	}
	
	public String getStrtestDate() {
		return strtestDate;
	}
	public void setStrtestDate(String strtestDate) {
		this.strtestDate = strtestDate;
	}
	public String getStrprocessDate() {
		return strprocessDate;
	}
	public void setStrprocessDate(String strprocessDate) {
		this.strprocessDate = strprocessDate;
	}
	public String getStrreceivedDate() {
		return strreceivedDate;
	}
	public void setStrreceivedDate(String strreceivedDate) {
		this.strreceivedDate = strreceivedDate;
	}
	public String getStrcompletionDate() {
		return strcompletionDate;
	}
	public void setStrcompletionDate(String strcompletionDate) {
		this.strcompletionDate = strcompletionDate;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCompletionDate() {
		return completionDate;
	}
	public void setCompletionDate(Date completionDate) {
		this.completionDate = completionDate;
	}
	public Date getTestDate() {
		return testDate;
	}
	public void setTestDate(Date testDate) {
		this.testDate = testDate;
	}
	public Date getProcessDate() {
		return processDate;
	}
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}
	public String getVeficationTyping() {
		return veficationTyping;
	}
	public void setVeficationTyping(String veficationTyping) {
		this.veficationTyping = veficationTyping;
	}
	public Date getReceivedDate() {
		return receivedDate;
	}
	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}
	public Long getFk_CategoryId() {
		return fk_CategoryId;
	}
	public void setFk_CategoryId(Long fk_CategoryId) {
		this.fk_CategoryId = fk_CategoryId;
	}
	public Long getFk_SubCategoryId() {
		return fk_SubCategoryId;
	}
	public void setFk_SubCategoryId(Long fk_SubCategoryId) {
		this.fk_SubCategoryId = fk_SubCategoryId;
	}
	public Long getFk_AttachmentId() {
		return fk_AttachmentId;
	}
	public void setFk_AttachmentId(Long fk_AttachmentId) {
		this.fk_AttachmentId = fk_AttachmentId;
	}
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	public String getStrreportDate() {
		return strreportDate;
	}
	
	public void setStrreportDate(String strreportDate) {
		this.strreportDate = strreportDate;
	}

	public Date getReportDate() {
		return reportDate;
	}
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}
	
	
		
}
