package com.velos.ordercomponent.business.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author Mohiuddin Ali Ahmed
 *
 */
@Entity
@Table(name="UI_USR_WID_INSTANCE")
@SequenceGenerator(sequenceName="SEQ_UI_USR_WID_INSTANCE",name="SEQ_UI_USR_WID_INSTANCE",allocationSize=1)
public class UserWidgetInstance {

	private Long userWidgetInstanceId;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_UI_USR_WID_INSTANCE")
	@Column(name="PK_USER_WID_INST")
	public Long getUserWidgetInstanceId() {
		return userWidgetInstanceId;
	}

	public void setUserWidgetInstanceId(Long userWidgetInstanceId) {
		this.userWidgetInstanceId = userWidgetInstanceId;
	}

	private Long userid;
	private Long contPageWidgetMapId;

	@Column(name="FK_USER_ID")
	public Long getUserid() {
		return userid;
	}

	public void setUserid(Long userid) {
		this.userid = userid;
	}

	@Column(name="FK_PG_CONT_WID_ID")
	public Long getContPageWidgetMapId() {
		return contPageWidgetMapId;
	}

	public void setContPageWidgetMapId(Long contPageWidgetMapId) {
		this.contPageWidgetMapId = contPageWidgetMapId;
	}
	
	
}
