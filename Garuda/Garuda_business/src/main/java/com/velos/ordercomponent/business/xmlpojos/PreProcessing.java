package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.datatype.XMLGregorianCalendar;

public class PreProcessing {

	private XMLGregorianCalendar PreProcessingStartDate;
	private String cbu_vol_pre_prcsng;
	private String nucl_cell_cnctrn_strt_prcsng;
	private String nucl_cell_cnt_strt_prcsng;
	
	@XmlElement(name="PreProcessingStartDate")
	public XMLGregorianCalendar getPreProcessingStartDate() {
		return PreProcessingStartDate;
	}
	public void setPreProcessingStartDate(XMLGregorianCalendar preProcessingStartDate) {
		PreProcessingStartDate = preProcessingStartDate;
	}
	
	@XmlElement(name="cbu_vol_pre_prcsng")
	public String getCbu_vol_pre_prcsng() {
		return cbu_vol_pre_prcsng;
	}
	public void setCbu_vol_pre_prcsng(String cbuVolPrePrcsng) {
		cbu_vol_pre_prcsng = cbuVolPrePrcsng;
	}
	
	@XmlElement(name="nucl_cell_cnctrn_strt_prcsng")
	public String getNucl_cell_cnctrn_strt_prcsng() {
		return nucl_cell_cnctrn_strt_prcsng;
	}
	public void setNucl_cell_cnctrn_strt_prcsng(String nuclCellCnctrnStrtPrcsng) {
		nucl_cell_cnctrn_strt_prcsng = nuclCellCnctrnStrtPrcsng;
	}
	
	@XmlElement(name="nucl_cell_cnt_strt_prcsng")
	public String getNucl_cell_cnt_strt_prcsng() {
		return nucl_cell_cnt_strt_prcsng;
	}
	public void setNucl_cell_cnt_strt_prcsng(String nuclCellCntStrtPrcsng) {
		nucl_cell_cnt_strt_prcsng = nuclCellCntStrtPrcsng;
	}
	
	
}
