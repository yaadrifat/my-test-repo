package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang.StringUtils;

public class XMLGeorgarianCalanderAdapter extends XmlAdapter<String, XMLGregorianCalendar> {

	@Override
	public String marshal(XMLGregorianCalendar v) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("marshal");
		return null;
	}

	@Override
	public XMLGregorianCalendar unmarshal(String v) throws Exception {
		System.out.println("unmarshal");
		if(StringUtils.isEmpty(v))
			return null;
		else {
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(v);
		}	
	}

	

}
