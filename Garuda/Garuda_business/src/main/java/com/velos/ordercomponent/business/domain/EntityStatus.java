package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CB_ENTITY_STATUS")
@SequenceGenerator(sequenceName="SEQ_CB_ENTITY_STATUS",name="SEQ_CB_ENTITY_STATUS",allocationSize=1)
public class EntityStatus extends Auditable {

	private Long pkEntityStatus;
	private Long entityId;
	private Long entityType;
	private String statusTypeCode;
	private Long statusValue;
	private Date statusDate;
	private Long reasonId;
	private String statusRemarks;
	private String licEligChangeReason;
	private Long addOrUpdate;
	

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_ENTITY_STATUS")
	@Column(name="PK_ENTITY_STATUS")
	public Long getPkEntityStatus() {
		return pkEntityStatus;
	}

	public void setPkEntityStatus(Long pkEntityStatus) {
		this.pkEntityStatus = pkEntityStatus;
	}
	
	@Column(name="ENTITY_ID")
	public Long getEntityId() {
		return entityId;
	}
	
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	
	@Column(name="FK_ENTITY_TYPE")
	public Long getEntityType() {
		return entityType;
	}
	public void setEntityType(Long entityType) {
		this.entityType = entityType;
	}
	
	@Column(name="STATUS_TYPE_CODE")
	public String getStatusTypeCode() {
		return statusTypeCode;
	}
	public void setStatusTypeCode(String statusTypeCode) {
		this.statusTypeCode = statusTypeCode;
	}
	
	@Column(name="FK_STATUS_VALUE")
	public Long getStatusValue() {
		return statusValue;
	}
	public void setStatusValue(Long statusValue) {
		this.statusValue = statusValue;
	}
	
	@Column(name="STATUS_DATE")
	public Date getStatusDate() {
		return statusDate;
	}
	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}
	
	/*@Column(name="")
	public Long getReasonId() {
		return reasonId;
	}
	public void setReasonId(Long reasonId) {
		this.reasonId = reasonId;
	}*/
	
	@Column(name="STATUS_REMARKS")
	public String getStatusRemarks() {
		return statusRemarks;
	}
	public void setStatusRemarks(String statusRemarks) {
		this.statusRemarks = statusRemarks;
	}
	@Column(name="LICN_ELIG_CHANGE_REASON")
	public String getLicEligChangeReason() {
		return licEligChangeReason;
	}

	public void setLicEligChangeReason(String licEligChangeReason) {
		this.licEligChangeReason = licEligChangeReason;
	}	
	
	
	@Column(name="ADD_OR_UPDATE")
	public Long getAddOrUpdate() {
		return addOrUpdate;
	}

	public void setAddOrUpdate(Long addOrUpdate) {
		this.addOrUpdate = addOrUpdate;
	}
	
	
}
