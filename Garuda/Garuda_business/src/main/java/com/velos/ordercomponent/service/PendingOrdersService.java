package com.velos.ordercomponent.service;

import java.util.List;

//import com.aithent.gems.objects.WorkFlowProcessObject;
import com.aithent.gems.objects.WorkFlowProcessObject;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.FilterDataPojo;
import com.velos.ordercomponent.business.pojoobjects.HLAPojo;
import com.velos.ordercomponent.business.pojoobjects.OrderPojo;
import com.velos.ordercomponent.business.pojoobjects.PackingSlipPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.pojoobjects.ShipmentPojo;

public interface PendingOrdersService {
	
	
	public List getOrderDetails(String orderId);
	
	public List getCtOrderDetails(String orderId);
	
	public CdrCbuPojo submitCordStatus(CdrCbuPojo cdrCbuPojo);
	
	public List getResultsForCancelFilter(String orderType,String colName,String colData,String pkOrderType,String cancelled,String order_status);
	
	public List getAntigensForReceipant(String receipantId,String entityType);
	
	public Long getdefaultshipper(Long orderId,String orderType);
	
	public ShipmentPojo saveShipmentInfo(ShipmentPojo shipmentPojo);
	
	public OrderPojo updateOrderDetails(OrderPojo orderPojo);
	
	public ShipmentPojo getshipmentDetails(ShipmentPojo shipmentPojo);
	
	public HLAPojo saveCurrentHlaTyping(HLAPojo hlaPojo);
	
	public List getWorkFlowNotification(PaginateSearch paginateSearch,int i,Long userId,String siteId,String condition,String OrdByCondition);
	
	public List getAlertNotifications(Long userId,String siteId,String criteria,String orderBy,PaginateSearch paginateSearch,int i);
	
	public void createAlertInstances(Long orderId);
	
	public Long getOrderType(Long orderId);
	
	public List getCbbProfile(String siteId,Long userId);
	
	public List getOrders(PaginateSearch paginateSearch,int i,Long userId,String siteId,String condition,String orderByCondition);
	
	public List getTasksListForOrder(Long orderId);
	
	public List getOrderResol(Long orderId);
	
	public List getcordAvailConfirm(Long orderId);
	
	public List getLatestHlaInfo(Long orderId,Long entityId,String entityType);
	
	public List getShipmentInfo(Long orderId,Long shipmentType);
	
	public List fetchNextTask(WorkFlowProcessObject objWorkFlow);
	
	public List getallOrderDetails(PaginateSearch paginateSearch, int i,String filterflag,String ordertypeval,String fksiteId,Long userId,String codition,String orderBy);
	
	public int updateAlertStatus(Long alertInstanceId);
	
	public List savedInprogress(PaginateSearch paginationSearch,String SIPCondition,Long userId,String fksiteId,String orderBy,int i);
	
	public List notAvailCbu(PaginateSearch paginationSearch,String nacbuCondition,int i,String fksiteId,String nacbuOrderBy,Long userId);
	
	public String getClinicChkStatus(Long cordId);
	
	public Long getCRI_CompletedFlag(Long pk_cord);
	
	public void autoCompleteCRI(Long pkcord);
	
	public List getUserslistData(String siteId);
	
	public List getfinalReviewStatus(Long cordId,Long orderId);
	
	public List<FilterDataPojo> getCustomFilterData(Long userId,Long module,Long submodule);
	
	public List getQBuilderList(String sql, String orderBy);
	
	public List getpackingSlipDetails(Long pkPackingSlip);
	
	public List<FilterDataPojo> customFilterInsert(FilterDataPojo filterDataPojo);
	
	public String getPkCbuStatus(String orderType,String cbuStatusCode,String statusType);
	
	public List getDataFromCodelstCustCol(String tableName,String columnName,String criteria);
	
	public void updateDataFromCodelstCustCol(String tableName,String columnName,Long value,String criteria,String operation);
	
	public PackingSlipPojo updatePackingSlipDet(PackingSlipPojo packingSlipPojo);
	
	public void updateOrderCompleteReqInfoflag(Long cordId);
	
	public String updateAssignTo(String ordId,String assignto,String flagval,Long loggedIn_userId,String cordIdval);
	
	public List getPatientInfo(Long pkCordId);
	
	public Long getPkActivity(String activityname);
	
	public List getCommonUsrLst(String site,String count);
	
	public List getLabelAssignLst(String siteId);
	
	public String getMinimumCriteriaStat(Long orderId);
	
	public String getMinimumCriteriaFlagByCordId(Long cordId);
	
	public String getNMDPResearchSample(Long siteId,Long cordId);
	
	public Long getReceipientId(Long cordId);
	
	public String getAcknowledgeCancelledOrderFlag(Long orderId,Long cordId);
	
	public OrderPojo getRequestDetails(Long orderId);
	
	public Long getOrderIdFromCordId(Long cordId);
	
	public Long getDefaultPaperLoc(Long orderId);
	
	public void updateOrderLastReviewedBy(Long orderId,Long user_id);

	public String getFlagForTaskCheck(Long cordId);
}
