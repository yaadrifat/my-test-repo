package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.datatype.XMLGregorianCalendar;

public class PostProcessingPreCryoPreservation {

	private XMLGregorianCalendar PreCryoStartDate;
	private String cbu_vol_frzn;
	private String nucl_cell_cnctrn_frzn;
	private String nucl_cell_cnt_frzn_uncorrect;	
	private String post_prcsng_nucl_rbc_cnt;
	private String pct_post_prcsng_nucl_rbc_cnt;
	private String cd34_cell_cnt_frzn;	
	private String prcnt_cd34_viable;
	private String prcnt_cd34_frzn_mono_cell_cn;
	private String prcnt_mono_frzn_nucl_cell_cn;	
	private String cd3_cell_cnt_frzn;
	private String prcnt_cd3_frzn_mono_cell_cn;
	private CFU_PostPrc_PreCryopPreservation cfuPostPrcPreCryopPreservation;
	private ViabilityPostPrc_PreCryopPreservation viabilityPostPrcPreCryopPreservation;
	
	@XmlElement(name="PreCryoStartDate")	
	public XMLGregorianCalendar getPreCryoStartDate() {
		return PreCryoStartDate;
	}
	public void setPreCryoStartDate(XMLGregorianCalendar preCryoStartDate) {
		PreCryoStartDate = preCryoStartDate;
	}
	@XmlElement(name="cbu_vol_frzn")	
	public String getCbu_vol_frzn() {
		return cbu_vol_frzn;
	}
	public void setCbu_vol_frzn(String cbuVolFrzn) {
		cbu_vol_frzn = cbuVolFrzn;
	}
	@XmlElement(name="nucl_cell_cnctrn_frzn")	
	public String getNucl_cell_cnctrn_frzn() {
		return nucl_cell_cnctrn_frzn;
	}
	public void setNucl_cell_cnctrn_frzn(String nuclCellCnctrnFrzn) {
		nucl_cell_cnctrn_frzn = nuclCellCnctrnFrzn;
	}
	@XmlElement(name="nucl_cell_cnt_frzn_uncorrect")	
	public String getNucl_cell_cnt_frzn_uncorrect() {
		return nucl_cell_cnt_frzn_uncorrect;
	}
	public void setNucl_cell_cnt_frzn_uncorrect(String nuclCellCntFrznUncorrect) {
		nucl_cell_cnt_frzn_uncorrect = nuclCellCntFrznUncorrect;
	}
	@XmlElement(name="post_prcsng_nucl_rbc_cnt")	
	public String getPost_prcsng_nucl_rbc_cnt() {
		return post_prcsng_nucl_rbc_cnt;
	}
	public void setPost_prcsng_nucl_rbc_cnt(String postPrcsngNuclRbcCnt) {
		post_prcsng_nucl_rbc_cnt = postPrcsngNuclRbcCnt;
	}
	@XmlElement(name="pct_post_prcsng_nucl_rbc_cnt")	
	public String getPct_post_prcsng_nucl_rbc_cnt() {
		return pct_post_prcsng_nucl_rbc_cnt;
	}
	public void setPct_post_prcsng_nucl_rbc_cnt(String pctPostPrcsngNuclRbcCnt) {
		pct_post_prcsng_nucl_rbc_cnt = pctPostPrcsngNuclRbcCnt;
	}
	@XmlElement(name="cd34_cell_cnt_frzn")	
	public String getCd34_cell_cnt_frzn() {
		return cd34_cell_cnt_frzn;
	}
	public void setCd34_cell_cnt_frzn(String cd34CellCntFrzn) {
		cd34_cell_cnt_frzn = cd34CellCntFrzn;
	}
	@XmlElement(name="prcnt_cd34_viable")	
	public String getPrcnt_cd34_viable() {
		return prcnt_cd34_viable;
	}
	public void setPrcnt_cd34_viable(String prcntCd34Viable) {
		prcnt_cd34_viable = prcntCd34Viable;
	}
	@XmlElement(name="prcnt_cd34_frzn_mono_cell_cn")	
	public String getPrcnt_cd34_frzn_mono_cell_cn() {
		return prcnt_cd34_frzn_mono_cell_cn;
	}
	public void setPrcnt_cd34_frzn_mono_cell_cn(String prcntCd34FrznMonoCellCn) {
		prcnt_cd34_frzn_mono_cell_cn = prcntCd34FrznMonoCellCn;
	}
	@XmlElement(name="prcnt_mono_frzn_nucl_cell_cn")	
	public String getPrcnt_mono_frzn_nucl_cell_cn() {
		return prcnt_mono_frzn_nucl_cell_cn;
	}
	public void setPrcnt_mono_frzn_nucl_cell_cn(String prcntMonoFrznNuclCellCn) {
		prcnt_mono_frzn_nucl_cell_cn = prcntMonoFrznNuclCellCn;
	}
	@XmlElement(name="cd3_cell_cnt_frzn")	
	public String getCd3_cell_cnt_frzn() {
		return cd3_cell_cnt_frzn;
	}
	public void setCd3_cell_cnt_frzn(String cd3CellCntFrzn) {
		cd3_cell_cnt_frzn = cd3CellCntFrzn;
	}
	@XmlElement(name="prcnt_cd3_frzn_mono_cell_cn")	
	public String getPrcnt_cd3_frzn_mono_cell_cn() {
		return prcnt_cd3_frzn_mono_cell_cn;
	}
	public void setPrcnt_cd3_frzn_mono_cell_cn(String prcntCd3FrznMonoCellCn) {
		prcnt_cd3_frzn_mono_cell_cn = prcntCd3FrznMonoCellCn;
	}
	@XmlElement(name="CFU_PostPrc_PreCryopPreservation")	
	public CFU_PostPrc_PreCryopPreservation getCfuPostPrcPreCryopPreservation() {
		return cfuPostPrcPreCryopPreservation;
	}
	public void setCfuPostPrcPreCryopPreservation(
			CFU_PostPrc_PreCryopPreservation cfuPostPrcPreCryopPreservation) {
		this.cfuPostPrcPreCryopPreservation = cfuPostPrcPreCryopPreservation;
	}
	@XmlElement(name="ViabilityPostPrc_PreCryopPreservation")	
	public ViabilityPostPrc_PreCryopPreservation getViabilityPostPrcPreCryopPreservation() {
		return viabilityPostPrcPreCryopPreservation;
	}
	public void setViabilityPostPrcPreCryopPreservation(
			ViabilityPostPrc_PreCryopPreservation viabilityPostPrcPreCryopPreservation) {
		this.viabilityPostPrcPreCryopPreservation = viabilityPostPrcPreCryopPreservation;
	}
	
	
}
