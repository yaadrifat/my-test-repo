package com.velos.ordercomponent.report;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.jdbc.Work;


public class EntityMap implements Work{
	public static final Log log = LogFactory.getLog(EntityMap.class);
	
	private String query;
	
	private Map<Long,Object[]> entityMap;	
	
	public EntityMap(String query) {
		this.query = query;
	}
	 @Override
	    public void execute( Connection connection ) throws SQLException
	    {
	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        try
	        {
	            ps = connection.prepareStatement( query);
	            ps.setFetchSize(50000);
	            rs = ps.executeQuery();
	            long startTime=System.currentTimeMillis();            
		           
	             entityMap = prepMapObj(rs);		            
	             long endTime=System.currentTimeMillis();
		         System.out.println();
		         System.out.println((endTime-startTime)/1000+" : "+(endTime-startTime)%1000+" MM:SS\n\n");
	        }
	        
	        
	        catch( SQLException e ) { 
	        	
	        	printException(e);
	        } catch (ParseException e) {
	        	
	        	printException(e);
			}
	        finally  {
	            if( rs != null )  
	            {
	                try {
	                    rs.close();
	                }
	                catch( Exception e ) {printException(e);}
	            }
	            if( ps != null ) 
	            {
	                try {
	                    ps.close();
	                }
	                catch( Exception e ) {printException(e);}
	            }
	        }
	    }
	
	 private  Map<Long,Object[]> prepMapObj(ResultSet rs) throws SQLException, ParseException{
		 
		 Map<Long,Object[]> entityMap = new HashMap<Long,Object[]>();
		 ResultSetMetaData metaData = rs.getMetaData();
		 final int columnCount = metaData.getColumnCount();
		 Long key = null;
		   
		 Object[] obj = null;
		 
		  while( rs.next())  
          {
			 
			  obj = new  Object[columnCount];
			  key = rs.getLong(1);
			  
			  for (int i=1; i <= columnCount; ++i){	
				  
		            obj[i-1] = rs.getObject(i);
		            
		        }	
			  
			  entityMap.put(key, obj);
          }
		 return entityMap;
	 }
	private void printException(Exception e){
		
		log.error(e.getMessage());
		e.printStackTrace();
		
	}
	
	
	public Map<Long, Object[]> getEntityMap() {
		return entityMap;
	}
	
	

}

