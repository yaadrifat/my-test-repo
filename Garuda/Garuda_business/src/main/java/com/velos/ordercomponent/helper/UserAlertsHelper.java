package com.velos.ordercomponent.helper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;

import com.velos.ordercomponent.business.domain.Address;
import com.velos.ordercomponent.business.domain.UserAlerts;
import com.velos.ordercomponent.business.domain.UserDomain;
import com.velos.ordercomponent.business.pojoobjects.AddressPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.pojoobjects.UserAlertsPojo;
import com.velos.ordercomponent.business.pojoobjects.UserPojo;
import com.velos.ordercomponent.business.util.HibernateUtil;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.VelosUtil;
import org.hibernate.SQLQuery;
public class UserAlertsHelper {
	public static final Log log=LogFactory.getLog(UserAlertsHelper.class);
	public static List<UserAlertsPojo> insertUserAlerts(List<UserAlertsPojo> userAlertsLst){

		return insertUserAlerts(userAlertsLst, null);
	}
	
	public static List<UserAlertsPojo> insertUserAlerts(List<UserAlertsPojo> userAlertsLst, Session session) {
		UserAlerts userAlerts2=new UserAlerts();
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			
			String query="from UserAlerts where fkUserId = "+userAlertsLst.get(0).getFkUserId();
			//log.debug("query is-->:"+query);
			List<UserAlerts> lis = session.createQuery(query).list();
			if(lis != null && lis.size()>0){
				Iterator<UserAlerts> it=lis.iterator();
				while(it.hasNext()){
					userAlerts2=(UserAlerts)it.next();
					session.delete(userAlerts2);
				}			
			}
			for(UserAlertsPojo userAlertsPojo : userAlertsLst){
				UserAlerts userAlerts=new UserAlerts();
				  if(userAlertsPojo.getFkCodelstId()!=null){
					  if(!userAlertsPojo.getEmailFlag()){
						  continue;
					  }else{
						  userAlerts=(UserAlerts)ObjectTransfer.transferObjects(userAlertsPojo, userAlerts);
						  userAlerts=(UserAlerts)  new VelosUtil().setAuditableInfo(userAlerts);
						  session.save(userAlerts);
					  }
				  }
			   }
			
			
			//close session, if session created in this method
			session.getTransaction().commit();

		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	
		return userAlertsLst;
	}
	
	public static List getUserAlerts(Long fkUserId,String alertType){

		return getUserAlerts(fkUserId, alertType,null);
	}
	
	public static List getUserAlerts(Long fkUserId,String alertType, Session session) {
		List fkcodeIds = new ArrayList();
		try {
			
			// check session
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			
			//String query="from UserAlerts where fkUserId = "+fkUserId;
			String query="select clst.alert_title,clst.alert_desc,EMAIL_FLAG,NOTIFY_FLAG,PROMPT_FLAG,clst.pk_codelst,ual.pk_alert,CT_LAB_FLAG,CT_SHIP_FLAG,HE_FLAG,OR_FLAG from (select pk_codelst,CODELST_DESC alert_title,CODELST_CUSTOM_COL1 alert_desc " +
					"from er_codelst where codelst_type='"+alertType+"' and CODELST_HIDE='N' order by pk_codelst) clst left outer join (select pk_alert,fk_codelst_alert,email_flag,notify_flag,prompt_flag,CT_LAB_FLAG,CT_SHIP_FLAG,HE_FLAG,OR_FLAG from cb_user_alerts " +
					"where fk_user="+fkUserId+") ual on(ual.fk_codelst_alert=clst.pk_codelst)";
			//log.debug("query is::::::"+query);
			fkcodeIds= session.createSQLQuery(query).list();
			//log.debug("size===>"+fkcodeIds.size());

		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	
		return fkcodeIds;
	}
	
	public static UserPojo getUserByid(UserPojo userPojo){
		return getUserByid(userPojo, null);
	}
	
	public static UserPojo getUserByid(UserPojo userPojo, Session session){
		boolean sessionFlag = false;
		UserDomain userDomain = new UserDomain();
		userDomain = (UserDomain) ObjectTransfer.transferObjects(userPojo, userDomain);
		try {
			// check session
			//log.debug("session " + session);
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			// //log.debug("Helper : " +
			// cbuUnitReportTemp.getPkCordExtInfo());
			// //log.debug("=========="+cdrCbu.getCordID());
			if (userDomain.getUserId() != null && userDomain.getUserId() > 0) {

				// //log.debug("inside update");
				userDomain = (UserDomain) session.load(UserDomain.class, userDomain.getUserId());
				userPojo = (UserPojo) ObjectTransfer.transferObjects(userDomain, userPojo);
			}

			/*
			 * if (sessionFlag) { session.getTransaction().commit(); }
			 */
		} catch (Exception e) {
			//log.debug(" Exception in CBU Unit Report " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			//log.debug(" session in finally " + session + " /"	+ sessionFlag);
			 if (session.isOpen()) {
			 session.close();
			 }
		}
		return userPojo;
	}
	public static String getLogNameByUserId(Long userId){
		return getLogNameByUserId(userId, null);
	}
	
	public static String getLogNameByUserId(Long userId,Session session){
		boolean sessionFlag = false;
		String logName = null;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;

				session.beginTransaction();
			}
			String sql = "select USR_LOGNAME from ER_USER where pk_user="+userId;
			SQLQuery query = session.createSQLQuery(sql);
			List list = query.list();
			if (list != null && list.size() > 0) {
				logName = (String) list.get(0);
			}
		} catch (Exception e) {
			//log.debug(" Exception in CBU Unit Report " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			//log.debug(" session in finally " + session + " /"+ sessionFlag);
			 if (session.isOpen()) {
			 session.close();
			 }
		}
		return logName;
	}
	
	public static AddressPojo updateUserMailId(AddressPojo addressPojo){
		Address address = new Address();

		try {
			address = (Address) ObjectTransfer
					.transferObjects(addressPojo, address);
			Object object = new VelosUtil().setAuditableInfo(address);
			address = updateUserMail((Address) object);
			addressPojo = (AddressPojo) ObjectTransfer.transferObjects(address,
					addressPojo);

		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return addressPojo;
	}
	
	public static Address updateUserMail(Address address) {
		//log.debug(" helper without session");
		return updateUserMail(address, null);
	}
	
	public static Address updateUserMail(Address address,Session session){
		boolean sessionFlag = false;
		try {
			// check session
			//log.debug("session " + session);
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
		
			if (address.getAddressId() != null && address.getAddressId() > 0) {

				//log.debug("inside update cordid");
				session.merge(address);
			}

			if (sessionFlag) {
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			//log.debug(" Exception in UpdateUsermail " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			//log.debug(" session in finally " + session + " /"+ sessionFlag);
			 if (session.isOpen()) {
			 session.close();
			 }
		}
		return address;
	}
	
	public static List getUsersByid(String criteria,PaginateSearch paginateSearch,int i){
		return getUsersByid(criteria,paginateSearch, null,i);
	}
	
	public static List getUsersByid(String criteria,PaginateSearch paginateSearch,Session session,int i){
		List<Object> returnList = null;
       	boolean sessionFlag = false;
    	try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "select userd.PK_USER,userd.usr_logname,userd.USR_LASTNAME,userd.USR_FIRSTNAME,uadd.add_email,userd.fk_account,userd.USR_TYPE from ER_USER userd,ER_ADD uadd where (userd.DELETED_FLAG=0 or userd.DELETED_FLAG is null) and userd.fk_peradd = uadd.pk_add "+criteria;
			Query query = session.createSQLQuery(sql);
			if(i==1){
			query.setFirstResult(paginateSearch.getiPageNo());
			query.setMaxResults(paginateSearch.getiShowRows());
			}
			returnList = (List<Object>) query.list();
    	} catch (Exception e) {
			//log.debug(" Exception in getCbuInfoById " + e);
    		
    		log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if(session.isOpen()){
				 session.close();		
			}
		}
		return returnList;
	}
}