package com.velos.ordercomponent.business.xmlpojos;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class GallbladderRemoved {

	private String gallbladder_removed_ind;
	/*private Relations relations;
	@XmlElement(name="Relations")
	public Relations getRelations() {
		return relations;
	}
	public void setRelations(Relations relations) {
		this.relations = relations;
	}*/
	@XmlElement(name="gallbladder_removed_ind")
	public String getGallbladder_removed_ind() {
		return gallbladder_removed_ind;
	}
	public void setGallbladder_removed_ind(String gallbladderRemovedInd) {
		gallbladder_removed_ind = gallbladderRemovedInd;
	}
	private List<Relations> Relations;
	@XmlElement(name="Relations")
	public List<Relations> getRelations() {
		return Relations;
	}
	public void setRelations(List<Relations> relations) {
		Relations = relations;
	}
}
