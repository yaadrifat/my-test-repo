package com.velos.ordercomponent.business.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.ordercomponent.business.domain.Auditable;

@Entity
@Table(name="CB_QUESTIONS")
@SequenceGenerator(sequenceName="SEQ_CB_QUESTIONS",name="SEQ_CB_QUESTIONS",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class FormQuestions extends Auditable {
	
	private Long pkQuestions; //PK_QUESTIONS
	private String questionDesc; //QUES_DESC
	private String questionHelp; //QUES_HELP
	private String unlicenReqFlag; //UNLICEN_REQ_FLAG
	private String licenReqFlag;  //LICEN_REQ_FLAG
	private String unlicenPriorShipFlag;  //UNLICN_PRIOR_TO_SHIPMENT_FLAG
	private String licenPriorShipFlag;  //LICEN_PRIOR_SHIPMENT_FLAG
	private String cbbNotUseSystem;  //CBB_NOT_USE_SYSTEM
	private String questionHover;  //QUES_HOVER
	private String addCommentFlag;  //ADD_COMMENT_FLAG
	private String assementFlag;  //ASSESMENT_FLAG
	//private Long fkMasterQues; //FK_MASTER_QUES
	//private Long fkDepenentQues; //FK_DEPENDENT_QUES
	//private String fkDepenentQuesVal; //FK_DEPENDENT_QUES_VALUE
	private Long responseType; //RESPONSE_TYPE
	private String quesCode; //QUES_CODE
	//private String quesSeq; //QUES_SEQ
	private String deletedFlag;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_QUESTIONS")
	@Column(name="PK_QUESTIONS")
	public Long getPkQuestions() {
		return pkQuestions;
	}
	public void setPkQuestions(Long pkQuestions) {
		this.pkQuestions = pkQuestions;
	}
	@Column(name="QUES_DESC")
	public String getQuestionDesc() {
		return questionDesc;
	}
	public void setQuestionDesc(String questionDesc) {
		this.questionDesc = questionDesc;
	}
	@Column(name="QUES_HELP")
	public String getQuestionHelp() {
		return questionHelp;
	}
	public void setQuestionHelp(String questionHelp) {
		this.questionHelp = questionHelp;
	}
	@Column(name="UNLICEN_REQ_FLAG")
	public String getUnlicenReqFlag() {
		return unlicenReqFlag;
	}
	public void setUnlicenReqFlag(String unlicenReqFlag) {
		this.unlicenReqFlag = unlicenReqFlag;
	}
	@Column(name="LICEN_REQ_FLAG")
	public String getLicenReqFlag() {
		return licenReqFlag;
	}
	public void setLicenReqFlag(String licenReqFlag) {
		this.licenReqFlag = licenReqFlag;
	}
	@Column(name="UNLICN_PRIOR_TO_SHIPMENT_FLAG")
	public String getUnlicenPriorShipFlag() {
		return unlicenPriorShipFlag;
	}
	public void setUnlicenPriorShipFlag(String unlicenPriorShipFlag) {
		this.unlicenPriorShipFlag = unlicenPriorShipFlag;
	}
	@Column(name="LICEN_PRIOR_SHIPMENT_FLAG")
	public String getLicenPriorShipFlag() {
		return licenPriorShipFlag;
	}
	public void setLicenPriorShipFlag(String licenPriorShipFlag) {
		this.licenPriorShipFlag = licenPriorShipFlag;
	}
	@Column(name="CBB_NOT_USE_SYSTEM")
	public String getCbbNotUseSystem() {
		return cbbNotUseSystem;
	}
	public void setCbbNotUseSystem(String cbbNotUseSystem) {
		this.cbbNotUseSystem = cbbNotUseSystem;
	}
	@Column(name="QUES_HOVER")
	public String getQuestionHover() {
		return questionHover;
	}
	public void setQuestionHover(String questionHover) {
		this.questionHover = questionHover;
	}
	@Column(name="ADD_COMMENT_FLAG")
	public String getAddCommentFlag() {
		return addCommentFlag;
	}
	public void setAddCommentFlag(String addCommentFlag) {
		this.addCommentFlag = addCommentFlag;
	}
	@Column(name="ASSESMENT_FLAG")
	public String getAssementFlag() {
		return assementFlag;
	}
	public void setAssementFlag(String assementFlag) {
		this.assementFlag = assementFlag;
	}
	@Column(name="RESPONSE_TYPE")
	public Long getResponseType() {
		return responseType;
	}
	public void setResponseType(Long responseType) {
		this.responseType = responseType;
	}
	@Column(name="QUES_CODE")
	public String getQuesCode() {
		return quesCode;
	}
	public void setQuesCode(String quesCode) {
		this.quesCode = quesCode;
	}
	@Column(name="DELETEDFLAG")
	public String getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(String deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
}
