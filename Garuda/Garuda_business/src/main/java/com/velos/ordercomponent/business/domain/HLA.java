package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name="CB_HLA")
@SequenceGenerator(sequenceName="SEQ_CB_HLA",name="SEQ_CB_HLA",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class HLA extends Auditable {
	private Long pkHla;
	private String fkCordCdrCbuId;
	private Long fkHlaCodeId;
	private Long fkHlaMethodId;
	private String hlaType1;
	private String hlaType2;
	
	private Long fkentityCordId;
	private Long fkEntityCodeType;
	private Long fkOrderId;
	private Long fkSpecType;
	private Long fkHlaOverrideClassCode;
	private Date hlaCritOverridedate;
	private Boolean hlaOverrideAntigen1;
	private Boolean hlaOverrideAntigen2;
	private Long fkHlaPrimeDna;
	private Long fkHlaProteinDipId;
	private Long fkHlaAntigen1;
	private Long fkHlaAntigen2;
	private Date hlaRecievedDate;
	private Date hlaTypingDate;
	private Long fkHlaReportingLab;
	private Long fkHlaReportingMethod;
	private Long fkHlaCordCtStatus;
	private Long fkSource;
	private Long hlaOrderSeq;
	private Long hlaTypingSeq;
	/*private Auditable auditable;
	*/
	
	
	/*
	 private CodeList hlaList;
	  
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="FK_HLA_CODE_ID", nullable = true ,insertable=false, updatable=false)
    @org.hibernate.annotations.NotFound(action=NotFoundAction.IGNORE)
	public CodeList getHlaList() {
		return hlaList;
	}
	public void setHlaList(CodeList hlaList) {
		this.hlaList = hlaList;
	}
	
	
	*/
	@Column(name="FK_ORDER_ID")
	public Long getFkOrderId() {
		return fkOrderId;
	}
	public void setFkOrderId(Long fkOrderId) {
		this.fkOrderId = fkOrderId;
	}
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_HLA")
	@Column(name="PK_HLA")
	public Long getPkHla() {
		return pkHla;
	}
	public void setPkHla(Long pkHla) {
		this.pkHla = pkHla;
	}
	
	@Column(name="FK_CORD_CDR_CBU_ID")
	public String getFkCordCdrCbuId() {
		return fkCordCdrCbuId;
	}
	public void setFkCordCdrCbuId(String fkCordCdrCbuId) {
		this.fkCordCdrCbuId = fkCordCdrCbuId;
	}
	
	@Column(name="FK_HLA_CODE_ID")
	public Long getFkHlaCodeId() {
		return fkHlaCodeId;
	}
	public void setFkHlaCodeId(Long fkHlaCodeId) {
		this.fkHlaCodeId = fkHlaCodeId;
	}
	
	@Column(name="FK_HLA_METHOD_ID")
	public Long getFkHlaMethodId() {
		return fkHlaMethodId;
	}
	public void setFkHlaMethodId(Long fkHlaMethodId) {
		this.fkHlaMethodId = fkHlaMethodId;
	}
	
	
	
	@Column(name="HLA_VALUE_TYPE1")
	public String getHlaType1() {
		return hlaType1;
	}
	public void setHlaType1(String hlaType1) {
		this.hlaType1 = hlaType1;
	}
	
	@Column(name="HLA_VALUE_TYPE2")
	public String getHlaType2() {
		return hlaType2;
	}
	public void setHlaType2(String hlaType2) {
		this.hlaType2 = hlaType2;
	}
		
	
	/*@Embedded
	public Auditable getAuditable() {
		return auditable;
	}
	public void setAuditable(Auditable auditable) {
		this.auditable = auditable;
	}
	*/
	private Boolean deletedFlag;
	
	@Column(name="deletedflag")
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	
	@Column(name="ENTITY_ID")
	public Long getFkentityCordId() {
		return fkentityCordId;
	}
	public void setFkentityCordId(Long fkentityCordId) {
		this.fkentityCordId = fkentityCordId;
	}
	
	@Column(name="ENTITY_TYPE")
	public Long getFkEntityCodeType() {
		return fkEntityCodeType;
	}
	public void setFkEntityCodeType(Long fkEntityCodeType) {
		this.fkEntityCodeType = fkEntityCodeType;
	}
	
	@Column(name="FK_SPEC_TYPE")
	public Long getFkSpecType() {
		return fkSpecType;
	}
	public void setFkSpecType(Long fkSpecType) {
		this.fkSpecType = fkSpecType;
	}
	@Column(name="FK_HLA_OVERCLS_CODE")
	public Long getFkHlaOverrideClassCode() {
		return fkHlaOverrideClassCode;
	}
	public void setFkHlaOverrideClassCode(Long fkHlaOverrideClassCode) {
		this.fkHlaOverrideClassCode = fkHlaOverrideClassCode;
	}
	@Column(name="HLA_CRIT_OVER_DATE")
	public Date getHlaCritOverridedate() {
		return hlaCritOverridedate;
	}
	public void setHlaCritOverridedate(Date hlaCritOverridedate) {
		this.hlaCritOverridedate = hlaCritOverridedate;
	}
	@Column(name="HLA_OVER_IND_ANTGN1")
	public Boolean getHlaOverrideAntigen1() {
		return hlaOverrideAntigen1;
	}
	public void setHlaOverrideAntigen1(Boolean hlaOverrideAntigen1) {
		this.hlaOverrideAntigen1 = hlaOverrideAntigen1;
	}
	@Column(name="HLA_OVER_IND_ANTGN2")
	public Boolean getHlaOverrideAntigen2() {
		return hlaOverrideAntigen2;
	}
	public void setHlaOverrideAntigen2(Boolean hlaOverrideAntigen2) {
		this.hlaOverrideAntigen2 = hlaOverrideAntigen2;
	}
	@Column(name="FK_HLA_PRIM_DNA_ID")
	public Long getFkHlaPrimeDna() {
		return fkHlaPrimeDna;
	}
	public void setFkHlaPrimeDna(Long fkHlaPrimeDna) {
		this.fkHlaPrimeDna = fkHlaPrimeDna;
	}
	@Column(name="FK_HLA_PROT_DIPLOID")
	public Long getFkHlaProteinDipId() {
		return fkHlaProteinDipId;
	}
	public void setFkHlaProteinDipId(Long fkHlaProteinDipId) {
		this.fkHlaProteinDipId = fkHlaProteinDipId;
	}
	@Column(name="FK_HLA_ANTIGENEID1")
	public Long getFkHlaAntigen1() {
		return fkHlaAntigen1;
	}
	public void setFkHlaAntigen1(Long fkHlaAntigen1) {
		this.fkHlaAntigen1 = fkHlaAntigen1;
	}
	@Column(name="FK_HLA_ANTIGENEID2")
	public Long getFkHlaAntigen2() {
		return fkHlaAntigen2;
	}
	public void setFkHlaAntigen2(Long fkHlaAntigen2) {
		this.fkHlaAntigen2 = fkHlaAntigen2;
	}
	@Column(name="HLA_RECEIVED_DATE",insertable=true,updatable=false)
	public Date getHlaRecievedDate() {
		return hlaRecievedDate;
	}
	public void setHlaRecievedDate(Date hlaRecievedDate) {
		this.hlaRecievedDate = hlaRecievedDate;
	}
	@Column(name="HLA_TYPING_DATE")
	public Date getHlaTypingDate() {
		return hlaTypingDate;
	}
	public void setHlaTypingDate(Date hlaTypingDate) {
		this.hlaTypingDate = hlaTypingDate;
	}
	@Column(name="FK_HLA_REPORTING_LAB")
	public Long getFkHlaReportingLab() {
		return fkHlaReportingLab;
	}
	public void setFkHlaReportingLab(Long fkHlaReportingLab) {
		this.fkHlaReportingLab = fkHlaReportingLab;
	}
	@Column(name="FK_HLA_REPORT_METHOD")
	public Long getFkHlaReportingMethod() {
		return fkHlaReportingMethod;
	}
	public void setFkHlaReportingMethod(Long fkHlaReportingMethod) {
		this.fkHlaReportingMethod = fkHlaReportingMethod;
	}
	@Column(name="FK_CORD_CT_STATUS")
	public Long getFkHlaCordCtStatus() {
		return fkHlaCordCtStatus;
	}
	public void setFkHlaCordCtStatus(Long fkHlaCordCtStatus) {
		this.fkHlaCordCtStatus = fkHlaCordCtStatus;
	}
	@Column(name="FK_SOURCE")
	public Long getFkSource() {
		return fkSource;
	}
	public void setFkSource(Long fkSource) {
		this.fkSource = fkSource;
	}
	@Column(name="CB_HLA_ORDER_SEQ")
	public Long getHlaOrderSeq() {
		return hlaOrderSeq;
	}
	public void setHlaOrderSeq(Long hlaOrderSeq) {
		this.hlaOrderSeq = hlaOrderSeq;
	}
	@Column(name="HLA_TYPING_SEQ")
	public Long getHlaTypingSeq() {
		return hlaTypingSeq;
	}
	public void setHlaTypingSeq(Long hlaTypingSeq) {
		this.hlaTypingSeq = hlaTypingSeq;
	}
	
	
}
