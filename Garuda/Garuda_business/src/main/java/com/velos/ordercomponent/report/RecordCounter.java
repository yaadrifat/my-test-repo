package com.velos.ordercomponent.report;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.jdbc.Work;



/**
 * @author Anurag Upadhyay
 * 
 *
 */

public class RecordCounter implements Work {
	
	public static final Log log = LogFactory.getLog(RecordCounter.class);
	private long totalRecord;
	private Map<String,Object> counterDetails;
	private String query;
	private Set<Long> uniqueCords;
    private Map<Long,Long> duplCords;
    private Long l;
	
	public RecordCounter(String query) {
		this.query = query;
		this.totalRecord = 0;
		this.uniqueCords = new LinkedHashSet<Long>();
		this.duplCords = new HashMap<Long,Long>();
		this.counterDetails = new HashMap<String,Object>();
	}
	
	 
	    @Override
	    public void execute( Connection connection ) throws SQLException
	    {
	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        try
	        {
	            ps = connection.prepareStatement( query);
	            ps.setFetchSize(500000);
	            rs = ps.executeQuery();
	            System.out.println("Query Result Returned");
	            long startTime=System.currentTimeMillis();
	            
		            while( rs.next())  
		            {
		            	   l = rs.getLong(1); 		           
		           	  
			           	   if(!uniqueCords.add(l)){
			           		   duplCords.put(l, l);		           	 
			               }
			           	   
			           	   totalRecord++;
		            }
	            
		            counterDetails.put("duplCords", duplCords);
		            counterDetails.put("uniqueCords", uniqueCords);
		            counterDetails.put("totalRecord", totalRecord);
		            
	             long endTime=System.currentTimeMillis();
		         System.out.println();
		         System.out.println((endTime-startTime)/1000+" : "+(endTime-startTime)%1000+" MM:SS\n\n");
	        }
	        
	        
	        catch( SQLException e ) { 
	        	
	        	printException(e);
	        }
	        finally  {
	            if( rs != null )  
	            {
	                try {
	                    rs.close();
	                }
	                catch( Exception e ) {printException(e);}
	            }
	            if( ps != null ) 
	            {
	                try {
	                    ps.close();
	                }
	                catch( Exception e ) {printException(e);}
	            }
	        }
	    }
	
	private void printException(Exception e){
		
		log.error(e.getMessage());
		e.printStackTrace();
		
	}
	public Map<String, Object> getCounterDetails() {
		return counterDetails;
	}

}
