package com.velos.ordercomponent.business.pojoobjects;

public class Progress {
	
	private Integer totalRecords;
	private Integer processedRecord;
	private Integer totalRemainRecord;
	private String fileName;
	private String fileType;
	private Integer progress;	
	public static Progress obj = null;
	
	public Integer getProgress() {
		return progress;
	}
	public void setProgress(Integer progress) {
		this.progress = progress;
	}
	public Integer getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(Integer totalRecords) {
		this.totalRecords = totalRecords;
	}
	public Integer getProcessedRecord() {
		return processedRecord;
	}
	public void setProcessedRecord(Integer processedRecord) {
		this.processedRecord = processedRecord;
	}
	public Integer getTotalRemainRecord() {
		return totalRemainRecord;
	}
	public void setTotalRemainRecord(Integer totalRemainRecord) {
		this.totalRemainRecord = totalRemainRecord;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	
	private Progress(){}

	public static Progress makeInstance(){
		if(obj==null){
			obj = new Progress();
		}
		return obj;
	}
	
}
