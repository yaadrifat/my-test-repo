package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author jamarendra
 *
 */
@Entity
@Table(name = "CB_ASSESSMENT")
@SequenceGenerator(sequenceName = "SEQ_CB_ASSESSMENT", name = "SEQ_CB_ASSESSMENT",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true, dynamicUpdate = true)
public class Assessment extends Auditable{
	private Long pkAssessment;
	private Long entityId;
	private Long entityType;
	private Long subEntityId;
	private Long subEntityType;
	private String assessmentRemarks;
	private Boolean flagforLater;
	private Long assessmentResp;
	private Boolean visibleTCFlag;
	private Long assessmentReason;
	private String reasonRemarks;
	private Date availableDate;
	private Boolean sendReviewFlag;
	private Boolean consultFlag;
	private String assessmentReviewed;
	private String columnReference;
	private String deletedFlag;
	private Long assessmentpostedby;
	private Date assessmentpostedon;
	private Long assessmentconsultby;
	private Date assessmentconsulton;
	private String assessmentFlagComment;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CB_ASSESSMENT")
	@Column(name = "PK_ASSESSMENT")
	public Long getPkAssessment() {
		return pkAssessment;
	}
	public void setPkAssessment(Long pkAssessment) {
		this.pkAssessment = pkAssessment;
	}
	
	@Column(name = "ENTITY_ID")
	public Long getEntityId() {
		return entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	
	@Column(name = "ENTITY_TYPE")
	public Long getEntityType() {
		return entityType;
	}
	public void setEntityType(Long entityType) {
		this.entityType = entityType;
	}
	
	@Column(name = "SUB_ENTITY_ID")
	public Long getSubEntityId() {
		return subEntityId;
	}
	public void setSubEntityId(Long subEntityId) {
		this.subEntityId = subEntityId;
	}
	
	@Column(name = "SUB_ENTITY_TYPE")
	public Long getSubEntityType() {
		return subEntityType;
	}
	public void setSubEntityType(Long subEntityType) {
		this.subEntityType = subEntityType;
	}
	
	@Column(name = "ASSESSMENT_REMARKS")
	public String getAssessmentRemarks() {
		return assessmentRemarks;
	}
	public void setAssessmentRemarks(String assessmentRemarks) {
		this.assessmentRemarks = assessmentRemarks;
	}
	
	@Column(name = "ASSESSMENT_FOR_RESPONSE")
	public Long getAssessmentResp() {
		return assessmentResp;
	}
	public void setAssessmentResp(Long assessmentResp) {
		this.assessmentResp = assessmentResp;
	}
	

	@Column(name = "ASSESSMENT_REASON")
	public Long getAssessmentReason() {
		return assessmentReason;
	}
	public void setAssessmentReason(Long assessmentReason) {
		this.assessmentReason = assessmentReason;
	}
	
	@Column(name = "ASSESSMENT_REASON_REMARKS")
	public String getReasonRemarks() {
		return reasonRemarks;
	}
	public void setReasonRemarks(String reasonRemarks) {
		this.reasonRemarks = reasonRemarks;
	}
	
	@Column(name = "AVAILABILITY_DATE")
	public Date getAvailableDate() {
		return availableDate;
	}
	public void setAvailableDate(Date availableDate) {
		this.availableDate = availableDate;
	}
	
	@Column(name = "ASSESSMENT_REVIEWED")
	public String getAssessmentReviewed() {
		return assessmentReviewed;
	}
	public void setAssessmentReviewed(String assessmentReviewed) {
		this.assessmentReviewed = assessmentReviewed;
	}	
	@Column(name = "COLUMN_REFERENCE")
	public String getColumnReference() {
		return columnReference;
	}
	public void setColumnReference(String columnReference) {
		this.columnReference = columnReference;
	}
	@Column(name = "DELETEDFLAG")
	public String getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(String deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	
	@Column(name = "ASSESSMENT_POSTEDBY")
	public Long getAssessmentpostedby() {
		return assessmentpostedby;
	}
	public void setAssessmentpostedby(Long assessmentpostedby) {
		this.assessmentpostedby = assessmentpostedby;
	}
	
	@Column(name = "ASSESSMENT_POSTEDON")
	public Date getAssessmentpostedon() {
		return assessmentpostedon;
	}
	public void setAssessmentpostedon(Date assessmentpostedon) {
		this.assessmentpostedon = assessmentpostedon;
	}
	
	@Column(name = "ASSESSMENT_CONSULTBY")
	public Long getAssessmentconsultby() {
		return assessmentconsultby;
	}
	public void setAssessmentconsultby(Long assessmentconsultby) {
		this.assessmentconsultby = assessmentconsultby;
	}
	
	@Column(name = "ASSESSMENT_CONSULTON")
	public Date getAssessmentconsulton() {
		return assessmentconsulton;
	}
	public void setAssessmentconsulton(Date assessmentconsulton) {
		this.assessmentconsulton = assessmentconsulton;
	}
	@Column(name = "FLAG_FOR_LATER")
	public Boolean getFlagforLater() {
		return flagforLater;
	}
	public void setFlagforLater(Boolean flagforLater) {
		this.flagforLater = flagforLater;
	}
	@Column(name = "ASSESSMENT_FLAG_COMMENT")
	public String getAssessmentFlagComment() {
		return assessmentFlagComment;
	}
	public void setAssessmentFlagComment(String assessmentFlagComment) {
		this.assessmentFlagComment = assessmentFlagComment;
	}
	@Column(name = "TC_VISIBILITY_FLAG")
	public Boolean getVisibleTCFlag() {
		return visibleTCFlag;
	}
	public void setVisibleTCFlag(Boolean visibleTCFlag) {
		this.visibleTCFlag = visibleTCFlag;
	}
	@Column(name = "SENT_TO_REVIEW_FLAG")
	public Boolean getSendReviewFlag() {
		return sendReviewFlag;
	}
	public void setSendReviewFlag(Boolean sendReviewFlag) {
		this.sendReviewFlag = sendReviewFlag;
	}
	@Column(name = "CONSULTE_FLAG")
	public Boolean getConsultFlag() {
		return consultFlag;
	}
	public void setConsultFlag(Boolean consultFlag) {
		this.consultFlag = consultFlag;
	}
	
}