/**
 * 
 */
package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

/**
 * @author jidrushbasha
 *
 */
public class AlertsListPojo {

	private Long alertInstanceId;
	private String alertTitle;
	private String alertWording;
	private String alertCloseFlag;
	private String orderType;
	private String orderNum;
	private String cordRegistryId;
	private String orderStatus;
	private String orderDate;
	private Long cordId;
	private Long orderId;
	private Long orderStatusId;
	private Long ordertypeId;
	private Long fksiteid;
	private String alertCreatedOn;
	private String regId1;
	
	
	
	public String getRegId1() {
		return regId1;
	}
	public void setRegId1(String regId1) {
		this.regId1 = regId1;
	}
	public String getAlertCreatedOn() {
		return alertCreatedOn;
	}
	public void setAlertCreatedOn(String alertCreatedOn) {
		this.alertCreatedOn = alertCreatedOn;
	}
	public Long getFksiteid() {
		return fksiteid;
	}
	public void setFksiteid(Long fksiteid) {
		this.fksiteid = fksiteid;
	}
	public Long getOrdertypeId() {
		return ordertypeId;
	}
	public void setOrdertypeId(Long ordertypeId) {
		this.ordertypeId = ordertypeId;
	}
	public Long getOrderStatusId() {
		return orderStatusId;
	}
	public void setOrderStatusId(Long orderStatusId) {
		this.orderStatusId = orderStatusId;
	}
	public Long getAlertInstanceId() {
		return alertInstanceId;
	}
	public void setAlertInstanceId(Long alertInstanceId) {
		this.alertInstanceId = alertInstanceId;
	}
	public String getAlertTitle() {
		return alertTitle;
	}
	public void setAlertTitle(String alertTitle) {
		this.alertTitle = alertTitle;
	}
	public String getAlertWording() {
		return alertWording;
	}
	public void setAlertWording(String alertWording) {
		this.alertWording = alertWording;
	}
	public String getAlertCloseFlag() {
		return alertCloseFlag;
	}
	public void setAlertCloseFlag(String alertCloseFlag) {
		this.alertCloseFlag = alertCloseFlag;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}
	public String getCordRegistryId() {
		return cordRegistryId;
	}
	public void setCordRegistryId(String cordRegistryId) {
		this.cordRegistryId = cordRegistryId;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public Long getCordId() {
		return cordId;
	}
	public void setCordId(Long cordId) {
		this.cordId = cordId;
	}
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	
	
	
	
}
