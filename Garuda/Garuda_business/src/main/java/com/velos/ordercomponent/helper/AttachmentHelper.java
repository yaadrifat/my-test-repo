package com.velos.ordercomponent.helper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;

import com.velos.ordercomponent.business.domain.Attachment;
import com.velos.ordercomponent.business.domain.CBUUnitReport;
import com.velos.ordercomponent.business.domain.CdrCbu;
import com.velos.ordercomponent.business.domain.DcmsLog;
import com.velos.ordercomponent.business.domain.EntityStatusReason;
import com.velos.ordercomponent.business.pojoobjects.AttachmentPojo;
import com.velos.ordercomponent.business.pojoobjects.DcmsLogPojo;
import com.velos.ordercomponent.business.pojoobjects.EntityStatusReasonPojo;
import com.velos.ordercomponent.business.util.HibernateUtil;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.VelosUtil;

public class AttachmentHelper {

	public static final Log log=LogFactory.getLog(AttachmentHelper.class);
	
	public static AttachmentPojo addAttachments(AttachmentPojo attachmentPojo)
	{
		//log.debug("Add Attachments method start");
		Attachment attachment = new Attachment();
		/*InputStream fis ;
		int errorCode = 100;*/
		try{
			attachment = (Attachment)ObjectTransfer.transferObjects(attachmentPojo, attachment);
			Object object = new VelosUtil().setAuditableInfo(attachment);
			//log.debug("----------");
			/*if(attachment.getDocumentFile()==null){
			fis = new FileInputStream(attachmentPojo.getDocFile());
			Blob blob = Hibernate.createBlob(fis);
			attachment.setDocumentFile(blob);
			}*/
			//// object = new VelosUtil().setAuditableInfo(attachment);
			//Auditable auditable = new VelosUtil().setAuditableInfo();
			//attachment.setAuditable(auditable);
			////log.debug(((Attachment)object).getIpAddress()+"-------------"+((Attachment)object).getCreatedOn());;
			attachment = addAttachment((Attachment)object);
			attachmentPojo.setAttachmentId(attachment.getAttachmentId());
			
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		log.error("AttachmentHelper : addAttachments() method end :: ");
		return attachmentPojo;		
	}
	
	public static Attachment addAttachment(Attachment attachment)
	{
		//log.debug("Add Attachment method with session");
		return addAttachment(attachment,null);
	}
	
	public static Attachment addAttachment(Attachment attachment,Session session)
	{
		//log.debug("Add Attachment method with session");
		//log.debug("VelosStudyHelper : addDocument Method Start ");
		boolean sessionFlag = false;
		try{
		 // check session 
		if(session== null){
			// create new session
		  session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}
		if(attachment.getAttachmentId()!=null)
		{session.merge(attachment);
		}else{
			session.save(attachment);
		}
		if(sessionFlag){
			// close session, if session created in this method
			session.getTransaction().commit();
		}
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		//log.debug("AttachmentHelper : addAttachment Method End ");
		return attachment;
	}
	
	public static List<AttachmentPojo> addMultipleAttachments(List<AttachmentPojo> attachmentpojos)
	{
		//log.debug("Add Attachments method start");
		Attachment attachment = new Attachment();
//		InputStream fis ;
//		int errorCode = 100;
		try{
			for(AttachmentPojo a : attachmentpojos){
				attachment = (Attachment)ObjectTransfer.transferObjects(a, attachment);
				Object object = new VelosUtil().setAuditableInfo(attachment);
				//log.debug("----------");
				if(attachment.getDocumentFile()==null){
//				fis = new FileInputStream(a.getDocFile());
//				Blob blob = Hibernate.createBlob(fis);
//				attachment.setDocumentFile(blob);
				}
				attachment = addAttachment((Attachment)object);
				a.setAttachmentId(attachment.getAttachmentId());
			}
			
			//// object = new VelosUtil().setAuditableInfo(attachment);
			//Auditable auditable = new VelosUtil().setAuditableInfo();
			//attachment.setAuditable(auditable);
			////log.debug(((Attachment)object).getIpAddress()+"-------------"+((Attachment)object).getCreatedOn());;
			
			
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
	
		return attachmentpojos;		
	}
	
	public static String getDcmsFileDocType(Long attachmentID){
		return getDcmsFileDocType(attachmentID,null);
	}
	
	public static String getDcmsFileDocType(Long attachmentID , Session session){
		String filetype=null;
		List<String> list = new ArrayList<String>();
		try{
			if(session== null){
				// create new session
			  session = HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			//log.debug("in heloper class before query");
			list =(List<String>)session.createSQLQuery("SELECT DOCUMENT_TYPE FROM ER_ATTACHMENTS where PK_ATTACHMENT="+attachmentID ).list();
			if(list!=null && list.size()>0)
			{
				filetype = list.get(0);
				//log.debug("filetype----"+filetype);
			}
			}catch(Exception e){
				log.error(e.getMessage());
				e.printStackTrace();
			}finally{
				if(session.isOpen()){
					session.close();
				}
			}
		return filetype;
	}
	
	public static String getDcmsAttchFileName(Long attachmentID){
		return getDcmsAttchFileName(attachmentID,null);
	}
	
	public static String getDcmsAttchFileName(Long attachmentID , Session session){
		String filename=null;
		List<String> list = new ArrayList<String>();
		try{
			if(session== null){
				// create new session
			  session = HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			//log.debug("in heloper class before query");
			list =(List<String>)session.createSQLQuery("SELECT ATTACHMENT_FILE_NAME FROM ER_ATTACHMENTS where PK_ATTACHMENT="+attachmentID ).list();
			if(list!=null && list.size()>0)
			{
				filename = list.get(0);
				//log.debug("filetype----"+filename);
			}
			}catch(Exception e){
				log.error(e.getMessage());
				e.printStackTrace();
			}finally{
				if(session.isOpen()){
					session.close();
				}
			}
		return filename;
	}
	
	public static Long getDcmsFileAttchId(Long attachmentID){
		return getDcmsFileAttchId(attachmentID,null);
	}
	
	public static Long getDcmsFileAttchId(Long attachmentID , Session session){
		boolean sessionFlag = false;
		BigDecimal attid=null;
		List<BigDecimal> list = new ArrayList<BigDecimal>();
		try{
			 // check session 
			if(session== null){
				// create new session
			  session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			//log.debug("in heloper class before query");
			list =(List<BigDecimal>)session.createSQLQuery("SELECT DCMS_FILE_ATTACHMENT_ID FROM ER_ATTACHMENTS where PK_ATTACHMENT="+attachmentID ).list();
			if(list!=null && list.size()>0)
			{
				attid = list.get(0);
				attachmentID=attid.longValue();
			}
			////log.debug("attachment id is********"+attachmentId);
//			if(sessionFlag){
//				// close session, if session created in this method
//				session.getTransaction().commit();
//			}
			}catch(Exception e){
				log.error(e.getMessage());
				e.printStackTrace();
			}finally{
				if(session.isOpen()){
					session.close();
				}
			}
		return attachmentID;
	}
	
	public static Long getDcmsAttachmentId(){
		
		return getDcmsAttachmentId(null);
	}
	
	
	public static Long getDcmsAttachmentId(Session session){
		boolean sessionFlag = false;
		Long attachmentId=null;
		BigDecimal attid=null;
		try{
			 // check session 
			if(session== null){
				// create new session
			  session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			//log.debug("in heloper class before query");
			attid=(BigDecimal)session.createSQLQuery("SELECT SEQ_DCMS_FILE_UPLOAD.nextval FROM DUAL").list().get(0);
			attachmentId=attid.longValue();
			////log.debug("attachment id is********"+attachmentId);
//			if(sessionFlag){
//				// close session, if session created in this method
//				session.getTransaction().commit();
//			}
			}catch(Exception e){
				log.error(e.getMessage());
				e.printStackTrace();
			}finally{
				if(session.isOpen()){
					session.close();
				}
			}
		return attachmentId;
	}
    public static String getDcmsFlagMethod(String flagValue){
		
		return getDcmsFlagMethod(flagValue,null);
	}
	
	
	public static String getDcmsFlagMethod(String flagValue,Session session){
		boolean sessionFlag = false;
		String dcmsFlag="Y";
		/*List lst=new ArrayList();
		try{
			 // check session 
			if(session== null){
				// create new session
			  session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			//log.debug("in heloper class before query");
		
		//	lst=session.createSQLQuery("SELECT CODELST_SUBTYP AS Y FROM ER_CODELST WHERE CODELST_TYPE='dcmsflag' AND CODELST_SUBTYP='"+flagValue+"'").list();
			//log.debug("list size is::"+lst.size());
			if(lst!=null && lst.size()>0){
				//log.debug("list is not empty");
				////log.debug("^^^^^^^^^^^^^^^^^^^^"+lst.iterator().next());
				dcmsFlag=(String)lst.iterator().next();
				//log.debug("query value is::::"+dcmsFlag);
				//log.debug("dcms flag is:::::::::"+dcmsFlag);
			}
//			//log.debug("%%%%%%%%%%%%%"+dcmsFlag);
//			if(sessionFlag){
//				// close session, if session created in this method
//				session.getTransaction().commit();
//			}
			}catch(Exception e){
				log.error("Exception in AttachmentHelper : addAttachment() :: " + e);
				e.printStackTrace();
			}finally{
				if(session.isOpen()){
					session.close();
				}
			}*/
		return dcmsFlag;
	}
	
	public static DcmsLogPojo addDcmsLog(DcmsLogPojo dcmsLogPojo){
		return addDcmsLog(dcmsLogPojo,null);
	}
	
	public static DcmsLogPojo addDcmsLog(DcmsLogPojo dcmsLogPojo,Session session){
		boolean sessionFlag = false;
		DcmsLog dcms_log=new DcmsLog();
		try{
			 // check session 
			if(session== null){
				// create new session
			  session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			dcms_log = (DcmsLog)ObjectTransfer.transferObjects(dcmsLogPojo, dcms_log);
			
			Object object = new VelosUtil().setAuditableInfo(dcms_log);
			
			//log.debug("in heloper class before query");
			session.save((DcmsLog)object);
			if(sessionFlag){
				// close session, if session created in this method
				session.getTransaction().commit();
			}
			}catch(Exception e){
				log.error(e.getMessage());
				e.printStackTrace();
			}finally{
				if(session.isOpen()){
					session.close();
				}
			}
			dcmsLogPojo = (DcmsLogPojo)ObjectTransfer.transferObjects(dcms_log, dcmsLogPojo);
		return dcmsLogPojo;
	}
	
	public  static List<Attachment> getAttachmentInfoLst(Long attachmentId){
		return getAttachmentInfoLst(attachmentId,null);
	}
	
    public static List<Attachment> getAttachmentInfoLst(Long attachmentId ,Session session){
    	List<Attachment> modifyAttachment = new ArrayList<Attachment>();
    	Attachment attachment = null;
    	 attachment = new Attachment();
    	boolean sessionFlag = false;
    	try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "From Attachment where attachmentId="+attachmentId;
			modifyAttachment = (List<Attachment>)session.createQuery(sql).list();
			
			
    	} catch (Exception e) {
			//log.debug(" Exception in getAttachmentInfoLst " + e);
    		log.error(e.getMessage());
			e.printStackTrace();
		} finally {						
			if(session.isOpen()){
				 session.close();		
			}
		}
		return modifyAttachment;
	}

	
}
