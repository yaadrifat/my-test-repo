package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


public class HBSAg {

	private String hbsag_react_ind;
	private XMLGregorianCalendar hbsag_date;
	private String cms_cert_lab_ind;
	private String fda_lic_mfg_inst_ind;
	@XmlElement(name="hbsag_react_ind")
	public String getHbsag_react_ind() {
		return hbsag_react_ind;
	}
	public void setHbsag_react_ind(String hbsagReactInd) {
		hbsag_react_ind = hbsagReactInd;
	}
	@XmlElement(name="hbsag_date")
	@XmlJavaTypeAdapter(type=XMLGregorianCalendar.class,value=XMLGeorgarianCalanderAdapter.class)
	public XMLGregorianCalendar getHbsag_date() {
		return hbsag_date;
	}
	public void setHbsag_date(XMLGregorianCalendar hbsagDate) {
		hbsag_date = hbsagDate;
	}
	@XmlElement(name="cms_cert_lab_ind")
	public String getCms_cert_lab_ind() {
		return cms_cert_lab_ind;
	}
	public void setCms_cert_lab_ind(String cmsCertLabInd) {
		cms_cert_lab_ind = cmsCertLabInd;
	}
	@XmlElement(name="fda_lic_mfg_inst_ind")
	public String getFda_lic_mfg_inst_ind() {
		return fda_lic_mfg_inst_ind;
	}
	public void setFda_lic_mfg_inst_ind(String fdaLicMfgInstInd) {
		fda_lic_mfg_inst_ind = fdaLicMfgInstInd;
	}
	
	
}
