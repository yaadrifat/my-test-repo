package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

public class PackingSlipPojo extends AuditablePojo {
	
	private Long pkPackingSlip;
	private Long fkShipmentId;
	private Long fkOrderId;
	private Date schShipDate;
	private String shipTrackingNo;
	private Long fkShipCompanyId;
	private Long fkCbbId;
	private Long fkCordId;
	private Long fkSampleType;
	private Long fkAliquotType;
	private Long fkHlaId;
	
	
	
	
	public Long getPkPackingSlip() {
		return pkPackingSlip;
	}
	public void setPkPackingSlip(Long pkPackingSlip) {
		this.pkPackingSlip = pkPackingSlip;
	}
	public Long getFkShipmentId() {
		return fkShipmentId;
	}
	public void setFkShipmentId(Long fkShipmentId) {
		this.fkShipmentId = fkShipmentId;
	}
	public Long getFkOrderId() {
		return fkOrderId;
	}
	public void setFkOrderId(Long fkOrderId) {
		this.fkOrderId = fkOrderId;
	}
	public Date getSchShipDate() {
		return schShipDate;
	}
	public void setSchShipDate(Date schShipDate) {
		this.schShipDate = schShipDate;
	}
	public String getShipTrackingNo() {
		return shipTrackingNo;
	}
	public void setShipTrackingNo(String shipTrackingNo) {
		this.shipTrackingNo = shipTrackingNo;
	}
	public Long getFkShipCompanyId() {
		return fkShipCompanyId;
	}
	public void setFkShipCompanyId(Long fkShipCompanyId) {
		this.fkShipCompanyId = fkShipCompanyId;
	}
	public Long getFkCbbId() {
		return fkCbbId;
	}
	public void setFkCbbId(Long fkCbbId) {
		this.fkCbbId = fkCbbId;
	}
	public Long getFkCordId() {
		return fkCordId;
	}
	public void setFkCordId(Long fkCordId) {
		this.fkCordId = fkCordId;
	}
	public Long getFkSampleType() {
		return fkSampleType;
	}
	public void setFkSampleType(Long fkSampleType) {
		this.fkSampleType = fkSampleType;
	}
	public Long getFkAliquotType() {
		return fkAliquotType;
	}
	public void setFkAliquotType(Long fkAliquotType) {
		this.fkAliquotType = fkAliquotType;
	}
	public Long getFkHlaId() {
		return fkHlaId;
	}
	public void setFkHlaId(Long fkHlaId) {
		this.fkHlaId = fkHlaId;
	}

	
	

}
