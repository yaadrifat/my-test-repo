package com.velos.ordercomponent.business.pojoobjects;

import java.io.File;
import java.sql.Blob;
import java.util.Date;

public class OrderAttachmentPojo extends AuditablePojo {
	private Long pk_orderattachmentId;
	private Long fk_orderid;
	private Long fk_attachmentId;
	private Boolean deletedFlag;
	
	public Long getPk_orderattachmentId() {
		return pk_orderattachmentId;
	}
	public void setPk_orderattachmentId(Long pk_orderattachmentId) {
		this.pk_orderattachmentId = pk_orderattachmentId;
	}
	public Long getFk_orderid() {
		return fk_orderid;
	}
	public void setFk_orderid(Long fk_orderid) {
		this.fk_orderid = fk_orderid;
	}
	public Long getFk_attachmentId() {
		return fk_attachmentId;
	}
	public void setFk_attachmentId(Long fk_attachmentId) {
		this.fk_attachmentId = fk_attachmentId;
	}
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	
	
	
}
