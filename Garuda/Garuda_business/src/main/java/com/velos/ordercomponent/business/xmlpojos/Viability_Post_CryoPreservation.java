package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.datatype.XMLGregorianCalendar;

public class Viability_Post_CryoPreservation {
    private String Viability_test;
    private String Viability_cnt;
    private XMLGregorianCalendar Viability_test_date;
    private String Viability_test_method;
    private String Viability_test_reason;
    @XmlElement(name="Viability_test")	
	public String getViability_test() {
		return Viability_test;
	}
	public void setViability_test(String viabilityTest) {
		Viability_test = viabilityTest;
	}
	@XmlElement(name="Viability_cnt")	
	public String getViability_cnt() {
		return Viability_cnt;
	}
	public void setViability_cnt(String viabilityCnt) {
		Viability_cnt = viabilityCnt;
	}
	@XmlElement(name="Viability_test_date")	
	public XMLGregorianCalendar getViability_test_date() {
		return Viability_test_date;
	}
	public void setViability_test_date(XMLGregorianCalendar viabilityTestDate) {
		Viability_test_date = viabilityTestDate;
	}
	@XmlElement(name="Viability_test_method")	
	public String getViability_test_method() {
		return Viability_test_method;
	}
	public void setViability_test_method(String viabilityTestMethod) {
		Viability_test_method = viabilityTestMethod;
	}
	@XmlElement(name="Viability_test_reason")	
	public String getViability_test_reason() {
		return Viability_test_reason;
	}
	public void setViability_test_reason(String viabilityTestReason) {
		Viability_test_reason = viabilityTestReason;
	}
    
    
}
