package com.velos.ordercomponent.business.pojoobjects;

import java.io.File;
import java.io.InputStream;

public class CordImportPojo {
	
	private Long fileLength;
	private Integer totalNoCords;
	private Integer totalProcessedCords;
	private Integer totalRemainCords;
	private Integer progress;
	private File fileUpload;
	private String fileUploadContentType;
	private String fileUploadFileName;
	private InputStream iso;
	
	
	public File getFileUpload() {
		return fileUpload;
	}
	public void setFileUpload(File fileUpload) {
		this.fileUpload = fileUpload;
	}
	public String getFileUploadContentType() {
		return fileUploadContentType;
	}
	public void setFileUploadContentType(String fileUploadContentType) {
		this.fileUploadContentType = fileUploadContentType;
	}
	public String getFileUploadFileName() {
		return fileUploadFileName;
	}
	public void setFileUploadFileName(String fileUploadFileName) {
		this.fileUploadFileName = fileUploadFileName;
	}
	public Integer getProgress() {
		return progress;
	}
	public void setProgress(Integer progress) {
		this.progress = progress;
	}
	public Long getFileLength() {
		return fileLength;
	}
	public void setFileLength(Long fileLength) {
		this.fileLength = fileLength;
	}
	public Integer getTotalNoCords() {
		return totalNoCords;
	}
	public void setTotalNoCords(Integer totalNoCords) {
		this.totalNoCords = totalNoCords;
	}
	public Integer getTotalProcessedCords() {
		return totalProcessedCords;
	}
	public void setTotalProcessedCords(Integer totalProcessedCords) {
		this.totalProcessedCords = totalProcessedCords;
	}
	public Integer getTotalRemainCords() {
		return totalRemainCords;
	}
	public void setTotalRemainCords(Integer totalRemainCords) {
		this.totalRemainCords = totalRemainCords;
	}
	public InputStream getIso() {
		return iso;
	}
	public void setIso(InputStream iso) {
		this.iso = iso;
	}
	

	
}
