package com.velos.ordercomponent.helper;



import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.classic.Session;

import com.velos.ordercomponent.business.domain.CBUploadInfo;
import com.velos.ordercomponent.business.domain.CdrCbu;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.util.HibernateUtil;

public class UploadDocHelper {
	public static final Log log=LogFactory.getLog(UploadDocHelper.class);
		/**
		 * This method returns the criteria of attachments for particular CBUID
		 * @param entityId
		 * @param entityTypeId
		 * @param attachmentType
		 * @return
		 * @throws Exception
		 */
	public static String getCbuAttachDetails(Long entityId,Long entityTypeId,Long attachmentType) throws Exception{
		
		String criteria = "";
		if(entityId !=null && entityId >0 ){
		criteria ="Select cordinfo.cordID,cordinfo.cdrCbuId,cordinfo.externalCbuId,cordinfo.registryId,cordinfo.localCbuId, "+
			"attach.dcmsAttachmentId,attach.attachmentId,attach.documentType,attach.documentFile,attach.fileName,attach.createdOn "+
			"from CdrCbu cordinfo,Attachment attach where cordinfo.cordID=attach.garudaEntityId and attach.garudaEntityId= "+ entityId +" and attach.garudaEntityType= " + entityTypeId +
			" and attach.garudaAttachmentType= "+ attachmentType;

			}
			
		
		return criteria;
	}	
		
	
	public static String getAttachments(Long entityId,Long entityTypeId,Long attachmentType,String categoryName) throws Exception {
		
		String appendquery="";
		String cbuDetailsQuery = "";
		
		if(categoryName!=null) {
			appendquery = getFkAttachmentId(categoryName);
		
		}
		
		
		cbuDetailsQuery = getCbuAttachDetails(entityId,entityTypeId,attachmentType);
		
		cbuDetailsQuery = cbuDetailsQuery + " and attach.attachmentId in (" + appendquery + ")";
		
		
		return cbuDetailsQuery;
	}
	
	
	public static String getOtherTestAttachInfo(Long entityId,Long entityTypeId,Long attachmentType,String categoryName) throws Exception {
		
		String appendquery="";
		String otherTestQuery = "";
		
		if(categoryName!=null) {
			appendquery = getFkAttachIdOtherTest(categoryName);
		
		}
		
		
		otherTestQuery = getCbuAttachDetails(entityId,entityTypeId,attachmentType);
		
		otherTestQuery = otherTestQuery + " and attach.attachmentId in (" + appendquery + ")";
		
		
		return otherTestQuery;
	}
	
	
	
	// health history screen
	public static String getHealthHistoryInfo(Long entityId,Long entityTypeId,Long attachmentType,String categoryName) throws Exception {
		
		String appendquery="";
		String otherTestQuery = "";
		
		if(categoryName!=null) {
			appendquery = getFkAttachIdHealthHistory(categoryName);
		
		}
		
		
		otherTestQuery = getCbuAttachDetails(entityId,entityTypeId,attachmentType);
		
		otherTestQuery = otherTestQuery + " and attach.attachmentId in (" + appendquery + ")";
		
		
		return otherTestQuery;
	}
	
	
	
	/**
	 * This method returns the fkAttachmentId of Category
	 * @param categoryName
	 * @return
	 * @throws Exception
	 */
	public static String getFkAttachmentId(String categoryName) throws Exception{
		
		String query="";
	
		query="select a.fk_AttachmentId from CBUploadInfo a, CodeList b where a.fk_CategoryId=b.pkCodeId and lower(b.type)='doc_categ' and lower(b.subType) = " + "'"+ categoryName + "'";
		
		
		//return velosHelper.getObjectList(query);
		return query;
	}
	public static String getFkAttachIdOtherTest(String categoryName) throws Exception{
		
		String query="";
	
		query="select a.fk_AttachmentId from CBUploadInfo a, CodeList b where a.fk_CategoryId=b.pkCodeId and lower(b.type)='doc_categ' and lower(b.subType) = " + "'"+ categoryName + "'";
		
		
		//return velosHelper.getObjectList(query);
		return query;
	}
	
	//HealthHistory screen
	
	public static String getFkAttachIdHealthHistory(String categoryName) throws Exception{
		
		String query="";
	
		query="select a.fk_AttachmentId from CBUploadInfo a, CodeList b where a.fk_CategoryId=b.pkCodeId and lower(b.type)='doc_categ' and lower(b.subType) = " + "'"+ categoryName + "'";
		
		
		//return velosHelper.getObjectList(query);
		return query;
	}
	
	public static List<CBUploadInfo> getCbUploadInfoLst(Long attachmentId){
		return getCbUploadInfoLst(attachmentId, null);
	}
	
	public static List<CBUploadInfo> getCbUploadInfoLst(
			Long attachmentId, Session session) {
		List<CBUploadInfo> returnList = null;
       	boolean sessionFlag = false;
    	try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "From CBUploadInfo where fk_AttachmentId="+attachmentId;
			returnList = (List<CBUploadInfo>) session.createQuery(sql).list();
			
			
    	} catch (Exception e) {
    		log.error(e.getMessage());
			e.printStackTrace();
		} finally {						
			if(session.isOpen()){
				 session.close();		
			}
		}
		return returnList;
	}
	
	
	public static String getEntityDocuments(Long entityId,Long entityTypeId,Long attachmentType) throws Exception{
		
		String criteria = "";
		//log.debug("EntityId  entityType  attachmentType in uploaddochelper "+entityId + entityTypeId + attachmentType);
		if(entityId !=null && entityId >0 ){
		criteria ="Select attach.attachmentId,attach.documentType,attach.documentFile,attach.fileName,attach.createdOn "+
		" from Attachment attach where attach.garudaEntityId= " + entityId +" and attach.garudaEntityType= " + entityTypeId +
		" and attach.garudaAttachmentType= " + attachmentType;
		}
		else{
			criteria ="Select attach.attachmentId,attach.documentType,attach.documentFile,attach.fileName,attach.createdOn "+
			" from Attachment attach where attach.garudaEntityType= " + entityTypeId +
			" and attach.garudaAttachmentType= " + attachmentType;
		}
		
		return criteria;
		
	}
	
	/**
	 * This method returns the PkCordId for particular CBUID
	 * @param cdrCbuPojo
	 * @return
	 * @throws Exception
	 */
	public static Long getCbuPkCordId(CdrCbuPojo cdrCbuPojo) throws Exception{
		return getCbuPkCordId(cdrCbuPojo,null);
	}
	public static Long getCbuPkCordId(CdrCbuPojo cdrCbuPojo,Session session) throws Exception{
		boolean sessionFlag = false;
		Long pkCordId = null;
		try {
			 // check session 
			if(session== null){
				// create new session
			  session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if(cdrCbuPojo.getCdrCbuId() != null && !cdrCbuPojo.getCdrCbuId().isEmpty()){
			List lstCordIdList = (List<CdrCbu>)session.createQuery("select cordID from CdrCbu cordinfo where lower(cordinfo.cdrCbuId) like lower('%" +
				cdrCbuPojo.getCdrCbuId() + "%')" ).list();
					if(lstCordIdList != null && !lstCordIdList.isEmpty()){
					pkCordId = (Long)lstCordIdList.get(0);
				}
			}else{
				 pkCordId = null;
				
			}
			
		} catch (Exception e) {
			//log.debug(e);
			log.error(e.getMessage());
			e.printStackTrace();
			
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return pkCordId;
	
	}
}
	

