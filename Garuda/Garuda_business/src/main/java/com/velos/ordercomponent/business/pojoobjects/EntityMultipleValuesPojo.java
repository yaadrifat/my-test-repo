package com.velos.ordercomponent.business.pojoobjects;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class EntityMultipleValuesPojo extends AuditablePojo {
	
	private Long pkEntityMultiple;
	private String entityMultipleType;
	private Long entityMultipleValue;
	private Long entityId;
	private String entityType;
	public Long getPkEntityMultiple() {
		return pkEntityMultiple;
	}
	public void setPkEntityMultiple(Long pkEntityMultiple) {
		this.pkEntityMultiple = pkEntityMultiple;
	}
	public String getEntityMultipleType() {
		return entityMultipleType;
	}
	public void setEntityMultipleType(String entityMultipleType) {
		this.entityMultipleType = entityMultipleType;
	}
	public Long getEntityMultipleValue() {
		return entityMultipleValue;
	}
	public void setEntityMultipleValue(Long entityMultipleValue) {
		this.entityMultipleValue = entityMultipleValue;
	}
	public Long getEntityId() {
		return entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	public String getEntityType() {
		return entityType;
	}
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}
	
	

}
