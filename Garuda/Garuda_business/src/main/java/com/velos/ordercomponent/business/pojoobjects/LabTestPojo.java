package com.velos.ordercomponent.business.pojoobjects;

public class LabTestPojo {
	
	private Long pkLabtest;
	private String labtestName;
	private String shrtName;
	private String labtestCpt;
	private Long fkADVNci;
	private String grdCalc;
	private Long fktestgroup;
	private Long labtestSeq;
	private String labtestHower;
	
	public Long getFktestgroup() {
		return fktestgroup;
	}
	public void setFktestgroup(Long fktestgroup) {
		this.fktestgroup = fktestgroup;
	}
	public Long getPkLabtest() {
		return pkLabtest;
	}
	public void setPkLabtest(Long pkLabtest) {
		this.pkLabtest = pkLabtest;
	}
	public String getLabtestName() {
		return labtestName;
	}
	public void setLabtestName(String labtestName) {
		this.labtestName = labtestName;
	}
	public String getShrtName() {
		return shrtName;
	}
	public void setShrtName(String shrtName) {
		this.shrtName = shrtName;
	}
	public String getLabtestCpt() {
		return labtestCpt;
	}
	public void setLabtestCpt(String labtestCpt) {
		this.labtestCpt = labtestCpt;
	}
	public Long getFkADVNci() {
		return fkADVNci;
	}
	public void setFkADVNci(Long fkADVNci) {
		this.fkADVNci = fkADVNci;
	}
	public String getGrdCalc() {
		return grdCalc;
	}
	public void setGrdCalc(String grdCalc) {
		this.grdCalc = grdCalc;
	}
	public Long getLabtestSeq() {
		return labtestSeq;
	}
	public void setLabtestSeq(Long labtestSeq) {
		this.labtestSeq = labtestSeq;
	}
	public String getLabtestHower() {
		return labtestHower;
	}
	public void setLabtestHower(String labtestHower) {
		this.labtestHower = labtestHower;
	}

}
