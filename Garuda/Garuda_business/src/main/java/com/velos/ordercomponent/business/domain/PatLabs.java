package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "er_patlabs")
@SequenceGenerator(sequenceName="SEQ_ER_PATLABS",name="SEQ_ER_PATLABS",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class PatLabs extends Auditable{
	private Long pkpatlabs;
	private Long fkper;
	private Long fktestid;
	private Date testdate;
	private String testresult;
	private Long fksite;
	private Long fktestgroup;
	private Long fkspecimen;
	private Long fktestoutcome;
	private Long cmsapprovedlab;
	private Long fdalicensedlab;
	private String fktestreason;
	private Long fktestmethod;
	private Long fttestsource;
	private Long fktestspecimen;
	private Long fkTimingOfTest;
	private String testDescription;
	private String testcomments;
	private Long viabilityMthd;
	private Long testcount;
	private Long fkidmmaster;
	
	private String notes;
	private String reasonTestDesc;
	private String testMthdDesc;
	
	private String otherListValue;
	
	

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_ER_PATLABS")
	@Column(name="PK_PATLABS")
	public Long getPkpatlabs() {
		return pkpatlabs;
	}
	public void setPkpatlabs(Long pkpatlabs) {
		this.pkpatlabs = pkpatlabs;
	}
	
	@Column(name="FK_PER")
	public Long getFkper() {
		return fkper;
	}
	public void setFkper(Long fkper) {
		this.fkper = fkper;
	}
	
	@Column(name="FK_TESTID")
	public Long getFktestid() {
		return fktestid;
	}
	public void setFktestid(Long fktestid) {
		this.fktestid = fktestid;
	}
	
	@Column(name="TEST_DATE")
	public Date getTestdate() {
		return testdate;
	}
	public void setTestdate(Date testdate) {
		this.testdate = testdate;
	}
	
	@Column(name="TEST_RESULT")
	public String getTestresult() {
		return testresult;
	}
	public void setTestresult(String testresult) {
		this.testresult = testresult;
	}
	
	@Column(name="FK_SITE")
	public Long getFksite() {
		return fksite;
	}
	public void setFksite(Long fksite) {
		this.fksite = fksite;
	}
	
	@Column(name="FK_TESTGROUP")
	public Long getFktestgroup() {
		return fktestgroup;
	}
	public void setFktestgroup(Long fktestgroup) {
		this.fktestgroup = fktestgroup;
	}
	
	
	@Column(name="FK_SPECIMEN")
	public Long getFkspecimen() {
		return fkspecimen;
	}
	public void setFkspecimen(Long fkspecimen) {
		this.fkspecimen = fkspecimen;
	}
	
	@Column(name="FK_TEST_OUTCOME")
	public Long getFktestoutcome() {
		return fktestoutcome;
	}
	public void setFktestoutcome(Long fktestoutcome) {
		this.fktestoutcome = fktestoutcome;
	}
	
	@Column(name="CMS_APPROVED_LAB")
	public Long getCmsapprovedlab() {
		return cmsapprovedlab;
	}
	public void setCmsapprovedlab(Long cmsapprovedlab) {
		this.cmsapprovedlab = cmsapprovedlab;
	}
	
	@Column(name="FDA_LICENSED_LAB")
	public Long getFdalicensedlab() {
		return fdalicensedlab;
	}
	public void setFdalicensedlab(Long fdalicensedlab) {
		this.fdalicensedlab = fdalicensedlab;
	}
	
	@Column(name="FK_TEST_REASON")
	public String getFktestreason() {
		return fktestreason;
	}
	public void setFktestreason(String fktestreason) {
		this.fktestreason = fktestreason;
	}
	
	@Column(name="FK_TEST_METHOD")
	public Long getFktestmethod() {
		return fktestmethod;
	}
	
	public void setFktestmethod(Long fktestmethod) {
		this.fktestmethod = fktestmethod;
	}
	
	@Column(name="FT_TEST_SOURCE")
	public Long getFttestsource() {
		return fttestsource;
	}
	public void setFttestsource(Long fttestsource) {
		this.fttestsource = fttestsource;
	}
	
	@Column(name="FK_TEST_SPECIMEN")
	public Long getFktestspecimen() {
		return fktestspecimen;
	}
	public void setFktestspecimen(Long fktestspecimen) {
		this.fktestspecimen = fktestspecimen;
	}
	
	@Column(name="FK_TIMING_OF_TEST")
	public Long getFkTimingOfTest() {
		return fkTimingOfTest;
	}
	public void setFkTimingOfTest(Long fkTimingOfTest) {
		this.fkTimingOfTest = fkTimingOfTest;
	}
	
	@Column(name="CUSTOM002")
	public String getTestDescription() {
		return testDescription;
	}
	public void setTestDescription(String testDescription) {
		this.testDescription = testDescription;
	}
	
	@Column(name="CUSTOM003")
	public String getTestcomments() {
		return testcomments;
	}
	public void setTestcomments(String testcomments) {
		this.testcomments = testcomments;
	}
	@Column(name="CUSTOM007")
	public Long getViabilityMthd() {
		return viabilityMthd;
	}
	public void setViabilityMthd(Long viabilityMthd) {
		this.viabilityMthd = viabilityMthd;
	}
	
	@Column(name="CUSTOM008")
	public Long getTestcount() {
		return testcount;
	}
	public void setTestcount(Long testcount) {
		this.testcount = testcount;
	}
    
	@Column(name="NOTES")
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	@Column(name="CUSTOM004")
	public String getReasonTestDesc() {
		return reasonTestDesc;
	}
	public void setReasonTestDesc(String reasonTestDesc) {
		this.reasonTestDesc = reasonTestDesc;
	}
	
	@Column(name="CUSTOM005")
	public String getTestMthdDesc() {
		return testMthdDesc;
	}
	public void setTestMthdDesc(String testMthdDesc) {
		this.testMthdDesc = testMthdDesc;
	}
	@Column(name="CUSTOM010")
	public Long getFkidmmaster() {
		return fkidmmaster;
	}
	public void setFkidmmaster(Long fkidmmaster) {
		this.fkidmmaster = fkidmmaster;
	}
	
	@Column(name="CUSTOM006")
	public String getOtherListValue() {
		return otherListValue;
	}
	public void setOtherListValue(String otherListValue) {
		this.otherListValue = otherListValue;
	}
	
	
	
}
