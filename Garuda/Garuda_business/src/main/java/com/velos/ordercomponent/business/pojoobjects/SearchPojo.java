package com.velos.ordercomponent.business.pojoobjects;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class SearchPojo implements Serializable {
	
	private Map paramMap;
	private Map codelstParam;
	private List resultlst;
	private String searchType;
	private String criteria;
	
	public Map getParamMap() {
		return paramMap;
	}
	public void setParamMap(Map paramMap) {
		this.paramMap = paramMap;
	}
	public Map getCodelstParam() {
		return codelstParam;
	}
	public void setCodelstParam(Map codelstParam) {
		this.codelstParam = codelstParam;
	}
	public List getResultlst() {
		return resultlst;
	}
	public void setResultlst(List resultlst) {
		this.resultlst = resultlst;
	}
	public String getSearchType() {
		return searchType;
	}
	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}
	public String getCriteria() {
		return criteria;
	}
	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}
	
	
	
		
}