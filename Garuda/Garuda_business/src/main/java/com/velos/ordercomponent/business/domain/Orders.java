package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
/**
 *@author Jyoti
 *
 */
@Entity
@Table(name="ER_ORDER")
@SequenceGenerator(sequenceName="SEQ_ER_ORDER",name="SEQ_ER_ORDER",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class Orders extends Auditable {
	private Long pk_OrderId;
	private Boolean deletedFlag;
	private String ack_order;
	private Date ack_date;
	private Long ack_by;
	private Long sampleTypeAvail;
	private Long aliquotsType;
	private Date orderStatusDate;
	private Long assignedTo;
	private Long orderType;
	private Long orderReviewedBy;
	private Date orderReviewedDate;
	private Date orderAssignedDate;
	private Long taskId;
	private String taskName;
	private Long resolByCbb;
	private Long resolByTc;
	private Date resolDate;
	private String sampleAtLab;
	private String cliniInfoChecklstFlag;
	private String recentHlaTypingFlag;
	private Date orderCancellationConfirmDate;
	private Long orderCancelledBy;
	private String orderCancellationAcceptFlag;
	private Long caseManagerId;
	private String caseManagerName;
	private String TransplantCenterId;
	private String TransplantCenterName;
	private String secondaryTcId;
	private String secondaryTcName;
	private String cmMailId;
	private String cmContactNo;
	private String recCliniInfoFlag;
	private String resolAckFlag;
	private Long fkOrderHeader;
	private Long orderStatus;
	private String recentHlaEnteredFlag;
	private String additiTypingFlag;
	private String cordAvailConfirmFlag;
	private String iscordavailfornmdp;
	private Date cordUnavailConfirmDate;
	private String shipmentSchFlag;
	private String cordShipedFlag;
	private String packageSlipFlag;
	private String nmdpSampleShippedFlag;
	private String completeReqCliniInfoFlag;
	private String finalReviewFlag;
	private Long orderLastViewedBy;
	private Date orderLastViewedDate;
	private String orderViewConfirmFlag;
	private Date orderViewConfirmDate;
	private String orderPriority;
	private Date resRecDate;
	private Long fkRecentHlaId;
	private Long labCode;
	private Long switchReason;
	private String switchRsnOther;
	private Date switchDate;
	private Long switchBy;
	private Long cordAvailConformedBy;
	private Date cordAvailConformedDt;
	private Long cordUnavailReason;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_ER_ORDER")
	@Column(name="PK_ORDER")
	public Long getPk_OrderId() {
		return pk_OrderId;
	}
	public void setPk_OrderId(Long pk_OrderId) {
		this.pk_OrderId = pk_OrderId;
	}
	
	@Column(name="DELETEDFLAG")
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	@Column(name="ORDER_ACK_FLAG")
	public String getAck_order() {
		return ack_order;
	}
	public void setAck_order(String ack_order) {
		this.ack_order = ack_order;
	}
	@Column(name="ORDER_ACK_DATE")
	public Date getAck_date() {
		return ack_date;
	}
	public void setAck_date(Date ack_date) {
		this.ack_date = ack_date;
	}
	@Column(name="ORDER_ACK_BY")
	public Long getAck_by() {
		return ack_by;
	}
	public void setAck_by(Long ack_by) {
		this.ack_by = ack_by;
	}
	
	@Column(name="FK_SAMPLE_TYPE_AVAIL")
	public Long getSampleTypeAvail() {
		return sampleTypeAvail;
	}
	public void setSampleTypeAvail(Long sampleTypeAvail) {
		this.sampleTypeAvail = sampleTypeAvail;
	}
	
	@Column(name="FK_ALIQUOTS_TYPE")
	public Long getAliquotsType() {
		return aliquotsType;
	}
	public void setAliquotsType(Long aliquotsType) {
		this.aliquotsType = aliquotsType;
	}
	
	
	@Column(name="ORDER_STATUS_DATE")
	public Date getOrderStatusDate() {
		return orderStatusDate;
	}
	public void setOrderStatusDate(Date orderStatusDate) {
		this.orderStatusDate = orderStatusDate;
	}
	
	@Column(name="CORD_AVAILABILITY_CONFORMEDBY")
	public Long getCordAvailConformedBy() {
		return cordAvailConformedBy;
	}
	@Column(name="CORD_AVAILABILITY_CONFORMEDON")
	public Date getCordAvailConformedDt() {
		return cordAvailConformedDt;
	}
	public void setCordAvailConformedBy(Long cordAvailConformedBy) {
		this.cordAvailConformedBy = cordAvailConformedBy;
	}
	public void setCordAvailConformedDt(Date cordAvailConformedDt) {
		this.cordAvailConformedDt = cordAvailConformedDt;
	}
	
	
	
	@Column(name="ASSIGNED_TO")
	public Long getAssignedTo() {
		return assignedTo;
	}
	public void setAssignedTo(Long assignedTo) {
		this.assignedTo = assignedTo;
	}
	
	@Column(name="ORDER_TYPE")
	public Long getOrderType() {
		return orderType;
	}
	public void setOrderType(Long orderType) {
		this.orderType = orderType;
	}
	
	@Column(name="ORDER_REVIEWED_BY")
	public Long getOrderReviewedBy() {
		return orderReviewedBy;
	}
	public void setOrderReviewedBy(Long orderReviewedBy) {
		this.orderReviewedBy = orderReviewedBy;
	}
	
	@Column(name="ORDER_REVIEWED_DATE")
	public Date getOrderReviewedDate() {
		return orderReviewedDate;
	}
	public void setOrderReviewedDate(Date orderReviewedDate) {
		this.orderReviewedDate = orderReviewedDate;
	}
	
	@Column(name="ORDER_ASSIGNED_DATE")
	public Date getOrderAssignedDate() {
		return orderAssignedDate;
	}
	public void setOrderAssignedDate(Date orderAssignedDate) {
		this.orderAssignedDate = orderAssignedDate;
	}
	
	@Column(name="TASK_ID")
	public Long getTaskId() {
		return taskId;
	}
	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}
	
	@Column(name="TASK_NAME")
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	
	@Column(name="FK_ORDER_RESOL_BY_CBB")
	public Long getResolByCbb() {
		return resolByCbb;
	}
	public void setResolByCbb(Long resolByCbb) {
		this.resolByCbb = resolByCbb;
	}
	
	@Column(name="FK_ORDER_RESOL_BY_TC")
	public Long getResolByTc() {
		return resolByTc;
	}
	public void setResolByTc(Long resolByTc) {
		this.resolByTc = resolByTc;
	}
	
	
	@Column(name="ORDER_RESOL_DATE")
	public Date getResolDate() {
		return resolDate;
	}
	public void setResolDate(Date resolDate) {
		this.resolDate = resolDate;
	}
	
	
	@Column(name="ORDER_SAMPLE_AT_LAB")
	public String getSampleAtLab() {
		return sampleAtLab;
	}
	public void setSampleAtLab(String sampleAtLab) {
		this.sampleAtLab = sampleAtLab;
	}
	
	
	@Column(name="CLINIC_INFO_CHECKLIST_STAT")
	public String getCliniInfoChecklstFlag() {
		return cliniInfoChecklstFlag;
	}
	public void setCliniInfoChecklstFlag(String cliniInfoChecklstFlag) {
		this.cliniInfoChecklstFlag = cliniInfoChecklstFlag;
	}
	
	@Column(name="RECENT_HLA_TYPING_AVAIL")
	public String getRecentHlaTypingFlag() {
		return recentHlaTypingFlag;
	}
	public void setRecentHlaTypingFlag(String recentHlaTypingFlag) {
		this.recentHlaTypingFlag = recentHlaTypingFlag;
	}
	
	@Column(name="CANCEL_CONFORM_DATE")
	public Date getOrderCancellationConfirmDate() {
		return orderCancellationConfirmDate;
	}
	public void setOrderCancellationConfirmDate(Date orderCancellationConfirmDate) {
		this.orderCancellationConfirmDate = orderCancellationConfirmDate;
	}
	
	
	@Column(name="CANCELED_BY")
	public Long getOrderCancelledBy() {
		return orderCancelledBy;
	}
	public void setOrderCancelledBy(Long orderCancelledBy) {
		this.orderCancelledBy = orderCancelledBy;
	}
	
	@Column(name="ACCPT_TO_CANCEL_REQ")
	public String getOrderCancellationAcceptFlag() {
		return orderCancellationAcceptFlag;
	}
	public void setOrderCancellationAcceptFlag(String orderCancellationAcceptFlag) {
		this.orderCancellationAcceptFlag = orderCancellationAcceptFlag;
	}
	
	@Column(name="FK_CASE_MANAGER")
	public Long getCaseManagerId() {
		return caseManagerId;
	}
	public void setCaseManagerId(Long caseManagerId) {
		this.caseManagerId = caseManagerId;
	}
	
	@Column(name="CASE_MANAGER")
	public String getCaseManagerName() {
		return caseManagerName;
	}
	public void setCaseManagerName(String caseManagerName) {
		this.caseManagerName = caseManagerName;
	}
	
	@Column(name="TRANS_CENTER_ID")
	public String getTransplantCenterId() {
		return TransplantCenterId;
	}
	public void setTransplantCenterId(String transplantCenterId) {
		TransplantCenterId = transplantCenterId;
	}
	
	@Column(name="TRANS_CENTER_NAME")
	public String getTransplantCenterName() {
		return TransplantCenterName;
	}
	public void setTransplantCenterName(String transplantCenterName) {
		TransplantCenterName = transplantCenterName;
	}
	
	@Column(name="SEC_TRANS_CENTER_ID")
	public String getSecondaryTcId() {
		return secondaryTcId;
	}
	public void setSecondaryTcId(String secondaryTcId) {
		this.secondaryTcId = secondaryTcId;
	}
	
	
	@Column(name="SEC_TRANS_CENTER_NAME")
	public String getSecondaryTcName() {
		return secondaryTcName;
	}
	public void setSecondaryTcName(String secondaryTcName) {
		this.secondaryTcName = secondaryTcName;
	}
	
	@Column(name="CM_MAIL_ID")
	public String getCmMailId() {
		return cmMailId;
	}
	public void setCmMailId(String cmMailId) {
		this.cmMailId = cmMailId;
	}
	
	@Column(name="CM_CONTACT_NO")
	public String getCmContactNo() {
		return cmContactNo;
	}
	public void setCmContactNo(String cmContactNo) {
		this.cmContactNo = cmContactNo;
	}
	
	@Column(name="REQ_CLIN_INFO_FLAG")
	public String getRecCliniInfoFlag() {
		return recCliniInfoFlag;
	}
	public void setRecCliniInfoFlag(String recCliniInfoFlag) {
		this.recCliniInfoFlag = recCliniInfoFlag;
	}
	
	@Column(name="RESOL_ACK_FLAG")
	public String getResolAckFlag() {
		return resolAckFlag;
	}
	public void setResolAckFlag(String resolAckFlag) {
		this.resolAckFlag = resolAckFlag;
	}
	
	@Column(name="FK_ORDER_HEADER")
	public Long getFkOrderHeader() {
		return fkOrderHeader;
	}
	public void setFkOrderHeader(Long fkOrderHeader) {
		this.fkOrderHeader = fkOrderHeader;
	}
	
	@Column(name="ORDER_STATUS")
	public Long getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(Long orderStatus) {
		this.orderStatus = orderStatus;
	}
	
	@Column(name="RECENT_HLA_ENTERED_FLAG")
	public String getRecentHlaEnteredFlag() {
		return recentHlaEnteredFlag;
	}
	public void setRecentHlaEnteredFlag(String recentHlaEnteredFlag) {
		this.recentHlaEnteredFlag = recentHlaEnteredFlag;
	}
	
	@Column(name="ADDITI_TYPING_FLAG")
	public String getAdditiTypingFlag() {
		return additiTypingFlag;
	}
	public void setAdditiTypingFlag(String additiTypingFlag) {
		this.additiTypingFlag = additiTypingFlag;
	}
	
	@Column(name="CBU_AVAIL_CONFIRM_FLAG")
	public String getCordAvailConfirmFlag() {
		return cordAvailConfirmFlag;
	}
	public void setCordAvailConfirmFlag(String cordAvailConfirmFlag) {
		this.cordAvailConfirmFlag = cordAvailConfirmFlag;
	}
	
	@Column(name="IS_CORD_AVAIL_FOR_NMDP")
	public String getIscordavailfornmdp() {
		return iscordavailfornmdp;
	}
	public void setIscordavailfornmdp(String iscordavailfornmdp) {
		this.iscordavailfornmdp = iscordavailfornmdp;
	}
	
	
	@Column(name="CORD_AVAIL_CONFIRM_DATE")
	public Date getCordUnavailConfirmDate() {
		return cordUnavailConfirmDate;
	}
	public void setCordUnavailConfirmDate(Date cordUnavailConfirmDate) {
		this.cordUnavailConfirmDate = cordUnavailConfirmDate;
	}
	
	@Column(name="SHIPMENT_SCH_FLAG")
	public String getShipmentSchFlag() {
		return shipmentSchFlag;
	}
	public void setShipmentSchFlag(String shipmentSchFlag) {
		this.shipmentSchFlag = shipmentSchFlag;
	}
	
	@Column(name="CORD_SHIPPED_FLAG")
	public String getCordShipedFlag() {
		return cordShipedFlag;
	}
	public void setCordShipedFlag(String cordShipedFlag) {
		this.cordShipedFlag = cordShipedFlag;
	}
	
	@Column(name="PACKAGE_SLIP_FLAG")
	public String getPackageSlipFlag() {
		return packageSlipFlag;
	}
	public void setPackageSlipFlag(String packageSlipFlag) {
		this.packageSlipFlag = packageSlipFlag;
	}
	
	@Column(name="NMDP_SAMPLE_SHIPPED_FLAG")
	public String getNmdpSampleShippedFlag() {
		return nmdpSampleShippedFlag;
	}
	public void setNmdpSampleShippedFlag(String nmdpSampleShippedFlag) {
		this.nmdpSampleShippedFlag = nmdpSampleShippedFlag;
	}
	
	@Column(name="COMPLETE_REQ_INFO_TASK_FLAG")
	public String getCompleteReqCliniInfoFlag() {
		return completeReqCliniInfoFlag;
	}
	public void setCompleteReqCliniInfoFlag(String completeReqCliniInfoFlag) {
		this.completeReqCliniInfoFlag = completeReqCliniInfoFlag;
	}
	
	
	@Column(name="FINAL_REVIEW_TASK_FLAG")
	public String getFinalReviewFlag() {
		return finalReviewFlag;
	}
	public void setFinalReviewFlag(String finalReviewFlag) {
		this.finalReviewFlag = finalReviewFlag;
	}
	
	
	@Column(name="ORDER_LAST_VIEWED_BY")
	public Long getOrderLastViewedBy() {
		return orderLastViewedBy;
	}
	public void setOrderLastViewedBy(Long orderLastViewedBy) {
		this.orderLastViewedBy = orderLastViewedBy;
	}
	
	@Column(name="ORDER_LAST_VIEWED_DATE")
	public Date getOrderLastViewedDate() {
		return orderLastViewedDate;
	}
	public void setOrderLastViewedDate(Date orderLastViewedDate) {
		this.orderLastViewedDate = orderLastViewedDate;
	}
	
	@Column(name="ORDER_VIEW_CONFIRM_FLAG")
	public String getOrderViewConfirmFlag() {
		return orderViewConfirmFlag;
	}
	public void setOrderViewConfirmFlag(String orderViewConfirmFlag) {
		this.orderViewConfirmFlag = orderViewConfirmFlag;
	}
	
	@Column(name="ORDER_VIEW_CONFIRM_DATE")
	public Date getOrderViewConfirmDate() {
		return orderViewConfirmDate;
	}
	public void setOrderViewConfirmDate(Date orderViewConfirmDate) {
		this.orderViewConfirmDate = orderViewConfirmDate;
	}
	
	
	@Column(name="ORDER_PRIORITY")
	public String getOrderPriority() {
		return orderPriority;
	}
	public void setOrderPriority(String orderPriority) {
		this.orderPriority = orderPriority;
	}
	
	/*@Column(name="FK_RESOLUTION_CODE")
	public Long getOrderResolutionCode() {
		return orderResolutionCode;
	}
	public void setOrderResolutionCode(Long orderResolutionCode) {
		this.orderResolutionCode = orderResolutionCode;
	}*/
	
	@Column(name="RESULT_REC_DATE")
	public Date getResRecDate() {
		return resRecDate;
	}
	public void setResRecDate(Date resRecDate) {
		this.resRecDate = resRecDate;
	}
	
	
	@Column(name="FK_CURRENT_HLA")
	public Long getFkRecentHlaId() {
		return fkRecentHlaId;
	}
	public void setFkRecentHlaId(Long fkRecentHlaId) {
		this.fkRecentHlaId = fkRecentHlaId;
	}
	
	@Column(name="LAB_CODE")
	public Long getLabCode() {
		return labCode;
	}
	public void setLabCode(Long labCode) {
		this.labCode = labCode;
	}
	
	@Column(name="SWITCH_REASON")
	public Long getSwitchReason() {
		return switchReason;
	}
	public void setSwitchReason(Long switchReason) {
		this.switchReason = switchReason;
	}
	
	@Column(name="SWITCH_REASON_OTHER")
	public String getSwitchRsnOther() {
		return switchRsnOther;
	}
	public void setSwitchRsnOther(String switchRsnOther) {
		this.switchRsnOther = switchRsnOther;
	}
	
	@Column(name="SWITCH_DATE")
	public Date getSwitchDate() {
		return switchDate;
	}
	public void setSwitchDate(Date switchDate) {
		this.switchDate = switchDate;
	}
	
	@Column(name="SWITCH_BY")
	public Long getSwitchBy() {
		return switchBy;
	}
	public void setSwitchBy(Long switchBy) {
		this.switchBy = switchBy;
	}
	
	@Column(name="CORD_UNAVAIL_REASON")
	public Long getCordUnavailReason() {
		return cordUnavailReason;
	}
	public void setCordUnavailReason(Long cordUnavailReason) {
		this.cordUnavailReason = cordUnavailReason;
	}
	
	
	
	
	
}