package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


public class Nat_hbv {
  
	private String nat_hbv_react_ind;
	private XMLGregorianCalendar nat_hbv_react_date;
	private String cms_cert_lab_ind;
	private String fda_lic_mfg_inst_ind;
	@XmlElement(name="nat_hbv_react_ind")	
	public String getNat_hbv_react_ind() {
		return nat_hbv_react_ind;
	}
	public void setNat_hbv_react_ind(String natHbvReactInd) {
		nat_hbv_react_ind = natHbvReactInd;
	}
	@XmlElement(name="nat_hbv_react_date")	
	@XmlJavaTypeAdapter(type=XMLGregorianCalendar.class,value=XMLGeorgarianCalanderAdapter.class)
	public XMLGregorianCalendar getNat_hbv_react_date() {
		return nat_hbv_react_date;
	}
	public void setNat_hbv_react_date(XMLGregorianCalendar natHbvReactDate) {
		nat_hbv_react_date = natHbvReactDate;
	}
	@XmlElement(name="cms_cert_lab_ind")	
	public String getCms_cert_lab_ind() {
		return cms_cert_lab_ind;
	}
	public void setCms_cert_lab_ind(String cmsCertLabInd) {
		cms_cert_lab_ind = cmsCertLabInd;
	}
	@XmlElement(name="fda_lic_mfg_inst_ind")	
	public String getFda_lic_mfg_inst_ind() {
		return fda_lic_mfg_inst_ind;
	}
	public void setFda_lic_mfg_inst_ind(String fdaLicMfgInstInd) {
		fda_lic_mfg_inst_ind = fdaLicMfgInstInd;
	}
	
}
