package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

public class UserSiteGroupPojo extends AuditablePojo {
	
	private Long pkUserSiteGroup;
	private Long fkGrpId;
	private Long fkUserSite;
	private Long fkUser;
	private Boolean deletedFlag;
	
	public Long getPkUserSiteGroup() {
		return pkUserSiteGroup;
	}
	public void setPkUserSiteGroup(Long pkUserSiteGroup) {
		this.pkUserSiteGroup = pkUserSiteGroup;
	}
	public Long getFkGrpId() {
		return fkGrpId;
	}
	public void setFkGrpId(Long fkGrpId) {
		this.fkGrpId = fkGrpId;
	}
	public Long getFkUserSite() {
		return fkUserSite;
	}
	public void setFkUserSite(Long fkUserSite) {
		this.fkUserSite = fkUserSite;
	}
	public Long getFkUser() {
		return fkUser;
	}
	public void setFkUser(Long fkUser) {
		this.fkUser = fkUser;
	}
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
}
