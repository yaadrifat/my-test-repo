package com.velos.ordercomponent.business.domain;


import javax.persistence.Column;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="CB_FUNDING_GUIDELINES")
@SequenceGenerator(sequenceName="SEQ_CB_FUNDING_GUIDELINES",name="SEQ_CB_FUNDING_GUIDELINES",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class FundingGuidelines  extends Auditable{
	
	private Long pkFundingGuidelin;
	private Long fkCbbId;
	private String fundingId;
	private Long fkFundCateg;
	private Date fundingBdate;
	private String description;
	private String fundingStatus;
	private Boolean deletedFlag;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_FUNDING_GUIDELINES")
	@Column(name="PK_FUNDING_GUIDELINE")
	public Long getPkFundingGuidelin() {
		return pkFundingGuidelin;
	}
	public void setPkFundingGuidelin(Long pkFundingGuidelin) {
		this.pkFundingGuidelin = pkFundingGuidelin;
	}
	
	@Column(name="FK_CBB_ID")
	public Long getFkCbbId() {
		return fkCbbId;
	}
	public void setFkCbbId(Long fkCbbId) {
		this.fkCbbId = fkCbbId;
	}
	
	@Column(name="FUNDING_ID")
	public String getFundingId() {
		return fundingId;
	}
	public void setFundingId(String fundingId) {
		this.fundingId = fundingId;
	}
	
	@Column(name="FK_FUND_CATEG")
	public Long getFkFundCateg() {
		return fkFundCateg;
	}
	public void setFkFundCateg(Long fkFundCateg) {
		this.fkFundCateg = fkFundCateg;
	}
	
	@Column(name="FUNDING_BDATE")
	public Date getFundingBdate() {
		return fundingBdate;
	}
	public void setFundingBdate(Date fundingBdate) {
		this.fundingBdate = fundingBdate;
	}
	
	@Column(name="DESCRIPTION")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(name="FUNDING_STATUS")
	public String getFundingStatus() {
		return fundingStatus;
	}
	public void setFundingStatus(String fundingStatus) {
		this.fundingStatus = fundingStatus;
	}
	
	@Column(name="DELETEDFLAG")
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	

}
