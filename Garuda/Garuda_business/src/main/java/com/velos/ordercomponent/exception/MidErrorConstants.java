/**
 * 
 */
package com.velos.ordercomponent.exception;


/**
 * @author akajeera
 *
 */
public class MidErrorConstants {
	public static final int GENERAL_ERR=100;

	public static final int SESSION_EXPIRED_ERR=GENERAL_ERR+1;

	public static final int SQL_ERR = GENERAL_ERR + 2;

	public static final int DB_RESOURCE_ERR = GENERAL_ERR + 3;

	public static final int APP_ERR_HIBERNATE = GENERAL_ERR + 4;
	
	public static final int REPORT_ERR_NOTFOUND = GENERAL_ERR + 5;
	
	public static final String GENERAL_ERRMSG_KEY = "GENERAL_ERR_MSG";
	public static final String SESSION_EXPIREDMSG_KEY = "SESSION_EXPIRED_MSG";
	public static final String REPORT_ERR_NOTFOUND_KEY = "REPORT_ERR_NOTFOUND_MSG";
	
	public static final String GARUDA_USER = "currentUser";
	
	public static final String DB_FUNCTION_LOOKUP_RESULTS = "F_GET_LOOKUPRESULTS";
	
}
