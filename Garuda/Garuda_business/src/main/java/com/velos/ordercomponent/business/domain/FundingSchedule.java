package com.velos.ordercomponent.business.domain;

import javax.persistence.Column;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="CB_FUNDING_SCHEDULE")
@SequenceGenerator(sequenceName="SEQ_CB_FUNDING_SCHEDULE",name="SEQ_CB_FUNDING_SCHEDULE",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class FundingSchedule extends Auditable{
	
	private Long pkFundingSchedule;
	private Long fkCbbId;
	private Date dateToGenerate;
	private Date cbbRegistrationDate;
	private Long notificationBefore;
	private String generatedStatus;
	private Boolean deletedFlag;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_FUNDING_SCHEDULE")
	@Column(name="PK_FUNDING_SCHEDULE")
	public Long getPkFundingSchedule() {
		return pkFundingSchedule;
	}
	public void setPkFundingSchedule(Long pkFundingSchedule) {
		this.pkFundingSchedule = pkFundingSchedule;
	}
	
	@Column(name="FK_CBB_ID")
	public Long getFkCbbId() {
		return fkCbbId;
	}
	public void setFkCbbId(Long fkCbbId) {
		this.fkCbbId = fkCbbId;
	}
	
	@Column(name="DATE_TO_GENERATE")
	public Date getDateToGenerate() {
		return dateToGenerate;
	}
	public void setDateToGenerate(Date dateToGenerate) {
		this.dateToGenerate = dateToGenerate;
	}
	
	@Column(name="CBB_REGISTRATION_DATE")
	public Date getCbbRegistrationDate() {
		return cbbRegistrationDate;
	}
	public void setCbbRegistrationDate(Date cbbRegistrationDate) {
		this.cbbRegistrationDate = cbbRegistrationDate;
	}
	
	@Column(name="NOTIFICATION_BEFORE")
	public Long getNotificationBefore() {
		return notificationBefore;
	}
	public void setNotificationBefore(Long notificationBefore) {
		this.notificationBefore = notificationBefore;
	}
	
	@Column(name="GENERATED_STATUS")
	public String getGeneratedStatus() {
		return generatedStatus;
	}
	public void setGeneratedStatus(String generatedStatus) {
		this.generatedStatus = generatedStatus;
	}
	
	@Column(name="DELETEDFLAG")
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	
}
