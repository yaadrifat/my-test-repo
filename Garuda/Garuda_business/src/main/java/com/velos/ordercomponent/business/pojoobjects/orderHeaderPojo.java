package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

public class orderHeaderPojo extends AuditablePojo{
	
	private Long pkOrderHeader;
	private String orderGuid;
	private String orderNumber;
	private Long orderEntityId;
	private Long orderEntityType;
	private Date orderOpendate;
	private Date orderCloseDate;
	private Long orderHeaderType;
	private Long orderHeaderStatus;
	private Date orderHeaderStatusDate;
	private String orderRemarks;
	private Long orderRequestedBy;
	private Long orderApprovedBy;
	
	
	
	
	public Long getPkOrderHeader() {
		return pkOrderHeader;
	}
	public void setPkOrderHeader(Long pkOrderHeader) {
		this.pkOrderHeader = pkOrderHeader;
	}
	public String getOrderGuid() {
		return orderGuid;
	}
	public void setOrderGuid(String orderGuid) {
		this.orderGuid = orderGuid;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public Long getOrderEntityId() {
		return orderEntityId;
	}
	public void setOrderEntityId(Long orderEntityId) {
		this.orderEntityId = orderEntityId;
	}
	public Long getOrderEntityType() {
		return orderEntityType;
	}
	public void setOrderEntityType(Long orderEntityType) {
		this.orderEntityType = orderEntityType;
	}
	public Date getOrderOpendate() {
		return orderOpendate;
	}
	public void setOrderOpendate(Date orderOpendate) {
		this.orderOpendate = orderOpendate;
	}
	public Date getOrderCloseDate() {
		return orderCloseDate;
	}
	public void setOrderCloseDate(Date orderCloseDate) {
		this.orderCloseDate = orderCloseDate;
	}
	public Long getOrderHeaderType() {
		return orderHeaderType;
	}
	public void setOrderHeaderType(Long orderHeaderType) {
		this.orderHeaderType = orderHeaderType;
	}
	public Long getOrderHeaderStatus() {
		return orderHeaderStatus;
	}
	public void setOrderHeaderStatus(Long orderHeaderStatus) {
		this.orderHeaderStatus = orderHeaderStatus;
	}
	public Date getOrderHeaderStatusDate() {
		return orderHeaderStatusDate;
	}
	public void setOrderHeaderStatusDate(Date orderHeaderStatusDate) {
		this.orderHeaderStatusDate = orderHeaderStatusDate;
	}
	public String getOrderRemarks() {
		return orderRemarks;
	}
	public void setOrderRemarks(String orderRemarks) {
		this.orderRemarks = orderRemarks;
	}
	public Long getOrderRequestedBy() {
		return orderRequestedBy;
	}
	public void setOrderRequestedBy(Long orderRequestedBy) {
		this.orderRequestedBy = orderRequestedBy;
	}
	public Long getOrderApprovedBy() {
		return orderApprovedBy;
	}
	public void setOrderApprovedBy(Long orderApprovedBy) {
		this.orderApprovedBy = orderApprovedBy;
	}
	
	
	

}
