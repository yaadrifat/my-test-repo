package com.velos.ordercomponent.business.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import com.nexoko.SignedUrl.Utils.URLSigning;
import com.opensymphony.xwork2.ActionContext;
import com.velos.eres.web.user.UserJB;
import com.velos.ordercomponent.business.domain.CBB;
import com.velos.ordercomponent.business.pojoobjects.AuditablePojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.VelosMidConstants;
import com.velos.ordercomponent.exception.MidErrorConstants;
import com.velos.ordercomponent.helper.CBBHelper;
import com.velos.ordercomponent.business.util.EresFormsBusiness;


public class VelosUtil {
	public static final Log log=LogFactory.getLog(VelosUtil.class);
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	HttpServletResponse response=(HttpServletResponse)ac.get(ServletActionContext.HTTP_RESPONSE);
	ResourceBundle resource = ResourceBundle.getBundle(VelosMidConstants.EXCEPTION_PROPERTYFILE_NAME);
	 
	 private static UserJB userObject = null;
	 
	 public static Object getValues(Object resultObj, String getMethod){
		 log.debug("VelosUtil : getValues Method Start ");
		 Object returnObj = null;
		 try{
			 Method[] remoteMethods = resultObj.getClass().getMethods();
			 for(int i=0;i<remoteMethods.length;i++){
					if(remoteMethods[i].getName().equals(getMethod)){
						try{
							//System.out.println(" inside method " + getMethod);
							returnObj = remoteMethods[i].invoke(resultObj, null);
						}catch(Exception e){
							//System.out.println(" Exception in eResearch method invoke " + e);
							e.printStackTrace();
						}
					}
				}
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 log.debug("VelosUtil : getValues Method End ");
		 return returnObj; 
		 
	 }
	 /*public Object setUserDetails(Object formObj) throws Exception{
		 	log.debug("VelosUtil : setUserDetails Method Start ");
			BaseObject baseObject = new BaseObject();
			Object returnObj = formObj;
			try{
				if(userObject != null){ 
					baseObject.setCreatedBy(userObject.getUserId());
					baseObject.setLastModifiedBy(userObject.getUserId());
				} 
				baseObject.setCreatedOn(Utilities.getCurrentDate());
				baseObject.setLastModifiedOn(Utilities.getCurrentDate());
				baseObject.setIpAddress(request.getRemoteAddr());
				baseObject.setDeletedFlag(0);
				 returnObj = ObjectTransfer.transferObjects(baseObject, formObj);
			}catch (Exception e){
				log.debug(" Exception in VelosUtil : setUserDetails() :: " + e);
				e.printStackTrace();
				errorHandling(e);
			}
			log.debug("VelosUtil : setUserDetails Method End ");
			return returnObj;
		}
	 
	 /*public Auditable setAuditableInfo() 
	 {
		 Auditable auditable = new Auditable();
		 userObject = getUserDetails();
		 if(userObject.getUserId()!=null)
		 {
			 auditable.setCreatedBy(userObject.getUserId());
			 auditable.setLastModifiedBy(userObject.getUserId());
		 }
		 auditable.setCreatedOn(new Date());
		 auditable.setLastModifiedOn(new Date());
		 auditable.setIpAddress(request.getRemoteAddr());
		 return auditable;
	 }*/
	 
	 public Object setAuditableInfo(Object formObj) 
	 {
		 log.debug("VelosUtil : setUserDetails Method Start ");
			AuditablePojo baseObject = new AuditablePojo();
			userObject = getUserDetails();
			Object returnObj = formObj;
					
			try{
				if(userObject != null){ 
					baseObject.setCreatedBy(((Integer)userObject.getUserId()).longValue());
					baseObject.setLastModifiedBy(((Integer)userObject.getUserId()).longValue());				} 
				baseObject.setCreatedOn(new Date());
				baseObject.setLastModifiedOn(new Date());
				baseObject.setIpAddress(request.getRemoteAddr());
				//System.out.println("util Ip address"+baseObject.getIpAddress());
				returnObj = ObjectTransfer.transferObjects(baseObject, formObj);
			}catch (Exception e){
				log.debug(" Exception in VelosUtil : setUserDetails() :: " + e);
				e.printStackTrace();
			}
			log.debug("VelosUtil : setUserDetails Method End ");
					
			return returnObj;
	 }
	 
	/* public static JSONObject constructJSON(HttpServletRequest request){
		 log.debug("VelosUtil : constructJSON Method Start ");
		 JSONObject valueObject = new JSONObject();
		 int index;
		 try{
			 Enumeration paramNames = request.getParameterNames();
			 while(paramNames.hasMoreElements()) {
				String paramName = (String)paramNames.nextElement();
				String[] paramValues = request.getParameterValues(paramName);
				//Check ParamName
				index = paramName.indexOf(".");
				if(index>0){
					paramName = paramName.substring(index+1);
				}
				valueObject.put(paramName,paramValues[0]);
			}
			 if(userObject != null){
				 valueObject.put(VelosP1VConstants.CREATEDBY, userObject.getUserId());
				 valueObject.put(VelosP1VConstants.LASTMODIFIEDBY, userObject.getUserId());
			 }
			 valueObject.put(VelosP1VConstants.CREATEDON, Utilities.getFormattedDate(Utilities.getCurrentDate()));
			 valueObject.put(VelosP1VConstants.LASTMODIFIEDON, Utilities.getFormattedDate(Utilities.getCurrentDate()));
			 valueObject.put(VelosP1VConstants.IPADDRESS, request.getRemoteAddr());
			 valueObject.put(VelosP1VConstants.DELETEDFLAG, "0");
			 //System.out.println("Ip Address : " + valueObject.get(VelosP1VConstants.IPADDRESS));
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 log.debug("VelosUtil : constructJSON Method End ");
		 return valueObject;
	 }
	 */
	 /*public String errorHandling(Exception e)throws Exception{
		 log.debug("VelosUtil : errorHandling Method Start ");
		 String forwardstr = "errorPage";
		 //Check Excepiton StackTrace is Enable
		String exceptionEnableKey = resource.getString(VelosP1VConstants.EXCEPTION_ENABLE_KEY);
		
		P1VBaseException baseException;
		if( e instanceof P1VBaseException){
			baseException = (P1VBaseException)e;
		}else{
			baseException  = new P1VBaseException(e.getMessage()); 
		}
		ErrorForm errorForm=new ErrorForm();
		errorForm.setExceptionEnable(exceptionEnableKey);
		ExceptionData exceptionData;
		 if( baseException instanceof P1VBaseException){
				exceptionData=((P1VBaseException)baseException).getExceptionData();
				 System.out.println("Inside  if " + exceptionData.getErrorCode());
				if(exceptionData.getErrorCode()==ErrorConstants.SESSION_EXPIRED_ERR){
					int errorCode=ErrorConstants.SESSION_EXPIRED_ERR;
					errorForm.setErrorcode(String.valueOf(errorCode));
					errorForm.setMessage(resource.getString(ErrorConstants.SESSION_EXPIREDMSG_KEY));
					request.setAttribute("sessionwarning",errorForm.getMessage());
					forwardstr = "loginPage";
				} else if(exceptionData.getErrorCode()== ErrorConstants.REPORT_ERR_NOTFOUND){
					int errorCode=ErrorConstants.REPORT_ERR_NOTFOUND;
					errorForm.setErrorcode(String.valueOf(errorCode));
					errorForm.setMessage(resource.getString(ErrorConstants.REPORT_ERR_NOTFOUND_KEY));
					P1VBaseException p1vbaseException=P1VBaseException.generateCommonException(baseException);
					errorForm.setException(p1vbaseException);
					request.setAttribute("errorForm", errorForm);
				} else{
					int errorcode=exceptionData.getErrorCode();
					errorForm.setErrorcode(String.valueOf(errorcode));
					errorForm.setMessage(resource.getString(ErrorConstants.GENERAL_ERRMSG_KEY));
					P1VBaseException p1vbaseException=P1VBaseException.generateCommonException(baseException);
					//System.out.println("Exception : ============== > " + errorForm.getException().getExceptionData().getErrorCode()) ;
					errorForm.setException(p1vbaseException);
					request.setAttribute("errorForm", errorForm);
				}
			}else if (baseException.getCause() instanceof P1VBaseException){
				P1VBaseException p1vBaseException = (P1VBaseException)baseException.getCause();
				
				if(p1vBaseException.getExceptionData() != null){
					errorForm.setMessage(getErrorString(p1vBaseException.getExceptionData().getErrorCode()));
				}
				errorForm.setException(p1vBaseException);
				request.setAttribute("errorForm", errorForm);
			}
		 log.debug("VelosUtil : errorHandling Method End ");
	 	return forwardstr;
	 }*/
	 /**
		 * Method returns the Error Message for the  Errorcode
		 * @param errorcode
		 * @return String
		 */


		public String getErrorString(int errorcode){
			String message;
			switch(errorcode){
			case MidErrorConstants.GENERAL_ERR:
				message=getMessageProperties(MidErrorConstants.GENERAL_ERRMSG_KEY);
				break;

			case MidErrorConstants.SESSION_EXPIRED_ERR:
				message = getMessageProperties(MidErrorConstants.SESSION_EXPIREDMSG_KEY);
				break;
			
			case MidErrorConstants.REPORT_ERR_NOTFOUND:
				message = getMessageProperties(MidErrorConstants.REPORT_ERR_NOTFOUND_KEY);
				break;

			default:
				message = getMessageProperties(MidErrorConstants.GENERAL_ERRMSG_KEY);

			}
			return message;
		}
		
		 public String getMessageProperties(String messageKey){
			return resource.getString(messageKey);			
		 }
		 public UserJB getUserDetails(){
			 log.debug("VelosUtil : getUserDetails Method Start ");
			HttpSession session = request.getSession(false);
			if(session != null){
				userObject=(UserJB)session.getAttribute(MidErrorConstants.GARUDA_USER);
			}
			log.debug("VelosUtil : getUserDetails Method End ");
			return userObject;	
		 }
		 
		 public Boolean isCdrUser(Long siteId){
			 Boolean flag = false;
			 try{
			 CBB cbb = CBBHelper.getCBBBySiteId(siteId);
				 if(cbb!=null){
					 if(cbb.getCdrUser()!=null && !cbb.getCdrUser().equals("")){
						 flag = cbb.getCdrUser();
					 }else
					 flag = false;
				 }		 	 
			 }catch(Exception e){
				 e.printStackTrace();
			 }
			 return flag;
		 }
		 public Boolean isReqElgQuest(Long siteId){
			 Boolean flag = false;
			 try{
			 CBB cbb = CBBHelper.getCBBBySiteId(siteId);
				 if(cbb!=null){
					 if(cbb.getCriReqElgQuest()!=null && !cbb.getCriReqElgQuest().equals("")){
						 flag = cbb.getCriReqElgQuest();
					 }else
					 flag = false;
				 }		 	 
			 }catch(Exception e){
				 e.printStackTrace();
			 }
			 return flag;
		 }
		 public Boolean isMember(Long siteId){
			 Boolean flag = false;
			 try{
			 CBB cbb = CBBHelper.getCBBBySiteId(siteId);
				 if(cbb!=null){
					 if(cbb.getAllowMember()!=null && cbb.getAllowMember()==1l){
						 flag=true;
					 }else
					 flag = false;
				 }		 	 
			 }catch(Exception e){
				 e.printStackTrace();
			 }
			 return flag;
		 }
		 
		 public String getAttachmentFromDcms(String docIdStr,String signed){
			 System.out.println("getAttachmentFromDcms"+docIdStr+signed);
				boolean isSignedRequest = false;
				String inputLine = "";
				String inputLine1 = "";
				 String ipAddress="";
				// PrintWriter out = null;
				 DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
		         DocumentBuilder docBuilder;
				try {
					docBuilder = docBuilderFactory.newDocumentBuilder();
					 try {
						@SuppressWarnings("deprecation")
						Document doc = docBuilder.parse (new File("dcmsAttachment.xml"));
						doc.getDocumentElement ().normalize ();
						NodeList dcms = doc.getElementsByTagName("dcms");

			            for(int s=0; s<dcms.getLength() ; s++){


			                Node dcmsNode = dcms.item(s);
			                if(dcmsNode.getNodeType() == Node.ELEMENT_NODE){


			                    Element dcmsElement = (Element)dcmsNode;

			                    //-------
			                    NodeList ipAddrList = dcmsElement.getElementsByTagName("dcmsIpAddress");
			                    Element ipAddrElement = (Element)ipAddrList.item(0);

			                    NodeList textFNList = ipAddrElement.getChildNodes();
			                    //System.out.println("dcmsIpAddress is: " +((Node)textFNList.item(0)).getNodeValue().trim());
			                    ipAddress=(String)((Node)textFNList.item(0)).getNodeValue().trim();

			                }//end of if clause


			            }//end of for loop with s var


					} catch (SAXException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} catch (ParserConfigurationException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		        
				 String initialUrl = "http://"+ipAddress+"/FileData-war/DcmsRetrieveServlet?";
		         try{
		         if(signed!= null){
		             isSignedRequest = signed.equals("true") ? true : false;
		             }
		             
		             String urlParameters = null;

		             if(isSignedRequest){
		                 urlParameters = getSignedURL(docIdStr);
		             }
		             URL url=new URL(initialUrl+urlParameters);
		             URLConnection huc= url.openConnection();
		             BufferedReader in = new BufferedReader(
                             new InputStreamReader(
                            		 huc.getInputStream()));
		             BufferedReader ino = new BufferedReader(
                             new InputStreamReader(
                            		 url.openStream()));
		             //System.out.println("attachmments data "+in);
		             //System.out.println("attachmments data content"+ino);
		             
				     while ((inputLine1=ino.readLine()) != null) 
				     {
				    	 inputLine += inputLine1;
				     //System.out.println(inputLine);
				     }
				     in.close();
		         }catch(Exception e){
		        	 e.printStackTrace();
		         }finally{
		        	// out.close();
		         }
		         System.out.println("string content"+inputLine);
				return inputLine;
			}
			
//			public static void main(String as[]){
//				getSignedURL("83");
//				
//			}

		private String getSignedURL(String docId){
			
			//System.out.println("document id is::"+docId);
			 String signKey = "84eb32ab8296465491e386afc102ebjk";
		        StringBuilder signedUrlParameters = new StringBuilder();
		        Map<String, String> urlParams = new TreeMap<String, String>();
		        Map<String, String> signedUrlParams = new TreeMap<String, String>();
		        //System.out.println("doc id sign key  -->"+signKey);
		        try{
		        urlParams.put("docId", docId);
		        urlParams.put("apiUser", "garuda");
		        try{
			       signedUrlParams.putAll(URLSigning.getSignedURLParameters(urlParams, signKey));
		        }catch(Exception e){
		        	  log.debug("getSignedURL : signedUrlParams Method error========"+e.getMessage());
		        	//System.out.println(e);
		        	//System.out.println("*********************************");
		        	e.printStackTrace();
		        }
		        //System.out.println("after getting signed url parameters");
		
		        for (Map.Entry<String, String> entry : signedUrlParams.entrySet()) {
		            signedUrlParameters.append(entry.getKey());
		            signedUrlParameters.append("=");
		            signedUrlParameters.append(entry.getValue());
		            signedUrlParameters.append("&");
		        }
		        //removing last & from urlParameters
		        signedUrlParameters.deleteCharAt(signedUrlParameters.lastIndexOf("&"));
		        }catch(Exception e){
		        	e.printStackTrace();
		        }
		        //System.out.println("before returning signed url");
		        return signedUrlParameters.toString();
			
		}
		
		public Map<Long,String> getAttachmentByFormAndFileId(Long[] ids,String attachmentType,Long entityId) throws Exception{
			System.out.println("getAttachmentByFormAndFileId"+ids.length+attachmentType+entityId);
			Map<Long,String> map = new HashMap<Long, String>();
			for(Long id : ids){
				if(attachmentType!=null && attachmentType!=""){
					if(attachmentType.equals(VelosMidConstants.FORM_TYPE) || attachmentType==VelosMidConstants.FORM_TYPE){
						if(entityId!=null){
							map.put(id, new EresFormsBusiness().getFormHtml(id.intValue(),entityId.toString(),false));
						}
					}else if(attachmentType.equals(VelosMidConstants.FILE_TYPE) || attachmentType==VelosMidConstants.FILE_TYPE){
                        if(entityId!=null){
                        	map.put(id, getAttachmentFromDcms(id.toString(),"true"));
						}
					}
				}
			}
			return map;			
		}
		
		public Boolean usingFdoe(Long siteId){
			Boolean flag = false;
			 try{
			 CBB cbb = CBBHelper.getCBBBySiteId(siteId);
				 if(cbb!=null){
					 if(cbb.getUseFdoe()!=null && !cbb.getUseFdoe().equals("")){
						 flag=cbb.getUseFdoe();
					 }else
					 flag = false;
				 }		 	 
			 }catch(Exception e){
				 e.printStackTrace();
			 }
			 return flag;
			
		}
		public Boolean usingAssessment(Long siteId){
			Boolean flag = false;
			 try{
			 CBB cbb = CBBHelper.getCBBBySiteId(siteId);
				 if(cbb!=null){
					 if(cbb.getCbuAssessment()!=null && !cbb.getCbuAssessment().equals("")){
						 flag=cbb.getCbuAssessment();
					 }else
					 flag = false;
				 }		 	 
			 }catch(Exception e){
				 e.printStackTrace();
			 }
			 return flag;
			
		}
		public Boolean stausUnitReport(Long siteId){
			Boolean flag = false;
			 try{
			 CBB cbb = CBBHelper.getCBBBySiteId(siteId);
				 if(cbb!=null){
					 if(cbb.getUnitReport()!=null && !cbb.getUnitReport().equals("")){
						 flag=cbb.getUnitReport();
					 }else
					 flag = false;
				 }		 	 
			 }catch(Exception e){
				 e.printStackTrace();
			 }
			 return flag;
			
		}
		
		public Boolean usingOwnDumn(Long siteId){
			Boolean flag = false;
			 try{
			 CBB cbb = CBBHelper.getCBBBySiteId(siteId);
				 if(cbb!=null){
					 if(cbb.getUseOwnDumn()!=null && !cbb.getUseOwnDumn().equals("")){
						 flag=cbb.getUseOwnDumn();
					 }else
					 flag = false;
				 }		 	 
			 }catch(Exception e){
				 e.printStackTrace();
			 }
			 return flag;
			
		}
		
		public Boolean usingNmdpDumn(Long siteId){
			Boolean flag = false;			 
			 return flag;			
		}
		
		public Boolean isCompleteCordEntry(Long siteId){
			Boolean flag = false;
			 try{
			 CBB cbb = CBBHelper.getCBBBySiteId(siteId);
				 if(cbb!=null){
					 if(cbb.getEnableAutoCompleteCordEntry()!=null && !cbb.getEnableAutoCompleteCordEntry().equals("")){
						 flag=cbb.getEnableAutoCompleteCordEntry();
					 }else
					 flag = false;
				 }		 	 
			 }catch(Exception e){
				 e.printStackTrace();
			 }
			 return flag;
		}
		
		public static PaginateSearch getPageNoShowRows(PaginateSearch paginateSearch){
			try{
				if(paginateSearch.getiShowRows()==0){
					paginateSearch.setiShowRows(5);
				}
				if(paginateSearch.getiPageNo()!=0){
					paginateSearch.setiPageNo(Math.abs((paginateSearch.getiPageNo()-1)*paginateSearch.getiShowRows()));
				}
				paginateSearch.setiTotalSearchRecords(10);
			}catch (Exception e) {
				//System.out.println(e);
				e.printStackTrace();
			}
			return paginateSearch;
		}
		// new added method getListTotalCount1
		public static PaginateSearch getListTotalCount1(PaginateSearch paginateSearch){
			try{
	            if(paginateSearch.getiTotalRows()<(paginateSearch.getiPageNo()+paginateSearch.getiShowRows()))
	            {
	                paginateSearch.setiEndResultNo(paginateSearch.getiTotalRows());
	            }
	            else
	            {  
	            	
	            	//paginateSearch.setiEndResultNo(paginateSearch.getiPageNo()+paginateSearch.getiShowRows());
	            }
	           
	            paginateSearch.setiStartResultNo(paginateSearch.getiPageNo()+1);
	            paginateSearch.setiTotalPages((int)(Math.ceil((double)paginateSearch.getiTotalRows()/paginateSearch.getiShowRows())));
			}catch (Exception e) {
				//System.out.println(e);
				e.printStackTrace();
			}
			return paginateSearch;
		}
		

		
		
		
		public static PaginateSearch getListTotalCount(PaginateSearch paginateSearch){
			try{
	            if(paginateSearch.getiTotalRows()<(paginateSearch.getiPageNo()+paginateSearch.getiShowRows()))
	            {
	                paginateSearch.setiEndResultNo(paginateSearch.getiTotalRows());
	            }
	            else
	            {  
	            	
	            	paginateSearch.setiEndResultNo(paginateSearch.getiPageNo()+paginateSearch.getiShowRows());
	            }
	           
	            paginateSearch.setiStartResultNo(paginateSearch.getiPageNo()+1);
	            paginateSearch.setiTotalPages((int)(Math.ceil((double)paginateSearch.getiTotalRows()/paginateSearch.getiShowRows())));
			}catch (Exception e) {
				//System.out.println(e);
				e.printStackTrace();
			}
			return paginateSearch;
		}
		
}