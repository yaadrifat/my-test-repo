/**
 * 
 */
package com.velos.ordercomponent.business.domain; 

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="ER_CODELST")
public class CodeList extends Auditable implements Cloneable{
	
	
	private Long pkCodeId;
	private String type;
	private String  subType;
	private String description;
	private String hide;
	private Long seq;
	private String codeListMaint;
	private String codeListCol;
	private String codeListCol1;
	private String role;
	
	/*private Auditable auditable;
*/
	//private CdrCbu cdrCbu;
/*
	private HLA hla;

	@OneToOne(fetch = FetchType.LAZY)
	public HLA getHla() {
		return hla;
	}

	public void setHla(HLA hla) {
		this.hla = hla;
	}*/
	/*
	@OneToOne(fetch = FetchType.LAZY)
	public CdrCbu getCdrCbu() {
		return cdrCbu;
	}

	public void setCdrCbu(CdrCbu cdrCbu) {
		this.cdrCbu = cdrCbu;
	}
*/
	@Id
	@GeneratedValue
	@Column(name="PK_CODELST")
	public Long getPkCodeId() {
		return pkCodeId;
	}
	public void setPkCodeId(Long pkCodeId) {
		this.pkCodeId = pkCodeId;
	}

	@Column(name="CODELST_TYPE")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name="CODELST_SUBTYP")
	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	@Column(name="CODELST_DESC")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name="CODELST_HIDE")
	public String getHide() {
		return hide;
	}

	public void setHide(String hide) {
		this.hide = hide;
	}

	@Column(name="CODELST_SEQ")
	public Long getSeq() {
		return seq;
	}
	public void setSeq(Long seq) {
		this.seq = seq;
	}
	@Column(name="CODELST_MAINT")
	public String getCodeListMaint() {
		return codeListMaint;
	}
	public void setCodeListMaint(String codeListMaint) {
		this.codeListMaint = codeListMaint;
	}
	@Column(name="CODELST_CUSTOM_COL")
	public String getCodeListCol() {
		return codeListCol;
	}
	public void setCodeListCol(String codeListCol) {
		this.codeListCol = codeListCol;
	}
	@Column(name="CODELST_CUSTOM_COL1")
	public String getCodeListCol1() {
		return codeListCol1;
	}
	public void setCodeListCol1(String codeListCol1) {
		this.codeListCol1 = codeListCol1;
	}
	@Column(name="CODELST_STUDY_ROLE")
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}	
	
	/*@Embedded
	public Auditable getAuditable() {
		return auditable;
	}
	public void setAuditable(Auditable auditable) {
		this.auditable = auditable;
	}*/
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
}
