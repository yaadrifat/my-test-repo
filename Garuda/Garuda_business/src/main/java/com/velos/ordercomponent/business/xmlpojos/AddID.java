package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;

public class AddID {

	private String IDType;
	private String IDValue;
	private String IDDesc;
	
	@XmlElement(name="IDType")
	public String getIDType() {
		return IDType;
	}
	public void setIDType(String iDType) {
		IDType = iDType;
	}
	
	@XmlElement(name="IDValue")
	public String getIDValue() {
		return IDValue;
	}
	public void setIDValue(String iDValue) {
		IDValue = iDValue;
	}
	
	@XmlElement(name="IDDesc")
	public String getIDDesc() {
		return IDDesc;
	}
	public void setIDDesc(String iDDesc) {
		IDDesc = iDDesc;
	}
	
	
}
