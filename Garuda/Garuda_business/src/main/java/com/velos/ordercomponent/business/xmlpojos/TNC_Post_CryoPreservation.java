package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.datatype.XMLGregorianCalendar;

public class TNC_Post_CryoPreservation {

	private String tnc_test;
	private String tnc_cnt;
	private XMLGregorianCalendar tnc_test_date;
	private String tnc_test_reason;
	private String tnc_test_reason_oth_describe;
	@XmlElement(name="tnc_test")
	public String getTnc_test() {
		return tnc_test;
	}
	public void setTnc_test(String tncTest) {
		tnc_test = tncTest;
	}
	@XmlElement(name="tnc_cnt")
	public String getTnc_cnt() {
		return tnc_cnt;
	}
	public void setTnc_cnt(String tncCnt) {
		tnc_cnt = tncCnt;
	}
	@XmlElement(name="tnc_test_date")
	public XMLGregorianCalendar getTnc_test_date() {
		return tnc_test_date;
	}
	public void setTnc_test_date(XMLGregorianCalendar tncTestDate) {
		tnc_test_date = tncTestDate;
	}
	@XmlElement(name="tnc_test_reason")
	public String getTnc_test_reason() {
		return tnc_test_reason;
	}
	public void setTnc_test_reason(String tncTestReason) {
		tnc_test_reason = tncTestReason;
	}
	@XmlElement(name="tnc_test_reason_oth_describe")
	public String getTnc_test_reason_oth_describe() {
		return tnc_test_reason_oth_describe;
	}
	public void setTnc_test_reason_oth_describe(String tncTestReasonOthDescribe) {
		tnc_test_reason_oth_describe = tncTestReasonOthDescribe;
	}
	
	
}
