package com.velos.ordercomponent.helper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;

import com.velos.ordercomponent.business.domain.Specimen;
import com.velos.ordercomponent.business.pojoobjects.SpecimenPojo;
import com.velos.ordercomponent.business.util.HibernateUtil;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.VelosUtil;

/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class SpecimenHelper {
	public static final Log log=LogFactory.getLog(SpecimenHelper.class);
	
	public static Specimen saveOrUpdateSpecimen(Specimen specimenPojo,Boolean fromUI){
		return saveOrUpdateSpecimen(specimenPojo, null,fromUI);
	}
	
    public static Specimen saveOrUpdateSpecimen(Specimen specimenPojo,Session session,Boolean fromUI){
    	Specimen specimen = new Specimen();
    	//log.debug("inside the saveOrUpdateSpecimen"+specimenPojo.getSpecCollDate());
    	boolean sessionFlag = false;		
		try{
			if(session== null){
				// create new session
			  session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
		if(fromUI)
		specimenPojo = (Specimen)new VelosUtil().setAuditableInfo(specimenPojo);	    	
		if(specimenPojo.getPkSpecimen()!=null){
			session.merge(specimenPojo);
		}else{
			session.save(specimenPojo);
		}	
		if(sessionFlag){
			// close session, if session created in this method
			session.getTransaction().commit();
		}		
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}			
		return specimenPojo;		
	}
}
