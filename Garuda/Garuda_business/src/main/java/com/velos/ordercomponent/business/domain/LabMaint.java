package com.velos.ordercomponent.business.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
/**
 *@author Jyoti
 *
 */
@Entity
@Table(name="VELOS_LAB_MAINT")
@SequenceGenerator(sequenceName="SQ_VELOS_LAB_MAINT",name="SQ_VELOS_LAB_MAINT",allocationSize=1)

public class LabMaint extends Auditable {
	private Long pk_LabTestId;
	private Long fk_CategroyId;
	private String labTestName; 
	private String normalRangefrom; 
	private String normalRangeTo; 
	private String cptCode; 
	private String testUnit;
	private Boolean deletedFlag;
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SQ_VELOS_LAB_MAINT")
	@Column(name="PK_LAB_TESTID")
	public Long getPk_LabTestId() {
		return pk_LabTestId;
	}
	public void setPk_LabTestId(Long pk_LabTestId) {
		this.pk_LabTestId = pk_LabTestId;
	}
	@Column(name="FK_CATEGORYID")
	public Long getFk_CategroyId() {
		return fk_CategroyId;
	}
	public void setFk_CategroyId(Long fk_CategroyId) {
		this.fk_CategroyId = fk_CategroyId;
	}
	@Column(name="LAB_TEST_NAME")
	public String getLabTestName() {
		return labTestName;
	}
	public void setLabTestName(String labTestName) {
		this.labTestName = labTestName;
	}
	@Column(name="NORMAL_RANGE_FROM")
	public String getNormalRangefrom() {
		return normalRangefrom;
	}
	public void setNormalRangefrom(String normalRangefrom) {
		this.normalRangefrom = normalRangefrom;
	}
	@Column(name="NORMAL_RANGE_TO")
	public String getNormalRangeTo() {
		return normalRangeTo;
	}
	public void setNormalRangeTo(String normalRangeTo) {
		this.normalRangeTo = normalRangeTo;
	}
	@Column(name="CPT_CODE")
	public String getCptCode() {
		return cptCode;
	}
	public void setCptCode(String cptCode) {
		this.cptCode = cptCode;
	}
	@Column(name="TEST_UNIT")
	public String getTestUnit() {
		return testUnit;
	}
	public void setTestUnit(String testUnit) {
		this.testUnit = testUnit;
	}
	
	@Column(name="DELETEDFLAG")
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	
}
