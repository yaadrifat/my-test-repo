package com.velos.ordercomponent.report;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.jdbc.Work;

import com.velos.ordercomponent.business.pojoobjects.CordFilterPojo;

public class CordID implements Work{
	public static final Log log = LogFactory.getLog(CordID.class);
	
	private String query;
	
	private List<CordFilterPojo> cordIds;	
	
	public CordID(String query) {
		this.query = query;
	}
	 @Override
	    public void execute( Connection connection ) throws SQLException
	    {
	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        try
	        {
	            ps = connection.prepareStatement( query);
	            ps.setFetchSize(50000);
	            rs = ps.executeQuery();
	            long startTime=System.currentTimeMillis();
	            
		           
	            cordIds = prepCordIdsObj(rs);
		           
	            
		            
		            
	             long endTime=System.currentTimeMillis();
		         System.out.println();
		         System.out.println((endTime-startTime)/1000+" : "+(endTime-startTime)%1000+" MM:SS\n\n");
	        }
	        
	        
	        catch( SQLException e ) { 
	        	
	        	printException(e);
	        } catch (ParseException e) {
	        	
	        	printException(e);
			}
	        finally  {
	            if( rs != null )  
	            {
	                try {
	                    rs.close();
	                }
	                catch( Exception e ) {printException(e);}
	            }
	            if( ps != null ) 
	            {
	                try {
	                    ps.close();
	                }
	                catch( Exception e ) {printException(e);}
	            }
	        }
	    }
	
	 private List<CordFilterPojo> prepCordIdsObj(ResultSet rs) throws SQLException, ParseException{
		 
		 List<CordFilterPojo> cordIds = new ArrayList<CordFilterPojo>();
		 CordFilterPojo cfp = null;
		 
		  while( rs.next())  
          {
			  cfp = new CordFilterPojo();
			  
			  cfp.cord =  rs.getLong(1);
			  cfp.site =  rs.getString(2);
			  cfp.registry =  rs.getString(3);
			  cfp.localCbu =  rs.getString(4);
			  cfp.numberOnBag =  rs.getString(5);
			  cfp.isbtCode =  rs.getString(6);
			  cfp.historic =  rs.getString(7);
			  cfp.maternal =  rs.getString(8);
			  cfp.localMat =  rs.getString(9);
			  
          	
			  cordIds.add(cfp);
          }
		 return cordIds;
	 }
	private void printException(Exception e){
		
		log.error(e.getMessage());
		e.printStackTrace();
		
	}
	
	/**
	 * @return the cordIds
	 */
	public List<CordFilterPojo> getCordIds() {
		return cordIds;
	}
	
	
	

}
