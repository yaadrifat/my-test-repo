package com.velos.ordercomponent.business.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name="CB_CORD")
@SequenceGenerator(sequenceName="SEQ_CB_CORD",name="SEQ_CB_CORD",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class CdrCbu extends Auditable {
	
	private Long cordID;
	private String cdrCbuId;
	private String externalCbuId;
	private Long tncFrozen;
	private Long cellCount;
	private Long cbuVolFrzn;
	private Long frozenPatWt;
	private Date birthDate;
	private String registryId;
	private String localCbuId;
	private String historicLocalCbuId;
	private String numberOnCbuBag;
	private Date creationDate;
	private String cdrCbuCreatedBy;
	private Date cdrCbuLstModDate;
	private String cdrCbuLstModBy;
	private Long bacterialResult;
	private Long fungalResult;
	private Long aboBloodType;	
	private Long rhType;
	private Long babyGenderId;
	private Long clinicalStatus;
	private Long cbuLicStatus;
	private String addInfoReq;
	private Boolean deletedFlag;	
	private Long fkCbbId;	
	private String cordIsbiDinCode;
	private String cordIsitProductCode;
	private String cordDonorIdNumber;
	private Long fkCordCbuEligible;
	private Long fkCordCbuStatus;
	private Long cordnmdpstatus;
	private Date cordavaildate;
	private String cord_Add_info;
	private Long fkCordCbuEligibleReason;
	private Long fkCordCbuUnlicenseReason;		
	private Long fkSpNMDPId;
	private Long fkSpOtherId;
	private Long fkNmdpNameId;
	private Long fkNmdpNumId;
	private String spOtherName;
	private String spOtherNum;
	private String clinicalStatReason;
	private Date clinicalStatDate;
	private String registryMaternalId;
	private String localMaternalId;
	private String ethnicity;
	private String race;
	private Boolean fundingRequested;
	private Boolean fundedCBU;
	private Long fkFundCateg;
//	private Long fkCordAdditionalIds;
	private List<AdditionalIds> additionalIds;
	private Site site;
	private Long fkSpecimenId;
	private Specimen specimen;
	private Long cordEntryProgress;
	private Long cordSearchable;
	/*private Auditable auditable;*/
	//private String cordUnavailReason;
	private String nmdpCbuId;
	private String nmdpMaternalId;
	//private String iscordavailfornmdp;
	private Long unavailReason;
	private Long hemoglobinResult;
	//private CBUFinalReview cbuFinalReview;
	//private String cordAvailConfirmFlag;
	//private String additiTypingFlag;
	private Long nuclcellcntfrzuncorrect;
	private Date processingDate;
	private Date procStartDate;
	private Date procTermiDate;
	private String procVersion;
	private Long fkCbbProcedure;
	private Long fkCBUStorageLocation;
	private Long fkCBUCollectionType;
	private Long fkCBUDeliveryType;
	private Long fkCBUCollectionSite;
	private Long fkMultipleBirth;
	private Long nmdpRaceId;
	private Long hrsaRaceRollUp;
	private String guid;
	private String productCode;
	
	private Date  frzDate;
	private Long hemoglobinScrn;
	private String bactComment;
	private String fungComment;
	//private Date cordUnavailConfirmDate;
	
	private Long idmcmsapprovedlab;
	private Long idmfdalicensedlab;
	
	private Date fungCultDate;
	private Date bactCultDate;
	/*private String cbuCollectionTime;
	private String babyBirthTime;*/
	private String cordIdsChangeReason;
	private String specRhTypeOther;
	private Long pregRefNumber;
	//private String processingTime;
	//private String freezeTime;
	private String matGuid;
	private String isSystemCord;
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_CORD")
	@Column(name="PK_CORD")
	public Long getCordID() {
		return cordID;
	}
	public void setCordID(Long cordID) {
		this.cordID = cordID;
	}
	
	
	
	@Column(name="FK_UNAVAIL_RSN")
	public Long getUnavailReason() {
		return unavailReason;
	}
	public void setUnavailReason(Long unavailReason) {
		this.unavailReason = unavailReason;
	}
	@Column(name="CORD_CDR_CBU_ID")
	public String getCdrCbuId() {
		return cdrCbuId;
	}
	public void setCdrCbuId(String cdrCbuId) {
		this.cdrCbuId = cdrCbuId;
	}
	
	
	@Column(name="CORD_EXTERNAL_CBU_ID")
	public String getExternalCbuId() {
		return externalCbuId;
	}
	public void setExternalCbuId(String externalCbuId) {
		this.externalCbuId = externalCbuId;
	}
	
	@Column(name="CORD_TNC_FROZEN")
	public Long getTncFrozen() {
		return tncFrozen;
	}
	public void setTncFrozen(Long tncFrozen) {
		this.tncFrozen = tncFrozen;
	}
	@Column(name="CORD_CD34_CELL_COUNT")
	public Long getCellCount() {
		return cellCount;
	}
	public void setCellCount(Long cellCount) {
		this.cellCount = cellCount;
	}
	
	@Column(name="CORD_TNC_FROZEN_PAT_WT")
	public Long getFrozenPatWt() {
		return frozenPatWt;
	}
	public void setFrozenPatWt(Long frozenPatWt) {
		this.frozenPatWt = frozenPatWt;
	}
	
	@Column(name="CORD_BABY_BIRTH_DATE")
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	
	@Column(name="CORD_REGISTRY_ID")
	public String getRegistryId() {
		return registryId;
	}
	public void setRegistryId(String registryId) {
		this.registryId = registryId;
	}
	
	@Column(name="CORD_LOCAL_CBU_ID")
	public String getLocalCbuId() {
		return localCbuId;
	}
	public void setLocalCbuId(String localCbuId) {
		this.localCbuId = localCbuId;
	}
	
	@Column(name="CORD_HISTORIC_CBU_ID")
	public String getHistoricLocalCbuId() {
		return historicLocalCbuId;
	}
	public void setHistoricLocalCbuId(String historicLocalCbuId) {
		this.historicLocalCbuId = historicLocalCbuId;
	}
	
	@Column(name="CORD_ID_NUMBER_ON_CBU_BAG")
	public String getNumberOnCbuBag() {
		return numberOnCbuBag;
	}
	public void setNumberOnCbuBag(String numberOnCbuBag) {
		this.numberOnCbuBag = numberOnCbuBag;
	}
	
	@Column(name="CORD_CREATION_DATE",insertable=true,updatable=false)
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	@Column(name="CORD_CDR_CBU_CREATED_BY")
	public String getCdrCbuCreatedBy() {
		return cdrCbuCreatedBy;
	}
	public void setCdrCbuCreatedBy(String cdrCbuCreatedBy) {
		this.cdrCbuCreatedBy = cdrCbuCreatedBy;
	}
	
	@Column(name="CORD_CDR_CBU_LAST_MOD_DT")
	public Date getCdrCbuLstModDate() {
		return cdrCbuLstModDate;
	}
	public void setCdrCbuLstModDate(Date cdrCbuLstModDate) {
		this.cdrCbuLstModDate = cdrCbuLstModDate;
	}
	
	@Column(name="CORD_CDR_CBU_LAST_MOD_BY")
	public String getCdrCbuLstModBy() {
		return cdrCbuLstModBy;
	}
	public void setCdrCbuLstModBy(String cdrCbuLstModBy) {
		this.cdrCbuLstModBy = cdrCbuLstModBy;
	}
	
	@Column(name="FK_CORD_BACT_CUL_RESULT")
	public Long getBacterialResult() {
		return bacterialResult;
	}
	public void setBacterialResult(Long bacterialResult) {
		this.bacterialResult = bacterialResult;
	}
	
	@Column(name="FK_CORD_FUNGAL_CUL_RESULT")
	public Long getFungalResult() {
		return fungalResult;
	}
	public void setFungalResult(Long fungalResult) {
		this.fungalResult = fungalResult;
	}
	
	@Column(name="FK_CORD_ABO_BLOOD_TYPE")
	public Long getAboBloodType() {
		return aboBloodType;
	}
	public void setAboBloodType(Long aboBloodType) {
		this.aboBloodType = aboBloodType;
	}
	
	@Column(name="FK_CORD_RH_TYPE")
	public Long getRhType() {
		return rhType;
	}
	public void setRhType(Long rhType) {
		this.rhType = rhType;
	}
	
	
	@Column(name="FK_CORD_BABY_GENDER_ID")
	public Long getBabyGenderId() {
		return babyGenderId;
	}
	
	public void setBabyGenderId(Long babyGenderId) {
		this.babyGenderId = babyGenderId;
	}
	
	@Column(name="FK_CORD_CLINICAL_STATUS")
	public Long getClinicalStatus() {
		return clinicalStatus;
	}
	public void setClinicalStatus(Long clinicalStatus) {
		this.clinicalStatus = clinicalStatus;
	}
	
	@Column(name="FK_CORD_CBU_LIC_STATUS")
	public Long getCbuLicStatus() {
		return cbuLicStatus;
	}
	public void setCbuLicStatus(Long cbuLicStatus) {
		this.cbuLicStatus = cbuLicStatus;
	}
	
	
	@Column(name="ADDITIONAL_INFO_RECQUIRED")
	public String getAddInfoReq() {
		return addInfoReq;
	}
	public void setAddInfoReq(String addInfoReq) {
		this.addInfoReq = addInfoReq;
	}
	
	/*@Embedded
	public Auditable getAuditable() {
		return auditable;
	}
	public void setAuditable(Auditable auditable) {
		this.auditable = auditable;
	}*/
	
	@Column(name="deletedflag")
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	

	@Column(name="CORD_ELIGIBLE_ADDITIONAL_INFO")
	public String getCord_Add_info() {
		return cord_Add_info;
	}
	public void setCord_Add_info(String cord_Add_info) {
		this.cord_Add_info = cord_Add_info;
	}

	@Column(name="FK_CBB_ID")
	public Long getFkCbbId() {
		return fkCbbId;
	}
	public void setFkCbbId(Long fkCbbId) {
		this.fkCbbId = fkCbbId;
	}
	@Column(name="CORD_ISBI_DIN_CODE")
	public String getCordIsbiDinCode() {
		return cordIsbiDinCode;
	}
	public void setCordIsbiDinCode(String cordIsbiDinCode) {
		this.cordIsbiDinCode = cordIsbiDinCode;
	}
	@Column(name="CORD_ISIT_PRODUCT_CODE")
	public String getCordIsitProductCode() {
		return cordIsitProductCode;
	}
	public void setCordIsitProductCode(String cordIsitProductCode) {
		this.cordIsitProductCode = cordIsitProductCode;
	}
	@Column(name="FK_CORD_CBU_ELIGIBLE_STATUS")
	public Long getFkCordCbuEligible() {
		return fkCordCbuEligible;
	}
	public void setFkCordCbuEligible(Long fkCordCbuEligible) {
		this.fkCordCbuEligible = fkCordCbuEligible;
	}
	@Column(name="FK_CORD_CBU_STATUS")
	public Long getFkCordCbuStatus() {
		return fkCordCbuStatus;
	}
	public void setFkCordCbuStatus(Long fkCordCbuStatus) {
		this.fkCordCbuStatus = fkCordCbuStatus;
	}
	@Column(name="CORD_NMDP_STATUS")
	public Long getCordnmdpstatus() {
		return cordnmdpstatus;
	}
	public void setCordnmdpstatus(Long cordnmdpstatus) {
		this.cordnmdpstatus = cordnmdpstatus;
	}
	@Column(name="CORD_AVAIL_DATE")
	public Date getCordavaildate() {
		return cordavaildate;
	}
	public void setCordavaildate(Date cordavaildate) {
		this.cordavaildate = cordavaildate;
	}
	
	@Column(name="FK_CORD_CBU_INELIGIBLE_REASON")
	public Long getFkCordCbuEligibleReason() {
		return fkCordCbuEligibleReason;
	}
	public void setFkCordCbuEligibleReason(Long fkCordCbuEligibleReason) {
		this.fkCordCbuEligibleReason = fkCordCbuEligibleReason;
	}
	
	@Column(name="FK_CORD_CBU_UNLICENSED_REASON")
	public Long getFkCordCbuUnlicenseReason() {
		return fkCordCbuUnlicenseReason;
	}
	public void setFkCordCbuUnlicenseReason(Long fkCordCbuUnlicenseReason) {
		this.fkCordCbuUnlicenseReason = fkCordCbuUnlicenseReason;
	}
	
	@Column(name="FK_CODELST_EXTERNAL")
	public Long getFkSpNMDPId() {
		return fkSpNMDPId;
	}
	public void setFkSpNMDPId(Long fkSpNMDPId) {
		this.fkSpNMDPId = fkSpNMDPId;
	}
	
	@Column(name="FK_CODE_OTHER")
	public Long getFkSpOtherId() {
		return fkSpOtherId;
	}
	public void setFkSpOtherId(Long fkSpOtherId) {
		this.fkSpOtherId = fkSpOtherId;
	}
	
	@Column(name="FK_CODELST_EXTERNAL_NAME")
	public Long getFkNmdpNameId() {
		return fkNmdpNameId;
	}
	public void setFkNmdpNameId(Long fkNmdpNameId) {
		this.fkNmdpNameId = fkNmdpNameId;
	}
	
	@Column(name="FK_CODELST_EXTERNAL_NUM")
	public Long getFkNmdpNumId() {
		return fkNmdpNumId;
	}
	public void setFkNmdpNumId(Long fkNmdpNumId) {
		this.fkNmdpNumId = fkNmdpNumId;
	}
	
	@Column(name="IND_OTHER_NAME")
	public String getSpOtherName() {
		return spOtherName;
	}
	public void setSpOtherName(String spOtherName) {
		this.spOtherName = spOtherName;
	}
	
	@Column(name="IND_OTHER_NUM")
	public String getSpOtherNum() {
		return spOtherNum;
	}
	public void setSpOtherNum(String spOtherNum) {
		this.spOtherNum = spOtherNum;
	}
	@Column(name="CORD_CLINICAL_STATUS_ADDI_INFO")
	public String getClinicalStatReason() {
		return clinicalStatReason;
	}
	public void setClinicalStatReason(String clinicalStatReason) {
		this.clinicalStatReason = clinicalStatReason;
	}
	@Column(name="CORD_CLINICAL_STATUS_DATE")
	public Date getClinicalStatDate() {
		return clinicalStatDate;
	}
	public void setClinicalStatDate(Date clinicalStatDate) {
		this.clinicalStatDate = clinicalStatDate;
	}
	@Column(name="registry_maternal_id")
	public String getRegistryMaternalId() {
		return registryMaternalId;
	}
	public void setRegistryMaternalId(String registryMaternalId) {
		this.registryMaternalId = registryMaternalId;
	}
	@Column(name="maternal_local_id")
	public String getLocalMaternalId() {
		return localMaternalId;
	}
	public void setLocalMaternalId(String localMaternalId) {
		this.localMaternalId = localMaternalId;
	}
	@Column(name="FK_CODELST_ETHNICITY")
	public String getEthnicity() {
		return ethnicity;
	}
	public void setEthnicity(String ethnicity) {
		this.ethnicity = ethnicity;
	}
	@Column(name="FK_CODELST_RACE")
	public String getRace() {
		return race;
	}
	public void setRace(String race) {
		this.race = race;
	}
	@Column(name="funding_requested")
	public Boolean getFundingRequested() {
		return fundingRequested;
	}
	public void setFundingRequested(Boolean fundingRequested) {
		this.fundingRequested = fundingRequested;
	}
	@Column(name="funded_cbu")
	public Boolean getFundedCBU() {
		return fundedCBU;
	}
	public void setFundedCBU(Boolean fundedCBU) {
		this.fundedCBU = fundedCBU;
	}
	@Column(name="FK_FUND_CATEG")
	public Long getFkFundCateg() {
		return fkFundCateg;
	}
	public void setFkFundCateg(Long fkFundCateg) {
		this.fkFundCateg = fkFundCateg;
	}
	/*@Column(name="FK_CORD_ADDITIONAL_IDS")
	public Long getFkCordAdditionalIds() {
		return fkCordAdditionalIds;
	}
	public void setFkCordAdditionalIds(Long fkCordAdditionalIds) {
		this.fkCordAdditionalIds = fkCordAdditionalIds;
	}*/
	/*@OneToMany(cascade=CascadeType.ALL,mappedBy="CB_CORD",fetch=FetchType.EAGER)
	public List<AdditionalIds> getAdditionalIds() {
		return additionalIds;
	}
	public void setAdditionalIds(List<AdditionalIds> additionalIds) {
		this.additionalIds = additionalIds;
	}*/
	
	@OneToOne
	@JoinColumn(name="FK_CBB_ID",nullable=false,insertable=false,updatable=false)
	@NotFound(action=NotFoundAction.IGNORE)
	public Site getSite() {
		return site;
	}
	public void setSite(Site site) {
		this.site = site;
	}
	
	
	@Column(name="FK_SPECIMEN_ID")
	public Long getFkSpecimenId() {
		return fkSpecimenId;
	}
	public void setFkSpecimenId(Long fkSpecimenId) {
		this.fkSpecimenId = fkSpecimenId;
	}
	
	@OneToOne
	@JoinColumn(name="FK_SPECIMEN_ID",nullable=false,insertable=false,updatable=false)
	@NotFound(action=NotFoundAction.IGNORE)
	public Specimen getSpecimen() {
		return specimen;
	}
	public void setSpecimen(Specimen specimen) {
		this.specimen = specimen;
	}
	
	@Column(name="CORD_ENTRY_PROGRESS")
	public Long getCordEntryProgress() {
		return cordEntryProgress;
	}
	public void setCordEntryProgress(Long cordEntryProgress) {
		this.cordEntryProgress = cordEntryProgress;
	}
	@Column(name="CORD_SEARCHABLE")
	public Long getCordSearchable() {
		return cordSearchable;
	}
	public void setCordSearchable(Long cordSearchable) {
		this.cordSearchable = cordSearchable;
	}
	@Column(name="cord_donor_identi_num")
	public String getCordDonorIdNumber() {
		return cordDonorIdNumber;
	}
	public void setCordDonorIdNumber(String cordDonorIdNumber) {
		this.cordDonorIdNumber = cordDonorIdNumber;
	}
	
	@Column(name="CORD_NMDP_CBU_ID")
	public String getNmdpCbuId() {
		return nmdpCbuId;
	}
	public void setNmdpCbuId(String nmdpCbuId) {
		this.nmdpCbuId = nmdpCbuId;
	}
	
	@Column(name="CORD_NMDP_MATERNAL_ID")
	public String getNmdpMaternalId() {
		return nmdpMaternalId;
	}	
	public void setNmdpMaternalId(String nmdpMaternalId) {
		this.nmdpMaternalId = nmdpMaternalId;
	}
	
	/*@Column(name="IS_CORD_AVAIL_FOR_NMDP")
	public String getIscordavailfornmdp() {
		return iscordavailfornmdp;
	}
	public void setIscordavailfornmdp(String iscordavailfornmdp) {
		this.iscordavailfornmdp = iscordavailfornmdp;
	}*/

    @Column(name="PRCSNG_START_DATE")
	
	public Date getProcessingDate() {
		return processingDate;
	}
	public void setProcessingDate(Date processingDate) {
		this.processingDate = processingDate;
	}
	
	
	@Column(name="FRZ_DATE")
	public Date getFrzDate() {
		return frzDate;
	}
	public void setFrzDate(Date frzDate) {
		this.frzDate = frzDate;
	}
	
	
	
	@Column(name="HEMOGLOBIN_SCRN")
	
	public Long getHemoglobinScrn() {
		return hemoglobinScrn;
	}
	public void setHemoglobinScrn(Long hemoglobinScrn) {
		this.hemoglobinScrn = hemoglobinScrn;
	}
	
	
	/*
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="PK_CORD",nullable=false,insertable=false,updatable=false)
	@NotFound(action=NotFoundAction.IGNORE)
	public CBUFinalReview getCbuFinalReview() {
		return cbuFinalReview;
	}
	public void setCbuFinalReview(CBUFinalReview cbuFinalReview) {
		this.cbuFinalReview = cbuFinalReview;
	}
	*/
	/*@Column(name="ADDITI_TYPING_FLAG")
	public String getAdditiTypingFlag() {
		return additiTypingFlag;
	}
	public void setAdditiTypingFlag(String additiTypingFlag) {
		this.additiTypingFlag = additiTypingFlag;
	}*/
	@Column(name="CBB_PROCEDURE_START_DATE")
	public Date getProcStartDate() {
		return procStartDate;
	}
	public void setProcStartDate(Date procStartDate) {
		this.procStartDate = procStartDate;
	}
	@Column(name="CBB_PROCEDURE_END_DATE")
	public Date getProcTermiDate() {
		return procTermiDate;
	}
	public void setProcTermiDate(Date procTermiDate) {
		this.procTermiDate = procTermiDate;
	}
	@Column(name="CBB_PROCEDURE_VERSION")
	public String getProcVersion() {
		return procVersion;
	}
	public void setProcVersion(String procVersion) {
		this.procVersion = procVersion;
	}
	@Column(name="FK_CBB_PROCEDURE")
	public Long getFkCbbProcedure() {
		return fkCbbProcedure;
	}
	public void setFkCbbProcedure(Long fkCbbProcedure) {
		this.fkCbbProcedure = fkCbbProcedure;
	}
	
	@Column(name="BACT_COMMENT")
	
	public String getBactComment() {
		return bactComment;
	}
	public void setBactComment(String bactComment) {
		this.bactComment = bactComment;
	}
	
	@Column(name="FUNG_COMMENT")
	
	public String getFungComment() {
		return fungComment;
	}
	public void setFungComment(String fungComment) {
		this.fungComment = fungComment;
	}
	/*@Column(name="CBU_AVAIL_CONFIRM_FLAG")
	public String getCordAvailConfirmFlag() {
		return cordAvailConfirmFlag;
	}
	public void setCordAvailConfirmFlag(String cordAvailConfirmFlag) {
		this.cordAvailConfirmFlag = cordAvailConfirmFlag;
	}*/
	@Column(name="FK_CBU_STOR_LOC")
	public Long getFkCBUStorageLocation() {
		return fkCBUStorageLocation;
	}
	public void setFkCBUStorageLocation(Long fkCBUStorageLocation) {
		this.fkCBUStorageLocation = fkCBUStorageLocation;
	}
	@Column(name="FK_CBU_COLL_TYPE")
	public Long getFkCBUCollectionType() {
		return fkCBUCollectionType;
	}
	public void setFkCBUCollectionType(Long fkCBUCollectionType) {
		this.fkCBUCollectionType = fkCBUCollectionType;
	}
	@Column(name="FK_CBU_DEL_TYPE")
	public Long getFkCBUDeliveryType() {
		return fkCBUDeliveryType;
	}
	public void setFkCBUDeliveryType(Long fkCBUDeliveryType) {
		this.fkCBUDeliveryType = fkCBUDeliveryType;
	}
	@Column(name="FK_CBU_COLL_SITE")
	public Long getFkCBUCollectionSite() {
		return fkCBUCollectionSite;
	}
	public void setFkCBUCollectionSite(Long fkCBUCollectionSite) {
		this.fkCBUCollectionSite = fkCBUCollectionSite;
	}
	@Column(name="MULTIPLE_BIRTH")
	public Long getFkMultipleBirth() {
		return fkMultipleBirth;
	}
	public void setFkMultipleBirth(Long fkMultipleBirth) {
		this.fkMultipleBirth = fkMultipleBirth;
	}
	@Column(name="NMDP_RACE_ID")
	public Long getNmdpRaceId() {
		return nmdpRaceId;
	}
	public void setNmdpRaceId(Long nmdpRaceId) {
		this.nmdpRaceId = nmdpRaceId;
	}
	@Column(name="HRSA_RACE_ROLLUP")
	public Long getHrsaRaceRollUp() {
		return hrsaRaceRollUp;
	}
	public void setHrsaRaceRollUp(Long hrsaRaceRollUp) {
		this.hrsaRaceRollUp = hrsaRaceRollUp;
	}
	@Column(name="GUID")
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	@Column(name="PRODUCT_CODE")
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	
	/*@Column(name="CORD_AVAIL_CONFIRM_DATE")
	public Date getCordUnavailConfirmDate() {
		return cordUnavailConfirmDate;
	}
	public void setCordUnavailConfirmDate(Date cordUnavailConfirmDate) {
		this.cordUnavailConfirmDate = cordUnavailConfirmDate;
	}*/
	
	@Column(name="IDM_CMS_APPROVED_LAB")
	public Long getIdmcmsapprovedlab() {
		return idmcmsapprovedlab;
	}
	public void setIdmcmsapprovedlab(Long idmcmsapprovedlab) {
		this.idmcmsapprovedlab = idmcmsapprovedlab;
	}
	
	@Column(name="IDM_FDA_LICENSED_LAB")
	public Long getIdmfdalicensedlab() {
		return idmfdalicensedlab;
	}
	public void setIdmfdalicensedlab(Long idmfdalicensedlab) {
		this.idmfdalicensedlab = idmfdalicensedlab;
	}
	
	@Column(name="FUNG_CULT_DATE")
	public Date getFungCultDate() {
		return fungCultDate;
	}
	public void setFungCultDate(Date fungCultDate) {
		this.fungCultDate = fungCultDate;
	}
	
	@Column(name="BACT_CULT_DATE")
	public Date getBactCultDate() {
		return bactCultDate;
	}
	public void setBactCultDate(Date bactCultDate) {
		this.bactCultDate = bactCultDate;
	}
	
	/*@Column(name="CBU_COLL_TIM")	
	public String getCbuCollectionTime() {
		return cbuCollectionTime;
	}
	public void setCbuCollectionTime(String cbuCollectionTime) {
		this.cbuCollectionTime = cbuCollectionTime;
	}
	
	@Column(name="CBU_BABY_BIR_TIM")	
	public String getBabyBirthTime() {
		return babyBirthTime;
	}
	public void setBabyBirthTime(String babyBirthTime) {
		this.babyBirthTime = babyBirthTime;
	}*/
	@Column(name="CB_IDS_CHANGE_REASON")	
	public String getCordIdsChangeReason() {
		return cordIdsChangeReason;
	}
	public void setCordIdsChangeReason(String cordIdsChangeReason) {
		this.cordIdsChangeReason = cordIdsChangeReason;
	}
	@Column(name="CB_RHTYPE_OTHER_SPEC")	
	public String getSpecRhTypeOther() {
		return specRhTypeOther;
	}
	public void setSpecRhTypeOther(String specRhTypeOther) {
		this.specRhTypeOther = specRhTypeOther;
	}
	@Column(name="PREG_REF_NUM")	
	public Long getPregRefNumber() {
		return pregRefNumber;
	}
	public void setPregRefNumber(Long pregRefNumber) {
		this.pregRefNumber = pregRefNumber;
	}
	/*@Column(name="PRCSNG_START_TIME")
	public String getProcessingTime() {
		return processingTime;
	}
	public void setProcessingTime(String processingTime) {
		this.processingTime = processingTime;
	}
	@Column(name="FRZ_TIME")
	public String getFreezeTime() {
		return freezeTime;
	}
	public void setFreezeTime(String freezeTime) {
		this.freezeTime = freezeTime;
	}*/
	@Column(name="MATERNAL_GUID")
	public String getMatGuid() {
		return matGuid;
	}
	public void setMatGuid(String matGuid) {
		this.matGuid = matGuid;
	}
	@Column(name="SYSTEM_CORD")
	public String getIsSystemCord() {
		return isSystemCord;
	}
	public void setIsSystemCord(String isSystemCord) {
		this.isSystemCord = isSystemCord;
	}
		
}