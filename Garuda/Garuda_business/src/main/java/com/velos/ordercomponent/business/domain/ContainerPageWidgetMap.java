package com.velos.ordercomponent.business.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author Mohiuddin Ali Ahmed
 *
 */
@Entity
@Table(name="UI_CONTAINER_WIDGET_MAP")
@SequenceGenerator(sequenceName="SEQ_UI_CONTAINER_WIDGET_MAP",name="SEQ_UI_CONTAINER_WIDGET_MAP",allocationSize=1)
public class ContainerPageWidgetMap {

	private Long contPageWidgetMapId;
	private Long containerId;
	private Long widgetId;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_UI_CONTAINER_WIDGET_MAP")
	@Column(name="PK_PG_CONTI_WID")
	public Long getContPageWidgetMapId() {
		return contPageWidgetMapId;
	}

	public void setContPageWidgetMapId(Long contPageWidgetMapId) {
		this.contPageWidgetMapId = contPageWidgetMapId;
	}

	@Column(name="FK_CONTAINER_ID")
	public Long getContainerId() {
		return containerId;
	}

	public void setContainerId(Long containerId) {
		this.containerId = containerId;
	}

	@Column(name="FK_WIDGET_ID")
	public Long getWidgetId() {
		return widgetId;
	}

	public void setWidgetId(Long widgetId) {
		this.widgetId = widgetId;
	}
	
	
	
}
