package com.velos.ordercomponent.business.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name="ER_USER")
@SequenceGenerator(sequenceName="SEQ_ER_USER",name="SEQ_ER_USER",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class UserDomain extends Auditable{
	private static final long serialVersionUID = 1L;
	private Long userId;
	private String loginId;
	private String lastName;
	private String firstName;
	private String middleName;
	private Address address;
	private Long fkAccount; 
	//private Long userType;
	private String usrType;
	private String email;
	private String deletedFlag;
	private String userTitle;
	private Grps group;
	
	

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_ER_USER")
	@Column(name="PK_USER")
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	@Column(name="USR_LOGNAME")
	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	@Column(name="USR_LASTNAME")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	@Column(name="USR_FIRSTNAME")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	@Column(name="USR_MIDNAME")
	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	@Column(name="FK_ACCOUNT")
	public Long getFkAccount() {
		return fkAccount;
	}

	public void setFkAccount(Long fkAccount) {
		this.fkAccount = fkAccount;
	}

	/*
	@Column(name="USR_TYPE")
	public Long getUserType() {
		return userType;
	}

	public void setUserType(Long userType) {
		this.userType = userType;
	}
	 */
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="FK_PERADD",nullable=false,insertable=false,updatable=false)
	@NotFound(action=NotFoundAction.IGNORE)
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	

	@Column(name="USR_TYPE")
	public String getUsrType() {
		return usrType;
	}

	public void setUsrType(String usrType) {
		this.usrType = usrType;
	}

	@Column(name="EMAIL")
	public String getEmail() {
		return email;
	}

	
	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name="DELETED_FLAG")
	public String getDeletedFlag() {
		return deletedFlag;
	}

	public void setDeletedFlag(String deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	
	@Column(name="USR_TITLE")
	public String getUserTitle() {
		return userTitle;
	}

	public void setUserTitle(String userTitle) {
		this.userTitle = userTitle;
	}
	
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="FK_GRP_DEFAULT",nullable=false,insertable=false,updatable=false)
	@NotFound(action=NotFoundAction.IGNORE)
	public Grps getGroup() {
		return group;
	}

	public void setGroup(Grps group) {
		this.group = group;
	}
	
}