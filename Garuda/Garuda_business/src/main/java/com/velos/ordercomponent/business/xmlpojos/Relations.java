package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;

public class Relations {

	private String Relation;
	private Diseases diseases;
	private String otherDescription;
	//private List<Diseases> diseases;
	@XmlElement(name="Relation")
	public String getRelation() {
		return Relation;
	}
	public void setRelation(String relation) {
		Relation = relation;
	}
	@XmlElement(name="Diseases")
	public Diseases getDiseases() {
		return diseases;
	}
	public void setDiseases(Diseases diseases) {
		this.diseases = diseases;
	}
	@XmlElement(name="OtherDescription")
	public String getOtherDescription() {
		return otherDescription;
	}
	public void setOtherDescription(String otherDescription) {
		this.otherDescription = otherDescription;
	}
	/*@XmlElement(name="Diseases")
	public List<Diseases> getDiseases() {
		return diseases;
	}
	public void setDiseases(List<Diseases> diseases) {
		this.diseases = diseases;
	}*/
	
	
	
}
