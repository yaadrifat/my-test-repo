package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

public class TaskListPojo {
/*private Long pkTaskId;
private Long taskType;
private Date taskStrtDate;
private Date taskEndDate;
private Long taskAssignTo;
private Long domainId;
private Long createdBy;
private Long createdOn;
private Long lstModBy;
private Date lstModOn;
private String ipAddress;
private String delFlag;
private Long recordId;

public Long getPkTaskId() {
	return pkTaskId;
}

public void setPkTaskId(Long pkTaskId) {
	this.pkTaskId = pkTaskId;
}

public Long getTaskType() {
	return taskType;
}

public void setTaskType(Long taskType) {
	this.taskType = taskType;
}

public Date getTaskStrtDate() {
	return taskStrtDate;
}

public void setTaskStrtDate(Date taskStrtDate) {
	this.taskStrtDate = taskStrtDate;
}

public Date getTaskEndDate() {
	return taskEndDate;
}

public void setTaskEndDate(Date taskEndDate) {
	this.taskEndDate = taskEndDate;
}

public Long getTaskAssignTo() {
	return taskAssignTo;
}

public void setTaskAssignTo(Long taskAssignTo) {
	this.taskAssignTo = taskAssignTo;
}

public Long getDomainId() {
	return domainId;
}

public void setDomainId(Long domainId) {
	this.domainId = domainId;
}

public Long getCreatedBy() {
	return createdBy;
}

public void setCreatedBy(Long createdBy) {
	this.createdBy = createdBy;
}

public Long getCreatedOn() {
	return createdOn;
}

public void setCreatedOn(Long createdOn) {
	this.createdOn = createdOn;
}

public Long getLstModBy() {
	return lstModBy;
}

public void setLstModBy(Long lstModBy) {
	this.lstModBy = lstModBy;
}

public Date getLstModOn() {
	return lstModOn;
}

public void setLstModOn(Date lstModOn) {
	this.lstModOn = lstModOn;
}

public String getIpAddress() {
	return ipAddress;
}

public void setIpAddress(String ipAddress) {
	this.ipAddress = ipAddress;
}

public String getDelFlag() {
	return delFlag;
}

public void setDelFlag(String delFlag) {
	this.delFlag = delFlag;
}

public Long getRecordId() {
	return recordId;
}

public void setRecordId(Long recordId) {
	this.recordId = recordId;
}*/
	private String orderType;
	private String orderNumber;
	private String cordRegistryId;
	private String orderDate;
	private String orderStatus;
	private String activityDesc;
	private Long taskCompflag;
	private Long pkOrder;
	private Long fkentityId;
	private String activityName;
	private String sampleAtLab;
	private String fksiteid;
	private String regId1;
	
	
	
	
	public String getRegId1() {
		return regId1;
	}
	public void setRegId1(String regId1) {
		this.regId1 = regId1;
	}
	public String getFksiteid() {
		return fksiteid;
	}
	public void setFksiteid(String fksiteid) {
		this.fksiteid = fksiteid;
	}
	public String getSampleAtLab() {
		return sampleAtLab;
	}
	public void setSampleAtLab(String sampleAtLab) {
		this.sampleAtLab = sampleAtLab;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getCordRegistryId() {
		return cordRegistryId;
	}
	public void setCordRegistryId(String cordRegistryId) {
		this.cordRegistryId = cordRegistryId;
	}
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getActivityDesc() {
		return activityDesc;
	}
	public void setActivityDesc(String activityDesc) {
		this.activityDesc = activityDesc;
	}
	public Long getTaskCompflag() {
		return taskCompflag;
	}
	public void setTaskCompflag(Long taskCompflag) {
		this.taskCompflag = taskCompflag;
	}
	public Long getPkOrder() {
		return pkOrder;
	}
	public void setPkOrder(Long pkOrder) {
		this.pkOrder = pkOrder;
	}
	public Long getFkentityId() {
		return fkentityId;
	}
	public void setFkentityId(Long fkentityId) {
		this.fkentityId = fkentityId;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	
	
	
	

}
