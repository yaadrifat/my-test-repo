package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.velos.ordercomponent.business.pojoobjects.CBBProcedurePojo;





@Entity
@Table(name="CBB_PROCESSING_PROCEDURES_INFO")
@SequenceGenerator(sequenceName="SEQ_CBB_PROCESSING_PROCED_INFO",name="SEQ_CBB_PROCESSING_PROCED_INFO",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class CBBProceduresInfo extends Auditable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long pkProcInfoId;
	private Long fkProcessingId;
	private Long fkStorMethod;
	private Long fkFreezManufac;
	private String otherFreezManufac;
	private Long fkStorTemp;
	private Long fkContrlRateFreezing;
	private Long fkFrozenIn;
	private String otherFrozenCont;
	private Long noOfIndiFrac;
	private Long fkBagType;
	private String cryoBagManufac;
	private Long maxValue;
	private Long fkProcMethId;
	private Long fkIfAutomated;
	private String otherProcessing;
	private Long fkProductModification;
	private String otherProdModi;
	private Long noOfSegments;
	private Long filterPaper;
	private Long rbcPallets;
	private Long noOfExtrDnaAliquots;
	private Long noOfNonviableAliquots;
	private Long noOfSerumAliquots;
	private Long noOfPlasmaAliquots;
	private Long noOfViableCellAliquots;
	private Long totCbuAliquots;
	private Long noOfExtrDnaMaterAliquots;
	private Long noOfCellMaterAliquots;
	private Long noOfSerumMaterAliquots;
	private Long noOfPlasmaMaterAliquots;
	private Long totMaterAliquots;
	private String sopRefNo;
	private String sopTitle;
	private Date sopStrtDate;
	private Date sopTermiDate;
	private Double thouUnitPerMlHeparin;
	private Double fiveUnitPerMlHeparin;
	private Double tenUnitPerMlHeparin;
	private Double sixPerHydroxyethylStarch;
	private Double cpda;
	private Double cpd;
	private Double acd;
	private Double otherAnticoagulant;
	private String specifyOthAnti;
	private Double hunPerDmso;
	private Double hunPerGlycerol;
	private Double tenPerDextran_40;
	private Double fivePerHumanAlbu;
	private Double twenFiveHumAlbu;
	private Double plasmalyte;
	private Double othCryoprotectant;
	private String specOthCryopro;
	private Double fivePerDextrose;
	private Double pointNinePerNacl;
	private Double othDiluents;
	private String specOthDiluents;
	private CBBProcedures cbbProcedures;
	private String otherBagType;
	private Double thouUnitPerMlHeparinper;
	private Double fiveUnitPerMlHeparinper;
	private Double tenUnitPerMlHeparinper;
	private Double sixPerHydroxyethylStarchper;
	private Double cpdaper;
	private Double cpdper;
	private Double acdper;
	private Double otherAnticoagulantper;
	private Double hunPerDmsoper;
	private Double hunPerGlycerolper;
	private Double tenPerDextran_40per;
	private Double fivePerHumanAlbuper;
	private Double twenFiveHumAlbuper;
	private Double plasmalyteper;
	private Double othCryoprotectantper;
	private Double fivePerDextroseper;
	private Double pointNinePerNaclper;
	private Double othDiluentsper;
	
	private Long noOfBags;
	private Long fkBag1Type;
	private Long fkBag2Type;
	private Long noOfViableSampleNotRep;
	private Long noOfOthRepAliqProd;
	private Long noOfOthRepAliqAltCond;
	private String cryoBagManufac2;
	private Long maxValue2;
	private String otherBagType2;
	private String otherBagType1;
	
	

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CBB_PROCESSING_PROCED_INFO")
	@Column(name="PK_PROC_INFO")
	public Long getPkProcInfoId() {
		return pkProcInfoId;
	}
	public void setPkProcInfoId(Long pkProcInfoId) {
		this.pkProcInfoId = pkProcInfoId;
	}
	@Column(name="FK_STOR_METHOD")
	public Long getFkStorMethod() {
		return fkStorMethod;
	}
	public void setFkStorMethod(Long fkStorMethod) {
		this.fkStorMethod = fkStorMethod;
	}
	@Column(name="FK_FREEZ_MANUFAC")
	public Long getFkFreezManufac() {
		return fkFreezManufac;
	}
	public void setFkFreezManufac(Long fkFreezManufac) {
		this.fkFreezManufac = fkFreezManufac;
	}
	@Column(name="OTHER_FREEZ_MANUFAC")
	public String getOtherFreezManufac() {
		return otherFreezManufac;
	}
	public void setOtherFreezManufac(String otherFreezManufac) {
		this.otherFreezManufac = otherFreezManufac;
	}
	@Column(name="FK_STOR_TEMP")
	public Long getFkStorTemp() {
		return fkStorTemp;
	}
	public void setFkStorTemp(Long fkStorTemp) {
		this.fkStorTemp = fkStorTemp;
	}
	@Column(name="FK_CONTRL_RATE_FREEZING")
	public Long getFkContrlRateFreezing() {
		return fkContrlRateFreezing;
	}
	public void setFkContrlRateFreezing(Long fkContrlRateFreezing) {
		this.fkContrlRateFreezing = fkContrlRateFreezing;
	}
	@Column(name="FK_FROZEN_IN")
	public Long getFkFrozenIn() {
		return fkFrozenIn;
	}
	public void setFkFrozenIn(Long fkFrozenIn) {
		this.fkFrozenIn = fkFrozenIn;
	}
	@Column(name="OTHER_FROZEN_CONT")
	public String getOtherFrozenCont() {
		return otherFrozenCont;
	}
	public void setOtherFrozenCont(String otherFrozenCont) {
		this.otherFrozenCont = otherFrozenCont;
	}
	@Column(name="NO_OF_INDI_FRAC")
	public Long getNoOfIndiFrac() {
		return noOfIndiFrac;
	}
	public void setNoOfIndiFrac(Long noOfIndiFrac) {
		this.noOfIndiFrac = noOfIndiFrac;
	}
	@Column(name="FK_BAG_TYPE")
	public Long getFkBagType() {
		return fkBagType;
	}
	public void setFkBagType(Long fkBagType) {
		this.fkBagType = fkBagType;
	}
	@Column(name="CRYOBAG_MANUFAC")
	public String getCryoBagManufac() {
		return cryoBagManufac;
	}
	public void setCryoBagManufac(String cryoBagManufac) {
		this.cryoBagManufac = cryoBagManufac;
	}
	@Column(name="MAX_VALUE")
	public Long getMaxValue() {
		return maxValue;
	}
	public void setMaxValue(Long maxValue) {
		this.maxValue = maxValue;
	}
	
	@Column(name="FK_PROCESSING_ID")
	public Long getFkProcessingId() {
		return fkProcessingId;
	}
	public void setFkProcessingId(Long fkProcessingId) {
		this.fkProcessingId = fkProcessingId;
	}
	
	@Column(name="FK_PROC_METH_ID")
	public Long getFkProcMethId() {
		return fkProcMethId;
	}
	public void setFkProcMethId(Long fkProcMethId) {
		this.fkProcMethId = fkProcMethId;
	}
	
	@Column(name="FK_IF_AUTOMATED")
	public Long getFkIfAutomated() {
		return fkIfAutomated;
	}
	public void setFkIfAutomated(Long fkIfAutomated) {
		this.fkIfAutomated = fkIfAutomated;
	}
	@Column(name="OTHER_PROCESSING")
	public String getOtherProcessing() {
		return otherProcessing;
	}
	public void setOtherProcessing(String otherProcessing) {
		this.otherProcessing = otherProcessing;
	}
	@Column(name="FK_PRODUCT_MODIFICATION")
	public Long getFkProductModification() {
		return fkProductModification;
	}
	public void setFkProductModification(Long fkProductModification) {
		this.fkProductModification = fkProductModification;
	}
	@Column(name="OTHER_PROD_MODI")
	public String getOtherProdModi() {
		return otherProdModi;
	}
	public void setOtherProdModi(String otherProdModi) {
		this.otherProdModi = otherProdModi;
	}
	@Column(name="NO_OF_SEGMENTS")
	public Long getNoOfSegments() {
		return noOfSegments;
	}
	public void setNoOfSegments(Long noOfSegments) {
		this.noOfSegments = noOfSegments;
	}
	@Column(name="FILTER_PAPER")
	public Long getFilterPaper() {
		return filterPaper;
	}
	public void setFilterPaper(Long filterPaper) {
		this.filterPaper = filterPaper;
	}
	@Column(name="RBC_PALLETS")
	public Long getRbcPallets() {
		return rbcPallets;
	}
	public void setRbcPallets(Long rbcPallets) {
		this.rbcPallets = rbcPallets;
	}
	@Column(name="NO_OF_EXTR_DNA_ALIQUOTS")
	public Long getNoOfExtrDnaAliquots() {
		return noOfExtrDnaAliquots;
	}
	public void setNoOfExtrDnaAliquots(Long noOfExtrDnaAliquots) {
		this.noOfExtrDnaAliquots = noOfExtrDnaAliquots;
	}
	@Column(name="NO_OF_NONVIABLE_ALIQUOTS")
	public Long getNoOfNonviableAliquots() {
		return noOfNonviableAliquots;
	}
	public void setNoOfNonviableAliquots(Long noOfNonviableAliquots) {
		this.noOfNonviableAliquots = noOfNonviableAliquots;
	}
	@Column(name="NO_OF_SERUM_ALIQUOTS")
	public Long getNoOfSerumAliquots() {
		return noOfSerumAliquots;
	}
	public void setNoOfSerumAliquots(Long noOfSerumAliquots) {
		this.noOfSerumAliquots = noOfSerumAliquots;
	}
	@Column(name="NO_OF_PLASMA_ALIQUOTS")
	public Long getNoOfPlasmaAliquots() {
		return noOfPlasmaAliquots;
	}
	public void setNoOfPlasmaAliquots(Long noOfPlasmaAliquots) {
		this.noOfPlasmaAliquots = noOfPlasmaAliquots;
	}
	@Column(name="NO_OF_VIABLE_CELL_ALIQUOTS")
	public Long getNoOfViableCellAliquots() {
		return noOfViableCellAliquots;
	}
	public void setNoOfViableCellAliquots(Long noOfViableCellAliquots) {
		this.noOfViableCellAliquots = noOfViableCellAliquots;
	}
	@Column(name="TOT_CBU_ALIQUOTS")
	public Long getTotCbuAliquots() {
		return totCbuAliquots;
	}
	public void setTotCbuAliquots(Long totCbuAliquots) {
		this.totCbuAliquots = totCbuAliquots;
	}
	@Column(name="NO_OF_EXTR_DNA_MATER_ALIQUOTS")
	public Long getNoOfExtrDnaMaterAliquots() {
		return noOfExtrDnaMaterAliquots;
	}
	public void setNoOfExtrDnaMaterAliquots(Long noOfExtrDnaMaterAliquots) {
		this.noOfExtrDnaMaterAliquots = noOfExtrDnaMaterAliquots;
	}
	@Column(name="NO_OF_CELL_MATER_ALIQUOTS")
	public Long getNoOfCellMaterAliquots() {
		return noOfCellMaterAliquots;
	}
	public void setNoOfCellMaterAliquots(Long noOfCellMaterAliquots) {
		this.noOfCellMaterAliquots = noOfCellMaterAliquots;
	}
	@Column(name="NO_OF_SERUM_MATER_ALIQUOTS")
	public Long getNoOfSerumMaterAliquots() {
		return noOfSerumMaterAliquots;
	}
	public void setNoOfSerumMaterAliquots(Long noOfSerumMaterAliquots) {
		this.noOfSerumMaterAliquots = noOfSerumMaterAliquots;
	}
	@Column(name="NO_OF_PLASMA_MATER_ALIQUOTS")
	public Long getNoOfPlasmaMaterAliquots() {
		return noOfPlasmaMaterAliquots;
	}
	public void setNoOfPlasmaMaterAliquots(Long noOfPlasmaMaterAliquots) {
		this.noOfPlasmaMaterAliquots = noOfPlasmaMaterAliquots;
	}
	@Column(name="TOT_MATER_ALIQUOTS")
	public Long getTotMaterAliquots() {
		return totMaterAliquots;
	}
	public void setTotMaterAliquots(Long totMaterAliquots) {
		this.totMaterAliquots = totMaterAliquots;
	}
	@Column(name="SOP_REF_NO")
	public String getSopRefNo() {
		return sopRefNo;
	}
	public void setSopRefNo(String sopRefNo) {
		this.sopRefNo = sopRefNo;
	}
	@Column(name="SOP_TITLE")
	public String getSopTitle() {
		return sopTitle;
	}
	public void setSopTitle(String sopTitle) {
		this.sopTitle = sopTitle;
	}
	@Column(name="SOP_STRT_DATE")
	public Date getSopStrtDate() {
		return sopStrtDate;
	}
	public void setSopStrtDate(Date sopStrtDate) {
		this.sopStrtDate = sopStrtDate;
	}
	@Column(name="SOP_TERMI_DATE")
	public Date getSopTermiDate() {
		return sopTermiDate;
	}
	public void setSopTermiDate(Date sopTermiDate) {
		this.sopTermiDate = sopTermiDate;
	}
	@Column(name="THOU_UNIT_PER_ML_HEPARIN")
	public Double getThouUnitPerMlHeparin() {
		return thouUnitPerMlHeparin;
	}
	public void setThouUnitPerMlHeparin(Double thouUnitPerMlHeparin) {
		this.thouUnitPerMlHeparin = thouUnitPerMlHeparin;
	}@Column(name="FIVE_UNIT_PER_ML_HEPARIN")
	public Double getFiveUnitPerMlHeparin() {
		return fiveUnitPerMlHeparin;
	}
	public void setFiveUnitPerMlHeparin(Double fiveUnitPerMlHeparin) {
		this.fiveUnitPerMlHeparin = fiveUnitPerMlHeparin;
	}
	@Column(name="TEN_UNIT_PER_ML_HEPARIN")
	public Double getTenUnitPerMlHeparin() {
		return tenUnitPerMlHeparin;
	}
	public void setTenUnitPerMlHeparin(Double tenUnitPerMlHeparin) {
		this.tenUnitPerMlHeparin = tenUnitPerMlHeparin;
	}
	@Column(name="SIX_PER_HYDROXYETHYL_STARCH")
	public Double getSixPerHydroxyethylStarch() {
		return sixPerHydroxyethylStarch;
	}
	public void setSixPerHydroxyethylStarch(Double sixPerHydroxyethylStarch) {
		this.sixPerHydroxyethylStarch = sixPerHydroxyethylStarch;
	}
	@Column(name="CPDA")
	public Double getCpda() {
		return cpda;
	}
	public void setCpda(Double cpda) {
		this.cpda = cpda;
	}
	@Column(name="CPD")
	public Double getCpd() {
		return cpd;
	}
	public void setCpd(Double cpd) {
		this.cpd = cpd;
	}
	@Column(name="ACD")
	public Double getAcd() {
		return acd;
	}
	public void setAcd(Double acd) {
		this.acd = acd;
	}
	@Column(name="OTHER_ANTICOAGULANT")
	public Double getOtherAnticoagulant() {
		return otherAnticoagulant;
	}
	public void setOtherAnticoagulant(Double otherAnticoagulant) {
		this.otherAnticoagulant = otherAnticoagulant;
	}
	@Column(name="SPECIFY_OTH_ANTI")
	public String getSpecifyOthAnti() {
		return specifyOthAnti;
	}
	public void setSpecifyOthAnti(String specifyOthAnti) {
		this.specifyOthAnti = specifyOthAnti;
	}
	@Column(name="HUN_PER_DMSO")
	public Double getHunPerDmso() {
		return hunPerDmso;
	}
	public void setHunPerDmso(Double hunPerDmso) {
		this.hunPerDmso = hunPerDmso;
	}
	@Column(name="HUN_PER_GLYCEROL")
	public Double getHunPerGlycerol() {
		return hunPerGlycerol;
	}
	public void setHunPerGlycerol(Double hunPerGlycerol) {
		this.hunPerGlycerol = hunPerGlycerol;
	}
	@Column(name="TEN_PER_DEXTRAN_40")
	public Double getTenPerDextran_40() {
		return tenPerDextran_40;
	}
	public void setTenPerDextran_40(Double tenPerDextran_40) {
		this.tenPerDextran_40 = tenPerDextran_40;
	}
	@Column(name="FIVE_PER_HUMAN_ALBU")
	public Double getFivePerHumanAlbu() {
		return fivePerHumanAlbu;
	}
	
	public void setFivePerHumanAlbu(Double fivePerHumanAlbu) {
		this.fivePerHumanAlbu = fivePerHumanAlbu;
	}
	@Column(name="TWEN_FIVE_HUM_ALBU")
	public Double getTwenFiveHumAlbu() {
		return twenFiveHumAlbu;
	}
	
	public void setTwenFiveHumAlbu(Double twenFiveHumAlbu) {
		this.twenFiveHumAlbu = twenFiveHumAlbu;
	}
	@Column(name="PLASMALYTE")
	public Double getPlasmalyte() {
		return plasmalyte;
	}
	public void setPlasmalyte(Double plasmalyte) {
		this.plasmalyte = plasmalyte;
	}
	@Column(name="OTH_CRYOPROTECTANT")
	public Double getOthCryoprotectant() {
		return othCryoprotectant;
	}
	public void setOthCryoprotectant(Double othCryoprotectant) {
		this.othCryoprotectant = othCryoprotectant;
	}
	@Column(name="SPEC_OTH_CRYOPRO")
	public String getSpecOthCryopro() {
		return specOthCryopro;
	}
	public void setSpecOthCryopro(String specOthCryopro) {
		this.specOthCryopro = specOthCryopro;
	}
	@Column(name="FIVE_PER_DEXTROSE")
	public Double getFivePerDextrose() {
		return fivePerDextrose;
	}
	public void setFivePerDextrose(Double fivePerDextrose) {
		this.fivePerDextrose = fivePerDextrose;
	}
	@Column(name="POINNT_NINE_PER_NACL")
	public Double getPointNinePerNacl() {
		return pointNinePerNacl;
	}
	public void setPointNinePerNacl(Double pointNinePerNacl) {
		this.pointNinePerNacl = pointNinePerNacl;
	}
	@Column(name ="OTH_DILUENTS")
	public Double getOthDiluents() {
		return othDiluents;
	}
	public void setOthDiluents(Double othDiluents) {
		this.othDiluents = othDiluents;
	}
	@Column(name="SPEC_OTH_DILUENTS")
	public String getSpecOthDiluents() {
		return specOthDiluents;
	}
	public void setSpecOthDiluents(String specOthDiluents) {
		this.specOthDiluents = specOthDiluents;
	}
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="FK_PROCESSING_ID",nullable=false,insertable=false,updatable=false)
	@NotFound(action=NotFoundAction.IGNORE)
	public CBBProcedures getCbbProcedures() {
		return cbbProcedures;
	}
	public void setCbbProcedures(CBBProcedures cbbProcedures) {
		this.cbbProcedures = cbbProcedures;
	}
	
	@Column(name="OTHER_BAG_TYPE")
	public String getOtherBagType() {
		return otherBagType;
	}
	public void setOtherBagType(String otherBagType) {
		this.otherBagType = otherBagType;
	}
	
	
	@Column(name="THOU_UNIT_PER_ML_HEPARIN_PER")
	public Double getThouUnitPerMlHeparinper() {
		return thouUnitPerMlHeparinper;
	}
	public void setThouUnitPerMlHeparinper(Double thouUnitPerMlHeparinper) {
		this.thouUnitPerMlHeparinper = thouUnitPerMlHeparinper;
	}
	
	@Column(name="FIVE_UNIT_PER_ML_HEPARIN_PER")
	public Double getFiveUnitPerMlHeparinper() {
		return fiveUnitPerMlHeparinper;
	}
	public void setFiveUnitPerMlHeparinper(Double fiveUnitPerMlHeparinper) {
		this.fiveUnitPerMlHeparinper = fiveUnitPerMlHeparinper;
	}
	
	@Column(name="TEN_UNIT_PER_ML_HEPARIN_PER")
	public Double getTenUnitPerMlHeparinper() {
		return tenUnitPerMlHeparinper;
	}
	public void setTenUnitPerMlHeparinper(Double tenUnitPerMlHeparinper) {
		this.tenUnitPerMlHeparinper = tenUnitPerMlHeparinper;
	}
	
	@Column(name="SIX_PER_HYDRO_STARCH_PER")
	public Double getSixPerHydroxyethylStarchper() {
		return sixPerHydroxyethylStarchper;
	}
	public void setSixPerHydroxyethylStarchper(Double sixPerHydroxyethylStarchper) {
		this.sixPerHydroxyethylStarchper = sixPerHydroxyethylStarchper;
	}
	
	@Column(name="CPDA_PER")
	public Double getCpdaper() {
		return cpdaper;
	}
	public void setCpdaper(Double cpdaper) {
		this.cpdaper = cpdaper;
	}
	
	@Column(name="CPD_PER")
	public Double getCpdper() {
		return cpdper;
	}
	public void setCpdper(Double cpdper) {
		this.cpdper = cpdper;
	}
	
	@Column(name="ACD_PER")
	public Double getAcdper() {
		return acdper;
	}
	public void setAcdper(Double acdper) {
		this.acdper = acdper;
	}
	
	
	@Column(name="OTHER_ANTICOAGULANT_PER")
	public Double getOtherAnticoagulantper() {
		return otherAnticoagulantper;
	}
	public void setOtherAnticoagulantper(Double otherAnticoagulantper) {
		this.otherAnticoagulantper = otherAnticoagulantper;
	}
	
	
	@Column(name="HUN_PER_DMSO_PER")
	public Double getHunPerDmsoper() {
		return hunPerDmsoper;
	}
	public void setHunPerDmsoper(Double hunPerDmsoper) {
		this.hunPerDmsoper = hunPerDmsoper;
	}
	
	@Column(name="HUN_PER_GLYCEROL_PER")
	public Double getHunPerGlycerolper() {
		return hunPerGlycerolper;
	}
	public void setHunPerGlycerolper(Double hunPerGlycerolper) {
		this.hunPerGlycerolper = hunPerGlycerolper;
	}
	
	
	@Column(name="TEN_PER_DEXTRAN_40_PER")
	public Double getTenPerDextran_40per() {
		return tenPerDextran_40per;
	}
	public void setTenPerDextran_40per(Double tenPerDextran_40per) {
		this.tenPerDextran_40per = tenPerDextran_40per;
	}
	
	@Column(name="FIVE_PER_HUMAN_ALBU_PER")
	public Double getFivePerHumanAlbuper() {
		return fivePerHumanAlbuper;
	}
	public void setFivePerHumanAlbuper(Double fivePerHumanAlbuper) {
		this.fivePerHumanAlbuper = fivePerHumanAlbuper;
	}
	
	@Column(name="TWEN_FIVE_HUM_ALBU_PER")
	public Double getTwenFiveHumAlbuper() {
		return twenFiveHumAlbuper;
	}
	public void setTwenFiveHumAlbuper(Double twenFiveHumAlbuper) {
		this.twenFiveHumAlbuper = twenFiveHumAlbuper;
	}
	
	
	@Column(name="PLASMALYTE_PER")
	public Double getPlasmalyteper() {
		return plasmalyteper;
	}
	public void setPlasmalyteper(Double plasmalyteper) {
		this.plasmalyteper = plasmalyteper;
	}
	
	@Column(name="OTH_CRYOPROTECTANT_PER")
	public Double getOthCryoprotectantper() {
		return othCryoprotectantper;
	}
	public void setOthCryoprotectantper(Double othCryoprotectantper) {
		this.othCryoprotectantper = othCryoprotectantper;
	}
	
	
	@Column(name="FIVE_PER_DEXTROSE_PER")
	public Double getFivePerDextroseper() {
		return fivePerDextroseper;
	}
	public void setFivePerDextroseper(Double fivePerDextroseper) {
		this.fivePerDextroseper = fivePerDextroseper;
	}
	
	
	@Column(name="POINNT_NINE_PER_NACL_PER")
	public Double getPointNinePerNaclper() {
		return pointNinePerNaclper;
	}
	public void setPointNinePerNaclper(Double pointNinePerNaclper) {
		this.pointNinePerNaclper = pointNinePerNaclper;
	}
	
	
	@Column(name="OTH_DILUENTS_PER")
	public Double getOthDiluentsper() {
		return othDiluentsper;
	}
	public void setOthDiluentsper(Double othDiluentsper) {
		this.othDiluentsper = othDiluentsper;
	}
	
	@Column(name="FK_NUM_OF_BAGS")
	public Long getNoOfBags() {
		return noOfBags;
	}
	public void setNoOfBags(Long noOfBags) {
		this.noOfBags = noOfBags;
	}
	
	@Column(name="NO_OF_VIABLE_SMPL_FINAL_PROD")
	public Long getNoOfViableSampleNotRep() {
		return noOfViableSampleNotRep;
	}
	public void setNoOfViableSampleNotRep(Long noOfViableSampleNotRep) {
		this.noOfViableSampleNotRep = noOfViableSampleNotRep;
	}
	
	@Column(name="NO_OF_OTH_REP_ALLIQUOTS_F_PROD")
	public Long getNoOfOthRepAliqProd() {
		return noOfOthRepAliqProd;
	}
	public void setNoOfOthRepAliqProd(Long noOfOthRepAliqProd) {
		this.noOfOthRepAliqProd = noOfOthRepAliqProd;
	}
	
	@Column(name="NO_OF_OTH_REP_ALLIQ_ALTER_COND")
	public Long getNoOfOthRepAliqAltCond() {
		return noOfOthRepAliqAltCond;
	}
	public void setNoOfOthRepAliqAltCond(Long noOfOthRepAliqAltCond) {
		this.noOfOthRepAliqAltCond = noOfOthRepAliqAltCond;
	}
	
	@Column(name="FK_BAG_1_TYPE")
	public Long getFkBag1Type() {
		return fkBag1Type;
	}

	public void setFkBag1Type(Long fkBag1Type) {
		this.fkBag1Type = fkBag1Type;
	}

	@Column(name="FK_BAG_2_TYPE")
	public Long getFkBag2Type() {
		return fkBag2Type;
	}

	public void setFkBag2Type(Long fkBag2Type) {
		this.fkBag2Type = fkBag2Type;
	
	}
	
	@Column(name="CRYOBAG_MANUFAC2")
	public String getCryoBagManufac2() {
		return cryoBagManufac2;
	}
	public void setCryoBagManufac2(String cryoBagManufac2) {
		this.cryoBagManufac2 = cryoBagManufac2;
	}
	
	
	@Column(name="MAX_VALUE2")
	public Long getMaxValue2() {
		return maxValue2;
	}
	public void setMaxValue2(Long maxValue2) {
		this.maxValue2 = maxValue2;
	}
	
	@Column(name="OTHER_BAG_TYPE2")
	public String getOtherBagType2() {
		return otherBagType2;
	}
	public void setOtherBagType2(String otherBagType2) {
		this.otherBagType2 = otherBagType2;
	}
	
	@Column(name="OTHER_BAG_TYPE1")
	public String getOtherBagType1() {
		return otherBagType1;
	}
	public void setOtherBagType1(String otherBagType1) {
		this.otherBagType1 = otherBagType1;
	}
	
	
}