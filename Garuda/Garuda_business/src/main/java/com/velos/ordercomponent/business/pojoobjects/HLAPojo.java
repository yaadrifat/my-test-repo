package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;


public class HLAPojo extends AuditablePojo {

	
	private Long pkHla;
	private String fkCordCdrCbuId;
	private Long fkHlaCodeId;
	private Long fkHlaMethodId;
	private String hlaType1;
	private String hlaType2;
	private Long fkentityCordId;
	private Long fkEntityCodeType;
	private Long fkOrderId;
	private Long pkcordId;
	private String orderType;
	private Long fkSpecType;
	private Long fkHlaOverrideClassCode;
	private Date hlaCritOverridedate;
	private Boolean hlaOverrideAntigen1;
	private Boolean hlaOverrideAntigen2;
	private Long fkHlaPrimeDna;
	private Long fkHlaProteinDipId;
	private Long fkHlaAntigen1;
	private Long fkHlaAntigen2;
	private Date hlaRecievedDate;
	private Date hlaTypingDate;
	private Long fkHlaReportingLab;
	private Long fkHlaReportingMethod;
	private Long fkHlaCordCtStatus;
	private Long fkSource;
	private String sampleAtLab;
	private Long hlaOrderSeq;
	private Long hlaTypingSeq;
	private String uName;
	public String lastModifiedByUser;
	
	public Long getPkHla() {
		return pkHla;
	}
	public void setPkHla(Long pkHla) {
		this.pkHla = pkHla;
	}
	public String getFkCordCdrCbuId() {
		return fkCordCdrCbuId;
	}
	public void setFkCordCdrCbuId(String fkCordCdrCbuId) {
		this.fkCordCdrCbuId = fkCordCdrCbuId;
	}
	public Long getFkHlaCodeId() {
		return fkHlaCodeId;
	}
	public void setFkHlaCodeId(Long fkHlaCodeId) {
		this.fkHlaCodeId = fkHlaCodeId;
	}
	public Long getFkHlaMethodId() {
		return fkHlaMethodId;
	}
	public void setFkHlaMethodId(Long fkHlaMethodId) {
		this.fkHlaMethodId = fkHlaMethodId;
	}
	public String getHlaType1() {
		return hlaType1;
	}
	
	public void setHlaType1(String hlaType1) {
		this.hlaType1 = hlaType1;
	}
	public void setHlaType2(String hlaType2) {
		this.hlaType2 = hlaType2;
	}
	public String getHlaType2() {
		return hlaType2;
	}

	private Boolean deletedFlag;
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	public Long getFkentityCordId() {
		return fkentityCordId;
	}
	public void setFkentityCordId(Long fkentityCordId) {
		this.fkentityCordId = fkentityCordId;
	}
	public Long getFkEntityCodeType() {
		return fkEntityCodeType;
	}
	public void setFkEntityCodeType(Long fkEntityCodeType) {
		this.fkEntityCodeType = fkEntityCodeType;
	}
	public Long getFkOrderId() {
		return fkOrderId;
	}
	public void setFkOrderId(Long fkOrderId) {
		this.fkOrderId = fkOrderId;
	}
	public Long getPkcordId() {
		return pkcordId;
	}
	public void setPkcordId(Long pkcordId) {
		this.pkcordId = pkcordId;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
		//System.out.println(" ");
	}
	public Long getFkSpecType() {
		return fkSpecType;
	}
	public void setFkSpecType(Long fkSpecType) {
		this.fkSpecType = fkSpecType;
	}
	public Long getFkHlaOverrideClassCode() {
		return fkHlaOverrideClassCode;
	}
	public void setFkHlaOverrideClassCode(Long fkHlaOverrideClassCode) {
		this.fkHlaOverrideClassCode = fkHlaOverrideClassCode;
	}
	public Date getHlaCritOverridedate() {
		return hlaCritOverridedate;
	}
	public void setHlaCritOverridedate(Date hlaCritOverridedate) {
		this.hlaCritOverridedate = hlaCritOverridedate;
	}
	public Boolean getHlaOverrideAntigen1() {
		return hlaOverrideAntigen1;
	}
	public void setHlaOverrideAntigen1(Boolean hlaOverrideAntigen1) {
		this.hlaOverrideAntigen1 = hlaOverrideAntigen1;
	}
	public Boolean getHlaOverrideAntigen2() {
		return hlaOverrideAntigen2;
	}
	public void setHlaOverrideAntigen2(Boolean hlaOverrideAntigen2) {
		this.hlaOverrideAntigen2 = hlaOverrideAntigen2;
	}
	public Long getFkHlaPrimeDna() {
		return fkHlaPrimeDna;
	}
	public void setFkHlaPrimeDna(Long fkHlaPrimeDna) {
		this.fkHlaPrimeDna = fkHlaPrimeDna;
	}
	public Long getFkHlaProteinDipId() {
		return fkHlaProteinDipId;
	}
	public void setFkHlaProteinDipId(Long fkHlaProteinDipId) {
		this.fkHlaProteinDipId = fkHlaProteinDipId;
	}
	public Long getFkHlaAntigen1() {
		return fkHlaAntigen1;
	}
	public void setFkHlaAntigen1(Long fkHlaAntigen1) {
		this.fkHlaAntigen1 = fkHlaAntigen1;
	}
	public Long getFkHlaAntigen2() {
		return fkHlaAntigen2;
	}
	public void setFkHlaAntigen2(Long fkHlaAntigen2) {
		this.fkHlaAntigen2 = fkHlaAntigen2;
	}
	public Date getHlaRecievedDate() {
		return hlaRecievedDate;
	}
	public void setHlaRecievedDate(Date hlaRecievedDate) {
		this.hlaRecievedDate = hlaRecievedDate;
	}
	public Date getHlaTypingDate() {
		return hlaTypingDate;
	}
	public void setHlaTypingDate(Date hlaTypingDate) {
		this.hlaTypingDate = hlaTypingDate;
	}
	public Long getFkHlaReportingLab() {
		return fkHlaReportingLab;
	}
	public void setFkHlaReportingLab(Long fkHlaReportingLab) {
		this.fkHlaReportingLab = fkHlaReportingLab;
	}
	public Long getFkHlaReportingMethod() {
		return fkHlaReportingMethod;
	}
	public void setFkHlaReportingMethod(Long fkHlaReportingMethod) {
		this.fkHlaReportingMethod = fkHlaReportingMethod;
	}
	public Long getFkHlaCordCtStatus() {
		return fkHlaCordCtStatus;
	}
	public void setFkHlaCordCtStatus(Long fkHlaCordCtStatus) {
		this.fkHlaCordCtStatus = fkHlaCordCtStatus;
	}
	public Long getFkSource() {
		return fkSource;
	}
	public void setFkSource(Long fkSource) {
		this.fkSource = fkSource;
	}
	public String getSampleAtLab() {
		return sampleAtLab;
	}
	public void setSampleAtLab(String sampleAtLab) {
		this.sampleAtLab = sampleAtLab;
	}
	public Long getHlaOrderSeq() {
		return hlaOrderSeq;
	}
	public void setHlaOrderSeq(Long hlaOrderSeq) {
		this.hlaOrderSeq = hlaOrderSeq;
	}
	public Long getHlaTypingSeq() {
		return hlaTypingSeq;
	}
	public void setHlaTypingSeq(Long hlaTypingSeq) {
		this.hlaTypingSeq = hlaTypingSeq;
	}
	public String getuName() {
		return uName;
	}
	public void setuName(String uName) {
		this.uName = uName;
	}	
	public String getLastModifiedByUser() {
		return lastModifiedByUser;
	}
	public void setLastModifiedByUser(String lastModifiedByUser) {
		this.lastModifiedByUser = lastModifiedByUser;
	}
	
}
