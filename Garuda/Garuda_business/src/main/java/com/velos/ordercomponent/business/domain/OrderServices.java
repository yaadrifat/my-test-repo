/**
 * 
 */
package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author jidrushbasha
 *
 */

@Entity
@Table(name="ER_ORDER_SERVICES")
@SequenceGenerator(sequenceName="SEQ_ER_ORDER_SERVICES",name="SEQ_ER_ORDER_SERVICES")
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class OrderServices extends Auditable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long pkOrderServices;
	private Long fkOrderId;
	private String serviceCode;
	private Date requestDate;
	private String resRecFlag;
	private Date resRecDate;
	private String cancelledFlag;
	private Date cancelledDate;
	private Long serviceStatus;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_ER_ORDER_SERVICES")
	@Column(name="PK_ORDER_SERVICES")
	public Long getPkOrderServices() {
		return pkOrderServices;
	}
	public void setPkOrderServices(Long pkOrderServices) {
		this.pkOrderServices = pkOrderServices;
	}
	
	@Column(name="FK_ORDER_ID")
	public Long getFkOrderId() {
		return fkOrderId;
	}
	public void setFkOrderId(Long fkOrderId) {
		this.fkOrderId = fkOrderId;
	}
	
	@Column(name="SERVICE_CODE")
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	
	@Column(name="REQUESTED_DATE")
	public Date getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	
	
	@Column(name="RESULTS_REC_FLAG")
	public String getResRecFlag() {
		return resRecFlag;
	}
	public void setResRecFlag(String resRecFlag) {
		this.resRecFlag = resRecFlag;
	}
	
	@Column(name="RESULTS_REC_DATE")
	public Date getResRecDate() {
		return resRecDate;
	}
	public void setResRecDate(Date resRecDate) {
		this.resRecDate = resRecDate;
	}
	
	@Column(name="CANCELLED_FLAG")
	public String getCancelledFlag() {
		return cancelledFlag;
	}
	public void setCancelledFlag(String cancelledFlag) {
		this.cancelledFlag = cancelledFlag;
	}
	
	@Column(name="CANCELLED_DATE")
	public Date getCancelledDate() {
		return cancelledDate;
	}
	public void setCancelledDate(Date cancelledDate) {
		this.cancelledDate = cancelledDate;
	}
	
	@Column(name="SERVICE_STATUS")
	public Long getServiceStatus() {
		return serviceStatus;
	}
	public void setServiceStatus(Long serviceStatus) {
		this.serviceStatus = serviceStatus;
	}
	
	
	
	

}
