package com.velos.ordercomponent.business.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class VDAHibernateUtil {

	private static final SessionFactory sessionFactory = buildSessionFactory();

	private static SessionFactory buildSessionFactory() {
		try {
			// Create the SessionFactory from hibernate.cfg.xml
			return new AnnotationConfiguration().configure("hibernate-vda.cfg.xml")
					.buildSessionFactory();
		} catch (Throwable ex) {
			//System.out.println(" Erron in hibernate session factory" +ex );
			ex.printStackTrace();
			
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}
