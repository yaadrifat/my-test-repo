package com.velos.ordercomponent.business.domain;

import java.sql.Blob;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.ordercomponent.business.util.VelosUtil;
/**
 * @author Jyoti
 *
 */
@Entity
@Table(name="ER_ORDER_ATTACHMENTS")
@SequenceGenerator(sequenceName="SEQ_ER_ORDER_ATTACHMENTS",name="SEQ_ER_ORDER_ATTACHMENTS",allocationSize=1)
public class OrderAttachments extends Auditable {
	private Long pk_orderattachmentId;
	private Long fk_orderid;
	private Long fk_attachmentId;
	
	private Boolean deletedFlag;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_ER_ORDER_ATTACHMENTS")
	@Column(name="PK_ORDER_ATTACHMENT")
	public Long getPk_orderattachmentId() {
		return pk_orderattachmentId;
	}
	public void setPk_orderattachmentId(Long pk_orderattachmentId) {
		this.pk_orderattachmentId = pk_orderattachmentId;
	}
	@Column(name="FK_ORDER")
	public Long getFk_orderid() {
		return fk_orderid;
	}
	public void setFk_orderid(Long fk_orderid) {
		this.fk_orderid = fk_orderid;
	}
	@Column(name="FK_ATTACHMENT")
	public Long getFk_attachmentId() {
		return fk_attachmentId;
	}
	public void setFk_attachmentId(Long fk_attachmentId) {
		this.fk_attachmentId = fk_attachmentId;
	}
	
	@Column(name="DELETEDFLAG")
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	
}