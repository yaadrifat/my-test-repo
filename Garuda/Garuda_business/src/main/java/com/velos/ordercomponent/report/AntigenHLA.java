package com.velos.ordercomponent.report;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.jdbc.Work;

import com.velos.ordercomponent.business.pojoobjects.HLAFilterPojo;
import com.velos.ordercomponent.business.pojoobjects.HLAPojo;
import com.velos.ordercomponent.report.AntigenBestHla.HLA;
/**
 * @author Anurag Upadhyay
 * it maintain multilple row (History)
 *
 */
public class AntigenHLA implements Work{
	
	public static final Log log = LogFactory.getLog(AntigenHLA.class);
	
	private String query;
	private Map<Long, Map<Long,List<HLAPojo>>> cbuHlas;
	private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	private Map<Long, String> cdlst;
	private Map<Long, Map<Long, HLAFilterPojo>> cbuHla;
	
	public AntigenHLA(String query, Map<Long, String> cdlst) {
		
		this.query = query;
		this.cdlst = cdlst;
	//	file = new File("D:/AntigenHLA.txt");
	}
	
	  @Override
	    public void execute( Connection connection ) throws SQLException
	    {
	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        try
	        {
	            ps = connection.prepareStatement( query);
	            ps.setFetchSize(50000);
	            rs = ps.executeQuery();
	            System.out.println("Query Result Returned");
	            long startTime=System.currentTimeMillis();
	            
	            if (cdlst == null)
					cbuHlas =  prepHlaObjects(rs,connection);
	            else 
	            	cbuHla = prepFilterHlaObj(rs, cdlst);	           
		            
	             long endTime=System.currentTimeMillis();
		         System.out.println();
		         System.out.println((endTime-startTime)/1000+" : "+(endTime-startTime)%1000+" MM:SS\n\n");
	        }
	        
	        
	        catch( SQLException e ) { 
	        	
	        	printException(e);
	        	
	        } catch (ParseException e) {
				
	        	printException(e);
	        	
			}catch (IOException e) {
				// TODO Auto-generated catch block
				printException(e);
			}    
	        finally  {
	            if( rs != null )  
	            {
	                try {
	                    rs.close();
	                    
	                }
	                catch( Exception e ) {printException(e);}
	            }
	            if( ps != null ) 
	            {
	                try {
	                    ps.close();
	                }
	                catch( Exception e ) {printException(e);}
	            }
	            /*if(bw != null){
	            	
	            	 try {
		                    bw.close();
		                }
		                catch( IOException e ) {printException(e);}
	            }*/
	        }
	        
	    }
	
	private void printException(Exception e){
		
		log.error(e.getMessage());
		e.printStackTrace();
		
	}
	public String getLastModifiedUser(Long usrId,Connection conn){
		String lastModifiedUsername=null;
		PreparedStatement pst=null;
		ResultSet modUsrResultset=null;
		
		try {
			pst = conn.prepareStatement("select USR_FIRSTNAME,USR_LASTNAME from ETVDA_CBU_USER_INFO where PK_USER='"+usrId+"'");
			modUsrResultset = pst.executeQuery();
			while(modUsrResultset.next()){
			lastModifiedUsername = modUsrResultset.getString("USR_FIRSTNAME")+" "+modUsrResultset.getString("USR_LASTNAME");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		return lastModifiedUsername;
	}
	
	private Map<Long, Map<Long,List<HLAPojo>>> prepHlaObjects(ResultSet rs,Connection conn) throws SQLException, ParseException, IOException{
		
		Map<Long, Map<Long,List<HLAPojo>>> cbuHlas = new HashMap<Long, Map<Long,List<HLAPojo>>>();
		HLAPojo hlaPojo;
		Map<Long,List<HLAPojo>> bHlaMap;
		List<HLAPojo> hlas;
		//fw = new FileWriter(file.getAbsoluteFile());
		//bw = new BufferedWriter(fw,1048576);
		
		while( rs.next())  
        {
				hlaPojo = new HLAPojo();
			
			
					hlaPojo.setPkHla(rs.getLong(1));				
					hlaPojo.setFkHlaCodeId(rs.getLong(2));				
					hlaPojo.setFkHlaMethodId(rs.getLong(3));				
					hlaPojo.setHlaRecievedDate(rs.getDate(4)!=null? format.parse(rs.getDate(4).toString()):null);				
					hlaPojo.setHlaTypingDate(rs.getDate(5)!=null? format.parse(rs.getDate(5).toString()):null);				
					hlaPojo.setFkentityCordId(rs.getLong(6));				
					hlaPojo.setFkEntityCodeType(rs.getLong(7));				
					hlaPojo.setHlaType1(rs.getString(8));				
					hlaPojo.setHlaType2(rs.getString(9));				
					hlaPojo.setFkSource(rs.getLong(10));			
					hlaPojo.setuName(rs.getString(11));			
					hlaPojo.setCreatedOn(rs.getDate(12)!=null? format.parse(rs.getDate(12).toString()):null);			
					hlaPojo.setHlaOrderSeq(rs.getLong(13));
					long lastModUsrId=rs.getLong(14);
					//getLastModifiedUser(lastModUsrId,conn);
					hlaPojo.setLastModifiedByUser(getLastModifiedUser(lastModUsrId,conn));
				
				if(cbuHlas.containsKey(hlaPojo.getFkentityCordId())){
					//bw.write(hlaPojo.toString());
					//bw.write("\n");
					bHlaMap = cbuHlas.get(hlaPojo.getFkentityCordId());								
					if(bHlaMap!=null){
					    Long hlaOrderSeq = hlaPojo.getHlaOrderSeq();
						if(bHlaMap.containsKey(hlaOrderSeq)){
							    hlas = bHlaMap.get(hlaOrderSeq);
							    hlas.add(hlaPojo);
							}else{
								hlas = new ArrayList<HLAPojo>();
								hlas.add(hlaPojo);
								if(hlaOrderSeq!=null){
								  bHlaMap.put(hlaOrderSeq, hlas);
								}
						}
					}
					cbuHlas.put(hlaPojo.getFkentityCordId(), bHlaMap);
					
				}else{
					
					//bw.write("*****************************\n");
					bHlaMap = new LinkedHashMap<Long, List<HLAPojo>>();
					hlas = new ArrayList<HLAPojo>();
					hlas.add(hlaPojo);
					//bw.write(hlaPojo.toString());
					//bw.write("\n");
					bHlaMap.put(hlaPojo.getHlaOrderSeq(), hlas);
					cbuHlas.put(hlaPojo.getFkentityCordId(), bHlaMap);
					
				}
			
        	 
        }
		
		//bw.close();
		return cbuHlas;
	}
	
	/*private enum HLA {
		A, B, C, DRB1, DRB3, DRB4, DRB5, DQB1, DPB1, EXCLUDE
	}*/
	
	private Map<Long, Map<Long, HLAFilterPojo>> prepFilterHlaObj(ResultSet rs, Map<Long, String> codelst) throws SQLException{
		
		Map<Long, Map<Long, HLAFilterPojo>> cbuHlas = new HashMap<Long, Map<Long, HLAFilterPojo>>();
		HLAFilterPojo hfp =null;	
		Map<Long,HLAFilterPojo> bHlaMap;
		    Long entityId;
		    Long seq;
			Long codeId;
			String codeType = null;
		
			while( rs.next())  
	        {
				
				entityId = rs.getLong(6);
				seq = rs.getLong(13);
				codeId = rs.getLong(2);
				
				if(cbuHlas.containsKey(entityId)){
					
					bHlaMap = cbuHlas.get(entityId);
					
					hfp = bHlaMap.get(seq);
					
					if(hfp == null){
						hfp = new HLAFilterPojo();
						bHlaMap.put(seq, hfp);
						hfp.receivedDate = rs.getDate(4) /*!= null ? format.parse(rs.getDate(4).toString()) : null*/;
						hfp.typingDate = rs.getDate(5)/* != null ? format.parse(rs.getDate(5).toString()) : null*/;
						hfp.user = rs.getString(11);
					}
					//hfp = hfp == null ? new HLAFilterPojo(): hfp;
					
				}else{
					
					bHlaMap = new HashMap<Long, HLAFilterPojo>();
					hfp = new HLAFilterPojo();
					
					bHlaMap.put(seq, hfp);
					cbuHlas.put(entityId, bHlaMap);
					
					hfp.receivedDate = rs.getDate(4) /*!= null ? format.parse(rs.getDate(4).toString()) : null*/;
					hfp.typingDate = rs.getDate(5)/* != null ? format.parse(rs.getDate(5).toString()) : null*/;
					hfp.user = rs.getString(11);
					
				}
				
				codeType = codelst.get(codeId);
				codeType = codeType == null ? "EXCLUDE" : codeType;

				switch (HLA.valueOf(codeType)) {

				case A:
					hfp.aType1 = rs.getString(8);
					hfp.aType2 = rs.getString(9);
					break;

				case B:
					hfp.bType1 = rs.getString(8);
					hfp.bType2 = rs.getString(9);
					break;

				case C:
					hfp.cType1 = rs.getString(8);
					hfp.cType2 = rs.getString(9);
					break;

				case DRB1:
					hfp.drb1Type1 = rs.getString(8);
					hfp.drb1Type2 = rs.getString(9);
					break;

				case DRB3:
					hfp.drb3Type1 = rs.getString(8);
					hfp.drb3Type2 = rs.getString(9);
					break;

				case DRB4:
					hfp.drb4Type1 = rs.getString(8);
					hfp.drb4Type2 = rs.getString(9);
					break;

				case DRB5:
					hfp.drb5Type1 = rs.getString(8);
					hfp.drb5Type2 = rs.getString(9);
					break;

				case DQB1:
					hfp.dqb1Type1 = rs.getString(8);
					hfp.dqb1Type2 = rs.getString(9);
					break;

				case DPB1:
					hfp.dpb1Type1 = rs.getString(8);
					hfp.dpb1Type2 = rs.getString(9);
					break;

				case EXCLUDE:
					break;

				}
				
				
	        }
			
			return cbuHlas;
	}
	
	public Map<Long, Map<Long, List<HLAPojo>>> getCbuHlas() {
		return cbuHlas;
	}
	
	public Map<Long, Map<Long, HLAFilterPojo>> getCbuHla() {
		return cbuHla;
	}	

	
}
