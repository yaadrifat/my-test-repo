package com.velos.ordercomponent.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.velos.ordercomponent.business.domain.Page;
import com.velos.ordercomponent.business.domain.Theme;
import com.velos.ordercomponent.business.domain.Widget;
import com.velos.ordercomponent.business.domain.WidgetInstance;
import com.velos.ordercomponent.business.util.HibernateUtil;

public class WidgetHelper {

	public static final Log log = LogFactory.getLog(WidgetHelper.class);

	public static List<Page> getWidgetInfo() {
		return getWidgetInfo(null);
	}

	public static List<Page> getWidgetInfo(Session session)
	{
		//log.debug("getWidgetInfo method start in WidgetHelper");
		List<Page> pages = new ArrayList<Page>();
		Page page = null;
		List<Object> objects = null;
		Widget widget = null;
		boolean sessionFlag = false;
		try{
		 // check session 
		if(session== null){
			// create new session
		  session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}
		//String sql = "select {wdge.*},{widg.*} from garuda_widget widg left join garuda_widget_instance wdge on widg.pk_widget_id=wdge.pk_widget_instance";
		String sql = "select {p.*},{wg.*} from ui_page p inner join ui_widget_container cn on p.pk_page=cn.fk_page_id inner join UI_CONTAINER_WIDGET_MAP wd ON cn.pk_container=wd.fk_container_id inner join  UI_WIDGET wg ON wd.fk_widget_id = wg.pk_widget";
		SQLQuery query = session.createSQLQuery(sql);
		query.addEntity("p", Page.class);
		query.addEntity("wg", Widget.class);
		objects = query.list();
		//System.out.print("List size in widget helper"+((List)objects).size());
		pages.clear();
		for(Object o : objects)
		{
			Object[] obj = (Object[]) o;
			page = new Page();
			widget = new Widget();
			if(obj!=null)
			{
				page = (Page)obj[0];
				widget = (Widget)obj[1] ;					
				page.setWidget(widget);
				pages.add(page);
			}
		}
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		for(Page p : pages)
		{
		//System.out.print("List size in widget helper size"+p.getWidget().getWidgetDivId());
		}
		return pages;
		
	}
	
	public static Map<Long,List<Widget>> getWidgetInfoDetailsByPageCode(Session session) {
		//log.debug("getWidgetInfo method start in WidgetHelper");
		Page page = null;
		List<Object> objects = null;
		Widget widget = null;
		List<Widget> widgets = null;
		Map<Long,List<Widget>> widgetMap = new HashMap<Long, List<Widget>>();
		boolean sessionFlag = false;
		try {
			// check session
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			// String sql =
			// "select {wdge.*},{widg.*} from garuda_widget widg left join garuda_widget_instance wdge on widg.pk_widget_id=wdge.pk_widget_instance";
			String sql = "select {p.*},{wg.*} from ui_page p inner join ui_widget_container cn on p.pk_page=cn.fk_page_id inner join UI_CONTAINER_WIDGET_MAP wd ON cn.pk_container=wd.fk_container_id inner join  ui_widget wg ON wd.fk_widget_id = wg.pk_widget";
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity("p", Page.class);
			query.addEntity("wg", Widget.class);
			objects = query.list();
			for (Object o : objects) {
				Object[] obj = (Object[]) o;
				page = new Page();
				widget = new Widget();
				
				if (obj != null) {
					page = (Page) obj[0];
					widget = (Widget) obj[1];
					if(widgetMap.containsKey(page.getPageCode()))
					{
						widgets = widgetMap.get(page.getPageCode());
						widgets.add(widget);
					}else{
						widgets = new ArrayList<Widget>();
						widgets.add(widget);
						widgetMap.put(page.getPageCode(), widgets);
					}				

				}
			}
			} catch (Exception e) {
				log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if(session.isOpen())
			{
			session.close();
			}
		}
		for(Map.Entry<Long,List<Widget>> m : widgetMap.entrySet()){
			//System.out.println("Map Info"+m.getKey());
			for(Widget w : (List<Widget>)m.getValue()){
				//System.out.println("div value"+w.getWidgetDivId());
			}
		}
		return widgetMap;

	}
	
	public static List<Theme> getThemes(Long themeId)
	{
		return getThemes(themeId, null);
	}
	
	public static List<Theme> getThemes(Long themeId,Session session)
	{
		//log.debug("getThemes method start in WidgetHelper");
		Theme theme = null;
		List<Theme> themes = null;
		boolean sessionFlag = false;
		try {
			// check session
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "select t.* from garuda_themes t ";
			if(themeId!=null){
				sql += " where t.pk_theme_id=? ";
			}
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity("t", Theme.class);
			if(themeId!=null)
			{
				query.setParameter(1, themeId);
			}
			themes = (List<Theme>)query.list();
			//System.out.print("Themes "+themes.size());
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return themes;
		
	}

}
