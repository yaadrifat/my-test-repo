package com.velos.ordercomponent.business.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.opensymphony.xwork2.ActionContext;
import com.velos.eres.business.common.FormLibDao;
import com.velos.eres.web.linkedForms.LinkedFormsJB;
import com.velos.ordercomponent.helper.EresFormsHelper;

public class EresFormsBusiness {
	
	public static final Log log=LogFactory.getLog(EresFormsBusiness.class);
	//ActionContext ac = ActionContext.getContext();
	//ServletContext c = ServletActionContext.getServletContext();
	//HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);

	public int getFormId(String formName) {
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
		.newInstance();
		DocumentBuilder docBuilder;
		int formId = 0;
		try {
			docBuilder = docBuilderFactory.newDocumentBuilder();
			try {
				Document doc = docBuilder.parse(new File("forms.xml"));
				doc.getDocumentElement().normalize();
				NodeList forms = doc.getElementsByTagName("forms");
				for (int s = 0; s < forms.getLength(); s++) {

					Node formsNode = forms.item(s);
					if (formsNode.getNodeType() == Node.ELEMENT_NODE) {

						Element dcmsElement = (Element) formsNode;

						NodeList formIdList = dcmsElement
						.getElementsByTagName(formName);
						Element formIdElement = (Element) formIdList.item(0);

						NodeList textFNList = formIdElement.getChildNodes();
						formId = Integer.parseInt(((Node) textFNList.item(0))
								.getNodeValue().trim());
					}
				}
				return formId;
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (ParserConfigurationException e1) {
			e1.printStackTrace();
		}
		return formId;
	}
	
	 public String getFormHtml(int formId, String specimenPk, Boolean showSubmit){
			String findStr = "";
			StringBuffer sbuffer = null;
			int len = 0;
			int pos = 0;

			String mode = "N";

			LinkedFormsJB link = new LinkedFormsJB();
			String formHtml = link.getFormHtmlforPreview(formId);
			findStr = "</Form>";
			len = findStr.length();
			pos = formHtml.lastIndexOf(findStr);
			sbuffer = new StringBuffer(formHtml);
			StringBuffer paramBuffer = new StringBuffer();

			String chkexists = "";
			String tochk = "type=\"checkbox\"";
			if (formHtml.indexOf(tochk) == -1) {
				chkexists = "N";
			} else {
				chkexists = "Y";
			}

			String formDispLocation = "A";

			paramBuffer
			.append("<input type=\"hidden\" name=\"formDispLocation\" value='"
					+ formDispLocation + "'/>");
			paramBuffer
			.append("<input type=\"hidden\" name=\"formFillMode\" value='"
					+ mode + "'/>");
			paramBuffer
			.append("<input type=\"hidden\" id=\"formId\" name=\"formId\" value='"
					+ formId + "'/>");
			paramBuffer.append("<input type=\"hidden\" name=\"specimenPk\" value='"
					+ specimenPk + "'/>");
			paramBuffer
			.append("<input type=\"hidden\" name=\"cord\" value='"
					+ 1 + "'/>");

			sbuffer
			.replace(
					pos,
					pos + len,
					"<input type=\"hidden\" name=\"mode\" value='"
					+ mode
					+ "'/> <input type=\"hidden\" name=\"chkexists\" value='"
					+ chkexists + "'/>" + paramBuffer.toString()
					+ " </Form>");
			formHtml = sbuffer.toString();
			if(!showSubmit){
				formHtml = formHtml.replaceAll("if \\(  ! \\(validateFormESign","//if (  ! (validateFormESign");
				formHtml = formHtml.replaceAll("<td id=\"eSignLabel\"","<!--td id=\"eSignLabel\"");
				formHtml = formHtml.replaceAll("Submit.gif\" type=\"image\"></td>","Submit.gif\" type=\"image\"></td-->");
			}
			return formHtml;
		}


		public String getEditFormHtml(int formId, Long cordId ,Boolean showSubmit) throws Exception {
			String findStr = "";
			StringBuffer sbuffer = null;
			List list = new ArrayList();
			LinkedFormsJB link = new LinkedFormsJB();			
			int len = 0;
			int pos = 0;
			String mode = "M";
			String formHtml = "";
			list = EresFormsHelper.getEresFormHtml(Integer.valueOf(formId).longValue() , cordId);
			if (list != null && list.size()>0) {
				formHtml = link.getFilledFormHtml(Integer.parseInt(""
						+ list.get(0)), "A");
			}
			
			/*if (list.get(0) != null) {
			formHtml = link.getFilledFormHtml(Integer.parseInt(""
					+ list.get(0)), "A");
			} else {
				return formHtml;
			}*/
			

			findStr = "</Form>";
			len = findStr.length();
			pos = formHtml.lastIndexOf(findStr);
			sbuffer = new StringBuffer(formHtml);
			StringBuffer paramBuffer = new StringBuffer();

			String chkexists = "";
			String tochk = "type=\"checkbox\"";
			if (formHtml.indexOf(tochk) == -1) {
				chkexists = "N";
			} else {
				chkexists = "Y";
			}

			String formDispLocation = "A";

			paramBuffer
			.append("<input type=\"hidden\" name=\"formDispLocation\" value='"
					+ formDispLocation + "'/>");
			paramBuffer
			.append("<input type=\"hidden\" name=\"formFillMode\" value='"
					+ mode + "'/>");
			paramBuffer
			.append("<input type=\"hidden\" id=\"formId\" name=\"formId\" value='"
					+ formId + "'/>");
			paramBuffer
			.append("<input type=\"hidden\" name=\"cord\" value='"
					+ 1 + "'/>");

			FormLibDao fd = new FormLibDao();

			int formLibVer = Integer.parseInt(fd
					.getLatestFormLibVersionPK(formId));

			if (!mode.equals("N")) {
				if(list.get(0)!= null) {
					paramBuffer
					.append("<input type=\"hidden\" name=\"formFilledFormId\" value='"
							+ Integer.parseInt("" + list.get(0))
							+ "'/>");
				}
				paramBuffer
				.append("<input type=\"hidden\" name=\"formLibVer\" value='"
						+ formLibVer + "'/>");
			}
			
			/*if (!mode.equals("N")) {
				if(list.get(0)!= null) {
					paramBuffer
					.append("<input type=\"hidden\" name=\"formFilledFormId\" value='"
							+ Integer.parseInt("" + list.get(0))
							+ "'/>");
				}
				paramBuffer
				.append("<input type=\"hidden\" name=\"formLibVer\" value='"
						+ formLibVer + "'/>");
			}*/
			

			sbuffer
			.replace(
					pos,
					pos + len,
					"<input type=\"hidden\" name=\"mode\" value='"
					+ mode
					+ "'/> <input type=\"hidden\" name=\"chkexists\" value='"
					+ chkexists + "'/>"
					+ paramBuffer.toString() + " </Form>");
			formHtml = sbuffer.toString();
			if(!showSubmit){
				formHtml = formHtml.replaceAll("if \\(  ! \\(validateFormESign","//if (  ! (validateFormESign");
				formHtml = formHtml.replaceAll("<td id=\"eSignLabel\"","<!--td id=\"eSignLabel\"");
				formHtml = formHtml.replaceAll("Submit.gif\" type=\"image\"></td>","Submit.gif\" type=\"image\"></td-->");
		    }
			return formHtml;
		}
		
		public String getForm(String formCode, Boolean showSubmit, Long specimenPk){
			String formHtml = "";
			int formId =0;
			formId = getFormId(formCode);
			if(specimenPk!=null)
			{
			  formHtml = getFormHtml(formId,specimenPk.toString(),showSubmit);
			}else{
			  formHtml = getFormHtml(formId,null,showSubmit);
			}
			return formHtml;
		}
		
		public String getEditForm(String formCode, Boolean showSubmit, Long cordId) throws Exception{
			String formHtml = "";
			int formId = 0;
			formId = getFormId(formCode);
			formHtml = getEditFormHtml(formId, cordId, showSubmit);
			return formHtml;
		}
		
}
