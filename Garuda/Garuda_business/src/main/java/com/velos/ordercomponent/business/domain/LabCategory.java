package com.velos.ordercomponent.business.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
/**
 *@author Jyoti
 *
 */
@Entity
@Table(name="VELOS_LAB_CATEGORY")
@SequenceGenerator(sequenceName="SQ_VELOS_LAB_CATEGORY",name="SQ_VELOS_LAB_CATEGORY",allocationSize=1)

public class LabCategory extends Auditable {
	private Long pk_CategoryId;
	private String categoryName;
	private Long categoryType;
	private Boolean deletedFlag;
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SQ_VELOS_LAB_CATEGORY")
	@Column(name="PK_CATEGORYID")
	public Long getPk_CategoryId() {
		return pk_CategoryId;
	}
	public void setPk_CategoryId(Long pk_CategoryId) {
		this.pk_CategoryId = pk_CategoryId;
	}
	@Column(name="CATEGORY_NAME")
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	@Column(name="CATEGORY_TYPE")
	public Long getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(Long categoryType) {
		this.categoryType = categoryType;
	}
	
	@Column(name="DELETEDFLAG")
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	
}
