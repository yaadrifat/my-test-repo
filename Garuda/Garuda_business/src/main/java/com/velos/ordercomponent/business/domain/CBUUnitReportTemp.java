package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CBU_UNIT_REPORT_REVIEW")
@SequenceGenerator(sequenceName="SEQ_CBU_UNIT_REPORT_REVIEW",name="SEQ_CBU_UNIT_REPORT_REVIEW",allocationSize=1)
public class CBUUnitReportTemp extends Auditable  {

	
		
	private Long fkCordCdrCbuId;

	private Long cordProcMethod;
	private Long cordTypeBag;
	private Long cordProdModification;
	private Long cordHemogScreen;
	private Long cordBacterialCul;
	private Long cordFungalCul;
	private String cordExplainNotes;
	private String cordAddNotes;
	private Long pkCordExtInfoTemp;
	private Long cordCompletedStatus;
	private String cordCompletedBy; 
	private Long cordViabPostProcess; 
	private Long cordViabMethod; 
	private Date cordViabPostTestDate; 
	private Boolean cordViabPostStatus;
	private Boolean cordViabPostQues1; 
	private Boolean cordViabPostQues2;
	private Long cordCfuPostProcessCount; 
	private Long cordCfuPostMethod; 
	private String cordCfuPostStatus; 
	private Date cordCfuPostTestDate; 
	private Boolean cordCfuPostQues1; 
	private Boolean cordCfuPostQues2;
	private Boolean cordCbuPostQues1; 
	private Boolean cordCbuPostQues2; 
	private Date cordCbuPostTestDate; 
	private Long cordCbuPostProcessCount; 
	private Boolean cordCbuPostStatus;	
	private Long cordSerialNumber;
	private Long attachmentId;
	private Long fkCordExtInfo;
	private Boolean deletedFlag;
	

	
	/*private Auditable auditable;*/
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CBU_UNIT_REPORT_REVIEW")
	@Column(name="PK_CBU_UNIT_REPORT_REVIEW")
	public Long getPkCordExtInfoTemp() {
		return pkCordExtInfoTemp;
	}
	public void setPkCordExtInfoTemp(Long pkCordExtInfoTemp) {
		this.pkCordExtInfoTemp = pkCordExtInfoTemp;
	}	
	
	@Column(name="FK_CORD_CDR_CBU_ID")
	public Long getFkCordCdrCbuId() {
		return fkCordCdrCbuId;
	}
	public void setFkCordCdrCbuId(Long fkCordCdrCbuId) {
		this.fkCordCdrCbuId = fkCordCdrCbuId;
	}
	@Column(name="FK_CORD_PROC_METHOD")
	public Long getCordProcMethod() {
		return cordProcMethod;
	}
	public void setCordProcMethod(Long cordProcMethod) {
		this.cordProcMethod = cordProcMethod;
	}
	@Column(name="FK_CORD_TYPE_BAG")
	public Long getCordTypeBag() {
		return cordTypeBag;
	}
	public void setCordTypeBag(Long cordTypeBag) {
		this.cordTypeBag = cordTypeBag;
	}
	@Column(name="FK_CORD_PROD_MODIFICATION")
	public Long getCordProdModification() {
		return cordProdModification;
	}
	public void setCordProdModification(Long cordProdModification) {
		this.cordProdModification = cordProdModification;
	}
	@Column(name="FK_CORD_HEMOG_SCREEN")
	public Long getCordHemogScreen() {
		return cordHemogScreen;
	}
	public void setCordHemogScreen(Long cordHemogScreen) {
		this.cordHemogScreen = cordHemogScreen;
	}
	@Column(name="CORD_BACTERIAL_CUL")
	public Long getCordBacterialCul() {
		return cordBacterialCul;
	}
	public void setCordBacterialCul(Long cordBacterialCul) {
		this.cordBacterialCul = cordBacterialCul;
	}
	@Column(name="CORD_FUNGAL_CUL")
	public Long getCordFungalCul() {
		return cordFungalCul;
	}
	public void setCordFungalCul(Long cordFungalCul) {
		this.cordFungalCul = cordFungalCul;
	}
	@Column(name="CORD_EXPLAIN_NOTES")
	public String getCordExplainNotes() {
		return cordExplainNotes;
	}
	public void setCordExplainNotes(String cordExplainNotes) {
		this.cordExplainNotes = cordExplainNotes;
	}
	@Column(name="CORD_ADD_NOTES")
	public String getCordAddNotes() {
		return cordAddNotes;
	}
	public void setCordAddNotes(String cordAddNotes) {
		this.cordAddNotes = cordAddNotes;
	}
	@Column(name="CORD_COMPLETED_BY")
	public String getCordCompletedBy() {
		return cordCompletedBy;
	}
	public void setCordCompletedBy(String cordCompletedBy) {
		this.cordCompletedBy = cordCompletedBy;
	}	
	
	@Column(name="CORD_COMPLETED_STATUS")
	public Long getCordCompletedStatus() {
		return cordCompletedStatus;
	}
	public void setCordCompletedStatus(Long  cordCompletedStatus) {
		this.cordCompletedStatus = cordCompletedStatus;
	}	
	
	@Column(name="CORD_VIAB_POST_PROCESS")
	public Long getCordViabPostProcess() {
		return cordViabPostProcess;
	}
	public void setCordViabPostProcess(Long cordViabPostProcess) {
		this.cordViabPostProcess = cordViabPostProcess;
	}
	@Column(name="CORD_VIAB_METHOD")
	public Long getCordViabMethod() {
		return cordViabMethod;
	}
	public void setCordViabMethod(Long cordViabMethod) {
		this.cordViabMethod = cordViabMethod;
	}
	@Column(name="CORD_VIAB_POST_TEST_DATE")
	public Date getCordViabPostTestDate() {
		return cordViabPostTestDate;
	}
	public void setCordViabPostTestDate(Date cordViabPostTestDate) {
		this.cordViabPostTestDate = cordViabPostTestDate;
	}
	@Column(name="CORD_VIAB_POST_STATUS")
	public Boolean getCordViabPostStatus() {
		return cordViabPostStatus;
	}
	public void setCordViabPostStatus(Boolean cordViabPostStatus) {
		this.cordViabPostStatus = cordViabPostStatus;
	}
	@Column(name="CORD_VIAB_POST_QUES1")
	public Boolean getCordViabPostQues1() {
		return cordViabPostQues1;
	}
	public void setCordViabPostQues1(Boolean cordViabPostQues1) {
		this.cordViabPostQues1 = cordViabPostQues1;
	}
	@Column(name="CORD_VIAB_POST_QUES2")
	public Boolean getCordViabPostQues2() {
		return cordViabPostQues2;
	}
	public void setCordViabPostQues2(Boolean cordViabPostQues2) {
		this.cordViabPostQues2 = cordViabPostQues2;
	}
	@Column(name="CORD_CFU_POST_PROCESS_COUNT")
	public Long getCordCfuPostProcessCount() {
		return cordCfuPostProcessCount;
	}
	public void setCordCfuPostProcessCount(Long cordCfuPostProcessCount) {
		this.cordCfuPostProcessCount = cordCfuPostProcessCount;
	}
	@Column(name="CORD_CFU_POST_METHOD")
	public Long getCordCfuPostMethod() {
		return cordCfuPostMethod;
	}
	public void setCordCfuPostMethod(Long cordCfuPostMethod) {
		this.cordCfuPostMethod = cordCfuPostMethod;
	}
	@Column(name="CORD_CFU_POST_STATUS")
	public String getCordCfuPostStatus() {
		return cordCfuPostStatus;
	}
	public void setCordCfuPostStatus(String cordCfuPostStatus) {
		this.cordCfuPostStatus = cordCfuPostStatus;
	}
	@Column(name="CORD_CFU_POST_TEST_DATE")
	public Date getCordCfuPostTestDate() {
		return cordCfuPostTestDate;
	}
	public void setCordCfuPostTestDate(Date cordCfuPostTestDate) {
		this.cordCfuPostTestDate = cordCfuPostTestDate;
	}
	@Column(name="CORD_CFU_POST_QUES1")
	public Boolean getCordCfuPostQues1() {
		return cordCfuPostQues1;
	}
	public void setCordCfuPostQues1(Boolean cordCfuPostQues1) {
		this.cordCfuPostQues1 = cordCfuPostQues1;
	}
	@Column(name="CORD_CFU_POST_QUES2")
	public Boolean getCordCfuPostQues2() {
		return cordCfuPostQues2;
	}
	public void setCordCfuPostQues2(Boolean cordCfuPostQues2) {
		this.cordCfuPostQues2 = cordCfuPostQues2;
	}
	@Column(name="CORD_CBU_POST_QUES1")
	public Boolean getCordCbuPostQues1() {
		return cordCbuPostQues1;
	}
	public void setCordCbuPostQues1(Boolean cordCbuPostQues1) {
		this.cordCbuPostQues1 = cordCbuPostQues1;
	}
	@Column(name="CORD_CBU_POST_QUES2")
	public Boolean getCordCbuPostQues2() {
		return cordCbuPostQues2;
	}
	public void setCordCbuPostQues2(Boolean cordCbuPostQues2) {
		this.cordCbuPostQues2 = cordCbuPostQues2;
	}
	@Column(name="CORD_CBU_POST_TEST_DATE")
	public Date getCordCbuPostTestDate() {
		return cordCbuPostTestDate;
	}
	public void setCordCbuPostTestDate(Date cordCbuPostTestDate) {
		this.cordCbuPostTestDate = cordCbuPostTestDate;
	}
	@Column(name="CORD_CBU_POST_PROCESS_COUNT")
	public Long getCordCbuPostProcessCount() {
		return cordCbuPostProcessCount;
	}
	public void setCordCbuPostProcessCount(Long cordCbuPostProcessCount) {
		this.cordCbuPostProcessCount = cordCbuPostProcessCount;
	}
	@Column(name="CORD_CBU_POST_STATUS")
	public Boolean getCordCbuPostStatus() {
		return cordCbuPostStatus;
	}
	public void setCordCbuPostStatus(Boolean cordCbuPostStatus) {
		this.cordCbuPostStatus = cordCbuPostStatus;
	}
	
	
	/*@Embedded
	public Auditable getAuditable() {
		return auditable;
	}
	public void setAuditable(Auditable auditable) {
		this.auditable = auditable;
	}*/
	
	@Column(name="deletedflag")
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	
	@Column(name="CORD_SERIAL")
	public Long getCordSerialNumber() {
		return cordSerialNumber;
	}
	public void setCordSerialNumber(Long cordSerialNumber) {
		this.cordSerialNumber = cordSerialNumber;
	}
	
	@Column(name="FK_ATTACHMENT_ID")
	public Long getAttachmentId() {
		return attachmentId;
	}
	public void setAttachmentId(Long attachmentId) {
		this.attachmentId = attachmentId;
	}
	
	@Column(name="FK_CORD_EXT_INFO")	
	public Long getFkCordExtInfo() {
		return fkCordExtInfo;
	}
	public void setFkCordExtInfo(Long fkCordExtInfo) {
		this.fkCordExtInfo = fkCordExtInfo;
	}
	
	
	
}