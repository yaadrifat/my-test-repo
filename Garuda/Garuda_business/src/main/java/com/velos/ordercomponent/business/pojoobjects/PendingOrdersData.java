package com.velos.ordercomponent.business.pojoobjects;

public class PendingOrdersData {
	private String subType;
	private String description;
	
	public String getSubType() {
		return subType;
	}
	public void setSubType(String subType) {
		this.subType = subType;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	

}
