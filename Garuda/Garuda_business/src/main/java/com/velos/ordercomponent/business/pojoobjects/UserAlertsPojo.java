package com.velos.ordercomponent.business.pojoobjects;

public class UserAlertsPojo {
	
	private Long pkUserAlerts;
	private Long fkUserId;
	private Long fkCodelstId;
	private Boolean deletedFlag;
	public Boolean emailFlag;
	public Boolean notifyFlag;
	public Boolean promptFlag;
	public Boolean ctlabFlag;
	public Boolean ctshipFlag;
	public Boolean heFlag;
	public Boolean orFlag;
	
	
	
	
	
	public Boolean getCtlabFlag() {
		return ctlabFlag;
	}
	public void setCtlabFlag(Boolean ctlabFlag) {
		this.ctlabFlag = ctlabFlag;
	}
	public Boolean getCtshipFlag() {
		return ctshipFlag;
	}
	public void setCtshipFlag(Boolean ctshipFlag) {
		this.ctshipFlag = ctshipFlag;
	}
	public Boolean getHeFlag() {
		return heFlag;
	}
	public void setHeFlag(Boolean heFlag) {
		this.heFlag = heFlag;
	}
	public Boolean getOrFlag() {
		return orFlag;
	}
	public void setOrFlag(Boolean orFlag) {
		this.orFlag = orFlag;
	}
	public Long getPkUserAlerts() {
		return pkUserAlerts;
	}
	public void setPkUserAlerts(Long pkUserAlerts) {
		this.pkUserAlerts = pkUserAlerts;
	}
	public Long getFkUserId() {
		return fkUserId;
	}
	public void setFkUserId(Long fkUserId) {
		this.fkUserId = fkUserId;
	}
	public Long getFkCodelstId() {
		return fkCodelstId;
	}
	public void setFkCodelstId(Long fkCodelstId) {
		this.fkCodelstId = fkCodelstId;
	}
	
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	public Boolean getEmailFlag() {
		return emailFlag;
	}
	public void setEmailFlag(Boolean emailFlag) {
		this.emailFlag = emailFlag;
	}
	public Boolean getNotifyFlag() {
		return notifyFlag;
	}
	public void setNotifyFlag(Boolean notifyFlag) {
		this.notifyFlag = notifyFlag;
	}
	public Boolean getPromptFlag() {
		return promptFlag;
	}
	public void setPromptFlag(Boolean promptFlag) {
		this.promptFlag = promptFlag;
	}
	
	
}
