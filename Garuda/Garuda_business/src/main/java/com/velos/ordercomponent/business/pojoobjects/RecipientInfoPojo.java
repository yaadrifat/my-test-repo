package com.velos.ordercomponent.business.pojoobjects;


public class RecipientInfoPojo extends AuditablePojo{

	private Long pkRecipient;
	private String recipientId;
	private Long fkPerson;
	private String currDiagnosis;
	private Long fkTransCenterId;
	private String diseaseStage;
	private Long receipientWgt;
	private Long fkSecTransCenterId;
	
	public Long getPkRecipient() {
		return pkRecipient;
	}
	public void setPkRecipient(Long pkRecipient) {
		this.pkRecipient = pkRecipient;
	}
	public String getRecipientId() {
		return recipientId;
	}
	public void setRecipientId(String recipientId) {
		this.recipientId = recipientId;
	}
	public Long getFkPerson() {
		return fkPerson;
	}
	public void setFkPerson(Long fkPerson) {
		this.fkPerson = fkPerson;
	}
	public String getCurrDiagnosis() {
		return currDiagnosis;
	}
	public void setCurrDiagnosis(String currDiagnosis) {
		this.currDiagnosis = currDiagnosis;
	}
	public Long getFkTransCenterId() {
		return fkTransCenterId;
	}
	public void setFkTransCenterId(Long fkTransCenterId) {
		this.fkTransCenterId = fkTransCenterId;
	}
	public String getDiseaseStage() {
		return diseaseStage;
	}
	public void setDiseaseStage(String diseaseStage) {
		this.diseaseStage = diseaseStage;
	}
	
	public Long getReceipientWgt() {
		return receipientWgt;
	}
	public void setReceipientWgt(Long receipientWgt) {
		this.receipientWgt = receipientWgt;
	}
	public Long getFkSecTransCenterId() {
		return fkSecTransCenterId;
	}
	public void setFkSecTransCenterId(Long fkSecTransCenterId) {
		this.fkSecTransCenterId = fkSecTransCenterId;
	}
	
	
}
