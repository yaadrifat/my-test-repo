package com.velos.ordercomponent.service.impl;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

//import com.aithent.gems.objects.WorkFlowProcessObject;
//import com.aithent.gems.workflow.helper.WorkFlowProcessHelper;
import com.aithent.gems.objects.WorkFlowProcessObject;
import com.aithent.gems.workflow.helper.WorkFlowProcessHelper;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.FilterDataPojo;
import com.velos.ordercomponent.business.pojoobjects.HLAPojo;
import com.velos.ordercomponent.business.pojoobjects.OrderPojo;
import com.velos.ordercomponent.business.pojoobjects.PackingSlipPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.pojoobjects.ShipmentPojo;
import com.velos.ordercomponent.helper.OrderAttachmentHelper;
import com.velos.ordercomponent.service.PendingOrdersService;

public class PendingOrdersServiceImpl implements PendingOrdersService{
	
	public static PendingOrdersService pendingOrdersService;

	private PendingOrdersServiceImpl() {
		//System.out.println(" PendingOrdersServiceImpl  constructor invoked......");
	}

	public static PendingOrdersService getService() throws Exception {
		//System.out.println(" get Pending orders service ");
		try {
			if(pendingOrdersService == null){
				pendingOrdersService = (PendingOrdersService) new PendingOrdersServiceImpl();
			}
		} catch (Exception e) {
			//System.out.println(e);
			e.printStackTrace();
			throw new RuntimeException();
		}
		return pendingOrdersService;
	}
	
	public List getOrderDetails(String orderId){
		return OrderAttachmentHelper.getOrderDetails(orderId);
	}
	
	public List getCtOrderDetails(String orderId){
		return OrderAttachmentHelper.getCtOrderDetails(orderId);
	}
	
	public CdrCbuPojo submitCordStatus(CdrCbuPojo cdrCbuPojo){
		return OrderAttachmentHelper.submitCordStatus(cdrCbuPojo);
	}
	public List getResultsForCancelFilter(String orderType,String colName,String colData,String pkOrderType,String cancelled,String order_status){
		return OrderAttachmentHelper.getResultsForCancelFilter(orderType, colName, colData,pkOrderType,cancelled,order_status);
	}
	
	public List getAntigensForReceipant(String receipantId,String entityType){
		return OrderAttachmentHelper.getAntigensForReceipant(receipantId,entityType);
	}
	
	public Long getdefaultshipper(Long orderId,String orderType){
		return OrderAttachmentHelper.getdefaultshipper(orderId,orderType);
	}
	
	public ShipmentPojo saveShipmentInfo(ShipmentPojo shipmentPojo){
		return OrderAttachmentHelper.saveShipmentInfo(shipmentPojo);
	}
	
	public List getUserslistData(String siteId){
		return OrderAttachmentHelper.getUserslistGridData(siteId);
	}
	
	public List getLabelAssignLst(String siteId){
		return OrderAttachmentHelper.getUserslistLabel(siteId);
	}
	
	public OrderPojo updateOrderDetails(OrderPojo orderPojo){
		return OrderAttachmentHelper.updateOrderDetails(orderPojo);
	}
	
	public Long getCRI_CompletedFlag(Long pk_cord){
		return OrderAttachmentHelper.getCRI_CompletedFlag(pk_cord);
	}
	
	
	public ShipmentPojo getshipmentDetails(ShipmentPojo shipmentPojo){
		return OrderAttachmentHelper.getshipmentDetails(shipmentPojo);
	}
	
	public void autoCompleteCRI(Long pkcord){
		OrderAttachmentHelper.autoCompleteCRI(pkcord);
	}
	
	public void updateOrderLastReviewedBy(Long orderId,Long user_id){
		OrderAttachmentHelper.updateOrderLastReviewedBy(orderId, user_id);
	}
	
	
	public List getQBuilderList(String sql, String orderBy){
		return OrderAttachmentHelper.getQBuilderList(sql, orderBy);
	}
	
	public List<FilterDataPojo> getCustomFilterData(Long userId,Long module,Long submodule){
		return OrderAttachmentHelper.getCustomFilterData(userId,module,submodule);
	}
	
	public  HLAPojo saveCurrentHlaTyping(HLAPojo hlaPojo){
		return OrderAttachmentHelper.saveCurrentHlaTyping(hlaPojo);
	}
	
	public List getWorkFlowNotification(PaginateSearch paginateSearch,int i,Long userId,String siteId,String condition,String OrdByCondition){
		//System.out.println("pending order service impl");
		return OrderAttachmentHelper.getTasksList(paginateSearch,i,userId,siteId,condition,OrdByCondition);
	}
	
	public List getAlertNotifications(Long userId,String siteId,String criteria,String orderBy,PaginateSearch paginateSearch,int i){
		return OrderAttachmentHelper.getAlertNotifications(userId,siteId,criteria,orderBy,paginateSearch,i);
	}
	
	public List<FilterDataPojo> customFilterInsert(FilterDataPojo filterDataPojo){
		return OrderAttachmentHelper.customFilterInsert(filterDataPojo);
	}
	
	
	public Long getOrderType(Long orderId){
		return OrderAttachmentHelper.getOrderType(orderId);
	}
	
	public void createAlertInstances(Long orderId){
		OrderAttachmentHelper.createAlertInstance(orderId);
	}
	
	public List getCbbProfile(String siteId,Long userId){
		return OrderAttachmentHelper.getCbbProfile(siteId,userId);
	}
	
	public List getOrders(PaginateSearch paginateSearch,int i,Long userId,String siteId,String condition,String orderByCondition){
		return OrderAttachmentHelper.getOrders(paginateSearch,i,userId,siteId,condition,orderByCondition);
	}
	
	public List getTasksListForOrder(Long orderId){
		return OrderAttachmentHelper.getTasksListForOrder(orderId);
	}
	
	public Long getPkActivity(String activityname){
		return OrderAttachmentHelper.getPkActivity(activityname);
	}
	
	public List getCommonUsrLst(String site,String count){
		return OrderAttachmentHelper.getCommonUsrLst(site,count);
	}
	
	public List getShipmentInfo(Long orderId,Long shipmentType){
		//System.out.println("pending order service impl");
		return OrderAttachmentHelper.getShipmentInfo(orderId,shipmentType);
	}
	
	public List getLatestHlaInfo(Long orderId,Long entityId,String entityType){
		return OrderAttachmentHelper.getLatestHlaInfo(orderId, entityId,entityType);
	}
	
	public List getcordAvailConfirm(Long orderId){
		return OrderAttachmentHelper.getcordAvailConfirm(orderId);
	}
	
	public List getOrderResol(Long orderId){
		return OrderAttachmentHelper.getOrderResol(orderId);
	}
	
	public List fetchNextTask(WorkFlowProcessObject objWorkFlow){
		List lst=new ArrayList();
		//System.out.println("in pendingservice impl for workflow process:::::::::::::::::::::::::::::::::::::::::::");
		try {
			lst=(List) WorkFlowProcessHelper.fetchNextTask(objWorkFlow);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lst;
		
	}

	public List getallOrderDetails(PaginateSearch paginateSearch, int i,String filterflag,String ordertypeval,String fksiteId,Long userId,String condition,String orderBy) {
		return OrderAttachmentHelper.getallOrderDetails(paginateSearch,i,filterflag,ordertypeval,fksiteId,userId,condition,orderBy);
	}
	
	public int updateAlertStatus(Long alertInstanceId){
		return OrderAttachmentHelper.updateAlertStatus(alertInstanceId);
	}
	
	public List savedInprogress(PaginateSearch paginationSearch,String SIPCondition,Long userId,String fksiteId,String orderBy,int i){
		return OrderAttachmentHelper.savedInprogress(paginationSearch,SIPCondition,userId,fksiteId,orderBy,i);
	}
	
	public List notAvailCbu(PaginateSearch paginationSearch,String nacbuCondition,int i,String fksiteId,String nacbuOrderBy,Long userId){
		return OrderAttachmentHelper.notAvailCbu(paginationSearch,nacbuCondition,i,fksiteId,nacbuOrderBy,userId);
	}
	
	public String getClinicChkStatus(Long cordId){
		//System.out.println("");
		return OrderAttachmentHelper.getClinicChkStatus(cordId);
	}
	
	public List getfinalReviewStatus(Long cordId,Long orderId){
		return OrderAttachmentHelper.getfinalReviewStatus(cordId,orderId);
	}
	
	public List getpackingSlipDetails(Long pkPackingSlip){
		return OrderAttachmentHelper.getpackingSlipDetails(pkPackingSlip);
	}
	
	public String getPkCbuStatus(String orderType,String cbuStatusCode,String statusType){
		return OrderAttachmentHelper.getPkCbuStatus(orderType,cbuStatusCode, statusType);
	}
	
	public List getDataFromCodelstCustCol(String tableName,String columnName,String criteria){
		return OrderAttachmentHelper.getDataFromCodelstCustCol(tableName, columnName, criteria);
	}
	
	public void updateDataFromCodelstCustCol(String tableName,String columnName,Long value,String criteria,String operation){
		OrderAttachmentHelper.updateDataFromCodelstCustCol(tableName, columnName, value, criteria,operation);
	}
	
	
	public PackingSlipPojo updatePackingSlipDet(PackingSlipPojo packingSlipPojo){
		return OrderAttachmentHelper.updatePackingSlipDet(packingSlipPojo);
	}
	
	public void updateOrderCompleteReqInfoflag(Long cordId){
		OrderAttachmentHelper.updateOrderCompleteReqInfoflag(cordId);
	}
	
	public String updateAssignTo(String ordId,String assignto,String flagval,Long loggedIn_userId,String cordIdval){
		//System.out.println("test");
		return OrderAttachmentHelper.updateAssignTo(ordId, assignto, flagval,loggedIn_userId,cordIdval);
	}
	
	public List getPatientInfo(Long orderId){
		return OrderAttachmentHelper.getPatientInfo(orderId);
	}
	
	public String getMinimumCriteriaStat(Long orderId){
		return OrderAttachmentHelper.getMinimumCriteriaStat(orderId);
	}
	
	public String getMinimumCriteriaFlagByCordId(Long cordId){
		return OrderAttachmentHelper.getMinimumCriteriaFlagByCordId(cordId);
	}
	
	public String getNMDPResearchSample(Long siteId,Long cordId){
		return OrderAttachmentHelper.getNMDPResearchSampleFlag(siteId,cordId);
	}
	
	public Long getReceipientId(Long cordId){
		return OrderAttachmentHelper.getReceipientId(cordId);
	}
	
	public String getAcknowledgeCancelledOrderFlag(Long orderId,Long cordId){
		return OrderAttachmentHelper.getAcknowledgeCancelledOrderFlag(orderId, cordId);
	}
	
	public OrderPojo getRequestDetails(Long orderId){
		return OrderAttachmentHelper.getRequestDetails(orderId);
	}
	
	public Long getOrderIdFromCordId(Long cordId){
		return OrderAttachmentHelper.getOrderIdFromCordId(cordId);
	}
	
	public String getFlagForTaskCheck(Long cordId){
		return OrderAttachmentHelper.getFlagForTaskCheck(cordId);
	}
	
	public Long getDefaultPaperLoc(Long orderId){
		return OrderAttachmentHelper.getDefaultPaperLoc(orderId);
	}
}
