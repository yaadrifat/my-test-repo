package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class SpecimenPojo extends AuditablePojo {
	public Long pkSpecimen;
    public String specimenId;
    public String specAltId;
    public String specDesc;
    public Long specOrgQnty;
    public Long spectQuntyUnits;
    public Long  specType;
    public Long  fkStudy;
    public Date specCollDate;
    public Long fkPer;
    public Long specOwnerFkUser;
    public Long fkStorage;
    public Long fkSite;
    public Long fkVisit;
    public Long fkSchEvents;
    public String specNote;
    public Long fkSpec;
    public Long specStorageTmp;
    public Long specStorageTmpUnit;    
    public Long fkAccount;
    public Long specProcType;
    public Long fkStrgKitComp;
    public String specUserPathologist;
    public String specUserSurgeon;
    public String specUserCollectingTech;
    public String specUserProcessingTech;
    public Date specRemovalDatetime;
    public Date specFreezeDatetime;
    public Long specAnatomicSite;
    public Long specTissueSide;
    public Long specPathologyStat;
    public Long fkStorageId;
    public Long specExpectQuant;
    public Long specBaseOrigQuant;
    public Long specExpectedQUnit;
    public Long specBaseOrigQUnit;
    public Long specDisposition;
    public String specEnvtCons;
   
	public Long getPkSpecimen() {
		return pkSpecimen;
	}
	public void setPkSpecimen(Long pkSpecimen) {
		this.pkSpecimen = pkSpecimen;
	}
	public String getSpecimenId() {
		return specimenId;
	}
	public void setSpecimenId(String specimenId) {
		this.specimenId = specimenId;
	}
	public String getSpecAltId() {
		return specAltId;
	}
	public void setSpecAltId(String specAltId) {
		this.specAltId = specAltId;
	}
	public String getSpecDesc() {
		return specDesc;
	}
	public void setSpecDesc(String specDesc) {
		this.specDesc = specDesc;
	}
	public Long getSpecOrgQnty() {
		return specOrgQnty;
	}
	public void setSpecOrgQnty(Long specOrgQnty) {
		this.specOrgQnty = specOrgQnty;
	}
	public Long getSpectQuntyUnits() {
		return spectQuntyUnits;
	}
	public void setSpectQuntyUnits(Long spectQuntyUnits) {
		this.spectQuntyUnits = spectQuntyUnits;
	}
	public Long getSpecType() {
		return specType;
	}
	public void setSpecType(Long specType) {
		this.specType = specType;
	}
	public Long getFkStudy() {
		return fkStudy;
	}
	public void setFkStudy(Long fkStudy) {
		this.fkStudy = fkStudy;
	}
	public Date getSpecCollDate() {
		return specCollDate;
	}
	public void setSpecCollDate(Date specCollDate) {
		this.specCollDate = specCollDate;
	}
	public Long getFkPer() {
		return fkPer;
	}
	public void setFkPer(Long fkPer) {
		this.fkPer = fkPer;
	}
	public Long getSpecOwnerFkUser() {
		return specOwnerFkUser;
	}
	public void setSpecOwnerFkUser(Long specOwnerFkUser) {
		this.specOwnerFkUser = specOwnerFkUser;
	}
	public Long getFkStorage() {
		return fkStorage;
	}
	public void setFkStorage(Long fkStorage) {
		this.fkStorage = fkStorage;
	}
	public Long getFkSite() {
		return fkSite;
	}
	public void setFkSite(Long fkSite) {
		this.fkSite = fkSite;
	}
	public Long getFkVisit() {
		return fkVisit;
	}
	public void setFkVisit(Long fkVisit) {
		this.fkVisit = fkVisit;
	}
	public Long getFkSchEvents() {
		return fkSchEvents;
	}
	public void setFkSchEvents(Long fkSchEvents) {
		this.fkSchEvents = fkSchEvents;
	}
	public String getSpecNote() {
		return specNote;
	}
	public void setSpecNote(String specNote) {
		this.specNote = specNote;
	}
	public Long getFkSpec() {
		return fkSpec;
	}
	public void setFkSpec(Long fkSpec) {
		this.fkSpec = fkSpec;
	}
	public Long getSpecStorageTmp() {
		return specStorageTmp;
	}
	public void setSpecStorageTmp(Long specStorageTmp) {
		this.specStorageTmp = specStorageTmp;
	}
	public Long getSpecStorageTmpUnit() {
		return specStorageTmpUnit;
	}
	public void setSpecStorageTmpUnit(Long specStorageTmpUnit) {
		this.specStorageTmpUnit = specStorageTmpUnit;
	}
	public Long getFkAccount() {
		return fkAccount;
	}
	public void setFkAccount(Long fkAccount) {
		this.fkAccount = fkAccount;
	}
	public Long getSpecProcType() {
		return specProcType;
	}
	public void setSpecProcType(Long specProcType) {
		this.specProcType = specProcType;
	}
	public Long getFkStrgKitComp() {
		return fkStrgKitComp;
	}
	public void setFkStrgKitComp(Long fkStrgKitComp) {
		this.fkStrgKitComp = fkStrgKitComp;
	}
	public String getSpecUserPathologist() {
		return specUserPathologist;
	}
	public void setSpecUserPathologist(String specUserPathologist) {
		this.specUserPathologist = specUserPathologist;
	}
	public String getSpecUserSurgeon() {
		return specUserSurgeon;
	}
	public void setSpecUserSurgeon(String specUserSurgeon) {
		this.specUserSurgeon = specUserSurgeon;
	}
	public String getSpecUserCollectingTech() {
		return specUserCollectingTech;
	}
	public void setSpecUserCollectingTech(String specUserCollectingTech) {
		this.specUserCollectingTech = specUserCollectingTech;
	}
	public String getSpecUserProcessingTech() {
		return specUserProcessingTech;
	}
	public void setSpecUserProcessingTech(String specUserProcessingTech) {
		this.specUserProcessingTech = specUserProcessingTech;
	}
	public Date getSpecRemovalDatetime() {
		return specRemovalDatetime;
	}
	public void setSpecRemovalDatetime(Date specRemovalDatetime) {
		this.specRemovalDatetime = specRemovalDatetime;
	}
	public Date getSpecFreezeDatetime() {
		return specFreezeDatetime;
	}
	public void setSpecFreezeDatetime(Date specFreezeDatetime) {
		this.specFreezeDatetime = specFreezeDatetime;
	}
	public Long getSpecAnatomicSite() {
		return specAnatomicSite;
	}
	public void setSpecAnatomicSite(Long specAnatomicSite) {
		this.specAnatomicSite = specAnatomicSite;
	}
	public Long getSpecTissueSide() {
		return specTissueSide;
	}
	public void setSpecTissueSide(Long specTissueSide) {
		this.specTissueSide = specTissueSide;
	}
	public Long getSpecPathologyStat() {
		return specPathologyStat;
	}
	public void setSpecPathologyStat(Long specPathologyStat) {
		this.specPathologyStat = specPathologyStat;
	}
	public Long getFkStorageId() {
		return fkStorageId;
	}
	public void setFkStorageId(Long fkStorageId) {
		this.fkStorageId = fkStorageId;
	}
	public Long getSpecExpectQuant() {
		return specExpectQuant;
	}
	public void setSpecExpectQuant(Long specExpectQuant) {
		this.specExpectQuant = specExpectQuant;
	}
	public Long getSpecBaseOrigQuant() {
		return specBaseOrigQuant;
	}
	public void setSpecBaseOrigQuant(Long specBaseOrigQuant) {
		this.specBaseOrigQuant = specBaseOrigQuant;
	}
	public Long getSpecExpectedQUnit() {
		return specExpectedQUnit;
	}
	public void setSpecExpectedQUnit(Long specExpectedQUnit) {
		this.specExpectedQUnit = specExpectedQUnit;
	}
	public Long getSpecBaseOrigQUnit() {
		return specBaseOrigQUnit;
	}
	public void setSpecBaseOrigQUnit(Long specBaseOrigQUnit) {
		this.specBaseOrigQUnit = specBaseOrigQUnit;
	}
	public Long getSpecDisposition() {
		return specDisposition;
	}
	public void setSpecDisposition(Long specDisposition) {
		this.specDisposition = specDisposition;
	}
	public String getSpecEnvtCons() {
		return specEnvtCons;
	}
	public void setSpecEnvtCons(String specEnvtCons) {
		this.specEnvtCons = specEnvtCons;
	}
	
    
    

}
