package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

public class DataModificationRequestPojo {
	
	private Long pkdatarequest;
	private Long requesttype;
	private String title;
	private Date requestDuedate;
	private String dataModiDescription;
	private String requestReason;
	private String affectedFields;
	private String cbbProcessChange;
	private Long requestStatus;
	private Long creater;
	private Date createdon;
	private Long cbbcode;
	
	
	
	public Long getPkdatarequest() {
		return pkdatarequest;
	}
	public void setPkdatarequest(Long pkdatarequest) {
		this.pkdatarequest = pkdatarequest;
	}
	public Long getRequesttype() {
		return requesttype;
	}
	public void setRequesttype(Long requesttype) {
		this.requesttype = requesttype;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getRequestDuedate() {
		return requestDuedate;
	}
	public void setRequestDuedate(Date requestDuedate) {
		this.requestDuedate = requestDuedate;
	}
	public String getDataModiDescription() {
		return dataModiDescription;
	}
	public void setDataModiDescription(String dataModiDescription) {
		this.dataModiDescription = dataModiDescription;
	}
	public String getRequestReason() {
		return requestReason;
	}
	public void setRequestReason(String requestReason) {
		this.requestReason = requestReason;
	}
	public String getAffectedFields() {
		return affectedFields;
	}
	public void setAffectedFields(String affectedFields) {
		this.affectedFields = affectedFields;
	}
	public String getCbbProcessChange() {
		return cbbProcessChange;
	}
	public void setCbbProcessChange(String cbbProcessChange) {
		this.cbbProcessChange = cbbProcessChange;
	}
	public Long getRequestStatus() {
		return requestStatus;
	}
	public void setRequestStatus(Long requestStatus) {
		this.requestStatus = requestStatus;
	}
	public Long getCreater() {
		return creater;
	}
	public void setCreater(Long creater) {
		this.creater = creater;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public Long getCbbcode() {
		return cbbcode;
	}
	public void setCbbcode(Long cbbcode) {
		this.cbbcode = cbbcode;
	}
	

}
