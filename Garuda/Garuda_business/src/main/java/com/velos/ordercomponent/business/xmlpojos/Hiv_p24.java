package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


public class Hiv_p24 {
	private String hiv_antigen_rslt;
	private XMLGregorianCalendar hiv_antigen_date;
	private String cms_cert_lab_ind;
	private String fda_lic_mfg_inst_ind;
	@XmlElement(name="hiv_antigen_rslt")	
	public String getHiv_antigen_rslt() {
		return hiv_antigen_rslt;
	}
	public void setHiv_antigen_rslt(String hivAntigenRslt) {
		hiv_antigen_rslt = hivAntigenRslt;
	}
	@XmlElement(name="hiv_antigen_date")
	@XmlJavaTypeAdapter(type=XMLGregorianCalendar.class,value=XMLGeorgarianCalanderAdapter.class)
	public XMLGregorianCalendar getHiv_antigen_date() {
		return hiv_antigen_date;
	}
	public void setHiv_antigen_date(XMLGregorianCalendar hivAntigenDate) {
		hiv_antigen_date = hivAntigenDate;
	}
	@XmlElement(name="cms_cert_lab_ind")	
	public String getCms_cert_lab_ind() {
		return cms_cert_lab_ind;
	}
	public void setCms_cert_lab_ind(String cmsCertLabInd) {
		cms_cert_lab_ind = cmsCertLabInd;
	}
	@XmlElement(name="fda_lic_mfg_inst_ind")	
	public String getFda_lic_mfg_inst_ind() {
		return fda_lic_mfg_inst_ind;
	}
	public void setFda_lic_mfg_inst_ind(String fdaLicMfgInstInd) {
		fda_lic_mfg_inst_ind = fdaLicMfgInstInd;
	}
	
	

}
