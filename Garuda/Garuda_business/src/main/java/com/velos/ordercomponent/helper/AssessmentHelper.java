package com.velos.ordercomponent.helper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;

import com.velos.ordercomponent.business.domain.Assessment;
import com.velos.ordercomponent.business.domain.Auditable;
import com.velos.ordercomponent.business.pojoobjects.AssessmentPojo;
import com.velos.ordercomponent.business.pojoobjects.PatLabsPojo;
import com.velos.ordercomponent.business.util.HibernateUtil;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.VelosUtil;

/**
 * @author jamarendra
 *
 */
public class AssessmentHelper {
	
	
	public static final Log log=LogFactory.getLog(AssessmentHelper.class);
	public static List<AssessmentPojo> saveUpdateAssessfromsession(List<AssessmentPojo> assessmentList,List patLabsPojos){
		Assessment assessment = new Assessment();
		AssessmentPojo assessmentPojoList=new AssessmentPojo();
		List<AssessmentPojo> assessmentList1 =new ArrayList<AssessmentPojo>();
		Long fktestid=null;
		Long pkpatlab=null;
		
		try {
			Iterator<AssessmentPojo> assessmentPojo=assessmentList.iterator();
				while (assessmentPojo.hasNext()) {
					assessmentPojoList=assessmentPojo.next();
					for(int l=0;l<patLabsPojos.size();l++){
						PatLabsPojo patPojos = (PatLabsPojo)patLabsPojos.get(l);
						if(patPojos.getPkpatlabs()!=null && !patPojos.getPkpatlabs().equals("")){
							pkpatlab=patPojos.getPkpatlabs();
						}
						if(patPojos.getFktestid()!=null && !patPojos.getFktestid().equals("")){
							fktestid=patPojos.getFktestid();
						}
						if(fktestid.equals(assessmentPojoList.getFkTestId()))
						{
							if(pkpatlab!=null && pkpatlab>0){
								assessmentPojoList.setSubEntityId(pkpatlab);
								assessmentPojoList = saveUpdateAssessment(assessmentPojoList,null);
								assessmentList1.add(assessmentPojoList);
							}
						}
					}
				}
				
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return assessmentList1;
	}
	public static AssessmentPojo saveorUpdateAssessment(AssessmentPojo assessmentPojo){
		return saveUpdateAssessment(assessmentPojo,null);
	}
	
	public static AssessmentPojo saveUpdateAssessment(AssessmentPojo assessmentPojo, Session session){
		boolean sessionFlag = false;
		Assessment assessment= new Assessment();
		try {
			assessment = (Assessment) ObjectTransfer.transferObjects(assessmentPojo, assessment);
			Object object = new VelosUtil().setAuditableInfo(assessment);
			assessment=(Assessment)object;
			// check session
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			// check assessment pk id is avialable or not
			if (assessment.getPkAssessment() != null && assessment.getPkAssessment() > 0) {
				//if pk id is available
				assessment = (Assessment) session.merge(assessment);
			}
			else{
				//if pk id is not available
				session.save(assessment);
			}
			// check sessionFlag is true then commit the transaction
			if (sessionFlag) { session.getTransaction().commit(); }
			
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			//log.debug(" session in finally " + session + " /"+ sessionFlag);
			 if (session.isOpen()) {
			 session.close();
			 }
		}
		return assessmentPojo;
	}
	
	public static List saveOrUpdateAssessmentList(List assessmentList){
		return saveOrUpdateAssessmentList(assessmentList,null);
	}
	
	public static List saveOrUpdateAssessmentList(List assessmentPojosList,Session session) {
		List lstAssessments = new ArrayList();
		Assessment assessment = null;
		AssessmentPojo assessmentPojo = null;
		Iterator iterator = null;
		iterator = assessmentPojosList.iterator();
		Auditable auditObject = new Auditable();
		auditObject = (Auditable) new VelosUtil().setAuditableInfo(auditObject);
		while(iterator.hasNext()){
			assessment = new Assessment();
			AssessmentPojo aPojo = (AssessmentPojo)iterator.next();
			assessment = (Assessment) ObjectTransfer.transferObjects(aPojo, assessment);
			//assessment = (Assessment) new VelosUtil().setAuditableInfo(assessment);
			assessment = (Assessment) ObjectTransfer.transferObjects(auditObject, assessment);
			lstAssessments.add(assessment);
		}
		boolean sessionFlag = false;
		try{
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			iterator= lstAssessments.iterator();
			while(iterator.hasNext()){
				assessment = new Assessment();
				assessment = (Assessment)iterator.next();
				if(assessment.getPkAssessment()!=null){
					session.merge(assessment);
				}else{
					session.save(assessment);
				}
			}
			if (lstAssessments != null && lstAssessments.size() > 0) {
				assessmentPojosList.clear();
				iterator = lstAssessments.iterator();
				while(iterator.hasNext()){
					assessmentPojo = new AssessmentPojo();
					Assessment assessment2 = (Assessment)iterator.next();
					assessmentPojo = (AssessmentPojo) ObjectTransfer.transferObjects(assessment2, assessmentPojo);
					assessmentPojosList.add(assessmentPojo);
				}
			}
			if (sessionFlag) {
				session.getTransaction().commit();
			}

		} catch (Exception e) {
			//log.debug(" Exception in saveOrUpdateAssessment List " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return assessmentPojosList;
	}

	
	public static AssessmentPojo getAssessmentPojoById(AssessmentPojo assessmentPojo){
		return getAssessmentById(assessmentPojo,null);
	}
	
	public static AssessmentPojo getAssessmentById(AssessmentPojo assessmentPojo, Session session){
		boolean sessionFlag = false;
		Assessment assessment= new Assessment();
		try {
			assessment = (Assessment) ObjectTransfer.transferObjects(assessmentPojo, assessment);
			Object object = new VelosUtil().setAuditableInfo(assessment);
			assessment=(Assessment)object;
			// check session
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			// check assessment pk id is avialable or not
			if (assessment.getPkAssessment() != null && assessment.getPkAssessment() > 0) {
				//based on pk id getting the assessement Object 
				assessment = (Assessment) session.get(Assessment.class, assessment.getPkAssessment());
			}
			assessmentPojo = (AssessmentPojo) ObjectTransfer.transferObjects(assessment,
					assessmentPojo);
			// check sessionFlag is true then commit the transaction
			if (sessionFlag) { session.getTransaction().commit(); }
			
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			//log.debug(" session in finally " + session + " /"+ sessionFlag);
			 if (session.isOpen()) {
			 session.close();
			 }
		}
		return assessmentPojo;
	}

}