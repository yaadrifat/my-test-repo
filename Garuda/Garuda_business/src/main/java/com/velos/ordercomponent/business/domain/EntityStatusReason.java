package com.velos.ordercomponent.business.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CB_ENTITY_STATUS_REASON")
@SequenceGenerator(sequenceName="SEQ_CB_ENTITY_STATUS_REASON",name="SEQ_CB_ENTITY_STATUS_REASON",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class EntityStatusReason extends Auditable{
	
	private Long pkEntityStatusReason;
	private Long fkEntityStatus;
	private Long fkReasonId;
	private Long fkFormId;
	private Long fkAttachmentId;
	private Boolean notAppCollPriorFlag;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_ENTITY_STATUS_REASON")
	@Column(name="PK_ENTITY_STATUS_REASON")
	public Long getPkEntityStatusReason() {
		return pkEntityStatusReason;
	}
	public void setPkEntityStatusReason(Long pkEntityStatusReason) {
		this.pkEntityStatusReason = pkEntityStatusReason;
	}
	
	@Column(name="FK_ENTITY_STATUS")
	public Long getFkEntityStatus() {
		return fkEntityStatus;
	}
	public void setFkEntityStatus(Long fkEntityStatus) {
		this.fkEntityStatus = fkEntityStatus;
	}
	
	@Column(name="FK_REASON_ID")
	public Long getFkReasonId() {
		return fkReasonId;
	}
	public void setFkReasonId(Long fkReasonId) {
		this.fkReasonId = fkReasonId;
	}
	
	@Column(name="FK_FORMID")
	public Long getFkFormId() {
		return fkFormId;
	}

	public void setFkFormId(Long fkFormId) {
		this.fkFormId = fkFormId;
	}

	@Column(name="FK_ATTACHMENTID")
	public Long getFkAttachmentId() {
		return fkAttachmentId;
	}

	public void setFkAttachmentId(Long fkAttachmentId) {
		this.fkAttachmentId = fkAttachmentId;
	}
	
	@Column(name="NOT_APP_PRIOR_FLAG")
	public Boolean getNotAppCollPriorFlag() {
		return notAppCollPriorFlag;
	}
	public void setNotAppCollPriorFlag(Boolean notAppCollPriorFlag) {
		this.notAppCollPriorFlag = notAppCollPriorFlag;
	}
	
	

}
