package com.velos.ordercomponent.business.pojoobjects;

import java.io.Serializable;
import java.util.Date;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class PersonPojo extends AuditablePojo {

	private Long personId;
    private String personCode;
    private Long personAccount;
    private Long personLocation;
    private String personAltId;
    private String personLname;
    private String personFname;
    private String personMname;
    private String personSuffix;
    private String personPrefix;
    private String personDegree;
    private String personMotherName;
    private Date personDob;
    private Long personGender;
    private String personAlias;
    private Long personRace;
    private Long personEthnicity;
    private Long personPrimaryLang;
    private Long personMarital;
    private Long personRelegion;
    private String personSSN;
    private String personDrivLic;
    private Long personMotherId;
    private Long personEthGroup;
    private String personBirthPlace;
    private String personMultiBirth;
    private Long personCitizen;
    private String personVetMilStatus;
    private Long personNationality;
    private Date personDeathDate  ;
    private Long personEmployment;
    private Long personEducation;
    private String personNotes;
    private Long personStatus;
    private Long personBloodGr;
    private String personAddress1;
    private String personAddress2;
    private String personCity;
    private String personState;
    private String personZip;
    private String personCountry;
    private String personHphone;
    private String personBphone;
    private String personEmail;
    private String personCounty;
    private Date personRegDate ;
    private Long personRegBy;
    private Long timeZoneId;  
    private String phyOther;
    private String orgOther;
    private String personSplAccess;
    private String personAddRace;
    private String personAddEthnicity;
    private Long patDthCause;
    private String dthCauseOther;
    private Long personNtype;

    /**
     * the Patient Facility Id
     */
    private String patientFacilityId;

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public String getPersonCode() {
		return personCode;
	}

	public void setPersonCode(String personCode) {
		this.personCode = personCode;
	}

	public Long getPersonAccount() {
		return personAccount;
	}

	public void setPersonAccount(Long personAccount) {
		this.personAccount = personAccount;
	}

	public Long getPersonLocation() {
		return personLocation;
	}

	public void setPersonLocation(Long personLocation) {
		this.personLocation = personLocation;
	}

	public String getPersonAltId() {
		return personAltId;
	}

	public void setPersonAltId(String personAltId) {
		this.personAltId = personAltId;
	}

	public String getPersonLname() {
		return personLname;
	}

	public void setPersonLname(String personLname) {
		this.personLname = personLname;
	}

	public String getPersonFname() {
		return personFname;
	}

	public void setPersonFname(String personFname) {
		this.personFname = personFname;
	}

	public String getPersonMname() {
		return personMname;
	}

	public void setPersonMname(String personMname) {
		this.personMname = personMname;
	}

	public String getPersonSuffix() {
		return personSuffix;
	}

	public void setPersonSuffix(String personSuffix) {
		this.personSuffix = personSuffix;
	}

	public String getPersonPrefix() {
		return personPrefix;
	}

	public void setPersonPrefix(String personPrefix) {
		this.personPrefix = personPrefix;
	}

	public String getPersonDegree() {
		return personDegree;
	}

	public void setPersonDegree(String personDegree) {
		this.personDegree = personDegree;
	}

	public String getPersonMotherName() {
		return personMotherName;
	}

	public void setPersonMotherName(String personMotherName) {
		this.personMotherName = personMotherName;
	}

	public Date getPersonDob() {
		return personDob;
	}

	public void setPersonDob(Date personDob) {
		this.personDob = personDob;
	}

	public Long getPersonGender() {
		return personGender;
	}

	public void setPersonGender(Long personGender) {
		this.personGender = personGender;
	}

	public String getPersonAlias() {
		return personAlias;
	}

	public void setPersonAlias(String personAlias) {
		this.personAlias = personAlias;
	}

	public Long getPersonRace() {
		return personRace;
	}

	public void setPersonRace(Long personRace) {
		this.personRace = personRace;
	}

	public Long getPersonEthnicity() {
		return personEthnicity;
	}

	public void setPersonEthnicity(Long personEthnicity) {
		this.personEthnicity = personEthnicity;
	}

	public Long getPersonPrimaryLang() {
		return personPrimaryLang;
	}

	public void setPersonPrimaryLang(Long personPrimaryLang) {
		this.personPrimaryLang = personPrimaryLang;
	}

	public Long getPersonMarital() {
		return personMarital;
	}

	public void setPersonMarital(Long personMarital) {
		this.personMarital = personMarital;
	}

	public Long getPersonRelegion() {
		return personRelegion;
	}

	public void setPersonRelegion(Long personRelegion) {
		this.personRelegion = personRelegion;
	}

	public String getPersonSSN() {
		return personSSN;
	}

	public void setPersonSSN(String personSSN) {
		this.personSSN = personSSN;
	}

	public String getPersonDrivLic() {
		return personDrivLic;
	}

	public void setPersonDrivLic(String personDrivLic) {
		this.personDrivLic = personDrivLic;
	}

	public Long getPersonMotherId() {
		return personMotherId;
	}

	public void setPersonMotherId(Long personMotherId) {
		this.personMotherId = personMotherId;
	}

	public Long getPersonEthGroup() {
		return personEthGroup;
	}

	public void setPersonEthGroup(Long personEthGroup) {
		this.personEthGroup = personEthGroup;
	}

	public String getPersonBirthPlace() {
		return personBirthPlace;
	}

	public void setPersonBirthPlace(String personBirthPlace) {
		this.personBirthPlace = personBirthPlace;
	}

	public String getPersonMultiBirth() {
		return personMultiBirth;
	}

	public void setPersonMultiBirth(String personMultiBirth) {
		this.personMultiBirth = personMultiBirth;
	}

	public Long getPersonCitizen() {
		return personCitizen;
	}

	public void setPersonCitizen(Long personCitizen) {
		this.personCitizen = personCitizen;
	}

	public String getPersonVetMilStatus() {
		return personVetMilStatus;
	}

	public void setPersonVetMilStatus(String personVetMilStatus) {
		this.personVetMilStatus = personVetMilStatus;
	}

	public Long getPersonNationality() {
		return personNationality;
	}

	public void setPersonNationality(Long personNationality) {
		this.personNationality = personNationality;
	}

	public Date getPersonDeathDate() {
		return personDeathDate;
	}

	public void setPersonDeathDate(Date personDeathDate) {
		this.personDeathDate = personDeathDate;
	}

	public Long getPersonEmployment() {
		return personEmployment;
	}

	public void setPersonEmployment(Long personEmployment) {
		this.personEmployment = personEmployment;
	}

	public Long getPersonEducation() {
		return personEducation;
	}

	public void setPersonEducation(Long personEducation) {
		this.personEducation = personEducation;
	}

	public String getPersonNotes() {
		return personNotes;
	}

	public void setPersonNotes(String personNotes) {
		this.personNotes = personNotes;
	}

	public Long getPersonStatus() {
		return personStatus;
	}

	public void setPersonStatus(Long personStatus) {
		this.personStatus = personStatus;
	}

	public Long getPersonBloodGr() {
		return personBloodGr;
	}

	public void setPersonBloodGr(Long personBloodGr) {
		this.personBloodGr = personBloodGr;
	}

	public String getPersonAddress1() {
		return personAddress1;
	}

	public void setPersonAddress1(String personAddress1) {
		this.personAddress1 = personAddress1;
	}

	public String getPersonAddress2() {
		return personAddress2;
	}

	public void setPersonAddress2(String personAddress2) {
		this.personAddress2 = personAddress2;
	}

	public String getPersonCity() {
		return personCity;
	}

	public void setPersonCity(String personCity) {
		this.personCity = personCity;
	}

	public String getPersonState() {
		return personState;
	}

	public void setPersonState(String personState) {
		this.personState = personState;
	}

	public String getPersonZip() {
		return personZip;
	}

	public void setPersonZip(String personZip) {
		this.personZip = personZip;
	}

	public String getPersonCountry() {
		return personCountry;
	}

	public void setPersonCountry(String personCountry) {
		this.personCountry = personCountry;
	}

	public String getPersonHphone() {
		return personHphone;
	}

	public void setPersonHphone(String personHphone) {
		this.personHphone = personHphone;
	}

	public String getPersonBphone() {
		return personBphone;
	}

	public void setPersonBphone(String personBphone) {
		this.personBphone = personBphone;
	}

	public String getPersonEmail() {
		return personEmail;
	}

	public void setPersonEmail(String personEmail) {
		this.personEmail = personEmail;
	}

	public String getPersonCounty() {
		return personCounty;
	}

	public void setPersonCounty(String personCounty) {
		this.personCounty = personCounty;
	}

	public Date getPersonRegDate() {
		return personRegDate;
	}

	public void setPersonRegDate(Date personRegDate) {
		this.personRegDate = personRegDate;
	}

	public Long getPersonRegBy() {
		return personRegBy;
	}

	public void setPersonRegBy(Long personRegBy) {
		this.personRegBy = personRegBy;
	}

	public Long getTimeZoneId() {
		return timeZoneId;
	}

	public void setTimeZoneId(Long timeZoneId) {
		this.timeZoneId = timeZoneId;
	}

	public String getPhyOther() {
		return phyOther;
	}

	public void setPhyOther(String phyOther) {
		this.phyOther = phyOther;
	}

	public String getOrgOther() {
		return orgOther;
	}

	public void setOrgOther(String orgOther) {
		this.orgOther = orgOther;
	}

	public String getPersonSplAccess() {
		return personSplAccess;
	}

	public void setPersonSplAccess(String personSplAccess) {
		this.personSplAccess = personSplAccess;
	}

	public String getPersonAddRace() {
		return personAddRace;
	}

	public void setPersonAddRace(String personAddRace) {
		this.personAddRace = personAddRace;
	}

	public String getPersonAddEthnicity() {
		return personAddEthnicity;
	}

	public void setPersonAddEthnicity(String personAddEthnicity) {
		this.personAddEthnicity = personAddEthnicity;
	}

	public Long getPatDthCause() {
		return patDthCause;
	}

	public void setPatDthCause(Long patDthCause) {
		this.patDthCause = patDthCause;
	}

	public String getDthCauseOther() {
		return dthCauseOther;
	}

	public void setDthCauseOther(String dthCauseOther) {
		this.dthCauseOther = dthCauseOther;
	}

	public Long getPersonNtype() {
		return personNtype;
	}

	public void setPersonNtype(Long personNtype) {
		this.personNtype = personNtype;
	}

	public String getPatientFacilityId() {
		return patientFacilityId;
	}

	public void setPatientFacilityId(String patientFacilityId) {
		this.patientFacilityId = patientFacilityId;
	}

	  
    
	
}
