package com.velos.ordercomponent.business.xmlpojos;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class CJD {

	private String cruetz_jakob_dis_ind;
	/*private Relations relations;
	@XmlElement(name="Relations")
	public Relations getRelations() {
		return relations;
	}
	public void setRelations(Relations relations) {
		this.relations = relations;
	}*/
	@XmlElement(name="cruetz_jakob_dis_ind")
	public String getCruetz_jakob_dis_ind() {
		return cruetz_jakob_dis_ind;
	}
	public void setCruetz_jakob_dis_ind(String cruetzJakobDisInd) {
		cruetz_jakob_dis_ind = cruetzJakobDisInd;
	}
	private List<Relations> Relations;
	@XmlElement(name="Relations")
	public List<Relations> getRelations() {
		return Relations;
	}
	public void setRelations(List<Relations> relations) {
		Relations = relations;
	}
}
