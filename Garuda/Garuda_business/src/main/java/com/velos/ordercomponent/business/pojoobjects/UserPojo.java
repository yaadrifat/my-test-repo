package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

import com.velos.ordercomponent.business.domain.Address;
import com.velos.ordercomponent.business.domain.Grps;

public class UserPojo {
	private Long userId;
	private String loginId;
	private String lastName;
	private String firstName;
	private String middleName;
	private Long fkAccount;
	private Address address;
	private String email;
	private String usrType;
	private Long createdBy;
	private Date createdOn;
	private Long lstModBy;
	private Date lstModOn;
	private String ipAddress;
	private String deletedFlag;
	
	private String signature;
	private String userTitle;
	private Grps group;
	
	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	
	public Long getFkAccount() {
		return fkAccount;
	}

	public void setFkAccount(Long fkAccount) {
		this.fkAccount = fkAccount;
	}
	
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsrType() {
		return usrType;
	}

	public void setUsrType(String usrType) {
		this.usrType = usrType;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getLstModBy() {
		return lstModBy;
	}

	public void setLstModBy(Long lstModBy) {
		this.lstModBy = lstModBy;
	}

	public Date getLstModOn() {
		return lstModOn;
	}

	public void setLstModOn(Date lstModOn) {
		this.lstModOn = lstModOn;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getDeletedFlag() {
		return deletedFlag;
	}

	public void setDeletedFlag(String deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public String getUserTitle() {
		return userTitle;
	}

	public void setUserTitle(String userTitle) {
		this.userTitle = userTitle;
	}

	public Grps getGroup() {
		return group;
	}

	public void setGroup(Grps group) {
		this.group = group;
	}

}
