package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;



@Entity
@Table(name="CB_REQUEST")
@SequenceGenerator(name="SEQ_CB_REQUEST",sequenceName="SEQ_CB_REQUEST",allocationSize=1)
public class Request extends Auditable{

	private Long pkRequest;
	private Long fkorderId;
	private String requestStatus;
	private Date lstReqStatDate;
	private Long reqLstReviewedBy;
	private Date reqLstReviewedDate;
	private Long reqAssignedTo;
	private Date reqAssignedDate;
	private long fkCordId;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_REQUEST")
	@Column(name="PK_REQUEST")
	public Long getPkRequest() {
		return pkRequest;
	}
	public void setPkRequest(Long pkRequest) {
		this.pkRequest = pkRequest;
	}
	
	@Column(name="FK_ORDER_ID")
	public Long getFkorderId() {
		return fkorderId;
	}
	public void setFkorderId(Long fkorderId) {
		this.fkorderId = fkorderId;
	}
	
	
	@Column(name="REQUEST_STATUS")
	public String getRequestStatus() {
		return requestStatus;
	}
	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}
	
	@Column(name="LAST_REQ_STAT_DATE")
	public Date getLstReqStatDate() {
		return lstReqStatDate;
	}
	public void setLstReqStatDate(Date lstReqStatDate) {
		this.lstReqStatDate = lstReqStatDate;
	}
	
	@Column(name="REQ_LAST_REVIEWED_BY")
	public Long getReqLstReviewedBy() {
		return reqLstReviewedBy;
	}
	public void setReqLstReviewedBy(Long reqLstReviewedBy) {
		this.reqLstReviewedBy = reqLstReviewedBy;
	}
	
	@Column(name="REQ_LAST_REVIEWED_DATE")
	public Date getReqLstReviewedDate() {
		return reqLstReviewedDate;
	}
	public void setReqLstReviewedDate(Date reqLstReviewedDate) {
		this.reqLstReviewedDate = reqLstReviewedDate;
	}
	
	@Column(name="REQ_ASSIGNED_TO")
	public Long getReqAssignedTo() {
		return reqAssignedTo;
	}
	public void setReqAssignedTo(Long reqAssignedTo) {
		this.reqAssignedTo = reqAssignedTo;
	}
	
	
	@Column(name="REQ_ASSIGNED_DATE")
	public Date getReqAssignedDate() {
		return reqAssignedDate;
	}
	public void setReqAssignedDate(Date reqAssignedDate) {
		this.reqAssignedDate = reqAssignedDate;
	}
	
	@Column(name="FK_CORD_ID")
	public long getFkCordId() {
		return fkCordId;
	}
	public void setFkCordId(long fkCordId) {
		this.fkCordId = fkCordId;
	}
	
	
	
}
