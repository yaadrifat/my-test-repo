package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

public class ShipmentPojo extends AuditablePojo{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long shipmentId;
	private Long orderId;
	private Date shipmentDate;
	private Date addiShipmentDdate;
	private String shipmentTrackingNo;
	private Long addiShipmentTrackingNo;
	private Long cbbId;
	private Long fkCordId;
	private String attnName;
	private Long fkDeliveryAddress;
	private Long fkShippingContainer;
	private Long fkTempMoniter;
	private Date expectingArriveDate;
	private String shipmentFlag;
	private Long shipmentConfirmBy;
	private Date shipmentConfirmDate;
	private Date propShipmentDate;
	private Date schShipmentDate;
	private String addiInfo;
	
	
	private Long fkshipingCompanyId;
	private String shipmentDateStr;
	private String addiShipmentDdateStr;
	private String expectingArriveDateStr;
	private String shipmentConfirmDateStr;
	private String propShipmentDateStr;
	private String schShipmentDateStr;
	
	private String shipmentTime;
	private String padlockCombination;
	private Long paperworkLoc;
	private String additiShiiperDet;
	private Long shipmentType;
	private String nmdpShipExcuse;
	private String dataModifiedFlag;
	private String switchWfFlag;
	private Long excuseFormSubmittedBy;
	private Date excuseFormSubmittedDate;
	private String updateFlag;
	
	
	
	
	public String getUpdateFlag() {
		return updateFlag;
	}
	public void setUpdateFlag(String updateFlag) {
		this.updateFlag = updateFlag;
	}
	public String getSwitchWfFlag() {
		return switchWfFlag;
	}
	public void setSwitchWfFlag(String switchWfFlag) {
		this.switchWfFlag = switchWfFlag;
	}
	
	public Long getShipmentId() {
		return shipmentId;
	}
	public void setShipmentId(Long shipmentId) {
		this.shipmentId = shipmentId;
	}
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public Date getShipmentDate() {
		return shipmentDate;
	}
	public void setShipmentDate(Date shipmentDate) {
		this.shipmentDate = shipmentDate;
	}
	public Date getAddiShipmentDdate() {
		return addiShipmentDdate;
	}
	public void setAddiShipmentDdate(Date addiShipmentDdate) {
		this.addiShipmentDdate = addiShipmentDdate;
	}
	public String getShipmentTrackingNo() {
		return shipmentTrackingNo;
	}
	public void setShipmentTrackingNo(String shipmentTrackingNo) {
		this.shipmentTrackingNo = shipmentTrackingNo;
	}
	public Long getAddiShipmentTrackingNo() {
		return addiShipmentTrackingNo;
	}
	public void setAddiShipmentTrackingNo(Long addiShipmentTrackingNo) {
		this.addiShipmentTrackingNo = addiShipmentTrackingNo;
	}
	public Long getCbbId() {
		return cbbId;
	}
	public void setCbbId(Long cbbId) {
		this.cbbId = cbbId;
	}
	public Long getFkCordId() {
		return fkCordId;
	}
	public void setFkCordId(Long fkCordId) {
		this.fkCordId = fkCordId;
	}
	public String getAttnName() {
		return attnName;
	}
	public void setAttnName(String attnName) {
		this.attnName = attnName;
	}
	public Long getFkDeliveryAddress() {
		return fkDeliveryAddress;
	}
	public void setFkDeliveryAddress(Long fkDeliveryAddress) {
		this.fkDeliveryAddress = fkDeliveryAddress;
	}
	public Long getFkShippingContainer() {
		return fkShippingContainer;
	}
	public void setFkShippingContainer(Long fkShippingContainer) {
		this.fkShippingContainer = fkShippingContainer;
	}
	public String getShipmentTime() {
		return shipmentTime;
	}
	public void setShipmentTime(String shipmentTime) {
		this.shipmentTime = shipmentTime;
	}
	public Long getFkTempMoniter() {
		return fkTempMoniter;
	}
	public void setFkTempMoniter(Long fkTempMoniter) {
		this.fkTempMoniter = fkTempMoniter;
	}
	public Date getExpectingArriveDate() {
		return expectingArriveDate;
	}
	public void setExpectingArriveDate(Date expectingArriveDate) {
		this.expectingArriveDate = expectingArriveDate;
	}
	public String getShipmentFlag() {
		return shipmentFlag;
	}
	public void setShipmentFlag(String shipmentFlag) {
		this.shipmentFlag = shipmentFlag;
	}
	public Long getShipmentConfirmBy() {
		return shipmentConfirmBy;
	}
	public void setShipmentConfirmBy(Long shipmentConfirmBy) {
		this.shipmentConfirmBy = shipmentConfirmBy;
	}
	public Date getShipmentConfirmDate() {
		return shipmentConfirmDate;
	}
	public void setShipmentConfirmDate(Date shipmentConfirmDate) {
		this.shipmentConfirmDate = shipmentConfirmDate;
	}
	public Date getPropShipmentDate() {
		return propShipmentDate;
	}
	public void setPropShipmentDate(Date propShipmentDate) {
		this.propShipmentDate = propShipmentDate;
	}
	public Date getSchShipmentDate() {
		return schShipmentDate;
	}
	public void setSchShipmentDate(Date schShipmentDate) {
		this.schShipmentDate = schShipmentDate;
	}
	public String getAddiInfo() {
		return addiInfo;
	}
	public void setAddiInfo(String addiInfo) {
		this.addiInfo = addiInfo;
	}
	
	public Long getFkshipingCompanyId() {
		return fkshipingCompanyId;
	}
	public void setFkshipingCompanyId(Long fkshipingCompanyId) {
		this.fkshipingCompanyId = fkshipingCompanyId;
	}
	public String getShipmentDateStr() {
		return shipmentDateStr;
	}
	public void setShipmentDateStr(String shipmentDateStr) {
		this.shipmentDateStr = shipmentDateStr;
	}
	public String getAddiShipmentDdateStr() {
		return addiShipmentDdateStr;
	}
	public void setAddiShipmentDdateStr(String addiShipmentDdateStr) {
		this.addiShipmentDdateStr = addiShipmentDdateStr;
	}
	public String getExpectingArriveDateStr() {
		return expectingArriveDateStr;
	}
	public void setExpectingArriveDateStr(String expectingArriveDateStr) {
		this.expectingArriveDateStr = expectingArriveDateStr;
	}
	public String getShipmentConfirmDateStr() {
		return shipmentConfirmDateStr;
	}
	public void setShipmentConfirmDateStr(String shipmentConfirmDateStr) {
		this.shipmentConfirmDateStr = shipmentConfirmDateStr;
	}
	public String getPropShipmentDateStr() {
		return propShipmentDateStr;
	}
	public void setPropShipmentDateStr(String propShipmentDateStr) {
		this.propShipmentDateStr = propShipmentDateStr;
	}
	public String getSchShipmentDateStr() {
		return schShipmentDateStr;
	}
	public void setSchShipmentDateStr(String schShipmentDateStr) {
		this.schShipmentDateStr = schShipmentDateStr;
	}
	
	public String getPadlockCombination() {
		return padlockCombination;
	}
	public void setPadlockCombination(String padlockCombination) {
		this.padlockCombination = padlockCombination;
	}
	public Long getPaperworkLoc() {
		return paperworkLoc;
	}
	public void setPaperworkLoc(Long paperworkLoc) {
		this.paperworkLoc = paperworkLoc;
	}
	public String getAdditiShiiperDet() {
		return additiShiiperDet;
	}
	public void setAdditiShiiperDet(String additiShiiperDet) {
		this.additiShiiperDet = additiShiiperDet;
	}
	
	public Long getShipmentType() {
		return shipmentType;
	}
	public void setShipmentType(Long shipmentType) {
		this.shipmentType = shipmentType;
	}
	public String getNmdpShipExcuse() {
		return nmdpShipExcuse;
	}
	public void setNmdpShipExcuse(String nmdpShipExcuse) {
		this.nmdpShipExcuse = nmdpShipExcuse;
	}
	public String getDataModifiedFlag() {
		return dataModifiedFlag;
	}
	public void setDataModifiedFlag(String dataModifiedFlag) {
		this.dataModifiedFlag = dataModifiedFlag;
	}
	public Long getExcuseFormSubmittedBy() {
		return excuseFormSubmittedBy;
	}
	public void setExcuseFormSubmittedBy(Long excuseFormSubmittedBy) {
		this.excuseFormSubmittedBy = excuseFormSubmittedBy;
	}
	public Date getExcuseFormSubmittedDate() {
		return excuseFormSubmittedDate;
	}
	public void setExcuseFormSubmittedDate(Date excuseFormSubmittedDate) {
		this.excuseFormSubmittedDate = excuseFormSubmittedDate;
	}
	
	
	
	
	
	

}
