package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

public class IdmTestPojo {

	/* Fields for IDMTEST data */
	
	private Long pkpatlabs;
	private Long fktestid;
	private Long fklabgroup;
	private Long fktestoutcome; 
	private Long fktestsource;
	private Long cmsapprovedlab;
	private Long fdalicensedlab;
	private String labtestname;
	private String testDatestr;
	private Date testDate;
	private Long fkspecimen;
	private String shrtName;
	private String testoutcomedec;
	private String testcomments;
	private Date createdon;
	private Long testcount;
	
	/* Fields for assessment data */
	
	private Long pkassessment;
	private String assessmentremark;
	private String flagforlater;
	private Long assessmentresponse;
	private String visibletotcflag;
	private String consultflag;
	private Long assessmentpostedby;
	private Date assessmentdate;
	private Long assessmentconsultby;
	private Date consulteddate;
	
	/* Getter and Setter for IDM Test Data*/
	
	public Long getPkpatlabs() {
		return pkpatlabs;
	}
	public void setPkpatlabs(Long pkpatlabs) {
		this.pkpatlabs = pkpatlabs;
	}
	public Long getFktestid() {
		return fktestid;
	}
	public void setFktestid(Long fktestid) {
		this.fktestid = fktestid;
	}
	public Long getFklabgroup() {
		return fklabgroup;
	}
	public void setFklabgroup(Long fklabgroup) {
		this.fklabgroup = fklabgroup;
	}
	public Long getFktestoutcome() {
		return fktestoutcome;
	}
	public void setFktestoutcome(Long fktestoutcome) {
		this.fktestoutcome = fktestoutcome;
	}
	public Long getFktestsource() {
		return fktestsource;
	}
	public void setFktestsource(Long fktestsource) {
		this.fktestsource = fktestsource;
	}
	public Long getCmsapprovedlab() {
		return cmsapprovedlab;
	}
	public void setCmsapprovedlab(Long cmsapprovedlab) {
		this.cmsapprovedlab = cmsapprovedlab;
	}
	public Long getFdalicensedlab() {
		return fdalicensedlab;
	}
	public void setFdalicensedlab(Long fdalicensedlab) {
		this.fdalicensedlab = fdalicensedlab;
	}
	public String getLabtestname() {
		return labtestname;
	}
	public void setLabtestname(String labtestname) {
		this.labtestname = labtestname;
	}

	public Date getTestDate() {
		return testDate;
	}
	public void setTestDate(Date testDate) {
		this.testDate = testDate;
	}
	
	public String getTestDatestr() {
		return testDatestr;
	}
	public void setTestDatestr(String testDatestr) {
		this.testDatestr = testDatestr;
	}
	
	public Long getFkspecimen() {
		return fkspecimen;
	}
	public void setFkspecimen(Long fkspecimen) {
		this.fkspecimen = fkspecimen;
	}
	public String getShrtName() {
		return shrtName;
	}
	public void setShrtName(String shrtName) {
		this.shrtName = shrtName;
	}
	public String getTestoutcomedec() {
		return testoutcomedec;
	}
	public void setTestoutcomedec(String testoutcomedec) {
		this.testoutcomedec = testoutcomedec;
	}
	public String getTestcomments() {
		return testcomments;
	}
	public void setTestcomments(String testcomments) {
		this.testcomments = testcomments;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	
	public Long getTestcount() {
		return testcount;
	}
	public void setTestcount(Long testcount) {
		this.testcount = testcount;
	}
	
	/* Getter and Setter for assessment data*/
	
	public Long getPkassessment() {
		return pkassessment;
	}
	public void setPkassessment(Long pkassessment) {
		this.pkassessment = pkassessment;
	}
	public String getAssessmentremark() {
		return assessmentremark;
	}
	public void setAssessmentremark(String assessmentremark) {
		this.assessmentremark = assessmentremark;
	}
	public String getFlagforlater() {
		return flagforlater;
	}
	public void setFlagforlater(String flagforlater) {
		this.flagforlater = flagforlater;
	}
	public Long getAssessmentresponse() {
		return assessmentresponse;
	}
	public void setAssessmentresponse(Long assessmentresponse) {
		this.assessmentresponse = assessmentresponse;
	}
	public String getVisibletotcflag() {
		return visibletotcflag;
	}
	public void setVisibletotcflag(String visibletotcflag) {
		this.visibletotcflag = visibletotcflag;
	}
	public String getConsultflag() {
		return consultflag;
	}
	public void setConsultflag(String consultflag) {
		this.consultflag = consultflag;
	}
	public Long getAssessmentpostedby() {
		return assessmentpostedby;
	}
	public void setAssessmentpostedby(Long assessmentpostedby) {
		this.assessmentpostedby = assessmentpostedby;
	}
	public Date getAssessmentdate() {
		return assessmentdate;
	}
	public void setAssessmentdate(Date assessmentdate) {
		this.assessmentdate = assessmentdate;
	}
	public Long getAssessmentconsultby() {
		return assessmentconsultby;
	}
	public void setAssessmentconsultby(Long assessmentconsultby) {
		this.assessmentconsultby = assessmentconsultby;
	}
	public Date getConsulteddate() {
		return consulteddate;
	}
	public void setConsulteddate(Date consulteddate) {
		this.consulteddate = consulteddate;
	}
}
