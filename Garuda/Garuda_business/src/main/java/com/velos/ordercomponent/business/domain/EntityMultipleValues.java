package com.velos.ordercomponent.business.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author Mohiuddin Ali Ahmed
 *
 */
@Entity
@Table(name="CB_MULTIPLE_VALUES")
@SequenceGenerator(sequenceName="SEQ_CB_MULTIPLE_VALUES",name="SEQ_CB_MULTIPLE_VALUES",allocationSize=1)
public class EntityMultipleValues extends Auditable {
	
	private Long pkEntityMultiple;
	private String entityMultipleType;
	private Long entityMultipleValue;
	private Long entityId;
	private String entityType;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_MULTIPLE_VALUES")
	@Column(name="PK_MULTIPLE_VALUES")
	public Long getPkEntityMultiple() {
		return pkEntityMultiple;
	}
	public void setPkEntityMultiple(Long pkEntityMultiple) {
		this.pkEntityMultiple = pkEntityMultiple;
	}
	@Column(name="FK_CODELST_TYPE")
	public String getEntityMultipleType() {
		return entityMultipleType;
	}
	public void setEntityMultipleType(String entityMultipleType) {
		this.entityMultipleType = entityMultipleType;
	}
	@Column(name="FK_CODELST")
	public Long getEntityMultipleValue() {
		return entityMultipleValue;
	}
	public void setEntityMultipleValue(Long entityMultipleValue) {
		this.entityMultipleValue = entityMultipleValue;
	}
	@Column(name="ENTITY_ID")
	public Long getEntityId() {
		return entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	@Column(name="ENTITY_TYPE")
	public String getEntityType() {
		return entityType;
	}
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}
	
	

}
