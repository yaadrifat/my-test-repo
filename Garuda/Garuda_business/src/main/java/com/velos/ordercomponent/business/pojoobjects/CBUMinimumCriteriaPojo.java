package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;


public class CBUMinimumCriteriaPojo extends AuditablePojo{
	
	private Long pkcordminimumcriteria;
	private Long fkCordId;
	private Long fkOrderId;
	private Long fkRecipient;
	/*private Boolean bacterialtestingflag;
	private Boolean fungaltestingflag;
	private Boolean hemoglobinopthyflag;
	private Boolean idmsflag;
	private Boolean postprocessdose; 
	private Boolean postprocessviab;
	private Boolean hlaconfirm;
	private Boolean antigensconfirm;*/
	private Long declarationresult;
	private Boolean saveinprogress;
	private Boolean conformedminimumcriteria;
	private Boolean deletedflag;
	private Long antihivcmp;
	private Long hivpnatcmp;
	private Long hbsagcmp;
	private Long antihcvcmp;
	private Long hlacnfm;
	private Long hlacnfmchild;
	private Long ctcompleted;
	private Long ctcompletedchild;
	private Long antigenmatch;
	private Long antigenmatchchild;
	private Long unitrecp;
	private Long unitrecpchild;
	private Long cbuplanned;
	private Long cbuplannedchild;
	private Long patienttyp;
	private Long patienttypchild;
	private Long viabchild;
	private Long postprocesschild;
	/*private Boolean patTyping;
	private Boolean priortoprepstart;
	private Boolean unitrecpflag;
	private Boolean ctcompetedFlag;
	private CdrCbu cdrCbu;*/
	private String patentId;
	private String prop_prep_date;
	private String shipdate;
	private Long secondReviewBy;
	private Date secondReviewOn;
	private Boolean reviewConformed;
	private Long ReviewBy;
	private Date ReviewOn;
	private Long viabTestTiming;
	private Long viabPerFlag;
	
	
	public Long getViabTestTiming() {
		return viabTestTiming;
	}
	public void setViabTestTiming(Long viabTestTiming) {
		this.viabTestTiming = viabTestTiming;
	}
	public Long getViabPerFlag() {
		return viabPerFlag;
	}
	public void setViabPerFlag(Long viabPerFlag) {
		this.viabPerFlag = viabPerFlag;
	}
	public Long getReviewBy() {
		return ReviewBy;
	}
	public void setReviewBy(Long reviewBy) {
		ReviewBy = reviewBy;
	}
	public Date getReviewOn() {
		return ReviewOn;
	}
	public void setReviewOn(Date reviewOn) {
		ReviewOn = reviewOn;
	}
	public Boolean getReviewConformed() {
		return reviewConformed;
	}
	public void setReviewConformed(Boolean reviewConformed) {
		this.reviewConformed = reviewConformed;
	}
	public String getProp_prep_date() {
		return prop_prep_date;
	}
	public void setProp_prep_date(String prop_prep_date) {
		this.prop_prep_date = prop_prep_date;
	}
	public String getShipdate() {
		return shipdate;
	}
	public void setShipdate(String shipdate) {
		this.shipdate = shipdate;
	}
	public String getPatentId() {
		return patentId;
	}
	public void setPatentId(String patentId) {
		this.patentId = patentId;
	}
	public Long getPkcordminimumcriteria() {
		return pkcordminimumcriteria;
	}
	public void setPkcordminimumcriteria(Long pkcordminimumcriteria) {
		this.pkcordminimumcriteria = pkcordminimumcriteria;
	}
	public Long getFkCordId() {
		return fkCordId;
	}
	public void setFkCordId(Long fkCordId) {
		this.fkCordId = fkCordId;
	}
	public Long getFkOrderId() {
		return fkOrderId;
	}
	public void setFkOrderId(Long fkOrderId) {
		this.fkOrderId = fkOrderId;
	}
	public Long getFkRecipient() {
		return fkRecipient;
	}
	public void setFkRecipient(Long fkRecipient) {
		this.fkRecipient = fkRecipient;
	}/*
	public Boolean getBacterialtestingflag() {
		return bacterialtestingflag;
	}
	public void setBacterialtestingflag(Boolean bacterialtestingflag) {
		this.bacterialtestingflag = bacterialtestingflag;
	}
	public Boolean getFungaltestingflag() {
		return fungaltestingflag;
	}
	public void setFungaltestingflag(Boolean fungaltestingflag) {
		this.fungaltestingflag = fungaltestingflag;
	}
	public Boolean getHemoglobinopthyflag() {
		return hemoglobinopthyflag;
	}
	public void setHemoglobinopthyflag(Boolean hemoglobinopthyflag) {
		this.hemoglobinopthyflag = hemoglobinopthyflag;
	}
	public Boolean getIdmsflag() {
		return idmsflag;
	}
	public void setIdmsflag(Boolean idmsflag) {
		this.idmsflag = idmsflag;
	}
	public Boolean getPostprocessdose() {
		return postprocessdose;
	}
	public void setPostprocessdose(Boolean postprocessdose) {
		this.postprocessdose = postprocessdose;
	}
	public Boolean getPostprocessviab() {
		return postprocessviab;
	}
	public void setPostprocessviab(Boolean postprocessviab) {
		this.postprocessviab = postprocessviab;
	}
	public Boolean getHlaconfirm() {
		return hlaconfirm;
	}
	public void setHlaconfirm(Boolean hlaconfirm) {
		this.hlaconfirm = hlaconfirm;
	}
	public Boolean getAntigensconfirm() {
		return antigensconfirm;
	}
	public void setAntigensconfirm(Boolean antigensconfirm) {
		this.antigensconfirm = antigensconfirm;
	}*/
	public Long getDeclarationresult() {
		return declarationresult;
	}
	public void setDeclarationresult(Long declarationresult) {
		this.declarationresult = declarationresult;
	}
	public Boolean getSaveinprogress() {
		return saveinprogress;
	}
	public void setSaveinprogress(Boolean saveinprogress) {
		this.saveinprogress = saveinprogress;
	}
	public Boolean getConformedminimumcriteria() {
		return conformedminimumcriteria;
	}
	public void setConformedminimumcriteria(Boolean conformedminimumcriteria) {
		this.conformedminimumcriteria = conformedminimumcriteria;
	}
	public Boolean getDeletedflag() {
		return deletedflag;
	}
	public void setDeletedflag(Boolean deletedflag) {
		this.deletedflag = deletedflag;
	}

	public Long getAntihivcmp() {
		return antihivcmp;
	}
	public void setAntihivcmp(Long antihivcmp) {
		this.antihivcmp = antihivcmp;
	}
	public Long getHbsagcmp() {
		return hbsagcmp;
	}
	public void setHbsagcmp(Long hbsagcmp) {
		this.hbsagcmp = hbsagcmp;
	}
	public Long getAntihcvcmp() {
		return antihcvcmp;
	}
	public void setAntihcvcmp(Long antihcvcmp) {
		this.antihcvcmp = antihcvcmp;
	}
	public Long getHlacnfm() {
		return hlacnfm;
	}
	public void setHlacnfm(Long hlacnfm) {
		this.hlacnfm = hlacnfm;
	}
	public Long getAntigenmatch() {
		return antigenmatch;
	}
	public void setAntigenmatch(Long antigenmatch) {
		this.antigenmatch = antigenmatch;
	}
	public Long getHivpnatcmp() {
		return hivpnatcmp;
	}
	public void setHivpnatcmp(Long hivpnatcmp) {
		this.hivpnatcmp = hivpnatcmp;
	}
	public Long getCtcompleted() {
		return ctcompleted;
	}
	public void setCtcompleted(Long ctcompleted) {
		this.ctcompleted = ctcompleted;
	}
	public Long getUnitrecp() {
		return unitrecp;
	}
	public void setUnitrecp(Long unitrecp) {
		this.unitrecp = unitrecp;
	}
	public Long getCbuplanned() {
		return cbuplanned;
	}
	public void setCbuplanned(Long cbuplanned) {
		this.cbuplanned = cbuplanned;
	}
	public Long getPatienttyp() {
		return patienttyp;
	}
	public void setPatienttyp(Long patienttyp) {
		this.patienttyp = patienttyp;
	}
	public Long getHlacnfmchild() {
		return hlacnfmchild;
	}
	public void setHlacnfmchild(Long hlacnfmchild) {
		this.hlacnfmchild = hlacnfmchild;
	}
	public Long getCtcompletedchild() {
		return ctcompletedchild;
	}
	public void setCtcompletedchild(Long ctcompletedchild) {
		this.ctcompletedchild = ctcompletedchild;
	}
	public Long getAntigenmatchchild() {
		return antigenmatchchild;
	}
	public void setAntigenmatchchild(Long antigenmatchchild) {
		this.antigenmatchchild = antigenmatchchild;
	}
	public Long getUnitrecpchild() {
		return unitrecpchild;
	}
	public void setUnitrecpchild(Long unitrecpchild) {
		this.unitrecpchild = unitrecpchild;
	}
	public Long getCbuplannedchild() {
		return cbuplannedchild;
	}
	public void setCbuplannedchild(Long cbuplannedchild) {
		this.cbuplannedchild = cbuplannedchild;
	}
	public Long getPatienttypchild() {
		return patienttypchild;
	}
	public void setPatienttypchild(Long patienttypchild) {
		this.patienttypchild = patienttypchild;
	}
	public Long getViabchild() {
		return viabchild;
	}
	public void setViabchild(Long viabchild) {
		this.viabchild = viabchild;
	}
	public Long getPostprocesschild() {
		return postprocesschild;
	}
	public void setPostprocesschild(Long postprocesschild) {
		this.postprocesschild = postprocesschild;
	}/*
	public Boolean getPatTyping() {
		return patTyping;
	}
	public void setPatTyping(Boolean patTyping) {
		this.patTyping = patTyping;
	}
	public Boolean getPriortoprepstart() {
		return priortoprepstart;
	}
	public void setPriortoprepstart(Boolean priortoprepstart) {
		this.priortoprepstart = priortoprepstart;
	}
	public Boolean getUnitrecpflag() {
		return unitrecpflag;
	}
	public void setUnitrecpflag(Boolean unitrecpflag) {
		this.unitrecpflag = unitrecpflag;
	}
	public Boolean getCtcompetedFlag() {
		return ctcompetedFlag;
	}
	public void setCtcompetedFlag(Boolean ctcompetedFlag) {
		this.ctcompetedFlag = ctcompetedFlag;
	}
	public CdrCbu getCdrCbu() {
		return cdrCbu;
	}
	public void setCdrCbu(CdrCbu cdrCbu) {
		this.cdrCbu = cdrCbu;
	}*/
	public Long getSecondReviewBy() {
		return secondReviewBy;
	}
	public void setSecondReviewBy(Long secondReviewBy) {
		this.secondReviewBy = secondReviewBy;
	}
	public Date getSecondReviewOn() {
		return secondReviewOn;
	}
	public void setSecondReviewOn(Date secondReviewOn) {
		this.secondReviewOn = secondReviewOn;
	}
}
