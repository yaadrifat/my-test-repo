package com.velos.ordercomponent.business.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */

@Entity
@Table(name="ER_NOTIFICATION")
@SequenceGenerator(sequenceName="SEQ_ER_NOTIFICATION",name="SEQ_ER_NOTIFICATION",allocationSize=1)
public class Notification extends Auditable implements Serializable {

	private Long notifyId;
	private String notifyFrom;
	private String notifyTo;
	private String notifyCC;
	private String notifyBCC;
	private String notifySubject;
	private String notifyContent;
	private Boolean notifyIsSent;
	private String attachementQuery;
	private Auditable auditable;


    @Id
    @Column(name="PK_NOTIFICATION")
	@GeneratedValue(strategy=GenerationType.SEQUENCE , generator="SEQ_ER_NOTIFICATION")
	public Long getNotifyId() {
	   return notifyId;
    }

	public void setNotifyId(Long notifyId) {
		this.notifyId = notifyId;
	}
	
	@Column(name="NOTIFY_FROM")
	public String getNotifyFrom() {
		return notifyFrom;
	}

	public void setNotifyFrom(String notifyFrom) {
		this.notifyFrom = notifyFrom;
	}

	@Column(name="NOTIFY_TO")
	public String getNotifyTo() {
		return notifyTo;
	}

	public void setNotifyTo(String notifyTo) {
		this.notifyTo = notifyTo;
	}

	@Column(name="NOTIFY_CC")
	public String getNotifyCC() {
		return notifyCC;
	}

	public void setNotifyCC(String notifyCC) {
		this.notifyCC = notifyCC;
	}

	@Column(name="NOTIFY_BCC")
	public String getNotifyBCC() {
		return notifyBCC;
	}

	public void setNotifyBCC(String notifyBCC) {
		this.notifyBCC = notifyBCC;
	}

	@Column(name="NOTIFY_SUBJECT")
	public String getNotifySubject() {
		return notifySubject;
	}

	public void setNotifySubject(String notifySubject) {
		this.notifySubject = notifySubject;
	}

	@Column(name="NOTIFY_CONTENT")
	public String getNotifyContent() {
		return notifyContent;
	}

	public void setNotifyContent(String notifyContent) {
		this.notifyContent = notifyContent;
	}

	@Column(name="NOTIFY_ISSENT")
	public Boolean getNotifyIsSent() {
		return notifyIsSent;
	}

	public void setNotifyIsSent(Boolean notifyIsSent) {
		this.notifyIsSent = notifyIsSent;
	}

	@Column(name="ATTACHMENT_QUERY")
	public String getAttachementQuery() {
		return attachementQuery;
	}

	public void setAttachementQuery(String attachementQuery) {
		this.attachementQuery = attachementQuery;
	}
	
	/*@Embedded
	public Auditable getAuditable() {
		return auditable;
	}
	public void setAuditable(Auditable auditable) {
		this.auditable = auditable;
	}*/
	
	private Boolean deletedFlag;
	
	@Column(name="deletedflag")
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	
}