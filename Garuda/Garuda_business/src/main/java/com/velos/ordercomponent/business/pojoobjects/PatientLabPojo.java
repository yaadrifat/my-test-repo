package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;



public class PatientLabPojo extends AuditablePojo {
	
	private Long pk_PatientLabTestId; 
	private Long fk_LabTestId;
	private Long testEntityId;  
	private Long testEntityType; 
	private String testResult; 
	private Date testDate; 
	private String testResultUnit;
	private Long fk_OrderId;
	private Long fk_AttachmentId; 
	private Long reviewedBy; 

	private Boolean deletedFlag;
	
	public Long getPk_PatientLabTestId() {
		return pk_PatientLabTestId;
	}
	public void setPk_PatientLabTestId(Long pk_PatientLabTestId) {
		this.pk_PatientLabTestId = pk_PatientLabTestId;
	}
	public Long getFk_LabTestId() {
		return fk_LabTestId;
	}
	public void setFk_LabTestId(Long fk_LabTestId) {
		this.fk_LabTestId = fk_LabTestId;
	}
	public Long getTestEntityId() {
		return testEntityId;
	}
	public void setTestEntityId(Long testEntityId) {
		this.testEntityId = testEntityId;
	}
	public Long getTestEntityType() {
		return testEntityType;
	}
	public void setTestEntityType(Long testEntityType) {
		this.testEntityType = testEntityType;
	}
	public String getTestResult() {
		return testResult;
	}
	public void setTestResult(String testResult) {
		this.testResult = testResult;
	}
	public Date getTestDate() {
		return testDate;
	}
	public void setTestDate(Date testDate) {
		this.testDate = testDate;
	}
	public String getTestResultUnit() {
		return testResultUnit;
	}
	public void setTestResultUnit(String testResultUnit) {
		this.testResultUnit = testResultUnit;
	}
	public Long getFk_OrderId() {
		return fk_OrderId;
	}
	public void setFk_OrderId(Long fk_OrderId) {
		this.fk_OrderId = fk_OrderId;
	}
	public Long getFk_AttachmentId() {
		return fk_AttachmentId;
	}
	public void setFk_AttachmentId(Long fk_AttachmentId) {
		this.fk_AttachmentId = fk_AttachmentId;
	}
	public Long getReviewedBy() {
		return reviewedBy;
	}
	public void setReviewedBy(Long reviewedBy) {
		this.reviewedBy = reviewedBy;
	}
	
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	
	
}
