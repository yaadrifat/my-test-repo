package com.velos.ordercomponent.business.pojoobjects;

public class CBUStatusPojo {
	
	private Long pkcbustatus;
	private String cbuStatusCode;
	private String cbuStatusDesc;
	private String codeType;
	private String cbbCT;
	private String cbbHE;
	private String cbbOR;
	private String teminalFlag;
	private Long statusCodeSeq;
	
	
	public String getTeminalFlag() {
		return teminalFlag;
	}
	public void setTeminalFlag(String teminalFlag) {
		this.teminalFlag = teminalFlag;
	}
	public Long getPkcbustatus() {
		return pkcbustatus;
	}
	public void setPkcbustatus(Long pkcbustatus) {
		this.pkcbustatus = pkcbustatus;
	}
	public String getCbuStatusCode() {
		return cbuStatusCode;
	}
	public void setCbuStatusCode(String cbuStatusCode) {
		this.cbuStatusCode = cbuStatusCode;
	}
	public String getCbuStatusDesc() {
		return cbuStatusDesc;
	}
	public void setCbuStatusDesc(String cbuStatusDesc) {
		this.cbuStatusDesc = cbuStatusDesc;
	}
	public String getCodeType() {
		return codeType;
	}
	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}
	public String getCbbCT() {
		return cbbCT;
	}
	public void setCbbCT(String cbbCT) {
		this.cbbCT = cbbCT;
	}
	public String getCbbHE() {
		return cbbHE;
	}
	public void setCbbHE(String cbbHE) {
		this.cbbHE = cbbHE;
	}
	public String getCbbOR() {
		return cbbOR;
	}
	public void setCbbOR(String cbbOR) {
		this.cbbOR = cbbOR;
	}
	public Long getStatusCodeSeq() {
		return statusCodeSeq;
	}
	public void setStatusCodeSeq(Long statusCodeSeq) {
		this.statusCodeSeq = statusCodeSeq;
	}
		
}
