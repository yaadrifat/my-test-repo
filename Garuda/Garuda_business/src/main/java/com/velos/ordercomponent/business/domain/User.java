package com.velos.ordercomponent.business.domain;

import java.io.Serializable;
import java.util.Map;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */


public class User implements Serializable {

	private Long userId;
	private String loginName;
	private String firstName;
	private String lastName;
	private String roleRights;
	private Map moduleMap;
	private String lastLogindt;
	private String signature;
	
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getRoleRights() {
		return roleRights;
	}
	public void setRoleRights(String roleRights) {
		this.roleRights = roleRights;
	}
	public Map getModuleMap() {
		return moduleMap;
	}
	public void setModuleMap(Map moduleMap) {
		this.moduleMap = moduleMap;
	}
	public String getLastLogindt() {
		return lastLogindt;
	}
	public void setLastLogindt(String lastLogindt) {
		this.lastLogindt = lastLogindt;
	}
}
