package com.velos.ordercomponent.helper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.velos.ordercomponent.business.domain.AdditionalIds;
import com.velos.ordercomponent.business.domain.ClinicalNote;
import com.velos.ordercomponent.business.pojoobjects.AdditionalIdsPojo;
import com.velos.ordercomponent.business.pojoobjects.ClinicalNotePojo;
import com.velos.ordercomponent.business.util.HibernateUtil;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.VelosUtil;



public class ClinicalNoteHelper {
	public static final Log log=LogFactory.getLog(ClinicalNoteHelper.class);
	public static  ClinicalNotePojo saveClinicalNotes(ClinicalNotePojo clinicalNotePojo){
		return  saveClinicalNotes(clinicalNotePojo,null);
	}
	
    public static ClinicalNotePojo saveClinicalNotes(ClinicalNotePojo clinicalNotePojo,Session session){
    	ClinicalNote clinicalNote = new ClinicalNote();
    	List clinicalNoteList = new ArrayList<ClinicalNote>();
    	List<ClinicalNote> clinicalNoteList1 = new ArrayList<ClinicalNote>();
    	Long size=1l;
    	String size1="";
    	Long entityId = clinicalNotePojo.getEntityId();
    	Long noteCategory = clinicalNotePojo.getFkNotesType();
    	String noteSeq1 = null;
    	noteSeq1= clinicalNotePojo.getNoteSeq();
    	
    	/*String notes = clinicalNotePojo.getNotes();
    	notes = notes.replaceAll("<BR>", " ");
    	clinicalNotePojo.setNotes(notes);*/
    	//<(\\S+)[^>]+?mso-[^>]*>.*?</\\1>
    	String notes = clinicalNotePojo.getNotes();
    	notes = notes.replaceAll("\\<.*?\\>", " ");
    	clinicalNotePojo.setNotes(notes);
    	
    	
    	
    	boolean sessionFlag = false;		
		try{
			if(session== null){
				// create new session
			  session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			
			if(noteSeq1==null||noteSeq1==""||noteSeq1.equals(""))
			{	
					String sql = "select distinct(lpad(max(noteSeq)+1,3,'0')) from ClinicalNote where entityId = "+entityId+" and fkNotesType = "+noteCategory;
					clinicalNoteList =  session.createQuery(sql).list();
					
					if(clinicalNoteList.get(0)==null)
					{						
						size1 = "00" + Long.valueOf(1l);
						clinicalNoteList = null;
					}
					
			}
			
			else
			{
				size1 = clinicalNotePojo.getNoteSeq();
				clinicalNoteList=null;
			}
			clinicalNote = (ClinicalNote)ObjectTransfer.transferObjects(clinicalNotePojo, clinicalNote);
			clinicalNote = (ClinicalNote)new VelosUtil().setAuditableInfo(clinicalNote);
			if(clinicalNote.getPkNotes()!=null){
				session.merge(clinicalNote);
			}else{
				if(clinicalNoteList!=null)
		    	{
		    			size1 = clinicalNoteList.get(0).toString();
		    			
		    	}
				clinicalNote.setNoteSeq(size1);	
	    	
			session.save(clinicalNote);
		}	
		if(sessionFlag){
			// close session, if session created in this method
			session.getTransaction().commit();
		}		
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		clinicalNotePojo = (ClinicalNotePojo)ObjectTransfer.transferObjects(clinicalNote, clinicalNotePojo);
		
		return clinicalNotePojo;		
	}
    
    
    public static List<ClinicalNotePojo> updateClinicalNotes(List<ClinicalNotePojo> cnPojos){
    	return updateClinicalNotes(cnPojos, null);
    }
    
    
    public static List<ClinicalNotePojo> updateClinicalNotes(List<ClinicalNotePojo> cnPojos , Session session){
    	List<ClinicalNote> clinicalNotes = new ArrayList<ClinicalNote>();
    	ClinicalNote clinicalNote = null;
    	ClinicalNotePojo cnPojo = null;
    	
    	for(ClinicalNotePojo cn : cnPojos){
    		clinicalNote = new ClinicalNote();
    		clinicalNote = (ClinicalNote)ObjectTransfer.transferObjects(cn, clinicalNote);
    		clinicalNote = (ClinicalNote) new VelosUtil().setAuditableInfo(clinicalNote);
    		clinicalNotes.add(clinicalNote);
    	}
    	
    	boolean sessionFlag = false;
    	try {
			if (session == null) {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			for(ClinicalNote c : clinicalNotes){
				if(c.getPkNotes()  !=null){
					session.merge(c);
				}else{
					//session.save(c);
				}
			}
			
			if(clinicalNotes!=null && clinicalNotes.size()>0){
				cnPojos.clear();
				for(ClinicalNote c1 : clinicalNotes){
					cnPojo = (ClinicalNotePojo)ObjectTransfer.transferObjects(c1, cnPojo);
					cnPojos.add(cnPojo);
		    	}
			}
			if(sessionFlag){
				session.getTransaction().commit();
			}
    	} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {		
			 if(session.isOpen()){
			 session.close();			
			 }
		}
		return cnPojos;
    }
    
    
    
   /* public static  List<ClinicalNotePojo> clinicalNoteHistory(Long cordId) {
		return  clinicalNoteHistory(cordId,null);
	}*/
    
  /* public static List<ClinicalNotePojo> clinicalNoteHistory(Long cordId , Session session) {
	   ClinicalNote clinicalNote = new ClinicalNote();
	   ClinicalNotePojo clinicalNotePojo;
	   List<ClinicalNote> clinicalNotes = new ArrayList<ClinicalNote>();
	   List<ClinicalNotePojo> clinicalNotePojos = new ArrayList<ClinicalNotePojo>();
	   boolean sessionFlag = false;		
		try{
			if(session== null){
				// create new session
			    session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			//String sql = "from ClinicalNote where entityId = "+cordId+"order by createdOn desc";
			String sql="";
			
			if(cordId!=null){
				//log.debug("------------cord id in clinicalNoteHistory "+cordId);
				sql = "from ClinicalNote where entityId = "+cordId+"  order by createdOn desc";
				/*sql = "select {A.*} from (select {a.*} from ClinicalNote a order by a.createdOn desc )A where a.entityId ="+cordId+
						"  order by A.fkNotesCategory desc";*/
					/*SELECT * FROM
					(
					SELECT  * FROM cb_notes ORDER BY CREATED_ON  DESC
					) A
					where ENTITY_ID = 2200 ORDER BY A.FK_NOTES_CATEGORY desc ;*/
			//}
			/*else{
				sql = "from ClinicalNote where entityId = "+cordId+"  order by createdOn desc";
			}
			clinicalNotes = (List<ClinicalNote>) session.createQuery(sql).list();
			//log.debug("After query building");
			//log.debug(clinicalNotes);
			 for(ClinicalNote c : clinicalNotes){ 
				clinicalNotePojo = new ClinicalNotePojo();
				clinicalNotePojo = (ClinicalNotePojo)ObjectTransfer.transferObjects(c, clinicalNotePojo);
				clinicalNotePojos.add(clinicalNotePojo);
			 }
			}catch (Exception e) {
				log.error("Exception in Clinical Helper : saveClinicalNotes :: " + e);
				e.printStackTrace();
			}finally{
				if(session.isOpen()){
					session.close();
				}
			}		
		return clinicalNotePojos;
	}*/
	
   public static List<ClinicalNotePojo>  amendNotes(Long pkNote){
		return  amendNotes(pkNote,null);
	}
   
   public static List<ClinicalNotePojo> amendNotes(Long pkNote , Session session) {
	   ClinicalNote clinicalNote = new ClinicalNote();
	   ClinicalNotePojo clinicalNotePojo;
	   List<ClinicalNote> clinicalNotes = new ArrayList<ClinicalNote>();
	   List<ClinicalNotePojo> clinicalNotePojos = new ArrayList<ClinicalNotePojo>();
	   boolean sessionFlag = false;		
		try{
			if(session== null){
				// create new session
			    session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "from ClinicalNote where pk_notes = "+pkNote;
			
			clinicalNotes = (List<ClinicalNote>) session.createQuery(sql).list();
			//log.debug(clinicalNotes);
			 for(ClinicalNote c : clinicalNotes){ 
				clinicalNotePojo = new ClinicalNotePojo();
				clinicalNotePojo = (ClinicalNotePojo)ObjectTransfer.transferObjects(c, clinicalNotePojo);
				clinicalNotePojos.add(clinicalNotePojo);
			 }
			
			}catch (Exception e) {
				log.error(e.getMessage());
				e.printStackTrace();
			}finally{
				if(session.isOpen()){
					session.close();
				}
			}		
		return clinicalNotePojos;
	}
   
   public static String getPkCategoryId(String categoryName) throws Exception{
		
		String query="";
	
		query="select b.pkCodeId from CodeList b where lower(b.type)='note_cat' and lower(b.subType) = " + "'"+ categoryName + "'";
		
		
		//return velosHelper.getObjectList(query);
		return query;
	}
   
   public static String getNotesDetails(Long entityId) throws Exception{
		String criteria = "";
		if(entityId !=null && entityId >0 )
		{
			criteria ="Select note.createdOn,note.createdBy,note.noteSeq,note.noteAssessment,note.visibility, note.amended, note.pkNotes  from ClinicalNote note where note.entityId="+entityId ;
		}
		return criteria;
	}	
   
   public static Map<Long,List> getNotes(Long entityId){
   	return getNotes(entityId,null);
   }
   
   public static Map<Long,List> getNotes(Long entityId,Session session){
	List<ClinicalNote> clinicalNotes = new ArrayList<ClinicalNote>();
   	boolean sessionFlag = false;
   	List list = null;
   	List categoryList ;
   	Map<Long,List> map = new HashMap<Long,List>();
   	try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			
			String sql = "select {c.*} from CB_NOTES c left join er_codelst e on c.FK_NOTES_CATEGORY=e.PK_CODELST where c.ENTITY_ID=? and lower(e.CODELST_TYPE)='note_cat' order by c.NOTE_SEQ desc, c.PK_NOTES asc";
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity("c", ClinicalNote.class);
			query.setLong(0, entityId);			
			clinicalNotes = (List<ClinicalNote>)query.list();
			
   	} 
   	catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}finally {		
			 if(session.isOpen()){
			 session.close();			
			 }
		}
		if(clinicalNotes!=null  && clinicalNotes.size()>0){
			for(ClinicalNote c : clinicalNotes){
				Object[] ob = new Object[11];
				ob[0] = c.getCreatedBy();
				ob[1] = c.getCreatedOn();
				ob[2] = c.getNoteSeq();
				ob[3] = c.getNoteAssessment();
				ob[4] = c.getVisibility();
				ob[5] = c.getAmended();		
				ob[6] = c.getPkNotes();
				ob[7] = c.getFkNotesType();
				ob[8] = c.getReply();
				ob[9] = c.getNotes();
				ob[10]= c.getFlagForLater();
				if(map.containsKey(c.getFkNotesCategory())){
					categoryList = map.get(c.getFkNotesCategory());
					categoryList.add(ob);
				}else{
					categoryList = new ArrayList();
					categoryList.add(ob);
					map.put(c.getFkNotesCategory(), categoryList);
				}
			}
		}		
		
   	return map;
   }
   
   public static Map<Long,List> getPfNotes(Long entityId,Long requestType){
	   	return getPfNotes(entityId,requestType,null);
	   }
	   
	   public static Map<Long,List> getPfNotes(Long entityId,Long requestType,Session session){
		List<ClinicalNote> clinicalNotes = new ArrayList<ClinicalNote>();
	   	boolean sessionFlag = false;
	   	List list = null;
	   	List categoryList ;
	   	Map<Long,List> map = new HashMap<Long,List>();
	   	try {
				if (session == null) {
					// create new session
					session = HibernateUtil.getSessionFactory().getCurrentSession();
					sessionFlag = true;
					session.beginTransaction();
				}
				
				String sql = "select {c.*} from CB_NOTES c left join er_codelst e on c.FK_NOTES_CATEGORY=e.PK_CODELST where c.ENTITY_ID=? and lower(e.CODELST_TYPE)='note_cat_pf' and c.REQUEST_TYPE=? order by c.NOTE_SEQ desc, c.PK_NOTES asc";
			
				SQLQuery query = session.createSQLQuery(sql);
				query.addEntity("c", ClinicalNote.class);
				if(entityId!=null){
					query.setLong(0, entityId);
				}else{
					query.setLong(0, 0);
				}
				if(requestType!=null){
					query.setLong(1, requestType);
				}
				else{
					query.setLong(1, 0);
				}
				clinicalNotes = (List<ClinicalNote>)query.list();
				//log.debug("pf notes size!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"+clinicalNotes.size());
				
				
	   	} 
	   	catch (Exception e) {
			log.error(e.getMessage());
				e.printStackTrace();
			}finally {		
				 if(session.isOpen()){
				 session.close();			
				 }
			}
			if(clinicalNotes!=null  && clinicalNotes.size()>0){
				for(ClinicalNote c : clinicalNotes){
					Object[] ob = new Object[9];
					ob[0] = c.getCreatedOn();
					ob[1] = c.getCreatedBy();
					ob[2] = c.getNoteSeq();
					ob[3] = c.getSubject();
					ob[4] = c.getNoteVisible();
					ob[5] = c.getPkNotes();
					ob[6] = c.getAmended();	
					ob[7]=  c.getTagNote();
					ob[8] = c.getNotes();
					if(map.containsKey(c.getFkNotesCategory())){
						categoryList = map.get(c.getFkNotesCategory());
						categoryList.add(ob);
					}else{
						categoryList = new ArrayList();
						categoryList.add(ob);
						map.put(c.getFkNotesCategory(), categoryList);
					}
				}
			}		
			
	   	return map;
	   }
	   
   
   public static String getNotesByCategory(Long entityId,Long entityTypeId,Long categoryTypeID,String categoryName) throws Exception {
		
	   
	   //log.debug("Inside notes Category in helper!!!!!!!!!!!!!!!!1");
		String appendquery="";
		String notesInfo = "";
		
		if(categoryName!=null) {
			appendquery = getPkCategoryId(categoryName);
		
		}
		notesInfo = getNotesDetails(entityId);
		notesInfo = notesInfo + " and note.fkNotesCategory in (" + appendquery + ")";
		
		
		return notesInfo;
	}
   
   public static  ClinicalNotePojo viewClinicalNotes(Long pkNotes){
		return  viewClinicalNotes(pkNotes,null);
	}
   
  public static ClinicalNotePojo viewClinicalNotes(Long pkNotes,Session session){

   	ClinicalNote clinicalNote = new ClinicalNote();
   	List<ClinicalNote> clinicalNoteList = new ArrayList<ClinicalNote>();
  
   	boolean sessionFlag = false;		
		try{
			if(session== null){
				// create new session
			  session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
				String sql = "from ClinicalNote where pkNotes = "+pkNotes;
	    	
				clinicalNoteList = (List)session.createQuery(sql).list();
		
		if(sessionFlag){
			// close session, if session created in this method
			//log.debug("Commit Statement!!!!!!!!!!!!!!!!!");
			session.getTransaction().commit();
		}		
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}	
		if(clinicalNoteList!=null){
			clinicalNote = clinicalNoteList.get(0);
			}
			ClinicalNotePojo clinicalNotePojo= new ClinicalNotePojo();
			clinicalNotePojo = (ClinicalNotePojo)ObjectTransfer.transferObjects(clinicalNote,  clinicalNotePojo);
		
		return clinicalNotePojo;		
	}
  
  	public static  List<ClinicalNotePojo> clinicalfilter(String keyword, Long type, Long cordId) {
		return  clinicalfilter(keyword,type, cordId, null);
	}
   
  	public static List<ClinicalNotePojo> clinicalfilter(String keyword, Long type , Long cordId, Session session) {
  		ClinicalNote clinicalNote = new ClinicalNote();
 	   ClinicalNotePojo clinicalNotePojo;
 	   List<ClinicalNote> clinicalNotes = new ArrayList<ClinicalNote>();
 	   List<ClinicalNotePojo> clinicalNotePojos = new ArrayList<ClinicalNotePojo>();
 	   String temp[];
 	   String comma=",";
 	   String sql = null;
 	  String sql1 = "lower(keyword) like ";
 	   String delimiter = ",";
 	   String codes = "&";
 	   temp = keyword.split(comma);
 	   
 	  	keyword = " ";
 	  	sql = "keyword IS NULL OR lower(keyword) like ";
 	  	

 	  	for(int i =0; i < temp.length ; i++)
 	  	{
 	  		temp[i] = temp[i].toLowerCase();
 	  		int j = i+1;
 	  		if(j==temp.length)
 	  			sql = sql +  "'%" + temp[i] + "%' " ;
 	  		else
 	  		sql = sql +  "'%" + temp[i] + "%'" + " or " + sql1;
 	  	 	 
 	  	 	
 	  	}

 	  	boolean sessionFlag = false;		
 		try{
 			if(session== null){
 				// create new session
 			    session = HibernateUtil.getSessionFactory().getCurrentSession();
 				sessionFlag = true;
 				session.beginTransaction();
 			}
 			if(type==null)
 			{
 				sql = "from ClinicalNote where ( " +sql+ " ) and (entityId="+cordId+")";
 			}
 			else 
 			{
 				sql = "from ClinicalNote where ( " +sql+ " ) and ( fkNotesType="+type+" ) and (entityId="+cordId+")";
 			}
 			//log.debug("Sql in helper is!!!!!!!!!!!!!"+sql);
 			clinicalNotes = (List<ClinicalNote>) session.createQuery(sql).list();
 			 for(ClinicalNote c : clinicalNotes){ 
 				clinicalNotePojo = new ClinicalNotePojo();
 				clinicalNotePojo = (ClinicalNotePojo)ObjectTransfer.transferObjects(c, clinicalNotePojo);
 				clinicalNotePojos.add(clinicalNotePojo);
 			 }
 			}catch (Exception e) {
 				log.error(e.getMessage());
 				e.printStackTrace();
 			}finally{
 				if(session.isOpen()){
 					session.close();
 				}
 			}		
 		return clinicalNotePojos;
 	}
 	
  	public static  List<ClinicalNotePojo> clinicalNoteHistory(Long cordId, Long noteCategory, Long requestId) {
		return  clinicalNoteHistory(cordId,noteCategory,requestId, null);
	} 
    
   public static List<ClinicalNotePojo> clinicalNoteHistory(Long cordId , Long noteCategory, Long requestId, Session session) {
	   ClinicalNote clinicalNote = new ClinicalNote();
	   ClinicalNotePojo clinicalNotePojo;
	   List<ClinicalNote> clinicalNotes = new ArrayList<ClinicalNote>();
	   List<ClinicalNotePojo> clinicalNotePojos = new ArrayList<ClinicalNotePojo>();
	   boolean sessionFlag = false;		
		try{
			if(session== null){
				// create new session
			    session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			//String sql = "from ClinicalNote where entityId = "+cordId+"order by createdOn desc";
			String sql="";
			if(noteCategory!=null)
			{
			if(cordId!=null){
				if(requestId!=null)
				{
					sql = "from ClinicalNote where entityId = "+cordId+" and fkNotesType = "+noteCategory+" and requestType ="+requestId+" order by noteSeq desc, pkNotes asc";
				}
				else
					{
					sql = "from ClinicalNote where entityId = "+cordId+" and fkNotesType = "+noteCategory+" order by noteSeq desc, pkNotes asc";
					}
				}
			else{
				if(requestId!=null)
				{
					sql = "from ClinicalNote where entityId = "+cordId+" and fkNotesType = "+noteCategory+" and requestType ="+requestId+" order by createdOn desc, pkNotes asc";
				}
				else
				{
				sql = "from ClinicalNote where entityId = "+cordId+" and fkNotesType = "+noteCategory+" order by noteSeq desc, pkNotes asc";
				}
			}
			}
			else
			{
				sql = "from ClinicalNote where entityId = "+cordId+" order by noteSeq desc, pkNotes asc";
				
			}
			clinicalNotes = (List<ClinicalNote>) session.createQuery(sql).list();
			 for(ClinicalNote c : clinicalNotes){ 
				clinicalNotePojo = new ClinicalNotePojo();
				clinicalNotePojo = (ClinicalNotePojo)ObjectTransfer.transferObjects(c, clinicalNotePojo);
				clinicalNotePojos.add(clinicalNotePojo);
			 }
			}catch (Exception e) {
				log.error(e.getMessage());
				e.printStackTrace();
			}finally{
				if(session.isOpen()){
					session.close();
				}
			}		
		return clinicalNotePojos;
	}
   

	public static String populateUserValues(Long cordId) throws Exception {
		String criteria = "";
		//log.debug("Populate values!!!!!!!!!!!!!!!!");
		criteria = "From UserDomain users where userId in(select fkUser from UserSite where fkSite in (select fkCbbId from CdrCbu where cordID="+cordId+"))";
			//log.debug("After critereia!!!!!!!!!!!!!!!");	
		return criteria;
	}	
	/*public static  ClinicalNotePojo saveClinicalNotesAssessment(ClinicalNotePojo clinicalNotePojo){
		return  saveClinicalNotes(clinicalNotePojo,null);
	}
	 public static ClinicalNotePojo saveClinicalNotesAssessment(ClinicalNotePojo clinicalNotePojo,Session session){
	    	//log.debug("Saving!!!!!!!!!!!!!!!!!!!!!!!!");
	    	ClinicalNote clinicalNote = new ClinicalNote();
	    	List<ClinicalNote> clinicalNoteList = new ArrayList<ClinicalNote>();
	    	Long size=1l;
	    	Long entityId = clinicalNotePojo.getEntityId();
	    	Long noteCategory = clinicalNotePojo.getFkNotesType();
	    	Long noteSeq = Long.valueOf(clinicalNotePojo.getNoteSeq());
	    	//log.debug("Note sequecne for save!!!!!!!!!!!"+noteSeq);
	    	boolean sessionFlag = false;		
			try{
				if(session== null){
					// create new session
				  session = HibernateUtil.getSessionFactory().getCurrentSession();
					sessionFlag = true;
					session.beginTransaction();
				}
				
				if(noteSeq==null)
				{	
						String sql = "select distinct(noteSeq) from ClinicalNote where entityId = "+entityId+ "and fkNotesType = "+noteCategory;
						clinicalNoteList = (List<ClinicalNote>) session.createQuery(sql).list();
				}
				else
				{
					size = noteSeq;
					clinicalNoteList=null;
				}
				if(clinicalNotePojo.getPkNotes()==null){
					//log.debug("Executed only if pk is null!!!!!!");
				
		    	if(clinicalNoteList!=null)
		    	{
		    		size = clinicalNoteList.size();
		    		//log.debug(size);
		    		size++;
		    	
		    	}
		    	clinicalNotePojo.setNoteSeq((long) (size));
				}
				else
				{
					size = clinicalNoteList.size();
					clinicalNotePojo.setNoteSeq((long) (size));
				}
		    	//log.debug("After  building!!!!!!!!1");
		    	
				clinicalNote = (ClinicalNote)ObjectTransfer.transferObjects(clinicalNotePojo, clinicalNote);
				clinicalNote = (ClinicalNote)new VelosUtil().setAuditableInfo(clinicalNote);
				if(clinicalNote.getPkNotes()!=null){
					//log.debug("Pk existing!!!!!!!!!!!!!!!");
					session.merge(clinicalNote);
				}else{
					if(clinicalNoteList!=null)
			    	{
			    		size = (long) clinicalNoteList.size();
			    		size++;
			    	
			    	}
		    	clinicalNote.setNoteSeq(size);
		    	//log.debug("Commit Statement for cord entry!!!!!!!!!!!!!!!!!"+size);
				session.save(clinicalNote);
			}	
			if(sessionFlag){
				// close session, if session created in this method
				session.getTransaction().commit();
			}		
			}catch (Exception e) {
				log.error("Exception in Clinical Helper : saveClinicalNotes :: " + e);
				e.printStackTrace();
			}finally{
				if(session.isOpen()){
					session.close();
				}
			}
			clinicalNotePojo = (ClinicalNotePojo)ObjectTransfer.transferObjects(clinicalNote, clinicalNotePojo);
			
			return clinicalNotePojo;		
		}
	    */
	 public static String finalReviewStatus(Long cordId){
		 //log.debug("Inside finalreview status helper1!!!!!!!!!!!!!!!!!!!!!!!!!");
			return finalReviewStatus(cordId,null);
		}
		
		public static String finalReviewStatus(Long cordId,Session session){
			//log.debug("Inside finalreview status helper2!!!!!!!!!!!!!!!!!!!!!!!");
			String code="";
			List temp=new ArrayList();
			try{
				if(session==null){
					session=HibernateUtil.getSessionFactory().getCurrentSession();
					session.beginTransaction();
				}
				String qry="select FINAL_REVIEW_CONFIRM_FLAG from CB_CORD_FINAL_REVIEW where FK_CORD_ID ="+cordId;
				temp=session.createSQLQuery(qry).list();
				if(temp!=null && temp.size()>0){
				Object obj=temp.get(0);
				code = obj.toString();
				}
				//log.debug("Review status code!!!!!!!!!!!!!! : "+code);
			}catch (Exception e) {
				// TODO: handle exception
				log.error(e.getMessage());
				e.printStackTrace();
			}
			finally{
				if(session.isOpen()){
					session.close();
				}
			}
			if(code.equals("Y"))
			{
				//log.debug("Inside yes of helper!!!!!!!!!!!!!!!!!!!!!!1");
				code ="Y";
			}
			else
			{
				//log.debug("Inside no of helper!!!!!!!!!!!!!!!!!!!!!!1");
				code ="N";
			}
			return code;
		}
		
		public static List<ClinicalNotePojo> saveCordMultipleNote(
				List<ClinicalNotePojo> clinicalNotePojo,Boolean fromUI,Long entityId, Long noteCategory) {
			return saveCordMultipleNote(null, clinicalNotePojo,fromUI,entityId,noteCategory);
		}

		public static List<ClinicalNotePojo> saveCordMultipleNote(
				Session session, List<ClinicalNotePojo> clinicalNotePojo,Boolean fromUI,Long entityId, Long noteCategory) {
			boolean sessionFlag = false;
			System.out.println("saveCordMultipleNote!@@@@@@@@@@@@@@@@@@@@@@@@@@2");
			List<ClinicalNote> infoClinicalIds = new ArrayList<ClinicalNote>();
			ClinicalNote clinicalNote = null;
			ClinicalNotePojo infoClinicalIdsPojo = null;
			List clinicalNoteList = new ArrayList<ClinicalNote>();
			try {
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "select distinct(lpad(max(noteSeq)+1,3,'0')) from ClinicalNote where entityId = "+entityId+" and fkNotesType = "+noteCategory;
			clinicalNoteList =  session.createQuery(sql).list();
			String size=null;
			Long size1;
			if(clinicalNoteList.get(0)==null)
			{		System.out.println("inisde clinicalNoteList.get(0)!!!!!!!!!!!!!!!!!!!!1");				
				size = "00" + Long.valueOf(1l);
				
			}
			else
			{
				size= clinicalNoteList.get(0).toString();
			}
			//Long count = (Long) clinicalNoteList.get(0);
			System.out.println("Inside multiple helper!!!!!!!!!!!!!!!!!!!!!!!!!1");
			for (ClinicalNotePojo a : clinicalNotePojo) {
			System.out.println("saveCordMultipleNote inside for!!!!!!!!!!!!!!!!!!!!!!!1"+clinicalNoteList.get(0));
				clinicalNote = new ClinicalNote();
				clinicalNote.setNoteSeq(size);
				System.out.println("clinicalNote.setNoteSeq@@@@@@"+clinicalNote.getNoteSeq());
				size1 = Long.valueOf(size);
				clinicalNote = (ClinicalNote) ObjectTransfer.transferObjects(a,
						clinicalNote);
				if(!fromUI){
					clinicalNote = (ClinicalNote) new VelosUtil()
						.setAuditableInfo(clinicalNote);
				}
				infoClinicalIds.add(clinicalNote);
				size1++;
				if(size1<10)
				size = "00" +size1;
				else if(size1>=10 && size1<100 )
					size = "0" +size1;
				else
					size =String.valueOf(size1);
				System.out.println("Size is !!!!!!!!!!!"+size);
			}
			
				
				for (ClinicalNote aid : infoClinicalIds) {
					System.out.println("saveCordMultipleNote inside save for!!!!!!!!!!!!!!!!!!!!!!1");
					if (aid.getPkNotes() != null) {
						System.out.println("merge!!!!!!!!!!!!!!!!!!!1");
						session.merge(aid);
					} else {
						System.out.println("save!!!!!!!!!!!!!!!!!!!1"); 
						session.save(aid);
					}
				}
				if (infoClinicalIds != null && infoClinicalIds.size() > 0) {
					clinicalNotePojo.clear();
					for (ClinicalNote a : infoClinicalIds) {
						infoClinicalIdsPojo = new ClinicalNotePojo();
						infoClinicalIdsPojo = (ClinicalNotePojo) ObjectTransfer
								.transferObjects(a, infoClinicalIdsPojo);
						clinicalNotePojo.add(infoClinicalIdsPojo);
					}
				}
				if (sessionFlag) {
					session.getTransaction().commit();
				}
			} catch (Exception e) {
				//log.debug(" Exception in saveCordinfoClinicalIds " + e);
				log.error(e.getMessage());
				e.printStackTrace();
			} finally {
				if (session.isOpen()) {
					session.close();
				}
			}
			return clinicalNotePojo;
		}

		public static String lastCBUStatus(Long cordId){
			 //log.debug("Inside finalreview status helper1!!!!!!!!!!!!!!!!!!!!!!!!!");
				return lastCBUStatus(cordId,null);
			}
			
			public static String lastCBUStatus(Long cordId,Session session){
				//log.debug("Inside finalreview status helper2!!!!!!!!!!!!!!!!!!!!!!!");
				String status="";
				List temp=new ArrayList();
				try{
					if(session==null){
						session=HibernateUtil.getSessionFactory().getCurrentSession();
						session.beginTransaction();
					}
					String qry="select count(*) from cb_notes where entity_id ="+cordId+ " and note_assessment = 'na'";
					
					
					
					temp=session.createSQLQuery(qry).list();
					if(temp!=null && temp.size()>0 && ((BigDecimal) temp.get(0)).intValue()!=0){
						status = "Y";
					}
					else
					{
						status = "N";
					}
					//log.debug("Review status code!!!!!!!!!!!!!! : "+code);
				}catch (Exception e) {
					// TODO: handle exception
					log.error(e.getMessage());
					e.printStackTrace();
				}
				finally{
					if(session.isOpen()){
						session.close();
					}
				}
			
				return status;
			}
		
}
