package com.velos.ordercomponent.business.xmlpojos;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class Status {

	private String cbb_lcl_registry_sts;
	private String licensure_status;
	private List<String> unlicensed_reason;
	private String cbu_location;
	
	@XmlElement(name="cbb_lcl_registry_sts")
	public String getCbb_lcl_registry_sts() {
		return cbb_lcl_registry_sts;
	}
	public void setCbb_lcl_registry_sts(String cbbLclRegistrySts) {
		cbb_lcl_registry_sts = cbbLclRegistrySts;
	}
	
	@XmlElement(name="licensure_status")
	public String getLicensure_status() {
		return licensure_status;
	}
	public void setLicensure_status(String licensureStatus) {
		licensure_status = licensureStatus;
	}
	
	@XmlElement(name="unlicensed_reason")
	public List<String> getUnlicensed_reason() {
		return unlicensed_reason;
	}
	public void setUnlicensed_reason(List<String> unlicensedReason) {
		unlicensed_reason = unlicensedReason;
	}
	
	@XmlElement(name="cbu_location")
	public String getCbu_location() {
		return cbu_location;
	}
	public void setCbu_location(String cbuLocation) {
		cbu_location = cbuLocation;
	}
	
	
	
}
