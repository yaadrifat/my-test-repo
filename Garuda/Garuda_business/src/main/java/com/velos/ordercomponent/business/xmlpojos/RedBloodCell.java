package com.velos.ordercomponent.business.xmlpojos;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class RedBloodCell {

	private String inher_rbc_dises_ind;
	/*private Relations relations;
	@XmlElement(name="Relations")
	public Relations getRelations() {
		return relations;
	}
	public void setRelations(Relations relations) {
		this.relations = relations;
	}*/
	@XmlElement(name="inher_rbc_dises_ind")
	public String getInher_rbc_dises_ind() {
		return inher_rbc_dises_ind;
	}
	public void setInher_rbc_dises_ind(String inherRbcDisesInd) {
		inher_rbc_dises_ind = inherRbcDisesInd;
	}
	private List<Relations> Relations;
	@XmlElement(name="Relations")
	public List<Relations> getRelations() {
		return Relations;
	}
	public void setRelations(List<Relations> relations) {
		Relations = relations;
	}
	
}
