package com.velos.ordercomponent.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.helper.VDAHelper;
import com.velos.ordercomponent.service.VelosVDAService;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class VelosVDAServiceImpl implements VelosVDAService {
	public static final Log log = LogFactory.getLog(VelosVDAServiceImpl.class);
	public static VelosVDAService vdaService;
	private VelosVDAServiceImpl(){}
	public static VelosVDAService getVDAService(){
		try{
			if(vdaService==null){
				vdaService = new VelosVDAServiceImpl();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return vdaService;
	}
	
	public List<Object> getQueryBuilderViewData(String sql,PaginateSearch paginateSearch){
		return VDAHelper.getQueryBuilderViewData(sql, paginateSearch);
	}
	
	public List getQueryBuilderViewData(String sql){
		return VDAHelper.getQueryBuilderViewData(sql);
	}
	
	public Map getQueryBuilderRecordDetails(String sql){

		return VDAHelper.getQueryBuilderRecordDetails(sql);
	}
	
	public Map getQueryBuilderHlaData(String sql){

		return VDAHelper.getQueryBuilderHlaData(sql);
	}
	
	public Map getQueryBuilderBestHlaData(String sql){

		return VDAHelper.getQueryBuilderBestHlaData(sql);
	}
	
	public Map getQueryBuilderBestHlaFilterData(String sql,  Map<Long, String> cdlst){

		return VDAHelper.getQueryBuilderBestHlaFilterData(sql, cdlst);
	}
	
	public List getQueryBuilderCordHlaData(String sql){

		return VDAHelper.getQueryBuilderCordHlaData(sql);
	}
	
	public Map getQueryBuilderHlaFilterData(String sql, Map<Long, String> cdlst) {
		
		return VDAHelper.getQueryBuilderHlaFilterData(sql, cdlst);	
	}
	
	public List getQueryBuilderListObject(String sql){
		
		return VDAHelper.getQueryBuilderListObject(sql);		
	}
	
	public List getQueryBuilderCordIdObject(String sql){
		
		return VDAHelper.getQueryBuilderCordIdObject(sql);		
	}
	
	public Map getQueryBuilderMapObject(String sql){
		
		return VDAHelper.getQueryBuilderMapObject(sql);		
	}
	
	public Map getLicEligMapObject(String sql, Map<Long, Object []> cord, Map<Long, ArrayList<Long>> reason){
		
		return VDAHelper.getLicEligMapObject(sql, cord,  reason);		
	}
	
	public Map getQueryBuilderMapLstObject(String sql){
		
		return VDAHelper.getQueryBuilderMapLstObject(sql);
	}
}
