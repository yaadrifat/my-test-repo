package com.velos.ordercomponent.business.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.ordercomponent.business.domain.Widget;

/**
 * @author Mohiuddin Ali Ahmed
 *
 */
@Entity
@Table(name="UI_PAGE")
@SequenceGenerator(sequenceName="seq_UI_PAGE",name="seq_UI_PAGE",allocationSize=1)
public class Page {

	private Long pageId;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="seq_UI_PAGE")
	@Column(name="PK_PAGE")
	public Long getPageId() {
		return pageId;
	}

	public void setPageId(Long pageId) {
		this.pageId = pageId;
	}
	
	private Long pageCode;
	private String pageName;
	private String helpUrl;
    private Widget widget;    
	
	@Column(name="PAGE_CODE")
	public Long getPageCode() {
		return pageCode;
	}
	public void setPageCode(Long pageCode) {
		this.pageCode = pageCode;
	}

	@Column(name="PAGE_NAME")
	public String getPageName() {
		return pageName;
	}
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	@Column(name="HELP_URL")
	public String getHelpUrl() {
		return helpUrl;
	}
	public void setHelpUrl(String helpUrl) {
		this.helpUrl = helpUrl;
	}

	@Transient
	public Widget getWidget() {
		return widget;
	}

	public void setWidget(Widget widget) {
		this.widget = widget;
	}	
	
	
}
