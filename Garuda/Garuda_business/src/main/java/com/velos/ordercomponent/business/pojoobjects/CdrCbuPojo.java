package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;

import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;


import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.ValidationParameter;
import com.opensymphony.xwork2.validator.annotations.Validations;
import com.velos.ordercomponent.business.domain.RecipientInfo;
import com.velos.ordercomponent.business.domain.Site;
import com.velos.ordercomponent.business.domain.Specimen;
import com.opensymphony.xwork2.validator.annotations.*;

public class CdrCbuPojo extends AuditablePojo {
	
	private Long cordID;
	private String cdrCbuId;
	private String externalCbuId;
	private Long tncFrozen;
	private Long cellCount;
	private Long frozenPatWt;
	private Date birthDate;
	private String registryId;
	private String localCbuId;
	private String historicLocalCbuId;
	private String numberOnCbuBag;
	private Date creationDate;
	private String cdrCbuCreatedBy;
	private Date cdrCbuLstModDate;
	private String cdrCbuLstModBy;
	private Long bacterialResult;
	private Long fungalResult;
	private Long aboBloodType;
	private Long rhType;
	private Long babyGenderId;
	private Long clinicalStatus;
	private Long cbuLicStatus;
	private String addInfoReq;
	private String rhTypeDes;
	private String bloodGpDes;
	private String license;
	private String eligibilitydes;	
	private Long fkCbbId;
	private String cordIsbiDinCode;
	private String cordIsitProductCode;
	private String cordDonorIdNumber;
	private Long fkCordCbuEligible;
	private Long fkCordCbuStatus;
	private Long cordnmdpstatus;
	private Date cordavaildate;
	private String CordCbuStatuscode;
	private String cordavaildatestr;
	private Long fkCordCbuEligibleReason;
	private Long fkCordCbuUnlicenseReason;	
	private String inEligibleReason;
	private String unlicenseReason;
	private String cord_Add_info;
	private int unLicesePkid;
	private int inEligiblePkid;
	private int defferedPkCode;
	private Long fkSpNMDPId;
	private Long fkSpOtherId;
	private Long fkNmdpNameId;
	private Long fkNmdpNumId;
	private String spOtherName;
	private String spOtherNum;	
	private int pkNMDPSponsorId;
	private int pkOtherSponsorId;	
	private String nmdpNameDesc;
	private String nmdpNumDesc;
	private int notActiveId;
	private int tempUnavail;
	private String clinicalStatReason;
	private Date clinicalStatDate;
	private String cordCbuStatusDesc;
	private String cordnmdbStatusDesc;
	private String registryMaternalId;
	private String localMaternalId;
	private String ethnicity;
	private String race;
	private Boolean fundingRequested;
	private Boolean fundedCBU;
	private Long fkFundCateg;
	private Long fkCordAdditionalIds;
	private List<AdditionalIdsPojo> additionalIdsPojo;
	private Long fkSpecimenId;
	//private SpecimenPojo specimenpojo;
	private Specimen specimen;
	private Long cordEntryProgress;
	private Long cordSearchable;
	private int activeId;
	//private String cordUnavailReason;
	private Site site;
	private String nmdpCbuId;
	private String nmdpMaternalId;
	//private String iscordavailfornmdp;
	private Long unavailReason;
	private Long cbuVolFrzn;
	private Long nuclcellcntfrzuncorrect;
	private Date processingDate;
	private Date procStartDate;
	private Date procTermiDate;
	private String procVersion;
	private Long fkCbbProcedure;
	private int incompleteReasonId;
	private int notCollectedToPriorReasonId;
	private Long hemoglobinResult;
	//private CBUFinalReview cbuFinalReview;
	//private String cordAvailConfirmFlag;
	//private String additiTypingFlag;
	private Long fkCBUStorageLocation;
	private Long fkCBUCollectionType;
	private Long fkCBUDeliveryType;
	private Long fkCBUCollectionSite;
	private Long fkMultipleBirth;
	private Long nmdpRaceId;
	private Long hrsaRaceRollUp;
	private String guid;
	private String productCode;
	private Date  frzDate;
	private Long hemoglobinScrn;
	private String bactComment;
	private String fungComment;
	//private Date cordUnavailConfirmDate;
	
	private Long idmcmsapprovedlab;
	private Long idmfdalicensedlab;
	private Boolean isSiteDomestic = false;
	private Date fungCultDate;
	private Date bactCultDate;
	private String cbuCollectionTime;
	private String babyBirthTime;
	private String cordIdsChangeReason;
	private String specRhTypeOther; 
	private int rhTypeOther; 
	private Long pregRefNumber;
	private String processingTime;
	private String freezeTime;
    private Long uniqueRegId;
	private Long uniqueLocaId;
	private Long uniqueIsbtId;
	private Long uniqueAddId;
	private Integer cordImportStatus; 
	private CordImportXmlMultipleValues xmlMultipleValues;
	private String matGuid;
	private RecipientInfo recipientInfo;
	private String isSystemCord;
	
	public Long getUniqueRegId() {
		return uniqueRegId;
	}
	public void setUniqueRegId(Long uniqueRegId) {
		this.uniqueRegId = uniqueRegId;
	}
	public Long getUniqueLocaId() {
		return uniqueLocaId;
	}
	public void setUniqueLocaId(Long uniqueLocaId) {
		this.uniqueLocaId = uniqueLocaId;
	}
	public Long getUniqueIsbtId() {
		return uniqueIsbtId;
	}
	public void setUniqueIsbtId(Long uniqueIsbtId) {
		this.uniqueIsbtId = uniqueIsbtId;
	}
	public Long getUniqueAddId() {
		return uniqueAddId;
	}
	public void setUniqueAddId(Long uniqueAddId) {
		this.uniqueAddId = uniqueAddId;
	}
	public Long getCbuVolFrzn() {
		return cbuVolFrzn;
	}
	public void setCbuVolFrzn(Long cbuVolFrzn) {
		this.cbuVolFrzn = cbuVolFrzn;
	}

	public Date getFrzDate() {
		return frzDate;
	}
	public void setFrzDate(Date frzDate) {
		this.frzDate = frzDate;
	}
	@CustomValidator(
			  type ="longfieldrequire",
			  fieldName = "hemoglobinScrn",				 
			  key="garuda.cbu.cordentry.hemotesting",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )})
	public Long getHemoglobinScrn() {
		return hemoglobinScrn;
	}
	public void setHemoglobinScrn(Long hemoglobinScrn) {
		this.hemoglobinScrn = hemoglobinScrn;
	}
	
	
	/*public String getIscordavailfornmdp() {
		return iscordavailfornmdp;
	}
	public void setIscordavailfornmdp(String iscordavailfornmdp) {
		this.iscordavailfornmdp = iscordavailfornmdp;
	}*/
	public String getNmdpCbuId() {
		return nmdpCbuId;
	}
	public void setNmdpCbuId(String nmdpCbuId) {
		this.nmdpCbuId = nmdpCbuId;
	}
	public String getNmdpMaternalId() {
		return nmdpMaternalId;
	}
	public void setNmdpMaternalId(String nmdpMaternalId) {
		this.nmdpMaternalId = nmdpMaternalId;
	}
	
	public int getActiveId() {
		return activeId;
	}
	public void setActiveId(int activeId) {
		this.activeId = activeId;
	}
	public String getClinicalStatReason() {
		return clinicalStatReason;
	}
	public void setClinicalStatReason(String clinicalStatReason) {
		this.clinicalStatReason = clinicalStatReason;
	}
	public Date getClinicalStatDate() {
		return clinicalStatDate;
	}
	public void setClinicalStatDate(Date clinicalStatDate) {
		this.clinicalStatDate = clinicalStatDate;
	}
	public int getTempUnavail() {
		return tempUnavail;
	}
	public void setTempUnavail(int tempUnavail) {
		this.tempUnavail = tempUnavail;
	}
	public int getNotActiveId() {
		return notActiveId;
	}
	public void setNotActiveId(int notActiveId) {
		this.notActiveId = notActiveId;
	}
	public String getNmdpNameDesc() {
		return nmdpNameDesc;
	}
	public void setNmdpNameDesc(String nmdpNameDesc) {
		this.nmdpNameDesc = nmdpNameDesc;
	}
	public String getNmdpNumDesc() {
		return nmdpNumDesc;
	}
	public void setNmdpNumDesc(String nmdpNumDesc) {
		this.nmdpNumDesc = nmdpNumDesc;
	}
	public int getPkNMDPSponsorId() {
		return pkNMDPSponsorId;
	}
	public void setPkNMDPSponsorId(int pkNMDPSponsorId) {
		this.pkNMDPSponsorId = pkNMDPSponsorId;
	}
	public int getPkOtherSponsorId() {
		return pkOtherSponsorId;
	}
	public void setPkOtherSponsorId(int pkOtherSponsorId) {
		this.pkOtherSponsorId = pkOtherSponsorId;
	}
	public Long getFkSpNMDPId() {
		return fkSpNMDPId;
	}
	public void setFkSpNMDPId(Long fkSpNMDPId) {
		this.fkSpNMDPId = fkSpNMDPId;
	}
	public Long getFkSpOtherId() {
		return fkSpOtherId;
	}
	public void setFkSpOtherId(Long fkSpOtherId) {
		this.fkSpOtherId = fkSpOtherId;
	}
	public Long getFkNmdpNameId() {
		return fkNmdpNameId;
	}
	public void setFkNmdpNameId(Long fkNmdpNameId) {
		this.fkNmdpNameId = fkNmdpNameId;
	}
	public Long getFkNmdpNumId() {
		return fkNmdpNumId;
	}
	public void setFkNmdpNumId(Long fkNmdpNumId) {
		this.fkNmdpNumId = fkNmdpNumId;
	}
	public String getSpOtherName() {
		return spOtherName;
	}
	public void setSpOtherName(String spOtherName) {
		this.spOtherName = spOtherName;
	}
	public String getSpOtherNum() {
		return spOtherNum;
	}
	public void setSpOtherNum(String spOtherNum) {
		this.spOtherNum = spOtherNum;
	}
	public int getDefferedPkCode() {
		return defferedPkCode;
	}
	public void setDefferedPkCode(int defferedPkCode) {
		this.defferedPkCode = defferedPkCode;
	}
	public String getCord_Add_info() {
		return cord_Add_info;
	}
	public void setCord_Add_info(String cord_Add_info) {
		this.cord_Add_info = cord_Add_info;
	}
	public int getUnLicesePkid() {
		return unLicesePkid;
	}
	public void setUnLicesePkid(int unLicesePkid) {
		this.unLicesePkid = unLicesePkid;
	}
	public int getInEligiblePkid() {
		return inEligiblePkid;
	}
	public void setInEligiblePkid(int inEligiblePkid) {
		this.inEligiblePkid = inEligiblePkid;
	}
	public String getInEligibleReason() {
		return inEligibleReason;
	}
	
	public int getIncompleteReasonId() {
		return incompleteReasonId;
	}
	public void setIncompleteReasonId(int incompleteReasonId) {
		this.incompleteReasonId = incompleteReasonId;
	}
	public int getNotCollectedToPriorReasonId() {
		return notCollectedToPriorReasonId;
	}
	public void setNotCollectedToPriorReasonId(int notCollectedToPriorReasonId) {
		this.notCollectedToPriorReasonId = notCollectedToPriorReasonId;
	}
	
	public void setInEligibleReason(String inEligibleReason) {
		this.inEligibleReason = inEligibleReason.trim();
	}
	public String getUnlicenseReason() {
		return unlicenseReason;
	}
	public void setUnlicenseReason(String unlicenseReason) {
		this.unlicenseReason = unlicenseReason.trim();
	}
	public Long getFkCordCbuEligibleReason() {
		return fkCordCbuEligibleReason;
	}
	public void setFkCordCbuEligibleReason(Long fkCordCbuEligibleReason) {
		this.fkCordCbuEligibleReason = fkCordCbuEligibleReason;
	}
	public Long getFkCordCbuUnlicenseReason() {
		return fkCordCbuUnlicenseReason;
	}
	public void setFkCordCbuUnlicenseReason(Long fkCordCbuUnlicenseReason) {
		this.fkCordCbuUnlicenseReason = fkCordCbuUnlicenseReason;
	}
	@Validations(customValidators={
			@CustomValidator(
					  type ="isbtfieldrequire",
					  fieldName = "cordIsbiDinCode",				 
					  key="garuda.cbu.cordentry.reqisbtdin",
					  shortCircuit=true,
					  parameters = { @ValidationParameter( name = "cdrUserField", value = "isCDRUser") }
					),
			@CustomValidator(
							  type ="isbtformat",
							  fieldName = "cordIsbiDinCode",				 
							  key="garuda.cbu.cordentry.reqvalidisbtdin",
							  shortCircuit=true,
							  parameters = { @ValidationParameter( name = "cdrUser", value = "true" ),
								             @ValidationParameter( name = "cdrUserField", value = "isCDRUser"),
								             @ValidationParameter( name = "defaultFieldValue", value = "defaultISBTCode")
				          }
			         ),	
			@CustomValidator(
							  type ="checkdataexist",
							  fieldName = "cordIsbiDinCode",				 
							  key="garuda.common.id.exist",
							  shortCircuit=true,
							  parameters = { @ValidationParameter( name = "defaultFieldValue", value = "defaultISBTCode")}
			         )		
	})
	public String getCordIsbiDinCode() {
		return cordIsbiDinCode;
	}
	public void setCordIsbiDinCode(String cordIsbiDinCode) {
		this.cordIsbiDinCode = cordIsbiDinCode;
	}
	public String getCordIsitProductCode() {
		return cordIsitProductCode;
	}
	public void setCordIsitProductCode(String cordIsitProductCode) {
		this.cordIsitProductCode = cordIsitProductCode;
	}
	public Long getFkCordCbuEligible() {
		return fkCordCbuEligible;
	}
	public void setFkCordCbuEligible(Long fkCordCbuEligible) {
		this.fkCordCbuEligible = fkCordCbuEligible;
	}
	public Long getFkCordCbuStatus() {
		return fkCordCbuStatus;
	}
	public void setFkCordCbuStatus(Long fkCordCbuStatus) {
		this.fkCordCbuStatus = fkCordCbuStatus;
	}
	public String getCordCbuStatuscode() {
		return CordCbuStatuscode;
	}
	public void setCordCbuStatuscode(String cordCbuStatuscode) {
		CordCbuStatuscode = cordCbuStatuscode;
	}
	public Long getCordnmdpstatus() {
		return cordnmdpstatus;
	}
	public void setCordnmdpstatus(Long cordnmdpstatus) {
		this.cordnmdpstatus = cordnmdpstatus;
	}
	public Date getCordavaildate() {
		return cordavaildate;
	}
	public void setCordavaildate(Date cordavaildate) {
		this.cordavaildate = cordavaildate;
	}
	public String getCordavaildatestr() {
		return cordavaildatestr;
	}
	public void setCordavaildatestr(String cordavaildatestr) {
		this.cordavaildatestr = cordavaildatestr;
	}
	
	
	//---
	public String getRhTypeDes() {
		return rhTypeDes;
	}
	public void setRhTypeDes(String rhTypeDes) {
		this.rhTypeDes = rhTypeDes;
	}
	public String getBloodGpDes() {
		return bloodGpDes;
	}
	public void setBloodGpDes(String bloodGpDes) {
		this.bloodGpDes = bloodGpDes;
	}
	
	public String getLicense() {
		return license;
	}
	public void setLicense(String license) {
		this.license = license.trim();
	}
	
	
	//---
	
	public String getCordCbuStatusDesc() {
		return cordCbuStatusDesc;
	}
	public void setCordCbuStatusDesc(String cordCbuStatusDesc) {
		this.cordCbuStatusDesc = cordCbuStatusDesc;
	}
	public String getCordnmdbStatusDesc() {
		return cordnmdbStatusDesc;
	}
	public void setCordnmdbStatusDesc(String cordnmdbStatusDesc) {
		this.cordnmdbStatusDesc = cordnmdbStatusDesc;
	}
	public String getEligibilitydes() {
		return eligibilitydes;
	}
	public void setEligibilitydes(String eligibilitydes) {
		this.eligibilitydes = eligibilitydes.trim();
	}
	public Long getFkCbbId() {
		return fkCbbId;
	}
	public void setFkCbbId(Long fkCbbId) {
		this.fkCbbId = fkCbbId;
	}
	public Long getCordID() {
		return cordID;
	}
	public void setCordID(Long cordID) {
		this.cordID = cordID;
	}
	public String getCdrCbuId() {
		return cdrCbuId;
	}
	public void setCdrCbuId(String cdrCbuId) {
		this.cdrCbuId = cdrCbuId;
	}
	public String getExternalCbuId() {
		return externalCbuId;
	}
	public void setExternalCbuId(String externalCbuId) {
		this.externalCbuId = externalCbuId;
	}
	public Long getTncFrozen() {
		return tncFrozen;
	}
	public void setTncFrozen(Long tncFrozen) {
		this.tncFrozen = tncFrozen;
	}
	public Long getCellCount() {
		return cellCount;
	}
	public void setCellCount(Long cellCount) {
		this.cellCount = cellCount;
	}
	public Long getFrozenPatWt() {
		return frozenPatWt;
	}
	public void setFrozenPatWt(Long frozenPatWt) {
		this.frozenPatWt = frozenPatWt;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	
	@RequiredStringValidator(fieldName = "registryId", key="garuda.cbu.cordentry.reqcburegid",type = ValidatorType.FIELD,shortCircuit=false)
	@Validations(customValidators={
			@CustomValidator(
					  type ="checkspecialchar",
					  fieldName = "registryId",				 
					  key="garuda.common.validation.alphanumeric",
					  shortCircuit=true,
					  parameters = { @ValidationParameter( name = "cdrUser", value = "true" ),
						             @ValidationParameter( name = "cdrUserField", value = "isCDRUser")						         
		                           }
					),
			@CustomValidator(
							  type ="checklength",
							  fieldName = "registryId",				 
							  key="garuda.common.validation.alphalength",
							  shortCircuit=true,
							  parameters = { @ValidationParameter( name = "cdrUser", value = "true" ),
							                 @ValidationParameter( name = "cdrUserField", value = "isCDRUser")						         
			                           }
			         ),			
			@CustomValidator(
							  type ="checkformat",
							  fieldName = "registryId",				 
							  key="garuda.cbu.cordentry.checkformat",
							  shortCircuit=true,
							  parameters = { @ValidationParameter( name = "cdrUser", value = "true" ),
									         @ValidationParameter( name = "cdrUserField", value = "isCDRUser"),
									         @ValidationParameter( name = "defaultFieldValue", value = "defaultRegId")
					          }
			         ), 
			@CustomValidator(
							  type ="checkstrequality",
							  fieldName = "registryId",				 
							  key="garuda.cbu.cordentry.cbumatsameregid",
							  shortCircuit=true,
							  parameters = { @ValidationParameter( name = "fieldName2", value = "registryMaternalId" )								       
								          }
			         ),
			 @CustomValidator(
							  type ="checksum",
							  fieldName = "registryId",				 
							  key="garuda.common.validation.checksumrid",
							  shortCircuit=true,
							  parameters = { @ValidationParameter( name = "cdrUser", value = "true" ),
								             @ValidationParameter( name = "cdrUserField", value = "isCDRUser"),
								             @ValidationParameter( name = "defaultFieldValue", value = "defaultRegId")
				          }
			         ),
			@CustomValidator(
							  type ="checkdataexist",
							  fieldName = "registryId",				 
							  key="garuda.common.id.exist",
							  shortCircuit=true,
							  parameters = { @ValidationParameter( name = "cdrUser", value = "true" ),
								             @ValidationParameter( name = "cdrUserField", value = "isCDRUser"),
								             @ValidationParameter( name = "defaultFieldValue", value = "defaultRegId")
				          }
			         ) 
})
	public String getRegistryId() {
		return registryId;
	}
	public void setRegistryId(String registryId) {
		this.registryId = registryId;
	}
	
	@Validations(customValidators={
			 @CustomValidator(
					  type ="requirefield",
					  fieldName = "localCbuId",				 
					  key="garuda.cbu.cordentry.reqcbulocid",
					  shortCircuit=true,
					  parameters = { @ValidationParameter( name = "cdrUser", value = "true" ),
						             @ValidationParameter( name = "cdrUserField", value = "isCDRUser")						         
		                           }
					),
			 @CustomValidator(
							  type ="checkstrequality",
							  fieldName = "localCbuId",				 
							  key="garuda.cbu.cordentry.cbulocsamematlocid",
							  shortCircuit=true,
							  parameters = { @ValidationParameter( name = "fieldName2", value = "localMaternalId" )								       
								          }
			         ),	
			         @CustomValidator(
							  type ="checkdataexist",
							  fieldName = "localCbuId",				 
							  key="garuda.common.id.exist",
							  shortCircuit=true,
							  parameters = { @ValidationParameter( name = "cdrUser", value = "true" ),
								             @ValidationParameter( name = "cdrUserField", value = "isCDRUser"),
								             @ValidationParameter( name = "defaultFieldValue", value = "defaultLocCbuId")
				          }
			         ) 
	})
	public String getLocalCbuId() {
		return localCbuId;
	}
	
	public void setLocalCbuId(String localCbuId) {
		this.localCbuId = localCbuId;
	}
	public String getHistoricLocalCbuId() {
		return historicLocalCbuId;
	}
	public void setHistoricLocalCbuId(String historicLocalCbuId) {
		this.historicLocalCbuId = historicLocalCbuId;
	}
	public String getNumberOnCbuBag() {
		return numberOnCbuBag;
	}
	public void setNumberOnCbuBag(String numberOnCbuBag) {
		this.numberOnCbuBag = numberOnCbuBag;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getCdrCbuCreatedBy() {
		return cdrCbuCreatedBy;
	}
	public void setCdrCbuCreatedBy(String cdrCbuCreatedBy) {
		this.cdrCbuCreatedBy = cdrCbuCreatedBy;
	}
	public Date getCdrCbuLstModDate() {
		return cdrCbuLstModDate;
	}
	public void setCdrCbuLstModDate(Date cdrCbuLstModDate) {
		this.cdrCbuLstModDate = cdrCbuLstModDate;
	}
	public String getCdrCbuLstModBy() {
		return cdrCbuLstModBy;
	}
	public void setCdrCbuLstModBy(String cdrCbuLstModBy) {
		this.cdrCbuLstModBy = cdrCbuLstModBy;
	}
	@CustomValidator(
			  type ="longfieldrequire",
			  fieldName = "bacterialResult",				 
			  key="garuda.cbu.cordentry.bactcul",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )})
	public Long getBacterialResult() {
		return bacterialResult;
	}
	public void setBacterialResult(Long bacterialResult) {
		this.bacterialResult = bacterialResult;
	}
	@CustomValidator(
			  type ="longfieldrequire",
			  fieldName = "fungalResult",				 
			  key="garuda.cbu.cordentry.fungcul",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )})
	public Long getFungalResult() {
		return fungalResult;
	}
	public void setFungalResult(Long fungalResult) {
		this.fungalResult = fungalResult;
	}
	@CustomValidator(
			  type ="longfieldrequire",
			  fieldName = "aboBloodType",				 
			  key="garuda.cbu.cordentry.abobloodtype",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )})
	public Long getAboBloodType() {
		return aboBloodType;
	}
	public void setAboBloodType(Long aboBloodType) {
		this.aboBloodType = aboBloodType;
	}
	@CustomValidator(
			  type ="longfieldrequire",
			  fieldName = "rhType",				 
			  key="garuda.cbu.cordentry.rhtype",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}) 
	public Long getRhType() {
		return rhType;
	}
	public void setRhType(Long rhType) {
		this.rhType = rhType;
	}	
	@CustomValidator(
			  type ="longfieldrequire",
			  fieldName = "babyGenderId",				 
			  key="garuda.cbu.cordentry.babygenderid",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )})
	public Long getBabyGenderId() {
		return babyGenderId;
	}
	public void setBabyGenderId(Long babyGenderId) {
		this.babyGenderId = babyGenderId;
	}
	public Long getClinicalStatus() {
		return clinicalStatus;
	}
	public void setClinicalStatus(Long clinicalStatus) {
		this.clinicalStatus = clinicalStatus;
	}
	public Long getCbuLicStatus() {
		return cbuLicStatus;
	}
	public void setCbuLicStatus(Long cbuLicStatus) {
		this.cbuLicStatus = cbuLicStatus;
	}
	
	public String getAddInfoReq() {
		return addInfoReq;
	}
	public void setAddInfoReq(String addInfoReq) {
		this.addInfoReq = addInfoReq;
	}
	private Boolean deletedFlag;
	
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	 @Validations(customValidators={
			 @CustomValidator(
					  type ="requirefield",
					  fieldName = "registryMaternalId",				 
					  key="garuda.cbu.cordentry.reqmatregid",
					  shortCircuit=true,
					  parameters = { @ValidationParameter( name = "cdrUser", value = "true" ),
						             @ValidationParameter( name = "cdrUserField", value = "isCDRUser")						         
		                           }
					),
			@CustomValidator(
					  type ="checkspecialchar",
					  fieldName = "registryMaternalId",				 
					  key="garuda.common.validation.alphanumeric",
					  shortCircuit=true,
					  parameters = { @ValidationParameter( name = "cdrUser", value = "true" ),
						             @ValidationParameter( name = "cdrUserField", value = "isCDRUser")						         
		                           }
					),
			@CustomValidator(
							  type ="checklength",
							  fieldName = "registryMaternalId",				 
							  key="garuda.common.validation.alphalength",
							  shortCircuit=true,
							  parameters = { @ValidationParameter( name = "cdrUser", value = "true" ),
							                 @ValidationParameter( name = "cdrUserField", value = "isCDRUser")						         
			                           }
			         ),			
			@CustomValidator(
							  type ="checkformat",
							  fieldName = "registryMaternalId",				 
							  key="garuda.cbu.cordentry.checkformat",
							  shortCircuit=true,
							  parameters = { @ValidationParameter( name = "cdrUser", value = "true" ),
									         @ValidationParameter( name = "cdrUserField", value = "isCDRUser"),
									         @ValidationParameter( name = "defaultFieldValue", value = "defaultMatId")
					          }
			         ), 
			@CustomValidator(
							  type ="checkstrequality",
							  fieldName = "registryMaternalId",				 
							  key="garuda.cbu.cordentry.cbumatsameregid",
							  shortCircuit=true,
							  parameters = { @ValidationParameter( name = "fieldName2", value = "registryId" )								       
								          }
			         ),
			 @CustomValidator(
							  type ="checksum",
							  fieldName = "registryMaternalId",				 
							  key="garuda.common.validation.checksummid",
							  shortCircuit=true,
							  parameters = { @ValidationParameter( name = "cdrUser", value = "true" ),
								             @ValidationParameter( name = "cdrUserField", value = "isCDRUser"),
								             @ValidationParameter( name = "defaultFieldValue", value = "defaultMatId")
				          }
			         ),
			@CustomValidator(
							  type ="checkdataexist",
							  fieldName = "registryMaternalId",				 
							  key="garuda.common.id.exist",
							  shortCircuit=true,
							  parameters = { @ValidationParameter( name = "cdrUser", value = "true" ),
								             @ValidationParameter( name = "cdrUserField", value = "isCDRUser"),
								             @ValidationParameter( name = "defaultFieldValue", value = "defaultMatId")
				          }
			         ) 
})
	public String getRegistryMaternalId() {
		return registryMaternalId;
	}
	public void setRegistryMaternalId(String registryMaternalId) {
		this.registryMaternalId = registryMaternalId;
	}
	@Validations(customValidators={
			@CustomValidator(
					  type ="checkstrequality",
					  fieldName = "localMaternalId",				 
					  key="garuda.cbu.cordentry.cbumatlocsamecbulocid",
					  shortCircuit=true,
					  parameters = { @ValidationParameter( name = "fieldName2", value = "localCbuId" )								       
						          }
	         ),	
			@CustomValidator(
							  type ="checkdataexist",
							  fieldName = "localMaternalId",				 
							  key="garuda.common.id.exist",
							  shortCircuit=true,
							  parameters = { //@ValidationParameter( name = "cdrUser", value = "true" ),
								            // @ValidationParameter( name = "cdrUserField", value = "isCDRUser"),
								             @ValidationParameter( name = "defaultFieldValue", value = "defaultMatLocId")
				          }
			         ) 
})
	public String getLocalMaternalId() {
		return localMaternalId;
	}
	public void setLocalMaternalId(String localMaternalId) {
		this.localMaternalId = localMaternalId;
	}	
	@CustomValidator(
			  type ="longfieldrequire",
			  fieldName = "ethnicity",				 
			  key="garuda.cbu.cordentry.birthethnicity",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )})
	public String getEthnicity() {
		return ethnicity;
	}
	public void setEthnicity(String ethnicity) {
		this.ethnicity = ethnicity;
	}
	public String getRace() {
		return race;
	}
	public void setRace(String race) {
		this.race = race;
	}
	public Boolean getFundingRequested() {
		return fundingRequested;
	}
	public void setFundingRequested(Boolean fundingRequested) {
		this.fundingRequested = fundingRequested;
	}
	public Boolean getFundedCBU() {
		return fundedCBU;
	}
	public void setFundedCBU(Boolean fundedCBU) {
		this.fundedCBU = fundedCBU;
	}	
	public Long getFkFundCateg() {
		return fkFundCateg;
	}
	public void setFkFundCateg(Long fkFundCateg) {
		this.fkFundCateg = fkFundCateg;
	}
	public Long getFkCordAdditionalIds() {
		return fkCordAdditionalIds;
	}
	public void setFkCordAdditionalIds(Long fkCordAdditionalIds) {
		this.fkCordAdditionalIds = fkCordAdditionalIds;
	}
	public List<AdditionalIdsPojo> getAdditionalIdsPojo() {
		return additionalIdsPojo;
	}
	public void setAdditionalIdsPojo(List<AdditionalIdsPojo> additionalIdsPojo) {
		this.additionalIdsPojo = additionalIdsPojo;
	}
	public Long getFkSpecimenId() {
		return fkSpecimenId;
	}
	public void setFkSpecimenId(Long fkSpecimenId) {
		this.fkSpecimenId = fkSpecimenId;
	}
	
	public Specimen getSpecimen() {
		return specimen;
	}
	public void setSpecimen(Specimen specimen) {
		this.specimen = specimen;
	}
	/*public SpecimenPojo getSpecimenpojo() {
		return specimenpojo;
	}
	public void setSpecimenpojo(SpecimenPojo specimenpojo) {
		this.specimenpojo = specimenpojo;
	}
	
	*/
	public Long getCordEntryProgress() {
		return cordEntryProgress;
	}
	public void setCordEntryProgress(Long cordEntryProgress) {
		this.cordEntryProgress = cordEntryProgress;
	}
	public Long getCordSearchable() {
		return cordSearchable;
	}
	public void setCordSearchable(Long cordSearchable) {
		this.cordSearchable = cordSearchable;
	}
	public String getCordDonorIdNumber() {
		return cordDonorIdNumber;
	}
	public void setCordDonorIdNumber(String cordDonorIdNumber) {
		this.cordDonorIdNumber = cordDonorIdNumber;
	}
	public Site getSite() {
		return site;
	}
	public void setSite(Site site) {
		this.site = site;
	}
	public Long getUnavailReason() {
		return unavailReason;
	}
	public void setUnavailReason(Long unavailReason) {
		this.unavailReason = unavailReason;
	}
	/*
	 * public CBUFinalReview getCbuFinalReview() {
		return cbuFinalReview;
	}
	public void setCbuFinalReview(CBUFinalReview cbuFinalReview) {
		if(cbuFinalReview == null){
			cbuFinalReview = new CBUFinalReview();
		}
		this.cbuFinalReview = cbuFinalReview;
	}
	*/
	/*public String getCordAvailConfirmFlag() {
		return cordAvailConfirmFlag;
	}
	public void setCordAvailConfirmFlag(String cordAvailConfirmFlag) {
		this.cordAvailConfirmFlag = cordAvailConfirmFlag;
	}*/
	/*public String getAdditiTypingFlag() {
		return additiTypingFlag;
	}
	public void setAdditiTypingFlag(String additiTypingFlag) {
		this.additiTypingFlag = additiTypingFlag;
	}*/
	public Long getNuclcellcntfrzuncorrect() {
		return nuclcellcntfrzuncorrect;
	}
	public void setNuclcellcntfrzuncorrect(Long nuclcellcntfrzuncorrect) {
		this.nuclcellcntfrzuncorrect = nuclcellcntfrzuncorrect;
	}
	public Date getProcessingDate() {
		return processingDate;
	}
	public void setProcessingDate(Date processingDate) {
		this.processingDate = processingDate;
	}

	public Long getHemoglobinResult() {
		return hemoglobinResult;
	}
	public void setHemoglobinResult(Long hemoglobinResult) {
		this.hemoglobinResult = hemoglobinResult;
	}
	
	
	public String getBactComment() {
		return bactComment;
	}
	public void setBactComment(String bactComment) {
		this.bactComment = bactComment;
	}
		
	public String getFungComment() {
		return fungComment;
	}
	public void setFungComment(String fungComment) {
		this.fungComment = fungComment;
	}
	
	public Date getProcStartDate() {
		return procStartDate;
	}
	public void setProcStartDate(Date procStartDate) {
		this.procStartDate = procStartDate;
	}
	public Date getProcTermiDate() {
		return procTermiDate;
	}
	public void setProcTermiDate(Date procTermiDate) {
		this.procTermiDate = procTermiDate;
	}
	public String getProcVersion() {
		return procVersion;
	}
	public void setProcVersion(String procVersion) {
		this.procVersion = procVersion;
	}
	 @CustomValidator(
			  type ="longfieldrequire",
			  fieldName = "fkCbbProcedure",				 
			  key="garuda.cbu.cordentry.cbbprocedure",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )})
	public Long getFkCbbProcedure() {
		return fkCbbProcedure;
	}
	public void setFkCbbProcedure(Long fkCbbProcedure) {
		this.fkCbbProcedure = fkCbbProcedure;
	}
	public Long getFkCBUStorageLocation() {
		return fkCBUStorageLocation;
	}
	public void setFkCBUStorageLocation(Long fkCBUStorageLocation) {
		this.fkCBUStorageLocation = fkCBUStorageLocation;
	}
	@CustomValidator(
			  type ="longfieldrequire",
			  fieldName = "fkCBUCollectionType",				 
			  key="garuda.cbu.cordentry.collectiontype",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )})
	public Long getFkCBUCollectionType() {
		return fkCBUCollectionType;
	}
	public void setFkCBUCollectionType(Long fkCBUCollectionType) {
		this.fkCBUCollectionType = fkCBUCollectionType;
	}
	@CustomValidator(
			  type ="longfieldrequire",
			  fieldName = "fkCBUDeliveryType",				 
			  key="garuda.cbu.cordentry.deliverytype",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )})
	public Long getFkCBUDeliveryType() {
		return fkCBUDeliveryType;
	}
	public void setFkCBUDeliveryType(Long fkCBUDeliveryType) {
		this.fkCBUDeliveryType = fkCBUDeliveryType;
	}
	public Long getFkCBUCollectionSite() {
		return fkCBUCollectionSite;
	}
	public void setFkCBUCollectionSite(Long fkCBUCollectionSite) {
		this.fkCBUCollectionSite = fkCBUCollectionSite;
	}	
	@CustomValidator(
			  type ="longfieldrequire",
			  fieldName = "fkMultipleBirth",				 
			  key="garuda.cbu.cordentry.MultipleBirth",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )})
	public Long getFkMultipleBirth() {
		return fkMultipleBirth;
	}
	public void setFkMultipleBirth(Long fkMultipleBirth) {
		this.fkMultipleBirth = fkMultipleBirth;
	}
	public Long getNmdpRaceId() {
		return nmdpRaceId;
	}
	public void setNmdpRaceId(Long nmdpRaceId) {
		this.nmdpRaceId = nmdpRaceId;
	}
	public Long getHrsaRaceRollUp() {
		return hrsaRaceRollUp;
	}
	public void setHrsaRaceRollUp(Long hrsaRaceRollUp) {
		this.hrsaRaceRollUp = hrsaRaceRollUp;
	}
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public Long getIdmcmsapprovedlab() {
		return idmcmsapprovedlab;
	}
	public void setIdmcmsapprovedlab(Long idmcmsapprovedlab) {
		this.idmcmsapprovedlab = idmcmsapprovedlab;
	}
	public Long getIdmfdalicensedlab() {
		return idmfdalicensedlab;
	}
	public void setIdmfdalicensedlab(Long idmfdalicensedlab) {
		this.idmfdalicensedlab = idmfdalicensedlab;
	}
	/*public Date getCordUnavailConfirmDate() {
		return cordUnavailConfirmDate;
	}
	public void setCordUnavailConfirmDate(Date cordUnavailConfirmDate) {
		this.cordUnavailConfirmDate = cordUnavailConfirmDate;
	}*/
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public Boolean getIsSiteDomestic() {
		return isSiteDomestic;
	}
	public void setIsSiteDomestic(Boolean isSiteDomestic) {
		this.isSiteDomestic = isSiteDomestic;
	}
	public Date getFungCultDate() {
		return fungCultDate;
	}
	public void setFungCultDate(Date fungCultDate) {
		this.fungCultDate = fungCultDate;
	}
	public Date getBactCultDate() {
		return bactCultDate;
	}
	public void setBactCultDate(Date bactCultDate) {
		this.bactCultDate = bactCultDate;
	}
	@Validations(customValidators={
	@CustomValidator(
			  type ="requirefield",
			  fieldName = "cbuCollectionDateStr",				 
			  key="garuda.cbu.cordentry.collectiontime",
			  shortCircuit=true	
			  ),
	@CustomValidator(
					  type ="maxtimerangevalidator",
					  fieldName = "cbuCollectionTime",				 
					  key="garuda.cbu.cordentry.collectiondateTimehrs",
					  shortCircuit=true,
					  parameters = { @ValidationParameter( name = "timeRange", value = "48" ),
							         @ValidationParameter( name = "invokingRespDateFieldName", value = "cbuCollectionDateStr" ),
							         @ValidationParameter( name = "compareTimeFieldName", value = "cdrCbuPojo.babyBirthTime" ),
							         @ValidationParameter( name = "compareRespDateFieldName", value = "birthDateStr" )
							        
							         
					}),
	@CustomValidator(
					  type ="mintimerangevalidator",
					  fieldName = "cbuCollectionTime",				 
					  key="garuda.cbu.cordentry.collectiondateTimehrs",
					  shortCircuit=true,
					  parameters = { //@ValidationParameter( name = "timeRange", value = "48" ),
							  @ValidationParameter( name = "invokingRespDateFieldName", value = "cbuCollectionDateStr" ),
							  @ValidationParameter( name = "compareTimeFieldName", value = "cdrCbuPojo.babyBirthTime" ),
							         @ValidationParameter( name = "compareRespDateFieldName", value = "birthDateStr" )
							        
							         
					})
	})				  
    @RegexFieldValidator( key = "garuda.common.validation.timeformat", expression = "([0-2][0-3]:[0-5][0-9])|([0-2][0-3]:[6][0])|([0-1][0-9]:[0-5][0-9])", shortCircuit=true)
   
	public String getCbuCollectionTime() {
		return cbuCollectionTime;
	}
	public void setCbuCollectionTime(String cbuCollectionTime) {
		this.cbuCollectionTime = cbuCollectionTime;
	}	
	@CustomValidator(
			  type ="requirefield",
			  fieldName = "babyBirthTime",				 
			  key="garuda.cbu.cordentry.babybirthtime",
			  shortCircuit=true	
			  )			 	  
    @RegexFieldValidator( key = "garuda.common.validation.timeformat", expression = "([0-2][0-3]:[0-5][0-9])|([0-2][0-3]:[6][0])|([0-1][0-9]:[0-5][0-9])", shortCircuit=true)
	public String getBabyBirthTime() {
		return babyBirthTime;
	}
	public void setBabyBirthTime(String babyBirthTime) {
		this.babyBirthTime = babyBirthTime;
	}
	public String getCordIdsChangeReason() {
		return cordIdsChangeReason;
	}
	public void setCordIdsChangeReason(String cordIdsChangeReason) {
		this.cordIdsChangeReason = cordIdsChangeReason;
	}
	@CustomValidator(
			  type ="cordentrynoncustomvalidator",
			  fieldName = "specRhTypeOther",				 
			  key="garuda.common.validation.reason",
			  shortCircuit=true,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
					  		 @ValidationParameter( name = "fields", value = "12641,cdrCbuPojo.rhType")
			})
	public String getSpecRhTypeOther() {
		return specRhTypeOther;
	}
	public void setSpecRhTypeOther(String specRhTypeOther) {
		this.specRhTypeOther = specRhTypeOther;
	}
	public int getRhTypeOther() {
		return rhTypeOther;
	}
	public void setRhTypeOther(int rhTypeOther) {
		this.rhTypeOther = rhTypeOther;
	}
	public Long getPregRefNumber() {
		return pregRefNumber;
	}
	public void setPregRefNumber(Long pregRefNumber) {
		this.pregRefNumber = pregRefNumber;
	}
	@Validations(customValidators={
			@CustomValidator(
					  type ="requirefield",
					  fieldName = "processingTime",				 
					  key="garuda.cbu.cordentry.proctime",
					  shortCircuit=true	
					  ),
			@CustomValidator(
							  type ="maxtimerangevalidator",
							  fieldName = "processingTime",				 
							  key="garuda.cbu.cordentry.procbeforefreeztime",
							  shortCircuit=true,
							  parameters = { @ValidationParameter( name = "invokingRespDateFieldName", value = "prcsngStartDateStr" ),
									         @ValidationParameter( name = "compareTimeFieldName", value = "cdrCbuPojo.freezeTime" ),
									         @ValidationParameter( name = "compareRespDateFieldName", value = "frzDateStr" )									         
							})
	})
	@RegexFieldValidator( key = "garuda.common.validation.timeformat", expression = "([0-2][0-3]:[0-5][0-9])|([0-2][0-3]:[6][0])|([0-1][0-9]:[0-5][0-9])", shortCircuit=true)
	public String getProcessingTime() {
		return processingTime;
	}
	public void setProcessingTime(String processingTime) {
		this.processingTime = processingTime;
	}
	@Validations(customValidators={
			@CustomValidator(
					  type ="requirefield",
					  fieldName = "freezeTime",				 
					  key="Please enter the freeze time",
					  shortCircuit=true	
					  ),
			@CustomValidator(
							  type ="maxtimerangevalidator",
							  fieldName = "freezeTime",				 
							  key="garuda.cbu.cordentry.frzTimehrs",
							  shortCircuit=true,
							  parameters = { @ValidationParameter( name = "timeRange", value = "48" ),
									         @ValidationParameter( name = "invokingRespDateFieldName", value = "frzDateStr" ),
									         @ValidationParameter( name = "compareTimeFieldName", value = "cdrCbuPojo.cbuCollectionTime" ),
									         @ValidationParameter( name = "compareRespDateFieldName", value = "cbuCollectionDateStr" )									         
									  }),
			@CustomValidator(
							  type ="mintimerangevalidator",
							  fieldName = "freezeTime",				 
							  key="garuda.cbu.cordentry.frztimeafterproctime",
							  shortCircuit=true,
							  parameters = { /*@ValidationParameter( name = "timeRange", value = "48" ),*/
									         @ValidationParameter( name = "invokingRespDateFieldName", value = "frzDateStr" ),
									         @ValidationParameter( name = "compareTimeFieldName", value = "cdrCbuPojo.processingTime" ),
									         @ValidationParameter( name = "compareRespDateFieldName", value = "prcsngStartDateStr" )									         
									  })

			})	
	@RegexFieldValidator( key = "garuda.common.validation.timeformat", expression = "([0-2][0-3]:[0-5][0-9])|([0-2][0-3]:[6][0])|([0-1][0-9]:[0-5][0-9])", shortCircuit=true)
	public String getFreezeTime() {
		return freezeTime;
	}
	public void setFreezeTime(String freezeTime) {
		this.freezeTime = freezeTime;
	}
	public CordImportXmlMultipleValues getXmlMultipleValues() {
		return xmlMultipleValues;
	}
	public void setXmlMultipleValues(CordImportXmlMultipleValues xmlMultipleValues) {
		this.xmlMultipleValues = xmlMultipleValues;
	}
	public Integer getCordImportStatus() {
		return cordImportStatus;
	}
	public void setCordImportStatus(Integer cordImportStatus) {
		this.cordImportStatus = cordImportStatus;
	}
	public String getMatGuid() {
		return matGuid;
	}
	public void setMatGuid(String matGuid) {
		this.matGuid = matGuid;
	}
	public RecipientInfo getRecipientInfo() {
		return recipientInfo;
	}
	public void setRecipientInfo(RecipientInfo recipientInfo) {
		this.recipientInfo = recipientInfo;
	}
	public String getIsSystemCord() {
		return isSystemCord;
	}
	public void setIsSystemCord(String isSystemCord) {
		this.isSystemCord = isSystemCord;
	}
	
}
