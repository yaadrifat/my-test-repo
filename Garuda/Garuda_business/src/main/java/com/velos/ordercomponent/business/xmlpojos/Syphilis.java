package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


public class Syphilis {

	private String syphilis_react_ind;
	private XMLGregorianCalendar syphilis_react_date;
	private String cms_cert_lab_ind;
	private String fda_lic_mfg_inst_ind;
	@XmlElement(name="syphilis_react_ind")	
	public String getSyphilis_react_ind() {
		return syphilis_react_ind;
	}
	public void setSyphilis_react_ind(String syphilisReactInd) {
		syphilis_react_ind = syphilisReactInd;
	}
	@XmlElement(name="syphilis_react_date")	
	@XmlJavaTypeAdapter(type=XMLGregorianCalendar.class,value=XMLGeorgarianCalanderAdapter.class)
	public XMLGregorianCalendar getSyphilis_react_date() {
		return syphilis_react_date;
	}
	public void setSyphilis_react_date(XMLGregorianCalendar syphilisReactDate) {
		syphilis_react_date = syphilisReactDate;
	}
	@XmlElement(name="cms_cert_lab_ind")	
	public String getCms_cert_lab_ind() {
		return cms_cert_lab_ind;
	}
	public void setCms_cert_lab_ind(String cmsCertLabInd) {
		cms_cert_lab_ind = cmsCertLabInd;
	}
	@XmlElement(name="fda_lic_mfg_inst_ind")	
	public String getFda_lic_mfg_inst_ind() {
		return fda_lic_mfg_inst_ind;
	}
	public void setFda_lic_mfg_inst_ind(String fdaLicMfgInstInd) {
		fda_lic_mfg_inst_ind = fdaLicMfgInstInd;
	}
	
	
}
