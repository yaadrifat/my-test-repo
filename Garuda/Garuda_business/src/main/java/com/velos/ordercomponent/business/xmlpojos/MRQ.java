package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


public class MRQ {

	private String cbb_val_protocol_used;
	private XMLGregorianCalendar at_risk_frm_cmplt_date;
	private String donate_cb_prev_ind;
	private String refused_bld_dnr_ind;
	private String refused_bld_desc;
	private String tkn_medications_ind;
	private String bovine_insulin_since_1980_ind;
	private String evr_tkn_pit_gh_ind;
	private String rabies_vacc_pst_year_ind;
	private String shots_vacc_pst_8wk_ind;
	private String shots_vacc_pst_8wk_desc;
	private String cntc_smallpox_vaccine_12wk_ind;
	private String pst_4mth_illness_symptom_ind;
	private String symptom_desc;
	private String cncr_ind;
	private String bld_prblm_5yr_ind;
	private String wnv_preg_diag_pos_test_ind;
	private String viral_hepatitis_age_11_ind;
	private String para_chagas_babesiosis_leish_ind;
	private String cjd_diag_neuro_unk_ind;
	private String bld_rlt_diag_rsk_cjd;
	private String dura_mater_ind;
	private String recv_xeno_tx_med_procedure_ind;
	private String liv_cntc_xeno_tx_med_proc_ind;
	private String malaria_antimalr_drug_3yr_ind;
	private String outside_us_or_canada_3yr_ind;
	private String outside_canada_us_desc;
	private String blood_transf_b4coll_12m_ind;
	private String tx_stemcell_12m_oth_self_ind;
	private String tattoo_ear_skin_pierce_12m_ind;
	private String tattoo_shared_nonsterile_12m_ind;
	private String acc_ns_stk_cut_bld_12m_ind;
	private String had_treat_syph_12m_ind;
	private String given_mn_dr_for_sex_ind;	
	private String sex_w_tk_mn_dr_pmt_sex_12m_5yr_ind;
	private String sex_cntc_liv_hep_b_c_ind;
	private String sex_w_iv_dr_in_pst_5yr_12m_ind;
	private String sex_w_man_oth_pst_5yr_12m_ind;	
	private String sex_w_clot_fact_bld_12m_ind;
	private String sex_w_hiv_aids_12m_ind;
	private String prison_jail_contin_72hrs_ind;	
	private String pst_5yr_tk_mn_dr_sex_ind;
	private String pst_5yr_iv_drug_ind;	
	private String aids_hiv_screen_test_ind;
	private String unexplained_symptom_ind;	
	private String unexpln_night_sweat;
	private String unexpln_blue_prpl_spot_kaposi_ind;
	private String unexpln_weight_loss;
	private String unexpln_persist_diarrhea;
	private String unexpln_cough_short_breath;
	private String unexpln_temp_over_ten_days;
	private String unexpln_mouth_sores_wht_spt;
	private String unexpln_1m_lump_nk_apit_grn_mult_ind;
	private String inf_dur_preg_ind;
	private String htlv_incl_screen_test_para_ind;
	private String hiv_contag_person_understand_ind;
	private String cjd_live_travel_country_ind;
	private String spent_uk_ge_3m_ind;
	private String recv_trnsfsn_uk_france_ind;
	private String spent_europe_ge_5y_cjd_cnt_ind;
	private String us_military_dep_ind;
	private String mil_base_europe_1_ge_6m_ind;
	private String mil_base_europe_2_ge_6m_ind;
	private String hiv_1_2_o_performed_ind;
	private String born_live_cam_afr_nig_ind;
	private String trav_ctry_rec_bld_or_med_ind;
	private String sex_w_born_live_in_ctry_ind;
	
	@XmlElement(name="cbb_val_protocol_used")
	public String getCbb_val_protocol_used() {
		return cbb_val_protocol_used;
	}
	public void setCbb_val_protocol_used(String cbb_val_protocol_used) {
		this.cbb_val_protocol_used = cbb_val_protocol_used;
	}
	@XmlElement(name="at_risk_frm_cmplt_date")
	@XmlJavaTypeAdapter(type=XMLGregorianCalendar.class,value=XMLGeorgarianCalanderAdapter.class)
	public XMLGregorianCalendar getAt_risk_frm_cmplt_date() {
		return at_risk_frm_cmplt_date;
	}
	public void setAt_risk_frm_cmplt_date(XMLGregorianCalendar atRiskFrmCmpltDate) {
		at_risk_frm_cmplt_date = atRiskFrmCmpltDate;
	}
	@XmlElement(name="donate_cb_prev_ind")
	public String getDonate_cb_prev_ind() {
		return donate_cb_prev_ind;
	}
	public void setDonate_cb_prev_ind(String donateCbPrevInd) {
		donate_cb_prev_ind = donateCbPrevInd;
	}
	@XmlElement(name="refused_bld_dnr_ind")
	public String getRefused_bld_dnr_ind() {
		return refused_bld_dnr_ind;
	}
	public void setRefused_bld_dnr_ind(String refusedBldDnrInd) {
		refused_bld_dnr_ind = refusedBldDnrInd;
	}
	@XmlElement(name="refused_bld_desc")
	public String getRefused_bld_desc() {
		return refused_bld_desc;
	}
	public void setRefused_bld_desc(String refusedBldDesc) {
		refused_bld_desc = refusedBldDesc;
	}
	@XmlElement(name="tkn_medications_ind")
	public String getTkn_medications_ind() {
		return tkn_medications_ind;
	}
	public void setTkn_medications_ind(String tknMedicationsInd) {
		tkn_medications_ind = tknMedicationsInd;
	}
	@XmlElement(name="bovine_insulin_since_1980_ind")
	public String getBovine_insulin_since_1980_ind() {
		return bovine_insulin_since_1980_ind;
	}
	public void setBovine_insulin_since_1980_ind(String bovineInsulinSince_1980Ind) {
		bovine_insulin_since_1980_ind = bovineInsulinSince_1980Ind;
	}
	@XmlElement(name="evr_tkn_pit_gh_ind")
	public String getEvr_tkn_pit_gh_ind() {
		return evr_tkn_pit_gh_ind;
	}
	public void setEvr_tkn_pit_gh_ind(String evrTknPitGhInd) {
		evr_tkn_pit_gh_ind = evrTknPitGhInd;
	}
	@XmlElement(name="rabies_vacc_pst_year_ind")
	public String getRabies_vacc_pst_year_ind() {
		return rabies_vacc_pst_year_ind;
	}
	public void setRabies_vacc_pst_year_ind(String rabiesVaccPstYearInd) {
		rabies_vacc_pst_year_ind = rabiesVaccPstYearInd;
	}
	@XmlElement(name="shots_vacc_pst_8wk_ind")
	public String getShots_vacc_pst_8wk_ind() {
		return shots_vacc_pst_8wk_ind;
	}
	public void setShots_vacc_pst_8wk_ind(String shotsVaccPst_8wkInd) {
		shots_vacc_pst_8wk_ind = shotsVaccPst_8wkInd;
	}
	@XmlElement(name="shots_vacc_pst_8wk_desc")
	public String getShots_vacc_pst_8wk_desc() {
		return shots_vacc_pst_8wk_desc;
	}
	public void setShots_vacc_pst_8wk_desc(String shotsVaccPst_8wkDesc) {
		shots_vacc_pst_8wk_desc = shotsVaccPst_8wkDesc;
	}
	@XmlElement(name="cntc_smallpox_vaccine_12wk_ind")
	public String getCntc_smallpox_vaccine_12wk_ind() {
		return cntc_smallpox_vaccine_12wk_ind;
	}
	public void setCntc_smallpox_vaccine_12wk_ind(String cntcSmallpoxVaccine_12wkInd) {
		cntc_smallpox_vaccine_12wk_ind = cntcSmallpoxVaccine_12wkInd;
	}
	@XmlElement(name="pst_4mth_illness_symptom_ind")
	public String getPst_4mth_illness_symptom_ind() {
		return pst_4mth_illness_symptom_ind;
	}
	public void setPst_4mth_illness_symptom_ind(String pst_4mthIllnessSymptomInd) {
		pst_4mth_illness_symptom_ind = pst_4mthIllnessSymptomInd;
	}
	@XmlElement(name="symptom_desc")
	public String getSymptom_desc() {
		return symptom_desc;
	}
	public void setSymptom_desc(String symptomDesc) {
		symptom_desc = symptomDesc;
	}
	@XmlElement(name="cncr_ind")
	public String getCncr_ind() {
		return cncr_ind;
	}
	public void setCncr_ind(String cncrInd) {
		cncr_ind = cncrInd;
	}
	@XmlElement(name="bld_prblm_5yr_ind")
	public String getBld_prblm_5yr_ind() {
		return bld_prblm_5yr_ind;
	}
	public void setBld_prblm_5yr_ind(String bldPrblm_5yrInd) {
		bld_prblm_5yr_ind = bldPrblm_5yrInd;
	}
	@XmlElement(name="wnv_preg_diag_pos_test_ind")
	public String getWnv_preg_diag_pos_test_ind() {
		return wnv_preg_diag_pos_test_ind;
	}
	public void setWnv_preg_diag_pos_test_ind(String wnvPregDiagPosTestInd) {
		wnv_preg_diag_pos_test_ind = wnvPregDiagPosTestInd;
	}
	@XmlElement(name="viral_hepatitis_age_11_ind")
	public String getViral_hepatitis_age_11_ind() {
		return viral_hepatitis_age_11_ind;
	}
	public void setViral_hepatitis_age_11_ind(String viralHepatitisAge_11Ind) {
		viral_hepatitis_age_11_ind = viralHepatitisAge_11Ind;
	}
	@XmlElement(name="para_chagas_babesiosis_leish_ind")
	public String getPara_chagas_babesiosis_leish_ind() {
		return para_chagas_babesiosis_leish_ind;
	}
	public void setPara_chagas_babesiosis_leish_ind(
			String paraChagasBabesiosisLeishInd) {
		para_chagas_babesiosis_leish_ind = paraChagasBabesiosisLeishInd;
	}
	@XmlElement(name="cjd_diag_neuro_unk_ind")
	public String getCjd_diag_neuro_unk_ind() {
		return cjd_diag_neuro_unk_ind;
	}
	public void setCjd_diag_neuro_unk_ind(String cjdDiagNeuroUnkInd) {
		cjd_diag_neuro_unk_ind = cjdDiagNeuroUnkInd;
	}
	@XmlElement(name="bld_rlt_diag_rsk_cjd")
	public String getBld_rlt_diag_rsk_cjd() {
		return bld_rlt_diag_rsk_cjd;
	}
	public void setBld_rlt_diag_rsk_cjd(String bldRltDiagRskCjd) {
		bld_rlt_diag_rsk_cjd = bldRltDiagRskCjd;
	}
	@XmlElement(name="dura_mater_ind")
	public String getDura_mater_ind() {
		return dura_mater_ind;
	}
	public void setDura_mater_ind(String duraMaterInd) {
		dura_mater_ind = duraMaterInd;
	}
	@XmlElement(name="recv_xeno_tx_med_procedure_ind")
	public String getRecv_xeno_tx_med_procedure_ind() {
		return recv_xeno_tx_med_procedure_ind;
	}
	public void setRecv_xeno_tx_med_procedure_ind(String recvXenoTxMedProcedureInd) {
		recv_xeno_tx_med_procedure_ind = recvXenoTxMedProcedureInd;
	}
	@XmlElement(name="liv_cntc_xeno_tx_med_proc_ind")
	public String getLiv_cntc_xeno_tx_med_proc_ind() {
		return liv_cntc_xeno_tx_med_proc_ind;
	}
	public void setLiv_cntc_xeno_tx_med_proc_ind(String livCntcXenoTxMedProcInd) {
		liv_cntc_xeno_tx_med_proc_ind = livCntcXenoTxMedProcInd;
	}
	@XmlElement(name="malaria_antimalr_drug_3yr_ind")
	public String getMalaria_antimalr_drug_3yr_ind() {
		return malaria_antimalr_drug_3yr_ind;
	}
	public void setMalaria_antimalr_drug_3yr_ind(String malariaAntimalrDrug_3yrInd) {
		malaria_antimalr_drug_3yr_ind = malariaAntimalrDrug_3yrInd;
	}
	@XmlElement(name="outside_us_or_canada_3yr_ind")
	public String getOutside_us_or_canada_3yr_ind() {
		return outside_us_or_canada_3yr_ind;
	}
	public void setOutside_us_or_canada_3yr_ind(String outsideUsOrCanada_3yrInd) {
		outside_us_or_canada_3yr_ind = outsideUsOrCanada_3yrInd;
	}
	@XmlElement(name="outside_canada_us_desc")
	public String getOutside_canada_us_desc() {
		return outside_canada_us_desc;
	}
	public void setOutside_canada_us_desc(String outsideCanadaUsDesc) {
		outside_canada_us_desc = outsideCanadaUsDesc;
	}
	@XmlElement(name="blood_transf_b4coll_12m_ind")
	public String getBlood_transf_b4coll_12m_ind() {
		return blood_transf_b4coll_12m_ind;
	}
	public void setBlood_transf_b4coll_12m_ind(String bloodTransfB4coll_12mInd) {
		blood_transf_b4coll_12m_ind = bloodTransfB4coll_12mInd;
	}
	@XmlElement(name="tx_stemcell_12m_oth_self_ind")
	public String getTx_stemcell_12m_oth_self_ind() {
		return tx_stemcell_12m_oth_self_ind;
	}
	public void setTx_stemcell_12m_oth_self_ind(String txStemcell_12mOthSelfInd) {
		tx_stemcell_12m_oth_self_ind = txStemcell_12mOthSelfInd;
	}
	@XmlElement(name="tattoo_ear_skin_pierce_12m_ind")
	public String getTattoo_ear_skin_pierce_12m_ind() {
		return tattoo_ear_skin_pierce_12m_ind;
	}
	public void setTattoo_ear_skin_pierce_12m_ind(String tattooEarSkinPierce_12mInd) {
		tattoo_ear_skin_pierce_12m_ind = tattooEarSkinPierce_12mInd;
	}
	@XmlElement(name="tattoo_shared_nonsterile_12m_ind")
	public String getTattoo_shared_nonsterile_12m_ind() {
		return tattoo_shared_nonsterile_12m_ind;
	}
	public void setTattoo_shared_nonsterile_12m_ind(
			String tattooSharedNonsterile_12mInd) {
		tattoo_shared_nonsterile_12m_ind = tattooSharedNonsterile_12mInd;
	}
	@XmlElement(name="acc_ns_stk_cut_bld_12m_ind")
	public String getAcc_ns_stk_cut_bld_12m_ind() {
		return acc_ns_stk_cut_bld_12m_ind;
	}
	public void setAcc_ns_stk_cut_bld_12m_ind(String accNsStkCutBld_12mInd) {
		acc_ns_stk_cut_bld_12m_ind = accNsStkCutBld_12mInd;
	}
	@XmlElement(name="had_treat_syph_12m_ind")
	public String getHad_treat_syph_12m_ind() {
		return had_treat_syph_12m_ind;
	}
	public void setHad_treat_syph_12m_ind(String hadTreatSyph_12mInd) {
		had_treat_syph_12m_ind = hadTreatSyph_12mInd;
	}
	@XmlElement(name="given_mn_dr_for_sex_ind")
	public String getGiven_mn_dr_for_sex_ind() {
		return given_mn_dr_for_sex_ind;
	}
	public void setGiven_mn_dr_for_sex_ind(String givenMnDrForSexInd) {
		given_mn_dr_for_sex_ind = givenMnDrForSexInd;
	}
	@XmlElement(name="sex_w_tk_mn_dr_pmt_sex_12m_5yr_ind")
	public String getSex_w_tk_mn_dr_pmt_sex_12m_5yr_ind() {
		return sex_w_tk_mn_dr_pmt_sex_12m_5yr_ind;
	}
	public void setSex_w_tk_mn_dr_pmt_sex_12m_5yr_ind(
			String sexWTkMnDrPmtSex_12m_5yrInd) {
		sex_w_tk_mn_dr_pmt_sex_12m_5yr_ind = sexWTkMnDrPmtSex_12m_5yrInd;
	}
	@XmlElement(name="sex_cntc_liv_hep_b_c_ind")public String getSex_cntc_liv_hep_b_c_ind() {
		return sex_cntc_liv_hep_b_c_ind;
	}
	public void setSex_cntc_liv_hep_b_c_ind(String sexCntcLivHepBCInd) {
		sex_cntc_liv_hep_b_c_ind = sexCntcLivHepBCInd;
	}
	@XmlElement(name="sex_w_iv_dr_in_pst_5yr_12m_ind")
	public String getSex_w_iv_dr_in_pst_5yr_12m_ind() {
		return sex_w_iv_dr_in_pst_5yr_12m_ind;
	}
	public void setSex_w_iv_dr_in_pst_5yr_12m_ind(String sexWIvDrInPst_5yr_12mInd) {
		sex_w_iv_dr_in_pst_5yr_12m_ind = sexWIvDrInPst_5yr_12mInd;
	}
	@XmlElement(name="sex_w_man_oth_pst_5yr_12m_ind")
	public String getSex_w_man_oth_pst_5yr_12m_ind() {
		return sex_w_man_oth_pst_5yr_12m_ind;
	}
	public void setSex_w_man_oth_pst_5yr_12m_ind(String sexWManOthPst_5yr_12mInd) {
		sex_w_man_oth_pst_5yr_12m_ind = sexWManOthPst_5yr_12mInd;
	}
	@XmlElement(name="sex_w_clot_fact_bld_12m_ind")
	public String getSex_w_clot_fact_bld_12m_ind() {
		return sex_w_clot_fact_bld_12m_ind;
	}
	public void setSex_w_clot_fact_bld_12m_ind(String sexWClotFactBld_12mInd) {
		sex_w_clot_fact_bld_12m_ind = sexWClotFactBld_12mInd;
	}
	@XmlElement(name="sex_w_hiv_aids_12m_ind")
	public String getSex_w_hiv_aids_12m_ind() {
		return sex_w_hiv_aids_12m_ind;
	}
	public void setSex_w_hiv_aids_12m_ind(String sexWHivAids_12mInd) {
		sex_w_hiv_aids_12m_ind = sexWHivAids_12mInd;
	}
	@XmlElement(name="prison_jail_contin_72hrs_ind")
	public String getPrison_jail_contin_72hrs_ind() {
		return prison_jail_contin_72hrs_ind;
	}
	public void setPrison_jail_contin_72hrs_ind(String prisonJailContin_72hrsInd) {
		prison_jail_contin_72hrs_ind = prisonJailContin_72hrsInd;
	}
	@XmlElement(name="pst_5yr_tk_mn_dr_sex_ind")
	public String getPst_5yr_tk_mn_dr_sex_ind() {
		return pst_5yr_tk_mn_dr_sex_ind;
	}
	public void setPst_5yr_tk_mn_dr_sex_ind(String pst_5yrTkMnDrSexInd) {
		pst_5yr_tk_mn_dr_sex_ind = pst_5yrTkMnDrSexInd;
	}
	@XmlElement(name="pst_5yr_iv_drug_ind")
	public String getPst_5yr_iv_drug_ind() {
		return pst_5yr_iv_drug_ind;
	}
	public void setPst_5yr_iv_drug_ind(String pst_5yrIvDrugInd) {
		pst_5yr_iv_drug_ind = pst_5yrIvDrugInd;
	}
	@XmlElement(name="aids_hiv_screen_test_ind")
	public String getAids_hiv_screen_test_ind() {
		return aids_hiv_screen_test_ind;
	}
	public void setAids_hiv_screen_test_ind(String aidsHivScreenTestInd) {
		aids_hiv_screen_test_ind = aidsHivScreenTestInd;
	}
	@XmlElement(name="unexplained_symptom_ind")
	public String getUnexplained_symptom_ind() {
		return unexplained_symptom_ind;
	}
	public void setUnexplained_symptom_ind(String unexplainedSymptomInd) {
		unexplained_symptom_ind = unexplainedSymptomInd;
	}
	@XmlElement(name="unexpln_night_sweat")
	public String getUnexpln_night_sweat() {
		return unexpln_night_sweat;
	}
	public void setUnexpln_night_sweat(String unexplnNightSweat) {
		unexpln_night_sweat = unexplnNightSweat;
	}
	@XmlElement(name="unexpln_blue_prpl_spot_kaposi_ind")
	public String getUnexpln_blue_prpl_spot_kaposi_ind() {
		return unexpln_blue_prpl_spot_kaposi_ind;
	}
	public void setUnexpln_blue_prpl_spot_kaposi_ind(
			String unexplnBluePrplSpotKaposiInd) {
		unexpln_blue_prpl_spot_kaposi_ind = unexplnBluePrplSpotKaposiInd;
	}
	@XmlElement(name="unexpln_weight_loss")
	public String getUnexpln_weight_loss() {
		return unexpln_weight_loss;
	}
	public void setUnexpln_weight_loss(String unexplnWeightLoss) {
		unexpln_weight_loss = unexplnWeightLoss;
	}
	@XmlElement(name="unexpln_persist_diarrhea")
	public String getUnexpln_persist_diarrhea() {
		return unexpln_persist_diarrhea;
	}
	public void setUnexpln_persist_diarrhea(String unexplnPersistDiarrhea) {
		unexpln_persist_diarrhea = unexplnPersistDiarrhea;
	}
	@XmlElement(name="unexpln_cough_short_breath")
	public String getUnexpln_cough_short_breath() {
		return unexpln_cough_short_breath;
	}
	public void setUnexpln_cough_short_breath(String unexplnCoughShortBreath) {
		unexpln_cough_short_breath = unexplnCoughShortBreath;
	}
	@XmlElement(name="unexpln_temp_over_ten_days")
	public String getUnexpln_temp_over_ten_days() {
		return unexpln_temp_over_ten_days;
	}
	public void setUnexpln_temp_over_ten_days(String unexplnTempOverTenDays) {
		unexpln_temp_over_ten_days = unexplnTempOverTenDays;
	}
	@XmlElement(name="unexpln_mouth_sores_wht_spt")
	public String getUnexpln_mouth_sores_wht_spt() {
		return unexpln_mouth_sores_wht_spt;
	}
	public void setUnexpln_mouth_sores_wht_spt(String unexplnMouthSoresWhtSpt) {
		unexpln_mouth_sores_wht_spt = unexplnMouthSoresWhtSpt;
	}
	@XmlElement(name="unexpln_1m_lump_nk_apit_grn_mult_ind")
	public String getUnexpln_1m_lump_nk_apit_grn_mult_ind() {
		return unexpln_1m_lump_nk_apit_grn_mult_ind;
	}
	public void setUnexpln_1m_lump_nk_apit_grn_mult_ind(
			String unexpln_1mLumpNkApitGrnMultInd) {
		unexpln_1m_lump_nk_apit_grn_mult_ind = unexpln_1mLumpNkApitGrnMultInd;
	}
	@XmlElement(name="inf_dur_preg_ind")
	public String getInf_dur_preg_ind() {
		return inf_dur_preg_ind;
	}
	public void setInf_dur_preg_ind(String infDurPregInd) {
		inf_dur_preg_ind = infDurPregInd;
	}
	@XmlElement(name="htlv_incl_screen_test_para_ind")
	public String getHtlv_incl_screen_test_para_ind() {
		return htlv_incl_screen_test_para_ind;
	}
	public void setHtlv_incl_screen_test_para_ind(String htlvInclScreenTestParaInd) {
		htlv_incl_screen_test_para_ind = htlvInclScreenTestParaInd;
	}
	@XmlElement(name="hiv_contag_person_understand_ind")
	public String getHiv_contag_person_understand_ind() {
		return hiv_contag_person_understand_ind;
	}
	public void setHiv_contag_person_understand_ind(
			String hivContagPersonUnderstandInd) {
		hiv_contag_person_understand_ind = hivContagPersonUnderstandInd;
	}
	@XmlElement(name="cjd_live_travel_country_ind")
	public String getCjd_live_travel_country_ind() {
		return cjd_live_travel_country_ind;
	}
	public void setCjd_live_travel_country_ind(String cjdLiveTravelCountryInd) {
		cjd_live_travel_country_ind = cjdLiveTravelCountryInd;
	}
	@XmlElement(name="spent_uk_ge_3m_ind")
	public String getSpent_uk_ge_3m_ind() {
		return spent_uk_ge_3m_ind;
	}
	public void setSpent_uk_ge_3m_ind(String spentUkGe_3mInd) {
		spent_uk_ge_3m_ind = spentUkGe_3mInd;
	}
	@XmlElement(name="recv_trnsfsn_uk_france_ind")
	public String getRecv_trnsfsn_uk_france_ind() {
		return recv_trnsfsn_uk_france_ind;
	}
	public void setRecv_trnsfsn_uk_france_ind(String recvTrnsfsnUkFranceInd) {
		recv_trnsfsn_uk_france_ind = recvTrnsfsnUkFranceInd;
	}
	@XmlElement(name="spent_europe_ge_5y_cjd_cnt_ind")
	public String getSpent_europe_ge_5y_cjd_cnt_ind() {
		return spent_europe_ge_5y_cjd_cnt_ind;
	}
	public void setSpent_europe_ge_5y_cjd_cnt_ind(String spentEuropeGe_5yCjdCntInd) {
		spent_europe_ge_5y_cjd_cnt_ind = spentEuropeGe_5yCjdCntInd;
	}
	@XmlElement(name="us_military_dep_ind")
	public String getUs_military_dep_ind() {
		return us_military_dep_ind;
	}
	public void setUs_military_dep_ind(String usMilitaryDepInd) {
		us_military_dep_ind = usMilitaryDepInd;
	}
	@XmlElement(name="mil_base_europe_1_ge_6m_ind")
	public String getMil_base_europe_1_ge_6m_ind() {
		return mil_base_europe_1_ge_6m_ind;
	}
	public void setMil_base_europe_1_ge_6m_ind(String milBaseEurope_1Ge_6mInd) {
		mil_base_europe_1_ge_6m_ind = milBaseEurope_1Ge_6mInd;
	}
	@XmlElement(name="mil_base_europe_2_ge_6m_ind")
	public String getMil_base_europe_2_ge_6m_ind() {
		return mil_base_europe_2_ge_6m_ind;
	}
	public void setMil_base_europe_2_ge_6m_ind(String milBaseEurope_2Ge_6mInd) {
		mil_base_europe_2_ge_6m_ind = milBaseEurope_2Ge_6mInd;
	}
	@XmlElement(name="hiv_1_2_o_performed_ind")
	public String getHiv_1_2_o_performed_ind() {
		return hiv_1_2_o_performed_ind;
	}
	public void setHiv_1_2_o_performed_ind(String hiv_1_2OPerformedInd) {
		hiv_1_2_o_performed_ind = hiv_1_2OPerformedInd;
	}
	@XmlElement(name="born_live_cam_afr_nig_ind")
	public String getBorn_live_cam_afr_nig_ind() {
		return born_live_cam_afr_nig_ind;
	}
	public void setBorn_live_cam_afr_nig_ind(String bornLiveCamAfrNigInd) {
		born_live_cam_afr_nig_ind = bornLiveCamAfrNigInd;
	}
	@XmlElement(name="trav_ctry_rec_bld_or_med_ind")
	public String getTrav_ctry_rec_bld_or_med_ind() {
		return trav_ctry_rec_bld_or_med_ind;
	}
	public void setTrav_ctry_rec_bld_or_med_ind(String travCtryRecBldOrMedInd) {
		trav_ctry_rec_bld_or_med_ind = travCtryRecBldOrMedInd;
	}
	@XmlElement(name="sex_w_born_live_in_ctry_ind")
	public String getSex_w_born_live_in_ctry_ind() {
		return sex_w_born_live_in_ctry_ind;
	}
	public void setSex_w_born_live_in_ctry_ind(String sexWBornLiveInCtryInd) {
		sex_w_born_live_in_ctry_ind = sexWBornLiveInCtryInd;
	}
	
	
	
}
