package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CB_CORD_FINAL_REVIEW")
@SequenceGenerator(sequenceName="SEQ_CB_CORD_FINAL_REVIEW",name="SEQ_CB_CORD_FINAL_REVIEW",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class CBUFinalReview extends Auditable{
	
	private Long pkCordFinalReview;
	private Long fkCordId;
	//commited without order
	//private Long fkOrderId;
	private Long licensureConfirmFlag;
	private Long eligibleConfirmFlag;
	private Boolean deletedFlag;
	//private CdrCbu cdrCbu;
	private Long reviewCordAcceptance;
	private String finalReviewConfirmStatus;
	private Date licCreatedOn;
	private Long LicCreatedBy;
	private Date eligCreatedOn;
	private Long eligCreatedBy;
	private Date finalReviewCreatedOn;
	private Long finalReviewCreatedBy;
	private Long fkFinalEligQuesId;
	private Long confirmCBBSignatureFlag;
	private Date confirmCBBSignCreatedOn;
	private Long confirmCBBSignCreator;
	private String finalEligibleCreator;
	//commited without order
	/*
	private String eligibleChangeReason;
	private String licensureChangeReason;
	*/
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_CORD_FINAL_REVIEW")
	@Column(name="PK_CORD_FINAL_REVIEW")
	public Long getPkCordFinalReview() {
		return pkCordFinalReview;
	}
	public void setPkCordFinalReview(Long pkCordFinalReview) {
		this.pkCordFinalReview = pkCordFinalReview;
	}
	@Column(name="FK_CORD_ID")
	public Long getFkCordId() {
		return fkCordId;
	}
	public void setFkCordId(Long fkCordId) {
		this.fkCordId = fkCordId;
	}
	@Column(name="LICENSURE_CONFIRM_FLAG")
	public Long getLicensureConfirmFlag() {
		return licensureConfirmFlag;
	}
	public void setLicensureConfirmFlag(Long licensureConfirmFlag) {
		this.licensureConfirmFlag = licensureConfirmFlag;
	}
	@Column(name="ELIGIBLE_CONFIRM_FLAG")
	public Long getEligibleConfirmFlag() {
		return eligibleConfirmFlag;
	}
	public void setEligibleConfirmFlag(Long eligibleConfirmFlag) {
		this.eligibleConfirmFlag = eligibleConfirmFlag;
	}
	@Column(name="DELETEDFLAG")
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	
	@Column(name="REVIEW_CORD_ACCEPTABLE_FLAG")
	public Long getReviewCordAcceptance() {
		return reviewCordAcceptance;
	}
	public void setReviewCordAcceptance(Long reviewCordAcceptance) {
		this.reviewCordAcceptance = reviewCordAcceptance;
	}
	@Column(name="FINAL_REVIEW_CONFIRM_FLAG")
	public String getFinalReviewConfirmStatus() {
		return finalReviewConfirmStatus;
	}
	public void setFinalReviewConfirmStatus(String finalReviewConfirmStatus) {
		this.finalReviewConfirmStatus = finalReviewConfirmStatus;
	}
	//commited without order
	/*
	@Column(name="FK_ORDER_ID")
	public Long getFkOrderId() {
		return fkOrderId;
	}
	public void setFkOrderId(Long fkOrderId) {
		this.fkOrderId = fkOrderId;
	}*/
	@Column(name="FINAL_ELIGIBLE_CREATOR")
	public String getFinalEligibleCreator() {
		return finalEligibleCreator;
	}
	public void setFinalEligibleCreator(String finalEligibleCreator) {
		this.finalEligibleCreator = finalEligibleCreator;
	}
	@Column(name="LIC_CREATED_ON")
	public Date getLicCreatedOn() {
		return licCreatedOn;
	}
	public void setLicCreatedOn(Date licCreatedOn) {
		this.licCreatedOn = licCreatedOn;
	}
	@Column(name="LIC_CREATOR")
	public Long getLicCreatedBy() {
		return LicCreatedBy;
	}
	public void setLicCreatedBy(Long licCreatedBy) {
		LicCreatedBy = licCreatedBy;
	}
	@Column(name="ELIG_CREATED_ON")
	public Date getEligCreatedOn() {
		return eligCreatedOn;
	}
	public void setEligCreatedOn(Date eligCreatedOn) {
		this.eligCreatedOn = eligCreatedOn;
	}
	@Column(name="ELIG_CREATOR")
	public Long getEligCreatedBy() {
		return eligCreatedBy;
	}
	public void setEligCreatedBy(Long eligCreatedBy) {
		this.eligCreatedBy = eligCreatedBy;
	}
	@Column(name="FINAL_REVIEW_CREATED_ON")
	public Date getFinalReviewCreatedOn() {
		return finalReviewCreatedOn;
	}
	public void setFinalReviewCreatedOn(Date finalReviewCreatedOn) {
		this.finalReviewCreatedOn = finalReviewCreatedOn;
	}
	@Column(name="FINAL_REVIEW_CREATOR")
	public Long getFinalReviewCreatedBy() {
		return finalReviewCreatedBy;
	}
	public void setFinalReviewCreatedBy(Long finalReviewCreatedBy) {
		this.finalReviewCreatedBy = finalReviewCreatedBy;
	}
	@Column(name="FK_FINAL_ELIG_QUES")
	public Long getFkFinalEligQuesId() {
		return fkFinalEligQuesId;
	}
	public void setFkFinalEligQuesId(Long fkFinalEligQuesId) {
		this.fkFinalEligQuesId = fkFinalEligQuesId;
	}
	@Column(name="CONFIRM_CBB_SIGN_FLAG")
	public Long getConfirmCBBSignatureFlag() {
		return confirmCBBSignatureFlag;
	}
	public void setConfirmCBBSignatureFlag(Long confirmCBBSignatureFlag) {
		this.confirmCBBSignatureFlag = confirmCBBSignatureFlag;
	}
	@Column(name="CBB_SIGNATURE_CREATED_ON")
	public Date getConfirmCBBSignCreatedOn() {
		return confirmCBBSignCreatedOn;
	}
	public void setConfirmCBBSignCreatedOn(Date confirmCBBSignCreatedOn) {
		this.confirmCBBSignCreatedOn = confirmCBBSignCreatedOn;
	}
	@Column(name="CBB_SIGNATURE_CREATOR")
	public Long getConfirmCBBSignCreator() {
		return confirmCBBSignCreator;
	}
	public void setConfirmCBBSignCreator(Long confirmCBBSignCreator) {
		this.confirmCBBSignCreator = confirmCBBSignCreator;
	}
	
	
}
