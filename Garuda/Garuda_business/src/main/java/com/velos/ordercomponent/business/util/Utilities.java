package com.velos.ordercomponent.business.util;

import java.io.InputStream;
import java.sql.Clob;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.velos.eres.service.util.Configuration;
import com.velos.ordercomponent.helper.OrderAttachmentHelper;


/**
 * Class used for having utility methods for the whole application.
 *
 * @author bbabu
 * @version 1.0
 */

public class Utilities {

	private static Logger log = Logger.getLogger(OrderAttachmentHelper.class);
	private static String DEF_DATE_FORMAT = "MM/dd/yyyy";
	private static String TIMESTAMP_FORMAT ="MM/dd/yyyy hh:mm";
	/**
	 * Responsible for returning the current date
	 *
	 * @return Date
	 * @throws Exception
	 */
	public static Date getCurrentDate() throws Exception {
		try {
			long ts = System.currentTimeMillis();
			Date utilDate = new java.util.Date(ts);
			return utilDate;
		} catch (Exception e) {
			throw e;
		}
	}


	/** Method used to get current timestamp value
	 *
	 * @return Timestamp
	 */

	public static Timestamp getCurrentTimeStamp(){
	       Date d=new Date();
	       return new java.sql.Timestamp(d.getTime());
	   }



	/**
	 * Method used to validate the given date as String.
	 *
	 * @param dateStr -
	 * @param format
	 * @return Date
	 * @throws Exception
	 */
	public static Date getDate(String dateStr, String format) throws Exception {
		if (format == null) {
			format = DEF_DATE_FORMAT;
		}
		try {
			SimpleDateFormat df = new SimpleDateFormat(format);
			Date date = df.parse(dateStr);
			return date;
		} catch (Exception e) {
			throw e;
		}
	}



	/**
	 * Method used to validate the given date as String.
	 *
	 * @param dateStr -
	 * @param format
	 * @return Date
	 * @throws Exception
	 */
	public static Date getDate(String dateStr) throws Exception {
		String format = DEF_DATE_FORMAT;
		return getDate(dateStr, format);
	}


	/**
	 * Responsible for retrieving LeonyFormatted parameter Date
	 *
	 * @param d
	 * @return
	 * @throws Exception
	 */

	public static String getFormattedDate(Date date) throws Exception{
		return getFormattedDate(date, DEF_DATE_FORMAT).toString();
	}

	/**
	 * Method used to get the formatted date for given date in the given format. If format is not
	 * given, the date is rendered in the default format.
	 *
	 * @param date
	 * @param format
	 * @return String
	 * @throws Exception
	 */
	public static String getFormattedDate(Date date, String format) throws Exception {
		String fmt = format;
		try {
			if (fmt == null) {
				fmt = DEF_DATE_FORMAT;
			}
			SimpleDateFormat sdf = new SimpleDateFormat(fmt);
			
			return sdf.format(date);
		} catch (Exception e) {
			//throw new RDSApplicationException("invalid Date:" + date, e);
			throw e;
		}
	}

	 public static Timestamp convertstringtotimestamp(String date,String hrs,String mins) throws Exception{
		   SimpleDateFormat formatter;
			formatter = new SimpleDateFormat(TIMESTAMP_FORMAT);
			String hours="";
			String strmins="";
			if(hrs.length()!=0){

				hours=hrs;

			}else{

			hours="00";
			}
			if(mins.length()!=0){
				strmins=mins;
			}else{
				strmins="00";
			}
		String time =hours + ":" + strmins ;
		String strdate = date + " " + time;
		Date date1=formatter.parse(strdate);
	//	String s = formatter.format(date1);
		return new java.sql.Timestamp(date1.getTime());
	}

	 public static byte[] covertClobToByte(Clob clob) throws Exception{
		 if(clob != null){
			 byte[] thedata=new byte[(int)clob.length()];
			 InputStream is=clob.getAsciiStream();
			 is.read(thedata);
			// System.out.println(thedata.toString());
			 return thedata;
		 }else{
			 return null;
		 }
	 }

	 public static String getDateFormatDate(String fromDate){
		 String returnDate = "";
		 	try{
				DateFormat fromFormat=new SimpleDateFormat("MMM dd,yyyy");
	
				Date utilDate=fromFormat.parse(fromDate);
	
				DateFormat toFormat=new SimpleDateFormat("MM/dd/yyyy");
	
				returnDate=toFormat.format(utilDate);
	
				//System.out.println(returnDate);
		 	}catch(Exception e){
		 		e.printStackTrace();
		 	}
		 	
		 	return returnDate;
	 }
	 public static String getOrdDateFormatDate(String fromDate){
		 String returnDate = "";
		 	try{
				DateFormat fromFormat=new SimpleDateFormat("dd-MMM-yyyy");
	
				Date utilDate=fromFormat.parse(fromDate);
	
				DateFormat toFormat=new SimpleDateFormat("MMM dd, yyyy");
	
				returnDate=toFormat.format(utilDate);
	
				log.debug("return date......."+returnDate);
		 	}catch(Exception e){
		 		e.printStackTrace();
		 	}
		 	
		 	return returnDate;
	 }
	 
	 public static Date getXmlDateFormat(XMLGregorianCalendar formDate){
		 String dateStr = "";
		 Date date = null;
		 try{
		 if(formDate!=null){
			 	dateStr = formDate+"";
			 	date = getXmlDateFormat(dateStr);
	    	}
		 }catch(Exception e){
			 //System.out.println("Exception in Utilities : getXmlDateFormat ::"+e);
			 e.printStackTrace();
		 }
		 return date;
	 }
	 
	 public static Date getXmlDateFormat(String formDate){
		 Date date = null;
		 SimpleDateFormat formatter = null;
		 try{
		 	if(formDate!=null && formDate.length()==20){
		 		formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		 		date = formatter.parse(formDate.substring(0, 20));
		 	}
		 	else if(formDate!=null && formDate.length()==19){
		 		formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		 		date = formatter.parse(formDate.substring(0, 19));
		 	}
		 }catch(Exception e){
			 //System.out.println("Exception in Utilities : getXmlDateFormat ::"+e);
			 e.printStackTrace();
		 }
		 return date;
	 }
	 
	 public static String getMessageProperties(String messageKey){
		 ResourceBundle resource = ResourceBundle.getBundle(VelosMidConstants.EXCEPTION_PROPERTYFILE_NAME);
		 return resource.getString(messageKey);
	 }
	 public static int nullIntconv(String str)
		{   
		    int conv=0;
		    if(str==null)
		    {
		        str="0";
		    }
		    else if((str.trim()).equals("null"))
		    {
		        str="0";
		    }
		    else if(str.equals(""))
		    {
		        str="0";
		    }else if(str.equals("undefined")){
		    	 str="0";
		    }
		    try{
		        conv=Integer.parseInt(str);
		    }
		    catch(Exception e)
		    {
		    	//System.out.println("exception utilities nullIntconv method::"+e);
		    	e.printStackTrace();
		    }
		    return conv;
		}
	 
	 public static String removeDelimeters(String stringValue,String delimiter){
		 if(stringValue!=null && !StringUtils.isEmpty(stringValue) && stringValue.indexOf(delimiter)!=-1){
			 while(stringValue.indexOf(delimiter)>0){
				   int index =  stringValue.indexOf(delimiter);
				   stringValue = stringValue.substring(0,index)+stringValue.substring(index+1,stringValue.length());					   
			   }
			 }
		 return stringValue;
		}
	 
	 public static int getIntValue(String stringValue){
		 int returnVal = 0;
		 try{
			 if(stringValue!=null && !stringValue.equals("")){
				 returnVal = Integer.parseInt(stringValue);
			 }
		 }	 
		 catch (Exception e) {
			//System.out.println("Exception:::::::"+e);
			 e.printStackTrace();
		}
		 return returnVal;
	 }
	 public static Boolean removesqlconstants(String qry){
		 Boolean flag=null;
		 if(!qry.toLowerCase().contains("insert") && !qry.toLowerCase().contains("delete") && !qry.toLowerCase().contains("update") && !qry.toLowerCase().contains("call")){
			 //log.debug("No sql injection found::::::::"+qry);
			 return true;
		 }else{
			 //log.debug("sql injection found::::::::"+qry);
			 return false;
		 }
		 
	 }
	 
	 public static String file(String fileName, String fileExtension, HttpServletRequest request){
			
		 String  fName = null;
		 String fpath ="";
		 Long  userId = null;
			
			
			if(request != null){
				
				 
				 
				 boolean onlyFileName = (Boolean) request.getAttribute("onlyFileName");
				 
				 if(onlyFileName){					 
					
					 userId = (Long) request.getAttribute("userId");
					 
					 fName = fileName+"["+System.currentTimeMillis()+"]";
					 fName += userId;
					 fName += fileExtension;
					 
					 request.setAttribute("onlyFileName", false);
					 request.setAttribute("fileName", fName);
					 return fName;
				 }else{
					 
					 fName = (String) request.getAttribute("fileName");
				 }
					 
				 
				 
			}
					Configuration.readAppendixParam(Configuration.ERES_HOME + "eresearch.xml");
					com.aithent.file.uploadDownload.Configuration.readSettings("eres");
					com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "study");	
					String fileDnPath = com.aithent.file.uploadDownload.Configuration.DOWNLOADSERVLET ;
					Configuration.readReportParam(Configuration.ERES_HOME + "eresearch.xml");
			
				  fpath  += Configuration.REPDWNLDPATH;
				  fpath = fpath.replace("\\", "/");
				 
				  if( fName == null ){
					  
					  fName  = fileName+"["+ System.nanoTime()+ "]";
					  //fName += userId;
					  fName += fileExtension;
				  }
				  
				  fpath += "/";
				  fpath += fName;
				  
			  
				  System.out.println("File Path : "+fpath);
				
			return  fpath;
		}
	 	
}


