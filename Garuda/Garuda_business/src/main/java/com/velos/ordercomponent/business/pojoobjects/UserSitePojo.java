package com.velos.ordercomponent.business.pojoobjects;

public class UserSitePojo extends AuditablePojo {

	
	   /**
	 * 
	 */
	private static final long serialVersionUID = 3704410728365395768L;
	   
	   private Long pkUserSite;
	   private Long fkUser;
	   private Long fkSite;
	   private Long userRight;

	   
	   
	public void setPkUserSite(Long pkUserSite) {
		this.pkUserSite = pkUserSite;
	}
	public Long getPkUserSite() {
		return pkUserSite;
	}
	public void setFkUser(Long fkUser) {
		this.fkUser = fkUser;
	}
	public Long getFkUser() {
		return fkUser;
	}
	public void setFkSite(Long fkSite) {
		this.fkSite = fkSite;
	}
	public Long getFkSite() {
		return fkSite;
	}
	public void setUserRight(Long userRight) {
		this.userRight = userRight;
	}
	public Long getUserRight() {
		return userRight;
	}
	
}
