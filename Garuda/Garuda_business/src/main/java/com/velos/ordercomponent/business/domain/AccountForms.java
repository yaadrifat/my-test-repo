package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity
@Table(name="ER_ACCTFORMS")
@SequenceGenerator(sequenceName="SEQ_ER_ACCTFORMS",name="SEQ_ER_ACCTFORMS",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class AccountForms extends Auditable{
	/*PK_ACCTFORMS
	FK_FORMLIB
	FK_ACCOUNT
	ACCTFORMS_FILLDATE
	ACCTFORMS_XML
	RID
	RECORD_TYPE
	FORM_COMPLETED
	CREATOR
	LAST_MODIFIED_BY
	ISVALID
	LAST_MODIFIED_DATE
	CREATED_ON
	IP_ADD
	NOTIFICATION_SENT
	PROCESS_DATA
	FK_FORMLIBVER
	FK_SPECIMEN
	*/
	private Long pkAccoutForms;
	private Long fkFormLib;
	private Long fkAcoount;
	private Date formFillDate;
	private Long fkSpecimen;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_ER_ACCTFORMS")
	@Column(name="PK_ACCTFORMS")
	public Long getPkAccoutForms() {
		return pkAccoutForms;
	}
	public void setPkAccoutForms(Long pkAccoutForms) {
		this.pkAccoutForms = pkAccoutForms;
	}
	@Column(name="FK_FORMLIB")
	public Long getFkFormLib() {
		return fkFormLib;
	}
	public void setFkFormLib(Long fkFormLib) {
		this.fkFormLib = fkFormLib;
	}
	@Column(name="FK_ACCOUNT")
	public Long getFkAcoount() {
		return fkAcoount;
	}
	public void setFkAcoount(Long fkAcoount) {
		this.fkAcoount = fkAcoount;
	}
	@Column(name="ACCTFORMS_FILLDATE")
	public Date getFormFillDate() {
		return formFillDate;
	}
	public void setFormFillDate(Date formFillDate) {
		this.formFillDate = formFillDate;
	}
	@Column(name="FK_SPECIMEN")
	public Long getFkSpecimen() {
		return fkSpecimen;
	}
	public void setFkSpecimen(Long fkSpecimen) {
		this.fkSpecimen = fkSpecimen;
	}
	
}
