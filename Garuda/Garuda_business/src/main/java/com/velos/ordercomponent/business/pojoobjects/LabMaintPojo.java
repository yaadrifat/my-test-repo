package com.velos.ordercomponent.business.pojoobjects;



public class LabMaintPojo extends AuditablePojo {
	
	private Long pk_LabTestId;
	private Long fk_CategroyId;
	private String labTestName; 
	private String normalRangefrom; 
	private String normalRangeTo; 
	private String cptCode; 
	private String testUnit;
	private Boolean deletedFlag;
	public Long getPk_LabTestId() {
		return pk_LabTestId;
	}
	public void setPk_LabTestId(Long pk_LabTestId) {
		this.pk_LabTestId = pk_LabTestId;
	}
	public Long getFk_CategroyId() {
		return fk_CategroyId;
	}
	public void setFk_CategroyId(Long fk_CategroyId) {
		this.fk_CategroyId = fk_CategroyId;
	}
	public String getLabTestName() {
		return labTestName;
	}
	public void setLabTestName(String labTestName) {
		this.labTestName = labTestName;
	}
	public String getNormalRangefrom() {
		return normalRangefrom;
	}
	public void setNormalRangefrom(String normalRangefrom) {
		this.normalRangefrom = normalRangefrom;
	}
	public String getNormalRangeTo() {
		return normalRangeTo;
	}
	public void setNormalRangeTo(String normalRangeTo) {
		this.normalRangeTo = normalRangeTo;
	}
	public String getCptCode() {
		return cptCode;
	}
	public void setCptCode(String cptCode) {
		this.cptCode = cptCode;
	}
	public String getTestUnit() {
		return testUnit;
	}
	public void setTestUnit(String testUnit) {
		this.testUnit = testUnit;
	}
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	
	
	
}
