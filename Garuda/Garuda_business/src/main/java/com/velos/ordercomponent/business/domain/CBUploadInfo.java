package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CB_UPLOAD_INFO")
@SequenceGenerator(sequenceName="SEQ_CB_UPLOAD_INFO",name="SEQ_CB_UPLOAD_INFO",allocationSize=1)
	public class CBUploadInfo extends Auditable {
		private Long pk_UploadInfo;
		private String description;
		private Date completionDate; 
		private Date testDate; 
		private Date processDate; 
		private String veficationTyping;
		private Date receivedDate; 
		private Long fk_CategoryId;
		private Long fk_SubCategoryId;
		private Long fk_AttachmentId;
		private Boolean deletedFlag;
		private Date reportDate; 
		
		@Id
		@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_UPLOAD_INFO")
		@Column(name="PK_UPLOAD_INFO")
		public Long getPk_UploadInfo() {
			return pk_UploadInfo;
		}
		public void setPk_UploadInfo(Long pk_UploadInfo) {
			this.pk_UploadInfo = pk_UploadInfo;
		}
		@Column(name="DESCRIPTION")
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		@Column(name="COMPLETION_DATE")
		public Date getCompletionDate() {
			return completionDate;
		}
		public void setCompletionDate(Date completionDate) {
			this.completionDate = completionDate;
		}
		@Column(name="TEST_DATE")
		public Date getTestDate() {
			return testDate;
		}
		public void setTestDate(Date testDate) {
			this.testDate = testDate;
		}
		@Column(name="PROCESS_DATE")
		public Date getProcessDate() {
			return processDate;
		}
		public void setProcessDate(Date processDate) {
			this.processDate = processDate;
		}
		@Column(name="VERIFICATION_TYPING")
		public String getVeficationTyping() {
			return veficationTyping;
		}
		public void setVeficationTyping(String veficationTyping) {
			this.veficationTyping = veficationTyping;
		}
		@Column(name="RECEIVED_DATE")
		public Date getReceivedDate() {
			return receivedDate;
		}
		public void setReceivedDate(Date receivedDate) {
			this.receivedDate = receivedDate;
		}
		@Column(name="FK_CATEGORY")
		public Long getFk_CategoryId() {
			return fk_CategoryId;
		}
		public void setFk_CategoryId(Long fk_CategoryId) {
			this.fk_CategoryId = fk_CategoryId;
		}
		@Column(name="FK_SUBCATEGORY")
		public Long getFk_SubCategoryId() {
			return fk_SubCategoryId;
		}
		public void setFk_SubCategoryId(Long fk_SubCategoryId) {
			this.fk_SubCategoryId = fk_SubCategoryId;
		}
		@Column(name="FK_ATTACHMENTID")
		public Long getFk_AttachmentId() {
			return fk_AttachmentId;
		}
		public void setFk_AttachmentId(Long fk_AttachmentId) {
			this.fk_AttachmentId = fk_AttachmentId;
		}
		@Column(name="DELETEDFLAG")
		public Boolean getDeletedFlag() {
			return deletedFlag;
		}
		public void setDeletedFlag(Boolean deletedFlag) {
			this.deletedFlag = deletedFlag;
		}
		@Column(name="REPORT_DATE")
		public Date getReportDate() {
			return reportDate;
		}
		public void setReportDate(Date reportDate) {
			this.reportDate = reportDate;
		}
		

}
