package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

public class FundingGuidelinesPojo extends AuditablePojo {
	
	private Long pkFundingGuidelin;
	private Long fkCbbId;
	private String fundingId;
	private Long fkFundCateg;
	private Date fundingBdate;
	private String description;
	private String fundingStatus;
	private Boolean deletedFlag;
	
	public Long getPkFundingGuidelin() {
		return pkFundingGuidelin;
	}
	public void setPkFundingGuidelin(Long pkFundingGuidelin) {
		this.pkFundingGuidelin = pkFundingGuidelin;
	}
	public Long getFkCbbId() {
		return fkCbbId;
	}
	public void setFkCbbId(Long fkCbbId) {
		this.fkCbbId = fkCbbId;
	}
	public String getFundingId() {
		return fundingId;
	}
	public void setFundingId(String fundingId) {
		this.fundingId = fundingId;
	}
	public Long getFkFundCateg() {
		return fkFundCateg;
	}
	public void setFkFundCateg(Long fkFundCateg) {
		this.fkFundCateg = fkFundCateg;
	}

	public Date getFundingBdate() {
		return fundingBdate;
	}
	public void setFundingBdate(Date fundingBdate) {
		this.fundingBdate = fundingBdate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getFundingStatus() {
		return fundingStatus;
	}
	public void setFundingStatus(String fundingStatus) {
		this.fundingStatus = fundingStatus;
	}
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	

}
