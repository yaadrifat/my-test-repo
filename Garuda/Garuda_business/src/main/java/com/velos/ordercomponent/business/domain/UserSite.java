package com.velos.ordercomponent.business.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity
@Table(name = "er_usersite")
@SequenceGenerator(sequenceName="SEQ_ER_USERSITE",name="SEQ_ER_USERSITE",allocationSize=1)
public class UserSite extends Auditable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2775505647190054354L;
	

	   private Long pkUserSite;
	   private Long fkUser;
	   private Long fkSite;
	   private Long userRight;
	 
	   
	   
	   	public void setPkUserSite(Long pkUserSite) {
		this.pkUserSite = pkUserSite;
	   	}
	   	
	   	@Id
		@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_ER_USERSITE")
		@Column(name="PK_USERSITE")	   
	   	public Long getPkUserSite() {
		return pkUserSite;
	   	}
	   	
	   
		public void setFkUser(Long fkUser) {
			this.fkUser = fkUser;
		}
		@Column(name="FK_USER")
		public Long getFkUser() {
			return fkUser;
		}
		
		
		public void setFkSite(Long fkSite) {
			this.fkSite = fkSite;
		}
		@Column(name="FK_SITE")
		public Long getFkSite() {
			return fkSite;
		}
		
		
		public void setUserRight(Long userRight) {
			this.userRight = userRight;
		}
		@Column(name="USERSITE_RIGHT")
		public Long getUserRight() {
			return userRight;
		}
		
		
		


}
