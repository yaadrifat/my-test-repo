package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="CB_ENTITY_SAMPLES")
@SequenceGenerator(sequenceName="SEQ_CB_ENTITY_SAMPLES",name="SEQ_CB_ENTITY_SAMPLES",allocationSize=1)

public class EntitySamples extends Auditable{
	
	private Long pkEntitySamples;
	private Long entityId;
	private Long entityType;
	private Long fkSampleType;
	private Long fkSampleStatus;
	private Long sampleQnty;
	private Long sampleVol;
	private String ocIndict;
	private Date recDate;
	private Date  stDate;
	private Long sequence;
	private String delFlag;
	private Long fkSpecimen;
	private Long noSegAvail;
	private Long filtPap;
	private Long rbcPel;
	private Long extDnaAli;
	private Long nonViaAli;
	private Long noSerAli;
	private Long noPlasAli;
	private Long viaCelAli;
	private Long totCbuAli;
	private Long extDnaMat;
	private Long celMatAli;
	private Long serMatAli;
	private Long plasMatAli;
	private Long totMatAli;
	
	private Long cbuRepAltCon;
	private Long noMiscMat;
	private Long cbuOthRepConFin;
	

//Created By Gaurav Dated 2 Sept 2012
	
	private Long convFiltPap;
	private Long convRbcPel;
	private Long convExtDnaAli;
	private Long convNoSerAli;
	private Long convNoPlasAli;
	private Long convNonViaAli;
	private Long convViaCelAli; 
	private Long convNoSegAvail;
	private Long convSerMatAli;
	private Long convPlasMatAli;
	private Long convExtDnaMat;
	private Long convNoMiscMat;
	
	
	

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_ENTITY_SAMPLES")
	@Column(name="PK_ENTITY_SAMPLES")
	
	
	public Long getPkEntitySamples() {
		return pkEntitySamples;
	}
	public void setPkEntitySamples(Long pkEntitySamples) {
		this.pkEntitySamples = pkEntitySamples;
	}
	
	@Column(name="ENTITY_ID")
	public Long getEntityId() {
		return entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	
	@Column(name="ENTITY_TYPE")
	
	public Long getEntityType() {
		return entityType;
	}
	public void setEntityType(Long entityType) {
		this.entityType = entityType;
	}
	
	@Column(name="FK_SAMPLE_TYPE")
	
	public Long getFkSampleType() {
		return fkSampleType;
	}
	public void setFkSampleType(Long fkSampleType) {
		this.fkSampleType = fkSampleType;
	}
	
	@Column(name="FK_SAMPLE_STATUS")
	
	public Long getFkSampleStatus() {
		return fkSampleStatus;
	}
	public void setFkSampleStatus(Long fkSampleStatus) {
		this.fkSampleStatus = fkSampleStatus;
	}
	
	@Column(name="SAMPLE_QUANTITY")
	
	public Long getSampleQnty() {
		return sampleQnty;
	}
	public void setSampleQnty(Long sampleQnty) {
		this.sampleQnty = sampleQnty;
	}
	
	@Column(name="SAMPLE_VOLUME")
	
	public Long getSampleVol() {
		return sampleVol;
	}
	public void setSampleVol(Long sampleVol) {
		this.sampleVol = sampleVol;
	}
	
	@Column(name="QC_INDICATOR")
	
	public String getOcIndict() {
		return ocIndict;
	}
	public void setOcIndict(String ocIndict) {
		this.ocIndict = ocIndict;
	}
	
	@Column(name="RECEIVE_DATE")
	public Date getRecDate() {
		return recDate;
	}
	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}
	
	@Column(name="STORAGE_DATE")
	public Date getStDate() {
		return stDate;
	}
	public void setStDate(Date stDate) {
		this.stDate = stDate;
	}
	
	@Column(name="SEQUENCE")
	
	public Long getSequence() {
		return sequence;
	}
	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}
	
	
	
	@Column(name="DELETEDFLAG")
	public String getDelFlag() {
		return delFlag;
	}
	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}
	
	@Column(name="FK_SPECIMEN")
	
	public Long getFkSpecimen() {
		return fkSpecimen;
	}
	public void setFkSpecimen(Long fkSpecimen) {
		this.fkSpecimen = fkSpecimen;
	}
	
	@Column(name="NO_OF_SEG_AVAIL")
	
	public Long getNoSegAvail() {
		return noSegAvail;
	}
	public void setNoSegAvail(Long noSegAvail) {
		this.noSegAvail = noSegAvail;
	}
	
	@Column(name="FILT_PAP_AVAIL")
	public Long getFiltPap() {
		return filtPap;
	}
	public void setFiltPap(Long filtPap) {
		this.filtPap = filtPap;
	}
	
	@Column(name="RBC_PEL_AVAIL")
	public Long getRbcPel() {
		return rbcPel;
	}
	public void setRbcPel(Long rbcPel) {
		this.rbcPel = rbcPel;
	}
	
	
	@Column(name="NO_EXT_DNA_ALI")
	public Long getExtDnaAli() {
		return extDnaAli;
	}
	public void setExtDnaAli(Long extDnaAli) {
		this.extDnaAli = extDnaAli;
	}
	
	@Column(name="NO_NON_VIA_ALI")
	public Long getNonViaAli() {
		return nonViaAli;
	}
	public void setNonViaAli(Long nonViaAli) {
		this.nonViaAli = nonViaAli;
	}
	
	@Column(name="NO_SER_ALI")
	
	public Long getNoSerAli() {
		return noSerAli;
	}
	public void setNoSerAli(Long noSerAli) {
		this.noSerAli = noSerAli;
	}
	
	@Column(name="NO_PLAS_ALI")
	
	public Long getNoPlasAli() {
		return noPlasAli;
	}
	public void setNoPlasAli(Long noPlasAli) {
		this.noPlasAli = noPlasAli;
	}
	
	@Column(name="NO_VIA_CEL_ALI")
	
	public Long getViaCelAli() {
		return viaCelAli;
	}
	public void setViaCelAli(Long viaCelAli) {
		this.viaCelAli = viaCelAli;
	}
	
	@Column(name="TOT_CBU_ALI_AVA")
	
	public Long getTotCbuAli() {
		return totCbuAli;
	}
	public void setTotCbuAli(Long totCbuAli) {
		this.totCbuAli = totCbuAli;
	}
	
	@Column(name="NO_EXT_DNA_MAT_ALI")
	public Long getExtDnaMat() {
		return extDnaMat;
	}
	public void setExtDnaMat(Long extDnaMat) {
		this.extDnaMat = extDnaMat;
	}
	
	@Column(name="NO_CEL_MAT_ALI")
	
	public Long getCelMatAli() {
		return celMatAli;
	}
	public void setCelMatAli(Long celMatAli) {
		this.celMatAli = celMatAli;
	}
	
	@Column(name="NO_SER_MAT_ALI")
	
	public Long getSerMatAli() {
		return serMatAli;
	}
	public void setSerMatAli(Long serMatAli) {
		this.serMatAli = serMatAli;
	}
	
	@Column(name="NO_PLAS_MAT_ALI")
	
	public Long getPlasMatAli() {
		return plasMatAli;
	}
	public void setPlasMatAli(Long plasMatAli) {
		this.plasMatAli = plasMatAli;
	}
	
	@Column(name="TOT_MAT_ALI_AVA")
	
	public Long getTotMatAli() {
		return totMatAli;
	}
	public void setTotMatAli(Long totMatAli) {
		this.totMatAli = totMatAli;
	}
	
	@Column(name="SI_NO_MISC_MAT")
	
	public Long getNoMiscMat() {
		return noMiscMat;
	}
	public void setNoMiscMat(Long noMiscMat) {
		this.noMiscMat = noMiscMat;
	}
		
	@Column(name="CBU_OT_REP_CON_FIN")
	
	public Long getCbuOthRepConFin() {
		return cbuOthRepConFin;
	}
	public void setCbuOthRepConFin(Long cbuOthRepConFin) {				
		this.cbuOthRepConFin = cbuOthRepConFin;
	}

	@Column(name="CBU_NO_REP_ALT_CON")
	public Long getCbuRepAltCon() {
			return cbuRepAltCon;
	}
	public void setCbuRepAltCon(Long cbuRepAltCon) {
		this.cbuRepAltCon = cbuRepAltCon;
	}
	
	@Column(name="CONV_FIL_PAP_SAMP")
	public Long getConvFiltPap() {
		return convFiltPap;
	}
	public void setConvFiltPap(Long convFiltPap) {
		this.convFiltPap = convFiltPap;
	}
	
	@Column(name="CONV_RPC_PELLETS")
	public Long getConvRbcPel() {
		return convRbcPel;
	}
	public void setConvRbcPel(Long convRbcPel) {
		this.convRbcPel = convRbcPel;
	}
	
	@Column(name="CONV_EXTRACT_DNA")
	public Long getConvExtDnaAli() {
		return convExtDnaAli;
	}
	public void setConvExtDnaAli(Long convExtDnaAli) {
		this.convExtDnaAli = convExtDnaAli;
	}
	
	@Column(name="CONV_SERUM_SAMP")
	public Long getConvNoSerAli() {
		return convNoSerAli;
	}
	public void setConvNoSerAli(Long convNoSerAli) {
		this.convNoSerAli = convNoSerAli;
	}
	
	@Column(name="CONV_PLASMA_SAMP")
	public Long getConvNoPlasAli() {
		return convNoPlasAli;
	}
	public void setConvNoPlasAli(Long convNoPlasAli) {
		this.convNoPlasAli = convNoPlasAli;
	}
	
	@Column(name="CONV_NONVIABLE_CELL")
	public Long getConvNonViaAli() {
		return convNonViaAli;
	}
	public void setConvNonViaAli(Long convNonViaAli) {
		this.convNonViaAli = convNonViaAli;
	}
	
	@Column(name="CONV_VIABLE_CELLSAMP")
	
	public Long getConvViaCelAli() {
		return convViaCelAli;
	}
	public void setConvViaCelAli(Long convViaCelAli) {
		this.convViaCelAli = convViaCelAli;
	}

	@Column(name="CONV_NO_OF_SEG")
	
	public Long getConvNoSegAvail() {
		return convNoSegAvail;
	}
	public void setConvNoSegAvail(Long convNoSegAvail) {
		this.convNoSegAvail = convNoSegAvail;
	}
	
	
	@Column(name="CONV_MATR_SERSAMP")
	
	
	public Long getConvSerMatAli() {
		return convSerMatAli;
	}
	public void setConvSerMatAli(Long convSerMatAli) {
		this.convSerMatAli = convSerMatAli;
	}
	@Column(name="CONV_MATR_PLASAMP")
	
	public Long getConvPlasMatAli() {
		return convPlasMatAli;
	}
	public void setConvPlasMatAli(Long convPlasMatAli) {
		this.convPlasMatAli = convPlasMatAli;
	}
	@Column(name="CONV_MATR_EXTDNA_SAMP")
	
	public Long getConvExtDnaMat() {
		return convExtDnaMat;
	}
	public void setConvExtDnaMat(Long convExtDnaMat) {
		this.convExtDnaMat = convExtDnaMat;
	}
	@Column(name="CONV_MISCMATR_SAMP")
	
	public Long getConvNoMiscMat() {
		return convNoMiscMat;
	}
	public void setConvNoMiscMat(Long convNoMiscMat) {
		this.convNoMiscMat = convNoMiscMat;
	}
	
	
	

}
