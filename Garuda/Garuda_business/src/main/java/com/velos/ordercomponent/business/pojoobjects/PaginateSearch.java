package com.velos.ordercomponent.business.pojoobjects;

public class PaginateSearch {

	 	private Integer iShowRows;  // Number of records show on per page
	 	private Integer iTotalSearchRecords;  // Number of pages index shown
	 	private Integer iTotalRows;	//total overall rows 
	 	private Integer iTotalPages;	//total pages 
	 	private Integer iPageNo;	//current page number
	 	private Integer iStartResultNo;	//starting count page numbers
	 	private Integer iEndResultNo;	//ending count page numbers
	 	
	 	
		public Integer getiShowRows() {
			return iShowRows;
		}
		public void setiShowRows(Integer iShowRows) {
			this.iShowRows = iShowRows;
		}
		public Integer getiTotalSearchRecords() {
			return iTotalSearchRecords;
		}
		public void setiTotalSearchRecords(Integer iTotalSearchRecords) {
			this.iTotalSearchRecords = iTotalSearchRecords;
		}
		public Integer getiTotalRows() {
			return iTotalRows;
		}
		public void setiTotalRows(Integer iTotalRows) {
			this.iTotalRows = iTotalRows;
		}
		public Integer getiTotalPages() {
			return iTotalPages;
		}
		public void setiTotalPages(Integer iTotalPages) {
			this.iTotalPages = iTotalPages;
		}
		public Integer getiPageNo() {
			return iPageNo;
		}
		public void setiPageNo(Integer iPageNo) {
			this.iPageNo = iPageNo;
		}
		public Integer getiStartResultNo() {
			return iStartResultNo;
		}
		public void setiStartResultNo(Integer iStartResultNo) {
			this.iStartResultNo = iStartResultNo;
		}
		public Integer getiEndResultNo() {
			return iEndResultNo;
		}
		public void setiEndResultNo(Integer iEndResultNo) {
			this.iEndResultNo = iEndResultNo;
		}
	 	
}
