package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

public class CBBProcedurePojo extends AuditablePojo{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long pkProcId;
	private Double procNo;
	private String procName;
	private Date procStartDate;
	private Date procTermiDate;
	private String procVersion;
	private String procStartDateStr;
	private String procTermiDateStr;
	private Long fkSite;
	private String productCode;
	
	public String getProcStartDateStr() {
		return procStartDateStr;
	}
	public void setProcStartDateStr(String procStartDateStr) {
		this.procStartDateStr = procStartDateStr;
	}
	public String getProcTermiDateStr() {
		return procTermiDateStr;
	}
	public void setProcTermiDateStr(String procTermiDateStr) {
		this.procTermiDateStr = procTermiDateStr;
	}
	public String getProcVersion() {
		return procVersion;
	}
	public void setProcVersion(String procVersion) {
		this.procVersion = procVersion;
	}
	public Long getPkProcId() {
		return pkProcId;
	}
	public void setPkProcId(Long pkProcId) {
		this.pkProcId = pkProcId;
	}
	public Double getProcNo() {
		return procNo;
	}
	public void setProcNo(Double procNo) {
		this.procNo = procNo;
	}
	public String getProcName() {
		return procName;
	}
	public void setProcName(String procName) {
		this.procName = procName;
	}
	public Date getProcStartDate() {
		return procStartDate;
	}
	public void setProcStartDate(Date procStartDate) {
		this.procStartDate = procStartDate;
	}
	public Date getProcTermiDate() {
		return procTermiDate;
	}
	public void setProcTermiDate(Date procTermiDate) {
		this.procTermiDate = procTermiDate;
	}
	public Long getFkSite() {
		return fkSite;
	}
	public void setFkSite(Long fkSite) {
		this.fkSite = fkSite;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	
	
	
}
