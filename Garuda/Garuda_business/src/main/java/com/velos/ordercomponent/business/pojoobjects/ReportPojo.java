package com.velos.ordercomponent.business.pojoobjects;

public class ReportPojo {

	private Long pkReportId;
	private String reportName;
	
	public Long getPkReportId() {
		return pkReportId;
	}
	public void setPkReportId(Long pkReportId) {
		this.pkReportId = pkReportId;
	}
	public String getReportName() {
		return reportName;
	}
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
	
}
