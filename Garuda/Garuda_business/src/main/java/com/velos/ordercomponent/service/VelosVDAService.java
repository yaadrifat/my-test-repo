package com.velos.ordercomponent.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public interface VelosVDAService {
	public List<Object> getQueryBuilderViewData(String sql,PaginateSearch paginateSearch);
	public List getQueryBuilderViewData(String sql);
	public Map getQueryBuilderRecordDetails(String sql);
	public Map getQueryBuilderHlaData(String sql);
	public Map getQueryBuilderBestHlaData(String sql);
	public Map getQueryBuilderBestHlaFilterData(String sql,  Map<Long, String> cdlst);
	public List getQueryBuilderCordHlaData(String sql);
	public Map getQueryBuilderHlaFilterData(String sql,  Map<Long, String> cdlst);
	public List getQueryBuilderListObject(String sql);
	public List getQueryBuilderCordIdObject (String sql);
	public Map getQueryBuilderMapObject (String sql);
	public Map getLicEligMapObject(String sql, Map<Long, Object []> cord, Map<Long, ArrayList<Long>> reason);
	public Map getQueryBuilderMapLstObject(String sql);
}
