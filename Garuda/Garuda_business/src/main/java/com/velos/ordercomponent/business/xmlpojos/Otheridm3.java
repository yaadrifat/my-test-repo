package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


public class Otheridm3 {
	private String other_idm_3_react_ind;
	private String other_idm_3_react_test;
	private XMLGregorianCalendar other_idm_3_react_date;
	@XmlElement(name="other_idm_3_react_ind")	
	public String getOther_idm_3_react_ind() {
		return other_idm_3_react_ind;
	}
	public void setOther_idm_3_react_ind(String otherIdm_3ReactInd) {
		other_idm_3_react_ind = otherIdm_3ReactInd;
	}
	@XmlElement(name="other_idm_3_react_test")	
	public String getOther_idm_3_react_test() {
		return other_idm_3_react_test;
	}
	public void setOther_idm_3_react_test(String otherIdm_3ReactTest) {
		other_idm_3_react_test = otherIdm_3ReactTest;
	}
	@XmlElement(name="other_idm_3_react_date")
	@XmlJavaTypeAdapter(type=XMLGregorianCalendar.class,value=XMLGeorgarianCalanderAdapter.class)
	public XMLGregorianCalendar getOther_idm_3_react_date() {
		return other_idm_3_react_date;
	}
	public void setOther_idm_3_react_date(XMLGregorianCalendar otherIdm_3ReactDate) {
		other_idm_3_react_date = otherIdm_3ReactDate;
	}

	
}

