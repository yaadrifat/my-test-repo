package com.velos.ordercomponent.business.domain;

import java.io.Serializable;
import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author Mohiuddin Ali Ahmed
 *
 */
@Entity
@Table(name="CB_MAIL_DOCUMENTS")
@SequenceGenerator(sequenceName="SEQ_CB_MAIL_DOCUMENTS",name="SEQ_CB_MAIL_DOCUMENTS",allocationSize=1)
public class EmailAttachment implements Serializable {

	private Long emailDocumentId; 
	private Long emailDoc; 
	private String emailDocType; 
	private String emailDocName; 
	private Long entityId; 
	private String entityType;
	private Blob emailDocFile;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_MAIL_DOCUMENTS")
	@Column(name="PK_MAIL_DOCUMENTS")
	public Long getEmailDocumentId() {
		return emailDocumentId;
	}
	public void setEmailDocumentId(Long emailDocumentId) {
		this.emailDocumentId = emailDocumentId;
	}	
	@Column(name="DOCUMENT_FILE")
	public Long getEmailDoc() {
		return emailDoc;
	}
	public void setEmailDoc(Long emailDoc) {
		this.emailDoc = emailDoc;
	}
	@Column(name="DOCUMENT_CONTENTTYPE")
	public String getEmailDocType() {
		return emailDocType;
	}
	public void setEmailDocType(String emailDocType) {
		this.emailDocType = emailDocType;
	}
	@Column(name="DOCUMENT_FILENAME")
	public String getEmailDocName() {
		return emailDocName;
	}
	public void setEmailDocName(String emailDocName) {
		this.emailDocName = emailDocName;
	}
	@Column(name="ENTITY_ID")
	public Long getEntityId() {
		return entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	@Column(name="ENTITY_TYPE")
	public String getEntityType() {
		return entityType;
	}
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}
	@Column(name="DOCUMENT_FILES")
	public Blob getEmailDocFile() {
		return emailDocFile;
	}
	public void setEmailDocFile(Blob emailDocFile) {
		this.emailDocFile = emailDocFile;
	} 
	
	
}
