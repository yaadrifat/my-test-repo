package com.velos.ordercomponent.business.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="ER_USRSITE_GRP")
@SequenceGenerator(sequenceName="SEQ_ER_USRSITE_GRP",name="SEQ_ER_USRSITE_GRP",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class UserSiteGroup  extends Auditable{
	
	private Long pkUserSiteGroup;
	private Long fkGrpId;
	private Long fkUserSite;
	private Long fkUser;
	private Boolean deletedFlag;
	

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_ER_USRSITE_GRP")
	@Column(name="PK_USR_SITE_GRP")
	public Long getPkUserSiteGroup() {
		return pkUserSiteGroup;
	}
	public void setPkUserSiteGroup(Long pkUserSiteGroup) {
		this.pkUserSiteGroup = pkUserSiteGroup;
	}
	
	@Column(name="FK_GRP_ID")
	public Long getFkGrpId() {
		return fkGrpId;
	}
	public void setFkGrpId(Long fkGrpId) {
		this.fkGrpId = fkGrpId;
	}
	
	@Column(name="FK_USER_SITE")
	public Long getFkUserSite() {
		return fkUserSite;
	}
	public void setFkUserSite(Long fkUserSite) {
		this.fkUserSite = fkUserSite;
	}
	
	@Column(name="FK_USER")
	public Long getFkUser() {
		return fkUser;
	}
	public void setFkUser(Long fkUser) {
		this.fkUser = fkUser;
	}
	
	@Column(name="DELETEDFLAG")
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	
	
}
