package com.velos.ordercomponent.business.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ER_LABTESTGRP")
@SequenceGenerator(sequenceName="SEQ_ER_LABTESTGRP",name="SEQ_ER_LABTESTGRP",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)

public class LabTestGrp {

	private Long pklabtestgrp;
	private Long fktestid;
	private Long fklabgrp;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_ER_LABTESTGRP")
	@Column(name="PK_LABTESTGRP")
	public Long getPklabtestgrp() {
		return pklabtestgrp;
	}
	public void setPklabtestgrp(Long pklabtestgrp) {
		this.pklabtestgrp = pklabtestgrp;
	}
	@Column(name="FK_TESTID")
	public Long getFktestid() {
		return fktestid;
	}
	public void setFktestid(Long fktestid) {
		this.fktestid = fktestid;
	}
	@Column(name="FK_LABGROUP")
	public Long getFklabgrp() {
		return fklabgrp;
	}
	public void setFklabgrp(Long fklabgrp) {
		this.fklabgrp = fklabgrp;
	}
}
