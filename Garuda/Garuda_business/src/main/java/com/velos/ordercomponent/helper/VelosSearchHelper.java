/**
 * 
 */
package com.velos.ordercomponent.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.ordercomponent.business.pojoobjects.SearchPojo;

/**
 * @author akajeera
 *
 */
public class VelosSearchHelper {
	public static final Log log=LogFactory.getLog(VelosSearchHelper.class);
	public static SearchPojo getSearchlst(SearchPojo searchPojo){
		//log.debug("VelosSearch Helper : ");
		List<Object> resultlst = new ArrayList<Object>();
		
		String objectName = "";
//		log.debug("------------------>"+searchPojo.getSearchType());
		
		objectName = searchPojo.getSearchType();
		//Construct Criteria
		if(searchPojo!=null && searchPojo.getParamMap()!=null){
			Map paramMap =  searchPojo.getParamMap();
			Set keyset = paramMap.keySet();
		}
		//log.debug("Object Name : " + objectName + " criteria = > " + searchPojo.getCriteria());
		resultlst = velosHelper.getObjectList(objectName, searchPojo.getCriteria());
		searchPojo.setResultlst(resultlst);
		
		// codelst map get from codelst list
		// search pojo
//	searchPojo.setCodelstParam(CodelstHelper.getCodelistValues(searchPojo.getCodelstParam()));
		
		return searchPojo;
	}
	
}
