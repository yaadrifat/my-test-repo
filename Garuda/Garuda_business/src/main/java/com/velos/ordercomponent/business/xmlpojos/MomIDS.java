package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;

public class MomIDS {

	private String nmdp_matrnl_cbuid;
	private String EXMCL;
	
	@XmlElement(name="nmdp_matrnl_cbuid",required=true)
	public String getNmdp_matrnl_cbuid() {
		return nmdp_matrnl_cbuid;
	}
	public void setNmdp_matrnl_cbuid(String nmdpMatrnlCbuid) {
		nmdp_matrnl_cbuid = nmdpMatrnlCbuid;
	}
	
	@XmlElement(name="EXMCL")
	public String getEXMCL() {
		return EXMCL;
	}
	public void setEXMCL(String eXMCL) {
		EXMCL = eXMCL;
	}
	
	
	
}
