package com.velos.ordercomponent.business.domain;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
@Entity
@Table(name="PERSON",schema="EPAT")
@SequenceGenerator(sequenceName="ERES.SEQ_ER_PER",name="SEQ_PERSON",allocationSize=1)
public class Person extends Auditable {

	  /**
     * 
     */
    private static final long serialVersionUID = 3257568425328194097L;

    private Long personId;
    private String personCode;
    private Long personAccount;
    private Long personLocation;
    private String personAltId;
    private String personLname;
    private String personFname;
    private String personMname;
    private String personSuffix;
    private String personPrefix;
    private String personDegree;
    private String personMotherName;
    private Date personDob;
    private Long personGender;
    private String personAlias;
    private Long personRace;
    private Long personEthnicity;
    private Long personPrimaryLang;
    private Long personMarital;
    private Long personRelegion;
    private String personSSN;
    private String personDrivLic;
    private Long personMotherId;
    private Long personEthGroup;
    private String personBirthPlace;
    private String personMultiBirth;
    private Long personCitizen;
    private String personVetMilStatus;
    private Long personNationality;
    private Date personDeathDate  ;
    private Long personEmployment;
    private Long personEducation;
    private String personNotes;
    private Long personStatus;
    private Long personBloodGr;
    private String personAddress1;
    private String personAddress2;
    private String personCity;
    private String personState;
    private String personZip;
    private String personCountry;
    private String personHphone;
    private String personBphone;
    private String personEmail;
    private String personCounty;
    private Date personRegDate ;
    private Long personRegBy;
    private Long timeZoneId;  
    private String phyOther;
    private String orgOther;
    private String personSplAccess;
    private String personAddRace;
    private String personAddEthnicity;
    private Long patDthCause;
    private String dthCauseOther;
    private Long personNtype;
    
    /**
     * the Patient Facility Id
     */
    private String patientFacilityId;
   

    // GETTER SETTER METHODS

    /**
     * 
     * 
     * @return PK Id for a person
     * 
     * 
     */

    @Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_PERSON")
	@Column(name="PK_PERSON")
	public Long getPersonId() {

        return this.personId;

    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

   
    /**
     * 
     * 
     * 
     * @return personPId
     * 
     * 
     */
    @Column(name = "PERSON_CODE")
    public String getPersonCode()

    {

        return this.personCode;

    }

    /**
     * 
     * 
     * 
     * @param personPId
     * 
     * 
     */

    public void setPersonCode(String personCode)

    {

        this.personCode = personCode;

    }

   

    @Column(name = "PERSON_AKA")
    public String getPersonAlias() {
        return this.personAlias;
    }

    public void setPersonAlias(String personAlias) {
        this.personAlias = personAlias;
    }

    @Column(name = "PERSON_ALTID")
    public String getPersonAltId() {
        return this.personAltId;
    }

    public void setPersonAltId(String personAltId) {
        this.personAltId = personAltId;
    }

   

    @Column(name = "PERSON_BIRTH_PLACE")
    public String getPersonBirthPlace() {
        return this.personBirthPlace;
    }

    public void setPersonBirthPlace(String personBirthPlace) {
        this.personBirthPlace = personBirthPlace;
    }

    @Column(name = "PERSON_DEGREE")
    public String getPersonDegree() {
        return this.personDegree;
    }

    public void setPersonDegree(String personDegree) {
        this.personDegree = personDegree;
    }

    @Column(name = "PERSON_DRIV_LIC")
    public String getPersonDrivLic() {
        return this.personDrivLic;
    }

    public void setPersonDrivLic(String personDrivLic) {
        this.personDrivLic = personDrivLic;
    }

    @Column(name = "PERSON_FNAME")
    public String getPersonFname() {
        return this.personFname;
    }

    public void setPersonFname(String personFname) {
        this.personFname = personFname;
    }

    @Column(name = "PERSON_LNAME")
    public String getPersonLname() {
        return this.personLname;
    }

    public void setPersonLname(String personLname) {
        this.personLname = personLname;
    }

    @Column(name = "PERSON_MNAME")
    public String getPersonMname() {
        return this.personMname;
    }

    public void setPersonMname(String personMname) {
        this.personMname = personMname;
    }

    @Column(name = "PERSON_MOTHER_NAME")
    public String getPersonMotherName() {
        return this.personMotherName;
    }

    public void setPersonMotherName(String personMotherName) {
        this.personMotherName = personMotherName;
    }

    @Column(name = "PERSON_MULTI_BIRTH")
    public String getPersonMultiBirth() {
        return this.personMultiBirth;
    }

    public void setPersonMultiBirth(String personMultiBirth) {
        this.personMultiBirth = personMultiBirth;
    }

    @Column(name = "PERSON_PREFIX")
    public String getPersonPrefix() {
        return this.personPrefix;
    }

    public void setPersonPrefix(String personPrefix) {
        this.personPrefix = personPrefix;
    }

    
    @Column(name = "PERSON_SSN")
    public String getPersonSSN() {
        return this.personSSN;
    }

    public void setPersonSSN(String personSSN) {
        this.personSSN = personSSN;
    }

    @Column(name = "PERSON_SUFFIX")
    public String getPersonSuffix() {
        return this.personSuffix;
    }

    public void setPersonSuffix(String personSuffix) {
        this.personSuffix = personSuffix;
    }

    @Column(name = "PERSON_MILVET")
    public String getPersonVetMilStatus() {
        return this.personVetMilStatus;
    }

    public void setPersonVetMilStatus(String personVetMilStatus) {
        this.personVetMilStatus = personVetMilStatus;
    }

    @Column(name = "PERSON_ADDRESS1")
    public String getPersonAddress1() {
        return this.personAddress1;
    }

    public void setPersonAddress1(String personAddress1) {
        this.personAddress1 = personAddress1;
    }

    @Column(name = "PERSON_ADDRESS2")
    public String getPersonAddress2() {
        return this.personAddress2;
    }

    public void setPersonAddress2(String personAddress2) {
        this.personAddress2 = personAddress2;
    }

    @Column(name = "PERSON_CITY")
    public String getPersonCity() {
        return this.personCity;
    }

    public void setPersonCity(String personCity) {
        this.personCity = personCity;
    }

    @Column(name = "PERSON_COUNTRY")
    public String getPersonCountry() {
        return this.personCountry;
    }

    public void setPersonCountry(String personCountry) {
        this.personCountry = personCountry;
    }

    @Column(name = "PERSON_COUNTY")
    public String getPersonCounty() {
        return this.personCounty;
    }

    public void setPersonCounty(String personCounty) {
        this.personCounty = personCounty;
    }

    @Column(name = "PERSON_EMAIL")
    public String getPersonEmail() {
        return this.personEmail;
    }

    public void setPersonEmail(String personEmail) {
        this.personEmail = personEmail;
    }

    @Column(name = "PERSON_HPHONE")
    public String getPersonHphone() {
        return this.personHphone;
    }

    public void setPersonHphone(String personHphone) {
        this.personHphone = personHphone;
    }

    @Column(name = "PERSON_STATE")
    public String getPersonState() {
        return this.personState;
    }

    public void setPersonState(String personState) {
        this.personState = personState;
    }

    @Column(name = "PERSON_ZIP")
    public String getPersonZip() {
        return this.personZip;
    }

    public void setPersonZip(String personZip) {
        this.personZip = personZip;
    }

    @Column(name = "PERSON_BPHONE")
    public String getPersonBphone() {
        return this.personBphone;
    }

    public void setPersonBphone(String personBphone) {
        this.personBphone = personBphone;
    }  
  

    @Column(name = "PERSON_PHYOTHER")
    public String getPhyOther() {
        return this.phyOther;
    }
    public void setPhyOther(String phyOther) {
        this.phyOther = phyOther;
    }
    

    public void setOrgOther(String orgOther) {
        this.orgOther = orgOther;
    }

    @Column(name = "PERSON_ORGOTHER")
    public String getOrgOther() {
        return this.orgOther;
    }

    public void setPersonSplAccess(String personSplAccess) {
        this.personSplAccess = personSplAccess;
    }

    @Column(name = "PERSON_SPLACCESS")
    public String getPersonSplAccess() {
        return this.personSplAccess;
    }

    @Column(name = "PERSON_ADD_RACE")
    public String getPersonAddRace() {
        return this.personAddRace;
    }

    public void setPersonAddRace(String personAddRace) {
        this.personAddRace = personAddRace;
    }

    @Column(name = "PERSON_ADD_ETHNICITY")
    public String getPersonAddEthnicity() {
        return this.personAddEthnicity;
    }

    public void setPersonAddEthnicity(String personAddEthnicity) {
        this.personAddEthnicity = personAddEthnicity;
    }

    @Column(name = "CAUSE_OF_DEATH_OTHER")
    public String getDthCauseOther() {
        return this.dthCauseOther;
    }

    public void setDthCauseOther(String dthCauseOther) {
        this.dthCauseOther = dthCauseOther;

    }
    
     /**
	 * Returns the value of patientFacilityId.
	 */
	 @Column(name = "PAT_FACILITYID")
	public String getPatientFacilityId()
	{
		return patientFacilityId;
	}

	/**
	 * Sets the value of patientFacilityId.
	 * @param patientFacilityId The value to assign patientFacilityId.
	 */
	public void setPatientFacilityId(String patientFacilityId)
	{
		this.patientFacilityId = patientFacilityId;
	}

	@Column(name = "FK_ACCOUNT")
	public Long getPersonAccount() {
		return personAccount;
	}

	public void setPersonAccount(Long personAccount) {
		this.personAccount = personAccount;
	}

	@Column(name = "FK_SITE")
	public Long getPersonLocation() {
		return personLocation;
	}

	public void setPersonLocation(Long personLocation) {
		this.personLocation = personLocation;
	}

	@Column(name = "PERSON_DOB")
	public Date getPersonDob() {
		return personDob;
	}

	public void setPersonDob(Date personDob) {
		this.personDob = personDob;
	}

	@Column(name = "FK_CODELST_GENDER")
	public Long getPersonGender() {
		return personGender;
	}

	public void setPersonGender(Long personGender) {
		this.personGender = personGender;
	}

	@Column(name = "FK_CODELST_RACE")
	public Long getPersonRace() {
		return personRace;
	}

	public void setPersonRace(Long personRace) {
		this.personRace = personRace;
	}

	@Column(name = "FK_CODELST_ETHNICITY")
	public Long getPersonEthnicity() {
		return personEthnicity;
	}

	public void setPersonEthnicity(Long personEthnicity) {
		this.personEthnicity = personEthnicity;
	}

	@Column(name = "FK_CODELST_PRILANG")
	public Long getPersonPrimaryLang() {
		return personPrimaryLang;
	}

	public void setPersonPrimaryLang(Long personPrimaryLang) {
		this.personPrimaryLang = personPrimaryLang;
	}

	@Column(name = "FK_CODELST_MARITAL")
	public Long getPersonMarital() {
		return personMarital;
	}

	public void setPersonMarital(Long personMarital) {
		this.personMarital = personMarital;
	}

	@Column(name = "FK_CODELST_RELIGION")
	public Long getPersonRelegion() {
		return personRelegion;
	}

	public void setPersonRelegion(Long personRelegion) {
		this.personRelegion = personRelegion;
	}

	@Column(name = "FK_PERSON_MOTHER")
	public Long getPersonMotherId() {
		return personMotherId;
	}

	public void setPersonMotherId(Long personMotherId) {
		this.personMotherId = personMotherId;
	}

	@Column(name = "PERSON_ETHGRP")
	public Long getPersonEthGroup() {
		return personEthGroup;
	}

	public void setPersonEthGroup(Long personEthGroup) {
		this.personEthGroup = personEthGroup;
	}

	@Column(name = "FK_CODELST_CITIZEN")
	public Long getPersonCitizen() {
		return personCitizen;
	}

	public void setPersonCitizen(Long personCitizen) {
		this.personCitizen = personCitizen;
	}

	@Column(name = "FK_CODELST_NATIONALITY")
	public Long getPersonNationality() {
		return personNationality;
	}

	public void setPersonNationality(Long personNationality) {
		this.personNationality = personNationality;
	}

	@Column(name = "PERSON_DEATHDT")
	public Date getPersonDeathDate() {
		return personDeathDate;
	}

	public void setPersonDeathDate(Date personDeathDate) {
		this.personDeathDate = personDeathDate;
	}

	@Column(name = "FK_CODELST_EMPLOYMENT")
	public Long getPersonEmployment() {
		return personEmployment;
	}

	public void setPersonEmployment(Long personEmployment) {
		this.personEmployment = personEmployment;
	}

	@Column(name = "FK_CODELST_EDU")
	public Long getPersonEducation() {
		return personEducation;
	}

	public void setPersonEducation(Long personEducation) {
		this.personEducation = personEducation;
	}

	@Column(name = "PERSON_NOTES")
	public String getPersonNotes() {
		return personNotes;
	}

	public void setPersonNotes(String personNotes) {
		this.personNotes = personNotes;
	}

	@Column(name = "FK_CODELST_PSTAT")
	public Long getPersonStatus() {
		return personStatus;
	}

	public void setPersonStatus(Long personStatus) {
		this.personStatus = personStatus;
	}

	@Column(name = "FK_CODELST_BLOODGRP")
	public Long getPersonBloodGr() {
		return personBloodGr;
	}

	public void setPersonBloodGr(Long personBloodGr) {
		this.personBloodGr = personBloodGr;
	}

	@Column(name = "PERSON_REGDATE")
	public Date getPersonRegDate() {
		return personRegDate;
	}

	public void setPersonRegDate(Date personRegDate) {
		this.personRegDate = personRegDate;
	}

	@Column(name = "PERSON_REGBY")
	public Long getPersonRegBy() {
		return personRegBy;
	}

	public void setPersonRegBy(Long personRegBy) {
		this.personRegBy = personRegBy;
	}

	@Column(name = "FK_TIMEZONE")
	public Long getTimeZoneId() {
		return timeZoneId;
	}

	public void setTimeZoneId(Long timeZoneId) {
		this.timeZoneId = timeZoneId;
	}	

	@Column(name = "FK_CODELST_PAT_DTH_CAUSE")
	public Long getPatDthCause() {
		return patDthCause;
	}

	public void setPatDthCause(Long patDthCause) {
		this.patDthCause = patDthCause;
	}
	@Column(name = "FK_CODLST_NTYPE")
	public Long getPersonNtype() {
		return personNtype;
	}

	public void setPersonNtype(Long personNtype) {
		this.personNtype = personNtype;
	}

	

}