package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.datatype.XMLGregorianCalendar;

public class CD3_Post_CryoPreservation {

	private String CD3_test;
	private String CD3_cnt;
	private XMLGregorianCalendar CD3_test_date;
	private String CD3_test_reason;
	
	@XmlElement(name="CD3_test")	
	public String getCD3_test() {
		return CD3_test;
	}
	public void setCD3_test(String cD3Test) {
		CD3_test = cD3Test;
	}
	@XmlElement(name="CD3_cnt")	
	public String getCD3_cnt() {
		return CD3_cnt;
	}
	public void setCD3_cnt(String cD3Cnt) {
		CD3_cnt = cD3Cnt;
	}
	@XmlElement(name="CD3_test_date")	
	public XMLGregorianCalendar getCD3_test_date() {
		return CD3_test_date;
	}
	public void setCD3_test_date(XMLGregorianCalendar cD3TestDate) {
		CD3_test_date = cD3TestDate;
	}
	@XmlElement(name="CD3_test_reason")	
	public String getCD3_test_reason() {
		return CD3_test_reason;
	}
	public void setCD3_test_reason(String cD3TestReason) {
		CD3_test_reason = cD3TestReason;
	}
	
	
}
