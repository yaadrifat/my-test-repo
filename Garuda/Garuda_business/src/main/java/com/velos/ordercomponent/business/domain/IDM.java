package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author 
 *
 */
@Entity
@Table(name = "CB_IDM")
@SequenceGenerator(sequenceName = "SEQ_CB_IDM", name = "SEQ_CB_IDM",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true, dynamicUpdate = true)
public class IDM extends Auditable{
	private Long pkidm;
	private Long fktestgrp;
	private Long fkspecimen;
	private Long idmfilledby;
	private Date idmfilleddate;
	private Date bloodcdate;
	private Long cmsapprovedlab;
	private Long fdalicensedlab;
	private String deletedflag;
	private Long fktestsource;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CB_IDM")
	@Column(name = "PK_IDM")
	public Long getPkidm() {
		return pkidm;
	}
	public void setPkidm(Long pkidm) {
		this.pkidm = pkidm;
	}
	@Column(name="FK_TESTGROUP")
	public Long getFktestgrp() {
		return fktestgrp;
	}
	public void setFktestgrp(Long fktestgrp) {
		this.fktestgrp = fktestgrp;
	}
	@Column(name="FK_SPECIMEN")
	public Long getFkspecimen() {
		return fkspecimen;
	}
	public void setFkspecimen(Long fkspecimen) {
		this.fkspecimen = fkspecimen;
	}
	@Column(name="IDM_FORM_FILLED_BY")
	public Long getIdmfilledby() {
		return idmfilledby;
	}
	public void setIdmfilledby(Long idmfilledby) {
		this.idmfilledby = idmfilledby;
	}
	@Column(name="IDM_FORM_FILLED_DATE")
	public Date getIdmfilleddate() {
		return idmfilleddate;
	}
	public void setIdmfilleddate(Date idmfilleddate) {
		this.idmfilleddate = idmfilleddate;
	}
	@Column(name="BLOOD_COLLECTION_DATE")
	public Date getBloodcdate() {
		return bloodcdate;
	}
	public void setBloodcdate(Date bloodcdate) {
		this.bloodcdate = bloodcdate;
	}
	@Column(name="CMS_APPROVED_LAB")
	public Long getCmsapprovedlab() {
		return cmsapprovedlab;
	}
	public void setCmsapprovedlab(Long cmsapprovedlab) {
		this.cmsapprovedlab = cmsapprovedlab;
	}
	@Column(name="FDA_LICENSED_LAB")
	public Long getFdalicensedlab() {
		return fdalicensedlab;
	}
	public void setFdalicensedlab(Long fdalicensedlab) {
		this.fdalicensedlab = fdalicensedlab;
	}
	@Column(name="DELETEDFLAG")
	public String getDeletedflag() {
		return deletedflag;
	}
	public void setDeletedflag(String deletedflag) {
		this.deletedflag = deletedflag;
	}
	@Column(name="FK_TEST_SOURCE")
	public Long getFktestsource() {
		return fktestsource;
	}
	public void setFktestsource(Long fktestsource) {
		this.fktestsource = fktestsource;
	}
}