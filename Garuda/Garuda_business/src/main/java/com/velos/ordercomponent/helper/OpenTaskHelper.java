package com.velos.ordercomponent.helper;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;

public class OpenTaskHelper {
	public static final Log log = LogFactory.getLog(OpenTaskHelper.class);
	public static List<Object> queryForCbuList(CdrCbuPojo cdrCbuPojo) {
		String criteria = "";

		if (cdrCbuPojo != null) {

			if (cdrCbuPojo.getCdrCbuId() != null
					&& !cdrCbuPojo.getCdrCbuId().equals("")) {
				criteria = "select cordinfo.cordID,cordinfo.cdrCbuId,extinfo.pkCordExtInfo,cl.pkCodeId from CdrCbu cordinfo, CBUUnitReport extinfo, CodeList cl where "
						+ "cordinfo.cordID = extinfo.fkCordCdrCbuId and extinfo.cordCompletedStatus = cl.pkCodeId and upper(cl.subType)='PENDING' and lower(cordinfo.cdrCbuId) like lower('%"
						+ cdrCbuPojo.getCdrCbuId() + "%')";
			} else
				criteria = "select cordinfo.cordID,cordinfo.cdrCbuId,extinfo.pkCordExtInfo,cl.pkCodeId from CdrCbu cordinfo, CBUUnitReport extinfo, CodeList cl where "
						+ "cordinfo.cordID = extinfo.fkCordCdrCbuId and extinfo.cordCompletedStatus = cl.pkCodeId and upper(cl.subType)='PENDING'";
		}

		return velosHelper.getObjectList(criteria);
	}

	public static List getUserId(String loginId) {
		
		
		String query = "select userId from UserDomain where loginId in("
				+ loginId + ")";
		return velosHelper.getObjectList(query);
	}

	public static List getUser() {
		
		String query = "select u.firstName,u.loginId,u.email from UserDomain u , CodeList cl where u.userType = cl.pkCodeId and cl.subType='DATA_ENTRY' ";
		return velosHelper.getObjectList(query);
	}
	public static List<Object> getTask(String loginId, String listid) {

		//log.debug("OpenTaskHelper.getTask()");
		String query = "select gu.userId,gu.loginId,gu.firstName,gu.lastName,gu.middleName,gu.email,gtl.pkTaskId,gcl.description,cdrcbu.cdrCbuId "+
        "from UserDomain gu, TaskList gtl, CodeList gcl, CBUUnitReport gci, CdrCbu cdrcbu "+
        "where gu.userId=gtl.taskAssignTo and gtl.taskType=gcl.pkCodeId and cdrcbu.cordID = gci.fkCordCdrCbuId and gtl.domainId=gci.pkCordExtInfo and gu.loginId in ("+loginId+") and gtl.pkTaskId in ("+listid+")";
		
		//log.debug("query :: "+query);
		
		return velosHelper.getObjectList(query);
	}



}


