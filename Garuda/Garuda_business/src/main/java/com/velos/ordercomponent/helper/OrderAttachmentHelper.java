package com.velos.ordercomponent.helper;



import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Types;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.classic.Session;

import com.sun.corba.se.impl.javax.rmi.CORBA.Util;
import com.velos.ordercomponent.business.domain.CBUStatus;
import com.velos.ordercomponent.business.domain.CBUploadInfo;
import com.velos.ordercomponent.business.domain.CdrCbu;
import com.velos.ordercomponent.business.domain.FilterData;
import com.velos.ordercomponent.business.domain.HLA;
import com.velos.ordercomponent.business.domain.LabMaint;
import com.velos.ordercomponent.business.domain.OrderAttachments;
import com.velos.ordercomponent.business.domain.Orders;
import com.velos.ordercomponent.business.domain.PackingSlip;
import com.velos.ordercomponent.business.domain.Shipment;
import com.velos.ordercomponent.business.pojoobjects.CBUploadInfoPojo;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.FilterDataPojo;
import com.velos.ordercomponent.business.pojoobjects.HLAPojo;
import com.velos.ordercomponent.business.pojoobjects.LabMaintPojo;
import com.velos.ordercomponent.business.pojoobjects.OrderAttachmentPojo;
import com.velos.ordercomponent.business.pojoobjects.OrderPojo;
import com.velos.ordercomponent.business.pojoobjects.PackingSlipPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.pojoobjects.ShipmentPojo;
import com.velos.ordercomponent.business.pojoobjects.TaskListPojo;
import com.velos.ordercomponent.business.util.HibernateUtil;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.Utilities;
import com.velos.ordercomponent.business.util.VelosMidConstants;
import com.velos.ordercomponent.business.util.VelosUtil;

public class OrderAttachmentHelper {
	
	
	private static Logger log = Logger.getLogger(OrderAttachmentHelper.class);
	public static OrderAttachmentPojo saveOrderAttachment(OrderAttachmentPojo orderAttachmentPojo){
		//log.debug(" Helper saveOrderAttachment called........");
		
		OrderAttachments orderAttachments = new OrderAttachments();
		
		
		orderAttachments = (OrderAttachments)ObjectTransfer.transferObjects(orderAttachmentPojo, orderAttachments);
		
		Object object = new VelosUtil().setAuditableInfo(orderAttachments);
		addOrderAttachments((OrderAttachments)object);
		
		
		return orderAttachmentPojo;
	}
	
	public static OrderAttachments addOrderAttachments(OrderAttachments orderAttachments) {
		//log.debug(" helper without session");
		return addOrderAttachments(orderAttachments,null);
	}
	
	public static OrderAttachments addOrderAttachments(OrderAttachments orderAttachments,Session session) {
		//log.debug(" helper "+cbuUnitReport.getCordCbuPostQues1());
		//log.debug(" helper "+cbuUnitReport.getCordCbuPostQues2());
		boolean sessionFlag = false;
		try{
		// check session 
			//log.debug("session " + session);
		if(session== null){
			// create new session
		  session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}
	//	//log.debug("Helper : " + cbuUnitReportTemp.getPkCordExtInfo());
		session.save(orderAttachments);
		if (sessionFlag) {
			session.getTransaction().commit();
		}
		} catch (Exception e) {
			//log.debug(" Exception in addOrderAttachments " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			//log.debug(" session in finally " + session + " /" +sessionFlag );
			if (session.isOpen()) {
				session.close();
			}
		}
		return orderAttachments;
	}
	
public static String getQueryForOpenOrderList(CdrCbuPojo cdrCbuPojo) throws Exception{
		
		String criteria = "";
		
		if (cdrCbuPojo != null) {
			//log.debug("cdrcbuId:::::::::::;;"+cdrCbuPojo.getCdrCbuId());

			if (cdrCbuPojo.getCordID() != null) {
				
				criteria="select ordhdr.orderNumber,ordhdr.orderRemarks,ordhdr.orderOpendate,codelst.description,orders.pk_OrderId,cordinfo.cordID from orderHeader ordhdr,Orders orders,CdrCbu cordinfo," +
						"CodeList codelst where orders.fkOrderHeader=ordhdr.pkOrderHeader and cordinfo.cordID = ordhdr.orderEntityId and orders.orderStatus = codelst.pkCodeId and (lower(codelst.subType) <>'"+VelosMidConstants.CLOSED+"' and " +
						"upper(codelst.subType) <>'CANCELLED') and  cordinfo.cordID ="+ cdrCbuPojo.getCordID();
				
				
			}
			else {
				
				criteria="select ordhdr.orderNumber,ordhdr.orderRemarks,ordhdr.orderOpendate,codelst.description,orders.pk_OrderId,cordinfo.cordID from orderHeader ordhdr,Orders orders,CdrCbu cordinfo," +
				"CodeList codelst where orders.fkOrderHeader=ordhdr.pkOrderHeader and cordinfo.cordID = ordhdr.orderEntityId and orders.orderStatus = codelst.pkCodeId and (lower(codelst.subType) <>'"+VelosMidConstants.CLOSED+"' and " +
				"upper(codelst.subType) <>'CANCELLED')";
				
		}}
		
		return criteria;
	}	
	
	public static String queryForCategoryList() throws Exception {
		
		String criteria = "";
		/*criteria = "Select lab from LabCategory lab,CodeList codelst "+
			" where lab.categoryType=codelst.pkCodeId and upper(codelst.subType)='LAB'";
		*/
		return criteria;
	}	
	
	public static String populateDropdownValues(Long selVal) throws Exception {
		String criteria = "";
		
		criteria = "Select codelst from CodeList codelst " +
			" where codelst.type in (select codelst1.subType from CodeList codelst1 where pkCodeId =" + selVal + ")";
				
		return criteria;
	}	
	
	public static Object getCordId(String objectName,String id,String fieldName) throws Exception {
		return getCordId(objectName,id,fieldName,null);
		
	}
	
	public static Object getCordId(String objectName,String id,String fieldName,Session session) throws Exception{
		boolean sessionFlag = false;
		//log.debug("OrderAttachmentHelper start");		
		Object resultObject = null;	
		
		
		try {
					
			// check session 
			if(session== null){
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			
			
			resultObject = velosHelper.getObject(objectName,id,fieldName, session);	
			
			
			
			
		}catch(Exception e) {
			//log.debug("Exception in OrderAttachmentHelper :: "+e);
			log.error(e.getMessage());
			e.printStackTrace();
			
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		//log.debug("OrderAttachmentHelper end");
		return resultObject;
	}	
	
	public static CBUploadInfoPojo saveCatUploadInfo(CBUploadInfoPojo cbUploadInfoPojo){
		//log.debug(" Helper saveCatUploadInfo called........");
		
		CBUploadInfo cbUploadInfo = new CBUploadInfo();
		
		
		cbUploadInfo = (CBUploadInfo)ObjectTransfer.transferObjects(cbUploadInfoPojo, cbUploadInfo);
		
		Object object = new VelosUtil().setAuditableInfo(cbUploadInfo);
		addCatUploadInfo((CBUploadInfo)object);
		
		
		return cbUploadInfoPojo;
	}
	
	public static CBUploadInfo addCatUploadInfo(CBUploadInfo cbUploadInfo) {
		//log.debug(" helper without session");
		return addCatUploadInfo(cbUploadInfo,null);
	}
	
	public static CBUploadInfo addCatUploadInfo(CBUploadInfo cbUploadInfo,Session session) {
		
		boolean sessionFlag = false;
		try{
		// check session 
			//log.debug("session " + session);
		if(session== null){
			// create new session
		  session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}
		if(cbUploadInfo.getPk_UploadInfo()!=null)
		{session.merge(cbUploadInfo);
		}else{
			session.save(cbUploadInfo);
		}
		if (sessionFlag) {
			session.flush();
			session.getTransaction().commit();
		}
		} catch (Exception e) {
			//log.debug(" Exception in addOrderAttachments " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			//log.debug(" session in finally " + session + " /" +sessionFlag );
			if (session.isOpen()) {
				session.close();
			}
		}
		return cbUploadInfo;
	}
	
	public static LabMaintPojo saveNewLabTestInfo(LabMaintPojo labMaintPojo){
		//log.debug(" Helper addLabTestInfo called........");
		
		LabMaint labMaint = new LabMaint();
		
		labMaint = (LabMaint)ObjectTransfer.transferObjects(labMaintPojo, labMaint);
		
		Object object = new VelosUtil().setAuditableInfo(labMaint);
		
		addLabTestInfo((LabMaint)object);
		
		labMaintPojo.setPk_LabTestId(labMaint.getPk_LabTestId());
		return labMaintPojo;
	}
	
	public static LabMaint addLabTestInfo(LabMaint labMaint) {
		
		return addLabTestInfo(labMaint,null);
	}
	
	public static LabMaint addLabTestInfo(LabMaint labMaint,Session session) {
		
		boolean sessionFlag = false;
		try{
		// check session 
			//log.debug("session " + session);
		if(session== null){
			// create new session
		  session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}
	
		session.save(labMaint);
		if (session.isOpen()) {
			session.getTransaction().commit();
		}
		} catch (Exception e) {
			//log.debug(" Exception in addLabTestInfo " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			//log.debug(" session in finally " + session + " /" +sessionFlag );
			if (sessionFlag) {
				session.close();
			}
		}
		return labMaint;
	}
	
	public static List getResultsForFilter(String orderType,String colName,String colData,String pkOrderType,String pending,String order_status){
		
		return getResultsForFilter(orderType,colName,colData,pkOrderType,pending,order_status,null);
	}
	
	public static List getResultsForFilter(String orderType,String colName,String colData,String pkOrderType,String pending,String order_status,Session session){
		List lst=new ArrayList();
		DateFormat dat=new SimpleDateFormat("mm/dd/yy");
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="select ordhdr.ORDER_ENTITYID,cbuid.cord_local_cbu_id,typ.codelst_desc typ,to_char(ordhdr.ORDER_OPEN_DATE,'Mon DD, YYYY'),stat.codelst_desc as status,ord.TASK_ID," +
					"ord.ASSIGNED_TO,ord.ORDER_ACK_FLAG,to_char(ord.LAST_MODIFIED_DATE,'Mon DD, YYYY'),ord.pk_order from er_order ord,er_order_header ordhdr,er_codelst typ,cb_cord cbuid," +
					"er_codelst stat where ord.fk_order_header=ordhdr.pk_order_header and ordhdr.order_entityid=cbuid.pk_cord and ord.order_type=typ.pk_codelst and ord.order_status=stat.pk_codelst and " +
					"stat.codelst_type='"+com.velos.ordercomponent.business.util.VelosMidConstants.ORDER_STATUS+"' and " +
					"stat.codelst_subtyp not in('"+com.velos.ordercomponent.business.util.VelosMidConstants.CLOSED+"')";
			
			if((orderType!=null && !orderType.equals("")) && (!orderType.equals(pkOrderType))){
				//log.debug("order type is::::"+orderType);
				qry=qry+" and ord.FK_ORDER_TYPE='"+orderType+"'";
			}
			
			if((colName!=null && !colName.equals("")) && (colData!=null && !colData.equals("")) && (!colName.equals(VelosMidConstants.ORDER_DATE))){
				qry=qry+" and ord."+colName+"='"+colData+"'";
			}
			if((colName!=null && !colName.equals("")) && (colData!=null && !colData.equals("")) && (colName.equals(VelosMidConstants.ORDER_DATE))){
				qry=qry+" and ord."+colName+"=to_date('"+colData+"','mm/dd/yy')";
			}
			//log.debug("query issssssssss::"+qry);
			if(Utilities.removesqlconstants(qry)){
				lst=session.createSQLQuery(qry).list();
			}
			//log.debug("list size isssssss"+lst.size());
			Iterator it=lst.iterator();
			while (it.hasNext()) {
				//log.debug("values**********"+it.next());
				
			}
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return lst;
	}
	public static List<FilterDataPojo> getCustomFilterData(Long userId,Long module,Long submodule){
		return getCustomFilterData(userId,module,submodule,null);
	}
	@SuppressWarnings("unchecked")
	public static List<FilterDataPojo> getCustomFilterData(Long userId,Long module,Long submodule,Session session){
		
		List<FilterDataPojo> customFilter=new ArrayList<FilterDataPojo>();
		List<FilterData> custfilter=new ArrayList<FilterData>();
		
		try {
			if (session == null) {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
					
			String sql = "From FilterData where fk_user in ("+userId+") and fk_module in ("+module+") and fk_sub_module in ("+submodule+") and fk_filter_type in (select pkCodeId from CodeList where lower(type) ='"+VelosMidConstants.FILTER_TYPE+"' and lower(subType)='"+VelosMidConstants.CUSTOM_FILTER+"') order by UPPER(filter_name)";
			
			//log.debug("\n\n\n\n\n\n\n\n\nsql:::::::::::::::::"+sql);
			if(Utilities.removesqlconstants(sql)){
				custfilter = session.createQuery(sql).list();
			}
			for(FilterData s : custfilter){
				FilterDataPojo filterdatapojo= new FilterDataPojo();
				filterdatapojo = (FilterDataPojo)ObjectTransfer.transferObjects(s,filterdatapojo);
				customFilter.add(filterdatapojo);
			}
			//log.debug("\n\n\n\n\n\n\n\n\ncustomfiltersize in get custom filter data function:::::::::::::::::"+customFilter.size());
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return customFilter;
			
		
	}
	
public static List getOrderDetails(String orderId){
		Long pkorder=0l;
		if(orderId!=null && !orderId.equals("") && !orderId.equals("undefined")){
			pkorder=Long.parseLong(orderId);
		}
		return getOrderDetails(pkorder,null);
	}
	
	public static List getOrderDetails(Long orderId,Session session){
		List lst=new ArrayList();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
				//log.debug("transaction begin&&&&&&&");
				//log.debug("iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
			}
			String qry="select c.CORD_REGISTRY_ID,c.CORD_TNC_FROZEN,c.CORD_CBU_COLLECTION_DATE,c.CORD_ISBI_DIN_CODE,"+
						" cbb.SITE_NAME,CORD_ISIT_PRODUCT_CODE,CORD_ID_NUMBER_ON_CBU_BAG,lic_stat.codelst_desc licstat,unlic_res.codelst_desc unlicres,"+
						" eli_stat.codelst_desc elistat,ineli_res.codelst_desc inelires,FK_CORD_CDR_CBU_STATUS,CORD_TNC_FROZEN_PAT_WT,"+
						" IND_OTHER_NAME,IND_OTHER_NUM,ext_name.codelst_desc extname,ext_num.codelst_desc extnum,cbu_stat.codelst_desc descrip from cb_cord c left outer join ER_SITE cbb on(cbb.PK_SITE=c.fk_cbb_id)"+
						" left outer join er_codelst lic_stat on(lic_stat.pk_codelst=c.fk_cord_cbu_lic_status)"+
						" left outer join er_codelst unlic_res on(unlic_res.pk_codelst=c.fk_cord_cbu_unlicensed_reason)"+
						" left outer join er_codelst  eli_stat on(eli_stat.pk_codelst=c.fk_cord_cbu_eligible_status)"+
						" left outer join er_codelst ineli_res on (ineli_res.pk_codelst=c.fk_cord_cbu_ineligible_reason)"+
						" left outer join er_codelst cbu_stat on(cbu_stat.pk_codelst=c.fk_cord_cbu_status)"+
						" left outer join er_codelst ext_name on(ext_name.pk_codelst=c.fk_codelst_external_name)"+
						" left outer join er_codelst ext_num on(ext_num.pk_codelst=c.fk_codelst_external_num)"+
						" where c.PK_CORD="+orderId;
			
			
			//log.debug("query issssssssss::"+qry);
			if(Utilities.removesqlconstants(qry)){
				lst=session.createSQLQuery(qry).list();
			}
			//log.debug("list size isssssss"+lst.size());
			Iterator it=lst.iterator();
			while (it.hasNext()) {
				//log.debug("values**********"+it.next());
				
			}
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return lst;
	}
	
	
public static List getCtOrderDetails(String orderId){
		Long pkorder=0l;
		if(orderId!=null){
			orderId = orderId.trim();
			if(!orderId.equals("") && !orderId.equals("undefined")){
				pkorder=Long.parseLong(orderId);
			}
		}
		return getCtOrderDetails(pkorder,null);
	}
	
	public static List getCtOrderDetails(Long orderId,Session session){
		List lst=new ArrayList();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			
			
			String qry="SELECT f_codelst_desc(ord.order_status) order_status_desc, " +
			"  NVL(TO_CHAR(ord.order_status_date,'Mon DD, YYYY'),'Not Provided') order_status_date, " +
			"  ord.order_sample_at_lab, " +
			"  ord.assigned_to, " +
			"  NVL(TO_CHAR(ord.order_assigned_date,'Mon DD, YYYY'),'Not Provided') assign_date, " +
			"  CASE " +
			"    WHEN ORD.ORDER_LAST_VIEWED_BY IS NULL " +
			"    THEN '' " +
			"    WHEN ORD.ORDER_LAST_VIEWED_DATE-NVL(ORD.LAST_MODIFIED_DATE,TO_DATE('01/01/1900','MM/DD/YYYY'))>0 " +
			"    THEN viewby.usr_logname " +
			"    WHEN ORD.LAST_MODIFIED_DATE-ORD.ORDER_LAST_VIEWED_DATE>0 " +
			"    THEN modifiedby.usr_logname " +
			"  Else viewby.usr_logname "+
			"  END viewed_by, " +
			"  CASE " +
			"    WHEN ORD.ORDER_LAST_VIEWED_DATE IS NULL " +
			"    THEN '' " +
			"    WHEN ORD.ORDER_LAST_VIEWED_DATE-NVL(ORD.LAST_MODIFIED_DATE,TO_DATE('01/01/1900','MM/DD/YYYY'))>0 " +
			"    THEN NVL(TO_CHAR(ord.ORDER_LAST_VIEWED_DATE,'Mon DD, YYYY/HH24:MI'),'Not Available') " +
			"    WHEN ORD.LAST_MODIFIED_DATE-ORD.ORDER_LAST_VIEWED_DATE>0 " +
			"    THEN NVL(TO_CHAR(ord.LAST_MODIFIED_DATE,'Mon DD, YYYY/HH24:MI'),'Not Available') " +
			"  Else TO_CHAR(ord.ORDER_LAST_VIEWED_DATE,'Mon DD, YYYY/HH24:MI')"+
			"  END order_review_date, " +
			"  trans.site_name tc_name, " +
			"  trans.site_id tc_id, " +
			"  trans1.site_id sectc_id, " +
			"  trans1.site_name sectc_name, " +
			"  TRUNC (MONTHS_BETWEEN (SYSDATE,PAT.PERSON_DOB) / 12) AGE, " +
			"  f_codelst_desc(PAT.fk_codelst_gender) gender, " +
			"  rec.receipant_id, " +
			"  NVL(rec.pre_current_diagnosis,'Not Provided'), " +
			"  ordusrs.USER_LOGINID, " +
			"	CASE " +
			"	WHEN ordusrs.user_fname||' '||ordusrs.user_lname=' ' THEN " +
			"   'Not Provided' " +
			"	WHEN ordusrs.user_fname IS NOT NULL AND (ordusrs.user_lname IS NULL OR ordusrs.user_lname='') THEN " +
			"   ordusrs.user_fname " +
			"	WHEN ordusrs.user_lname IS NOT NULL AND (ordusrs.user_fname IS NULL OR ordusrs.user_fname='') THEN " +
			"   ordusrs.user_lname " +
			"	ELSE " +
			"   ordusrs.user_fname||' '||ordusrs.user_lname " +
			"	END cn_name,"+
			"  ordusrs.user_mailid, " +
			"  ordusrs.user_contactno, " +
			"  NVL(TO_CHAR(f_codelst_desc(rec.FK_TRANSPLANT_TYPE)),'Not Provided'), " +
			"  NVL(TO_CHAR(rec.disease_stage),'Not Provided'), " +
			"  NVL(TO_CHAR(REC.NO_OF_REMISSIONS),'Not Provided'), " +
			"  NVL(TO_CHAR(rec.fk_leu_curr_status),'Not Provided'), " +
			"  NVL(TO_CHAR(rec.rec_transfused),'Not Provided'), " +
			"  NVL(TO_CHAR(rec.rec_weight),'Not Provided'), " +
			"  NVL(TO_CHAR(rec.prop_infusion_date,'Mon DD, YYYY'),'Not Provided') prop_infusion_date, " +
			"  NVL(TO_CHAR(rec.prop_prep_date,'Mon DD, YYYY'),'Not Provided') prop_prep_date, " +
			"  rec.res_cel_sam_flag, " +
			"  NVL(trans2.site_name,'Not Provided'), " +
			"  NVL(per.person_address1,'Not Provided'), " +
			"  NVL(per.person_address2,'Not Provided'), " +
			"  CASE " +
			"   WHEN per.person_city||'/'||per.person_state='/' THEN " +
			"  'Not Provided' " +
			"	WHEN per.person_city IS NOT NULL AND (per.person_state IS NULL OR per.person_state='') THEN " +
			"  	per.person_city " +
			"   WHEN per.person_state IS NOT NULL AND (per.person_city IS NULL OR per.person_city='') THEN " +
			"   per.person_state " +
			"	ELSE " +
			"   per.person_city||'/'||per.person_state " +
			"	END city,"+
			"  NVL(per.person_state,'Not Provided'), " +
			"  NVL(TO_CHAR(per.person_zip),'Not Provided'), " +
			"	CASE " +
			"	WHEN per.person_country||'/'||TO_CHAR(per.person_zip)='/' THEN " +
			"  'Not Provided' " +
			"	WHEN per.person_country IS NOT NULL AND (TO_CHAR(per.person_zip) IS NULL OR TO_CHAR(per.person_zip)='') THEN " +
			"   per.person_country " +
			"	WHEN TO_CHAR(per.person_zip) IS NOT NULL AND (per.person_country IS NULL OR per.person_country='') THEN " +
			"   TO_CHAR(per.person_zip) " +
			"	ELSE " +
			"   per.person_country||'/'||TO_CHAR(per.person_zip) " +
			"	END country,"+
			"  NVL(per.person_notes,'Not Provided'), " +
			"	CASE " +
			"	WHEN p1.person_fname||' '||p1.person_lname=' ' THEN " +
			"   'Not Provided' " +
			"	WHEN p1.person_fname IS NOT NULL AND (p1.person_lname IS NULL OR p1.person_lname='') THEN " +
			"   p1.person_fname " +
			"	WHEN p1.person_lname IS NOT NULL AND (p1.person_fname IS NULL OR p1.person_fname='') THEN " +
			"   p1.person_lname " +
			"	ELSE " +
			"   p1.person_fname||' '||p1.person_lname " +
			"	END contact1_name,"+
			"  NVL(TO_CHAR(p1.person_hphone),'Not Provided') contact1_hphone, " +
			"  NVL(TO_CHAR(p1.person_bphone),'Not Provided') contact1_bphone, " +
			"	CASE " +
			"	WHEN p2.person_fname||' '||p2.person_lname=' ' THEN " +
			"   'Not Provided' " +
			"	WHEN p2.person_fname IS NOT NULL AND (p2.person_lname IS NULL OR p2.person_lname='') THEN " +
			"   p2.person_fname " +
			"	WHEN p2.person_lname IS NOT NULL AND (p2.person_fname IS NULL OR p2.person_fname='') THEN " +
			"   p2.person_lname " +
			"	ELSE " +
			"   p2.person_fname||' '||p2.person_lname " +
			"	END contact2_name,"+
			"  NVL(TO_CHAR(p2.person_hphone),'Not Provided') contact2_hphone, " +
			"  NVL(TO_CHAR(p2.person_bphone),'Not Provided') contact2_bphone, " +
			"	CASE " +
			"	WHEN p3.person_fname||' '||p3.person_lname=' ' THEN " +
			"   'Not Provided' " +
			"	WHEN p3.person_fname IS NOT NULL AND (p3.person_lname IS NULL OR p3.person_lname='') THEN " +
			"   p3.person_fname " +
			"	WHEN p3.person_lname IS NOT NULL AND (p3.person_fname IS NULL OR p3.person_fname='') THEN " +
			"   p3.person_lname " +
			"	ELSE " +
			"   p3.person_fname||' '||p3.person_lname " +
			"	END contact3_name,"+
			"  NVL(TO_CHAR(p3.person_hphone),'Not Provided') contact3_hphone, " +
			"  NVL(TO_CHAR(p3.person_bphone),'Not Provided') contact3_bphone, " +
			"  NVL(TO_CHAR(ordhdr.order_open_date,'Mon DD, YYYY'),'Not Provided') order_date, " +
			"	CASE " +
			"	WHEN phy.person_fname||' '||phy.person_lname=' ' THEN " +
			"   'Not Provided' " +
			"	WHEN phy.person_fname IS NOT NULL AND (phy.person_lname IS NULL OR phy.person_lname='') THEN " +
			"   phy.person_fname " +
			"	WHEN phy.person_lname IS NOT NULL AND (phy.person_fname IS NULL OR phy.person_fname='') THEN " +
			"   phy.person_lname " +
			"	ELSE " +
			"   phy.person_fname||' '||phy.person_lname " +
			"	END order_physician,"+
			"  ord.order_status order_status_code, " +
			"  to_date(TO_CHAR(sysdate,'Mon DD, YYYY'),'Mon DD, YYYY')-to_date(TO_CHAR(ordhdr.order_open_date,'Mon DD, YYYY'),'Mon DD, YYYY') no_of_days, " +
			"  '' hla, " +
			"  '' lucas, " +
			"  '' typ1, " +
			"  '' typ2, " +
			"  '' hladate, " +
			"  ord.additi_typing_flag, " +
			"  NVL(crd.cord_sample_at_lab,' '), " +
			"  NVL(TO_CHAR(ord.infusion_date,'Mon DD, YYYY'),'Not Provided') infusion_date, " +
			"  ord.fk_order_resol_by_tc, " +
			"  ord.fk_order_resol_by_cbb, " +
			"  ord.RECENT_HLA_TYPING_AVAIL, " +
			"  ord.RECENT_HLA_ENTERED_FLAG, " +
			"  ord.NMDP_SAMPLE_SHIPPED_FLAG, " +
			"  ord.FINAL_REVIEW_TASK_FLAG, " +
			"  ord.COMPLETE_REQ_INFO_TASK_FLAG, " +
			"  f_codelst_desc(ord.ORDER_PRIORITY) order_priority, " +
			"  NVL(TO_CHAR(ord.RESULT_REC_DATE,'Mon DD, YYYY'),'Not Available'), " +
			"  ord.FK_CURRENT_HLA, " +
			"  ord.PREV_CT_DONE_FLAG, " +
			"  rec.ind_protocol, " +
			"  nvl(TO_CHAR(rec.ind_sponser),'Not Provided'), " +
			"  nvl(TO_CHAR(rec.ind_number),'Not Provided'), " +
			"  f_codelst_desc(ord.SWITCH_REASON) switch_reason, " +
			"  ord.SWITCH_REASON_OTHER, " +
			"  TO_CHAR(ord.SWITCH_DATE,'Mon DD, YYYY/HH24:MI'), " +
			"  ord.SWITCH_BY, " +
			"  ordhdr.ORDER_CLOSE_REASON, " +
			"  NVL(ord.is_cord_avail_for_nmdp,'-'), " +
			"  NVL(P1.PERSON_FAX,'Not Provided') CONTACT1_FAX, " +
			"  NVL(P2.PERSON_FAX,'Not Provided') CONTACT2_FAX, " +
			"  NVL(P3.PERSON_FAX,'Not Provided') CONTACT3_FAX, " +
			"  rec.EX_VIVO_TRANSPLANT, " +
			"  rec.MULTI_TRANSPLANT, " +
			"  rec.OTHER_TRANSPLANT, " +
			"  rec.SINGLE_UNIT_TRANSPLANT, " +
			"	CASE " +
			"	WHEN per.person_fname||' '||per.person_lname=' ' THEN " +
			"   'Not Provided' " +
			"	WHEN per.person_fname IS NOT NULL AND (per.person_lname IS NULL OR per.person_lname='') THEN " +
			"   per.person_fname " +
			"	WHEN per.person_lname IS NOT NULL AND (per.person_fname IS NULL OR per.person_fname='') THEN " +
			"   per.person_lname " +
			"	ELSE " +
			"   per.person_fname||' '||per.person_lname " +
			"	END attn_name,"+
			"  NVL(TO_CHAR(rec.PROPOSED_SHIP_DTE,'Mon DD, YYYY'),'Not Provided') proposed_shipdate, " +
			"  nvl(rec.re_current_diagnosis,'Not Available'),"+
			"  rec.pk_receipant "+
			"FROM er_order_header ordhdr " +
			"LEFT OUTER JOIN er_order ord " +
			"ON(ord.fk_order_header=ordhdr.pk_order_header) " +
			"LEFT OUTER JOIN cb_cord crd " +
			"ON(ordhdr.order_entityid=crd.pk_cord) " +
			"LEFT OUTER JOIN " +
			"  (SELECT RECEIPANT_ID, " +
			"    FK_CONTACT_PERSON, " +
			"    TRANS_CENTER_ID, " +
			"    NO_OF_REMISSIONS, " +
			"    pre.REC_WEIGHT, " +
			"    FK_RECEIPANT_CONTACT1, " +
			"    FK_RECEIPANT_CONTACT2, " +
			"    FK_RECEIPANT_CONTACT3, " +
			"    REC_HEIGHT, " +
			"    SEC_TRANS_CENTER_ID, " +
			"    ORDER_PHYSICIAN, " +
			"    FK_RECEIPANT, " +
			"    FK_ORDER_ID, " +
			"    IND_PROTOCOL, " +
			"    IND_SPONSER, " +
			"    IND_NUMBER, " +
			"    pre.CURRENT_DIAGNOSIS pre_current_diagnosis, " +
			"    re.CURRENT_DIAGNOSIS re_current_diagnosis, " +
			"    pre.DISEASE_STAGE, " +
			"    FK_TRANSPLANT_TYPE, " +
			"    FK_LEU_CURR_STATUS, " +
			"    REC_TRANSFUSED, " +
			"    PROP_INFUSION_DATE, " +
			"    PROP_PREP_DATE, " +
			"    RES_CEL_SAM_FLAG, " +
			"    EX_VIVO_TRANSPLANT, " +
			"    MULTI_TRANSPLANT, " +
			"    OTHER_TRANSPLANT, " +
			"    SINGLE_UNIT_TRANSPLANT, " +
			"    PROPOSED_SHIP_DTE, " +
			"    FK_PERSON_ID, " +
			"	 pre.PRODUCT_DELIVERY_TC_NAME, "+
			"	 re.pk_receipant pk_receipant"+
			"  FROM er_order_receipant_info pre, " +
			"    cb_receipant_info re " +
			"  WHERE pre.fk_receipant=re.pk_receipant " +
			"  ) rec " +
			"ON(rec.fk_order_id=ord.pk_order) " +
			"LEFT OUTER JOIN EPAT.person per " +
			"ON(rec.FK_CONTACT_PERSON=per.pk_person) " +
			"LEFT OUTER JOIN EPAT.person p1 " +
			"ON(rec.fk_receipant_contact1=p1.pk_person) " +
			"LEFT OUTER JOIN er_site trans2 " +
			"ON(rec.PRODUCT_DELIVERY_TC_NAME=trans2.pk_site) " +
			"LEFT OUTER JOIN er_site trans " +
			"ON(rec.trans_center_id=trans.pk_site) " +
			"LEFT OUTER JOIN er_site trans1 " +
			"ON(rec.sec_trans_center_id=trans1.pk_site) " +
			"LEFT OUTER JOIN EPAT.person p2 " +
			"ON(rec.fk_receipant_contact2=p2.pk_person) " +
			"LEFT OUTER JOIN EPAT.person p3 " +
			"ON(rec.fk_receipant_contact3=p3.pk_person) " +
			"LEFT OUTER JOIN er_order_users ordusrs " +
			"ON(ordhdr.ORDER_REQUESTED_BY=ordusrs.PK_ORDER_USER) " +
			"LEFT OUTER JOIN er_user modifiedby " +
			"ON(ord.LAST_MODIFIED_BY=modifiedby.pk_user) " +
			"LEFT OUTER JOIN epat.person phy " +
			"ON (rec.ORDER_PHYSICIAN=phy.pk_person) " +
			"LEFT OUTER JOIN epat.person pat "+
			"ON (REC.FK_PERSON_ID=PAT.PK_PERSON)"+
			"LEFT OUTER JOIN er_user viewby " +
			"ON(ord.ORDER_LAST_VIEWED_BY=viewby.pk_user) " +
			"WHERE ord.pk_order     ="+orderId;
			log.debug("query issssssssss:::"+qry);
			
			if(Utilities.removesqlconstants(qry)){
				lst=session.createSQLQuery(qry).list();
			}
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return lst;
	}
	
	
	public static CdrCbuPojo submitCordStatus(CdrCbuPojo cdrCbuPojo){
		return submitCordStatus(cdrCbuPojo,null);
	}
	
	public static CdrCbuPojo submitCordStatus(CdrCbuPojo cdrCbuPojo,Session session){
		CdrCbu cdrCbu=new CdrCbu();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			//log.debug("in helper classs"+cdrCbuPojo.getCordID());
			
			if(cdrCbuPojo.getCordID()!=null){
				
				//log.debug("before update status in helper class");
				Object object = new VelosUtil().setAuditableInfo(cdrCbuPojo);
				cdrCbuPojo=(CdrCbuPojo)ObjectTransfer.transferObjects(object, cdrCbuPojo);
				cdrCbu=(CdrCbu)ObjectTransfer.transferObjects(cdrCbuPojo, cdrCbu);
				//session.saveOrUpdate(cdrCbu);
				session.merge(cdrCbu);
				session.getTransaction().commit();
			}
			
		}catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return cdrCbuPojo;
	}
	public static List  getQBuilderList(String sql, String orderBy) {
		return getQBuilderList(sql,orderBy,null);
	}
	
	public static List getQBuilderList(String sql,String orderBy,Session session){
		List ordList = new ArrayList();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			
			
			////log.debug("Inside Helper\n \n \n \n \n \n \n \n \n \n \nquery is:::"+sql);
			Query query = session.createSQLQuery(sql);
			/*if(i==1){
				////log.debug("\n \n \n \n \n \n \n \n \n \n In helper ipage no iShowRows():::"+paginateSearch.getiPageNo()+"::"+paginateSearch.getiShowRows());
				query.setFirstResult(paginateSearch.getiPageNo());
				query.setMaxResults(paginateSearch.getiShowRows());
			}*/
			
			ordList = query.list();
			
			
			////log.debug("List size in Helper..........................."+ordList.size());
			
		}
		catch(Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return ordList;
	}
	
	public static Long getCRI_CompletedFlag(Long pk_cord){
		return getCRI_CompletedFlag(pk_cord,null);
	}
	
	public static Long getCRI_CompletedFlag(Long pk_cord,Session session){
		Long completedFlag=(long)0;
		String sql="";
		List temp=new ArrayList();
		
		try{
			
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			sql="SELECT "+
				  "CASE "+
				    " WHEN (SELECT COUNT(*) FROM CB_CORD_COMPLETE_REQ_INFO WHERE FK_CORD_ID="+pk_cord+")>0 "+
				    " THEN "+
				    " (SELECT NVL(COMP_REQ_INFO.COMPLETE_REQ_INFO_FLAG,'0') "+
				    " FROM CB_CORD_COMPLETE_REQ_INFO COMP_REQ_INFO "+
				    " WHERE COMP_REQ_INFO.PK_CORD_COMPLETE_REQINFO IN "+
				    " (SELECT MAX(PK_CORD_COMPLETE_REQINFO) "+
				    " FROM CB_CORD_COMPLETE_REQ_INFO "+
				    " WHERE FK_CORD_ID="+pk_cord+"))"+
				    " ELSE "+
				    " '0' "+
				    " END COMPLET_FLAG "+
				 " FROM DUAL";
			
			//log.debug("\n\n\n\n\n\n\n\n\n\nSQL for Get CRI flag::::::::::::::::::::::::::"+sql);
			if(Utilities.removesqlconstants(sql)){
				temp=session.createSQLQuery(sql).list();
			}
			if(temp!=null && temp.size()>0){
				Object obj=temp.get(0);
				if(obj!=null){
					completedFlag=Long.parseLong(obj.toString());
				}
			}
			//log.debug("\n\n\n\n\n\n\n\n\n\ncompletedFlag"+completedFlag);
			
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		
		
		return completedFlag;
	}
	public static List getResultsForCancelFilter(String orderType,String colName,String colData,String pkOrderType,String cancelled,String order_status){
		
		return getResultsForFilter(orderType,colName,colData,pkOrderType,cancelled,order_status,null);
	}
	
	public static List getResultsForCancelFilter(String orderType,String colName,String colData,String pkOrderType,String cancelled,String order_status,Session session){
		List lst=new ArrayList();
		DateFormat dat=new SimpleDateFormat("mm/dd/yy");
		
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
				//log.debug("transaction begin&&&&&&&");
				//log.debug("iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
			}
			String qry="select ord.ORDER_ENTITYID,cbuid.cord_local_cbu_id,typ.codelst_desc typ,to_char(ord.ORDER_DATE,'Mon DD, YYYY') ORDER_DATE,"+
						"stat.codelst_desc as status,ord.TASK_ID,ord.ASSIGNED_TO,ord.ORDER_ACK_FLAG,to_char(ord.LAST_MODIFIED_DATE,'Mon DD, YYYY') LAST_MODIFIED_DATE from er_order ord,"+
						"er_codelst typ,cb_cord cbuid,er_codelst stat "+
						"where ord.order_entityid=cbuid.pk_cord and ord.fk_order_type=typ.pk_codelst and "+
						"ord.fk_order_status=stat.pk_codelst and stat.codelst_type='"+order_status+"' and stat.codelst_subtyp='"+cancelled+"'";
			
			if((orderType!=null && !orderType.equals("")) && (!orderType.equals(pkOrderType))){
				//log.debug("order type is::::"+orderType);
				qry=qry+" and ord.FK_ORDER_TYPE='"+orderType+"'";
			}
			
			if((colName!=null && !colName.equals("")) && (colData!=null && !colData.equals("")) && (!colName.equals(VelosMidConstants.ORDER_DATE))){
				qry=qry+" and ord."+colName+"='"+colData+"'";
			}
			if((colName!=null && !colName.equals("")) && (colData!=null && !colData.equals("")) && (colName.equals(VelosMidConstants.ORDER_DATE))){
				qry=qry+" and ord."+colName+"=to_date('"+colData+"','mm/dd/yy')";
			}
			//log.debug("query issssssssss::"+qry);
			if(Utilities.removesqlconstants(qry)){
				lst=session.createSQLQuery(qry).list();
			}
			//log.debug("list size isssssss"+lst.size());
			Iterator it=lst.iterator();
			while (it.hasNext()) {
				//log.debug("values**********"+it.next());
				
			}
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return lst;
	}

public static OrderPojo updateAcknowledgement(OrderPojo orderpojo){
	
	return updateAcknowledgement(orderpojo,null);
}
public static OrderPojo updateAcknowledgement(OrderPojo orderpojo,Session session){
	Orders orders=new Orders();
	try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			//log.debug("before update status in helper class");
			orders=(Orders)ObjectTransfer.transferObjects(orderpojo, orders);
			session.merge(orders);
			//log.debug("after update");
			session.getTransaction().commit();
			//log.debug("after commit");
			
	}catch(Exception e){
		log.error(e.getMessage());
		e.printStackTrace();
	}finally {
		if (session.isOpen()) {
			session.close();
		}
	}
return orderpojo;
}

public static void submitCtResolution(String pkorderId,String availDt,String resolbyCbb,String resolbyTc,String ackTcResolution,String acceptToCancel,String pkTempunavail,String pkcordId,String rejectDt,String rejectBy,String userID){
	//log.debug("in helper1");
	submitCtResolution(pkorderId,availDt,resolbyCbb,resolbyTc,ackTcResolution,acceptToCancel,pkTempunavail,pkcordId,rejectDt,rejectBy,userID,null);
}
@SuppressWarnings("unused")
public static void submitCtResolution(String pkorderId,String availDt,String resolbyCbb,String resolbyTc,String ackTcResolution,String acceptToCancel,String pkTempunavail,String pkcordId,String rejectDt,String rejectBy,String userID,Session session){
	//log.debug("in helper2");
	Long pkorder=0l;
	if(pkorderId!=null){
		pkorderId = pkorderId.trim();
		if(pkorderId!=null && !pkorderId.equals("") && !pkorderId.equals("undefined")){
			pkorder=Long.parseLong(pkorderId);
		}
	}
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			//log.debug("Inside submitCtResolution helper");
			/*log.debug("\n Pk of order : "+pkorderId+
							   "\n Available date: : "+availDt+
							   "\n resolbyCbb : "+resolbyCbb+
							   "\n resolbyTc : "+resolbyTc+
							   "\n ackTcResolution : "+ackTcResolution+
							   "\n acceptToCancel : "+acceptToCancel+
							   "\n pkTempunavail : "+pkTempunavail+
							   "\n pkCancelId : "+pkcordId+
							   "\n rejectDt : "+rejectDt+
							   "\n rejectBy : "+rejectBy+
							   "\n UserId :"+userID);*/
			if(availDt!=null && availDt!=""){
					//log.debug("Inside availDt update..........");
					String qry1="update cb_cord cord set cord.CORD_AVAIL_DATE=to_date('"+availDt+"','Mon-DD-YYYY') where cord.PK_CORD=(select ordhre.ORDER_ENTITYID from er_order_header ordhre inner join er_order ord on(ord.fk_order_header=ordhre.pk_order_header) where ord.pk_order="+pkorder+")";
					if(Utilities.removesqlconstants(pkorderId+availDt+" "+resolbyCbb+" "+resolbyTc+" "+ackTcResolution+" "+acceptToCancel+" "+pkTempunavail+" "+pkcordId+" "+rejectDt+" "+rejectBy+" "+userID)){
						SQLQuery sqry1=session.createSQLQuery(qry1);
						int a1=sqry1.executeUpdate();
					//log.debug("after commiting submitCtResolution");
					}
			}
			else{
				String qry1="update cb_cord cord set cord.CORD_AVAIL_DATE='' where cord.PK_CORD=(select ordhre.ORDER_ENTITYID from er_order_header ordhre inner join er_order ord on(ord.fk_order_header=ordhre.pk_order_header) where ord.pk_order="+pkorder+")";
				SQLQuery sqry1=session.createSQLQuery(qry1);
				int a1=sqry1.executeUpdate();
			}
			/*if(resolbyCbb!=null && resolbyCbb!=""){
				
				//log.debug("Inside Cbb resolution..........");
				String qry="update er_order ord set ord.FK_ORDER_RESOL_BY_CBB='"+resolbyCbb+"',ord.ORDER_RESOL_DATE=sysdate where ord.pk_order='"+pkorderId+"'";
				SQLQuery sqry2=session.createSQLQuery(qry);
				int a1=sqry2.executeUpdate();
				session.getTransaction().commit();
				//log.debug("after commiting submitCtResolution");
				
			}*/
			if(resolbyTc!= null && resolbyTc!= ""){
				//log.debug("Inside TC resolution..........");
				String pkcord="0";
			    updateAssignTo(pkorderId, "", "acknowledge", Long.parseLong(userID),pkcord);
			}
		
		
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally {
			if (session.isOpen()) {
				session.close();
			}
		}
}


public static void submitHeResolution(String pkorderId,String availDt,String resolbyCbb,String resolbyTc,String ackTcResolution,String pkTempunavail){
	//log.debug("in helper1");
	submitHeResolution(pkorderId,availDt,resolbyCbb,resolbyTc,ackTcResolution,pkTempunavail,null);
}
public static void submitHeResolution(String pkorderId,String availDt,String resolbyCbb,String resolbyTc,String ackTcResolution,String pkTempunavail,Session session){
	//log.debug("in helper2");
	Long pkorder=0l;
	if(pkorderId!=null){
		pkorderId = pkorderId.trim();
		if(!pkorderId.equals("") && !pkorderId.equals("undefined")){
			pkorder=Long.parseLong(pkorderId);
		}
	}
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			/*log.debug("\n Pk of order : "+pkorderId+
							   "\n Available date: : "+availDt+
							   "\n resolbyCbb : "+resolbyCbb+
							   "\n resolbyTc : "+resolbyTc+
							   "\n ackTcResolution : "+ackTcResolution+
							   "\n pkTempunavail : "+pkTempunavail);*/
			if( (resolbyCbb!= null && resolbyCbb!= "") || ((resolbyTc== null && resolbyTc== "")&&(resolbyCbb== null && resolbyCbb== "")) ){
							
				//log.debug("Inside Cbb resolution..........");
				if(availDt!=null && availDt!=""){
						//log.debug("Inside availDt update..........");
						String qry1="update cb_cord cord set cord.CORD_AVAIL_DATE=to_date('"+availDt+"','Mon-DD-YYYY') where cord.PK_CORD=(select ORDER_ENTITYID from er_order where PK_ORDER="+pkorder+")";
						SQLQuery sqry1=session.createSQLQuery(qry1);
						int a1=sqry1.executeUpdate();
						//log.debug("after commiting submitCtResolution");
				}
				String qry="update er_order ord set ord.FK_ORDER_RESOL_BY_CBB='"+resolbyCbb+"',ord.ORDER_RESOL_DATE=sysdate where ord.pk_order="+pkorder+"";
				SQLQuery sqry2=session.createSQLQuery(qry);
				int a1=sqry2.executeUpdate();
				session.getTransaction().commit();
				//log.debug("after commiting submitCtResolution");
				
			}
			else if(resolbyTc!= null && resolbyTc!= ""){
				if(availDt!=null && availDt!=""){
					//log.debug("Inside availDt update..........");
					String qry1="update cb_cord cord set cord.CORD_AVAIL_DATE=to_date('"+availDt+"','Mon-DD-YYYY') where cord.PK_CORD=(select ordhdr.ORDER_ENTITYID from er_order ord,er_order_header ordhdr where ord.fk_order_header=ordhdr.pk_order_header and PK_ORDER="+pkorder+")";
					SQLQuery sqry1=session.createSQLQuery(qry1);
					int a1=sqry1.executeUpdate();
					//log.debug("after commiting submitCtResolution");
					}
					//log.debug("Inside TC resolution..........");
					String qry="update er_order ord set ord.ORDER_ACK_FLAG='Y',ord.ORDER_ACK_DATE=sysdate,ord. where PK_ORDER="+pkorder;
					SQLQuery sqry=session.createSQLQuery(qry);
					int a=sqry.executeUpdate();
					session.getTransaction().commit();
					//log.debug("after commiting submitCtResolution");
				
			}
			else{
				//log.debug("Condition not matched..........");
			}
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally {
			if (session.isOpen()) {
				session.close();
			}
		}
}
	
	public static List getAntigensForReceipant(String receipantId,String entityType){
		Long pkid=0l;
		if(receipantId!=null){
			receipantId = receipantId.trim();
			if(!receipantId.equals("") && !receipantId.equals("undefined")){
				pkid=Long.parseLong(receipantId);
			}
		}
		return getAntigensForReceipant(pkid,entityType,null);
	}
	
	public static List getAntigensForReceipant(Long receipantId,String entityType,Session session){
		List lst=new ArrayList();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
				
			}
			String sql="select c.codelst_desc hla_type,c1.codelst_desc hla_method,HLA_VALUE_TYPE1,HLA_VALUE_TYPE2,to_char(h.created_on,'Mon DD YYYY') "+
						"from cb_receipant_info r left outer join cb_hla h on(r.pk_receipant=h.entity_id and h.entity_type="+entityType+") "+
						"left outer join er_codelst c on(h.fk_hla_code_id=c.pk_codelst) "+
						"left outer join er_codelst c1 on(h.fk_hla_method_id=c1.pk_codelst) where r.receipant_id="+receipantId;
			//log.debug("query is:::"+sql);
			lst=(List)session.createSQLQuery(sql).list();
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}
		finally{
			if (session.isOpen()) {
				session.close();
			}
		}
		return lst;
	}
	
	public static Long getdefaultshipper(Long orderId,String orderType){
		return getdefaultshipper(orderId,orderType,null);
	}
	
	public static Long getdefaultshipper(Long orderId,String orderType,Session session){
		Long defaultshipper=null;
		List lst=new ArrayList();
		String sql="";
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
				
			}
			if(orderType!=null && orderType.equalsIgnoreCase("CT")){
				sql="select default_ct_shipper from ERES.cbb cb,ERES.er_site s where cb.fk_site=s.pk_site and " +
					"s.pk_site=(select fk_cbb_id from eres.cb_cord cb,eres.er_order ord,eres.er_order_header ordhdr where ord.fk_order_header=ordhdr.pk_order_header and " +
					"cb.pk_cord=ordhdr.ORDER_ENTITYID and pk_order="+orderId+")";
			}
			else if(orderType!=null && orderType.equalsIgnoreCase("OR")){
				sql="select DEFAULT_OR_SHIPPER from ERES.cbb cb,ERES.er_site s where cb.fk_site=s.pk_site and " +
				"s.pk_site=(select fk_cbb_id from eres.cb_cord cb,eres.er_order ord,eres.er_order_header ordhdr where ord.fk_order_header=ordhdr.pk_order_header and " +
				"cb.pk_cord=ordhdr.ORDER_ENTITYID and pk_order="+orderId+")";
			}
			else{
				sql="select 0 from dual";
			}
			//log.debug("query is:::"+sql);
			lst=session.createSQLQuery(sql).list();
			if(lst.size()>0 && lst.get(0)!=null){
				defaultshipper=Long.parseLong(lst.get(0).toString());
			}
			
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}
		finally{
			if (session.isOpen()) {
				session.close();
			}
		}
		return defaultshipper;
	}
	
	public static ShipmentPojo saveShipmentInfo(ShipmentPojo shipmentPojo){
		return saveShipmentInfo(shipmentPojo, null);
	}
	
	public static ShipmentPojo saveShipmentInfo(ShipmentPojo shipmentPojo,Session session){
			Shipment shipment=new Shipment();
			Shipment shipment2=new Shipment();
			List lst=new ArrayList();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
				
			}
			
			String qry="from Shipment where orderId='"+shipmentPojo.getOrderId()+"' and shipmentType='"+shipmentPojo.getShipmentType()+"'";
			//log.debug("shipment update query is:::"+qry);
			lst=session.createQuery(qry).list();
			//log.debug("in saveShipmentInfo  lst.size()::::"+lst.size());
			if(lst!=null && lst.size()>0){
				shipment2=(Shipment)lst.get(0);
			}
			shipment=(Shipment)ObjectTransfer.transferObjects(shipmentPojo, shipment);
			shipment2=(Shipment)ObjectTransfer.transferObjects(shipment, shipment2);
//			session.clear();
			Object object = new VelosUtil().setAuditableInfo(shipment2);
			shipment2=(Shipment)ObjectTransfer.transferObjects(object, shipment2);
			
			if(shipmentPojo.getSwitchWfFlag()!=null && shipmentPojo.getSwitchWfFlag().equals("1")){
				//log.debug("shipmentPojo.getSwitchWfFlag():::::"+shipmentPojo.getSwitchWfFlag());
				session.delete(shipment2);
			}
			else{
				if(shipment2.getShipmentId()==null){
					shipment2.setDataModifiedFlag("N");
					//log.debug("in save shipment2.getDataModifiedFlag()::"+shipment2.getDataModifiedFlag());					
					session.save(shipment2);
				}
				else{
					
					if(shipment2.getDataModifiedFlag()!=null && shipment2.getDataModifiedFlag().equals("replace")){
						shipment2.setDataModifiedFlag("");
					}else{
						shipment2.setDataModifiedFlag("Y");
					}
					if(shipmentPojo.getUpdateFlag()!=null && shipmentPojo.getUpdateFlag().equals("true")){
						shipment2.setSchShipmentDate(shipmentPojo.getSchShipmentDate());
						shipment2.setShipmentTrackingNo(shipmentPojo.getShipmentTrackingNo());
						shipment2.setFkshipingCompanyId(shipmentPojo.getFkshipingCompanyId());
						shipment2.setAdditiShiiperDet(shipmentPojo.getAdditiShiiperDet());
						shipment2.setPaperworkLoc(shipmentPojo.getPaperworkLoc());
						shipment2.setPadlockCombination(shipmentPojo.getPadlockCombination());
						shipment2.setShipmentTime(shipmentPojo.getShipmentTime());
						shipment2.setFkShippingContainer(shipmentPojo.getFkShippingContainer());
						shipment2.setFkTempMoniter(shipmentPojo.getFkTempMoniter());
						/*if(shipmentPojo.getFkShippingContainer()!=null && shipmentPojo.getFkShippingContainer().equals(shipmentPojo.getPkothershippingcont())){
							shipment2.setOthershippingcont(shipmentPojo.getOthershippingcont());
						}else{
							shipment2.setOthershippingcont(null);
						}
						if(shipmentPojo.getFkTempMoniter()!=null && shipmentPojo.getFkTempMoniter().equals(shipmentPojo.getPkothertempmonitor())){
							shipment2.setOthertempmonitor(shipmentPojo.getOthertempmonitor());
						}else{
							shipment2.setOthertempmonitor(null);
						}
						//log.debug("shipment2.getOthershippingcont()::::::::::"+shipment2.getOthershippingcont());
						//log.debug("shipment2.getOthertempmonitor():::::::::"+shipment2.getOthertempmonitor());*/
						log.debug("updating shipment details when reset the ship data:::::::::::::::::::::::::::::::::::::::");
					}
					
					//log.debug("in update shipment2.getDataModifiedFlag()::"+shipment2.getDataModifiedFlag());
					session.merge(shipment2);
					
				}
			}
			
			session.getTransaction().commit();
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}
		finally{
			if (session.isOpen()) {
				session.close();
			}
		}
		return shipmentPojo;
	}
	
	
	public static OrderPojo updateOrderDetails(OrderPojo orderPojo){
		return updateOrderDetails(orderPojo, null);
	}
	
	public static OrderPojo updateOrderDetails(OrderPojo orderPojo,Session session){
			Orders orders=new Orders();
			Orders orders2=new Orders();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
				
			}
			List<Orders> lst=(List<Orders>)session.createQuery("from Orders where pk_OrderId="+orderPojo.getPk_OrderId()).list();
			if(lst!=null && lst.size()>0){
				orders2=(Orders)lst.get(0);
			}
			orders=(Orders)ObjectTransfer.transferObjects(orderPojo, orders);
			orders2=(Orders)ObjectTransfer.transferObjects(orders, orders2);
			session.clear();
			Object object = new VelosUtil().setAuditableInfo(orders2);
			orders2=(Orders)ObjectTransfer.transferObjects(object, orders2);
			//log.debug("orders2.getOrderStatusDate()::::"+orders2.getOrderStatusDate());
			if(orders2!=null && orders2.getPk_OrderId()!=null &&!orders2.getPk_OrderId().equals("")){
				//log.debug("before merge");
				if(orders2.getAliquotsType()!=null && orders2.getAliquotsType().equals(0l)){
					orders2.setAliquotsType(null);
				}
				if(orderPojo.getUpdateShipdetFlag()!=null && orderPojo.getUpdateShipdetFlag().equals("true")){
					orders2.setSampleTypeAvail(orderPojo.getSampleTypeAvail());
					orders2.setAliquotsType(orderPojo.getAliquotsType());
					log.debug("resetting sample type and aliquot type>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
				}
				session.merge(orders2);
			}
			orderPojo=(OrderPojo)ObjectTransfer.transferObjects(orders2, orderPojo);
			session.getTransaction().commit();
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}
		finally{
			if (session.isOpen()) {
				session.close();
			}
		}
		return orderPojo;
	}
	
	
	public static ShipmentPojo getshipmentDetails(ShipmentPojo shipmentPojo){
		return getshipmentDetails(shipmentPojo,null);
	}
	
	
	public static ShipmentPojo getshipmentDetails(ShipmentPojo shipmentPojo,Session session){
		Shipment shipment=new Shipment();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			//log.debug("order is in helper class issssssssssssssssss::::::::"+shipmentPojo.getOrderId());
			if(shipmentPojo.getOrderId()!=null && !shipmentPojo.getOrderId().equals("")){
				shipment=(Shipment)session.createQuery("from Shipment where orderId="+shipmentPojo.getOrderId()).list().get(0);
				shipmentPojo=(ShipmentPojo)ObjectTransfer.transferObjects(shipment, shipmentPojo);
			}
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			// TODO: handle exception
			//log.debug(e.getMessage());
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		
		
		return shipmentPojo;
	}
	
	
	public static HLAPojo saveCurrentHlaTyping(HLAPojo hlaPojo){
		return saveCurrentHlaTyping(hlaPojo,null);
	}
	
	public static HLAPojo saveCurrentHlaTyping(HLAPojo hlaPojo,Session session){
		HLA hla=new HLA();
		//RecipientInfo recipientInfo=new RecipientInfo();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			//recipientInfo=(RecipientInfo)session.createQuery("from RecipientInfo where fkCord="+hlaPojo.getFkentityCordId()).list().get(0);
			//log.debug("recipient info pk id:::::::"+recipientInfo.getPkRecipient());
			//hlaPojo.setFkentityCordId(recipientInfo.getPkRecipient());
			hla=(HLA)ObjectTransfer.transferObjects(hlaPojo, hla);
			Object object = new VelosUtil().setAuditableInfo(hla);
			hla=(HLA)ObjectTransfer.transferObjects(object, hla);
			if(hla.getPkHla()==null){
				session.save(hla);
			}
			session.getTransaction().commit();
			hlaPojo=(HLAPojo)ObjectTransfer.transferObjects(hla, hlaPojo);
			//log.debug("hlapojo.pkhla:::::"+hlaPojo.getPkHla());
		}catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage());
			e.printStackTrace();
		}
		finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return hlaPojo;
	}
	
	public static List getTasksList(PaginateSearch paginateSearch,int i,Long userId,String siteId,String condition,String OrdByCondition){
		//log.debug("helper pending order");
		return getTasksList(paginateSearch,null,i,userId,siteId,condition,OrdByCondition);
	}
	public static List getTasksList(PaginateSearch paginateSearch, Session session,int i,Long userId,String siteId,String condition,String OrdByCondition){
		List tasksList=new ArrayList<TaskListPojo>();
		//log.debug("in helper classsssssssssssssssss get task list::::::");
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="";
			String selectResult="";
			
			if(i==0 || i==1){
				selectResult=" CASE WHEN f_codelst_desc(ord.order_type)='"+VelosMidConstants.CT_ORDER+"' THEN decode(ord.order_sample_at_lab,'Y','"+VelosMidConstants.CT_LAB_ORDER+"',"+
	            "'N','"+VelosMidConstants.CT_SHIP_ORDER+"') WHEN f_codelst_desc(ord.order_type)!='"+VelosMidConstants.CT_ORDER+"' THEN f_codelst_desc(ord.order_type) END order_type,ordhdr.order_num,crd.cord_registry_id,to_char(ordhdr.order_open_date,'Mon DD, YYYY'),f_codelst_desc(ord.order_status) order_status,ac.activity_desc," +
				"tsk.task_completeflag ,ord.pk_order,ordhdr.order_entityid,ac.activity_name,ord.order_sample_at_lab,crd.fk_cbb_id,ROW_NUMBER () OVER(ORDER BY ord.pk_order),ROW_NUMBER () OVER(ORDER BY ord.order_type),ROW_NUMBER () OVER(ORDER BY wa.wa_sequence) ";
			}
			if(i==2){
				selectResult=" count(*) totalrec ";
			}
			
			if(siteId==null || siteId.equals("")){
				
				qry="select "+selectResult+" from task tsk,workflow_instance wi,workflow_activity wa,activity ac,er_order ord,cb_cord crd,er_codelst stat,er_order_header ordhdr,er_site site" +
				" where tsk.fk_wfinstanceid=wi.pk_wfinstanceid and tsk.fk_workflowactivity=wa.pk_workflowactivity and wa.fk_activityid=ac.pk_activity and ord.fk_order_header=ordhdr.pk_order_header and tsk.fk_entity=ord.pk_order" +
				" and ordhdr.order_entityid=crd.pk_cord and ord.order_status=stat.pk_codelst and stat.codelst_type='"+VelosMidConstants.ORDER_STATUS+"' and stat.codelst_subtyp not in('"+VelosMidConstants.CLOSED+"') and site.pk_site=crd.fk_cbb_id and site.pk_site in ( select us.fk_site From er_usersite us,cbb cb where cb.fk_site=us.fk_site and us.fk_user="+userId+" and us.usersite_right>0 and cb.using_cdr=1) and site.fk_codelst_type=(select pk_codelst from er_codelst where codelst_type ='"+VelosMidConstants.CBB_TYPE+"' and codelst_subtyp='"+VelosMidConstants.CBB_SUBTYPE+"') "+condition+" order by ord.pk_order,ord.order_type,wa.wa_sequence";
				
			}else{
				
				qry="select "+selectResult+
				" FROM task tsk,workflow_instance wi,workflow_activity wa,activity ac,er_order ord,cb_cord crd,er_order_header ordhdr WHERE tsk.fk_wfinstanceid=wi.pk_wfinstanceid AND tsk.fk_workflowactivity=wa.pk_workflowactivity "+
				" AND wa.fk_activityid=ac.pk_activity AND ord.fk_order_header=ordhdr.pk_order_header AND tsk.fk_entity=ord.pk_order AND ordhdr.order_entityid=crd.pk_cord AND ord.order_status<> (select pk_codelst from er_codelst where codelst_type='"+VelosMidConstants.ORDER_STATUS+"' and codelst_subtyp ='"+VelosMidConstants.CLOSED+"') "+
				" AND crd.fk_cbb_id IN ("+siteId+")" +condition;
				
				/*" from task tsk,workflow_instance wi,workflow_activity wa,activity ac,er_order ord,cb_cord crd,er_codelst stat,er_order_header ordhdr,er_site site" +
				" where tsk.fk_wfinstanceid=wi.pk_wfinstanceid and tsk.fk_workflowactivity=wa.pk_workflowactivity and wa.fk_activityid=ac.pk_activity and ord.fk_order_header=ordhdr.pk_order_header and tsk.fk_entity=ord.pk_order" +
				" and ordhdr.order_entityid=crd.pk_cord and ord.order_status=stat.pk_codelst and stat.codelst_type='"+VelosMidConstants.ORDER_STATUS+"' and stat.codelst_subtyp not in('"+VelosMidConstants.CLOSED+"') and site.pk_site=crd.fk_cbb_id and site.pk_site in ("+siteId+") and site.fk_codelst_type=(select pk_codelst from er_codelst where codelst_type ='"+VelosMidConstants.CBB_TYPE+"' and codelst_subtyp='"+VelosMidConstants.CBB_SUBTYPE+"') "+condition;*/
				
				
			
				if(OrdByCondition==null || OrdByCondition.equals("")){
					qry=qry+" order by ord.pk_order,ord.order_type,wa.wa_sequence";
				}else{
					qry=qry+OrdByCondition;
				}
				
			}
			//log.debug(" \n \n \n \n \n \n \n \nqry in workflow:::::::::"+ qry);
			Query query = session.createSQLQuery(qry);
			if(paginateSearch!=null && i==1){
			query.setFirstResult(paginateSearch.getiPageNo());
			query.setMaxResults(paginateSearch.getiShowRows());
			}
			tasksList=query.list();
			//log.debug("lsit sizeeeeeeee is in task list::::"+tasksList.size());
			
		}
		catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return tasksList;
	}

	public static String checkShedShipDt(String orderid,String cbuid,String pkavailable){
		Long pkorder=0l;
		Long pkcbu=0l;
		Long pkavail=0l;
		if(orderid!=null && !orderid.equals("") && !orderid.equals("undefined")){
			pkorder=Long.parseLong(orderid);
		}
		if(cbuid!=null && !cbuid.equals("") && !cbuid.equals("undefined")){
			pkcbu=Long.parseLong(cbuid);
		}
		if(pkavailable!=null && !pkavailable.equals("") && !pkavailable.equals("undefined")){
			pkavail=Long.parseLong(pkavailable);
		}
		return checkShedShipDt(pkorder,pkcbu,pkavail,null);
	}
	@SuppressWarnings("unused")
	public static String checkShedShipDt(Long orderid,Long cbuid,Long pkavailable,Session session){
		String returnval=null;
		String shedShipDt=null;
		List lst= new ArrayList();
		try{
			
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			/*log.debug("\n cbuId "+cbuid+
					"\n orderid "+orderid+
					"\n pkavailable"+pkavailable);*/
			String qry="select ship.SHIPMENT_TIME from cb_shipment ship where ship.fk_order_id="+orderid;
			lst=session.createSQLQuery(qry).list();
			
			if(lst.size()>0){
				if(lst.get(0)!=null){
					shedShipDt=lst.get(0).toString();
				}
			}
			//log.debug("After executing select command & value of shipped date: "+shedShipDt);
			if(shedShipDt==null){
				changeStatus(orderid,cbuid,pkavailable);
				returnval= shedShipDt;
			}
			else{
				returnval= shedShipDt;
			}
		}
		catch (Exception e) {
			// TODO: handle exception
			//log.debug(e.getMessage());
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		//log.debug("return value from helper class : "+returnval);
		return returnval;
	}
	public static void changeStatus(Long orderid,Long cbuid,Long pkavailable){
		Session session=null;
		try{
			
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			//String qry1="update er_order ord set ord.ORDER_STATUS='"+pkResolved+"',ord.ORDER_REMARK='cancelled' ACCPT_TO_CANCEL_REQ='Y' where ord.PK_ORDER='"+orderid+"'";
			String qry1="update cb_cord cord set cord.FK_CORD_CBU_STATUS='"+pkavailable+"' where cord.PK_CORD='"+cbuid+"'";
			SQLQuery sqry1=session.createSQLQuery(qry1);
			//SQLQuery sqry2=session.createSQLQuery(qry2);
			int a1=sqry1.executeUpdate();
			//int a2=sqry2.executeUpdate();
			session.getTransaction().commit();
		}
		catch (Exception e) {
			// TODO: handle exception
			//log.debug(e.getMessage());
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
public static List getallOrderDetails(PaginateSearch paginateSearch,int i,String filterflag,String ordertypeval,String fksiteId,Long userId,String condition,String orderBy){
		
		return getallOrderDetails(paginateSearch,null,i,filterflag,ordertypeval,fksiteId,userId,condition,orderBy);
}	
public static List getallOrderDetails(PaginateSearch paginateSearch,Session session,int i,String filterflag,String ordertypeval,String fksiteId,Long userId,String condition,String orderBy){
	List lst=new ArrayList();
	try{
		if(session==null){
			session=HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
		}
		String qry="";
		String resultSet="";
		//log.debug("\n\n\n\n\nstarting time in action..."+new Date());
		if(filterflag.equals("0")){
			
			if(i==0 || i==1){
				resultSet= " site.site_id as cbbid,adr.add_city as cbbsatellite,cord.cord_registry_id as Registryid,cord.cord_local_cbu_id localid,CASE WHEN (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = ord.order_type)='"+VelosMidConstants.CT_ORDER+"' THEN decode(ord.order_sample_at_lab,'Y','"+VelosMidConstants.CT_LAB_ORDER+"',"+
			                "'N','"+VelosMidConstants.CT_SHIP_ORDER+"') WHEN (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = ord.order_type)!='"+VelosMidConstants.CT_ORDER+"' THEN  (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = ord.order_type) END ordertype, to_char(ordhdr.order_open_date,'Mon DD, YYYY') as requestDt," +
							"to_date(to_char(sysdate,'Mon DD, YYYY'),'Mon DD, YYYY')-to_date(to_char(ordhdr.order_open_date,'Mon DD, YYYY'),'Mon DD, YYYY') as numberofdays,ord.order_ack_flag as acknowledgeResult,cbustat.cbu_status_desc as acknowledbyTc, (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = ord.order_status) as status," +
							"nvl(usr.pk_user,'') as assignedto,ord.ORDER_LAST_VIEWED_BY as reviewedby,ord.pk_order as pkorder, cord.pk_cord as pkcord,site.pk_site as pksite, (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = ord.ORDER_PRIORITY) as priority,usr1.usr_logname logname2,cbustat1.cbu_status_desc closereason ";
			}
			if(i==2){
				resultSet=" count(*) totalRec ";
			}
			qry="select "+resultSet+
			" FROM er_order ord INNER JOIN er_order_header ordhdr ON(ord.fk_order_header=ordhdr.pk_order_header) INNER JOIN cb_cord cord ON(ordhdr.order_entityid=cord.pk_cord) LEFT JOIN er_site site ON(site.pk_site=cord.fk_cbb_id) LEFT JOIN er_add adr ON(adr.pk_add = site.fk_peradd) "+
			" LEFT JOIN cb_cbu_status cbustat ON(cbustat.pk_cbu_status=ord.fk_order_resol_by_tc) LEFT JOIN er_user usr ON (usr.pk_user=ord.assigned_to) LEFT OUTER JOIN er_user usr1 ON (usr1.pk_user=ord.ORDER_LAST_VIEWED_BY) LEFT JOIN cb_cbu_status cbustat1 ON(ordhdr.order_close_reason=cbustat1.pk_cbu_status) "+
			" WHERE ORD.ORDER_STATUS <> (SELECT CL.PK_CODELST FROM er_codelst cl WHERE CL.CODELST_TYPE='"+VelosMidConstants.ORDER_STATUS+"' AND CL.CODELST_SUBTYP='"+VelosMidConstants.CLOSED+"') AND cord.fk_cbb_id IN  ( "+fksiteId+" )"+condition;
				
			
			/*" from er_order ord inner join er_order_header ordhdr on(ord.fk_order_header=ordhdr.pk_order_header) inner join cb_cord cord on(ordhdr.order_entityid=cord.pk_cord) left join er_site site on(site.pk_site=cord.fk_cbb_id) left"+
				" join er_add adr on(adr.pk_add = site.fk_peradd) left join cb_cbu_status cbustat on(cbustat.pk_cbu_status=ord.fk_order_resol_by_tc)"+ 
				" left join er_user usr on (usr.pk_user=ord.assigned_to) left outer join er_user usr1 on (usr1.pk_user=ord.ORDER_LAST_VIEWED_BY) LEFT JOIN cb_cbu_status cbustat1 ON(ordhdr.order_close_reason=cbustat1.pk_cbu_status)"+
				"  where ORD.ORDER_STATUS not in ((select CL.PK_CODELST from er_codelst cl where CL.CODELST_TYPE='"+VelosMidConstants.ORDER_STATUS+"' and CL.CODELST_SUBTYP='"+VelosMidConstants.CLOSED+"')) "+
				" and site.pk_site in ( "+fksiteId+" ) and site.fk_codelst_type=(select pk_codelst from er_codelst where codelst_type ='"+VelosMidConstants.CBB_TYPE+"' and codelst_subtyp='"+VelosMidConstants.CBB_SUBTYPE+"') "+condition;*/
			
			if(orderBy!=null && orderBy!=""){
					qry=qry+" ORDER BY "+orderBy;
			}
			
		}
		if(filterflag.equals("1")){
			
			if(i==0 || i==1){
				resultSet= " site.site_id as cbbid,adr.add_city as cbbsatellite,cord.cord_registry_id as Registryid,cord.cord_local_cbu_id localid,CASE WHEN (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = ord.order_type)='"+VelosMidConstants.CT_ORDER+"' THEN decode(ord.order_sample_at_lab,'Y','"+VelosMidConstants.CT_LAB_ORDER+"',"+
				            "'N','"+VelosMidConstants.CT_SHIP_ORDER+"') WHEN (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = ord.order_type)!='"+VelosMidConstants.CT_ORDER+"' THEN (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = ord.order_type) END ordertype, to_char(ordhdr.order_open_date,'Mon DD, YYYY') as requestDt," +
							"to_date(to_char(sysdate,'Mon DD, YYYY'),'Mon DD, YYYY')-to_date(to_char(ordhdr.order_open_date,'Mon DD, YYYY'),'Mon DD, YYYY') as numberofdays,ord.order_ack_flag as acknowledgeResult,cbustat.cbu_status_desc as acknowledbyTc, (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = ord.order_status) as status," +
							"nvl(usr.pk_user,'') as assignedto,ord.ORDER_LAST_VIEWED_BY as reviewedby,ord.pk_order as pkorder, cord.pk_cord as pkcord,site.pk_site as pksite, (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = ord.ORDER_PRIORITY) as priority,usr1.usr_logname logname2,cbustat1.cbu_status_desc closereason";
			}
			if(i==2){
				resultSet=" count(*) totalRec ";
			}
			
			qry="select "+resultSet+
			" FROM er_order ord INNER JOIN er_order_header ordhdr ON(ord.fk_order_header=ordhdr.pk_order_header) INNER JOIN cb_cord cord ON(ordhdr.order_entityid=cord.pk_cord) LEFT JOIN er_site site ON(site.pk_site=cord.fk_cbb_id) LEFT JOIN er_add adr ON(adr.pk_add = site.fk_peradd) "+
			" LEFT JOIN cb_cbu_status cbustat ON(cbustat.pk_cbu_status=ord.fk_order_resol_by_tc) LEFT JOIN er_user usr ON (usr.pk_user=ord.assigned_to) LEFT OUTER JOIN er_user usr1 ON (usr1.pk_user=ord.ORDER_LAST_VIEWED_BY) LEFT JOIN cb_cbu_status cbustat1 ON(ordhdr.order_close_reason=cbustat1.pk_cbu_status) "+
			" WHERE ORD.ORDER_STATUS <> (SELECT CL.PK_CODELST FROM er_codelst cl WHERE CL.CODELST_TYPE='"+VelosMidConstants.ORDER_STATUS+"' AND CL.CODELST_SUBTYP='"+VelosMidConstants.CLOSED+"') AND cord.fk_cbb_id IN  ( "+fksiteId+" )"+condition
			+" ORDER BY (CASE WHEN SITE.PK_SITE in("+fksiteId+") and ORD.ORDER_TYPE="+ordertypeval+" and f_codelst_desc(ORD.ORDER_STATUS)='New' THEN 0 ELSE 1 END)";
			
			
/*			"inner join er_order_header ordhdr on(ord.fk_order_header=ordhdr.pk_order_header) inner join cb_cord cord on(ordhdr.order_entityid=cord.pk_cord) left join er_site site on(site.pk_site=cord.fk_cbb_id) left"+
			" join er_add adr on(adr.pk_add = site.fk_peradd) left join cb_cbu_status cbustat on(cbustat.pk_cbu_status=ord.fk_order_resol_by_tc)"+ 
			" left join er_user usr on (usr.pk_user=ord.assigned_to) left join er_user usr1 on(usr1.pk_user=ord.ORDER_LAST_VIEWED_BY) LEFT JOIN cb_cbu_status cbustat1 ON(ordhdr.order_close_reason=cbustat1.pk_cbu_status)"+
			"  where ORD.ORDER_STATUS not in ((select CL.PK_CODELST from er_codelst cl where CL.CODELST_TYPE='"+VelosMidConstants.ORDER_STATUS+"' and CL.CODELST_SUBTYP='"+VelosMidConstants.CLOSED+"')) "+
			" and site.pk_site in ( "+fksiteId+" ) and site.fk_codelst_type=(select pk_codelst from er_codelst where codelst_type ='"+VelosMidConstants.CBB_TYPE+"' and codelst_subtyp='"+VelosMidConstants.CBB_SUBTYPE+"') " +
					"" +*/
					
			
			if(orderBy!=null && orderBy!=""){
				qry=qry+" ,"+orderBy;
			}
	    }
		
		//log.debug("\n\n\n\nquery in pendingorders...\n\n\n\n"+qry);
		Query query=session.createSQLQuery(qry);
		if(i==1){
			query.setFirstResult(paginateSearch.getiPageNo());
			query.setMaxResults(paginateSearch.getiShowRows());
		}
		lst = query.list();
		//log.debug("\n\n\n\n\nEnding time in action..."+new Date());
	}catch(Exception e){
		log.error(e.getMessage());
		e.printStackTrace();
	}finally {
		if (session.isOpen()) {
			session.close();
		}
	}
	return lst;
}
	

	public static List getCbbProfile(String siteId,Long userId) {
		return getCbbProfile(siteId,userId,null);
	}
	
	public static List getCbbProfile(String siteId,Long userId,Session session){
		List profList = new ArrayList();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="";
			if(siteId!=null && !siteId.equals("")){
				System.out.println("Site Id Numeric:::"+siteId.matches("[0-9]+")+":::SiteId Value:::"+siteId);
				if (siteId.matches("[0-9]+")){
					qry="SELECT case when f_codelst_desc(ab.order_type)='"+VelosMidConstants.CT_ORDER+"' THEN DECODE(AB.SAMPLEATLAB,'Y','"+VelosMidConstants.CT_LAB_ORDER+"','N','"+VelosMidConstants.CT_SHIP_ORDER+"') when f_codelst_desc(ab.order_type)!='"+VelosMidConstants.CT_ORDER+"' THEN f_codelst_desc(ab.order_type) END  order_type,ab.totalorders totalorders,"+
							" CASE WHEN AB.SAMPLEATLAB = '0' then NVL(f_get_new_orders_count("+Long.valueOf(siteId)+",ab.order_type,'N','Site'),0) else NVL(f_get_new_orders_count("+Long.valueOf(siteId)+",ab.order_type,AB.SAMPLEATLAB,'Site'),0) end neworders,AB.SAMPLEATLAB SAMPLE_AT_LAB,f_codelst_desc(ab.order_type) genric_ord_typ FROM (SELECT order_type,"+
							" COUNT(order_type) totalorders,CASE WHEN f_codelst_desc(ord.order_type)='CT' THEN NVL(ord.order_sample_at_lab,'0') WHEN f_codelst_desc(ord.order_type)!='CT' THEN '0' END SAMPLEATLAB FROM er_order ord,er_order_header ordhdr,cb_cord crd WHERE ord.order_status not in (select CL.PK_CODELST from er_codelst cl where CL.CODELST_TYPE='order_status' and CL.CODELST_SUBTYP='close_ordr') and ord.fk_order_header=ordhdr.pk_order_header and crd.fk_cbb_id IN ("+siteId+")"+
							" AND ordhdr.order_entityid=crd.pk_cord GROUP BY order_type,CASE WHEN f_codelst_desc(ord.order_type)='CT' THEN NVL(ord.order_sample_at_lab,'0') WHEN f_codelst_desc(ord.order_type)!='CT' THEN '0' END) ab ORDER BY f_codelst_desc(ab.order_type)";
				}
				else{
					qry="SELECT case when f_codelst_desc(ab.order_type)='"+VelosMidConstants.CT_ORDER+"' THEN DECODE(AB.SAMPLEATLAB,'Y','"+VelosMidConstants.CT_LAB_ORDER+"','N','"+VelosMidConstants.CT_SHIP_ORDER+"') when f_codelst_desc(ab.order_type)!='"+VelosMidConstants.CT_ORDER+"' THEN f_codelst_desc(ab.order_type) END  order_type,ab.totalorders totalorders,"+
							" CASE WHEN AB.SAMPLEATLAB = '0' then NVL(f_get_new_orders_count("+userId+",ab.order_type,'N','All'),0) else NVL(f_get_new_orders_count("+userId+",ab.order_type,AB.SAMPLEATLAB,'All'),0) end neworders,AB.SAMPLEATLAB SAMPLE_AT_LAB,f_codelst_desc(ab.order_type) genric_ord_typ FROM (SELECT order_type,"+
							" COUNT(order_type) totalorders,CASE WHEN f_codelst_desc(ord.order_type)='CT' THEN NVL(ord.order_sample_at_lab,'0') WHEN f_codelst_desc(ord.order_type)!='CT' THEN '0' END SAMPLEATLAB FROM er_order ord,er_order_header ordhdr,cb_cord crd WHERE ord.order_status not in (select CL.PK_CODELST from er_codelst cl where CL.CODELST_TYPE='order_status' and CL.CODELST_SUBTYP='close_ordr') and ord.fk_order_header=ordhdr.pk_order_header and crd.fk_cbb_id IN ("+siteId+")"+
							" AND ordhdr.order_entityid=crd.pk_cord GROUP BY order_type,CASE WHEN f_codelst_desc(ord.order_type)='CT' THEN NVL(ord.order_sample_at_lab,'0') WHEN f_codelst_desc(ord.order_type)!='CT' THEN '0' END) ab ORDER BY f_codelst_desc(ab.order_type)";
				}
			}else{
				qry="select f_codelst_desc(ab.order_type) order_type,ab.totalorders totalorders,NVL(f_get_new_orders_count("+userId+",ab.order_type,'','User'),0) neworders from "+
				     "(select order_type,count(order_type) totalorders from er_order ord left outer join er_order_header ordhdr "+
				     " on(ordhdr.pk_order_header=ord.fk_order_header)left outer join cb_cord crd on (ordhdr.order_entityid=crd.pk_cord) "+
				     " left outer join er_site site on  (ordhdr.fk_site_id=site.pk_site ) where site.pk_site in (select us.fk_site From er_usersite us,cbb cb where cb.fk_site=us.fk_site and us.fk_user='"+userId+"' and us.usersite_right>0 and cb.using_cdr=1) and site.fk_codelst_type in (select pk_codelst from "+
				     " er_codelst where codelst_type ='"+VelosMidConstants.CBB_TYPE+"' and codelst_subtyp='"+VelosMidConstants.CBB_SUBTYPE+"') group by order_type) ab order by f_codelst_desc(ab.order_type)";

			}
			//log.debug("\n\n\nLanding page CBB Summary Data::"+qry);
			profList = session.createSQLQuery(qry).list();
			
		}
		catch(Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return profList;
	}
	
	public static List getOrders(PaginateSearch paginateSearch,int i,Long userId,String siteId,String condition,String orderByCondition) {
		return getOrders(paginateSearch,null,i,userId,siteId,condition,orderByCondition);
	}
	
	public static List getOrders(PaginateSearch paginateSearch, Session session, int i, Long userId,String siteId,String condition,String orderByCondition){
		List ordList = new ArrayList();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			
			String qry="";
			String columsGetting="";
			
			if(i==0 || i==1){
				columsGetting=" nvl(crd.cord_registry_id,'Not Available') cbu_registry_id,CASE WHEN (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = ord.order_type)='"+VelosMidConstants.CT_ORDER+"' THEN decode(ord.order_sample_at_lab,'Y','"+VelosMidConstants.CT_LAB_ORDER+"','N','"+VelosMidConstants.CT_SHIP_ORDER+"') WHEN (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = ord.order_type)!='"+VelosMidConstants.CT_ORDER+"' THEN  (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = ord.order_type) END order_type,"+
                "to_char(ordhdr.order_open_date,'Mon DD, YYYY') request_date, (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = ord.order_status) status,nvl(usr.usr_logname,'Unassigned') assigned_to,ord.pk_order,crd.pk_cord,crd.fk_cbb_id,ord.order_type order_type1,usr.pk_user pk_user1,ord.order_status order_status1 ";
			}
			if(i==2){
				columsGetting=" count(*) totalval";
			}
			
			
			if(siteId==null || siteId.equals("")){
				qry="select "+columsGetting+
				" from er_order ord left outer join er_order_header ordhdr on (ord.fk_order_header=ordhdr.pk_order_header) left outer join cb_cord crd on (ordhdr.order_entityid=crd.pk_cord) left outer join er_user usr on(ord.assigned_to=usr.pk_user) left outer join er_site site on (site.pk_site=crd.fk_cbb_id) "+
				" where site.pk_site in ( select us.fk_site From er_usersite us,cbb cb where cb.fk_site=us.fk_site and us.fk_user="+userId+" AND us.usersite_right<>0 and us.usersite_right>0 and cb.using_cdr=1) and site.fk_codelst_type=(select pk_codelst from er_codelst where codelst_type ='"+VelosMidConstants.CBB_TYPE+"' and codelst_subtyp='"+VelosMidConstants.CBB_SUBTYPE+"') "+condition+orderByCondition;		
				
			}else{
				qry="select "+columsGetting+
				/*" from er_order ord left outer join er_order_header ordhdr on (ord.fk_order_header=ordhdr.pk_order_header) left outer join cb_cord crd on (ordhdr.order_entityid=crd.pk_cord) left outer join er_user usr on(ord.assigned_to=usr.pk_user) left outer join er_site site on (site.pk_site=crd.fk_cbb_id) "+
				" where site.pk_site in ("+siteId+") and site.fk_codelst_type=(select pk_codelst from er_codelst where codelst_type ='"+VelosMidConstants.CBB_TYPE+"' and codelst_subtyp='"+VelosMidConstants.CBB_SUBTYPE+"') "+condition+orderByCondition;// order by f_codelst_desc(ord.order_type),f_codelst_desc(ord.order_status)";
				*/
				" FROM er_order ord left outer join er_order_header ordhdr ON ( ord.fk_order_header =ordhdr.pk_order_header ) left outer join cb_cord crd ON ( crd.pk_cord =ordhdr.order_entityid ) left outer join er_user usr ON( usr.pk_user=ord.assigned_to ) WHERE crd.fk_cbb_id IN ("+siteId+")"+condition+orderByCondition;
				
				
			}
			
			//log.debug("Landing page orders\n \n \n \n \n \n \n \n \n \n \nquery is:::"+qry);
			Query query = session.createSQLQuery(qry);
			if(i==1){
				//log.debug("\n \n \n \n \n \n \n \n \n \n In helper ipage no iShowRows():::"+paginateSearch.getiPageNo()+"::"+paginateSearch.getiShowRows());
				query.setFirstResult(paginateSearch.getiPageNo());
				query.setMaxResults(paginateSearch.getiShowRows());
			}
			
			ordList = query.list();
			
		}
		catch(Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return ordList;
	}
	
	public static List getTasksListForOrder(Long orderId){
		//log.debug("helper pending order");
		return getTasksListForOrder(orderId,null);
	}
	public static List getTasksListForOrder(Long orderId,Session session){
		List tasksList=new ArrayList<TaskListPojo>();
		//log.debug("in helper classsssssssssssssssss get task list::::::");
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			
			/*"select f_codelst_desc(ord.fk_order_type) order_type,ord.order_number,crd.cord_registry_id,"+
			"to_char(ord.order_date,'Mon DD, YYYY'),f_codelst_desc(ord.fk_order_status) order_status,ac.activity_desc,tsk.task_completeflag ,ord.pk_order,ord.order_entityid,ac.activity_name"+
			" from task tsk,workflow_instance wi,workflow_activity wa,activity ac,er_order ord,cb_cord crd,er_codelst stat where tsk.fk_wfinstanceid=wi.pk_wfinstanceid"+
			" and tsk.fk_workflowactivity=wa.pk_workflowactivity and wa.fk_activityid=ac.pk_activity and tsk.fk_entity=ord.pk_order and "+
			"ord.order_entityid=crd.pk_cord and ord.fk_order_status=stat.pk_codelst and stat.codelst_type='"+VelosMidConstants.ORDER_STATUS+
			"' and stat.codelst_subtyp not in('"+VelosMidConstants.CLOSED+"','"+VelosMidConstants.RESOLVED+"') and ord.pk_order="+orderId+" order by ord.order_number,wa.wa_sequence"*/
			
			
			String qry=" select  (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = ord.order_type) order_type,ordhdr.order_num,crd.cord_registry_id,to_char(ordhdr.order_open_date,'Mon DD, YYYY'), (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = ord.order_status) order_status,ac.activity_desc," +
					"tsk.task_completeflag ,ord.pk_order,ordhdr.order_entityid,ac.activity_name from task tsk,workflow_instance wi,workflow_activity wa,activity ac,er_order ord,cb_cord crd,er_codelst stat,er_order_header ordhdr" +
					" where tsk.fk_wfinstanceid=wi.pk_wfinstanceid and tsk.fk_workflowactivity=wa.pk_workflowactivity and wa.fk_activityid=ac.pk_activity and ord.fk_order_header=ordhdr.pk_order_header and tsk.fk_entity=ord.pk_order" +
					" and ordhdr.order_entityid=crd.pk_cord and ord.order_status=stat.pk_codelst and ord.pk_order="+orderId+" order by ord.pk_order,ord.order_type,wa.wa_sequence";
					
			tasksList=session.createSQLQuery(qry).list();
			//log.debug("lsit size is::::"+tasksList.size());
		}
		catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return tasksList;
	}
	
	
	public static List getShipmentInfo(Long orderId,Long shipmentType){
		return getShipmentInfo(orderId,shipmentType,null);
	}
	
	public static List getShipmentInfo(Long orderId,Long shipmentType,Session session){
		List lst=new ArrayList();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="";
			
			qry="SELECT TO_CHAR(sh.sch_shipment_date,'Mon DD, YYYY') ship_date, " +
			"  TO_CHAR(sh.addi_shipment_date,'Mon DD, YYYY') addi_date, " +
			"  sh.fk_ship_company_id, " +
			"  sh.shipment_tracking_no, " +
			"  sh.addi_shipment_tracking_no, " +
			"  ord.fk_sample_type_avail, " +
			"  ord.fk_aliquots_type, " +
			"  sh.fk_shipping_cont, " +
			"  sh.fk_temperature_moniter, " +
			"  NVL(TO_CHAR(sh.shipment_date,'Mon DD, YYYY'),'Not Provided'), " +
			"  NVL(TO_CHAR(sh.expect_arr_date,'Mon DD, YYYY'),'Not Provided'), " +
			"  ord.SHIPMENT_SCH_FLAG, " +
			"  ord.CORD_SHIPPED_FLAG, " +
			"  ord.PACKAGE_SLIP_FLAG, " +
			"  usr.usr_firstname " +
			"  ||' ' " +
			"  ||usr.usr_lastname username, " +
			"  TO_CHAR(SHIPMENT_CONFIRM_ON,'Mon DD, YYYY HH:MI AM') shipment_confirm_on, " +
			"  SCH_SHIPMENT_TIME shipment_tm, " +
			"  to_date(TO_CHAR(sh.shipment_date,'Mon DD, YYYY'),'Mon DD, YYYY')-to_date(TO_CHAR(ordhdr.order_open_date,'Mon DD, YYYY'),'Mon DD, YYYY') noofdays, " +
			"  sh.PAPERWORK_LOCATION, " +
			"  sh.ADDITI_SHIPPER_DET, " +
			"  PADLOCK_COMBINATION, " +
			"  ord.lab_code, " +
			"  to_date(TO_CHAR(sh.sch_shipment_date,'Mon DD, YYYY'),'Mon DD, YYYY')-to_date(TO_CHAR(ordhdr.order_open_date,'Mon DD, YYYY'),'Mon DD, YYYY') noofdaystoct, " +
			"  sh.NMDP_SHIP_EXCUSE_FLAG, " +
			"  sh.DATA_MODIFIED_FLAG, " +
			"  sh.PK_SHIPMENT, " +
			"  pslip.pk_packingslip, " +
			"  NVL(TO_CHAR(sh.prop_shipment_date,'Mon DD,YYYY'),'Not Provided'), " +
			"  NVL(sh.attn_name,'Not Provided'), " +
			"  sh.SHIPMENT_CONFIRM, " +
			"  '' tmp1, " +
			"  sh.sch_shipment_date-sysdate, " +
			"  sh.NMDP_SHIP_EXCUSE_SUBMITTED_BY, " +
			"  TO_CHAR(sh.NMDP_SHIP_EXCUSE_SUBMITTED_ON,'Mon DD, YYYY') " +
			//"  sh.SHIPPING_CONT_OTHR, " +
			//"  sh.TEMPERATURE_MONITER_OTHR " +
			"FROM er_order ord " +
			"INNER JOIN er_order_header ordhdr " +
			"ON(ord.fk_order_header=ordhdr.pk_order_header) " +
			"LEFT OUTER JOIN cb_shipment sh " +
			"ON(sh.fk_order_id=ord.pk_order) " +
			"LEFT OUTER JOIN er_user usr " +
			"ON(sh.shipment_confirm_by=usr.pk_user) " +
			"LEFT OUTER JOIN " +
			"  (SELECT MAX(pk_packing_slip) pk_packingslip, " +
			"    fk_shipment " +
			"  FROM cb_packing_slip " +
			"  GROUP BY fk_shipment " +
			"  ) pslip " +
			"ON(pslip.fk_shipment=sh.pk_shipment) " +
			"WHERE ord.pk_order  ="+orderId+
			" AND fk_shipment_type="+shipmentType;
			log.debug("query is:::"+qry);
			lst=session.createSQLQuery(qry).list();
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		
		return lst;
	}


	public static List getLatestHlaInfo(Long orderId,Long entityId,String entityType){
		return getLatestHlaInfo(orderId,entityId,entityType,null);
	}
	
	public static List getLatestHlaInfo(Long orderId,Long entityId,String entityType,Session session){
		List lst=new ArrayList();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			//log.debug("orderidddddddd"+orderId+"   "+entityId);
			String qry="select f_codelst_desc(hl.fk_hla_code_id) hla_lucas,f_codelst_desc(hl.fk_hla_method_id) hla_method,hl.hla_value_type1,hl.hla_value_type2,hl.created_on,hl.pk_hla " +
					"from cb_hla hl where hl.pk_hla =(select max(pk_hla) from cb_hla where entity_id='"+entityId+"' and ENTITY_TYPE='"+entityType+"')";
			//log.debug("query is:::"+qry);
			lst=session.createSQLQuery(qry).list();
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return lst;
	}
	
	public static List getcordAvailConfirm(Long orderId){
		return getcordAvailConfirm(orderId,null);
	}
	
	public static List getcordAvailConfirm(Long orderId,Session session){
		List lst=new ArrayList();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="select ord.is_cord_avail_for_nmdp,cbustat.cbu_status_desc unavail_rsn,usr.usr_firstname||' '||usr.usr_lastname username,to_char(ord.CORD_AVAILABILITY_CONFORMEDON,'Mon DD, YYYY')," +
					"ord.CBU_AVAIL_CONFIRM_FLAG,to_date(to_char(ord.cord_avail_confirm_date,'Mon DD, YYYY'),'Mon DD, YYYY')-to_date(to_char(ordhdr.order_open_date,'Mon DD, YYYY'),'Mon DD, YYYY') no_of_days from er_order ord " +
					"inner join er_order_header ordhdr on(ord.fk_order_header=ordhdr.pk_order_header) left outer join cb_cord crd on(ordhdr.order_entityid=crd.pk_cord) " +
					"left outer join er_user usr on(ord.CORD_AVAILABILITY_CONFORMEDBY=usr.pk_user) left outer join cb_cbu_status cbustat on(ord.cord_unavail_reason=cbustat.pk_cbu_status) where ord.pk_order='"+orderId+"'";
			//log.debug("query is:::"+qry);
			lst=session.createSQLQuery(qry).list();
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return lst;
	}
	
	
	
	public static List getUserslistGridData(String siteId){
		return getUserslistGridData(siteId,null);
	}
	
	public static List getUserslistGridData(String siteId,Session session){
		List lst=new ArrayList();
		List lst1=new ArrayList();
		int position=getCtrlTabValues(VelosMidConstants.ASSIGNTO_ACCESS_RIGHT);
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="SELECT USR.PK_USER, " +
						"  USRSITE.FK_SITE, " +
						"  USR.USR_LOGNAME " +
						" FROM ER_USER USR "+
						" INNER JOIN ER_USERSITE USRSITE ON(USRSITE.FK_USER=USR.PK_USER) "+
						" INNER JOIN ER_USRSITE_GRP USG ON(USG.FK_USER=USR.PK_USER AND USRSITE.PK_USERSITE=USG.FK_USER_SITE) "+
						" INNER JOIN ER_GRPS GRP ON(USG.FK_GRP_ID=GRP.PK_GRP) "+
						" WHERE USR.USR_STAT='A' "+
						" AND USRSITE.USERSITE_RIGHT<>0 "+
						" AND USRSITE.FK_SITE IN("+siteId+") "+
						" AND SUBSTR(GRP.GRP_RIGHTS,"+position+",1)>=6 "+
						"AND usrsite.usersite_right<>0 " +
						"ORDER BY USR.USR_LOGNAME";
		   //log.debug("\n\n\n\n\nQUERY for userlist grid..."+qry+"\n\n\n\n");
			
			lst=session.createSQLQuery(qry).list();
			//lst1=getAssignedOrdersUserList(lst,"1");
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return lst;
	}
	
	public static List getUserslistLabel(String siteId){
		return getUserslistLabel(siteId,null);
	}
	
	@SuppressWarnings("unchecked")
	public static List getUserslistLabel(String siteId,Session session){
		List lst=new ArrayList();
		List<Long> lst1=new ArrayList<Long>();
		int position=getCtrlTabValues(VelosMidConstants.ASSIGNTO_ACCESS_RIGHT);
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
				
			String qry="SELECT "+
						  " DISTINCT USR.PK_USER, "+
						  " USR.fk_siteid, "+
						  " USR.USR_LOGNAME "+
						" FROM ER_USER USR "+
						" INNER JOIN ER_USERSITE USRSITE ON(USRSITE.FK_USER=USR.PK_USER) "+
						" INNER JOIN ER_USRSITE_GRP USG ON(USG.FK_USER=USR.PK_USER AND USRSITE.PK_USERSITE=USG.FK_USER_SITE) "+
						" INNER JOIN ER_GRPS GRP ON(USG.FK_GRP_ID=GRP.PK_GRP) "+
						" WHERE USR.USR_STAT='A' "+
						" AND USRSITE.USERSITE_RIGHT<>0 "+
						" AND USRSITE.FK_SITE IN("+siteId+") "+
						" AND SUBSTR(GRP.GRP_RIGHTS,"+position+",1)>=6 "+
						" ORDER BY USR.USR_LOGNAME ";
					
			
		    //log.debug("\n\n\n\n\nQUERY for userlist label ..."+qry+"\n\n\n\n");
			
			lst=session.createSQLQuery(qry).list();
			
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return lst;
	}
	
	public static int getCtrlTabValues(String contrlkey){
		return getCtrlTabValues(contrlkey,null);
	}
	
	public static int getCtrlTabValues(String contrlkey,Session session){
		
		List temp=new ArrayList();
		int postion=0;
		String contrlkeyInlst="";
		
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			
			String qry="select ctrl_value from er_ctrltab  where ctrl_key='app_rights' order by ctrl_seq";
			temp=session.createSQLQuery(qry).list();
			Object obj=new Object();
			
			if(temp!=null && temp.size()>0){
				
				for(int i=0;i<temp.size();i++){
					
					obj=temp.get(i);
					contrlkeyInlst=obj.toString();
					if(contrlkeyInlst.equals(contrlkey)){
						postion=i+1;
						//log.debug("position in the loop:::::::::::"+postion);
						break;
					}
					
				}
				//log.debug("\n\n\n\n\n\nPosition for the String::::::"+postion+"\n\n\n\n\n\n");
			 }
		
			
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		
		return postion;
	}
	
	@SuppressWarnings("rawtypes")
	public static List getCommonUsrLst(String site, String count){
		return getCommonUsrLst(site, count,null);
	}
	public static List getCommonUsrLst(String site, String count,Session session){
		
		List lst=new ArrayList();
		List lst1=new ArrayList();
		int position=getCtrlTabValues(VelosMidConstants.ASSIGNTO_ACCESS_RIGHT);
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="SELECT usr.pk_user, " +
						"  usr.fk_siteid, " +
						"  usr.usr_logname " +
						"FROM er_user usr " +
						"WHERE pk_user IN " +
						"  (SELECT USR.PK_USER " +
						" FROM ER_USER USR "+
						" INNER JOIN ER_USERSITE USRSITE ON(USRSITE.FK_USER=USR.PK_USER) "+
						" INNER JOIN ER_USRSITE_GRP USG ON(USG.FK_USER=USR.PK_USER AND USRSITE.PK_USERSITE=USG.FK_USER_SITE) "+
						" INNER JOIN ER_GRPS GRP ON(USG.FK_GRP_ID=GRP.PK_GRP) "+
						" WHERE USR.USR_STAT='A' "+
						" AND USRSITE.USERSITE_RIGHT<>0 "+
						" AND USRSITE.FK_SITE IN("+site+") "+
						" AND SUBSTR(GRP.GRP_RIGHTS,"+position+",1)>=6 "+
						"  GROUP BY usr.pk_user " +
						"  HAVING COUNT(usr.pk_user)=("+count+") "+ 
						"  ) " +
						"ORDER BY USR.USR_LOGNAME "; 

			log.debug("\n\n\n\n\nquery is for common list user..................."+qry+"\n\n\n\n");
			lst=session.createSQLQuery(qry).list();
			
			//lst1=getAssignedOrdersUserList(lst,"1");
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return lst;
		
	}
	
	public static List getOrderResol(Long orderId){
		return getOrderResol(orderId,null);
	}
	
	public static List getOrderResol(Long orderId,Session session){
		List lst=new ArrayList();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="select  ord.fk_order_resol_by_cbb,  ord.fk_order_resol_by_tc,  to_char(ord.order_resol_date,'Mon DD, YYYY'),'',to_char(ord.cancel_conform_date,'Mon DD, YYYY'),to_char(crd.cord_avail_date,'Mon DD, YYYY') as availabledate,"+
						"to_char(ord.order_ack_date,'Mon DD, YYYY HH24:MI') as acknowlededate,ord.order_ack_by as acknowledgeby,crd.fk_cord_cbu_status as fkcordstatus,ord.ORDER_ACK_FLAG,ordhdr.ORDER_CLOSE_REASON from er_order ord left outer join er_order_header ordhdr on(ord.fk_order_header=ordhdr.pk_order_header)"+
					    " left outer join cb_cord crd on(crd.pk_cord=ordhdr.order_entityid) where ord.pk_order="+orderId;
			//log.debug("query is:::"+qry);
			lst=session.createSQLQuery(qry).list();
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return lst;
	}
	
	public static List savedInprogress(PaginateSearch paginationSearch,String SIPCondition,Long userId,String fksiteId,String orderBy,int i){
		return savedInprogress(paginationSearch,SIPCondition,userId,fksiteId,orderBy,i,null);
	}
	public static List savedInprogress(PaginateSearch paginationSearch,String SIPCondition,Long userId,String fksiteId,String orderBy,int i,Session session){
		List cordentryprogress=new ArrayList();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			
			String resultSet="";
			
			if(i==0 || i==1){
				resultSet=" cord.cord_registry_id as registryid,cord.cord_local_cbu_id as localcbuid,to_char(cord.created_on,'Mon DD, YYYY') as startedon,"+
						"to_date(to_char(sysdate,'Mon DD, YYYY'),'Mon DD, YYYY')-to_date(to_char(cord.created_on,'Mon DD, YYYY'),'Mon DD, YYYY') as noofdays,cord.cord_entry_progress as percompleted,cord.pk_cord as pkcordid,site.site_id as cbbid,site.pk_site as pksite,"+
						"cord.created_on ";
			}
			if(i==2){
				resultSet=" count(*) resultSet ";
			}
			
			
			String qry="select "+resultSet+" from cb_cord cord left outer join er_site site on(cord.fk_cbb_id=site.pk_site) left join er_usersite usersite on site.PK_SITE=usersite.FK_SITE where cord_searchable='0' and cord.fk_cbb_id in ("+fksiteId +") and usersite.fk_user in ("+userId+") and site.FK_CODELST_TYPE in(SELECT pk_codelst FROM er_codelst WHERE lower(codelst_type)  ='site_type' AND lower(codelst_subtyp)='cbb') "+SIPCondition+orderBy;
			
			//log.debug("\n\n\n\n\n\n\n\n\n\n\n\n\n SIPqry"+qry);

			Query query = session.createSQLQuery(qry);
			if(i==1){
				//log.debug("\n\n\n\n\n\n\n\n\n\n\n\n\n paginationSearch.getiPageNo()"+paginationSearch.getiPageNo()+"paginationSearch.getiShowRows()"+paginationSearch.getiShowRows());
				query.setFirstResult(paginationSearch.getiPageNo());
				query.setMaxResults(paginationSearch.getiShowRows());
			}
			cordentryprogress = query.list();
			//log.debug("Inside OtherRequest Helper & Size of CordEntryProgressList:::;"+cordentryprogress.size());
		}catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen())
				session.close();
		}
		return cordentryprogress;
	}
	public static List notAvailCbu(PaginateSearch paginationSearch,String nacbuCondition,int i,String fksiteId,String nacbuOrderBy,Long userId){
		return notAvailCbu(paginationSearch,nacbuCondition,i,null,fksiteId,nacbuOrderBy,userId);
	}
	public static List notAvailCbu(PaginateSearch paginationSearch,String nacbuCondition,int i,Session session,String fksiteId,String nacbuOrderBy,Long userId){
		List naCbu=new ArrayList();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			
			String resultSet="";
			
			if(i==0 || i==1){
				resultSet=" site.site_id as cbbid,cord.cord_registry_id as registryId,cord.cord_local_cbu_id as localcbuid,nmdpstat.cbu_status_desc as status,"+
							"loclstat.cbu_status_desc as reason,usr.usr_logname as removedby,cord.pk_cord pkcordid,cord.fk_cbb_id as pksiteid,cord.LAST_MODIFIED_BY,"+
							"cord.cord_nmdp_status,cord.FK_CORD_CBU_STATUS,cord.CORD_SEARCHABLE ";
			}
			if(i==2){
				resultSet=" count(*) recCount";
			}
			
			String qry="select "+resultSet+" from cb_cord cord left outer join er_site site on(cord.fk_cbb_id=site.pk_site) left outer join cb_cbu_status nmdpstat"+
						" on(cord.cord_nmdp_status=nmdpstat.pk_cbu_status)left outer join cb_cbu_status loclstat on(cord.FK_CORD_CBU_STATUS=loclstat.pk_cbu_status)"+
						"left outer join er_user usr on(cord.LAST_MODIFIED_BY=usr.pk_user) left join er_usersite usersite on site.PK_SITE=usersite.FK_SITE where cord.fk_cord_cbu_status in(select pk_cbu_status from cb_cbu_status where IS_AUTOCOMPLETE_STATUS=1 AND cbu_status_code NOT IN ('DS','LS','TS','RO') and (deletedflag IS NULL OR deletedflag=0)) and cord.fk_cbb_id in ("+fksiteId+") and usersite.fk_user in ("+userId+") and site.FK_CODELST_TYPE in(SELECT pk_codelst FROM er_codelst WHERE lower(codelst_type)  ='site_type' AND lower(codelst_subtyp)='cbb')"+nacbuCondition+nacbuOrderBy;
			
			//log.debug("\n\n\n\n\n\n\n\n\n\n\n\n\nNACBU qry"+qry);
			
			Query query=session.createSQLQuery(qry);
			
			if(i==1){
				//log.debug("paginationSearch.getiPageNo()"+paginationSearch.getiPageNo()+":paginationSearch.getiShowRows():"+paginationSearch.getiShowRows());
				query.setFirstResult(paginationSearch.getiPageNo());
				query.setMaxResults(paginationSearch.getiShowRows());
			}
			naCbu=query.list();
			
		}catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen())
				session.close();
		}
		return naCbu;
	}
	
	public static List maincbuRequestHistory(String pkcord,String pkorderId){
		Long cordId=0l;
		Long orderId=0l;
		if(pkcord!=null && !pkcord.equals("") && !pkcord.equals("undefined")){
			cordId=Long.parseLong(pkcord);
		}
		if(pkorderId!=null){
			pkorderId = pkorderId.trim();
			if(!pkorderId.equals("") && !pkorderId.equals("undefined")){
				orderId=Long.parseLong(pkorderId);
			}
		}
		return maincbuRequestHistory(cordId,orderId,null);
	}
	public static List maincbuRequestHistory(Long pkcord,Long pkorderId,Session session){
		List maincbureqhistory=new ArrayList();
		String sql="";
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			//if(pkorderId==null || pkorderId.equals("")){
			sql="SELECT TO_CHAR(ordhdr.order_open_date,'Mon DD, YYYY') AS Requestdate, " +
			"  CASE WHEN f_codelst_desc(ord.order_type)='"+VelosMidConstants.CT_ORDER+"' THEN decode(ord.order_sample_at_lab,'Y','"+VelosMidConstants.CT_LAB_ORDER+"','N','"+VelosMidConstants.CT_SHIP_ORDER+"') WHEN f_codelst_desc(ord.order_type)!='"+VelosMidConstants.CT_ORDER+"' THEN f_codelst_desc(ord.order_type) END  AS requestype, " +
			"  cbustat.CBU_STATUS_DESC, " +
			"  reci.receipant_id    AS patientid, " +
			"  site.site_id AS transplantcenter, " +
			"  reci.pk_receipant    AS pk_receipant, " +
			"  ord.pk_order         AS pkorder, " +
			"  crd.pk_cord          AS pkcord, " +
			"  ordhdr.fk_site_id siteid " +
			"FROM er_order ord " +
			"LEFT OUTER JOIN er_order_header ordhdr " +
			"ON (ord.fk_order_header=ordhdr.pk_order_header) " +
			"LEFT OUTER JOIN cb_cord crd " +
			"ON(ordhdr.order_entityid=crd.pk_cord) " +
			"LEFT OUTER JOIN cb_cbu_status cbustat " +
			"ON(ord.fk_order_resol_by_cbb=cbustat.pk_cbu_status " +
			"OR ord.fk_order_resol_by_tc =cbustat.pk_cbu_status) " +
			"LEFT OUTER JOIN " +
			"  (SELECT RECEIPANT_ID, " +
			"    TRANS_CENTER_ID, " +
			"    PK_RECEIPANT, " +
			"    FK_ORDER_ID " +
			"  FROM cb_receipant_info reci, " +
			"    ER_ORDER_RECEIPANT_INFO PRE " +
			"  WHERE PRE.FK_RECEIPANT=RECI.PK_RECEIPANT " +
			"  ) reci " +
			"ON(reci.fk_order_id=ord.pk_order) " +
			"LEFT OUTER JOIN er_site site ON (reci.TRANS_CENTER_ID=site.pk_site) "+
			"WHERE crd.pk_cord  ="+pkcord+
			"ORDER BY ordhdr.order_open_date DESC";
			
			//log.debug("\n\n\n\n\n\n\nsql in main req history:::"+sql);
			maincbureqhistory=session.createSQLQuery(sql).list();
			
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			// TODO: handle exception
		}finally{
			if(session.isOpen())
				session.close();
		}
		return maincbureqhistory;
	}
	

	public static List cbuRequestHistory(String pkcord,String pkorder,String cdrUser){
		return cbuRequestHistory(pkcord,pkorder,cdrUser,null);
	}
	public static List cbuRequestHistory(String pkcord,String pkorder,String cdrUser,Session session){
		List cbureqhistory=new ArrayList();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String inval="";
			String taskStatusCode="";
			String ordersCode="";
			String nwCondi="";
			if(cdrUser!=null && !cdrUser.equals("") && !cdrUser.equals("undefined") && cdrUser.equals("1")){
				inval="'"+VelosMidConstants.ORDER_STATUS+"','"+VelosMidConstants.LICENSE+"','"+VelosMidConstants.ELIGIBLE+"','"+VelosMidConstants.SWITCH+"'";
				taskStatusCode="      WHEN STAT.STATUS_TYPE_CODE IN ('"+VelosMidConstants.TASK_STATUS+"','"+VelosMidConstants.TASK_RESET+"' )" +
				  "      THEN " +
				  "        (SELECT ACTIVITY_DESC " +
				  "        FROM ACTIVITY " +
				  "        WHERE PK_ACTIVITY IN (STAT.FK_STATUS_VALUE) " +
				  "        ) ";
				ordersCode="  OR (stat.entity_id     =" +pkorder+
				  "  AND stat.fk_entity_type= " +
				  "    (SELECT c.pk_codelst " +
				  "    FROM er_codelst c " +
				  "    WHERE c.codelst_type='"+VelosMidConstants.ENTITY_CBU_CODE_TYPE+"' " +
				  "    AND c.codelst_subtyp='"+VelosMidConstants.ENTITY_ORDER_CODE_SUB_TYPE+"' " +
				  "    )) " +
				  "  OR (stat.entity_id= " +
				  "    (SELECT fk_order_header FROM er_order WHERE pk_order='"+pkorder+"' " +
				  "    ) " +
				  "  AND stat.fk_entity_type= " +
				  "    (SELECT c.pk_codelst " +
				  "    FROM er_codelst c " +
				  "    WHERE c.codelst_type='"+VelosMidConstants.ENTITY_CBU_CODE_TYPE+"' " +
				  "    AND c.codelst_subtyp='"+VelosMidConstants.ENTITY_ORDHDR_CODE_SUB_TYPE+"' " +
				  "    )) ";
				
				nwCondi=" and stat.order_id="+pkorder;
				
			}else if(cdrUser!=null && cdrUser.equals("") || cdrUser.equals("0")){
				inval="'"+VelosMidConstants.LICENSE+"','"+VelosMidConstants.ELIGIBLE+"'";
			}
		  
		  String sql="SELECT * " +
		  "FROM " +
		  "  (SELECT TO_CHAR(stat.STATUS_DATE,'Mon DD, YYYY') statDate, " +
		  "    status.CBU_STATUS_DESC AS statusvalue, " +
		  "    CASE " +
		  "      WHEN STAT.CREATOR=0 " +
		  "      THEN 'System' " +
		  "      WHEN STAT.CREATOR!=0 " +
		  "      THEN F_GETUSER(STAT.CREATOR) " +
		  "    END userval, " +
		  "    stat.status_type_code typecode, " +
		  "    TO_CHAR(sysdate,'Mon DD, YYYY') systemdate, " +
		  "    CASE " +
		  "      WHEN STAT.STATUS_TYPE_CODE IN ("+inval+" ) " +
		  "      THEN f_codelst_desc(STAT.FK_STATUS_VALUE) " +taskStatusCode+
		  "    END statDesc, " +
		  "    stat.STATUS_DATE DATEVA," +
		  "    STAT.PK_ENTITY_STATUS VAL2, " +
		  "    STATUS1.CBU_STATUS_CODE CODE, " +
		  "    STAT.FK_STATUS_VALUE value, "+
		  "    STAT.ADD_OR_UPDATE value2 "+
		  "  FROM cb_entity_status stat " +
		  "  LEFT OUTER JOIN cb_cbu_status status " +
		  "  ON(stat.fk_status_value=status.PK_CBU_STATUS) " +
		  "  LEFT OUTER JOIN cb_cbu_status status1 "+
		  "  ON(stat.order_fk_resol=status1.PK_CBU_STATUS) "+
		  "  WHERE (stat.ENTITY_ID  =" +pkcord+nwCondi+ 
		  "  AND stat.fk_entity_type= " +
		  "    (SELECT c.pk_codelst " +
		  "    FROM er_codelst c " +
		  "    WHERE c.codelst_type='"+VelosMidConstants.ENTITY_CBU_CODE_TYPE+"' " +
		  "    AND c.codelst_subtyp='"+VelosMidConstants.ENTITY_CBU_CODE_SUB_TYPE+"' " +
		  "    )) " +ordersCode+
		  /*"  UNION ALL " +
		  "  SELECT TO_CHAR(FRMVR.CREATED_ON,'Mon DD, YYYY'), " +
		  "    FRM.FORMS_DESC statval, " +
		  "    F_GETUSER(FRMVR.CREATOR) userval, " +
		  "    '"+VelosMidConstants.FORMS_CODE+"' FRMNAME, " +
		  "    TO_CHAR(sysdate,'Mon DD, YYYY') systemdate, " +
		  "    '' desc7, " +
		  "    FRMVR.CREATED_ON DATE2," +
		  "    FRM.PK_FORM VAL2, " +
		  "    '' CODE, "+
		  "    0 value "+
		  "  FROM CB_FORM_VERSION FRMVR " +
		  "  LEFT OUTER JOIN CB_FORMS FRM " +
		  "  ON (FRMVR.FK_FORM      =FRM.PK_FORM) " +
		  "  WHERE FRMVR.ENTITY_ID  =" +pkcord+
		  "  AND FRMVR.CREATED_ON  IS NOT NULL " +
		  "  AND FRMVR.ENTITY_TYPE IN " +
		  "    (SELECT C.PK_CODELST " +
		  "    FROM ER_CODELST C " +
		  "    WHERE C.CODELST_TYPE='"+VelosMidConstants.MATERNAL_CODE_TYPE+"' " +
		  "    AND C.CODELST_SUBTYP='"+VelosMidConstants.MATERNAL_CODE_SUB_TYPE+"' " +
		  "    ) " +
		  "  ) " +*/
		  " ) ORDER BY 8 DESC,7 DESC";
		  //log.debug("Request history sql \n\n\n\n\n"+sql);
		 
		  cbureqhistory=session.createSQLQuery(sql).list();
			
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			// TODO: handle exception
		}finally{
			if(session.isOpen())
				session.close();
		}
		return cbureqhistory;
	}
	public static List cbuHistory(String pkcord,String pkorder,PaginateSearch paginationSearch,int i,String cdrUser,String condition,String orderBy){
		Long cordId=0l;
		Long orderId=0l;
		if(pkcord!=null && !pkcord.equals("") && !pkcord.equals("undefined")){
			cordId=Long.parseLong(pkcord);
		}
		if(pkorder!=null && !pkorder.equals("") && !pkorder.equals("undefined")){
			orderId=Long.parseLong(pkorder);
		}
		return cbuHistory(cordId,orderId,paginationSearch,i,cdrUser,condition,orderBy,null);
	}
	public static List cbuHistory(Long pkcord,Long pkorder,PaginateSearch paginationSearch,int i,String cdrUser,String condition,String orderby,Session session){
		List cbuHistry=new ArrayList();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String sql="";
			String ordersCode="";
			
			long order_entity_type=getCodelstPk(VelosMidConstants.ENTITY_CBU_CODE_TYPE,VelosMidConstants.ENTITY_ORDER_CODE_SUB_TYPE);
			long cbu_entity_type=getCodelstPk(VelosMidConstants.ENTITY_CBU_CODE_TYPE,VelosMidConstants.ENTITY_CBU_CODE_SUB_TYPE);
			long maternalpk=getCodelstPk(VelosMidConstants.MATERNAL_CODE_TYPE,VelosMidConstants.MATERNAL_CODE_SUB_TYPE);
			
			
			
			if(session==null || !session.isOpen()){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			
			
			
			if(cdrUser!=null && !cdrUser.equals("") && !cdrUser.equals("undefined") && cdrUser.equals("1")){
				ordersCode="UNION ALL " +
				"SELECT TO_CHAR(STAT.STATUS_DATE,'Mon DD, YYYY') AS DATEVAL, " +
				"  CASE " +
				"    WHEN STAT.STATUS_TYPE_CODE IN ('"+VelosMidConstants.TASK_STATUS+"','"+VelosMidConstants.TASK_RESET+"' ) " +
				"    THEN ACT.ACTIVITY_DESC " +
				"  END DESC1, " +
				"  ' "+VelosMidConstants.HISTORY_ORDER_TASK+" ' DESC2, " +
				"  CASE " +
				"    WHEN STAT.STATUS_TYPE_CODE = '"+VelosMidConstants.TASK_STATUS+"'" +
				"    THEN '"+VelosMidConstants.HISTORY_ORDER_TASK_COMPLETED+"' " +
				"    WHEN STAT.STATUS_TYPE_CODE = '"+VelosMidConstants.TASK_RESET+"'" +
				"    THEN '"+VelosMidConstants.HISTORY_ORDER_TASK_RESET+"' " +
				"  END DESC3, " +
				"  CASE " +
				"    WHEN STAT.CREATOR=0 " +
				"    THEN '"+VelosMidConstants.HISTORY_USER_SYSTEM+"' " +
				"    WHEN STAT.CREATOR!=0 " +
				"    THEN F_GETUSER(STAT.CREATOR) " +
				"  END USERNAME, " +
				"  CASE " +
				"    WHEN f_codelst_desc(ord.order_type) IS NULL " +
				"    THEN '-' " +
				"    WHEN f_codelst_desc(ord.order_type) IS NOT NULL " +
				"    AND f_codelst_desc(ord.order_type)   ='"+VelosMidConstants.CT_ORDER+"' " +
				"    THEN DECODE(ord.order_sample_at_lab,'Y','"+VelosMidConstants.CT_LAB_ORDER+"','N','"+VelosMidConstants.CT_SHIP_ORDER+"') " +
				"    WHEN f_codelst_desc(ord.order_type) IS NOT NULL " +
				"    AND f_codelst_desc(ord.order_type)  !='"+VelosMidConstants.CT_ORDER+"' " +
				"    THEN f_codelst_desc(ord.order_type) " +
				"  END ordertype, " +
				"  STAT.STATUS_DATE HISTORYDATE, " +
				"  STAT.STATUS_TYPE_CODE, " +
				"  TO_CHAR(SYSDATE,'Mon DD, YYYY') SYSTEMDATE, "+
				"  ORD.order_sample_at_lab " +
				"  FROM CB_ENTITY_STATUS STAT " +
				"  LEFT OUTER JOIN ER_ORDER ORD " +
				"  ON (ORD.PK_ORDER      =STAT.ENTITY_ID) " +
				"  LEFT OUTER JOIN ACTIVITY ACT ON (ACT.PK_ACTIVITY=STAT.FK_STATUS_VALUE)"+
				"  WHERE STAT.ENTITY_ID IN " +
				"    (SELECT PK_ORDER " +
				"    FROM ER_ORDER ORD, " +
				"      ER_ORDER_HEADER ORDHDR " +
				"    WHERE ORD.FK_ORDER_HEADER=ORDHDR.PK_ORDER_HEADER " +
				"    AND ORDHDR.ORDER_ENTITYID='"+pkcord+"' " +
				"    ) " +
				"  AND STAT.FK_ENTITY_TYPE= " +order_entity_type +
				"  AND (" +
				"  STAT.STATUS_TYPE_CODE   IN ('"+VelosMidConstants.TASK_STATUS+"','"+VelosMidConstants.TASK_RESET+"' ) )" +
				"  AND ACT.ACTIVITY_NAME IN ('"+VelosMidConstants.REQ_CLINC_INFO+"','"+VelosMidConstants.FINAL_CBU_REVIEW+"')";
			}
			
		
			if(i==0){
				sql="SELECT count(*) From";
			}else{
				sql="SELECT HIST.DATEVAL AS DATEVALUE, HIST.DESC1||HIST.DESC2||HIST.DESC3 AS HISTORYDESC,HIST.USERNAME AS USERNAME,HIST.ORDERTYPE AS ORDERTYPE From ";
			}
			
			sql=sql+"(SELECT TO_CHAR(STATUS.STATUS_DATE,'Mon DD, YYYY') AS DATEVAL, " +
					"  CASE " +
					"    WHEN STATUS.STATUS_TYPE_CODE='"+VelosMidConstants.CORD_STATUS+"' " +
					"    THEN '"+VelosMidConstants.HISTORY_CBU_REASON+"' " +
					"    WHEN STATUS.STATUS_TYPE_CODE='"+VelosMidConstants.CORD_NMDP_STTS+"' " +
					"    THEN '"+VelosMidConstants.HISTORY_CBU_STATUS+"' " +
					"    WHEN STATUS.STATUS_TYPE_CODE='"+VelosMidConstants.ELIGIBLE+"' " +
					"    THEN '"+VelosMidConstants.HISTORY_CBU_ELIGIBILITY+"' " +
					"    WHEN STATUS.STATUS_TYPE_CODE='"+VelosMidConstants.LICENSE+"' " +
					"    THEN '"+VelosMidConstants.HISTORY_CBU_LICENSE+"' " +
					"  END DESC1, " +
					"  CASE " +
					"    WHEN STATUS.ADD_OR_UPDATE      IS NULL " +
					"    OR TO_CHAR(STATUS.ADD_OR_UPDATE)='0' " +
					"    THEN ' "+VelosMidConstants.HISTORY_ENTERED_AS+" ' " +
					"    WHEN TO_CHAR(STATUS.ADD_OR_UPDATE)='1' " +
					"    THEN ' "+VelosMidConstants.HISTORY_CHANGED_AS+" ' " +
					"  END DESC2, " +
					"  CASE " +
					"    WHEN (STATUS.STATUS_TYPE_CODE='"+VelosMidConstants.CORD_STATUS+"' " +
					"    OR STATUS.STATUS_TYPE_CODE   ='"+VelosMidConstants.CORD_NMDP_STTS+"') " +
					"    THEN CBUSTATUS.CBU_STATUS_DESC " +
					"    WHEN (STATUS.STATUS_TYPE_CODE='"+VelosMidConstants.ELIGIBLE+"' " +
					"    OR STATUS.STATUS_TYPE_CODE   ='"+VelosMidConstants.LICENSE+"') " +
					"    THEN F_CODELST_DESC(STATUS.FK_STATUS_VALUE) " +
					"  END DESC3, " +
					"  CASE " +
					"    WHEN STATUS.CREATOR=0 " +
					"    THEN '"+VelosMidConstants.HISTORY_USER_SYSTEM+"' " +
					"    WHEN STATUS.CREATOR!=0 " +
					"    THEN F_GETUSER(STATUS.CREATOR) " +
					"  END USERNAME, " +
					"  '-' ORDERTYPE, " +
					"  STATUS.STATUS_DATE HISTORYDATE, " +
					"  STATUS.STATUS_TYPE_CODE, " +
					"  TO_CHAR(SYSDATE,'Mon DD,YYYY') SYSTEMDATE, " +
					"  '' SAMPLEATLAB " +
					"FROM CB_ENTITY_STATUS STATUS " +
					"LEFT OUTER JOIN CB_CBU_STATUS CBUSTATUS " +
					"ON (STATUS.FK_STATUS_VALUE=CBUSTATUS.PK_CBU_STATUS) " +
					"WHERE (STATUS.ENTITY_ID   ='"+pkcord+"' " +
					"AND STATUS.FK_ENTITY_TYPE = " +cbu_entity_type+") " +
					"AND( STATUS.STATUS_TYPE_CODE='"+VelosMidConstants.CORD_STATUS+"' " +
					"OR STATUS.STATUS_TYPE_CODE  ='"+VelosMidConstants.CORD_NMDP_STTS+"' " +
					"OR STATUS.STATUS_TYPE_CODE  ='"+VelosMidConstants.ELIGIBLE+"' " +
					"OR STATUS.STATUS_TYPE_CODE  ='"+VelosMidConstants.LICENSE+"')"+ordersCode+
					"UNION ALL " +
					"SELECT TO_CHAR(FRMVR.CREATED_ON,'Mon DD, YYYY') AS DATEVAL, " +
					"  FRM.FORMS_DESC DESC1, " +
					"  ' "+VelosMidConstants.HISTORY_FORM+" ' DESC2, " +
					"  CASE " +
					"    WHEN FRMVR.CREATE_OR_MODIFY IS NULL " +
					"    OR FRMVR.CREATE_OR_MODIFY    =0 " +
					"    THEN '"+VelosMidConstants.HISTORY_FORM_ENTERED+"' " +
					"    WHEN FRMVR.CREATE_OR_MODIFY=1 " +
					"    THEN '"+VelosMidConstants.HISTORY_FORM_UPDATED+"' " +
					"  END DESC3, " +
					"  F_GETUSER(FRMVR.CREATOR) USERNAME, " +
					"  '-' ORDERTYPE, " +
					"  FRMVR.CREATED_ON HISTORYDATE, " +
					"  '' CODETYP, " +
					"  TO_CHAR(SYSDATE,'Mon DD, YYYY') SYSTEMDATE, " +
					"  '' SAMPLEATLAB " +
					"FROM CB_FORM_VERSION FRMVR " +
					"LEFT OUTER JOIN CB_FORMS FRM " +
					"ON (FRMVR.FK_FORM      =FRM.PK_FORM) " +
					"WHERE FRMVR.ENTITY_ID  ='"+pkcord+"' " +
					"AND FRMVR.CREATED_ON  IS NOT NULL " +
					"AND FRMVR.ENTITY_TYPE = "+maternalpk+
					") HIST";
			
			if(!condition.equals("")){
				sql=sql+condition;
			}
			
			//log.debug("\n\n\n\n\n\nOrder By text::::::::::::::::::::"+orderby+":ivalue:::::::::"+i);
			
			if(i==1){
				
				if(orderby!=null || ! orderby.equals("")){
					sql=sql+orderby;
				}else{
					sql=sql+" ORDER BY HIST.HISTORYDATE DESC";
				}
				
			}
			
			
			
			//log.debug("\n\n\n\n\n\n\nquery for CBU  History ::"+sql);
			
			//log.debug("\n\n\n\n\n\n\nsql in cbu history:::"+sql);
			Query query=session.createSQLQuery(sql);
			if(paginationSearch!=null && i==1){
				query.setFirstResult(paginationSearch.getiPageNo());
				query.setMaxResults(paginationSearch.getiShowRows());
			}
			cbuHistry=query.list();
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			// TODO: handle exception
		}finally{
			if(session.isOpen())
				session.close();
		}
		return cbuHistry;
	}
	
	public static String getClinicChkStatus(Long cordId){
		return getClinicChkStatus(cordId, null);
	}
	
	public static String getClinicChkStatus(Long cordId,Session session){
		String clinicChkStatus=null;
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="select cri.complete_req_info_flag from cb_cord_complete_req_info cri where cri.pk_cord_complete_reqinfo=" +
					"(select max(cr.pk_cord_complete_reqinfo) from cb_cord_complete_req_info cr where cr.fk_cord_id='"+cordId+"')";
			//log.debug("query is::::"+qry);
			List lst=session.createSQLQuery(qry).list();
			//log.debug("getclinicalcheckstsus list size::::::::::"+lst.size());
			if(lst.size()>0){
				clinicChkStatus=(String)lst.get(0);
			}
			//log.debug("clinical ckecklist flag is::::"+clinicChkStatus);
			
		}catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return clinicChkStatus;
	}
	
	public static List getfinalReviewStatus(Long cordId,Long orderId){
		return getfinalReviewStatus(cordId,orderId, null);
	}
	
	public static List getfinalReviewStatus(Long cordId,Long orderId,Session session){
		//String finalreviewStatus=null;
		List lst=new ArrayList();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="select FINAL_REVIEW_CONFIRM_FLAG,to_char(FINAL_REVIEW_CREATED_ON,'Mon DD, YYYY'),to_date(to_char(FINAL_REVIEW_CREATED_ON,'Mon DD, YYYY'),'Mon DD, YYYY')-to_date(to_char(ordhdr.order_open_date,'Mon DD, YYYY'),'Mon DD, YYYY') nofodays from CB_CORD_FINAL_REVIEW rv left outer join cb_cord crd on(rv.FK_CORD_ID=crd.pk_cord) " +
					"left outer join (select ordh.order_entityid order_entityid,order_open_date from er_order_header ordh,er_order ord where ord.fk_order_header=ordh.pk_order_header and ord.pk_order="+orderId+") ordhdr on(ordhdr.ORDER_ENTITYID=crd.pk_cord) where rv.pk_cord_final_review=" +
					"(select max(pk_cord_final_review) from cb_cord_final_review where fk_cord_id="+cordId+")";
			lst=session.createSQLQuery(qry).list();
			//log.debug("getfinalreview list size::::::::::"+lst.size());
			/*if(lst.size()>0){
				
				if(lst.get(0)!=null && !lst.get(0).equals("")){
					//log.debug("lst.get(0).toString()::::"+lst.get(0).toString());
					finalreviewStatus=lst.get(0).toString();
				}
			}
			//log.debug("final review flag is::::"+finalreviewStatus);*/
			
		}catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return lst;
	}
	
	public static List getpackingSlipDetails(Long pkPackingSlip){
		return getpackingSlipDetails(pkPackingSlip, null);
	}
	
	public static List getpackingSlipDetails(Long pkPackingSlip,Session session){
		
		List lst=new ArrayList();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			
			String qry="";
			/*qry="select crd.CORD_REGISTRY_ID,crd.cord_local_cbu_id,crd.cord_isbi_din_code,cbb.site_name,addr.ADDRESS,addr.ADD_CITY,addr.ADD_ZIPCODE,addr.ADD_STATE,addr.ADD_COUNTRY," +
					"nvl(to_char(ship.SCH_SHIPMENT_DATE,'Mon DD, YYYY'),'Not Available') shipdate,nvl(f_codelst_desc(ord.fk_sample_type_avail),'Not Available') sample_type,nvl(f_codelst_desc(ord.fk_aliquots_type),'Not Available') aliquots_type," +
					"to_char(sysdate,'Mon DD, YYYY') run_date,TO_CHAR(SYSTIMESTAMP, 'HH:MI AM') run_time from er_order ord inner join er_order_header ordhdr on(ord.fk_order_header=ordhdr.pk_order_header) " +
					"left outer join cb_cord crd on(ordhdr.order_entityid=crd.pk_cord) left outer join cb_shipment ship on(ship.fk_order_id=ord.pk_order) left outer join er_site cbb on(crd.fk_cbb_id=cbb.pk_site) " +
					"left outer join er_add addr on(cbb.fk_peradd=addr.pk_add) where ord.pk_order="+orderId+" and ship.fk_shipment_type=(select pk_codelst from er_codelst " +
					"where codelst_type='"+VelosMidConstants.SHIPMENT_TYPE+"' and codelst_subtyp='"+VelosMidConstants.CBU+"')";*/
			qry="select crd.CORD_REGISTRY_ID,crd.cord_local_cbu_id,crd.cord_isbi_din_code,cbb.site_name,addr.ADDRESS,addr.ADD_CITY,addr.ADD_ZIPCODE," +
					"addr.ADD_STATE,addr.ADD_COUNTRY,nvl(to_char(pslip.SCH_SHIPMENT_DATE,'Mon DD, YYYY'),'Not Available') shipdate," +
					"nvl(f_codelst_desc(pslip.FK_SAMPLE_TYPE_AVAIL),'Not Available') sample_type,nvl(f_codelst_desc(pslip.FK_ALIQUOTS_TYPE),'Not Available') aliquots_type," +
					"to_char(sysdate,'Mon DD, YYYY') run_date,TO_CHAR(SYSTIMESTAMP, 'HH:MI AM') run_time,nvl(f_codelst_desc(hla.FK_HLA_CODE_ID),'Not Available') lucas," +
					"nvl(f_codelst_desc(hla.FK_HLA_METHOD_ID),'Not Available') meth,nvl(hla.HLA_VALUE_TYPE1,'-'),nvl(hla.HLA_VALUE_TYPE2,'-'),nvl(to_char(hla.created_on,'Mon DD, YYYY'),'Not Available'),PSLIP.PK_PACKING_SLIP  from cb_packing_slip pslip " +
					"left outer join cb_cord crd on(pslip.fk_cord_id=crd.pk_cord) left outer join er_site cbb on(pslip.cbb_id=cbb.pk_site) " +
					"left outer join er_add addr on(cbb.fk_peradd=addr.pk_add) left outer join er_order ord on(pslip.fk_order_id=ord.pk_order) " +
					"left outer join cb_hla hla on(ord.fk_current_hla=hla.pk_hla) where pslip.pk_packing_slip="+pkPackingSlip;
			//log.debug("qry is::::::::"+qry);
			lst=session.createSQLQuery(qry).list();
			//log.debug("packageslip list size::::::::::"+lst.size());
						
		}catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return lst;
	}
	public static String getCBUStatusCode(String pkval){
		Long val=0l;
		if(pkval!=null && !pkval.equals("") && !pkval.equals("undefined")){
			val=Long.parseLong(pkval);
		}
		return getCBUStatusCode(val,null);
	}
	
	public static String getCBUStatusCode(Long pkval,Session session){
		String code="";
		List temp=new ArrayList();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="select status.cbu_status_code from cb_cbu_status status where status.pk_cbu_status="+pkval;
			temp=session.createSQLQuery(qry).list();
			if(temp!=null && temp.size()>0){
			Object obj=temp.get(0);
			code = obj.toString();
			}
			//log.debug("CBU status code : "+code);
		}catch (Exception e) {
			log.error(e.getMessage());
			// TODO: handle exception
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
			    session.close();
			}
		}
		return code;
	}
	public static String getCBUStatusType(String pkval){
		Long val=0l;
		if(pkval!=null && !pkval.equals("") && !pkval.equals("undefined")){
			val=Long.parseLong(pkval);
		}
		return getCBUStatusType(val,null);
	}
	
	public static String getCBUStatusType(Long pkval,Session session){
		String ctype=null;
		List temp=new ArrayList();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="select status.CODE_TYPE from cb_cbu_status status where pk_cbu_status="+pkval;
			temp=session.createSQLQuery(qry).list();
			//log.debug("temp.size():::::"+temp.size());
			if(temp!=null && temp.size()>0){
				Object obj=temp.get(0);
				ctype=obj.toString();
			}
			//log.debug("CBU code type : "+ctype);
		}catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
			    session.close();
			}
		}
		return ctype;
	}
	public static void LCStoResolution(String pkresol,String ordertype,String pkorderId){
		Long orderId=0l;
		Long resol=0l;
		if(pkorderId!=null){
			pkorderId = pkorderId.trim();
			if(!pkorderId.equals("") && !pkorderId.equals("undefined")){
				orderId=Long.parseLong(pkorderId);
			}
		}
		if(pkresol!=null && !pkresol.equals("") && !pkresol.equals("undefined")){
			resol=Long.parseLong(pkresol);
		}
		LCStoResolution(resol,ordertype,orderId,null);
	}
	public static void LCStoResolution(Long pkresol,String ordertype,Long pkorderId,Session session){
		
		String fkcbustatus="0";
		List temp=new ArrayList();
		String type="";
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String statuscode=getCBUStatusCode(pkresol,null);
			//log.debug("statuscode::::::::"+statuscode);
			if( (ordertype.equals("CT")) || ( (ordertype.equals("HE") || ordertype.equals("OR")) && !statuscode.equals("AG") )){
			String qry = "select status.fk_cbu_status from cb_cbu_status status where status.pk_cbu_status="+pkresol;
			//log.debug("qry issssss::::::::::"+qry);
			temp=session.createSQLQuery(qry).list();
			if(temp!=null && temp.size()>0){
				Object obj=temp.get(0);
				if(obj!=null && !obj.equals("")){
					fkcbustatus=obj.toString();
				}
			}
			type=getCBUStatusType(fkcbustatus);
			//log.debug("type:::::::"+type);
			if(type!=null && type.equals("Resolution")){
				//log.debug("in Resolution...........");
				String qry1="update cb_cord cord set cord.FK_CORD_CBU_STATUS="+pkresol+" where cord.pk_cord=(select ordhre.ORDER_ENTITYID from er_order_header ordhre inner join er_order ord on(ord.fk_order_header=ordhre.pk_order_header) where ord.pk_order="+pkorderId+")";
				SQLQuery q1=session.createSQLQuery(qry1);
				q1.executeUpdate();
				q1=session.createSQLQuery("update er_order set FK_ORDER_RESOL_BY_CBB="+fkcbustatus+",order_resol_date=sysdate where pk_order="+pkorderId);
				q1.executeUpdate();
				session.getTransaction().commit();
			}
			else if(type.equals("Response")){
				//log.debug("in Response...........");
				String qry2="update cb_cord cord set cord.FK_CORD_CBU_STATUS='"+fkcbustatus+"' where cord.pk_cord=(select ordhre.ORDER_ENTITYID from er_order_header ordhre inner join er_order ord on(ord.fk_order_header=ordhre.pk_order_header) where ord.pk_order='"+pkorderId+"')";
				SQLQuery q2=session.createSQLQuery(qry2);
				q2.executeUpdate();
				q2=session.createSQLQuery("update er_order set FK_ORDER_RESOL_BY_CBB='"+fkcbustatus+"',order_resol_date=sysdate where pk_order="+pkorderId);
				q2.executeUpdate();
				session.getTransaction().commit();
			}
			
			}
			else{
				//log.debug("condition not mapped resolution to lcs not change");
			}
			
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	public static List getpatientList(Long orderId,Long cordId){
		return getpatientList(orderId,cordId,null);
	}
	public static List getpatientList(Long orderId,Long cordId,Session session){
		List pkpatient=null;
		List lst = null;
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if (orderId != null && cordId!=null) {
				String sql = "select PK_RECEIPANT,receipant_id from ER_ORDER_RECEIPANT_INFO PRE,CB_RECEIPANT_INFO REC where PRE.FK_RECEIPANT=REC.PK_RECEIPANT AND PRE.FK_ORDER_ID="+orderId+" and PRE.FK_CORD_ID="+cordId;
				//log.debug("SQL:"+sql);
				SQLQuery query = session.createSQLQuery(sql);
				lst = query.list();
				/*if(lst!=null && lst.size()>0){
					if(lst.get(0)!=null){
						pkpatient = Long.parseLong(lst.get(0).toString());
					}
				}*/
				//log.debug("patentLst : "+lst);
			}
		} catch (Exception e) {
			//log.debug(" Exception in getIDMTestcount " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return lst;
	}
	
	public static List getShipmentDetails(Long orderId,Long cordId){
		return getShipmentDetails(orderId,cordId,null);
	}
	public static List getShipmentDetails(Long orderId,Long cordId,Session session){
		List pkpatient=null;
		List lst = null;
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if (orderId != null && cordId!=null) {
				String sql = "select to_char(recInfo.prop_prep_date,'Mon DD, YYYY') prop_prep_date, to_char(shipinfo.shipdate,'Mon DD, YYYY') shipdate from ER_ORDER_RECEIPANT_INFO recInfo LEFT OUTER JOIN (select" 
				    		+" ship.fk_order_id,ship.SCH_SHIPMENT_DATE shipdate from cb_shipment ship ) shipinfo ON (shipinfo.fk_order_id = recinfo.fk_order_id)"
				    		+" where recInfo.fk_order_id ="+orderId+" and recinfo.fk_cord_id="+cordId;
				//log.debug("SQL:"+sql);
				SQLQuery query = session.createSQLQuery(sql);
				lst = query.list();
				/*if(lst!=null && lst.size()>0){
					if(lst.get(0)!=null){
						pkpatient = Long.parseLong(lst.get(0).toString());
					}
				}*/
				//log.debug("patentLst : "+lst);
			}
		} catch (Exception e) {
			//log.debug(" Exception in getShipmentDetails:" + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return lst;
	}
	
	public static String getPkCbuStatus(String ordertype,String codetype,String statuscode){
		return getPkCbuStatus(ordertype,codetype,statuscode,null);
	}
	public static String getPkCbuStatus(String ordertype,String codetype,String statuscode,Session session){
		
		String pktu="";
		String votype = "";
		List temp=new ArrayList();
		String type;
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			//log.debug("ordertype in helper:"+ordertype);
			if(ordertype!=null && ordertype!=""){
				if(ordertype.equals("CT"))votype="cbb_ct";
				else if(ordertype.equals("HE"))votype="cbb_he";
				else if(ordertype.equals("OR"))votype="cbb_or";
				String qry = "select status.pk_cbu_status from cb_cbu_status status where status."+votype+"='1' and status.code_type='"+codetype+"' and status.cbu_status_code='"+statuscode+"'";
				temp=session.createSQLQuery(qry).list();
				if(temp!=null && temp.size()>0){
					Object obj=temp.get(0);
					if(obj!=null){
						pktu=obj.toString();
					}
				}
				//log.debug("Inside if Pk tempunavail"+pktu);
			}else{
				String qry = "select status.pk_cbu_status from cb_cbu_status status where status.code_type='"+codetype+"' and status.cbu_status_code='"+statuscode+"'";
				temp=session.createSQLQuery(qry).list();
				if(temp!=null && temp.size()>0){
					Object obj=temp.get(0);
					if(obj!=null){
						pktu=obj.toString();
					}
				}
				//log.debug("Inside else Pk tempunavail"+pktu);
			}
			
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return pktu;
	}
	
	public static String getTermialStatusByPkOrDesc(Long pkValue, String desc){
		return getTermialStatusByPkOrDesc(pkValue,desc,null);
	}
	public static String getTermialStatusByPkOrDesc(Long pkValue, String desc,Session session){
		
		String pktu="";
		List temp=new ArrayList();
		String qry = null;
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			//log.debug("pkValue::::::::"+pkValue+"--------desc::::::::"+desc);
			if(pkValue!=null){
				qry = "select status.IS_TERMINAL_RELEASE from cb_cbu_status status where status.PK_CBU_STATUS = '"+pkValue+"'";
			}
			else if(desc!=null){
				qry = "select status.IS_TERMINAL_RELEASE from cb_cbu_status status where status.CBU_STATUS_DESC = '"+desc+"'";
			}
			if(qry!=null){
				temp=session.createSQLQuery(qry).list();
				if(temp!=null && temp.size()>0){
					Object obj=temp.get(0);
					if(obj!=null){
						pktu=obj.toString();
					}
				}
				//log.debug("Inside if Pk teminal release"+pktu);
			}
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return pktu;
	}
	
	public static Long getOrderType(Long pkOrder){
		return getOrderType(pkOrder,null);
	}
	public static Long getOrderType(Long pkOrder, Session session){
		
		String ordType="";
		Long ordTypel=0L;
		List temp=new ArrayList();
		String qry = null;
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			//log.debug("pkValue::::::::"+pkValue+"--------desc::::::::"+desc);
			if(pkOrder!=null){
				qry = "select order_type from er_order where pk_order in ( "+pkOrder+")";
			}
			
			if(qry!=null){
				temp=session.createSQLQuery(qry).list();
				if(temp!=null && temp.size()>0){
					Object obj=temp.get(0);
					if(obj!=null){
						ordType=obj.toString();
					}
				}
				log.debug("Ordertype:::::::::::;"+ordType);
				ordTypel=Long.parseLong(ordType);
				
			}
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return ordTypel;
	}
	public static Long getPkActivity(String activityname){
		return getPkActivity(activityname,null);
	}
	public static Long getPkActivity(String activityname, Session session){
		
		String strpkactivity="";
		Long pkactivity=0L;
		List temp=new ArrayList();
		String qry = null;
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			//log.debug("pkValue::::::::"+pkValue+"--------desc::::::::"+desc);
			if(!activityname.equals("")){
				qry = "select pk_activity from activity where activity_name='"+activityname+"'";
			}
			
			if(qry!=null){
				temp=session.createSQLQuery(qry).list();
				if(temp!=null && temp.size()>0){
					Object obj=temp.get(0);
					if(obj!=null){
						strpkactivity=obj.toString();
					}
				}
				//log.debug("strpkactivity:::::::::::;"+strpkactivity);
				pkactivity=Long.parseLong(strpkactivity);
				
			}
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return pkactivity;
	}
	public static List cbbDetailPopup(String pksite,String ordertype,String sampleAtlab){
		return cbbDetailPopup(pksite,ordertype,sampleAtlab,null);
	}
	public static List cbbDetailPopup(String pksite,String ordertype,String sampleAtlab,Session session){
		
		List temp=new ArrayList();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			if(pksite!=null && pksite!=""){
				
					String qry="";
				
				    if(ordertype.equals(VelosMidConstants.CT_ORDER)){
				    	
				    	qry="SELECT site.site_name,ab.totalorders totalorders1,NVL(bb.neworders, 0) neworders,CASE WHEN f_codelst_desc(ab.ordertype)='"+VelosMidConstants.CT_ORDER+"' THEN DECODE(ab.sampatlab,'Y','"+VelosMidConstants.CT_LAB_ORDER+"','N','"+VelosMidConstants.CT_SHIP_ORDER+"') "+
						" WHEN f_codelst_desc(ab.ordertype)!='"+VelosMidConstants.CT_ORDER+"' THEN f_codelst_desc(ab.ordertype) END ordertype,SITE.PK_SITE SITEID,ab.ordertype ordertype1 FROM (SELECT crd.fk_cbb_id cbb_id1,"+
						" COUNT(order_type) totalorders,ord.order_sample_at_lab sampatlab,ORD.ORDER_TYPE ordertype FROM er_order ord,er_order_header ordhdr,cb_cord crd WHERE ord.fk_order_header=ordhdr.pk_order_header"+
						" AND ordhdr.order_entityid=crd.pk_cord AND crd.fk_cbb_id   IN ("+pksite+") and ord.order_status not in (select CL.PK_CODELST from er_codelst cl where CL.CODELST_TYPE='"+VelosMidConstants.ORDER_STATUS+"' and CL.CODELST_SUBTYP='"+VelosMidConstants.CLOSED+"') GROUP BY crd.fk_cbb_id,ord.order_sample_at_lab, ORD.ORDER_TYPE HAVING f_codelst_desc(ord.order_type)='"+ordertype+"' AND ORD.ORDER_SAMPLE_AT_LAB='"+sampleAtlab+"'"+
						" ) ab left outer join (SELECT crd.fk_cbb_id cbb_id,COUNT(ord.pk_order) neworders,ord.order_sample_at_lab sampatlab FROM er_order ord LEFT OUTER JOIN er_order_header ordhdr ON(ord.fk_order_header=ordhdr.pk_order_header) LEFT OUTER JOIN cb_cord crd ON (crd.pk_cord = ordhdr.order_entityid)"+
						" where crd.fk_cbb_id IN ("+pksite+") GROUP BY crd.fk_cbb_id,ord.order_type,ord.order_sample_at_lab,ord.order_status HAVING f_codelst_desc(order_type)='"+ordertype+"'  AND ORD.ORDER_SAMPLE_AT_LAB='"+sampleAtlab+"' AND f_codelst_desc(order_status) ='"+VelosMidConstants.NEWORDER+"') bb on (bb.cbb_id=ab.cbb_id1)"+
						" left join er_site site on (site.pk_site=bb.cbb_id or site.pk_site=ab.cbb_id1) order by site.site_name";
				    	
				    	//log.debug("\n\n\n\n\nIn CT qry::::::::::"+qry);
					
				    	
				    }else if(ordertype.equals(VelosMidConstants.HE) || ordertype.equals(VelosMidConstants.OR)){
				    	qry="SELECT site.site_name,ab.totalorders totalorders1,NVL(bb.neworders, 0) neworders,"+
						" f_codelst_desc(ab.ordertype) ordertype,SITE.PK_SITE SITEID,ab.ordertype ordertype1 FROM (SELECT crd.fk_cbb_id cbb_id1,"+
						" COUNT(order_type) totalorders,ORD.ORDER_TYPE ordertype FROM er_order ord,er_order_header ordhdr,cb_cord crd WHERE ord.fk_order_header=ordhdr.pk_order_header"+
						" AND ordhdr.order_entityid=crd.pk_cord AND crd.fk_cbb_id   IN ("+pksite+") and ord.order_status not in (select CL.PK_CODELST from er_codelst cl where CL.CODELST_TYPE='order_status' and CL.CODELST_SUBTYP='close_ordr') GROUP BY crd.fk_cbb_id, ORD.ORDER_TYPE HAVING f_codelst_desc(ord.order_type)='"+ordertype+"'"+
						" ) ab left outer join (SELECT crd.fk_cbb_id cbb_id,COUNT(ord.pk_order) neworders FROM er_order ord LEFT OUTER JOIN er_order_header ordhdr ON(ord.fk_order_header=ordhdr.pk_order_header) LEFT OUTER JOIN CB_CORD crd ON (crd.PK_CORD = ordhdr.order_entityid)"+
						" where crd.fk_cbb_id IN ("+pksite+") GROUP BY crd.fk_cbb_id,ord.order_type,ord.order_status HAVING f_codelst_desc(order_type)='"+ordertype+"' AND f_codelst_desc(order_status) ='"+VelosMidConstants.NEWORDER+"') bb on (bb.cbb_id=ab.cbb_id1)"+
						" left join er_site site on (site.pk_site=bb.cbb_id or site.pk_site=ab.cbb_id1) order by site.site_name";
				    	
				    	//log.debug("\n\n\n\n\nIn HE Or OR qry::::::::::"+qry);
				    	
				    }
					
				    temp = session.createSQLQuery(qry).list();
					
					
			}
				
			
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return temp;
	}
	public static String getTcResol(String orderId){
		Long pkorder=0l;
		if(orderId!=null && !orderId.equals("") && !orderId.equals("undefined")){		
			pkorder=Long.parseLong(orderId);
		}
		return getTcResol(pkorder,null);
	}
	public static String getTcResol(Long orderId,Session session){
		String tcResol = null;
		List temp;
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="select nvl(ord.fk_order_resol_by_tc,0) from er_order ord where PK_ORDER="+orderId;
			temp=session.createSQLQuery(qry).list();
			if(temp!=null && temp.size()>0){
				Object obj=temp.get(0);
				tcResol=obj.toString();
			}
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			// TODO: handle exception
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return tcResol;
	}
	public static List getShedShipDts(String shipDt,String siteId){
		return getShedShipDts(shipDt,siteId,null);
	}
	public static List getShedShipDts(String shipDt,String siteId,Session session){
		//log.debug("Inside helper for Ship date::::");
		List ShedShipDtsLst=new ArrayList();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="";
			if(shipDt!=null){
				//log.debug("Inside if::::");
						qry="select to_char(shpmt.sch_shipment_date,'Mon DD, YYYY'),cord.cord_registry_id,ord.pk_order,cord.pk_cord from cb_shipment shpmt"+ 
						" left outer join er_order ord on(shpmt.FK_ORDER_ID=ord.pk_order) left outer join er_order_header ordhdr on"+
						"(ordhdr.pk_order_header=ord.fk_order_header) left outer join cb_cord cord on(ordhdr.order_entityid=cord.pk_cord)"+
						" where f_codelst_desc(ord.ord.order_type)='"+VelosMidConstants.OR+"' AND shpmt.fk_shipment_type in (select pk_codelst from er_codelst where codelst_type='"+VelosMidConstants.SHIPMENT_TYPE+"' and codelst_subtyp='"+VelosMidConstants.CBU+"')  and shpmt.sch_shipment_date='"+shipDt+"' and cord.fk_cbb_id in ("+siteId+")";
				
			}
			else{
				//log.debug("Inside else::::");
						qry="select to_char(shpmt.sch_shipment_date,'Mon DD YYYY'),cord.cord_registry_id,ord.pk_order,cord.pk_cord from cb_shipment shpmt"+ 
						" left outer join er_order ord on(shpmt.FK_ORDER_ID=ord.pk_order) left outer join er_order_header ordhdr on"+
						"(ordhdr.pk_order_header=ord.fk_order_header) left outer join cb_cord cord on(ordhdr.order_entityid=cord.pk_cord)"+
						" where f_codelst_desc(ord.ord.order_type)='"+VelosMidConstants.OR+"' AND shpmt.fk_shipment_type in (select pk_codelst from er_codelst where codelst_type='"+VelosMidConstants.SHIPMENT_TYPE+"' and codelst_subtyp='"+VelosMidConstants.CBU+"') and ORD.ORDER_STATUS NOT IN  (SELECT CL.PK_CODELST FROM er_codelst cl "+
						" WHERE CL.CODELST_TYPE='"+VelosMidConstants.ORDER_STATUS+"' AND CL.CODELST_SUBTYP='"+VelosMidConstants.CLOSED+"' ) and  (ord.fk_order_resol_by_tc is null or  ord.fk_order_resol_by_tc not in (SELECT resol.pk_cbu_status from cb_cbu_status resol WHERE resol.code_type='"+VelosMidConstants.RESOLUTION+"' and resol.cbu_status_code='"+VelosMidConstants.CA+"')) "+
						" AND cord.fk_cbb_id IN ("+siteId+") AND shpmt.sch_shipment_date IS NOT NULL";
			}
			//log.debug(" \n \n \n \n \n \n \nquery for shipmentcalender::::::::::::::::::::::::"+qry);
		ShedShipDtsLst = session.createSQLQuery(qry).list();
		}catch (Exception e) {
			// TODO: handle exception
			//log.debug("Error:::"+e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return ShedShipDtsLst;
	}
	public static List getDataFromCodelstCustCol(String tableName,String columnName,String criteria){
		return getDataFromCodelstCustCol(tableName, columnName, criteria,null);
	}
		
	public static List getDataFromCodelstCustCol(String tableName,String columnName,String criteria,Session session){
		List lst=new ArrayList();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			
			String qry="select "+columnName+" from "+tableName+criteria;
			//log.debug("query from getDataFromCodelstCustCol is::"+qry);
			if(Utilities.removesqlconstants(qry)){
				lst=session.createSQLQuery(qry).list();
			}
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return lst;
	}
	
	public static void updateDataFromCodelstCustCol(String tableName,String columnName,Long value,String criteria,String operation){
		updateDataFromCodelstCustCol(tableName, columnName,value,criteria,operation,null);
	}
	
	public static int updateDataFromCodelstCustCol(String tableName,String columnName,Long value,String criteria,String operation,Session session){
		int count=0;
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="";
			if(operation.equals("ADD")){
				qry="update "+tableName+" set "+columnName+"="+columnName+"+1"+criteria;
			}else if(operation.equals("MINUS")){
				qry="update "+tableName+" set "+columnName+"="+columnName+"-1"+criteria;
			}
			//log.debug("query from updateDataFromCodelstCustCol is::"+qry);
			SQLQuery sqry=session.createSQLQuery(qry);
			count=sqry.executeUpdate();
			//log.debug(count+" records Updated...............");
			session.getTransaction().commit();
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return count;
	}
	
	public static List<FilterDataPojo> customFilterInsert(FilterDataPojo filterDataPojo){
		return customFilterInsert(filterDataPojo,null);
	}
	public static List<FilterDataPojo> customFilterInsert(FilterDataPojo filterDataPojo,Session session){
		List<FilterDataPojo> customFilter=new ArrayList<FilterDataPojo>();
		List<FilterData> custfilter=new ArrayList<FilterData>();
		FilterData filterdata = new FilterData();
		filterdata = (FilterData) ObjectTransfer.transferObjects(filterDataPojo, filterdata);
		
		try {
			if (session == null) {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}

			List<FilterData>  tempList=new ArrayList<FilterData>();
			
			String tempFilterName=filterDataPojo.getFilter_name().trim().toLowerCase();
			
			//log.debug("checkdata...."+tempFilterName);
			
			String isPrevious="From FilterData where fk_user in ("+filterDataPojo.getFk_user()+") and fk_module in ("+filterDataPojo.getFk_module()+") and fk_filter_type in ("+filterDataPojo.getFk_filter_type()+") and fk_sub_module in ("+filterDataPojo.getFk_sub_module()+") and lower(filter_name)  ='"+tempFilterName+"'";
			tempList = session.createQuery(isPrevious).list();
			//log.debug("\n\n\n\nlistSize for current filterName......."+tempList.size());
			
			if(tempList!=null && tempList.size()>0){
				FilterDataPojo filterOldpojo= new FilterDataPojo();
				for(FilterData s : tempList){
					filterOldpojo = (FilterDataPojo)ObjectTransfer.transferObjects(s,filterOldpojo);
				}
				
				String updateQuery="update FilterData set filter_paraameter='"+filterDataPojo.getFilter_paraameter()+"',filter_name='"+tempFilterName+"' where fk_user in ("+filterDataPojo.getFk_user()+") and fk_module in ("+filterDataPojo.getFk_module()+") and fk_filter_type in ("+filterDataPojo.getFk_filter_type()+") and fk_sub_module in ("+filterDataPojo.getFk_sub_module()+") and pk_filter='"+filterOldpojo.getPk_filter()+"'";
				Query query = session.createQuery(updateQuery);
				int result = query.executeUpdate();
				session.getTransaction().commit();
				//log.debug("Inside update action......");
				
			}else{
				
				//log.debug("Inside adding new record action......");
				session.save(filterdata);
				session.getTransaction().commit();
				
			}
			
			
			
			String sql = "From FilterData where fk_user in ("+filterDataPojo.getFk_user()+") and fk_module in ("+filterDataPojo.getFk_module()+") and fk_filter_type in ("+filterDataPojo.getFk_filter_type()+") and fk_sub_module in ("+filterDataPojo.getFk_sub_module()+") order by UPPER(filter_name)";
			
			//log.debug("\n\n\n\n\n\n\n\n\nsql:::::::::::::::::"+sql);
			//session.close();
			if(!session.isOpen()){
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			custfilter = session.createQuery(sql).list();
			for(FilterData s : custfilter){
				FilterDataPojo filterdatapojo= new FilterDataPojo();
				filterdatapojo = (FilterDataPojo)ObjectTransfer.transferObjects(s,filterdatapojo);
				customFilter.add(filterdatapojo);
			}
			//log.debug("\n\n\n\n\n\n\n\n\ncustomfiltersize:::::::::::::::::"+customFilter.size());
			
		} catch (Exception e) {
			//log.debug(" Exception in customFilterInsert " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		
		return customFilter;
	}
	
	public static PackingSlipPojo updatePackingSlipDet(PackingSlipPojo packingSlipPojo){
		return updatePackingSlipDet(packingSlipPojo,null);
	}
	public static PackingSlipPojo updatePackingSlipDet(PackingSlipPojo packingSlipPojo,Session session){
		try{
			PackingSlip packingSlip=new PackingSlip();
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			
			Object object = new VelosUtil().setAuditableInfo(packingSlipPojo);
			packingSlipPojo=(PackingSlipPojo)ObjectTransfer.transferObjects(object, packingSlipPojo);
			packingSlip=(PackingSlip)ObjectTransfer.transferObjects(packingSlipPojo, packingSlip);
			//log.debug("packingslip.fkorderid:::"+packingSlip.getFkOrderId());
			//log.debug("packingslip.created by::"+packingSlip.getCreatedBy());
			if(packingSlip.getPkPackingSlip()==null){
				session.save(packingSlip);
			}
			else{
				session.merge(packingSlip);
			}
			session.getTransaction().commit();
			packingSlipPojo=(PackingSlipPojo)ObjectTransfer.transferObjects(packingSlip, packingSlipPojo);
			//log.debug("packingslippojo.pkpackingslip:::"+packingSlipPojo.getPkPackingSlip());
			
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		
		return packingSlipPojo;
	}
	
	public static String updateAssignTo(String orderId,String userId,String flag,Long loggedIn_userId,String cordIdval){
		return updateAssignTo(orderId,userId,flag,loggedIn_userId,cordIdval,null);
	}
	public static String updateAssignTo(String orderId,String userId,String flag,Long loggedIn_userId,String cordIdval,Session session){
		//log.debug("in updateAssignTo helper");
		//log.debug("flag value is:::::::::"+flag);
		String qry="";
		String cancelled_ack_ords="";
		int init_flag= 0;
		String checkflag="";
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			if(flag.equals("assignedto") || flag.equals("assign_acknowledge")){
				
				qry="update er_order set assigned_to='"+userId+"',ORDER_ASSIGNED_DATE=sysdate,LAST_MODIFIED_BY='"+loggedIn_userId+"',LAST_MODIFIED_DATE=sysdate where pk_order in ("+orderId+")";
				session.createSQLQuery(qry).executeUpdate();
			    session.getTransaction().commit();
			    
			    if(flag.equals("assign_acknowledge")){
			    	if(session.isOpen()){
						session.close();
					}
			    }
			    
			}
			if(flag.equals("acknowledge") || flag.equals("assign_acknowledge")){
				
				
               if(orderId.contains(",") && cordIdval.contains(",")){
					
					String orderId1[]=orderId.split(",");
					String cordIdval1[]=cordIdval.split(",");
					
					for(int i=0;i<orderId1.length;i++){
						
						checkflag=checkAckCondition(Long.parseLong(orderId1[i]),Long.parseLong(cordIdval1[i]));
						
						if(!session.isOpen()){
							session=HibernateUtil.getSessionFactory().getCurrentSession();
							session.beginTransaction();
						}
						
						if(checkflag.equals("1")){
								
							    qry="UPDATE ER_ORDER SET ORDER_ACK_FLAG='"+VelosMidConstants.FLAG_Y+"', ORDER_ACK_DATE=SYSDATE, ORDER_ACK_BY='"+loggedIn_userId+"', ORDER_STATUS=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='"+VelosMidConstants.ORDER_STATUS+"' AND CODELST_SUBTYP='"+VelosMidConstants.CLOSED+"'),ORDER_STATUS_DATE=sysdate,LAST_MODIFIED_BY='"+loggedIn_userId+"',LAST_MODIFIED_DATE=sysdate where pk_order in ('"+orderId1[i]+"')";
								session.createSQLQuery(qry).executeUpdate();
								session.getTransaction().commit();	
						}else{
									if(init_flag==0){
										cancelled_ack_ords=orderId1[i];
										init_flag=1;
									}else{
										cancelled_ack_ords=cancelled_ack_ords+","+orderId1[i];
									}
						}
						
					}
					
					
				}else{
					
					  checkflag=checkAckCondition(Long.parseLong(orderId),Long.parseLong(cordIdval));
					  if(!session.isOpen()){
							session=HibernateUtil.getSessionFactory().getCurrentSession();
							session.beginTransaction();
					  }
					  if(checkflag.equals("1")){
					        qry="UPDATE ER_ORDER SET ORDER_ACK_FLAG='"+VelosMidConstants.FLAG_Y+"',ORDER_ACK_DATE=SYSDATE, ORDER_ACK_BY='"+loggedIn_userId+"', ORDER_STATUS=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='"+VelosMidConstants.ORDER_STATUS+"' AND CODELST_SUBTYP='"+VelosMidConstants.CLOSED+"'),ORDER_STATUS_DATE=sysdate,LAST_MODIFIED_BY='"+loggedIn_userId+"',LAST_MODIFIED_DATE=sysdate where pk_order in ('"+orderId+"')";
					        //log.debug("\n\n\n\n\n\n\n2.Inside update acknowledge to qryL:::::::::::::::::::::::::::::::::::::::::::::"+qry);
					        //log.debug("Inside update acknowledge to qryL:::::::::::::::::::::::::::::::::::::::::::::"+qry);
					        session.createSQLQuery(qry).executeUpdate();
					        session.getTransaction().commit();
					        //log.debug("after commit");
					        //createAlertInstance(Long.parseLong(orderId));
					  }else{
						    cancelled_ack_ords=orderId;
					  }
				}
               //log.debug("\n\n\n\n\n\nafter update Assign to...........");
				
			}
				
			
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return cancelled_ack_ords;
		
	}
	public static String checkAckCondition(Long orderId,Long cordId){
		return checkAckCondition(orderId,cordId,null);
	}
	public static String checkAckCondition(Long orderId,Long cordId,Session session){
		String condition = null;
		
		List templst=new ArrayList();
		List ordtype_lst=new ArrayList();
		List cancel_flag_Lst=new ArrayList();
		List loc_cord_stat_lst=new ArrayList();
		List resol_recei_lst=new ArrayList();
		List cancel_proced_lst=new ArrayList();
		
		String flagvalue="";
		String ordertype="";
		String qry="";
		String loc_cord_stat="";
		String cancel_flag="";
		String defferd_cord_flag="0";
		String v_cordId=cordId.toString();
		String not_needed="0";
		String allowFlag="";
		int success_count=0;
		
		try{
			
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			
			
			String ordtype_sql = "select f_codelst_desc(order_type) from er_order where PK_ORDER in ('"+orderId+"')";
			SQLQuery query = session.createSQLQuery(ordtype_sql);
			ordtype_lst=query.list();
			
			
			if(ordtype_lst!=null && ordtype_lst.size()>0){
				ordertype = ordtype_lst.get(0).toString();
			}
			
			String quer_resol="SELECT CASE WHEN ORD.FK_ORDER_RESOL_BY_TC IS NOT NULL OR ORD.FK_ORDER_RESOL_BY_CBB IS NOT NULL THEN '0' ELSE '1' END TC_RESOL FROM ER_ORDER ORD WHERE ORD.PK_ORDER IN ("+orderId+")";
			SQLQuery query_res = session.createSQLQuery(quer_resol);
			resol_recei_lst=query_res.list();
			
			
			if(resol_recei_lst!=null && resol_recei_lst.size()>0){
				not_needed = resol_recei_lst.get(0).toString();
			}
			
			
			
			//log.debug("ordertype value:::::::::::"+ordertype+"success count::::::"+success_count);
			
			if((ordertype.equals("CT") || ordertype.equals("HE") || ordertype.equals("OR") ) && not_needed.equals("0") ){
				flagvalue="1";	
				//log.debug("success count value in HE and CT:"+success_count);
			}
			
			//log.debug("Inside flag value after execution of loop and success count value"+success_count);
			//log.debug("In check ack condition Flage value:::::::::::::::"+flagvalue);
			
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session!=null && session.isOpen()){
				session.close();
			}
		}
		
		return flagvalue;
	}
	public static List getAssigntoLst(Long siteId){
		return getAssigntoLst(siteId,null);
	}
	public static List getAssigntoLst(Long siteId,Session session){
		List assigntoLst = null;
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="select usr.pk_user, usr.usr_firstname||' '||usr.usr_lastname from er_user usr where usr.fk_siteid='"+siteId+"'";
			Object obj=session.createSQLQuery(qry);
			
			
			
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		
		return assigntoLst;
	}
	
	public static void updateOrderCompleteReqInfoflag(Long cordId){
		updateOrderCompleteReqInfoflag(cordId,null);
	}
	public static void updateOrderCompleteReqInfoflag(Long cordId,Session session){
		
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="update er_order set  complete_req_info_task_flag='Y' where pk_order in (select ord.pk_order"+
			" from er_order ord left outer join er_order_header ordhdr on (ord.fk_order_header=ordhdr.pk_order_header) where"+ 
			" ordhdr.order_entityid="+cordId+")";
			//log.debug("Inside pdateOrderCompleteReqInfoflag query is::::"+qry);
			SQLQuery sql_qry=session.createSQLQuery(qry);
			sql_qry.executeUpdate();
			session.getTransaction().commit();
			//log.debug("After executing update action updateOrderCompleteReqInfoflag::::"+qry);
			
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
			//log.debug(" Getting Exection"+e.getMessage());
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		
	}
	public static void updateOrderLastReviewedBy(Long orderId,Long user_id){
		updateOrderLastReviewedBy(orderId,user_id,null);
	}
	public static void updateOrderLastReviewedBy(Long orderId,Long user_id,Session session){
		
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="update er_order set  ORDER_LAST_VIEWED_BY="+user_id+", ORDER_LAST_VIEWED_DATE=sysdate,LAST_MODIFIED_BY='"+user_id+"',LAST_MODIFIED_DATE=sysdate where pk_order in ("+orderId+")";
			//log.debug("Inside update last viewed by query is::::"+qry);
			SQLQuery sql_qry=session.createSQLQuery(qry);
			sql_qry.executeUpdate();
			session.getTransaction().commit();
			//log.debug("\n\n\n\n\n\n\n\n\n\n\nAfter executing update action ::::"+qry);
			
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
			//log.debug(" After Update Last Review By"+e.getMessage());
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		
	}
	
	public static List getPatientInfo(Long orderId){
		return getPatientInfo(orderId, null);
	}
	
	public static List getPatientInfo(Long orderId,Session session){
		List lst=new ArrayList();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			
			String qry="SELECT ord.infusion_flag, " +
			"  TO_CHAR(ord.infusion_date,'Mon DD, YYYY'), " +
			"  NVL(rec.current_diagnosis,''), " +
			"  NVL(rec.disease_stage,''), " +
			"  NVL(to_char(rec.rec_weight),'0'), " +
			"  NVL(rec.receipant_id,''), " +
			"  TO_CHAR(ship.shipment_date,'Mon DD, YYYY'), " +
			"  ship.SHIPMENT_CONFIRM, " +
			"  rec.IND_SPONSER, " +
			"  rec.IND_NUMBER " +
			"FROM er_order ord " +
			"LEFT OUTER JOIN " +
			"  (SELECT pk_receipant, " +
			"    fk_order_id, " +
			"    pre.current_diagnosis, " +
			"    pre.disease_stage, " +
			"    pre.rec_weight rec_weight, " +
			"    receipant_id, " +
			"    IND_SPONSER, " +
			"    IND_NUMBER " +
			"  FROM ER_ORDER_RECEIPANT_INFO PRE, " +
			"    cb_receipant_info REC " +
			"  WHERE PRE.FK_RECEIPANT=REC.PK_RECEIPANT " +
			"  AND fk_order_id       =" +orderId+
			"  ) rec " +
			"ON(rec.fk_order_id=ord.pk_order) " +
			"LEFT OUTER JOIN " +
			"  (SELECT pk_shipment, " +
			"    shipment_date, " +
			"    fk_order_id, " +
			"    SHIPMENT_CONFIRM " +
			"  FROM cb_shipment " +
			"  WHERE fk_shipment_type= " +
			"    (SELECT pk_codelst " +
			"    FROM er_codelst " +
			"    WHERE codelst_type='shipment_type' " +
			"    AND codelst_subtyp='CBU' " +
			"    ) " +
			//"  AND shipment_confirm IS NOT NULL " +
			"  AND fk_order_id       = " +orderId+
			"  ) ship " +
			"ON(ship.fk_order_id=rec.fk_order_id) " +
			"WHERE ord.pk_order ="+orderId;
			log.debug("query to get patient info is::::"+qry);
			lst=session.createSQLQuery(qry).list();
		}catch(Exception e){
			//log.debug(" Error in getPatientInfo:::"+e.getMessage());
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return lst;
	}
	public static String get_deferedStatusLst(Long fkcordstat){
		
		String flag="0";
		
		List cord_defredLst=new ArrayList<String>();
		String pkval11=getPkCbuStatus("",VelosMidConstants.RESPONSE,VelosMidConstants.DD);
		cord_defredLst.add(pkval11);
	    String pkval12=getPkCbuStatus("",VelosMidConstants.RESPONSE,VelosMidConstants.CD);
	    cord_defredLst.add(pkval12);
	    String pkval13=getPkCbuStatus("",VelosMidConstants.RESPONSE,VelosMidConstants.AG);
	    cord_defredLst.add(pkval13);
	    String pkval14=getPkCbuStatus("",VelosMidConstants.RESPONSE,VelosMidConstants.OT);
	    cord_defredLst.add(pkval14);
	    String pkval15=getPkCbuStatus("",VelosMidConstants.RESPONSE,VelosMidConstants.SO);
	    cord_defredLst.add(pkval15);
	    String pkval16=getPkCbuStatus("",VelosMidConstants.RESPONSE,VelosMidConstants.QR);
	    cord_defredLst.add(pkval16);
	    String pkval17=getPkCbuStatus("",VelosMidConstants.RESPONSE,VelosMidConstants.RO);
	    cord_defredLst.add(pkval17);
	    
	    Iterator it1=cord_defredLst.iterator();
	   
	    
	    while(it1.hasNext()){
	    	Long temp=Long.parseLong(it1.next().toString());
	    	if(fkcordstat.equals(temp)){
	    		flag=flag+1;
	    	}
	    }
	   	   
	    return flag;
	}
	
	public static List getAlertNotifications(Long userId,String siteId,String criteria,String orderBy,PaginateSearch paginateSearch,int i){
		return getAlertNotifications(userId,siteId,criteria,orderBy,paginateSearch,i,null);
	}
	
	
	public static List getAlertNotifications(Long userId,String siteId,String criteria,String orderBy,PaginateSearch paginateSearch,int i,Session session){
		List lst=new ArrayList();
		try{
		
		
		long pkCompleted=getCodelstPk(VelosMidConstants.ORDER_STATUS,VelosMidConstants.CLOSED);
		long pkAlertType=getCodelstPk(VelosMidConstants.ALERT_CODELST_TYPE,VelosMidConstants.NOTIFY_CODELST_SUBTYPE);	
		
		if(session==null || !session.isOpen()){
			session=HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
		}
		
		String qry="";
		String resultSet="";
		
		if(i==0 || i==1){
			resultSet=" ains.pk_alert_instances,alrt.alert_title,ains.alert_wording,NVL(ains.alert_close_flag,'0'),CASE WHEN f_codelst_desc(ord.order_type)='"+VelosMidConstants.CT_ORDER+"' THEN DECODE(ord.order_sample_at_lab,'Y','"+VelosMidConstants.CT_LAB_ORDER+"','N','"+VelosMidConstants.CT_SHIP_ORDER+"') "+
					  " WHEN f_codelst_desc(ord.order_type)!='"+VelosMidConstants.CT_ORDER+"' THEN f_codelst_desc(ord.order_type) END order_type_data, ordhdr.order_num,crd.cord_registry_id,f_codelst_desc(ord.order_status),TO_CHAR(ordhdr.order_open_date,'Mon DD, YYYY') request_date,crd.pk_cord,ord.pk_order,ord.order_status,ord.order_type,"+
					  "  ROW_NUMBER () OVER( ORDER BY ord.pk_order),ROW_NUMBER () OVER( ORDER BY AINS.PK_ALERT_INSTANCES),'' order_type_param,crd.fk_cbb_id,TO_CHAR(ains.CREATED_ON,'Mon DD, YYYY')";
		}
		if(i==2){
			resultSet=" count(*) totalrec ";
		}
		
		if(siteId==null || siteId.equals("")){
			
			qry="select "+resultSet+" from cb_alert_instances ains,er_order ord,er_order_header ordhdr,cb_cord crd,(select pk_codelst alert_id,codelst_desc alert_title from er_codelst where codelst_type in('alert','alert_resol')) alrt,er_site site " +
			"where ains.fk_codelst_alert=alrt.alert_id and ains.entity_id=ord.pk_order and ains.entity_type=(select pk_codelst from er_codelst where codelst_type='entity_type' and codelst_subtyp='ORDER') and ord.fk_order_header=ordhdr.pk_order_header and " +
			"ordhdr.order_entityid=crd.pk_cord and ordhdr.order_entitytype=(select pk_codelst from er_codelst where codelst_type='entity_type' and codelst_subtyp='CBU') and ord.order_status not in(select pk_codelst from er_codelst where codelst_type='"+VelosMidConstants.ORDER_STATUS+"' " +
			"and codelst_subtyp ='"+VelosMidConstants.CLOSED+"')"+criteria+" and ains.fk_userid="+userId+" and ains.alert_type=(select pk_codelst from er_codelst where codelst_type='alert_type' and codelst_subtyp='notify') and"+
			" site.pk_site=crd.fk_cbb_id and site.pk_site in ( select us.fk_site From er_usersite us,cbb cb where cb.fk_site=us.fk_site and us.fk_user="+userId+" and us.usersite_right>0 and cb.using_cdr=1) and site.fk_codelst_type=(select pk_codelst from er_codelst where codelst_type ='"+VelosMidConstants.CBB_TYPE+"' and codelst_subtyp='"+VelosMidConstants.CBB_SUBTYPE+"') "+
			" order by ains.pk_alert_instances desc,f_codelst_desc(ord.order_type),ord.pk_order";
			
		}else{
			
			qry="select "+resultSet+"FROM cb_alert_instances ains,er_order ord,er_order_header ordhdr,cb_cord crd,(SELECT pk_codelst alert_id,codelst_desc alert_title FROM er_codelst WHERE codelst_type IN('alert','alert_resol')) alrt WHERE ains.fk_codelst_alert=alrt.alert_id "+
                " AND ains.entity_id=ord.pk_order AND ord.fk_order_header=ordhdr.pk_order_header "+
                "  AND ordhdr.order_entityid  =crd.pk_cord and ord.order_status <>"+pkCompleted+" AND ains.alert_type="+pkAlertType+criteria+" AND crd.fk_cbb_id IN ("+siteId+") and ains.fk_userid="+userId;
        
		}
		
		if(orderBy==null || orderBy.equals("")){
			//qry=qry+" order by ains.pk_alert_instances desc,f_codelst_desc(ord.order_type),ord.pk_order";
			qry=qry+" order by ains.pk_alert_instances desc,ord.pk_order desc";
			
		}else{
			qry=qry+orderBy;
		}
	
		//log.debug("getAlertNotifications query is:::::::::"+qry);
		
		Query query=session.createSQLQuery(qry);
		
		if(i==1){
				query.setFirstResult(paginateSearch.getiPageNo());
				query.setMaxResults(paginateSearch.getiShowRows());
		}
		
		lst=query.list();
		
		}catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return lst;
	}
	public static long getCodelstPk(String codelst_type,String codelst_subtype){
		return getCodelstPk(codelst_type,codelst_subtype,null);
	}
	public static long getCodelstPk(String codelst_type,String codelst_subtype,Session session){
		
		long pkcodelst=0l;
		String sql="";
		List codelstLst=new ArrayList();
		SQLQuery query;
		String temp="";
		
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			
			if(!codelst_type.equals("") && !codelst_subtype.equals("")){
				
				sql="SELECT pk_codelst FROM er_codelst WHERE codelst_type='"+codelst_type+"' AND codelst_subtyp='"+codelst_subtype+"'";
				query=session.createSQLQuery(sql);
				codelstLst=query.list();
				if(codelstLst!=null && codelstLst.size()>0){
					temp=codelstLst.get(0).toString();
					pkcodelst=Long.parseLong(temp);
					//log.debug("\n\n\n\nValue for codelst::::::::::::::::::::::::::::::::"+pkcodelst);
				}
				
				
			}
			
			
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return pkcodelst;
		
	}
	
	public static void createAlertInstance(Long orderId){
		
		createAlertInstance(orderId,null);
	}
	
	public static void createAlertInstance(Long orderId,Session session){
		//log.debug("orderId in createAlertInstance method is::::::::::"+orderId);
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			CallableStatement cs=null;
			cs=session.connection().prepareCall("{ call SP_ALERTS(?)}");
			cs.setLong(1, orderId);
			cs.execute();
			//log.debug("afetr execute prepare call");
			session.flush();
			
		}catch (Exception e) {
			// TODO: handle exception
			//log.debug("in createAlertInstance method:::::::"+e.getMessage());
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	public static void autoCompleteCRI(Long pkcordId){
		
		autoCompleteCRI(pkcordId,null);
	}
	
	public static void autoCompleteCRI(Long pkcordId,Session session){
		//log.debug("\n\n\n\n\n\n\n\n\npkcordId in autoCompleteCRI::::::::::"+pkcordId);
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			CallableStatement cs=null;
			cs=session.connection().prepareCall("{ call SP_CRI_TASKS(?)}");
			cs.setLong(1, pkcordId);
			cs.execute();
			//log.debug("afetr execute prepare call in autoCompleteCRI");
			session.flush();
			
		}catch (Exception e) {
			// TODO: handle exception
			//log.debug("Exception in autoCompleteCRI:::::::"+e.getMessage());
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
	
	public static int updateAlertStatus(Long alertInstanceId){
		return updateAlertStatus(alertInstanceId,null);
	}
	
	
	public static int updateAlertStatus(Long alertInstanceId,Session session){
		int cnt=0;
		String qry="";
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			qry="update cb_alert_instances set ALERT_CLOSE_FLAG=1 where PK_ALERT_INSTANCES="+alertInstanceId;
			log.debug("query isssssssss:::::::"+qry);
			cnt=session.createSQLQuery(qry).executeUpdate();
			session.getTransaction().commit();
			log.debug("No of records updated::::"+cnt);
		}catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return cnt;
	}
	
	public static String getMinimumCriteriaStat(Long orderId){
		return getMinimumCriteriaStat(orderId, null);
	}
	public static String getMinimumCriteriaStat(Long orderId,Session session){
		String minimumCriteriaStat="";
		List lst=new ArrayList();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="";
			qry="select CONFORMED_MINIMUM_CRITIRIA from CB_CORD_MINIMUM_CRITERIA where FK_ORDER_ID="+orderId;
			lst=session.createSQLQuery(qry).list();
			if(lst!=null && lst.size()>0){
				minimumCriteriaStat=(String)lst.get(0);
				//log.debug("minimumCriteriaStat::::::::::"+minimumCriteriaStat);
			}
		}catch(Exception e){
			e.printStackTrace();
			log.error(e.getMessage());
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return minimumCriteriaStat;
	}
	
	public static String getMinimumCriteriaFlagByCordId(Long cordId){
		return getMinimumCriteriaFlagByCordId(cordId, null);
	}
	public static String getMinimumCriteriaFlagByCordId(Long cordId,Session session){
		String minimumCriteriaFlag="";
		List lst=new ArrayList();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="";
			qry="select CONFORMED_MINIMUM_CRITIRIA from CB_CORD_MINIMUM_CRITERIA where FK_CORD_ID="+cordId;
			lst=session.createSQLQuery(qry).list();
			if(lst!=null && lst.size()>0){
				minimumCriteriaFlag=(String)lst.get(0);
				//log.debug("minimumCriteriaStat::::::::::"+minimumCriteriaFlag);
			}
			else{
				minimumCriteriaFlag="0";
			}
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return minimumCriteriaFlag;
	}
	
	public static List<CBUStatus> getAllCbuStatus(){
		return getAllCbuStatus(null);
	}
	
    public static List<CBUStatus> getAllCbuStatus(Session session){
    	List<CBUStatus> list = new ArrayList<CBUStatus>();
    	try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String sql = "From CBUStatus";
			Query query = session.createQuery(sql);
			list = query.list();
    	}catch(Exception e){
    		log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
    	return list;
	}
    public static List getPkOrders(Long cordId){
    	return getPkOrders(cordId,null);
    }
    
    public static List getPkOrders(Long cordId, Session session){
    	List pkOrderlist= new ArrayList();
    	try{
    		if(session==null){
    			session=HibernateUtil.getSessionFactory().getCurrentSession();
    			session.beginTransaction();
    		}
    		String sql="select o.pk_order from er_order_header oh,er_order o where"
    				+" o.fk_order_header=oh.pk_order_header and oh.order_entityid="+cordId+" and o.order_status not in ("
    				+"select pk_codelst from er_codelst where codelst_type='"+VelosMidConstants.ORDER_STATUS+"' and codelst_subtyp in('CANCELLED','"+VelosMidConstants.CLOSED+"','"+VelosMidConstants.RESOLVED+"'))";
    		
    		Query query=session.createSQLQuery(sql);
    		pkOrderlist=query.list();
    	}catch (Exception e) {
    		//log.debug("Exception in getPkOrders:"+e);
    		log.error(e.getMessage());
    		e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
    	return pkOrderlist;
    }
    
    public static String getNMDPResearchSampleFlag(Long siteId,Long cordId){
    	return getNMDPResearchSampleFlag(siteId,cordId, null);
    }
    public static String getNMDPResearchSampleFlag(Long siteId,Long cordId,Session session){
    	String flag=null;
    	String sql=null;
    	try{
    		if(session==null){
    			session=HibernateUtil.getSessionFactory().getCurrentSession();
    			session.beginTransaction();
    		}
    		if(siteId!=null && !siteId.equals("")){
    			
    			sql="select NMD_PRESEARCH_FLAG from cbb,er_site where cbb.FK_SITE=er_site.pk_site and er_site.pk_site="+siteId;
    			
    		}else if(cordId!=null && !cordId.equals("")){
    			
    			sql="select NMD_PRESEARCH_FLAG from cb_cord crd,cbb cb,er_site s where crd.fk_cbb_id=s.pk_site and cb.FK_SITE=s.pk_site and crd.pk_cord="+cordId;
    		}
    		log.debug("sql for getNMDPResearchSampleFlag:::::::::::"+sql);
    		List lst=session.createSQLQuery(sql).list();
    		if(lst!=null && lst.size()>0){
    			flag=(String)lst.get(0);
    		}
    	}catch(Exception e){
    		log.error(e.getMessage());
    	e.printStackTrace();
    	}finally{
    		if(session.isOpen()){
    			session.close();
    		}
    	}
    	return flag;
    }
    
    public static Long getReceipientId(Long cordId){
    	return getReceipientId(cordId,null);
    }
    
    public static Long getReceipientId(Long cordId,Session session){
    	Long receipientId=0l;
    	Long orderId=0l;
    	String sql="";
    	try{
    		if(cordId!=null){
    			orderId=getOrderIdFromCordId(cordId);
    		}
    		if(session==null){
    			session=HibernateUtil.getSessionFactory().getCurrentSession();
    			session.beginTransaction();
    		}
    		sql="SELECT pk_receipant FROM cb_receipant_info REC, ER_ORDER_RECEIPANT_INFO PRE WHERE PRE.FK_RECEIPANT=REC.PK_RECEIPANT AND pre.FK_ORDER_ID="+orderId;
    		log.debug("sql in getReceipientId()::::::::"+sql);
    		List lst=(List)session.createSQLQuery(sql).list();
    		if(lst!=null && lst.size()>0){
    			if(lst.get(0)!=null && !lst.get(0).equals("")){
    				receipientId=Long.parseLong(lst.get(0).toString());
    			}
    		}
    		log.debug("receipientId in getReceipientId()::::"+receipientId);
    	}catch(Exception e){
    		e.printStackTrace();
    		log.error(e.getMessage());
    	}finally{
    		if(session.isOpen()){
    			session.close();
    		}
    	}
    	return receipientId;
    }
    public static String getAcknowledgeCancelledOrderFlag(Long orderId,Long cordId){
    	return checkAckCondition(orderId,cordId);
    }
    
    public static OrderPojo getRequestDetails(Long orderId){
    	return getRequestDetails(orderId, null);
    }
    public static OrderPojo getRequestDetails(Long orderId,Session session){
    	OrderPojo orderPojo=new OrderPojo();
    	Orders orders=new Orders();
    	List<Orders> lst=new ArrayList<Orders>();
    	try{
    		if(session==null){
    			session=HibernateUtil.getSessionFactory().getCurrentSession();
    			session.beginTransaction();
    		}
    		log.debug("orderId::::::::::::"+orderId);
			lst=session.createQuery("from Orders where pk_OrderId="+orderId).list();
			log.debug("lst::::::::::::::::::::"+lst.get(0).getSampleAtLab());
			if(lst!=null && lst.size()>0){
				orders=(Orders)lst.get(0);
			}
			orderPojo=(OrderPojo)ObjectTransfer.transferObjects(orders, orderPojo);
    	}catch (Exception e) {
			// TODO: handle exception
    		log.error(e.getMessage());
		}finally{
			if(session!=null && session.isOpen()){
				session.close();
			}
		}
    	return orderPojo;
    }
    public static Long getOrderIdFromCordId(Long cordId){
    	return getOrderIdFromCordId(cordId, null);
    }
    public static Long getOrderIdFromCordId(Long cordId,Session session){
    	Long orderId=0l;
    	String qry="";
    	List lst=new ArrayList();
    	try{
    		if(session==null){
    			session=HibernateUtil.getSessionFactory().getCurrentSession();
    			session.beginTransaction();
    		}
    		if(cordId==null || cordId.equals("")){
    			cordId=0l;
    		}
    		qry="select nvl(max(pk_order),0) from er_order ord,er_order_header ordhdr where ord.fk_order_header=ordhdr.pk_order_header and ordhdr.order_entityid="+cordId;
    		log.debug("qry to get orderid from cordid::::::"+qry);
    		lst=session.createSQLQuery(qry).list();
    		if(lst!=null && lst.size()>0){
    			orderId=Long.parseLong(lst.get(0).toString());
    		}
    	}catch (Exception e) {
			// TODO: handle exception
    		e.printStackTrace();
    		log.error(e.getMessage());
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
    	return orderId;
    }
    public static String getFlagForTaskCheck(Long cordId){
    	return getFlagForTaskCheck(cordId, null);
    }
    public static String getFlagForTaskCheck(Long cordId,Session session){
    	String flag="0";
    	String qry="";
    	List lst=new ArrayList();
    	try{
    		if(session==null){
    			session=HibernateUtil.getSessionFactory().getCurrentSession();
    			session.beginTransaction();
    		}
    		if(cordId==null || cordId.equals("")){
    			cordId=0l;
    		}
    		qry="select f_is_new_task_needed("+cordId+") from dual";
    		lst=session.createSQLQuery(qry).list();
    		if(lst!=null && lst.size()>0){
    			flag=lst.get(0).toString();
    		}
    	}catch (Exception e) {
			// TODO: handle exception
    		e.printStackTrace();
    		log.error(e.getMessage());
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
    	return flag;
    }
    
    public static Long getDefaultPaperLoc(Long orderId){
		return getDefaultPaperLoc(orderId,null);
	}
	
	public static Long getDefaultPaperLoc(Long orderId,Session session){
		Long defaultpaploc=null;
		List lst=new ArrayList();
		String sql="";
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
				
			}
			if(orderId!=null && !orderId.equals("")){
				sql="select FK_SHP_PWRK_LOC from ERES.cbb cb,ERES.er_site s where cb.fk_site=s.pk_site and " +
					"s.pk_site=(select fk_cbb_id from eres.cb_cord cb,eres.er_order ord,eres.er_order_header ordhdr where ord.fk_order_header=ordhdr.pk_order_header and " +
					"cb.pk_cord=ordhdr.ORDER_ENTITYID and pk_order='"+orderId+"')";
			}
			else{
				sql="select 0 from dual";
			}
			//log.debug("query is:::"+sql);
			lst=session.createSQLQuery(sql).list();
			if(lst.size()>0 && lst.get(0)!=null){
				defaultpaploc=Long.parseLong(lst.get(0).toString());
			}
			
		}catch(Exception e){
log.error(e.getMessage());
			e.printStackTrace();
		}
		finally{
			if (session.isOpen()) {
				session.close();
			}
		}
		return defaultpaploc;
	}
	
	public static Long getMC_flag(Long cordId){
		return getMC_flag(cordId, null);
	}
	
	public static Long getMC_flag(Long cordId,Session session){
		Long mcflag=null;
		String sql=null;
		if(session==null){
			session=HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
		}
		try{
			sql="select case when count(*)>0 then 1  else 0 end MC_FLAG from cb_cord_minimum_criteria where fk_cord_id="+cordId+" and CONFORMED_MINIMUM_CRITIRIA=1";
			log.debug("sql in getMC_flag:::"+sql);
			BigDecimal fcrvalue = (BigDecimal)session.createSQLQuery(sql).list().get(0);
			mcflag = fcrvalue.longValue();
			log.debug("mcflag:::::::"+mcflag);
		}catch(Exception e){
			e.printStackTrace();
			log.error(e.getMessage());
		}finally{
			if(session.isOpen()){
				session.close();
			}
			return mcflag;
		}
	}
	
	public static Long getFCR_flag(Long cordId){
		return getFCR_flag(cordId, null);
	}
	
	public static Long getFCR_flag(Long cordId,Session session){
		Long fcrflag=null;
		String sql=null;
		if(session==null){
			session=HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
		}
		try{
			sql="select case when count(*)>0 then 1  else 0 end FCR_FLAG from cb_cord_final_review where fk_cord_id="+cordId+" and FINAL_REVIEW_CONFIRM_FLAG='Y'";
			log.debug("sql in getFCR_flag:::"+sql);
			BigDecimal fcrvalue = (BigDecimal)session.createSQLQuery(sql).list().get(0);
			
			fcrflag = fcrvalue.longValue();
			log.debug("fcrflag:::::::::::::::"+fcrflag);
		}catch(Exception e){
			e.printStackTrace();
			log.error(e.getMessage());
		}finally{
			if(session.isOpen()){
				session.close();
			}
			return fcrflag;
		}
	}
	
	public static Long getCBUAssessment_flag(Long cordId){
		return getCBUAssessment_flag(cordId, null);
	}
	
	public static Long getCBUAssessment_flag(Long cordId,Session session){
		Long cbuassflag=null;
		String sql=null;
		if(session==null){
			session=HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
		}
		try{
			sql="SELECT " +
			"  CASE " +
			"    WHEN COUNT(*)>0 " +
			"    THEN 1 " +
			"    ELSE 0 " +
			"  END CBUAssessment_FLAG " +
			"FROM er_order ord, " +
			"  er_order_header ordhdr " +
			"WHERE ord.fk_order_header  =ordhdr.pk_order_header " +
			"AND ordhdr.order_entityid  = " +cordId+
			"AND ordhdr.order_entitytype= " +
			"  (SELECT pk_codelst " +
			"  FROM er_codelst " +
			"  WHERE codelst_type='"+VelosMidConstants.ENTITY_CBU_CODE_TYPE+"' " +
			"  AND codelst_subtyp='"+VelosMidConstants.ENTITY_CBU_CODE_SUB_TYPE+"' " +
			"  ) " +
			"AND ord.order_type= " +
			"  (SELECT pk_codelst " +
			"  FROM er_codelst " +
			"  WHERE codelst_type='"+VelosMidConstants.ORDER_TYPE_NEW+"' " +
			"  AND codelst_subtyp='"+VelosMidConstants.OR+"' " +
			"  )";
			log.debug("sql in getFCR_flag:::"+sql);
			BigDecimal cbuassvalue = (BigDecimal)session.createSQLQuery(sql).list().get(0);
			cbuassflag = cbuassvalue.longValue();
			log.debug("cbuassflag:::::::::::::::"+cbuassflag);
		}catch(Exception e){
			e.printStackTrace();
			log.error(e.getMessage());
		}finally{
			if(session.isOpen()){
				session.close();
			}
			return cbuassflag;
		}
	}

	public static Long getEligTaskFlag(Long cordID,int i) {
		return getEligTaskFlag(cordID,i, null);
	}
	
	public static Long getEligTaskFlag(Long cordID,int i, Session session) {
		Long eliflag=null;
		String sql=null;
		if(session==null){
			session=HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
		}
		if(i==1){
		try{
			CallableStatement cs;
	        cs=session.connection().prepareCall("{call SP_NEW_FINAL_ELIG_TASK_FCR(?,?)}");
	        cs.setLong(1, cordID);
	        cs.registerOutParameter(2, Types.BIGINT);
	        cs.execute();
	        eliflag=cs.getLong(2);
	        
		}catch(Exception e){
			e.printStackTrace();
			log.error(e.getMessage());
		}finally{
			if(session.isOpen()){
				session.close();
			}
			
		}
		}
		else if(i==0){
			try{
				CallableStatement cs;
		        cs=session.connection().prepareCall("{call SP_FINAL_ELIG_TASK_FCR(?,?)}");
		        cs.setLong(1, cordID);
		        cs.registerOutParameter(2, Types.BIGINT);
		        cs.execute();
		        eliflag=cs.getLong(2);
		        
			}catch(Exception e){
				e.printStackTrace();
				log.error(e.getMessage());
			}finally{
				if(session.isOpen()){
					session.close();
				}
				
			}
		}
		return eliflag;
	}
	
	public static List<String> getRaceOrderByValue(String qStr) throws Exception {
		return getRaceOrderByValue(qStr, null);
	}
	public static List<String> getRaceOrderByValue(String qStr,Session session) {
		List<String> raceList = new ArrayList<String>();
		if(session==null){
			session=HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
		}
		try{
			raceList = session.createSQLQuery(qStr).list();
			Collections.sort(raceList);
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e.getMessage());
		}finally{
			if(session.isOpen()){
				session.close();
		}
		}
		// TODO Auto-generated method stub
		return raceList;
	}
}