package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

public class WidgetPojo {

	private Long id;
	private String applicationName;
	private String widgetName;
	private String state;
	private String url;
	private String helpUrl;
	private String description;
	private String iconSprite;
	private Boolean isAuthenticated;
	private Boolean isEditable;
	private Boolean isCustomizable;
	private Boolean isHeaderDisplayed;
	private Boolean isExpandable;
	private Boolean isMinimizable;
	private Boolean isRefreshable;
	private Boolean isClosable;
	private Boolean isResizable;
	private Boolean isDragable;
	private Boolean isMouseHoverEnabled;
	private Boolean isDefaultLoad;
	private Long resizeMinHeight;
	private Long resizeMinWidth;
	private Long expandPanelHeight;
	private Long expandPanelWidth;
	private Long editPanelHeight;
	private Long editPanelWidth;
	private Date ceatedDate;
	private Date lastUpdate;
	private Long versionNo;
	private String widgetDivId;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public String getWidgetName() {
		return widgetName;
	}
	public void setWidgetName(String widgetName) {
		this.widgetName = widgetName;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getHelpUrl() {
		return helpUrl;
	}
	public void setHelpUrl(String helpUrl) {
		this.helpUrl = helpUrl;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIconSprite() {
		return iconSprite;
	}
	public void setIconSprite(String iconSprite) {
		this.iconSprite = iconSprite;
	}
	public Boolean getIsAuthenticated() {
		return isAuthenticated;
	}
	public void setIsAuthenticated(Boolean isAuthenticated) {
		this.isAuthenticated = isAuthenticated;
	}
	public Boolean getIsEditable() {
		return isEditable;
	}
	public void setIsEditable(Boolean isEditable) {
		this.isEditable = isEditable;
	}
	public Boolean getIsCustomizable() {
		return isCustomizable;
	}
	public void setIsCustomizable(Boolean isCustomizable) {
		this.isCustomizable = isCustomizable;
	}
	public Boolean getIsHeaderDisplayed() {
		return isHeaderDisplayed;
	}
	public void setIsHeaderDisplayed(Boolean isHeaderDisplayed) {
		this.isHeaderDisplayed = isHeaderDisplayed;
	}
	public Boolean getIsExpandable() {
		return isExpandable;
	}
	public void setIsExpandable(Boolean isExpandable) {
		this.isExpandable = isExpandable;
	}
	public Boolean getIsMinimizable() {
		return isMinimizable;
	}
	public void setIsMinimizable(Boolean isMinimizable) {
		this.isMinimizable = isMinimizable;
	}
	public Boolean getIsRefreshable() {
		return isRefreshable;
	}
	public void setIsRefreshable(Boolean isRefreshable) {
		this.isRefreshable = isRefreshable;
	}
	public Boolean getIsClosable() {
		return isClosable;
	}
	public void setIsClosable(Boolean isClosable) {
		this.isClosable = isClosable;
	}
	public Boolean getIsResizable() {
		return isResizable;
	}
	public void setIsResizable(Boolean isResizable) {
		this.isResizable = isResizable;
	}
	public Boolean getIsDragable() {
		return isDragable;
	}
	public void setIsDragable(Boolean isDragable) {
		this.isDragable = isDragable;
	}
	public Boolean getIsMouseHoverEnabled() {
		return isMouseHoverEnabled;
	}
	public void setIsMouseHoverEnabled(Boolean isMouseHoverEnabled) {
		this.isMouseHoverEnabled = isMouseHoverEnabled;
	}
	public Boolean getIsDefaultLoad() {
		return isDefaultLoad;
	}
	public void setIsDefaultLoad(Boolean isDefaultLoad) {
		this.isDefaultLoad = isDefaultLoad;
	}
	public Long getResizeMinHeight() {
		return resizeMinHeight;
	}
	public void setResizeMinHeight(Long resizeMinHeight) {
		this.resizeMinHeight = resizeMinHeight;
	}
	public Long getResizeMinWidth() {
		return resizeMinWidth;
	}
	public void setResizeMinWidth(Long resizeMinWidth) {
		this.resizeMinWidth = resizeMinWidth;
	}
	public Long getExpandPanelHeight() {
		return expandPanelHeight;
	}
	public void setExpandPanelHeight(Long expandPanelHeight) {
		this.expandPanelHeight = expandPanelHeight;
	}
	public Long getExpandPanelWidth() {
		return expandPanelWidth;
	}
	public void setExpandPanelWidth(Long expandPanelWidth) {
		this.expandPanelWidth = expandPanelWidth;
	}
	public Long getEditPanelHeight() {
		return editPanelHeight;
	}
	public void setEditPanelHeight(Long editPanelHeight) {
		this.editPanelHeight = editPanelHeight;
	}
	public Long getEditPanelWidth() {
		return editPanelWidth;
	}
	public void setEditPanelWidth(Long editPanelWidth) {
		this.editPanelWidth = editPanelWidth;
	}
	public Date getCeatedDate() {
		return ceatedDate;
	}
	public void setCeatedDate(Date ceatedDate) {
		this.ceatedDate = ceatedDate;
	}
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public Long getVersionNo() {
		return versionNo;
	}
	public void setVersionNo(Long versionNo) {
		this.versionNo = versionNo;
	}
	public String getWidgetDivId() {
		return widgetDivId;
	}
	public void setWidgetDivId(String widgetDivId) {
		this.widgetDivId = widgetDivId;
	}
	
}
