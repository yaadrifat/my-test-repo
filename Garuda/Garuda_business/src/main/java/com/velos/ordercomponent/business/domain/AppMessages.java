/**
 * 
 */
package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author jamarendra
 *
 */
@Entity
@Table(name = "CB_APP_MESSAGES")
@SequenceGenerator(sequenceName = "SEQ_CB_APP_MESSAGES", name = "SEQ_CB_APP_MESSAGES",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true, dynamicUpdate = true)
public class AppMessages extends Auditable{
	
	private Long pkAppMsg;
	private String msgDesc;
	private Long msgStatus;
	private Long msgDispAt;
	private Long fkSite;
	private Long fkGrps;
	private Long msgDispOrder;
	private Date msgStartDt;
	private Date msgEndDt;
	private String deletedFlag;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CB_APP_MESSAGES")
	@Column(name = "PK_APP_MESSAGES")
	public Long getPkAppMsg() {
		return pkAppMsg;
	}

	public void setPkAppMsg(Long pkAppMsg) {
		this.pkAppMsg = pkAppMsg;
	}

	@Column(name = "MESSAGE_DESC")
	public String getMsgDesc() {
		return msgDesc;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	@Column(name = "MESSAGE_STATUS")
	public Long getMsgStatus() {
		return msgStatus;
	}

	public void setMsgStatus(Long msgStatus) {
		this.msgStatus = msgStatus;
	}

	@Column(name = "MESSAGE_DISP_AT")
	public Long getMsgDispAt() {
		return msgDispAt;
	}

	public void setMsgDispAt(Long msgDispAt) {
		this.msgDispAt = msgDispAt;
	}

	@Column(name = "FK_SITE")
	public Long getFkSite() {
		return fkSite;
	}

	public void setFkSite(Long fkSite) {
		this.fkSite = fkSite;
	}

	@Column(name = "FK_GRPS")
	public Long getFkGrps() {
		return fkGrps;
	}

	public void setFkGrps(Long fkGrps) {
		this.fkGrps = fkGrps;
	}

	@Column(name = "MSG_DISP_ORDER")
	public Long getMsgDispOrder() {
		return msgDispOrder;
	}

	public void setMsgDispOrder(Long msgDispOrder) {
		this.msgDispOrder = msgDispOrder;
	}

	@Column(name = "MSG_START_DT")
	public Date getMsgStartDt() {
		return msgStartDt;
	}

	public void setMsgStartDt(Date msgStartDt) {
		this.msgStartDt = msgStartDt;
	}

	@Column(name = "MSG_END_DT")
	public Date getMsgEndDt() {
		return msgEndDt;
	}

	public void setMsgEndDt(Date msgEndDt) {
		this.msgEndDt = msgEndDt;
	}
	@Column(name = "DELETED_FLAG")
	public String getDeletedFlag() {
		return deletedFlag;
	}

	public void setDeletedFlag(String deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	
}
	
