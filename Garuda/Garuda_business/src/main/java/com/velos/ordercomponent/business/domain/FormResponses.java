package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CB_FORM_RESPONSES")
@SequenceGenerator(sequenceName="SEQ_CB_FORM_RESPONSES",name="SEQ_CB_FORM_RESPONSES",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class FormResponses extends Auditable{
	private Long pkresponse;
	private Long fkform;
	private Long fkquestion;
	private Long entityId;
	private Long entityType;
	private Long responsecode;
	private String responsevalue;
	private String comment;
	private Long fkformresponses;
	private Long fkassessment;
	private Date dynaFormDate;
	private Long fkformversion;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_FORM_RESPONSES")
	@Column(name="PK_FORM_RESPONSES")
	public Long getPkresponse() {
		return pkresponse;
	}
	public void setPkresponse(Long pkresponse) {
		this.pkresponse = pkresponse;
	}
	
	@Column(name="FK_FORM")
	public Long getFkform() {
		return fkform;
	}
	public void setFkform(Long fkform) {
		this.fkform = fkform;
	}
	
	@Column(name="FK_QUESTION")
	public Long getFkquestion() {
		return fkquestion;
	}
	public void setFkquestion(Long fkquestion) {
		this.fkquestion = fkquestion;
	}
	
	@Column(name="ENTITY_ID")
	public Long getEntityId() {
		return entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	
	@Column(name="ENTITY_TYPE")
	public Long getEntityType() {
		return entityType;
	}
	public void setEntityType(Long entityType) {
		this.entityType = entityType;
	}
	
	@Column(name="RESPONSE_CODE")
	public Long getResponsecode() {
		return responsecode;
	}
	public void setResponsecode(Long responsecode) {
		this.responsecode = responsecode;
	}
	
	@Column(name="RESPONSE_VALUE")
	public String getResponsevalue() {
		return responsevalue;
	}
	public void setResponsevalue(String responsevalue) {
		this.responsevalue = responsevalue;
	}
	
	@Column(name="COMMENTS")
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	@Column(name="FK_FORM_RESPONSES")
	public Long getFkformresponses() {
		return fkformresponses;
	}
	public void setFkformresponses(Long fkformresponses) {
		this.fkformresponses = fkformresponses;
	}
	
	@Column(name="FK_ASSESSMENT")
	public Long getFkassessment() {
		return fkassessment;
	}
	public void setFkassessment(Long fkassessment) {
		this.fkassessment = fkassessment;
	}
	@Column(name="DATE_MOMANS_OR_FORMFILL")
	public Date getDynaFormDate() {
		return dynaFormDate;
	}
	public void setDynaFormDate(Date dynaFormDate) {
		this.dynaFormDate = dynaFormDate;
	}
	@Column(name="FK_FORM_VERSION")
	public Long getFkformversion() {
		return fkformversion;
	}
	public void setFkformversion(Long fkformversion) {
		this.fkformversion = fkformversion;
	}
}
