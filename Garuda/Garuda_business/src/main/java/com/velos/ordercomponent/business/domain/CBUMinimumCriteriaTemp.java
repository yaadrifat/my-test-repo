package com.velos.ordercomponent.business.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CB_CORD_TEMP_MINIMUM_CRITERIA")
@SequenceGenerator(sequenceName="SEQ_CB_TEMP_MINIMUM_CRITERIA",name="SEQ_CB_TEMP_MINIMUM_CRITERIA",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class CBUMinimumCriteriaTemp extends Auditable{
	
	private Long pkMinimumCritField;		//PK_CORD_MINIMUM_CRITERIA_FIELD
	private Long fkMinimumCrit;				//FK_CORD_MINIMUM_CRITERIA
	private Long bacterialResult;			//BACTERIAL_RESULT
	private Long fungalResult;				//FUNGAL_RESULT
	private Long hemoglobresult;			//HEMOGLOB_RESULT
	private Long hivresult;					//HIVRESULT
	private Long hivp;						//HIVP_RESULT
	private Long hbsag;						//HBSAG_RESULT
	private Long hbvnat;					//HBVNAT_RESULT
	private Long hcv;						//HCV_RESULT
	private Long tcruzichages;				//T_CRUZI_CHAGAS_RESULT
	private Long wnc;						//WNV_RESULT
	private String tncptwg;					//TNCKG_PT_WEIGHT_RESULT
	private Long viabresult;				//VIABILITY_RESULT
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_TEMP_MINIMUM_CRITERIA")
	@Column(name="PK_MINIMUM_CRITERIA_FIELD")
	public Long getPkMinimumCritField() {
		return pkMinimumCritField;
	}
	public void setPkMinimumCritField(Long pkMinimumCritField) {
		this.pkMinimumCritField = pkMinimumCritField;
	}
	@Column(name="FK_MINIMUM_CRITERIA")
	public Long getFkMinimumCrit() {
		return fkMinimumCrit;
	}
	public void setFkMinimumCrit(Long fkMinimumCrit) {
		this.fkMinimumCrit = fkMinimumCrit;
	}
	@Column(name="BACTERIAL_RESULT")
	public Long getBacterialResult() {
		return bacterialResult;
	}
	public void setBacterialResult(Long bacterialResult) {
		this.bacterialResult = bacterialResult;
	}
	@Column(name="FUNGAL_RESULT")
	public Long getFungalResult() {
		return fungalResult;
	}
	public void setFungalResult(Long fungalResult) {
		this.fungalResult = fungalResult;
	}
	@Column(name="HEMOGLOB_RESULT")
	public Long getHemoglobresult() {
		return hemoglobresult;
	}
	public void setHemoglobresult(Long hemoglobresult) {
		this.hemoglobresult = hemoglobresult;
	}
	@Column(name="HIVRESULT")
	public Long getHivresult() {
		return hivresult;
	}
	public void setHivresult(Long hivresult) {
		this.hivresult = hivresult;
	}
	@Column(name="HIVPRESULT")
	public Long getHivp() {
		return hivp;
	}
	public void setHivp(Long hivp) {
		this.hivp = hivp;
	}
	@Column(name="HBSAG_RESULT")
	public Long getHbsag() {
		return hbsag;
	}
	public void setHbsag(Long hbsag) {
		this.hbsag = hbsag;
	}
	@Column(name="HBVNAT_RESULT")
	public Long getHbvnat() {
		return hbvnat;
	}
	public void setHbvnat(Long hbvnat) {
		this.hbvnat = hbvnat;
	}
	@Column(name="HCV_RESULT")
	public Long getHcv() {
		return hcv;
	}
	public void setHcv(Long hcv) {
		this.hcv = hcv;
	}
	@Column(name="T_CRUZI_CHAGAS_RESULT")
	public Long getTcruzichages() {
		return tcruzichages;
	}
	public void setTcruzichages(Long tcruzichages) {
		this.tcruzichages = tcruzichages;
	}
	@Column(name="WNV_RESULT")
	public Long getWnc() {
		return wnc;
	}
	public void setWnc(Long wnc) {
		this.wnc = wnc;
	}
	@Column(name="TNCKG_PT_WEIGHT_RESULT")
	public String getTncptwg() {
		return tncptwg;
	}
	public void setTncptwg(String tncptwg) {
		this.tncptwg = tncptwg;
	}
	@Column(name="VIABILITY_RESULT")
	public Long getViabresult() {
		return viabresult;
	}
	public void setViabresult(Long viabresult) {
		this.viabresult = viabresult;
	}
}
