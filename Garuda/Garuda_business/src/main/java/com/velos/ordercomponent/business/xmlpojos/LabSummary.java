package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.datatype.XMLGregorianCalendar;

public class LabSummary {

	private String prcsng_strt_dte;
	private String cbu_prcsng_protocol_used;
	private String frz_dte;
	private String abo_bld_typ;
	private String rh_typ;
	private String rh_specifyother;
	private String bact_cult_start_date;
	private String bact_cult_sts;
	private String fung_cult_start_date;	
	private String fung_cult_sts;
	private String hemoglobin_screening;	
	private PreProcessing preProcessing;
	private PostProcessingPreCryoPreservation cryoPreservation;
	private PostCryoPreservationThaw cryoPreservationThaw;
	
	@XmlElement(name="prcsng_strt_dte")
	public String getPrcsng_strt_dte() {
		return prcsng_strt_dte;
	}
	public void setPrcsng_strt_dte(String prcsngStrtDte) {
		prcsng_strt_dte = prcsngStrtDte;
	}
	@XmlElement(name="cbu_prcsng_protocol_used")
	public String getCbu_prcsng_protocol_used() {
		return cbu_prcsng_protocol_used;
	}
	public void setCbu_prcsng_protocol_used(String cbuPrcsngProtocolUsed) {
		cbu_prcsng_protocol_used = cbuPrcsngProtocolUsed;
	}
	@XmlElement(name="frz_dte")
	public String getFrz_dte() {
		return frz_dte;
	}
	public void setFrz_dte(String frzDte) {
		frz_dte = frzDte;
	}
	@XmlElement(name="abo_bld_typ")
	public String getAbo_bld_typ() {
		return abo_bld_typ;
	}
	public void setAbo_bld_typ(String aboBldTyp) {
		abo_bld_typ = aboBldTyp;
	}
	@XmlElement(name="rh_typ")
	public String getRh_typ() {
		return rh_typ;
	}
	public void setRh_typ(String rhTyp) {
		rh_typ = rhTyp;
	}
	@XmlElement(name="rh_specifyother")
	public String getRh_specifyother() {
		return rh_specifyother;
	}
	public void setRh_specifyother(String rhSpecifyother) {
		rh_specifyother = rhSpecifyother;
	}
	@XmlElement(name="bact_cult_start_date")
	public String getBact_cult_start_date() {
		return bact_cult_start_date;
	}
	public void setBact_cult_start_date(String bactCultStartDate) {
		bact_cult_start_date = bactCultStartDate;
	}
	@XmlElement(name="bact_cult_sts")
	public String getBact_cult_sts() {
		return bact_cult_sts;
	}
	public void setBact_cult_sts(String bactCultSts) {
		bact_cult_sts = bactCultSts;
	}
	@XmlElement(name="fung_cult_start_date")
	public String getFung_cult_start_date() {
		return fung_cult_start_date;
	}
	public void setFung_cult_start_date(String fungCultStartDate) {
		fung_cult_start_date = fungCultStartDate;
	}
	@XmlElement(name="fung_cult_sts")
	public String getFung_cult_sts() {
		return fung_cult_sts;
	}
	public void setFung_cult_sts(String fungCultSts) {
		fung_cult_sts = fungCultSts;
	}
	@XmlElement(name="hemoglobin_screening")
	public String getHemoglobin_screening() {
		return hemoglobin_screening;
	}
	public void setHemoglobin_screening(String hemoglobinScreening) {
		hemoglobin_screening = hemoglobinScreening;
	}
	@XmlElement(name="PreProcessing")
	public PreProcessing getPreProcessing() {
		return preProcessing;
	}
	public void setPreProcessing(PreProcessing preProcessing) {
		this.preProcessing = preProcessing;
	}
	@XmlElement(name="PostProcessingPreCryoPreservation")
	public PostProcessingPreCryoPreservation getCryoPreservation() {
		return cryoPreservation;
	}
	public void setCryoPreservation(
			PostProcessingPreCryoPreservation cryoPreservation) {
		this.cryoPreservation = cryoPreservation;
	}
	@XmlElement(name="PostCryoPreservationThaw")
	public PostCryoPreservationThaw getCryoPreservationThaw() {
		return cryoPreservationThaw;
	}
	public void setCryoPreservationThaw(
			PostCryoPreservationThaw cryoPreservationThaw) {
		this.cryoPreservationThaw = cryoPreservationThaw;
	}
	
	
}
