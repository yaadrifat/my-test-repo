package com.velos.ordercomponent.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.velos.eres.web.user.UserJB;
import com.velos.ordercomponent.business.domain.AntigenEncoding;
import com.velos.ordercomponent.business.domain.Attachment;
import com.velos.ordercomponent.business.domain.CBB;
import com.velos.ordercomponent.business.domain.CBUStatus;
import com.velos.ordercomponent.business.domain.CBUUnitReport;
import com.velos.ordercomponent.business.domain.CBUploadInfo;
import com.velos.ordercomponent.business.domain.CodeList;
import com.velos.ordercomponent.business.domain.FormVersion;
import com.velos.ordercomponent.business.domain.Page;
import com.velos.ordercomponent.business.domain.RecipientInfo;
import com.velos.ordercomponent.business.domain.Specimen;
import com.velos.ordercomponent.business.domain.TaskList;
import com.velos.ordercomponent.business.domain.UserAlerts;
import com.velos.ordercomponent.business.pojoobjects.AdditionalIdsPojo;
import com.velos.ordercomponent.business.pojoobjects.AddressPojo;
import com.velos.ordercomponent.business.pojoobjects.AntigenPojo;
import com.velos.ordercomponent.business.pojoobjects.AppMessagesPojo;
import com.velos.ordercomponent.business.pojoobjects.AssessmentPojo;
import com.velos.ordercomponent.business.pojoobjects.AttachmentPojo;
import com.velos.ordercomponent.business.pojoobjects.BestHLAPojo;
import com.velos.ordercomponent.business.pojoobjects.CBBPojo;
import com.velos.ordercomponent.business.pojoobjects.CBBProcedureInfoPojo;
import com.velos.ordercomponent.business.pojoobjects.CBBProcedurePojo;
import com.velos.ordercomponent.business.pojoobjects.CBUCompleteReqInfoPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUFinalReviewPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUMinimumCriteriaPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUMinimumCriteriaTempPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUUnitReportPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUUnitReportTempPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUploadInfoPojo;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.ClinicalNotePojo;
import com.velos.ordercomponent.business.pojoobjects.CordHlaExtPojo;
import com.velos.ordercomponent.business.pojoobjects.CordHlaExtTempPojo;
import com.velos.ordercomponent.business.pojoobjects.CordImportHistoryPojo;
import com.velos.ordercomponent.business.pojoobjects.CordImportProcessMessagesPojo;
import com.velos.ordercomponent.business.pojoobjects.DataModificationRequestPojo;
import com.velos.ordercomponent.business.pojoobjects.DcmsLogPojo;
import com.velos.ordercomponent.business.pojoobjects.DumnPojo;
import com.velos.ordercomponent.business.pojoobjects.EligibilityDeclPojo;
import com.velos.ordercomponent.business.pojoobjects.EligibilityNewDeclPojo;
import com.velos.ordercomponent.business.pojoobjects.EmailAttachmentPojo;
import com.velos.ordercomponent.business.pojoobjects.EntityMultipleValuesPojo;
import com.velos.ordercomponent.business.pojoobjects.EntitySamplesPojo;
import com.velos.ordercomponent.business.pojoobjects.EntityStatusPojo;
import com.velos.ordercomponent.business.pojoobjects.EntityStatusReasonPojo;
import com.velos.ordercomponent.business.pojoobjects.FinalRevUploadInfoPojo;
import com.velos.ordercomponent.business.pojoobjects.FormResponsesPojo;
import com.velos.ordercomponent.business.pojoobjects.FormVersionPojo;
import com.velos.ordercomponent.business.pojoobjects.FundingGuidelinesPojo;
import com.velos.ordercomponent.business.pojoobjects.FundingSchedulePojo;
import com.velos.ordercomponent.business.pojoobjects.GrpsPojo;
import com.velos.ordercomponent.business.pojoobjects.HLAPojo;
import com.velos.ordercomponent.business.pojoobjects.IDMPojo;
import com.velos.ordercomponent.business.pojoobjects.LabMaintPojo;
import com.velos.ordercomponent.business.pojoobjects.LabTestGrpPojo;
import com.velos.ordercomponent.business.pojoobjects.LabTestPojo;
import com.velos.ordercomponent.business.pojoobjects.NotificationPojo;
import com.velos.ordercomponent.business.pojoobjects.OrderAttachmentPojo;
import com.velos.ordercomponent.business.pojoobjects.OrderPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.pojoobjects.PatLabsPojo;
import com.velos.ordercomponent.business.pojoobjects.PatientHLAPojo;
import com.velos.ordercomponent.business.pojoobjects.PersonPojo;
import com.velos.ordercomponent.business.pojoobjects.SearchPojo;
import com.velos.ordercomponent.business.pojoobjects.SitePojo;
import com.velos.ordercomponent.business.pojoobjects.TempHLAPojo;
import com.velos.ordercomponent.business.pojoobjects.UserAlertsPojo;
import com.velos.ordercomponent.business.pojoobjects.UserPojo;
import com.velos.ordercomponent.business.domain.FinalRevUploadInfo;


public interface VelosService {
	
	public SearchPojo velosSearch(SearchPojo searchPojo);
	
	public List<Object> getCBUCordInfoList(String objectName, String criteria,PaginateSearch paginateSearch);
	
	public Object getMailConfigurationByCode(String code );
	
	public NotificationPojo sendEmail(NotificationPojo notificationPojo,Boolean fromUI);
	
	public List<CodeList> getCodelst(String parentcode);
	
	public List<CBUStatus> getCBUStatusValues(String codeSubType);

	public Map getCodelistValues(Map codelstParams) throws Exception;
	
	public CBUUnitReportPojo saveCBUUnitReport(CBUUnitReportPojo cbuUnitReportPojo);
	
	public CordHlaExtPojo saveHlaInfo(CordHlaExtPojo cordHlaExtPojo);

	public CBUUnitReportTempPojo saveCBUUnitTempReport(CBUUnitReportTempPojo cbuUnitReportTempPojo);
	
	public CordHlaExtTempPojo saveHlaTempInfo(CordHlaExtTempPojo cordHlaExtTempPojo);
	
	public Object getCbuInfo(String objectName,String id,String fieldName) throws Exception;
	
	public AttachmentPojo addAttachments(AttachmentPojo attachmentPojo);
	
	public List<AttachmentPojo> addMultipleAttachments(List<AttachmentPojo> attachmentpojos);
	
	public CodeList getCodeListByCode(String codetype,String codesubtype );
	
	public List<CBUUnitReportTempPojo> getCordExtendedTempInfoData(Long extInfoId);

	public Object getObject(String objectName,String id,String fieldName)throws Exception;

	public CBUUnitReportTempPojo addReviewUnitReport(CBUUnitReportTempPojo cbuUnitReportTempPojo1) throws Exception;
	
	public List<CordHlaExtTempPojo> getCordExtendedHlaTempData(Long hlaExtId);
	
	public List<Page> getWidgetInfo();
	
	public List getpatientList(Long fkorderid,Long fkcordid);
	
	public List getShipmentDetails(Long fkorderid,Long fkcordid);
	
	public List addTaskList(TaskList taskList1,TaskList taskList2) throws Exception;

	public CordHlaExtTempPojo addReviewHlaUnitReport(CordHlaExtTempPojo cordHlaExtPojo);
	
	public OrderAttachmentPojo saveOrderAttachment(OrderAttachmentPojo orderAttachmentPojo);
	
	public Long getCodeListPkIds(String parentCode, String code) throws Exception;
	
	public CdrCbuPojo updateCodeListStatus(CdrCbuPojo cdrCbuPojo)throws Exception;
	
	public EntityStatusPojo updateEntityStatus(EntityStatusPojo entityStatusPojo,Boolean fromUI) throws Exception;
	
	public EntityStatusReasonPojo updateEntityStatusReason(EntityStatusReasonPojo entityStatusReasonPojo,Boolean fromUI) throws Exception;
	
	public Map<Long,CBB> getCBBMapWithIds();
	
	public List getCBBProcedureCount(String siteIds,String procedureCondi,int i);
	
	public List<SitePojo> getCBBDetails(Long id);
	
	public List<SitePojo> getCBBDetailsByIds(Long[] ids);
	
	public CBBPojo saveCBBDefaults(CBBPojo cbbPojo );
	
	public List getcbuFlaggedItems(Long cordId);
	
	public CdrCbuPojo getCordInfoById(CdrCbuPojo cdrCbuPojo);
	
	public String getQueryForOpenOrderList(CdrCbuPojo cdrCbuPojo) throws Exception ;
	
	public String queryForCategoryList() throws Exception ;
	
	public String populateDropdownValues(Long selVal) throws Exception;
	
	public Object getCordId(String objectName,String id,String fieldName) throws Exception;
	
	public CBUploadInfoPojo saveCatUploadInfo(CBUploadInfoPojo cbUploadInfoPojo);
	
	public LabMaintPojo saveNewLabTestInfo(LabMaintPojo labMaintPojo);
	
	public List<UserAlertsPojo> saveUserAlert(List<UserAlertsPojo> userAlertsLst);
	
	public String getAttachments(Long entityId,Long entityType,Long attachmentType,String categoryName) throws Exception ;
	
	public String getEntityDocuments(Long entityId,Long entityTypeId,Long attachmentType) throws Exception;
	
	public Long getCbuPkCordId(CdrCbuPojo cdrCbuPojo)throws Exception ;
	
	public String getOtherTestAttachInfo(Long entityId,Long entityTypeId,Long attachmentType,String categoryName) throws Exception;
	
	public String getHealthHistoryInfo(Long entityId,Long entityTypeId,Long attachmentType,String categoryName) throws Exception;
	
	public String getEligibilityAttachInfo(Long entityId,Long entityTypeId,Long attachmentType,String categoryName) throws Exception;
	
	public List<CBBProcedurePojo> getCBBProcedure(Long id,String siteIds,PaginateSearch paginateSearch,int i);
	
	public List<CBBProcedurePojo> getCBBProcedureSort(Long id,String siteIds,PaginateSearch paginateSearch,int i,String filterCondtion,String orderBy);
	
	public List<CBBProcedureInfoPojo> getCBBProcedureInfo(String id);
	
	public List getUserAlerts(Long userid,String alertType);
	
	public AddressPojo saveAddress(AddressPojo addressPojo);
	
	public PersonPojo savePerson(PersonPojo personPojo);
	
	public PersonPojo getPersonById(PersonPojo personPojo);
	
	public CBBProcedureInfoPojo saveCBBProcedureDetails(CBBProcedureInfoPojo cbbProcedureInfoPojo );
	
	public CBBProcedurePojo saveCBBProcedure(CBBProcedurePojo cbbProcedurePojo );
	
	public List<Object> getCbuinfo(String criteria,PaginateSearch paginateSearch,int i);
	
	public List<Object> getListCdrCBB(String criteria,PaginateSearch paginateSearch,int i);
	
	public List<Object> getHla(Long cordId,PaginateSearch paginateSearch,int i);
	
	public List<Object> getCbuAttachInfo(String cbuid);
	
	public String getcordinfopk() throws Exception;
	
	public CBBProcedureInfoPojo updateCbbProcedureInfo(CBBProcedureInfoPojo cbbProcedureInfoPojo, CBBProcedurePojo cbbProcedurePojo); 

    public List<Object> getdataentryCbuAttachInfo(String cbuid);
	
	public List<Object> queryForCbuList(CdrCbuPojo cdrCbuPojo);
	
	public List<Object>  getUserId(String loginId);
	
	public List<Object>  getUser(); 
	
	public List<Object> getTask(String loginId, String listid);
	
	public UserPojo getUserById(UserPojo userPojo);
	
	public String getLogNameByUserId(Long userId);
	
	public AddressPojo upadateuserMailId(AddressPojo addressPojo);
	
	public Long getDcmsAttachmentId();
	
	public String getDcmsFlagMethod(String flagValue);
	
	public DcmsLogPojo addDcmsLog(DcmsLogPojo dcmsLogPojo);
		
	public List<AdditionalIdsPojo> getCordInfoAdditionalIds(Long cordId);
	
	public List<AdditionalIdsPojo> saveCordInfoAdditionalIds(List<AdditionalIdsPojo> additionalIdsPojo,Boolean fromUI);	
	
	public CdrCbuPojo saveOrUpdateCordInfo(CdrCbuPojo cdrCbuPojo,Boolean fromUI);
 
	public List<HLAPojo> saveOrUpdateHlas(List<HLAPojo> hlaPojos,Boolean fromUI);
	
	public List getResultsForFilter(String orderType,String colName,String colData,String pkOrderType,String pending,String order_status);
	
	public Specimen saveOrUpdateSpecimen(Specimen specimenPojo,Boolean fromUI);
	
	public Boolean getValidateDbData(String entityName, String entityFiled, String entityValue, String entityField1, String entityValue1);
	
	public Boolean getValidateDbLongData(String entityName, String entityFiled, String entityValue, String entityField1, Long entityValue1);
	
	public Boolean getValidateDbLongDataWithAdditionalField(String entityName, String entityFiled, String entityValue, String entityFiled2, String entityValue2, String entityField1, Long entityValue1);
	
	public List<SitePojo> getSites();
	
	public List<SitePojo> getSitesWithPagination(PaginateSearch paginateSearch,int i);
	
	public List<SitePojo> getSitesWithPaginationSort(PaginateSearch paginateSearch,int i,String condition,String orderby);
	
	public List<SitePojo> getSitesByCriteria(String criteria,PaginateSearch paginateSearch,int i);
	
	public List<GrpsPojo> getGroups(Long siteId);
	
	public List getSitesCount(PaginateSearch paginateSearch,int i,String condition);
	
	public List<CdrCbuPojo> getCordsInProgress(Long cordId);
	
	public List<Object> getStatusHistory(Long entityID, Long entityType,String statusTypeCode,PaginateSearch paginateSearch,int i);
	
	public List<Object> getMultipleIneligibleReasons(Long pkEntityStatus,PaginateSearch paginateSearch,int i);
	
	public List<HLAPojo> getHlasByCord(Long cordId, Long entityType, String criteria);
	
	public List getOrderDetails(String orderId);
	
	public List getCtOrderDetails(String orderId);
	
	public ClinicalNotePojo saveClinicalNotes(ClinicalNotePojo clinicalNotePojo);
	
	public List<ClinicalNotePojo> updateClinicalNotes(List<ClinicalNotePojo> cnPojos);
	
	//public List<ClinicalNotePojo> clinicalNoteHistory(Long cordId);
	
	public CdrCbuPojo submitCordStatus(CdrCbuPojo cdrCbuPojo);
	
	public List<ClinicalNotePojo> amendNotes(Long pkNotes) ;
	
	public List getResultsForCancelFilter(String orderType,String colName,String colData,String pkOrderType,String cancelled,String order_status);
	
	public CBB getCBBBySiteId(Long siteId);

	public OrderPojo updateAcknowledgement(OrderPojo orderpojo);
	
	public void submitCtResolution(String pkorderId,String availDt,String resolbyCbb,String resolbyTc,String ackTcResolution,String acceptToCancel,String pkTempunavail,String pkcordId,String rejectDt,String rejectBy,String userID);

	public void submitHeResolution(String pkorderId,String availDt,String resolbyCbb,String resolbyTc,String ackTcResolution,String pkTempunavail);

	public List<EntityStatusReasonPojo> getReasonsByEntityStatusId(Long entityStatusId);
	
	public List<Object> getCbuInfoById(String entityType, String entityValue);
	
	public List<Attachment> getAttachmentInfoLst(Long attachmentId) throws Exception;
	
	public List<CBUUnitReport> getCbuUnitReportLst(Long cordId, Long attachmentId)throws Exception;
	
	public List<CBUploadInfo> getCbUploadInfoLst(Long attachmentId)throws Exception;
	
	public List getRevokeUnitReportLst(String cordID,PaginateSearch paginateSearch,int i, String condition, String orderBy) throws Exception;
	
	public List getUploadedDocumentLst(String cordID) throws Exception;
	
	public List getEresFormHtml(Long formId, Long cordId);
	
	public String checkShedShipDt(String orderid,String cbuid,String pkavailable);
	
	public SitePojo getSiteBySiteId(Long siteId);
	
	public SitePojo saveSite(SitePojo sitePojo);
	
	public String getNotesByCategory(Long entityId,Long entityType,Long categoryTypeID,String categoryName) throws Exception ;
	
	public CBUFinalReviewPojo saveCBUFinalReview(CBUFinalReviewPojo cbuFinalReviewPojo);
	
	public List<EmailAttachmentPojo> saveEmailDocuments(List<EmailAttachmentPojo> pojos);
	
	public EntityStatusReasonPojo loadEntityStatusReason(EntityStatusReasonPojo entityStatusReasonPojo);
	
	public Map<String,List> getCordUploadDocument(Long attachmentType,Long entityId,Long entityType);
	
	public List getTestsForIDM(Long fkSpecId);
	
	public List getDynaFormLst(Long cordId,String formName,String formvers,Long fkfrmversion);
	
	public void getOldFormVersion(Long entityId,String formName,boolean formUI);
	
	public List<Object> getPrevAssessData(Long entityId,Long fkformVersion);
	
	public List getFormVersionLst(Long cordId,String formName);
	
	public String getLatestFormVer(String formName);
	
	public FormVersionPojo saveFormVersion(FormVersionPojo formVersionPojo,boolean formUI);
	
	public List getIdmDeferCondition(String formName);
	
	public List getIdmResponseList(Long entityId);
	
	public Long getFormseq(Long cordId,String formName,String formVers);
	
	public FormVersionPojo getFormVerPojo(Long cordId,String formName,String formVers,Long formseq);
	
	public Long getformvmaxPk(Long cordId,String formName,String formvers);
	
	public Object getformvmax(Long cordId,String formName);
	
	public List getDynaQuesLst(Long cordId,String formName);
	
	public List getQuestionGroup(String formName,String formVers);
	
	public List saveorupdateDynaForms(List<FormResponsesPojo> formResponsesPojo,boolean formUI);
	
	public FormResponsesPojo saveFormData(FormResponsesPojo formobjects,Long entityId,String formName,boolean formUI);
	
	public Date getFormCreatedDate(Long cordId, String formName, String formVers);
	//public List getChildQuest(Long pkQuestion,String formName);
	
	public List getdataFromIDM(Long fkSpecId);
	
	public Long getLabTestpkbyshname(String shname);
	
	public List getpatlabpklist(Long fkSpecId);
	
	public List getIDMTests();
	
	public Long getLabgrppks(String grptype);
	
	public LabTestGrpPojo saveorupdateLTestgrp(LabTestGrpPojo labTestGrpPojo,LabTestPojo labTestPojo);
	
	public Long getLabTestInfoByName(String testName);
	
	public Long getLabTestInfoByName(String testName,String testShrtName);
	
	public List getLatTestlist(Long pklabgrp);
	
	public Long getIDMTestcount(Long fkspcid);
	
	public List getCbuStatus(String cbustatusin);
	
	public EntitySamplesPojo getEntitySampleInfo(Long entityId);
	
	public SitePojo getSiteBySiteName(String siteName,UserJB userObject);
	
	public List<SitePojo> getMultipleValuesByErSite(Long entityId,
			String entityType, String entitySubType,long isActive);	
	
	public List<SitePojo> getMultipleValuesByErSites(Long entityId,
			String entityType, String entitySubType);	
	
	public List<SitePojo> isSiteExist(String siteName);
	
	public List<SitePojo> isCbbIdExist(String cbbId);
	
	public EntitySamplesPojo saveOrUpdateSampInfo(EntitySamplesPojo entitySamplesPojo,Boolean fromUI);
	
	public CBUMinimumCriteriaPojo saveOrConfirmCriteria(CBUMinimumCriteriaPojo cbuMinimumCriteriaPojo);
	
	public CBUMinimumCriteriaTempPojo saveMinimumCriteriaTemp(CBUMinimumCriteriaTempPojo minimumCriteriaTempPojo);
	
	public List getPkOrders(Long cordId);
	
	public IDMPojo saveIDMValues(IDMPojo idmPojo);
	
	public CBUCompleteReqInfoPojo saveCBUCompleteReqInfo(CBUCompleteReqInfoPojo cbuCompleteReqInfoPojo);
	
	public Map<Long,List> getNotes(Long entityId);
	
	public Map<Long,List> getPfNotes(Long entityId, Long requestType);
	
	public ClinicalNotePojo  viewClinicalNotes(Long pkNotes);
	
	public List<ClinicalNotePojo> clinicalfilter(String Keyword, Long type, Long CordId);
	
	public List<CBBProcedurePojo> getCBBProcedures(Long id);
	
	public void updateSpecimenFormResponse(Long specimenId,List<Long> pkAcctForm);
	
	public List<PatLabsPojo> saveOrUpdatePatLabs(List<PatLabsPojo> patLabsPojos);
	
	public List<PatLabsPojo> savePatLabs(List<PatLabsPojo> patLabsPojos);
	
	public List<AssessmentPojo> saveUpdateAssessfromsession(List<AssessmentPojo> assessmentPojo,List patLabsPojos);
	
	public List<AssessmentPojo> saveOrUpdateAssessmentList(List<AssessmentPojo> assessmentPojoList);
	
	public List getLabTestgrp();
	
	public AssessmentPojo saveorUpdateAssessment(AssessmentPojo assessmentPojo);
	
	public List<EntityMultipleValuesPojo> getMultipleValuesByEntityId(Long entityId,String entityType,String entityMultipleType);
	
	public List<EntityMultipleValuesPojo> saveMultipleEntityValue(List<EntityMultipleValuesPojo> entityMultipleValuesPojos,Boolean fromUI);
	
	public List<EntityStatusReasonPojo> saveMultipleReasonValue(List<EntityStatusReasonPojo> entityStatusReasonPojo,Boolean fromUI);
	
	public void transientMultipleValues(List<Long> ids,Long entityId);
	
	public void transientMultipleReasonValues(List<Long> ids,Long entityStatus);
	
	public String generateGuid();
	
	public Long getTestIdByName(String testName);
	
	public List<PatLabsPojo> getOtherTestLst(Long specimenId,Long testId);
	
	public PatLabsPojo getPatLabsPojoById(Long specimenId,Long pkPatLabs);
	
	public AntigenPojo getAntigen(String genomicFormat,String locus,String typingMethod);
	
	public AntigenPojo getLatestAntigen(Long antigenId);
	
	public List getusersById(String criteria,PaginateSearch paginateSearch,int i);
	
	public List<TempHLAPojo> getTempHla(Long entityType,String registryId);
	
	public List<BestHLAPojo> getBestHla(Long entityType,Long entityId);	
	
    public void LCStoResolution(String pkresol,String ordertype,String pkorderId);

    public String getPkCbuStatus(String ordertype,String codetype,String statuscode);
    
    public String getTermialStatusByPkOrDesc(Long pkValue, String desc);

	public List<SitePojo> getMultipleValuesByErSite(Long entityId,
			String entityType, String entityMultipleType);

	public AppMessagesPojo updateAppMessages(AppMessagesPojo appMessagesPojo);
	
	public AppMessagesPojo getAppMessagesById(AppMessagesPojo appMessagesPojo);

	public EligibilityDeclPojo getEligibilityDeclQuestionsByEntity(Long entityId,Long entityType,Long pkEligQues);
	
	public DumnPojo getDumnQuestionsByEntity(Long entityId,Long entityType,Long pkEligQues);
	
	public AddressPojo getAddress(AddressPojo addressPojo);
	
	public List<ClinicalNotePojo> clinicalNoteHistory(Long cordId, Long noteCategory, Long requestId);
	
	//public String getRequestInfo(Long cordId) throws Exception;
	
	
	public EligibilityDeclPojo saveOrUpdateFinalEligibilityQuestions(EligibilityDeclPojo eDeclPojo);
	
	public DumnPojo saveOrUpdateDumnQuestions(DumnPojo dumnPojo);
	
	public String populateUserValues(Long cordId) throws Exception;

	public List maincbuRequestHistory(String pkcord,String pkorderId);

	
	public List cbuRequestHistory(String pkcord,String pkorder,String cdrUser);
	
	public List cbuHistory(String pkcord,String pkorder,PaginateSearch paginateSearch,int i,String cdrUser,String condition,String orderBy);
	
	public List<PatientHLAPojo> getPatientHla(Long entityType,Long entityId);
	
	public List cbbDetailPopup(String pksite,String ordType,String sampleAtlab);
	
	public String getTcResol(String orderId);
	
	public String checkForProcAssignment(String procId);
	
	public String getProcCordsMaxCollectionDt(String procId);
	
	public List getShedShipDts(String tshipDate,String siteId);
	
	public List<FundingGuidelinesPojo> getCBBGuidlinesByid(String siteId);
	
	public List<FundingGuidelinesPojo> updateCBBGuidlines(List<FundingGuidelinesPojo> fundingGuidelinesPojoLst);

	public List<FundingGuidelinesPojo> getValidfundingguidelinesdate(String birthDate,String Category,String cbbId);
	
	public List<String> getCodelstCustCols(Long pkCodelst);
	
	public void createDataRequest(DataModificationRequestPojo datarequestpojo);
	
	public List viewDataRequest(Long pkDatarequest,PaginateSearch paginateSearch,int i);
	
	public DataModificationRequestPojo getRequestDetails(DataModificationRequestPojo datarequestpojo);
	
	public void updateDMRequestStatus(DataModificationRequestPojo datarequestpojo);
	
	public List<LabTestPojo> getProcessingTestInfo(String groupType);
	
	public List<LabTestPojo> getOtherProcessingTestInfo(Long specimenId,String groupType);
	
	public LabTestPojo getOtrProcingTstInfoByTestId(Long testId);
	
	/*public List<PatLabsPojo> getPreTestListInfo(String preGroup,Long specimenId);*/
	
	public List<PatLabsPojo> getvalueSavePreTestList(List<PatLabsPojo> alist,Boolean fromUI);
	
	//public List<PatLabsPojo> getvalueSavePostTestList(List<PatLabsPojo> blist);
	
	public List<PatLabsPojo> getvalueSavePostTestList(List<PatLabsPojo> blist,Boolean fromUI);
	
	public List<PatLabsPojo> getPatLabsPojoInfo(Long SpecimenId ,Long testValueType);
	
	public List<SitePojo> getCBBListForSchAvail();
	
	public List<FundingSchedulePojo> getScheduledFundingRepLst(List<FundingSchedulePojo> fundingSchedulePojoLst,PaginateSearch paginateSearch,int i);
	
	public List<FundingSchedulePojo> getfundingSchedule(Long scheduledFundingId);
	
	public String getSiteInfoBySiteId(String siteColName,Long siteID);
	
	public AssessmentPojo getAssessmentPojoById(AssessmentPojo assessmentPojo);
	
	public List saveOrUpdateAssessments(List AssessmentPojos);

	public List<CdrCbuPojo> getCordsByCbuStatus(Long cordId,Long cbuStatus);
		
	public List<PatLabsPojo> getvalueSaveOtherTestList(List<PatLabsPojo> clist);
	
	public List getAttachmentByEntityId(Long entityId , Long categoryId,Long subCategoryId);
	
	public List getAttachmentByEntityId(Long entityId , Long categoryId, Long revokedFlag , int i);

	public String checkForProcName(String procName, String cbbId);
	
	public List getGroupsBySiteId(Long siteId);
	
	public List<Object> getAuditRowDataByEntityId(Long entityId,String viewName,PaginateSearch paginateSearch, int i, String condition, String orderBy);
	
	public List<Object> getAuditColumnDataByEntityId(Long entityId,String viewName);
	
	public Long getLabGrpPkByGrpType(String grpType);
	
	public Integer getCordByCountByMaternalRegistry(String maternalRegistryId);
	
	public String getCbbType(String Siteid);
	
	public List<CBUStatus> getAllCbuStatus();
	
	public List getQuestionsByFormId(Long formId);
	
	public String getCBUStatusCode(String pkcbustatus);
	
	public Long getfkCbbid(Long entityId);
	
	public String getidmdefervalue(Long entityId);
	
	public String getAssessmentDetail(Long pkassessment);
	
	public CordImportHistoryPojo saveCordImportHistory(CordImportHistoryPojo historyPojo);
	
	public List<CordImportHistoryPojo> getCordImportHistory(Long pkCordimportHistory,Long userPk,PaginateSearch paginateSearch);
	
	public List getCordImportHistoryJson(Long pkCordimportHistory,Long userPk,PaginateSearch paginateSearch,String condtion,String orderby);
	
	public List getCordImportHistoryCount(Long userPk,String condtion);
	
	public List<CordImportProcessMessagesPojo> saveCordImportErrorMessages(List<CordImportProcessMessagesPojo> messagesPojos);
	
	public List<CordImportProcessMessagesPojo> getCordImportErrorMessagesByHistoryId(Long importHistoryId,Integer level,String registryId);
	
	public void updateCordSearchable(Long cordSearchable,Long entityId);
	
	public void updateCordProgress(Long cordSearchable,Long entityId);
	
	public Integer getDomainCount(String objectName,String criteria,String pkName);
	
	public Integer saveCordListForSearch(List<Long> cordListForSearch);
	
	public Map<Long,RecipientInfo> getRecipientInfoMap(List<Long> cordList);
	
	public Long getSequence(String seqName);
	
	public List<AntigenEncoding> getEncodingList(List<Long> pkAntigenList);
	
	public String getLookUpFilterData(String filterValue);
	
	public List<Object> getCordListAsobject(String sql,PaginateSearch paginateSearch);
	
	public String getCbbInfoBySiteId(String CBBColName,Long siteID);
	
	public Long getMC_flag(Long cordId);
	
	public Long getFCR_flag(Long cordId);
	
	public Long getCBUAssessment_flag(Long cordId);
	
	public List<PatLabsPojo> getTestResultByTestNames(Long specimenId,Long fkTimingOfTest,List<String> testNames);
	
	public boolean getAssDetailsForLabSum(Long CordId,Long responceId,Long subEntityType,String questionNo);
	
	public void deleteHla(Long entityType,Long entityId);
	
	public void deleteTempHla(Long entityType,String registryId);
	
	public Map<Long,List<GrpsPojo>> getGroupsOfAllUserBySiteId(Long siteId);
	
	public EntityStatusPojo loadLatestEntityStatus(Long entityId,Long entityType,String status);
	
	public List<ClinicalNotePojo> saveCordMultipleNote(List<ClinicalNotePojo> clinicalNotePojo,Boolean fromUI, Long entityId, Long noteCategory);
	
	public String getSiteName(Long siteID);
	
	public String getSiteID(Long siteID);
		
	public EligibilityNewDeclPojo saveOrUpdateNewFinalEligibilityQuestions(EligibilityNewDeclPojo eDeclPojo);
	
	public EligibilityNewDeclPojo getEligibilityNewDeclQuestionsByEntity(Long entityId,Long entityType,Long pkEligQues);
	
	public void insertCordEntryHlaEsbRef(Long fkCord,UserJB user);	
	
	public void deleteFinalReviewDocs(Long entityType,Long entityId);
	
	public List<FinalRevUploadInfoPojo> saveFinalReviewDocs(List<FinalRevUploadInfoPojo> reviewPojos,Boolean fromUI);
	
	public List<FinalRevUploadInfo> getFinalReviewUploadDocLst(Long cordID,Long entityType) throws Exception;
	
	public List<SitePojo> getSitesForQueryBuilder();
	
	public String getFmhqQuestionResp(Long cordID)throws Exception;
	
	public Long getEligTaskFlag(Long cordID,int i);
	
	public String lastCBUStatus(Long cordID)throws Exception;

	public List<String> getRaceOrderByValue(String qStr) throws Exception;
	
	public List<Object> updateCdrCbuObject(Object obj) throws Exception;

	public List validateDataExistence(StringBuilder sqlQuery);
}