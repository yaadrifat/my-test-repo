package com.velos.ordercomponent.business.xmlpojos;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class ChronicBloodTransfusions {

	private String chron_bld_tranf_ind;
	/*private Relations relations;
	@XmlElement(name="Relations")
	public Relations getRelations() {
		return relations;
	}
	public void setRelations(Relations relations) {
		this.relations = relations;
	}*/
	@XmlElement(name="chron_bld_tranf_ind")
	public String getChron_bld_tranf_ind() {
		return chron_bld_tranf_ind;
	}
	public void setChron_bld_tranf_ind(String chronBldTranfInd) {
		chron_bld_tranf_ind = chronBldTranfInd;
	}
	private List<Relations> Relations;
	@XmlElement(name="Relations")
	public List<Relations> getRelations() {
		return Relations;
	}
	public void setRelations(List<Relations> relations) {
		Relations = relations;
	}
	
}
