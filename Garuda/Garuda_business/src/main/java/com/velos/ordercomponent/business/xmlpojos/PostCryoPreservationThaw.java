package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;

public class PostCryoPreservationThaw {
	
	private TNC_Post_CryoPreservation cryoPreservation;
	private nRBC_Post_CryoPreservation nRBCPostCryoPreservation;
	private CD34_Post_CryoPreservation cd34PostCryoPreservation;
	private CD3_Post_CryoPreservation cd3PostCryoPreservation;
	private CFU_Post_CryoPreservation cfuPostCryoPreservation;
	private Viability_Post_CryoPreservation postCryoPreservation;
	private TNC_PostCryo tncPostCryo;

	@XmlElement(name="TNC_Post-CryoPreservation")
	public TNC_Post_CryoPreservation getCryoPreservation() {
		return cryoPreservation;
	}

	public void setCryoPreservation(TNC_Post_CryoPreservation cryoPreservation) {
		this.cryoPreservation = cryoPreservation;
	}

	@XmlElement(name="nRBC_Post-CryoPreservation")
	public nRBC_Post_CryoPreservation getnRBCPostCryoPreservation() {
		return nRBCPostCryoPreservation;
	}

	public void setnRBCPostCryoPreservation(
			nRBC_Post_CryoPreservation nRBCPostCryoPreservation) {
		this.nRBCPostCryoPreservation = nRBCPostCryoPreservation;
	}

	@XmlElement(name="CD34_Post-CryoPreservation")
	public CD34_Post_CryoPreservation getCd34PostCryoPreservation() {
		return cd34PostCryoPreservation;
	}

	public void setCd34PostCryoPreservation(
			CD34_Post_CryoPreservation cd34PostCryoPreservation) {
		this.cd34PostCryoPreservation = cd34PostCryoPreservation;
	}

	@XmlElement(name="CD3_Post-CryoPreservation")
	public CD3_Post_CryoPreservation getCd3PostCryoPreservation() {
		return cd3PostCryoPreservation;
	}

	public void setCd3PostCryoPreservation(
			CD3_Post_CryoPreservation cd3PostCryoPreservation) {
		this.cd3PostCryoPreservation = cd3PostCryoPreservation;
	}

	@XmlElement(name="CFU_Post-CryoPreservation")
	public CFU_Post_CryoPreservation getCfuPostCryoPreservation() {
		return cfuPostCryoPreservation;
	}

	public void setCfuPostCryoPreservation(
			CFU_Post_CryoPreservation cfuPostCryoPreservation) {
		this.cfuPostCryoPreservation = cfuPostCryoPreservation;
	}

	@XmlElement(name="Viability_Post-CryoPreservation")
	public Viability_Post_CryoPreservation getPostCryoPreservation() {
		return postCryoPreservation;
	}

	public void setPostCryoPreservation(
			Viability_Post_CryoPreservation postCryoPreservation) {
		this.postCryoPreservation = postCryoPreservation;
	}

	@XmlElement(name="TNC_PostCryo")
	public TNC_PostCryo getTncPostCryo() {
		return tncPostCryo;
	}

	public void setTncPostCryo(TNC_PostCryo tncPostCryo) {
		this.tncPostCryo = tncPostCryo;
	}
	
	

}
