package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="ER_ORDER_HEADER")
@SequenceGenerator(sequenceName="SEQ_ER_ORDER_HEADER",name="SEQ_ER_ORDER_HEADER",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class orderHeader extends Auditable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long pkOrderHeader;
	private String orderGuid;
	private String orderNumber;
	private Long orderEntityId;
	private Long orderEntityType;
	private Date orderOpendate;
	private Date orderCloseDate;
	private Long orderHeaderType;
	private Long orderHeaderStatus;
	private Date orderHeaderStatusDate;
	private String orderRemarks;
	private Long orderRequestedBy;
	private Long orderApprovedBy;
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_ER_ORDER_HEADER")
	@Column(name="PK_ORDER_HEADER")
	public Long getPkOrderHeader() {
		return pkOrderHeader;
	}
	public void setPkOrderHeader(Long pkOrderHeader) {
		this.pkOrderHeader = pkOrderHeader;
	}
	
	@Column(name="ORDER_GUID")
	public String getOrderGuid() {
		return orderGuid;
	}
	public void setOrderGuid(String orderGuid) {
		this.orderGuid = orderGuid;
	}
	
	
	@Column(name="ORDER_NUM")
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	
	@Column(name="ORDER_ENTITYID")
	public Long getOrderEntityId() {
		return orderEntityId;
	}
	public void setOrderEntityId(Long orderEntityId) {
		this.orderEntityId = orderEntityId;
	}
	
	
	@Column(name="ORDER_ENTITYTYPE")
	public Long getOrderEntityType() {
		return orderEntityType;
	}
	public void setOrderEntityType(Long orderEntityType) {
		this.orderEntityType = orderEntityType;
	}
	
	@Column(name="ORDER_OPEN_DATE")
	public Date getOrderOpendate() {
		return orderOpendate;
	}
	public void setOrderOpendate(Date orderOpendate) {
		this.orderOpendate = orderOpendate;
	}
	
	@Column(name="ORDER_CLOSE_DATE")
	public Date getOrderCloseDate() {
		return orderCloseDate;
	}
	public void setOrderCloseDate(Date orderCloseDate) {
		this.orderCloseDate = orderCloseDate;
	}
	
	@Column(name="FK_ORDER_TYPE")
	public Long getOrderHeaderType() {
		return orderHeaderType;
	}
	public void setOrderHeaderType(Long orderHeaderType) {
		this.orderHeaderType = orderHeaderType;
	}
	
	
	@Column(name="FK_ORDER_STATUS")
	public Long getOrderHeaderStatus() {
		return orderHeaderStatus;
	}
	public void setOrderHeaderStatus(Long orderHeaderStatus) {
		this.orderHeaderStatus = orderHeaderStatus;
	}
	
	@Column(name="ORDER_STATUS_DATE")
	public Date getOrderHeaderStatusDate() {
		return orderHeaderStatusDate;
	}
	public void setOrderHeaderStatusDate(Date orderHeaderStatusDate) {
		this.orderHeaderStatusDate = orderHeaderStatusDate;
	}
	
	@Column(name="ORDER_REMARKS")
	public String getOrderRemarks() {
		return orderRemarks;
	}
	public void setOrderRemarks(String orderRemarks) {
		this.orderRemarks = orderRemarks;
	}
	
	@Column(name="ORDER_REQUESTED_BY")
	public Long getOrderRequestedBy() {
		return orderRequestedBy;
	}
	public void setOrderRequestedBy(Long orderRequestedBy) {
		this.orderRequestedBy = orderRequestedBy;
	}
	
	@Column(name="ORDER_APPROVED_BY")
	public Long getOrderApprovedBy() {
		return orderApprovedBy;
	}
	public void setOrderApprovedBy(Long orderApprovedBy) {
		this.orderApprovedBy = orderApprovedBy;
	}
	
	
	
	
	

}
