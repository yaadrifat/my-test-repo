package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

public class ActiveOrderPojo {
	
	private Long orderId;
	private Long cordId;
	private String cbuRegId;
	private Long siteId;
	private String siteName;
	private String localCbuId;
	private String requestType;
	private String priority;
	private String requestDate;
	private int noOfDays;
	private String status;
	private String resolution;
	private String assignTo;
	private String reviewedBy;
	private String acknowledgeResol;
	private Long pksite;
	private Long fkordertype;
	private Long fkorderstatus;
	private Long fkorderresol;
	private Long fkassignto;
	private Long fkreviewby;
	private Long fkpriority;
	
	
	public Long getFkpriority() {
		return fkpriority;
	}
	public void setFkpriority(Long fkpriority) {
		this.fkpriority = fkpriority;
	}
	public Long getFkassignto() {
		return fkassignto;
	}
	public void setFkassignto(Long fkassignto) {
		this.fkassignto = fkassignto;
	}
	public Long getFkreviewby() {
		return fkreviewby;
	}
	public void setFkreviewby(Long fkreviewby) {
		this.fkreviewby = fkreviewby;
	}
	public Long getFkorderresol() {
		return fkorderresol;
	}
	public void setFkorderresol(Long fkorderresol) {
		this.fkorderresol = fkorderresol;
	}
	public Long getFkorderstatus() {
		return fkorderstatus;
	}
	public void setFkorderstatus(Long fkorderstatus) {
		this.fkorderstatus = fkorderstatus;
	}
	public Long getFkordertype() {
		return fkordertype;
	}
	public void setFkordertype(Long fkordertype) {
		this.fkordertype = fkordertype;
	}
	public Long getPksite() {
		return pksite;
	}
	public void setPksite(Long pksite) {
		this.pksite = pksite;
	}
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public Long getCordId() {
		return cordId;
	}
	public void setCordId(Long cordId) {
		this.cordId = cordId;
	}
	public String getCbuRegId() {
		return cbuRegId;
	}
	public void setCbuRegId(String cbuRegId) {
		this.cbuRegId = cbuRegId;
	}
	public Long getSiteId() {
		return siteId;
	}
	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public String getLocalCbuId() {
		return localCbuId;
	}
	public void setLocalCbuId(String localCbuId) {
		this.localCbuId = localCbuId;
	}
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}
	public int getNoOfDays() {
		return noOfDays;
	}
	public void setNoOfDays(int noOfDays) {
		this.noOfDays = noOfDays;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getResolution() {
		return resolution;
	}
	public void setResolution(String resolution) {
		this.resolution = resolution;
	}
	public String getAssignTo() {
		return assignTo;
	}
	public void setAssignTo(String assignTo) {
		this.assignTo = assignTo;
	}
	public String getReviewedBy() {
		return reviewedBy;
	}
	public void setReviewedBy(String reviewedBy) {
		this.reviewedBy = reviewedBy;
	}
	public String getAcknowledgeResol() {
		return acknowledgeResol;
	}
	public void setAcknowledgeResol(String acknowledgeResol) {
		this.acknowledgeResol = acknowledgeResol;
	}

}
