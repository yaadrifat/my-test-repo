package com.velos.ordercomponent.business.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 *@author Gaurav
 *
 */

@Entity
@Table(name="ER_LABTEST")
@SequenceGenerator(sequenceName="SEQ_ER_LABTEST",name="SEQ_ER_LABTEST",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)

public class LabTest extends Auditable{
	
	
	private Long pkLabtest;
	private String labtestName;
	private String shrtName;
	private String labtestCpt;
	private Long fkADVNci;
	private String grdCalc;
	private Long labtestSeq;
	private String labtestHower;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_ER_LABTEST")
	@Column(name="PK_LABTEST")
    public Long getPkLabtest() {
		return pkLabtest;
	}
	public void setPkLabtest(Long pkLabtest) {
		this.pkLabtest = pkLabtest;
	}
	
	@Column(name="LABTEST_NAME")
	public String getLabtestName() {
		return labtestName;
	}
	public void setLabtestName(String labtestName) {
		this.labtestName = labtestName;
	}
	
	@Column(name="LABTEST_SHORTNAME")
	public String getShrtName() {
		return shrtName;
	}
	public void setShrtName(String shrtName) {
		this.shrtName = shrtName;
	}
	
	@Column(name="LABTEST_CPT")
	public String getLabtestCpt() {
		return labtestCpt;
	}
	public void setLabtestCpt(String labtestCpt) {
		this.labtestCpt = labtestCpt;
	}
	
	@Column(name="FK_LKP_ADV_NCI")
	public Long getFkADVNci() {
		return fkADVNci;
	}
	public void setFkADVNci(Long fkADVNci) {
		this.fkADVNci = fkADVNci;
	}
	
	@Column(name="GRADE_CALC")
	public String getGrdCalc() {
		return grdCalc;
	}
	public void setGrdCalc(String grdCalc) {
		this.grdCalc = grdCalc;
	}
	
	@Column(name="LABTEST_SEQ")
	public Long getLabtestSeq() {
		return labtestSeq;
	}
	public void setLabtestSeq(Long labtestSeq) {
		this.labtestSeq = labtestSeq;
	}
	
	@Column(name="LABTEST_HOWER")
	
	public String getLabtestHower() {
		return labtestHower;
	}
	public void setLabtestHower(String labtestHower) {
		this.labtestHower = labtestHower;
	}
	
	
	
	
	

}
