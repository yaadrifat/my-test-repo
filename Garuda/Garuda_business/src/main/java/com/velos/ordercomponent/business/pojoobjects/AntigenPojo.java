package com.velos.ordercomponent.business.pojoobjects;

import com.velos.ordercomponent.business.domain.AntigenEncoding;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class AntigenPojo extends AuditablePojo {
	
	private Long pkAntigen;
	private Long antigenId;
	private String HlaCode;
	private String HlaMethod;	
	private Boolean validInd;
	private Boolean activeInd;	
	
	public Long getPkAntigen() {
		return pkAntigen;
	}
	public void setPkAntigen(Long pkAntigen) {
		this.pkAntigen = pkAntigen;
	}
	public Long getAntigenId() {
		return antigenId;
	}
	public void setAntigenId(Long antigenId) {
		this.antigenId = antigenId;
	}	
	public String getHlaCode() {
		return HlaCode;
	}
	public void setHlaCode(String hlaCode) {
		HlaCode = hlaCode;
	}
	public String getHlaMethod() {
		return HlaMethod;
	}
	public void setHlaMethod(String hlaMethod) {
		HlaMethod = hlaMethod;
	}
	public Boolean getValidInd() {
		return validInd;
	}
	public void setValidInd(Boolean validInd) {
		this.validInd = validInd;
	}
	public Boolean getActiveInd() {
		return activeInd;
	}
	public void setActiveInd(Boolean activeInd) {
		this.activeInd = activeInd;
	}	
	

}
