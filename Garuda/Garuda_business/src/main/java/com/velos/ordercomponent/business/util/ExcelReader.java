package com.velos.ordercomponent.business.util;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

public class ExcelReader {

	public static int TARGETLIST_STARTROWNUM =0; 
	public static int TARGETLIST_STARTCOLUMN =0;

	public ExcelReader(){
		try{
		//System.out.println(" init ");	
		ResourceBundle resource = ResourceBundle.getBundle("TargetList");
		TARGETLIST_STARTCOLUMN = Integer.parseInt(resource.getString("TARGETLIST_STARTCOLUMN"));
		TARGETLIST_STARTROWNUM = Integer.parseInt(resource.getString("TARGETLIST_STARTROWNUM"));
		}catch(Exception e){
			//System.out.println("Exception in Reader ::" + e);
			e.printStackTrace();
		}
	}
	

	
	public List<String[]> getMRNValues(InputStream input){
		List<String[]> lstResult = null;
		try{
			
			lstResult = fileProcess(input);
			
			//System.out.println(" ***************** Result ***********************");
			if(lstResult!=null && lstResult.size() >0){
				String[] strValue;
				for(int i=0;i<lstResult.size();i++){
					strValue = (String[])lstResult.get(i);
					System.out.print("[" +i + "]" );
					for(int j=0;j<strValue.length;j++){
						System.out.print(strValue[j] + " ->  " );
					}
					//System.out.println("");
				}
			}
			
		}catch(Exception e){
			//System.out.println(" Exception in getMRNValues :: " + e );
			e.printStackTrace();
		}
		return lstResult;
		
	}
	
	public  ArrayList<String[]> fileProcess(InputStream input) {

        ArrayList<String[]> arrlist = new ArrayList<String[]>();
        try {
            POIFSFileSystem fs = new POIFSFileSystem(input);
            HSSFWorkbook wb = new HSSFWorkbook(fs);
            int noOfSheet = wb.getNumberOfSheets();
            noOfSheet = 1;
           
            String cellValue;
            String[] cellValues;
            for (int k = 0; k < noOfSheet; k++) {
                HSSFSheet sheet = wb.getSheetAt(k);
                if (sheet != null && sheet.getPhysicalNumberOfRows() > 1) {
                	  for (int j = TARGETLIST_STARTROWNUM; j < sheet.getPhysicalNumberOfRows(); j++) {
                            HSSFRow row = sheet.getRow(j);
                            if (row != null && row.getPhysicalNumberOfCells() > 0) {
                            	cellValues = new String[row.getPhysicalNumberOfCells()];
                                for (int i = TARGETLIST_STARTCOLUMN; i < row.getLastCellNum(); i++) {
                                    HSSFCell cell = row.getCell(new Integer(i).shortValue());
                                    if (cell != null) {
                                    	//System.out.println( " Cell type " + cell.getCellType());
                                        switch (cell.getCellType()) {
                                       
                                        case HSSFCell.CELL_TYPE_NUMERIC: {
                                        	cellValue = String.valueOf(new Double(cell.getNumericCellValue()).intValue());
                                        	cellValues[i] = cellValue;     
                                            break;
                                        }
                                        case HSSFCell.CELL_TYPE_STRING: {
                                        	cellValue = cell.getStringCellValue();
                                        	if(cellValue != null  ){
                                        		cellValues[i] = cellValue;
                                        	}
                                            break;
                                        }default:
                                            break;
                                        }//end of switch
                                    }// end of if to check if the cell is null
                                }// end of for loop of lastcell num
                                arrlist.add(cellValues);
                            }//end of if to check row is null
                        }//end for loop of lastRownum
                    
                }
            }
        
        } catch (Exception ex) {
    		//System.out.println(" Exception in fileProcess ::"+ex);
    		ex.printStackTrace();
        }
        return arrlist;
    }



    
}
