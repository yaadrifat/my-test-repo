package com.velos.ordercomponent.helper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.velos.ordercomponent.business.domain.CBUUnitReport;
import com.velos.ordercomponent.business.domain.CBUUnitReportTemp;
import com.velos.ordercomponent.business.domain.CordHlaExt;
import com.velos.ordercomponent.business.domain.CordHlaExtTemp;
import com.velos.ordercomponent.business.pojoobjects.CBUUnitReportTempPojo;
import com.velos.ordercomponent.business.pojoobjects.CordHlaExtTempPojo;
import com.velos.ordercomponent.business.util.HibernateUtil;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.VelosMidConstants;

public class ReviewUnitReportHelper {

	public static final Log log = LogFactory
			.getLog(ReviewUnitReportHelper.class);
	private static CordHlaExt sdv;

	public static List<CBUUnitReportTempPojo> getCordExtendedTempInfoData(
			Long extInfoId) {
		return getCordExtendedTempInfoData(extInfoId, null);
	}

	public static List<CBUUnitReportTempPojo> getCordExtendedTempInfoData(
			Long extInfoId, Session session) {

		boolean sessionFlag = false;
		CBUUnitReportTemp cbuUnitReportTemp = null;
		CBUUnitReportTempPojo cbuUnitReportTempPojo = new CBUUnitReportTempPojo();
		CBUUnitReportTempPojo cbuUnitReportTempPojo1 = new CBUUnitReportTempPojo();
		List<CBUUnitReportTemp> unitReportTempList = null;
		List<CBUUnitReportTempPojo> list = new ArrayList<CBUUnitReportTempPojo>();
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}

			String sql = "select {exttemp.*} from CBU_UNIT_REPORT_REVIEW exttemp where exttemp.fk_cord_ext_info= ?";
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity("exttemp", CBUUnitReportTemp.class);
			query.setLong(0, extInfoId);
			unitReportTempList = (List<CBUUnitReportTemp>) query.list();
			cbuUnitReportTempPojo = (CBUUnitReportTempPojo) ObjectTransfer
					.transferObjects(unitReportTempList.get(0),
							cbuUnitReportTempPojo);
			cbuUnitReportTempPojo1 = (CBUUnitReportTempPojo) ObjectTransfer
					.transferObjects(unitReportTempList.get(1),
							cbuUnitReportTempPojo1);
			list.add(cbuUnitReportTempPojo);
			list.add(cbuUnitReportTempPojo1);
//			if (sessionFlag) {
//				 close session, if session created in this method
//				 session.getTransaction().commit();
//			}
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return list;
	}

	public static CBUUnitReportTempPojo addReviewUnitReport(
			CBUUnitReportTempPojo cbuUnitReportTempPojo1) throws Exception {

		try {

			CBUUnitReport cbuUnitReport = new CBUUnitReport();
			if (cbuUnitReportTempPojo1.getFkCordExtInfo() != null
					&& cbuUnitReportTempPojo1.getFkCordExtInfo() > 0) {
				Object cbuUnitObject = velosHelper.getObject("CBUUnitReport",
						cbuUnitReportTempPojo1.getFkCordExtInfo(),
						VelosMidConstants.PK_CBU_UNIT_REPORT);
				cbuUnitReport = (CBUUnitReport) ObjectTransfer.transferObjects(
						cbuUnitObject, cbuUnitReport);
			}
			cbuUnitReport = (CBUUnitReport) ObjectTransfer.transferObjects(
					cbuUnitReportTempPojo1, cbuUnitReport);
			cbuUnitReport = addReviewUnitReport(cbuUnitReport);

		} catch (Exception e) {
			//log.debug(e);
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return cbuUnitReportTempPojo1;
	}

	private static CordHlaExt addReviewUnitReport(CordHlaExt corHlaExt) {
		// TODO Auto-generated method stub
		return addReviewUnitReport(corHlaExt, null);
	}

	private static CordHlaExt addReviewUnitReport(CordHlaExt corHlaExt,
			Session session) {
		
		
		try {
			log.debug("ReviewUnitReportHelper.addReviewUnitReport() start");
			boolean sessionFlag = false;
			// check session
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if (corHlaExt.getPkCordHlaExt() != null
					&& corHlaExt.getPkCordHlaExt() > 0) {
				session.update(corHlaExt);
			} else {
				session.save(corHlaExt);
			}
			if (sessionFlag) {
				// close session, if session created in this method
				session.getTransaction().commit();
			}

		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			// throw new P1VBaseException();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		//log.debug("ReviewUnitReportHelper.addReviewUnitReport() end");
		return corHlaExt;
	}

	private static CBUUnitReport addReviewUnitReport(CBUUnitReport cbuUnitReport) {
		// TODO Auto-generated method stub
		return addReviewUnitReport(cbuUnitReport, null);
	}

	private static CBUUnitReport addReviewUnitReport(
			CBUUnitReport cbuUnitReport, Session session) {
		try {
			//log.debug("ReviewUnitReportHelper.addReviewUnitReport() start");
			boolean sessionFlag = false;
			// check session
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if (cbuUnitReport.getPkCordExtInfo() != null
					&& cbuUnitReport.getPkCordExtInfo() > 0) {
				session.update(cbuUnitReport);
			} else {
				session.save(cbuUnitReport);
			}
			if (sessionFlag) {
				// close session, if session created in this method
				session.getTransaction().commit();
			}

		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			// throw new P1VBaseException();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		//log.debug("ReviewUnitReportHelper.addReviewUnitReport() end");
		return cbuUnitReport;
	}

	public static List<CordHlaExtTempPojo> getCordExtendedHlaTempData(
			Long hlaExtId) {

		return getCordExtendedHlaTempData(hlaExtId, null);
	}

	public static List<CordHlaExtTempPojo> getCordExtendedHlaTempData(
			Long hlaExtId, Session session) {
		boolean sessionFlag = false;
		System.out.print("hla ext id" + hlaExtId);
		CordHlaExtTemp cordHlaExtTemp = null;
		CordHlaExtTempPojo cordHlaExtTempPojo = new CordHlaExtTempPojo();
		CordHlaExtTempPojo cordHlaExtTempPojo1 = new CordHlaExtTempPojo();
		List<CordHlaExtTemp> hlaExtTemps = null;
		List<CordHlaExtTempPojo> list = new ArrayList<CordHlaExtTempPojo>();
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "select {hlatemp.*} from CBU_UNIT_REPORT_HLA_REVIEW hlatemp where hlatemp.fk_cord_hla_ext= ?";
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity("hlatemp", CordHlaExtTemp.class);
			query.setLong(0, hlaExtId);
			hlaExtTemps = (List<CordHlaExtTemp>) query.list();
			//log.debug("hla ext id size" + hlaExtTemps.size());
			cordHlaExtTempPojo = (CordHlaExtTempPojo) ObjectTransfer
					.transferObjects(hlaExtTemps.get(0), cordHlaExtTempPojo);
			cordHlaExtTempPojo1 = (CordHlaExtTempPojo) ObjectTransfer
					.transferObjects(hlaExtTemps.get(1), cordHlaExtTempPojo1);
			list.add(cordHlaExtTempPojo);
			list.add(cordHlaExtTempPojo1);
//			if (sessionFlag) {
//				 close session, if session created in this method
//				 session.getTransaction().commit();
//			}
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return list;
	}

	public static CordHlaExtTempPojo addReviewHlaUnitReport(
			CordHlaExtTempPojo cordHlaExtPojo) {
		try {
			CordHlaExt corHlaExt = new CordHlaExt();
			if (cordHlaExtPojo.getFkCordHlaExt() != null
					&& cordHlaExtPojo.getFkCordHlaExt() > 0) {
				Object hlaUnitObject = velosHelper.getObject("CordHlaExt",
						cordHlaExtPojo.getFkCordHlaExt(),
						VelosMidConstants.PK_CBU_UNIT_REPORT_HLA);
				corHlaExt = (CordHlaExt) ObjectTransfer.transferObjects(
						hlaUnitObject, corHlaExt);
			}
			corHlaExt = (CordHlaExt) ObjectTransfer.transferObjects(
					cordHlaExtPojo, corHlaExt);
			corHlaExt = addReviewUnitReport(corHlaExt);

		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return cordHlaExtPojo;
	}
	public static List<Object> getCbuAttachInfo(String cbuid) {
		String appendQuery = "";

		if (cbuid != null && !cbuid.equals("")) {
			appendQuery = "select cordinfo.cdrCbuId,extemp.pkCordExtInfo,hlaext.pkCordHlaExt,att.attachmentId,att.fileName,att.documentType,att.documentFile"
					+ " from CBUUnitReport extemp, CdrCbu cordinfo, CordHlaExt hlaext,Attachment att, CodeList cl"
					+ " where hlaext.fkCordCdrCbuId = cordinfo.cordID and extemp.fkCordCdrCbuId = cordinfo.cordID and extemp.pkCordExtInfo =  hlaext.fkCordExtInfo and att.attachmentId = extemp.attachmentId and cl.pkCodeId =  extemp.cordCompletedStatus and upper(cl.subType)='PENDING' and lower(cordinfo.cdrCbuId) like lower('%"+ cbuid + "%')";
		} else
			appendQuery = "select cordinfo.cdrCbuId,extemp.pkCordExtInfo,hlaext.pkCordHlaExt,att.attachmentId,att.fileName,att.documentType,att.documentFile "
					+ " from CBUUnitReport extemp, CdrCbu cordinfo, CordHlaExt hlaext,Attachment att,CodeList cl"
					+ " where hlaext.fkCordCdrCbuId = cordinfo.cordID and extemp.fkCordCdrCbuId = cordinfo.cordID  and extemp.pkCordExtInfo =  hlaext.fkCordExtInfo and att.attachmentId = extemp.attachmentId and cl.pkCodeId =  extemp.cordCompletedStatus and upper(cl.subType)='PENDING'";
		return velosHelper.getObjectList(appendQuery);

	}

}
