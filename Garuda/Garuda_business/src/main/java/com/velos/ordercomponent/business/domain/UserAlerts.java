package com.velos.ordercomponent.business.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CB_USER_ALERTS")
@SequenceGenerator(sequenceName = "SEQ_CB_USER_ALERTS", name = "SEQ_CB_USER_ALERTS", allocationSize = 1)
public class UserAlerts extends Auditable {
	private Long pkUserAlerts;
	private Long fkUserId;
	private Long fkCodelstId;
	private Boolean deletedFlag;
	public Boolean emailFlag;
	public Boolean notifyFlag;
	public Boolean promptFlag;
	public Boolean ctlabFlag;
	public Boolean ctshipFlag;
	public Boolean heFlag;
	public Boolean orFlag;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CB_USER_ALERTS")
	@Column(name = "PK_ALERT")
	public Long getPkUserAlerts() {
		return pkUserAlerts;
	}

	public void setPkUserAlerts(Long pkUserAlerts) {
		this.pkUserAlerts = pkUserAlerts;
	}

	@Column(name = "FK_USER")
	public Long getFkUserId() {
		return fkUserId;
	}

	public void setFkUserId(Long fkUserId) {
		this.fkUserId = fkUserId;
	}

	@Column(name = "FK_CODELST_ALERT")
	public Long getFkCodelstId() {
		return fkCodelstId;
	}

	public void setFkCodelstId(Long fkCodelstId) {
		this.fkCodelstId = fkCodelstId;
	}

	@Column(name = "deletedflag")
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}

	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	@Column(name="EMAIL_FLAG")
	public Boolean getEmailFlag() {
		return emailFlag;
	}

	public void setEmailFlag(Boolean emailFlag) {
		this.emailFlag = emailFlag;
	}

	@Column(name="NOTIFY_FLAG")
	public Boolean getNotifyFlag() {
		return notifyFlag;
	}

	public void setNotifyFlag(Boolean notifyFlag) {
		this.notifyFlag = notifyFlag;
	}

	@Column(name="PROMPT_FLAG")
	public Boolean getPromptFlag() {
		return promptFlag;
	}

	public void setPromptFlag(Boolean promptFlag) {
		this.promptFlag = promptFlag;
	}

	@Column(name="CT_LAB_FLAG")
	public Boolean getCtlabFlag() {
		return ctlabFlag;
	}

	public void setCtlabFlag(Boolean ctlabFlag) {
		this.ctlabFlag = ctlabFlag;
	}

	@Column(name="CT_SHIP_FLAG")
	public Boolean getCtshipFlag() {
		return ctshipFlag;
	}

	public void setCtshipFlag(Boolean ctshipFlag) {
		this.ctshipFlag = ctshipFlag;
	}

	@Column(name="HE_FLAG")
	public Boolean getHeFlag() {
		return heFlag;
	}

	public void setHeFlag(Boolean heFlag) {
		this.heFlag = heFlag;
	}

	@Column(name="OR_FLAG")
	public Boolean getOrFlag() {
		return orFlag;
	}

	public void setOrFlag(Boolean orFlag) {
		this.orFlag = orFlag;
	}
	
	
	
}