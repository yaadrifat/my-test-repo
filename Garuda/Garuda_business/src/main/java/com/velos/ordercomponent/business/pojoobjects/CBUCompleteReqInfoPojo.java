package com.velos.ordercomponent.business.pojoobjects;

public class CBUCompleteReqInfoPojo extends AuditablePojo{
	private Long pkCbuCompleteReqInfo;
	private Long fkCordId;
	//private Long fkOrderId;
	private Long cbuInfoFlag;
	private Long cbuProcessInfoFlag;
	private Long labSummaryFlag;
	private Long mrqFlag;
	private Long fmhqFlag;
	private Long idmFlag;
	private Long idsFlag;
	private Long licensureFlag;
	private Long eligibleFlag;
	private Long completeReqinfoFlag;
	private Boolean deletedFlag;

	
	public Long getPkCbuCompleteReqInfo() {
		return pkCbuCompleteReqInfo;
	}
	public void setPkCbuCompleteReqInfo(Long pkCbuCompleteReqInfo) {
		this.pkCbuCompleteReqInfo = pkCbuCompleteReqInfo;
	}
	public Long getFkCordId() {
		return fkCordId;
	}
	public void setFkCordId(Long fkCordId) {
		this.fkCordId = fkCordId;
	}
	public Long getCbuInfoFlag() {
		return cbuInfoFlag;
	}
	public void setCbuInfoFlag(Long cbuInfoFlag) {
		this.cbuInfoFlag = cbuInfoFlag;
	}
	public Long getCbuProcessInfoFlag() {
		return cbuProcessInfoFlag;
	}
	public void setCbuProcessInfoFlag(Long cbuProcessInfoFlag) {
		this.cbuProcessInfoFlag = cbuProcessInfoFlag;
	}
	public Long getLabSummaryFlag() {
		return labSummaryFlag;
	}
	public void setLabSummaryFlag(Long labSummaryFlag) {
		this.labSummaryFlag = labSummaryFlag;
	}
	public Long getMrqFlag() {
		return mrqFlag;
	}
	public void setMrqFlag(Long mrqFlag) {
		this.mrqFlag = mrqFlag;
	}
	public Long getFmhqFlag() {
		return fmhqFlag;
	}
	public void setFmhqFlag(Long fmhqFlag) {
		this.fmhqFlag = fmhqFlag;
	}
	public Long getIdmFlag() {
		return idmFlag;
	}
	public void setIdmFlag(Long idmFlag) {
		this.idmFlag = idmFlag;
	}
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public Long getCompleteReqinfoFlag() {
		return completeReqinfoFlag;
	}
	public void setCompleteReqinfoFlag(Long completeReqinfoFlag) {
		this.completeReqinfoFlag = completeReqinfoFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	public Long getIdsFlag() {
		return idsFlag;
	}
	public void setIdsFlag(Long idsFlag) {
		this.idsFlag = idsFlag;
	}
	public Long getLicensureFlag() {
		return licensureFlag;
	}
	public void setLicensureFlag(Long licensureFlag) {
		this.licensureFlag = licensureFlag;
	}
	public Long getEligibleFlag() {
		return eligibleFlag;
	}
	public void setEligibleFlag(Long eligibleFlag) {
		this.eligibleFlag = eligibleFlag;
	}
	
	
}
