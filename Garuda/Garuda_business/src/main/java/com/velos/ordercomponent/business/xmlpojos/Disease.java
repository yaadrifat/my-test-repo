package com.velos.ordercomponent.business.xmlpojos;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class Disease {
	
	private String Code;
	private String OtherDescription;
	private String OtherCancerLeukDescription;
	
	@XmlElement(name="Code")
	public String getCode() {
		return Code;
	}
	public void setCode(String code) {
		Code = code;
	}
	@XmlElement(name="OtherDescription")
	public String getOtherDescription() {
		return OtherDescription;
	}
	public void setOtherDescription(String otherDescription) {
		OtherDescription = otherDescription;
	}
	@XmlElement(name="OtherCancerLeukDescription")
	public String getOtherCancerLeukDescription() {
		return OtherCancerLeukDescription;
	}
	public void setOtherCancerLeukDescription(String otherCancerLeukDescription) {
		OtherCancerLeukDescription = otherCancerLeukDescription;
	}
	

}
