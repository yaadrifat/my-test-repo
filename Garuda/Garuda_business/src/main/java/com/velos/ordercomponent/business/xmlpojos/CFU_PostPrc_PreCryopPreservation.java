package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;

public class CFU_PostPrc_PreCryopPreservation {

	private String cfu_post_prcsng_test;
	private String cfu_post_prcsng_oth_describe;
	private String cfu_post_prcsng_cnt;
	
	@XmlElement(name="cfu_post_prcsng_test")
	public String getCfu_post_prcsng_test() {
		return cfu_post_prcsng_test;
	}
	public void setCfu_post_prcsng_test(String cfuPostPrcsngTest) {
		cfu_post_prcsng_test = cfuPostPrcsngTest;
	}
	
	@XmlElement(name="cfu_post_prcsng_oth_describe")
	public String getCfu_post_prcsng_oth_describe() {
		return cfu_post_prcsng_oth_describe;
	}
	public void setCfu_post_prcsng_oth_describe(String cfuPostPrcsngOthDescribe) {
		cfu_post_prcsng_oth_describe = cfuPostPrcsngOthDescribe;
	}
	
	@XmlElement(name="cfu_post_prcsng_cnt")
	public String getCfu_post_prcsng_cnt() {
		return cfu_post_prcsng_cnt;
	}
	public void setCfu_post_prcsng_cnt(String cfuPostPrcsngCnt) {
		cfu_post_prcsng_cnt = cfuPostPrcsngCnt;
	}
	
}
