package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CB_CORD_MINIMUM_CRITERIA")
@SequenceGenerator(sequenceName="SEQ_CB_CORD_MINIMUM_CRITERIA",name="SEQ_CB_CORD_MINIMUM_CRITERIA",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class CBUMinimumCriteria extends Auditable {
	
	private Long pkcordminimumcriteria;
	private Long fkCordId;
	private Long fkOrderId;
	private Long fkRecipient;
	//private Boolean bacterialtestingflag;
	//private Boolean fungaltestingflag;
	//private Boolean hemoglobinopthyflag;
	//private Boolean idmsflag;
	//private Boolean postprocessdose; 
	//private Boolean postprocessviab;
	//private Boolean hlaconfirm;
	//private Boolean antigensconfirm;
	private Long declarationresult;
	private Boolean saveinprogress;
	private Boolean conformedminimumcriteria;
	private Boolean deletedflag;
	private Long antihivcmp;
	private Long hivpnatcmp;
	private Long hbsagcmp;
	private Long antihcvcmp;
	private Long hlacnfm;
	private Long hlacnfmchild;
	private Long ctcompleted;
	private Long ctcompletedchild;
	private Long antigenmatch;
	private Long antigenmatchchild;
	//private Boolean unitrecpflag;
	private Long unitrecp;
	private Long unitrecpchild;
	private Long cbuplanned;
	private Long cbuplannedchild;
	private Long patienttyp;
	private Long patienttypchild;
	private Long viabchild;
	private Long postprocesschild;
	//private Boolean patTyping;
	//private Boolean priortoprepstart;
	//private Boolean ctcompetedFlag;
	//private CdrCbu cdrCbu;
	private Long secondReviewBy;
	private Date secondReviewOn;
	private Boolean reviewConformed;
	private Long ReviewBy;
	private Date ReviewOn;
	private Long viabTestTiming;
	private Long viabPerFlag;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_CORD_MINIMUM_CRITERIA")
	@Column(name="PK_CORD_MINIMUM_CRITERIA")
	public Long getPkcordminimumcriteria() {
		return pkcordminimumcriteria;
	}
	public void setPkcordminimumcriteria(Long pkcordminimumcriteria) {
		this.pkcordminimumcriteria = pkcordminimumcriteria;
	}
	@Column(name="FK_CORD_ID")
	public Long getFkCordId() {
		return fkCordId;
	}
	public void setFkCordId(Long fkCordId) {
		this.fkCordId = fkCordId;
	}
	@Column(name="FK_ORDER_ID")
	public Long getFkOrderId() {
		return fkOrderId;
	}
	public void setFkOrderId(Long fkOrderId) {
		this.fkOrderId = fkOrderId;
	}
	@Column(name="FK_RECIPIENT")
	public Long getFkRecipient() {
		return fkRecipient;
	}
	public void setFkRecipient(Long fkRecipient) {
		this.fkRecipient = fkRecipient;
	}
	/*@Column(name="BACTERIAL_TESTING_FLAG")
	public Boolean getBacterialtestingflag() {
		return bacterialtestingflag;
	}
	public void setBacterialtestingflag(Boolean bacterialtestingflag) {
		this.bacterialtestingflag = bacterialtestingflag;
	}
	@Column(name="FUNGAL_TESTING_FLAG")
	public Boolean getFungaltestingflag() {
		return fungaltestingflag;
	}
	public void setFungaltestingflag(Boolean fungaltestingflag) {
		this.fungaltestingflag = fungaltestingflag;
	}
	@Column(name="HEMOGLOBINOPTHY_FLAG")
	public Boolean getHemoglobinopthyflag() {
		return hemoglobinopthyflag;
	}
	public void setHemoglobinopthyflag(Boolean hemoglobinopthyflag) {
		this.hemoglobinopthyflag = hemoglobinopthyflag;
	}
	@Column(name="IDMS_FLAG")
	public Boolean getIdmsflag() {
		return idmsflag;
	}
	public void setIdmsflag(Boolean idmsflag) {
		this.idmsflag = idmsflag;
	}
	@Column(name="POST_PROCESS_DOSE")
	public Boolean getPostprocessdose() {
		return postprocessdose;
	}
	public void setPostprocessdose(Boolean postprocessdose) {
		this.postprocessdose = postprocessdose;
	}
	@Column(name="POST_PROCESS_VIAB")
	public Boolean getPostprocessviab() {
		return postprocessviab;
	}
	public void setPostprocessviab(Boolean postprocessviab) {
		this.postprocessviab = postprocessviab;
	}
	@Column(name="HLA_CONFIRM")
	public Boolean getHlaconfirm() {
		return hlaconfirm;
	}
	public void setHlaconfirm(Boolean hlaconfirm) {
		this.hlaconfirm = hlaconfirm;
	}
	@Column(name="ANTIGENS_CONFIRM")
	public Boolean getAntigensconfirm() {
		return antigensconfirm;
	}
	public void setAntigensconfirm(Boolean antigensconfirm) {
		this.antigensconfirm = antigensconfirm;
	}*/
	@Column(name="DECLARATION_RESULT")
	public Long getDeclarationresult() {
		return declarationresult;
	}
	public void setDeclarationresult(Long declarationresult) {
		this.declarationresult = declarationresult;
	}
	@Column(name="SAVEINPROGESS")
	public Boolean getSaveinprogress() {
		return saveinprogress;
	}
	public void setSaveinprogress(Boolean saveinprogress) {
		this.saveinprogress = saveinprogress;
	}
	
	@Column(name="CONFORMED_MINIMUM_CRITIRIA")
	public Boolean getConformedminimumcriteria() {
		return conformedminimumcriteria;
	}
	public void setConformedminimumcriteria(Boolean conformedminimumcriteria) {
		this.conformedminimumcriteria = conformedminimumcriteria;
	}
	
	@Column(name="DELETEDFLAG")
	public Boolean getDeletedflag() {
		return deletedflag;
	}
	public void setDeletedflag(Boolean deletedflag) {
		this.deletedflag = deletedflag;
	}
	@Column(name="ANTIHIV_II_HIVP24_HIVNAT_COMP")
	public Long getAntihivcmp() {
		return antihivcmp;
	}
	public void setAntihivcmp(Long antihivcmp) {
		this.antihivcmp = antihivcmp;
	}
	@Column(name="HIVPNAT_COMP")
	public Long getHivpnatcmp() {
		return hivpnatcmp;
	}
	public void setHivpnatcmp(Long hivpnatcmp) {
		this.hivpnatcmp = hivpnatcmp;
	}
	@Column(name="HBSAG_COMP")
	public Long getHbsagcmp() {
		return hbsagcmp;
	}
	public void setHbsagcmp(Long hbsagcmp) {
		this.hbsagcmp = hbsagcmp;
	}
	@Column(name="ANTIHCV_COMP")
	public Long getAntihcvcmp() {
		return antihcvcmp;
	}
	public void setAntihcvcmp(Long antihcvcmp) {
		this.antihcvcmp = antihcvcmp;
	}
	@Column(name="HLA_CNFM")
	public Long getHlacnfm() {
		return hlacnfm;
	}
	public void setHlacnfm(Long hlacnfm) {
		this.hlacnfm = hlacnfm;
	}
	@Column(name="HLACHILD")
	public Long getHlacnfmchild() {
		return hlacnfmchild;
	}
	public void setHlacnfmchild(Long hlacnfmchild) {
		this.hlacnfmchild = hlacnfmchild;
	}
	@Column(name="CTCOMPLETED")
	public Long getCtcompleted() {
		return ctcompleted;
	}
	public void setCtcompleted(Long ctcompleted) {
		this.ctcompleted = ctcompleted;
	}
	@Column(name="CTCOMPLETED_CHILD")
	public Long getCtcompletedchild() {
		return ctcompletedchild;
	}
	public void setCtcompletedchild(Long ctcompletedchild) {
		this.ctcompletedchild = ctcompletedchild;
	}
	@Column(name="ANTIGEN_MATCH")
	public Long getAntigenmatch() {
		return antigenmatch;
	}
	public void setAntigenmatch(Long antigenmatch) {
		this.antigenmatch = antigenmatch;
	}
	@Column(name="ANTIGENCHILD")
	public Long getAntigenmatchchild() {
		return antigenmatchchild;
	}
	public void setAntigenmatchchild(Long antigenmatchchild) {
		this.antigenmatchchild = antigenmatchchild;
	}
	@Column(name="UNITRECIPIENT")
	public Long getUnitrecp() {
		return unitrecp;
	}
	public void setUnitrecp(Long unitrecp) {
		this.unitrecp = unitrecp;
	}
	@Column(name="UNITRECIPIENT_CHILD")
	public Long getUnitrecpchild() {
		return unitrecpchild;
	}
	public void setUnitrecpchild(Long unitrecpchild) {
		this.unitrecpchild = unitrecpchild;
	}
	@Column(name="CBUPLANNED")
	public Long getCbuplanned() {
		return cbuplanned;
	}
	public void setCbuplanned(Long cbuplanned) {
		this.cbuplanned = cbuplanned;
	}
	@Column(name="CBUPLANNED_CHILD")
	public Long getCbuplannedchild() {
		return cbuplannedchild;
	}
	public void setCbuplannedchild(Long cbuplannedchild) {
		this.cbuplannedchild = cbuplannedchild;
	}
	@Column(name="PATIENTTYPING")
	public Long getPatienttyp() {
		return patienttyp;
	}
	public void setPatienttyp(Long patienttyp) {
		this.patienttyp = patienttyp;
	}
	@Column(name="PATIENTTYPING_CHILD")
	public Long getPatienttypchild() {
		return patienttypchild;
	}
	public void setPatienttypchild(Long patienttypchild) {
		this.patienttypchild = patienttypchild;
	}
	@Column(name="POSTPROCESS_CHILD")
	public Long getPostprocesschild() {
		return postprocesschild;
	}
	public void setPostprocesschild(Long postprocesschild) {
		this.postprocesschild = postprocesschild;
	}
	@Column(name="VIABILITY_CHILD")
	public Long getViabchild() {
		return viabchild;
	}
	public void setViabchild(Long viabchild) {
		this.viabchild = viabchild;
	}/*
	@Column(name="PAT_TYPING")
	public Boolean getPatTyping() {
		return patTyping;
	}
	public void setPatTyping(Boolean patTyping) {
		this.patTyping = patTyping;
	}
	@Column(name="PRIOR_TO_PREP_START")
	public Boolean getPriortoprepstart() {
		return priortoprepstart;
	}
	public void setPriortoprepstart(Boolean priortoprepstart) {
		this.priortoprepstart = priortoprepstart;
	}
	@Column(name="UNIT_RECP_FLAG")
	public Boolean getUnitrecpflag() {
		return unitrecpflag;
	}
	public void setUnitrecpflag(Boolean unitrecpflag) {
		this.unitrecpflag = unitrecpflag;
	}
	@Column(name="CT_COMPLETED_FLAG")
	public Boolean getCtcompetedFlag() {
		return ctcompetedFlag;
	}
	public void setCtcompetedFlag(Boolean ctcompetedFlag) {
		this.ctcompetedFlag = ctcompetedFlag;
	}
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="FK_CORD_ID",nullable=false,insertable=false,updatable=false)
	@NotFound(action=NotFoundAction.IGNORE)	
	public CdrCbu getCdrCbu() {
		return cdrCbu;
	}
	public void setCdrCbu(CdrCbu cdrCbu) {
		this.cdrCbu = cdrCbu;
	}*/
	@Column(name="SECOND_REVIEW_ON")
	public Date getSecondReviewOn() {
		return secondReviewOn;
	}
	public void setSecondReviewOn(Date secondReviewOn) {
		this.secondReviewOn = secondReviewOn;
	}
	@Column(name="SECOND_REVIEW_BY")
	public Long getSecondReviewBy() {
		return secondReviewBy;
	}
	public void setSecondReviewBy(Long secondReviewBy) {
		this.secondReviewBy = secondReviewBy;
	}
	@Column(name="SECOND_REVIEW_CONFIRMED")
	public Boolean getReviewConformed() {
		return reviewConformed;
	}
	public void setReviewConformed(Boolean reviewConformed) {
		this.reviewConformed = reviewConformed;
	}
	@Column(name="REVIEWED_BY")
	public Long getReviewBy() {
		return ReviewBy;
	}
	public void setReviewBy(Long reviewBy) {
		ReviewBy = reviewBy;
	}
	@Column(name="REVIEWED_ON")
	public Date getReviewOn() {
		return ReviewOn;
	}
	public void setReviewOn(Date reviewOn) {
		ReviewOn = reviewOn;
	}
	@Column(name="VIAB_TEST_TIMING")
	public Long getViabTestTiming() {
		return viabTestTiming;
	}
	public void setViabTestTiming(Long viabTestTiming) {
		this.viabTestTiming = viabTestTiming;
	}
	@Column(name="VIAB_PERCENTAGE_FLAG")
	public Long getViabPerFlag() {
		return viabPerFlag;
	}
	public void setViabPerFlag(Long viabPerFlag) {
		this.viabPerFlag = viabPerFlag;
	}
}
