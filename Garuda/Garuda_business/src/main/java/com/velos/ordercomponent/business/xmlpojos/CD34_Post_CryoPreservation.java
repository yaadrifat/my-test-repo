package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.datatype.XMLGregorianCalendar;

public class CD34_Post_CryoPreservation {

	private String CD34_test;
	private String CD34_cnt;
	private XMLGregorianCalendar CD34_test_date;
	private String CD34_test_reason;
	
	@XmlElement(name="CD34_test")	
	public String getCD34_test() {
		return CD34_test;
	}
	public void setCD34_test(String cD34Test) {
		CD34_test = cD34Test;
	}
	
	@XmlElement(name="CD34_cnt")	
	public String getCD34_cnt() {
		return CD34_cnt;
	}
	public void setCD34_cnt(String cD34Cnt) {
		CD34_cnt = cD34Cnt;
	}
	
	@XmlElement(name="CD34_test_date")
	public XMLGregorianCalendar getCD34_test_date() {
		return CD34_test_date;
	}
	public void setCD34_test_date(XMLGregorianCalendar cD34TestDate) {
		CD34_test_date = cD34TestDate;
	}
	
	@XmlElement(name="CD34_test_reason")
	public String getCD34_test_reason() {
		return CD34_test_reason;
	}
	public void setCD34_test_reason(String cD34TestReason) {
		CD34_test_reason = cD34TestReason;
	}
	
	
}
