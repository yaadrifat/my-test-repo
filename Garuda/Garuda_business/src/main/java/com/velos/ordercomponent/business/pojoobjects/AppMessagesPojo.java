/**
 * 
 */
package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

/**
 * @author jamarendra
 *
 */
public class AppMessagesPojo extends AuditablePojo{

	private Long pkAppMsg;
	private String msgDesc;
	private Long msgStatus;
	private Long msgDispAt;
	private Long fkSite;
	private Long fkGrps;
	private Long msgDispOrder;
	private Date msgStartDt;
	private String msgStartDtStr;
	private Date msgEndDt;
	private String msgEndDtStr;
	private String deletedFlag;
	
	public Long getPkAppMsg() {
		return pkAppMsg;
	}
	public void setPkAppMsg(Long pkAppMsg) {
		this.pkAppMsg = pkAppMsg;
	}
	public String getMsgDesc() {
		return msgDesc;
	}
	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}
	public Long getMsgStatus() {
		return msgStatus;
	}
	public void setMsgStatus(Long msgStatus) {
		this.msgStatus = msgStatus;
	}
	public Long getMsgDispAt() {
		return msgDispAt;
	}
	public void setMsgDispAt(Long msgDispAt) {
		this.msgDispAt = msgDispAt;
	}
	public Long getFkSite() {
		return fkSite;
	}
	public void setFkSite(Long fkSite) {
		this.fkSite = fkSite;
	}
	public Long getFkGrps() {
		return fkGrps;
	}
	public void setFkGrps(Long fkGrps) {
		this.fkGrps = fkGrps;
	}
	public Long getMsgDispOrder() {
		return msgDispOrder;
	}
	public void setMsgDispOrder(Long msgDispOrder) {
		this.msgDispOrder = msgDispOrder;
	}
	public Date getMsgStartDt() {
		return msgStartDt;
	}
	public void setMsgStartDt(Date msgStartDt) {
		this.msgStartDt = msgStartDt;
	}
	public Date getMsgEndDt() {
		return msgEndDt;
	}
	public void setMsgEndDt(Date msgEndDt) {
		this.msgEndDt = msgEndDt;
	}
	public String getMsgStartDtStr() {
		return msgStartDtStr;
	}
	public void setMsgStartDtStr(String msgStartDtStr) {
		this.msgStartDtStr = msgStartDtStr;
	}
	public String getMsgEndDtStr() {
		return msgEndDtStr;
	}
	public void setMsgEndDtStr(String msgEndDtStr) {
		this.msgEndDtStr = msgEndDtStr;
	}
	public String getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(String deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	
	
}
