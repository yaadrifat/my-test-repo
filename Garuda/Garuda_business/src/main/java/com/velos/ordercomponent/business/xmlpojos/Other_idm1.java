package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


public class Other_idm1 {
	private String other_idm_1_react_ind;
	private String other_idm_1_react_test;
	private XMLGregorianCalendar other_idm_1_react_date;
	@XmlElement(name="other_idm_1_react_ind")	
	public String getOther_idm_1_react_ind() {
		return other_idm_1_react_ind;
	}
	public void setOther_idm_1_react_ind(String otherIdm_1ReactInd) {
		other_idm_1_react_ind = otherIdm_1ReactInd;
	}
	@XmlElement(name="other_idm_1_react_test")	
	public String getOther_idm_1_react_test() {
		return other_idm_1_react_test;
	}
	public void setOther_idm_1_react_test(String otherIdm_1ReactTest) {
		other_idm_1_react_test = otherIdm_1ReactTest;
	}
	@XmlElement(name="other_idm_1_react_date")
	@XmlJavaTypeAdapter(type=XMLGregorianCalendar.class,value=XMLGeorgarianCalanderAdapter.class)
	public XMLGregorianCalendar getOther_idm_1_react_date() {
		return other_idm_1_react_date;
	}
	public void setOther_idm_1_react_date(XMLGregorianCalendar otherIdm_1ReactDate) {
		other_idm_1_react_date = otherIdm_1ReactDate;
	}

	
}

