package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

public class FormVersionPojo extends AuditablePojo{
	private Long pkformversion;		
	private Long fkform;			
	private Long entityId;			
	private Date dynaformDate;		
	private Long entityType;	
	private Long formseq;
	private String dynaFormDateStr;
	private String void_flag;
	private Date void_date;
	
	public String getVoid_flag() {
		return void_flag;
	}
	public void setVoid_flag(String void_flag) {
		this.void_flag = void_flag;
	}
	public Date getVoid_date() {
		return void_date;
	}
	public void setVoid_date(Date void_date) {
		this.void_date = void_date;
	}
	public String getDynaFormDateStr() {
		return dynaFormDateStr;
	}
	public void setDynaFormDateStr(String dynaFormDateStr) {
		this.dynaFormDateStr = dynaFormDateStr;
	}
	public Long getPkformversion() {
		return pkformversion;
	}
	public void setPkformversion(Long pkformversion) {
		this.pkformversion = pkformversion;
	}
	public Long getFkform() {
		return fkform;
	}
	public void setFkform(Long fkform) {
		this.fkform = fkform;
	}
	public Long getEntityId() {
		return entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	public Date getDynaformDate() {
		return dynaformDate;
	}
	public void setDynaformDate(Date dynaformDate) {
		this.dynaformDate = dynaformDate;
	}
	public Long getEntityType() {
		return entityType;
	}
	public void setEntityType(Long entityType) {
		this.entityType = entityType;
	}
	public Long getFormseq() {
		return formseq;
	}
	public void setFormseq(Long formseq) {
		this.formseq = formseq;
	}

}
