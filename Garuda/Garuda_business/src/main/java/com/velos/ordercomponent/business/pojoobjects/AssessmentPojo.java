package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

/**
 * @author jamarendra
 *
 */
public class AssessmentPojo extends AuditablePojo{
	private Long pkAssessment;
	private Long entityId;
	private Long entityType;
	private Long subEntityId;
	private Long subEntityType;
	private String assessmentRemarks;
	private Boolean flagforLater;
	private Long assessmentResp;
	private Boolean visibleTCFlag;
	private Long assessmentReason;
	private String reasonRemarks;
	private Date availableDate;
	private Boolean sendReviewFlag;
	private Boolean consultFlag;
	private String assessmentReviewed;
	private String columnReference;
	private String deletedFlag;
	private Long fkTestId;
	private Long fkSpecimenId;
	private Long assessmentpostedby;
	private Date assessmentpostedon;
	private Long assessmentconsultby;
	private Date assessmentconsulton;
	private String assessmentFlagComment;
	
	public String getAssessmentFlagComment() {
		return assessmentFlagComment;
	}
	public void setAssessmentFlagComment(String assessmentFlagComment) {
		this.assessmentFlagComment = assessmentFlagComment;
	}
	public Boolean getFlagforLater() {
		return flagforLater;
	}
	public void setFlagforLater(Boolean flagforLater) {
		this.flagforLater = flagforLater;
	}
	public Boolean getVisibleTCFlag() {
		return visibleTCFlag;
	}
	public void setVisibleTCFlag(Boolean visibleTCFlag) {
		this.visibleTCFlag = visibleTCFlag;
	}
	public Boolean getSendReviewFlag() {
		return sendReviewFlag;
	}
	public void setSendReviewFlag(Boolean sendReviewFlag) {
		this.sendReviewFlag = sendReviewFlag;
	}
	public Boolean getConsultFlag() {
		return consultFlag;
	}
	public void setConsultFlag(Boolean consultFlag) {
		this.consultFlag = consultFlag;
	}
	public Long getFkSpecimenId() {
		return fkSpecimenId;
	}
	public void setFkSpecimenId(Long fkSpecimenId) {
		this.fkSpecimenId = fkSpecimenId;
	}
	public Long getFkTestId() {
		return fkTestId;
	}
	public void setFkTestId(Long fkTestId) {
		this.fkTestId = fkTestId;
	}
	public Long getPkAssessment() {
		return pkAssessment;
	}
	public void setPkAssessment(Long pkAssessment) {
		this.pkAssessment = pkAssessment;
	}
	public Long getEntityId() {
		return entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	public Long getEntityType() {
		return entityType;
	}
	public void setEntityType(Long entityType) {
		this.entityType = entityType;
	}
	public Long getSubEntityId() {
		return subEntityId;
	}
	public void setSubEntityId(Long subEntityId) {
		this.subEntityId = subEntityId;
	}
	public Long getSubEntityType() {
		return subEntityType;
	}
	public void setSubEntityType(Long subEntityType) {
		this.subEntityType = subEntityType;
	}
	public String getAssessmentRemarks() {
		return assessmentRemarks;
	}
	public void setAssessmentRemarks(String assessmentRemarks) {
		this.assessmentRemarks = assessmentRemarks;
	}
	
	public Long getAssessmentResp() {
		return assessmentResp;
	}
	public void setAssessmentResp(Long assessmentResp) {
		this.assessmentResp = assessmentResp;
	}
	
	public Long getAssessmentReason() {
		return assessmentReason;
	}
	public void setAssessmentReason(Long assessmentReason) {
		this.assessmentReason = assessmentReason;
	}
	public String getReasonRemarks() {
		return reasonRemarks;
	}
	public void setReasonRemarks(String reasonRemarks) {
		this.reasonRemarks = reasonRemarks;
	}
	public Date getAvailableDate() {
		return availableDate;
	}
	public void setAvailableDate(Date availableDate) {
		this.availableDate = availableDate;
	}

	public String getAssessmentReviewed() {
		return assessmentReviewed;
	}
	public void setAssessmentReviewed(String assessmentReviewed) {
		this.assessmentReviewed = assessmentReviewed;
	}	
	public String getColumnReference() {
		return columnReference;
	}
	public void setColumnReference(String columnReference) {
		this.columnReference = columnReference;
	}
	public String getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(String deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	public Long getAssessmentpostedby() {
		return assessmentpostedby;
	}
	public void setAssessmentpostedby(Long assessmentpostedby) {
		this.assessmentpostedby = assessmentpostedby;
	}
	public Date getAssessmentpostedon() {
		return assessmentpostedon;
	}
	public void setAssessmentpostedon(Date assessmentpostedon) {
		this.assessmentpostedon = assessmentpostedon;
	}
	public Long getAssessmentconsultby() {
		return assessmentconsultby;
	}
	public void setAssessmentconsultby(Long assessmentconsultby) {
		this.assessmentconsultby = assessmentconsultby;
	}
	public Date getAssessmentconsulton() {
		return assessmentconsulton;
	}
	public void setAssessmentconsulton(Date assessmentconsulton) {
		this.assessmentconsulton = assessmentconsulton;
	}
}