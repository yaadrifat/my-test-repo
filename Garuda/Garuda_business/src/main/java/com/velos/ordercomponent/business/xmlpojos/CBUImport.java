package com.velos.ordercomponent.business.xmlpojos;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="CBUImport")
public class CBUImport {
   
	private List<Bank> Bank;

	public CBUImport(){}

	public List<Bank> getBanks() {
		return Bank;
	}

	public void setBanks(List<Bank> banks) {
		this.Bank = banks;
	}
	
	
	
	
}
