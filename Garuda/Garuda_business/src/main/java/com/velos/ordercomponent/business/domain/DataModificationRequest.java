package com.velos.ordercomponent.business.domain;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CB_DATA_MODI_REQUEST")
@SequenceGenerator(sequenceName="SEQ_CB_DATA_MODI_REQUEST",name="SEQ_CB_DATA_MODI_REQUEST",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class DataModificationRequest {
	
	private Long pkdatarequest;
	private Long requesttype;
	private String title;
	private Date requestDuedate;
	private String dataModiDescription;
	private String requestReason;
	private String affectedFields;
	private String cbbProcessChange;
	private Long requestStatus;
	private Long creater;
	private Date createdon;
	private Long cbbcode;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_DATA_MODI_REQUEST")
	@Column(name="PK_DATA_MODI_REQUEST")
	public Long getPkdatarequest() {
		return pkdatarequest;
	}
	public void setPkdatarequest(Long pkdatarequest) {
		this.pkdatarequest = pkdatarequest;
	}
	
	@Column(name="REQUEST_TITLE")
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	@Column(name="REQUEST_TYPE")
	public Long getRequesttype() {
		return requesttype;
	}
	public void setRequesttype(Long requesttype) {
		this.requesttype = requesttype;
	}
	
	@Column(name="REQUEST_DUE_DATE")
	public Date getRequestDuedate() {
		return requestDuedate;
	}
	public void setRequestDuedate(Date requestDuedate) {
		this.requestDuedate = requestDuedate;
	}
	
	@Column(name="REQUEST_DESC")
	public String getDataModiDescription() {
		return dataModiDescription;
	}
	public void setDataModiDescription(String dataModiDescription) {
		this.dataModiDescription = dataModiDescription;
	}
	
	@Column(name="REQUEST_REASON")
	public String getRequestReason() {
		return requestReason;
	}
	public void setRequestReason(String requestReason) {
		this.requestReason = requestReason;
	}
	
	@Column(name="AFFECTED_FIELDS")
	public String getAffectedFields() {
		return affectedFields;
	}
	public void setAffectedFields(String affectedFields) {
		this.affectedFields = affectedFields;
	}
	
	@Column(name="CBB_PROCESS_CHANGE")
	public String getCbbProcessChange() {
		return cbbProcessChange;
	}
	public void setCbbProcessChange(String cbbProcessChange) {
		this.cbbProcessChange = cbbProcessChange;
	}
	
	@Column(name="REQUEST_APPROVAL_STATUS")
	public Long getRequestStatus() {
		return requestStatus;
	}
	public void setRequestStatus(Long requestStatus) {
		this.requestStatus = requestStatus;
	}
	
	@Column(name="CREATOR")
	public Long getCreater() {
		return creater;
	}
	public void setCreater(Long creater) {
		this.creater = creater;
	}
	
	
	@Column(name="FK_SITEID")
	public Long getCbbcode() {
		return cbbcode;
	}
	public void setCbbcode(Long cbbcode) {
		this.cbbcode = cbbcode;
	}
	
	@Column(name="CREATED_ON")
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	


}
