package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

public class CordHlaExtPojo extends AuditablePojo {
	private Long pkCordHlaExt;
	private Long fkCordCdrCbuId;
	private String cordHlaTwice;
	private String cordIndHlaSamples;
	private String cordHlaSecondLab;
	private String cordHlaContiSeg;
	private String cordHlaContiSegBefRel;
	private String cordHlaIndBefRel;
	private Long cordHlaContiSegNum;
	private Long fkCordExtInfo;
	
	
	
	public Long getFkCordExtInfo() {
		return fkCordExtInfo;
	}
	public void setFkCordExtInfo(Long fkCordExtInfo) {
		this.fkCordExtInfo = fkCordExtInfo;
	}
	public Long getPkCordHlaExt() {
		return pkCordHlaExt;
	}
	public void setPkCordHlaExt(Long pkCordHlaExt) {
		this.pkCordHlaExt = pkCordHlaExt;
	}
	public Long getFkCordCdrCbuId() {
		return fkCordCdrCbuId;
	}
	public void setFkCordCdrCbuId(Long fkCordCdrCbuId) {
		this.fkCordCdrCbuId = fkCordCdrCbuId;
	}
	public String getCordHlaTwice() {
		return cordHlaTwice;
	}
	public void setCordHlaTwice(String cordHlaTwice) {
		this.cordHlaTwice = cordHlaTwice;
	}
	public String getCordIndHlaSamples() {
		return cordIndHlaSamples;
	}
	public void setCordIndHlaSamples(String cordIndHlaSamples) {
		this.cordIndHlaSamples = cordIndHlaSamples;
	}
	public String getCordHlaSecondLab() {
		return cordHlaSecondLab;
	}
	public void setCordHlaSecondLab(String cordHlaSecondLab) {
		this.cordHlaSecondLab = cordHlaSecondLab;
	}
	public String getCordHlaContiSeg() {
		return cordHlaContiSeg;
	}
	public void setCordHlaContiSeg(String cordHlaContiSeg) {
		this.cordHlaContiSeg = cordHlaContiSeg;
	}
	public String getCordHlaContiSegBefRel() {
		return cordHlaContiSegBefRel;
	}
	public void setCordHlaContiSegBefRel(String cordHlaContiSegBefRel) {
		this.cordHlaContiSegBefRel = cordHlaContiSegBefRel;
	}
	public String getCordHlaIndBefRel() {
		return cordHlaIndBefRel;
	}
	public void setCordHlaIndBefRel(String cordHlaIndBefRel) {
		this.cordHlaIndBefRel = cordHlaIndBefRel;
	}
	public Long getCordHlaContiSegNum() {
		return cordHlaContiSegNum;
	}
	public void setCordHlaContiSegNum(Long cordHlaContiSegNum) {
		this.cordHlaContiSegNum = cordHlaContiSegNum;
	}
	
	private Boolean deletedFlag;
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
}
