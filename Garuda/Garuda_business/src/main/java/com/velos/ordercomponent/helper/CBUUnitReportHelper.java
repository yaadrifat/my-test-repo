package com.velos.ordercomponent.helper;



import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.classic.Session;
import org.hibernate.SQLQuery;

import com.velos.ordercomponent.business.domain.CBUUnitReport;
import com.velos.ordercomponent.business.domain.CBUUnitReportTemp;
import com.velos.ordercomponent.business.domain.CordHlaExt;
import com.velos.ordercomponent.business.domain.CordHlaExtTemp;
import com.velos.ordercomponent.business.pojoobjects.CBUUnitReportPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUUnitReportTempPojo;
import com.velos.ordercomponent.business.pojoobjects.CordHlaExtPojo;
import com.velos.ordercomponent.business.pojoobjects.CordHlaExtTempPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.util.HibernateUtil;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.Utilities;
import com.velos.ordercomponent.business.util.VelosUtil;

public class CBUUnitReportHelper {
	
	public static final Log log=LogFactory.getLog(CBUUnitReportHelper.class);
	
	public static CBUUnitReportTempPojo saveCBUUnitTempReport(CBUUnitReportTempPojo cbuUnitReportTempPojo){
		//log.debug("velos Unit Report Helper saveUnitReport called........");
		
		CBUUnitReportTemp cbuUnitReportTemp = new CBUUnitReportTemp();
		//log.debug("after creating cbuUnitReportTemp obj"+cbuUnitReportTemp);
		
		//log.debug("getCordViabPostTestDate :: "+cbuUnitReportTempPojo.getCordViabPostTestDate()+"--------------");
		try {
		if(cbuUnitReportTempPojo.getCordViabPostTestDate() != null && !cbuUnitReportTempPojo.getCordViabPostTestDate().equals("")) {
			cbuUnitReportTempPojo.setCordViabPostTestDate(new Date(Utilities.getFormattedDate(new Date(cbuUnitReportTempPojo.getStrcordViabPostTestDate()))));
			}
		if(cbuUnitReportTempPojo.getCordCfuPostTestDate() != null && !cbuUnitReportTempPojo.getCordCfuPostTestDate().equals("")) {
			cbuUnitReportTempPojo.setCordCfuPostTestDate(new Date(Utilities.getFormattedDate(new Date(cbuUnitReportTempPojo.getStrcordCfuPostTestDate()))));
		}
		if(cbuUnitReportTempPojo.getCordCbuPostTestDate() != null && !cbuUnitReportTempPojo.getCordCbuPostTestDate().equals("")) {
			cbuUnitReportTempPojo.setCordCbuPostTestDate(new Date(Utilities.getFormattedDate(new Date(cbuUnitReportTempPojo.getStrcordCbuPostTestDate()))));
			}
		
		}catch (Exception e) {
					// TODO Auto-generated catch block
					log.error(e.getMessage());
					e.printStackTrace();
				}
		
		cbuUnitReportTemp = (CBUUnitReportTemp)ObjectTransfer.transferObjects(cbuUnitReportTempPojo, cbuUnitReportTemp);
		Object object = new VelosUtil().setAuditableInfo(cbuUnitReportTemp);
		cbuUnitReportTemp = addUnitReportTemp((CBUUnitReportTemp)object);
		//log.debug("cbuUnitReportTemp getPkCordExtInfoTemp() :: "+cbuUnitReportTemp.getPkCordExtInfoTemp());
		cbuUnitReportTempPojo.setPkCordExtInfoTemp(cbuUnitReportTemp.getPkCordExtInfoTemp());
		
		return cbuUnitReportTempPojo;
	}
	
	public static String getDescriptionUploadInfo(Long attachmentID){
		return getDescriptionUploadInfo(attachmentID,null);
	}
	
	public static String getDescriptionUploadInfo(Long attachmentID , Session session){
		boolean sessionFlag = false;
		String description =null;
		try{
			 // check session 
			if(session== null){
				// create new session
			  session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			//log.debug("in heloper class before query");
			description=(String)session.createSQLQuery("SELECT DESCRIPTION FROM CB_UPLOAD_INFO where FK_ATTACHMENTID="+attachmentID ).list().get(0);
			//attachmentID=attid.longValue();
			//log.debug("attachment id is********"+attachmentId);
//			if(sessionFlag){
//				// close session, if session created in this method
//				session.getTransaction().commit();
//			}
			}catch(Exception e){
				log.error(e.getMessage());
				e.printStackTrace();
			}finally{
				if(session.isOpen()){
					session.close();
				}
			}
		return description;
	}
	public static Date getCompltionDateUploadInfo(Long attachmentID){
		return getCompltionDateUploadInfo(attachmentID,null);
	}
	
	public static Date getCompltionDateUploadInfo(Long attachmentID , Session session){
		boolean sessionFlag = false;
		Date compltionDate =null;
		try{
			 // check session 
			if(session== null){
				// create new session
			  session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			//System.out.println("in heloper class before query");
			compltionDate=(Date)session.createSQLQuery("SELECT COMPLETION_DATE FROM CB_UPLOAD_INFO where FK_ATTACHMENTID="+attachmentID ).list().get(0);
			//System.out.println("compltionDate"+compltionDate);
			//attachmentID=attid.longValue();
			//System.out.println("attachment id is********"+attachmentId);
//			if(sessionFlag){
//				// close session, if session created in this method
//				session.getTransaction().commit();
//			}
			}catch(Exception e){
				log.error("Exception in CBUUnitReportHelper : getDescription() :: " + e);
				e.printStackTrace();
			}finally{
				if(session.isOpen()){
					session.close();
				}
			}
		return compltionDate;
	}
	public static CBUUnitReportTemp addUnitReportTemp(CBUUnitReportTemp cbuUnitReportTemp) {
		//log.debug(" helper without session");
		return addUnitReportTemp(cbuUnitReportTemp,null);
	}
	
	public static CBUUnitReportTemp addUnitReportTemp(CBUUnitReportTemp cbuUnitReportTemp,Session session) {
		//log.debug(" helper "+cbuUnitReport.getCordCbuPostQues1());
		//log.debug(" helper "+cbuUnitReport.getCordCbuPostQues2());
		boolean sessionFlag = false;
		try{
		// check session 
			//log.debug("session " + session);
		if(session== null){
			// create new session
		  session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}
	//	//log.debug("Helper : " + cbuUnitReportTemp.getPkCordExtInfo());
		if (cbuUnitReportTemp.getPkCordExtInfoTemp() != null
				&& cbuUnitReportTemp.getPkCordExtInfoTemp() > 0) {
		session.update(cbuUnitReportTemp);
		}
		else
			session.save(cbuUnitReportTemp);
		
		if (sessionFlag) {
			session.getTransaction().commit();
		}
		} catch (Exception e) {
			//log.debug(" Exception in CBU Unit Report " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			//log.debug(" session in finally " + session + " /" +sessionFlag );
			if (session.isOpen()) {
				session.close();
			}
		}
		return cbuUnitReportTemp;
	}
	
	public static CBUUnitReportPojo saveCBUUnitReport(CBUUnitReportPojo cbuUnitReportPojo){
		//log.debug("velos Unit Report Helper saveUnitReport called........");
		
		CBUUnitReport cbuUnitReport = new CBUUnitReport();
		
		//log.debug("getCordViabPostTestDate :: "+cbuUnitReportPojo.getStrcordViabPostTestDate()+"--------------");
		try {
		if(cbuUnitReportPojo.getStrcordViabPostTestDate() != null && !cbuUnitReportPojo.getStrcordViabPostTestDate().equals("")) {
					cbuUnitReportPojo.setCordViabPostTestDate(new Date(Utilities.getFormattedDate(new Date(cbuUnitReportPojo.getStrcordViabPostTestDate()))));
			}
		if(cbuUnitReportPojo.getStrcordCfuPostTestDate() != null && !cbuUnitReportPojo.getStrcordCfuPostTestDate().equals("")) {
			cbuUnitReportPojo.setCordCfuPostTestDate(new Date(Utilities.getFormattedDate(new Date(cbuUnitReportPojo.getStrcordCfuPostTestDate()))));
		}
		if(cbuUnitReportPojo.getStrcordCbuPostTestDate() != null && !cbuUnitReportPojo.getStrcordCbuPostTestDate().equals("")) {
			cbuUnitReportPojo.setCordCbuPostTestDate(new Date(Utilities.getFormattedDate(new Date(cbuUnitReportPojo.getStrcordCbuPostTestDate()))));
			}
		
		}catch (Exception e) {
					// TODO Auto-generated catch block
			log.error(e.getMessage());
					e.printStackTrace();
				}
				
	
		cbuUnitReport = (CBUUnitReport)ObjectTransfer.transferObjects(cbuUnitReportPojo, cbuUnitReport);
		Object object = new VelosUtil().setAuditableInfo(cbuUnitReport);
		addUnitReport((CBUUnitReport)object);
		return cbuUnitReportPojo;
	}
	
	// without session
	public static CBUUnitReport addUnitReport(CBUUnitReport cbuUnitReport) {
		//log.debug(" helper without session");
		return addUnitReport(cbuUnitReport,null);
	}
	
	// with session
	public static CBUUnitReport addUnitReport(CBUUnitReport cbuUnitReport,Session session) {
		//log.debug(" helper "+cbuUnitReport.getCordCbuPostQues1());
		//log.debug(" helper "+cbuUnitReport.getCordCbuPostQues2());
		boolean sessionFlag = false;
		try{
		// check session 
			//log.debug("session " + session);
		if(session== null){
			// create new session
		  session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}
		//log.debug("Helper : " + cbuUnitReport.getPkCordExtInfo());
		if (cbuUnitReport.getPkCordExtInfo() != null
				&& cbuUnitReport.getPkCordExtInfo() > 0) {
		session.merge(cbuUnitReport);
		}
		else{
		session.save(cbuUnitReport);
		}
		if (sessionFlag) {
			session.getTransaction().commit();
		}
		} catch (Exception e) {
			//log.debug(" Exception in CBU Unit Report " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			//log.debug(" session in finally " + session + " /" +sessionFlag );
			if (session.isOpen()) {
				session.close();
			}
		}
		return cbuUnitReport;
	}

	
	
	public static CordHlaExtTempPojo saveHlaTempInfo(CordHlaExtTempPojo cordHlaExtTempPojo){
		//log.debug("velos HLA Helper saveUnitReport called........");
		
		CordHlaExtTemp cordHlaExtTemp = new CordHlaExtTemp();
		
		
		cordHlaExtTemp = (CordHlaExtTemp)ObjectTransfer.transferObjects(cordHlaExtTempPojo, cordHlaExtTemp);
		
		
		
		Object object = new VelosUtil().setAuditableInfo(cordHlaExtTemp);
		
		
		addHlaTempInfo((CordHlaExtTemp)object);
		
		return cordHlaExtTempPojo;
	}
	
	public static CordHlaExtTemp addHlaTempInfo(CordHlaExtTemp cordHlaExtTemp) {
				
		
		return addHlaTempInfo(cordHlaExtTemp,null);
	}
	
	public static CordHlaExtTemp addHlaTempInfo(CordHlaExtTemp cordHlaExtTemp,Session session) {
		
		boolean sessionFlag = false;
		try{
		// check session 
		if(session== null){
			// create new session
		  session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}
		if (cordHlaExtTemp.getPkCordHlaExtTemp() != null
				&& cordHlaExtTemp.getPkCordHlaExtTemp() > 0) {
		session.update(cordHlaExtTemp);
		}
		else
			session.save(cordHlaExtTemp);
		
		if (sessionFlag) {
			session.getTransaction().commit();
		}
		} catch (Exception e) {
			//log.debug(" Exception in CBU Unit Report " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return cordHlaExtTemp;
	}
	public static CordHlaExtPojo saveHlaInfo(CordHlaExtPojo cordHlaExtPojo){
		//log.debug("velos HLA Helper saveUnitReport called........");
		
		CordHlaExt cordHlaExt = new CordHlaExt();
		//log.debug("cordHlaExtPojo.getCordHlaTwice()--"+cordHlaExtPojo.getCordHlaTwice());
		//log.debug("cordHlaExtPojo.getCordIndHlaSamples()--"+cordHlaExtPojo.getCordIndHlaSamples());
		
		cordHlaExt = (CordHlaExt)ObjectTransfer.transferObjects(cordHlaExtPojo, cordHlaExt);
		Object object = new VelosUtil().setAuditableInfo(cordHlaExt);
		addHlaInfo((CordHlaExt)object);
		return cordHlaExtPojo;
	}
	
	// without session
	public static CordHlaExt addHlaInfo(CordHlaExt cordHlaExt) {
		
		return addHlaInfo(cordHlaExt,null);
	}
	
	// with session
	public static CordHlaExt addHlaInfo(CordHlaExt cordHlaExt,Session session) {
		//log.debug("cordHlaExt--"+cordHlaExt);
		//log.debug(" helper "+cordHlaExt.getCordHlaTwice());
		//log.debug(" helper "+cordHlaExt.getCordIndHlaSamples());
		boolean sessionFlag = false;
		try{
		// check session 
		if(session== null){
			// create new session
		  session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}
		
		session.save(cordHlaExt);
		if (sessionFlag) {
			session.getTransaction().commit();
		}
		} catch (Exception e) {
			//log.debug(" Exception in CBU Unit Report " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return cordHlaExt;
	}

	public static Object getCbuInfo(String objectName,String id,String fieldName) throws Exception {
		return getCbuInfo(objectName,id,fieldName,null);
		
	}
	
	
	
	public static Object getCbuInfo(String objectName,String id,String fieldName,Session session) throws Exception{
		boolean sessionFlag = false;
		//log.debug("CBUUnitReportHelper.getCbuInfo() start");		
		Object resultObject = null;	
		
		
		try {
			// check session 
			if(session== null){
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			
			
			resultObject = velosHelper.getObject(objectName,id,fieldName, session);		
			
		}catch(Exception e) {
			log.error(e.getMessage());
			//log.debug("Exception in CBUUnitReportHelper.getCbuInfo() :: "+e);
			e.printStackTrace();
			
		}finally{
			if(session.isOpen()){
				//log.debug("before session closed");
				session.close();
			}
		}
		//log.debug("CBUHelper.getEditVolunteer() end");
		return resultObject;
	}	
	
	public static String getcordinfopk() throws Exception {
		
		String criteria = "";
		criteria = "from CBUUnitReport order by pkCordExtInfo desc";
		
		return criteria;
	}	
	
	public static List<CBUUnitReport> getCbuUnitReportLst(Long cordId, Long attachmentId){
		return getCbuUnitReportLst(cordId, attachmentId, null);
	}
	
	public static List<CBUUnitReport> getCbuUnitReportLst(Long cordId, Long attachmentId
			, Session session) {
		List<CBUUnitReport> returnList = null;
       	boolean sessionFlag = false;
    	try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "From CBUUnitReport where fkCordCdrCbuId="+cordId+" and attachmentId="+attachmentId;
			returnList = (List<CBUUnitReport>) session.createQuery(sql).list();
			
			
    	} catch (Exception e) {
			//log.debug(" Exception in getCbuInfoById " + e);
    		log.error(e.getMessage());
			e.printStackTrace();
		} finally {						
			if(session.isOpen()){
				 session.close();		
			}
		}
		return returnList;
	}
	
	public static List getRevokeUnitReportLst(String cordID,PaginateSearch paginateSearch,int i, String condition, String orderBy){
		return getRevokeUnitReportLst(cordID,paginateSearch,i,null,condition, orderBy);
	}
	
	public static List getRevokeUnitReportLst(String cordID, PaginateSearch paginateSearch,int i,Session session, String condition, String orderBy){
		List<Object> returnList = new ArrayList<Object>();
    	boolean sessionFlag = false;
    	try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			//String sql = "select a.lastModifiedOn,a.lastModifiedBy,cu.fk_CategoryId,cu.fk_SubCategoryId From Attachment a,CBUploadInfo cu where a.garudaEntityId="+cordID+" and a.deletedFlag=1"
			//+" and cu.fk_AttachmentId=a.attachmentId ";
			String sql ="select * from (select f_codelst_desc(cu.fk_category) category, f_codelst_desc(cu.fk_subcategory) subcategory, " +
						" (select usr_logname from er_user where pk_user = arm.user_id) user_id,  to_char(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp, " + 
						" decode(arm.action,'I','Add','U','Revoke','') action, (select dcms_file_attachment_id from er_attachments where pk_attachment = arm.module_id) doclink ,arm.module_id, arm.timestamp "+
						" from audit_row_module arm, cb_upload_info cu where arm.module_id in (select pk_attachment from er_attachments where entity_id = "+cordID+") and arm.table_name = 'ER_ATTACHMENTS' " +
						" and arm.module_id = cu.fk_attachmentid order by 8 desc) ";
			
			if (i==1)
				sql = sql + condition+ orderBy;
			if (i==0)
				sql = " select count(*) from ("+sql+")" + condition;
				
			
			//returnList = (List) session.createQuery(sql).list();
			SQLQuery query =  session.createSQLQuery(sql);
			if(paginateSearch!=null && i==1){
				query.setFirstResult(paginateSearch.getiPageNo());
				query.setMaxResults(paginateSearch.getiShowRows());
			}
			returnList = (List<Object>)query.list();	
			
			
    	} catch (Exception e) {
			//log.debug(" Exception in getRevokeUnitReportLst " + e);
    		log.error(e.getMessage());
			e.printStackTrace();
		} finally {						
			if(session.isOpen()){
				 session.close();		
			}
		}
		return returnList;
	}
	
	public static List getUploadedDocumentLst(String cordID){
		return getUploadedDocumentLst(cordID,null);
	}
	
	public static List getUploadedDocumentLst(String cordID,Session session){
		List returnList = null;
    	boolean sessionFlag = false;
    	try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "select a.createdOn,a.createdBy,a.dcmsAttachmentId,cu.description,cu.fk_CategoryId,cu.fk_SubCategoryId,a.attachmentId,cu.completionDate,cu.reportDate,cu.processDate,cu.testDate,cu.receivedDate From Attachment a,CBUploadInfo cu where a.garudaEntityId="+cordID+" and a.deletedFlag is null"
			+" and cu.fk_AttachmentId=a.attachmentId order by a.createdOn DESC";
			returnList = (List) session.createQuery(sql).list();

    	} catch (Exception e) {
			//log.debug(" Exception in getRevokeUnitReportLst " + e);
    		log.error(e.getMessage());
			e.printStackTrace();
		} finally {						
			if(session.isOpen()){
				 session.close();		
			}
		}
		return returnList;
	}
	
}
