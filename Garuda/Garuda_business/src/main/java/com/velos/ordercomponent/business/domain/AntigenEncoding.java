package com.velos.ordercomponent.business.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
@Entity
@Table(name="CB_ANTIGEN_ENCOD")
@SequenceGenerator(name="SEQ_CB_ANTIGEN_ENCOD",sequenceName="SEQ_CB_ANTIGEN_ENCOD",allocationSize=1)
public class AntigenEncoding extends Auditable {

	private Long pkAntigenEncoding;
	private Long fkAntigen;
	private Integer version;
	private String searchFormat;
	private String genomicFormat;
	private Boolean validInd;
	private Boolean activeInd;	
	private String lastUpdateSource;
	private String createdBySource;		
	
	@Id
	@Column(name="PK_ANTIGEN_ENCOD")
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_ANTIGEN_ENCOD")
	public Long getPkAntigenEncoding() {
		return pkAntigenEncoding;
	}
	public void setPkAntigenEncoding(Long pkAntigenEncoding) {
		this.pkAntigenEncoding = pkAntigenEncoding;
	}
	
	@Column(name="FK_ANTIGEN")
	public Long getFkAntigen() {
		return fkAntigen;
	}
	public void setFkAntigen(Long fkAntigen) {
		this.fkAntigen = fkAntigen;
	}
	@Column(name="VERSION")
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
	@Column(name="SEARCH_FORMAT")
	public String getSearchFormat() {
		return searchFormat;
	}
	public void setSearchFormat(String searchFormat) {
		this.searchFormat = searchFormat;
	}
	@Column(name="GENOMIC_FORMAT")
	public String getGenomicFormat() {
		return genomicFormat;
	}
	public void setGenomicFormat(String genomicFormat) {
		this.genomicFormat = genomicFormat;
	}
	@Column(name="VALID_IND")
	public Boolean getValidInd() {
		return validInd;
	}
	public void setValidInd(Boolean validInd) {
		this.validInd = validInd;
	}
	@Column(name="ACTIVE_IND")
	public Boolean getActiveInd() {
		return activeInd;
	}
	public void setActiveInd(Boolean activeInd) {
		this.activeInd = activeInd;
	}
	
	@Column(name="LAST_UPDATED_SOURCE")
	public String getLastUpdateSource() {
		return lastUpdateSource;
	}
	public void setLastUpdateSource(String lastUpdateSource) {
		this.lastUpdateSource = lastUpdateSource;
	}
	@Column(name="CREATED_BY_SOURCE")
	public String getCreatedBySource() {
		return createdBySource;
	}
	public void setCreatedBySource(String createdBySource) {
		this.createdBySource = createdBySource;
	}	
	
	
}
