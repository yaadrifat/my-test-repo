package com.velos.ordercomponent.business.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author Mohiuddin Ali Ahmed
 *
 */
@Entity
@Table(name="CB_ADDITIONAL_IDS")
@SequenceGenerator(sequenceName="SEQ_CB_ADDITIONAL_IDS",name="SEQ_CB_ADDITIONAL_IDS",allocationSize=1)
public class AdditionalIds extends Auditable {
  
	private Long pkAdditionalIds;
	private String additionalId;
	private String additionalIdDesc;
	private Long entityType;
	private Long entityId;
	private Long additionalIdType;
	private Boolean deletedFlag;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_ADDITIONAL_IDS")
	@Column(name="PK_CB_ADDITIONAL_IDS")
	public Long getPkAdditionalIds() {
		return pkAdditionalIds;
	}
	public void setPkAdditionalIds(Long pkAdditionalIds) {
		this.pkAdditionalIds = pkAdditionalIds;
	}
	
	
	@Column(name="CB_ADDITIONAL_ID")
	public String getAdditionalId() {
		return additionalId;
	}
	public void setAdditionalId(String additionalId) {
		this.additionalId = additionalId;
	}
	@Column(name="CB_ADDITIONAL_ID_DESC")
	public String getAdditionalIdDesc() {
		return additionalIdDesc;
	}
	public void setAdditionalIdDesc(String additionalIdDesc) {
		this.additionalIdDesc = additionalIdDesc;
	}
	@Column(name="ENTITY_TYPE")
	public Long getEntityType() {
		return entityType;
	}
	public void setEntityType(Long entityType) {
		this.entityType = entityType;
	}
	@Column(name="ENTITY_ID")
	public Long getEntityId() {
		return entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	
	@Column(name="FK_CB_ADDITIONAL_ID_TYPE")
	public Long getAdditionalIdType() {
		return additionalIdType;
	}
	public void setAdditionalIdType(Long additionalIdType) {
		this.additionalIdType = additionalIdType;
	}	
	
	@Column(name="deletedflag")
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
}
