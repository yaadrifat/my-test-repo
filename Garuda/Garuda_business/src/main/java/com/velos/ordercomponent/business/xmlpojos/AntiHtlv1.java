package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


public class AntiHtlv1 {

	private String anti_htlv1_react_ind;
	private XMLGregorianCalendar anti_htlv1_date;
	private String cms_cert_lab_ind;
	private String fda_lic_mfg_inst_ind;
	@XmlElement(name="anti_htlv1_react_ind")	
	public String getAnti_htlv1_react_ind() {
		return anti_htlv1_react_ind;
	}
	public void setAnti_htlv1_react_ind(String antiHtlv1ReactInd) {
		anti_htlv1_react_ind = antiHtlv1ReactInd;
	}
	@XmlElement(name="anti_htlv1_date")	
	@XmlJavaTypeAdapter(type=XMLGregorianCalendar.class,value=XMLGeorgarianCalanderAdapter.class)
	public XMLGregorianCalendar getAnti_htlv1_date() {
		return anti_htlv1_date;
	}
	public void setAnti_htlv1_date(XMLGregorianCalendar antiHtlv1Date) {
		anti_htlv1_date = antiHtlv1Date;
	}
	@XmlElement(name="cms_cert_lab_ind")	
	public String getCms_cert_lab_ind() {
		return cms_cert_lab_ind;
	}
	public void setCms_cert_lab_ind(String cmsCertLabInd) {
		cms_cert_lab_ind = cmsCertLabInd;
	}
	@XmlElement(name="fda_lic_mfg_inst_ind")	
	public String getFda_lic_mfg_inst_ind() {
		return fda_lic_mfg_inst_ind;
	}
	public void setFda_lic_mfg_inst_ind(String fdaLicMfgInstInd) {
		fda_lic_mfg_inst_ind = fdaLicMfgInstInd;
	}
	
	
}
