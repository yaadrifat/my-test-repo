package com.velos.ordercomponent.business.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author Mohiuddin Ali Ahmed
 *
 */
@Entity
@Table(name="UI_WIDGET_CONTAINER")
@SequenceGenerator(sequenceName="seq_UI_widget_CONTAINER",name="seq_UI_widget_CONTAINER",allocationSize=1)
public class Container {

	private Long containerId;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="seq_UI_widget_CONTAINER")
	@Column(name="PK_CONTAINER")
	public Long getContainerId() {
		return containerId;
	}

	public void setContainerId(Long containerId) {
		this.containerId = containerId;
	}
	
	private Long pageId;
    private String containerName;

    @Column(name="FK_PAGE_ID")
	public Long getPageId() {
		return pageId;
	}
	public void setPageId(Long pageId) {
		this.pageId = pageId;
	}

	@Column(name="CONTAINER_NAME")
	public String getContainerName() {
		return containerName;
	}
	public void setContainerName(String containerName) {
		this.containerName = containerName;
	}
    
    
	
}
