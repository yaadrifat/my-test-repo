package com.velos.ordercomponent.business.pojoobjects;

import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.ValidationParameter;

public class EligibilityNewDeclPojo extends AuditablePojo {
	private Long pkNewEligDecl;
	private Boolean mrqNewQues1;
	private Boolean mrqNewQues1a;
	private String mrqNewQues1AddDetail;
	private Boolean mrqNewQues2;
	private String mrqNewQues2AddDetail;
	private Boolean mrqNewQues3;
	private String mrqNewQues3AddDetail;
	private Boolean phyNewFindQues4;
	private Boolean phyNewFindQues4A;
	private String phyNewFindQues4AddDetail;
	private Boolean phyNewFindQues5;
	private String phyNewFindQues5AddDetail;
	private Boolean phyNewFindQues6;
	private Boolean phyNewFindQues6a;
	private Boolean phyNewFindQues6b;
	private Boolean phyNewFindQues6c;
	private Boolean phyNewFindQues6d;
	private Boolean phyNewFindQues6e;
	private Boolean phyNewFindQues6f;
	private Boolean phyNewFindQues6g;
	private Boolean phyNewFindQues6h;
	private Boolean phyNewFindQues6i;
	private String phyNewFindQues6AddDetail;
	private Boolean idmNewQues7;
	private Boolean idmNewQues7a;
	private String idmNewQues7AddDetail;
	private Boolean idmNewQues8;
	private Boolean idmNewQues8a;
	private String idmNewQues8AddDetail;
	private Boolean idmNewQues9;
	private String idmNewQues9AddDetail;
	private Long entityId;
	private Long entityType;
	public Long getPkNewEligDecl() {
		return pkNewEligDecl;
	}
	public void setPkNewEligDecl(Long pkNewEligDecl) {
		this.pkNewEligDecl = pkNewEligDecl;
	}
	/*@CustomValidator(
			  type ="eligradiovalidator",
			  fieldName = "mrqNewQues1",				 
			  key="garuda.cbu.cordentry.question",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )})*/
	public Boolean getMrqNewQues1() {
		return mrqNewQues1;
	}
	public void setMrqNewQues1(Boolean mrqNewQues1) {
		this.mrqNewQues1 = mrqNewQues1;
	}
	@CustomValidator(
			  type ="eligradiovalidator",
			  fieldName = "mrqNewQues1a",				 
			  key="garuda.cbu.cordentry.question",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
						     @ValidationParameter( name = "dependencyField1", value = "mrqNewQues1")						    
					       }
			  )
	public Boolean getMrqNewQues1a() {
		return mrqNewQues1a;
	}
	public void setMrqNewQues1a(Boolean mrqNewQues1a) {
		this.mrqNewQues1a = mrqNewQues1a;
	}
	@CustomValidator(
			  type ="eligfieldvalidator",
			  fieldName = "mrqNewQues1AddDetail",				 
			  key="garuda.common.validation.detail",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
						     @ValidationParameter( name = "dependencyField1", value = "mrqNewQues1a")						    
					       }
			  )
	public String getMrqNewQues1AddDetail() {
		return mrqNewQues1AddDetail;
	}
	public void setMrqNewQues1AddDetail(String mrqNewQues1AddDetail) {
		this.mrqNewQues1AddDetail = mrqNewQues1AddDetail;
	}
	/*@CustomValidator(
			  type ="eligradiovalidator",
			  fieldName = "mrqNewQues2",				 
			  key="garuda.cbu.cordentry.question",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )})*/
	public Boolean getMrqNewQues2() {
		return mrqNewQues2;
	}
	public void setMrqNewQues2(Boolean mrqNewQues2) {
		this.mrqNewQues2 = mrqNewQues2;
	}
	@CustomValidator(
			  type ="eligfieldvalidator",
			  fieldName = "mrqNewQues2AddDetail",				 
			  key="garuda.common.validation.detail",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
						     @ValidationParameter( name = "dependencyField1", value = "mrqNewQues2")						    
					       }
			  )
	public String getMrqNewQues2AddDetail() {
		return mrqNewQues2AddDetail;
	}
	public void setMrqNewQues2AddDetail(String mrqNewQues2AddDetail) {
		this.mrqNewQues2AddDetail = mrqNewQues2AddDetail;
	}
	public Boolean getMrqNewQues3() {
		return mrqNewQues3;
	}
	public void setMrqNewQues3(Boolean mrqNewQues3) {
		this.mrqNewQues3 = mrqNewQues3;
	}
	@CustomValidator(
			  type ="eligfieldvalidator",
			  fieldName = "mrqNewQues3AddDetail",				 
			  key="garuda.common.validation.detail",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
						     @ValidationParameter( name = "dependencyField1", value = "mrqNewQues3")						    
					       }
			  )
	public String getMrqNewQues3AddDetail() {
		return mrqNewQues3AddDetail;
	}
	public void setMrqNewQues3AddDetail(String mrqNewQues3AddDetail) {
		this.mrqNewQues3AddDetail = mrqNewQues3AddDetail;
	}
	/*@CustomValidator(
			  type ="eligradiovalidator",
			  fieldName = "phyNewFindQues4",				 
			  key="garuda.cbu.cordentry.question",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )})*/
	public Boolean getPhyNewFindQues4() {
		return phyNewFindQues4;
	}
	public void setPhyNewFindQues4(Boolean phyNewFindQues4) {
		this.phyNewFindQues4 = phyNewFindQues4;
	}
	@CustomValidator(
			  type ="eligradiovalidator",
			  fieldName = "phyNewFindQues4A",				 
			  key="garuda.cbu.cordentry.question",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
						     @ValidationParameter( name = "dependencyField1", value = "phyNewFindQues4")						    
					       }
			  )
	public Boolean getPhyNewFindQues4A() {
		return phyNewFindQues4A;
	}
	public void setPhyNewFindQues4A(Boolean phyNewFindQues4A) {
		this.phyNewFindQues4A = phyNewFindQues4A;
	}
	@CustomValidator(
			  type ="eligfieldvalidator",
			  fieldName = "phyNewFindQues4AddDetail",				 
			  key="garuda.common.validation.detail",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
						     @ValidationParameter( name = "dependencyField1", value = "phyNewFindQues4A")						    
					       }
			  )
	public String getPhyNewFindQues4AddDetail() {
		return phyNewFindQues4AddDetail;
	}
	public void setPhyNewFindQues4AddDetail(String phyNewFindQues4AddDetail) {
		this.phyNewFindQues4AddDetail = phyNewFindQues4AddDetail;
	}
	/*@CustomValidator(
			  type ="eligradiovalidator",
			  fieldName = "phyNewFindQues5",				 
			  key="garuda.cbu.cordentry.question",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )})*/
	public Boolean getPhyNewFindQues5() {
		return phyNewFindQues5;
	}
	public void setPhyNewFindQues5(Boolean phyNewFindQues5) {
		this.phyNewFindQues5 = phyNewFindQues5;
	}
	@CustomValidator(
			  type ="eligfieldvalidator",
			  fieldName = "phyNewFindQues5AddDetail",				 
			  key="garuda.common.validation.detail",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
						     @ValidationParameter( name = "dependencyField1", value = "phyNewFindQues5")						    
					       }
			  )
	public String getPhyNewFindQues5AddDetail() {
		return phyNewFindQues5AddDetail;
	}
	public void setPhyNewFindQues5AddDetail(String phyNewFindQues5AddDetail) {
		this.phyNewFindQues5AddDetail = phyNewFindQues5AddDetail;
	}
	/*@CustomValidator(
			  type ="eligradiovalidator",
			  fieldName = "phyNewFindQues6",				 
			  key="garuda.cbu.cordentry.question",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )})*/
	public Boolean getPhyNewFindQues6() {
		return phyNewFindQues6;
	}
	public void setPhyNewFindQues6(Boolean phyNewFindQues6) {
		this.phyNewFindQues6 = phyNewFindQues6;
	}
	@CustomValidator(
			  type ="eligradiovalidator",
			  fieldName = "phyNewFindQues6a",				 
			  key="garuda.cbu.cordentry.question",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
						     @ValidationParameter( name = "dependencyField1", value = "phyNewFindQues6")						    
					       }
			  )
	public Boolean getPhyNewFindQues6a() {
		return phyNewFindQues6a;
	}
	public void setPhyNewFindQues6a(Boolean phyNewFindQues6a) {
		this.phyNewFindQues6a = phyNewFindQues6a;
	}
	@CustomValidator(
			  type ="eligradiovalidator",
			  fieldName = "phyNewFindQues6b",				 
			  key="garuda.cbu.cordentry.question",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
						     @ValidationParameter( name = "dependencyField1", value = "phyNewFindQues6")						    
					       }
			  )
	public Boolean getPhyNewFindQues6b() {
		return phyNewFindQues6b;
	}
	public void setPhyNewFindQues6b(Boolean phyNewFindQues6b) {
		this.phyNewFindQues6b = phyNewFindQues6b;
	}
	@CustomValidator(
			  type ="eligradiovalidator",
			  fieldName = "phyNewFindQues6c",				 
			  key="garuda.cbu.cordentry.question",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
						     @ValidationParameter( name = "dependencyField1", value = "phyNewFindQues6")						    
					       }
			  )
	public Boolean getPhyNewFindQues6c() {
		return phyNewFindQues6c;
	}
	public void setPhyNewFindQues6c(Boolean phyNewFindQues6c) {
		this.phyNewFindQues6c = phyNewFindQues6c;
	}
	@CustomValidator(
			  type ="eligradiovalidator",
			  fieldName = "phyNewFindQues6d",				 
			  key="garuda.cbu.cordentry.question",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
						     @ValidationParameter( name = "dependencyField1", value = "phyNewFindQues6")						    
					       }
			  )
	public Boolean getPhyNewFindQues6d() {
		return phyNewFindQues6d;
	}
	public void setPhyNewFindQues6d(Boolean phyNewFindQues6d) {
		this.phyNewFindQues6d = phyNewFindQues6d;
	}
	@CustomValidator(
			  type ="eligradiovalidator",
			  fieldName = "phyNewFindQues6e",				 
			  key="garuda.cbu.cordentry.question",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
						     @ValidationParameter( name = "dependencyField1", value = "phyNewFindQues6")						    
					       }
			  )
	public Boolean getPhyNewFindQues6e() {
		return phyNewFindQues6e;
	}
	public void setPhyNewFindQues6e(Boolean phyNewFindQues6e) {
		this.phyNewFindQues6e = phyNewFindQues6e;
	}
	@CustomValidator(
			  type ="eligradiovalidator",
			  fieldName = "phyNewFindQues6f",				 
			  key="garuda.cbu.cordentry.question",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
						     @ValidationParameter( name = "dependencyField1", value = "phyNewFindQues6")						    
					       }
			  )
	public Boolean getPhyNewFindQues6f() {
		return phyNewFindQues6f;
	}
	public void setPhyNewFindQues6f(Boolean phyNewFindQues6f) {
		this.phyNewFindQues6f = phyNewFindQues6f;
	}
	@CustomValidator(
			  type ="eligradiovalidator",
			  fieldName = "phyNewFindQues6g",				 
			  key="garuda.cbu.cordentry.question",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
						     @ValidationParameter( name = "dependencyField1", value = "phyNewFindQues6")						    
					       }
			  )
	public Boolean getPhyNewFindQues6g() {
		return phyNewFindQues6g;
	}
	public void setPhyNewFindQues6g(Boolean phyNewFindQues6g) {
		this.phyNewFindQues6g = phyNewFindQues6g;
	}
	@CustomValidator(
			  type ="eligradiovalidator",
			  fieldName = "phyNewFindQues6h",				 
			  key="garuda.cbu.cordentry.question",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
						     @ValidationParameter( name = "dependencyField1", value = "phyNewFindQues6")						    
					       }
			  )
	public Boolean getPhyNewFindQues6h() {
		return phyNewFindQues6h;
	}
	public void setPhyNewFindQues6h(Boolean phyNewFindQues6h) {
		this.phyNewFindQues6h = phyNewFindQues6h;
	}
	@CustomValidator(
			  type ="eligradiovalidator",
			  fieldName = "phyNewFindQues6i",				 
			  key="garuda.cbu.cordentry.question",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
						     @ValidationParameter( name = "dependencyField1", value = "phyNewFindQues6")						    
					       }
			  )
	public Boolean getPhyNewFindQues6i() {
		return phyNewFindQues6i;
	}
	public void setPhyNewFindQues6i(Boolean phyNewFindQues6i) {
		this.phyNewFindQues6i = phyNewFindQues6i;
	}
	@CustomValidator(
			  type ="eligfieldvalidator",
			  fieldName = "phyNewFindQues6AddDetail",				 
			  key="garuda.common.validation.detail",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
						     @ValidationParameter( name = "dependencyField1", value = "phyNewFindQues6")						    
					       }
			  )
	public String getPhyNewFindQues6AddDetail() {
		return phyNewFindQues6AddDetail;
	}
	public void setPhyNewFindQues6AddDetail(String phyNewFindQues6AddDetail) {
		this.phyNewFindQues6AddDetail = phyNewFindQues6AddDetail;
	}
	/*@CustomValidator(
			  type ="eligradiovalidator",
			  fieldName = "idmNewQues7",				 
			  key="garuda.cbu.cordentry.question",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )						  						    
					       }
			  )*/
	public Boolean getIdmNewQues7() {
		return idmNewQues7;
	}
	public void setIdmNewQues7(Boolean idmNewQues7) {
		this.idmNewQues7 = idmNewQues7;
	}
	@CustomValidator(
			  type ="eligradiovalidator",
			  fieldName = "idmNewQues7a",				 
			  key="garuda.cbu.cordentry.question",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
						     @ValidationParameter( name = "dependencyField1", value = "idmNewQues7")						    
					       }
			  )
	public Boolean getIdmNewQues7a() {
		return idmNewQues7a;
	}
	public void setIdmNewQues7a(Boolean idmNewQues7a) {
		this.idmNewQues7a = idmNewQues7a;
	}
	@CustomValidator(
			  type ="eligfieldvalidator",
			  fieldName = "idmNewQues7AddDetail",				 
			  key="garuda.common.validation.detail",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
						     @ValidationParameter( name = "dependencyField1", value = "idmNewQues7a")						    
					       }
			  )
	public String getIdmNewQues7AddDetail() {
		return idmNewQues7AddDetail;
	}
	public void setIdmNewQues7AddDetail(String idmNewQues7AddDetail) {
		this.idmNewQues7AddDetail = idmNewQues7AddDetail;
	}
	/*@CustomValidator(
			  type ="eligradiovalidator",
			  fieldName = "idmNewQues8",				 
			  key="garuda.cbu.cordentry.question",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )						  						    
					       }
			  )*/
	public Boolean getIdmNewQues8() {
		return idmNewQues8;
	}
	public void setIdmNewQues8(Boolean idmNewQues8) {
		this.idmNewQues8 = idmNewQues8;
	}
	@CustomValidator(
			  type ="eligradiovalidator",
			  fieldName = "idmNewQues8a",				 
			  key="garuda.cbu.cordentry.question",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
						     @ValidationParameter( name = "dependencyField1", value = "idmNewQues8")						    
					       }
			  )
	public Boolean getIdmNewQues8a() {
		return idmNewQues8a;
	}
	public void setIdmNewQues8a(Boolean idmNewQues8a) {
		this.idmNewQues8a = idmNewQues8a;
	}
	@CustomValidator(
			  type ="eligfieldvalidator",
			  fieldName = "idmNewQues8AddDetail",				 
			  key="garuda.common.validation.detail",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
						     @ValidationParameter( name = "dependencyField1", value = "idmNewQues8a")						    
					       }
			  )
	public String getIdmNewQues8AddDetail() {
		return idmNewQues8AddDetail;
	}
	public void setIdmNewQues8AddDetail(String idmNewQues8AddDetail) {
		this.idmNewQues8AddDetail = idmNewQues8AddDetail;
	}
	public Boolean getIdmNewQues9() {
		return idmNewQues9;
	}
	public void setIdmNewQues9(Boolean idmNewQues9) {
		this.idmNewQues9 = idmNewQues9;
	}
	@CustomValidator(
			  type ="eligfieldvalidator",
			  fieldName = "idmNewQues9AddDetail",				 
			  key="garuda.common.validation.detail",
			  shortCircuit=false,
			  parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
						     @ValidationParameter( name = "dependencyField1", value = "idmNewQues9")						    
					       }
			  )
	public String getIdmNewQues9AddDetail() {
		return idmNewQues9AddDetail;
	}
	public void setIdmNewQues9AddDetail(String idmNewQues9AddDetail) {
		this.idmNewQues9AddDetail = idmNewQues9AddDetail;
	}
	public Long getEntityId() {
		return entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	public Long getEntityType() {
		return entityType;
	}
	public void setEntityType(Long entityType) {
		this.entityType = entityType;
	}
	
	

}
