package com.velos.ordercomponent.business.domain;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
//import javax.persistence.Id;

//import javax.persistence.Entity;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;


@javax.persistence.Entity
@javax.persistence.Table(name="CB_FINAL_REV_UPLOAD_INFO")
@javax.persistence.SequenceGenerator(sequenceName="SEQ_CB_FINAL_REV_UPLOAD_INFO",name="SEQ_CB_FINAL_REV_UPLOAD_INFO",allocationSize=1)
public class FinalRevUploadInfo extends Auditable{

	private Long pkFinalRevUploadInfo;
	private Long entityType;
	private Long entityId;
	private Long fkCbuUploadInfo;
	private Long fkFinalCbuReview;
	private Boolean deletedFlag;
	private String attachmentType;
	
	@javax.persistence.Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_FINAL_REV_UPLOAD_INFO")
	@Column(name="PK_FINAL_REV_UPLOAD_INFO")
	public Long getPkFinalRevUploadInfo() {
		return pkFinalRevUploadInfo;
	}
	public void setPkFinalRevUploadInfo(Long pkFinalRevUploadInfo) {
		this.pkFinalRevUploadInfo = pkFinalRevUploadInfo;
	}
	
	
	@Column(name="ENTITY_ID")
	public Long getEntityId() {
		return entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	
	@Column(name="ENTITY_TYPE")
	public Long getEntityType() {
		return entityType;
	}
	public void setEntityType(Long entityType) {
		this.entityType = entityType;
	}	
	
	@Column(name="FK_CB_UPLOAD_INFO")
	public Long getFkCbuUploadInfo() {
		return fkCbuUploadInfo;
	}
	public void setFkCbuUploadInfo(Long fkCbuUploadInfo) {
		this.fkCbuUploadInfo = fkCbuUploadInfo;
	}	
	
	@Column(name="FK_CB_FINAL_CBU_REVIEW")
	public Long getFkFinalCbuReview() {
		return fkFinalCbuReview;
	}
	public void setFkFinalCbuReview(Long fkFinalCbuReview) {
		this.fkFinalCbuReview = fkFinalCbuReview;
	}	
	
	@Column(name="DELETEDFLAG")
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	@Column(name="ATTACHMENT_TYPE")
	public String getAttachmentType() {
		return attachmentType;
	}
	public void setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
	}
	
	
}
