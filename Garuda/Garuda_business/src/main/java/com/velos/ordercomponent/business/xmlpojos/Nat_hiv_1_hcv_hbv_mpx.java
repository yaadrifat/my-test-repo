package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


public class Nat_hiv_1_hcv_hbv_mpx {
    private String nat_hiv_1_hcv_hbv_mpx_ind;
    private XMLGregorianCalendar nat_hiv_1_hcv_hbv_mpx_date;
    private String cms_cert_lab_ind;
    private String fda_lic_mfg_inst_ind;
    
    @XmlElement(name="nat_hiv_1_hcv_hbv_mpx_ind")		
	public String getNat_hiv_1_hcv_hbv_mpx_ind() {
		return nat_hiv_1_hcv_hbv_mpx_ind;
	}
	public void setNat_hiv_1_hcv_hbv_mpx_ind(String natHiv_1HcvHbvMpxInd) {
		nat_hiv_1_hcv_hbv_mpx_ind = natHiv_1HcvHbvMpxInd;
	}
	@XmlElement(name="nat_hiv_1_hcv_hbv_mpx_date")	
	@XmlJavaTypeAdapter(type=XMLGregorianCalendar.class,value=XMLGeorgarianCalanderAdapter.class)
	public XMLGregorianCalendar getNat_hiv_1_hcv_hbv_mpx_date() {
		return nat_hiv_1_hcv_hbv_mpx_date;
	}
	public void setNat_hiv_1_hcv_hbv_mpx_date(XMLGregorianCalendar natHiv_1HcvHbvMpxDate) {
		nat_hiv_1_hcv_hbv_mpx_date = natHiv_1HcvHbvMpxDate;
	}
	@XmlElement(name="cms_cert_lab_ind")	
	public String getCms_cert_lab_ind() {
		return cms_cert_lab_ind;
	}
	public void setCms_cert_lab_ind(String cmsCertLabInd) {
		cms_cert_lab_ind = cmsCertLabInd;
	}
	@XmlElement(name="fda_lic_mfg_inst_ind")	
	public String getFda_lic_mfg_inst_ind() {
		return fda_lic_mfg_inst_ind;
	}
	public void setFda_lic_mfg_inst_ind(String fdaLicMfgInstInd) {
		fda_lic_mfg_inst_ind = fdaLicMfgInstInd;
	}
   
}
