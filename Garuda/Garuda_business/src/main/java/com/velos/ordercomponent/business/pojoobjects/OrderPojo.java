package com.velos.ordercomponent.business.pojoobjects;


import java.util.Date;

public class OrderPojo extends AuditablePojo {
	private Long pk_OrderId;
	private Boolean deletedFlag;
	private String ack_order;
	private Date ack_date;
	private Long ack_by;
	private Long sampleTypeAvail;
	private Long aliquotsType;
	private Date orderStatusDate;
	private Long assignedTo;
	private Long orderType;
	private Long orderReviewedBy;
	private Date orderReviewedDate;
	private Date orderAssignedDate;
	private Long taskId;
	private String taskName;
	private Long resolByCbb;
	private Long resolByTc;
	private Date resolDate;
	private String sampleAtLab;
	private String cliniInfoChecklstFlag;
	private String recentHlaTypingFlag;
	private Date orderCancellationConfirmDate;
	private Long orderCancelledBy;
	private String orderCancellationAcceptFlag;
	private Long caseManagerId;
	private String caseManagerName;
	private String TransplantCenterId;
	private String TransplantCenterName;
	private String secondaryTcId;
	private String secondaryTcName;
	private Long orderPhysian;
	private String cmMailId;
	private String cmContactNo;
	private String recCliniInfoFlag;
	private String resolAckFlag;
	private Long fkOrderHeader;
	private Long orderStatus;
	private String recentHlaEnteredFlag;
	private String additiTypingFlag;
	private String cordAvailConfirmFlag;
	private String iscordavailfornmdp;
	private Date cordUnavailConfirmDate;
	private String shipmentSchFlag;
	private String cordShipedFlag;
	private String packageSlipFlag;
	private String nmdpSampleShippedFlag;
	private String completeReqCliniInfoFlag;
	private String finalReviewFlag;
	private Long orderLastViewedBy;
	private Date orderLastViewedDate;
	private String orderViewConfirmFlag;
	private Date orderViewConfirmDate;
	private String orderPriority;
	private Date resRecDate;
	private Long fkRecentHlaId;
	private Long labCode;
	private Long switchReason;
	private String switchRsnOther;
	private Date switchDate;
	private Long switchBy;
	private Long cordAvailConformedBy;
	private Date cordAvailConformedDt;
	private Long cordUnavailReason;
	private String updateShipdetFlag;
	
	
	
	
	public String getUpdateShipdetFlag() {
		return updateShipdetFlag;
	}
	public void setUpdateShipdetFlag(String updateShipdetFlag) {
		this.updateShipdetFlag = updateShipdetFlag;
	}
	public Long getCordUnavailReason() {
		return cordUnavailReason;
	}
	public void setCordUnavailReason(Long cordUnavailReason) {
		this.cordUnavailReason = cordUnavailReason;
	}
	public Long getCordAvailConformedBy() {
		return cordAvailConformedBy;
	}
	public Date getCordAvailConformedDt() {
		return cordAvailConformedDt;
	}
	public void setCordAvailConformedBy(Long cordAvailConformedBy) {
		this.cordAvailConformedBy = cordAvailConformedBy;
	}
	public void setCordAvailConformedDt(Date cordAvailConformedDt) {
		this.cordAvailConformedDt = cordAvailConformedDt;
	}
	
	public Long getPk_OrderId() {
		return pk_OrderId;
	}
	public void setPk_OrderId(Long pk_OrderId) {
		this.pk_OrderId = pk_OrderId;
	}
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	public String getAck_order() {
		return ack_order;
	}
	public void setAck_order(String ack_order) {
		this.ack_order = ack_order;
	}
	public Date getAck_date() {
		return ack_date;
	}
	public void setAck_date(Date ack_date) {
		this.ack_date = ack_date;
	}
	public Long getAck_by() {
		return ack_by;
	}
	public void setAck_by(Long ack_by) {
		this.ack_by = ack_by;
	}
	public Long getSampleTypeAvail() {
		return sampleTypeAvail;
	}
	public void setSampleTypeAvail(Long sampleTypeAvail) {
		this.sampleTypeAvail = sampleTypeAvail;
	}
	public Long getAliquotsType() {
		return aliquotsType;
	}
	public void setAliquotsType(Long aliquotsType) {
		this.aliquotsType = aliquotsType;
	}
	public Date getOrderStatusDate() {
		return orderStatusDate;
	}
	public void setOrderStatusDate(Date orderStatusDate) {
		this.orderStatusDate = orderStatusDate;
	}
	public Long getAssignedTo() {
		return assignedTo;
	}
	public void setAssignedTo(Long assignedTo) {
		this.assignedTo = assignedTo;
	}
	public Long getOrderType() {
		return orderType;
	}
	public void setOrderType(Long orderType) {
		this.orderType = orderType;
	}
	public Long getOrderReviewedBy() {
		return orderReviewedBy;
	}
	public void setOrderReviewedBy(Long orderReviewedBy) {
		this.orderReviewedBy = orderReviewedBy;
	}
	public Date getOrderReviewedDate() {
		return orderReviewedDate;
	}
	public void setOrderReviewedDate(Date orderReviewedDate) {
		this.orderReviewedDate = orderReviewedDate;
	}
	public Date getOrderAssignedDate() {
		return orderAssignedDate;
	}
	public void setOrderAssignedDate(Date orderAssignedDate) {
		this.orderAssignedDate = orderAssignedDate;
	}
	public Long getTaskId() {
		return taskId;
	}
	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public Long getResolByCbb() {
		return resolByCbb;
	}
	public void setResolByCbb(Long resolByCbb) {
		this.resolByCbb = resolByCbb;
	}
	public Long getResolByTc() {
		return resolByTc;
	}
	public void setResolByTc(Long resolByTc) {
		this.resolByTc = resolByTc;
	}
	public Date getResolDate() {
		return resolDate;
	}
	public void setResolDate(Date resolDate) {
		this.resolDate = resolDate;
	}
	public String getSampleAtLab() {
		return sampleAtLab;
	}
	public void setSampleAtLab(String sampleAtLab) {
		this.sampleAtLab = sampleAtLab;
	}
	public String getCliniInfoChecklstFlag() {
		return cliniInfoChecklstFlag;
	}
	public void setCliniInfoChecklstFlag(String cliniInfoChecklstFlag) {
		this.cliniInfoChecklstFlag = cliniInfoChecklstFlag;
	}
	public String getRecentHlaTypingFlag() {
		return recentHlaTypingFlag;
	}
	public void setRecentHlaTypingFlag(String recentHlaTypingFlag) {
		this.recentHlaTypingFlag = recentHlaTypingFlag;
	}
	public Date getOrderCancellationConfirmDate() {
		return orderCancellationConfirmDate;
	}
	public void setOrderCancellationConfirmDate(Date orderCancellationConfirmDate) {
		this.orderCancellationConfirmDate = orderCancellationConfirmDate;
	}
	public Long getOrderCancelledBy() {
		return orderCancelledBy;
	}
	public void setOrderCancelledBy(Long orderCancelledBy) {
		this.orderCancelledBy = orderCancelledBy;
	}
	public String getOrderCancellationAcceptFlag() {
		return orderCancellationAcceptFlag;
	}
	public void setOrderCancellationAcceptFlag(String orderCancellationAcceptFlag) {
		this.orderCancellationAcceptFlag = orderCancellationAcceptFlag;
	}
	public Long getCaseManagerId() {
		return caseManagerId;
	}
	public void setCaseManagerId(Long caseManagerId) {
		this.caseManagerId = caseManagerId;
	}
	public String getCaseManagerName() {
		return caseManagerName;
	}
	public void setCaseManagerName(String caseManagerName) {
		this.caseManagerName = caseManagerName;
	}
	public String getTransplantCenterId() {
		return TransplantCenterId;
	}
	public void setTransplantCenterId(String transplantCenterId) {
		TransplantCenterId = transplantCenterId;
	}
	public String getTransplantCenterName() {
		return TransplantCenterName;
	}
	public void setTransplantCenterName(String transplantCenterName) {
		TransplantCenterName = transplantCenterName;
	}
	public String getSecondaryTcId() {
		return secondaryTcId;
	}
	public void setSecondaryTcId(String secondaryTcId) {
		this.secondaryTcId = secondaryTcId;
	}
	public String getSecondaryTcName() {
		return secondaryTcName;
	}
	public void setSecondaryTcName(String secondaryTcName) {
		this.secondaryTcName = secondaryTcName;
	}
	public Long getOrderPhysian() {
		return orderPhysian;
	}
	public void setOrderPhysian(Long orderPhysian) {
		this.orderPhysian = orderPhysian;
	}
	public String getCmMailId() {
		return cmMailId;
	}
	public void setCmMailId(String cmMailId) {
		this.cmMailId = cmMailId;
	}
	public String getCmContactNo() {
		return cmContactNo;
	}
	public void setCmContactNo(String cmContactNo) {
		this.cmContactNo = cmContactNo;
	}
	public String getRecCliniInfoFlag() {
		return recCliniInfoFlag;
	}
	public void setRecCliniInfoFlag(String recCliniInfoFlag) {
		this.recCliniInfoFlag = recCliniInfoFlag;
	}
	public String getResolAckFlag() {
		return resolAckFlag;
	}
	public void setResolAckFlag(String resolAckFlag) {
		this.resolAckFlag = resolAckFlag;
	}
	public Long getFkOrderHeader() {
		return fkOrderHeader;
	}
	public void setFkOrderHeader(Long fkOrderHeader) {
		this.fkOrderHeader = fkOrderHeader;
	}
	public Long getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(Long orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getRecentHlaEnteredFlag() {
		return recentHlaEnteredFlag;
	}
	public void setRecentHlaEnteredFlag(String recentHlaEnteredFlag) {
		this.recentHlaEnteredFlag = recentHlaEnteredFlag;
	}
	public String getAdditiTypingFlag() {
		return additiTypingFlag;
	}
	public void setAdditiTypingFlag(String additiTypingFlag) {
		this.additiTypingFlag = additiTypingFlag;
	}
	public String getCordAvailConfirmFlag() {
		return cordAvailConfirmFlag;
	}
	public void setCordAvailConfirmFlag(String cordAvailConfirmFlag) {
		this.cordAvailConfirmFlag = cordAvailConfirmFlag;
	}
	public String getIscordavailfornmdp() {
		return iscordavailfornmdp;
	}
	public void setIscordavailfornmdp(String iscordavailfornmdp) {
		this.iscordavailfornmdp = iscordavailfornmdp;
	}
	public Date getCordUnavailConfirmDate() {
		return cordUnavailConfirmDate;
	}
	public void setCordUnavailConfirmDate(Date cordUnavailConfirmDate) {
		this.cordUnavailConfirmDate = cordUnavailConfirmDate;
	}	
	
	public String getShipmentSchFlag() {
		return shipmentSchFlag;
	}
	public void setShipmentSchFlag(String shipmentSchFlag) {
		this.shipmentSchFlag = shipmentSchFlag;
	}
	public String getCordShipedFlag() {
		return cordShipedFlag;
	}
	public void setCordShipedFlag(String cordShipedFlag) {
		this.cordShipedFlag = cordShipedFlag;
	}
	
	public String getPackageSlipFlag() {
		return packageSlipFlag;
	}
	public void setPackageSlipFlag(String packageSlipFlag) {
		this.packageSlipFlag = packageSlipFlag;
	}
	public String getNmdpSampleShippedFlag() {
		return nmdpSampleShippedFlag;
	}
	public void setNmdpSampleShippedFlag(String nmdpSampleShippedFlag) {
		this.nmdpSampleShippedFlag = nmdpSampleShippedFlag;
	}
	public String getCompleteReqCliniInfoFlag() {
		return completeReqCliniInfoFlag;
	}
	public void setCompleteReqCliniInfoFlag(String completeReqCliniInfoFlag) {
		this.completeReqCliniInfoFlag = completeReqCliniInfoFlag;
	}
	public String getFinalReviewFlag() {
		return finalReviewFlag;
	}
	public void setFinalReviewFlag(String finalReviewFlag) {
		this.finalReviewFlag = finalReviewFlag;
	}
	public Long getOrderLastViewedBy() {
		return orderLastViewedBy;
	}
	public void setOrderLastViewedBy(Long orderLastViewedBy) {
		this.orderLastViewedBy = orderLastViewedBy;
	}
	public Date getOrderLastViewedDate() {
		return orderLastViewedDate;
	}
	public void setOrderLastViewedDate(Date orderLastViewedDate) {
		this.orderLastViewedDate = orderLastViewedDate;
	}
	public String getOrderViewConfirmFlag() {
		return orderViewConfirmFlag;
	}
	public void setOrderViewConfirmFlag(String orderViewConfirmFlag) {
		this.orderViewConfirmFlag = orderViewConfirmFlag;
	}
	public Date getOrderViewConfirmDate() {
		return orderViewConfirmDate;
	}
	public void setOrderViewConfirmDate(Date orderViewConfirmDate) {
		this.orderViewConfirmDate = orderViewConfirmDate;
	}
	public String getOrderPriority() {
		return orderPriority;
	}
	public void setOrderPriority(String orderPriority) {
		this.orderPriority = orderPriority;
	}
	/*public Long getOrderResolutionCode() {
		return orderResolutionCode;
	}
	public void setOrderResolutionCode(Long orderResolutionCode) {
		this.orderResolutionCode = orderResolutionCode;
	}*/
	public Date getResRecDate() {
		return resRecDate;
	}
	public void setResRecDate(Date resRecDate) {
		this.resRecDate = resRecDate;
	}
	public Long getFkRecentHlaId() {
		return fkRecentHlaId;
	}
	public void setFkRecentHlaId(Long fkRecentHlaId) {
		this.fkRecentHlaId = fkRecentHlaId;
	}
	public Long getLabCode() {
		return labCode;
	}
	public void setLabCode(Long labCode) {
		this.labCode = labCode;
	}
	public Long getSwitchReason() {
		return switchReason;
	}
	public void setSwitchReason(Long switchReason) {
		this.switchReason = switchReason;
	}
	public String getSwitchRsnOther() {
		return switchRsnOther;
	}
	public void setSwitchRsnOther(String switchRsnOther) {
		this.switchRsnOther = switchRsnOther;
	}
	public Date getSwitchDate() {
		return switchDate;
	}
	public void setSwitchDate(Date switchDate) {
		this.switchDate = switchDate;
	}
	public Long getSwitchBy() {
		return switchBy;
	}
	public void setSwitchBy(Long switchBy) {
		this.switchBy = switchBy;
	}
	
	
	
	
	
}
