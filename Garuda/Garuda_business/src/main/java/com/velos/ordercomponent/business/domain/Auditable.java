package com.velos.ordercomponent.business.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.MappedSuperclass;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
@MappedSuperclass
public class Auditable implements Serializable {

	public Date createdOn;
	public Long createdBy;
	public Long lastModifiedBy;
	public Date lastModifiedOn;
	public String ipAddress;
	
	
	@Column(updatable=false, name="created_on")
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(updatable=false,name="CREATOR")
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	@Column(insertable=false,name="last_modified_by")
	public Long getLastModifiedBy() {
		return lastModifiedBy;
	}
	public void setLastModifiedBy(Long lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	
	@Column(insertable=false,name="LAST_MODIFIED_DATE")
	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}
	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}
	
	@Column(name="IP_ADD")
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	
	
}
