package com.velos.ordercomponent.business.pojoobjects;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FormResponsesPojo extends AuditablePojo{
	
	private Long pkresponse;
	private Long fkform;
	private Long fkquestion;
	private Long entityId;
	private Long entityType;
	private Long responsecode;
	private String responsevalue;
	private String comment;
	private Long fkformresponses;
	private Long fkassessment;
	private Date dynaFormDate;
	private String dynaFormDateStr;
	private Long fkformversion;
	private String lengthValidMsg;
	private String userFriendlyName;
	private FormVersionPojo formVersionPojo;
	private List<FormResponsesPojo> formresponsepojoList = new ArrayList<FormResponsesPojo>();
	private Long natHivResp;
	private Long hbvMpxResp;
	
	public Long getNatHivResp() {
		return natHivResp;
	}
	public void setNatHivResp(Long natHivResp) {
		this.natHivResp = natHivResp;
	}
	public Long getHbvMpxResp() {
		return hbvMpxResp;
	}
	public void setHbvMpxResp(Long hbvMpxResp) {
		this.hbvMpxResp = hbvMpxResp;
	}
	public List<FormResponsesPojo> getFormresponsepojoList() {
		return formresponsepojoList;
	}
	public void setFormresponsepojoList(List<FormResponsesPojo> formresponsepojoList) {
		this.formresponsepojoList = formresponsepojoList;
	}
	public FormVersionPojo getFormVersionPojo() {
		return formVersionPojo;
	}
	public void setFormVersionPojo(FormVersionPojo formVersionPojo) {
		this.formVersionPojo = formVersionPojo;
	}
	public String getDynaFormDateStr() {
		return dynaFormDateStr;
	}
	public void setDynaFormDateStr(String dynaFormDateStr) {
		this.dynaFormDateStr = dynaFormDateStr;
	}
	public Long getPkresponse() {
		return pkresponse;
	}
	public void setPkresponse(Long pkresponse) {
		this.pkresponse = pkresponse;
	}
	public String getLengthValidMsg() {
		return lengthValidMsg;
	}
	public void setLengthValidMsg(String lengthValidMsg) {
		this.lengthValidMsg = lengthValidMsg;
	}
	public Long getFkform() {
		return fkform;
	}
	public void setFkform(Long fkform) {
		this.fkform = fkform;
	}
	public Long getFkquestion() {
		return fkquestion;
	}
	public void setFkquestion(Long fkquestion) {
		this.fkquestion = fkquestion;
	}
	public Long getEntityId() {
		return entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	public Long getEntityType() {
		return entityType;
	}
	public void setEntityType(Long entityType) {
		this.entityType = entityType;
	}
	public Long getResponsecode() {
		return responsecode;
	}
	public void setResponsecode(Long responsecode) {
		this.responsecode = responsecode;
	}
	public String getResponsevalue() {
		return responsevalue;
	}
	public void setResponsevalue(String responsevalue) {
		this.responsevalue = responsevalue;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Long getFkformresponses() {
		return fkformresponses;
	}
	public void setFkformresponses(Long fkformresponses) {
		this.fkformresponses = fkformresponses;
	}
	public Long getFkassessment() {
		return fkassessment;
	}
	public void setFkassessment(Long fkassessment) {
		this.fkassessment = fkassessment;
	}
	public Date getDynaFormDate() {
		return dynaFormDate;
	}
	public void setDynaFormDate(Date dynaFormDate) {
		this.dynaFormDate = dynaFormDate;
	}
	public Long getFkformversion() {
		return fkformversion;
	}
	public void setFkformversion(Long fkformversion) {
		this.fkformversion = fkformversion;
	}
	public String getUserFriendlyName() {
		return userFriendlyName;
	}
	public void setUserFriendlyName(String userFriendlyName) {
		this.userFriendlyName = userFriendlyName;
	}
	
}
