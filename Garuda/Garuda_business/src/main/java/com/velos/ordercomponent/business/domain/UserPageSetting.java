package com.velos.ordercomponent.business.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author Mohiuddin Ali Ahmed
 *
 */
@Entity
@Table(name="UI_USER_PAGE")
@SequenceGenerator(sequenceName="seq_UI_PAGE",name="seq_UI_PAGE",allocationSize=1)
public class UserPageSetting {

	private Long userPageSettingId;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="seq_UI_PAGE")
	@Column(name="PK_USER_PAGE")
	public Long getUserPageSettingId() {
		return userPageSettingId;
	}

	public void setUserPageSettingId(Long userPageSettingId) {
		this.userPageSettingId = userPageSettingId;
	}
	
	private Long pageId;
	private Long userId;

	@Column(name="FK_PAGE_ID")
	public Long getPageId() {
		return pageId;
	}

	public void setPageId(Long pageId) {
		this.pageId = pageId;
	}

	@Column(name="FK_USER_ID")
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	
	
}
