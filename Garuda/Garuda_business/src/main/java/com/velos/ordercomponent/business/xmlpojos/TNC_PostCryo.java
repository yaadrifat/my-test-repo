package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;

public class TNC_PostCryo {
	
	private String nucl_cell_cnt_frzn_uncorrect_Post;

	@XmlElement(name="nucl_cell_cnt_frzn_uncorrect_Post")	
	public String getNucl_cell_cnt_frzn_uncorrect_Post() {
		return nucl_cell_cnt_frzn_uncorrect_Post;
	}

	public void setNucl_cell_cnt_frzn_uncorrect_Post(
			String nuclCellCntFrznUncorrectPost) {
		nucl_cell_cnt_frzn_uncorrect_Post = nuclCellCntFrznUncorrectPost;
	}
	
	

}
