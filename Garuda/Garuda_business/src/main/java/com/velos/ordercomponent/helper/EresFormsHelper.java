package com.velos.ordercomponent.helper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.velos.eres.web.linkedForms.LinkedFormsJB;
import com.velos.ordercomponent.business.util.HibernateUtil;


public class EresFormsHelper {
	public static final Log log=LogFactory.getLog(EresFormsHelper.class);
	
	public static List getEresFormHtml(Long formId, Long cordId){
		return getEresFormsHtml(formId, cordId, null);
	}
	
	public static List getEresFormsHtml(Long formId, Long cordId, Session session){
		LinkedFormsJB link = new LinkedFormsJB();
		boolean sessionFlag = false;
		String formHtml = "";
		List list = new ArrayList();
		try{
		 // check session 
		if(session== null){
			// create new session
		  session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}

		/*String sql = "select max(pk_acctforms) from er_acctforms where fk_formlib = "
			+ formId
			+ " and fk_specimen = (select fk_specimen_id from cb_cord where pk_cord ="
			+ cordId + " ) ";*/
		
		String sql = " select max(pk_acctforms) from er_acctforms where acctforms_filldate =   (select max(acctforms_filldate) from er_acctforms where fk_formlib = "
			+ formId
			+ " and fk_specimen = (select fk_specimen_id from cb_cord where pk_cord ="
			+ cordId + " )) ";
		
		
		SQLQuery query = session.createSQLQuery(sql);

		/*List list = query.list();
		try {
			list = getPkAccoutForm(formId, cordId);
			System.out.println(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
        list = query.list();
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}
		finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return list;
	}
	
	public static void updateSpecimenFormResponse(Long specimenId,List<Long> pkAcctForm){
		updateSpecimenFormResponse(specimenId,pkAcctForm,null);
	}

    public static void updateSpecimenFormResponse(Long specimenId,List<Long> pkAcctForm,Session session){
    	boolean sessionFlag = false;
		try{
   		 // check session 
   		if(session== null){
   			// create new session
   		  session = HibernateUtil.getSessionFactory().getCurrentSession();
   			sessionFlag = true;
   			session.beginTransaction();
   		}
   		String sql = "update er_acctforms set FK_SPECIMEN="+specimenId+" where PK_ACCTFORMS in (";
   		Integer len = pkAcctForm.size();
		Integer idCount = 0;
		for(Long id : pkAcctForm){
			if(id!=null){
				sql += id ;
				idCount++;
				if(idCount==len){
					sql += ")";
					break;
				}else{
					sql += ",";
				}
			}
		}
   		SQLQuery query = session.createSQLQuery(sql);
   		query.executeUpdate();
    	}catch(Exception e){
    		log.error(e.getMessage());
			e.printStackTrace();
		}
		finally{
			if(session.isOpen()){
				session.close();
			}
		}
	}
}
