package com.velos.ordercomponent.business.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author Mohiuddin Ali Ahmed
 *
 */
@Entity
@Table(name="GARUDA_USER_PREFERENCES")
@SequenceGenerator(sequenceName="SQ_GARUDA_USER_PREFERENCES",name="SQ_GARUDA_USER_PREFERENCES",allocationSize=1)
public class UserPreferences {

	private Long userPrefId;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SQ_GARUDA_USER_PREFERENCES")
	@Column(name="PK_USER_PREF_ID")
	public Long getUserPrefId() {
		return userPrefId;
	}

	public void setUserPrefId(Long userPrefId) {
		this.userPrefId = userPrefId;
	}
	
	private Long userId;
	private Long themeId;

	@Column(name="FK_USER_ID")
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Column(name="FK_THEME_ID")
	public Long getThemeId() {
		return themeId;
	}

	public void setThemeId(Long themeId) {
		this.themeId = themeId;
	}
	
	
	
}
