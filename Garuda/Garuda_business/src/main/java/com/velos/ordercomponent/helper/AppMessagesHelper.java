package com.velos.ordercomponent.helper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;

import com.velos.ordercomponent.business.domain.AppMessages;
import com.velos.ordercomponent.business.pojoobjects.AppMessagesPojo;
import com.velos.ordercomponent.business.util.HibernateUtil;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.VelosUtil;

/**
 * @author jamarendra
 *
 */
public class AppMessagesHelper {
	public static final Log log=LogFactory.getLog(AppMessagesHelper.class);
	public static AppMessagesPojo updateAppMessages(AppMessagesPojo appMessagesPojo){
		AppMessages appMessages = new AppMessages();

		try {
			appMessages = (AppMessages) ObjectTransfer
					.transferObjects(appMessagesPojo, appMessages);
			Object object = new VelosUtil().setAuditableInfo(appMessages);
			appMessages = updateAppMessages((AppMessages) object,null);
			appMessagesPojo = (AppMessagesPojo) ObjectTransfer.transferObjects(appMessages,
					appMessagesPojo);

		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return appMessagesPojo;
	}
	
	public static AppMessages updateAppMessages(AppMessages appMessages, Session session){
		boolean sessionFlag = false;
		try {
			// check session
			//log.debug("session " + session);
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			// check appmessages pk id is avialable or not
			if (appMessages.getPkAppMsg() != null && appMessages.getPkAppMsg() > 0) {
				//if pk id is available
				appMessages = (AppMessages) session.merge(appMessages);
			}
			else{
				//if pk id is not available 
				session.save(appMessages);
			}
			// check sessionFlag is true then commit the transaction
			if (sessionFlag) { session.getTransaction().commit(); }
			
		} catch (Exception e) {
			//log.debug(" Exception in AppMessagesHelper " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			//log.debug(" session in finally " + session + " /"+ sessionFlag);
			 if (session.isOpen()) {
			 session.close();
			 }
		}
		return appMessages;
	}

	public static AppMessagesPojo getAppMessagesById(AppMessagesPojo appMessagesPojo){
		AppMessages appMessages = new AppMessages();

		try {
			appMessages = (AppMessages) ObjectTransfer
					.transferObjects(appMessagesPojo, appMessages);
			Object object = new VelosUtil().setAuditableInfo(appMessages);
			appMessages = getAppMessagesById((AppMessages) object,null);
			appMessagesPojo = (AppMessagesPojo) ObjectTransfer.transferObjects(appMessages,
					appMessagesPojo);

		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return appMessagesPojo;
	}
	
	public static AppMessages getAppMessagesById(AppMessages appMessages, Session session){
		boolean sessionFlag = false;
		try {
			// check session
			//log.debug("session " + session);
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			// check appmessages pk id is avialable or not
			if (appMessages.getPkAppMsg() != null && appMessages.getPkAppMsg() > 0) {
				//if pk id is available
				appMessages = (AppMessages) session.get(AppMessages.class, appMessages.getPkAppMsg());
			}
			
			// if (sessionFlag) { session.getTransaction().commit(); }
			
		} catch (Exception e) {
			//log.debug(" Exception in AppMessagesHelper " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			//log.debug(" session in finally " + session + " /"	+ sessionFlag);
			 if (session.isOpen()) {
			 session.close();
			 }
		}
		return appMessages;
	}
	
	public static List getGroupsBySiteId(Long siteId){
		return getGroupsBySiteId(siteId,null);
	}
	public static List getGroupsBySiteId(Long siteId,Session session){
		boolean sessionFlag = false;
		List list = new ArrayList();
		try {
			// check session
			//log.debug("session " + session);
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "select g.pk_grp, g.grp_name from er_usrsite_grp e inner join er_usersite u ON e.fk_user_site=u.pk_usersite inner join er_grps g ON g.pk_grp = e.fk_grp_id where u.fk_site="+siteId;
			list = session.createSQLQuery(sql).list();
			
			// if (sessionFlag) { session.getTransaction().commit(); }
			
		} catch (Exception e) {
			//log.debug(" Exception in AppMessagesHelper >> getGroupsBySiteId" + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			//log.debug(" session in finally " + session + " /"					+ sessionFlag);
			 if (session.isOpen()) {
			 session.close();
			 }
		}
		return list;
	}
}
