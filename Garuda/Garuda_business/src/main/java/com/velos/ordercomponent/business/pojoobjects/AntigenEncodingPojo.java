package com.velos.ordercomponent.business.pojoobjects;

import java.util.Set;

import com.velos.ordercomponent.business.domain.AntigenEncoding;

/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class AntigenEncodingPojo extends AuditablePojo {

	private Long pkAntigenEncoding;
	private Long fkAntigen;
	private String version;
	private String searchFormat;
	private String genomicFormat;
	private Boolean validInd;
	private Boolean activeInd;
	private String lastUpdateSource;
	private String createdBySource;
	private Set<AntigenEncoding> encodings;
	
	public Long getPkAntigenEncoding() {
		return pkAntigenEncoding;
	}
	public void setPkAntigenEncoding(Long pkAntigenEncoding) {
		this.pkAntigenEncoding = pkAntigenEncoding;
	}
	public Long getFkAntigen() {
		return fkAntigen;
	}
	public void setFkAntigen(Long fkAntigen) {
		this.fkAntigen = fkAntigen;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getSearchFormat() {
		return searchFormat;
	}
	public void setSearchFormat(String searchFormat) {
		this.searchFormat = searchFormat;
	}
	public String getGenomicFormat() {
		return genomicFormat;
	}
	public void setGenomicFormat(String genomicFormat) {
		this.genomicFormat = genomicFormat;
	}
	public Boolean getValidInd() {
		return validInd;
	}
	public void setValidInd(Boolean validInd) {
		this.validInd = validInd;
	}
	public Boolean getActiveInd() {
		return activeInd;
	}
	public void setActiveInd(Boolean activeInd) {
		this.activeInd = activeInd;
	}	
	
	public String getLastUpdateSource() {
		return lastUpdateSource;
	}
	public void setLastUpdateSource(String lastUpdateSource) {
		this.lastUpdateSource = lastUpdateSource;
	}
	public String getCreatedBySource() {
		return createdBySource;
	}
	public void setCreatedBySource(String createdBySource) {
		this.createdBySource = createdBySource;
	}
	public Set<AntigenEncoding> getEncodings() {
		return encodings;
	}
	public void setEncodings(Set<AntigenEncoding> encodings) {
		this.encodings = encodings;
	}	
	
}
