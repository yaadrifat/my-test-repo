package com.velos.ordercomponent.business.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
@Entity
@Table(name="CB_CORD_IMPORT_MESSAGES")
@SequenceGenerator(name="SEQ_CB_CORD_IMPORT_MESSAGES",sequenceName="SEQ_CB_CORD_IMPORT_MESSAGES",allocationSize=1)
public class CordImportProcessMessages extends Auditable {
	
	private Long pkCordImportProcessMessage;
	private Long messageId;
	private String messageType;
	private String messageText;
	private Integer level;
	private String cbuRegistryId;
	private Long fkCordId;
	private Long fkCordImportHistory;
	private Long cordImportProgress;
	private Boolean submitted;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_CORD_IMPORT_MESSAGES")
	@Column(name="PK_CORD_IMPORT_MESSAGES")
	public Long getPkCordImportProcessMessage() {
		return pkCordImportProcessMessage;
	}
	public void setPkCordImportProcessMessage(Long pkCordImportProcessMessage) {
		this.pkCordImportProcessMessage = pkCordImportProcessMessage;
	}
	@Column(name="MESSAGE_ID")
	public Long getMessageId() {
		return messageId;
	}
	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}
	@Column(name="MESSAGE_TYPE")
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	@Column(name="MESSAGE_TEXT")
	public String getMessageText() {
		return messageText;
	}
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}
	@Column(name="CBU_REGISTRY_ID")
	public String getCbuRegistryId() {
		return cbuRegistryId;
	}
	public void setCbuRegistryId(String cbuRegistryId) {
		this.cbuRegistryId = cbuRegistryId;
	}
	@Column(name="FK_CORD_IMPORT_HISTORY_ID")
	public Long getFkCordImportHistory() {
		return fkCordImportHistory;
	}
	public void setFkCordImportHistory(Long fkCordImportHistory) {
		this.fkCordImportHistory = fkCordImportHistory;
	}
	@Column(name="MESSAGE_LEVEL")
	public Integer getLevel() {
		return level;
	}
	public void setLevel(Integer level) {
		this.level = level;
	}
	@Column(name="CORD_PROGRESS")
	public Long getCordImportProgress() {
		return cordImportProgress;
	}
	public void setCordImportProgress(Long cordImportProgress) {
		this.cordImportProgress = cordImportProgress;
	}
	@Column(name="FK_CORD_ID")
	public Long getFkCordId() {
		return fkCordId;
	}
	public void setFkCordId(Long fkCordId) {
		this.fkCordId = fkCordId;
	}
	@Column(name="CORD_SUBMITTED")
	public Boolean getSubmitted() {
		return submitted;
	}
	public void setSubmitted(Boolean submitted) {
		this.submitted = submitted;
	}

	
}
