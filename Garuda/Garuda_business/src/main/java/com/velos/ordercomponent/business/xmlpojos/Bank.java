package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


public class Bank {
	private String cbb_id;
	private cbus cbus;
    
      
    @XmlElement(name="cbb_id",required=true)
	public String getCbb_id() {
		return cbb_id;
	}
	
	public void setCbb_id(String cbbId) {
		cbb_id = cbbId;
	}

	@XmlElement(name="cbus")
	public cbus getCbus() {
		return cbus;
	}

	public void setCbus(cbus cbus) {
		this.cbus = cbus;
	}
  
}
