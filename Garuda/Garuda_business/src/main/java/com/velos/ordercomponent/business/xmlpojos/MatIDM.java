package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


public class MatIDM {
	private String cbb_val_protocol_used;	
	private XMLGregorianCalendar bld_coll_date;	
	private String cms_cert_lab_ind;
	private String fda_lic_mfg_inst_ind;
	private HBSAg hbsAg;
	private Anti_Hbc antiHbc;
	private Anti_Hcv antiHcv;
	private AntiHiv12o antiHiv12o;
	private AntiHiv12 antiHiv12;
	private AntiHtlv1 antiHtlv1;
	private CmvTotal cmvTotal;
	private Nat_hiv_1_hcv_hbv_mpx hbvMpx;
	private Nat_hbv hbv;
	private Nat_hcv hcv;
	private Nat_hiv hiv;
	private Hiv_p24 hivP24;
	private Syphilis syphilis;
	private WNV_NAT wnvNAT;
	private Chagas chagas;
	private SyphilisCT syphilisCT;
	private Other_idm1 idm1;
	private Other_idm2 idm2;
	private Otheridm3 idm3;
	private Otheridm4 idm4;
	private Otheridm5 idm5;
	private Otheridm6 idm6;
	
	@XmlElement(name="cbb_val_protocol_used")
	public String getCbb_val_protocol_used() {
		return cbb_val_protocol_used;
	}
	public void setCbb_val_protocol_used(String cbbValProtocolUsed) {
		cbb_val_protocol_used = cbbValProtocolUsed;
	}
	@XmlElement(name="bld_coll_date")
	@XmlJavaTypeAdapter(type=XMLGregorianCalendar.class,value=XMLGeorgarianCalanderAdapter.class)
	public XMLGregorianCalendar getBld_coll_date() {
		return bld_coll_date;
	}
	public void setBld_coll_date(XMLGregorianCalendar bldCollDate) {
		bld_coll_date = bldCollDate;
	}
	@XmlElement(name="cms_cert_lab_ind")
	public String getCms_cert_lab_ind() {
		return cms_cert_lab_ind;
	}
	public void setCms_cert_lab_ind(String cmsCertLabInd) {
		cms_cert_lab_ind = cmsCertLabInd;
	}
	@XmlElement(name="fda_lic_mfg_inst_ind")
	public String getFda_lic_mfg_inst_ind() {
		return fda_lic_mfg_inst_ind;
	}
	public void setFda_lic_mfg_inst_ind(String fdaLicMfgInstInd) {
		fda_lic_mfg_inst_ind = fdaLicMfgInstInd;
	}
	@XmlElement(name="HBSAg")
	public HBSAg getHbsAg() {
		return hbsAg;
	}
	public void setHbsAg(HBSAg hbsAg) {
		this.hbsAg = hbsAg;
	}
	@XmlElement(name="anti_hbc")
	public Anti_Hbc getAntiHbc() {
		return antiHbc;
	}
	public void setAntiHbc(Anti_Hbc antiHbc) {
		this.antiHbc = antiHbc;
	}
	@XmlElement(name="anti_hcv")
	public Anti_Hcv getAntiHcv() {
		return antiHcv;
	}
	public void setAntiHcv(Anti_Hcv antiHcv) {
		this.antiHcv = antiHcv;
	}
	@XmlElement(name="anti_hiv_1_2_o")
	public AntiHiv12o getAntiHiv12o() {
		return antiHiv12o;
	}
	public void setAntiHiv12o(AntiHiv12o antiHiv12o) {
		this.antiHiv12o = antiHiv12o;
	}
	@XmlElement(name="anti_hiv_1_2")
	public AntiHiv12 getAntiHiv12() {
		return antiHiv12;
	}
	public void setAntiHiv12(AntiHiv12 antiHiv12) {
		this.antiHiv12 = antiHiv12;
	}
	@XmlElement(name="cmv_total")
	public CmvTotal getCmvTotal() {
		return cmvTotal;
	}
	public void setCmvTotal(CmvTotal cmvTotal) {
		this.cmvTotal = cmvTotal;
	}
	@XmlElement(name="anti_htlv1")	
	public AntiHtlv1 getAntiHtlv1() {
		return antiHtlv1;
	}
	public void setAntiHtlv1(AntiHtlv1 antiHtlv1) {
		this.antiHtlv1 = antiHtlv1;
	}
	@XmlElement(name="nat_hiv_1_hcv_hbv_mpx")	
	public Nat_hiv_1_hcv_hbv_mpx getHbvMpx() {
		return hbvMpx;
	}
	public void setHbvMpx(Nat_hiv_1_hcv_hbv_mpx hbvMpx) {
		this.hbvMpx = hbvMpx;
	}
	@XmlElement(name="nat_hbv")	
	public Nat_hbv getHbv() {
		return hbv;
	}
	public void setHbv(Nat_hbv hbv) {
		this.hbv = hbv;
	}
	@XmlElement(name="nat_hcv")	
	public Nat_hcv getHcv() {
		return hcv;
	}
	public void setHcv(Nat_hcv hcv) {
		this.hcv = hcv;
	}
	@XmlElement(name="nat_hiv")	
	public Nat_hiv getHiv() {
		return hiv;
	}
	public void setHiv(Nat_hiv hiv) {
		this.hiv = hiv;
	}
	@XmlElement(name="HIV_p24")	
	public Hiv_p24 getHivP24() {
		return hivP24;
	}
	public void setHivP24(Hiv_p24 hivP24) {
		this.hivP24 = hivP24;
	}
	@XmlElement(name="syphilis")	
	public Syphilis getSyphilis() {
		return syphilis;
	}
	public void setSyphilis(Syphilis syphilis) {
		this.syphilis = syphilis;
	}
	@XmlElement(name="WNV_NAT")	
	public WNV_NAT getWnvNAT() {
		return wnvNAT;
	}
	public void setWnvNAT(WNV_NAT wnvNAT) {
		this.wnvNAT = wnvNAT;
	}
	@XmlElement(name="chagas")	
	public Chagas getChagas() {
		return chagas;
	}
	public void setChagas(Chagas chagas) {
		this.chagas = chagas;
	}
	@XmlElement(name="syphilisCT")	
	public SyphilisCT getSyphilisCT() {
		return syphilisCT;
	}
	public void setSyphilisCT(SyphilisCT syphilisCT) {
		this.syphilisCT = syphilisCT;
	}
	@XmlElement(name="Other_idm1")	
	public Other_idm1 getIdm1() {
		return idm1;
	}
	public void setIdm1(Other_idm1 idm1) {
		this.idm1 = idm1;
	}
	@XmlElement(name="Other_idm2")	
	public Other_idm2 getIdm2() {
		return idm2;
	}
	public void setIdm2(Other_idm2 idm2) {
		this.idm2 = idm2;
	}
	@XmlElement(name="Otheridm3")	
	public Otheridm3 getIdm3() {
		return idm3;
	}
	public void setIdm3(Otheridm3 idm3) {
		this.idm3 = idm3;
	}
	@XmlElement(name="Otheridm4")	
	public Otheridm4 getIdm4() {
		return idm4;
	}
	public void setIdm4(Otheridm4 idm4) {
		this.idm4 = idm4;
	}
	@XmlElement(name="Otheridm5")	
	public Otheridm5 getIdm5() {
		return idm5;
	}
	public void setIdm5(Otheridm5 idm5) {
		this.idm5 = idm5;
	}
	@XmlElement(name="Otheridm6")	
	public Otheridm6 getIdm6() {
		return idm6;
	}
	public void setIdm6(Otheridm6 idm6) {
		this.idm6 = idm6;
	}
	
	
}
