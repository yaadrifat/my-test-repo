package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CB_FORM_VERSION")
@SequenceGenerator(sequenceName="SEQ_CB_FORM_VERSION",name="SEQ_CB_FORM_VERSION",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class FormVersion extends Auditable{
	private Long pkformversion;		//PK_FORM_VERSION;
	private Long fkform;			//FK_FORM;
	private Long entityId;			//ENTITY_ID;
	private Date dynaformDate;		//DATE_MOMANS_OR_FORMFILL;
	private Long entityType;		//ENTITY_TYPE;
	private Long formseq;			//FORM_SEQ;
	private String void_flag;
	private Date void_date;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_FORM_VERSION")
	@Column(name="PK_FORM_VERSION")
	public Long getPkformversion() {
		return pkformversion;
	}
	public void setPkformversion(Long pkformversion) {
		this.pkformversion = pkformversion;
	}
	@Column(name="FK_FORM")
	public Long getFkform() {
		return fkform;
	}
	public void setFkform(Long fkform) {
		this.fkform = fkform;
	}
	@Column(name="ENTITY_ID")
	public Long getEntityId() {
		return entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	@Column(name="DATE_MOMANS_OR_FORMFILL")
	public Date getDynaformDate() {
		return dynaformDate;
	}
	public void setDynaformDate(Date dynaformDate) {
		this.dynaformDate = dynaformDate;
	}
	@Column(name="ENTITY_TYPE")
	public Long getEntityType() {
		return entityType;
	}
	public void setEntityType(Long entityType) {
		this.entityType = entityType;
	}
	@Column(name="FORM_SEQ")
	public Long getFormseq() {
		return formseq;
	}
	public void setFormseq(Long formseq) {
		this.formseq = formseq;
	}
	@Column(name="VOID_FLAG")
	public String getVoid_flag() {
		return void_flag;
	}
	public void setVoid_flag(String void_flag) {
		this.void_flag = void_flag;
	}
	@Column(name="VOID_DATE")
	public Date getVoid_date() {
		return void_date;
	}
	public void setVoid_date(Date void_date) {
		this.void_date = void_date;
	}
}
