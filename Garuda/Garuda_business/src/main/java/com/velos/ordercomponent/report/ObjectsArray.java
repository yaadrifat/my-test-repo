package com.velos.ordercomponent.report;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.jdbc.Work;



/**
 * @author Anurag Upadhyay
 * 
 *
 */

public class ObjectsArray implements Work {
	
	public static final Log log = LogFactory.getLog(ObjectsArray.class);
	
	private String query;
	private List<Object> objLst;
  
	
	public ObjectsArray(String query) {
		this.query = query;
		
	}
	
	 
	    @Override
	    public void execute( Connection connection ) throws SQLException
	    {
	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        try
	        {
	            ps = connection.prepareStatement( query);
	            ps.setFetchSize(500);
	            rs = ps.executeQuery();
	            System.out.println("Query Result Returned");
	            long startTime=System.currentTimeMillis();
	            
		         
	            objLst = objectList(rs);
	            
		           
		            
	             long endTime=System.currentTimeMillis();
		         System.out.println();
		         System.out.println((endTime-startTime)/1000+" : "+(endTime-startTime)%1000+" MM:SS\n\n");
	        }
	        
	        
	        catch( SQLException e ) { 
	        	
	        	printException(e);
	        }
	        finally  {
	            if( rs != null )  
	            {
	                try {
	                    rs.close();
	                }
	                catch( Exception e ) {printException(e);}
	            }
	            if( ps != null ) 
	            {
	                try {
	                    ps.close();
	                }
	                catch( Exception e ) {printException(e);}
	            }
	        }
	    }
	
	    
	   private List<Object> objectList(ResultSet rs) throws SQLException{
		   
		   ArrayList<Object> result = new ArrayList<Object>();
		   
		   ResultSetMetaData metaData = rs.getMetaData();
		   final int columnCount = metaData.getColumnCount();
		   Object [] obj;
		   
		   
		   while(rs.next()){
			   
			   obj = new Object[columnCount];
			   
			   for (int i=1; i <= columnCount; ++i){				 
		            obj[i-1] = rs.getObject(i);
		        }
			   
			   result.add(obj);
		   }
		   
		   return result;
	   }
	private void printException(Exception e){
		
		log.error(e.getMessage());
		e.printStackTrace();
		
	}


	public List<Object> getObjLst() {
		return objLst;
	}
	
}
