package com.velos.ordercomponent.business.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author Mohiuddin Ali Ahmed
 *
 */
@Entity
@Table(name="GARUDA_THEMES")
@SequenceGenerator(sequenceName="SQ_GARUDA_THEMES",name="SQ_GARUDA_THEMES",allocationSize=1)
public class Theme {

	private Long themeId;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SQ_GARUDA_THEMES")
	@Column(name="PK_THEME_ID")
	public Long getThemeId() {
		return themeId;
	}

	public void setThemeId(Long themeId) {
		this.themeId = themeId;
	}
	
	private String themeName;

	@Column(name="THEME_NAME")
	public String getThemeName() {
		return themeName;
	}
	public void setThemeName(String themeName) {
		this.themeName = themeName;
	}
	
	
}
