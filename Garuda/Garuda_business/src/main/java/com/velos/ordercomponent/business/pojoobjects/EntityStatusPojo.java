package com.velos.ordercomponent.business.pojoobjects;

import com.velos.ordercomponent.business.domain.Auditable;
import java.util.Date;

public class EntityStatusPojo extends Auditable {
	private Long pkEntityStatus;
	private Long entityId;
	private Long entityType;
	private String statusTypeCode;
	private Long statusValue;
	private Date statusDate;
	private Long reasonId;
	private String statusRemarks;
	private String licEligChangeReason;
	private Long addOrUpdate;
	
	
	public Long getAddOrUpdate() {
		return addOrUpdate;
	}
	public void setAddOrUpdate(Long addOrUpdate) {
		this.addOrUpdate = addOrUpdate;
	}
	public Long getPkEntityStatus() {
		return pkEntityStatus;
	}
	public void setPkEntityStatus(Long pkEntityStatus) {
		this.pkEntityStatus = pkEntityStatus;
	}
	public Long getEntityId() {
		return entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	public Long getEntityType() {
		return entityType;
	}
	public void setEntityType(Long entityType) {
		this.entityType = entityType;
	}
	public String getStatusTypeCode() {
		return statusTypeCode;
	}
	public void setStatusTypeCode(String statusTypeCode) {
		this.statusTypeCode = statusTypeCode;
	}
	public Long getStatusValue() {
		return statusValue;
	}
	public void setStatusValue(Long statusValue) {
		this.statusValue = statusValue;
	}
	public Date getStatusDate() {
		return statusDate;
	}
	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}
	/*public long getReasonId() {
		return reasonId;
	}
	public void setReasonId(long reasonId) {
		this.reasonId = reasonId;
	}*/
	public String getStatusRemarks() {
		return statusRemarks;
	}
	public void setStatusRemarks(String statusRemarks) {
		this.statusRemarks = statusRemarks;
	}
	public String getLicEligChangeReason() {
		return licEligChangeReason;
	}
	public void setLicEligChangeReason(String licEligChangeReason) {
		this.licEligChangeReason = licEligChangeReason;
	}	
	
	
}
