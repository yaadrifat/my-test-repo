package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

public class AccountFormsPojo extends AuditablePojo{
	private Long pkAccoutForms;
	private Long fkFormLib;
	private Long fkAcoount;
	private Date formFillDate;
	private Long fkSpecimen;
	
	public Long getPkAccoutForms() {
		return pkAccoutForms;
	}
	public void setPkAccoutForms(Long pkAccoutForms) {
		this.pkAccoutForms = pkAccoutForms;
	}
	public Long getFkFormLib() {
		return fkFormLib;
	}
	public void setFkFormLib(Long fkFormLib) {
		this.fkFormLib = fkFormLib;
	}
	public Long getFkAcoount() {
		return fkAcoount;
	}
	public void setFkAcoount(Long fkAcoount) {
		this.fkAcoount = fkAcoount;
	}
	public Date getFormFillDate() {
		return formFillDate;
	}
	public void setFormFillDate(Date formFillDate) {
		this.formFillDate = formFillDate;
	}
	public Long getFkSpecimen() {
		return fkSpecimen;
	}
	public void setFkSpecimen(Long fkSpecimen) {
		this.fkSpecimen = fkSpecimen;
	}
}
