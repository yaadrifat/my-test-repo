package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;
/****
 * 
 *  Setter Getter Is not used to remove Over-Head
 *  of Method Call.
 *  This POJO is used for reporting purpose
 * 
 * ********/

public class CordFilterPojo {
	
	public Long cord;
	public String site;
	public String registry;
	public String localCbu;
	public String numberOnBag;
	public String isbtCode;
	public String historic;
	public String maternal;
	public String localMat;
	public String patient;
	public Date collectDate;
	public Long nmdpStatus;
	

}
