package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


public class SyphilisCT {

	private String syphilis_ct_sup_react_ind;
	private XMLGregorianCalendar syphilis_ct_sup_react_date;
	private String treponemal_ct_ind;
	private String cms_cert_lab_ind;
	private String fda_lic_mfg_inst_ind;
	@XmlElement(name="syphilis_ct_sup_react_ind")	
	public String getSyphilis_ct_sup_react_ind() {
		return syphilis_ct_sup_react_ind;
	}
	public void setSyphilis_ct_sup_react_ind(String syphilisCtSupReactInd) {
		syphilis_ct_sup_react_ind = syphilisCtSupReactInd;
	}
	@XmlElement(name="syphilis_ct_sup_react_date")	
	@XmlJavaTypeAdapter(type=XMLGregorianCalendar.class,value=XMLGeorgarianCalanderAdapter.class)
	public XMLGregorianCalendar getSyphilis_ct_sup_react_date() {
		return syphilis_ct_sup_react_date;
	}
	public void setSyphilis_ct_sup_react_date(XMLGregorianCalendar syphilisCtSupReactDate) {
		syphilis_ct_sup_react_date = syphilisCtSupReactDate;
	}
	@XmlElement(name="treponemal_ct_ind")	
	public String getTreponemal_ct_ind() {
		return treponemal_ct_ind;
	}
	public void setTreponemal_ct_ind(String treponemalCtInd) {
		treponemal_ct_ind = treponemalCtInd;
	}
	@XmlElement(name="cms_cert_lab_ind")	
	public String getCms_cert_lab_ind() {
		return cms_cert_lab_ind;
	}
	public void setCms_cert_lab_ind(String cmsCertLabInd) {
		cms_cert_lab_ind = cmsCertLabInd;
	}
	@XmlElement(name="fda_lic_mfg_inst_ind")	
	public String getFda_lic_mfg_inst_ind() {
		return fda_lic_mfg_inst_ind;
	}
	public void setFda_lic_mfg_inst_ind(String fdaLicMfgInstInd) {
		fda_lic_mfg_inst_ind = fdaLicMfgInstInd;
	}
	
	
}
