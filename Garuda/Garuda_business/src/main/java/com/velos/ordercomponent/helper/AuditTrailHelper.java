package com.velos.ordercomponent.helper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.util.HibernateUtil;
import com.velos.ordercomponent.queryconstants.AuditTrailQueryConstants;
import com.velos.ordercomponent.helper.OrderAttachmentHelper;

public class AuditTrailHelper implements AuditTrailQueryConstants {

	public static final Log log=LogFactory.getLog(AuditTrailHelper.class);
	
	public static List<Object> getAuditRowDataByEntityId(Long entityId,String viewName,PaginateSearch paginateSearch,int i,String condition,String orderby){
		return getAuditRowDataByEntityId(entityId,viewName,paginateSearch,i,null,condition,orderby); 
	}
	
    public static List<Object> getAuditRowDataByEntityId(Long entityId,String viewName, PaginateSearch paginateSearch,int i,Session session,String condition,String orderby){
    	List<Object> list = new ArrayList<Object>();
    	boolean sessionFlag = false;
		try{
		 //check session  
			if(session== null){
				// create new session
			  session = HibernateUtil.getSessionFactory().getCurrentSession();
			  sessionFlag = true;
			  session.beginTransaction();
			}
			String sql = "";
			if(viewName!=null && viewName.equals(CORD_ROW_LEVEL) && i==1){ 
				sql = AUDIT_TRAIL_ROW_CORD_DATA$ + condition+orderby; 
			}else if(viewName!=null && viewName.equals(CORD_ROW_LEVEL) && i==0){ 
				sql = AUDIT_TRAIL_ROW_CORD_DATA_COUNT + condition;  
			}else if(viewName!=null && viewName.equals(ENTITY_STATUS_ROW_LEVEL) && i==1){
				sql = AUDIT_TRAIL_ROW_ENTITY_STATUS_DATA+ condition+orderby;
			}else if(viewName!=null && viewName.equals(ENTITY_STATUS_ROW_LEVEL) && i==0){
				sql = AUDIT_TRAIL_ROW_ENTITY_STATUS_DATA_COUNT + condition;
			}
			else if(viewName!=null && viewName.equals(ENTITY_STATUS_REASON_ROW_LEVEL)){
			   sql = AUDIT_TRAIL_ROW_ENTITY_STATUS_REASON_DATA;
			}else if(viewName!=null && viewName.equals(MULTIPLE_VALUE_ROW_LEVEL) && i==1){
			   sql = AUDIT_TRAIL_ROW_MULTIPLE_VALUES_DATA + condition+orderby;  
			}else if(viewName!=null && viewName.equals(MULTIPLE_VALUE_ROW_LEVEL)&& i==0 ){
			   sql = AUDIT_TRAIL_ROW_MULTIPLE_VALUES_DATA_COUNT + condition;
			}
			else if(viewName!=null && viewName.equals(REVIEW_ROW_LEVEL)&& i==1){
				   sql = AUDIT_TRAIL_ROW_FINALREVIEW_DATA+ condition+orderby;  
			}else if(viewName!=null && viewName.equals(REVIEW_ROW_LEVEL)&& i==0 ){
				   sql = AUDIT_TRAIL_ROW_FINALREVIEW_DATA_COUNT+ condition;
			}
			else if(viewName!=null && viewName.equals(HLA_ROW_LEVEL)&& i==1){
			   sql = AUDIT_TRAIL_ROW_HLA_DATA+ condition+orderby;  
			}else if(viewName!=null && viewName.equals(HLA_ROW_LEVEL)&& i==0 ){
			   sql = AUDIT_TRAIL_ROW_HLA_DATA_COUNT+ condition;
			}
			else if(viewName!=null && viewName.equals(PATHLA_ROW_LEVEL)&& i==1){
				   entityId = OrderAttachmentHelper.getReceipientId(entityId);
				   session = null;
				   if(session== null){
						// create new session
					  session = HibernateUtil.getSessionFactory().getCurrentSession();
					  sessionFlag = true;
					  session.beginTransaction();
					}
				   sql = AUDIT_TRAIL_ROW_PATHLA_DATA+ condition+orderby;  
				}else if(viewName!=null && viewName.equals(PATHLA_ROW_LEVEL)&& i==0 ){ 
				   entityId = OrderAttachmentHelper.getReceipientId(entityId);
				   session = null;
				   if(session== null){
						// create new session
					   
					  session = HibernateUtil.getSessionFactory().getCurrentSession();
					  sessionFlag = true;
					  session.beginTransaction();
					}
				   sql = AUDIT_TRAIL_ROW_PATHLA_DATA_COUNT+ condition;
				}
			else if(viewName!=null && viewName.equals(LABSUMMARY_ROW_LEVEL)&& i==1 ){
				if (orderby.equals(""))
					orderby = "order by 10 desc";
				else
					orderby = orderby + ", 10 desc";
				sql = AUDIT_TRAIL_ROW_LABSUMMARY_DATA+ condition+orderby;  
			   
			}
			else if(viewName!=null && viewName.equals(LABSUMMARY_ROW_LEVEL)&& i==0){
				sql = AUDIT_TRAIL_ROW_LABSUMMARY_DATA_COUNT+ condition;
				}
			else if(viewName!=null && viewName.equals(NOTES_ROW_LEVEL)&& i==1 ){
			   sql = AUDIT_TRAIL_ROW_NOTES_DATA+ condition+orderby; 
			}
			else if(viewName!=null && viewName.equals(NOTES_ROW_LEVEL)&& i==0){
				   sql = AUDIT_TRAIL_ROW_NOTES_DATA_COUNT+ condition;
				}
			else if(viewName!=null && viewName.equals(FORMS_ROW_LEVEL_IDM)&& i==1 ){
			   sql = AUDIT_TRAIL_ROW_FORMS_IDM+ condition+orderby; 
			}
			else if(viewName!=null && viewName.equals(FORMS_ROW_LEVEL_IDM)&& i==0){
				   sql = AUDIT_TRAIL_ROW_FORMS_IDM_COUNT+ condition;
				}
			else if(viewName!=null && viewName.equals(FORMS_ROW_LEVEL_HHS)&& i==1 ){
				   sql = AUDIT_TRAIL_ROW_FORMS_HHS+ condition+orderby; 
			}
			else if(viewName!=null && viewName.equals(FORMS_ROW_LEVEL_HHS)&& i==0){
				   sql = AUDIT_TRAIL_ROW_FORMS_HHS_COUNT+ condition;
			}
			else if(viewName!=null && viewName.equals(BEST_HLA_ROW_LEVEL)){
			   sql = AUDIT_TRAIL_BEST_HLA;
			}else if(viewName!=null && viewName.equals(PF_ROW_LEVEL)&& i==1 ){
			   sql = AUDIT_TRAIL_PF_DATA+ condition+orderby;  
			}
			else if(viewName!=null && viewName.equals(PF_ROW_LEVEL)&& i==0){
				   sql = AUDIT_TRAIL_PF_DATA_COUNT+ condition;
				}
			else if(viewName!=null && viewName.equals(ID_ROW_LEVEL) && i==1){
			   sql = AUDIT_TRAIL_ID_DATA+ condition+orderby;
			}else if(viewName!=null && viewName.equals(ID_ROW_LEVEL) && i==0){
			   sql = AUDIT_TRAIL_ID_DATA_COUNT+ condition;
			}
			else if(viewName!=null && viewName.equals(PROC_INFO_ROW_LEVEL) && i==1){
			   sql = AUDIT_TRAIL_PROCINFO_DATA+ condition+orderby;
			}else if(viewName!=null && viewName.equals(PROC_INFO_ROW_LEVEL) && i==0){
			   sql = AUDIT_TRAIL_PROCINFO_DATA_COUNT+ condition;
			}
			else if(viewName!=null && viewName.equals(MIN_INFO_ROW_LEVEL)&& i==1){
			   sql = AUDIT_TRAIL_MIN_CRITERIA_DATA+ condition+orderby;
			}
			else if(viewName!=null && viewName.equals(MIN_INFO_ROW_LEVEL)&& i==0){
				   sql = AUDIT_TRAIL_MIN_CRITERIA_DATA_COUNT+ condition;
				}
			
			SQLQuery query = session.createSQLQuery(sql);
			query.setLong(0,entityId);
			if (viewName.equals(PF_ROW_LEVEL)){
				query.setLong(1,entityId); 
				query.setLong(2,entityId);
				query.setLong(3,entityId);
				query.setLong(4,entityId);
				query.setLong(5,entityId);
				query.setLong(6,entityId);
				query.setLong(7,entityId);
			} 
			if (viewName.equals(ID_ROW_LEVEL) || viewName.equals(PROC_INFO_ROW_LEVEL) || viewName.equals(ENTITY_STATUS_ROW_LEVEL) || 
					viewName.equals(MULTIPLE_VALUE_ROW_LEVEL) || viewName.equals(MIN_INFO_ROW_LEVEL)){
				query.setLong(1,entityId);
			}
			if(viewName.equals(HLA_ROW_LEVEL) || viewName.equals(PATHLA_ROW_LEVEL)){
				query.setString(1,entityId.toString());
			}
			if (viewName.equals(LABSUMMARY_ROW_LEVEL)){
				query.setLong(1,entityId);
				query.setLong(2,entityId); 
			}
			
			if (viewName.equals(CORD_ROW_LEVEL)  ){
				query.setLong(1,entityId);
				query.setLong(2,entityId);
			}
			
			if(paginateSearch!=null && i==1){
				query.setFirstResult(paginateSearch.getiPageNo());
				query.setMaxResults(paginateSearch.getiShowRows());
			}
			System.out.println("IN AuditTrailHelper getAuditRowDataByEntityId() generated query="+query.toString());
			list = (List<Object>)query.list();	
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return list;
	}
    
    public static List<Object> getAuditColumnDataByEntityId(Long entityId,String viewName){
		return getAuditColumnDataByEntityId(entityId,viewName,null); 
	}
	
    public static List<Object> getAuditColumnDataByEntityId(Long entityId,String viewName,Session session){
    	List<Object> list = new ArrayList<Object>();
    	boolean sessionFlag = false; 
		try{
		 // check session 
			if(session== null){
				// create new session
			  session = HibernateUtil.getSessionFactory().getCurrentSession();
			  sessionFlag = true;
			  session.beginTransaction(); 
			}
			String sql = "";
			if(viewName!=null && viewName.equals(CORD_COLUMN_LEVEL)){
			   sql = AUDIT_TRAIL_COLUMN_CORD_DATA; 
			}else if(viewName!=null && viewName.equals(ENTITY_STATUS_COLUMN_LEVEL)){
			   sql = AUDIT_TRAIL_COLUMN_ENTITY_STATUS_DATA;
			}else if(viewName!=null && viewName.equals(ENTITY_STATUS_REASON_COLUMN_LEVEL)){
			   sql = AUDIT_TRAIL_COLUMN_ENTITY_STATUS_REASON_DATA;
			}else if(viewName!=null && viewName.equals(MULTIPLE_VALUE_COLUMN_LEVEL)){ 
			   sql = AUDIT_TRAIL_COLUMN_MULTIPLE_VALUES_DATA; 
			}
			SQLQuery query = session.createSQLQuery(sql);
			query.setLong(0,entityId);
			list = (List<Object>)query.list();			
			
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
		}finally{ 
			if(session.isOpen()){ 
				session.close();
			}
		}
		return list;
	}
}
