package com.velos.ordercomponent.business.pojoobjects;

public class FormsPojo extends AuditablePojo{
	
	private Long pkForm;
	private String formsDesc;
	private String version;
	private String isCurrentFlag;
	
	public Long getPkForm() {
		return pkForm;
	}
	public void setPkForm(Long pkForm) {
		this.pkForm = pkForm;
	}
	public String getFormsDesc() {
		return formsDesc;
	}
	public void setFormsDesc(String formsDesc) {
		this.formsDesc = formsDesc;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getIsCurrentFlag() {
		return isCurrentFlag;
	}
	public void setIsCurrentFlag(String isCurrentFlag) {
		this.isCurrentFlag = isCurrentFlag;
	}
	
	
	
}
