package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlJavaTypeAdapter(type=XMLGregorianCalendar.class,value=XMLGeorgarianCalanderAdapter.class)
public class Forms {
    private MRQ mrq;
    private MatIDM matIDM;
    private CBUIdm cbuIdm;
    private FMHQ fmhq;

    @XmlElement(name="MRQ")
	public MRQ getMrq() {
		return mrq;
	}
	
	public void setMrq(MRQ mrq) {
		this.mrq = mrq;
	}

	@XmlElement(name="MatIDM")
	public MatIDM getMatIDM() {
		return matIDM;
	}

	public void setMatIDM(MatIDM matIDM) {
		this.matIDM = matIDM;
	}

	@XmlElement(name="CBUIdm")
	public CBUIdm getCbuIdm() {
		return cbuIdm;
	}

	public void setCbuIdm(CBUIdm cbuIdm) {
		this.cbuIdm = cbuIdm;
	}

	@XmlElement(name="FMHQ")
	public FMHQ getFmhq() {
		return fmhq;
	}

	public void setFmhq(FMHQ fmhq) {
		this.fmhq = fmhq;
	}
  
	
}
