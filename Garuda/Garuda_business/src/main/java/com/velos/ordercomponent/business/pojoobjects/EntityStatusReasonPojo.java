package com.velos.ordercomponent.business.pojoobjects;

import com.velos.ordercomponent.business.domain.Auditable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class EntityStatusReasonPojo extends Auditable {

	private Long pkEntityStatusReason;
	private Long fkEntityStatus;
	private Long fkReasonId;
	private Long fkFormId;
	private Long fkAttachmentId;
	private Boolean notAppCollPriorFlag;
	
	public Long getPkEntityStatusReason() {
		return pkEntityStatusReason;
	}
	public void setPkEntityStatusReason(Long pkEntityStatusReason) {
		this.pkEntityStatusReason = pkEntityStatusReason;
	}	
	
	public Long getFkEntityStatus() {
		return fkEntityStatus;
	}
	public void setFkEntityStatus(Long fkEntityStatus) {
		this.fkEntityStatus = fkEntityStatus;
	}
	public Long getFkReasonId() {
		return fkReasonId;
	}
	public void setFkReasonId(Long fkReasonId) {
		this.fkReasonId = fkReasonId;
	}
	
	public Long getFkFormId() {
		return fkFormId;
	}
	public void setFkFormId(Long fkFormId) {
		this.fkFormId = fkFormId;
	}
	
	public Long getFkAttachmentId() {
		return fkAttachmentId;
	}
	public void setFkAttachmentId(Long fkAttachmentId) {
		this.fkAttachmentId = fkAttachmentId;
	}
	public Boolean getNotAppCollPriorFlag() {
		return notAppCollPriorFlag;
	}
	public void setNotAppCollPriorFlag(Boolean notAppCollPriorFlag) {
		this.notAppCollPriorFlag = notAppCollPriorFlag;
	}
	
	
}
