package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;

public class CBUSamples {

	private String segments_avail;
	private String filter_paper_avail;
	private String rbc_pellets_avail;
	private String dna_aliquots_avail;
	private String cell_aliquots_avail;
	private String cbu_serum_aliquots_avail;
	private String plasma_aliquots_avail;
	private String viable_cell_aliquots_avail;
	private String matrnl_dna_aliquots_avail;
	private String matrnl_cell_aliquots_avail;
	private String matrnl_serum_aliquots_avail;
	private String matrnl_plasma_aliquots_avail;
	private String viable_sampes_not_rep_final_prod;
	private String number_other_rep_aliquots_consist;
	private String number_other_rep_aliquots_alt;
	
	@XmlElement(name="segments_avail")	
	public String getSegments_avail() {
		return segments_avail;
	}
	public void setSegments_avail(String segmentsAvail) {
		segments_avail = segmentsAvail;
	}
	@XmlElement(name="filter_paper_avail")	
	public String getFilter_paper_avail() {
		return filter_paper_avail;
	}
	public void setFilter_paper_avail(String filterPaperAvail) {
		filter_paper_avail = filterPaperAvail;
	}
	@XmlElement(name="rbc_pellets_avail")	
	public String getRbc_pellets_avail() {
		return rbc_pellets_avail;
	}
	public void setRbc_pellets_avail(String rbcPelletsAvail) {
		rbc_pellets_avail = rbcPelletsAvail;
	}
	@XmlElement(name="dna_aliquots_avail")	
	public String getDna_aliquots_avail() {
		return dna_aliquots_avail;
	}
	public void setDna_aliquots_avail(String dnaAliquotsAvail) {
		dna_aliquots_avail = dnaAliquotsAvail;
	}
	@XmlElement(name="cell_aliquots_avail")	
	public String getCell_aliquots_avail() {
		return cell_aliquots_avail;
	}
	public void setCell_aliquots_avail(String cellAliquotsAvail) {
		cell_aliquots_avail = cellAliquotsAvail;
	}
	@XmlElement(name="cbu_serum_aliquots_avail")	
	public String getCbu_serum_aliquots_avail() {
		return cbu_serum_aliquots_avail;
	}
	public void setCbu_serum_aliquots_avail(String cbuSerumAliquotsAvail) {
		cbu_serum_aliquots_avail = cbuSerumAliquotsAvail;
	}
	@XmlElement(name="plasma_aliquots_avail")	
	public String getPlasma_aliquots_avail() {
		return plasma_aliquots_avail;
	}
	public void setPlasma_aliquots_avail(String plasmaAliquotsAvail) {
		plasma_aliquots_avail = plasmaAliquotsAvail;
	}
	@XmlElement(name="viable_cell_aliquots_avail")	
	public String getViable_cell_aliquots_avail() {
		return viable_cell_aliquots_avail;
	}
	public void setViable_cell_aliquots_avail(String viableCellAliquotsAvail) {
		viable_cell_aliquots_avail = viableCellAliquotsAvail;
	}
	@XmlElement(name="matrnl_dna_aliquots_avail")	
	public String getMatrnl_dna_aliquots_avail() {
		return matrnl_dna_aliquots_avail;
	}
	public void setMatrnl_dna_aliquots_avail(String matrnlDnaAliquotsAvail) {
		matrnl_dna_aliquots_avail = matrnlDnaAliquotsAvail;
	}
	@XmlElement(name="matrnl_cell_aliquots_avail")	
	public String getMatrnl_cell_aliquots_avail() {
		return matrnl_cell_aliquots_avail;
	}
	public void setMatrnl_cell_aliquots_avail(String matrnlCellAliquotsAvail) {
		matrnl_cell_aliquots_avail = matrnlCellAliquotsAvail;
	}
	@XmlElement(name="matrnl_serum_aliquots_avail")	
	public String getMatrnl_serum_aliquots_avail() {
		return matrnl_serum_aliquots_avail;
	}
	public void setMatrnl_serum_aliquots_avail(String matrnlSerumAliquotsAvail) {
		matrnl_serum_aliquots_avail = matrnlSerumAliquotsAvail;
	}
	@XmlElement(name="matrnl_plasma_aliquots_avail")	
	public String getMatrnl_plasma_aliquots_avail() {
		return matrnl_plasma_aliquots_avail;
	}
	public void setMatrnl_plasma_aliquots_avail(String matrnlPlasmaAliquotsAvail) {
		matrnl_plasma_aliquots_avail = matrnlPlasmaAliquotsAvail;
	}
	@XmlElement(name="viable_sampes_not_rep_final_prod")	
	public String getViable_sampes_not_rep_final_prod() {
		return viable_sampes_not_rep_final_prod;
	}
	public void setViable_sampes_not_rep_final_prod(
			String viableSampesNotRepFinalProd) {
		viable_sampes_not_rep_final_prod = viableSampesNotRepFinalProd;
	}
	@XmlElement(name="number_other_rep_aliquots_consist")	
	public String getNumber_other_rep_aliquots_consist() {
		return number_other_rep_aliquots_consist;
	}
	public void setNumber_other_rep_aliquots_consist(
			String numberOtherRepAliquotsConsist) {
		number_other_rep_aliquots_consist = numberOtherRepAliquotsConsist;
	}
	@XmlElement(name="number_other_rep_aliquots_alt")	
	public String getNumber_other_rep_aliquots_alt() {
		return number_other_rep_aliquots_alt;
	}
	public void setNumber_other_rep_aliquots_alt(String numberOtherRepAliquotsAlt) {
		number_other_rep_aliquots_alt = numberOtherRepAliquotsAlt;
	}
	
	
}
