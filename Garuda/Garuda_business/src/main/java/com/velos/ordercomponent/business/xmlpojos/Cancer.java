package com.velos.ordercomponent.business.xmlpojos;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class Cancer {

	private String can_leuk_ind;
	private String inher_cl_other_dises_ind;
	/*private Relations relations;*/
	private List<Relations> Relations;
	
	@XmlElement(name="can_leuk_ind")
	public String getCan_leuk_ind() {
		return can_leuk_ind;
	}
	public void setCan_leuk_ind(String canLeukInd) {
		can_leuk_ind = canLeukInd;
	}
	@XmlElement(name="inher_cl_other_dises_ind")
	public String getInher_cl_other_dises_ind() {
		return inher_cl_other_dises_ind;
	}
	public void setInher_cl_other_dises_ind(String inherClOtherDisesInd) {
		inher_cl_other_dises_ind = inherClOtherDisesInd;
	}
	/*@XmlElement(name="Relations")
	public Relations getRelations() {
		return relations;
	}
	public void setRelations(Relations relations) {
		this.relations = relations;
	}*/
	@XmlElement(name="Relations")
	public List<Relations> getRelations() {
		return Relations;
	}
	public void setRelations(List<Relations> relations) {
		Relations = relations;
	}	
	
}
