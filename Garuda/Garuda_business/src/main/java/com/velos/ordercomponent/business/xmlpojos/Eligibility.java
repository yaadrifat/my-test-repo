package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;



public class Eligibility {

	private String eligibilityCode;
    private IneligibleandIncompleteReasons incompleteReasons;
	
	@XmlElement(name="eligibilityCode")
	public String getEligibilityCode() {
		return eligibilityCode;
	}

	public void setEligibilityCode(String eligibilityCode) {
		this.eligibilityCode = eligibilityCode;
	}

	@XmlElement(name="IneligibleandIncompleteReasons")
	public IneligibleandIncompleteReasons getIncompleteReasons() {
		return incompleteReasons;
	}

	public void setIncompleteReasons(
			IneligibleandIncompleteReasons incompleteReasons) {
		this.incompleteReasons = incompleteReasons;
	}
	
	
	
}
