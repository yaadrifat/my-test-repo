package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;
/****
 * 
 * Setter Getter Is not used to remove Over-Head
 *  of Method Call.
 *  This POJO is used for reporting purpose
 * 
 * ********/
public class HLAFilterPojo {
	
	public Date   typingDate;
	public Date   receivedDate;
	public String source;
	public String aType1;
	public String aType2;
	public String bType1;
	public String bType2;
	public String cType1;
	public String cType2;
	public String drb1Type1;
	public String drb1Type2;
	public String drb3Type1;
	public String drb3Type2;
	public String drb4Type1;
	public String drb4Type2;
	public String drb5Type1;
	public String drb5Type2;
	public String dqb1Type1;
	public String dqb1Type2;
	public String dpb1Type1;
	public String dpb1Type2;
	public String user;
	
	
}
