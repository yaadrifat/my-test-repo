package com.velos.ordercomponent.business.xmlpojos;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class AdditionalIds {

	private List<AddID> AddID;

	@XmlElement(name="AddID")
	public List<AddID> getAddID() {
		return AddID;
	}

	public void setAddID(List<AddID> addID) {
		AddID = addID;
	}
	
	
}
