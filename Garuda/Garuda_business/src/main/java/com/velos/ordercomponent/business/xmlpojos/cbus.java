package com.velos.ordercomponent.business.xmlpojos;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


public class cbus {
	private List<cbu> cbu;

	public List<cbu> getCbu() {
		return cbu;
	}

	public void setCbu(List<cbu> cbu) {
		this.cbu = cbu;
	}   

		
	
}
