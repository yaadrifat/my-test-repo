package com.velos.ordercomponent.business.pojoobjects;

import java.util.List;

import com.velos.ordercomponent.business.domain.CodeList;

public class CodeListPopulatedData {
	
	private List<CodeList> raceList;

	private List<CodeList> funCulList;
	
	private List<CodeList> bactCulList;
	
	private List<CodeList> genderIdList;

	
	public List<CodeList> getGenderIdList() {
		return genderIdList;
	}

	public void setGenderIdList(List<CodeList> genderIdList) {
		this.genderIdList = genderIdList;
	}

	public List<CodeList> getBactCulList() {
		return bactCulList;
	}

	public void setBactCulList(List<CodeList> bactCulList) {
		this.bactCulList = bactCulList;
	}

	public List<CodeList> getFunCulList() {
		return funCulList;
	}

	public void setFunCulList(List<CodeList> funCulList) {
		this.funCulList = funCulList;
	}

	public List<CodeList> getRaceList() {
		return raceList;
	}
	
	public void setRaceList(List<CodeList> raceList) {
		this.raceList = raceList;
	}

}
