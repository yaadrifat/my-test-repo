package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.datatype.XMLGregorianCalendar;

public class CFU_Post_CryoPreservation {
    private String CFU_test;
    private String CFU_cnt;
    private XMLGregorianCalendar CFU_test_date;
    private String CFU_test_method;
    private String CFU_test_reason;
    
    @XmlElement(name="CFU_test")	
	public String getCFU_test() {
		return CFU_test;
	}
	public void setCFU_test(String cFUTest) {
		CFU_test = cFUTest;
	}
	@XmlElement(name="CFU_cnt")	
	public String getCFU_cnt() {
		return CFU_cnt;
	}
	public void setCFU_cnt(String cFUCnt) {
		CFU_cnt = cFUCnt;
	}
	@XmlElement(name="CFU_test_date")	
	public XMLGregorianCalendar getCFU_test_date() {
		return CFU_test_date;
	}
	public void setCFU_test_date(XMLGregorianCalendar cFUTestDate) {
		CFU_test_date = cFUTestDate;
	}
	@XmlElement(name="CFU_test_method")	
	public String getCFU_test_method() {
		return CFU_test_method;
	}
	public void setCFU_test_method(String cFUTestMethod) {
		CFU_test_method = cFUTestMethod;
	}
	@XmlElement(name="CFU_test_reason")	
	public String getCFU_test_reason() {
		return CFU_test_reason;
	}
	public void setCFU_test_reason(String cFUTestReason) {
		CFU_test_reason = cFUTestReason;
	}
   
   
}
