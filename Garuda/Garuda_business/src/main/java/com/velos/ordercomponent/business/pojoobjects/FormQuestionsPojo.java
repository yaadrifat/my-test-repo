package com.velos.ordercomponent.business.pojoobjects;


public class FormQuestionsPojo extends AuditablePojo {
	
	private Long pkQuestions; //PK_QUESTIONS
	private String questionDesc; //QUES_DESC
	private String questionHelp; //QUES_HELP
	private String unlicenReqFlag; //UNLICEN_REQ_FLAG
	private String licenReqFlag;  //LICEN_REQ_FLAG
	private String unlicenPriorShipFlag;  //UNLICN_PRIOR_TO_SHIPMENT_FLAG
	private String licenPriorShipFlag;  //LICEN_PRIOR_SHIPMENT_FLAG
	private String cbbNotUseSystem;  //CBB_NOT_USE_SYSTEM
	private String questionHover;  //QUES_HOVER
	private String addCommentFlag;  //ADD_COMMENT_FLAG
	private String assementFlag;  //ASSESMENT_FLAG
	//private Long fkMasterQues; //FK_MASTER_QUES
	//private Long fkDepenentQues; //FK_DEPENDENT_QUES
	//private String fkDepenentQuesVal; //FK_DEPENDENT_QUES_VALUE
	private Long responseType; //RESPONSE_TYPE
	private String quesCode; //QUES_CODE
	//private String quesSeq; //QUES_SEQ
	private String deletedFlag;
	
	public Long getPkQuestions() {
		return pkQuestions;
	}
	public void setPkQuestions(Long pkQuestions) {
		this.pkQuestions = pkQuestions;
	}
	public String getQuestionDesc() {
		return questionDesc;
	}
	public void setQuestionDesc(String questionDesc) {
		this.questionDesc = questionDesc;
	}
	public String getQuestionHelp() {
		return questionHelp;
	}
	public void setQuestionHelp(String questionHelp) {
		this.questionHelp = questionHelp;
	}
	public String getUnlicenReqFlag() {
		return unlicenReqFlag;
	}
	public void setUnlicenReqFlag(String unlicenReqFlag) {
		this.unlicenReqFlag = unlicenReqFlag;
	}
	public String getLicenReqFlag() {
		return licenReqFlag;
	}
	public void setLicenReqFlag(String licenReqFlag) {
		this.licenReqFlag = licenReqFlag;
	}
	public String getUnlicenPriorShipFlag() {
		return unlicenPriorShipFlag;
	}
	public void setUnlicenPriorShipFlag(String unlicenPriorShipFlag) {
		this.unlicenPriorShipFlag = unlicenPriorShipFlag;
	}
	public String getLicenPriorShipFlag() {
		return licenPriorShipFlag;
	}
	public void setLicenPriorShipFlag(String licenPriorShipFlag) {
		this.licenPriorShipFlag = licenPriorShipFlag;
	}
	public String getCbbNotUseSystem() {
		return cbbNotUseSystem;
	}
	public void setCbbNotUseSystem(String cbbNotUseSystem) {
		this.cbbNotUseSystem = cbbNotUseSystem;
	}
	public String getQuestionHover() {
		return questionHover;
	}
	public void setQuestionHover(String questionHover) {
		this.questionHover = questionHover;
	}
	public String getAddCommentFlag() {
		return addCommentFlag;
	}
	public void setAddCommentFlag(String addCommentFlag) {
		this.addCommentFlag = addCommentFlag;
	}
	public String getAssementFlag() {
		return assementFlag;
	}
	public void setAssementFlag(String assementFlag) {
		this.assementFlag = assementFlag;
	}
	public Long getResponseType() {
		return responseType;
	}
	public void setResponseType(Long responseType) {
		this.responseType = responseType;
	}
	public String getQuesCode() {
		return quesCode;
	}
	public void setQuesCode(String quesCode) {
		this.quesCode = quesCode;
	}
	public String getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(String deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	
}
