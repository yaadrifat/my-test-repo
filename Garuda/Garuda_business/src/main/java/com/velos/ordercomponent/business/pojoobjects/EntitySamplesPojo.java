package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.ValidationParameter;
import com.opensymphony.xwork2.validator.annotations.Validations;


public class EntitySamplesPojo extends AuditablePojo {

	private Long pkEntitySamples;
	private Long entityId;
	private Long entityType;
	private Long fkSampleType;
	private Long fkSampleStatus;
	private Long sampleQnty;
	private Long sampleVol;
	private String ocIndict;
	private Date recDate;
	private Date  stDate;
	private Long sequence;
	private String delFlag;
	private Long fkSpecimen;
	private Long noSegAvail;
	private Long filtPap;
	private Long rbcPel;
	private Long extDnaAli;
	private Long nonViaAli;
	private Long noSerAli;
	private Long noPlasAli;
	private Long viaCelAli;
	private Long totCbuAli;
	private Long extDnaMat;
	private Long celMatAli;
	private Long serMatAli;
	private Long plasMatAli;
	private Long totMatAli;
	private Long noMiscMat;
	private Long cbuOthRepConFin;
	private Long cbuRepAltCon;
	
//Created By Gaurav Dated 2 Sept 2012
	private Long convFiltPap;
	private Long convRbcPel;
	private Long convExtDnaAli;
	private Long convNoSerAli;
	private Long convNoPlasAli;
	private Long convNonViaAli;
	private Long convViaCelAli; 
	private Long convNoSegAvail;
	private Long convSerMatAli;
	private Long convPlasMatAli;
	private Long convExtDnaMat;
	private Long convNoMiscMat;
	
	//set for tracking the null and blank in the cord import by Mohiuddin 
	private Boolean filtPapOmitted;
	private Boolean rbcPelOmitted;
	private Boolean extDnaAliOmitted;
	private Boolean nonViaAliOmitted;
	private Boolean noSerAliOmitted;
	private Boolean noPlasAliOmitted;
	private Boolean viaCelAliOmitted;	
	private Boolean extDnaMatOmitted;
	private Boolean serMatAliOmitted;
	private Boolean plasMatAliOmitted;
	private Boolean noMiscMatOmitted;
	private Boolean noSegAvailOmitted;
	private Boolean cbuOthRepConFinOmitted;
	private Boolean cbuRepAltConOmitted;
	
	
	public Long getPkEntitySamples() {
		return pkEntitySamples;
	}
	public void setPkEntitySamples(Long pkEntitySamples) {
		this.pkEntitySamples = pkEntitySamples;
	}
	
	public Long getEntityId() {
		return entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	
	public Long getEntityType() {
		return entityType;
	}
	public void setEntityType(Long entityType) {
		this.entityType = entityType;
	}
	
	public Long getFkSampleType() {
		return fkSampleType;
	}
	public void setFkSampleType(Long fkSampleType) {
		this.fkSampleType = fkSampleType;
	}
	
	public Long getFkSampleStatus() {
		return fkSampleStatus;
	}
	public void setFkSampleStatus(Long fkSampleStatus) {
		this.fkSampleStatus = fkSampleStatus;
	}
	
	public Long getSampleQnty() {
		return sampleQnty;
	}
	public void setSampleQnty(Long sampleQnty) {
		this.sampleQnty = sampleQnty;
	}
	
	
	public Long getSampleVol() {
		return sampleVol;
	}
	public void setSampleVol(Long sampleVol) {
		this.sampleVol = sampleVol;
	}

	public String getOcIndict() {
		return ocIndict;
	}
	public void setOcIndict(String ocIndict) {
		this.ocIndict = ocIndict;
	}
	
	public Date getRecDate() {
		return recDate;
	}
	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}
	
	public Date getStDate() {
		return stDate;
	}
	public void setStDate(Date stDate) {
		this.stDate = stDate;
	}
	
	public Long getSequence() {
		return sequence;
	}
	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}
	
	public String getDelFlag() {
		return delFlag;
	}
	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}
	
	public Long getFkSpecimen() {
		return fkSpecimen;
	}
	public void setFkSpecimen(Long fkSpecimen) {
		this.fkSpecimen = fkSpecimen;
	}
	
	@Validations(customValidators={
		@CustomValidator(
			 type ="longfieldrequire",
			 fieldName = "noSegAvail",				 
			 key="garuda.pf.procedure.noofsegments",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}),
	    @CustomValidator(
			 type ="longrangevalidator",
			 fieldName = "noSegAvail",				 
			 key="garuda.common.range.from0to99",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
					        @ValidationParameter( name = "min", value = "0" ),
					        @ValidationParameter( name = "max", value = "99" )})			 
	})
	public Long getNoSegAvail() {
		return noSegAvail;
	}
		public void setNoSegAvail(Long noSegAvail) {
		this.noSegAvail = noSegAvail;
	}	
	@Validations(customValidators={
		@CustomValidator(
			 type ="longfieldrequire",
			 fieldName = "filtPap",				 
			 key="garuda.cbu.samples.filterpaper",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}),
	    @CustomValidator(
			 type ="longrangevalidator",
			 fieldName = "filtPap",				 
			 key="garuda.common.range.from0to99",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
				            @ValidationParameter( name = "min", value = "0" ),
				            @ValidationParameter( name = "max", value = "99" )})			 
	})
	public Long getFiltPap() {
		return filtPap;
	}
	public void setFiltPap(Long filtPap) {
		this.filtPap = filtPap;
	}	
	@Validations(customValidators={
		@CustomValidator(
			 type ="longfieldrequire",
			 fieldName = "rbcPel",				 
			 key="garuda.cbu.samples.rbcpallets",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}),
	    @CustomValidator(
			 type ="longrangevalidator",
			 fieldName = "rbcPel",				 
			 key="garuda.common.range.from0to99",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
				            @ValidationParameter( name = "min", value = "0" ),
				            @ValidationParameter( name = "max", value = "99" )})		 
	})
	public Long getRbcPel() {
		return rbcPel;
	}
	public void setRbcPel(Long rbcPel) {
		this.rbcPel = rbcPel;
	}	
	@Validations(customValidators={
		@CustomValidator(
			 type ="longfieldrequire",
			 fieldName = "extDnaAli",				 
			 key="garuda.cbu.samples.numextracteddna",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}),
	    @CustomValidator(
			 type ="longrangevalidator",
			 fieldName = "extDnaAli",				 
			 key="garuda.common.range.from0to99",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
				            @ValidationParameter( name = "min", value = "0" ),
				            @ValidationParameter( name = "max", value = "99" )})		 
	})
	public Long getExtDnaAli() {
		return extDnaAli;
	}
	public void setExtDnaAli(Long extDnaAli) {
		this.extDnaAli = extDnaAli;
	}	
	@Validations(customValidators={
		@CustomValidator(
			 type ="longfieldrequire",
			 fieldName = "nonViaAli",				 
			 key="garuda.cbu.samples.numnonviable",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}),
	    @CustomValidator(
			 type ="longrangevalidator",
			 fieldName = "nonViaAli",				 
			 key="garuda.common.range.from0to99",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
					        @ValidationParameter( name = "min", value = "0" ),
					        @ValidationParameter( name = "max", value = "99" )})	 
	})
	public Long getNonViaAli() {
		return nonViaAli;
	}
	public void setNonViaAli(Long nonViaAli) {
		this.nonViaAli = nonViaAli;
	}
	@Validations(customValidators={
	    @CustomValidator(
			 type ="longfieldrequire",
			 fieldName = "noSerAli",				 
			 key="garuda.cbu.samples.numserum",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}),
	    @CustomValidator(
			 type ="longrangevalidator",
			 fieldName = "noSerAli",				 
			 key="garuda.common.range.from0to99",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
				            @ValidationParameter( name = "min", value = "0" ),
				            @ValidationParameter( name = "max", value = "99" )})		 
	})
	public Long getNoSerAli() {
		return noSerAli;
	}
	public void setNoSerAli(Long noSerAli) {
		this.noSerAli = noSerAli;
	}	
	@Validations(customValidators={
		@CustomValidator(
			 type ="longfieldrequire",
			 fieldName = "noPlasAli",				 
			 key="garuda.cbu.samples.numplasma",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}),
	    @CustomValidator(
			 type ="longrangevalidator",
			 fieldName = "noPlasAli",				 
			 key="garuda.common.range.from0to99",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
					        @ValidationParameter( name = "min", value = "0" ),
					        @ValidationParameter( name = "max", value = "99" )})		 
	})
	public Long getNoPlasAli() {
		return noPlasAli;
	}
	public void setNoPlasAli(Long noPlasAli) {
		this.noPlasAli = noPlasAli;
	}	
	@Validations(customValidators={
		@CustomValidator(
			 type ="longfieldrequire",
			 fieldName = "viaCelAli",				 
			 key="garuda.cbu.samples.numviable",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}),
	    @CustomValidator(
			 type ="longrangevalidator",
			 fieldName = "viaCelAli",				 
			 key="garuda.common.range.from0to99",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
					        @ValidationParameter( name = "min", value = "0" ),
					        @ValidationParameter( name = "max", value = "99" )})	 
	})
	public Long getViaCelAli() {
		return viaCelAli;
	}
	public void setViaCelAli(Long viaCelAli) {
		this.viaCelAli = viaCelAli;
	}
	
	public Long getTotCbuAli() {
		return totCbuAli;
	}
	public void setTotCbuAli(Long totCbuAli) {
		this.totCbuAli = totCbuAli;
	}	
	@Validations(customValidators={
		@CustomValidator(
			 type ="longfieldrequire",
			 fieldName = "extDnaMat",				 
			 key="garuda.cbu.samples.matextracteddna",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}),
	    @CustomValidator(
			 type ="longrangevalidator",
			 fieldName = "extDnaMat",				 
			 key="garuda.common.range.from0to99",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
					        @ValidationParameter( name = "min", value = "0" ),
					        @ValidationParameter( name = "max", value = "99" )})		 
	})
	public Long getExtDnaMat() {
		return extDnaMat;
	}
	public void setExtDnaMat(Long extDnaMat) {
		this.extDnaMat = extDnaMat;
	}
	
	public Long getCelMatAli() {
		return celMatAli;
	}
	public void setCelMatAli(Long celMatAli) {
		this.celMatAli = celMatAli;
	}	
	@Validations(customValidators={
		@CustomValidator(
			 type ="longfieldrequire",
			 fieldName = "serMatAli",				 
			 key="garuda.cbu.samples.nummatserum",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}),
	    @CustomValidator(
			 type ="longrangevalidator",
			 fieldName = "serMatAli",				 
			 key="garuda.common.range.from0to99",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
					        @ValidationParameter( name = "min", value = "0" ),
					        @ValidationParameter( name = "max", value = "99" )}) 
	})
	public Long getSerMatAli() {
		return serMatAli;
	}
	public void setSerMatAli(Long serMatAli) {
		this.serMatAli = serMatAli;
	}	
	@Validations(customValidators={
		@CustomValidator(
			 type ="longfieldrequire",
			 fieldName = "plasMatAli",				 
			 key="garuda.cbu.samples.nummatplasma",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}),
	    @CustomValidator(
			 type ="longrangevalidator",
			 fieldName = "plasMatAli",				 
			 key="garuda.common.range.from0to99",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
					        @ValidationParameter( name = "min", value = "0" ),
					        @ValidationParameter( name = "max", value = "99" )})		 
	})
	public Long getPlasMatAli() {
		return plasMatAli;
	}
	public void setPlasMatAli(Long plasMatAli) {
		this.plasMatAli = plasMatAli;
	}
	
	public Long getTotMatAli() {
		return totMatAli;
	}
	public void setTotMatAli(Long totMatAli) {
		this.totMatAli = totMatAli;
	}	
	@Validations(customValidators={
		@CustomValidator(
			 type ="longfieldrequire",
			 fieldName = "noMiscMat",				 
			 key="garuda.cbu.samples.nummiscmat",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}),
	    @CustomValidator(
			 type ="longrangevalidator",
			 fieldName = "noMiscMat",				 
			 key="garuda.common.range.from0to99",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
					        @ValidationParameter( name = "min", value = "0" ),
					        @ValidationParameter( name = "max", value = "99" )})
	})
	public Long getNoMiscMat() {
		return noMiscMat;
	}
	
	public void setNoMiscMat(Long noMiscMat) {
		this.noMiscMat = noMiscMat;
	}	
	@Validations(customValidators={
		@CustomValidator(
			 type ="longfieldrequire",
			 fieldName = "cbuOthRepConFin",				 
			 key="garuda.pf.procedure.numfinalproduct",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}),
	    @CustomValidator(
			 type ="longrangevalidator",
			 fieldName = "cbuOthRepConFin",				 
			 key="garuda.common.range.from0to99",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
					        @ValidationParameter( name = "min", value = "0" ),
					        @ValidationParameter( name = "max", value = "99" )})		 
	})
	public Long getCbuOthRepConFin() {
		return cbuOthRepConFin;
	}
	
	public void setCbuOthRepConFin(Long cbuOthRepConFin) {
		this.cbuOthRepConFin = cbuOthRepConFin;
	}	
	@Validations(customValidators={
		@CustomValidator(
			 type ="longfieldrequire",
			 fieldName = "cbuRepAltCon",				 
			 key="garuda.pf.procedure.numaltcond",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" )}),
	    @CustomValidator(
			 type ="longrangevalidator",
			 fieldName = "cbuRepAltCon",				 
			 key="garuda.common.range.from0to99",
			 shortCircuit=true,
			 parameters = { @ValidationParameter( name = "searchableCheck", value = "true" ),
					        @ValidationParameter( name = "min", value = "0" ),
					        @ValidationParameter( name = "max", value = "99" )}) 
	})
	public Long getCbuRepAltCon() {
		return cbuRepAltCon;
	}
	
	public void setCbuRepAltCon(Long cbuRepAltCon) {
		this.cbuRepAltCon = cbuRepAltCon;
	}
	public Long getConvFiltPap() {
		return convFiltPap;
	}
	public void setConvFiltPap(Long convFiltPap) {
		this.convFiltPap = convFiltPap;
	}
	public Long getConvRbcPel() {
		return convRbcPel;
	}
	public void setConvRbcPel(Long convRbcPel) {
		this.convRbcPel = convRbcPel;
	}
	public Long getConvExtDnaAli() {
		return convExtDnaAli;
	}
	public void setConvExtDnaAli(Long convExtDnaAli) {
		this.convExtDnaAli = convExtDnaAli;
	}
	public Long getConvNoSerAli() {
		return convNoSerAli;
	}
	public void setConvNoSerAli(Long convNoSerAli) {
		this.convNoSerAli = convNoSerAli;
	}
	public Long getConvNoPlasAli() {
		return convNoPlasAli;
	}
	public void setConvNoPlasAli(Long convNoPlasAli) {
		this.convNoPlasAli = convNoPlasAli;
	}
	public Long getConvNonViaAli() {
		return convNonViaAli;
	}
	public void setConvNonViaAli(Long convNonViaAli) {
		this.convNonViaAli = convNonViaAli;
	}
	public Long getConvViaCelAli() {
		return convViaCelAli;
	}
	public void setConvViaCelAli(Long convViaCelAli) {
		this.convViaCelAli = convViaCelAli;
	}
	public Long getConvNoSegAvail() {
		return convNoSegAvail;
	}
	public void setConvNoSegAvail(Long convNoSegAvail) {
		this.convNoSegAvail = convNoSegAvail;
	}
	public Long getConvSerMatAli() {
		return convSerMatAli;
	}
	public void setConvSerMatAli(Long convSerMatAli) {
		this.convSerMatAli = convSerMatAli;
	}
	public Long getConvPlasMatAli() {
		return convPlasMatAli;
	}
	public void setConvPlasMatAli(Long convPlasMatAli) {
		this.convPlasMatAli = convPlasMatAli;
	}
	public Long getConvExtDnaMat() {
		return convExtDnaMat;
	}
	public void setConvExtDnaMat(Long convExtDnaMat) {
		this.convExtDnaMat = convExtDnaMat;
	}
	public Long getConvNoMiscMat() {
		return convNoMiscMat;
	}
	public void setConvNoMiscMat(Long convNoMiscMat) {
		this.convNoMiscMat = convNoMiscMat;
	}
	public Boolean getFiltPapOmitted() {
		return filtPapOmitted;
	}
	public void setFiltPapOmitted(Boolean filtPapOmitted) {
		this.filtPapOmitted = filtPapOmitted;
	}
	public Boolean getRbcPelOmitted() {
		return rbcPelOmitted;
	}
	public void setRbcPelOmitted(Boolean rbcPelOmitted) {
		this.rbcPelOmitted = rbcPelOmitted;
	}
	public Boolean getExtDnaAliOmitted() {
		return extDnaAliOmitted;
	}
	public void setExtDnaAliOmitted(Boolean extDnaAliOmitted) {
		this.extDnaAliOmitted = extDnaAliOmitted;
	}
	public Boolean getNonViaAliOmitted() {
		return nonViaAliOmitted;
	}
	public void setNonViaAliOmitted(Boolean nonViaAliOmitted) {
		this.nonViaAliOmitted = nonViaAliOmitted;
	}
	public Boolean getNoSerAliOmitted() {
		return noSerAliOmitted;
	}
	public void setNoSerAliOmitted(Boolean noSerAliOmitted) {
		this.noSerAliOmitted = noSerAliOmitted;
	}
	public Boolean getNoPlasAliOmitted() {
		return noPlasAliOmitted;
	}
	public void setNoPlasAliOmitted(Boolean noPlasAliOmitted) {
		this.noPlasAliOmitted = noPlasAliOmitted;
	}
	public Boolean getViaCelAliOmitted() {
		return viaCelAliOmitted;
	}
	public void setViaCelAliOmitted(Boolean viaCelAliOmitted) {
		this.viaCelAliOmitted = viaCelAliOmitted;
	}
	public Boolean getExtDnaMatOmitted() {
		return extDnaMatOmitted;
	}
	public void setExtDnaMatOmitted(Boolean extDnaMatOmitted) {
		this.extDnaMatOmitted = extDnaMatOmitted;
	}
	public Boolean getSerMatAliOmitted() {
		return serMatAliOmitted;
	}
	public void setSerMatAliOmitted(Boolean serMatAliOmitted) {
		this.serMatAliOmitted = serMatAliOmitted;
	}
	public Boolean getPlasMatAliOmitted() {
		return plasMatAliOmitted;
	}
	public void setPlasMatAliOmitted(Boolean plasMatAliOmitted) {
		this.plasMatAliOmitted = plasMatAliOmitted;
	}
	public Boolean getNoMiscMatOmitted() {
		return noMiscMatOmitted;
	}
	public void setNoMiscMatOmitted(Boolean noMiscMatOmitted) {
		this.noMiscMatOmitted = noMiscMatOmitted;
	}
	public Boolean getNoSegAvailOmitted() {
		return noSegAvailOmitted;
	}
	public void setNoSegAvailOmitted(Boolean noSegAvailOmitted) {
		this.noSegAvailOmitted = noSegAvailOmitted;
	}
	public Boolean getCbuOthRepConFinOmitted() {
		return cbuOthRepConFinOmitted;
	}
	public void setCbuOthRepConFinOmitted(Boolean cbuOthRepConFinOmitted) {
		this.cbuOthRepConFinOmitted = cbuOthRepConFinOmitted;
	}
	public Boolean getCbuRepAltConOmitted() {
		return cbuRepAltConOmitted;
	}
	public void setCbuRepAltConOmitted(Boolean cbuRepAltConOmitted) {
		this.cbuRepAltConOmitted = cbuRepAltConOmitted;
	}

	
}
