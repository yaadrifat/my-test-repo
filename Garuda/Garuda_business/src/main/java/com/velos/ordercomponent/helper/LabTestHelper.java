package com.velos.ordercomponent.helper;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;

import com.velos.ordercomponent.business.domain.LabTest;
import com.velos.ordercomponent.business.domain.LabTestGrp;
import com.velos.ordercomponent.business.pojoobjects.LabTestGrpPojo;
import com.velos.ordercomponent.business.pojoobjects.LabTestPojo;
import com.velos.ordercomponent.business.pojoobjects.PatLabsPojo;
import com.velos.ordercomponent.business.util.HibernateUtil;
import com.velos.ordercomponent.business.util.ObjectTransfer;

/**
 * @author Ganesavel
 *
 */

public class LabTestHelper {
	public static final Log log = LogFactory.getLog(LabTestHelper.class);
	public static List<Object> getLabtestgroup(){
		return getLabtestgroup(null);
	}
	
	public static List<Object> getLabtestgroup(Session session){
		List labgrps=null;
		try{
			// check session
				if(session==null){
					session=HibernateUtil.getSessionFactory().getCurrentSession();
					session.beginTransaction();
				}
			String query="select * from er_labgroup order by pk_labgroup";
			labgrps=session.createSQLQuery(query).list();
		}catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage());
			e.printStackTrace();
			
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return labgrps;
	}
	
	public static List getLabtestlist(Long pklabgrp){
		return getLabtestlist(pklabgrp,null);
	}
	
	public static List<Object> getLabtestlist(Long pklabgrp,Session session){
		List labtestlist=null;
		try{
			// check session
				if(session==null){
					session=HibernateUtil.getSessionFactory().getCurrentSession();
					session.beginTransaction();
				}
			String query="select * from er_labtest ltest, er_labtestgrp ltestgrp where ltest.pk_labtest= ltestgrp.fk_testid and ltestgrp.fk_labgroup='"+pklabgrp+"' order by ltest.pk_labtest";
			labtestlist=session.createSQLQuery(query).list();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e.getMessage());
			
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return labtestlist;
	}
	
	public static Long getLabgrppks(String grptype){
		return getLabgrppks(grptype,null);
	}
	
	public static Long getLabgrppks(String grptype,Session session){
		Long pklabgrp=null;
		try{
			// check session
				if(session==null){
					session=HibernateUtil.getSessionFactory().getCurrentSession();
					session.beginTransaction();
				}
			String query="select PK_LABGROUP from ER_LABGROUP where GROUP_TYPE='"+grptype+"'";
			List lst=session.createSQLQuery(query).list();
			if(lst.get(0)!=null){
				pklabgrp = ((BigDecimal) lst.get(0)).longValue();
			}
			//log.debug("Pklabgroup : "+pklabgrp);
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e.getMessage());
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return pklabgrp;
	}
	
	public static LabTestGrpPojo saveorupdateLTestgrp(LabTestGrpPojo labgrpPojo,LabTestPojo labTestPojo){
		labTestPojo=saveorupdateLTest(labTestPojo);
		if(labTestPojo.getPkLabtest()!=null){
			//log.debug("labTestPojo.getPkLabtest"+labTestPojo.getPkLabtest());
			Long fktestid=labTestPojo.getPkLabtest();
			labgrpPojo.setFktestid(fktestid);
		}
		return saveorupdateTestgrp(labgrpPojo,null);
	}
	public static LabTestGrpPojo saveorupdateTestgrp(LabTestGrpPojo labgrpPojo,Session session){
		LabTestGrpPojo labTestGrpPojo=null;
		LabTestGrp labTestGrp=null;
		boolean sessionFlag = false;
		if(labgrpPojo!=null){
			labTestGrp=new LabTestGrp();
			labTestGrp=(LabTestGrp) ObjectTransfer.transferObjects(labgrpPojo, labTestGrp);
		}
		
		try{
			// check session
				if(session==null){
					session=HibernateUtil.getSessionFactory().getCurrentSession();
					session.beginTransaction();
					sessionFlag = true;
				}
			if(labTestGrp.getPklabtestgrp()!=null){
				//log.debug("merging labTestGrp");
				session.merge(labTestGrp);
			}else{
				//log.debug("saving labTestGrp");
				session.save(labTestGrp);
			}
			if(sessionFlag){
				session.getTransaction().commit();
			}
			if(labTestGrp!=null){
				labTestGrpPojo=new LabTestGrpPojo();
				labTestGrpPojo=(LabTestGrpPojo) ObjectTransfer.transferObjects(labTestGrp, labTestGrpPojo);
			}
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e.getMessage());
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return labTestGrpPojo;
	}
	public static LabTestPojo saveorupdateLTest(LabTestPojo labTestPojo){
		return saveorupdateLTest(labTestPojo,null);
	}
	public static LabTestPojo saveorupdateLTest(LabTestPojo labTestPojo,Session session){
		LabTestPojo labTestPojo2=null;
		LabTest labTest=null;
		boolean sessionFlag = false;
		if(labTestPojo!=null){
			labTest=new LabTest();
			labTest=(LabTest) ObjectTransfer.transferObjects(labTestPojo, labTest);
		}
		
		try{
			// check session
				if(session==null){
					session=HibernateUtil.getSessionFactory().getCurrentSession();
					session.beginTransaction();
					sessionFlag = true;
				}
			if(labTest.getPkLabtest()!=null){
				//log.debug("merging labTest");
				session.merge(labTest);
			}else{
				//log.debug("saving labTest");
				session.save(labTest);
			}
			if(sessionFlag){
				session.getTransaction().commit();
			}
			if(labTest!=null){
				labTestPojo2=new LabTestPojo();
				labTestPojo2=(LabTestPojo) ObjectTransfer.transferObjects(labTest, labTestPojo2);
			}
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e.getMessage());
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return labTestPojo2;
		
	}
	
	public static Long getLabTestInfoByName(String testName){
		return getLabTestInfoByName(testName,null,null);
	}
	
	public static Long getLabTestInfoByName(String testName,String testShrtName){
		return getLabTestInfoByName(testName,testShrtName,null);
	}
	
	public static Long getLabTestInfoByName(String testName,String testShrtName,Session session){
		Long pklabTest=null;
		try{
			// check session
				if(session==null){
					session=HibernateUtil.getSessionFactory().getCurrentSession();
					session.beginTransaction();
				}
			String query="select PK_LABTEST from ER_LABTEST where LABTEST_NAME='"+testName+"'";
			query = testShrtName == null ? query : query+"and LABTEST_SHORTNAME='"+testShrtName+"'";
			List lst=session.createSQLQuery(query).list();
			if(lst.size()>0){
				if(lst.get(0)!=null){
					pklabTest = ((BigDecimal) lst.get(0)).longValue();
				}
			}
			//log.debug("PklabTest : "+pklabTest);
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e.getMessage());
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return pklabTest;
	}
}
