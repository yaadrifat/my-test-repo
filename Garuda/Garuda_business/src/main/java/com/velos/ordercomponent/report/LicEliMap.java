package com.velos.ordercomponent.report;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.jdbc.Work;

import com.velos.ordercomponent.report.AntigenBestHla.HLA;


public class LicEliMap implements Work{
	public static final Log log = LogFactory.getLog(LicEliMap.class);
	
	private String query;
	
	private  Map<String, Map<Long,ArrayList<Long>>> entityMap;
	Map<Long, Object []> cord;
	Map<Long, ArrayList<Long>> reason;
	
	public LicEliMap(String query, Map<Long, Object []> cord, Map<Long, ArrayList<Long>> reason) {
		
		this.query = query;
		this.cord = cord;
		this.reason = reason;
		
		
		
	}
	 @Override
	    public void execute( Connection connection ) throws SQLException
	    {
	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        try
	        {
	            ps = connection.prepareStatement( query);
	            ps.setFetchSize(5000);
	            rs = ps.executeQuery();
	           
	            long startTime=System.currentTimeMillis();            
		           
	            entityMap = prepMapObj(rs, cord, reason);		            
		            
	             long endTime=System.currentTimeMillis();
		        
		         System.out.println((endTime-startTime)/1000+" : "+(endTime-startTime)%1000+" MM:SS\n\n");
	        }
	        
	        
	        catch( SQLException e ) { 
	        	
	        	printException(e);
	        } catch (ParseException e) {
	        	
	        	printException(e);
			}
	        finally  {
	            if( rs != null )  
	            {
	                try {
	                    rs.close();
	                }
	                catch( Exception e ) {printException(e);}
	            }
	            if( ps != null ) 
	            {
	                try {
	                    ps.close();
	                }
	                catch( Exception e ) {printException(e);}
	            }
	        }
	    }
	
	 private  Map<String, Map<Long,ArrayList<Long>>> prepMapObj(ResultSet rs, Map<Long, Object []> cord, Map<Long, ArrayList<Long>> reason) throws SQLException, ParseException{
		 
		 Map<String, Map<Long,ArrayList<Long>>> entityMap = new HashMap<String, Map<Long,ArrayList<Long>>>();
		 ArrayList<Long>  eligility;
		 ArrayList<Long>  licensure;
		 
		 HashMap<Long, ArrayList<Long>> licMap = new HashMap<Long, ArrayList<Long>>();
		 HashMap<Long, ArrayList<Long>> eligMap   = new HashMap<Long, ArrayList<Long>>();
		
		// ResultSetMetaData metaData = rs.getMetaData();
		// final int columnCount = metaData.getColumnCount();
	
		 Long entityId = null ;
		 Long pkEntity =null;
		 ArrayList<Long> reasonList = null;
		 String codeType = null;
		 Long unLicReason = null; 
		 Long eligReason  = null;
		 Object[] obj = null;
		
		  while( rs.next())  
          {
			  
			  entityId = rs.getLong(1);
			  pkEntity = rs.getLong(2);
			  codeType = rs.getString(3);
			  
			  obj = cord.get(entityId);		
			  
			  if(obj != null){
				
				  unLicReason  = obj[1] == null ? null:((BigDecimal) obj[1]).longValue();
				  eligReason   = obj[2] == null ? null:((BigDecimal) obj[2]).longValue();
				  
				 if( pkEntity.equals(unLicReason) || pkEntity.equals(eligReason)){
					 
					 codeType = codeType == null ? "EXCLUDE" : codeType;
					 switch(HLA.valueOf(codeType)){
					 
					 case licence:						 
						 reasonList = reason.get(pkEntity)== null ? null : reason.get(pkEntity);
						/* 
						 licensure = licMap.get(entityId);
						 
						 if(licensure != null){							 
							 licensure.add(reasonId);							 
						 }else{							 
							 licensure = new ArrayList<Long>();
							 licensure.add(reasonId);
						 }*/
						 
						 licMap.put(entityId/* CordID*/, reasonList);
						 break;
						 
					 case eligibility:						 
						 reasonList = reason.get(pkEntity)== null ? null : reason.get(pkEntity);
						 
						/* eligility = licMap.get(entityId);
						 
						 if(eligility != null){							 
							 eligility.add(reasonId);							 
						 }else{							 
							 eligility = new ArrayList<Long>();
							 eligility.add(reasonId);
						 }*/
						 
						 eligMap.put(entityId/* CordID*/, reasonList);						 
						 break;
						 
					 case EXCLUDE:
							break;				
					 
					 }
					 
				 }
			  }
			  /*obj = new  Object[columnCount];
			  key = rs.getLong(1);
			  
			  for (int i=1; i <= columnCount; ++i){	
				  
		            obj[i-1] = rs.getObject(i);
		            
		        }	*/
			  
			 
          }
		  entityMap.put("licence", licMap);
		  entityMap.put("eligibility", eligMap);
		 return entityMap;
	 }
	private void printException(Exception e){
		
		log.error(e.getMessage());
		e.printStackTrace();
		
	}
	
	/**
	 * @return the entityMap
	 */
	public  Map<String, Map<Long,ArrayList<Long>>> getEntityMap() {
		return entityMap;
	}
	
	


}

