package com.velos.ordercomponent.business.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CB_FORMS")
@SequenceGenerator(sequenceName = "SEQ_CB_FORMS", name = "SEQ_CB_FORMS", allocationSize = 1)
@org.hibernate.annotations.Entity(dynamicInsert = true, dynamicUpdate = true)
public class FormsDomain extends Auditable {
	
	private Long pkForm;
	private String formsDesc;
	private String version;
	private String isCurrentFlag;
	private String deletedFlag;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CB_FORMS")
	@Column(name = "PK_FORM")
	public Long getPkForm() {
		return pkForm;
	}

	public void setPkForm(Long pkForm) {
		this.pkForm = pkForm;
	}

	@Column(name = "FORMS_DESC")
	public String getFormsDesc() {
		return formsDesc;
	}

	public void setFormsDesc(String formsDesc) {
		this.formsDesc = formsDesc;
	}

	@Column(name = "VERSION")
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Column(name = "IS_CURRENT_FLAG")
	public String getIsCurrentFlag() {
		return isCurrentFlag;
	}

	public void setIsCurrentFlag(String isCurrentFlag) {
		this.isCurrentFlag = isCurrentFlag;
	}

	@Column(name = "DELETEDFLAG")
	public String getDeletedFlag() {
		return deletedFlag;
	}

	public void setDeletedFlag(String deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

}
