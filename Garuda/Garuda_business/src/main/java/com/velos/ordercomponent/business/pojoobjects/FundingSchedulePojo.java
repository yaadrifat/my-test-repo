package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

public class FundingSchedulePojo  extends AuditablePojo{
	
	private Long pkFundingSchedule;
	private Long fkCbbId;
	private Date dateToGenerate;
	private Date cbbRegistrationDate;
	private Long notificationBefore;
	private String generatedStatus;
	private Boolean deletedFlag;
	
	public Long getPkFundingSchedule() {
		return pkFundingSchedule;
	}
	public void setPkFundingSchedule(Long pkFundingSchedule) {
		this.pkFundingSchedule = pkFundingSchedule;
	}
	public Long getFkCbbId() {
		return fkCbbId;
	}
	public void setFkCbbId(Long fkCbbId) {
		this.fkCbbId = fkCbbId;
	}
	public Date getDateToGenerate() {
		return dateToGenerate;
	}
	public void setDateToGenerate(Date dateToGenerate) {
		this.dateToGenerate = dateToGenerate;
	}
	public Date getCbbRegistrationDate() {
		return cbbRegistrationDate;
	}
	public void setCbbRegistrationDate(Date cbbRegistrationDate) {
		this.cbbRegistrationDate = cbbRegistrationDate;
	}
	public Long getNotificationBefore() {
		return notificationBefore;
	}
	public void setNotificationBefore(Long notificationBefore) {
		this.notificationBefore = notificationBefore;
	}
	public String getGeneratedStatus() {
		return generatedStatus;
	}
	public void setGeneratedStatus(String generatedStatus) {
		this.generatedStatus = generatedStatus;
	}
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	

}
