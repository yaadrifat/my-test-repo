package com.velos.ordercomponent.helper;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.opensymphony.xwork2.ActionContext;
import com.velos.eres.web.user.UserJB;
import com.velos.ordercomponent.business.domain.Address;
import com.velos.ordercomponent.business.domain.CBB;
import com.velos.ordercomponent.business.domain.CBBProcedures;
import com.velos.ordercomponent.business.domain.CBBProceduresInfo;
import com.velos.ordercomponent.business.domain.FundingGuidelines;
import com.velos.ordercomponent.business.domain.FundingSchedule;
import com.velos.ordercomponent.business.domain.Grps;
import com.velos.ordercomponent.business.domain.Person;
import com.velos.ordercomponent.business.domain.Site;
import com.velos.ordercomponent.business.domain.User;
import com.velos.ordercomponent.business.domain.UserDomain;
import com.velos.ordercomponent.business.domain.UserSiteGroup;
import com.velos.ordercomponent.business.pojoobjects.AddressPojo;
import com.velos.ordercomponent.business.pojoobjects.CBBPojo;
import com.velos.ordercomponent.business.pojoobjects.CBBProcedureInfoPojo;
import com.velos.ordercomponent.business.pojoobjects.CBBProcedurePojo;
import com.velos.ordercomponent.business.pojoobjects.FundingGuidelinesPojo;
import com.velos.ordercomponent.business.pojoobjects.FundingSchedulePojo;
import com.velos.ordercomponent.business.pojoobjects.GrpsPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.pojoobjects.PersonPojo;
import com.velos.ordercomponent.business.pojoobjects.SitePojo;
import com.velos.ordercomponent.business.util.HibernateUtil;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.VelosMidConstants;
import com.velos.ordercomponent.business.util.VelosUtil;
import com.velos.ordercomponent.exception.MidErrorConstants;

public class CBBHelper {

	public static final Log log=LogFactory.getLog(CBBHelper.class);
	private static UserJB userObject = null;
	
	public static Map<Long,CBB> getCBBMapWithIds(){
		return getCBBMapWithIds(null);
	}
	
	public static Map<Long,CBB> getCBBMapWithIds(Session session){
		log.debug("getCBBMapWithIds method start in CBBHelper");
		Map<Long,CBB> cbbMap = new HashMap<Long, CBB>();
		CBB cbb = null;
		boolean sessionFlag = false;		
		try{
			if(session== null){
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				
				session.beginTransaction();
			}
			String sql = "from CBB cbb";
			Query query = session.createQuery(sql);
			//query.addEntity("cbb", CBB.class);
			List<CBB> list = (List<CBB>)query.list();
			for(CBB c : list){
				//log.debug("inside for loop");
				cbbMap.put(c.getPkCbbId(), c);
			}
			for(Map.Entry<Long,CBB> me : cbbMap.entrySet()){
//				log.debug("CBB MAp KEy------"+me.getKey()+me.getValue().getSite().getSiteName());
			}
		}catch (Exception e) {
			
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return cbbMap;
	}
	
	public static List<SitePojo> getSites(){
		return getSites(null);
	}
	
    @SuppressWarnings("unchecked")
	public static List<SitePojo> getSites(Session session){
    	log.debug("getSites method start in CBBHelper");
    	////log.debug("Uservalue==========>>>>>>>> inside getsites");
    	List<Site> sites = new ArrayList<Site>();
    	List<SitePojo> sitesPojos = new ArrayList<SitePojo>();
    	ActionContext ac = ActionContext.getContext();
		HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
		ResourceBundle resource = ResourceBundle.getBundle(VelosMidConstants.EXCEPTION_PROPERTYFILE_NAME);
		HttpSession sessions = request.getSession(true);
		
    	Site site = new Site();
    	SitePojo sitePojo = null;
    	boolean sessionFlag = false;
    	List<Object> objects = null;
    	
		try{
			
			if(sessions != null){
				userObject=(UserJB)sessions.getAttribute(MidErrorConstants.GARUDA_USER);
			}		
			 ////log.debug("Uservalue==========>>>>>>>> inside getsites"+((Integer)userObject.getUserId()).longValue());
			 
			if(session== null){
				
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			//String sql = "From Site left join CBB on CBB.siteId=Site.siteId where CBB.siteId in (select fkSite From UserSite where fkUser="+((Integer)userObject.getUserId()).longValue()+" and userRight <> 0) and siteCodelstType in (select pkCodeId from CodeList where lower(type) ='site_type' and lower(subType)='cbb') and CBB.cdrUser=1 order by UPPER(siteIdentifier)";
			String sql="select {s.*} from er_site s left join cbb c on c.fk_site=s.pk_site where c.fk_site in (select us.fk_site from er_usersite us where us.fk_user="+((Integer)userObject.getUserId()).longValue()+" and us.usersite_right<>0) and s.fk_codelst_type=(select pk_codelst from er_codelst where lower(codelst_type)='site_type' and lower(codelst_subtyp)='cbb') and c.using_cdr=1 order by UPPER(nvl(s.site_id,0))";
			//sites = session.createSQLQuery(sql).list();
			log.debug("sql from getSItes method::::"+sql);
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity("s",Site.class);
			sites= (List<Site>)query.list();
			for(Site s : sites){
				sitePojo = new SitePojo();
				sitePojo = (SitePojo)ObjectTransfer.transferObjects(s,sitePojo);
				sitesPojos.add(sitePojo);  //change here
			}
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return sitesPojos;
	}
	
    
    public static List<SitePojo> getSitesForQueryBuilder(){
		return getSitesForQueryBuilder(null);
	}
	
    @SuppressWarnings("unchecked")
	public static List<SitePojo> getSitesForQueryBuilder(Session session){
    	log.debug("getSites method start in CBBHelper");
    	////log.debug("Uservalue==========>>>>>>>> inside getsites");
    	List<Site> sites = new ArrayList<Site>();
    	List<SitePojo> sitesPojos = new ArrayList<SitePojo>();
    	ActionContext ac = ActionContext.getContext();
		HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
		ResourceBundle resource = ResourceBundle.getBundle(VelosMidConstants.EXCEPTION_PROPERTYFILE_NAME);
		HttpSession sessions = request.getSession(true);
		
    	Site site = new Site();
    	SitePojo sitePojo = null;
    	boolean sessionFlag = false;
    	List<Object> objects = null;
    	
		try{
			
			if(sessions != null){
				userObject=(UserJB)sessions.getAttribute(MidErrorConstants.GARUDA_USER);
			}		
			 ////log.debug("Uservalue==========>>>>>>>> inside getsites"+((Integer)userObject.getUserId()).longValue());
			 
			if(session== null){
				
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			//String sql = "From Site left join CBB on CBB.siteId=Site.siteId where CBB.siteId in (select fkSite From UserSite where fkUser="+((Integer)userObject.getUserId()).longValue()+" and userRight <> 0) and siteCodelstType in (select pkCodeId from CodeList where lower(type) ='site_type' and lower(subType)='cbb') and CBB.cdrUser=1 order by UPPER(siteIdentifier)";
			String sql="select {s.*} from er_site s left join cbb c on c.fk_site=s.pk_site where c.fk_site in (select us.fk_site from er_usersite us where us.fk_user="+((Integer)userObject.getUserId()).longValue()+" and us.usersite_right<>0)order by UPPER(nvl(s.site_id,0))";
			//sites = session.createSQLQuery(sql).list();
			log.debug("sql from getSItes method::::"+sql);
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity("s",Site.class);
			sites= (List<Site>)query.list();
			for(Site s : sites){
				sitePojo = new SitePojo();
				sitePojo = (SitePojo)ObjectTransfer.transferObjects(s,sitePojo);
				sitesPojos.add(sitePojo);
			}
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return sitesPojos;
	}
	
    
    
	public static List<SitePojo> getSitesWithPagination(PaginateSearch paginateSearch,int i){
		return getSitesWithPagination(paginateSearch,i,null);
	}
	
    public static List<SitePojo> getSitesWithPagination(PaginateSearch paginateSearch,int i,Session session){
    	//log.debug("getSites method start in CBBHelper");
    	//log.debug("Uservalue==========>>>>>>>> inside getsites");
    	List<Site> sites = new ArrayList<Site>();
    	List<SitePojo> sitesPojos = new ArrayList<SitePojo>();
    	ActionContext ac = ActionContext.getContext();
		HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
		ResourceBundle resource = ResourceBundle.getBundle(VelosMidConstants.EXCEPTION_PROPERTYFILE_NAME);
		HttpSession sessions = request.getSession(true);
		
    	Site site = new Site();
    	SitePojo sitePojo = null;
    	boolean sessionFlag = false;
    	
		try{
			
			if(sessions != null){
				userObject=(UserJB)sessions.getAttribute(MidErrorConstants.GARUDA_USER);
			}		
			 ////log.debug("Uservalue==========>>>>>>>> inside getsites"+((Integer)userObject.getUserId()).longValue());
			 
			if(session== null){
				
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "From Site where siteId in (select fkSite From UserSite where fkUser="+((Integer)userObject.getUserId()).longValue()+" and userRight <> 0) and siteCodelstType in (select pkCodeId from CodeList where lower(type) ='site_type' and lower(subType)='cbb')";
			Query query = session.createQuery(sql);
			if(paginateSearch!=null && i==1){
				query.setFirstResult(paginateSearch.getiPageNo());
				query.setMaxResults(paginateSearch.getiShowRows());
			}
			sites = query.list();
			for(Site s : sites){
				sitePojo = new SitePojo();
				sitePojo = (SitePojo)ObjectTransfer.transferObjects(s,sitePojo);
				sitesPojos.add(sitePojo);
			}
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return sitesPojos;
	}
 
    public static List getSitesCount(PaginateSearch paginateSearch,int i,String condition){
		return getSitesCount(paginateSearch,i,condition,null);
	}
	
    public static List getSitesCount(PaginateSearch paginateSearch,int i,String condition,Session session){

    	ActionContext ac = ActionContext.getContext();
		HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
		HttpSession sessions = request.getSession(true);
		List countLst=new ArrayList();

		try{
			
			if(sessions != null){
				userObject=(UserJB)sessions.getAttribute(MidErrorConstants.GARUDA_USER);
			}		
			 
			if(session== null){
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String sql = "select count(siteId) From Site where siteId in (select fkSite From UserSite where fkUser="+((Integer)userObject.getUserId()).longValue()+" and userRight <> 0) and siteCodelstType in (select pkCodeId from CodeList where lower(type) ='site_type' and lower(subType)='cbb')"+condition;
			Query query = session.createQuery(sql);
			
			countLst=query.list();
			
			//System.out.println("\n\n\n\n\nhql in count Query order by................."+sql);
			
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return countLst;
	}
	
	
	
    
    public static List<SitePojo> getSitesWithPaginationSort(PaginateSearch paginateSearch,int i,String condition,String orderBy){
		return getSitesWithPaginationSort(paginateSearch,i,condition,orderBy,null);
	}
	
    public static List<SitePojo> getSitesWithPaginationSort(PaginateSearch paginateSearch,int i,String condition,String orderBy,Session session){
    	//log.debug("getSites method start in CBBHelper");
    	//log.debug("Uservalue==========>>>>>>>> inside getsites");
    	List<Site> sites = new ArrayList<Site>();
    	List<SitePojo> sitesPojos = new ArrayList<SitePojo>();
    	ActionContext ac = ActionContext.getContext();
		HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
		ResourceBundle resource = ResourceBundle.getBundle(VelosMidConstants.EXCEPTION_PROPERTYFILE_NAME);
		HttpSession sessions = request.getSession(true);
		
    	Site site = new Site();
    	SitePojo sitePojo = null;
    	boolean sessionFlag = false;
    	
		try{
			
			if(sessions != null){
				userObject=(UserJB)sessions.getAttribute(MidErrorConstants.GARUDA_USER);
			}		
			 ////log.debug("Uservalue==========>>>>>>>> inside getsites"+((Integer)userObject.getUserId()).longValue());
			 
			if(session== null){
				
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "From Site where siteId in (select fkSite From UserSite where fkUser="+((Integer)userObject.getUserId()).longValue()+" and userRight <> 0) and siteCodelstType in (select pkCodeId from CodeList where lower(type) ='site_type' and lower(subType)='cbb') "+condition;
			if(orderBy!=null && orderBy!=""){
				sql=sql+orderBy;
			}
			//System.out.println("hql order by"+sql);
			Query query = session.createQuery(sql);
			if(paginateSearch!=null && i==1){
				query.setFirstResult(paginateSearch.getiPageNo());
				query.setMaxResults(paginateSearch.getiShowRows());
			}
			sites = query.list();
			for(Site s : sites){
				sitePojo = new SitePojo();
				sitePojo = (SitePojo)ObjectTransfer.transferObjects(s,sitePojo);
				sitesPojos.add(sitePojo);
			}
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return sitesPojos;
	}
    
    public static List<SitePojo> getSitesByCriteria(String criteria,PaginateSearch paginateSearch,int i){
		return getSitesByCriteria(criteria, paginateSearch, i,null);
	}
    
    public static List<SitePojo> getSitesByCriteria(String criteria ,PaginateSearch paginateSearch, int i, Session session){
    	//log.debug("getSites method start in CBBHelper");
    	//log.debug("Uservalue==========>>>>>>>> inside getsites");
    	List<Site> sites = new ArrayList<Site>();
    	List<SitePojo> sitesPojos = new ArrayList<SitePojo>();
    	ActionContext ac = ActionContext.getContext();
		HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
		ResourceBundle resource = ResourceBundle.getBundle(VelosMidConstants.EXCEPTION_PROPERTYFILE_NAME);
		HttpSession sessions = request.getSession(true);
		
    	Site site = new Site();
    	SitePojo sitePojo = null;
    	boolean sessionFlag = false;
    	
		try{
			
			if(sessions != null){
				userObject=(UserJB)sessions.getAttribute(MidErrorConstants.GARUDA_USER);
			}		
			 //log.debug("Uservalue==========>>>>>>>> inside getsites"+((Integer)userObject.getUserId()).longValue());
			 
			if(session== null){
				
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "From Site where ("+ criteria +")" + " and siteId in (select fkSite From UserSite where fkUser="+((Integer)userObject.getUserId()).longValue()+" and userRight <> 0)";
			
			Query query = session.createQuery(sql);
			if(paginateSearch!=null && i==1){
				query.setFirstResult(paginateSearch.getiPageNo());
				query.setMaxResults(paginateSearch.getiShowRows());
			}
			sites = query.list();

			for(Site s : sites){
				sitePojo = new SitePojo();
				sitePojo = (SitePojo)ObjectTransfer.transferObjects(s,sitePojo);
				sitesPojos.add(sitePojo);
			}
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return sitesPojos;
	}
	
    public static List<GrpsPojo> getGroups(Long siteId){
		return getGroups(siteId,null);		
	}
    
    public static List<GrpsPojo> getGroups(Long siteId,Session session){
    	List<Grps> grpsDomain= new ArrayList<Grps>();
    	List<GrpsPojo> grpsPojos= new ArrayList<GrpsPojo>();
    	
    	ActionContext ac = ActionContext.getContext();
		HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
		ResourceBundle resource = ResourceBundle.getBundle(VelosMidConstants.EXCEPTION_PROPERTYFILE_NAME);
		HttpSession sessions = request.getSession(true);
		
    	Grps grps= new Grps();
    	GrpsPojo grpsPojo = null;
	    boolean sessionFlag=false;
	    try{
	    	
	    	if(sessions != null){
				userObject=(UserJB)sessions.getAttribute(MidErrorConstants.GARUDA_USER);
				
			}	
	    	if(session== null){
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
	      String sql = "select {g.*} from er_grps g left join er_usrsite_grp u on u.FK_GRP_ID=g.PK_GRP left join er_usersite eu on eu.PK_USERSITE=u.FK_USER_SITE where u.fk_user = ? and eu.fk_site = ?";
	      SQLQuery query = session.createSQLQuery(sql);
	      query.addEntity("g", Grps.class);
	      query.setLong(0,((Integer)userObject.getUserId()).longValue());
	      query.setLong(1, siteId);
	      grpsDomain = query.list();
			for(Grps g : grpsDomain){
				grpsPojo = new GrpsPojo();
				grpsPojo=(GrpsPojo)ObjectTransfer.transferObjects(g, grpsPojo);
				grpsPojos.add(grpsPojo);	
			}
	
	    	}catch (Exception e) {
	    		log.error(e.getMessage());
	    		e.printStackTrace();
	    	}finally{
	    		if(session.isOpen()){
	    			session.close();
	    		}
	    	}
    	return grpsPojos; 	
    }
    
    public static Map<Long,List<GrpsPojo>> getGroupsOfAllUserBySiteId(Long siteId){
		return getGroupsOfAllUserBySiteId(siteId,null);		
	}
    
    public static Map<Long,List<GrpsPojo>> getGroupsOfAllUserBySiteId(Long siteId,Session session){
    	List<Grps> grpsDomain= new ArrayList<Grps>();
    	List<GrpsPojo> grpsPojos= null;  
    	List<Object> objects = new ArrayList<Object>();
    	Map<Long,List<GrpsPojo>> map = new HashMap<Long, List<GrpsPojo>>();
    	UserSiteGroup userSiteGroup = null;    
    	Grps group = null;
    	Grps grps= new Grps();
    	GrpsPojo grpsPojo = null;
	    boolean sessionFlag=false;
	    try{	    	
	    	  if(session== null){
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			  }
		      String sql = "select {g.*},{u.*} from er_grps g left join er_usrsite_grp u on u.FK_GRP_ID=g.PK_GRP left join er_usersite eu on eu.PK_USERSITE=u.FK_USER_SITE where eu.fk_site = ?";
		      SQLQuery query = session.createSQLQuery(sql);
		      query.addEntity("g", Grps.class);
		      query.addEntity("u", UserSiteGroup.class);
		      query.setLong(0, siteId);
		      objects = query.list();
		      if(objects!=null){
		    	for(Object o : objects){ 
		    	  Object[] objArr = (Object[])o;
		    	  group = new Grps();
		    	  group = (Grps)objArr[0];
		    	  userSiteGroup = new UserSiteGroup();
		    	  userSiteGroup = (UserSiteGroup)objArr[1];
		    	  if(map.containsKey(userSiteGroup.getFkUser())){
		    		  grpsPojos= map.get(userSiteGroup.getFkUser());  
		    		  grpsPojo = new GrpsPojo();
					  grpsPojo=(GrpsPojo)ObjectTransfer.transferObjects(group, grpsPojo);
					  grpsPojos.add(grpsPojo);
		    	  }else{
		    		  grpsPojos= new ArrayList<GrpsPojo>();
		    		  grpsPojo = new GrpsPojo();
					  grpsPojo=(GrpsPojo)ObjectTransfer.transferObjects(group, grpsPojo);
					  grpsPojos.add(grpsPojo);
					  map.put(userSiteGroup.getFkUser(), grpsPojos);
		    	  }
		    	  
		    	}				
		      }
	    }catch (Exception e) {
	    		log.error(e.getMessage());
	    		e.printStackTrace();
	    }finally{
	    		if(session.isOpen()){
	    			session.close();
	    		}
	    }
    	return map; 	
    }        
    
	public static List<SitePojo> getCBBDetails(Long id){
		return getCBBDetails(id, null);
	}
	
	public static List<SitePojo> getCBBDetails(Long id,Session session){
		//log.debug("getCBBMapWithIds method start in CBBHelper");
		List<Site> sites = new ArrayList<Site>();
    	List<SitePojo> sitesPojos = new ArrayList<SitePojo>();
    	List<Object> objects = null;
		Address address = null;
		Address address1 =null;
		Address addressDryShipper =null;
		CBB cbb = null;
		Person person = null;
		AddressPojo addressPojo = null;
		AddressPojo addressPojo1 =null;
		AddressPojo addressDryShipperPojo =null;
		PersonPojo personPojo = null;
		CBBPojo cbbPojo = null;
	    Site site = null;
	    SitePojo sitePojo = null;
		boolean sessionFlag = false;		
		try{
			if(session== null){
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			StringBuffer sql = new StringBuffer("select {cb.*},{p.*},{a.*},{s.*},{a1.*},{a2.*} from er_site s left join CBB cb ON s.PK_SITE=cb.FK_SITE left join EPAT.Person p on cb.FK_PERSON=p.PK_PERSON left join ER_ADD a on s.FK_PERADD=a.PK_ADD left join ER_ADD a1 on a1.PK_ADD=cb.FK_CBU_PCK_ADD left join ER_ADD a2 on a2.PK_ADD=cb.FK_DRY_SHIPPER_ADD");
			if(id!=null){
				sql.append(" where s.PK_SITE=?");
			}
			SQLQuery query = session.createSQLQuery(sql.toString());
			query.addEntity("cb", CBB.class);
			query.addEntity("p",Person.class);
			query.addEntity("a",Address.class);
			query.addEntity("s",Site.class);
			query.addEntity("a1",Address.class);
			query.addEntity("a2",Address.class);
			if(id!=null){
				query.setLong(0, id);
			}
			objects = (List<Object>)query.list();
			for(Object o : objects){
				Object[] obj = (Object[])o;
				cbb = new CBB();
				person = new Person();
				address = new Address();
				site = new Site();
				address1 = new Address();
				addressDryShipper = new Address();
				if(obj != null){
					cbb = (CBB)obj[0];
					person = (Person)obj[1];
					address = (Address)obj[2];
					site = (Site)obj[3];
					address1 = (Address)obj[4];
					addressDryShipper  = (Address)obj[5];
					if(person!=null)
					{
					  cbb.setPerson(person);
					}
					if(address1!=null){
						cbb.setAddress(address1);
					}
					if(addressDryShipper!=null){
						cbb.setAddressDryShipper(addressDryShipper);
					}
					if(address!=null){
					site.setAddress(address);
					}
					site.setCbb(cbb);
				}
				if(site!=null){
					sites.add(site);
				}
			}
			
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		for(Site c : sites){
			cbbPojo = new CBBPojo();
			addressPojo = new AddressPojo();
			addressPojo1 = new AddressPojo();
			personPojo = new PersonPojo();
			addressDryShipperPojo = new AddressPojo();
			sitePojo = new SitePojo();
			if(c.getCbb()!=null){
			cbbPojo = (CBBPojo)ObjectTransfer.transferObjects(c.getCbb(), cbbPojo);
			    if(c.getCbb().getPerson()!=null){
				  personPojo = (PersonPojo)ObjectTransfer.transferObjects(c.getCbb().getPerson(), personPojo);
				  cbbPojo.setPerson(personPojo);
				  addressPojo1=(AddressPojo)ObjectTransfer.transferObjects(c.getCbb().getAddress(), addressPojo1);
				  cbbPojo.setAddress(addressPojo1);
				  addressDryShipperPojo = (AddressPojo)ObjectTransfer.transferObjects(c.getCbb().getAddressDryShipper(), addressDryShipperPojo);
				  cbbPojo.setAddressDryShipper(addressDryShipperPojo);
				}
			}
			sitePojo = (SitePojo)ObjectTransfer.transferObjects(c, sitePojo);
			if(c.getAddress()!=null){
				  addressPojo = (AddressPojo)ObjectTransfer.transferObjects(c.getAddress(), addressPojo);
				  sitePojo.setAddress(addressPojo);
			}
			sitePojo.setCbb(cbbPojo);
			sitesPojos.add(sitePojo);
		}
		return sitesPojos;
	}
	
	public static List<SitePojo> getCBBDetailsByIds(Long[] ids){
		return getCBBDetailsByIds(ids, null);
	}	
	
	public static List<SitePojo> getCBBDetailsByIds(Long[] ids,Session session){
		//log.debug("getCBBMapWithIds method start in CBBHelper");
		List<Site> sites = new ArrayList<Site>();
    	List<SitePojo> sitesPojos = new ArrayList<SitePojo>();
    	List<Object> objects = null;
		Address address = null;
		Address address1 =null;
		Address addressDryShipper =null;
		Person person = null;
		AddressPojo addressPojo = null;
		AddressPojo addressPojo1 = null;
		AddressPojo addressDryShipperPojo =null;
		PersonPojo personPojo = null;
		CBB cbb = null;
		CBBPojo cbbPojo = null;
		Site site = null;
	    SitePojo sitePojo = null;
		boolean sessionFlag = false;		
		try{
			if(session== null){
				// create new session
			  session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			StringBuffer sql = new StringBuffer("select {cb.*},{p.*},{a.*},{s.*},{a1.*},{a2.*} from er_site s left join CBB cb ON s.PK_SITE=cb.FK_SITE left join EPAT.Person p on cb.FK_PERSON=p.PK_PERSON left join ER_ADD a on s.FK_PERADD=a.PK_ADD left join ER_ADD a1 on a1.PK_ADD=cb.FK_CBU_PCK_ADD left join ER_ADD a2 on a2.PK_ADD=cb.FK_DRY_SHIPPER_ADD");
			if(ids.length>0){
				sql.append(" where s.PK_SITE in (:ids)");
			}
			SQLQuery query = session.createSQLQuery(sql.toString());
			query.addEntity("cb", CBB.class);
			query.addEntity("p",Person.class);
			query.addEntity("a",Address.class);
			query.addEntity("s",Site.class);
			query.addEntity("a1",Address.class);
			query.addEntity("a2",Address.class);
			if(ids.length>0){
				query.setParameterList("ids", ids);
			}
			objects = (List<Object>)query.list();
			for(Object o : objects){
				Object[] obj = (Object[])o;
				cbb = new CBB();
				person = new Person();
				address = new Address();
				address1 = new Address();
				addressDryShipper = new Address();
				site = new Site();
				if(obj != null){
					cbb = (CBB)obj[0];
					person = (Person)obj[1];
					address = (Address)obj[2];
					site = (Site)obj[3];
					address1 = (Address)obj[4];
					addressDryShipper  = (Address)obj[5];
					if(person!=null)
					{
					  cbb.setPerson(person);
					}
					if(address1!=null){
						cbb.setAddress(address1);
					}
					if(address!=null){
						site.setAddress(address);
					}
					if(addressDryShipper!=null){
						cbb.setAddressDryShipper(addressDryShipper);
					}
					site.setCbb(cbb);
				}
				if(site!=null){
					sites.add(site);
				}
			}
			
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		if(sites!=null && sites.size()>0){
			for(Site c : sites){
				cbbPojo = new CBBPojo();
				addressPojo = new AddressPojo();
				addressPojo1 = new AddressPojo();
				addressDryShipperPojo = new AddressPojo();
				personPojo = new PersonPojo();
				sitePojo = new SitePojo();
				if(c.getCbb()!=null){
				cbbPojo = (CBBPojo)ObjectTransfer.transferObjects(c.getCbb(), cbbPojo);
					if(c.getCbb().getPerson()!=null){
						personPojo = (PersonPojo)ObjectTransfer.transferObjects(c.getCbb().getPerson(), personPojo);
						addressPojo1=(AddressPojo)ObjectTransfer.transferObjects(c.getCbb().getAddress(), addressPojo1);
						  cbbPojo.setAddress(addressPojo1);
						addressDryShipperPojo = (AddressPojo)ObjectTransfer.transferObjects(c.getCbb().getAddressDryShipper(), addressDryShipperPojo);
						  cbbPojo.setAddressDryShipper(addressDryShipperPojo);  
					}
				}
				sitePojo = (SitePojo)ObjectTransfer.transferObjects(c, sitePojo);
				if(c.getAddress()!=null){
					  addressPojo = (AddressPojo)ObjectTransfer.transferObjects(c.getAddress(), addressPojo);
					  sitePojo.setAddress(addressPojo);
			    }				
				cbbPojo.setPerson(personPojo);
				sitePojo.setCbb(cbbPojo);
				sitesPojos.add(sitePojo);
			}
		}
		return sitesPojos;
	}
	
	public static CBBPojo saveCBBDefaults(CBBPojo cbbPojo ){
		return saveCBBDefaults(cbbPojo,null);
	}
	
	public static CBBPojo saveCBBDefaults(CBBPojo cbbPojo , Session session){
	    	log.debug("saveCBBDefaults with session");
			log.debug("CBBHelper : saveCBBDefaults Start ");
			CBB cbb = new CBB();
			boolean sessionFlag = false;
			try{
			 // check session 
			if(session== null){
				// create new session
			  session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			cbb = (CBB)ObjectTransfer.transferObjects(cbbPojo, cbb);
			cbb = (CBB)new VelosUtil().setAuditableInfo(cbb);
			if(cbb.getPkCbbId()!=null)
			{session.merge(cbb);
			}else{
				session.save(cbb);
			}
			cbbPojo = (CBBPojo)ObjectTransfer.transferObjects(cbb, cbbPojo);
			if(sessionFlag){
				// close session, if session created in this method
				session.getTransaction().commit();
			}
			}catch(Exception e){
				log.error(e.getMessage());
				e.printStackTrace();
			}finally{
				if(session.isOpen()){
					session.close();
				}
			}
			//log.debug("CBBHelper : saveCBBDefaults Method End ");
			return cbbPojo;
	}
	
	public static List<CBBProcedurePojo> getCBBProcedure(Long ids,String siteIds,PaginateSearch paginateSearch,int i){
		return getCBBProcedure(ids,siteIds,paginateSearch,i,null);
	}	
	
	@SuppressWarnings("unchecked")
	public static List<CBBProcedurePojo> getCBBProcedure(Long id,String siteIds,PaginateSearch paginateSearch,int i,Session session){
		//log.debug("getCBBMapWithIds method start in CBBHelper");
		////log.debug("Inside getCBBProcedure --.>>");
		List<CBBProcedurePojo> cbbPojos = new ArrayList<CBBProcedurePojo>();
		List<CBBProcedures> cbbs = null;
		CBBProcedurePojo cbbPojo = null;
		boolean sessionFlag = false;		
		try{
			if(session== null){
				// create new session
			  session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			StringBuffer sql = new StringBuffer("From CBBProcedures cbb where fkSite in("+siteIds+") order by procStartDate desc");
			Query query = session.createQuery(sql.toString());
			if(paginateSearch!=null && i==1){
				query.setFirstResult(paginateSearch.getiPageNo());
				query.setMaxResults(paginateSearch.getiShowRows());
			}
			cbbs =(List<CBBProcedures>) query.list();
			
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		
		if(cbbs!=null && cbbs.size()>0){
		for(CBBProcedures c :cbbs){
			cbbPojo = new CBBProcedurePojo();
			cbbPojo = (CBBProcedurePojo)ObjectTransfer.transferObjects(c, cbbPojo);
			cbbPojos.add(cbbPojo);
		}
		}
		return cbbPojos;
	}
	
	
	public static List<CBBProcedurePojo> getCBBProcedureSort(Long ids,String siteIds,PaginateSearch paginateSearch,int i, String condition,String orderBy){
		return getCBBProcedureSort(ids,siteIds,paginateSearch,i,condition, orderBy,null);
	}	
	
	@SuppressWarnings("unchecked")
	public static List<CBBProcedurePojo> getCBBProcedureSort(Long id,String siteIds,PaginateSearch paginateSearch,int i,String condition,String orderBy,Session session){
		//log.debug("getCBBMapWithIds method start in CBBHelper");
		////log.debug("Inside getCBBProcedure --.>>");
		List<CBBProcedurePojo> cbbPojos = new ArrayList<CBBProcedurePojo>();
		List<CBBProcedures> cbbs = null;
		CBBProcedurePojo cbbPojo = null;
		boolean sessionFlag = false;		
		try{
			if(session== null){
				// create new session
			  session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			
			//System.out.println("orderBy:::"+orderBy);
			String sql = "From CBBProcedures cbb where fkSite in ("+siteIds+") "+condition;
			if(orderBy!=null && orderBy!=""){
				sql=sql+orderBy;
			}else{
				sql=sql+" order by procStartDate desc";
			}
			
			//System.out.println("sql getCBBProcedureSort Helper::::::::::::::::::::::::::::::"+sql);
			Query query = session.createQuery(sql.toString());
			if(paginateSearch!=null && i==1){
				query.setFirstResult(paginateSearch.getiPageNo());
				query.setMaxResults(paginateSearch.getiShowRows());
				//System.out.println("\n\n\n\n\n\nPagno in helper:::::::"+paginateSearch.getiPageNo()+":ishowrows:"+paginateSearch.getiShowRows());
			}
			
			cbbs =(List<CBBProcedures>) query.list();
			
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		
		if(cbbs!=null && cbbs.size()>0){
			for(CBBProcedures c :cbbs){
				cbbPojo = new CBBProcedurePojo();
				cbbPojo = (CBBProcedurePojo)ObjectTransfer.transferObjects(c, cbbPojo);
				cbbPojos.add(cbbPojo);
			}
		}
		
		//System.out.println("after converiting into pojo object size in Helper:::::::::::::"+cbbPojos.size());
		return cbbPojos;
	}
	
	public static List getCBBProcedureCount(String sites,String condition,int i){
		return getCBBProcedureCount(sites,condition,i,null);
	}
	public static List getCBBProcedureCount(String sites,String condition, int i,Session session){
		List countLst=new ArrayList();
		String sql="";
		try{
			if(session== null){
			  session = HibernateUtil.getSessionFactory().getCurrentSession();
			  session.beginTransaction();
			}
			sql="select count(cbb) From CBBProcedures cbb where fkSite in ("+sites+") "+condition;
			//System.out.println("sql in helper:::::::::::::::::::::"+sql);			
			Query query=session.createQuery(sql);
			countLst=query.list();
			
			//System.out.println("values::::::"+countLst.get(0).toString());
			
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		
		
		return countLst;
	}
	
	
	public static List<CBBProcedureInfoPojo> getCBBProcedureInfo(String ids){
		return getCBBProcedureInfo(ids, null);
	}	
	
	@SuppressWarnings("unchecked")
	public static List<CBBProcedureInfoPojo> getCBBProcedureInfo(String id,Session session){
		//log.debug("getCBBMapWithIds method start in CBBHelper");
		List<CBBProcedureInfoPojo> cbbPojos = new ArrayList<CBBProcedureInfoPojo>();
		List<CBBProceduresInfo> cbbs = null;
		CBBProcedureInfoPojo cbbPojo = null;
		try{
			if(session== null){

			  session = HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			
			StringBuffer sql = new StringBuffer("from CBBProceduresInfo cbb");  //y stringbuffer
			if(id!=null && !id.trim().equals("")){
				
				sql.append(" where fkProcessingId in("+id+") order by fkProcessingId desc");
			}
			////log.debug("query is================"+sql.toString());
			
			cbbs = (List<CBBProceduresInfo>) session.createQuery(sql.toString()).list();
			
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		for(CBBProceduresInfo c :cbbs){
			cbbPojo = new CBBProcedureInfoPojo();
			cbbPojo = (CBBProcedureInfoPojo)ObjectTransfer.transferObjects(c, cbbPojo);
			cbbPojos.add(cbbPojo);
		}
		return cbbPojos;
	}	
	
	public static CBBProcedureInfoPojo saveCBBProcedureDetails(CBBProcedureInfoPojo cbbProcedureInfoPojo , Session session){
    	//log.debug("saveCBBProcedureInfo with session");
		//log.debug("CBBHelper : saveCBBProcedureDetails Start ");
		////log.debug("Inside SaveCBBProceduredetails --.>>");
		CBBProceduresInfo cbb = new CBBProceduresInfo();
		boolean sessionFlag = false;
		try{
		 // check session 
		if(session== null){
			// create new session
		  session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}
		//log.debug("in cbb healper");
		cbb = (CBBProceduresInfo)ObjectTransfer.transferObjects(cbbProcedureInfoPojo, cbb);
		cbb = (CBBProceduresInfo)new VelosUtil().setAuditableInfo(cbb);
		if(cbb.getPkProcInfoId()!=null)
		{
			//log.debug("in cbb procedure update");
			session.merge(cbb);
		}else{
			//log.debug("in cbb save");
			session.save(cbb);
		}
		cbbProcedureInfoPojo = (CBBProcedureInfoPojo)ObjectTransfer.transferObjects(cbb, cbbProcedureInfoPojo);
		if(sessionFlag){
			// close session, if session created in this method
			session.getTransaction().commit();
		}
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		//log.debug("CBBHelper : saveCBBDefaults Method End ");
		return cbbProcedureInfoPojo;
}
	
	public static CBBProcedureInfoPojo saveCBBProcedureDetails(CBBProcedureInfoPojo cbbProcedureInfoPojo ){
		return saveCBBProcedureDetails(cbbProcedureInfoPojo,null);
	}
	
	
	public static CBBProcedurePojo saveCBBProcedure(CBBProcedurePojo cbbProcedurePojo ){
		return saveCBBProcedure(cbbProcedurePojo,null);
	}
	
	
	public static CBBProcedurePojo saveCBBProcedure(CBBProcedurePojo cbbProcedurePojo , Session session){
    	//log.debug("saveCBBProcedure with session");
		//log.debug("CBBHelper : saveCBBProcedure Start ");
		////log.debug("Inside saveCBBProcedure --.>>");
		CBBProcedures cbb = new CBBProcedures();
		boolean sessionFlag = false;
		try{
		 // check session 
		if(session== null){
			// create new session
		  session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}
		//log.debug("in cbb healper");
		cbb = (CBBProcedures)ObjectTransfer.transferObjects(cbbProcedurePojo, cbb);
		cbb = (CBBProcedures)new VelosUtil().setAuditableInfo(cbb);
		if(cbb.getPkProcId()!=null)
		{
			//log.debug("in cbb procedure update");
			session.merge(cbb);
		}else{
			//log.debug("in cbb save");
			session.save(cbb);
		}
		cbbProcedurePojo = (CBBProcedurePojo)ObjectTransfer.transferObjects(cbb, cbbProcedurePojo);
		if(sessionFlag){
			// close session, if session created in this method
			session.getTransaction().commit();
		}
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		//log.debug("CBBHelper : saveCBBDefaults Method End ");
		return cbbProcedurePojo;
}
	
	public static CBBProcedureInfoPojo updateCBBProcedureInfo(CBBProcedureInfoPojo cbbProcedureInfoPojo, CBBProcedurePojo cbbProcedurePojo){
		
		return updateCBBProcedureInfo(cbbProcedureInfoPojo,cbbProcedurePojo,null);	
	}
	
	public static CBBProcedureInfoPojo updateCBBProcedureInfo(CBBProcedureInfoPojo cbbProcedureInfoPojo, CBBProcedurePojo cbbProcedurePojo , Session session){
    	CBBProceduresInfo cbbProceduresInfo = new CBBProceduresInfo();
    	CBBProcedures cbbProcedures = new CBBProcedures();
		boolean sessionFlag = false;
		try{
		 // check session 
		if(session== null){
			// create new session
		  session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}
		//log.debug("in cbb healper");
		cbbProcedures = (CBBProcedures)ObjectTransfer.transferObjects(cbbProcedurePojo, cbbProcedures);
		cbbProcedures = (CBBProcedures)new VelosUtil().setAuditableInfo(cbbProcedures);
		cbbProcedureInfoPojo.setCbbProcedures(cbbProcedures);
		cbbProceduresInfo = (CBBProceduresInfo)ObjectTransfer.transferObjects(cbbProcedureInfoPojo, cbbProceduresInfo);
		cbbProceduresInfo = (CBBProceduresInfo)new VelosUtil().setAuditableInfo(cbbProceduresInfo);
		
		if(cbbProceduresInfo.getPkProcInfoId()!=null && cbbProceduresInfo.getPkProcInfoId() > 0)
		{
			//log.debug("in cbb procedure update");
			session.merge(cbbProcedures);
			session.merge(cbbProceduresInfo);
		}
		cbbProcedureInfoPojo = (CBBProcedureInfoPojo)ObjectTransfer.transferObjects(cbbProceduresInfo, cbbProcedureInfoPojo);
		if(sessionFlag){
			// close session, if session created in this method
			session.getTransaction().commit();
		}
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		//log.debug("CBBHelper : saveCBBDefaults Method End ");
		return cbbProcedureInfoPojo;
}
	
	public static CBB getCBBBySiteId(Long siteId){
		return getCBBBySiteId(siteId,null);
	}
	
	public static CBB getCBBBySiteId(Long siteId, Session session){
		//log.debug("getCBBBySiteId method start in CBBHelper");
		CBB cbb = new CBB();
		List<CBB> list = new ArrayList<CBB>();
		boolean sessionFlag = false;		
		try{
			if(session== null){
				// create new session
			  session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			cbb.setSiteId(siteId);
			String sql = "from CBB cbb where siteId = "+siteId;
			Query query = session.createQuery(sql);			
			//query.addEntity("cbb", CBB.class);
			list = (List<CBB>)query.list();
			if(list!=null && list.size()>0){
			cbb = list.get(0);		
			}
			//if(cbb.getSiteId()!=null)
			//cbb = (CBB)session.load(CBB.class,cbb.getSiteId());
		}catch (Exception e) {
			//log.debug("exception  in cbb helper --.>>"+e);
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return cbb;
	}
	
	public static SitePojo getSiteBySiteId(Long siteId){
		return getSiteBySiteId(siteId,null);
	}
	
	public static SitePojo getSiteBySiteId(Long siteId, Session session){
		//log.debug("getSiteBySiteId method start in CBBHelper");
		SitePojo sitePojo = new SitePojo();
		Site site = new Site();
		List<Site> list = new ArrayList<Site>();
		boolean sessionFlag = false;		
		try{
			if(session== null){
				// create new session
			  session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "from Site where siteId = "+siteId;
			Query query = session.createQuery(sql);
			//query.addEntity("cbb", CBB.class);
			list = (List<Site>)query.list();
			if(list!=null && list.size()>0){
				site = list.get(0);
				sitePojo = (SitePojo)ObjectTransfer.transferObjects(site, sitePojo);
			}			
		}catch (Exception e) {
			//log.debug("exception  in cbb helper --.>>"+e);
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return sitePojo;
	}
	
	public static SitePojo saveSite(SitePojo sitePojo){
		return saveSite(sitePojo,null);
	}
	
	public static SitePojo saveSite(SitePojo sitePojo, Session session){
	    	//log.debug("SitePojo with session");
			//log.debug("CBBHelper : SitePojo Start ");
			Site site = new Site();
			boolean sessionFlag = false;
			try{
			 // check session 
			if(session== null){
				// create new session
			  session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			site = (Site)ObjectTransfer.transferObjects(sitePojo, site);
			site = (Site)new VelosUtil().setAuditableInfo(site);
			if(site.getSiteId() !=null)
			{session.merge(site);
			}else{
				session.save(site);
			}
			sitePojo = (SitePojo)ObjectTransfer.transferObjects(site, sitePojo);
			if(sessionFlag){
				// close session, if session created in this method
				session.getTransaction().commit();
			}
			}catch(Exception e){
				log.error(e.getMessage());
				e.printStackTrace();
			}finally{
				if(session.isOpen()){
					session.close();
				}
			}
			
			return sitePojo;
	}
	
	public static List<CBBProcedurePojo> getCBBProcedures(Long id){
		return getCBBProcedures(id, null);
	}	
	
	public static List<CBBProcedurePojo> getCBBProcedures(Long id,Session session){
		//log.debug("getCBBMapWithIds method start in CBBHelper");
		////log.debug("Inside getCBBProcedure --.>>");
		List<CBBProcedurePojo> cbbPojos = new ArrayList<CBBProcedurePojo>();
		List<CBBProcedures> cbbs = new ArrayList<CBBProcedures>();
		CBBProcedurePojo cbbPojo = null;
		boolean sessionFlag = false;		
		try{
			if(session== null){
				// create new session
			  session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			StringBuffer sql = new StringBuffer(" from CBBProcedures cbb");
			if(id!=null){
				sql.append(" where pkProcId="+id);
			}
			cbbs = (List<CBBProcedures>) (session.createQuery(sql.toString()).list());
			
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		for(CBBProcedures c :cbbs){
			cbbPojo = new CBBProcedurePojo();
			cbbPojo = (CBBProcedurePojo)ObjectTransfer.transferObjects(c, cbbPojo);
			cbbPojos.add(cbbPojo);
		}
		return cbbPojos;
	}
	
	
	
	public static String checkForProcAssignment(String procId){
		return checkForProcAssignment(procId,null);
	}
	
	public static String checkForProcAssignment(String procId,Session session){
		String flag="";
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="select count(*) from cb_cord where FK_CBB_PROCEDURE="+procId;
			//log.debug("qry is::::::::::"+qry);
			List lst=session.createSQLQuery(qry).list();
			if(lst!=null && lst.size()>0){
				if(lst.get(0)!=null){
					flag=lst.get(0).toString();
					//log.debug("flag::::::::::::::::::::::::"+flag);
				}
			}
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return flag;
	}
	

	public static String getProcCordsMaxCollectionDt(String procId){
		return getProcCordsMaxCollectionDt(procId,null);
	}
	
	public static String getProcCordsMaxCollectionDt(String procId,Session session){
		String collDt ="";
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="select to_char(nvl(max(SPEC_COLLECTION_DATE),''),'Mon DD, YYYY') from er_specimen  where pk_specimen in ( select fk_specimen_id from cb_cord where FK_CBB_PROCEDURE ="+procId+" )";
			//log.debug("qry is::::::::::"+qry);
			List lst=session.createSQLQuery(qry).list();
			if(lst!=null && lst.size()>0){
				if(lst.get(0)!=null){
					collDt = lst.get(0).toString();
					////log.debug("collDt::::::::::::::::::::::::"+collDt);
				}
			}
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return collDt;
	}
	
	public static List<FundingGuidelinesPojo> getCBBGuidlinesByid(String siteId){
		return getCBBGuidlinesByid(siteId , null);
	}	
	
	public static List<FundingGuidelinesPojo> getCBBGuidlinesByid(String siteId,Session session){
		//log.debug("getCBBGuidlinesByid method start in CBBHelper");
		////log.debug("Inside getCBBProcedure --.>>");
		List<FundingGuidelinesPojo> fundingGuidelinesPojosLst = new ArrayList<FundingGuidelinesPojo>();
		List<FundingGuidelines> fundingGuidelinesLst = new ArrayList<FundingGuidelines>();
		FundingGuidelinesPojo fundingGuidelinesPojo = null;
		FundingGuidelines fundingGuidelines =null;
		boolean sessionFlag = false;		
		try{
			if(session== null){
				// create new session
			  session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			StringBuffer sql = new StringBuffer(" from FundingGuidelines fg");
			if(siteId!=null){
				sql.append(" where fkCbbId="+siteId);
			}
			fundingGuidelinesLst = (List<FundingGuidelines>) (session.createQuery(sql.toString()).list());
			
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		for(FundingGuidelines fg :fundingGuidelinesLst){
			fundingGuidelinesPojo = new FundingGuidelinesPojo();
			fundingGuidelinesPojo = (FundingGuidelinesPojo)ObjectTransfer.transferObjects(fg, fundingGuidelinesPojo);
			fundingGuidelinesPojosLst.add(fundingGuidelinesPojo);
		}
		return fundingGuidelinesPojosLst;
	}
	
	public static List<FundingGuidelinesPojo> updateCBBGuidlines(List<FundingGuidelinesPojo> fundingGuidelinesPojoLst){
		return updateCBBGuidlines(fundingGuidelinesPojoLst , null);
	}	
	
	public static List<FundingGuidelinesPojo> updateCBBGuidlines(List<FundingGuidelinesPojo> fundingGuidelinesPojoLst,Session session){
		//log.debug("getCBBGuidlinesByid method start in CBBHelper");
		////log.debug("Inside getCBBProcedure --.>>");
		List<FundingGuidelinesPojo> fundingGuidelinesPojosLst1 = new ArrayList<FundingGuidelinesPojo>();
		List<FundingGuidelines> fundingGuidelinesLst = new ArrayList<FundingGuidelines>();
		FundingGuidelinesPojo fundingGuidelinesPojo = null;
		FundingGuidelines fundingGuidelines =null;
		for(FundingGuidelinesPojo fGuidelinesPojo : fundingGuidelinesPojoLst){
			fundingGuidelines = new FundingGuidelines();
			fundingGuidelines = (FundingGuidelines)ObjectTransfer.transferObjects(fGuidelinesPojo, fundingGuidelines);
			fundingGuidelines = (FundingGuidelines) new VelosUtil().setAuditableInfo(fundingGuidelines);
			fundingGuidelinesLst.add(fundingGuidelines);
    	}
		boolean sessionFlag = false;		
		
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			
			for(FundingGuidelines fg : fundingGuidelinesLst){
				//log.debug("merge or save:::: fundingGuidelines object .......");
				if(fg.getPkFundingGuidelin()!=null){
					//log.debug("merging .........  ::::");
					session.merge(fg);
				}else{
					//log.debug("saving .............  ::::");
					session.save(fg);
				}
			}
			if(sessionFlag){
				session.getTransaction().commit();
			}
			
			if(fundingGuidelinesLst!=null && fundingGuidelinesLst.size()>0){
				//log.debug("fundingGuidelinesLst clearred .............");
				fundingGuidelinesPojoLst.clear();
				fundingGuidelinesPojosLst1=getCBBGuidlinesByid(fundingGuidelinesLst.get(0).getFkCbbId().toString());
				/*for(FundingGuidelines fg :fundingGuidelinesLst){
					fundingGuidelinesPojo = new FundingGuidelinesPojo();
					fundingGuidelinesPojo = (FundingGuidelinesPojo)ObjectTransfer.transferObjects(fg, fundingGuidelinesPojo);
					fundingGuidelinesPojosLst1.add(fundingGuidelinesPojo);
				}*/	
			}
			
    	} catch (Exception e) {
			//log.debug(" Exception in saveOrupdateCBBGuidlines " + e);
    		log.error(e.getMessage());
			e.printStackTrace();
		} finally {		
			 if(session.isOpen()){
			 session.close();			
			 }
		}
	
		
		return fundingGuidelinesPojosLst1;
	}
	
	public static List<FundingGuidelinesPojo> getValidfundingguidelinesdate(String birthDate,String Category,String cbbId){
		return getValidfundingguidelinesdate(birthDate,Category,cbbId,null);
	}	
	
	public static List<FundingGuidelinesPojo> getValidfundingguidelinesdate(String birthDate,String Category,String cbbId,Session session){
		//log.debug("getCBBGuidlinesByid method start in CBBHelper");
		////log.debug("Inside getCBBProcedure --.>>");
		List<FundingGuidelinesPojo> fundingGuidelinesPojosLst1 = new ArrayList<FundingGuidelinesPojo>();
		List<FundingGuidelines> fundingGuidelinesLst = new ArrayList<FundingGuidelines>();
		FundingGuidelinesPojo fundingGuidelinesPojo = null;
		FundingGuidelines fundingGuidelines =null;
		boolean sessionFlag = false;		
		
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			StringBuffer sql = new StringBuffer(" from FundingGuidelines fg");
			if((cbbId!=null||cbbId!="" )&&( Category!=null||Category!="") && birthDate!=null){
				sql.append(" where fkFundCateg='"+Category+"'and fundingBdate = TO_DATE('"+birthDate+"','Month DD, YYYY') and fkCbbId="+cbbId);
			}
			fundingGuidelinesLst = (List<FundingGuidelines>) (session.createQuery(sql.toString()).list());
			
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		for(FundingGuidelines fg :fundingGuidelinesLst){
			fundingGuidelinesPojo = new FundingGuidelinesPojo();
			fundingGuidelinesPojo = (FundingGuidelinesPojo)ObjectTransfer.transferObjects(fg, fundingGuidelinesPojo);
			fundingGuidelinesPojosLst1.add(fundingGuidelinesPojo);
		}
		return fundingGuidelinesPojosLst1;
	}
	
	public static List<SitePojo> getCBBListForSchAvail(){
		return getCBBListForSchAvail(null);
	}
	
    public static List<SitePojo> getCBBListForSchAvail(Session session){
    	//log.debug("getCBBListForSchAvail method start in CBBHelper");
    	////log.debug("Uservalue==========>>>>>>>> inside getsites");
    	List<Site> sites = new ArrayList<Site>();
    	List<SitePojo> sitesPojos = new ArrayList<SitePojo>();
    	ActionContext ac = ActionContext.getContext();
		HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
		ResourceBundle resource = ResourceBundle.getBundle(VelosMidConstants.EXCEPTION_PROPERTYFILE_NAME);
		HttpSession sessions = request.getSession(true);
		
    	Site site = new Site();
    	SitePojo sitePojo = null;
    	boolean sessionFlag = false;
    	
		try{
			
			if(sessions != null){
				userObject=(UserJB)sessions.getAttribute(MidErrorConstants.GARUDA_USER);
			}		
			 ////log.debug("Uservalue==========>>>>>>>> inside getsites"+((Integer)userObject.getUserId()).longValue());
			 
			if(session== null){
				
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "from Site where siteId in(select distinct fkCbbId from FundingGuidelines where fkCbbId in(select fkSite From UserSite where fkUser="+((Integer)userObject.getUserId()).longValue()+" and userRight <> 0 ))";
			sites = session.createQuery(sql).list();
			for(Site s : sites){
				sitePojo = new SitePojo();
				sitePojo = (SitePojo)ObjectTransfer.transferObjects(s,sitePojo);
				sitesPojos.add(sitePojo);
			}
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return sitesPojos;
	}
    
    public static List<FundingSchedulePojo> getScheduledFundingRepLst(List<FundingSchedulePojo> fundingSchedulePojoLst,PaginateSearch paginateSearch,int i){
		return getScheduledFundingRepLst(fundingSchedulePojoLst,paginateSearch,i,null);
	}	
	
	public static List<FundingSchedulePojo> getScheduledFundingRepLst(List<FundingSchedulePojo> fundingSchedulePojoLst,PaginateSearch paginateSearch,int i , Session session){
		//log.debug("getCBBGuidlinesByid method start in CBBHelper");
		////log.debug("Inside getCBBProcedure --.>>");
		List<FundingSchedulePojo> fundingSchedulePojosLst = new ArrayList<FundingSchedulePojo>();
		List<FundingSchedule> fundingScheduleLst = new ArrayList<FundingSchedule>();
		FundingSchedulePojo fundingSchedulePojo = null;
		FundingSchedule fundingSchedule =null;
		if(fundingSchedulePojoLst!=null && fundingSchedulePojoLst.size()>0){
		for(FundingSchedulePojo fSchedulePojo : fundingSchedulePojoLst){
			fundingSchedule = new FundingSchedule();
			fundingSchedule = (FundingSchedule)ObjectTransfer.transferObjects(fSchedulePojo, fundingSchedule);
			fundingSchedule = (FundingSchedule) new VelosUtil().setAuditableInfo(fundingSchedule);
			fundingScheduleLst.add(fundingSchedule);
    	}}
		boolean sessionFlag = false;		
		
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if(fundingSchedulePojoLst!=null && fundingSchedulePojoLst.size()>0){
			for(FundingSchedule fg : fundingScheduleLst){
				//log.debug("merge or save:::: fundingSchedule object .......");
				if(fg.getPkFundingSchedule()!=null){
					//log.debug("merging .........  ::::");
					session.merge(fg);
				}else{
					//log.debug("saving .............  ::::");
					session.save(fg);
				}
			}
			if(sessionFlag){
				session.getTransaction().commit();
			}
			fundingScheduleLst.clear();
			//log.debug("fundingScheduleLst clearred .............");
			}
			session=null;
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "from FundingSchedule fs where fkCbbId in(select fkSite From UserSite where fkUser="+((Integer)userObject.getUserId()).longValue()+" and userRight <> 0 )";
			Query query = session.createQuery(sql);
			if(paginateSearch!=null && i==1){
				query.setFirstResult(paginateSearch.getiPageNo());
				query.setMaxResults(paginateSearch.getiShowRows());
			}
			fundingScheduleLst = query.list();
			if(fundingScheduleLst!=null && fundingScheduleLst.size()>0){
				for(FundingSchedule fg :fundingScheduleLst){
					fundingSchedulePojo = new FundingSchedulePojo();
					fundingSchedulePojo = (FundingSchedulePojo)ObjectTransfer.transferObjects(fg, fundingSchedulePojo);
					fundingSchedulePojosLst.add(fundingSchedulePojo);
				}
			}
			
    	} catch (Exception e) {
			//log.debug(" Exception in getScheduledFundingRepLst " + e);
			e.printStackTrace();
			log.error(e.getMessage());
		} finally {		
			 if(session.isOpen()){
			 session.close();			
			 }
		}
		return fundingSchedulePojosLst;
	}
	
	public static List<FundingSchedulePojo> getfundingSchedule(Long scheduledFundingId){
		return getfundingSchedule(scheduledFundingId,null);
	}	
	
	public static List<FundingSchedulePojo> getfundingSchedule(Long scheduledFundingId , Session session){
		//log.debug("getfundingSchedule method start in CBBHelper");
		////log.debug("Inside getCBBProcedure --.>>");
		List<FundingSchedulePojo> fundingSchedulePojosLst = new ArrayList<FundingSchedulePojo>();
		List<FundingSchedule> fundingScheduleLst = new ArrayList<FundingSchedule>();
		FundingSchedulePojo fundingSchedulePojo = null;
		FundingSchedule fundingSchedule =null;
		
		boolean sessionFlag = false;		
		
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "from FundingSchedule fs where pkFundingSchedule="+scheduledFundingId;
			fundingScheduleLst = session.createQuery(sql).list();
			if(fundingScheduleLst!=null && fundingScheduleLst.size()>0){
				for(FundingSchedule fg :fundingScheduleLst){
					fundingSchedulePojo = new FundingSchedulePojo();
					fundingSchedulePojo = (FundingSchedulePojo)ObjectTransfer.transferObjects(fg, fundingSchedulePojo);
					fundingSchedulePojosLst.add(fundingSchedulePojo);
				}
			}
			
    	} catch (Exception e) {
			//log.debug(" Exception in getfundingSchedule " + e);
			e.printStackTrace();
			log.error(e.getMessage());
		} finally {		
			 if(session.isOpen()){
			 session.close();			
			 }
		}
		return fundingSchedulePojosLst;
	}
	
	public static String getSiteInfoBySiteId(String siteColName,Long siteID){
		return getSiteInfoBySiteId(siteColName,siteID,null);
	}
	
	public static String getSiteInfoBySiteId(String siteColName,Long siteID,Session session){
		String result="";
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="select "+siteColName+" from ER_SITE where PK_SITE="+siteID;
			//log.debug("qry is::::::::::"+qry);
			List lst=session.createSQLQuery(qry).list();
			if(lst!=null && lst.size()>0){
				if(lst.get(0)!=null){
					result=lst.get(0).toString();
					//log.debug("result::::::::::::::::::::::::"+result);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			log.error(e.getMessage());
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return result;
	}
	
	public static String checkForProcName(String procName, String cbbId){
		return checkForProcName(procName,cbbId,null);
	}
	
	public static String checkForProcName(String procName,String cbbId,Session session){
		String procNameFlag="";
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="select count(*) from CBB_PROCESSING_PROCEDURES where PROC_NAME='"+procName+"' and fk_site ='"+cbbId+"'";
			//log.debug("query in checkForrocName is::::"+qry);
			List lst=session.createSQLQuery(qry).list();
			if(lst!=null && lst.size()>0){
				if(lst.get(0)!=null){
					procNameFlag=lst.get(0).toString();
					//log.debug("result::::::::::::::::::::::::"+procNameFlag);
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
			//log.debug("exception in checkForProcName========>"+e.getMessage());
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return procNameFlag;
	}
	
	public static String  getCbbType(String Siteid)
	{
		return getCbbType(Siteid, null);
	}
	
	public static String  getCbbType(String Siteid,Session session)
	{
		List<String> lst=new ArrayList<String>();
		String resultCbbType = ""; 
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="";
			qry="select codelst_desc from er_codelst where codelst_type = 'site_type' "+
"and  pk_codelst ="+Siteid;
			//log.debug("codelstcustcols qry is:::::"+qry);
			lst=(List<String>)session.createSQLQuery(qry).list();
			//log.debug("codelstcustcols list length is:::"+lst.size());
			//log.debug("codelstcustcols list get(0) :::"+lst.get(0));
			resultCbbType=lst.get(0).toString();
		}catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return resultCbbType;
	}
	
	public static SitePojo getSiteBySiteName(String siteName,UserJB userObject){
		return getSiteBySiteName(siteName,null,userObject);
	}
	
	public static SitePojo getSiteBySiteName(String siteName, Session session,UserJB userObject){
		//log.debug("getSiteBySiteId method start in CBBHelper");
		SitePojo sitePojo = new SitePojo();
		Site site = new Site();
		List<Site> list = new ArrayList<Site>();
		boolean sessionFlag = false;		
		try{
			if(session== null){
				// create new session
			  session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "from Site where upper(siteIdentifier) = '"+siteName.toUpperCase()+"' and siteId in (select fkSite From UserSite where fkUser="+((Integer)userObject.getUserId()).longValue()+" and userRight <> 0) and siteCodelstType in (select pkCodeId from CodeList where lower(type) ='site_type' and lower(subType)='cbb')";
			Query query = session.createQuery(sql);
			//query.addEntity("cbb", CBB.class);
			list = (List<Site>)query.list();
			if(list!=null && list.size()>0){
				site = list.get(0);
				sitePojo = (SitePojo)ObjectTransfer.transferObjects(site, sitePojo);
			}			
		}catch (Exception e) {
			//log.debug("exception  in cbb helper --.>>"+e);
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return sitePojo;
	}
	
	
	public static List<SitePojo> getMultipleValuesByErSite(Long entityId,
			String entityType, String entitySubType,long isActive) {
		return getMultipleValuesByErSite(entityId, entityType, entitySubType,
				isActive, null);
	}
	
	public static List<SitePojo> getMultipleValuesByErSite(Long entityId,
			String entityType, String entitySubType,long isActive, Session session) {
		List<SitePojo> valuesPojos = new ArrayList<SitePojo>();
		SitePojo sitePojo = null;
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;

				session.beginTransaction();
			}

			String sql = "from Site er";
			if (entityType != null && entitySubType != null) {
				sql += " where er.siteCodelstType in(select pkCodeId from CodeList where type ='"
						+ entityType
						+ "' and subType ='"
						+ entitySubType
						+ "')"
						+" and er.siteHidden ="+isActive;
			}
			if (entityId != null) {
				sql += "and er.siteParent=" + entityId;
			}
						
			Query query = session.createQuery(sql);

			List<Site> list = (List<Site>) query.list();
			Site sitObj = null;

			for (Site e : list) {
				sitePojo = new SitePojo();
				sitePojo = (SitePojo) ObjectTransfer.transferObjects(e,
						sitePojo);
				valuesPojos.add(sitePojo);
			}

		} catch (Exception e) {
			//log.debug("exception  in cbu helper --.>>" + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return valuesPojos;
	}

	public static List<SitePojo> getMultipleValuesByErSites(Long entityId,
			String entityType, String entitySubType) {
		return getMultipleValuesByErSites(entityId, entityType, entitySubType,
				 null);
	}
	
	
	public static List<SitePojo> getMultipleValuesByErSites(Long entityId,
			String entityType, String entitySubType, Session session) {
		List<SitePojo> valuesPojos = new ArrayList<SitePojo>();
		SitePojo sitePojo = null;
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;

				session.beginTransaction();
			}

			String sql = "from Site er";
			if (entityType != null && entitySubType != null) {
				sql += " where er.siteCodelstType in(select pkCodeId from CodeList where type ='"
						+ entityType
						+ "' and subType ='"
						+ entitySubType
						+ "')"
						+" and (er.siteHidden = 1 or er.siteHidden = 0)";
			}
			if (entityId != null) {
				sql += "and er.siteParent=" + entityId;
			}
						
			Query query = session.createQuery(sql);

			List<Site> list = (List<Site>) query.list();
			Site sitObj = null;

			for (Site e : list) {
				sitePojo = new SitePojo();
				sitePojo = (SitePojo) ObjectTransfer.transferObjects(e,
						sitePojo);
				valuesPojos.add(sitePojo);
			}

		} catch (Exception e) {
			//log.debug("exception  in cbu helper --.>>" + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return valuesPojos;
	}

	public static List<SitePojo> isSiteExist(String siteName) {
		return isSiteExist(siteName,null);
	}
	
	
	public static List<SitePojo> isSiteExist (String siteName, Session session) {
		List<SitePojo> valuesPojos = new ArrayList<SitePojo>();
		SitePojo sitePojo = null;
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;

				session.beginTransaction();
			}

			String sql = "From Site where siteName ='"+siteName+"'";
			
						
			Query query = session.createQuery(sql);

			List<Site> list = (List<Site>) query.list();
			Site sitObj = null;

			for (Site e : list) {
				sitePojo = new SitePojo();
				sitePojo = (SitePojo) ObjectTransfer.transferObjects(e,
						sitePojo);
				valuesPojos.add(sitePojo);
			}

		} catch (Exception e) {
			//log.debug("exception  in cbu helper --.>>" + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return valuesPojos;
	}
	
	public static List<SitePojo> isCbbIdExist(String cbbId) {
		return isCbbIdExist(cbbId,null);
	}
		
	public static List<SitePojo> isCbbIdExist (String cbbId, Session session) {
		List<SitePojo> valuesPojos = new ArrayList<SitePojo>();
		SitePojo sitePojo = null;
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;

				session.beginTransaction();
			}

			String sql = "From Site where siteIdentifier ='"+cbbId+"'";
			
						
			Query query = session.createQuery(sql);

			List<Site> list = (List<Site>) query.list();
			Site sitObj = null;

			for (Site e : list) {
				sitePojo = new SitePojo();
				sitePojo = (SitePojo) ObjectTransfer.transferObjects(e,
						sitePojo);
				valuesPojos.add(sitePojo);
			}

		} catch (Exception e) {
		//	System.out.println("exception  in cbu helper --.>>" + e);
			log.error("Exception in CBUHelper : getMultipleValuesByEntityId() :: "
					+ e);
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return valuesPojos;
	}
	
	
	public static String getCbbInfoBySiteId(String CBBColName,Long siteId){
		return getCbbInfoBySiteId(CBBColName,siteId,null);
	}
	
	public static String getCbbInfoBySiteId(String CBBColName,Long siteId,Session session){
		String result="";
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="select "+CBBColName+" from CBB c,ER_SITE s where c.FK_SITE=s.PK_SITE and FK_SITE="+siteId;
			//System.out.println("qry in getCbbInfoBySiteId is::::::::::"+qry);
			List lst=session.createSQLQuery(qry).list();
			if(lst!=null && lst.size()>0){
				if(lst.get(0)!=null){
					result=lst.get(0).toString();
				//	System.out.println("result in getCbbInfoBySiteId::::::::::::::::::::::::"+result);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return result;
	}

	public static String getSiteName(Long siteId){
		return getSiteName(siteId,null);
	}
	
	public static String getSiteName(Long siteId,Session session){
		String result="";
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="select SITE_NAME from er_site where pk_site="+siteId;
			//System.out.println("qry in getCbbInfoBySiteId is::::::::::"+qry);
			List lst=session.createSQLQuery(qry).list();
			if(lst!=null && lst.size()>0){
				if(lst.get(0)!=null){
					result=lst.get(0).toString();
				//	System.out.println("result in getCbbInfoBySiteId::::::::::::::::::::::::"+result);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return result;
	}

	
	public static String getSiteID(Long siteId){
		return getSiteID(siteId,null);
	}
	
	public static String getSiteID(Long siteId,Session session){
		String result="";
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="select SITE_ID from er_site where pk_site="+siteId;
			//System.out.println("qry in getCbbInfoBySiteId is::::::::::"+qry);
			List lst=session.createSQLQuery(qry).list();
			if(lst!=null && lst.size()>0){
				if(lst.get(0)!=null){
					result=lst.get(0).toString();
				//	System.out.println("result in getCbbInfoBySiteId::::::::::::::::::::::::"+result);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		
		return result;
	}

	

}
