package com.velos.ordercomponent.business.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="CB_RECEIPANT_INFO")
@SequenceGenerator(name="SEQ_CB_RECEIPANT_INFO",sequenceName="SEQ_CB_RECEIPANT_INFO",allocationSize=1)
public class RecipientInfo extends Auditable{
	
	
	private Long pkRecipient;
	private String recipientId;
	private Long fkPerson;
	private String currDiagnosis;
	private Long fkTransCenterId;
	private String diseaseStage;
	private Long receipientWgt;
	private Long fkSecTransCenterId;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_RECEIPANT_INFO")
	@Column(name="PK_RECEIPANT")
	public Long getPkRecipient() {
		return pkRecipient;
	}
	public void setPkRecipient(Long pkRecipient) {
		this.pkRecipient = pkRecipient;
	}
	
	@Column(name="RECEIPANT_ID")
	public String getRecipientId() {
		return recipientId;
	}
	public void setRecipientId(String recipientId) {
		this.recipientId = recipientId;
	}
	
	@Column(name="FK_PERSON_ID")
	public Long getFkPerson() {
		return fkPerson;
	}
	public void setFkPerson(Long fkPerson) {
		this.fkPerson = fkPerson;
	}
	
	@Column(name="CURRENT_DIAGNOSIS")
	public String getCurrDiagnosis() {
		return currDiagnosis;
	}
	public void setCurrDiagnosis(String currDiagnosis) {
		this.currDiagnosis = currDiagnosis;
	}
	
	@Column(name="TRANS_CENTER_ID")
	public Long getFkTransCenterId() {
		return fkTransCenterId;
	}
	public void setFkTransCenterId(Long fkTransCenterId) {
		this.fkTransCenterId = fkTransCenterId;
	}
	
	@Column(name="DISEASE_STAGE")
	public String getDiseaseStage() {
		return diseaseStage;
	}
	public void setDiseaseStage(String diseaseStage) {
		this.diseaseStage = diseaseStage;
	}
	
	@Column(name="REC_WEIGHT")
	public Long getReceipientWgt() {
		return receipientWgt;
	}
	public void setReceipientWgt(Long receipientWgt) {
		this.receipientWgt = receipientWgt;
	}
	
	@Column(name="SEC_TRANS_CENTER_ID")
	public Long getFkSecTransCenterId() {
		return fkSecTransCenterId;
	}
	public void setFkSecTransCenterId(Long fkSecTransCenterId) {
		this.fkSecTransCenterId = fkSecTransCenterId;
	}
}
