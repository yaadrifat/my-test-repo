package com.velos.ordercomponent.business.xmlpojos;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class severe_aut_imm_sys_disordr {
	private String severe_aut_imm_sys_disordr_ind;
	private List<Relations> Relations;
	
	
	@XmlElement(name="severe_aut_imm_sys_disordr_ind")
	public String getSevere_aut_imm_sys_disordr_ind() {
		return severe_aut_imm_sys_disordr_ind;
	}
	public void setSevere_aut_imm_sys_disordr_ind(
			String severe_aut_imm_sys_disordr_ind) {
		this.severe_aut_imm_sys_disordr_ind = severe_aut_imm_sys_disordr_ind;
	}
	
	@XmlElement(name="Relations")
	public List<Relations> getRelations() {
		return Relations;
	}
	public void setRelations(List<Relations> relations) {
		Relations = relations;
	}
}
