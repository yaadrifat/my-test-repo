package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author Idrush Basha Syed
 *
 */
@Entity
@Table(name="CB_DCMS_LOG")
@SequenceGenerator(sequenceName="SEQ_CB_DCMS_LOG",name="SEQ_CB_DCMS_LOG",allocationSize=1)
public class DcmsLog extends Auditable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long dcmsLogId;
	private Long userId;
	private Long attachmentId;
	private String task;
	private Date taskDate;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_DCMS_LOG")
	@Column(name="PK_DCMS_LOG")
	public Long getDcmsLogId() {
		return dcmsLogId;
	}
	public void setDcmsLogId(Long dcmsLogId) {
		this.dcmsLogId = dcmsLogId;
	}
	@Column(name="USER_ID")
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	@Column(name="ATTACHMENT_ID")
	public Long getAttachmentId() {
		return attachmentId;
	}
	public void setAttachmentId(Long attachmentId) {
		this.attachmentId = attachmentId;
	}
	@Column(name="ACTION")
	public String getTask() {
		return task;
	}
	public void setTask(String task) {
		this.task = task;
	}
	@Column(name="ACTION_DATE")
	public Date getTaskDate() {
		return taskDate;
	}
	public void setTaskDate(Date taskDate) {
		this.taskDate = taskDate;
	}
	
	
	
	

}