package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


public class Identifiers {
	private CordIDs CordIDs;
    private MomIDS MomIDS;
    private AdditionalIds AdditionalIds;
	private String IdOnBag;
	
	
	@XmlElement(name="IdOnBag")
	public String getIdOnBag() {
		return IdOnBag;
	}

	public void setIdOnBag(String idOnBag) {
		IdOnBag = idOnBag;
	}

	@XmlElement(name="AdditionalIds")
	public AdditionalIds getAdditionalIds() {
		return AdditionalIds;
	}

	public void setAdditionalIds(AdditionalIds additionalIds) {
		AdditionalIds = additionalIds;
	}

	@XmlElement(name="MomIDS")	
	public MomIDS getMomIDS() {
		return MomIDS;
	}

	public void setMomIDS(MomIDS momIDS) {
		MomIDS = momIDS;
	}

	@XmlElement(name="CordIDS")
	public CordIDs getCordIDs() {
		return CordIDs;
	}
	
	public void setCordIDs(CordIDs CordIDs) {
		this.CordIDs = CordIDs;
	} 
  
}
