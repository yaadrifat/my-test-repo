package com.velos.ordercomponent.helper;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.classic.Session;

import com.velos.ordercomponent.business.pojoobjects.BestHLAPojo;
import com.velos.ordercomponent.business.pojoobjects.CordFilterPojo;
import com.velos.ordercomponent.business.pojoobjects.HLAFilterPojo;
import com.velos.ordercomponent.business.pojoobjects.HLAPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.util.HibernateUtil;
import com.velos.ordercomponent.business.util.VDAHibernateUtil;
import com.velos.ordercomponent.report.AntigenBestHla;
import com.velos.ordercomponent.report.AntigenHLA;
import com.velos.ordercomponent.report.CordHla;
import com.velos.ordercomponent.report.CordID;
import com.velos.ordercomponent.report.EntityMap;
import com.velos.ordercomponent.report.EntityMapWithList;
import com.velos.ordercomponent.report.LicEliMap;
import com.velos.ordercomponent.report.ObjectsArray;
import com.velos.ordercomponent.report.RecordCounter;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class VDAHelper {

	public static final Log log=LogFactory.getLog(VDAHelper.class);
	
	public static List<Object> getQueryBuilderViewData(String sql,PaginateSearch paginateSearch){
		return getQueryBuilderViewData(sql, paginateSearch, null);
	}
	
   public static List<Object> getQueryBuilderViewData(String sql,PaginateSearch paginateSearch,Session session){
	   boolean sessionFlag = false;
		List<Object> returnList = null;

		try {
			// check session
			if (session == null) {
				// create new session
				session = VDAHibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			Query query = session.createSQLQuery(sql);
			if(paginateSearch!=null){
				query.setFirstResult(paginateSearch.getiPageNo());
				query.setMaxResults(paginateSearch.getiShowRows());
			}
			System.out.println("trace query for lookup& advance lookup : \n\t\t"+ query);
			System.out.println();
			long startTime=System.currentTimeMillis();
			returnList = (List<Object>) query.list();
			long endTime=System.currentTimeMillis();
			System.out.println();
			System.out.println((endTime-startTime)/1000+" : "+(endTime-startTime)%1000+" SS:MM (Second\\Milisecond)\n\n");

						
			
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}

		return returnList;
	}
   
   public static Map getQueryBuilderRecordDetails(String sql){
		return getQueryBuilderRecordDetails(sql,null);
	}
   
   public static Map<String, Object> getQueryBuilderRecordDetails(String sql,Session session){
	   Map<String, Object> returnMap = null;

		try {
			
			if (session == null) {
				session = VDAHibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}		
			
			RecordCounter rc = new RecordCounter( sql );
			System.out.println("trace query for lookup& advance lookup : \n\t\t"+ sql);
			System.out.println();			
	        session.doWork( rc );			
	        returnMap = rc.getCounterDetails();	
	        			
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}

		return returnMap;
	}
   
   
   
	public static Map getQueryBuilderHlaData(String sql){
		return getQueryBuilderHlaData(sql,null);
	}
   
   public static Map<Long, Map<Long,List<HLAPojo>>> getQueryBuilderHlaData(String sql,Session session){
	   Map<Long, Map<Long,List<HLAPojo>>> returnMap = null;

		try {
			
			if (session == null) {
				session = VDAHibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}		
			
			AntigenHLA antHla = new AntigenHLA(sql, null);
			System.out.println("trace query for lookup& advance lookup : \n\t\t"+ sql);
			System.out.println();			
	        session.doWork( antHla );			
	        returnMap = antHla.getCbuHlas();	
	        			
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}

		return returnMap;
	}
   
   
   
   public static Map getQueryBuilderHlaFilterData(String sql, Map<Long, String> cdlst){
		return getQueryBuilderHlaFilterData(sql, cdlst, null);
	}
  
  public static Map<Long, Map<Long, HLAFilterPojo>> getQueryBuilderHlaFilterData(String sql,  Map<Long, String> cdlst, Session session){
	  Map<Long, Map<Long, HLAFilterPojo>> returnMap = null;

		try {
			
			if (session == null) {
				session = VDAHibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}		
			
			AntigenHLA antHla = new AntigenHLA(sql, cdlst);
			System.out.println("trace query for lookup& advance lookup : \n\t\t"+ sql);
			System.out.println();			
	        session.doWork( antHla );			
	        returnMap = antHla.getCbuHla();	
	        			
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}

		return returnMap;
	}
   
   public static Map getQueryBuilderBestHlaData(String sql){
		//return getQueryBuilderViewData(sql, paginateSearch, null);
		return getQueryBuilderBestHlaData(sql,null);
	}
  
  public static Map<Long, List<BestHLAPojo>> getQueryBuilderBestHlaData(String sql,Session session){
	  Map<Long, List<BestHLAPojo>> returnMap = null;

		try {
			
			if (session == null) {
				session = VDAHibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}		
			
			AntigenBestHla antBHla = new AntigenBestHla(sql, null);
			System.out.println("trace query for lookup& advance lookup : \n\t\t"+ sql);
			System.out.println();			
	        session.doWork( antBHla );			
	        returnMap = antBHla.getBestHlasMap();	
	        			
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}

		return returnMap;
	}
	
 public static Map getQueryBuilderBestHlaFilterData(String sql,  Map<Long, String> cdlst){
		//return getQueryBuilderViewData(sql, paginateSearch, null);
		return getQueryBuilderBestHlaFilterData(sql, cdlst, null);
	}
  
  public static Map<Long, HLAFilterPojo> getQueryBuilderBestHlaFilterData(String sql, Map<Long, String> cdlst, Session session){
	  Map<Long, HLAFilterPojo> returnMap = null;

		try {
			
			if (session == null) {
				session = VDAHibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}		
			
			AntigenBestHla antBHla = new AntigenBestHla(sql, cdlst);
			System.out.println("trace query for lookup& advance lookup : \n\t\t"+ sql);
			System.out.println();			
	        session.doWork( antBHla );			
	        returnMap = antBHla.getBestHlas();	
	        			
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}

		return returnMap;
	}
   
  public static List getQueryBuilderListObject(String sql){
		//return getQueryBuilderViewData(sql, paginateSearch, null);
		return getQueryBuilderListObject(sql, null);
	}

public static List<Object> getQueryBuilderListObject(String sql, Session session){
	List<Object> returnList = null;

		try {
			
			if (session == null) {
				session = VDAHibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}		
			
			ObjectsArray obj = new ObjectsArray(sql);
			System.out.println("trace query for lookup& advance lookup : \n\t\t"+ sql);
			System.out.println();			
	        session.doWork( obj );			
	        returnList =  obj.getObjLst();	
	        			
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}

		return returnList;
	}


	public static List getQueryBuilderCordIdObject(String sql){
		//return getQueryBuilderViewData(sql, paginateSearch, null);
		return getQueryBuilderCordIdObject(sql, null);
	}
	
	private static List<CordFilterPojo> getQueryBuilderCordIdObject(String sql, Session session){
		List<CordFilterPojo> returnList= null;
	
		try {
			
			if (session == null) {
				session = VDAHibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}		
			
			CordID obj = new CordID(sql);
			System.out.println("trace query for lookup& advance lookup : \n\t\t"+ sql);
			System.out.println();			
	        session.doWork( obj );			
	        returnList =  obj.getCordIds();	
	        			
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
	
		return returnList;
	}
	
public static List getQueryBuilderCordHlaData(String sql){
		//return getQueryBuilderViewData(sql, paginateSearch, null);
		return getQueryBuilderCordHlaData(sql, null);
	}

public static List<CordFilterPojo> getQueryBuilderCordHlaData(String sql, Session session){
List<CordFilterPojo> returnList = null;

	try {
		
		if (session == null) {
			session = VDAHibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
		}		
		
		CordHla ch = new CordHla(sql);
		System.out.println("trace query for lookup& advance lookup : \n\t\t"+ sql);
		System.out.println();			
        session.doWork( ch );			
        returnList =  ch.getCord();	
        			
	} catch (Exception e) {
		log.error(e.getMessage());
		e.printStackTrace();
	} finally {
		if (session.isOpen()) {
			session.close();
		}
	}

	return returnList;
}

public static Map getQueryBuilderMapObject(String sql){
	//return getQueryBuilderViewData(sql, paginateSearch, null);
	return getQueryBuilderMapObject(sql, null);
}

public static Map getQueryBuilderMapObject(String sql, Session session){
	Map returnList = null;

try {
	
	if (session == null) {
		session = VDAHibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
	}		
	
	EntityMap ch = new EntityMap(sql);
	System.out.println("trace query for lookup& advance lookup : \n\t\t"+ sql);
	System.out.println();			
    session.doWork( ch );			
    returnList =  ch.getEntityMap();	
    			
} catch (Exception e) {
	log.error(e.getMessage());
	e.printStackTrace();
} finally {
	if (session.isOpen()) {
		session.close();
	}
}

return returnList;
}

public static Map getLicEligMapObject(String sql, Map<Long, Object []> cord, Map<Long, ArrayList<Long>> reason){
	
	return getLicEligMapObject( sql, cord, reason, null);
}

public static Map getLicEligMapObject(String sql, Map<Long, Object []> cord, Map<Long, ArrayList<Long>> reason, Session session){
	Map returnList = null;

try {
	
	if (session == null) {
		session = VDAHibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
	}		
	
	LicEliMap lem = new LicEliMap(sql, cord, reason);
	System.out.println("trace query for lookup& advance lookup : \n\t\t"+ sql);
	System.out.println();			
    session.doWork( lem );			
    returnList =  lem.getEntityMap();	
    			
} catch (Exception e) {
	log.error(e.getMessage());
	e.printStackTrace();
} finally {
	if (session.isOpen()) {
		session.close();
	}
}

return returnList;
}

public static Map getQueryBuilderMapLstObject(String sql){
	
	return getQueryBuilderMapLstObject( sql, null);
}

public static Map getQueryBuilderMapLstObject(String sql, Session session){
	Map returnList = null;

try {
	
	if (session == null) {
		session = VDAHibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
	}		
	
	EntityMapWithList leml = new EntityMapWithList(sql);
	System.out.println("trace query for lookup& advance lookup : \n\t\t"+ sql);
	System.out.println();			
    session.doWork( leml );			
    returnList =  leml.getEntityMap();	
    			
} catch (Exception e) {
	log.error(e.getMessage());
	e.printStackTrace();
} finally {
	if (session.isOpen()) {
		session.close();
	}
}

return returnList;
}

   public static List getQueryBuilderViewData(String sql){
		return getQueryBuilderExcelData(sql,null);
	}
   
   public static List getQueryBuilderExcelData(String sql,Session session){
	   boolean sessionFlag = false;
		
	   List<Object> returnList = null;

		try {
			if (session == null) {
				// create new session
				session = VDAHibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}		
			
			
			 SQLQuery query = session.createSQLQuery(sql.toString());	
			 
			 System.out.println("trace query for lookup& advance lookup : \n\t\t"+ query);
			 System.out.println();
			 long startTime=System.currentTimeMillis();
				
	         query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
	      
	         returnList = query.list();
	         
	            long endTime=System.currentTimeMillis();
				System.out.println();
				System.out.println((endTime-startTime)/1000+" : "+(endTime-startTime)%1000+" SS:MM (Second\\Milisecond)\n\n");
	        


		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}

		return returnList;
	}
}
