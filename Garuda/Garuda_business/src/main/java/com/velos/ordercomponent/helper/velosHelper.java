package com.velos.ordercomponent.helper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.classic.Session;

import com.velos.ordercomponent.business.domain.CodeList;
import com.velos.ordercomponent.business.pojoobjects.ClinicalNotePojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.pojoobjects.PendingOrdersData;
import com.velos.ordercomponent.business.util.HibernateUtil;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.VelosMidConstants;


public class velosHelper{
	public static final Log log=LogFactory.getLog(velosHelper.class);
	public static List<Object> getObjectList(String objectName, String criteria) {
		return getObjectList(objectName, criteria, null);
	}

	public static List<Object> getObjectList(String objectName,
			String criteria, Session session) {
		//log.debug("----------- Velos Helper");
		String query = null;
		boolean sessionFlag = false;
		List<Object> returnList = null;
		try {
			// check session
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				//log.debug("----------- session"+session);
				session.beginTransaction();
			}
			if(objectName.equals("Address"))
			{
			 query = " from " + objectName;
					/*+ " where (deletedFlag = 0 or deletedFlag is null)";*/
			if (criteria != null && !criteria.trim().equals("")) {
				query +=" where " + criteria;
			}
			}
			else if(objectName.equals("CodeList"))
			{
			 query = " from " + objectName;
					/*+ " where (deletedFlag = 0 or deletedFlag is null)";*/
			if (criteria != null && !criteria.trim().equals("")) {
				query +=" where " + criteria;
			}
			}
			else if(objectName.equals("AccountForms"))
			{
			 query = " from " + objectName;
					/*+ " where (deletedFlag = 0 or deletedFlag is null)";*/
			if (criteria != null && !criteria.trim().equals("")) {
				query +=" where " + criteria;
			}
			}
			else
			{
			 query = " from " + objectName
				+ " where (deletedFlag = 0 or deletedFlag is null)";
			 if (criteria != null && !criteria.trim().equals("")) {
				 	query +=" and " + criteria;
			 	}
			}
			//log.debug(" trace query " + query);
			returnList = (List<Object>) session.createQuery(query).list();
//			//log.debug("list size is--------"+returnList.size());
//			Iterator it=returnList.iterator();
//			while (it.hasNext()) {
//				//log.debug("listed values areEEEEEEEEEEEEEEEE--"+it.next());
//				
//			}

//			if (sessionFlag) {
//				 close session, if session created in this method
//				 session.getTransaction().commit();
//			}

		} catch (Exception e) {
			//log.debug(" Exception in getObjectList " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}

		return returnList;
	}
	
	public static List<Object> getObjectList(String query){
		List<Object> list=new ArrayList<Object>();
		Session session=null;
		try{
		if (session == null) {
			// create new session
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
		}
		list=(List<Object>) session.createQuery(query).list();
		
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return list;
	}
	
	public static Object getObject(String objectName,String id,String fieldName,int i)throws Exception{
		//log.debug("VelosHelper : getObject Method ");
		return getObject(objectName,id,fieldName,null);
		
	}
	
	public static Object getObject(String objectName,Long id,String fieldName)throws Exception{
		//log.debug("VelosHelper : getObject Method ");
		return getObject(objectName,id,fieldName,null);
		
	}
	
	public static Object getObject(String objectName,Long id,String fieldName,Session session)throws Exception{
		//log.debug("VelosHelper : getObject Method Start ");
		Object returnObject = new Object();
		try{
			
				String criteria =   fieldName + " = " + id;
				List<Object> objList = getObjectList(objectName,criteria,session);
				
				if(objList!=null && objList.size() > 0){
					returnObject = (Object)objList.get(0);
				}
			
		}catch(Exception e){
			//log.error("Exception in VelosHelper : getObject() :: " + e);
			log.error(e.getMessage());
			e.printStackTrace();
			//throw new GarudaBaseException();
		}
		//log.debug("VelosHelper : getObject Method End ");
		return returnObject;
	}
	
	public static Object getObject(String objectName,String id,String fieldName){
		return getObject(objectName,id,fieldName,null);
		
	}
	
	public static Object getObject(String objectName,String id,String fieldName,Session session){
		Object returnObject = new Object();
		try{
				
				String criteria =  fieldName + " = '" + id+"'";
				List<Object> objList = getObjectList(objectName,criteria,session);
				
				if(objList!=null && objList.size() > 0){
					returnObject = (Object)objList.get(0);
				}
			
		}catch(Exception e){
			//log.debug(" exception in velos helper" + e);
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return returnObject;
	}
	
	public static List getListObject(String code,PaginateSearch paginateSearch,int i)
	{
		return getListObject(code,paginateSearch,i, null);
	}
	public static List getListObject(String code,PaginateSearch paginateSearch,int i , Session session)
	{
	
		boolean sessionFlag = false;
		List list = new ArrayList();
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = code;
			Query query = session.createSQLQuery(sql);
			if(paginateSearch!=null && i==1){
				query.setFirstResult(paginateSearch.getiPageNo());
				query.setMaxResults(paginateSearch.getiShowRows());
			}
			list = query.list();
//			if (sessionFlag) {
//				 close session, if session created in this method
//				 session.getTransaction().commit();
//			}

		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
     
		return list;
	}
	
	public static List<PendingOrdersData> getColValues(String colName,String orderType){
		return getColValues(colName,orderType,null);
	}
	
	public static List<PendingOrdersData> getColValues(String colName,String orderType,Session session){
		List<PendingOrdersData> lst=new ArrayList<PendingOrdersData>();
		String sql=null;
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String order=session.createSQLQuery("select pk_codelst from er_codelst where codelst_type='"+VelosMidConstants.ORDER_TYPE_NEW+"' and codelst_subtyp='"+VelosMidConstants.ALL_ORDERS+"'").list().get(0).toString();
			//log.debug("order ype is:::::::::::"+order);
			if(orderType.toString().equals(order)){
			if (colName.toString().equals("ORDER_ENTITYID")){
				sql="SELECT DISTINCT E.ORDER_ENTITYID AS subType,CB.CORD_LOCAL_CBU_ID AS description FROM ER_ORDER E,CB_CORD CB WHERE E.ORDER_ENTITYID=CB.PK_CORD";
			}
			else if (colName.toString().equals("FK_ORDER_STATUS")){
				sql="SELECT DISTINCT E.FK_ORDER_STATUS AS subType,CL.CODELST_DESC AS description FROM ER_ORDER E,ER_CODELST CL WHERE E.FK_ORDER_STATUS=CL.PK_CODELST";
			}
			else if (colName.toString().equals("FK_ORDER_TYPE")){
				sql="SELECT DISTINCT E.FK_ORDER_TYPE AS subType,CL.CODELST_DESC AS description FROM ER_ORDER E,ER_CODELST CL WHERE E.FK_ORDER_TYPE=CL.PK_CODELST";
			}
			else if (colName.toString().equals("ORDER_DATE")){
				sql="SELECT DISTINCT TO_CHAR(ORDER_DATE,'Mon dd, yyyy') AS subType,TO_CHAR(ORDER_DATE,'Mon dd, yyyy') AS description FROM ER_ORDER";
			}
			else if (colName.toString().equals("TASK")){
				sql="SELECT DISTINCT TASK_ID AS subType,TASK_NAME AS description FROM ER_ORDER";
			}
			else if (colName.toString().equals("ASSIGNED_TO")){
				sql="SELECT DISTINCT ASSIGNED_TO AS subType,ASSIGNED_TO AS description FROM ER_ORDER";
			}
			}
			else{
				if (colName.toString().equals("ORDER_ENTITYID")){
					sql="SELECT DISTINCT E.ORDER_ENTITYID AS subType,CB.CORD_LOCAL_CBU_ID AS description FROM ER_ORDER E,CB_CORD CB WHERE E.ORDER_ENTITYID=CB.PK_CORD AND FK_ORDER_TYPE='"+orderType+"'";
				}
				else if (colName.toString().equals("FK_ORDER_STATUS")){
					sql="SELECT DISTINCT E.FK_ORDER_STATUS AS subType,CL.CODELST_DESC AS description FROM ER_ORDER E,ER_CODELST CL WHERE E.FK_ORDER_STATUS=CL.PK_CODELST AND FK_ORDER_TYPE='"+orderType+"'";
				}
				else if (colName.toString().equals("FK_ORDER_TYPE")){
					sql="SELECT DISTINCT E.FK_ORDER_TYPE AS subType,CL.CODELST_DESC AS description FROM ER_ORDER E,ER_CODELST CL WHERE E.FK_ORDER_TYPE=CL.PK_CODELST AND FK_ORDER_TYPE='"+orderType+"'";
				}
				else if (colName.toString().equals("ORDER_DATE")){
					sql="SELECT DISTINCT TO_CHAR(ORDER_DATE,'Mon dd, yyyy') AS subType,TO_CHAR(ORDER_DATE,'Mon dd, yyyy') AS description FROM ER_ORDER WHERE FK_ORDER_TYPE='"+orderType+"'";
				}
				else if (colName.toString().equals("TASK")){
					sql="SELECT DISTINCT TASK_ID AS subType,TASK_NAME AS description FROM ER_ORDER WHERE FK_ORDER_TYPE='"+orderType+"'";
				}
				else if (colName.toString().equals("ASSIGNED_TO")){
					sql="SELECT DISTINCT ASSIGNED_TO AS subType,ASSIGNED_TO AS description FROM ER_ORDER WHERE FK_ORDER_TYPE='"+orderType+"'";
				}
			}
			//log.debug("Query isssssssssss:::::"+sql);
			SQLQuery query=session.createSQLQuery(sql);
			List li=query.list();
			
			
			for(int l=0;l<li.size();l++){
	             //log.debug("list after iteration--"+li.get(l));
	             Object[] obj = (Object[])li.get(l);
	             //log.debug("obj.length :: "+obj.length);
	             PendingOrdersData pod =new PendingOrdersData();
	             pod.setSubType(obj[0].toString());
	             pod.setDescription(obj[1].toString());
	             //log.debug("sub type -->"+pod.getSubType());
				 //log.debug("disc --->"+pod.getDescription());
	             lst.add(pod);
			}

			/*while(it.hasNext()){
				PendingOrdersData pod = new PendingOrdersData();
				Object obj = (Object)it.next();
				
				pod = (PendingOrdersData) ObjectTransfer
				.transferObjects(obj, pod);
				//log.debug("sub type -->"+pod.getSubType());
				//log.debug("disc --->"+pod.getDescription());
				lst.add(pod);
			}*/
			
			
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return lst;
	}
	
	
	public static List<CodeList> getOrdersList(){
		return getOrdersList(null);
	}
	
	public static List<CodeList> getOrdersList(Session session){
		List<CodeList> lst=new ArrayList<CodeList>();
		//String sql=null;
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			
//				sql="SELECT PK_CODELST,CODELST_DESC FROM ER_CODELST WHERE CODELST_TYPE='order_type' AND CODELST_SUBTYP IN('CT','HE','OR','ALL')";
//			
//			//log.debug("Query isssssssssss:::::"+sql);
//			SQLQuery query=session.createSQLQuery(sql);
			lst=(List<CodeList>)session.createQuery("from CodeList where type='order_type' and subType in('CT','HE','OR','ALL')").list();
			
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return lst;
	}
	
	public static ClinicalNotePojo getAmendList(String query){
		
		//log.debug("Inside velos helper!!!!!!!!!!!!!!");
		ClinicalNotePojo clinicalNotePojo = new ClinicalNotePojo();
		List<Object> list=new ArrayList<Object>();
		Session session=null;
		try{
		if (session == null) {
			// create new session
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
		}
		list=(List<Object>) session.createQuery(query).list();
		
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		clinicalNotePojo = (ClinicalNotePojo)ObjectTransfer.transferObjects(list.get(0), clinicalNotePojo);
		//log.debug("Exiting velos helper!!!!!!!!!!!!!!");
		return clinicalNotePojo;
	}

}
