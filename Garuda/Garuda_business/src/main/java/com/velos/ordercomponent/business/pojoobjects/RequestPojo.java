package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

public class RequestPojo extends AuditablePojo{

	private Long pkRequest;
	private Long fkorderId;
	private String requestStatus;
	private Date lstReqStatDate;
	private Long reqLstReviewedBy;
	private Date reqLstReviewedDate;
	private Long reqAssignedTo;
	private Date reqAssignedDate;
	private long fkCordId;
	
	
	
	
	public Long getPkRequest() {
		return pkRequest;
	}
	public void setPkRequest(Long pkRequest) {
		this.pkRequest = pkRequest;
	}
	public Long getFkorderId() {
		return fkorderId;
	}
	public void setFkorderId(Long fkorderId) {
		this.fkorderId = fkorderId;
	}
	public String getRequestStatus() {
		return requestStatus;
	}
	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}
	public Date getLstReqStatDate() {
		return lstReqStatDate;
	}
	public void setLstReqStatDate(Date lstReqStatDate) {
		this.lstReqStatDate = lstReqStatDate;
	}
	public Long getReqLstReviewedBy() {
		return reqLstReviewedBy;
	}
	public void setReqLstReviewedBy(Long reqLstReviewedBy) {
		this.reqLstReviewedBy = reqLstReviewedBy;
	}
	public Date getReqLstReviewedDate() {
		return reqLstReviewedDate;
	}
	public void setReqLstReviewedDate(Date reqLstReviewedDate) {
		this.reqLstReviewedDate = reqLstReviewedDate;
	}
	public Long getReqAssignedTo() {
		return reqAssignedTo;
	}
	public void setReqAssignedTo(Long reqAssignedTo) {
		this.reqAssignedTo = reqAssignedTo;
	}
	public Date getReqAssignedDate() {
		return reqAssignedDate;
	}
	public void setReqAssignedDate(Date reqAssignedDate) {
		this.reqAssignedDate = reqAssignedDate;
	}
	public long getFkCordId() {
		return fkCordId;
	}
	public void setFkCordId(long fkCordId) {
		this.fkCordId = fkCordId;
	}
	
	
	
}
