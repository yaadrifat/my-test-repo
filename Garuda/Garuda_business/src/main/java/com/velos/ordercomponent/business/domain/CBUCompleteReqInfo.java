package com.velos.ordercomponent.business.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CB_CORD_COMPLETE_REQ_INFO")
@SequenceGenerator(sequenceName="SEQ_CB_CORD_COMPLETE_REQ_INFO",name="SEQ_CB_CORD_COMPLETE_REQ_INFO",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class CBUCompleteReqInfo extends Auditable{
	private Long pkCbuCompleteReqInfo;
	private Long fkCordId;
	//private Long fkOrderId;
	private Long cbuInfoFlag;
	private Long cbuProcessInfoFlag;
	private Long labSummaryFlag;
	private Long mrqFlag;
	private Long fmhqFlag;
	private Long idmFlag;
	private Long completeReqinfoFlag;
	private Boolean deletedFlag;
	private Long idsFlag;
	private Long licensureFlag;
	private Long eligibleFlag;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_CORD_COMPLETE_REQ_INFO")
	@Column(name="PK_CORD_COMPLETE_REQINFO")
	public Long getPkCbuCompleteReqInfo() {
		return pkCbuCompleteReqInfo;
	}
	public void setPkCbuCompleteReqInfo(Long pkCbuCompleteReqInfo) {
		this.pkCbuCompleteReqInfo = pkCbuCompleteReqInfo;
	}
	
	@Column(name="FK_CORD_ID")
	public Long getFkCordId() {
		return fkCordId;
	}
	public void setFkCordId(Long fkCordId) {
		this.fkCordId = fkCordId;
	}
	
	@Column(name="CBU_INFO_FLAG")
	public Long getCbuInfoFlag() {
		return cbuInfoFlag;
	}
	public void setCbuInfoFlag(Long cbuInfoFlag) {
		this.cbuInfoFlag = cbuInfoFlag;
	}
	
	@Column(name="CBU_PROCESSING_FLAG")
	public Long getCbuProcessInfoFlag() {
		return cbuProcessInfoFlag;
	}
	public void setCbuProcessInfoFlag(Long cbuProcessInfoFlag) {
		this.cbuProcessInfoFlag = cbuProcessInfoFlag;
	}
	
	@Column(name="LAB_SUMMARY_FLAG")
	public Long getLabSummaryFlag() {
		return labSummaryFlag;
	}
	public void setLabSummaryFlag(Long labSummaryFlag) {
		this.labSummaryFlag = labSummaryFlag;
	}
	
	@Column(name="MRQ_FLAG")
	public Long getMrqFlag() {
		return mrqFlag;
	}
	public void setMrqFlag(Long mrqFlag) {
		this.mrqFlag = mrqFlag;
	}
	
	@Column(name="FMHQ_FLAG")
	public Long getFmhqFlag() {
		return fmhqFlag;
	}
	public void setFmhqFlag(Long fmhqFlag) {
		this.fmhqFlag = fmhqFlag;
	}
	
	@Column(name="IDM_FLAG")
	public Long getIdmFlag() {
		return idmFlag;
	}
	public void setIdmFlag(Long idmFlag) {
		this.idmFlag = idmFlag;
	}
	
	@Column(name="COMPLETE_REQ_INFO_FLAG")
	public Long getCompleteReqinfoFlag() {
		return completeReqinfoFlag;
	}
	public void setCompleteReqinfoFlag(Long completeReqinfoFlag) {
		this.completeReqinfoFlag = completeReqinfoFlag;
	}
	
	@Column(name="IDS_FLAG")
	public Long getIdsFlag() {
		return idsFlag;
	}
	public void setIdsFlag(Long idsFlag) {
		this.idsFlag = idsFlag;
	}
	
	@Column(name="LICENSURE_FLAG")
	public Long getLicensureFlag() {
		return licensureFlag;
	}
	public void setLicensureFlag(Long licensureFlag) {
		this.licensureFlag = licensureFlag;
	}
	
	@Column(name="ELIGIBILITY_FLAG")
	public Long getEligibleFlag() {
		return eligibleFlag;
	}
	public void setEligibleFlag(Long eligibleFlag) {
		this.eligibleFlag = eligibleFlag;
	}

	@Column(name="DELETEDFLAG")
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	
	
}
