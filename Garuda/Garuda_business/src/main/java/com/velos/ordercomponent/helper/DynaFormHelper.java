package com.velos.ordercomponent.helper;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SQLQuery;
import org.hibernate.classic.Session;

import com.velos.eres.web.user.UserJB;
import com.velos.ordercomponent.business.domain.Auditable;
import com.velos.ordercomponent.business.domain.FormResponses;
import com.velos.ordercomponent.business.domain.FormVersion;
import com.velos.ordercomponent.business.pojoobjects.FormResponsesPojo;
import com.velos.ordercomponent.business.pojoobjects.FormVersionPojo;
import com.velos.ordercomponent.business.util.HibernateUtil;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.VelosUtil;

public class DynaFormHelper {
	static DateFormat dat=new SimpleDateFormat("MMM dd, yyyy");
	public static final Log log = LogFactory.getLog(DynaFormHelper.class);

	public static List getDynaFormLst(Long cordId,String formName,String formvers,Long fkfrmversion){
		return getDynaFormLst(cordId,formName,formvers,fkfrmversion,null);
	}
	
	public static List getDynaFormLst(Long cordId,String formName,String formvers,Long fkfrmversion,Session session){
		//log.debug("Inside the Dynamic Form Helper");
		List dynamicfrmList=null;
		String sql=null;
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
		sql="select pk_questions,ques_desc,ques.response_type,forq.fk_master_ques,ques.ques_help,"
				+"ques.ques_hover,ques.add_comment_flag,ques.assesment_flag,forq.response_value,forq.unexpected_response_value,"
				+"ques.unlicn_prior_to_shipment_flag,forq.fk_dependent_ques,forq.fk_dependent_ques_value,formres.pk_form_responses,"
				+"formres.fk_question,formres.resp_code,formres.resp_val,formres.comments,formres.fk_form_responses,"
				+"formres.pk_assessment,ques.ques_code,formres.dynaformDate,qgrp.fk_question_group,ques.unlicen_req_flag,"
				+"forq.ques_seq,forq.fk_form,ques.ref_chart,forq.defer_value,"
				+"(select 1 from cb_form_responses s where s.fk_question = forq.fk_master_ques and s.fk_form_version = "+fkfrmversion
				+" and s.entity_id ="+cordId+" and forq.fk_dependent_ques_value LIKE '%'||nvl(s.response_code,-1)||'%') status1,"
				+"(select 1 from cb_form_responses s where s.fk_question = forq.fk_dependent_ques and s.fk_form_version = "+fkfrmversion
				+" and s.entity_id ="+cordId+" and (s.response_value LIKE  '%'||forq.fk_dependent_ques_value||'%' OR forq.fk_dependent_ques_value LIKE '%'||nvl(s.response_code,-1)||'%')) status2,"
				+" (SELECT 1 FROM cb_form_responses s WHERE s.fk_question = (SELECT s.fk_dependent_ques FROM cb_form_questions s,cb_form_responses f WHERE s.fk_question=forq.fk_dependent_ques AND f.fk_form_version ="+fkfrmversion 
				+" AND f.entity_id = "+cordId+" and s.fk_question=f.fk_question and s.fk_form=f.fk_form)  AND s.fk_form_version ="+fkfrmversion
				+" AND s.entity_id="+cordId+" AND (s.response_value LIKE '%'||forq.fk_dependent_ques_value||'%' OR forq.fk_dependent_ques_value LIKE '%'||NVL(s.response_code,-1)||'%')) status3"
				+" from cb_form_questions forq,cb_question_grp qgrp,cb_questions ques left outer join (select frmres.pk_form_responses,"
				+"frmres.fk_question,frmres.response_code resp_code,frmres.response_value resp_val,frmres.comments,"
				+"frmres.fk_form_responses,asses.pk_assessment pk_assessment,"
				+"to_char(frmres.DATE_MOMANS_OR_FORMFILL,'Mon DD, YYYY') dynaformDate from "
				+" cb_form_responses frmres  left outer join (select count(pk_assessment) pk_assessment,"
				+"sub_entity_id FROM cb_assessment WHERE sub_entity_type = (select"
                +" pk_codelst from  er_codelst  where  codelst_type='sub_entity_type' and codelst_subtyp='"+formName
                +"') GROUP BY sub_entity_id) asses on( asses.sub_entity_id=frmres.pk_form_responses"
                +" ) where frmres.entity_id="+cordId+" and frmres.fk_form_version="+fkfrmversion
                +") formres on(formres.FK_QUESTION=ques.pk_questions) where"
				+" ques.pk_questions=forq.fk_question and ques.pk_questions=qgrp.fk_question and "
				+"forq.fk_form=(select pk_form from cb_forms where forms_desc='"+formName+"' AND"
				+" version='"+formvers+"') and qgrp.fk_form = (SELECT pk_form FROM cb_forms WHERE forms_desc ='"+formName
				+"' AND version='"+formvers+"') order by to_number(regexp_substr(forq.ques_seq,'^[0-9]+')),forq.ques_seq,"
				+" ques.pk_questions,formres.pk_form_responses";
		
		//log.debug("sql:"+sql);
		SQLQuery query = session.createSQLQuery(sql);
		dynamicfrmList = query.list();

	} catch (Exception e) {
		log.error(e.getMessage());
		//log.debug(" Exception in getDynaFormLst: " + e);
		e.printStackTrace();
	} finally {
		if (session!=null && session.isOpen()) {
			session.close();
		}
	}
		return dynamicfrmList;
		
	}
	
	public static FormVersionPojo getFormVerPojo(Long cordId,String formName,String formVers,Long formseq){
		return getFormVerPojo(cordId,formName,formVers,formseq,null);
	}
	
	public static FormVersionPojo getFormVerPojo(Long cordId,String formName,String formVers,Long formseq,Session session){
		FormVersion formVersion = new FormVersion();
		FormVersionPojo formVersionPojo = new FormVersionPojo();
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if (cordId != null) {
				String sql = "select to_char(DATE_MOMANS_OR_FORMFILL,'Mon DD, YYYY'),PK_FORM_VERSION,ENTITY_TYPE from cb_form_version where ENTITY_ID="+cordId+" and FK_FORM=(select forms.pk_form from cb_forms forms where forms.forms_desc='"+formName+"' and forms.version='"+formVers+"') and FORM_SEQ="+formseq+" and (void_flag is null or void_flag=0)";
				//log.debug("getFormVerPojo:SQL:"+sql);
				SQLQuery query =session.createSQLQuery(sql);
				List lst = query.list();
				if(lst!=null && lst.size()>0){
					if(lst.get(0)!=null){
						Object [] obj = (Object[])lst.get(0);
						if(obj[0]!=null && !obj[0].equals("")){
							formVersionPojo.setDynaFormDateStr(obj[0].toString());
						}
						if(obj[1]!=null && !obj[1].equals("")){
							formVersionPojo.setPkformversion(Long.parseLong(obj[1].toString()));
						}
						if(obj[2]!=null && !obj[2].equals("")){
							formVersionPojo.setEntityType(Long.parseLong(obj[2].toString()));
						}
					}
				}
				/*formVersion =(FormVersion) lst.get(0);
				if (formVersion != null) {
					formVersionPojo = (FormVersionPojo) ObjectTransfer.transferObjects(
							formVersion, formVersionPojo);
				}*/
				//log.debug("formVersionPojo : "+formVersionPojo);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session!=null && session.isOpen()) {
				session.close();
			}
		}
		return formVersionPojo;
	}
	
	private static Date Date(String string) {
		// TODO Auto-generated method stub
		return null;
	}
	public static Date getFormCreatedDate(Long cordId,String formName,String formVers){
		return getFormCreatedDate(cordId,formName,formVers,null);
	}
	
	public static Date getFormCreatedDate(Long cordId,String formName,String formVers,Session session){
		Date formCDate = null;
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if (cordId != null) {
				//String sql="select max(formv.form_seq) from cb_form_version formv,cb_forms forms WHERE forms.pk_form =formv.fk_form	AND forms.forms_desc='"+formName+"' AND formv.entity_id ="+cordId;
				String sql = "select created_on from cb_form_version where entity_id="+cordId+" and fk_form=(select pk_form from cb_forms where forms_desc='"+formName+"' and version='"+formVers+"') order by pk_form_version";
				log.debug("getFormCreatedDate:SQL:"+sql);
				SQLQuery query = session.createSQLQuery(sql);
				List lst=query.list();
				if(lst!=null && lst.size()>0){
					if(lst.get(0)!=null){
						formCDate = (Date) lst.get(0);
					}
				}
				System.out.println("formCDate : "+formCDate);
			}
		} catch (Exception e) {
			//log.debug(" Exception " + e);
			//log.debug(e.getStackTrace());
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session!=null && session.isOpen()) {
				session.close();
			}
		}
		return formCDate;
	}
	public static Long getFormseq(Long cordId,String formName,String formVers){
		return getFormseq(cordId,formName,formVers,null);
	}
	
	public static Long getFormseq(Long cordId,String formName,String formVers,Session session){
		Long seqnumber=null;
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if (cordId != null) {
				String sql="select max(formv.form_seq) from cb_form_version formv,cb_forms forms WHERE forms.pk_form =formv.fk_form	AND forms.forms_desc='"+formName+"' AND formv.entity_id ="+cordId;
				//log.debug("getFormseq:SQL:"+sql);
				SQLQuery query = session.createSQLQuery(sql);
				List lst=query.list();
				if(lst!=null && lst.size()>0){
					if(lst.get(0)!=null){
						seqnumber = Long.parseLong(lst.get(0).toString());
					}
				}
				//log.debug("seqnumber : "+seqnumber);
			}
		} catch (Exception e) {
			//log.debug(" Exception " + e);
			//log.debug(e.getStackTrace());
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session!=null && session.isOpen()) {
				session.close();
			}
		}
		return seqnumber;
	}
	
/*	public static List getChildQuest(Long pkQuestion,String formName){
		return getChildQuest(pkQuestion,formName,null);
	}
	
	
	
	public static List getChildQuest(Long pkQuestion,String formName,Session session){
		//log.debug("Inside the getChildQuest Method");
		List dynamicfrmList=null;
		String sql=null;
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
		sql= " select pk_questions,ques_desc,(select CODELST_DESC from er_codelst where PK_CODELST=ques.response_type),"
	        +" ques.fk_master_ques,ques.ques_help,ques.ques_hover,ques.add_comment_flag,ques.assesment_flag,qres.response_value,"
	        +" qres.unexpected_response_value,ques.unlicn_prior_to_shipment_flag,ques.fk_dependent_ques,"
	        +" ques.fk_dependent_ques_value from cb_form_questions forq,cb_question_responses qres,cb_questions ques" 
	        +" where ques.pk_questions=forq.fk_question and ques.pk_questions=qres.fk_question and forq.fk_form=("
	        +" select pk_form from cb_forms where forms_desc='"+formName+"' and IS_CURRENT_FLAG=1) and ques.fk_dependent_ques="+pkQuestion
	        +" order by ques.pk_questions";
		SQLQuery query = session.createSQLQuery(sql);
		dynamicfrmList = query.list();

	} catch (Exception e) {
		//log.debug(" Exception in getDynaFormLst: " + e);
		//log.debug(e.getStackTrace());
	} finally {
		if (session.isOpen()) {
			session.close();
		}
	}
		return dynamicfrmList;
		
	}*/
	
	public static List getQuestionGroup(String formName,String formvers){
		return getQuestionGroup(formName,formvers,null);
	}
	
	public static List getQuestionGroup(String formName,String formvers,Session session){
		List quesgrp= new ArrayList();
		String sql=null;
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			sql="select distinct pk_question_group,question_grp_desc from cb_question_group quesgrp,"
				+" cb_question_grp qgrp where qgrp.fk_question_group=quesgrp.pk_question_group and "
				+" qgrp.fk_form=(select pk_form from cb_forms frms where frms.forms_desc='"+formName+"' and frms.version='"+formvers+"') order by quesgrp.pk_question_group";
			//log.debug("getQuestionGroup:SQL:"+sql);
			SQLQuery query=session.createSQLQuery(sql);
			quesgrp=query.list();
		}
		catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage());
			e.printStackTrace();
			//log.debug("Exception in getQuestionGroup:"+e);
			//log.debug(e.getStackTrace());
		}finally {
			if (session!=null && session.isOpen()) {
				session.close();
			}
		}
		return quesgrp;
	}
	
	public static FormVersionPojo saveFormVersion(FormVersionPojo formVersionPojo,boolean formUI){
		return saveFormVersion(formVersionPojo,null,formUI);
	}
	
public static FormVersionPojo saveFormVersion(FormVersionPojo formVersionPojo,Session session,boolean formUI){
	try {
		FormVersion formversion = new FormVersion();
		formversion = (FormVersion) ObjectTransfer.transferObjects(formVersionPojo,formversion);
		if(formUI)
		formversion = (FormVersion) new VelosUtil().setAuditableInfo(formversion);
		boolean sessionFlag = false;
		
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}

			if (formversion!=null && !formversion.equals("")) {
				//log.debug("merge or save:::: FormVersion object .......");
				if (formversion.getPkformversion() != null) {
					//log.debug("merging .........  ::::");
					session.merge(formversion);
				} else {
					//log.debug("saving .............  ::::");
					session.save(formversion);
				}
			}
			if (sessionFlag) {
				session.getTransaction().commit();
			}

			if (formversion  != null && !formversion.equals("")) {
				formVersionPojo=new FormVersionPojo();
				formVersionPojo = (FormVersionPojo) ObjectTransfer.transferObjects(formversion, formVersionPojo);
			}

		} catch (Exception e) {
			//log.debug(" Exception in saveFormVersion " + e);
			//log.debug(e.getStackTrace());
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session!=null && session.isOpen()) {
				session.close();
			}
		}
		return formVersionPojo;
	}
public static List getFormVersionList(Long cordId,String formName){
	return getFormVersionList(cordId,formName,null);
}

public static List getFormVersionList(Long CordId,String formName,Session session){
	List formverson= new ArrayList();
	String sql=null;
	boolean sessionFlag = false;
	try {
		if (session == null) {
			// create new session
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}
		sql="select formv.pk_form_version,formv.entity_id,formv.form_seq,formv.date_momans_or_formfill,"
			+" formv.fk_form,forms.forms_desc,forms.version,formv.created_on from cb_form_version formv,cb_forms forms where"
			+" forms.pk_form=formv.fk_form and forms.forms_desc='"+formName+"' and formv.entity_id="+CordId
			+" AND formv.void_flag is null or formv.void_flag = 0 order by formv.pk_form_version DESC";
		//System.out.println("sql::::"+sql);
		SQLQuery query=session.createSQLQuery(sql);
		formverson=query.list();
	}
	catch (Exception e) {
		// TODO: handle exception
		//log.debug("Exception in getFormVersionList:"+e);
		//log.debug(e.getStackTrace());
		log.error(e.getMessage());
		e.printStackTrace();
	}finally {
		if (session!=null && session.isOpen()) {
			session.close();
		}
	}
	return formverson;
}
	public static List saveorupdateDynaForm(List<FormResponsesPojo> formResponsesPojo,boolean formUI){
		return saveorupdateDynaForm(formResponsesPojo,null,formUI);
	}
	
	public static List saveorupdateDynaForm(List<FormResponsesPojo> formResponsesPojo,Session session,boolean formUI){
		//log.debug("Inside saveorupdateDynaForm");
		try {
		List<FormResponses> formresp = new ArrayList<FormResponses>();
		FormResponses formResponses = null;
		FormResponsesPojo formResponsesPojo2 = null;
		for (FormResponsesPojo formResponsesPojo3 : formResponsesPojo) {
			formResponses = new FormResponses();
			formResponses = (FormResponses) ObjectTransfer.transferObjects(formResponsesPojo3,formResponses);
			if(formUI)
			formResponses = (FormResponses) new VelosUtil().setAuditableInfo(formResponses);
			formresp.add(formResponses);
		}
		boolean sessionFlag = false;
		
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}

			for (FormResponses p : formresp) {
				//log.debug("merge or save:::: DynaForm object .......");
				/*if (p.getPkresponse() != null) {
					//log.debug("merging .........  ::::");
					session.merge(p);
				} else {*/
					//log.debug("saving .............  ::::");
					session.save(p);
			//	}
			}
			if (sessionFlag) {
				session.getTransaction().commit();
			}

			if (formresp != null && formresp.size() > 0) {
				formResponsesPojo.clear();
				for (FormResponses pl : formresp) {
					formResponsesPojo2=new FormResponsesPojo();
					formResponsesPojo2 = (FormResponsesPojo) ObjectTransfer.transferObjects(pl, formResponsesPojo2);
					formResponsesPojo.add(formResponsesPojo2);
				}
			}

		} catch (Exception e) {
			//log.debug(" Exception in saveOrUpdateDynaForms " + e);
			//log.debug(e.getStackTrace());
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session!=null && session.isOpen()) {
				session.close();
			}
		}
		return formResponsesPojo;
	}
	
	public static Object getformvmax(Long cordId,String formName){
		return getformvmax(cordId,formName,null);
	}
	

	public static Object getformvmax(Long cordId,String formName,Session session){
		//log.debug("Inside the Dynamic Form Helper");
		Long pkformversion=null;
		String sql=null;
		Object [] obj = new Object [5];
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			
		sql="select max(pk_form_version) from cb_form_version where entity_id="+cordId+" and fk_form in (select pk_form from cb_forms where forms_desc='"+formName+"') and (void_flag is null or void_flag=0)";
		log.debug("getformvmaxPk:SQL:"+sql);
		SQLQuery query = session.createSQLQuery(sql);
		List lst=query.list();
		if(lst!=null && lst.size()>0){
			if(lst.get(0)!=null){
				obj[0] = ((BigDecimal)lst.get(0)).longValue();
			}
		}
		if(obj[0]!=null){
			sql = "SELECT (SELECT version FROM cb_forms WHERE pk_form=fk_form) FROM cb_form_version WHERE pk_form_version="+obj[0];
			log.debug("getForm Version :SQL:"+sql);
			query = session.createSQLQuery(sql);
			lst = query.list();
			if(lst!=null && lst.size()>0){
				if(lst.get(0)!=null){
					obj[1] = lst.get(0).toString();
				}
			}
		}
	} catch (Exception e) {
		//log.debug(" Exception in getDynaFormLst: " + e);
		//log.debug(e.getStackTrace());
		log.error(e.getMessage());
		e.printStackTrace();
	} finally {
		if (session!=null && session.isOpen()) {
			session.close();
		}
	}
		return obj;
		
	}
	
	public static Long getformvmaxPk(Long cordId,String formName,String formvers){
		return getformvmaxPk(cordId,formName,formvers,null);
	}
	

	public static Long getformvmaxPk(Long cordId,String formName,String formvers,Session session){
		//log.debug("Inside the Dynamic Form Helper");
		Long pkformversion=null;
		String sql=null;
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			
			sql="select max(pk_form_version) from cb_form_version where entity_id="+cordId+" and fk_form in (select pk_form from cb_forms where forms_desc='"+formName+"') and (void_flag is null or void_flag=0)";
			//log.debug("getformvmaxPk:SQL:"+sql);
		SQLQuery query = session.createSQLQuery(sql);
		List lst=query.list();
		if(lst!=null && lst.size()>0){
			if(lst.get(0)!=null){
				pkformversion = Long.parseLong(lst.get(0).toString());
			}
		}
	} catch (Exception e) {
		//log.debug(" Exception in getDynaFormLst: " + e);
		//log.debug(e.getStackTrace());
		log.error(e.getMessage());
		e.printStackTrace();
	} finally {
		if (session!=null && session.isOpen()) {
			session.close();
		}
	}
		return pkformversion;
		
	}
	public static Long getfkCbbid(Long entityId){
		return getfkCbbid(entityId,null);
	}
	public static Long getfkCbbid(Long entityId,Session session) {
		String sql=null;
		boolean sessionFlag = false;
		Long siteId=null;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			sql="select fk_cbb_id from cb_cord where pk_cord="+entityId;
			SQLQuery query = session.createSQLQuery(sql);
			List lst=query.list();
			if(lst!=null && lst.size()>0){
				if(lst.get(0)!=null){
					siteId = Long.parseLong(lst.get(0).toString());
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
			//log.debug("Exception In DynaFormHelper");
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session!=null && session.isOpen()) {
				session.close();
			}
		}
		return siteId;
	}
	
public static List getQuestionsByFormId(Long formId){
		return getQuestionsByFormId(formId,null);
}
public static List getQuestionsByFormId(Long formId,Session session){
	String sql=null;
	List lst = null;
	boolean sessionFlag = false;
	try {
		if (session == null) {
			// create new session
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}
		lst = new ArrayList();
		sql = "select ques.PK_QUESTIONS,QUES.QUES_CODE,fques.response_value,fques.unexpected_response_value from cb_questions ques,cb_form_questions fques  where ques.PK_QUESTIONS = fques.FK_QUESTION and fques.fk_form="+formId+" order by to_number(regexp_substr(fques.ques_seq,'^[0-9]+')),fques.ques_seq";
		//log.debug("sql::::::::::::::::::::::::::::"+sql);
		SQLQuery query = session.createSQLQuery(sql);
		//query.addEntity("ques",FormQuestions.class);
		lst=query.list();
} catch (Exception e) {
	//log.debug(" Exception in getDynaFormLst: " + e);
	log.error(e.getMessage());
	e.printStackTrace();
} finally {
	if (session!=null && session.isOpen()) {
		session.close();
	}
}
	return lst;
}

public static List getDynaQuesLst(Long cordId,String formName){
	return getDynaQuesLst(cordId,formName,null);
}

public static List getDynaQuesLst(Long cordId,String formName,Session session){
	//log.debug("Inside the Dynamic Form Helper");
	List dynamicfrmList=null;
	String sql=null;
	boolean sessionFlag = false;
	try {
		if (session == null) {
			// create new session
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}
		
		sql="SELECT pk_questions,ques_desc,ques.response_type,"
			+" forq.fk_master_ques,ques.ques_help,ques.ques_hover,ques.add_comment_flag,ques.assesment_flag,forq.response_value,"
			+" forq.unexpected_response_value,ques.unlicn_prior_to_shipment_flag,forq.fk_dependent_ques,forq.fk_dependent_ques_value,ques.ques_code,"
			+" qgrp.fk_question_group,ques.unlicen_req_flag,forq.ques_seq,forq.fk_form,ques.ref_chart FROM cb_form_questions forq,"
			+" cb_question_grp qgrp,cb_questions ques WHERE ques.pk_questions=forq.fk_question AND ques.pk_questions = qgrp.fk_question"
			+" AND forq.fk_form =(SELECT pk_form FROM cb_forms WHERE forms_desc='"+formName+"' AND is_current_flag=1 )"
			+" AND qgrp.fk_form =(SELECT pk_form FROM cb_forms WHERE forms_desc ='"+formName+"' AND is_current_flag=1)"
			+" ORDER BY qgrp.pk_question_grp, ques.pk_questions";
		//log.debug("getDynaQuesLst:SQL:"+sql);
	SQLQuery query = session.createSQLQuery(sql);
	dynamicfrmList = query.list();

} catch (Exception e) {
	//log.debug(" Exception in getDynaFormLst: " + e);
	//log.debug(e.getMessage());
	log.error(e.getMessage());
	e.printStackTrace();
} finally {
	if (session!=null && session.isOpen()) {
		session.close();
	}
}
	return dynamicfrmList;
	
}

public static List<Object> getPrevAssessData(Long entityId,Long fk_formVersion){
	return getPrevAssessData(entityId,fk_formVersion,null);
}
public static List<Object> getPrevAssessData(Long entityId,Long fk_formVersion,Session session){
	//log.debug("Inside getPrevAssessData");
	List<Object> assessmentList=new ArrayList<Object>();
	String sql=null;
	boolean sessionFlag = false;
	try {
		if (session == null) {
			// create new session
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}
		//log.debug("formResponsesPojo:"+formResponsesPojo.size());
		//log.debug("formResponsesPojo:"+formResponsesPojo);
		/*int i=0;
		for(FormResponsesPojo formres:formResponsesPojo){
			List assessList=null;
			sql="select assess.assessment_remarks, assess.flag_for_later, assess.assessment_for_response, assess.tc_visibility_flag,"
				+"assess.assessment_reason,assess.assessment_reason_remarks,assess.availability_date,assess.sent_to_review_flag,"
				+"assess.consulte_flag,assess.assessment_reviewed,assess.column_reference,assess.assessment_postedby,assess.assessment_postedon,"
				+"assess.assessment_consultby,assess.assessment_consulton,formres.fk_question from cb_assessment assess, cb_form_responses formres"
				+" where assess.sub_entity_id ="+formres.getPkresponse()+" and formres.pk_form_responses="+formres.getPkresponse()+" and assess.entity_id="+formres.getEntityId();
			//log.debug("Sql:"+sql);
			SQLQuery query = session.createSQLQuery(sql);
			assessList =query.list(); 
			//log.debug("assessList:"+assessList);
			if(assessList!=null && !assessList.isEmpty() && assessList.size()>0){
				assessmentList.addAll(assessList);
			}
			i++;
		}*/
		sql="SELECT assess.assessment_remarks, " +
		"  assess.flag_for_later, " +
		"  assess.assessment_for_response, " +
		"  assess.tc_visibility_flag, " +
		"  assess.assessment_reason, " +
		"  assess.assessment_reason_remarks, " +
		"  assess.availability_date, " +
		"  assess.sent_to_review_flag, " +
		"  assess.consulte_flag, " +
		"  assess.assessment_reviewed, " +
		"  assess.column_reference, " +
		"  assess.assessment_postedby, " +
		"  assess.assessment_postedon, " +
		"  assess.assessment_consultby, " +
		"  assess.assessment_consulton, " +
		"  formres.fk_question, " +
		"  assess.ASSESSMENT_FLAG_COMMENT "+
		" FROM cb_assessment assess, " +
		"  cb_form_responses formres " +
		" WHERE assess.sub_entity_id IN " +
		"  (SELECT pk_form_responses " +
		"  FROM cb_form_responses " +
		"  WHERE fk_form_version = "+fk_formVersion +
		"  ) " +
		/*"AND formres.pk_form_responses IN " +
		"  (SELECT pk_form_responses " +
		"  FROM cb_form_responses " +
		"  WHERE fk_form_version =" +fk_formVersion +
		"  ) " +*/
		"AND assess.sub_entity_id = formres.pk_form_responses " +
		"AND assess.entity_id     ="+entityId;
		SQLQuery query = session.createSQLQuery(sql);
		assessmentList =query.list(); 
	//	System.out.println("sql:::"+sql);
	//	System.out.println("assessmentList size:"+assessmentList.size());
	//	System.out.println("assessmentList:"+assessmentList);
	}catch (Exception e) {
		// TODO: handle exception
		//log.debug("Exception in getPrevAssessData:"+e);
		log.error(e.getMessage());
		e.printStackTrace();
	}finally{
		if(session!=null && session.isOpen()){
			session.close();
		}
	}
	return assessmentList;
}
public static String getLatestFormVer(String formName){
	return getLatestFormVer(formName,null);
}
public static String getLatestFormVer(String formName,Session session){
	String formvers=null;
	String sql=null;
	boolean sessionFlag = false;
	try{
		if (session == null) {
			// create new session
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}
		sql="select version from cb_forms where forms_desc='"+formName+"' and is_current_flag=1";
		SQLQuery query = session.createSQLQuery(sql);
		List lst=query.list();
		if(lst!=null && lst.size()>0){
			if(lst.get(0)!=null){
				formvers = (lst.get(0).toString());
			}
		}
	}catch (Exception e) {
		// TODO: handle exception
		//log.debug("Exception in getFormVersion:"+e);
		log.error(e.getMessage());
		e.printStackTrace();
	}finally{
		if(session!=null && session.isOpen()){
			session.close();
		}
	}
	return formvers;
}

public static List getIdmDeferCondition(String formName){
	return getIdmDeferCondition(formName,null);
}
public static List getIdmDeferCondition(String formName,Session session){
	List deferConditionlst=null;
	String sql=null;
	boolean sessionFlag = false;
	try{
		if (session == null) {
			// create new session
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}
		sql="select fk_question,defer_value from cb_form_questions where fk_form=(select pk_form from cb_forms where forms_desc='"+formName+"' and is_current_flag=1) and defer_value is not null";
		SQLQuery query = session.createSQLQuery(sql);
		deferConditionlst=query.list();
		//log.debug("deferConditionlst:::"+deferConditionlst);
	}catch (Exception e) {
		// TODO: handle exception
		//log.debug("Exception in getFormVersion:"+e);
		log.error(e.getMessage());
		e.printStackTrace();
	}finally{
		if(session!=null && session.isOpen()){
			session.close();
		}
	}
	return deferConditionlst;
}
public static List getIdmResponseList(Long entityId){
	return getIdmResponseList(entityId,null);
}
public static List getIdmResponseList(Long entityId,Session session){
	List idmResponseList=null;
	String sql=null;
	boolean sessionFlag = false;
	try{
		if (session == null) {
			// create new session
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}
		/*sql="select fk_question,response_code from cb_form_responses WHERE fk_form=(SELECT pk_form FROM cb_forms WHERE forms_desc='"+formName+"' AND IS_CURRENT_FLAG=1)"
			+" and pk_form_responses in(select max(pk_form_responses) from cb_form_responses group by entity_id, fk_question having entity_id="+entityId+")";
		*/
		//sql = "SELECT fk_question,response_code,fk_form_version FROM cb_form_responses WHERE fk_form="+fkform+" AND fk_form_version ="+fkformversion;
		sql="SELECT fk_question, " +
		"  response_code " +
		"FROM cb_form_responses " +
		"WHERE fk_form= " +
		"  (SELECT pk_form FROM cb_forms WHERE forms_desc ='IDM' AND IS_CURRENT_FLAG=1 " +
		"  ) " +
		"AND fk_form_version = " +
		"  (SELECT MAX(pk_form_version) " +
		"  FROM cb_form_version " +
		"  WHERE entity_id=" +entityId+
		"  AND fk_form    = " +
		"    (SELECT pk_form FROM cb_forms WHERE forms_desc ='IDM' AND IS_CURRENT_FLAG=1 " +
		"    ) " +
		"  )";
		//System.out.println("sql:::"+sql);
		SQLQuery query = session.createSQLQuery(sql);
		idmResponseList=query.list();
		//log.debug("idmResponseList:::"+idmResponseList);
	}catch (Exception e) {
		// TODO: handle exception
		//log.debug("Exception in getFormVersion:"+e);
		log.error(e.getMessage());
		e.printStackTrace();
	}finally{
		if(session!=null && session.isOpen()){
			session.close();
		}
	}
	return idmResponseList;
}

public static void getOldFormVersion(Long entityId,String formName, boolean formUI) {
	boolean sessionFlag = false;
	Session session = null;
	String sql = null;
	Long modifiedBy = null;
	try {
		if(formUI){
			UserJB userObject = new VelosUtil().getUserDetails();
			if(userObject != null){ 
				modifiedBy = ((Integer)userObject.getUserId()).longValue();				
			}
		}
		//log.debug("modifiedBy:::"+modifiedBy);
		if (session == null) {
			// create new session
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();

		}
		sql="update cb_form_version set void_flag = 1,void_date = sysdate,LAST_MODIFIED_DATE=sysdate,LAST_MODIFIED_BY ="+modifiedBy+" where fk_form in (select pk_form from cb_forms where forms_desc = '"+formName+"') and entity_id ="+entityId+" and void_flag is null";
		SQLQuery query = session.createSQLQuery(sql);
		int status = query.executeUpdate();
		session.getTransaction().commit();
		//log.debug("Update status:::"+status);

	} catch (Exception e) {
		log.debug("Exception in getOldFormVersion:>::::::::::"+e);
		//log.debug(" Exception in getOldFormVersion " + e);
		log.error(e.getMessage());
		e.printStackTrace();

	} finally {
		if (session!=null && session.isOpen()) {
			session.close();
		}
	}
}
public static String getAssessmentDetail(Long assessmentPk){
	return getAssessmentDetail(assessmentPk,null);
}
public static String getAssessmentDetail(Long assessmentPk,Session session){
	String assessData = null;
	String sql = null;
	boolean sessionFlag = false;
	try{
		if (session == null) {
			// create new session
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}
		sql = "select f_get_assessdata("+assessmentPk+") from dual";
		
		SQLQuery query = session.createSQLQuery(sql);
		List assessDataList = query.list();
		if(assessDataList!=null && assessDataList.size()>0){
			assessData = assessDataList.get(0).toString();
		}
		//System.out.println("assessData from Helper::"+assessData);
		//log.debug("idmResponseList:::"+idmResponseList);
	}catch (Exception e) {
		// TODO: handle exception
		//log.debug("Exception in getFormVersion:"+e);
		log.error(e.getMessage());
		e.printStackTrace();
	}finally{
		if(session!=null && session.isOpen()){
			session.close();
		}
	}
	return assessData;
}
public static String getidmdefervalue(Long pkcord){
	return getidmdefervalue(pkcord,null);
}
public static String getidmdefervalue(Long pkcord,Session session){
	String deferstatus = null;
	String sql = null;
	boolean sessionFlag = false;
	try{
		if (session == null) {
			// create new session
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}
		sql = "select f_get_idmdefervalue("+pkcord+") from dual";
		
		SQLQuery query = session.createSQLQuery(sql);
		List DataList = query.list();
		if(DataList!=null && DataList.size()>0){
			deferstatus = DataList.get(0).toString();
		}
		//System.out.println("deferstatus from Helper::"+deferstatus);
		//log.debug("idmResponseList:::"+idmResponseList);
	}catch (Exception e) {
		// TODO: handle exception
		//log.debug("Exception in getFormVersion:"+e);
		log.error(e.getMessage());
		e.printStackTrace();
	}finally{
		if(session!=null && session.isOpen()){
			session.close();
		}
	}
	return deferstatus;
}
public static FormResponsesPojo saveFormData(FormResponsesPojo formobjects,Long entityId,String formName,boolean formUI){
	return saveFormData(formobjects,entityId,formName,formUI,null);
}
public static FormResponsesPojo saveFormData(FormResponsesPojo formobjects,Long entityId,String formName,boolean formUI,Session session){
	/**Required variable Initialization */
	log.debug("-------------------inside save MEthod-----------------------------");
	FormResponsesPojo finalFormObject = new FormResponsesPojo();
	String sql = null;
	Long modifiedBy = null;
	FormVersion formversion = new FormVersion();
	FormVersionPojo formVersionPojo = new FormVersionPojo();
	List<FormResponses> formresp = new ArrayList<FormResponses>();
	FormResponses formResponses = null;
	FormResponsesPojo formResponsesPojo2 = null;
	List<FormResponsesPojo> formResponsesPojo = new ArrayList<FormResponsesPojo>();
	List<FormResponsesPojo> formResPojo = new ArrayList<FormResponsesPojo>();
	
	formVersionPojo = formobjects.getFormVersionPojo();
	formResponsesPojo = formobjects.getFormresponsepojoList();
	Auditable baseObject = null;
	if(formUI){
		baseObject = new Auditable();
		baseObject = (Auditable) new VelosUtil().setAuditableInfo(baseObject);
		log.debug("User id c::"+baseObject.getCreatedBy());
		log.debug("User id M::"+baseObject.getLastModifiedBy());
	}
	boolean sessionFlag = false;
 	try {
 			/**For Form Version Table Auditrail Information*/
 			formversion = (FormVersion) ObjectTransfer.transferObjects(formVersionPojo,formversion);
			if(formUI){
				UserJB userObject = new VelosUtil().getUserDetails();
				if(userObject != null){ 
					modifiedBy = ((Integer)userObject.getUserId()).longValue();				
				}
			}
			else{
				modifiedBy = formobjects.getLastModifiedBy();
			}
		if (session == null) {
			// create new session
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			sessionFlag = true;
		}
		/**First Updating the Old Form Version with Void Flag*/
			sql = "update cb_form_version set void_flag = 1,void_date = sysdate,LAST_MODIFIED_DATE=sysdate,LAST_MODIFIED_BY ="+modifiedBy+" where fk_form in (select pk_form from cb_forms where forms_desc = '"+formName+"') and entity_id ="+entityId+" and void_flag is null";
			SQLQuery query = session.createSQLQuery(sql);
			int status = query.executeUpdate();
		
		/**Second Saving the data to Form Version Table*/
		if(formUI)
			//formversion = (FormVersion) new VelosUtil().setAuditableInfo(formversion);
			formversion = (FormVersion) ObjectTransfer.transferObjects(baseObject, formversion);
		else{
			formversion.setCreatedBy(formobjects.getCreatedBy());
			formversion.setCreatedOn(formobjects.getCreatedOn());
		}
		if (formversion != null && !formversion.equals("")) {
			if (formversion.getPkformversion() != null) {
				session.merge(formversion);
			} else {
				session.save(formversion);
			}
		}
		
		if (formversion  != null && !formversion.equals("")) {
			formVersionPojo = new FormVersionPojo();
			formVersionPojo = (FormVersionPojo) ObjectTransfer.transferObjects(formversion, formVersionPojo);
		}
		
			/**Third Saving the data to Form Response Table*/
				for (FormResponsesPojo formResPojo1 : formResponsesPojo) {
					if(formVersionPojo!=null && !formVersionPojo.equals("")){
						formResPojo1.setFkformversion(formVersionPojo.getPkformversion());
						formResPojo.add(formResPojo1);
					}
				}
				
				for (FormResponsesPojo formResponsesPojo3 : formResPojo) {
					formResponses = new FormResponses();
					formResponses = (FormResponses) ObjectTransfer.transferObjects(formResponsesPojo3,formResponses);
					if(formUI)
						//formResponses = (FormResponses) new VelosUtil().setAuditableInfo(formResponses);
						formResponses = (FormResponses) ObjectTransfer.transferObjects(baseObject,formResponses);
					else{
						formResponses.setCreatedBy(formobjects.getCreatedBy());
						formResponses.setCreatedOn(formobjects.getCreatedOn());
					}
					formresp.add(formResponses);
				}
				//System.out.println("formresp:::"+formresp.size());
				for (FormResponses p : formresp) {
					session.save(p);
				}
			/**End*/
				
			if (sessionFlag) {
				session.getTransaction().commit();
			}
			if (formresp != null && formresp.size() > 0) {
				formResponsesPojo.clear();
				for (FormResponses pl : formresp) {
					formResponsesPojo2 = new FormResponsesPojo();
					formResponsesPojo2 = (FormResponsesPojo) ObjectTransfer.transferObjects(pl, formResponsesPojo2);
					formResponsesPojo.add(formResponsesPojo2);
				}
			}
			finalFormObject.setFormVersionPojo(formVersionPojo);
			finalFormObject.setFormresponsepojoList(formResponsesPojo);
			log.debug("-------------------Exit save MEthod-----------------------------");
	}catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		log.debug("Exception While Saving Form Data:::"+e);
	}finally {
		if (session!=null && session.isOpen()) {
			session.close();
		}
	}
	return finalFormObject;
}
}
