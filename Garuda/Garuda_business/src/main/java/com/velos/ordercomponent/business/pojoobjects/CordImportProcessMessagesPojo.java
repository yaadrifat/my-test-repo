package com.velos.ordercomponent.business.pojoobjects;

import com.velos.ordercomponent.business.domain.Auditable;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class CordImportProcessMessagesPojo extends AuditablePojo {

	
	private Long pkCordImportProcessMessage;
	private Long messageId;
	private String messageType;
	private String messageText;
	private Integer level;
	private String cbuRegistryId;
	private Long fkCordImportHistory;
	private Long fkCordId;
	private Long cordImportProgress;
	private Boolean submitted;
	
	
	public Long getCordImportProgress() {
		return cordImportProgress;
	}
	public void setCordImportProgress(Long cordImportProgress) {
		this.cordImportProgress = cordImportProgress;
	}
	public Long getPkCordImportProcessMessage() {
		return pkCordImportProcessMessage;
	}
	public void setPkCordImportProcessMessage(Long pkCordImportProcessMessage) {
		this.pkCordImportProcessMessage = pkCordImportProcessMessage;
	}
	public Long getMessageId() {
		return messageId;
	}
	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getMessageText() {
		return messageText;
	}
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}
	public String getCbuRegistryId() {
		return cbuRegistryId;
	}
	public void setCbuRegistryId(String cbuRegistryId) {
		this.cbuRegistryId = cbuRegistryId;
	}
	public Long getFkCordImportHistory() {
		return fkCordImportHistory;
	}
	public void setFkCordImportHistory(Long fkCordImportHistory) {
		this.fkCordImportHistory = fkCordImportHistory;
	}
	public Integer getLevel() {
		return level;
	}
	public void setLevel(Integer level) {
		this.level = level;
	}
	public Long getFkCordId() {
		return fkCordId;
	}
	public void setFkCordId(Long fkCordId) {
		this.fkCordId = fkCordId;
	}
	public Boolean getSubmitted() {
		return submitted;
	}
	public void setSubmitted(Boolean submitted) {
		this.submitted = submitted;
	}
	
}
