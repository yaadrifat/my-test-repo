package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

public class IDMPojo {
	private Long pkidm;
	private Long fktestgrp;
	private Long fkspecimen;
	private Long idmfilledby;
	private Date idmfilleddate;
	private Date bloodcdate;
	private Long cmsapprovedlab;
	private Long fdalicensedlab;
	private String deletedflag;
	private String bloodcdatestr;
	private Long fktestsource;
	

	public Long getPkidm() {
		return pkidm;
	}
	public void setPkidm(Long pkidm) {
		this.pkidm = pkidm;
	}
	public Long getFktestgrp() {
		return fktestgrp;
	}
	public void setFktestgrp(Long fktestgrp) {
		this.fktestgrp = fktestgrp;
	}
	public Long getFkspecimen() {
		return fkspecimen;
	}
	public void setFkspecimen(Long fkspecimen) {
		this.fkspecimen = fkspecimen;
	}
	public Long getIdmfilledby() {
		return idmfilledby;
	}
	public void setIdmfilledby(Long idmfilledby) {
		this.idmfilledby = idmfilledby;
	}
	public Date getIdmfilleddate() {
		return idmfilleddate;
	}
	public void setIdmfilleddate(Date idmfilleddate) {
		this.idmfilleddate = idmfilleddate;
	}
	public Date getBloodcdate() {
		return bloodcdate;
	}
	public void setBloodcdate(Date bloodcdate) {
		this.bloodcdate = bloodcdate;
	}
	public Long getCmsapprovedlab() {
		return cmsapprovedlab;
	}
	public void setCmsapprovedlab(Long cmsapprovedlab) {
		this.cmsapprovedlab = cmsapprovedlab;
	}
	public Long getFdalicensedlab() {
		return fdalicensedlab;
	}
	public void setFdalicensedlab(Long fdalicensedlab) {
		this.fdalicensedlab = fdalicensedlab;
	}
	public String getDeletedflag() {
		return deletedflag;
	}
	public void setDeletedflag(String deletedflag) {
		this.deletedflag = deletedflag;
	}
	public String getBloodcdatestr() {
		return bloodcdatestr;
	}
	public void setBloodcdatestr(String bloodcdatestr) {
		this.bloodcdatestr = bloodcdatestr;
	}
	public Long getFktestsource() {
		return fktestsource;
	}
	public void setFktestsource(Long fktestsource) {
		this.fktestsource = fktestsource;
	}
}
