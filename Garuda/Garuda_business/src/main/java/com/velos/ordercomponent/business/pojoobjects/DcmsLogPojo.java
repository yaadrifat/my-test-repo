package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

public class DcmsLogPojo {
	
	private Long dcmsLogId;
	private Long userId;
	private Long attachmentId;
	private String task;
	private Date taskDate;
	
	
	public Long getDcmsLogId() {
		return dcmsLogId;
	}
	public void setDcmsLogId(Long dcmsLogId) {
		this.dcmsLogId = dcmsLogId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getAttachmentId() {
		return attachmentId;
	}
	public void setAttachmentId(Long attachmentId) {
		this.attachmentId = attachmentId;
	}
	public String getTask() {
		return task;
	}
	public void setTask(String task) {
		this.task = task;
	}
	public Date getTaskDate() {
		return taskDate;
	}
	public void setTaskDate(Date taskDate) {
		this.taskDate = taskDate;
	}
	
	

}
