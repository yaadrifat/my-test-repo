package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;

public class OtherSeriousDiseases {
	//private String inher_other_dises_ind;
	private String Description;

//	@XmlElement(name="inher_other_dises_ind")
	///public String getInher_other_dises_ind() {
	///	return inher_other_dises_ind;
	//}

	//public void setInher_other_dises_ind(String inherOtherDisesInd) {
	//	inher_other_dises_ind = inherOtherDisesInd;
	//}

	@XmlElement(name="Description")
	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}
	
	

}
