package com.velos.ordercomponent.business.xmlpojos;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class OtherBloodDiseases {

	private String inher_other_bld_dises_ind;
	/*private Relations relations;
	@XmlElement(name="Relations")
	public Relations getRelations() {
		return relations;
	}
	public void setRelations(Relations relations) {
		this.relations = relations;
	}*/
	@XmlElement(name="inher_other_bld_dises_ind")
	public String getInher_other_bld_dises_ind() {
		return inher_other_bld_dises_ind;
	}
	public void setInher_other_bld_dises_ind(String inherOtherBldDisesInd) {
		inher_other_bld_dises_ind = inherOtherBldDisesInd;
	}
	private List<Relations> relations;
	@XmlElement(name="Relations")
	public List<Relations> getRelations() {
		return relations;
	}
	public void setRelations(List<Relations> relations) {
		this.relations = relations;
	}
	
		 
}
