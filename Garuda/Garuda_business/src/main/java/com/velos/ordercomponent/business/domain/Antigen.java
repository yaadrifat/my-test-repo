package com.velos.ordercomponent.business.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
@Entity
@Table(name="CB_ANTIGEN")
@SequenceGenerator(name="SEQ_CB_ANTIGEN",sequenceName="SEQ_CB_ANTIGEN",allocationSize=1)
public class Antigen extends Auditable {
	
	private Long pkAntigen;
	private Long antigenId;
	private String HlaCode;
	private String HlaMethod;
	private Boolean validInd;
	private Boolean activeInd;
	private Long altAntigenId;
	private String lastUpdateSource;
	private String createdBySource;
	private Set<AntigenEncoding> encodings;
	
	@Id
	@Column(name="PK_ANTIGEN")
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_ANTIGEN")
	public Long getPkAntigen() {
		return pkAntigen;
	}
	public void setPkAntigen(Long pkAntigen) {
		this.pkAntigen = pkAntigen;
	}
	@Column(name="ANTIGEN_ID")
	public Long getAntigenId() {
		return antigenId;
	}
	public void setAntigenId(Long antigenId) {
		this.antigenId = antigenId;
	}	
	@Column(name="HLA_LOCUS")
	public String getHlaCode() {
		return HlaCode;
	}
	public void setHlaCode(String hlaCode) {
		HlaCode = hlaCode;
	}
	@Column(name="HLA_TYPING_METHOD")
	public String getHlaMethod() {
		return HlaMethod;
	}
	public void setHlaMethod(String hlaMethod) {
		HlaMethod = hlaMethod;
	}
	@Column(name="VALID_IND")
	public Boolean getValidInd() {
		return validInd;
	}
	public void setValidInd(Boolean validInd) {
		this.validInd = validInd;
	}
	@Column(name="ACTIVE_IND")
	public Boolean getActiveInd() {
		return activeInd;
	}
	public void setActiveInd(Boolean activeInd) {
		this.activeInd = activeInd;
	}
	
	@Column(name="ALT_ANTIGEN_ID")
	public Long getAltAntigenId() {
		return altAntigenId;
	}
	public void setAltAntigenId(Long altAntigenId) {
		this.altAntigenId = altAntigenId;
	}
	@Column(name="LAST_UPDATED_SOURCE")
	public String getLastUpdateSource() {
		return lastUpdateSource;
	}
	public void setLastUpdateSource(String lastUpdateSource) {
		this.lastUpdateSource = lastUpdateSource;
	}
	@Column(name="CREATED_BY_SOURCE")
	public String getCreatedBySource() {
		return createdBySource;
	}
	public void setCreatedBySource(String createdBySource) {
		this.createdBySource = createdBySource;
	}
	@OneToMany(cascade=CascadeType.ALL)
	@JoinTable(name="AntigenEncoding",joinColumns={@JoinColumn(name="pkAntigen")},inverseJoinColumns={@JoinColumn(name="fkAntigen")})
	public Set<AntigenEncoding> getEncodings() {
		return encodings;
	}
	public void setEncodings(Set<AntigenEncoding> encodings) {
		this.encodings = encodings;
	}
	
	
	
}
