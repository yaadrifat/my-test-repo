/**
 * 
 */
package com.velos.ordercomponent.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.swing.text.html.parser.Entity;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.hibernate.SQLQuery;
import org.hibernate.classic.Session;

import com.opensymphony.xwork2.ActionContext;
import com.velos.eres.web.user.UserJB;
import com.velos.ordercomponent.business.domain.CBUStatus;
import com.velos.ordercomponent.business.domain.CodeList;
import com.velos.ordercomponent.business.util.HibernateUtil;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.VelosMidConstants;
import com.velos.ordercomponent.exception.MidErrorConstants;


public class CodelstHelper {

	public static final Log log = LogFactory.getLog(WidgetHelper.class);

	
	public static List<CodeList> getCodelst(String type){
		List<CodeList> lstcodeList = new ArrayList<CodeList>();
		Session session = null;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			
			lstcodeList = (List<CodeList>)session.createQuery("from CodeList where type='"+type+"'").list();
			
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		
		return lstcodeList;
	}
	
		//log.debug("Codelst Helper : getCodelistValue Method Start ");
	public static Map<Object, List> getCodelistValues(Map<String,String> codelstParams){
		Map<Object,List> result = new HashMap<Object, List>();
		List lstcodeList ;
//		List objectList ; 
		String[] fields =  {"pkCodeId","description"};
		Session session = null;
		try {
			//log.debug("inside codelstHelper");
			 session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			//log.debug("codelstParam-->"+codelstParams);
			if(codelstParams != null && !codelstParams.isEmpty()){
				Set set = codelstParams.entrySet();
				// Get an iterator
				Iterator i = set.iterator();
				// Display elements
				while(i.hasNext()) {
					Map.Entry me = (Map.Entry)i.next();
					//log.debug(me.getKey() + ":==================  " + me.getValue()+"******");
					
					if(me.getValue() != null && !((String)me.getValue()).equals("CdrCbu")){
						lstcodeList = (List<CodeList>)session.createQuery("from CodeList where type='"+me.getValue()+"'").list();
					}else{
						//lstcodeList = (List<CodeList>)session.createQuery("from CdrCbu where (deletedFlag = 0 or deletedFlag is null)" ).list();
						//lstcodeList = (List<CodeList>)session.createQuery("from CodeList where type='CLINICAL' and subType='SUBCLINICAL' and hide = 1").list();
					lstcodeList = (List<CodeList>)session.createQuery("from CdrCbu").list();
					}
				//objectList = (List)ObjectTransfer.transferObjectListToList(lstcodeList, fields);
					result.put(me.getKey(), lstcodeList);
				}
				
			}
			
			
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		
		//log.debug("Codelst Helper : getCodelistValue Method End ");
		return result;
	}
	
	public static CodeList getCodeListByCode(String codetype,String codesubtype )
	{
		return getCodeListByCode(codetype, codesubtype, null);
	}
	
	public static CodeList getCodeListByCode(String codetype,String codesubtype , Session session)
	{
		boolean sessionFlag = false;
		CodeList codeList  = null;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "select {cl.*} from er_codelst cl where cl.codelst_type=? and cl.codelst_subtyp=?";
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity("cl", CodeList.class);
			query.setString(0, codetype);
			query.setString(1, codesubtype);
			codeList = (CodeList)query.list().get(0);
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
       return codeList;
	}
	
	
	public static List<CodeList> getCodeListValues(){
		
		 return getCodeListValues(null);	
		 
	}		
		
	public static List<CodeList> getCodeListValues(Session session){
			//log.debug("getCodeListValues method start in WidgetHelper");		
			List<CodeList> codeListValues = null;
			try {
				// check session
				if (session == null) {
					// create new session
					session = HibernateUtil.getSessionFactory().getCurrentSession();
					session.beginTransaction();
				}
				String sql = "select {g.*} from er_codelst g where g.codelst_type<>'CODE' and g.codelst_hide='N' order by g.codelst_type,g.codelst_seq ";
				SQLQuery query = session.createSQLQuery(sql);
				query.addEntity("g", CodeList.class);
				codeListValues = (List<CodeList>)query.list();
				//log.debug("Code List Size ==="+codeListValues.size());
			}catch (Exception e) {
				log.error(e.getMessage());
				e.printStackTrace();
			}finally{
				if(session.isOpen()){
					session.close();
				}
			}
			return codeListValues;
	}
	
	
	public static List<CBUStatus> getCBUStatusValues(String codeType){
		
		 return getCBUStatusValues(codeType,null);	
		 
	}		
		
	public static List<CBUStatus> getCBUStatusValues(String codeType,Session session){
			//log.debug("getCBUStatusValues method start in CodelstHelper");		
			List<CBUStatus> cbuStatus = null;
			try {
				// check session
				if (session == null) {
					// create new session
					session = HibernateUtil.getSessionFactory().getCurrentSession();
					session.beginTransaction();
				}
				String sql = "select {s.*} from cb_cbu_status s where s.code_type = '"+codeType+"' and  s.deletedflag is null and s.is_local_reg=1 and s.cbu_status_code not in ('RCT','RHE')";
				if(codeType.equals(VelosMidConstants.RESOLUTION) || codeType.equals(VelosMidConstants.CLOSE_REASON)){
					sql = "select {s.*} from cb_cbu_status s where s.code_type = '"+codeType+"' and  (s.deletedflag is null or s.deletedflag=0) order by s.CBU_STATUS_DESC";
				}
				if(codeType.equals(VelosMidConstants.DEFRED_CORD)){
					sql = "select {s.*} from cb_cbu_status s where s.code_type = '"+VelosMidConstants.RESPONSE+"' and s.IS_AUTOCOMPLETE_STATUS=1 and (s.deletedflag is null or s.deletedflag=0) and s.cbu_status_code not in ('DS','LS','TS','RO') order by s.CBU_STATUS_DESC";
				}
				if(codeType.equals(VelosMidConstants.NATIONAL_STATUS)){
					sql = "select {s.*} from cb_cbu_status s where s.code_type = '"+VelosMidConstants.RESPONSE+"' and s.IS_NATIONAL_REG=1 and (s.deletedflag is null or s.deletedflag=0) order by s.CBU_STATUS_DESC";
				}
				SQLQuery query = session.createSQLQuery(sql);
				query.addEntity("s", CBUStatus.class);
				cbuStatus = (List<CBUStatus>)query.list();
				//log.debug("cbuStatus Size ==="+cbuStatus.size());
			}catch (Exception e) {
				log.error(e.getMessage());
				e.printStackTrace();
			}finally{
				if(session.isOpen()){
					session.close();
				}
			}
			return cbuStatus;
	}
	
	public static List<CodeList> getCodeListInvalidValues(){
		
		 return getCodeListInvalidValues(null);	
		 
	}		
		
	public static List<CodeList> getCodeListInvalidValues(Session session){
			//log.debug("getCodeListValues method start in WidgetHelper");		
			List<CodeList> codeListValues = null;
			try {
				// check session
				if (session == null) {
					// create new session
					session = HibernateUtil.getSessionFactory().getCurrentSession();
					session.beginTransaction();
				}
				String sql = "select {g.*} from er_codelst g where g.codelst_type<>'CODE' and g.codelst_hide='Y' order by g.codelst_type,g.codelst_seq ";
				SQLQuery query = session.createSQLQuery(sql);
				query.addEntity("g", CodeList.class);
				codeListValues = (List<CodeList>)query.list();
				//log.debug("Code List Size ==="+codeListValues.size());
			}catch (Exception e) {
				log.error(e.getMessage());
				e.printStackTrace();
			}finally{
				if(session.isOpen()){
					session.close();
				}
			}
			return codeListValues;
	}
	
	public static Long getCodelstPrimaryId(String parentCode, String code) throws Exception{
		return getCodelstPrimaryId(parentCode, code,null);
	}
	public static Long getCodelstPrimaryId(String parentCode, String code,Session session) throws Exception{
		boolean sessionFlag = false;
		Long pkId = null;
		try {
			 // check session 
			if(session== null){
				// create new session
			  session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}

			List lstcodeList = (List<CodeList>)session.createQuery("select pkCodeId from CodeList where type='"+parentCode+"' and subType = '"+code+"'").list();
			if(lstcodeList != null && !lstcodeList.isEmpty()){
				pkId = (Long)lstcodeList.get(0);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return pkId;
	}

	public static List<CodeList> getCodeListForIds()
	{
		return getCodeListForIds(null);
	}
	
	public static List<CodeList> getCodeListForIds(Session session)
	{
		boolean sessionFlag = false;
		
		List<CodeList> codeList  = null;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "select cl.* from er_codelst cl";
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity("cl", CodeList.class);
			codeList = (List<CodeList>)query.list();		
			
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
      return codeList;
	}
	
	public static Boolean getValidateDbData(String entityName, String entityFiled, String entityValue, String entityField1, String entityValue1){
		return getValidateDbData(entityName, entityFiled, entityValue, entityField1,entityValue1,null);
	}
	
	public static Boolean getValidateDbData(String entityName, String entityFiled, String entityValue, String entityField1, String entityValue1, Session session){
		Long count=null;
		boolean sessionFlag = false;
	    try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}			
			String sql = "select count(*) From "+entityName+" where Replace("+entityFiled+",'-','') ='"+entityValue+"' ";
			if(entityField1!=null && entityValue1!=null){				
				    sql += " and Replace("+entityField1+",'-','')='"+entityValue1+"' ";				
			}			
			count = (Long)session.createQuery(sql).uniqueResult();
			
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		
		return ((count==null || count==0)?true:false); 
	}
	
	public static Boolean getValidateDbLongData(String entityName, String entityFiled, String entityValue, String entityField1, Long entityValue1){
		return getValidateDbLongData(entityName, entityFiled, entityValue, entityField1,entityValue1,null);
	}
	
	public static Boolean getValidateDbLongData(String entityName, String entityFiled, String entityValue, String entityField1, Long entityValue1, Session session){
		Long count=null;
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "select count(*) From "+entityName+" where Replace("+entityFiled+",'-','') ='"+entityValue+"' ";
			if(entityField1!=null && entityValue1!=null){
				sql += " and Replace("+entityField1+",'-','')="+entityValue1;
			}
			//log.debug("url"+sql);
			count = (Long)session.createQuery(sql).uniqueResult();
			
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		/*if(list==null || list.size()==0){
		    return true;	
		}else{
			return false;		
		}*/
		return ((count==null || count==0)?true:false);
	}
	
	public static Boolean getValidateDbLongDataWithAdditionalField(String entityName, String entityFiled, String entityValue, String entityFiled2, String entityValue2, String entityField1, Long entityValue1){
		return getValidateDbLongDataWithAdditionalField(entityName, entityFiled, entityValue,entityFiled2,entityValue2, entityField1,entityValue1,null);
	}
	
	public static Boolean getValidateDbLongDataWithAdditionalField(String entityName, String entityFiled, String entityValue, String entityFiled2, String entityValue2, String entityField1, Long entityValue1, Session session){
		Long count=null;
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "select count(*) From "+entityName+" where Replace("+entityFiled+",'-','') ='"+entityValue+"' ";
			if(entityFiled2!=null && entityValue2!=null){
				sql += " and Replace("+entityFiled2+",'-','')='"+entityValue2+"'";
			}
			if(entityField1!=null && entityValue1!=null){
				sql += " and Replace("+entityField1+",'-','')="+entityValue1;
			}
			//log.debug("url"+sql);
			count = (Long)session.createQuery(sql).uniqueResult();
			
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		/*if(list==null || list.size()==0){
		    return true;	
		}else{
			return false;		
		}*/
		return ((count==null || count==0))?true:false;
	}
	
	public static List<String> getCodelstCustCols(Long pkCodelst){
		return getCodelstCustCols(pkCodelst, null);
	}
	
	public static List<String> getCodelstCustCols(Long pkCodelst,Session session){
		List<String> lst=new ArrayList<String>();
		try{
			if(session==null){
				session=HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			String qry="";
			qry="select codelst_custom_col,codelst_custom_col1 from er_codelst where pk_codelst="+pkCodelst;
			//log.debug("codelstcustcols qry is:::::"+qry);
			lst=(List<String>)session.createSQLQuery(qry).list();
			//log.debug("codelstcustcols list length is:::"+lst.size());
		}catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return lst;
	}
	
}

