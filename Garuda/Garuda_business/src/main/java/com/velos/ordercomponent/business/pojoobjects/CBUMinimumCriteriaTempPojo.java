package com.velos.ordercomponent.business.pojoobjects;

public class CBUMinimumCriteriaTempPojo extends AuditablePojo{
	
	private Long pkMinimumCritField;		//PK_CORD_MINIMUM_CRITERIA_FIELD
	private Long fkMinimumCrit;				//FK_CORD_MINIMUM_CRITERIA
	private Long bacterialResult;			//BACTERIAL_RESULT
	private Long fungalResult;				//FUNGAL_RESULT
	private Long hemoglobresult;			//HEMOGLOB_RESULT
	private Long hivresult;					//HIVI_II_RESULT
	private Long hivp;						//HIVP_RESULT
	private Long hbsag;						//HBSAG_RESULT
	private Long hbvnat;					//HBVNAT_RESULT
	private Long hcv;						//HCV_RESULT
	private Long tcruzichages;				//T_CRUZI_CHAGAS_RESULT
	private Long wnc;						//WNV_RESULT
	private String tncptwg;					//TNCKG_PT_WEIGHT_RESULT
	private Long viabresult;				//VIABILITY_RESULT
	
	
	public Long getPkMinimumCritField() {
		return pkMinimumCritField;
	}
	public void setPkMinimumCritField(Long pkMinimumCritField) {
		this.pkMinimumCritField = pkMinimumCritField;
	}
	public Long getFkMinimumCrit() {
		return fkMinimumCrit;
	}
	public void setFkMinimumCrit(Long fkMinimumCrit) {
		this.fkMinimumCrit = fkMinimumCrit;
	}
	public Long getBacterialResult() {
		return bacterialResult;
	}
	public void setBacterialResult(Long bacterialResult) {
		this.bacterialResult = bacterialResult;
	}
	public Long getFungalResult() {
		return fungalResult;
	}
	public void setFungalResult(Long fungalResult) {
		this.fungalResult = fungalResult;
	}
	public Long getHemoglobresult() {
		return hemoglobresult;
	}
	public void setHemoglobresult(Long hemoglobresult) {
		this.hemoglobresult = hemoglobresult;
	}
	public Long getHivresult() {
		return hivresult;
	}
	public void setHivresult(Long hivresult) {
		this.hivresult = hivresult;
	}
	public Long getHivp() {
		return hivp;
	}
	public void setHivp(Long hivp) {
		this.hivp = hivp;
	}
	public Long getHbsag() {
		return hbsag;
	}
	public void setHbsag(Long hbsag) {
		this.hbsag = hbsag;
	}
	public Long getHbvnat() {
		return hbvnat;
	}
	public void setHbvnat(Long hbvnat) {
		this.hbvnat = hbvnat;
	}
	public Long getHcv() {
		return hcv;
	}
	public void setHcv(Long hcv) {
		this.hcv = hcv;
	}
	public Long getTcruzichages() {
		return tcruzichages;
	}
	public void setTcruzichages(Long tcruzichages) {
		this.tcruzichages = tcruzichages;
	}
	public Long getWnc() {
		return wnc;
	}
	public void setWnc(Long wnc) {
		this.wnc = wnc;
	}
	public String getTncptwg() {
		return tncptwg;
	}
	public void setTncptwg(String tncptwg) {
		this.tncptwg = tncptwg;
	}
	public Long getViabresult() {
		return viabresult;
	}
	public void setViabresult(Long viabresult) {
		this.viabresult = viabresult;
	}
}
