package com.velos.ordercomponent.business.pojoobjects;

import java.sql.Blob;

/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class EmailAttachmentPojo {

	private Long emailDocumentId; 
	private Long emailDoc; 
	private String emailDocType; 
	private String emailDocName; 
	private Long entityId; 
	private String entityType;
	private Blob emailDocFile;
	
	public Long getEmailDocumentId() {
		return emailDocumentId;
	}
	public void setEmailDocumentId(Long emailDocumentId) {
		this.emailDocumentId = emailDocumentId;
	}
	public Long getEmailDoc() {
		return emailDoc;
	}
	public void setEmailDoc(Long emailDoc) {
		this.emailDoc = emailDoc;
	}
	public String getEmailDocType() {
		return emailDocType;
	}
	public void setEmailDocType(String emailDocType) {
		this.emailDocType = emailDocType;
	}
	public String getEmailDocName() {
		return emailDocName;
	}
	public void setEmailDocName(String emailDocName) {
		this.emailDocName = emailDocName;
	}
	public Long getEntityId() {
		return entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	public String getEntityType() {
		return entityType;
	}
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}
	public Blob getEmailDocFile() {
		return emailDocFile;
	}
	public void setEmailDocFile(Blob emailDocFile) {
		this.emailDocFile = emailDocFile;
	}
	
	
}
