package com.velos.ordercomponent.helper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.opensymphony.xwork2.ActionContext;
import com.velos.eres.web.user.UserJB;
import com.velos.ordercomponent.business.domain.AdditionalIds;
import com.velos.ordercomponent.business.domain.Antigen;
import com.velos.ordercomponent.business.domain.AntigenEncoding;
import com.velos.ordercomponent.business.domain.Assessment;
import com.velos.ordercomponent.business.domain.Attachment;
import com.velos.ordercomponent.business.domain.BestHLA;
import com.velos.ordercomponent.business.domain.CBUCompleteReqInfo;
import com.velos.ordercomponent.business.domain.CBUFinalReview;
import com.velos.ordercomponent.business.domain.CBUMinimumCriteria;
import com.velos.ordercomponent.business.domain.CBUMinimumCriteriaTemp;
import com.velos.ordercomponent.business.domain.CBUploadInfo;
import com.velos.ordercomponent.business.domain.CdrCbu;
import com.velos.ordercomponent.business.domain.CodeList;
import com.velos.ordercomponent.business.domain.Dumn;
import com.velos.ordercomponent.business.domain.EligibilityDecl;
import com.velos.ordercomponent.business.domain.EligibilityNewDecl;
import com.velos.ordercomponent.business.domain.EntityMultipleValues;
import com.velos.ordercomponent.business.domain.EntityStatus;
import com.velos.ordercomponent.business.domain.EntityStatusReason;
import com.velos.ordercomponent.business.domain.FinalRevUploadInfo;
import com.velos.ordercomponent.business.domain.HLA;
import com.velos.ordercomponent.business.domain.IDM;
import com.velos.ordercomponent.business.domain.PatLabs;
import com.velos.ordercomponent.business.domain.PatientHLA;
import com.velos.ordercomponent.business.domain.RecipientInfo;
import com.velos.ordercomponent.business.domain.Site;
import com.velos.ordercomponent.business.domain.TempHLA;
import com.velos.ordercomponent.business.domain.orderHeader;
import com.velos.ordercomponent.business.pojoobjects.AdditionalIdsPojo;
import com.velos.ordercomponent.business.pojoobjects.AntigenPojo;
import com.velos.ordercomponent.business.pojoobjects.BestHLAPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUCompleteReqInfoPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUFinalReviewPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUMinimumCriteriaPojo;
import com.velos.ordercomponent.business.pojoobjects.CBUMinimumCriteriaTempPojo;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.DumnPojo;
import com.velos.ordercomponent.business.pojoobjects.EligibilityDeclPojo;
import com.velos.ordercomponent.business.pojoobjects.EligibilityNewDeclPojo;
import com.velos.ordercomponent.business.pojoobjects.EntityMultipleValuesPojo;
import com.velos.ordercomponent.business.pojoobjects.EntityStatusPojo;
import com.velos.ordercomponent.business.pojoobjects.EntityStatusReasonPojo;
import com.velos.ordercomponent.business.pojoobjects.FinalRevUploadInfoPojo;
import com.velos.ordercomponent.business.pojoobjects.HLAPojo;
import com.velos.ordercomponent.business.pojoobjects.IDMPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.pojoobjects.PatLabsPojo;
import com.velos.ordercomponent.business.pojoobjects.PatientHLAPojo;
import com.velos.ordercomponent.business.pojoobjects.SitePojo;
import com.velos.ordercomponent.business.pojoobjects.TempHLAPojo;
import com.velos.ordercomponent.business.util.HibernateUtil;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.VelosMidConstants;
import com.velos.ordercomponent.business.util.VelosUtil;
import com.velos.ordercomponent.exception.MidErrorConstants;

public class CBUHelper {
	public static final Log log = LogFactory.getLog(CBUHelper.class);
	
	private static UserJB userObject = null;	
	
	public static List<Object> getCBUCordInfoList(String objectName,
			String criteria,PaginateSearch paginateSearch) {
		return getCBUCordInfoList(objectName, criteria,paginateSearch, null);
	}

	public static List<Object> getCBUCordInfoList(String objectName,
			String criteria,PaginateSearch paginateSearch, Session session) {
		boolean sessionFlag = false;
		List<Object> returnList = null;
		try {
			// check session
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = " from " + objectName
					+ " where (deletedFlag = 0 or deletedFlag is null)";
			if (criteria != null && !criteria.trim().equals("")) {
				sql += criteria;
			}
			Query query = session.createQuery(sql);
			if(paginateSearch!=null){
				query.setFirstResult(paginateSearch.getiPageNo());
				query.setMaxResults(paginateSearch.getiShowRows());
			}
			//log.debug(" trace query " + query);
			returnList = (List<Object>) query.list();

//			if (sessionFlag) {
//				 close session, if session created in this method
//				 session.getTransaction().commit();
//			}

		} catch (Exception e) {
			//log.debug(" Exception in getObjectList " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}

		return returnList;
	}
	
	public static List<Object> getCbuInfoById(String entityType, String entityValue){
		return getCbuInfoById(entityType, entityValue, null);
	}
	
	public static List<Object> getCbuInfoById(String entityType,
			String entityValue, Session session) {
		List<Object> returnList = null;
        boolean sessionFlag = false;
    	try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "From CdrCbu where lower("+entityType+")=lower('"+entityValue.trim()+"')";
			returnList = (List<Object>) session.createQuery(sql).list();
			
			
    	} catch (Exception e) {
			//log.debug(" Exception in getCbuInfoById " + e);
    		log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return returnList;
	}

	public static CdrCbuPojo updateCodeList(CdrCbuPojo cdrCbuPojo) {

		CdrCbu cdrcbu = new CdrCbu();

		try {
			cdrcbu = (CdrCbu) ObjectTransfer
					.transferObjects(cdrCbuPojo, cdrcbu);
			Object object = new VelosUtil().setAuditableInfo(cdrcbu);
			cdrcbu = updateCodeListStatus((CdrCbu) object);
			cdrCbuPojo = (CdrCbuPojo) ObjectTransfer.transferObjects(cdrcbu,
					cdrCbuPojo);

		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}

		return cdrCbuPojo;
	}

	public static CdrCbu updateCodeListStatus(CdrCbu cdrcbu) {
		//log.debug(" helper without session");
		return updateCodeListStatus(cdrcbu, null);
	}

	public static CdrCbu updateCodeListStatus(CdrCbu cdrCbu, Session session) {

		boolean sessionFlag = false;
		try {
			// check session
			//log.debug("session " + session);
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			// //log.debug("Helper : " +
			// cbuUnitReportTemp.getPkCordExtInfo());
			// //log.debug("=========="+cdrCbu.getCordID());
			if (cdrCbu.getCordID() != null && cdrCbu.getCordID() > 0) {


				//log.debug("inside update cordid");

				//System.out.println("inside update cordid");
				if(cdrCbu.getFkCbbId()!=null && cdrCbu.getFkCbbId()!=-1){
					Site s = new Site();
					s.setSiteId(cdrCbu.getFkCbbId());
					s = (Site)session.get(Site.class,s.getSiteId());
					cdrCbu.setSite(s);
				}

				session.merge(cdrCbu.getSpecimen());
				session.merge(cdrCbu);			
			}

			if (sessionFlag) {
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			//log.debug(" Exception in CBU Unit Report " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			//log.debug(" session in finally " + session + " /"				+ sessionFlag);
			if (session.isOpen()) {
				session.close();
			}
		}
		return cdrCbu;
	}

	public static CdrCbuPojo getCordInfoById(CdrCbuPojo cdrCbuPojo) {
		return getCordInfoById(cdrCbuPojo, null);
	}

	public static CdrCbuPojo getCordInfoById(CdrCbuPojo cdrCbuPojo,
			Session session) {
		boolean sessionFlag = false;
		CdrCbu cdrCbu = new CdrCbu();
		if(cdrCbuPojo.getCordID()!=null)
			 cdrCbu.setCordID(cdrCbuPojo.getCordID());
		try {
			// check session
			//log.debug("session " + session);
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			// //log.debug("Helper : " +
			// cbuUnitReportTemp.getPkCordExtInfo());
			// //log.debug("=========="+cdrCbu.getCordID());
			if (cdrCbu.getCordID() != null && cdrCbu.getCordID() > 0) {

				// //log.debug("inside update");
				/*
				 * 
				 * Below commented code because of load the cord from cache
				 */
				/*String sql=" From CdrCbu where cordID="+cdrCbu.getCordID();
				List<CdrCbu> cdrCbulist = (List<CdrCbu>) session.createQuery(sql).list();
						//.load(CdrCbu.class, cdrCbu.getCordID());
				for(CdrCbu cdrcbu: cdrCbulist){
				cdrCbuPojo = (CdrCbuPojo) ObjectTransfer.transferObjects(
						cdrcbu, cdrCbuPojo);
				}*/
				cdrCbu = (CdrCbu) session.get(CdrCbu.class,cdrCbu.getCordID());
				cdrCbuPojo = (CdrCbuPojo) ObjectTransfer.transferObjects(
						cdrCbu, cdrCbuPojo);
			}

			/*
			 * if (sessionFlag) { session.getTransaction().commit(); }
			 */
		} catch (Exception e) {
			//log.debug(" Exception in CBU Unit Report " + e);
			e.printStackTrace();
			log.error(e.getMessage());
		} finally {
			//log.debug(" session in finally " + session + " /"					+ sessionFlag);
			if (session.isOpen()) {
				session.close();
			}
		}
		return cdrCbuPojo;
	}

	@SuppressWarnings("unchecked")
	public static List<Object> getCbuinfo(String criteria,PaginateSearch paginateSearch,int i) {
		String query = "";	
		ActionContext ac = ActionContext.getContext();
		HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
		ResourceBundle resource = ResourceBundle.getBundle(VelosMidConstants.EXCEPTION_PROPERTYFILE_NAME);
		HttpSession session = request.getSession(true);
		if(session != null){
			userObject=(UserJB)session.getAttribute(MidErrorConstants.GARUDA_USER);
		}		
		 //log.debug("Uservalue==========>>>>>>>>"+((Integer)userObject.getUserId()).longValue());		 
		if (criteria.equalsIgnoreCase(" ".trim())) {

			query = "SELECT distinct cinfo.pk_cord,cinfo.cord_registry_id, cinfo.registry_maternal_id, cinfo.cord_local_cbu_id, cinfo.maternal_local_id, " +
					" cinfo.cord_isbi_din_code, NVL(cbb.site_name,'Not Available'),  NVL(pinfo.receipant_id,'Not Available'),  " +
					" cinfo.fk_cbb_id from cb_cord cinfo  left outer join er_site cbb on(cinfo.fk_cbb_id =cbb.pk_site)  " +
					" left outer join er_specimen es on(cinfo.fk_specimen_id=es.pk_specimen)  " +
					" left outer join er_order_receipant_info pre on(pre.fk_cord_id  = cinfo.pk_cord)  " +
					" left outer join cb_receipant_info pinfo on(pinfo.pk_receipant  = pre.fk_receipant)  " +
					" left outer join er_order ord on(ord.pk_order=pre.fk_order_id) WHERE cinfo.fk_cbb_id IN  " +
					" (SELECT fk_site  FROM er_usersite  WHERE er_usersite.fk_user ="+((Integer) userObject.getUserId()).longValue()+"  AND er_usersite.usersite_right<>0  ) " +
					"  ORDER BY cinfo.cord_registry_id";
		} else{
			query = "SELECT distinct cinfo.pk_cord,cinfo.cord_registry_id, cinfo.registry_maternal_id, cinfo.cord_local_cbu_id, cinfo.maternal_local_id, " +
			" cinfo.cord_isbi_din_code, NVL(cbb.site_name,'Not Available'),  NVL(pinfo.receipant_id,'Not Available'),  " +
			" cinfo.fk_cbb_id from cb_cord cinfo  left outer join er_site cbb on(cinfo.fk_cbb_id =cbb.pk_site)  " +
			" left outer join er_specimen es on(cinfo.fk_specimen_id=es.pk_specimen)  " +
			" left outer join er_order_receipant_info pre on(pre.fk_cord_id  = cinfo.pk_cord)  " +
			" left outer join cb_receipant_info pinfo on(pinfo.pk_receipant  = pre.fk_receipant)  " +
			" left outer join er_order ord on(ord.pk_order=pre.fk_order_id) WHERE ("+ criteria +") and cinfo.fk_cbb_id IN  " +
			" (SELECT fk_site  FROM er_usersite  WHERE er_usersite.fk_user ="+((Integer) userObject.getUserId()).longValue()+"  AND er_usersite.usersite_right<>0  ) " +
			" ORDER BY cinfo.cord_registry_id";
		}
		//log.debug("-----query---" + query);

		return velosHelper.getListObject(query,paginateSearch,i);
	}

	public static List<Object> getListCdrCBB(String criteria,PaginateSearch paginateSearch,int i) {
		String query = "";
		Long cordID = Long.parseLong(criteria);
		query = "select cdrcbu.CORD_REGISTRY_ID, cdrcbu.cord_cbu_collection_date, cdrcbu.cord_isbi_din_code, nvl(site.site_name,'Not Available'), "
				+ "cdrcbu.cord_isit_product_code, cdrcbu.cord_tnc_frozen, cdrcbu.cord_id_number_on_cbu_bag,  cdrcbu.fk_cord_cbu_lic_status,  "
				+ "cdrcbu.fk_cord_cbu_eligible_status,  cdrcbu.FK_CORD_CBU_INELIGIBLE_REASON,  cdrcbu.FK_CORD_CBU_UNLICENSED_REASON,  "
				+ "cdrcbu.cord_local_cbu_id ,cdrcbu.PK_CORD from  cb_cord cdrcbu left outer join ER_SITE site on (cdrcbu.fk_cbb_id = site.pk_site) "
				+ " where cdrcbu.PK_CORD ="
				+ cordID
				+ " and (cdrcbu.CORD_SEARCHABLE=1)";

		/*
		 * query=
		 * "select cdrcbu.CORD_REGISTRY_ID,cdrcbu.cord_cbu_collection_date,cdrcbu.cord_isbi_din_code, "
		 * +
		 * " nvl(site.site_name,'Not Available'),cdrcbu.cord_isit_product_code,cdrcbu.cord_tnc_frozen, "
		 * +
		 * "cdrcbu.cord_id_number_on_cbu_bag,cdrcbu.fk_cord_cbu_lic_status,cdrcbu.fk_cord_cbu_eligible_status, "
		 * +
		 * "cdrcbu.FK_CORD_CBU_INELIGIBLE_REASON,cdrcbu.FK_CORD_CBU_UNLICENSED_REASON,cdrcbu.cord_local_cbu_id"
		 * +
		 * " from  cb_cord cdrcbu, cbb cbb left outer join (ER_SITE site ) on( cbb.fk_site = site.pk_site) where "
		 * + "cdrcbu.fk_cbb_id = cbb.pk_cbb and cdrcbu.CORD_REGISTRY_ID ='"+
		 * criteria + "'";
		 */

		return velosHelper.getListObject(query,paginateSearch,i);

	}

	public static List<Object> getHla(Long cordId,PaginateSearch paginateSearch,int i) {
		//log.debug("cdrcbuid ::" + cordId);
		String query = "select c1.codelst_desc hla_code,c2.codelst_desc hla_method,hl.HLA_VALUE_TYPE1, hl.HLA_VALUE_TYPE2, c3.codelst_desc entity,to_char(hl.CREATED_ON,'Mon DD,YYYY') created_on from cb_hla hl,  er_codelst c1,  er_codelst c2,"
				+ " er_codelst c3	where hl.fk_hla_method_id=c2.pk_codelst and hl.fk_hla_code_id=c1.pk_codelst and hl.ENTITY_ID='"
				+ cordId +"' and hl.ENTITY_TYPE=(select pk_codelst from er_codelst where codelst_type='entity_type' and codelst_subtyp='CBU')"
				+ " and c2.codelst_type='hla_meth'"
				+ " and c1.codelst_type='hla_locus' and HL.ENTITY_TYPE=c3.pk_codelst order by hl.pk_hla desc";

		/*
		 * String query =
		 * "select distinct(cl.CODELST_DESC), (select cl1.CODELST_DESC  from  ER_CODELST cl1  where  cl1.PK_CODELST = hl.FK_HLA_METHOD_ID  and cl1.CODELST_TYPE='hla_meth'),"
		 * +
		 * " hl.HLA_VALUE_TYPE1,hl.HLA_VALUE_TYPE2,  cl2.CODELST_DESC  from  ER_CODELST cl, CB_HLA hl, ER_CODELST cl2  where cl.CODELST_TYPE ='hla_locus' "
		 * + " and hl.FK_CORD_CDR_CBU_ID= '"+cdrcbuid+
		 * "'  and cl.PK_CODELST = hl.FK_HLA_CODE_ID   and cl2.PK_CODELST = hl.GARUDA_ENTITY_TYPE"
		 * ;
		 */
		return velosHelper.getListObject(query,paginateSearch,i);
	}

	public static List<Object> getStatusHistory(Long entityID, Long entityType,
			String statusTypeCode,PaginateSearch paginateSearch,int i) {
		String query = "select (es.STATUS_DATE),(es.CREATOR),(es.FK_STATUS_VALUE),(es.PK_ENTITY_STATUS),(es.STATUS_REMARKS) from CB_ENTITY_STATUS es where es.ENTITY_ID="
				+ entityID
				+ " and es.FK_ENTITY_TYPE="
				+ entityType
				+ " and es.STATUS_TYPE_CODE='"
				+ statusTypeCode
				+ "' order by es.STATUS_DATE DESC,es.PK_ENTITY_STATUS DESC";
		return velosHelper.getListObject(query,paginateSearch,i);
	}

	public static List<Object> getMultipleIneligibleReasons(Long pkEntityStatus,PaginateSearch paginateSearch,int i) {
		String query = "select (esr.FK_REASON_ID) from CB_ENTITY_STATUS_REASON esr where esr.FK_ENTITY_STATUS="
				+ pkEntityStatus;
		return velosHelper.getListObject(query,paginateSearch,i);
	}

	public static List<EntityStatusReasonPojo> getReasonsByEntityStatusId(
			Long entityStatusId) {
		return getReasonsByEntityStatusId(entityStatusId, null);
	}

	public static List<EntityStatusReasonPojo> getReasonsByEntityStatusId(
			Long entityStatusId, Session session) {
		List<EntityStatusReasonPojo> entityStatusReasonPojos = new ArrayList<EntityStatusReasonPojo>();
		EntityStatusReasonPojo entityStatusReasonPojo = null;
		EntityStatusReason reason = new EntityStatusReason();
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "From EntityStatusReason where fkEntityStatus="
					+ entityStatusId;
			List<EntityStatusReason> list = (List<EntityStatusReason>) session
					.createQuery(sql).list();
			for (EntityStatusReason e : list) {
				entityStatusReasonPojo = new EntityStatusReasonPojo();
				entityStatusReasonPojo = (EntityStatusReasonPojo) ObjectTransfer
						.transferObjects(e, entityStatusReasonPojo);
				entityStatusReasonPojos.add(entityStatusReasonPojo);
			}
			System.out
					.println("-------------------Size of reasons---------------"
							+ entityStatusReasonPojos.size());
		} catch (Exception e) {
			//log.debug(" Exception in getReasonsByEntityStatusId " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return entityStatusReasonPojos;
	}

	public static List<AdditionalIdsPojo> getCordInfoAdditionalIds(Long cordId) {
		return getCordInfoAdditionalIds(null, cordId);
	}

	public static List<AdditionalIdsPojo> getCordInfoAdditionalIds(
			Session session, Long cordId) {
		
		boolean sessionFlag = false;
		AdditionalIdsPojo infoAdditionalIdsPojo = null;
		List<AdditionalIdsPojo> infoAdditionalIdsPojos = new ArrayList<AdditionalIdsPojo>();
		List<AdditionalIds> infoAdditionalIds = null;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if (cordId != null) {
				String sql = "select {c.*} from CB_ADDITIONAL_IDS c where c.ENTITY_ID=? and (c.deletedflag = 0 or c.deletedflag is null)";
				SQLQuery query = session.createSQLQuery(sql);
				query.addEntity("c", AdditionalIds.class);
				query.setLong(0, cordId);
				infoAdditionalIds = (List<AdditionalIds>) query.list();
			}
			if (infoAdditionalIds != null && infoAdditionalIds.size() > 0) {
				for (AdditionalIds a : infoAdditionalIds) {
					infoAdditionalIdsPojo = new AdditionalIdsPojo();
					infoAdditionalIdsPojo = (AdditionalIdsPojo) ObjectTransfer
							.transferObjects(a, infoAdditionalIdsPojo);
					infoAdditionalIdsPojos.add(infoAdditionalIdsPojo);
				}
			}
		} catch (Exception e) {
			//log.debug(" Exception in getCordInfoAdditionalIds " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		
		return infoAdditionalIdsPojos;
		
	}
	
	public static List getcbuFlaggedItems(Long cordId) {
		return getcbuFlaggedItems(null, cordId);
	}
	
	public static List getcbuFlaggedItems(Session session, Long cordId) {
		boolean sessionFlag = false;
		List<Assessment> lstFlaggedItems = null;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if (cordId != null) {
					/*String sql = "SELECT {ass.*} FROM cb_assessment ass WHERE ass.entity_id=? AND ass.flag_for_later = 1 AND (ass.sub_entity_id IN (SELECT pk_form_responses"
				 +" FROM cb_form_responses  WHERE fk_form_version IN (SELECT MAX(pk_form_version) FROM cb_form_version  GROUP BY ass.entity_id,"
				 +" fk_form HAVING ass.entity_id=? )) or (ass.sub_entity_type in(select pk_codelst from er_codelst where codelst_type='sub_entity_type' and codelst_subtyp in('NOTES'))" 
				 +" and ass.sub_entity_id not in(select pk_notes from cb_notes where amended=1)) or (ass.sub_entity_type in(select pk_codelst from er_codelst where codelst_type='sub_entity_type' and codelst_subtyp in('LAB_SUM'))"
				 +" and ass.assessment_reason in(select distinct ass.assessment_reason from cb_assessment where ass.assessment_reason=(select hemoglobin_scrn from cb_cord where pk_cord=?) or"
				 +" ass.assessment_reason=(select fk_cord_fungal_cul_result from cb_cord where pk_cord=?))))";
	
				SQLQuery query = session.createSQLQuery(sql);
				query.addEntity("ass", Assessment.class);
				query.setLong(0, cordId);
				lstFlaggedItems = (List<Assessment>)query.list();*/
				
				String sql = "SELECT ass.* " +
				"FROM cb_assessment ass " +
				"WHERE entity_id     ="+cordId+
				"AND flag_for_later  = 1 " +
				"AND (sub_entity_id IN " +
				"  (SELECT pk_form_responses " +
				"  FROM cb_form_responses " +
				"  WHERE fk_form_version IN " +
				"    (SELECT MAX(pk_form_version) " +
				"    FROM cb_form_version " +
				"    WHERE entity_id="+cordId+
				"    AND fk_form   IN " +
				"      (SELECT pk_form FROM cb_forms WHERE is_current_flag=1 " +
				"      ) " +
				"    GROUP BY entity_id, " +
				"      fk_form " +
				"    ) " +
				"  ) " +
				"OR (sub_entity_type IN " +
				"  (SELECT pk_codelst " +
				"  FROM er_codelst " +
				"  WHERE codelst_type  ='sub_entity_type' " +
				"  AND codelst_subtyp IN('NOTES') " +
				"  ) " +
				"AND sub_entity_id NOT IN " +
				"  (SELECT pk_notes FROM cb_notes WHERE amended=1 " +
				"  )) " +
				"OR (sub_entity_type IN " +
				"  (SELECT pk_codelst " +
				"  FROM er_codelst " +
				"  WHERE codelst_type  ='sub_entity_type' " +
				"  AND codelst_subtyp IN('LAB_SUM') " +
				"  ) " +
				"AND assessment_reason IN " +
				"  (SELECT DISTINCT assessment_reason " +
				"  FROM cb_assessment " +
				"  WHERE assessment_reason= " +
				"    (SELECT hemoglobin_scrn FROM cb_cord WHERE pk_cord="+cordId+
				"    ) " +
				"  OR assessment_reason= " +
				"    (SELECT fk_cord_fungal_cul_result FROM cb_cord WHERE pk_cord="+cordId+
				"    ) " +
				"  )))";
				SQLQuery query = session.createSQLQuery(sql);
				query.addEntity("ass",Assessment.class);
				lstFlaggedItems = query.list();
			}
		} catch (Exception e) {
			//log.debug(" Exception in getCordInfoAdditionalIds " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session!=null && session.isOpen()) {
				session.close();
			}
		}
		return lstFlaggedItems;
	}
	public static List<AdditionalIdsPojo> saveCordInfoAdditionalIds(
			List<AdditionalIdsPojo> additionalIdsPojo,Boolean fromUI) {
		return saveCordInfoAdditionalIds(null, additionalIdsPojo,fromUI);
	}

	public static List<AdditionalIdsPojo> saveCordInfoAdditionalIds(
			Session session, List<AdditionalIdsPojo> additionalIdsPojo,Boolean fromUI) {
		boolean sessionFlag = false;
		List<AdditionalIds> infoAdditionalIds = new ArrayList<AdditionalIds>();
		AdditionalIds additionalIds = null;
		AdditionalIdsPojo infoAdditionalIdsPojo = null;
		for (AdditionalIdsPojo a : additionalIdsPojo) {
			if(a != null){
				additionalIds = new AdditionalIds();
				additionalIds = (AdditionalIds) ObjectTransfer.transferObjects(a,
						additionalIds);
				if(!fromUI){
				additionalIds = (AdditionalIds) new VelosUtil()
						.setAuditableInfo(additionalIds);
				}
				infoAdditionalIds.add(additionalIds);
			}
		}
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			for (AdditionalIds aid : infoAdditionalIds) {
				if (aid.getPkAdditionalIds() != null) {
					session.merge(aid);
				} else {
					session.save(aid);
				}
			}
			if (infoAdditionalIds != null && infoAdditionalIds.size() > 0) {
				additionalIdsPojo.clear();
				for (AdditionalIds a : infoAdditionalIds) {
					infoAdditionalIdsPojo = new AdditionalIdsPojo();
					infoAdditionalIdsPojo = (AdditionalIdsPojo) ObjectTransfer
							.transferObjects(a, infoAdditionalIdsPojo);
					additionalIdsPojo.add(infoAdditionalIdsPojo);
				}
			}
			if (sessionFlag) {
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			//log.debug(" Exception in saveCordInfoAdditionalIds " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return additionalIdsPojo;
	}

	public static CdrCbuPojo saveOrUpdateCordInfo(CdrCbuPojo cdrCbuPojo,Boolean fromUI) {
		return saveOrUpdateCordInfo(cdrCbuPojo, null,fromUI);
	}

	public static CdrCbuPojo saveOrUpdateCordInfo(CdrCbuPojo cdrCbuPojo,
			Session session,Boolean fromUI) {
		boolean sessionFlag = false;
		CdrCbu cdrCbu = new CdrCbu();
		cdrCbu = (CdrCbu) ObjectTransfer.transferObjects(cdrCbuPojo, cdrCbu);
		if(fromUI) 
		cdrCbu = (CdrCbu) new VelosUtil().setAuditableInfo(cdrCbu);
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if (cdrCbu.getCordID() != null) {
				if(cdrCbu.getFkCbbId()!=null && cdrCbu.getFkCbbId()!=-1){
					Site s = new Site();
					s.setSiteId(cdrCbu.getFkCbbId());
					s = (Site)session.get(Site.class,s.getSiteId());
					cdrCbu.setSite(s);
				}
				session.merge(cdrCbu);
			 } else {
				session.save(cdrCbu);
				if(cdrCbu.getFkCbbId()!=null && cdrCbu.getFkCbbId()!=-1){
					Site s = new Site();
					s.setSiteId(cdrCbu.getFkCbbId());
					s = (Site)session.get(Site.class,s.getSiteId());
					cdrCbu.setSite(s);
				}
			 }
			cdrCbuPojo = (CdrCbuPojo) ObjectTransfer.transferObjects(cdrCbu,
					cdrCbuPojo);
			if (sessionFlag) {
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			//log.debug(" Exception in saveOrUpdateCordInfo " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return cdrCbuPojo;
	}

	public static EntityStatusPojo saveOrUpdateEntityStatus(
			EntityStatusPojo entityStatusPojo,Boolean fromUI) {
		return saveOrUpdateEntityStatus(entityStatusPojo, null,fromUI);
	}

	public static EntityStatusPojo saveOrUpdateEntityStatus(
			EntityStatusPojo entityStatusPojo, Session session,Boolean fromUI) {
		boolean sessionFlag = false;
		EntityStatus entityStatus = new EntityStatus();
		entityStatus = (EntityStatus) ObjectTransfer.transferObjects(
				entityStatusPojo, entityStatus);
		if(fromUI)
		entityStatus = (EntityStatus) new VelosUtil().setAuditableInfo(entityStatus);
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if (entityStatus.getPkEntityStatus() != null) {
				session.merge(entityStatus);
			} else {
				session.save(entityStatus);
			}
			if (sessionFlag) {
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			//log.debug(" Exception in saveOrUpdateEntityStatus " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		entityStatusPojo = new EntityStatusPojo();
		entityStatusPojo = (EntityStatusPojo) ObjectTransfer.transferObjects(
				entityStatus, entityStatusPojo);
		//log.debug(entityStatusPojo.getPkEntityStatus());
		return entityStatusPojo;
	}

	public static EntityStatusReasonPojo saveOrUpdateEntityStatusReason(
			EntityStatusReasonPojo entityStatusReasonPojo,Boolean fromUI) {
		return saveOrUpdateEntityStatusReason(entityStatusReasonPojo, null,fromUI);
	}

	public static EntityStatusReasonPojo saveOrUpdateEntityStatusReason(
			EntityStatusReasonPojo entityStatusReasonPojo, Session session,Boolean fromUI) {
		boolean sessionFlag = false;
		EntityStatusReason entityStatusReason = new EntityStatusReason();
		entityStatusReason = (EntityStatusReason) ObjectTransfer
				.transferObjects(entityStatusReasonPojo, entityStatusReason);
		if(fromUI)
		entityStatusReason = (EntityStatusReason) new VelosUtil().setAuditableInfo(entityStatusReason);
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();

			}
			if (entityStatusReason.getPkEntityStatusReason() != null) {
				session.merge(entityStatusReason);

			} else {

				session.save(entityStatusReason);
			}
			entityStatusReasonPojo = (EntityStatusReasonPojo) ObjectTransfer
					.transferObjects(entityStatusReason, entityStatusReasonPojo);
			if (sessionFlag) {
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			//log.debug(" Exception in saveEntityStatus " + e);
			log.error(e.getMessage());
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return entityStatusReasonPojo;
	}
	
	public static EntityStatusPojo loadLatestEntityStatus(Long entityId,Long entityType,String status){	
		return loadLatestEntityStatus(entityId, entityType, status,null);
	}
	public static EntityStatusPojo loadLatestEntityStatus(Long entityId,Long entityType,String status,Session session){	
		boolean sessionFlag = false;
		EntityStatusPojo entityStatusPojo = null;
		EntityStatus entityStatus = null;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "From EntityStatus where entityId="+entityId+" and entityType="+entityType+" and statusTypeCode='"+status+"'" +
					" order by pkEntityStatus desc";
			Query query = session.createQuery(sql);
			List<EntityStatus> list = query.list();
			if(list!=null && list.size()>0){
				entityStatus = new EntityStatus();
				entityStatus = (EntityStatus)list.get(0);
				if(entityStatus!=null){
					entityStatusPojo = new EntityStatusPojo();
					entityStatusPojo = (EntityStatusPojo) ObjectTransfer
					.transferObjects(entityStatus,entityStatusPojo);
				}
			}			
		} catch (Exception e) {
			//log.debug(" Exception in saveEntityStatus " + e);
			log.error(e.getMessage());
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return entityStatusPojo;
	}

	public static EntityStatusReasonPojo loadEntityStatusReason(
			EntityStatusReasonPojo entityStatusReasonPojo) {
		return loadEntityStatusReason(entityStatusReasonPojo, null);

	}

	public static EntityStatusReasonPojo loadEntityStatusReason(
			EntityStatusReasonPojo entityStatusReasonPojo, Session session) {
		boolean sessionFlag = false;
		EntityStatusReason entityStatusReason = new EntityStatusReason();
		entityStatusReason = (EntityStatusReason) ObjectTransfer
				.transferObjects(entityStatusReasonPojo, entityStatusReason);
		entityStatusReason = (EntityStatusReason) new VelosUtil()
				.setAuditableInfo(entityStatusReason);
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();

			}
			if (entityStatusReason.getPkEntityStatusReason() != null) {
				entityStatusReason = (EntityStatusReason) session.load(
						EntityStatusReason.class,
						entityStatusReason.getPkEntityStatusReason());
			}
			entityStatusReasonPojo = (EntityStatusReasonPojo) ObjectTransfer
					.transferObjects(entityStatusReason, entityStatusReasonPojo);

		} catch (Exception e) {
			//log.debug(" Exception in saveEntityStatus " + e);
			log.error(e.getMessage());
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return entityStatusReasonPojo;
	}

	public static List<HLAPojo> saveOrUpdateHlas(List<HLAPojo> hlaPojos,Boolean fromUI) {
		return saveOrUpdateHlas(hlaPojos,fromUI, null);
	}

	public static List<HLAPojo> saveOrUpdateHlas(List<HLAPojo> hlaPojos
			,Boolean fromUI,Session session) {
		List<HLA> hlas = new ArrayList<HLA>();
		HLA hla = null;
		HLAPojo hlaPojo = null;
		for (HLAPojo hp : hlaPojos) {
			hla = new HLA();
			hla = (HLA) ObjectTransfer.transferObjects(hp, hla);
			if(fromUI)
			   hla = (HLA) new VelosUtil().setAuditableInfo(hla);
			hlas.add(hla);
		}
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			for (HLA h : hlas) {
				if (h.getPkHla() != null) {
					session.merge(h);
				} else {
					session.save(h);
				}
			}
			if (hlas != null && hlas.size() > 0) {
				hlaPojos.clear();
				for (HLA hl : hlas) {
					hlaPojo = (HLAPojo) ObjectTransfer.transferObjects(hl,
							hlaPojo);
					hlaPojos.add(hlaPojo);
				}
			}
			if (sessionFlag) {
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			//log.debug(" Exception in saveOrUpdateHla " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return hlaPojos;
	}

	public static List<CdrCbuPojo> getCordsInProgress(Long cordId) {
		return getCordsInProgress(cordId, null);
	}

	public static List<CdrCbuPojo> getCordsInProgress(Long cordId,
			Session session) {
		List<CdrCbuPojo> cbuListPojos = new ArrayList<CdrCbuPojo>();
		CdrCbuPojo cbuPojo = null;
		List<CdrCbu> cbuList = new ArrayList<CdrCbu>();
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "select {c.*} from CB_CORD c where c.CORD_SEARCHABLE=0 ";
			if (cordId != null) {
				sql += "and c.PK_CORD= ?";
			}
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity("c", CdrCbu.class);
			if (cordId != null) {
				query.setLong(0, cordId);
			}
			cbuList = (List<CdrCbu>) query.list();
			if (cbuList != null && cbuList.size() > 0) {
				for (CdrCbu d : cbuList) {
					cbuPojo = new CdrCbuPojo();
					cbuPojo = (CdrCbuPojo) ObjectTransfer.transferObjects(d,
							cbuPojo);
					cbuListPojos.add(cbuPojo);
				}
			}
		} catch (Exception e) {
			//log.debug(" Exception in getCordsInProgress " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return cbuListPojos;
	}
	
	public static List<CdrCbuPojo> getCordsByCbuStatus(Long cordId,Long cbuStatus) {
		return getCordsByCbuStatus(cordId,cbuStatus,null);
	}

	public static List<CdrCbuPojo> getCordsByCbuStatus(Long cordId,Long cbuStatus,Session session){
		List<CdrCbuPojo> cbuListPojos = new ArrayList<CdrCbuPojo>();
		CdrCbuPojo cbuPojo = null;
		List<CdrCbu> cbuList = new ArrayList<CdrCbu>();
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "select {c.*} from CB_CORD c where c.FK_CORD_CBU_STATUS=? ";
			if (cordId != null) {
				sql += "and c.PK_CORD= ?";
			}
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity("c", CdrCbu.class);
			query.setLong(0, cbuStatus);
			if (cordId != null) {
				query.setLong(1, cordId);
			}
			cbuList = (List<CdrCbu>) query.list();
			if (cbuList != null && cbuList.size() > 0) {
				for (CdrCbu d : cbuList) {
					cbuPojo = new CdrCbuPojo();
					cbuPojo = (CdrCbuPojo) ObjectTransfer.transferObjects(d,
							cbuPojo);
					cbuListPojos.add(cbuPojo);
				}
			}
		} catch (Exception e) {
			//log.debug(" Exception in getCordsInProgress " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return cbuListPojos;
	}

	public static List<HLAPojo> getHlasByCord(Long cordId, Long entityType, String criteria) {
		return getHlasByCord(cordId, entityType,criteria, null);
	}

	public static List<HLAPojo> getHlasByCord(Long cordId, Long entityType,String criteria,
			Session session) {
		List<HLAPojo> hlaPojos = new ArrayList<HLAPojo>();
		HLAPojo hlaPojo = null;
		List<HLA> hlas = new ArrayList<HLA>();
		boolean sessionFlag = false;
		criteria = (criteria == null || "".equals(criteria))?"":criteria;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "select {h.*},{ec1.*},{ec2.*} from CB_HLA h ,cb_antigen_encod ec1,cb_antigen_encod ec2 where h.fk_hla_antigeneid1=ec1.fk_antigen (+) and ( ec1.version = ( select max(ec1s.version) from cb_antigen_encod ec1s where ec1.fk_antigen=ec1s.fk_antigen ) or ec1.version is null)" +
			" and h.fk_hla_antigeneid2  =  ec2.fk_antigen (+) and (ec2.version =( select max(ec2s.version) from cb_antigen_encod ec2s where ec2.fk_antigen=ec2s.fk_antigen  ) or ec2.version is null) and  h.ENTITY_ID=? and h.ENTITY_TYPE=?  "+criteria+" order by h.CB_HLA_ORDER_SEQ desc,h.HLA_RECEIVED_DATE desc nulls last";
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity("h", HLA.class);
			query.addEntity("ec1",AntigenEncoding.class);
			query.addEntity("ec2",AntigenEncoding.class);
			query.setLong(0, cordId);
			query.setLong(1, entityType);
			List<Object> objList = (List<Object>)query.list();
			if(objList!=null && objList.size()>0){
				for(Object o : objList){
					Object[] oArr = (Object[])o;
					HLA hla = null;
					AntigenEncoding anEncoding1 = null;
					AntigenEncoding anEncoding2 = null;
					if(oArr[0]!=null){
						hla = new HLA();
						anEncoding1 = new AntigenEncoding();
						anEncoding2 = new AntigenEncoding();
						hla = (HLA)oArr[0];
						anEncoding1 = (AntigenEncoding)oArr[1];
						anEncoding2 = (AntigenEncoding)oArr[2];
						if(anEncoding1!=null && hla!=null)
						    hla.setHlaType1(anEncoding1.getGenomicFormat());
						if(anEncoding2!=null && hla!=null)
						    hla.setHlaType2(anEncoding2.getGenomicFormat());
					}
					if(hla!=null)
						hlas.add(hla);
				}
			}
			//hlas = (List<HLA>) query.list();
			for (HLA h : hlas) {
				hlaPojo = new HLAPojo();
				hlaPojo = (HLAPojo) ObjectTransfer.transferObjects(h, hlaPojo);
				hlaPojos.add(hlaPojo);
			}
		} catch (Exception e) {
			//log.debug(" Exception in getCordsInProgress " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return hlaPojos;
	}	
	
	public static List<AntigenEncoding> getEncodingList(List<Long> pkAntigenList){
		return getEncodingList(pkAntigenList,null);
	}
	
    public static List<AntigenEncoding> getEncodingList(List<Long> pkAntigenList,Session session){
    	List<AntigenEncoding> encodings = new ArrayList<AntigenEncoding>();
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "From AntigenEncoding ";
			
			if(pkAntigenList!=null && pkAntigenList.size()>0)
				sql+= "where fkAntigen in (:pkAntigenList)";
			Query query = session.createQuery(sql);
			if(pkAntigenList!=null && pkAntigenList.size()>0)
			    query.setParameterList("pkAntigenList", pkAntigenList);
			encodings = (List<AntigenEncoding>)query.list();
			
		} catch (Exception e) {
			//log.debug(" Exception in getEncodingList " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return encodings;
	}
	
	public static Long getLabTestpkbyshname(String shname){
		return getLabTestpkbyshname(shname, null);
	}
	
	public static Long getLabTestpkbyshname(String shname, Session session){
		 Long pklabtest=null;
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if (shname != null) {
				String sql = "select PK_LABTEST from er_labtest where LABTEST_SHORTNAME='"+shname+"'";
				SQLQuery query = session.createSQLQuery(sql);
				List lst=query.list();
				if(lst.get(0)!=null){
					pklabtest = Long.parseLong(lst.get(0).toString());
				}
				//log.debug("pklabtest: "+pklabtest);
			}
		} catch (Exception e) {
			//log.debug(" Exception in getLabTestpkbyshname " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return pklabtest;
	}
	
	public static CBUMinimumCriteriaTempPojo saveMinimumCriteriaTemp(CBUMinimumCriteriaTempPojo minimumCriteriatempPojo) {
		return saveMinimumCriteriaTemp(minimumCriteriatempPojo, null);
	}
	
	public static CBUMinimumCriteriaTempPojo saveMinimumCriteriaTemp(CBUMinimumCriteriaTempPojo minimumCriteriatempPojo, Session session) {
		boolean sessionFlag = false;
		CBUMinimumCriteriaTemp cbuMinimumCriteria = new CBUMinimumCriteriaTemp();
		cbuMinimumCriteria = (CBUMinimumCriteriaTemp) ObjectTransfer.transferObjects(minimumCriteriatempPojo, cbuMinimumCriteria);
		cbuMinimumCriteria = (CBUMinimumCriteriaTemp) new VelosUtil().setAuditableInfo(cbuMinimumCriteria);
		try {
			if (session == null) {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if (cbuMinimumCriteria.getFkMinimumCrit() != null) {
				session.saveOrUpdate(cbuMinimumCriteria);
			}
			minimumCriteriatempPojo = (CBUMinimumCriteriaTempPojo) ObjectTransfer.transferObjects(cbuMinimumCriteria, minimumCriteriatempPojo);
			if (sessionFlag) {
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			// TODO: handle exception
			//log.debug("Exception : " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return minimumCriteriatempPojo;
	}
	public static CBUMinimumCriteriaPojo saveOrConfirmCriteria(
			CBUMinimumCriteriaPojo cbuMinimumCriteriaPojo) {
		return saveOrConfirmCriteria(cbuMinimumCriteriaPojo, null);
	}

	public static CBUMinimumCriteriaPojo saveOrConfirmCriteria(
			CBUMinimumCriteriaPojo cbuMinimumCriteriaPojo, Session session) {
		boolean sessionFlag = false;
		CBUMinimumCriteria cbuMinimumCriteria = new CBUMinimumCriteria();
		cbuMinimumCriteria = (CBUMinimumCriteria) ObjectTransfer
				.transferObjects(cbuMinimumCriteriaPojo, cbuMinimumCriteria);
		cbuMinimumCriteria = (CBUMinimumCriteria) new VelosUtil()
		.setAuditableInfo(cbuMinimumCriteria);
		try {
			if (session == null) {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if (cbuMinimumCriteria.getFkCordId() != null) {
				session.saveOrUpdate(cbuMinimumCriteria);
			}
			cbuMinimumCriteriaPojo = (CBUMinimumCriteriaPojo) ObjectTransfer
					.transferObjects(cbuMinimumCriteria, cbuMinimumCriteriaPojo);
			if (sessionFlag) {
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			// TODO: handle exception
			//log.debug("Exception : " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return cbuMinimumCriteriaPojo;
	}

	public static CBUFinalReviewPojo saveCbuFinalReview(
			CBUFinalReviewPojo cbuFinalReviewPojo) {
		return saveCbuFinalReview(cbuFinalReviewPojo, null);
	}

	public static CBUFinalReviewPojo saveCbuFinalReview(
			CBUFinalReviewPojo cbuFinalReviewPojo, Session session) {
		boolean sessionFlag = false;
		CBUFinalReview cbuFinalReview = new CBUFinalReview();
		cbuFinalReview = (CBUFinalReview) ObjectTransfer.transferObjects(
				cbuFinalReviewPojo, cbuFinalReview);
		cbuFinalReview = (CBUFinalReview) new VelosUtil()
				.setAuditableInfo(cbuFinalReview);
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}

			session.save(cbuFinalReview);

			cbuFinalReviewPojo = (CBUFinalReviewPojo) ObjectTransfer
					.transferObjects(cbuFinalReview, cbuFinalReviewPojo);

			if (sessionFlag) {
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			//log.debug(" Exception in saveAfterCbuFinalReview " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return cbuFinalReviewPojo;
	}

	public static Map<String, List> getCordUploadDocument(Long attachmentType,
			Long entityId, Long entityType) {
		return getCordUploadDocument(attachmentType, entityId, entityType, null);
	}

	public static Map<String, List> getCordUploadDocument(Long attachmentType,
			Long entityId, Long entityType, Session session) {
		//log.debug("Values----------" + attachmentType + entityId+ entityType);
		boolean sessionFlag = false;
		List list = null;
		List categoryList;
		Map<String, List> map = new HashMap<String, List>();
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "select {c.*},{a.*},{e.*},{u.*} "
					+ " from cb_cord c left join er_attachments a on c.PK_CORD=a.ENTITY_ID left join cb_upload_info u on u.FK_ATTACHMENTID=a.PK_ATTACHMENT "
					+ "left join er_codelst e on u.FK_CATEGORY=e.PK_CODELST where a.FK_ATTACHMENT_TYPE=? and a.ENTITY_ID=? and a.ENTITY_TYPE=? and (a.DELETEDFLAG is null or a.DELETEDFLAG='0') "
					+ "and lower(e.CODELST_TYPE)='doc_categ' order by a.PK_ATTACHMENT";
			// String sql1 =
			// "Select cordinfo.cordID,cordinfo.cdrCbuId,cordinfo.externalCbuId,cordinfo.registryId,cordinfo.localCbuId,attach.dcmsAttachmentId,attach.attachmentId,attach.documentType,attach.documentFile,attach.fileName,attach.createdOn,b.subType "
			// +
			// " from CdrCbu cordinfo left join Attachment attach on cordinfo.cordID=attach.garudaEntityId left join CBUploadInfo a on a.fk_AttachmentId=attach.attachmentId"
			// +
			// " left join CodeList b on a.fk_CategoryId=b.pkCodeId where attach.garudaEntityId= "+
			// entityId +" and attach.garudaEntityType= " + entityType +
			// " and attach.garudaAttachmentType= "+
			// attachmentType+" and lower(b.type)='doc_categ'";
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity("c", CdrCbu.class);
			query.addEntity("a", Attachment.class);
			query.addEntity("e", CodeList.class);
			query.addEntity("u", CBUploadInfo.class);
			query.setLong(0, attachmentType);
			query.setLong(1, entityId);
			query.setLong(2, entityType);
			// Query query = session.createQuery(sql1);
			list = query.list();
		} catch (Exception e) {
			//log.debug(" Exception in getCordUploadDocument " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		if (list != null && list.size() > 0) {
			for (Object o : list) {
				Object[] oArr = (Object[]) o;
				CdrCbu cbu = (CdrCbu) oArr[0];
				Attachment attachment = (Attachment) oArr[1];
				CodeList c = (CodeList) oArr[2];
				CBUploadInfo u = (CBUploadInfo) oArr[3];
				Object[] ob = new Object[21];
				ob[0] = cbu.getCordID();
				ob[1] = cbu.getCdrCbuId();
				ob[2] = cbu.getExternalCbuId();
				ob[3] = cbu.getRegistryId();
				ob[4] = cbu.getLocalCbuId();
				ob[5] = attachment.getDcmsAttachmentId();
				ob[6] = attachment.getAttachmentId();
				ob[7] = attachment.getDocumentType();
				ob[8] = attachment.getDocumentFile();
				ob[9] = attachment.getFileName();
				ob[10] = attachment.getCreatedOn();
				ob[11] = c.getSubType();
				ob[12] = u.getFk_SubCategoryId();
				ob[13] = u.getDescription();
				ob[14] = attachment.getCreatedBy();
				ob[15] = u.getFk_CategoryId();
				ob[16] = u.getReportDate();
				ob[17] = u.getProcessDate();
				ob[18] = u.getTestDate();
				ob[19] = u.getReceivedDate();
				ob[20] = u.getCompletionDate();
				
				if (map.containsKey(c.getSubType())) {
					categoryList = map.get(c.getSubType());
					categoryList.add(ob);
				} else {
					categoryList = new ArrayList();
					categoryList.add(ob);
					map.put(c.getSubType(), categoryList);
				}
			}
		}
		return map;
	}
	public static Long getIDMTestcount(Long fkSpecId){
		return getIDMTestcount(fkSpecId,null);
	}
	
	public static Long getIDMTestcount(Long fkSpecId,Session session){
		Long count=0l;
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if (fkSpecId != null) {
				String sql = "select max(plabs.custom008) from er_patlabs plabs where fk_specimen="+fkSpecId;
				SQLQuery query = session.createSQLQuery(sql);
				List lst=query.list();
				if(lst.get(0)!=null){
					count = Long.parseLong(lst.get(0).toString());
				}
				//log.debug("IDM count : "+count);
			}
		} catch (Exception e) {
			//log.debug(" Exception in getIDMTestcount " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return count;
	}
	
	public static List getIDMTests() {
		return getIDMTests(null);
	}
	
	public static List getIDMTests(Session session) {
		List idmList = new ArrayList();
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			// String sql =
			// "select ltest.pk_labtest,ltest.labtest_name from er_labtest ltest, er_labtestgrp testgroup, er_labgroup lgroup where ltest.pk_labtest=testgroup.fk_testid and testgroup.fk_labgroup=lgroup.pk_labgroup and lgroup.group_type='I'";
			String sql = "select ltest.pk_labtest FK_TESTID,ltestgrp.fk_labgroup FK_TESTGROUP,ltest.labtest_name,ltest.LABTEST_SHORTNAME from er_labtest ltest"
						+" left outer join er_labtestgrp ltestgrp on(ltestgrp.fk_testid=ltest.pk_labtest) where ltestgrp.fk_labgroup = (select lg.pk_labgroup from er_labgroup lg where lg.group_type='I') or ltestgrp.fk_labgroup ="
						+" (select lg.pk_labgroup from er_labgroup lg where lg.group_type='IOG') order by ltest.pk_labtest";
			SQLQuery query = session.createSQLQuery(sql);
			idmList = query.list();
		} catch (Exception e) {
			//log.debug(" Exception in getIDMTests " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return idmList;

	}
	public static List getpatlabpklist(Long fkSpecId){
		return getpatlabpklist(fkSpecId, null);
	}
	public static List getpatlabpklist(Long fkSpecId, Session session){
		List pkpatlablst=null;
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "select plab.fk_testid from er_patlabs plab left outer join er_labtestgrp ltestgrp "
				+" on( ltestgrp.fk_testid=plab.fk_testid ) where plab.pk_patlabs in( select max(pk_patlabs) from er_patlabs where fk_specimen = "
				+ fkSpecId
				+ "group by fk_specimen,fk_testid) and ltestgrp.fk_labgroup = ( select lg.pk_labgroup from er_labgroup lg where lg.group_type='IOG') order by plab.pk_patlabs";
		SQLQuery query = session.createSQLQuery(sql);
		pkpatlablst = query.list();

	} catch (Exception e) {
		//log.debug(" Exception in getpatlabpks " + e);
		log.error(e.getMessage());
		e.printStackTrace();
	} finally {
		if (session.isOpen()) {
			session.close();
		}
	}
		return pkpatlablst;
	}
	
	public static List<IDMPojo> getdataFromIDM(Long fkSpecId) {
		return getdataFromIDM(fkSpecId, null);
	}

	public static List<IDMPojo> getdataFromIDM(Long fkSpecId, Session session) {
		List<IDMPojo> idmList = new ArrayList<IDMPojo>();
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			// String sql =
			// "select ltest.pk_labtest,ltest.labtest_name from er_labtest ltest, er_labtestgrp testgroup, er_labgroup lgroup where ltest.pk_labtest=testgroup.fk_testid and testgroup.fk_labgroup=lgroup.pk_labgroup and lgroup.group_type='I'";
			String sql = "FROM IDM where fkspecimen="+fkSpecId+" order by pkidm";
			Query query = session.createQuery(sql);
			idmList = (List<IDMPojo>) query.list();

		} catch (Exception e) {
			//log.debug(" Exception in getTestsForIDM " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return idmList;
	}
	
	public static List getTestsForIDM(Long fkSpecId) {
		return getTestsForIDM(fkSpecId, null);
	}

	public static List getTestsForIDM(Long fkSpecId, Session session) {
		List idmList = new ArrayList();
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			// String sql =
			// "select ltest.pk_labtest,ltest.labtest_name from er_labtest ltest, er_labtestgrp testgroup, er_labgroup lgroup where ltest.pk_labtest=testgroup.fk_testid and testgroup.fk_labgroup=lgroup.pk_labgroup and lgroup.group_type='I'";
			String sql = "select  pl.pk_patlabs PK_PATLABS, ltest.pk_labtest FK_TESTID, ltestgrp.fk_labgroup FK_TESTGROUP, pl.FK_TEST_OUTCOME FK_TEST_OUTCOME, pl.FT_TEST_SOURCE FT_TEST_SOURCE, pl.CMS_APPROVED_LAB CMS_APPROVED_LAB, pl.FDA_LICENSED_LAB FDA_LICENSED_LAB, ltest.labtest_name, pl.testDate, pl.fk_specimen,ltest.LABTEST_SHORTNAME, f_get_codelstdesc(nvl(pl.FK_TEST_OUTCOME,0)),pl.custom003,pl.PK_ASSESSMENT PK_ASSESSMENT,pl.ASSESSMENT_REMARKS ASSESSMENT_REMARKS,pl.FLAG_FOR_LATER FLAG_FOR_LATER,pl.ASSESSMENT_FOR_RESPONSE ASSESSMENT_FOR_RESPONSE,pl.TC_VISIBILITY_FLAG TC_VISIBILITY_FLAG,pl.CONSULTE_FLAG CONSULTE_FLAG,pl.ASSESSMENT_POSTEDBY ASSESSMENT_POSTEDBY,pl.ASSESSMENT_POSTEDON ASSESSMENT_POSTEDON,pl.ASSESSMENT_CONSULTBY ASSESSMENT_CONSULTBY,pl.ASSESSMENT_CONSULTON ASSESSMENT_CONSULTON,pl.custom008 custom008 from er_labtest ltest  left outer join ( select plabs.pk_patlabs pk_patlabs,plabs.custom008 custom008, plabs.fk_testid fk_testid, plabs.fk_test_outcome fk_test_outcome, plabs.ft_test_source ft_test_source, plabs.cms_approved_lab cms_approved_lab, plabs.fda_licensed_lab fda_licensed_lab, plabs.fk_specimen fk_specimen, to_char(plabs.test_date,'Mon DD, YYYY') testDate,plabs.custom003 CUSTOM003,ass.PK_ASSESSMENT PK_ASSESSMENT,ass.ASSESSMENT_REMARKS ASSESSMENT_REMARKS,ass.FLAG_FOR_LATER FLAG_FOR_LATER,ass.ASSESSMENT_FOR_RESPONSE ASSESSMENT_FOR_RESPONSE,ass.TC_VISIBILITY_FLAG TC_VISIBILITY_FLAG,ass.SENT_TO_REVIEW_FLAG SENT_TO_REVIEW_FLAG,ass.CONSULTE_FLAG CONSULTE_FLAG,ass.ASSESSMENT_POSTEDBY ASSESSMENT_POSTEDBY,ass.ASSESSMENT_POSTEDON ASSESSMENT_POSTEDON,ass.ASSESSMENT_CONSULTBY ASSESSMENT_CONSULTBY,ass.ASSESSMENT_CONSULTON ASSESSMENT_CONSULTON from  er_patlabs plabs left outer join cb_assessment ass on(ass.sub_entity_id=plabs.pk_patlabs) where plabs.pk_patlabs in(select pk_patlabs from er_patlabs where fk_specimen="
					+ fkSpecId
					+ ")) pl on(ltest.pk_labtest=pl.fk_testid)  left outer join er_labtestgrp ltestgrp on( ltestgrp.fk_testid=ltest.pk_labtest )  where ltestgrp.fk_labgroup = ( select lg.pk_labgroup from er_labgroup lg  where  lg.group_type='I' ) or "
					+ "ltestgrp.fk_labgroup = ( select lg.pk_labgroup from er_labgroup lg  where  lg.group_type='IOG' ) order by pl.pk_patlabs";
			SQLQuery query = session.createSQLQuery(sql);
			idmList = query.list();

		} catch (Exception e) {
			//log.debug(" Exception in getTestsForIDM " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return idmList;
	}

	public static List getCbuStatus(String cbustatusin) {
		return getCbuStatus(cbustatusin, null);
	}

	public static List getCbuStatus(String cbustatusin, Session session) {
		List cbustatus = new ArrayList();
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if (cbustatusin != null) {
				String sql = "select PK_CBU_STATUS,CBU_STATUS_DESC,CBU_STATUS_CODE from cb_cbu_status where CODE_TYPE ='Response' and "
						+ cbustatusin
						+ " =1 and (deletedflag is null or deletedflag=0) order by STATUS_CODE_SEQ";
				SQLQuery query = session.createSQLQuery(sql);
				cbustatus = query.list();
			} else {
				String sql = "select PK_CBU_STATUS,CBU_STATUS_DESC,CBU_STATUS_CODE from cb_cbu_status order by STATUS_CODE_SEQ";
				SQLQuery query = session.createSQLQuery(sql);
				cbustatus = query.list();
			}
		} catch (Exception e) {
			//log.debug(" Exception in getCbuStatus " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return cbustatus;

	}

	public static CBUCompleteReqInfoPojo saveCBUCompleteReqInfo(
			CBUCompleteReqInfoPojo cbuCompleteReqInfoPojo) {
		return saveCBUCompleteReqInfo(cbuCompleteReqInfoPojo, null);
	}

	public static CBUCompleteReqInfoPojo saveCBUCompleteReqInfo(
			CBUCompleteReqInfoPojo cbuCompleteReqInfoPojo, Session session) {
		boolean sessionFlag = false;
		CBUCompleteReqInfo cbuCompleteReqInfo = new CBUCompleteReqInfo();
		cbuCompleteReqInfo = (CBUCompleteReqInfo) ObjectTransfer
				.transferObjects(cbuCompleteReqInfoPojo, cbuCompleteReqInfo);
		cbuCompleteReqInfo = (CBUCompleteReqInfo) new VelosUtil()
				.setAuditableInfo(cbuCompleteReqInfo);
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			session.save(cbuCompleteReqInfo);
			cbuCompleteReqInfoPojo = (CBUCompleteReqInfoPojo) ObjectTransfer
					.transferObjects(cbuCompleteReqInfo, cbuCompleteReqInfoPojo);

			if (sessionFlag) {
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			//log.debug(" Exception in saveCBUCompleteReqInfo " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return cbuCompleteReqInfoPojo;

	}
	public static IDMPojo saveIDMValues(IDMPojo idmpojo){
		return saveIDMValues(idmpojo,null);
	}
	
	public static IDMPojo saveIDMValues(IDMPojo idmpojo,Session session){
		
		boolean sessionFlag = false;
		IDM idm = new IDM();
		idm = (IDM) ObjectTransfer
				.transferObjects(idmpojo, idm);
		idm = (IDM) new VelosUtil().setAuditableInfo(idm);
		try {
			if (session == null) {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if (idm!= null) {
				session.saveOrUpdate(idm);
			}
			idmpojo = (IDMPojo) ObjectTransfer.transferObjects(idm, idmpojo);
			if (sessionFlag) {
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			// TODO: handle exception
			//log.debug("Exception : " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return idmpojo;
	}
	public static List<PatLabsPojo> saveOrUpdatePatLabs(
			List<PatLabsPojo> patLabsPojos) {
		return saveOrUpdatePatLabs(patLabsPojos, null);
	}

	public static List<PatLabsPojo> saveOrUpdatePatLabs(
			List<PatLabsPojo> patLabsPojos, Session session) {
		List<PatLabs> pLabs = new ArrayList<PatLabs>();
		PatLabs patLabs = null;
		PatLabsPojo patLabsPojo = null;
		for (PatLabsPojo pLabsPojo : patLabsPojos) {
			patLabs = new PatLabs();
			patLabs = (PatLabs) ObjectTransfer.transferObjects(pLabsPojo,
					patLabs);
			patLabs = (PatLabs) new VelosUtil().setAuditableInfo(patLabs);
			pLabs.add(patLabs);
		}
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}

			for (PatLabs p : pLabs) {
				String sql=null;
					if(p.getFkTimingOfTest()==null){
						if(p.getOtherListValue()!=null){
						sql = "select PK_PATLABS From ER_PATLABS where FK_TESTID=" + p.getFktestid()
						+ " and FK_SPECIMEN=" + p.getFkspecimen() +" and CUSTOM006='" + p.getOtherListValue()+"'";
						}
				}
				else{
					sql = "select PK_PATLABS From ER_PATLABS where FK_TESTID=" + p.getFktestid()
				+ " and FK_SPECIMEN=" + p.getFkspecimen() +" and FK_TIMING_OF_TEST=" + p.getFkTimingOfTest();
				}
				if(sql!=null){	
					List l1 = new ArrayList();
					l1 = (List<ArrayList>)session.createSQLQuery(sql).list();
					Long pkOtherTest=null;
					if(l1.size()>0){
						pkOtherTest= ((BigDecimal)l1.get(0)).longValue();
					}
					if(pkOtherTest!=null){
						p.setPkpatlabs(pkOtherTest);
					}
				}
				//log.debug("merge or save:::: patlabs object .......");
				if (p.getPkpatlabs() != null) {
					//log.debug("merging .........  ::::");
					session.merge(p);
				} else {
					//log.debug("saving .............  ::::");
					session.save(p);
				}
			}
			if (sessionFlag) {
				session.getTransaction().commit();
			}

			if (pLabs != null && pLabs.size() > 0) {
				//log.debug("patlabss clearred .............");
				patLabsPojos.clear();
				for (PatLabs pl : pLabs) {
					patLabsPojo=new PatLabsPojo();
					patLabsPojo = (PatLabsPojo) ObjectTransfer.transferObjects(
							pl, patLabsPojo);
					patLabsPojos.add(patLabsPojo);
				}
			}

		} catch (Exception e) {
			//log.debug(" Exception in saveOrUpdatePatLabs " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return patLabsPojos;

	}

	public static List<PatLabsPojo> savePatLabs(List<PatLabsPojo> patLabsPojos) {
		return savePatLabs(patLabsPojos, null);
	}

	public static List<PatLabsPojo> savePatLabs(List<PatLabsPojo> patLabsPojos,
			Session session) {
		List<PatLabs> pLabs = new ArrayList<PatLabs>();
		PatLabs patLabs = null;
		PatLabsPojo patLabsPojo = null;
		for (PatLabsPojo pLabsPojo : patLabsPojos) {
			patLabs = new PatLabs();
			patLabs = (PatLabs) ObjectTransfer.transferObjects(pLabsPojo,
					patLabs);
			patLabs = (PatLabs) new VelosUtil().setAuditableInfo(patLabs);
			pLabs.add(patLabs);
		}
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}

			for (PatLabs p : pLabs) {
				/*
				 * //log.debug("merge or save:::: patlabs object .......")
				 * ; if(p.getPkpatlabs()!=null){
				 * //log.debug("merging .........  ::::");
				 */
				session.save(p);
				/*
				 * }else{ //log.debug("saving .............  ::::");
				 * session.save(p); }
				 */
			}
			if (sessionFlag) {
				session.getTransaction().commit();
			}

			if (pLabs != null && pLabs.size() > 0) {
				//log.debug("patlabss clearred .............");
				patLabsPojos.clear();
				for (PatLabs pl : pLabs) {
					patLabsPojo = (PatLabsPojo) ObjectTransfer.transferObjects(
							pl, patLabsPojo);
					patLabsPojos.add(patLabsPojo);
				}
			}

		} catch (Exception e) {
			//log.debug(" Exception in saveOrUpdatePatLabs " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return patLabsPojos;

	}

	public static List<PatLabsPojo> getOtherTestLst(Long specimenId, Long testId) {
		return getOtherTestLst(specimenId, testId, null);
	}

	public static List<PatLabsPojo> getOtherTestLst(Long specimenId,
			Long testId, Session session) {
		List<PatLabs> pLabs = new ArrayList<PatLabs>();
		List<PatLabsPojo> pLabsPojo = new ArrayList<PatLabsPojo>();
		PatLabs patLabs = null;
		PatLabsPojo patLabsPojo = null;
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "From PatLabs where fktestid=" + testId
					+ " and fkspecimen=" + specimenId;
			pLabs = (List<PatLabs>) session.createQuery(sql).list();

			if (pLabs != null && pLabs.size() > 0) {
				System.out
						.println("get Other Lab Tests CBUHelper .............");
				pLabsPojo.clear();
				for (PatLabs pl : pLabs) {
					patLabsPojo = new PatLabsPojo();
					patLabsPojo = (PatLabsPojo) ObjectTransfer.transferObjects(
							pl, patLabsPojo);
					pLabsPojo.add(patLabsPojo);
				}

			}
			// pLabsPojo =
			// (List<PatLabsPojo>)ObjectTransfer.transferObjects(pLabs,
			// pLabsPojo);
		} catch (Exception e) {
			//log.debug(" Exception in getOtherTestLst " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return pLabsPojo;

	}
	

	public static PatLabsPojo getPatLabsPojoById(Long specimenId, Long pkPatLabs) {
		return getPatLabsPojoById(specimenId, pkPatLabs, null);
	}

	public static PatLabsPojo getPatLabsPojoById(Long specimenId,
			Long pkPatLabs, Session session) {
		PatLabs patLabs = new PatLabs();
		PatLabsPojo patLabsPojo = new PatLabsPojo();
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "From PatLabs where pkpatlabs=" + pkPatLabs
					+ " and fkspecimen=" + specimenId;
			patLabs = (PatLabs) session.createQuery(sql).list().get(0);

			if (patLabs != null) {
				patLabsPojo = (PatLabsPojo) ObjectTransfer.transferObjects(
						patLabs, patLabsPojo);
			}

		}

		catch (Exception e) {
			//log.debug(" Exception in getPatLabsPojoById " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return patLabsPojo;
	}

	public static List<EntityMultipleValuesPojo> saveMultipleEntityValue(
			List<EntityMultipleValuesPojo> entityMultipleValuesPojos,Boolean fromUI) {
		return saveMultipleEntityValue(entityMultipleValuesPojos, null,fromUI);
	}

	public static List<EntityMultipleValuesPojo> saveMultipleEntityValue(
			List<EntityMultipleValuesPojo> entityMultipleValuesPojos,
			Session session,Boolean fromUI) {
		List<EntityMultipleValues> list = new ArrayList<EntityMultipleValues>();
		List<EntityMultipleValues> entityMultipleValues = new ArrayList<EntityMultipleValues>();
		EntityMultipleValues multipleValues;
		EntityMultipleValuesPojo multipleValuesPojo;
		for (EntityMultipleValuesPojo p : entityMultipleValuesPojos) {
			multipleValues = new EntityMultipleValues();
			multipleValues = (EntityMultipleValues) ObjectTransfer
					.transferObjects(p, multipleValues);
			if(fromUI)
			multipleValues = (EntityMultipleValues) new VelosUtil().setAuditableInfo(multipleValues);
			list.add(multipleValues);
		}
		for (EntityMultipleValues e : list) {
			multipleValues = new EntityMultipleValues();
			multipleValues = saveEntityMultipleValues(e, session);
			entityMultipleValues.add(multipleValues);
		}
		entityMultipleValuesPojos.clear();
		if (entityMultipleValues != null && entityMultipleValues.size() > 0) {
			for (EntityMultipleValues e1 : entityMultipleValues) {
				multipleValuesPojo = new EntityMultipleValuesPojo();
				multipleValuesPojo = (EntityMultipleValuesPojo) ObjectTransfer
						.transferObjects(e1, multipleValuesPojo);
				entityMultipleValuesPojos.add(multipleValuesPojo);
			}
		}

		return entityMultipleValuesPojos;
	}

	
	
	public static List<EntityStatusReasonPojo> saveMultipleReasonValue(
			List<EntityStatusReasonPojo> entityStatusReasonPojo,Boolean fromUI) {
		return saveMultipleReasonValue(entityStatusReasonPojo, null,fromUI);
	}

	public static List<EntityStatusReasonPojo> saveMultipleReasonValue(
		List<EntityStatusReasonPojo> entityStatusReasonPojos,
		Session session,Boolean fromUI) {
		List<EntityStatusReason> list = new ArrayList<EntityStatusReason>();
		List<EntityStatusReason> entityStatusReasons = new ArrayList<EntityStatusReason>();
		EntityStatusReason entityStatusReason;
		EntityStatusReasonPojo entityStatusReasonPojo;
		for (EntityStatusReasonPojo p : entityStatusReasonPojos) {
			entityStatusReason = new EntityStatusReason();
			entityStatusReason = (EntityStatusReason) ObjectTransfer
					.transferObjects(p, entityStatusReason);
			if(fromUI)
				entityStatusReason = (EntityStatusReason) new VelosUtil().setAuditableInfo(entityStatusReasons);
			list.add(entityStatusReason);
		}
		for (EntityStatusReason e : list) {
			entityStatusReason = new EntityStatusReason();
			entityStatusReason = saveReasonMultipleValues(e, session);
			entityStatusReasons.add(entityStatusReason);
		}
		entityStatusReasonPojos.clear();
		if (entityStatusReasons != null && entityStatusReasons.size() > 0) {
			for (EntityStatusReason e1 : entityStatusReasons) {
				entityStatusReasonPojo = new EntityStatusReasonPojo();
				entityStatusReasonPojo = (EntityStatusReasonPojo) ObjectTransfer
						.transferObjects(e1, entityStatusReasonPojo);
				entityStatusReasonPojos.add(entityStatusReasonPojo);
			}
		}

		return entityStatusReasonPojos;
	}

	
	
	public static EntityMultipleValues saveEntityMultipleValues(
			EntityMultipleValues entityMultipleValues, Session session) {
		boolean sessionFlag = false;
		try {
			// check session
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if (entityMultipleValues.getPkEntityMultiple() != null) {
				session.merge(entityMultipleValues);
			} else {
				session.save(entityMultipleValues);
			}

			if (sessionFlag) {
				// close session, if session created in this method
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return entityMultipleValues;
	}

	
	public static EntityStatusReason saveReasonMultipleValues(
			EntityStatusReason entityStatusReasons, Session session) {
		boolean sessionFlag = false;
		try {
			// check session
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if (entityStatusReasons.getPkEntityStatusReason() != null) {
				session.merge(entityStatusReasons);
			} else {
				session.save(entityStatusReasons);
			}

			if (sessionFlag) {
				// close session, if session created in this method
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return entityStatusReasons;
	}

	
	
	public static List<EntityMultipleValuesPojo> getMultipleValuesByEntityId(
			Long entityId, String entityType, String entityMultipleType) {
		return getMultipleValuesByEntityId(entityId, entityType,
				entityMultipleType, null);
	}

	public static List<SitePojo> getMultipleValuesByErSite(Long entityId,
			String entityType, String entitySubType) {
		return getMultipleValuesByErSite(entityId, entityType, entitySubType,
				null);
	}

	public static List<EntityMultipleValuesPojo> getMultipleValuesByEntityId(
			Long entityId, String entityType, String entityMultipleType,
			Session session) {
		List<EntityMultipleValuesPojo> valuesPojos = new ArrayList<EntityMultipleValuesPojo>();
		EntityMultipleValuesPojo entityMultipleValuesPojo = null;
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;

				session.beginTransaction();
			}
			String sql = "from EntityMultipleValues";
			if (entityId != null) {
				sql += " where entityId=" + entityId;
			}
			if (entityType != null) {
				sql += " and entityType='" + entityType + "'";
			}
			if (entityMultipleType != null) {
				sql += " and entityMultipleType='" + entityMultipleType + "'";
			}
			Query query = session.createQuery(sql);
			List<EntityMultipleValues> list = (List<EntityMultipleValues>) query
					.list();
			for (EntityMultipleValues e : list) {
				entityMultipleValuesPojo = new EntityMultipleValuesPojo();
				entityMultipleValuesPojo = (EntityMultipleValuesPojo) ObjectTransfer
						.transferObjects(e, entityMultipleValuesPojo);
				valuesPojos.add(entityMultipleValuesPojo);
			}
		} catch (Exception e) {
			//log.debug("exception  in cbu helper --.>>" + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return valuesPojos;
	}

	public static List<SitePojo> getMultipleValuesByErSite(Long entityId,
			String entityType, String entitySubType, Session session) {
		List<SitePojo> valuesPojos = new ArrayList<SitePojo>();
		SitePojo sitePojo = null;
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;

				session.beginTransaction();
			}

			String sql = "from Site er";
			if (entityType != null && entitySubType != null) {
				sql += " where er.siteCodelstType in(select pkCodeId from CodeList where type ='"
						+ entityType
						+ "' and subType ='"
						+ entitySubType
						+ "')";
			}
			if (entityId != null) {
				sql += "and er.siteParent=" + entityId;
			}

			Query query = session.createQuery(sql);

			List<Site> list = (List<Site>) query.list();
			Site sitObj = null;

			for (Site e : list) {
				sitePojo = new SitePojo();
				sitePojo = (SitePojo) ObjectTransfer.transferObjects(e,
						sitePojo);
				valuesPojos.add(sitePojo);
			}

		} catch (Exception e) {
			//log.debug("exception  in cbu helper --.>>" + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return valuesPojos;
	}

	public static void transientMultipleValues(List<Long> ids,Long entityId) {
		transientMultipleValues(ids,entityId, null);
	}

	public static void transientMultipleValues(List<Long> ids,Long entityId, Session session) {
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;

				session.beginTransaction();
			}
			String sql = "Delete EntityMultipleValues e where e.entityId=? and e.entityMultipleValue in (:ids)";
			Query query = session.createQuery(sql);
			query.setParameter(0, entityId);
			query.setParameterList("ids", ids);
			query.executeUpdate();
			if (sessionFlag) {
				// close session, if session created in this method
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			//log.debug("exception  in cbu helper --.>>" + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
	}

	
	public static void transientMultipleReasonValues(List<Long> ids,Long entityStatus) {
		transientMultipleReasonValues(ids,entityStatus, null);
	}

	public static void transientMultipleReasonValues(List<Long> ids,Long entityStatus, Session session) {
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;

				session.beginTransaction();
			}
			String sql = "Delete EntityStatusReason e where e.fkEntityStatus=? and e.fkReasonId in (:ids)";
			Query query = session.createQuery(sql);
			query.setParameter(0, entityStatus);
			query.setParameterList("ids", ids);
			query.executeUpdate();
			if (sessionFlag) {
				// close session, if session created in this method
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			//log.debug("exception  in cbu helper --.>>" + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
	}
	
	
	public static String generateGuid() {
		return generateGuid(null);
	}

	public static String generateGuid(Session session) {
		String guid = null;
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;

				session.beginTransaction();
			}
			String sql = "select rawtohex(sys_guid()) from dual";
			SQLQuery query = session.createSQLQuery(sql);
			List list = query.list();
			if (list != null && list.size() > 0) {
				guid = (String) list.get(0);
			}
		} catch (Exception e) {
			//log.debug("exception  in cbu helper --.>>" + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return guid;
	}
	
	public static void updateCordSearchable(Long cordSearchable,Long entityId) {
		updateCordSearchable(cordSearchable,entityId,null);
	}

	public static void updateCordSearchable(Long cordSearchable,Long entityId,Session session) {
		String guid = null;
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;

				session.beginTransaction();
			}
			String sql = "Update CB_CORD set CORD_SEARCHABLE=? where PK_CORD=?";
			SQLQuery query = session.createSQLQuery(sql);
			query.setLong(0, cordSearchable);
			query.setLong(1, entityId);
			query.executeUpdate();
		} catch (Exception e) {
			//log.debug("exception  in cbu helper --.>>" + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			
			 try {
				session.getTransaction().commit();
			}catch (Exception e) {
				log.error(e.getMessage());
			    e.printStackTrace();
			 }finally{
				if(session.isOpen()){
				    session.close();
				}
			}
	  }		
	}
	
	public static void updateCordProgress(Long cordSearchable,Long entityId) {
		updateCordProgress(cordSearchable,entityId,null);
	}

	public static void updateCordProgress(Long cordProgress,Long entityId,Session session) {
		String guid = null;
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;

				session.beginTransaction();
			}
			String sql = "Update CB_CORD set CORD_ENTRY_PROGRESS=? where PK_CORD=?";
			SQLQuery query = session.createSQLQuery(sql);
			query.setLong(0, cordProgress);
			query.setLong(1, entityId);
			query.executeUpdate();
		} catch (Exception e) {
			//System.out.println("exception  in cbu helper --.>>" + e);
			log.error("Exception in CBUHelper : updateCordProgress() :: " + e);
			e.printStackTrace();
		} finally {
			
			 try {
				session.getTransaction().commit();
			}catch (Exception e) {
				log.error(e.getMessage());
			    e.printStackTrace();
			 }finally{
				if(session.isOpen()){
				    session.close();
				}
			}
	  }		
	}

	public static Long getTestIdByName(String testName) {
		return getTestIdByName(testName, null);
	}

	public static Long getTestIdByName(String testName, Session session) {
		Long testId = null;
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;

				session.beginTransaction();
			}
			String sql = "select PK_LABTEST from ER_LABTEST where LABTEST_NAME='"
					+ testName + "'";
			SQLQuery query = session.createSQLQuery(sql);
			testId = ((BigDecimal) query.list().get(0)).longValue();
		} catch (Exception e) {
			//log.debug("exception  in cbu helper --.>>" + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return testId;
	}

	public static AntigenPojo getAntigen(String genomicFormat, String locus,
			String typingMethod) {
		return getAntigen(genomicFormat, locus, typingMethod, null);
	}

	public static AntigenPojo getAntigen(String genomicFormat, String locus,
			String typingMethod, Session session) {
		boolean sessionFlag = false;
		AntigenPojo antigenPojo = null;
		Antigen antigen = null;
		AntigenEncoding antigenEncoding = null;
		List<AntigenEncoding> list = new ArrayList<AntigenEncoding>();
		List<Antigen> antigeList = new ArrayList<Antigen>();
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;

				session.beginTransaction();
			}
			//String sql = "select {ca.*} from cb_antigen_encod ca where ca.GENOMIC_FORMAT = ?";
			String sql = "select {ca.*} from cb_antigen_encod ca left join cb_antigen ct ON ct.pk_antigen = ca.fk_antigen where ct.hla_locus=? and ct.hla_typing_method=? and ca.GENOMIC_FORMAT = ?";
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity("ca", AntigenEncoding.class);
			query.setString(0, locus);
			query.setString(1, typingMethod);
			query.setString(2, genomicFormat.toUpperCase());//Change to upper case for bug 13519 so that all cases are validated that are entered through screen
			list = query.list();
			if (list != null && list.size() > 0) {
				for(AntigenEncoding a : list){
				  if(a.getActiveInd()==true && a.getValidInd()==true){
					  antigenEncoding = new AntigenEncoding();
					  antigenEncoding = list.get(0);
				  }
				}
			}
			if (antigenEncoding != null) {
				if (antigenEncoding.getActiveInd() == true
						&& antigenEncoding.getValidInd() == true) {
					String sql1 = "select {c.*} from cb_antigen c start with c.PK_ANTIGEN=? connect by nocycle prior c.alt_antigen_id=c.antigen_id";
					SQLQuery query1 = session.createSQLQuery(sql1);
					query1.addEntity("c", Antigen.class);
					query1.setLong(0, antigenEncoding.getFkAntigen());
					antigeList = query1.list();
					if (antigeList != null && antigeList.size() > 0) {
						for (Antigen an : antigeList) {
							if ((an.getAltAntigenId() == null || an
									.getAltAntigenId() == 0)
									&& typingMethod.equalsIgnoreCase(an
											.getHlaMethod())
									&& locus.equalsIgnoreCase(an.getHlaCode())) {
								antigen = new Antigen();
								antigenPojo = new AntigenPojo();
								antigenPojo = (AntigenPojo) ObjectTransfer
										.transferObjects(an, antigenPojo);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			//log.debug("exception  in cbu helper --.>>" + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return antigenPojo;
	}

	public static AntigenPojo getLatestAntigen(Long antigenId) {
		return getLatestAntigen(antigenId, null);
	}

	public static AntigenPojo getLatestAntigen(Long antigenId, Session session) {
		boolean sessionFlag = false;
		AntigenPojo antigenPojo = null;
		Antigen antigen = null;
		List<Antigen> antigeList = new ArrayList<Antigen>();
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;

				session.beginTransaction();
			}
			String sql1 = "select {c.*} from cb_antigen c start with c.PK_ANTIGEN=? connect by nocycle prior c.alt_antigen_id=c.antigen_id";
			SQLQuery query1 = session.createSQLQuery(sql1);
			query1.addEntity("c", Antigen.class);
			query1.setLong(0, antigenId);
			antigeList = query1.list();
			if (antigeList != null && antigeList.size() > 0) {
				for (Antigen an : antigeList) {
					if (an.getAltAntigenId() == null
							|| an.getAltAntigenId() == 0) {
						antigen = new Antigen();
						antigenPojo = new AntigenPojo();
						antigenPojo = (AntigenPojo) ObjectTransfer
								.transferObjects(an, antigenPojo);
					}
				}
			}
		} catch (Exception e) {
			//log.debug("exception  in cbu helper --.>>" + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return antigenPojo;
	}

	public static List<BestHLAPojo> getBestHla(Long entityType, Long entityId) {
		return getBestHla(entityType, entityId, null);
	}

	public static List<BestHLAPojo> getBestHla(Long entityType, Long entityId,
			Session session) {
		List<BestHLAPojo> hlaPojos = new ArrayList<BestHLAPojo>();
		BestHLAPojo hlaPojo = null;
		List<BestHLA> hlas = new ArrayList<BestHLA>();
		List<Object> objList=new ArrayList<Object>();
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "";
			if (entityType != null && entityId != null && entityId !=0l) {
				sql += "select {h.*},{ec1.*},{ec2.*} from CB_BEST_HLA h,cb_antigen_encod ec1,cb_antigen_encod ec2 where h.ENTITY_TYPE=? and h.ENTITY_ID=? "+
						" and h.fk_hla_antigeneid1=ec1.fk_antigen (+) and ( ec1.version = ( select max(ec1s.version) from cb_antigen_encod ec1s " +
						"where ec1.fk_antigen=ec1s.fk_antigen ) or ec1.version is null) and h.fk_hla_antigeneid2  =  ec2.fk_antigen (+) and (ec2.version =( select max(ec2s.version) " +
						"from cb_antigen_encod ec2s where ec2.fk_antigen=ec2s.fk_antigen  ) or ec2.version is null) order by h.CB_BEST_HLA_ORDER_SEQ desc nulls last";
			log.debug("sql in getBestHla:::::::::"+sql);
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity("h", BestHLA.class);
			query.addEntity("ec1", AntigenEncoding.class);
			query.addEntity("ec2", AntigenEncoding.class);
				query.setLong(0, entityType);
				query.setLong(1, entityId);
				objList = (List<Object>)query.list();
			} 
			
			if(objList!=null && objList.size()>0){
				for(Object o : objList){
					Object[] oArr = (Object[])o;
					BestHLA hla = null;
					AntigenEncoding encoding1 = null;
					AntigenEncoding encoding2 = null;
					if(oArr[0]!=null){
						hla = new BestHLA();
						encoding1 = new AntigenEncoding();
						encoding2 = new AntigenEncoding();
						hla = (BestHLA)oArr[0];
						encoding1 = (AntigenEncoding)oArr[1];
						encoding2 = (AntigenEncoding)oArr[2];
						if(encoding1!=null && hla!=null)
						    hla.setHlaType1(encoding1.getGenomicFormat());
						else
							hla.setHlaType1(null);
						if(encoding2!=null && hla!=null)
							hla.setHlaType2(encoding2.getGenomicFormat());
						else
							hla.setHlaType2(null);
					}
					if(hla!=null)
						hlas.add(hla);
				}
			}			
			//hlas = (List<BestHLA>) query.list();
			for (BestHLA h : hlas) {
				hlaPojo = new BestHLAPojo();
				hlaPojo = (BestHLAPojo) ObjectTransfer.transferObjects(h,
						hlaPojo);
				hlaPojos.add(hlaPojo);
			}
		} catch (Exception e) {
			//log.debug(" Exception in getCordsInProgress " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return hlaPojos;
	}

	public static List<TempHLAPojo> getTempHla(Long entityType,
			String registryId) {
		return getTempHla(entityType, registryId, null);
	}

	public static List<TempHLAPojo> getTempHla(Long entityType,
			String registryId, Session session) {
		List<TempHLAPojo> hlaPojos = new ArrayList<TempHLAPojo>();
		TempHLAPojo hlaPojo = null;
		List<TempHLA> hlas = new ArrayList<TempHLA>();
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "select {h.*} from CB_HLA_TEMP h ";
			if (entityType != null) {
				sql += " where h.ENTITY_TYPE=?";
			}
			if (registryId != null && entityType == null) {
				sql += " where h.CBU_REGISTRY_ID=?";
			} else if (registryId != null && entityType != null) {
				sql += " and h.CBU_REGISTRY_ID=?";
			}
			sql += " order by h.created_on desc";
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity("h", TempHLA.class);
			if (entityType != null) {
				query.setLong(0, entityType);
			}
			if (registryId != null && entityType == null) {
				query.setString(0, registryId);
			} else if (registryId != null && entityType != null) {
				query.setString(1, registryId);
			}
			hlas = (List<TempHLA>) query.list();
			for (TempHLA h : hlas) {
				hlaPojo = new TempHLAPojo();
				hlaPojo = (TempHLAPojo) ObjectTransfer.transferObjects(h,
						hlaPojo);
				hlaPojos.add(hlaPojo);
			}
		} catch (Exception e) {
			//log.debug(" Exception in getCordsInProgress " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return hlaPojos;
	}

	public static void deleteTempHla(Long entityType,
			String registryId) {
		deleteTempHla(entityType, registryId, null);
	}

	public static void deleteTempHla(Long entityType,
			String registryId, Session session) {
		List<TempHLAPojo> hlaPojos = new ArrayList<TempHLAPojo>();
		TempHLAPojo hlaPojo = null;
		List<TempHLA> hlas = new ArrayList<TempHLA>();
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "Delete from CB_HLA_TEMP h ";
			if (entityType != null) {
				sql += " where h.ENTITY_TYPE=?";
			}
			if (registryId != null && entityType == null) {
				sql += " where h.CBU_REGISTRY_ID=?";
			} else if (registryId != null && entityType != null) {
				sql += " and h.CBU_REGISTRY_ID=?";
			}
			SQLQuery query = session.createSQLQuery(sql);
			if (entityType != null) {
				query.setLong(0, entityType);
			}
			if (registryId != null && entityType == null) {
				query.setString(0, registryId);
			} else if (registryId != null && entityType != null) {
				query.setString(1, registryId);
			}
			int deleRows = query.executeUpdate();
			if(sessionFlag){
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			//log.debug(" Exception in getCordsInProgress " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		
	}

	public static void deleteHla(Long entityType,
			Long entityId) {
		deleteHla(entityType, entityId, null);
	}

	public static void deleteHla(Long entityType,
			Long entityId, Session session) {
		List<TempHLAPojo> hlaPojos = new ArrayList<TempHLAPojo>();
		TempHLAPojo hlaPojo = null;
		List<TempHLA> hlas = new ArrayList<TempHLA>();
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "Delete from CB_HLA h ";
			if (entityType != null) {
				sql += " where h.ENTITY_TYPE=?";
			}
			if (entityId != null && entityType == null) {
				sql += " where h.ENTITY_ID=?";
			} else if (entityId != null && entityType != null) {
				sql += " and h.ENTITY_ID=?";
			}
			SQLQuery query = session.createSQLQuery(sql);
			if (entityType != null) {
				query.setLong(0, entityType);
			}
			if (entityId != null && entityType == null) {
				query.setLong(0, entityId);
			} else if (entityId != null && entityType != null) {
				query.setLong(1, entityId);
			}
			int deleRows = query.executeUpdate();
			if(sessionFlag){
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			//log.debug(" Exception in getCordsInProgress " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		
	}

	
    public static DumnPojo saveOrUpdateDumnQuestions(DumnPojo dumnPojo){
    	return saveOrUpdateDumnQuestions(dumnPojo, null);
    }
    
    public static DumnPojo saveOrUpdateDumnQuestions(DumnPojo dumnPojo, Session session){
    	boolean sessionFlag = false;
    	Dumn dumn = new Dumn();
    	dumn = (Dumn)ObjectTransfer.transferObjects(dumnPojo, dumn);
    	dumn = (Dumn)new VelosUtil().setAuditableInfo(dumn);
    	try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if(dumn.getPkDumn()!=null){
				session.merge(dumn);
			}else{
				session.save(dumn);
			}
			dumnPojo = (DumnPojo)ObjectTransfer.transferObjects(dumn, dumnPojo);	
			if(sessionFlag){
				session.getTransaction().commit();
			}
    	} catch (Exception e) {
			//log.debug(" Exception in saveOrUpdateDumnQuestions " + e);
    		log.error(e.getMessage());
			e.printStackTrace();
		} finally {		
			 if(session.isOpen()){
			 session.close();			
			 }
		}
		return dumnPojo;
    }
    
    public static EligibilityDeclPojo saveOrUpdateFinalEligibilityQuestions(EligibilityDeclPojo eDeclPojo){
    	return saveOrUpdateFinalEligibilityQuestions(eDeclPojo, null);
    }
    
    public static EligibilityDeclPojo saveOrUpdateFinalEligibilityQuestions(EligibilityDeclPojo eDeclPojo, Session session){
    	boolean sessionFlag = false;
    	EligibilityDecl decl = new EligibilityDecl();
    	decl = (EligibilityDecl)ObjectTransfer.transferObjects(eDeclPojo, decl);
    	decl = (EligibilityDecl)new VelosUtil().setAuditableInfo(decl);
    	try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if(decl.getPkEligDecl()!=null){
				session.merge(decl);
			}else{
				session.save(decl);
			}
			eDeclPojo = (EligibilityDeclPojo)ObjectTransfer.transferObjects(decl, eDeclPojo);	
			if(sessionFlag){
				session.getTransaction().commit();
			}
    	} catch (Exception e) {
			//log.debug(" Exception in saveOrUpdateFinalEligibilityQuestions " + e);
    		log.error(e.getMessage());
			e.printStackTrace();
		} finally {		
			 if(session.isOpen()){
			 session.close();			
			 }
		}
		return eDeclPojo;
    }
    
    public static DumnPojo getDumnQuestionsByEntity(Long entityId,Long entityType,Long pkEligQues){
		return getDumnQuestionsByEntity(entityId,entityType,pkEligQues,null);		
    }
    
    public static DumnPojo getDumnQuestionsByEntity(Long entityId,Long entityType,Long pkEligQues,Session session){
    	boolean sessionFlag = false;	
    	DumnPojo dumnPojo = null;
    	Dumn dumn = null; 
    	List<Dumn> dumnList = new ArrayList<Dumn>();
    	try{
			if(session== null){
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				
				session.beginTransaction();
			}
			String sql1 = "From Dumn where " ;
			if(pkEligQues!=null){
				sql1 +=	"pkDumn="+pkEligQues+" and " ;
			}
			sql1 +=	"entityId="+entityId+" and entityType="+entityType;
			Query query1 = session.createQuery(sql1);
			dumnList = query1.list();
			if(dumnList!=null && dumnList.size()>0){
				for(Dumn an : dumnList){
				   dumn = new Dumn();								
				   dumnPojo = new DumnPojo();
				   dumnPojo = (DumnPojo)ObjectTransfer.transferObjects(an, dumnPojo);
			       
				}
			}
    	}
		catch (Exception e) {
				//log.debug("exception  in cbu helper --.>>"+e);
			log.error(e.getMessage());
				e.printStackTrace();
		}finally{
				if(session.isOpen()){
					session.close();
				}
		}
    	return dumnPojo;
    }
    
    public static EligibilityDeclPojo getEligibilityDeclQuestionsByEntity(Long entityId,Long entityType,Long pkEligQues){
		return getEligibilityDeclQuestionsByEntity(entityId,entityType,pkEligQues,null);		
    }
    
    public static EligibilityDeclPojo getEligibilityDeclQuestionsByEntity(Long entityId,Long entityType,Long pkEligQues,Session session){
    	boolean sessionFlag = false;	
    	EligibilityDeclPojo declPojo = null;
    	EligibilityDecl decl = null; 
    	List<EligibilityDecl> declList = new ArrayList<EligibilityDecl>();
    	try{
			if(session== null){
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				
				session.beginTransaction();
			}
			String sql1 = "From EligibilityDecl where " ;
			if(pkEligQues!=null){
				sql1 +=	"pkEligDecl="+pkEligQues+" and " ;
			}
			sql1 +=	"entityId="+entityId+" and entityType="+entityType;
			Query query1 = session.createQuery(sql1);
			declList = query1.list();
			if(declList!=null && declList.size()>0){
				for(EligibilityDecl an : declList){
				   decl = new EligibilityDecl();								
				   declPojo = new EligibilityDeclPojo();
				   declPojo = (EligibilityDeclPojo)ObjectTransfer.transferObjects(an, declPojo);
			       
				}
			}
    	}
		catch (Exception e) {
				//log.debug("exception  in cbu helper --.>>"+e);
			log.error(e.getMessage());
				e.printStackTrace();
		}finally{
				if(session.isOpen()){
					session.close();
				}
		}
    	return declPojo;
    }
    
    public static List<PatientHLAPojo> getPatientHla(Long entityType,Long entityId){
    	return getPatientHla(entityType,entityId,null);
    }
    
    public static List<PatientHLAPojo> getPatientHla(Long entityType,Long entityId, Session session){
    	List<PatientHLAPojo> hlaPojos = new ArrayList<PatientHLAPojo>();
    	PatientHLAPojo hlaPojo = null;
    	List<PatientHLA> hlas = new ArrayList<PatientHLA>();
    	boolean sessionFlag = false;
    	try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "select {h.*} from CB_PATIENT_HLA h ";
			if(entityType!=null && entityId!=null){
				sql += " where h.ENTITY_TYPE=? and h.ENTITY_ID=? ";
			}else if(entityType!=null && entityId==null){
				sql += " where h.ENTITY_TYPE=? ";
			}
			else if(entityType==null && entityId!=null){
				sql += " where h.ENTITY_ID=? ";
			}
			sql += " order by h.created_on desc";
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity("h",PatientHLA.class);
			if(entityType!=null && entityId!=null){
				query.setLong(0, entityType);
				query.setLong(1, entityId);
			}else if(entityType!=null && entityId==null){
				query.setLong(0, entityType);
			}
			else if(entityType==null && entityId!=null){
				query.setLong(0, entityId);
			}
			if(entityType!=null){
			query.setLong(0, entityType);
			}
			hlas = (List<PatientHLA>)query.list();
			for(PatientHLA h : hlas){
				hlaPojo = new PatientHLAPojo();
				hlaPojo = (PatientHLAPojo)ObjectTransfer.transferObjects(h,hlaPojo);
				hlaPojos.add(hlaPojo);
			}
    	} catch (Exception e) {
			//log.debug(" Exception in getPatientHla " + e);
    		log.error(e.getMessage());
			e.printStackTrace();
		} finally {		
			 if(session.isOpen()){
			 session.close();			
			 }
		}	
		return hlaPojos;    
    }
    
    public static List getAttachmentList(Long entityId, Long categoryId, Long subCategoryId,PaginateSearch paginateSearch,int i) {
    	String query = "";
    	if(subCategoryId==null){
    		query = "select er.pk_attachment, upload.created_on, upload.completion_date  from er_attachments er,cb_upload_info upload where er.pk_attachment = upload.fk_attachmentid and er.entity_id="+entityId+" and upload.fk_category="+categoryId+" order by upload.fk_attachmentid";
    	}else
    		query = "select er.pk_attachment, upload.created_on, upload.completion_date  from er_attachments er,cb_upload_info upload where er.pk_attachment = upload.fk_attachmentid and er.entity_id="+entityId+" and upload.fk_category="+categoryId+" and upload.fk_subcategory="+subCategoryId+" order by upload.fk_attachmentid";
    	
		return velosHelper.getListObject(query,paginateSearch,i);
	}
    
    public static List getAttachmentList(Long entityId, Long categoryId, int j,PaginateSearch paginateSearch,int i,Long revokedFlag) {
    	String query = "";
    	if(revokedFlag==null){
    		query = "select er.pk_attachment, upload.created_on, upload.completion_date  from er_attachments er,cb_upload_info upload where er.pk_attachment = upload.fk_attachmentid and er.entity_id="+entityId+" and upload.fk_category="+categoryId+" and er.deletedFlag is null order by upload.fk_attachmentid";
    	}
    	else{
    		query = "select er.pk_attachment, upload.created_on, upload.completion_date  from er_attachments er,cb_upload_info upload where er.pk_attachment = upload.fk_attachmentid and er.entity_id="+entityId+" and upload.fk_category="+categoryId+" and er.deletedFlag='1' order by upload.fk_attachmentid";
    	}
    	log.debug("query:::::::::::query::::::::::::::::::::::::::::;"+query);
		return velosHelper.getListObject(query,paginateSearch,i);
	}
    
    public static Long getLabGrpPkByGrpType(String grpType){
    	return getLabGrpPkByGrpType(grpType,null);
    }
    
    public static Long getLabGrpPkByGrpType(String grpType,Session session){
    	Long groupType = null;
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;

				session.beginTransaction();
			}
			String sql = "select PK_LABGROUP from ER_LABGROUP where GROUP_TYPE='"
					+ grpType + "'";
			SQLQuery query = session.createSQLQuery(sql);
			if(query.list().size()>0){
			groupType = ((BigDecimal) query.list().get(0)).longValue();
			}
			} catch (Exception e) {
			//log.debug("exception  in cbu helper --.>>" + e);
				log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return groupType;
    }
    
    public static Integer getCordByCountByMaternalRegistry(String maternalRegistryId){
    	return getCordByCountByMaternalRegistry(maternalRegistryId,null);
    }
    
    public static Integer getCordByCountByMaternalRegistry(String maternalRegistryId,Session session){
    	Integer count = 1;
    	boolean sessionFlag = false;
    	List<CdrCbu> list = null;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;

				session.beginTransaction();
			}
			String sql = "From CdrCbu where registryMaternalId='"+maternalRegistryId+"' and cordSearchable=1";
			Query query = session.createQuery(sql);
			list = new ArrayList<CdrCbu>();
			list = query.list();
			if(list!=null && list.size()>0){
				count = list.size();
				count = count+1;
			}
		} catch (Exception e) {
			//log.debug("exception  in cbu helper --.>>" + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return count;
    }
    
    public static Integer getDomainCount(String objectName,
			String criteria,String pkName) {
		return getDomainCount(objectName,criteria,pkName,null);
	}

	public static Integer getDomainCount(String objectName,
			String criteria,String pkName,Session session) {
		boolean sessionFlag = false;
		Integer count = 0;
		List<Object> returnList = null;
		try {
			// check session
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String query = "Select count("+pkName+") from " + objectName
					+ " where (deletedFlag = 0 or deletedFlag is null)";
			if (criteria != null && !criteria.trim().equals("")) {
				query += criteria;
			}
			//log.debug(" trace query " + query);
			returnList = (List<Object>) session.createQuery(query).list();

//			if (sessionFlag) {
//				 close session, if session created in this method
//				 session.getTransaction().commit();
//			}

		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
        if(returnList!=null && returnList.size()>0){
        	count = ((Long)returnList.get(0)).intValue();
        } 
		return count;
	}
	
	public static Integer saveCordListForSearch(List<Long> cordListForSearch){
		return saveCordListForSearch(cordListForSearch, null);
	}
	
	public static Integer saveCordListForSearch(List<Long> cordListForSearch,Session session){
		boolean sessionFlag = false;
		int updateCount = 0;
		try {
			// check session
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "Update CdrCbu set cordSearchable=1 where cordID in (";
			int count =1;
			if(cordListForSearch!=null){
				for(Long l : cordListForSearch){
					if(count==1){
						sql += l;
					}else{
						sql += ","+l;
					}
				}
			}
			sql += ")";
			Query query =  session.createQuery(sql);
			updateCount = query.executeUpdate();
			if (sessionFlag) {
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			//log.debug(" Exception in saveCordListForSearch " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}	
		return updateCount;
	}
	
	public static Map<Long,RecipientInfo> getRecipientInfoMap(List<Long> cordList){
		return getRecipientInfoMap(cordList,null);
	}
	
    public static Map<Long,RecipientInfo> getRecipientInfoMap(List<Long> cordList,Session session){
    	Map<Long,RecipientInfo> mapRecipient = new HashMap<Long, RecipientInfo>();
    	boolean sessionFlag = false;
    	RecipientInfo recipientInfo = null;
    	orderHeader orders = null;
		int updateCount = 0;
		try {
			// check session
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			
			String sql = "select {c.*},{e.*} from cb_receipant_info c left outer join er_order_receipant_info pre on(pre.fk_receipant=c.pk_receipant) left join er_order o on o.pk_order=pre.fk_order_id  left join ER_ORDER_HEADER e on e.pk_order_header=o.fk_order_header ";
			if(cordList!=null && cordList.size()>0){
				sql += " where e.order_entityid in (:cordList) and o.order_status not in(SELECT pk_codelst FROM er_codelst WHERE codelst_type  ='order_status' AND codelst_subtyp IN('close_ordr','resolved'))";
			}
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity("c",RecipientInfo.class);
			query.addEntity("e",orderHeader.class);
			if(cordList!=null && cordList.size()>0){
				query.setParameterList("cordList", cordList);
			}
			List<Object> objects = (List<Object>)query.list();
			if(objects!=null && objects.size()>0){
				for(Object o : objects){
					Object[] oArr = (Object[])o;
					recipientInfo = new RecipientInfo();
					orders = new orderHeader();
					if(oArr!=null){
						recipientInfo = (RecipientInfo)oArr[0];
						orders = (orderHeader)oArr[1];
						if(orders!=null && orders.getOrderEntityId()!=null && recipientInfo!=null){
						  mapRecipient.put(orders.getOrderEntityId(), recipientInfo);
						}
					}
				}
			}
		} catch (Exception e) {
			//log.debug(" Exception in getRecipientInfoMap " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}	
    	return mapRecipient;
	}
    
    public static Long getSequence(String seqName) {
		return getSequence(seqName,null);
	}

	public static Long getSequence(String seqName,Session session) {
		Long seq = null;
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;

				session.beginTransaction();
			}
			String sql = "select "+seqName+".nextval from dual";
			SQLQuery query = session.createSQLQuery(sql);
			List list = query.list();
			if (list != null && list.size() > 0) {
				seq = ((BigDecimal) list.get(0)).longValue();
			}
		} catch (Exception e) {
			//log.debug("exception  in cbu helper --.>>" + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return seq;
	}
	
	public static String getLookUpFilterData(String filterValue) {
		return getLookUpFilterData(filterValue,null);
	}

	public static String getLookUpFilterData(String filterValue,Session session) {
		String data = "";
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;

				session.beginTransaction();
			}
			String sql = "SELECT distinct(C.PK_CORD) as cordId FROM CB_CORD C Left Join CB_ADDITIONAL_IDS ca on c.pk_cord=ca.ENTITY_ID left join ER_ORDER_HEADER e ON C.PK_CORD=e.order_entityid left join er_order o on e.pk_order_header=o.fk_order_header LEFT JOIN (select receipant_id,fk_order_id from er_order_receipant_info pre,cb_receipant_info rec where pre.fk_receipant=rec.pk_receipant) pat on o.pk_order=pat.fk_order_id WHERE replace(lower(C.CORD_REGISTRY_ID),'-','') like lower('%"+filterValue+"%') OR replace(lower(C.CORD_LOCAL_CBU_ID),'-','') like lower('%"+filterValue+"%')" +
					"OR replace(lower(C.CORD_ID_NUMBER_ON_CBU_BAG),'-','') like lower('%"+filterValue+"%') OR replace(lower(C.REGISTRY_MATERNAL_ID),'-','') like lower('%"+filterValue+"%')" +
					"OR replace(lower(C.MATERNAL_LOCAL_ID),'-','') like lower('%"+filterValue+"%') OR replace(lower(C.CORD_ISBI_DIN_CODE),'-','') like lower('%"+filterValue+"%')" +
					"OR replace(lower(ca.CB_ADDITIONAL_ID),'-','') like lower('%"+filterValue+"%') OR replace(lower(pat.RECEIPANT_ID),'-','') like lower('%"+filterValue+"%') and o.order_status not in(SELECT pk_codelst FROM er_codelst WHERE codelst_type  ='order_status' AND codelst_subtyp IN('close_ordr','resolved'))";
			SQLQuery query = session.createSQLQuery(sql);
			List<Object> list = query.list();
			Integer count = 0;
			if (list != null && list.size() > 0) {
				for(Object o : list){
					if(count==0){
						data = data + ((BigDecimal)o).longValue();
					}else{
						data = data +","+ ((BigDecimal)o).longValue();
					}
					count++;
				}
				
			}
		} catch (Exception e) {
			//log.debug("exception  in cbu helper --.>>" + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return data;
	}
	
	public static List<Object> getCordListAsobject(String sql,PaginateSearch paginateSearch) {
		return getCordListAsobject(sql,paginateSearch, null);
	}

	public static List<Object> getCordListAsobject(String sql,PaginateSearch paginateSearch, Session session) {
		
		boolean sessionFlag = false;
		List<Object> returnList = null;
		if(paginateSearch==null)
			returnList = new ArrayList<Object>(5);
		else
			returnList = new ArrayList<Object>(paginateSearch.getiShowRows());	
		
		try {
			// check session
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			Query query = session.createSQLQuery(sql.toString());
			
			if(paginateSearch!=null){
				query.setFirstResult(paginateSearch.getiPageNo());
				query.setMaxResults(paginateSearch.getiShowRows());
			}
			log.debug(" trace query for lookup& advance lookup::::::::" + query);
			returnList = (List<Object>) query.list();
			
//			if (sessionFlag) {
//				 close session, if session created in this method
//				 session.getTransaction().commit();
//			}

		} catch (Exception e) {
			//log.debug(" Exception in getCordListAsobject " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}

		return returnList;
	}
	
	public static boolean getAssDetailsForLabSum(Long CordId,Long responceId,Long subEntityType,String questionNo){
		return getAssDetailsForLabSum(CordId,responceId,subEntityType,questionNo,null);
	}

	public static boolean getAssDetailsForLabSum(Long CordId,Long responceId,Long subEntityType,String questionNo,Session session) {
		boolean sessionFlag = false;
		boolean containFlag=false;
		try {
			// check session
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "SELECT PK_ASSESSMENT from CB_ASSESSMENT where ENTITY_ID="+CordId+" and SUB_ENTITY_ID="+responceId+"and SUB_ENTITY_TYPE="+subEntityType+"and COLUMN_REFERENCE='"+questionNo+"'";
			SQLQuery query = session.createSQLQuery(sql);
			List<Object> list = query.list();
			if(list!=null && list.size()>0){
				containFlag=true;
			}
		} catch (Exception e) {
			//log.debug(" Exception in getCordListAsobject " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return containFlag;
	}
	
		
	
	public static EligibilityNewDeclPojo saveOrUpdateNewFinalEligibilityQuestions(EligibilityNewDeclPojo eDeclPojo){
    	return saveOrUpdateNewFinalEligibilityQuestions(eDeclPojo, null);
    }
    
    public static EligibilityNewDeclPojo saveOrUpdateNewFinalEligibilityQuestions(EligibilityNewDeclPojo eDeclPojo, Session session){
    	boolean sessionFlag = false;
    	EligibilityNewDecl decl = new EligibilityNewDecl();
    	decl = (EligibilityNewDecl)ObjectTransfer.transferObjects(eDeclPojo, decl);
    	decl = (EligibilityNewDecl)new VelosUtil().setAuditableInfo(decl);
    	try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if(decl.getPkNewEligDecl()!=null){
				System.out.println("Merge new questionsss!!!!!!!!!!");
				session.merge(decl);
			}else{
				System.out.println("Save new questionsss!!!!!!!!!!");
				session.save(decl);
			}
			eDeclPojo = (EligibilityNewDeclPojo)ObjectTransfer.transferObjects(decl, eDeclPojo);	
			if(sessionFlag){
				session.getTransaction().commit();
			}
    	} catch (Exception e) {
			//log.debug(" Exception in saveOrUpdateFinalEligibilityQuestions " + e);
    		log.error(e.getMessage());
			e.printStackTrace();
		} finally {		
			 if(session.isOpen()){
			 session.close();			
			 }
		}
		return eDeclPojo;
    }
    
    public static EligibilityNewDeclPojo getEligibilityNewDeclQuestionsByEntity(Long entityId,Long entityType,Long pkEligQues){
		return getEligibilityNewDeclQuestionsByEntity(entityId,entityType,pkEligQues,null);		
    }
    
    public static EligibilityNewDeclPojo getEligibilityNewDeclQuestionsByEntity(Long entityId,Long entityType,Long pkEligQues,Session session){
    	boolean sessionFlag = false;	
    	EligibilityNewDeclPojo declNewPojo = null;
    	EligibilityNewDecl decl = null; 
    	List<EligibilityNewDecl> declNewList = new ArrayList<EligibilityNewDecl>();
    	try{
			if(session== null){
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				
				session.beginTransaction();
			}
			System.out.println("getEligibilityNewDeclQuestionsByEntity!!!!!!!!!!!!!!"+pkEligQues);
			//String sql1 = "From EligibilityDecl where pkEligDecl="+pkEligQues+" and entityId="+entityId+" and entityType="+entityType;
			//String sql1 = "From EligibilityDecl where entityId="+entityId+" and entityType="+entityType;
			String sql1 = "From EligibilityNewDecl where " ;
			if(pkEligQues!=null){
				sql1 +=	"pkNewEligDecl="+pkEligQues+" and " ;
			}
			sql1 +=	"entityId="+entityId+" and entityType="+entityType;
			Query query1 = session.createQuery(sql1);
			System.out.println("Query is !!!!!!!!!!!!!!"+query1);
			declNewList = query1.list();
			System.out.println("size is !!!!!!!!!!!!!"+declNewList.size());
			if(declNewList!=null && declNewList.size()>0){
				for(EligibilityNewDecl an : declNewList){
				   decl = new EligibilityNewDecl();								
				   declNewPojo = new EligibilityNewDeclPojo();
				   declNewPojo = (EligibilityNewDeclPojo)ObjectTransfer.transferObjects(an, declNewPojo);
			       
				}
			}
    	}
		catch (Exception e) {
				//log.debug("exception  in cbu helper --.>>"+e);
			log.error(e.getMessage());
				e.printStackTrace();
		}finally{
				if(session.isOpen()){
					session.close();
				}
		}
    	return declNewPojo;
    }
    
    public static boolean isFkQuestionExists(Long fkEligQuestion, Long fkCordId){
		return isFkQuestionExists(fkEligQuestion,fkCordId,null);
	}

	public static boolean isFkQuestionExists(Long fkEligQuestion, Long fkCordId,Session session) {
		boolean sessionFlag = false;
		boolean containFlag=false;
		try {
			// check session
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "SELECT PK_CORD_FINAL_REVIEW from CB_CORD_FINAL_REVIEW where FK_CORD_ID="+fkCordId+" and FK_FINAL_ELIG_QUES="+fkEligQuestion;
			SQLQuery query = session.createSQLQuery(sql);
			List<Object> list = query.list();
			if(list!=null && list.size()>0){
				containFlag=true;
			}
		} catch (Exception e) {
			//log.debug(" Exception in getCordListAsobject " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return containFlag;
	}
	
	public static void deleteFinalReviewDocs(Long entityType,
			Long entityId) {
		    deleteFinalReviewDocs(entityType, entityId, null);
	}

	public static void deleteFinalReviewDocs(Long entityType,
			Long entityId, Session session) {	
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "Delete from CB_FINAL_REV_UPLOAD_INFO h ";
			if (entityType != null) {
				sql += " where h.ENTITY_TYPE=?";
			}
			if (entityId != null && entityType == null) {
				sql += " where h.ENTITY_ID=?";
			} else if (entityId != null && entityType != null) {
				sql += " and h.ENTITY_ID=?";
			}
			SQLQuery query = session.createSQLQuery(sql);
			if (entityType != null) {
				query.setLong(0, entityType);
			}
			if (entityId != null && entityType == null) {
				query.setLong(0, entityId);
			} else if (entityId != null && entityType != null) {
				query.setLong(1, entityId);
			}
			int deleRows = query.executeUpdate();
			if(sessionFlag){
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			//log.debug(" Exception in getCordsInProgress " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		
	}
	
	public static List<FinalRevUploadInfoPojo> saveFinalReviewDocs(List<FinalRevUploadInfoPojo> reviewPojos,Boolean fromUI) {
		return saveFinalReviewDocs(reviewPojos,fromUI, null);
	}

	public static List<FinalRevUploadInfoPojo> saveFinalReviewDocs(List<FinalRevUploadInfoPojo> reviewPojos
			,Boolean fromUI,Session session) {
		List<FinalRevUploadInfo> reviews = new ArrayList<FinalRevUploadInfo>();
		FinalRevUploadInfo review = null;
		FinalRevUploadInfoPojo reviewPojo = null;
		for (FinalRevUploadInfoPojo hp : reviewPojos) {
			review = new FinalRevUploadInfo();
			review = (FinalRevUploadInfo) ObjectTransfer.transferObjects(hp, review);
			if(fromUI)
				review = (FinalRevUploadInfo) new VelosUtil().setAuditableInfo(review);
			reviews.add(review);
		}
		boolean sessionFlag = false;
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			for (FinalRevUploadInfo h : reviews) {
				if (h.getPkFinalRevUploadInfo() != null) {
					session.merge(h);
				} else {
					session.save(h);
				}
			}
			if (reviews != null && reviews.size() > 0) {
				reviewPojos.clear();
				for (FinalRevUploadInfo hl : reviews) {
					reviewPojo = (FinalRevUploadInfoPojo) ObjectTransfer.transferObjects(hl,
							reviewPojo);
					reviewPojos.add(reviewPojo);
				}
			}
			if (sessionFlag) {
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			//log.debug(" Exception in saveOrUpdateHla " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return reviewPojos;
	}
	public static List<FinalRevUploadInfo> getFinalReviewUploadDocLst(Long cordID,Long entityType){
		return getFinalReviewUploadDocLst(cordID,entityType,null);
	}
	
	public static List<FinalRevUploadInfo> getFinalReviewUploadDocLst(Long cordID,Long entityType,Session session){
		List<FinalRevUploadInfo> returnList = new ArrayList<FinalRevUploadInfo>();
    	boolean sessionFlag = false;
    	try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "From FinalRevUploadInfo fu where fu.entityId="+cordID+"and fu.entityType="+entityType+"and fu.deletedFlag is null order by fu.createdOn DESC";
			returnList = (List<FinalRevUploadInfo>) session.createQuery(sql).list();

    	} catch (Exception e) {
			//log.debug(" Exception in getRevokeUnitReportLst " + e);
    		log.error(e.getMessage());
			e.printStackTrace();
		} finally {						
			if(session.isOpen()){
				 session.close();		
			}
		}
		return returnList;
	}
	
	
	public static List<Object> updateCdrCbuObject(Object obj){
		return updateCdrCbuObject(obj,null);
	}
	
	public static List<Object> updateCdrCbuObject(Object obj, Session session){
		
		boolean sessionFlag = false;
		CdrCbu cdrCbu = new CdrCbu();
		cdrCbu = (CdrCbu) ObjectTransfer.transferObjects((CdrCbuPojo)obj, cdrCbu);
		
	
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if (cdrCbu.getCordID() != null) {
				if(cdrCbu.getFkCbbId()!=null && cdrCbu.getFkCbbId()!=-1){
					Site s = new Site();
					s.setSiteId(cdrCbu.getFkCbbId());
					s = (Site)session.get(Site.class,s.getSiteId());
					cdrCbu.setSite(s);
				}
				session.merge(cdrCbu);
			 } 
			
			if (sessionFlag) {
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			//log.debug(" Exception in saveOrUpdateCordInfo " + e);
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return null;
		
	}
	
	public static List validateDataExistence(StringBuilder sqlQuery){
		return validateDataExistence(sqlQuery,null);
	}
	
	public static List validateDataExistence(StringBuilder sqlQuery,Session session){
		List returnList=new ArrayList();
    	boolean sessionFlag = false;
    	try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			returnList = (ArrayList) session.createSQLQuery(sqlQuery.toString()).list();
			// SQLQuery query = session.createSQLQuery(sqlQuery.toString());
	        // query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
	        // returnList = query.list();

    	} catch (Exception e) {
			//log.debug(" Exception in getRevokeUnitReportLst " + e);
    		log.error(e.getMessage());
			e.printStackTrace();
		} finally {						
			if(session.isOpen()){
				 session.close();		
			}
		}
		return returnList;
		
	}

}