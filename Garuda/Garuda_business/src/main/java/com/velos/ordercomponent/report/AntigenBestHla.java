package com.velos.ordercomponent.report;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.jdbc.Work;


import com.velos.ordercomponent.business.pojoobjects.BestHLAPojo;
import com.velos.ordercomponent.business.pojoobjects.HLAFilterPojo;


/**
 * @author Anurag Upadhyay It maintain only single row (Summary Of HLA) there is
 *         no sequence case is aplied in best HLA
 */
public class AntigenBestHla implements Work {

	public static final Log log = LogFactory.getLog(AntigenHLA.class);

	private String query;
	private Map<Long, List<BestHLAPojo>> bestHlasMap;
	private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	private Map<Long, String> cdlst;
	Map<Long, HLAFilterPojo> bestHlas;
	
	public AntigenBestHla(String query, Map<Long, String> cdlst) {

		this.query = query;
		this.cdlst = cdlst;
	}

	@Override
	public void execute(Connection connection) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement(query);
			ps.setFetchSize(50000);
			rs = ps.executeQuery();
			System.out.println("Query Result Returned");
			long startTime = System.currentTimeMillis();
			if (cdlst == null)
				bestHlasMap = prepHlaObjects(rs);
			else
				bestHlas = prepFilterHlaObjects(rs, cdlst);

			long endTime = System.currentTimeMillis();
			System.out.println();
			System.out.println((endTime - startTime) / 1000 + " : "
					+ (endTime - startTime) % 1000 + " MM:SS\n\n");
		}

		catch (SQLException e) {

			printException(e);

		} catch (ParseException e) {

			printException(e);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			printException(e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					printException(e);
				}
			}
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					printException(e);
				}
			}/*
			 * if(bw != null){
			 * 
			 * try { //bw.close(); } catch( IOException e ) {printException(e);}
			 * }
			 */
		}
	}

	private void printException(Exception e) {

		log.error(e.getMessage());
		e.printStackTrace();

	}

	private Map<Long, List<BestHLAPojo>> prepHlaObjects(ResultSet rs)
			throws SQLException, ParseException, IOException {

		Map<Long, List<BestHLAPojo>> bestHlasMap = new HashMap<Long, List<BestHLAPojo>>(
				45748);
		System.out.println(bestHlasMap.size());
		BestHLAPojo bhlaPojo;
		List<BestHLAPojo> bhlas;
		// fw = new FileWriter(file.getAbsoluteFile());
		// bw = new BufferedWriter(fw,1048576);

		while (rs.next()) {

			bhlaPojo = new BestHLAPojo();

			bhlaPojo.setPkHla(rs.getLong(1));
			bhlaPojo.setFkHlaCodeId(rs.getLong(2));
			bhlaPojo.setFkHlaMethodId(rs.getLong(3));
			bhlaPojo.setHlaRecievedDate(rs.getDate(4) != null ? format.parse(rs
					.getDate(4).toString()) : null);
			bhlaPojo.setHlaTypingDate(rs.getDate(5) != null ? format.parse(rs
					.getDate(5).toString()) : null);
			bhlaPojo.setFkentityCordId(rs.getLong(6));
			bhlaPojo.setFkEntityCodeType(rs.getLong(7));
			bhlaPojo.setHlaType1(rs.getString(8));
			bhlaPojo.setHlaType2(rs.getString(9));
			bhlaPojo.setFkSource(rs.getLong(10));
			bhlaPojo.setuName(rs.getString(11));
			bhlaPojo.setCreatedOn(rs.getDate(12) != null ? format.parse(rs
					.getDate(12).toString()) : null);
			bhlaPojo.setBestHlaOrderSeq(rs.getLong(13));

			if (bestHlasMap.containsKey(bhlaPojo.getFkentityCordId())) {
				// bw.write(bhlaPojo.toString());
				// bw.write("\n");
				bhlas = bestHlasMap.get(bhlaPojo.getFkentityCordId());
				bhlas.add(bhlaPojo);
				bestHlasMap.put(bhlaPojo.getFkentityCordId(), bhlas);
			} else {
				// bw.write("*****************************\n");
				bhlas = new ArrayList<BestHLAPojo>();
				bhlas.add(bhlaPojo);
				// bw.write(bhlaPojo.toString());
				// bw.write("\n");
				bestHlasMap.put(bhlaPojo.getFkentityCordId(), bhlas);
			}
		}
		System.out.println(bestHlasMap.size());
		return bestHlasMap;
	}

	public enum HLA {
		A, B, C, DRB1, DRB3, DRB4, DRB5, DQB1, DPB1, licence, eligibility, EXCLUDE
	}

	private Map<Long, HLAFilterPojo> prepFilterHlaObjects(ResultSet rs, Map<Long, String> codelst) throws SQLException, ParseException,
			IOException {

		// Map<Long, List<BestHLAPojo>> bestHlasMap = new HashMap<Long,
		// List<BestHLAPojo>>();HLAFilterPojo
		Map<Long, HLAFilterPojo> bestHlasMap = new HashMap<Long, HLAFilterPojo>();
		HLAFilterPojo hfp = null;
		// BestHLAPojo bhlaPojo;
		// List<BestHLAPojo> bhlas;
		Long entityId;
		Long codeId;
		String codeType = null;

		while (rs.next()) {

			entityId = rs.getLong(6);
			codeId = rs.getLong(2);

			if ((hfp = bestHlasMap.get(entityId)) != null) {
				// hfp = bestHlasMap.get(key)
			} else {
				hfp = new HLAFilterPojo();
				hfp.receivedDate = rs.getDate(4) != null ? format.parse(rs.getDate(4).toString()) : null;
				hfp.typingDate = rs.getDate(5) != null ? format.parse(rs.getDate(5).toString()) : null;
				hfp.user = rs.getString(11);
				bestHlasMap.put(entityId, hfp);
			}
			/*
			 * if(codelst.get(codeId)== null ){ System.out.println(codeId); }
			 */

			codeType = codelst.get(codeId);
			codeType = codeType == null ? "EXCLUDE" : codeType;

			switch (HLA.valueOf(codeType)) {

			case A:
				hfp.aType1 = rs.getString(8);
				hfp.aType2 = rs.getString(9);
				break;

			case B:
				hfp.bType1 = rs.getString(8);
				hfp.bType2 = rs.getString(9);
				break;

			case C:
				hfp.cType1 = rs.getString(8);
				hfp.cType2 = rs.getString(9);
				break;

			case DRB1:
				hfp.drb1Type2 = rs.getString(8);
				hfp.drb1Type2 = rs.getString(9);
				break;

			case DRB3:
				hfp.drb3Type1 = rs.getString(8);
				hfp.drb3Type2 = rs.getString(9);
				break;

			case DRB4:
				hfp.drb4Type1 = rs.getString(8);
				hfp.drb4Type2 = rs.getString(9);
				break;

			case DRB5:
				hfp.drb5Type1 = rs.getString(8);
				hfp.drb5Type2 = rs.getString(9);
				break;

			case DQB1:
				hfp.dqb1Type1 = rs.getString(8);
				hfp.dqb1Type2 = rs.getString(9);
				break;

			case DPB1:
				hfp.dpb1Type1 = rs.getString(8);
				hfp.dpb1Type2 = rs.getString(9);
				break;

			case EXCLUDE:
				break;

			}
		}
		System.out.println("Map Size ::::::::::" + bestHlasMap.size());
		return bestHlasMap;
	}

	public Map<Long, HLAFilterPojo> getBestHlas() {
		return bestHlas;
	}


	public Map<Long, List<BestHLAPojo>> getBestHlasMap() {
		return bestHlasMap;
	}

}
