package com.velos.ordercomponent.business.pojoobjects;

import javax.persistence.Transient;

import com.velos.ordercomponent.business.domain.Address;
import com.velos.ordercomponent.business.domain.Person;

/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class CBBPojo extends AuditablePojo{

	private Long pkCbbId;
	private String cbbId;
	private Boolean enableEditAntigens;
	private Boolean displayMaternalAntigens;
	private Boolean enableCbuInventFeature;
	private Long defaultCTShipper;
	private Long defaultORShipper;
	private Boolean defaultMicroContamDateToProcDate;
	private Boolean defaultCfuTestDateToProcDate;
	private Boolean defaultViabTestDateToProcDate;
	private Boolean cordInfoLock;
	private Long personId;
	private Long addressId;
	private Long siteId;	
	private Long allowMember;
	private PersonPojo person;
	private Boolean cdrUser;
	private Long localcbuassgnmentid;
	private Long localmaternalassgnmentid;
	private String localCbuIdPrefix;
	private Long localCbuIdStrtNum;
	private String localMaternalIdPrefix;
	private Long localMaternalIdStrtNum;
	private Boolean useOwnDumn;
	private Boolean isDomesticCBB;
	private Boolean useFdoe;
	private Boolean enableAutoCompleteCordEntry;
	private Boolean enableAutoCompleteAckResTask;
	private Long fkDefaultShippingPaperWorkLocation;
	private AddressPojo address;
	private AddressPojo addressDryShipper;	
	private Long fkCbuPickUpAddress;
	private String attention;
	private String attentiondDryShipper;	
	private Long fkDateFormat;
	private Long fkTimeFormat;
	private Long fkAccreditation;
	private Long fkCbuStorage;
	private Long fkCbuCollection;
	private Long fkDryshipperAdd;
	private String pickUpAddSplInstuction;
	private String pickUpAddLastName;	
	private String pickUpAddFirstName;
	private Boolean cbuAssessment;
	private Boolean unitReport;
	private String bmdw;
	private String emdis;
	private Boolean nmdpResearch;	
	private Boolean criReqElgQuest;
	
	
	
	public Boolean getCriReqElgQuest() {
		return criReqElgQuest;
	}
	public void setCriReqElgQuest(Boolean criReqElgQuest) {
		this.criReqElgQuest = criReqElgQuest;
	}
	public Boolean getCbuAssessment() {
		return cbuAssessment;
	}
	public void setCbuAssessment(Boolean cbuAssessment) {
		this.cbuAssessment = cbuAssessment;
	}
	public Boolean getUnitReport() {
		return unitReport;
	}
	public void setUnitReport(Boolean unitReport) {
		this.unitReport = unitReport;
	}
	
	public String getBmdw() {
		return bmdw;
	}
	public void setBmdw(String bmdw) {
		this.bmdw = bmdw;
	}
	public String getEmdis() {
		return emdis;
	}
	public void setEmdis(String emdis) {
		this.emdis = emdis;
	}
	public String getLocalCbuIdPrefix() {
		return localCbuIdPrefix;
	}
	public void setLocalCbuIdPrefix(String localCbuIdPrefix) {
		this.localCbuIdPrefix = localCbuIdPrefix;
	}
	public Long getLocalCbuIdStrtNum() {
		return localCbuIdStrtNum;
	}
	public void setLocalCbuIdStrtNum(Long localCbuIdStrtNum) {
		this.localCbuIdStrtNum = localCbuIdStrtNum;
	}
	public String getLocalMaternalIdPrefix() {
		return localMaternalIdPrefix;
	}
	public void setLocalMaternalIdPrefix(String localMaternalIdPrefix) {
		this.localMaternalIdPrefix = localMaternalIdPrefix;
	}
	public Long getLocalMaternalIdStrtNum() {
		return localMaternalIdStrtNum;
	}
	public void setLocalMaternalIdStrtNum(Long localMaternalIdStrtNum) {
		this.localMaternalIdStrtNum = localMaternalIdStrtNum;
	}
	public Long getLocalcbuassgnmentid() {
		return localcbuassgnmentid;
	}
	public void setLocalcbuassgnmentid(Long localcbuassgnmentid) {
		this.localcbuassgnmentid = localcbuassgnmentid;
	}
	public Long getLocalmaternalassgnmentid() {
		return localmaternalassgnmentid;
	}
	public void setLocalmaternalassgnmentid(Long localmaternalassgnmentid) {
		this.localmaternalassgnmentid = localmaternalassgnmentid;
	}
	public Long getPkCbbId() {
		return pkCbbId;
	}
	public void setPkCbbId(Long pkCbbId) {
		this.pkCbbId = pkCbbId;
	}
	public String getCbbId() {
		return cbbId;
	}
	public void setCbbId(String cbbId) {
		this.cbbId = cbbId;
	}
	public Boolean getEnableEditAntigens() {
		return enableEditAntigens;
	}
	public void setEnableEditAntigens(Boolean enableEditAntigens) {
		this.enableEditAntigens = enableEditAntigens;
	}
	public Boolean getDisplayMaternalAntigens() {
		return displayMaternalAntigens;
	}
	public void setDisplayMaternalAntigens(Boolean displayMaternalAntigens) {
		this.displayMaternalAntigens = displayMaternalAntigens;
	}
	public Boolean getEnableCbuInventFeature() {
		return enableCbuInventFeature;
	}
	public void setEnableCbuInventFeature(Boolean enableCbuInventFeature) {
		this.enableCbuInventFeature = enableCbuInventFeature;
	}
	public Long getDefaultCTShipper() {
		return defaultCTShipper;
	}
	public void setDefaultCTShipper(Long defaultCTShipper) {
		this.defaultCTShipper = defaultCTShipper;
	}
	public Long getDefaultORShipper() {
		return defaultORShipper;
	}
	public void setDefaultORShipper(Long defaultORShipper) {
		this.defaultORShipper = defaultORShipper;
	}
	public Boolean getDefaultMicroContamDateToProcDate() {
		return defaultMicroContamDateToProcDate;
	}
	public void setDefaultMicroContamDateToProcDate(
			Boolean defaultMicroContamDateToProcDate) {
		this.defaultMicroContamDateToProcDate = defaultMicroContamDateToProcDate;
	}
	public Boolean getDefaultCfuTestDateToProcDate() {
		return defaultCfuTestDateToProcDate;
	}
	public void setDefaultCfuTestDateToProcDate(Boolean defaultCfuTestDateToProcDate) {
		this.defaultCfuTestDateToProcDate = defaultCfuTestDateToProcDate;
	}
	public Boolean getDefaultViabTestDateToProcDate() {
		return defaultViabTestDateToProcDate;
	}
	public void setDefaultViabTestDateToProcDate(
			Boolean defaultViabTestDateToProcDate) {
		this.defaultViabTestDateToProcDate = defaultViabTestDateToProcDate;
	}
	public Boolean getCordInfoLock() {
		return cordInfoLock;
	}
	public void setCordInfoLock(Boolean cordInfoLock) {
		this.cordInfoLock = cordInfoLock;
	}
	
	public Long getPersonId() {
		return personId;
	}
	public void setPersonId(Long personId) {
		this.personId = personId;
	}
	public Long getAddressId() {
		return addressId;
	}
	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}
	public PersonPojo getPerson() {
		return person;
	}
	public void setPerson(PersonPojo person) {
		this.person = person;
	}
	public Long getAllowMember() {
		return allowMember;
	}
	public void setAllowMember(Long allowMember) {
		this.allowMember = allowMember;
	}
	public Long getSiteId() {
		return siteId;
	}
	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}
	public Boolean getCdrUser() {
		return cdrUser;
	}
	public void setCdrUser(Boolean cdrUser) {
		this.cdrUser = cdrUser;
	}
	public Boolean getUseOwnDumn() {
		return useOwnDumn;
	}
	public void setUseOwnDumn(Boolean useOwnDumn) {
		this.useOwnDumn = useOwnDumn;
	}
	public Boolean getIsDomesticCBB() {
		return isDomesticCBB;
	}
	public void setIsDomesticCBB(Boolean isDomesticCBB) {
		this.isDomesticCBB = isDomesticCBB;
	}
	public Boolean getUseFdoe() {
		return useFdoe;
	}
	public void setUseFdoe(Boolean useFdoe) {
		this.useFdoe = useFdoe;
	}
	public Boolean getEnableAutoCompleteCordEntry() {
		return enableAutoCompleteCordEntry;
	}
	public void setEnableAutoCompleteCordEntry(Boolean enableAutoCompleteCordEntry) {
		this.enableAutoCompleteCordEntry = enableAutoCompleteCordEntry;
	}
	public Boolean getEnableAutoCompleteAckResTask() {
		return enableAutoCompleteAckResTask;
	}
	public void setEnableAutoCompleteAckResTask(Boolean enableAutoCompleteAckResTask) {
		this.enableAutoCompleteAckResTask = enableAutoCompleteAckResTask;
	}
	public Long getFkDefaultShippingPaperWorkLocation() {
		return fkDefaultShippingPaperWorkLocation;
	}
	public void setFkDefaultShippingPaperWorkLocation(
			Long fkDefaultShippingPaperWorkLocation) {
		this.fkDefaultShippingPaperWorkLocation = fkDefaultShippingPaperWorkLocation;
	}
	
	public AddressPojo getAddress() {
		return address;
	}
	public void setAddress(AddressPojo address) {
		this.address = address;
	}		
	public AddressPojo getAddressDryShipper() {
		return addressDryShipper;
	}
	public void setAddressDryShipper( AddressPojo addressDryShipper) {
		this.addressDryShipper = addressDryShipper;
	}		
	public Long getFkCbuPickUpAddress() {
		return fkCbuPickUpAddress;
	}
	public void setFkCbuPickUpAddress(Long fkCbuPickUpAddress) {
		this.fkCbuPickUpAddress = fkCbuPickUpAddress;
	}
	public String getAttention() {
		return attention;
	}
	public void setAttention(String attention) {
		this.attention = attention;
	}
	public Long getFkDateFormat() {
		return fkDateFormat;
	}
	public void setFkDateFormat(Long fkDateFormat) {
		this.fkDateFormat = fkDateFormat;
	}
	public Long getFkTimeFormat() {
		return fkTimeFormat;
	}
	public void setFkTimeFormat(Long fkTimeFormat) {
		this.fkTimeFormat = fkTimeFormat;
	}
	public Long getFkAccreditation() {
		return fkAccreditation;
	}
	public void setFkAccreditation(Long fkAccreditation) {
		this.fkAccreditation = fkAccreditation;
	}
	public Long getFkCbuStorage() {
		return fkCbuStorage;
	}
	public void setFkCbuStorage(Long fkCbuStorage) {
		this.fkCbuStorage = fkCbuStorage;
	}
	public Long getFkCbuCollection() {
		return fkCbuCollection;
	}
	public void setFkCbuCollection(Long fkCbuCollection) {
		this.fkCbuCollection = fkCbuCollection;
	}
	public Long getFkDryshipperAdd() {
		return fkDryshipperAdd;
	}
	public void setFkDryshipperAdd(Long fkDryshipperAdd) {
		this.fkDryshipperAdd = fkDryshipperAdd;
	}
	public String getAttentiondDryShipper() {
		return attentiondDryShipper;
	}
	public void setAttentiondDryShipper(String attentiondDryShipper) {
		this.attentiondDryShipper = attentiondDryShipper;
	}
	public String getPickUpAddSplInstuction() {
		return pickUpAddSplInstuction;
	}
	public void setPickUpAddSplInstuction(String pickUpAddSplInstuction) {
		this.pickUpAddSplInstuction = pickUpAddSplInstuction;
	}
	public String getPickUpAddLastName() {
		return pickUpAddLastName;
	}
	public void setPickUpAddLastName(String pickUpAddLastName) {
		this.pickUpAddLastName = pickUpAddLastName;
	}
	public String getPickUpAddFirstName() {
		return pickUpAddFirstName;
	}
	public void setPickUpAddFirstName(String pickUpAddFirstName) {
		this.pickUpAddFirstName = pickUpAddFirstName;
	}		
	
	public Boolean getNmdpResearch() {
		return nmdpResearch;
	}
	public void setNmdpResearch(Boolean nmdpResearch) {
		this.nmdpResearch = nmdpResearch;
	}
	
	
	
	
}
