package com.velos.ordercomponent.business.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
@Entity
@Table(name="CBB")
@SequenceGenerator(sequenceName="SEQ_CBB",name="SEQ_CBB",allocationSize=1)
public class CBB extends Auditable{
	
	private Long pkCbbId;
	private String cbbId;
	private Boolean enableEditAntigens;
	private Boolean displayMaternalAntigens;
	private Boolean enableCbuInventFeature;
	private Long defaultCTShipper;
	private Long defaultORShipper;
	private Boolean defaultMicroContamDateToProcDate;
	private Boolean defaultCfuTestDateToProcDate;
	private Boolean defaultViabTestDateToProcDate;
	private Boolean cordInfoLock;
	private Long personId;
	private Long addressId;
	private Long siteId;
	private Long allowMember;
	private Boolean cdrUser;
	private Boolean criReqElgQuest;
	private Person person;
	private Site site;
	private Boolean useOwnDumn;
	private Boolean isDomesticCBB;
	private Boolean useFdoe;
	private Long localcbuassgnmentid;
	private Long localmaternalassgnmentid;
	private String localCbuIdPrefix;
	private Long localCbuIdStrtNum;
	private String localMaternalIdPrefix;
	private Long localMaternalIdStrtNum;
	private Boolean enableAutoCompleteCordEntry;
	private Boolean enableAutoCompleteAckResTask;
	private Long fkDefaultShippingPaperWorkLocation;
	private Address address;
	private Address addressDryShipper;	
	private Long fkCbuPickUpAddress;
	private String attention;
	private String attentiondDryShipper;	
	private Long fkDateFormat;
	private Long fkTimeFormat;
	private Long fkAccreditation;
	private Long fkCbuStorage;
	private Long fkCbuCollection;
	private Long fkDryshipperAdd;
	private String pickUpAddSplInstuction;
	private String pickUpAddLastName;	
	private String pickUpAddFirstName;		
	private Boolean cbuAssessment;
	private Boolean unitReport;	
	private String bmdw;
	private String emdis;
	private Boolean nmdpResearch;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CBB")
	@Column(name="PK_CBB")
	public Long getPkCbbId() {
		return pkCbbId;
	}
	public void setPkCbbId(Long pkCbbId) {
		this.pkCbbId = pkCbbId;
	}
	@Column(name="CBB_ID")
	public String getCbbId() {
		return cbbId;
	}
	public void setCbbId(String cbbId) {
		this.cbbId = cbbId;
	}
	@Column(name="ENABLE_EDIT_ANTIGENS")
	public Boolean getEnableEditAntigens() {
		return enableEditAntigens;
	}
	public void setEnableEditAntigens(Boolean enableEditAntigens) {
		this.enableEditAntigens = enableEditAntigens;
	}
	@Column(name="DISPLAY_MATERNAL_ANTIGENS")
	public Boolean getDisplayMaternalAntigens() {
		return displayMaternalAntigens;
	}
	public void setDisplayMaternalAntigens(Boolean displayMaternalAntigens) {
		this.displayMaternalAntigens = displayMaternalAntigens;
	}
	@Column(name="ENABLE_CBU_INVENT_FEATURE")
	public Boolean getEnableCbuInventFeature() {
		return enableCbuInventFeature;
	}
	public void setEnableCbuInventFeature(Boolean enableCbuInventFeature) {
		this.enableCbuInventFeature = enableCbuInventFeature;
	}
	@Column(name="DEFAULT_CT_SHIPPER")
	public Long getDefaultCTShipper() {
		return defaultCTShipper;
	}
	public void setDefaultCTShipper(Long defaultCTShipper) {
		this.defaultCTShipper = defaultCTShipper;
	}
	@Column(name="DEFAULT_OR_SHIPPER")
	public Long getDefaultORShipper() {
		return defaultORShipper;
	}
	public void setDefaultORShipper(Long defaultORShipper) {
		this.defaultORShipper = defaultORShipper;
	}
	@Column(name="MICRO_CONTAM_DATE")
	public Boolean getDefaultMicroContamDateToProcDate() {
		return defaultMicroContamDateToProcDate;
	}
	public void setDefaultMicroContamDateToProcDate(
			Boolean defaultMicroContamDateToProcDate) {
		this.defaultMicroContamDateToProcDate = defaultMicroContamDateToProcDate;
	}
	@Column(name="CFU_TEST_DATE")
	public Boolean getDefaultCfuTestDateToProcDate() {
		return defaultCfuTestDateToProcDate;
	}
	public void setDefaultCfuTestDateToProcDate(Boolean defaultCfuTestDateToProcDate) {
		this.defaultCfuTestDateToProcDate = defaultCfuTestDateToProcDate;
	}
	@Column(name="VIAB_TEST_DATE")
	public Boolean getDefaultViabTestDateToProcDate() {
		return defaultViabTestDateToProcDate;
	}
	public void setDefaultViabTestDateToProcDate(Boolean defaultViabTestDateToProcDate) {
		this.defaultViabTestDateToProcDate = defaultViabTestDateToProcDate;
	}
	@Column(name="ENABLE_CORD_INFO_LOCK")
	public Boolean getCordInfoLock() {
		return cordInfoLock;
	}
	public void setCordInfoLock(Boolean cordInfoLock) {
		this.cordInfoLock = cordInfoLock;
	}
	
	
	@Column(name="FK_PERSON")
	public Long getPersonId() {
		return personId;
	}
	public void setPersonId(Long personId) {
		this.personId = personId;
	}
	@Column(name="FK_ADD")
	public Long getAddressId() {
		return addressId;
	}
	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}	
	
	@Column(name="FK_SITE")
	public Long getSiteId() {
		return siteId;
	}
	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}
	@Column(name="MEMBER")
	public Long getAllowMember() {
		return allowMember;
	}
	public void setAllowMember(Long allowMember) {
		this.allowMember = allowMember;
	}

	@Transient
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	
	@Transient
	public Site getSite() {
		return site;
	}
	public void setSite(Site site) {
		this.site = site;
	}
	
	@Column(name="USING_CDR")
	public Boolean getCdrUser() {
		return cdrUser;
	}
	public void setCdrUser(Boolean cdrUser) {
		this.cdrUser = cdrUser;
	}
	
	@Column(name="CRI_REQ_ELG_QUEST")
	public Boolean getCriReqElgQuest() {
		return criReqElgQuest;
	}
	public void setCriReqElgQuest(Boolean criReqElgQuest) {
		this.criReqElgQuest = criReqElgQuest;
	}
	
	@Column(name="USE_OWN_DUMN")
	public Boolean getUseOwnDumn() {
		return useOwnDumn;
	}
	public void setUseOwnDumn(Boolean useOwnDumn) {
		this.useOwnDumn = useOwnDumn;
	}
	@Column(name="DOMESTIC_NMDP")
	public Boolean getIsDomesticCBB() {
		return isDomesticCBB;
	}
	public void setIsDomesticCBB(Boolean isDomesticCBB) {
		this.isDomesticCBB = isDomesticCBB;
	}
	@Column(name="USE_FDOE")
	public Boolean getUseFdoe() {
		return useFdoe;
	}
	public void setUseFdoe(Boolean useFdoe) {
		this.useFdoe = useFdoe;
	}
	
	@Column(name="LOC_CBU_ID_PRE")
	public String getLocalCbuIdPrefix() {
		return localCbuIdPrefix;
	}
	public void setLocalCbuIdPrefix(String localCbuIdPrefix) {
		this.localCbuIdPrefix = localCbuIdPrefix;
	}
	
	@Column(name="LOC_CBU_ID_STR_NUM")
	public Long getLocalCbuIdStrtNum() {
		return localCbuIdStrtNum;
	}
	public void setLocalCbuIdStrtNum(Long localCbuIdStrtNum) {
		this.localCbuIdStrtNum = localCbuIdStrtNum;
	}
	
	@Column(name="LOC_MAT_ID_PRE")
	public String getLocalMaternalIdPrefix() {
		return localMaternalIdPrefix;
	}
	public void setLocalMaternalIdPrefix(String localMaternalIdPrefix) {
		this.localMaternalIdPrefix = localMaternalIdPrefix;
	}
	
	@Column(name="LOC_MAT_ID_STR_NUM")
	public Long getLocalMaternalIdStrtNum() {
		return localMaternalIdStrtNum;
	}
	public void setLocalMaternalIdStrtNum(Long localMaternalIdStrtNum) {
		this.localMaternalIdStrtNum = localMaternalIdStrtNum;
	}
	
	@Column(name="FK_LOC_CBU_ASSGN_ID")
	public Long getLocalcbuassgnmentid() {
		return localcbuassgnmentid;
	}
	public void setLocalcbuassgnmentid(Long localcbuassgnmentid) {
		this.localcbuassgnmentid = localcbuassgnmentid;
	}
	
	@Column(name="FK_LOC_MAT_ASSGN_ID")
	public Long getLocalmaternalassgnmentid() {
		return localmaternalassgnmentid;
	}
	public void setLocalmaternalassgnmentid(Long localmaternalassgnmentid) {
		this.localmaternalassgnmentid = localmaternalassgnmentid;
	}
	
	@Column(name="COMP_CORD_ENTRY")
	public Boolean getEnableAutoCompleteCordEntry() {
		return enableAutoCompleteCordEntry;
	}
	public void setEnableAutoCompleteCordEntry(Boolean enableAutoCompleteCordEntry) {
		this.enableAutoCompleteCordEntry = enableAutoCompleteCordEntry;
	}
	
	@Column(name="COMP_ACK_RES_TASK")
	public Boolean getEnableAutoCompleteAckResTask() {
		return enableAutoCompleteAckResTask;
	}
	public void setEnableAutoCompleteAckResTask(Boolean enableAutoCompleteAckResTask) {
		this.enableAutoCompleteAckResTask = enableAutoCompleteAckResTask;
	}
	
	@Column(name="FK_SHP_PWRK_LOC")
	public Long getFkDefaultShippingPaperWorkLocation() {
		return fkDefaultShippingPaperWorkLocation;
	}
	public void setFkDefaultShippingPaperWorkLocation(
			Long fkDefaultShippingPaperWorkLocation) {
		this.fkDefaultShippingPaperWorkLocation = fkDefaultShippingPaperWorkLocation;
	}
	
	@Transient
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	
	@Transient
	public Address getAddressDryShipper() {
		return addressDryShipper;
	}
	public void setAddressDryShipper(Address addressDryShipper) {
		this.addressDryShipper = addressDryShipper;
	}
		
	@Column(name="FK_CBU_PCK_ADD")
	public Long getFkCbuPickUpAddress() {
		return fkCbuPickUpAddress;
	}
	public void setFkCbuPickUpAddress(Long fkCbuPickUpAddress) {
		this.fkCbuPickUpAddress = fkCbuPickUpAddress;
	}
	
	@Column(name="ATTENTION")
	public String getAttention() {
		return attention;
	}
	public void setAttention(String attention) {
		this.attention = attention;
	}
	
	@Column(name="FK_DATE_FORMAT")
	public Long getFkDateFormat() {
		return fkDateFormat;
	}
	public void setFkDateFormat(Long fkDateFormat) {
		this.fkDateFormat = fkDateFormat;
	}
	
	@Column(name="FK_TIME_FORMAT")
	public Long getFkTimeFormat() {
		return fkTimeFormat;
	}
	public void setFkTimeFormat(Long fkTimeFormat) {
		this.fkTimeFormat = fkTimeFormat;
	}
	
	@Column(name="FK_ACCREDITATION")
	public Long getFkAccreditation() {
		return fkAccreditation;
	}
	public void setFkAccreditation(Long fkAccreditation) {
		this.fkAccreditation = fkAccreditation;
	}
	
	@Column(name="FK_CBU_STORAGE")
	public Long getFkCbuStorage() {
		return fkCbuStorage;
	}
	public void setFkCbuStorage(Long fkCbuStorage) {
		this.fkCbuStorage = fkCbuStorage;
	}
	@Column(name="FK_CBU_COLLECTION")
	public Long getFkCbuCollection() {
		return fkCbuCollection;
	}
	public void setFkCbuCollection(Long fkCbuCollection) {
		this.fkCbuCollection = fkCbuCollection;
	}
	
	@Column(name="FK_DRY_SHIPPER_ADD")
	public Long getFkDryshipperAdd() {
		return fkDryshipperAdd;
	}
	public void setFkDryshipperAdd(Long fkDryshipperAdd) {
		this.fkDryshipperAdd = fkDryshipperAdd;
	}
	
	@Column(name="ATTENTION_DRY_SHIPPER")	
	public String getAttentiondDryShipper() {
		return attentiondDryShipper;
	}
	public void setAttentiondDryShipper(String attentiondDryShipper) {
		this.attentiondDryShipper = attentiondDryShipper;
	}
	
	@Column(name="PICKUP_ADD_SPL_INSTRUCTION")
	public String getPickUpAddSplInstuction() {
		return pickUpAddSplInstuction;
	}
	public void setPickUpAddSplInstuction(String pickUpAddSplInstuction) {
		this.pickUpAddSplInstuction = pickUpAddSplInstuction;
	}
	
	@Column(name="PICKUP_ADD_CONTACT_LASTNAME")	
	public String getPickUpAddLastName() {
		return pickUpAddLastName;
	}
	public void setPickUpAddLastName(String pickUpAddLastName) {
		this.pickUpAddLastName = pickUpAddLastName;
	}
	
	@Column(name="PICKUP_ADD_CONTACT_FIRSTNAME")	
	public String getPickUpAddFirstName() {
		return pickUpAddFirstName;
	}
	public void setPickUpAddFirstName(String pickUpAddFirstName) {
		this.pickUpAddFirstName = pickUpAddFirstName;
	}	
	
	@Column(name="CBU_ASSESSMENT")
	public Boolean getCbuAssessment() {
		return cbuAssessment;
	}
	public void setCbuAssessment(Boolean cbuAssessment) {
		this.cbuAssessment = cbuAssessment;
	}
	
	@Column(name="UNIT_REPORT")
	public Boolean getUnitReport() {
		return unitReport;
	}
	public void setUnitReport(Boolean unitReport) {
		this.unitReport = unitReport;
	}
	
	
	@Column(name="BMDW")
	public String getBmdw() {
		return bmdw;
	}
	public void setBmdw(String bmdw) {
		this.bmdw = bmdw;
	}
	
	@Column(name="EMDIS")
	public String getEmdis() {
		return emdis;
	}
	public void setEmdis(String emdis) {
		this.emdis = emdis;
	}
	
	@Column(name="NMD_PRESEARCH_FLAG")
	public Boolean getNmdpResearch() {
		return nmdpResearch;
	}
	public void setNmdpResearch(Boolean nmdpResearch) {
		this.nmdpResearch = nmdpResearch;
	}
	
	
	
}

