package com.velos.ordercomponent.business.xmlpojos;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class IneligibleandIncompleteReasons {

	private List<String> reasons;

	@XmlElement(name="Reason")
	public List<String> getReasons() {
		return reasons;
	}

	public void setReasons(List<String> reasons) {
		this.reasons = reasons;
	}
	
	
}
