package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.datatype.XMLGregorianCalendar;

public class nRBC_Post_CryoPreservation {
	
	private String nRBC_test;
	private String nRBC_cnt;
	private XMLGregorianCalendar nRBC_test_date;
	private String nRbc_test_reason;
	
	@XmlElement(name="nRBC_test")
	public String getnRBC_test() {
		return nRBC_test;
	}
	public void setnRBC_test(String nRBCTest) {
		nRBC_test = nRBCTest;
	}
	
	@XmlElement(name="nRBC_cnt")
	public String getnRBC_cnt() {
		return nRBC_cnt;
	}
	public void setnRBC_cnt(String nRBCCnt) {
		nRBC_cnt = nRBCCnt;
	}
	
	@XmlElement(name="nRBC_test_date")
	public XMLGregorianCalendar getnRBC_test_date() {
		return nRBC_test_date;
	}
	public void setnRBC_test_date(XMLGregorianCalendar nRBCTestDate) {
		nRBC_test_date = nRBCTestDate;
	}
	
	@XmlElement(name="nRbc_test_reason")
	public String getnRbc_test_reason() {
		return nRbc_test_reason;
	}
	public void setnRbc_test_reason(String nRbcTestReason) {
		nRbc_test_reason = nRbcTestReason;
	}
	

}
