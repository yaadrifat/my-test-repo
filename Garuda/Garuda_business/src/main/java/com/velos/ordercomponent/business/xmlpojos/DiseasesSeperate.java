package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;

public class DiseasesSeperate {

	private String Code;
	private String OtherDescription;
	private RelationsSep relationsSep;
	@XmlElement(name="Code")
	public String getCode() {
		return Code;
	}
	public void setCode(String code) {
		Code = code;
	}
	@XmlElement(name="OtherDescription")
	public String getOtherDescription() {
		return OtherDescription;
	}
	public void setOtherDescription(String otherDescription) {
		OtherDescription = otherDescription;
	}
	@XmlElement(name="Relations")
	public RelationsSep getRelationsSep() {
		return relationsSep;
	}
	public void setRelationsSep(RelationsSep relationsSep) {
		this.relationsSep = relationsSep;
	}
	
	
}
