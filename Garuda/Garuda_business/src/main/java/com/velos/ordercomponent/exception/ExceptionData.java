/**
 * 
 */
package com.velos.ordercomponent.exception;

import java.io.Serializable;

/**
 * @author akajeera
 *
 */
public class ExceptionData implements Serializable{
		
		/**
		 * The Cause of Exception
		 */
		private Throwable cause;
		
		/**
		 * Message Occured During Exception
		 */
		private String message;
		
		/**
		 * Errorcode Occured During Exception
		 */
		private int errorCode;
		
		
		/**
		 * Error Description Corresponding To Error Code
		 */
		private String errorDescription;
		
		
		
		/**
		 * @return Returns the cause.
		 */
		public Throwable getCause() {
			return cause;
		}
		/**
		 * @param cause The cause to set.
		 */
		public void setCause(Throwable cause) {
			this.cause = cause;
		}
		/**
		 * @return Returns the errorCode.
		 */
		public int getErrorCode() {
			return errorCode;
		}
		/**
		 * @param errorCode The errorCode to set.
		 */
		public void setErrorCode(int errorCode) {
			this.errorCode = errorCode;
		}
		/**
		 * @return Returns the errorDescription.
		 */
		public String getErrorDescription() {
			return errorDescription;
		}
		/**
		 * @param errorDescription The errorDescription to set.
		 */
		public void setErrorDescription(String errorDescription) {
			this.errorDescription = errorDescription;
		}
		/**
		 * @return Returns the message.
		 */
		public String getMessage() {
			return message;
		}
		/**
		 * @param message The message to set.
		 */
		public void setMessage(String message) {
			this.message = message;
		}
}


