package com.velos.ordercomponent.business.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="CB_CBU_STATUS")
@SequenceGenerator(sequenceName="SEQ_CB_ENTITY_STATUS",name="SEQ_CB_ENTITY_STATUS",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class CBUStatus {
	
	private Long pkcbustatus;
	private String cbuStatusCode;
	private String cbuStatusDesc;
	private String codeType;
	private String cbbCT;
	private String cbbHE;
	private String cbbOR;
	private String teminalFlag;
	private Long statusCodeSeq;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_ENTITY_STATUS")
	@Column(name="PK_CBU_STATUS")
	public Long getPkcbustatus() {
		return pkcbustatus;
	}
	public void setPkcbustatus(Long pkcbustatus) {
		this.pkcbustatus = pkcbustatus;
	}
	@Column(name="CBU_STATUS_CODE")
	public String getCbuStatusCode() {
		return cbuStatusCode;
	}
	public void setCbuStatusCode(String cbuStatusCode) {
		this.cbuStatusCode = cbuStatusCode;
	}
	@Column(name="CBU_STATUS_DESC")
	public String getCbuStatusDesc() {
		return cbuStatusDesc;
	}
	public void setCbuStatusDesc(String cbuStatusDesc) {
		this.cbuStatusDesc = cbuStatusDesc;
	}
	@Column(name="CODE_TYPE")
	public String getCodeType() {
		return codeType;
	}
	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}
	@Column(name="CBB_CT")
	public String getCbbCT() {
		return cbbCT;
	}
	public void setCbbCT(String cbbCT) {
		this.cbbCT = cbbCT;
	}
	@Column(name="CBB_HE")
	public String getCbbHE() {
		return cbbHE;
	}
	public void setCbbHE(String cbbHE) {
		this.cbbHE = cbbHE;
	}
	@Column(name="CBB_OR")
	public String getCbbOR() {
		return cbbOR;
	}
	public void setCbbOR(String cbbOR) {
		this.cbbOR = cbbOR;
	}
	@Column(name="IS_TERMINAL_RELEASE")
	public String getTeminalFlag() {
		return teminalFlag;
	}
	public void setTeminalFlag(String teminalFlag) {
		this.teminalFlag = teminalFlag;
	}
	@Column(name="STATUS_CODE_SEQ")
	public Long getStatusCodeSeq() {
		return statusCodeSeq;
	}
	public void setStatusCodeSeq(Long statusCodeSeq) {
		this.statusCodeSeq = statusCodeSeq;
	}
	

}
