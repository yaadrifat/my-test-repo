package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

import com.velos.ordercomponent.business.domain.Auditable;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class AddressPojo extends AuditablePojo {

	private Long addressId;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zip;
	private String phone;
	private String fax;
	private String email;
	private String county;
	private String country;
	private Date addressEffectiveDate;
	private String pager;
	private String mobile;
	private Long addCreatedBy;
	private String ext;
	
	public Long getAddressId() {
		return addressId;
	}
	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Date getAddressEffectiveDate() {
		return addressEffectiveDate;
	}
	public void setAddressEffectiveDate(Date addressEffectiveDate) {
		this.addressEffectiveDate = addressEffectiveDate;
	}
	public String getPager() {
		return pager;
	}
	public void setPager(String pager) {
		this.pager = pager;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public Long getAddCreatedBy() {
		return addCreatedBy;
	}
	public void setAddCreatedBy(Long addCreatedBy) {
		this.addCreatedBy = addCreatedBy;
	}	
	public String getExt() {
		return ext;
	}
	public void setExt(String ext) {
		this.ext = ext;
	}
	
	
}
