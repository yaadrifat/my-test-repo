package com.velos.ordercomponent.report;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.jdbc.Work;


public class EntityMapWithList implements Work{
	public static final Log log = LogFactory.getLog(EntityMapWithList.class);
	
	private String query;
	
	private Map<Long, ArrayList<Long>> entityMap;	
	
	public EntityMapWithList(String query) {
		this.query = query;
	}
	 @Override
	    public void execute( Connection connection ) throws SQLException
	    {
	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        try
	        {
	            ps = connection.prepareStatement( query);
	            ps.setFetchSize(50000);
	            rs = ps.executeQuery();
	            long startTime=System.currentTimeMillis();            
		           
	             entityMap = prepMapObj(rs);		            
	             long endTime=System.currentTimeMillis();
		         System.out.println();
		         System.out.println((endTime-startTime)/1000+" : "+(endTime-startTime)%1000+" MM:SS\n\n");
	        }
	        
	        
	        catch( SQLException e ) { 
	        	
	        	printException(e);
	        } catch (ParseException e) {
	        	
	        	printException(e);
			}
	        finally  {
	            if( rs != null )  
	            {
	                try {
	                    rs.close();
	                }
	                catch( Exception e ) {printException(e);}
	            }
	            if( ps != null ) 
	            {
	                try {
	                    ps.close();
	                }
	                catch( Exception e ) {printException(e);}
	            }
	        }
	    }
	
	 private  Map<Long, ArrayList<Long>> prepMapObj(ResultSet rs) throws SQLException, ParseException{
		 
		 Map<Long, ArrayList<Long>> entityMap = new HashMap<Long, ArrayList<Long>>();
		 
		 Long key = null;
		 Long val =null;
		 
		 ArrayList<Long> lst = null;
		 
		  while(rs.next())  
          {			  
			  key = rs.getLong(1);
			  val = rs.getLong(2);
			  
			  if(entityMap.get(key) == null){
				  
				  lst = new ArrayList<Long>();
				  lst.add(val);
				  entityMap.put(key, lst);
			  }else{
				  
				  lst = entityMap.get(key);
				  lst.add(val);
				  entityMap.put(key, lst);
			  }
			  
			  
          }
		 return entityMap;
	 }
	private void printException(Exception e){
		
		log.error(e.getMessage());
		e.printStackTrace();
		
	}
	
	
	public Map<Long, ArrayList<Long>> getEntityMap() {
		return entityMap;
	}
	
	

}

