package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


public class AntiHiv12 {

	private String anti_hiv_1_2_ind;
	private XMLGregorianCalendar anti_hiv_1_2_date;
	private String cms_cert_lab_ind;
	private String fda_lic_mfg_inst_ind;
	
	@XmlElement(name="anti_hiv_1_2_ind")	
	public String getAnti_hiv_1_2_ind() {
		return anti_hiv_1_2_ind;
	}
	public void setAnti_hiv_1_2_ind(String antiHiv_1_2Ind) {
		anti_hiv_1_2_ind = antiHiv_1_2Ind;
	}
	@XmlElement(name="anti_hiv_1_2_date")	
	@XmlJavaTypeAdapter(type=XMLGregorianCalendar.class,value=XMLGeorgarianCalanderAdapter.class)
	public XMLGregorianCalendar getAnti_hiv_1_2_date() {
		return anti_hiv_1_2_date;
	}
	public void setAnti_hiv_1_2_date(XMLGregorianCalendar antiHiv_1_2Date) {
		anti_hiv_1_2_date = antiHiv_1_2Date;
	}
	@XmlElement(name="cms_cert_lab_ind")	
	public String getCms_cert_lab_ind() {
		return cms_cert_lab_ind;
	}
	public void setCms_cert_lab_ind(String cmsCertLabInd) {
		cms_cert_lab_ind = cmsCertLabInd;
	}
	@XmlElement(name="fda_lic_mfg_inst_ind")	
	public String getFda_lic_mfg_inst_ind() {
		return fda_lic_mfg_inst_ind;
	}
	public void setFda_lic_mfg_inst_ind(String fdaLicMfgInstInd) {
		fda_lic_mfg_inst_ind = fdaLicMfgInstInd;
	}
	
}
