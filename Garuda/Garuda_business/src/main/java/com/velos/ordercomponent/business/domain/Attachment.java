package com.velos.ordercomponent.business.domain;

import java.sql.Blob;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.ordercomponent.business.util.VelosUtil;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
@Entity
@Table(name="ER_ATTACHMENTS")
@SequenceGenerator(sequenceName="SEQ_ER_ATTACHMENTS",name="SEQ_ER_ATTACHMENTS",allocationSize=1)
public class Attachment extends Auditable {
	private Long attachmentId;
	private Long garudaEntityId;
	private Long garudaEntityType;
	private Long garudaAttachmentType;
	private String garudaAttachmentsTypeRem;
	private String documentType;
	private Blob documentFile;
	private String fileName;
	private Long dcmsAttachmentId;
	private String actionFlag;
	/*private Auditable auditable;*/

	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_ER_ATTACHMENTS")
	@Column(name="PK_ATTACHMENT")
	public Long getAttachmentId() {
		return attachmentId;
	}
	public void setAttachmentId(Long attachmentId) {
		this.attachmentId = attachmentId;
	}
	
	
	@Column(name="ENTITY_ID")
	public Long getGarudaEntityId() {
		return garudaEntityId;
	}
	public void setGarudaEntityId(long garudaEntityId) {
		this.garudaEntityId = garudaEntityId;
	}
	
	@Column(name="ENTITY_TYPE")
	public Long getGarudaEntityType() {
		return garudaEntityType;
	}
	public void setGarudaEntityType(Long garudaEntityType) {
		this.garudaEntityType = garudaEntityType;
	}
	
	@Column(name="FK_ATTACHMENT_TYPE")
	public Long getGarudaAttachmentType() {
		return garudaAttachmentType;
	}
	public void setGarudaAttachmentType(Long garudaAttachmentType) {
		this.garudaAttachmentType = garudaAttachmentType;
	}
	
	@Column(name="ATTACHMENTS_TYPE_REM")
	public String getGarudaAttachmentsTypeRem() {
		return garudaAttachmentsTypeRem;
	}
	public void setGarudaAttachmentsTypeRem(String garudaAttachmentsTypeRem) {
		this.garudaAttachmentsTypeRem = garudaAttachmentsTypeRem;
	}
	
	@Column(name="DOCUMENT_TYPE")	
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	
	@Column(name="DOCUMENT_FILE")
	public Blob getDocumentFile() {
		return documentFile;
	}
	public void setDocumentFile(Blob documentFile) {
		this.documentFile = documentFile;
	}
	
	/*@Embedded
	public Auditable getAuditable() {
		return auditable;
	}
	public void setAuditable(Auditable auditable) {
		this.auditable = auditable;
	}*/
	
	
	@Column(name="ATTACHMENT_FILE_NAME")	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	private Boolean deletedFlag;
	
	@Column(name="deletedflag")
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	
	@Column(name="DCMS_FILE_ATTACHMENT_ID")
	public Long getDcmsAttachmentId() {
		return dcmsAttachmentId;
	}
	public void setDcmsAttachmentId(Long dcmsAttachmentId) {
		this.dcmsAttachmentId = dcmsAttachmentId;
	}
	
	@Column(name="ACTION_FLAG")
	public String getActionFlag() {
		return actionFlag;
	}
	public void setActionFlag(String actionFlag) {
		this.actionFlag = actionFlag;
	}
	
	
	
}
