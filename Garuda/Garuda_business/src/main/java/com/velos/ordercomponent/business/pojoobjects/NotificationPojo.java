package com.velos.ordercomponent.business.pojoobjects;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class NotificationPojo extends AuditablePojo {

	private Long notifyId;
	private String notifyFrom;
	private String notifyTo;
	private String notifyCC;
	private String notifyBCC;
	private String notifySubject;
	private String notifyContent;
	private Boolean notifyIsSent;
	private String attachementQuery;
	public Long getNotifyId() {
		return notifyId;
	}
	public void setNotifyId(Long notifyId) {
		this.notifyId = notifyId;
	}
	public String getNotifyFrom() {
		return notifyFrom;
	}
	public void setNotifyFrom(String notifyFrom) {
		this.notifyFrom = notifyFrom;
	}
	public String getNotifyTo() {
		return notifyTo;
	}
	public void setNotifyTo(String notifyTo) {
		this.notifyTo = notifyTo;
	}
	public String getNotifyCC() {
		return notifyCC;
	}
	public void setNotifyCC(String notifyCC) {
		this.notifyCC = notifyCC;
	}
	public String getNotifyBCC() {
		return notifyBCC;
	}
	public void setNotifyBCC(String notifyBCC) {
		this.notifyBCC = notifyBCC;
	}
	public String getNotifySubject() {
		return notifySubject;
	}
	public void setNotifySubject(String notifySubject) {
		this.notifySubject = notifySubject;
	}
	public String getNotifyContent() {
		return notifyContent;
	}
	public void setNotifyContent(String notifyContent) {
		this.notifyContent = notifyContent;
	}
	public Boolean getNotifyIsSent() {
		return notifyIsSent;
	}
	public void setNotifyIsSent(Boolean notifyIsSent) {
		this.notifyIsSent = notifyIsSent;
	}
	public String getAttachementQuery() {
		return attachementQuery;
	}
	public void setAttachementQuery(String attachementQuery) {
		this.attachementQuery = attachementQuery;
	}
	
	private Boolean deletedFlag;
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
}
