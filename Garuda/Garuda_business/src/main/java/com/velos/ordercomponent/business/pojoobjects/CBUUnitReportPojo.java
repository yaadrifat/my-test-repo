package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

public class CBUUnitReportPojo extends AuditablePojo {
	private Long fkCordCdrCbuId;
	private Long cordProcMethod;
	private Long cordTypeBag;
	private Long cordProdModification;
	private Long cordHemogScreen;
	private Long cordBacterialCul;
	private Long cordFungalCul;
	private String cordExplainNotes;
	private String cordAddNotes;
	private Long pkCordExtInfo;
	private Long cordViabPostProcess; 
	private Long cordViabMethod; 
	private String strcordViabPostTestDate;
	private Date cordViabPostTestDate; 
	private Long cordViabPostStatus;
	private Boolean cordViabPostQues1; 
	private Boolean cordViabPostQues2;
	private Long cordCfuPostProcessCount; 
	private Long cordCfuPostMethod; 
	private Long cordCfuPostStatus; 
	private Date cordCfuPostTestDate; 
	private String strcordCfuPostTestDate;
	private Boolean cordCfuPostQues1; 
	private Boolean cordCfuPostQues2;
	private Boolean cordCbuPostQues1; 
	private Boolean cordCbuPostQues2; 
	private Date cordCbuPostTestDate; 
	private String strcordCbuPostTestDate; 
	private Long cordCbuPostProcessCount; 
	private Long cordCbuPostStatus;
	private Long cordSerial;
	private String cordCompletedBy; 
	private Long cordCompletedStatus;
	private Boolean deletedFlag;
	private Long attachmentId;
	private String actionFlag;
	
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	
	
	
	
	public Long getFkCordCdrCbuId() {
		return fkCordCdrCbuId;
	}
	public void setFkCordCdrCbuId(Long fkCordCdrCbuId) {
		this.fkCordCdrCbuId = fkCordCdrCbuId;
	}
	public Long getCordProcMethod() {
		return cordProcMethod;
	}
	public void setCordProcMethod(Long cordProcMethod) {
		this.cordProcMethod = cordProcMethod;
	}
	public Long getCordTypeBag() {
		return cordTypeBag;
	}
	public void setCordTypeBag(Long cordTypeBag) {
		this.cordTypeBag = cordTypeBag;
	}
	public Long getCordProdModification() {
		return cordProdModification;
	}
	public void setCordProdModification(Long cordProdModification) {
		this.cordProdModification = cordProdModification;
	}
	public Long getCordHemogScreen() {
		return cordHemogScreen;
	}
	public void setCordHemogScreen(Long cordHemogScreen) {
		this.cordHemogScreen = cordHemogScreen;
	}
	public String getStrcordCfuPostTestDate() {
		return strcordCfuPostTestDate;
	}
	public void setStrcordCfuPostTestDate(String strcordCfuPostTestDate) {
		this.strcordCfuPostTestDate = strcordCfuPostTestDate;
	}
	public String getStrcordCbuPostTestDate() {
		return strcordCbuPostTestDate;
	}
	public void setStrcordCbuPostTestDate(String strcordCbuPostTestDate) {
		this.strcordCbuPostTestDate = strcordCbuPostTestDate;
	}
	public Long getCordBacterialCul() {
		return cordBacterialCul;
	}
	public void setCordBacterialCul(Long cordBacterialCul) {
		this.cordBacterialCul = cordBacterialCul;
	}
	public Long getCordFungalCul() {
		return cordFungalCul;
	}
	public void setCordFungalCul(Long cordFungalCul) {
		this.cordFungalCul = cordFungalCul;
	}
	public String getCordExplainNotes() {
		return cordExplainNotes;
	}
	public void setCordExplainNotes(String cordExplainNotes) {
		this.cordExplainNotes = cordExplainNotes;
	}
	public String getCordAddNotes() {
		return cordAddNotes;
	}
	public void setCordAddNotes(String cordAddNotes) {
		this.cordAddNotes = cordAddNotes;
	}
	public Long getPkCordExtInfo() {
		return pkCordExtInfo;
	}
	public void setPkCordExtInfo(Long pkCordExtInfo) {
		this.pkCordExtInfo = pkCordExtInfo;
	}
	public Long getCordViabPostProcess() {
		return cordViabPostProcess;
	}
	public void setCordViabPostProcess(Long cordViabPostProcess) {
		this.cordViabPostProcess = cordViabPostProcess;
	}
	public Long getCordViabMethod() {
		return cordViabMethod;
	}
	public void setCordViabMethod(Long cordViabMethod) {
		this.cordViabMethod = cordViabMethod;
	}
	
	public String getStrcordViabPostTestDate() {
		return strcordViabPostTestDate;
	}
	public void setStrcordViabPostTestDate(String strcordViabPostTestDate) {
		this.strcordViabPostTestDate = strcordViabPostTestDate;
	}
	public Date getCordViabPostTestDate() {
		return cordViabPostTestDate;
	}
	public void setCordViabPostTestDate(Date cordViabPostTestDate) {
		this.cordViabPostTestDate = cordViabPostTestDate;
	}
	public Long getCordViabPostStatus() {
		return cordViabPostStatus;
	}
	public void setCordViabPostStatus(Long cordViabPostStatus) {
		this.cordViabPostStatus = cordViabPostStatus;
	}
	public Boolean getCordViabPostQues1() {
		return cordViabPostQues1;
	}
	public void setCordViabPostQues1(Boolean cordViabPostQues1) {
		this.cordViabPostQues1 = cordViabPostQues1;
	}
	public Boolean getCordViabPostQues2() {
		return cordViabPostQues2;
	}
	public void setCordViabPostQues2(Boolean cordViabPostQues2) {
		this.cordViabPostQues2 = cordViabPostQues2;
	}
	public Long getCordCfuPostProcessCount() {
		return cordCfuPostProcessCount;
	}
	public void setCordCfuPostProcessCount(Long cordCfuPostProcessCount) {
		this.cordCfuPostProcessCount = cordCfuPostProcessCount;
	}
	public Long getCordCfuPostMethod() {
		return cordCfuPostMethod;
	}
	public void setCordCfuPostMethod(Long cordCfuPostMethod) {
		this.cordCfuPostMethod = cordCfuPostMethod;
	}
	public Long getCordCfuPostStatus() {
		return cordCfuPostStatus;
	}
	public void setCordCfuPostStatus(Long cordCfuPostStatus) {
		this.cordCfuPostStatus = cordCfuPostStatus;
	}
	public Date getCordCfuPostTestDate() {
		return cordCfuPostTestDate;
	}
	public void setCordCfuPostTestDate(Date cordCfuPostTestDate) {
		this.cordCfuPostTestDate = cordCfuPostTestDate;
	}
	public Boolean getCordCfuPostQues1() {
		return cordCfuPostQues1;
	}
	public void setCordCfuPostQues1(Boolean cordCfuPostQues1) {
		this.cordCfuPostQues1 = cordCfuPostQues1;
	}
	public Boolean getCordCfuPostQues2() {
		return cordCfuPostQues2;
	}
	public void setCordCfuPostQues2(Boolean cordCfuPostQues2) {
		this.cordCfuPostQues2 = cordCfuPostQues2;
	}
	public Boolean getCordCbuPostQues1() {
		return cordCbuPostQues1;
	}
	public void setCordCbuPostQues1(Boolean cordCbuPostQues1) {
		this.cordCbuPostQues1 = cordCbuPostQues1;
	}
	public Boolean getCordCbuPostQues2() {
		return cordCbuPostQues2;
	}
	public void setCordCbuPostQues2(Boolean cordCbuPostQues2) {
		this.cordCbuPostQues2 = cordCbuPostQues2;
	}
	public Date getCordCbuPostTestDate() {
		return cordCbuPostTestDate;
	}
	public void setCordCbuPostTestDate(Date cordCbuPostTestDate) {
		this.cordCbuPostTestDate = cordCbuPostTestDate;
	}
	public Long getCordCbuPostProcessCount() {
		return cordCbuPostProcessCount;
	}
	public void setCordCbuPostProcessCount(Long cordCbuPostProcessCount) {
		this.cordCbuPostProcessCount = cordCbuPostProcessCount;
	}
	public Long getCordCbuPostStatus() {
		return cordCbuPostStatus;
	}
	public void setCordCbuPostStatus(Long cordCbuPostStatus) {
		this.cordCbuPostStatus = cordCbuPostStatus;
	}

	public Long getCordSerial() {
		return cordSerial;
	}
	public void setCordSerial(Long cordSerial) {
		this.cordSerial = cordSerial;
	}
	public String getCordCompletedBy() {
		return cordCompletedBy;
	}
	public void setCordCompletedBy(String cordCompletedBy) {
		this.cordCompletedBy = cordCompletedBy;
	}
	public Long getCordCompletedStatus() {
		return cordCompletedStatus;
	}
	public void setCordCompletedStatus(Long cordCompletedStatus) {
		this.cordCompletedStatus = cordCompletedStatus;
	}
	public Long getAttachmentId() {
		return attachmentId;
	}
	public void setAttachmentId(Long attachmentId) {
		this.attachmentId = attachmentId;
	}
	public String getActionFlag() {
		return actionFlag;
	}
	public void setActionFlag(String actionFlag) {
		this.actionFlag = actionFlag;
	}	
	
	
}
