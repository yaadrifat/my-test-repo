package com.velos.ordercomponent.business.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity
@Table(name="CBU_UNIT_REPORT_HLA_REVIEW")
@SequenceGenerator(sequenceName="SEQ_CBU_UNIT_REPORT_HLA_REVIEW",name="SEQ_CBU_UNIT_REPORT_HLA_REVIEW",allocationSize=1)
public class CordHlaExtTemp extends Auditable {

	private Long pkCordHlaExtTemp;

	private Long fkCordCdrCbuId;
	
	private String cordHlaTwice;
	private String cordIndHlaSamples;
	private String cordHlaSecondLab;
	private String cordHlaContiSeg;
	private String cordHlaContiSegBefRel;
	private String cordHlaIndBefRel;
	private Long cordHlaContiSegNum;
	private Boolean deletedFlag;
	private Long fkCordHlaExt;
	private Long fkCordExtInfoTemp;

	
	
	/*private Auditable auditable;*/

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CBU_UNIT_REPORT_HLA_REVIEW")
	@Column(name="PK_CBU_UNIT_REPORT_HLA_REVIEW")
	public Long getPkCordHlaExtTemp() {
		return pkCordHlaExtTemp;
	}
	public void setPkCordHlaExtTemp(Long pkCordHlaExtTemp) {
		this.pkCordHlaExtTemp = pkCordHlaExtTemp;
	}	
	
	@Column(name="FK_CORD_CDR_CBU_ID")
	public Long getFkCordCdrCbuId() {
		return fkCordCdrCbuId;
	}
	public void setFkCordCdrCbuId(Long fkCordCdrCbuId) {
		this.fkCordCdrCbuId = fkCordCdrCbuId;
	}

	@Column(name="CORD_HLA_CONTI_SEG_NUM")
	public Long getCordHlaContiSegNum() {
		return cordHlaContiSegNum;
	}
	public void setCordHlaContiSegNum(Long cordHlaContiSegNum) {
		this.cordHlaContiSegNum = cordHlaContiSegNum;
	}

	@Column(name="CORD_HLA_TWICE")
	public String getCordHlaTwice() {
		return cordHlaTwice;
	}
	public void setCordHlaTwice(String cordHlaTwice) {
		this.cordHlaTwice = cordHlaTwice;
	}
	@Column(name="CORD_IND_HLA_SAMPLES")
	public String getCordIndHlaSamples() {
		return cordIndHlaSamples;
	}
	public void setCordIndHlaSamples(String cordIndHlaSamples) {
		this.cordIndHlaSamples = cordIndHlaSamples;
	}
	@Column(name="CORD_HLA_SECOND_LAB")
	public String getCordHlaSecondLab() {
		return cordHlaSecondLab;
	}
	public void setCordHlaSecondLab(String cordHlaSecondLab) {
		this.cordHlaSecondLab = cordHlaSecondLab;
	}
	@Column(name="CORD_HLA_CONTI_SEG")
	public String getCordHlaContiSeg() {
		return cordHlaContiSeg;
	}
	public void setCordHlaContiSeg(String cordHlaContiSeg) {
		this.cordHlaContiSeg = cordHlaContiSeg;
	}
	@Column(name="CORD_HLA_CONTI_SEG_BEF_REL")
	public String getCordHlaContiSegBefRel() {
		return cordHlaContiSegBefRel;
	}
	public void setCordHlaContiSegBefRel(String cordHlaContiSegBefRel) {
		this.cordHlaContiSegBefRel = cordHlaContiSegBefRel;
	}
	@Column(name="CORD_HLA_IND_BEF_REL")
	public String getCordHlaIndBefRel() {
		return cordHlaIndBefRel;
	}
	public void setCordHlaIndBefRel(String cordHlaIndBefRel) {
		this.cordHlaIndBefRel = cordHlaIndBefRel;
	}
	
	
	/*@Embedded
	public Auditable getAuditable() {
		return auditable;
	}
	public void setAuditable(Auditable auditable) {
		this.auditable = auditable;
	}*/
	
	@Column(name="deletedflag")
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	
	@Column(name="FK_CORD_HLA_EXT")	
	public Long getFkCordHlaExt() {
		return fkCordHlaExt;
	}
	public void setFkCordHlaExt(Long fkCordHlaExt) {
		this.fkCordHlaExt = fkCordHlaExt;
	}
	
	
	@Column(name="FK_CORD_EXT_INFO_TEMP")
	public Long getFkCordExtInfoTemp() {
		return fkCordExtInfoTemp;
	}
	public void setFkCordExtInfoTemp(Long fkCordExtInfoTemp) {
		this.fkCordExtInfoTemp = fkCordExtInfoTemp;
	}
	
	
}