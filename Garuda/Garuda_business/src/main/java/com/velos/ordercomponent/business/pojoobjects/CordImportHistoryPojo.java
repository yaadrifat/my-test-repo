package com.velos.ordercomponent.business.pojoobjects;

import java.sql.Blob;
import java.sql.Timestamp;

import com.velos.ordercomponent.business.domain.UserDomain;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class CordImportHistoryPojo extends AuditablePojo {
	private Long pkCordimportHistory;
	private String cordImportHistoryId;
	private Blob importFile;
	private String importFileName;
	private Timestamp startTime;
	private Timestamp endTime;
	private String importHistoryDesc;
	private UserDomain userDomain;
	private Long fkSiteId;
	
	public Long getPkCordimportHistory() {
		return pkCordimportHistory;
	}
	public void setPkCordimportHistory(Long pkCordimportHistory) {
		this.pkCordimportHistory = pkCordimportHistory;
	}
	public String getCordImportHistoryId() {
		return cordImportHistoryId;
	}
	public void setCordImportHistoryId(String cordImportHistoryId) {
		this.cordImportHistoryId = cordImportHistoryId;
	}
	public Timestamp getStartTime() {
		return startTime;
	}
	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}
	public Timestamp getEndTime() {
		return endTime;
	}
	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}
	public String getImportHistoryDesc() {
		return importHistoryDesc;
	}
	public void setImportHistoryDesc(String importHistoryDesc) {
		this.importHistoryDesc = importHistoryDesc;
	}
	public Blob getImportFile() {
		return importFile;
	}
	public void setImportFile(Blob importFile) {
		this.importFile = importFile;
	}
	public String getImportFileName() {
		return importFileName;
	}
	public void setImportFileName(String importFileName) {
		this.importFileName = importFileName;
	}
	public UserDomain getUserDomain() {
		return userDomain;
	}
	public void setUserDomain(UserDomain userDomain) {
		this.userDomain = userDomain;
	}
	public Long getFkSiteId() {
		return fkSiteId;
	}
	public void setFkSiteId(Long fkSiteId) {
		this.fkSiteId = fkSiteId;
	}
	
	
	
}
