package com.velos.ordercomponent.business.pojoobjects;

import java.util.List;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class CordImportXmlMultipleValues {
	private Long[] races;
	private List<Long> deperecatedRaces;
	private EntitySamplesPojo samplesPojo;
    private EntityStatusPojo entityStatusPojo;
    private List<EntityStatusReasonPojo> entityStatusReasonPojoList;
    private EntitySamplesPojo entitySamplesPojo;
    private EntityStatusPojo entityStatusLicensePojo;
    private List<FormResponsesPojo> mrqFormresponsepojoList;
    private List<FormResponsesPojo> fmhqFormresponsepojoList;
    private List<FormResponsesPojo> matIdmFormresponsepojoList;
    private List<EntityStatusReasonPojo> entityStatusReasonPojoLicenseList;
    private Boolean licStatusSectionOmitted;
    private Boolean licStatusOmittedAndReasonExist;
    private Boolean licStatusReasonOmitted;
    private Boolean cbuDemographicSectionOmitted;
    private Boolean labSummarySectionOmited;
    private Boolean cbuSamplesSectionOmited;
    private Boolean eligStatusSectionOmited;
    private FormVersionPojo mrqFormVersionPojo;
    private FormVersionPojo fmhqFormVersionPojo;
    private FormVersionPojo matIdmFormVersionPojo;
    private FormsPojo mrqFormsPojo;
    private FormsPojo fmhqFormsPojo;
    private FormsPojo matIdmFormsPojo;
    private Boolean saveMrqForm;
    private Boolean saveFmhqForm;
    private Boolean saveIdmForm;
    private List<AdditionalIdsPojo> additionalIdsPojos;
    private Boolean cbuStorageLocValid;
    private Boolean raceInvalid;
    private String cmsData;
    private String fdaData;
    private Boolean procedureOmitted;
    private CBBProcedurePojo procedurePojo;
    private Boolean frzDateOmmited;
    private EntitySamplesPojo savedEntitySamplesPojo;
    private Boolean birthDateOmitted;
    private Boolean collectionDateomitted;
    private Boolean genderOmitted;
    private Boolean multipleBirthOmitted;
    private Boolean typeOfCollectionOmitted;
    private Boolean typeOfDeliveryOmitted;
    private Boolean procesingStatDateOmitted;
    private Boolean cbuProcessingProtocolOmitted;
    private Boolean bloodTypeOmitted;
    private Boolean rhTypeOmitted;
    private Boolean bactCulStratDateOmitted;
    private Boolean bactCulOmitted;
    private Boolean fungCulStartDateOmitted;
    private Boolean fungCulOmitted;
    private Boolean hemoGlobinScreenOmitted;
    private Boolean ethnicityOmitted;
    private String babyGenderInvalidValue;
    private String raceInvalidValue;
    private String rhTypeInvalidValue;
    private String ethnicityInvalidvalue;
    private String hemoGlobinScreenInvalidValue;
    private Boolean babyGenderInvalidExistence;
    private Boolean ethnicityInvalidExistence;
    private Boolean rhTypeInvalidExistence;
    private Boolean hemoGlobinScreenInvalidExistence;
    private Boolean licenseMultipleSameReason;
    private Boolean eligMultipleSameReason;
    private Boolean eligbilityReasonOmmitted;
    private Boolean eligbilityReasonExistWithoutStatus;
    private Boolean cfuCountOtherDescInvalidExistence;
    private Boolean viabilityOtherDescInvalidExistence;
    private String cfuCountOtherDescInvalidValue;
    private String viabilityOtherDescInvalidValue;
    private Boolean raceBlankOutExistence;
    private Boolean raceOmitted;
    private Boolean cbVolCheckExitanceNode;   
    
    //Lab Summary Validation Check Existance Node
   
    private Boolean totCbuNuclCheckExistanceNode;
    private Boolean totCD34CheckExistanceNode;
    private Boolean cbuNRbcAbsCheckExistanceNode;
    private Boolean cbuNRbcPerCheckExistanceNode;    
    private Boolean cfuTestMthdCheckExistanceNode;
    private Boolean viaTestMthdCheckExistanceNode;
    private Boolean cfuTestResultCheckExistanceNode;
    private Boolean viaTestResultCheckExistanceNode;
    private Boolean isDefer;
    private Boolean collDateNotLieInProcedure;
	private Boolean deprecatedRaceExistence;
    
    
    
	public Boolean getLicStatusReasonOmitted() {
		return licStatusReasonOmitted;
	}

	public void setLicStatusReasonOmitted(Boolean licStatusReasonOmitted) {
		this.licStatusReasonOmitted = licStatusReasonOmitted;
	}

	public Boolean getDeprecatedRaceExistence() {
		return deprecatedRaceExistence;
	}

	public void setDeprecatedRaceExistence(Boolean deprecatedRaceExistence) {
		this.deprecatedRaceExistence = deprecatedRaceExistence;
	}

	public List<Long> getDeperecatedRaces() {
		return deperecatedRaces;
	}

	public void setDeperecatedRaces(List<Long> deperecatedRaces) {
		this.deperecatedRaces = deperecatedRaces;
	}

	public Boolean getEligbilityReasonExistWithoutStatus() {
		return eligbilityReasonExistWithoutStatus;
	}

	public void setEligbilityReasonExistWithoutStatus(
			Boolean eligbilityReasonExistWithoutStatus) {
		this.eligbilityReasonExistWithoutStatus = eligbilityReasonExistWithoutStatus;
	}

	public Boolean getLicStatusOmittedAndReasonExist() {
		return licStatusOmittedAndReasonExist;
	}

	public void setLicStatusOmittedAndReasonExist(
			Boolean licStatusOmittedAndReasonExist) {
		this.licStatusOmittedAndReasonExist = licStatusOmittedAndReasonExist;
	}

	public Boolean getCollDateNotLieInProcedure() {
		return collDateNotLieInProcedure;
	}

	public void setCollDateNotLieInProcedure(Boolean collDateNotLieInProcedure) {
		this.collDateNotLieInProcedure = collDateNotLieInProcedure;
	}

	public Boolean getIsDefer() {
		return isDefer;
	}

	public void setIsDefer(Boolean isDefer) {
		this.isDefer = isDefer;
	}

	public Boolean getRaceOmitted() {
		return raceOmitted;
	}

	public void setRaceOmitted(Boolean raceOmitted) {
		this.raceOmitted = raceOmitted;
	}
	public Boolean getCfuTestResultCheckExistanceNode() {
		return cfuTestResultCheckExistanceNode;
	}

	public void setCfuTestResultCheckExistanceNode(
			Boolean cfuTestResultCheckExistanceNode) {
		this.cfuTestResultCheckExistanceNode = cfuTestResultCheckExistanceNode;
	}

	public Boolean getViaTestResultCheckExistanceNode() {
		return viaTestResultCheckExistanceNode;
	}

	public void setViaTestResultCheckExistanceNode(
			Boolean viaTestResultCheckExistanceNode) {
		this.viaTestResultCheckExistanceNode = viaTestResultCheckExistanceNode;
	}

	public Boolean getCbuNRbcPerCheckExistanceNode() {
		return cbuNRbcPerCheckExistanceNode;
	}

	public void setCbuNRbcPerCheckExistanceNode(Boolean cbuNRbcPerCheckExistanceNode) {
		this.cbuNRbcPerCheckExistanceNode = cbuNRbcPerCheckExistanceNode;
	}

	public Boolean getCbuNRbcAbsCheckExistanceNode() {
		return cbuNRbcAbsCheckExistanceNode;
	}

	public void setCbuNRbcAbsCheckExistanceNode(Boolean cbuNRbcAbsCheckExistanceNode) {
		this.cbuNRbcAbsCheckExistanceNode = cbuNRbcAbsCheckExistanceNode;
	}

	public Boolean getTotCD34CheckExistanceNode() {
		return totCD34CheckExistanceNode;
	}

	public void setTotCD34CheckExistanceNode(Boolean totCD34CheckExistanceNode) {
		this.totCD34CheckExistanceNode = totCD34CheckExistanceNode;
	}

	public Boolean getTotCbuNuclCheckExistanceNode() {
		return totCbuNuclCheckExistanceNode;
	}

	public void setTotCbuNuclCheckExistanceNode(Boolean totCbuNuclCheckExistanceNode) {
		this.totCbuNuclCheckExistanceNode = totCbuNuclCheckExistanceNode;
	}

	public Boolean getViaTestMthdCheckExistanceNode() {
		return viaTestMthdCheckExistanceNode;
	}

	public void setViaTestMthdCheckExistanceNode(
			Boolean viaTestMthdCheckExistanceNode) {
		this.viaTestMthdCheckExistanceNode = viaTestMthdCheckExistanceNode;
	}

	public Boolean getCfuTestMthdCheckExistanceNode() {
		return cfuTestMthdCheckExistanceNode;
	}

	public void setCfuTestMthdCheckExistanceNode(
			Boolean cfuTestMthdCheckExistanceNode) {
		this.cfuTestMthdCheckExistanceNode = cfuTestMthdCheckExistanceNode;
	}

	public Boolean getCbVolCheckExitanceNode() {
		return cbVolCheckExitanceNode;
	}

	public void setCbVolCheckExitanceNode(Boolean cbVolCheckExitanceNode) {
		this.cbVolCheckExitanceNode = cbVolCheckExitanceNode;
	}

	public Boolean getRaceBlankOutExistence() {
		return raceBlankOutExistence;
	}

	public void setRaceBlankOutExistence(Boolean raceBlankOutExistence) {
		this.raceBlankOutExistence = raceBlankOutExistence;
	}

	public Boolean getEthnicityOmitted() {
		return ethnicityOmitted;
	}

	public void setEthnicityOmitted(Boolean ethnicityOmitted) {
		this.ethnicityOmitted = ethnicityOmitted;
	}

	public Boolean getGenderOmitted() {
		return genderOmitted;
	}

	public void setGenderOmitted(Boolean genderOmitted) {
		this.genderOmitted = genderOmitted;
	}

	public Boolean getMultipleBirthOmitted() {
		return multipleBirthOmitted;
	}

	public void setMultipleBirthOmitted(Boolean multipleBirthOmitted) {
		this.multipleBirthOmitted = multipleBirthOmitted;
	}

	public Boolean getTypeOfCollectionOmitted() {
		return typeOfCollectionOmitted;
	}

	public void setTypeOfCollectionOmitted(Boolean typeOfCollectionOmitted) {
		this.typeOfCollectionOmitted = typeOfCollectionOmitted;
	}

	public Boolean getTypeOfDeliveryOmitted() {
		return typeOfDeliveryOmitted;
	}

	public void setTypeOfDeliveryOmitted(Boolean typeOfDeliveryOmitted) {
		this.typeOfDeliveryOmitted = typeOfDeliveryOmitted;
	}

	public Boolean getProcesingStatDateOmitted() {
		return procesingStatDateOmitted;
	}

	public void setProcesingStatDateOmitted(Boolean procesingStatDateOmitted) {
		this.procesingStatDateOmitted = procesingStatDateOmitted;
	}

	public Boolean getCbuProcessingProtocolOmitted() {
		return cbuProcessingProtocolOmitted;
	}

	public void setCbuProcessingProtocolOmitted(Boolean cbuProcessingProtocolOmitted) {
		this.cbuProcessingProtocolOmitted = cbuProcessingProtocolOmitted;
	}

	public Boolean getBloodTypeOmitted() {
		return bloodTypeOmitted;
	}

	public void setBloodTypeOmitted(Boolean bloodTypeOmitted) {
		this.bloodTypeOmitted = bloodTypeOmitted;
	}

	public Boolean getRhTypeOmitted() {
		return rhTypeOmitted;
	}

	public void setRhTypeOmitted(Boolean rhTypeOmitted) {
		this.rhTypeOmitted = rhTypeOmitted;
	}

	public Boolean getBactCulStratDateOmitted() {
		return bactCulStratDateOmitted;
	}

	public void setBactCulStratDateOmitted(Boolean bactCulStratDateOmitted) {
		this.bactCulStratDateOmitted = bactCulStratDateOmitted;
	}

	public Boolean getBactCulOmitted() {
		return bactCulOmitted;
	}

	public void setBactCulOmitted(Boolean bactCulOmitted) {
		this.bactCulOmitted = bactCulOmitted;
	}

	public Boolean getFungCulStartDateOmitted() {
		return fungCulStartDateOmitted;
	}

	public void setFungCulStartDateOmitted(Boolean fungCulStartDateOmitted) {
		this.fungCulStartDateOmitted = fungCulStartDateOmitted;
	}

	public Boolean getFungCulOmitted() {
		return fungCulOmitted;
	}

	public void setFungCulOmitted(Boolean fungCulOmitted) {
		this.fungCulOmitted = fungCulOmitted;
	}

	public Boolean getHemoGlobinScreenOmitted() {
		return hemoGlobinScreenOmitted;
	}

	public void setHemoGlobinScreenOmitted(Boolean hemoGlobinScreenOmitted) {
		this.hemoGlobinScreenOmitted = hemoGlobinScreenOmitted;
	}

	public Boolean getBirthDateOmitted() {
		return birthDateOmitted;
	}

	public void setBirthDateOmitted(Boolean birthDateOmitted) {
		this.birthDateOmitted = birthDateOmitted;
	}

	public Boolean getCollectionDateomitted() {
		return collectionDateomitted;
	}

	public void setCollectionDateomitted(Boolean collectionDateomitted) {
		this.collectionDateomitted = collectionDateomitted;
	}

	public EntitySamplesPojo getSavedEntitySamplesPojo() {
		return savedEntitySamplesPojo;
	}

	public void setSavedEntitySamplesPojo(EntitySamplesPojo savedEntitySamplesPojo) {
		this.savedEntitySamplesPojo = savedEntitySamplesPojo;
	}

	public Boolean getFrzDateOmmited() {
		return frzDateOmmited;
	}

	public void setFrzDateOmmited(Boolean frzDateOmmited) {
		this.frzDateOmmited = frzDateOmmited;
	}

	public Boolean getCbuStorageLocValid() {
		return cbuStorageLocValid;
	}

	public void setCbuStorageLocValid(Boolean cbuStorageLocValid) {
		this.cbuStorageLocValid = cbuStorageLocValid;
	}

    public String getCmsData() {
		return cmsData;
	}

	public void setCmsData(String cmsData) {
		this.cmsData = cmsData;
	}

	public String getFdaData() {
		return fdaData;
	}

	public void setFdaData(String fdaData) {
		this.fdaData = fdaData;
	}

	public List<FormResponsesPojo> getMatIdmFormresponsepojoList() {
		return matIdmFormresponsepojoList;
	}

	public void setMatIdmFormresponsepojoList(
			List<FormResponsesPojo> matIdmFormresponsepojoList) {
		this.matIdmFormresponsepojoList = matIdmFormresponsepojoList;
	}

	public FormVersionPojo getMatIdmFormVersionPojo() {
		return matIdmFormVersionPojo;
	}

	public void setMatIdmFormVersionPojo(FormVersionPojo matIdmFormVersionPojo) {
		this.matIdmFormVersionPojo = matIdmFormVersionPojo;
	}

	public FormsPojo getMatIdmFormsPojo() {
		return matIdmFormsPojo;
	}

	public void setMatIdmFormsPojo(FormsPojo matIdmFormsPojo) {
		this.matIdmFormsPojo = matIdmFormsPojo;
	}

	public FormVersionPojo getMrqFormVersionPojo() {
		return mrqFormVersionPojo;
	}

	public void setMrqFormVersionPojo(FormVersionPojo mrqFormVersionPojo) {
		this.mrqFormVersionPojo = mrqFormVersionPojo;
	}

	public FormVersionPojo getFmhqFormVersionPojo() {
		return fmhqFormVersionPojo;
	}

	public void setFmhqFormVersionPojo(FormVersionPojo fmhqFormVersionPojo) {
		this.fmhqFormVersionPojo = fmhqFormVersionPojo;
	}

	public List<FormResponsesPojo> getMrqFormresponsepojoList() {
		return mrqFormresponsepojoList;
	}

	public void setMrqFormresponsepojoList(
			List<FormResponsesPojo> mrqFormresponsepojoList) {
		this.mrqFormresponsepojoList = mrqFormresponsepojoList;
	}

	public List<FormResponsesPojo> getFmhqFormresponsepojoList() {
		return fmhqFormresponsepojoList;
	}

	public void setFmhqFormresponsepojoList(
			List<FormResponsesPojo> fmhqFormresponsepojoList) {
		this.fmhqFormresponsepojoList = fmhqFormresponsepojoList;
	}

	public FormsPojo getMrqFormsPojo() {
		return mrqFormsPojo;
	}

	public void setMrqFormsPojo(FormsPojo mrqFormsPojo) {
		this.mrqFormsPojo = mrqFormsPojo;
	}

	public FormsPojo getFmhqFormsPojo() {
		return fmhqFormsPojo;
	}

	public void setFmhqFormsPojo(FormsPojo fmhqFormsPojo) {
		this.fmhqFormsPojo = fmhqFormsPojo;
	}

     
     
        
    private List<PatLabsPojo> preTestLst;
    private List<PatLabsPojo> postTestLst;
    
    
    
    
	public Boolean getLicStatusSectionOmitted() {
		return licStatusSectionOmitted;
	}

	public void setLicStatusSectionOmitted(Boolean licStatusSectionOmitted) {
		this.licStatusSectionOmitted = licStatusSectionOmitted;
	}

	public Boolean getCbuDemographicSectionOmitted() {
		return cbuDemographicSectionOmitted;
	}

	public void setCbuDemographicSectionOmitted(Boolean cbuDemographicSectionOmitted) {
		this.cbuDemographicSectionOmitted = cbuDemographicSectionOmitted;
	}

	public Boolean getLabSummarySectionOmited() {
		return labSummarySectionOmited;
	}

	public void setLabSummarySectionOmited(Boolean labSummarySectionOmited) {
		this.labSummarySectionOmited = labSummarySectionOmited;
	}

	public Boolean getCbuSamplesSectionOmited() {
		return cbuSamplesSectionOmited;
	}

	public void setCbuSamplesSectionOmited(Boolean cbuSamplesSectionOmited) {
		this.cbuSamplesSectionOmited = cbuSamplesSectionOmited;
	}

	public Boolean getEligStatusSectionOmited() {
		return eligStatusSectionOmited;
	}

	public void setEligStatusSectionOmited(Boolean eligStatusSectionOmited) {
		this.eligStatusSectionOmited = eligStatusSectionOmited;
	}

	public EntitySamplesPojo getSamplesPojo() {
		return samplesPojo;
	}

	public void setSamplesPojo(EntitySamplesPojo samplesPojo) {
		this.samplesPojo = samplesPojo;
	}

	public Long[] getRaces() {
		return races;
	}

	public void setRaces(Long[] races) {
		this.races = races;
	}

	public EntityStatusPojo getEntityStatusPojo() {
		return entityStatusPojo;
	}

	public void setEntityStatusPojo(EntityStatusPojo entityStatusPojo) {
		this.entityStatusPojo = entityStatusPojo;
	}

	public List<EntityStatusReasonPojo> getEntityStatusReasonPojoList() {
		return entityStatusReasonPojoList;
	}

	public void setEntityStatusReasonPojoList(
			List<EntityStatusReasonPojo> entityStatusReasonPojoList) {
		this.entityStatusReasonPojoList = entityStatusReasonPojoList;
	}

	public EntitySamplesPojo getEntitySamplesPojo() {
		return entitySamplesPojo;
	}

	public void setEntitySamplesPojo(EntitySamplesPojo entitySamplesPojo) {
		this.entitySamplesPojo = entitySamplesPojo;
	}

	public EntityStatusPojo getEntityStatusLicensePojo() {
		return entityStatusLicensePojo;
	}

	public void setEntityStatusLicensePojo(EntityStatusPojo entityStatusLicensePojo) {
		this.entityStatusLicensePojo = entityStatusLicensePojo;
	}

	public List<EntityStatusReasonPojo> getEntityStatusReasonPojoLicenseList() {
		return entityStatusReasonPojoLicenseList;
	}

	public void setEntityStatusReasonPojoLicenseList(
			List<EntityStatusReasonPojo> entityStatusReasonPojoLicenseList) {
		this.entityStatusReasonPojoLicenseList = entityStatusReasonPojoLicenseList;
	}

	public List<PatLabsPojo> getPreTestLst() {
		return preTestLst;
	}

	public void setPreTestLst(List<PatLabsPojo> preTestLst) {
		this.preTestLst = preTestLst;
	}

	public List<PatLabsPojo> getPostTestLst() {
		return postTestLst;
	}

	public void setPostTestLst(List<PatLabsPojo> postTestLst) {
		this.postTestLst = postTestLst;
	}
	

	public Boolean getSaveFmhqForm() {
		return saveFmhqForm;
	}

	public void setSaveFmhqForm(Boolean saveFmhqForm) {
		this.saveFmhqForm = saveFmhqForm;
	}

	public Boolean getSaveIdmForm() {
		return saveIdmForm;
	}

	public void setSaveIdmForm(Boolean saveIdmForm) {
		this.saveIdmForm = saveIdmForm;
	}

	public Boolean getSaveMrqForm() {
		return saveMrqForm;
	}

	public void setSaveMrqForm(Boolean saveMrqForm) {
		this.saveMrqForm = saveMrqForm;
	}

	public List<AdditionalIdsPojo> getAdditionalIdsPojos() {
		return additionalIdsPojos;
	}

	public void setAdditionalIdsPojos(List<AdditionalIdsPojo> additionalIdsPojos) {
		this.additionalIdsPojos = additionalIdsPojos;
	}

	public Boolean getRaceInvalid() {
		return raceInvalid;
	}

	public void setRaceInvalid(Boolean raceInvalid) {
		this.raceInvalid = raceInvalid;
	}

	public Boolean getProcedureOmitted() {
		return procedureOmitted;
	}

	public void setProcedureOmitted(Boolean procedureOmitted) {
		this.procedureOmitted = procedureOmitted;
	}

	public CBBProcedurePojo getProcedurePojo() {
		return procedurePojo;
	}

	public void setProcedurePojo(CBBProcedurePojo procedurePojo) {
		this.procedurePojo = procedurePojo;
	}

	public String getBabyGenderInvalidValue() {
		return babyGenderInvalidValue;
	}

	public void setBabyGenderInvalidValue(String babyGenderInvalidValue) {
		this.babyGenderInvalidValue = babyGenderInvalidValue;
	}

	public String getRaceInvalidValue() {
		return raceInvalidValue;
	}

	public void setRaceInvalidValue(String raceInvalidValue) {
		this.raceInvalidValue = raceInvalidValue;
	}

	public String getRhTypeInvalidValue() {
		return rhTypeInvalidValue;
	}

	public void setRhTypeInvalidValue(String rhTypeInvalidValue) {
		this.rhTypeInvalidValue = rhTypeInvalidValue;
	}

	public String getHemoGlobinScreenInvalidValue() {
		return hemoGlobinScreenInvalidValue;
	}

	public void setHemoGlobinScreenInvalidValue(String hemoGlobinScreenInvalidValue) {
		this.hemoGlobinScreenInvalidValue = hemoGlobinScreenInvalidValue;
	}

	public Boolean getBabyGenderInvalidExistence() {
		return babyGenderInvalidExistence;
	}

	public void setBabyGenderInvalidExistence(Boolean babyGenderInvalidExistence) {
		this.babyGenderInvalidExistence = babyGenderInvalidExistence;
	}

	public Boolean getEthnicityInvalidExistence() {
		return ethnicityInvalidExistence;
	}

	public void setEthnicityInvalidExistence(Boolean ethnicityInvalidExistence) {
		this.ethnicityInvalidExistence = ethnicityInvalidExistence;
	}

	public Boolean getRhTypeInvalidExistence() {
		return rhTypeInvalidExistence;
	}

	public void setRhTypeInvalidExistence(Boolean rhTypeInvalidExistence) {
		this.rhTypeInvalidExistence = rhTypeInvalidExistence;
	}

	public Boolean getHemoGlobinScreenInvalidExistence() {
		return hemoGlobinScreenInvalidExistence;
	}

	public void setHemoGlobinScreenInvalidExistence(
			Boolean hemoGlobinScreenInvalidExistence) {
		this.hemoGlobinScreenInvalidExistence = hemoGlobinScreenInvalidExistence;
	}

	public String getEthnicityInvalidvalue() {
		return ethnicityInvalidvalue;
	}

	public void setEthnicityInvalidvalue(String ethnicityInvalidvalue) {
		this.ethnicityInvalidvalue = ethnicityInvalidvalue;
	}

	public Boolean getLicenseMultipleSameReason() {
		return licenseMultipleSameReason;
	}

	public void setLicenseMultipleSameReason(Boolean licenseMultipleSameReason) {
		this.licenseMultipleSameReason = licenseMultipleSameReason;
	}

	public Boolean getEligMultipleSameReason() {
		return eligMultipleSameReason;
	}

	public void setEligMultipleSameReason(Boolean eligMultipleSameReason) {
		this.eligMultipleSameReason = eligMultipleSameReason;
	}

	public Boolean getEligbilityReasonOmmitted() {
		return eligbilityReasonOmmitted;
	}

	public void setEligbilityReasonOmmitted(Boolean eligbilityReasonOmmitted) {
		this.eligbilityReasonOmmitted = eligbilityReasonOmmitted;
	}

	public Boolean getCfuCountOtherDescInvalidExistence() {
		return cfuCountOtherDescInvalidExistence;
	}

	public void setCfuCountOtherDescInvalidExistence(
			Boolean cfuCountOtherDescInvalidExistence) {
		this.cfuCountOtherDescInvalidExistence = cfuCountOtherDescInvalidExistence;
	}

	public Boolean getViabilityOtherDescInvalidExistence() {
		return viabilityOtherDescInvalidExistence;
	}

	public void setViabilityOtherDescInvalidExistence(
			Boolean viabilityOtherDescInvalidExistence) {
		this.viabilityOtherDescInvalidExistence = viabilityOtherDescInvalidExistence;
	}

	public String getCfuCountOtherDescInvalidValue() {
		return cfuCountOtherDescInvalidValue;
	}

	public void setCfuCountOtherDescInvalidValue(
			String cfuCountOtherDescInvalidValue) {
		this.cfuCountOtherDescInvalidValue = cfuCountOtherDescInvalidValue;
	}

	public String getViabilityOtherDescInvalidValue() {
		return viabilityOtherDescInvalidValue;
	}

	public void setViabilityOtherDescInvalidValue(
			String viabilityOtherDescInvalidValue) {
		this.viabilityOtherDescInvalidValue = viabilityOtherDescInvalidValue;
	}
	
}