package com.velos.ordercomponent.business.pojoobjects;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class MailConfigPojo extends AuditablePojo {

	private Long mailConfigID;
	private String mailConfigCode;
	private String mailConfigSubject;
	private String mailConfigContent;
	private String mailConfigContentType;
	private String mailConfigFrom;
	private String mailConfigTo;
	private String mailConfigBCC;
	private String mailConfigCC;
	private Boolean mailConfigHasAttachment;
	private Long mailConfigCodeListOrgId;
	public Long getMailConfigID() {
		return mailConfigID;
	}
	public void setMailConfigID(Long mailConfigID) {
		this.mailConfigID = mailConfigID;
	}
	public String getMailConfigCode() {
		return mailConfigCode;
	}
	public void setMailConfigCode(String mailConfigCode) {
		this.mailConfigCode = mailConfigCode;
	}
	public String getMailConfigSubject() {
		return mailConfigSubject;
	}
	public void setMailConfigSubject(String mailConfigSubject) {
		this.mailConfigSubject = mailConfigSubject;
	}
	public String getMailConfigContent() {
		return mailConfigContent;
	}
	public void setMailConfigContent(String mailConfigContent) {
		this.mailConfigContent = mailConfigContent;
	}
	public String getMailConfigContentType() {
		return mailConfigContentType;
	}
	public void setMailConfigContentType(String mailConfigContentType) {
		this.mailConfigContentType = mailConfigContentType;
	}
	public String getMailConfigFrom() {
		return mailConfigFrom;
	}
	public void setMailConfigFrom(String mailConfigFrom) {
		this.mailConfigFrom = mailConfigFrom;
	}
	public String getMailConfigBCC() {
		return mailConfigBCC;
	}
	public void setMailConfigBCC(String mailConfigBCC) {
		this.mailConfigBCC = mailConfigBCC;
	}
	public String getMailConfigCC() {
		return mailConfigCC;
	}
	public void setMailConfigCC(String mailConfigCC) {
		this.mailConfigCC = mailConfigCC;
	}
	public Boolean getMailConfigHasAttachment() {
		return mailConfigHasAttachment;
	}
	public void setMailConfigHasAttachment(Boolean mailConfigHasAttachment) {
		this.mailConfigHasAttachment = mailConfigHasAttachment;
	}
	public Long getMailConfigCodeListOrgId() {
		return mailConfigCodeListOrgId;
	}
	public void setMailConfigCodeListOrgId(Long mailConfigCodeListOrgId) {
		this.mailConfigCodeListOrgId = mailConfigCodeListOrgId;
	}
	private Boolean deletedFlag;
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public String getMailConfigTo() {
		return mailConfigTo;
	}
	public void setMailConfigTo(String mailConfigTo) {
		this.mailConfigTo = mailConfigTo;
	}
}
