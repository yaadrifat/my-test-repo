package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


public class FMHQ {
	private String cbb_val_protocol_used;
	private XMLGregorianCalendar hlth_hist_frm_cmplt_date;
	private String m_or_f_adopted_ind;
	private String m_or_f_adopted_medhist_ind;
	private String baby_moth_fath_bld_rel_ind;
	private String preg_use_dnr_egg_sperm_ind;
	private String dnr_egg_sperm_fmhq_avail_ind;	
	private String abn_prenat_test_rslt_ind;
	private String abn_prenat_test_rslt_testname;	
	private String abn_prenat_test_abort_desc;
	private String abn_prenat_test_rslt_diag_ind;
	private String abn_prenat_test_rslt_diag_desc;
	private String child_die_age_10_ind;
	private String child_die_age_10_desc;	
	private String stillborn_ind;
	private String stillborn_cause;
	private String answ_both_moth_fath_ind;
	private Cancer cancer;
	private RedBloodCell bloodCell;
	private WhiteBloodCell whiteBloodCell;
	private ImmuneDeficiencies deficiencies;
	private PlatletDiseases platletDiseases;
	private OtherBloodDiseases otherBloodDiseases;
	private Thalassemia thalassemia;
	private SickleCell sickleCell;
	private MetabolicStorage metabolicStorage;
	private Aids aids;
	private SevereAutoImmune autoImmune;
	private ChronicBloodTransfusions bloodTransfusions;
	private HemolyticAnemia hemolyticAnemia;
	private SpleenRemoved removed;
	private GallbladderRemoved gallbladderRemoved;
	private CJD cjd;
	private OtherSeriousDiseasesSep otherSeriousDiseasesSep;
	
	@XmlElement(name="cbb_val_protocol_used")
	public String getCbb_val_protocol_used() {
		return cbb_val_protocol_used;
	}
	public void setCbb_val_protocol_used(String cbb_val_protocol_used) {
		this.cbb_val_protocol_used = cbb_val_protocol_used;
	}
	@XmlElement(name="hlth_hist_frm_cmplt_date")
	@XmlJavaTypeAdapter(type=XMLGregorianCalendar.class,value=XMLGeorgarianCalanderAdapter.class)
	public XMLGregorianCalendar getHlth_hist_frm_cmplt_date() {
		return hlth_hist_frm_cmplt_date;
	}
	public void setHlth_hist_frm_cmplt_date(XMLGregorianCalendar hlthHistFrmCmpltDate) {
		hlth_hist_frm_cmplt_date = hlthHistFrmCmpltDate;
	}
	@XmlElement(name="m_or_f_adopted_ind")
	public String getM_or_f_adopted_ind() {
		return m_or_f_adopted_ind;
	}
	public void setM_or_f_adopted_ind(String mOrFAdoptedInd) {
		m_or_f_adopted_ind = mOrFAdoptedInd;
	}
	@XmlElement(name="m_or_f_adopted_medhist_ind")
	public String getM_or_f_adopted_medhist_ind() {
		return m_or_f_adopted_medhist_ind;
	}
	public void setM_or_f_adopted_medhist_ind(String mOrFAdoptedMedhistInd) {
		m_or_f_adopted_medhist_ind = mOrFAdoptedMedhistInd;
	}
	@XmlElement(name="baby_moth_fath_bld_rel_ind")
	public String getBaby_moth_fath_bld_rel_ind() {
		return baby_moth_fath_bld_rel_ind;
	}
	public void setBaby_moth_fath_bld_rel_ind(String babyMothFathBldRelInd) {
		baby_moth_fath_bld_rel_ind = babyMothFathBldRelInd;
	}
	@XmlElement(name="preg_use_dnr_egg_sperm_ind")
	public String getPreg_use_dnr_egg_sperm_ind() {
		return preg_use_dnr_egg_sperm_ind;
	}
	public void setPreg_use_dnr_egg_sperm_ind(String pregUseDnrEggSpermInd) {
		preg_use_dnr_egg_sperm_ind = pregUseDnrEggSpermInd;
	}
	@XmlElement(name="dnr_egg_sperm_fmhq_avail_ind")
	public String getDnr_egg_sperm_fmhq_avail_ind() {
		return dnr_egg_sperm_fmhq_avail_ind;
	}
	public void setDnr_egg_sperm_fmhq_avail_ind(String dnrEggSpermFmhqAvailInd) {
		dnr_egg_sperm_fmhq_avail_ind = dnrEggSpermFmhqAvailInd;
	}
	@XmlElement(name="abn_prenat_test_rslt_ind")
	public String getAbn_prenat_test_rslt_ind() {
		return abn_prenat_test_rslt_ind;
	}
	public void setAbn_prenat_test_rslt_ind(String abnPrenatTestRsltInd) {
		abn_prenat_test_rslt_ind = abnPrenatTestRsltInd;
	}
	@XmlElement(name="abn_prenat_test_rslt_testname")
	public String getAbn_prenat_test_rslt_testname() {
		return abn_prenat_test_rslt_testname;
	}
	public void setAbn_prenat_test_rslt_testname(String abnPrenatTestRsltTestname) {
		abn_prenat_test_rslt_testname = abnPrenatTestRsltTestname;
	}
	@XmlElement(name="abn_prenat_test_abort_desc")
	public String getAbn_prenat_test_abort_desc() {
		return abn_prenat_test_abort_desc;
	}
	public void setAbn_prenat_test_abort_desc(String abnPrenatTestAbortDesc) {
		abn_prenat_test_abort_desc = abnPrenatTestAbortDesc;
	}
	@XmlElement(name="abn_prenat_test_rslt_diag_ind")
	public String getAbn_prenat_test_rslt_diag_ind() {
		return abn_prenat_test_rslt_diag_ind;
	}
	public void setAbn_prenat_test_rslt_diag_ind(String abnPrenatTestRsltDiagInd) {
		abn_prenat_test_rslt_diag_ind = abnPrenatTestRsltDiagInd;
	}
	@XmlElement(name="abn_prenat_test_rslt_diag_desc")
	public String getAbn_prenat_test_rslt_diag_desc() {
		return abn_prenat_test_rslt_diag_desc;
	}
	public void setAbn_prenat_test_rslt_diag_desc(String abnPrenatTestRsltDiagDesc) {
		abn_prenat_test_rslt_diag_desc = abnPrenatTestRsltDiagDesc;
	}
	@XmlElement(name="child_die_age_10_ind")
	public String getChild_die_age_10_ind() {
		return child_die_age_10_ind;
	}
	public void setChild_die_age_10_ind(String childDieAge_10Ind) {
		child_die_age_10_ind = childDieAge_10Ind;
	}
	@XmlElement(name="child_die_age_10_desc")
	public String getChild_die_age_10_desc() {
		return child_die_age_10_desc;
	}
	public void setChild_die_age_10_desc(String childDieAge_10Desc) {
		child_die_age_10_desc = childDieAge_10Desc;
	}
	@XmlElement(name="stillborn_ind")
	public String getStillborn_ind() {
		return stillborn_ind;
	}
	public void setStillborn_ind(String stillbornInd) {
		stillborn_ind = stillbornInd;
	}
	@XmlElement(name="stillborn_cause")
	public String getStillborn_cause() {
		return stillborn_cause;
	}
	public void setStillborn_cause(String stillbornCause) {
		stillborn_cause = stillbornCause;
	}
	@XmlElement(name="answ_both_moth_fath_ind")
	public String getAnsw_both_moth_fath_ind() {
		return answ_both_moth_fath_ind;
	}
	public void setAnsw_both_moth_fath_ind(String answBothMothFathInd) {
		answ_both_moth_fath_ind = answBothMothFathInd;
	}
	@XmlElement(name="Cancer")
	public Cancer getCancer() {
		return cancer;
	}
	public void setCancer(Cancer cancer) {
		this.cancer = cancer;
	}
	@XmlElement(name="RedBloodCell")
	public RedBloodCell getBloodCell() {
		return bloodCell;
	}
	public void setBloodCell(RedBloodCell bloodCell) {
		this.bloodCell = bloodCell;
	}
	@XmlElement(name="WhiteBloodCell")
	public WhiteBloodCell getWhiteBloodCell() {
		return whiteBloodCell;
	}
	public void setWhiteBloodCell(WhiteBloodCell whiteBloodCell) {
		this.whiteBloodCell = whiteBloodCell;
	}
	@XmlElement(name="ImmuneDeficiencies")
	public ImmuneDeficiencies getDeficiencies() {
		return deficiencies;
	}
	public void setDeficiencies(ImmuneDeficiencies deficiencies) {
		this.deficiencies = deficiencies;
	}
	@XmlElement(name="PlatletDiseases")
	public PlatletDiseases getPlatletDiseases() {
		return platletDiseases;
	}
	public void setPlatletDiseases(PlatletDiseases platletDiseases) {
		this.platletDiseases = platletDiseases;
	}
	@XmlElement(name="OtherBloodDiseases")
	public OtherBloodDiseases getOtherBloodDiseases() {
		return otherBloodDiseases;
	}
	public void setOtherBloodDiseases(OtherBloodDiseases otherBloodDiseases) {
		this.otherBloodDiseases = otherBloodDiseases;
	}
	@XmlElement(name="Thalassemia")
	public Thalassemia getThalassemia() {
		return thalassemia;
	}
	public void setThalassemia(Thalassemia thalassemia) {
		this.thalassemia = thalassemia;
	}
	@XmlElement(name="SickleCell")
	public SickleCell getSickleCell() {
		return sickleCell;
	}
	public void setSickleCell(SickleCell sickleCell) {
		this.sickleCell = sickleCell;
	}
	@XmlElement(name="MetabolicStorage")
	public MetabolicStorage getMetabolicStorage() {
		return metabolicStorage;
	}
	public void setMetabolicStorage(MetabolicStorage metabolicStorage) {
		this.metabolicStorage = metabolicStorage;
	}
	@XmlElement(name="Aids")
	public Aids getAids() {
		return aids;
	}
	public void setAids(Aids aids) {
		this.aids = aids;
	}
	@XmlElement(name="SevereAutoImmune")
	public SevereAutoImmune getAutoImmune() {
		return autoImmune;
	}
	public void setAutoImmune(SevereAutoImmune autoImmune) {
		this.autoImmune = autoImmune;
	}
	@XmlElement(name="ChronicBloodTransfusions")
	public ChronicBloodTransfusions getBloodTransfusions() {
		return bloodTransfusions;
	}
	public void setBloodTransfusions(ChronicBloodTransfusions bloodTransfusions) {
		this.bloodTransfusions = bloodTransfusions;
	}
	@XmlElement(name="HemolyticAnemia")
	public HemolyticAnemia getHemolyticAnemia() {
		return hemolyticAnemia;
	}
	public void setHemolyticAnemia(HemolyticAnemia hemolyticAnemia) {
		this.hemolyticAnemia = hemolyticAnemia;
	}
	@XmlElement(name="SpleenRemoved")
	public SpleenRemoved getRemoved() {
		return removed;
	}
	public void setRemoved(SpleenRemoved removed) {
		this.removed = removed;
	}
	@XmlElement(name="GallbladderRemoved")
	public GallbladderRemoved getGallbladderRemoved() {
		return gallbladderRemoved;
	}
	public void setGallbladderRemoved(GallbladderRemoved gallbladderRemoved) {
		this.gallbladderRemoved = gallbladderRemoved;
	}
	@XmlElement(name="CJD")
	public CJD getCjd() {
		return cjd;
	}
	public void setCjd(CJD cjd) {
		this.cjd = cjd;
	}
	@XmlElement(name="OtherSeriousDiseases")
	public OtherSeriousDiseasesSep getOtherSeriousDiseasesSep() {
		return otherSeriousDiseasesSep;
	}
	public void setOtherSeriousDiseasesSep(
			OtherSeriousDiseasesSep otherSeriousDiseasesSep) {
		this.otherSeriousDiseasesSep = otherSeriousDiseasesSep;
	}
	
	
	
	
	
}
