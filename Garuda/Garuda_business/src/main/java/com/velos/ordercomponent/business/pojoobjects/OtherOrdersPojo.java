package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

public class OtherOrdersPojo {
	
	private Long orderId;
	private Long cordId;
	private Long pksite;
	private Long siteId;
	private String cbuRegId;
	private String localCbuId;
	private String dateOfentryStr;
	private String dateOfentry;
	private int noOfDays;
	private int percentageComplete;
	private String status;
	private String reason;
	private String removedBy;
	private Long fkcordstatus;
	private Long fkcordreason;
	private Long fkuserremovedby;
	
	
	public Long getOrderId() {
		return orderId;
	}
	public String getDateOfentryStr() {
		return dateOfentryStr;
	}
	public void setDateOfentryStr(String dateOfentryStr) {
		this.dateOfentryStr = dateOfentryStr;
	}
	public Long getCordId() {
		return cordId;
	}
	public Long getPksite() {
		return pksite;
	}
	public Long getSiteId() {
		return siteId;
	}
	public String getCbuRegId() {
		return cbuRegId;
	}
	public String getLocalCbuId() {
		return localCbuId;
	}
	public String getDateOfentry() {
		return dateOfentry;
	}
	public int getNoOfDays() {
		return noOfDays;
	}
	public int getPercentageComplete() {
		return percentageComplete;
	}
	public String getStatus() {
		return status;
	}
	public String getReason() {
		return reason;
	}
	public String getRemovedBy() {
		return removedBy;
	}
	public Long getFkcordstatus() {
		return fkcordstatus;
	}
	public Long getFkcordreason() {
		return fkcordreason;
	}
	public Long getFkuserremovedby() {
		return fkuserremovedby;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public void setCordId(Long cordId) {
		this.cordId = cordId;
	}
	public void setPksite(Long pksite) {
		this.pksite = pksite;
	}
	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}
	public void setCbuRegId(String cbuRegId) {
		this.cbuRegId = cbuRegId;
	}
	public void setLocalCbuId(String localCbuId) {
		this.localCbuId = localCbuId;
	}
	public void setDateOfentry(String dateOfentry) {
		this.dateOfentry = dateOfentry;
	}
	public void setNoOfDays(int noOfDays) {
		this.noOfDays = noOfDays;
	}
	public void setPercentageComplete(int percentageComplete) {
		this.percentageComplete = percentageComplete;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public void setRemovedBy(String removedBy) {
		this.removedBy = removedBy;
	}
	public void setFkcordstatus(Long fkcordstatus) {
		this.fkcordstatus = fkcordstatus;
	}
	public void setFkcordreason(Long fkcordreason) {
		this.fkcordreason = fkcordreason;
	}
	public void setFkuserremovedby(Long fkuserremovedby) {
		this.fkuserremovedby = fkuserremovedby;
	}
	
	

}
