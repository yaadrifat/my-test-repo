package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;

public class RaceEthnicity {

	private String ethnicity;
	private Races races;
	@XmlElement(name="ethnicity")
	public String getEthnicity() {
		return ethnicity;
	}
	public void setEthnicity(String ethnicity) {
		this.ethnicity = ethnicity;
	}
	@XmlElement(name="Races")
	public Races getRaces() {
		return races;
	}
	public void setRaces(Races races) {
		this.races = races;
	}
	
}
