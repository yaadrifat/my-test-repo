package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ER_TASK_LIST")
public class TaskList {
	private Long pkTaskId;
	private Long taskType;
	private Date taskStrtDate;
	private Date taskEndDate;
	private Long taskAssignTo;
	private Long domainId;
	private Long createdBy;
	private Date createdOn;
	private Long lstModBy;
	private Date lstModOn;
	private String ipAddress;
	private String deletedFlag;
	
	@Id
	@GeneratedValue
	@Column(name="PK_TASK")
	public Long getPkTaskId() {
		return pkTaskId;
	}
	public void setPkTaskId(Long pkTaskId) {
		this.pkTaskId = pkTaskId;
	}
	
	@Column(name="TASK_TYPE")
	public Long getTaskType() {
		return taskType;
	}
	public void setTaskType(Long taskType) {
		this.taskType = taskType;
	}
	
	@Column(name="TASK_START_DATE")
	public Date getTaskStrtDate() {
		return taskStrtDate;
	}
	public void setTaskStrtDate(Date taskStrtDate) {
		this.taskStrtDate = taskStrtDate;
	}
	
	@Column(name="TASK_END_DATE")
	public Date getTaskEndDate() {
		return taskEndDate;
	}
	public void setTaskEndDate(Date taskEndDate) {
		this.taskEndDate = taskEndDate;
	}
	
	@Column(name="TASK_ASSIGNED_TO")
	public Long getTaskAssignTo() {
		return taskAssignTo;
	}
	public void setTaskAssignTo(Long taskAssignTo) {
		this.taskAssignTo = taskAssignTo;
	}
	
	@Column(name="DOMAIN_ID")
	public Long getDomainId() {
		return domainId;
	}
	public void setDomainId(Long domainId) {
		this.domainId = domainId;
	}
	
	@Column(name="CREATOR")
	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name="CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column(name="LAST_MODIFIED_BY")
	public Long getLstModBy() {
		return lstModBy;
	}

	public void setLstModBy(Long lstModBy) {
		this.lstModBy = lstModBy;
	}

	@Column(name="LAST_MODIFIED_DATE")
	public Date getLstModOn() {
		return lstModOn;
	}

	public void setLstModOn(Date lstModOn) {
		this.lstModOn = lstModOn;
	}

	@Column(name="IP_ADDRESS")
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name="DELETEDFLAG")
	public String getDeletedFlag() {
		return deletedFlag;
	}

	public void setDeletedFlag(String deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	


}