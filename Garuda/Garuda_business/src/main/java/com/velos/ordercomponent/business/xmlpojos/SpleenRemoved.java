package com.velos.ordercomponent.business.xmlpojos;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class SpleenRemoved {

	private String spleen_removed_ind;
	/*private Relations relations;
	@XmlElement(name="Relations")
	public Relations getRelations() {
		return relations;
	}
	public void setRelations(Relations relations) {
		this.relations = relations;
	}*/
	@XmlElement(name="spleen_removed_ind")
	public String getSpleen_removed_ind() {
		return spleen_removed_ind;
	}
	public void setSpleen_removed_ind(String spleenRemovedInd) {
		spleen_removed_ind = spleenRemovedInd;
	}
	private List<Relations> Relations;
	@XmlElement(name="Relations")
	public List<Relations> getRelations() {
		return Relations;
	}
	public void setRelations(List<Relations> relations) {
		Relations = relations;
	}
	
}
