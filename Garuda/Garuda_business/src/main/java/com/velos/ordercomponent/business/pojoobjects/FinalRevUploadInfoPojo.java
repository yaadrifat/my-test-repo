package com.velos.ordercomponent.business.pojoobjects;

public class FinalRevUploadInfoPojo extends AuditablePojo{
	
	private Long pkFinalRevUploadInfo;
	private Long entityType;
	private Long entityId;
	private Long fkCbuUploadInfo;
	private Long fkFinalCbuReview;
	private Boolean deletedFlag;
	private String attachmentType;
	
	public Long getPkFinalRevUploadInfo() {
		return pkFinalRevUploadInfo;
	}
	public void setPkFinalRevUploadInfo(Long pkFinalRevUploadInfo) {
		this.pkFinalRevUploadInfo = pkFinalRevUploadInfo;
	}
	public Long getEntityType() {
		return entityType;
	}
	public void setEntityType(Long entityType) {
		this.entityType = entityType;
	}
	public Long getEntityId() {
		return entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	public Long getFkCbuUploadInfo() {
		return fkCbuUploadInfo;
	}
	public void setFkCbuUploadInfo(Long fkCbuUploadInfo) {
		this.fkCbuUploadInfo = fkCbuUploadInfo;
	}
	public Long getFkFinalCbuReview() {
		return fkFinalCbuReview;
	}
	public void setFkFinalCbuReview(Long fkFinalCbuReview) {
		this.fkFinalCbuReview = fkFinalCbuReview;
	}
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	public String getAttachmentType() {
		return attachmentType;
	}
	public void setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
	}
	

}
