package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;

public class ViabilityPostPrc_PreCryopPreservation {

	private String post_prcsng_viability_test;
	private String post_prcsng_viability_oth_describe;
	private String post_prcsng_viability_cnt;
	
	@XmlElement(name="post_prcsng_viability_test")
	public String getPost_prcsng_viability_test() {
		return post_prcsng_viability_test;
	}
	public void setPost_prcsng_viability_test(String postPrcsngViabilityTest) {
		post_prcsng_viability_test = postPrcsngViabilityTest;
	}
	
	@XmlElement(name="post_prcsng_viability_oth_describe")	
	public String getPost_prcsng_viability_oth_describe() {
		return post_prcsng_viability_oth_describe;
	}
	public void setPost_prcsng_viability_oth_describe(
			String postPrcsngViabilityOthDescribe) {
		post_prcsng_viability_oth_describe = postPrcsngViabilityOthDescribe;
	}
	
	@XmlElement(name="post_prcsng_viability_cnt")
	public String getPost_prcsng_viability_cnt() {
		return post_prcsng_viability_cnt;
	}
	public void setPost_prcsng_viability_cnt(String postPrcsngViabilityCnt) {
		post_prcsng_viability_cnt = postPrcsngViabilityCnt;
	}
	
}
