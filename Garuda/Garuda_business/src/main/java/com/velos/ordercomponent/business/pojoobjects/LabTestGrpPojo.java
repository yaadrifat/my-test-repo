package com.velos.ordercomponent.business.pojoobjects;

public class LabTestGrpPojo {
	
	private Long pklabtestgrp;
	private Long fktestid;
	private Long fklabgrp;
	
	public Long getPklabtestgrp() {
		return pklabtestgrp;
	}
	public void setPklabtestgrp(Long pklabtestgrp) {
		this.pklabtestgrp = pklabtestgrp;
	}
	public Long getFktestid() {
		return fktestid;
	}
	public void setFktestid(Long fktestid) {
		this.fktestid = fktestid;
	}
	public Long getFklabgrp() {
		return fklabgrp;
	}
	public void setFklabgrp(Long fklabgrp) {
		this.fklabgrp = fklabgrp;
	}

}
