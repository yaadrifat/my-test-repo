
package com.velos.ordercomponent.helper;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.velos.ordercomponent.business.domain.Auditable;
import com.velos.ordercomponent.business.domain.EmailAttachment;
import com.velos.ordercomponent.business.domain.MailConfig;
import com.velos.ordercomponent.business.domain.Notification;
import com.velos.ordercomponent.business.pojoobjects.EmailAttachmentPojo;
import com.velos.ordercomponent.business.pojoobjects.MailConfigPojo;
import com.velos.ordercomponent.business.pojoobjects.NotificationPojo;
import com.velos.ordercomponent.business.util.HibernateUtil;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.VelosUtil;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class MailHelper {
	public static final Log log = LogFactory.getLog(MailHelper.class);
	
	public static Object getMailConfigurationByCode(String code )
	{
		return getMailConfigurationByCode(code, null);
	}
	
	public static Object getMailConfigurationByCode(String code , Session session)
	{
		
		boolean sessionFlag = false;
		MailConfig mailConfig  = null;
		MailConfigPojo mailConfigPojo = new MailConfigPojo();
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String sql = "select {mc.*} from ER_MAILCONFIG mc where mc.MAILCONFIG_MAILCODE = ?";
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity("mc", MailConfig.class);
			query.setString(0, code);
			if(query.list().size() != 0){
			mailConfig = (MailConfig)query.list().get(0);
			}
//			if (sessionFlag) {
//				 close session, if session created in this method
//				 session.getTransaction().commit();
//			}

		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
      //  System.out.print("In Mail Helper"+mailConfig);
        mailConfigPojo = (MailConfigPojo)ObjectTransfer.transferObjects(mailConfig, mailConfigPojo);
		return mailConfigPojo;
	}
	
	public static NotificationPojo sendEmail(NotificationPojo notificationPojo,Boolean fromUI)
	{
		return 	sendEmail(notificationPojo,fromUI,null);
	}
	
	public static NotificationPojo sendEmail(NotificationPojo notificationPojo,Boolean fromUI, Session session)
	{
		//log.debug("check in mail helper"+notificationPojo);
		boolean sessionFlag = false;
		Object object = null;
		try{
		 // check session 
		if(session== null){
			// create new session
		  session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}
		Notification notification = new Notification();
		notification = (Notification)ObjectTransfer.transferObjects(notificationPojo, notification);
		if(fromUI)
		notification = (Notification)new VelosUtil().setAuditableInfo(notification);
		//Auditable auditable = new VelosUtil().setAuditableInfo();
		//notification.setAuditable(auditable);
		//notification = (Notification)object;
		if(notification.getNotifyId()!=null)
		{
		  session.update(notification);	
		}else{
			session.save(notification);
		}
		notificationPojo = (NotificationPojo)ObjectTransfer.transferObjects(notification, notificationPojo);		
		if(sessionFlag){
			// close session, if session created in this method
			session.getTransaction().commit();
		}
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
			
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return notificationPojo;
	}
	
	public static List<EmailAttachmentPojo> saveEmailDocuments(List<EmailAttachmentPojo> pojos){
		return saveEmailDocuments(pojos, null);
	}
	
   public static List<EmailAttachmentPojo> saveEmailDocuments(List<EmailAttachmentPojo> pojos,Session session){
	   List<EmailAttachment> list = new ArrayList<EmailAttachment>();
	   List<EmailAttachment> attachments = new ArrayList<EmailAttachment>();
	   EmailAttachment emailAttachment ;
	   EmailAttachmentPojo attachmentPojo;
	   for(EmailAttachmentPojo p : pojos){
		   emailAttachment = new EmailAttachment();
		   emailAttachment = (EmailAttachment)ObjectTransfer.transferObjects(p, emailAttachment);
		   emailAttachment = (EmailAttachment)new VelosUtil().setAuditableInfo(emailAttachment);
		   list.add(emailAttachment);
	   }
	   for(EmailAttachment e : list){
		   emailAttachment = new EmailAttachment();
		   emailAttachment = saveEmailDocument(e, session);
		   attachments.add(emailAttachment);
	   }
		pojos.clear();
		if(attachments!=null && attachments.size()>0){
			for(EmailAttachment e1 : attachments){
				attachmentPojo = new EmailAttachmentPojo();
				attachmentPojo = (EmailAttachmentPojo)ObjectTransfer.transferObjects(e1, attachmentPojo);
				pojos.add(attachmentPojo);
			}
		}
		return pojos;
	}
   
   public static EmailAttachment saveEmailDocument(EmailAttachment attachment,Session session){
	   boolean sessionFlag = false;
		try{
		 // check session 
		if(session== null){
			// create new session
		  session = HibernateUtil.getSessionFactory().getCurrentSession();
			sessionFlag = true;
			session.beginTransaction();
		}
		if(attachment.getEmailDocumentId()!=null){
			session.merge(attachment);
		}else{
			session.save(attachment);
		}				
			
		if(sessionFlag){
			// close session, if session created in this method
			session.getTransaction().commit();			
		}
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
			
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return attachment;
   }

}

