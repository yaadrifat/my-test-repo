package com.velos.ordercomponent.business.pojoobjects;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class EligibilityDeclPojo extends AuditablePojo {
	
	private Long pkEligDecl;
	private Boolean mrqQues1;
	private Boolean mrqQues1a;
	private Boolean mrqQues1b;
	private String mrqQues1AddDetail;
	private Boolean mrqQues2;
	private String mrqQues2AddDetail;
	private Boolean phyFindQues3;
	private Boolean phyFindQues3a;
	private Boolean phyFindQues3b;
	private Boolean phyFindQues3c;
	private Boolean phyFindQues3d;
	private String phyFindQues3AddDetail;
	private Boolean phyFindQues4;
	private String phyFindQues4AddDetail;
	private Boolean phyFindQues5;
	private Boolean phyFindQues5a;
	private Boolean phyFindQues5b;
	private Boolean phyFindQues5c;
	private Boolean phyFindQues5d;
	private Boolean phyFindQues5e;
	private Boolean phyFindQues5f;
	private Boolean phyFindQues5g;
	private Boolean phyFindQues5h;
	private Boolean phyFindQues5i;
	private String phyFindQues5AddDetail;
	private Boolean idmQues6;
	private Boolean idmQues6a;
	private Boolean idmQues6b;
	private String idmQues6AddDetail;
	private Boolean idmQues7;
	private Boolean idmQues7a;
	private Boolean idmQues7b;
	private String idmQues7AddDetail;
	private Boolean idmQues8;
	private String idmQues8AddDetail;
	private Long entityId;
	private Long entityType;
	public Long getPkEligDecl() {
		return pkEligDecl;
	}
	public void setPkEligDecl(Long pkEligDecl) {
		this.pkEligDecl = pkEligDecl;
	}
	public Boolean getMrqQues1() {
		return mrqQues1;
	}
	public void setMrqQues1(Boolean mrqQues1) {
		this.mrqQues1 = mrqQues1;
	}
	public Boolean getMrqQues1a() {
		return mrqQues1a;
	}
	public void setMrqQues1a(Boolean mrqQues1a) {
		this.mrqQues1a = mrqQues1a;
	}
	public Boolean getMrqQues1b() {
		return mrqQues1b;
	}
	public void setMrqQues1b(Boolean mrqQues1b) {
		this.mrqQues1b = mrqQues1b;
	}
	public String getMrqQues1AddDetail() {
		return mrqQues1AddDetail;
	}
	public void setMrqQues1AddDetail(String mrqQues1AddDetail) {
		this.mrqQues1AddDetail = mrqQues1AddDetail;
	}
	public Boolean getMrqQues2() {
		return mrqQues2;
	}
	public void setMrqQues2(Boolean mrqQues2) {
		this.mrqQues2 = mrqQues2;
	}
	public String getMrqQues2AddDetail() {
		return mrqQues2AddDetail;
	}
	public void setMrqQues2AddDetail(String mrqQues2AddDetail) {
		this.mrqQues2AddDetail = mrqQues2AddDetail;
	}
	public Boolean getPhyFindQues3() {
		return phyFindQues3;
	}
	public void setPhyFindQues3(Boolean phyFindQues3) {
		this.phyFindQues3 = phyFindQues3;
	}
	public Boolean getPhyFindQues3a() {
		return phyFindQues3a;
	}
	public void setPhyFindQues3a(Boolean phyFindQues3a) {
		this.phyFindQues3a = phyFindQues3a;
	}
	public Boolean getPhyFindQues3b() {
		return phyFindQues3b;
	}
	public void setPhyFindQues3b(Boolean phyFindQues3b) {
		this.phyFindQues3b = phyFindQues3b;
	}
	public Boolean getPhyFindQues3c() {
		return phyFindQues3c;
	}
	public void setPhyFindQues3c(Boolean phyFindQues3c) {
		this.phyFindQues3c = phyFindQues3c;
	}
	public Boolean getPhyFindQues3d() {
		return phyFindQues3d;
	}
	public void setPhyFindQues3d(Boolean phyFindQues3d) {
		this.phyFindQues3d = phyFindQues3d;
	}
	public String getPhyFindQues3AddDetail() {
		return phyFindQues3AddDetail;
	}
	public void setPhyFindQues3AddDetail(String phyFindQues3AddDetail) {
		this.phyFindQues3AddDetail = phyFindQues3AddDetail;
	}
	public Boolean getPhyFindQues4() {
		return phyFindQues4;
	}
	public void setPhyFindQues4(Boolean phyFindQues4) {
		this.phyFindQues4 = phyFindQues4;
	}
	public String getPhyFindQues4AddDetail() {
		return phyFindQues4AddDetail;
	}
	public void setPhyFindQues4AddDetail(String phyFindQues4AddDetail) {
		this.phyFindQues4AddDetail = phyFindQues4AddDetail;
	}
	public Boolean getPhyFindQues5() {
		return phyFindQues5;
	}
	public void setPhyFindQues5(Boolean phyFindQues5) {
		this.phyFindQues5 = phyFindQues5;
	}
	public Boolean getPhyFindQues5a() {
		return phyFindQues5a;
	}
	public void setPhyFindQues5a(Boolean phyFindQues5a) {
		this.phyFindQues5a = phyFindQues5a;
	}
	public Boolean getPhyFindQues5b() {
		return phyFindQues5b;
	}
	public void setPhyFindQues5b(Boolean phyFindQues5b) {
		this.phyFindQues5b = phyFindQues5b;
	}
	public Boolean getPhyFindQues5c() {
		return phyFindQues5c;
	}
	public void setPhyFindQues5c(Boolean phyFindQues5c) {
		this.phyFindQues5c = phyFindQues5c;
	}
	public Boolean getPhyFindQues5d() {
		return phyFindQues5d;
	}
	public void setPhyFindQues5d(Boolean phyFindQues5d) {
		this.phyFindQues5d = phyFindQues5d;
	}
	public Boolean getPhyFindQues5e() {
		return phyFindQues5e;
	}
	public void setPhyFindQues5e(Boolean phyFindQues5e) {
		this.phyFindQues5e = phyFindQues5e;
	}
	public Boolean getPhyFindQues5f() {
		return phyFindQues5f;
	}
	public void setPhyFindQues5f(Boolean phyFindQues5f) {
		this.phyFindQues5f = phyFindQues5f;
	}
	public Boolean getPhyFindQues5g() {
		return phyFindQues5g;
	}
	public void setPhyFindQues5g(Boolean phyFindQues5g) {
		this.phyFindQues5g = phyFindQues5g;
	}
	public Boolean getPhyFindQues5h() {
		return phyFindQues5h;
	}
	public void setPhyFindQues5h(Boolean phyFindQues5h) {
		this.phyFindQues5h = phyFindQues5h;
	}
	public Boolean getPhyFindQues5i() {
		return phyFindQues5i;
	}
	public void setPhyFindQues5i(Boolean phyFindQues5i) {
		this.phyFindQues5i = phyFindQues5i;
	}
	public String getPhyFindQues5AddDetail() {
		return phyFindQues5AddDetail;
	}
	public void setPhyFindQues5AddDetail(String phyFindQues5AddDetail) {
		this.phyFindQues5AddDetail = phyFindQues5AddDetail;
	}
	public Boolean getIdmQues6() {
		return idmQues6;
	}
	public void setIdmQues6(Boolean idmQues6) {
		this.idmQues6 = idmQues6;
	}
	public Boolean getIdmQues6a() {
		return idmQues6a;
	}
	public void setIdmQues6a(Boolean idmQues6a) {
		this.idmQues6a = idmQues6a;
	}
	public Boolean getIdmQues6b() {
		return idmQues6b;
	}
	public void setIdmQues6b(Boolean idmQues6b) {
		this.idmQues6b = idmQues6b;
	}
	public String getIdmQues6AddDetail() {
		return idmQues6AddDetail;
	}
	public void setIdmQues6AddDetail(String idmQues6AddDetail) {
		this.idmQues6AddDetail = idmQues6AddDetail;
	}
	public Boolean getIdmQues7() {
		return idmQues7;
	}
	public void setIdmQues7(Boolean idmQues7) {
		this.idmQues7 = idmQues7;
	}
	public Boolean getIdmQues7a() {
		return idmQues7a;
	}
	public void setIdmQues7a(Boolean idmQues7a) {
		this.idmQues7a = idmQues7a;
	}
	public Boolean getIdmQues7b() {
		return idmQues7b;
	}
	public void setIdmQues7b(Boolean idmQues7b) {
		this.idmQues7b = idmQues7b;
	}
	public String getIdmQues7AddDetail() {
		return idmQues7AddDetail;
	}
	public void setIdmQues7AddDetail(String idmQues7AddDetail) {
		this.idmQues7AddDetail = idmQues7AddDetail;
	}
	public Boolean getIdmQues8() {
		return idmQues8;
	}
	public void setIdmQues8(Boolean idmQues8) {
		this.idmQues8 = idmQues8;
	}
	public String getIdmQues8AddDetail() {
		return idmQues8AddDetail;
	}
	public void setIdmQues8AddDetail(String idmQues8AddDetail) {
		this.idmQues8AddDetail = idmQues8AddDetail;
	}
	public Long getEntityId() {
		return entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	public Long getEntityType() {
		return entityType;
	}
	public void setEntityType(Long entityType) {
		this.entityType = entityType;
	}
	
	
}
