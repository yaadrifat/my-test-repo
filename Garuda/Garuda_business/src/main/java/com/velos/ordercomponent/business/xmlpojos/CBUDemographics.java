package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.datatype.XMLGregorianCalendar;

public class CBUDemographics {
	
	private String b_dte;
	private String sex;
	private String coll_dte;
	private String multiplebirth;
	private String typeofcollection;
	private String typeofdelivery;
	private RaceEthnicity raceEthnicity;
	
	@XmlElement(name="b_dte")
	public String getB_dte() {
		return b_dte;
	}
	public void setB_dte(String bDte) {
		b_dte = bDte;
	}
	@XmlElement(name="sex")
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	@XmlElement(name="coll_dte")
	public String getColl_dte() {
		return coll_dte;
	}
	public void setColl_dte(String collDte) {
		coll_dte = collDte;
	}
	@XmlElement(name="multiplebirth")
	public String getMultiplebirth() {
		return multiplebirth;
	}
	public void setMultiplebirth(String multiplebirth) {
		this.multiplebirth = multiplebirth;
	}
	@XmlElement(name="typeofcollection")
	public String getTypeofcollection() {
		return typeofcollection;
	}
	public void setTypeofcollection(String typeofcollection) {
		this.typeofcollection = typeofcollection;
	}
	@XmlElement(name="typeofdelivery")
	public String getTypeofdelivery() {
		return typeofdelivery;
	}
	public void setTypeofdelivery(String typeofdelivery) {
		this.typeofdelivery = typeofdelivery;
	}
	@XmlElement(name="RaceEthnicity")
	public RaceEthnicity getRaceEthnicity() {
		return raceEthnicity;
	}
	public void setRaceEthnicity(RaceEthnicity raceEthnicity) {
		this.raceEthnicity = raceEthnicity;
	}
	

}
