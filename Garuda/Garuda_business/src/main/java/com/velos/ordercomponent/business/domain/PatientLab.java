package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
/**
 *@author Jyoti
 *
 */
@Entity
@Table(name="CB_PATIENT_LAB")
@SequenceGenerator(sequenceName="SEQ_CB_PATIENT_LAB",name="SEQ_CB_PATIENT_LAB",allocationSize=1)

public class PatientLab extends Auditable {
	
	
	private Long pk_PatientLabTestId; 
	private Long fk_LabTestId;
	private Long testEntityId;  
	private Long testEntityType; 
	private String testResult; 
	private Date testDate; 
	private String testResultUnit;
	private Long fk_OrderId;
	private Long fk_AttachmentId; 
	private Long reviewedBy; 
	private Boolean deletedFlag;
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_PATIENT_LAB")
	@Column(name="PK_PATIENT_LAB_TEST")
	public Long getPk_PatientLabTestId() {
		return pk_PatientLabTestId;
	}
	public void setPk_PatientLabTestId(Long pk_PatientLabTestId) {
		this.pk_PatientLabTestId = pk_PatientLabTestId;
	}
	@Column(name="FK_LAB_TESTID")
	public Long getFk_LabTestId() {
		return fk_LabTestId;
	}
	public void setFk_LabTestId(Long fk_LabTestId) {
		this.fk_LabTestId = fk_LabTestId;
	}
	@Column(name="TEST_ENTITYID")
	public Long getTestEntityId() {
		return testEntityId;
	}
	
	public void setTestEntityId(Long testEntityId) {
		this.testEntityId = testEntityId;
	}
	@Column(name="TEST_ENTITYTYPE")
	public Long getTestEntityType() {
		return testEntityType;
	}
	public void setTestEntityType(Long testEntityType) {
		this.testEntityType = testEntityType;
	}
	@Column(name="TEST_RESULT")
	public String getTestResult() {
		return testResult;
	}
	public void setTestResult(String testResult) {
		this.testResult = testResult;
	}
	@Column(name="TEST_DATE")
	public Date getTestDate() {
		return testDate;
	}
	public void setTestDate(Date testDate) {
		this.testDate = testDate;
	}
	@Column(name="TEST_RESULT_UNIT")
	public String getTestResultUnit() {
		return testResultUnit;
	}
	public void setTestResultUnit(String testResultUnit) {
		this.testResultUnit = testResultUnit;
	}
	@Column(name="FK_ORDERID")
	public Long getFk_OrderId() {
		return fk_OrderId;
	}
	public void setFk_OrderId(Long fk_OrderId) {
		this.fk_OrderId = fk_OrderId;
	}
	@Column(name="FK_ATTACHMENTID")
	public Long getFk_AttachmentId() {
		return fk_AttachmentId;
	}
	public void setFk_AttachmentId(Long fk_AttachmentId) {
		this.fk_AttachmentId = fk_AttachmentId;
	}
	@Column(name="REVIEWED_BY")
	public Long getReviewedBy() {
		return reviewedBy;
	}
	
	public void setReviewedBy(Long reviewedBy) {
		this.reviewedBy = reviewedBy;
	}

	@Column(name="DELETEDFLAG")
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	
}