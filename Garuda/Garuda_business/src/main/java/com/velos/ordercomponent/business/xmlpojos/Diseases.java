package com.velos.ordercomponent.business.xmlpojos;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;

public class Diseases {

	//private Disease disease;
	private List<Disease> Disease;
	private OtherSeriousDiseases otherSeriousDiseases;
	/*@XmlElement(name="Disease")
	public Disease getDisease() {
		return disease;
	}
	public void setDisease(Disease disease) {
		this.disease = disease;
	}*/
	@XmlElement(name="OtherSeriousDiseases")
	public OtherSeriousDiseases getOtherSeriousDiseases() {
		return otherSeriousDiseases;
	}
	public void setOtherSeriousDiseases(OtherSeriousDiseases otherSeriousDiseases) {
		this.otherSeriousDiseases = otherSeriousDiseases;
	}
	@XmlElement(name="Disease")
	public List<Disease> getDisease() {
		return Disease;
	}
	public void setDisease(List<Disease> disease) {
		Disease = disease;
	}
	
	
}
