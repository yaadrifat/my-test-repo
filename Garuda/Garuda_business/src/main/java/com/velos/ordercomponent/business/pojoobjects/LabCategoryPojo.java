package com.velos.ordercomponent.business.pojoobjects;



public class LabCategoryPojo extends AuditablePojo {
	private Long pk_CategoryId;
	private String categoryName;
	private Long categoryType;
	private Boolean deletedFlag;
	public Long getPk_CategoryId() {
		return pk_CategoryId;
	}
	public void setPk_CategoryId(Long pk_CategoryId) {
		this.pk_CategoryId = pk_CategoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public Long getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(Long categoryType) {
		this.categoryType = categoryType;
	}
	
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	
	
}
