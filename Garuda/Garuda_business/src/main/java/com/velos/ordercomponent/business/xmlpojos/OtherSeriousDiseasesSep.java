package com.velos.ordercomponent.business.xmlpojos;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class OtherSeriousDiseasesSep {
	private String inher_other_dises_ind;	
	/*private Relations relations;*/
	/*private DiseasesSeperate diseasesSeperate;*/

	@XmlElement(name="inher_other_dises_ind")
	public String getInher_other_dises_ind() {
		return inher_other_dises_ind;
	}

	public void setInher_other_dises_ind(String inherOtherDisesInd) {
		inher_other_dises_ind = inherOtherDisesInd;
	}

	/*@XmlElement(name="Relations")
	public Relations getRelations() {
		return relations;
	}

	public void setRelations(Relations relations) {
		this.relations = relations;
	}*/

	/*@XmlElement(name="Diseases")
	public DiseasesSeperate getDiseasesSeperate() {
		return diseasesSeperate;
	}

	public void setDiseasesSeperate(DiseasesSeperate diseasesSeperate) {
		this.diseasesSeperate = diseasesSeperate;
	}	
	*/
	private List<Relations> Relations;
	@XmlElement(name="Relations")
	public List<Relations> getRelations() {
		return Relations;
	}
	public void setRelations(List<Relations> relations) {
		Relations = relations;
	}
}
