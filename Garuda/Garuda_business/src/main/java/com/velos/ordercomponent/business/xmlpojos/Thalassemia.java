package com.velos.ordercomponent.business.xmlpojos;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class Thalassemia {
	
	private String inher_thalassemia_ind;
	/*private Relations relations;
	@XmlElement(name="Relations")
	public Relations getRelations() {
		return relations;
	}
	public void setRelations(Relations relations) {
		this.relations = relations;
	}*/
	@XmlElement(name="inher_thalassemia_ind")
	public String getInher_thalassemia_ind() {
		return inher_thalassemia_ind;
	}
	public void setInher_thalassemia_ind(String inherThalassemiaInd) {
		inher_thalassemia_ind = inherThalassemiaInd;
	}
	private List<Relations> Relations;
	@XmlElement(name="Relations")
	public List<Relations> getRelations() {
		return Relations;
	}
	public void setRelations(List<Relations> relations) {
		Relations = relations;
	}
}
