package com.velos.ordercomponent.business.util;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;

import org.hibernate.LockMode;
import org.hibernate.Session;

public class CommonDao<T> implements Serializable {
	
	Class<T> persistenceClass;
	
	public CommonDao(){
		this.persistenceClass = (Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	public CommonDao(Class<T> persistenceClass){
		this.persistenceClass = persistenceClass;
	}
	
	public Class<T> getPersistenceClass(){
		return persistenceClass;
	}
	
	public static Session getSession(Session session){
		if(session == null){
		 session = HibernateUtil.getSessionFactory().getCurrentSession();
		 session.beginTransaction();
		}
		return session;		
	}
	
	public static void closeSession(Session session){
		if(session.isOpen()){
			session.close();
		}
	}
	
	public T findById(Long Id){
		return findById(Id, false);
	}
	
	public T findById(Long Id, Boolean flag){
		T entity = null;
		Session session= null;
		try {
			session = getSession(null);
			if(flag){
			  entity = (T)(session.load(getPersistenceClass(), Id, LockMode.UPGRADE));
			}else{
			  entity = (T)(session.load(getPersistenceClass(), Id));	
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
			    closeSession(session);
			}
		}
		return entity;
		
	}

}
