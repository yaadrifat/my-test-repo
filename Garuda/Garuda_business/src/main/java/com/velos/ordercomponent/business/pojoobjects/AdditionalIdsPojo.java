package com.velos.ordercomponent.business.pojoobjects;

import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;

public class AdditionalIdsPojo extends AuditablePojo {
	
	private Long pkAdditionalIds;
	private String additionalId;
	private String additionalIdDesc;
	private Long entityType;
	private Long entityId;
	private Long additionalIdType;
	private String additionalIdValueType;
	private Boolean deletedFlag;
	
	public Long getPkAdditionalIds() {
		return pkAdditionalIds;
	}
	public void setPkAdditionalIds(Long pkAdditionalIds) {
		this.pkAdditionalIds = pkAdditionalIds;
	}
	@RequiredStringValidator(message = "Please Enter Id", shortCircuit = true)
	public String getAdditionalId() {
		return additionalId;
	}
	public void setAdditionalId(String additionalId) {
		this.additionalId = additionalId;
	}
	@RequiredStringValidator(message = "Please Enter Description", shortCircuit = true)
	public String getAdditionalIdDesc() {
		return additionalIdDesc;
	}
	public void setAdditionalIdDesc(String additionalIdDesc) {
		this.additionalIdDesc = additionalIdDesc;
	}
	public Long getEntityType() {
		return entityType;
	}
	public void setEntityType(Long entityType) {
		this.entityType = entityType;
	}
	public Long getEntityId() {
		return entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	public Long getAdditionalIdType() {
		return additionalIdType;
	}
	public void setAdditionalIdType(Long additionalIdType) {
		this.additionalIdType = additionalIdType;
	}
	@RequiredStringValidator(message = "Please Select Value", shortCircuit = true )
	public String getAdditionalIdValueType() {
		return additionalIdValueType;
	}
	public void setAdditionalIdValueType(String additionalIdValueType) {
		this.additionalIdValueType = additionalIdValueType;
	}
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	
}
