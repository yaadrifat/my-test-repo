package com.velos.ordercomponent.helper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Query;
import javax.swing.text.StyledEditorKit.BoldAction;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.velos.ordercomponent.business.domain.BestHLA;
import com.velos.ordercomponent.business.domain.CdrCbu;
import com.velos.ordercomponent.business.domain.ClinicalNote;
import com.velos.ordercomponent.business.domain.EntitySamples;
import com.velos.ordercomponent.business.domain.HLA;
import com.velos.ordercomponent.business.domain.LabTest;
import com.velos.ordercomponent.business.domain.PatLabs;
import com.velos.ordercomponent.business.pojoobjects.BestHLAPojo;
import com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo;
import com.velos.ordercomponent.business.pojoobjects.ClinicalNotePojo;
import com.velos.ordercomponent.business.pojoobjects.EntitySamplesPojo;
import com.velos.ordercomponent.business.pojoobjects.LabTestPojo;
import com.velos.ordercomponent.business.pojoobjects.PatLabsPojo;
import com.velos.ordercomponent.business.util.HibernateUtil;
import com.velos.ordercomponent.business.util.ObjectTransfer;
import com.velos.ordercomponent.business.util.VelosUtil;

public class SampleInventHelper {
	public static final Log log=LogFactory.getLog(SampleInventHelper.class);
	
	public static EntitySamplesPojo getEntitySampleInfo(Long entityId){
		String sql = "From EntitySamples where entityId="+entityId;
		EntitySamplesPojo entitySamplesPojo = new EntitySamplesPojo();
		List<Object> list=new ArrayList<Object>();
		Session session=null;
		 EntitySamples  entitySample = new EntitySamples();
		try{
		if (session == null) {
			// create new session
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
		}
		list=(List<Object>) session.createQuery(sql).list();
		if(list!=null && list.size()>0){
			entitySamplesPojo = (EntitySamplesPojo)ObjectTransfer.transferObjects(list.get(0), entitySamplesPojo);
		}	
		//log.debug("Exiting velos helper!!!!!!!!!!!!!!");
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return entitySamplesPojo;
	}
	
	
	
	public static  EntitySamplesPojo saveOrUpdateSampInfo(EntitySamplesPojo entitySamplesPojo,Boolean fromUI) {
		//log.debug("Inside helper!!!!!!!!!!!!!!!!!!!!!!");
		return  saveOrUpdateSampInfo(entitySamplesPojo,null,fromUI);
	}
	
	  public static EntitySamplesPojo saveOrUpdateSampInfo(EntitySamplesPojo entitySamplesPojo,Session session,Boolean fromUI){
	
	       EntitySamples  entitySample = new EntitySamples();
		   entitySample = (EntitySamples) ObjectTransfer.transferObjects(entitySamplesPojo, entitySample);
		   if(fromUI)
		   entitySample = (EntitySamples)new VelosUtil().setAuditableInfo(entitySample);
		   boolean sessionFlag = false;		
			try{
				//log.debug("session" +session);
				if(session== null){
					// create new session
				  session = HibernateUtil.getSessionFactory().getCurrentSession();
					sessionFlag = true;
					session.beginTransaction();
				}
		  
				if(entitySample.getPkEntitySamples()!=null){
					//entitySample = (EntitySamples) ObjectTransfer.transferObjects(entitySamplesPojo, entitySample);	
					//log.debug("Pk note available!!!!!!!");
					session.update(entitySample);
				}else{
					//log.debug("Else part!!!!!!!!!!!!!!!!!!!!!!");
					//entitySample = (EntitySamples) ObjectTransfer.transferObjects(entitySamplesPojo, entitySample);
					session.saveOrUpdate(entitySample);
				}	
				if(sessionFlag){
					// close session, if session created in this method
					//log.debug("Commit Statement!!!!!!!!!!!!!!!!!");
					session.flush();
					session.getTransaction().commit();
				}		
				}catch (Exception e) {
					log.error(e.getMessage());
					e.printStackTrace();
				}finally{
					if(session.isOpen()){
						session.close();
					}
				}
			   entitySamplesPojo = (EntitySamplesPojo)ObjectTransfer.transferObjects(entitySample, entitySamplesPojo);
				return entitySamplesPojo;
	  }
	  
	  public static List<LabTestPojo> getProcessingTestInfo(String groupType){
		  //log.debug("Inside the Test Helper @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		  return getProcessingTestInfo(groupType,null);
	 }
	 
	  public static List<LabTestPojo> getProcessingTestInfo(String groupType,Session session) {
			//log.debug("Inside Test!!!!!!!!!!!!!!!!!!!!!!");
			boolean sessionFlag = false;
			
			List<Object> codeList = null;
			List<LabTestPojo> valueList = new ArrayList<LabTestPojo>();
			//List<LabTestPojo> labPojolist = new ArrayList<LabTestPojo>();
			LabTestPojo labtestpojo = null;
			List<LabTest> labDoaList  = new ArrayList<LabTest>();
			//List<String[]> valueList = new ArrayList<String[]>();
			
			//"select {p.*} from ER_LABTEST p where p.pk_labtest in (select fk_testid from er_labtestgrp where er_labtestgrp.fk_labgroup in "+ 
			// "(select pk_labgroup from er_labgroup where group_type ='PC'))";
			try{
				if(session ==null){
					//create new session
					session = HibernateUtil.getSessionFactory().getCurrentSession();
					sessionFlag = true;
					session.beginTransaction();
				}	
	        //log.debug("11111111111111111111111111111111111111111111111111111111111111111111111");
			String sqlTest = "select nvl(pk_labtest,0),nvl(labtest_name,' '),nvl(labtest_shortname,' '),nvl(labtest_hower,' ') from er_labtest where pk_labtest in (select fk_testid from er_labtestgrp where er_labtestgrp.fk_labgroup in" + 
			"(select pk_labgroup from er_labgroup where group_type ='"+groupType+"'))  order by LABTEST_SEQ";
			
			//log.debug("2222222222222222222222222222222222222222222222222222222222222222222222222222"+sqlTest);
			SQLQuery queryTest =session.createSQLQuery(sqlTest);
				
				codeList = queryTest.list();
				for (Iterator<Object> iter = codeList.iterator(); iter.hasNext();) {
					Object codeList22 = (Object) iter.next();
					labtestpojo = new LabTestPojo();
					
					Object[] dataArray = (Object[]) codeList22;
					//log.debug("Hi My Value is : "+dataArray[0].toString());
					labtestpojo.setPkLabtest(Long.parseLong(dataArray[0].toString()));
					labtestpojo.setLabtestName(dataArray[1].toString());
					labtestpojo.setShrtName(dataArray[2].toString());
					if( null!=dataArray[3] && !dataArray[3].equals("")){
					labtestpojo.setLabtestHower(dataArray[3].toString());
					}
					valueList.add(labtestpojo);
				}
			}catch (Exception e){
			//log.debug("Exception in Catch");
				log.error(e.getMessage());
			e.printStackTrace();
			}finally
			{
				if(session.isOpen()){
					session.close();
				}
			}
			return valueList;
		}
	  
	  public static List<LabTestPojo> getOtherProcessingTestInfo(Long specimenId,String groupType){
		  //log.debug("Inside the Test Helper @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		  return getOtherProcessingTestInfo(specimenId,groupType,null);
	 }
	 
	  public static List<LabTestPojo> getOtherProcessingTestInfo(Long specimenId,String groupType,Session session) {
			//log.debug("Inside Test!!!!!!!!!!!!!!!!!!!!!!");
			boolean sessionFlag = false;
			String sqlTest = "";
			List<Object> codeList = null;
			List<LabTestPojo> valueList = new ArrayList<LabTestPojo>();
			//List<LabTestPojo> labPojolist = new ArrayList<LabTestPojo>();
			LabTestPojo labtestpojo = null;
			List<LabTest> labDoaList  = new ArrayList<LabTest>();
			//List<String[]> valueList = new ArrayList<String[]>();
			
			//"select {p.*} from ER_LABTEST p where p.pk_labtest in (select fk_testid from er_labtestgrp where er_labtestgrp.fk_labgroup in "+ 
			// "(select pk_labgroup from er_labgroup where group_type ='PC'))";
			try{
				if(session ==null){
					//create new session
					session = HibernateUtil.getSessionFactory().getCurrentSession();
					sessionFlag = true;
					session.beginTransaction();
				}
				if(specimenId!=null && !specimenId.equals("")){
					sqlTest = "select pk_labtest,labtest_name from er_labtest where pk_labtest in(select fk_testid from er_labtestgrp where er_labtestgrp.fk_labgroup in(select pk_labgroup from er_labgroup where group_type ='"+groupType+"') and fk_testid in(select fk_testid from er_patlabs where fk_specimen='"+specimenId+"'))";
				
			
					SQLQuery queryTest =session.createSQLQuery(sqlTest);
				
				codeList = queryTest.list();
				for (Iterator<Object> iter = codeList.iterator(); iter.hasNext();) {
					Object codeList22 = (Object) iter.next();
					labtestpojo = new LabTestPojo();
					
					Object[] dataArray = (Object[]) codeList22;
					//log.debug("Hi My Value is : "+dataArray[0].toString());
					labtestpojo.setPkLabtest(Long.parseLong(dataArray[0].toString()));
					labtestpojo.setLabtestName(dataArray[1].toString());
					valueList.add(labtestpojo);
				}
				
				}
				
			}catch (Exception e){
			//log.debug("Exception in Catch");
				log.error(e.getMessage());
			e.printStackTrace();
			}finally
			{
				if(session.isOpen()){
					session.close();
				}
			}
			return valueList;
		}
	  
	  public static LabTestPojo getOtrProcingTstInfoByTestId(Long testId){
		  //log.debug("Inside the Test Helper @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		  return getOtrProcingTstInfoByTestId(testId,null);
	 }
	 
	  public static LabTestPojo getOtrProcingTstInfoByTestId(Long testId,Session session) {
			//log.debug("Inside Test!!!!!!!!!!!!!!!!!!!!!!");
			boolean sessionFlag = false;
			
			List<Object> codeList = null;
			//LabTestPojo valueList = new LabTestPojo();
			//List<LabTestPojo> labPojolist = new ArrayList<LabTestPojo>();
			LabTestPojo labtestpojo = new LabTestPojo();
			List<LabTest> labDoaList  = new ArrayList<LabTest>();
			//List<String[]> valueList = new ArrayList<String[]>();
			
			//"select {p.*} from ER_LABTEST p where p.pk_labtest in (select fk_testid from er_labtestgrp where er_labtestgrp.fk_labgroup in "+ 
			// "(select pk_labgroup from er_labgroup where group_type ='PC'))";
			try{
				if(session ==null){
					//create new session
					session = HibernateUtil.getSessionFactory().getCurrentSession();
					sessionFlag = true;
					session.beginTransaction();
				}
			String sqlTest = "select pk_labtest,labtest_name from er_labtest where pk_labtest ="+testId;
			
			
			SQLQuery queryTest =session.createSQLQuery(sqlTest);
				
				codeList = queryTest.list();
				for (Iterator<Object> iter = codeList.iterator(); iter.hasNext();) {
					Object codeList22 = (Object) iter.next();
					labtestpojo = new LabTestPojo();
					
					Object[] dataArray = (Object[]) codeList22;
					//log.debug("Hi My Value is : "+dataArray[0].toString());
					labtestpojo.setPkLabtest(Long.parseLong(dataArray[0].toString()));
					labtestpojo.setLabtestName(dataArray[1].toString());
				}
			}catch (Exception e){
			//log.debug("Exception in Catch");
				log.error(e.getMessage());
			e.printStackTrace();
			}finally
			{
				if(session.isOpen()){
					session.close();
				}
			}
			return labtestpojo;
		}
	 
	/* public static List<PatLabsPojo> getPreTestListInfo(String preGroup,Long specimenId){
		  //log.debug("---------------------------------------");
		  return  getPreTestListInfo(preGroup,specimenId,null);
	  }
	  
	 public static List<PatLabsPojo> getPreTestListInfo(String testType,Long specimenId, Session session ){
		 boolean sessionFlag = false;
		 List<PatLabs> patDoaList = new ArrayList<PatLabs>();
		 PatLabs patlabs = null;
	     List<PatLabsPojo> patlabsPojolist = new ArrayList<PatLabsPojo>();
		
	 for(PatLabsPojo pp : patlabsPojolist){
			patlabs = new PatLabs();
			patlabs =(PatLabs)ObjectTransfer.transferObjects(pp, patlabs);
			patDoaList.add(patlabs);
		}		
		try{
			if(session == null){
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
				}
			for(PatLabs pt : patDoaList){
			if(pt.getFkspecimen()!= null){
			session.merge(pt);	
			}else{
				session.save(pt);
			}
			if(patDoaList!= null && patDoaList.size()>0){
			for(PatLabs p1 : patDoaList){
			    patlabsPojolist.clear();
				patlabsPojolist = (List<PatLabsPojo>)ObjectTransfer.transferObjects(p1, patDoaList);
		   }
			}
			if(sessionFlag){
				session.getTransaction().commit();
			}
			}
				} catch (Exception e) {
				//log.debug(" Exception in TEST TYPE LIST " + e);
				e.printStackTrace();
			} finally {		
				 if(session.isOpen()){
				 session.close();			
				 }
			}
			return patlabsPojolist;
    }*/	
    
	public static List<PatLabsPojo> getvalueSavePreTestList(List<PatLabsPojo> alist,Boolean fromUI){
		return getvalueSavePreTestList(alist,null,fromUI);
	}
	
    public static List<PatLabsPojo> getvalueSavePreTestList(List<PatLabsPojo> alist,Session session,Boolean fromUI){
		   boolean sessionFlag = false;
		   List<PatLabs> preSaveDoaList = new ArrayList<PatLabs>();
		   List<PatLabsPojo> preSavePojoList = new ArrayList<PatLabsPojo>();
		   PatLabsPojo patlabpojo = new PatLabsPojo();
		   PatLabs savepatlabs = null;
 	  try{
	    if(session == null){
	      session = HibernateUtil.getSessionFactory().getCurrentSession();
		  sessionFlag = true;
		  session.beginTransaction();
		 }
		for(PatLabsPojo abc : alist){
		   savepatlabs = new PatLabs();
		   savepatlabs = (PatLabs) ObjectTransfer.transferObjects(patlabpojo, savepatlabs);
		   if(fromUI)
		   savepatlabs = (PatLabs)new VelosUtil().setAuditableInfo(savepatlabs);

		 savepatlabs =(PatLabs)ObjectTransfer.transferObjects(abc,savepatlabs);
		 if(savepatlabs.getTestresult()!=null && savepatlabs.getTestresult()!=""){
		  preSaveDoaList.add(savepatlabs);
		  }
		}
		for(PatLabs saveList : preSaveDoaList)
		 if(saveList.getPkpatlabs()!= null){
		  session.merge(saveList);
		 }
		else{
		 session.save(saveList);
	    }
	  if(preSaveDoaList!= null && preSaveDoaList.size()>0){
		 preSavePojoList.clear();
		}
	 for(PatLabs p1 : preSaveDoaList){
		patlabpojo = (PatLabsPojo)ObjectTransfer.transferObjects(p1, patlabpojo);
		preSavePojoList.add(patlabpojo);
		}	
	}catch (Exception e) {
		log.error(e.getMessage());
	    e.printStackTrace();
	 }finally{
		
			 try {
				session.getTransaction().commit();
			}catch (Exception e) {
				log.error(e.getMessage());
			    e.printStackTrace();
			 }finally{
				if(session.isOpen()){
				    session.close();
				}
			}
	  }
	  return preSavePojoList;
  }
 

	  public static List<PatLabsPojo> getvalueSavePostTestList(List<PatLabsPojo> blist,Boolean fromUI){
		   return getvalueSavePostTestList(blist,null,fromUI);
	    }

 
	public static List<PatLabsPojo> getvalueSavePostTestList(List<PatLabsPojo> blist ,Session session,Boolean fromUI){
			boolean sessionFlag = false;
			List<PatLabs> postSaveDoaList = new ArrayList<PatLabs>();
			List<PatLabsPojo> postSavePojoList = new ArrayList<PatLabsPojo>();
			PatLabsPojo postPatLabPojo = new PatLabsPojo();
			PatLabs postPatlab = null;
		try{
		  if(session == null){
		  session = HibernateUtil.getSessionFactory().getCurrentSession();
		  sessionFlag = true ;
		  session.beginTransaction();
		 }
       for(PatLabsPojo aa :blist){
		 postPatlab = new PatLabs();
		
		 postPatlab = (PatLabs) ObjectTransfer.transferObjects(postPatLabPojo, postPatlab);
		 if(fromUI)
		 postPatlab = (PatLabs)new VelosUtil().setAuditableInfo(postPatlab);
		 postPatlab = (PatLabs)ObjectTransfer.transferObjects(aa, postPatlab);
	     if(postPatlab.getTestresult()!=null && postPatlab.getTestresult()!=""){
		  postSaveDoaList.add(postPatlab);
		 }
        }
 	   for(PatLabs savepatlab : postSaveDoaList){
		if(savepatlab.getPkpatlabs()!= null){
		 session.merge(savepatlab);  
		}
	  else{
		 //log.debug("Else part!!!!!!!!!!!!!!!!!!!!!!");
		 session.save(savepatlab);
	     }
	  }
	  if(postSaveDoaList!= null && postSaveDoaList.size()>0){
		//postSaveDoaList.clear();
	   }
	 for(PatLabs p2 : postSaveDoaList){
		 if(p2.getTestresult()!= null && p2.getTestresult().length()>0){
		//postSaveDoaList.clear();
		postPatLabPojo = new PatLabsPojo();
		postPatLabPojo = (PatLabsPojo)ObjectTransfer.transferObjects(p2, postPatLabPojo);
		postSavePojoList.add(postPatLabPojo);
		}
	   }
   	}catch (Exception e){
   		log.error(e.getMessage());
   	    e.printStackTrace();
   	 }finally{
 		
		 try {
			session.getTransaction().commit();
		}catch (Exception e) {
			log.error(e.getMessage());
		    e.printStackTrace();
		 }finally{
			if(session.isOpen()){
			    session.close();
			}
		}
  }
	   return postSavePojoList;
     }
  
  public static List<PatLabsPojo> getPatLabsPojoInfo(Long specimenId,Long testValueType){
  	  return getPatLabsPojoInfo(specimenId,testValueType,null);
  }  
  
  public static List<PatLabsPojo> getPatLabsPojoInfo(Long specimenId,Long testValueType,Session session){
			  boolean sessionFlag = false;
			  List<PatLabsPojo> editPatlabsPojo = new ArrayList<PatLabsPojo>();
			  List<PatLabs> editPatLabs = new ArrayList<PatLabs>();
			  PatLabsPojo editPojo = null;
			  PatLabs editDoaPatlabs = null;
			

	  try{
	    if(session ==null){
		  session = HibernateUtil.getSessionFactory().getCurrentSession();
		  sessionFlag = true;
		  session.beginTransaction();
	    }
	     //From EntitySamples 
	    if(testValueType!=null && testValueType==1 || testValueType==2 || testValueType==3 || testValueType==4 || testValueType==5){
	   	String sqlResult = "From PatLabs where fkspecimen="+specimenId;
	   	 sqlResult += " and otherListValue='"+testValueType+"'";
	   	sqlResult += "and fkspecimen <> null";
	   	 org.hibernate.Query testQuery = session.createQuery(sqlResult);
	   	 editPatLabs = testQuery.list();
	    }
	    else{
	String sqlResult = "From PatLabs where fkspecimen="+specimenId;
	 sqlResult += " and fkTimingOfTest="+testValueType;
	 sqlResult += "and fkspecimen <> null";
	 org.hibernate.Query testQuery = session.createQuery(sqlResult);
	 editPatLabs = testQuery.list();
	    }        
	for(PatLabs updatelist :editPatLabs){
	   editPojo = new PatLabsPojo();
	   editPojo = (PatLabsPojo)ObjectTransfer.transferObjects(updatelist, editPojo); 
	   editPatlabsPojo.add(editPojo);
	  }
	  //editPatlabsPojo = testQuery.list();
	}catch (Exception e) {
		log.error(e.getMessage());
	    e.printStackTrace();
	}finally{
	  if(session.isOpen()){
	    session.close();
	   }
	}
  return editPatlabsPojo;
  }
  
  
  public static List<PatLabsPojo> getvalueSaveOtherTestList(List<PatLabsPojo> clist){
	   return getvalueSaveOtherTestList(clist,null);
   }

	public static List<PatLabsPojo> getvalueSaveOtherTestList(List<PatLabsPojo> clist ,Session session){
 		
		boolean sessionFlag = false;
		List<PatLabs> otherSaveDoaList = new ArrayList<PatLabs>();
		List<PatLabsPojo> otherSavePojoList = new ArrayList<PatLabsPojo>();
		PatLabsPojo otherPatLabsPojo = new PatLabsPojo();
		PatLabs otherPatlab = null;
		
		try{
			  if(session == null){
			  session = HibernateUtil.getSessionFactory().getCurrentSession();
			  sessionFlag = true ;
			  session.beginTransaction();
			  }
			  
		  for(PatLabsPojo cc :clist){
			 otherPatlab = new PatLabs();
			 otherPatlab = (PatLabs) ObjectTransfer.transferObjects(otherPatLabsPojo, otherPatlab);
			 otherPatlab = (PatLabs)new VelosUtil().setAuditableInfo(otherPatlab);
			
			 otherPatlab = (PatLabs)ObjectTransfer.transferObjects(cc, otherPatlab);
		   if(otherPatlab.getTestresult()!=null && otherPatlab.getTestresult()!=""){
			 otherSaveDoaList.add(otherPatlab);
				 }
			  }
		  for(PatLabs saveOtherPatlab : otherSaveDoaList){
			if(saveOtherPatlab.getPkpatlabs()!= null){
			  session.merge(saveOtherPatlab);  
		 	}
		    else{
		 	  //log.debug("Else part!!!!!!!!!!!!!!!!!!!!!!");
		 	  session.save(saveOtherPatlab);
		 	}
		   } 
		// if(otherSaveDoaList!=null && otherSaveDoaList.size()>0){
			//otherSaveDoaList.clear();
		 // }
		
			   
	     for(PatLabs p3 : otherSaveDoaList){
			otherPatLabsPojo = (PatLabsPojo)ObjectTransfer.transferObjects(p3, otherPatLabsPojo);
			otherSavePojoList.add(otherPatLabsPojo);
		    }
	    }catch (Exception e) {
	    	log.error(e.getMessage());
			 e.printStackTrace();
		}
		// TODO: handle exception
	    finally{
			
			 try {
				session.getTransaction().commit();
			}catch (Exception e) {
				log.error(e.getMessage());
			    e.printStackTrace();
			 }finally{
				if(session.isOpen()){
				    session.close();
				}
			}
	  }
		 return otherSavePojoList;
	  }
	
	public static List<PatLabsPojo> getTestResultByTestNames(Long specimenId,Long fkTimingOfTest,List<String> testNames){
	  	  return getTestResultByTestNames(specimenId,fkTimingOfTest,testNames,null);
}

public static List<PatLabsPojo> getTestResultByTestNames(Long specimenId,Long fkTimingOfTest,List<String> testNames,Session session) {
		boolean sessionFlag = false;
		String sqlQuery = "";
		List<PatLabsPojo> valueList = new ArrayList<PatLabsPojo>();
		PatLabsPojo patLabsPojo = null;	
		try{
			if(session ==null){
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String query = "Select {p.*} from Er_Patlabs p where p.Fk_Specimen=? and p.fk_timing_of_test=? and p.Fk_Testid in (Select Pk_Labtest From Er_Labtest Where LABTEST_NAME in (:testNames))";
			SQLQuery queryTest =session.createSQLQuery(query);
			queryTest.addEntity("p",PatLabs.class);
			queryTest.setLong(0,specimenId);
			queryTest.setLong(1,fkTimingOfTest);
			queryTest.setParameterList("testNames", testNames);				
			List<PatLabs> patLabsList = queryTest.list();
			if(patLabsList!=null && patLabsList.size()>0){				
				for(PatLabs p : patLabsList){
					patLabsPojo = new PatLabsPojo();
					patLabsPojo = (PatLabsPojo)ObjectTransfer.transferObjects(p, patLabsPojo);
					valueList.add(patLabsPojo);
				}
			}			
		}catch (Exception e){
		//log.debug("Exception in Catch");
			log.error(e.getMessage());
		e.printStackTrace();
		}finally
		{
			if(session.isOpen()){
				session.close();
			}
		}
		return valueList;
	}
}