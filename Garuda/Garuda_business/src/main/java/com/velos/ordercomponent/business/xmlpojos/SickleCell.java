package com.velos.ordercomponent.business.xmlpojos;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class SickleCell {

	private String inher_sickle_cell_ind;
	/*private Relations relations;
	@XmlElement(name="Relations")
	public Relations getRelations() {
		return relations;
	}
	public void setRelations(Relations relations) {
		this.relations = relations;
	}*/
	@XmlElement(name="inher_sickle_cell_ind")
	public String getInher_sickle_cell_ind() {
		return inher_sickle_cell_ind;
	}
	public void setInher_sickle_cell_ind(String inherSickleCellInd) {
		inher_sickle_cell_ind = inherSickleCellInd;
	}
	private List<Relations> Relations;
	@XmlElement(name="Relations")
	public List<Relations> getRelations() {
		return Relations;
	}
	public void setRelations(List<Relations> relations) {
		Relations = relations;
	}
	
}
