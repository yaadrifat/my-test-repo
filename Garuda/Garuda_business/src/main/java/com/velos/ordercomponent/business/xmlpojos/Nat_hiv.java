package com.velos.ordercomponent.business.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


public class Nat_hiv {

	private String nat_hiv_react_ind;
	private XMLGregorianCalendar nat_hiv_react_date;
	private String cms_cert_lab_ind;
	private String fda_lic_mfg_inst_ind;
	@XmlElement(name="nat_hiv_react_ind")	
	public String getNat_hiv_react_ind() {
		return nat_hiv_react_ind;
	}
	public void setNat_hiv_react_ind(String natHivReactInd) {
		nat_hiv_react_ind = natHivReactInd;
	}
	@XmlElement(name="nat_hiv_react_date")	
	@XmlJavaTypeAdapter(type=XMLGregorianCalendar.class,value=XMLGeorgarianCalanderAdapter.class)
	public XMLGregorianCalendar getNat_hiv_react_date() {
		return nat_hiv_react_date;
	}
	public void setNat_hiv_react_date(XMLGregorianCalendar natHivReactDate) {
		nat_hiv_react_date = natHivReactDate;
	}
	@XmlElement(name="cms_cert_lab_ind")	
	public String getCms_cert_lab_ind() {
		return cms_cert_lab_ind;
	}
	public void setCms_cert_lab_ind(String cmsCertLabInd) {
		cms_cert_lab_ind = cmsCertLabInd;
	}
	@XmlElement(name="fda_lic_mfg_inst_ind")	
	public String getFda_lic_mfg_inst_ind() {
		return fda_lic_mfg_inst_ind;
	}
	public void setFda_lic_mfg_inst_ind(String fdaLicMfgInstInd) {
		fda_lic_mfg_inst_ind = fdaLicMfgInstInd;
	}
	
	
	
}
