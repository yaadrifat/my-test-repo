package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
@Entity
@Table(name="UI_WIDGET")
@SequenceGenerator(sequenceName="SEQ_UI_WIDGET",name="SEQ_UI_WIDGET",allocationSize=1)
public class Widget {
	
	private Long id;
	private String widgetName;
	private String state;
	private String helpUrl;
	private String description;
	private Boolean isAuthenticated;
	private Boolean isMinimizable;
	private Boolean isClosable;
	private Boolean isResizable;
	private Boolean isDragable;
	private Boolean isMouseHoverEnabled;
	private Date ceatedDate;
	private Date lastUpdate;
	private Long versionNo;
	private String widgetDivId;
	private Long resizeMinHeight;
	private Long resizeMinWidth;
	private Long resizeMaxHeight;
	private Long resizeMaxWidth;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_UI_WIDGET")
	@Column(name="PK_WIDGET")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(name="NAME")	
	public String getWidgetName() {
		return widgetName;
	}
	public void setWidgetName(String widgetName) {
		this.widgetName = widgetName;
	}
	@Column(name="STATE")	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@Column(name="HELP_URL")	
	public String getHelpUrl() {
		return helpUrl;
	}
	public void setHelpUrl(String helpUrl) {
		this.helpUrl = helpUrl;
	}
	@Column(name="DESCRIPTION")	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Column(name="IS_AUTHENTICATED")	
	public Boolean getIsAuthenticated() {
		return isAuthenticated;
	}
	public void setIsAuthenticated(Boolean isAuthenticated) {
		this.isAuthenticated = isAuthenticated;
	}
	@Column(name="IS_MINIZABLE")	
	public Boolean getIsMinimizable() {
		return isMinimizable;
	}
	public void setIsMinimizable(Boolean isMinimizable) {
		this.isMinimizable = isMinimizable;
	}
	@Column(name="IS_CLOSABLE")	
	public Boolean getIsClosable() {
		return isClosable;
	}
	public void setIsClosable(Boolean isClosable) {
		this.isClosable = isClosable;
	}
	@Column(name="IS_RESIZEABLE")	
	public Boolean getIsResizable() {
		return isResizable;
	}
	public void setIsResizable(Boolean isResizable) {
		this.isResizable = isResizable;
	}
	@Column(name="IS_DRAGABLE")	
	public Boolean getIsDragable() {
		return isDragable;
	}
	public void setIsDragable(Boolean isDragable) {
		this.isDragable = isDragable;
	}
	@Column(name="IS_MOUSE_HOVER_ENABLED")	
	public Boolean getIsMouseHoverEnabled() {
		return isMouseHoverEnabled;
	}
	public void setIsMouseHoverEnabled(Boolean isMouseHoverEnabled) {
		this.isMouseHoverEnabled = isMouseHoverEnabled;
	}
	@Column(name="CREATED_ON", updatable=false)	
	public Date getCeatedDate() {
		return ceatedDate;
	}
	public void setCeatedDate(Date ceatedDate) {
		this.ceatedDate = ceatedDate;
	}
	@Column(name="LAST_MODIFIED_DATE" ,insertable=false)	
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	@Column(name="VERSION_NO")	
	public Long getVersionNo() {
		return versionNo;
	}
	public void setVersionNo(Long versionNo) {
		this.versionNo = versionNo;
	}
	@Column(name="WIDGET_DIV_ID")	
	public String getWidgetDivId() {
		return widgetDivId;
	}
	public void setWidgetDivId(String widgetDivId) {
		this.widgetDivId = widgetDivId;
	}
	@Column(name="RESIZE_MIN_HEIGHT")	
	public Long getResizeMinHeight() {
		return resizeMinHeight;
	}
	public void setResizeMinHeight(Long resizeMinHeight) {
		this.resizeMinHeight = resizeMinHeight;
	}
	@Column(name="RESIZE_MIN_WIDTH")	
	public Long getResizeMinWidth() {
		return resizeMinWidth;
	}
	public void setResizeMinWidth(Long resizeMinWidth) {
		this.resizeMinWidth = resizeMinWidth;
	}
	@Column(name="RESIZE_MAX_HEIGHT")	
	public Long getResizeMaxHeight() {
		return resizeMaxHeight;
	}
	public void setResizeMaxHeight(Long resizeMaxHeight) {
		this.resizeMaxHeight = resizeMaxHeight;
	}
	@Column(name="RESIZE_MAX_WIDTH")	
	public Long getResizeMaxWidth() {
		return resizeMaxWidth;
	}
	public void setResizeMaxWidth(Long resizeMaxWidth) {
		this.resizeMaxWidth = resizeMaxWidth;
	}
	
	

}
