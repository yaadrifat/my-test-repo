package com.velos.ordercomponent.business.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="CB_PACKING_SLIP")
@SequenceGenerator(sequenceName="SEQ_CB_PACKING_SLIP",name="SEQ_CB_PACKING_SLIP",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class PackingSlip extends Auditable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long pkPackingSlip;
	private Long fkShipmentId;
	private Long fkOrderId;
	private Date schShipDate;
	private String shipTrackingNo;
	private Long fkShipCompanyId;
	private Long fkCbbId;
	private Long fkCordId;
	private Long fkSampleType;
	private Long fkAliquotType;
	private Long fkHlaId;
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_PACKING_SLIP")
	@Column(name="PK_PACKING_SLIP")
	public Long getPkPackingSlip() {
		return pkPackingSlip;
	}
	public void setPkPackingSlip(Long pkPackingSlip) {
		this.pkPackingSlip = pkPackingSlip;
	}
	
	
	@Column(name="FK_SHIPMENT")
	public Long getFkShipmentId() {
		return fkShipmentId;
	}
	public void setFkShipmentId(Long fkShipmentId) {
		this.fkShipmentId = fkShipmentId;
	}
	
	@Column(name="FK_ORDER_ID")
	public Long getFkOrderId() {
		return fkOrderId;
	}
	public void setFkOrderId(Long fkOrderId) {
		this.fkOrderId = fkOrderId;
	}
	
	@Column(name="SCH_SHIPMENT_DATE")
	public Date getSchShipDate() {
		return schShipDate;
	}
	public void setSchShipDate(Date schShipDate) {
		this.schShipDate = schShipDate;
	}
	
	@Column(name="SHIPMENT_TRACKING_NO")
	public String getShipTrackingNo() {
		return shipTrackingNo;
	}
	public void setShipTrackingNo(String shipTrackingNo) {
		this.shipTrackingNo = shipTrackingNo;
	}
	
	@Column(name="FK_SHIP_COMPANY_ID")
	public Long getFkShipCompanyId() {
		return fkShipCompanyId;
	}
	public void setFkShipCompanyId(Long fkShipCompanyId) {
		this.fkShipCompanyId = fkShipCompanyId;
	}
	
	@Column(name="CBB_ID")
	public Long getFkCbbId() {
		return fkCbbId;
	}
	public void setFkCbbId(Long fkCbbId) {
		this.fkCbbId = fkCbbId;
	}
	
	@Column(name="FK_CORD_ID")
	public Long getFkCordId() {
		return fkCordId;
	}
	public void setFkCordId(Long fkCordId) {
		this.fkCordId = fkCordId;
	}
	
	@Column(name="FK_SAMPLE_TYPE_AVAIL")
	public Long getFkSampleType() {
		return fkSampleType;
	}
	public void setFkSampleType(Long fkSampleType) {
		this.fkSampleType = fkSampleType;
	}
	
	@Column(name="FK_ALIQUOTS_TYPE")
	public Long getFkAliquotType() {
		return fkAliquotType;
	}
	public void setFkAliquotType(Long fkAliquotType) {
		this.fkAliquotType = fkAliquotType;
	}
	
	@Column(name="PK_HLA")
	public Long getFkHlaId() {
		return fkHlaId;
	}
	public void setFkHlaId(Long fkHlaId) {
		this.fkHlaId = fkHlaId;
	}
	
	
	
}
