package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

import com.velos.ordercomponent.business.domain.Widget;

public class WidgetInstancePojo {
	private Long id;
	private Long pageId;
	private Long containerId;
	private String widgetId;
	private Long pageCellIndex;
	private Boolean isExpanded;
	private Boolean isClosed;
	private String title;
	private String state;
	private Long height;
	private Long width;
	private String theme;
	private Date createdDate;
	private Date lastUpdate;
	private Long versionNo;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getPageId() {
		return pageId;
	}
	public void setPageId(Long pageId) {
		this.pageId = pageId;
	}
	public Long getContainerId() {
		return containerId;
	}
	public void setContainerId(Long containerId) {
		this.containerId = containerId;
	}
	public String getWidgetId() {
		return widgetId;
	}
	public void setWidgetId(String widgetId) {
		this.widgetId = widgetId;
	}
	public Long getPageCellIndex() {
		return pageCellIndex;
	}
	public void setPageCellIndex(Long pageCellIndex) {
		this.pageCellIndex = pageCellIndex;
	}
	public Boolean getIsExpanded() {
		return isExpanded;
	}
	public void setIsExpanded(Boolean isExpanded) {
		this.isExpanded = isExpanded;
	}
	public Boolean getIsClosed() {
		return isClosed;
	}
	public void setIsClosed(Boolean isClosed) {
		this.isClosed = isClosed;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Long getHeight() {
		return height;
	}
	public void setHeight(Long height) {
		this.height = height;
	}
	public Long getWidth() {
		return width;
	}
	public void setWidth(Long width) {
		this.width = width;
	}
	public String getTheme() {
		return theme;
	}
	public void setTheme(String theme) {
		this.theme = theme;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public Long getVersionNo() {
		return versionNo;
	}
	public void setVersionNo(Long versionNo) {
		this.versionNo = versionNo;
	}
	
	
	
}
