package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

import com.velos.ordercomponent.business.domain.CdrCbu;

public class CBUFinalReviewPojo extends AuditablePojo{

	private Long pkCordFinalReview;
	private Long fkCordId;
	//commited without order
	//private Long fkOrderId;
	private Long licensureConfirmFlag;
	private Long eligibleConfirmFlag;
	private Boolean deletedFlag;
	private CdrCbu cdrCbu;
	private Long reviewCordAcceptance;
	private String finalReviewConfirmStatus;
	private Date licCreatedOn;
	private Long LicCreatedBy;
	private Date eligCreatedOn;
	private Long eligCreatedBy;
	private Date finalReviewCreatedOn;
	private Long finalReviewCreatedBy;
	private Long fkFinalEligQuesId;
	private Long confirmCBBSignatureFlag;
	private Date confirmCBBSignCreatedOn;
	private Long confirmCBBSignCreator;
	private String finalEligibleCreator;
	
	public String getFinalEligibleCreator() {
		return finalEligibleCreator;
	}
	public void setFinalEligibleCreator(String finalEligibleCreator) {
		this.finalEligibleCreator = finalEligibleCreator;
	}
	public Long getPkCordFinalReview() {
		return pkCordFinalReview;
	}
	public void setPkCordFinalReview(Long pkCordFinalReview) {
		this.pkCordFinalReview = pkCordFinalReview;
	}
	public Long getFkCordId() {
		return fkCordId;
	}
	public void setFkCordId(Long fkCordId) {
		this.fkCordId = fkCordId;
	}
	public Long getLicensureConfirmFlag() {
		return licensureConfirmFlag;
	}
	public void setLicensureConfirmFlag(Long licensureConfirmFlag) {
		this.licensureConfirmFlag = licensureConfirmFlag;
	}
	public Long getEligibleConfirmFlag() {
		return eligibleConfirmFlag;
	}
	public void setEligibleConfirmFlag(Long eligibleConfirmFlag) {
		this.eligibleConfirmFlag = eligibleConfirmFlag;
	}
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	
	public CdrCbu getCdrCbu() {
		return cdrCbu;
	}
	public void setCdrCbu(CdrCbu cdrCbu) {
		this.cdrCbu = cdrCbu;
	}
	public Long getReviewCordAcceptance() {
		return reviewCordAcceptance;
	}
	public void setReviewCordAcceptance(Long reviewCordAcceptance) {
		this.reviewCordAcceptance = reviewCordAcceptance;
	}
	public String getFinalReviewConfirmStatus() {
		return finalReviewConfirmStatus;
	}
	public void setFinalReviewConfirmStatus(String finalReviewConfirmStatus) {
		this.finalReviewConfirmStatus = finalReviewConfirmStatus;
	}
	//commited without order
	/*
	public Long getFkOrderId() {
		return fkOrderId;
	}
	public void setFkOrderId(Long fkOrderId) {
		this.fkOrderId = fkOrderId;
	}
	*/
	public Date getLicCreatedOn() {
		return licCreatedOn;
	}
	public void setLicCreatedOn(Date licCreatedOn) {
		this.licCreatedOn = licCreatedOn;
	}
	public Long getLicCreatedBy() {
		return LicCreatedBy;
	}
	public void setLicCreatedBy(Long licCreatedBy) {
		LicCreatedBy = licCreatedBy;
	}
	public Date getEligCreatedOn() {
		return eligCreatedOn;
	}
	public void setEligCreatedOn(Date eligCreatedOn) {
		this.eligCreatedOn = eligCreatedOn;
	}
	public Long getEligCreatedBy() {
		return eligCreatedBy;
	}
	public void setEligCreatedBy(Long eligCreatedBy) {
		this.eligCreatedBy = eligCreatedBy;
	}
	public Date getFinalReviewCreatedOn() {
		return finalReviewCreatedOn;
	}
	public void setFinalReviewCreatedOn(Date finalReviewCreatedOn) {
		this.finalReviewCreatedOn = finalReviewCreatedOn;
	}
	public Long getFinalReviewCreatedBy() {
		return finalReviewCreatedBy;
	}
	public void setFinalReviewCreatedBy(Long finalReviewCreatedBy) {
		this.finalReviewCreatedBy = finalReviewCreatedBy;
	}
	public Long getFkFinalEligQuesId() {
		return fkFinalEligQuesId;
	}
	public void setFkFinalEligQuesId(Long fkFinalEligQuesId) {
		this.fkFinalEligQuesId = fkFinalEligQuesId;
	}
	
	public Long getConfirmCBBSignatureFlag() {
		return confirmCBBSignatureFlag;
	}
	public void setConfirmCBBSignatureFlag(Long confirmCBBSignatureFlag) {
		this.confirmCBBSignatureFlag = confirmCBBSignatureFlag;
	}
	public Date getConfirmCBBSignCreatedOn() {
		return confirmCBBSignCreatedOn;
	}
	public void setConfirmCBBSignCreatedOn(Date confirmCBBSignCreatedOn) {
		this.confirmCBBSignCreatedOn = confirmCBBSignCreatedOn;
	}
	public Long getConfirmCBBSignCreator() {
		return confirmCBBSignCreator;
	}
	public void setConfirmCBBSignCreator(Long confirmCBBSignCreator) {
		this.confirmCBBSignCreator = confirmCBBSignCreator;
	}
	
}
