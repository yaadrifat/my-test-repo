/**
 * 
 */
package com.velos.ordercomponent.business.util;

/**
 * @author akajeera
 *
 */
public class VelosMidConstants {
	
	public static final String SECUSER_OBJECTNAME = "SecUser";
	public static final String STUDY_OBJECTNAME ="Study";
	public static final String STUDY_ID = "studyId";
	public static final String STUDY_DOMAINOBJECT = "com.velos.ordercomponent.business.domain.Study";
	public static final String MAILCONFIG_OBJECTNAME = "MailConfig";
	
	
	//for volunteer
	public static final String PERSON_OBJECTNAME ="Person";
	public static final String PERSON_ID ="personid";
	
	public static final String DONOT_OBJECTNAME ="DonotContact";
	public static final String DONOT_ID ="donotContactId";
	
	public static final String GUARDIAN_OBJECTNAME ="Guardian";
	public static final String GUARDIAN_ID ="guardianId";
	public static final String GUARDIAN_PERSONID ="guardPersonId";
	public static final String VOLUNTEER_OBJECTNAME ="Person";
	
	public static final String MEDICALCONDN_OBJECTNAME ="MedicalCondition";
	public static final String MEDICALCONDN_ID ="medicalCondnId";
	
	public static final String ADDRESS_OBJECTNAME ="Address";
	public static final String ADDRESS_ID ="addressId";
	public static final String RESOLUTION = "Resolution";
	public static final String CLOSE_REASON = "CloseReason";
	
	//end of volunteer
	
	//Constants
	public static final String OUTCOME_PARENTCODE = "PRESCREENOUTCOME";
	public static final String STUDYSTATUS_PARENTCODE = "STUDYSTATUS";
	public static final String RECRUITSTATUS_PARENTCODE = "RECSTATUS";
	public static final String IRBRECORD_PARENTCODE = "IRBRECORD";
	public static final String DUKE_CODE = "DUKE";
	public static final String RECRUITSTATUS_CODE = "REC";
	public static final String ACTIVESTATUS_CODE = "'active'";
	public static final String FORGOTPASSWORD_CODE = "FGTPASSWD";
	public static final String PASSED_CODE = "PASSED";
	
	
	//Error Message
	public static final String STUDY_UNIQUE = "Study Number should be Unique";
	public static final String ERROR_UNIQUE = "Unique";
	
	public static final String EXCEPTION_PROPERTYFILE_NAME="ExceptionMessages";
	
	public static final String PK_CBU_UNIT_REPORT = "PK_CBU_UNIT_REPORT";
	public static final String PK_CBU_UNIT_REPORT_HLA ="PK_CBU_UNIT_REPORT_HLA";
    //order attachment helper
	public static final String ORDER_DATE="ORDER_DATE";
	
	//for PendingOrders
	public static final String ORDER_TYPE_NEW="order_type";
	public static final String ALL_ORDERS="ALL";
	public static final String ORDER_STATUS="order_status";
	public static final String TASK_STATUS="task_status";
	public static final String CLOSED_REASON="closed_reason";
	public static final String CANCELLED="CANCELLED";
	public static final String CLOSED="close_ordr";
	public static final String SHIPMENT_TYPE="shipment_type";
	public static final String YES_FLAG="Yes";
	public static final String NO_FLAG="No";
	public static final String ALLOW_FLAG="Allow";
	public static final String DONT_ALLOW_FLAG="Dont_allow";

	public static final String CBU="CBU";
	public static final String NMDP="NMDP";
	
	//Mail Attachment
	public static final String FORM_TYPE = "form";
	public static final String FILE_TYPE = "file";
	public static final String RESOLVED="resolved";
	public static final String NEWORDER="New";
	public static final String NEW="NEW";
	
	
	public static final String CORD_NMDP_STTS ="Cord_Nmdp_Status";
	public static final String MATERNAL_CODE_TYPE ="test_source";
	public static final String MATERNAL_CODE_SUB_TYPE ="maternal";
	public static final String FORMS_CODE ="FORMS";
	public static final String CORD_STATUS="cord_status";	
	public static final String ENTITY_CBU_CODE_TYPE = "entity_type";
	public static final String ENTITY_CBU_CODE_SUB_TYPE = "CBU";
	public static final String ENTITY_ORDER_CODE_SUB_TYPE = "ORDER";
	public static final String ENTITY_ORDHDR_CODE_SUB_TYPE = "ORDER_HEADER";
	public static final String CT_ORDER = "CT";
	public static final String CT_SHIP_ORDER = "CT - Ship Sample";
	public static final String CT_LAB_ORDER = "CT - Sample at Lab";
	public static final String FLAG_Y = "Y";
	public static final String FLAG_N = "N";
	public static final String RESOLUTION_TC ="resolution_tc";
	public static final String LICENSE = "licence";
	public static final String ELIGIBLE = "eligibility";
	public static final String SWITCH ="switch_to";
	public static final String CBB_TYPE ="site_type";
	public static final String CBB_SUBTYPE ="cbb";
	public static final String FILTER_MODULE ="filter_module";
	public static final String PENDING_SCREEN ="pending_screen";
	public static final String LANDING_PAGE ="landing_page";
	public static final String FILTER_TYPE ="filter_type";
	public static final String CUSTOM_FILTER ="custom_filter";
	public static final String ASSIGNTO_ACCESS_RIGHT="CB_PFPENDINGW";
	public static final String ALERT_CODELST_TYPE="alert_type";
	public static final String NOTIFY_CODELST_SUBTYPE="notify";
	public static final String REQ_CLINC_INFO="reqclincinfobar";
	public static final String FINAL_CBU_REVIEW="finalcbureviewbar";
	
	
	public static final String HISTORY_ORDER_TASK="task";
	public static final String HISTORY_ORDER_TASK_COMPLETED="completed";
	public static final String HISTORY_ORDER_TASK_RESET="reset";
	public static final String HISTORY_USER_SYSTEM="System";
	public static final String HISTORY_CBU_REASON="Reason";
	public static final String HISTORY_CBU_STATUS="CBU Status";
	public static final String HISTORY_CBU_ELIGIBILITY="Eligibility";
	public static final String HISTORY_CBU_LICENSE="License Status";
	public static final String HISTORY_ENTERED_AS="entered as";
	public static final String HISTORY_CHANGED_AS="changed to";
	public static final String HISTORY_FORM="form";
	public static final String HISTORY_FORM_ENTERED="entered";
	public static final String HISTORY_FORM_UPDATED="updated";
	
	
	public static final String RESPONSE = "Response";
	public static final String DEFRED_CORD = "DEFFERED";
	public static final String NATIONAL_STATUS = "NATIONAL_STATUS";
	public static final String TASK_RESET="task_reset";
	public static final String RO = "RO";
	public static final String AV = "AV";
	public static final String AG = "AG";
	public static final String CA = "CA";
	public static final String DT = "DT";
	public static final String NR = "NR";
	public static final String CT = "CT";
	public static final String HE = "HE";
	public static final String OR = "OR";
	public static final String SA = "SA";
	public static final String SB = "SB";
	public static final String SC = "SC";
	public static final String SD = "SD";
	public static final String SE = "SE";
	public static final String NA = "NA";
	public static final String AC = "AC";
	public static final String DD = "DD";
	public static final String QR = "QR";
	public static final String CD = "CD";
	public static final String OT = "OT";
	public static final String SO = "SO";	
}
