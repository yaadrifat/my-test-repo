package com.velos.ordercomponent.business.pojoobjects;

import java.util.Date;

public class FilterDataPojo {
	public long pk_filter;
	public String filter_name;
	public String filter_paraameter;
	public long fk_user;
	public long created_by;
	public Date created_on;
	public long fk_module;
	public long fk_filter_type;
	public long fk_sub_module;
	/*public String selected_param;*/
	
	

	/*public String getSelected_param() {
		return selected_param;
	}
	public void setSelected_param(String selected_param) {
		this.selected_param = selected_param;
	}*/
	public long getPk_filter() {
		return pk_filter;
	}
	public String getFilter_name() {
		return filter_name;
	}
	public String getFilter_paraameter() {
		return filter_paraameter;
	}
	public long getFk_user() {
		return fk_user;
	}
	public long getCreated_by() {
		return created_by;
	}
	public Date getCreated_on() {
		return created_on;
	}
	
	public long getFk_module() {
		return fk_module;
	}
	public long getFk_filter_type() {
		return fk_filter_type;
	}
	public void setFk_module(long fk_module) {
		this.fk_module = fk_module;
	}
	public void setFk_filter_type(long fk_filter_type) {
		this.fk_filter_type = fk_filter_type;
	}
	public void setPk_filter(long pk_filter) {
		this.pk_filter = pk_filter;
	}
	public void setFilter_name(String filter_name) {
		this.filter_name = filter_name;
	}
	public void setFilter_paraameter(String filter_paraameter) {
		this.filter_paraameter = filter_paraameter;
	}
	public void setFk_user(long fk_user) {
		this.fk_user = fk_user;
	}
	public void setCreated_by(long created_by) {
		this.created_by = created_by;
	}
	public void setCreated_on(Date created_on) {
		this.created_on = created_on;
	}
	public long getFk_sub_module() {
		return fk_sub_module;
	}
	public void setFk_sub_module(long fk_sub_module) {
		this.fk_sub_module = fk_sub_module;
	}
	
	
	

}
