package com.velos.ordercomponent.helper;


import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.classic.Session;

import com.velos.ordercomponent.business.domain.DataModificationRequest;
import com.velos.ordercomponent.business.pojoobjects.DataModificationRequestPojo;
import com.velos.ordercomponent.business.pojoobjects.PaginateSearch;
import com.velos.ordercomponent.business.util.HibernateUtil;
import com.velos.ordercomponent.business.util.ObjectTransfer;



public class DataModificationRequestHelper {
	
	public static final Log log = LogFactory.getLog(DataModificationRequestHelper.class);
		public static void createDataRequest(DataModificationRequestPojo datarequestpojo){
			createDataRequest(datarequestpojo,null);
		}
		public static void createDataRequest(DataModificationRequestPojo datarequestpojo,Session session){
			
			if(session==null){
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			try{
				DataModificationRequest datarequest=new DataModificationRequest();
				datarequest=(DataModificationRequest) ObjectTransfer.transferObjects(datarequestpojo, datarequest);
				session.save(datarequest);
				session.getTransaction().commit();	
			}catch (Exception e) {
				log.error(e.getMessage());
				e.printStackTrace();
			}finally{
				if(session.isOpen()){
					session.close();
				}
			}
			
		}
		public static List<DataModificationRequestPojo> viewDataRequest(Long requestType,PaginateSearch paginateSearch,int i){
			return viewDataRequest(requestType,paginateSearch,i,null);
		}
		@SuppressWarnings("unchecked")
		public static List<DataModificationRequestPojo> viewDataRequest(Long requestType,PaginateSearch paginateSearch,int i,Session session){
			List<DataModificationRequestPojo> dataReqList = null;
			if(session==null){
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			try{
				String sql = "select datareq.pk_data_modi_request,f_codelst_desc(datareq.request_type),to_char(datareq.created_on,'Mon DD, YYYY'),datareq.fk_siteid,"+
							 "datareq.creator,to_char(datareq.request_due_date,'Mon DD, YYYY'),datareq.request_title,f_codelst_desc(datareq.request_approval_status) from"+ 
							 " cb_data_modi_request datareq where datareq.REQUEST_TYPE="+requestType;
				Query query = session.createSQLQuery(sql);
				if(paginateSearch!=null && i==1){
					query.setFirstResult(paginateSearch.getiPageNo());
					query.setMaxResults(paginateSearch.getiShowRows());
				}
				dataReqList = query.list();
				
			}catch (Exception e) {
				log.error(e.getMessage());
				e.printStackTrace();
			}finally{
				if(session.isOpen()){
					session.close();
				}
			}
			return dataReqList;
		}
		public static DataModificationRequestPojo getRequestDetails(DataModificationRequestPojo datarequestpojo){
			return getRequestDetails(datarequestpojo,null);
		}
		public static DataModificationRequestPojo getRequestDetails(DataModificationRequestPojo datarequestpojo,Session session){
			DataModificationRequest dataModrequest = new DataModificationRequest();
			if(session==null){
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			try{
				dataModrequest = (DataModificationRequest)session.get(DataModificationRequest.class, datarequestpojo.getPkdatarequest());
				datarequestpojo = (DataModificationRequestPojo)ObjectTransfer.transferObjects(dataModrequest, datarequestpojo);
			}catch (Exception e) {
				log.error(e.getMessage());
				e.printStackTrace();
			}finally{
				if(session.isOpen()){
					session.close();
				}
			}
			
			return datarequestpojo;
		}
		public static void updateDMRequestStatus(DataModificationRequestPojo datarequestpojo){
			updateDMRequestStatus(datarequestpojo,null);
		}
		public static void updateDMRequestStatus(DataModificationRequestPojo datarequestpojo,Session session){
			DataModificationRequest dataModrequest = new DataModificationRequest();
			if(session==null){
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
			}
			try{
				dataModrequest = (DataModificationRequest)session.get(DataModificationRequest.class, datarequestpojo.getPkdatarequest());
				dataModrequest.setRequestStatus(datarequestpojo.getRequestStatus());
				session.saveOrUpdate(dataModrequest);
				session.getTransaction().commit();
			}catch (Exception e) {
				log.error(e.getMessage());
				e.printStackTrace();
			}finally{
				if(session.isOpen()){
					session.close();
				}
			}
			
		}
		
		
}
