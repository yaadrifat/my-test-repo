/**
 * 
 */
package com.velos.ordercomponent.business.pojoobjects;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author jidrushbasha
 *
 */
public class OrderServicesPojo extends AuditablePojo{
	
	private Long pkOrderServices;
	private Long fkOrderId;
	private String serviceCode;
	private Date requestDate;
	private String resRecFlag;
	private Date resRecDate;
	private String cancelledFlag;
	private Date cancelledDate;
	private Long serviceStatus;
	private String requestDateStr;
	private String resRecDateStr;
	private String cancelledDateStr;
	DateFormat dat = new SimpleDateFormat("MMM dd,yyyy");
	
	public Long getPkOrderServices() {
		return pkOrderServices;
	}
	public void setPkOrderServices(Long pkOrderServices) {
		this.pkOrderServices = pkOrderServices;
	}
	public Long getFkOrderId() {
		return fkOrderId;
	}
	public void setFkOrderId(Long fkOrderId) {
		this.fkOrderId = fkOrderId;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public Date getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	public String getResRecFlag() {
		return resRecFlag;
	}
	public void setResRecFlag(String resRecFlag) {
		this.resRecFlag = resRecFlag;
	}
	public Date getResRecDate() {
		return resRecDate;
	}
	public void setResRecDate(Date resRecDate) {
		this.resRecDate = resRecDate;
	}
	public String getCancelledFlag() {
		return cancelledFlag;
	}
	public void setCancelledFlag(String cancelledFlag) {
		this.cancelledFlag = cancelledFlag;
	}
	public Date getCancelledDate() {
		return cancelledDate;
	}
	public void setCancelledDate(Date cancelledDate) {
		this.cancelledDate = cancelledDate;
	}
	public Long getServiceStatus() {
		return serviceStatus;
	}
	public void setServiceStatus(Long serviceStatus) {
		this.serviceStatus = serviceStatus;
	}
	public String getRequestDateStr() {
		return requestDateStr;
	}
	public void setRequestDateStr(String requestDateStr) {
		this.requestDateStr = requestDateStr;
	}
	public String getResRecDateStr() {
		return resRecDateStr;
	}
	public void setResRecDateStr(String resRecDateStr) {
		this.resRecDateStr = resRecDateStr;
	}
	public String getCancelledDateStr() {
		return cancelledDateStr;
	}
	public void setCancelledDateStr(String cancelledDateStr) {
		this.cancelledDateStr = cancelledDateStr;
	}
	
	
	
	

}
