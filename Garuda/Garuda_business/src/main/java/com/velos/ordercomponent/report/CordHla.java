package com.velos.ordercomponent.report;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.jdbc.Work;

import com.velos.ordercomponent.business.pojoobjects.CordFilterPojo;

public class CordHla implements Work{
	public static final Log log = LogFactory.getLog(CordHla.class);
	
	private String query;
	private List<CordFilterPojo> cord;
	private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	
	public CordHla(String query) {
		this.query = query;
	}
	 @Override
	    public void execute( Connection connection ) throws SQLException
	    {
	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        try
	        {
	            ps = connection.prepareStatement( query);
	            ps.setFetchSize(50000);
	            rs = ps.executeQuery();
	            System.out.println("Query Result Returned");
	            long startTime=System.currentTimeMillis();
	            
		           
	            cord = prepCordObj(rs);
		           
	            
		            
		            
	             long endTime=System.currentTimeMillis();
		         System.out.println();
		         System.out.println((endTime-startTime)/1000+" : "+(endTime-startTime)%1000+" MM:SS\n\n");
	        }
	        
	        
	        catch( SQLException e ) { 
	        	
	        	printException(e);
	        } catch (ParseException e) {
	        	
	        	printException(e);
			}
	        finally  {
	            if( rs != null )  
	            {
	                try {
	                    rs.close();
	                }
	                catch( Exception e ) {printException(e);}
	            }
	            if( ps != null ) 
	            {
	                try {
	                    ps.close();
	                }
	                catch( Exception e ) {printException(e);}
	            }
	        }
	    }
	
	 private List<CordFilterPojo> prepCordObj(ResultSet rs) throws SQLException, ParseException{
		 
		 List<CordFilterPojo> cords = new ArrayList<CordFilterPojo>();
		 CordFilterPojo cfp = null;
		 
		  while( rs.next())  
          {
			  cfp = new CordFilterPojo();
			  
			  cfp.cord =  rs.getLong(1);
			  cfp.site =  rs.getString(2);
			  cfp.registry =  rs.getString(3);
			  cfp.localCbu =  rs.getString(4);
			  cfp.numberOnBag =  rs.getString(5);
			  cfp.isbtCode =  rs.getString(6);
			  cfp.historic =  rs.getString(7);
			  cfp.maternal =  rs.getString(8);
			  cfp.localMat =  rs.getString(9);
			  cfp.patient =  rs.getString(10);
			  cfp.collectDate =  rs.getDate(11)!=null? format.parse(rs.getDate(11).toString()):null;
			  cfp.nmdpStatus =  rs.getLong(12);
          	
			  cords.add(cfp);
          }
		 return cords;
	 }
	private void printException(Exception e){
		
		log.error(e.getMessage());
		e.printStackTrace();
		
	}
	public List<CordFilterPojo> getCord() {
		return cord;
	}
	public void setCord(List<CordFilterPojo> cord) {
		this.cord = cord;
	}
	
	

}
