package com.velos.ordercomponent.helper;

import java.util.List;

public class DataEntryHelper {
	
	public static List<Object>  getdataentryCbuAttachInfo(String cbuid){
		String appendQuery="";
	
		if(cbuid != null && !cbuid.equals("")){
			appendQuery="select cordinfo.cdrCbuId,cordinfo.externalCbuId,cordinfo.registryId,cordinfo.localCbuId," +
			"att.attachmentId,att.documentType,att.documentFile,att.fileName,extemp.pkCordExtInfo,hlaext.pkCordHlaExt,att.dcmsAttachmentId from Attachment att, CBUUnitReport extemp, CdrCbu cordinfo, CordHlaExt hlaext  " +
			"where att.attachmentId = extemp.attachmentId and extemp.fkCordCdrCbuId = cordinfo.cordID and hlaext.fkCordCdrCbuId = cordinfo.cordID and lower(cordinfo.cdrCbuId) like lower('%"+ cbuid + "%')" ;
		}
	
		else
			appendQuery="select cordinfo.cdrCbuId,cordinfo.externalCbuId,cordinfo.registryId,cordinfo.localCbuId," +
			"att.attachmentId,att.documentType,att.documentFile,att.fileName,extemp.pkCordExtInfo,hlaext.pkCordHlaExt,att.dcmsAttachmentId from Attachment att, CBUUnitReport extemp, CdrCbu cordinfo, CordHlaExt hlaext  " +
			"where att.attachmentId = extemp.attachmentId and extemp.fkCordCdrCbuId = cordinfo.cordID and hlaext.fkCordCdrCbuId = cordinfo.cordID";
			
	
	
		return velosHelper.getObjectList(appendQuery);

	}
	
}
