package com.velos.ordercomponent.business.pojoobjects;

import com.velos.ordercomponent.business.domain.Address;
import com.velos.ordercomponent.business.domain.CBB;

/**
 * @author Mohiuddin Ali Ahmed
 *
 */
public class SitePojo extends AuditablePojo {

	   private Long siteId;
	   private Long siteCodelstType;
	   private Long siteAccountId;
	   private Long sitePerAdd;
	   private String siteName;
	   private String siteInfo;
	   private String siteNotes;
	   private String siteStatus;
	   private Long siteParent;
	   private String siteIdentifier;
	   private Long siteSequence;    
	   private String siteHidden; 
	  
	   private String siteAltId;
	   private String siteRestrict;
	   private CBBPojo cbb;
	   private AddressPojo address;	 
	   
	public Long getSiteId() {
		return siteId;
	}
	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}
	public Long getSiteCodelstType() {
		return siteCodelstType;
	}
	public void setSiteCodelstType(Long siteCodelstType) {
		this.siteCodelstType = siteCodelstType;
	}
	public Long getSiteAccountId() {
		return siteAccountId;
	}
	public void setSiteAccountId(Long siteAccountId) {
		this.siteAccountId = siteAccountId;
	}
	public Long getSitePerAdd() {
		return sitePerAdd;
	}
	public void setSitePerAdd(Long sitePerAdd) {
		this.sitePerAdd = sitePerAdd;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public String getSiteInfo() {
		return siteInfo;
	}
	public void setSiteInfo(String siteInfo) {
		this.siteInfo = siteInfo;
	}
	public String getSiteNotes() {
		return siteNotes;
	}
	public void setSiteNotes(String siteNotes) {
		this.siteNotes = siteNotes;
	}
	public String getSiteStatus() {
		return siteStatus;
	}
	public void setSiteStatus(String siteStatus) {
		this.siteStatus = siteStatus;
	}
	public Long getSiteParent() {
		return siteParent;
	}
	public void setSiteParent(Long siteParent) {
		this.siteParent = siteParent;
	}
	public String getSiteIdentifier() {
		return siteIdentifier;
	}
	public void setSiteIdentifier(String siteIdentifier) {
		this.siteIdentifier = siteIdentifier;
	}
	public Long getSiteSequence() {
		return siteSequence;
	}
	public void setSiteSequence(Long siteSequence) {
		this.siteSequence = siteSequence;
	}
	public String getSiteHidden() {
		return siteHidden;
	}
	public void setSiteHidden(String siteHidden) {
		this.siteHidden = siteHidden;
	}
	
	public String getSiteAltId() {
		return siteAltId;
	}
	public void setSiteAltId(String siteAltId) {
		this.siteAltId = siteAltId;
	}
	public String getSiteRestrict() {
		return siteRestrict;
	}
	public void setSiteRestrict(String siteRestrict) {
		this.siteRestrict = siteRestrict;
	}
	public CBBPojo getCbb() {
		return cbb;
	}
	public void setCbb(CBBPojo cbb) {
		this.cbb = cbb;
	}
	public AddressPojo getAddress() {
		return address;
	}
	public void setAddress(AddressPojo address) {
		this.address = address;
	}
	
}
