package com.velos.ordercomponent.business.domain;

import java.sql.Blob;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
/**
 * @author Mohiuddin Ali Ahmed
 *
 */
@Entity
@Table(name="CB_CORD_IMPORT_HISTORY")
@SequenceGenerator(name="SEQ_CB_CORD_IMPORT_HISTORY",sequenceName="SEQ_CB_CORD_IMPORT_HISTORY",allocationSize=1)
public class CordImportHistory extends Auditable {

	private Long pkCordimportHistory;
	private String cordImportHistoryId;
	private Timestamp startTime;
	private Blob importFile;
	private String importFileName;
	private Timestamp endTime;
	private String importHistoryDesc;
	private UserDomain userDomain;
	private Long fkSiteId;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_CB_CORD_IMPORT_HISTORY")
	@Column(name="PK_CORD_IMPORT_HISTORY")
	public Long getPkCordimportHistory() {
		return pkCordimportHistory;
	}
	public void setPkCordimportHistory(Long pkCordimportHistory) {
		this.pkCordimportHistory = pkCordimportHistory;
	}
	@Column(name="IMPORT_HISTORY_ID")
	public String getCordImportHistoryId() {
		return cordImportHistoryId;
	}
	public void setCordImportHistoryId(String cordImportHistoryId) {
		this.cordImportHistoryId = cordImportHistoryId;
	}
	@Column(name="START_TIME",updatable=false)
	public Timestamp getStartTime() {
		return startTime;
	}
	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}
	@Column(name="END_TIME",insertable=false)
	public Timestamp getEndTime() {
		return endTime;
	}
	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}
	@Column(name="IMPORT_HISTORY_DESC")
	public String getImportHistoryDesc() {
		return importHistoryDesc;
	}
	public void setImportHistoryDesc(String importHistoryDesc) {
		this.importHistoryDesc = importHistoryDesc;
	}
	@Column(name="IMPORT_FILE")
	public Blob getImportFile() {
		return importFile;
	}
	public void setImportFile(Blob importFile) {
		this.importFile = importFile;
	}
	@Column(name="IMPORT_FILE_NAME")
	public String getImportFileName() {
		return importFileName;
	}
	public void setImportFileName(String importFileName) {
		this.importFileName = importFileName;
	}
	@Transient
	public UserDomain getUserDomain() {
		return userDomain;
	}
	public void setUserDomain(UserDomain userDomain) {
		this.userDomain = userDomain;
	}
	
	@Column(name="FK_SITE_ID")
	public Long getFkSiteId() {
		return fkSiteId;
	}
	public void setFkSiteId(Long fkSiteId) {
		this.fkSiteId = fkSiteId;
	}
	
	
}
