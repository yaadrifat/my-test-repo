package com.velos.ordercomponent.business.pojoobjects;

import java.io.File;
import java.sql.Blob;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Transient;

public class AttachmentPojo extends AuditablePojo {
	private Long attachmentId;
	private Long garudaEntityId;
	private Long garudaEntityType;
	private Long garudaAttachmentType;
	private String garudaAttachmentsTypeRem;
	private String documentType;
	private Blob documentFile;
	private File docFile;
	private String fileName;
	private Timestamp timestamp;
	private Long dcmsAttachmentId;
	private String actionFlag;
		
	public File getDocFile() {
		return docFile;
	}
	public void setDocFile(File docFile) {
		this.docFile = docFile;
	}
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public Blob getDocumentFile() {
		return documentFile;
	}
	public void setDocumentFile(Blob documentFile) {
		this.documentFile = documentFile;
	}
	public Long getAttachmentId() {
		return attachmentId;
	}
	public void setAttachmentId(Long attachmentId) {
		this.attachmentId = attachmentId;
	}
	public Long getGarudaEntityId() {
		return garudaEntityId;
	}
	public void setGarudaEntityId(Long garudaEntityId) {
		this.garudaEntityId = garudaEntityId;
	}
	public Long getGarudaEntityType() {
		return garudaEntityType;
	}
	public void setGarudaEntityType(Long garudaEntityType) {
		this.garudaEntityType = garudaEntityType;
	}
	public Long getGarudaAttachmentType() {
		return garudaAttachmentType;
	}
	public void setGarudaAttachmentType(Long garudaAttachmentType) {
		this.garudaAttachmentType = garudaAttachmentType;
	}
	public String getGarudaAttachmentsTypeRem() {
		return garudaAttachmentsTypeRem;
	}
	public void setGarudaAttachmentsTypeRem(String garudaAttachmentsTypeRem) {
		this.garudaAttachmentsTypeRem = garudaAttachmentsTypeRem;
	}
	
	private Boolean deletedFlag;
	
	public Boolean getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Timestamp getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}
	public Long getDcmsAttachmentId() {
		return dcmsAttachmentId;
	}
	public void setDcmsAttachmentId(Long dcmsAttachmentId) {
		this.dcmsAttachmentId = dcmsAttachmentId;
	}
	public String getActionFlag() {
		return actionFlag;
	}
	public void setActionFlag(String actionFlag) {
		this.actionFlag = actionFlag;
	}
	
	
	
	
	
}
