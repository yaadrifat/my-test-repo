package com.velos.ordercomponent.business.xmlpojos;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class Races {
    private List<String> race;

    @XmlElement(name="race")
	public List<String> getRace() {
		return race;
	}
	
	public void setRace(List<String> race) {
		this.race = race;
	}
   
}
