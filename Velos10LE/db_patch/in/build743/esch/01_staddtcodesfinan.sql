set define off;

create or replace 
FUNCTION  "LST_ADDITIONALCODES_FINANCIAL" (event number,p_additional_code_type varchar2, accessYN number) return varchar is
  addl_lst varchar2(32000);
  TYPE tab_columns IS TABLE OF varchar2(4000);
  tab_rows tab_columns;
begin
  case (accessYN)
  when 0 then
   select trim(' &bull; ' || (codelst_desc) || ' - ' || md_modelementdata || '<br>') event_addlcode bulk collect into tab_rows 
   from  er_moredetails, er_codelst  where pk_codelst = md_modelementpk  and fk_modpk = event and md_modname = p_additional_code_type
    and CODELST_TYPE = p_additional_code_type and (codelst_kind is null or codelst_kind <> 'financial')
    and trim(md_modelementdata) is not null  order by codelst_seq;
  when -1 then
  select trim(' &bull; ' || (codelst_desc) || ' - ' || md_modelementdata || '<br>') event_addlcode bulk collect into tab_rows 
   from  er_moredetails, er_codelst where pk_codelst = md_modelementpk and fk_modpk = event and md_modname = p_additional_code_type
    and CODELST_TYPE = p_additional_code_type and codelst_kind ='financial' and trim(md_modelementdata) is not null
    order by codelst_seq;
  else
  select trim(' &bull; ' || (codelst_desc) || ' - ' || md_modelementdata || '<br>') event_addlcode bulk collect into tab_rows 
   from  er_moredetails, er_codelst where pk_codelst = md_modelementpk and fk_modpk = event and md_modname = p_additional_code_type
    and CODELST_TYPE = p_additional_code_type and trim(md_modelementdata) is not null
    order by codelst_seq;
  end case;
  
  FOR l_rec IN 1 .. tab_rows.COUNT
   loop
  addl_lst := addl_lst || tab_rows(l_rec);
  end loop;
 --dbms_output.put_line (substr(addl_lst,1,length(addl_lst)-1) ) ;

return addl_lst ;
end;
/


CREATE or replace SYNONYM ERES.LST_ADDITIONALCODES_FINANCIAL FOR LST_ADDITIONALCODES_FINANCIAL;


GRANT EXECUTE, DEBUG ON LST_ADDITIONALCODES_FINANCIAL TO ERES;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,342,1,'01_staddtcodesfinan.sql',sysdate,'v9.3.0 #743');

commit;
