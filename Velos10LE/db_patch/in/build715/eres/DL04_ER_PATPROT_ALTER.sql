set define off;

Update er_patprot 
Set PATPROT_OTHR_DIS_CODE = ANATOMIC_SITE where ANATOMIC_SITE is not null;
commit;

alter table er_patprot
drop column  ANATOMIC_SITE;
commit;
/