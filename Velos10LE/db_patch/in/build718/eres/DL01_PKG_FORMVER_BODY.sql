CREATE OR REPLACE PACKAGE BODY        "PKG_FORMVER" 
AS
/****************************************************************************************************
** Procedure to create version of form when the form status is made active
** Author: Sonika Talwar May 05, 2004
** Input parameter: Form Id
**/

PROCEDURE CREATEFORMVERSION (p_form NUMBER)
AS
v_ver_number NUMBER;
v_xslrefresh NUMBER;
v_html CLOB;

BEGIN
  --if the form xsl is not generated or is not latest then first generate the xsl
 /* select form_xslrefresh
  into v_xslrefresh
  from er_formlib
  where pk_formlib = p_form;
  p('xslrefresh 3 ' || v_xslrefresh);

  if (v_xslrefresh=1) then
    pkg_form.SP_CLUBFORMXSL(p_form,v_html);
    commit;
  end if;*/
/* modified by SA, 11/20/06 - formlib version number can be changed and can be text. so count() will be a safe option*/
  SELECT COUNT(formlibver_number)
  INTO v_ver_number
  FROM ER_FORMLIBVER
  WHERE fk_formlib = p_form;

  v_ver_number := v_ver_number + 1;

 BEGIN
  INSERT INTO ER_FORMLIBVER (PK_FORMLIBVER  ,
              FK_FORMLIB,
		    FORMLIBVER_NUMBER,
		    FORMLIBVER_DATE,
		    FORMLIBVER_XML,
		    FORMLIBVER_XSL,
		    FORMLIBVER_VIEWXSL,
		    CREATOR,
		    RECORD_TYPE,
		    LAST_MODIFIED_BY,
		    IP_ADD)
  SELECT seq_er_formlibver.NEXTVAL,
       pk_formlib,
	  v_ver_number,
       SYSDATE,
       form_xml,
       form_xsl,
       form_viewxsl,
       creator,
       'N',
       last_modified_by,
       ip_add
  FROM ER_FORMLIB
  WHERE pk_formlib = p_form;

 EXCEPTION WHEN NO_DATA_FOUND THEN
    P('no data');
 END;
 COMMIT;

END;

/* Added by Sonia Abrol, 09/11/06 -  get the form's version number from the filled from ID and from type*/
FUNCTION f_get_versionumber(p_filledformid NUMBER, p_formtype VARCHAR2) RETURN VARCHAR2
IS
  v_versionNumber ER_FORMLIBVER.formlibver_number%TYPE;
BEGIN

	 BEGIN
		 IF p_formtype = 'P' THEN

		 	SELECT formlibver_number
			INTO v_versionNumber
			FROM ER_FORMLIBVER, ER_PATFORMS
			WHERE pk_patforms = p_filledformid  AND fk_formlibver = pk_formlibver;

		 ELSIF p_formtype = 'S' THEN

		 	SELECT formlibver_number
			INTO v_versionNumber
			FROM ER_FORMLIBVER, ER_STUDYFORMS
			WHERE pk_studyforms = p_filledformid  AND fk_formlibver = pk_formlibver;


		 ELSIF p_formtype = 'A' THEN

			SELECT formlibver_number
			INTO v_versionNumber
			FROM ER_FORMLIBVER, ER_ACCTFORMS
			WHERE pk_acctforms = p_filledformid  AND fk_formlibver = pk_formlibver;


		 END IF;
	 EXCEPTION WHEN NO_DATA_FOUND THEN

	 		   v_versionNumber := '';
	 END;

	 RETURN v_versionNumber;
END;


/* Added by Sonia Abrol, 11/21/06 - update the formlib ver pk in formslinear for a form, used in form version migration*/
PROCEDURE SP_MIGRATE_FORMLIBVER( p_form IN VARCHAR2 ,p_formlibver IN VARCHAR2 , o_err OUT VARCHAR2)
AS
   v_number VARCHAR2(100);
   v_active_status  NUMBER;
    v_creator NUMBER;
	 v_ip VARCHAR2(15);
	 v_last_statuspk NUMBER;

BEGIN
	 	  -- get form version number
		  BEGIN
		  	   	 SELECT formlibver_number
				 INTO v_number
				 FROM ER_FORMLIBVER
				 WHERE pk_formlibver = p_formlibver;


				 UPDATE ER_FORMSLINEAR
				 SET form_version =v_number
				 WHERE  fk_form = p_form;


				 --update the status form to 'Active'

				 SELECT pk_codelst
					INTO v_active_status FROM ER_CODELST
					WHERE trim(codelst_type) = 'frmstat' AND
					trim(codelst_subtyp) = 'A' ;

					UPDATE ER_FORMLIB
					SET form_status = v_active_status
					WHERE pk_formlib = p_form;

					SELECT creator , ip_add,pk_formstat  INTO v_creator, v_ip, v_last_statuspk
					FROM ER_FORMSTAT
					WHERE fk_formlib = p_form AND FORMSTAT_ENDDATE IS NULL AND ROWNUM < 2;

					 SP_MIGRATE_FORM_DATAVAL(p_form);


					UPDATE ER_FORMSTAT
					SET  FORMSTAT_ENDDATE = SYSDATE
					WHERE fk_formlib = p_form AND pk_formstat = v_last_statuspk;


				INSERT INTO ER_FORMSTAT ( PK_FORMSTAT, FK_FORMLIB, FK_CODELST_STAT,
FORMSTAT_STDATE, FORMSTAT_NOTES, RECORD_TYPE, CREATOR,
 CREATED_ON, IP_ADD,formstat_changedby ) VALUES (
seq_ER_FORMSTAT.NEXTVAL, P_form,  v_active_status, SYSDATE, 'Form Responses migrated to latest form version',  'N', v_creator,
SYSDATE, v_ip,v_creator);


				 COMMIT;

		  EXCEPTION WHEN OTHERS THEN
		  			o_err := SQLERRM;
		  END;
END;


/* Added by Sonia Abrol, 12/19/06 - update the data values in multiple choice fields, used in form version migration*/
PROCEDURE SP_MIGRATE_FORM_DATAVAL( p_form IN VARCHAR2)
AS
v_sql LONG;
v_form NUMBER;
v_cbx_cur Gk_Cv_Types.GenericCursorType;
 v_cur_cbxval  VARCHAR2(4000);
 v_cur_unreplaced_val  VARCHAR2(4000);

v_checbox_colsql VARCHAR2(4000);
v_chkcount NUMBER;
v_cbx_dispval VARCHAR2(4000);
v_counter NUMBER;
v_new_dataval VARCHAR2(4000);
v_new_cbxval VARCHAR2(4000);
v_update_chksql VARCHAR2(8000);
v_updarows NUMBER;
v_dataval  VARCHAR2(4000);
v_MR_MD_COUNT NUMBER ;

v_chkbox_string VARCHAR2(4000);
v_pos NUMBER;


BEGIN
v_form := p_form;




	-- for all fields
	FOR k IN (SELECT mp_mapcolname,mp_pkfld,mp_flddatatype FROM ER_MAPFORM WHERE fk_form = v_form AND mp_flddatatype  IN ( 'MC','MD','MR' ) )
	LOOP
		-- get distinct column values for this column

		v_checbox_colsql := 'select distinct ' ||  k.mp_mapcolname || ' from er_formslinear where fk_form = ' ||  v_form || ' and '
		||  k.mp_mapcolname || ' is not null ' ;


		OPEN v_cbx_cur FOR v_checbox_colsql;

		LOOP

		 FETCH v_cbx_cur  INTO v_cur_cbxval;

		 EXIT WHEN v_cbx_cur %NOTFOUND;

		 --process tha value, get only the display value part

		 v_chkbox_string := v_cur_cbxval;


		 v_cur_cbxval := SUBSTR(v_cur_cbxval,1,(INSTR(v_cur_cbxval, '[VELSEP1]') -1) );
		 v_cur_unreplaced_val :=  v_cur_cbxval;

		 -- escape spl char

		  v_cur_cbxval := REPLACE( v_cur_cbxval,'''','''''');

		 --split the display value

		  IF (k.mp_flddatatype = 'MC') THEN
		  	 v_chkbox_string := REPLACE(v_chkbox_string,'''','''''');
	 		 v_chkbox_string := SUBSTR(v_chkbox_string,1,INSTR(v_chkbox_string,'[VELSEP1]')+8);
	 		 v_chkbox_string := REPLACE(v_chkbox_string,'[VELSEP1]',',') || '[';
			 v_new_dataval := ' ';
			 WHILE TRUE
	 		 LOOP
	 	 	 	 v_pos := INSTR(v_chkbox_string,'],[');
	 	 		  IF v_pos IS NULL THEN
		 		  	 EXIT;
				  END IF;
		 		  v_cbx_dispval := SUBSTR(v_chkbox_string,1,v_pos);
				  BEGIN
		 		  	   SELECT fldresp_dataval INTO v_dataval FROM ER_FLDRESP WHERE fk_field = k.mp_pkfld AND '['||  REPLACE( fldresp_dispval,'''','''''')  ||']' = v_cbx_dispval AND ROWNUM < 2;
		 			   EXCEPTION WHEN NO_DATA_FOUND THEN
		 			   v_dataval := '';

		 		 END;
 				 v_dataval := REPLACE( v_dataval,'''','''''');
		 		 IF (  v_new_dataval = ' ' )	THEN
		 		 	v_new_dataval := v_dataval;
		 		 ELSE
		 			v_new_dataval := v_new_dataval || '[VELCOMMA]' || v_dataval;
		 		END IF;
		 		 v_chkbox_string := SUBSTR(v_chkbox_string,v_pos+2);
		 	END LOOP;
            v_new_dataval := REGEXP_REPLACE(v_new_dataval, '\[VELCOMMA\]$', '');
			v_new_cbxval := v_cur_cbxval ||'[VELSEP1]' || v_new_dataval;

			--update formslinear sql

			v_update_chksql := 'update er_formslinear set ' ||  k.mp_mapcolname ||
			' = ''' || v_new_cbxval || ''' where fk_form = ' || v_form || ' and '
			||  k.mp_mapcolname || ' like ''' || v_cur_cbxval ||'[VELSEP1]%''';

			EXECUTE IMMEDIATE v_update_chksql;


/*
		 IF (k.mp_flddatatype = 'MC') THEN
			 SELECT COUNT(*)
			 INTO v_chkcount
			 FROM TABLE(Pkg_Util.f_split(v_cur_cbxval,','))	  ;

		 ELSE
		 		v_chkcount := 0;
		 END IF	 ;

		plog.DEBUG(pctx, ' v_cur_cbxval ' ||  v_cur_cbxval || 'v_chkcount  ' || v_chkcount  );


		 IF v_chkcount  > 0 THEN

		 	v_counter := 0;
		 	v_new_dataval := ' ';
		 	v_new_cbxval := '';

		 	FOR o IN 1..v_chkcount
		 	LOOP
		 		v_counter := v_counter + 1;
		 		v_cbx_dispval := Pkg_Util.f_getvaluefromdelimitedstring(','|| v_cur_cbxval || ',',o ,',');

		 		-- get data value for this
		 		BEGIN
		 		SELECT fldresp_dataval INTO v_dataval
		 		FROM ER_FLDRESP WHERE fk_field = k.mp_pkfld AND
		 		'['||  REPLACE( fldresp_dispval,'''','''''')  ||']' = v_cbx_dispval AND ROWNUM < 2;

				plog.DEBUG(pctx, 'dataval sql ' || 'SELECT fldresp_dataval INTO v_dataval FROM ER_FLDRESP WHERE fk_field = '||k.mp_pkfld || 'AND [''|| fldresp_dispval ||''] =' || v_cbx_dispval  || 'AND ROWNUM < 2');

		 		EXCEPTION WHEN NO_DATA_FOUND THEN
		 			v_dataval := '';
		 		END;

				v_dataval := REPLACE( v_dataval,'''','''''');

		 		IF (  v_new_dataval = ' ' )	THEN
		 			v_new_dataval := v_dataval;
		 		ELSE
		 			v_new_dataval := v_new_dataval || '[VELCOMMA]' || v_dataval;
		 		END IF;

		 	END LOOP;
			v_new_cbxval := v_cur_cbxval ||'[VELSEP1]' || v_new_dataval;

			--update formslinear sql

			v_update_chksql := 'update er_formslinear set ' ||  k.mp_mapcolname ||
			' = ''' || v_new_cbxval || ''' where fk_form = ' || v_form || ' and '
			||  k.mp_mapcolname || ' like ''' || v_cur_cbxval ||'[VELSEP1]%''';

					plog.DEBUG(pctx, 'v_update_chksql sql ' || v_update_chksql);

			EXECUTE IMMEDIATE v_update_chksql;
		 END IF;
*/
ELSE
	 	 		BEGIN
	 	 				SELECT fldresp_dataval
	 	 				INTO v_dataval
		 				FROM ER_FLDRESP WHERE fk_field = k.mp_pkfld AND
		 				fldresp_dispval = v_cur_unreplaced_val  AND ROWNUM < 2;

		 				v_dataval := REPLACE( v_dataval,'''','''''');

	 	 		 EXCEPTION WHEN NO_DATA_FOUND THEN
	 	 				v_dataval := '';
	 	 		END;

		 	 	v_new_cbxval := v_cur_cbxval ||'[VELSEP1]' || v_dataval ;

		 	 	v_update_chksql := 'update er_formslinear set ' ||  k.mp_mapcolname ||
				' = ''' || v_new_cbxval || ''' where fk_form = ' || v_form || ' and '
				||  k.mp_mapcolname || ' like ''' || v_cur_cbxval ||'[VELSEP1]%''';


				EXECUTE IMMEDIATE v_update_chksql;

		 	 		--end if for other fields
	 	 END IF;

	    END LOOP; -- for chkbox column cursor

	 CLOSE v_cbx_cur ;


	END LOOP; -- for chkbox columns

	commit;


END;


END Pkg_Formver;
/


CREATE OR REPLACE SYNONYM ESCH.PKG_FORMVER FOR PKG_FORMVER;


CREATE OR REPLACE SYNONYM EPAT.PKG_FORMVER FOR PKG_FORMVER;


GRANT EXECUTE, DEBUG ON PKG_FORMVER TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_FORMVER TO ESCH;

