SET define OFF;

DECLARE
filterID number := 0;
v_exists NUMBER := 0;
begin
	select count(*) into v_exists from er_repfilter where REPFILTER_KEYWORD = 'inclValMsgChk';
	if (v_exists = 0) then
		select Max(PK_REPFILTER)+1 into filterID from er_repfilter;

		INSERT INTO er_repfilter (PK_REPFILTER,REPFILTER_COLUMN,REPFILTER_VALUESQL,REPFILTER_COLDISPNAME,REPFILTER_KEYWORD,REPFILTER_DISPDIV) 
		VALUES (filterID,'<td><DIV id="inclValMsgChkDIV"><input type="checkbox" name="valCheck" value="0" onClick="this.value = (this.checked)? ''1'' : ''0'';"/> Including Validation Messages </DIV></td>',
		NULL,'Including Validation Messages','inclValMsgChk','inclValMsgChkDIV');
		COMMIT;
	end if;

END;
/


