CREATE  OR REPLACE TRIGGER ER_BULK_UPLOAD_ERRORROW_AD0 AFTER DELETE ON ER_BULK_UPLOAD_ERRORROW        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 oldUsr := 'New User' ;
 
 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END; 
  Audit_Trail.record_transaction (raid, 'ER_BULK_UPLOAD_ERRORROW', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr );
       Audit_Trail.column_delete (raid, 'FILE_ERROR_ROWDATA', :OLD.FILE_ERROR_ROWDATA);
       Audit_Trail.column_delete (raid, 'FILE_HEADER_ROWDATA', :OLD.FILE_HEADER_ROWDATA);
       Audit_Trail.column_delete (raid, 'FILE_ROW_NUMBER', :OLD.FILE_ROW_NUMBER);
       Audit_Trail.column_delete (raid, 'FK_BULK_UPLOAD', :OLD.FK_BULK_UPLOAD);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr );
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'PK_BULK_UPLOAD_ERRORROW', :OLD.PK_BULK_UPLOAD_ERRORROW);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
COMMIT;
END;
/
