SET define OFF;

-- Update into ER_REPORT
DECLARE
  v_record_exists NUMBER := 0;
BEGIN
  SELECT COUNT(*) INTO v_record_exists FROM er_report WHERE pk_report = 181;
  IF (v_record_exists != 0) THEN
 UPDATE er_report SET rep_sql = 'SELECT disease_site,
  repsitename,
  enrsitename,
  studyTypes,
  Anatomicsitenull,
  COUNT 
  FROM( select codelst_desc as disease_site,
    (select site_name from er_site where pk_site in (:repOrgId)) as repsitename,
     SITE_NAME as enrsitename,
   '''' as studyTypes,
  '''' AS Anatomicsitenull,
  (SELECT count(*)
      FROM ER_PATPROT patprt, er_patstudystat patst,er_study stdy, er_studysites stsite
      WHERE  stdy.fk_account = :sessAccId
      AND stdy.pk_study = patprt.fk_study
      AND patprt.FK_PER = patst.fk_per
      AND stsite.fk_study=stdy.pk_study
      AND  patprt.fk_study  = patst.fk_study
      AND FK_CODELST_STAT = (SELECT pk_codelst FROM ER_CODELST WHERE CODELST_TYPE = ''patStatus'' AND codelst_subtyp = ''enrolled'')
      AND PATSTUDYSTAT_DATE BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
      AND PATPRT.PATPROT_STAT = 1 AND fk_site_enrolling in (pk_site)
      AND FK_SITE in (:repOrgId) AND NVL(PATPROT_TREATINGORG,0) IN (:trtOrgId) 
      AND PK_CODELST in (SELECT REGEXP_SUBSTR(NVL(anatomic_site,0), ''[^,]+'',1,ROWNUM) FROM   dual
	         CONNECT BY ROWNUM <= LENGTH(NVL(anatomic_site,0)) - LENGTH(REPLACE(NVL(anatomic_site,0),'',''))+1) 
      AND FK_CODELST_TYPE in (
      select pk_codelst from ER_CODELST where pk_codelst in (:studyType) and CODELST_CUSTOM_COL1=''interventional''
      )) as count
  FROM er_codelst, er_site
  WHERE codelst_type = ''disease_site'' AND PK_SITE in (:enrOrgId)
  UNION ALL
  Select '''' as disease_site,
  (select site_name from er_site where pk_site in (:repOrgId)) as repsitename,
  '''' as enrsitename,
  F_STRING_AGGREGATE(q''[SELECT CODELST_DESC from er_codelst where pk_codelst IN (:studyType) AND  NVL(CODELST_CUSTOM_COL1, ''noninterventional'') <> ''interventional'']'') as studyTypes,
  '''' AS Anatomicsitenull,
  0 as count
  FROM dual
  UNION ALL
  SELECT '''' as disease_site,
    (select site_name from er_site where pk_site in (:repOrgId)) as repsitename,
     SITE_NAME as enrsitename,
     '''' as studyTypes,
     (''&#160;Study:&#160;study number-''||STUDY_NUMBER ||'', ''||''Patient:&#160;patient-''||(select PER_CODE from er_per where PK_PER = patprt.fk_per)) AS Anatomicsitenull,
     0 as count
    FROM ER_PATPROT patprt,er_patstudystat patst,er_study stdy,er_site st,er_studysites stsite
      WHERE  stdy.fk_account = :sessAccId AND stdy.pk_study = patprt.fk_study
      AND patprt.FK_PER = patst.fk_per
      AND stsite.fk_study=stdy.pk_study
      AND  patprt.fk_study  = patst.fk_study
      AND FK_CODELST_STAT = (SELECT pk_codelst FROM ER_CODELST WHERE CODELST_TYPE = ''patStatus'' AND codelst_subtyp = ''enrolled'')
      AND PATSTUDYSTAT_DATE BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
      AND PATPRT.PATPROT_STAT = 1 AND fk_site_enrolling in (pk_site) 
      AND FK_SITE in (:repOrgId) AND NVL(PATPROT_TREATINGORG,0) IN (:trtOrgId) AND ANATOMIC_SITE is null
      AND stdy.FK_CODELST_TYPE in (select pk_codelst from ER_CODELST where pk_codelst in (:studyType) and CODELST_CUSTOM_COL1=''interventional'')
      AND PK_SITE in (:enrOrgId)
  ) order by enrsitename nulls first,disease_site nulls first '
    WHERE pk_report = 181;
    COMMIT;

UPDATE er_report SET rep_sql_clob = 'SELECT disease_site,
  repsitename,
  enrsitename,
  studyTypes,
  Anatomicsitenull,
  COUNT 
  FROM( select codelst_desc as disease_site,
    (select site_name from er_site where pk_site in (:repOrgId)) as repsitename,
     SITE_NAME as enrsitename,
   '''' as studyTypes,
  '''' AS Anatomicsitenull,
  (SELECT count(*)
      FROM ER_PATPROT patprt, er_patstudystat patst,er_study stdy, er_studysites stsite
      WHERE  stdy.fk_account = :sessAccId
      AND stdy.pk_study = patprt.fk_study
      AND patprt.FK_PER = patst.fk_per
      AND stsite.fk_study=stdy.pk_study
      AND  patprt.fk_study  = patst.fk_study
      AND FK_CODELST_STAT = (SELECT pk_codelst FROM ER_CODELST WHERE CODELST_TYPE = ''patStatus'' AND codelst_subtyp = ''enrolled'')
      AND PATSTUDYSTAT_DATE BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
      AND PATPRT.PATPROT_STAT = 1 AND fk_site_enrolling in (pk_site)
      AND FK_SITE in (:repOrgId) AND NVL(PATPROT_TREATINGORG,0) IN (:trtOrgId) 
      AND PK_CODELST in (SELECT REGEXP_SUBSTR(NVL(anatomic_site,0), ''[^,]+'',1,ROWNUM) FROM   dual
	         CONNECT BY ROWNUM <= LENGTH(NVL(anatomic_site,0)) - LENGTH(REPLACE(NVL(anatomic_site,0),'',''))+1) 
      AND FK_CODELST_TYPE in (
      select pk_codelst from ER_CODELST where pk_codelst in (:studyType) and CODELST_CUSTOM_COL1=''interventional''
      )) as count
  FROM er_codelst, er_site
  WHERE codelst_type = ''disease_site'' AND PK_SITE in (:enrOrgId)
  UNION ALL
  Select '''' as disease_site,
  (select site_name from er_site where pk_site in (:repOrgId)) as repsitename,
  '''' as enrsitename,
  F_STRING_AGGREGATE(q''[SELECT CODELST_DESC from er_codelst where pk_codelst IN (:studyType) AND  NVL(CODELST_CUSTOM_COL1, ''noninterventional'') <> ''interventional'']'') as studyTypes,
  '''' AS Anatomicsitenull,
  0 as count
  FROM dual
  UNION ALL
  SELECT '''' as disease_site,
    (select site_name from er_site where pk_site in (:repOrgId)) as repsitename,
     SITE_NAME as enrsitename,
     '''' as studyTypes,
     (''&#160;Study:&#160;study number-''||STUDY_NUMBER ||'', ''||''Patient:&#160;patient-''||(select PER_CODE from er_per where PK_PER = patprt.fk_per)) AS Anatomicsitenull,
     0 as count
    FROM ER_PATPROT patprt,er_patstudystat patst,er_study stdy,er_site st,er_studysites stsite
      WHERE  stdy.fk_account = :sessAccId AND stdy.pk_study = patprt.fk_study
      AND patprt.FK_PER = patst.fk_per
      AND stsite.fk_study=stdy.pk_study
      AND  patprt.fk_study  = patst.fk_study
      AND FK_CODELST_STAT = (SELECT pk_codelst FROM ER_CODELST WHERE CODELST_TYPE = ''patStatus'' AND codelst_subtyp = ''enrolled'')
      AND PATSTUDYSTAT_DATE BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
      AND PATPRT.PATPROT_STAT = 1 AND fk_site_enrolling in (pk_site) 
      AND FK_SITE in (:repOrgId) AND NVL(PATPROT_TREATINGORG,0) IN (:trtOrgId) AND ANATOMIC_SITE is null
      AND stdy.FK_CODELST_TYPE in (select pk_codelst from ER_CODELST where pk_codelst in (:studyType) and CODELST_CUSTOM_COL1=''interventional'')
      AND PK_SITE in (:enrOrgId)
  ) order by enrsitename nulls first,disease_site nulls first '
    WHERE pk_report = 181;
    COMMIT;
    
  END IF;
  END;
/