Set Define off;

--eSample build ES011
declare
rowExists number := 0;
begin
	select count(*) into rowExists from ER_BULK_ENTITY_DETAIL where PK_BULK_ENTITY_DETAIL = 26;
	if (rowExists != 0) then
		Delete from ER_BULK_ENTITY_DETAIL where PK_BULK_ENTITY_DETAIL = 26;
	end if;
	
	select count(*) into rowExists from ER_BULK_ENTITY_DETAIL where PK_BULK_ENTITY_DETAIL = 27;
	if (rowExists != 0) then
		Delete from ER_BULK_ENTITY_DETAIL where PK_BULK_ENTITY_DETAIL = 27;
	end if;
	
	select count(*) into rowExists from ER_BULK_ENTITY_DETAIL where PK_BULK_ENTITY_DETAIL = 28;
	if (rowExists != 0) then
		Delete from ER_BULK_ENTITY_DETAIL where PK_BULK_ENTITY_DETAIL = 28;
	end if;

	select count(*) into rowExists from ER_BULK_ENTITY_DETAIL where PK_BULK_ENTITY_DETAIL = 29;
	if (rowExists != 0) then
		Update ER_BULK_ENTITY_DETAIL set PK_BULK_ENTITY_DETAIL = 26 where PK_BULK_ENTITY_DETAIL = 29;
	end if;
	COMMIT;
end ;
/