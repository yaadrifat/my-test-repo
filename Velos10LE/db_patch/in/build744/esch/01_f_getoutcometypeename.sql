-- Author:
 --Vivek Kumar Jha 24/08/2015
CREATE OR REPLACE FUNCTION F_GETOUTCOMETYPENAME(outcome_type varchar) RETURN VARCHAR 
is
pos number;
  counter number:=1;
  returnStr varchar(100);
  selectStr varchar(100);
  outcomes  varchar2(20);
BEGIN
outcomes:=outcome_type;
-- find all  1(one) form outcomes varchar using while loop
WHILE 1=1
loop
pos:=INSTR(outcomes,'1',counter,1);
if pos > 0 then
-- find codelst_desc
select codelst_desc  into selectStr from sch_codelst where codelst_type='outcome' and  codelst_seq =pos;
returnStr:=returnStr ||selectStr || ',';
counter:=pos+1;
end if;
--pos=0 means 1(one) did not find
 EXIT WHEN pos = 0;
END loop;
  if length(returnStr)> 0 then
  --remove the comma of last index from returnStr
  returnStr:=SUBSTR(returnStr, 1, length(returnStr)-1);
  else
  returnStr:=' '; -- for space
  end if;
  RETURN returnStr;
END F_GETOUTCOMETYPENAME;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,343,0,'01_f_getoutcometypeename.sql',sysdate,'v9.3.0 #744');

commit;