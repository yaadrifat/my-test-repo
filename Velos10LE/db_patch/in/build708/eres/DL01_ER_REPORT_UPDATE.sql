SET define OFF;

-- Update into ER_REPORT
DECLARE
  v_record_exists NUMBER := 0;
  SQL_CLOB CLOB;
BEGIN
  SELECT COUNT(*) INTO v_record_exists FROM er_report WHERE pk_report = 182;
  IF (v_record_exists != 0) THEN
    Update ER_REPORT Set REP_HIDE = 'N', REP_FILTERBY = 'Date, Reporting Organization, Study Type, Research Type', 
	REP_FILTERKEYWORD='[DATEFILTER]D:repOrgId:studyType:studyResType', 
	REP_FILTERAPPLICABLE='date:repOrgId:studyType:studyResType' 
	where pk_report = 182;
    COMMIT;
 SQL_CLOB := 'SELECT
  ( SELECT CODELST_DESC FROM ER_CODELST WHERE PK_CODELST = FK_CODELST_RESTYPE
  ) AS RESEARCH_TYPE,
 ( SELECT CODELST_DESC FROM ER_CODELST WHERE PK_CODELST = FK_CODELST_TYPE
  ) AS STUDY_TYPE,
  (SELECT SITE_NAME
  FROM ER_SITE
  WHERE PK_SITE IN (FK_SITE)
  ) AS REPSITENAME,
  DECODE(fk_codelst_sponsor,NULL,'''',
  (SELECT CODELST_DESC FROM ER_CODELST WHERE PK_CODELST = FK_CODELST_SPONSOR)) 
  ||DECODE(study_sponsor,NULL,'''','', ''||study_sponsor)
  ||DECODE((select count(*) from er_studySites 
   where FK_STUDY = PK_STUDY and FK_CODELST_STUDYSITETYPE in 
   (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE = ''studySiteType'' 
   and CODELST_SUBTYP = ''sponsor'')),0,'''','', ''||(SELECT wm_concat(SITE_NAME) FROM ER_SITE
   WHERE PK_SITE IN (select FK_SITE from er_studySites 
   where FK_STUDY = PK_STUDY and FK_CODELST_STUDYSITETYPE in 
   (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE = ''studySiteType'' 
   and CODELST_SUBTYP = ''sponsor''))))  AS SPECIFIC_FUNDING_SOURCE,
  F_GETDIS_SITE(STUDY_DISEASE_SITE) AS ANATOMIC_SITE,
  STUDY_NUMBER                      AS PROTO_ID,
  DECODE(study_prinv,NULL,DECODE(study_otherprinv,NULL,'''',study_otherprinv),
  (SELECT USR_LASTNAME
    || '',''
    || usr_firstname
  FROM ER_USER
  WHERE pk_user = study_prinv
  )
  || DECODE(study_otherprinv,NULL,'''',''; ''
  || STUDY_OTHERPRINV)) AS PI,
  (SELECT studyid_id
  FROM er_studyid
  WHERE fk_codelst_idtype =
    (SELECT pk_codelst
    FROM ER_CODELST
    WHERE CODELST_TYPE = ''studyidtype''
    AND codelst_subtyp = ''sum4_prg''
    )
  AND fk_study = stdy.pk_study
  ) AS Program_Code,
  (SELECT TO_CHAR(MIN(studystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT)
  FROM ER_STUDYSTAT
  WHERE fk_study           = stdy.pk_study
  AND fk_codelst_studystat =
    (SELECT pk_codelst
    FROM ER_CODELST
    WHERE CODELST_TYPE = ''studystat''
    AND codelst_subtyp = ''active''
    )
  ) AS OPEN_FOR_ENROLL_DT,
  (SELECT TO_CHAR(MIN(studystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT)
  FROM ER_STUDYSTAT
  WHERE fk_study           = stdy.pk_study
  AND fk_codelst_studystat =
    (SELECT pk_codelst
    FROM ER_CODELST
    WHERE CODELST_TYPE = ''studystat''
    AND codelst_subtyp = ''prmnt_cls''
    )
  ) AS CLOSED_TO_ACCR_DT,
  DECODE(FK_CODELST_PHASE,NULL,'''',
  ( SELECT CODELST_DESC FROM ER_CODELST WHERE PK_CODELST = FK_CODELST_PHASE
  )) AS PHASE,
  DECODE(FK_CODELST_PURPOSE,NULL,'''',
  ( SELECT CODELST_DESC FROM ER_CODELST WHERE PK_CODELST = FK_CODELST_PURPOSE
  ))          AS PRIMARY_PURPOSE,
  study_title AS OFFICIAL_TITLE,
  DECODE(
  (SELECT CODELST_DESC FROM ER_CODELST WHERE PK_CODELST = FK_CODELST_SCOPE
  ),NULL,'''',''Multi Center Study'',''YES'',''Single Center Study'',''NO'') AS IS_MULTI_INST_TRIAL,
  NVL(STUDY_NSAMPLSIZE,0)                                          AS ENTIRE_STUDY,
  NVL(studysite_lsamplesize,0)                                     AS YOUR_CENTER,
  NVL(
  (SELECT COUNT(*)
  FROM ER_PATPROT PATPRT,
    ER_PATSTUDYSTAT PATST
  WHERE PATPRT.FK_STUDY = pk_study
  AND PATPRT.FK_STUDY   = PATST.FK_STUDY
  AND PATPRT.FK_PER     = PATST.FK_PER
  AND FK_CODELST_STAT   =
    (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_SUBTYP = ''enrolled''
    )
  AND PATSTUDYSTAT_DATE BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
  AND PATPRT.FK_SITE_ENROLLING          = STDSITE.FK_SITE
  AND STDSITE.FK_CODELST_STUDYSITETYPE IN
    (SELECT pk_codelst
    FROM ER_CODELST
    WHERE CODELST_TYPE = ''studySiteType''
    AND CODELST_SUBTYP = ''primary''
    )
  ),0) AS MOS_PRIMARY,
  NVL(
  (SELECT COUNT(*)
  FROM ER_PATPROT PATPRT,
    ER_PATSTUDYSTAT PATST
  WHERE PATPRT.FK_STUDY = pk_study
  AND PATPRT.FK_STUDY   = PATST.FK_STUDY
  AND PATPRT.FK_PER     = PATST.FK_PER
  AND FK_CODELST_STAT   =
    (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_SUBTYP = ''enrolled''
    )
  AND PATPRT.FK_SITE_ENROLLING          = STDSITE.FK_SITE
  AND STDSITE.FK_CODELST_STUDYSITETYPE IN
    (SELECT pk_codelst
    FROM ER_CODELST
    WHERE CODELST_TYPE = ''studySiteType''
    AND CODELST_SUBTYP = ''primary''
    )
  ),0) AS TO_DATE_PRIMARY,
  NVL(
  (SELECT COUNT(*)
  FROM ER_PATPROT PATPRT,
    ER_PATSTUDYSTAT PATST
  WHERE PATPRT.FK_STUDY = pk_study
  AND PATPRT.FK_STUDY   = PATST.FK_STUDY
  AND PATPRT.FK_PER     = PATST.FK_PER
  AND FK_CODELST_STAT   =
    (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_SUBTYP = ''enrolled''
    )
  AND PATSTUDYSTAT_DATE BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
  AND PATPRT.FK_SITE_ENROLLING          = STDSITE.FK_SITE
  AND STDSITE.FK_CODELST_STUDYSITETYPE IN
    (SELECT pk_codelst
    FROM ER_CODELST
    WHERE CODELST_TYPE  = ''studySiteType''
    AND CODELST_SUBTYP <> ''primary''
    )
  ),0) AS MOS_OTHERS,
  NVL(
  (SELECT COUNT(*)
  FROM ER_PATPROT PATPRT,
    ER_PATSTUDYSTAT PATST
  WHERE PATPRT.FK_STUDY = pk_study
  AND PATPRT.FK_STUDY   = PATST.FK_STUDY
  AND PATPRT.FK_PER     = PATST.FK_PER
  AND FK_CODELST_STAT   =
    (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_SUBTYP = ''enrolled''
    )
  AND PATPRT.FK_SITE_ENROLLING          = STDSITE.FK_SITE
  AND STDSITE.FK_CODELST_STUDYSITETYPE IN
    (SELECT pk_codelst
    FROM ER_CODELST
    WHERE CODELST_TYPE  = ''studySiteType''
    AND CODELST_SUBTYP <> ''primary''
    )
  ),0) AS TO_DATE_OTHERS
FROM ER_STUDY stdy,
  ER_STUDYSITES STDSITE
WHERE STDSITE.FK_STUDY    = stdy.PK_STUDY
AND stdy.FK_ACCOUNT       = :sessAccId
AND FK_SITE              IN (:repOrgId)
AND stdy.FK_CODELST_TYPE IN
  (SELECT PK_CODELST
  FROM ER_CODELST
  WHERE PK_CODELST      IN (:studyType)
  AND CODELST_CUSTOM_COL1=''interventional''
  )
AND stdy.FK_CODELST_RESTYPE IN (:studyResType)
AND CCSG_REPORTABLE_STUDY    = 1
ORDER BY STUDY_TYPE, RESEARCH_TYPE,SPECIFIC_FUNDING_SOURCE,
  PI ';

  UPDATE er_report SET rep_sql_clob = SQL_CLOB
    WHERE pk_report = 182;
    COMMIT;
    
  END IF;
  END;
/
