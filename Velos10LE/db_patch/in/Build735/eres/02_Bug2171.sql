update er_codelst 
set codelst_desc ='Data Monitoring'  
where  codelst_desc = 'Form Queries'
          and codelst_type = 'newdashboard' and 
		  codelst_subtyp = 'dbformquery';
commit;		  


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,334,2,'02_Bug2171.sql',sysdate,'v9.3.0 #735');

commit;