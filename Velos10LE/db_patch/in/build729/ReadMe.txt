/////*********This readMe is specific to v9.3.0 build #730**********////

In Build#728 we have made a partial release of following  enhancements :
1. INF_22616_No1
2. Form_22617_No2

and a complete release of 
3. Form_22620_No3 

We have further worked on these two requirements and released them in this build. 

What has been done till now in INF_22616_No1 and Form_22617_No2:

1. Dynamic Drop down to Select dashboard type.
2. Search Filters have been created however, only 2 filters(Study Filter and Organization Filter) are linked to the queries and they are working. rest others needs to 	be linked to the Queries.
3. Search filter with auto complete and mulch-select.
4. Data-table with pagination.
5. Multi-column sorting.
6. Column hide and show functionality 
7. Exporting to pdf, doc and excel
8. Localization
9. Filters complete  implementation.  (Study and Organization filters are working and  rest other filters  are under implementation i.e. not fully funtional.You can see the data.)
10. Saved Search functionality.
11. Bulk Status Update
12. Save User views

Features not implemented:

1. Color coded column for status
2. Column cell check-boxes.
3. Check boxes based on codelst type with color coding defined in custom config bundle

Notes:
1. To have the full export functionality, one needs to have FLASH PLAYER installed in the browser/system.
2. Based on Input from QA team , We have released around 12 bugs, which are no longer reproducible in the current v9.3. builds. These bugs might have been fixed due to some other fixes over the period of time.

