/////*********This readMe is specific to v9.3.0 build #723**********////

1. Velos hidden feature (keep this info away from the customers)
The "Configure" menu with all its submenus is supposed to be a hidden feature reserved 
for Velos employees, not intended for the customers. This feature is now hidden except from
users who are specified in ER_CTRLTAB. To enable this feature, add your user login to the
CTRL_VALUE of the row with CTRL_KEY='auth_user'. Multiple user logins can be added as
a comma-separated list like this: 'myuser,youruser'.

*********************************************************************************************
