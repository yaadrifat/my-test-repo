-- Author:Vivek Kumar Jha
-- check Number is or not 
create or replace
FUNCTION IS_NUMBER(p_string varchar2)

RETURN INT 
IS
 v_new_num NUMBER;
BEGIN
   v_new_num := TO_NUMBER(p_string);
   RETURN 1;
EXCEPTION
WHEN VALUE_ERROR THEN
   RETURN 0;
END IS_NUMBER;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,337,2,'02_is_number.sql',sysdate,'v9.3.0 #738');

commit;
