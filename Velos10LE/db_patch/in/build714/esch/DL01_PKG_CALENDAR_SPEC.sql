set define off;

create or replace
PACKAGE        "PKG_CALENDAR" AS
--KM-#5949
PROCEDURE sp_delete_event_visit (p_eventOrVisitIds IN ARRAY_STRING , tname IN VARCHAR2, p_flags IN ARRAY_STRING, usr IN NUMBER);

PROCEDURE sp_updteOflgEventsVisits(p_eventId IN NUMBER, p_offline_flag IN VARCHAR2);
PROCEDURE sp_hide_unhide_EventsVisits(p_eventOrVisitIds IN ARRAY_STRING,  p_hide_flag IN VARCHAR2, p_visit_flags IN ARRAY_STRING);

PROCEDURE sp_update_Pat_Schedules_now(p_calId IN NUMBER, p_chkVal IN NUMBER, p_statId IN NUMBER, p_date IN DATE, p_user IN NUMBER, p_ipAdd IN VARCHAR2);

--PROCEDURE sp_updt_unsch_evts1(p_calId in number, p_user in number, p_ipAdd in varchar2, p_evtIds IN ARRAY_STRING);
PROCEDURE sp_updt_unsch_evts1(p_patId IN NUMBER, p_patProtId IN NUMBER, p_calId IN NUMBER, p_user IN NUMBER, p_ipAdd IN VARCHAR2, p_evtIds IN ARRAY_STRING);


FUNCTION f_get_event_sequence(p_visit_id NUMBER, p_event_name VARCHAR2, p_table VARCHAR2) RETURN NUMBER;

----no need of this function-----
FUNCTION givemeevent_count(p_event_id IN NUMBER) RETURN NUMBER;

/* procedure to add additional milestones when anunscheduled event is created
Sonia Abrol
08/20/08
*/

PROCEDURE sp_add_additionaMS(p_event in number) ;


PROCEDURE sp_updateCurrentStatus(p_eventId IN number);

procedure sp_regenerate_sch_mails(p_calId IN NUMBER,p_patprot in number, p_user IN NUMBER, p_ipAdd IN VARCHAR2,p_patprot_stat in number,p_per in number,
p_calassoc Varchar2 ,p_protocol_study number );

procedure sp_update_sch_eventseq(p_calId IN NUMBER,p_patprot in number, p_user IN NUMBER, p_ipAdd IN VARCHAR2 );

procedure sp_insert_event2SCH(p_calId in number,p_patprot in number,p_eventid in number,p_PATPROT_START in date,
p_per in number,p_dmlby in number,p_ipadd in varchar2,p_notdone in number,p_curr_disc_status in number);

END pkg_calendar ;
/


