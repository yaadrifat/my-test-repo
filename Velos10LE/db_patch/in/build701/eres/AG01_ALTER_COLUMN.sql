SET DEFINE OFF;

-- Increasing the column size so no data loss issues expected


ALTER TABLE EPAT.PAT_PERID MODIFY (PERID_ID VARCHAR2(4000));

ALTER TABLE ERES.ER_STUDYID MODIFY (STUDYID_ID VARCHAR2(4000));

/