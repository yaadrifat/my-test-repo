create or replace PACKAGE BODY   "ESCH"."PKG_BGT"
AS
/* author : Arvind Kumar
   date: 2nd April, 2002
   modified by : sonia sahni
   date: 8 April,2002
*/
  PROCEDURE sp_study_bgtsection (
     P_BUDGET IN NUMBER,
 P_DMLBY  IN   NUMBER,
     P_IPADD  IN   VARCHAR2
   )
 AS
   v_bgtcal NUMBER;
   v_subtype VARCHAR2(15);
   v_section VARCHAR2(250);
   v_seq NUMBER;
   v_bgtsection NUMBER;
   BEGIN
/*get default sections corresponding to the Study */
      SELECT pk_bgtcal
      INTO v_bgtcal
      FROM SCH_BGTCAL
      WHERE fk_budget = P_BUDGET ;
      FOR i IN (SELECT pk_codelst,codelst_subtyp,codelst_desc,CODELST_SEQ FROM SCH_CODELST WHERE codelst_type='studybgt_sec')
     LOOP
   v_section := i.codelst_desc;
   v_seq := i.CODELST_SEQ * 10;
   v_subtype := i.codelst_subtyp;
   SELECT SEQ_SCH_BGTSECTION.NEXTVAL
   INTO v_bgtsection
   FROM dual;
   INSERT INTO SCH_BGTSECTION
    (PK_BUDGETSEC,
     FK_BGTCAL ,
     BGTSECTION_NAME  ,
     BGTSECTION_DELFLAG,
     BGTSECTION_SEQUENCE,
 CREATOR,
 LAST_MODIFIED_BY,
 LAST_MODIFIED_DATE,
 CREATED_ON,
 IP_ADD)
     VALUES
   (v_bgtsection,
    v_bgtcal ,
    v_section,
    'N',
    v_seq,
    P_DMLBY,
    NULL,
    SYSDATE,
    SYSDATE,
    P_IPADD
   );
/*Add the line items corrosponding to the each default section for Budget associated with the study
*/
   INSERT INTO SCH_LINEITEM
    (PK_LINEITEM,
     FK_BGTSECTION ,
 LINEITEM_NAME  ,
     LINEITEM_DESC  ,
     LINEITEM_SPONSORUNIT ,
     LINEITEM_CLINICNOFUNIT,
     LINEITEM_OTHERCOST,
     LINEITEM_DELFLAG,
 CREATOR,
 LAST_MODIFIED_BY,
 LAST_MODIFIED_DATE,
 CREATED_ON,
 IP_ADD)
   (SELECT
    SEQ_SCH_LINEITEM.NEXTVAL,
    v_bgtsection ,
    trim(c.CODELST_DESC),
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    P_DMLBY,
    NULL,
    NULL,--SYSDATE,--JM: 16Jan2008 : #3010
    SYSDATE,
    P_IPADD
    FROM SCH_CODELST c
    WHERE c.codelst_type = 'studybgt_line' AND
    c.codelst_subtyp = v_subtype
   );
   END LOOP;
END sp_study_bgtsection;

PROCEDURE sp_pat_bgtsection (
	P_PROTOCOL IN NUMBER,
	P_PKBGTCAL IN NUMBER,
	P_PROTTYPE IN VARCHAR2,
	P_DMLBY IN NUMBER,
	P_IPADD IN VARCHAR2,
	P_SOURCE IN VARCHAR2
   )
 AS
   v_bgtcal NUMBER;
   v_seq NUMBER;
   v_bgtsection NUMBER;
   v_protid NUMBER;
   v_codeid NUMBER;
   v_visit_id NUMBER;
   v_oldvisit_id NUMBER;
   v_prottype CHAR(1);
   v_inteval VARCHAR2(100);
   v_visit VARCHAR2(100);
   v_visit_name VARCHAR2(4000); --SV, 10/18/04, for cal-enh
   v_oldvisit_name VARCHAR2(4000);
   v_eventid  CHAR(10);
   v_oldvisit VARCHAR2(100);
   v_oldinteval VARCHAR2(100);
   v_evname VARCHAR2(4000);

   TYPE cur_bgt IS REF CURSOR;
   cursor_bgt cur_bgt;
   v_sql long; --SV, 10/30/04, this wasn't sufficient for the SQL I added for cal-enh and thus was generating value or numeric error!!
   v_budget NUMBER;
   v_code_stdcare NUMBER;
   v_unitcost VARCHAR2(20);
   v_stdcare VARCHAR2(20);
   v_total_visits NUMBER := 0;
   v_cpt VARCHAR2(255);  --Added by IA 11.03.2006
   v_category NUMBER;
   v_event_sequence number;
   v_event_line_category number;
   v_event_desc varchar2(4000);
   v_cost_count Number;

   v_budget_study Number;
   v_budget_org Number;
   v_budget_calendar Number;
   v_budget_org_patNO Number;
   v_applyindirects number;
   v_soc_costpk  number;
   v_vis_displacement number;

  maxsection number;
  minsection number;
BEGIN
 /*
  Author: Sonia Sahni
  Purpose: Adds the sections from the base budget calendar and then creates sections from the events and visits of the calendar
*/
   v_protid := P_PROTOCOL;
   v_prottype := P_PROTTYPE;
   v_bgtcal := P_PKBGTCAL;
   SELECT fk_budget INTO v_budget
       FROM SCH_BGTCAL
   WHERE pk_bgtcal = P_PKBGTCAL;
   v_seq := 0;
   
   if (P_SOURCE != 'C') then
	   --Copy the sections of BASE calendar
	   sp_copy_base_sections(V_BUDGET, P_PKBGTCAL, P_DMLBY, P_IPADD, v_seq,1);
   end if;

   select nvl(fk_study,0),nvl(fk_site,0), nvl(BUDGET_CALENDAR,0)
   into v_budget_study,v_budget_org, v_budget_calendar
   from sch_budget where pk_budget = V_BUDGET;

   if v_budget_study > 0 and v_budget_org >0 then
    -- get study site local sample size

    begin
        select nvl(STUDYSITE_LSAMPLESIZE,1)
        into v_budget_org_patNO
        from er_studysites
        where fk_study = v_budget_study and fk_site =  v_budget_org;
    exception when no_data_found then
        v_budget_org_patNO := 1;
    end;

   else

        v_budget_org_patNO := 1;
   end if;


   SELECT pk_codelst INTO v_codeid FROM SCH_CODELST WHERE LOWER(codelst_type)='cost_desc'
      AND LOWER(codelst_subtyp)='res_cost';



  begin
      SELECT pk_codelst INTO v_soc_costpk
      FROM SCH_CODELST WHERE LOWER(codelst_type)='cost_desc'
            AND trim(LOWER(codelst_subtyp))='stdcare_cost';
  exception when no_data_found then
    v_soc_costpk := 0;
 end;

    SELECT COUNT(*) TOTAL_VISITS
    INTO v_total_visits
    FROM SCH_PROTOCOL_VISIT
    WHERE FK_PROTOCOL = v_protid;

    SELECT  pk_codelst  INTO v_category FROM  SCH_CODELST
    WHERE trim (codelst_type) = 'category' AND trim (codelst_subtyp) = 'ctgry_patcare';


    IF v_prottype = 'L' THEN

 --BK-MAR-22-11 FIX 5888
            v_sql := ' select decode(vis.num_days,0,vis.num_days,vis.displacement) displacement, def.description,def.event_line_category, def.event_sequence, vis.visit_name, ' ||
            ' event_id,name, event_cptcode as cpt,(pk_protocol_visit) as visit_id ' ||
            ' from  SCH_PROTOCOL_VISIT vis left outer join EVENT_DEF def '||
            ' on  vis.pk_protocol_visit = def.fk_visit '||
            ' , (select min(displacement) mindisp ' ||
            ' from  EVENT_DEF where chain_id  = ' || v_protid ||' and ' ||
            ' EVENT_TYPE=''A'') d  ' ||
            ' WHERE  '||
            ' ((CHAIN_ID = '|| v_protid ||' and EVENT_TYPE=''A'') OR FK_PROTOCOL = '|| v_protid ||') '||
            ' order by vis.displacement,vis.num_days,vis.pk_protocol_visit,def.event_sequence' ;

    ELSE


            v_sql := 'SELECT  decode(vis.num_days,0,vis.num_days,vis.displacement) displacement, assoc.description,assoc.event_line_category,assoc.event_sequence, vis.visit_name, ' ||
            ' event_id,name, event_cptcode as cpt,(pk_protocol_visit) as visit_id  ' ||
            ' FROM SCH_PROTOCOL_VISIT vis left outer join EVENT_ASSOC assoc '||
            ' on vis.pk_protocol_visit = assoc.fk_visit '||
            ' , (select min(displacement) mindisp ' ||
            ' from  EVENT_ASSOC where chain_id  = ' || v_protid ||' and ' ||
              ' EVENT_TYPE=''A'' and  nvl(event_calassocto,''P'')=''P'' ) d  ' ||
            ' WHERE '||
            ' (( CHAIN_ID  = ' || v_protid ||' and EVENT_TYPE=''A'') OR FK_PROTOCOL = '|| v_protid ||') '||
            ' order by vis.displacement,vis.num_days,vis.pk_protocol_visit,assoc.event_sequence ' ;

    END IF;

   --INSERT INTO TMP VALUES (v_sql);
   --COMMIT;


    OPEN cursor_bgt FOR  v_sql;
    v_oldvisit_id:= 0;
    LOOP
    FETCH cursor_bgt INTO v_vis_displacement , v_event_desc, v_event_line_category , v_event_sequence,v_visit_name, v_eventid,v_evname,v_cpt,v_visit_id;
    EXIT WHEN cursor_bgt % NOTFOUND;
--        IF v_visit <> v_oldvisit THEN
        IF v_oldvisit_id <> v_visit_id THEN

            SELECT SEQ_SCH_BGTSECTION.NEXTVAL
            INTO v_bgtsection
            FROM dual;

            v_seq := v_vis_displacement ;

            IF (v_visit_name = '') THEN
                v_visit_name :=  v_seq ;
            END IF;

            --SV, 10/18/04
            --             'Month '||v_inteval || ' Visit '||v_visit,
            INSERT INTO SCH_BGTSECTION
            (PK_BUDGETSEC,
            FK_BGTCAL ,
            BGTSECTION_NAME  ,
            BGTSECTION_DELFLAG,
            BGTSECTION_SEQUENCE,
            CREATOR,
            LAST_MODIFIED_BY,
            LAST_MODIFIED_DATE,
            CREATED_ON,
            IP_ADD,FK_VISIT,bgtsection_patno)
            VALUES
            (v_bgtsection,
            v_bgtcal ,
            v_visit_name,
            NULL,
            v_seq,
            P_DMLBY,
            NULL,
            SYSDATE,
            SYSDATE,
            P_IPADD,v_visit_id,v_budget_org_patNO
            );
            --insert repeated ,all sections and per time fees section's line items
            --LINEITEM_CLINICNOFUNIT, FK_CODELST_CATEGORY
            INSERT INTO SCH_LINEITEM(PK_LINEITEM,FK_BGTSECTION,LINEITEM_NAME, LINEITEM_DESC,
            LINEITEM_DELFLAG, CREATED_ON , IP_ADD,LINEITEM_SPONSORUNIT,
            LINEITEM_CLINICNOFUNIT ,LINEITEM_OTHERCOST,
            LINEITEM_INPERSEC, LINEITEM_STDCARECOST, LINEITEM_RESCOST,
            LINEITEM_INVCOST, LINEITEM_INCOSTDISC, LINEITEM_CPTCODE, LINEITEM_REPEAT,
            LINEITEM_PARENTID, LINEITEM_APPLYINFUTURE ,LINEITEM_NOTES,FK_CODELST_CATEGORY,
            LINEITEM_TMID, LINEITEM_CDM,LINEITEM_APPLYINDIRECTS , LINEITEM_TOTALCOST, LINEITEM_SPONSORAMOUNT, LINEITEM_VARIANCE       )
            SELECT SEQ_SCH_LINEITEM.NEXTVAL,v_bgtsection,LINEITEM_NAME,LINEITEM_DESC,
            LINEITEM_DELFLAG,SYSDATE, P_IPADD ,LINEITEM_SPONSORUNIT,
            LINEITEM_CLINICNOFUNIT ,LINEITEM_OTHERCOST,
            LINEITEM_INPERSEC, LINEITEM_STDCARECOST, LINEITEM_RESCOST,
            LINEITEM_INVCOST, LINEITEM_INCOSTDISC, LINEITEM_CPTCODE, LINEITEM_REPEAT,
            PK_LINEITEM, LINEITEM_APPLYINFUTURE, LINEITEM_NOTES,FK_CODELST_CATEGORY,
            LINEITEM_TMID, LINEITEM_CDM,LINEITEM_APPLYINDIRECTS,LINEITEM_TOTALCOST, LINEITEM_SPONSORAMOUNT, LINEITEM_VARIANCE
            FROM SCH_BGTSECTION, SCH_LINEITEM
            WHERE FK_BGTCAL      =  P_PKBGTCAL AND
            PK_BUDGETSEC   = FK_BGTSECTION
            AND ( bgtsection_delflag='R' AND
            LINEITEM_REPEAT
            IN (SELECT  pk_codelst FROM SCH_CODELST
            WHERE (codelst_subtyp='allperpatfee' OR codelst_subtyp='allsec')
            AND codelst_type='bgtline_repeat'
            ));
            INSERT INTO SCH_LINEITEM(PK_LINEITEM,FK_BGTSECTION,LINEITEM_NAME, LINEITEM_DESC,
            LINEITEM_DELFLAG, CREATED_ON , IP_ADD,LINEITEM_SPONSORUNIT,
            LINEITEM_CLINICNOFUNIT ,LINEITEM_OTHERCOST,
            LINEITEM_INPERSEC, LINEITEM_STDCARECOST, LINEITEM_RESCOST,
            LINEITEM_INVCOST, LINEITEM_INCOSTDISC, LINEITEM_CPTCODE, LINEITEM_REPEAT,
            LINEITEM_PARENTID, LINEITEM_APPLYINFUTURE ,LINEITEM_NOTES,FK_CODELST_CATEGORY,
            LINEITEM_TMID, LINEITEM_CDM,LINEITEM_APPLYINDIRECTS , LINEITEM_TOTALCOST, LINEITEM_SPONSORAMOUNT, LINEITEM_VARIANCE )
            SELECT SEQ_SCH_LINEITEM.NEXTVAL,v_bgtsection,LINEITEM_NAME,LINEITEM_NOTES,
            LINEITEM_DELFLAG,SYSDATE, P_IPADD ,LINEITEM_SPONSORUNIT,
            LINEITEM_CLINICNOFUNIT ,LINEITEM_OTHERCOST,
            1, LINEITEM_STDCARECOST, LINEITEM_RESCOST,
            LINEITEM_INVCOST, LINEITEM_INCOSTDISC, LINEITEM_CPTCODE, LINEITEM_REPEAT,
            PK_LINEITEM, LINEITEM_APPLYINFUTURE, LINEITEM_NOTES,FK_CODELST_CATEGORY,
            LINEITEM_TMID, LINEITEM_CDM,LINEITEM_APPLYINDIRECTS, LINEITEM_TOTALCOST, LINEITEM_SPONSORAMOUNT, LINEITEM_VARIANCE
            FROM SCH_BGTSECTION, SCH_LINEITEM
            WHERE FK_BGTCAL      =  P_PKBGTCAL AND
            PK_BUDGETSEC   = FK_BGTSECTION
            AND
            bgtsection_personlflag=0
            AND (lineitem_inpersec=0  OR (lineitem_inpersec=2 AND
            LINEITEM_REPEAT = (SELECT  pk_codelst FROM SCH_CODELST
            WHERE codelst_subtyp='allperpatfee'
            AND codelst_type='bgtline_repeat')))
            AND bgtsection_delflag='P'
            ;
        END IF;
        --insert line item.
        v_unitcost := NULL;
        v_stdcare := NULL;


        -- loop through event costs to insert multiple line items for multiple costs

        v_cost_count := 0;

        for cst in (select eventcost_value,fk_cost_desc from sch_eventcost where fk_event=v_eventid)
        loop

            v_cost_count := v_cost_count+1;

            if (cst.fk_cost_desc = v_soc_costpk  ) then

                v_applyindirects := 0;
            else

            v_applyindirects := 1;

            end if;

            INSERT INTO SCH_LINEITEM(PK_LINEITEM,FK_BGTSECTION,LINEITEM_NAME, LINEITEM_DESC,LINEITEM_SPONSORUNIT,
            LINEITEM_DELFLAG, LINEITEM_CPTCODE, CREATOR, CREATED_ON,IP_ADD, LINEITEM_CLINICNOFUNIT, FK_CODELST_CATEGORY ,
            fk_event,lineitem_seq,FK_CODELST_COST_TYPE, LINEITEM_OTHERCOST  ,lineitem_applyindirects     )
            VALUES( SEQ_SCH_LINEITEM.NEXTVAL,v_bgtsection,v_evname,v_event_desc,cst.eventcost_value,
            'N', v_cpt, P_DMLBY,SYSDATE, P_IPADD, '1.00',nvl(v_event_line_category ,v_category),
            v_eventid,v_event_sequence,cst.fk_cost_desc,cst.eventcost_value,v_applyindirects);


        end loop;

        if v_cost_count = 0  AND v_evname is not null then -- no event cost specified,create one lineitem for research cost-0.0
                      -- BK-5121 no creation of lineitem if there is no event.Fixed for all kind of budget.7/14/10
            v_applyindirects := 1;

            INSERT INTO SCH_LINEITEM(PK_LINEITEM,FK_BGTSECTION,LINEITEM_NAME, LINEITEM_DESC,LINEITEM_SPONSORUNIT,
            LINEITEM_DELFLAG, LINEITEM_CPTCODE, CREATOR, CREATED_ON,IP_ADD, LINEITEM_CLINICNOFUNIT, FK_CODELST_CATEGORY ,
            fk_event,lineitem_seq,FK_CODELST_COST_TYPE, LINEITEM_OTHERCOST  ,lineitem_applyindirects     )
            VALUES( SEQ_SCH_LINEITEM.NEXTVAL,v_bgtsection,v_evname,v_event_desc,'0.0',
            'N', v_cpt, P_DMLBY,SYSDATE, P_IPADD, '1.00',nvl(v_event_line_category ,v_category),
            v_eventid,v_event_sequence,v_codeid,'0.0',v_applyindirects);


        end if;

     v_oldvisit_id:=v_visit_id;
        END LOOP;

  select max(BGTSECTION_SEQUENCE) into maxsection from SCH_BGTSECTION where FK_VISIT is null
  and BGTSECTION_TYPE is not null and FK_BGTCAL = P_PKBGTCAL;

  select min(BGTSECTION_SEQUENCE) into minsection from SCH_BGTSECTION where FK_VISIT is not null and FK_BGTCAL = P_PKBGTCAL;

  while (minsection <= maxsection)
  loop
    --plog.debug(pctx, '1 maxsection: '||maxsection|| ' minsection: ' || minsection );

    if maxsection < 0 then
      Update SCH_BGTSECTION set BGTSECTION_SEQUENCE = BGTSECTION_SEQUENCE + maxsection  ,
      LAST_MODIFIED_BY = P_DMLBY  ,
      last_modified_date = SYSDATE ,
      IP_ADD = P_IPADD
      where FK_VISIT is null and FK_BGTCAL = P_PKBGTCAL;
    else
      if maxsection = 0 then
        maxsection := 1;
      end if;

      Update SCH_BGTSECTION set BGTSECTION_SEQUENCE = BGTSECTION_SEQUENCE - maxsection  ,
      LAST_MODIFIED_BY = P_DMLBY  ,
      last_modified_date = SYSDATE ,
      IP_ADD = P_IPADD
      where FK_VISIT is null and FK_BGTCAL = P_PKBGTCAL;
    end if;

    select max(BGTSECTION_SEQUENCE) into maxsection from SCH_BGTSECTION where FK_VISIT is null
    and BGTSECTION_TYPE is not null and FK_BGTCAL = P_PKBGTCAL;

    select min(BGTSECTION_SEQUENCE) into minsection from SCH_BGTSECTION where FK_VISIT is not null and FK_BGTCAL = P_PKBGTCAL;
  end loop;

		--v_budget_calendar > 0 means default budget
		IF (v_budget_calendar = 0) THEN
			--Subject Cost Items will here become Budget Line-items.
	        INSERT INTO SCH_LINEITEM(PK_LINEITEM,FK_BGTSECTION,LINEITEM_NAME,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
	            LINEITEM_DELFLAG, LINEITEM_CPTCODE, CREATOR, CREATED_ON,IP_ADD, LINEITEM_CLINICNOFUNIT, FK_CODELST_CATEGORY ,
	            fk_event, lineitem_seq, FK_CODELST_COST_TYPE, LINEITEM_OTHERCOST, lineitem_applyindirects, SUBCOST_ITEM_FLAG)
	        SELECT SEQ_SCH_LINEITEM.NEXTVAL, PK_BUDGETSEC, SUBCOST_ITEM_NAME, SUBCOST_ITEM_DESC, SUBCOST_ITEM_COST,
	        'N', '', P_DMLBY, sysdate, P_IPADD, SUBCOST_ITEM_UNIT, FK_CODELST_CATEGORY,
	        PK_SUBCOST_ITEM, seq, v_codeid, NVL((to_char(SUBCOST_ITEM_COST * SUBCOST_ITEM_UNIT)), '0.0'),1,1
	        FROM ( SELECT DISTINCT PK_BUDGETSEC, SUBCOST_ITEM_NAME, SUBCOST_ITEM_NAME as SUBCOST_ITEM_DESC, SUBCOST_ITEM_COST, SUBCOST_ITEM_UNIT, FK_CODELST_CATEGORY,
	        PK_SUBCOST_ITEM, (SELECT MAX(lineitem_seq)+1 FROM SCH_LINEITEM WHERE FK_BGTSECTION = PK_BUDGETSEC) as seq
	        FROM SCH_SUBCOST_ITEM, SCH_SUBCOST_ITEM_VISIT, SCH_BGTCAL, SCH_BGTSECTION
	        WHERE PK_SUBCOST_ITEM = FK_SUBCOST_ITEM AND FK_CALENDAR = v_protid
	        AND FK_VISIT = FK_PROTOCOL_VISIT AND NVL(BGTSECTION_DELFLAG,'N') != 'Y'
	        AND PK_BGTCAL = FK_BGTCAL AND PK_BGTCAL = v_bgtcal);
		END IF;
 COMMIT;
END sp_pat_bgtsection;
----------------------------------------------------------------------------------------------------------------------------
 PROCEDURE sp_copy_base_sections (
     P_BUDGET IN NUMBER,
     P_PKBGTCAL IN NUMBER,
     P_CREATOR  IN   NUMBER,
     P_IPADD  IN   VARCHAR2,
 P_SEQ    IN OUT NUMBER,p_commitflag In Number
 )
 AS
 /*
  Author: Sonika Talwar
  Date: July 01, 04
  Purpose: Copy the sections of BASE calendar
  Input parameters: Budget Id
                  : Budget Cal Id
      : creator
      : ip address
      : section sequence
*/
   v_bgtsection NUMBER;
   v_basecal NUMBER;
   v_newpkbgtsec NUMBER;
   v_oldpkbgtsec NUMBER;
   v_seq NUMBER;
   v_rec_mapping rec_mapping   := rec_mapping(NULL,NULL);
   v_table_parent_mapping tab_mapping := tab_mapping(); --index-by table typ
   v_cnt NUMBER := 0;
   v_new_lineitem NUMBER;
   V_SUCCESS NUMBER;
    sp_head FLOAT(126);
   sp_flag CHAR(1);
   frg_benefit  FLOAT(126);
  frg_flag NUMBER(1);
  disc_amt FLOAT(126);
   disc_flag NUMBER(1);
   soc_flag NUMBER(1);
   V_GRAND_RES_COST VARCHAR2(255);
V_GRAND_RES_TOTALCOST VARCHAR2(255);
V_GRAND_SOC_COST VARCHAR2(255);
V_GRAND_SOC_TOTALCOST VARCHAR2(255);
V_GRAND_TOTAL_COST  VARCHAR2(255);
V_GRAND_TOTAL_TOTALCOST VARCHAR2(255);
V_GRAND_SALARY_COST VARCHAR2(255);
V_GRAND_SALARY_TOTALCOST VARCHAR2(255);
V_GRAND_FRINGE_COST VARCHAR2(255);
V_GRAND_FRINGE_TOTALCOST VARCHAR2(255);
V_GRAND_DISC_COST VARCHAR2(255);
V_GRAND_DISC_TOTALCOST VARCHAR2(255);
V_GRAND_IND_COST VARCHAR2(255);
V_GRAND_IND_TOTALCOST VARCHAR2(255);
V_GRAND_RES_SPONSOR VARCHAR2(255);
V_GRAND_RES_VIARIANCE VARCHAR2(255);
V_GRAND_SOC_SPONSOR VARCHAR2(255);
V_GRAND_SOC_VARIANCE VARCHAR2(255);
V_GRAND_TOTAL_SPONSOR VARCHAR2(255);
V_GRAND_TOTAL_VARIANCE VARCHAR2(255);
V_GRAND_RES_VARIANCE VARCHAR2(255);

maxsection number;
minsection number;
 BEGIN
   --find the base calendar in which prot_id is null
   SELECT pk_bgtcal
      INTO v_basecal
      FROM SCH_BGTCAL
  WHERE fk_budget = p_budget
  AND bgtcal_protid IS NULL ;
   v_seq := p_seq;
  -- Update  bgtcal % age related fields with the base calendar fields
SELECT  BGTCAL_SPONSOROHEAD, BGTCAL_SPONSORFLAG,BGTCAL_FRGBENEFIT,
  BGTCAL_FRGFLAG, BGTCAL_DISCOUNT, BGTCAL_DISCOUNTFLAG,BGTCAL_EXCLDSOCFLAG,
  GRAND_RES_COST,  GRAND_RES_TOTALCOST, GRAND_SOC_COST,  GRAND_SOC_TOTALCOST,
GRAND_TOTAL_COST, GRAND_TOTAL_TOTALCOST, GRAND_SALARY_COST,GRAND_SALARY_TOTALCOST  ,
GRAND_FRINGE_COST, GRAND_FRINGE_TOTALCOST, GRAND_DISC_COST, GRAND_DISC_TOTALCOST,
GRAND_IND_COST, GRAND_IND_TOTALCOST, GRAND_RES_SPONSOR, GRAND_RES_VARIANCE,
GRAND_SOC_SPONSOR, GRAND_SOC_VARIANCE, GRAND_TOTAL_SPONSOR, GRAND_TOTAL_VARIANCE,
GRAND_RES_VARIANCE
  INTO sp_head, sp_flag, frg_benefit,
  frg_flag, disc_amt, disc_flag,soc_flag, V_GRAND_RES_COST , V_GRAND_RES_TOTALCOST , V_GRAND_SOC_COST ,
V_GRAND_SOC_TOTALCOST, V_GRAND_TOTAL_COST,  V_GRAND_TOTAL_TOTALCOST ,
V_GRAND_SALARY_COST, V_GRAND_SALARY_TOTALCOST, V_GRAND_FRINGE_COST ,
V_GRAND_FRINGE_TOTALCOST,  V_GRAND_DISC_COST, V_GRAND_DISC_TOTALCOST ,
V_GRAND_IND_COST,  V_GRAND_IND_TOTALCOST, V_GRAND_RES_SPONSOR,
V_GRAND_RES_VIARIANCE, V_GRAND_SOC_SPONSOR, V_GRAND_SOC_VARIANCE,
V_GRAND_TOTAL_SPONSOR,  V_GRAND_TOTAL_VARIANCE, V_GRAND_RES_VARIANCE
   FROM SCH_BGTCAL WHERE PK_BGTCAL = v_basecal;
  -- update bgt calendar record
  UPDATE SCH_BGTCAL
  SET
  BGTCAL_SPONSOROHEAD = sp_head ,
  BGTCAL_SPONSORFLAG = sp_flag ,
  BGTCAL_FRGBENEFIT = frg_benefit ,
  BGTCAL_FRGFLAG = frg_flag ,
  BGTCAL_DISCOUNT = disc_amt ,
  BGTCAL_DISCOUNTFLAG  = disc_flag,
  BGTCAL_EXCLDSOCFLAG = soc_flag,
  GRAND_RES_COST = V_GRAND_RES_COST,
GRAND_RES_TOTALCOST = v_GRAND_RES_TOTALCOST ,
GRAND_SOC_COST = v_GRAND_SOC_COST ,
GRAND_SOC_TOTALCOST = v_GRAND_SOC_TOTALCOST,
GRAND_TOTAL_COST  = v_GRAND_TOTAL_COST,
GRAND_TOTAL_TOTALCOST = v_GRAND_TOTAL_TOTALCOST,
GRAND_SALARY_COST = v_GRAND_SALARY_COST,
GRAND_SALARY_TOTALCOST = v_GRAND_SALARY_TOTALCOST ,
GRAND_FRINGE_COST = v_GRAND_FRINGE_COST ,
GRAND_FRINGE_TOTALCOST = v_GRAND_FRINGE_TOTALCOST ,
GRAND_DISC_COST = v_GRAND_DISC_COST  ,
GRAND_DISC_TOTALCOST = v_GRAND_DISC_TOTALCOST  ,
GRAND_IND_COST = v_GRAND_IND_COST  ,
GRAND_IND_TOTALCOST = v_GRAND_IND_TOTALCOST  ,
GRAND_RES_SPONSOR = v_GRAND_RES_SPONSOR  ,
GRAND_SOC_SPONSOR = v_GRAND_SOC_SPONSOR  ,
GRAND_SOC_VARIANCE = v_GRAND_SOC_VARIANCE  ,
GRAND_TOTAL_SPONSOR =  v_GRAND_TOTAL_SPONSOR  ,
GRAND_TOTAL_VARIANCE = v_GRAND_TOTAL_VARIANCE  ,
GRAND_RES_VARIANCE = v_GRAND_RES_VARIANCE  ,
LAST_MODIFIED_BY = P_CREATOR   ,
last_modified_date = SYSDATE ,
IP_ADD = P_IPADD
  WHERE PK_BGTCAL = P_PKBGTCAL;
   FOR i IN (SELECT PK_BUDGETSEC, BGTSECTION_NAME, BGTSECTION_DELFLAG,
             BGTSECTION_PATNO, BGTSECTION_PERSONLFLAG,
             BGTSECTION_TYPE,BGTSECTION_NOTES, SRT_COST_TOTAL ,  SRT_COST_GRANDTOTAL ,
             SOC_COST_TOTAL , SOC_COST_GRANDTOTAL , SRT_COST_SPONSOR , SRT_COST_VARIANCE ,
             SOC_COST_SPONSOR , SOC_COST_VARIANCE
     FROM SCH_BGTSECTION
     WHERE fk_bgtcal = v_basecal
  ORDER BY BGTSECTION_SEQUENCE)
   LOOP
      -- new bgt section pk
            SELECT SEQ_SCH_BGTSECTION.NEXTVAL
         INTO v_newpkbgtsec
            FROM dual;
            v_oldpkbgtsec := i.PK_BUDGETSEC;
    v_seq := v_seq + 10;
              --insert section record
                  INSERT INTO SCH_BGTSECTION
                  (PK_BUDGETSEC, FK_BGTCAL , BGTSECTION_NAME,
                   BGTSECTION_DELFLAG, BGTSECTION_SEQUENCE,  CREATOR, CREATED_ON, IP_ADD,
                         BGTSECTION_PATNO, BGTSECTION_PERSONLFLAG, BGTSECTION_TYPE,BGTSECTION_NOTES,
                          SRT_COST_TOTAL ,  SRT_COST_GRANDTOTAL , SOC_COST_TOTAL , SOC_COST_GRANDTOTAL ,
                           SRT_COST_SPONSOR , SRT_COST_VARIANCE , SOC_COST_SPONSOR , SOC_COST_VARIANCE  )
                  VALUES
                      (v_newpkbgtsec, P_PKBGTCAL, i.BGTSECTION_NAME,
                      i.BGTSECTION_DELFLAG, v_seq, P_CREATOR,SYSDATE,P_IPADD,
                i.BGTSECTION_PATNO, i.BGTSECTION_PERSONLFLAG, i.BGTSECTION_TYPE,i.BGTSECTION_NOTES,
                    i.SRT_COST_TOTAL ,  i.SRT_COST_GRANDTOTAL , i.SOC_COST_TOTAL , i.SOC_COST_GRANDTOTAL ,
                     i.SRT_COST_SPONSOR , i.SRT_COST_VARIANCE , i.SOC_COST_SPONSOR , i.SOC_COST_VARIANCE
      );
        --insert line items
    IF NVL(i.BGTSECTION_DELFLAG,'N') = 'N' THEN -- section is not deleted
      INSERT INTO SCH_LINEITEM(PK_LINEITEM,FK_BGTSECTION,LINEITEM_NAME, LINEITEM_DESC,
           LINEITEM_DELFLAG, CREATOR ,CREATED_ON , IP_ADD,LINEITEM_SPONSORUNIT,
                       LINEITEM_CLINICNOFUNIT ,LINEITEM_OTHERCOST,
       LINEITEM_INPERSEC, LINEITEM_STDCARECOST, LINEITEM_RESCOST,
       LINEITEM_INVCOST, LINEITEM_INCOSTDISC, LINEITEM_CPTCODE, LINEITEM_REPEAT,
                    LINEITEM_PARENTID, LINEITEM_APPLYINFUTURE,LINEITEM_NOTES,FK_CODELST_CATEGORY,
             LINEITEM_TMID, LINEITEM_CDM,LINEITEM_APPLYINDIRECTS, LINEITEM_TOTALCOST,
             LINEITEM_SPONSORAMOUNT, LINEITEM_VARIANCE  ,lineitem_seq   ,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG)
                   SELECT SEQ_SCH_LINEITEM.NEXTVAL,v_newpkbgtsec,LINEITEM_NAME,LINEITEM_DESC,
           LINEITEM_DELFLAG,P_CREATOR,SYSDATE, P_IPADD ,LINEITEM_SPONSORUNIT,
                       LINEITEM_CLINICNOFUNIT ,LINEITEM_OTHERCOST,
       LINEITEM_INPERSEC, LINEITEM_STDCARECOST, LINEITEM_RESCOST,
       LINEITEM_INVCOST, LINEITEM_INCOSTDISC, LINEITEM_CPTCODE, LINEITEM_REPEAT,
                    LINEITEM_PARENTID, LINEITEM_APPLYINFUTURE, LINEITEM_NOTES,FK_CODELST_CATEGORY,
             LINEITEM_TMID, LINEITEM_CDM,LINEITEM_APPLYINDIRECTS,LINEITEM_TOTALCOST,
             LINEITEM_SPONSORAMOUNT, LINEITEM_VARIANCE,lineitem_seq,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG
         FROM SCH_LINEITEM WHERE FK_BGTSECTION = v_oldpkbgtsec ;
     ELSIF NVL(i.BGTSECTION_DELFLAG,'N') = 'P' OR NVL(i.BGTSECTION_DELFLAG,'N') = 'R' THEN -- Personnel cost section
     -- get the lineitems for personnel and repeat
            FOR LL IN (  SELECT pk_lineitem,LINEITEM_NAME,LINEITEM_DESC,
                  LINEITEM_DELFLAG,LINEITEM_SPONSORUNIT,
                        LINEITEM_CLINICNOFUNIT ,LINEITEM_OTHERCOST,
         LINEITEM_INPERSEC, LINEITEM_STDCARECOST, LINEITEM_RESCOST,
         LINEITEM_INVCOST, LINEITEM_INCOSTDISC, LINEITEM_CPTCODE, LINEITEM_REPEAT,
         LINEITEM_PARENTID, LINEITEM_APPLYINFUTURE , LINEITEM_NOTES,FK_CODELST_CATEGORY,
              LINEITEM_TMID, LINEITEM_CDM,LINEITEM_APPLYINDIRECTS, LINEITEM_TOTALCOST,
              LINEITEM_SPONSORAMOUNT, LINEITEM_VARIANCE,lineitem_seq,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG
           FROM SCH_LINEITEM WHERE FK_BGTSECTION = v_oldpkbgtsec )
       LOOP
            -- create  a mapping record for the new spl. line item.
     v_cnt := v_cnt + 1;
     SELECT SEQ_SCH_LINEITEM.NEXTVAL
     INTO v_new_lineitem FROM dual;
      INSERT INTO SCH_LINEITEM(PK_LINEITEM,FK_BGTSECTION,LINEITEM_NAME, LINEITEM_DESC,
           LINEITEM_DELFLAG, CREATOR ,CREATED_ON , IP_ADD,LINEITEM_SPONSORUNIT,
                       LINEITEM_CLINICNOFUNIT ,LINEITEM_OTHERCOST,
       LINEITEM_INPERSEC, LINEITEM_STDCARECOST, LINEITEM_RESCOST,
       LINEITEM_INVCOST, LINEITEM_INCOSTDISC, LINEITEM_CPTCODE, LINEITEM_REPEAT,
                    LINEITEM_PARENTID, LINEITEM_APPLYINFUTURE, LINEITEM_NOTES,FK_CODELST_CATEGORY,
             LINEITEM_TMID, LINEITEM_CDM ,LINEITEM_APPLYINDIRECTS, LINEITEM_TOTALCOST,
             LINEITEM_SPONSORAMOUNT, LINEITEM_VARIANCE      ,lineitem_seq  ,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG)
                VALUES ( v_new_lineitem ,v_newpkbgtsec,LL.LINEITEM_NAME,LL.LINEITEM_DESC,
           LL.LINEITEM_DELFLAG,P_CREATOR,SYSDATE, P_IPADD ,LL.LINEITEM_SPONSORUNIT,
                       LL.LINEITEM_CLINICNOFUNIT ,LL.LINEITEM_OTHERCOST,
       LL.LINEITEM_INPERSEC, LL.LINEITEM_STDCARECOST, LL.LINEITEM_RESCOST,
       LL.LINEITEM_INVCOST, LL.LINEITEM_INCOSTDISC, LL.LINEITEM_CPTCODE, LL.LINEITEM_REPEAT,
                    LL.LINEITEM_PARENTID, LL.LINEITEM_APPLYINFUTURE, LL.LINEITEM_NOTES,LL.FK_CODELST_CATEGORY,
             LL.LINEITEM_TMID, LL.LINEITEM_CDM, LL.LINEITEM_APPLYINDIRECTS , LL.LINEITEM_TOTALCOST,
             LL.LINEITEM_SPONSORAMOUNT, LL.LINEITEM_VARIANCE  ,ll.lineitem_seq,ll.FK_CODELST_COST_TYPE,LL.SUBCOST_ITEM_FLAG );
        v_rec_mapping.old_id := LL.pk_lineitem;
     v_rec_mapping.new_id := v_new_lineitem;
     v_table_parent_mapping.EXTEND;
     v_table_parent_mapping(v_cnt) := v_rec_mapping;
     v_cnt := v_table_parent_mapping.COUNT();
      END LOOP;
  END IF;
   END LOOP;--section loop

  select max(BGTSECTION_SEQUENCE) into maxsection from SCH_BGTSECTION where FK_VISIT is null
  and BGTSECTION_TYPE is not null and FK_BGTCAL = P_PKBGTCAL;

  select min(BGTSECTION_SEQUENCE) into minsection from SCH_BGTSECTION where FK_VISIT is not null and FK_BGTCAL = P_PKBGTCAL;

  --plog.debug(pctx, '2 maxsection: '||maxsection|| ' minsection: ' || minsection );

  while (minsection <= maxsection)
  loop
    if maxsection < 0 then
      Update SCH_BGTSECTION set BGTSECTION_SEQUENCE = BGTSECTION_SEQUENCE + maxsection ,
      LAST_MODIFIED_BY = P_CREATOR  ,
      last_modified_date = SYSDATE ,
      IP_ADD = P_IPADD
      where FK_VISIT is null and FK_BGTCAL = P_PKBGTCAL;
    else
      if maxsection = 0 then
        maxsection := 1;
      end if;

      Update SCH_BGTSECTION set BGTSECTION_SEQUENCE = BGTSECTION_SEQUENCE - maxsection ,
      LAST_MODIFIED_BY = P_CREATOR  ,
      last_modified_date = SYSDATE ,
      IP_ADD = P_IPADD
      where FK_VISIT is null and FK_BGTCAL = P_PKBGTCAL;
    end if;

    select max(BGTSECTION_SEQUENCE) into maxsection from SCH_BGTSECTION where FK_VISIT is null
	and BGTSECTION_TYPE is not null and FK_BGTCAL = P_PKBGTCAL;

	select min(BGTSECTION_SEQUENCE) into minsection from SCH_BGTSECTION where FK_VISIT is not null and FK_BGTCAL = P_PKBGTCAL;
  end loop;

   p_seq := v_seq;
   sp_apply_special_bgtsections(P_BUDGET ,P_PKBGTCAL ,v_table_parent_mapping,
      P_CREATOR ,
      P_IPADD  ,
   V_SUCCESS);

   if (nvl(p_commitflag,0) =1) then
    COMMIT;
   end if;

 END sp_copy_base_sections;
 ----------
 PROCEDURE sp_apply_special_bgtsections (P_BUDGET IN NUMBER,  P_PKBGTCAL IN NUMBER,
     P_MAPPING_TABLE IN tab_mapping,
      P_CREATOR  IN   NUMBER,
      P_IPADD  IN   VARCHAR2,
   P_SUCCESS   IN OUT NUMBER
 )
 AS
 v_basecal NUMBER;
 v_spl_sec NUMBER;
 v_oldid NUMBER;
 v_newid NUMBER;
 BEGIN
    -- loop through the mapping table
 FOR i IN 1..P_MAPPING_TABLE.COUNT
 LOOP
          v_oldid := (P_MAPPING_TABLE(i).old_id);
    v_newid := (P_MAPPING_TABLE(i).new_id);
    UPDATE SCH_LINEITEM A
    SET LINEITEM_PARENTID = v_newid, LAST_MODIFIED_BY = P_CREATOR,last_modified_date = SYSDATE, IP_ADD = P_IPADD
    WHERE EXISTS (SELECT FK_BGTSECTION FROM SCH_BGTSECTION, SCH_LINEITEM
           WHERE fk_bgtcal = P_PKBGTCAL AND
        FK_BGTSECTION = PK_BUDGETSEC AND
        A.FK_BGTSECTION = FK_BGTSECTION
        ) AND
    LINEITEM_PARENTID = v_oldid;
    END LOOP;
 END;
 ----------------------------------------------------------------------------------------------------------------------------
 PROCEDURE sp_pat_misc (
     P_PKBGTCAL IN NUMBER,
     P_DMLBY  IN   NUMBER,
     P_IPADD  IN   VARCHAR2,
 P_SEQ IN NUMBER
 )
 AS
 /*
  Author: Sonia Sahni
  Purpose: Adds a miscellaneous section
*/
 v_bgtsection NUMBER;
 maxsection number;
 minsection number;
 BEGIN
    select max(BGTSECTION_SEQUENCE) into maxsection from SCH_BGTSECTION where FK_VISIT is null
    and BGTSECTION_TYPE is not null and FK_BGTCAL = P_PKBGTCAL;

    select min(BGTSECTION_SEQUENCE) into minsection from SCH_BGTSECTION where FK_VISIT is not null and FK_BGTCAL = P_PKBGTCAL;

    --plog.debug(pctx, '3 maxsection: '||maxsection|| ' minsection: ' || minsection );

    while (minsection <= P_SEQ)
    loop
      if P_SEQ < 0 then
        Update SCH_BGTSECTION set BGTSECTION_SEQUENCE = BGTSECTION_SEQUENCE + (P_SEQ-1) ,
        LAST_MODIFIED_BY = P_DMLBY  ,
      	last_modified_date = SYSDATE ,
        IP_ADD = P_IPADD
        where FK_VISIT is null and FK_BGTCAL = P_PKBGTCAL;
      else
      	if maxsection = 0 then
         maxsection := 1;
        end if;

        Update SCH_BGTSECTION set BGTSECTION_SEQUENCE = BGTSECTION_SEQUENCE - (P_SEQ-1) ,
        LAST_MODIFIED_BY = P_DMLBY  ,
      	last_modified_date = SYSDATE ,
        IP_ADD = P_IPADD
        where FK_VISIT is null and FK_BGTCAL = P_PKBGTCAL;
      end if;

      select max(BGTSECTION_SEQUENCE) into maxsection from SCH_BGTSECTION where FK_VISIT is null
      and BGTSECTION_TYPE is not null and FK_BGTCAL = P_PKBGTCAL;

	  select min(BGTSECTION_SEQUENCE) into minsection from SCH_BGTSECTION where FK_VISIT is not null and FK_BGTCAL = P_PKBGTCAL;
    end loop;

     SELECT SEQ_SCH_BGTSECTION.NEXTVAL
     INTO v_bgtsection
     FROM dual;
           INSERT INTO SCH_BGTSECTION
             (PK_BUDGETSEC,
             FK_BGTCAL ,
             BGTSECTION_NAME  ,
             BGTSECTION_DELFLAG,
             BGTSECTION_SEQUENCE,
             CREATOR,
             LAST_MODIFIED_BY,
             LAST_MODIFIED_DATE,
             CREATED_ON,
             IP_ADD)
             VALUES
             (v_bgtsection,
              P_PKBGTCAL ,
             'Miscellaneous',
             NULL,
             p_seq,
             P_DMLBY,
             NULL,
             SYSDATE,
             SYSDATE,
             P_IPADD
           );
     --insert line item
     INSERT INTO SCH_LINEITEM(PK_LINEITEM,FK_BGTSECTION,LINEITEM_NAME, LINEITEM_DESC, LINEITEM_DELFLAG,
                     CREATOR                ,
                     CREATED_ON             ,
                     IP_ADD                )
       VALUES( SEQ_SCH_LINEITEM.NEXTVAL,v_bgtsection,'Misc',NULL,'N',P_DMLBY,SYSDATE, P_IPADD);
  END  sp_pat_misc;
--------------------------------------------------------------------------------------------------------------------
PROCEDURE sp_update_budgetTemplate (
     P_PKBGTCAL IN NUMBER,
     P_Modified_by  IN   NUMBER,
     P_IPADD  IN   VARCHAR2)
  AS
  /*Author:Sampada Mhalagi
  * Date: July 27,10
  * Purpose: delete budget template id from calendar row for a budget, called from trigger
  * Input parameter: Calendar PK
  * P_Modified_by: last_modified_by
  * P_ipadd: ip address
  */
  BEGIN
  --update event_assoc row to remove budget template id
 	UPDATE event_assoc e SET e.BUDGET_TEMPLATE = NULL,
 	last_modified_by = P_Modified_by,
    last_modified_date = SYSDATE, ip_add = P_IPADD
	WHERE e.EVENT_ID = P_PKBGTCAL
	AND e.chain_id IS NOT NULL AND e.EVENT_TYPE='P' ;
END sp_update_budgetTemplate;
--------------------------------------------------------------------------------------------------------------------
  /*
  Author: Sonia Sahni
  Date: 04/09/2002
  Purpose: set delete flag of all children for a budget calendar*/
 PROCEDURE sp_update_bgtcal_children (
     P_PKBGTCAL IN NUMBER,
     P_DMBY  IN   NUMBER,
     P_IPADD  IN   VARCHAR2
    )
     AS
 BEGIN
--update delflag of all line_items associated with bgtsections of given bgtcal
    UPDATE SCH_LINEITEM
    SET lineitem_delflag = 'Y',LAST_MODIFIED_BY = P_DMBY,
             LAST_MODIFIED_DATE = SYSDATE,IP_ADD = P_IPADD
    WHERE fk_bgtsection IN (SELECT pk_budgetsec FROM SCH_BGTSECTION WHERE fk_bgtcal = P_PKBGTCAL) ;

 --update delflag of all bgtsections associated with given bgtcal
    UPDATE SCH_BGTSECTION
    SET bgtsection_delflag = 'Y',LAST_MODIFIED_BY = P_DMBY,
             LAST_MODIFIED_DATE = SYSDATE,IP_ADD = P_IPADD
    WHERE fk_bgtcal = P_PKBGTCAL;

 END sp_update_bgtcal_children;
--------------------------------------------------------------------------------------------------------------------
  PROCEDURE sp_update_budget_children (
     P_PKBGT IN NUMBER,
     P_Modified_by  IN   NUMBER,
     P_IPADD  IN   VARCHAR2)
  AS
  /*Author:Sonika Talwar
  * Date: June 18,04
  * Purpose: set delete flag of all calendar children for a budget, called from trigger
  * Input parameter: Budget PK
  * P_Modified_by: last_modified_by
  * P_ipadd: ip address
  */
  BEGIN
  --set the delflag to 'Y' for all the budget calendars
    UPDATE SCH_BGTCAL
    SET bgtcal_delflag = 'Y',
        last_modified_by = P_Modified_by,
        last_modified_date = SYSDATE,
    ip_add = P_IPADD
    WHERE fk_budget = P_PKBGT ;
  END sp_update_budget_children;
------------------------------------------------------------------------------------------------------------------
   /*
  Author: Sonia Sahni
  Date: 04/10/2002
  Modified on : 04/25/2002 - check for user allocated space for bgt appendix
  Modified on 08/02/2004 - for patient budget enhancements - personnel cost, repeat items
  Purpose: Copy complete budget as new budget*/
  PROCEDURE sp_copy_budget (
  P_PKOLDBGT IN NUMBER,
     P_CREATOR  IN   NUMBER,
     P_IPADD  IN   VARCHAR2,
  P_NEWPKBGT IN NUMBER,
  P_TEMPFLAG IN CHAR,
 P_RETURN OUT NUMBER
    )
    AS
   Begin

   sp_copy_budgetcontent (     P_PKOLDBGT ,
     P_CREATOR  ,     P_IPADD  ,
 P_NEWPKBGT , P_TEMPFLAG , P_RETURN
    ) ;

   COMMIT;


  END sp_copy_budget;


 PROCEDURE sp_check_duplicate (
     P_ACCID IN NUMBER,
     P_NEWBGTNAME  IN   VARCHAR2,
     P_NEWBGTVER  IN   VARCHAR2,
     P_MODE  IN   VARCHAR2,
 P_BGTPK IN NUMBER,
 P_RETURN OUT NUMBER
    )
   AS
   /*
  Author: Sonia Sahni
*/
  v_vount NUMBER := 0;
  V_NEWBGTVER  VARCHAR2(25);
  BEGIN
--dbm_output.put_line('rahul');
  -- check for duplicacy on name and version
 IF P_MODE = 'N' THEN
   IF trim(P_NEWBGTVER) = 'null'  THEN
     SELECT COUNT(*)
     INTO v_vount
     FROM SCH_BUDGET
     WHERE trim(LOWER(BUDGET_NAME)) = trim(LOWER(P_NEWBGTNAME)) AND
     BUDGET_VERSION IS NULL AND fk_account = P_ACCID
  AND NVL(BUDGET_DELFLAG,'Z') <> 'Y';
--    V_NEWBGTVER := null;
   ELSE
     SELECT COUNT(*)
     INTO v_vount
     FROM SCH_BUDGET
     WHERE trim(LOWER(BUDGET_NAME)) = trim(LOWER(P_NEWBGTNAME)) AND
     trim(BUDGET_VERSION) = trim(P_NEWBGTVER) AND fk_account = P_ACCID
  AND NVL(BUDGET_DELFLAG,'Z') <> 'Y';
--     V_NEWBGTVER := P_NEWBGTVER;
   END IF;
     IF v_vount > 0 THEN
         P_RETURN := -2;   -- duplicate combination of version # and bud name
     RETURN;
     ELSE
        P_RETURN := 0;   -- not duplicate
     END IF;
  END IF; -- mode is New
  IF P_MODE = 'M' THEN
   IF trim(P_NEWBGTVER) = 'null'  THEN
     SELECT COUNT(*)
     INTO v_vount
     FROM SCH_BUDGET
     WHERE trim(LOWER(BUDGET_NAME)) = trim(LOWER(P_NEWBGTNAME)) AND
     BUDGET_VERSION IS NULL AND fk_account = P_ACCID AND pk_budget <> P_BGTPK
  AND NVL(BUDGET_DELFLAG,'Z') <> 'Y';
--    V_NEWBGTVER := null;
   ELSE
     SELECT COUNT(*)
     INTO v_vount
     FROM SCH_BUDGET
     WHERE trim(LOWER(BUDGET_NAME)) = trim(LOWER(P_NEWBGTNAME)) AND
     trim(BUDGET_VERSION) = trim(P_NEWBGTVER) AND fk_account = P_ACCID AND pk_budget <> P_BGTPK
  AND NVL(BUDGET_DELFLAG,'Z') <> 'Y';
--     V_NEWBGTVER := P_NEWBGTVER;
   END IF;
     IF v_vount > 0 THEN
         P_RETURN := -2;   -- duplicate combination of version # and bud name
     RETURN;
     ELSE
        P_RETURN := 0;   -- not duplicate
     END IF;
  END IF; -- mode is EDIt
 --
END sp_check_duplicate;
PROCEDURE sp_get_apndxused (
     P_ACCID IN NUMBER,
 P_RETURN OUT NUMBER
    )
    AS
 /*
  Author: Sonia Sahni
*/
   v_used NUMBER;
  BEGIN
    BEGIN
      SELECT NVL (SUM (DBMS_LOB.GETLENGTH(A.BGTAPNDX_FILEOBJ)), 0) BGT_APNDXSUM
   INTO v_used
      FROM SCH_BGTAPNDX A, SCH_BUDGET B
      WHERE B.FK_ACCOUNT = P_ACCID
      AND A.FK_BUDGET = B.PK_BUDGET AND A.BGTAPNDX_TYPE = 'F';
      EXCEPTION WHEN NO_DATA_FOUND THEN v_used := 0;
    END;
      P_RETURN :=   v_used;
  END sp_get_apndxused;
  PROCEDURE sp_poststudybgt (
     P_PKBGT IN NUMBER,
     P_CREATOR  IN   NUMBER,
     P_IPADD  IN   VARCHAR2
    )
  AS
   /*
  Author: Sonia Sahni
*/
    BEGIN
       INSERT INTO SCH_BGTCAL(PK_BGTCAL,
       FK_BUDGET,
       BGTCAL_PROTID ,
       BGTCAL_PROTTYPE,
       BGTCAL_DELFLAG,
       CREATOR,
       IP_ADD)
      VALUES  (SEQ_SCH_BGTCAL.NEXTVAL,P_PKBGT,
       NULL,NULL,'N', P_CREATOR, P_IPADD );
  Pkg_Bgt.sp_study_bgtsection (
  P_PKBGT,
  P_CREATOR,
  P_IPADD) ;
  COMMIT;
  END sp_poststudybgt ;

  PROCEDURE sp_new_budget(
   P_PKOLDBGT IN NUMBER,
 P_NEWBGTNAME  IN   VARCHAR2,
 P_NEWBGTVER  IN   VARCHAR2,
    P_CREATOR  IN   NUMBER,
 P_IPADD  IN   VARCHAR2,
 P_RETURN OUT NUMBER
 )
 AS

 BEGIN

 sp_new_budget_common(   P_PKOLDBGT , P_NEWBGTNAME  , P_NEWBGTVER  ,    P_CREATOR  ,
 P_IPADD  , P_RETURN ,NULL ,null);

 COMMIT;


 END sp_new_budget;
---
/*Procedure to create and then copy the desired budget to this budget */
PROCEDURE sp_create_copy_budget(
 p_old_budget IN NUMBER,
 p_new_budname IN VARCHAR2,
 p_new_budver IN VARCHAR2,
 p_creator IN NUMBER,
 p_ip_Add IN VARCHAR2,
 p_return OUT NUMBER
 )
AS
retPK NUMBER;
retVal NUMBER;
ret NUMBER ;
BEGIN
 Pkg_Bgt.sp_new_budget(p_old_budget,p_new_budname,p_new_budver,p_creator,p_ip_Add,retPK);
 IF(retPK > 0) THEN
     Pkg_Bgt.sp_copy_budget(p_old_budget,p_creator,p_ip_Add,retPK,'',retVal);
 END IF;
p_return := retVal;
RETURN ;
END sp_create_copy_budget;
/***************************************************************************************************************
   ** Procedure to insert lineitems depending on the repeat line item type.
   ** Author: Anu Khanna 25th June 2004
   ** Input parameter:pk of the SCH_BGTCALID
   ** Input parameter:pk of the SCH_BGTSECTIOn
   ** Input parameter:Array String of lineitem names
   ** Input parameter:Array String of lineitem descriptions
   ** Input parameter:Array String of lineitem cpt codes
   ** Input parameter:Array String of lineitem repeat type(all sections, one time fee section. all per patient fee sections)
   ** Input parameter:id of the user
   ** Input parameter:ipadd
   ** Output parameter: pk_lineitem if the procedur is successful
   **/
PROCEDURE sp_insert_repeat_line_items(p_budgetcal_id IN NUMBER ,
             p_lineitem_names IN Array_String,
             p_lineitem_descs IN Array_String,
             p_lineitem_cpts IN Array_String,
             p_lineitem_repeat IN Array_String,
       p_category IN ARRAY_STRING,
       p_tmid IN ARRAY_STRING,
       p_cdm IN ARRAY_STRING,
             p_creator IN NUMBER, p_ip_Add VARCHAR2,o_ret OUT NUMBER)
AS
v_count NUMBER;
v_repeat NUMBER;
v_lineitem_name VARCHAR2(250);
v_cpt VARCHAR2(25);
v_desc VARCHAR2(250);
v_lineitem_id NUMBER;
v_category NUMBER;
v_tmid VARCHAR2(15);
v_cdm VARCHAR2(15);
i NUMBER ;
j NUMBER;
k NUMBER;
v_repeat_type VARCHAR2(200);
v_org_pk_lineitem NUMBER;
v_pk_lineitem NUMBER;
v_pk_rep_def_section NUMBER;
BEGIN
v_count := p_lineitem_names.COUNT();
i:=1;
j:=1;
k:=1;
---Find the pk of Repeat Default section-- 15/07/04...anu..
SELECT pk_budgetsec INTO v_pk_rep_def_section
    FROM SCH_BGTSECTION s, SCH_BGTCAL c
  WHERE
  c.pk_bgtcal = s.fk_bgtcal
  AND c.pk_bgtcal = p_budgetcal_id
  AND bgtsection_delflag ='R';
WHILE i <= v_count LOOP
 v_lineitem_name := p_lineitem_names(i);
 v_cpt := p_lineitem_cpts(i);
 v_desc := p_lineitem_descs(i);
 v_repeat := TO_NUMBER(p_lineitem_repeat(i));
 v_category := TO_NUMBER(trim(p_category(i)));
 v_tmid := p_tmid(i);
 v_cdm := p_cdm(i);
 SELECT seq_sch_lineitem.NEXTVAL
      INTO v_org_pk_lineitem FROM dual ;
   ---insert the lineitem in Repeat lineitem section..
 INSERT INTO SCH_LINEITEM
 (PK_LINEITEM,FK_BGTSECTION,
      LINEITEM_DELFLAG,CREATOR,IP_ADD,
      LINEITEM_NAME,LINEITEM_DESC,
      LINEITEM_INPERSEC,LINEITEM_REPEAT,LINEITEM_CPTCODE,FK_CODELST_CATEGORY,
      LINEITEM_TMID, LINEITEM_CDM,LINEITEM_CLINICNOFUNIT)
 VALUES
 (v_org_pk_lineitem,v_pk_rep_def_section,
      'N',p_creator,p_ip_Add,
      v_lineitem_name,v_desc,
      '0',v_repeat,v_cpt,v_category,v_tmid,v_cdm, '1.00');
 ---insert the lineitem in section
/* INSERT into sch_lineitem
 (PK_LINEITEM,FK_BGTSECTION,
      LINEITEM_DELFLAG,CREATOR,IP_ADD,
      LINEITEM_NAME,LINEITEM_DESC,
      LINEITEM_INPERSEC,LINEITEM_REPEAT,LINEITEM_CPTCODE)
 values
 (v_org_pk_lineitem,p_section_id,
      'N',p_creator,p_ip_Add,
      v_lineitem_name,v_desc,
      '0',v_repeat,v_cpt);
*/
 -- find the codelst type from codelst
 BEGIN
 SELECT  CODELST_SUBTYP INTO v_repeat_type FROM SCH_CODELST WHERE pk_codelst = v_repeat AND codelst_type='bgtline_repeat';
 v_repeat_type := trim(v_repeat_type);
 EXCEPTION WHEN NO_DATA_FOUND THEN
 P('no repeat option');
 END;
 ------------------
 --- when repeat in all sections
 IF v_repeat_type = 'allsec' THEN
  --To find out all the sections in a budget
  FOR i IN (SELECT PK_BUDGETSEC  FROM  SCH_BGTSECTION
   WHERE pk_budgetsec <> v_pk_rep_def_section
   AND  fk_bgtcal = p_budgetcal_id
   AND NVL(bgtsection_delflag,'Z') <> 'P') LOOP
   SELECT seq_sch_lineitem.NEXTVAL
      INTO v_pk_lineitem FROM dual ;
   INSERT INTO SCH_LINEITEM(PK_LINEITEM,FK_BGTSECTION,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
      LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,LINEITEM_DELFLAG,
      CREATOR,IP_ADD,LINEITEM_NAME,LINEITEM_NOTES,
      LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
      LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
       LINEITEM_REPEAT,LINEITEM_PARENTID,FK_CODELST_CATEGORY,
      LINEITEM_TMID, LINEITEM_CDM,lineitem_seq,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG
)
   SELECT v_pk_lineitem,i.PK_BUDGETSEC,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
      LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,'N',
      CREATOR,IP_ADD,LINEITEM_NAME,LINEITEM_NOTES,
      LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
      LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
       '',v_org_pk_lineitem,FK_CODELST_CATEGORY,
      LINEITEM_TMID, LINEITEM_CDM,lineitem_seq,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG
   FROM SCH_LINEITEM WHERE PK_LINEITEM = v_org_pk_lineitem;
      IF SQL%FOUND THEN
              DBMS_OUTPUT.PUT_LINE ('Insert into lineitem ') ;
           ELSE
              DBMS_OUTPUT.PUT_LINE ('lineitem not found' ) ;
            o_ret :=-1;
          RETURN;
        END IF;
END LOOP;--all sec
END IF;--all sec
----------------------
--when repeat in one time fee section is selected
 IF v_repeat_type = 'allonefee' THEN
  -- To find out budget sections which are one time fee sections
  FOR j IN (SELECT PK_BUDGETSEC FROM  SCH_BGTSECTION
   WHERE pk_budgetsec <> v_pk_rep_def_section
   AND bgtsection_type = 'O'
   AND fk_bgtcal =p_budgetcal_id
   AND NVL(bgtsection_delflag,'Z') <> 'P') LOOP
   SELECT seq_sch_lineitem.NEXTVAL
      INTO v_pk_lineitem FROM dual ;
     INSERT INTO SCH_LINEITEM(PK_LINEITEM,FK_BGTSECTION,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
     LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,LINEITEM_DELFLAG,
     CREATOR,IP_ADD,LINEITEM_NAME,LINEITEM_NOTES,
     LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
     LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
      LINEITEM_REPEAT,LINEITEM_PARENTID,FK_CODELST_CATEGORY,
      LINEITEM_TMID, LINEITEM_CDM,lineitem_seq,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG
)
   SELECT v_pk_lineitem,j.PK_BUDGETSEC,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
     LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,'N',
     CREATOR,IP_ADD,LINEITEM_NAME,LINEITEM_NOTES,
     LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
     LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
      '',v_org_pk_lineitem,FK_CODELST_CATEGORY,
      LINEITEM_TMID, LINEITEM_CDM,lineitem_seq,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG
   FROM SCH_LINEITEM WHERE PK_LINEITEM = v_org_pk_lineitem;
     IF SQL%FOUND THEN
             DBMS_OUTPUT.PUT_LINE ('Insert into lineitem ') ;
          ELSE
             DBMS_OUTPUT.PUT_LINE ('lineitem not found' ) ;
           o_ret :=-1;
         RETURN;
       END IF;
 END LOOP;--one time fee
END IF;--one time fee
--------------------
----when repeat in all per patient fee is selected
 IF v_repeat_type = 'allperpatfee' THEN
  -- To find out budget sections which are per patient fee sections
  FOR k IN(SELECT PK_BUDGETSEC   ,BGTSECTION_NAME FROM  SCH_BGTSECTION
   WHERE pk_budgetsec <> v_pk_rep_def_section
    AND bgtsection_type = 'P'
   AND fk_bgtcal = p_budgetcal_id
   AND NVL(bgtsection_delflag,'Z') <> 'P') LOOP
   SELECT seq_sch_lineitem.NEXTVAL
   INTO v_pk_lineitem FROM dual ;
   INSERT INTO SCH_LINEITEM(PK_LINEITEM,FK_BGTSECTION,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
     LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,LINEITEM_DELFLAG,
     CREATOR,IP_ADD,LINEITEM_NAME,LINEITEM_NOTES,
     LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
     LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
      LINEITEM_REPEAT,LINEITEM_PARENTID,FK_CODELST_CATEGORY,
      LINEITEM_TMID, LINEITEM_CDM,lineitem_seq,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG
)
    SELECT v_pk_lineitem,k.PK_BUDGETSEC,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
     LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,'N',
     CREATOR,IP_ADD,LINEITEM_NAME,LINEITEM_NOTES,
     LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
     LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
      '',v_org_pk_lineitem,FK_CODELST_CATEGORY,
      LINEITEM_TMID, LINEITEM_CDM,lineitem_seq,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG
    FROM SCH_LINEITEM WHERE PK_LINEITEM = v_org_pk_lineitem;
     IF SQL%FOUND THEN
             DBMS_OUTPUT.PUT_LINE ('Insert into lineitem ') ;
          ELSE
             DBMS_OUTPUT.PUT_LINE ('lineitem not found' ) ;
           o_ret :=-1;
         RETURN;
       END IF;
  END LOOP;
 END IF;
 i:= i+1;
END LOOP;
COMMIT;
o_ret := v_pk_lineitem;
RETURN;
END sp_insert_repeat_line_items;
--------------------------------------------------------------------------------------------------------
/***************************************************************************************************************
   ** Procedure to insert lineitems for edit personnel cost
   ** Author: Anu Khanna 29th June 2004
   ** Input parameter:pk of the SCH_BGTCALI
   ** Input parameter:Array String of lineitem names
   ** Input parameter:Array String of lineitem rates
   ** Input parameter:Array String of lineitem notes
   ** Input parameter:id of the user
   ** Input parameter:ipadd
   ** Output parameter: pk_lineitem if the procedur is successful
   **/
PROCEDURE SP_INSERT_PERSONNEL_COST(p_budgetcal_id IN NUMBER,p_per_types IN ARRAY_STRING,
           p_rates IN ARRAY_STRING, p_inclusions IN ARRAY_STRING,
         p_notes IN ARRAY_STRING,p_category IN ARRAY_STRING,p_tmid IN ARRAY_STRING,
   p_cdm IN ARRAY_STRING,p_creator IN NUMBER,
         p_ip_Add IN VARCHAR2,o_ret_number OUT NUMBER)
AS
v_count NUMBER;
i NUMBER;
j NUMBER;
k NUMBER;
L NUMBER;
v_per_name VARCHAR2(250);
v_rate FLOAT;
v_inclusion NUMBER;
v_note VARCHAR2(1000);
v_category NUMBER;
v_tmid VARCHAR2(15);
v_cdm VARCHAR2(15);
v_personnel_sec_id NUMBER;
v_pk_lineitem NUMBER;
v_pk_def_section NUMBER;
v_pk_parent_lineitem NUMBER;
v_pk_codelst NUMBER;
v_lineitem_repeat NUMBER;
BEGIN
v_count := p_per_types.COUNT();
i := 1;
j := 1;
k := 1;
L := 1;
SELECT  PK_CODELST INTO v_pk_codelst FROM SCH_CODELST WHERE codelst_type='bgtline_repeat'
AND CODELST_SUBTYP = 'allperpatfee';
----Find the pk of default section
SELECT pk_budgetsec INTO v_pk_def_section
    FROM SCH_BGTSECTION s, SCH_BGTCAL c
  WHERE
  c.pk_bgtcal = s.fk_bgtcal
  AND c.pk_bgtcal = p_budgetcal_id
  AND bgtsection_delflag ='P';
WHILE (i<= v_count) LOOP
       v_per_name := p_per_types(i);
     v_rate := p_rates(i);
     v_inclusion := p_inclusions(i);
     v_note := p_notes(i);
  v_category := TO_NUMBER(trim(p_category(i)));
  v_tmid := p_tmid(i);
  v_cdm := p_cdm(i);
  IF(v_inclusion=2) THEN
    v_lineitem_repeat := v_pk_codelst;
  ELSE
    v_lineitem_repeat := 0;
    END IF;
    ----First insert the lineitem in default section
       SELECT seq_sch_lineitem.NEXTVAL
       INTO v_pk_parent_lineitem FROM dual ;
    INSERT INTO SCH_LINEITEM
     (pk_lineitem ,fk_bgtsection,lineitem_name,
      lineitem_sponsorunit,lineitem_inpersec,
      lineitem_notes,fk_codelst_category,
   lineitem_tmid, lineitem_cdm, creator, ip_add,lineitem_repeat, LINEITEM_CLINICNOFUNIT,lineitem_othercost)
     VALUES
      (v_pk_parent_lineitem,v_pk_def_section,v_per_name,
       v_rate, v_inclusion,
       v_note,v_category,v_tmid,v_cdm,p_creator, p_ip_Add,v_lineitem_repeat, '1.00',v_rate);
 -- TO include in personnel cost section only
     IF v_inclusion = 1 THEN
     SELECT pk_budgetsec INTO v_personnel_sec_id
       FROM SCH_BGTSECTION s, SCH_BGTCAL c
    WHERE s.fk_bgtcal=c.pk_bgtcal
    AND c.pk_bgtcal= p_budgetcal_id
    AND s.bgtsection_personlflag = 1;
    SELECT seq_sch_lineitem.NEXTVAL
      INTO v_pk_lineitem FROM dual ;
    INSERT INTO SCH_LINEITEM
    (pk_lineitem ,fk_bgtsection,lineitem_name,
     lineitem_sponsorunit,lineitem_inpersec,
     lineitem_notes,lineitem_parentid, fk_codelst_category,
   lineitem_tmid, lineitem_cdm, creator, ip_add,LINEITEM_CLINICNOFUNIT,lineitem_othercost)
     VALUES
     (v_pk_lineitem,v_personnel_sec_id,v_per_name,
      v_rate, 1,
      v_note,v_pk_parent_lineitem,v_category,v_tmid,v_cdm, p_creator, p_ip_Add,'1.00',v_rate);
 ---- To include in all sections
   ELSIF v_inclusion = 0 THEN
   FOR j IN(SELECT pk_budgetsec FROM SCH_BGTSECTION s, SCH_BGTCAL c
           WHERE s.fk_bgtcal=c.pk_bgtcal AND c.pk_bgtcal=p_budgetcal_id
       AND NVL(bgtsection_delflag,'Z') <> 'P'
       AND NVL(bgtsection_delflag,'Z') <> 'R')
    LOOP
           SELECT seq_sch_lineitem.NEXTVAL
         INTO v_pk_lineitem FROM dual ;
       INSERT INTO SCH_LINEITEM
       (pk_lineitem ,fk_bgtsection,lineitem_name,
       lineitem_sponsorunit,lineitem_inpersec,
       lineitem_notes,lineitem_parentid,fk_codelst_category,
   lineitem_tmid, lineitem_cdm, creator, ip_add, LINEITEM_CLINICNOFUNIT,lineitem_othercost)
       VALUES
       (v_pk_lineitem,j.pk_budgetsec,v_per_name,
        v_rate, 1,
        v_note,v_pk_parent_lineitem, v_category,v_tmid,v_cdm,p_creator, p_ip_Add, '1.00',v_rate);
   END LOOP ;--for j loop
  --Add in all per patient cost section .. 29/Sept/04.
  ELSIF v_inclusion = 2 THEN
  -- To find out budget sections which are per patient fee sections
  FOR L IN(SELECT PK_BUDGETSEC FROM  SCH_BGTSECTION
   WHERE bgtsection_type = 'P'
   AND fk_bgtcal = p_budgetcal_id
   AND NVL(bgtsection_delflag,'Z') <> 'P'
   AND NVL(bgtsection_delflag,'Z') <> 'R') LOOP
   SELECT seq_sch_lineitem.NEXTVAL
   INTO v_pk_lineitem FROM dual ;
   INSERT INTO SCH_LINEITEM
       (pk_lineitem ,fk_bgtsection,lineitem_name,
       lineitem_sponsorunit,lineitem_inpersec,
       lineitem_notes,lineitem_parentid,fk_codelst_category,
   lineitem_tmid, lineitem_cdm, creator, ip_add,lineitem_repeat, LINEITEM_CLINICNOFUNIT,lineitem_othercost)
       VALUES
       (v_pk_lineitem,L.pk_budgetsec,v_per_name,
        v_rate, 1,
        v_note,v_pk_parent_lineitem, v_category,v_tmid,v_cdm,p_creator, p_ip_Add,v_lineitem_repeat, '1.00',v_rate);
     END LOOP ;--for j loop
     END IF;
    --29/Sept/04.Anu..
i := i+1;
END LOOP;--while
COMMIT;
o_ret_number := v_pk_lineitem;
RETURN;
END SP_INSERT_PERSONNEL_COST;
/***************************************************************************************************************
   ** Procedure procedure to update lineitems depending on the inclusion type.
   ** Author: Anu Khanna 29th June 2004
   ** Input parameter:pk of the SCH_BGTCAL
   ** Input parameter:Array String of default section's lineitem ids
   ** Input parameter:Array String of lineitem names
   ** Input parameter:Array String of lineitem rates
   ** Input parameter:Array String of lineitem inclusion type( in all sections, or in personnel section only)
   ** Input parameter:Array String of lineitem notes
   ** Input parameter:id of the user
   ** Input parameter:ipadd
   ** Output parameter: pk_lineitem if the procedur is successful
   **/
PROCEDURE sp_update_personnel_cost( p_budgetcal_id IN NUMBER,p_parent_ids IN Array_String,
 p_personnel_types IN Array_String,p_rates IN Array_String, p_inclusions IN Array_String,
 p_notes IN Array_String,p_apply_types IN Array_String,p_category IN ARRAY_STRING,p_tmid IN ARRAY_STRING,
   p_cdm IN ARRAY_STRING,p_creator IN NUMBER,p_ip_Add IN VARCHAR2,
 o_ret_number OUT NUMBER)
AS
v_count NUMBER;
j NUMBER;
i NUMBER;
v_lineitem_name VARCHAR2(250);
v_rate FLOAT;
v_inclusion NUMBER;
v_note VARCHAR2(1000);
v_apply_type NUMBER;
v_pk_lineitem NUMBER;
v_parent_id NUMBER;
v_category NUMBER;
v_tmid VARCHAR2(15);
v_cdm VARCHAR2(15);
v_def_old_incl NUMBER;
v_repeat NUMBER;
v_pk_codelst NUMBER;
BEGIN
	SELECT  PK_CODELST INTO v_pk_codelst FROM SCH_CODELST WHERE codelst_type='bgtline_repeat'
	AND CODELST_SUBTYP = 'allperpatfee';
	v_count := p_parent_ids.COUNT();
	i := 1;
	j := 1;
	WHILE( i <= v_count) LOOP
		v_parent_id := p_parent_ids(i);
		v_lineitem_name := p_personnel_types(i);
		v_rate := p_rates(i);
		v_inclusion := p_inclusions(i);
		v_note := p_notes(i);
		v_category := TO_NUMBER(trim(p_category(i)));
		v_tmid := p_tmid(i);
		v_cdm := p_cdm(i);
		v_apply_type := TO_NUMBER(trim(p_apply_types(i)));

		IF(v_inclusion = 2) THEN --All Per Patient Fee sections
			v_repeat := v_pk_codelst;
		ELSE
			v_repeat := 0;
		END IF;

		---update lineitem of the default section.
		SELECT lineitem_inpersec INTO v_def_old_incl FROM SCH_LINEITEM
		WHERE pk_lineitem = v_parent_id;
		BEGIN
			UPDATE SCH_LINEITEM
			SET
			lineitem_name = v_lineitem_name,
			lineitem_sponsorunit = v_rate,
			lineitem_notes = v_note,
			fk_codelst_category = v_category,
			lineitem_repeat = v_repeat,
			lineitem_tmid = v_tmid,
			lineitem_cdm = v_cdm,
			lineitem_inpersec = v_inclusion,
			last_modified_by = p_creator,
      		last_modified_date = SYSDATE,
			ip_add = p_ip_Add,
			lineitem_applyinfuture = v_apply_type,LINEITEM_CLINICNOFUNIT='1.00',lineitem_othercost=v_rate
			WHERE
			pk_lineitem = v_parent_id;
			EXCEPTION  WHEN OTHERS THEN
				P('ERROR');
				o_ret_number:=-1;
				RETURN;
		END;
		-- changed by sonia 20 july 04, handle inclusion type, irrespective of 'apply type'
		IF (v_inclusion <> v_def_old_incl) THEN
			BEGIN
			--from all to only per--29thSept 04.Anu
				IF(v_inclusion = 1 AND v_def_old_incl=  0)  THEN -- only in personnel section
					--Before: All sections
					--After: Personnel Cost Section only
					DELETE
					FROM
					SCH_LINEITEM o WHERE EXISTS (SELECT * FROM
					SCH_BGTCAL c,
					SCH_BGTSECTION s,
					SCH_LINEITEM l
					WHERE c.pk_bgtcal= p_budgetcal_id AND
					s.fk_bgtcal=c.pk_bgtcal
					AND NVL(s.bgtsection_personlflag,0) <> 1
					AND NVL(s.bgtsection_delflag,'Z') <> 'P'
					AND l.fk_bgtsection=s.pk_budgetsec
					AND l.lineitem_parentid=v_parent_id AND l.pk_lineitem =
					o.pk_lineitem);
				END IF; -- for  if(v_inclusion = 1) then
				--from only per to all sections..
				IF (v_inclusion = 0 AND v_def_old_incl=  1)THEN
					--Before: Personnel Cost Section only
					--After: All sections
					-- insert lineitem records in all other sections except the default and personnel section
					INSERT INTO SCH_LINEITEM
					(pk_lineitem, fk_bgtsection, lineitem_name, lineitem_sponsorunit, lineitem_inpersec, lineitem_notes,
					lineitem_parentid,fk_codelst_category,lineitem_tmid, lineitem_cdm ,creator, ip_add,LINEITEM_CLINICNOFUNIT,lineitem_othercost )
					SELECT seq_sch_lineitem.NEXTVAL, pk_budgetsec, v_lineitem_name,v_rate,1,v_note,
					v_parent_id,v_category,
					v_tmid,v_cdm,p_creator,p_ip_Add, '1.00',v_rate
					FROM SCH_BGTCAL c ,SCH_BGTSECTION s
					WHERE c.pk_bgtcal= p_budgetcal_id AND
					s.fk_bgtcal=c.pk_bgtcal AND
					NVL(s.bgtsection_delflag,'Z') <> 'P' AND
					NVL(bgtsection_delflag,'Z') <> 'R' AND -- by sonia 21st jul 2004, the 'repeat section' should not be included
					NVL(s.bgtsection_personlflag,0) <> 1;
				END IF;
				EXCEPTION  WHEN OTHERS THEN
					P('ERROR');
					o_ret_number:=-1;
					RETURN;
			END;
			--- 29thSept 04 Anu.
			-- from all to perpatfees sections
			IF (v_inclusion = 2 AND v_def_old_incl = 0) THEN
				--Before: All sections
				--After: All Per Patient Fee sections
				--- delete all lineitems except perpatfee type.
				DELETE
				FROM
				SCH_LINEITEM WHERE pk_lineitem
				IN(SELECT pk_lineitem FROM
				SCH_BGTSECTION s,
				SCH_BGTCAL c,
				SCH_LINEITEM l
				WHERE s.fk_bgtcal=c.pk_bgtcal
				AND l.fk_bgtsection=s.pk_budgetsec
				AND c.pk_bgtcal= p_budgetcal_id
				AND NVL(s.bgtsection_delflag,'Z') <> 'R'
				AND NVL(s.bgtsection_type,'Z') <> 'P'
				AND l.lineitem_parentid=v_parent_id);
				--update all perpatfee sections
				/* if(v_apply_type = 1) then
				update sch_lineitem
				set
				lineitem_name = v_lineitem_name,
				lineitem_sponsorunit = v_rate,
				lineitem_repeat = v_repeat,
				lineitem_notes = v_note,
				fk_codelst_category = v_category,
				lineitem_tmid = v_tmid,
				lineitem_cdm = v_cdm,
				last_modified_by = p_creator,
				ip_add = p_ip_Add
				where pk_lineitem in (select
				pk_lineitem from
				sch_bgtsection s, sch_bgtcal c,
				sch_lineitem l
				where s.fk_bgtcal=c.pk_bgtcal
				and l.fk_bgtsection = s.pk_budgetsec
				and c.pk_bgtcal= p_budgetcal_id
				and nvl(s.bgtsection_type,'Z') = 'P'
				and l.lineitem_parentid=v_parent_id);
				end if;-- apply type =1.*/
			END IF;--changed from allsec to allper fee--
			-- from all per to all sec
			IF (v_inclusion = 0 AND v_def_old_incl = 2) THEN
				--Before: All Per Patient Fee sections
				--After: All sections
				--find all sections other than perpat fees--
				FOR k IN(SELECT PK_BUDGETSEC   ,BGTSECTION_NAME FROM  SCH_BGTSECTION
					WHERE NVL(bgtsection_type,'Z') <> 'P'
					AND NVL(bgtsection_delflag,'Z')<>'P'
					AND fk_bgtcal = p_budgetcal_id
					AND NVL(bgtsection_delflag,'Z') <> 'R') LOOP

					SELECT seq_sch_lineitem.NEXTVAL
					INTO v_pk_lineitem FROM dual ;

					INSERT INTO SCH_LINEITEM(PK_LINEITEM,FK_BGTSECTION,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
					LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,LINEITEM_DELFLAG,
					CREATOR,IP_ADD,LINEITEM_NAME,LINEITEM_NOTES,
					LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
					LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
					LINEITEM_REPEAT,LINEITEM_PARENTID,FK_CODELST_CATEGORY,
					LINEITEM_TMID, LINEITEM_CDM,lineitem_seq,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG)
					SELECT v_pk_lineitem,k.PK_BUDGETSEC,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
					LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,'N',
					CREATOR,IP_ADD,LINEITEM_NAME,LINEITEM_NOTES,
					LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
					LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
					'',v_parent_id,FK_CODELST_CATEGORY,LINEITEM_TMID, LINEITEM_CDM,lineitem_seq,FK_CODELST_COST_TYPE,
					SUBCOST_ITEM_FLAG
					FROM SCH_LINEITEM WHERE PK_LINEITEM = v_parent_id;
					IF SQL%FOUND THEN
						DBMS_OUTPUT.PUT_LINE ('Insert into lineitem ') ;
					ELSE
						DBMS_OUTPUT.PUT_LINE ('lineitem not found' ) ;
						RETURN;
					END IF;
				END LOOP;--for k..
				/*if(v_apply_type = 1) then
				update sch_lineitem
				set
				lineitem_name = v_lineitem_name,
				lineitem_sponsorunit = v_rate,
				lineitem_repeat = v_repeat,
				lineitem_notes = v_note,
				fk_codelst_category = v_category,
				lineitem_tmid = v_tmid,
				lineitem_cdm = v_cdm,
				last_modified_by = p_creator,
				ip_add = p_ip_Add
				where pk_lineitem in (select
				pk_lineitem from
				sch_bgtsection s, sch_bgtcal c,
				sch_lineitem l
				where s.fk_bgtcal=c.pk_bgtcal
				and l.fk_bgtsection = s.pk_budgetsec
				and c.pk_bgtcal= p_budgetcal_id
				and nvl(s.bgtsection_type,'Z') = 'P'
				and l.lineitem_parentid=v_parent_id);
				end if;--apply type =1.*/
			END IF;--changed from per fee to allsec  --
		 	-- from all per to all only personnel.
			IF (v_inclusion = 1 AND v_def_old_incl = 2) THEN
				--Before: All Per Patient Fee sections
				--After: Personnel Cost Section only
				DELETE
				FROM
				SCH_LINEITEM o WHERE EXISTS (SELECT * FROM
				SCH_BGTCAL c,
				SCH_BGTSECTION s,
				SCH_LINEITEM l
				WHERE c.pk_bgtcal= p_budgetcal_id AND
				s.fk_bgtcal=c.pk_bgtcal
				AND NVL(s.bgtsection_personlflag,0) <> 1
				AND NVL(s.bgtsection_delflag,'Z') <> 'P'
				AND l.fk_bgtsection=s.pk_budgetsec
				AND l.lineitem_parentid=v_parent_id AND l.pk_lineitem =
				o.pk_lineitem);
			END IF;
			IF (v_inclusion = 2 AND v_def_old_incl = 1) THEN
				--Before: Personnel Cost Section only
				--After: All Per Patient Fee sections
				DELETE
				FROM
				SCH_LINEITEM o WHERE EXISTS (SELECT * FROM
				SCH_BGTCAL c,
				SCH_BGTSECTION s,
				SCH_LINEITEM l
				WHERE c.pk_bgtcal= p_budgetcal_id AND
				s.fk_bgtcal=c.pk_bgtcal
				AND NVL(s.bgtsection_personlflag,0) = 1
				AND NVL(s.bgtsection_delflag,'Z') <> 'P'
				AND l.fk_bgtsection=s.pk_budgetsec
				AND l.lineitem_parentid=v_parent_id AND l.pk_lineitem =
				o.pk_lineitem);
				--insert records in all per fees section.
		  		--------------------
				FOR k IN(SELECT PK_BUDGETSEC   ,BGTSECTION_NAME FROM  SCH_BGTSECTION
					WHERE NVL(bgtsection_type,'Z') = 'P'
					AND NVL(bgtsection_delflag,'Z')<>'P'
					AND fk_bgtcal = p_budgetcal_id
					AND NVL(bgtsection_delflag,'Z') <> 'R') LOOP

					SELECT seq_sch_lineitem.NEXTVAL
					INTO v_pk_lineitem FROM dual ;

					INSERT INTO SCH_LINEITEM(PK_LINEITEM,FK_BGTSECTION,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
					LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,LINEITEM_DELFLAG,
					CREATOR,IP_ADD,LINEITEM_NAME,LINEITEM_NOTES,
					LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
					LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
					LINEITEM_REPEAT,LINEITEM_PARENTID,FK_CODELST_CATEGORY,
					LINEITEM_TMID, LINEITEM_CDM,lineitem_seq,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG)
					SELECT v_pk_lineitem,k.PK_BUDGETSEC,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
					LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,'N',
					CREATOR,IP_ADD,LINEITEM_NAME,LINEITEM_NOTES,
					LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
					LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
					'',v_parent_id,FK_CODELST_CATEGORY,LINEITEM_TMID, LINEITEM_CDM,lineitem_seq,FK_CODELST_COST_TYPE,
					SUBCOST_ITEM_FLAG
					FROM SCH_LINEITEM WHERE PK_LINEITEM = v_parent_id;
					IF SQL%FOUND THEN
						DBMS_OUTPUT.PUT_LINE ('Insert into lineitem ') ;
					ELSE
						DBMS_OUTPUT.PUT_LINE ('lineitem not found' ) ;
						RETURN;
						-------------
					END IF;
				END LOOP;
			END IF;
		END IF; --for v_inclusion <> v_def_old_incl

		-- v_apply_type = 1 All Previous and Future Personnel Costs
		IF v_apply_type = 1 AND v_inclusion = v_def_old_incl THEN
			--no change in inclusion criterion
			--update lineitems where parent id is v_parent_id.
			UPDATE SCH_LINEITEM
			SET lineitem_name = v_lineitem_name,
			lineitem_sponsorunit = v_rate,
			lineitem_notes = v_note,
			fk_codelst_category = v_category,
			lineitem_tmid = v_tmid,
			lineitem_cdm = v_cdm,
			last_modified_by = p_creator,
      		last_modified_date = SYSDATE,
			ip_add = p_ip_Add,LINEITEM_CLINICNOFUNIT='1.00',lineitem_othercost=v_rate
			WHERE
			lineitem_parentid = v_parent_id;
		ELSIF v_apply_type = 1 AND v_inclusion <> v_def_old_incl THEN
			--change in the inclusion criterion
			BEGIN
				--SM:Bug#4842 Added Case stmt to check what is the current selection
				CASE(v_inclusion)
				WHEN 1 THEN
					--included in: Personnel Cost Section only
					--- update the personnel cost section lineitem
					UPDATE SCH_LINEITEM
					SET  lineitem_name = v_lineitem_name,
					lineitem_sponsorunit = v_rate,
					lineitem_repeat = v_repeat,
					lineitem_notes = v_note,
					fk_codelst_category = v_category,
					lineitem_tmid = v_tmid,
					lineitem_cdm = v_cdm,
					last_modified_by = p_creator,
          			last_modified_date = SYSDATE,
					ip_add = p_ip_Add ,LINEITEM_CLINICNOFUNIT='1.00',lineitem_othercost=v_rate
					WHERE pk_lineitem = (SELECT
					pk_lineitem FROM
					SCH_BGTCAL c, SCH_BGTSECTION s,
					SCH_LINEITEM l
					WHERE c.pk_bgtcal= p_budgetcal_id AND
					s.fk_bgtcal=c.pk_bgtcal  AND
					s.bgtsection_personlflag = 1 AND
					l.fk_bgtsection = s.pk_budgetsec AND
					l.lineitem_parentid=v_parent_id);
				WHEN 0 THEN
					--included in: All sections
					--- update the All sections lineitem
					UPDATE SCH_LINEITEM
					SET  lineitem_name = v_lineitem_name,
					lineitem_sponsorunit = v_rate,
					lineitem_repeat = v_repeat,
					lineitem_notes = v_note,
					fk_codelst_category = v_category,
					lineitem_tmid = v_tmid,
					lineitem_cdm = v_cdm,
					last_modified_by = p_creator,
          			last_modified_date = SYSDATE,
					ip_add = p_ip_Add ,LINEITEM_CLINICNOFUNIT='1.00',lineitem_othercost=v_rate
					WHERE pk_lineitem in (SELECT
					pk_lineitem FROM
					SCH_BGTCAL c, SCH_BGTSECTION s,
					SCH_LINEITEM l
					WHERE c.pk_bgtcal= p_budgetcal_id AND
					s.fk_bgtcal=c.pk_bgtcal  AND
					l.fk_bgtsection = s.pk_budgetsec AND
					l.lineitem_parentid=v_parent_id);
				WHEN 2 THEN
					--included in: All Per Patient Fee sections
					--- update the All Per Patient Fee sections lineitem
					UPDATE SCH_LINEITEM
					SET  lineitem_name = v_lineitem_name,
					lineitem_sponsorunit = v_rate,
					lineitem_repeat = v_repeat,
					lineitem_notes = v_note,
					fk_codelst_category = v_category,
					lineitem_tmid = v_tmid,
					lineitem_cdm = v_cdm,
					last_modified_by = p_creator,
          			last_modified_date = SYSDATE,
					ip_add = p_ip_Add ,LINEITEM_CLINICNOFUNIT='1.00',lineitem_othercost=v_rate
					WHERE pk_lineitem in (SELECT
					pk_lineitem FROM
					SCH_BGTCAL c, SCH_BGTSECTION s,
					SCH_LINEITEM l
					WHERE c.pk_bgtcal= p_budgetcal_id AND
					s.fk_bgtcal=c.pk_bgtcal  AND
					NVL(s.bgtsection_type,'Z') = 'P' AND
					l.fk_bgtsection = s.pk_budgetsec AND
					l.lineitem_parentid=v_parent_id);
				END CASE;
			END;
		END IF; -- if apply_type = 1
		---Anu..
		-- end of change - by sonia 20 july 04
		/*if(v_apply_type = 1) then
		----select all lineitems where parent id is v_parent_id.
		for j in(select pk_lineitem, lineitem_inpersec
		from sch_lineitem
		where lineitem_parentid = v_parent_id) loop
		--- see whether the inclusion type is same ie inclusion type not changed
		if(v_inclusion = v_def_old_incl) then
		--if inclusion type is not changed then update all lineitems
		update sch_lineitem
		set
		lineitem_name = v_lineitem_name,
		lineitem_sponsorunit = v_rate,
		lineitem_notes = v_note,
		last_modified_by = p_creator,
		ip_add = p_ip_Add
		where
		pk_lineitem = j.pk_lineitem;
		elsif (v_inclusion <> v_def_old_incl) then
		--if inclusion type is changed from all sections to personnel section
		if(v_inclusion = 1) then
		delete
		from
		sch_lineitem where pk_lineitem
		in(select pk_lineitem from
		sch_bgtsection s,
		sch_bgtcal c,
		sch_lineitem l
		where s.fk_bgtcal=c.pk_bgtcal
		and l.fk_bgtsection=s.pk_budgetsec
		and c.pk_bgtcal= p_budgetcal_id
		and nvl(s.bgtsection_personlflag,0) <> 1
		and nvl(s.bgtsection_delflag,'Z') <> 'P'
		and l.lineitem_parentid=v_parent_id);
		--- update the personnel costs section lineitem
		update sch_lineitem
		set
		lineitem_name = v_lineitem_name,
		lineitem_sponsorunit = v_rate,
		lineitem_notes = v_note,
		lineitem_inpersec = 1,
		last_modified_by = p_creator,
		ip_add = p_ip_Add
		where pk_lineitem = (select
		pk_lineitem from
		sch_bgtsection s, sch_bgtcal c,
		sch_lineitem l
		where s.fk_bgtcal=c.pk_bgtcal
		and l.fk_bgtsection = s.pk_budgetsec
		and c.pk_bgtcal= p_budgetcal_id
		and s.bgtsection_personlflag = 1
		and l.lineitem_parentid=v_parent_id);
		end if;--inclusion is changed to 1.
		---if inclusion is changed from
		if(v_inclusion = 0) then
		--- update the personnel costs section lineitem
		update sch_lineitem
		set
		lineitem_name = v_lineitem_name,
		lineitem_sponsorunit = v_rate,
		lineitem_notes = v_note,
		lineitem_inpersec = 1,
		last_modified_by = p_creator,
		ip_add = p_ip_Add
		where pk_lineitem = (select
		pk_lineitem from
		sch_bgtsection s, sch_bgtcal c,
		sch_lineitem l
		where s.fk_bgtcal=c.pk_bgtcal
		and l.fk_bgtsection = s.pk_budgetsec
		and c.pk_bgtcal= p_budgetcal_id
		and s.bgtsection_personlflag = 1
		and l.lineitem_parentid=v_parent_id);
		-- insert lineitem records in all other sections except the default and personnel section
		for k in( select pk_budgetsec  from sch_bgtsection s, sch_bgtcal c where s.fk_bgtcal=c.pk_bgtcal
		and c.pk_bgtcal= p_budgetcal_id
		and nvl(bgtsection_delflag,'Z') <> 'P'
		and nvl(s.bgtsection_personlflag,0) <> 1) loop
		select seq_sch_lineitem.nextval
		into v_pk_lineitem from dual;
		begin
		insert into sch_lineitem
		(pk_lineitem, fk_bgtsection, lineitem_name, lineitem_sponsorunit, lineitem_inpersec, lineitem_notes,
		lineitem_parentid,creator, ip_add)
		values(
		v_pk_lineitem, k.pk_budgetsec, v_lineitem_name,v_rate,1,v_note,
		v_parent_id,p_creator,p_ip_Add);
		EXCEPTION  WHEN OTHERS THEN
		p('ERROR');
		o_ret_number:=-1;
		RETURN;
		end;
		end loop;-- loop for k.
		end if; --inclusion is 0
		end if;--check inclusion
		end loop;--j loop
		end if;--if 1
		*/ --commented by sonia 20th july
		i := i+1;
	END LOOP;--end of while loop
	COMMIT;
RETURN;
END sp_update_personnel_cost;
/***************************************************************************************************************
   ** Procedure procedure to delete lineitems logically.
   ** Author: Anu Khanna 1st July 2004
   ** Input parameter:pk of the SCH_LINEITEM
   ** Input parameter:id of the last modified by user
   ** Input parameter:ipadd
   ** Output parameter: 1 is successful
*/
PROCEDURE sp_delete_lineitem_per_cost(p_lineitem_id IN NUMBER,p_apply_type IN NUMBER,p_last_modified_by IN NUMBER,p_ip_Add IN VARCHAR2,o_ret OUT NUMBER)
AS
 v_lineitem  ARRAY_STRING  := ARRAY_STRING();
 v_count NUMBER;
BEGIN

--first set the delflag to 'Y' in the default section lineitem
 BEGIN
v_count := 1 ;
 UPDATE SCH_LINEITEM
 SET
 lineitem_delflag = 'Y',
 last_modified_by = p_last_modified_by,
 last_modified_date = SYSDATE,
 ip_add = p_ip_Add
 WHERE pk_lineitem = p_lineitem_id;
 EXCEPTION  WHEN OTHERS THEN
         P('ERROR');
         o_ret:=-1;
     RETURN;
END;
--update all lineitems where parentid is p_lineitem_id if apply type is 1.
--if(p_apply_type = 1) then
FOR i IN(SELECT pk_lineitem FROM SCH_LINEITEM WHERE lineitem_parentid=p_lineitem_id) LOOP
 UPDATE SCH_LINEITEM
 SET
 lineitem_delflag = 'Y',
 last_modified_by = p_last_modified_by,
 last_modified_date = SYSDATE,
 ip_add = p_ip_Add
 WHERE pk_lineitem = i.pk_lineitem;
 v_lineitem.EXTEND;
  v_lineitem(v_count) :=  i.pk_lineitem;
 v_count := v_count + 1;
END LOOP;
--end if;
COMMIT;
 sp_clear_subTotals(v_lineitem);

o_ret := 1;
RETURN;
END sp_delete_lineitem_per_cost;
--------------
--
/***************************************************************************************************************
   ** Procedure procedure to insert all lineitems of default section where inclusion type is 'all sections''
   ** Author: Anu Khanna 2st July 2004
   ** Input parameter:pk of the SCH_BGTSECTION
   ** Input parameter:pk of the SCH_BGTCAL
   ** Input parameter:id of user
   ** Input parameter:ipadd
   ** Output parameter: 1 is successful
*/
/*PROCEDURE sp_insert_default_sec_lineitms(p_section_id IN NUMBER, p_bgtcal_id IN NUMBER, p_creator IN NUMBER,
               p_ip_add VARCHAR2, o_ret OUT NUMBER)
AS
v_pk_lineitem NUMBER;
v_bgt_type CHAR(1);
BEGIN
--select all default section's lineitems where inclusion type is all section
FOR i IN(SELECT  pk_lineitem FROM sch_lineitem l,sch_bgtsection s
       WHERE s.FK_BGTCAL= p_bgtcal_id
     AND s.BGTSECTION_DELFLAG='P'
  AND NVL(lineitem_delflag,'Z') <> 'Y'
     AND l.fk_bgtsection=s.pk_budgetsec
  ---include per time fee lineitems as well.. 29thSpt 04. Anu..
  AND (l.lineitem_inpersec=0  OR (l.lineitem_inpersec=2 AND
              l.LINEITEM_REPEAT = (SELECT  pk_codelst FROM sch_codelst
              WHERE codelst_subtyp='allperpatfee'
          AND codelst_type='bgtline_repeat')))) LOOP
     SELECT seq_sch_lineitem.NEXTVAL
     INTO v_pk_lineitem FROM dual;
     ---insert these lineitems in created section
    INSERT INTO sch_lineitem(PK_LINEITEM,FK_BGTSECTION,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
          LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,LINEITEM_DELFLAG,
          CREATOR,IP_ADD,LINEITEM_NAME,LINEITEM_NOTES,
          LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
          LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
          LINEITEM_REPEAT,LINEITEM_PARENTID,FK_CODELST_CATEGORY,
       LINEITEM_TMID, LINEITEM_CDM)
       SELECT v_pk_lineitem,p_section_id,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
          LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,'N',
          p_creator,p_ip_add,LINEITEM_NAME,LINEITEM_NOTES,
          1,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
          LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
           '',i.pk_lineitem,FK_CODELST_CATEGORY,
       LINEITEM_TMID, LINEITEM_CDM FROM sch_lineitem WHERE PK_LINEITEM = i.pk_lineitem;
          IF SQL%FOUND THEN
                  DBMS_OUTPUT.PUT_LINE ('Insert into lineitem ') ;
               ELSE
                  DBMS_OUTPUT.PUT_LINE ('lineitem not found' ) ;
                o_ret :=-1;
              RETURN;
            END IF;
END LOOP; -- for i loop
---Select the  bgtsection type of the current budget section
 SELECT bgtsection_type INTO v_bgt_type
  FROM sch_bgtsection WHERE pk_budgetsec = p_section_id;
--  v_bgt_type will have value either 'P' or "O'
IF(v_bgt_type = 'P') THEN
FOR j IN(SELECT pk_lineitem, lineitem_repeat
   FROM sch_lineitem l, sch_bgtsection s, sch_bgtcal c
   WHERE l.fk_bgtsection = s.pk_budgetsec
   AND c.pk_bgtcal = s.fk_bgtcal
   AND c.pk_bgtcal = p_bgtcal_id
   AND bgtsection_delflag='R'
   AND NVL(lineitem_delflag,'Z') <> 'Y'
   AND LINEITEM_REPEAT
    IN (SELECT  pk_codelst FROM sch_codelst
             WHERE
          (codelst_subtyp='allperpatfee' OR codelst_subtyp='allsec')
          AND codelst_type='bgtline_repeat')) LOOP
     SELECT seq_sch_lineitem.NEXTVAL
     INTO v_pk_lineitem FROM dual;
     ---insert these lineitems in created section
    INSERT INTO sch_lineitem(PK_LINEITEM,FK_BGTSECTION,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
          LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,LINEITEM_DELFLAG,
          CREATOR,IP_ADD,LINEITEM_NAME,LINEITEM_NOTES,
          LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
          LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
           LINEITEM_REPEAT,LINEITEM_PARENTID,FK_CODELST_CATEGORY,
       LINEITEM_TMID, LINEITEM_CDM)
       SELECT v_pk_lineitem,p_section_id,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
          LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,'N',
          p_creator,p_ip_add,LINEITEM_NAME,LINEITEM_NOTES,
          '',LINEITEM_STDCARECOST,LINEITEM_RESCOST,
          LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
           '',j.pk_lineitem,FK_CODELST_CATEGORY,
       LINEITEM_TMID, LINEITEM_CDM FROM sch_lineitem WHERE PK_LINEITEM = j.pk_lineitem;
          IF SQL%FOUND THEN
                  DBMS_OUTPUT.PUT_LINE ('Insert into lineitem ') ;
               ELSE
                  DBMS_OUTPUT.PUT_LINE ('lineitem not found' ) ;
                o_ret :=-1;
              RETURN;
            END IF;
END LOOP; -- end of for loop.
END IF;
IF(v_bgt_type ='O') THEN
    FOR j IN(SELECT pk_lineitem, lineitem_repeat
    FROM sch_lineitem l, sch_bgtsection s, sch_bgtcal c
    WHERE l.fk_bgtsection = s.pk_budgetsec
    AND c.pk_bgtcal = s.fk_bgtcal
    AND c.pk_bgtcal = p_bgtcal_id
    AND bgtsection_delflag='R'
    AND NVL(lineitem_delflag,'Z') <> 'Y'
 AND LINEITEM_REPEAT
    IN (SELECT  pk_codelst FROM sch_codelst
             WHERE (codelst_subtyp='allonefee' OR codelst_subtyp='allsec')
                 AND codelst_type='bgtline_repeat')) LOOP
               SELECT seq_sch_lineitem.NEXTVAL
     INTO v_pk_lineitem FROM dual;
     ---insert these lineitems in created section
    INSERT INTO sch_lineitem(PK_LINEITEM,FK_BGTSECTION,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
          LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,LINEITEM_DELFLAG,
          CREATOR,IP_ADD,LINEITEM_NAME,LINEITEM_NOTES,
          LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
          LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
           LINEITEM_REPEAT,LINEITEM_PARENTID,FK_CODELST_CATEGORY,
       LINEITEM_TMID, LINEITEM_CDM)
       SELECT v_pk_lineitem,p_section_id,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
          LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,'N',
          p_creator,p_ip_add,LINEITEM_NAME,LINEITEM_NOTES,
          '',LINEITEM_STDCARECOST,LINEITEM_RESCOST,
          LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
           '',j.pk_lineitem,FK_CODELST_CATEGORY,
       LINEITEM_TMID, LINEITEM_CDM FROM sch_lineitem WHERE PK_LINEITEM = j.pk_lineitem;
          IF SQL%FOUND THEN
                  DBMS_OUTPUT.PUT_LINE ('Insert into lineitem ') ;
               ELSE
                  DBMS_OUTPUT.PUT_LINE ('lineitem not found' ) ;
                o_ret :=-1;
              RETURN;
            END IF;
END LOOP;--end of for loop.
END IF;
COMMIT;
o_ret := 1;
RETURN;
END;*/
---------15/July/04---------
/***************************************************************************************************************
   ** Procedure procedure to update all repaet line items''
   ** Author: Anu Khanna 15/July/04-
   ** Input parameter:pk of the SCH_BGTCAL
   ** Input parameter:Array String of default repeat section's lineitem ids
   ** Input parameter:Array String of lineitem names
   ** Input parameter:Array String of lineitem descs
   ** Input parameter:Array String of lineitem cpts)
   ** Input parameter:Array String of lineitem repeat type
   ** Input parameter:Array String of lineitem apply type
   ** Input parameter:id of the user
   ** Input parameter:ipadd
   ** Output parameter: 1 is successful
*/
PROCEDURE sp_update_repeat_line_items(p_budgetcal_id IN NUMBER ,
           p_parent_ids IN Array_String,
           p_lineitem_names IN Array_String,
           p_lineitem_descs IN Array_String,
           p_lineitem_cpts IN Array_String,
           p_lineitem_repeat IN Array_String,
           p_apply_types IN Array_String,
     p_category IN Array_String,
     p_tmid IN Array_String,
     p_cdm IN Array_String,
           p_creator IN NUMBER, p_ip_Add VARCHAR2,o_ret OUT NUMBER)
AS
v_count NUMBER;
i NUMBER;
j NUMBER;
v_parent_id NUMBER;
v_lineitem_name VARCHAR2(250);
v_desc VARCHAR2(250);
v_cpt VARCHAR2(25);
v_repeat VARCHAR2(200);
v_apply_type NUMBER;
v_def_old_repeat VARCHAR2(200);
v_repeat_org_type VARCHAR2(15);
v_repeat_new_type VARCHAR2(15);
v_pk_rep_def_section NUMBER;
v_pk_lineitem NUMBER;
v_category NUMBER;
v_tmid VARCHAR2(15);
v_cdm VARCHAR2(15);
BEGIN
v_count := p_parent_ids.COUNT();
i := 1;
j := 1;
   SELECT pk_budgetsec INTO v_pk_rep_def_section
    FROM SCH_BGTSECTION s, SCH_BGTCAL c
  WHERE
  c.pk_bgtcal = s.fk_bgtcal
  AND c.pk_bgtcal = p_budgetcal_id
  AND NVL(bgtsection_delflag,'Z') ='R';
WHILE( i <= v_count) LOOP
    v_parent_id := p_parent_ids(i);
    v_lineitem_name := p_lineitem_names(i);
    v_desc := p_lineitem_descs(i);
    v_cpt := p_lineitem_cpts(i);
    v_repeat := p_lineitem_repeat(i);
    v_apply_type := TO_NUMBER(trim(p_apply_types(i)));
 v_category := TO_NUMBER(trim(p_category(i)));
 v_tmid := p_tmid(i);
 v_cdm := p_cdm(i);
    --- select original repeat type--
    SELECT lineitem_repeat INTO v_def_old_repeat FROM SCH_LINEITEM
     WHERE pk_lineitem = v_parent_id;
    ---update lineitem of the default section.
    BEGIN
    UPDATE SCH_LINEITEM
    SET
    lineitem_name = v_lineitem_name,
    lineitem_desc = v_desc,
    lineitem_repeat = v_repeat,
    lineitem_cptcode = v_cpt,
    last_modified_by = p_creator,
    last_modified_date = SYSDATE,
    ip_add = p_ip_Add,
    lineitem_applyinfuture = v_apply_type,
 fk_codelst_category = v_category,
    lineitem_tmid = v_tmid,
    lineitem_cdm = v_cdm
    WHERE
     pk_lineitem = v_parent_id;
  EXCEPTION  WHEN OTHERS THEN
         P('ERROR');
         o_ret:=-1;
     RETURN;
    END;
--anu if(v_apply_type = 1) then
   BEGIN
 SELECT  CODELST_SUBTYP INTO v_repeat_org_type FROM SCH_CODELST WHERE pk_codelst = v_def_old_repeat
         AND codelst_type='bgtline_repeat';
 v_repeat_org_type := trim(v_repeat_org_type);
 EXCEPTION WHEN NO_DATA_FOUND THEN
 P('no repeat option');
 END;
 BEGIN
 SELECT  CODELST_SUBTYP INTO v_repeat_new_type FROM SCH_CODELST WHERE pk_codelst = v_repeat
         AND codelst_type='bgtline_repeat';
 v_repeat_new_type := trim(v_repeat_new_type);
 EXCEPTION WHEN NO_DATA_FOUND THEN
 P('no repeat option');
 END;
 IF(v_repeat_org_type = v_repeat_new_type AND v_apply_type = 1) THEN
 UPDATE SCH_LINEITEM
   SET
    lineitem_name = v_lineitem_name,
      lineitem_desc = v_desc,
      lineitem_repeat = v_repeat,
      lineitem_cptcode = v_cpt,
      last_modified_by = p_creator,
      last_modified_date = SYSDATE,
   fk_codelst_category = v_category,
      lineitem_tmid = v_tmid,
      lineitem_cdm = v_cdm,
      ip_add = p_ip_Add
      WHERE
      lineitem_parentid=v_parent_id;
 END IF;--when repeat type is not changed.
 IF (v_repeat_org_type = 'allsec' AND v_repeat_new_type = 'allperpatfee') THEN
  --- delete all lineitems except perpatfee type.
     DELETE
   FROM
   SCH_LINEITEM WHERE pk_lineitem
       IN(SELECT pk_lineitem FROM
       SCH_BGTSECTION s,
       SCH_BGTCAL c,
       SCH_LINEITEM l
       WHERE s.fk_bgtcal=c.pk_bgtcal
       AND l.fk_bgtsection=s.pk_budgetsec
       AND c.pk_bgtcal= p_budgetcal_id
       AND NVL(s.bgtsection_delflag,'Z') <> 'R'
       AND NVL(s.bgtsection_type,'Z') <> 'P'
       AND l.lineitem_parentid=v_parent_id);
   --update all perpatfee sections
   IF(v_apply_type = 1) THEN
   UPDATE SCH_LINEITEM
   SET
   lineitem_name = v_lineitem_name,
      lineitem_desc = v_desc,
      lineitem_repeat = v_repeat,
      lineitem_cptcode = v_cpt,
      last_modified_by = p_creator,
      last_modified_date = SYSDATE,
   fk_codelst_category = v_category,
      lineitem_tmid = v_tmid,
      lineitem_cdm = v_cdm,
      ip_add = p_ip_Add
      WHERE pk_lineitem IN (SELECT
       pk_lineitem FROM
       SCH_BGTSECTION s, SCH_BGTCAL c,
       SCH_LINEITEM l
       WHERE s.fk_bgtcal=c.pk_bgtcal
       AND l.fk_bgtsection = s.pk_budgetsec
       AND c.pk_bgtcal= p_budgetcal_id
       AND NVL(s.bgtsection_type,'Z') = 'P'
       AND l.lineitem_parentid=v_parent_id);
  END IF;-- apply type =1.
 END IF;--changed from allsec to allper fee--
 IF (v_repeat_org_type = 'allsec' AND v_repeat_new_type = 'allonefee') THEN
 --- delete all lineitems except onetimefee type.
     DELETE
   FROM
   SCH_LINEITEM WHERE pk_lineitem
       IN(SELECT pk_lineitem FROM
       SCH_BGTSECTION s,
       SCH_BGTCAL c,
       SCH_LINEITEM l
       WHERE s.fk_bgtcal=c.pk_bgtcal
       AND l.fk_bgtsection=s.pk_budgetsec
       AND c.pk_bgtcal= p_budgetcal_id
       AND NVL(s.bgtsection_delflag,'Z') <> 'R'
       AND NVL(s.bgtsection_type,'Z') <> 'O'
       AND l.lineitem_parentid=v_parent_id);
  IF(v_apply_type = 1) THEN
   UPDATE SCH_LINEITEM
   SET
   lineitem_name = v_lineitem_name,
      lineitem_desc = v_desc,
      lineitem_repeat = v_repeat,
      lineitem_cptcode = v_cpt,
      last_modified_by = p_creator,
      last_modified_date = SYSDATE,
   fk_codelst_category = v_category,
      lineitem_tmid = v_tmid,
      lineitem_cdm = v_cdm,
      ip_add = p_ip_Add
      WHERE pk_lineitem IN (SELECT
       pk_lineitem FROM
       SCH_BGTSECTION s, SCH_BGTCAL c,
       SCH_LINEITEM l
       WHERE s.fk_bgtcal=c.pk_bgtcal
       AND l.fk_bgtsection = s.pk_budgetsec
       AND c.pk_bgtcal= p_budgetcal_id
       AND NVL(s.bgtsection_type,'Z') = 'O'
       AND l.lineitem_parentid=v_parent_id);
END IF;--apply type=1.
 END IF;--changed from allsec to allone fee--
 IF (v_repeat_org_type = 'allperpatfee' AND v_repeat_new_type = 'allsec') THEN
 --find all sections other than perpat fees--
 FOR k IN(SELECT PK_BUDGETSEC   ,BGTSECTION_NAME FROM  SCH_BGTSECTION
   WHERE pk_budgetsec <> v_pk_rep_def_section
    AND NVL(bgtsection_type,'Z') <> 'P'
    AND NVL(bgtsection_delflag,'Z')<>'P'
   AND fk_bgtcal = p_budgetcal_id
   AND NVL(bgtsection_delflag,'Z') <> 'R') LOOP
   SELECT seq_sch_lineitem.NEXTVAL
   INTO v_pk_lineitem FROM dual ;
   INSERT INTO SCH_LINEITEM(PK_LINEITEM,FK_BGTSECTION,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
     LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,LINEITEM_DELFLAG,
     CREATOR,IP_ADD,LINEITEM_NAME,LINEITEM_NOTES,
     LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
     LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
      LINEITEM_REPEAT,LINEITEM_PARENTID,FK_CODELST_CATEGORY,
      LINEITEM_TMID, LINEITEM_CDM,lineitem_seq,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG)
    SELECT v_pk_lineitem,k.PK_BUDGETSEC,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
     LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,'N',
     CREATOR,IP_ADD,LINEITEM_NAME,LINEITEM_NOTES,
     LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
     LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
      '',v_parent_id,FK_CODELST_CATEGORY,LINEITEM_TMID, LINEITEM_CDM,lineitem_seq,FK_CODELST_COST_TYPE,
      SUBCOST_ITEM_FLAG
   FROM SCH_LINEITEM WHERE PK_LINEITEM = v_parent_id;
     IF SQL%FOUND THEN
             DBMS_OUTPUT.PUT_LINE ('Insert into lineitem ') ;
          ELSE
             DBMS_OUTPUT.PUT_LINE ('lineitem not found' ) ;
           o_ret :=-1;
         RETURN;
       END IF;
  END LOOP;--for k..
  IF(v_apply_type = 1) THEN
  UPDATE SCH_LINEITEM
   SET
   lineitem_name = v_lineitem_name,
      lineitem_desc = v_desc,
      lineitem_repeat = v_repeat,
      lineitem_cptcode = v_cpt,
      last_modified_by = p_creator,
      last_modified_date = SYSDATE,
   fk_codelst_category = v_category,
      lineitem_tmid = v_tmid,
      lineitem_cdm = v_cdm,
      ip_add = p_ip_Add
      WHERE pk_lineitem IN (SELECT
       pk_lineitem FROM
       SCH_BGTSECTION s, SCH_BGTCAL c,
       SCH_LINEITEM l
       WHERE s.fk_bgtcal=c.pk_bgtcal
       AND l.fk_bgtsection = s.pk_budgetsec
       AND c.pk_bgtcal= p_budgetcal_id
       AND NVL(s.bgtsection_type,'Z') = 'P'
       AND l.lineitem_parentid=v_parent_id);
 END IF;--apply type =1.
 END IF;--changed from allper fee to allsec  --
 IF (v_repeat_org_type = 'allonefee' AND v_repeat_new_type = 'allsec') THEN
 --find all sections other than perpat fees--
 FOR k IN(SELECT PK_BUDGETSEC   ,BGTSECTION_NAME FROM  SCH_BGTSECTION
   WHERE pk_budgetsec <> v_pk_rep_def_section
    AND NVL(bgtsection_type,'Z') <> 'O'
     AND NVL(bgtsection_delflag,'Z')<>'P'
   AND fk_bgtcal = p_budgetcal_id
   AND NVL(bgtsection_delflag,'Z') <> 'R') LOOP
   SELECT seq_sch_lineitem.NEXTVAL
   INTO v_pk_lineitem FROM dual ;
   INSERT INTO SCH_LINEITEM(PK_LINEITEM,FK_BGTSECTION,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
     LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,LINEITEM_DELFLAG,
     CREATOR,IP_ADD,LINEITEM_NAME,LINEITEM_NOTES,
     LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
     LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
      LINEITEM_REPEAT,LINEITEM_PARENTID,FK_CODELST_CATEGORY,
      LINEITEM_TMID, LINEITEM_CDM,lineitem_seq,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG)
    SELECT v_pk_lineitem,k.PK_BUDGETSEC,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
     LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,'N',
     CREATOR,IP_ADD,LINEITEM_NAME,LINEITEM_NOTES,
     LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
     LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
      '',v_parent_id,FK_CODELST_CATEGORY,LINEITEM_TMID, LINEITEM_CDM,lineitem_seq,FK_CODELST_COST_TYPE,
      SUBCOST_ITEM_FLAG
   FROM SCH_LINEITEM WHERE PK_LINEITEM = v_parent_id;
     IF SQL%FOUND THEN
             DBMS_OUTPUT.PUT_LINE ('Insert into lineitem ') ;
          ELSE
             DBMS_OUTPUT.PUT_LINE ('lineitem not found' ) ;
           o_ret :=-1;
         RETURN;
       END IF;
  END LOOP;--for k..
  IF(v_apply_type = 1) THEN
  UPDATE SCH_LINEITEM
   SET
   lineitem_name = v_lineitem_name,
      lineitem_desc = v_desc,
      lineitem_repeat = v_repeat,
      lineitem_cptcode = v_cpt,
      last_modified_by = p_creator,
      last_modified_date = SYSDATE,
      ip_add = p_ip_Add,
   fk_codelst_category = v_category,
      lineitem_tmid = v_tmid,
      lineitem_cdm = v_cdm
      WHERE pk_lineitem IN (SELECT
       pk_lineitem FROM
       SCH_BGTSECTION s, SCH_BGTCAL c,
       SCH_LINEITEM l
       WHERE s.fk_bgtcal=c.pk_bgtcal
       AND l.fk_bgtsection = s.pk_budgetsec
       AND c.pk_bgtcal= p_budgetcal_id
       AND NVL(s.bgtsection_type,'Z') = 'O'
       AND l.lineitem_parentid=v_parent_id);
   END IF;--apply type=1.
 END IF;--changed from allone fee to allsec  --
 IF (v_repeat_org_type = 'allperpatfee' AND v_repeat_new_type = 'allonefee') THEN
 --- delete all lineitems of perpatfee type.
     DELETE
   FROM
   SCH_LINEITEM WHERE pk_lineitem
       IN(SELECT pk_lineitem FROM
       SCH_BGTSECTION s,
       SCH_BGTCAL c,
       SCH_LINEITEM l
       WHERE s.fk_bgtcal=c.pk_bgtcal
       AND l.fk_bgtsection=s.pk_budgetsec
       AND c.pk_bgtcal= p_budgetcal_id
       AND NVL(s.bgtsection_delflag,'Z') <> 'R'
       AND NVL(s.bgtsection_type,'Z') = 'P'
       AND l.lineitem_parentid=v_parent_id);
 --find all sections of one time fees--
 FOR k IN(SELECT PK_BUDGETSEC   ,BGTSECTION_NAME FROM  SCH_BGTSECTION
   WHERE pk_budgetsec <> v_pk_rep_def_section
    AND NVL(bgtsection_type,'Z') = 'O'
    AND NVL(bgtsection_delflag,'Z')<>'P'
   AND fk_bgtcal = p_budgetcal_id
   AND NVL(bgtsection_delflag,'Z') <> 'R') LOOP
   SELECT seq_sch_lineitem.NEXTVAL
   INTO v_pk_lineitem FROM dual ;
   INSERT INTO SCH_LINEITEM(PK_LINEITEM,FK_BGTSECTION,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
     LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,LINEITEM_DELFLAG,
     CREATOR,IP_ADD,LINEITEM_NAME,LINEITEM_NOTES,
     LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
     LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
      LINEITEM_REPEAT,LINEITEM_PARENTID,FK_CODELST_CATEGORY,
      LINEITEM_TMID, LINEITEM_CDM,lineitem_seq,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG)
    SELECT v_pk_lineitem,k.PK_BUDGETSEC,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
     LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,'N',
     CREATOR,IP_ADD,LINEITEM_NAME,LINEITEM_NOTES,
     LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
     LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
      '',v_parent_id,FK_CODELST_CATEGORY,
      LINEITEM_TMID, LINEITEM_CDM ,lineitem_seq  ,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG
      FROM SCH_LINEITEM WHERE PK_LINEITEM = v_parent_id;
     IF SQL%FOUND THEN
             DBMS_OUTPUT.PUT_LINE ('Insert into lineitem ') ;
          ELSE
             DBMS_OUTPUT.PUT_LINE ('lineitem not found' ) ;
           o_ret :=-1;
         RETURN;
       END IF;
  END LOOP;--for k..
 END IF;--changed from allper fee to allone fee  --
 IF (v_repeat_org_type = 'allonefee' AND v_repeat_new_type = 'allperpatfee') THEN
 --- delete all lineitems of onetimefee type.
     DELETE
   FROM
   SCH_LINEITEM WHERE pk_lineitem
       IN(SELECT pk_lineitem FROM
       SCH_BGTSECTION s,
       SCH_BGTCAL c,
       SCH_LINEITEM l
       WHERE s.fk_bgtcal=c.pk_bgtcal
       AND l.fk_bgtsection=s.pk_budgetsec
       AND c.pk_bgtcal= p_budgetcal_id
       AND NVL(s.bgtsection_delflag,'Z') <> 'R'
       AND NVL(s.bgtsection_type,'Z') = 'O'
       AND l.lineitem_parentid=v_parent_id);
 --find all sections of per time fees--
 FOR k IN(SELECT PK_BUDGETSEC   ,BGTSECTION_NAME FROM  SCH_BGTSECTION
   WHERE pk_budgetsec <> v_pk_rep_def_section
    AND NVL(bgtsection_type,'Z') = 'P'
   AND fk_bgtcal = p_budgetcal_id
   AND NVL(bgtsection_delflag,'Z') <> 'P'
   AND NVL(bgtsection_delflag,'Z') <> 'R') LOOP
   SELECT seq_sch_lineitem.NEXTVAL
   INTO v_pk_lineitem FROM dual ;
   INSERT INTO SCH_LINEITEM(PK_LINEITEM,FK_BGTSECTION,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
     LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,LINEITEM_DELFLAG,
     CREATOR,IP_ADD,LINEITEM_NAME,LINEITEM_NOTES,
     LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
     LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
      LINEITEM_REPEAT,LINEITEM_PARENTID,FK_CODELST_CATEGORY,
      LINEITEM_TMID, LINEITEM_CDM ,lineitem_seq,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG)
    SELECT v_pk_lineitem,k.PK_BUDGETSEC,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
     LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,'N',
     CREATOR,IP_ADD,LINEITEM_NAME,LINEITEM_NOTES,
     LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
     LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
      '',v_parent_id,FK_CODELST_CATEGORY,
      LINEITEM_TMID, LINEITEM_CDM ,lineitem_seq ,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG
      FROM SCH_LINEITEM WHERE PK_LINEITEM = v_parent_id;
     IF SQL%FOUND THEN
             DBMS_OUTPUT.PUT_LINE ('Insert into lineitem ') ;
          ELSE
             DBMS_OUTPUT.PUT_LINE ('lineitem not found' ) ;
           o_ret :=-1;
         RETURN;
       END IF;
  END LOOP;--for k..
 END IF;--changed from allone fee to allper fee  --
--end if;--apply type is 1.
COMMIT;
o_ret := 1;
i := i+1;
END LOOP;--end of while loop
RETURN;
END;



--------------- new start--------------



/***************************************************************************************************************
   ** Procedure procedure to insert all lineitems of default section where inclusion type is 'all sections''
   ** Author: Anu Khanna 2st July 2004
   ** Input parameter:pk of the SCH_BGTSECTION
   ** Input parameter:pk of the SCH_BGTCAL
   ** Input parameter:id of user
   ** Input parameter:ipadd
   ** Output parameter: 1 is successful
*/
PROCEDURE sp_insert_default_sec_lineitms(p_section_id IN NUMBER, p_bgtcal_id IN NUMBER,
               p_creator IN NUMBER, p_ip_add VARCHAR2, o_ret OUT NUMBER)
AS

BEGIN
  sp_insert_def_lineitms_commit('Y', p_section_id, p_bgtcal_id, p_creator, p_ip_add, o_ret);
  return;
END;
PROCEDURE sp_insert_def_lineitms_commit(p_commit IN CHAR, p_section_id IN NUMBER, p_bgtcal_id IN NUMBER,
               p_creator IN NUMBER, p_ip_add VARCHAR2, o_ret OUT NUMBER)
AS
v_pk_lineitem NUMBER;
v_bgt_type CHAR(1);
v_repall_code NUMBER;
v_rep_per_patcode NUMBER;
v_rep_one_code NUMBER;

BEGIN

BEGIN

  SELECT  pk_codelst
  INTO v_repall_code
  FROM SCH_CODELST
  WHERE  codelst_type='bgtline_repeat' AND codelst_subtyp='allsec'  ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
 v_repall_code := NULL;
END ;

BEGIN
  SELECT  pk_codelst
  INTO v_rep_per_patcode
  FROM SCH_CODELST
     WHERE codelst_type='bgtline_repeat' AND codelst_subtyp='allperpatfee';
 EXCEPTION WHEN NO_DATA_FOUND THEN
     v_rep_per_patcode := NULL;
END;

BEGIN
   SELECT  pk_codelst
   INTO v_rep_one_code
   FROM SCH_CODELST
   WHERE codelst_type='bgtline_repeat' AND codelst_subtyp='allonefee';

 EXCEPTION WHEN NO_DATA_FOUND THEN
     v_rep_one_code  := NULL;
END;



---Select the  bgtsection type of the current budget section
  SELECT bgtsection_type INTO v_bgt_type
  FROM SCH_BGTSECTION WHERE pk_budgetsec = p_section_id;


---select pks and other info of default sections in budgetcal
FOR i IN(SELECT pk_budgetsec,bgtsection_delflag FROM SCH_BGTSECTION WHERE
         fk_bgtcal = p_bgtcal_id AND (NVL(bgtsection_delflag,'Z') ='R' OR NVL(bgtsection_delflag,'Z') ='P'))
   LOOP



   IF i.bgtsection_delflag = 'P' THEN


    INSERT INTO SCH_LINEITEM(PK_LINEITEM,FK_BGTSECTION,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
          LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,LINEITEM_DELFLAG,
          CREATOR,IP_ADD,LINEITEM_NAME,LINEITEM_NOTES,
          LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
          LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
          LINEITEM_REPEAT,LINEITEM_PARENTID,FK_CODELST_CATEGORY,
       LINEITEM_TMID, LINEITEM_CDM,lineitem_seq,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG)
    SELECT seq_sch_lineitem.NEXTVAL,p_section_id,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
          LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,'N',
          p_creator,p_ip_add,LINEITEM_NAME,LINEITEM_NOTES,
          1,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
          LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
           '',k.pk_lineitem,FK_CODELST_CATEGORY,
       LINEITEM_TMID, LINEITEM_CDM,lineitem_seq,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG
    FROM SCH_LINEITEM k
    WHERE fk_bgtsection = i.pk_budgetsec AND
    k.lineitem_inpersec=0
    AND NVL(k.lineitem_delflag,'Z') <> 'Y';

    END IF;-- if 'P'

    IF i.bgtsection_delflag = 'R' THEN


    INSERT INTO SCH_LINEITEM(PK_LINEITEM,FK_BGTSECTION,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
          LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,LINEITEM_DELFLAG,
          CREATOR,IP_ADD,LINEITEM_NAME,LINEITEM_NOTES,
          LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
          LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
          LINEITEM_REPEAT,LINEITEM_PARENTID,FK_CODELST_CATEGORY,
       LINEITEM_TMID, LINEITEM_CDM,lineitem_seq,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG)
    SELECT seq_sch_lineitem.NEXTVAL,p_section_id,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
          LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,'N',
          p_creator,p_ip_add,LINEITEM_NAME,LINEITEM_NOTES,
          -1,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
          LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
           '',k.pk_lineitem,FK_CODELST_CATEGORY,
       LINEITEM_TMID, LINEITEM_CDM,lineitem_seq,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG
    FROM SCH_LINEITEM k
    WHERE fk_bgtsection = i.pk_budgetsec
    AND LINEITEM_REPEAT = v_repall_code
    AND NVL(k.lineitem_delflag,'Z') <> 'Y' ;

     END IF; -- if 'R'

     IF(v_bgt_type ='O') THEN -- if the new section is one time fee

        INSERT INTO SCH_LINEITEM(PK_LINEITEM,FK_BGTSECTION,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
           LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,LINEITEM_DELFLAG,
           CREATOR,IP_ADD,LINEITEM_NAME,LINEITEM_NOTES,
           LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
           LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
            LINEITEM_REPEAT,LINEITEM_PARENTID,FK_CODELST_CATEGORY,
        LINEITEM_TMID, LINEITEM_CDM,lineitem_seq ,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG)
        SELECT seq_sch_lineitem.NEXTVAL,p_section_id,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
           LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,'N',
           p_creator,p_ip_add,LINEITEM_NAME,LINEITEM_NOTES,
           -1,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
           LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
            '',j.pk_lineitem,FK_CODELST_CATEGORY,
        LINEITEM_TMID, LINEITEM_CDM ,lineitem_seq ,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG
       	FROM SCH_LINEITEM j
     WHERE fk_bgtsection = i.pk_budgetsec AND
       NVL(j.lineitem_delflag,'Z') <> 'Y' AND
     j.LINEITEM_REPEAT = v_rep_one_code ;

     END IF;  -- end of if the new section is one time fee

    --end if;-- if 'R'


    IF(v_bgt_type = 'P') THEN -- if the new section is a per patient fee


         INSERT INTO SCH_LINEITEM(PK_LINEITEM,FK_BGTSECTION,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
           LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,LINEITEM_DELFLAG,
           CREATOR,IP_ADD,LINEITEM_NAME,LINEITEM_NOTES,
           LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
           LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
            LINEITEM_REPEAT,LINEITEM_PARENTID,FK_CODELST_CATEGORY,
        LINEITEM_TMID, LINEITEM_CDM,lineitem_seq ,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG)
          SELECT seq_sch_lineitem.NEXTVAL,p_section_id,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
           LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,'N',
           p_creator,p_ip_add,LINEITEM_NAME,LINEITEM_NOTES,
           1,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
           LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
            '',k.pk_lineitem,FK_CODELST_CATEGORY,
        LINEITEM_TMID, LINEITEM_CDM ,lineitem_seq ,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG
        FROM SCH_LINEITEM k
     WHERE fk_bgtsection = i.pk_budgetsec
       AND lineitem_inpersec=2  AND
     NVL(k.lineitem_delflag,'Z') <> 'Y';

       IF i.bgtsection_delflag = 'R' THEN
           INSERT INTO SCH_LINEITEM(PK_LINEITEM,FK_BGTSECTION,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
           LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,LINEITEM_DELFLAG,
           CREATOR,IP_ADD,LINEITEM_NAME,LINEITEM_NOTES,
           LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
           LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
            LINEITEM_REPEAT,LINEITEM_PARENTID,FK_CODELST_CATEGORY,
                LINEITEM_TMID, LINEITEM_CDM ,lineitem_seq ,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG)
          SELECT seq_sch_lineitem.NEXTVAL,p_section_id,LINEITEM_DESC,LINEITEM_SPONSORUNIT,
           LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,'N',
           p_creator,p_ip_add,LINEITEM_NAME,LINEITEM_NOTES,
           -1,LINEITEM_STDCARECOST,LINEITEM_RESCOST,
           LINEITEM_INVCOST,LINEITEM_INCOSTDISC,LINEITEM_CPTCODE,
            '',k.pk_lineitem,FK_CODELST_CATEGORY,
        LINEITEM_TMID, LINEITEM_CDM ,lineitem_seq ,FK_CODELST_COST_TYPE,SUBCOST_ITEM_FLAG
        FROM SCH_LINEITEM k
     WHERE fk_bgtsection = i.pk_budgetsec AND
     LINEITEM_REPEAT = v_rep_per_patcode
     AND NVL(k.lineitem_delflag,'Z') <> 'Y';
     END IF;



    END IF;

   END LOOP; --loop for pks of defalult sect  ions
IF (p_commit = 'Y') THEN
   COMMIT;
END IF;
o_ret := 1;
RETURN;
END;

PROCEDURE sp_clear_subTotals( p_lineitem_ids IN ARRAY_STRING)
AS
    v_count NUMBER;
    v_count_total NUMBER;
    v_loop NUMBER;
    v_countFields NUMBER;
    v_fk_bgtsection NUMBER;
    v_lineitem_id  NUMBER;
    v_fK_bgtcal NUMBER;

BEGIN
    v_countFields := p_lineitem_ids.COUNT();
    v_loop := 1;
    WHILE v_loop <= v_countFields LOOP

        BEGIN
            v_lineitem_id := TO_NUMBER(p_lineitem_ids(v_loop));
            SELECT FK_BGTSECTION INTO v_fk_bgtsection FROM SCH_LINEITEM WHERE PK_LINEITEM = v_lineitem_id;
        EXCEPTION
           WHEN NO_DATA_FOUND THEN
            v_fk_bgtsection := 0;
        END;


        SELECT COUNT(*) INTO v_count FROM SCH_LINEITEM WHERE FK_BGTSECTION = v_fk_bgtsection AND LINEITEM_DELFLAG <> 'Y';

        IF v_count = 0 THEN
            UPDATE SCH_BGTSECTION SET SRT_COST_TOTAL = '0.00',  SRT_COST_GRANDTOTAL = '0.00', SOC_COST_TOTAL = '0.00', SOC_COST_GRANDTOTAL = '0.00', SRT_COST_SPONSOR = '0.00', SRT_COST_VARIANCE = '0.00', SOC_COST_SPONSOR = '0.00', SOC_COST_VARIANCE = '0.00' WHERE PK_BUDGETSEC = v_fk_bgtsection;
        END IF;

        BEGIN
                SELECT FK_BGTCAL  INTO v_fK_bgtcal FROM SCH_BGTSECTION WHERE PK_BUDGETSEC = v_fk_bgtsection;
        EXCEPTION
           WHEN NO_DATA_FOUND THEN
            v_fK_bgtcal := 0;
        END;


        SELECT COUNT(*) INTO v_count_total FROM SCH_LINEITEM  WHERE  LINEITEM_DELFLAG <> 'Y'  AND  FK_BGTSECTION IN ( SELECT PK_budgetsec   FROM SCH_BGTSECTION WHERE FK_BGTCAL =  v_fK_bgtcal AND BGTSECTION_DELFLAG = 'N' ) ;

        IF v_count_total = 0 THEN
            UPDATE SCH_BGTCAL SET  GRAND_RES_COST = '0.00',   GRAND_RES_TOTALCOST = '0.00', GRAND_SOC_COST = '0.00',  GRAND_SOC_TOTALCOST = '0.00',
            GRAND_TOTAL_COST = '0.00', GRAND_TOTAL_TOTALCOST = '0.00', GRAND_SALARY_COST = '0.00', GRAND_SALARY_TOTALCOST = '0.00',
            GRAND_FRINGE_COST = '0.00', GRAND_FRINGE_TOTALCOST = '0.00',  GRAND_DISC_COST = '0.00', GRAND_DISC_TOTALCOST = '0.00',
            GRAND_IND_COST = '0.00', GRAND_IND_TOTALCOST = '0.00', GRAND_RES_SPONSOR   = '0.00',  GRAND_SOC_SPONSOR = '0.00', GRAND_SOC_VARIANCE = '0.00',
            GRAND_TOTAL_SPONSOR = '0.00', GRAND_TOTAL_VARIANCE = '0.00',GRAND_RES_VARIANCE  = '0.00' WHERE PK_BGTCAL  =v_fK_bgtcal;
        END IF;


        v_loop := v_loop+1;
    END LOOP;

    COMMIT;
END;


PROCEDURE sp_copy_budgetcontent (     P_PKOLDBGT IN NUMBER,
     P_CREATOR  IN   NUMBER,     P_IPADD  IN   VARCHAR2,
 P_NEWPKBGT IN NUMBER, P_TEMPFLAG IN CHAR, P_RETURN OUT NUMBER
    )

 AS
  v_newpkbgt NUMBER(38);
  v_newpkbgtcal NUMBER(38);
  v_newpkbgtsec NUMBER(38);
  v_oldpkbgtcal NUMBER(38);
  v_oldpkbgtsec NUMBER(38);
  rt_str VARCHAR2(10);
  rt_count NUMBER(10);
  v_vount NUMBER := 0;
  v_acc NUMBER;
  v_useraccspace NUMBER(38);
  v_spacereq NUMBER(38);
  V_NEWBGTVER  VARCHAR2(25);
  v_rec_mapping rec_mapping   := rec_mapping(NULL,NULL);
  v_table_parent_mapping tab_mapping := tab_mapping(); --index-by table typ
  v_cnt NUMBER := 0;
  v_new_lineitem NUMBER;
  V_SUCCESS NUMBER;

  maxsection number;
  minsection number;
  BEGIN
  -- check for duplicacy on name and version
  -- get user account
  --uncommented to find the user account id (fix for bug 1647: none uploaded files are copied, when copying a budget)--anu
 SELECT fk_account
  INTO v_acc
  FROM ER_USER
  WHERE pk_user = P_CREATOR  ;

  -- copy protocols associated with the budget
   FOR j IN (SELECT PK_BGTCAL, BGTCAL_PROTID,BGTCAL_PROTTYPE,  BGTCAL_DELFLAG , BGTCAL_SPONSOROHEAD,
            BGTCAL_CLINICOHEAD , BGTCAL_SPONSORFLAG,  BGTCAL_CLINICFLAG ,   BGTCAL_INDIRECTCOST,
            BGTCAL_FRGBENEFIT, BGTCAL_FRGFLAG, BGTCAL_DISCOUNT, BGTCAL_DISCOUNTFLAG, BGTCAL_EXCLDSOCFLAG,
            GRAND_RES_COST, GRAND_RES_TOTALCOST, GRAND_SOC_COST, GRAND_SOC_TOTALCOST, GRAND_TOTAL_COST,
            GRAND_TOTAL_TOTALCOST, GRAND_SALARY_COST, GRAND_SALARY_TOTALCOST, GRAND_FRINGE_COST, GRAND_FRINGE_TOTALCOST,
            GRAND_DISC_COST, GRAND_DISC_TOTALCOST, GRAND_IND_COST, GRAND_IND_TOTALCOST, GRAND_RES_SPONSOR,
            GRAND_RES_VARIANCE, GRAND_SOC_SPONSOR, GRAND_SOC_VARIANCE, GRAND_TOTAL_SPONSOR, GRAND_TOTAL_VARIANCE
            FROM SCH_BGTCAL WHERE FK_BUDGET = P_PKOLDBGT)
  LOOP
      -- new bgt cal pk
          SELECT SEQ_SCH_BGTCAL.NEXTVAL
          INTO v_newpkbgtcal
          FROM dual;
            v_oldpkbgtcal:= j.PK_BGTCAL;
  -- insert bgt calendar record
          INSERT INTO SCH_BGTCAL (  PK_BGTCAL, FK_BUDGET , BGTCAL_PROTID  ,
             BGTCAL_PROTTYPE,  BGTCAL_DELFLAG , BGTCAL_SPONSOROHEAD,
             BGTCAL_CLINICOHEAD , BGTCAL_SPONSORFLAG,  BGTCAL_CLINICFLAG ,
             BGTCAL_INDIRECTCOST,  CREATOR , CREATED_ON, IP_ADD,
             BGTCAL_FRGBENEFIT, BGTCAL_FRGFLAG, BGTCAL_DISCOUNT, BGTCAL_DISCOUNTFLAG, BGTCAL_EXCLDSOCFLAG ,
             GRAND_RES_COST, GRAND_RES_TOTALCOST, GRAND_SOC_COST, GRAND_SOC_TOTALCOST, GRAND_TOTAL_COST,
             GRAND_TOTAL_TOTALCOST, GRAND_SALARY_COST, GRAND_SALARY_TOTALCOST, GRAND_FRINGE_COST, GRAND_FRINGE_TOTALCOST,
             GRAND_DISC_COST, GRAND_DISC_TOTALCOST, GRAND_IND_COST, GRAND_IND_TOTALCOST, GRAND_RES_SPONSOR,
             GRAND_RES_VARIANCE, GRAND_SOC_SPONSOR, GRAND_SOC_VARIANCE, GRAND_TOTAL_SPONSOR, GRAND_TOTAL_VARIANCE   )
              VALUES(v_newpkbgtcal, P_NEWPKBGT,j.BGTCAL_PROTID,
                                j.BGTCAL_PROTTYPE,  j.BGTCAL_DELFLAG , j.BGTCAL_SPONSOROHEAD,
                                 j.BGTCAL_CLINICOHEAD , j.BGTCAL_SPONSORFLAG,  j.BGTCAL_CLINICFLAG ,
                                 j.BGTCAL_INDIRECTCOST, P_CREATOR,SYSDATE,P_IPADD,
                                j.BGTCAL_FRGBENEFIT, j.BGTCAL_FRGFLAG, j.BGTCAL_DISCOUNT, j.BGTCAL_DISCOUNTFLAG, j.BGTCAL_EXCLDSOCFLAG,
                                j.GRAND_RES_COST, j.GRAND_RES_TOTALCOST, j.GRAND_SOC_COST, j.GRAND_SOC_TOTALCOST, j.GRAND_TOTAL_COST,
                                j.GRAND_TOTAL_TOTALCOST, j.GRAND_SALARY_COST, j.GRAND_SALARY_TOTALCOST, j.GRAND_FRINGE_COST, j.GRAND_FRINGE_TOTALCOST,
                                j.GRAND_DISC_COST, j.GRAND_DISC_TOTALCOST, j.GRAND_IND_COST, j.GRAND_IND_TOTALCOST, j.GRAND_RES_SPONSOR,
                                j.GRAND_RES_VARIANCE, j.GRAND_SOC_SPONSOR, j.GRAND_SOC_VARIANCE, j.GRAND_TOTAL_SPONSOR, j.GRAND_TOTAL_VARIANCE );
   FOR i IN (SELECT PK_BUDGETSEC, BGTSECTION_NAME, BGTSECTION_DELFLAG,
             BGTSECTION_PATNO, BGTSECTION_PERSONLFLAG,
             BGTSECTION_TYPE,BGTSECTION_NOTES,BGTSECTION_SEQUENCE,
             SRT_COST_TOTAL, SRT_COST_GRANDTOTAL, SOC_COST_TOTAL, SOC_COST_GRANDTOTAL, SRT_COST_SPONSOR,
             SRT_COST_VARIANCE, SOC_COST_SPONSOR, SOC_COST_VARIANCE,FK_VISIT
             FROM SCH_BGTSECTION
              WHERE fk_bgtcal = v_oldpkbgtcal
              ORDER BY BGTSECTION_SEQUENCE)
   LOOP
      -- new bgt section pk
          SELECT SEQ_SCH_BGTSECTION.NEXTVAL
         INTO v_newpkbgtsec
         FROM dual;
            v_oldpkbgtsec := i.PK_BUDGETSEC;
              --insert section record
                  INSERT INTO SCH_BGTSECTION
                    (PK_BUDGETSEC, FK_BGTCAL , BGTSECTION_NAME,
                     BGTSECTION_DELFLAG, BGTSECTION_SEQUENCE,  CREATOR, CREATED_ON, IP_ADD,
                    BGTSECTION_PATNO, BGTSECTION_PERSONLFLAG, BGTSECTION_TYPE,BGTSECTION_NOTES,
                    SRT_COST_TOTAL, SRT_COST_GRANDTOTAL, SOC_COST_TOTAL, SOC_COST_GRANDTOTAL, SRT_COST_SPONSOR,
                    SRT_COST_VARIANCE, SOC_COST_SPONSOR, SOC_COST_VARIANCE,FK_VISIT)
                  VALUES
                    (v_newpkbgtsec, v_newpkbgtcal, i.BGTSECTION_NAME,
                    i.BGTSECTION_DELFLAG, i.BGTSECTION_SEQUENCE, P_CREATOR,SYSDATE,P_IPADD,
                    i.BGTSECTION_PATNO, i.BGTSECTION_PERSONLFLAG, i.BGTSECTION_TYPE,i.BGTSECTION_NOTES,
                    i.SRT_COST_TOTAL, i.SRT_COST_GRANDTOTAL, i.SOC_COST_TOTAL, i.SOC_COST_GRANDTOTAL, i.SRT_COST_SPONSOR,
                    i.SRT_COST_VARIANCE, i.SOC_COST_SPONSOR, i.SOC_COST_VARIANCE,i.FK_VISIT
                          );
        --insert line items
    IF NVL(i.BGTSECTION_DELFLAG,'N') = 'N' THEN
       INSERT INTO SCH_LINEITEM(PK_LINEITEM,FK_BGTSECTION,LINEITEM_NAME, LINEITEM_DESC,
            LINEITEM_DELFLAG, CREATOR ,CREATED_ON , IP_ADD,LINEITEM_SPONSORUNIT,
            LINEITEM_CLINICNOFUNIT ,LINEITEM_OTHERCOST,
            LINEITEM_INPERSEC, LINEITEM_STDCARECOST, LINEITEM_RESCOST,
            LINEITEM_INVCOST, LINEITEM_INCOSTDISC, LINEITEM_CPTCODE, LINEITEM_REPEAT,
            LINEITEM_PARENTID, LINEITEM_APPLYINFUTURE,FK_CODELST_CATEGORY,
            LINEITEM_TMID, LINEITEM_CDM, LINEITEM_APPLYINDIRECTS, LINEITEM_TOTALCOST,
            LINEITEM_SPONSORAMOUNT, LINEITEM_VARIANCE ,FK_EVENT,lineitem_seq,FK_CODELST_COST_TYPE,
            SUBCOST_ITEM_FLAG)
            SELECT SEQ_SCH_LINEITEM.NEXTVAL,v_newpkbgtsec,LINEITEM_NAME,LINEITEM_DESC,
            LINEITEM_DELFLAG,P_CREATOR,SYSDATE, P_IPADD ,LINEITEM_SPONSORUNIT,
            LINEITEM_CLINICNOFUNIT ,LINEITEM_OTHERCOST,
            LINEITEM_INPERSEC, LINEITEM_STDCARECOST, LINEITEM_RESCOST,
            LINEITEM_INVCOST, LINEITEM_INCOSTDISC, LINEITEM_CPTCODE, LINEITEM_REPEAT,
            LINEITEM_PARENTID, LINEITEM_APPLYINFUTURE,FK_CODELST_CATEGORY,
            LINEITEM_TMID, LINEITEM_CDM, LINEITEM_APPLYINDIRECTS,
            LINEITEM_TOTALCOST, LINEITEM_SPONSORAMOUNT, LINEITEM_VARIANCE, FK_EVENT,lineitem_seq,FK_CODELST_COST_TYPE,
            SUBCOST_ITEM_FLAG
            FROM SCH_LINEITEM WHERE FK_BGTSECTION = v_oldpkbgtsec ;
     ELSIF NVL(i.BGTSECTION_DELFLAG,'Y') = 'P' OR NVL(i.BGTSECTION_DELFLAG,'Y') = 'R' THEN -- Personnel cost section
     -- get the lineitems for personnel and repeat
            FOR LL IN (  SELECT pk_lineitem,LINEITEM_NAME,LINEITEM_DESC,
                LINEITEM_DELFLAG,LINEITEM_SPONSORUNIT,
                LINEITEM_CLINICNOFUNIT ,LINEITEM_OTHERCOST,
                LINEITEM_INPERSEC, LINEITEM_STDCARECOST, LINEITEM_RESCOST,
                LINEITEM_INVCOST, LINEITEM_INCOSTDISC, LINEITEM_CPTCODE, LINEITEM_REPEAT,
                LINEITEM_PARENTID, LINEITEM_APPLYINFUTURE , LINEITEM_NOTES,FK_CODELST_CATEGORY,
                LINEITEM_TMID, LINEITEM_CDM, LINEITEM_APPLYINDIRECTS,
                LINEITEM_TOTALCOST, LINEITEM_SPONSORAMOUNT, LINEITEM_VARIANCE,FK_EVENT,lineitem_seq,FK_CODELST_COST_TYPE,
                SUBCOST_ITEM_FLAG
                  FROM SCH_LINEITEM WHERE FK_BGTSECTION = v_oldpkbgtsec )
       LOOP
            -- create  a mapping record for the new spl. line item.
     v_cnt := v_cnt + 1;
     SELECT SEQ_SCH_LINEITEM.NEXTVAL
     INTO v_new_lineitem FROM dual;
      INSERT INTO SCH_LINEITEM(PK_LINEITEM,FK_BGTSECTION,LINEITEM_NAME, LINEITEM_DESC,
            LINEITEM_DELFLAG, CREATOR ,CREATED_ON , IP_ADD,LINEITEM_SPONSORUNIT,
            LINEITEM_CLINICNOFUNIT ,LINEITEM_OTHERCOST,
            LINEITEM_INPERSEC, LINEITEM_STDCARECOST, LINEITEM_RESCOST,
            LINEITEM_INVCOST, LINEITEM_INCOSTDISC, LINEITEM_CPTCODE, LINEITEM_REPEAT,
            LINEITEM_PARENTID, LINEITEM_APPLYINFUTURE, LINEITEM_NOTES,FK_CODELST_CATEGORY,
            LINEITEM_TMID, LINEITEM_CDM, LINEITEM_APPLYINDIRECTS,
            LINEITEM_TOTALCOST, LINEITEM_SPONSORAMOUNT, LINEITEM_VARIANCE ,FK_EVENT,lineitem_seq,FK_CODELST_COST_TYPE,
            SUBCOST_ITEM_FLAG)
        VALUES ( v_new_lineitem ,v_newpkbgtsec,LL.LINEITEM_NAME,LL.LINEITEM_DESC,
            LL.LINEITEM_DELFLAG,P_CREATOR,SYSDATE, P_IPADD ,LL.LINEITEM_SPONSORUNIT,
            LL.LINEITEM_CLINICNOFUNIT ,LL.LINEITEM_OTHERCOST,
            LL.LINEITEM_INPERSEC, LL.LINEITEM_STDCARECOST, LL.LINEITEM_RESCOST,
            LL.LINEITEM_INVCOST, LL.LINEITEM_INCOSTDISC, LL.LINEITEM_CPTCODE, LL.LINEITEM_REPEAT,
            LL.LINEITEM_PARENTID, LL.LINEITEM_APPLYINFUTURE, LL.LINEITEM_NOTES,LL.FK_CODELST_CATEGORY,
            LL.LINEITEM_TMID, LL.LINEITEM_CDM, LL.LINEITEM_APPLYINDIRECTS,
            LL.LINEITEM_TOTALCOST, LL.LINEITEM_SPONSORAMOUNT, LL.LINEITEM_VARIANCE,LL.FK_EVENT,ll.lineitem_seq,ll.FK_CODELST_COST_TYPE,
            ll.SUBCOST_ITEM_FLAG);

            v_rec_mapping.old_id := LL.pk_lineitem;
            v_rec_mapping.new_id := v_new_lineitem;
            v_table_parent_mapping.EXTEND;
            v_table_parent_mapping(v_cnt) := v_rec_mapping;
            v_cnt := v_table_parent_mapping.COUNT();
      END LOOP;
  END IF;
   END LOOP;--section loop

  select max(BGTSECTION_SEQUENCE) into maxsection from SCH_BGTSECTION where FK_VISIT is null
  and BGTSECTION_TYPE is not null and FK_BGTCAL = v_newpkbgtcal;

  select min(BGTSECTION_SEQUENCE) into minsection from SCH_BGTSECTION where FK_VISIT is not null and FK_BGTCAL = v_newpkbgtcal;

  --plog.debug(pctx, '4 maxsection: '||maxsection|| ' minsection: ' || minsection );

  while (minsection <= maxsection)
  loop
    if maxsection < 0 then
      Update SCH_BGTSECTION set BGTSECTION_SEQUENCE = BGTSECTION_SEQUENCE + maxsection ,
      LAST_MODIFIED_BY = P_CREATOR  ,
      last_modified_date = SYSDATE ,
      IP_ADD = P_IPADD
      where FK_VISIT is null and FK_BGTCAL = v_newpkbgtcal;
    else
      if maxsection = 0 then
        maxsection := 1;
      end if;

      Update SCH_BGTSECTION set BGTSECTION_SEQUENCE = BGTSECTION_SEQUENCE - maxsection ,
      LAST_MODIFIED_BY = P_CREATOR  ,
      last_modified_date = SYSDATE ,
      IP_ADD = P_IPADD
      where FK_VISIT is null and FK_BGTCAL = v_newpkbgtcal;
    end if;

    select max(BGTSECTION_SEQUENCE) into maxsection from SCH_BGTSECTION where FK_VISIT is null
  	and BGTSECTION_TYPE is not null and FK_BGTCAL = v_newpkbgtcal;

	select min(BGTSECTION_SEQUENCE) into minsection from SCH_BGTSECTION where FK_VISIT is not null and FK_BGTCAL = v_newpkbgtcal;
  end loop;

   sp_apply_special_bgtsections(P_NEWPKBGT ,v_newpkbgtcal,v_table_parent_mapping,
      P_CREATOR ,
      P_IPADD  ,
   V_SUCCESS);
        END LOOP; -- for bgt protocol loop
 IF(P_TEMPFLAG <> 'T') THEN
   -- insert bgt apndx records
       --get available space from the allocated space for user's account
        SELECT Ac_Free_Space(v_acc)
    INTO v_useraccspace
    FROM dual;
   -- get space required for budget's appendix
  BEGIN
          SELECT SUM(dbms_lob.getlength(BGTAPNDX_FILEOBJ))
          INTO v_spacereq
          FROM SCH_BGTAPNDX
          WHERE FK_BUDGET = P_PKOLDBGT;
          EXCEPTION WHEN NO_DATA_FOUND THEN v_spacereq := 0;
      END;
  IF v_spacereq <= v_useraccspace THEN --copy complete appendix
       INSERT INTO SCH_BGTAPNDX(PK_BGTAPNDX,FK_BUDGET,BGTAPNDX_DESC ,BGTAPNDX_URI,
                     BGTAPNDX_FILEOBJ,BGTAPNDX_TYPE,CREATOR,CREATED_ON, IP_ADD )
    SELECT SEQ_SCH_BGTAPNDX.NEXTVAL,P_NEWPKBGT,BGTAPNDX_DESC ,BGTAPNDX_URI,
                     BGTAPNDX_FILEOBJ,BGTAPNDX_TYPE,P_CREATOR,SYSDATE, P_IPADD
     FROM SCH_BGTAPNDX
     WHERE FK_BUDGET = P_PKOLDBGT;
   ELSE --copy only URI type appendix
              INSERT INTO SCH_BGTAPNDX(PK_BGTAPNDX,FK_BUDGET,BGTAPNDX_DESC ,BGTAPNDX_URI,
                     BGTAPNDX_FILEOBJ,BGTAPNDX_TYPE,CREATOR,CREATED_ON, IP_ADD )
            SELECT SEQ_SCH_BGTAPNDX.NEXTVAL,P_NEWPKBGT,BGTAPNDX_DESC ,BGTAPNDX_URI,
                     BGTAPNDX_FILEOBJ,BGTAPNDX_TYPE,P_CREATOR,SYSDATE, P_IPADD
     FROM SCH_BGTAPNDX
     WHERE FK_BUDGET = P_PKOLDBGT AND BGTAPNDX_TYPE = 'U';
   END IF;
   END IF;
 END;

-------------

/*Procedure to create and then copy the selected budget/template to this budget
p_commitflag : if passed as 1, issue commit else no commit
p_templag : Pass T for templates
*/
PROCEDURE sp_copyCompleteBudget(
 p_old_budget IN NUMBER,
 p_new_budname IN VARCHAR2,
 p_new_budver IN VARCHAR2,
 p_creator IN NUMBER,
 p_ip_Add IN VARCHAR2,
 p_return OUT NUMBER,
 p_commitflag IN number,
 p_tempflag IN Varchar2,
 P_DEF_CALENDAR IN NUMBER
 )
AS
retPK NUMBER;
retVal NUMBER;
ret NUMBER ;
BEGIN
     Pkg_Bgt.sp_new_budget_common(p_old_budget,p_new_budname,p_new_budver,p_creator,p_ip_Add,retPK,P_DEF_CALENDAR,p_old_budget);
     IF(retPK > 0) THEN
         Pkg_Bgt.sp_copy_budgetcontent(p_old_budget,p_creator,p_ip_Add,retPK,p_tempflag,retVal);
     END IF;
    p_return := retPK;


    if nvl(p_commitflag,0) = 1 then
        commit;
    end if;

end;
/*
Procedure to create default linked budget for a protoocl calendar
*/
PROCEDURE sp_createProtocolBudget(
 p_event_id IN Number, p_budget_template IN Number,
 p_new_budname IN VARCHAR2,
 p_creator IN NUMBER,
 p_ip_Add IN VARCHAR2,
 p_return OUT NUMBER,
 p_commitflag IN number,
 p_study in number

 )
AS
/*SM: CCF-FIN1 Changed 2nd parameter from p_calledfrom to p_budget_template
  This is done explicitely as Library calendars will not have default budgets anymore.
  The only place the procedure will be called from is study setup calendars
*/
retPK NUMBER;
retVal NUMBER;
ret NUMBER ;

v_new_budver  VARCHAR2(10) := '1';
v_tempflag Varchar2(1) := 'T';
v_newbudpk number;
v_newbgtcal number;
v_seq number;
v_study number;
v_budget_rightsscope char(1);
v_budget_rights varchar2(3);

BEGIN

/*CCF-FIN1=> Default template won't be used anymore
 -- get the default template to create the new budget
 begin
     select to_number(nvl(SETTINGS_VALUE,'0'))
     into v_budget_template from er_settings
     where SETTINGS_KEYWORD = 'CALBUD_TEMP' and SETTINGS_MODNAME = 1 and SETTINGS_MODNUM = (select fk_account from er_user where pk_user = p_creator);

 	exception when no_data_found then
    	v_budget_template := 0;
 end;
*/

 if (p_budget_template is not null) then
     sp_copyCompleteBudget( p_budget_template,
     p_new_budname , v_new_budver , p_creator , p_ip_Add , p_return , p_commitflag , v_tempflag ,p_event_id);

  	v_newbudpk := p_return;

   --update the new budget with other required information

		v_study := p_study;
	    v_budget_rightsscope := 'S';
	    v_budget_rights := '777' ;

	    update sch_budget
	    set fk_study = v_study, last_modified_by = p_creator,last_modified_date = SYSDATE,ip_add= p_ip_Add,budget_siteflag = 1,fk_site = (select fk_siteid from er_user where pk_user = p_creator),
	    budget_rightscope = v_budget_rightsscope,budget_rights=v_budget_rights
	    where pk_budget = v_newbudpk;

		--link the calendar to budget
	    select seq_SCH_BGTCAL.nextval
	    into v_newbgtcal
	    from dual;

	if (v_newbudpk >0) then
	begin
		Insert into SCH_BGTCAL
		   (PK_BGTCAL, FK_BUDGET, BGTCAL_PROTID, BGTCAL_PROTTYPE, BGTCAL_DELFLAG,
		    BGTCAL_SPONSOROHEAD, BGTCAL_CLINICOHEAD, BGTCAL_SPONSORFLAG, BGTCAL_CLINICFLAG, BGTCAL_INDIRECTCOST,
		    CREATOR, CREATED_ON,
		    IP_ADD)
		 Values
		   (v_newbgtcal, v_newbudpk, p_event_id, 'S', 'N',
		    0, NULL, 'N', 'N', NULL,
		    p_creator, sysdate,
		    p_ip_add);

		v_seq := 0;

	 	sp_copy_base_sections(p_budget_template, v_newbgtcal, p_creator, P_IP_ADD, v_seq,p_commitflag );
	 end;
	 end if;

 end if;

END sp_createProtocolBudget;



PROCEDURE sp_new_budget_common(
   P_PKOLDBGT IN NUMBER,
 P_NEWBGTNAME  IN   VARCHAR2,
 P_NEWBGTVER  IN   VARCHAR2,
    P_CREATOR  IN   NUMBER,
 P_IPADD  IN   VARCHAR2,
 P_RETURN OUT NUMBER,
 P_DEF_CALENDAR IN NUMBER,
 P_BUDGET_TEMPLATE IN NUMBER
 )
 AS
   v_newpkbgt NUMBER(38);
   rt_str VARCHAR2(10);
   rt_count NUMBER(10);
   v_vount NUMBER := 0;
   v_acc NUMBER;
   V_NEWBGTVER  VARCHAR2(25);
   v_stat_code varchar2(15);
   v_def_status number;

 BEGIN

   -- get PK for 'work in progress' status
  begin
    select pk_codelst
    into v_def_status
      from sch_codelst
      where trim(codelst_type)='budget_stat' and trim(codelst_subtyp) = 'W';

      v_stat_code := 'W';

  exception when no_data_found then
    v_def_status := NULL;
    v_stat_code := NULL;
  end;

 -- check for duplicacy on name and version
  -- get user account
  SELECT fk_account
  INTO v_acc
  FROM ER_USER
  WHERE pk_user = P_CREATOR  ;
   IF trim(P_NEWBGTVER) = 'null'  THEN
     SELECT COUNT(*)
     INTO v_vount
     FROM SCH_BUDGET
     WHERE trim(BUDGET_NAME) = trim(P_NEWBGTNAME) AND  fk_account = v_acc AND
  NVL(BUDGET_DELFLAG,'Z')<>'Y' AND
     BUDGET_VERSION IS NULL ;
    V_NEWBGTVER := NULL;
   ELSE
     SELECT COUNT(*)
     INTO v_vount
     FROM SCH_BUDGET
     WHERE trim(BUDGET_NAME) = trim(P_NEWBGTNAME) AND  fk_account = v_acc AND
  NVL(BUDGET_DELFLAG,'Z')<>'Y' AND
     trim(BUDGET_VERSION) = trim(P_NEWBGTVER) ;
     V_NEWBGTVER := P_NEWBGTVER;
   END IF;
   IF v_vount > 0 THEN
        P_RETURN := -2;   -- duplicate combination of version # and bud name
    RETURN;
   END IF;
  --
  -- new bgt pk
   SELECT SEQ_SCH_BUDGET.NEXTVAL
   INTO v_newpkbgt
   FROM dual;
   --new bgt def rights
   SELECT COUNT(*)
   INTO rt_count
   FROM ER_CTRLTAB
   WHERE CTRL_KEY = 'bgt_rights';
    FOR i IN 0 .. rt_count - 1
     LOOP
       rt_str:= rt_str || '0';
     END LOOP;
   --copy budget master details
   INSERT INTO SCH_BUDGET ( PK_BUDGET, BUDGET_NAME,
    BUDGET_VERSION ,BUDGET_DESC, BUDGET_CREATOR,BUDGET_TYPE,BUDGET_TEMPLATE        ,
    BUDGET_STATUS   , BUDGET_CURRENCY, BUDGET_SITEFLAG,
    BUDGET_CALFLAG  ,FK_STUDY  ,  FK_ACCOUNT  , FK_SITE            ,
    BUDGET_RIGHTSCOPE  , BUDGET_RIGHTS ,
    CREATOR ,CREATED_ON  ,IP_ADD, BUDGET_DELFLAG ,FK_CODELST_STATUS,BUDGET_CALENDAR)
    SELECT v_newpkbgt, P_NEWBGTNAME,V_NEWBGTVER,BUDGET_DESC, P_CREATOR,BUDGET_TYPE,DECODE(P_BUDGET_TEMPLATE,null,BUDGET_TEMPLATE,P_BUDGET_TEMPLATE),
    v_stat_code, BUDGET_CURRENCY, BUDGET_SITEFLAG,
    BUDGET_CALFLAG  ,FK_STUDY  ,  FK_ACCOUNT  , FK_SITE            ,
    NULL,rt_str,P_CREATOR,SYSDATE,P_IPADD, 'N',v_def_status,p_def_calendar
    FROM SCH_BUDGET
    WHERE pk_budget = P_PKOLDBGT;

 P_RETURN := v_newpkbgt;

 END sp_new_budget_common;

/*
Procedure to add to default linked budget for a protoocl calendar
*/

PROCEDURE sp_addToProtocolBudget(
 p_event_id IN Number,p_calledfrom IN varchar2, p_calendar IN number,
  p_creator IN NUMBER,
 p_ip_Add IN VARCHAR2,
 p_return OUT NUMBER,
 p_commitflag IN number,
 p_visit IN Number,
 p_evname IN Varchar2,
 p_cpt IN VARCHAR2,p_category IN Number,p_desc IN Varchar2,p_event_sequence in number

 )AS
 v_pk_budget Number;
v_pk_bgtcal Number;
v_budgetsec Number;
v_category Number;

BEGIN

	--get calendar's default budget
	begin
		v_category := p_category;

		select pk_budget,PK_BGTCAL,pk_budgetsec
		into v_pk_budget,v_pk_bgtcal,v_budgetsec
		from sch_budget,sch_bgtcal,sch_bgtsection
		where budget_calendar = p_calendar and pk_budget = fk_budget and
		bgtcal_protid = p_calendar and PK_BGTCAL = fk_bgtcal and sch_bgtsection.fk_visit = p_visit and rownum < 2;

		select SEQ_SCH_LINEITEM.NEXTVAL into p_return from dual;

		INSERT INTO SCH_LINEITEM(PK_LINEITEM,FK_BGTSECTION,LINEITEM_NAME, LINEITEM_DESC,LINEITEM_SPONSORUNIT,LINEITEM_STDCARECOST,
		LINEITEM_DELFLAG, LINEITEM_CPTCODE, CREATOR, CREATED_ON,IP_ADD, LINEITEM_CLINICNOFUNIT, FK_CODELST_CATEGORY ,fk_event,lineitem_seq,lineitem_applyindirects)
		VALUES( p_return ,v_budgetsec,p_evname,p_desc,NULL,NULL,'N', p_cpt, P_creator,SYSDATE, p_ip_Add , '1.00', v_category,
		p_event_id,p_event_sequence,1);


	exception when no_data_found then
		v_pk_budget := 0;
		v_pk_bgtcal := 0;
		p_return := 0;

	end;
end;

END Pkg_Bgt;
/

CREATE OR REPLACE SYNONYM ERES.PKG_BGT FOR PKG_BGT;


CREATE OR REPLACE SYNONYM EPAT.PKG_BGT FOR PKG_BGT;


GRANT EXECUTE, DEBUG ON PKG_BGT TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_BGT TO ERES;

