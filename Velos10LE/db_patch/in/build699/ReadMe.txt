/////*********This readMe is specific to v9.3.0 build #699**********////
-----------------------------------------------------------------------------------------------------
We have introduced a new properties file named "configBundle.properties". The purpose of this file
is to allow configurations of eResearch features with reasonable defaults and at the same time
avoid overwriting the previous configurations on each upgrade. It works the same way as labelBundle
and messageBundle. To override defaults, place a file named "configBundle_custom.properties" in
the same location as configBundle.properties and copy-paste the key-value and override. When the 
custom file is created the very first time, a restart is needed for the custom file to be read.
But thereafter clicking the link Manage >> Users >> Refresh Menus/Tabs will reload the custom values,
i.e., restarts are no longer necessary.
-----------------------------------------------------------------------------------------------------
