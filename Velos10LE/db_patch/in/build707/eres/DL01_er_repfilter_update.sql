SET define OFF;

DECLARE
filterID number := 0;
lkpvwID INTEGER DEFAULT 0;
begin
        select PK_REPFILTER into filterID from er_repfilter where REPFILTER_KEYWORD = 'studyType';
	SELECT PK_LKPVIEW INTO lkpvwID FROM ER_LKPVIEW WHERE LKPVIEW_KEYWORD = 'codeLstData';
	IF (filterID != 0) THEN
	UPDATE er_repfilter set REPFILTER_COLUMN = '<td><DIV id="studytypeDIV"><FONT class = "comments"><A href="javascript:void(0);" onClick="return openLookup(document.reports,''viewId='|| lkpvwID ||'&form=reports&seperator=,&defaultvalue=[ALL]&keyword=selstudyType|CODEDESC~paramstudyType|CODEPK|[VELHIDE]'',''dfilter=codelst_type=\''study_type\'' '')">Select Study Type</A> </FONT></DIV> </td>
	       	<td><DIV id="studytypedataDIV"><Input type="text" name="selstudyType" size="20" READONLY value="[ALL]">
	       	<Input type="hidden" name="paramstudyType" value="[ALL]"></DIV>
	       	</td>'  WHERE PK_REPFILTER = filterID;
	else
	dbms_output.put_line('Selected ID is not existing');
	END IF;
	COMMIT;
	
	select PK_REPFILTER into filterID from er_repfilter where REPFILTER_KEYWORD = 'repOrgId';
	SELECT PK_LKPVIEW INTO lkpvwID FROM ER_LKPVIEW WHERE LKPVIEW_KEYWORD = 'orgLookup';
	IF (filterID != 0) THEN
	UPDATE er_repfilter set REPFILTER_COLUMN = '<td><DIV id="repOrgDIV"><FONT class = "comments"><A href="javascript:openLookup(document.reports,''viewId='|| lkpvwID ||'&form=reports&maxselect=1&seperator=,&keyword=selrepOrgId|site_name~paramrepOrgId|pk_site|[VELHIDE]'','''')">Select Reporting Organization</A> </FONT></DIV> </td>
	       	<td><DIV id=''repOrgdataDIV''><Input TYPE="text" NAME="selrepOrgId" SIZE="20" READONLY VALUE=''[SESSSITENAME]''>
	       	<Input TYPE="hidden" NAME="paramrepOrgId" VALUE=''[SESSSITEID]''></DIV>
	       	</td>'  WHERE PK_REPFILTER = filterID;
	else
	dbms_output.put_line('Selected ID is not existing');
	END IF;
	COMMIT;

	select PK_REPFILTER into filterID from er_repfilter where REPFILTER_KEYWORD = 'enrOrgId';
	IF (filterID != 0) THEN
	UPDATE er_repfilter set REPFILTER_COLUMN = '<td><DIV id="enrOrgDIV"><FONT class = "comments"><A href="javascript:openLookup(document.reports,''viewId='|| lkpvwID ||'&form=reports&seperator=,&defaultvalue=[ALL]&keyword=selenrOrgId|site_name~paramenrOrgId|pk_site|[VELHIDE]'','''')">Select Enrolling Organization</A> </FONT></DIV> </td>
	       	<td><DIV id=''enrOrgdataDIV''><Input TYPE="text" NAME="selenrOrgId" SIZE="20" READONLY VALUE=''[ALL]''>
	       	<Input TYPE="hidden" NAME="paramenrOrgId" VALUE=''[ALL]''></DIV>
	       	</td>' WHERE PK_REPFILTER = filterID;
	else
	dbms_output.put_line('Selected ID is not existing');
	END IF;
	COMMIT;

	select PK_REPFILTER into filterID from er_repfilter where REPFILTER_KEYWORD = 'trtOrgId';
	IF (filterID != 0) THEN
	UPDATE er_repfilter set REPFILTER_COLUMN = '<td><DIV id="trtOrgDIV"><FONT class = "comments"><A href="javascript:openLookup(document.reports,''viewId='|| lkpvwID ||'&form=reports&seperator=,&defaultvalue=[ALL]&keyword=seltrtOrgId|site_name~paramtrtOrgId|pk_site|[VELHIDE]'','''')">Select Treating Organization</A> </FONT></DIV> </td>
	       	<td><DIV id=''trtOrgdataDIV''><Input TYPE="text" NAME="seltrtOrgId" SIZE="20" READONLY VALUE=''[ALL]''>
	       	<Input TYPE="hidden" NAME="paramtrtOrgId" VALUE=''[ALL]''></DIV>
	       	</td>', 
	REPFILTER_VALUESQL = 'SELECT pk_site FROM erv_allusersites_distinct WHERE fk_user = :sessUserId AND site_hidden <> 1 
	UNION  SELECT 0 from erv_allusersites_distinct ' where PK_REPFILTER = filterID;
	else
	dbms_output.put_line('Selected ID is not existing');
	END IF;
	COMMIT;
END;
/