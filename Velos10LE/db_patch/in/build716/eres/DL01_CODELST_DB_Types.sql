SET DEFINE OFF;
DECLARE
  v_cnt NUMBER;
BEGIN
  SELECT COUNT(*)
  INTO v_cnt
  FROM ER_CODELST
  WHERE CODELST_TYPE = 'newdashboard'
  AND CODELST_SUBTYP = 'dbformquery';
  IF(v_cnt           <1) THEN
    INSERT
    INTO ER_CODELST
      (
        PK_CODELST,
        CODELST_TYPE,
        CODELST_SUBTYP,
        CODELST_DESC,
        CODELST_HIDE,
        CODELST_SEQ
      )
      VALUES (SEQ_ER_CODELST.nextval,'newdashboard','dbformquery','Forms Queries','N',1);  
END IF;

SELECT COUNT(*) INTO v_cnt FROM ER_CODELST WHERE CODELST_TYPE = 'newdashboard' AND CODELST_SUBTYP = 'dbstudyreview';

IF(v_cnt<1) THEN

INSERT INTO ER_CODELST (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)

VALUES (SEQ_ER_CODELST.nextval,'newdashboard','dbstudyreview',
        'Study Review Cycle',
        'N',
        2
      );
  END IF;
SELECT COUNT(*)
INTO v_cnt
FROM ER_CODELST
WHERE CODELST_TYPE = 'newdashboard'
AND CODELST_SUBTYP = 'dbothers';
IF(v_cnt           <1) THEN
  INSERT
  INTO ER_CODELST
    (
      PK_CODELST,
      CODELST_TYPE,
      CODELST_SUBTYP,
      CODELST_DESC,
      CODELST_HIDE,
      CODELST_SEQ
    )
    VALUES
    (
      SEQ_ER_CODELST.nextval,
      'newdashboard',
      'dbothers','OTHERS','N',3);      

END IF;

END;
/