set Define off;
create or replace
PROCEDURE      SP_CREATE_EVTS_DEF (
  P_EIDS         IN       ARRAY_STRING,
  P_VIDS         IN       ARRAY_STRING,
  P_SEQS         IN       ARRAY_STRING,
  P_DISPS        IN       ARRAY_STRING,
  P_HIDEFLGS     IN       ARRAY_STRING,
  P_STATUS       OUT      NUMBER,
  p_user         IN       NUMBER,
  p_ip           IN       VARCHAR2,
  p_copy_propagateflag IN NUMBER default 1
)
AS
  v_total NUMBER;
  v_idx NUMBER;
  v_event_id EVENT_DEF.EVENT_ID%TYPE;
  v_chain_id EVENT_DEF.CHAIN_ID%TYPE;
  v_event_type EVENT_DEF.EVENT_TYPE%TYPE;
  v_name EVENT_DEF.NAME%TYPE;
  v_notes EVENT_DEF.NOTES%TYPE;
  v_cost EVENT_DEF.COST%TYPE;
  v_cost_description EVENT_DEF.COST_DESCRIPTION%TYPE;
  v_duration EVENT_DEF.DURATION%TYPE;
  v_user_id EVENT_DEF.USER_ID%TYPE;
  v_linked_uri EVENT_DEF.LINKED_URI%TYPE;
  v_fuzzy_period EVENT_DEF.FUZZY_PERIOD%TYPE;
  v_msg_to EVENT_DEF.MSG_TO%TYPE;
  v_status EVENT_DEF.STATUS%TYPE;
  v_description EVENT_DEF.DESCRIPTION%TYPE;
  v_org_id EVENT_DEF.ORG_ID%TYPE;
  v_event_msg EVENT_DEF.EVENT_MSG%TYPE;
  v_event_res EVENT_DEF.EVENT_RES%TYPE;
  v_event_flag EVENT_DEF.EVENT_FLAG%TYPE;
  v_pat_daysbefore EVENT_DEF.PAT_DAYSBEFORE%TYPE;
  v_pat_daysafter EVENT_DEF.PAT_DAYSAFTER%TYPE;
  v_usr_daysbefore EVENT_DEF.USR_DAYSBEFORE%TYPE;
  v_usr_daysafter EVENT_DEF.USR_DAYSAFTER%TYPE;
  v_pat_msgbefore EVENT_DEF.PAT_MSGBEFORE%TYPE;
  v_pat_msgafter EVENT_DEF.PAT_MSGAFTER%TYPE;
  v_usr_msgbefore EVENT_DEF.USR_MSGBEFORE%TYPE;
  v_usr_msgafter EVENT_DEF.USR_MSGAFTER%TYPE;
  v_calendar_sharedwith EVENT_DEF.CALENDAR_SHAREDWITH%TYPE;
  v_duration_unit EVENT_DEF.DURATION_UNIT%TYPE;
  v_event_cptcode EVENT_DEF.EVENT_CPTCODE%TYPE;
  v_event_fuzzyafter EVENT_DEF.EVENT_FUZZYAFTER%TYPE;
  v_event_durationafter EVENT_DEF.EVENT_DURATIONAFTER%TYPE;
  v_event_durationbefore EVENT_DEF.EVENT_DURATIONBEFORE%TYPE;
  v_fk_catlib EVENT_DEF.FK_CATLIB%TYPE;
  v_event_category EVENT_DEF.EVENT_CATEGORY%TYPE;
  v_event_library_type EVENT_DEF.EVENT_LIBRARY_TYPE%TYPE;
  v_event_line_category EVENT_DEF.EVENT_LINE_CATEGORY%TYPE;
  v_service_site_id EVENT_DEF.SERVICE_SITE_ID%TYPE;
  v_facility_id EVENT_DEF.FACILITY_ID%TYPE;
  v_fk_codelst_covertype EVENT_DEF.FK_CODELST_COVERTYPE%TYPE;
  v_reason_for_coveragechange EVENT_DEF.REASON_FOR_COVERAGECHANGE%TYPE;
  v_coverage_notes EVENT_DEF.COVERAGE_NOTES%TYPE;
  v_fk_codelst_calstat EVENT_DEF.FK_CODELST_CALSTAT%TYPE;
  v_temp NUMBER;

BEGIN
  v_total := P_EIDS.COUNT;
  v_idx := 1;
  WHILE v_idx <= v_total LOOP
    v_event_id := 0;
    select EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME,
      NOTES, COST, COST_DESCRIPTION, DURATION, USER_ID,
      LINKED_URI, FUZZY_PERIOD, MSG_TO, STATUS, DESCRIPTION,
      ORG_ID, EVENT_MSG, EVENT_RES, EVENT_FLAG, PAT_DAYSBEFORE,
      PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER, PAT_MSGBEFORE,
      PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER, CALENDAR_SHAREDWITH,
      DURATION_UNIT, EVENT_CPTCODE, EVENT_FUZZYAFTER,
      EVENT_DURATIONAFTER, EVENT_DURATIONBEFORE, FK_CATLIB,
      EVENT_CATEGORY, EVENT_LIBRARY_TYPE, EVENT_LINE_CATEGORY,
      SERVICE_SITE_ID, FACILITY_ID,
      FK_CODELST_COVERTYPE, REASON_FOR_COVERAGECHANGE, COVERAGE_NOTES,
      FK_CODELST_CALSTAT
    into
      v_event_id, v_chain_id, v_event_type, v_name,
      v_notes, v_cost, v_cost_description, v_duration,
      v_user_id, v_linked_uri, v_fuzzy_period, v_msg_to,
      v_status, v_description,
      v_org_id, v_event_msg, v_event_res, v_event_flag,
      v_pat_daysbefore, v_pat_daysafter, v_usr_daysbefore,
      v_usr_daysafter, v_pat_msgbefore, v_pat_msgafter,
      v_usr_msgbefore, v_usr_msgafter, v_calendar_sharedwith,
      v_duration_unit, v_event_cptcode,
      v_event_fuzzyafter, v_event_durationafter,
      v_event_durationbefore, v_fk_catlib,
      v_event_category, v_event_library_type,
      v_event_line_category, v_service_site_id, v_facility_id,
      v_fk_codelst_covertype, v_reason_for_coveragechange,
      v_coverage_notes, v_fk_codelst_calstat
    from event_def where EVENT_ID = P_EIDS(v_idx);

    if (v_event_id > 0) then
      --insert into T(C) values('update event_def row for eid=' || v_event_id||' vid='||P_VIDS(v_idx));
      insert into EVENT_DEF (
        EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME,
        FK_VISIT, EVENT_SEQUENCE, DISPLACEMENT,
        NOTES, COST, COST_DESCRIPTION, DURATION, USER_ID,
        LINKED_URI, FUZZY_PERIOD, MSG_TO, STATUS, DESCRIPTION,
        ORG_ID, EVENT_MSG, EVENT_RES, EVENT_FLAG, PAT_DAYSBEFORE,
        PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER, PAT_MSGBEFORE,
        PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER,
        DURATION_UNIT, EVENT_CPTCODE, EVENT_FUZZYAFTER,
        EVENT_DURATIONAFTER, EVENT_DURATIONBEFORE, FK_CATLIB,
        EVENT_CATEGORY,
        EVENT_LIBRARY_TYPE,
        EVENT_LINE_CATEGORY, SERVICE_SITE_ID, FACILITY_ID,
        FK_CODELST_COVERTYPE, REASON_FOR_COVERAGECHANGE, COVERAGE_NOTES,
        FK_CODELST_CALSTAT
      ) values (
        EVENT_DEFINITION_SEQ.nextval, v_chain_id, v_event_type, v_name,
        TO_NUMBER(P_VIDS(v_idx)), TO_NUMBER(P_SEQS(v_idx)),
        DECODE(TO_NUMBER(P_DISPS(v_idx)), 0, null, TO_NUMBER(P_DISPS(v_idx))),
        v_notes, v_cost, v_cost_description, v_duration,
        v_user_id, v_linked_uri, v_fuzzy_period, v_msg_to,
        v_status, v_description,
        v_org_id, v_event_msg, v_event_res, v_event_flag,
        v_pat_daysbefore, v_pat_daysafter, v_usr_daysbefore,
        v_usr_daysafter, v_pat_msgbefore, v_pat_msgafter,
        v_usr_msgbefore, v_usr_msgafter, v_duration_unit, v_event_cptcode,
        v_event_fuzzyafter, v_event_durationafter,
        v_event_durationbefore, v_fk_catlib,
        v_event_category, v_event_library_type,
        v_event_line_category, v_service_site_id, v_facility_id,
        v_fk_codelst_covertype, v_reason_for_coveragechange,
        '', v_fk_codelst_calstat
      );

      select EVENT_DEFINITION_SEQ.CURRVAL into v_temp from dual;
      ESCH.SP_CPEVEDTLS(v_event_id, v_temp, 'S', P_STATUS, p_user,
        p_ip, p_copy_propagateflag);
    end if;

    v_idx := v_idx+1;
  END LOOP;
  COMMIT;
END;
/
 
 INSERT INTO track_patches
VALUES(seq_track_patches.nextval,349,2,'02_spcreateevtsdef.sql',sysdate,'v10 #750');

commit;