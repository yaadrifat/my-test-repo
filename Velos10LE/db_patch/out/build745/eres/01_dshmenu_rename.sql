DECLARE
begin
update ER_OBJECT_SETTINGS set object_displaytext='Query Dashboard' where object_displaytext='Dashboards' and object_subtype='dash_menu';
update ER_OBJECT_SETTINGS set object_displaytext='Dashboard' where object_displaytext='New Dashboards' and object_subtype='new_dash_menu';
commit;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,344,1,'01_dshmenu_rename.sql',sysdate,'v9.3.0 #745');

commit;

