DECLARE
countFlag number;
v_cnt number;
begin
 SELECT COUNT(1) INTO countFlag FROM USER_TABLES WHERE TABLE_NAME='ER_CODELST';
    if (countFlag > 0) then
      SELECT COUNT(*)  INTO v_cnt from er_codelst  WHERE codelst_type ='priority';
      if v_cnt=0 then
      insert into er_codelst (pk_codelst,codelst_type,codelst_subtyp,codelst_desc,
codelst_hide,codelst_seq,last_modified_date,created_on) values(seq_er_codelst.nextval,
'priority','priority_1','P1','N',1,sysdate,sysdate);

insert into er_codelst (pk_codelst,codelst_type,codelst_subtyp,codelst_desc,
codelst_hide,codelst_seq,last_modified_date,created_on) values(seq_er_codelst.nextval,
'priority','priority_2','P2','N',2,sysdate,sysdate);

insert into er_codelst (pk_codelst,codelst_type,codelst_subtyp,codelst_desc,
codelst_hide,codelst_seq,last_modified_date,created_on) values(seq_er_codelst.nextval,
'priority','priority_3','P3','N',3,sysdate,sysdate);

insert into er_codelst (pk_codelst,codelst_type,codelst_subtyp,codelst_desc,
codelst_hide,codelst_seq,last_modified_date,created_on) values(seq_er_codelst.nextval,
'priority','priority_4','P4','N',4,sysdate,sysdate);

insert into er_codelst (pk_codelst,codelst_type,codelst_subtyp,codelst_desc,
codelst_hide,codelst_seq,last_modified_date,created_on) values(seq_er_codelst.nextval,
'priority','priority_5','P5','N',5,sysdate,sysdate);

insert into er_codelst (pk_codelst,codelst_type,codelst_subtyp,codelst_desc,
codelst_hide,codelst_seq,last_modified_date,created_on) values(seq_er_codelst.nextval,
'priority','priority_6','P6','N',6,sysdate,sysdate);
commit;
      end if;
end if;
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,337,1,'01_priority_alert_insert.sql',sysdate,'v9.3.0 #738');

commit;