set define off;

UPDATE ER_CTRLTAB SET CTRL_VALUE = 'v9.3.0 #714.02' 
where CTRL_KEY = 'app_version' ; 

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,313,7,'07_hotfix2_er_version.sql',sysdate,'v9.3.0 #714.02');

commit;