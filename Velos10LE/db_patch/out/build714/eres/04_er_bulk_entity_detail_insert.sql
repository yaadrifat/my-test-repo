Set Define off;

--eSample
declare
rowExists number := 0;
begin
	select count(*) into rowExists from ER_BULK_ENTITY_DETAIL where VELOS_FIELD_NAME Like 'Calendar Name';
	if (rowExists = 0) then
		Insert into ER_BULK_ENTITY_DETAIL
		   (PK_BULK_ENTITY_DETAIL, FK_BULK_ENTITY_MASTER, VELOS_FIELD_NAME, DATA_TYPE, MANDATORY_FLAG, CREATED_ON, LAST_MODIFIED_DATE)
		 Values
		   (30, 1, 'Calendar Name', 'String', 0, sysdate, sysdate);
   COMMIT;    
	end if;
  
  select count(*) into rowExists from ER_BULK_ENTITY_DETAIL where PK_BULK_ENTITY_DETAIL = 27;
	if (rowExists = 0) then
		Insert into ER_BULK_ENTITY_DETAIL
		   (PK_BULK_ENTITY_DETAIL, FK_BULK_ENTITY_MASTER, VELOS_FIELD_NAME, DATA_TYPE, MANDATORY_FLAG, CREATED_ON, LAST_MODIFIED_DATE)
		 Values
		   (27, 1, 'Visit', 'Integer', 0, sysdate, sysdate);
  COMMIT;
	end if;
	
	select count(*) into rowExists from ER_BULK_ENTITY_DETAIL where PK_BULK_ENTITY_DETAIL = 28;
	if (rowExists = 0) then
		Insert into ER_BULK_ENTITY_DETAIL
		   (PK_BULK_ENTITY_DETAIL, FK_BULK_ENTITY_MASTER, VELOS_FIELD_NAME, DATA_TYPE, MANDATORY_FLAG, CREATED_ON, LAST_MODIFIED_DATE)
		 Values
		   (28, 1, 'Event', 'Integer', 0, sysdate, sysdate);
  COMMIT;
	end if;
  
  select count(*) into rowExists from er_bulk_template_detail where FK_BULK_ENTITY_DETAIL = 26;
	if (rowExists > 0) then
    update er_bulk_template_detail set FK_BULK_ENTITY_DETAIL = 30 where FK_BULK_ENTITY_DETAIL = 26;
  COMMIT;
	end if;
  
  select count(*) into rowExists from ER_BULK_ENTITY_DETAIL where PK_BULK_ENTITY_DETAIL = 26 AND VELOS_FIELD_NAME Like 'Patient Study ID';
	if (rowExists > 0) then
    update ER_BULK_ENTITY_DETAIL set PK_BULK_ENTITY_DETAIL = 29 where PK_BULK_ENTITY_DETAIL=26;
  COMMIT;
	end if;
  
  select count(*) into rowExists from er_bulk_template_detail where FK_BULK_ENTITY_DETAIL = 30;
	if (rowExists > 0) then
    update er_bulk_template_detail set FK_BULK_ENTITY_DETAIL = 29 where FK_BULK_ENTITY_DETAIL = 30;
  COMMIT;
	end if;
  
  select count(*) into rowExists from ER_BULK_ENTITY_DETAIL where PK_BULK_ENTITY_DETAIL = 30;
	if (rowExists > 0) then
    update ER_BULK_ENTITY_DETAIL set PK_BULK_ENTITY_DETAIL = 26 where PK_BULK_ENTITY_DETAIL=30;
  COMMIT;
	end if;

end ;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,313,4,'04_er_bulk_entity_detail_insert.sql',sysdate,'v9.3.0 #714');

commit;
