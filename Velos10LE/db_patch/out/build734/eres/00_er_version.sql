set define off; 

UPDATE ER_CTRLTAB SET CTRL_VALUE = 'v9.3.0 #734' 
where CTRL_KEY = 'app_version' ; 

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,333,0,'00_er_version.sql',sysdate,'v9.3.0 #734');

commit;

