set define off;
set serveroutput on;

DECLARE
  v_item_exists number := 0;  
BEGIN
select count(*) into v_item_exists from er_object_settings where object_name = 'irb_new_tab'
and object_subtype = 'irb_approv_tab';

if (v_item_exists = 0) then
 insert into er_object_settings(pk_object_settings, object_type, object_subtype, 
  object_name, object_sequence, object_visible, object_displaytext, fk_account) 
 values (SEQ_ER_OBJECT_SETTINGS.NEXTVAL, 'T', 'irb_approv_tab', 'irb_new_tab', 1, 1,
  'Approved Version',0);
  commit;
  dbms_output.put_line('irb_approv_tab added');
else
  dbms_output.put_line('irb_approv_tab already exists');
end if;

END;
/

DECLARE
  v_item_exists number := 0;  
BEGIN
select count(*) into v_item_exists from ER_CODELST where CODELST_TYPE = 'studystat'
and CODELST_SUBTYP = 'crc_amend';

if (v_item_exists = 0) then
 insert into ER_CODELST(PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE) 
 values (SEQ_ER_CODELST.NEXTVAL, 'studystat' , 'crc_amend', 'Amendment in draft', 'N');
  commit;
  dbms_output.put_line('crc_amend added');
else
  dbms_output.put_line('crc_amend already exists');
end if;
END;
/
DECLARE
  v_item_exists number := 0;  
BEGIN
select count(*) into v_item_exists from ER_CODELST where CODELST_TYPE = 'studystat'
and CODELST_SUBTYP = 'submitting';

if (v_item_exists = 0) then
 insert into ER_CODELST(PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE) 
 values (SEQ_ER_CODELST.NEXTVAL, 'studystat' , 'submitting', 'Submitting', 'N');
  commit;
  dbms_output.put_line('submitting added');
else
  dbms_output.put_line('submitting already exists');
end if;

END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,322,3,'03_insert_data.sql',sysdate,'v9.3.0 #723');

commit;


