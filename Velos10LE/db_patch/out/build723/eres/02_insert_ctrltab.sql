set define off;
set serveroutput on;

DECLARE
  v_item_exists number := 0;  
BEGIN

select count(*) into v_item_exists from ER_CTRLTAB where CTRL_KEY = 'auth_user';
if (v_item_exists = 0) then
 Insert into ERES.ER_CTRLTAB
   (PK_CTRLTAB, CTRL_VALUE, CTRL_DESC, CTRL_KEY)
 Values
   (SEQ_ER_CTRLTAB.nextval, NULL, 'Lognames of authorized users, comma-separated', 'auth_user');
  commit;
  dbms_output.put_line('One row inserted');
else
  dbms_output.put_line('Row already exists');
end if;

END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,322,2,'02_insert_ctrltab.sql',sysdate,'v9.3.0 #723');

commit;
