set define off;

declare

begin
	for rec in (select distinct fk_study, min(pk_submission) minSubmissionPK from er_submission, 
	(select distinct page_mod_table, page_mod_pk, count(*) cnt from ui_flx_pagever where page_mod_table = 'er_study' group by page_mod_table, page_mod_pk) vertable
	where fk_study = vertable.page_mod_pk
	group by fk_study)
	loop
		update ui_flx_pagever set fk_submission = rec.minSubmissionPK where page_mod_table = 'er_study' 
    and page_mod_pk = rec.fk_study and fk_submission is null;
	end loop;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,322,5,'05_FK_SUBMISSION_Update.sql',sysdate,'v9.3.0 #723');

commit;

