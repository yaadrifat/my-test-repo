/////*********This readMe is specific to v9.3.0 build #700**********////
-----------------------------------------------------------------------------------------------------

NOTE: Since the velos forums is not able to upload the document. we are providing the enhancement documents through build release only.
we have a folder "enhancement_doc" where we have place all the enhancement documents released with this version.

-----------------------------------------------------------------------------------------------------
 Please refer to document "Velos eResearch Technical HowTo - Configuring Race and Ethnicity Hierarchy.docx", for
 configuring race and ethnicity options for testing P-22462.

-----------------------------------------------------------------------------------------------------

For Enhancements please follow the below guidelines

AE-22383 :-

On Adverse Event screen ,'Adverse Event/Grading' field would be separated in to two fields, one being Grade/Severity and
other would be 'Adverse Event Name'. To which lookup column the 'Adverse Event Name' field would map to is under
discussion.

The enhancement document would be updated in future.Also Please note Phase 2 and point 3 of Phase 1
is not yet implemented.


INF-22589 :-

Phase2 and Point3 of the phase1 is not implemented yet.


