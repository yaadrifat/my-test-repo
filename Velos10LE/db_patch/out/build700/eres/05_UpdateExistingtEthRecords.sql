set define off;

update er_codelst set codelst_custom_col1 = 'eth_prim,eth_second' where codelst_type = 'ethnicity';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,299,5,'05_UpdateExistingtEthRecords.sql',sysdate,'v9.3.0 #700');

commit;