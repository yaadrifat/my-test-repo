set define off;
--STARTS INSERT ER_CODELST FOR ENHANCEMENT (JUPITER) FORM:22620--
DECLARE
  v_cnt NUMBER;
  countFlag NUMBER(5);
 
 BEGIN
    SELECT COUNT(1) INTO countFlag FROM USER_TABLES WHERE TABLE_NAME='ER_CODELST';
    if (countFlag > 0) then
	
		SELECT COUNT(*)  INTO v_cnt from sch_codelst  WHERE codelst_type ='fillformstat'  AND CODELST_SUBTYP='unlock';
		IF(v_cnt <1) THEN				
			INSERT INTO ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_STUDY_ROLE)				
			VALUES(SEQ_ER_CODELST.NEXTVAL,'fillformstat','soft_lock','Locked For Review','N',9,'role_prin');
		END IF;	
		
		SELECT COUNT(*)  INTO v_cnt from sch_codelst  WHERE codelst_type ='fillformstat'  AND CODELST_SUBTYP='soft_lock';
		IF(v_cnt <1) THEN  
			INSERT INTO ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_STUDY_ROLE) 
			VALUES(SEQ_ER_CODELST.NEXTVAL,'fillformstat','unlock','Unlock Review','N',10,'role_prin');
		END IF;				
			
commit;
end if;
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,327,1,'01_ER_CODELST.sql',sysdate,'v9.3.0 #728');

commit;

