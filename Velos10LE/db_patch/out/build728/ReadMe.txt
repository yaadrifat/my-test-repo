/////*********This readMe is specific to v9.3.0 build #728**********////

We are releasing first 3 Jupiter Enhancements for the Demo purpose to Velos.
1. INF_22616_No1
2. Form_22617_No2
3. Form_22620_No3

1(Form_22616_No1) and 2(Form_22617_No2) are partially implemented whereas 3(Form_22620_No3) is completely done.

What has been done till now in INF_22616_No1 and Form_22617_No2:
1. Dynamic Drop down to Select dashboard type.
2. Search Filters have been created however, only 2 filters(Study Filter and Organization Filter) are linked to the queries and they are working. rest others needs to 	be linked to the Queries.
3. Search filter with auto complete and mulch-select.
4. Data-table with pagination.
5. Multi-column sorting.
6. Column hide and show functionality 
7. Exporting to pdf, doc and excel
8. Localization


Features not implemented(inprogress):
1.  Study and Organization filters are working and  rest other filters  are under implementation i.e. not fully funtional.You can see the data.
2. Saved Search functionality.
3. Color coded column for status
4. Column cell check-boxes.
5. Check boxes based on codelst type with color coding defined in custom config bundle
6. Save User views
7. Bulk Status Update

Notes:
1. To have the full export functionality, one needs to have FLASH PLAYER installed in the browser/system.