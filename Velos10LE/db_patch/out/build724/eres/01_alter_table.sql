set define off;

ALTER TABLE ERES.UI_FLX_PAGEVER MODIFY(PAGE_MINOR_VER  DEFAULT 0)
/

update UI_FLX_PAGEVER set PAGE_MINOR_VER = 0 where PAGE_MINOR_VER is NULL;

commit;
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,323,1,'01_alter_table.sql',sysdate,'v9.3.0 #724');

commit;
