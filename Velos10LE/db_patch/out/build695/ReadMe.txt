/////*********This readMe is specific to v9.3.0 build #695**********////
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 INV-22518 (Bulk Upload):
------------------------------
This enhancement was earlier released in Build#686(Developed by BT team and integrated in core product ). Later on we found that this enhancement needs to be reviewed again and subsequently this feature has been disabled in  
9.2 .We have worked further on this module and now we are releasing this enhancement again in this build.

There are no db patches released in today's release, since the required database scripts are already executed during Build#686.
During the initial release of this enhancement, we have asked QA to place four jar files (jmimemagic-0.1.0.jar, mime-util-2.1.1.jar,opencsv-2.3.jar & xstream-1.4.4.jar) to be kept at various locations. 

There are some modifications in this regard. If QA has already placed these Jar file at both the locations mentioned  as in Build#686 then, please follow these instructions.

1. Remove the jar files from :server\eresearch\deploy\velos.ear\velos.war\WEB-INF\lib

2. Please make sure that these four jar files  are placed under:server\eresearch\lib


NOTE: Even though the jar files are already released in build#686, for reference purpose we are releasing them with this build again.


