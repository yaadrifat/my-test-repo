/////*********This readMe is specific to v9.3.0 build #704**********////

*********************************************************************************************
If you are taking build without deleting files under velos.ear folder then Please follow the instructions mentioned below.

1. Please delete all files under -
\eResearch_jboss510\server\eresearch\deploy\velos.ear\velos-ejb-eres.jar\com\velos\eres\gems
The files under mentioned folder have been renamed to have correct case. Hence files have been renamed as Workflow*.class instead of WorkFlow*.class.

2. Please delete file workFlow.jsp under -
\eResearch_jboss510\server\eresearch\deploy\velos.ear\velos.war\jsp