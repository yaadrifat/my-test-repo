create or replace
PROCEDURE      "SP_DELETE_BULKMAPPING" (p_ids in ARRAY_STRING, p_userId in varchar2, p_ipAdd in varchar2, o_ret OUT NUMBER)
IS
   v_cnt NUMBER;
   i NUMBER;
   v_id_str VARCHAR2(100);
   v_inStr VARCHAR2(500);
   v_temp VARCHAR2(100);
   v_sql VARCHAR2(1000);
   v_sql1 VARCHAR2(1000);
   v_sql2 VARCHAR2(1000);
   v_sql3 VARCHAR2(1000);
   v_sql4 VARCHAR2(1000);
   v_sql5 VARCHAR2(1000);
   v_sql6 VARCHAR2(1000);
   v_sql7 VARCHAR2(1000);
   v_sql8 VARCHAR2(1000);
   v_sql9 VARCHAR2(1000);
BEGIN

   v_cnt := p_ids.COUNT; --get the # of elements in array

   i:=1;
    WHILE i <= v_cnt LOOP
       v_temp:='to_number('''||p_ids(i)||''')';

    IF (LENGTH(v_inStr) > 0) THEN
    v_inStr:=v_inStr||', '||v_temp ;
    ELSE
    v_inStr:= '('||v_temp ;
    END IF;

    i := i+1;
 END LOOP;

    v_inStr:=v_inStr||')' ;
    BEGIN
      v_sql9:= 'update ER_BULK_UPLOAD set LAST_MODIFIED_BY=''' || p_userId || ''', IP_ADD=''' || p_ipAdd || ''', LAST_MODIFIED_DATE=''' || SYSDATE || ''' where  FK_BULK_TEMPLATE_MASTER in ' || v_inStr;
        EXECUTE IMMEDIATE v_sql9 ;
      COMMIT;
      v_sql8:= 'update ER_BULK_TEMPLATE_DETAIL set LAST_MODIFIED_BY=''' || p_userId || ''', IP_ADD=''' || p_ipAdd || ''', LAST_MODIFIED_DATE=''' || SYSDATE || ''' where  FK_BULK_TEMPLATE_MASTER in ' || v_inStr;
        EXECUTE IMMEDIATE v_sql8 ;
      COMMIT;
      v_sql7:= 'update ER_BULK_TEMPLATE_MASTER set LAST_MODIFIED_BY=''' || p_userId || ''', IP_ADD=''' || p_ipAdd || ''', LAST_MODIFIED_DATE=''' || SYSDATE || ''' where  PK_BULK_TEMPLATE_MASTER in ' || v_inStr;
        EXECUTE IMMEDIATE v_sql7 ;
      commit;
      v_sql6:= 'update ER_BULK_UPLOAD_ERRORLOG set LAST_MODIFIED_BY=''' || p_userId || ''', IP_ADD=''' || p_ipAdd || ''', LAST_MODIFIED_DATE=''' || SYSDATE || ''' where PK_BULK_UPLOAD_ERRORLOG in(select l.PK_BULK_UPLOAD_ERRORLOG from ER_BULK_UPLOAD_ERRORLOG l,ER_BULK_TEMPLATE_DETAIL d,ER_BULK_TEMPLATE_MASTER m where  M.PK_BULK_TEMPLATE_MASTER in '|| v_inStr|| ' and M.PK_BULK_TEMPLATE_MASTER=D.FK_BULK_TEMPLATE_MASTER and L.FK_BULK_TEMPLATE_DETAIL=D.PK_BULK_TEMPLATE_DETAIL)';
        EXECUTE IMMEDIATE v_sql6 ;
      commit;
      v_sql5:= 'update ER_BULK_UPLOAD_ERRORROW set LAST_MODIFIED_BY=''' || p_userId || ''', IP_ADD=''' || p_ipAdd || ''', LAST_MODIFIED_DATE=''' || SYSDATE || '''  where PK_BULK_UPLOAD_ERRORROW in(select l.PK_BULK_UPLOAD_ERRORROW from ER_BULK_UPLOAD_ERRORROW l,ER_BULK_UPLOAD d,ER_BULK_TEMPLATE_MASTER m where M.PK_BULK_TEMPLATE_MASTER in '|| v_inStr|| ' and M.PK_BULK_TEMPLATE_MASTER=D.FK_BULK_TEMPLATE_MASTER and L.FK_BULK_UPLOAD=D.PK_BULK_UPLOAD)';
        EXECUTE IMMEDIATE v_sql5 ;
      COMMIT;	  
      v_sql4:= 'delete from ER_BULK_UPLOAD_ERRORLOG where PK_BULK_UPLOAD_ERRORLOG in(select l.PK_BULK_UPLOAD_ERRORLOG from ER_BULK_UPLOAD_ERRORLOG l,ER_BULK_TEMPLATE_DETAIL d,ER_BULK_TEMPLATE_MASTER m where  M.PK_BULK_TEMPLATE_MASTER in '|| v_inStr|| ' and M.PK_BULK_TEMPLATE_MASTER=D.FK_BULK_TEMPLATE_MASTER and L.FK_BULK_TEMPLATE_DETAIL=D.PK_BULK_TEMPLATE_DETAIL)';
        EXECUTE IMMEDIATE v_sql4 ;
      commit;
      v_sql3:= 'delete from ER_BULK_UPLOAD_ERRORROW  where PK_BULK_UPLOAD_ERRORROW in(select l.PK_BULK_UPLOAD_ERRORROW from ER_BULK_UPLOAD_ERRORROW l,ER_BULK_UPLOAD d,ER_BULK_TEMPLATE_MASTER m where M.PK_BULK_TEMPLATE_MASTER in '|| v_inStr|| ' and M.PK_BULK_TEMPLATE_MASTER=D.FK_BULK_TEMPLATE_MASTER and L.FK_BULK_UPLOAD=D.PK_BULK_UPLOAD)';
        EXECUTE IMMEDIATE v_sql3 ;
      COMMIT;
      v_sql2:= 'delete from ER_BULK_UPLOAD where FK_BULK_TEMPLATE_MASTER in ' || v_inStr;
        EXECUTE IMMEDIATE v_sql2 ;
      commit;
          v_sql1:= 'delete from ER_BULK_TEMPLATE_DETAIL where  FK_BULK_TEMPLATE_MASTER in ' || v_inStr;
        EXECUTE IMMEDIATE v_sql1 ;
      commit;
          v_sql:='delete from ER_BULK_TEMPLATE_MASTER where PK_BULK_TEMPLATE_MASTER in ' || v_inStr;
      EXECUTE IMMEDIATE V_SQL ;
       COMMIT;
      EXCEPTION  WHEN OTHERS THEN
        P('ERROR');
        o_ret:=-1;
       RETURN;
      END ;--end of insert begin
   o_ret:=0;

 END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,308,8,'08_SP_DELETE_BULKMAPPING.sql',sysdate,'v9.3.0 #709');

commit; 
 