DECLARE
filterMID number := 0;
filterFK number := 0;
v_exists NUMBER := 0;
begin
	select PK_REPFILTER into filterFK from er_repfilter where REPFILTER_KEYWORD = 'inclValMsgChk';
	select count(*) into v_exists from er_repfiltermap where fk_repfilter = filterFK and repfiltermap_repcat = 'data safety monitoring';
	
	if (v_exists = 0) then	
		select Max(PK_REPFILTERMAP)+1 into filterMID from er_repfiltermap;

		INSERT INTO er_repfiltermap (pk_repfiltermap,fk_repfilter,repfiltermap_repcat,repfiltermap_seq) 
		VALUES (filterMID,filterFK, 'data safety monitoring',17);
			
		COMMIT;
	end if;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,308,2,'02_er_repfiltermap.sql',sysdate,'v9.3.0 #709');

commit;
