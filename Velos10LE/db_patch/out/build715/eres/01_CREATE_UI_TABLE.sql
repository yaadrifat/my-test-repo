set define off;
set serveroutput on;

DECLARE
  v_item_exists number := 0;  
BEGIN
select count(*) into v_item_exists from user_tables where TABLE_NAME = 'UI_FLX_PAGE';
if (v_item_exists = 0) then
  execute immediate '
CREATE TABLE ERES.UI_FLX_PAGE
(
  PAGE_ID   VARCHAR2(255) NOT NULL,
  PAGE_DEF  CLOB,
  PAGE_JS   CLOB
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
NOMONITORING  
  ';
  execute immediate '
ALTER TABLE ERES.UI_FLX_PAGE ADD 
CONSTRAINT UI_FLX_PAGE_U01
 UNIQUE (PAGE_ID)
 ENABLE
 VALIDATE
  ';
  dbms_output.put_line('Table UI_FLX_PAGE created');
else
  dbms_output.put_line('Table UI_FLX_PAGE already exists');
end if;

END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,314,1,'01_CREATE_UI_TABLE.sql',sysdate,'v9.3.0 #715');

commit;