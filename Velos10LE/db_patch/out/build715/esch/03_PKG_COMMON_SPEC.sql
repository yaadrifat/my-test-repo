create or replace PACKAGE  "PKG_COMMON" as
FUNCTION SCH_GETPATCODE  (p_patprot NUMBER)
    RETURN  varchar2 ;

FUNCTION VALIDATENAME (
   EVENTID     NUMBER,
   USERID      NUMBER,
   EVENTNAME   VARCHAR2,
   EVENTTYPE   VARCHAR2, CHAINID VARCHAR2, CALASSOCTO VARCHAR2 )
   RETURN varchar2 ;


/*
** The function checks if the event name is already existing or not. It returns a -1 if a duplicate
** is found. -1 is passed in the EVENTID parameter if the name is to be checked during an Insert.
** To check during an UPDATE the event_id is passed in the EVENTID parameter. This is used to check the name
** in all other events other than this particular EVENT_ID
**
** Return Value
** -1 on finding a duplicate
**  0 on not finding a duplicate
*/

FUNCTION SCH_GETMAIL (p_message varchar2, p_params varchar2)
    RETURN  varchar2 ;

FUNCTION SCH_GETCELLMAILMSG (p_mesgtype varchar2)
    RETURN  varchar2 ;

FUNCTION SCH_GETMAILMSG (p_mesgtype varchar2)
    RETURN  varchar2 ;
    
FUNCTION SCH_GETMSGSUBJECT (p_mesgtype varchar2)
    RETURN  varchar2 ;
end pkg_common ;

/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,314,3,'03_PKG_COMMON_SPEC.sql',sysdate,'v9.3.0 #715');

commit;