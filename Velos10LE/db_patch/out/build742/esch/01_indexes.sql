DECLARE 
  index_count INTEGER; 
BEGIN 
  SELECT COUNT(*) into index_count FROM user_indexes WHERE
    index_name = 'IDX_SCH_EVENTS1_FK_VISITASSOC' and upper(table_owner) = 'ESCH';
  if (index_count = 0) then
    execute immediate 'CREATE INDEX ESCH.IDX_SCH_EVENTS1_FK_VISITASSOC ON ESCH.SCH_EVENTS1
      (fk_assoc,FK_VISIT)
       LOGGING
       NOPARALLEL';
  	dbms_output.put_line ('Index created IDX_SCH_EVENTS1_FK_VISIT');
	ELSE
		dbms_output.put_line ('Index not created IDX_SCH_EVENTS1_FK_VISIT');
	END IF;

END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,341,1,'01_indexes.sql',sysdate,'v9.3.0 #742');

commit;