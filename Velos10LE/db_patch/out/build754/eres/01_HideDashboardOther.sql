set define off;
DECLARE
	v_obj_subtyp number := 0;
BEGIN
	SELECT COUNT(*) INTO v_obj_subtyp FROM er_codelst WHERE codelst_type='newdashboard' and codelst_subtyp='dbothers';
	IF (v_obj_subtyp!=0) THEN
		update er_codelst 
		set codelst_hide='Y' 
		where codelst_subtyp='dbothers' and codelst_type='newdashboard';
	END IF;
	commit;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,353,1,'01_HideDashboardOther.sql',sysdate,'v10 #754');

commit;

