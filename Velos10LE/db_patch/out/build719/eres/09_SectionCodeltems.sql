set define off;

update er_codelst set codelst_desc = '3.1.1. Tumor Assessments' where codelst_type  ='section' and codelst_desc = '3.1.1.Tumor Assessments';

update er_codelst set codelst_desc = '3.1.3. Continued Access to Medication' where codelst_type  ='section' and codelst_desc = '3.1.3.Continued Access to Medication';

commit;

update er_studysec set studysec_name = '3.1.1. Tumor Assessments' where studysec_name = '3.1.1.Tumor Assessments';

update er_studysec set studysec_name = '3.1.3. Continued Access to Medication' where studysec_name = '3.1.3.Continued Access to Medication';

commit;

update er_codelst set codelst_subtyp = 'section_' || replace(rtrim(substr(codelst_desc, 1,  instr(codelst_desc, ' ', 1)-1),'.'), '.','_')
where codelst_type = 'section' and 
length('section_' || replace(rtrim(substr(codelst_desc, 1,  instr(codelst_desc, ' ', 1)-1),'.'), '.','_')) < = 15 and codelst_subtyp is null;

commit;

declare
v_pk NUMBER := 0;
begin
  for codeRec in (
  select fk_account, codelst_subtyp from (select fk_account, codelst_subtyp, count(*) cnt from er_codelst where codelst_type ='section' group by fk_account, codelst_subtyp) t where cnt > 1
  )
  loop
    select min(pk_codelst) into v_pk from er_codelst where codelst_type = 'section' and codelst_subtyp = codeRec.codelst_subtyp and fk_account = codeRec.fk_account;
   
    delete from er_codelst where codelst_type ='section' and codelst_subtyp = codeRec.codelst_subtyp and fk_account = codeRec.fk_account and pk_codelst <> v_pk;
  end loop;

  commit;
end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,318,9,'09_SectionCodeltems.sql',sysdate,'v9.3.0 #719');

commit;  