set define off;
set serveroutput on;

DECLARE
  v_item_exists number := 0;  
BEGIN
select count(*) into v_item_exists from user_tab_cols where TABLE_NAME = 'ER_REVIEW_BOARD'
and COLUMN_NAME = 'FK_CODELST_REVBOARD';
if (v_item_exists = 0) then
  execute immediate 'ALTER TABLE ERES.ER_REVIEW_BOARD ADD (FK_CODELST_REVBOARD NUMBER)';
  dbms_output.put_line('Col FK_CODELST_REVBOARD added');
else
  dbms_output.put_line('Col FK_CODELST_REVBOARD already exists');
end if;

END;
/

COMMENT ON COLUMN 
ERES.ER_REVIEW_BOARD.FK_CODELST_REVBOARD IS 
'This column stores the review board for the status. maps to er_codelst, codelst_type=rev_board';

DECLARE
  v_item_exists number := 0;  
BEGIN
select count(*) into v_item_exists from user_tab_cols where TABLE_NAME = 'ER_STUDYSTAT'
and COLUMN_NAME = 'EXTERNAL_LINK';
if (v_item_exists = 0) then
  execute immediate 'ALTER TABLE ERES.ER_STUDYSTAT ADD (EXTERNAL_LINK VARCHAR2(4000 BYTE))';
  dbms_output.put_line('Col EXTERNAL_LINK added');
else
  dbms_output.put_line('Col EXTERNAL_LINK already exists');
end if;

END;
/

COMMENT ON COLUMN 
ERES.ER_STUDYSTAT.EXTERNAL_LINK IS 
'This column stores the URL to an external site which added this status';



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,318,1,'01_AlterTables.sql',sysdate,'v9.3.0 #719');

commit;