create or replace
FUNCTION "F_GETCOLORFLAG" ( from_res number,study_id number,linked_to number)
RETURN VARCHAR
IS
TYPE asc_arr_t IS TABLE OF varchar2(4000)  INDEX BY  varchar(100);
v_retvalues VARCHAR2(4000);
 p_query number;
   p1count number;
    p2count number;
     p3count number;
      p4count number;
      p5count number;

       p6count number;
       v_count number;
       color_flag varchar2(100);
status_arr asc_arr_t;
 query_arr asc_arr_t;
 threshold_days number;
 empty_data varchar2(10);
 threshold_count number;
 threshold_day_var varchar2(100);
 CHECK_FLAG NUMBER;
 pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'f_getcolorflag', pLEVEL  => Plog.LFATAL);
 -- Author:
 --Vivek Kumar Jha
BEGIN
p6count:=0;
   p5count:=0;
   p4count:=0;
   p3count:=0;
   p2count:=0;
   p1count:=0;
   empty_data:='';
   --***************************find threshold of study****************************

   select count(*) into threshold_count from (select studyId_id from er_studyid where fk_study=study_id and fk_codelst_idtype =(select pk_codelst from er_codelst where codelst_subtyp='thresh_days'));
   if threshold_count=0 then
   empty_data:='empty';  --no data found
   else

   select studyId_id into  threshold_day_var from er_studyid where fk_study=study_id and fk_codelst_idtype =(select pk_codelst from er_codelst where codelst_subtyp='thresh_days');
    select is_number(threshold_day_var) INTO CHECK_FLAG FROM dual;
    IF CHECK_FLAG=0 THEN
     empty_data:='empty';
    -- Plog.fatal(pCTX,'sTRINGdATE:' || empty_data) ;
    ELSE
    threshold_days:=TO_NUMBER(threshold_day_var);
    --Plog.fatal(pCTX,'tHRESOLDDATA:' || threshold_days) ;
    --dbms_output.put_line(' CONVERT INTO nUMBER FORM STRING :'||threshold_days );
    END IF;
   end if;


--Plog.fatal(pCTX,'vvvvvvvvvv:' || from_res||' study:'||study_id||' Threshold_day:'||threshold_days) ;

--********************store query status in collection************************************
for x in(select codelst_type,codelst_subtyp,codelst_desc from  er_codelst where codelst_type='query_status' )
loop
status_arr(x.codelst_subtyp):=x.codelst_desc;
End loop;
--****************************store query Type in collection****************************
for x in(select codelst_type,codelst_subtyp, codelst_desc from  er_codelst where codelst_type='form_query' )
loop
query_arr(x.codelst_subtyp):=x.codelst_desc;
End loop;
--****************************count total query of form response*************************
select count(*) into v_count from (select formquery.pk_formQuery ,formstatus.fk_codelst_queryType ,(select codelst_desc from er_codelst where pk_codelst=formstatus.fk_codelst_querytype) As querytype
, formstatus.fk_codelst_querystatus,
 (select codelst_desc from er_codelst where pk_codelst=formstatus.fk_codelst_querystatus) As querystatus,
 TRUNC(sysdate) - to_date(formstatus.ENTERED_ON) as age
 from er_formquery formquery
 inner join er_formquerystatus formstatus on formquery.pk_formquery=formstatus.fk_formquery
  inner join
  (select max(pk_formquery) AS form_query, fk_field from er_formquery
  where fk_querymodule=from_res group by fk_field) aa on aa.form_query=formquery.pk_formQuery
  where formquery.fk_querymodule=from_res
  and formquery.querymodule_linkedto=linked_to
  and formstatus.fk_codelst_queryType is not null);
  dbms_output.put_line('count '|| v_count);

  --**************************set color flag of form response****************************

  if v_count>0 then
  for sta in (select formquery.pk_formQuery ,formstatus.fk_codelst_queryType ,(select codelst_desc from er_codelst where pk_codelst=formstatus.fk_codelst_querytype) As querytype
, formstatus.fk_codelst_querystatus,
 (select codelst_desc from er_codelst where pk_codelst=formstatus.fk_codelst_querystatus) As querystatus,
 TRUNC(sysdate) - to_date(formstatus.ENTERED_ON) as age
 from er_formquery formquery
 inner join er_formquerystatus formstatus on formquery.pk_formquery=formstatus.fk_formquery
  inner join
  (select max(pk_formquery) AS form_query, fk_field from er_formquery
  where fk_querymodule=from_res group by fk_field) aa on aa.form_query=formquery.pk_formQuery
  where formquery.fk_querymodule=from_res
  and formquery.querymodule_linkedto=linked_to
  and formstatus.fk_codelst_queryType is not null)
  loop
  if empty_data='empty' then
  threshold_days:=sta.age+1;
  end if;
   if sta.querytype=query_arr('normal')  and (sta.querystatus=status_arr('open')or sta.querystatus=status_arr('re-opened')) then
 --color_flag:='purple';
 p4count:=p4count+1;
 elsif  sta.querytype=query_arr('high_priority')  and (sta.querystatus=status_arr('open')or sta.querystatus=status_arr('re-opened')) then
  p1count:=p1count+1;
 -- color_flag:='red';
  elsif (sta.querystatus=status_arr('open')or sta.querystatus=status_arr('re-opened'))and sta.age > threshold_days then
  p1count:=p1count+1;
  -- color_flag:='red';
  elsif  sta.querytype=query_arr('high_priority')  and sta.querystatus=status_arr('mon_rev') then
 -- dbms_output.put_line('orange');
   p2count:=p2count+1;
  --color_flag:='orange';
  elsif  sta.querytype=query_arr('normal')  and sta.querystatus=status_arr('mon_rev') then
  p3count:=p3count+1;
   elsif   sta.querystatus=status_arr('resolved') then
  p6count:=p6count+1;
  
  -- Above Condition not found 
  else
  color_flag:=' ';
  end if;

   End loop;
   if p1count>=1  then
   color_flag:='red';
   elsif p2count>=1 then
 color_flag:='orange';
   elsif p3count>=1 then
   color_flag:='yellow';
   elsif p4count>=1 then
   color_flag:='purple';
    elsif p6count=v_count then
   color_flag:='green';
   end if;
   else
   --dbms_output.put_line('White');
   color_flag:='white';
   end if;
  RETURN color_flag;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,338,0,'01_f_colorflag_patch.sql',sysdate,'v9.3.0 #739');

commit;
