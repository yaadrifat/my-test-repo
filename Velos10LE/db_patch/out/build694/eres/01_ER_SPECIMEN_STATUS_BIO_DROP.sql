set define off;
declare
v_TrgCount number;
begin

select count(*) into v_TrgCount from USER_TRIGGERS where table_name='ER_SPECIMEN_STATUS' and trigger_name ='ER_SPECIMEN_STATUS_BIO'; 

if v_TrgCount=1 then

execute immediate 'DROP TRIGGER ER_SPECIMEN_STATUS_BIO';
end if;

end;
/
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,293,1,'01_ER_SPECIMEN_STATUS_BIO_DROP.sql',sysdate,'v9.3.0 #694');

commit;