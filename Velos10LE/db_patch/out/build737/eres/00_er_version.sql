set define off; 

UPDATE ER_CTRLTAB SET CTRL_VALUE = 'v9.3.0 #737' 
where CTRL_KEY = 'app_version' ; 

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,336,0,'00_er_version.sql',sysdate,'v9.3.0 #737');

commit;

