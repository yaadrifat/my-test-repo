set define off;

DELETE from er_repxsl where pk_repxsl in (110,159,183,184);

commit;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,342,6,'06_er_repxsl_del.sql',sysdate,'v9.3.0 #743');

commit;
