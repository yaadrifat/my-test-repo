set define off;

DECLARE
    i INTEGER;
BEGIN
    i := 0;
    SELECT COUNT(*) INTO i FROM user_indexes WHERE index_name = 'IDX_ESCH_AUDITROW_RAID';
    IF i > 0 THEN
        EXECUTE IMMEDIATE 'DROP INDEX IDX_ESCH_AUDITROW_RAID';
    END IF;
END;
/

declare
v_indexName VARCHAR2(200) := '';
begin
	select a1.Index_name INTO v_indexName from USER_IND_COLUMNS a1 where a1.COLUMN_NAME='RAID' and a1.table_name = 'AUDIT_ROW' 
	and a1.Index_name not in (select b1.Index_name from USER_IND_COLUMNS b1 where (a1.table_name= b1.table_name and a1.index_name = b1.index_name) and b1.COLUMN_NAME <> 'RAID');
	
	EXCEPTION WHEN NO_DATA_FOUND THEN
		v_indexName := v_indexName;

	if (length(v_indexName) IS NULL) then
		
EXECUTE IMMEDIATE 'CREATE INDEX ESCH.IDX_ESCH_AUDITROW_RAID ON ESCH.AUDIT_ROW
(RAID)
LOGGING
TABLESPACE ESCH_LARGE_INDX
NOPARALLEL';

end if;
	
end;
/

DECLARE
    i INTEGER;
BEGIN
    i := 0;
    SELECT COUNT(*) INTO i FROM user_indexes WHERE index_name = 'IDX_ESCH_AUDITROW_RID';
    IF i > 0 THEN
        EXECUTE IMMEDIATE 'DROP INDEX IDX_ESCH_AUDITROW_RID';
    END IF;
END;
/

declare
v_indexName VARCHAR2(200) := '';
begin
	select a1.Index_name INTO v_indexName from USER_IND_COLUMNS a1 where a1.COLUMN_NAME='RID' and a1.table_name = 'AUDIT_ROW' 
	and a1.Index_name not in (select b1.Index_name from USER_IND_COLUMNS b1 where (a1.table_name= b1.table_name and a1.index_name = b1.index_name) and b1.COLUMN_NAME <> 'RID');
	
	EXCEPTION WHEN NO_DATA_FOUND THEN
		v_indexName := v_indexName;

	if (length(v_indexName) IS NULL) then
		
EXECUTE IMMEDIATE 'CREATE INDEX ESCH.IDX_ESCH_AUDITROW_RID ON ESCH.AUDIT_ROW
(RID)
LOGGING
TABLESPACE ESCH_LARGE_INDX
NOPARALLEL';

end if;
	
end;
/

DECLARE
    i INTEGER;
BEGIN
    i := 0;
    SELECT COUNT(*) INTO i FROM user_indexes WHERE index_name = 'IDX_ESCH_AUDITROW_USER_NAME';
    IF i > 0 THEN
        EXECUTE IMMEDIATE 'DROP INDEX IDX_ESCH_AUDITROW_USER_NAME';
    END IF;
END;
/

declare
v_indexName VARCHAR2(200) := '';
begin
	select a1.Index_name INTO v_indexName from USER_IND_COLUMNS a1 where a1.COLUMN_NAME='USER_NAME' and a1.table_name = 'AUDIT_ROW' 
	and a1.Index_name not in (select b1.Index_name from USER_IND_COLUMNS b1 where (a1.table_name= b1.table_name and a1.index_name = b1.index_name) and b1.COLUMN_NAME <> 'USER_NAME');
	
	EXCEPTION WHEN NO_DATA_FOUND THEN
		v_indexName := v_indexName;

	if (length(v_indexName) IS NULL) then
		
EXECUTE IMMEDIATE 'CREATE INDEX ESCH.IDX_ESCH_AUDITROW_USER_NAME ON ESCH.AUDIT_ROW
(USER_NAME)
LOGGING
TABLESPACE ESCH_LARGE_INDX
NOPARALLEL';

end if;
	
end;
/

--
DECLARE
    i INTEGER;
BEGIN
    SELECT COUNT(*) INTO i FROM user_indexes WHERE index_name = 'IDX_SCH_EVENTS1_EVENTID';
    IF i > 0 THEN
        EXECUTE IMMEDIATE 'DROP INDEX IDX_SCH_EVENTS1_EVENTID';
    END IF;

    i := 0;
    SELECT COUNT(*) INTO i FROM user_indexes WHERE index_name = 'IDX_SCH_EVRES_TRACK_EVSTAT';
    IF i > 0 THEN
        EXECUTE IMMEDIATE 'DROP INDEX IDX_SCH_EVRES_TRACK_EVSTAT';
    END IF;
    
    i := 0;
    SELECT COUNT(*) INTO i FROM user_indexes WHERE index_name = 'IDX_ESCH_AUDITCOL_RAID';
    IF i > 0 THEN
        EXECUTE IMMEDIATE 'DROP INDEX IDX_ESCH_AUDITCOL_RAID';
    END IF;
        
END;
/

declare
v_indexName VARCHAR2(200) := '';
begin
	select a1.Index_name INTO v_indexName from USER_IND_COLUMNS a1 where a1.COLUMN_NAME='EVENT_ID' and a1.table_name = 'SCH_EVENTS1' 
	and a1.Index_name not in (select b1.Index_name from USER_IND_COLUMNS b1 where (a1.table_name= b1.table_name and a1.index_name = b1.index_name) and b1.COLUMN_NAME <> 'EVENT_ID');
  
  EXCEPTION WHEN NO_DATA_FOUND THEN
		v_indexName := v_indexName;
    
	if (length(v_indexName) IS NULL) then
EXECUTE IMMEDIATE 'CREATE UNIQUE INDEX ESCH.IDX_SCH_EVENTS1_EVENTID ON ESCH.SCH_EVENTS1 (EVENT_ID DESC) 
LOGGING 
TABLESPACE ESCH_LARGE_INDX 
PCTFREE 10 
INITRANS 2 
STORAGE 
( 
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1 
  MAXEXTENTS UNLIMITED 
  BUFFER_POOL DEFAULT 
) 
NOPARALLEL';

end if;
end;
/

declare
v_indexName VARCHAR2(200) := '';
begin
	select a1.Index_name INTO v_indexName from USER_IND_COLUMNS a1 where a1.COLUMN_NAME='FK_EVENTSTAT' and a1.table_name = 'SCH_EVRES_TRACK' 
	and a1.Index_name not in (select b1.Index_name from USER_IND_COLUMNS b1 where (a1.table_name= b1.table_name and a1.index_name = b1.index_name) and b1.COLUMN_NAME <> 'FK_EVENTSTAT');
	
  EXCEPTION WHEN NO_DATA_FOUND THEN
		v_indexName := v_indexName; 
    
	if (length(v_indexName) IS NULL) then

EXECUTE IMMEDIATE 'CREATE INDEX ESCH.IDX_SCH_EVRES_TRACK_EVSTAT ON ESCH.SCH_EVRES_TRACK (FK_EVENTSTAT) 
TABLESPACE ESCH_LARGE_INDX 
STORAGE 
( 
  BUFFER_POOL DEFAULT 
)';

	end if;

end;
/

declare
v_indexName VARCHAR2(200) := '';
begin
	select a1.Index_name INTO v_indexName from USER_IND_COLUMNS a1 where a1.COLUMN_NAME='RAID' and a1.table_name = 'AUDIT_COLUMN' 
	and a1.Index_name not in (select b1.Index_name from USER_IND_COLUMNS b1 where (a1.table_name= b1.table_name and a1.index_name = b1.index_name) and b1.COLUMN_NAME <> 'RAID');
	
  EXCEPTION WHEN NO_DATA_FOUND THEN
		v_indexName := v_indexName;
    
	if (length(v_indexName) IS NULL) then

EXECUTE IMMEDIATE 'CREATE INDEX ESCH.IDX_ESCH_AUDITCOL_RAID ON ESCH.AUDIT_COLUMN
(RAID)
LOGGING
TABLESPACE ESCH_LARGE_INDX
NOPARALLEL';

	end if;
end;
/



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,342,4,'04_esch_indexes.sql',sysdate,'v9.3.0 #743');

commit;