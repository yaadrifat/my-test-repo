package com.velos.eres.service.util;


import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Before;
import org.junit.Test;


public class VelosResourceBundleTests {

    @Before
    public void setUp() throws Exception {
        if (System.getProperty("ERES_HOME") == null) {
            System.setProperty("ERES_HOME", "src/test/resources/");
        }
    }
    
    @Test
    public void testFileExistence() {
    	File file = new File(System.getProperty("ERES_HOME")+"/labelBundle.properties");
    	assertTrue(file.exists());
    }
    
    @Test
    public void testOneString() {
        assertTrue("Patients".equals(LC.Pat_Patients));
    }

}
