formatForm.js

Use it to fix formatting of forms. Typically onLoad. For test, I added it as part of the comment field's rich text format. Included the JS as:

<script src="formatForm.js" type="text/javascript" language="JavaScript"></script>

TEST