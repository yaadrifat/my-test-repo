<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id="studyrights" scope="request" class="com.velos.eres.web.studyRights.StudyRightsJB"/>
<jsp:useBean id="grpB" scope="request" class="com.velos.eres.web.group.GroupJB"/>

<jsp:useBean id="ctrl" scope="session" class="com.velos.eres.business.common.CtrlDao"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*, com.velos.eres.service.util.*"%>
<%

int ret = 0;

String groupId,rights[],cnt,rght="0";

int totrows =0,count=0,id;

String src;

src=request.getParameter("src");

groupId = request.getParameter("groupId"); //Total Rows

String eSign = request.getParameter("eSign");

//Check if budget super user screen need to be displayed after this screen
	String budRight=request.getParameter("budRight");
	budRight=(budRight==null)?"":budRight;



//JM: 13Apr2010: Enh #D-FIN2

String supUserRole = request.getParameter("role");
if (EJBUtil.isEmpty(supUserRole)){
	supUserRole = "0";
}

String defChecked = request.getParameter("defRoleRights");
defChecked = (defChecked==null)?"":defChecked;



HttpSession tSession = request.getSession(true);
 if (sessionmaint.isValidSession(tSession))
   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>

<%
  	String oldESign = (String) tSession.getValue("eSign");


	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	} else {

 String ipAdd = (String) tSession.getValue("ipAdd");
 String usr = null;
 usr = (String) tSession.getValue("userId");

id = Integer.parseInt(groupId);

studyrights.setId(id);

totrows = Integer.parseInt(request.getParameter("totalrows")); //Total Rows

//out.print( "rows" + totrows);

if (totrows > 1){

	rights = request.getParameterValues("rights");

	for (count=0;count<totrows;count++){

		rght = rights[count];

		//out.print("Count" +count +" Rights" +rght);

		cnt = String.valueOf(count);

		studyrights.setGrSeq(cnt);

		studyrights.setFtrRights(rght);

	}

}else{

	rght = request.getParameter("rights");

	studyrights.setGrSeq("1");

	studyrights.setFtrRights(rght);

}





studyrights.setModifiedBy(usr);
studyrights.setIpAdd(ipAdd);
studyrights.updateGrpSupUserStudyRights();

//out.println("UPDATE Study Rights");




//JM: 22Apr2010: #4853

	if (defChecked.equals("on") && !defChecked.equals("") && !supUserRole.equals("0") && !supUserRole.equals("") ){
		//to update the super user rihts in the er_grps table with the default rights of the role from er_ctrl tab
		grpB.copyRoleDefRightsToGrpSupUserRights(EJBUtil.stringToNum(supUserRole), id);
	}

	//set the selected role to the er_grp table
	grpB.setGroupId(id);
	grpB.getGroupDetails();
	grpB.setRoleId(supUserRole);
	grpB.updateGroup();





%>
<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%-- Data was Saved successfully.*****--%> </p>
<%if ((budRight.toLowerCase()).equals("true"))
 {%>
 <META HTTP-EQUIV=Refresh CONTENT="1; URL=supbudgetrights.jsp?mode=M&groupId=<%=groupId%>&srcmenu=<%=src%>&fromPage=groupbrowser">
 <%}else{%>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=groupbrowserpg.jsp?srcmenu=<%=src%>">
<%}%>

<%
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





