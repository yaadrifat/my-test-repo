<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language = "java" import="javax.xml.transform.TransformerFactory,javax.xml.transform.Transformer,javax.xml.transform.Source,javax.xml.transform.stream.StreamSource,javax.xml.transform.stream.StreamResult,javax.xml.transform.TransformerException,javax.xml.transform.TransformerConfigurationException,java.io.*,java.net.URL"%>
<%
 // Output goes in the response stream.
    PrintWriter out1 = new PrintWriter (response.getOutputStream());
    // The servlet returns HTML.
    response.setContentType("text/html");    
    try
    {	
      TransformerFactory tFactory = TransformerFactory.newInstance();
      // Get the XML input document and the stylesheet.
      Source xmlSource = new StreamSource(new URL("file:c:/studyapndx.xml").openStream());
      Source xslSource = new StreamSource(new URL("file:c:/studyapndx.xsl").openStream());
      // Generate the transformer.
      Transformer transformer = tFactory.newTransformer(xslSource);
      // Perform the transformation, sending the output to the response.
      transformer.transform(xmlSource, new StreamResult(out1));
    }
    catch (Exception e)
    {
      out.write(e.getMessage());
      e.printStackTrace(out1);    
    }
    out1.close();
 
%>