<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=MC.M_AddOrEdit_File%><%--Add/Edit File*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
  <SCRIPT Language="javascript">

function  validate(formobj){
	 mode = formobj.mode.value;
	
	 if (mode == 'N')
	 {
	 if(formobj.name.value == "")
	 {
	 	 alert("<%=MC.M_Etr_MandantoryFlds%>");/*alert("Please enter data in all mandatory fields");*****/
	 	 formobj.name.focus();
	 	 return false;
	 }
	 
	     
	 }
	  if(formobj.desc.value == "")
	 {
		  alert("<%=MC.M_Etr_MandantoryFlds%>");/*alert("Please enter data in all mandatory fields");*****/
	 	 formobj.desc.focus();
	 	 return false;
	 }
   
     if(formobj.eSign.value == "")
	 {
    	 alert("<%=MC.M_Etr_MandantoryFlds%>");/*alert("Please enter data in all mandatory fields");*****/
	 	 formobj.eSign.focus();
	 	 return false;
	 }
	 if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
		}
 }


 
 function openwin() {

      window.open("","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=600,height=450")

;}
 

</SCRIPT>

<body>
<br>
<DIV class="popDefault" id="div1"> 
<jsp:useBean id="studySiteApndxB" scope="request" class="com.velos.eres.web.studySiteApndx.StudySiteApndxJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import ="com.velos.eres.business.common.*,java.util.*, com.velos.eres.service.util.EJBUtil" %>
  
<%
 
 
// String eSign = request.getParameter("eSign");

HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))
{
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	 <jsp:include page="include.jsp" flush="true"/>
<% 	
	String accId = (String) tSession.getValue("accountId"); 
	String userId = (String) tSession.getAttribute("userId");
	String studyId = request.getParameter("studyId");
	String studySiteId = request.getParameter("studySiteId");
	String mode = request.getParameter("mode");
	String desc = "";
	
	String studySiteApndxId = "";

	String accMaxStorage = (String) tSession.getAttribute("accMaxStorage");

	int stId=EJBUtil.stringToNum(studyId);
	
	CtrlDao ctrlDao = new CtrlDao();	
	long freeSpace = ctrlDao.getFreeSpace(EJBUtil.stringToNum(accId));

if (mode.equals("N"))
 {
	 com.aithent.file.uploadDownload.Configuration.readSettings("eres");
	com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "studysite");
	String upld=com.aithent.file.uploadDownload.Configuration.UPLOADSERVLET;
	%>

  <form name=upload id="siteApndxFile" action=<%=upld%> METHOD=POST ENCTYPE=multipart/form-data onSubmit=" if (validate(document.upload)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
  <%
}
  else
  {
	studySiteApndxId = (String) request.getParameter("studySiteApndxId");
	studySiteApndxB.setStudySiteApndxId(com.velos.eres.service.util.EJBUtil.stringToNum(studySiteApndxId));  
	studySiteApndxB.getStudySiteApndxDetails();
	desc = studySiteApndxB.getAppendixDescription(); 
  
  %>
  <form name="upload" action="insertStudySiteApndxFile.jsp" METHOD=POST onSubmit=" return validate(document.upload)">
  <%
  }
   %>
    <table width="100%" >
      <tr> 
        <td>
		<%
		if (mode.equals("N"))
		  {
	   %> 
          <P class = "defComments"> <%=MC.M_DocOrFrm_Org%><%--Add documents/forms to your Organization.*****--%>
		  </P>
		 <%
			}
		 else
		  {
		%>
	      <P class = "defComments"> <%=LC.L_Edit_ShortDesc%><%--Edit Short Description*****--%>
		  </P>
	
		<%
		  }
		%>   
		    
        </td>
      </tr>
    </table>
    <table width="100%" >
	  <%
	  if (mode.equals("N"))
	  {
	   %>
      <tr> 
        <td width="35%"> <%=LC.L_File%><%--File*****--%> <FONT class="Mandatory">* </FONT> </td>
        <td width="65%"> 
          <input type=file name=name size=40 >
		  <input type=hidden name=db value='eres'>
		  <input type=hidden name=module value='studysite'>  
		  <input type=hidden name=tableName value='er_studysites_apndx'>
		  <input type=hidden name=columnName value='APNDX_FILE'>
		  <input type=hidden name=pkValue value='0'>
		  <input type=hidden name=pkColumnName value='PK_STUDYSITES_APNDX'>
		   <input type=hidden name=userId value=<%=userId%>>
		   <input type=hidden name=maxFileSize value=<%=freeSpace%>>
			<input type=hidden name=accMaxStorage value=<%=accMaxStorage%>> 
		   
		   		  
	

	
	<input type=hidden name=nextPage value='../../velos/jsp/newOrganization.jsp?mode=M&studyId=<%=studyId%>&studySiteId=<%=studySiteId%>'>
		  

	
		  
        </td>
      </tr>
      <tr> 
        <td width="35%"> </td>
        <td width="65%"> 
          <P class="defComments"> <%=MC.M_Specify_FullFilePath%><%--Specify full path of the file.*****--%> </P>
        </td>
      </tr>
	  <%
	  }
	  %>
      <tr> 
        <td width="35%"> <%=LC.L_Short_Desc%><%--Short Description*****--%> <FONT class="Mandatory" >* </FONT> 
        </td>
        <td width="65%">
		<%
		if (mode.equals("N"))
		  {
	   %>
		   <input type=text name=desc MAXLENGTH=250 size=40>
		<%
			}
		 else
		  {
		%>
		    <input type=text name=desc MAXLENGTH=250 size=40 value="<%=desc%>">
		<%
		  }
		%>   
		   
        </td>
      </tr>
      <tr> 
        <td width="35%"> </td>
        <td width="65%"> 
          <P class="defComments"> <%=MC.M_ShortDescFile_250CharMax%><%--Give a short description of your file (250 char  max.)*****--%> </P>
        </td>
      </tr>
    </table>
   
  	<input type=hidden name=accId value=<%=accId%>>
  	
  	<input type=hidden name=studySiteId value=<%=studySiteId%>>  	
  	<input type=hidden name=studyId value=<%=studyId%>>
	<input type=hidden name=accountId value=<%=accId%>>
	<input type=hidden name=maxFileSize value=<%=freeSpace%>>
	<input type=hidden name=accMaxStorage value=<%=accMaxStorage%>> 
	
	
    <input type="hidden" name="type" value='F'>
    <input type="hidden" name="mode" value="<%=mode%>">

  
    <%
	  if (mode.equals("M"))
	  {
    %>
	   <input type="hidden" name="studySiteApndxId" value="<%=studySiteApndxId%>">
 	<%
	   }
	%>
	  
	  
	<BR>
    
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="siteApndxFile"/>
			<jsp:param name="showDiscard" value="N"/>
    </jsp:include>


	 <table width="100%" >
      <tr> 
        <td > <BR>
          <P class = "defComments"> <FONT class="Mandatory">* </FONT> <%=LC.L_Indicates_MandatoryFlds%><%--Indicates 
            mandatory fields*****--%> </P>
        </td>
      </tr>
    </table>
  </form>
  <%

}//end of if body for session
else
{
%>
<jsp:include page="timeout.html" flush="true"/> 
<%
}
%>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

</BODY>
</DIV>
</HTML>





