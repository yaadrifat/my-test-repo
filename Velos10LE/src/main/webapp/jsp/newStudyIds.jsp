<%!
static final String StudyScreenFlag = "StudyScreen_ON";
static final String Str_input = "input", Str_lookup = "lookup", Str_chkbox = "chkbox", Str_checkbox = "checkbox", Str_dropdown = "dropdown",
	Str_splfld = "splfld", Str_date = "date", Str_RO_input = "readonly-input", Str_Hidden_input = "hidden-input";
static final String Str_selection_single = "single", Str_selection_multi = "multi";
public static final String Str_tarea="textarea";
public static final String Str_tarea_RO="readonly-textarea";

%>
<%
String studyId = request.getParameter("studyId");
//if (StringUtil.stringToNum(studyId)<= 0){
//	return;
//}
%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.StringUtil"%>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.StringUtil"%>
<%@ page import="org.json.*"%>
<jsp:useBean id="altId" scope="request" class="com.velos.eres.web.studyId.StudyIdJB"/>

<%
	HttpSession tSession = request.getSession(true);
	 if (sessionmaint.isValidSession(tSession))
	{
	 %>
	<jsp:include page="sessionlogging.jsp" flush="true"/>

<%

	 String defUserGroup = (String) tSession.getAttribute("defUserGroup");

	 String accountId = (String) tSession.getAttribute("accountId");
	 int studyRights = StringUtil.stringToNum(request.getParameter("studyRights"));

	 //get study ids
	 StudyIdDao sidDao = new StudyIdDao();
	 sidDao = altId.getStudyIds(StringUtil.stringToNum(studyId),defUserGroup);

 	 ArrayList studyIdType  = new ArrayList();
	 ArrayList studyIdTypesDesc = new ArrayList();
	 ArrayList id = new ArrayList();
	 ArrayList alternateIdKey = new ArrayList();
	 ArrayList alternateId = new ArrayList();
 	 ArrayList recordType = new ArrayList();
	 ArrayList dispTypes=new ArrayList();
	 ArrayList dispData=new ArrayList();

	 id = sidDao.getId();
	 studyIdType =  sidDao.getStudyIdType();
	 studyIdTypesDesc = sidDao.getStudyIdTypesDesc();
	 alternateIdKey = sidDao.getAlternateIdKey();
	 alternateId = sidDao.getAlternateId();
	 recordType = sidDao.getRecordType ();
	 dispTypes=sidDao.getDispType();
	 dispData=sidDao.getDispData();

	 String strStudyIdTypesDesc ;
 	 String strAlternateId ;
  	 String strRecordType ;
  	 Integer intStudyIdType;
   	 Integer intId;
	 String disptype="";
	 String dispdata="";
	 String ddStr="";
     String subTypeName="";

%>
<table id="newstudyidTable" class="basetbl" width="100%" border="0" cellspacing="2" cellpadding="2">
<%
	int counter = 0,cbcount=0;
	for (counter = 0; counter <= studyIdType.size() -1 ; counter++)
		{
				strStudyIdTypesDesc = (String) studyIdTypesDesc.get(counter);
 	 			strAlternateId = (String) alternateId.get(counter);
 	 			intStudyIdType = (Integer) studyIdType.get(counter) ; 
 	 			disptype=(String) dispTypes.get(counter);
 	 			subTypeName=(String) alternateIdKey.get(counter);
 	 			 %>
 	 			 <script>
 	 				moreStudyDetFunctions.moreStudyDetCodeJSON["<%=alternateIdKey.get(counter)%>"] = {
 	 						pk: "<%=intStudyIdType%>", 
 	 						fieldType: '<%=(StringUtil.isEmpty(disptype))? Str_input : disptype%>',
 	 						subType:"<%=subTypeName%>"
 	 				};
 	 				
 	 			<%--//alert(moreStudyDetFunctions.moreStudyDetCodeJSON["<%=alternateIdKey.get(counter)%>"]);--%>
 	 			 </script>
 	 			 <%

				dispdata=(String) dispData.get(counter);
				if (disptype==null) disptype="";
 	 			disptype=disptype.toLowerCase();

				if (dispdata==null) dispdata="";
				if (disptype.equals(Str_dropdown)) {
					if (null != dispdata){
						dispdata = StringUtil.replace(dispdata, "alternateId", "alternateId"+intStudyIdType);
					}
				}

 	 			if (strAlternateId == null)
 	 				strAlternateId = "";

  	 			strRecordType = (String) recordType.get(counter);
   	 			intId = (Integer) id.get(counter) ;

			%>
			<%if (Str_Hidden_input.equals(disptype)){
				%>
				<input type="hidden" readonly id="alternateId<%=intStudyIdType%>" name="alternateId<%=intStudyIdType%>" value="<%=strAlternateId.trim()%>" size="25" maxlength="100"/>
				<input type = "hidden" name = "recordType<%=intStudyIdType%>" value = "<%=strRecordType %>" >
				<input type = "hidden" name = "id<%=intStudyIdType%>" value = "<%=intId%>" >
				<input type = "hidden" name = "studyIdType" value = "<%=intStudyIdType%>" >
				<input type = "hidden" name = "studyId" value = "<%=studyId%>" >
				<%
				continue;
			}	
			%>

	<%if (Str_splfld.equals(disptype)){ %>
	<tr id="<%="alternateId"+intStudyIdType%>" name="<%="alternateId"+intStudyIdType%>">
	<%} else {%>
	<tr>  <%--Added this --%>
	<%} %>
		     <td width="20%" >
				  <%= strStudyIdTypesDesc %> 
			 </td>
		     <td width="80%" > <%--[<%=alternateIdKey.get(counter)%>]&nbsp; --%>
		     <%if (Str_lookup.equals(disptype)){ %> 
			    <%//lookup
			    String lkpKeyworkStr = "";
			    int lkpPK = 0;
			    try{
				    JSONObject lookupJSON = new JSONObject(dispdata);
				    lkpPK = StringUtil.stringToNum(""+lookupJSON.get("lookupPK"));
				    if (lkpPK <= 0){
				    	System.out.println("Study More Details: invalid lookup PK");
				    	continue;
				    }

				    JSONArray mappingArray = (JSONArray)lookupJSON.get("mapping");
					for (int indx=0; indx < mappingArray.length(); indx++){
						JSONObject checkboxJSON = (JSONObject)mappingArray.get(indx);
				        try{
				        	String source = checkboxJSON.getString("source").trim();
				            String target = checkboxJSON.getString("target").trim();
				            if (StringUtil.isEmpty(source) || StringUtil.isEmpty(target)) continue;

				            target = ("alternateId".equals(target))? "alternateId"+intStudyIdType : target;

				            if (!StringUtil.isEmpty(lkpKeyworkStr))
								lkpKeyworkStr += "~";
				            lkpKeyworkStr += target+"|"+source;
				        }catch(Exception e){
				        	System.out.println("More Study Details: invalid lookup mapping");
				        }
					}
					%>
					<input class='readonly-input' type="text" id="alternateId<%=intStudyIdType%>" name="alternateId<%=intStudyIdType%>" readonly value="<%=strAlternateId.trim()%>" size="25" maxlength="100"/>
					<%if (lkpPK > 0){
						String selection = "";
						try {
				    		selection = (String)lookupJSON.get("selection");
						} catch (Exception e){}
				    	selection = (StringUtil.isEmpty(selection))? Str_selection_single : selection;
				    	%>
				    	<%if (Str_selection_single.equals(selection)){%>
						<A id="alternateId<%=intStudyIdType%>Link" href=# onClick="moreStudyDetFunctions.openLookup(<%=lkpPK %>,'<%=lkpKeyworkStr%>')"><%=LC.L_Select%><%--Select*****--%></A>
						<%} %>
						<%if (Str_selection_multi.equals(selection)){%>
						<A id="alternateId<%=intStudyIdType%>Link" href=# onClick="moreStudyDetFunctions.openMultiLookup(<%=lkpPK %>,'<%=lkpKeyworkStr%>')"><%=LC.L_Select%><%--Select*****--%></A>
						<%} %>
					<%}
			    } catch (Exception e){
			    	//e.printStackTrace();
			    	System.out.println("More Study Details: lookup configuration error!");
			    }			   
			} else if (Str_chkbox.equals(disptype) || Str_checkbox.equals(disptype)){%>
				<%//checkbox 
				if (!StringUtil.isEmpty(dispdata)){
					//checkbox-group
					String[] strAlternateIds = strAlternateId.split(",");
					try{
						JSONObject chkboxesJSON = new JSONObject(dispdata);
					    int chkColCnt = 0;
					    try{
					    	chkColCnt = StringUtil.stringToNum(""+chkboxesJSON.get("chkColCnt"));
					    } catch(Exception e){

					    }
					    chkColCnt = (chkColCnt <= 0) ? 5 : chkColCnt;

					    try{
							JSONArray checkboxArray = chkboxesJSON.getJSONArray("chkArray");

							for (int indx=0; indx < checkboxArray.length(); indx++){
								JSONObject checkboxJSON = (JSONObject)checkboxArray.get(indx);
						        try{
									String data = checkboxJSON.getString("data");
									String display = checkboxJSON.getString("display");
									String checked = "";
									if((","+strAlternateId+",").indexOf(","+data+",") > -1){
										checked = "checked";
									}
									if (indx % chkColCnt == 0){
									%>
										<br>
									<%
									}
					             %>
									<input type="checkbox" id="alternateId<%=intStudyIdType%>Checks<%=indx%>" name="alternateId<%=intStudyIdType%>Checks" value="<%=data.trim()%>" onClick="moreStudyDetFunctions.setValue4ChkBoxGrp(this,<%=intStudyIdType%>)" size = "25" maxlength = "100" <%=checked%>> <%=display%>
								<%
						       	}catch(Exception e){}
							}
				        }catch(Exception e){
				        	//e.printStackTrace();
					    	System.out.println("More Study Details: checkbox configuration error!");
				        }
					}catch(Exception e){
			        	//e.printStackTrace();
				    	System.out.println("More Study Details: checkbox configuration error!");
			        }
					%>
					<input type="hidden" id="alternateId<%=intStudyIdType%>" name="alternateId<%=intStudyIdType%>" value="<%=strAlternateId.trim()%>" size = "25" maxlength = "100" >
					<%
				} else {
					cbcount=cbcount+1;
					%>
					<input type = "hidden" id = "alternateId<%=intStudyIdType%>" name = "alternateId<%=intStudyIdType%>" value = "<%=strAlternateId.trim()%>" size = "25" maxlength = "100" >
					<%if ((strAlternateId.trim()).equals("Y")){%>
					 <input type="checkbox" name="alternate" value="<%=strAlternateId.trim()%>" onClick="moreStudyDetFunctions.setValue(moreStudyDetFunctions.formObj,<%=intStudyIdType%>,<%=cbcount%>)" checked>
					<% }else{%>
					  <input type="checkbox"  name="alternate" value="<%=strAlternateId.trim()%>" onClick="moreStudyDetFunctions.setValue(moreStudyDetFunctions.formObj,<%=intStudyIdType%>,<%=cbcount%>)">
					<%}%>
				<%} %>
			<%} else if (Str_dropdown.equals(disptype)) {
				//dropdown
				if (ddStr.length()==0) ddStr=(intStudyIdType)+":"+strAlternateId;
					else ddStr=ddStr+"||"+(intStudyIdType)+":"+strAlternateId;
				%>
				  <%=dispdata%>
			<%} else if (Str_splfld.equals(disptype)) {%>
				<%//Special Field %>
				  <%=dispdata%>
			<%}else if (Str_date.equals(disptype)) {%>
				<%//Date input%>
				<input id="alternateId<%=intStudyIdType%>" name="alternateId<%=intStudyIdType%>" type="text" class="datefield" size="10" readOnly  value="<%=strAlternateId.trim()%>">
			<%}else if (Str_RO_input.equals(disptype)){%>
				<%//Read-only input %>
				<input type="text" class='readonly-input' readonly id="alternateId<%=intStudyIdType%>" name="alternateId<%=intStudyIdType%>" value="<%=strAlternateId.trim()%>" size="25" maxlength="100"/>
			<%} else if(Str_tarea.equals(disptype)){%>
				<textarea class="mdTextArea" name = "alternateId<%=intStudyIdType%>"  rows="4" cols="50" ><%=strAlternateId.trim()%></textarea>
			<%} else if(Str_tarea_RO.equals(disptype)){%>
			<textarea class="mdTextArea" readonly name = "alternateId<%=intStudyIdType%>"  rows="4" cols="50" ><%=strAlternateId.trim()%></textarea>
			<%} else{%>
				<%//Basic input %>
				<input type = "text" id="alternateId<%=intStudyIdType%>"  name="alternateId<%=intStudyIdType%>" value = "<%=strAlternateId.trim()%>" size = "25" maxlength = "100" >
			<%}%>
				<input type = "hidden" name = "recordType<%=intStudyIdType%>" value = "<%=strRecordType %>" >
				<input type = "hidden" name = "id<%=intStudyIdType%>" value = "<%=intId%>" >
				<input type = "hidden" name = "studyIdType" value = "<%=intStudyIdType%>" >
				<input type = "hidden" name = "studyId" value = "<%=studyId%>" >
			 </td>
	</tr>

	<%
		}
	%>
	<input type="hidden" name="cbcount" value=<%=cbcount%>>
	<input type="hidden" name="ddlist" value="<%=ddStr%>">
	</table>
	<br>
<%}else{%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/>
<%}%>