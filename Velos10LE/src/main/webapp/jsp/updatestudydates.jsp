<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<%@ page import="com.velos.eres.service.util.LC"%>
<%@ page language = "java" import = "com.velos.esch.service.util.EJBUtil,com.velos.esch.business.common.*,java.util.*,com.velos.eres.service.util.MC"%>
<jsp:useBean id="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>


<%
 HttpSession tSession = request.getSession(true);
 if (sessionmaint.isValidSession(tSession))
 {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String eSign = request.getParameter("eSign");
	String oldESign = (String) tSession.getValue("eSign");

	int retVal = 0;
	String message  = "";

	if(!(oldESign.equals(eSign)))  {
	%>
	  <jsp:include page="incorrectesign.jsp" flush="true"/>
	<%
	} else {
		String ipAdd = (String) tSession.getValue("ipAdd");
		String usr = (String) tSession.getValue("userId");
		String studyId = request.getParameter("studyId");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");

		studyB.setId(EJBUtil.stringToNum(studyId));
		studyB.getStudyDetails();
		studyB.setStudyActBeginDate(startDate);
		studyB.setStudyEndDate(endDate);
		studyB.setModifiedBy(usr);
		studyB.setIpAdd(ipAdd);
		retVal = studyB.updateStudy();

		if (retVal == 0)
		{
			message  = MC.M_Data_SvdSucc/*"Data was saved successfully"*****/;
		} else if (retVal == -4)
		{
			message  = MC.M_StdStat_LnkToStdStartDt/*"There is a "+LC.Std_Study_Lower+" status linked to the '"+LC.Std_Study+" Start Date'. You can not remove '"+LC.Std_Study+" Start Date'."*****/;
		}
		else if (retVal == -5)
		{
			message  = MC.M_StdStat_LnkToStdEndDt/*"There is a "+LC.Std_Study_Lower+" status linked to the '"+LC.Std_Study+" End Date'. You can not remove '"+LC.Std_Study+" End Date'."*****/;
		} else if (retVal == -2)
		{
			message  = MC.M_DataNotSvd_Succ/*"Data was not saved successfully"*****/;
		}



%>
		<br><br><br>
		<p class="successfulmsg" align=center> <%=message%> </p>

			<%
				if (retVal == -4 || retVal == -5)
				{
					String agent1 = request.getHeader("USER-AGENT");
				    agent1=agent1.toLowerCase();

				    if ((agent1.indexOf("mozilla") != -1) && (agent1.indexOf("gecko") <0))
				    {
				  	   %>
				  	   	   <p align="center">
					  		  <button onclick="history.go(-1);"><%=LC.L_Back%></button>
				  		   </p>

					 <%} else{
					 %>
						<p align="center">
						   <button tabindex=2 onclick="history.go(-1);"><%=LC.L_Back%></button>
					 </p>
					<%}
				}
				else
				{
			%>


		<script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
		</script>
	<%
		} //for else of retVal

	}
} //end of if for session
else{%>
<jsp:include page="timeout.html" flush="true"/>
<%}%>
</BODY>
</HTML>
