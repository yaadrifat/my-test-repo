<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
 <html>
<head>
<title><%=LC.L_Visit_Mstone%><%--Visit Milestone*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">




function validate(formobj) {

     totalVisitsCount =formobj.visitCount.value;
	 var amount;
	 var selectedRule;
	 var ruleSubType;
	 var exist = 0;


	 if (totalVisitsCount == 1) {
			//KM-#4585
			 if (!(validate_col('Visit',formobj.dVisit))) return false;
			 if (!(validate_col('Rule',formobj.rule))) return false;

              selectedRule  =  formobj.rule.selectedIndex;
		      ruleSubType = formobj.ruleSubType[selectedRule].value;


            if ((!validate_col_no_alert('Event Status',formobj.dEventStatus )) )
	        {

	    		if (ruleSubType != 'vm_4')
	    		{
					alert("<%=MC.M_Selc_EvtStatus%>");/*alert("Please select Event Status");*****/
					formobj.dEventStatus.focus();
	    	  	   return false;
	    	  	 }
	         }
	         else
	         {
	        	if (ruleSubType == 'vm_4')
	    		{
					alert("<%=MC.M_CntSelEvt_MstoneRule%>");/*alert("You cannot select an Event Status with the selected Milestone Rule");*****/
					formobj.dEventStatus.focus();
	    	  	   return false;
	    	  	 }

	         }

           formobj.amount.value   = formobj.amountNum.value + "." + formobj.amountFrac.value;
            amount = formobj.amountNum.value + "." + formobj.amountFrac.value;
              if (amount != ".")
              {
	            if ( ! validate_col('Rule',formobj.rule) ){
				alert("<%=MC.M_SelVisits_ForAmt%>");/*alert("Please select Visits for all Amounts.");*****/
				return false;
			}
         }


		if(isNaN(formobj.amountNum.value) == true && isNegNum(formobj.amountNum.value) == false) {
				alert("<%=MC.M_EtrNum_AmtVal%>");/*alert("Please enter a numeric value for Amount.");*****/
				formobj.amountNum.focus();
				return false;
			}


		 // var index2 = formobj.amountNum.value.indexOf(".");
		  	 // if (index2 != -1) {
		    		//alert("<%=MC.M_DecimalNotAlw_Fld%>");/*alert("Decimal value not allowed in this field. Please enter the decimal part in the appropriate field");*****/
					//formobj.amountNum.focus();

					//return false;
		    	//}

	   //if(isNaN(formobj.amountFrac.value) == true) {
			//	alert("<%=MC.M_EtrNum_AmtVal%>");/*alert("Please enter a numeric value for Amount.");*****/
			//	formobj.amountFrac.focus();
			//	return false;
			//}
		if(formobj.amount.value != "."){
	 		if (!(isDecimal(formobj.amount.value))){
	 			alert("<%=LC.L_Invalid_Amount%>");/*alert("Invalid Amount");*****/
				formobj.amountNum.focus();
		 	 	return false;
		 	}
		}

		if (! isInteger(formobj.mLimit.value))
		{
				formobj.mLimit[counter].focus();
				alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
			 	return false;
		}

		if ( !isInteger(formobj.count.value) )
		{
			if (!isNegNum(formobj.count.value))
			{
				formobj.count.focus();
				alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
			 	return false;
			}
			else
			{
				if (formobj.count.value == '-0')
				{
					formobj.count.focus();
					alert("<%=MC.M_InvalidNegVal_EtrVldCnt%>");/*alert("Invalid negative value. Please enter a valid 'Count'");*****/
				 	return false;
				}

				if (parseInt(formobj.count.value) < -1)
				{
					formobj.count.focus();
					alert("<%=MC.M_CntLess_EtrValidCount%>");/*alert("The count cannot be less than -1. Please enter a valid 'Count'");*****/
				 	return false;
				}
			}
		}
		//KM-For Milestone Status validation
		if (!(validate_col('Milestone Status',formobj.milestoneStat))) return false;

		if ((formobj.dateFrom.value != '') && (formobj.dateTo.value != ''))
        {
			if(CompareDates(formobj.dateFrom.value,formobj.dateTo.value)){
				alert("<%=MC.M_DtToNotLessThan_DtFrom%>")
				formobj.dateTo.focus();
				return false;
			}
        }

     } else {

		 //KM-#4585
		 for (count = 0 ; count < totalVisitsCount ; count++)
         {
			if (validate_col_no_alert('Visit',formobj.dVisit[count])) {
				exist = 1;
			}

			if (validate_col_no_alert('Rule',formobj.rule[count])) {
				exist = 1;
			}
			if (validate_col_no_alert('Event Status',formobj.dEventStatus[count])) {
				exist = 1;
			}
			if (validate_col_no_alert('Milestone Status',formobj.milestoneStat[count])) {
				exist = 1;
			}

		 }

		 if(exist == 0) {
			 alert("<%=MC.M_Etr_MandantoryFlds%>");/*alert("Please enter data in all mandatory fields");*****/
			 formobj.dVisit[0].focus();
		     return false;
		 }



		 for (counter = 0 ; counter < totalVisitsCount ; counter ++)
         {

	        if ((validate_col_no_alert('Rule',formobj.rule[counter])) && (! validate_col_no_alert('Visit',formobj.dVisit[counter])))
	        {
				alert("<%=MC.M_SelVisit_ForRule%>");/*alert("Please select Visit for all Rules");*****/
				formobj.dVisit[counter].focus();
	      	   return false;
	         }



	        if ((!validate_col_no_alert('Rule',formobj.rule[counter])) && (validate_col_no_alert('Visit',formobj.dVisit[counter])))
	        {
				alert("<%=MC.M_PlsSelRule_AllVsts%>");/*alert("Please select Rules for all Visits");*****/
				formobj.rule[counter].focus();
	      	   return false;
	         }



	        if ((!validate_col_no_alert('Event Status',formobj.dEventStatus[counter] )) && ( validate_col_no_alert('Visit',formobj.dVisit[counter]) && validate_col_no_alert('Rule',formobj.rule[counter])) )
	        {
	    		 selectedRule  =  formobj.rule[counter].selectedIndex;
		         ruleSubType = formobj.ruleSubType[selectedRule].value;

	    		if (ruleSubType != 'vm_4')
	    		{
					alert("<%=MC.M_Selc_EvtStatus%>");/*alert("Please select Event Status");*****/
					formobj.dEventStatus[counter].focus();
	    	  	   return false;
	    	  	 }
	         }


			if ((validate_col_no_alert('Event Status',formobj.dEventStatus[counter])) && (! validate_col_no_alert('Visit',formobj.dVisit[counter]))  )
	        {
				alert("<%=MC.M_PlsSelectVisit%>");/*alert("Please select Visit");*****/
				formobj.dVisit[counter].focus();
	      	    return false;
	         }


			if ((validate_col_no_alert('Event Status',formobj.dEventStatus[counter])) && (!validate_col_no_alert('Rule',formobj.rule[counter]) ) )
	        {
				alert("<%=MC.M_PlsSelectRule%>");/*alert("Please select Rule");*****/
				formobj.rule[counter].focus();
	      	    return false;
	         }


	        if ((validate_col_no_alert('Event Status',formobj.dEventStatus[counter])) )
	        {
	    		 selectedRule  =  formobj.rule[counter].selectedIndex;
		         ruleSubType = formobj.ruleSubType[selectedRule].value;

	    		if (ruleSubType == 'vm_4')
	    		{
					alert("<%=MC.M_CntSelEvt_MstoneRule%>");/*alert("You cannot select an Event Status with the selected Milestone Rule");*****/
					formobj.dEventStatus[counter].focus();
	    	  	   return false;
	    	  	 }

	         }

			//KM-04Jun10-D-FIN7 issues.
			if ((!validate_col_no_alert('Milestone Status',formobj.milestoneStat[counter] )) &&
					(validate_col_no_alert('Visit',formobj.dVisit[counter]) && validate_col_no_alert('Rule',formobj.rule[counter]) &&   validate_col_no_alert('Event Status',formobj.dEventStatus[counter] )))
	        {
				alert("<%=MC.M_Selc_MstoneStatus%>");/*alert("Please select Milestone Status");*****/
				formobj.milestoneStat[counter].focus();
	      	    return false;
	        }

			//KM-15Jun10-#5037
			if ((!validate_col_no_alert('Milestone Status',formobj.milestoneStat[counter] )) &&
					( validate_col_no_alert('Rule',formobj.rule[counter])))
	        {
				alert("<%=MC.M_Selc_MstoneStatus%>");/*alert("Please select Milestone Status");*****/
				formobj.milestoneStat[counter].focus();
	      	    return false;
	        }



			if ((validate_col_no_alert('Milestone Status',formobj.milestoneStat[counter])) &&														(!validate_col_no_alert('Visit',formobj.dVisit[counter])))
	        {
				alert("<%=MC.M_PlsSelectVisit%>");/*alert("Please select Visit");*****/
				formobj.dVisit[counter].focus();
	      	    return false;
	        }


	       	if (! isInteger(formobj.mLimit[counter].value))
			{
					formobj.mLimit[counter].focus();
					alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
				 	return false;
			}
					if ( !isInteger(formobj.count[counter].value) )
		{
			if (!isNegNum(formobj.count[counter].value))
			{
				formobj.count[counter].focus();
				alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
			 	return false;
			}
			else
			{
				if (formobj.count[counter].value == '-0')
				{
					formobj.count[counter].focus();
					alert("<%=MC.M_InvalidNegVal_EtrVldCnt%>");/*alert("Invalid negative value. Please enter a valid 'Count'");*****/
				 	return false;
				}

				if (parseInt(formobj.count[counter].value) < -1)
				{
					formobj.count[counter].focus();
					alert("<%=MC.M_CntLess_EtrValidCount%>");/*alert("The count cannot be less than -1. Please enter a valid 'Count'");*****/
				 	return false;
				}
			}
		}

	         // var index3 = formobj.amountNum[counter].value.indexOf(".");
		  	 // if (index3 != -1) {
		    //		alert("<%=MC.M_DecimalNotAlw_Fld%>");/*alert("Decimal value not allowed in this field. Please enter the decimal part in the appropriate field");*****/
			//		formobj.amountNum[counter].focus();
	 		//		return false;
		    //	}

   			// formobj.amount[counter].value = formobj.amountNum[counter].value + "." + formobj.amountFrac[counter].value;
   			
				var amountMilestone = formobj.amountNum[counter].value;
				var amountLength = amountMilestone.split('.');
				if(amountLength[0].length>12){
					alert("<%=MC.M_AmtBeforeDecimalValueLimitExceeds%>");
					formobj.amountNum[counter].focus();
					return false;
				}
   			
   			 formobj.amount[counter].value = formobj.amountNum[counter].value + "." + formobj.amountFrac[counter].value;

   			if (formobj.amount[counter].value != ".")
			 {

		        if ((!validate_col_no_alert('Rule',formobj.rule[counter])) && (validate_col_no_alert('Amount',formobj.amount[counter])))
		        {
					alert("<%=MC.M_SelRules_ForAllAmts%>");/*alert("Please select Rules for all Amounts");*****/
					formobj.rule[counter].focus();
		      	   return false;
		         }

				  if ((!validate_col_no_alert('Visit',formobj.dVisit[counter])) && (validate_col_no_alert('Amount',formobj.amount[counter])))
		        {
					alert("<%=MC.M_SelVisits_ForAmt%>");/*alert("Please select Visits for all Amounts");*****/
					formobj.dVisit[counter].focus();
		      	   return false;
		         }


		         if(isNaN(formobj.amountNum[counter].value) == true && isNegNum(formobj.amountNum[counter].value) == false) {
					alert("<%=MC.M_EtrNum_AmtVal%>");/*alert("Please enter a numeric value for Amount.");*****/
					formobj.amountNum[counter].focus();
					return false;
				}

		 		if (/^\d{0,11}?$/.test(formobj.amountNum[counter].value) == false && !isEmpty(formobj.amountNum[counter].value)) {
					alert("<%=MC.M_ValEtrFwg_AmtFmt11Dgt%>");/*alert("The value entered does not pass the following criteria:\n \n -'Amount' should be a number.\n -'Amount' should conform to format - ###########.## \n    i.e. max 11 digits followed by max 2 decimal places.\n \nPlease enter a valid value.");*****/
					formobj.amountNum[counter].focus();
					return false;
		   		}
		   		
				//if(isNaN(formobj.amountFrac[counter].value) == true) {
				//	alert("<%=MC.M_EtrNum_AmtVal%>");/*alert("Please enter a numeric value for Amount.");*****/
				//	formobj.amountFrac[counter].focus();
				//	return false;
				//}

				if (/^\d{0,2}?$/.test(formobj.amountFrac[counter].value) == false && !isEmpty(formobj.amountFrac[counter].value)) {
					alert("<%=MC.M_ValEtrFwg_AmtFmt11Dgt%>");/*alert("The value entered does not pass the following criteria:\n \n -'Amount' should be a number.\n -'Amount' should conform to format - ###########.## \n    i.e. max 11 digits followed by max 2 decimal places.\n \nPlease enter a valid value.");*****/
					formobj.amountFrac[counter].focus();
					return false;
				}

				/*if (!(isDecimal(formobj.amount[counter].value))){
		 			alert("Invalid Amount");
					formobj.amountNum[counter].focus();
			 	 	return false;
				 	} 			*/

			}
			if ((validate_col_no_alert('<%=LC.L_Date_From%>',formobj.dateFrom[counter])) && ( validate_col_no_alert('<%=LC.L_Date_To%>',formobj.dateTo[counter])))
	        {
				if(CompareDates(formobj.dateFrom[counter].value,formobj.dateTo[counter].value)){
					alert("<%=MC.M_DtToNotLessThan_DtFrom%>")
					formobj.dateTo[counter].focus();
					return false;
				}
	        }
   			
   		 //Added by Ashu for BUG#5403
   		 if(   (!validate_col_no_alert('Visit',formobj.dVisit[counter])) 
   	    	   && (!validate_col_no_alert('Rule',formobj.rule[counter]))
   	    	   && (!validate_col_no_alert('Event Status',formobj.dEventStatus[counter]))
   	    	   && (!validate_col_no_alert('Milestone Status',formobj.milestoneStat[counter]))
   	    	 ){
   	    	 if( (validate_col_no_alert('Patient Count',formobj.count[counter])) 
   	   	    	   || (validate_col_no_alert('Patient Status',formobj.dStatus[counter]))
   	   	    	   || (validate_col_no_alert('Amount',formobj.amountNum[counter]))
   	   	    	   ||(validate_col_no_alert('Payment For',formobj.dpayFor[counter]))
   	   	    	   ||(validate_col_no_alert('Limit',formobj.mLimit[counter]))
   	    	    ){
   	    		alert("<%=MC.M_Etr_MandantoryFlds%>");/*alert("Please enter data in all mandatory fields");*****/
				 formobj.dVisit[counter].focus();
			     return false;
   	    	 }
   	     	}
         }
     }


//	if (!(validate_col('e-Signature',formobj.eSign))) return false
//	if(isNaN(formobj.eSign.value) == true) {
//		alert("Incorrect e-Signature. Please enter again");
//		formobj.eSign.focus();
//		return false;
//	}
}


function fclose() {
	self.close();
}

function validate_protocol(formobj) {
	 if(formobj.protocolId.value == '') {
		alert("<%=MC.M_Selc_PcolCal%>");/*alert("Please select a Protocol Calendar");*****/
		formobj.protocolId.focus();
		return false;
	}

	var idx ;
	var prottype;

	idx = formobj.protocolId.selectedIndex;
	prottype = formobj.protocolType[idx].value;

	formobj.selProtocolType.value = prottype;
}

/*function setVisit(formobj,sequence) {
	selrow = sequence ;
	visitCount = formobj.visitCount.value;
	selectedVisits = formobj.selVisits.value;
	totalVisits = parseInt(visitCount);
	selectedVisitsCount = parseInt(selectedVisits);
	if (totalVisits  > 1) {
		if (formobj.sequence[selrow].checked) {
			selectedVisitsCount ++;
 		 	formobj.selVisits.value = selectedVisitsCount;
		} else {
			selectedVisitsCount --;
			formobj.selVisits.value = selectedVisitsCount;
		}

	} else {
		if (formobj.sequence.checked) {
			selectedVisitsCount ++;
 		 	formobj.selVisits.value = selectedVisitsCount;
		} else {
			selectedVisitsCount --;
			formobj.selVisits.value = selectedVisitsCount;
		}
	}
}
*/
</SCRIPT>

<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1)
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {
%>
<body style="overflow:scroll;">
<%
	} else {
%>
<body>
<%
	}
%>
<P class="sectionHeadings"><%=LC.L_Mstone_VstMstone%><%--Milestones >> Visit Milestones*****--%> </P>


<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<jsp:useBean id="protvisitB" scope="request" class="com.velos.esch.web.protvisit.ProtVisitJB"/>


<jsp:useBean id="milestoneB" scope="request" class="com.velos.eres.web.milestone.MilestoneJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />


<%@ page language = "java" import = "com.velos.eres.business.common.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB, com.velos.esch.business.common.EventAssocDao,com.velos.esch.business.common.*"%>
<BR>
<%
   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession)) {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>
<%
   		EventAssocDao eventAssocDao = new EventAssocDao();
   		EventAssocDao eventAssocDaoAdmin  = new EventAssocDao();

		int accId = EJBUtil.stringToNum((String) tSession.getValue("accountId"));
		String uName = (String) tSession.getValue("userName");
		String userIdFromSession = (String) tSession.getValue("userId");

		String studyId = request.getParameter("studyId");
		String selProtocol , pageMode;

		String studyCurrency = "";
		ArrayList curr = new ArrayList();
		String currency = "";

		studyB.setId(EJBUtil.stringToNum(studyId));
   		studyB.getStudyDetails();
   		studyCurrency = studyB.getStudyCurrency();
		SchCodeDao cdDesc = new SchCodeDao();
		cdDesc.getCodeValuesById(EJBUtil.stringToNum(studyCurrency));
		curr =  cdDesc.getCSubType();

		if (curr.size() > 0) {
			currency=(((curr.get(0)) ==null)?"-":(curr.get(0)).toString());
		}

		selProtocol=request.getParameter("protocolId");
		int selProtInt = 0;
		ProtVisitDao visitDao = new ProtVisitDao();

		if (selProtocol==null) {
			pageMode="NF"; //for no filter mode
		} else {
			selProtInt = EJBUtil.stringToNum(selProtocol);
		    pageMode = "F"; //filter mode

			visitDao = protvisitB.getProtocolVisits(selProtInt);
		}

		int len= 0;
		int lenAdmin = 0;
		int eventLen = 0;

		ArrayList protIds=new ArrayList() ;
		ArrayList protNames= new ArrayList();

		String protName="";
		Integer protId;

		int noOfCheckboxes=0;
		StringBuffer protDD = new StringBuffer();
		StringBuffer sbProtTypes = new StringBuffer();
		//eventAssocDao= eventassocB.getStudyProts(EJBUtil.stringToNum(studyId));
		//JM: #2454 Active Protocols only...
		//eventAssocDao = eventassocB.getStudyProtsActive(EJBUtil.stringToNum(studyId));
		
		//enh V-FIN27
		eventAssocDao = eventassocB.getStudyProts(EJBUtil.stringToNum(studyId),"ND");

		protIds=eventAssocDao.getEvent_ids() ;
		protNames= eventAssocDao.getNames();
		//SV, commented, 10/28/04, show visit names instead of visit #s, cal-enh

   	  	len= protIds.size();

       	int counter = 0;

		CodeDao codeLst = new CodeDao();
		String 	dRule = "";

		//get Admin Calendars
		//eventAssocDaoAdmin = eventassocB.getStudyAdminProts(EJBUtil.stringToNum(studyId),"A");
		
		//enh V-FIN27
		eventAssocDaoAdmin = eventassocB.getStudyAdminProts(EJBUtil.stringToNum(studyId),"ND");
		
        ArrayList eventIdsAdmin = eventAssocDaoAdmin .getEvent_ids();
        ArrayList namesAdmin = eventAssocDaoAdmin.getNames();

		//KM-#4770
		lenAdmin = eventIdsAdmin.size();

		protDD.append("<SELECT name=protocolId>");
		protDD.append("<option value=''>"+LC.L_Select_AnOption/*Select an Option*****/+" </Option>");
		protDD.append("<option value=''>***"+LC.L_Patient_Calendars/*Patient Calendars*****/+"***</Option>");

		// add prot type 4 above 2 options
		sbProtTypes.append("<input type=hidden name=\"protocolType\" value=\"P\">");
		sbProtTypes.append("<input type=hidden name=\"protocolType\" value=\"P\">");


	    for(counter = 0;counter<len;counter++) {

	    	sbProtTypes.append("<input type=hidden name=\"protocolType\" value=\"P\">");

			protId = (Integer)protIds.get(counter);
			protName=((protNames.get(counter)) == null)?"-":(protNames.get(counter)).toString();
			if (selProtInt == protId.intValue()) {
				protDD.append("<option value="+ protId.toString() +" SELECTED>" + protName +"</option>");
			} else {
				protDD.append("<option value="+ protId.toString() +">" + protName +"</option>");
			}
		}

		protDD.append("<option value=''>***"+LC.L_Admin_Cal/*Administrative Calendars*****/+"***</Option>");
		sbProtTypes.append("<input type=hidden name=\"protocolType\" value=\"S\">");

		for(counter = 0;counter<eventIdsAdmin.size();counter++) {

			sbProtTypes.append("<input type=hidden name=\"protocolType\" value=\"S\">");
			protId = (Integer)eventIdsAdmin.get(counter);
			protName=((namesAdmin.get(counter)) == null)?"-":(namesAdmin.get(counter)).toString();

			if (selProtInt == protId.intValue()) {
				protDD.append("<option value="+ protId.toString() +" SELECTED>" + protName +"</option>");
			} else {
				protDD.append("<option value="+ protId.toString() +">" + protName +"</option>");
			}
		}



		protDD.append("</SELECT>");
%>

<DIV>

<%   if(len==0 && lenAdmin == 0)
			{
%>
        	<P class = "defComments"><%=MC.M_NoCal_InSelStd%><%--There is no calendar in the  selected <%=LC.Std_Study_Lower%>*****--%></P>
			  <table width="100%" cellspacing="0" cellpadding="0">
			  <tr> <td align=center> <button onClick = "self.close()"><%=LC.L_Close%></button>
		      </td>
		      </tr>
			  </table>
<% 	} else {
%>
  <Form name="mileprotocol" method="post" action="visitmilestone.jsp" onsubmit="return validate_protocol(document.mileprotocol)">
   <input type="hidden" name="studyId" value="<%=studyId%>">
    <table width="60%" cellspacing="0" cellpadding="0">
	<tr>
	<td width=30%><P class="defComments"><b><%=LC.L_Select_ACal%><%--Select a Calendar*****--%></b></P></td>
	<td width=25%><%=protDD.toString()%></td>
     <td width=5% align=left>
   	  <button type="submit"><%=LC.L_Search%></button>
     </td>
	</tr>
	</table><%=sbProtTypes%>
	 <input type="hidden" name="selProtocolType" value="">

	</form>




   <form name="milerule" method="post" id="visitmilerule" action="visitrulesave.jsp" onsubmit="ripLocaleFromAll(); if (validate(document.milerule)== false) { setValidateFlag('false'); applyLocaleToAll(); return false; } else { setValidateFlag('true'); return true;}">

   <input type="hidden" name="studyId" value="<%=studyId%>">
   <input type="hidden" name="protocolId" value="<%=selProtocol%>">



	<table>

	<%
	int i = 0;
	  if (pageMode.equals("F"))
	  {

	  	String selProtocolType = "";
	  	selProtocolType = request.getParameter("selProtocolType");

	  	if (StringUtil.isEmpty(selProtocolType))
	  	{
	  		selProtocolType = "P";
	  	}

		ArrayList protocolVisitNames= new ArrayList();
		ArrayList fk_visits= new ArrayList();


		String protocolVisitName = ""; //SV, 10/12/04
		String fk_visit = ""; //SV, 11/01/04
		String oldVisit="";
		String amount = "";
		String dVisit = "";
			String dStatus = "";
		String dpayType = "";
		String dpayFor = "";
		CodeDao codeLstPayType = new CodeDao();
		CodeDao codeLstPayFor = new CodeDao();

		ArrayList ruleSubType = new ArrayList();



		protocolVisitNames= visitDao.getNames();
		fk_visits=visitDao.getVisit_ids();

		fk_visits.add(new Integer("-1"));
		protocolVisitNames.add("All");

		dVisit = EJBUtil.createPullDown("dVisit",0 ,fk_visits,protocolVisitNames);

 		eventLen = fk_visits.size();


		CodeDao codeLstStatus = new CodeDao();
		SchCodeDao cdEvent = new SchCodeDao();

		String dEventStatus = "";

		codeLstStatus.getCodeValues("patStatus",accId);
		dStatus = codeLstStatus.toPullDown("dStatus");

 		cdEvent.getCodeValues("eventstatus",0);
 		dEventStatus =   cdEvent.toPullDown("dEventStatus");

 		//JM: 30Dec2009: #3357
		codeLstPayType.getCodeValues("milepaytype");

		//retrieve 'Receivable' by default
		int default_sel = codeLstPayType.getCodeId("milepaytype", "rec");

 		dpayType = EJBUtil.createPullDownWithStrNoSelectIfHidden("dpayType",""+default_sel,codeLstPayType.getCId(),codeLstPayType.getCDesc(), codeLstPayType.getCodeHide());

		codeLstPayFor.getCodeValues("milePayfor");
		 dpayFor = codeLstPayFor.toPullDown("dpayFor");


		if (eventLen >0 )
		{
	  %>
	  <P class="defComments"><%=MC.M_VisitsAval_ForSel%><%--Following Visit(s) are available for selection*****--%></P>
	  <%

		codeLst.getCodeValues("VM");
		dRule = codeLst.toPullDown("rule");

		ruleSubType = codeLst.getCSubType();

		if (ruleSubType != null )
		{
			for ( int k=0; k < ruleSubType.size(); k++)
			{
			 	%>
		 			<input type=hidden name=ruleSubType value='<%=ruleSubType.get(k) %>'>
			 	<%

			}
		}
		%>
		<table width="90%">
		<tr>
		<th class="headernoColor" width = '35%'><%=LC.L_Select_Visits%><%--Select Visits*****--%><FONT class="Mandatory">* </FONT> </td>
		<th class="headernoColor" width = '20%'><%=LC.L_Milestone_Rule%><%--Milestone Rule*****--%><FONT class="Mandatory">* </FONT></td>
		<th class="headernoColor" width = '30%'><%=LC.L_Event_Status%><%--Event Status*****--%><FONT class="Mandatory">* </FONT></td>

		<%if (selProtocolType.equals("P")) {
		  %>
		<th class="headernoColor"  width=10% ><%=LC.L_Pat_Count%><%--<%=LC.Pat_Patient%> Count*****--%></th>
		<th class="headernoColor"  width=15% ><%=LC.L_Patient_Status%><%--<%=LC.Pat_Patient%> Status*****--%></th>
		<% } %>
		<th class="headernoColor" width = '15%'><%Object[] arguments1 = {currency}; %><%=VelosResourceBundle.getLabelString("L_Amount_In",arguments1)%><%--Amount (in <%=currency%>)*****--%></td>
		<th class="headernoColor" width=5% ><%=LC.L_Limit%><%--Limit*****--%></th>
		<th class="headernoColor" width=5% ><%=LC.L_Payment_Type%><%--Payment Type*****--%></th>
		<th class="headernoColor" width=15% ><%=LC.L_Payment_For%><%--Payment For*****--%></th>
		<th class="headernoColor" width=15% ><%=LC.L_Mstone_Status%><%--Milestone Status*****--%> <FONT class="Mandatory">* </FONT> </th>
		<th class="headernoColor" width=15% ><%=LC.L_Date_From%><%--Date From*****--%> </th>
		<th class="headernoColor" width=15% ><%=LC.L_Date_To%><%--Date To*****--%> </th>
		</tr>

		<tr>
		<td><hr class="thinLine"></td>
		<td><hr class="thinLine"></td>
		<td><hr class="thinLine"></td>

		<%if (selProtocolType.equals("P")) {
		  %>
		<td><hr class="thinLine"></td>
		<td><hr class="thinLine"></td>
			<% } %>

		<td><hr class="thinLine"></td>
		<td><hr class="thinLine"></td>
		<td><hr class="thinLine"></td>
		<td><hr class="thinLine"></td>
		<td><hr class="thinLine"></td>
		<td><hr class="thinLine"></td>
		<td><hr class="thinLine"></td>
		<td><hr class="thinLine"></td>
		<td><hr class="thinLine"></td>
		</tr>

		<%
		} else {
	  %>
	  <P class="defComments"><%=MC.M_NoVisitAval_ForSelc%><%--No Visit available for selection*****--%></P>
	  <%
		}

		//show 10 rows at the max
		if (eventLen > 10)
		{
			eventLen = 10;
		}

		//KM-D-FIN7
		CodeDao codeMsStat = new CodeDao();
		String roleCodePk="";

	    if (EJBUtil.stringToNum(studyId) > 0)
		{
			ArrayList tId = new ArrayList();

			TeamDao teamRoleDao = new TeamDao();
			teamRoleDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));
			tId = teamRoleDao.getTeamIds();

			if (tId != null && tId.size() > 0)
			{
				ArrayList arRoles = new ArrayList();
				arRoles = teamRoleDao.getTeamRoleIds();

				if (arRoles != null && arRoles.size() >0 )
				{
					roleCodePk = (String) arRoles.get(0);

					if (StringUtil.isEmpty(roleCodePk))
					{
						roleCodePk="";
					}
				}
				else
				{
					roleCodePk ="";
				}

			}
			else
			{
				roleCodePk ="";
			}
		} else
			{
			  roleCodePk ="";
			}

		codeMsStat.getCodeValuesForStudyRole("milestone_stat",roleCodePk);



		for(counter = 0;counter<eventLen;counter++)
			{

			protocolVisitName =((protocolVisitNames.get(counter)) == null)?"-":(protocolVisitNames.get(counter)).toString();
			if (fk_visits.size() > 0) {
				fk_visit =((fk_visits.get(counter)) == null)?"-":(fk_visits.get(counter)).toString();
			}

			%>
			<tr>
				<td align=left  class="tdDefault">
					<%=dVisit%>

				<td>
					<%=dRule%>
				</td>
				<td><%=dEventStatus%></td>

				<%

				if (selProtocolType.equals("P")) {
		  			%>
				<td align=center><input type=text name=count size=5 maxlength=10 class="leftAlign numberfield" data-unitsymbol="" data-formatas="number" /></td>
				<td ><%=dStatus%></td>
				 <% } else { %>
				 		<input type=hidden name=count size=5 maxlength=10 class="leftAlign" >
				 		<input type=hidden name=dStatus size=5 maxlength=10 class="leftAlign" >

				 <% }  %>

				<td align=center>
	<INPUT NAME="amount" TYPE="hidden" value="<%=amount%>" SIZE=20>
	<input type=text name=amountNum size=16 maxlength=16 class="leftAlign numberfield" data-unitsymbol="" data-formatas="currency" >
  		<!--  <B>.</B>
		<input type=text name=amountFrac size=3 maxlength=2  class="leftAlign" > -->
	 </td>
			<td><input type=text name="mLimit" size=3 maxlength=10  class="leftAlign numberfield" data-unitsymbol="" data-formatas="number" /></td>
		<td >
	<%=dpayType%>
	</td>
	<td >
	<%=dpayFor%>
	</td>

    <%
    //KM-01Jun10-#4962
	String msStat = codeMsStat.toPullDown("milestoneStat");
	%>
	<td align = center> <%=msStat%> </td>
	<td align = center><input type="text" name="dateFrom" class="datefield" size ="12" MAXLENGTH ="8" /></td>
	<td align = center><input type="text" name="dateTo" class="datefield" size ="12" MAXLENGTH ="8" /></td>
	</tr>
			<%
				i++;

			}
	  }
	%>
		<INPUT name=visitCount type=hidden value=<%=i%> >


	<%
	  if (pageMode.equals("F") && (eventLen >0))
	  {
	%>
   </table>
	<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="N"/>
		<jsp:param name="formID" value="visitmilerule"/>
		<jsp:param name="showDiscard" value="N"/>
  </jsp:include>
	<%}%>


  </Form>
  <%
  } // end of length check

	} //end of if session times out

else

{

%>
  <jsp:include page="timeout_childwindow.jsp" flush="true"/>
  <%

} //end of else body for page right

%>
</div>
<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>

</html>












