<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML><HEAD><TITLE><%=LC.L_Multi_EvtDets%><%--*****Multiple Event Details--%></TITLE>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<script>

/*function changeView(formobj,cmbView)
{


if(cmbView == "visit")
	view = "event";
if(cmbView == "event")
	view = "visit";
formobj.action= "editMulEventDetails.jsp"+"?view="+ view;
formobj.submit();


}*/

function selectDeSelectAll(formobj)
{

    cmbView = formobj.cmbView.value;

	if(formobj.selDeSelAll.checked)
	{
			if(cmbView == "visit")
			{

			   countVisit =formobj.countVisit.value;

			   if(countVisit==1)
				{
				    formobj.chkVst.checked=true;
				}
				else{
					   for(cntVst=0;cntVst<countVisit;cntVst++)
						{
						  formobj.chkVst[cntVst].checked=true;
						}
				}

			}
			if(cmbView == "event")
			{
			  countEvent =formobj.countEvent.value;

				if(countEvent==1)
				{
				    formobj.chkEvent.checked=true;
				}
				else{
					  for(cntEvnt=0;cntEvnt<countEvent;cntEvnt++)
						{
						  formobj.chkEvent[cntEvnt].checked=true;
						}
				    }
			}
	}
	else
	{
		if(cmbView == "visit")
				{

				  countVisit =formobj.countVisit.value;
				   if(countVisit==1)
					{
						formobj.chkVst.checked=false;
					}
				else{
					  for(cntVst=0;cntVst<countVisit;cntVst++)
						{
						  formobj.chkVst[cntVst].checked=false;
						}
				  }

				}
				if(cmbView == "event")
				{
				  countEvent =formobj.countEvent.value;

				  if(countEvent==1)
					{
						formobj.chkEvent.checked=false;
					}
				else{
					  for(cntEvnt=0;cntEvnt<countEvent;cntEvnt++)
						{
						  formobj.chkEvent[cntEvnt].checked=false;
						}
					}
				}

	}

}

function updateAll(formobj)
{
cmbView = formobj.cmbView.value;
//status = formobj.status.value;
caldate = formobj.caldate.value;

var idx = formobj.status.options.selectedIndex ;
var status = formobj.status.options[idx].value;


var cntVst=0;
var cntEvt=0;

if(status == "" && caldate == "" ){
	alert("<%=MC.M_PlsStatOrStat_Frm%>");/*alert("Please Select Status/Status Valid From");*****/
	return false;

}

if(cmbView == "visit")
	{

	  countVisit =formobj.countVisit.value;

			if(countVisit > 1){
				 for(i=0;i<countVisit;i++)
					{
					if(formobj.chkVst[i].checked)
						{
							if(status != "")
							formobj.eventstatus[i].value = status;
							if(caldate != "" )
							formobj.exeon[i].value = caldate;
							cntVst++;

						 }

					}
			}
			else if(countVisit == 1)
			{
				if(formobj.chkVst.checked)
						{
						if(status != "")
						 formobj.eventstatus.value = status;
						if(caldate != "" )
						  formobj.exeon.value = caldate;
						  cntVst++;
						 }

			}
	if(cntVst==0)
		{
			alert("<%=MC.M_PlsSelectVisit%>");/*alert("Please Select Visit");*****/
		}
	}
	if(cmbView == "event")
		{

				countEvent =formobj.countEvent.value;

				if(countEvent > 1){
					 for(i=0;i<countEvent;i++)
						{
						if(formobj.chkEvent[i].checked)
							{
								if(status != "")
								formobj.eventstatus[i].value = status;
								if(caldate != "" )
								formobj.exeon[i].value = caldate;
								cntEvt++;
							 }

						}
				}
				else if(countEvent == 1)
				{
					if(formobj.chkEvent.checked)
							{
							    if(status != "")
								formobj.eventstatus.value = status;
								if(caldate != "" )
								formobj.exeon.value = caldate;
								cntEvt++;
							 }

				}

		if(cntEvt==0)
			{
				alert("<%=MC.M_PlsSelectEvt%>");/*alert("Please Select Event");*****/
			}

		}
}

function setFlag(formobj)
{
  formobj.buttonFlag.value = "S";
}

function setVals(formobj,studyId,len){


	if(formobj.buttonFlag.value != 'S') {
	if(len == 0)
	{
		alert("<%=MC.M_NoRec_ToSave%>");/*alert("No Records to be saved");*****/
		return false;
	}
	else{
			cmbView = formobj.cmbView.value;

			arrCheck = new Array();
			var i=0;
			var j=0;
			var k=0;
			var chk=0;
			if(cmbView == "visit" || cmbView == "")
			{
					countVisit =formobj.countVisit.value;

					if(countVisit > 1){
						 for(i=0;i<countVisit;i++)
							{
							if(formobj.chkVst[i].checked)
								{

									if(formobj.exeon[i].value=="")
									{
										alert("<%=MC.M_StatValidFor_Visits%>");/*alert("Please Enter Status Valid From for Checked Visits");*****/
										return false;
									}
									arrCheck[k] ="1";
									k++;
									chk++;

								 }
								 else{
										arrCheck[k] ="0";
										k++;
								}
							}
					}
					else if(countVisit == 1)
					{
						if(formobj.chkVst.checked)
								{
								 if(formobj.exeon.value=="")
									{
									 	alert("<%=MC.M_StatValidFor_Visits%>");/*alert("Please Enter Status Valid From for Checked Visits");*****/
										return false;
									}
								  arrCheck[k]="1";
								  chk++;

								 }
								 else{
									 arrCheck[k]="0";

								 }
							}



							if(chk==0)
							{
								alert("<%=MC.M_PlsSelectVisit%>");/*alert("Please Select Visit");*****/
							return false;
							}
							if(formobj.eSign.value== "")
								{
									alert("<%=MC.M_PlsEnterEsign%>");/*alert("Please enter e-Signature");*****/
									formobj.eSign.focus();
									return false;
								}


								 if (!(validate_col('e-Signature',formobj.eSign))) return false

								 if(isNaN(formobj.eSign.value) == true) {
									alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
									formobj.eSign.focus();
									return false;
								   }

						formobj.action = "mulEventsSave.jsp" + "?studyId="+studyId+"&arrCheck="+arrCheck+"&calassoc=S";
						return true;

			}
			if(cmbView == "event")
			{

					countEvent =formobj.countEvent.value;

					if(countEvent > 1){
						 for(i=0;i<countEvent;i++)
							{
							if(formobj.chkEvent[i].checked)
								{
									if(formobj.exeon[i].value=="")
									{
										alert("<%=MC.M_StatValidFor_Evt%>");/*alert("Please Enter Status Valid From for Checked Events");*****/
										return false;
									}
									arrCheck[k] ="1";
									k++;
									chk++;

								 }
								 else{
									 arrCheck[k] ="0";
									  k++;
								}
							}
					}
					else if(countEvent == 1)
					{
						if(formobj.chkEvent.checked)
								{
								 if(formobj.exeon.value=="")
									{
										alert("<%=MC.M_StatValidFor_Evt%>");/*alert("Please Enter Status Valid From for Checked Events");*****/
										return false;
									}
								 arrCheck[k]="1";
								 chk++;
								 }
								 else{
									 arrCheck[k]="0";

								 }
					}



						if(chk==0)
							{
							alert("<%=MC.M_PlsSelectEvt%>");/*alert("Please Select Event");*****/
							return false;
							}
							if(formobj.eSign.value== "")
							{
								alert("<%=MC.M_PlsEnterEsign%>");/*alert("Please enter e-Signature");*****/
								formobj.eSign.focus();
								return false;
							}


							 if (!(validate_col('e-Signature',formobj.eSign))) return false

							 if(isNaN(formobj.eSign.value) == true) {
								alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
								formobj.eSign.focus();
								return false;
							   }

						formobj.action = "mulEventsSave.jsp" + "?studyId="+studyId+"&arrCheck="+arrCheck+"&calassoc=S";
						return true;



			}

	}
}
}
</script>


<BODY>

<div class="popDefault">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="personB" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.esch.business.common.*"%>
<jsp:include page="include.jsp" flush="true"/>

<%
	HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
	{
		String studyId = request.getParameter("studyId");
		String calledFrom = request.getParameter("calledFrom");
		String calId=request.getParameter("calId");
		calId=(calId==null)?"":calId;

		int countVisit = 0;
		int countEvent = 0;
		String patStudyId="";

		int totalNumberOfVisits = 0;

		 %>
	<jsp:include page="sessionlogging.jsp" flush="true"/>

	<%

	String userIdFromSession = (String) tSession.getValue("userId");
	int pageRight = 0;

	String studyNumber = "";

	    studyB.setId(EJBUtil.stringToNum(studyId));
		studyB.getStudyDetails();
		studyNumber = studyB.getStudyNumber();


		int orgRight=7;
		pageRight=7;




	if ( ( pageRight >= 4 ) && ( orgRight >= 4) )
	{
%>

<Form class=formDefault id="editStdEvtFrm" name="MultipleEvent" action="" method="post" onSubmit="if (setVals(document.MultipleEvent,<%=studyId%>,document.MultipleEvent.totEvt.value)== false) { setValidateFlag('false'); return false; } else { if(document.MultipleEvent.buttonFlag.value=='S') setValidateFlag('false'); else setValidateFlag('true'); return true;}" >

	<input type="hidden" name="calledFrom" value="<%=calledFrom%>">
<%

	SchCodeDao cd = new SchCodeDao();
	cd.getCodeValues("eventstatus",0);
	String dEvStat = "";
	int stTempPk;


	String cmbView = request.getParameter("view");

	if(cmbView == null)
		{
		cmbView ="visit";
		}
	String evParmStatus = request.getParameter("dstatus");


	String cmbVisit = request.getParameter("visit");

	int tempVisit =EJBUtil.stringToNum(cmbVisit);
	int tempStat = EJBUtil.stringToNum(evParmStatus);
	if(tempStat==0 && evParmStatus==null )
		{
		  tempStat=cd.getCodePKByDesc("eventstatus", "Not Done", "ev_notdone");
		}
	StringBuffer mainStr = new StringBuffer();
	int cdcounter = 0;
	String hideCode = "";
	//int val=41;
	//if(cmbView.equals("visit"))
		mainStr.append("<SELECT NAME='dstatus'>") ;

	/*if(cmbView.equals("event"))
		mainStr.append("<SELECT NAME='dstatus'><Option value='' Selected>All</option>") ;	*/

	for (cdcounter = 0; cdcounter <= cd.getCDesc().size() -1 ; cdcounter++)
	{
		stTempPk = ((Integer) cd.getCId().get(cdcounter)).intValue() ;
		hideCode = cd.getCodeHide(cdcounter);

		if (tempStat == stTempPk )
		{
		mainStr.append("<OPTION SELECTED value = "+ cd.getCId().get(cdcounter)+">" + cd.getCDesc().get(cdcounter)+ "</OPTION>");
		}
		else
		{
			if (! StringUtil.isEmpty(hideCode) && (!hideCode.equals("Y"))){
				mainStr.append("<OPTION value = "+ cd.getCId().get(cdcounter)+">" + cd.getCDesc().get(cdcounter)+ "</OPTION>");
			}
		}
	}
	//mainStr.append("<OPTION value='' SELECTED> Select an Option</OPTION>");
	mainStr.append("</SELECT>");
	dEvStat = mainStr.toString();


    EventdefDao eventdao = new EventdefDao();
	eventdao = eventdefB.getStudyScheduleEventsVisits(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(calId));


			java.util.ArrayList visits = eventdao.getVisit();

			if (tempVisit > 0 )
			{
				totalNumberOfVisits = 1;
			}
			else
			{
				totalNumberOfVisits = visits.size();
			}



			//System.out.println("visits sonia "+ visits.size());

			java.util.ArrayList visitnames = eventdao.getVisitName();

			String visitDD="<select name='visit'><option value=\"0\" Selected>"+LC.L_All/*All*****/+"</option>"  ;

			int visitNo = 0;

		 	for(int cnt=0; cnt< visits.size();cnt++)
			{
				visitNo = EJBUtil.stringToNum(visits.get(cnt ).toString()) ;

				Integer integerVisit = new Integer(0);
				integerVisit = (Integer) visits.get(cnt) ;

				int intVisit = 0;
				intVisit = integerVisit.intValue();



				if ( tempVisit == intVisit  )
				{
					/* SV, 10/15/04, show visit name instead	"Visit"+ cnt  to eventdao.getDefinedVisitNameFromVisitNo(visitNo)*/
					if (visitnames.size() > 0) {
						visitDD = visitDD +"<option value=" + intVisit +" Selected> " + visitnames.get(cnt)  +"</option>";
}
					else {
						visitDD = visitDD +"<option value=" + intVisit +" Selected> " + "Visit "+visits.get(cnt)  +"</option>";

					}


				}
				else
				{
					/* SV, 10/15/04, show visit name instead	"Visit"+ cnt  to eventdao.getDefinedVisitNameFromVisitNo(visitNo)*/

					if (visitnames.size() > 0) {
						visitDD = visitDD +"<option value=" + intVisit +">" + visitnames.get(cnt)  +"</option>";
}
					else {
						visitDD = visitDD +"<option value=" + intVisit +"> " + "Visit "+visits.get(cnt)  +"</option>";

					}
				}
			}
			visitDD = visitDD+"</select>";

%>  <table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl">
      <tr>

		<td><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>: <%=studyNumber%></td>
	</tr>
	</table>
	<table width=100% cellspacing="0" cellpadding="0" border="0">
	<tr>
	<%
	ArrayList eventIds = null;
	ArrayList descs = null;
	ArrayList vsts = null;
	ArrayList status = null;
	ArrayList notes = null;
	ArrayList exeons = null;

	EventdefDao eventD = new EventdefDao();

	int visit=0;
	eventD = eventdefB.getVisitEvents(EJBUtil.stringToNum(studyId),tempStat,tempVisit,null,"S",EJBUtil.stringToNum(calId));
	descs = eventD.getDescriptions();
	eventIds = eventD.getEvent_ids();
	vsts = eventD.getVisit();
	status = eventD.getStatus();
	notes = eventD.getNotes();
	exeons = eventD.getEvent_exeons();


	String vst = "";
	String firstVst = "";
	String desc = "";
	String firstStatus = "";
	int uniqueVisit = 0;
	Integer visitToCompare = null;
	int ctrVst=0;

	int len= eventIds.size();

	%>

	<input type="hidden" name="totEvt" value=<%=len%>>

	<%
	//SV, iterate through the visit ArrayList and see if the events belong to only one visit or theer ar e more than one visists

	if (totalNumberOfVisits > 1 && len > 0)
	{
		for ( ctrVst=0; ctrVst < visits.size()  ; ctrVst++)
	 	{

	 		visitToCompare = (Integer) visits.get(ctrVst);

	 		// see if this visit exists in the retrieved events's visit
	 		if (vsts.indexOf(visitToCompare) > -1)
	 		{
	 			uniqueVisit = uniqueVisit +1;
	 		}

	 	}
	}
	else
	{
		uniqueVisit = 1;
	}

	totalNumberOfVisits = uniqueVisit;

	// SA, 07/27/05
	//	check the number of events, if number of events ==1, totalNumberOfVisits also becomes 1

	if (len == 1)
	{
		totalNumberOfVisits = 1;
	}




	String oldStatusId ="";
	String eveId = "";
	%>


	   </tr>
	<%if(orgRight == 5 || orgRight == 7){%>
	 <tr>
		<jsp:include page="submitBar.jsp" flush="true">
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="editStdEvtFrm"/>
			<jsp:param name="showDiscard" value="N"/>
		</jsp:include>
   </tr>
	<%}%>
	</table>
	<hr>
	<br>
	   <table width=100%>
		   <tr>
				<td colspan=7>
					<P class=defcomments><%=MC.M_VisitsEvts_AvalForSel%><%--Following are the Visit(s)/Event(s) available for the selection*****--%> </p>
				</td>
		   </tr>
		   <tr>
		   <td class=tdDefault width=10% align="center"><%=LC.L_Select%><%--Select*****--%></td>
		   <td width=10% align=left>
		   <select name="view">

		   <% if(cmbView.equals("visit") || cmbView.equals("")){%>
			   <option value="visit" selected><%=LC.L_Visit_View%><%--Visit View*****--%></option>
			   <option value="event"><%=LC.L_Evt_View%><%--Event View*****--%></option>
			   <%}
			else if(cmbView.equals("event")) {%>
			   <option value="visit" ><%=LC.L_Visit_View%><%--Visit View*****--%></option>
			   <option value="event" selected><%=LC.L_Evt_View%><%--Event View*****--%></option>
				<%}%>
		   </select>
		   </td>
		   <td class="tdDefault" width=15% align="center"><%=LC.L_Visit%><%--Visit*****--%>&nbsp;&nbsp;&nbsp;&nbsp;<%=visitDD%></td>

		   <td class=tdDefault width=40% align=left><%=MC.M_FilterVisits_EvtStat%><%--Filter Visits/Events with Status*****--%>&nbsp;&nbsp;&nbsp;&nbsp;<%=dEvStat%></td>
		   	<td width=15% align=center><button type="submit" onclick="setFlag(document.MultipleEvent)"><%=LC.L_Search%></button></td>
		   </tr>
		   </table>
			<input type="hidden" name="buttonFlag" value ="">
			<br>
<%

	if(len >= 1){

			 firstVst = vsts.get(0).toString();

			 desc = descs.get(0).toString();
			 eveId= eventIds.get(0).toString();

			String exeon = "";
			String statusId = "";
			String note= "";
			String eventId = "";

			SchCodeDao schDao = new SchCodeDao();
			int k=0;
			String[] arr = new String[len];
			String[] arrIds = new String[len];


			   String dCur = "";

			%>
				<table width="100%" cellspacing="1" cellpadding="0" border="0">
				<TH width=30% bgcolor="#9999ff"><%=LC.L_SelVisit_Evt%><%--Select Visit/Events*****--%></th>
				<th width=25% bgcolor="#9999ff"><%=LC.L_Status%><%--Status*****--%></th>
				<th width=25% bgcolor="#9999ff"><%=LC.L_Status_ValidFrom%><%--Status Valid From*****--%></th>
				<th width=20% bgcolor="#9999ff"><%=LC.L_Notes%><%--Notes*****--%></th>
<!--				<tr>
				<td><hr class="thinLine"></td>
				<td><hr class="thinLine"></td>
				<td><hr class="thinLine"></td>
				<td><hr class="thinLine"></td>
				</tr>
-->
				</table>

				<table width=100%>
					<%schDao.getCodeValues("eventstatus",0);
					 String dStatus =  schDao.toPullDown("status");
					  %>
					<td width=30%><input type="checkbox" name="selDeSelAll" onclick="selectDeSelectAll(document.MultipleEvent)"><%=LC.L_Select_All%><%--Select All*****--%></td>
					<td width=25%><%=dStatus%></td>
<%-- INF-20084 Datepicker-- AGodara --%>
				    <td width=25%><input type="text" name="caldate" class="datefield"  READONLY size=10 ></td>
					<td width=20%><A href=# onclick="updateAll(document.MultipleEvent)"><%=MC.M_Updt_AllSelcRows%><%--Update All Selected Rows*****--%></A></td>


			<%

				for(int i=0,m=0;i<len;i++)
				{
				  vst =  vsts.get(i).toString();
				  visitNo = EJBUtil.stringToNum(vst) ;

				  statusId= status.get(i).toString();
				  oldStatusId = statusId;

   				  note = ((notes.get(i)) == null)?" ":(notes.get(i)).toString();

				  eventId = eventIds.get(i).toString();

				  if(statusId!=null){
				  dCur =  schDao.toPullDown("eventstatus",EJBUtil.stringToNum(statusId));
				  }

				  if(exeons.get(i) !=null){
						exeon= exeons.get(i).toString();
				    	//exeon= exeon.substring(5,7)+"/"+exeon.substring(8,10)+"/"+exeon.substring(0,4);
						java.sql.Date exeonDt = java.sql.Date.valueOf(exeon);
						exeon= DateUtil.dateToString(exeonDt);

				  }else{exeon="";}

		/* When user selects Visit View */
		if(cmbView.equals("visit") || cmbView.equals("")){
						note="";
						exeon="";


				if(i==0){

				%>
<tr>
				<td><hr class="thinLine"></td>
				<td><hr class="thinLine"></td>
				<td><hr class="thinLine"></td>
				<td><hr class="thinLine"></td>
</tr>
   				 <tr>
					 <td width=30%><b><input type="checkBox" name="chkVst" value="<%=vst%>*<%=eveId%>"><%=eventdao.getDefinedVisitNameFromVisitNo(visitNo)%></b></td>
					<%countVisit++;%>
					<td width=25% align=left >
					<input type="hidden" name="statusId" value=<%=statusId%>><%=dCur%></td>
<%-- INF-20084 Datepicker-- AGodara --%>
					<td class=tdDefault width=25%><INPUT type="text" name="exeon" class="datefield" READONLY size=10 value=<%=exeon%>></td>
					<!-- value=<%=note%> has been changed as value="<%=note%>" - eRes Chennai Team-->
					 <td width=20%><input type="text" name="note" value="<%=note%>"></td>
					<%desc="";
					 eveId="";%>

				 </tr>
				 <%
				 }
				if(!firstVst.equals(vst)){

							arr[m] = desc;
							arrIds[m] = eveId;
							//fVst = vsts.get(i).toString();
							desc =(String) descs.get(i);
							eveId = eventIds.get(i).toString();
							%>
						<tr><td></td></tr>
					 <tr>
						<td width=30%><b><input type="checkBox" name="chkVst" value="<%=vst%>*<%=eveId%> "><%=eventdao.getDefinedVisitNameFromVisitNo(visitNo)%></b></td>
								<%countVisit++;%>

						<td width=25% align=left ><input type="hidden" name="statusId" value=<%=statusId%>><%=dCur%></td>
<%-- INF-20084 Datepicker-- AGodara --%>						
						<td class=tdDefault width=25%><INPUT type="text" class="datefield" name="exeon" READONLY size=10 value=<%=exeon%>></td>
						<!-- value=<%=note%> has been changed as value="<%=note%>" - eRes Chennai Team-->
						 <td width=20%><input type="text" name="note" value="<%=note%>"></td>

						 <td><input type="hidden" name="oldStatusId" value=<%=oldStatusId%>></td>


					</tr>
					<%if(i<len-1){
							if(!(vsts.get(i+1).toString().equals(vst)) ){
							desc =(String) descs.get(i);
							if(!eveId.equals(eventIds.get(i).toString())){
							eveId = eveId+":"+eventIds.get(i).toString();
							}
							arr[m] = desc;
							arrIds[m] = eveId;

							%> <tr>
								<td ><input type="hidden" name="eveIds" value=<%=arrIds[m]%>>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=arr[m]%></td>
							  </tr>
							<%
							}

						}

				if(i==len-1)
					{
					if(!(vsts.get(i-1).toString().equals(vst)) ){
							desc =(String) descs.get(i);
							if(!eveId.equals(eventIds.get(i).toString())){
							eveId = eveId+":"+eventIds.get(i).toString();
							}
							arr[m] = desc;
							arrIds[m] = eveId;

							%> <tr>
								<td ><input type="hidden" name="eveIds" value=<%=arrIds[m]%>>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=arr[m]%></td>
							  </tr>
							<%
							}

					}

				}



				else {
						if(i==0){%>

					<%	desc = (String) descs.get(i);
						if(!eveId.equals(eventIds.get(i).toString())){
						eveId = eveId+":"+eventIds.get(i).toString();
						  }
						}
						else{
						 desc = desc + ","+(String) descs.get(i);
						 if(!eveId.equals(eventIds.get(i).toString())){
						 eveId = eveId+":"+eventIds.get(i).toString();
						  }
						}

						 arr[m] = desc;
						 arrIds[m] = eveId;
					if(i<len-1){
					if(!(vsts.get(i+1).toString().equals(vst)) ){
					%>  <tr>
						<td ><input type="hidden" name="eveIds" value=<%=arrIds[m]%>>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=arr[m]%></td>
						</tr><%}
						}
					else if(i == len-1){%>
						 <tr >
						<td ><input type="hidden" name="eveIds" value=<%=arrIds[m]%>>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=arr[m]%></td>
						</tr>
						<%}%>
					 <td><input type="hidden" name="oldStatusId" value=<%=oldStatusId%>></td>

				<%}
			m++;}

/* When user selects Event View */
else{
			if(i==0){%>
				<tr>
						<td><hr class="thinLine"></td>
						<td><hr class="thinLine"></td>
						<td><hr class="thinLine"></td>
						<td><hr class="thinLine"></td>
						</tr>
<%--SV, 10/15/04, changed to show visit name					<tr><td width=30%><b>Visit <%=vst%></b></td></tr> --%>
					<tr><td width=30%><b><%=eventdao.getDefinedVisitNameFromVisitNo(visitNo)%></b></td></tr>

				<%desc =(String) descs.get(i);
			%>

				<tr><td>

					<input type="checkBox" name="chkEvent" value=<%=eventId%>><%=desc%></td>
				<td width=25% align=left ><input type="hidden" name="statusId" value=<%=statusId%>><%=dCur%></td>
<%-- INF-20084 Datepicker-- AGodara --%>				
					 <td class=tdDefault width=25%><INPUT type="text" name="exeon" class="datefield" READONLY size=10 value=<%=exeon%>></td>
					<!-- value=<%=note%> has been changed as value="<%=note%>" - eRes Chennai Team-->
					 <td width=20%><input type="text" name="note" value="<%=note%>"></td>
						<td><input type="hidden" name="oldStatusId" value=<%=oldStatusId%>></td></tr>

				<%countEvent++;}
				else if(!firstVst.equals(vst)){%>
							<tr><td></td></tr>
					<tr><td></td></tr><tr><td></td></tr><tr><td></td></tr>
<%--SV, 10/15/04, changed t show visit name.						<tr><td width=30%><b>Visit <%=vst%></b></td></tr> --%>

						<tr><td width=30%><b><%=eventdao.getDefinedVisitNameFromVisitNo(visitNo)%></b></td></tr>

							<%desc =(String) descs.get(i);%>
					<tr>
								<td><input type="checkBox" name="chkEvent" value=<%=eventId%>><%=desc%></td>
								<td width=25% align=left ><input type="hidden" name="statusId" value=<%=statusId%>><%=dCur%></td>
<%-- INF-20084 Datepicker-- AGodara --%>								
					 <td class=tdDefault width=25%><INPUT type="text" name="exeon" class="datefield" READONLY size=10 value=<%=exeon%>></td>
					<!-- value=<%=note%> has been changed as value="<%=note%>" - eRes Chennai Team-->
					 <td width=20%><input type="text" name="note" value="<%=note%>"></td>
					<td><input type="hidden" name="oldStatusId" value=<%=oldStatusId%>></td></tr>

				<% countEvent++;}
				else{
						desc =(String) descs.get(i);%>
					<tr>

							<td><input type="checkBox" name="chkEvent" value=<%=eventId%>><%=desc%></td>
							<td width=25% align=left ><input type="hidden" name="statusId" value=<%=statusId%>><%=dCur%></td>
<%-- INF-20084 Datepicker-- AGodara --%>							
					 <td class=tdDefault width=25%><INPUT type="text" name="exeon" class="datefield" READONLY size=10 value=<%=exeon%>></td>
					<!-- value=<%=note%> has been changed as value="<%=note%>" - eRes Chennai Team-->
					 <td width=20%><input type="text" name="note" value="<%=note%>"></td>
				<td><input type="hidden" name="oldStatusId" value=<%=oldStatusId%>></td></tr>
				<%countEvent++;}
}
				firstVst = vst;

			}//end of for loop%>
			<Input type="hidden" name="studyId" value=<%=studyId%>>
				<input type="hidden" name="countVisit" value=<%=countVisit%>>
				<input type="hidden" name="cmbView" value=<%=cmbView%>>
				<input type="hidden" name="countEvent" value=<%=countEvent%>>

	</table>
				<%



			}//end of if length
			else{	%>
			<font class="recNumber" align=center><%=MC.M_NoRecordsFound%><%--No Records Found*****--%></font>
			<%}%>
				<input type="hidden" name="studyId" value=<%=studyId%>>
				<input type="hidden" name="len" value=<%=len%>>


		</form><%

} //end of if body for page right
else
{
%>

  <jsp:include page="accessdenied.jsp" flush="true"/>

  <%

}
	}//end of else body for page right


		else{



				%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/>
<%}%>
<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>
</body>
