<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML> 
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
 </HEAD>
 
 
 <BODY> 
 <%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.util.*,javax.mail.*, javax.mail.internet.*, javax.activation.*"%> <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
 <jsp:useBean id="notify" scope="request" class="com.velos.eres.web.studyNotify.StudyNotifyJB"/>
 <jsp:useBean id="addressUserB" scope="page" class="com.velos.eres.web.address.AddressJB" />
   <%  int ret = 0; 
	String areas[],cnt,area="0",str,email;

	String messageTo = null;
	String domain = "";
	

 
 
	int totrows =0,count=0,tAreaId,rows;
	String strTArea = null; String userId = null;
	String userEmail = null;
	String src;
	src=request.getParameter("src");
 	String eSign = request.getParameter("eSign");
	String tab = request.getParameter("selectedTab");
	HttpSession tSession = request.getSession(true);
	
	String userMailStatus = "";
    int metaTime = 1;
     

  if (sessionmaint.isValidSession(tSession))

   {	
   	String uName = (String) tSession.getValue("userName");
		
   	   	
	
	 
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   	
	String oldESign = (String) tSession.getValue("eSign");
	if(!oldESign.equals(eSign)) {
	%>
	<jsp:include page="incorrectesign.jsp" flush="true"/>	
	<%
	} else {
 	String userAddressId = (String) tSession.getValue("userAddressId"); 	
	addressUserB.setAddId(EJBUtil.stringToNum(userAddressId));
 	addressUserB.getAddressDetails(); 	
	userEmail = addressUserB.getAddEmail();  
	
	totrows = Integer.parseInt(request.getParameter("selectedrows")); //Total Rows
	if(totrows == 0){
	%>
	<table width=100%>
	<tr>
	<td align=center>

	<p class = "sectionHeadings">
	<br><br><br><br><br><br><br><br><br><br><br>
	<%=MC.M_YouNotSelThrpArea_SelOne %><%-- You have not selected any Therapeutic area. Please select any one Therapeutic area to proceed.*****--%>
	</p>
	</td>
	</tr>

	<tr height=20></tr>
	<tr>
	<td align=center>
		<button onclick="window.history.back();"><%=LC.L_Back%></button>
		<!--<input tabindex = 2 id =  "h" name = "h" type="image" src="../images/jpg/Back.gif" 	onClick="window.history.back();" align="absmiddle"   border="0">-->
		
	</td>		
	</tr>		
	</table>	
	
	<%
	
	}else{
  	
		
 	userId = (String) tSession.getValue("userId");
	String ipAdd = (String) tSession.getValue("ipAdd");

	if (totrows > 1) 	   {
		areas = request.getParameterValues("subscribeme"); 
	 for (count=0;count<totrows;count++){ 
		   str = areas[count]; 		
	   tAreaId = Integer.parseInt(str.substring(0, str.indexOf("~"))) ; 	
	   strTArea = str.substring(0, str.indexOf("~"));   
	   strTArea = strTArea.trim(); 	   //save in database 	
	   notify.setStudyNotifyCodelstTarea(strTArea); 
	   notify.setStudyNotifyUserId(userId); 
	   notify.setStudyNotifyType("tarea"); 
	   notify.setCreator(userId); 
	   notify.setIpAdd(ipAdd);		   
	   notify.setStudyNotifyDetails();  		   //end of save in db
	   area = str.substring(str.indexOf("~") + 1) ; 
	   area = area.trim(); 		
	 

  %> 	<br> 	<br> 
<% 	} 
}else
{ %>
<% 	str = request.getParameter("subscribeme");
 	tAreaId = Integer.parseInt(str.substring(0, str.indexOf("~"))) ; 
	area = str.substring(str.indexOf("~") + 1);  
	strTArea = str.substring(0, str.indexOf("~"));
 	strTArea = strTArea.trim();   	//save in database 
	notify.setStudyNotifyCodelstTarea(strTArea); 
	notify.setStudyNotifyUserId(userId); 	
	notify.setStudyNotifyType("tarea");
	notify.setCreator(userId); 
	notify.setIpAdd(ipAdd);
	notify.setStudyNotifyDetails();  	//end of save in db 
	area = str.substring(str.indexOf("~") + 1) ; 	
	area = area.trim(); 	
	
	
	 
	 
	 
 %>
<%

		//end of sending mail
 }
%>

<br><br><br><br><br>

<%
  if ( EJBUtil.isEmpty(userMailStatus) )
  {
  	metaTime = 1;
	
  %>
	<p class = "sectionHeadings" align = center> <%=MC.M_Data_SvdSucc %><%-- Data was saved successfully*****--%> </p>

 <%
	 }	%>	 	

<META HTTP-EQUIV=Refresh CONTENT="<%=metaTime%>; URL=mySubscriptions.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>">
 
<%
}
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
</BODY> 
</HTML>
