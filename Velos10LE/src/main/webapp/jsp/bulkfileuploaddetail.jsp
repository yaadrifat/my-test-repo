<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<jsp:include page="localization.jsp" flush="true"/>
<head>
<title>Bulk Upload Details</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
</head>
<script>
function fdownload(formobj,pk,filename,dnldurl)
{
	formobj.file.value = filename;
	formobj.pkValue.value = pk ;
	formobj.dnldurl.value=dnldurl;
	formobj.action="bulkFileDownload.jsp";
	formobj.target = "_filedownload";
	formobj.method = "POST";
	formobj.submit();
}

</script>
<% String src;
src= request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<jsp:useBean id ="sessionmaint" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page  language = "java" import="com.velos.eres.bulkupload.business.*,com.velos.esch.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*"%>
<body>
<br>
<DIV class="tabDefTopN" id="div1">
		<jsp:include page="inventorytabs.jsp" flush="true">
		<jsp:param name="selectedTab" value="1"/>
		</jsp:include>
</DIV>
  <%
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
{
	ArrayList pkTemplate=null;
    ArrayList fkBulkEntity=null;;
    ArrayList templateName=null;;
    ArrayList createdOn=null;;
	ArrayList fkUser=null;
	String accId = (String) tSession.getAttribute("accountId");
	String userId = (String) tSession.getAttribute("userId");
	String uName = (String) tSession.getAttribute("userName");
	String ipAdd = (String) tSession.getAttribute("ipAdd");
	String mode = (String) request.getParameter("mode");
	String selectedTab = request.getParameter("selectedTab");
	CtrlDao ctrlDao = new CtrlDao();
	long freeSpace = ctrlDao.getFreeSpace(EJBUtil.stringToNum(accId));
	String accMaxStorage = (String) tSession.getAttribute("accMaxStorage");
	BulkTemplateDao bulkTemDao=new BulkTemplateDao();
	bulkTemDao.getUserTemplates(accId);
	pkTemplate=bulkTemDao.getPkTemplate();
	fkBulkEntity=bulkTemDao.getFkBulkEntity();
	templateName=bulkTemDao.getTemplateName();
	createdOn=bulkTemDao.getCreatedOn();
	fkUser=bulkTemDao.getFkUser();
   	%>
<DIV class="tabFormTopN tabFormTopN_PAS"  id="div2" style="overflow:visible">
<s:form action="bulkUpload" method="post" enctype="multipart/form-data" name="bulk">    
	<table  width="100%" cellspacing="0" cellpadding="0" border="0" >
     
      <tr >
		<td height="40"><b><font class="sectionHeadingsFrm" style="padding-left:0px;"><%=ES.ES_StrtNew_Upload%></font></b></td>
        </tr>
		<tr >
	 <td></td>
        </tr>
			 <!-- <tr >
	 <td><font size="3" color="red"><s:if test="hasActionErrors()">
   <div class="errors">
      <s:actionerror/>
   </div>
</s:if></font></td>
        </tr> -->
		 
		    <tr>
        <td   ><%=ES.ES_Select_Data_File%> </td>
		<td> <s:file name="bulkUpload" label="CSV_File" size="40"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <!-- <a href="" onClick="fdownload(document.bulk,27,bulkUploadSample.csv,bulkDownload);return false;"><font color="blue">Get the default Template</font></a> -->
		<s:url id="fileDownload" namespace="/" action="jsp/download" ></s:url>
 		<s:a href="%{fileDownload}"><%=ES.ES_Get_Def_Template%></s:a>
		</td>
        </tr>
        <tr>
        <td>
        		<font class="defComments" style="padding:0 0 0 0;"><%=ES.ES_PlsSelc_DotCSVFile%></font>
        <br/>
        <br/>
        </td>
        </tr>
	 	     
	  <tr >
	 <td><%=ES.ES_FldMapp%>  </td> <td>
	 <select name="mapping">
  <option value='0' SELECTED><%=ES.ES_Create_NewMAppt%></option>
  	 <%if(!pkTemplate.isEmpty()){
	 %>
	   <%for (int errSize=0;errSize<pkTemplate.size();errSize++)  
	   {
	   %>
		<option value='<%=pkTemplate.get(errSize)%>'><%=templateName.get(errSize)%></option>
	  <%}%>
	   <%}%>
</select> </td>
        </tr>
		 <tr >
	 <td colspan="2" align="center">  
	 <button type="submit" name="submit"><%=ES.ES_Submit%></button></td>
        </tr>
      </table>
	  
	<input type=hidden name=db value='eres'>
	<input type=hidden name=module value='specimen'>
	<input type=hidden name=tableName value='ER_BULK_UPLOAD'>
	<input type=hidden name=columnName value='BULK_FILEOBJ'>
	<input type=hidden name=pkValue value='0'>
	<input type=hidden name=pkColumnName value='PK_BULK_UPLOAD'>
	<input type=hidden name=nextPage value='../../velos/jsp/bulkuploaddetails.jsp?&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>'>
	<input type=hidden name=accId value=<%=accId%>>
  	<input type=hidden name=userId value=<%=userId%>>
	<input type=hidden name=ipAdd value=<%=ipAdd%>>
	<input type=hidden name=accountId value=<%=accId%>>
	<input type="hidden" name="type" value='F'>
     	<input type="hidden" name="mode" value=<%=mode%>>
	<input type="hidden" name="srcmenu" value=<%=src%>>
	<input type=hidden name=maxFileSize value=<%=freeSpace%>>
	<input type=hidden name=accMaxStorage value=<%=accMaxStorage%>>
	<BR>
</s:form>
</div>
  <%

}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <% 
}
%>
 <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

<DIV class="mainMenu" id = "emenu">
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>
</body>
</html>