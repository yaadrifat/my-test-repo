//this function includes all necessary js files for the application  
var Jan=L_Jan;
var January=L_January;
var Feb=L_Feb;
var February=L_February;
var Mar=L_Mar;
var March=L_March;
var Apr=L_Apr;
var April=L_April;
var May=L_May;
var Jun=L_Jun;
var June=L_June;
var Jul=L_Jul;
var July=L_July;
var Aug=L_Aug;
var August=L_August;
var Sep=L_Sep;
var September=L_September;
var Oct=L_Oct;
var October=L_October;
var Nov=L_Nov;
var November=L_November;
var Dec=L_Dec;
var December=L_December;
var Thur=L_Thur;
var Fr=L_Fr;
var Mo=L_Mo;
var Sa=L_Sa;  
var Su=L_Su;
var Th=L_Th;                                                               
var To=L_To;
var Tu=L_Tu; 
var We=L_We; 
var Clear_Date_Upper=L_Clear_Date_Upper;
function includeJS(file)
{
  var script  = document.createElement('script');  
  script.src  = file;  
  script.type = 'text/javascript';  
  script.defer = true;  
  
  document.getElementsByTagName('head').item(0).appendChild(script);  
  
}  

/* include any js files here */  
/*
includeJS('js/velos/prototype.js');
*/

/*var monthString = Array("","January","February","March","April","May","June","July","August","September","October","November","December");*****/
var monthString = Array("",January,February,March,April,May,June,July,August,September,October,November,December); 

/*var monthStringShort = Array("","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec","13");*****/
  var monthStringShort = Array("",Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec,"13");
 

var beginYr = 1850;

var finalYr = 2100;

var temp1;

var temp2;

var temp3;

var temp4;

var temp5;

var multipleTarget;

function createDate(strdate) 
{
	   var mon = Nth_Tab(strdate,calMonthPos,calDateFormatSeparator);
       var day = Nth_Tab(strdate,calDayPos,calDateFormatSeparator);
       var yr  = Nth_Tab(strdate,calYearPos,calDateFormatSeparator);
       
       if (mon.length == 3)
       {
    	    var tmpMon = mon.substring(0,1).toUpperCase() + mon.substring(1,3).toLowerCase();
       		mon = monthStringShort.indexOf(tmpMon);
       }
       
     return new Date(yr, (parseInt(mon) - 1), day) ;  

}



function processTemps(temp1,temp2) {

       if ((temp2==1)||(temp2==3)||(temp2==5)||(temp2==7)||(temp2==8)||(temp2==10)||(temp2==12)) temp3=31

  else if ((temp2==4)||(temp2==6)||(temp2==9)||(temp2==11)) temp3=30

  else if ((((temp1 % 100)==0) && ((temp1 % 400)==0)) || (((temp1 % 100)!=0) && ((temp1 % 4)==0))) temp3 = 29

  else temp3 = 28;

  return temp3;

};

 

function selectMonth(monthActual,day) {

  var selectedMonth = "";

 selectedMonth = "<select name='temp2' size='1' onChange='javascript:opener.monthTemp2(self.document.Forma1.temp1[self.document.Forma1.temp1.selectedIndex].value,self.document.Forma1.temp2[self.document.Forma1.temp2.selectedIndex].value,"+ day+");'>\r\n";



  for (var i=1; i<=12; i++) {
	
	monthval = i;
	
	if (i < 10)
		monthval = "0" + i;
		
    selectedMonth = selectedMonth + "  <option value='" + monthval + "'";

    if (i == monthActual) selectedMonth = selectedMonth + " selected";

    selectedMonth = selectedMonth + ">" + monthString[i] + "</option>\r\n";

  }

  selectedMonth = selectedMonth + "</select>\r\n";

	 
	
  return selectedMonth;
  

}

 

function selectYear(yearActual,day) {

  var selectedYear = "";

  

  selectedYear = "<select name='temp1' size='1' onChange='javascript:opener.monthTemp2(self.document.Forma1.temp1[self.document.Forma1.temp1.selectedIndex].value,self.document.Forma1.temp2[self.document.Forma1.temp2.selectedIndex].value,"+ day+");'>\r\n";

  for (var i=finalYr; i>=beginYr; i--) {

    selectedYear = selectedYear + "  <option value='" + i + "'";

    if (i == yearActual) selectedYear = selectedYear + " selected";

    selectedYear = selectedYear + ">" + i + "</option>\r\n";

  }

  selectedYear = selectedYear + "</select>";

  return selectedYear;

}

 

function createDayTable(year,month,d) {



  var thetable = "<table border='0' cellpadding='2' cellspacing='0' bgcolor='#CFC8FF'>\r\n  <tr>";

//modified by sonia abrol,07/17/08-use the date contructor to create full date
// set methods were not creating correct date in some cases ex: 31st march2008 
  
  var dateObj = new Date(year,(month-1),1);

  //dateObj.setYear(year);

  //dateObj.setMonth(month-1);

  //dateObj.setDate(1); 

  justDay = dateObj.getUTCDay();
       
   /*thetable = thetable + "\r\n    <td align='center' >Su</td><td align='center'>Mo</td><td align='center'>Tu</td><td align='center'>We</td><td align='center'>Th</td><td align='center'>Fr</td><td align='center'>Sa</td></div>\r\n  <tr>";*****/   
  thetable = thetable + "\r\n    <td align='center' >"+Su+"</td><td align='center'>"+Mo+"</td><td align='center'>"+Tu+"</td><td align='center'>"+We+"</td><td align='center'>"+Th+"</td><td align='center'>"+Fr+"</td><td align='center'>"+Sa+"</td></div>\r\n  <tr>";

  for (var j=1; j<=justDay; j++) {

    thetable = thetable + "\r\n    <td></td>";

  }

  

  var cday = fetchDay();
  
    

  if (d!= "")

  {

	  cday = d;

  }



  

  for (var i=1; i<10; i++) {

    thetable = thetable + "\r\n    <td"

	

    if (i == cday) thetable = thetable + " bgcolor='#ff0000'";

	else

	thetable = thetable + " bgcolor='#CFC8FF'";

	

    thetable = thetable + "><input type='button' value='0" + i + "' onClick='javascript:opener.temp1=self.document.Forma1.temp1[self.document.Forma1.temp1.selectedIndex].value; opener.temp2=self.document.Forma1.temp2[self.document.Forma1.temp2.selectedIndex].value; opener.temp3=\"0" +  i + "\"; self.close();'></td>";

    if (((i+justDay) % 7)==0) thetable = thetable + "\r\n  </tr>\r\n\  <tr>";

  }

  for (var i=10; i<=processTemps(year,month); i++) {

    thetable = thetable + "\r\n    <td"

    if (i == cday) thetable = thetable + " bgcolor='#ff0000'";

	else

	thetable = thetable + " bgcolor='#CFC8FF'";



    thetable = thetable + "><input type='button' value='" + i + "' onClick='javascript:opener.temp1=self.document.Forma1.temp1[self.document.Forma1.temp1.selectedIndex].value; opener.temp2=self.document.Forma1.temp2[self.document.Forma1.temp2.selectedIndex].value; opener.temp3=" + i + "; self.close();'></td>";

    if (((i+justDay) % 7)==0) thetable = thetable + "\r\n  </tr>\r\n\  <tr>";

  }

  thetable = thetable + "\r\n  </tr>\r\n</table>";

  return thetable;

}

 

function monthTemp2(year,month,day) {

  var html = ""; 

  html = html + "<html>\r\n<head>\r\n  <title>" + temp5 + "</title>\r\n</head>\r\n<body bgcolor='#CFC8FF' onUnload='opener.setDateStr();'>\r\n  <div align='center'>\r\n  <form name='Forma1'>\r\n";

  html = html + "<table width = '200'><tr><td  bgcolor = '#CFC8FF'>" + selectMonth(month,day) + "</td><td  bgcolor = '#CFC8FF'>";

  html = html + selectYear(year,day) + "</td></tr></table>";

 
	
  html = html + "<br>" + createDayTable(year,month,day);  

//  html = html + "<center><p><input type='button' name='hoy' value='today: " + month + "/" + curDay + "/" + year + "' onClick='javascript:opener.temp2 = "+ month + "; opener.temp3 = "+ curDay + "; opener.temp1 = "+ year + "; self.close();'></center>";


// Changed by Rajeev - 12/15/2003 - Begin

/*html = html + "<input type='button' name='CLEAR DATE' value='CLEAR DATE' onclick='javascript:opener.temp1=13; opener.temp2=13; opener.temp3=13; self.close();'>";*****/
  html = html + "<input type='button' name='CLEAR DATE' value='"+Clear_Date_Upper+"' onclick='javascript:opener.temp1=13; opener.temp2=13; opener.temp3=13; self.close();'>";

// Changed by Rajeev - 12/15/2003 - End

  html = html + "\r\n  </form>\r\n  </div>\r\n</body>\r\n</html>\r\n";





//  ventana = open("#","calendario","width=220,height=265");

//  ventana.document.open();

//  ventana.document.writeln(html);

//  ventana.document.close();

//  ventana.focus();




if (typeof(win)=="undefined")
{

if (navigator.appName == "Netscape") win = window.open("cal4ssl.html","win","width=305,height=270")
  else win = window.open("cal4ssl.html","win","width=220,height=280");
} else
{
 if (win.closed) 
 {
 	if (navigator.appName == "Netscape") win = window.open("cal4ssl.html","win","width=305,height=270")
  else win = window.open("cal4ssl.html","win","width=220,height=280");
 }	
	
}

win.document.open("text/html","replace");
win.document.write(html);
win.focus();
win.document.close();





}

 

function fetchYear() {

  var dtObj = new Date();

  if (navigator.appName == "Netscape") return dtObj.getYear() + 1900

  else return dtObj.getYear();

}

 

function fetchMonth() {

  var dtObj = new Date();

  return dtObj.getMonth()+1;

}

 

function fetchDay() {

  var dtObj = new Date();

  return dtObj.getDate();

}

 
function calculateValForMultipleTargets(to,from,otherTargets)
{
 
	multipleTarget = otherTargets;
	calculateVal(to,from);

}
function calculateVal(to,from) {

  temp1 = fetchYear();

  temp2 = fetchMonth();

  temp3 = fetchDay();

  

  temp4 = to;

  temp5 = from;

  

  seldate= to.value;



  //check whether the date entered by user is in correct format
  //if date is invalid then set the date to today
  
  if(seldate==null || seldate=='' || (!(validate_date_optionalmsg(to,false))) ) 

  {

    	curYr = fetchYear();

  	    curMon = fetchMonth();

        curDay = fetchDay();

  }

  else
  {
  	   curMon = Nth_Tab(seldate,calMonthPos,calDateFormatSeparator);
       curDay = Nth_Tab(seldate,calDayPos,calDateFormatSeparator);
       curYr  = Nth_Tab(seldate,calYearPos,calDateFormatSeparator);
       
       if (curMon.length == 3)
       {
    	    var tmpMon = curMon.substring(0,1).toUpperCase() + curMon.substring(1,3).toLowerCase();
       	    curMon = monthStringShort.indexOf(tmpMon);
       }
  }
	 
  temp1 = "";

  temp2 = "";

  temp3 = "";
  

  monthTemp2(curYr,curMon,curDay);

}

 

function setDateStr() {
var selectedDate;

//selectedDate = temp2 + "/" + temp3 + "/" + temp1;

/* temp2 : month
	temp3 : day
	temp1 : year
*/

var compileDate;

compileDate = calDateFormat;
 
compileDate = compileDate.replace("dd",temp3);
compileDate = compileDate.replace("yyyy",temp1);
 

compileDate = compileDate.replace("MMM",monthStringShort[parseInt(temp2,10)]);

compileDate = compileDate.replace("MM",temp2);

selectedDate  = compileDate;

// Changed by Rajeev - 12/15/2003 - Begin

if (selectedDate == ('13'+ calDateFormatSeparator+'13' + calDateFormatSeparator+ '13'))
{
	selectedDate = "";

}
// Changed by Rajeev - 12/15/2003 - End


if (temp2 != '')

	{
	  
	  temp4.value =  selectedDate;
	  temp4.focus();	    
	  if (multipleTarget.length > 0)
	  {
	  			  		
	  		for (var k=0; k < multipleTarget.length; k++)
	  			{
	  					  multipleTarget[k].value = selectedDate; 
	  						
	  			}
	  
	  }
	  
	}


}

 



/*function fnCalShowCalendar(theDateObj)

{

	var msgWindow = null;

//	validate_date(theDateObj);

	var strDate = theDateObj.value;

	if( strDate == "MM/DD/YYYY" || strDate == "" || !validate_date(theDateObj) || parseInt(Nth_Tab(theDateObj.value,3,'/')) < 1951 || parseInt(Nth_Tab(theDateObj.value,3,'/')) > 2050)

	{

		strDate = theDateObj.value;

		strDate = fnGetFullCurrentDate();

	}

	calculateVal(theDateObj,'Select Date');

	return true;

}

*/

function getFormattedDateString(  year,  dt,  month)
    {
    	//get the default setting

         compileDate =   calDateFormat;

		if (compileDate == "undefined" || compileDate == "" )
		{
			compileDate = "MM/dd/yyyy";
		}

		compileDate = compileDate.replace("dd",dt);
		compileDate = compileDate.replace("yyyy",year);
		 

		compileDate = compileDate.replace("MMM",monthStringShort[parseInt(month,10)]);

		compileDate = compileDate.replace("MM",month);
 
    	return compileDate;
    }