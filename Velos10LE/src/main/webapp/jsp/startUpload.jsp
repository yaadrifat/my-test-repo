<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<jsp:include page="localization.jsp" flush="true"/>
<head>
<title>Insert bulk upload data</title>
<script>
/*function checkBefore(size)
{ 
var i=0;
document.getElementById('demo').innerHTML ="";
       var timer = window.setInterval(function(){
               if( i == size ){
                   window.clearInterval( timer );
               }
			   //text= document.getElementById('upld').value;
               document.getElementById('demo').innerHTML =i;
			   i++;
           }, 0);
}*/
</script>
</head>
    <%@ page autoFlush="true" buffer="1094kb"%>   
<jsp:useBean id ="sessionmaint" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page  language = "java" import="com.velos.esch.business.common.*,com.velos.eres.bulkupload.web.BulkUploadJB,com.velos.eres.bulkupload.web.FileUploadAction,java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*"%>
<body onload="clearcon();">
  <%
  int j=0;
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
{
	String autospecid="";
	String savemapfld="";
	savemapfld=request.getParameter("savemapfld");
	savemapfld=savemapfld.replace("*****"," ").trim();
	String mapName="";
	
	ArrayList<Integer> rowBulk=(ArrayList)tSession.getAttribute("wholefilerows");
	String accId = (String) tSession.getValue("accountId");
	autospecid=request.getParameter("autospecid");
	ArrayList<Integer> insertFlag=(ArrayList)tSession.getAttribute("insertFlag");
	
	if (StringUtil.isEmpty(autospecid))
		{
			autospecid = "";
		}
		if (StringUtil.isEmpty(savemapfld))
		{
			savemapfld = "";
		}
	mapName=request.getParameter("savemapchk");
		if (StringUtil.isEmpty(mapName))
		{
			mapName = "";
		}
		FileUploadAction istData=new FileUploadAction();
		int size=rowBulk.size();
		
		%>
	<BR/>
	 
		
		
		<%
		j=istData.validateUpload(size,mapName,autospecid,savemapfld,accId);
		ArrayList <BulkUploadJB> insertErrors=new ArrayList(); 
		insertErrors=(ArrayList<BulkUploadJB>)tSession.getAttribute("fileerrlog");
		tSession.removeAttribute("wholefilerows");
		tSession.removeAttribute("pkbulk");
		tSession.removeAttribute("wholefiledata");
		tSession.removeAttribute("saveMapping");
		tSession.removeAttribute("bulkpkfld");
    		tSession.removeAttribute("bulkfldflag");
    		tSession.removeAttribute("fileerrlog");
    		tSession.removeAttribute("mandflag");
    		tSession.removeAttribute("filename");
    		tSession.removeAttribute("bulkfileheader");
   	%> 
	
		

	 <table class="basetbl outline midAlign lhsFont" width="100%" cellspacing="0" cellpadding="0">
	 
     	 <tr ><td width="30%" >Rows Imported : <span id='demo'>	<%=j%>
	</span> </td> </tr>
    </table>
	
	
	  <%if(!insertErrors.isEmpty()){%>
	  <BR/>
	  <table class="basetbl outline midAlign lhsFont" width="100%" cellspacing="0" cellpadding="0">
	 <tr ><td><font class="sectionHeadingsFrm"><b>Issue Log:</b></font></td> </tr>
	 <tr ><th>Row #</th><th>Issue Description</th>  </tr>
	   <%for (int errSize=0;errSize<insertErrors.size();errSize++){
		   BulkUploadJB upl=insertErrors.get(errSize);
		
		String errMsgS = upl.errMsg;
		if (errMsgS != null)
		{
		errMsgS= errMsgS.replace("allready", "already");
		}
	   %>
	 <tr ><td><%=upl.rownum%></td><td><%=errMsgS %></td>  </tr>
	  <%}%>
	   </table>
	   <%}%>
   
	
	
  <%

}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <% 
}
%>
<script>

	 //checkBefore('<%=j%>');
	 //window.title="Done";
</script>

</body>
</html>