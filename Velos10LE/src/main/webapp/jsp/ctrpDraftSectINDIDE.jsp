<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.velos.eres.web.user.ConfigFacade,com.velos.eres.web.user.ConfigObject, com.velos.eres.web.user.ConfigDetailsObject,com.velos.eres.business.common.CommonDAO"%>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC,com.velos.eres.business.studyNIHGrant.impl.StudyNIHGrantBean" %>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB" %>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<html>
<%
// CTRP-20527  :AGodara 

int studyId = (request.getParameter("studyId")== null)? 0 : (StringUtil.stringToNum(request.getParameter("studyId")));
String studyInvNum = (request.getParameter("studyInvNum")== null)? "":(request.getParameter("studyInvNum")) ;
String studyInvFlag = (request.getParameter("studyInvFlag")==null)? "":(request.getParameter("studyInvFlag"));
String displaymode = (request.getParameter("mode")==null)? "":(request.getParameter("mode"));


String dIndGrantor = "";
String dIdeGrantor = "";
String dHolder = "";
String dIndIdeProgramCode = "";

CodeDao codeDaoIndGrantor = new CodeDao();
//Getting drop down for codelst_type='INDIDEGrantor'
codeDaoIndGrantor.getCodeValues("INDIDEGrantor","IND",1);
dIndGrantor = codeDaoIndGrantor.toPullDown("indIdeGrantor",0,true);

CodeDao codeDaoIdeGrantor = new CodeDao();
//Getting drop down for codelst_type='INDIDEGrantor'
codeDaoIdeGrantor.getCodeValues("INDIDEGrantor","IDE",1);
dIdeGrantor = codeDaoIdeGrantor.toPullDown("indIdeGrantor",0,true);

CodeDao codeDaoIndIdeHolder = new CodeDao();
//Getting drop down for codelst_type='INDIDEHolder'
codeDaoIndIdeHolder.getCodeValues("INDIDEHolder");
dHolder = codeDaoIndIdeHolder.toPullDown("indIdeHolder",0,true);

CodeDao codeDaoIndIdeAccessType = new CodeDao();
//Getting drop down for codelst_type='INDIDEAccess'
codeDaoIndIdeAccessType.getCodeValues("INDIDEAccess");

%>

<%	if(displaymode.equalsIgnoreCase("readonly")){ %>
<script>

$j(document).ready(function (){
	
	$j("td", "#indIdeTable").children("select,input,a").bind("focus", function(){
		$j(this).unbind("click");
		$j(this).unbind("dblclick");
	       $j(this).blur();  
	          return ;
	     
	      });
});

</script>
<%} %>
<script>


	var nihCodeId = <%=codeDaoIndIdeHolder.getCodeId("INDIDEHolder","NIH")%>;
	var nciCodeId = <%=codeDaoIndIdeHolder.getCodeId("INDIDEHolder","NCI")%>;

function fnAddIndIdeRow(){

	var numIndIdeRowExists = 0;
	numIndIdeRowExists = document.getElementById("numIndIdeRows").value;
		
	var myRow = document.createElement('tr');
	myRow.className="browserOddRow";

	// Insert IDE/IND localized here
	<%--Start Modified to fix Bug#7969 : Akshi--%>
	var myCell = document.createElement('td');
	myCell.className="tdDefault";
	myCell.innerHTML = '<input type="hidden" name="pKStudyIndIde" value = "0" />'
      +'<select name="indIdeType" onchange="indIdeGrantorToggle(this)"><option value=""><%=LC.L_Select_AnOption%></option><option value="1"><%=LC.L_Ind%></option><option value="2" selected="selected"><%=LC.L_Ide%></option></select>';
	myRow.appendChild(myCell);

    myCell = document.createElement('td');
	myCell.className="tdDefault";
	myCell.innerHTML = '<input type="text" name="indIdeNumber" value="" maxlength="25"/>';
	myRow.appendChild(myCell);

    myCell = document.createElement('td');
	myCell.className="tdDefault";
	myCell.innerHTML = "<%=dIdeGrantor%>";
	myRow.appendChild(myCell);  

    myCell = document.createElement('td');
	myCell.className="tdDefault";
	myCell.innerHTML = "<%=codeDaoIndIdeHolder.toPullDown("indIdeHolder",-1,"onchange=fnProgramCodeListToggle(this);")%>";
	myRow.appendChild(myCell);

    myCell = document.createElement('td');
	myCell.className="tdDefault";
	myCell.innerHTML = '<input type="text" name="programCodeDesc" id="programCodeDescId'+rowNum+'" readonly value="" size="40px"/>'
						+'<input type="hidden" name="indIdeProgramCode" id="indIdeProgramCodeId'+rowNum+'" value="" />'
						+' <A href="#" onClick="openLkupProgramCode(this)"><%=LC.L_Select%></A>'
	myRow.appendChild(myCell);

	rowNum++;
						
	// implement chkbxchked function to get next field edit/non edit
	myCell = document.createElement('td');
	myCell.className="tdDefault";
	myCell.align="center";
	myCell.innerHTML = '<input type="checkbox" name="cbExpandedAccess" onClick="cbExpandedAccessFlagToggle(this)"/>' 
					 + '<input type="hidden" name="cbExpandedAccessFlag" value="0"/>';
	myRow.appendChild(myCell); 

	myCell = document.createElement('td');
	myCell.className="tdDefault";
	myCell.align="center";
	myCell.innerHTML = "<select name='indIdeAccessType'><option value=''></option></select>";
	myRow.appendChild(myCell); 

	myCell = document.createElement('td');
	myCell.className="tdDefault";
	myCell.align="center";
	myCell.innerHTML = '<input type="checkbox" name="indIdeExempt" onclick="cbFlagIndIdeExemptToggle(this)" align="middle"/>'
			 		 + '<input type="hidden" name="cbIndIdeExemptFlag" value="0"/>';
	myRow.appendChild(myCell); 

	myCell = document.createElement('td');
	myCell.className="tdDefault";
	<%--Modified to fix Bug#7968 : Raviesh--%>
	myCell.innerHTML = '<A id="delIndIde" name="delIndIde" onClick="fnDeleteIndIdeRow(this);">'
		+'<img src="../images/delete.png" alt="'+Delete_Row+'"  title="<%=LC.L_Delete%>" border="0"/></A>';
	myRow.appendChild(myCell); 

	document.getElementById('indIdeScrollTableBody').appendChild(myRow);
	if (numIndIdeRowExists == 0){
		makeMeFixedHeader('indIdeScrollTableBody');
	}
	<%--End Modified to fix Bug#7969 : Akshi--%>	
	numIndIdeRowExists++;
	document.getElementById("numIndIdeRows").value = numIndIdeRowExists;
}

//  Enhancement CTRP-20527  :AGodara
//Toggle the visibility of the INDIDE grid
//  Modified for Bug #7969  :Akshi
//Modified for Bug #15953
	var fixedHeaderCount = 0;
	function fnIndIdeGridChkbx(){
		if(document.getElementById("cbIndIdeGrid").checked){
			$j("#INDIDE").show();
			document.getElementById("indIdeTable").style.display='block';
			if(fixedHeaderCount==0){
			makeMeFixedHeader('indIdeScrollTableBody');
			fixedHeaderCount++;
			}
		}else if(document.getElementById("indIdeTable")!=null){
			$j("#INDIDE").hide();	
			document.getElementById("indIdeTable").style.display='none';
		}
	}

	var indIdeDeletedRowCount=0;


	// delete the Row and must add the id to the deletedids
	function fnDeleteIndIdeRow(currentElement){
		var formobj = document.studyScreenForm;
		if (!formobj) return;
		 //Fixed Bug#7903 : Raviesh 
		 if(confirm("<%=MC.M_YouSureWant_DelIndIdeInfo%>")){
			var numIndIdeRowExists = 0;
			numIndIdeRowExists = formobj.numIndIdeRows.value
			var i=currentElement.parentNode.parentNode.rowIndex;
	    	currentRow = currentElement.parentNode.parentNode;
		    var deletedId = currentRow.cells[0].getElementsByTagName("input")[0].value;
	   		var deletedIds = document.getElementById("deletedIndIdeRows").value;
	    
		    deletedIds = deletedId+","+deletedIds;
			document.getElementById("deletedIndIdeRows").value=deletedIds;
		
			document.getElementById('indIdeTable').deleteRow(i);
			numIndIdeRowExists--;
			formobj.numIndIdeRows.value = numIndIdeRowExists;
			if (i==1)
				makeMeFixedHeader('indIdeScrollTableBody');
		}
	}




	function validateIndIdeGrid(formobj){

		var numIndIdeRowExists=0;
		numIndIdeRowExists = formobj.numIndIdeRows.value
		if (numIndIdeRowExists==1){	      	 

			var indIdeNumber  = formobj.indIdeNumber.value;
	  		var indIdeGrantor = formobj.indIdeGrantor.value;
	  		var indIdeHolder = formobj.indIdeHolder.value;
	   	 	var indIdeProgramCode = formobj.indIdeProgramCode.value;
			var indIdeAccessType = formobj.indIdeAccessType.value;
		
			if(indIdeNumber==''){
				alert("<%=MC.M_PlsEtrInd_IdeNum%>");
				formobj.indIdeNumber.focus(); 
				return false;
			}
  			if(indIdeGrantor==''){
  	  			alert("<%=MC.M_PlsEtrSel_IdeGrantor%>");
  	  			formobj.indIdeGrantor.focus();
  	  			 return false;
  	  		}
			if(indIdeHolder==''){
				alert("<%=MC.M_PlsSelInd_IdeHldrType%>");
				 formobj.indIdeHolder.focus();
				 return false;
			 }
			var selIndex = formobj.indIdeHolder.selectedIndex;
			if((formobj.indIdeHolder.options[selIndex].value==nihCodeId || formobj.indIdeHolder.options[selIndex].value==nciCodeId) && indIdeProgramCode =='' ){ 
				alert("<%=MC.M_PlsSelNih_NicDivCode%>");
				formobj.indIdeProgramCode.focus();
				return false;
			}

			if(formobj.cbExpandedAccess.checked && indIdeAccessType==''){
				alert("<%=MC.M_PlsSelExpdInd_IdeAcc%>"); 
				formobj.indIdeAccessType.focus();
				return false;
			}    


			  	

		}else if (numIndIdeRowExists>1){
	
				for(i=0;i<numIndIdeRowExists;i++){

					var indIdeNumber  = formobj.indIdeNumber[i].value;
		    		var indIdeGrantor = formobj.indIdeGrantor[i].value;
		    		var indIdeHolder = formobj.indIdeHolder[i].value;
		   			var indIdeProgramCode = formobj.indIdeProgramCode[i].value;
					var indIdeAccessType = formobj.indIdeAccessType[i].value;
					
					if(indIdeNumber==''){alert("<%=MC.M_PlsEtrInd_IdeNum%>");formobj.indIdeNumber[i].focus(); return false;}
      				if(indIdeGrantor==''){alert("<%=MC.M_PlsEtrSel_IdeGrantor%>");formobj.indIdeGrantor[i].focus(); return false;}

      				
					if(indIdeHolder==''){alert("<%=MC.M_PlsSelInd_IdeHldrType%>"); formobj.indIdeGrantor[i].focus();return false;}
				
					if(formobj.cbExpandedAccess[i].checked && indIdeAccessType==''){
						alert("<%=MC.M_PlsSelExpdInd_IdeAcc%>"); 
						formobj.indIdeAccessType[i].focus();
						return false;
					}  
					var selIndex = formobj.indIdeHolder[i].selectedIndex;
					if((formobj.indIdeHolder[i].options[selIndex].value==nihCodeId || formobj.indIdeHolder[i].options[selIndex].value==nciCodeId) && indIdeProgramCode ==''){ 
						alert("<%=MC.M_PlsSelNih_NicDivCode%>");
						formobj.programCode[i].focus();						
						return false;
					}
				
					  	
      			}
		}
		return true;
	}
//  Enhancement CTRP-20527  :AGodara
// Toggle the checkbox flag and AccessType drop-down 
function cbExpandedAccessFlagToggle(cbExpandedAccess){

	currentRow = cbExpandedAccess.parentNode.parentNode;
    currentCell = cbExpandedAccess.parentNode;
    cbExpandedAccessFlag = currentCell.getElementsByTagName("input")[1];	  
    currentAccessType = currentRow.cells[6];	

	if(cbExpandedAccess.checked){
		cbExpandedAccessFlag.value = 1;
		currentAccessType.innerHTML= "<td class='tdDefault'><%=codeDaoIndIdeAccessType.toPullDown("indIdeAccessType",0,true)%></td>";
	}else{
		cbExpandedAccessFlag.value = 0;
		currentAccessType.innerHTML = "<td class='tdDefault'><select name='indIdeAccessType'><option value=''></option></select></td>";
		
		}
}

	
function cbFlagIndIdeExemptToggle(indIdeExempt){
	
	var currentCell = indIdeExempt.parentNode;
	var cbIndIdeExemptFlag = currentCell.getElementsByTagName("input")[1];
	if(indIdeExempt.checked){
		cbIndIdeExemptFlag.value = 1;
	}else{
		cbIndIdeExemptFlag.value = 0;
	}
}

function indIdeGrantorToggle(indIdeType){

	var currentRow = indIdeType.parentNode.parentNode;
	if(indIdeType.value==1){
		currentRow.cells[2].innerHTML = "<td class='tdDefault'><%=dIndGrantor%></td>";
	}else{
		currentRow.cells[2].innerHTML = "<td class='tdDefault'><%=dIdeGrantor%></td>";
	}
	
}

function fnProgramCodeListToggle(indIdeHolder){

	var currentRow = indIdeHolder.parentNode.parentNode;
    
	var currentIndIdeProgramCodeCell = currentRow.cells[4];

	var indIdeprogramCodeDesc = currentIndIdeProgramCodeCell.getElementsByTagName("input")[0];
	var indIdeProgramCode = currentIndIdeProgramCodeCell.getElementsByTagName("input")[1];
	indIdeprogramCodeDesc.value = "";
	indIdeProgramCode.value = "";
}


function openLkupProgramCode(linkClicked) {
	var formobj = document.studyScreenForm;
	if (!formobj) return;

	var filter;
	var currentRow = linkClicked.parentNode.parentNode;
	
    var currentIndIdeHolderCell = currentRow.cells[3];
    var selectedIndIdeHolder = currentIndIdeHolderCell.getElementsByTagName("select")[0];

    var currentIndIdeProgramCodeCell = currentRow.cells[4];
    var indIdeprogramCodeDescId = currentIndIdeProgramCodeCell.getElementsByTagName("input")[0].id;
	var indIdeProgramCodeId = currentIndIdeProgramCodeCell.getElementsByTagName("input")[1].id;
	
    var selIndex = selectedIndIdeHolder.selectedIndex;
	
	if(selectedIndIdeHolder.options[selIndex].value == nihCodeId){
		filter = "NIHInstitutionCode";
	}else if(selectedIndIdeHolder.options[selIndex].value == nciCodeId){
		filter = "NCIProgramCode";
	}else{
		return false;
	}

	
	formobj.target="Lookup";
	formobj.method="post";
	formobj.action="multilookup.jsp?viewId=6011&form="+formobj.name+"&seperator=&dfilter="+
                  filter+ "&keyword="+indIdeprogramCodeDescId+"|CODEDESC~"+indIdeProgramCodeId+"|CODEPK|[VELHIDE]&maxselect=1";

	formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');

	if (formWin && !formWin.closed) formWin.focus();
	formobj.submit();
	formobj.target="_self";
	formobj.action="#";
	void(0);
}



</script>

<!-- Enhancement CTRP-20527:AGodara -->

<%	
	int len = 0; 
	int rowNum = 0;
	StudyINDIDEDAO studyIndIdeDAO = new StudyINDIDEDAO();
	len = studyIndIdeDAO.getStudyIndIdeInfo(studyId);
%>
<script type="text/javascript">
var rowNum = <%=len%> +1;
</script>
<%
	// checking for if atleast one INDIDE record exists
	
	if(len == 0 && displaymode.equalsIgnoreCase("readonly")){%>
	<table>
		<tr>
		<td align="left">
			<p class = "defComments"><%=MC.M_NoRecordsFound%></p>
		</td>
		<td align="left">
			<span id="ctrpDraftJB.trialIndIde_error" class="errorMessage" style="display:none;"></span>
		</td>
		<td width="10%">&nbsp;</td>
		</tr>
	</table>
<% 	
	}else{
	if (len >0 || (!StringUtil.isEmpty(studyInvNum)) || studyInvFlag.equals("1")){ %>
	<%if(displaymode.equalsIgnoreCase("editable")){%>
	<%=LC.L_IndIde_Info%>
	<%} %>
	<INPUT type="checkbox" name="cbIndIdeGrid" id="cbIndIdeGrid" value="1" checked="checked" style="visibility: hidden"/> 
	<br/>
	<!-- Fixed Bug#7969 : Akshi -->
	<div id="INDIDE" class="TableContainer" border="1">
    <TABLE class="scrollTable basetbl" id="indIdeTable" style="display: block;" width="90%" CELLSPACING="1" border="1">
<% }else{ %>
<!-- Fixed Bug#7889 : Raviesh -->
	<INPUT type="checkbox" name="cbIndIdeGrid" id="cbIndIdeGrid" onclick="fnIndIdeGridChkbx()" value="1" />
	<%=LC.L_IndIde_InfoAval%>
	<br/>	
	<!-- Fixed Bug#7970 : Raviesh -->
	<div id="INDIDE" class="TableContainer" style="display: none; border-collapse:separate;"  border="1">
    <TABLE class="scrollTable basetbl" id="indIdeTable" width="90%" style="display: none;" CELLSPACING="1" border="1">
<%}%>
		<thead class="fixedHeader headerFormat">
        <TR>
 			<th align ="center" height="35px" nowrap="nowrap"><%=LC.L_IndIde_Type%><FONT class="Mandatory">* </FONT></th>
    		<th align ="center" nowrap="nowrap"><%=LC.L_IndIde_Number%><FONT class="Mandatory">* </FONT></th>
    		<th align ="center" nowrap="nowrap"><%=LC.L_IndIde_Grantor%><FONT class="Mandatory">*</FONT></th>
    		<th align ="center" nowrap="nowrap"><%=LC.L_IndIde_HldrType%><FONT class="Mandatory">*</FONT></th>
    		<!-- Fixed Bug#7904 and 7930: Raviesh -->
    		<th align ="center" nowrap="nowrap"><%=MC.M_NihInstNic_DivCode%> <%=LC.L_If_applicable%></th>
    		<th align ="center" nowrap="nowrap"><%=LC.L_Expanded_Access%>?</th>
 			<th align ="center" nowrap="nowrap"><%=LC.L_Expanded_AccType%> <%=LC.L_If_applicable%></th>
 			<th align ="center" nowrap="nowrap"><%=LC.L_Exempt%> <%=LC.L_If_applicable%></th>
 			<%if(displaymode.equalsIgnoreCase("editable")){ %>
 			<th>
 				<A id="addMoreIndIde" name="addMoreIndIde" onclick="fnAddIndIdeRow();">
					<img class="headerImage" src="../images/add.png" alt="<%=LC.L_Add%>" title="<%=LC.L_Add%>" border="0">
				</A>
 			</th>
			<%} %>		
		</TR>
		</thead>
		<tbody id="indIdeScrollTableBody" name="indIdeScrollTableBody" class="scrollContent bodyFormat" style="height:70px;">
<%	
	// Displaying one row for the old data
	if (!StringUtil.isEmpty(studyInvNum) || studyInvFlag.equals("1")){ 
		len++;
%>
		<tr name="rowIndIdeETable" class="browserOddRow">
			<td class="tdDefault"><input type="hidden" name="pKStudyIndIde" value = "0"  />
				<select name="indIdeType" onchange="indIdeGrantorToggle(this)">
					<option value=""><%=LC.L_Select_AnOption%></option>
					<option value="1" ><%=LC.L_Ind%></option>
					<option value="2" selected="selected"><%=LC.L_Ide%></option>
				</select>
			</td>			
			<td class="tdDefault"><input type="text" name="indIdeNumber" value = "<%=studyInvNum%>" maxlength="25" /></td>
			<td class="tdDefault"><%=dIdeGrantor%></td>
			<td class="tdDefault"><%=codeDaoIndIdeHolder.toPullDown("indIdeHolder",codeDaoIndIdeHolder.getCodeId("INDIDEHolder","investigator"),"onchange=fnProgramCodeListToggle(this);")%></td>
			<td class="tdDefault">
				<input type="text" name="programCodeDesc" id="programCodeDescId<%=rowNum%>" readonly size="40px" />
				<input type="hidden" name="indIdeProgramCode" id="indIdeProgramCodeId<%=rowNum%>"/>
				<A href="#" onClick='openLkupProgramCode(this)'><%=LC.L_Select%></A>
			</td>
			<td class="tdDefault" align ="center">
				<input type='checkbox' name='cbExpandedAccess' onclick='cbExpandedAccessFlagToggle(this)'/>
				<input type='hidden' name='cbExpandedAccessFlag' value="0"/>
			</td>
			<td class="tdDefault" align="center"><select name='indIdeAccessType'><option value=''></option></select></td>
			<td class="tdDefault" align ="center">
				<input type='checkbox' name='indIdeExempt' onclick="cbFlagIndIdeExemptToggle(this)"/>
				<input type='hidden' name='cbIndIdeExemptFlag' value="0" />	
			</td>
			<%if(displaymode.equalsIgnoreCase("editable")){ %>
			<td class="tdDefault">
				<A id="delIndIde" name="delIndIde" onClick="fnDeleteIndIdeRow(this);">
					<img src="../images/delete.png" alt="<%=LC.L_Delete%>"  title="<%=LC.L_Delete%>" border="0">
				</A>
			</td>
			<%} %>
		</tr>
<%	}else {
	ArrayList<Integer> pkStudyIndIdeList = studyIndIdeDAO.getIdIndIdeList();
	ArrayList<Integer> typeIndIdeList = studyIndIdeDAO.getTypeIndIdeList();
	ArrayList<String> numberIndIdeList = studyIndIdeDAO.getNumberIndIdeList();
	ArrayList<Integer> grantorList = studyIndIdeDAO.getGrantorList();
	ArrayList<Integer> holderList = studyIndIdeDAO.getHolderList();
	ArrayList<Integer> programCodeList = studyIndIdeDAO.getProgramCodeList();
	ArrayList<Integer> expandedAccessList = studyIndIdeDAO.getExpandedAccessList();
	ArrayList<Integer> accessTypeList = studyIndIdeDAO.getAccessTypeList();
	ArrayList<Integer> exemptIndIdeList = studyIndIdeDAO.getExemptIndIdeList();
	len = grantorList.size();
	CodeDao programCodeCodeDao = new CodeDao();
	if(len>0){
	for ( ; rowNum < len; rowNum ++){
		String pkStudyIndIde=((pkStudyIndIdeList.get(rowNum)) == null)?"":(pkStudyIndIdeList.get(rowNum).toString());
		int typeIndIde = typeIndIdeList.get(rowNum);
		String numberIndIde=((numberIndIdeList.get(rowNum)) == null)?"":(numberIndIdeList.get(rowNum).toString());
		int grantor = grantorList.get(rowNum);
		int holder = holderList.get(rowNum);
		int programCode = programCodeList.get(rowNum);
		String programCodeDesc = programCodeCodeDao.getCodeDescription(programCode)==null?"":programCodeCodeDao.getCodeDescription(programCode); 
		String expandedAccess =((expandedAccessList.get(rowNum)) == null)?"":(expandedAccessList.get(rowNum).toString());
		int accessType =accessTypeList.get(rowNum);
		String exemptIndIde =((exemptIndIdeList.get(rowNum)) == null)?"":(exemptIndIdeList.get(rowNum).toString());
%>
	<tr name="rowIndIdeTable" class="browserOddRow">
	<td class="tdDefault"><input type="hidden" name="pKStudyIndIde" value = "<%=pkStudyIndIde%>" />
	
	<% if(typeIndIde==1){%>
		<select name="indIdeType" onchange="indIdeGrantorToggle(this)">	
			<option value=""><%=LC.L_Select_AnOption%></option>
			<option value="1" selected="selected"><%=LC.L_Ind%></option>
			<option value="2"><%=LC.L_Ide%></option>
		</select></td>
		<td class=tdDefault><input type="text" name="indIdeNumber" value = "<%=numberIndIde%>" maxlength="25" /></td>
		<td class=tdDefault><%=codeDaoIndGrantor.toPullDown("indIdeGrantor",grantor,true)%></td>		
	<%}else{ %>
		<select name="indIdeType" onchange="indIdeGrantorToggle(this)">
			<option value=""><%=LC.L_Select_AnOption%></option>
			<option value="1"><%=LC.L_Ind%></option>
			<option value="2" selected="selected"><%=LC.L_Ide%></option>
		</select></td>
		<td class=tdDefault><input type="text" name="indIdeNumber" value = "<%=numberIndIde%>"  maxlength="25" /></td>
		<td class=tdDefault><%=codeDaoIdeGrantor.toPullDown("indIdeGrantor",grantor,true)%></td>
	<%} dHolder = codeDaoIndIdeHolder.toPullDown("indIdeHolder",holder,"onchange=fnProgramCodeListToggle(this);"); %>

		<td class=tdDefault><%=dHolder%></td>
		<td class=tdDefault>
			<input type="text" name="programCodeDesc" id="programCodeDescId<%=rowNum%>" readonly value="<%=programCodeDesc%>" size="40px"/>
			<input type="hidden" name="indIdeProgramCode" id="indIdeProgramCodeId<%=rowNum%>" value="<%=programCode%>"/>
			<% if(displaymode.equalsIgnoreCase("editable")){ %>
			<A href="#" onClick="openLkupProgramCode(this)"><%=LC.L_Select%></A>
			<%}%>
		</td>
<% 	if(expandedAccess.equals("1")&& displaymode.equalsIgnoreCase("editable")){ %>
		<td class="tdDefault" align ="center"><input type='checkbox' name='cbExpandedAccess' onclick='cbExpandedAccessFlagToggle(this)' align="middle" checked="checked"/>
		<input type='hidden' name='cbExpandedAccessFlag' value="1"/></td>
		<td class="tdDefault" align="center"><%=codeDaoIndIdeAccessType.toPullDown("indIdeAccessType",accessType,true)%></td>
<%}else if(!expandedAccess.equals("1")&& displaymode.equalsIgnoreCase("editable")){ %>
		<td class="tdDefault" align ="center"><input type='checkbox' name='cbExpandedAccess' onclick='cbExpandedAccessFlagToggle(this)' align="middle" />
		<input type='hidden' name='cbExpandedAccessFlag' value="0"/></td>		
		<td class="tdDefault" align ="center"><select name='indIdeAccessType'><option value=''></option></select></td>
<% }else if(expandedAccess.equals("1")&& displaymode.equalsIgnoreCase("readonly")){  %>
		<td class="tdDefault" align ="center"><input type='checkbox' name='cbExpandedAccess' onclick="return false" onkeydown="return false" align="middle" checked="checked"/>
		<input type='hidden' name='cbExpandedAccessFlag' value="1"/></td>
		<td class="tdDefault" align ="center"><%=codeDaoIndIdeAccessType.toPullDown("indIdeAccessType",accessType,true)%></td>
<% }else if(!expandedAccess.equals("1")&& displaymode.equalsIgnoreCase("readonly")){%>
		<td class="tdDefault" align ="center"><input type='checkbox' name='cbExpandedAccess' onclick="return false" onkeydown="return false" align="middle" />
		<input type='hidden' name='cbExpandedAccessFlag' value="0"/></td>		
		<td class="tdDefault" align ="center"><select name='indIdeAccessType'><option value=''></option></select></td>

<%} 	if(exemptIndIde.equals("1")&& displaymode.equalsIgnoreCase("editable")){ %>		
		<td class="tdDefault" align="center"><input type='checkbox' name='indIdeExempt' onclick="cbFlagIndIdeExemptToggle(this)" checked="checked" align="middle"/>
		<input type='hidden' name='cbIndIdeExemptFlag' value="1"  /></td>
<%}else if(exemptIndIde.equals("1")&& displaymode.equalsIgnoreCase("readonly")){ %>		
		<td class="tdDefault" align="center"><input type='checkbox' name='indIdeExempt' onclick="return false" onkeydown="return false" checked="checked" align="middle"/>
		<input type='hidden' name='cbIndIdeExemptFlag' value="1"  /></td>
<%} else if(!exemptIndIde.equals("1")&& displaymode.equalsIgnoreCase("readonly")){%>		
		<td class="tdDefault" align="center"><input type='checkbox' name='indIdeExempt' onclick="return false" onkeydown="return false" align="middle"/>
		<input type='hidden' name='cbIndIdeExemptFlag' value="0"  /></td>
<%}else if(!exemptIndIde.equals("1")&& displaymode.equalsIgnoreCase("editable")){ %>
		<td class="tdDefault" align="center"><input type='checkbox' name='indIdeExempt' onclick="cbFlagIndIdeExemptToggle(this)" align="middle"/>
		<input type='hidden' name='cbIndIdeExemptFlag' value="0"  /></td>
<%} %>
		<!--delete things-------->
		<%if(displaymode.equalsIgnoreCase("editable")){ %>
		<td class="tdDefault">
			<A id="delIndIde" name="delIndIde" onClick="fnDeleteIndIdeRow(this);">
				<img src="../images/delete.png" title="<%=LC.L_Delete%>" alt="<%=LC.L_Delete%>" border="0">
			</A>
		</td>
	<%} %>
	</tr>
<%		}
	}else{
		len++;
%>
	<tr name="rowIndIdeETable" class="browserOddRow">
			<td class="tdDefault"><input type="hidden" name="pKStudyIndIde" value = "0"  />
				<select name="indIdeType" onchange="indIdeGrantorToggle(this)">
					<option value=""><%=LC.L_Select_AnOption%></option>
					<option value="1" ><%=LC.L_Ind%></option>
					<option value="2" selected="selected"><%=LC.L_Ide%></option>
				</select>
			</td>			
			<td class="tdDefault"><input type="text" name="indIdeNumber" value = "" maxlength="25" /></td>
			<td class="tdDefault"><%=dIdeGrantor%></td>
			<td class="tdDefault"><%=codeDaoIndIdeHolder.toPullDown("indIdeHolder",-1,"onchange=fnProgramCodeListToggle(this);")%></td>
			<td class="tdDefault">
				<input type="text" name="programCodeDesc" id="programCodeDescId<%=rowNum%>" readonly size="40px"/>
				<input type="hidden" name="indIdeProgramCode" id="indIdeProgramCodeId<%=rowNum%>" />
				<A href="#" onClick="openLkupProgramCode(this)"><%=LC.L_Select%></A>
			</td><td class="tdDefault" align ="center">
				<input type='checkbox' name='cbExpandedAccess' onclick='cbExpandedAccessFlagToggle(this)'/>
				<input type='hidden' name='cbExpandedAccessFlag' value="0"/>
			</td>
			<td class="tdDefault" align="center"><select name='indIdeAccessType'><option value=''></option></select></td>
			<td class="tdDefault" align ="center">
				<input type='checkbox' name='indIdeExempt' onclick="cbFlagIndIdeExemptToggle(this)"/>
				<input type='hidden' name='cbIndIdeExemptFlag' value="0" />					
			</td>
			<%if(displaymode.equalsIgnoreCase("editable")){ %>
			<td class="tdDefault">
				<A id="delIndIde" name="delIndIde" onClick="fnDeleteIndIdeRow(this);">
					<img src="../images/delete.png" alt="<%=LC.L_Delete%>" title="<%=LC.L_Delete%>" border="0">
				</A>
			</td>
			<%} %>
		</tr>
<%		}
	}
%>
		</tbody>
	</TABLE>
</div>
<br/>
<%
}%>
	<Input type = "hidden" id="numIndIdeRows" name="numIndIdeRows" value="<%=len%>"/>
	<Input type = "hidden" id="deletedIndIdeRows" name="deletedIndIdeRows" value="" />
<script>
if (document.getElementById('indIdeScrollTableBody')){
	if (navigator.userAgent.indexOf("MSIE") == -1){ 
		$E.onContentReady(makeMeFixedHeader('indIdeScrollTableBody'));
	}
}
</script>