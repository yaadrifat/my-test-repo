<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>



<title><%=LC.L_Alerts_Notifications%><%--Alerts and Notifications*****--%></title>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



<SCRIPT language="JavaScript1.1">




function  validate(formobj){    

  

     if (!(validate_col('e-Signature',formobj.eSign))) return false



	if(isNaN(formobj.eSign.value) == true) {

	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/

	formobj.eSign.focus();

	return false;

     }

}



function openwin1(formobj,counter) {

	  var names = formobj.enteredByName[counter].value;

	  var ids = formobj.enteredByIds[counter].value;



	  

	  while(names.indexOf(' ') != -1){
		   

	  	names = names.replace(' ','~');

	  }

	  

	  var windowToOpen = "multipleusersearchdetails.jsp?fname=&lname=&from=alertnotify&mode=initial&ids=" + ids + "&names=" + names + "&rownum=" + counter;

//alert(windowToOpen);

      window.open(windowToOpen,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")

;}









</SCRIPT>



</head>







<% String src="";

src= request.getParameter("srcmenu");

%>	



<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   



<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="eventAssocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
	
<!-- Virendra: Fixed Bug no. 4698, added import com.velos.eres.service.util.StringUtil  -->
<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.DateUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.common.*,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.*"%>


<body>
<br>


<DIV class="browserDefault" id="div1"> 
  <%
   HttpSession tSession = request.getSession(true); 

   if (sessionmaint.isValidSession(tSession))

   {

   String mode="N";//request.getParameter("mode");
   String globalFlag = "G"; //global settings for study
   String studyId;
   String selectedTab ;
   String from;
   int pageRight = 0;
   //Virendra: Fixed Bug no. 4698, added variable addAnotherWidth
   int addAnotherWidth = 0;


   studyId = request.getParameter("studyId");
   from = request.getParameter("from");
   selectedTab = request.getParameter("selectedTab");
  

%>
		<jsp:include page="studytabs.jsp" flush="true"> 
			<jsp:param name="from" value="<%=from%>"/> 
			<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
			<jsp:param name="studyId" value="<%=studyId%>"/>
		</jsp:include>

<%

	stdRights =(StudyRightsJB) tSession.getValue("studyRights");	 
	if ((stdRights.getFtrRights().size()) == 0){
 		pageRight= 0;
		}else{
		pageRight = 	Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
		}
	if (pageRight >= 4 )
	  {

	String yob = "";

	String protocolId = "";
	
	protocolId = request.getParameter("protocolId");

	// = (String) tSession.getValue("studyId");	

	String enrollId; 



	String studyTitle = "";

	String studyNumber = "";

	studyB.setId(EJBUtil.stringToNum(studyId));



    studyB.getStudyDetails();

    studyTitle = studyB.getStudyTitle();

	studyNumber = studyB.getStudyNumber();



	//patEnrollB.setPatProtId(EJBUtil.stringToNum(enrollId));

	//patEnrollB.getPatProtDetails();

	//protocolId =patEnrollB.getPatProtProtocolId();

	String protName = "";

	if(protocolId != null) {

		eventAssocB.setEvent_id(EJBUtil.stringToNum(protocolId));

		eventAssocB.getEventAssocDetails();

		protName = 	eventAssocB.getName();

	} else {

		protocolId = "";

	}



if(protocolId != null) {



%>



<table width=100%>

 <tr>
 	<td   class="sectionheadings" width="20%"><%=LC.L_Protocol_Calendar%><%--Protocol Calendar*****--%>:</td><td width ="80%"  class=tdDefault><%=protName%></td>
 </tr>

</table>

<Form name="alertnotify" id="alrtnote" method=post action="updatestudyalnotettings.jsp" onsubmit="if (validate(document.alrtnote)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">


<input type="hidden" name="selectedTab" value="<%=selectedTab%>">
<input type="hidden" name="from" value="<%=from%>">
<input type="hidden" name="srcmenu" value="<%=src%>">
	
<table width="100%" cellspacing="0" cellpadding="0" >

	<tr>	

	<td width=60%>

	<p class = "sectionHeadings" ><%=LC.L_Alert%><%--Alert*****--%></p>

	</td>

	<td width=10%>

	<%=LC.L_Send_AsEmail%><%--Send as Email*****--%>

	</td>



	<td width=20% align="center">

	<%=MC.M_SendCellPhOrPager%><%--Send To  CellPhone/Pager*****--%>

	</td>

	</tr>



<tr>

<td width=60%>&nbsp</td>

<td width=20%>&nbsp</td>

<td width=20% ><p class="defcomments">(<%=MC.M_Specify_CellEmailOrPager%><%--Specify cellphone email ID or pager number*****--%>)</p></td>

</tr>

</table>



<table width="100%" cellspacing="0" cellpadding="0" >





	<% 
	EventInfoDao alertNotify = new EventInfoDao();	

	

	alertNotify.getAlertNotifyValues(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(protocolId),"A"); 

	ArrayList anCodelstDescs=alertNotify.getAnCodelstDescs();
	ArrayList anIds=alertNotify.getAnIds();	
	ArrayList anCodelstSubTypes=alertNotify.getAnCodelstSubTypes();
	ArrayList anTypes=alertNotify.getAnTypes();
	ArrayList anuserLists=alertNotify.getAnuserLists();
	ArrayList anuserIds=alertNotify.getAnuserIds();
	ArrayList anMobIds=alertNotify.getAnMobIds();
	ArrayList anFlags=alertNotify.getAnFlags();



	String anCodelstDesc="";

	String anId="";

	String anCodelstSubType="";

	String anType="";

	String anuserList="";

	String anuserId="";

	String anMobId="";

	String anFlag="";

	String status="";

	int checkcount=0;



	int length=anIds.size();

	//out.println("study Id is " +studyId);	

	//out.println("protocolId is " +protocolId);	

	//out.println("length is " +length);
	
	int alnotCounterForLKP = -1;	

	%>



	<% for (int i=0;i<length; i++) {

	

	anCodelstDesc=((anCodelstDescs.get(i)) == null)?"-":(anCodelstDescs.get(i)).toString();


	anId=((anIds.get(i)) == null)?"-":(anIds.get(i)).toString();

	anCodelstSubType=((anCodelstSubTypes.get(i)) == null)?"":(anCodelstSubTypes.get(i)).toString();
	anType=((anTypes.get(i)) == null)?"-":(anTypes.get(i)).toString();
	anuserList=((anuserLists.get(i)) == null)?"":(anuserLists.get(i)).toString();
	anuserId=((anuserIds.get(i)) == null)?"":(anuserIds.get(i)).toString();
	anMobId=((anMobIds.get(i)) == null)?"":(anMobIds.get(i)).toString();
	anFlag=((anFlags.get(i)) == null)?"-":(anFlags.get(i)).toString();

	if(anCodelstSubType.substring(0,2).equals("al"))

	{	

	
	alnotCounterForLKP++;
	checkcount++;

	if(anFlag.equals("1")){

	status="Checked=true";

	}else{ status="";}	 





	 %>

	<tr>

	<td width=60%>

	

	<input type="checkbox" name=alertTypes<%=checkcount%> <%=status%> ><%=anCodelstDesc%> 

	</td>	

	

	<td width=15%>

	 <input type=text size=15 READONLY name=enteredByName value="<%=anuserList%>">
	 <input type="hidden" name=enteredByIds value="<%=anuserId%>">
	 <input type="hidden" name=anIds value="<%=anId%>">
	 <input type="hidden" name=studyId value="<%=studyId%>">
	 <input type="hidden" name=globalFlag value="<%=globalFlag%>">
	 <input type="hidden" name=protocolId value="<%=protocolId%>">

	</td>

	<td width=10%>

	<A HREF=# onClick='openwin1(document.alertnotify,<%=alnotCounterForLKP%>)'><%=LC.L_User_Search%><%--User Search*****--%></A>

	</td>

	<td width=15%>

	<INPUT type=text name=userMobs value='<%=anMobId%>'size=15 >

	</td>

	</tr>

	<% }} %>

</table>

<table width="700" cellspacing="0" cellpadding="0" >

	<tr>	
		<td COLSPAN=3><br>
			<p class = "sectionHeadings" ><%=LC.L_Notification_Opts%><%--Notification Options*****--%></p>
			<p class="defComments"><%=MC.M_OptsDefined_PcolCal%><%--The following options apply to Notifications defined for Events in this Protocol Calendar (Event Details:Messages Tab)*****--%> </p>
		</td>
	</tr>

	<% for (int i=0;i<length; i++) {

	anCodelstDesc=((anCodelstDescs.get(i)) == null)?"-":(anCodelstDescs.get(i)).toString();
	anId=((anIds.get(i)) == null)?"-":(anIds.get(i)).toString();
	anCodelstSubType=((anCodelstSubTypes.get(i)) == null)?"-":(anCodelstSubTypes.get(i)).toString();
	anMobId=((anMobIds.get(i)) == null)?"":(anMobIds.get(i)).toString();
	anFlag=((anFlags.get(i)) == null)?"-":(anFlags.get(i)).toString();
	anType=((anTypes.get(i)) == null)?"-":(anTypes.get(i)).toString();

	if(!anCodelstSubType.substring(0,2).equals("al"))

	{
		checkcount++;

		if(anFlag.equals("1")){
		status="Checked=true";
		}else{ status="";}	 

	%>

	<tr>

	<td>



	<input type="checkbox" name=alertTypes<%=checkcount%> <%=status%> >&nbsp;&nbsp;&nbsp;<%=anCodelstDesc%> 	

	 <input type="hidden" name=anIds value='<%=anId%>'>

	<INPUT type="hidden" name=userMobs value='' >

	 <input type="hidden" name=enteredByIds value=''>

	 <input type="hidden" name=studyId value=<%=studyId%>>

	  <input type="hidden" name=globalFlag value=<%=globalFlag%>>

	   <input type="hidden" name=protocolId value=<%=protocolId%>>

	</td>

	

	</tr>



	<% }} %>



	<tr><td>&nbsp;</td></tr>







	</tr>

	</table>

</table>	

<table width=100%>
	<tr align=center bgcolor="#cccccc">
		<td>
			<A href="studynotification.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>" type="submit"><%=LC.L_Back%></A>	
	    </td>
	<%
	 if (pageRight > 4 )
	  {
	%>
			<!-- Virendra: Fixed Bug no. 4698, added td with addAnotherWidth  -->
			<td bgcolor="<%=StringUtil.eSignBgcolor%>" valign="baseline" align="left" width="85%" <%=addAnotherWidth > 0 ? "" : "colspan=5" %>>
		   <jsp:include page="submitBar.jsp" flush="true"> 
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="alrtnote"/>
				<jsp:param name="showDiscard" value="N"/>
				<jsp:param name="noBR" value="Y"/>
		   </jsp:include>
		</td>	
	<%
		}
	%>
	<tr>

  </table>

</FORM>

<%
	}
 } // end of if for page right
} //end of if session times out

else

{



%>

  <jsp:include page="timeout.html" flush="true"/>

  <%



} //end of else body for page right



%>

   <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</div>



<div class ="mainMenu" id = "emenu">



<jsp:include page="getmenu.jsp" flush="true"/>   



</div>

</body>

</html>

