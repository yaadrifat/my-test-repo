<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title><%=LC.L_Form_RptPreview%><%--Form Report Preview*****--%></title>
	<%@ page language = "java" import = "java.util.*,java.sql.*,com.velos.eres.web.dynrep.holder.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="dynrepB" scope="page" class="com.velos.eres.web.dynrep.DynRepJB" />
	<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
	<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
	<jsp:useBean id="sasB" scope="page" class="com.velos.eres.web.common.CommonJB" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</head>
<% String src,sql="",tempStr="",sqlStr="",name="",selValue="",repFldStr="";
   int strlen=0,firstpos=-1,secondpos=-1,patId=0,studyId=0;
//src= request.getParameter("srcmenu");
%>
<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1)
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {
%>
<body style="overflow:scroll;">
<%
	} else {
%>
<body>
<%
	}
%>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<form  name="preview" method="POST">
<%

	int seq=0;
	String eolString="";
	String repFooter="",formId="",formName="";

	String value="",patientID="",studyNumber="",orderStr="",order="",filter="",dataOrder="",fltrDD="",prevSecId="",mapSecId="",headAlign="",footAlign="";
 	String allFformIdStr = "" , fldKey="";
 	;
 	boolean repeatFormHeader = true;

 	StringBuffer sbOutput = new StringBuffer();
	String respAccessRightWhereClause = "";

	int personPk=0,studyPk=0,repId=0,index=-1;
	DynRepDao dynDao=new DynRepDao();
	DynRepDao dynDaoFltr=new DynRepDao();
	ArrayList selectedfld=null;
	ArrayList dataList=new ArrayList() ;
	ArrayList tempVal=new ArrayList();
	ArrayList tempList=new ArrayList();
	ArrayList lfltrIds= new ArrayList();
	ArrayList lfltrNames= new ArrayList();
	ArrayList lfltrStrs= new ArrayList();
	String rightsWhereClause = "";
	boolean bIsEmptySQL = false;
	String patStudyFilter = "";


	HttpSession tSession = request.getSession(true);
	//for repeat column crash, 04/06/06
	int repColCount  = 0;
	int repColDataUpperLimit = 0;
	int repColCharAvailable = 0;

	if (sessionmaint.isValidSession(tSession))
	{
		%>
		<jsp:include page="sessionlogging.jsp" flush="true"/>
		<%
	String defUserGroup = (String) tSession.getAttribute("defUserGroup");


	ReportHolder container=null;
	String from=request.getParameter("from");
	String fmt = "";

	fmt =request.getParameter("exp");
	from=(from==null)?"":from;
	String fltrId=request.getParameter("fltrId");
	if (fltrId==null) fltrId="";
	String repIdStr=request.getParameter("repId");
	String containerReportId = "";

	String calledfrom = request.getParameter("calledfrom");
	if(EJBUtil.isEmpty(calledfrom)){
		calledfrom = LC.L_Rpt_Central;
	}

	/** changed by Sonia Abrol, 02/21/06, to make patient adHoc patient context sensitive, formPatient parameter
	 will be passed from patient forms */

	String formPatient = request.getParameter("formPatient");
	if (StringUtil.isEmpty(formPatient))
	{
		formPatient = "";
	}

	if (repIdStr==null) repIdStr="";

	int userId=EJBUtil.stringToNum((String)tSession.getAttribute("userId"));

	respAccessRightWhereClause = " and ( pkg_filledform.fn_getUserAccess (fk_form," + userId +",creator) > 0 )"	;

	String accId = (String) tSession.getValue("accountId");

	String fldWidthStr = "";



	 HashMap FldAttr=new HashMap();
	 HashMap TypeAttr=new HashMap();
	 HashMap FltrAttr=new HashMap();
	 HashMap SortAttr= new HashMap();
	 HashMap RepAttr= new HashMap();
	 HashMap attributes=new HashMap();
	 //This is done to remove the session varibale from previous report.

	 if (from.equals("browse")){
		// container=(ReportHolder)tSession.getAttribute("reportHolder");
		System.out.println("Browse mode container is null");
		from = "browsePreview";

	 }else{
	 //attributes=(HashMap)tSession.getAttribute("attributes");
	 container=(ReportHolder)tSession.getAttribute("reportHolder");

	 }
	  System.out.println("container::::::::::::::::::"+container);

	 if (container != null)
	 {
	 	//check if container's reportID is same as the one passed as parameter
	 		containerReportId = container.getReportId();
	 		if (StringUtil.isEmpty(containerReportId))
	 				containerReportId = "";

	 		System.out.println("^^^^^containerReportId^^^^^" + containerReportId);

	 		if (! StringUtil.isEmpty(containerReportId))
	 		{
	 			if (! containerReportId.equals(repIdStr))
	 			{
	 				//container object is for a different report
	 				System.out.println("Different report get the container again");
	 				container = null;
	 			}
	 		}
	  }


	 if (container==null){
	 DynRepDao mDynDao=new DynRepDao();
	if (repIdStr.length() > 0)
		repId=(new Integer(repIdStr)).intValue();
		//dyndao=dynrepB.fillReportContainer(repIdStr);
		mDynDao.fillReportContainer(repIdStr);
		container=mDynDao.getReportContainer();
		container.DisplayDetail();
		tSession.setAttribute("reportHolder",container);
	 }





	ArrayList secFldCols=new ArrayList();

	ArrayList formIdList=new ArrayList();
	ArrayList formNameList = new ArrayList();
	FilterContainer fltContainer = null;
	String defDateRangeFilterType = "";
	String defDateRangeFilterStr = "";
	String coreDefDateRangeFilterStr = "";
	String origFormFilterSQL = "";
	String ignoreDRFilter = "";

	ArrayList selectedCoreLookupformIds = new ArrayList();
	String  mapTablePK  = "";
 	String  mapPKTableName = "";
 	String  mapTableFilterCol = "";
	String  mapStudyColumn = "";
 	boolean checkForStudyRights = false;
 	String fldUseDataValue;
 	int fldDataSeperatorPos = 0;


	checkForStudyRights = container.getCheckForStudyRights();


	String repHeader=request.getParameter("repHeader");
	repFooter=request.getParameter("repFooter");
	if (repHeader==null) repHeader="";
	if (repHeader.length()==0){
	 repHeader=container.getReportHeader();
	if (repHeader==null) repHeader="";
	}
	//Extract alignment String from report Header
	if ((repHeader.indexOf("[VELCENTER]")>=0)||(repHeader.indexOf("[VELLEFT]")>=0)||(repHeader.indexOf("[VELRIGHT]")>=0))
	 {
	   headAlign=repHeader.substring(0,(repHeader.indexOf(":",0)));
	   repHeader=repHeader.substring(headAlign.length()+1);
	   }
	   else
	   {
	   	headAlign=request.getParameter("headerAlign");
		if (headAlign==null) headAlign="center";
	   }

	   if (headAlign.equals("[VELCENTER]")) headAlign="center";
	   if (headAlign.equals("[VELLEFT]")) headAlign="left";
	   if (headAlign.equals("[VELRIGHT]")) headAlign="right";

	//end extracting header

	if (repFooter==null) repFooter="";
	if (repFooter.length()==0)
	{
	repFooter=container.getReportFooter();
	if (repFooter==null) repFooter="";
	}

	//Extract alignment String from report Footer
	if ((repFooter.indexOf("[VELCENTER]")>=0)||(repFooter.indexOf("[VELLEFT]")>=0)||(repFooter.indexOf("[VELRIGHT]")>=0))
	 {
	   footAlign=repFooter.substring(0,(repFooter.indexOf(":",0)));
	   repFooter=repFooter.substring(footAlign.length()+1);

	 }
	   else {
	   footAlign=request.getParameter("footerAlign");
	   if (footAlign==null) footAlign="center";
	   }
	   if (footAlign.equals("[VELCENTER]")) footAlign="center";
	   if (footAlign.equals("[VELLEFT]")) footAlign="left";
	   if (footAlign.equals("[VELRIGHT]")) footAlign="right";

	//end extracting footer

	System.out.println("^^^^^^^^^^^^^^^^^^^^^^fltrId:::"+fltrId);

	// if fltrId is empty, get the first filter from the container.filterObject

	if (EJBUtil.isEmpty(fltrId))
	{
		Collection fltrSet = (container.getFilterObjects()).values();
		Iterator itrFltr = fltrSet.iterator();

		if (itrFltr.hasNext()) {
			fltContainer = (FilterContainer) itrFltr.next();
			fltContainer.Display();
			fltrId = fltContainer.getFilterId();
		}
	}
	else
	{
		fltContainer=container.getFilterObject(fltrId);
	}

	if (fltContainer!=null)
		{
 		filter=fltContainer.getFilterStr();
		defDateRangeFilterType = fltContainer.getFilterDateRangeType();
		if (StringUtil.isEmpty(defDateRangeFilterType))
		{
			defDateRangeFilterType = "";
		}
	}



	if (filter==null) filter="";

	filter=filter.trim();
	filter=StringUtil.decodeString(filter);


	 //end
	 if (dynDaoFltr!=null){
	 lfltrIds=dynDaoFltr.getFltrIds();
	 lfltrNames=dynDaoFltr.getFltrNames();
	 lfltrStrs=dynDaoFltr.getFltrStrs();
	  }

	CommonDAO common=new CommonDAO();
	ArrayList ids=new ArrayList();
	Connection conn;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	String sqlId="";
	String sqlIdTemp = "";
	if (fltContainer!=null)

	sqlId=fltContainer.getFilterStr();

	sqlId=(sqlId==null)?"":sqlId;
	origFormFilterSQL = sqlId;
	sqlId=StringUtil.decodeString(sqlId);

	formIdList = container.getFormIds();
	formNameList = container.getFormNames();

	selectedCoreLookupformIds = container.getSelectedCoreLookupformIds();

	allFformIdStr = container.getAllFormIdStr();


	String formType=container.getReportType();

	String recordCountString = "";
	String formIdComma = "";

	formType=(formType==null)?"":formType;
	formType=(formType=="study")?"S":formType;
	formType=(formType=="pat")?"P":formType;

	studyId = EJBUtil.stringToNum(container.getStudyId());
	patId = EJBUtil.stringToNum(container.getPatientId());

	//changed by Sonia Abrol, 02/21/06, to make patient adHoc patient context sensitive
	if((! StringUtil.isEmpty(formPatient)) && patId == 0)
	{
		patId = EJBUtil.stringToNum(formPatient);
	}



	if (formType.equals("S"))
	{
		recordCountString = LC.L_MatchStd_Fnd+" : ";/*recordCountString = "Matching "+LC.Std_Studies+" Found : ";*****/
	} else if (formType.equals("P"))
	{
		recordCountString = LC.L_MatchPat_Fnd+" : ";/*recordCountString = "Matching "+LC.Pat_Patients+" Found : ";*****/
	}

	//get a comma separated list of formids (normal forms)

	 for (int k=0;k<formIdList.size();k++)
	 {
	 	 if ( ! ((String)formIdList.get(k)).startsWith("C") )
	 	 {
			formIdComma = formIdComma + "," + formIdList.get(k);
		 }
	 }
	if (!  StringUtil.isEmpty(formIdComma) )
	{
		if (formIdComma.startsWith(","))
		{
			formIdComma = formIdComma.substring(1); //remove first comma
		}

	}

	//get a comma separated list of patientids
	if (sqlId.length()==0)
	{
	 sqlId="";
	 bIsEmptySQL = true;

	 if (formType.equals("P") && studyId > 0)
	 {
	 	patStudyFilter = " and ( (fk_patprot IS NULL) OR " + studyId + " = (SELECT fk_study FROM ER_PATPROT WHERE pk_patprot=fk_patprot)) " ;
	 }



	 if (! StringUtil.isEmpty(formIdComma))
	 {
		 for (int k=0;k<formIdList.size();k++)
		 {
		 	 if ( ! ((String)formIdList.get(k)).startsWith("C") )
		   if ( sqlId.length()==0)
		   	  sqlId="select distinct ID from er_formslinear where fk_form="+formIdList.get(k)  + patStudyFilter +  respAccessRightWhereClause;
		   else
		    sqlId=sqlId+" union select ID from er_formslinear where fk_form=" + formIdList.get(k) + patStudyFilter +  respAccessRightWhereClause;
		 }

	 }
	}
	else
	{
		//replace loggedinuserid place holder

		sqlId = StringUtil.replace(sqlId,"[:loggedinuser]",String.valueOf(userId));
		sqlId = StringUtil.replace(sqlId,"[:VELACCOUNT]",accId);

	}

	//see if the main id sql is empty and if there are core lookup tables selected, add basic 'select id'
 	//the sqlId is empty
	 if ( bIsEmptySQL )
	  {

	 	 	if (selectedCoreLookupformIds != null && selectedCoreLookupformIds.size() > 0)
	 	 	{
	 	 			if (fltContainer !=null)
	 	 			{
						defDateRangeFilterStr = fltContainer.getDateRangeFilterString()	;
					}

					if (! StringUtil.isEmpty(defDateRangeFilterStr))
						{
							defDateRangeFilterStr = " Where " + defDateRangeFilterStr;
           				    defDateRangeFilterStr = StringUtil.decodeString(defDateRangeFilterStr);
           				}


	 	 		//create the sql Id
	 	 		for (int ctr = 0; ctr < selectedCoreLookupformIds.size(); ctr++)
	 	 			{
	 	 				FieldContainer fldCTemp =container.getFldObject((String)selectedCoreLookupformIds.get(ctr));
							if (fldCTemp !=null)
								{
	 								 mapTablePK = fldCTemp.getPkColName();
	 								 mapPKTableName = fldCTemp.getPkTableName();
							 		 mapStudyColumn = fldCTemp.getMapStudyColumn();

									if (formType.equals("S")) //in case of study lookups, table pk is pk/fk_study
									{
										mapStudyColumn = mapTablePK;
									}


	 								 if ( sqlId.length()==0)
	 								 {
	 								 	sqlId = "Select distinct " + mapTablePK + " as ID from " + mapPKTableName;
	 								 }
	 								 else
	 								 {
	 								 	sqlId = sqlId + " union Select distinct " + mapTablePK + " as ID from " + mapPKTableName;
	 								 }


	 								ignoreDRFilter = container.getIgnoreFilters((String)selectedCoreLookupformIds.get(ctr));
									if (!StringUtil.isEmpty(ignoreDRFilter))
									{
										coreDefDateRangeFilterStr = "";
									}

	 								if (formType.equals("P") && (! StringUtil.isEmpty(coreDefDateRangeFilterStr)) )
									{
										if (defDateRangeFilterType.equals("PS"))
										{
											coreDefDateRangeFilterStr = defDateRangeFilterStr;

											if (! StringUtil.isEmpty(mapStudyColumn))
	 								 		{
	 								 			if (! StringUtil.isEmpty(coreDefDateRangeFilterStr))
								            	{
								            		coreDefDateRangeFilterStr = StringUtil.replace(coreDefDateRangeFilterStr,"[VELPAT]",mapTablePK);
								            		coreDefDateRangeFilterStr = StringUtil.replace(coreDefDateRangeFilterStr,"[VELSTUDY]",mapStudyColumn);

								            	}
				   					        }
				   					        else
				   					        {
				   					        	// if study rights are not checked, and study id is provided
				   					        	if (studyId > 0)
				   					        	{
				   					        		coreDefDateRangeFilterStr = StringUtil.replace(coreDefDateRangeFilterStr,"[VELPAT]",mapTablePK);
								            		coreDefDateRangeFilterStr = StringUtil.replace(coreDefDateRangeFilterStr,"[VELSTUDY]",String.valueOf(studyId));

				   					        	}
				   					        	else
				   					        	{
				   					        		coreDefDateRangeFilterStr = "";
				   					        	}


				   					        }

										}
									}


	 								sqlId = sqlId + coreDefDateRangeFilterStr;
	 								if (!StringUtil.isEmpty(mapStudyColumn) &&  studyId > 0)
	 								{
										if (! StringUtil.isEmpty(coreDefDateRangeFilterStr))
		 								{
		 									sqlId = sqlId  + " and " + mapStudyColumn + " = " + studyId;
		 								}
		 								else
		 								{
		 									sqlId = sqlId  + " Where " + mapStudyColumn + " = " + studyId;
		 								}
	 								}

	 								if (patId > 0 && formType.equals("P") )
	 								{
										if (! StringUtil.isEmpty(coreDefDateRangeFilterStr))
		 								{
		 									sqlId = sqlId  + " and " + mapTablePK + " = " + patId;
		 								}
		 								else
		 								{
		 									sqlId = sqlId  + " Where " + mapTablePK + " = " + patId;
		 								}
	 								}


 	 						}

	 	 							////////////
	 	 			}


	 	 	}
	 	 	mapTablePK = "";
	 		mapPKTableName = "";
	 		//System.out.println("Now main sql" +  sqlId);

	}

	//if preview mode and the report is not saved, recalculate checkForStudyRights  flag


	 	 	if ((! from.equals("browsePreview") ) && selectedCoreLookupformIds != null && selectedCoreLookupformIds.size() > 0)
	 	 	{
	 	 		System.out.println("recalculating checkForStudyRights flag");
	 	 		container.setCheckForStudyRights(false);
	 	 		checkForStudyRights = false;


	 	 		//create the sql Id
	 	 		for (int ctr = 0; ctr < selectedCoreLookupformIds.size(); ctr++)
	 	 			{
	 	 				FieldContainer fldCTemp =container.getFldObject((String)selectedCoreLookupformIds.get(ctr));
							if (fldCTemp !=null)
								{
	 								 mapStudyColumn = fldCTemp.getMapStudyColumn();

	 								 if (! StringUtil.isEmpty(mapStudyColumn))
	 								 {
	 								 	 container.setCheckForStudyRights(true);
	 								 	 checkForStudyRights = true;
	 								 	 break;
	 								 }
	 							}
	 	 				////////////
	 	 			}

	 	 	}
	/////////////////////////////////////////////////////////


	//////////////////////append the access rights filters to the main ID sql//////////////////////////////////

	if (formType.equals("P"))
	{
		// get the patient stataus date range filter string only if the original filter sql was empty
		if (defDateRangeFilterType.equals("PS") && StringUtil.isEmpty(origFormFilterSQL))
           	{
           		if (fltContainer != null)
           		{
           			defDateRangeFilterStr = fltContainer.getDateRangeFilterString()	;
           		}
           	}
           	else
           	{
           		defDateRangeFilterStr = "";
           	}


		 /* Attach Organization check */

	 	 if (checkForStudyRights || studyId > 0 )
		 {

			rightsWhereClause = "  exists ( SELECT * FROM ER_PER c, ER_PATPROT pp "+
			 	 " WHERE c.fk_account = " + accId + " and  c.pk_per = ID and pp.fk_per = pk_per and patprot_stat = 1 and (0 < pkg_user.f_chk_right_for_patprotsite(pp.pk_patprot,"+userId+") )";
				if (studyId > 0)
				{
					rightsWhereClause = rightsWhereClause + " and pp.fk_study = " + studyId ;
				}
			if (patId>0)
				 {
				 	rightsWhereClause = rightsWhereClause+ " and fk_per = "+ patId ;
				 }
			rightsWhereClause = rightsWhereClause + " )";

		 }
		 else
		 {
			 rightsWhereClause = " exists ( SELECT * FROM ER_PER c  WHERE c.fk_account = " + accId + " and c.pk_per = ID and  "   +
				 	"pkg_user.f_right_forpatsites(pk_per,"+ userId +") > 0";


		 	if (patId>0)
				 {
				 	rightsWhereClause  = rightsWhereClause  + " and pk_per = "+ patId ;
				 }
			rightsWhereClause = rightsWhereClause + " )";

		 }



	} //end of formType equals to P


	if (formType.equals("S"))
	{
		 if (! StringUtil.isEmpty(sqlId))
		 {
		 	//sqlId = sqlId + " INTERSECT ";
		 	sqlId = " Select ID from ( " + sqlId + " ) Where exists ( select * from er_study where  id = pk_study ";
		 }

		//sqlId = sqlId + " Select pk_study from er_study where ";

		if (studyId > 0)
		{
			sqlId = sqlId + " and pk_study = " + studyId + " and 1 = (Pkg_Util.f_checkStudyTeamRight(pk_study,"+userId+",'STUDYFRMACC')) ";
		}
		else
		{
		 	/* block studies to which user does not have access*/
		 	sqlId=sqlId + " and 1 = (Pkg_Util.f_checkStudyTeamRight(pk_study,"+userId+",'STUDYFRMACC') ) " ;
		}
		sqlId = sqlId + " )"; //for exists

	}

	/////////////////////////////////////////////////////////////////////////////////////////
	if(formType.equals("P"))
	{
		//get patient Code
		sqlId = "select  id  from ( " + sqlId + " ) out ";
		sqlId = sqlId + " Where " + rightsWhereClause ;
	}

	System.out.println("SQL MAIN"+sqlId);


	////////////

	sbOutput.append("<hr class=\"thickLine\">");
	sbOutput.append("<table width=\"100%\">");
	if (repHeader.length()>0)
	{
		sbOutput.append("<tr><td class=\"reportName\" align=\""+headAlign+"\"><pre>"+ repHeader+"</pre></td></tr>");
		sbOutput.append("<tr height=\"20\"></tr>");
	}
	sbOutput.append("</table>");
	  VelosWriter vWrite=new VelosWriter();
	  vWrite.GetFileWriter("SAS",".sas");
	for (int z=0;z<formIdList.size();z++)
	{

		ArrayList fldCols=new ArrayList();
		ArrayList fldNames=new ArrayList();
		ArrayList fldDispName=new ArrayList();
		ArrayList tmpFldNames=new ArrayList();
		ArrayList tmpFldDispName=new ArrayList();
		ArrayList fldWidth=new ArrayList();
		ArrayList repFldNames=new ArrayList();
		ArrayList repFldNamesSeq=new ArrayList();
		ArrayList mapSecNames=new ArrayList();
		ArrayList mapSecIds=new ArrayList();
		ArrayList fieldTypes = new ArrayList();
		ArrayList fieldDisplayDataValue = new ArrayList();
		ArrayList repFieldDisplayDataValue = new ArrayList();

		boolean anyMaskedColumn = false;
		ArrayList arMaskedColumnList = new ArrayList();

		ArrayList fldKeywords=new ArrayList();
		ArrayList arCompleteFieldList = new ArrayList();


		formId=(String)formIdList.get(z);

		if (StringUtil.isEmpty(formId))
		{
			continue;
		}

		if (formId.startsWith("C"))
		{
			//check if it exists in selectedCoreLookupformIds, if no then continue

			if (! selectedCoreLookupformIds.contains(formId))
			{
				continue;
			}

		}
	sbOutput.append("<br><br>");

	formName = (String)formNameList.get(z);

	if (fltContainer!=null)
	{
	 filter=fltContainer.getChoppedFilter(formId);
	 filter=filter.trim();


	 if (StringUtil.isEmpty(filter) && (!defDateRangeFilterType.equals("PS")) )
	 {
	 	 //get the date range filter (if defined)
	 	  ignoreDRFilter = container.getIgnoreFilters(formId);
	 	  if (StringUtil.isEmpty(ignoreDRFilter))
			{
	 			filter = fltContainer.getDateRangeFilterString();
	 		}
	 }

	 filter=StringUtil.decodeString(filter);
	 filter=(filter==null)?"":filter;
	 }
	 else
	 {
	 	defDateRangeFilterType  = "";
	 }



	SortContainer sortContainer=container.getSortObject(formId);
	if (sortContainer!=null)
	{
		dataOrder=sortContainer.getSortStr();
		if (dataOrder==null) dataOrder="";
		if (dataOrder.length()>0){
		dataOrder=StringUtil.replace(dataOrder,"[$$]",",");
		dataOrder=StringUtil.replace(dataOrder,"[$]"," ");
		orderStr=" order by "+ dataOrder;
		}
	}
	else
	{
		System.out.println("sort container null");
		orderStr = "";
	}


	FieldContainer fldContainer=container.getFldObject(formId);
	if (fldContainer!=null)
	{
		 fldKeywords = fldContainer.getFieldKeyword();

		 fldCols=fldContainer.getFieldColId();
		 fldNames=fldContainer.getFieldName();
		 fldDispName=fldContainer.getFieldDisplayName();
		 fldWidth=fldContainer.getFieldWidth();
	 	 fieldTypes = fldContainer.getFieldType();
		 fieldDisplayDataValue = fldContainer.getUseDataValues();
		 arCompleteFieldList =  fldContainer.getFieldList();


		 mapTablePK = fldContainer.getPkColName();
		 mapPKTableName = fldContainer.getPkTableName();
		 mapTableFilterCol = fldContainer.getMainFilterColName();
    	 mapStudyColumn = fldContainer.getMapStudyColumn();

	}
	else
	{
	}

	 if (fldCols.size() <= 0)
	 {

	 	sbOutput.append("<table border=\"1\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">");
		sbOutput.append("<TR class=\"browserOddRow\" > <td><B>"+LC.L_Form/*Form*****/+" : " + formName + "</B></td></TR>");
		sbOutput.append("</table><P class=\"redMessage\">"+MC.M_NoFldsSelected/*No Fields Selected*****/+"</P>");
		continue;
	 }

	tmpFldNames=fldNames;
	tmpFldDispName=fldDispName;
	mapSecNames.clear();
	mapSecIds.clear();
	repFldNames.clear();
	repFieldDisplayDataValue.clear();

	sql="";
	sqlStr="";

	int idxInMaster = -1;


	for (int i=0;i<fldCols.size();i++){


		if (formId.startsWith("C"))
		{

			//System.out.println("arCompleteFieldList" + arCompleteFieldList);
			//System.out.println("fldCols" + fldCols);
			//find the col in arCompleteFieldList
			idxInMaster = arCompleteFieldList.indexOf((String)fldCols.get(i));

			if (idxInMaster >= 0)
			{
				fldKey = (String) fldKeywords.get(idxInMaster);
				// System.out.println("fldKey" +fldKey);

				if (fldKey.startsWith("MASK_"))
				{
				  //System.out.println("madkcol" +(String)fldCols.get(i));
					anyMaskedColumn = true;
					arMaskedColumnList.add((String)fldCols.get(i));

				}
		}
		}

	 	 tempStr=(String)fldCols.get(i) ;
	  	name=(String)fldCols.get(i); //use col name instead of display name
	   if (name==null) name="";
	  if (name.length()> 30) name=name.substring(0,30);
	  if (name.indexOf("?")>=0)
	   name=name.replace('?','q');
	   if (name.indexOf("\\")>=0)
	   name=name.replace('\\','S');
	  if (tempStr.indexOf("|")>=0)
	  {
	  //store the repeated fields names in an arraylist and the sequence to remove the repeated fldnames from original fldNames list
	  //populate secfldCols to retirve section for only repeated fields.
	  secFldCols.add(tempStr);
	  repFldNames.add( StringUtil.decodeString((String)fldDispName.get(i)) );
	  repFldNamesSeq.add(new Integer(i));
	  repFieldDisplayDataValue.add((String)fieldDisplayDataValue.get(i));

	  //Sonia Abrol, 04/06/06 , for repeat field crash, if concatenated string's data is  is more than 4000 char
	  	String[] arRepColsTemp = StringUtil.chopChop(tempStr,'|');
		repColCount  = 0;

	  	if (arRepColsTemp != null)
	  	{
	  		repColCount = arRepColsTemp.length;

	  		if (repColCount > 1)
	  		{
		  		repColCharAvailable = 4000 - (repColCount * 8); //substract (number of columns * 8) 8 for [VELSEP]

			  	System.out.println("repColCount " + repColCount + "repColCharAvailable  " + repColCharAvailable  + "char" + repColCharAvailable /repColCount + "*");
	  			repColDataUpperLimit = (int) Math.floor(repColCharAvailable /repColCount) ;
			  	System.out.println("repColDataUpperLimit " + repColDataUpperLimit  + "*");
	  		}
	  		else
	  		{
	  			repColDataUpperLimit  = 4000;
	  		}
	  	}
	  	else
	  	{
	  		repColDataUpperLimit = 4000;
	  	}
		//////////////

	  tempStr=tempStr.replace('|',',');
	  tempStr=StringUtil.replaceAll(tempStr, ",", ",'-') , 1,"+ repColDataUpperLimit +") ||'[VELSEP]'|| substr(nvl(");
	  tempStr = "substr(nvl( " + tempStr + ",'-') ,1,"+ repColDataUpperLimit +")";

	     if (sqlStr.length()==0)
	  	sqlStr=" ("+tempStr+") as \"" + name+ "\"" ;
		 else
		sqlStr=sqlStr+",  ("+tempStr+") as \"" + name +"\"";
	}
	else{
		if (sqlStr.length()==0)  sqlStr=tempStr +" as \"" +name +"\"";
		else sqlStr=sqlStr + ", " + tempStr + " as \"" + name+"\"";
	}
	}


		if (anyMaskedColumn)
	{
		sqlStr = sqlStr + ", pkg_studystat.f_get_patientright("+userId+", " + defUserGroup+ " , " + mapTablePK +") as pat_detail_right";
	}


	 /* check if the filter is 'undefined' and set it to blank
	 */
	 filter=(filter.equals("undefined")?"":filter);

	 if ((filter.trim()).length()>0)
	 {
			filter= StringUtil.decodeString(filter);
			filter = " and ( " + filter + " ) ";
	 }

	 if (StringUtil.isEmpty(filter))
	 {
	 	 filter = "";
	 }

	if (formType.equals("A"))
	{
		if (! formId.startsWith("C") )
		{

		sql =" select ID ,FK_PATPROT as PATPROT," + sqlStr + " from er_formslinear where fk_form = "+ formId + filter +
		 " and fk_form in (select PK_FORMLIB from er_formlib a,er_linkedforms b,er_formstat c " +
		" Where b.fk_account = "+ accId+"  and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and  b.record_type<>'D' and "+
		"((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
		"   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+
		" and (c.formstat_enddate is null) and  "+
		"b.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=" +userId +" and usersite_right <> 0)) and "+
		"b.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user="+userId +" )) "+
		" AND ((LF_DISPLAYTYPE = 'A' ))   union " +
		"select PK_FORMLIB from er_formlib a,er_linkedforms b,er_formstat c " +
		" where b.fk_account = "+accId+" and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and b.record_type<>'D'and "+
		" ((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
		"   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+
		" and (c.formstat_enddate is null) and  "+
		" (b.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user="+userId+" )) "+
		" or b.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user="+userId+" and usersite_right <> 0))) "+
		" AND ((LF_DISPLAYTYPE = 'A' )) )" +  respAccessRightWhereClause  ;
	}
		else //core table
		{

			if (! StringUtil.isEmpty(sqlStr))
			{
				sqlStr = " , " + sqlStr;
			}

			sql =" select "+ mapTablePK + ",'' as PATPROT " + sqlStr + " from " + mapPKTableName + " Where " +  mapTablePK + "="+accId +  filter  ;
		}

	}

	if (formType.equals("S"))
	{
		if (! StringUtil.isEmpty(sqlStr))
		{
			sqlStr = " , " + sqlStr;
		}

		if (! formId.startsWith("C") )
		{
			sql =" select ID,fk_patprot as PATPROT " + sqlStr + " from er_formslinear where fk_form="+formId + filter + respAccessRightWhereClause ;
		}
		else //core table
		{
			sql =" select "+ mapTablePK + ",'' as PATPROT " + sqlStr + " from " + mapPKTableName + " Where " +  mapTablePK + " > 0  "  +  filter  ;
		}


		if (studyId > 0)
		{
			sql = sql + " and "+ mapTablePK+ " = " + studyId + " and 1 = (Pkg_Util.f_checkStudyTeamRight("+studyId+","+userId+",'STUDYFRMACC')) ";
		}
		else
		{
		 	/* block studies to which user does not have access*/
		 	sql = sql + " and  1 = (Pkg_Util.f_checkStudyTeamRight("+ mapTablePK +","+userId+",'STUDYFRMACC') ) " ;
		}

	}


	if (formType.equals("P"))
	{
			defDateRangeFilterStr = "";

		if (defDateRangeFilterType.equals("PS") && StringUtil.isEmpty(filter))
           	{
           		if (fltContainer != null)
           		{
           		  		defDateRangeFilterStr = fltContainer.getDateRangeFilterString()	;
           		}
           	}
           	else
           	{
           		defDateRangeFilterStr = "";
           	}


	 	ignoreDRFilter = container.getIgnoreFilters(formId);

		if (!StringUtil.isEmpty(ignoreDRFilter))
			{
				defDateRangeFilterStr = "";

			}


		if (! StringUtil.isEmpty(sqlStr))
		{
			sqlStr = " , " + sqlStr;
		}

		if (! formId.startsWith("C") )
		{
			sql =" select ID, a.fk_patprot as PATPROT " + sqlStr + " from er_formslinear a where fk_form = "+formId  + filter + respAccessRightWhereClause ;
		}
		else
		{

			sql =" select "+ mapTablePK + ",'' as PATPROT " + sqlStr + " from " + mapPKTableName+ " Where " +  mapTablePK + " > 0 " ;

            if (! StringUtil.isEmpty(mapStudyColumn) )
            {
            	//check study rights on basis of this column

            	sql = sql + " and (0 < pkg_user.f_chk_studyright_using_pat("+mapTablePK +","+mapStudyColumn+" ,"+userId+") ) " ;

            	  	//if defDateRangeFilterType = 'PS', and defDateRangeFilterStr is not empty, replace values

            	if (! StringUtil.isEmpty(defDateRangeFilterStr))
            	{
            		defDateRangeFilterStr = StringUtil.replace(defDateRangeFilterStr,"[VELPAT]",mapTablePK);
            		defDateRangeFilterStr = StringUtil.replace(defDateRangeFilterStr,"[VELSTUDY]",mapStudyColumn);
            		sql = sql + " and " + defDateRangeFilterStr ;
            	}

            	if (studyId>0)
		 		{
		 			sql = sql + " and "+mapStudyColumn+ " = " + studyId;
		 		}

            }

            sql = sql + filter ;
		}

		//commented by sonia, 07/12/05, we dont need to check it in outer sql as we have already checked this in Inner ID sql
		/*if ((! checkForStudyRights) && (studyId <= 0 ))
		{
		 sql = sql +  " and " + mapTablePK + " in (SELECT pk_per FROM ER_PER c ,ER_USERSITE d  WHERE c.fk_site = d. fk_site AND d. fk_user = "
		 	 + userId+  " AND d.usersite_right > 0 )";
		} */

		 if (patId>0)
		 {
		 	sql = sql + " and "+ mapTablePK + "="+ patId ;
		 }
		  else if (studyId > 0 && StringUtil.isEmpty(mapStudyColumn))
		 {
		 	 sql = sql  + " and " + mapTablePK + " in (select fk_per from er_patprot where fk_study="+studyId+ " and patprot_stat=1 and (0 < pkg_user.f_chk_right_for_patprotsite(pk_patprot,"+ userId +")) )";

 	 		if (! StringUtil.isEmpty(defDateRangeFilterStr))
            	{
            		defDateRangeFilterStr = StringUtil.replace(defDateRangeFilterStr,"[VELPAT]",mapTablePK);
            		defDateRangeFilterStr = StringUtil.replace(defDateRangeFilterStr,"[VELSTUDY]",String.valueOf(studyId));
            		sql = sql + " and " + defDateRangeFilterStr ;

            	}


		 }

		if (! formId.startsWith("C") )
		{
			if (studyId>0)
		 	{
		 		sql = sql + " and ( (a.fk_patprot is null) or " + studyId + " = (SELECT fk_study FROM ER_PATPROT WHERE pk_patprot=a.fk_patprot)) ";
		 	}
			 sql = sql + " AND ((a.fk_patprot is null) or (1 = Pkg_Util.f_checkStudyTeamRight((SELECT fk_study FROM ER_PATPROT WHERE pk_patprot=a.fk_patprot),"+userId+",'STUDYFRMACC')) )"  ;

				if (! StringUtil.isEmpty(defDateRangeFilterStr))
            	{
            		defDateRangeFilterStr = StringUtil.replace(defDateRangeFilterStr,"[VELPAT]","ID");

            		if (studyId>0)
            		{
            			defDateRangeFilterStr = StringUtil.replace(defDateRangeFilterStr,"[VELSTUDY]",String.valueOf(studyId));
            			sql = sql + " and " + defDateRangeFilterStr  ;
            		}
            		else
            		{
            			defDateRangeFilterStr = StringUtil.replace(defDateRangeFilterStr,"[VELSTUDY]","(SELECT fk_study FROM ER_PATPROT WHERE pk_patprot=a.fk_patprot)");
            			sql = sql + " and ((a.fk_patprot is null) or " + defDateRangeFilterStr + " )";
            		}



            	}


		}

	}

	//append id sql at the end of the SQL to filter appropriate patients

	sql = 	sql + " and " + mapTablePK +" in (" +  sqlId + ")";

	if (orderStr.length()>0) sql = sql+ "  " + orderStr ;
	else
	{
		//changed to column number
		if (formType.equals("P")) sql = sql+ "  " + " order by 1" ;
		else if (formType.equals("S")) sql =sql+ "  " + " order by 1" ;
	}

	//sql="select "+ sqlStr+" from er_formslinear where fk_form="+formId;
	System.out.println("sonia sql!!" + sql+formType);
	if (formId.startsWith("C")) formId=formId.substring(1);
	SasExportDao sas=sasB.getSasHandle("madhoc",EJBUtil.stringToNum(repIdStr));
	sas.setFormId(formId);
	//Replace special chracters in dataset name
	formName=StringUtil.replace(formName," ","");
	formName=StringUtil.replace(formName,"(","");
	formName=StringUtil.replace(formName,"-","");
	//end replacing
	sas.setFormName(formName); //SA,27Apr10 - the form name is set here but not used anymore to create vaiables or to define dataset

	System.out.println("fldContainer.getFieldList()" + fldContainer.getFieldPkList());
	sas.setColList(fldContainer.getFieldPkList());
	System.out.println("fldContainer.getFieldTypeListSSSSSS()" + fldContainer.getFieldTypeList());
	sas.setColTypes(fldContainer.getFieldTypeList());
	sas.setReportType(container.getReportType());
	sas.setReportCol(fldContainer.getFieldColId());
	System.out.println("fldContainer.getFieldColIdVVVVVVVVVV" + fldContainer.getFieldColId());
	sas.setReportColName(fldContainer.getFieldName());
	sas.setReportColType(fldContainer.getFieldType());
	sas.setReportColDisp(fldContainer.getFieldDisplayName());
	sas.setColUseDataValues(fldContainer.getUseDataValues());
	System.out.println("fldContainer.fldContainer.getUseDataValues()" + fldContainer.getUseDataValues());
	sas.GenerateStructure();

	Hashtable moreParameters = new Hashtable();
	 if (anyMaskedColumn)
		 {
		 	moreParameters.put("arMaskedColumnList", arMaskedColumnList);
		 	moreParameters.put("maskedReferenceColumn", "pat_detail_right");
		 }

	sas.GenerateData(sql,(( String [] ) fldNames.toArray ( new String [ fldNames.size() ] )),formType,fldContainer.getFieldType() , moreParameters);

	vWrite.writeFile("options validvarname=any;\n");
	//System.out.println((sas.getProcFormat()).toString());
	vWrite.writeFile((sas.getProcFormat()).toString());

	//SA, 04/27/10 - form name will not be used to geneate the DATA name. form name can have characters that are not supported by SAS and can cause crashes

	vWrite.writeFile("DATA "+ vWrite.generateFileName("EX","",false) +";\n");
	vWrite.writeFile((sas.getFormatCommand()).toString());
	System.out.println((sas.getFormatCommand()).toString());
	vWrite.writeFile((sas.getLabelCommand()).toString());
	System.out.println((sas.getLabelCommand()).toString());
	vWrite.writeFile("INFILE DATALINES DSD DLM='|' TRUNCOVER;\n");
	vWrite.writeFile((sas.getProcInput()).toString() +"\n");
	vWrite.writeFile("DATALINES;\n");
	vWrite.writeFile((sas.getProcData()).toString().replaceAll("&#160;",""));


	}
	String downloadPath="";
	String filePath = null;
	downloadPath=vWrite.getDownloadStr();
	vWrite.closeFile();
	String requestURL  = (request.getRequestURL()==null) ? "" : (request.getRequestURL().toString());
	String queryString = (request.getQueryString()==null) ? "" : (request.getQueryString().toString());
	filePath =com.aithent.file.uploadDownload.Configuration.DOWNLOADFOLDER; 
	downloadPath=downloadPath+"&repId="+repIdStr+"&moduleName="+calledfrom+"&requestURL="+requestURL+"&queryString"+queryString+"&filePath="+filePath;
	System.out.println("Path to Open the file is"+downloadPath);%>
	<META HTTP-EQUIV=Refresh CONTENT="0; URL=<%=downloadPath%>">
	<%
	/*Changes for: INF-22402 Date:10/04/2012 By:Yogendra Pratap Singh*/
	//String filePath = null;
	String fileName = null;
	fileName = downloadPath.substring(downloadPath.indexOf("file=")+5);
	//filePath =com.aithent.file.uploadDownload.Configuration.DOWNLOADFOLDER; 
	
	int downloadFlag=0;
	String downloadFormat = null;
	if (fileName.indexOf("doc")!=-1){
		downloadFormat = LC.L_Word_Format;
		downloadFlag=1;
	}else if (fileName.indexOf("xls")!=-1){
		downloadFormat = LC.L_Excel_Format;
		downloadFlag=1;
	}else if (fileName.indexOf("sas")!=-1){
		downloadFormat = LC.L_Sas_Format;
		downloadFlag=1;
	}
	%><%-- 
	<jsp:include page="userReportLogging.jsp" flush="true">
		<jsp:param 	value="<%=repIdStr %>" 		name="repId"/>
		<jsp:param 	value="<%=fileName%>" 		name="fileName"/>
		<jsp:param 	value="<%=filePath %>"		name="fileDnPath"/>
		<jsp:param 	value="<%=calledfrom%>"		name="moduleName"/>
		<jsp:param 	value="" 					name="repXsl"/>
		<jsp:param 	value="" 					name="repXml"/>
		<jsp:param 	value="<%=downloadFlag%>"	name="downloadFlag"/>
		<jsp:param value="<%=downloadFormat %>" name="downloadFormat"/>
	</jsp:include>--%>
	<p class="sectionHeading"><%=LC.L_Sas_Exported%><%--SAS Exported*****--%></p>
<%} else {  //else of if body for session
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
</body>
</html>
