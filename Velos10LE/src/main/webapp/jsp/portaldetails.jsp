<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<%
	String src;
	src= request.getParameter("srcmenu");
%>
<!--  Modified By parminder singh Bug #10726-->
<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>
<jsp:include page="panel.jsp" flush="true">
	<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<html>
<head>
	<title>   <%-- Portal Details*****--%><%=LC.L_Portal_Dets%></title>
	<SCRIPT LANGUAGE = "JavaScript">
	var screenWidth = screen.width;
	var screenHeight = screen.height;
	var prevportalPatPopstd;
	var prevportalPatPoporg;
	var prevportalPatPopall;
	var prevportalPatPoppat;
	var prevStd;
	var prevOrg;
	var prevPat;
	var prevEmailnotify ;

    //code added by KN for defect 2965
   function setPreviousPatValues() {
    	enableSubmit(true);
   var portalform ;

   portalform = document.portal;

   if (portalform != undefined)
   {

     prevportalPatPopstd=portal.portalPatPop[0].checked;
	 prevportalPatPoporg=portal.portalPatPop[1].checked;
	 prevportalPatPopall=portal.portalPatPop[2].checked;
	 prevportalPatPoppat=portal.portalPatPop[3].checked;
	 prevStd = portal.selStudy.value;
	 prevOrg = portal.selOrg.value;
	 prevPat = portal.patientNames.value;
	 prevEmailnotify = portal.emailNotify.checked ;
   }

  }     //Modified By parminder singh Bug #10726
		setPreviousPatValues();
</SCRIPT>

<script>
$j(document).ready(function(){
	$j(".passwordCheck").passStrength({
		shortPass: 		"validation-fail",
		valid    :      "validation-pass",
		Invalid  :      "validation-fail",
		okPass:         "",
		messageloc:     1,
		comeFrom:       2 
		
	});
})
</script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body  onLoad="setPreviousPatValues()">
	<%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.grpRights.GrpRightsJB"%>
	<%@page import="com.velos.eres.service.util.MC"%>
   <%@page import="com.velos.eres.service.util.LC"%>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="portalB"  scope="request" class="com.velos.eres.web.portal.PortalJB"/>
	<jsp:useBean id="accountB" scope="page" class="com.velos.eres.web.account.AccountJB" />
	<jsp:useBean id="userB"    scope="page" class="com.velos.eres.web.user.UserJB" />
	<jsp:useBean id="modCtlDao" scope="request" class="com.velos.eres.business.common.CtrlDao"/>


<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT Language="javascript">

function validate(formobj){

    //code added by KN for defect 2965
     if(prevportalPatPopstd != portal.portalPatPop[0].checked){
           formobj.patmodFlag.value = "Y";
	}
	if(prevportalPatPoporg != portal.portalPatPop[1].checked){
	   formobj.patmodFlag.value = "Y";
	}
	if(prevportalPatPopall !=portal.portalPatPop[2].checked){
	   formobj.patmodFlag.value = "Y";
	}
	if(prevportalPatPoppat !=portal.portalPatPop[3].checked){
	   formobj.patmodFlag.value = "Y";
	}
	if(prevStd != portal.selStudy.value){
	    formobj.patmodFlag.value = "Y";
	}
	if(prevOrg != portal.selOrg.value){
	   formobj.patmodFlag.value = "Y";
	}
	if(prevPat != portal.patientNames.value){
	   formobj.patmodFlag.value = "Y";
	}

	//commented by Sonia, we dont need to reinsert portal population if notification flag changes, they are not related

	/*if(prevEmailnotify != portal.emailNotify.checked){
	   formobj.patmodFlag.value = "Y";
	}*/

	if (!(validate_col('portalName',formobj.portalName))) return false;

	if(formobj.portalDesc.value.length > 4000) {
		/*alert("Description allowed 4000 Characters only");*****/
		alert("<%=MC.M_DescAllow_4000Char%>");
		formobj.portalDesc.focus();
		return false;

	}

	if((formobj.portalPatPop[0].checked == true) && (formobj.selStudyIds.value=="")){

		 /*alert("Please Select a <%=LC.Std_Study%>");*****/
		       alert("<%=MC.M_Selc_Std%>");
		 return false;
	}

	if((formobj.portalPatPop[1].checked == true) && (formobj.selOrg.value=="")){

		 /*alert("Please Select Organization");*****/
		     alert("<%=MC.M_PlsSelectOrg%>");
		 return false;
	}
	//JM: 20Aug2007
	if((formobj.portalPatPop[3].checked == true) && (formobj.patientNames.value=="")){

		/*alert("Please Select <%=LC.Pat_Patient%>(S)");*****/
		     alert("<%=LC.L_PlsSel_Pat_S%>");
		 return false;

		 }

	if ((formobj.createLoginsFlag.checked==true) && ((validate_col_no_alert("defPasshide",formobj.defPasshide)== false) && (validate_col_no_alert("defpass",formobj.defPass)== false) ) )
	{
		  /*alert("Default password is mandatory when you select the option 'Provide Create <%=LC.Pat_Patients%> Login button in Manage Logins'");*****/

		   alert("<%=MC.M_DefPwdMndt_CreatePat%>");
		      
		formobj.defPass.focus();
		return false;
	}

	//JM: 20Aug2007: added
 	 if(((formobj.selStudyIds.value != formobj.oldStudySpecific.value) && formobj.oldStudySpecific.value != '') || (formobj.oldStudySpecific.value != '' && (formobj.portalPatPop[1].checked == true || formobj.portalPatPop[2].checked == true || formobj.portalPatPop[3].checked == true))){

	/* alert("Please review the Form(s)/Calendar(s) selected in the 'Manage Portal >> Design Portal' module. " +	 "Please select Form(s)/Calendar(s) as per " +	 "the changed 'Portal Population' options "); *****/
	 
	   alert("<%=MC.M_RevFrmMng_CalChgOpt%>");

		 formobj.resetFlg.value= 1;
	 }


 	    //Portal Password Validation Check for INF-18669 : Raviesh
		var isSubmitFlag=$j("#isSubmitFlag").val();
		if(isSubmitFlag=="false"){
			alert("<%=MC.M_PlzEtr_validPass%>");
			formobj.defPass.focus();
			return false;
		}
		
	//if defpassword is entered, check its length

	if((! isEmpty(formobj.defPass.value)) && formobj.defPass.value.length < 8 )
	{
		/*alert("Password must be atleast 8 characters long. Please enter again.");*****/
		alert("<%=MC.M_PwdMust8CharLong_ReEtr%>");
		formobj.defPass.focus();
		return false;
	}



	if (!(validate_col('e-Sign',formobj.eSign))) return false;
		if(isNaN(formobj.eSign.value) == true) {
			
		/*alert("Incorrect e-Signature. Please enter again");*****/
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");
			formobj.eSign.focus();
			return false;
		}

}


      function openwin1(frm) {
		windowName = window.open("usersearchdetails.jsp?fname=&lname=&from="+ frm,"TEST","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
		windowName.focus();
	;}

    function openStudyWindow(formobj,formname) {
    selectStudy=formobj.selStudy.value;

    windowName=window.open("selectStudy.jsp?openerform="+formname+"&selectStudy="+selectStudy,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=350,height=300");
	windowName.focus();
    }


function openOrgLookup(formobj) {


	formobj.target="Lookup";
	formobj.method="post";

	formobj.action="multilookup.jsp?viewId=6014&form=portal&seperator=,"+
                  "&keyword=selOrg|site_name~selOrgIds|pk_site|[VELHIDE]";

	formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');
	if (formWin && !formWin.closed) formWin.focus();
	formobj.submit();
	formobj.target="_self";
	formobj.action="updateportal.jsp";
	void(0);

}

//JM: 08May07
function openLookup(formobj) {
	formobj.target="Lookup";
	formobj.method="post";

	formobj.dfilter.value = "portaldetails";

	formobj.action="multilookup.jsp?viewId=6017&form=portal&seperator=,"+
                  "&keyword=patientNames|PERSON_CODE~patientIds|LKP_PK|[VELHIDE]&filter=[VELGET:dfilter]";

	formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');
	if (formWin && !formWin.closed) formWin.focus();
	formobj.submit();
	formobj.target="_self";
	formobj.action="updateportal.jsp";
	void(0);
}

function openLookupStudy(formobj) {
	formobj.target="Lookup";
	formobj.method="post";
	formobj.action="multilookup.jsp?viewId=6013&form=portal&seperator=,"+
                  "&keyword=selStudy|STUDY_NUMBER~selStudyIds|LKP_PK|[VELHIDE]&maxselect=1";

	formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');


	if (formWin && !formWin.closed) formWin.focus();
	formobj.submit();
	formobj.target="_self";
	formobj.action="updateportal.jsp";
	void(0);
}


</SCRIPT>
<br>
<DIV class="formDefault" id="div1">
<%
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession)) {
		String uName = (String) tSession.getValue("userName");
        String mode = "";
		String accId = (String) tSession.getValue("accountId");
		String userIdFromSession = (String) tSession.getValue("userId"); //Amarnadh

		int pageRight = 0;
		int userId = 0; //Amarnadh
		GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("PORTALAD"));



		String objectIdComm = "";
		String objectIdStd = "";
		int resetFlag = 0;


		String creatorId ="";
		String shrdWithNames="";

		String portalId = "";
       	String portalName = "";
		String portalDesc ="";
       	String portalLevel = "";
		String portalStat ="";
		String notificationFlag="";
      	String firstname ="";
		String lastname="";

        String fullname="";
		mode = request.getParameter("mode");
        String histStatId = request.getParameter("histStatId");

		//get create auto login feature right
		String modRight = (String) tSession.getValue("modRight");

		int portalseq  = 0;
		char autloginright= '0';

		modCtlDao.getControlValues("module"); //get extra modules
		ArrayList modCtlDaofeature =  modCtlDao.getCValue();
		ArrayList modCtlDaoftrSeq = modCtlDao.getCSeq();

		portalseq = modCtlDaofeature.indexOf("MODPAUTOLOG");

		portalseq = ((Integer) modCtlDaoftrSeq.get(portalseq)).intValue();

		autloginright = modRight.charAt(portalseq - 1);



		    String stdNum ="";
            String siteName="";
            String patName ="";
            String stdobjIds="";
            String orgobjIds="";
            String patobjIds="";
           String defPass  = "";
           String createLoginsFlag = "";
           String createLoginsFlagChecked = "";
       // KN Defect 3073
          String portalStatus = request.getParameter("portalStat");
         CodeDao codeDao = new CodeDao();
		 int res = codeDao.getCodeId("portalstatus","W");
		 String codeDescription = codeDao.getCodeDescription();



    // Added By Amarnadh for Bugzilla issue #3033
	if(mode.equals("N")){
		userId = EJBUtil.stringToNum(userIdFromSession);
		userB.setUserId(userId);
		userB.getUserDetails();
		creatorId=userId+"";
		userB.setUserId(EJBUtil.stringToNum(""));
	   	fullname = userB.getUserFirstName() + " " + userB.getUserLastName();

	}
	if (mode.equals("M")) {

            portalId = request.getParameter("portalId");
	    	PortalDao portaldao =portalB.getPortalValue(EJBUtil.stringToNum(portalId));
	   		//portaldao.getPortalValue(EJBUtil.stringToNum(portalId));

            ArrayList createdBys = portaldao.getCreatedBy();

	    	ArrayList portalLevels = portaldao.getPortalLevels();
	    	ArrayList notificationFlags = portaldao.getNotificationFlags();
            ArrayList portalStats = portaldao.getPortalStats();
            ArrayList studyNumbers = portaldao.getStudyNumber();
            ArrayList siteNames = portaldao.getSiteName();
            ArrayList perCodes = portaldao.getPerCode();
            ArrayList objectIds = portaldao.getObjectId();

	    	ArrayList firstNames = portaldao.getFirstNames();
	    	ArrayList lastNames = portaldao.getLastNames();

	    	//populate the portal bean too

	    	portalB.setPortalId(EJBUtil.stringToNum(portalId));
		    portalB.getPortalDetails();
			portalName = portalB.getPortalName();
			portalDesc = portalB.getPortalDesc();

			if (StringUtil.isEmpty(portalDesc))
			{
				portalDesc = "";
			}

			defPass = portalB.getPortalDefaultPassword();

			if (StringUtil.isEmpty(defPass))
			{
				defPass = "";
			}
			createLoginsFlag = portalB.getPortalCreateLogins();

			if (StringUtil.isEmpty(createLoginsFlag))
			{
				createLoginsFlag = "0";
			}

	    	if (createLoginsFlag.equals("1"))
	    	{
	    		createLoginsFlagChecked = " CHECKED ";
	    	}
	    int len = objectIds.size();



	    creatorId = ((createdBys.get(0))==null)?"-":(createdBys.get(0)).toString();


	       portalStat = ((portalStats.get(0))==null)?"-":(portalStats.get(0)).toString();
	       notificationFlag = ((notificationFlags.get(0))==null)?"-":(notificationFlags.get(0)).toString();
	       portalLevel = ((portalLevels.get(0))==null)?"-":(portalLevels.get(0)).toString();

	      firstname = ((firstNames.get(0))==null)?"":(firstNames.get(0)).toString();
          lastname = ((lastNames.get(0))==null)?"":(lastNames.get(0)).toString();
          fullname = firstname +" "+lastname;
          int i=0;

	     if(len >0) {
	           if(portalLevel.equals("S")){
               for(i=0;i<len;i++) {
	              objectIdStd = ((objectIds.get(i))==null)?"":(objectIds.get(i)).toString();
                  stdobjIds = stdobjIds + (objectIds.get(i).toString()) +",";
                  stdNum = stdNum + (studyNumbers.get(i).toString()) + ",";
               }
			   // commented by Amarnadh for defect #3173 8thFeb'08
		//   if (stdobjIds.length() > 0){
		   stdobjIds = stdobjIds.substring(0,stdobjIds.length()-1);
		   stdNum = stdNum.substring(0,stdNum.length()-1);
            }
            if(portalLevel.equals("O")){
               for(i=0;i<len;i++) {
	              objectIdComm = ((objectIds.get(i))==null)?"":(objectIds.get(i)).toString();
                  orgobjIds = orgobjIds + (objectIds.get(i).toString()) +",";
                  siteName = siteName + (siteNames.get(i).toString()) + ",";

               }
                  orgobjIds = orgobjIds.substring(0,orgobjIds.length()-1);
	              siteName = siteName.substring(0,siteName.length()-1);

            }

	   if(portalLevel.equals("P")){
               for(i=0;i<len;i++) {
	             objectIdComm = ((objectIds.get(i))==null)?"":(objectIds.get(i)).toString();
                  patobjIds = patobjIds + (objectIds.get(i).toString()) +",";
                  patName = patName + (perCodes.get(i).toString()) + ",";
           }

           if (patobjIds.length() > 0){
	   //objectIdComm = ((objectIds.get(i))==null)?"":(objectIds.get(i)).toString();
            patobjIds = patobjIds.substring(0,patobjIds.length()-1);
	      	patName = patName.substring(0,patName.length()-1);
				}
	      //    }
			}
		}
	}

	//JM: check for maximum number of portal and if exhausted...
	int existingPortals = 0;
	int totalPortalsAllowed = 0;

	if  (mode.equals("N"))
	{
		 existingPortals  = accountB.getPortalsCount(EJBUtil.stringToNum(accId));
		 totalPortalsAllowed = EJBUtil.stringToNum((String) tSession.getValue("totalPortalsAllowed"));
	}


	if ( (mode.equals("N"))&& totalPortalsAllowed > 0 && totalPortalsAllowed <= existingPortals) {

%>

<table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl">
	<tr>
		<td	align=center>
			<p class = "sectionHeadings">
				<%--The number of portals you can create in this account has exhausted. Please contact the Velos Administrator.*****--%> <%=MC.M_NumPortals_VelosAdmin%>
			</p>
		</td>
	</tr>
	<tr	height=20></tr>
	<tr>
		<td	align=center>
			<button onclick="window.history.back();"><%=LC.L_Back%></button>
		</td>
	</tr>
</table>
<%}
  else{ %>
<!--Added By Amarnadh for Bugzilla issue #3030 -->
<% if(mode.equals("M")) { %>

	<%} else { %>

    <% } %>
	<jsp:include page="accounttabs.jsp" flush="true">
	<jsp:param name="selectedTab" value="6"/>
	</jsp:include>

<!-- Bug#15367 Fix for 1360*768 resolution- Tarun Kumar -->	
<SCRIPT LANGUAGE="JavaScript">
if (navigator.userAgent.indexOf("MSIE") == -1){ 
	if(screenWidth>1280 || screenHeight>1024)
		document.write('<div style="height: 62%; overflow:auto;">');
}else
	{
	if(screenWidth>1280 || screenHeight>1024)
		document.write('<div style="height: 90%; overflow:auto;">');
	}
</SCRIPT>
	<Form name="portal" id="portal_form" method="post" action="updateportal.jsp" onSubmit ="if (validate(document.portal)==false) {setValidateFlag('false');return false;} else {setValidateFlag('true'); return true;}">
	      <input type="hidden" name="mode" value=<%=mode%>>
	      <input type="hidden" name="dfilter" value="">
	      <input type="hidden" name="popLevel" value=<%=portalLevel%>>
	      <input type="hidden" name="portalId" id="portalId" value=<%=portalId%>>
          <input type="hidden" name="selStudyIds" value="<%=stdobjIds%>">
	      <input type="hidden" name="patmodFlag" value ="N">

          <input type="hidden" name="oldStudyId" value="<%=objectIdComm%>">
          <input type="hidden" name="oldStudySpecific" value="<%=objectIdStd%>">
          <input type="hidden" name="resetFlg" value="<%=resetFlag%>">
     	  <input type="hidden" name="selOrgIds" value="<%=orgobjIds%>">
          <input type="hidden" name="histStatId" value ="<%=histStatId%>">
	      <table width="90%" cellspacing="0" cellpadding="0" border="0">
			<table width="100%" cellspacing="1" cellpadding="1" class="basetbl" border="0">
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr> 

   
			<tr>
				<td width="17%"> <%--Portal Name*****--%><%=LC.L_Portal_Name%> <FONT class="Mandatory">* </FONT> </td>
				<td  colspan="6">
					<input type="text" name="portalName" size =35 maxlength="250" value="<%=portalName%>">
				</td>
			</tr>
			<tr>
		        <td width="17%"> <%--Created By*****--%><%=LC.L_Created_By%>  </td>
			<input type="hidden" name="creatorId" value=<%=creatorId%>>
			<td  colspan="6"> <input type="text" name="createdBy" readonly value="<%=fullname%>">&nbsp;&nbsp;&nbsp;
			<A HREF=# onClick=openwin1('portalstudy') ><%--Select User*****--%><%=LC.L_Select_User%></A></td>
			</tr>

			<tr>
				<td width="17%" > <%--Description*****--%><%=LC.L_Description%> </td>
				<td  colspan="6"  class="textareaheight"><textarea  name="portalDesc" rows="3" cols="50" value="" maxlength = "4000"><%=portalDesc%></textarea>

		        	</td>
			</tr>
	  		<tr>
	  		<td  colspan="6"><%--Portal Population*****--%><%=LC.L_Portal_Population%></td>
	  		</tr>
	  	<tr>
		       <% if(mode.equals("N") || portalStatus.equals(codeDescription)) {%>
			<% if(portalLevel.equals("S")) {%>
				<td width="20%"><br><input type="radio" name="portalPatPop" value="S" checked>&nbsp;&nbsp; <%--All <%=LC.Pat_Patients%> on a <%=LC.Std_Study%>*****--%>				
				<%=MC.M_AllPat_OnStd%></td>
				<% }
				else { %>
				<td width="20%"><br><input type="radio" name="portalPatPop" value="S"  checked>&nbsp;&nbsp;  
				<%--All <%=LC.Pat_Patients%> on a <%=LC.Std_Study%>*****--%>
				    <%=MC.M_AllPat_OnStd%></td>
				<%}%>
			<% }
				else { %>
				<% if(portalLevel.equals("S")) {%>
				<td width="20%"><br><input type="radio" name="portalPatPop" disabled value="S" checked>&nbsp;&nbsp; <%--All <%=LC.Pat_Patients%> on a <%=LC.Std_Study%>*****--%>
				    <%=MC.M_AllPat_OnStd%></td>
				<% }
				else { %>
				<td width="20%"><br><input type="radio" name="portalPatPop" disabled value="S" >&nbsp;&nbsp; 
				<%--All <%=LC.Pat_Patients%> on a <%=LC.Std_Study%>*****--%><%=MC.M_AllPat_OnStd%>
				<%}%>
				<%}%>
				<!--Modified By Amarnadh for Issue #3173 17/12-->
				<% if(mode.equals("N") || portalStatus.equals(codeDescription)) {%></td>
				<td width="30%"><br><input type="text" name="selStudy"  size=35 maxlength=40 value="<%=stdNum%>" READONLY>&nbsp;&nbsp;&nbsp;&nbsp;
				<A href="javascript:void(0);" onClick ="return openLookupStudy(document.portal)">
				<%--Select <%=LC.Std_Study%>*****--%>
				    <%=LC.L_Select_Study%></A></td>
				<% } else { %>
				<td width="30%"><br><input type="text" name="selStudy"  size=35 maxlength=40 value="<%=stdNum%>" READONLY></td>
				<%}%>

	  		</tr>
	  		<tr>
	  		<td width="12%">&nbsp;</td>
			<%if(mode.equals("N") || portalStatus.equals(codeDescription)) {%>
	  		<td width="10%" colspan="6"><p class="defComments">[<%--Please select one <%=LC.Std_Study%> only*****--%> <%=MC.M_Selc_OneStdOnly%> ]</p></td>
			<%}%>
	  		</tr>
			<tr>
	  		   <% if(mode.equals("N") || portalStatus.equals(codeDescription)) {%>
				<% if(portalLevel.equals("O")){%>
				<td width="20%"><br><input type="radio" name="portalPatPop" value="O"  checked>&nbsp;&nbsp;<%-- All <%=LC.Pat_Patients%> in an Organization 
				*****--%><%=MC.M_AllPat_InOrg%></td>

				<% } else { %>
				<td width="20%"><br><input type="radio" name="portalPatPop" value="O" >&nbsp;&nbsp; 
				     <%-- All <%=LC.Pat_Patients%> in an Organization 
				*****--%><%=MC.M_AllPat_InOrg%></td>
				
				<%}

				 }
				else { %>
				<% if(portalLevel.equals("O")) {%>

				<td width="20%"><br><input type="radio" disabled  name="portalPatPop" value="O"  checked>&nbsp;&nbsp;<%-- All <%=LC.Pat_Patients%> in an Organization 
				*****--%><%=MC.M_AllPat_InOrg%></td>

				<% } else { %>
				<td width="20%"><br><input type="radio"  disabled  name="portalPatPop" value="O">&nbsp;&nbsp; <%-- All <%=LC.Pat_Patients%> in an Organization 
				*****--%><%=MC.M_AllPat_InOrg%></td>
				<%}
				}%>

				<% if(mode.equals("N") || portalStatus.equals(codeDescription)) {%>
				<td width="30%"><br><input type="text" name="selOrg"  size=35 maxlength=40 value="<%=siteName%>" readOnly=true>&nbsp;&nbsp;&nbsp;&nbsp;
				<A href="#" onClick="openOrgLookup(document.portal)">
				<%--Select Organization*****--%> <%=LC.L_Select_Org%> </A></td><% } else { %>
				<td width="30%"><br><input type="text" name="selOrg"  size=35 maxlength=40 value="<%=siteName%>" readOnly=true>&nbsp;&nbsp;&nbsp;&nbsp
				<%} %></td>

	  		</tr>
			<tr>
	  			<% if(mode.equals("N") || portalStatus.equals(codeDescription)) {%>
				<% if(portalLevel.equals("A")) {%>

				<td width="20%"><br><input type="radio" name="portalPatPop" value="A" checked>&nbsp;&nbsp;<%--All <%=LC.Pat_Patients%> in the Account*****--%><%=MC.M_AllPat_InAcc%></td>
				<% } else { %>
				<td width="20%"><br><input type="radio" name="portalPatPop" value="A" >&nbsp;&nbsp; 
				   <%--All <%=LC.Pat_Patients%> in the Account*****--%><%=MC.M_AllPat_InAcc%></td>
				
				<%}
				}
				else {
				 if(portalLevel.equals("A")) {%>

				<td width="20%"><br><input type="radio" disabled name="portalPatPop" value="A" checked>&nbsp;&nbsp; 
				
				<%--All <%=LC.Pat_Patients%> in the Account*****--%><%=MC.M_AllPat_InAcc%></td>
				<% } else { %>
				<td width="20%"><br><input type="radio" disabled name="portalPatPop" value="A" >&nbsp;&nbsp; <%--All <%=LC.Pat_Patients%> in the Account*****--%><%=MC.M_AllPat_InAcc%></td>
				<%}
				}%>

				<td width="30%">&nbsp;</td>
				<td width="20%">&nbsp;</td>
	  		</tr>
			<tr>
	  			<% if(mode.equals("N") || portalStatus.equals(codeDescription)) {%>
				<% if(portalLevel.equals("P")) {%>
				<td width="20%"><br><input type="radio" name="portalPatPop" value="P" checked>&nbsp;&nbsp;<%-- Specific <%=LC.Pat_Patients%> Only*****--%>
				<%=LC.L_Specific_PatsOnly%></td>
				<% } else { %>
				<td width="20%"><br><input type="radio" name="portalPatPop" value="P" >&nbsp;&nbsp;      
				<%-- Specific <%=LC.Pat_Patients%> Only*****--%>
				<%=LC.L_Specific_PatsOnly%></td>
				<%}} else {%>
				<% if(portalLevel.equals("P")) {%>
				<td width="20%"><br><input type="radio" disabled name="portalPatPop" value="P" checked>&nbsp;&nbsp;<%-- Specific <%=LC.Pat_Patients%> Only*****--%>
				<%=LC.L_Specific_PatsOnly%></td>
				<% } else { %>
				<td width="20%"><br><input type="radio" disabled name="portalPatPop" value="P" >&nbsp;&nbsp; <%-- Specific <%=LC.Pat_Patients%> Only*****--%>
				<%=LC.L_Specific_PatsOnly%></td>
				<%}}%>

				<!-- <td width="20%"><br>
				<input type="text" name="selPat"  size=35 maxlength=40 value="<%=patName%>" readOnly>&nbsp;&nbsp;&nbsp;&nbsp;
				<A href="#" onClick="openPatientWindow(document.portal,'portal');" >Select <%=LC.Pat_Patient%></A></td>
				<td width="12%">&nbsp;</td>
				-->
				<% if(mode.equals("N") || portalStatus.equals(codeDescription)) {%>
				<td><br>
				<Input TYPE="text" NAME="patientNames" SIZE="35" value="<%=patName%>" READONLY>&nbsp;&nbsp;&nbsp;&nbsp;
				<A href="javascript:void(0);" onClick="return openLookup(document.portal)">
				<%-- Select <%=LC.Pat_Patient%>*****--%>
				<%=LC.L_Select_Pat%></A>
				</td>
				<% } else { %>
				<td><br>
				<Input TYPE="text" NAME="patientNames" SIZE="35" value="<%=patName%>" READONLY>&nbsp;&nbsp;&nbsp;&nbsp
				 </A>
				</td>
				<%}%>
				<%if (mode.equals("M")) { %>
				<Input TYPE="hidden" NAME="patientIds" value="<%=patobjIds%>">
				<%}else{ %>
				<Input TYPE="hidden" NAME="patientIds">
				<%} %>

	  		</tr>
	  		<tr>
	  			<td colspan="6"><br><%--<%=LC.Pat_Patient%> Login Settings*****--%>
				<%=LC.L_Pat_LoginSettings%><br><br></td>

	  		</tr>
	  		<tr>
	  			<td width="17%"><%-- Default Password*****--%><%=LC.L_Def_Pwd%></td>
				<td colspan="6">
					<input type="password" name="defPass" id="defPass" class="passwordCheck" size =35 maxlength="50"> <%if (defPass.length()>0){ %><DIV><font size=1 color="red">
					(<%--Default Password is already set. Enter another password to change*****--%>
					 <%=MC.M_DefPwdAldySet_EtrOtherPwd%>)</font></DIV> <%}%>
					 <input type="hidden" name="defPasshide" size =35 maxlength="50" value=<%=defPass%>>
				</td>
			</tr>
			<tr>
				<td>
					<A HREF="pwdtips.htm" target="_new"><%=MC.M_SelcPwdCrt%></A>
				</td>
			</tr>

		<% if (autloginright == '1') { %>
			<tr>
				<td colspan="6">
					<input type="checkbox" name="createLoginsFlag"  value="1" <%=createLoginsFlagChecked%> >&nbsp;&nbsp; 
					 <%--Provide 'Create <%=LC.Pat_Patients%> Login' button in 'Manage Logins'*****--%>  <%=MC.M_CreatePatLogin_Btn%>
					<p class="defComments">[<%--Please note: 'New Login Information' notifications will not be sent when <%=LC.Pat_Patient_Lower%> logins are created using 'Create Logins Button'*****--%><%=MC.M_NewLoginInfo_PatLogin%>]</p>
				</td>

			</tr>
		<% } else
		{
			%>

				 <input type="hidden" name="createLoginsFlag"  value="0" />
			<%

		}%>

			<tr>
			    <%
			    if(notificationFlag.equals("1")) { %>
				<td width="16%" colspan=3><br><br><input type="checkbox" name="emailNotify"  checked>&nbsp;&nbsp;    <%--Send system-generated email notification to <%=LC.Pat_Patient_Lower%> with login information*****--%> <%=MC.M_SendSysNotfic_ToPat%> </td>
				<%} else {%>
				<td width="16%" colspan=3><br><br><input type="checkbox" name="emailNotify" >&nbsp;&nbsp; <%--Send system-generated email notification to <%=LC.Pat_Patient_Lower%> with login information*****--%> <%=MC.M_SendSysNotfic_ToPat%></td>
				<%
				}
			  %>

			</tr>


		<br>

		</table>

    <br>

	 <% if ((mode.equals("N")&& (pageRight==5 || pageRight==7)) || (mode.equals("M")&& pageRight>=6)) {%>

	 <jsp:include page="submitBar.jsp" flush="true">
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="portal_form"/>
			<jsp:param name="showDiscard" value="N"/>
	 </jsp:include>

	<%} %>
	
	<input type="hidden" id="isSubmitFlag" value="" />
	
  </Form>
</div>
<%

	}// else part, number of portal
}//end of if body for session
	else {
%>
  <jsp:include page="timeout.html" flush="true"/>
<% } %>

<div class = "myHomebottomPanel">
	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</div>

<div class ="mainMenu" id="emenu">
	<jsp:include page="getmenu.jsp" flush="true"/>
</div>

</body>

</html>



