<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>Velos eResearch</title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<jsp:useBean id="homepage" class="com.velos.eres.service.util.SearchHomepageXml" scope="session" />
<% homepage.setRoot("d:\\eres\\conf\\HomePage.xml"); %>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<jsp:include page="homepanel.jsp" flush="true"/>   

<body id = "home">
<SCRIPT LANGUAGE = "JavaScript">
		// preload images
		var img_velocity = new Image(326,203);
		var img_vision = new Image(326,203);
		var img_value = new Image(326,203);
		img_velocity.src = "../images/Velos-velocity.jpg";
		img_vision.src = "../images/Velos-vision.jpg";
		img_value.src = "../images/Velos-value.jpg";
</SCRIPT>

<DIV class="homeCenter" id="div1" style="border-style:none;height:400;">
<layer class = "cw"  src="http://www.centerwatch.com/bin/cl.pl?p=drugs/druglist.htm">
</layer>
 <nolayer>
<iframe class = "cw" src="http://www.centerwatch.com/bin/cl.pl?p=drugs/druglist.htm">
</iframe>
</nolayer> 
<div style="position:absolute; margin-top:300;">
<jsp:include page="bottompanel.jsp" flush="true"> 
</jsp:include>   
</div>

</div>


<form method="post" action="login.jsp">
 <div class=login id="emenu"> 
    <table class=tableDefault width="130" cellpadding="0" cellspacing="0">
	<tr> 
        <td width="109"></td>
    	<td width="21" halign = right><img src="../images/jpg/top_login.jpg"></td>
		</tr>
	</table>

    <table width="130" border="0" cellpadding="0" cellspacing="0" >
	  <tr > 
        <td width="5"></td>
        <td width="125">User 
          Name</td>
      </tr>
      <tr > 
        <td width="5"></td>
        <td width="125"> 
          <input type="text" name="username" size = 13 MAXLENGTH = 20>
        </td>
      </tr>
	 <tr height = 5> 
        <td width="5"></td>
        <td width="125"  align="center"> 
        </td>
      </tr>
      <tr > 
        <td width="5"></td>
        <td width="125" >Password</td>
      </tr>
      <tr > 
        <td width="5"></td>
        <td width="125" > 
          <input type="password" name="password" size = 13 MAXLENGTH = 20>
        </td>
      </tr>
      <tr height = 5> 
        <td width="5"></td>
        <td width="125" > 
        </td>
      </tr>
      <tr > 
        <td width="5"></td>
        <td width="125" align="center"> 
          <input type="submit" name="submit" value = "Login">
	    </td>
      </tr>
     <tr height = 5> 
        <td width="5"></td>
        <td width="125"  align="center"> 
        </td>
      </tr>
	<tr > 
	<td width="5"></td>
        <td width="125"> <font size=1>New User?</font> <A href="register.jsp">Sign up! </a> </td>
      </tr>
      <tr > 
	<td width="5"></td>
        <td width="125"> <A href="forgotpwd.jsp?mode=N">  
         Forgot Password? </a> </td>
      </tr>
    </table>

    <table width="130" cellpadding="0" cellspacing="0" >
	<tr> 
        <td width="109"></td>
    	<td width="21" valign = absbottom  align="center"><img src="../images/jpg/bottom_login.jpg"></td>
		</tr>
	</table>
</div>
<p>&nbsp;</p></body>


</div>
</form>

<div class="section" id=secdiv style="overflow:none;border-style:none">
<% {
    String sectionsList[] = homepage.listOfSections();
    int noOfSections = sectionsList.length;
    int j = 0;
    int counter = 0;
    for(int cnt =0;cnt<noOfSections;j++)
	{//for loop begins
	 if (sectionsList[j] == null)
	 {
	   break;
	 }
		counter++;
	}

    for(j =0;j<counter;j++)
	{//for loop begins
		
	 if (sectionsList[j] == null)
	 {
	   break;
	 }
%>
	<table width="200" cellspacing="0" cellpadding="0">
  <tr >
	<% if ( j == 0 ){%>
	      <td width="12" valign="bottom" ><img src="../images/jpg/tab_top_left_curve.jpg" width="12" height="10"></td>
	<% }else { 

	%>	
	<td width="12" align = right valign="top" ><img src="../images/jpg/vline.jpg" width="3" height="20" ></td>
	<%
	}
	%>	
    <td width="183" height="10">&nbsp;</td>
  </tr>
	
  <% Vector linksList = homepage.listOfLinks(sectionsList[j]);
	   int size = linksList.size();
		int pictHeight  = 0 , nameLen = 0, intFactor = 0;
		float multiplyFactor  = 0;
      	   for(int i =0;i<size;i++)
		   {	
			  /*String name = (String)((Hashtable)linksList.elementAt(i)).get("Name");
			  nameLen = name.length();
			  intFactor = nameLen/30;	
			  multiplyFactor = (float) nameLen/30;	
			  out.println(nameLen);	
			  out.println(intFactor );	
			  out.println(multiplyFactor);	

			  if ( intFactor == 0)
				{
				  pictHeight = 20;
				  	
				}
				else if( intFactor >= multiplyFactor )
				{ 
				 pictHeight = intFactor * 20;
				}else 
				{
					pictHeight = (intFactor + 1) * 20;	
				} */
//
 %>
			
			<tr height="10">
		      
			<td width="6" align = right valign="top" background = "../images/jpg/tabvline.jpg"></td>

			<td width="183">
		
			<%
		      	String type = (String)((Hashtable)linksList.elementAt(i)).get("Type");
				if(type.equals("email"))
				{
			%>
				<A HREF="mailto:<%=((Hashtable)linksList.elementAt(i)).get("Link") %>">
				   <%=((Hashtable)linksList.elementAt(i)).get("Name")%>
				</A>
			<%
		  		}
				if (type.equals("Link"))
				{
			%>
		  	<A HREF="http://<%=((Hashtable)linksList.elementAt(i)).get("Link") %>" target="div1"> 
				<%=((Hashtable)linksList.elementAt(i)).get("Name")%> 
			</A>							
	  
			    <% } %>
		   </td></tr>
			<% } %>
	<tr height="10">
      	<td width="6" align = right valign="top" background = "../images/jpg/tabvline.jpg"></td>
	    <td width="183"></td>
	  </tr>

	</table>
	<div style = "margin-left:8;position:absolute; width:100; margin-top:-5">
	<table width="150" border="0" cellspacing="0" cellpadding="0">
	  <tr >
	    <%if (j == counter-1){%>	
		<td  valign="bottom" ><img src="../images/jpg/tab_left_last_bottom.jpg"></td>
	    <%}else{ %>	
	      <td  valign="bottom" ><img src="../images/jpg/tab_left_bottom.jpg"></td>
	   <%}%>	
	    <td width="207" valign="bottom"><img src="../images/jpg/tabline.jpg" width="207" height="3"></td>
	  </tr>
	</table>
	<div style = "margin-left:205;position:absolute; margin-top:-3">
	<table width="15" border="0" cellspacing="0" cellpadding="0">
	  <tr >
	    <td  valign="bottom" ><img src="../images/jpg/tab_bottom_right_curve.jpg"></td>
	  </tr>
	</table>
	</div>
	</div>
	<%
	}
	}

	%>


</html>
