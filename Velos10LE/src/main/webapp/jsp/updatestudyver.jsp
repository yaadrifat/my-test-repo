<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
  <jsp:useBean id="studyVerB" scope="request" class="com.velos.eres.web.studyVer.StudyVerJB"/>
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>
  <%

	String src = null;
	src = request.getParameter("srcmenu");
	String eSign = request.getParameter("eSign");	
	String studyVerId = request.getParameter("studyVerId");
	String studyId = request.getParameter("studyId");
	String mode = request.getParameter("mode");
	String verNumber = request.getParameter("verNumber");
	if (verNumber != null) { verNumber = verNumber.trim(); }
	    
	String verNotes = request.getParameter("verNotes");
	String versionDate = request.getParameter("versionDate");//JM
	if (versionDate==null) versionDate="";
	String studyVercat = request.getParameter("dStudyvercat");
	String studyVertype = request.getParameter("dStudyvertype");
HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))

   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/> 

<%   
   
   	String oldESign = (String) tSession.getValue("eSign");
	String eventId = (String) tSession.getValue("eventId");
	
	if(!oldESign.equals(eSign)) 
	{
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} 
	else 
  	{
	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");
	String enrollId =(String) tSession.getValue("enrollId");
%>

<%	int ret = 0;
	if(mode.equals("M")) 
	{
		studyVerB.setStudyVerId(EJBUtil.stringToNum(studyVerId));
		studyVerB.getStudyVerDetails();	
		
		studyVerB.setStudyVerNumber(verNumber);
		studyVerB.setStudyVerNotes(verNotes);
		studyVerB.setIpAdd(ipAdd);
		
		studyVerB.setVersionDate(versionDate);		
		studyVerB.setStudyVercat(studyVercat);	
		studyVerB.setStudyVertype(studyVertype);
		studyVerB.setModifiedBy(usr);		
		
		ret = studyVerB.updateStudyVer();
	}
	else if(mode.equals("N")){
		
	 
//JM: 11/16/2005: Modified: version date, categofy and type parameter passed

		//ret =  studyVerB.newStudyVersion(EJBUtil.stringToNum(studyId), verNumber, verNotes,EJBUtil.stringToNum(usr));
	    ret =  studyVerB.newStudyVersion(EJBUtil.stringToNum(studyId), verNumber, verNotes,versionDate,studyVercat,studyVertype, EJBUtil.stringToNum(usr));
	
	}
	%>
	  <br>
      <br>
      <br>
      <br>
      <br>
	 <%
		if (ret == -3) { //duplicate version number
      %>
	   
       <table>
       <tr>
       <td>
         <P class="successfulmsg" align = "center"><%=MC.M_VerNum_AldyExst%><%--The Version Number-Category combination you have entered already exists. Please click on Back button and enter it again.*****--%> </P>
       </td>
       </tr>
        <tr height=20></tr>
       <tr>
        <td align=center>
      		<button onclick="window.history.back(); return false;"><%=LC.L_Back%></button>
       </td>		
       </tr>
       </table>		
      <%  
        }else {
	  %>
     <p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
	  <script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
	  </script>	  
	<% }
//	}
	
%>


<%
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





