<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Space_Break%><%--Space Break*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>



<SCRIPT Language="javascript">

 function  validate(formobj)
 {
 
 	if (   !(validate_col ('  Section', formobj.section)  )  )  return false
	if (!(validate_col('Sequence',formobj.sequence))) return false		

 	if (   !(validate_col ('Number of Spaces', formobj.fldLength)  )  )  return false
 	if (formobj.fldLength.value>100)
 	{
 		alert("<%=MC.M_SpacePercCntGtr100_ReEtr%>");/*alert("Space Percentage cannot be greater than 100.Please enter again");*****/	
 	  formobj.fldLength.focus();
 	  return false;
 	}	
 	if(formobj.codeStatus.value != "WIP"){	
 	if (  !(validate_col('e-Signature',formobj.eSign)     )   ) return false
	 
	if(isNaN(formobj.eSign.value) == true) 
		{
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		 return false;
   		}
 	}	
	
	if(isNaN(formobj.sequence.value) == true) 
	{
		alert("<%=MC.M_SecSeq_EtrValidNum%>");/*alert("Section Sequence has to be a Number. Please enter a valid Number");*****/
		formobj.sequence.focus();
		 return false;
   	}
	if(isNaN(formobj.fldLength.value) == true) 
	{
		alert("<%=MC.M_SpacePerc_EtrValid%>");/*alert("Space Percentage has to be a Number. Please enter a valid Number");*****/
		formobj.fldLength.focus();
		return false;
   	}
	
	
  }
  
 
 


</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="fieldLibJB" scope="request"  class="com.velos.eres.web.fieldLib.FieldLibJB"/>
<jsp:useBean id="formSecJB" scope="request"  class="com.velos.eres.web.formSec.FormSecJB"/>
<jsp:useBean id="formFieldJB" scope="request"  class="com.velos.eres.web.formField.FormFieldJB"/>
<jsp:include page="include.jsp" flush="true"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*"%>

<body>
<DIV class="popDefault" id="div1"> 
 <%

	HttpSession tSession = request.getSession(true);
	int pageRight = 0;	

   if (sessionmaint.isValidSession(tSession))
   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	 GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	 String calledFrom=request.getParameter("calledFrom");
	 String codeStatus=request.getParameter("codeStatus");
	 System.out.println("Code Status - > " + codeStatus);
	 String lnkFrom=request.getParameter("lnkFrom");
	 if(lnkFrom==null) 
		{lnkFrom="-";}
	 
	 String stdId=request.getParameter("studyId");
	 int studyId= EJBUtil.stringToNum(stdId);			

		String userIdFromSession = (String) tSession.getValue("userId");
		
		if( calledFrom.equals("A")){ //from account form management
	 	    if (lnkFrom.equals("S")) 
	 	      {   
		 		 ArrayList tId ;
				 tId = new ArrayList();
				 if(studyId >0)
				 {
				 	TeamDao teamDao = new TeamDao();
					teamDao.getTeamRights(studyId,EJBUtil.stringToNum(userIdFromSession));
					tId = teamDao.getTeamIds();
					if (tId.size() <=0)
					 {
						pageRight  = 0;
					  } else 
					  {
					  	StudyRightsJB stdRights = new StudyRightsJB();
						stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));
						
							ArrayList teamRights ;
						teamRights  = new ArrayList();
						teamRights = teamDao.getTeamRights();
							 
						stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
						stdRights.loadStudyRights();
					 
						 		
						if ((stdRights.getFtrRights().size()) == 0)
						{					
						  pageRight= 0;		
						} else 
						{								
							pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
						}
					  }
			    }
			 }	
    	 	else
    	    {		 
    	  		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMS"));
    		 }
	  	} 
		
	 	if( calledFrom.equals("L")) //form library
	 	{
			pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFRMLIB"));
	 	}
		
		if( calledFrom.equals("St")) //study
	 	{
			StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");	
	  		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
		}
		 

	if (pageRight >= 4)
		
	{
	
		String mode=request.getParameter("mode");
		
		String fieldLibId="";
		String fldLength="";
		
		// THE STYLE OBJECT WHICH TAKES VALUES OF ATTRIBUTES OF THE FIELD  
		// LIKE BOLD, SAMELINE, ALIGN , ITALICS
		Style aStyle= new Style();
		String formId="";
		String formSecId="";
		String fldSeq="";
		
		String samLinePrevFld="";
		String formFldId="";
		String fldType="";
		String fldIsVisible="";
		String offlineFlag = "0";
		formId=request.getParameter("formId");	
		
		String accountId=(String)tSession.getAttribute("accountId");
		
		int tempAccountId=EJBUtil.stringToNum(accountId);
		

		
		ArrayList idSec= new ArrayList();
		ArrayList descSec= new ArrayList();
				
		String pullDownSection;
		FormSecDao formSecDao= new FormSecDao();
		formSecDao = formSecJB.getAllSectionsOfAForm( EJBUtil.stringToNum(formId));
		idSec= formSecDao.getFormSecId();
		descSec=formSecDao.getFormSecName();
		
		pullDownSection=EJBUtil.createPullDown("section", 0, idSec, descSec);
						
		if (mode.equals("M"))
		{
			
				formId=request.getParameter("formId");		
				formFldId=request.getParameter("formFldId");
				
				formFieldJB.setFormFieldId(Integer.parseInt(formFldId) );
				formFieldJB.getFormFieldDetails();
				fieldLibId=formFieldJB.getFieldId();
				offlineFlag = formFieldJB.getOfflineFlag();
				
				
				fieldLibJB.setFieldLibId(Integer.parseInt(fieldLibId));
				fieldLibJB.getFieldLibDetails();
			
				formSecId=formFieldJB.getFormSecId();  
				fldIsVisible = fieldLibJB.getFldIsVisible();
				fldLength = fieldLibJB.getFldLength();
				
				//TO GIVE A SINGLE SECTION IN EDIT MODE
							
				int secId =EJBUtil.stringToNum(formSecId) ;
				
				formSecJB.setFormSecId(secId);
				formSecJB.getFormSecDetails();	
				
				String sectionName = "";
				sectionName = formSecJB.getFormSecName();
							
				
				// TO MAKE A READONLY BOX FOR NOT LETTING THE EDITING OF THE SECTION FOR FIELD IN EDIT MODE 
				
				pullDownSection = "<label>"+sectionName +"</label>" 
				 							+ "<Input type=\"hidden\" name=\"section\" value="+ formSecId +" >  " ;
								  
				fldSeq=((formFieldJB.getFormFldSeq()) == null)?"":(formFieldJB.getFormFldSeq()) ;
				
						
				//  WHILE GETTING THE STYLE WE CHECK ITS VALUES TO DISPLAY APPROPRIATELY
				aStyle=fieldLibJB.getAStyle( ); 
				if ( aStyle != null )  
				{
					samLinePrevFld = (  (   aStyle.getSameLine()   ) == null)?"-1":(   aStyle.getSameLine()   ) ;	
				}
						
			
				
				fldType=fieldLibJB.getFldType();
				fldLength = ((fieldLibJB.getFldLength()) == null)?"":(fieldLibJB.getFldLength()) ;		
				
		}
		
		// #5437,5438 02/02/2011 @Ankit
		if(((pageRight == 6 && mode.equals("M") ) || (pageRight == 5 && mode.equals("N")) || pageRight == 7) && (mode.equals("M") && (!codeStatus.equals("Lockdown") && !codeStatus.equals("Deactivated") && !codeStatus.equals("Freeze") && !codeStatus.equals("Active")) )|| mode.equals("N")){%>
		<Form name="spaceType" id="spaceTypeId" method="post" action="spaceTypeSubmit.jsp" onsubmit="if (validate(document.spaceType)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
			<%}else{%>
		<Form name="spaceType" method="post" onsubmit="return false">
			<%}%>

	<table width="60%" cellspacing="0" cellpadding="0" border="0">
  	<tr>
		<td ><P class="sectionHeadings"> <%=MC.M_FldTyp_SpcBreak_Upper%><%--FIELD TYPE:  SPACE BREAK*****--%> </P> </td>
		
	 </tr>   
	 </table>
	 <br>
 
	
	<Input type="hidden" name="mode" value=<%=mode%> >
	<Input type="hidden" name="formId" value=<%=formId%> >
	<Input type="hidden" name="codeStatus" value=<%=codeStatus%> >  
	<Input type="hidden" name="formFldId" value=<%=formFldId%> >  
	<table width="80%" cellspacing="0" cellpadding="0" border="0"  >
		<tr> 
      <td width="20%"><%=LC.L_Section%><%--Section*****--%><FONT class="Mandatory">* </FONT></td>
        <td width="60%"><%=pullDownSection%> </td>
	  </tr>
	<tr> 
      <td width="20%"><%=LC.L_Sequence%><%--Sequence*****--%><FONT class="Mandatory">* </FONT></td>
        <td width="60%"><input type="text" name="sequence" size = 10 MAXLENGTH = 10 value="<%=fldSeq%>"> </td>
		</tr>
			<tr ><td ><label><%=LC.L_Space_Percentage%><%--Space Percentage*****--%><FONT class="Mandatory">* </FONT></td></label></td>
	  		<td > <Input name="fldLength"  size = 3 type="text" value=<%=fldLength%> >% </td></tr>
		 	
		</table>
		<br>
		<table  width="48%" cellspacing="0" cellpadding="0" border="0" >
		<%//Commented as per UCSF requirements*/%>
	<%//Now uncommented as per 19th April enhancements of %>
	 <tr height"5"><td> &nbsp; </td></tr> 
		<%if (  ( samLinePrevFld.equals("-1")  ) || ( samLinePrevFld.equals("0" ) ) ||  (samLinePrevFld.equals("") ) )
		   {%>
			<tr><td width="25%">	<input type="checkbox" name="sameLine" value="1" ><%=MC.M_SameLine_AsPrevFld%><%--Same line as previous field*****--%><br></td></tr>
		 <%}%>
		 <% if (   samLinePrevFld.equals("1")  ) 
		   {%>
			<tr><td width="25%">	<input type="checkbox" name="sameLine" value="1" CHECKED><%=MC.M_SameLine_AsPrevFld%><%--Same line as previous field*****--%><br></td></tr>
		 <%}%>
	</table>
	<br> <br>
	<br>
	<%
		// #5437,5438 02/02/2011 @Ankit
	if(((pageRight == 6 && mode.equals("M") ) || (pageRight == 5 && mode.equals("N")) || pageRight == 7) && (mode.equals("M") && (!codeStatus.equals("Lockdown") && !codeStatus.equals("Deactivated") && !codeStatus.equals("Freeze") && !codeStatus.equals("Active")) )|| mode.equals("N")){
		if(codeStatus.equals("WIP")){
						%>
						<table align="center"><tr>
	  <td align="center"><br />
				<button type="submit" id="submit_btn" ondblclick="return false"><%=LC.L_Submit%></button>
			</td> 
	  </tr></table>
     
<%}else{ 
%>
<br />
	<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="editBoxFieldFrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include> 

<br>
<% }
	}%>
     <br>

      <td align=right> 

      </td> 
      </tr>
  </table>
  </Form>
  
<%

	} //end of if body for page right
	else
	{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%

	} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
<%

}

%>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

</body>

</html>


		

