<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.velos.eres.service.util.*"%>
<html>
<head>
<title><%=LC.L_Register_WithEres%><%--Register with eResearch*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
 
<jsp:include page="panel.jsp" flush="true"/>   

<body>
<br>
<DIV class="formDefault" id="div1" >
<Form name="signup" method="post" action="accountsave.jsp" >
    <p>&nbsp;</p>
    <table width="680" cellspacing="0" cellpadding="0">
      <tr> 
        <td height="30"> 
          <p class = "defComments"><font size="5"><%=MC.M_ReqDocu_NotFound%><%--We''re sorry, the document you requested is not found.*****--%></font></p>
        </td>
      </tr>
    </table>
    <p>&nbsp;</p>
    <table width="567" border="0">
      <tr> 
        <td width="29" height="22">1.</td>
        <td width="528" height="22"><%=MC.M_Misspelling_InUrl%><%--There may be a typo or misspelling in the URL you have entered.*****--%></td>
      </tr>
      <tr> 
        <td width="29">2.</td>
        <td width="528"><%=MC.M_PgFile_LookingMoved%><%--The page or file you are looking for has been moved, retired or does not exist.*****--%></td>
      </tr>
      <tr>
        <td width="29">3.</td>
        <td width="528"><a href="http://www.veloseresearch.com"><%=MC.M_ClkHere_RetHpage%><%--Click here</a> to return to the home page.*****--%></td>
      </tr>
    </table>
    <P class = "sectionHeadings"><br>
    </P>
  </Form>
</div>
</body>
</html>

