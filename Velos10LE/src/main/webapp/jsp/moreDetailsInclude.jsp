<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
HttpSession tSession = request.getSession(true);
if (!sessionmaint.isValidSession(tSession))
	return;
%>
<%!
public static final String Str_ModuleName_AE = "advtype";
public static final String Str_ModuleName_Event = "evtaddlcode";
public static final String Str_ModuleName_User = "user";
%>
<%
boolean Module_AE = (Str_ModuleName_AE.equals(request.getParameter("modName")))? true : false;
boolean Module_UD = (Str_ModuleName_User.equals(request.getParameter("modName")))? true : false;
boolean Module_ED = (Str_ModuleName_Event.equals(request.getParameter("modName")))? true : false;

%>
<script>
var moreDetailsFunctions = {

	formObj: {},
	openLookup: {},
	openMultiLookup: {},
	moreDetailsCodeJSON: {},
	getMoreDetailsCode: {},
	getMoreDetailsCodePK: {},
	getMoreDetailsField: {},

	setValue4ChkBoxGrp: {},
	setValue: {},
	setDD: {},
	validate: {},
	fixTextAreas: {}
};

var parentPageFormObj;

moreDetailsFunctions.setValue4ChkBoxGrp = function(obj,iElementId){
	
	var chkFlds = document.getElementsByName('alternateId'+iElementId+"Checks");
	var hiddenInputFld =  document.getElementById('alternateId'+iElementId);
	
	hiddenInputFld.value = '';
	
	for (var indx = 0; indx < chkFlds.length; indx++){
		if (chkFlds[indx].checked){
			var checkValue = chkFlds[indx].value;
			if (hiddenInputFld.value.length==0){
				hiddenInputFld.value = checkValue;
			} else {
				hiddenInputFld.value += ','+checkValue;
			}
		}
	}
	
};

moreDetailsFunctions.fixTextAreas = function(){
	
	//Maximum possible database limit is 4000 charcters
	var characters= 4000;
	$j(".mdTextArea").keyup(function(){
	    
		if($j(this).val().length > characters){
	        $j(this).val($j(this).val().substr(0, characters));
		}	
	});
}


moreDetailsFunctions.setValue = function(formobj,iElementId,cbcount){
	
	if(parentPageFormObj){
		formobj = parentPageFormObj;
	}
	
	var chkFld = formobj['alternateId'+iElementId];
	var value = chkFld.value;
	if (value=="Y") {
		chkFld.value="N";
		chkFld.checked=false;
	} else if ((value.length==0) || (value=="N"))  {
		chkFld.value="Y";
		chkFld.checked=true;
	} else { // <== there is some junk data in the DB column already
		chkFld.value="Y";
		chkFld.checked=true;
	}
}


moreDetailsFunctions.setDD = function(formobj){
	
	if(parentPageFormObj){
		formobj=parentPageFormObj;
	}
	
	var optvalue;
	optvalue=formobj.ddlist.value;
	if (optvalue.length>0)
	{
		var arrayofStrings=optvalue.split("||");
		if (arrayofStrings.length>1) {
			for (var j=0;j<arrayofStrings.length;j++)
			{
				var ddStr=arrayofStrings[j];
				var arrayofDD=ddStr.split(":");
				var ddcount=arrayofDD[0];
				var selvalue=arrayofDD[1];
				var ddFld = formobj['alternateId'+ddcount];
				var opt = null;
				if (ddFld && ddFld.options) {
					opt = ddFld.options;
					for (var i=0;i<opt.length;i++){
						if (opt[i].value==selvalue){
							ddFld.selectedIndex=i ;
						}
					}
				}
			}
		} else {
			var ddStr=arrayofStrings[0];
			var arrayofDD=ddStr.split(":");
			var ddcount=arrayofDD[0];
			var selvalue=arrayofDD[1];
			var ddFld = formobj['alternateId'+ddcount];
			if (ddFld && ddFld.options) {
			 	var opt = ddFld.options; 
				if (opt == undefined)
			    	opt = ddFld.options; 
				
				for (var i=0;i<opt.length;i++){
					if (opt[i].value==selvalue){
						ddFld.selectedIndex=i ;
					}
				}
			}
		}// end else
	}//optvalue.length>0	
}

moreDetailsFunctions.validate = function(formobj) {
	
	if (!(validate_col('e-Signature',formobj.eSign))) return false;
	
    if(isNaN(formobj.eSign.value) == true) {
    	 alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
    	 formobj.eSign.focus();
    	 
		return false;
	}

	return true;
} 

moreDetailsFunctions.openLookup = function (viewId, keyword){
	
	if (!viewId) return;
	var dfilter = '';
	
	var formName;
	if(parentPageFormObj){
		formName=parentPageFormObj.name;
	}else{
		docuement.forms[0].name
	}
	
	windowName = window.open("getlookup.jsp?viewId="+viewId+"&form="+formName+"&dfilter="+dfilter+"&keyword="+keyword,"Information",
			"toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100 0, ");
	windowName.focus();
};

moreDetailsFunctions.openMultiLookup = function (viewId, keyword){
	if (!viewId) return;
	var dfilter = '';
	var formName;
	
	if(parentPageFormObj){
		formName=parentPageFormObj.name;
	}else{
		docuement.forms[0].name;
	}
	
	windowName = window.open("multilookup.jsp?viewId="+viewId+"&form="+formName+"&seperator=,&dfilter="+dfilter+"&keyword="+keyword,"Information",
			"toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100 0, ");
	windowName.focus();
};

$j(document).ready( function() {
	
	<%if (Module_AE){%>
		parentPageFormObj = document.adverseEventScreenForm;
	<%}%>
	
	<%if (Module_UD){%>
		parentPageFormObj = document.userdetails;
	<%}%>
	
	<%if (Module_ED){%>
		parentPageFormObj = document.eventdetail;
	<%}%>
	
	moreDetailsFunctions.setDD(document.moredetails);
	moreDetailsFunctions.fixTextAreas();
	
});
</script>