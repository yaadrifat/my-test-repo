<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<!-- Bug#9911 01-Jun-2012 -Sudhir-->
<jsp:include page="include.jsp" flush="true"/>
<%@page import="com.velos.eres.service.util.*"%><HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=">

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<BODY>
<jsp:useBean id="formlibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
  
 int iformLibId=0;
 String selectedForms[] = null;
 selectedForms = request.getParameterValues("selectedForms");
 String formName=StringUtil.stripScript(request.getParameter("name"));
 formName = formName.trim();
 String formType=request.getParameter("formType");
 
 
 HttpSession tSession = request.getSession(true); 
 String ipAdd = (String) tSession.getValue("ipAdd");

 String usr = (String) tSession.getValue("userId");


 if (sessionmaint.isValidSession(tSession)) {
%>
	
<jsp:include page="sessionlogging.jsp" flush="true"/> 

<%	
	 String eSign= request.getParameter("eSign");
	 String oldESign = (String) tSession.getValue("eSign");
	 String frmStat = request.getParameter("formStatus");
	 if((frmStat.equals("W")) || ((!frmStat.equals("W") && !frmStat.equals("")) && (oldESign.equals(eSign)))){

	 iformLibId = formlibB.copyFormsFromLib( selectedForms ,  formType ,  formName , usr ,   ipAdd ) ;
   	 if (iformLibId == -3)
		{%>
			<br><br><br><br><br>
			<p class = "successfulmsg" align = center > <%=MC.M_UnqFrmName_EtrUnqFrmName%><%-- The unique form name already exists. Please enter a different unique form name*****--%></p>
			<center><!-- Bug#9911 01-Jun-2012 -Sudhir--><button tabindex=2 onClick="history.go(-1);return false;"><%=LC.L_Back%></button>
		<%
		} else if(iformLibId < 0 ) {
		%> 
		<br><br><br><br><br>
			<p class = "successfulmsg" align = center> <%=MC.M_Data_NotSvdSucc%><%-- Data not saved successfully*****--%> </p>
			<!-- Bug#9911 01-Jun-2012 -Sudhir-->
			<button name="back" onclick="window.history.back();return false;"><%=LC.L_Back%></button></p>
		<% return;
		} 
	
	else {%>
		<br><br><br><br><br><p class = "successfulmsg" align = center>
		<%=MC.M_Data_SavedSucc %><%-- Data saved successfully*****--%></p>	
		<script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
	  	</script>
	<%}
	}else{ %>
	
	<jsp:include page="incorrectesign.jsp" flush="true"/>
	<% }
}
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
 <%-- Ankit: Process bar appears continuously. Date:10-July-2012 --%>
<jsp:include page="bottompanel.jsp" flush="true"/>
</BODY>

</HTML>

