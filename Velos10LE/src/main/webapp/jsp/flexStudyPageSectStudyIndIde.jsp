<%--
This JSP inherited most of the logic from patientschedule.jsp. This JSP gets called 
by patientschedule.jsp via AJAX for a particular visit and returns the events in that visit.
 --%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="studyJB" scope="session" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="altId" scope="request" class="com.velos.eres.web.studyId.StudyIdJB"/>
<%@page import="com.velos.eres.business.common.*,com.velos.eres.service.util.*"%>
<%@page import="com.velos.eres.web.studyRights.StudyRightsJB"%>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@page import="com.velos.eres.web.study.StudyJB"%>
<%@page import="com.velos.esch.business.common.*"%>
<%@page import="com.velos.eres.widget.service.util.FlxPageCache,com.velos.eres.widget.service.util.FlxPageFields"%>
<%@page import="java.util.*,java.io.*"%>
<%@page import="org.json.*,org.w3c.dom.*"%>
<%
String jPageAvailSections = FlxPageCache.getStudyPageSections();
JSONArray jsSectionArray = (StringUtil.isEmpty(jPageAvailSections)? new JSONArray() :  new JSONArray(jPageAvailSections));
%>
<script>
var pageSectionRegistry = <%=jPageAvailSections.toString()%>;
var activeSection = 0;
</script>
<script language="JavaScript" src="formjs.js"><!-- FORM JS--></script>
  <%
   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))
   {

    int sectionNo = StringUtil.stringToNum(request.getParameter("sectionNo"));

    String defUserGroup = (String) tSession.getAttribute("defUserGroup");

	//put the values in session
	String studyId = request.getParameter("studyId");
	if(studyId == null || studyId.equals("null")) {
		studyId = (String) tSession.getAttribute("studyId");
		studyJB = null;
	} else 	{
		if (StringUtil.stringToNum(studyId) < 1){
			studyId="0";
			studyJB = null;
		} else {
			studyJB = new StudyJB();
			studyJB.setId(StringUtil.stringToNum(studyId));
			studyJB.getStudyDetails();
		}
		tSession.setAttribute("studyId",studyId);
	}
	
	String usr = (String) tSession.getAttribute("userId");
	int accountId = StringUtil.stringToNum((String)tSession.getAttribute("accountId"));
    int pageRight = 0;

    if (StringUtil.stringToNum(studyId) == 0){
    	grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
   		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));
    } else {
		//******************GET STUDY TEAM RIGHTS and put the same in session, this will be a separate obj as no study is selected //***************************************************************
	    TeamDao teamDao = new TeamDao();
	    teamDao.getTeamRights(StringUtil.stringToNum(studyId),StringUtil.stringToNum(usr));
	    ArrayList tId = teamDao.getTeamIds();
	    if (tId.size() == 0)
		{
	    	pageRight=0 ;
	    }
		else
		{
	    	stdRights.setId(StringUtil.stringToNum(tId.get(0).toString()));
	   	 	ArrayList teamRights ;
			teamRights  = new ArrayList();
			teamRights = teamDao.getTeamRights();
	
			stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
			stdRights.loadStudyRights();
	
	    	tSession.setAttribute("studyRights",stdRights);
	    	if ((stdRights.getFtrRights().size()) == 0)
			{
	    	 	pageRight= 0;
	    	}
			else
			{
	    		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYSUM"));
	    	}
	    }
    }%>

	<%if (StringUtil.isAccessibleFor(pageRight, 'V')) {%>
   <table width="100%" cellspacing="2" cellpadding="2" border="0" class="basetbl midAlign">
		<tr>					
			<td align="left" width="50%">
			<br/>
				<jsp:include page="ctrpDraftSectINDIDE.jsp">
					<jsp:param value="<%=studyId%>" name="studyId"/>
					<jsp:param value="editable" name="mode"/>
				</jsp:include>					
			</td>
	    </tr>				
	</table>
	<% } else {%>
	  <jsp:include page="accessdenied.jsp" flush="true"/>
	<% }
} //end of if session times out
else
{
%>
	<jsp:include page="timeout.html" flush="true"/>
<%
} //end of else body for page right
%>
