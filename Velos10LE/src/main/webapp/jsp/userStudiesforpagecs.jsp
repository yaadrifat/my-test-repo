<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>

<title><%=LC.L_Manage_Pcols%><%--Manage Protocols*****--%></title>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT language="JavaScript1.1">

function openwinsnap() {
    windowName=window.open("snapshottext.htm","Information","toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes,width=500,height=400")
	windowName.focus();
;}

function confirmBox(pageRight) {

	if(f_check_perm(pageRight,'N'))
	{
		if (confirm("<%=MC.M_SureToCreate_StdPcolVer%>"))/*if (confirm("Are you sure you want to create a version of this study protocol?"))*****/ {
	    return true;
	}else{
		return false;
	}
	}
	else
	{
	return false;
	}
}

/* copied from GB
function openGroup(id,name,grprights)
{
	var src = document.group.src.value;
	var frompage =document.group.frompage.value;
	var mode =document.group.mode.value;
	group.action= "group.jsp?mode="+mode +"&grpId=" +id +"&srcmenu="+src +"&fromPage="+frompage;
	//alert(group.action);
	//this.location.replace("group.jsp?mode="+mode +"&grpId=" +id +"&srcmenu="+src +"&fromPage="+frompage)
	group.submit();
}

function assignRights(id,name,grprights)
{
	var src = document.group.src.value;
	var frompage =document.group.frompage.value;
	var mode =document.group.mode.value;
	group.action= "groupRights.jsp?mode="+mode +"&groupId=" +id +"&srcmenu="+src +"&fromPage="+frompage+"&groupName=" +name;
	group.submit();
}

function groupUsers(id,name,grprights)
{
	var src = document.group.src.value;
	var frompage =document.group.frompage.value;
	var mode =document.group.mode.value;
	group.action= "groupUsers.jsp?mode="+mode +"&srcmenu=" +src +"&groupId=" +id +"&grpName=" +name;
	group.submit();
}
*/

</SCRIPT>

</HEAD>



<BODY overflow=visible>
<jsp:useBean id="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.web.user.UserJB,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.ulink.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*"%>
<BR>




  <%HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))



{





 	String uName = (String) tSession.getValue("userName");





	int pageRight = 0;



	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");

	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));

%>
  <%-- IN THE ABOVE LINE OF CODE THE Id (MSITES) FOR SITES NEED TO REPLACED BY THAT OF Study--%>
  <%

if (pageRight > 0 )

	{
	   	String userId = (String) tSession.getValue("userId");
/*		StudyDao studyDao = studyB.getStudyValuesForUsers(userId);
		ArrayList studyIds 		= studyDao.getStudyIds();
		ArrayList studyTitles 		= studyDao.getStudyTitles();
		ArrayList studyObjectives 	= studyDao.getStudyObjectives();
		ArrayList studySummarys 	= studyDao.getStudySummarys();
		ArrayList studyDurs 		= studyDao.getStudyDurations();
		ArrayList studyDurUnits 	= studyDao.getStudyDurationUnits();
		ArrayList studyEstBgnDts 	= studyDao.getStudyEstBeginDates();
		ArrayList studyActBghDts 	= studyDao.getStudyActBeginDates();
		ArrayList studyParentIds 	= studyDao.getStudyParents();
		ArrayList studyAuthors 		= studyDao.getStudyAuthors();
		ArrayList studyNumbers 		= studyDao.getStudyNumbers();
		ArrayList studyVersions = studyDao.getStudyVersions();
	   	int studyId = 0;

int len = studyIds.size();
		String studyTitle = "";
		String studyNumber = "";
		String studyVersion = "";*/
	   	int counter = 0;

			Vector vec = new Vector();

			Column studyId = new Column(LC.L_Std_Id/*Study Id*****/);
			studyId.passAsParameter = true;
			studyId.hidden = true;

			vec.add(studyId);


			Column studyNumber = new Column(LC.L_Study_Number/*Study Number*****/);
			studyNumber.width = "25%"	;
			studyNumber.align = "left"	;
			vec.add(studyNumber);

			Column studyVersion = new Column(LC.L_Version/*Version*****/);
			studyVersion.width = "25%"	;
			studyVersion.align = "left"	;
			vec.add(studyVersion);

			Column studyTitle = new Column(LC.L_Study_Title/*Study Title*****/);
			studyTitle.width = "45%" ;
			studyTitle.align = "left"	;
			vec.add(studyTitle);

			Column manageProtocol  = new Column("");
			manageProtocol.value = LC.L_Manage_Protocol/*Manage Protocol*****/;
			manageProtocol.funcName = "manageProtocol";
			manageProtocol.width = "15%";
			manageProtocol.align = "left";
			vec.add(manageProtocol);

			Column managePatient  = new Column("");
			managePatient.value = LC.L_Manage_Pat/*"Manage "+LC.Pat_Patient*****/;
			managePatient.funcName = "managePatient";
			managePatient.width = "15%";
			managePatient.align = "left";
			vec.add(managePatient);

			String sSQL = "select PK_STUDY, STUDY_TITLE, STUDY_NUMBER,  STUDY_VER_NO from er_study where pk_study in (select FK_STUDY from er_studyteam where fk_user = " + userId +")";
			String title =MC.M_ListGrp_AldyCreated+":"/*The list below displays the Groups that have already been created for your account:*****/;




			String srchType=request.getParameter("srchType");
			String sortType=request.getParameter("sortType");
			String sortOrder=request.getParameter("sortOrder");

			request.setAttribute("sSQL",sSQL);

			request.setAttribute("srchType",srchType);
			request.setAttribute("vec",vec);
			request.setAttribute("sTitle",title);

			String s_scroll=request.getParameter("scroll");
			request.setAttribute("scroll",s_scroll);
			request.setAttribute("sortType",sortType);
			request.setAttribute("sortOrder",sortOrder);


%>
  <P class = "userName"> <%= uName %> </P>
  <P class="sectionHeadings"> <%=LC.L_MngPcol_Open%><%--Manage Protocols>> Open*****--%> </P>
  <Form name="studybrowser" method="post" action="userStudiesforpage.jsp" onsubmit="">
    <table width="100%" cellspacing="0" cellpadding="0" border=0 >
      <tr >
        <td width = "100%">
          <P class = "defComments"> <%=MC.M_AssignRgt_PatInfo%><%--You should have access to a protocol and have been assigned rights to patient information to be able to Manage patients.*****--%>
		  <Br>
		  <%=MC.M_AcesToThe_FlwStdPcol%><%--You have access to the following Study Protocols*****--%>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href = '#' onClick=openwinsnap()><%=MC.M_What_IsSnapshot%><%--What is a Snapshot*****--%> </A>
          </P>
        </td>
      </tr>
      <tr>
        <td height="10"></td>
        <td height="10"></td>
      </tr>
    </table>

	<jsp:include page="result2cs.jsp?jsFile=searchJS2cs&browser=1" flush="true"></jsp:include>
  </Form>




  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
<br>

</body>

</html>



