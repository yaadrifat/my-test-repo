<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.StringUtil" %><%@page import="com.velos.eres.service.util.LC"%><%@page import="com.velos.eres.service.util.MC"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="topicJB" scope="request" class="com.velos.eres.web.submission.ReviewMeetingTopicJB"/>

<%@page import="com.velos.eres.service.util.EJBUtil"%><html>
<head>
<title><%=LC.L_Meeting_Topic%><%--Meeting Topic*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<body>
<%
  HttpSession tSession = request.getSession(true); 
  String accId = (String) tSession.getValue("accountId");
  String studyId = (String) tSession.getValue("studyId");
  String grpId = (String) tSession.getValue("defUserGroup");
  String ipAdd = (String) tSession.getValue("ipAdd");
  String usr = (String) tSession.getValue("userId");
  String eSign = request.getParameter("eSign");
  if (sessionmaint.isValidSession(tSession))
  {	
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
    String oldESign = (String) tSession.getValue("eSign");
	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
	    String resultMsg = "";
        int result = 0;
	    if (EJBUtil.stringToNum(request.getParameter("pkTopic")) > 0) {
	        // modify mode
	        topicJB.setId(EJBUtil.stringToNum(request.getParameter("pkTopic")));
	        int existingId = topicJB.loadMeetingTopic();
	        if (existingId < 1) {
	            resultMsg = MC.M_CntFindRec_InDb;/*resultMsg = "Could not find this record in database to modify";*****/
%>
<jsp:include page="errorback.jsp" flush="true"><jsp:param name="errorMsg" value="<%=resultMsg%>"/></jsp:include>
</BODY></HTML>
<%              return; // An error occurred; let's get out of here
	        }
	        
            String topicNumber = StringUtil.trueValue(request.getParameter("topicNumber"));
            topicNumber = StringUtil.htmlEncodeXss(topicNumber).trim();
            if (!topicNumber.equals(topicJB.getTopicNumber())) {
                // Make sure the new Topic Number does not exist
                String reviewMeeting = topicJB.getReviewMeeting();
                topicJB.setTopicNumber(topicNumber);
                int anotherId = topicJB.findByFkReviewMeetingAndTopicNumber();
                if (anotherId > 0) {
                    resultMsg = MC.M_TopicNum_AldyUsed;/*resultMsg = "This Topic Number is already being used. Please specify another.";*****/
%>
<jsp:include page="errorback.jsp" flush="true"><jsp:param name="errorMsg" value="<%=resultMsg%>"/></jsp:include>
</BODY></HTML>
<%                  return; // An error occurred; let's get out of here
                }
            }
            // Passed checks; now update
            topicJB.setId(existingId);
            topicJB.setTopicNumber(topicNumber);
            String topic = StringUtil.trueValue(request.getParameter("topic"));
            //topic = topic.replaceAll("\r\n","<br/>").replaceAll("\n","<br/>");
            //topic = StringUtil.htmlEncodeXss(topic);
            if (topic.length() > 4000) {
                topic = topic.substring(0, 3999);
            }
            topicJB.setMeetingTopic(topic);
            topicJB.setLastModifiedBy(usr);
            topicJB.setIpAdd(ipAdd);
            result = topicJB.updateMeetingTopic(); 
	    } else {
	        // new mode
            topicJB.setReviewMeeting(request.getParameter("meetingId"));
            String topicNumber = StringUtil.trueValue(request.getParameter("topicNumber"));
            topicJB.setTopicNumber(StringUtil.htmlEncodeXss(topicNumber));
            int existingId = topicJB.findByFkReviewMeetingAndTopicNumber();
            if (existingId > 0) {
            	resultMsg = MC.M_TopicNum_AldyUsed;/*resultMsg = "This Topic Number is already being used. Please specify another.";*****/
%>
<jsp:include page="errorback.jsp" flush="true"><jsp:param name="errorMsg" value="<%=resultMsg%>"/></jsp:include>
</BODY></HTML>
<%              return; // An error occurred; let's get out of here
            }
            String topic = StringUtil.trueValue(request.getParameter("topic"));
            //topic = topic.replaceAll("\r\n","<br/>").replaceAll("\n","<br/>");
            //topic = StringUtil.htmlEncodeXss(topic);
            if (topic.length() > 4000) {
                topic = topic.substring(0, 3999);
            }
            topicJB.setMeetingTopic(topic);
            topicJB.setCreator(usr);
            topicJB.setLastModifiedBy(usr);
            topicJB.setIpAdd(ipAdd);
            result = topicJB.createMeetingTopic();
	    }
        if (result > 0) {
            resultMsg = MC.M_Data_SvdSucc;/*resultMsg = "Data was saved successfully";*****/
        } else {
            resultMsg = MC.M_Err_SavingData;/*resultMsg = "There was an error while saving data";*****/
%>
<jsp:include page="errorback.jsp" flush="true"><jsp:param name="errorMsg" value="<%=resultMsg%>"/></jsp:include>
</BODY></HTML>
<%          return; // An error occurred; let's get out of here
        }
%>
  <br><br><br><br>
  <p class="successfulmsg" align="center"><%=resultMsg%></p>
<script>
  window.opener.location.href = 'irbmeeting.jsp?meetingDate=<%=topicJB.getReviewMeeting()%>&reviewBoard=<%=StringUtil.htmlEncodeXss(request.getParameter("reviewBoard"))%>';
  setTimeout("self.close()",1000);
</script>
<%        
    }// end of if body for e-sign
  } //end of if body for valid session
  else
  {
  %>
  <jsp:include page="timeout.html" flush="true"/>
  <%
  }
%>
</body>
</html>
