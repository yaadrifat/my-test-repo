<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_AddOrEdit_Queries%><%--Add/Edit Queries*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>				
<%-- Nicholas : Start --%>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%--<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>--%>
<%-- Nicholas : End --%>
<Link Rel=STYLESHEET HREF='./styles/velos_popup.css' type=text/css>
<script>

function openQueryHistoryWin(formobj,formQueryId,fieldName)
{
        param1="formQueryHistory.jsp?formQueryId="+formQueryId+"&fieldName="+encodeString(fieldName);
		windowName= window.open(param1,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,left=90,top=80,width=800,height=300");
		windowName.focus();
}

function openAddNewWin(filledFormId, from, formId)
{
  param1="addNewQuery.jsp?queryType=2&thread=new&&filledFormId="+filledFormId+"&from="+from+"&formId="+formId;
		windowName= window.open(param1,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,left=90,top=80,width=600,height=500");
		windowName.focus();

}



	
</script>


<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="formQueryDao" scope="request" class="com.velos.eres.business.common.FormQueryDao"/>
<jsp:useBean id="formQueryB" scope="request" class="com.velos.eres.web.formQuery.FormQueryJB"/>

<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,com.velos.eres.service.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>

<BR>


<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>







<DIV id="div1"> 
<b><p class="defcomments"> <%=LC.L_FrmAdd_EditQry%><%--Forms >> Add/Edit Queries*****--%></p></b>

  <%
   HttpSession tSession = request.getSession(true); 
  String calledFrom=request.getParameter("calledFrom");
  String dashBoard=request.getParameter("dashBoard");
  String formType=request.getParameter("formType");/*calling form jupiter*/
  ArrayList fieldNames=null;
  ArrayList fieldIds=null;
  String siteId="";
   if (sessionmaint.isValidSession(tSession))
   { 
	   /* YK Bug#4822 19May2011 */	
      int pageRight = 0;
      pageRight = EJBUtil.stringToNum((String)session.getAttribute("formQueryRight"));
      if("jupiter".equals(dashBoard))
	   {
		   if(pageRight==0){
	    		  pageRight=7;
	    	  }  
	   }
      if("Advrse_Form".equals(formType)){
    	  if(pageRight==0){
    		  pageRight=7; 
    	  }
      }
      if("PatStudy_Form".equals(formType)){
    	  int viewRight=0;
    	  siteId=request.getParameter("site_id");
    	  StdSiteRightsDao stdSiteRightDao=new StdSiteRightsDao();
    	  viewRight=stdSiteRightDao.stdSiteRightOfOrgTouser(request.getParameter("studyId"),(String) tSession.getValue("userId"),siteId);
    	  if(viewRight==1){
    		  pageRight=7; 
    	  }  
      }
	  String userId = (String) tSession.getValue("userId");
      String accountId = (String) tSession.getValue("accountId");		
	// String adveventId=request.getParameter("adveventId");
	 String advFieldId=request.getParameter("advFieldId");
	 String pkey=request.getParameter("pkey");
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
		int account_form_rights = Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMS"));

   
     
     String filledFormId = "";
     String from = "";
     int count = 0;
	String studyId = "";

	
	   int iaccId=EJBUtil.stringToNum(accountId);

    
	 if (isAccessibleFor(pageRight, 'V'))
	 {
	 	 filledFormId = request.getParameter("filledFormId");
	 	 from = request.getParameter("from");
	 	
	 	studyId = request.getParameter("studyId");
	 	
	 	if (StringUtil.isEmpty(studyId))
	 	{
	 		studyId="";
	 	}
	 	
	 		
		 
		 String formId= request.getParameter("formId");
%>
		<Form  name="addEditQueries" method="post" action="">
	  <%
		int formQueryId = 0;
		int queryStatusId = 0;
		String entrdOn = "";
		int fieldId = 0; 
		String queryStat = "";
		String entrdBy = "";
		String fieldName = "";
		String queryType = "";
	if("Advrse_Form".equals(formType)||"adverseEvent".equals(request.getParameter("calledFrom"))){
		formQueryDao.getQueriesForStaticForm( EJBUtil.stringToNum(filledFormId), "Advrse_Form",8);
		fieldIds=formQueryDao.getFieldIds();
		
		 //code of FieldName at here.
	}else if("PatStudy_Form".equals(formType)){	 //code of FieldName at here.
		formQueryDao.getQueriesForStaticForm( EJBUtil.stringToNum(filledFormId), "PatStudy_Form",9);
		fieldIds=formQueryDao.getFieldIds();	
		
	}else{
		formQueryDao = formQueryB.getQueriesForForm(EJBUtil.stringToNum(formId), EJBUtil.stringToNum(filledFormId), EJBUtil.stringToNum(from));
		 fieldNames = formQueryDao.getFieldNames();
		 
	}

		ArrayList formQueryIds = formQueryDao.getFormQueryIds();
		
				
		ArrayList queryStatusIds = formQueryDao.getFormQueryStatusIds();
		
		ArrayList queryTypes = formQueryDao.getQueryType();
		
				
		ArrayList enteredOn = formQueryDao.getEnteredOn();
		
				
		//ArrayList fieldIds = formQueryDao.getFieldIds() ;
				
		ArrayList queryStatus = formQueryDao.getQueryStatus();
				
		ArrayList enteredBy = formQueryDao.getEnteredBy();
				
		
	
		int rowsReturned = formQueryIds.size();
	
	
	  %>
	    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="basetbl">
		<tr>
	    <td width="80%"><P class="sectionheadings"><%=LC.L_Queries_ForForm%><%--Queries for Form*****--%></P></td>
		<td>
		<%if (isAccessibleFor(pageRight, 'E')){
		if("adverseEvent".equals(request.getParameter("calledFrom"))){%>
		<A href="addNewQuery.jsp?studyId=<%=studyId%>&queryType=2&thread=new&pkey=<%=pkey%>&from=<%=from%>&adveventId=<%=filledFormId%>&calledFrom=adverseEvent&advFieldId=<%=advFieldId%>" ><%=LC.L_Add_New%><%--Add New*****--%> </A>
		<%}else if("Advrse_Form".equals(formType)||"PatStudy_Form".equals(formType)){%>
			
			<A href="addNewQuery.jsp?studyId=<%=studyId%>&queryType=2&thread=new&filledFormId=<%=filledFormId%>&from=<%=from%>&formId=<%=formId%>&from=1&formType=<%=formType%>&site_id=<%=siteId%>" ><%=LC.L_Add_New%><%--Add New*****--%> </A>
		<% }
		else{ %>
		<A href="addNewQuery.jsp?studyId=<%=studyId%>&queryType=2&thread=new&filledFormId=<%=filledFormId%>&from=<%=from%>&formId=<%=formId%>&from=1" ><%=LC.L_Add_New%><%--Add New*****--%> </A>
		
		<%}
		}
	    
		%>
		</td>		
		</tr>
		
		</table>
<%-- Nicholas : Start --%>
    <table class="basetbl outline" width="100%" cellspacing="0" cellpadding="0" border="0">
<%-- Nicholas : End --%>
       <tr> 

       <th > <%=LC.L_Query_Id%><%--Query ID*****--%></th> <%-- YK -20DEC EDC_AT3 REQ {POINT 3} --%>
       <th > <%=LC.L_Field%><%--Field*****--%></th>
	   <th > <%=LC.L_Query%><%--Query*****--%></th>
	   <th > <%=LC.L_Latest_Status%><%--Latest Status*****--%></th>
	   <th > <%=LC.L_Query_By%><%--Query By*****--%></th>
	   <th > <%=LC.L_Query_Date%><%--Query Date*****--%></th>	  
	   </tr>
	   <% 
	    int fldId=0;
	    for(count = 0;count < rowsReturned;count++)
					{
						 formQueryId = (   (Integer)formQueryIds.get(count)  ).intValue();
						 
						 queryStatusId = (   (Integer)queryStatusIds.get(count)  ).intValue();
						 
						 queryType =  ((queryTypes.get(count)) == null)?"-":(queryTypes.get(count)).toString();
					 	 
						 entrdOn = ((enteredOn.get(count)) == null)?"-":(enteredOn.get(count)).toString();
						 
						 queryStat = ((queryStatus.get(count)) == null)?"-":(queryStatus.get(count)).toString();
						 
						 entrdBy = ((enteredBy.get(count)) == null)?"-":(enteredBy.get(count)).toString();
						 if("Advrse_Form".equals(formType)||"adverseEvent".equals(request.getParameter("calledFrom"))){
							 fldId=((Integer)fieldIds.get(count)).intValue();
							 fieldName= (fieldIds.get(count)==null)?"":getAdverseFieldName(((Integer)fieldIds.get(count)).intValue());
						 }else if("PatStudy_Form".equals(formType)){
							 System.out.println("1.addEdiQuery.jsp");
							 fldId=((Integer)fieldIds.get(count)).intValue();
							 fieldName= (fieldIds.get(count)==null)?"":getPatStudyFieldName(((Integer)fieldIds.get(count)).intValue());
						 }else{
						 fieldName = ((fieldNames.get(count)) == null)?"-":(fieldNames.get(count)).toString();
						 }
						 
						if ((count%2)==0) 
						{
						%>
					      	<tr class="browserEvenRow">
						<%}else
						 { %>
				      		<tr class="browserOddRow">
						<%} %>
						<td><%=formQueryId%></td> <%-- YK -20DEC EDC_AT3 REQ {POINT 3} --%>
						<td>
						<%if("Advrse_Form".equals(formType)){ %>
						<A href="formQueryHistory.jsp?studyId=<%=studyId%>&formQueryId=<%=formQueryId%>&fieldName=<%=StringUtil.encodeString(fieldName)%>&filledFormId=<%=filledFormId%>&from=<%=from%>&formId=<%=formId%>&formType=Advrse_Form&fieldId=<%=fldId%>"><%=fieldName%></A>
						<%}else if("PatStudy_Form".equals(formType)){System.out.println("2.addEditQuery.jsp"); %>
						<A href="formQueryHistory.jsp?studyId=<%=studyId%>&formQueryId=<%=formQueryId%>&fieldName=<%=StringUtil.encodeString(fieldName)%>&filledFormId=<%=filledFormId%>&from=<%=from%>&formId=<%=formId%>&formType=PatStudy_Form&fieldId=<%=fldId%>"><%=fieldName%></A>
						<%}else{ %>
						<A href="formQueryHistory.jsp?studyId=<%=studyId%>&formQueryId=<%=formQueryId%>&fieldName=<%=StringUtil.encodeString(fieldName)%>&filledFormId=<%=filledFormId%>&from=<%=from%>&formId=<%=formId%>"><%=fieldName%></A>
						<%} %>
						</td>
						<td><%=queryType%></td>
						<td><%=queryStat%></td>						
						<td><%
						if(entrdBy.equals("system-generated")){%>
						&lt;<%=entrdBy%>&gt;
						<%}else{%>
						<%=entrdBy%>
						<%}%>
						</td>
						
						<td><%=entrdOn%></A>
						</td>
								
			    	 	</tr>
						
				<%	}//counter
	%>
	  </table>
	  </FORM>
 <%
		
	} //end of if body for page right

else
{%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%
} //end of else body for page right
	}
else
{ %>
 <jsp:include page="timeout.html" flush="true"/> 
<%}%>
  
</body>

</html>
<%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
//Adverse Event Field Identified
private String getAdverseFieldName(int fieldId){
	String fieldName="";
	switch(fieldId){
	case 11012: fieldName="Adverse Event ID";
	break;
	case 11013: fieldName="Adverse Event Type";
    break;
	case 11014: fieldName="Category";
    break;
	case 11015: fieldName="Adverse Event Name";
    break;
	case 11016: fieldName="Toxicity";
    break;
	case 11017: fieldName="Toxicity Description";
    break;
	case 11018: fieldName="Severity/Grade";
    break;
	case 11019: fieldName="MedDRA code";
    break;
	case 11020: fieldName="Dictionary";
    break;
	case 11021: fieldName="Other Description";
    break;
	case 11022: fieldName="Treatment Course";
    break;
	case 11023: fieldName="Start Date";
    break;
	case 11024: fieldName="Stop Date";
    break;
	case 11025: fieldName="AE Discovery Date";
    break;
	case 11026: fieldName="AE Logged Date ";
    break;
	case 11027: fieldName="Entered By";
    break;
	case 11028: fieldName="Reported By";
    break;
	case 11029: fieldName="Attribution";
    break;
	case 11030: fieldName="Linked To ";
    break;
	case 11031: fieldName="Outcome Information";
    break;
	case 11032: fieldName="Action";
    break;
	case 11033: fieldName="Additional Information";
    break;
	case 11034: fieldName="The Following were Notified";
    break;
	case 11035: fieldName="Notes";
    break;
	case 11036: fieldName="Reason For Change (FDA Audit)";
    break;
	case 11037: fieldName="AE Status";
    break;
	case 11038: fieldName="Severity/Grade Description";
    break;
	case 11039: fieldName="Recovery Description";
    break;
	case 11040: fieldName="Outcome Notes";
    break;
	}
	return fieldName;
}

private String getPatStudyFieldName(int fieldId){
	String fieldName="";
	switch(fieldId){
	case 13012: fieldName="Attachment Status";
    break;
	case 13013: fieldName="Reason";
    break;
	case 13014: fieldName="Status Date";
    break;
	case 13015: fieldName="Notes";
    break;
	case 13016: fieldName="Next Follow-up Date";
    break;
	case 13017: fieldName="Patient Study ID";
    break;
	case 13018: fieldName="Enrolling Site";
    break;
	case 13019: fieldName="Assigned To";
    break;
	case 13020: fieldName="Physician";
    break;
	case 13021: fieldName="Treatment Location";
    break;
	case 13022: fieldName="Treating Organization";
    break;
	case 13023: fieldName="Disease Code";
    break;
	case 13024: fieldName="Anatomic Site";
    break;
	case 13025: fieldName="Evaluable Flag";
    break;
	case 13027: fieldName="Evaluable Status";
    break;
	case 13028: fieldName="Unevaluable Status";
    break;
	case 13029: fieldName="Status";
    break;
	case 13030: fieldName="Survival Status";
    break;
	case 13031: fieldName="Date of Death";
    break;
	case 13032: fieldName="Cause of Death";
    break;
	case 13033: fieldName="Specify Cause";
    break;
	case 13034: fieldName="Death Related to Study";
    break;
	case 13035: fieldName="Reason of Death Related to Study";
    break;
	case 13036: fieldName="Current Status";
    break;
	case 13037: fieldName="Screen Number";
    break;
	case 13038: fieldName="Screen By";
    break;
	case 13039: fieldName="Screen OutCome";
    break;
	case 13040: fieldName="Randomization Number";
    break;
	case 13041: fieldName="Enrolled By";
    break;
	case 13042: fieldName="Version No";
    break;
	
	}
	return fieldName;
}
%>