<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>



<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<script>
var windowname="null";
window.name = "labbrowse";
	function setOrder(formObj,orderBy,pgRight) //orderBy column number
{
	var lorderType;
	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";
		lorderType="desc";
	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
		lorderType="asc";
	}

	lsrc = formObj.srcmenu.value;
	lcurPage = formObj.page.value;
	lselectedtab= formObj.selectedTab.value;
	formObj.action="allPatient.jsp?srcmenu="+lsrc+"&page="+lcurPage+"&orderBy="+orderBy+"&orderType="+lorderType+"&selectedTab="+lselectedtab;
	formObj.submit();
}
function setText(form){
  form.selectedStudy.value=form.selStudyId.value;

 }
 function setFilterText(form){
 	  form.ptxt.value=form.patCode.value;
 	  form.atxt.value=form.patage[form.patage.selectedIndex].text ;
 	  if (form.patgender[form.patgender.selectedIndex].value != "")
 	       form.gtxt.value=form.patgender[form.patgender.selectedIndex].text ;
 	  if (form.patstatus[form.patstatus.selectedIndex].value != "")
 	   form.rtxt.value=form.patstatus[form.patstatus.selectedIndex].text ;
 	  form.rbytxt.value=form.regBy.value ;

 }

//Modified by Manimaran to fix the Bugs 2387&2388
function openLabSelect(url,formobj,pageRightPat,pageRightOrg,orgRightPat){

    if (pageRightPat!=null && f_check_perm(pageRightPat,'N') == true){
        formobj.groupName.selectedIndex=0;
 	groupName=formobj.groupName.value;
 	url=url+"&groupName="+groupName;
 	windowname=window.open(url,"labselection","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=870,height=600 top=40,left=100");
 	windowname.focus();
    }

    if (pageRightOrg!=null && f_check_perm_org(pageRightOrg,orgRightPat,'E') == true) {
	formobj.groupName.selectedIndex=0;
 	groupName=formobj.groupName.value;
 	url=url+"&groupName="+groupName;
 	windowname=window.open(url,"labselection","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=870,height=600 top=40,left=100");
 	windowname.focus();
    }
}

//Added by Manimaran to fix the Bugs 2387&2388

function editLabs(pageRightPat,pageRightOrg,orgRightPat) {
    if (pageRightPat!=null)
        return f_check_perm(pageRightPat,'E')

    if (pageRightOrg!=null)
        return f_check_perm_org(pageRightOrg,orgRightPat,'E')
}

 function checkAll(formobj){
 	act="check";
 	totcount=formobj.totcount.value;
  if (formobj.All.value=="checked") act="uncheck" ;
   if (totcount==1){
   if (act=="uncheck") formobj.Del.checked =false ;
   else formobj.Del.checked =true ;}
   else {
     for (i=0;i<totcount;i++){
     if (act=="uncheck") formobj.Del[i].checked=false;
     else formobj.Del[i].checked=true;
     }
    }


    if (act=="uncheck") formobj.All.value="unchecked";
    else formobj.All.value="checked";


 }

 //Modified by Manimaran to fix the Bug2388
 function deleteLabs(formobj,pageRightPat,pageRightOrg,orgRightPat, specPk, calledFrom){


	var check = false;

	if (pageRightOrg!=null) {
		if(f_check_perm_org(pageRightOrg,orgRightPat,'E') == true)  //KM
		{check = true; }
		else
		{check =false;}
	}



	if (pageRightPat!=null && f_check_perm(pageRightPat,'E') == true){
		check = true;
	}



     if(check == true) {

	 var j=0;
	 var cnt = 0;
	 selLabs = new Array();
	 var selLab ="";
	 totcount = formobj.totcount.value;
	 srcmenu = formobj.srcmenu.value;

	 pkey = formobj.pkey.value;
	 patProtId = formobj.patProtId.value;
	 orderBy = formobj.orderBy.value;
	 orderType = formobj.orderType.value;
	 fromLab=formobj.fromLab.value;
	 studyId=formobj.studyId.value;
	 patientCode=formobj.patientCode.value;
	 pageName = formobj.page.value;

	 //added by JM on 4Mar05
	 filEnrDt=formobj.filterEnrDate.value;
	 groupId=formobj.groupName.value;
     labText=formobj.labname.value;
	 selStudyId=formobj.selStudyId.value;
     abnText=formobj.abnresult.value;


	// For pagination but currently not in use
	 //pagenum=formobj.pagenum.value;
	 // pagenum=document.getElementById("pagenum").value;



	 submit="no";
	 if(totcount==0)
	 {
	   alert("<%=MC.M_SelLabs_ToDel%>");/*alert("Please select Labs to be Deleted");*****/
	   return false;
	 }

	 if (totcount==1){
		 if (formobj.Del.checked==true){
			   msg="<%=MC.M_WantToDel_SelLabs%>"/*"Do you want to delete the selected Lab"*****/;
			 if (confirm(msg))
			{
			cnt++;

	//window.open("deleteLabs.jsp?searchFrom=search&srcmenu="+srcmenu+"&selLabs="+formobj.Del.value+"&pkey="+pkey+"&patProtId="+patProtId+"&orderBy="+orderBy+"&orderType="+orderType+"&fromlab="+fromLab+"&studyId="+studyId+"&patientCode="+patientCode,"_self");

	// Modified by JM on 04March05
	//Modified by Manimaran on 29Aug08
	window.open("deleteLabs.jsp?searchFrom=search&srcmenu="+srcmenu+"&page="+pageName+"&selLabs="+formobj.Del.value+"&pkey="+pkey+"&patProtId="+patProtId+"&orderBy="+orderBy+"&orderType="+orderType+"&fromlab="+fromLab+"&studyId="+studyId+"&filterEnrDate="+filEnrDt+"&groupName="+groupId+"&labname="+labText+"&selStudyId="+selStudyId+"&abnresult="+abnText+"&patientCode="+patientCode+"&fromLab="+fromLab+"&specimenPk="+specPk+"&calledFromPg="+calledFrom,"_self");




		 }
		 else
				{
					return false;
				}
		 }

	 }else{
	  for(i=0;i<totcount;i++){

	  if (formobj.Del[i].checked){
		  selLabs[j] = formobj.Del[i].value;
		  j++;
		  cnt++;
			}
		}
		if(cnt>0){
			  msg="<%=MC.M_WantToDel_SelLabs%>"/*"Do you want to delete the selected Lab"*****/;
			  if(confirm(msg)) {


		//window.open("deleteLabs.jsp?searchFrom=search&srcmenu="+srcmenu+"&selLabs="+selLabs+"&pkey="+pkey+"&patProtId="+patProtId+"&orderBy="+orderBy+"&orderType="+orderType+"&fromlab="+fromLab+"&studyId="+studyId+"&patientCode="+patientCode,"_self");

		// Modified by JM on 04March05
		//Modified by Manimaran on 29Aug08
		window.open("deleteLabs.jsp?searchFrom=search&srcmenu="+srcmenu+"&page="+pageName+"&selLabs="+selLabs+"&pkey="+pkey+"&patProtId="+patProtId+"&orderBy="+orderBy+"&orderType="+orderType+"&fromlab="+fromLab+"&studyId="+studyId+"&filterEnrDate="+filEnrDt+"&groupName="+groupId+"&labname="+labText+"&selStudyId="+selStudyId+"&abnresult="+abnText+"&patientCode="+patientCode+"&fromLab="+fromLab+"&specimenPk="+specPk+"&calledFromPg="+calledFrom,"_self");




			  }
			  else
				{
				  return false;
				}
		}

	  }
	  if(cnt==0)
		 {
		   alert("<%=MC.M_SelLabs_ToDel%>");/*alert("Please select Labs to be Deleted");*****/
		   return false;
		 }

	  formobj.selLabs.value=selLabs;

 }
 }
	</script>


<title><%=LC.L_Search_Pats%><%--Search Patients*****--%></title>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.DateUtil,com.velos.eres.business.common.*" %>
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.person.*,com.velos.eres.service.util.*,com.velos.eres.web.user.*,com.velos.eres.web.grpRights.GrpRightsJB"%>

<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="codelst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="labJB" scope="request" class="com.velos.eres.web.lab.LabJB"/>
<jsp:useBean id="userSite" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<% String src;
src= request.getParameter("srcmenu");
%>


<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1)
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {
%>
<body onUnload="javascript:if (windowname!='null') {windowname.close()}" onstyle="overflow:scroll;">
<%
	} else {
%>
<body onUnload="javascript:if (windowname!='null') {windowname.close()}">
<%
	}
%>

<!--<DIV class="browserDefault" id="div1"> -->
<%
  HttpSession tSession = request.getSession(true);
  if (sessionmaint.isValidSession(tSession)) {
  String groupId="",groupNamePullDown="",abnresult="",abnrFlag="",studyDD="",selStudyId="" ;
  String filEnrDt="",labText="",labStr="",groupStr="",abnText="",dateStr="",whereStr="",abnStr="",studyStr="";
  String specStr = "";
  String[] deleteIds;
  String whereSpecimenStr = "";



  userB = (com.velos.eres.web.user.UserJB) tSession.getValue("currentUser");
   	int pageRight = 0;
   	int labPageRight = 0;
   	int orgRight  = 0;
    String userId = (String) tSession.getValue("userId");



   	int usrId = EJBUtil.stringToNum(userId);

    String pkey=request.getParameter("pkey");
	int personPK=EJBUtil.stringToNum(pkey);


	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");
    pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));
    labPageRight = Integer.parseInt(grpRights.getFtrRightsByValue("PATLABS"));

     orgRight = userSite.getUserPatientFacilityRight(EJBUtil.stringToNum(userId), personPK);


	 //JM: 07Jul2009: #INVP2.11
	String calldFrom = request.getParameter("calledFromPg");
	if (EJBUtil.isEmpty(calldFrom))
		calldFrom = "";

	String specimenPk = request.getParameter("specimenPk");
	specimenPk = (specimenPk==null)?"":specimenPk;

 if ((pageRight>0 && labPageRight > 0 && orgRight > 0) || calldFrom.equals("specimen")) {
	Calendar cal = new GregorianCalendar();
	int pstudy = 0;
	String searchFilter ="" ;
    String selectedTab = "" ;
    selectedTab=request.getParameter("selectedTab");
    String uName = (String) tSession.getValue("userName");
    UserJB user = (UserJB) tSession.getValue("currentUser");

    String fromLab=request.getParameter("fromlab");
    fromLab=(fromLab==null)?"":fromLab;

    String frmLab=request.getParameter("frmLab");
    frmLab=(frmLab==null)?"":frmLab;

    String selectedStudy = request.getParameter("selectedStudy");
    String studyId=request.getParameter("studyId");
    studyId=(studyId==null)?"":studyId;
    selStudyId=request.getParameter("selStudyId");
    selStudyId=(selStudyId==null)?"":selStudyId;
    LabDao labdao=new LabDao();
	CodeDao cdLabs=new CodeDao();
	cdLabs.getCodeValues("abflag");

	String responseId=request.getParameter("responseId");
           responseId=(responseId==null)?"":responseId;

	String page1=request.getParameter("page");

	String patProtId=request.getParameter("patProtId");
	String pageRightPat = request.getParameter("pageRightPat");//KM
	String pageRightOrg = request.getParameter("pageRightOrg");
	String orgRightPat = request.getParameter("orgRightPat");

	patProtId=(patProtId==null)?"":patProtId;
     //in case string 'null'
    patProtId=(patProtId.equals("null"))?"":patProtId;


    PatStudyStatDao stDao=studyB.getPatientStudies(personPK,EJBUtil.stringToNum(userId));
	ArrayList arStudyIds=stDao.getStudyIds();
	ArrayList arStudyNums=stDao.getStudyNums();

    String studyIds = EJBUtil.ArrayListToString(arStudyIds);

//JM: 10Jul2009: caught an out of context error while doing #INVP2.11, if studyIds is '' then recodsets are not getting displayed(java.sql.SQLException: ORA-00604: error occurred at recursive SQL level 1)....
	if (studyIds.equals("")){
	  studyIds = "0";
	}


	if(selectedStudy.equals("default")){
	selStudyId = studyId;

	studyDD = EJBUtil.createPullDownWithStr("selStudyId", studyId, arStudyIds , arStudyNums );

	}
	else {

	studyDD = EJBUtil.createPullDown("selStudyId", EJBUtil.stringToNum(selStudyId), arStudyIds , arStudyNums );
	}


	String mode=request.getParameter("mode");
	groupId=request.getParameter("groupName");


	labText=request.getParameter("labname");
	abnText=request.getParameter("abnresult");
	cal= cal.getInstance();
	String patientCode = request.getParameter("patientCode");

	filEnrDt = request.getParameter("filterEnrDate") ;
	if (filEnrDt==null) filEnrDt="ALL";
	String dEnrFilter = EJBUtil.getRelativeTimeDD("filterEnrDate",filEnrDt);
	if (filEnrDt.equals("ALL")) dateStr="";
	else  {
	dateStr=EJBUtil.getRelativeTime (cal,filEnrDt);
	whereStr= " and test_date  "+dateStr;

	}

	if (patientCode  == null)
		patientCode = "";
	if (groupId==null) groupId="";
	if (labText==null) labText="";
	if (abnText==null) abnText="";

	if (selStudyId.length()>0) {
		studyStr=" and fk_study="+selStudyId +" ";
		whereStr=whereStr+studyStr;

	}
	else {
		studyStr = " and  ( fk_study in ("+ studyIds +" ) or fk_study is null) ";
		whereStr=whereStr+studyStr;
	}

    if (!calldFrom.equals("")){

		specStr = " and fk_specimen = "+specimenPk;
		whereStr = whereStr + specStr;
	}



	if (groupId.length()>0) {
		groupStr=" and fk_testgroup="+groupId +" ";
		whereStr=whereStr+groupStr;
		}
		// modified by Amarnadh for issue #3269
	if (labText.length()>0){
	//	labText=labText.toLowerCase();
		labStr=" and lower(labtest_name) like '%"+labText.toLowerCase().trim()+"%'" ;
		whereStr=whereStr+labStr;
		}
	if (abnText.length()>0){
		abnStr=" and fk_codelst_abflag="+abnText +" ";
		whereStr=whereStr+abnStr;
		}


//JM: 27Aug2009: 4200
if (calldFrom.equals("specimen")) {

	whereSpecimenStr = " or fk_per is null or fk_per is not null " ;
}
	abnresult=cdLabs.toPullDown("abnresult",EJBUtil.stringToNum(abnText));

	labdao=labJB.groupPullDown(groupId);
	groupNamePullDown=labdao.getPullDown();

	deleteIds=request.getParameterValues("Del");


 	String patCode = "";
 	String regBy ="", ptxt="All",atxt="All",gtxt="All",rtxt="All",rbytxt="All";
 	int lowLimit=0,highLimit=0 ;


String sql =  "SELECT  to_char(test_date,PKG_DATEUTIL.F_GET_DATEFORMAT) as testDate,pk_patlabs  , test_result,test_unit, (select codelst_subtyp from er_codelst where pk_codelst=fk_codelst_abflag ) as fk_codelst_abflag,  group_name, labtest_name," +
  		" (select codelst_desc from er_codelst where pk_codelst=fk_codelst_stdphase ) as fk_codelst_stdphase FROM er_patlabs, er_labgroup, er_labtest"+
		" WHERE     (fk_testgroup = pk_labgroup)     AND (fk_testid = pk_labtest) and (fk_per="+ pkey+ whereSpecimenStr +")  " + whereStr + " order by test_date desc ";


String countsql=  "select count(*) from (SELECT to_char(test_date,PKG_DATEUTIL.F_GET_DATEFORMAT) as testDate,pk_patlabs  , test_result, test_unit,(select codelst_subtyp from er_codelst where pk_codelst=fk_codelst_abflag ) as fk_codelst_abflag,  group_name, labtest_name," +
  		" (select codelst_desc from er_codelst where pk_codelst=fk_codelst_stdphase ) as fk_codelst_stdphase FROM er_patlabs, er_labgroup, er_labtest"+
		" WHERE     (fk_testgroup = pk_labgroup)     AND (fk_testid = pk_labtest) and (fk_per="+ pkey+ whereSpecimenStr +")  " + whereStr + " )";

			//System.out.println(sql);
			//System.out.println(countsql);

			String pagenum = "";
			int curPage = 0;
			long startPage = 1;
			long cntr = 0;
			String pagenumView = "";
			int curPageView = 0;
			long startPageView = 1;
			long cntrView = 0;

			pagenum = request.getParameter("pagenum");
			if (pagenum == null)
			{
			pagenum = "1";
			}
			curPage = EJBUtil.stringToNum(pagenum);

			String orderBy = "";
			orderBy = request.getParameter("orderBy");
			if (orderBy==null) orderBy="1";
			String orderType = "";
			orderType = request.getParameter("orderType");

			if (orderType == null)
			{
			orderType = "desc";
			}


			pagenumView = request.getParameter("pageView");
			if (pagenumView == null)
			{
			pagenumView = "1";
			}
			curPageView = EJBUtil.stringToNum(pagenumView);

			String orderByView = "";
			orderByView = request.getParameter("orderByView");

			String orderTypeView = "";
			orderTypeView = request.getParameter("orderTypeView");

			if (orderTypeView == null)
			{
			orderTypeView = "asc";
			}

			long rowsPerPage=0;
			long totalPages=0;
			long rowsReturned = 0;
			long showPages = 0;
			long totalRows = 0;
			long firstRec = 0;
			long lastRec = 0;
			boolean hasMore = false;
			boolean hasPrevious = false;

			Configuration conf = new Configuration();
			rowsPerPage = conf.getRowsPerBrowserPage(conf.ERES_HOME +"eresearch.xml");


			totalPages = conf.getTotalPagesPerBrowser(conf.ERES_HOME +"eresearch.xml");

			BrowserRows br = new BrowserRows();
			br.getPageRows(curPage,rowsPerPage,sql,totalPages,countsql,orderBy,orderType);
			rowsReturned = br.getRowReturned();
			showPages = br.getShowPages();

		     startPage = br.getStartPage();

			hasMore = br.getHasMore();

			hasPrevious = br.getHasPrevious();


			totalRows = br.getTotalRows();


			firstRec = br.getFirstRec();

			lastRec = br.getLastRec();

	String patStatSubType = "";
	patStatSubType = request.getParameter("patStatSubType");
	if (EJBUtil.isEmpty(patStatSubType))
		patStatSubType = "";



%>


<%if (fromLab.equals("pat")){%>
  <Form name="results" method="post" action="formfilledpatbrowser.jsp?pagenum=1&formPullDown=lab" onSubmit="return setText(document.results)">


<%} else if (fromLab.equals("patstd")){%>
  <Form name="results" method="post" action="formfilledstdpatbrowser.jsp?pagenum=1&formPullDown=lab"
  onSubmit="return setText(document.results)">

<%}else if (calldFrom.equals("specimen")){%>
 <Form name="results" method="post" action="specimenlabbrowser.jsp?pagenum=1&formPullDown=spec&specimenPk=<%=specimenPk%>&calledFromPg=<%=calldFrom%>"
  onSubmit="return setText(document.results)">

<%}%>

<input type="hidden" name="srcmenu" Value="<%=src%>">
<input type="hidden" name="searchFrom" Value="search">
<input type="hidden" name="pkey" Value="<%=pkey%>">
<input type="hidden" name="patProtId" Value="<%=patProtId%>">
<input type="hidden" name="mode" Value="<%=mode%>">
<input type="hidden" name="page" Value="<%=page1%>">
<input type="hidden" name="patientCode" Value="<%=patientCode%>">
<input type="hidden" name="studyId" Value="<%=studyId%>">
<input type="hidden" name="fromLab" Value="<%=fromLab%>">
<input type="hidden" name="totcount" Value="<%=rowsReturned%>">
<input type="hidden" name="selectedStudy" Value="<%=selStudyId%>">
<input type="hidden" name="selLabs" >

<input type="hidden" name="specimenPk" value="<%=specimenPk%>">
<input type="hidden" name="calledFromPg" value="<%=calldFrom%>">


  <table class = tableDefault width="98%" cellspacing="0" cellpadding="0" border=0 >

    <tr><td><%=LC.L_Labs_DoneOn%><%--Labs Done On*****--%></td>
	<td><%=LC.L_Category%><%--Category*****--%></td>
	<td><%=LC.L_Lab_Name%><%--Lab Name*****--%></td>
	<td><%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%></td>
	<td><%=LC.L_Abnormal_Results%><%--Abnormal Results*****--%></td>
	</tr>
    <tr>
	<td class=tdDefault><%=dEnrFilter%></td>
	<td class=tdDefault><%=groupNamePullDown%></td>
	<td class=tdDefault><input type="text" name="labname" Value="<%=labText%>" size="12" ></td>
	<td class=tdDefault> <%=studyDD%></td>
	<td class=tdDefault> <%=abnresult%> </td>

 	<td class=tdDefault width=10%> <button type="submit"><%=LC.L_Search%></button> </td>
    </tr>

  </table>
  <%
  String labname1="";
  labname1=request.getParameter("labname");
  %>

	<input type="hidden" name="labname" value="<%=labname1%>">


 <table align=center>
	<tr>
<%

	if (curPage==1) startPage=1;

    for (int count = 1; count <= showPages;count++)
	{
   cntr = (startPage - 1) + count;

	if ((count == 1) && (hasPrevious))
	{
    %>
	<td colspan = 2>
	<%if (fromLab.equals("pat")){%>
  	<A href="formfilledpatbrowser.jsp?selectedTab=10&srcmenu=<%=src%>&mode=<%=mode%>&page=<%=page1%>&pkey=<%=pkey%>&patProtId=<%=patProtId%>&pagenum=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&formPullDown=lab&studyId=<%=studyId%>&selStudyId=<%=selStudyId%>&patientCode=<%=patientCode%>&labname=<%=labText%>&filterEnrDate=<%=filEnrDt%>&abnresult=<%=abnText%>&groupName=<%=groupId%>&selectedStudy=<%=selStudyId%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;
	<%} else if (fromLab.equals("patstd")){%>
	<A href="formfilledstdpatbrowser.jsp?selectedTab=10&srcmenu=<%=src%>&mode=<%=mode%>&page=<%=page1%>&pkey=<%=pkey%>&patProtId=<%=patProtId%>&pagenum=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&formPullDown=lab&studyId=<%=studyId%>&selStudyId=<%=selStudyId%>&patientCode=<%=patientCode%>&filterEnrDate=<%=filEnrDt%>&labname=<%=labText%>&abnresult=<%=abnText%>&groupName=<%=groupId%>&selectedStudy=<%=selStudyId%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;
	<%}else if (calldFrom.equals("specimen")){%>
	<A href="specimenlabbrowser.jsp?selectedTab=1&specimenPk=<%=specimenPk%>&calledFromPg=<%=calldFrom%>&searchFrom=initial&srcmenu=<%=src%>&mode=<%=mode%>&page=<%=page1%>&pkey=<%=pkey%>&patProtId=<%=patProtId%>&pagenum=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&formPullDown=spec&studyId=<%=studyId%>&selStudyId=<%=selStudyId%>&patientCode=<%=patientCode%>&filterEnrDate=<%=filEnrDt%>&labname=<%=labText%>&abnresult=<%=abnText%>&groupName=<%=groupId%>&selectedStudy=<%=selStudyId%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;
	<%}%>
	</td>
	<%
  	}
	%>
	<td>
	<%

	 if (curPage  == cntr)
	 {
     %>
		<FONT class = "pageNumber"><%= cntr %></Font>
       <%
       }
      else
        {
	if (fromLab.equals("pat")){
       %>
       	  <A href="formfilledpatbrowser.jsp?selectedTab=10&searchFrom=initial&srcmenu=<%=src%>&mode=<%=mode%>&pkey=<%=pkey%>&patProtId=<%=patProtId%>&page=<%=page1%>&pagenum=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&formPullDown=lab&studyId=<%=studyId%>&selStudyId=<%=selStudyId%>&patientCode=<%=patientCode%>&filterEnrDate=<%=filEnrDt%>&labname=<%=labText%>&abnresult=<%=abnText%>&groupName=<%=groupId%>&selectedStudy=<%=selStudyId%>"><%= cntr%></A>
	  <%} else if (fromLab.equals("patstd")){%>
	    <A href="formfilledstdpatbrowser.jsp?selectedTab=10&searchFrom=initial&srcmenu=<%=src%>&mode=<%=mode%>&pkey=<%=pkey%>&patProtId=<%=patProtId%>&page=<%=page1%>&pagenum=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&formPullDown=lab&studyId=<%=studyId%>&selStudyId=<%=selStudyId%>&patientCode=<%=patientCode%>&filterEnrDate=<%=filEnrDt%>&labname=<%=labText%>&abnresult=<%=abnText%>&groupName=<%=groupId%>&selectedStudy=<%=selStudyId%>"><%= cntr%></A>

	  	<%}else if (calldFrom.equals("specimen")){%>
	     <A href="specimenlabbrowser.jsp?selectedTab=1&specimenPk=<%=specimenPk%>&calledFromPg=<%=calldFrom%>&searchFrom=initial&srcmenu=<%=src%>&mode=<%=mode%>&page=<%=page1%>&pkey=<%=pkey%>&patProtId=<%=patProtId%>&pagenum=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&formPullDown=spec&studyId=<%=studyId%>&selStudyId=<%=selStudyId%>&patientCode=<%=patientCode%>&filterEnrDate=<%=filEnrDt%>&labname=<%=labText%>&abnresult=<%=abnText%>&groupName=<%=groupId%>&selectedStudy=<%=selStudyId%>"><%= cntr%></A>

	    <%}
    	}
	 %>
	</td>
	<%
	  }

	if (hasMore)
	{
   %>
   <td colspan = 3 align = center>
   <%if (fromLab.equals("pat")){       %>
  	&nbsp;&nbsp;&nbsp;&nbsp;<A href="formfilledpatbrowser.jsp?selectedTab=10&searchFrom=initial&srcmenu=<%=src%>&mode=<%=mode%>&pkey=<%=pkey%>&patProtId=<%=patProtId%>&page=<%=page1%>&pagenum=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&formPullDown=lab&studyId=<%=studyId%>&selStudyId=<%=selStudyId%>&patientCode=<%=patientCode%>&filterEnrDate=<%=filEnrDt%>&labname=<%=labText%>&abnresult=<%=abnText%>&groupName=<%=groupId%>&selectedStudy=<%=selStudyId%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
    <%} else if (fromLab.equals("patstd")){%>
	&nbsp;&nbsp;&nbsp;&nbsp;<A href="formfilledstdpatbrowser.jsp?selectedTab=10&searchFrom=initial&srcmenu=<%=src%>&mode=<%=mode%>&pkey=<%=pkey%>&patProtId=<%=patProtId%>&page=<%=page1%>&pagenum=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&formPullDown=lab&studyId=<%=studyId%>&selStudyId=<%=selStudyId%>&patientCode=<%=patientCode%>&filterEnrDate=<%=filEnrDt%>&labname=<%=labText%>&abnresult=<%=abnText%>&groupName=<%=groupId%>&selectedStudy=<%=selStudyId%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
	<%}else if (calldFrom.equals("specimen")){%>
	&nbsp;&nbsp;&nbsp;&nbsp;<A href="specimenlabbrowser.jsp?selectedTab=1&specimenPk=<%=specimenPk%>&calledFromPg=<%=calldFrom%>&searchFrom=initial&srcmenu=<%=src%>&mode=<%=mode%>&page=<%=page1%>&pkey=<%=pkey%>&patProtId=<%=patProtId%>&pagenum=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&formPullDown=spec&studyId=<%=studyId%>&selStudyId=<%=selStudyId%>&patientCode=<%=patientCode%>&filterEnrDate=<%=filEnrDt%>&labname=<%=labText%>&abnresult=<%=abnText%>&groupName=<%=groupId%>&selectedStudy=<%=selStudyId%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>

	<%}%>
	</td>
	<%
  	}
	%>

   </tr>
  </table>
<table class=tableDefault width="98%" border=0>
<tr height="30">
<td width="98%" align="right">
	   <%
		//Modified by Manimaran for November Enhancement PS4.
		if( !(patStatSubType.trim().equalsIgnoreCase("lockdown")) && ((labPageRight >= 6 && orgRight >= 6 ) || calldFrom.equals("specimen")) )
		{ %>
				<A href="#" onClick="deleteLabs(document.results,<%=pageRightPat%>,<%=pageRightOrg%>,<%=orgRightPat%>, '<%=specimenPk%>', '<%=calldFrom%>')"><img src="./images/DeleteSelected.gif" title="<%=LC.L_Del_Selected%>" alt="<%=LC.L_Del_Selected %><%--Delete Multiple*****--%>" border ="0" align="absbotton"><%//=LC.L_Del_Selected%><%--DELETE SELECTED*****--%></A>
	   	<%
	   	}
	   	else
	   	{
	   		%>
	   			&nbsp;
	   		<%
	   	}
	    %>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <%
   //Modified by Manimaran for November Enhancement PS4.
   if(!(patStatSubType.trim().equalsIgnoreCase("lockdown")) && ((labPageRight >= 6 && orgRight >= 6) || calldFrom.equals("specimen")))
   {
 %>
	<!-- Modified by JM on 08Mar05 -->
   <A href="#" onClick="openLabSelect('labSelection.jsp?mode=<%=mode%>&specimenPk=<%=specimenPk%>&calledFromPg=<%=calldFrom%>&patProtId=<%=patProtId%>&patientCode=<%=patientCode%>&pkey=<%=pkey%>&page=<%=page1%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>&fromlab=<%=fromLab%>&filterEnrDate=<%=filEnrDt%>&groupName1=<%=groupId%>&labname=<%=labText%>&selStudyId=<%=selStudyId%>&abnresult=<%=abnText%>&pagenum=<%=pagenum%>',document.results,<%=pageRightPat%>,<%=pageRightOrg%>,<%=orgRightPat%>)"><img title="<%=LC.L_Add_New%>" src="./images/Create.gif" border ="0"><%//=LC.L_Add_New%><%--ADD NEW*****--%></A> <!--KM-->
<%
	}
  %>
<!--<A href="labdata.jsp?mode=<%=mode%>&patProtId=<%=patProtId%>&pkey=<%=pkey%>&page=<%=page1%>&srcmenu=tdmenubaritem5&selectedTab=1">Add New Lab</A>-->
</td>
</tr>
</table>
<table class=tableDefault width="100%" border=0>

    <tr>
         	<th class=tdDefault width=10%><%=LC.L_Date%><%--Date*****--%></th>

    <th class=tdDefault width=15%><%=LC.L_Category%><%--Category*****--%></th>

	<th class=tdDefault width=25%><%=LC.L_Lab_Name%><%--Lab Name*****--%></th>

  	<th class=tdDefault width=10%><%=LC.L_Test_Results%><%--Test Results*****--%></th>

  	<th class=tdDefault width=10%><%=LC.L_Units%><%--Units*****--%></th>
  	<th class=tdDefault width=10%><%=LC.L_Std_Phase%><%--Study Phase*****--%></th>
  	<th class=tdDefault width=10%><%=LC.L_Result_Type%><%--Result Type*****--%></th>
	<th class=tdDefault width=10%><%=LC.L_Edit%><%--Edit*****--%></th>
	<th class=tdDefault width=5%><input type="checkbox" name="All" value="" onClick="checkAll(document.results)"></th>



   </tr>

<!--</table> -->
<!--</Form>-->

<input type="hidden" name="selectedTab" Value=<%=selectedTab%>>
<Input type="hidden" name="orderBy" value="<%=orderBy%>">
<Input type="hidden" name="pagenum" value="<%=curPage%>">
<Input type="hidden" name="orderType" value="<%=orderType%>">
<input type="hidden" name="ptxt">
<input type="hidden" name="atxt">
<input type="hidden" name="gtxt">
<input type="hidden" name="rtxt">
<input type="hidden" name="rbytxt">




<%
//Retrieve study count for this user


	ArrayList studyList=new ArrayList();



	if (totalRows <= 0 ) {

%>
<tr><td colspan="8" align="center"><BR>
	<b><%=MC.M_NoLabTests_SrchCrit%><%--No Lab Tests found for your search criteria. Please modify your criteria and try again.*****--%></b>
</td></tr>
<%

	} else {

	   int i = 0;


	   if(rowsReturned == 0) {

%>
<tr><td colspan="8" align="center"><BR>
	<b><%=MC.M_NoLabTests_SrchCrit%><%--No Lab Tests found for your search criteria. Please modify your criteria and try again.*****--%></b>
</td></tr>
<%

	} else {



	   String patientId = "";
       String studyNumber="" ;
       String totalEnroll="" ;



	%>





<!-- <table class=tableDefault width="100%" border=0> -->
<%


        String tempPercode="" ;
        String creator="" ;
            int study_counter = 0 ;
            String enrollDate="";
            String activecount = "" ,tercount="",comcount="",suscount="",dropcount="";
	    String labDate="",category="",labName="",testResult="",units="",abrflag="",labPk="",stdPhase="";
             String studypk="";
		for(i = 1 ; i <=rowsReturned ; i++)

	  	{

	  	   labDate = br.getBValues(i,"testDate");
	  	   labName = br.getBValues(i,"labtest_name");
		   category=  br.getBValues(i,"group_name");
		   testResult= br.getBValues(i,"test_result");
		   units=    br.getBValues(i,"test_unit") ;
		   abrflag=   br.getBValues(i,"fk_codelst_abflag") ;
		   labPk= br.getBValues(i,"pk_patlabs") ;
		   stdPhase=br.getBValues(i,"fk_codelst_stdphase");
		   stdPhase=(stdPhase==null)?"-":stdPhase;

	   if (testResult==null) testResult="-";
	  if (units==null) units="-";



	 		   if ((i%2)==0) {

%>

      <tr class="browserEvenRow">

<%

	 		   }else{

%>

      <tr class="browserOddRow">

<%

	 		   }

%>

        <td width =10%><%=labDate%></td>
        <td width =15%><%=category%></td>
        <td width =25%><%=labName%></td>
        <td width =10%><%=testResult%></td>
        <td width =10%><%=units%></td>
	<td width =10%><%=stdPhase%></td>

	<td width ="10%" align="center">
	<%
	if (abrflag!=null) {
	 if ((abrflag.toUpperCase()).trim().equals("N")){%>
	<img src="../images/jpg/Normal.jpg"  title="<%=LC.L_Normal%>" border="0"></img>
        <%}else{%>
	<img src="../images/jpg/Abnormal.jpg" title="<%=LC.L_Abnormal%>" border="0"></img>
	<%}}%>
	</td>
        <td width=15%>
        <%
	   //Modified by Manimaran for November Enhancement PS4.
	   if(!(patStatSubType.trim().equalsIgnoreCase("lockdown")))
   		{
		 %>
	<!-- Modified by Manimaran to fix the bugs 2387 and 2388 -->
	<A href="labmodify.jsp?pk_lab=<%=labPk%>&specimenPk=<%=specimenPk%>&calledFromPg=<%=calldFrom%>&testName=<%=StringUtil.encodeString(labName)%>&mode=<%=mode%>&patProtId=<%=patProtId%>&pkey=<%=pkey%>&page=<%=page1%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&patientCode=<%=patientCode%>&studyId=<%=studyId%>&selStudyId=<%=selStudyId%>&fromlab=<%=fromLab%>&filterEnrDate=<%=filEnrDt%>&groupName=<%=groupId%>&labname=<%=labText%>&abnresult1=<%=abnText%>&pagenum=<%=pagenum%>" onClick ="return editLabs(<%=pageRightPat%>,<%=pageRightOrg%>,<%=orgRightPat%>)" ><img title="<%=LC.L_Edit%>" src="./images/edit.gif" border ="0"><%//=LC.L_Edit%><%--Edit*****--%></A>
		<%
		}
		else
	   	{
	   		%>
	   			&nbsp;
	   		<%
	   	}
	    %>
        </td>
        	<td width=5%>&nbsp;&nbsp;<input type="checkbox" name="Del" value="<%=labPk%>"></td>

     </tr>

	  <%

	  }// end for for loop

	  } // end of if for length == 0

  } //end of if for the check of patientResults == null

	  %>

    </table>
    	<table>
	<tr>
	<td>
	<% if (totalRows > 0) { Object[] arguments = {firstRec,lastRec,totalRows}; %>
		<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%--<font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
	<%} %>
	</td>
	</tr>
	</table>

	<table align=center>
	<tr>
<%

	if (curPage==1) startPage=1;

    for (int count = 1; count <= showPages;count++)
	{
   cntr = (startPage - 1) + count;

	if ((count == 1) && (hasPrevious))
	{
    %>
	<td colspan=2>
	<%if (fromLab.equals("pat")){%>
  	<A href="formfilledpatbrowser.jsp?selectedTab=10&srcmenu=<%=src%>&mode=<%=mode%>&page=<%=page1%>&pkey=<%=pkey%>&patProtId=<%=patProtId%>&pagenum=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&formPullDown=lab&studyId=<%=studyId%>&selStudyId=<%=selStudyId%>&patientCode=<%=patientCode%>&filterEnrDate=<%=filEnrDt%>&labname=<%=labText%>&abnresult=<%=abnText%>&groupName=<%=groupId%>&selectedStudy=<%=selStudyId%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;
	<%} else if (fromLab.equals("patstd")){%>
	<A href="formfilledstdpatbrowser.jsp?selectedTab=10&srcmenu=<%=src%>&mode=<%=mode%>&page=<%=page1%>&pkey=<%=pkey%>&patProtId=<%=patProtId%>&pagenum=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&formPullDown=lab&studyId=<%=studyId%>&selStudyId=<%=selStudyId%>&patientCode=<%=patientCode%>&filterEnrDate=<%=filEnrDt%>&labname=<%=labText%>&abnresult=<%=abnText%>&groupName=<%=groupId%>&selectedStudy=<%=selStudyId%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;
	<%}else if (calldFrom.equals("specimen")){%>
	<A href="specimenlabbrowser.jsp?selectedTab=1&specimenPk=<%=specimenPk%>&calledFromPg=<%=calldFrom%>&searchFrom=initial&srcmenu=<%=src%>&mode=<%=mode%>&page=<%=page1%>&pkey=<%=pkey%>&patProtId=<%=patProtId%>&pagenum=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&formPullDown=spec&studyId=<%=studyId%>&selStudyId=<%=selStudyId%>&patientCode=<%=patientCode%>&filterEnrDate=<%=filEnrDt%>&labname=<%=labText%>&abnresult=<%=abnText%>&groupName=<%=groupId%>&selectedStudy=<%=selStudyId%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;
	<%}%>
	</td>
	<%
  	}
	%>
	<td>
	<%

	 if (curPage  == cntr)
	 {
     %>
		<FONT class = "pageNumber"><%= cntr %></Font>
       <%
       }
      else
        {
       %>
       <%if (fromLab.equals("pat")){ 	%>
	   <A href="formfilledpatbrowser.jsp?selectedTab=10&searchFrom=initial&srcmenu=<%=src%>&mode=<%=mode%>&pkey=<%=pkey%>&patProtId=<%=patProtId%>&page=<%=page1%>&pagenum=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&formPullDown=lab&studyId=<%=studyId%>&selStudyId=<%=selStudyId%>&patientCode=<%=patientCode%>&filterEnrDate=<%=filEnrDt%>&labname=<%=labText%>&abnresult=<%=abnText%>&groupName=<%=groupId%>&selectedStudy=<%=selStudyId%>"><%= cntr%></A>
	   <%} else if (fromLab.equals("patstd")){%>
	   <A href="formfilledstdpatbrowser.jsp?selectedTab=10&searchFrom=initial&srcmenu=<%=src%>&mode=<%=mode%>&pkey=<%=pkey%>&patProtId=<%=patProtId%>&page=<%=page1%>&pagenum=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&formPullDown=lab&studyId=<%=studyId%>&selStudyId=<%=selStudyId%>&patientCode=<%=patientCode%>&filterEnrDate=<%=filEnrDt%>&labname=<%=labText%>&abnresult=<%=abnText%>&groupName=<%=groupId%>&selectedStudy=<%=selStudyId%>"><%= cntr%></A>
	   <%}else if (calldFrom.equals("specimen")){%>
	   <A href="specimenlabbrowser.jsp?selectedTab=1&specimenPk=<%=specimenPk%>&calledFromPg=<%=calldFrom%>&searchFrom=initial&srcmenu=<%=src%>&mode=<%=mode%>&page=<%=page1%>&pkey=<%=pkey%>&patProtId=<%=patProtId%>&pagenum=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&formPullDown=spec&studyId=<%=studyId%>&selStudyId=<%=selStudyId%>&patientCode=<%=patientCode%>&filterEnrDate=<%=filEnrDt%>&labname=<%=labText%>&abnresult=<%=abnText%>&groupName=<%=groupId%>&selectedStudy=<%=selStudyId%>"><%= cntr%></A>
	   <%}%>
       <%
    	}
	 %>
	</td>
	<%
	  }

	if (hasMore)
	{
   %>
   <td colspan = 3 align = center>
   <%if (fromLab.equals("pat")){%>
  	&nbsp;&nbsp;&nbsp;&nbsp;<A href="formfilledpatbrowser.jsp?selectedTab=10&searchFrom=initial&srcmenu=<%=src%>&mode=<%=mode%>&pkey=<%=pkey%>&patProtId=<%=patProtId%>&page=<%=page1%>&pagenum=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&formPullDown=lab&studyId=<%=studyId%>&selStudyId=<%=selStudyId%>&patientCode=<%=patientCode%>&filterEnrDate=<%=filEnrDt%>&labname=<%=labText%>&abnresult=<%=abnText%>&groupName=<%=groupId%>&selectedStudy=<%=selStudyId%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
	<%} else if (fromLab.equals("patstd")){%>
	&nbsp;&nbsp;&nbsp;&nbsp;<A href="formfilledstdpatbrowser.jsp?selectedTab=10&searchFrom=initial&srcmenu=<%=src%>&mode=<%=mode%>&pkey=<%=pkey%>&patProtId=<%=patProtId%>&page=<%=page1%>&pagenum=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&formPullDown=lab&studyId=<%=studyId%>&selStudyId=<%=selStudyId%>&patientCode=<%=patientCode%>&filterEnrDate=<%=filEnrDt%>&labname=<%=labText%>&abnresult=<%=abnText%>&groupName=<%=groupId%>&selectedStudy=<%=selStudyId%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
	<%}else if (calldFrom.equals("specimen")){%>
	&nbsp;&nbsp;&nbsp;&nbsp;<A href="specimenlabbrowser.jsp?selectedTab=1&specimenPk=<%=specimenPk%>&calledFromPg=<%=calldFrom%>&searchFrom=initial&srcmenu=<%=src%>&mode=<%=mode%>&page=<%=page1%>&pkey=<%=pkey%>&patProtId=<%=patProtId%>&pagenum=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&formPullDown=spec&studyId=<%=studyId%>&selStudyId=<%=selStudyId%>&patientCode=<%=patientCode%>&filterEnrDate=<%=filEnrDt%>&labname=<%=labText%>&abnresult=<%=abnText%>&groupName=<%=groupId%>&selectedStudy=<%=selStudyId%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
	<%}%>
	</td>
	<%
  	}
	%>
   </tr>
  </table>





  </Form>



 <% }	//} //end of if for page right
else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right



}//end of if body for session

%>
</body>

</html>

