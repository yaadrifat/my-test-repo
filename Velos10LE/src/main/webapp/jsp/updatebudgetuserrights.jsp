<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="budgetUsersB" scope="request" class="com.velos.esch.web.budgetUsers.BudgetUsersJB"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.MC"%>
<%
int ret = 0;
int count = 0;
String src=request.getParameter("srcmenu");
String rights[]=null;
String rght="";
String budgetId = request.getParameter("budgetId");
String bgtUsrId = request.getParameter("bgtUsrId");
int totrows = EJBUtil.stringToNum(request.getParameter("totalrows"));
String selectedTab = request.getParameter("selectedTab");
String budgetType=request.getParameter("budgetType");
String budgetTemplate=request.getParameter("budgetTemplate");
String mode=request.getParameter("mode");

String eSign = request.getParameter("eSign");
HttpSession tSession = request.getSession(true);
  
if (sessionmaint.isValidSession(tSession))  {	
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
   	String oldESign = (String) tSession.getValue("eSign");
	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {

	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");

	if (totrows > 1){
		rights = request.getParameterValues("rights");
		for (count=0;count<totrows;count++){
			rght = rght + String.valueOf(rights[count]);
		}
	}else{
		rght = request.getParameter("rights");
	}
	budgetUsersB.setBgtUsersId(EJBUtil.stringToNum(bgtUsrId));
	budgetUsersB.getBudgetUsersDetails(); 	
	
	budgetUsersB.setModifiedBy(usr);
	budgetUsersB.setIpAdd(ipAdd);	
	
    budgetUsersB.setBgtUsersRights(rght);
	
    budgetUsersB.updateBudgetUsers();	   
%>
<br>
<br>
<br>
<br>
<br>
<% if (ret >= 0 ) {%>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%-- Data was saved successfully*****--%> </p>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=budgetrights.jsp?srcmenu=<%=src%>&budgetId=<%=budgetId%>&selectedTab=<%=selectedTab%>&budgetType=<%=budgetType%>&mode=<%=mode%>&budgetTemplate=<%=budgetTemplate%>">
	
<% 
	} else {
%>
<p class = "successfulmsg" align = center> <%=MC.M_Data_NotSaved%><%-- Data not saved*****--%> </p>	
<%
	}
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





