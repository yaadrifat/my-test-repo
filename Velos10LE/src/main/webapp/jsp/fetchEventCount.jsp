<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.esch.business.common.ScheduleDao"%>
<%@page import="com.velos.eres.service.util.LC"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
response.setDateHeader("Expires", 0);
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession)) {
	int count=0;
	ScheduleDao schDao = new ScheduleDao();
	String visitId = request.getParameter("fk_visit");
	String patProtId = request.getParameter("patProtId");
	count= schDao.getEventsCount(visitId,patProtId);
    out.println("<p>"+LC.L_Loading+" "+count+" Events...<img class=\"asIsImage\" src=\"../images/jpg/loading_pg.gif\" /></p>");
} else { // else of valid session
%>
<%
} // end of else of valid session
%>

