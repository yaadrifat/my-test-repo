dashboardUtils = {
    fetchCodeLstItems: {},
    fetchFormStatues: {}
}
dashboardUtils.fetchCodeLstItems = function (codelstType, fnToBeExecuted) {
    $.ajax({
        url: urls.dashboardUtils.fetchCodeLstItems,
        type: 'GET',
        global: true,
        dataType: 'json',
        async: false,
        data: {
            'codelstType': codelstType
        },
        cache: true,
        success: function (data) {
            fnToBeExecuted(data);
        },
        error: function (xhr, status, errorThrown) {
            console.log('errorThrown  ' + errorThrown);
            return null;
        }
    });
}

dashboardUtils.fetchFormStatues = function (codelstType, tabType,studyNum, fnToBeExecuted) {
    $.ajax({
        url: urls.dashboardUtils.fetchFormAdvStatus,
        type: 'GET',
        global: true,
        dataType: 'json',
        async: false,
        data: {
            'codelstType': codelstType,
            'tabType':tabType,
            'studyNum':studyNum
        },
        cache: true,
        success: function (data) {
            fnToBeExecuted(data);
        },
        error: function (xhr, status, errorThrown) {
            console.log('errorThrown  ' + errorThrown);
            return null;
        }
    });
}