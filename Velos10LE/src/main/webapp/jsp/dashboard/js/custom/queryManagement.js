 var oTable='';
 var moduleSaved='';
 var tabClicked = 0;
 var displayLength=10;
 var displayStart=0;
 var sortCol=3;
 var sortDir='desc';
 var iDisp
 var Search_Widget=L_Search_Widget;
 var Save_As=L_Save_As;
 var Save_Search=L_Save_Search;
 var Saved_Searches=L_Saved_Searches;
 var Delete_Searh=L_Delete_Searh;
 var Reset_Search=L_Reset_Search;
 var dataCount=0;
 var formType='';
 var formTyp='';
 var dataRowIndex=0;
 var studyTeamrole='';
 var filterStudyTRole='';
 var clearStudyRoleForTab=0;
 var filterEmtyFlag=0;
 var tab0ShowAndHide = [];
 var tab1ShowAndHide = [];
 var tab2ShowAndHide = [];
 var tab3ShowAndHide = [];
 var temptabSelected = -1;
 var alertFlag=0;
queryManagement = {
    renderPage: {},
    pageLoaderShow: {},
    pageLoaderHide: {},
    renderToggleButton: {},
    renderFormResponseToggle: {},
    renderStatusFilters :{},
    renderSavedSearches :{},
    setUpStdAutoComplete :{},
    setUpOrgAutoComplete :{},
    setUpFormAutoComplete :{},
    setUpFormStatAutoComplete :{},
    setUpAdvEveStatAutoComplete :{},
    setUpPatIDAutoComplete :{},
    setUpPatStdIDAutoComplete :{},
    setUpCalAutoComplete :{},
    setUpVisitAutoComplete :{},
    setUpEventAutoComplete :{},
    setUpQryCrtAutoComplete :{},
    setUpQryStsAutoComplete :{},
    renderFormQueryDataTable :{},
    renderAdvEventQueriesDataTable :{},
    renderPatStdStatDataTable :{},
    renderpatDemographicsDataTable:{},
    renderSavedView:{},
    getSearchFilter:{},
    renderUpdateDiv :{},
    saveSearchFilter:{},
    renderSaveViewButton :{},
    renderFormStatuesDD :{},
    clearFilters:{},
    renderSearchTab:{},
    renderFromSelected:{},
    urlParameterFormat:{},
    searchCriteriaArr:{},
    pushData:{},
    formResAndQueryView:{},
    patStudyResAndQuery:{},
    moveFormRes:{},
    patStudyQuery:{},
    hideExcelButton:{},
    formResponseAudit:{},
    //formResponseTrackChanges:{}, Remove in bug no#23196 By Amit
    accountFormAndQuery:{},
    deleteSaveSearch:{},
    removeSelect2Filter:{},
    sameTeamRoleforStudy:{},
    defaultElemt:{},
    commonFiltersSetting:{},
    latestTeamroleForMultipleStudy:{},
    refreshPage:{},
    defaultStRoleForSuperUser:{},
    decodeUrlParam:{},
    encodeUrlParam:{}
}
queryManagement.renderPage = function() {
    $("#pageContent").load(
       urls.queryManagement.template, function () {
    	   var source   = $("#pageContent").html();
       	   var template = Handlebars.compile(source);
       	   var html    = template(LC);
       	   $("#pageContent").html(html);
            $('#statusFiltersDIV, #savedSearchDIV').accordion({
                collapsible: true,
                heightStyle: 'content',
                animate: false
            });
            $("#queryTabs").tabs();
            queryManagement.pageLoaderShow();
            queryManagement.renderStatusFilters();
            queryManagement.renderSavedSearches();
            queryManagement.setUpStdAutoComplete();
            queryManagement.setUpOrgAutoComplete();
            queryManagement.setUpFormAutoComplete();
            queryManagement.setUpFormStatAutoComplete();
            queryManagement.setUpAdvEveStatAutoComplete();
            queryManagement.setUpPatIDAutoComplete();
            queryManagement.setUpPatStdIDAutoComplete();
            queryManagement.setUpCalAutoComplete();
            queryManagement.setUpVisitAutoComplete();
            queryManagement.setUpEventAutoComplete();
            queryManagement.setUpQryCrtAutoComplete();
            queryManagement.setUpQryStsAutoComplete();
            queryManagement.renderSavedView();
            //queryManagement.renderFormQueryDataTable();
            //queryManagement.renderUpdateDiv('#formQueriesTable_filter');
            openCal();
            queryManagement.pageLoaderHide();
        });
}
//   
function getStatusRed(obj){
	var redflg = document.getElementById('redflg').value;
	var red=document.getElementsByName('red');
	var i;
	for(i=0;i<red.length;i++){
		red[i].title=MC.M_Priority_1;
	}
	
	messageBox.style.top=obj.offsetTop
	messageBox.style.left=obj.offsetLeft+obj.offsetWidth+5
	contents.innerHTML=redflg+"<p>"+obj.href
	messageBox.style.display="block"
			 }

function getStatusOrange(){
var orngflg=document.getElementById('orngflg').value;
var orange=document.getElementsByName('orange');
var i;
for(i=0;i<orange.length;i++)
orange[i].title=MC.M_Priority_2
		}

function getStatusYellow(){
var yelloflg=document.getElementById('yelloflg').value;
var yellow=document.getElementsByName('yellow');
var i;
for(i=0;i<yellow.length;i++)
	yellow[i].title=MC.M_Priority_3
		}

function getStatusPurple(){
var prplflg=document.getElementById('prplflg').value;
var prpl=document.getElementsByName('prpl');
var i;
for(i=0;i<prpl.length;i++)
     prpl[i].title=MC.M_Priority_4
		}	

function getStatusGreen(){
var greenflg=document.getElementById('greenflg').value;
var green=document.getElementsByName('green');
var i;
for(i=0;i<green.length;i++)
    green[i].title=MC.M_Priority_6; 
		}

function getStatusWhite(){
var whiteflg=document.getElementById('whiteflg').value;
var white=document.getElementsByName('white');
var i;
for(i=0;i<white.length;i++)
    white[i].title=MC.M_Priority_5; 
		}

function getPK(){
	var tabSelected =  $("#queryTabs").tabs('option', 'active');
	if(tabSelected==0){
	  queryManagement.pageLoaderShow();
     $("#formQueriesTable").dataTable().fnDestroy(); 
 	 $("#formQueriesTable").empty();
 	addformQueryTablenodes();
 	queryManagement.renderFormQueryDataTable();
 	queryManagement.renderUpdateDiv('#formQueriesTable_filter');
  	queryManagement.pageLoaderHide();
	$('#advEveTabHeader').html('');
  	$('#patStdStatTabHeader').html('');
	}
	if(tabSelected==1){
		queryManagement.pageLoaderShow();
		 $("#adverseEventsTable").dataTable().fnDestroy(); 
	 	 $("#adverseEventsTable").empty();
	 	adverseEventsTablenodes();
	 	queryManagement.renderAdvEventQueriesDataTable();
	 	queryManagement.renderUpdateDiv('#adverseEventsTable_filter');
    	queryManagement.pageLoaderHide();
    	$('#formRespTabHeader').html('');
      	$('#patStdStatTabHeader').html('');
	}
	if(tabSelected==2){
		queryManagement.pageLoaderShow();
		 $("#patStdStatTable").dataTable().fnDestroy(); 
	 	 $("#patStdStatTable").empty();
	 	patStdStatTablenodes();
	 	queryManagement.renderPatStdStatDataTable();
	 	 queryManagement.pageLoaderHide();
	 	$('#formRespTabHeader').html('');
		$('#advEveTabHeader').html('');
	}
             }
queryManagement.pageLoaderShow = function () {
	$('#sideMenuTogleDIV').html('');
    $('#pageContent').css("backgroundColor", "#E6E6E6");
    $('#pageContent').css("opacity", 5);
    document.getElementById('spinner').style.display = 'block';
}
queryManagement.pageLoaderHide = function () {
	document.getElementById('spinner').style.display = 'none';
	queryManagement.renderToggleButton();
}

queryManagement.renderStatusFilters = function () {
    dashboardUtils
        .fetchCodeLstItems(
            'priority',
            function (jsonData) {
                var html = '';
                var htmlred='';
                var htmlorange='';
                var htmlyellow='';
                var htmlpurple='';
                var htmlwhite='';
                var htmlgreen='';
                $.each(
                        jsonData,
                        function () { 
                           
                        	var checkOpnFlg="";
                        	var checkCloseFlg="";
                        	html += '<label style="background-color:' + this.colorcode + '">'
                        	 if(this.REDICONDISP == "SHOWRED") 
                        	 {                      		
                        	 htmlred += '<input type="checkbox"  id="' + this.PK_CODELST +'" name="qryStatusRed" value="'+this.CODELST_DESC +'" onclick="getPK();" ></input><a href="#"><img src="./images/redflag.jpg"  border="0" title="'+MC.M_Priority_1+'"></a>'+this.CODELST_DESC+' &nbsp;&nbsp;'
                        	 }                        	
                        	 if(this.ORANGICONDISP == "SHOWORANGE")
                        	 {                       		
                        	 htmlorange += '<input type="checkbox"  id="' + this.PK_CODELST +'" name="qryStatusOrange" value="'+this.CODELST_DESC +'" onclick="getPK();" ><a href="#"><img src="./images/orangflg.jpg"  border="0" title="'+MC.M_Priority_2+'"></a>'+this.CODELST_DESC+' &nbsp;&nbsp;'
                        	 }
                        	 if(this.YELLOWICONDISP == "SHOWYELLOW")
                        	 {                        		
                        	 htmlyellow +=  '<input type="checkbox"  id="' + this.PK_CODELST + '" name="qryStatusYellow" value="'+this.CODELST_DESC +'" onclick="getPK();" ><a href="#"><img src="./images/yellowflg.jpg"  border="0" title="'+MC.M_Priority_3+'"></a>'+this.CODELST_DESC+' &nbsp;&nbsp;'
                        	 }                        	 
                        	 if(this.PURPLICONDISP == "SHOWPURPLE")
                        	 {                        		
                        	 htmlpurple +=  '<input type="checkbox"  id="' + this.PK_CODELST + '" name="qryStatusPurple" value="'+this.CODELST_DESC +'" onclick="getPK();" ><a href="#"><img src="./images/purplflg.jpg"  border="0" title="'+MC.M_Priority_4+'"></a>'+this.CODELST_DESC+' &nbsp;&nbsp;<br/>' 
                        	 }                      	
                        	if(this.GREENICONDISP == "SHOWGREEN")
                           	 {                          		
                           	htmlgreen += '<input type="checkbox"  id="' + this.PK_CODELST + '" name="qryStatusGreen" value="'+this.CODELST_DESC +'" onclick="getPK();" ><a href="#"><img src="./images/greenflg.jpg"  border="0" title="'+MC.M_Priority_6+'"></a>'+this.CODELST_DESC+'&nbsp;&nbsp;<br/>'
                           	 }
                        	if(this.WHITICONDISP == "SHOWHITE")
                     	     {                      		
                     	    htmlwhite += '<input type="checkbox"  id="' + this.PK_CODELST + '" name="qryStatusWhite" value="'+this.CODELST_DESC +'" onclick="getPK();" ><a href="#"><img src="./images/white.jpg"  border="0" title="'+MC.M_Priority_5+'"></a>'+this.CODELST_DESC+'&nbsp;&nbsp;<br/>'
                     	    /*End Bug #21625*/
                     	     } 
                        	html=''
                        	html += htmlred+htmlpurple+htmlorange+htmlwhite+htmlyellow+htmlgreen
                        	html += '</label><br/>'
                        	 
                        	 
                        		 var tabSelected =  $("#queryTabs").tabs('option', 'active');
                             
                        });
                $('#statusFiltersContentDIV').append(html); 
            });
}

queryManagement.renderToggleButton = function () {
       	 	var html ='';
        	html += '<a id = "btnSideMenuToggleDIV" style="padding-left: 10px;" href="#"><img id="toggleGadgetList" title="Show Gadget List" src="../images/jpg/ListCollapse.jpg" style="border:none;"></a>'
        	$('#sideMenuTogleDIV').html('');
        	$('#sideMenuTogleDIV').append(html);
            var showOrHide = false;
            $("#btnSideMenuToggleDIV").click(function () {
              $("#searchPanelDIV").toggle(showOrHide);
              		      if ( showOrHide === true ) {
      		  $( "#searchPanelDIV" ).show();
      		  $( "#dashBoardContentDIV" ).attr("style","width:84%;");
      		  $("#toggleGadgetList").attr('src',"../images/jpg/ListCollapse.jpg");
      		  showOrHide = false;
      		} else if ( showOrHide === false ) {
      		  $( "#searchPanelDIV" ).hide();
      		  $( "#dashBoardContentDIV" ).attr("style","width:100%;");
      		  $("#toggleGadgetList").attr('src',"../images/jpg/ListExpand.jpg");
      		  showOrHide = true;
      		}
            });
    }

queryManagement.renderFormResponseToggle = function () {

	var showOrHide = false;
	$("#toggleFormResponseDIV").click(function () {
		$("#previewTDLeft").toggle(showOrHide);
		if ( showOrHide === true ) {
			$( "#previewTDLeft" ).attr("style","width:100%;");
			$( "#previewTDRight" ).hide();
			$("#toggleFormResponses").attr('src',"../images/jpg/ListCollapse.jpg");
			showOrHide = false;
		} else if ( showOrHide === false ) {
			$( "#previewTDLeft" ).attr("style","width:49%;");
			$( "#previewTDRight" ).attr("style","width:52%;");
			$("#toggleFormResponses").attr('src',"../images/jpg/ListExpand.jpg");
			showOrHide = true;
		}
	});
}

queryManagement.renderSavedSearches = function () {
	var tabSelected =  $("#queryTabs").tabs('option', 'active');
	var modulename = '';
	if(tabSelected==0){
		modulename = 'formresponse';
	}
	else if(tabSelected==1){
		modulename = 'adverseevent';
	}
	else if(tabSelected==2){
		modulename = 'patientstdstat';
	}
	else{
		modulename = 'patdemographic';
	}
	$.ajax({
        url: urls.dashboardUtils.fetchSavedSearches,
        type: 'GET',
        global: true,
        dataType: 'json',
        async: false,
        data: {
            'searchModule': modulename
        },
        cache: false,
        success: function (data) {
        	 var count=0;
        	 var html ='';;
        	$.each(
        			data,
        			 function () {  /***** @Hemant fixed for bug #21105 start *****/
                        html += '<input type="radio" value="' + this.browsersearch_criteria + '" id="savedSearch_'+count+'" name="savedSearch" onclick= "searchSaveFilter();" ></input>' + queryManagement.decodeUrlParam(this.browsersearch_name) + '<br/>'
                        html += '<input type="hidden" id="savedsrchcnt1_'+count+'" value="'+this.browsersearch_name+'"/>'
                        count++;
        			});
        	html += '<input checked="true" type="radio" id="savedSearch_'+count+'" name="savedSearch" value="none"></input>None<br/>'
        	html += '<input type="hidden" id="savedsrchcnt" value="'+count+'"/>'
        	/***** @Hemant fixed for bug #21105 end *****/
        	$('#savedSearchContentDIV').append(html);
        },
        error: function (xhr, status, errorThrown) {
            console.log('errorThrown  ' + errorThrown);
            return null;
        }
    });	
}

queryManagement.setUpStdAutoComplete = function (){
	 filterEmtyFlag=1;
        $("#selectedStudies").select2({
            placeholder: "Search a Study",
            minimumInputLength: 1,
            multiple: true,
            ajax: {
                url: urls.dashboardUtils.fetchAutoCompleteStudyData,
                dataType: 'json',
                allowClear: true,
                data: function(term, page) {
                    return {
                    	studyNum: term
                    };
                },
                results: function (data, page) {
                    return {
                        results: data
                    };
                }
            }
        });
        $("#selectedStudies").change(function() {
            var selections = ( JSON.stringify($("#selectedStudies").select2('data')) );
            var json = $.parseJSON(selections);
            var tabSelected =  $("#queryTabs").tabs('option', 'active');
            if(tabSelected==0){
            	document.getElementById('formid').style.display ='block';
            	document.getElementById('patStudyFormid').style.display ='none';
            	document.getElementById('advFormid').style.display ='none';
            }
            if(tabSelected==1){
            	document.getElementById('formid').style.display ='none';
            	  document.getElementById('advFormid').style.display ='block';
            }
            if(tabSelected==2){
            	document.getElementById('formstatus').style.display ='block';
            	document.getElementById('formid').style.display ='none';
            	  document.getElementById('patStudyFormid').style.display ='block';
            }
            //**********************Study team role base implementation *******************************************
            var studyIds = $("#selectedStudies").val();
            var studyArr=studyIds.split(',');
            if(studyIds==''){
            	filterStudyTRole='';
            	filterEmtyFlag=1;
            }	
            if(studyIds!=''){
            	 queryManagement.latestTeamroleForMultipleStudy('select2Changed');
            	$.ajax({
            		url: urls.dashboardUtils.studyTeamRoleQueryFetch,
                    type: 'GET',
                    global: true,
                    dataType: 'json',
                    async: false,
                    data: {
                        'studyIds': studyArr[studyArr.length-1]
                    },
                    cache: false,
                    success: function (jsonData) {
                    	if(filterEmtyFlag==1){
                    		if(jsonData.length==0){
                    		studyTeamrole=queryManagement.defaultStRoleForSuperUser();
                    		if(filterStudyTRole!=''){
                    			studyTeamrole=filterStudyTRole;
                    			filterStudyTRole='';
                    		}
                    		filterEmtyFlag=0;
                    		}else{
                    			studyTeamrole=jsonData[0].codelst_subtyp;
                    		if(filterStudyTRole!=''){
                    			studyTeamrole=filterStudyTRole;
                    			filterStudyTRole='';
                    		}
                    		filterEmtyFlag=0;
                    		}
                    	}
                    	if(jsonData.length>0){
                    	if(jsonData[0].codelst_subtyp!='tRoleNotRequired'){ /* If no study team role is defined in the ER_CODELST   */
                    	if(studyTeamrole!=jsonData[0].codelst_subtyp){
                    		//alert("alert found-"+jsonData[0].codelst_subtyp);
                    		alert(LC.STUDY_TMR_PROMT);
                    		alertFlag=0;
                    		}
                    	}
                    	}else if(alertFlag==1){
                    		alert(LC.STUDY_TMR_PROMT);
                    		alertFlag=0;
                    		}
                    	
                    },error: function (xhr, status, errorThrown) {
                        console.log('errorThrown  ' + errorThrown);
                        return null;
                    }
            	});
            }
            //********************** End of Study team role base implementation *******************************************
            if(studyIds == '')
            {
            	$("#selectedOrgs").select2('data', null);
            	$("#selectedForms").select2('data', null);
            	$("#selectedFormStatues").select2('data', null);
            	$("#selectedAdvEveStatus").select2('data', null);
            	$("#selectedPatientId").select2('data', null);
            	document.getElementById('patid').style.display ='block';
            	$("#selectedPatientStudyId").select2('data', null);
            	document.getElementById('patstdid').style.display ='block';
            	$("#selectedCalName").select2('data', null);
            	document.getElementById('calid').style.display ='none'
            	$("#selectedVisitName").select2('data', null);
            	document.getElementById('visitid').style.display = 'none'; 
            	$("#selectedEventName").select2('data', null);
           		document.getElementById('eventid').style.display = 'none';
           		document.getElementById('selectedEventDate').value = "";
           		document.getElementById('evntdt').style.display = 'none';
           		document.getElementById('selectedqueryStatuess').value = "";
           		document.getElementById('selectedTotalRespQuery').value = "";
           		
            }
           
            var tabSelected =  $("#queryTabs").tabs('option', 'active');
            if(tabSelected ==0){
            if($("#selectedStudies").val()!=''){
            	
           /*Start Bug #21074 by Shoaib*/
            	$('[href="#adverseEventsTab"]').closest('li').show();
        		$('[href="#patStdStatTab"]').closest('li').show();	
			/*End Bug #21074 by Shoaib*/
            queryManagement.pageLoaderShow();
            $("#formQueriesTable").dataTable().fnDestroy();    
        	$("#formQueriesTable").empty();
        	addformQueryTablenodes();
        	queryManagement.renderFormQueryDataTable();
        	queryManagement.renderUpdateDiv('#formQueriesTable_filter');
        	queryManagement.pageLoaderHide();
            	}
            else{     
            	/* message display if do clear search filter in case of Form Response tab */
            	$('#formQueriesTab').html("<p>"+"\""+"Please select a saved search or one or more studies from the left hand panel to begin your search. Use other filters as needed."+"\""+"</p>");
            	$('#formRespTabHeader').html('');
            	$('#advEveTabHeader').html('');
              	$('#patStdStatTabHeader').html('');
            }
            
            }
            else if(tabSelected ==1){
            	 if($("#selectedStudies").val()!=''){
            document.getElementById('selectedAdvForms').value='Adverse Events';		 	
            queryManagement.pageLoaderShow();
        	$("#adverseEventsTable").dataTable().fnDestroy();    
        	$("#adverseEventsTable").empty();
        	adverseEventsTablenodes();
        	queryManagement.renderAdvEventQueriesDataTable();
        	queryManagement.renderUpdateDiv('#adverseEventsTable_filter');
        	queryManagement.pageLoaderHide();}
            	 else{
            		 $("#selectedAdvForms").val('');
                 	/* message display if do clear search filter in case of Form Response tab */
                 	$('#adverseEventsTab').html("<p>"+"\""+"Please select a saved search or one or more studies from the left hand panel to begin your search. Use other filters as needed."+"\""+"</p>");
                 	$('#formRespTabHeader').html('');
                 	$('#advEveTabHeader').html('');
                 	$('#patStdStatTabHeader').html('');
                 }	 
            }
            else if(tabSelected ==2){
            	 if($("#selectedStudies").val()!=''){
            document.getElementById('selectedPatStudyForms').value=""+L_Pat_StdStatus+"";
            queryManagement.pageLoaderShow();
            $("#patStdStatTable").dataTable().fnDestroy();    
        	$("#patStdStatTable").empty();
        	patStdStatTablenodes();
        	queryManagement.renderPatStdStatDataTable();
        	queryManagement.pageLoaderHide();
        	}
            	 else{
            		 $("#selectedPatStudyForms").val('');
                  	/* message display if do clear search filter in case of Form Response tab */
                  	$('#patStdStatTab').html("<p>"+"\""+"Please select a saved search or one or more studies from the left hand panel to begin your search. Use other filters as needed."+"\""+"</p>");
                  	$('#formRespTabHeader').html('');
                  	$('#advEveTabHeader').html('');
                  	$('#patStdStatTabHeader').html('');
                  	
                  }	 
            	 }
          
        });
}

queryManagement.setUpOrgAutoComplete = function (){
    $("#selectedOrgs").select2({
        placeholder: "All",
        minimumInputLength: 1,
        multiple: true,
        ajax: {
            url: urls.dashboardUtils.fetchAutoCompleteOrgData,
            dataType: 'json',
            allowClear: true,
            data: function(term, page) {
                return {
                	orgName: term,
                	studyNum: $("#selectedStudies").val(),
                	tabType: $("#queryTabs").tabs('option', 'active')
                };
            },
            results: function (data, page) {
                return {
                    results: data
                };
            }
        }
    });
    $("#selectedOrgs").change(function() {  
    	var selectedstdy=$("#selectedStudies").val();
      	var selectedorgs=$("#selectedOrgs").val(); 
    	var orgIds = $("#selectedOrgs").val();
    	var tabSelected =  $("#queryTabs").tabs('option', 'active');
        var studyIds = $("#selectedStudies").val();
       /*Start Bug #21074 by Shoaib*/
        if(orgIds != '' && tabSelected==0)
        {
        	if(orgIds==0)
        	{
        		document.getElementById('patid').style.display = 'block';
        		document.getElementById('patid').style.display = 'none';
        		document.getElementById('patstdid').style.display = 'none';
        		$('[href="#adverseEventsTab"]').closest('li').hide();
    			$('[href="#patStdStatTab"]').closest('li').hide();
        	}
    	/*End Bug #21074 by Shoaib*/
        }
        else { 
        /*Start Bug #21074 by Shoaib*/
        	document.getElementById('patid').style.display ='block';
        	document.getElementById('patstdid').style.display ='block';
        	$('[href="#adverseEventsTab"]').closest('li').show();
    		$('[href="#patStdStatTab"]').closest('li').show();
        	//$("#selectedPatientId").select2('data', null);           //Commented for #23241 bug
        /*End Bug #21074 by Shoaib*/
        }
        
        if(studyIds != '' && orgIds != '')
        {
        	if(tabSelected!=3)
        	/*Start Bug #21074 by Shoaib*/
        	{
        		if(tabSelected==0 && orgIds!=0)
        			document.getElementById('patstdid').style.display ='block';
        		else if(tabSelected==0 && orgIds==0)
        			document.getElementById('patstdid').style.display = 'none';
        		else
        			document.getElementById('patstdid').style.display = 'block';
        	}
        	/*End Bug #21074 by Shoaib*/
        	if(tabSelected==0)
        
        		document.getElementById('calid').style.display = 'block';
            
        }
        else
        {
        	/*End Bug #21074*/
        	/*$("#selectedForms").select2('data', null);                      //Commented for #23241 bug
        	$("#selectedFormStatues").select2('data', null);
        	$("#selectedAdvEveStatus").select2('data', null);*/
        	//$("#selectedPatientStudyId").select2('data', null);              //commented for #23240 bug
        	document.getElementById('patstdid').style.display ='block';
        	$("#selectedCalName").select2('data', null);
        	document.getElementById('calid').style.display ='none';
        	$("#selectedVisitName").select2('data', null);
        	document.getElementById('visitid').style.display = 'none'; 
        	$("#selectedEventName").select2('data', null);
       		document.getElementById('eventid').style.display = 'none';
       		document.getElementById('selectedEventDate').value = "";
       		document.getElementById('evntdt').style.display = 'none';
        }
        
        if(tabSelected ==0){
        	if(studyIds == ''){
        		alert('Please select a study first');
        		$("#selectedOrgs").select2('data', null);
        	}
        	else{
        queryManagement.pageLoaderShow();
        	$("#formQueriesTable").dataTable().fnDestroy();    
    	$("#formQueriesTable").empty();
    	addformQueryTablenodes();
    	queryManagement.renderFormQueryDataTable();
    	queryManagement.renderUpdateDiv('#formQueriesTable_filter');
    	queryManagement.pageLoaderHide();}}
        else if(tabSelected ==1){
        	if(studyIds == ''){
        		alert('Please select a study first');
        		$("#selectedOrgs").select2('data', null);
        	}
        	else{
    queryManagement.pageLoaderShow();
    	$("#adverseEventsTable").dataTable().fnDestroy();    
    	$("#adverseEventsTable").empty();
    	adverseEventsTablenodes();
    	queryManagement.renderAdvEventQueriesDataTable();
    	queryManagement.renderUpdateDiv('#adverseEventsTable_filter');
    	queryManagement.pageLoaderHide();}}
        else if(tabSelected ==2){
        	if(studyIds == ''){
        		alert('Please select a study first');
        		$("#selectedOrgs").select2('data', null);
        	}
        	else{
queryManagement.pageLoaderShow();
    	$("#patStdStatTable").dataTable().fnDestroy();    
    	$("#patStdStatTable").empty();
    	patStdStatTablenodes();
    	queryManagement.renderPatStdStatDataTable();
    	queryManagement.pageLoaderHide();}}
        else{
        	if(studyIds == ''){
        		alert('Please select a study first');
        		$("#selectedOrgs").select2('data', null);
        	}
        	else{
        	queryManagement.pageLoaderShow();
    	$("#patDemographicsTable").dataTable().fnDestroy();    
    	$("#patDemographicsTable").empty();
    	patDemographicsTablenodes();
    	queryManagement.renderpatDemographicsDataTable();
    	queryManagement.pageLoaderHide();}}
    });
}
queryManagement.setUpFormAutoComplete = function () {
    $("#selectedForms").select2({
        placeholder: "All",
        minimumInputLength: 1,
        multiple: true,
        ajax: {
            url: urls.dashboardUtils.fetchAutoCompleteFormData,
            dataType: 'json',
            allowClear: true,
            data: function(term, page) {
                return {
                	formName: term,
                	studyNum: $("#selectedStudies").val(),
                	orgId: $("#selectedOrgs").val(),
                	tabType: $("#queryTabs").tabs('option', 'active')
                };
            },
            results: function (data, page) {
                return {
                    results: data
                };
            },
            error:function(data, page){
            	alert("study first");
            }
        }
    });
 
    $("#selectedForms").change(function() {
    	var tabSelected =  $("#queryTabs").tabs('option', 'active');
        if(tabSelected ==0){
        	if($("#selectedForms").val() != ''){
        		
            	var x = document.getElementById('selectedStudies').value;
            	if(x == '' || x == null){
            	alert('Select Study first')	
            	$("#selectedForms").select2('data', null);}
            	}
        	queryManagement.pageLoaderShow();
        $("#formQueriesTable").dataTable().fnDestroy();    
    	$("#formQueriesTable").empty();
    	addformQueryTablenodes();
    	queryManagement.renderFormQueryDataTable();
    	queryManagement.renderUpdateDiv('#formQueriesTable_filter');
    	queryManagement.pageLoaderHide();}
        else if(tabSelected ==1){
        	
        	if($("#selectedForms").val() != ''){
        		
            	var x = document.getElementById('selectedStudies').value;
            	if(x == '' || x == null){
            	alert('Select Study first')	
            	$("#selectedForms").select2('data', null);}
            	}
        
        	
        	queryManagement.pageLoaderShow();
    	$("#adverseEventsTable").dataTable().fnDestroy();    
    	$("#adverseEventsTable").empty();
    	adverseEventsTablenodes();
    	queryManagement.renderAdvEventQueriesDataTable();
    	queryManagement.renderUpdateDiv('#adverseEventsTable_filter');
    	queryManagement.pageLoaderHide();}
        else if(tabSelected ==2){
        	if($("#selectedForms").val() != ''){
        		
            	var x = document.getElementById('selectedStudies').value;
            	if(x == '' || x == null){
            	alert('Select Study first')	
            	$("#selectedForms").select2('data', null);}
            	}
        	queryManagement.pageLoaderShow();
        	$("#patStdStatTable").dataTable().fnDestroy();    
        	$("#patStdStatTable").empty();
        	patStdStatTablenodes();
        	queryManagement.renderPatStdStatDataTable();
        	
        	queryManagement.pageLoaderHide();}
        	
        	
        	
        
    });
}

queryManagement.setUpFormStatAutoComplete = function () {
    $("#selectedFormStatues").select2({
        placeholder: "All",
        minimumInputLength: 1,
        multiple: true,
        ajax: {
            url: urls.dashboardUtils.fetchAutoCompleteFormStatData,
            dataType: 'json',
            allowClear: true,
            data: function(term, page) {
                return {
                	statusDesc: term,
                	studyNum:$("#selectedStudies").val(),
                	tabType: $("#queryTabs").tabs('option', 'active')
                };
            },
            results: function (data, page) {
                return {
                    results: data
                };
            }
        }
    });
    $("#selectedFormStatues").change(function() {
    	var tabSelected =  $("#queryTabs").tabs('option', 'active');
        if(tabSelected ==0){
        	if($("#selectedFormStatues").val() != ''){
        		
            	var x = document.getElementById('selectedStudies').value;
            	if(x == '' || x == null){
            	alert('Select Study first')	
            	$("#selectedFormStatues").select2('data', null);}
            	}
        	

        queryManagement.pageLoaderShow();
        $("#formQueriesTable").dataTable().fnDestroy();    
    	$("#formQueriesTable").empty();
    	addformQueryTablenodes();
    	queryManagement.renderFormQueryDataTable();
    	queryManagement.renderUpdateDiv('#formQueriesTable_filter');
    	queryManagement.pageLoaderHide();}
        if(tabSelected==2){

            if($("#selectedFormStatues").val() != ''){
  		var x = document.getElementById('selectedStudies').value;
      	if(x == '' || x == null){
      	alert('Select Study first')	
      	$("#selectedFormStatues").select2('data', null);}
      	}
  	queryManagement.pageLoaderShow();
	$("#patStdStatTable").dataTable().fnDestroy();    
	$("#patStdStatTable").empty();
	patStdStatTablenodes();
	queryManagement.renderPatStdStatDataTable();
	queryManagement.pageLoaderHide();
        	
        	
        }
        
    });
}

queryManagement.setUpAdvEveStatAutoComplete = function () {
    $("#selectedAdvEveStatus").select2({
        placeholder: "All",
        minimumInputLength: 1,
        multiple: true,
        ajax: {
            url: urls.dashboardUtils.fetchAutoCompleteAEStatData,
            dataType: 'json',
            allowClear: true,
            data: function(term, page) {
                return {
                	aeStatusDesc: term,
                	studyNum:$("#selectedStudies").val()
                };
            },
            results: function (data, page) {
                return {
                    results: data
                };
            }
        }
    });
    $("#selectedAdvEveStatus").change(function() {
    	var tabSelected =  $("#queryTabs").tabs('option', 'active');
        if(tabSelected ==1){
        	
if($("#selectedAdvEveStatus").val() != ''){
        		
            	var x = document.getElementById('selectedStudies').value;
            	if(x == '' || x == null){
            	alert('Select Study first')	
            	$("#selectedAdvEveStatus").select2('data', null);}
            	}
        	queryManagement.pageLoaderShow();
        	$("#adverseEventsTable").dataTable().fnDestroy();    
        	$("#adverseEventsTable").empty();
        	adverseEventsTablenodes();
        	queryManagement.renderAdvEventQueriesDataTable();
        	queryManagement.renderUpdateDiv('#adverseEventsTable_filter');
        	queryManagement.pageLoaderHide();}
    });
}

queryManagement.setUpPatIDAutoComplete = function () {
    $("#selectedPatientId").select2({
        placeholder: "All",
        minimumInputLength: 1,
        multiple: true,
        ajax: {
            url: urls.dashboardUtils.fetchAutoCompletePatIDData,
            dataType: 'json',
            allowClear: true,
            data: function(term, page) {
                return {
                	patientId: term,
                	studyNum: $("#selectedStudies").val(),
                	orgId: $("#selectedOrgs").val()
                };
            },
            results: function (data, page) {
                return {
                    results: data
                };
            }
        }
    });
    $("#selectedPatientId").change(function() {
        var tabSelected =  $("#queryTabs").tabs('option', 'active');
        if(tabSelected ==0){
        	 if($("#selectedPatientId").val() != ''){
         		
             	var x = document.getElementById('selectedStudies').value;
             	if(x == '' || x == null){
             	alert('Select Study first')	
             	$("#selectedPatientId").select2('data', null);}
             	}
        	queryManagement.pageLoaderShow();
        $("#formQueriesTable").dataTable().fnDestroy();    
    	$("#formQueriesTable").empty();
    	addformQueryTablenodes();
    	queryManagement.renderFormQueryDataTable();
    	queryManagement.renderUpdateDiv('#formQueriesTable_filter');
    	queryManagement.pageLoaderHide();}
        else if(tabSelected ==1){
                   if($("#selectedPatientId").val() != ''){
        		
            	var x = document.getElementById('selectedStudies').value;
            	if(x == '' || x == null){
            	alert('Select Study first')	
            	$("#selectedPatientId").select2('data', null);}
            	}
        queryManagement.pageLoaderShow();
    	$("#adverseEventsTable").dataTable().fnDestroy();    
    	$("#adverseEventsTable").empty();
    	adverseEventsTablenodes();
    	queryManagement.renderAdvEventQueriesDataTable();
    	queryManagement.renderUpdateDiv('#adverseEventsTable_filter');
    	queryManagement.pageLoaderHide();}
        else if(tabSelected ==2){
                  if($("#selectedPatientId").val() != ''){
        		var x = document.getElementById('selectedStudies').value;
            	if(x == '' || x == null){
            	alert('Select Study first')	
            	$("#selectedPatientId").select2('data', null);}
            	}
        	queryManagement.pageLoaderShow();
    	$("#patStdStatTable").dataTable().fnDestroy();    
    	$("#patStdStatTable").empty();
    	patStdStatTablenodes();
    	queryManagement.renderPatStdStatDataTable();
    	queryManagement.pageLoaderHide();}
        else{
                  if($("#selectedPatientId").val() != ''){
        		var x = document.getElementById('selectedStudies').value;
            	if(x == '' || x == null){
            	alert('Select Study first')	
            	$("#selectedPatientId").select2('data', null);}
            	}
        	queryManagement.pageLoaderShow();
    	$("#patDemographicsTable").dataTable().fnDestroy();    
    	$("#patDemographicsTable").empty();
    	patDemographicsTablenodes();
    	queryManagement.renderpatDemographicsDataTable();
    	queryManagement.pageLoaderHide();}
    });
}

queryManagement.setUpPatStdIDAutoComplete = function () {
    $("#selectedPatientStudyId").select2({
        placeholder: "All",
        minimumInputLength: 1,
        multiple: true,
        ajax: {
            url: urls.dashboardUtils.fetchAutoCompleteStdPatIDData,
            dataType: 'json',
            allowClear: true,
            data: function(term, page) {
                return {
                	studyPatientId: term,
                	studyNum: $("#selectedStudies").val(),
                	orgId: $("#selectedOrgs").val()
                };
            },
            results: function (data, page) {
                return {
                    results: data
                };
            }
        }
    });
   $("#selectedPatientStudyId").change(function() {
	   var tabSelected =  $("#queryTabs").tabs('option', 'active');
       if(tabSelected ==0){
    	    if($("#selectedPatientStudyId").val() != ''){
       	 	var x = document.getElementById('selectedStudies').value;
           	if(x == '' || x == null){
           	alert('Select Study first')	
           	$("#selectedPatientStudyId").select2('data', null);}
           	}
    	   queryManagement.pageLoaderShow();
       $("#formQueriesTable").dataTable().fnDestroy();    
   	$("#formQueriesTable").empty();
   	addformQueryTablenodes();
   	queryManagement.renderFormQueryDataTable();
   	queryManagement.renderUpdateDiv('#formQueriesTable_filter');
   	queryManagement.pageLoaderHide();}
       else if(tabSelected ==1){
    	   if($("#selectedPatientStudyId").val() != ''){
          	 	var x = document.getElementById('selectedStudies').value;
              	if(x == '' || x == null){
              	alert('Select Study first')	
              	$("#selectedPatientStudyId").select2('data', null);}
              	}
    	   queryManagement.pageLoaderShow();
   	$("#adverseEventsTable").dataTable().fnDestroy();    
   	$("#adverseEventsTable").empty();
   	adverseEventsTablenodes();
   	queryManagement.renderAdvEventQueriesDataTable();
   	queryManagement.renderUpdateDiv('#adverseEventsTable_filter');
   	queryManagement.pageLoaderHide();}
       else if(tabSelected ==2){
    	   if($("#selectedPatientStudyId").val() != ''){
          	 	var x = document.getElementById('selectedStudies').value;
              	if(x == '' || x == null){
              	alert('Select Study first')	
              	$("#selectedPatientStudyId").select2('data', null);}
              	}
    	   queryManagement.pageLoaderShow();
   	$("#patStdStatTable").dataTable().fnDestroy();    
   	$("#patStdStatTable").empty();
   	patStdStatTablenodes();
   	queryManagement.renderPatStdStatDataTable();
   	queryManagement.pageLoaderHide();}
       else{
    	   if($("#selectedPatientStudyId").val() != ''){
          	 	var x = document.getElementById('selectedStudies').value;
              	if(x == '' || x == null){
              	alert('Select Study first')	
              	$("#selectedPatientStudyId").select2('data', null);}
              	}
    	   queryManagement.pageLoaderShow();
   	$("#patDemographicsTable").dataTable().fnDestroy();    
   	$("#patDemographicsTable").empty();
   	patDemographicsTablenodes();
   	queryManagement.renderpatDemographicsDataTable();
   	queryManagement.pageLoaderHide();}
    });
}

queryManagement.setUpCalAutoComplete = function () {
    $("#selectedCalName").select2({
        placeholder: "All",
        minimumInputLength: 1,
        multiple: true,
        ajax: {
            url: urls.dashboardUtils.fetchAutoCompleteCalData,
            dataType: 'json',
            allowClear: true,
            data: function(term, page) {
                return {
                	calendarName: term,
                	studyNum: $("#selectedStudies").val(),
                	orgId: $("#selectedOrgs").val()
                };
            },
            results: function (data, page) {
                return {
                    results: data
                };
            }
        }
    });
    $("#selectedCalName").change(function() {
   	 var calName = $("#selectedCalName").val();
   	 if(calName != '')
   	 {
   		 document.getElementById('visitid').style.display = 'block';     		 
   	 }
   	 else
   	 {
   		 $("#selectedVisitName").select2('data', null);
   		 document.getElementById('visitid').style.display = 'none'; 
   	 }
   	
   	var tabSelected =  $("#queryTabs").tabs('option', 'active');
    if(tabSelected ==0){
    	
    	queryManagement.pageLoaderShow();
    $("#formQueriesTable").dataTable().fnDestroy();    
	$("#formQueriesTable").empty();
	addformQueryTablenodes();
	queryManagement.renderFormQueryDataTable();
	queryManagement.renderUpdateDiv('#formQueriesTable_filter');
	queryManagement.pageLoaderHide();}
    });
}

queryManagement.setUpVisitAutoComplete = function () {
    $("#selectedVisitName").select2({
        placeholder: "All",
        minimumInputLength: 0,
        multiple: true,
        ajax: {
            url: urls.dashboardUtils.fetchAutoCompleteVisitData,
            dataType: 'json',
            allowClear: true,
            data: function(term, page) {
                return {
                	visitName: term,
                	calendarIds: $("#selectedCalName").val()
                };
            },
            results: function (data, page) {
                return {
                    results: data
                };
            }
        }
    });
    
   $("#selectedVisitName").change(function() {
   	var visitVal = $("#selectedVisitName").val();
   	if(visitVal != '')
   	{
   		document.getElementById('eventid').style.display = 'block';
   	}
   	else
   	{
   		$("#selectedEventName").select2('data', null);
   		document.getElementById('eventid').style.display = 'none';
   	}
   	
   	
   	var tabSelected =  $("#queryTabs").tabs('option', 'active');
    if(tabSelected ==0){
    	
    	queryManagement.pageLoaderShow();
    $("#formQueriesTable").dataTable().fnDestroy();    
	$("#formQueriesTable").empty();
	addformQueryTablenodes();
	queryManagement.renderFormQueryDataTable();
	queryManagement.renderUpdateDiv('#formQueriesTable_filter');
	queryManagement.pageLoaderHide();}
    });
}

queryManagement.setUpEventAutoComplete = function () {
    $("#selectedEventName").select2({
        placeholder: "All",
        minimumInputLength: 0,
        multiple: true,
        ajax: {
            url: urls.dashboardUtils.fetchAutoCompleteEventData,
            dataType: 'json',
            allowClear: true,
            data: function(term, page) {
                return {
                	eventName: term,
                	visitIds: $("#selectedVisitName").val()
                };
            },
            results: function (data, page) {
                return {
                    results: data
                };
            }
        }
    });
    
   $("#selectedEventName").change(function() {
   	var evntVal = $("#selectedEventName").val();
   	if(evntVal != '')
   	{
   		document.getElementById('evntdt').style.display = 'block';
   	}
   	else
   	{
   		document.getElementById('selectedEventDate').value = "";
   		document.getElementById('evntdt').style.display = 'none';
   	}

   	var tabSelected =  $("#queryTabs").tabs('option', 'active');
    if(tabSelected ==0){
    	
    	queryManagement.pageLoaderShow();
    $("#formQueriesTable").dataTable().fnDestroy();    
	$("#formQueriesTable").empty();
	addformQueryTablenodes();
	queryManagement.renderFormQueryDataTable();
	queryManagement.renderUpdateDiv('#formQueriesTable_filter');
	queryManagement.pageLoaderHide();}
    });
}

queryManagement.setUpQryCrtAutoComplete = function () {
    $("#selectedQueryCreator").select2({
        placeholder: "All",
        minimumInputLength: 0,
        multiple: true,
        ajax: {
            url: urls.dashboardUtils.fetchAutoCompleteQryCrtData,
            dataType: 'json',
            allowClear: true,
            data: function(term, page) {
                return {
                	queryCreator: term
                };
            },
            results: function (data, page) {
                return {
                    results: data
                };
            }
        }
    });
    $("#selectedQueryCreator").change(function() {
    	var tabSelected =  $("#queryTabs").tabs('option', 'active');
        if(tabSelected ==0){
        	 if($("#selectedQueryCreator").val() != ''){
         		
             	var x = document.getElementById('selectedStudies').value;
             	if(x == '' || x == null){
             	alert('Select Study first')	
             	$("#selectedQueryCreator").select2('data', null);}
             	}
        queryManagement.pageLoaderShow();
        $("#formQueriesTable").dataTable().fnDestroy();    
    	$("#formQueriesTable").empty();
    	addformQueryTablenodes();
    	queryManagement.renderFormQueryDataTable();
    	queryManagement.renderUpdateDiv('#formQueriesTable_filter');
    	queryManagement.pageLoaderHide();}
        else if(tabSelected ==1){
         if($("#selectedQueryCreator").val() != ''){
         		
             	var x = document.getElementById('selectedStudies').value;
             	if(x == '' || x == null){
             	alert('Select Study first')	
             	$("#selectedQueryCreator").select2('data', null);}
             	}
     	queryManagement.pageLoaderShow();
    	$("#adverseEventsTable").dataTable().fnDestroy();    
    	$("#adverseEventsTable").empty();
    	adverseEventsTablenodes();
    	queryManagement.renderAdvEventQueriesDataTable();
    	queryManagement.renderUpdateDiv('#adverseEventsTable_filter');
    	queryManagement.pageLoaderHide();}
        else if(tabSelected ==2){
        	
 if($("#selectedQueryCreator").val() != ''){
         		
             	var x = document.getElementById('selectedStudies').value;
             	if(x == '' || x == null){
             	alert('Select Study first')	
             	$("#selectedQueryCreator").select2('data', null);}
             	}
   	   queryManagement.pageLoaderShow();
    	$("#patStdStatTable").dataTable().fnDestroy();    
    	$("#patStdStatTable").empty();
    	patStdStatTablenodes();
    	queryManagement.renderPatStdStatDataTable();
    	queryManagement.pageLoaderHide();}
        else{
             if($("#selectedQueryCreator").val() != ''){
         		
             	var x = document.getElementById('selectedStudies').value;
             	if(x == '' || x == null){
             	alert('Select Study first')	
             	$("#selectedQueryCreator").select2('data', null);}
             	}
   	   queryManagement.pageLoaderShow();
    	$("#patDemographicsTable").dataTable().fnDestroy();    
    	$("#patDemographicsTable").empty();
    	patDemographicsTablenodes();
    	queryManagement.renderpatDemographicsDataTable();
    	queryManagement.pageLoaderHide();}
    });
}

queryManagement.setUpQryStsAutoComplete = function () {
    $("#selectedQueryStatues").select2({
        placeholder: "All",
        minimumInputLength: 0,
        multiple: true,
        ajax: {
            url: urls.dashboardUtils.fetchAutoCompleteQryStsData,
            dataType: 'json',
            allowClear: true,
            data: function(term, page) {
                return {
                	queryStatus: term
                };
            },
            results: function (data, page) {
                return {
                    results: data
                };
            }
        }
    });
    $("#selectedQueryStatues").change(function() {
    	var tabSelected =  $("#queryTabs").tabs('option', 'active');
        if(tabSelected ==0){
      if($("#selectedQueryStatues").val() != ''){
         		
             	var x = document.getElementById('selectedStudies').value;
             	if(x == '' || x == null){
             	alert('Select Study first')	
             	$("#selectedQueryStatues").select2('data', null);}
             	}
        	queryManagement.pageLoaderShow();
        $("#formQueriesTable").dataTable().fnDestroy();    
    	$("#formQueriesTable").empty();
    	addformQueryTablenodes();
    	queryManagement.renderFormQueryDataTable();
    	queryManagement.renderUpdateDiv('#formQueriesTable_filter');
    	queryManagement.pageLoaderHide();}
        
        else if(tabSelected ==1){
            if($("#selectedQueryStatues").val() != ''){
            		
                	var x = document.getElementById('selectedStudies').value;
                	if(x == '' || x == null){
                	alert('Select Study first')	
                	$("#selectedQueryStatues").select2('data', null);}
                	}
        	queryManagement.pageLoaderShow();
       	$("#adverseEventsTable").dataTable().fnDestroy();    
       	$("#adverseEventsTable").empty();
       	adverseEventsTablenodes();
       	queryManagement.renderAdvEventQueriesDataTable();
       	queryManagement.renderUpdateDiv('#adverseEventsTable_filter');
       	queryManagement.pageLoaderHide();}
        else if(tabSelected ==2){
        	
        	 if($("#selectedQueryStatues").val() != ''){
        	         		
        	             	var x = document.getElementById('selectedStudies').value;
        	             	if(x == '' || x == null){
        	             	alert('Select Study first')	
        	             	$("#selectedQueryStatues").select2('data', null);}
        	             	}
        	   	   queryManagement.pageLoaderShow();
        	    	$("#patStdStatTable").dataTable().fnDestroy();    
        	    	$("#patStdStatTable").empty();
        	    	patStdStatTablenodes();
        	    	queryManagement.renderPatStdStatDataTable();
        	    	queryManagement.pageLoaderHide();}
    });
}

queryManagement.renderUpdateDiv = function(DivId) {
	var tabType=0;
     var studyNum=$("#selectedStudies").val();
	if(DivId == '#formQueriesTable_filter')
		tabType=0;
	else
		tabType=1;
	dashboardUtils
    .fetchFormStatues(
        'fillformstat',
        tabType,
        studyNum,
        function (jsonData) {
        	var html ='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select id="formStatuesDD"><options><option value = "0">Enter a Form Status</option>'
            $.each(jsonData, function () {
                html += '<option data-codelstSubType="' + this.CODELST_SUBTYP + '" value="' + this.PK_CODELST + '">' + this.CODELST_DESC + '</option>'
            });
            html += '</options><select>'
            html += '<span id="eSignMessage" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"></span>'
            html += '<font style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;">e-Signature<FONT class="Mandatory">*</FONT>&nbsp'
            html += '<input type="password" name="eSign" id="eSign" maxlength="8" size="10" autocomplete="off"'
            html += 'onkeyup="$j(\'#eSignMessage\').show();ajaxvalidate(\'misc:\'+this.id,4,\'eSignMessage\',\'Valid e-Sign\',\'Invalid e-Sign\',\'sessUserId\')" onkeypress="fnOnceEnterKeyPress(event)" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"/>&nbsp;</font>'
            	//onkeypress="if (fnOnceEnterKeyPress(event)>0) { return false; } " has been changed in onkeypress="fnOnceEnterKeyPress(event)"--Old code Changed by vivek
            html += '<button id="submit_btn" onclick="return bulkUpdateStatus(event); " ondblclick="return false;">Update Selected</button>'
            $(DivId).html('');
            $(DivId).append(html);  
        });
	 }

queryManagement.renderSaveViewButton = function() {
			var html = '<a href="javascript:void(0);" id="saveView" onclick="queryManagement.saveSearchFilter(\'saveView\')" title="Save View" ><img src="./images/save.gif" border="0"></a>&nbsp<a href="javascript:void(0);" id="refresh" title="Refresh" onclick="queryManagement.refreshPage()"><img src="./images/Refresh.gif" border="0"></a>'
            $(".DTTT_container").append(html);
	 }
queryManagement.hideExcelButton = function() {
	
	$('#ToolTables_formQueriesTable_2').css("display","none");	
}

queryManagement.renderSavedView=function(){
	var dstudy=[];
	var dOrgId=[];
	var dFormScreen=[];
	var dFormStatus=[];
	var dAeStatus=[];
	var dPatId=[];
	var dPatStdId=[];
	var dCalName=[];
	var dVisitName=[];
	var eventName=[];
	var dQueryCtr=[];
	var dQuerySts=[];
	var dQueryStsGT=[];
	var dtotalRes=[];
	var dDataEntTo=[];
	var dDataEntFrom=[];
	var dEventDate=[];
	var dMOduleName=[];
	var dtabSelected=[];
	var dIDisplayLength=[];
	var dIDisplayStart=[];
	var dSortCol=[];
	var dSortDir=[];
	  alertFlag=0;
	$.ajax({
		url: urls.dashboardUtils.fetchSavedView,
        type: 'GET',
        global: true,
        dataType: 'json',
        async: false,
        data: {//'moduleName': moduleName
        },
        cache: false,
        success: function (data) {
        	if(data.length!=0){
        	var urlFilterCtra=data[0].urlFilterCtra;
        	urlFilterCtra=decodeURIComponent(urlFilterCtra);
        	var data1=urlFilterCtra.split("&");
        	console.log(data1.length);
        	dstudy=data1[0].split("=");
        	dstudy=queryManagement.searchCriteriaArr(dstudy[1],',')
        	if(dstudy[0]!=""){
        		var tempDataArr=[];
        		tempDataArr=queryManagement.sameTeamRoleforStudy(dstudy);
        		if(tempDataArr.length!=dstudy.length){
			    	
			    	 alertFlag=1;
			     }
        		dstudy= queryManagement.pushData(tempDataArr,':');
        		$("#selectedStudies").select2('data', dstudy);}
        	
        	dOrgId=data1[1].split("=");
        	dOrgId=queryManagement.searchCriteriaArr(dOrgId[1],',')
        	if(dOrgId[0]!=""){
        		document.getElementById('patid').style.display = 'block'; 
              	if(tabClicked!=3)
              	document.getElementById('patstdid').style.display = 'block';
              	if(tabClicked==0)
              	document.getElementById('calid').style.display = 'block';
        		dOrgId= queryManagement.pushData(dOrgId,':');
            	$("#selectedOrgs").select2('data', dOrgId); }
        	dFormScreen=data1[2].split("=");
        	dFormScreen=queryManagement.searchCriteriaArr(dFormScreen[1],',')
        	if(dFormScreen[0]!=""){
        		dFormScreen= queryManagement.pushData(dFormScreen,':');
            	$("#selectedForms").select2('data', dFormScreen); }
        	dFormStatus=data1[3].split("=");
        	dFormStatus=queryManagement.searchCriteriaArr(dFormStatus[1],',')
        	if(dFormStatus[0]!=""){
        		dFormStatus= queryManagement.pushData(dFormStatus,':');
            	$("#selectedFormStatues").select2('data', dFormStatus);}
        	dAeStatus=data1[4].split("=");
        	dAeStatus=queryManagement.searchCriteriaArr(dAeStatus[1],',')
        	if(dAeStatus[0]!=""){
        	dAeStatus= queryManagement.pushData(dAeStatus,':');
        	$("#selectedAdvEveStatus").select2('data', dAeStatus);}
        	dPatId=data1[5].split("=");
        	dPatId=queryManagement.searchCriteriaArr(dPatId[1],',')
        	if(dPatId[0]!=""){
        	dPatId= queryManagement.pushData(dPatId,':');
        	$("#selectedPatientId").select2('data', dPatId);}
        	dPatStdId=data1[6].split("=");
        	dPatStdId=queryManagement.searchCriteriaArr(dPatStdId[1],',')
        	if(dPatStdId[0]!=""){
        	dPatStdId= queryManagement.pushData(dPatStdId,':');
        	$("#selectedPatientStudyId").select2('data', dPatStdId);}
        	dCalName=data1[7].split("=");
        	dCalName=queryManagement.searchCriteriaArr(dCalName[1],',')
        	if(dCalName[0]!=""){
        	document.getElementById('visitid').style.display = 'block';  
        	dCalName= queryManagement.pushData(dCalName,':');
        	$("#selectedCalName").select2('data', dCalName);}
        	dVisitName=data1[8].split("=");
        	dVisitName=queryManagement.searchCriteriaArr(dVisitName[1],',')
        	if(dVisitName[0]!=""){
        	document.getElementById('eventid').style.display = 'block'; 
        	dVisitName= queryManagement.pushData(dVisitName,':');
        	$("#selectedVisitName").select2('data', dVisitName);}
        	eventName=data1[9].split("=");
        	eventName=queryManagement.searchCriteriaArr(eventName[1],',')
        	if(eventName[0]!=""){
        	document.getElementById('evntdt').style.display = 'block'; 
        	eventName= queryManagement.pushData(eventName,':');
        	$("#selectedEventName").select2('data', eventName);}
        	dQueryCtr=data1[10].split("=");
        	dQueryCtr=queryManagement.searchCriteriaArr(dQueryCtr[1],',')
        	if(dQueryCtr[0]!=""){
        	dQueryCtr= queryManagement.pushData(dQueryCtr,':');
        	$("#selectedQueryCreator").select2('data', dQueryCtr);}
        	dQuerySts=data1[11].split("=");
        	dQuerySts=queryManagement.searchCriteriaArr(dQuerySts[1],',')
        	if(dQuerySts[0]!=""){
        	dQuerySts= queryManagement.pushData(dQuerySts,':');
        	$("#selectedQueryStatues").select2('data', dQuerySts);}
        	dQueryStsGT=data1[12].split("=");
        	$("#selectedqueryStatuess").val(dQueryStsGT[1]);
		    dtotalRes=data1[13].split("=");
		    $("#selectedTotalRespQuery").val(dtotalRes[1]);	
        	dDataEntTo=data1[14].split("=");
        	$("#selectedDataEntryDateTo").val(dDataEntTo[1]);
        	dDataEntFrom=data1[15].split("=");
        	$("#selectedDataEntryDateFrom").val(dDataEntFrom[1]);
        	dEventDate=data1[16].split("=");
        	$("#selectedEventDate").val(dEventDate[1]);	
        	dMOduleName=data1[17].split("=");
        	moduleSaved=dMOduleName[1];
        	dtabSelected=data1[18].split("=");
        	tabClicked = dtabSelected[1];
        	dIDisplayLength=data1[19].split("=");
        	displayLength=dIDisplayLength[1];
        	dIDisplayStart=data1[20].split("=");
        	displayStart=dIDisplayStart[1];
        	dSortCol=data1[21].split("=");
        	sortCol=dSortCol[1];
        	dSortDir=queryManagement.searchCriteriaArr(data1[22],'=');
        	sortDir=dSortDir[1];
        	
        	/*Start Bug #21074 & #22535 by Shoaib*/
        	if(tabClicked==0){
        		
        		var selectedorgs=$("#selectedOrgs").val();
        		var selectedstdy=$("#selectedStudies").val();
        		if(selectedstdy!='')
        		{
        			if(selectedorgs!='' && selectedorgs==0)
        			{
        				$('[href="#adverseEventsTab"]').closest('li').hide();
        				$('[href="#patStdStatTab"]').closest('li').hide();
        				document.getElementById('patid').style.display = 'none';
                		document.getElementById('patstdid').style.display = 'none';
        			}
        			else
        			{
        				$('[href="#adverseEventsTab"]').closest('li').show();
            			$('[href="#patStdStatTab"]').closest('li').show();
        			}
        		}
        		else
        		{
        			$('[href="#adverseEventsTab"]').closest('li').show();
        			$('[href="#patStdStatTab"]').closest('li').show();
        		}
        		/*End Bug #21074 & #22535*/
        		
        		
        		if($("#selectedStudies").val()!=''){
        		$("#queryTabs").tabs({ active: 0 });
                $("#adverseEventsTable").dataTable().fnDestroy();
                $("#formQueriesTable").dataTable().fnDestroy();    
            	$("#formQueriesTable").empty();
            	addformQueryTablenodes();
            	queryManagement.renderFormQueryDataTable(1);
            	queryManagement.renderUpdateDiv('#formQueriesTable_filter');
            	queryManagement.pageLoaderHide();
        	    queryManagement.renderSearchTab(0);
        		}else{
        			/* message display in case of saved view to Form Response*/
        			 $('#formQueriesTab').html("<p>"+"\""+"Please select a saved search or one or more studies from the left hand panel to begin your search. Use other filters as needed."+"\""+"</p>")
        			 $('#formRespTabHeader').html('');
        		}
        		if(alertFlag==1){
		      		alertFlag=0;
		      	}
        	}
        	else if(tabClicked==1){
        		$("#queryTabs").tabs({ active: 1 });
                $("#formQueriesTable").dataTable().fnDestroy();
                $("#adverseEventsTable").dataTable().fnDestroy();    
            	$("#adverseEventsTable").empty();
            	adverseEventsTablenodes();
            	queryManagement.renderAdvEventQueriesDataTable(1);
            	queryManagement.renderUpdateDiv('#adverseEventsTable_filter');
        	    queryManagement.renderSearchTab(1);
        	    if(alertFlag==1){
		      		//alert("Please select one Study at a time");
		      		alertFlag=0;
		      	}
        	}
        	else if(tabClicked==2){
        	$("#queryTabs").tabs({ active: 2 });
            $("#patStdStatTable").dataTable().fnDestroy();    
           	$("#patStdStatTable").empty();
           	patStdStatTablenodes();
           	queryManagement.renderPatStdStatDataTable(1);
           	queryManagement.renderSearchTab(2);
           	if(alertFlag==1){
	      		alert("Please select one Study at a time");
	      		alertFlag=0;
	      	}
        	}
        	else{
        	 $("#queryTabs").tabs({ active: 3 });
             $("#patDemographicsTable").dataTable().fnDestroy();    
           	 $("#patDemographicsTable").empty();
           	 patDemographicsTablenodes();
           	 queryManagement.renderpatDemographicsDataTable(1);
        	 queryManagement.renderSearchTab(3);
        	 if(alertFlag==1){
		      		//alert("Please select one Study at a time");
		      		alertFlag=0;
		      	}
        	}
        	
        	}
        	else{
                $("#adverseEventsTable").dataTable().fnDestroy();
                $("#formQueriesTable").dataTable().fnDestroy();    
            	$("#formQueriesTable").empty();
            	addformQueryTablenodes();
            	queryManagement.renderFormQueryDataTable();
            	queryManagement.renderUpdateDiv('#formQueriesTable_filter');
        	}
        },
        error: function (xhr, status, errorThrown) {
            console.log('errorThrown  ' + errorThrown);
            alert("error");
            return null;
        }
	 });
}

queryManagement.renderSaveView = function(){
	queryManagement.renderSavedView();
	if(moduleSaved!=''){
	
	}
	
}
 

queryManagement.renderFormQueryDataTable = function (dataSet) {
	var state='stateSave';
	var show=true;
$(".selectAllFormResp").on('click',function(){
		
		if(this.checked){
			$('.selectFormResps').each(function() { 
                this.checked = true;              
            });
		}else{
			$('.selectFormResps').each(function() { 
                this.checked = false;                     
            });
		}
		
	});
oTable=$('#formQueriesTable').dataTable({
	dom: 'TC<"clear">lfrtip',
	
    tableTools: {
        "sSwfPath": "/velos/jsp/dashboard/js/extensions/swf/copy_csv_xls_pdf.swf"
    },
"bProcessing": false,
"bServerSide": true,
"bDestroy": true,
"sAjaxSource": urls.queryManagement.fetchFormQueries,
"bLengthChange": true,	   
"bSort": true,
"order": [[ 3, "desc" ]],
"sPaginationType": "full_numbers",
"bPaginate": true,
"aoColumns": [{
    "mData": "editboxPlaceHolder"
},{
   "mData": "flg_color"
},{
    "mData": "total_queries" 
},{
    "mData": "query_status"
}, {
    "mData": "days_open"
}, {
    "mData": "target_days"
}, {
    "mData": "study_number"
}, {
    "mData": "organization"
}, {
    "mData": "patient_study_id"
}, {
    "mData": "form_name"
}, {
    "mData": "form_status"
}, {
    "mData": "form_target_days"
}, {
    "mData": "data_entry_date"
}, {
    "mData": "last_modified_date"
}, {
    "mData": "data_entered_by"
}, {
    "mData": "calender"
}, {
    "mData": "visit"
}, {
    "mData": "event"
}, {
    "mData": "event_date"
}, {
    "mData": "response_id"
},  {
	 "mData": "auditboxPlaceHolder"
},{
    "mData": "checkboxPlaceHolder"
} 

] ,
"columnDefs": [{
"targets": [1],
"orderable": false,
"createdCell": function(td, cellData, rowData, row, col) {
if(cellData == "red") { 
	$(td).html('<a href="javascript:void(0);"  onclick="#" title="Edit" name="red" > <img src="./images/redflag.jpg" border="0" onmouseover="getStatusRed(this)" onclick="formAndQueryDialog(this);"> </a> <input type="hidden" id="redflg" value="'+cellData+'"/>');
}
else if(cellData == "orange") { 
$(td).html('<a href="javascript:void(0);"  onclick="#" title="Edit" name="orange" > <img src="./images/orangflg.jpg" border="0" onmouseover="getStatusOrange()" onmouseout="return nd()" > </a> <input type="hidden" id="orngflg" value="'+cellData+'"/> ');
}
else if(cellData == "yellow") { 
$(td).html('<a href="javascript:void(0);"  onclick="#" title="Edit" name="yellow" > <img src="./images/yellowflg.jpg" border="0" onmouseover="getStatusYellow()" onmouseout="return nd()" > </a> <input type="hidden" id="yelloflg" value="'+cellData+'"/> ');
}
else if(cellData == "purple") { 
$(td).html('<a href="javascript:void(0);"  onclick="#" title="Edit" name="prpl" > <img src="./images/purplflg.jpg" border="0" onmouseover="getStatusPurple()" onmouseout="return nd()" > </a> <input type="hidden" id="prplflg" value="'+cellData+'"/> ');
}
else if(cellData == "green") { 
$(td).html('<a href="javascript:void(0);"  onclick="#" title="Edit" name="green" > <img src="./images/greenflg.jpg" border="0" onmouseover="getStatusGreen()" onmouseout="return nd()" > </a> <input type="hidden" id="greenflg" value="'+cellData+'"/> ');
}
else if(cellData == "white") { 
$(td).html('<a href="javascript:void(0);"  onclick="#" title="Edit" name="white" > <img src="./images/white.jpg" border="0" onmouseover="getStatusWhite()" onmouseout="return nd()" > </a> <input type="hidden" id="whiteflg" value="'+cellData+'"/> ');
}
$(td).css("text-align","center");
}
},
{
	"targets": [2],
	"createdCell": function(td, cellData, rowData, row, col) {   
	 	$(td).css('background-color', rowData.colorcode);
	}
},

{
"targets": [0],
 "orderable": false,
"createdCell": function(td, cellData, rowData, row, col) {
	$(td).html('<a href="javascript:void(0);"  onclick="formAndQueryDialog(this);" title="Edit" ><img src="./images/edit.gif" border="0"></a><input type="hidden" id="study_id" name="study_id" value="'+rowData.pk_study+'"/><input type="hidden" id="fk_form" name="fk_form" value="'+rowData.fk_form+'"/><input type="hidden" id="fk_per" name="fk_per" value="'+rowData.fk_per+'"/><input type="hidden" id="pk_patprot" name="pk_patprot" value="'+rowData.pk_patprot+'"/><input type="hidden" id="FK_FORMLIBVER" name="FK_FORMLIBVER" value="'+rowData.FK_FORMLIBVER+'"/><input type="hidden" id="fld_name" name="fld_name" value="'+rowData.fld_name+'"/><input type="hidden" id="pk_formquery" name="pk_formquery" value="'+rowData.pk_formquery+'"/><input type="hidden" id="lf_entrychar" name="lf_entrychar" value="'+rowData.lf_entrychar+'"/>');
	$(td).css("text-align","center");
}
},{
	"targets": [20],
	 "orderable": false,
	"createdCell": function(td, cellData, rowData, row, col) {
		$(td).html('<a href="javascript:void(0);"  onclick="queryManagement.formResponseAudit(this);"  ><img src="../images/jpg/Audit.gif" title="Audit" border="0"></a>');
		$(td).css("text-align","center");
	}	
},{
"targets": [21],
 "orderable": false,
"createdCell": function(td, cellData, rowData, row, col) {
	$('#formRespTabHeader').html('{'+rowData.rows+'}');
	$(td).html('<input type="checkbox" name="selectFormResp" class="selectFormResps" value="'+rowData.response_id+','+rowData.sub_type+'"/><input type="hidden" name="formtype" value="'+rowData.formtype+'"/>');
	$(td).css("text-align","center");
}
}
],
"fnServerParams": function ( aoData )
{
    	if(aoData[4].value!=-1){
    	displayLength=aoData[4].value;
    	displayStart=aoData[3].value;
    	}
    	
	    aoData.push( { "name": "iDisplayLength", "value": displayLength});
	    aoData.push( { "name": "iDisplayStart", "value": displayStart});
	    aoData.push( { "name": "iSortCol_0", "value": sortCol});
	    aoData.push( { "name": "sSortDir_0", "value": sortDir});
	   
	var _values = $('form[name="QueryFiltersForm"]').serializeArray();

	return $.merge(aoData, _values);
},
/*"oLanguage": {
	"sEmptyTable" : "Your custom message for empty table"
},*/
"fnInitComplete": function(oSettings, json) {
	if ($(this).find('tbody tr').html().indexOf('No data available in table')!=-1) {
		$('#formRespTabHeader').html('{0}');
     }
}
});
queryManagement.renderSaveViewButton();
queryManagement.hideExcelButton();/*Hide the Excel button in Form Responses tab */
//if(dataSet==1){
var oSettings = oTable.fnSettings();
var page = Math.ceil(displayStart / displayLength);
setTimeout(function() { oTable.fnPageChange(page); }, 700);
oTable.fnSort( [ [sortCol,sortDir] ] );//}
queryManagement.selectColumnAfterSave(0);
	$('.clear').html("<span style='color:red'>"+MC.M_Enter_None_PatientEnrollingSite_filter+"<br><br><br></span>");
//alert(MC.M_Enter_None_PatientEnrollingSite_filter);

}

function formAndQueryDialog(el) {
	
	var tabSelected =  $("#queryTabs").tabs('option', 'active');
	var tr=$(el).closest("tr")[0];
	var data = oTable.fnGetData(tr);
	if(tabSelected==2){
		document.getElementById('calender').style.display='none';
		document.getElementById('visit').style.display='none';
		document.getElementById('event').style.display='none';
		var pk_study='';
		var fk_per='';
		var statid='';
		var fieldId='';
		var site_id='';
		var pk_formquery='';
		var traveseIndex=tr.rowIndex;
		var dataIndex=0;
        dataIndex=traveseIndex-1;
        var studyNumber=data['study_number'];
		var patStudyId=data['PATSTDID'];
		
		var count=1;
		
        $('input:hidden[name=pst_study]').each(function() 
				{         
				    		if(count==traveseIndex)	{	    	
				    			pk_study= $(this).val();
				    			
				    			
				    		}
				    		
				    	count++;
				      				    
				});
         count=1;
        $('input:hidden[name=pstfk_per]').each(function() 
				{         
				    		if(count==traveseIndex)	{	    	
				    			fk_per= $(this).val();
				    			
				    		}
				    		
				    	count++;
				      				    
				});
         count=1;
        $('input:hidden[name=pst_statid]').each(function() 
				{         
				    		if(count==traveseIndex)	{	    	
				    			statid= $(this).val();
				    			
				    		}
				    		
				    	count++;
				      				    
				});
        count=1;
        $('input:hidden[name=pstField_id]').each(function() 
				{         
				    		if(count==traveseIndex)	{	    	
				    			fieldId= $(this).val();
				    			
				    		}
				    		
				    	count++;
				      				    
				});
        count=1;
        $('input:hidden[name=pst_formquery]').each(function() 
				{         
				    		if(count==traveseIndex)	{	    	
				    			pk_formquery= $(this).val();
				    			if(pk_formquery=="null"  || pk_formquery==null ){pk_formquery='';}
				    		}
				    		
				    	count++;
				      				    
				});
        count=1;
        $('input:hidden[name=site_id]').each(function() 
				{         
				    		if(count==traveseIndex)	{	    	
				    			site_id= $(this).val();
				    		}
				    		
				    	count++;
				      				    
				});
		$('#preview').attr('src', '/velos/jsp/patstudystatus.jsp?studyId='+pk_study+'&pkey='+fk_per+'&dashBoard=jupiter&statid='+statid+'&changeStatusMode=no');
		$('#preview1').attr('src', '/velos/jsp/addeditquery.jsp?studyId='+pk_study+'&formQueryId='+pk_formquery +'&fieldId='+fieldId+'&filledFormId='+statid+'&from=9&formId=&formType=PatStudy_Form&dashBoard=jupiter&site_id='+site_id);
		var html='<a href="javascript:void(0);" id="frdId" name="frdTab" data-index="'+dataIndex+'"  title="Forward" onclick="forwardMove();"><img src="../images/jpg/imagesForward.jpg" alt="Forward" title="Forward" border="0"></a>' ;
   	    var brdhtml='<a href="javascript:void(0);" id="bwdId" name="bwdTab" data-index="'+dataIndex+'"  title="Backward" onclick="backwardMove();"><img src="../images/jpg/imagesBackward.jpg" alt="Forward" title="Backward" border="0"></a>' ;
   	    $("#forwardId").html(html);
		$("#backwardId").html(brdhtml);
		$("#formNameId").html('Form Name:'+"Patient Study Status");
        $("#entryDate").html('Date Entry:'+data['created_on']);
     	 $("#frmStatus").html('Patient Status:'+data['Stat']);
        $("#responseId").html('Response Id:'+statid);
    	if(data['calender']==null){
          	$("#calender").html('Calender:');
          	}else{
          		$("#calender").html('Calender:'+data['calender']);
          	}
    	if(data['event']==null){
          	$("#event").html('Event:');
          	}else{
          		$("#event").html('Event:'+data['event']);
          	}
    	if(data['visit']==null){
          	$("#visit").html('Visit:');
          	}else{
          		$("#visit").html('Visit:'+data['visit']);
          	}PatientStudyID
          	 $("#PatientStudyID").html('Patient Study ID:'+data['PATSTDID']);
    	$("#organization").html('Organization:'+data['Org']);
        
        
        $("#studyId").html('Study Number:'+data['study_number']);
		queryManagement.formResAndQueryView();
	}
	if(tabSelected==1){
		document.getElementById('calender').style.display='none';
		document.getElementById('visit').style.display='none';
		document.getElementById('event').style.display='none';
		var adv_Id='';
		var pk_formquery='';
		var fieldId='';
		var pk_study='';
		var adv_typeId='';
		var pk_patprot='';
		var fk_per='';
		var statid='';
		var formType='';
		var subtype='';
		var traveseIndex=tr.rowIndex;
        var dataIndex=0;
        dataIndex=traveseIndex-1;
		 var count=1;
	        $('input:hidden[name=adv_Id]').each(function() 
					{         
					    		if(count==traveseIndex)	{	    	
					    			adv_Id= $(this).val();
					    		}
					    		
					    	count++;
					      				    
					});
	      //For Bug 21806 starts 		
			 count=1;
			$('input:hidden[name=sub_type]').each(function()
					{
								if(count==traveseIndex){
									subtype=$(this).val();
									subtype=subtype.replace(/ /g,'')
								}
								
							count++;
					});
			//For Bug 21806 ends 
	         count=1;
	        $('input:hidden[name=pk_study]').each(function() 
					{         
					    		if(count==traveseIndex)	{	    	
					    			pk_study= $(this).val();
					    		}
					    		
					    	count++;
					      				    
					});
	         count=1;
	        $('input:hidden[name=pk_patprot]').each(function() 
					{         
					    		if(count==traveseIndex)	{	    	
					    			pk_patprot= $(this).val();
					    		}
					    		
					    	count++;
					      				    
					});
	         count=1;
	        $('input:hidden[name=fk_per]').each(function() 
					{         
					    		if(count==traveseIndex)	{	    	
					    			fk_per= $(this).val();
					    		}
					    		
					    	count++;
					      				    
					});
	         count=1;
	        $('input:hidden[name=statid]').each(function() 
					{         
					    		if(count==traveseIndex)	{	    	
					    			statid= $(this).val();
					    		}
					    		
					    	count++;
					      				    
					});
	        count=1;
	        $('input:hidden[name=advPk_formquery]').each(function() 
					{         
					    		if(count==traveseIndex)	{	    	
					    			pk_formquery= $(this).val();
					    			if(pk_formquery=="null"  || pk_formquery==null ){pk_formquery='';}
					    			
					    		}
					    		
					    	count++;
					      				    
					});
	        count=1;
	        $('input:hidden[name=advField_id]').each(function() 
					{         
					    		if(count==traveseIndex)	{	    	
					    			fieldId= $(this).val();
					    			
					    		}
					    		
					    	count++;
					      				    
					});

		 $('#preview').attr('src', '/velos/jsp/adverseEventScreen.jsp?adveventId='+adv_Id+'&studyId='+pk_study+'&pkey='+fk_per+'&patProtId='+pk_patprot+'&statid='+statid+'&sub_type='+subtype+'&studyVer=&mode=M&src=tdMenuBarItem5&dashBoard=jupiter');
		 $('#preview1').attr('src', '/velos/jsp/addeditquery.jsp?studyId='+pk_study+'&formQueryId='+pk_formquery +'&fieldId='+fieldId+'&filledFormId='+adv_Id+'&sub_type='+subtype+'&from=8&formId=&formType=Advrse_Form&dashBoard=jupiter');
		 var html='<a href="javascript:void(0);" id="frdId" name="frdTab" data-index="'+dataIndex+'"  title="Forward" onclick="forwardMove();"><img src="../images/jpg/imagesForward.jpg" alt="Forward" title="Forward" border="0"></a>' ;
   	     var brdhtml='<a href="javascript:void(0);" id="bwdId" name="bwdTab" data-index="'+dataIndex+'"  title="Backward" onclick="backwardMove();"><img src="../images/jpg/imagesBackward.jpg" alt="Forward" title="Backward" border="0"></a>' ;
		 $("#forwardId").html(html);
		 $("#backwardId").html(brdhtml);
		 $("#formNameId").html('Advese Type:'+data['adv_type']);
       	 $("#entryDate").html('Start Date:'+data['adv_Sdate']);
       	if(data['adv_cat']==null){
          	$("#frmStatus").html('Category:');
          	}else{
          		$("#frmStatus").html('Category:'+data['adv_cat']);
          	}
         $("#responseId").html('Adverse Id:'+adv_Id);
         $("#PatientStudyID").html('Patient Study ID:'+data['patstdid']);
         $("#organization").html('Organization:'+data['organization']);
         $("#studyId").html('Study Number:'+data['studynum']);
         var subType=data['sub_type'];
         
         if('soft_lock'==subtype){
        	 $("#new1icon").html('&nbsp;&nbsp;&nbsp;<img src="./images/ctrp_images/softlock.png" title="SOFTLOCK" border="0">');
         }
         
         else if('ae_lockdown'==subtype){
                    $("#new1icon").html('&nbsp;&nbsp;&nbsp;<img src="./images/ctrp_images/exclaimation.png" title="HARDLOCK" border="0">');
                    }
		 else if('soft_lock'!=subtype || 'ae_lockdown'!=subtype){
  		   $("#new1icon").html('');
  	   }
		 else{
                   	 $("#backwardId").html(brdhtml);
                    }
		 //For bug 21806 ends
		 queryManagement.formResAndQueryView();
		 
	}
	if(tabSelected==0){
		document.getElementById('calender').style.display='table-cell';
		document.getElementById('visit').style.display='table-cell';
		document.getElementById('event').style.display='table-cell';
		var table = $('#formQueriesTable').DataTable();
		var allData = table.rows().data();
		var fk_per='';
		var study_id='';
		var fk_form='';
		var pk_patprot='';
		var FK_FORMLIBVER='';
		var fld_name='';
		var pk_formquery='';
		var lf_entrychar='';
        var traveseIndex=tr.rowIndex;
        var dataIndex=0;
        dataIndex=traveseIndex-1;
        var count=1;
        $('input:hidden[name=fk_per]').each(function() 
				{         
				    		if(count==traveseIndex)	{	    	
				    	    fk_per= $(this).val();
				    		}
				    		
				    	count++;
				      				    
				}); 
        count=1;
        $('input:hidden[name=study_id]').each(function() 
				{    
				    		if(count==traveseIndex)	{	    	
				    	    study_id= $(this).val();
				    		}
				    	    count++;
				      				    
				}); 
        count=1;
        $('input:hidden[name=fk_form]').each(function() 
				{    
				    		if(count==traveseIndex)	{	    	
				    		fk_form= $(this).val();
				    		}
				    	count++;
				      				    
				}); 
        count=1;
        $('input:hidden[name=pk_patprot]').each(function() 
				{    
				    		if(count==traveseIndex)	{	    	
				    		pk_patprot= $(this).val();
				    		}
				    	   count++;
				      				    
				}); 
        count=1;
        $('input:hidden[name=FK_FORMLIBVER]').each(function() 
				{    
				    		if(count==traveseIndex)	{	    	
				    	 FK_FORMLIBVER= $(this).val();
				    		}
				    	count++;
				      				    
				}); 
        count=1;
        $('input:hidden[name=fld_name]').each(function() 
				{    
				    		if(count==traveseIndex)	{	    	
				    			fld_name= $(this).val();
				    			if(fld_name=="null"){fld_name="";}
				    		}
				    	count++;
				      				    
				}); 
        count=1;
        $('input:hidden[name=pk_formquery]').each(function() 
				{    
				    		if(count==traveseIndex)	{	    	
				    	    pk_formquery= $(this).val();
				    	    if(pk_formquery=="null"){pk_formquery="";}			    	
				    		}
				    	count++;
				      				    
				}); 
        count=1;
        $('input:hidden[name=lf_entrychar]').each(function() 
				{    
				    		if(count==traveseIndex)	{	    	
				    			lf_entrychar= $(this).val();
				    			if(fld_name=="null"){fld_name='';}
				    	
				    		}
				    	count++;
				      				    
				});
        var studyIds=$('#selectedStudies').val();
		var orgIds=$('#selectedOrgs').val();
        if(studyIds!=''){
			if(orgIds==0 && orgIds!=''){
				formTyp='S';
				$('#preview').attr('src', '/velos/jsp/studyformdetails.jsp?patStudyId='+data['patient_study_id']+"&calledFromForm=dashboard&studyId="+study_id+"&pkey="+fk_per+"&formId="+fk_form+"&formLibVer="+FK_FORMLIBVER+"&patProtId="+pk_patprot+"&formFillDt=ALL&fkStorageForSpecimen=&filledFormId="+data['response_id']+"&mode=M&formPullDown="+fk_form.concat("*M*").concat(allData.length)+"&entryChar="+lf_entrychar+"&formDispLocation=S&dashBoard=jupiter");
				$('#preview1').attr('src', '/velos/jsp/addeditquery.jsp?studyId='+study_id+'&formQueryId='+pk_formquery +'&fieldName='+fld_name+'&filledFormId='+data['response_id']+'&from=2&formId='+fk_form+'&dashBoard=jupiter');
				 queryManagement.formResAndQueryView();
				 var html='<a href="javascript:void(0);" id="frdId" name="frdTab" data-index="'+dataIndex+'"  title="Forward" onclick="forwardMove();"><img src="../images/jpg/imagesForward.jpg" alt="Forward" title="Forward" border="0"></a>' ;
           	     var brdhtml='<a href="javascript:void(0);" id="bwdId" name="bwdTab" data-index="'+dataIndex+'"  title="Backward" onclick="backwardMove();"><img src="../images/jpg/imagesBackward.jpg" alt="Forward" title="Backward" border="0"></a>' ;
                 $("#forwardId").html(html);
                 $("#formNameId").html('Form Name:'+data['form_name']);
                 $("#entryDate").html('Date Entry:'+data['data_entry_date']);
              	 $("#frmStatus").html('Form Status:'+data['form_status']);
              	 $("#PatientStudyID").html('Patient Study ID:'+data['patient_study_id']);
                 $("#responseId").html('Response Id:'+data['response_id']);
                 $("#organization").html('Organization:'+data['organization']);
                 if(data['calender']==null){
                   	$("#calender").html('Calender:');
                   	}else{
                   		$("#calender").html('Calender:'+data['calender']);
                   	}
             	if(data['event']==null){
                   	$("#event").html('Event:');
                   	}else{
                   		$("#event").html('Event:'+data['event']);
                   	}
             	if(data['visit']==null){
                   	$("#visit").html('Visit:');
                   	}else{
                   		$("#visit").html('Visit:'+data['visit']);
                   	}
                 $("#studyId").html('Study Number:'+data['study_number']);
                 if(data['form_status']=='Locked For Review' || data['form_status']=='Lockdown'){
                     $("#backwardId").html(brdhtml+'&nbsp;&nbsp;&nbsp;<img src="./images/ctrp_images/exclaimation.png" title="Locked/Softlock" border="0">');
                     }else{
                    	 $("#backwardId").html(brdhtml);
                     }
                }
			else{
				formTyp='SP';
			$('#preview').attr('src', '/velos/jsp/patstudyformdetails.jsp?studyNumber='+data['study_number']+"&patStudyId="+data['patient_study_id']+"&calledFromForm=dashboard&studyId="+study_id+"&pkey="+fk_per+"&formId="+fk_form+"&formLibVer="+FK_FORMLIBVER+"&patProtId="+pk_patprot+"&formFillDt=ALL&fkStorageForSpecimen=&filledFormId="+data['response_id']+"&mode=M&formPullDown="+fk_form.concat("*M*").concat(allData.length)+"&entryChar="+lf_entrychar+"&dashBoard=jupiter");
			$('#preview1').attr('src', '/velos/jsp/addeditquery.jsp?studyId='+study_id+'&formQueryId='+pk_formquery +'&fieldName='+fld_name+'&filledFormId='+data['response_id']+'&from=4&formId='+fk_form+'&dashBoard=jupiter');
			 queryManagement.formResAndQueryView();
			 var html='<a href="javascript:void(0);" id="frdId" name="frdTab" data-index="'+dataIndex+'"  title="Forward" onclick="forwardMove();"><img src="../images/jpg/imagesForward.jpg" alt="Forward" title="Forward" border="0"></a>' ;
       	     var brdhtml='<a href="javascript:void(0);" id="bwdId" name="bwdTab" data-index="'+dataIndex+'"  title="Backward" onclick="backwardMove();"><img src="../images/jpg/imagesBackward.jpg" alt="Forward" title="Backward" border="0"></a>' ;
       	    $("#forwardId").html(html);
         	$("#formNameId").html('Form Name:'+data['form_name']);
         	$("#entryDate").html('Date Entry:'+data['data_entry_date']);
         	$("#frmStatus").html('Form Status:'+data['form_status']);
         	$("#PatientStudyID").html('Patient Study ID:'+data['patient_study_id']);
            $("#responseId").html('Response Id:'+data['response_id']);
            $("#organization").html('Organization:'+data['organization']);
            if(data['calender']==null){
              	$("#calender").html('Calender:');
              	}else{
              		$("#calender").html('Calender:'+data['calender']);
              	}
        	if(data['event']==null){
              	$("#event").html('Event:');
              	}else{
              		$("#event").html('Event:'+data['event']);
              	}
        	if(data['visit']==null){
              	$("#visit").html('Visit:');
              	}else{
              		$("#visit").html('Visit:'+data['visit']);
              	}
         	$("#studyId").html('Study Number:'+data['study_number']);
            if(data['form_status']=='Locked For Review' || data['form_status']=='Lockdown'){
                $("#backwardId").html(brdhtml+'&nbsp;&nbsp;&nbsp;<img src="./images/ctrp_images/exclaimation.png" title="Locked/Softlock" border="0">');
                }else{
               	 $("#backwardId").html(brdhtml);
                }
			}
			
		}
//		formType='A';
//		queryManagement.patStudyResAndQuery('','','A');
//		$('#preview').attr('src', '/velos/jsp/acctformdetails.jsp?formId=355&formLibVer=55&filledFormId=47&entryChar=M&formFillDt=ALL&formPullDown=355*M*2&studyId=&mode=M&jupiter=jupiter');
//		queryManagement.formResAndQueryView();
	}
	if(tabSelected==3){
		alert("Patient Demographics:-Working PatStudy form  only");
	}
//    $('#preview').attr('src', '/velos/jsp/dashboard/templates/formRes.jsp?studyNumber='+studyNumber+"&patStudyId="+patStudyId+"&calledFromForm=dashboard&studyId=184&pkey=138&formId=373&formLibVer=64&patProtId=398&formFillDt=ALL&fkStorageForSpecimen=&filledFormId=340&mode=M&formPullDown=373*M*5&entryChar=M");
//    $('#preview1').attr('src', '/velos/jsp/formQueryHistory.jsp?studyId=184&formQueryId=5&fieldName=Age&filledFormId=340&from=4&formId=373');
//    queryManagement.formResAndQueryView();
}



queryManagement.renderAdvEventQueriesDataTable = function (dataSet) {
$(".selectAllAdvEvent").on('click',function(){
		
		if(this.checked){
			$('.selectAdvEvent').each(function() { 
                this.checked = true;              
            });
			//console.log('ckeck all');
		}else{
			$('.selectAdvEvent').each(function() { 
                this.checked = false;                     
            });
			//console.log('cunckeck all');
		}
		
	});
oTable=$('#adverseEventsTable').dataTable({
		dom: 'TC<"clear">lfrtip',
		
	    tableTools: {
	        "sSwfPath": "/velos/jsp/dashboard/js/extensions/swf/copy_csv_xls_pdf.swf"
	    },
	"bProcessing": false,
	"bServerSide": true,
	"bDestroy": true,
	"sAjaxSource": urls.queryManagement.fetchAdvEventQueriesData,
	"bLengthChange": true,	   
	"bSort": true,
	"order": [[ 5, "desc" ]],
	"sPaginationType": "full_numbers",
	"bPaginate": true,
	"aoColumns": [{
	    "mData": "editboxPlaceHolder"
	},{
		 "mData": "flg_color"
	},{
        "mData": "total_queries"
    },{
        "mData": "query_status1"
    },{
        "mData": "days_open"
    },{
        "mData": "target_days"
    },{
    	"mData": "ae_treatment_course"
    },{
        "mData": "studynum"
    },{
        "mData": "organization"
    },{
        "mData": "patstdid"
    },{
        "mData": "adv_type"
    },{
        "mData": "adv_cat"
    },{
        "mData": "AE_TOXICITY"
    },{
        "mData": "adv_grade"
    },{
        "mData": "meddra_code"
    },{
        "mData": "DICTIONARY"
    },{
        "mData": "attribution"
    },{
    	"mData": "create_on"
    },{
        "mData": "adv_Sdate"
    },{
        "mData": "adv_Edate"
    },{
    	"mData": "last_modified_date"
    },{
        "mData": "AE_DISCVRYDATE"
    },{
        "mData": "AE_LOGGEDDATE"
    },{
        "mData": "entered_by"
    },{
        "mData": "reported_by"
    },{
        "mData": "AE_OUTTYPE"
    },{
        "mData": "action"
    },{
        "mData": "AE_OUTNOTES"
    },{
        "mData": "AE_RECOVERY_DESC"
    },{
    	"mData": "AE_STATUS"
    },{
    	"mData": "adv_Id"
    },{
        "mData": "checkboxPlaceHolder"
    } 

    ],
"columnDefs": [{
	"targets": [1],
	"orderable": false,
	"createdCell": function(td, cellData, rowData, row, col) {
	if(cellData == "red") { 
		$(td).html('<a href="javascript:void(0);"  onclick="#" title="Edit" name="red" > <img src="./images/redflag.jpg" border="0" onmouseover="getStatusRed(this)" onclick="formAndQueryDialog(this);"> </a> <input type="hidden" id="redflg" value="'+cellData+'"/> ');
    }
else if(cellData == "orange") { 
	$(td).html('<a href="javascript:void(0);"  onclick="#" title="Edit" name="orange" > <img src="./images/orangflg.jpg" border="0" onmouseover="getStatusOrange()" onmouseout="return nd()" > </a> <input type="hidden" id="orngflg" value="'+cellData+'"/> ');
	}
else if(cellData == "yellow") { 
	$(td).html('<a href="javascript:void(0);"  onclick="#" title="Edit" name="yellow" > <img src="./images/yellowflg.jpg" border="0" onmouseover="getStatusYellow()" onmouseout="return nd()" > </a> <input type="hidden" id="yelloflg" value="'+cellData+'"/> ');
	}
else if(cellData == "purple") { 
	$(td).html('<a href="javascript:void(0);"  onclick="#" title="Edit" name="prpl" > <img src="./images/purplflg.jpg" border="0" onmouseover="getStatusPurple()" onmouseout="return nd()" > </a> <input type="hidden" id="prplflg" value="'+cellData+'"/> ');
	}
else if(cellData == "green") { 
	$(td).html('<a href="javascript:void(0);"  onclick="#" title="Edit" name="green" > <img src="./images/greenflg.jpg" border="0" onmouseover="getStatusGreen()" onmouseout="return nd()" > </a> <input type="hidden" id="greenflg" value="'+cellData+'"/> ');
	}
else if(cellData == "white") { 
	$(td).html('<a href="javascript:void(0);"  onclick="#" title="Edit" name="white" > <img src="./images/white.jpg" border="0" onmouseover="getStatusWhite()" onmouseout="return nd()" > </a> <input type="hidden" id="whiteflg" value="'+cellData+'"/> ');
	}
	$(td).css("text-align","center");
	}
	},{'bSortable': false,
    "targets": [2],
    "createdCell": function(td, cellData, rowData, row, col) {
		$(td).css('background-color', rowData.colorcode);
    }
	},{
	    "targets": [3],
	    'bSortable': false,
		},{
		    "targets": [4],
		    'bSortable': false,
			},{
    	"targets": [0],
   	 "orderable": false,
   	"createdCell": function(td, cellData, rowData, row, col) {
			$(td).html('<a href="javascript:void(0);"  onclick="formAndQueryDialog(this);" title="Edit" ><img src="./images/edit.gif" border="0"></a><input type="hidden" id="adv_Id" name="adv_Id" value="'+rowData.adv_Id+'"/><input type="hidden" id="pk_study" name="pk_study" value="'+rowData.pk_study+'"/><input type="hidden" id="pk_patprot" name="pk_patprot" value="'+rowData.pk_patprot+'"/><input type="hidden" id="fk_per" name="fk_per" value="'+rowData.fk_per+'"/><input type="hidden" id="statid" name="statid" value="'+rowData.statid+'"/><input type="hidden" id="advField_id" name="advField_id" value="'+rowData.field_id+'"/><input type="hidden" id="advPk_formquery" name="advPk_formquery" value="'+rowData.pk_formquery+'"/><input type="hidden" id="form_type" name="form_type" value="'+rowData.form_type+'"/><input type="hidden" id="sub_type" name="sub_type" value="'+rowData.sub_type+'"/>');
			//For bug 21806 <input type id="sub_type"> 
			$(td).css("text-align","center");
       }
   },{
    	"targets": [31],
    	 "orderable": false,
    	"createdCell": function(td, cellData, rowData, row, col) {
	   		$('#advEveTabHeader').html('{'+rowData.rows+'}');
			$(td).html('<input type="checkbox" name="selectAdvEvent" class="selectAdvEvent" Value="'+rowData.adv_Id+'"/>');
			$(td).css("text-align","center");
        }
    }
     ],
	"fnServerParams": function ( aoData )
	{
	    	if(aoData[4].value!=-1){
	        	displayLength=aoData[4].value;
	        	displayStart=aoData[3].value;
	        	}
	    	//if(dataSet==1){
	    	    aoData.push( { "name": "iDisplayLength", "value": displayLength});
	    	    aoData.push( { "name": "iDisplayStart", "value": displayStart});
	    	    aoData.push( { "name": "iSortCol_0", "value": sortCol});
	    	    aoData.push( { "name": "sSortDir_0", "value": sortDir});
	    	   // }
		var _values = $('form[name="QueryFiltersForm"]').serializeArray();

		return $.merge(aoData, _values);
	},
	/*"oLanguage": {
		"sEmptyTable" : "Your custom message for empty table"
	},*/
	"fnInitComplete": function(oSettings, json) {
		if ($(this).find('tbody tr').html().indexOf('No data available in table')!=-1) {
			$('#advEveTabHeader').html('{0}');
	     }
	}
	});
queryManagement.renderSaveViewButton();
$('#ToolTables_adverseEventsTable_2').css("display","none");/*Hide the Excel button in Adverse Event tab */	
//if(dataSet==1){
var oSettings = oTable.fnSettings();
var page = Math.ceil(displayStart / displayLength);
setTimeout(function() { oTable.fnPageChange(page); }, 700);
oTable.fnSort( [ [sortCol,sortDir] ] );//}
queryManagement.selectColumnAfterSave(1);
}

queryManagement.renderPatStdStatDataTable = function (dataSet) {	
oTable= $('#patStdStatTable').dataTable({
		dom: 'TC<"clear">lfrtip',
		"colVis": {                            /*Bug id:- 21104 fixed by Shoaib and Rashi*/
	        "exclude": [  ]           
	    },
	    tableTools: {
	        "sSwfPath": "/velos/jsp/dashboard/js/extensions/swf/copy_csv_xls_pdf.swf"
	    },
	"bProcessing": false,
	"bServerSide": true,
	"bDestroy": true,
	"sAjaxSource": urls.queryManagement.fetchPatStdStatesData,
	"bFilter" : false,
	"bLengthChange": true,	   
	"bSort": true,
	"order": [[ 5, "desc" ]],
	"sPaginationType": "full_numbers",
	"bPaginate": true,
	"aoColumns": [{
	    "mData": "editboxPlaceHolder"
	},{
		"mData": "flg_color"
	}, {
        "mData": "Total_Queries"
    }, {
        "mData": "query_status1"
    },{
        "mData": "days_open"
    },{
        "mData": "target_days"
    },{
        "mData": "study_number"
    }, {
        "mData": "Org"
    }, {
        "mData": "PATSTDID"
    }, {
        "mData": "Stat"
    }, {
        "mData": "reason_desc"
    }, {
        "mData": "PATSTUDYSTAT_DATE"
    }, {
        "mData": "CURRENT_STAT"
    }, {
        "mData": "created_on"
    }, {
        "mData": "entered_by"
    }, {
        "mData": "last_modified_date"
    }, {
        "mData": "last_modified_by"
    }/*, {
        "mData": "checkboxPlaceHolder"
    } */
    	],
		"columnDefs": [{
			"targets": [1],
			"orderable": false,
			"createdCell": function(td, cellData, rowData, row, col) {
			if(cellData == "red") { 
				$(td).html('<a href="javascript:void(0);"  onclick="#" title="Edit" name="red" > <img src="./images/redflag.jpg" border="0" onmouseover="getStatusRed(this)" onclick="formAndQueryDialog(this);"> </a> <input type="hidden" id="redflg" value="'+cellData+'"/> ');
		    }
		else if(cellData == "orange") { 
			$(td).html('<a href="javascript:void(0);"  onclick="#" title="Edit" name="orange" > <img src="./images/orangflg.jpg" border="0" onmouseover="getStatusOrange()" onmouseout="return nd()" > </a> <input type="hidden" id="orngflg" value="'+cellData+'"/> ');
			}
		else if(cellData == "yellow") { 
			$(td).html('<a href="javascript:void(0);"  onclick="#" title="Edit" name="yellow" > <img src="./images/yellowflg.jpg" border="0" onmouseover="getStatusYellow()" onmouseout="return nd()" > </a> <input type="hidden" id="yelloflg" value="'+cellData+'"/> ');
			}
		else if(cellData == "purple") { 
			$(td).html('<a href="javascript:void(0);"  onclick="#" title="Edit" name="prpl" > <img src="./images/purplflg.jpg" border="0" onmouseover="getStatusPurple()" onmouseout="return nd()" > </a> <input type="hidden" id="prplflg" value="'+cellData+'"/> ');
			}
		else if(cellData == "green") { 
			$(td).html('<a href="javascript:void(0);"  onclick="#" title="Edit" name="green" > <img src="./images/greenflg.jpg" border="0" onmouseover="getStatusGreen()" onmouseout="return nd()" > </a> <input type="hidden" id="greenflg" value="'+cellData+'"/> ');
			}
		else if(cellData == "white") { 
			$(td).html('<a href="javascript:void(0);"  onclick="#" title="Edit" name="white" > <img src="./images/white.jpg" border="0" onmouseover="getStatusWhite()" onmouseout="return nd()" > </a> <input type="hidden" id="whiteflg" value="'+cellData+'"/> ');
			}
			$(td).css("text-align","center");
			}
			},{'bSortable': false,
		    "targets": [2],
		    "createdCell": function(td, cellData, rowData, row, col) {
				$(td).css('background-color', rowData.colorcode);
		    }
			},{
			    "targets": [3],
			    'bSortable': false,
				},{
				    "targets": [4],
				    'bSortable': false,
					},{
	"targets": [0],
	 "orderable": false,
	"createdCell": function(td, cellData, rowData, row, col) {
		$('#patStdStatTabHeader').html('{'+rowData.rows+'}');
		$(td).html('<a href="javascript:void(0);"  onclick="formAndQueryDialog(this);" title="Edit" ><img src="./images/edit.gif" border="0"></a><input type="hidden" id="pst_study" name="pst_study" value="'+rowData.pk_study+'"/><input type="hidden" id="pstfk_per" name="pstfk_per" value="'+rowData.fk_per+'"/><input type="hidden" id="pst_statid" name="pst_statid" value="'+rowData.statid+'"/><input type="hidden" id="pstField_id" name="pstField_id" value="'+rowData.field_id+'"/><input type="hidden" id="pst_formquery" name="pst_formquery" value="'+rowData.pk_formquery+'"/><input type="hidden" id="site_id" name="site_id" value="'+rowData.site_id+'"/>');
		$(td).css("text-align","center");
   }
		}/*,{
	"targets": [15],
	 "orderable": false,
	"createdCell": function(td, cellData, rowData, row, col) {
		$(td).html('<input type="checkbox" name="selectStdStat" class="selectStdStat"/>');
		$(td).css("text-align","center");
    }
		}*/
	],
	"fnServerParams": function ( aoData )
	{
	    	if(aoData[4].value!=-1){
	        	displayLength=aoData[4].value;
	        	displayStart=aoData[3].value;
	        	}
	    	//if(dataSet==1){
	    	    aoData.push( { "name": "iDisplayLength", "value": displayLength});
	    	    aoData.push( { "name": "iDisplayStart", "value": displayStart});
	    	    aoData.push( { "name": "iSortCol_0", "value": sortCol});
	    	    aoData.push( { "name": "sSortDir_0", "value": sortDir});
	    	   // }
		var _values = $('form[name="QueryFiltersForm"]').serializeArray();

		return $.merge(aoData, _values);
	},
	/*"oLanguage": {
		"sEmptyTable" : "Your custom message for empty table"
	},*/
	"fnInitComplete": function(oSettings, json) {
		if ($(this).find('tbody tr').html().indexOf('No data available in table')!=-1) {
			$('#patStdStatTabHeader').html('{0}');
	     }
	}
	});
queryManagement.renderSaveViewButton();
$('#ToolTables_patStdStatTable_2').css("display","none");/*Hide the Excel button in Patient Study Status tab */
//if(dataSet==1){
var oSettings = oTable.fnSettings();
var page = Math.ceil(displayStart / displayLength);
setTimeout(function() { oTable.fnPageChange(page); }, 700);
oTable.fnSort( [ [sortCol,sortDir] ] );//}
queryManagement.selectColumnAfterSave(2);
}

queryManagement.renderpatDemographicsDataTable = function (dataSet) {	
oTable= $('#patDemographicsTable').dataTable({
		dom: 'TC<"clear">lfrtip',
	    tableTools: {
	        "sSwfPath": "/velos/jsp/dashboard/js/extensions/swf/copy_csv_xls_pdf.swf"
	    },
	"bProcessing": false,
	"bServerSide": true,
	"bDestroy": true,
	"sAjaxSource": urls.queryManagement.fetchPatDemographicsData,
	"bFilter" : false,
	"bLengthChange": true,	   
	"bSort": true,
	"order": [[ 3, "desc" ]],
	"sPaginationType": "full_numbers",
	"bPaginate": true,
	//"bStateSave": true,
	"aoColumns": [{
        "mData": "Query_Status"
    }, {
        "mData": "Status_Days"
    }, {
        "mData": "Total_Queries"
    }, {
        "mData": "study_number"
    }, {
        "mData": "organization"
    }, {
        "mData": "reg_date"
    }, {
        "mData": "patient_id"
    }, {
        "mData": "facility_id"
    }, {
        "mData": "fname"
    }, {
        "mData": "lname"
    }, {
        "mData": "dob"
    }, {
        "mData": "gender"
    }, {
        "mData": "created_on"
    }, {
        "mData": "entered_by"
    }, {
        "mData": "last_modified_date"
    }, {
        "mData": "last_modified_by"
    }, {
        "mData": "editboxPlaceHolder"
    }/*, {
        "mData": "checkboxPlaceHolder"
    }*/
    	],
    "columnDefs": [{'bSortable': false,
    "targets": [0],
    "createdCell": function(td, cellData, rowData, row, col) {
		$(td).css('background-color', rowData.colorcode);
    }
	},{
	    "targets": [1],
	    'bSortable': false,
		},{
		    "targets": [2],
		    'bSortable': false,
			},{
	"targets": [16],
	 "orderable": false,
	"createdCell": function(td, cellData, rowData, row, col) {
		$('#patDemographicsTabHeader').html('{'+rowData.rows+'}');
		$(td).html('<a href="javascript:void(0);"  onclick="formAndQueryDialog(this);" title="Edit" ><img src="./images/edit.gif" border="0"></a>');
		$(td).css("text-align","center");
   }
	}/*,{
	"targets": [17],
	 "orderable": false,
	"createdCell": function(td, cellData, rowData, row, col) {
		$(td).html('<input type="checkbox" name="selectDemog" class="selectDemog"/>');
		$(td).css("text-align","center");
    }
	}*/
	],
	"fnServerParams": function ( aoData )
	{	
	    	if(dataSet==1){
	    	    aoData.push( { "name": "iDisplayLength", "value": displayLength});
	    	    aoData.push( { "name": "iDisplayStart", "value": displayStart});
	    	    aoData.push( { "name": "iSortCol_0", "value": sortCol});
	    	    aoData.push( { "name": "sSortDir_0", "value": sortDir});
	    	    }
		var _values = $('form[name="QueryFiltersForm"]').serializeArray();
		//aoData.push( { "name": "sEcho", "value": aoData[0].value});
		//aoData.push( { "name": "iTotalDisplayRecords", "value": aoData[3].value});
		//aoData.push( { "name": "iTotalRecords", "value": aoData[4].value});
		return $.merge(aoData, _values);
	},
	/*"oLanguage": {
		"sEmptyTable" : "Your custom message for empty table"
	},*/
	"fnInitComplete": function(oSettings, json) {
		if ($(this).find('tbody tr').html().indexOf('No data available in table')!=-1) {
			$('#patDemographicsTabHeader').html('{0}');
	     }
	}
	});
queryManagement.renderSaveViewButton();
$('#ToolTables_patDemographicsTable_2').css("display","none");/*Hide the Excel button in Patient Demographics tab */
if(dataSet==1){
var oSettings = oTable.fnSettings();
var page = Math.ceil(displayStart / displayLength);
setTimeout(function() { oTable.fnPageChange(page); }, 700);
oTable.fnSort( [ [sortCol,sortDir] ] );}
}

queryManagement.clearFilters = function (tabSelected) {
	$("#selectedStudies").select2('data', null);
	$("#selectedOrgs").select2('data', null);
	if(tabSelected == 0 || tabSelected == 1){
		
		if(tabSelected == 1){
			$("#selectedFormStatues").select2('data', null);
			document.getElementById('formstatus').style.display ='none';
			$("#selectedAdvEveStatus").select2('data', null);
			document.getElementById('advevntstatus').style.display ='block';
		}
		else{
			$("#selectedForms").select2('data', null);
			document.getElementById('formid').style.display ='block';
			$("#selectedFormStatues").select2('data', null);
			document.getElementById('formstatus').style.display ='block';
			$("#selectedAdvEveStatus").select2('data', null);
			document.getElementById('advevntstatus').style.display ='none';
		}
	}
	else{
		$("#selectedForms").select2('data', null);
		//@Hemant fix for bug #21073,21072 
		document.getElementById('formid').style.display ='none';
		$("#selectedFormStatues").select2('data', null);
		document.getElementById('formstatus').style.display ='block';
		$("#selectedAdvEveStatus").select2('data', null);
		document.getElementById('advevntstatus').style.display ='none';
	}
	$("#selectedPatientId").select2('data', null);
	document.getElementById('patid').style.display ='block';
	$("#selectedPatientStudyId").select2('data', null);
	document.getElementById('patstdid').style.display ='block';
	$("#selectedCalName").select2('data', null);
	document.getElementById('calid').style.display ='none'
	$("#selectedVisitName").select2('data', null);
	document.getElementById('visitid').style.display = 'none'; 
	$("#selectedEventName").select2('data', null);
	document.getElementById('eventid').style.display = 'none';
	document.getElementById('selectedEventDate').value = "";
	document.getElementById('evntdt').style.display = 'none';
	$("#selectedQueryCreator").select2('data', null);
	$("#selectedQueryStatues").select2('data', null);
	$("#selectedDataEntryDateFrom").val('');
	$("#selectedDataEntryDateTo").val('');
	$("#selectedqueryStatuess").val('');
	$("#selectedTotalRespQuery").val('');

}
var uid = 1;
var toggleSearchSetting=function()
{
	   var search = this.searchWidget();
	   search.className="";
	   hangDIV(search,this.uid+"_search");
	   //var refObj=$E.getTarget(e);
	   //this.retrieveSearch(refObj);
 
 }
var resetFilter =  function(){
    $('input[type=text]').each(function() {
        $(this).select2('data', null);
        $(this).val('');
    });
    var tabSelected =  $("#queryTabs").tabs('option', 'active');
    if(tabSelected ==0){
    queryManagement.pageLoaderShow();
    $("#formQueriesTable").dataTable().fnDestroy();    
	$("#formQueriesTable").empty();
	addformQueryTablenodes();
	queryManagement.renderFormQueryDataTable();
	queryManagement.renderUpdateDiv('#formQueriesTable_filter');
	/* message display in case of reset to form response*/
	$('#formQueriesTab').html("<p>"+"\""+"Please select a saved search or one or more studies from the left hand panel to begin your search. Use other filters as needed."+"\""+"</p>");
	$('#formRespTabHeader').html('');
	queryManagement.pageLoaderHide();}
    else if(tabSelected==1){
    queryManagement.pageLoaderShow();
    $("#adverseEventsTable").dataTable().fnDestroy();    
    $("#adverseEventsTable").empty();
    adverseEventsTablenodes();
    queryManagement.renderAdvEventQueriesDataTable();
    queryManagement.renderUpdateDiv('#adverseEventsTable_filter');
    queryManagement.pageLoaderHide();
    }
    else if(tabSelected==2){
    queryManagement.pageLoaderShow();
    $("#patStdStatTable").dataTable().fnDestroy();    
   	$("#patStdStatTable").empty();
   	patStdStatTablenodes();
   	queryManagement.renderPatStdStatDataTable();
   	queryManagement.pageLoaderHide();
    }
    else{
    queryManagement.pageLoaderShow();
    $("#patDemographicsTable").dataTable().fnDestroy();    
    $("#patDemographicsTable").empty();
    patDemographicsTablenodes();
    queryManagement.renderpatDemographicsDataTable();
    queryManagement.pageLoaderHide();
    }
}

var toggleSearchSetting1=function(){
	//$("#saveFilterDiv").fadeIn(300);
	$( "#saveFilterDiv" ).dialog("open");
	
}
this.searchWidget=function()
{ 	
   var srchStr="";
   var srchW=document.createElement('div');
   srchW.id=this.uid+"_searchsave";
   srchW.className="hide";
  srchStr="<div class=\"hd\">"+Search_Widget/*Search Widget*****/+"</div><div class=\"bd\"><table><tr><td><span>"+Save_As/*Save as*****/+" "+
	"<input id=\""+this.uid+"_searchName\" type=\"text\" size=\"10\"/></span>"+
	"<A href=\"javascript:void(0);\" title=\""+Save_Search/*Save Search*****/+"\" id=\""+this.uid+"_ssearch\">"+
	"<img src=\"./images/save.gif\" border=\"0\"></img></A></td></tr><tr><td>"+
	"<span id=\""+this.uid+"_s-search-span\">"+Saved_Searches/*Saved Searches*****/+"<span id=\""+this.uid+"_s-search-dd\"></span></span></td>"+
    "<td><A href=\"javascript:void(0);\" title=\""+Delete_Searh/*Delete Search*****/+"\" id=\""+this.uid+"_dsearch\" >"+
    "<img src=\"../images/jpg/delete_pg.png\" border=\"0\"></A></td></tr></table></DIV>";
    srchW.innerHTML=srchStr;
    return srchW;
}		

function hangDIV(obj,cntxt)
{ 
	var panel= new YAHOO.widget.Panel(obj, { width:"320px", visible:false,constraintoviewport:true,underlay: "none",context:[cntxt, "bl", "tr"] } );
    var success = panel.render(document.body);
    panel.show();
    


}

/**
 * Converts project date format setting in to jquery dateformat setting.
 */ 
function  compiledFormat()
{
 var compiledDateFormat = calDateFormat;
	 compiledDateFormat = compiledDateFormat.replace("MMM","M");
	 compiledDateFormat = compiledDateFormat.replace("MM","mm");
	 compiledDateFormat = compiledDateFormat.replace("yyyy","yy");
	 return compiledDateFormat;		 
}
var jQueryDateFormat = compiledFormat() ;
/**
 * Forms the date range by taking upperbound and lowerbound from velosConfig.js. 
 */
var eResYearRange = "-"+YEAR_LOWER_BOUND+":"+"+"+YEAR_UPPER_BOUND ;
/**
 * Opens jquery datepicker based on selected theme and dateformat. 
 */

function openCal() {	 	 	 
	var datefields = $jq(".datefield");
	if (datefields && datefields.length && datefields.length > 0){
		for (var indx = 0; indx < datefields.length; indx++){
			var form = datefields[indx].form;
			var showDPicker = true;
			if (datefields[indx].readOnly){
				/* Datepicker is not displayed for Readonly date type form fields. 
				All other places in the application with Readonly date type fields, Datepicker will be displayed. */
				showDPicker = ($jq(form).attr('name') == 'er_fillform1')? false : true;
			}
			if (datefields[indx].disabled){
				showDPicker = false;
			}
			if (showDPicker){
				$jq(datefields[indx]).datepicker({
					changeMonth: true,
					changeYear: true,
					yearRange: eResYearRange,
					showButtonPanel: true,
					prevText: '',
					nextText: '',
					closeText: L_Clear,
					currentText: L_Today,
					dateFormat: jQueryDateFormat
				});
			}
		}
	}
	//Added as Custom Date entry with limit to Max Date as Current Date
	if ($jq(".cusdatefield") && $jq(".cusdatefield").length && $jq(".cusdatefield").length > 0){
		
		$jq(".cusdatefield").datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: eResYearRange,
			showButtonPanel: true, 
			prevText: '',
			nextText: '',
			closeText: L_Clear,
			currentText: L_Today,
			dateFormat: jQueryDateFormat,
			maxDate: "0D"
		});
	}
	
	// Added for Milestone YUI grid where clicking on datepicker loses the focus of the cell
	// Make sure to set global varibles on 'cellClickEvent' event, those would be accesed in onClose call back function of datepicker
	if ($jq(".msGridDatefield") && $jq(".msGridDatefield").length && $jq(".msGridDatefield").length > 0){
		$jq(".msGridDatefield").datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: eResYearRange,
			showButtonPanel: true, 
			prevText: '',
			nextText: '',
			closeText: L_Clear,
			currentText: L_Today,
			dateFormat: jQueryDateFormat,
			onClose: function(dateText,obj){
				$jq("#"+globalTd.id).children("div").html(dateText);
				VELOS.milestoneGrid.auditAction(globalORecord.getData('mileTypeId'), globalORecord.getData('mileSeq'), 'Updated', globalrecnum);
				globalORecord.setData(globalColumnKey, dateText);
			}
		});
	}
}

function enableCal(dateFld) {	 	 	 
	if ($jq(dateFld)){	
		$jq(dateFld).datepicker("enable");
	}
}

function disableCal(dateFld) {	 	 	 
	if ($jq(dateFld)){	
		$jq(dateFld).datepicker("disable");
	}
}
$jq.datepicker._generateHTML_Old = $jq.datepicker._generateHTML;
 /**
 * Clear date functionality.
 **/
$jq.datepicker._generateHTML = function(inst) {
	    res = this._generateHTML_Old(inst);
	    res = res.replace("_hideDatepicker()","_clearDate('#"+inst.id+"')"); 
	    return res;
	}



	//Added by AK for INV9.1 enhancement(Auto complete feature).
	function openAuto(column,idColumn,data,columnName,placeHolderText){
		$jq( "#"+column ).autocomplete({
			autoFocus: true,
			extraParams: {
	        format: 'json' 
	        },
	        minLength:0,
	        change: function(event,ui) {
	        	validateAutoComplete(column,idColumn,data,columnName,true,placeHolderText);
	        },        
	        select:function(event,ui) {
	        	$jq("#"+idColumn).val(ui.item.id);
	        	$jq("#"+column).val(ui.item.value);
	          },  
	        source: data
	    });
	    return true;

	} 

	//Ak:Added the autocomplete validation here
	validateAutoComplete = function (column,idColumn,data,columnName,autoCompFlag, placeHolderText){
		var fieldValue=document.getElementById(column).value;
		var fieldId=document.getElementById(idColumn).value;
		
		if(fieldValue==null || fieldValue==""){
			document.getElementById(idColumn).value="";
			return true;
		}
		if (fieldValue == placeHolderText){
			document.getElementById(idColumn).value="";
			return true;
		}
		for (var j=0; j<data.length; j++) {
			if(data[j].value.toLowerCase()==fieldValue.toLowerCase()){
				document.getElementById(idColumn).value=data[j].id;
		        	return true;
		     }
	    }
		var paramArray = [columnName];
		if(autoCompFlag)
		alert(getLocalizedMessageString("M_NoSuch_PlsVldVal",paramArray));/*alert("No such "+columnName+" exists.Please enter a valid value.");*****/
		document.getElementById(idColumn).value="";
		document.getElementById(column).value="";
		
		
	};

/** 
 * Utility functions and Settings for
 * Locale based number formatting. 
 */  

	$jq(document).ready(formatNumbersToLocale);

/**
 * formatNumbersToLocale() function is called to format numbers to 'appNumberLocale'
 * set in 'velosConfigCustom.js' file, all the fields with CLASS attribute set as 'numberfield'
 * on the page are formatted.
 * This function also associate the blur event with the formatting. 
 *
 */
 

	function formatNumbersToLocale(){

		$jq(".numberfield").each(function(){
		
				//var currencySymbol = $jq(this).attr('data-currencySymbol');
				var formatAs = $jq(this).attr('data-formatas');
				if(formatAs=='number'){
					$jq(this).formatCurrency({
							region:appNumberLocale
							,roundToDecimalPlace:0
					//		,symbol: currencySymbol
					});
				$jq(this).blur(function(){
					$jq(this).formatCurrency({
						region:appNumberLocale
						,roundToDecimalPlace:0
				//		,symbol: currencySymbol
						});
					});
				}else{
					$jq(this).formatCurrency({
							region:appNumberLocale
					//		,symbol: currencySymbol
					});
					$jq(this).blur(function(){
						$jq(this).formatCurrency({
							region:appNumberLocale
					//		,symbol: currencySymbol
						});
					});
				}
		});
}

	// This fn DO NOT modify the original jQuery object and would JUST return striped number value
	// Only works with a single element
	$jq.fn.ripLocale = function() {
			var n = $jq(this).asNumber({
				region:appNumberLocale
			});
			return n;
	}
	
	/**
	 * ripLocaleFromAll() function is called to rip format from all fields with CLASS attr "numberfield".
	 *
	 */
	function ripLocaleFromAll(){
		
		$jq(".numberfield").each(function(){
			var num = $jq(this).asNumber({
				region:appNumberLocale
				});
		$jq(this).val(num);
		});
	}
	
	// This fn would MODIFY the ORIGINAL jQuery object and would also return formatted value
	// Only works with a single element
	$jq.fn.applyLocale = function(){
		var formatAs = $jq(this).attr('data-formatas');
		if(formatAs=='number'){
			var str = $jq(this).formatCurrency({
						region:appNumberLocale
						,roundToDecimalPlace:0
					});
		
		}else{
			var str = $jq(this).formatCurrency({
						region:appNumberLocale
					});
		}
		return str.val();
	}
	
	/**
	 * applyLocaleToAll() function is called to apply format to all fields with CLASS attr  "numberfield".
	 *
	 */
	function applyLocaleToAll(){
	
		$jq(".numberfield").each(function(){
				var formatAs = $jq(this).attr('data-formatas');
				if(formatAs=='number'){
					$jq(this).formatCurrency({
						region:appNumberLocale
						,roundToDecimalPlace:0
					});
				}else{
					$jq(this).formatCurrency({
						region:appNumberLocale
					//	,symbol: currencySymbol
					});
				}
		});
	}
	function openFormsWin(qryId,fldName,perId,formId,fillFormId,studyId,patprot,formVerId,eventId,formCount){
		fldName = encodeURIComponent(fldName);
		var w = screen.availWidth;
	    w = w > 1100 ? w - 350 : w - 50;
	    windowName = window.open("queryReview.jsp?qryId="+qryId+"&amp;fldName="+fldName+"&amp;pkey="+perId+"&amp;formId="+formId+"&amp;fillFormId="+fillFormId+"&amp;studyId="+studyId+"&amp;formverId="+formVerId+"&amp;patprotId="+patprot+"&amp;eventId="+eventId+"&amp;formCount="+formCount,"revWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width="+w+",height=800,left=10,top=100");
	    windowName.focus();
	}
	function openStudyWin(studyId){
	    var w = screen.availWidth-200;
	    windowName = window.open("study.jsp?mode=M&amp;srcmenu=tdmenubaritem3&amp;selectedTab=1&amp;studyId="+studyId,"revWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width="+w+",height=800,left=10,top=100");
	    windowName.focus();
	}
	function openPatientWin(studyId,perId){
	    var w = screen.availWidth-200;
	    windowName = window.open("studypatients.jsp?srcmenu=tdmenubaritem5&amp;selectedTab=2&amp;studyId="+studyId+"&amp;patstatus=&amp;patid="+perId,"revWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width="+w+",height=800,left=10,top=100");
	    windowName.focus();
	}
	function openSchWin(perId,patprot,studyId){
	    var w = screen.availWidth-50;
	    var h = screen.availHeight-250;
	    windowName = window.open("patientDemogScreen.jsp?srcmenu=null&selectedTab=1&calledFromForm=dashboard&mode=M&pkey=278&page=patient&patCode=00001&studyId=0&includeMode=Y#","revWin","toolbar=yes,scrollbars=yes,resizable=yes,menubar=yes,status=yes,location=yes,width="+w+",height="+h+",left=10,top=100");
	    windowName.focus();
	}
	function goToSch(perId,patprot,studyId){
	    window.top.location.href = "patientschedule.jsp?srcmenu=tdmenubaritem5&amp;selectedTab=3&amp;mode=M&amp;pkey="+perId+"&amp;patProtId="+patprot+"&amp;studyId="+studyId+"&amp;page=patientEnroll&amp;generate=N";
	}	
	function fnOnceEnterKeyPress(e) {
		var evt = (e) || window.event;
	    if (!evt) { return 0; }
		try {
	        var code = evt.charCode || evt.keyCode;
	        if (code == 13 || code == 10) {
	            var thisTimeSubmitted = new Date();
	            if (!lastTimeSubmitted) { lastTimeSubmitted = 0; }
	            if (!thisTimeSubmitted) { return 0; }
	            if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) {
	                return -1;
	            }
	            lastTimeSubmitted = thisTimeSubmitted;
	            return 1;
	        }
		} catch(e) {}
		return 0;
	} //fnOnceEnterKeyPress function closed

	function bulkUpdateStatus() {
	var matches = new Array();
	var filledFormId = '';
		try {
	        var thisTimeSubmitted = new Date();
	        if (!lastTimeSubmitted) { return true; }
	        if (!thisTimeSubmitted) { return true; }
	        if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) {
	            return false;
	        }
	        lastTimeSubmitted = thisTimeSubmitted;
		} catch(e) {}
		if($("#eSignMessage").text()==L_Invalid_Esign){
			alert("Incorrect e-Signature. Please enter again");/*alert("Incorrect e-Signature. Please enter again");*****/
			$("#eSign").focus();
			return false;
		}

		if($("#eSign").val()==null || $("#eSign").val()==''){
			alert("Please enter e-Signature");/*alert("Please enter e-Signature");*****/
			$("#eSign").focus();
			return false;
		}//if closed
		else if (isNaN($("#eSign").val()) == true) {
			alert("Incorrect e-Signature. Please enter again");/*alert("Incorrect e-Signature. Please enter again");*****/
			$("#eSign").focus();
			return false;
		}else{
		var formType= '';
		var urlParam;
		var filledFormStat = $('#formStatuesDD').val();
		var tabSelected =  $("#queryTabs").tabs('option', 'active');
		//var formStatusId=0;
		var formStatusId= '';
		if(tabSelected==0){
		$('input:checkbox[name=selectFormResp]').each(function() 
				{    
				    if($(this).is(':checked')) {
				    	formStatusId= $(this).val();
				    	formStatusId=formStatusId.split(',');
				    	if(formStatusId[1]=='soft_lock' ){
				    		alert("Form status 'Locked for Review' of  response id("+formStatusId[0]+") can't be changed");
				    	}
				    	else if(formStatusId[1]=='lockdown'){
				    		alert("Form status 'Lock Down' of  response id("+formStatusId[0]+") can't be changed");
				    	}
				    	else{
				    	filledFormId  += formStatusId[0]+",";
				    	}
				      }					    
				});
		filledFormId = filledFormId.substring(0,filledFormId.length-1);
		formType = $('input[name="formtype"]:eq(0)').val();
		urlParam = "filledformids="+filledFormId+"&filledformstat="+filledFormStat+"&formtype="+formType+"&tabSelected="+tabSelected;
		}
		else{
			$('input:checkbox[name=selectAdvEvent]').each(function() 
					{    
					    if($(this).is(':checked')) {				    	
					    	filledFormId += $(this).val()+","; 
					      }					    
					}); 
			filledFormId = filledFormId.substring(0,filledFormId.length-1);
			formType = '';
			urlParam = "filledformids="+filledFormId+"&filledformstat="+filledFormStat+"&formtype="+formType+"&tabSelected="+tabSelected;
		}
		if(filledFormStat=='' || filledFormStat==0){
			alert("Please Select Form Status");/*alert("Please Select Form Status");*****/
			$("#formStatuesDD").focus();
			return false;
		}
		filledFormId=1;
		if(filledFormId=='' || filledFormId==0){
			if(tabSelected==0)
				alert("Please Checked any form response");/*alert("Please Checked any form response");*****/
			else
				alert("Please Checked any Adverse Event");/*alert("Please Checked any Adverse Event");*****/
			return false;
		}
		if(filledFormId!='' && filledFormStat !=''){
		submitData(urlParam);
		}
		}
	}
	
	/*This function is used to send selected invoice(s) Id for deletion*/
	function submitData(urlParam) {
		$.ajax({
			url: urls.queryManagement.bulkFormStatusUpdate,
			type: "POST",
			async:false,
			data:urlParam,		
			success: (function (data){
				var tabSelectd =  $("#queryTabs").tabs('option', 'active');
	            if(tabSelectd ==0){
	            	queryManagement.pageLoaderShow();
	                $("#formQueriesTable").dataTable().fnDestroy();    
	            	$("#formQueriesTable").empty();
	            	addformQueryTablenodes();
	            	queryManagement.renderFormQueryDataTable();
	            	queryManagement.renderUpdateDiv('#formQueriesTable_filter');
	            	queryManagement.pageLoaderHide();}
	            else if(tabSelectd ==1){
	            	queryManagement.pageLoaderShow();
	            	$("#adverseEventsTable").dataTable().fnDestroy();    
	            	$("#adverseEventsTable").empty();
	            	adverseEventsTablenodes();
	            	queryManagement.renderAdvEventQueriesDataTable();
	            	queryManagement.renderUpdateDiv('#adverseEventsTable_filter');
	            	queryManagement.pageLoaderHide();}
				
			}),
			error:function(response) { }
		});
	}//submitData function closed
	
	function changeDate(){
		
		if($("#selectedStudies").val() == '' || $("#selectedStudies").val() == null){
	 		$('#selectedDataEntryDateFrom').val('');
	 		$('#selectedDataEntryDateTo').val('');	 		
	 		alert("select a study first");	 		
	 	}
		
		var tabSelected =  $("#queryTabs").tabs('option', 'active');
        if(tabSelected ==0){
        queryManagement.pageLoaderShow();
        $("#formQueriesTable").dataTable().fnDestroy();    
    	$("#formQueriesTable").empty();
    	addformQueryTablenodes();
    	queryManagement.renderFormQueryDataTable();
    	queryManagement.renderUpdateDiv('#formQueriesTable_filter');
    	queryManagement.pageLoaderHide();}
        else if(tabSelected==1){
        queryManagement.pageLoaderShow();
        $("#adverseEventsTable").dataTable().fnDestroy();    
        $("#adverseEventsTable").empty();
        adverseEventsTablenodes();
        queryManagement.renderAdvEventQueriesDataTable();
        queryManagement.renderUpdateDiv('#adverseEventsTable_filter');
        queryManagement.pageLoaderHide();
        }
        else if(tabSelected==2){
        queryManagement.pageLoaderShow();
        $("#patStdStatTable").dataTable().fnDestroy();    
       	$("#patStdStatTable").empty();
       	patStdStatTablenodes();
       	queryManagement.renderPatStdStatDataTable();
       	queryManagement.pageLoaderHide();
        }
        else{
        queryManagement.pageLoaderShow();
        $("#patDemographicsTable").dataTable().fnDestroy();    
        $("#patDemographicsTable").empty();
        patDemographicsTablenodes();
        queryManagement.renderpatDemographicsDataTable();
        queryManagement.pageLoaderHide();
        }
	}
	
	
		queryManagement.saveSearchFilter=function saveSearchFilter(saveType){
		var saveType=saveType;
		var studyDetail="";
		var studyDetailArr;
		var parameter;
		var oSettings = oTable.fnSettings();
		var iDisplayStart=oSettings._iDisplayStart;
		var iDisplayLength=oSettings._iDisplayLength;	
	    var sortAttr=oTable.fnSettings().aaSorting;
	    var tabSelected =  $("#queryTabs").tabs('option', 'active');
		studyDetailArr=$("#selectedStudies").val().split(",");
		parameter=$("#selectedStudies").val();
		var tabSelected =  $("#queryTabs").tabs('option', 'active');	
	    var selections = ( JSON.stringify($("#selectedStudies").select2('data')) );
	    var studyIds=queryManagement.urlParameterFormat(parameter,selections);
	    studyIds=encodeURIComponent(studyIds);
	    selections = ( JSON.stringify($("#selectedOrgs").select2('data')) );
	    parameter=$("#selectedOrgs").val();
	    var orgIds=queryManagement.urlParameterFormat(parameter,selections);
	    selections = ( JSON.stringify($("#selectedForms").select2('data')) );
	    parameter=$("#selectedForms").val();
	    var selectFormsIds=queryManagement.urlParameterFormat(parameter,selections);
	    selections = ( JSON.stringify($("#selectedFormStatues").select2('data')) );
	    parameter=$("#selectedFormStatues").val();
	    var formStatus=queryManagement.urlParameterFormat(parameter,selections);
	    selections = ( JSON.stringify($("#selectedAdvEveStatus").select2('data')) );
	    parameter=$("#selectedAdvEveStatus").val();
	    var aeStatus=queryManagement.urlParameterFormat(parameter,selections);
	    selections = ( JSON.stringify($("#selectedPatientId").select2('data')) );
	    parameter=$("#selectedPatientId").val();
	    var patientId=queryManagement.urlParameterFormat(parameter,selections);
	    selections = ( JSON.stringify($("#selectedPatientStudyId").select2('data')) );
	    parameter=$("#selectedPatientStudyId").val();
	    var patStudyId=queryManagement.urlParameterFormat(parameter,selections);
	    selections = ( JSON.stringify($("#selectedCalName").select2('data')) );
	    parameter=$("#selectedCalName").val();
	    var calName=queryManagement.urlParameterFormat(parameter,selections);
	    selections = ( JSON.stringify($("#selectedVisitName").select2('data')) );
	    parameter=$("#selectedVisitName").val();
	    var visitName=queryManagement.urlParameterFormat(parameter,selections);
	    selections = ( JSON.stringify($("#selectedEventName").select2('data')) );
	    parameter=$("#selectedEventName").val();
	    var eventName=queryManagement.urlParameterFormat(parameter,selections);
	    selections = ( JSON.stringify($("#selectedQueryCreator").select2('data')) );
	    parameter=$("#selectedQueryCreator").val();
	    var queryCreator=queryManagement.urlParameterFormat(parameter,selections);
	    selections = ( JSON.stringify($("#selectedQueryStatues").select2('data')) );
	    parameter=$("#selectedQueryStatues").val();
	    var queryStatus=queryManagement.urlParameterFormat(parameter,selections);
		
		 var saveAs=$('#saveAS').val();
		 var modulename = '';
			if(tabSelected==0){
				modulename = 'formresponse';
			}
			else if(tabSelected==1){
				modulename = 'adverseevent';
			}
			else if(tabSelected==2){
				modulename = 'patientstdstat';
			}
			else{
				modulename = 'patdemographic';
			}
			if(saveType == 'saveSearch' && saveAs==''){
				alert('Enter save search value');
				return false;
			}
			else{
				if(saveType == 'saveSearch'){
					saveAs=queryManagement.encodeUrlParam(saveAs);
				}
		$.ajax({
	        url: urls.dashboardUtils.SaveFilterCompleteData,
	        type: 'GET',
	        global: true,
	        dataType: 'json',
	        async: false,
	        data: $('#QueryFiltersForm').serialize() + '&saveAs='+saveAs + '&moduleName='+modulename +'&selections='+selections +'&studyIds='+studyIds +'&orgIds='+orgIds +'&selectFormsIds='+selectFormsIds +'&formStatus='+formStatus +'&aeStatus='+aeStatus +'&patientId='+patientId +'&patStudyId='+patStudyId +'&calName='+calName +'&visitName='+visitName +'&eventName='+eventName +'&queryCreator='+queryCreator +'&queryStatus='+queryStatus +'&iDisplayStart='+iDisplayStart +'&iDisplayLength='+iDisplayLength +'&tabSelected='+tabSelected +'&sortAttr='+sortAttr +'&saveType='+saveType,
	        cache: false,
	        success: function (data) {
			queryManagement.pageLoaderShow();
			queryManagement.renderSearchTab(tabSelected);
			/*starts bug #22566 by Shoaib*/
			if(saveAs!='' || saveAs!=null)
			{
				 var countsaved = document.getElementById("savedsrchcnt").value;
			     for(var i=0;i<countsaved;i++)
			     {
			    	 if(saveAs == document.getElementById("savedsrchcnt1_"+i).value )
			    	 document.getElementById("savedSearch_"+i).checked = true;
			     }
			}
			/*ends bug #22566 by Shoaib*/
			setTimeout(function() { queryManagement.pageLoaderHide(); }, 700);      
	             },
			error:function(){
	            	 alert("error");
	             }
		});
		}
			/******** Save View implementation to hide column of each tab in Dash board after click on Show/Hide Button:*************
			                                           Bug No:#22565  
		    */
			if(tabSelected==0){
				tab0ShowAndHide=[];
			var checkedStatus=0;
			$('ul.ColVis_collection:last input[type=checkbox]').each(function() {
				if($('ul.ColVis_collection:last').css('opacity') == '0')
					 checkedStatus =this.checked==true?1:0;
				var spanText=$(this).closest('label').find('span').text();
				tab0ShowAndHide.push(checkedStatus+':'+spanText);
			});
			}else if(tabSelected==1){
				tab1ShowAndHide=[];
				//$("#tab1").nextAll().remove();
				var checkedStatus=0;
				$('ul.ColVis_collection:last input[type=checkbox]').each(function() {
					if($('ul.ColVis_collection:last').css('opacity') == '0')
						 checkedStatus =this.checked==true?1:0;;
					var spanText=$(this).closest('label').find('span').text();
					tab1ShowAndHide.push(checkedStatus+':'+spanText);
					
				});
			}else if(tabSelected==2){
				tab2ShowAndHide=[];
				var checkedStatus=0;
				$('ul.ColVis_collection:last input[type=checkbox]').each(function() {
					if($('ul.ColVis_collection:last').css('opacity') == '0')
						 checkedStatus =this.checked==true?1:0;;
					var spanText=$(this).closest('label').find('span').text();
					tab2ShowAndHide.push(checkedStatus+':'+spanText);
					});
			}
		            	/*********** End of Bug No:#22565********************/
		}
		
			queryManagement.renderSearchTab=function(tabSelected){
		
		$('#savedSearchContentDIV').empty();
		if(tabSelected==0){
			modulename = 'formresponse';
		}
		else if(tabSelected==1){
			modulename = 'adverseevent';
		}
		else if(tabSelected==2){
			modulename = 'patientstdstat';
		}
		else{
			modulename = 'patdemographic';
		}
		
		$.ajax({
	        url: urls.dashboardUtils.fetchSavedSearches,
	        type: 'GET',
	        global: true,
	        dataType: 'json',
	        async: false,
	        data: {
	            'searchModule': modulename
	        },
	        cache: false,
	        success: function (data) { 
	        	 var count=0;
	        	 var html ='';;
	        	$.each(
	        			data,
	                    function () {
	        				/***** @Hemant fixed for bug #21105 start  *****/
	                        html += '<input type="radio" value="' + this.browsersearch_criteria + '" id="savedSearch_'+count+'" name="savedSearch" onclick= "searchSaveFilter();"></input>' + queryManagement.decodeUrlParam(this.browsersearch_name) + '<br/>'
	                        html += '<input type="hidden" id="savedsrchcnt1_'+count+'" value="'+this.browsersearch_name+'"/>'
	                        count++;
	        			});
	        	html += '<input checked="true" type="radio" id="savedSearch_'+count+'" name="savedSearch" onclick= "searchSaveFilter();" value="none"></input>None<br/>'/*Bug #21114 add value="none" by Shoaib and Rashi*/
	        	html += '<input type="hidden" id="savedsrchcnt" value="'+count+'"/>'
	        	html += '<input type="hidden" id="modulename" value="'+modulename+'"/>'   /*Bug #21114 by Shoaib and Rashi*/
	        	
	        	/***** @Hemant fixed for bug #21105 end *****/
	        	$('#savedSearchContentDIV').append(html);
	        	$('#savedSearchContentDIV').scroll();
	        },
	        error: function (xhr, status, errorThrown) {
	            console.log('errorThrown  ' + errorThrown);
	            return null;
	        }
	    });	
	}
	
		queryManagement.renderFromSelected=function(searchData){
			var tabSelected =  $("#queryTabs").tabs('option', 'active');
			queryManagement.clearFilters(tabSelected);
		$.ajax({
	        url: urls.dashboardUtils.fetchSavedSelected,
	        type: 'POST',
	        global: true,
	        dataType: 'json',
	        async: false,
	        data:searchData,
	        cache: false,
	        success: function (data) {
			var count=0;
			var select2Json="";
		    var searchCriArr=new Array();
		    var studyData = [];
		    var orgData = [];
		    var formScreenData = [];
		    var formStatusData = [];
		    var aeData = [];
		    var patData = [];
		    var patStdData = [];
		    var studyData = [];
		    var calData = [];
		    var studyData = [];
		    var visitData = [];
		    var eventData = [];
		    var queryStsData = [];
		    var queryCtrData=[];
		    var queryStatusessData = [];
		    var tempDataArr=[];
		     alertFlag=0;
			 $.each(data, function () {
				     searchCriArr=queryManagement.searchCriteriaArr(data[0].dStudy,',');
				     if(searchCriArr[0]!=""){				    
					     tempDataArr=queryManagement.sameTeamRoleforStudy(searchCriArr);
					     if(tempDataArr.length!=searchCriArr.length){
					    	
					    	 alertFlag=1;
					     }
				    	 studyData= queryManagement.pushData(tempDataArr,':');
				    	  $("#selectedStudies").select2('data', studyData);
					     }
				    
				     searchCriArr=queryManagement.searchCriteriaArr(data[0].dOrgId,',');
                      if(searchCriArr[0]!=""){
                          	document.getElementById('patid').style.display = 'block'; 
                         
                          	if(tabSelected!=3)
                          	document.getElementById('patstdid').style.display = 'block';
                          	if(tabSelected==0)
                          	document.getElementById('calid').style.display = 'block';
                       
                    	  orgData= queryManagement.pushData(searchCriArr,':');
                    	  $("#selectedOrgs").select2('data', orgData);
					     }
				     
				     searchCriArr=queryManagement.searchCriteriaArr(data[0].dFormScreen,',');
				     if(searchCriArr[0]!=""){
					     
				    	 formScreenData= queryManagement.pushData(searchCriArr,':');
				    	 $("#selectedForms").select2('data', formScreenData);
					     }
				     
				     searchCriArr=queryManagement.searchCriteriaArr(data[0].dFormStatus,',');
                      if(searchCriArr[0]!=""){
					     
                    	  formStatusData= queryManagement.pushData(searchCriArr,':');
                    	  $("#selectedFormStatues").select2('data', formStatusData);
					     }
				    
				     searchCriArr=queryManagement.searchCriteriaArr(data[0].dAeStatus,',');
				     if(searchCriArr[0]!=""){
					     
				    	 aeData= queryManagement.pushData(searchCriArr,':');
				    	 $("#selectedAdvEveStatus").select2('data', aeData);
					     }
				    
				     searchCriArr=queryManagement.searchCriteriaArr(data[0].dPatId,',');
                      if(searchCriArr[0]!=""){
					     
                    	  patData= queryManagement.pushData(searchCriArr,':');
                    	 $("#selectedPatientId").select2('data', patData);
					     }
				     searchCriArr=queryManagement.searchCriteriaArr(data[0].dPatStdId,',');
                     if(searchCriArr[0]!=""){
					     
                    	 patStdData= queryManagement.pushData(searchCriArr,':');
                    	 $("#selectedPatientStudyId").select2('data', patStdData);
					     }
				    
				     searchCriArr=queryManagement.searchCriteriaArr(data[0].dCalName,',');
                     if(searchCriArr[0]!=""){
                    	 document.getElementById('visitid').style.display = 'block';  
                    	 calData= queryManagement.pushData(searchCriArr,':');
                    	 $("#selectedCalName").select2('data', calData);
					     }
				     
				     searchCriArr=queryManagement.searchCriteriaArr(data[0].dVisitName,',');
				     if(searchCriArr[0]!=""){
				    	 document.getElementById('eventid').style.display = 'block'; 
				    	 visitData= queryManagement.pushData(searchCriArr,':');
				    	 $("#selectedVisitName").select2('data', visitData);
					     }
				     
				     searchCriArr=queryManagement.searchCriteriaArr(data[0].deventName,',');
				     if(searchCriArr[0]!=""){
				    	 document.getElementById('evntdt').style.display = 'block';
					      eventData= queryManagement.pushData(searchCriArr,':');
					      $("#selectedEventName").select2('data', eventData);
					     }
				     searchCriArr=queryManagement.searchCriteriaArr(data[0].dQuerySts,',');
				     if(searchCriArr[0]!=""){
					    
					      queryStsData= queryManagement.pushData(searchCriArr,':');
					      $("#selectedQueryStatues").select2('data', queryStsData);;
					     }
				     searchCriArr=queryManagement.searchCriteriaArr(data[0].dQueryCtr,',');
				     if(searchCriArr[0]!=""){
				    
				      queryCtrData= queryManagement.pushData(searchCriArr,':');
				      $("#selectedQueryCreator").select2('data', queryCtrData);
				     }
			           count++;
			       });
		      $("#selectedqueryStatuess").val(data[0].dQueryStatusess);	
		      $("#selectedTotalRespQuery").val(data[0].dtotalRes);	
		      $("#selectedEventDate").val(data[0].dEventDate);	
		      $("#selectedDataEntryDateFrom").val(data[0].dDataEntFrom);	
		      $("#selectedDataEntryDateTo").val(data[0].dDataEntTo);
		      if(tabSelected==0){
		    	  queryManagement.pageLoaderShow(); 
		          $("#formQueriesTable").dataTable().fnDestroy();    
		      	$("#formQueriesTable").empty();
		      	addformQueryTablenodes();
		      	queryManagement.renderFormQueryDataTable();
		      	queryManagement.renderUpdateDiv('#formQueriesTable_filter');
		      	queryManagement.pageLoaderHide();
		      	if(alertFlag==1){
		      		alertFlag=0;
		      	}
		      }
		      else if(tabSelected==1){
		    	  queryManagement.defaultElemt(tabSelected);
		    	  queryManagement.pageLoaderShow();
		          $("#adverseEventsTable").dataTable().fnDestroy();    
		      	$("#adverseEventsTable").empty();
		      	adverseEventsTablenodes();
		      	queryManagement.renderAdvEventQueriesDataTable();
		      	queryManagement.renderUpdateDiv('#adverseEventsTable_filter');
		      	queryManagement.pageLoaderHide();
		      	if(alertFlag==1){
		      		alertFlag=0;
		      	}
		      }
		      else if(tabSelected==2){
		    	  queryManagement.defaultElemt(tabSelected);
		    	  queryManagement.pageLoaderShow();
		          $("#patStdStatTable").dataTable().fnDestroy();    
		     	$("#patStdStatTable").empty();
		     	patStdStatTablenodes();
		     	queryManagement.renderPatStdStatDataTable();
		     	queryManagement.pageLoaderHide();
		     	if(alertFlag==1){
		      		alertFlag=0;
		      	}
		      }
		      else{
		    	  queryManagement.pageLoaderShow();
		          $("#patDemographicsTable").dataTable().fnDestroy();    
		     	$("#patDemographicsTable").empty();
		     	patDemographicsTablenodes();
		     	queryManagement.renderpatDemographicsDataTable();
		     	queryManagement.pageLoaderHide();
		     	if(alertFlag==1){
		      		alertFlag=0;
		      	}
		      }
	        },
	        error: function (xhr, status, errorThrown) {
	            console.log('errorThrown  ' + errorThrown);
	            return null;
	        }
	    });	
	}
	
	
		queryManagement.urlParameterFormat=function(parameter,jsonObject){
		var formatParam="";
		var splitParam=parameter.split(",");
		var temp;
		var count=0;
		 var json = $.parseJSON(jsonObject);
		 $(json).each(function(i,val){
			    $.each(val,function(k,v){  
		        var str=v.toString();
			    	if(str.indexOf('+')>0){
			    	str=queryManagement.operatorReplaceAll(str,'+','[vplus]')
			    	}
			    	str=str.replace(/,/g,'[vcom]').replace(/=/g,'[veq]').replace(/:/g,'[vcol]').replace(/%/g,'[vpst]').replace(/&/g,'[vamp]')
                    temp =splitParam[i]+":"+str+",";
		         count++ 
		        
		});
		   
			    formatParam=formatParam+temp;
		   
		   
		});
		 formatParam=formatParam.substring(0, formatParam.length-1);
		
		return formatParam.trim();
	}
	queryManagement.searchCriteriaArr=function(searchCriArr, splitCriteria){
		var searchData=searchCriArr.split(splitCriteria);
		return searchData
	}
	queryManagement.pushData=function(searchCriArr,splitCriteria){
		var data1 = [];
		$(searchCriArr).each(function(i,val){
	    	 var splitData=val.split(splitCriteria);
            var str=splitData[1].toString();  
              str= queryManagement.replaceAll(str,'[veq]','=');
	    	  str= queryManagement.replaceAll(str,'[vcom]',',');
	    	  str= queryManagement.replaceAll(str,'[vcol]',':');
	    	  str= queryManagement.replaceAll(str,'[vpst]','%');
	    	  str= queryManagement.replaceAll(str,'[vamp]','&');
	    	  str= queryManagement.replaceAll(str,'[vplus]','+');
              data1.push({id: splitData[0], text: str});
            });
		return data1;
	}
	queryManagement.findTabIndexName=function(tabIndex){
		var moduleName='';
		if(tabIndex==0){
			moduleName = 'formresponse';
		}
		else if(tabIndex==1){
			moduleName = 'adverseevent';
		}
		else if(tabIndex==2){
			moduleName = 'patientstdstat';
		}
		else{
			moduleName = 'patdemographic';
		}
		return moduleName;
	}
	queryManagement.replaceAll=function(str ,replc,replcWith){
	while(str.indexOf(replc)>-1){
		str=str.replace(replc,replcWith);
	}
	return str;
	}
   queryManagement.operatorReplaceAll=function(str, splitOperator,replaceWith){
	  var operatorString =str.split(splitOperator) ;
	  var finalStr="";
	
	  for(var i=0;i<operatorString.length;i++){
		  
		  if(i==operatorString.length-1){
			  finalStr=finalStr+operatorString[i];
		  }
		  else{
			  if(i==0){
				  finalStr=finalStr.trim();  
			  }
			  finalStr=finalStr+operatorString[i]+replaceWith;
			 
		  }
     }
	  return finalStr;
   }
   queryManagement.formResAndQueryView=function(){
	   var winW = $(window).width()*0.9;
	   var winH = $(window).height()*0.9;
	   $("#responseView").dialog({
			modal:true,
			height: winH,
			 width: winW,
			//width: '27%' ,
			close: function(ev, ui) {$(this).dialog('destroy'); 
			queryManagement.refreshPage();
	   		}
		});
	   queryManagement.renderFormResponseToggle();
   }
   queryManagement.patStudyResAndQuery=function(studyNumber,patStudyId,formType){
	   $.ajax({
		   url: urls.dashboardUtils.patStudyFormQuery,
	        type: 'GET',
	        global: true,
	        dataType: 'json',
	        async: false,
	        data: {
		   'studyNumber':studyNumber,
		   'patStudyId':patStudyId,
		   'formType':formType
	              },
	        cache: false,
	        success: function (data) {
	            	  dataCount=data.length;
	            	  alert("dataCount in js-"+dataCount);
	            	  if(dataCount==0){
	            		  alert("form not found");
	            		  exit(1);
	            	  }
	            	 
	            	 if(formType=='SP'){
	            	  queryManagement.patStudyQuery(data[0].fk_filledform);
	            	  var formPullDown="";
	            	  formPullDown=data[0].fk_form+"*"+"M"+"*"+data.length;
	            	  $('#preview').attr('src', '/velos/jsp/patstudyformdetails.jsp?studyNumber='+studyNumber+"&patStudyId="+patStudyId+"&calledFromForm=dashboard&studyId="+data[0].pk_study+"&pkey="+data[0].pk_per+"&formId="+data[0].fk_form+"&formLibVer="+data[0].pk_formLibver+"&patProtId="+data[0].pk_patprot+"&formFillDt=ALL&fkStorageForSpecimen=&filledFormId="+data[0].fk_filledform+"&mode=M&formPullDown="+formPullDown+"&entryChar="+data[0].lf_entrychar+"&dashBoard=jupiter");
	            	  $("#formNameId").html('');
	                	$("#entryDate").html('');
	                	$("#frmStatus").html('');
	                   $("#responseId").html('');
	                   $("#studyId").html('');
	            	  queryManagement.formResAndQueryView();
	              }
	            	 else if(formType=='A'){alert("Account form will be display on first time");
	            	 var formPullDown="";
	            	  formPullDown=data[0].formId+"*"+"M"+"*"+data.length;
	            	  alert(formPullDown);
	            	 alert("filledForm-"+data[0].filledFormId);
	            	 $('#preview').attr('src', '/velos/jsp/acctformdetails.jsp?formId='+data[0].formId+'&formLibVer='+data[0].formLibVer+'&filledFormId='+data[0].filledFormId+'&entryChar='+data[0].entryChar+'&formFillDt=ALL&formPullDown='+formPullDown+'&studyId=&mode=M&jupiter=jupiter');
	            	 $("#formNameId").html('');
	               	$("#entryDate").html('');
	               	$("#frmStatus").html('');
	                  $("#responseId").html('');
	                  $("#studyId").html('');
	            	 queryManagement.formResAndQueryView();
	            	 }
	            	
	            	  
	   },
	   error:function(){
      	 alert("error");
       }
	   });   
   }
   queryManagement.moveFormRes=function(indexNum,direction){
	   var tabSelected =  $("#queryTabs").tabs('option', 'active'); 
	   var table='';
	   var allData='';
	   var data='';
	   if(tabSelected==0){
	    table = $('#formQueriesTable').DataTable();
	    allData = table.rows().data(); 
         if(direction=='forward'){
		   indexNum=indexNum+1;
		  if(indexNum>=allData.length){
			  indexNum=0;
		  }
	  }else{
		  if(indexNum<=0){
			  indexNum=allData.length-1;
		  }else{
		   indexNum=indexNum-1;
		  }
	  }
          
        data = table.row(indexNum).data();
        var fk_per='';
		var study_id='';
		var fk_form='';
		var pk_patprot='';
		var FK_FORMLIBVER='';
		var fld_name='';
		var pk_formquery='';
		var lf_entrychar='';
        var count=0;
        $('input:hidden[name=fk_per]').each(function() 
				{    
				    		if(count==indexNum)	{	    	
				    	    fk_per= $(this).val();
				    		}	
				    	count++;
				      				    
				}); 
        count=0;
        $('input:hidden[name=study_id]').each(function() 
				{    
				    		if(count==indexNum)	{	    	
				    	    study_id= $(this).val();
				    		}
				    	    count++;
				      				    
				}); 
        count=0;
        $('input:hidden[name=fk_form]').each(function() 
				{    
				    		if(count==indexNum)	{	    	
				    		fk_form= $(this).val();
				    		}
				    	count++;
				      				    
				}); 
        count=0;
        $('input:hidden[name=pk_patprot]').each(function() 
				{    
				    		if(count==indexNum)	{	    	
				    		pk_patprot= $(this).val();
				    		}
				    	   count++;
				      				    
				}); 
        count=0;
        $('input:hidden[name=FK_FORMLIBVER]').each(function() 
				{    
				    		if(count==indexNum)	{	    	
				    	 FK_FORMLIBVER= $(this).val();
				    		}
				    	count++;
				      				    
				}); 
        count=0;
        $('input:hidden[name=pk_formquery]').each(function() 
				{    
				    		if(count==indexNum)	{	    	
				    			pk_formquery= $(this).val();
				    			if(pk_formquery=="null"){pk_formquery='';}
				    	
				    		}
				    	count++;
				      				    
				}); 
        count=0;
        $('input:hidden[name=fld_name]').each(function() 
				{    
				    		if(count==indexNum)	{	    	
				    			fld_name= $(this).val();
				    			if(fld_name=="null"){fld_name='';}
				    	
				    		}
				    	count++;
				      				    
				});
        count=0;
        $('input:hidden[name=lf_entrychar]').each(function() 
				{    
				    		if(count==indexNum)	{	    	
				    			lf_entrychar= $(this).val();
				    			if(fld_name=="null"){fld_name='';}
				    	
				    		}
				    	count++;
				      				    
				});
        if(formTyp=='SP'){
        $('#preview').attr('src', '/velos/jsp/patstudyformdetails.jsp?studyNumber='+data['study_number']+"&patStudyId="+data['PATSTDID']+"&calledFromForm=dashboard&studyId="+study_id+"&pkey="+fk_per+"&formId="+fk_form+"&formLibVer="+FK_FORMLIBVER+"&patProtId="+pk_patprot+"&formFillDt=ALL&fkStorageForSpecimen=&filledFormId="+data['response_id']+"&mode=M&formPullDown="+fk_form.concat("*M*").concat(allData.length)+"&entryChar="+lf_entrychar+"&dashBoard=jupiter");
        $('#preview1').attr('src', '/velos/jsp/addeditquery.jsp?studyId='+study_id+'&formQueryId='+pk_formquery +'&fieldName='+fld_name+'&filledFormId='+data['response_id']+'&from=4&formId='+fk_form+'&dashBoard=jupiter');
		 queryManagement.formResAndQueryView();
		 var html='<a href="javascript:void(0);" id="frdId" name="frdTab" data-index="'+indexNum+'"  title="Forward" onclick="forwardMove();"><img src="../images/jpg/imagesForward.jpg" alt="Forward" title="Forward" border="0"></a>' ;
  	     var brdhtml='<a href="javascript:void(0);" id="bwdId" name="bwdTab" data-index="'+indexNum+'"  title="Backward" onclick="backwardMove();"><img src="../images/jpg/imagesBackward.jpg" alt="Forward" title="Backward" border="0"></a>' ;
        $("#forwardId").html(html);
        $("#formNameId").html('Form Name:'+data['form_name']);
        $("#entryDate").html('Date Entry:'+data['data_entry_date']);
     	$("#frmStatus").html('Form Status:'+data['form_status']);
        $("#responseId").html('Response Id:'+data['response_id']);
        if(data['calender']==null){
          	$("#calender").html('Calender:');
          	}else{
          		$("#calender").html('Calender:'+data['calender']);
          	}
    	if(data['event']==null){
          	$("#event").html('Event:');
          	}else{
          		$("#event").html('Event:'+data['event']);
          	}
    	if(data['visit']==null){
          	$("#visit").html('Visit:');
          	}else{
          		$("#visit").html('Visit:'+data['visit']);
          	}

    	$("#organization").html('Organization:'+data['organization']);
    	$("#PatientStudyID").html('Patient Study ID:'+data['patient_study_id']);
     	$("#studyId").html('Study Number:'+data['study_number']);
        if(data['form_status']=='Locked For Review' || data['form_status']=='Lockdown'){
            $("#backwardId").html(brdhtml+'&nbsp;&nbsp;&nbsp;<img src="./images/ctrp_images/exclaimation.png" title="Locked/Softlock" border="0">');
            }else{
           	 $("#backwardId").html(brdhtml);
            }
       
        }
        else{
        	$('#preview').attr('src', '/velos/jsp/studyformdetails.jsp?patStudyId='+data['patient_study_id']+"&calledFromForm=dashboard&studyId="+study_id+"&pkey="+fk_per+"&formId="+fk_form+"&formLibVer="+FK_FORMLIBVER+"&patProtId="+pk_patprot+"&formFillDt=ALL&fkStorageForSpecimen=&filledFormId="+data['response_id']+"&mode=M&formPullDown="+fk_form.concat("*M*").concat(allData.length)+"&entryChar="+lf_entrychar+"&formDispLocation=S&dashBoard=jupiter");
			$('#preview1').attr('src', '/velos/jsp/addeditquery.jsp?studyId='+study_id+'&formQueryId='+pk_formquery +'&fieldName='+fld_name+'&filledFormId='+data['response_id']+'&from=2&formId='+fk_form+'&dashBoard=jupiter');
			 queryManagement.formResAndQueryView();
			 var html='<a href="javascript:void(0);" id="frdId" name="frdTab" data-index="'+indexNum+'"  title="Forward" onclick="forwardMove();"><img src="../images/jpg/imagesForward.jpg" alt="Forward" title="Forward" border="0"></a>' ;
       	     var brdhtml='<a href="javascript:void(0);" id="bwdId" name="bwdTab" data-index="'+indexNum+'"  title="Backward" onclick="backwardMove();"><img src="../images/jpg/imagesBackward.jpg" alt="Forward" title="Backward" border="0"></a>' ;
             $("#forwardId").html(html);
             $("#formNameId").html('Form Name:'+data['form_name']);
             $("#entryDate").html('Date Entry:'+data['data_entry_date']);
          	 $("#frmStatus").html('Form Status:'+data['form_status']);
             $("#responseId").html('Response Id:'+data['response_id']);
             if(data['calender']==null){
               	$("#calender").html('Calender:');
               	}else{
               		$("#calender").html('Calender:'+data['calender']);
               	}
         	if(data['event']==null){
               	$("#event").html('Event:');
               	}else{
               		$("#event").html('Event:'+data['event']);
               	}
         	if(data['visit']==null){
               	$("#visit").html('Visit:');
               	}else{
               		$("#visit").html('Visit:'+data['visit']);
               	}

         	$("#organization").html('Organization:'+data['organization']);
         	$("#PatientStudyID").html('Patient Study ID:'+data['patient_study_id']);
             $("#studyId").html('Study Number:'+data['study_number']);
             if(data['form_status']=='Locked For Review'|| data['form_status']=='Lockdown'){
             $("#backwardId").html(brdhtml+'&nbsp;&nbsp;&nbsp;<img src="./images/ctrp_images/exclaimation.png" title="Locked/Softlock" border="0">');
             }
             else{
            	 $("#backwardId").html(brdhtml);
             }
        }
	   }
	   if(tabSelected==1){
		   table = $('#adverseEventsTable').DataTable();
		    allData = table.rows().data(); 
	         if(direction=='forward'){
			  var indexNum=indexNum+1;
			  if(indexNum>=allData.length){
				  indexNum=0;
			  }
		  }else{
			  if(indexNum<=0){
				  var indexNum=allData.length-1;
			  }else{
			  var indexNum=indexNum-1;
			  }
		  }
	          
	        data = table.row(indexNum).data();
	        var adv_Id='';
			var pk_study='';
			var adv_typeId='';
			var pk_patprot='';
			var fk_per='';
			var statid='';
			var pk_formquery='';
			var fieldId='';
			var sub_type='';
			 var count=0;
		        $('input:hidden[name=adv_Id]').each(function() 
						{         
						    		if(count==indexNum)	{	    	
						    			adv_Id= $(this).val();
						    		}
						    		
						    	count++;
						      				    
						});
		      //For Bug 21806 starts 		
				 count=0;
				$('input:hidden[name=sub_type]').each(function()
						{
									if(count==indexNum){
										subtype=$(this).val();										
										subtype=subtype.replace(/ /g,'')
									}
									
								count++;
						});
				//For Bug 21806 ends 
		         count=0;
		        $('input:hidden[name=pk_study]').each(function() 
						{         
						    		if(count==indexNum)	{	    	
						    			pk_study= $(this).val();
						    		}
						    		
						    	count++;
						      				    
						});
		         count=0;
		        $('input:hidden[name=pk_patprot]').each(function() 
						{         
						    		if(count==indexNum)	{	    	
						    			pk_patprot= $(this).val();
						    		}
						    		
						    	count++;
						      				    
						});
		         count=0;
		        $('input:hidden[name=fk_per]').each(function() 
						{         
						    		if(count==indexNum)	{	    	
						    			fk_per= $(this).val();
						    		}
						    		
						    	count++;
						      				    
						});
		         count=0;
		        $('input:hidden[name=statid]').each(function() 
						{         
						    		if(count==indexNum)	{	    	
						    			statid= $(this).val();
						    		}
						    		
						    	count++;
						      				    
						});
		        count=0;
		        $('input:hidden[name=advPk_formquery]').each(function() 
						{         
						    		if(count==indexNum)	{	    	
						    			pk_formquery= $(this).val();
						    			if(pk_formquery=="null"  || pk_formquery==null ){pk_formquery='';}
						    			
						    		}
						    		
						    	count++;
						      				    
						});
		        count=0;
		        $('input:hidden[name=advField_id]').each(function() 
						{         
						    		if(count==indexNum)	{	    	
						    			fieldId= $(this).val();
						    			
						    		}
						    		
						    	count++;
						      				    
						});
		    $('#preview').attr('src', '/velos/jsp/adverseEventScreen.jsp?adveventId='+adv_Id+'&studyId='+pk_study+'&pkey='+fk_per+'&patProtId='+pk_patprot+'&statid='+statid+'&sub_type='+subtype+'&studyVer=&mode=M&src=tdMenuBarItem5&dashBoard=jupiter');
		    $('#preview1').attr('src', '/velos/jsp/addeditquery.jsp?studyId='+pk_study+'&formQueryId='+pk_formquery +'&fieldId='+fieldId+'&filledFormId='+adv_Id+'&sub_type='+subtype+'&from=8&formId=&formType=Advrse_Form&dashBoard=jupiter');
	        var html='<a href="javascript:void(0);" id="frdId" name="frdTab" data-index="'+indexNum+'"  title="Forward" onclick="forwardMove();"><img src="../images/jpg/imagesForward.jpg" alt="Forward" title="Forward" border="0"></a>' ;
      	    var brdhtml='<a href="javascript:void(0);" id="bwdId" name="bwdTab" data-index="'+indexNum+'"  title="Backward" onclick="backwardMove();"><img src="../images/jpg/imagesBackward.jpg" alt="Forward" title="Backward" border="0"></a>' ;
            $("#forwardId").html(html);
            $("#backwardId").html(brdhtml);
            $("#formNameId").html('Adverse Type:'+data['adv_type']);
          	$("#entryDate").html('Start Date:'+data['adv_Sdate']);
          	if(data['adv_cat']==null){
          	$("#frmStatus").html('Category:');
          	}else{
          		$("#frmStatus").html('Category:'+data['adv_cat']);
          	}
             $("#responseId").html('Adverse Id:'+adv_Id);
             $("#PatientStudyID").html('Patient Study ID:'+data['patient_study_id']);
             $("#organization").html('Organization:'+data['organization']);
             $("#studyId").html('Study Number:'+data['studynum']);
             var subType=data['sub_type'];
            
             if('soft_lock'==subtype){
            	 $("#new1icon").html('&nbsp;&nbsp;&nbsp;<img src="./images/ctrp_images/softlock.png" title="SOFTLOCK" border="0">');
             }
             
             else if('ae_lockdown'==subtype){
                        $("#new1icon").html('&nbsp;&nbsp;&nbsp;<img src="./images/ctrp_images/exclaimation.png" title="HARDLOCK" border="0">');
                        }
    		 else if('soft_lock'!=subtype || 'ae_lockdown'!=subtype){
        		   $("#new1icon").html('');
        	   }
    		 			else{
                       	 $("#backwardId").html(brdhtml);
                        }
    		 //For bug 21806 ends
    		 queryManagement.formResAndQueryView();
	   }
	   if(tabSelected==2){
		   table = $('#patStdStatTable').DataTable();
		    allData = table.rows().data(); 
		    var pk_formquery='';
			var fieldId='';
			var site_id='';
	         if(direction=='forward'){
			  var indexNum=indexNum+1;
			  if(indexNum>=allData.length){
				  indexNum=0;
			  }
		  }else{
			  if(indexNum<=0){
				   indexNum=allData.length-1;
			  }else{
			   indexNum=indexNum-1;
			  }
		  }
	          
	        data = table.row(indexNum).data(); 
	        var count=0;
			
	        $('input:hidden[name=pst_study]').each(function() 
					{         
					    		if(count==indexNum)	{	    	
					    			pk_study= $(this).val();
					    			
					    			
					    		}
					    		
					    	count++;
					      				    
					});
	         count=0;
	        $('input:hidden[name=pstfk_per]').each(function() 
					{         
					    		if(count==indexNum)	{	    	
					    			fk_per= $(this).val();
					    			
					    		}
					    		
					    	count++;
					      				    
					});
	         count=0;
	        $('input:hidden[name=pst_statid]').each(function() 
					{         
					    		if(count==indexNum)	{	    	
					    			statid= $(this).val();
					    			
					    		}
					    		
					    	count++;
					      				    
					});
	        count=0;
	        $('input:hidden[name=pstField_id]').each(function() 
					{         
					    		if(count==indexNum)	{	    	
					    			fieldId= $(this).val();
					    			
					    		}
					    		
					    	count++;
					      				    
					});
	        count=0;
	        $('input:hidden[name=pst_formquery]').each(function() 
					{         
					    		if(count==indexNum)	{	    	
					    			pk_formquery= $(this).val();
					    			if(pk_formquery=="null"  || pk_formquery==null ){pk_formquery='';}
					    		}
					    		
					    	count++;
					      				    
					});
	        count=0;
	        $('input:hidden[name=site_id]').each(function() 
					{         
					    		if(count==indexNum)	{	    	
					    			site_id= $(this).val();
					    			alert("after move site_id-"+site_id);
					    		}
					    		
					    	count++;
					      				    
					});
	        $('#preview').attr('src', '/velos/jsp/patstudystatus.jsp?studyId='+pk_study+'&pkey='+fk_per+'&dashBoard=jupiter&statid='+statid+'&changeStatusMode=no');
		//	$('#preview1').attr('src', 'about:blank');
			$('#preview1').attr('src', '/velos/jsp/addeditquery.jsp?studyId='+pk_study+'&formQueryId='+pk_formquery +'&fieldId='+fieldId+'&filledFormId='+statid+'&from=9&formId=&formType=PatStudy_Form&dashBoard=jupiter&site_id='+site_id);
			var html='<a href="javascript:void(0);" id="frdId" name="frdTab" data-index="'+indexNum+'"  title="Forward" onclick="forwardMove();"><img src="../images/jpg/imagesForward.jpg" alt="Forward" title="Forward" border="0"></a>' ;
     	     var brdhtml='<a href="javascript:void(0);" id="bwdId" name="bwdTab" data-index="'+indexNum+'"  title="Backward" onclick="backwardMove();"><img src="../images/jpg/imagesBackward.jpg" alt="Forward" title="Backward" border="0"></a>' ;
     	    $("#forwardId").html(html);
            $("#backwardId").html(brdhtml);
            $("#formNameId").html('Form Name:'+"Patient Study Status");
            $("#entryDate").html('Date Entry:'+data['created_on']);
         	 $("#frmStatus").html('Patient Status:'+data['Stat']);
            $("#responseId").html('Response Id:'+statid);
            if(data['calender']==null){
              	$("#calender").html('Calender:');
              	}else{
              		$("#calender").html('Calender:'+data['calender']);
              	}
        	if(data['event']==null){
              	$("#event").html('Event:');
              	}else{
              		$("#event").html('Event:'+data['event']);
              	}
        	if(data['visit']==null){
              	$("#visit").html('Visit:');
              	}else{
              		$("#visit").html('Visit:'+data['visit']);
              	}

        	$("#organization").html('Organization:'+data['Org']);
        	$("#PatientStudyID").html('Patient Study ID:'+data['PATSTDID']);
            $("#studyId").html('Study Number:'+data['study_number']);
	   }
//      $.ajax({
//		   url: urls.dashboardUtils.patStudyFormQuery,
//	        type: 'GET',
//	        global: true,
//	        dataType: 'json',
//	        async: false,
//	        data: {
//		   'indexNum':indexNum
//		   
//	              },
//	   		cache: false,
//	        success: function (data) {
//	            	  if(formType=='SP'){
//	            	  var formPullDown="";
//	            	  formPullDown=data[0].fk_form+"*"+"M"+"*"+dataCount;
//	            	 
//	            	  queryManagement.patStudyQuery(data[0].fk_filledform);
//	            	  var html='<a href="javascript:void(0);" id="frdId" name="frdTab" data-index="'+indexNum+'"  title="Forward" onclick="forwardMove();"><img src="../images/jpg/imagesForward.jpg" alt="Forward" title="Forward" border="0"></a>' ;
//	            	  var brdhtml='<a href="javascript:void(0);" id="bwdId" name="bwdTab" data-index="'+indexNum+'"  title="Backward" onclick="backwardMove();"><img src="../images/jpg/imagesBackward.jpg" alt="Forward" title="Backward" border="0"></a>' ;
//                      $("#forwardId").html(html);
//                      $("#backwardId").html(brdhtml);
//                     
//                      $('#preview').attr('src', '/velos/jsp/patstudyformdetails.jsp?studyNumber='+data[0].study_number+"&patStudyId="+data[0].per_code+"&calledFromForm=dashboard&studyId="+data[0].pk_study+"&pkey="+data[0].pk_per+"&formId="+data[0].fk_form+"&formLibVer="+data[0].pk_formLibver+"&patProtId="+data[0].pk_patprot+"&formFillDt=ALL&fkStorageForSpecimen=&filledFormId="+data[0].fk_filledform+"&mode=M&formPullDown="+formPullDown+"&entryChar="+data[0].lf_entrychar);
//                      queryManagement.formResAndQueryView();
//	            	  }
//	            	  else if(formType=='A'){
//	            		  alert("Account type form will Display using move");
//	            	  }
//	            	  
//	   },
//	   error:function(){
//     	 alert("error");
//      }
//	   });   
   }
   queryManagement.patStudyQuery=function(filledForm){
	   
	   $.ajax({
		   url: urls.dashboardUtils.patStudyFormQueryFetch,
	        type: 'GET',
	        global: true,
	        dataType: 'json',
	        async: false,
	        data: {
		   'filledForm':filledForm
		   
	              },
	        cache: false,
	        success: function (data) {
	            	  
	            	  if(data.length==0){
	            	  
	            	  $('#preview1').attr('src', '/velos/jsp/formQueryHistory.jsp?studyId=&formQueryId=&fieldName=&filledFormId=&from=4&formId=');
	            	  }else{
	            		  $('#preview1').attr('src', '/velos/jsp/formQueryHistory.jsp?studyId='+data[0].pk_study +'&formQueryId='+data[0].pk_formquery +'&fieldName='+data[0].fld_name+'&filledFormId='+data[0].filledFormedId+'&from=4&formId='+data[0].formLib);
	            	  }
	              },
	        error:function(){
	              	 alert("error");
	               }
	   });   
   }
   queryManagement.accountFormAndQuery=function(){
	   alert("accountForm and query found");
	   $.ajax({
		   url: urls.dashboardUtils.accountFormQueryFetch,
	        type: 'GET',
	        global: true,
	        dataType: 'json',
	        async: false,
	        cache: false,
		   success: function (data) {
		   alert("data in accountFormAndQuery-"+data.length);
	   },
	   error:function(){
        	 alert("error");
         }
	   });   
   }
   $('#formQueriesTable tbody').on( 'click', 'tr', function () {
	   alert("click on row");
	   var rowData = table.row( this ).data();
	   // ... do something with `rowData`
	 } );
   queryManagement.formResponseAudit=function(el){
       var tr=$(el).closest("tr")[0];
	   var data = oTable.fnGetData(tr);
	   var studyIds=$('#selectedStudies').val();
	   var orgIds=$('#selectedOrgs').val();
	   if(studyIds!=''){
		   if(orgIds!=''){
			   if(orgIds==0){
			   window.open('/velos/jsp/repRetrieve.jsp?repName=Form Audit Trail &filterType=A&repId=101&repFilledFormId='+data['response_id'],'_newtab');
		   }
			   else
			   {
				   window.open('/velos/jsp/repRetrieve.jsp?repName=Form Audit Trail &filterType=A&repId=99&repFilledFormId='+data['response_id'],'_newtab');
			   }
		   }
		   else{
	         window.open('/velos/jsp/repRetrieve.jsp?repName=Form Audit Trail &filterType=A&repId=99&repFilledFormId='+data['response_id'],'_newtab');
		   }
	   }
   }
   /* formResponseTrackChanges Function is not working after resolve the bug no#23196 */
   queryManagement.formResponseTrackChanges=function(el){
	   var tr=$(el).closest("tr")[0];
	   var data = oTable.fnGetData(tr);
	   var studyIds=$('#selectedStudies').val();
	   var orgIds=$('#selectedOrgs').val();
	   if(studyIds!=''){
		   if(orgIds!=''){
			   window.open('/velos/jsp/repRetrieve.jsp?repName=Track Changes&displayMode=E&filterType=A&repId=99&repFilledFormId='+data['response_id'],'_newtab');
		   }
		   else{
	         window.open('/velos/jsp/repRetrieve.jsp?repName=Track Changes&displayMode=E&filterType=A&repId=101&repFilledFormId='+data['response_id'],'_newtab');
		   }
	   } 
   }
   
   
   
   
  queryManagement.deleteSaveSearch=function(){
	   
	   
	   var that = this;
	   str = $("input[name='savedSearch']:checked").attr('id');
	   var str_sub = str.substr(str.lastIndexOf("_")+1);
	   var searchData=jQuery("input:radio[name=savedSearch]:checked").val();
	   if(searchData!="none"){
		   var r = confirm("Do you want to delete saved search");
		   }else{
			   alert("The selected option cannot be deleted ");
			}
	    if (r == true) {
	    	str_sub=queryManagement.encodeUrlParam($('#savedsrchcnt1_'+str_sub).val());
	   $.ajax({
		   url: urls.dashboardUtils.deleteSavedSearches,
	        type: 'GET',
	        global: true,
	        dataType: 'json',
	        async: false,
	        cache: false,
	        data : { 
		   		'savedSearchId'	 : str_sub		   
	   			},
		   success: function (data) {
	   				var tabSelected =  $("#queryTabs").tabs('option', 'active');
	   				
	   				if(tabSelected==0){
	   				
	   				 queryManagement.pageLoaderShow();
	   		        queryManagement.clearFilters(0);
	   		        $("#adverseEventsTable").dataTable().fnDestroy();
	   		        $("#formQueriesTable").dataTable().fnDestroy();    
	   		    	$("#formQueriesTable").empty();
	   		    	addformQueryTablenodes();
	   				
	   				
	   		     $('#formRespTabHeader').html('');
	          	$('#formQueriesTab').html("<p>"+"\""+"Please select a saved search or one or more studies from the left hand panel to begin your search. Use other filters as needed."+"\""+"</p>");

	          queryManagement.renderUpdateDiv('#formQueriesTable_filter');
	     	queryManagement.pageLoaderHide();
	 	queryManagement.renderSearchTab(0);
	   }
	   				if(tabSelected==1){
	   					
	   					
	   				  queryManagement.pageLoaderShow();
	   		        queryManagement.clearFilters(1);
	   		        $("#formQueriesTable").dataTable().fnDestroy();
	   		        $("#adverseEventsTable").dataTable().fnDestroy();    
	   		    	$("#adverseEventsTable").empty();
	   		    	adverseEventsTablenodes();
	   		   
	   		            	$('#advEveTabHeader').html('');
	   		            	$('#adverseEventsTab').html("<p>"+"\""+"Please select a saved search or one or more studies from the left hand panel to begin your search. Use other filters as needed."+"\""+"</p>");
	   		            	
	   		            
	   		    	queryManagement.renderUpdateDiv('#adverseEventsTable_filter');
	   		    	queryManagement.pageLoaderHide();
	   			queryManagement.renderSearchTab(1);
	   				}
	   			
	   			
	   				if(tabSelected==2)
	   				{
	   					
	   					
	   					
	   					
	   					queryManagement.pageLoaderShow();
	   		         queryManagement.clearFilters(2);
	   		         $("#patStdStatTable").dataTable().fnDestroy();    
	   		    	$("#patStdStatTable").empty();
	   		    	patStdStatTablenodes();
	   		    	$('#patStdStatTabHeader').html('');
	   		            	$('#patStdStatTab').html("<p>"+"\""+"Please select a saved search or one or more studies from the left hand panel to begin your search. Use other filters as needed."+"\""+"</p>");
	   		            	
	   		    	queryManagement.pageLoaderHide();
	   			queryManagement.renderSearchTab(2);
	   				}
	   			},
	   error:function(){
        	 alert("error");
         }
	   });  
	    }
	    else{
	    	//donot delete
	    	
	    }
	   
   }
   function fnOnceEnterKeyPress(event) {
	 
	   try {
		   
		   if($("#selectedTotalRespQuery").val().length==1 && event.keyCode == 8){
			   
			   $("#selectedTotalRespQuery").val('');
			   totalRes();
		   }else if (event.keyCode == 13 || event.keyCode == 10) {
	        	
	        	totalRes();
	        }
		} catch(e) {}
		return true;
	   
	}
   function fnOnceEnterKeyPressForQuery(event) {
		 
	   try {
		   
		   if($("#selectedqueryStatuess").val().length==1 && event.keyCode == 8 ){
			   
			   $("#selectedqueryStatuess").val('');
			   totalRes();
		   }		
		   
		   else if (event.keyCode == 13 || event.keyCode == 10) {
	        	
	        	totalRes();
	        }
		} catch(e) {}
		return true;
	   
	}
  
function totalRes(){
	

	
	if($("#selectedStudies").val() == '' || $("#selectedStudies").val() == null){
 		$('#selectedTotalRespQuery').val('');
 		$('#selectedqueryStatuess').val('');
 				
 		alert("select a study first");	 		
 	}
	
	var tabSelected =  $("#queryTabs").tabs('option', 'active');
    
	if(tabSelected ==0){
    queryManagement.pageLoaderShow();
    $("#formQueriesTable").dataTable().fnDestroy();    
	$("#formQueriesTable").empty();
	addformQueryTablenodes();
	queryManagement.renderFormQueryDataTable();
	queryManagement.renderUpdateDiv('#formQueriesTable_filter');
	queryManagement.pageLoaderHide();}
    
    else if(tabSelected==1){
    queryManagement.pageLoaderShow();
    $("#adverseEventsTable").dataTable().fnDestroy();    
    $("#adverseEventsTable").empty();
    adverseEventsTablenodes();
    queryManagement.renderAdvEventQueriesDataTable();
    queryManagement.renderUpdateDiv('#adverseEventsTable_filter');
    queryManagement.pageLoaderHide();
    }
    
    else if(tabSelected==2){
    queryManagement.pageLoaderShow();
    $("#patStdStatTable").dataTable().fnDestroy();    
   	$("#patStdStatTable").empty();
   	patStdStatTablenodes();
   	queryManagement.renderPatStdStatDataTable();
   	queryManagement.pageLoaderHide();
    }
    
    else{
    queryManagement.pageLoaderShow();
    $("#patDemographicsTable").dataTable().fnDestroy();    
    $("#patDemographicsTable").empty();
    patDemographicsTablenodes();
    queryManagement.renderpatDemographicsDataTable();
    queryManagement.pageLoaderHide();
    }
    

	
}
   
   
   
   
   
   
   
/* @Hemant line #(1586-1587,1592-1593) fixed for bug 21122,21123 */
  queryManagement.removeSelect2Filter=function(){
	  var strId=0;
	  var strText='';
	  var data1 = [];
	  var selections = ( JSON.stringify($("#selectedStudies").select2('data')) );
	  var json = $.parseJSON(selections);
	  $(json).each(function(i,val){
		    $.each(val,function(k,v){ 
	        var str=v.toString();
		    if(i!=json.length-1){
		    	if(k=='id'){
		    		strId=val[k];
		    	}if(k=='text'){
		    		strText=val[k];
		    		}
		    	
		    }
	        
	});
		 
		    data1.push({id: strId, text: strText});
		   
		 
		
	});
	  $("#selectedStudies").select2('data', data1); 
  }
  
  
  queryManagement.sameTeamRoleforStudy=function(searchCriArr){
	  
	  var ids='';
	  var data1=[];
	  $(searchCriArr).each(function(i,val){
	    	 
 		 var studySplit=val.split(':');
 		 ids=ids+studySplit[0]+' ,';
 		
 	
  });
	  
	  ids=ids.substring(0,ids.length-1);
	  $.ajax({
  		url: urls.dashboardUtils.studyTeamRoleQueryFetch,
          type: 'GET',
          global: true,
          dataType: 'json',
          async: false,
          data: {
              'studyIds': ids
          },
          cache: false,
          success: function (jsonData) {
        	  var tRoleFlag=0;
        	  /* If no study team role is defined in the ER_CODELST   */
        	  if(jsonData.length!=0){
        		  if(jsonData[0].codelst_subtyp=='tRoleNotRequired'){
        			  tRoleFlag=1;
        			  $(searchCriArr).each(function(i,val){
        				  var studySplit=val.split(':');
        				  data1.push(studySplit[0]+":"+studySplit[1]);
        			  });
        		  }
        	  }
        	  /* End  If no study team role is defined in the ER_CODELST   */
        	  if(tRoleFlag==0){
        	  var role='';
        	  var  flag='';
        	  var searchIndex=0;
        	  $(searchCriArr).each(function(i,val){
        		  var studySplit=val.split(':');
        		  var id=studySplit[0];
        		  searchIndex=0;
     	    	 for(var k=0;k<jsonData.length;k++){
        	 		if(id==jsonData[k].pk_study){
        	 			role=jsonData[k].codelst_subtyp;
        	 			flag=true;
        	 			searchIndex=searchIndex+1;
        	 		}
        	 		 
     	    	 
     	    	 }
     	    	 if(searchIndex == 0 ){
     	    		role=queryManagement.defaultStRoleForSuperUser();  
     	    	 }
     	    	 if(i==0){
     	    		 if(flag==true){
     	    			filterStudyTRole=role;
     	    			data1.push(studySplit[0]+":"+studySplit[1]);
     	    			flag=false;
     	    		 }else{
     	    			filterStudyTRole=role;
     	    			data1.push(studySplit[0]+":"+studySplit[1]);
     	    			flag=false;
     	    		 }
     	    	 }
     	    	 if(i>0){
     	    			if(filterStudyTRole==role){
     	    				data1.push(studySplit[0]+":"+studySplit[1]);
     	    			}
     	    	 }
     	    	 
        	  });
          }
          },error: function (xhr, status, errorThrown) {
              console.log('errorThrown  ' + errorThrown);
              return null;
          }
  	});
	  clearStudyRoleForTab=1;
	  return data1;
  }/* End of sameTeamRoleforStudy Function and need to optimize*/
  queryManagement.defaultElemt=function(tabIndex){
	  var selectVar=$("#selectedForms").val();
	  var selectedstudy=$("#selectedStudies").val();
	  if(tabIndex==0){
		  document.getElementById('formid').style.display ='block';
		  document.getElementById('advFormid').style.display ='none';
		  document.getElementById('patStudyFormid').style.display ='none';
	  }
	  
	  if(tabIndex==1){
		 if (selectVar!='' || selectVar!=null ){
		  document.getElementById('formid').style.display ='none';
		  document.getElementById('advFormid').style.display ='block';
		  document.getElementById('patStudyFormid').style.display ='none';
		  document.getElementById('selectedAdvForms').value='Adverse Events';
		  $("#selectedForms").select2('data', null);
		  if(selectedstudy=='')
		  {
			  $("#selectedAdvForms").val('');
		  }
		  }
	  }
	  if(tabIndex==2){
		  document.getElementById('formid').style.display ='none';
		  document.getElementById('advFormid').style.display ='none';
		  document.getElementById('patStudyFormid').style.display ='block';
		  document.getElementById('selectedPatStudyForms').value=""+L_Pat_StdStatus+"";
		  if(selectedstudy=='')
		  {
			  $("#selectedPatStudyForms").val('');
		  }
	  }
  }
  
  queryManagement.commonFiltersSetting=function(tabSelected){
      document.getElementById('formid').style.display ='block';
	  
	  if(tabSelected==0){
		  document.getElementById('formid').style.display ='block';
		  document.getElementById('formstatus').style.display ='block';
			document.getElementById('advevntstatus').style.display ='none';
			$("#selectedFormStatues").select2('data', null);
	  }
	  if(tabSelected==1){
		  
		  document.getElementById('formid').style.display ='none';
		  document.getElementById('formstatus').style.display ='none';
		  document.getElementById('advevntstatus').style.display ='block';
			$("#selectedForms").select2('data', null);
		  
	  }
	  if(tabSelected==2){
		  document.getElementById('formid').style.display ='none';
		  document.getElementById('formstatus').style.display ='block';			
	      document.getElementById('advevntstatus').style.display ='none';
	      $("#selectedFormStatues").select2('data', null);
	  }
	  
		document.getElementById('patid').style.display ='block';
		
		document.getElementById('patstdid').style.display ='block';

		document.getElementById('calid').style.display ='none'

		document.getElementById('visitid').style.display = 'none'; 

		document.getElementById('eventid').style.display = 'none';

		document.getElementById('evntdt').style.display = 'none';
  }
  queryManagement.latestTeamroleForMultipleStudy=function(eventFrom){
	 var dataCount=0;
	  var selections = ( JSON.stringify($("#selectedStudies").select2('data')) );
	  var json = $.parseJSON(selections);
	  if(eventFrom=='select2Changed'){		  
		  dataCount=json.length-1;
	  }
	  else{
		  dataCount=json.length;
	  }
	  var studyId='';
	  $(json).each(function(i,val){
		 
		  $.each(val,function(k,v){ 
		        if(k=='id'){
		        	studyId=studyId+v+',';
		        }
			    
		        
		});
		 
	  });
	  studyId=studyId.substring(0,studyId.length-1);
	  var findRole=0;
	  var role='';
	  var data1=[];
	  var select2Id=0;
	  var select2Text='';
	  $.ajax({
		  url: urls.dashboardUtils.studyTeamRoleQueryFetch,
          type: 'GET',
          global: true,
          dataType: 'json',
          async: false, 
          data: {
          'studyIds': studyId
      },
      cache: false,
      success: function (jsonData) {
    	 if(jsonData.length==0){
    		 studyTeamrole=queryManagement.defaultStRoleForSuperUser();
    		 filterEmtyFlag=0;
    		 
    		 
    	 }else{
    		 if(jsonData[0].codelst_subtyp=='teamRoleNOtRequired'){
    			 alertFlag=0;
    		 }else{
    		 $(json).each(function(i,val){
    			 if(studyTeamrole=='default'){
    				 role="default";
     				$.each(val,function(k,v){
     					if(k=='id'){
     						select2Id=v;
     						for(var j=0;j<jsonData.length;j++){
     							if(v==jsonData[j].pk_study){
     								role=jsonData[j].codelst_subtyp;
     							}
     						}
     					}
     					 if(k=='text'){
        					  select2Text=v;
        				  }
     				}); 
     					
     			}
     				
     			else{
    				$.each(val,function(k,v){
    					
    			        if(k=='id'){
    			        	var searchIndex=0;
    			        	select2Id=v;
    			        	for(var j=0;j<jsonData.length;j++){
    			        		if(v==jsonData[j].pk_study){
    			        			if(i==0){
    			      				  studyTeamrole=jsonData[j].codelst_subtyp;
    			      				  filterEmtyFlag=0;
    			      				  findRole=1;
    			      				
    			      			  } 
    			        			else{
    			        			role=	jsonData[j].codelst_subtyp;
    			        			}
    			        			searchIndex=searchIndex+1;	
    			        		
    			        		}
    			        	}
    			        	if(findRole==0){
    			        		studyTeamrole=queryManagement.defaultStRoleForSuperUser();
    			        		filterEmtyFlag=0;
    			       		    findRole=1;
    			        	}
    			        	if(searchIndex==0 || findRole==0){
    			        		role=queryManagement.defaultStRoleForSuperUser();
    			        		
    			        	}
    			        	
    			        } //Ending Point of  if(k=='id')
    				  if(k=='text'){
    					  select2Text=v;
    				  }
    				  
    			        
    			}); //Ending Point of   $.each(val,function(k,v)
    		 }	
    			
    			if(i==0){
    				data1.push({id: select2Id, text: select2Text});
    				
    			}else{
    				if(role==studyTeamrole){
    					data1.push({id: select2Id, text: select2Text});
    				}else{alertFlag=1;}
    			}
    		 
    		  }); 
    		 $("#selectedStudies").select2('data', data1); 
    	 }
      }
    	
      },error: function (xhr, status, errorThrown) {
          console.log('errorThrown  ' + errorThrown);
          return null;
      }
      
	  });
	  
  } /* End of latestTeamroleForMultipleStudy Function and   need to optimize*/
	/******** Save View implementation to hide column of each tab in Dash board after click on Show/Hide Button:*************
                                                 Bug No:#22565  
*/
  /************This method is calling from queryManagement.html after click on Show/Hide Button in Dash board***********/
  queryManagement.showHideButtonClick=function(){
	  var tabSelected =  $("#queryTabs").tabs('option', 'active');
        $('ul.ColVis_collection').each(function() {
	    if ($(this).css('opacity') == '1') {
	    	if(tabSelected==0){
	    		var i=0;
	    	$(this).attr('id', 'tab0');
	    	$('ul.ColVis_collection:last input[type=checkbox]').each(function() {
	    		
	    		if(i==0){
	    		$(this).closest('label').find('span').html('<img src="./images/edit.gif" border="0">');
	    		
	    		}
	    		if(i==1){
	    			$(this).closest('label').find('span').html('<img src="../images/help.png" border="0">');
	    		}
	    		if(i==20){
	    			$(this).closest('label').find('span').html('Audit');
	    		}
	    		if(i==21){
	    			$(this).closest('label').find('span').html('<img src="./images/chkbox.jpg" border="0">');
	    		}
	    		i=i+1;
	    	});
	    	
	    	}
	    	else if(tabSelected==1){
	    		var i=0;
	    		$(this).attr('id', 'tab1');
	    		
	    		$('ul.ColVis_collection:last input[type=checkbox]').each(function() {
	    			if(i==0){
	    	    		$(this).closest('label').find('span').html('<img src="./images/edit.gif" border="0">');
	    			}
		    		if(i==1){
		    			$(this).closest('label').find('span').html('<img src="../images/help.png" border="0">');
		    		}
		    		if(i==13){
		    			$(this).closest('label').find('span').html('Grade');
		    		}
		    		if(i==31){
		    			$(this).closest('label').find('span').html('<img src="./images/chkbox.jpg" border="0">');
		    		}
	    		i=i+1;
	    			
	    	});
	    	
	    	}
	    	
	    	
	    	else if(tabSelected==2){
	    		
	    		var i=0;
	    		$(this).attr('id', 'tab2');
	    	
	    		$('ul.ColVis_collection:last input[type=checkbox]').each(function() {
	    			
	    			if(i==0){
	    	    		$(this).closest('label').find('span').html('<img src="./images/edit.gif" border="0">');
	    			}
		    		if(i==1){
		    			$(this).closest('label').find('span').html('<img src="../images/help.png" border="0">');
		    		}
		    		if(i==12){
		    			$(this).closest('label').find('span').html('Current status');
		    		}
	    		i=i+1;
	    	});
	    			
	    	}
	    	else if(tabSelected==3){$(this).attr('id', 'tab3');}
	    	
	    }
	});
  }
  queryManagement.selectColumnAfterSave=function(tabSelected){
	  var showHideColumn=[];
	  if(tabSelected==0){
		  showHideColumn=tab0ShowAndHide;
		  oTable.fnSetColumnVis(5, false );
		  oTable.fnSetColumnVis(11, false );
	      oTable.fnSetColumnVis(14, false ); 
	      oTable.fnSetColumnVis(15, false ); 
	      oTable.fnSetColumnVis(17, false ); 
	      oTable.fnSetColumnVis(18, false ); 
	      oTable.fnSetColumnVis(19, false ); 
	      oTable.fnSetColumnVis(20, false );
		  for(columnIndex=0;columnIndex<tab0ShowAndHide.length;columnIndex++){
		  		var checkedSts=tab0ShowAndHide[columnIndex];
		  		var splitSta=checkedSts.split(':');
		  		 if(splitSta[0]==0){
		  			 oTable.fnSetColumnVis(columnIndex, false );
					   }else{
						   oTable.fnSetColumnVis(columnIndex, true );   
					   }
		  		
		      	
		  	}
	  }
	  else if(tabSelected==1){
		  showHideColumn=tab1ShowAndHide;
		  
		  oTable.fnSetColumnVis(5, false );
	      oTable.fnSetColumnVis(6, false ); 
	      oTable.fnSetColumnVis(11, false ); 
	      oTable.fnSetColumnVis(14, false ); 
	      oTable.fnSetColumnVis(15, false ); 
	      oTable.fnSetColumnVis(17, false ); 
	      oTable.fnSetColumnVis(19, false ); 
	      oTable.fnSetColumnVis(21, false );
	      oTable.fnSetColumnVis(22, false );
	      oTable.fnSetColumnVis(23, false ); 
	      oTable.fnSetColumnVis(24, false ); 
	      oTable.fnSetColumnVis(25, false ); 
	      oTable.fnSetColumnVis(26, false ); 
	      oTable.fnSetColumnVis(27, false ); 
	      oTable.fnSetColumnVis(28, false );
	      oTable.fnSetColumnVis(30, false );
		  
		  for(columnIndex=0;columnIndex<tab1ShowAndHide.length;columnIndex++){
		  		var checkedSts=tab1ShowAndHide[columnIndex];
		  		var splitSta=checkedSts.split(':');
		  		 if(splitSta[0]==0){
		  			 oTable.fnSetColumnVis(columnIndex, false );
					   }else{
						   oTable.fnSetColumnVis(columnIndex, true );   
					   }
		  		
		      	
		  	}
		  }
	  else if(tabSelected==2){
		  showHideColumn=tab2ShowAndHide;
		  oTable.fnSetColumnVis(16, false );
		  for(columnIndex=0;columnIndex<tab2ShowAndHide.length;columnIndex++){
		  		var checkedSts=tab2ShowAndHide[columnIndex];
		  		var splitSta=checkedSts.split(':');
		  		 if(splitSta[0]==0){
		  			 oTable.fnSetColumnVis(columnIndex, false );
					   }else{
						   oTable.fnSetColumnVis(columnIndex, true );   
					   }
		  		
		      	
		  	}
		  } 
  }
  
  				/*********** End of Bug No:#22565********************/
  
  queryManagement.refreshPage=function(){
	  var tabSelected =  $("#queryTabs").tabs('option', 'active');	  
	  if(tabSelected==0){
		   filterEmtyFlag=1;
		    if(clearStudyRoleForTab==1){
		    filterStudyTRole='';
		    studyTeamrole='';
		    }
		        queryManagement.pageLoaderShow();
		        $("#adverseEventsTable").dataTable().fnDestroy();
		        $("#formQueriesTable").dataTable().fnDestroy();    
		    	$("#formQueriesTable").empty();
		    	addformQueryTablenodes();
                queryManagement.renderFormQueryDataTable();
		        queryManagement.renderUpdateDiv('#formQueriesTable_filter');
		    	queryManagement.renderSearchTab(0);
			    queryManagement.pageLoaderHide();
	  }
	  else if(tabSelected==1){
		  filterEmtyFlag=1;
		    if(clearStudyRoleForTab==1){
		     filterStudyTRole='';
		     studyTeamrole='';
		    }
		    	queryManagement.pageLoaderShow();
		        $("#formQueriesTable").dataTable().fnDestroy();
		        $("#adverseEventsTable").dataTable().fnDestroy();    
		    	$("#adverseEventsTable").empty();
		        adverseEventsTablenodes();
		        queryManagement.renderAdvEventQueriesDataTable(); 
		        queryManagement.renderUpdateDiv('#adverseEventsTable_filter');
		    	queryManagement.renderSearchTab(1);
			    queryManagement.pageLoaderHide();  
	  }
	  else if(tabSelected==2){
		  filterEmtyFlag=1;
		    if(clearStudyRoleForTab==1){
		     filterStudyTRole='';
		     studyTeamrole='';
		    
		    }
		        queryManagement.pageLoaderShow();
		        $("#patStdStatTable").dataTable().fnDestroy();    
		    	$("#patStdStatTable").empty();
		    	patStdStatTablenodes();
		        queryManagement.renderPatStdStatDataTable();
		        queryManagement.renderSearchTab(2);
		    	queryManagement.pageLoaderHide();
	  }
  }
  queryManagement.defaultStRoleForSuperUser=function(){
	  var role='';
	  $.ajax({
		  url: urls.dashboardUtils.defaultStRoleForSuperUser,
          type: 'GET',
          global: true,
          dataType: 'json',
          async: false,
          cache: false,
      success: function (jsonData) {
		  if(jsonData[0].codelst_subtyp=='' || jsonData[0].codelst_subtyp==null){
			  role="default";
		  }else{
			  role=	jsonData[0].codelst_subtyp;
		  }
		  
      },error: function (xhr, status, errorThrown) {
          console.log('errorThrown  ' + errorThrown);
          return null;
      }
      
	  });
	  return role;
  }
  queryManagement.decodeUrlParam=function(InputStr){
	if (InputStr.indexOf('[SQuote]') >= 0) {
		  InputStr= queryManagement.replaceAll(InputStr,'[SQuote]',"'");
     }
     if (InputStr.indexOf('[DQuote]') >= 0) {
   	  InputStr= queryManagement.replaceAll(InputStr,'[DQuote]',"\"");
     }
     if (InputStr.indexOf('[CRet]') >= 0) {
         InputStr= queryManagement.replaceAll(InputStr,'[CRet]',"\r");
     }
     if (InputStr.indexOf('[NLine]') >= 0) {
   	  InputStr= queryManagement.replaceAll(InputStr,'[NLine]',"\n");

     }
     if (InputStr.indexOf('[HTab]') >= 0) {
   	  InputStr= queryManagement.replaceAll(InputStr,'[HTab]',"\t");
     }
     if (InputStr.indexOf('[BSlash]') >= 0) {
         InputStr= queryManagement.replaceAll(InputStr,'[BSlash]','\\');
     }
     if (InputStr.indexOf('[VelHash]') >= 0) {
   	  InputStr= queryManagement.replaceAll(InputStr,'[VelHash]','#');
     }
     if (InputStr.indexOf('[vcom]') >= 0) {
   	  InputStr= queryManagement.replaceAll(InputStr,'[vcom]',',');
     }
     if (InputStr.indexOf('[vpst]') >= 0) {
   	  InputStr= queryManagement.replaceAll(InputStr,'[vpst]','%');
     }

     if (InputStr.indexOf('[vamp]') >= 0) {
   	  InputStr= queryManagement.replaceAll(InputStr,'[vamp]','&');
     }

     if (InputStr.indexOf('[VELSEMICOLON]') >= 0) {
   	  InputStr= queryManagement.replaceAll(InputStr,'[VELSEMICOLON]',';');
  
     }
     if (InputStr.indexOf('[VELGTSIGN]') >= 0) {
   	  InputStr= queryManagement.replaceAll(InputStr,'[VELGTSIGN]','>');

     }
     if (InputStr.indexOf('[VELLTSIGN]') >= 0) {
   	  InputStr= queryManagement.replaceAll(InputStr,'[VELLTSIGN]','<');

     }
     if (InputStr.indexOf('[vplus]') >= 0) {
   	  InputStr= queryManagement.replaceAll(InputStr,'[vplus]','+');

     }
     
     if (InputStr.indexOf('[VELSP]') >= 0) {
         InputStr= queryManagement.replaceAll(InputStr,'[VELSP]',' ');
     } 
     if (InputStr.indexOf('[veq]') >= 0) {
   	  InputStr= queryManagement.replaceAll(InputStr,'[veq]','=');
     } 
     if (InputStr.indexOf('[vcol]') >= 0) {
    	  InputStr= queryManagement.replaceAll(InputStr,'[vcol]',':');
      } 
     if (InputStr.indexOf('[VELDIV]') >= 0) {
    	  InputStr= queryManagement.replaceAll(InputStr,'[VELDIV]','/');
      } 
     if (InputStr.indexOf('[VELMINUS]') >= 0) {
    	  InputStr= queryManagement.replaceAll(InputStr,'[VELMINUS]','-');
      } 
     if (InputStr.indexOf('[VELMUL]') >= 0) {
    	  InputStr= queryManagement.replaceAll(InputStr,'[VELMUL]','*');
      } 
     return InputStr; 
  }
  queryManagement.encodeUrlParam=function(InputStr){ 
	  if (InputStr.indexOf("'") >= 0) {
		  InputStr= queryManagement.replaceAll(InputStr,"'",'[SQuote]');
      }
      if (InputStr.indexOf("\"") >= 0) {
    	  InputStr= queryManagement.replaceAll(InputStr,"\"",'[DQuote]');
      }
      if (InputStr.indexOf("\r") >= 0) {
          InputStr= queryManagement.replaceAll(InputStr,'\r','[CRet]');
      }
      if (InputStr.indexOf("\n") >= 0) {
    	  InputStr= queryManagement.replaceAll(InputStr,'\n','[NLine]');

      }
      if (InputStr.indexOf("\t") >= 0) {
    	  InputStr= queryManagement.replaceAll(InputStr,'\t','[HTab]');
      }
      if (InputStr.indexOf("\\") >= 0) {
          InputStr= queryManagement.replaceAll(InputStr,'\\','[BSlash]');
      }
      if (InputStr.indexOf("#") >= 0) {
    	  InputStr= queryManagement.replaceAll(InputStr,'#','[VelHash]');
      }
      if (InputStr.indexOf(",") >= 0) {
    	  InputStr= queryManagement.replaceAll(InputStr,',','[vcom]');
      }
      if (InputStr.indexOf("%") >= 0) {
    	  InputStr= queryManagement.replaceAll(InputStr,'%','[vpst]');
      }

      if (InputStr.indexOf("&") >= 0) {
    	  InputStr= queryManagement.replaceAll(InputStr,'&','[vamp]');
      }

      if (InputStr.indexOf(";") >= 0) {
    	  InputStr= queryManagement.replaceAll(InputStr,';','[VELSEMICOLON]');
   
      }
      if (InputStr.indexOf(">") >= 0) {
    	  InputStr= queryManagement.replaceAll(InputStr,'>','[VELGTSIGN]');

      }
      if (InputStr.indexOf("<") >= 0) {
    	  InputStr= queryManagement.replaceAll(InputStr,'<','[VELLTSIGN]');

      }
      if (InputStr.indexOf("+") >= 0) {
    	  InputStr= queryManagement.replaceAll(InputStr,'+','[vplus]');

      }
      
      if (InputStr.indexOf(" ") >= 0) {
          InputStr= queryManagement.replaceAll(InputStr,' ','[VELSP]');
      } 
      if (InputStr.indexOf("=") >= 0) {
    	  InputStr= queryManagement.replaceAll(InputStr,'=','[veq]');
      } 
      if (InputStr.indexOf(":") >= 0) {
     	  InputStr= queryManagement.replaceAll(InputStr,':','[vcol]');
       } 
      if (InputStr.indexOf("/") >= 0) {
     	  InputStr= queryManagement.replaceAll(InputStr,'/','[VELDIV]');
       } 
      if (InputStr.indexOf("-") >= 0) {
     	  InputStr= queryManagement.replaceAll(InputStr,'-','[VELMINUS]');
       } 
      if (InputStr.indexOf("*") >= 0) {
     	  InputStr= queryManagement.replaceAll(InputStr,'*','[VELMUL]');
       } 
      return InputStr; 
  }