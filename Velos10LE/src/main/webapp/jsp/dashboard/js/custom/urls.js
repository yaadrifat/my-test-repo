urls = {
		compliance :{},
		queryManagement : {},
		dashboard :{},
		dashboardUtils :{}
}

urls.dashboardUtils = {
		fetchCodeLstItems : '/velos/jsp/dbFetchCodeLstStaues',
		fetchFormAdvStatus : '/velos/jsp/dbFetchFormAdvStaues',
		fetchSavedSearches : '/velos/jsp/dbFetchSavedSearches',
		deleteSavedSearches : '/velos/jsp/dbDeleteSavedSearches',
		fetchAutoCompleteStudyData : '/velos/jsp/dbFetchStudies',
		fetchAutoCompleteOrgData :  '/velos/jsp/dbFetchOrganizations',
		fetchAutoCompleteFormData :  '/velos/jsp/dbFetchForms',
		fetchAutoCompleteFormStatData :  '/velos/jsp/dbFetchFormStatues',
		fetchAutoCompleteAEStatData :  '/velos/jsp/dbFetchAEStatues',
		fetchAutoCompletePatIDData :  '/velos/jsp/dbFetchPatientIDs',
		fetchAutoCompleteStdPatIDData :  '/velos/jsp/dbFetchPatientStudyIDs',
		fetchAutoCompleteCalData :  '/velos/jsp/dbFetchCalendars',
		fetchAutoCompleteVisitData :  '/velos/jsp/dbFetchVisits',
		fetchAutoCompleteEventData :  '/velos/jsp/dbFetchEvents',
		fetchAutoCompleteQryCrtData :  '/velos/jsp/dbFetchQueryCreators',
		fetchAutoCompleteQryStsData :  '/velos/jsp/dbFetchQueryStatuses',
		SaveFilterCompleteData:  '/velos/jsp/dbSaveFlterData',
		fetchSavedSelected:      '/velos/jsp/dbfetchSavedSelected',
		fetchSavedView:	'/velos/jsp/dbfetchSavedView',
		patStudyFormQuery:'/velos/jsp/patStudyFormQuery',
		patStudyFormQueryFetch:'/velos/jsp/patStudyFormQueryFetch',
		studyTeamRoleQueryFetch : '/velos/jsp/studyTeamRoleQueryFetch',
		defaultAdverseFormData : '/velos/jsp/fetchDefaultAdverseFormData',
		viewRightOrgBaseToUse:'/velos/jsp/viewRightOrgBaseToUse',
		defaultStRoleForSuperUser:'/velos/jsp/fetchDefaultStRoleForSuper'
}
urls.compliance = {
		template : '/velos/jsp/dashboard/templates/compliance.html',
		studyAutoComplete : '/velos/jsp/dbFetchStudies',
		fetchSubmissionDataTable : '/velos/jsp/fetchStdSubmissionData'		
}

urls.queryManagement = {
		template : '/velos/jsp/dashboard/templates/queryManagement.html',
		fetchFormQueries : '/velos/jsp/fetchFormQueries',
		fetchAdvEventQueriesData : '/velos/jsp/fetchAdvEventQueriesData',
		fetchPatStdStatesData : '/velos/jsp/fetchPatStdStatesData',
		fetchPatDemographicsData : '/velos/jsp/fetchPatDemographicsData',
		bulkFormStatusUpdate : '/velos/jsp/updateBulkFormStatusData'
}
