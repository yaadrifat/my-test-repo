<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.*"%>
<HTML>
<HEAD>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>

<BODY>
<form METHOD=POST action="auditreport.jsp">
<%=MC.M_PlsSelRpt_Parameters %><%-- Please select a report and enter the corresponding parameters. Following conditions apply as of now*****--%> :-
<BR><BR>
i) <%=MC.M_CurUsrDbUsr_ToFindFireSql %><%-- Currently the user can only be the database user in your database. To find this fire the SQL 'select distinct user_name from audit_report'. This is case sensitive.*****--%> 
<BR>
ii) <%=MC.M_EtrDtInDtFmt %><%-- Enter dates in mm/dd/yyyy format*****--%>
<BR>
iii) <%=MC.M_DtFromLessThan_DtTo %><%-- Date from should be less than date To.*****--%>
<BR>
iv) <%=MC.M_RptParam_UseParticularRpt %><%-- The report names suggest the parameters that would be used in a particular report.*****--%>
<BR><BR> 

<table>
	<tr>		
	   	<td>
   			<%=LC.L_Rpt_Type %><%-- Report Type*****--%>
	   	</td>
		<td>
			<select name=repnum>
				<option value=1><%=MC.M_OnBasis_OfDates %><%-- On basis of dates*****--%></option>
				<option value=2><%=MC.M_OnBasis_OfAction %><%-- On basis of Action*****--%></option>
				<option value=3><%=MC.M_OnBasisOf_ActAndUsr %><%-- On basis of action and user*****--%></option>
				<option value=4><%=MC.M_BasisOf_ActAndDt %><%-- On basis of action and dates*****--%></option>
				<option value=5><%=MC.M_OnBasis_ActUsrDt %><%-- On basis of action, user and dates*****--%></option>
				<option value=6><%=MC.M_OnBasis_OfUser %><%-- On basis of user*****--%></option>
				<option value=7><%=MC.M_OnBasis_UsrDt %><%-- On basis of user and dates*****--%></option>																				
			</select>
		</td>
	</tr>	
	<tr>
	   	<td>
   			<%=LC.L_Action %><%-- Action*****--%>
	   	</td>
		<td>
			<select name=action>
				<option value=UPDATE><%=LC.L_Update %><%-- Update*****--%></option>
<%--				<option value=>Insert</option> 
--%>
				<option value=DELETE><%=LC.L_Delete %><%-- Delete*****--%></option>				
			</select>
		</td>
	</tr>
	<tr>
	   	<td>
			<%=LC.L_Date_From %><%-- Date From*****--%>    		
	   	</td>
		<td>
			<input name=datefrom>
		</td>
	</tr>
	<tr>		
	   	<td>
   			<%=LC.L_Date_To %><%-- Date To*****--%>
	   	</td>
		<td>
			<input name=dateto>
		</td>
	</tr>
	<tr>
		<td>
			<%=LC.L_User %><%-- User*****--%>
		</td>
		<td>
			<input type="text" name=user value=ER>
		</td>
	</tr>
		

</table>	
<input type=submit name=submit value=Submit>
</form>
</BODY>
</HTML>

