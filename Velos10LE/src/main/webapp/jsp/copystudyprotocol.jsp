<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_CopyStdPcol_Cal%><%--Copy Study Protocol Calendar*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT Language="javascript">

 function  validate(formobj){

     if (!(validate_col('Protocol Id',formobj.protocolId))) return false
     if (!(validate_col('Protocol Name',formobj.protocolName))) return false
	 if (!(validate_col('e-Signature',formobj.eSign))) return false
	 /* Bug#9899 5-Jun-2012 -Sudhir*/
	 //if (!allsplcharcheck(formobj.protocolName.value))  
	//{
	//	formobj.protocolName.focus();  return false;
	//}
 	 
	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }

   }
   
   function  validate1(formobj){

     if (!(validate_col('Protocol Name',formobj.protocolName))) return false
	 if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }

   }
   
   
</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventassocdao" scope="request" class="com.velos.esch.business.common.EventAssocDao"/>

<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*"%>


<% String src;

src= request.getParameter("srcmenu");

%>

<body>
<br>
<DIV class="popDefault" id="div1"> 
  <%

	String protocolId ="";
	String protocolName ="";
	ArrayList eventIds = new ArrayList();
	ArrayList names = new ArrayList(); 
	
	int length = 0;

	String from = request.getParameter("from");

	HttpSession tSession = request.getSession(true); 
    

	if (sessionmaint.isValidSession(tSession))
	{
 %>
  <jsp:include page="sessionlogging.jsp" flush="true"/> 
	 <jsp:include page="include.jsp" flush="true"/>

  <%	
  		String accId = (String) tSession.getValue("accountId");	
		int accountId = EJBUtil.stringToNum(accId);
		String ipAdd = (String) tSession.getValue("ipAdd");
		String usr = (String) tSession.getValue("userId");     
		String study = (String) tSession.getValue("studyId");
		
		String calAssoc=request.getParameter("calassoc");
		calAssoc=(calAssoc==null)?"":calAssoc;
%>

<%
if(from.equals("initial")) {
%>
<P class="sectionHeadings"> <%=MC.M_StdPcol_CalCopy%><%--<%=LC.Std_Study%> Protocol Calendar >> Copy*****--%> </P>
<TABLE width="100%" cellspacing="0" cellpadding="0" >
		<tr>	
			<td width="50%">
			<P> <%=MC.M_SelPcol_ToCopy%><%--Please select the Protocol to be copied*****--%> <FONT class="Mandatory">* </FONT>  </P>
			</td>
			<%
			String eventType="P";
			if (calAssoc.equals("S"))
				eventassocdao= eventassocB.getStudyAdminProts(EJBUtil.stringToNum(study),"");
			else
			eventassocdao= eventassocB.getStudyProts(EJBUtil.stringToNum(study));
			
			eventIds=eventassocdao.getEvent_ids() ; 
			names= eventassocdao.getNames(); 
			length = eventIds.size();
			%>
			
			<Form name="select" id="selectFrm" method="post" action="copystudyprotocol.jsp?from=final&srcmenu=<%=src%>&calassoc=<%=calAssoc%>" onsubmit="if (validate(document.select)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
			
			<td width="50%">
			<select  name="protocolId">
			<OPTION value ='' SELECTED><%=LC.L_Select_AnOption%><%--Select an Option*****--%></OPTION> 

			<%
			for(int i = 0; i< length;i++)
			{
			%>
			<option value=<%=eventIds.get(i)%>> <%=names.get(i)%> </option>
			<%
			}
			%>
			</select>
			</td>
			<tr>
			<td width="50%">
			<P > <%=MC.M_PlsEtrName_GivenCopyPcol%><%--Please enter a name to be given to the copied protocol*****--%> <FONT class="Mandatory">* </FONT>  </P>
			</td>
			<td width="50%">
			<input type="text" name="protocolName">
			</td>
		</tr>
		
	<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
		<jsp:include page="submitBar.jsp" flush="true"> 
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="selectFrm"/>
				<jsp:param name="showDiscard" value="N"/>
		</jsp:include>
		

 </table>
</form>

<%
} //end of if for checking whether from.equals("initial")


if(from.equals("final")) {
 String eSign = request.getParameter("eSign");
 String oldESign = (String) tSession.getValue("eSign");
 if(!oldESign.equals(eSign)) {

 %>
 
 <br> <br> 
 <table width=100%>
<tr>
<td align=center>
<p class = "sectionHeadings">

<%=MC.M_EtrWrongEsign_ClkBack%><%--You have entered a wrong e-Signature. Please click on Back button to enter again.*****--%>

</p>
</td>
</tr>

<tr height=20></tr>
<tr>
<td align=center>

	<!--	<button onclick="window.history.back();"><%=LC.L_Back%></button>-->
	<button onClick="history.go(-1);"><%=LC.L_Back%></button>	
</td>		
</tr>		
</table>		
 	
 <%
  } else {
 
   protocolId = request.getParameter("protocolId");
   protocolName = StringUtil.stripScript(request.getParameter("protocolName"));

//  if(eventassocB.copyStudyProtocol(protocolId,protocolName,usr,ipAdd,accId) == -1) { --Amar
    
 // Added by Amarnadh to Copying Calendar in Study setup/Admin schedule 'Define the Calendar' be opened    
  int newProtocolId = eventassocB.copyStudyProtocol(protocolId,protocolName,usr,ipAdd,accId,calAssoc);
  Object[] arguments = {protocolName};
  if(newProtocolId == -1) {

%>

<Form name="name" id="nameDup" method="post" action="copystudyprotocol.jsp?from=final&srcmenu=<%=src%>&calassoc=<%=calAssoc%>" onsubmit="if (validate1(document.name)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
<P class="defComments">
	<Font class="mandatory">
	<%=VelosResourceBundle.getMessageString("M_PcolName_EtrExists",arguments)%><%--The protocol name you have entered already exists.*****--%>
	</Font>
</P>
<TABLE width="100%" cellspacing="0" cellpadding="0" >
<tr>
<td width="20%">
	<input type=hidden name="protocolId" value="<%=protocolId%>">
	<label><%=LC.L_Cal_Name%><%--Calendar name.*****--%></label>
</td>
<td align="left">
	<input type="text" name="protocolName" value="<%=protocolName%>">
</td>
</tr>
<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
<tr>
<td align="left" colspan="2">
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="nameDup"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>
</td>
</tr>


</table>

<%
   } else {

	
%>
<BR>
<BR>

<P class = "sectionHeadings" align="center"> <%=MC.M_Data_SavedSucc%><%--Data saved successfully*****--%> </P>

  <script>

//  window.opener.location.reload(); -- Amar
/*  Added by Amarnadh to Copying Calendar in Study setup/Admin schedule 'Define the Calendar' be opened */
window.opener.location.href = "protocolcalendar.jsp?mode=M&srcmenu=<%=src%>&selectedTab=1&calledFrom=S&protocolId=<%=newProtocolId%>&pageNo=1&displayType=V&headingNo=1&displayDur=3&protocolName=<%=protocolName%>&calassoc=<%=calAssoc%>" ;

	setTimeout("self.close()",1000);
  </script>	  

<%
}
}//end of if for esign
}

} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>

</div>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

</body>

</html>


