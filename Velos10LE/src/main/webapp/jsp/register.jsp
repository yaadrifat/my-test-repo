<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<html>
<head>
	<title><%=LC.L_VelosEres_Register%><%--Velos eResearch : Register*****--%></title>

	<%@ page import="java.util.*,com.velos.eres.service.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,com.velos.eres.service.util.Configuration"%>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<link rel="stylesheet" href="styles/velos_style.css" type="text/css">
<!--<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">-->
<%
Configuration.readSettings();
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1) 
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {	
%>
<body style="overflow:scroll;">
<%
	} else {
%>
<body>
<%
	}
%>




<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
function Openflashtour()
	{
	thewindow = window.open('http://www.veloseresearch.com/eres/jsp/producttour.html', 'anew', config='height=430,width=570,toolbar=no,menubar=no,scrollbars=no,resizable=no,location=no,directories=no,status=no');
	}
</SCRIPT>


<!--  LIBRARY JAVASCRIPT END -->
<SCRIPT Language="javascript">
 function  validate(formobj){
//     formobj=document.signup

     if (!(validate_col('User F Name',formobj.userFirstName))) return false
     if (!(validate_col('User LastName',formobj.userLastName))) return false
     if (!(validate_col('User Phone',formobj.addPhoneUser))) return false
     if (!(validate_col('User Email',formobj.addEmailUser))) return false
     if(formobj.addEmailUser.value.search("@") == -1) {
    	 alert("<%=MC.M_EtrValid_EmailAddr%>");/*alert("Please enter a valid email address.");*****/
	 formobj.addEmailUser.focus();
	 return false;
     } else {
	var temp = new String(formobj.addEmailUser.value.substr(formobj.addEmailUser.value.search("@") + 1));
	 if (temp.indexOf(".") == -1) {
		 alert("<%=MC.M_EtrValid_EmailAddr%>");/*alert("Please enter a valid email address.");*****/
	    formobj.addEmailUser.focus();
	    return false;
	 }

     }
     if (!(validate_col('Time Zone',formobj.timeZone))) return false
     if (!(validate_col('SiteName',formobj.siteName))) return false
     if (!(validate_col('Site Address',formobj.sitePerAdd))) return false
     if (!(validate_col('Site City',formobj.addCitySite))) return false
     if (!(validate_col('Site State',formobj.addStateSite))) return false
     if (!(validate_col('Site Country',formobj.addCountrySite))) return false     
     if (!(validate_col('Login ID',formobj.userLogin))) return false     
     if (!(validate_col('User Password',formobj.userPwd))) return false

   //Account Register Password Expiry Validation Check for INF-18669 : Raviesh
  	var isSubmitFlag = document.getElementById("isSubmitFlag").value;
  	if(isSubmitFlag=="false"){
  		alert("<%=MC.M_PlzEtr_validPass%>");
  		formobj.userPwd.focus();
  		return false;
  	}
  	
     	 
     if ( formobj.userPwd.value.indexOf(' ') == 0 || ( formobj.userPwd.value.lastIndexOf(' ') ==  (formobj.userPwd.value.length - 1) ) )
    	{
    		  alert("<%=MC.M_PwdCntLeadTrlSpace_ReEtr%>");/*alert("Password cannot contain leading or trailing spaces. Please enter again.");*****/
    		  formobj.userPwd.focus()
    		  return false;
    	}
    	
    if( formobj.userPwd.value.length < 8)
	{
    	alert("<%=MC.M_PwdMust8CharLong_ReEtr%>");/*alert("Password must be atleast 8 characters long. Please enter again.");*****/
	 formobj.userPwd.focus()
	 return false;
	}
	
	if( formobj.userPwd.value == formobj.userLogin.value)
	{
	 alert("<%=MC.M_PwdCnt_PlsEtr%>");/*alert("Password cannot be same as User Name. Please enter again.");*****/
	 formobj.userPwd.focus()
	 return false;
	} 
	 
     if (!(validate_col('User Confirm Password',formobj.userConfirmPwd))) return false
	 	 	
    if( formobj.userPwd.value != formobj.userConfirmPwd.value)
	{
    	alert("<%=MC.M_PwdConfirmPwd_NotSame%>");/*alert("Values in 'Password' and 'Confirm Password' are not same. Please enter again.");*****/
	 formobj.userPwd.focus()
	 return false;
	} 
     if (!(validate_col('E-Signature',formobj.userESign))) return false
    if( isNaN(formobj.userESign.value) == true)
	{
    	alert("<%=MC.M_Vldt_EsignForNume%>");/*alert("E-Signature must be numeric. Please enter again.");*****/
	 formobj.userESign.focus()
	 return false;
	}     
	
	if ( formobj.userESign.value.indexOf(' ') >= 0 )
			{
		alert("<%=MC.M_Vldt_EsignForNume%>");/*alert("E-Signature must be numeric. Please enter again.");*****/
			  formobj.userESign.focus()
			  return false;
			}
		
    if( formobj.userESign.value.length < 4)
	{
    	alert("<%=MC.M_EsignCharMust4_EtrAgain%>");/*alert("E-Signature must be atleast 4 characters long. Please enter again.");*****/
	 formobj.userESign.focus()
	 return false;
	} 
	if (!(validate_col('Confirm E-Signature',formobj.userConfirmESign))) return false
    if( formobj.userESign.value != formobj.userConfirmESign.value)
	{
    	alert("<%=MC.M_ValEsign_CnfrmEsign_Same%>");/*alert("Values in 'E-Signature' and 'Confirm E-Signature' are not same. Please enter again.");*****/
	 formobj.userESign.focus()
	 return false;
	} 
    
     if( formobj.userESign.value == formobj.userPwd.value)
	{
	 alert("<%=MC.M_PwdEsign_CorrectCont%>");/*alert("Password and e-signature cannot be same. Please correct to continue.");*****/
	 formobj.userESign.focus()
	 return false;
	} 
     	 

     if (!(validate_col('User Question',formobj.userSecQues))) return false
     if (!(validate_col('User Answer',formobj.userAnswer))) return false
 }

function openwin() {
      window.open("","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")
;}

</SCRIPT>

<script>
$j(document).ready(function(){
	$j(".passwordCheck").passStrength({
		shortPass: 		"validation-fail",
		valid    :      "validation-pass",
		Invalid  :      "validation-fail",
		okPass:         "",
		messageloc:     1,
		comeFrom:       6 
		
	});
})
</script>

<DIV class="scroll">
<%	int userId = 0;
	String userAddresId = "";
	String dJobType = "" ;
	String dRoleType= "" ;
	String dPrimSpl= "" ;
	String dTimeZone="";
	CodeDao cd = new CodeDao();
	CodeDao cd2 = new CodeDao();
	CodeDao cd3 = new CodeDao();
	
	CodeDao cd4 = new CodeDao();
	
	cd2.getCodeValues("role_type"); 	
	cd.getCodeValues("job_type"); 
	cd3.getCodeValues("prim_sp");
	cd4.getTimeZones();
      dRoleType = cd2.toPullDown("roleType");	
      dJobType = cd.toPullDown("userCodelstJobtype");	
      dPrimSpl = cd3.toPullDown("primarySpeciality");		
	  dTimeZone=cd4.toPullDown("timeZone");	
%>


<Form name="signup" method="post" action="accountsave.jsp" onsubmit="return validate(document.signup)">
<table border="0" cellspacing="0" cellpadding="0" width="770">

	 <!-- HEADER OBJECT START-->
	<tr>
	  <td colspan="14"><a href="http://www.velos.com"><img src="./images/toplogo.jpg" width="770" height="60" alt="<%=LC.L_Velos_Eres%><%--Velos eResearch*****--%>" border="0"></a></td>
	</tr>
	 </table>
	 <!-- HEADER OBJECT END -->


	 
	   <table width="770" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">


	   		  <tr>
		   	   <!-- LIBRARY OBJECT START-->
	  	      <td width="1" bgcolor="9999ff"><img src="./images/sspacer.gif" width="1">				  
		      </td>
		   	   <!-- LIBRARY OBJECT END-->
			  	  <td width="125" valign="top" background="./images/leftsidebg.gif">
				  <BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>
				  </td>
				  <td>

				  
<table width="650" cellspacing="2" cellpadding="0" border="0">
  <tr>
	  <td colspan="2">
	  <p>
	  <BR><BR>&nbsp;&nbsp;&nbsp;<%=MC.M_CompleteSbmt_RegFrm%><%--Please complete and submit the following registration form. Once it is approved, your account will be<BR> &nbsp;&nbsp;&nbsp;created and you will receive an email confirming your assigned login and password.<BR><BR><i><b>&nbsp;&nbsp;&nbsp;Personal Details</b>*****--%></i>
	  <img src="./images/sspacer.gif" width="590" height="2"><BR>
	  </p>

	  </td>
  	  </tr>
  	  <tr> 
      	   <td width="200">
       	   &nbsp;&nbsp;&nbsp;<%=LC.L_First_Name%><%--First Name*****--%> <font color="#ff0000">*</font>
    	   </td>
    	   <td>
		   <input type="text" name="userFirstName" size = 35 MAXLENGTH = 30>
   		   </td>
  	  </tr>
  	  <tr> 
      	   <td width="200">
      	   &nbsp;&nbsp;&nbsp;<%=LC.L_Last_Name%><%--Last Name*****--%> <font color="#ff0000">*</font>
      	   </td>
      	   <td>
	  	   <input type="text" name="userLastName" size = 35 MAXLENGTH = 30>
      	   </td>
  	  </tr>
  <tr> 
    <td width="200">
       &nbsp;&nbsp;&nbsp;<%=LC.L_Phone%><%--Phone*****--%> <font color="#ff0000">*</font>
    </td>
    <td>
	<input type="text" name="addPhoneUser" size = 35 MAXLENGTH = 100>
    </td>
  </tr>
  <tr> 
    <td width="200">
       &nbsp;&nbsp;&nbsp;<%=LC.L_EHypenMail%><%--E_Mail*****--%> <font color="#ff0000">*</font>
    </td>
    <td>
	<input type="text" name="addEmailUser" size = 35 MAXLENGTH = 100>
    </td>
  </tr>		
	<tr>
	<td width="200">&nbsp;&nbsp;&nbsp;<%=LC.L_Time_Zone%><%--Time Zone*****--%> <font color="#ff0000">*</font> </td>
	<td> <%=dTimeZone%> </td>
	</tr>
  <tr> 
    <td width="200">
       &nbsp;&nbsp;&nbsp;<%=LC.L_Account_Type%><%--Account Type*****--%>
    </td>
    <td>
	<select name=accType>
		<option value=I><%=LC.L_Individual%><%--Individual*****--%></option>
		<option value=E><%=LC.L_Evaluation%><%--Evaluation*****--%></option>
	</select>
    </td>
  </tr>		
  <tr> 
    <td width="200">
       &nbsp;&nbsp;&nbsp;<%=LC.L_Job_Type%><%--Job Type*****--%>
    </td>
    <td>
	<%=dJobType%>  
    </td>
  </tr>	
</table>
<br>

<table  width="650" cellspacing="2" cellpadding="0" > 
  <tr> 
    <td width="650" colspan = "3" id="tdComments">
	&nbsp;&nbsp;&nbsp;<i><%=MC.M_IfInvestigator_Complete%><%--If you are an investigator, please complete the following*****--%>:</i>
    </td>
  </tr>	
	<tr height=10>	</tr>
  <tr> 
    <td width="550">
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Primary_Speciality%><%--Primary Speciality*****--%> 
    </td>
    <td width="225">
	<%=dPrimSpl%>  
  </td>
  </tr>	
  <tr> 
     <td  width="650">
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=MC.M_Years_YouInClnclRes%><%--How many years have you been involved in clinical research?*****--%>
    </td>
    <td >
	<input type="text" name="userWrkExp" size = 20 MAXLENGTH = 100>
    </td>
  </tr>	
  <tr> 
    <td width="400">
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=MC.M_PhaseTrials_YouInvolved%><%--Which phases of trials have you been involved with?*****--%>
    </td>
    <td >
	<input type="text" name="userPhaseInv" size = 20 MAXLENGTH = 100>
    </td>
  </tr>	
</table>


<table width="650" cellspacing="2" cellpadding="0" >
<tr>
<td colspan="2">
<p>
	  <BR><i><b>&nbsp;&nbsp;&nbsp;<%=LC.L_Org_Dets%><%--Organization Details*****--%></b></i><BR>
	  <img src="./images/sspacer.gif" width="600" height="2"><BR>
</p>
</td>
</tr>
  <tr> 
    <td width="200">
       &nbsp;&nbsp;&nbsp;<%=LC.L_Organization_Name%><%--Organization Name*****--%> <font color="#ff0000">*</font>
    </td>
    <td>
	<input type="text" name="siteName" size = 35 MAXLENGTH = 50>
    </td>
  </tr>

  <tr> 
    <td width="200">
       &nbsp;&nbsp;&nbsp;<%=LC.L_Address%><%--Address*****--%> <font color="#ff0000">*</font>
    </td>
    <td>
	<input type="text" name="sitePerAdd" size = 35 MAXLENGTH = 50>
    </td>
  </tr>	
   <tr> 
    <td width="200">
       &nbsp;&nbsp;&nbsp;<%=LC.L_City%><%--City*****--%> <font color="#ff0000">*</font>
    </td>
    <td>
	<input type="text" name="addCitySite" size = 35  MAXLENGTH = 30>
    </td>
  </tr>
  <tr> 
    <td width="200">
       &nbsp;&nbsp;&nbsp;<%=LC.L_State%><%--State*****--%> <font color="#ff0000">*</font>
    </td>
    <td>
	<input type="text" name="addStateSite" size = 35 MAXLENGTH = 30>
    </td>
  </tr>
  <tr> 
    <td width="200">
       &nbsp;&nbsp;&nbsp;<%=LC.L_Country%><%--Country*****--%> <font color="#ff0000">*</font>
    </td>
    <td>
	<input type="text" name="addCountrySite" size = 35 MAXLENGTH = 30>
    </td>
  </tr>
  <tr> 
    <td width="200">
       &nbsp;&nbsp;&nbsp;<%=LC.L_ZipOrPostal_Code%><%--Zip/Postal Code*****--%>
    </td>
    <td>
	<input type="text" name="addZipSite" size = 35 MAXLENGTH = 15>
    </td>
  </tr>
  <tr> 
    <td width="200">
       &nbsp;&nbsp;&nbsp;<%=LC.L_Phone%><%--Phone*****--%>
    </td>
    <td>
	<input type="text" name="addPhoneSite" size = 35 MAXLENGTH = 100>
    </td>
  </tr>
</table>


<table width="650" cellspacing="2" cellpadding="0">
  <tr>
  	  <td colspan="3">
	  <BR><i><b>&nbsp;&nbsp;&nbsp;<%=LC.L_Login_Dets%><%--Login Details*****--%></b></i><BR>
	  <img src="./images/sspacer.gif" width="600" height="2"><BR>
	  </td>
  </tr>
  <tr> 
    <td >
       &nbsp;&nbsp;&nbsp;<%=LC.L_User_Name%><%--User Name*****--%> <font color="#ff0000">*</font>
    </td>
    <td>
	<input type="text" name="userLogin" size = 35 MAXLENGTH = 15> 
    </td>
  </tr>
  <tr> 
    <td width="30%">
       &nbsp;&nbsp;&nbsp;<%=LC.L_Password%><%--Password*****--%> <font color="#ff0000">*</font>
    </td>
    <td width="100%">
	<input type="password" name="userPwd" id="userPwd" class="passwordCheck" size = 35 MAXLENGTH = 15> 
    </td>
  </tr>
	  <tr>
		<td colspan=3>
			<A HREF="pwdtips.htm" target="_new"><%=MC.M_SelcPwdCrt%><%--Criteria for selecting a Password*****--%></A>
		</td>
	</tr>
  <tr> 
    <td >
       &nbsp;&nbsp;&nbsp;<%=LC.L_Confirm_Pwd%><%--Confirm Password*****--%> <font color="#ff0000">*</font>
    </td>
    <td>
	<input type="password" name="userConfirmPwd" size = 35 MAXLENGTH = 15>
    </td>
    <td >
    </td>
  </tr>
  <tr> 
    <td >
       &nbsp;&nbsp;&nbsp;<%=LC.L_Esign_Numeric%><%--e-Signature (numeric)*****--%><font color="#ff0000">*</font>
    </td>
    <td>
	<input type="password" name="userESign" size = 35 MAXLENGTH = 8> 
    </td>
    </tr>
    <tr>
    <td ><font color="#ff0000">(<%=LC.L_Min4_Digits%><%--minimum 4 digits*****--%>)</font>  </td>
  </tr>
  <tr> 
    <td >
       &nbsp;&nbsp;&nbsp;<%=LC.L_Confirm_Esign%><%--Confirm e-Signature*****--%> <font color="#ff0000">*</font>
    </td>
    <td>
	<input type="password" name="userConfirmESign" size = 35 MAXLENGTH = 8>
    </td>
    <td >
    </td>
  </tr>
  <tr> 
    <td>
       &nbsp;&nbsp;&nbsp;<%=LC.L_Secret_Que%><%--Secret Question*****--%> <font color="#ff0000">*</font>
    </td>
    <td>
	<input type="text" name="userSecQues" size = 35 MAXLENGTH = 150>
    </td>
    <td >
    </td>
  </tr>
  <tr> 
    <td >
       &nbsp;&nbsp;&nbsp;<%=LC.L_Your_Answer%><%--Your Answer*****--%>  <font color="#ff0000">*</font>
    </td>
    <td>
	<input type="text" name="userAnswer" size = 35 MAXLENGTH = 150>
    </td>
	<td >
    </td>
  </tr>
</table>

<table width="650" cellspacing="2" cellpadding="0" >

  <tr></tr>
  <tr> 
    <td colspan="2">
	&nbsp;&nbsp;&nbsp;<input type="checkbox" name="accMailFlag" value = "Y" CHECKED>
	&nbsp;
       <%=MC.M_ChkVelosCont_UsefulInfo%><%--Check if you want Velos to contact you regarding useful information*****--%>
    </td>
  </tr>
  <tr> 
    <td colspan = "2">
	&nbsp;&nbsp;&nbsp;<input type="checkbox" name="accPubFlag" value = "Y" CHECKED>
	&nbsp;
	<%=MC.M_CkhAgreeVelos_UsefulInfo%><%--Check if you agree to Velos giving your name to other companies that might<BR> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;have useful information for you*****--%> 
    </td>
  </tr>
</table>
 <table>
    <BR><BR><b>&nbsp;&nbsp;&nbsp;<%=LC.L_Reg_AgreementAnd%><%--Registration Agreement and*****--%> <A href="http://www.velos.com/privacypolicy.shtml" target="Information" onClick="openwin()"><%=LC.L_Privacy_Policy%><%--Privacy Policy*****--%></A>
    <BR><BR>
    <A href="http://www.velos.com/termsofservice.shtml" target="Information" onClick="openwin()">&nbsp;&nbsp;&nbsp;<%=LC.L_Terms_OfService%><%--Terms of Service*****--%></a></b>
    </td>
  </tr>
  <tr><td colspan="2">
	<P class = "italicsComments">&nbsp;&nbsp;&nbsp;<%=MC.M_ClkAcptBtn_Agree%><%--By clicking on the I Accept button, you agree to the*****--%> <A href="http://www.velos.com/termsofservice.shtml" target="Information" onClick="openwin()"><%=MC.M_TermsCndnAgment_Acc%><%--terms and conditions in the Agreement</a> and your registration will<BR> &nbsp;&nbsp;&nbsp;be submitted for account creation.*****--%> 	</P>
	</td>
  </tr>
  <tr>
	<td>
<!--	&nbsp;&nbsp;<input type="image" src="./images/iaccept.gif" border="0" onClick="return validate()" align="absmiddle" border="0">	-->
   &nbsp;&nbsp;<input type="image" src="./images/iaccept.gif" border="0"  align="absmiddle" border="0">
	</td>
	<td>
	&nbsp;&nbsp;<a href="http://www.velos.com"><img src="./images/idecline.gif" width="81" height="25" border="0" alt="<%=LC.L_I_Decline%><%--I decline*****--%>"></a>	

	</td>
	</tr>
 </table>

<input type="hidden" id="isSubmitFlag" value="" />

</form>
				  </td>
		   	   <!-- LIBRARY OBJECT START-->
	  	      <td width="1" bgcolor="9999ff"><img src="./images/sspacer.gif" width="1"> 				  
		      </td>
		   	   <!-- LIBRARY OBJECT END-->
			  </tr>

      </table>

</table>
	</DIV>
</body>
	
</html>
