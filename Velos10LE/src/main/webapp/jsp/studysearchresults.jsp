<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT language="JavaScript1.1">

function protocolDetails(id)
{
	studysearchresults.action= "study.jsp?mode=M&studyId=" +id +"&srcmenu=tdMenuBarItem3&selectedTab=1";
	studysearchresults.submit();
}

function createVersion(id)
{   srchCriteria = document.studysearchresults.searchCriteria.value;
	studysearchresults.action= "createversion.jsp?studyId=" +id +"&srcmenu=tdMenuBarItem3&calledfrom=S&searchCriteria="+srchCriteria;
	studysearchresults.submit();
}

</SCRIPT> 

</HEAD>
<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.service.util.*"%>

<%
HttpSession tSession = request.getSession(true); 
String userId = null;
if (sessionmaint.isValidSession(tSession)) {
	 userId = (String) tSession.getValue("userId");
}

String resultSet=request.getParameter("resultSet");
String searchCriteria = request.getParameter("searchCriteria");
if (searchCriteria==null) {searchCriteria="";}

Vector vec = new Vector();
			
Column studyId = new Column(LC.L_Std_Id/*Study Id*****/);
studyId.passAsParameter = true;
studyId.hidden = true;
			
vec.add(studyId);	
			
Column studyNumber = new Column(LC.L_Study_Number/*Study Number*****/);
studyNumber.width = "20%"	;
studyNumber.align = "left"	;		
vec.add(studyNumber);

Column studyVersion = new Column(LC.L_Version/*Version*****/);
studyVersion.width = "10%"	;
studyVersion.align = "left"	;		
vec.add(studyVersion);
			
Column studyTitle = new Column(LC.L_Study_Title/*Study Title*****/);
studyTitle.width = "40%" ;
studyTitle.align = "left"	;
vec.add(studyTitle);
			
Column manageProtocol  = new Column("");
manageProtocol.value = "Protocol Details";
manageProtocol.funcName = "protocolDetails";
manageProtocol.width = "15%";
manageProtocol.align = "left";
vec.add(manageProtocol);
			
Column managePatient  = new Column("");
managePatient.value = "Create a New Version";
managePatient.funcName = "createVersion";
managePatient.width = "15%";
managePatient.align = "left";
vec.add(managePatient);

String sSQL = "select PK_STUDY, STUDY_NUMBER,  STUDY_VER_NO, case when length(study_title) > 80 then substr(study_title,1,80) || '...'  else study_title  end from er_study where pk_study in (select FK_STUDY from er_studyteam where fk_user = " + userId +") and (lower(study_number) like lower('%"+searchCriteria+"%') or lower(study_title) like lower('%"+searchCriteria+"%') or lower(study_keywrds) like lower('%"+searchCriteria+"%'))" ;	

String title =MC.M_StdsMatch_SrchCriteria/*The following studies match your search criteria*****/+": ";

String srchType=request.getParameter("srchType");
String sortType=request.getParameter("sortType");
String sortOrder=request.getParameter("sortOrder");

request.setAttribute("sSQL",sSQL);
request.setAttribute("srchType",srchType);
request.setAttribute("vec",vec);
request.setAttribute("sTitle",title);
			
//This parameter is recieved on clicking back or next button
String s_scroll=request.getParameter("scroll"+resultSet);
request.setAttribute("scroll"+resultSet,s_scroll);
request.setAttribute("sortType",sortType);
request.setAttribute("sortOrder",sortOrder);

%>

<Form name="studysearchresults" method="post" action="studysearchbrowser.jsp" onsubmit="">
<input type=hidden name=mode value="M">
<input type=hidden name=searchCriteria value="<%=searchCriteria%>">

<% if (!(searchCriteria.equals(""))) {%>
  <P class = "defComments"> <%=MC.M_StdsMatch_SrchCriteria%><%--The following studies match your search criteria*****--%>: "<%=searchCriteria%>" </P>
<%}%>
  <P class = "defComments"> <%=MC.M_StdPcols_ViewAces%><%--Study Protocols with View access*****--%> </P>
		
</Form>

<jsp:include page="getresults.jsp?" flush="true">
<jsp:param name="resultSet" value="<%=resultSet%>"/>
</jsp:include>
 
</body>

</html>




