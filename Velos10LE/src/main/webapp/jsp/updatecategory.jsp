<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<%@page import="com.velos.eres.service.util.*"%>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
  <jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

  <%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
  <%

	HttpSession tSession = request.getSession(true); 

	int ret = 0;

	int eventId=0;
	String name ="";
	String description ="";
	String eventType="";
	String accId="";
	String src = null;
	String mode="";
	String catMode="";
	String msg="";
	String eventLibType = "";
	String eventLineCat ="";
	
	String duration= request.getParameter("duration");
	String protocolId = request.getParameter("protocolId");
	String calledFrom = request.getParameter("calledFrom");
	String fromPage = request.getParameter("fromPage");
	if (fromPage.equals("null")) {
		fromPage="xyz"; //this is set to 'lib' when the page is called from select events of a new protocol calendar 
	}	
	
	String eSign = request.getParameter("eSign");

    if (sessionmaint.isValidSession(tSession)) {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%	
	String oldESign = (String) tSession.getValue("eSign");


	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");

	accId = (String) tSession.getValue("accountId");

	src = request.getParameter("srcmenu");

	eventType = "L";

	mode = request.getParameter("mode");
	catMode = request.getParameter("catMode");

	name = StringUtil.stripScript(request.getParameter("categoryName"));

	description = request.getParameter("desc");
	eventLibType = request.getParameter("cmbLibType");
	eventLineCat = request.getParameter("cmbBgtCat");



	eventdefB.setName(name); 
	eventdefB.setDescription(description);
	eventdefB.setEvent_type(eventType);
	eventdefB.setUser_id(accId); 	
	eventdefB.setEventLibType(eventLibType);
	eventdefB.setEventLineCategory(eventLineCat);
	
	if (catMode.equals("M")){	

	   eventdefB.setEvent_id(EJBUtil.stringToNum(request.getParameter("categoryId")));

	   eventdefB.setModifiedBy(usr);
	   eventdefB.setIpAdd(ipAdd);

	   ret = eventdefB.updateEventdef();

	   if (ret == 0) {

		   msg = MC.M_Cat_SavedSucc;/*msg = "Category saved successfully";*****/

	   }else {

		   msg = LC.L_Cat_NotSaved;/*msg = "Category not saved";*****/

	   }  

	}else{

		eventdefB.setCreator(usr); 
		eventdefB.setIpAdd(ipAdd);

		eventdefB.setEventdefDetails();
		ret = eventdefB.getEvent_id();
		//out.print("return" +ret);
		if (ret > 0) {
			msg = MC.M_Cat_SavedSucc;/*msg = "Category saved successfully";*****/
	   	}else {
			msg = LC.L_Cat_NotSaved;/*msg = "Category not saved";*****/
		} 
	
  }  
  %> 
  
<%  	if (ret == -1){%>
			<br><br><br><br><br><p class = "successfulmsg" align = center>
			<%=MC.M_CatNameDup_ClkBack %><%-- Category Name is duplicate. Click on "Back" Button to change the name*****--%> <Br><Br>
			<button name="back" onclick="window.history.back();"><%=LC.L_Back%></button></p>
		<% return;} 
		%>
 		
<br>
<br>
<br>
<br>
<br>

<p class = "successfulmsg" align = center> <%=msg%> </p>
<%if (calledFrom.equals("L")){
	if (fromPage.equals("lib")) {%>
		<META HTTP-EQUIV=Refresh CONTENT="0; URL=eventbrowser.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=M">	
	<%}%>
	<META HTTP-EQUIV=Refresh CONTENT="0; URL=eventlibrary.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=M">
<%}else{%>
	<META HTTP-EQUIV=Refresh CONTENT="0; URL=eventbrowser.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=M">
<% }
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





