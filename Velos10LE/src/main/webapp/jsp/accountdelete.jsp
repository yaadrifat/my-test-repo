<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.velos.eres.service.util.LC"%>
<%@page import="com.velos.eres.service.util.MC,com.aithent.audittrail.reports.AuditUtils"%>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<%--<title>Event Library</title>*****--%>
<title><%=LC.L_Evt_Lib%></title>

<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	/*alert("Incorrect e-Signature. Please enter again");*****/
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>


</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 


<jsp:include page="panel.jsp" flush="true"/> 

<BODY> 
<br>

<DIV class="formDefault" id="div1">
<% 
	
HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))	{
		String userId = request.getParameter("userId");
		String delMode=request.getParameter("delMode");
		String lname = request.getParameter("lname");
		String fname = request.getParameter("fname");
		String uStat = request.getParameter("uStat");
	if (delMode==null) {
			delMode="final";
%>
	<FORM name="deleteAccount" method="post" action="accountdelete.jsp" onSubmit="return validate(document.deleteAccount)">
	<input type="hidden" name="fname" value=<%=fname%>>
	<input type="hidden" name="lname" value=<%=lname%>>
	<input type="hidden" name="uStat" value=<%=uStat%>>
	<input type="hidden" name="userId" value=<%=userId%>>	
		
	<br><br>
	<%--<P class="defComments">Please enter e-Signature to proceed with Account Delete.</P>*****--%>
	<P class="defComments"><%=MC.M_EsignToProc_WithAccDel%></P>
	<TABLE width="100%" cellspacing="0" cellpadding="0" >
	<tr>
	   <td width="40%">
		<%--e-Signature*****--%>
		<%=LC.L_Esignature%> <FONT class="Mandatory">* </FONT> 
	   </td>
	   <td width="60%">
		<input type="password" name="eSign" maxlength="8" SIZE=15 autocomplete="off">
	   </td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>
		 <button type="submit"><%=LC.L_Submit%></button>
		</td>
	</tr>
	</table>

 	 <input type="hidden" name="delMode" value="<%=delMode%>">
	 
	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/> 	
<%
			} else {
				userB.setUserId(EJBUtil.stringToNum(userId));
				userB.getUserDetails();
				userB.setUserStatus("X");
				// Modified for INF-18183 ::: AGodara 				
				userB.updateUser(AuditUtils.createArgs(tSession,"",LC.L_Manage_Acc));
%>				
			<br><br><br><br><br> <p class = "successfulmsg" align = center> 
			<%--Data deleted successfully.*****--%>
			<%=MC.M_Data_DelSucc%> </p>

				<META HTTP-EQUIV=Refresh CONTENT="1; URL=velosmsgs.jsp?lname=<%=lname%>&fname=<%=fname%>&userStatus=<%=uStat%> ">
<%				
			} //end esign
	} //end of delMode	
}//end of if body for session 
else { %>
 <jsp:include page="timeout.html" flush="true"/> 
 <% } %>
 <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/> 
  </div>
  
</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page="velosmenus.htm" flush="true"/>
</div>

</body>
</HTML>


