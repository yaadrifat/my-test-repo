<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<jsp:include page="velos_includes.jsp"></jsp:include>
<html>
<head><meta	http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_User_Dets%><%--User Details*****--%></title>
<%@ page import="com.velos.eres.service.util.*"%>
<%@	page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="addressUserB" scope="page" class="com.velos.eres.web.address.AddressJB" />
<jsp:useBean id="siteB" scope="page" class="com.velos.eres.web.site.SiteJB" />
<jsp:useBean id="addressSiteB" scope="page" class="com.velos.eres.web.address.AddressJB" />
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="ctrlB" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/>
<jsp:useBean id="accB" scope="request" class="com.velos.eres.web.account.AccountJB"/>

<%@	page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil"%>
</head>
<SCRIPT	Language="javascript">

var screenWidth = screen.width;
var screenHeight = screen.height;

	function alertDeactivate(formobj,stat)
	{
		if (stat=='D')
		{
			if (confirm("<%=MC.M_ConfirmUsr_AllStdTeams%>") )/*if (confirm("Would you like to make this User inactive in all <%=LC.Std_Study%> Teams that they are part of?") )*****/
			{
				formobj.deactivateFromTeam.value = 1;
			}
			else
			{
				formobj.deactivateFromTeam.value = 0;
			}
		}
		else
		{
				formobj.deactivateFromTeam.value = 0;
		}
	}

	function  validate(formobj,userSiteId){
				formobj=document.userdetails
				checkQuote = "N";
				if (!(validate_col('User F Name',formobj.userFirstName))) return false
				if (!(validate_col('User LastName',formobj.userLastName))) return false
				if (!(validate_col('User Email',formobj.userEmail))) return false
				if (formobj.pname.value=='null'){
				 if (!(validate_col('Organization Name',formobj.accsites))) return false
				 if (!(validate_col('Group',formobj.usrgrps))) return false
				}
				if (!(validate_col('e-Sign',formobj.eSign)))return false;
				if (formobj.pname.value=='null'){
			 if(formobj.mode.value == 'M' && formobj.accsites.value!=userSiteId && formobj.changeOrgFlg.value=='false'){
				formobj.changeOrgFlg.value="true";
				msg="<%=MC.M_ConfirmMulti_RgtReset%>";/*msg="You are changing the User's Primary Organization. This users multiple facility rights will be reset to the new setting. Do you want to proceed?";*****/
				if (!(confirm(msg))) {
				 //if the user presses cancel then set the organization to the original value
					formobj.accsites.value = userSiteId;
				}
			  }
		}

				//New User Password Validation Check for INF-18669 : Raviesh
				var isSubmitFlag=$j("#isSubmitFlag").val();
				if(isSubmitFlag=="false"){
					alert("<%=MC.M_PlzEtr_validPass%>");
					formobj.userPassword.focus();
					return false;
				}

				
		//JM: 04/11/2005 Modified: if condition for validation
		if	((formobj.mode.value =='M' && formobj.selfFlag.value == true) || !(formobj.mode.value =='M') || (formobj.mode.value	=='M' && formobj.userType.value =='NS' && !(formobj.JFlag.value =='1'))) {
		if (formobj.pname.value=='null'){
			if (!(validate_col('Login',formobj.userLogin))) return false
			if (!(validate_col('Password',formobj.userPassword))) return false
			if (!(validate_col('Password',formobj.userConfirmPassword))) return false
			if (!(validate_col('e-Signature',formobj.userESign))) return false
			if (!(validate_col('Confirm e-Signature',formobj.userConfirmESign))) return false
				if ( formobj.userPassword.value.indexOf(' ') == 0 || ( formobj.userPassword.value.lastIndexOf(' ') ==  (formobj.userPassword.value.length - 1) ) )	{
				  alert("<%=MC.M_PwdCntLeadTrlSpace_ReEtr%>");/*alert("Password cannot contain leading or trailing spaces. Please enter again.");*****/
				  formobj.userPassword.focus()
				  return false;
				}
				if( formobj.userPassword.value.length < 8){
					alert("<%=MC.M_PwdMust8CharLong_ReEtr%>");/*alert("Password must be atleast 8 characters long. Please enter again.");*****/
					formobj.userPassword.focus();
					return false;
				}
				if( formobj.userPassword.value == formobj.userLogin.value){
					alert("<%=MC.M_PwdCntSameLogin_ReEtr%>");/*alert("Password cannot be same as Login Name. Please enter again.");*****/
					formobj.userPassword.focus()
					return false;
				}
				if( formobj.userPassword.value != formobj.userConfirmPassword.value){
					alert("<%=MC.M_PwdConfirmPwd_NotSame%>");/*alert("Values in 'Password' and 'Confirm Password' are not same. Please enter again.");*****/
					formobj.userPassword.focus();
					return false;
				}
				if( isNaN(formobj.userESign.value) == true){
					alert("<%=MC.M_Vldt_EsignForNume%>");/*alert("e-Signature must be numeric. Please enter again.");*****/
					formobj.userESign.focus()
					return false;
				}
				if ( formobj.userESign.value.indexOf(' ') >= 0 ){
					alert("<%=MC.M_Vldt_EsignForNume%>");/*alert("e-Signature must be numeric. Please enter again.");*****/
					formobj.userESign.focus()
					return false;
				}
				if( formobj.userESign.value.length < 4){
					alert("<%=MC.M_EsignCharMust4_EtrAgain%>");/*alert("e-Signature must be atleast 4 characters long. Please enter again.");*****/
					formobj.userESign.focus()
					return false;
				}
				if( formobj.userESign.value != formobj.userConfirmESign.value){
					alert("<%=MC.M_ValEsign_CnfrmEsign_Same%>");/*alert("Values in 'e-Signature' and 'Confirm e-Signature' are not same. Please enter again.");*****/
					formobj.userESign.focus()
					return false;
				}
				//JM: 04/11/2005 Modified: if condition
				// if	(formobj.mode.value =='M')
				 if	(formobj.mode.value =='M' && !(formobj.userType.value =='NS')){
					 if( formobj.userPassword.value != formobj.userOldPwd.value){
						alert("<%=MC.M_VldtPwd_UpdtDet%>");/*alert("Please enter the correct password to update the details. You can not change the password from this page.");*****/
						return false;
					}
				}
				if( formobj.userESign.value==formobj.userPassword.value){
					alert("<%=MC.M_EsignCnt_AsPwd%>");/*alert("e-signature cannot be same as password. Please enter again.");*****/
					formobj.userESign.focus();
					return false;
				}
			}
		}
		if (!(validate_col('TimeZone',formobj.timeZone))) return false
		if(formobj.userEmail.value.search("@") == -1) {
			alert("<%=MC.M_EtrValid_EmailAddr%>");/*alert("Please enter a valid email address.");*****/
			formobj.userEmail.focus();
			return false;
		 }
		if(isNaN(formobj.eSign.value) == true) {
			alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
			formobj.eSign.focus();
			return false;
		}
				
		return checkDupeUserAndEmail();
		
		
	}
	
	
	function checkDupeUserAndEmail(){
		
		var userFname = $j('input[name="userFirstName"]').val();
   		var mode = $j('input[name="mode"]').val();
		var userLname = $j('input[name="userLastName"]').val();		
		var email = $j('input[name="userEmail"]').val();
		var userLogin=$j('input[name="userLogin"]').val();
		var mode = $j('input[name="mode"]').val();
		var userType = $j('input[name="userType"]').val();
		var JFlag = $j('input[name="JFlag"]').val();
		
		var countUserMails;
		var countUserNames;
		var userLogins;
		
		var urlParam="userFname="+userFname+"&mode="+mode+"&userLname="+userLname+"&email="+email+"&userLogin="+userLogin;
		
		$j.ajax({
			url:'validateUserData.jsp',
			data:urlParam,		
			success:(function (data){
				
				countUserMails=data.countUserMails;
				countUserNames=data.countUserNames;
				userLogins=data.userLogins;
				}),type: "POST",
			async:false});
		
		
		//if ((mode.equals("M") && userType.equals("NS") && JFlag.equals(""))
				//if ((mode.equals("N")) || ((mode.equals("M") && userType.equals("NS") && JFlag.equals(""))))
					//if (mode.equals("M") && userType.equals("NS") ) //KM
					//count--;
		
		if(mode=='M'){			
			userLogins = userLogins-1;			
			if(userLogins>0){
				alert(M_LoginIdExst_EtrNew);
				$j('input[name="userLogin"]').focus();
				return false;
			}
		}else if(mode=='N'){
			if(userLogins>0){
				alert(M_LoginIdExst_EtrNew);
				$j('input[name="userLogin"]').focus();
				return false;
			}
		}
		
		if(mode=='M'){
			countUserNames=countUserNames-1;
			if(countUserNames>0){
				var proceed = window.confirm(M_UsrName_AldyExst);
				if(!proceed){
					return proceed;
				}
			}
		}else if(mode=='N'){
			if(countUserNames>0){
				var proceed = window.confirm(M_UsrName_AldyExst);
				if(!proceed){
					return proceed;
				}
			}
		}
		
		if(mode=='M'){
			countUserMails =countUserMails-1;
			if(countUserMails>0){
				var proceed = window.confirm(M_EmailIdExst_Cont);
				if(!proceed){
					return proceed;
				}
			}
		}else if(mode=='N'){
			if(countUserMails>0){
				var proceed = window.confirm(M_EmailIdExst_Cont);
				if(!proceed){
					return proceed;
				}
			}
		}
	
		
		
	}

	function openWin(formobj) {
		userId = formobj.userId.value;
		windowName = window.open('managesites.jsp?userId='+userId,'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400')
		windowName.focus();
	}
	
	function openWinMoreDetails(modId, mode, modName,pageRight)
	{
		if (mode == 'M'){
			windowname=window.open("moredetails.jsp?modId=" + modId +"&modName="+modName,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=840,height=340 top=120,left=200 0, ");
			windowname.focus();
		} else if (mode == 'N')	{
			alert("<%=MC.M_SavePage_BeforeDets%>");/*alert("Please save the page first before entering More Details");*****/
			return false;
		}
	}

$j(document).ready(function(){
	$j(".passwordCheck").passStrength({
		shortPass: 		"validation-fail",
		valid    :      "validation-pass",
		Invalid  :      "validation-fail",
		okPass:         "",
		messageloc:     1,
		comeFrom:       4 
		
	});
});

$j(document).ready(function(){
	
	var screenWidth = screen.width;
	var screenHeight = screen.height;
	
	if(screenWidth>1280 || screenHeight>1024){
		$j('#divbot').css("height","80%");
	}
});

</script>

<%	String src;
	String pname = null;
	pname=request.getParameter("pname");
	src= request.getParameter("srcmenu");
	String userType = request.getParameter("userType");
	userType=(userType==null)?"":userType;
%>
<jsp:include page="panel.jsp" flush="true">
	<jsp:param name="src" value="<%=StringUtil.htmlEncodeXss(src)%>"/>
</jsp:include>
<body>
<DIV class="tabDefTopN" id = "divtab">
<%
	HttpSession tSession = request.getSession(true);
	String uName = (String) tSession.getValue("userName");
	String sessUserId =	(String) tSession.getValue("userId");
//JM:
	String JFlag=request.getParameter("JFlag");
	if(	JFlag==null) JFlag="";
	String acc = (String) tSession.getValue("accountId");
 if (sessionmaint.isValidSession(tSession)){
	 int accId = EJBUtil.stringToNum(acc);
	 int pageRight = 0;
	 int pageRightMngOrg = 0;
	 char mngOrgAccRight = '0';
	 String modRight = (String) tSession.getValue("modRight");
	 ctrlB.getControlValues("module"); //get extra modules
	 ArrayList acmodfeature =  ctrlB.getCValue();
	 ArrayList acmodftrSeq = ctrlB.getCSeq();
	 int orgSeq = 0;
	 orgSeq = acmodfeature.indexOf("MODORG");
	 orgSeq = ((Integer) acmodftrSeq.get(orgSeq)).intValue();
	 mngOrgAccRight = modRight.charAt(orgSeq - 1);
	 String mode = request.getParameter("mode");
	 int userId = 0;
	 int totalUsersAllowed = EJBUtil.stringToNum((String) tSession.getValue("totalUsersAllowed"));
	 int existingUsers = 0;
	 GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
	 pageRightMngOrg = Integer.parseInt(grpRights.getFtrRightsByValue("MSITES"));

	 if(mode.equals("N") || (mode.equals("M") && (pname==null ))  ) {
		//existingUsers = userB.getUsersInAccount(EJBUtil.stringToNum(acc));
		existingUsers = userB.getActiveUsersInAccount(EJBUtil.stringToNum(acc));
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MUSERS"));
	 } else {
		//pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("EPROFILE"));
		pageRight = 7;
	 }
	if((pname !=null )&& pname.equals("ulinkBrowser")){
%>
		<jsp:include page="personalizetabs.jsp" flush="true"/>
</div>
<DIV class="tabDefBotN" id="divbot">
<%
	}else{%>
  	<jsp:include page="accounttabs.jsp" flush="true">
	<jsp:param name="selectedTab" value="3"/>
	</jsp:include>
</div>
<!-- Bug#15388 Fix for 1360*768 resolution- Tarun Kumar -->

	<DIV class="tabDefBotN" id="divbot">


<%	}if ((mode.equals("M") && ((pageRight >=6) || (pageRight >=4))) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 )) ){
//JM: Modified: If condition
	if ( (mode.equals("N") ||(mode.equals("M") && userType.equals("NS") && JFlag.equals("")) )&& totalUsersAllowed > 0 && totalUsersAllowed <= existingUsers) {
%>
<table width=100%>
	<tr>
		<td	align=center>
			<p class = "sectionHeadings">
				<%=MC.M_NumAccExhust_VelosAdmin%><%--The number of users you can create in this account has exhausted. Please contact the Velos Administrator.*****--%>
			</p>
		</td>
	</tr>
	
	<tr>
		<td	align=center>
			<button onclick="window.history.back();"><%=LC.L_Back%></button>
		</td>
	</tr>
</table>
<%
	} else { 	//else of if that checks the number of existing users with allowed users
		String dJobType = "" ;
		CodeDao cd = new CodeDao();
		String userAddresId = "";
		CodeDao cd1 = new CodeDao();
		CodeDao cd2 = new CodeDao();
		CodeDao cd3 = new CodeDao();
		CodeDao cd4 = new CodeDao();
		String  userLastName="";
		String  userFirstName="";
		String  userMidName="";
		String  userAddPri="";
		String  userAddCity="";
		String  userAddState="";
		String  userAddZip="";
		String  userAddCountry="";
		String  userAddPhone="";
		String  userEmail = "";
		String userGroup = "";
		String userLogin = "";
		String userOldPwd = "";
		String userOldESign = "";
		String userSession = "";
		String userSiteId = "";
		String  userJobType="";
		String  userSpl="";
		String  userWrkExp="";
		String  userPhaseInv="";
		String  siteName="";
		String  siteAddPri="";
		String  userSecretQues = "";
		String  userSecretAns = "";
		String dUsrGrps="";
		String dAccSites="";
		String dPrimSpl = "";
		String dTimeZone ="";
		String timeZone="";
		String uStat = "";
		String pwdXpDate="";
		String esignXpDate="";
		String accLoginModuleDDStr="";
		String ldapEnabled="";
		String userLoginModuleMap="",userLoginModuleStr="";
		int userLoginModule=0;
		boolean selfFlag = false;
		boolean cntExceeded = false;
		String userHidden = "0";

		//cd2.getAccountSites(accId);
		//KM- 22Aug08
		cd2.getAccountSitesNoHidden(accId);

		cd.getCodeValues("job_type");
		cd3.getCodeValues("prim_sp");
		cd4.getTimeZones();
		//Added for July-August'06 Enhancement(U2)
		String usrTz="";
		String usrTzOverride="";
		int modname=1;
		SettingsDao settingsDao=commonB.getSettingsInstance();
		ArrayList keys=new ArrayList();
		keys.add("ACC_USER_TZ_OVERRIDE");
		keys.add("ACC_USER_TZ");
		settingsDao.retrieveSettings(accId,modname,keys);
		ArrayList keywords=settingsDao.getSettingKeyword();
		ArrayList setvalues=settingsDao.getSettingValue();
	if ((keywords!=null) && (keywords.size() >0)){
		for (int i=0;i<keywords.size();i++){
			if (((String)keywords.get(i)).equals("ACC_USER_TZ")){
				usrTz=(String)setvalues.get(i);
				usrTz=(usrTz==null)?"":usrTz;
			} else {
				usrTzOverride=(String)setvalues.get(i);
				usrTzOverride=(usrTzOverride==null)?"":usrTzOverride;
			}
		}
	}
	AccountDao accDao=accB.getLoginModuleDetails(accId);
	ArrayList accLoginModuleDNList=accDao.getAccLoginModuleDNList();
	ArrayList accLoginModuleIdlist=accDao.getAccLoginModuleIdList();
	accLoginModuleDDStr=EJBUtil.createPullDown("dnname",userLoginModule,accLoginModuleIdlist,accLoginModuleDNList);
	accB.setAccId(accId);
	accB.getAccountDetails();
	ldapEnabled=accB.getLdapEnabled();
	ldapEnabled=(ldapEnabled==null)?"":ldapEnabled;
	ldapEnabled=(ldapEnabled.length()==0)?"N":ldapEnabled;
	//JM: 06/09/05 modified
	//if (mode.equals("M"))
	//if( (mode.equals("M")) ||((mode.equals("N")) && userType.equals("NS")) )
	if( (mode.equals("M")) ||((mode.equals("M")) && userType.equals("NS")) ) {

		if ("ulinkBrowser".equals(request.getParameter("pname"))) {
			userId = StringUtil.stringToNum(sessUserId);
		} else {
			userId = EJBUtil.stringToNum(request.getParameter("userId"));
		}
		userB.setUserId(userId);
		userB.getUserDetails();
		
		if (StringUtil.stringToNum(userB.getUserAccountId()) != accId) {
		%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
			<div class = "myHomebottomPanel">
			<jsp:include page="bottompanel.jsp" flush="true"/>
			</div>
			
			<div class ="mainMenu" id = "emenu">
			<jsp:include page="getmenu.jsp" flush="true"/>
			</div>
			</body>
			</html>
		<%
			return;
		} // End of same account check
		
		if (userB.getUserPerAddressId() != null) {
			addressUserB.setAddId(EJBUtil.stringToNum(userB.getUserPerAddressId()));
			addressUserB.getAddressDetails();
		}
		if (userB.getUserSiteId() != null) {
			siteB.setSiteId(EJBUtil.stringToNum(userB.getUserSiteId()));
			siteB.getSiteDetails();
		}
		userLoginModuleStr=userB.getUserLoginModule();
		userLoginModuleStr=(userLoginModuleStr==null)?"0":userLoginModuleStr;

		userLoginModule=new Integer(userLoginModuleStr).intValue();

		userLoginModuleMap=userB.getUserLoginModuleMap();
		userLoginModuleMap=(userLoginModuleMap==null)?"":userLoginModuleMap;

		accLoginModuleDDStr=EJBUtil.createPullDown("dnname",userLoginModule,accLoginModuleIdlist,accLoginModuleDNList);
		//Retrieve the Login Module Details for user's account


		userLastName =userB.getUserLastName();
		userFirstName= userB.getUserFirstName();
		userMidName= userB.getUserMidName();
		userGroup = userB.getUserGrpDefault();
		userLogin = userB.getUserLoginName();
		userLogin=(userLogin==null)?"":userLogin;//JM
		userSecretAns = userB.getUserAnswer();
		userSecretAns=(userSecretAns==null)?"":userSecretAns;
		userSecretQues	= userB.getUserSecQues();
		userSecretQues=(userSecretQues==null)?"":userSecretQues;
		userSession = 	userB.getUserSessionTime();
		userAddPri= addressUserB.getAddPri();
		userAddPri = (   userAddPri  == null      )?"":(  userAddPri ) ;
		userAddCity =addressUserB.getAddCity();
		userAddCity = (   userAddCity  == null      )?"":(  userAddCity ) ;
		userAddState =addressUserB.getAddState();
		userAddState = (   userAddState  == null      )?"":(  userAddState ) ;
		userAddCountry= addressUserB.getAddCountry();
		userAddCountry = (   userAddCountry  == null      )?"":(  userAddCountry ) ;
		userAddZip= addressUserB.getAddZip();
		userAddZip = (   userAddZip  == null      )?"":(  userAddZip ) ;
		userAddPhone= addressUserB.getAddPhone();
		userAddPhone = (   userAddPhone  == null      )?"":(  userAddPhone ) ;
		userEmail = addressUserB.getAddEmail();
		userEmail = ( userEmail == null )?"":(userEmail);//JM
		userJobType =userB.getUserCodelstJobtype();
		userSpl= userB.getUserCodelstSpl();
		userWrkExp =userB.getUserWrkExp();
		userWrkExp = (   userWrkExp  == null      )?"":(  userWrkExp ) ;
		userPhaseInv= userB.getUserPhaseInv();
		userPhaseInv = (   userPhaseInv  == null      )?"":(  userPhaseInv ) ;
		userOldPwd = userB.getUserPwd();
		userOldESign = userB.getUserESign();
		userSiteId = userB.getUserSiteId();
		userSiteId=(userSiteId==null)?"":userSiteId;
		uStat = userB.getUserStatus();
		if (uStat==null)
			uStat = "-";




		siteName =siteB.getSiteName();
		siteAddPri =addressSiteB.getAddPri();
		userAddresId  = userB.getUserPerAddressId();
		esignXpDate=userB.getUserESignExpiryDate();
		pwdXpDate =userB.getUserPwdExpiryDate();
		timeZone = userB.getTimeZoneId();
		userHidden = userB.getUserHidden();//KM


		//JM: 06/09/05 for getting 'default group' drop down...
		if ((mode.equals("M")) && userType.equals("NS") && JFlag.equals("")) {
			//cd1.getAccountGroups(accId);
			//KM-22Aug08
			cd1.getAccountGroupsNoHidden(accId);
			dUsrGrps = cd1.toPullDown("usrgrps");
		}else {
			cd1.getUserGroups(userId,accId);
			dUsrGrps = cd1.toPullDown("usrgrps",EJBUtil.stringToNum(userGroup));
		}
		dAccSites = cd2.toPullDown("accsites",EJBUtil.stringToNum(userSiteId));
		dJobType = cd.toPullDown("jobType", EJBUtil.stringToNum(userJobType));
		dPrimSpl = cd3.toPullDown("primarySpeciality", EJBUtil.stringToNum(userSpl));
	//	dTimeZone = cd4.toPullDown("timeZone", EJBUtil.stringToNum(timeZone));

//JM: 27OCT2006: added
	//Modified by Manimaran on 29Aug08 to fix the timezone problem for Non-system user.
	if ((mode.equals("M")) && userType.equals("NS") && JFlag.equals("") ){
			if (usrTzOverride.equals("")){
				dTimeZone = cd4.toPullDown("timeZone");
		   	} else {
				timeZone=usrTz;
			    dTimeZone = cd4.toPullDown("timeZone", EJBUtil.stringToNum(timeZone));
			}
	}else {

			dTimeZone = cd4.toPullDown("timeZone", EJBUtil.stringToNum(timeZone));

	}
/////

	} else {
		//cd1.getAccountGroups(accId);
		//KM-22Aug08 Modified.
		cd1.getAccountGroupsNoHidden(accId);
		dUsrGrps = cd1.toPullDown("usrgrps");
		dAccSites = cd2.toPullDown("accsites");
		dJobType = cd.toPullDown("jobType");
		dPrimSpl = cd3.toPullDown("primarySpeciality");
	   /* Added for July-August'06 Enhancement (U2) - Default account level settings for User Timezone
	   */
	   if (usrTzOverride.equals("")){
		  dTimeZone = cd4.toPullDown("timeZone");
	   } else {
		 timeZone=usrTz;
		 dTimeZone = cd4.toPullDown("timeZone", EJBUtil.stringToNum(timeZone));
	   }
	}
%>
<Form name = "userdetails" id="userDetForm" method="post" action="updateNewUser.jsp?userType=<%=StringUtil.htmlEncodeXss(userType)%>"  onsubmit="if (validate(document.userdetails,'<%=StringUtil.htmlEncodeXss(userSiteId)%>')==false){setValidateFlag('false');return false;} else {setValidateFlag('true'); return true;}">
	<%
		if (mode.equals("M") || (mode.equals("M") && userType.equals("NS"))){ %>
			<input type="hidden" name="userOldPwd" size = 20>
			<input type="hidden" name="userOldESign" size = 20>
			<input type="hidden" name="userId" size = 20  value = <%=userId%> >
			<input type="hidden" name="userAddId" size = 20  value = <%=StringUtil.htmlEncodeXss(userAddresId)%> >
			<input type="hidden" name="userSession" size = 20  value = <%=StringUtil.htmlEncodeXss(userSession)%> >
			<input type="hidden" name="pwdXpDate" size = 20  value = <%=StringUtil.htmlEncodeXss(pwdXpDate)%> >
			<input type="hidden" name="esignXpDate" size = 20  value = <%=StringUtil.htmlEncodeXss(esignXpDate)%> >
			<input type="hidden" name="JFlag" size = 20  value = <%=StringUtil.htmlEncodeXss(JFlag)%> >
			<input type="hidden" name="userType" size = 20  value = <%=StringUtil.htmlEncodeXss(userType)%> >
	<%
		if(EJBUtil.stringToNum((String)tSession.getValue("userId")) == userId) {
			selfFlag = true;
		}
	} %>
		<input type="hidden" name="mode" size = 20  value = <%=StringUtil.htmlEncodeXss(mode)%> >
		<input type="hidden" name="selfFlag" size = 20  value = <%=selfFlag%> >
		<input type="hidden" name="srcmenu" size = 20  value = <%=StringUtil.htmlEncodeXss(src)%> >
		<input type="hidden" name="pname" size=20 value='<%=StringUtil.htmlEncodeXss(pname)%>'>
		<input type="hidden" name="prevgroup" size = 20  value = <%=StringUtil.htmlEncodeXss(userGroup)%> >
		<P class = "sectionHeadings"> <%=LC.L_Member_Info%><%--Member Information*****--%>
		&nbsp;&nbsp;&nbsp;
	<!-- <A href="#" onClick="openWinMoreDetails('<%=userId%>','<%=StringUtil.htmlEncodeXss(mode)%>','user','<%=pageRight%>');">
	<img src="../jsp/images/moreInfo.png" border="0" title="<%=LC.L_More_UserDets_Upper%>"></A> -->

		</P>
			<%
			if (uStat.equals("B")){
			%><table border="0" cellspacing="0" cellpadding="0">
			<tr><td><FONT class="Mandatory">
			<%=LC.L_Usr_Blocked %><%--User is blocked*****--%>
			</FONT>
			</td></tr></table>
			<%
			}
			if (uStat.equals("D")){
			%><table border="0" cellspacing="0" cellpadding="0">
			<tr><td><FONT class="Mandatory">
			<%
			  if (totalUsersAllowed > 0 && totalUsersAllowed <= existingUsers) {
				 cntExceeded = true;
			  %>
				<%=MC.M_UsrDeact_CntActvCnctAdmin%><%--User is deactivated. The number of users you can activate in this account has exhausted. You cannot activate user. Please contact the Velos Administrator.*****--%>
			  <%
			  }  else {
			%>
				<%=MC.M_User_IsDeactivated%><%--User is deactivated *****--%>
			 <%}%>
			</FONT>
			</td></tr></table>
			<%
			}
			%>
			<div style="border:1">
			
				<div id="userDetailsDIV" class="portlet portletstatus ui-widget ui-widget-border ui-helper-clearfix ui-corner-all">
					<div id="userDetailsTab1content" onclick="toggleDiv('userDetailsTab1')" class="portlet-header portletstatus ui-widget 
					ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all" style="background-color:#CCCCCC;">
						<span class="ui-icon ui-icon-triangle-1-s"></span>
			<%=LC.L_User_Dets%>
					</div>
				<div id='userDetailsTab1'  width="99%">			
		<table width="100%" cellspacing="0" cellpadding="1" border="0">
	<tr>
	  <% if(selfFlag){
		String userCode = userB.getUserCode((new Integer(userId)).toString());
	  %>


		<td width="20%"> <%=LC.L_User_Id%><%--User ID*****--%>  </td>
		<td >
			<input type="text" name="userCode" size = 35 MAXLENGTH = 30 Value="<%=StringUtil.htmlEncodeXss(userCode)%>" READONLY>
		</td>
		<td>&nbsp;</td>
	<%}else{%>
	<td width="50">&nbsp;</td>
	<td width="50">&nbsp;</td>
	<%} %>


	</tr>


	<tr>
		<td width="25%" > <%=LC.L_First_Name%><%--First Name*****--%> <FONT class="Mandatory">* </FONT> </td>
		<td  width="75%" >
		  <input type="text" name="userFirstName" size = 35 MAXLENGTH = 30 Value="<%=StringUtil.htmlEncodeXss(userFirstName)%>">
		</td>
		<td>&nbsp;</td>
	</tr>
	</div>
			</div>
	<tr>
		<td > <%=LC.L_Last_Name%><%--Last Name*****--%> <FONT class="Mandatory">* </FONT> </td>
		<td>
		  <input type="text" name="userLastName" size = 35 MAXLENGTH = 30 Value="<%=StringUtil.htmlEncodeXss(userLastName)%>">
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td ><%=LC.L_Address%><%--Address*****--%> </td>
		<td>
		  <input type="text" name="userAddress" size = 35 MAXLENGTH = 50 Value="<%=StringUtil.htmlEncodeXss(userAddPri)%>">
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td ><%=LC.L_City%><%--City*****--%> </td>
		<td>
		  <input type="text" name="userCity" size = 35  MAXLENGTH = 30 Value="<%=StringUtil.htmlEncodeXss(userAddCity)%>">
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td ><%=LC.L_State%><%--State*****--%> </td>
		<td>
		  <input type="text" name="userState" size = 35 MAXLENGTH = 30 Value="<%=StringUtil.htmlEncodeXss(userAddState)%>">
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td ><%=LC.L_ZipOrPostal_Code%><%--Zip/Postal Code*****--%> </td>
		<td>
		  <input type="text" name="userZip" size = 15 MAXLENGTH = 15 Value="<%=StringUtil.htmlEncodeXss(userAddZip)%>">
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td ><%=LC.L_Country%><%--Country*****--%> </td>
		<td>
		  <input type="text" name="userCountry" size = 35 MAXLENGTH = 50 Value="<%=StringUtil.htmlEncodeXss(userAddCountry)%>">
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td > <%=LC.L_Phone%><%--Phone*****--%> </td>
		<td>
		  <input type="text" name="userPhone" size = 35 MAXLENGTH = 100 Value="<%=StringUtil.htmlEncodeXss(userAddPhone)%>">
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td > <%=LC.L_EhypMail%><%--E-Mail*****--%> <FONT class="Mandatory">* </FONT> </td>
		<td>
		  <input type="text" name="userEmail" size = 35 MAXLENGTH = 100 Value="<%=StringUtil.htmlEncodeXss(userEmail)%>">
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td > <%=LC.L_Job_Type%><%--Job Type*****--%> </td>
		<td> <%=dJobType%> </td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td valign="top"> <%=LC.L_Time_Zone%><%--Time Zone*****--%> <FONT class="Mandatory">* </FONT> </td>
		<td>
		<%
		if((EJBUtil.stringToNum(usrTzOverride))==0 && !(usrTz.equals(""))){
		 %>
			<input type="text" name="timeZoneText" size=70 MAXLENGTH=100 readonly value="" style="border:none;background:#f7f7f7">
			<div style="visibility:hidden;height:10px;left:0px;">
				<%=dTimeZone%>
			</div>
		<script>
				document.userdetails.timeZoneText.value = document.userdetails.timeZone.options[document.userdetails.timeZone.selectedIndex].text;
		</script>
		<%} else {%>
		<%=dTimeZone%>
	<%}%>
		</td>
		<td>&nbsp;</td>
	</tr>

	<tr>
		<td > <%=LC.L_Primary_Speciality%><%--Primary Speciality*****--%>
		</td>
		<td > <%=dPrimSpl%> </td>
<td>&nbsp;</td>
	</tr>
	<tr>
		<td  > # <%=LC.L_Yrs_Research%><%--Years in Research?*****--%>
		</td>
		<td >
		  <input type="text" name="userWrkExp" size = 20 MAXLENGTH = 100 Value="<%=StringUtil.htmlEncodeXss(userWrkExp)%>">
		</td>
		<td >
		</td>
		
	</tr>
	<tr>
		<td > <%=LC.L_Trial_PhaseInvolvement%><%--Trial Phase Involvement*****--%> </td>
		<td >
		  <input type="text" name="userPhaseInv" size = 20 MAXLENGTH = 100 Value="<%=StringUtil.htmlEncodeXss(userPhaseInv)%>">
		</td>
		<td >
		</td>
	</tr>
	<tr>
		<td height="10"></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	
	<%if (pname!=null){ %>
	<tr>
		<td colspan=3>
			<P class = "sectionHeadings"> <%=LC.L_Acc_Info%><%--Account Information*****--%> </A>
			</P>
		</td>
	</tr>
<tr>
		<td height="10"></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<% if (ldapEnabled.equals("Y")) {%>
	<tr>
	<td ><%=LC.L_Ldap_Login%><%--LDAP Login*****--%></td> <td>
	<input type="text" name="lmmap" size =20 MAXLENGTH = 50 Value="<%=StringUtil.htmlEncodeXss(userLoginModuleMap)%>">&nbsp;
	<%=accLoginModuleDDStr%></td>
	</tr>
	<%} else { %>
	<input type="hidden" name="lmmap" size =20 MAXLENGTH = 50 Value="<%=StringUtil.htmlEncodeXss(userLoginModuleMap)%>">&nbsp;
	<input type="hidden" name="dnname" size =20 MAXLENGTH = 50 Value="<%=userLoginModule%>">&nbsp;


	<%}} %>
	
	

	<% if (pname ==null ){ //show Account Info section only when user comes form Manage Account menu %>
	<tr>
		<td colspan=3>
			<P class = "sectionHeadings"> <%=LC.L_Acc_Info%><%--Account Information*****--%> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A HREF="pwdtips.htm" target="_new"><%=MC.M_SelcPwdCrt%><%--Criteria for selecting a Password*****--%></A>
			</P>
		</td>
	</tr>
	<tr>
		<td height=10></td>
	</tr>
	<tr>
		  <td><%=MC.M_Users_PrimaryOrganization%><%--User's Primary Organization*****--%> <FONT class="Mandatory">*</FONT></td>
		  <td > <%=dAccSites%> &nbsp;&nbsp;&nbsp;
			<%
	//JM: 04/11/2005
	if ( !userType.equals("NS") ){
		if (mode.equals("M")) { //check for modify mode %>
			<%//if (String.valueOf(mngOrgAccRight).compareTo("1") == 0) { //manage orgs account right check %>
			<% if (pageRight >= 4) { //manage users Group right check%>
			<A href=# onclick="openWin(userdetails);"><%=LC.L_Multi_OrgAccess%><%--Multiple Organization Access*****--%></A>
			<%}
			  }
		  }
			  //}//mode%>
		</td>
	</tr>
	<tr>
		<td > <%=LC.L_Default_Group%><%--Default Group*****--%> <FONT class="Mandatory">* </FONT> </td>
		<td> <%=dUsrGrps%>  </td>

	</tr>
	<%
		if (mode.equals("M") && selfFlag == true){
	%>
	<tr>
		<td> <%=LC.L_Login%><%--Login*****--%> <FONT class="Mandatory">* </FONT> </td>
		<td>
			  <input type="text" name="userLogin1" id="userLogin1" size = 35 MAXLENGTH = 15 Value="<%=StringUtil.htmlEncodeXss(userLogin)%>" readonly>
			  <input type="hidden" name="userLogin" id="userLogin" size = 35 MAXLENGTH = 15 Value="<%=StringUtil.htmlEncodeXss(userLogin)%>">
	<%
		} else if (mode.equals("M") && selfFlag == false){
	if(!(mode.equals("M") && userType.equals("NS"))||(mode.equals("M") && userType.equals("NS") && (!JFlag.equals("")))){
		%>
	<tr>
		<td > <%=LC.L_Login%><%--Login*****--%> <FONT class="Mandatory">* </FONT> </td>
		<td><input type="text" name="userLogin1" id="userLogin1" size = 35 MAXLENGTH = 15 Value="<%=StringUtil.htmlEncodeXss(userLogin)%>" readonly>
		<input type="hidden" name="userLogin" id="userLogin" size = 35 MAXLENGTH = 15 Value="<%=StringUtil.htmlEncodeXss(userLogin)%>">
			  <%
			  }
		}
		if(!mode.equals("M") || (mode.equals("M") && userType.equals("NS") && JFlag.equals(""))) { //else for if of mode and selfFlag
	%>
	<tr>
		<td > <%=LC.L_Login%><%--Login*****--%> <FONT class="Mandatory">* </FONT> </td>
		<td>
		  <input type="text" name="userLogin" id="userLogin" size = 35 MAXLENGTH = 15 >
	<%
		}
	%>
		</td>

	</tr>
<%
if (ldapEnabled.equals("Y")){
%>
	<tr>
	<td ><%=LC.L_Ldap_Login%><%--LDAP Login*****--%></td> <td>
	<input type="text" name="lmmap" size =20 MAXLENGTH = 50 Value="<%=StringUtil.htmlEncodeXss(userLoginModuleMap)%>">&nbsp;
	<%=accLoginModuleDDStr%></td>
	</tr>
	<%} else { %>
	<input type="hidden" name="lmmap" size =20 MAXLENGTH = 50 Value="<%=StringUtil.htmlEncodeXss(userLoginModuleMap)%>">&nbsp;
	<input type="hidden" name="dnname" size =20 MAXLENGTH = 50 Value="<%=userLoginModule%>">&nbsp;

	<%} %>
		 <% if (mode.equals("M") && selfFlag == true){ %>
			  <input type="hidden" name="userPassword" size = 25 MAXLENGTH = 15>
		  <%
		  } else if (mode.equals("M") && selfFlag == false){
				if(!(mode.equals("M") && userType.equals("NS"))||(mode.equals("M") && userType.equals("NS") && (!JFlag.equals("")))){
			%>
			  <input type="hidden" name="userPassword" size = 35 MAXLENGTH = 15 >
			  <% }
			}
		  if(!mode.equals("M") || (mode.equals("M") && userType.equals("NS") && JFlag.equals("")) ){ %>
	<tr>
		<td > <%=LC.L_Password%><%--Password*****--%> <FONT class="Mandatory">* </FONT> </td>
		<td >
		  <input type="password" name="userPassword" id="userPassword" class="passwordCheck" size = 35 MAXLENGTH = 15 >
		</td>
	</tr>
			<%}%>
		<% if (mode.equals("M") && selfFlag == true){ %>
		  <input type="hidden" name="userConfirmPassword" size = 25 MAXLENGTH = 15 >
		<%}else if(!mode.equals("M") || (mode.equals("M") && userType.equals("NS") && JFlag.equals(""))) { %>
	<tr>
		<td > <%=LC.L_Confirm_Pwd%><%--Confirm Password*****--%> <FONT class="Mandatory">* </FONT> </td>
		<td>
			  <input type="password" name="userConfirmPassword" size = 35  MAXLENGTH = 15 >
		</td>

	</tr>
		<%}%>
		<% if (mode.equals("M") && selfFlag == true){ %>
			  <input type="hidden" name="userESign" size = 25 MAXLENGTH = 8 readonly>
		  <% }else if (mode.equals("M") && selfFlag == false){
			if(!(mode.equals("M") && userType.equals("NS"))||(mode.equals("M") && userType.equals("NS") && (!JFlag.equals("")))){
			%>
			  <input type="hidden" name="userESign" size = 25 MAXLENGTH = 8 >
			  <%
			}
		}
	 if(!mode.equals("M") || (mode.equals("M") && userType.equals("NS") && JFlag.equals(""))){ %>
	<tr class="eSign">
		<td > <%=LC.L_Esignature%><%--e-Signature*****--%> <FONT class="Mandatory">* </FONT> </td>
		<td>
			<input type="password" name="userESign" size = 35 MAXLENGTH = 8 >
			  <font class="mandatory">(<%=LC.L_Min4_Digits%><%--minimum 4 digits*****--%>)</font>
		</td>

		  </tr>
		 <%}%>
		  <% if (mode.equals("M") && selfFlag == true){ %>
			  <input type="hidden" name="userConfirmESign" size = 25 MAXLENGTH = 8 >
		  <%}else if(!mode.equals("M") || (mode.equals("M") && userType.equals("NS") && JFlag.equals(""))) { %>
	  <tr>
			<td > <%=LC.L_Confirm_Esign%><%--Confirm e-Signature*****--%> <FONT class="Mandatory">* </FONT> </td>
			<td>
			  <input type="password" name="userConfirmESign" size = 35  MAXLENGTH = 8>
			</td>

	  </tr>
		  <%}%>
		  <%
	  if ((mode.equals("M") && selfFlag == true) || mode.equals("N") || (mode.equals("M") && userType.equals("NS") && JFlag.equals(""))) {
		%>
	  <tr>
			<td > <%=LC.L_Secret_Que%><%--Secret Question*****--%> </td>
			<td>
			  <input type="text" name="userSecretQues" size = 35 MAXLENGTH = 150  Value="<%=StringUtil.htmlEncodeXss(userSecretQues)%>">
			</td>

	  </tr>
	  <tr>
			<td > <%=LC.L_Your_Answer%><%--Your Answer*****--%> </td>
			<td>
			  <input type="text" name="userSecretAns" size = 35 MAXLENGTH = 150 Value="<%=StringUtil.htmlEncodeXss(userSecretAns)%>">
			</td>

	  </tr>
		  <% }	%>


		  <input type=hidden name="deactivateFromTeam" value="0" >

		<tr>
		<%
		if (mode.equals("M") && userType.equals("NS") && JFlag.equals("")){
		%>
	<td><input type=hidden name="userStatus" value="A" onClick="alertDeactivate(document.userdetails,'A')"> </td>
	<%}else {%>
	 <input type="hidden" name="userSecretQues" size = 35 MAXLENGTH = 150  Value="<%=StringUtil.htmlEncodeXss(userSecretQues)%>">
     <input type="hidden" name="userSecretAns" size = 35 MAXLENGTH = 150 Value="<%=StringUtil.htmlEncodeXss(userSecretAns)%>">

	<td> <%=LC.L_User_Status%><%--User Status*****--%> </td>

					<% if (mode.equals("N"))
				{%>

				<td><input type="Radio" name="userStatus" value="A" CHECKED onClick="alertDeactivate(document.userdetails,'A')"><%=LC.L_Active%><%--Active*****--%>
				<input type="Radio" name="userStatus" value="D" onClick="alertDeactivate(document.userdetails,'D')"><%=LC.L_Inactive%><%--Inactive*****--%> </td>

				<%	}
				else {
					if (uStat.equals("A")){	%>
						<td><input type="Radio" name="userStatus" value="A" CHECKED onClick="alertDeactivate(document.userdetails,'A')"><%=LC.L_Active%><%--Active*****--%>
						<input type="Radio" name="userStatus" value="D" onClick="alertDeactivate(document.userdetails,'D')" ><%=LC.L_Inactive%><%--Inactive*****--%> </td>
						<%
						}
						else if(uStat.equals("D"))
						{
						%>
						<td><input type="Radio" name="userStatus" value="A" <%if (cntExceeded) {%>disabled<%}%> onClick="alertDeactivate(document.userdetails,'A')"><%=LC.L_Active%><%--Active*****--%>
						<input type="Radio" name="userStatus" value="D" CHECKED ><%=LC.L_Inactive%><%--Inactive*****--%> </td>

						<%
						}else{%>
						<td><input type="Radio" name="userStatus" value="A" onClick="alertDeactivate(document.userdetails,'A')"><%=LC.L_Active%><%--Active*****--%>
							<input type="Radio" name="userStatus" value="D" onClick="alertDeactivate(document.userdetails,'D')" ><%=LC.L_Inactive%><%--Inactive*****--%> </td>
							<input type="hidden" name="userStatus" value="B" onClick="alertDeactivate(document.userdetails,'B')">
						<%}
						}
						%>
						<td>
					</td>
				<%}%>
		</tr>
			<%} // (end of pname==null)
			 else { // if from personalize%>
				 <input type="hidden" name="usrgrps" size = 20  value = <%=StringUtil.htmlEncodeXss(userGroup)%> >
			<% }
		//Added by Manimaran for Enh.#U11
		String checkStr ="";
		if (userHidden.equals("1"))
	   		checkStr = "checked";
		if (mode.equals("M") && pname==null){ %>
		<tr><td colspan="3"><input type="checkbox" name="userHidden" <%=checkStr%>> <%=MC.M_HideUsrIn_LkUp%><%--Hide this User in User selection Lookups*****--%></td></tr>
	<%} else {%>
			<!--KM-Modified on 1Sep08 -->
	 	 <input type="hidden" name="userHidden" value="<%=StringUtil.htmlEncodeXss(userHidden)%>">
	 	<%
	}%>

		</table>
</div>
</div>
<div id="userMoreDetailsDIV" class="portlet portletstatus ui-widget ui-widget-border ui-helper-clearfix ui-corner-all">
		<div id="userDetailsTab2content" onclick="toggleDiv('userDetailsTab2');" class="portlet-header portletstatus ui-widget 
		ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all" style="background-color:#CCCCCC;">
			<span class="ui-icon ui-icon-triangle-1-s"></span>
		<%=LC.L_More_Dets%>
		</div>
		<div id='userDetailsTab2'  width="99%">
			<jsp:include page="moredetails.jsp" >
				<jsp:param name="modId" value="<%=userId %>"/>
				<jsp:param name="modName" value="user"/>
				<jsp:param name="includeMode" value="Y"/>
			</jsp:include>
		</div>
</div>


</div>
	  <%if (mode.equals("N") || ( !(mode.equals("N")) && (pageRight >= 6))){%>
	<TABLE width="100%" cellspacing="0" cellpadding="0" border="0" >
	<%
		if (mode.equals("N") || (mode.equals("M") && userType.equals("NS") && JFlag.equals("") ))
		{%>
			<tr><td colspan="3"><input type="checkbox" name="donotsend_notification" value="1" >
			  <%=MC.M_DntSendLoginNotfic_ToUsr%><%--Do not send 'login details' notification to the user*****--%>
			</td></tr>
		<% }%>

		<tr>
			<td>
		<!--JM: 19June2009: implementing common Submit Bar in all pages -->
		<%if (mode.equals("N")){%>
			<jsp:include page="submitBar.jsp" flush="true">
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="userDetForm"/>
			<jsp:param name="showDiscard" value="N"/>
		</jsp:include>
		<%}else {
		if (pageRight >= 6){%>
			<jsp:include page="submitBar.jsp" flush="true">
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="userDetForm"/>
			<jsp:param name="showDiscard" value="N"/>
		</jsp:include>


		<%}
		}%>
		</td>
		</tr>
	</TABLE>
	
	
		<%}%>
			  <input type="hidden" name="userSiteId" value = <%=StringUtil.htmlEncodeXss(userSiteId)%>>
			  <input type="hidden" name="changeOrgFlg" value="false">
			  
			  <input type="hidden" id="isSubmitFlag" value="" />
			  
</Form>

  <%} //end of if that checks the number of existing users with allowed users
	} //end of if body for page right
	else{
	%>
	  <jsp:include page="accessdenied.jsp" flush="true"/>
	<%
	} //end of else body for page right
	}//end of if body for session
	else{
%>

  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>

  <div class = "myHomebottomPanel">

	<jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

<div class ="mainMenu" id = "emenu">

  <jsp:include page="getmenu.jsp" flush="true"/>
</div>

</body>

</html>


