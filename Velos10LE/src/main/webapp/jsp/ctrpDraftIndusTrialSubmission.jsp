<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC" %>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr>
<td width="50%" valign="top">
<table width="50%" cellpadding="0" cellspacing="0" border="0">
<tr>
<td align="right" height="22px" width="30%"><%=LC.CTRP_DraftSectSubmType %>&nbsp;
	<A href="#" align="left" alt="" onmouseout="return nd();" 
	onmouseover="return overlib('<%=MC.CTRP_DraftSubmTypeHelpIndus%>',CAPTION,'<%=LC.CTRP_DraftSectSubmType%>',RIGHT , BELOW);">
	<img src="../images/jpg/help.jpg" border="0"></A>
</td>
<td width="2%"><FONT id="draft_subm_type_asterisk" style="visibility:visible" class="Mandatory">*</FONT></td>
<td align="left"> <s:property value="ctrpDraftJB.indusTrialTypeMenu" escape="false"/>  </td>

</tr>
<tr>
<td height="22px">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td align="right" height="22px"><%=LC.CTRP_DraftTrialOwner %>&nbsp;</td>
<td ><FONT id="draft_subm_type_asterisk" style="visibility:visible" class="Mandatory">*</FONT></td>
<td><a href="javascript:openSelectUser(singleUser);" ><%=LC.L_Select %></a></td> 

</tr>

<tr>
<td align="right" height="22px">
	<%=LC.L_First_Name %>&nbsp;
	<span id="ctrpDraftJB.trialFirstName_error" class="errorMessage" style="display:none;"></span>
</td>
<td align="right" height="22px">&nbsp;</td>
<td><s:hidden name="ctrpDraftJB.trialUsrId" id="ctrpDraftJB.trialUsrId" ></s:hidden>
<s:textfield name="ctrpDraftJB.trialFirstName" id="ctrpDraftJB.trialFirstName" readonly="true" size="50"></s:textfield> </td>

</tr>

<tr>
<td align="right" height="22px">
	<%=LC.L_Last_Name %>&nbsp;
	<span id="ctrpDraftJB.trialLastName_error" class="errorMessage" style="display:none;"></span>
</td>
<td align="right" height="22px">&nbsp;</td>
<td>
<s:textfield name="ctrpDraftJB.trialLastName" id="ctrpDraftJB.trialLastName" readonly="true" size="50"></s:textfield> </td>

</tr>
<tr>
<td align="right" height="22px">
	<%=LC.L_Email_Addr %>&nbsp;
	<span id="ctrpDraftJB.trialEmailId_error" class="errorMessage" style="display:none;"></span>
</td>
<td align="right" height="22px">&nbsp;</td>
<td>
<s:textfield name="ctrpDraftJB.trialEmailId" id="ctrpDraftJB.trialEmailId" readonly="true" size="50"></s:textfield></td>
</tr>
</table>
</td>
<td width="50%" valign="top">
<table border="0">
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
</td>
</tr>
</table>