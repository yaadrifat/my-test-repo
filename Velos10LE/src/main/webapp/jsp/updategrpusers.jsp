<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id="grpusr" scope="request" class="com.velos.eres.web.usrGrp.UsrGrpJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
<%
int ret = 0;
String grpId,users[],cnt,user="0";
int totrows =0,count=0,id; 
String src, grpName;
src=request.getParameter("src"); 
grpId = request.getParameter("grpId"); //GroupId
grpName = request.getParameter("grpName"); //GroupName

String eSign = request.getParameter("eSign");

HttpSession tSession = request.getSession(true);  
if (sessionmaint.isValidSession(tSession))
   {	
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
	String oldESign = (String) tSession.getValue("eSign");


	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
String ipAdd = (String) tSession.getValue("ipAdd");
String usr = (String) tSession.getValue("userId");

grpusr.setUsrGrpGroupId(grpId);
totrows = Integer.parseInt(request.getParameter("totalrows")); //Total Rows
if (totrows > 1){
	users = request.getParameterValues("assign");
	for (count=0;count<totrows;count++){
	
	
		user = users[count];
		grpusr.setUsrGrpUserId(user);
		grpusr.setCreator(usr); 
		grpusr.setIpAdd(ipAdd);

		grpusr.setUsrGrpDetails();
	}
}else{
	user = request.getParameter("assign");
	grpusr.setUsrGrpUserId(user);
	grpusr.setCreator(usr); 
	grpusr.setIpAdd(ipAdd);

	grpusr.setUsrGrpDetails();
}
%>
<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc %><%-- Data was saved successfully*****--%> </p>


<%--
totrows**<%=totrows%><br>
user**<%=user%><br>
grpId**<%=grpId%><br>
grpName**<%=grpName%><br>
--%>


<META HTTP-EQUIV=Refresh CONTENT="1; URL=groupUsers.jsp?srcmenu=<%=src%>&mode=M&groupId=<%=grpId%>&grpName=<%=grpName%>">


<%
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>
</HTML>
