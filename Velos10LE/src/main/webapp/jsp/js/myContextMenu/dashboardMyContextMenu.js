/**
 * Author  : Yogesh Kumar
 * Date	   : 10/03/2012 
 * Purpose : To display Context menu for Study and Patient.
 * 
 */

/**
 * When Context Menu is clicked based on the value link is opened in the same window or in the pop. 
 */
	/* click functionality*/
	$j('.context-menu-item').on('click',function(e){
		var linkValue="";
		var accessRights="";
			linkValue = $j(this).find('#actionValue').html();
			if(linkValue!=null){
			linkValue=linkValue.replace(/&amp;/gi, '&');
			var accessProp = linkValue.split('##');
			var lengthOf = accessProp.length;
			if(lengthOf==2){
				var accessRgths = accessProp[0].split('||');
				var lengthOfAcc = accessRgths.length;
				var isAccess =true;
				
				if(lengthOfAcc==2){
					if(f_check_perm(accessRgths[1],accessRgths[0])==false){
						 isAccess =false;	
					}
				}else if(lengthOfAcc==3){
					if(f_check_perm_org(accessRgths[1],accessRgths[2],accessRgths[0])==false){
						 isAccess =false;	
					}
				}
			if(isAccess == true){
				window.location=accessProp[1];
			}else{
				return false;
			}
				
				
			}else{
			window.location=linkValue;
			}
			}else{
				
				/*check access Rights*/
				accessRights = $j(this).find('#accessRights').html();
				var accessProp = accessRights.split('||');
				var lengthOf = accessProp.length;
				
				var isAccess =true;
				
				if(lengthOf==2){
					if(f_check_perm(accessProp[1],accessProp[0])==false){
						 isAccess =false;	
					}
				}else if(lengthOf==3){
					if(f_check_perm_org(accessProp[1],accessProp[2],accessProp[0])==false){
						 isAccess =false;	
					}
				}
				
			if(isAccess == true){
				/*open Parent page*/
				linkValue = $j(this).find('#parentValue').html();
				linkValue=linkValue.replace(/&amp;/gi, '&');
				window.location=linkValue;
				/*open child page as pop*/
				linkValue = $j(this).find('#popValue').html();
				linkValue=linkValue.replace(/&amp;/gi, '&');
				//var dispVal ="'"+ $j(this).find('#displayValue').html()+"'";
				windowName=window.open(linkValue,'',"toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=680,height=500, top = 100 , left = 100");
				}else{
					return false;
				}
			}
		
	});

	/**
	 * Binded right click to open Study sub menu as context menu. 
	 */
	/* Show study Menu pop  menu*/
	$j('.studyMenuPop').on('contextmenu',function(e){

		var studyId= $j(this).attr('id');
		//alert("hi I am clicked "+$j(this).attr('id'));
		 $j(this).addClass('busy'); 
		$j.ajax({
		          type: "POST",
		          url: "studyMenuPop.jsp",
		          data: "studyId="+studyId,
		          async: false,
		          success: function(result){ 
	          		$j('#myContextMenu').html(result);
	          		$j('.menuTable').css('width',"250px" ).css('left',e.pageX+"px" ).css('top',e.pageY+"px");
	          		$j('.menuTable').slideDown('slow');
	          		var isVisible = isFullyVisibleStudy('.menuTable');
	          		if(isVisible==false)
	          		{
	          			var tp= newTopStudy('.menuTable', e.pageY);
	          			$j('.menuTable').css('left',(e.pageX)+"px" ).css('top',tp+"px");	
	          		}
	          		$j('.studyMenuPop').removeClass('busy');  
	      		}
		});  
		return false; });
	
	
	/**
	 * Binded right click to open Patient sub menu as context menu. 
	 */
	/* Show Patient Menu pop  menu*/
	$j('.patientMenuPop').on('contextmenu',function(e){

		var patUrl= $j(this).find("#patientMenuPop").val();
		//alert("hi I am clicked "+$j(this).attr('id'));
		 $j(this).addClass('busy'); 
		$j.ajax({
		          type: "POST",
		          url: "PatientMenuPop.jsp",
		         data: patUrl,
		          async: false,
		          success: function(result){ 
	          		$j('#myContextMenu').html(result);
	          		$j('.menuTable').css('width',"250px" ).css('left',e.pageX+"px" ).css('top',e.pageY+"px");
	          		$j('.menuTable').slideDown('slow');
	          		var isVisible = isFullyVisiblePat('.menuTable');
	          		if(isVisible==false)
	          		{
	          			var tp= newTopPat('.menuTable', e.pageY);
	          			$j('.menuTable').css('left',(e.pageX)+"px" ).css('top',tp+"px");	
	          		}
	          		$j('.patientMenuPop').removeClass('busy');  
	      		}
		});  
		return false; });

$j(document).ready( function(){
	/**
	 * Binded click to hide context menu when clicked anywhere in the page. 
	 */
	$j(document.body).click(function(e){$j('.menuTable').hide();});
	/**
	 * Binded right click to hide context menu when right clicked anywhere in the page. 
	 */
	$j(document.body).bind('contextmenu',function(){$j('.menuTable').hide();});

	/**
	 * Keypress event (ESC) to hide context menu. 
	 */
	$j(document.body).keypress(function(e){
		var code = e.keyCode ? e.keyCode : e.which;
        if(code == 27){
        	$j('.menuTable').hide();
        }
    });
});	
	/**
	 * Binded Mouse enter event to display Study sub Menus. 
	 */
	$j('#openStudySubMenu').on('mouseenter',function() {
			$j("#studySubMenu").slideDown("slow");
	});
	/**
	 * Binded Mouse enter event to display Milestone sub Menus. 
	 */
	$j('#openMileSubMenu').on('mouseenter',function() {
			$j("#mileSubMenu").slideDown("slow");
	});
	/**
	 * Binded Mouse enter event to display Study Status sub Menus. 
	 */
	$j('#openStatSubMenu').on('mouseenter',function() {
		$j("#statSubMenu").slideDown("slow");
	});
	/**
	 * Binded Mouse enter event to display Study Team sub Menus. 
	 */
	$j('#openTeamSubMenu').on('mouseenter',function() {
		$j("#teamSubMenu").slideDown("slow");
	});
	/**
	 * Binded Mouse enter event to display Patient Protocol sub Menus. 
	 */
	$j('#openProtsSubMenu').on('mouseenter',function() {
		$j("#protSubMenu").slideDown("slow");
	});
	/**
	 * Binded Mouse enter event to display Study Team sub Menus. 
	 */
	$j('#openApndxSubMenu').on('mouseenter',function() {
		$j("#apndxSubMenu").slideDown("slow");
	});
	/**
	 * Binded Mouse enter event to hide sub Menus when mouse enters on the menus without sub menus. 
	 */
	$j('#noSubMenu').on('mouseenter',function() {
			$j('.subMenus').slideUp();
			
	});
	/**
	 * Binded Mouse Leave event to hide sub Menus when when mouse leaves the extended sub menus. 
	 */
	/*$j('.subMenus').on('mouseleave',function() {
		$j(this).slideUp();

	 });*/
							
	
	/**
	 * Function to get the parameter value from the URL. 
	 */
	function GetURLParameter(sParam)
	{
	    var sPageURL = window.location.search.substring(1);
	    var sURLVariables = sPageURL.split('&');
	    for (var i = 0; i < sURLVariables.length; i++)
	    {
	        var sParameterName = sURLVariables[i].split('=');
	        if (sParameterName[0] == sParam)
	        {
	            return sParameterName[1];
	        }
	   }
	}
	
	function isFullyVisibleStudy(elem){
		var off = $j(elem).offset();
		var t = off.top;
		var l = off.left;
		var h = $j(elem).height();
		var w = $j(elem).width();
		var docH = $j(window).height();
		var docW = $j(window).width();
		var isEntirelyVisible = (t > 0 && l > 0 && t + h+100 < docH && l+ w < docW);
		return isEntirelyVisible;
	}
	function newTopStudy(elem, yCoOrdinate){
		var off = $j(elem).offset();
		var h = $j(elem).height();
		var docH = $j(window).height();
		if ((yCoOrdinate+h) > docH){
			return (yCoOrdinate-h);
		}
		return (docH-(h+100));
	}
	function isFullyVisiblePat(elem){
		var off = $j(elem).offset();
		var t = off.top;
		var l = off.left;
		var h = $j(elem).height();
		var w = $j(elem).width();
		var docH = $j(window).height();
		var docW = $j(window).width();
		var isEntirelyVisible = (t > 0 && l > 0 && t + h+ 100 < docH && l+ w < docW);
		return isEntirelyVisible;
	}
	function newTopPat(elem, yCoOrdinate){
		var off = $j(elem).offset();
		var h = $j(elem).height();
		var docH = $j(window).height();
		if ((yCoOrdinate+h) > docH){
			return (yCoOrdinate-h);
		}
		return (docH-(h+100));
	}
	
	
/**  END OF FILE*/