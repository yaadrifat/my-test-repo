
yuiDataTabletoJSON = function(p_myDataTable)
{	
var records = p_myDataTable.getRecordSet().getRecords();
var str="[";
 for (var i=0; i < records.length; i++) {
  str= str+"{\"";
  var keys = p_myDataTable.getColumnSet().keys;
  for (var j=0; j < keys.length; j++) {
   str= str+keys[j].getKey();
   str=str+"\":\"";
   var oData = records[i].getData(keys[j].getKey());
   var oColumn = p_myDataTable.getColumn(keys[j].getKey()); 
   if (oColumn.formatter == 'checkbox'){
	   oData = (oData==undefined || oData==null)?'false':oData;
   }
   str=str+oData;
   if(j== (keys.length)-1)
   {
    if(i== (records.length)-1)
    {
     str= str+"\"}";
    }
    else
    {
     str= str+"\"},";
    }
   }
   else
   {
    str= str+"\",\"";
   }
  }
 }   
 str=str+"]";
       // alert(str);
 return str;

}
