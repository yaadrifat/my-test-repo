	//added by Bikash for INVP 2.8	
//Handle the click event for add to cart link. 
var SelItems_AddSucc=M_SelItems_AddSucc;
var PlsSel_OneItem=M_PlsSel_OneItem;
var varViewCart=L_ViewCart;
var AddPrep_Cart=M_AddPrep_Cart;
var Manage_PreparationCart=L_Manage_PreparationCart;
var Prepare_Samples=L_Prepare_Samples;
addToCart = function(e){
	
				
				var selectRowId = this.startIndex;				
				var param = "[ROWSEP]";
				var colsep ="[COLSEP]";
				var rowsep = "[ROWSEP]"
				var personCodeList = new Array();
				var studyNumberList = new Array();
				var calendarList = new Array();
				var visitList = new Array();
				var dateList = new Array();
				var eventList = new Array();
				var kitList = new Array();
				var pkStorageList = new Array();
				var FkSchEventList = new Array();
				var FkVisitList = new Array();
				var FkStudyList = new Array();
				var FkPerList = new Array();
				var FkSiteList = new Array();	
				
				var param;
				var checkSelected = false;

				//KM-5788 and 5789
				var pgRight = document.uiinclude.pgright.value;
				if (! f_check_perm(pgRight,'N') )
				{
					return false;
				}
				   records = this.globalSpecimenDataTable.getRecordSet().getRecords();
				  for (i=0,j=0; i < records.length; i++) {   
					   cell = document.getElementById('check_'+selectRowId);		
					  if (cell.checked) {					  
						  checkSelected = true;
					  personCodeList[j]=records[i].getData("PERSON_CODE");					  
					  studyNumberList[j]=records[i].getData("STUDY_NUMBER");
					  calendarList[j]=records[i].getData("PROTOCOL_NAME");
					  visitList[j]=records[i].getData("VISIT_NAME");
					  dateList[j]=records[i].getData("EVENT_SCHDATE_DATESORT");//AK:Fixed BUG#5956 (30thMar2011)
					  eventList[j]=records[i].getData("EVENT_NAME");
					  kitList[j]=records[i].getData("STORAGE_KIT");
					  pkStorageList[j]=records[i].getData("PK_STORAGE");
					  FkSchEventList[j]=records[i].getData("FK_SCH_EVENTS1");
					  FkVisitList[j]=records[i].getData("FK_VISIT");
					  FkStudyList[j]=records[i].getData("FK_STUDY");
					  FkPerList[j]=records[i].getData("FK_PER");
					  FkSiteList[j]=records[i].getData("FK_SITE");
					 // alert(records[i].getData("PK_STORAGE")+"dd "+records[i].getData("FK_SCH_EVENTS1")+" dd"+records[i].getData("FK_VISIT")+"dd "+records[i].getData("FK_STUDY"));
					  j++;
					  } 
					  selectRowId ++;
				    }				   
				   if(checkSelected == true){
				 for (i=0; i < personCodeList.length; i++) {
					 param = param+colsep+personCodeList[i]+colsep+studyNumberList[i]+colsep+calendarList[i]+colsep
			           +visitList[i]+colsep+dateList[i]+colsep+eventList[i]+colsep+kitList[i]+colsep+pkStorageList[i]
			           +colsep+FkSchEventList[i]+colsep+FkVisitList[i]+colsep+FkStudyList[i]+colsep+FkPerList[i]
			           +colsep+FkSiteList[i];
				   		if(i<(personCodeList.length-1)){
				   			param = param+rowsep;
				   		}				  
				  }
				// alert(param);
			   var rows=personCodeList.length;
			   var columns = 2;
			   var refObj=$E.getTarget(e);		
			   new VELOS.ajaxObject("addToCart.jsp", {
			   		urlData:"dataParam=" + param + "&rows="+rows +"&columns="+columns,
					reqType:"POST",
					context:refObj
					    }
				).startRequest();
			   alert(SelItems_AddSucc);/*alert("Selected item(s) are added successfully.");*****/
					   }
					   else{
						   alert(PlsSel_OneItem);/*alert("Please select at least one item.");*****/

			     }
			}
	//added by Bikash for INVP 2.8
//Handle the click event for manage cart link. 
	manageCart = function(e){	
		//KM-5788 and 5789
		var pgRight = document.uiinclude.pgright.value;
		if (! f_check_perm(pgRight,'N') )
		{
			return false;
		}
		windowName=window.open("managePreparationCart.jsp?","Information","toolbar=no,scrollbars=no,resizable=yes,menubar=no,status=yes,width=700,height=400,left=150");
		windowName.focus();
	}
	//added by Bikash for INVP 2.8
	
	prepareSpecimen = function(e){			
		//KM-5788 and 5789
		var pgRight = document.uiinclude.pgright.value;
		if (! f_check_perm(pgRight,'N') )
		{
			return false;
		}
		windowName=window.open("prepareSpecimen.jsp?","Information","toolbar=no,scrollbars=no,resizable=yes,menubar=no,status=yes,width=700,height=400,left=150");
		windowName.focus();
	}
	
	//added by Bikash for INVP 2.8
	viewCart = function(e){		
		//KM-5788 and 5789
		var pgRight = document.uiinclude.pgright.value;
		if (! f_check_perm(pgRight,'N') )
		{
			return false;
		}
		windowName=window.open("preparationCart.jsp?","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=700,height=400,left=150");
		windowName.focus();
	}

	//
	function formSpecimenNavigation(navLinks,viewCartEnable,addCartEnable,manageCartEnable,prepareEnable,uid){
	if (viewCartEnable) {
		navLinks=navLinks+
		"<td >&nbsp;<A href='javascript:void(0);' id='"+uid+"_view' title = '"+varViewCart/*View Cart*****/+"'><img type='image' src='../jsp/images/cart.jpg' border='0'/></A></td>";
		}
	if (addCartEnable) {
		navLinks=navLinks+
		"<td >&nbsp;<A href='javascript:void(0);' id='"+uid+"_add'>"+AddPrep_Cart/*Add to Preparation Cart*****/+"</A></td>";
		}
	if (manageCartEnable) {
		navLinks=navLinks+
		"<td >&nbsp;<A href='javascript:void(0);' id='"+uid+"_manage'>"+Manage_PreparationCart/*Manage Preparation Cart*****/+"</A></td>";
	}	
	if (prepareEnable) {
		navLinks=navLinks+
		"<td >&nbsp;<A href='javascript:void(0);' id='"+uid+"_prepare'>"+Prepare_Samples/*Prepare Samples*****/+"</A></td>";
	}
	return navLinks;
	}