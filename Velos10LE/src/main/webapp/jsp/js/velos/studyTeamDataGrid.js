// IE does not support Array indexOf(); handle it here
if (Array.prototype.indexOf === undefined) {
	Array.prototype.indexOf = function(myObj, startIndex) {
		for (var iX = (startIndex || 0), jX = this.length; iX < jX; iX++) {
			if (this[iX] === myObj) { return iX; }
		}
		return -1;
	}
}

var gridIndexInstanceIndexMap = {};
var instanceIndexSelOrgMap = {};
var myDataTables = {};
var instanceUserIds = {};
var instanceUserNames = {};
var roleIdList = [];
var roleList = [];
var deletedTeamMembers = [];
var studyDm;

var isIe;
var selOrgId;
var incomingUrlParams = null;

var ErrGetResp_CnctVelos=M_ErrGetResp_CnctVelos;
var CommErr_CnctVelos=M_CommErr_CnctVelos;
var LoadingPlsWait=L_LoadingPlsWait;

//IE does not support Array indexOf(); handle it here
if (Array.prototype.indexOf === undefined) {
	Array.prototype.indexOf = function(myObj, startIndex) {
		for (var iX = (startIndex || 0), jX = this.length; iX < jX; iX++) {
			if (this[iX] === myObj) { return iX; }
		}
		return -1;
	}
}

// Define VELOS.studyTeamDataGrid object. "VELOS" is a namespace so that the object can be called globally
VELOS.studyTeamDataGrid = function (url, args) {
	isIe = jQuery.browser.msie;

	this.url = url;  // save it in this.url; later it will be sent in startRequest()
	incomingUrlParams = args.urlParams; // this will be sent to updateEvtVisits.jsp
	if (incomingUrlParams) {
		incomingUrlParams = incomingUrlParams.replace("'", "\"");
	}
	
	showPanel(); // Show the wait panel; this will be hidden later
	
	// Define handleSuccess() of VELOS.studyTeamDataGrid object. This will be added to the callback object to be
	// used when the call is processed successfully.
	this.handleSuccess = function(o) {
		/* Response Object Fields:
		 * o.tId, o.status, o.statusText, o.getResponseHeader[],
		 * o.getAllResponseHeaders, o.responseText, o.responseXML, o.argument
		 */
        this.dataTable = args.dataTable ? args.dataTable : 'serverpagination'; // name of the div to hold studyTeamDataGrid
		var respJ = null; // response in JSON format
		try {
			respJ= $J.parse(o.responseText);
		
		} catch(e) {
			alert(ErrGetResp_CnctVelos);/*alert('Error getting response from server. Please contact Velos Support.');*****/
			return;
		}
		if (respJ.error < 0) {
			var paramArray = [respJ.errorMsg];
		    alert(getLocalizedMessageString("L_Error_Msg", paramArray));/*alert('Error Msg: '+respJ.errorMsg);*****/
			return;
		}
	
		var myFieldArray = [];
		var myColumnDefs = [];
		var visitNos=0;
		if (respJ.colArray) {
			myColumnDefs = respJ.colArray;
			for (var iX=0; iX<myColumnDefs.length; iX++) {
				if (myColumnDefs[iX].key == 'delete'){
					myColumnDefs[iX].width = 50;
				} else if (myColumnDefs[iX].key == 'user'){
					myColumnDefs[iX].width = 300;
				} else if (myColumnDefs[iX].key == 'role'){
					myColumnDefs[iX].width = 200;
				} else if (myColumnDefs[iX].key == 'userId' || myColumnDefs[iX].key == 'roleId'){
					myColumnDefs[iX].width = 50;
				} else {
					myColumnDefs[iX].width = 200;
				}
				myColumnDefs[iX].resizeable = true;
				myColumnDefs[iX].sortable = false;

				if (myColumnDefs[iX].type!=undefined){
					var type= myColumnDefs[iX].type;
				 	var typeInfo = VELOS.studyTeamDataGrid.types[type];
				 	if (typeInfo.parser!=undefined){
				 		myColumnDefs[iX].parser=typeInfo.parser;
				 	}
				 	if (myColumnDefs[iX].formatter == undefined){
				 		if (typeInfo.formatter!=undefined)
				 			myColumnDefs[iX].formatter=typeInfo.formatter;
				 	}
				 	if (typeInfo.stringer!=undefined){
				 		myColumnDefs[iX].stringer=typeInfo.stringer;
					}
				 	if (typeInfo.editorOptions!=undefined){
				 		myColumnDefs[iX].editor=new YAHOO.widget.TextboxCellEditor(typeInfo.editorOptions);
					}
				}
				if (myColumnDefs[iX].key == 'user') {
					myColumnDefs[iX].editor = new YAHOO.widget.DropdownCellEditor({dropdownOptions:[],disableBtns:true});
				}

				if (myColumnDefs[iX].key == 'role') {
					myColumnDefs[iX].editor = new YAHOO.widget.DropdownCellEditor({dropdownOptions:respJ.roleList,disableBtns:true});
				}

				var fieldElem = [];
				fieldElem['key'] = myColumnDefs[iX].key; 
				myFieldArray.push(fieldElem);
			}
		}

		studyDm = respJ.studyDm;
		roleIdList = respJ.roleIdList;
		roleList = respJ.roleList;

		var myDataSource = new YAHOO.util.DataSource(respJ.dataArray);
		myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
		myDataSource.responseSchema = {
			fields: myFieldArray
		};
		
		//alert(this.dataTable +' '+ gridIndex);
		var maxWidth = 800;
		var myDataTable = new YAHOO.widget.DataTable(
			this.dataTable,
			myColumnDefs, myDataSource,
			{
				maxWidth:"98%",
				scrollable:true 
			}
		);
		myDataTable.render('#'+this.dataTable);

		var gridIndex = args.gridIndex;
		var selOrgId = respJ.selOrgId;

		var dataTableName = ''+myDataTable;
		var dtInstance = 'DataTable instance yui-dt';			
		var instanceIndex = ""+dataTableName.substring(dtInstance.length);
		
		gridIndexInstanceIndexMap[gridIndex] = instanceIndex;
		instanceIndexSelOrgMap[instanceIndex] = selOrgId;
		
		instanceUserIds[instanceIndex] = respJ.userIds;
		instanceUserNames[instanceIndex] = respJ.userNames; 

		myDataTables[instanceIndex] = myDataTable;
		$("sTeamGridData"+selOrgId).value = htmlEncode(yuiDataTabletoJSON(myDataTable));
		
		var calcHeight = VELOS.studyTeamDataGrid.calcDataTableHeight(respJ.dataArray.length);
		myDataTable.set("height", calcHeight+"px");

	 //for (var indx=0; indx < myDataTable.length; indx++){
		 myDataTable.subscribe("renderEvent", function (oArgs) {
			var dataTableName = ''+this;
			var dtInstance = 'DataTable instance yui-dt';			
			var instanceIndex = dataTableName.substring(dtInstance.length);

			var myDT = myDataTables[instanceIndex];

			var nameHeaders = $D.getElementsByClassName('yui-dt-col-user', 'th');
			var el = new YAHOO.util.Element(nameHeaders[0]);
			var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
			//var nameTH = parent.getElementsByClassName('yui-dt-col-user', 'th')[0];
			//nameTH.colSpan = "2";
			
			var teamIdCells = $D.getElementsByClassName('yui-dt-col-teamId', 'td', myDT.getTableEl());
			for (var iX=0; iX<teamIdCells.length; iX++) {
				var el = new YAHOO.util.Element(teamIdCells[iX]);			 
				var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
				var delButtonTd = parent.getElementsByClassName('yui-dt-col-delete', 'td')[0];
				var delButtonEl = new YAHOO.util.Element(delButtonTd);
				var delButton = delButtonEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
				var displayStyle ="";
				/*if (!f_check_perm_noAlert(pgRight,'E')){
					displayStyle='style="display:none" ';
				}*/
				delButtonEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML
				= '<div align="center"><img class="headerImage" src="../images/delete.png" border="0"'
				+ ' name="delete_e" id="delete_e" '+displayStyle+'/></div> ';
			}
			
			//myDataTables[instanceIndex] = myDT;
		  });
		 
		 myDataTable.subscribe('cellClickEvent',function(oArgs) {
			var target = oArgs.target;
			var column = this.getColumn(target);
			var oRecord = this.getRecord(target);
			var teamId = parseInt(oRecord.getData('teamId'));
			
			var dataTableName = ''+this;
			var dtInstance = 'DataTable instance yui-dt';			
			var instanceIndex = dataTableName.substring(dtInstance.length);
			var myDT = myDataTables[instanceIndex]; 
			
			if (column.key=="phone" || column.key=="email" ) return false;
			
			var ev = oArgs.event;

			switch(column.action){
				case 'delete':
					/*if ((teamId > 0 && false == f_check_perm_noAlert(pgRight,'E')) || (teamId == 0 && false == f_check_perm_noAlert(pgRight,'N'))){//BUG#7543
						f_check_perm(pgRight,'E');
						return false;
					} else{*/
						var paramArray = [oRecord.getData('user')];
						
						var userId = oRecord.getData('userId');
						if (userId == studyDm){
							alert(getLocalizedMessageString("M_UsrDataMgr_CntRemFrmTeam",paramArray));/*alert(username + " is identified as Data Manager in <%=LC.L_Study%> Summary. This user can not be removed from the <%=LC.L_Study%> Team.");*****/
							return;
						}

						var confirmMessage = getLocalizedMessageString("M_Del_UserFromStudyTeam",paramArray)
						if (confirm(confirmMessage)) {
							var teamId = oRecord.getData("teamId");
							var selOrgId = oRecord.getData("orgId");

							myDT.deleteRow(target);
							oRecord.setData(column.key, true);
							delCount ++;
							
							var count = (myDT.getRecordSet()).getLength();
							var calcHeight = VELOS.studyTeamDataGrid.calcDataTableHeight(count);
							myDT.set("height", calcHeight+"px");
							
							if (teamId > 0){
								$("sTeamGridData"+selOrgId).value = htmlEncode(yuiDataTabletoJSON(this));
								
								deletedTeamMembers.push(teamId);
								$("sTeamPurged").value = deletedTeamMembers;
							} 
					    }
					//}
					return false;
					break;
				default:
					if (teamId == 0){ //New Record
						//if (f_check_perm(pgRight,'N')){
							if (column.key == 'user') {
								var usrNames = instanceUserNames[instanceIndex];
								column.editor.dropdownOptions=usrNames;
								if (!usrNames || usrNames.length <= 0){
									return;
								}
								column.editor.render();
							}
							if (column.key =='role'){
								console.log(respJ.roleList);
								column.editor.dropdownOptions=respJ.roleList;
								if (usrNames && usrNames.length > 0){
									column.editor.render();									
								}
							}
							// If no action is given, I try to edit it
							this.onEventShowCellEditor(oArgs);//Ak:Fixed Bug#6905
						//}
					}else{//Existing Record 
						//if (f_check_perm(pgRight,'E')){
							if (column.key == 'user') {
								return;
							}
							if (column.key =='role'){
								console.log(respJ.roleList);
								column.editor.dropdownOptions=respJ.roleList;
								column.editor.render();
							}
							// If no action is given, I try to edit it
							this.onEventShowCellEditor(oArgs);
						//}
					}
					return true;
					break;
			}
		 });

		 myDataTable.subscribe("editorBlurEvent", function(oArgs){
			var dataTableName = ''+this;
			var dtInstance = 'DataTable instance yui-dt';			
			var instanceIndex = dataTableName.substring(dtInstance.length);
			var myDT = myDataTables[instanceIndex]; 
			myDT.onEventSaveCellEditor(oArgs); 
		 });
		 
		 myDataTable.subscribe('editorSaveEvent',function(oArgs) {			
			var elCell = oArgs.editor.getTdEl();
			var oOldData = oArgs.oldData;
			var oNewData = oArgs.newData;			
			var ev = oArgs.event;
			var oRecord = this.getRecord(elCell);
			var column =  this.getColumn(elCell);
			
			var dataTableName = ''+this;
			var dtInstance = 'DataTable instance yui-dt';			
			var instanceIndex = dataTableName.substring(dtInstance.length);
			var myDT = myDataTables[instanceIndex]; 
			if (!myDT) return;

			if (column.key == 'user'){
				var usrIds = instanceUserIds[instanceIndex];
				var usrNames = instanceUserNames[instanceIndex];
				VELOS.studyTeamDataGrid.setDropDownId(myDT, elCell, usrIds, usrNames);				
			}
			
			if (column.key == 'role'){
				VELOS.studyTeamDataGrid.setDropDownId(myDT, elCell, roleIdList, roleList);				
			}

			var selOrgId = instanceIndexSelOrgMap[instanceIndex];
			if (!selOrgId) return;

			$("sTeamGridData"+selOrgId).value = htmlEncode(yuiDataTabletoJSON(this));

			return false;
		 });

		if (!$E.getListeners($('addRows'+gridIndex))) {
			 $E.addListener($('addRows'+gridIndex), 'click', VELOS.studyTeamDataGrid.addRows, false, true); 
		}
		myDataTable.focus();   

		hidePanel();	
	};
	this.handleFailure = function(o) {
		alert(CommErr_CnctVelos); /*alert(svrCommErr);Error Message displayed from local var*****/
		hidePanel();
	};
	this.cArgs = [];
	this.callback = {
		success:this.handleSuccess,
		failure:this.handleFailure,
		argument:this.cArgs
	};
	this.initConfigs(args);
}

VELOS.studyTeamDataGrid.types = {
	'inlineText':{
		editor:'autocomplete',
		editorOptions:{disableBtns:true},
		editorConfig:{
		    autocompleteConfig:{
		        alwaysShowList: true
		    }
		}
	},
	'varchar':{
		editor:'textbox',
		editorOptions:{disableBtns:true},
		formatter:'varchar'
	},
	integer: {
		formatter:'integer',
		parser:'number',
		// I am using my own RegularExpressionCellEditor, defined below
		editor:'regexp',
		// I'm only accepting digits (probably this data type should be called integer)
		editorOptions: {
			disableBtns:true,
			regExp:'^\\d{0,11}$'
			//validator: YAHOO.widget.DataTable.validateNumber
		}
	},
	number: {
		formatter:'number',
		parser:'number',
		// I am using my own RegularExpressionCellEditor, defined below
		editor:'regexp',
		// I'm only accepting digits (probably this data type should be called integer)
		editorOptions: {
			disableBtns:true,
			regExp:'^\\d{0,11}([.,]\\d{0,2})?$'
			//regExp:'^\\d{11}$',
			//validator: YAHOO.widget.DataTable.validateNumber
		}
	},
	// This is the default, do-nothing data type.
	// There is no need to parse, format or stringify anything and if an editor is needed, the textbox editor will have to do.
	string: {
		editor:'textbox'
	}
};
VELOS.studyTeamDataGrid.calcDataTableHeight = function(count){
	var maxHeight = 800;
	
	var calcHeight = count*((isIe==true)?25:25)+((isIe==true)?31:30);
	if (calcHeight > maxHeight) { calcHeight = maxHeight; }
	
	return calcHeight;
}

VELOS.studyTeamDataGrid.addRows = function(e){
	var btnId = e.target.id;
	var gridIndex = btnId.substring(('addRows').length);
	var instanceIndex = gridIndexInstanceIndexMap[gridIndex];
	var myDT = myDataTables[instanceIndex];

	var selOrgId = instanceIndexSelOrgMap[instanceIndex];
	if (!myDT || !selOrgId) return;
	
	var myArray = [];
	var dataJSON = {"teamId":0, "orgId": selOrgId, "delete":""};
	var record = YAHOO.widget.DataTable._cloneObject(dataJSON);
	record.row = 0;			
	myArray.push(record);
	myDT.addRows(myArray);
	
	var count = (myDT.getRecordSet()).getLength();
	var calcHeight = VELOS.studyTeamDataGrid.calcDataTableHeight(count);
	myDT.set("height", calcHeight+"px");
};

VELOS.studyTeamDataGrid.setDropDownId = function (myGrid,elCell,myIds,myTypes){			
	var oRecord = myGrid.getRecord(elCell);			
	var oColumn =  myGrid.getColumn(elCell);	

	for (var j=0; j<myIds.length; j++){
		if (myTypes[j] == oRecord.getData(oColumn.key)) break;
	}

	oRecord.setData(oColumn.key +'Id', myIds[j]);
	
	var parent = new YAHOO.util.Element($D.getAncestorByTagName(elCell, 'tr'));
	var idTd = parent.getElementsByClassName('yui-dt-col-'+oColumn.key+'Id', 'td')[0];

	var idEl = new YAHOO.util.Element(idTd);

	var Id = idEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
	idEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = myIds[j];
	
	if(oColumn.key == 'calendar'){
		oRecord.setData(oColumn.key +'Type', myCalendarTypes[j]);
		var idTd = parent.getElementsByClassName('yui-dt-col-'+oColumn.key+'Type', 'td')[0];

		var idEl = new YAHOO.util.Element(idTd);

		var Id = idEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
		idEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = myCalendarTypes[j];
	}
};

VELOS.studyTeamDataGrid.prototype.initConfigs = function(args) {
	if (!args) { return false; }
	if (args.constructor != Object) { return false; }
	if (args.urlParams) { this.urlParams = args.urlParams; }
	else { this.dataTable = 'serverpagination'; }
	if ((args.success) && (typeof args.success == 'function')) { 
		this.handleSuccess = args.success;
	}
	if ((args.failure) && (typeof args.failure == 'function')) {
		this.handleSuccess=args.success;
	}
	return true;
}

VELOS.studyTeamDataGrid.prototype.startRequest = function() {
	$C.asyncRequest(
		'POST',
		this.url,
		this.callback,
		this.urlParams
	);
}

VELOS.studyTeamDataGrid.prototype.render = function() {
	this.startRequest();
}

VELOS.studyTeamDataGrid.prototype.loadingPanel = function() {
	// Initialize the temporary Panel to display while waiting for external content to load
	if (!(VELOS.wait)) {
		VELOS.wait = 
			new YAHOO.widget.Panel("wait",  
				{ width:"240px", 
				  fixedcenter:true, 
				  close:false, 
				  draggable:false, 
				  zindex:4,
				  modal:true,
				  visible:false
				} 
			);

		VELOS.wait.setHeader(LoadingPlsWait);/*VELOS.wait.setHeader("Loading, please wait...");*****/
		VELOS.wait.setBody('<img class="asIsImage" src="../images/jpg/loading_pg.gif" />');
		VELOS.wait.render(document.body);
	}
}

VELOS.studyTeamDataGrid.prototype.showPanel = function () {
	if (!(VELOS.wait)) { loadingPanel(); }
	VELOS.wait.show();
}

VELOS.studyTeamDataGrid.prototype.hidePanel = function() {
	if (VELOS.wait) { VELOS.wait.hide(); }
}
