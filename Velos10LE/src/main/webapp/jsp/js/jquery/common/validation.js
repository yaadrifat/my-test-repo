
var toDisplayAlert=true;

function setMask(obj,vMask)
{

	obj.setAttribute('maskType',vMask);
}

//*****************************************************************
//Check whether string s is empty.
function isEmpty(s)
{
	if((fnTrimSpaces(s) == null) || (fnTrimSpaces(s).length == 0))
	{
		gErrFlag = "T";
		return true;
	}
	else
	{
		gErrFlag = "F";
		return false;
	}
}

var rgEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9])+$/;

function isEmail (s)
{
	return  rgEmail.test(s);
}

function checkEmail (theField)
{
    if (!(isEmpty(theField.value)))
		if (!(isEmail(theField.value)))
			return false;
		else return true;
}

//**************************************************************************
//Shows error messages on Status Bar/alert depending on var toDisplayAlert
//**************************************************************************

function fnShowErrorMessageasReq(message)
{
if(toDisplayAlert)
{
	if(message!='' && message!='Done' && message!='null')
	alert(message);

//	if (top.opener==null || top.opener=='undefined' || top.opener.closed)
//		top.close(); // incase the parent window is closed then close the window on which error is coming
}
else
{
	fnShowErrorMessageasReq(message);
}

}

//***********************************************************************************
//fnTrimSpaces trims leading and trailing spaces in 'strToTrim'
//***********************************************************************************

function fnTrimSpaces(strToTrim)
{
	strTemp = strToTrim;
	len=strTemp.length;
	i=0;
	while (i < len && strTemp.charAt(i)==" ")
		i=i+1;
	strTemp = strTemp.substring(i);
	len=strTemp.length;
	j = len-1;
	while (j>0 && strTemp.charAt(j)==" ")
		j = j-1;
	strTemp = strTemp.substring(0,j+1);
	return strTemp;
}

function passwordValidation(pw){
	value=pw.value;
	if( (value.length>0) && ( ((value.search(/[a-z]+/))==-1) || ((value.search(/[A-Z]+/))==-1) || ((value.search(/[0-9]+/))==-1) ||  ((value.search(/[$#<>@\\/\?]+/))==-1) )){
		return false;
	}
	return true;
}

function isAlphanumericUserPwd(obj, isSearch){
		var i;
		var	s;
		s = obj.value;
		s = fnTrimSpaces(s);
    if (!(isEmpty(s))){
		for (i = 0; i < s.length; i++){
		    // Check that current character is number or letter.
		    var c = s.charAt(i);
		    if (!(isAllValidCharPwd(c,isSearch))){
				gErrFlag = "F";
		    	return false;
			}else if(c == "'"){
		    	strTempBefore = s.substring(0,i);
		    	strTempAfter = s.substring(i+1,s.length);
		    	s = strTempBefore + '`' + strTempAfter;
		    	obj.value = s;
		    }
		}
		// All characters are numbers or letters.
		gErrFlag = "T";
		return true;
    }
		gErrFlag = "T";
    return true;
}

function isAllValidCharPwd (c,isSearch){

	if (!(isSearch)){
		if(c == '*' || c == '_' || c == '"' || c == '%'){
			gErrFlag = "F";
			return false;
		}else{
			gErrFlag = "T";
			return true;
		}
	}else{
		if(c == '"' || c == '%'){
			gErrFlag = "F";
			return false;
		}else{
			gErrFlag = "T";
			return true;
		}
	}
}


//***********************************************************************************
//fnShowCalendar opens the Calendar window with the current date set as default in it.
//****
//trackChange - Used to indicate whether track change is required or not
function fnShowCalendar(theDateObj,trackChange)
{

	var msgWindow = null;
//Commented by Rajeev to avoid the prompt msg twice if calendar is opened with invalid date.
//	fnValidateDate(theDateObj);
//End of Comment
	var strDate = fnTrimSpaces(theDateObj.value);
	if( strDate == "MM/DD/YYYY" || strDate == "" || !fnValidateDateWithoutMessage(theDateObj) || parseInt(Nth_Tab(theDateObj.value,3,'/')) < 1900 || parseInt(Nth_Tab(theDateObj.value,3,'/')) > 2500 || parseInt(Nth_Tab(theDateObj.value,3,'-')) < 1900 || parseInt(Nth_Tab(theDateObj.value,3,'-')) > 2500)
	{
//		strDate = theDateObj.value;
		strDate = fnGetFullCurrentDate();
	}
	var callUrl = window.location.href;
	var pos = parseInt(callUrl.indexOf(".html"));
	var callUrlUpper = callUrl.toUpperCase();
	var utilPos = parseInt(callUrlUpper.toUpperCase().indexOf("/UTILITY"));
	var url;
	if(utilPos != -1)
		url = contextpath+"/Doc/HTML/en/calendar.html?"+strDate;
	else if (pos == -1)
		url = contextpath+"/Doc/HTML/en/calendar.html?"+strDate;
	else
		url = contextpath+"/Doc/HTML/en/calendar.html?"+strDate;
	//msgWindow=window.open(url,'DateWindow','alwaysLowered=0,alwaysRaised=0,height=270,width=300,status=no, titlebar = no,toolbar=no,menubar=no,resizable=no,location=no,dependent=yes');
	var sFeatures="dialogHeight:325px;dialogWidth:340px; status:0; help:0";
	strDate = window.showModalDialog(url, "",sFeatures);

	if( strDate != "")
	{

		theDateObj.value=strDate;

//trackChange variable is also sent in the function
		if (fnShowCalendar.arguments.length==1)
			Flag();
		if (window.document.forms[0].hChange)
			window.document.forms[0].hChange.value = true;
	}else if (fnShowCalendar.arguments.length==1)
		fnClearChange();
		gErrFlag = 'T';
      fnShowErrorMessageasReq("");
	return true;
}

//Checks for the following valid date formats: MM/DD/YY   MM/DD/YYYY   MM-DD-YY   MM-DD-YYYY
//Also separates date into month, day, and year variables
//ST 09/25/2001
function fnValidateDateWithoutMessage(dateField)
{
	var dateStr = dateField.value;

	if (isEmpty(dateStr))  //No date specified
		return true;

      // To require a 4 digit year entry with / or -  as seperator, use this line instead:
      var datePat = /^(\d{1,2})(\/|-)(\d{1,2})\2(\d{4})$/;

      // To require a 2 or 4 digit year entry with / or -  as seperator, use this line instead:
      //var datePat = /^(\d{1,2})(\/|-)(\d{1,2})\2(\d{2}|\d{4})$/;

      // To require a 4 digit year entry with / as seperator, use this line instead:
     // var datePat = /^(\d{1,2})(\/)(\d{1,2})\2(\d{4})$/;


      var matchArray = dateStr.match(datePat); // is the format ok?
      if (matchArray == null)
      {
              return false;
      }

      month = matchArray[1]; // parse date into variables
      day = matchArray[3];
      year = matchArray[4];
      if (month < 1 || month > 12)
      { // check month range
              return false;
      }

      if (day < 1 || day > 31)
      {
              return false;
      }

      if ((month==4 || month==6 || month==9 || month==11) && day==31)
      {
              return false;
      }

      if (month == 2)
      { // check for february 29th
              var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
              if (day>29 || (day==29 && !isleap))
              {
                      return false;
              }
      }
      if (year.length == 4)
      {
          if (parseInt(year)==0  || parseInt(year)<1900 || parseInt(year)>2500)
          {
//		var vErrorMsg = "Invalid date";
//		fnShowErrorMessageasReq(vErrorMsg);
	        return false;
          }
	}
      return true;  // date is valid
}

//***********************************************************************************
//fnGetFullCurrentDate sets current date set as default in the Calendar.
//***********************************************************************************

function fnGetFullCurrentDate()
{
	var strFinalDate;
	var d = new Date();

	//Get all the attributes
	var strMonth = d.getMonth() +1;

	if( strMonth < 10 )
		strMonth = "0" + strMonth ;

	var strDay   = d.getDate();
	if( strDay < 10 )
		strDay = "0" + strDay;

	var strYear  = d.getFullYear(); //This will return full year

	var strFinalDate = strMonth + "/" + strDay + "/" + strYear;
	return strFinalDate;
}

//***************************************************************************************
//fnExpired check the given date is valid or not (future)  
//***************************************************************************************
function expired( month, year ) {

	var now = new Date();
	var expiresIn = new Date(year,month,0,0,0);
	//expiresIn.setMonth(expiresIn.getMonth());
	if( now.getTime() < expiresIn.getTime() ) return false;
	return true;
}

