var APPMODE_SAVE ="SAVE";
var APPMODE_ADD="ADD";
var APPMODE_DELETE="DELETE";
var APPMODE_READ="READ";
var errMsgNoRights ='Error: User does not have rights to ';
var $j=jQuery;
$j.ajaxSetup({ cache: false });



function checkAppRights(rights,mode){
    if(mode == APPMODE_READ && rights.indexOf("R") == -1 ){
			alert(errMsgNoRights + mode);
			return false;      		
    }else if(mode == APPMODE_ADD && rights.indexOf("A") == -1 ){
			alert(errMsgNoRights + mode);
			return false;      		
    }else if(mode == APPMODE_SAVE && rights.indexOf("M") == -1 ){
			alert(errMsgNoRights + mode);
			return false;      		
    }else if(mode == APPMODE_DELETE && rights.indexOf("D") == -1 ){
			alert(errMsgNoRights + mode);
			return false;      		
    }
   
    return true;
}


//JavaScript Document
//code inside this will happen after page is loaded  
/*$(document).ready(function(){     
// find element with ID of "target" and put file contents into it
	//alert(" script.js ready");
	
	
	$('#header').load('html/inc_header.html');
	$('#breadcrumb').load('html/inc_breadcrumb.html');
	
	$('#footer').load('cb_inc_footer.jsp');
	
	

});
*/
/**** For showing add widget modal  *********/

function showAddWidgetModal(url)
{
	var position = $j("#widgetId").position();
	 $j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        success: function (result, status, error){
	        	
	        	$j("#modelPopup1").html(result);
	        	$j("#modelPopup1").dialog(
	     			   {autoOpen: true,
	     				title: 'Add Widgets',
	     				closeText: '',
						closeOnEscape: false ,
	     				position: [(position.left-300),(position.top+25)],
	    				modal: true, width:411, height:200,
	     				close: function() {
	     					//$j(".ui-dialog-content").html("");
	     					//jQuery("#subeditpop").attr("id","subeditpop_old");
	     	        		jQuery("#modelPopup1").dialog("destroy");
	     			    }
	     			   }
	     			  );        	
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }

		});
}
		
/*****  Add widget functions            ******/

function exportData(tableId){
	// alert("export -> " + tableId);
	 $j('#'+tableId).table2CSV({
		separator : '\t'
		});
}

function showData(data){
	//alert("showData")
	//alert("data " + data);
	//window.location='data:application/vnd.ms-excel;charset=utf8,' + encodeURIComponent(data) + ', #Content-Disposition:filename=export.xls,';	
	
	window.location='data:application/vnd.ms-excel;charset=utf8;Content-Type=application/vnd.ms-excel;Content-Disposition:attachment;filename=export.xls,' + encodeURIComponent(data) ;
 return true;
}

function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}

function clearFields(){
	$j('#username').val("");
	$j('#password').val("");
	$j("#errormessagetd").html("");
}
function updateBreadcrumb(dispname,link ){
	//alert("update bread");
	if( $j('#breadcrumblink').text().indexOf( dispname ) == -1 ){

		$j('#breadcrumblink').append("&nbsp;&gt;&gt; <a  href='#' onclick='"+link+"';>" +dispname +"</a>");
	}
}
 
function changeBreadcrumb(pagename){
	var breadcrumbVal = '&nbsp;&nbsp;<a href="#" onclick="javascript:showHighlight(\'mnuHome\');checkLogin();" >Home </a>';
	if(pagename=="volunteer"){
		showHighlight('mnuVol');
		breadcrumbVal += '&nbsp;&nbsp;&gt;&gt;&nbsp;<a href="#" >New Volunteer</a>&nbsp;';
		$j('#breadcrumblink').html(breadcrumbVal);
	}else if(pagename=="respondent"){
		showHighlight('mnuResp');
		breadcrumbVal += '&nbsp;&nbsp;&gt;&gt;&nbsp;<a href="#" onclick="loadPage(\'getRespondents\');">Respondents</a>&nbsp;';
		breadcrumbVal += '&nbsp;&nbsp;&gt;&gt;&nbsp;<a href="#" >Edit Respondent</a>&nbsp;';
		$j('#breadcrumblink').html(breadcrumbVal);
	}else if(pagename=="editVolunteer"){
		showHighlight('mnuVol');
		breadcrumbVal += '&nbsp;&nbsp;&gt;&gt;&nbsp;<a href="#" onclick="loadPage(\'getVolunteerlst\');">Volunteers</a>&nbsp;';
		breadcrumbVal += '&nbsp;&nbsp;&gt;&gt;&nbsp;<a href="#" >Edit Volunteer</a>&nbsp;';
		$j('#breadcrumblink').html(breadcrumbVal);
	}else if(pagename=="Volunteers"){
		showHighlight('mnuVol');
		breadcrumbVal += '&nbsp;&nbsp;&gt;&gt;&nbsp;<a href="#" onclick="loadPage(\'getVolunteerlst\');">Volunteers</a>&nbsp;';		
		$j('#breadcrumblink').html(breadcrumbVal);
	}
	
	
}



function updateDisplayName(type, name){
	//alert("update user sel");
	$j("#pageType").val(type);
	$j("#pageName").val(name);
	var userSelection = type + " : " + name;
	$j('#userSelection').html(userSelection);
}


function getHome(){
	$j('#header').load('cb_inc_blankheader.jsp');
	
	$j('#header').removeClass('headerStyle').addClass('blankheader');
	$j('#nav').css('height','32px');
	$j('#nav').load('cb_inc_nav.jsp');
	$j('#breadcrumb').load('html/inc_breadcrumb.html');
	$j('#footer').load('cb_inc_footer.jsp');
	$j('#menu').removeClass('hidden').addClass('visible');
	
	$j('#breadcrumb').removeClass('hidden').addClass('visible');
	$j('#accordion').removeClass('hidden').addClass('visible');
	$j('#div_accordion').removeClass('col_0').addClass('col_15');
	$j('#div_main').removeClass('col_100').addClass('col_85');
	
	loadPage('home');
	$j('.jclock').jclock();
}
function forgotpassword(){
	$j("#errormesssagetd").html("");
	var data = "forgotuserName="+$j("#forgotuserName").val();+"&email="+$j("#email").val();
	//alert("context path " + contextpath);
	$j.ajax({
		type: "POST",
		url : "getforgotpassword",
		data : $j("#forgotform").serialize(),
		success : function(data) {
			$j("#errormesssagetd").html(data.errorMessage);
		},
		error:function() { 
			alert("Error ");
		}
		
	});
	return false;
}/*
function checkLogin(){
	//alert("check login");
	if($('#username').val()!= "" && $('#password').val()!=""){
		//alert(" inside if");
		var errorMessage = "";
		var navHtml="";
		var userNavRight = "";
		var errorMessage = validateUserDetails("getLoginDetails?userName="+$('#username').val()+"&password="+$('#password').val());
		var userNavDetails = readXml1("navigation.xml");
		if(errorMessage != "" && errorMessage!=null)
		{
		alert(errorMessage);
		}
		if(errorMessage == "" || errorMessage==null){  
			if($('#username').val()=="pfuser")
			{
				userNavRight = 2;
			}else if($('#username').val()=="cdruser")
			{
				userNavRight = 4;
			}else if($('#username').val()=="reviewuser")
			{
				userNavRight = 16;
			}else if($('#username').val()=="dataentryuser")
			{
				userNavRight = 8;
			}	
			navHtml += '<ul id="accordion" >';			
			$(userNavDetails).find("heading").each(function() {
				if((parseInt($(this).find("userright").text())& userNavRight) > 0){
					navHtml += '<li><a href="#"><span class="ui-icon ui-icon-triangle-1-e"></span>'+$(this).find("headingname").text()+'</a><ul>';
					$(this).find("nav").each(function(){
						navHtml += '<li><a href="#" onclick="javascript:showHighlight(\''+$(this).find("link").text()+'\');loadPage(\''+$(this).find("link").text()+'\')">'+$(this).find("navname").text()+'</a></li>';
					});
					navHtml += '</ul></li>';
				}
			});
			navHtml += "</ul>";
			
			
			$('#header').load('cb_inc_blankheader.jsp');
	
			$('#header').removeClass('headerStyle').addClass('blankheader');
			$('#nav').css('height','32px');
			$('#nav').load('cb_inc_nav.jsp');
			$( "button,a", "#addWidgetTable" ).button();			 
			$('#breadcrumb').load('html/inc_breadcrumb.html');
			$('#menu').removeClass('hidden').addClass('visible');
			$('#footer').load('cb_inc_footer.jsp');
			
			$('#breadcrumb').removeClass('hidden').addClass('visible');
			$('#leftNav').html(navHtml);	
			$( "input:submit, a, button", "#accordion" ).button();
			//$('#accordion').removeClass('hidden').addClass('visible');
			//$( "#accordion" ).accordion();
			//$( "#accordion" ).removeClass('ui-widget ui-helper-reset');			
			$('#div_accordion').removeClass('col_0').addClass('col_15');
			$('#accordion ul').hide();
			$('#accordion li a ').click(

				    function() {
					    $(this).next().slideToggle('normal');               
					    $(this).find(".ui-icon").each(function(){
					    	if($(this).is(".ui-icon-triangle-1-s")){
					    		$(this).removeClass("ui-icon-triangle-1-s").addClass("ui-icon-triangle-1-e");
					    	}
					    	else if($(this).is(".ui-icon-triangle-1-e")){
					    		$(this).removeClass("ui-icon-triangle-1-e").addClass("ui-icon-triangle-1-s");
					    	}
					    });					   

				    });
			
			
			
	$('#div_main').removeClass('col_100').addClass('col_80');
		//	loadPage('home');
			
			//$('#main').html(" ");
			
			//$('#footer').load('html/inc_footer.html');
			//alert("wait");
		//	 $('.jclock').jclock();
		}else{
			$("#errormessagetd").html(errorMessage);
			return false;
		}	
	}
	 $('#fimg').removeClass('hidden').addClass('visible');
	return true;
	//alert(" end of check ");
}
 */
function validateUserDetails(url){
	//alert("validateUserDetails ");
	var errorMessage = "";
	 $j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        success: function (result){
	        	//alert(result);
	        	//alert(result.errorMessage);
	        	errorMessage = result.errorMessage;
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }

		});
	 return errorMessage;
}

/*this method is for check value exist in database or not*/
function validateDbData(url){
	if(url.indexOf("null")==-1){
		var dbValue;
		$j.ajax({
		        type: "POST",
		        url: url,
		        async:false,
		        success: function (result){
			       dbValue = result.dbValue;
		        },
		        error: function (request, status, error) {
		        	alert("Error " + error);
		            alert(request.responseText);
		        }
	
			});
		 return dbValue;
	}else{
		return true;
	}
}

function validateDbAdditionalData(url){
	var flag = true;
	if(url.indexOf("null")==-1){
		var dbValue;
		$j.ajax({
		        type: "POST",
		        url: url,
		        async:false,
		        success: function (result){
			       dbValue = result.dbValue;
			       if(dbValue == true){
			    	   flag = false;
			       }
		        },
		        error: function (request, status, error) {
		        	alert("Error " + error);
		            alert(request.responseText);
		        }
	
			});
		 return flag;
	}else{
		return flag;
	}
}


function validateValues(id1,id2){
	var flag = true;
	var val1 = $j("#"+id1).val();
	var val2 = $j("#"+id2).val();
	if(val1==val2){
		flag = false;
	}
	return flag;
}
/*-----*/

function showPage(frmName){
	var pageName = frmName;
	$j('.ui-dialog').html("");
	$j('.ui-datepicker').html("");
	$j('.tabledisplay').html("");
	$j('#main').html("");
	$j('.tabledisplay').html("");
	$j('#main').load(pageName);
}

function showModal(title,url,height,width)
{
	$j('.progress-indicator').css( 'display', 'block' );
	if(height==null || height==""){
		height = 650;
	}
	if(width==null || width==""){
		width = 750;
	}
var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	
	$j("#modelPopup1").html(progressMsg);
		$j("#modelPopup1").dialog(
				   {autoOpen: false,
					title: title  ,
					resizable: true,
					closeText: '',
					closeOnEscape: false ,
					modal: true, width : width, height : height,
					close: function() {
						//$(".ui-dialog-content").html("");
						//jQuery("#subeditpop").attr("id","subeditpop_old");
		      		jQuery("#modelPopup1").dialog("destroy");
				    }
				   }
				  ); 
		$j("#modelPopup1").dialog("open");
	$j.ajax({
        type: "POST",
        url: url,
       // async:false,
        success: function (result, status, error){
	       	$j("#modelPopup1").html(result);
        	       	
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	$j('.progress-indicator').css( 'display', 'none' );
}


function showModalOne(title,url,height,width)
{
	$j('.progress-indicator').css( 'display', 'block' );
	if(height==null || height==""){
		height = 650;
	}
	if(width==null || width==""){
		width = 750;
	}
var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	
	$j("#modelPopup2").html(progressMsg);
		$j("#modelPopup2").dialog(
				   {autoOpen: false,
					title: title  ,
					resizable: true,
					closeText: '',
					closeOnEscape: false ,
					modal: true, width : width, height : height,
					close: function() {
						//$(".ui-dialog-content").html("");
						//jQuery("#subeditpop").attr("id","subeditpop_old");
		      		jQuery("#modelPopup2").dialog("destroy");
				    }
				   }
				  ); 
		$j("#modelPopup2").dialog("open");
	$j.ajax({
        type: "POST",
        url: url,
       // async:false,
        success: function (result, status, error){
	       	$j("#modelPopup2").html(result);
        	       	
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	$j('.progress-indicator').css( 'display', 'none' );
}


function closeModals(divname){
	 $j("#"+divname).dialog("close");
	 $j("#"+divname).dialog("destroy");
	 $j("#"+divname).html("");
}

function showModals(title,url,height,width,divname){
	
	$j('.progress-indicator').css( 'display', 'block' );
	if(height==null || height==""){
		height = 650;
	}
	if(width==null || width==""){
		width = 750;
	}
	var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img class=\"asIsImage_progbar\" src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
		
		$j("#"+divname).html(progressMsg);
			$j("#"+divname).dialog(
					   {autoOpen: false,
						title: title,
						resizable: true,
						closeText: '',
						closeOnEscape: false ,
						modal: true, 
						width : width,
						height : height
					   }
					  );
			$j("#"+divname).dialog("open");
			$j.ajax({
		        type: "POST",
		        url: url,
		       // async:false,
		        success: function (result, status, error){
			       	$j("#"+divname).html(result);
		        	       	
		        },
		        error: function (request, status, error) {
		        	alert("Error " + error);
		            alert(request.responseText);
		        }

			});
			$j('.progress-indicator').css( 'display', 'none' );	
	}

	$j(function() {
		$j( "#modelPopup1" ).resizable({
			maxHeight: $j(window).height(),
		      maxWidth: $j(window).width(),
		      minHeight: 650,
		      minWidth: 750
		});
		$j( "#dialogForCRI" ).resizable({
			maxHeight: $j(window).height(),
		      maxWidth: $j(window).width(),
		      minHeight: 650,
		      minWidth: 750
		});
	});


function maximizeScreen(id)
{
	$j('.progress-indicator').css( 'display', 'block' );
	var result = $j("#"+id).html();
	var str="<form>";
	var str1="</form>";
	var res = str + result + str1;
	$j("#modelPopup1").html(res);
	$j("#modelPopup1").dialog(
     			   {autoOpen: true,
     				resizable: false,
     				closeText: '',
					closeOnEscape: false ,
     				modal: true,width : 750, height : 650,
     				close: function() {
     					//$(".ui-dialog-content").html("");
     					//jQuery("#subeditpop").attr("id","subeditpop_old");
     	        		jQuery("#modelPopup1").dialog("destroy");
     			    }
     			   }
     			  );        	
   $j('.progress-indicator').css( 'display', 'none' );
}
/*
function loadPage(url){  
	   // alert(" url " + url);
	$('.progress-indicator').css( 'display', 'block' );
	 $.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        success: function (result){
	        	//alert("test" + result);
	        	//showPage(pageName);
	        	//$(".ui-dialog-content").html("");
	        	$('.ui-dialog').html("");
	        	//$('.ui-datepicker').html("");
	        	$('.tabledisplay').remove();
	        	$('#main').html(result);
	        	valid8();
	        	
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }

		});
		$('.progress-indicator').css( 'display', 'none' );
}*/
// save Modal form

function modalFormSubmit(url,formId)
{
	$j('.progress-indicator').css( 'display', 'block' );
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data : $j("#"+formId).serialize(),
        success: function (result){
        	$j('.ui-datepicker').html("");
        	//$('.tabledisplay').html("");
            var $response=$j(result);
            //query the jq object for the values
            var errorpage = $response.find('#errorForm').html();
           // alert(oneval);
           // var subval = $response.find('#sub').text();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
            	$j("#modelPopup1").html(result);
            }
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	$j('.progress-indicator').css( 'display', 'none' );
}

//Load GET Request by Mohiuddin

function loadPageByGetRequset(url,divname){  
	   // alert(" url " + url); 
	$j('.progress-indicator').css( 'display', 'block' );
	$j.ajax({
	        type: "GET",
	        url: url,
	        async:false,
	        success: function (result){
	        	//alert("test" + result);
	        	//showPage(pageName);
	        	//$(".ui-dialog-content").html("");
		    	 $j('.ui-datepicker').html("");
		     	//$('.tabledisplay').html("");
		         var $response=$j(result);
		         //query the jq object for the values
		         var errorpage = $response.find('#errorForm').html();
		       // alert(errorpage);
		        // var subval = $response.find('#sub').text();
		         if(errorpage != null &&  errorpage != "" ){
		         	$j("#main").html(result);
		         }else{
		         	$j("#"+divname).html(result);
		       }
	        	
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }

		});
 	$j('.progress-indicator').css( 'display', 'none' );
}

//Load Post Form by Mohiuddin
function loadDivWithFormSubmit(url,divname,formId){
	$j('#progress-indicator').css( 'display', 'block' );  
   var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	
	$j("#progress-indicator").html(progressMsg);
		$j("#progress-indicator").dialog(
				   {autoOpen: false,
					resizable: false,
					closeText: '',
					closeOnEscape: false ,
					modal: true, width : 300, height : 100,
					close: function() {
						//$(".ui-dialog-content").html("");
						//jQuery("#subeditpop").attr("id","subeditpop_old");
		      		jQuery("#progress-indicator").dialog("destroy");
				    }
				   }
				  ); 
	$j("#progress-indicator").dialog().parents(".ui-dialog").find(".ui-dialog-titlebar").remove();
	$j("#progress-indicator").dialog("open");
	if($j("#"+formId).valid()){
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data : $j("#"+formId).serialize(),
        success: function (result){
        	$j('.ui-datepicker').html("");
        	//$('.tabledisplay').html("");
            var $response=$j(result);
            //query the jq object for the values
            var errorpage = $response.find('#errorForm').html();
           // alert(oneval);
           // var subval = $response.find('#sub').text();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
            	$j("#"+divname).html(result);
            }
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	}
	jQuery("#progress-indicator").dialog("destroy");
	$j('#progress-indicator').css( 'display', 'none' );	

}

//Load Particular div
function loaddiv(url,divname){  
	//$j('.progress-indicator').css( 'display', 'block' );
	 $j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        success: function (result){
	        	//$('.ui-dialog').html("");
	        	//$('.ui-datepicker').html("");
	        	var $response=$j(result);
	        	var errorpage = $response.find('#errorForm').html();
	        	if(errorpage != null &&  errorpage != "" ){
	            	$j('#main').html(result);
	            }else{
	            	$j("#"+divname).replaceWith($j('#'+divname, $j(result)));
	            }
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }

		});
		//$j('.progress-indicator').css( 'display', 'none' );
}
//load more div
function loadmultiplediv(url,divname){ 
	$j('.progress-indicator').css( 'display', 'block' );
	var divnamearr = divname.split(",");
	var newdivname = "";
	 $j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        success: function (result){
	        	//$('.ui-dialog').html("");
	        	//$('.ui-datepicker').html("");
	        	var $response=$j(result);
	        	var errorpage = $response.find('#errorForm').html();
	        	if(errorpage != null &&  errorpage != "" ){
	            	$j('#main').html(result);
	            }else{ 
		        	for(i=0;i<divnamearr.length;i++) {
		        		newdivname = divnamearr[i];
		        		$j("#"+newdivname).replaceWith($j('#'+newdivname, $j(result)));
		        	}
	            }
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }

		});
		$j('.progress-indicator').css( 'display', 'none' );
}

function refreshDiv(url,divname,formId){
	//$('.progress-indicator').css( 'display', 'block' );
	if($j("#"+formId).valid()){
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data : $j("#"+formId).serialize(),
        success: function (result){
        	$j('.ui-datepicker').html("");
        	//$('.tabledisplay').html("");
            var $response=$j(result);
            //query the jq object for the values
            var errorpage = $response.find('#errorForm').html();
           // alert(oneval);
           // var subval = $response.find('#sub').text();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
            	$j("#"+divname).replaceWith($j('#'+divname, $j(result)));
            	//$j("#"+divname).html(result);
            	constructTable();
            }
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }
	});
  }
}





/*window.onload=function(){
var formref=document.getElementById("switchform")
indicateSelected(formref.choice)
}*/


function showCallQueue(){
	//alert("showCallQueue");
	$j('#tdshowid').html($j('#callQueue').html())
}
function showResQueue(){
	$j('#tdshowid').html($j('#tblresp').html())
	
}

function showusecase(){
	if(typeof(document.getElementById("formname"))!=undefined){
		var frmName = document.getElementById("formname").value;
		if(frmName != "" ){
			url = "images/" + frmName +"_usecase.jpg";
			
			window.open(url);	
		}
	}else{
		alert("To Be Developed");	
	}
}

function showwireframe(){
	if(typeof(document.getElementById("formname"))!=undefined){
		var frmName = document.getElementById("formname").value;
		if(frmName != "" ){
			url = "images/" + frmName +"_wireframe.jpg";
			window.open(url);	
		}
	}else{
		alert("To Be Developed");	
	}
}

function showassdoc(){
	if(typeof(document.getElementById("formname"))!=undefined){
		var frmName = document.getElementById("formname").value;
		if(frmName != "" ){
			url = "images/" + frmName +"_assdoc.jpg";
			window.open(url);	
		}
	}else{
		alert("To Be Developed");	
	}
}

function showhelp(){
	alert("To Be Developed");	
}

function showdetails(){
	document.getElementById("editrespondent").style.visibility="visible";
	}
	
function showcontact() {
	document.getElementById("contactactivity").style.visibility="visible";
	document.getElementById("recruitactivity").style.visibility="hidden";
	}
	
	
function showrecruit() {
	document.getElementById("contactactivity").style.visibility="hidden";
	document.getElementById("recruitactivity").style.visibility="visible";
	
	}

function showsearchValues(){
	if(document.getElementById("searchVal").innerHTML == ""){
		document.getElementById("searchVal").innerHTML = 	'<a href="#" onclick="removeSearch()" >Remove All</a>' ;
	}
	document.getElementById("searchVal").innerHTML = document.getElementById("searchVal").innerHTML  + '<a href="#">' + document.getElementById("searchType").value +":" + 	document.getElementById("name").value + '<br /></a>';
	
}

function removeSearch(){
	document.getElementById("searchVal").innerHTML = "";
}

function showMedications(){
	document.getElementById("listMedications").style.display="block";
}

function readXml1(fileName){
	// alert(fileName);
	var resXml = "";
	var pathname = window.location.pathname;
	//alert("pathname" + pathname);
	 var fileurl = "xml/"+fileName;
	
	//alert("fileurl " + fileurl);
	 $j.ajaxSetup({
         url: fileurl
      });
	 $j.ajax({
	        type: "GET",	      
	        dataType: "xml",
	        async:false,
	        cache:false,
	        success: function(xml){
	        	resXml = xml;
	        },
			complete: function( xhr, status ){
				//alert("status "  +status);
				if( status == 'parsererror' ){
					xmlDoc = null;
					if (document.implementation.createDocument){
						// code for Firefox, Mozilla, Opera, etc.
						var xmlhttp = new window.XMLHttpRequest();
						xmlhttp.open("GET", fileurl, false);
						xmlhttp.send(null);
						xmlDoc = xmlhttp.responseXML.documentElement;
					}else  if (window.ActiveXObject){
						
					  xmlDoc=new ActiveXObject( "Microsoft.XMLDOM" ) ;
					  xmlDoc.async = "false" ;
					  xmlDoc.loadXML( xhr.responseText ) ;
					}  else if( window.DOMParser ) {
						// others
					  parser=new DOMParser();
					  xmlDoc=parser.parseFromString( xhr.responseText,"text/xml" ) ;
					}else{
						// 
						alert('Your browser cannot handle this script');
					} 
						resXml= xmlDoc ;				
				  }
				
			},
			error: function(xmlhttp, status, error){
	 			//alert("Err"+status +"status"+ error); 
			}
	 });
	 //alert("resXml " + resXml);
	 return resXml;
}

function unliceseReason(value,divName){
	//alert("1 : "+value+"===="+document.getElementById("unLicensedPk").value);
	
	if(value == document.getElementById("unLicensedPk").value){
		//alert("2 if open");
		//alert(divName);
		var div = document.getElementById(divName);
		//alert(div);
		//div.removeAttribute("style");
		div.setAttribute("style","display:block");
		 //jQuery("#"+divName).show();
		//document.getElementById(divName).style.display='block';	
		}
	else{
		//alert("2.1 else close");
		//alert(divName);
		var div = document.getElementById(divName);
		//alert(div);
		//div.removeAttribute("style");
		div.setAttribute("style","display:none");
		//jQuery("#"+divName).hide();
		//document.getElementById(divName).style.display='none';
	}
}

function inEligibleReason(value,divName){
if((value == document.getElementById("inEligiblePk").value) || (value == document.getElementById("incompleteReasonId").value)){
	document.getElementById(divName).style.display='block';
	$j("#ineligiblereason").show();
	if(value == document.getElementById("inEligiblePk").value){
		$j("#inelreason").css('display','block');
		$j("#increason").css('display','none');
		$j("#priorreason").css('display','none');
	}else if(value == document.getElementById("incompleteReasonId").value){
		$j("#inelreason").css('display','none');
		$j("#increason").css('display','block');
		$j("#priorreason").css('display','none');
	}
}
else
	document.getElementById(divName).style.display='none';
	if(value == document.getElementById("notCollectedToPriorReasonId").value){
		document.getElementById(divName).style.display='block';
		document.getElementById("ineligiblereason").style.display='none';
	}else if(value != document.getElementById("inEligiblePk").value && value != document.getElementById("incompleteReasonId").value){
		document.getElementById(divName).style.display='none';
		}
	else{
		document.getElementById(divName).style.display='block';
		}
}

function closeModal(){
	 $j("#modelPopup1").dialog("close");
	 $j("#modelPopup").dialog("close");
	 $j("#modelPopup1").dialog("destroy");
	 $j("#modelPopup").dialog("destroy");
}

function modalFormSubmitRefreshDiv(formid,url,divname,updateDivId,statusDivId){
	$j('.progress-indicator').css( 'display', 'block' );
	if($j("#"+formid).valid()){
		var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
		var processingSaveDialog = document.createElement('div');
		processingSaveDialog.innerHTML=progressMsg;		
		$j(processingSaveDialog).dialog({autoOpen: false,
			resizable: false, width:400, height:90,closeOnEscape: false,
			modal: true}).siblings('.ui-dialog-titlebar').remove();		
		$j(processingSaveDialog).dialog("open");		
			$j.ajax({
		        type: "POST",
		        url: url,
		       // async:false,
		        data : $j("#"+formid).serialize(),
		        success: function (result){
		        	$j('.ui-datepicker').html("");
		        	//$('.tabledisplay').html("");
		            var $response=$j(result);
		            //query the jq object for the values
		            var errorpage = $response.find('#errorForm').html();
		           // alert(oneval);
		           // var subval = $response.find('#sub').text();
		            if(errorpage != null &&  errorpage != "" ){
		            	$j(processingSaveDialog).dialog("close");	
		            	$j('#main').html(result);
		            }else{
		            	$j("#"+divname).html(result);
		            	$j(processingSaveDialog).dialog("close");
		            	if(updateDivId!=undefined && updateDivId!=""){
		            		$j("#"+updateDivId).css('display','block');			            	
		            	}else{
		            		$j("#update").css('display','block');			            	
		            	}
		            	if(statusDivId!=undefined && statusDivId!=""){
		            		$j("#"+statusDivId).css('display','none');			            	
		            	}else{
		            		$j("#status").css('display','none');			            	
		            	}		            	
		            }
		        },
		        error: function (request, status, error) {
		        	alert("Error " + error);
		        	$j(processingSaveDialog).dialog("close");	
		            alert(request.responseText);
		        }

			});	
    }
	$j('.progress-indicator').css( 'display', 'none' );
}

function toggleDiv(toggleid){
	var contentId = toggleid+"content";
	$j("#"+toggleid).toggle();	
	$j("#"+contentId).find(".ui-icon").each(function(){
		if($j(this).is(".ui-icon-triangle-1-s") || $j(this).is(".ui-icon-triangle-1-e"))
		{
		if($j(this).is(".ui-icon-triangle-1-s")){
			    $j(this).removeClass("ui-icon-triangle-1-s").addClass("ui-icon-triangle-1-e");
	    	}
	    	else if($j(this).is(".ui-icon-triangle-1-e")){
	    	    $j(this).removeClass("ui-icon-triangle-1-e").addClass("ui-icon-triangle-1-s");
	    	}
		}
	});    
   
}


function addMoreFiles(id){

	$j("#"+id).find("tr .add").each(function(){
         $j(this).after("<tr ><td> <s:text name='garuda.unitreport.label.selectyourfile' /></td>"
 				+"<td><s:file name='fileUpload' /><span id='span1' style='color: red;display: none;'><strong>Uploaded file - <s:property value='#request.path' />"
				+"</strong></span></td></tr>");
		});		
}

function limitText(limitField, limitCount, limitNum) {
	//alert(limitField +"--"+ limitCount+"--"+ limitNum);
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}

//for show and hide the columns in the grid


var showFlag1 = false;
var tableColCout =0;

function fn_events(tableall,targetall,ulselectcol){
	 $j('#'+targetall).hide();
		hideCol =[];
	$j('#'+tableall).columnManager({listTargetID:targetall, onClass: 'advon', offClass: 'advoff', hideInList: [tableColCout],  
    saveState: true, colsHidden: hideCol}); 
	//create the clickmenu from the target
	$j('#'+ulselectcol).click( function(){
		  if(showFlag1){
			//$j('#'+targetall).fadeOut();
			showFlag1 = false;
		  }else{
			  $j('#'+targetall).fadeIn();
			  showFlag1 = true;
		  }
		}
	);
	$j('#'+targetall).prepend('<div style="display:inline-block;" ><li onclick="selectCheckBox(this);" class="lineDiv">Reset All</li><img class="crossDiv" src="./images/cross.png" onclick="$j(this).parent().parent().fadeOut();"></div><br/>');
}
var showFlag1 = false;
var tableColCout =0;

function fn_events1(tableall,targetall,ulselectcol){
	 $j('#'+targetall).hide();
		hideCol =[];
		
	$j('#'+tableall).dTcolumnManager({listTargetID:targetall, onClass: 'advon', offClass: 'advoff', hideInList: [tableColCout],  
    saveState: true, colsHidden: hideCol}); 
	//create the clickmenu from the target
	$j('#'+ulselectcol).click( function(){
		  if(showFlag1){
			//$j('#'+targetall).fadeOut();
			showFlag1 = false;
		  }else{
			  $j('#'+targetall).fadeIn();
			  showFlag1 = true;
		  }
		}
	);
	
	$j('#'+targetall).prepend('<div style="display:inline-block;" ><li onclick="selectCheckBox(this);" class="lineDiv">Reset All</li><img class="crossDiv" src="./images/cross.png" onclick="$j(this).parent().parent().fadeOut();"></div><br/>');
	
}
function selectCheckBox(el){
	var ulObj = $j(el).closest("ul");
	var isSelected = $j(ulObj).find(".advoff").length;
	if(isSelected > 0){
		$j(ulObj).find(".advoff").each(function(i,el){
			$j(el).triggerHandler("click");
		})
	}
	else{
		//var shownLength = $j(ulObj).find(".advon");
		//$j(ulObj).find(".advon").each(function(i,el){
		//	if(i!=0){
		//		$j(el).triggerHandler("click");
		//	}
		//})
		 
	}
}

//toolip is for displaying the text in the onmouseover action and hide the text while onmouseout event
/*var tooltip=function(){
	var id = 'tt';
	var top = 3;
	var left = 3;
	var maxw = 300;
	var speed = 10;
	var timer = 20;
	var endalpha = 95;
	var alpha = 0;
	var tt,t,c,b,h;
	var ttie = document.all ? true : false;
	return{
		show:function(v,w){
			if(tt == null){
				tt = document.createElement('div');
				tt.setAttribute('id',id);
				t = document.createElement('div');
				t.setAttribute('id',id + 'top');
				c = document.createElement('div');
				c.setAttribute('id',id + 'cont');
				b = document.createElement('div');
				b.setAttribute('id',id + 'bot');
				tt.appendChild(t);
				tt.appendChild(c);
				tt.appendChild(b);
				document.body.appendChild(tt);
				tt.style.opacity = 0;
				tt.style.filter = 'alpha(opacity=0)';
				document.onmousemove = this.pos;
			}
			tt.style.display = 'block';
			c.innerHTML = v;
			tt.style.width = w ? w + 'px' : 'auto';
			if(!w && ttie){
				t.style.display = 'none';
				b.style.display = 'none';
				tt.style.width = tt.offsetWidth;
				t.style.display = 'block';
				b.style.display = 'block';
			}
			if(tt.offsetWidth > maxw){tt.style.width = maxw + 'px'}
			h = parseInt(tt.offsetHeight) + top;
			clearInterval(tt.timer);
			tt.timer = setInterval(function(){tooltip.fade(1)},timer);
		},
		pos:function(e){
			var u = ttie ? event.clientY + document.documentElement.scrollTop : e.pageY;
			var l = ttie ? event.clientX + document.documentElement.scrollLeft : e.pageX;
			tt.style.top = (u - h) + 'px';
			tt.style.left = (l + left) + 'px';
		},
		fade:function(d){
			var a = alpha;
			if((a != endalpha && d == 1) || (a != 0 && d == -1)){
				var i = speed;
				if(endalpha - a < speed && d == 1){
					i = endalpha - a;
				}else if(alpha < speed && d == -1){
					i = a;
				}
				alpha = a + (i * d);
				tt.style.opacity = alpha * .01;
				tt.style.filter = 'alpha(opacity=' + alpha + ')';
			}else{
				clearInterval(tt.timer);
				if(d == -1){tt.style.display = 'none'}
			}
		},
		hide:function(){
			clearInterval(tt.timer);
			tt.timer = setInterval(function(){tooltip.fade(-1)},timer);
		}
	};
}();*/

	function highlight(id){
		   $j('#'+id).addClass('high');
		}
	
	function removeHighlight(id){
			$j('#'+id).removeClass('high');	
	}
	
	function checkMininum(org_id){
		var flag = true;
		if(org_id==""){
			return false;
	    }
        while (org_id.indexOf("-") > 0) {
			
			var index = org_id.indexOf("-");
			 org_id = org_id.substr(0,index)+org_id.substr(index+1,org_id.length-(index+1)); 
		}
        var idlength=org_id.length;
        if(idlength!=9){
        	flag = false;
        }
        return flag;
	}
	
	function checkFormat(value){
		var flag = true;
		var flag1 = false;
		var flag2 = false;
		var count = 0;
		if(value.indexOf("-")==-1){
			return false;
		}
		while(value.indexOf("-") > 0){
			var index = value.indexOf("-");		
			value = value.substr(0,index)+value.substr(index+1,value.length-(index+1));
			if(index==4 ){
				flag1 = true;				
			}else if(index==8){
				flag2 = true;
			}
			else{
				flag = false;
				break;
			}
		}
		if(!flag)
		  return flag;
		else 
		  return (flag1 && flag2);
	}
	
	function CheckSum(org_id)
	{
		    if(org_id==""){
		    	return false;
		    }
		    var chkarray = new Array(6);
			var chkdex; 
			var chkadd=0;
			var chksum=0; 
			var nxthighest=0;
			var idlength=0;
			
			while (org_id.indexOf("-") > 0) {
			
			index = org_id.indexOf("-");
			 org_id = org_id.substr(0,index)+org_id.substr(index+1,org_id.length-(index+1)); 
			}
			
			var i = 0;
			var flag = true;
			var j = 0;
			var len = org_id.length;
			while(j!=len){
				if((org_id.charCodeAt(i)>64 && org_id.charCodeAt(i)<91) || (org_id.charCodeAt(i)>96 && org_id.charCodeAt(i)<123) ){
						index = org_id.indexOf(org_id.charAt(i));
						org_id = org_id.substr(0,index)+org_id.substr(index+1,org_id.length-(index+1));							
				}else{
					i++;
				}
				j++;
			}
			idlength=org_id.length;
			
			
			for (chkdex = 0; chkdex <= idlength; chkdex++) { 
			chkarray[chkdex] = parseInt(org_id.substr(chkdex, 1));
			if((chkdex>=1)&&(chkdex%2!=0))
			{
			chkarray[chkdex] = 2 * chkarray[chkdex];
			
			}
			
			} 
			
			chkadd = 0; 
			for(chkdex=0;chkdex<idlength-1;chkdex++)
			{
			if(chkarray[chkdex]>=10)
			{
			num=chkarray[chkdex];
			while(num>0)
			{
			chkadd=chkadd+ num%10;
			num=parseInt(num/10);
			
			}
			}
			else
			{
			chkadd=chkadd+chkarray[chkdex];
			}
			}
			
			nxthighest = (Math.floor(chkadd/10)+1)*10; 
			
			chksum = nxthighest - chkadd; 
			if (chksum == 10) {
			chksum = 1;
			} 
			else { 
			if (chksum == 9)
			 {
			chksum = 0;
			}
			 else {
			chksum = chksum + 1;
			}}
			
			 if (chksum == chkarray[idlength-1])
			{
				 return true;
			}else{				   
				   return false;
			 }
	}
	function submitform(formname)	       
	{		     
          if($j("#"+formname).valid()){				 
			       if(document.getElementById('callresolver').value=="true"){              		
	               var cbuid=document.getElementById('cbuid').value;
		           document.getElementById('cbusearchid').value=cbuid;
	               showCbuDiv();
			       document.getElementById("maincontainer").style.display = 'block';
			      }
        
	             else{               
	                 document.getElementById(formname).submit();
				   }
			 }
	}
	
	function enter2Refreshdiv(e)
	{
	    var key = window.event ? e.keyCode : e.which;
	    if (key == 13) //enter
	   {
	   	 	showDiv();
		        return false;
	   }
		    
	return true;
	}
	
	
	function entersRefreshdiv(e)
	{
	    var key = window.event ? e.keyCode : e.which;
	    if (key == 13) //enter
	   {
	  
	    	showCbuDiv();
		        return false;
	   }
		    
	return true;
	}
	
	
		function checkSpecialCharacters(data){
		 var flag = true;
		 var iChars = "!@#$%^&*()+=[]\\\';,./{}|\":<>?~_"; 
		   for (var i = 0; i < data.length; i++) {
		  	if (iChars.indexOf(data.charAt(i)) != -1) {		  	 
		  		flag = false;
		  		break;
		  	}
		  }
		   return flag;
	}
	
	function validateSign(value,id,invalid,minimum,pass){
		if(id=="" || id==null){
			id="submit";			
		}
		if(invalid=="" || invalid==null){
			invalid="invalid";			
		}
		if(minimum=="" || minimum==null){
			minimum="minimum";			
		}
		if(pass=="" || pass==null){
			pass="pass";			
		}
		if(value.length<4){
			$j("#"+minimum).css('display','block');
			$j("#"+invalid).css('display','none');
			$j("#"+pass).css('display','none');
			$j("#"+id).attr("disabled", true);
			$j("#"+id).addClass('ui-state-disabled');
			return false;
		}else{
		var url = "getEsignDetails?userPojo.signature="+value;
			$j.ajax({
				type : "POST",
				url : url,
				async : false,
				url : 'getEsignDetails?userPojo.signature='+value,
				success : function(result) {
				if(result.message=="true"){	
					$j("#"+minimum).css('display','none');
					$j("#"+invalid).css('display','none');
					$j("#"+pass).css('display','block'); 
				    $j("#"+id).attr("disabled", false);
				    $j("#"+id).removeClass('ui-state-disabled');
	              }else{
	            	  $j("#"+minimum).css('display','none');
	            	  $j("#"+invalid).css('display','block');
	            	  $j("#"+pass).css('display','none'); 
					  $j("#"+id).attr("disabled", true);
					  $j("#"+id).addClass('ui-state-disabled');
				 }
			   }
			});		
		}
	}
	

function validateSignOne(value){
		if(value.length<4){
			$j("#minimum1").css('display','block');
			$j("#invalid1").css('display','none');
			$j("#pass1").css('display','none');
			$j("#submit1").attr("disabled", true);
			return false;
		}else{
		var url = "getEsignDetails?userPojo.signature="+value;
			$j.ajax({
				type : "POST",
				url : url,
				async : false,
				url : 'getEsignDetails?userPojo.signature='+value,
				success : function(result) {
				if(result.message=="true"){	
					$j("#minimum1").css('display','none');
					$j("#invalid1").css('display','none');
					$j("#pass1").css('display','block'); 
				    $j("#submit1").attr("disabled", false);              
	              }else{
	            	    $j("#minimum1").css('display','none');
						$j("#invalid1").css('display','block');
						$j("#pass1").css('display','none'); 
					    $j("#submit1").attr("disabled", true); 
				 }
			   }
			});		
		}
	}

function showDocument(AttachmentId){
	//var signed='true';
	var encrypted='false';
	var url='getAttachmentFromDcms?docId='+AttachmentId+'&encrypted='+encrypted;
	window.open( url, "Attachment", "status = 1, height = 500, width = 800, resizable = 0" );
}

function showDocumentForNonMembers(attachmentDiv,AttachmentId,divid,filetype,filename){	
	$j('#'+attachmentDiv+' td').each(function(index,el){
			$j(el).addClass('tabnormal').removeClass('tabactive');
	});
	$j('#'+divid).removeClass('tabnormal').addClass('tabactive');
	var signed='true';
	var ftype="";
	var size=filename.length;
	var index=filename.indexOf('.');
	ftype=filename.substring(index+1,size);
	var url='getAttachment?docId='+AttachmentId+'&signed='+signed+'&getDoc=true&ftype='+ftype;
	refreshAttachment(url,"downloaddoc");
	var obj="";
	var path="temp"+"/"+$j('#filepath').val();
	$j('#downloaddoc1').html("");
	$j('#errorpage').hide();
	if($j('#filepath').val()!=""){
		if(filetype=='application/pdf'){
			obj='<object data="'+path+'" height="750" width="100%" type="'+filetype+'">It appears you don\'t have Required plug-in support in this web browser.<a href="'+path+'">Click here to download the Attachment</a></object>';
		}else{
			obj='<object data="'+path+'" width="100%" type="'+filetype+'">It appears you don\'t have Required plug-in support in this web browser.<a href="'+path+'">Click here to download the Attachment</a></object>';
		}
		$j('#downloaddoc1').append(obj);
	}else{
		$j('#errorpage').show();
	}
}

function refreshAttachment(url,divname){
	var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	var processingSaveDialog = document.createElement('div');
		$j(processingSaveDialog).attr("id",'progressMsg');
		processingSaveDialog.innerHTML=progressMsg;		
		$j(processingSaveDialog).dialog({autoOpen: false,
			resizable: false, width:400, height:90,closeOnEscape: false,
			modal: true}).siblings('.ui-dialog-titlebar').remove();		
		$j(processingSaveDialog).dialog("open");
	$j.ajax({
	    type: "POST",
	    url: url,
	    async:false,
	    success: function (result){
	        var $response=$j(result);
	       	$j("#"+divname).replaceWith($j('#'+divname, $j(result)));
	       	$j(processingSaveDialog).dialog("close");
        },
        error: function (request, status, error) {
        	$j(processingSaveDialog).dialog("close");
        	alert("Error " + error);
            alert(request.responseText);
            
        }
	});
	$j('#progressMsg').remove();
}

function sopReference(){
	var data = readXml1("link-navigation.xml");
	var html = "<ul id='sopTree' class='filetree' >";	
	    $j(data).find("sops").each(function() {
		    html += "<li><span class='folder' >"+$j(this).find("sopname").text()+"</span><ul>";
			$j(this).find("subsop").each(function() {
				html += "<li><span class='file' ><a href='"+$j(this).find("soplink").text()+"' target='blank'>"+$j(this).find("sopdesc").text()+"</a></span></li>";
			});
			html += '</ul></li>';
		});	
	html += "</ul>";	
	//alert(html);
	$j("#ref").html(html);
	$j("#sopTree").treeview();
}

function mopReference(){
	var data = readXml1("link-navigation.xml");
	var html = "<ul id='sopTree' class='filetree' >";	
	    $j(data).find("mops").each(function() {
		    html += "<li><span class='folder' >"+$j(this).find("mopname").text()+"</span><ul><li>";
		    html += "<table width='100%' cellpadding='0' cellspacing='0' id='"+$j(this).find("mopid").text()+"'>";
		    html+= "<thead><tr><th></th><th></th></tr></thead>";
		    html += "<tbody>";
	    	$j(this).find("submop").each(function() {
		    	html+="<tr><td>";
				html += "<a href='"+$j(this).find("moplink").text()+"' target='blank'>"+$j(this).find("mopchapter").text()+"</a>";
				html+= "</td>";
				html+= "<td>";
				html += $j(this).find("moppurpose").text();
				html+="</td><tr>";
				
			});
	    	html += "</tbody>";
	    	html+= "<tfoot><tr><td colspan='2'></td></tr></tfoot>";
		    html += "</table>";
			html += '</li></ul></li>';
		});	
	html += "</ul>";	
	//alert(html);
	$j("#ref").html(html);
	 //$j(data).find("mops").each(function() {
	//	 var id = $j(this).find("mopid").text();
	//	 $j("#"+id).dataTable();
	// )};
	$j("#sopTree").treeview();
}

function guideReference(){
	var data = readXml1("link-navigation.xml");
	var html = "<ul id='sopTree' class='filetree' >";	
	    $j(data).find("guides").each(function() {
		    html += "<li><span class='folder' >"+$j(this).find("guidename").text()+"</span><ul><li>";
		    html += "<table width='100%' border='1' cellpadding='0' class='displaycdr' cellspacing='0' id='"+$j(this).find("guideid").text()+"'>";
		    html+= "<thead><tr><th>Guide Name</th><th>View</th><th>Print</th><th>Download</th></tr></thead>";
		    html += "<tbody>";
	    	$j(this).find("subguide").each(function() {
		    	html+="<tr><td>";
				html += $j(this).find("subguidename").text();
				html+= "</td>";
				html+= "<td>";
				html += "<a href='"+$j(this).find("subguidelink").text()+"' target='blank'>View</a>";
				html+= "</td>";
				html+= "<td>";
				html += "<span class='ui-icon ui-icon-print'></span>";
				html+= "</td>";
				html+= "<td>";
				html += "<span class='ui-icon ui-icon-arrowthickstop-1-s'></span>";
				html+="</td><tr>";				
			});
	    	html += "</tbody>";
	    	//html+= "<tfoot><tr><td colspan='4'></td></tr></tfoot>";
		    html += "</table>";
			html += '</li></ul></li>';
		});	
	html += "</ul>";	
	//alert(html);
	$j("#ref").html(html);
	 //$j(data).find("guides").each(function() {
	//	 var id = $j(this).find("guideid").text();
	//	 $j("#"+id).dataTable();
	// )};
	$j("#sopTree").treeview();
}

function helpReference(){
	var data = readXml1("link-navigation.xml");
	var html = "<ul id='sopTree' class='filetree' >";	
	    $j(data).find("helplinks").each(function() {
		    html += "<li><span class='folder' >"+$j(this).find("helplinkname").text()+"</span><ul>";
			$j(this).find("subhelplink").each(function() {
				html += "<li><span class='file' ><a href='"+$j(this).find("subhelplinkref").text()+"' target='blank'>"+$j(this).find("subhelplinkdesc").text()+"</a></span></li>";
			});
			html += '</ul></li>';
		});	
	html += "</ul>";	
	//alert(html);
	$j("#ref").html(html);
	$j("#sopTree").treeview();
}

function networkReference(){
	var data = readXml1("link-navigation.xml");
	var html = "";
	    	html += "<table width='100%' cellpadding='0' cellspacing='0' id='"+$j(this).find("mopid").text()+"'>";
		    html+= "<thead><tr><th></th></tr></thead>";
		    html += "<tbody>";
		    $j(data).find("networks").each(function(){
		    	$j(this).find("subnetwork").each(function() {
			    	html+="<tr><td>";
					html += "<a href='"+$j(this).find("networklink").text()+"' target='blank'>"+$j(this).find("networkdesc").text()+"</a>";
					html+="</td><tr>";
					
				});
		    });
	    	html += "</tbody>";
	    	html+= "<tfoot><tr><td></td></tr></tfoot>";
		    html += "</table>";
	//alert(html);
	$j("#ref").html(html);
	// $j(data).find("mops").each(function() {
	//	 var id = $j(this).find("mopid").text();
	//	 $j("#"+id).dataTable();
	// )};
	$j("#sopTree").treeview();
}

function clickheretoprint(idOfPrintDiv,regId){
	 var sourceFile ="";
	 var regId = $j.trim(regId);
	 if(regId!=""){
		 sourceFile = "<center><h3><u>CBU Registry ID:"+regId+"</u></h3></center><br/>"+$j('#'+idOfPrintDiv).html();
		 }
	 else
		 sourceFile = $j('#'+idOfPrintDiv).html();
		 $j('#widgetPrintDiv').html(sourceFile)
	 hideshowDivs();
	 $j('#widgetPrintDiv').printArea({mode: "popup", popClose: false});
	 $j('#widgetPrintDiv').html("");
}

function printDUMN(idOfPrintDiv,regId,idOnBag,cbbName,patientId){
	 var sourceFile ="";
	 var regId = $j.trim(regId);
	 if(regId!=""){
		 //sourceFile = "<center><h3><u>CBU Registry ID:"+regId+"</u></h3></center><br/>"+$j('#'+idOfPrintDiv).html();
		 sourceFile +='<table width="100%" style="border:1px solid black;" ><tr><td>Patient Identification Number :'+patientId+'</td>'			 
				   +'</tr><tr><td>Cord Blood Bank/Registry Name :'+cbbName+'</td></tr><tr><td>CBU Registry ID :'+regId+'</td></tr><tr><td>Unique Product Identity on Bag :'+idOnBag+'</td></tr></table>';
		 sourceFile += $j('#'+idOfPrintDiv).html();
		 sourceFile += '<table width="100%"><tr><td width="33%">________________________________</td><td width="33%">________________________________</td><td width="33%">_____/&nbsp;_____/&nbsp;_____/&nbsp;</td></tr>'
		               +'<tr><td width="33%">Transplant Physician Name(Print)</td><td width="33%">Transplant Physician Signature</td><td width="33%"> MM&nbsp;DD&nbsp;YYYY</td></tr>'
	                   +'</table><table width="100%"><tr><td >CBU Declaration of Urgent Medical Need, version x.x,March 31, 2012</td></tr><tr><td> <Font size ="2"><b>Document Number :</b></td></tr>'
		               +'<tr><td><Font size ="2"><b>Replaces :</b></td></tr></table><table width="100%"><tr>'
			           +'<td align="center"><Font size ="1">Copyright &copy; National Marrow Donor Program, All Rights Reserved.</td></tr>'
		               +'<tr><td align="center"><Font size ="1" >National Marrow Donor Program &reg; Priority and Confidential Information</td></tr><p><time pubdate datetime="2012-03-01"></time></p>'
	                   +'</table>';
		 
		 }
	 else
		 sourceFile = $j('#'+idOfPrintDiv).html();
		 $j('#widgetPrintDiv').html(sourceFile)
	 hideshowDivs();
	 $j('#widgetPrintDiv').printArea({mode: "popup", popClose: false});
}
function f_countRows(TblEl,tblfoot,searchtag,selectEl,NoofDaysEl,dateEl1,dateEl2,PercentEl){
	
	
	
	var rowCountval=$j('#'+TblEl).find('tbody tr').length;
	var cellCountval = $j("#"+TblEl).find('tbody tr')[0].cells.length;
	var OrdTblCol = $j("#"+TblEl).find('tr')[0].cells.length;
	var ordFlag=0;
	if(cellCountval==1){
		rowCountval=0;
	}
	
	var i=1;
	while(i<OrdTblCol){
		   
			if($j('#'+selectEl+i).val()!=undefined && $j('#'+selectEl+i).val()!=null && $j('#'+selectEl+i).val()!=""){
						ordFlag=1;
			}
			i++;
	}
				
	if($j('#'+NoofDaysEl).val()!=undefined  && $j('#'+NoofDaysEl).val()!=null && $j('#'+NoofDaysEl).val()!='' ){
		ordFlag=1;
	}
	
	if(($j('#'+dateEl1).val()!=undefined && $j('#'+dateEl1).val()!=null  &&  $j('#'+dateEl1).val()!='' ) || ($j('#'+dateEl2).val()!=undefined && $j('#'+dateEl2).val()!=null && $j('#'+dateEl2).val()!='' )){
		ordFlag=1;
	}
	
	if($j('#'+PercentEl).val()!=undefined && $j('#'+PercentEl).val()!=null && $j('#'+PercentEl).val()!='' ){
		ordFlag=1;
	}
	
	if(searchtag=='fromsearchall'){
		var tbl_filter=TblEl+"_filter";
		var value1=$j('#'+tbl_filter).find('input:text').val();
		
		if(value1!=''){
			ordFlag=1;
		}else{
			ordFlag=0;
		}
		
	}

	if(ordFlag==1){
		var maxRec=Number($j('#maxRec'+tblfoot).val())+1;
	 	var minRec=Number($j('#minRec'+tblfoot).val());
	 	var tcount=maxRec-minRec;
		
		var tmp="<b> Showing 1 to "+rowCountval+" of "+rowCountval+" Entries ( filtered from "+tcount+" total entries )</b>";
		if(cellCountval==1){
			var tmp="<b> Showing 0 to "+rowCountval+" of "+rowCountval+" Entries ( filtered from "+tcount+" total entries )</b>";
		}
		
		$j('#filterShow'+tblfoot).html(tmp);
		$j('#paginationDiv'+tblfoot).hide();
		$j('#filterResult'+tblfoot).show();
	}else{
		$j('#paginationDiv'+tblfoot).show();
		$j('#filterResult'+tblfoot).hide();
	}
	
	
}
function hideshowDivs(){
	  $j("#widgetPrintDiv div > table > tbody").each(function(i, el){
		 var tr = $j(el).find("tr"); 
		 var trlenth = tr.length;
		 if(trlenth==0){
			  $j(el).closest("div").remove(); 
		  }
		});
	  $j('#widgetPrintDiv div').show();
	  $j('#widgetPrintDiv .portlet-content').removeAttr("style");
	  $j('#widgetPrintDiv button').remove();
	  $j('#widgetPrintDiv input[type="button"]').remove();
	  $j('#widgetPrintDiv input[type="text"]').each(function(i,el){
			 $j(el).replaceWith($j(el).val());
		  });
	  
	  $j("#widgetPrintDiv select").each(function (i, el) {
         var isMultiSelect=$j(el).attr("multiple");
         if(isMultiSelect){
            var id =el.id;
            var text ="";
            $j('#'+id+" option:selected").each(function (i, el) {
                if(text!=""){
               	 text+=",";
                   }
           	 text+=$j(this).text();
              });
            $j(el).replaceWith(text);
         }
       });
		 
	  $j("#widgetPrintDiv select option:selected").each(function (i, el) {
         var  text = $j(this).text();
         if(text == "Select"){
       	  text = "";
            }
         var parent = $j(el).closest("select");
         $j(parent).replaceWith(text);
       });
     
	  $j('#widgetPrintDiv input[type="radio"]').each(function(i,el){
		  	var isChecked = $j(el).attr("checked");
		  	if(!isChecked){
		  		var label = $j(el).next('label');
				$j(label).remove();
			 }
			 $j(el).remove();
		  });

	  $j('#widgetPrintDiv .dataTables_info,#widgetPrintDiv .dataTables_length,#widgetPrintDiv .dataTables_filter').hide();
	  $j('#widgetPrintDiv .dataTables_paginate,.paging_two_button').hide();
	
	  var dtable = $j('#widgetPrintDiv .displaycdr');
	  		dtable.css({'border-bottom':'0'});
	  		for(var i=0;i<dtable.length;i++){
		   		dtable[i].border="1";
		   		dtable[i].align="center";
		   }
	}

	function paginationFooter(pageNo, allParams,totalCount,module){
		//String totalParameters = url+","+showsRecordId+","+cbuid+","+formName+","+divName+","+bodyLoadFn;
		var refershMethod = 0;
		var totalparams = allParams.split(","); 
		var url = totalparams[0]+"?iPageNo="+pageNo+"&iShowRows="+$j('#'+totalparams[1]).val();
		if(totalCount!=undefined && totalCount!=""){
			url += "&cbuCount="+totalCount;
		}else{
			url += "&cbuCount=0";
		}
		if(module!=undefined && module!=""){
			url += "&module="+module;
			refershMethod = 1;
		}else{
			url += "&module=saveInProgress";			
		}
		if(totalparams[2]!="" && totalparams[2]!="temp"){
			url=url+"&cbuid="+$j('#'+totalparams[2]).val();
		}
		
		if(totalparams[3] == "notrequired"){
			loadPageByGetRequset(url,totalparams[4]);
			fundingrptsPaginateTable();
		}
		if(totalparams.length == 6){
			if(refershMethod==0)
			refreshDiv(url,totalparams[4],totalparams[3]);
			else
			loadDivWithFormSubmit(url,totalparams[4],totalparams[3]);	
		}
		
		if(totalparams.length == 7 || totalparams.length == 8 || totalparams.length == 9 || totalparams.length == 10){
			if(totalparams.length == 7){
				url=url+"&pkcordId="+$j('#'+totalparams[6]).val();
				refreshDiv(url,totalparams[4],totalparams[3]);
			}
			
			
			if(totalparams.length == 9 && totalparams[7]=="entityId"){
				url=url+"&entityId="+$j('#'+totalparams[7]).val()+"&cordID="+$j('#'+totalparams[6]).val()+"&widget="+totalparams[8];
				refreshDiv(url,totalparams[4],totalparams[3]);
			}
			
			
			if(totalparams.length == 8 && totalparams[6] != "naCBUflag" && totalparams[6]!="sipflag"){
				url=url+"&cordId="+$j('#'+totalparams[6]).val();
				refreshDiv(url,totalparams[4],totalparams[3]);
			}
			if(totalparams.length == 8 && totalparams[6]=="naCBUflag"){
				url=url+$j('#'+totalparams[7]).val();
				refreshDiv(url,totalparams[4],totalparams[3]);
			}
			if(totalparams.length == 8 && totalparams[6]=="sipflag"){
				url=url+$j('#'+totalparams[7]).val();
				refreshDiv(url,totalparams[4],totalparams[3]);
			}
		
			if(totalparams.length == 9 && totalparams[6]!="filterflag"){
				url=url+"&cbbId="+$j('#'+totalparams[6]).val()+"&cbbName="+$j('#'+totalparams[7]).val();
				refreshDiv(url,totalparams[4],totalparams[3]);
				$j('#'+totalparams[4]).show();
			}
			
			if(totalparams.length == 10 && totalparams[6]=="filterflag"){
				url=url+"&pksiteid="+$j('#'+totalparams[7]).val()+"&fkordertype="+$j('#'+totalparams[8]).val()+"&filterPending="+$j('#'+totalparams[6]).val()+$j('#'+totalparams[9]).val();
				refreshDiv(url,totalparams[4],totalparams[3]);
			}
		}
		if(totalparams[5]!=""){
			window[totalparams[5]]();
		}
	 }
	
	function setValueFromId(val,destinationId,valueSetFromId,rights){
		if($j('#'+valueSetFromId).val()!=null && $j('#'+valueSetFromId).val()!="" && $j('#'+valueSetFromId).val()!=undefined
				&& $j('#'+rights).val()!=null && $j('#'+rights).val()!="" && $j('#'+rights).val()!=undefined){
			if($j('#'+rights).val()=='false'){			
			        $j('#'+destinationId).val($j('#'+valueSetFromId).val());								
		   }
	    }
	}
	
	$j(function(){
		$j(".copyPaste").keypress(function(e) {
			var key;
			var keychar;
			var isCtrl;
			var forbiddenKeys = new Array('a', 'n', 'c', 'x', 'v', 'j','t');

		//controls a=select all, n=new window, c=copy, x=cut, v=paste, j=focus will go to google search , t=new tab.
		 
		if (window.event){
			key = window.event.keyCode;
			if(window.event.ctrlKey)
				isCtrl = true;
			else
				isCtrl = false;
		}else if (e){
			key = e.which;
			if(e.ctrlKey)
				isCtrl = true;
			else
				isCtrl = false;
		}
		else
		return true;
		if(isCtrl){
			for(i=0; i<forbiddenKeys .length; i++){
				if(forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase()){
					//alert('Key combination CTRL + '+String.fromCharCode(key)+' has been disabled.');
					return false;
				}
			}
		}
		if((key == 8) || (key == 0))
			return true;
	     });
		
		$j(".copyPaste").keydown(function(e) {
			var key;
			var keychar;
			var isCtrl;
			var forbiddenKeys = new Array('a', 'n', 'c', 'x', 'v', 'j','t');

		//controls a=select all, n=new window, c=copy, x=cut, v=paste, j=focus will go to google search , t=new tab.
		 
		if (window.event){
			key = window.event.keyCode;
			if(window.event.ctrlKey)
				isCtrl = true;
			else
				isCtrl = false;
		}else if (e){
			key = e.which;
			if(e.ctrlKey)
				isCtrl = true;
			else
				isCtrl = false;
		}
		else
		return true;
		if(isCtrl){
			for(i=0; i<forbiddenKeys .length; i++){
				if(forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase()){
					//alert('Key combination CTRL + '+String.fromCharCode(key)+' has been disabled.');
					return false;
				}
			}
		}
		if((key == 8) || (key == 0))
			return true;
	     });
	});
	
	function modifyDocument(cdrCbuId,attachmentId,cbuRegisteryId,dcmsAttachId){
			var url='modifyUnitrpt?cbuCordId='+cdrCbuId+'&attachmentId='+attachmentId+'&modifyUnitrpt=True'+'&modifyDoc=True'+'&cbuRegisteryId='+cbuRegisteryId;
			showModal('Modify Document',url,'500','500');
			
	}
	
	function revokeDocument(cdrCbuId,attachmentId,dcmsAttachId){
		
		var answer = confirm('Please confirm if you want to revoke the document.');
		if (answer){	
	
					var url='revokeDoc?cbuCordId='+cdrCbuId+'&cbuid='+cdrCbuId+'&cordId='+cdrCbuId+'&attachmentId='+attachmentId;
					var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
					var processingSaveDialog = document.createElement('div');
					processingSaveDialog.innerHTML=progressMsg;		
					$j(processingSaveDialog).dialog({autoOpen: false,
						resizable: false, width:400, height:90,closeOnEscape: false,
						modal: true}).siblings('.ui-dialog-titlebar').remove();		
					$j(processingSaveDialog).dialog("open");
								$j.ajax({
									type : "POST",
									async : false,
									url : url,
									//data : $j("#searchOpenOrders2").serialize(),
									success : function(result) {
										$j("#main").html(result);
										refreshMain(cdrCbuId);
										$j(processingSaveDialog).dialog("close");
										alert("Document revoked successfully");
									},
									error: function (request, status, error) {
									alert("Error " + error);
									$j(processingSaveDialog).dialog("close");	
									alert(request.responseText);
									}
								
							});
		}
}
	function refreshMain(cbuid){
		loadPageByGetRequset('refreshMain?cordId='+cbuid,'main');
	}
	
function revokeDocumentForModal(cdrCbuId,attachmentId,dcmsAttachId,title,url1,lenght,breadth){
		
		var answer = confirm('Please confirm if you want to revoke the document.');
		if (answer){	
	
					var url='revokeDoc?cbuCordId='+cdrCbuId+'&cbuid='+cdrCbuId+'&cordId='+cdrCbuId+'&attachmentId='+attachmentId;
					var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
					var processingSaveDialog = document.createElement('div');
					processingSaveDialog.innerHTML=progressMsg;		
					$j(processingSaveDialog).dialog({autoOpen: false,
						resizable: false, width:400, height:90,closeOnEscape: false,
						modal: true}).siblings('.ui-dialog-titlebar').remove();		
					$j(processingSaveDialog).dialog("open");
								$j.ajax({
									type : "POST",
									async : false,
									url : url,
									//data : $j("#searchOpenOrders2").serialize(),
									success : function(result) {
										$j("#main").html(result);
										if(url1!=null && url1!=""){
											subFunForRevokeDocForModal(title,url1,lenght,breadth);
											}
										refreshMain(cdrCbuId);
										$j(processingSaveDialog).dialog("close");
										alert("Document revoked successfully");
									},
									error: function (request, status, error) {
									alert("Error " + error);
									$j(processingSaveDialog).dialog("close");	
									alert(request.responseText);
									}
								
							});
		}
}
	
	function subFunForRevokeDocForModal(title,url,lenght,breadth){
		showModal(title,url,lenght,breadth);
	}
	
	
	function checkDates(id1,id2){
		var flag = false;
		var val1 = $j("#"+id1).val();
		var val2 = $j("#"+id2).val();
		if(val1!=null && val2!=null && val1!="" && val2!="" && val1!=undefined && val2!=undefined){
			//date1 = $j.datepicker.parseDate('yy-mm-dd', val1);
			date1 = $j("#"+id1).datepicker('getDate').format('yy-mm-dd');
			//date2 = $j.datepicker.parseDate('yy-mm-dd', val2);
			date2 = $j("#"+id2).datepicker('getDate').format('yy-mm-dd');
			datearr1 = date1.split("-");
			datearr2 = date2.split("-");			
			year1 = datearr1[0];
			month1 = datearr1[1];
			day1 = datearr1[2];
			year2 = datearr2[0];
			month2 = datearr2[1];
			day2 = datearr2[2];
			if(year1>year2){
				return true;
			}else if(year1<year2){
				return false;
			}else if(year1 == year2){
					if(month1>month2){
						return true;
					}else if(month1<month2){
						return false;
					}else if(month1==month2){
							if(day1>day2){
								return true;
							}else if(day1<day2){
								return false;
							}else if(day1==day2){
								return true;
							}
					}
			}
			
		}else if(val2==null && val2==""){
			return true;
		}else{
			return false;
		}
	}
	
	function checkEligDates(id1,id2){
		var flag = false;
		var val1 = $j("#"+id1).val();
		var val2 = $j("#"+id2).val();
		if(val1!=null && val2!=null && val1!="" && val2!="" && val1!=undefined && val2!=undefined){
			//date1 = $j.datepicker.parseDate('yy-mm-dd', val1);
			date1 = val1;
			//date2 = $j.datepicker.parseDate('yy-mm-dd', val2);
			date2 = val2;
			datearr1 = date1.split("-");
			datearr2 = date2.split("-");			
			year1 = datearr1[0];
			month1 = datearr1[1];
			day1 = datearr1[2];
			year2 = datearr2[0];
			month2 = datearr2[1];
			day2 = datearr2[2];
			if(year1>year2){
				return true;
			}else if(year1<year2){
				return false;
			}else if(year1 == year2){
					if(month1>month2){
						return true;
					}else if(month1<month2){
						return false;
					}else if(month1==month2){
							if(day1>day2){
								return true;
							}else if(day1<day2){
								return false;
							}else if(day1==day2){
								return true;
							}
					}
			}
			
		}else if(val2==null && val2==""){
			return true;
		}else{
			return false;
		}
	}
	
	function checkHour(id1,id2){
		var flag = false;
		var val1 = $j("#"+id1).val();
		var val2 = $j("#"+id2).val();
		if(val1!=null && val2!=null && val1!="" && val2!="" && val1!=undefined && val2!=undefined){
			//date1 = $j.datepicker.parseDate('yy-mm-dd', val1);
			date1 = $j("#"+id1).datepicker('getDate').format('yy-mm-dd');
			//date2 = $j.datepicker.parseDate('yy-mm-dd', val2);
			date2 = $j("#"+id2).datepicker('getDate').format('yy-mm-dd');
			datearr1 = date1.split("-");
			datearr2 = date2.split("-");			
			year1 = datearr1[0];
			month1 = datearr1[1];
			day1 = datearr1[2];
			year2 = datearr2[0];
			month2 = datearr2[1];
			day2 = datearr2[2];
			var diff = Math.abs(new Date(year1, month1, day1) - new Date(year2, month2, day2));
		    if (Math.floor(diff/86400000)) {
		    	if( Math.floor(diff/86400000) >2){
		        	return false;
		        }else{
		        	return true;
		        }
		    } else if (Math.floor(diff/3600000)) {
		    	if(Math.floor(diff/3600000) >48 ){
		        	return false;
		        }else{
		        	return true;
		        }
		    } else if (Math.floor(diff/60000)) {
		    	return true;
		    } else{
		    	return true;
		    }
			
		}else if(val2==null && val2==""){
			return true;
		}else{
			return false;
		}
	}
	
	function dateTimeFormat(val){
		var flag = false;
		var mflag = false;
		var amflag = false;
		var totflag = false;
		//var totflag2 = false;
		var totval = val;
		    while(val.indexOf(":")!=-1){
		    	index = val.indexOf(":");
		    	val = val.substr(0,index)+val.substr(index+1,val.length-(index+1));
		    	if(index==2){
		    		totflag = true;
		    	}
		    }
		    if(totflag){
		       if(totval!=null && totval!="" && totval!=undefined){
					var valarr = totval.split(":");
					if(valarr[0]!=null && valarr[0]!="" && valarr[0]!=undefined){
						valar0 = valarr[0];
						checkhour = isNaN(valar0);
						if(!checkhour){
							if(valar0<=23){
								flag = true;
							}
						}
				    }
					if(valarr[1]!=null && valarr[1]!="" && valarr[1]!=undefined){
						valar1 = valarr[1];
						checkmm = isNaN(valar1);
						if(!checkmm){
							if(valar1<=59){
								mflag = true;								
							}
						}
					}
					/*if(valarr[2]!=null && valarr[2]!="" && valarr[2]!=undefined){
						valar2 = valarr[2];
						checkam = isNaN(valar2);
						if(checkam){
							if(valar2.toUpperCase()=="AM" || valar2.toUpperCase()=="PM"){
								amflag = true;								
							}							
						}
					}*/			
		         }
		       return (flag && mflag);
		    }else{
		    	return false;
		    }
	}

function getDatePic(){
	
	var today = new Date();
	var d = today.getDate();
	var m = today.getMonth();
	var y = today.getFullYear();
	var h=today.getHours();
	var mn=today.getMinutes()+1;

	if($j( ".datePicWMaxDate" ) && $j( ".datePicWMaxDate" ).length>0){
		$j( ".datePicWMaxDate" ).datepicker({
			dateFormat: 'M dd, yy',
			maxDate: new Date(y, m, d),
			changeMonth: true,
			changeYear: true,
			prevText: '',
			nextText: '',
			showButtonPanel: true,
			closeText: "Done",
			currentText: "Today"
		}).click(function() {
		    $j('button.ui-datepicker-current').removeClass('ui-priority-secondary').addClass('ui-priority-primary');
		});
		$j('button.ui-datepicker-current').live('click', function() {
		    $j.datepicker._curInst.input.datepicker('setDate', new Date()).datepicker('hide').blur();
		});

	}
	if($j( ".datePicWMinDate" ) && $j( ".datePicWMinDate" ).length>0){
		$j( ".datePicWMinDate" ).datepicker({
			dateFormat: 'M dd, yy',
			minDate: new Date(y, m, d),
			changeMonth: true,
			changeYear: true,
			prevText: '',
			nextText: '',
			showButtonPanel: true,
			closeText: "Done",
			currentText: "Today"
		}).click(function() {
		    $j('button.ui-datepicker-current').removeClass('ui-priority-secondary').addClass('ui-priority-primary');
		});
		$j('button.ui-datepicker-current').live('click', function() {
		    $j.datepicker._curInst.input.datepicker('setDate', new Date()).datepicker('hide').blur();
		});
	}
	if($j( ".datePicWOMinDate" ) && $j( ".datePicWOMinDate" ).length>0){
		$j( ".datePicWOMinDate" ).datepicker({
			dateFormat: 'M dd, yy',
			changeMonth: true,
			changeYear: true,
			prevText: '',
			nextText: '',
			showButtonPanel: true,
			closeText: "Done",
			currentText: "Today"
		}).click(function() {
		    $j('button.ui-datepicker-current').removeClass('ui-priority-secondary').addClass('ui-priority-primary');
		});
		$j('button.ui-datepicker-current').live('click', function() {
		    $j.datepicker._curInst.input.datepicker('setDate', new Date()).datepicker('hide').blur();
		});
	}
}
function readNMDPXML(linkId){
	var nmdpLink = readXml1("NMDP-Links.xml");
	$j(nmdpLink).find('links[id="'+linkId+'"]').find('url').each(function() {
		var url=$j(this).text();
		window.open(url);
    });
}

function getJQButton(){
    $j('button').button();
    $j('input type[button]').button();
    $j('input type[submit]').button();
}
function showSelectedValue(obj){
	var fieldId=$j(obj).attr('id');
	var selectedText=$j('#'+fieldId+' option:selected').text();
	overlib(selectedText);
}
function getHiddedValues(){
    $j('select').each(function () {
    	if($j(this).is(':visible')){
	        var i = 0;
	        var s = this;
	        for (i = 0; i < s.length; i++){
	            s.options[i].title = s.options[i].text;
	        }
	        if (s.selectedIndex > -1) {
	            s.onmousemove = function () { s.title = s.options[s.selectedIndex].text; };
	
	            if (this.disabled) {
	                var selectHolder = $j('<span>').css({ position: 'relative' });
	                $j(this).wrap(selectHolder);
	
	                var titleSpan = $j('<span>').attr({'class': 'tooltipspan'}).html(s.options[s.selectedIndex].text)
	                							.css({'width':$j(s).width(),'height':$j(s).height(),'top':$j(s).position().top,'left':$j(s).position().left,'background-color':'#D49DAB','opacity':0})
	                							.hover(function(){overlib($j(this).html());},function(){nd();});
	                $j(this).parent().append(titleSpan);
	            }
	        }
    	}
    });
    $j('input:text').each(function () {
    	if($j(this).is(':visible')){
	        var i = 0;
	        var s = this;
	        s.title = s.value;
	        s.onmousemove = function () { s.title = s.value; };
	        if (this.disabled) {
	        	var selectHolder = $j('<span>').css({ position: 'relative' });
	            $j(this).wrap(selectHolder);
	            var titleSpan = $j('<span>').attr({'class': 'tooltipspan' }).html(s.value)
	            							.css({'width':$j(s).width(),'height':$j(s).height(),'top':$j(s).position().top,'left':$j(s).position().left,'background-color':'#D49DAB','opacity':0})
	            							.hover(function(){if($j.trim($j(this).html()).length>0){overlib($j(this).html());}},function(){nd();});
	            $j(this).parent().append(titleSpan);
	        }
	    	}
    });
}