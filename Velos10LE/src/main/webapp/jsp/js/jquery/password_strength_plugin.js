/**
 * @author Raviesh Arora
 * @date 02/11/2013
 * @param shortPass:	"validation-fail",
 * @param messageloc:	1				//before == 0 or after == 1
*/

(function($){ 

	$.fn.shortPass = L_TooShort;
	$.fn.valid = L_Valid;
	$.fn.Invalid = L_Invalid;
	$.fn.RotationError = L_RotationError;
	
	
	$.fn.okPass = '';
	$.fn.resultStyle = "";
	
	//Function to check password strength
	 $.fn.passStrength = function(options) {
		    
		 var defaults = {
				shortPass: 		"validation-fail",
				valid    :      "validation-pass",
				Invalid  :      "validation-fail",
				messageloc:		1				    //before == 0 or after == 1
				
			}; 
		 	var opts = $.extend(defaults, options);
		      
		 	return this.each(function(){ 
		 		 
		 		var obj = $(this);
		 		$(obj).unbind().keyup(function()
		 		{
		 			//Calling teststrength() function to verify valid password conditions and Password Rotation Error
					var results = $.fn.teststrength($(this).val(),opts);
					
					if(opts.messageloc === 1)
					{
						$(this).next(".test").remove();
						$(this).after("<span id=\"passPortion\" class=\"test\"><span></span></span>");
						$(this).next(".test").addClass($(this).resultStyle).find("span").text(results);
					}
					else
					{
						$(this).prev(".test").remove();
						$(this).before("<span id=\"passPortion\" class=\"test\"><span></span></span>");
						$(this).prev(".test").addClass($(this).resultStyle).find("span").text(results);
					}
		 		 });
		 		
		 		//Function to verify valid password conditions and Password Rotation Error
		 		$.fn.teststrength = function(password,option){
		 				
		 				//No message will be shown if nothing in the password field - Bug#15014 : Raviesh
		 				if(password.length == 0){
		 					$("#isSubmitFlag").val("false");
		 					this.resultStyle =  "";
		 					return $(this).okPass;
		 				}
		 				
		 			    //Check for Password Length less than 8 not allowed
		 			    if (password.length < 8 ) { 
		 			    	$("#isSubmitFlag").val("false");
		 			    	this.resultStyle =  option.shortPass;
		 			    	return $(this).shortPass; 
		 			    }else if(password.length > 7){
		 			    	
		 			    	//Check for User Password Error, if above conditions pass
		 			    	if(opts.comeFrom === 1){
		 			    		
		 			    		var checkConscutiveCharacters = new Boolean(false);
			 					var isValid=true;
			 					var rotationFlag="false";
			 					var checkConsecResult="false";
			 					
			 					/*Regular expression to check for :
			 					  Password must contain no spaces and 3 out of the 4 of these:
			 					  1)lowercase letter
			 					  2)uppercase letter
			 					  3)number
			 					  4)special characters */
			 					//   ! # \\$ \\^ % & * ( ) + = \\- \\[ \\] \\\ \\' ; , \\. \\/ \\{ \\} \\| : < > \\?
			 					var regExpPassValid = new RegExp("^((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])|(?=.*[a-z])(?=.*[A-Z])(?=.*[! @ # \\$ \\^ % & * ( ) + = \\- \\[ \\] \\\ \\' ; , \\. \\/ \\{ \\} \\| : < > \\?])|(?=.*[a-z])(?=.*[0-9])(?=.*[! @ # \\$ \\^ % & * ( ) + = \\- \\[ \\] \\\ \\' ; , \\. \\/ \\{ \\} \\| : < > \\?])|(?=.*[A-Z])(?=.*[0-9])(?=.*[! @ # \\$ \\^ % & * ( ) + = \\- \\[ \\] \\\ \\' ; , \\. \\/ \\{ \\} \\| : < > \\?])).{8,}$");
			 					var testwhitespace = new RegExp(/\s/g);
			 					
			 			    	var oldPass = document.getElementById('oldPass').value;
			 					var newPass = document.getElementById('newPass').value;
			 					var loginName = document.getElementById('loginName').value;
		 					
			 					//Check for the equality of Login Name and New Password should not be same
			 					if (loginName==newPass) {
			 						$("#isSubmitFlag").val("false");
			 						isValid=false;
			 						this.resultStyle =  option.Invalid;
			 						return $(this).Invalid;
			 					}
			 					
			 					var resultPassValid = (regExpPassValid.test(newPass))?"true":"false";
			 					var isWhiteSpace = (testwhitespace.test(newPass))?"true":"false";
			 					
			 					//Check for Password valid conditions
			 					if(resultPassValid=='false' || isWhiteSpace=='true'){
			 						$("#isSubmitFlag").val("false");
			 						isValid=false;
			 						this.resultStyle = option.Invalid;
			 						return $(this).Invalid;
			 					}
			 					
			 					//Check for not allowing three consecutive characters in the password
			 					checkConsecResult = $.fn.checkConsecutive(newPass);
			 					if(checkConsecResult==true){
			 						$("#isSubmitFlag").val("false");
			 						isValid=false;
			 						this.resultStyle = option.Invalid;
			 						return $(this).Invalid;
			 					}
			 					
			 					if(isValid){
			 						newPass = encodeURIComponent(newPass);
			 						$.ajax({ url:"checkPasswordStrength.jsp?newPass="+newPass+"&comeFrom="+opts.comeFrom ,
			 							type: "POST",
			 							async:false,
			 							success: (function(data){
			 					  		var response=$(data);
			 					  		var checkPassRotationResult = response.filter('#filter').val();
			 					  		rotationFlag=checkPassRotationResult;
			 							}),
			 							error:function(response) {  return false; }
			 							});
			 						if(rotationFlag=='true'){
			 							$("#isSubmitFlag").val("false");
			 							this.resultStyle =  option.Invalid;return $(this).RotationError;
			 							
			 							}
			 							else{
			 								$("#isSubmitFlag").val("true");
			 								this.resultStyle =  option.valid;
			 								return $(this).valid;
		 								}
			 					}
		 					
		 			    	}
		 			    	//Check for Portal Password Error, if above conditions pass
		 			    	else if(opts.comeFrom === 2){
		 			    		var newPass = password;
		 			    		var checkConscutiveCharacters = new Boolean(false);
			 					var isValid=true;
			 					var rotationFlag="false";
			 					var checkConsecResult="false";
			 					
			 					/*Regular expression to check for :
			 					  Password must contain no spaces and 3 out of the 4 of these:
			 					  1)lowercase letter
			 					  2)uppercase letter
			 					  3)number
			 					  4)special characters */
			 					//   ! # \\$ \\^ % & * ( ) + = \\- \\[ \\] \\\ \\' ; , \\. \\/ \\{ \\} \\| : < > \\?
			 					var regExpPassValid = new RegExp("^((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])|(?=.*[a-z])(?=.*[A-Z])(?=.*[! @ # \\$ \\^ % & * ( ) + = \\- \\[ \\] \\\ \\' ; , \\. \\/ \\{ \\} \\| : < > \\?])|(?=.*[a-z])(?=.*[0-9])(?=.*[! @ # \\$ \\^ % & * ( ) + = \\- \\[ \\] \\\ \\' ; , \\. \\/ \\{ \\} \\| : < > \\?])|(?=.*[A-Z])(?=.*[0-9])(?=.*[! @ # \\$ \\^ % & * ( ) + = \\- \\[ \\] \\\ \\' ; , \\. \\/ \\{ \\} \\| : < > \\?])).{8,}$");
			 					var testwhitespace = new RegExp(/\s/g);
			 					
			 					//var newPass = document.getElementById('defPass').value;
			 				    var portalId = document.getElementById('portalId').value;
		 					
			 					var resultPassValid = (regExpPassValid.test(newPass))?"true":"false";
			 					var isWhiteSpace = (testwhitespace.test(newPass))?"true":"false";
			 					
			 					//Check for Password valid conditions
			 					if(resultPassValid=='false' || isWhiteSpace=='true'){
			 						$("#isSubmitFlag").val("false");
			 						isValid=false;
			 						this.resultStyle = option.Invalid;
			 						return $(this).Invalid;
			 					}
			 					
			 					//Check for not allowing three consecutive characters in the password
			 					checkConsecResult = $.fn.checkConsecutive(newPass);
			 					if(checkConsecResult==true){
			 						$("#isSubmitFlag").val("false");
			 						isValid=false;
			 						this.resultStyle = option.Invalid;
			 						return $(this).Invalid;
			 					}
			 					
			 					if(isValid){
			 						newPass = encodeURIComponent(newPass);
			 						$.ajax({ url:"checkPasswordStrength.jsp?defPass="+newPass+"&comeFrom="+opts.comeFrom+"&portalId="+portalId ,
			 							type: "POST",
			 							async:false,
			 							success: (function(data){
			 					  		var response=$(data);
			 					  		var checkPassRotationResult = response.filter('#filter').val();
			 					  		rotationFlag=checkPassRotationResult;
			 							}),
			 							error:function(response) {  return false; }
			 							});
			 						if(rotationFlag=='true'){
			 							$("#isSubmitFlag").val("false");
			 							this.resultStyle =  option.Invalid;return $(this).RotationError;
			 							
			 							}
			 							else{
			 								$("#isSubmitFlag").val("true");
			 								this.resultStyle =  option.valid;
			 								return $(this).valid;
		 								}
			 					}
		 					
		 					}
		 			    	//Check for Portal Login Password Error, if above conditions pass
		 			    	else if(opts.comeFrom === 3){
		 			    		var newPass = password;
		 			    		var checkConscutiveCharacters = new Boolean(false);
			 					var isValid=true;
			 					var rotationFlag="false";
			 					var checkConsecResult="false";
			 					
			 					/*Regular expression to check for :
			 					  Password must contain no spaces and 3 out of the 4 of these:
			 					  1)lowercase letter
			 					  2)uppercase letter
			 					  3)number
			 					  4)special characters */
			 					//   ! # \\$ \\^ % & * ( ) + = \\- \\[ \\] \\\ \\' ; , \\. \\/ \\{ \\} \\| : < > \\?
			 					var regExpPassValid = new RegExp("^((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])|(?=.*[a-z])(?=.*[A-Z])(?=.*[! @ # \\$ \\^ % & * ( ) + = \\- \\[ \\] \\\ \\' ; , \\. \\/ \\{ \\} \\| : < > \\?])|(?=.*[a-z])(?=.*[0-9])(?=.*[! @ # \\$ \\^ % & * ( ) + = \\- \\[ \\] \\\ \\' ; , \\. \\/ \\{ \\} \\| : < > \\?])|(?=.*[A-Z])(?=.*[0-9])(?=.*[! @ # \\$ \\^ % & * ( ) + = \\- \\[ \\] \\\ \\' ; , \\. \\/ \\{ \\} \\| : < > \\?])).{8,}$");
			 					var testwhitespace = new RegExp(/\s/g);
			 					
			 					//var newPass = document.getElementById('defPass').value;
			 				    var portalId = document.getElementById('portalId').value;
			 				    var portalLoginId = document.getElementById('portalLoginId').value;
		 					
			 					var resultPassValid = (regExpPassValid.test(newPass))?"true":"false";
			 					var isWhiteSpace = (testwhitespace.test(newPass))?"true":"false";
			 					
			 					var patLogin = $j("#patLogin").val();

			 					//Check for the equality of Login Name and New Password should not be same
			 					if(patLogin==newPass){
			 						$("#isSubmitFlag").val("false");
			 						isValid=false;
			 						this.resultStyle = option.Invalid;
			 						return $(this).Invalid;
			 					}
			 					
			 					//Check for Password valid conditions
			 					if(resultPassValid=='false' || isWhiteSpace=='true'){
			 						$("#isSubmitFlag").val("false");
			 						isValid=false;
			 						this.resultStyle = option.Invalid;
			 						return $(this).Invalid;
			 					}
			 					
			 					//Check for not allowing three consecutive characters in the password
			 					checkConsecResult = $.fn.checkConsecutive(newPass);
			 					if(checkConsecResult==true){
			 						$("#isSubmitFlag").val("false");
			 						isValid=false;
			 						this.resultStyle = option.Invalid;
			 						return $(this).Invalid;
			 					}
			 					
			 					if(isValid){
			 						newPass = encodeURIComponent(newPass);
			 						$.ajax({ url:"checkPasswordStrength.jsp?defPass="+newPass+"&comeFrom="+opts.comeFrom+"&portalId="+portalId+"&portalLoginId="+portalLoginId ,
			 							type: "POST",
			 							async:false,
			 							success: (function(data){
			 					  		var response=$(data);
			 					  		var checkPassRotationResult = response.filter('#filter').val();
			 					  		rotationFlag=checkPassRotationResult;
			 							}),
			 							error:function(response) {  return false; }
			 							});
			 						if(rotationFlag=='true'){
			 							$("#isSubmitFlag").val("false");
			 							this.resultStyle =  option.Invalid;return $(this).RotationError;
			 							
			 							}
			 							else{
			 								$("#isSubmitFlag").val("true");
			 								this.resultStyle =  option.valid;
			 								return $(this).valid;
		 								}
			 					}
		 					
		 					}
		 			    	//Check for New User Password Error, if above conditions pass
		 			    	else if(opts.comeFrom === 4){
		 			    		var newPass = password;   //New userPassword
		 			    		var checkConscutiveCharacters = new Boolean(false);
			 					var isValid=true;
			 					var rotationFlag="false";
			 					var checkConsecResult="false";
			 					
			 					/*Regular expression to check for :
			 					  Password must contain no spaces and 3 out of the 4 of these:
			 					  1)lowercase letter
			 					  2)uppercase letter
			 					  3)number
			 					  4)special characters */
			 					 //   ! # \\$ \\^ % & * ( ) + = \\- \\[ \\] \\\ \\' ; , \\. \\/ \\{ \\} \\| : < > \\?
			 					var regExpPassValid = new RegExp("^((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])|(?=.*[a-z])(?=.*[A-Z])(?=.*[! @ # \\$ \\^ % & * ( ) + = \\- \\[ \\] \\\ \\' ; , \\. \\/ \\{ \\} \\| : < > \\?])|(?=.*[a-z])(?=.*[0-9])(?=.*[! @ # \\$ \\^ % & * ( ) + = \\- \\[ \\] \\\ \\' ; , \\. \\/ \\{ \\} \\| : < > \\?])|(?=.*[A-Z])(?=.*[0-9])(?=.*[! @ # \\$ \\^ % & * ( ) + = \\- \\[ \\] \\\ \\' ; , \\. \\/ \\{ \\} \\| : < > \\?])).{8,}$");
			 					var testwhitespace = new RegExp(/\s/g);
			 					
			 					//var userPassword = document.getElementById('userPassword').value;
			 					var userLogin = (document.getElementById('userLogin').value==null)?"":(document.getElementById('userLogin').value);
		 					
			 					//Check for the equality of Login Name and New Password should not be same
			 					if (userLogin==newPass) {
			 						$("#isSubmitFlag").val("false");
			 						isValid=false;
			 						this.resultStyle =  option.Invalid;
			 						return $(this).Invalid;
			 					}
		 					
			 					var resultPassValid = (regExpPassValid.test(newPass))?"true":"false";
			 					var isWhiteSpace = (testwhitespace.test(newPass))?"true":"false";
			 					
			 					//Check for Password valid conditions
			 					if(resultPassValid=='false' || isWhiteSpace=='true'){
			 						$("#isSubmitFlag").val("false");
			 						isValid=false;
			 						this.resultStyle = option.Invalid;
			 						return $(this).Invalid;
			 					}
			 					
			 					//Check for not allowing three consecutive characters in the password
			 					checkConsecResult = $.fn.checkConsecutive(newPass);
			 					if(checkConsecResult==true){
			 						$("#isSubmitFlag").val("false");
			 						isValid=false;
			 						this.resultStyle = option.Invalid;
			 						return $(this).Invalid;
			 					}
			 					
			 					if(isValid){
			 								$("#isSubmitFlag").val("true");
			 								this.resultStyle =  option.valid;
			 								return $(this).valid;
			 					}
		 					
		 					}
		 			    	//Check for User Password Expiry Error, if above conditions pass
		 			    	else if(opts.comeFrom === 5){
		 			    		var newPass_expiry = password;   //New userPassword
		 			    		var checkConscutiveCharacters = new Boolean(false);
			 					var isValid=true;
			 					var rotationFlag="false";
			 					var checkConsecResult="false";
			 					
			 					/*Regular expression to check for :
			 					  Password must contain no spaces and 3 out of the 4 of these:
			 					  1)lowercase letter
			 					  2)uppercase letter
			 					  3)number
			 					  4)special characters */
			 					//   ! # \\$ \\^ % & * ( ) + = \\- \\[ \\] \\\ \\' ; , \\. \\/ \\{ \\} \\| : < > \\?
			 					var regExpPassValid = new RegExp("^((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])|(?=.*[a-z])(?=.*[A-Z])(?=.*[! @ # \\$ \\^ % & * ( ) + = \\- \\[ \\] \\\ \\' ; , \\. \\/ \\{ \\} \\| : < > \\?])|(?=.*[a-z])(?=.*[0-9])(?=.*[! @ # \\$ \\^ % & * ( ) + = \\- \\[ \\] \\\ \\' ; , \\. \\/ \\{ \\} \\| : < > \\?])|(?=.*[A-Z])(?=.*[0-9])(?=.*[! @ # \\$ \\^ % & * ( ) + = \\- \\[ \\] \\\ \\' ; , \\. \\/ \\{ \\} \\| : < > \\?])).{8,}$");
			 					var testwhitespace = new RegExp(/\s/g);
			 					
			 					var loginName = document.getElementById('loginName').value;
		 					
			 					//Check for the equality of Login Name and New Password should not be same
			 					if (loginName==newPass_expiry) {
			 						$("#isSubmitFlag").val("false");
			 						isValid=false;
			 						this.resultStyle =  option.Invalid;
			 						return $(this).Invalid;
			 					}
			 					
			 					var resultPassValid = (regExpPassValid.test(newPass_expiry))?"true":"false";
			 					var isWhiteSpace = (testwhitespace.test(newPass_expiry))?"true":"false";
			 					
			 					//Check for Password valid conditions
			 					if(resultPassValid=='false' || isWhiteSpace=='true'){
			 						$("#isSubmitFlag").val("false");
			 						isValid=false;
			 						this.resultStyle = option.Invalid;
			 						return $(this).Invalid;
			 					}
			 					
			 					//Check for not allowing three consecutive characters in the password
			 					checkConsecResult = $.fn.checkConsecutive(newPass_expiry);
			 					if(checkConsecResult==true){
			 						$("#isSubmitFlag").val("false");
			 						isValid=false;
			 						this.resultStyle = option.Invalid;
			 						return $(this).Invalid;
			 					}
			 					
			 					if(isValid){
			 						newPass_expiry = encodeURIComponent(newPass_expiry);
			 						$.ajax({ url:"checkPasswordStrength.jsp?newPass_expiry="+newPass_expiry+"&comeFrom="+opts.comeFrom ,
			 							type: "POST",
			 							async:false,
			 							success: (function(data){
			 					  		var response=$(data);
			 					  		var checkPassRotationResult = response.filter('#filter').val();
			 					  		rotationFlag=checkPassRotationResult;
			 							}),
			 							error:function(response) {  return false; }
			 							});
			 						if(rotationFlag=='true'){
			 							$("#isSubmitFlag").val("false");
			 							this.resultStyle =  option.Invalid;return $(this).RotationError;
			 							
			 							}
			 							else{
			 								$("#isSubmitFlag").val("true");
			 								this.resultStyle =  option.valid;
			 								return $(this).valid;
		 								}
			 					}
		 					
		 			    	}
		 			    	//Check for Account Registration Password Expiry Error, if above conditions pass
		 			    	else if(opts.comeFrom === 6){
		 			    		var userPwd = password;   //New userPassword
		 			    		var checkConscutiveCharacters = new Boolean(false);
			 					var isValid=true;
			 					var rotationFlag="false";
			 					var checkConsecResult="false";
			 					
			 					/*Regular expression to check for :
			 					  Password must contain no spaces and 3 out of the 4 of these:
			 					  1)lowercase letter
			 					  2)uppercase letter
			 					  3)number
			 					  4)special characters */
			 					//   ! # \\$ \\^ % & * ( ) + = \\- \\[ \\] \\\ \\' ; , \\. \\/ \\{ \\} \\| : < > \\?
			 					var regExpPassValid = new RegExp("^((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])|(?=.*[a-z])(?=.*[A-Z])(?=.*[! @ # \\$ \\^ % & * ( ) + = \\- \\[ \\] \\\ \\' ; , \\. \\/ \\{ \\} \\| : < > \\?])|(?=.*[a-z])(?=.*[0-9])(?=.*[! @ # \\$ \\^ % & * ( ) + = \\- \\[ \\] \\\ \\' ; , \\. \\/ \\{ \\} \\| : < > \\?])|(?=.*[A-Z])(?=.*[0-9])(?=.*[! @ # \\$ \\^ % & * ( ) + = \\- \\[ \\] \\\ \\' ; , \\. \\/ \\{ \\} \\| : < > \\?])).{8,}$");
			 					var testwhitespace = new RegExp(/\s/g);
			 					
			 					var resultPassValid = (regExpPassValid.test(userPwd))?"true":"false";
			 					var isWhiteSpace = (testwhitespace.test(userPwd))?"true":"false";
			 					
			 					//Check for Password valid conditions
			 					if(resultPassValid=='false' || isWhiteSpace=='true'){
			 						$("#isSubmitFlag").val("false");
			 						isValid=false;
			 						this.resultStyle = option.Invalid;
			 						return $(this).Invalid;
			 					}
			 					
			 					//Check for not allowing three consecutive characters in the password
			 					checkConsecResult = $.fn.checkConsecutive(userPwd);
			 					if(checkConsecResult==true){
			 						$("#isSubmitFlag").val("false");
			 						isValid=false;
			 						this.resultStyle = option.Invalid;
			 						return $(this).Invalid;
			 					}
			 					
			 					if(isValid){
			 						userPwd = encodeURIComponent(userPwd);
			 						$.ajax({ url:"checkPasswordStrength.jsp?userPwd="+userPwd+"&comeFrom="+opts.comeFrom ,
			 							type: "POST",
			 							async:false,
			 							success: (function(data){
			 					  		var response=$(data);
			 					  		var checkPassRotationResult = response.filter('#filter').val();
			 					  		rotationFlag=checkPassRotationResult;
			 							}),
			 							error:function(response) {  return false; }
			 							});
			 						if(rotationFlag=='true'){
			 							$("#isSubmitFlag").val("false");
			 							this.resultStyle =  option.Invalid;return $(this).RotationError;
			 							
			 							}
			 							else{
			 								$("#isSubmitFlag").val("true");
			 								this.resultStyle =  option.valid;
			 								return $(this).valid;
		 								}
			 					}
		 					
		 			    	}
		 			    }else{
		 			    	this.resultStyle =  option.okPass;
		 			    	return $(this).okPass;
		 			    	}
		 		};
		 		
		 		//Function to Check for not allowing three consecutive characters in the password
		 		$.fn.checkConsecutive  = function(newPass){
		 			for(var i = 0, length = newPass.length; i < length; i++) {
		 				if(newPass.charAt(i)==newPass.charAt(i+1)){
		 					if(newPass.charAt(i)==newPass.charAt(i+2)){
		 						checkConsecResult="true";
		 						return true;
		 						}
		 				}
		 			}
		 			checkConsecResult="false";
		 			return false;
		 		};
		  });  
	 };  
})(jQuery); 
