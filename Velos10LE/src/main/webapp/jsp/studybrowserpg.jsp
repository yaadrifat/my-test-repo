<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_MngPcol_Open%><%--Manage Protocols>> Open*****--%></title>
<%@ page import="com.velos.eres.service.util.*"%>
<script>//km
var screenWidth = screen.width;
var screenHeight = screen.height;

function confirmBox(name){
	var paramArray = [name];
    if (confirm(getLocalizedMessageString("M_Want_DelStd",paramArray)))/*if (confirm("Do you want to delete <%=LC.Std_Study%> "+ name+"?" ))*****/
	return true;
    else
	return false;

}//km

function opensvwin(id) {
       windowName = window.open("studyview.jsp?studyVPk="+ id,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")
	 windowName.focus();
;}

function setOrder(formObj,orderBy) //orderBy column number
{
	var lorderType;
	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";
		lorderType="desc";
	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
		lorderType="asc";
	}

	lsrc = formObj.srcmenu.value;
	lcurPage = formObj.page.value;
	formObj.action="studybrowserpg.jsp?srcmenu="+lsrc+"&page="+lcurPage+"&orderBy="+orderBy+"&orderType="+lorderType;
	formObj.submit();
}

function setOrderView(formObj,orderBy) //orderBy column number
{
	var lorderType;
	if (formObj.orderTypeView.value=="asc") {
		formObj.orderTypeView.value= "desc";
		lorderType="desc";
	} else 	if (formObj.orderTypeView.value=="desc") {
		formObj.orderTypeView.value= "asc";
		lorderType="asc";
	}

	lsrc = formObj.srcmenu.value;
	lcurPage = formObj.pageView.value;
	formObj.action="studybrowserpg.jsp?srcmenu="+lsrc+"&pageView="+lcurPage+"&orderByView="+orderBy+"&orderTypeView="+lorderType;
	formObj.submit();
}




</script>

<%
String src="tdMenuBarItem3";
%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<body>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>

<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/><!--km-->
<jsp:useBean id="eventassocB" scope="request"	class="com.velos.esch.web.eventassoc.EventAssocJB" />
<%@ page language = "java" import = "com.velos.eres.web.user.UserJB,com.velos.eres.web.codelst.CodelstJB, com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.EJBUtil,java.util.*,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,java.sql.*,com.velos.eres.business.common.*,com.velos.eres.service.util.StringUtil"%>
<br>

<SCRIPT LANGUAGE="JavaScript">
if (navigator.userAgent.indexOf("MSIE") == -1){ 
	if(screenWidth>1280 || screenHeight>1024)
		document.write('<div class="browserDefault browserDefault_studySearch" id="div1" style="height:525px;">');
	else
		document.write('<div class="browserDefault browserDefault_studySearch" id="div1">');
}else
	{
	if(screenWidth>1280 || screenHeight>1024)
		document.write('<div class="browserDefault browserDefault_studySearch" id="div1" style="height:565px;">');
	else
		document.write('<div class="browserDefault browserDefault_studySearch" id="div1">');
	}
</SCRIPT>

<%
String pagenum = "";
int curPage = 0;
long startPage = 1;
long cntr = 0;
String pagenumView = "";
int curPageView = 0;
long startPageView = 1;
long cntrView = 0;

pagenum = request.getParameter("page");
if (pagenum == null)
{
	pagenum = "1";
}
curPage = EJBUtil.stringToNum(pagenum);

String orderBy = "";
orderBy = request.getParameter("orderBy");


if (EJBUtil.isEmpty(orderBy))
   orderBy = "4";

String orderType = "";
orderType = request.getParameter("orderType");

if (orderType == null)
{
	orderType = "asc";
}


pagenumView = request.getParameter("pageView");
if (pagenumView == null)
{
	pagenumView = "1";
}
curPageView = EJBUtil.stringToNum(pagenumView);

String orderByView = "";
orderByView = request.getParameter("orderByView");

if (EJBUtil.isEmpty(orderByView))
   orderByView = "4";

String orderTypeView = "";
orderTypeView = request.getParameter("orderTypeView");

if (orderTypeView == null)
{
	orderTypeView = "asc";
}

String searchCriteria = request.getParameter("searchCriteria");

if (searchCriteria==null) {searchCriteria="";}



HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession)) {
	String userId = (String) tSession.getValue("userId");

	UserJB usrSessionJb = (UserJB) tSession.getValue("currentUser");

	String userSiteId = "";
	userSiteId = usrSessionJb.getUserSiteId();

	//Added by Manimaran to give access right for default admin group to the delete link
	int usrId = EJBUtil.stringToNum(userId);


	String defGroup = usrSessionJb.getUserGrpDefault();

	groupB.setGroupId(EJBUtil.stringToNum(defGroup));
	groupB.getGroupDetails();
	String groupName = groupB.getGroupName();
	//km

	int pageRight = 0;
	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");
	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));

	String accId = (String) tSession.getValue("accountId");

	acmod.getControlValues("study_rights","STUDYMPAT");
	ArrayList aRightSeq = acmod.getCSeq();
	String rightSeq = aRightSeq.get(0).toString();
	int iRightSeq = EJBUtil.stringToNum(rightSeq);
	String superuserRights = "";

	if (pageRight > 0 )	{

//get default super user rights , returns null if user does not belong to a super user group
            superuserRights = GroupDao.getDefaultStudySuperUserRights(userId);


String studySql =  " select a.pk_study, a.study_title,a.STUDY_NUMBER,"
	+ "  lower(a.STUDY_NUMBER) STUDY_NUMBER_SORT , lower(a.study_title) study_title_sort, "
	+ " a.STUDY_ACTUALDT, "
	+ " (select ER_CODELST.CODELST_DESC from ER_CODELST where ER_CODELST.PK_CODELST=a.fk_codelst_phase) phase, "
	+ " (select ER_CODELST.CODELST_DESC from ER_CODELST where ER_CODELST.PK_CODELST=a.fk_codelst_tarea) tarea, "
	+ "  NVL(a.last_modified_date,a.created_on)   last_modified_date, "
 	+ " (nvl((select study_team_rights from er_studyteam where fk_user = " + userId +" and "
	+ " fk_study = pk_study  and study_team_usr_type = 'D'), '"+superuserRights+"' ) ) as	study_team_rights, "
	+ " nvl( (SELECT MAX(pk_studystat) FROM ER_STUDYSTAT WHERE "
	+ " fk_study = pk_study and fk_site= " + userSiteId
	+ " AND fk_codelst_studystat IN ( select pk_codelst "
	+ " from er_codelst  where codelst_type='studystat'   and (trim(codelst_custom_col)='browser') "
	+ " )  AND studystat_date = (SELECT MAX(studystat_date) FROM ER_STUDYSTAT WHERE "
	+ " fk_codelst_studystat IN ( select pk_codelst from er_codelst "
	+ " where codelst_type='studystat'   and (trim(codelst_custom_col)='browser') "
	+ ") AND fk_study = pk_study and fk_site="+userSiteId+")),0)as pk_studystat"
	+ " from er_study a"
	+ " where a.STUDY_VERPARENT is null and fk_account = " + accId
	+ " and ( exists ( select * from er_studyteam where fk_study = pk_study and "
	+ " fk_user = "+userId+" and  nvl(study_team_usr_type,'D')='D') or pkg_superuser.F_Is_Superuser("+userId+", pk_study) = 1 )  " ;

	if (! StringUtil.isEmpty(searchCriteria))
	{
		//17/5/2010 - bug no 4000 fixed by BK
		String newSearchCriteria = StringUtil.replace(searchCriteria,"'","''");
		studySql = studySql + " and (lower(a.study_number) like lower('%"+newSearchCriteria.trim()+"%') or lower(a.study_title) like lower('%"+newSearchCriteria.trim()+"%') or lower(a.study_keywrds) like lower('%"+newSearchCriteria.trim()+"%')) ";
	}

//use the output of above sql to get the status data

	studySql = " select o.*, s.studystat_note ,"
				+ " (select site_name from  er_site where pk_site = s.fk_site) as site_name ,"
				+ " to_char(s.studystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT)studystat_date , F_Get_Codelstdesc(s.FK_CODELST_STUDYSTAT) status,s.FK_CODELST_STUDYSTAT "
				+ "  from "
				+ " ( " +  studySql + ")o, er_studystat s where o.pk_studystat = s.pk_studystat (+)";




//cut title at 80 char and add ...

//get status codes using the subtype. no need to execute subqueries for each status. use the following codepks for comparison

	CodelstJB cjb = new CodelstJB();
	int activeStatusCode = 0;
	int tempClosureCode = 0;

	activeStatusCode = cjb.getCodeId("studystat", "active");
	tempClosureCode = cjb.getCodeId("studystat", "temp_cls");

  System.out.println(studySql);

	String countSql = "select count(*) from (" + studySql + ")";

	   long rowsPerPage=0;
   	   long totalPages=0;
	   long rowsReturned = 0;
	   long showPages = 0;
	   long totalRows = 0;
	   long firstRec = 0;
	   long lastRec = 0;

	   String studyNumber = null;
	   String studyTitle = null;
	   String studyPhase = null;
	   String studyTarea = null;
	   String studyStatus = null;

	   String studyStatusNote = null;
	   String studyStatusDate = null;
	   String studyActualDt = null;
	   String studyTeamRght = null;
	   String studySite = null ;//JM
	   int studyId=0;
	   boolean hasMore = false;
	   boolean hasPrevious = false;
	   int statusCodePK = 0;

	   String  study_verparent = "";

		rowsPerPage =  Configuration.ROWSPERBROWSERPAGE ;
		totalPages =Configuration.PAGEPERBROWSER ;

       BrowserRows br = new BrowserRows();


	   br.getPageRows(curPage,rowsPerPage,studySql,totalPages,countSql,orderBy,orderType);
   	   rowsReturned = br.getRowReturned();
	   showPages = br.getShowPages();
	   startPage = br.getStartPage();
	   hasMore = br.getHasMore();
	   hasPrevious = br.getHasPrevious();
	   totalRows = br.getTotalRows();
	   firstRec = br.getFirstRec();
	   lastRec = br.getLastRec();

%>

<Form name="studybrowserpg" method=post onsubmit="" action="studybrowserpg.jsp?srcmenu=<%=src%>&page=1">
	<table width="100%">
	<tr class="searchbg" height="38">

		<td width="7%"><P class="defComments lhsFont"><%=LC.L_Search_AStd%><%--Search a Study*****--%></P></td>	
 		<td width="30%"><P><Input type=text name="searchCriteria" size="50" value="<%=StringUtil.htmlEncodeXss(searchCriteria)%>" ></P></td>
		<td width="15%"><P><button type="submit" onmouseover="return overlib('<%=MC.M_EntPartial_CompleteText%><%--Enter partial or complete text to search across <%=LC.Std_Study_Lower%> number, <%=LC.Std_Study_Lower%> title and keyword fields.*****--%>',CAPTION,'<%=LC.L_StdSrch_Help%><%--<%=LC.Std_Study%> Search Help*****--%>');" onmouseout="return nd();"><%=LC.L_Search%></button></P></td>
		<td width="45%" align="left">
			<A href="advStudysearchpg.jsp"> <%=LC.L_Adva_Search%><%--Advanced Search*****--%>
			</A>
		</td>
	</tr>
	</table>

		<table width=100%>
<!--		<td width=25% align="right"> <P class="defComments">Search a <%=LC.Std_Study%></P></td>
		<td class=tdDefault width=30%> <Input type=text name="searchCriteria" size="20%"> </td>
    	<td width=5% align="left">
		<Input type="image" src="../images/jpg/Go.gif" size="5%" border="0"></td> -->
<tr>
		<td width=50%>
		 <% if (!(searchCriteria.equals(""))) {%>
          <P class = "defComments"><%=MC.M_StdsMatch_SrchCriteria%><%--The following <%=LC.Std_Studies_Lower%> match your search criteria*****--%>: "<%=StringUtil.htmlEncodeXss(searchCriteria)%>"</P>
		 <%}%>
		</td>
</tr>
	<tr height="25">
		<td><P class="defComments"><%=MC.M_Std_ClkHdr%><%--<%=LC.Std_Studies%> (Click column headers to sort)*****--%></P></td>
		<td></td>
		<td class="rhsFont strongFont" align="right">
		<A href="studybrowserpg.jsp?srcmenu=<%=src%>"><img src="../jsp/images/moreInfo.png" border="0" title="<%=LC.L_View_AllStudies%>"></A>
		</td>
	</tr>
	</table>

	<Input type="hidden" name="srcmenu" value="<%=src%>">
	<Input type="hidden" name="page" value="<%=curPage%>">
	<Input type="hidden" name="orderBy" value="<%=orderBy%>">
	<Input type="hidden" name="orderType" value="<%=orderType%>">

    <table width="100%" class="outline" >
      <tr>
		<th width="9%"><%=LC.L_QuickAccess %><%--Quick Access*****--%></th>
        <th width="21%" onClick="setOrder(document.studybrowserpg,4)"><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%></th>
		<th width="21%" onClick="setOrder(document.studybrowserpg,5)"><%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%> </th><!--km-->
        <th width="20%" onClick="setOrder(document.studybrowserpg,8)"><%=LC.L_Therapeutic_Area%><%--Therapeutic Area*****--%></th>
        <!-- Rohit CCF-FIN16 -->
        <!-- <th width="12%" onClick="setOrder(document.studybrowserpg,7)">Phase</th> -->
        <th width="10%"  onClick="setOrder(document.studybrowserpg,'status')"><%=LC.L_Study_Status%><%--<%=LC.Std_Study%> Status*****--%></th>
        <th width="3%" ><%=LC.L_Delete%><%--Delete*****--%></th>

	<% if (groupName.equalsIgnoreCase("admin")) { %>
	<!-- th width="7%"></th-->	<!--km-->
	<%}%>
      </tr>


	  <!--KM-28Aug08 -->
	<script language=javascript>
	   var varViewTitle =  new Array (<%=rowsReturned%>);
	</script>

	<%
		  String mPatRight ="";

	for(int counter = 1;counter<=rowsReturned;counter++)
	{
		studyId=EJBUtil.stringToNum(br.getBValues(counter,"pk_study")) ;
		studyNumber=br.getBValues(counter,"study_number");
		studyTitle=br.getBValues(counter,"study_title");


		/*if (! StringUtil.isEmpty(studyTitle))
		{
			if (studyTitle.length() > 80)
			{
				studyTitle = studyTitle.substring(0,80);
				if (studyTitle.length() == 80)
				{
					studyTitle = studyTitle + "...";
				}
			}
		}*/


		studyPhase=br.getBValues(counter,"phase");
		studyTarea=br.getBValues(counter,"tarea");

		studyStatus=br.getBValues(counter,"status");

	    if (StringUtil.isEmpty(studyStatus))
	    {
	    	studyStatus = "...";
	    }

		statusCodePK = EJBUtil.stringToNum(br.getBValues(counter,"FK_CODELST_STUDYSTAT"));

		studyActualDt = br.getBValues(counter,"STUDY_ACTUALDT");
		studyTeamRght = br.getBValues(counter,"study_team_rights");
		//JM: 01.25.2006 for getting org name in the study status tooltips
		studySite = br.getBValues(counter,"site_name");

		if (studyTeamRght.length() >= 11 )
				{
					mPatRight = studyTeamRght.substring(iRightSeq - 1, iRightSeq);
				}
		else
				{
					mPatRight = "0";
				}



		studyStatusNote=br.getBValues(counter,"studystat_note");
		studyStatusDate=br.getBValues(counter,"studystat_date");

		if (StringUtil.isEmpty(studySite))
		{
			studySite = "-";
		}

		studyStatusNote = (studyStatusNote == null)?"-":studyStatusNote;
	    studyStatusDate = (studyStatusDate == null)?"-":studyStatusDate;
	    studyTitle = (studyTitle == null)?"-":studyTitle;
	    studyPhase = (studyPhase == null)?"-":studyPhase;
	    studyTarea = (studyTarea == null)?"-":studyTarea;

	    //GM 03/14/08 to fix CL7364 . This fix is to render studytitle which contains special characters like ' < ' and ' > '.
	    //NOTE : we are also aware that this would create problems to render korean characters. We need to find a resolution which fixes BOTH the problems

	    /*if(studyTitle!=null){
	    	studyTitle = StringUtil.htmlEncode(studyTitle);
		}*/
		if ((counter%2)==0) {
 %>
      <tr class="browserEvenRow">
        <%
		}
		else{
  %>
      <tr class="browserOddRow">
        <%
		}

	studyStatusNote = studyStatusNote.replace('\n',' ');
	studyStatusNote = studyStatusNote.replace('\r',' ');
	//km-to fix Bug2480

	studyStatusNote=StringUtil.escapeSpecialCharJS(studyStatusNote);
  %>
  <td align="center">
  <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0"><tr>
  <td width="25%" style="border: 0px"><a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=studyId%>"><img class="studyMenuPop" id="<%=studyId%>" src="../images/jpg/study.gif" alt="<%=LC.L_StdSummary%><%--<%=LC.Std_Study%> Summary*****--%>" border=0 title="<%=LC.L_StdSummary%><%--<%=LC.Std_Study%> Summary*****--%>"/></a>
  </td>
  <td width="25%" style="border: 0px">
  <%if(studyActualDt!=null && EJBUtil.stringToNum(mPatRight) > 0){%>
	<a href="studypatients.jsp?srcmenu=tdmenubaritem5&srcmenu=<%=src%>&studyId=<%=studyId%>&patid=&patstatus=&selectedTab=2"><img src="../images/jpg/patient.gif" alt="<%=LC.L_Patient_Enrlmnt%><%--<%=LC.Pat_Patient%> Management - Enrolled <%=LC.Pat_Patients%>*****--%>" border=0 title="<%=LC.L_Patient_Enrlmnt%><%--<%=LC.Pat_Patient%> Management - Enrolled <%=LC.Pat_Patients%>*****--%>"/></a>
	<%}%>
  </td>
  <td width="25%" style="border: 0px">
	<a href="budgetbrowserpg.jsp?srcmenu=tdmenubaritem6&studyId=<%=studyId%>&calledFrom=study"><img src="../images/jpg/Budget.gif" title="<%=LC.L_Budgets%><%--Budgets*****--%>" border=0 ></a>
  </td>
  <td width="25%" style="border: 0px">
  	<%
	if (eventassocB.getStudyProts(studyId)!=null && eventassocB.getStudyProts(studyId).getEvent_ids().size() > 0){
	%>
	<a href="studyprotocols.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=7&studyId=<%=studyId%>"><img src="../images/jpg/Calendar.gif" title="<%=LC.L_Calendars%><%--Calendars*****--%>" border=0 ></a>
	<%
	}
	%>
   </td>
 </tr></table>	
	</td>
		<!--Start for S-22306 Enhancement : Raviesh -->
		<td><%if(studyNumber.length()>50){%>
		<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
		<td style="border-color:; border-width: 0;"><%=studyNumber.substring(0,50)%>&nbsp;<IMG width="16" height="16" src ='./images/More.png' class='asIsImage' border=0 onMouseOver="return overlib('<%=studyNumber%>',CAPTION,'<%=LC.L_Study_Number%><%--<%=LC.L_Study_Number%> Study Number*****--%>');" onMouseOut="return nd();"/></td>
		</tr></table>
		<%} else {%>
		&nbsp;&nbsp;<%=studyNumber%>
		<%}%></td>

		<%
		//Modified by Manimaran to display full title in mouseover.
		String viewStudyTitle = StringUtil.escapeSpecialCharJS(studyTitle);
		%>
		<script language=javascript>
			 varViewTitle[<%=counter%>] = htmlEncode('<%=viewStudyTitle%>');
		</script>
		
		<td><%if(studyTitle.length()>50){%>
		<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
		<td style="border-color:; border-width: 0;"><%=studyTitle.substring(0,50)%>&nbsp;<IMG src ='./images/More.png' class='asIsImage' border=0 onmouseover="return overlib(varViewTitle[<%=counter%>],CAPTION,'<%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>');" onmouseout="return nd();"/></td>
		</tr></table>
		<%}else{%>
		&nbsp;&nbsp;<%=studyTitle%>
		<%}%></td>
		<!--End for S-22306 Enhancement : Raviesh -->
		
        <td> <%=studyTarea%> </td>
        <!-- Rohit CCF-FIN16 -->
         <%--  <td> <%=studyPhase%> </td> --%>
        <td> <% if (statusCodePK == tempClosureCode) { %>
				<A href="studystatusbrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=3&studyId=<%=studyId%>" onmouseover="return overlib('<%="<tr><td><font size=2>"+LC.L_Note/*Note*****/+":</font><font size=1> "+studyStatusNote+"</font></td></tr><tr><td><font size=2>"+LC.L_Organization/*Organization*****/+":</font><font size=1> " +studySite+"</font></td></tr>"%>',CAPTION,'<%=studyStatusDate%>');" onmouseout="return nd();"> <FONT class="Red"><%=studyStatus%></FONT> </A>
			<%} else if ( statusCodePK == activeStatusCode ) {%>
				<A href="studystatusbrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=3&studyId=<%=studyId%>" onmouseover="return overlib('<%="<tr><td><font size=2>"+LC.L_Note/*Note*****/+":</font><font size=1> "+studyStatusNote+"</font></td></tr><tr><td><font size=2>"+LC.L_Organization/*Organization*****/+":</font><font size=1> " +studySite+"</font></td></tr>"%>',CAPTION,'<%=studyStatusDate%>');" onmouseout="return nd();">  <FONT class="Green"><%=studyStatus%></FONT> </A>
			 	<!--Added by sudhir for s-22307 enhancement on 12-03-2012-->		
			<%} else if( studyStatus.equalsIgnoreCase("...") ) {%>
				<div align="center"><A href="studystatusbrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=3&studyId=<%=studyId%>" onMouseOver="return overlib('<%=MC.M_Status_Cannot_Be_Displayed%>',CAPTION,'<%=LC.L_Study_Status%>');" onMouseOut="return nd();"><%//=studyStatusDate%><img src="../images/help.png" border="0"/></A></div>
				<!--Added by sudhir for s-22307 enhancement on 12-03-2012-->		
			<%} else {%>
				<A href="studystatusbrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=3&studyId=<%=studyId%>" onmouseover="return overlib('<%="<tr><td><font size=2>"+LC.L_Note/*Note*****/+":</font><font size=1> "+studyStatusNote+"</font></td></tr><tr><td><font size=2>"+LC.L_Organization/*Organization*****/+":</font><font size=1> " +studySite+"</font></td></tr>"%>',CAPTION,'<%=studyStatusDate%>');" onmouseout="return nd();">  <%=studyStatus%> </A>
			<%}%>
		</td>
		
		<%   if (groupName.equalsIgnoreCase("admin")) {
		 //Added by Manimaran to fix Bug 2323-Shouldn't allow to remove Study with has already linked Patient.
			PatStudyStatDao psdao=new PatStudyStatDao();
            Integer stid=new Integer(studyId);
           	psdao.getStudyPatients(stid.toString());
			int cnt=psdao.getCRows();
		if (cnt == 0)
		{
		%><!-- Added by gopu to fix the bug #2373 -->
		<td align="center">
		<!--km -to fix Bug2408 -->
		<A href="deletepatstudy.jsp?srcmenu=<%=src%>&studyNo=<%=StringUtil.encodeString(studyNumber)%>&studyId=<%=studyId%>&delId=study" onClick="return confirmBox('<%=studyNumber%>')"> <img src="../images/delete.png" border="0" align="left" title="<%=LC.L_Delete%>"/></A></td> <!--km-->
		<%
		}
		else
		{

		%>
		<td align="center">
		<A href="#" onClick="alert('<%=MC.M_StdLnkToPat_CntRem%><%--<%=LC.Std_Study%> is already linked to <%=LC.Pat_Patient%> and cannot be removed*****--%>')"><img src="../images/delete.png" border="0" align="left" title="<%=LC.L_Delete%>" alt="<%=LC.L_Delete%>"/></A></td>
		<%}}%>

      </tr>
      <%
		}
%>
    </table>

	<!-- Bug#9751 16-May-2012 Ankit -->
	<table width="98%" cellspacing="0" cellpadding="0" class="midalign">
	<tr>
	<td>
	<% if (totalRows > 0) { Object[] arguments = {firstRec,lastRec,totalRows}; %>
		<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%-- <font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
	<%} else {%>
		<font class="recNumber"><%=MC.M_NoRecordsFound%><%--No Records Found*****--%></font>
	<%}%>
	</td>
	</tr>
	</table>

	<div align="center" class="midalign">
<%
	if (curPage==1) startPage=1;

    for (int count = 1; count <= showPages;count++)
	{
     cntr = (startPage - 1) + count;

	if ((count == 1) && (hasPrevious))
	{
    %>
  	<A href="studybrowserpg.jsp?searchCriteria=<%=StringUtil.htmlEncodeXss(searchCriteria)%>&srcmenu=<%=src%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&pageView=<%=curPageView%>&orderByView=<%=orderByView%>&orderTypeView=<%=orderTypeView%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;
	<%
  	}
	%>
	<%

	 if (curPage  == cntr)
	 {
     %>
		<FONT class = "pageNumber"><%= cntr %></Font>
       <%
       }
      else
        {
       %>

	   <A href="studybrowserpg.jsp?searchCriteria=<%=StringUtil.htmlEncodeXss(searchCriteria)%>&srcmenu=<%=src%>&page=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&pageView=<%=curPageView%>&orderByView=<%=orderByView%>&orderTypeView=<%=orderTypeView%>"><%= cntr%></A>
       <%
    	}
	 %>
	<%
	  }

	if (hasMore)
	{
   %>
  	&nbsp;&nbsp;&nbsp;&nbsp;<A href="studybrowserpg.jsp?searchCriteria=<%=StringUtil.htmlEncodeXss(searchCriteria)%>&srcmenu=<%=src%>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&pageView=<%=curPageView%>&orderByView=<%=orderByView%>&orderTypeView=<%=orderTypeView%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
	<%
  	}
	%>
  </div>

<%

    	String replSearchCriteria = StringUtil.replace(searchCriteria,"'","''");

	   String studyViewSql = "select pk_studyview, STUDYVIEW_NUM,  case when length(STUDYVIEW_TITLE) > 80 then substr(STUDYVIEW_TITLE,1,80) || '...'  else STUDYVIEW_TITLE  end as STUDYVIEW_TITLE, lower(STUDYVIEW_NUM) STUDYVIEW_NUM_SORT,  lower(case when length(STUDYVIEW_TITLE) > 80 then substr(STUDYVIEW_TITLE,1,80) || '...'  else STUDYVIEW_TITLE  end ) as STUDYVIEW_TITLE_SORT from er_studyview where  STUDYVIEW_VERNO is null and fk_user = " + userId +" and (lower(studyview_num) like lower('%"+replSearchCriteria+"%') or lower(studyview_title) like lower('%"+replSearchCriteria+"%'))";

	   String countViewSql = "select count(*) from er_studyview where STUDYVIEW_VERNO is null and fk_user = " + userId +" and (lower(studyview_num) like lower('%"+replSearchCriteria+"%') or lower(studyview_title) like lower('%"+replSearchCriteria+"%'))";

	   long rowsPerPageView=0;
   	   long totalPagesView=0;

   	   rowsPerPageView = rowsPerPage;
	   totalPagesView = totalPages;

	   long rowsReturnedView = 0;
	   long showPagesView = 0;

	   String studyNumberView = null;
	   String studyTitleView = null;
	   int studyIdView=0;
	   boolean hasMoreView = false;
	   boolean hasPreviousView = false;
	   long totalRowsView = 0;
	   long firstRecView = 0;
	   long lastRecView = 0;

       BrowserRows brView = new BrowserRows();

	   brView.getPageRows(curPageView,rowsPerPageView,studyViewSql,totalPagesView,countViewSql,orderByView,orderTypeView);
   	   rowsReturnedView = brView.getRowReturned();
	   showPagesView = brView.getShowPages();
	   startPageView = brView.getStartPage();
	   hasMoreView = brView.getHasMore();
	   hasPreviousView = brView.getHasPrevious();
	   totalRowsView = brView.getTotalRows();
	   firstRecView = brView.getFirstRec();
	   lastRecView = brView.getLastRec();
%>
	<table width="100%">
	<tr>
		<td><P class="defComments"><%=MC.M_Std_WithViewAces%><%--<%=LC.Std_Studies%> with View access*****--%></P></td>
	</tr>
	</table>
	<Input type="hidden" name="pageView" value="<%=curPageView%>">
	<Input type="hidden" name="orderByView" value="<%=orderByView%>">
	<Input type="hidden" name="orderTypeView" value="<%=orderTypeView%>">

    <table width="100%" class="outline" >
      <tr>
        <th width="20%" onClick="setOrderView(document.studybrowserpg,4)"><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%></th>
        <th width="70%" onClick="setOrderView(document.studybrowserpg,5)"> <%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%> </th>
      </tr>

      <%
    for(int counter = 1;counter<=rowsReturnedView;counter++)
	{
		studyIdView=EJBUtil.stringToNum(brView.getBValues(counter,"pk_studyview")) ;
		studyNumberView=brView.getBValues(counter,"STUDYVIEW_NUM");
		studyTitleView=brView.getBValues(counter,"STUDYVIEW_TITLE");
		if ((counter%2)==0) {
  %>
      <tr class="browserEvenRow">
        <%
		}
		else{
  %>
      <tr class="browserOddRow">
        <%
		}
  %>
        <td> <A HREF = '#' onClick='opensvwin(<%=studyIdView%>)'><%=studyNumberView%> </A> </td>
        <td> <%=studyTitleView%> </td>
      </tr>
      <%
		}
%>
    </table>
	<!-- Bug#9751 16-May-2012 Ankit -->
	<table width="98%" cellspacing="0" cellpadding="0" class="midalign">
	<tr>
	<td>
	<% if (totalRowsView > 0) { Object[] arguments = {firstRecView,lastRecView,totalRowsView}; %>
		<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%-- <font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
	<%} else {%>
		<font class="recNumber"><%=MC.M_NoRecordsFound%><%--No Records Found*****--%></font>
	<%}%>
	</td>
	</tr>
	</table>

	<div align="center" class="midalign">
	<%
	if (curPageView==1) startPageView=1;

    for (int count = 1; count <= showPagesView;count++)
	{
     cntrView = (startPageView - 1) + count;

	if ((count == 1) && (hasPreviousView))
	{
    %>
  	<A href="studybrowserpg.jsp?searchCriteria=<%=StringUtil.htmlEncodeXss(searchCriteria)%>&srcmenu=<%=src%>&pageView=<%=cntrView-1%>&orderByView=<%=orderByView%>&orderTypeView=<%=orderTypeView%>&page=<%=curPage%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPagesView%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;
	<%
  	}
	 if (curPageView == cntrView)
	 {
     %>
		<FONT class = "pageNumber"><%= cntrView %></Font>
       <%
       }
      else
        {
       %>
	   <A href="studybrowserpg.jsp?searchCriteria=<%=StringUtil.htmlEncodeXss(searchCriteria)%>&srcmenu=<%=src%>&pageView=<%=cntrView%>&orderByView=<%=orderByView%>&orderTypeView=<%=orderTypeView%>&page=<%=curPage%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>"><%= cntrView%></A>
       <%
    	}
	  }

	if (hasMoreView)
	{
    %>
  	&nbsp;&nbsp;&nbsp;&nbsp;<A href="studybrowserpg.jsp?searchCriteria=<%=StringUtil.htmlEncodeXss(searchCriteria)%>&srcmenu=<%=src%>&pageView=<%=cntrView+1%>&orderByView=<%=orderByView%>&orderTypeView=<%=orderTypeView%>&page=<%=curPage%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>">< <%=LC.L_Next%><%--Next*****--%><%=totalPagesView%>></A>
	<%
  	}
	%>
	</div>
  </Form>

	<%
	} //end of if body for page right
	else{
%>

  <jsp:include page="accessdenied.jsp" flush="true"/>
<%
	} //end of else body for page right
} //end of if body for session
else {
%>

	<jsp:include page="timeout.html" flush="true"/>
<%}
%>
<br>
<div>

    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>

<DIV class="mainMenu" id = "emenu">

    <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>

</body>
</html>
