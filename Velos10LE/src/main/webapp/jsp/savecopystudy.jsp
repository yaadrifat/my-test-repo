<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title><%=MC.M_CopyExisting_StdPcol%><%--Copy an Existing <%=LC.Std_Study%> Protocol*****--%></title>

<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>


<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB"%>





<% String src;
	src= request.getParameter("srcmenu");
%>

<body>

  <%

	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))

	{

	    String usr = null;
		usr = (String) tSession.getValue("userId");
	    String ipAdd = (String) tSession.getValue("ipAdd");

		String uName = (String) tSession.getValue("userName");
		String accId = (String) tSession.getValue("accountId");
		int accountId = EJBUtil.stringToNum(accId);

	  	String fromstudyId = request.getParameter("fromstudyId");
	  	String studymode = request.getParameter("studymode");
		String from = request.getParameter("fromMode");

		String eSign = request.getParameter("eSign");
		String oldESign = (String) tSession.getValue("eSign");
 	    String newStudyNum = request.getParameter("newStudyNum");
		String newTitle = request.getParameter("studyTitle");
		String dataManager = request.getParameter("dataManager");
		String dataManagerName = request.getParameter("dataManagerName");
		String oldStudy = request.getParameter("oldStudy");

		String[] studyVer = null;
		String[] studyCal = null;
		String[] studyTx = null; //JM
		String[] checkForms = null; //JM
		String copyStat;
		String copyTeam;
		String checkdict;

		int copyStatFlag = 0;
		int copyTeamFlag = 0;
		int copyDictFlag = 0;



			if(!oldESign.equals(eSign))
			{

%>
				 <jsp:include page="incorrectesign.jsp" flush="true"/>
<%

			}
			else
			{

				studyVer = 	request.getParameterValues("checkver");
				studyCal = 	request.getParameterValues("checkcal");

				copyStat = 	request.getParameter("checkstat");
				copyTeam = 	request.getParameter("checkteam");

				checkdict = request.getParameter("checkdict");//JM
				studyTx = request.getParameterValues("checktx"); //JM
				checkForms = request.getParameterValues("checkforms");//JM

				if (copyStat == null)
				{
					copyStatFlag = 0;
				}
				else if (copyStat.equals("on"))
				{
					copyStatFlag = 1;
				}

				if (copyTeam == null)
				{
					copyTeamFlag = 0;
				}
				else if (copyTeam.equals("on"))
				{
					copyTeamFlag = 1;
				}

				//JM:
				if (checkdict == null)
				{
					copyDictFlag = 0;
				}
				else if (checkdict.equals("on"))
				{
					copyDictFlag = 1;
				}

				//StudyDao std = new StudyDao ();
				int newstudyid = 0;
//JM: 04AUG2006 Blocked for Enh: #S5
//				newstudyid = studyB.copyStudySelective(EJBUtil.stringToNum(oldStudy),copyStatFlag, copyTeamFlag, studyVer, studyCal,newStudyNum, EJBUtil.stringToNum(dataManager),newTitle ,EJBUtil.stringToNum(usr),ipAdd);
				newstudyid = studyB.copyStudySelective(EJBUtil.stringToNum(oldStudy),copyStatFlag, copyTeamFlag, copyDictFlag, studyVer, studyCal, studyTx,checkForms, newStudyNum, EJBUtil.stringToNum(dataManager),newTitle ,EJBUtil.stringToNum(usr),ipAdd);

				if (newstudyid >= 0)
				{
				%>
				<br>
				<br>
				<br>
				<br>
				<br>
				<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=study.jsp?srcmenu=<%=src%>&selectedTab=1&mode=M&studyId=<%=newstudyid%>">

				<%
				}

			} // for e-sign

	} else {  //else of if body for session

%>

  <jsp:include page="timeout.html" flush="true"/>

  <%

}


%>



</body>

</html>





