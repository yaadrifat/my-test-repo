<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<%
boolean isIrb = "irb_upload_tab".equals(request.getParameter("selectedTab")) ? true : false;
String includeTabsJsp = isIrb ? ("LIND".equals(CFG.EIRB_MODE) ? "ecompNewTabs.jsp" : "irbnewtabs.jsp") : "studytabs.jsp";
if (isIrb) {
%>
<title><%=MC.M_ResComp_NewUpldDocu%><%--Research Compliance >> New Application >> Upload Documents*****--%></title>
<% } else { %>
<title><%=MC.M_Std_VerHist%><%--Study >> Version History*****--%></title>
<% } %>


<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<% String src;
src= request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<SCRIPT language="javascript">
</SCRIPT>
<body>
<br>
<%
HttpSession tSession = request.getSession(true);
String tab = request.getParameter("selectedTab");
String studyId = (String) tSession.getValue("studyId");
String from = request.getParameter("from");
String verNumber = StringUtil.decodeString(request.getParameter("verNumber"));//JM: 031006


%>

<DIV class="BrowserTopn" id = "div1">
<jsp:include page="<%=includeTabsJsp%>" flush="true">
<jsp:param name="verNumber" value="<%=verNumber%>"/>



<jsp:param name="from" value="<%=from%>"/>

<jsp:param name="selectedTab" value="<%=tab%>"/>
<jsp:param name="studyId" value="<%=studyId%>"/>
</jsp:include>
</DIV>
<DIV class="BrowserBotN BrowserBotN_S_3" id="div1">
<jsp:useBean id="studyVerB" scope="request" class="com.velos.eres.web.studyVer.StudyVerJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<%@ page language = "java" import = "com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
	<%

	if (sessionmaint.isValidSession(tSession))
	{
		String uName = (String) tSession.getValue("userName");
		String modulePk = request.getParameter("modulePk");
		String fromjsp = request.getParameter("fromjsp");



		int pageRight = 0;
		StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
			if ((stdRights.getFtrRights().size()) == 0){
		 		pageRight= 0;
			   }else{
			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYVER"));
	   		}

		String showDel = "";

		if (pageRight < 6)
		{
			showDel = "false";
		}
		else
		{
			showDel = "true";
		}

	%>

		<%

	    if ( pageRight > 0 )
		{%>
		     <p class="defComments"><%=LC.L_Version_Number%><%--Version Number*****--%>: <%=verNumber%> &nbsp;&nbsp;
	  		 <jsp:include page="statusHistory.jsp" flush="true">
     		 <jsp:param name="modulePk" value="<%=modulePk%>"/>
			 <jsp:param name="moduleTable" value="er_studyver"/>
			 <jsp:param name="srcmenu" value="<%=src%>"/>
			 <jsp:param name="selectedTab" value="<%=tab%>"/>
			 <jsp:param name="fromjsp" value="<%=fromjsp%>"/>
			 <jsp:param name="pageRight" value="<%=pageRight%>"/>
			 <jsp:param name="showDeleteLink" value="<%=showDel%>"/>
			 </jsp:include>

<%
		} //end of if body for page right
		else
		{
		%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
		<%
		}//end of else body for page right%>

			<%

	}//end of if body for session
	else
	{
	%>
		<jsp:include page="timeout.html" flush="true"/>
	<%
	}
	%>
	<div>
    	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</div>
<div class ="mainMenu" id = "emenu" >
  <jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>

