<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="com.velos.eres.service.util.LC"%>
<%@page import="com.velos.eres.service.util.MC"%>
<%@page import="com.velos.eres.business.common.StudyStatusDao"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@page import="com.velos.eres.business.common.TeamDao"%>
<%@page import="com.velos.eres.service.util.StringUtil"%>
<%@page import="com.velos.eres.web.objectSettings.ObjectSettingsCache"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.velos.eres.service.util.EJBUtil"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@page import="com.velos.eres.web.studyRights.StudyRightsJB"%>

<%@page import="com.velos.eres.web.objectSettings.ObjectSettings"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyStatB" scope="request" class="com.velos.eres.web.studyStatus.StudyStatusJB"/>
<jsp:useBean id="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="studyTeam" scope="request" class="com.velos.eres.web.team.TeamJB"/>
<jsp:useBean id="linkedformsdao" scope="request"	class="com.velos.eres.business.common.LinkedFormsDao" />
<jsp:useBean id="linkedformsB" scope="request"	class="com.velos.eres.web.linkedForms.LinkedFormsJB" />

<%

request.setCharacterEncoding("UTF-8");
HttpSession tSession = request.getSession(true);

String mode="N";
String studyId="";
String study="";
String studyNo="";
String verNumber = ""  ;
String userId = "";
String userIdFromSession = "";
String studyNumber = "";
String acc = (String) tSession.getValue("accountId");
%>
<%!
/////Commonly used variables
int itemCount = 0;
int maxItemsInColumn = 12;
String blankMenuItem = "<div class=\"context-menu-item\"><div style=\"\" class=\"context-menu-item-inner\">&nbsp;</div></div>";
/////
private StringBuffer addNewColumn(int arrayIndx, int tabCount) {
	StringBuffer sbNewColumn = new StringBuffer();
	itemCount++;

	//Last Element
	if (arrayIndx + 1 == tabCount){
		return sbNewColumn;
	}
	// Not a Last Element 
	if (itemCount % maxItemsInColumn == 0){
		sbNewColumn.append("</div></td>")
			.append("<td width='[colPercentWidth]' NOWRAP>")
			.append("<div class=\"context-menu context-menu-theme-default\">")
			.append(blankMenuItem);
		itemCount++;
	}
	return sbNewColumn;
}
%>
<%
if (sessionmaint.isValidSession(tSession))
{
	//IMP: Resetting itemCount for every single call
	itemCount = 0;
	tSession.removeAttribute("submissionType");

	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
	int grpRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));
	int saveAstemplateRight = 0; //in new mode no right for template
	
	
	userIdFromSession= (String) tSession.getValue("userId");

	studyId = request.getParameter("studyId");
	tSession.setAttribute("studyId",studyId);
	study = (String) tSession.getValue("studyId");
	
	StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");

	studyNo = (String) tSession.getValue("studyNo");
	verNumber = request.getParameter("verNumber");
	userId = (String) tSession.getValue("userId");
	

	ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
	ArrayList tabList = objCache.getAccountObjects(EJBUtil.stringToNum(acc), "study_tab");

	if (EJBUtil.stringToNum(studyId) > 0) mode="M";
	
	 tSession.setAttribute("studyId",studyId);
		studyB.setId(EJBUtil.stringToNum(studyId));
		studyB.getStudyDetails();
		studyNumber = studyB.getStudyNumber();
		tSession.setAttribute("studyNo",studyNumber);
		studyNo = studyNumber;

		TeamDao teamDao = new TeamDao();
		teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));

		ArrayList tId ;
		tId = new ArrayList();
		tId = teamDao.getTeamIds();

		stdRights = new StudyRightsJB();

		if (tId.size() <=0)
		{
			tSession.putValue("studyRights",stdRights);
		} else {

			stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));

			ArrayList teamRights ;
			teamRights  = new ArrayList();
			teamRights = teamDao.getTeamRights();

			stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
			stdRights.loadStudyRights();
			tSession.putValue("studyRights",stdRights);
		}
		if (grpRight >0) {
			
			// To check for the account level rights
			String modRight = (String) tSession.getValue("modRight");
			 int patProfileSeq = 0, formLibSeq = 0;
			 acmod.getControlValues("module");
			 ArrayList acmodfeature =  acmod.getCValue();
			 ArrayList acmodftrSeq = acmod.getCSeq();
			 char formLibAppRight = '0';

			 patProfileSeq = acmodfeature.indexOf("MODPATPROF");
			 formLibSeq = acmodfeature.indexOf("MODFORMS");
			 formLibSeq = ((Integer) acmodftrSeq.get(formLibSeq)).intValue();

			 formLibAppRight = modRight.charAt(formLibSeq - 1);

		int pageRight = 0;
		int formRights= 0;
		
		int studyIds = EJBUtil.stringToNum(studyId);
		linkedformsdao=linkedformsB.getStudyLinkedForms(studyIds);
		ArrayList formnames= linkedformsdao.getFormName();
		int formLen=formnames.size();
		
		
		int allOrg=0;
		int accountId = EJBUtil.stringToNum((String) tSession.getValue("accountId"));
		   StudyStatusDao currStudyStatDao = studyStatB.getStudyStatusDesc(studyIds, allOrg, EJBUtil.stringToNum(userIdFromSession), accountId);
		   ArrayList currentStats =  currStudyStatDao.getCurrentStats();
		   ArrayList currStatusIdLst = currStudyStatDao.getIds();
		   int len = currStatusIdLst.size();
		   String currentStatId="";
		   String currentVal="";
		   for (int i=0;i<len;i++) {
		        currentVal = (currentStats.get(i) == null)?"":(currentStats.get(i)).toString();
			if ( currentVal.equals("1")) {
				 currentStatId = (String)currStatusIdLst.get(i).toString();
		        }
 		   }
		
		   StudyStatusDao studyStatDao = studyStatB.getStudyStatusDesc(studyIds, allOrg, EJBUtil.stringToNum(userIdFromSession), accountId);
		   String lastStudyStatusId = null;
		   ArrayList studyStatusIdLst = studyStatDao.getIds();
		   ArrayList studyStatEndDates = studyStatDao.getStatEndDates();
		   len = studyStatusIdLst.size();
		   for(int counter = 0;counter<len;counter++)
			{
			   if  (   studyStatEndDates.get(counter)   == null )
				{
						lastStudyStatusId = ((Integer)studyStatusIdLst.get(counter)).toString() ;
				}
			}
		

		String myStudyNumber="";
		if(studyNo.length() >20)
		{
			String titleExceedingLimit=studyNo.substring(0,20);
			studyNo =studyNo.replace("\\","\\\\");
			myStudyNumber=titleExceedingLimit+"&nbsp;<img src=\"./images/More.png\"  class=\"asIsImage\" border=\"0\" onmouseover=\"return overlib('"+studyNo+"',CAPTION,'"+LC.Std_Study+"' ,LEFT , ABOVE);\" onmouseout=\"return nd();\" ></img>";
		}else{			
			myStudyNumber=studyNo;
		}

		int tabCount = tabList.size();
		StringBuilder menuOut= new StringBuilder();
		menuOut.append("<table id=\"contextMenuTable\" cellspacing=\"0\" cellpadding=\"0\" class=\"menuTable\" style=\"position: absolute; z-index: 9999;\"><tbody><tr><td width='[colPercentWidth]' NOWRAP>")
		.append("<div class=\"context-menu context-menu-theme-default\">")
		.append("<div title=\"\" class=\"context-menu-title\">")
		.append("<div class=\"context-menu-title-inner\">&nbsp;&nbsp;<b>"+LC.L_Study+":&nbsp;").append(myStudyNumber).append("</b>&nbsp;&nbsp;</div></div>")
		.append(addNewColumn(-1, tabCount));

		for (int iX=0; iX<tabCount; iX++) {
		ObjectSettings settings = (ObjectSettings)tabList.get(iX);
		HashMap menuOption = new HashMap();


		if ("0".equals(settings.getObjVisible())) {
			continue;
		}

		boolean showThisTab = false;
		if ("1".equals(settings.getObjSubType())) {
			if (mode.equals("M")) {
				if ((stdRights.getFtrRights().size()) == 0){
					pageRight= 0;
				}else{
					pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYSUM"));
				}
			}
			else
			{
	   			pageRight = grpRight;
			}

			if (((mode.equals("M") && pageRight >=4) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 ))) && ((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 ))))
			{
				
				String href="<div id=\"actionValue\" style=\"display:none\">study.jsp?mode="+mode+"&srcmenu=tdmenubaritem3&selectedTab=1&studyId="+studyId+"</div><div id=\"displayValue\"><b>"+settings.getObjDispTxt()+"</b></div>";
				menuOut.append("<div title=\"\" id=\"noSubMenu\" class=\"context-menu-item\">")
				.append("<div style=\"\" class=\"context-menu-item-inner\">")
				.append(href)
				.append("</div></div>")
				.append(addNewColumn(iX, tabCount));
			}
		}
		else if ("2".equals(settings.getObjSubType())) {
			if (mode.equals("N"))
			{
					pageRight= 7;
			}
			else
			{
				if ((stdRights.getFtrRights().size()) == 0){
			 		pageRight= 0;
				}else{
					pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYVER"));
		   		}
			}
			if ((pageRight > 0) && ((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 )))) {
				
				String href="<div id=\"actionValue\" style=\"display:none\">studyVerBrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=2&mode=N&studyId="+studyId+"</div><div id=\"displayValue\"><b>"+settings.getObjDispTxt()+"</b></div>";
				menuOut.append("<div title=\"\" id=\"noSubMenu\" class=\"context-menu-item\">")
				.append("<div style=\"\" class=\"context-menu-item-inner\">")
				.append(href)
				.append("</div></div>")
				.append(addNewColumn(iX, tabCount));
			}
		}
		else if ("10".equals(settings.getObjSubType())) {

			if (mode.equals("N"))
			{
					pageRight= 7;
			}
			else
			{
				if ((stdRights.getFtrRights().size()) == 0) {
					   pageRight = 0;
		        } else {
		               pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));

		        }
		    }

			if ((pageRight > 0) && ((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 )))) {
				
				String href="<div id=\"actionValue\" style=\"display:none\">studyadmincal.jsp?srcmenu=tdmenubaritem3&selectedTab=10&studyId="+studyId+"</div><div id=\"displayValue\"><b>"+settings.getObjDispTxt()+"</b></div>";
				menuOut.append("<div title=\"\" id=\"noSubMenu\" class=\"context-menu-item\">")
				.append("<div style=\"\" class=\"context-menu-item-inner\">")
				.append(href)
				.append("</div></div>")
				.append(addNewColumn(iX, tabCount));
			}
		}
		else if ("7".equals(settings.getObjSubType())) {
			if (mode.equals("N"))
			{
					pageRight= 7;
					formRights = 7;
			}
			else
			{
				if ((stdRights.getFtrRights().size()) == 0){
				 	pageRight= 0;
				}else{
					pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
				}

				if ((stdRights.getFtrRights().size()) == 0){
				 	formRights= 0;
				}else{
		        			formRights = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
			   	}
			}

			  if (((pageRight > 0 ) || (formRights >0)) && ((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 ))))  {
				 	
					/*Header item*/
					String href="<div id=\"actionValue\" style=\"display:none\">studyprotocols.jsp?mode="+mode+"&srcmenu=tdmenubaritem3&selectedTab=7&studyId="+studyId+"</div><div id=\"displayValue\"><b>"+settings.getObjDispTxt()+"</b></div>";
				  	menuOut.append("<div title=\"\" id=\"openStudySubMenu\" class=\"context-menu-item\">")
					.append("<div style=\"\" class=\"context-menu-item-inner\">")
					.append(href)
					.append("</div>").append("</div>")
					.append(addNewColumn(iX, tabCount));

				  	StringBuffer subMenu= new StringBuffer();
				  	String parentValue="<div id=\"parentValue\" style=\"display:none\">studyprotocols.jsp?mode="+mode+"&srcmenu=tdmenubaritem3&selectedTab=7&studyId="+studyId+"</div>";
				  	String subMenuhref="";
				  	/*Update Multiple Schedules*/
				  	
				  	subMenuhref="<div id=\"accessRights\" style=\"display:none\">N||"+pageRight+"</div><div id=\"popValue\" style=\"display:none\">updatemultschedules.jsp?srcmenu=tdmenubaritem3&from=initial</div><div id=\"displayValue\" style=\"margin-left:10px;\">"+LC.L_Update_MultiSch+"</div>"+parentValue;
				  	subMenu.append("<div title=\"\" class=\"context-menu-item\">")
					.append("<div style=\"\" class=\"context-menu-item-inner\">")
					.append(subMenuhref)
					.append("</div></div>")
					.append(addNewColumn(iX, tabCount));
				  	
				  	/*Display and Sequencing Options*/
				  	subMenuhref="<div id=\"accessRights\" style=\"display:none\">E||"+formRights+"</div><div id=\"popValue\" style=\"display:none\">formhideseq.jsp?studyId="+studyId+"&calledFrom=S</div><div id=\"displayValue\" style=\"margin-left:10px;\">"+MC.M_Disp_SeqOpts+"</div>"+parentValue;
				  	subMenu.append("<div title=\"\" class=\"context-menu-item\">")
					.append("<div style=\"\" class=\"context-menu-item-inner\">")
					.append(subMenuhref)
					.append("</div></div>")
					.append(addNewColumn(iX, tabCount));

				  	/*Select Form from Library*/
				  	subMenuhref="<div id=\"accessRights\" style=\"display:none\">N||"+formRights+"</div><div id=\"popValue\" style=\"display:none\">linkformstostudyacc.jsp?lnkStudyId="+studyId+"&linkFrom=S</div><div id=\"displayValue\" style=\"margin-left:10px;\">"+MC.M_Selc_FrmYourLib+"</div>"+parentValue;
				  	subMenu.append("<div title=\"\" class=\"context-menu-item\">")
					.append("<div style=\"\" class=\"context-menu-item-inner\">")
					.append(subMenuhref)
					.append("</div></div>")
					.append(addNewColumn(iX, tabCount));

				  	if(formLen>0){
						/*Copy an Existing Form*/
					  	subMenuhref="<div id=\"accessRights\" style=\"display:none\">N||"+formRights+"</div><div id=\"popValue\" style=\"display:none\">copyFormForStudy.jsp?studyId="+studyId+"</div><div id=\"displayValue\" style=\"margin-left:10px;\">"+MC.M_Copy_ExistingFrm+"</div>"+parentValue;
					  	subMenu.append("<div title=\"\" class=\"context-menu-item\">")
						.append("<div style=\"\" class=\"context-menu-item-inner\">")
						.append(subMenuhref)
						.append("</div></div>")
						.append(addNewColumn(iX, tabCount));
				  	}
				  	
				  	menuOut.append(subMenu);
			  }
		}
		else if ("11".equals(settings.getObjSubType())) {
			if (mode.equals("M")) {
				if ((stdRights.getFtrRights().size()) == 0){
					pageRight= 0;
				}else{
					pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
				}
			}

			if (((mode.equals("M") && pageRight >=4) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 ))) && ((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 ))))
			{ 
				String href="<div id=\"actionValue\" style=\"display:none\">combinedBudget.jsp?mode="+mode+"&srcmenu=tdmenubaritem3&selectedTab=11&studyId="+studyId+"</div><div id=\"displayValue\"><b>"+settings.getObjDispTxt()+"</b></div>";
				menuOut.append("<div title=\"\" id=\"noSubMenu\" class=\"context-menu-item\">")
				.append("<div style=\"\" class=\"context-menu-item-inner\">")
				.append(href)
				.append("</div></div>")
				.append(addNewColumn(iX, tabCount));
			}
		}
		else if ("12".equals(settings.getObjSubType())) {
			int grp_milestone_right = 0;
			grp_milestone_right = Integer.parseInt(grpRights.getFtrRightsByValue("MILEST"));
			if (grp_milestone_right > 3){
				if (mode.equals("M")) {
					if ((stdRights.getFtrRights().size()) == 0){
						pageRight= 0;
					}else{
						pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("MILESTONES"));
					}
				}
		
				if (((mode.equals("M") && pageRight >=4) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 ))) && ((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 ))))
				{ 
					
					/*Header item*/
					String href="<div id=\"actionValue\" style=\"display:none\">studyMilestones.jsp?mode="+mode+"&srcmenu=tdmenubaritem3&selectedTab=12&studyId="+studyId+"</div><div id=\"displayValue\"><b>"+settings.getObjDispTxt()+"</b></div>";
					menuOut.append("<div title=\"\" id=\"openMileSubMenu\" class=\"context-menu-item\">")
					.append("<div style=\"\" class=\"context-menu-item-inner\">")
					.append(href)
					.append("</div></div>")
					.append(addNewColumn(iX, tabCount));

					StringBuffer subMenu= new StringBuffer();
					/*Add New Patient Status Milestones*/
				  	String subMenuhref="<div id=\"actionValue\" style=\"display:none\">studyMilestones.jsp?mode="+mode+"&srcmenu=tdmenubaritem3&selectedTab=12&studyId="+studyId+"&mileStoneType=PM</div><div id=\"displayValue\" style=\"margin-left:10px;\">"+LC.L_AddPatStat_Milestone+"</div>";
				  	subMenu.append("<div title=\"\" class=\"context-menu-item\">")
					.append("<div style=\"\" class=\"context-menu-item-inner\">")
					.append(subMenuhref)
					.append("</div></div>")
					.append(addNewColumn(iX, tabCount));

					/*Add New Visit Milestones*/
				  	subMenuhref="<div id=\"actionValue\" style=\"display:none\">studyMilestones.jsp?mode="+mode+"&srcmenu=tdmenubaritem3&selectedTab=12&studyId="+studyId+"&mileStoneType=VM</div><div id=\"displayValue\" style=\"margin-left:10px;\">"+LC.L_AddVisit_Milestone+"</div>";
				  	subMenu.append("<div title=\"\" class=\"context-menu-item\">")
					.append("<div style=\"\" class=\"context-menu-item-inner\">")
					.append(subMenuhref)
					.append("</div></div>")
					.append(addNewColumn(iX, tabCount));

					/*Add New Event Milestones*/
				  	subMenuhref="<div id=\"actionValue\" style=\"display:none\">studyMilestones.jsp?mode="+mode+"&srcmenu=tdmenubaritem3&selectedTab=12&studyId="+studyId+"&mileStoneType=EM</div><div id=\"displayValue\" style=\"margin-left:10px;\">"+LC.L_AddEvent_Milestone+"</div>";
				  	subMenu.append("<div title=\"\" class=\"context-menu-item\">")
					.append("<div style=\"\" class=\"context-menu-item-inner\">")
					.append(subMenuhref)
					.append("</div></div>")
					.append(addNewColumn(iX, tabCount));

					/*Add New Study Status Milestones*/
				  	subMenuhref="<div id=\"actionValue\" style=\"display:none\">studyMilestones.jsp?mode="+mode+"&srcmenu=tdmenubaritem3&selectedTab=12&studyId="+studyId+"&mileStoneType=SM</div><div id=\"displayValue\" style=\"margin-left:10px;\">"+LC.L_AddStudyStat_Milestone+"</div>";
				  	subMenu.append("<div title=\"\" class=\"context-menu-item\">")
					.append("<div style=\"\" class=\"context-menu-item-inner\">")
					.append(subMenuhref)
					.append("</div></div>")
					.append(addNewColumn(iX, tabCount));

					/*Add New Additional Milestones*/
				  	subMenuhref="<div id=\"actionValue\" style=\"display:none\">studyMilestones.jsp?mode="+mode+"&srcmenu=tdmenubaritem3&selectedTab=12&studyId="+studyId+"&mileStoneType=AM</div><div id=\"displayValue\" style=\"margin-left:10px;\">"+LC.L_AddAdditional_Milestone+"</div>";
				  	subMenu.append("<div title=\"\" class=\"context-menu-item\">")
					.append("<div style=\"\" class=\"context-menu-item-inner\">")
					.append(subMenuhref)
					.append("</div></div>")
					.append(addNewColumn(iX, tabCount));

					menuOut.append(subMenu);
				}
			}			
		}
		else if ("9".equals(settings.getObjSubType())) {

			if (mode.equals("N"))
			{
					pageRight= 7;
			}
			else
			{
				if ((stdRights.getFtrRights().size()) == 0){
				 	pageRight= 0;
				}else{
					pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
				}
			}
			if ((pageRight >= 4) && ((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 )))) {
				
				String href="<div id=\"actionValue\" style=\"display:none\">studynotification.jsp?srcmenu=tdmenubaritem3&selectedTab=9&studyId="+studyId+"</div><div id=\"displayValue\"><b>"+settings.getObjDispTxt()+"</b></div>";
				menuOut.append("<div title=\"\" id=\"noSubMenu\" class=\"context-menu-item\">")
				.append("<div style=\"\" class=\"context-menu-item-inner\">")
				.append(href)
				.append("</div></div>")
				.append(addNewColumn(iX, tabCount));
			}
	   }

	   	else if ("3".equals(settings.getObjSubType())) {
			if (mode.equals("N"))
			{
					pageRight= 7;
			}
			else
			{
				if ((stdRights.getFtrRights().size()) == 0){
			 		pageRight= 0;
				}else{
					pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYPTRACK"));
		   	    }
		   	}
			if (StringUtil.isAccessibleFor(pageRight, 'V')) {
				/*Header item*/
				String href="<div id=\"actionValue\" style=\"display:none\">studystatusbrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=3&studyId="+studyId+"</div><div id=\"displayValue\"><b>"+settings.getObjDispTxt()+"</b></div>";
				menuOut.append("<div title=\"\" id=\"openStatSubMenu\" class=\"context-menu-item\">")
				.append("<div style=\"\" class=\"context-menu-item-inner\">")
				.append(href)
				.append("</div></div>")
				.append(addNewColumn(iX, tabCount));
				
				if (StringUtil.isAccessibleFor(pageRight, 'N')) {
					StringBuffer subMenu= new StringBuffer();
					/*Add New Status*/
				  	String subMenuhref="<div id=\"actionValue\" style=\"display:none\">studystatus.jsp?lastStatId="+lastStudyStatusId+"&mode=N&srcmenu=tdmenubaritem3&selectedTab=3&selOrgId=0&studyStartDt=&studyEndDt=&amp;currentStatId="+currentStatId+"</div><div id=\"displayValue\" style=\"margin-left:10px;\">"+LC.L_Add_NewStatus+"</div>";
				  	subMenu.append("<div title=\"\" class=\"context-menu-item\">")
					.append("<div style=\"\" class=\"context-menu-item-inner\">")
					.append(subMenuhref)
					.append("</div></div>")
					.append(addNewColumn(iX, tabCount));

					menuOut.append(subMenu);
				}
			}
	   }

		else if ("4".equals(settings.getObjSubType())) {
			if (mode.equals("N"))
			{
					pageRight= 7;
			}
			else
			{
				if ((stdRights.getFtrRights().size()) == 0){
			 		pageRight= 0;
				   }else{
					pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYREP"));
		   		}
		   	}
			if ((pageRight >= 4) && ((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 )))) {
				
				String href="<div id=\"actionValue\" style=\"display:none\">reportsinstudy.jsp?srcmenu=tdmenubaritem3&selectedTab=4&studyId="+studyId+"</div><div id=\"displayValue\"><b>"+settings.getObjDispTxt()+"</b></div>";
				menuOut.append("<div title=\"\" id=\"noSubMenu\" class=\"context-menu-item\">")
				.append("<div style=\"\" class=\"context-menu-item-inner\">")
				.append(href)
				.append("</div></div>")
				.append(addNewColumn(iX, tabCount));
			}
	   }

		else if ("5".equals(settings.getObjSubType())) {
			if (mode.equals("N"))
			{
					pageRight= 7;
			}
			else
			{

				if ((stdRights.getFtrRights().size()) == 0){
					  pageRight= 0;
				 }else{
					  pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYTEAM"));
				 }
			}
			if ((pageRight > 0) && ((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 )))) {
				
				/*Header item*/
				String href="<div id=\"actionValue\" style=\"display:none\">teamBrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=5&studyId="+studyId+"</div><div id=\"displayValue\"><b>"+settings.getObjDispTxt()+"</b></div>";
				menuOut.append("<div title=\"\" id=\"openTeamSubMenu\" class=\"context-menu-item\">")
				.append("<div style=\"\" class=\"context-menu-item-inner\">")
				.append(href)
				.append("</div></div>")
				.append(addNewColumn(iX, tabCount));
				
				StringBuffer subMenu= new StringBuffer();
				String parentValue="<div id=\"parentValue\" style=\"display:none\">teamBrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=5&studyId="+studyId+"</div>";
				/*View Super Users*/
			  	String subMenuhref="<div id=\"accessRights\" style=\"display:none\">V||4</div><div id=\"popValue\" style=\"display:none\">getTeamSupUser.jsp?studyId="+studyId+"</div><div id=\"displayValue\" style=\"margin-left:10px;\">"+LC.L_ViewSuprUsers+"</div>"+parentValue;
			  	subMenu.append("<div title=\"\" class=\"context-menu-item\">")
				.append("<div style=\"\" class=\"context-menu-item-inner\">")
				.append(subMenuhref)
				.append("</div></div>")
				.append(addNewColumn(iX, tabCount));
			  	
				/*Add/Edit Study Member*/
			  	subMenuhref="<div id=\"actionValue\" style=\"display:none\">modifyTeam.jsp?srcmenu=tdmenubaritem3&selectedTab=5&right="+pageRight+"</div><div id=\"displayValue\" style=\"margin-left:10px;\">"+MC.M_AddEdtStd_TeamMem+"</div>";
			  	subMenu.append("<div title=\"\" class=\"context-menu-item\">")
				.append("<div style=\"\" class=\"context-menu-item-inner\">")
				.append(subMenuhref)
				.append("</div></div>")
				.append(addNewColumn(iX, tabCount));
				
				menuOut.append(subMenu);
			}
	   }

		else if ("6".equals(settings.getObjSubType())) {

			if (mode.equals("N"))
			{
					pageRight= 7;
			}
			else
			{
				if ((stdRights.getFtrRights().size()) == 0){
					pageRight= 0;
				}else{
					pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYNOTIFY"));
			   	}
			}
			if ((pageRight > 4) && ((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 )))) {
				
				String href="<div id=\"actionValue\" style=\"display:none\">notify.jsp?mode="+mode+"&srcmenu=tdmenubaritem3&selectedTab=6&studyId="+studyId+"</div><div id=\"displayValue\"><b>"+settings.getObjDispTxt()+"</b></div>";
				menuOut.append("<div title=\"\" id=\"noSubMenu\" class=\"context-menu-item\">")
				.append("<div style=\"\" class=\"context-menu-item-inner\">")
				.append(href)
				.append("</div></div>")
				.append(addNewColumn(iX, tabCount));
			}
	   }

		else if ("8".equals(settings.getObjSubType())) {//KM-21Jul09
			int study_acc_form_right = 0;
			int study_team_form_access_right = 0;
			if ((String.valueOf(formLibAppRight).compareTo("1") == 0)) {
				if (mode.equals("N"))
				{
					String href="<div id=\"actionValue\" style=\"display:none\">formfilledstudybrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=8&studyId="+studyId+"</div><div id=\"displayValue\"><b>"+settings.getObjDispTxt()+"</b></div>";
					menuOut.append("<div title=\"\" id=\"noSubMenu\" class=\"context-menu-item\">")
					.append("<div style=\"\" class=\"context-menu-item-inner\">")
					.append(href)
					.append("</div></div>")
					.append(addNewColumn(iX, tabCount));
				}
				else
				{
					study_acc_form_right = Integer.parseInt(grpRights.getFtrRightsByValue("STUDYFRMSACC"));
					study_team_form_access_right=Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMACC"));
					if (((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 )))){
				    	if (study_acc_form_right >=4  || study_team_form_access_right>0) { //KM-#4591--refer 4099also.
				    		String href="<div id=\"actionValue\" style=\"display:none\">formfilledstudybrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=8&studyId="+studyId+"</div><div id=\"displayValue\"><b>"+settings.getObjDispTxt()+"</b></div>";
				    		menuOut.append("<div title=\"\" id=\"noSubMenu\" class=\"context-menu-item\">")
							.append("<div style=\"\" class=\"context-menu-item-inner\">")
							.append(href)
							.append("</div></div>")
							.append(addNewColumn(iX, tabCount));
						}
					}
				}
			}

	   }
	   else {
		 	 menuOut.append("<div title=\"\" id=\"noSubMenu\" class=\"context-menu-item\">")
			.append("<div style=\"\" class=\"\">")
			.append("<b>"+LC.L_NoAccsRights+"</b>")
			.append("</div></div>");
		 	itemCount++;
		}
	}

	if (itemCount > maxItemsInColumn){
		int itemsInLastColumn = (itemCount % maxItemsInColumn);
		if (itemsInLastColumn > 0){
			for(int item = 0; item < (maxItemsInColumn - itemsInLastColumn); item++){
				menuOut.append(blankMenuItem);
			}
		}
	}

	menuOut.append("</div></td></tr></tbody></table>");

	double colPercentWidth = 0;
	int extraColumn = ((itemCount % maxItemsInColumn) == 0) ? 0 : 1 ;
	colPercentWidth = 100 /((itemCount / maxItemsInColumn) + extraColumn);

	while (menuOut.indexOf("[colPercentWidth]") >= 0){
		int startIndx = menuOut.indexOf("[colPercentWidth]");
		int endIndx = menuOut.indexOf("[colPercentWidth]") + "[colPercentWidth]".length();
		menuOut.replace(startIndx, endIndx, colPercentWidth+"%");
	}

	out.println(menuOut.toString());
	}
}	
%>

