<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.velos.eres.business.common.StudyStatusDao" %>
<%@ page import="com.velos.eres.service.util.StringUtil" %>
<%@ page import="com.velos.eres.service.util.CFG,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.DateUtil, java.util.*"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyStatB" scope="request" class="com.velos.eres.web.studyStatus.StudyStatusJB"/>

<%
HttpSession tSession = request.getSession(false);
if (!sessionmaint.isValidSession(tSession)) {
	return;
}
String studyId = request.getParameter("studyId");
StudyStatusDao studyStatDao = studyStatB.getStudy(Integer.valueOf(studyId));
String externalLink = studyStatDao.getLatestExternalLinkForStudy(Integer.valueOf(studyId));
if (StringUtil.isEmpty(externalLink)) {
	return;
}
externalLink = StringUtil.stripScript(externalLink);
%>
<script>
var popProtocolInClick = function () {
	window.open("<%=externalLink%>");
}
</script>
<div id="extraneous">
<a href="javascript:void(0);" onClick="popProtocolInClick();"><table><tr><td><img src="images/click.jpg"></td><td>View Protocol in Click</td></tr></table></a>
</div>

