<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>




<BODY>
<%--
<DIV class="browserDefault" id = "div1"> 
--%>
  <jsp:useBean id="grp" scope="request" class="com.velos.eres.web.group.GroupJB"/>
  <jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
  <jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
  <%
  
  int ienet = 2;
  int proceed = 1;
  String agent1 ;
  agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1) 
     ienet = 0; //IE
    else
	ienet = 1;
	

	String src = null;



	src = request.getParameter("src");
	String fromPage = request.getParameter("fromPage");
String eSign = request.getParameter("eSign");


HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))

   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/> 

<%   
//   	String oldESign = (String) tSession.getValue("eSign");
//	if(!oldESign.equals(eSign)) {
//%>
 
<%
//	} else {
	

	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");      

	int ret = 0;
	int id=0;
	int grpId  =0;

	String grpName ="";

	String grpDesc ="";

	String accId="";

	String mode="";

	String msg="";

	String rights="";
	
	String groupHidden = "";
	
	String groupNameOld ="";  //Ankit: Bug-10718 Date-17-July-2012

	grpId = Integer.parseInt(request.getParameter("groupId"));
   	
	mode = request.getParameter("mode");

	grpName = StringUtil.stripScript(request.getParameter("groupName"));
	
	groupNameOld = StringUtil.stripScript(request.getParameter("groupNameOld"));
	
	grpDesc = StringUtil.stripScript(request.getParameter("groupDesc"));

	accId = request.getParameter("groupAccId");
	
	groupHidden = request.getParameter("groupHidden");//KM
	
	if (groupHidden != null)
		groupHidden ="1";
	else
		groupHidden ="0";
	

	int count = grp.getCount(grpName, accId);

        if(count > 0 && (mode.equals("N") || (mode.equals("M") && !grpName.equals(groupNameOld)))) {
%>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<p class = "successfulmsg" align = center> <%=MC.M_GrpNameExst_EtrNew %><%-- The Group Name that you have entered already 
  exists. Please enter a new name.*****--%> </p>
<form method="post">
  <table align=center>
    <tr width="100%"> 
      <td width="100%" > 
	<%
	proceed = 2;
	%>
	<button onclick="window.history.back(); return false;"><%=LC.L_Back%></button>

	  <%--
        <input type="button" name="return" value="Return" onClick="window.history.back()">
		--%>
      </td>
    </tr>
  </table>
</form>
<%
	} else {
	
	ctrl.getControlValues("app_rights");

	int rows = ctrl.getCRows();

	for(int i=0;i<rows;i++){ 

		rights = rights +"0";

	}



	if (mode.equals("M")){	
	grp.setGroupId(grpId);
    grp.getGroupDetails();
	grp.setGroupName(grpName);

	grp.setGroupDesc(grpDesc);

	grp.setGroupAccId(accId);
	   grp.setModifiedBy(usr);
	   grp.setIpAdd(ipAdd);
	   grp.setGroupHidden(groupHidden); //KM
//out.println(grp.getIpAdd());
	   ret = grp.updateGroup(); //Updating the existing user Link

	   if (ret == 0) {

		msg = MC.M_Grp_DetsSvdSucc;/*msg = "Group details saved successfully";*****/

	   }else {

		msg = MC.M_Grp_DetsNotSvd;/*msg = "Group details not saved";*****/

	   }  

		//out.print(msg);

	}else{

	grp.setGroupName(grpName);

	grp.setGroupDesc(grpDesc);

	grp.setGroupAccId(accId);
		grp.setCreator(usr); 
		grp.setIpAdd(ipAdd);
		grp.setGroupSuperUserFlag("0");
//out.println(grp.getIpAdd());

		grp.setGroupHidden("0");//KM
		grp.setGroupDetails();

		id = grp.getGroupId();

		grpRights.setId(id);
		grpRights.setCreator(usr);
		grpRights.setModifiedBy(usr);
		grpRights.setIpAdd(ipAdd);
		grpRights.setFtrRights(rights);

		grpRights.updateGrpRights();  ///Assigning rights to the group as not accessible
		
	}

%>

<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc %><%-- Data was saved successfully*****--%> </p>

<%
	} //end of if for count 

%>

<!--Modified by Manimaran for November Enhancement #7 -->

<% if (fromPage.equals("accountbrowser")) {%>
	<META HTTP-EQUIV=Refresh CONTENT="3; URL=accountbrowser.jsp?srcmenu=<%=src%>">
<%} else if (fromPage.equals("groupbrowser")) {
%>
<% 
if(proceed == 1)
{
if(mode.equals("N")) {
%>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=groupRights.jsp?mode=M&srcmenu=<%=src%>&groupId=<%=id%>&groupName=<%=grpName%>"> 
<%
}
else 
{%>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=groupbrowserpg.jsp?srcmenu=<%=src%>">
<%
}
}
%>	

<%}%>

<%
//}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





