<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<Link Rel=STYLESHEET HREF="common.css" type=text/css>

<SCRIPT  Language="JavaScript1.2">
function fclose_to_role()
{
	window.opener.location.reload();
	self.close();
}
</SCRIPT>

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>

<BODY>
  <jsp:useBean id="milepaymentB" scope="request" class="com.velos.eres.web.milepayment.MilepaymentJB"/>
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.MC"%>
  <%

	String src = null;
	src = request.getParameter("srcmenu");
	String eSign = request.getParameter("eSign");	
	String milepaymentId = request.getParameter("milepaymentId");

	String mode = request.getParameter("mode");
	String studyId = request.getParameter("studyId");
	String description = request.getParameter("description");
	String date = request.getParameter("date");
	String amount = request.getParameter("amount");
	String comments = request.getParameter("comments");
	String dpayCode = request.getParameter("dpayCode");
	
HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))

   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
//   	String oldESign = (String) tSession.getAttribute("eSign");
	String eventId = (String) tSession.getAttribute("eventId");

//	if(!oldESign.equals(eSign)) 
//	{
%>
  
<%
//	} 
//	else 
//  	{
	String ipAdd = (String) tSession.getAttribute("ipAdd");
	String usr = (String) tSession.getAttribute("userId");
	String enrollId =(String) tSession.getAttribute("enrollId");
%>

<%

	if(mode.equals("N")) 
	{

		milepaymentB.setMilepaymentStudyId(studyId);

		milepaymentB.setMilepaymentAmount(amount);
		milepaymentB.setMilepaymentDesc(description);
		milepaymentB.setMilepaymentDate(date);
		milepaymentB.setMilepaymentDelFlag("N");
		milepaymentB.setCreator(usr);
		milepaymentB.setIpAdd(ipAdd);
		milepaymentB.setMilepaymentType(dpayCode);
		milepaymentB.setMilepaymentComments(comments);
		
		milepaymentB.setMilepaymentDetails();
		
	}
	
	if(mode.equals("M")) 
	{
		milepaymentB.setId(EJBUtil.stringToNum(milepaymentId));
		milepaymentB.getMilepaymentDetails();	
		
		milepaymentB.setMilepaymentStudyId(studyId);
		milepaymentB.setMilepaymentAmount(amount);
		milepaymentB.setMilepaymentDesc(description);
		milepaymentB.setMilepaymentDate(date);
		milepaymentB.setMilepaymentDelFlag("N");
		milepaymentB.setModifiedBy(usr);
		milepaymentB.setIpAdd(ipAdd);
		milepaymentB.setMilepaymentType(dpayCode);
		milepaymentB.setMilepaymentComments(comments);
		
		milepaymentB.updateMilepayment();
	}	
%>

<SCRIPT>
	fclose_to_role();
</SCRIPT>

<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc %><%-- Data was saved successfully*****--%> </p>
<%
//}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





