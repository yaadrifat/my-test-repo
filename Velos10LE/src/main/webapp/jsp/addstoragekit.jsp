<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<%
String mode= "";
mode = request.getParameter("mode");

 String dProcSteps="";
 String dStorageCateg = "", dStorType="", dSpecimenType="", dSpecQuantUnit="",  dDisposition="",dStoreChildType="";;
 CodeDao cdSpecProcSteps = new CodeDao();//Processing steps
 cdSpecProcSteps.getCodeValues("spec_proctype");
 dProcSteps = cdSpecProcSteps.toPullDown("processingsteps");
 String processingsteps_edit = "";


 CodeDao cdStorageType = new CodeDao();//storage Type
 cdStorageType.getCodeValuesSaveKit("store_type");
 dStorType = cdStorageType.toPullDown("storageType");
 String storageType_edit = "";


 CodeDao cdSpecType = new CodeDao();//Specimen Type
 cdSpecType.getCodeValues("specimen_type");
 dSpecimenType = cdSpecType.toPullDown("specimentype");
 String specimentype_edit = "";


 CodeDao cdSpecQntUnit = new CodeDao();//Specimen Quantity unit
 cdSpecQntUnit.getCodeValues("spec_q_unit");
 dSpecQuantUnit = cdSpecQntUnit.toPullDown("specimenqunit");
 String specimenqunit_edit = "";


 CodeDao cdDisposition = new CodeDao();//Disposition
 cdDisposition.getCodeValues("specdisposition");
 dDisposition = cdDisposition.toPullDown("disposition");
 String disposition_edit = "";

//JM: 09Oct2009: #4311: to fetch the Enviornmental Constraints in Storage Kit Details from the er_codelst table
 CodeDao cdEcons = new CodeDao();
 cdEcons.getCodeValues("envt_cons");
 ArrayList codelstDescs=cdEcons.getCDesc();
 String codelstdesc="";
 int length=codelstDescs.size();



%>

<head>
	<title> <%=MC.M_MngInv_StrgKitDets%><%--Manage Inventory >> Storage Kit Details*****--%></title>
	 <SCRIPT LANGUAGE = "JavaScript">
		var screenWidth = screen.width;
		var screenHeight = screen.height;
	    var specimenValues ;
	    var storageValues ;

		function validateName(storageName)
		{
			if(storageName.value == ''||storageName.value==null )
			{
				alert("<%=MC.M_StorName_CntBlank%>");/*alert("Storage Name cannot be left blank");*****/
				storageName.focus();
				return false;
			}
			if (isWhitespace(storageName.value))
			{
				alert("<%=MC.M_Etr_ValidData%>");/*alert("Please enter valid data");*****/
				storageName.focus();
				return false;

			}

		}




</SCRIPT>
 </head>
 <body  >
<SCRIPT language="javascript">

function  validate(formobj){

	    var disFlagsp = false;
        var disFlagst = false;
	
		/* Bug#6320 10-Sep-2012 -Sudhir*/
		var  rowIds= formobj.rowId.value;


     if (!(validate_col('Storage Name',formobj.storageKitName))) return false;
     
     if (!(validate_col('Storage Kit Type',formobj.storeChildType))) return false;


	 if(isNaN(formobj.eSign.value) == true) {
		 alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
     }

	/* Bug#6320 10-Sep-2012 -Sudhir*/
	for(rw=0;rw<rowIds;rw++) {
		var kitCompNames = document.getElementsByName("kitCompName");		
		if (!(validate_col('Storage Name',kitCompNames[rw]))) return false;

		 var strgTyp= document.getElementsByName("storageType");	
		if (!(validate_col('Storage Kit',strgTyp[rw]))) return false;

		var spcmnTyp= document.getElementsByName("specimentype");	
		if (!(validate_col('Specimen Type',spcmnTyp[rw]))) return false;
		
	}

	 if (!(validate_col('e-Signature',formobj.eSign))) return false;


      var totalComp = formobj.totalExtgRows.value;

      if (totalComp==1){

      	  kit_comp_name = formobj.kitCompName.value;
	      kit_strType = formobj.storageType.value;

	      kit_quantity = formobj.quantity.value;
		  kit_specimenqunit = formobj.specimenqunit.value;
		//Fixed bug 6030 mandatory validation for specimenType ,BK
	      kit_strSpecimenType = formobj.specimentype.value;

	      if ((kit_comp_name !='' || kit_strType !='' || kit_strSpecimenType != '')) {

				if(kit_comp_name==''){alert("<%=MC.M_Etr_StorKitName%>")/*alert("Please enter Storage Kit Name")*****/; formobj.kitCompName.focus(); return false;}
				if(kit_strType==''){alert("<%=MC.M_Etr_StorKitType%>")/*alert("Please enter Storage Kit Type")*****/; formobj.storageType.focus();return false;}
				if(kit_strSpecimenType==''){alert("<%=MC.M_PlsEtr_SpmenType%>")/*alert("Please enter Specimen Type")*****/; formobj.specimentype.focus();return false;}
		  }

		  if ((kit_quantity !='' || kit_specimenqunit !='')) {

				if(kit_quantity==''){alert("<%=MC.M_Etr_StorKitQty%>")/*alert("Please enter Storage Kit Quantity")*****/; formobj.quantity.focus(); return false;}
				if((kit_comp_name !='' || kit_strType !='') && parseFloat(formobj.quantity.value)!= '0' && kit_specimenqunit==''){alert("<%=MC.M_Etr_StrKitQtyUnit%>")/*alert("Please enter Storage Kit Quantity Unit")*****/; formobj.specimenqunit.focus(); return false;}

		  }


      }else if (totalComp>1){

	      for(i=0;i<totalComp;i++) {

		      kit_comp_name = formobj.kitCompName[i].value;
		      kit_strType = formobj.storageType[i].value;

		      kit_quantity = formobj.quantity[i].value;
			  kit_specimenqunit = formobj.specimenqunit[i].value;
			  kit_strSpecimenType = formobj.specimentype[i].value;	
		      if ((kit_comp_name !='' || kit_strType !='' || kit_strSpecimenType != '')) {

					if(kit_comp_name==''){alert("<%=MC.M_Etr_StorKitName%>")/*alert("Please enter Storage Kit Name")*****/; formobj.kitCompName[i].focus(); return false;}
					if(kit_strType==''){alert("<%=MC.M_Etr_StorKitType%>")/*alert("Please enter Storage Kit Type")*****/; formobj.storageType[i].focus();return false;}
					if(kit_strSpecimenType==''){alert("<%=MC.M_PlsEtr_SpmenType%>")/*alert("Please enter Specimen Type")*****/; formobj.specimentype[i].focus();return false;}
			  }

			  if ((kit_quantity !='' || kit_specimenqunit !='')) {

					if(kit_quantity==''){alert("<%=MC.M_Etr_StorKitQty%>")/*alert("Please enter Storage Kit Quantity")*****/; formobj.quantity[i].focus(); return false;}
					if((kit_comp_name !='' || kit_strType !='') && parseFloat(formobj.quantity[i].value)!= '0' && kit_specimenqunit==''){alert("<%=MC.M_Etr_StrKitQtyUnit%>")/*alert("Please enter Storage Kit Quantity Unit")*****/; formobj.specimenqunit[i].focus(); return false;}

			  }

		  }
	  }//end totalComp>1


} //end of validate method


function indivChek(formobj){
 	var numCompRowExists = 0;
	numCompRowExists = formobj.totalExtgRows.value

	for (var i=0; i<numCompRowExists; i++){
	var strEnvtChkd="";
	var checkmed = "";


		if (numCompRowExists==1){
			if (formobj.checkme0.checked ==true){
		 	strEnvtChkd +="1";
			}else if (formobj.checkme0.checked ==false){
			strEnvtChkd +="0";
			}

			if (formobj.checkme1.checked ==true){
		 	strEnvtChkd +="1";
			}else if (formobj.checkme1.checked ==false){
			strEnvtChkd +="0";
			}

			if (formobj.checkme2.checked ==true){
		 	strEnvtChkd +="1";
			}else if (formobj.checkme2.checked ==false){
			strEnvtChkd +="0";
			}

			formobj.envtStringChecked.value = strEnvtChkd;
		}else{
			if (formobj.checkme0[i].checked ==true){
		 	strEnvtChkd +="1";
			}else if (formobj.checkme0[i].checked ==false){
			strEnvtChkd +="0";
			}

			if (formobj.checkme1[i].checked ==true){
		 	strEnvtChkd +="1";
			}else if (formobj.checkme1[i].checked ==false){
			strEnvtChkd +="0";
			}

			if (formobj.checkme2[i].checked ==true){
		 	strEnvtChkd +="1";
			}else if (formobj.checkme2[i].checked ==false){
			strEnvtChkd +="0";
			}

			formobj.envtStringChecked[i].value = strEnvtChkd;

		}

	}


return true;

}

function fnChk(formobj){

 	var numCompRowExists = 0;
	numCompRowExists = formobj.totalExtgRows.value

	for (var i=0; i<numCompRowExists; i++){

		 if (numCompRowExists==1){
		 	if (formobj.checkCopyChild.checked){
		 		formobj.checkCopyChildStr.value = "1";
			}else{
				formobj.checkCopyChildStr.value = "0";
			}

	 	}else{
		 	if (formobj.checkCopyChild[i].checked){
		 		formobj.checkCopyChildStr[i].value = "1";
			}else{
				formobj.checkCopyChildStr[i].value = "0";
			}

	 	}


	}
}


function fnAddDropDown(formobj, turn){

	var numCompRowExists = 0;
	numCompRowExists = formobj.totalExtgRows.value

	var baseRowNum = 0;
	baseRowNum = formobj.baseOrigRows.value

	var addedNum = 0;
	addedNum = formobj.numDropDownPerRow[turn].value

	//for (var f=0; f<numCompRowExists; f++){

	    var myRow = eval("document.getElementById(\"myTable"+turn+"\").insertRow(addedNum)");
		var myCell = myRow.insertCell(0);
		myCell.innerHTML = "<%=dProcSteps%>";
		myCell = myRow.insertCell(1);
		myCell.innerHTML = "<input type='text' name='sequence' size='3'/>";

		addedNum ++
		formobj.numDropDownPerRow[turn].value=addedNum;
		//break;
	//}

}





function fnAddMoreComponentRows(formobj, exits){

/* Bug#6320 10-Sep-2012 -Sudhir*/
var rowId = formobj.rowId.value;
//how many new row do you want to add in the existing rows of the table
var newAddCount = formobj.addmore.value

	var numCompRowExists = 0;
	numCompRowExists = formobj.totalExtgRows.value


	for (var f=0; f<newAddCount; f++){

	   //cTurn is component turn used for the hidden check boxes for deletion operation
	    var cTurn= (eval(formobj.totalExtgRows.value)+1)



	    var myRow = document.getElementById("myMainTable").insertRow((eval(formobj.totalExtgRows.value)+1));


	    var myCell = myRow.insertCell(0);
		myCell.innerHTML = '<tr id="myiRow'+numCompRowExists+'" > <td><input type="text" name="kitCompName" value="" size="10"/></td>';
	  //myCell.innerHTML = "<tr id='myiRow"+numCompRowExists+"' > <td><input type='text' name='kitCompName' value='' size='10'/></td>";

		myCell = myRow.insertCell(1);
		myCell.innerHTML = "<td class=tdDefault><%=dStorType%></td>";
		myCell = myRow.insertCell(2);
		myCell.innerHTML = "<td class=tdDefault><%=dSpecimenType%></td>";
		myCell = myRow.insertCell(3);
		myCell.innerHTML = "<td><input type='text' name='quantity' value='' size='05'/></td>";

		myCell = myRow.insertCell(4);
		myCell.innerHTML = "<td class=tdDefault><%=dSpecQuantUnit%></td>";

		myCell = myRow.insertCell(5);
		myCell.innerHTML = '<td><table>'
		+ '	<tr><td>&nbsp;&nbsp;'
		+ '	<a href=# onClick="fnAddDropDown(document.storagekit, ('+numCompRowExists+'));"><%=LC.L_Add_More%><%--Add More*****--%></a>'
		+ '	</td></tr>'
		+ '	</table>'
		+ '	<table id="myTable'+numCompRowExists+'" border="0">'
		+ '	<tr id="tr1"><td class="tdDefault">'+"<%=dProcSteps%>"+'</td>'
		+ '	<td><input type="text" name="sequence" value="1" size="03"></td>'
		+ '	</tr>'
		+ '	<tr id="tr2"><td class="tdDefault">'+"<%=dProcSteps%>"+'</td>'
		+ '	<td><input type="text" name="sequence" value="2" size="03"></td>'
		+ '	</tr></table>'
		+ '	</td><input type="hidden" name="numDropDownPerRow" value="2">';


		myCell = myRow.insertCell(6);
		myCell.innerHTML = "<td class=tdDefault><%=dDisposition%></td>";
		myCell = myRow.insertCell(7);
		myCell.innerHTML =
		"   <td><input type='checkbox' name='checkme0' onClick='indivChek(document.storagekit)'>"+'<%=(String)codelstDescs.get(0)%>'+" <br>"
		+ " <input type='checkbox' name='checkme1' onClick='indivChek(document.storagekit)'>"+'<%=(String)codelstDescs.get(1)%>'+" <br>"
		+ " <input type='checkbox' name='checkme2' onClick='indivChek(document.storagekit)'>"+'<%=(String)codelstDescs.get(2)%>'+" <br></td>"
		+ " <input type='hidden' name='envtStringChecked'>";


		myCell = myRow.insertCell(8);
		myCell.innerHTML = "<td><input type='text' name='numSpecChildren' size='03'/></td>";
		myCell = myRow.insertCell(9);
		//myCell.innerHTML = "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='checkbox' name='checkCopyChild' id='checkCopyChild' onClick='fnChk(document.storagekit)'><input type='hidden' name='checkCopyChildStr'></td>"

		myCell = myRow.insertCell(10);
		myCell.innerHTML = '<td width="10%" align="center"><A href="#" onClick ="return fnDeleteRowComponentRows(document.storagekit,  '+(eval(formobj.totalExtgRows.value)+1)+')"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" /></A></td></tr>';
		myCell.align='center';

		myCell = myRow.insertCell(11);
		var element1 = document.createElement("input");
    	element1.type = "checkbox";
    	element1.name="hiddenChk";
    	element1.id="hiddenChk_"+cTurn;
		element1.style.display='none';
    	myCell.appendChild(element1);


		numCompRowExists++
		formobj.totalExtgRows.value = numCompRowExists;
		/* Bug#6320 10-Sep-2012 -Sudhir*/
		rowId++;
	}

	/* Bug#6320 10-Sep-2012 -Sudhir*/
	formobj.rowId.value = rowId;
}



function fnDeleteRowComponentRows(formobj, turn)
{

	var numCompRowExists = 0;
	numCompRowExists = formobj.totalExtgRows.value

	document.getElementById('hiddenChk_'+turn).checked='true';
	/* Bug#6320 10-Sep-2012 -Sudhir*/
	var rowId = formobj.rowId.value ;

	 try {
            var table = document.getElementById("myMainTable");
            var rowCount = table.rows.length;


            for(var i=1; i<rowCount; i++) {



                var row = table.rows[i];
                var chkbox = row.cells[11].children[0];   //Akshi:Modified for Bug#7182


                if(null != chkbox && true == chkbox.checked ) {
               	//alert("hare krishna hare krishan kishna krishna hare hare hare rama hare rama rama rama hare hare")

                    table.deleteRow(i);
                    rowCount--;
					/* Bug#6320 10-Sep-2012 -Sudhir*/
					rowId = rowCount;
					rowId--;
                    i--;
                    numCompRowExists--;
					formobj.totalExtgRows.value = numCompRowExists;


                }

			
	    	}
			/* Bug#6320 10-Sep-2012 -Sudhir*/
			formobj.rowId.value = rowId;
	 }catch(e) {
	        alert(e);
	 }

}//end fn



</SCRIPT>


<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="srctdmenubaritem5"/>
</jsp:include>


<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.StringUtil,com.velos.eres.business.common.*,com.velos.eres.web.storageAllowedItems.* ,com.velos.eres.web.grpRights.GrpRightsJB" %>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="storageB" scope="request" class="com.velos.eres.web.storage.StorageJB"/>
<jsp:useBean id="StorageStatJB" scope="request" class="com.velos.eres.web.storageStatus.StorageStatusJB"/>
<jsp:useBean id="stdJB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="usrJB" scope="request" class="com.velos.eres.web.user.UserJB"/>




<div class="tabDefTopN" id="div1">
<%

HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession)) {

	 int pageRight = 0;

	 GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");

	 pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSTORAGE"));

String selectedTab = request.getParameter("selectedTab");


String storageType = "";
String notes ="";
String location = "";
String pkStorageKit ="";
String dim1Naming ="";
String dim1Order ="";
String dim2Naming ="";
String dim2Order ="";
String capNum ="1";
String capUnit ="";
String dim1CellNum="";
String dim2CellNum="";
String strCoordinateX ="";
String strCoordinateY ="";
String childStorageType ="";
String  stdy = "", studyNumber = "";
String  user = "", userName ="";
String  modFlg = "";
String template = "";
String templateTyp = "", alternateId="", storageUnitClas="";



String storageParentIsKit = "";
String disableDim="";
ArrayList fkCodelstStorageType  = new ArrayList();
ArrayList fkCodelstSpecimenType = new ArrayList();
ArrayList allowedItemTypes  = new ArrayList();
ArrayList pkAllowedItems    = new ArrayList();
ArrayList fkStorages        = new ArrayList();

String mainFKStorage = "";
String mainParentStorageName = "";

String acc = (String) tSession.getValue("accountId");

 CodeDao cdStorCateg= new CodeDao();//category
 cdStorCateg.getCodeValues("storecategory");
 dStorageCateg = cdStorCateg.toPullDown("storecate");
 String storecateg = "";
 
//Added by Bikash for Invp 2.8 Storage Child Type 
 
 CodeDao cdStoreChildType= new CodeDao();//child  storage type
 //removed kit from kit storage type
 cdStoreChildType.getCodeValuesSaveKit("store_type");
 dStoreChildType = cdStoreChildType.toPullDown("storeChildType");
 String storeChildType = "";



String add_more = request.getParameter("addmore");
add_more = (add_more==null)?"1":add_more;


String counter_more = request.getParameter("counterBox");
counter_more = (counter_more==null)?"":counter_more;


String counter_more1 = request.getParameter("counterBox");
counter_more1 = (counter_more1==null)?"":counter_more1;
pkStorageKit = request.getParameter("pkstoragekit");




%>

<table width="100%" cellspacing="0" cellpadding="0" border="0" >

	<tr><td colspan = "4">
	<jsp:include page="inventorytabs.jsp" flush="true">
		<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
		</jsp:include>
	</td>
	</tr>
</table>

</div>
<div class="tabDefBotN" id="div2">

<Form name="storagekit" id="storagekitid" method="post" action="updatestoragekit.jsp" onSubmit="if (validate(document.storagekit)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

 <input type="hidden" name="mode" value=<%=mode%>>

 <input type="hidden" name="pkStorageKit" value=<%=pkStorageKit%>>

  <input type="hidden" name="storageParentType" value="<%=storageParentIsKit%>">

<%
int baseOginalRows = 0;
baseOginalRows = 5;

String numExi = "";
numExi = request.getParameter("totalExtgRows");
numExi = (numExi ==null)?"5":numExi;




%>

<input type = "hidden" name="totalExtgRows" value=<%=numExi%>>
<input type = "hidden" name="baseOrigRows" value=<%=baseOginalRows%>>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tr>
	<td colspan="2"><%=MC.M_StrKitIdBlank_ForAutoGenr%><%--Leave 'Storage Kit ID' field blank for system auto-generated ID*****--%></td>
	</tr>

	<tr>
	<td width="50%">
		<table>
		<tr>
		<td class=tdDefault width="30%"><%=LC.L_Storage_KitId%><%--Storage Kit ID*****--%></td>
		<td class=tdDefault><Input type = "text" name="storageKitId"  size=15 MAXLENGTH =100></td>

		</td>


		<% if (mode.equals("N")){ %>
		<td class=tdDefault ><%=LC.L_Category%><%--Category*****--%></td>
		<td class=tdDefault><%=dStorageCateg%></td>
			    <%}else {

				dStorageCateg = cdStorCateg.toPullDown("storecate",EJBUtil.stringToNum(storecateg));
				%>
		<td class=tdDefault align="center"><%=LC.L_Category%><%--Category*****--%></td>
		<td class=tdDefault><%=dStorageCateg%></td>

			    <%}%>

		</tr>
		<tr>
		<td class=tdDefault ><%=LC.L_Storage_KitName%><%--Storage Kit Name*****--%><FONT class="Mandatory">* </FONT></td>
		<td class=tdDefault ><Input type = "text" name="storageKitName"   size=25 MAXLENGTH = 250 align = right></td>
		<!--Added by Bikash Invp 2.8 change-->
		<%
		dStoreChildType = cdStoreChildType.toPullDown("storeChildType",EJBUtil.stringToNum(storeChildType));
		%>
		<td class=tdDefault align="center"><%=LC.L_Storage_KitType%><%--Storage Kit Type*****--%><FONT  class="Mandatory">* </FONT></td> <!-- AK:FixedBug#5816 (16Mar11) -->
		<td class=tdDefault><%=dStoreChildType%></td>
		</tr>

</table>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<br>

<input type="hidden" name="counterBox" value="" size="2">
<tr>
<td ><P class = "sectionHeadings"> <%=LC.L_Kit_Components%><%--Kit Components*****--%></p></td>
<td> <a href=# onClick="return fnAddMoreComponentRows(document.storagekit, <%=numExi%>);"><img border="0" src="../images/add.png" title="<%=LC.L_Add%><%--Add*****--%>"/></a>&nbsp;
<input type="text" name="addmore" value="<%=add_more%>" size="2">&nbsp;
<a href=# onClick="return fnAddMoreComponentRows(document.storagekit, <%=numExi%>);"><%=LC.L_More_Components%><%--More Component(s)*****--%></a></td>
</td>
</tr>
</table>

<table class=tableDefault id="myMainTable" width="100%" border=0 >
<TR id="myiRow0">

	 <!--Changes By Sudhir to Remove width attribute of <th> tag for Bug 8749 on 3/21/2012-->
 	 <th align =center><%=LC.L_Name%><%--Name*****--%><FONT class="Mandatory">* </FONT></th>
     <th align =center><%=LC.L_Storage_Type%><%--Storage Type*****--%><FONT class="Mandatory">* </FONT></th>
     <th align =center><%=LC.L_Specimen_Type%><%--Specimen Type*****--%><FONT class="Mandatory">*</th>
     <th align =center colspan=2><%=LC.L_Quantity%><%--Quantity*****--%></th>

     <th align =center ><%=LC.L_Processing_Steps%><%--Processing Steps*****--%> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <%=LC.L_Seq%><%--Seq*****--%>#</th>
      <!--
      <th width=05%  align =left >Seq#</th>
      -->

 	 <th align =center ><%=LC.L_Disposition%><%--Disposition*****--%></th>
 	 <th align =center ><%=LC.L_Environmental_Consts%><%--Environmental Constraints*****--%></th>
 	 <th align =center ><%=LC.L_Specimen_Children%><%--Specimen Children*****--%></th>
 	<!-- <th width=10%  align =center colspan=1 >Copy Attributes to Children</th> -->
 	 <th align =center >&nbsp;&nbsp;&nbsp;</th>
 	 <th align =center colspan=1 ><%=LC.L_Delete%></th>
 	 <th align =center >&nbsp;&nbsp;</th>
	 <!--Changes By Sudhir for Bug 8749 on 3/21/2012-->
<INPUT type="checkbox" name="hiddenChk" id="hiddenChk_0" style="visibility:hidden"/>

</TR>



<%


for (int n=0 ; n < EJBUtil.stringToNum(numExi); n ++){

%>

<tr id="myiRow<%=n+1%>" >
<td>
<input type="text" name="kitCompName" value="" size="10">
</td>

<% if (mode.equals("N")){ %>
	<td class=tdDefault><%=dStorType%></td>
<%}else {
	dStorType = cdStorageType.toPullDown("storageType",EJBUtil.stringToNum(storageType_edit));
%>
	<td class=tdDefault><%=dStorType%></td>
<%}%>

<% if (mode.equals("N")){ %>
	<td class=tdDefault><%=dSpecimenType%></td>
<%}else {
	dSpecimenType = cdSpecType.toPullDown("specimentype",EJBUtil.stringToNum(specimentype_edit));
%>
	<td class=tdDefault><%=dSpecimenType%></td>
<%}%>
<td>
<input type="text" name="quantity" value="" size="05">
</td>
<% if (mode.equals("N")){ %>
	<td class=tdDefault><%=dSpecQuantUnit%></td>
<%}else {
	dSpecQuantUnit = cdSpecQntUnit.toPullDown("specimenqunit",EJBUtil.stringToNum(specimenqunit_edit));
%>
	<td class=tdDefault><%=dSpecQuantUnit%></td>
<%}%>

<!--Processing Steps and sequence----------------->
<td>
<table>
<tr>
<td>&nbsp;&nbsp;<a href=# onClick="fnAddDropDown(document.storagekit, '<%=n%>')" ><%=LC.L_Add_More%><%--Add More*****--%></a>
</td>
</tr>
</table>

<table id="myTable<%=n%>" name="myTable<%=n%>" border="0">
<tr id="tr1">
	<% if (mode.equals("N")){ %>
		<td class=tdDefault><%=dProcSteps%></td>

	<%}else {
		dProcSteps = cdSpecProcSteps.toPullDown("processingsteps",EJBUtil.stringToNum(processingsteps_edit));
	%>
		<td class=tdDefault><%=dProcSteps%></td>
	<%}
	%>


	<td>
	<input type="text" name="sequence" value="1" size="03">
	</td>
</tr>

<tr id="tr2">

	<% if (mode.equals("N")){ %>
		<td class=tdDefault><%=dProcSteps%></td>

	<%}else {
		dProcSteps = cdSpecProcSteps.toPullDown("processingsteps",EJBUtil.stringToNum(processingsteps_edit));
	%>
		<td class=tdDefault><%=dProcSteps%></td>
	<%}
	%>

	<td>
	<input type="text" name="sequence" value="2" size="03">
	</td>

</tr>
</table>


</td>


<%

String baseVal="2";

baseVal = request.getParameter("numDropDownPerRow");
baseVal = (baseVal ==null)?"2":baseVal ;

%>

<input type="hidden" name="numDropDownPerRow" value="<%=baseVal%>">




<!--Disposition----------------->
<% if (mode.equals("N")){ %>
		<td class=tdDefault><%=dDisposition%></td>

<%}else {
		dDisposition = cdDisposition.toPullDown("disposition",EJBUtil.stringToNum(disposition_edit));
	%>
		<td class=tdDefault><%=dDisposition%></td>
<%}%>
<td>
<%
	for (int i=0;i<length; i++) {
	codelstdesc=(String)  codelstDescs.get(i);

%>

<input type="checkbox" name="checkme<%=i%>" id="checkme<%=i%>" onClick="indivChek(document.storagekit)" > <%=codelstdesc%><br>

<%
	}
%>
</td>

<input type="hidden" name="envtStringChecked">

<td>
<input type="text" name="numSpecChildren" value="" size="03">
</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 <!--<input type="checkbox" name="checkCopyChild" id="checkCopyChild" onClick="fnChk(document.storagekit)">--> 
 <!--<input type="hidden" name="checkCopyChildStr"> -->
</td>
<!--delete things-------->
<td width="10%"  align="center"><A href="#" onClick ="return fnDeleteRowComponentRows(document.storagekit, <%=n+1%>);"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0"/></A></td>
<td>
<INPUT type="checkbox" name="hiddenChk" id="hiddenChk_<%=n+1%>" style="visibility:hidden"/>
</td>
</tr>

<%
}//ending of for loop
%>


<Input type = "hidden" name="lengthOfLoop" value="<%=numExi%>">

</table>


<table>
<tr>
<td width="50%">
	<table>
	<tr>
	<td width="30%"><%=LC.L_Notes%><%--Notes*****--%></td>
	<td class=tdDefault><textarea name="notes" rows=4 cols=30 MAXLENGTH = 20000><%=notes%></textarea></td>
	</tr>
	</table>
</td>
</tr>
</table>

<%

 if ( pageRight >=5  )
 {
%>
<BR>

<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="storagekitid"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>
<% } %>
<!-- Bug#6320 10-Sep-2012 -Sudhir-->
<Input type = "hidden" name="rowId" value="<%=numExi%>" >
</Form>
<%
}//end of if body for session
	else {
%>
  <jsp:include page="timeout.html" flush="true"/>
<% } %>
	<div class = "myHomebottomPanel">
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</div>
</body>

</html>
<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024)
		$j("#div2").attr("style","height:80%;");
</SCRIPT>