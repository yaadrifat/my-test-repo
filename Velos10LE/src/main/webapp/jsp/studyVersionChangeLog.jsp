<%@page import="com.velos.eres.web.study.StudyJB"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC,com.velos.eres.service.util.DateUtil"%> 
<%@page import="org.json.JSONObject,org.json.JSONArray" %>
<%@page import="com.velos.eres.widget.service.util.FlxPageCache, com.velos.eres.service.util.StringUtil"%>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@page import="com.velos.eres.web.studyRights.StudyRightsJB"%>
<%@page import="com.velos.eres.business.common.*,com.velos.eres.service.util.*"%>
<%@page import="com.velos.eres.compliance.web.ProtocolChangeLog"%>
<%@page import="com.velos.eres.compliance.web.ComplianceJB" %>
<%@page import="java.util.*,java.io.*"%>
<%@page import="com.velos.eres.web.submission.SubmissionStatusJB" %>
<jsp:include page="ui-include.jsp" flush="true"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="grpRights" scope="session" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="stdRights" scope="session" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="studyJB" scope="session" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="complianceJB" scope="request" class="com.velos.eres.compliance.web.ComplianceJB"/>
<link href="styles/studyVersion.css" rel="stylesheet" type="text/css" />
<%
String isNewAmendment="false";

HttpSession tSession = request.getSession(false); 
if (!sessionmaint.isValidSession(tSession)) {
	return;
}
String userId = (String)tSession.getAttribute("userId");
String grpId = (String) tSession.getValue("defUserGroup");
String studyId = (String)request.getParameter("studyId");
int pageVer = StringUtil.stringToNum(request.getParameter("pageVer"));
int pageMinVer = StringUtil.stringToNum(request.getParameter("pageMinorVer"));
if(studyId == null || studyId.equals("null")){
	studyId = "0";
} 

%>
<link rel="stylesheet" type="text/css" href="styles/flexPage.css">

<%
StudyDao studyDao = new StudyDao();
String studyNumber = studyDao.getStudy(StringUtil.stringToNum(studyId));
out.println(LC.L_Study_Number + " : " + studyNumber);
out.println("<BR>");
ProtocolChangeLog protocolChangeLog = new ProtocolChangeLog();
	out.println(protocolChangeLog.fetchProtocolVersionDiffForVersion(pageVer, pageMinVer, StringUtil.stringToNum(studyId), grpId));

%>
