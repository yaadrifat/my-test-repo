<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language = "java" import = "com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*,com.velos.eres.business.common.CtrlDao,java.util.*,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,com.velos.eres.business.common.TeamDao"%>
<%@ page import="com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings,com.velos.eres.business.common.*"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="studymod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="studyTeam" scope="request" class="com.velos.eres.web.team.TeamJB"/>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<%
boolean isAmendIrb = "irb_amend_tab".equals(request.getParameter("selectedTab")) ? true : false; 

String src= request.getParameter("srcmenu");
HttpSession tSession = request.getSession(true); 
String accountId = "";
String studyId = "";
String grpId = "";
String usrId = "";
ArrayList boardNameList = null;
ArrayList boardIdList = null;
ArrayList statusList = null;
ArrayList statusDateList = null;
EIRBDao eIrbDao = new EIRBDao();
if (sessionmaint.isValidSession(tSession))
{
    accountId = (String) tSession.getValue("accountId");
    studyId = (String) tSession.getValue("studyId");
    grpId = (String) tSession.getValue("defUserGroup");
    usrId = (String) tSession.getValue("userId");
	
	if (StringUtil.isEmpty(usrId))
	{
		usrId="";
	}

	if("M".equals(request.getParameter("mode")) 
	        && request.getParameter("studyId") != null) {
        studyId = StringUtil.htmlEncodeXss(request.getParameter("studyId"));
	}
    if(accountId == null || accountId == "") {
    %>
		<jsp:include page="timeout.html" flush="true"/>
	<%
        return;
    }
   
}

String isNewAmendment="true";
session.setAttribute("isNewAmendment", isNewAmendment);

%>
<body>
<form name="newAmendForm" id="newAmendFormId" action="flexStudyScreen" method="post">
<input type=hidden name="studyId" value="<%=studyId%>">
<input type=hidden name="accountId" value="<%=accountId%>">
<input type=hidden name="userId" value="<%=usrId%>">
<input type=hidden name="isNewAmendment" value="<%=isNewAmendment%>">
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tr><td align="Center"><br /></td></tr>
        <tr><td align="Center"><button name="New Amendment" type="submit"><%=LC.L_New_Amendment%></button></td></tr>
        <tr><td align="Center"><br /></td></tr>
  	</table> 
</form>
</body>

</html>

