<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- Bug#9941 29-May-2012 -Sudhir-->
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="include.jsp" flush="true"/>
<HTML> 

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id="accwrapB" scope="request" class="com.velos.eres.web.accountWrapper.AccountWrapperJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,com.velos.eres.service.util.*"%>
<%

   int ret=2;

 /* Modified by Amarnadh ,Added an attribue siteNotes for June Enhancement '07 #U7 */

   String msg=null;

   String mode=null;

   String siteId=null;

   String siteAddId=null;

   String siteName = null;

   String siteType = null;

   String siteInfo = null;

   String siteNotes = null;  

   String siteParent = null;

   String siteStatus = null;

   String siteAddress = null;

   String siteCity = null;

   String siteState = null;

   String siteZip = null;

   String siteCountry = null;

   String sitePhone = null;

   String siteEmail = null;

   String siteAccount = null;
   String siteIdentifier = "";
   String poIdentifier  ="";



   String src = request.getParameter("src");   



   mode = request.getParameter("mode");

   siteId = request.getParameter("siteId");
   

   siteName = StringUtil.stripScript(request.getParameter("siteName").trim());//KM
	
  
   siteType = request.getParameter("siteType");
   

   	siteInfo = StringUtil.stripScript(request.getParameter("siteInfo"));
    siteInfo = (   siteInfo  == null      )?"":(  siteInfo ) ;	

    
	siteNotes = StringUtil.stripScript(request.getParameter("siteNotes"));

     siteNotes = ( siteNotes == null  ) ?"":( siteNotes) ;

      
     
	String eSign = request.getParameter("eSign");

   if(request.getParameter("siteParent")!=null)

	{

	   siteParent = request.getParameter("siteParent");

  	   if (siteParent == null ) siteParent = "";	

        }



   if (siteParent.equals("")) siteParent = null;	



   siteStatus = request.getParameter("siteStatus");
		if (StringUtil.isEmpty(siteStatus))
			{
				siteStatus="";
			}
   siteAddId =  request.getParameter("siteAddId");
   siteAddId = (   siteAddId  == null      )?"":(  siteAddId ) ;	

   siteAddress = StringUtil.stripScript(request.getParameter("siteAddress"));
   siteAddress = (   siteAddress  == null      )?"":(  siteAddress ) ;	


   siteCity = StringUtil.stripScript(request.getParameter("siteCity"));
   siteCity = (   siteCity  == null      )?"":(  siteCity ) ;	

   siteState = StringUtil.stripScript(request.getParameter("siteState"));
   siteState = (   siteState  == null      )?"":(  siteState ) ;	

	 siteZip = StringUtil.stripScript(request.getParameter("siteZip"));
    siteZip = (   siteZip  == null      )?"":(  siteZip ) ;	

   siteCountry = StringUtil.stripScript(request.getParameter("siteCountry"));
   siteCountry = (   siteCountry  == null      )?"":(  siteCountry ) ;	

   sitePhone = request.getParameter("sitePhone");
   sitePhone = (   sitePhone  == null      )?"":(  sitePhone ) ;	

   siteEmail = StringUtil.stripScript(request.getParameter("siteEmail"));
   siteEmail = (   siteEmail  == null      )?"":(  siteEmail ) ;	

   siteIdentifier = StringUtil.stripScript(request.getParameter("siteIdentifier"));
   poIdentifier   = StringUtil.stripScript(request.getParameter("poIdentifier"));
   
   if (StringUtil.isEmpty(siteIdentifier))
   {
   	   siteIdentifier = "";
   	   
   	   /*if (mode.equals("N"))
   	   {
   		siteIdentifier = String.valueOf(System.currentTimeMillis()) ;
   	   }
   	   else
   	   {
   	   		siteIdentifier = "";
   	   }*/
   	   	   
   }
   siteIdentifier = siteIdentifier.trim();
   if (StringUtil.isEmpty(poIdentifier)){
	   poIdentifier = "";
   }
   poIdentifier = poIdentifier.trim();
  	  
//   siteAccount = request.getParameter("siteAccount");

	//KM-Enh.#U11
   String siteHidden = request.getParameter("siteHidden");
   
   if ( siteHidden != null)
	   siteHidden = "1";
   else
	   siteHidden = "0";
	   
HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))
   {	
%>
<jsp:include page="sessionlogging.jsp" flush="true"/> 

<%   
//  	String oldESign = (String) tSession.getValue("eSign");
//	if(!oldESign.equals(eSign)) {
//%>
  
<%
//	} else {


 String ipAdd = (String) tSession.getValue("ipAdd");
 String usr = null;
 usr = (String) tSession.getValue("userId");

 siteAccount = (String) tSession.getValue("accountId");



 if (mode.equals("M"))

 {

   accwrapB.setSiteId(siteId);

   accwrapB.setSitePerAdd(siteAddId);

 }

   accwrapB.setSiteStatus(siteStatus);
   accwrapB.setUserAccountId(siteAccount);

   accwrapB.setSiteCodelstType(siteType);

   accwrapB.setSiteInfo(siteInfo);

   accwrapB.setSiteName(siteName);
   
   accwrapB.setSiteIdentifier(siteIdentifier);
   accwrapB.setPoIdentifier(poIdentifier);

   accwrapB.setSiteParent(siteParent);


   accwrapB.setSiteNotes(siteNotes);


  //*******************SET SITE ADDRESS DETAILS



   accwrapB.setAddPriSite(siteAddress);

   accwrapB.setAddCitySite(siteCity);

   accwrapB.setAddStateSite(siteState);

   accwrapB.setAddZipSite(siteZip);

   accwrapB.setAddCountrySite(siteCountry);

   accwrapB.setAddPhoneSite(sitePhone);

   accwrapB.setAddEmailSite(siteEmail);
   
   accwrapB.setSiteHidden(siteHidden);//KM  

   


   if (mode.equals("M")) {

	   accwrapB.setModifiedBy(usr);
	   accwrapB.setIpAdd(ipAdd);
	   ret = accwrapB.updateSite();

   }

   else if (mode.equals("N"))  {     

	accwrapB.setCreator(usr);
	accwrapB.setIpAdd(ipAdd);   
	ret = accwrapB.createSite();

   }



   if (ret >= 0) {

	   msg = MC.M_Site_DetsSvdSucc;/*msg = "Site details saved successfully";*****/

   }
   else if (ret == -3) {

	   msg = MC.M_SiteIdExst_EtrUnq;/*msg = "The Site ID you have entered already exists. Please enter a unique Site Id.";*****/

   } //Added by Manimaran to fix the Bug2729.  
   else if (ret == -4) {

	   msg = MC.M_OrgNameExst_EtrUnq;/*msg = "The Organization name you have entered already exists. Please enter a unique Organization name.";*****/
	   
   }  
   else {

	msg = MC.M_Site_DetsNotSvd;/*msg = "Site details not saved";*****/

   }  

%>
<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=msg%></p>
	<%
		if (ret == -3 || ret == -4) 
		{	
			 String agent1 = request.getHeader("USER-AGENT");
    		 agent1=agent1.toLowerCase();
    		
  			 if ((agent1.indexOf("mozilla") != -1) && (agent1.indexOf("gecko") <0)) 
  			 {

  	   	%>
  	 	<table width="70%" ><tr><td width="10%"></td><td width="60%" align="center">&nbsp;&nbsp;
  	 	<!-- Bug#9941 29-May-2012 -Sudhir-->  	  
  	   <button onclick="history.go(-1);return false;"><%=LC.L_Back%></button>	
		</td></tr>	</table> 
	 <%} else{
	 %>
	   <table  width="70%"  >	<tr><td width="10%"></td><td width="60%" align="center">&nbsp;&nbsp;
	   <!-- Bug#9941 29-May-2012 -Sudhir-->
		<button tabindex=2 onclick="history.go(-1);return false;"><%=LC.L_Back%></button>	
		</td></tr>	</table>
	<%}
		}
		else
		{
	%>	
		<META HTTP-EQUIV=Refresh CONTENT="1; URL=sitebrowser.jsp?srcmenu=<%=src%>">
    <%
		} // end of if for return value
//}//end of if for eSign check
} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</BODY>

</HTML>




