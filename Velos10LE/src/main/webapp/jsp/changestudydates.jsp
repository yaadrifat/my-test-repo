<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<title><%=MC.M_ChgStd_StEndDt%><%-- Change <%=LC.Std_Study%> Start and End Date*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
	<script>
function validate(formobj) {
 	if (!(validate_col('e-Signature',formobj.eSign))) return false;
    if(isNaN(formobj.eSign.value) == true) {
    	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
   }
//added for date field validation
     if (!(validate_date(formobj.startDate))) return false	
     if (!(validate_date(formobj.endDate))) return false	

//Added by gopu on 15th April 2005 for date field validation 
//modified by gopu on 26th April 2005 for date field validation fixing the bug 2132 / 2133

//modified by gopu on 05th May 2005 
	if ((formobj.endDate.value != null && formobj.endDate.value != '') && (formobj.startDate.value != null || formobj.startDate.value !=''))
	{
		if (CompareDates(formobj.startDate.value,formobj.endDate.value, '>'))
		{
				/*if (confirm("You are entering a '<%=LC.Std_Study%> End Date' that is less than the '<%=LC.Std_Study%> Start Date', would you like to proceed?"))*****/
				if (confirm("<%=MC.M_EtrStdEndDt_LessStdStrtDt%>"))  
				{}
				else 
				{
					formobj.startDate.focus();
					return false;
				}
		}		
	} 


		 
//condition for date field validation by JM on 07Apr05
// Commented by Gopu on 15th April 2005 for fixing the study end date is less than the study start date 
     
   /*  if ((formobj.startDate.value != null && formobj.startDate.value != '' )&&(formobj.endDate.value != null && formobj.endDate.value !=''))
	{
		
		if (CompareDates(formobj.startDate.value,formobj.endDate.value, '>'))
		{
		  	alert ("Study Start Date can not be greater than Study End Date");			
			formobj.startDate.focus();
			return false;
		}		
	}*/


return true;
} 

function fnChangeStartDate(frm, active) {
	if (frm.startDate.value=="") {
		var paramArray = [active];
		alert(getLocalizedMessageString("M_EtrStdStrtDt_StatEtr",paramArray));/*alert("To enter/edit the <%=LC.Std_Study%> Start Date, a status of "+active+" should have been entered.");*****/
		return false;
	} 	 
	fnShowCalendar(frm.startDate);
}
// INF-20084 Datepicker-- AGodara --%>
function fnChangeEndDate(frm, closure) {
	var paramArray = [closure];
	alert(getLocalizedMessageString("M_EtrStdDt_ShldBeenEtr",paramArray));/*alert("To enter/edit the <%=LC.Std_Study%> End Date, a status of "+closure+" should have been entered.");*****/
		return false;
}

</script>


<BODY>
<BR>
<DIV class="popDefault">
<P class="sectionHeadings"><%=LC.L_Std_ChgDt %><%-- <%=LC.Std_Study%> >> Change Dates*****--%> </P>
<jsp:useBean id ="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.esch.business.common.*,com.velos.eres.service.util.*"%>

<%
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession)) {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>
<%	
		int studyId = EJBUtil.stringToNum(request.getParameter("studyId"));
		studyB.setId(studyId);
		studyB.getStudyDetails();
		String studyStartDate = studyB.getStudyActBeginDate();
		String studyEndDate = studyB.getStudyEndDate();	
		 CodeDao  cd = new CodeDao();
		  cd.getCodeId("studystat","active");
		  String active = cd.getCodeDescription();
		  
		  cd.getCodeId("studystat","prmnt_cls");
		  		
   		  String closure = cd.getCodeDescription();
	%>

<Form class=formDefault id="formdefid" name="changeDates" action="updatestudydates.jsp" method="post" onSubmit="if (validate(document.changeDates)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}" >
<input type="hidden" name="studyId" value=<%=studyId%>>
<table>	
<%-- INF-20084 Datepicker-- AGodara --%>
	<tr>
		<td><%=LC.L_Std_StartDate%><%-- <%=LC.Std_Study%> Start Date*****--%></td>
		<td><Input type="text" value="<%=studyStartDate%>" name="startDate" class="datefield" size="12" readonly></td> 	
	</tr>
	<tr height=5></tr>
	<tr>
		<td><%=LC.L_Std_EndDate%><%-- <%=LC.Std_Study%> End Date*****--%></td>
		<td>
			<%if(studyEndDate.equals("")){ %>
				<Input type="text" value="<%=studyEndDate%>" name="endDate" size="12" onClick="return fnChangeEndDate(document.changeDates,'<%=closure%>')" readonly>
			<%}else{ %>
					<Input type="text" value="<%=studyEndDate%>" name="endDate" class="datefield" size="12" readonly></Input>
			<%} %>
		</td>
		<td></td>
	</tr>	
	<tr height=10>
	</tr>
	

	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="formdefid"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
		
</table>
</Form>

<%  

} else { //end of if body for session

%>
  <jsp:include page="timeout_childwindow.jsp" flush="true"/>
  <%
}

%>
</DIV>


<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</BODY>


