<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*,com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>

<TITLE><%=LC.L_Std_AccessRights%><%--<%=LC.Std_Study%> Access Rights*****--%></TITLE>


<SCRIPT Language="javascript">

function openwin1() {
	windowName=window.open("completepatientinfo.htm","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400");
}

 function  validate(formobj){

//     formobj=document.stdRights;



//JM: 22Apr2010: #4857
if ( formobj.defRoleRights.checked == true && formobj.role.value =="" ){

	alert("<%=MC.M_SelRole_ToSetRgt%>");/*alert("Please select a role to set default role rights");*****/
	return false;

}


	if (!(validate_col('e-Signature',formobj.eSign))) return false

     if(isNaN(formobj.eSign.value) == true) {
    	 alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }

}

</SCRIPT>


<!--Added by Manimaran for June Enhancement #GL2-->
<SCRIPT Language="javascript">

	var arr = new Array()
	function checkAll(formobj) {
		totcount = formobj.totalrows.value;
		if(formobj.All.checked) {
		   for (i=0;i<totcount;i++) {
                if(arr[i].substring(0,2) == "H_"){
				formobj.newr[i].checked = false;
				formobj.edit[i].checked = false;
				formobj.view[i].checked = false;
				formobj.rights[i].value =0;
			   }
			   else
    			if(arr[i] == 'STUDYREP')
			   {
				 formobj.newr[i].checked = false;
				 formobj.edit[i].checked = false;
				 formobj.view[i].checked = true;
				 formobj.rights[i].value =4;

			   }
   		      else
    			if(arr[i] == 'STUDYVPDET')
			   {
				 formobj.newr[i].checked = false;
				 formobj.edit[i].checked = false;
				 formobj.view[i].checked = true;
				 formobj.rights[i].value =4;

			   }
				else
    			if(arr[i] == 'STUDYSUM')
			   {
				 formobj.newr[i].checked = false;
				 formobj.edit[i].checked = true;
				 formobj.view[i].checked = true;
				 formobj.rights[i].value =6;

			   }

			   else
    			if(arr[i] == 'STUDYNOTIFY')
			   {
				 formobj.newr[i].checked = true;
				 formobj.edit[i].checked = false;
				 formobj.view[i].checked = true;
				 formobj.rights[i].value =5;

			   }

               else
			   {
				formobj.newr[i].checked = true;
				formobj.edit[i].checked = true;
				formobj.view[i].checked = true;
				formobj.rights[i].value =7;

			   }

           }
        }

		else
		{
			for (i=0;i<totcount;i++) {
				formobj.newr[i].checked = false;
				formobj.edit[i].checked = false;
				formobj.view[i].checked = false;
				formobj.rights[i].value =0;
            }
		}
	}


	function changeRights(obj,row,formobj)

	{

	  selrow = row ;

	  totrows = formobj.totalrows.value;

	  if (totrows > 1) 		rights =formobj.rights[selrow].value;

	  else 		rights =formobj.rights.value;

	  objName = obj.name;

	  if (obj.checked)
   	  {

       	if (objName == "newr")

	{

		rights = parseInt(rights) + 1;

		if (!formobj.view[selrow].checked)

		{

			formobj.view[selrow].checked = true;

			rights = parseInt(rights) + 4;

		}



	}


     	if (objName == "edit")
	{

		rights = parseInt(rights) + 2;

		if (!formobj.view[selrow].checked)

		{

			formobj.view[selrow].checked = true;

			rights = parseInt(rights) + 4;

		}

	}



     	if (objName == "view") rights = parseInt(rights) + 4;

	if (totrows > 1 )

		formobj.rights[selrow].value = rights;

	else

		formobj.rights.value = rights;

  }else

	{

       	if (objName == "newr") rights = parseInt(rights) - 1;

       	if (objName == "edit") rights = parseInt(rights) - 2;

       	if (objName == "view")
		{
			if (formobj.newr[selrow].checked)
			{
				alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
				formobj.view[selrow].checked = true;
			}

		    else if (formobj.edit[selrow].checked)
			{
				alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
				formobj.view[selrow].checked = true;
			}
			else
			{
			  rights = parseInt(rights) - 4;
			}
		}


	if (totrows > 1 ) {

		formobj.rights[selrow].value = rights;

    }
	else {

		formobj.rights.value = rights;

  	 }

  }
}


</SCRIPT>


<%
String src;

String groupId;



src= request.getParameter("srcmenu");

groupId = request.getParameter("groupId");

%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>



<BODY>
<br>
<jsp:useBean id="stdRights" scope="request" class="com.velos.eres.web.studyRights.StudyRightsJB"/>
<jsp:useBean id="modStdRights" scope="request" class="com.velos.eres.web.studyRights.StudyRightsJB"/>

<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="xtra" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>

<jsp:useBean id="grpB" scope="request" class="com.velos.eres.web.group.GroupJB"/>
<!-- Bug#15387 Fix for 1360*768 resolution - Tarun Kumar -->
<DIV class="formDefault tabDefBotN" id="div1" style="margin-top:-28px;">


  <%

	HttpSession tSession = request.getSession(true);

	int pageRight = 0;



	if (sessionmaint.isValidSession(tSession))

	{
	String uName = (String) tSession.getValue("userName");

	String tab = request.getParameter("selectedTab");
	String userName = null;

	//Check if budget super user screen need to be displayed after this screen
	String budRight=request.getParameter("budRight");
	budRight=(budRight==null)?"":budRight;

  	 GrpRightsJB gRights = (GrpRightsJB) tSession.getValue("GRights");
	pageRight = Integer.parseInt(gRights.getFtrRightsByValue("MGRPRIGHTS"));


	userName = request.getParameter("userName");


	//StudyRightsJB gRights = (GrpRightsJB) tSession.getValue("GRights");

     	//pageRight = Integer.parseInt(gRights.getFtrRightsByValue("STUDYTEAM"));

	//pageRight =7;


//JM: 13Apr2010: Enh #D-FIN2
 grpB.setGroupId(EJBUtil.stringToNum(groupId));
 grpB.getGroupDetails();
 String teamRole = grpB.getRoleId();



	   CodeDao cdRole = new CodeDao();

   	   cdRole.getCodeValues("role");


	if (pageRight > 0)

	{



%>
  <Form Name="stdRights" id="stdRightsFrm" method="post" action="updatesupuserrights.jsp" onSubmit="if (validate(document.stdRights)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

    <%int stdId = EJBUtil.stringToNum(groupId) ;

	//Integer.parseInt(request.getParameter("studyId"));

	stdRights.setId(stdId);%>
    <%

 	out.print("<Input type=hidden name=\"fk_study\" value="+stdId +">");

	stdRights.getGrpSupUserStudyRightsDetails();

	ctrl.getControlValues("study_rights");
	int rows = ctrl.getCRows();

	ArrayList feature =  ctrl.getCValue();
	ArrayList ftrDesc = ctrl.getCDesc();
	ArrayList ftrSeq = ctrl.getCSeq();
	int prevSeq =0;
	String str="";

	xtra.getControlValues("hid_rights");

	int xrows = xtra.getCRows();

	ArrayList xfeature =  xtra.getCValue();
	ArrayList xftrDesc = xtra.getCDesc();
	ArrayList xftrSeq = xtra.getCSeq();

	//get account module

	String modRight = (String) tSession.getValue("modRight");

	int modlen = modRight.length();

	acmod.getControlValues("module");
	int acmodrows = acmod.getCRows();

	ArrayList acmodfeature =  acmod.getCValue();
	ArrayList acmodftrDesc = acmod.getCDesc();
	ArrayList acmodftrSeq = acmod.getCSeq();
	ArrayList acmodftrRight = new ArrayList();
	String strR;

	ArrayList acmodValue = new ArrayList();


	for (int counter = 0; counter <= (modlen - 1);counter ++)
	{
		strR = String.valueOf(modRight.charAt(counter));
		acmodftrRight.add(strR);
	}

	modStdRights.setGrSeq(acmodftrSeq);
    modStdRights.setFtrRights(acmodftrRight);
	modStdRights.setGrValue(acmodfeature);
	modStdRights.setGrDesc(acmodftrDesc);

	// end of account module

	//Added by Manimaran for June Enhancement #GL2.
   for(int i = 0; i<feature.size();i++){
%>
   <SCRIPT Language="javascript">
      arr[<%=i%>]='<%=feature.get(i).toString()%>'
   </SCRIPT>
  <%
  }
  %>


    <Input type="hidden" name="totalrows" value= <%=rows%> >
    <Input type="hidden" name="src" value= <%=src%> >
    <Input type="hidden" name="groupId" value= <%=groupId%> >
    <Input type="hidden" name="budRight" value= <%=budRight%> >

    <P class="sectionHeadings"><%=LC.L_Assign_AccessRights%><%--Assign Access Rights*****--%>
	</P>
	<TABLE width="550" cellspacing="0" cellpadding="0" border="0" class="basetbl midAlign">
	  <tr><td colspan=4 align=right><A href=# onClick=openwin1()><%=MC.M_What_CompletePatData%><%--What is Complete <%=LC.Pat_Patient%> Data*****--%></A></td></tr>
	<tr>
	  <td><%=LC.L_Select_Role%><%--Select Role*****--%>: &nbsp;
          <%String role = 	cdRole.toPullDown("role",EJBUtil.stringToNum(teamRole), "0");%>
          <%=role%> </td>
	  <td colspan=4>

	  <input type="checkbox" name="defRoleRights" >
	  <%=MC.M_SelRgt_LnkRole%><%--Select default access rights linked with the selected role*****--%> </td>
	</tr>
	  <tr><td height="10" colspan="4"></td></tr>
	  <TH></TH>
      <TH><%=LC.L_New%><%--New*****--%></TH>
      <TH><%=LC.L_Edit%><%--Edit*****--%></TH>
      <TH><%=LC.L_View%><%--View*****--%></TH>
	  <!-- Modified by Amar for Bugzilla issue #3023 -->
	  <tr><td align="left" colspan="4"><input type="checkbox" name="All"  onClick="checkAll(document.stdRights)">  <%=LC.L_SelOrDeSel_All%><%--Select / Deselect All*****--%></td> </tr>
 <%
	 boolean showRight = false;

	 boolean showNew = true;
	 boolean showEdit = true;
	 boolean showView = true;

     int hidIndex = -1;
	 String xSeq = "-1";
	 String XRight = "-1";
	 String rights;
     for(int count=0;count<rows;count++){
     		String ftr = (String) feature.get(count);
			String desc = (String) ftrDesc.get(count);
			Integer seq = (Integer) ftrSeq.get(count);

			showNew = true;
			showEdit = true;
			showView = true;


			if ( (ftr.substring(0,2)).equals("H_") )
    		{
    			desc = "<br><b>" + desc + "</b>";
	    		showNew = false;
    			showEdit = false;
    			showView = false;
	    		rights = "0";
    	   	} else if ( (ftr.compareTo("STUDYSEC") == 0) || (ftr.compareTo("STUDYAPNDX") == 0)  || (ftr.compareTo("STUDYEUSR") == 0))
	    	{
    			desc = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + desc ;
    		}
			
		if (ftr.compareTo("STUDYEUSR") == 0)
		{
			desc = "";
		} 

			if ((stdRights.getFtrRights().size()) == 0)
			 	rights= "0";
			else rights = stdRights.getFtrRightsByValue(ftr);
				hidIndex = xfeature.indexOf(ftr);

	if (hidIndex >= 0)
	{
		xSeq = ((Integer) xftrSeq.get(hidIndex)).toString(); //the sequence of the xtra module
  		XRight =  (String) modStdRights.getFtrRightsBySeq(xSeq);
		//look for this right in acMod String
		if (XRight.compareTo("1") == 0)
		{
			showRight = true;
		}
		else
		{
			showRight = false;
		}
	}
	else
	{
		showRight = true;
	}
	%>
         <Input type="hidden" name=rights value= <%=rights%> >
    <%
		if (!showRight)
		{
		%>
		<tr>
		<td>
		<Input type="hidden" name="newr">
		<Input type="hidden" name="edit">
		<Input type="hidden" name="view">
		</td>
		</tr>
		<%
		}
		else
		{
	if(desc.equalsIgnoreCase("")){
		%>
		<TR style="display:none;">
		<%}else{ %>
		<TR>
		<%} %>

        <TD>
		<%= desc%>
		</TD>
          <%
     		//check for special rights

	       	if ((ftr.compareTo("STUDYSUM") == 0) || (ftr.compareTo("STUDYVPDET") == 0)  || (ftr.compareTo("STUDYREP") == 0))
			{
			  showNew = false;
			}

			if ( (ftr.compareTo("STUDYNOTIFY") == 0)  || (ftr.compareTo("STUDYREP") == 0) || (ftr.compareTo("STUDYVPDET") == 0)  )
			{
			  showEdit = false;
			}
			
			if (ftr.compareTo("STUDYEUSR") == 0)
			{
			  showView = false;
			  showNew = false;
			  showEdit = false;
			} 

			/*if (ftr.compareTo("STUDYNOTIFY") == 0)
			{
			  showView = false;
			} */


			//end of check


		  if (rights.compareTo("7") == 0){%>
           <TD align=center>
	  	    <%
			if (showNew)
			{
	  		%>
			   <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
			<%
		 	}
			else
			 {
			%>
           		<Input type="hidden" name="newr" value="0">
			<%
			 }
			%>
		  </TD>
          <TD align=center>
		  <%
			if (showEdit)
			{
    		%>
		       <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
			<%
			}
			else
			{
			%>
        		<Input type="hidden" name="edit" value="0">
			<%
			}
			%>
         </TD>
		 <TD align=center>
		   <%
		if (showView)
			{
    		%>
         	  <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
		  	<%
			}
			else
			{
			%>
         		<Input type="hidden" name="view" value="0">
			<%
			}
			%>
        </TD>
        <%}else if (rights.compareTo("1") == 0){ %>
        <TD align=center>
		 <%
			if (showNew)
			{
	  		%>
	          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
			<%
		 	}
			else
			 {
			%>
           		<Input type="hidden" name="newr" value="0">
			<%
			 }
			%>
		  </TD>
          <TD align=center>
		  <%
			if (showEdit)
			{
    		%>
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>,document.stdRights)" >
			<%
			}
			else
			{
			%>
        		<Input type="hidden" name="edit" value="0">
			<%
			}
			%>
         </TD>
		 <TD align=center>
		   <%
		if (showView)
			{
    		%>
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>,document.stdRights)">
		  <%
			}
			else
			{
			%>
         		<Input type="hidden" name="view" value="0">
			<%
			}
			%>
        </TD>
        <%}else if(rights.compareTo("3") == 0){%>
        <TD align=center>
		 <%
			if (showNew)
			{
	  		%>
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
   			<%
		 	}
			else
			 {
			%>
           		<Input type="hidden" name="newr" value="0">
			<%
			 }
			%>
		  </TD>
          <TD align=center>
		  <%
			if (showEdit)
			{
    		%>

	      <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
			<%
			}
			else
			{
			%>
        		<Input type="hidden" name="edit" value="0">
			<%
			}
			%>
         </TD>
		 <TD align=center>
		   <%
		if (showView)
			{
    		%>
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>,document.stdRights)">
		  <%
			}
			else
			{
			%>
         		<Input type="hidden" name="view" value="0">
			<%
			}
			%>
        </TD>
        <%}else if (rights.compareTo("2") == 0){%>
	   <TD align=center>
	    <%
			if (showNew)
			{
	  		%>
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>,document.stdRights)">
			<%
		 	}
			else
			 {
			%>
           		<Input type="hidden" name="newr" value="0">
			<%
			 }
			%>
		  </TD>
          <TD align=center>
		  <%
			if (showEdit)
			{
    		%>

          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
			<%
			}
			else
			{
			%>
        		<Input type="hidden" name="edit" value="0">
			<%
			}
			%>
         </TD>
		 <TD align=center>
		   <%
		if (showView)
			{
    		%>
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>,document.stdRights)">
		  <%
			}
			else
			{
			%>
         		<Input type="hidden" name="view" value="0">
			<%
			}
			%>
        </TD>
        <%}else if (rights.compareTo("4") == 0){%>
        <TD align=center>
		 <%
			if (showNew)
			{
	  		%>
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>,document.stdRights)">
			<%
		 	}
			else
			 {
			%>
           		<Input type="hidden" name="newr" value="0">
			<%
			 }
			%>
		  </TD>
          <TD align=center>
		  <%
			if (showEdit)
			{
    		%>
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>,document.stdRights)">
			<%
			}
			else
			{
			%>
        		<Input type="hidden" name="edit" value="0">
			<%
			}
			%>
         </TD>
		 <TD align=center>
		   <%
		if (showView)
			{
    		%>
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
		  <%
			}
			else
			{
			%>
         		<Input type="hidden" name="view" value="0">
			<%
			}
			%>
        </TD>
        <%}else if (rights.compareTo("5") == 0){%>
        <TD align=center>
		 <%
			if (showNew)
			{
	  		%>
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
			<%
		 	}
			else
			 {
			%>
           		<Input type="hidden" name="newr" value="0">
			<%
			 }
			%>
		  </TD>
          <TD align=center>
		  <%
			if (showEdit)
			{
    		%>
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>,document.stdRights)">
			<%
			}
			else
			{
			%>
        		<Input type="hidden" name="edit" value="0">
			<%
			}
			%>
         </TD>
		 <TD align=center>
		   <%
		if (showView)
			{
    		%>
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
		  <%
			}
			else
			{
			%>
         		<Input type="hidden" name="view" value="0">
			<%
			}
			%>
        </TD>
        <%}else if (rights.compareTo("6") == 0){%>
        <TD align=center>
		 <%
			if (showNew)
			{
	  		%>
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>,document.stdRights)" >
			<%
		 	}
			else
			 {
			%>
           		<Input type="hidden" name="newr" value="0">
			<%
			 }
			%>
		  </TD>
          <TD align=center>
		  <%
			if (showEdit)
			{
    		%>
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
			<%
			}
			else
			{
			%>
        		<Input type="hidden" name="edit" value="0">
			<%
			}
			%>
         </TD>
		 <TD align=center>
		   <%
		if (showView)
			{
    		%>
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
		  <%
			}
			else
			{
			%>
         		<Input type="hidden" name="view" value="0">
			<%
			}
			%>
        </TD>
        <%}else{ %>
        <TD align=center>
		 <%
			if (showNew)
			{
	  		%>
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>,document.stdRights)">
        			<%
		 	}
			else
			 {
			%>
           		<Input type="hidden" name="newr" value="0">
			<%
			 }
			%>
		  </TD>
          <TD align=center>
		  <%
			if (showEdit)
			{
    		%>

          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>,document.stdRights)">
			<%
			}
			else
			{
			%>
        		<Input type="hidden" name="edit" value="0">
			<%
			}
			%>
         </TD>
		 <TD align=center>
		   <%
		if (showView)
			{
    		%>
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>,document.stdRights)">
		  <%
			}
			else
			{
			%>
         		<Input type="hidden" name="view" value="0">
			<%
			}
			%>
        </TD>
        <%}%>
      </TR>
      <%}
	  	 }//end of show
	  %>
      <TR></tr>
	</table>

          <% if (pageRight > 4) {%>


		  <jsp:include page="submitBar.jsp" flush="true">
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="stdRightsFrm"/>
				<jsp:param name="showDiscard" value="N"/>
		  </jsp:include>


	    <%}%>


	<table>
      <TR>

        <TD COLSPAN =4 align="right">
          <% if (pageRight > 4)

		  { %>

		 <br>

          <% }

		else { %>

          <% } %>
        </TD>
      </TR>
    </TABLE>
  </Form>

  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right
}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu">
  <jsp:include page="getmenu.jsp" flush="true"/>
</div>
</BODY>

</HTML>







