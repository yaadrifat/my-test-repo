<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="portalB" scope="page" class="com.velos.eres.web.portal.PortalJB"/>
<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,java.text.*,com.velos.eres.service.util.*,com.velos.eres.web.portalDesign.*,com.velos.esch.web.portalForms.PortalFormsJB,com.aithent.audittrail.reports.AuditUtils"%>

<jsp:useBean id="PFormsB"  scope="request" class="com.velos.esch.web.portalForms.PortalFormsJB"/>
<%


	
	
	String calProtId = request.getParameter("paramprotCalId");
	
	String calProtIdOld = request.getParameter("paramprotCalIdOld");	
	
	if (calProtId==null) calProtId = "";
	
	String eSign = request.getParameter("eSign");
	String portalId = request.getParameter("portalId");
        
	String fmodType = request.getParameter("rdForm");
        if (fmodType==null) fmodType="";
    String smodType = request.getParameter("rdpatSchedule");
        if (smodType==null) smodType="";
    String selFormIds = request.getParameter("paramFormIds"); // can be used to hold the comma separated Form ids
        if (selFormIds==null) selFormIds="";
    
    
	String fmodAlign = request.getParameter("align");
		if (fmodAlign==null) fmodAlign="";	
	String smodAlign = request.getParameter("status");
		if (smodAlign==null) smodAlign="";
	
	String pmFrom = request.getParameter("pmFrom");	
	if (pmFrom==null) pmFrom="";
	String pmTo = request.getParameter("pmTo");
	if (pmTo==null) pmTo="";
	String pmFromUnit = request.getParameter("pmFromUnit");
	String pmToUnit = request.getParameter("pmToUnit");
	if ((pmFrom.equals("")||pmFrom.equals("0")) && (pmTo.equals("") || pmTo.equals("0"))){
		pmFromUnit = "";
		pmToUnit = "";
	}
	
	
	String selfLogout = "";
	
	selfLogout = request.getParameter("selfLogout");
	
	if (StringUtil.isEmpty(selfLogout))
	{
		selfLogout = "0";
	}
	
	 
	
	String designBgColor = request.getParameter("designBgColor");
	String designHeaderInfo = request.getParameter("designHeaderInfo");
	String designFooterInfo = request.getParameter("designFooterInfo");
	String mode = request.getParameter("mode");
	String selConsentFormId = request.getParameter("selConsentFormId");
	
	if (StringUtil.isEmpty(selConsentFormId))
	{
		selConsentFormId = null;
	}
	
	
	String pkFormId = request.getParameter("pkFormId");
	if (pkFormId==null) pkFormId="";
	String pkCalId = request.getParameter("pkCalId");
	if (pkCalId==null) pkCalId="";
	
	String date = request.getParameter("rdDates");
	
	String portalFormAfterResp = request.getParameter("portalFormAfterResp");
	
	
	
	//JM: ------5654: 27DEC2010------------------------------------------------
	 
	 String fromFormAccPg =request.getParameter("fromFormAccessPage");
	 fromFormAccPg = (fromFormAccPg==null)?"false":fromFormAccPg;
	 String totalCount = "";
	 
	 String checkVal = "";
	 String portalformKey = "";
	 String portalEvtId = "";
	 String portalFormId = "";	 
	 
	 String portalformKeys = null;
	 String portalEvtIds = null;
	 String portalFormIds = null;
	 String checkVals = null;	 

	 int deletedId = 0;	 
	 int  keyIndx, evtIdIndx, pfIndx, chkValIndx = 0;	 
	
	 
	 //if (fromFormAccPg.equals("true")) {		
			portalformKeys =request.getParameter("portalformKey");//pks
			portalformKeys=portalformKeys+",";
				   
			portalEvtIds=request.getParameter("hiddenEvtId");
			portalEvtIds=portalEvtIds+",";
			
			portalFormIds=request.getParameter("hiddenFormId");
			portalFormIds=portalFormIds+",";
			
			checkVals=request.getParameter("inclChks");
			checkVals=checkVals+",";
			
			totalCount=request.getParameter("pfCounter");
	 //}

			String clearAll=request.getParameter("clrAll");
			clearAll=(clearAll==null)?"0":clearAll;
			//Bug#15827
			if(clearAll.equals("1")){
				if(selFormIds=="")
		        fmodType="";
				if(calProtId=="")
		        smodType="";	
			}
	
	
		    	
	HttpSession tSession = request.getSession(true); 
	String ipAdd = (String) tSession.getValue("ipAdd");
     	String usr = null;
	usr = (String) tSession.getValue("userId");
	String acc = (String) tSession.getValue("accountId");
	int setId=0;
	int uId = -1;//JM:
	int uFormat = -1;//JM:
	
	if (sessionmaint.isValidSession(tSession)) {	
		%>
		<jsp:include page="sessionlogging.jsp" flush="true"/>
		<%   
		String oldESign = (String) tSession.getValue("eSign");

		if(!oldESign.equals(eSign)) {
			
		
		%>
			<jsp:include page="incorrectesign.jsp" flush="true"/>
		<%
		} else {		
			
			if (StringUtil.isEmpty(date))
			{
				date = "";
			}
			if (date.equals("S")){
		    	pmFrom = "";
		    	pmTo = "";
		    	pmFromUnit = "";
		    	pmToUnit = "";
		    }
			
			
			int delCntr = 0;		    
			int deletedCals	= 0;			
			
			int loopCntr = 0;
			String delFormCalId=request.getParameter("patSchelinkFormCalId");
			if ("2".equals(clearAll)){
				deletedCals=PFormsB.removeMulCalForms(EJBUtil.stringToNum(portalId),delFormCalId ,AuditUtils.createArgs(tSession,"",LC.L_Portal_Admin));
			}
			if (!fmodType.equals("")&& !smodType.equals("")){				
				loopCntr = 2;				
			}else if (fmodType.equals("")&& smodType.equals("")){
				loopCntr = 0;				
				if ("1".equals(clearAll)){
					// Modified for INF-18183 ::: AGodara
					deletedCals=PFormsB.removeCalFromPortal(EJBUtil.stringToNum(portalId), -1,AuditUtils.createArgs(tSession,"",LC.L_Portal_Admin));	//JM: 15Mar2011, #5742
				}
				
			}else {
				loopCntr = 1;
			}
				
			
		    String[] fkIds = new String[loopCntr]; 
		    String[] modTyp = new String[loopCntr];
		    String[] modAlign = new String[loopCntr];
		    String[] portalModIdPks = new String[loopCntr];
		    
		    
		    
		    //JM: 17Aug2007: #3061
		    PortalDesignJB portalDsnDelB = new PortalDesignJB();
		    
		    //Reset form(s) alone
		    if (fmodType.equals("")&& smodType.equals("") && (!pkFormId.equals("")&& pkCalId.equals(""))){
		    	// Modified for INF-18183 ::: AGodara
		    	uId = portalDsnDelB.deletePortalModules(EJBUtil.stringToNum(pkFormId),AuditUtils.createArgs(tSession,"",LC.L_Portal_Admin)); 
		    	loopCntr = 0;
		    }
   		    //Reset caledar(s) alone
		    if (fmodType.equals("")&& smodType.equals("") && (pkFormId.equals("")&& (!pkCalId.equals("")))){
		    	// Modified for INF-18183 ::: AGodara
		    	uId = portalDsnDelB.deletePortalModules(EJBUtil.stringToNum(pkCalId),AuditUtils.createArgs(tSession,"",LC.L_Portal_Admin)); 
		    	loopCntr = 0;
		   	}
	   	    //Reset form(s) and calendar(s) both
		   	if (fmodType.equals("")&& smodType.equals("") && (!pkFormId.equals("")&& (!pkCalId.equals("")))){
		   	// Modified for INF-18183 ::: AGodara
		    	uId = portalDsnDelB.deletePortalModules(EJBUtil.stringToNum(pkFormId),AuditUtils.createArgs(tSession,"",LC.L_Portal_Admin)); 
		    	uId = portalDsnDelB.deletePortalModules(EJBUtil.stringToNum(pkCalId),AuditUtils.createArgs(tSession,"",LC.L_Portal_Admin)); 
		    	loopCntr = 0;
		   	}		    
		    //Reset either existing form(s)/calendar(s), and also add calendar(s) --(OR)-- 
		    //Reset existing form(s) & calendar(s), and also add calendar(s)
		    if (fmodType.equals("")&& (!smodType.equals("")) && (  !pkFormId.equals("") || (!pkFormId.equals("")&& pkCalId.equals("")))){
		    	// Modified for INF-18183 ::: AGodara
		    	uId = portalDsnDelB.deletePortalModules(EJBUtil.stringToNum(pkFormId),AuditUtils.createArgs(tSession,"",LC.L_Portal_Admin)); 
		    	loopCntr = 1;
		    }
		    //Reset either existing form(s)/calendar(s), and also add form(s)  --(OR)-- 
		    //Reset existing form(s) & calendar(s), also add form(s) 
		    if (!fmodType.equals("")&& smodType.equals("") && ( !pkCalId.equals("") || (pkFormId.equals("")&& (!pkCalId.equals(""))))){
		    	// Modified for INF-18183 ::: AGodara
		    	uId = portalDsnDelB.deletePortalModules(EJBUtil.stringToNum(pkCalId),AuditUtils.createArgs(tSession,"",LC.L_Portal_Admin)); 
		    	loopCntr = 1;
		    }
		    //Reset existing form(s) & calendar(s), add form(s) and calendar(s) 
		    //above case taken into consideration while (loopCntr==2) 	    
		    	    
		    
		    
		    if (loopCntr==1){		    	
		    	
		    	  if (fmodType.equals("")&& (!smodType.equals(""))){
		    		
		    		fkIds[loopCntr-1] = calProtId;		    
				    
				    modTyp[loopCntr-1] = smodType;
				    
				    modAlign[loopCntr-1] = smodAlign;
				    
				    portalModIdPks[loopCntr-1] = pkCalId;
		    	}
			      if ((!fmodType.equals(""))&& smodType.equals("")){	
					
			    	fkIds[loopCntr-1] = selFormIds;		    
				    
				    
				    modTyp[loopCntr-1] = fmodType;
				    
				    modAlign[loopCntr-1] = fmodAlign;
				    
				    portalModIdPks[loopCntr-1] = pkFormId;
		    	}
		    	
		    }else if (loopCntr==2){
		    
		    fkIds[0] = selFormIds; 		    	fkIds[1] = calProtId;		    
		    
		    modTyp[0] = fmodType; 		    	modTyp[1] = smodType;
		    
		    modAlign[0] = fmodAlign;	    	modAlign[1] = smodAlign;
		    
		    portalModIdPks[0] = pkFormId;	    portalModIdPks[1] = pkCalId;
		    
		    }
		    
		    
		    
		    
		    for (int i = 0; i < loopCntr ; i++){	 
		    
		    	PortalDesignJB portalDesignB = new PortalDesignJB();
		    	
		    	
			   if(mode.equals("M")) {	
			   	if (!portalModIdPks[i].equals("")){
			   		portalDesignB.setPortalModId(EJBUtil.stringToNum(portalModIdPks[i]));
			   		portalDesignB.getPortalDesignDetails();
			   	}else{
				    mode = "MN";
			   	}
			   }
			   portalDesignB.setFkPortalId(portalId);
			   portalDesignB.setFkId(fkIds[i]);  //selFormIds 
	           portalDesignB.setPortalModType(modTyp[i]);	//modType
         	   portalDesignB.setPortalModAlign(modAlign[i]);	//modAlign
               
         	  if(modTyp[i].equals("S") || modTyp[i].equals("SF")){
  			   portalDesignB.setPortalModFrom(pmFrom);
			   portalDesignB.setPortalModTo(pmTo);
			   portalDesignB.setPortalModFromUnit(pmFromUnit);
			   portalDesignB.setPortalModToUnit(pmToUnit);
         	  }
			  
         	//JM: 15DEC2010: Enh-#PP-1: In the SCH_PORTAL_FORMS table only keep the current calendar against the portal and delete the other calendar if exists      	   
         			//int deletedCals	= 0;
         	// Modified for PP-18306 ::: AGodara	
         	  if ((!calProtIdOld.equals("")) && (!calProtIdOld.equals(calProtId)) && modTyp[i].equals("SF")){
         			 PFormsB.removeMulCalForms(StringUtil.stringToNum(portalId), calProtId,AuditUtils.createArgs(tSession,"",LC.L_Portal_Admin));
        		}else if ( !modTyp[i].equals("SF")){
						delCntr = delCntr + i; 
					}
         			
         			//JM: 25Mar2011: #5742
         			// Modified for INF-18183 ::: AGodara
         			if (delCntr > 0){
         				deletedCals=PFormsB.removeCalFromPortal(EJBUtil.stringToNum(portalId), -1,AuditUtils.createArgs(tSession,"",LC.L_Portal_Admin));
         			}
         			
         			
		  	   if(fmodType.equals("EF") || fmodType.equals("LF"))
			    {
			    	portalDesignB.setPortalFormAfterResp(portalFormAfterResp);
			     }	    			

               if(mode.equals("M")) {
				   //JM: 31Aug2007: added last modified by
				   portalDesignB.setModifiedBy(usr);
			   	   uId=portalDesignB.updatePortalDesign();			   	 
               }else if (mode.equals("MN")){
               
               	//JM: 31Aug2007: added, creator and ip address
				 portalDesignB.setCreator(usr);				  
			     portalDesignB.setIpAdd(ipAdd);	
				 setId = portalDesignB.setPortalDesignDetails();
				 mode="M";
			   }               
 		    }
			   
			   portalB.setPortalId(EJBUtil.stringToNum(portalId));
 
			   portalB.getPortalDetails();
	
			   if(portalB.getModifiedBy()==null  ){
	
			   portalB.setModifiedBy(usr);
			   }
			   portalB.setPortalBgColor(designBgColor);
			   portalB.setPortalHeader(designHeaderInfo);
			   portalB.setPortalSelfLogout(selfLogout);
			   
 			   portalB.setPortalFooter(designFooterInfo);
 			   
 			   portalB.setPortalConsentingForm(selConsentFormId); 
 			   					   
			   uFormat = portalB.updatePortal();
			   
			   
			   
			   
			 //JM: ------5654: 27DEC2010------------------------------------------------
			 if ((setId > 0 || uId==0) && EJBUtil.stringToNum(totalCount)>0){
			   for (int j=0; j<EJBUtil.stringToNum(totalCount); j++){				
				   
				  
				   
				   if (portalformKeys.indexOf(",") >=0){
				   keyIndx = portalformKeys.indexOf(",");
				   portalformKey = portalformKeys.substring(0,keyIndx);
				   portalformKeys = portalformKeys.substring(keyIndx+1, portalformKeys.length());
				   }
				   
				   evtIdIndx = portalEvtIds.indexOf(",");
				   portalEvtId = portalEvtIds.substring(0,evtIdIndx);
				   portalEvtIds = portalEvtIds.substring(evtIdIndx+1, portalEvtIds.length());	 
				   
				   pfIndx = portalFormIds.indexOf(",");
				   portalFormId = portalFormIds.substring(0,pfIndx);
				   portalFormIds = portalFormIds.substring(pfIndx+1, portalFormIds.length());				   
				   
				   if (checkVals.indexOf(",") >=0){
				   chkValIndx = checkVals.indexOf(",");
				   checkVal = checkVals.substring(0,chkValIndx);
				   checkVals = checkVals.substring(chkValIndx+1, checkVals.length());	   
				   }
				   
				    			
					
					
					
					if (portalformKey.equals("0") && "Y".equals(checkVal)){
						PortalFormsJB pfJB = new PortalFormsJB();
						
						pfJB.setPortalId(portalId);
						//pfJB.setCaledarId(calProtId);
						pfJB.setEventId(portalEvtId);
						pfJB.setFormId(portalFormId);
						pfJB.setCreator(usr);
						pfJB.setIpAdd(ipAdd);
						pfJB.setPortalForms();
						
					}else if (!portalformKey.equals("0")){
						PortalFormsJB portFrmJB = new PortalFormsJB();
						
						portFrmJB.setPortalFormId(EJBUtil.stringToNum(portalformKey));
						portFrmJB.getPortalFormDetails();
						
						//YK:01/12/13 Bug#13215
						if("N".equals(checkVal)){
							deletedId = portFrmJB.removePortalFormId(EJBUtil.stringToNum(portalformKey),AuditUtils.createArgs(session,"",LC.L_Portal_Admin));
						}
						/*if ("Y".equals(checkVal)){
							portFrmJB.setFormId(portalFormId);
						}else{	
							portFrmJB.setFormId("0");//blank form id saved

							// Modified for INF-18183 ::: Raviesh
							deletedId = portFrmJB.removePortalFormId(EJBUtil.stringToNum(portalformKey),AuditUtils.createArgs(session,"",LC.L_Portal_Admin));

						}
						portFrmJB.setModifiedBy(usr);
						portFrmJB.setIpAdd(ipAdd);
						portFrmJB.updatePortalForms();*/
						
					}
					
					
				}
			 }
			 
			 
			 
			 
			   
			 
			 //JM: ------5654: 27DEC2010------------------------------------------------			   
			   
			   
if((setId > 0) || (uId==0) || (uFormat==0) )
  {
%>
  <br>
  <Br>
  <br>
  <Br>
  <p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
  <script>
  window.opener.location.reload();  
  setTimeout("self.close()",1000); 
  </script>
<%
} 

else

{

%>

<br>

<br>

<br>

<br>

<br>

<p class = "successfulmsg" align = center> <%=MC.M_DataCnt_Svd%><%--Data could not be saved.*****--%></p>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=portalDesign.jsp?srcmenu=tdMenuBarItem2&portalId=<%=portalId%>">


<%

}

		}// end of if body for e-sign 
	}//end of if body for session
	else {
	%>
		<jsp:include page="timeout.html" flush="true"/>
	<%
	} 
	%>
</BODY>
</HTML>
