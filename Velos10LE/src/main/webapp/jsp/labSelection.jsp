<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Add_NewLabs%><%--Add New Labs*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<script>
function setNavigation(formobj,val)
{
 formobj.navigation.value=val;
}

function  validate(formobj)
 {
  	num = formobj.numDsel.value ;
	if (num==0){
	alert("<%=MC.M_SelLabTests_BeforeSbmt%>");/*alert("Please Select Lab Tests before Submitting");*****/
	return false;
	}
	return true;
	//this.close();
}


function openWin(Id,flag,formobject)
{
	 if ( (flag =="ET") || (flag=="EN") || (flag=="ED" ) || (flag=="EI") )
	{

		param1="editBox.jsp?fieldLibId="+Id+"&mode=RO";

		windowName= window.open(param1,"FieldDetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=600,height=600");
		windowName.focus();
		return ;
	}

	else if ( (flag =="ML")  || (flag=="MD") || (flag=="MC") || (flag=="MR") )
	{


		param1="multipleChoiceBox.jsp?fieldLibId="+Id+"&mode=RO";
		windowName= window.open(param1,"FieldDetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=600,height=600");
		windowName.focus();
		return ;
	}



}
function selectLab(formobj){
		num = formobj.numSel.value;
		group=document.labsearch.groupName.value;
		formobj.groupName.value=group;
		navigation=formobj.navigation.value;
	for (i=0;i<num;i++)
		{
			if (num > 1)
			{
				if (formobj.selected[i].checked)
				{	formobj.action="labSelection.jsp#"+navigation;
					formobj.target="";
					formobj.submit();
					return true;
				}
			}
			else
			{
				if (formobj.selected.checked)
				{	formobj.action="labSelection.jsp#"+navigation;
					formobj.target="";
					formobj.submit();
					return true;
				}
			}
	 }
	alert("<%=MC.M_PlsSel_Tab%>");/*alert("Please Select a Lab.");*****/
	return false;
}
function deSelectLab(formobj){
 num = formobj.numDsel.value ;
 group=document.labsearch.groupName.value;
formobj.groupName.value=group;
		for (i=0;i<num;i++)
		{
			if (num > 1)
			{
				if (formobj.deselected[i].checked)
				{
					formobj.action="labSelection.jsp";
					formobj.target="";
					formobj.submit();
					return true;

				}
			}
			else
			{
				if (formobj.deselected.checked)
				{
					formobj.action="labSelection.jsp";
					formobj.target="";
					formobj.submit();
					return true;
				}
			}
	}
	alert("<%=MC.M_PlsDesel_Lab%>");/*alert("Please Deselect a Lab.");*****/
	return false;

}
function setMode(formobject, flag)
{
	
	if(flag=="R")
	{
		formobject.refreshFlag.value="captureIdsSel" ;

		num = formobject.numSel.value ;
		for (i=0;i<num;i++)
		{
			if (num > 1)
			{
				if (formobject.selected[i].checked)
				{
					self.close();
					return true;
				}
			}
			else
			{
				if (formobject.selected.checked)
				{
					self.close();
					return true;
				}
			}
	 }
	alert("<%=MC.M_Selc_Fld%>");/*alert("Please Select a Field.");*****/
	return false;



	}
	if(flag=="L")
	{

		formobject.refreshFlag.value="captureIdsDSel" ;
		 num = formobject.numDsel.value ;
		for (i=0;i<num;i++)
		{
			if (num > 1)
			{
				if (formobject.deselected[i].checked)
				{
					self.close();
					return true;
				}
			}
			else
			{
				if (formobject.deselected.checked)
				{
					self.close();
					return true;
				}
			}
	}
	alert("<%=MC.M_Deselect_Fld%>");/*alert("Please Deselect a Field.");*****/
	return false;




	}
	if(flag=="S")
	{
		formobject.refreshFlag.value="submit";
		if (validate(formobject))
		{
		formobject.submit();
		self.close();
		}
		else
			return false;
	}
	if(flag=="SE")
	{
		formobject.mode.value="final" ;
	}


}




</script>
<body>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="labJB" scope="session" class="com.velos.eres.web.lab.LabJB"/>

<%@ page language = "java" import = "com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB"%>
<jsp:include page="include.jsp" flush="true"/>
<%
	//String selected[]=request.getParameterValues("selected");

%>


<%

   HttpSession tSession = request.getSession(true);
   int pageRight = 0;
   GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
   //pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFRMLIB"));

%>

<DIV class="popDefault" id="div1">

<%
	if (sessionmaint.isValidSession(tSession))
	{

		String labId="",src="";
		String catName="";
		String labName="";
		String labCatId="";
		String nciId="";
		String gradeCalc="";
		String groupNamePullDown="",refreshFlag="";
		ArrayList labIds= null;
		ArrayList labCatIds= null;
		ArrayList labNames= null;
		ArrayList labCatNames= null;
		ArrayList selectList=null;
		ArrayList selectTemp=null;
		ArrayList deselectList=null;
		ArrayList labNciIds=null;
		ArrayList labGradeCalc=null;
		String studyId="";
		int counter = 0;
		int len=0,index=-1;
		String selected[] = request.getParameterValues("selected");
		String deselected[]=request.getParameterValues("deselected");
		//get previously selected labs list
		String selectAll[]= request.getParameterValues("select");
		if (selectAll!=null) selectList=EJBUtil.strArrToArrayList(selectAll);
		if (!(selected==null))	selectTemp=EJBUtil.strArrToArrayList(selected);
		if (!(selectTemp==null)){
		if (!(selectList==null)){
			for(int i=0;i<selectTemp.size();i++){
				selectList.add(selectTemp.get(i));
			}

		}else {
			selectList=selectTemp;
		}
		}
		if (!(deselected==null)) deselectList=EJBUtil.strArrToArrayList(deselected);
		//remove the deselected values from selected list
		if (!(selectList==null))
				{
				for(int i=0;i<selectList.size();i++)
				{
					System.out.println("select"+selectList.get(i));


				}

					}


			if (!(deselectList==null))
				{
				for(int i=0;i<deselectList.size();i++)
				{
					System.out.println("deselect"+deselectList.get(i));
					index=selectList.indexOf(deselectList.get(i));
					System.out.println(index);
					if (index>-1)		selectList.remove(index);

				}

					}

		//String deselectedNames[] = request.getParameterValues("deselectedName");
		request.setAttribute("selectList",selectList);
		String uName = (String) tSession.getValue("userName");
		labCatId=request.getParameter("groupName");
		String accountId=(String)tSession.getAttribute("accountId");
		String mode= request.getParameter("mode");
		String pkey = request.getParameter("pkey");
		String patProtId=request.getParameter("patProtId");
		studyId=request.getParameter("studyId");
		studyId=(studyId==null)?"":studyId;
		src= request.getParameter("srcmenu");
		System.out.println("src" + src);
		String fromLab=request.getParameter("fromlab");
		fromLab=(fromLab==null)?"":fromLab;
		String selectedTab = request.getParameter("selectedTab");
		String patientCode = request.getParameter("patientCode");
			if (patientCode == null)
			patientCode = "";


		if (labId == null )
		{
			labId="";
		}

		if (labCatId == null )
		{
			labCatId="" ;
		}


		//here we will get all the categories
		LabDao labdao=new LabDao();
		labdao=labJB.groupPullDown(labCatId);
		groupNamePullDown=labdao.getPullDown();
		if (labCatId=="") labdao=labJB.getData();
		else labdao=labJB.getData(labCatId);
		labIds= labdao.getLabIds();
		labCatIds= labdao.getGroupIds();
		labNames= labdao.getLabNames();
		labCatNames=labdao.getGroupNames();
		labNciIds=labdao.getNciIds();
		labGradeCalc=labdao.getGradeCalc();
		len=labIds.size();
		System.out.println("length of arraylist"+len);

		String specimenPk=request.getParameter("specimenPk");
		specimenPk=(specimenPk==null)?"":specimenPk;

		String calldFrom=request.getParameter("calledFromPg");
		calldFrom=(calldFrom==null)?"":calldFrom;

%>
     <Form name="labsearch" action="labSelection.jsp"  method="POST">
		<Input type="hidden" name="mode" value=<%=mode%> >
		<input type="hidden" name="pkey" value="<%=pkey%>">
		<input type="hidden" name="patProtId" value=<%=patProtId%>>
		<input type="hidden" name="studyId" value=<%=studyId%>>
		<input type="hidden" name="srcmenu" value=<%=src%>>
		<input type="hidden" name="mode" value="select">
		<input type="hidden" name="fromlab" value="<%=fromLab%>">
		<input type="hidden" name="selectedTab" value="<%=selectedTab%>">
		<input type="hidden" name="patientCode" value="<%=patientCode%>">
		<input type="hidden" name="calledFromPg" value="<%=calldFrom%>">
		<input type="hidden" name="specimenPk" value="<%=specimenPk%>">


		<P class="defComments"><%=MC.M_Search_FldBased%><%--Search fields based on*****--%>:</P>
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
   				 <tr>
				      <td class="defComments"><%=LC.L_Category%><%--Category*****--%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=groupNamePullDown%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit"><%=LC.L_Search%></button></td>
<!--			          <td width="23%"><%=groupNamePullDown%> </td>
				  <td width ="10%"><Input type="image" src="../images/jpg/smallGo.gif" border="0" align="absmiddle">  </td>
			-->
					  </tr>
					<%if (selectList!=null){
		for(int i=0;i<selectList.size();i++){%>
			<Input type="hidden" name="select" value="<%=(String)selectList.get(i)%>">
		<%}}%>
			</table>

				</FORM>

			<table width="100%" border="0">
				<tr>
				<td  width="50%">
				<!-- THE COLUMN FOR THE BROWSER -->
			<!--<Form name="labselect" action="labdata.jsp?page=Demographics"  method="post" onsubmit="return validate(document.labselect)"  target="_opener">-->

			<%if (calldFrom.equals("specimen")){%>

			<Form name="labselect" id="labSelFrm" action="labdata.jsp?page=specimen"  method="post" target="labbrowse" onsubmit="if (setMode(document.labselect,'S')== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return false;}">
			<%}else{%>

			<Form name="labselect" id="labSelFrm" action="labdata.jsp?page=patient"  method="post" target="labbrowse" onsubmit="if (setMode(document.labselect,'S')== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return false;} " >
			<%}%>

			<input type="hidden" name="refreshFlag" value=<%=refreshFlag%>>
				<input type="hidden" name="pkey" value="<%=pkey%>">
				<input type="hidden" name="patProtId" value=<%=patProtId%>>
				<input type="hidden" name="studyId" value=<%=studyId%>>
				<input type="hidden" name="srcmenu" value=<%=src%>>
				<input type="hidden" name="mode" value="select">
				<input type="hidden" name="selectedTab" value="<%=selectedTab%>">
				<input type="hidden" name="groupName" value="">
				<input type="hidden" name="patientCode" value="<%=patientCode%>">
				<input type="hidden" name="fromlab" value="<%=fromLab%>">
				<!--JM: 07Jul2009: #INVP2.11 -->
				<input type="hidden" name="specimenPk" value="<%=specimenPk%>">
				<input type="hidden" name="calledFromPg" value="<%=calldFrom%>">


				<%if (selectList!=null){
		for(int i=0;i<selectList.size();i++){%>
			<Input type="hidden" name="select" value="<%=(String)selectList.get(i)%>">
		<%}}%>
			  <div style="overflow:auto;overflow-x:hidden; height:400; border:groove;">

				<table width="100%" cellspacing="1" cellpadding="0" border="0" align="">
				    <tr>
						<th width="10%"> <%=LC.L_Select%><%--Select*****--%> </th>
				        <th width="30%"> <%=LC.L_Cat_Name%><%--Category Name*****--%> </th>
				        <th width="60%"><%=LC.L_Lab_Name%><%--Lab Name*****--%> </th>


					</tr>

		<%

					for(counter = 0;counter<len;counter++)
					{


						mode="captureIds" ;

						labName = ((labNames.get(counter)) == null)?"-":(labNames.get(counter)).toString();
						labId = ((labIds.get(counter)) == null)?"-":(labIds.get(counter)).toString();
						catName=((labCatNames.get(counter)) == null)?"-":(labCatNames.get(counter)).toString();
						labCatId=((labCatIds.get(counter)) == null)?"-":(labCatIds.get(counter)).toString();
						nciId=((labNciIds.get(counter)) == null)?"-":(labNciIds.get(counter)).toString();
						gradeCalc=((labGradeCalc.get(counter)) == null)?"-":(labGradeCalc.get(counter)).toString();

						String selFields = StringUtil.encodeString(labId+";"+labName+";"+catName+";"+labCatId+";"+nciId+";"+gradeCalc);

						if ((counter%2)==0)
						{

						%>

					      	<tr class="browserEvenRow">

						<%}else
						 { %>

				      		<tr class="browserOddRow">

						<%}  %>



						<td><input type="checkbox" name="selected" value="<%=selFields%>" onClick="setNavigation(document.labselect,'<%=labId%>')"> </td>

						<td> <%=catName%><A name="<%=labId%>"></A></td>
						<td><%=labName%></td>

			    	 	</tr>

					<%
			}//for loop


					%>


				</table>

				<input type="hidden" name="numSel" value="<%=len%>" >
			  </div>

			</td>	<!-- END OF THE TD OF THE FIELD BROWSER -->


			<td width="5%"> <!-- TD FOR THE ARROWS BMP -->

			<A href="#" onClick="selectLab(document.labselect)"><img src="../images/jpg/formright.gif"  align="absmiddle"   border="0"></img></A>
				<!--<input  type="image" src="../images/jpg/formright.gif" onClick="" align="absmiddle"   border="0">-->
				<br>
				<br>
				<A href="#" onClick="deSelectLab(document.labselect)"><img src="../images/jpg/formleft.gif"  align="absmiddle"   border="0"></img></A>
				<!--<input  type="image" src="../images/jpg/formleft.gif" onClick="" align="absmiddle"   border="0">-->
			</td><!-- TD FOR THE ARROWS BMP-->

			<td width="50%">

			<div style="overflow:auto;overflow-x:hidden; height:400;border:groove;" >
			<table width="100%" cellspacing="1" cellpadding="0" border="0" align="left">
			    <tr>
						<th width="10%"> <%=LC.L_DehypenSelect%><%--De-Select*****--%> </th>
				          <th width="30%"> <%=LC.L_Cat_Name%><%--Category Name*****--%> </th>
				        <th width="60%"> <%=LC.L_Lab_Name%><%--Lab Name*****--%></th>
					</tr>
					<%
					int cnt=0;
					String deselectedName = "";
					String tmp = "";
					if (!(selectList==null))
					{
					   //cnt =deselected.length;
					   cnt=selectList.size();
					}
					String deselId = "";
					String deselName = "";
					String deselUniqId = "",deselCatId="";
					String deselNciId="",deselGradeCalc="";
					int strlen  =0 ;
					int secondpos  = 0 ;
					int firstpos  = 0 ,thirdpos=0;
					String temp="";

					for(counter = 0;counter<cnt;counter++)
					{
						temp=StringUtil.decodeString(((String)selectList.get(counter)));
						StringTokenizer st=new StringTokenizer(temp,";");
						/* strlen = (temp).length();
						 firstpos = (temp).indexOf(";",1);
						 deselId = (temp).substring(0,firstpos);
						 secondpos = (temp).indexOf(";",firstpos+1);
						 deselName = (temp).substring(firstpos+1,secondpos);
						 thirdpos=(temp).indexOf(";",secondpos+1);
						 deselUniqId=(temp).substring(secondpos+1,thirdpos);
						 deselCatId= (temp).substring(thirdpos+1,strlen);*/
						 deselId=st.nextToken();
						 deselName=st.nextToken();
						 deselUniqId=st.nextToken();
						 deselCatId=st.nextToken();
						 deselNciId=st.nextToken();
						 deselGradeCalc=st.nextToken();

						String selFields = StringUtil.encodeString(deselId+";"+deselName+";"+deselUniqId+";"+deselCatId+";"+deselNciId+";"+deselGradeCalc);
						%>
						<input type="hidden" name="deselectedAll" value="<%=selFields%>" >
						<%
						if ((counter%2)==0)
						{

						%>

					      	<tr class="browserEvenRow">

						<%} else
						 { %>

				      		<tr class="browserOddRow">

						<%}  %>
						<td><input type="checkbox" name="deselected" value="<%=selFields%>"> </td>
						<td><%=deselUniqId%></td>
						<td> <%=deselName%> </A></td>
						<input type="hidden" name="testId" value="<%=deselId%>">
					<%}%>

			</table>

			</div>
			<input type="hidden" name="numDsel" value="<%=cnt%>" >
			<input type="hidden" name="navigation" value="" >
			</td>	<!-- END OF THE TD FOR THE ADDED FEILDS -->
		</tr>
		</table>


				<jsp:include page="submitBar.jsp" flush="true"> 
						<jsp:param name="displayESign" value="N"/>
						<jsp:param name="formID" value="labSelFrm"/>
						<jsp:param name="showDiscard" value="Y"/>
				</jsp:include>


			</Form>



		<%





	}//end of if body for session

	else

	{

	%>

		<jsp:include page="timeout.html" flush="true"/>

	<%

	}

	%>



</div>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>




</body>

</html>

