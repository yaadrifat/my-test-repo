<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title>Protocol Team Details</title>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.common.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<SCRIPT Language="javascript">
 function  validate()
 {
     formobj=document.team;
     if (!(validate_col('E-Signature',formobj.eSign))) return false
     if(isNaN(formobj.eSign.value) == true) 
     {
	alert("Incorrect E-Signature. Please enter again");
	formobj.eSign.focus();
	return false;
      }

	}
</SCRIPT>

<% String src;
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true"></jsp:include>   
<body>
<jsp:useBean id="teamB" scope="page" class="com.velos.eres.web.team.TeamJB"/>
<jsp:useBean id ="sessionmaint" scope="page" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.team.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil"%>
<DIV class="browserDefault" id="div1"> 
    <%

   HttpSession tSession = request.getSession(true); 

   if (sessionmaint.isValidSession(tSession))

	{
	String uName = (String) tSession.getValue("userName");
	String study = (String) tSession.getValue("studyId");
	String acc = (String) tSession.getValue("accountId");
	String rights = request.getParameter("right");
	String tab = request.getParameter("selectedTab");
			 
  int pageRight = EJBUtil.stringToNum(rights);

%>
  <%-- IN THE ABOVE LINE OF CODE THE Id (MSITES) FOR SITES NEED TO REPLACED BY THAT OF TEAM--%>
  <%

 if (pageRight > 0 )

	{
		
	   int studyId = 0;   
	   studyId = EJBUtil.stringToNum(study);
	   
	   TeamDao teamDao = teamB.getTeamValues(studyId,EJBUtil.stringToNum(acc));
	   ArrayList teamIds 		= teamDao.getTeamIds(); 
	   ArrayList teamUserLastNames 	= teamDao.getTeamUserLastNames();
	   ArrayList teamUserFirstNames = teamDao.getTeamUserFirstNames();


	   String teamRole = null;
	   String teamUserLastName = null;
	   String teamUserFirstName = null;
	   int teamId = 0;

	   int len = teamIds.size();
	   int counter = 0;
 
%>
  <br>
<!--  <Form  name="search" method="post" action="assignToTeam.jsp" onsubmit="return f_check_perm(<%=pageRight%>,'N');">-->
	<!--<Form  name="search" method="post" action="assignToTeam.jsp">-->
	<Form  name="search" method="post" action="">
    <Input type="hidden" name="selectedTab" value="<%=tab%>">
    <Input type="hidden" name="right" value="<%=rights%>">
<br><br><br>
    <table class = tableDefault width="100%" cellspacing="0" cellpadding="0" border=0 >
      <tr> 
        <td class=tdDefault width=20%> User Search</td>
        <td class=tdDefault width=30%> User First Name: <Input type=text name="fname"></td>
        <td class=tdDefault width=30%> User Last Name: <Input type=text name="lname"></td>
        <td class=tdDefault width=10%> 
	<% if (pageRight == 5 || pageRight == 7)
	{
	%>
	  <button type="submit"><%=LC.L_Search%></button>
		  <%
		  	}
		  %>
        </td>
      </tr>
    </table>
    <Input type=hidden name="srcmenu" value="<%=src%>">
  </Form>
  
  
<Form name="team" method="post" action="">
    <Input type="hidden" name="selectedTab" value="<%=tab%>">
    <table width="100%" cellspacing="0" cellpadding="0" border=0 >
      <tr > 
        <td width = "100%"> 
          <P class = "defComments"> If you are unable to find a user in the existing 
            user list, you may <A href="userdetails.jsp?mode=N&srcmenu=tdMenuBarItem3&selectedTab=2">Add 
            New User </a> here <br>
            <br>
            </P>
        </td>
      </tr>
      <tr> 
        <td height="10"></td>
        <td height="10"></td>
      </tr>
    </table>
    <table width="100%" >
        <%
	String selName = null;
    for(counter = 0;counter<len;counter++)

	{	
		teamId=EJBUtil.stringToNum(((teamIds.get(counter)) == null)?"-":(teamIds.get(counter)).toString());
		teamUserLastName=((teamUserLastNames.get(counter)) == null)?"-":(teamUserLastNames.get(counter)).toString();
		teamUserFirstName=((teamUserFirstNames.get(counter)) == null)?"-":(teamUserFirstNames.get(counter)).toString();
		selName =  teamUserLastName + ", "  + teamUserFirstName; 	

		if ((counter%2)==0) 
		{
  		%>
      <tr class="browserEvenRow"> 
      <%
		}

		else
  	{
  		%>
      <tr class="browserOddRow"> 
      <%
		}

	}
%>
      <input type="hidden" name="src" Value="<%=src%>">
      <input type="hidden" name="totrows" Value="<%=len%>">
	  </table>
	  	<% if (pageRight == 6 || pageRight == 7)
				{
				%>
        
	  <table>
<tr>
	   <td>
		E-Signature <FONT class="Mandatory">* </FONT>
	   </td>
	   <td>
		<input type="password" name="eSign" maxlength="8" autocomplete="off">
	   </td>
</tr>
</table>
  <% }%>
	  <table>
      <tr> 
        <td align="right"> 
			<% if (pageRight == 6 || pageRight == 7)
				{
				%>
		  <input type="image" src="../images/jpg/Submit.gif" align="absmiddle" onClick = "return validate()" border="0">
		  <% }%>
        </td>
      </tr>
    </table>
  </Form>
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<DIV class="mainMenu" id = "emenu"> 
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>
</body>

</html>
