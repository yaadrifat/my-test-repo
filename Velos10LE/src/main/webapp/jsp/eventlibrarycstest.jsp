<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>
<html>
<head>
<title><%=LC.L_Evt_Lib%><%--Event Library*****--%></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<SCRIPT language="javascript">
function confirmBox(fileName,type,pgRight) {
	if (f_check_perm(pgRight,'E') == true) {

	if (type=="L") {
		var paramArray = [fileName];
		msg=getLocalizedMessageString("M_DelCatLib_EvtCat",paramArray);/*msg="Delete " + fileName + " Category from Library?. This will delete all the Events in this Category.";*****/
	}else if (type=="E") {
		var paramArray = [fileName];
		msg=getLocalizedMessageString("L_Del_FrmLib",paramArray);/*msg="Delete " + fileName + " from Library?";*****/
 	}
 
	if (confirm(msg)) 
	{
    	return true;
	}
	else
	{
		return false;
	}
	} else {
		return false;
	}
}
</SCRIPT> 

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="resolution.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT>
	function openwin() {
      window.open("","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")
;}
</SCRIPT>  
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<body id="bd" onLoad=load() leftmargin="0" marginwidth="0" topmargin="0" marginheight="0">

	<table class=tableDefault width="546" cellspacing="0" cellpadding="0" border="0">
		<tr> 
		<td id=topleftCurve class=tdDefault width="52"><img src="../images/jpg/top_left_curve.jpg" width="52" 	height="32"></td>
		
				
	    	<td valign=TOP><img id="hometop" src="../images/jpg/topline.jpg" width="658" height="5" align="top"></td> 
			
			
 		</tr>
	</table>
	<table class=leftPanelVLine width="22" cellspacing="0" cellpadding="0" border="0">
		<tr> 
	    <td width="21">&nbsp;</td>
	  <td width="1" >
 	   
			  <img id=homeright src="../images/jpg/vline.jpg" width="2" height="369"></td>
			  
				  
		
		</tr>
	</table>
<table class=tableDefault width="75%" cellpadding="0" cellspacing="0" border="0">
  	<tr> 
    	<td class=tDDefault > 
      		<p align="left"><img id=bottomcurve src="../images/jpg/test.jpg"></p>
    	</td>
  	</tr>
</table>
<form name="panel">
  <Input type="hidden" name="src" value="tdmenubaritem4">

</form>
<DIV CLASS=topRightPanel id=topRightDiv> 
  <form name="form1" action="studySearchAfterLogin.jsp" method="POST">
    <table width="245" cellpadding="0" cellspacing="0" border="0">
      <tr> 
        <td class=tdDefault width="49" ROWSPAN = 4><img src="../images/jpg/top_right_curve.jpg" width="49"></td>
        <td width="203" VALIGN = TOP height=20>&nbsp;</td>
      </tr>
      <tr> 
        <td class=tdDefault width=200 > <b><%=LC.L_Search%><%--Search*****--%></b> </td>
      </tr>
      <tr> 
        <td class=tdDefault height=44> 
          <input type="hidden" name="from" value="afterLogin">
          <input type="text" name="search" size = 10 MAXLENGTH = 20>
          
          <input type="hidden" name="searchOn" value="keyword">
          &nbsp;&nbsp;&nbsp;
          <!-- <input type="submit" value="Go" > -->
          <button type="submit" onClick = "form.submit()"><%=LC.L_Search%><%--<%=LC.L_Search%>*****--%></button>
        </td>
      </tr>
      <tr > 
        <td  width="200"> 
          <p><img src="../images/jpg/topline.jpg" width="245" height="7" align="absbottom"></p>
        </td>
      </tr>
    </table>
  </form>
</DIV>
<DIV class="logo" ID="vlogo"> 
  <table width=100% border="0">
    <tr> 
      <td width="200"><A href="http://www.velos.com"> <img src="../images/jpg/vlogo.jpg" width="189" height="78" border="0"></A></td>
	  <td>
	  
	  <SCRIPT>
	
	  </SCRIPT>
		
			<P class = "userNameNew" id="userNameNew"> <%=LC.L_Raghu_Chandra%><%--Raghuram Chandrasekar*****--%></p>
			<div id="test" class="plwt" >
			<P class = "plwait" id="plwait" align="abscenter"><%=MC.M_Wait_PageLoading%><%--Please wait, page loading*****--%>...... </p>
			</div>
		
	  </td>

    </tr>
  </table>
</DIV>
</body>
   
<body>








<BR>
<DIV class="browserDefault" id="div1"> 
	
			






<P class = "userName"> <%=LC.L_Raghu_Chandra%><%--Raghuram Chandrasekar*****--%> </P>
	
			<P class="sectionHeadings"> <%=LC.L_Lib_EvtLib%><%--Library >> Event Library*****--%></P>
	
	<table  class= "selectedTab" cellspacing="0" cellpadding="0" border="0"> 
	<tr>
		<td  valign="TOP">
		
	<table class="unselectedTab"  cellspacing="0" cellpadding="0"  border="0">
	<tr>
   	<td rowspan="3" valign="top" >
	<img src="../images/leftgreytab.gif" height=20 border=0 alt=""></td> 
	<td>
		<a href="protocollist.jsp?mode=N&srcmenu=tdmenubaritem4&selectedTab=1&calledFrom=L"><%=LC.L_Pcol_CalLib%><%--Protocol Calendar Library*****--%>
		</a>
	</td> 
        <td rowspan="3" valign="top" >
		<img src="../images/rightgreytab.gif"  height=20 border=0 alt="">
        </td>
   	</tr> 
   	</table> 
        </td> 
        <td  valign="TOP"> 
		
        <table class="selectedTab" border="0" cellspacing="0" cellpadding="0">
		<tr>
	   	<td rowspan="3" valign="top" >
			<img src="../images/leftgreytab.gif"  height=20 border=0 alt=""></td> 
		<td>
		<a href="eventlibrary.jsp?srcmenu=tdmenubaritem4&selectedTab=2&calledFrom=L&duration=&protocolId=&mode=M&calStatus=W"><%=LC.L_Evt_Lib%><%--Event Library*****--%>
		</a>
	</td> 
        <td rowspan="3" valign="top" >
		<img src="../images/rightgreytab.gif"  height=20 border=0 alt="">
        </td>
   	</tr> 
   	</table> 
	</td>
	</tr>
     <tr>
     <td colspan=5 height=10></td>
     </tr> 
     </table>

			<br>
		    <Form name="eventlibrary" action="eventlibrary.jsp" method="post">
			    
				<P class="defComments"><%=LC.L_Category%><%--Category*****--%>
					<select  name="catId">
						<option value=" " Selected><%=LC.L_All%><%--All*****--%> </option>
						
							<option value="15409"> <%=LC.L_Blood%><%--Blood*****--%> </option>
							
							<option value="15414"> <%=LC.L_Sugar%><%--Sugar*****--%> </option>
							
					</select>
					<button type="submit" border="0"><%=LC.L_Search%><%--<%=LC.L_Search%>*****--%></button>
				<P>
			  	<table width="100%" cellspacing="1" cellpadding="0" border="0" >
				    <tr > 
					    <td width = "70%"> 
				    	    <P class = "defComments"><%=MC.M_FlwEvtListed_InYourLib%><%--The following are the Events currently listed in your Library*****--%></P>
					    </td>
					    <td width = "30%" align="right"> 
					        <p> <A href="category.jsp?mode=M&catMode=N&srcmenu=tdmenubaritem4&duration=&protocolId=&calledFrom=L" onClick="return f_check_perm(7,'N')"><%=LC.L_Add_NewCat%><%--Add New Category*****--%></A> </p>
					    </td>
				    </tr>
				</table>
			    <input type="hidden" name="srcmenu" value="tdmenubaritem4">
			    <Input name="duration" type="hidden" value=>
			    <Input name="protocolId" type="hidden" value=>
		    	<Input name="calledFrom" type="hidden" value=L>
			    <Input name="mode" type="hidden" value=M>
			    <Input name="calStatus" type="hidden" value=W>
			    <Input name="selectedTab" type="hidden" value=2>
				<Input name="srcmenu" type="hidden" value=tdmenubaritem4>
			    <table width="100%" cellspacing="1" cellpadding="0" border="0">
				    <tr> 
				        <th width="15%"> <%=LC.L_Category%><%--Category*****--%> </th>
				        <th width="30%"> <%=LC.L_Event%><%--Event*****--%> </th>
				        <th width="45%"> <%=LC.L_Description%><%--Description*****--%> </th>
						<th width="10%"> &nbsp; </th>
					</tr>
					
					      	<tr class="browserEvenRow"> 
							
						    <td><A href = "category.jsp?mode=M&srcmenu=tdmenubaritem4&catMode=M&categoryId=15409&duration=&protocolId=&calledFrom=L" onClick="return f_check_perm(7,'E')"><%=LC.L_Blood%><%--Blood*****--%></A> </td>	
						    <td align=right><A href = "eventdetails.jsp?srcmenu=tdmenubaritem4&eventmode=N&categoryId=15409&duration=&protocolId=&calledFrom=L&mode=M&calStatus=W&fromPage=eventlibrary&selectedTab=1" onClick="return f_check_perm(7,'N')"><I><%=LC.L_Add_NewEvt%><%--Add New Event*****--%></I></A>&nbsp;&nbsp;&nbsp;&nbsp;</td>	
						
						<td><%=LC.L_Bld_RelatedTest%><%--Blood related test*****--%></td>
						<td><A href="eventdelete.jsp?eventId=15409&eventType=L&srcmenu=tdmenubaritem4&selectedTab=2&duration=2&protocolId=&calledFrom=L&mode=M&calStatus=W" onClick="return confirmBox('<%=LC.L_Blood%><%--Blood*****--%>','L',7);" ><%=LC.L_Delete%><%--Delete*****--%></A></td>
			    	 </tr>
					
				      		<tr class="browserOddRow"> 
						
							<td>&nbsp;</td>
							<td><A href = "eventdetails.jsp?srcmenu=tdmenubaritem4&eventmode=M&eventId=15413&duration=&protocolId=&calledFrom=L&mode=M&calStatus=W&fromPage=eventlibrary&selectedTab=1"><%=LC.L_Dlc%><%--DLC*****--%></A> </td>																																					
							<INPUT name=event_name15413 type=hidden value="DLC">
						
						<td><%=LC.L_Dlc%><%--DLC*****--%></td>
						<td><A href="eventdelete.jsp?eventId=15413&eventType=E&srcmenu=tdmenubaritem4&selectedTab=2&duration=2&protocolId=&calledFrom=L&mode=M&calStatus=W" onClick="return confirmBox('<%=LC.L_Dlc%><%--DLC*****--%>','E',7);" ><%=LC.L_Delete%><%--Delete*****--%></A></td>
			    	 </tr>
					
					      	<tr class="browserEvenRow"> 
						
							<td>&nbsp;</td>
							<td><A href = "eventdetails.jsp?srcmenu=tdmenubaritem4&eventmode=M&eventId=15411&duration=&protocolId=&calledFrom=L&mode=M&calStatus=W&fromPage=eventlibrary&selectedTab=1"><%=LC.L_Esr%><%--ESR*****--%></A> </td>																																					
							<INPUT name=event_name15411 type=hidden value="ESR">
						
						<td><%=LC.L_Esr%><%--ESR*****--%></td>
						<td> <A href="eventdelete.jsp?eventId=15411&eventType=E&srcmenu=tdmenubaritem4&selectedTab=2&duration=2&protocolId=&calledFrom=L&mode=M&calStatus=W" onClick="return confirmBox('<%=LC.L_Esr%><%--ESR*****--%>','E',7);" ><%=LC.L_Delete%><%--Delete*****--%></A></td>
			    	 </tr>
					
				      		<tr class="browserOddRow"> 
						
							<td>&nbsp;</td>
							<td><A href = "eventdetails.jsp?srcmenu=tdmenubaritem4&eventmode=M&eventId=15410&duration=&protocolId=&calledFrom=L&mode=M&calStatus=W&fromPage=eventlibrary&selectedTab=1"><%=LC.L_Hb_small%><%--Hb*****--%></A> </td>																																					
							<INPUT name=event_name15410 type=hidden value="Hb">
						
						<td><%=LC.L_Hemoglobin%><%--Hemoglobin*****--%></td>
						<td> <A href="eventdelete.jsp?eventId=15410&eventType=E&srcmenu=tdmenubaritem4&selectedTab=2&duration=2&protocolId=&calledFrom=L&mode=M&calStatus=W" onClick="return confirmBox('<%=LC.L_Hb_small%><%--Hb*****--%>','E',7);" ><%=LC.L_Delete%><%--Delete*****--%></A></td>
			    	 </tr>
					
					      	<tr class="browserEvenRow"> 
						
							<td>&nbsp;</td>
							<td><A href = "eventdetails.jsp?srcmenu=tdmenubaritem4&eventmode=M&eventId=15412&duration=&protocolId=&calledFrom=L&mode=M&calStatus=W&fromPage=eventlibrary&selectedTab=1"><%=LC.L_Tlc%><%--TLC*****--%></A> </td>																																					
							<INPUT name=event_name15412 type=hidden value="TLC">
						
						<td><%=LC.L_Tlc%><%--TLC*****--%></td>
						<td> <A href="eventdelete.jsp?eventId=15412&eventType=E&srcmenu=tdmenubaritem4&selectedTab=2&duration=2&protocolId=&calledFrom=L&mode=M&calStatus=W" onClick="return confirmBox('<%=LC.L_Tlc%><%--TLC*****--%>','E',7);" ><%=LC.L_Delete%><%--Delete*****--%></A></td>
			    	 </tr>
					
				      		<tr class="browserOddRow"> 
							
						    <td><A href = "category.jsp?mode=M&srcmenu=tdmenubaritem4&catMode=M&categoryId=15414&duration=&protocolId=&calledFrom=L" onClick="return f_check_perm(7,'E')"><%=LC.L_Sugar%><%--Sugar*****--%></A> </td>	
						    <td align=right><A href = "eventdetails.jsp?srcmenu=tdmenubaritem4&eventmode=N&categoryId=15414&duration=&protocolId=&calledFrom=L&mode=M&calStatus=W&fromPage=eventlibrary&selectedTab=1" onClick="return f_check_perm(7,'N')"><I><%=LC.L_Add_NewEvt%><%--Add New Event*****--%></I></A>&nbsp;&nbsp;&nbsp;&nbsp;</td>	
						
						<td><%=LC.L_Sugar_Test%><%--Sugar test*****--%></td>
						<td> <A href="eventdelete.jsp?eventId=15414&eventType=L&srcmenu=tdmenubaritem4&selectedTab=2&duration=2&protocolId=&calledFrom=L&mode=M&calStatus=W" onClick="return confirmBox('<%=LC.L_Sugar%><%--Sugar*****--%>','L',7);" ><%=LC.L_Delete%><%--Delete*****--%></A></td>
			    	 </tr>
					
					      	<tr class="browserEvenRow"> 
						
							<td>&nbsp;</td>
							<td><A href = "eventdetails.jsp?srcmenu=tdmenubaritem4&eventmode=M&eventId=15415&duration=&protocolId=&calledFrom=L&mode=M&calStatus=W&fromPage=eventlibrary&selectedTab=1"><%=LC.L_Blood%><%--Blood*****--%></A> </td>																																					
							<INPUT name=event_name15415 type=hidden value="Blood">
						
						<td><%=LC.L_Blood_Sugar%><%--Blood Sugar*****--%></td>
						<td><A href="eventdelete.jsp?eventId=15415&eventType=E&srcmenu=tdmenubaritem4&selectedTab=2&duration=2&protocolId=&calledFrom=L&mode=M&calStatus=W" onClick="return confirmBox('<%=LC.L_Blood%><%--Blood*****--%>','E',7);" ><%=LC.L_Delete%><%--Delete*****--%></A></td>
			    	 </tr>
					
				      		<tr class="browserOddRow"> 
						
							<td>&nbsp;</td>
							<td><A href = "eventdetails.jsp?srcmenu=tdmenubaritem4&eventmode=M&eventId=16592&duration=&protocolId=&calledFrom=L&mode=M&calStatus=W&fromPage=eventlibrary&selectedTab=1"><%=MC.M_HBTestCopy_FromCRF%><%--HB Test copy from CRF*****--%></A> </td>																																					
							<INPUT name=event_name16592 type=hidden value="HB Test copy from CRF">
						
						<td><%=LC.L_Hemoglobin%><%--Hemoglobin*****--%></td>
						<td><A href="eventdelete.jsp?eventId=16592&eventType=E&srcmenu=tdmenubaritem4&selectedTab=2&duration=2&protocolId=&calledFrom=L&mode=M&calStatus=W" onClick="return confirmBox('<%=MC.M_HBTestCopy_FromCRF%><%--HB Test copy from CRF*****--%>','E',7);" ><%=LC.L_Delete%><%--Delete*****--%></A></td>
			    	 </tr>
					
					      	<tr class="browserEvenRow"> 
						
							<td>&nbsp;</td>
							<td><A href = "eventdetails.jsp?srcmenu=tdmenubaritem4&eventmode=M&eventId=15416&duration=&protocolId=&calledFrom=L&mode=M&calStatus=W&fromPage=eventlibrary&selectedTab=1"><%=LC.L_Urine%><%--Urine*****--%></A> </td>																																					
							<INPUT name=event_name15416 type=hidden value="Urine">
						
						<td><%=LC.L_Urine_Test%><%--Urine Test*****--%></td>
						<td> <A href="eventdelete.jsp?eventId=15416&eventType=E&srcmenu=tdmenubaritem4&selectedTab=2&duration=2&protocolId=&calledFrom=L&mode=M&calStatus=W" onClick="return confirmBox('<%=LC.L_Urine%><%--Urine*****--%>','E',7);" ><%=LC.L_Delete%><%--Delete*****--%></A></td>
			    	 </tr>
					
				</table>
				<br>
				<br>
			</Form>
		
	<div> 
    	<SCRIPT>

	function openwin() {

     // window.open("","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")

;}

	function openwinflash() {

      window.open("","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=600,height=500")

;}

function waitfor() {
	//alert("Check1");
	if (document.all) {
		if (document.all("plwait")) {
			document.all("test").style.visibility="hidden";
			//alert("Check");
	//	document.all("userNameNew").style.visibility="visible";
			}
	}
	else
	{
		if (navigator.appVersion.substring(0,1) == '5') {
			if (document.getElementById("test")) {
				document.getElementById("plwait").style.visibility="hidden";
			}
		}
		else if (window.document.vlogo.document.test) {
			window.document.vlogo.document.test.visibility="hidden";
			
		}
		
	}
	}

</SCRIPT>  


<DIV CLASS="bottomPanel" id=botpanel> 

  <form>

  <table cellpadding="0" cellspacing="0" border=0 align=center>

    <tr> 

	<td> <A href="home.jsp"><%=LC.L_Home%><%--Home*****--%></A>&nbsp;|&nbsp;

	<A href="faq.html" ><%=LC.L_Faqs%><%--FAQs*****--%></A> &nbsp;|&nbsp;

	<A href="contactus.html"><%=LC.L_Contact_Us%><%--Contact us*****--%></A> &nbsp;|&nbsp;

	<A href="termsofservice.html"><%=LC.L_Terms_OfService%><%--Terms of Service*****--%></A> &nbsp;|&nbsp;

	<A href="privacypolicy.html"><%=LC.L_Privacy_Policy%><%--Privacy Policy*****--%></A>  &nbsp;|&nbsp;

	<A href="sitemap.html"><%=LC.L_Site_Map%><%--Site Map*****--%></A>
	
	<img src="../images/jpg/notDone.gif" onLoad=waitfor() width="0" height="0">  

	</td>

    </tr>

    <tr>

	<td height=3></td>

    </tr>

    <tr>

	<td class="tdDefault" align=center> <%=LC.L_C_VelosInc_2001%><%--© Velos, Inc. 2001*****--%> </td>

    </tr>

  </table>

  </form>



</DIV>

  	</div>
</div>
<div class ="mainMenu" id = "emenu"> 
  




<!--<div class ="mainMenu" id="emenu">--> 
<html>
	<link REL="stylesheet" TYPE="text/css" HREF="menusNS.css" />
	<script LANGUAGE="JavaScript" SRC="menusNS.js">
	</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<body onLoad="load()" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0">

<div Class="backgrnd" id="bakgrd">
 	<table width="100%">
		<tr>
			<td width="100%" height="400">&nbsp;
			</td>
		</tr>
	</table> 
</div>

<div Class="backgrnd1" id="bakgrd1">
 	<table width="100%">
		<tr>
			<td width="100%" height="100">&nbsp;
			</td>
		</tr>
	</table> 
</div>

<div Class="clsMenuBarItem" id="tdMenuBarItemPict1">
	<img SRC="../images/jpg/left_menu_pict.jpg" height=25 width=17><sup><a href="myHome.jsp?srcmenu=tdMenuBarItem1"><%=LC.L_My_Homepage%><%--My Homepage*****--%></a></sup>
</div>
<div Class="clsMenuBarItem" id="tdMenuBarItemPict2">
	<img SRC="../images/jpg/left_menu_pict.jpg" height=25 width=17> <sup><a href="#" onClick="return click('2')"><%=LC.L_Manage_Acc%><%--Manage Account*****--%></a></sup>
</div>
<div Class="clsMenu" id="submenuItem1">
	<img SRC="../images/jpg/submenu_pict.jpg" ><sup><a href="sitebrowser.jsp?srcmenu=tdMenuBarItem2"><%=LC.L_Organizations%><%--Organizations*****--%></a></sup>
</div>
<div Class="clsMenu" id="submenuItem2">
	<img SRC="../images/jpg/submenu_pict.jpg" ><sup><a href="groupbrowser.jsp?srcmenu=tdMenuBarItem2"><%=LC.L_Groups%><%--Groups*****--%></a></sup>
</div>

<div Class="clsMenu" id="submenuItem3">
	<img SRC="../images/jpg/submenu_pict.jpg" ><sup><a href="accountbrowser.jsp?srcmenu=tdMenuBarItem2"><%=LC.L_Users%><%--Users*****--%></a></sup>
</div>

<div Class="clsMenu" id="submenuItem4">
	<img SRC="../images/jpg/submenu_pict.jpg" ><sup><a href="ulinkBrowser.jsp?srcmenu=tdMenuBarItem2&selectedTab=2"><%=LC.L_Personalize_Acc%><%--Personalize Account*****--%></a></sup>
</div>

<div Class="clsMenuBarItem" id="tdMenuBarItemPict3">
	<img SRC="../images/jpg/left_menu_pict.jpg" height=25 width=17><sup><a href="#" onClick="return click('3')"><%=LC.L_Manage_Pcols%><%--Manage Protocols*****--%></a></sup>
</div>

<DIV  Class="clsMenu" id="submenuItem5">
	<img src = "../images/jpg/submenu_pict.jpg"><sup><A HREF="studywizard.jsp?srcmenu=tdMenuBarItem3"><%=LC.L_New%><%--New*****--%></A></sup>
</DIV>
<DIV  Class="clsMenu" id="submenuItem6">
	<img src = "../images/jpg/submenu_pict.jpg"><sup><A HREF="userStudies.jsp?srcmenu=tdMenuBarItem3"><%=LC.L_Open%><%--Open*****--%></A></sup>
</DIV>

<div Class="clsMenuBarItem" id="tdMenuBarItemPict4">
		<img src = "../images/jpg/left_menu_pict.jpg" width="17" height="25"><sup><a href="#" onClick="return click('4')"><%=LC.L_Library%><%--Library*****--%></a></sup>
</div>

<DIV  Class="clsMenu" id="submenuItem7">
	<img src = "../images/jpg/submenu_pict.jpg"><sup><A HREF="protocollist.jsp?mode=N&srcmenu=tdmenubaritem4&selectedTab=1&calledFrom=L"><%=LC.L_Protocol_Calendar%><%--Protocol Calendar*****--%></A></sup>
</DIV>
<DIV  Class="clsMenu" id="submenuItem8">
	<img src = "../images/jpg/submenu_pict.jpg"><sup><A HREF="eventlibrary.jsp?srcmenu=tdmenubaritem4&selectedTab=2&calledFrom=L&duration=&protocolId=&mode=M&calStatus=W"><%=LC.L_Evt_Lib%><%--Event Library*****--%></A></sup>
</DIV>
 

<div Class="clsMenuBarItem" id="tdMenuBarItemPict5">
	<img SRC="../images/jpg/left_menu_pict.jpg" height=25 width=17><sup><a href="#" onClick="return click('5')"><%=LC.L_Mng_Pats%><%--Manage Patients*****--%>%></a></sup>
</div>
<div Class="clsMenu" id="submenuItem9">
	<img SRC="../images/jpg/submenu_pict.jpg" ><sup><A href="patientdetails.jsp?srcmenu=tdmenubaritem5&mode=N&selectedTab=1&pkey=&page=patient"><%=LC.L_New%><%--New*****--%></A></sup>
</div>
<div Class="clsMenu" id="submenuItem10">
	<img SRC="../images/jpg/submenu_pict.jpg" ><sup><A href="studypatients.jsp?srcmenu=tdmenubaritem5&studyId=&patid=&patstatus=&openMode=F"><%=LC.L_Open%><%--Open*****--%></A></sup>
</div>

<div Class="clsMenuBarItem" id="tdMenuBarItemPict6">
	<img SRC="../images/jpg/left_menu_pict.jpg" height=25 width=17><sup><a href="accountreports.jsp?selectedTab=1&srcmenu=tdmenubaritem4"><%=LC.L_Reports%><%--Reports*****--%></a></sup>
</div>

<div Class="clsMenuBarItem" id="tdMenuBarItemPict7">
	<img SRC="../images/jpg/left_menu_pict.jpg" height=25 width=17><sup><a href="#" onClick="return click('6')"><%=LC.L_Help%><%--Help*****--%></a></sup>
</div>

<div Class="clsMenu" id="submenuItem11">
	<img SRC="../images/jpg/submenu_pict.jpg" ><sup><A href="ereshelp.jsp" target="Information" onclick="openwin()"><%=LC.L_Online_Help%><%--Online Help*****--%></A></sup>
</div>
<div Class="clsMenu" id="submenuItem12">
	<img SRC="../images/jpg/submenu_pict.jpg" ><sup><A href="contactus.html" target="Information" onclick="openwin()"><%=LC.L_Contact_Us%><%--Contact Us*****--%></A></sup>
</div>

<div Class="clsMenu" id="submenuItem13">
	<img SRC="../images/jpg/submenu_pict.jpg" ><sup><A href="termsofservice.html" target="Information" onclick="openwin()"><%=LC.L_Terms_OfService%><%--Terms of Service*****--%></A></sup>
</div>

<div Class="clsMenu" id="submenuItem14">
	<img SRC="../images/jpg/submenu_pict.jpg" ><sup><A href="privacypolicy.html" target="Information" onclick="openwin()"><%=LC.L_Privacy_Policy%><%--Privacy Policy*****--%></A></sup>
</div>

<div Class="clsMenuBarItem" id="tdMenuBarItemPict8">
	<img SRC="../images/jpg/left_menu_pict.jpg" height=25 width=17><sup><a href="logout.jsp"><%=LC.L_Logout%><%--Logout*****--%></a></sup>
</div>

</html>

<!--</div>-->



</div>
</body>
</html>
