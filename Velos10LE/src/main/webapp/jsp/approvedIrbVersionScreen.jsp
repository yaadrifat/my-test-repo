<%@page import="com.velos.eres.web.study.StudyJB"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC,com.velos.eres.service.util.DateUtil"%> 
<%@page import="org.json.JSONObject,org.json.JSONArray" %>
<%@page import="com.velos.eres.widget.service.util.FlxPageCache, com.velos.eres.service.util.StringUtil"%>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@page import="com.velos.eres.web.studyRights.StudyRightsJB"%>
<%@page import="com.velos.eres.business.common.*,com.velos.eres.service.util.*"%>
<%@page import="com.velos.eres.widget.business.common.UIFlxPageDao"%>
<%@page import="com.velos.eres.compliance.web.ComplianceJB" %>
<%@page import="java.util.*,java.io.*"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="grpRights" scope="session" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="stdRights" scope="session" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="studyJB" scope="session" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="complianceJB" scope="request" class="com.velos.eres.compliance.web.ComplianceJB"/>
<jsp:include page="ui-include.jsp" flush="true"/>
<%

HttpSession tSession = request.getSession(false); 
if (!sessionmaint.isValidSession(tSession)) {
	return;
}
String userId = (String)tSession.getAttribute("userId");
String studyId = (String)request.getParameter("studyId");
String mode = "N"; 
String createType = (String)request.getParameter("createType");
if (!"A".equals(createType)) { createType = "D"; }

if(studyId == null || studyId.equals("null")){
	studyId = "0";
	studyJB = null;
} else {
	if (StringUtil.stringToNum(studyId) < 1){
		studyId="0";
		studyJB = null;
	} else {
		studyJB = new StudyJB();
		studyJB.setId(StringUtil.stringToNum(studyId));
		studyJB.getStudyDetails();
		mode = "M";
		createType = studyJB.getStudyCreationType();
		HashMap sysDataMap = studyJB.getStudySysData(request);
		if (null == sysDataMap)
			return;
		String autoGenStudy = (String) sysDataMap.get("autoGenStudy");
		tSession.setAttribute("autoGenStudyForJS",autoGenStudy);
		tSession.setAttribute("studyNo",sysDataMap.get("studyNo"));
		
		String roleCodePk = (String) sysDataMap.get("studyRoleCodePk");
		tSession.setAttribute("studyRoleCodePk",roleCodePk);

		stdRights = (StudyRightsJB) sysDataMap.get("studyRights");
		tSession.setAttribute("studyRights",stdRights);

		int stdRight = StringUtil.stringToNum((sysDataMap.get("stdSummRight").toString()));
		tSession.setAttribute("studyRightForJS",stdRight);
	}
	tSession.setAttribute("studyId",studyId);
	tSession.setAttribute("studyJB",studyJB);
}

int accountId = StringUtil.stringToNum((String)tSession.getAttribute("accountId"));
int pageRight = 0;

if (StringUtil.stringToNum(studyId) == 0){
	grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));
} else {
	//******************GET STUDY TEAM RIGHTS and put the same in session, this will be a separate obj as no study is selected //***************************************************************
    TeamDao teamDao = new TeamDao();
    teamDao.getTeamRights(StringUtil.stringToNum(studyId),StringUtil.stringToNum(userId));
    ArrayList tId = teamDao.getTeamIds();
    if (tId.size() == 0)
	{
    	pageRight=0 ;
    }
	else
	{
    	stdRights.setId(StringUtil.stringToNum(tId.get(0).toString()));
   	 	ArrayList teamRights ;
		teamRights  = new ArrayList();
		teamRights = teamDao.getTeamRights();

		stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
		stdRights.loadStudyRights();

    	tSession.setAttribute("studyRights",stdRights);
    	if ((stdRights.getFtrRights().size()) == 0)
		{
    	 	pageRight= 0;
    	}
		else
		{
    		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYSUM"));
    	}
    }
}

FlxPageCache.reloadStudyPage();

String jPageAvailSections = FlxPageCache.getStudyPageSections();
%>
<link rel="stylesheet" type="text/css" href="styles/flexPage.css">

<script>
var pageSectionRegistry = <%=jPageAvailSections.toString()%>;
var activeSection = 0;

var showNotification = function(){
	
};

var sectionHeaderClick = function(sectionNo){
	activeSection = sectionNo;
	if (sectionNo < 1) return;
	if (!document.getElementById('sectionLoading'+ sectionNo)){
		return;
	}

	var vUrl = 'flexStudyPageBySection.jsp?sectionNo='+sectionNo +'&studyId=<%=StringUtil.stringToNum(studyId)%>';
	$j('#sectionBody'+ sectionNo).load(vUrl, function(){});	
	return;
};

var loadSections = function(){
	var secCount = 0;
	for(var indx=0; indx < pageSectionRegistry.length; indx++){
		var sectionJSObject = pageSectionRegistry[indx];
		pageSections[indx] = sectionJSObject;
		secCount++;
	}
	return pageSections;
};
  
var createSectionAccordion =  function(sectionNo){
	$j('#sectionAccordion'+sectionNo)
    .accordion('destroy')
    .accordion({
		header: "> div > h3",
		autoHeight: false, collapsible: true, active: false, fillSpace: false, animated: false
	});
};

var getPageSectionUrl = function(selSection) {
	var vUrl = 'flexStudyPageBySection.jsp?sectionNo='+selSection +'&studyId=<%=StringUtil.stringToNum(studyId)%>';
	
	return vUrl;
}

var renderSections = function(){
	$j('#sectionAccordions').html('');
	var additionalSectionHTML = '';
	for (var indx=0; indx < pageSectionRegistry.length; indx++){
		var sectionNo = indx+1;
		var jsSectionObject = pageSectionRegistry[indx];

		var secFldCount = 0;
		var secType = jsSectionObject['secType'];
		secType = (!secType)? "generic" : secType;

		if (secType == "generic"){
			var sectionFldArray = jsSectionObject['fields'];
			sectionFldArray = (!sectionFldArray)? [] : sectionFldArray;
			secFldCount = sectionFldArray.length;
		} else {
			secFldCount = "";
		}
		
		additionalSectionHTML += '<div id="sectionAccordion'+ sectionNo +'" onClick="sectionHeaderClick('+sectionNo+');">'
		+'<div id="sectionHead'+ sectionNo +'" class="group">'
		+'<h3 id="sectionHeader'+ sectionNo +'">'
		+'<table width="100%">'
		+'<tr>'
		+'<td width="70%">'
		+'<input type="hidden" id="sectionSeq" name="sectionSeq" value="'+ sectionNo +'"/>'
		+'<a id="sectionAnchor'+ sectionNo +'" href="#" >'+jsSectionObject.title+'&nbsp;&nbsp;'
		+'<span id="sectionTitleSpan'+ sectionNo +'" style="display:none"><input type="text" id="sectionTitle" name="sectionTitle" value="'+jsSectionObject.title+'"/></span>&nbsp;'
		+'<input type="hidden" id="secFldWDataCount'+ sectionNo +'" value=""/>'
		+'<input type="hidden" id="secFldCount'+ sectionNo +'" value="'+secFldCount+'"/>'
		+'</a>'
		+'</td>'
		+'<td id="secFldWDataCountTD'+sectionNo+'" align="right" width="10%"></td>'
		+'<td align="right" width="10%">'
		+'<div id="progressBar'+ sectionNo +'"></div>'
		+'</td>'
		+'<td id="secFldCountTD'+sectionNo+'" align="left" width="5%">'+secFldCount+'</td>'
		+'</tr>'
		+'</table>'
		+'</h3>'
		+'<div id="sectionBody'+ sectionNo +'">'
		+'<br/><p id="sectionLoading'+ sectionNo +'">Loading...<img style="width:200px;" src="../images/jpg/loading_pg.gif"/></p>'
		+'</div>'
		+'</div>'
		+'</div>';

		$j('#sectionAccordions').append(additionalSectionHTML);
		additionalSectionHTML = '';

		createSectionAccordion(sectionNo);
	}
	openFirstSection(1);
	
	//$j('#sectionAnchor'+activeSection).trigger("click");
    //sectionHeaderClick(activeSection);
};

var openFirstSection = function() {
	sectionHeaderClick(1);
	$j('#sectionAccordion1').accordion({active:0});
}

var replicateESign = function(id) {
	$('esign').value = $(id).value;
	ajaxvalidate('misc:esign', 4, id+'_eSign',
            '<%=LC.L_Valid_Esign%>', '<%=LC.L_Invalid_Esign%>', 'sessUserId');
};

</script>
<%--Include JSP --%>
<jsp:include page="studyScreenInclude.jsp" flush="true"/>
<jsp:include page="studySubmissionScreenInclude.jsp" flush="true"/>
<%----%>
<%
boolean isFrozen = complianceJB.getIsLockedStudy();
UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
if((studyId != null ) && !("".equals(studyId))) {
 	out.println(uiFlxPageDao.getFlexPageHtmlOfHighestApprovedVersion("er_study", StringUtil.stringToNum(studyId)));
}
%>
<script>
<% if ("M".equals(mode)) { %>
$j(".BrowserBotN_RR_1").removeClass("BrowserBotN_RR_1").addClass("BrowserBotN_S_3");
<% } else { %>
$j(".BrowserBotN_RR_1").removeClass("BrowserBotN_RR_1").addClass("BrowserBotN_RC_1");
<% }%>

jQuery(function() {
	renderSections();
	$j('#toggleExpandCollapseAll').click(toggleExpandCollapseAllClick);
});

$j(document).ready(function(){
	var screenWidth = screen.width;
	var screenHeight = screen.height;

	if(screenHeight<=768){
		$j("div.flxPageContainer").css({height:"550px"});
	} else {
		$j("div.flxPageContainer").css({height:"770px"});
	}
});
</script>