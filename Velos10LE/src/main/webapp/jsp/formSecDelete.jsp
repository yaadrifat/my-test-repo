<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Form_Lib%><%--Form Library*****--%></title>
<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*,com.aithent.audittrail.reports.AuditUtils"%>
<jsp:useBean id="formSecJB" scope="request" class="com.velos.eres.web.formSec.FormSecJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 

<% String src;
	src= request.getParameter("srcmenu");
	
%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  

<!-- "%>" has been commented in Chennai Aithent.-->
<!-- %> -->


<BODY> 
<br>

<DIV class="formDefault" id="div1">
<% 
	

	String formSecId= "";
	String formId="";
	String secId= "";
   	String fieldId= "";
	String mode= "";
	String calledFrom = "";
	String selectedTab="";
	String modBy="";
	String codeStatus="" ; 
	String lnkFrom="";
	String studyId= "";
	String flag = "";
	int ret=0;
	boolean hasInterFieldActions = false;
	int refreshSec = 1;
	
HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))	{ 	
		formSecId= request.getParameter("formSecId");
		formId=request.getParameter("formId");
		mode= request.getParameter("mode");
		calledFrom = request.getParameter("calledFrom");
		selectedTab= request.getParameter("selectedTab");
		modBy=(String) tSession.getAttribute("userId");
	
		codeStatus = request.getParameter("codeStatus");
		lnkFrom = request.getParameter("lnkFrom");
		studyId = request.getParameter("studyId");

		// this flag determines if the section contains the default date field or not 
		//  Y : indicates contains default field   N : indicates that the section does not contain the default field  

		flag = request.getParameter("flag");
		
		
		
		if ( flag.equals("Y")  )
		{%>
		<table align="center">
		<tr><td align=center>
   		<P class="successfulmsg"><%=MC.M_SecCntDel_MndtFld%><%--This section can't be deleted as it has the mandatory field 'Data entry Date', delete the fields individually if neccessary*****--%> </P>
		</td>
		</tr>
		<tr height=20>
		<td align=center>
		<button onclick="window.history.back();"><%=LC.L_Back%></button>
		</td>
		</tr>
		</table>
		
<%	}else{%>
	<FORM name="deleteSec" id="delSecFrmId" method="post" action="formSecDelete.jsp" onSubmit="if (validate(document.deleteSec)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>
	<%  if ( mode.equals("initial") && !codeStatus.equals("WIP") )
			{      
	%>			
	<P class="defComments"><%=MC.M_EsignToProc_WithSecDel%><%--Please enter e-Signature to proceed with Section Delete.*****--%></P>
				<jsp:include page="submitBar.jsp" flush="true"> 
						<jsp:param name="displayESign" value="Y"/>
						<jsp:param name="formID" value="delSecFrmId"/>
						<jsp:param name="showDiscard" value="N"/>
				</jsp:include>

 		 <input type="hidden" name="formSecId" value="<%=formSecId%>">
		 <input type="hidden" name="calledFrom" value="<%=calledFrom%>">
		  <input type="hidden" name="codeStatus" value="<%=codeStatus%>">
		   <input type="hidden" name="lnkFrom" value="<%=lnkFrom%>">
		    <input type="hidden" name="studyId" value="<%=studyId%>">
		 
		 
		 <input type="hidden" name="formId" value="<%=formId%>">
		 <Input type="hidden" name="mode" value="visited" >
		 <Input type="hidden" name="srcmenu" value="<%=src%>" >
		  <Input type="hidden" name="selectedTab" value="<%=selectedTab%>" >
		   <input type="hidden" name="flag" value="<%=flag%>">
  	 
  	
	 
	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			
			if((codeStatus.equals("WIP")) || ((!codeStatus.equals("WIP") && !codeStatus.equals("")) && (oldESign.equals(eSign)))){
					
					formSecId=  request.getParameter("formSecId");
						
					formId=  request.getParameter("formId");	
					calledFrom = request.getParameter("calledFrom");
					selectedTab = request.getParameter("selectedTab");
					flag = request.getParameter("flag");
					
					if ( flag.equals("N") )
					{
						hasInterFieldActions = formSecJB.sectionHasInterFieldActions(formSecId);
						
						if (!hasInterFieldActions)
						{
							formSecJB.setFormSecId(Integer.parseInt(formSecId));
							formSecJB.getFormSecDetails();
							formSecJB.setModifiedBy(modBy);
							formSecJB.setRecordType("D");
							
							//Modified for INF-18183 ::: Ankit
							String moduleName = ("L".equals(calledFrom))? LC.L_Form_Lib : LC.L_Form_Mgmt;
							ret = formSecJB.updateFormSec(AuditUtils.createArgs(session,"",moduleName));
						}
						else
						{
							ret = -4;
						}	
					}
					/*else  if ( flag.equals("Y")  )
					{
						int idsec = 0 ;
						int idform = 0 ;
						
						String ipAdd = "" ;  
						ipAdd=(String)tSession.getAttribute("ipAdd");
						idsec =  Integer.parseInt(formSecId);
						idform = Integer.parseInt(formId);
						ret = formSecJB.updateDefaultFormSec(idsec,idform,modBy,ipAdd) ;		
						
						
					}*/
					if (ret==-2 || ret == -4) 
					{%>	

						<br><br><br><br><br><br>
	
						<table width=100%>
						<tr>
						<td align=center><p class = "successfulmsg"><%=MC.M_Data_CntDel%><%--Data cannot be deleted.*****--%></p>
						
						<% if (ret == -4 ) { 
							refreshSec = 4;
						%>
							<p class = "successfulmsg" align = "center"><%=MC.M_FldLnkInter_RemFldTry%><%--This section has field(s) linked with one or more Inter Field Actions. Please remove the field(s) from the Inter Field Action(s) and then try again.*****--%></p>
						
					<%
						} //ret == -4
					} else 
					{ %>
						<P class="successfulmsg" align = "center"><%=MC.M_Data_DelSucc%><%--Data deleted successfully.*****--%> </P>
					<%}%>
					
					<META HTTP-EQUIV=Refresh CONTENT="<%=refreshSec%>; URL=formfldbrowser.jsp?studyId=<%=studyId%>&lnkFrom=<%=lnkFrom%>&codeStatus=<%=codeStatus%>&calledFrom=<%=calledFrom%>&srcmenu=<%=src%>&mode=M&formId=<%=formId%>&selectedTab=<%=selectedTab%>">					
					
			 			</td>
						</tr>
						<tr height=20></tr>
						<tr>
						<td align=center>
						</td>		
						</tr>		
						</table>				
						<br><br><br><br><br> <p class = "successfulmsg" align = center>  </p>
				<% 
				}else{ %>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>					 
		
	<%	}
		
	}	 				
  
	}//end esign
	}
		
	else
	{ %> 
	 	<jsp:include page="timeout.html" flush="true"/> 
 <% } %>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</DIV>
<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/> 
</div>
</body>
</HTML>


