<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC" %>
<%@page import="com.velos.eres.service.util.VelosResourceBundle" %>
<script>
$j(document).ready(function() {
	respPartyAction();
	sponsRespSectAction();
	sponsContactTypeAction()
	$j('#ctrpDraftNonIndustrialForm_ctrpDraftJB_draftXmlRequired1').change(function() { sponsRespSectAction(); });
	$j('#ctrpDraftNonIndustrialForm_ctrpDraftJB_draftXmlRequired0').change(function() { sponsRespSectAction(); });
	$j('#ctrpDraftSectSponsRespJB\\.responsibleParty1').change(function() { respPartyAction(); });
	$j('#ctrpDraftSectSponsRespJB\\.responsibleParty0').change(function() { respPartyAction(); });
	$j('#ctrpDraftSectSponsRespJB\\.sponsorContactType1').change(function() { sponsContactTypeAction(); });
	$j('#ctrpDraftSectSponsRespJB\\.sponsorContactType0').change(function() { sponsContactTypeAction(); });
});
function respPartyAction() {
	if ($j('#ctrpDraftSectSponsRespJB\\.responsibleParty1').is(":checked")) {
		$j('#sponsor_contact_type_row').hide();
		$j('#sponsor_generic_contact').hide();
		$j('#sponsor_personal_contact').hide();
	} else if ($j('#ctrpDraftSectSponsRespJB\\.responsibleParty0').is(":checked")) {
		$j('#sponsor_contact_type_row').show();
		sponsContactTypeAction();
	}
}
function sponsRespSectAction() {
	if($j('#ctrpDraftNonIndustrialForm_ctrpDraftJB_draftXmlRequired1').is(':checked')) {
		$j(':input', '#ctrp_draft5').removeAttr('disabled');
		$j('#ctrp_draft5_section').show();
	} else if ($j('#ctrpDraftNonIndustrialForm_ctrpDraftJB_draftXmlRequired0').is(':checked')) {
				$j(':input', '#ctrp_draft5').attr('disabled', 'disabled');
				$j('#ctrp_draft5_section').hide();
	}
	if((!$j('#ctrpDraftNonIndustrialForm_ctrpDraftJB_draftXmlRequired1').is(':checked')) 
			&& (!$j('#ctrpDraftNonIndustrialForm_ctrpDraftJB_draftXmlRequired0').is(':checked'))) {
				$j(':input', '#ctrp_draft5').attr('disabled', 'disabled');
				$j('#ctrp_draft5_section').hide();
	}
}
function sponsContactTypeAction() {
	if ($j('#ctrpDraftSectSponsRespJB\\.sponsorContactType1').is(":checked")) {
		$j('#sponsor_generic_contact').hide();
		$j('#sponsor_personal_contact').show();
	} else if ($j('#ctrpDraftSectSponsRespJB\\.sponsorContactType0').is(":checked")) {
		$j('#sponsor_personal_contact').hide();
		$j('#sponsor_generic_contact').show();
	}
}
</script>
<table>
	<tr>
		<td width="50%" valign="top">
			<table>
				<tr>
					<td align="right" width="30%"><b><%=LC.L_Sponsor_Id%></b></td>
					<td width="1%">&nbsp;&nbsp;</td>
					<td align="left">
						<s:textfield name="ctrpDraftSectSponsRespJB.sponsorId" 
							id="ctrpDraftSectSponsRespJB.sponsorId" size="50" maxlength="50" />
					</td>
				</tr>
				<tr>
					<td>
						<span id="ctrpDraftSectSponsRespJB.sponsorId_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td valign="top">
						<FONT id="sponsor_id_asterisk" class="Mandatory">* </FONT>
					</td>
					<td align="left">
						<p class="defComments"><FONT id="sponsor_id_asterisk">
						<%=MC.CTRP_SponsRespEnterOrSelect%>
						</FONT></p>
					</td>
				</tr>
				<tr>
					<td align="right">
						<a href="javascript:openSelectOrg(constSponsor);" ><%=LC.L_Select %></a>
						<s:hidden name="ctrpDraftSectSponsRespJB.sponsorFkSite" id="ctrpDraftSectSponsRespJB.sponsorFkSite" />
						<s:hidden name="ctrpDraftSectSponsRespJB.sponsorFkAdd" id="ctrpDraftSectSponsRespJB.sponsorFkAdd" />
					</td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_Name%></td>
					<td>&nbsp;&nbsp;</td>
					<td align="left">
						<s:textfield name='ctrpDraftSectSponsRespJB.sponsorName' id='ctrpDraftSectSponsRespJB.sponsorName' 
							type="text" size="50" cssClass="readonly-input" readonly="true" />
					</td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_Street_Address%>
						<span id="ctrpDraftSectSponsRespJB.sponsorStreet_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td>&nbsp;&nbsp;</td>
					<td align="left">
						<s:textfield name='ctrpDraftSectSponsRespJB.sponsorStreet' id='ctrpDraftSectSponsRespJB.sponsorStreet' 
							type="text" size="50" cssClass="readonly-input" readonly="true" />
					</td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_City%>
						<span id="ctrpDraftSectSponsRespJB.sponsorCity_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td>&nbsp;&nbsp;</td>
					<td align="left">
						<s:textfield name='ctrpDraftSectSponsRespJB.sponsorCity' id='ctrpDraftSectSponsRespJB.sponsorCity'
							type="text" size="50" cssClass="readonly-input" readonly="true" />
					</td>
				</tr>
				<tr>
					<td align="right"><%=LC.CTRP_DraftStateProvince%>
						<span id="ctrpDraftSectSponsRespJB.sponsorState_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td>&nbsp;&nbsp;</td>
					<td align="left">
						<s:textfield name='ctrpDraftSectSponsRespJB.sponsorState' id='ctrpDraftSectSponsRespJB.sponsorState' 
							type="text" size="50" cssClass="readonly-input" readonly="true" />
					</td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_Zip_Code%>
						<span id="ctrpDraftSectSponsRespJB.sponsorZip_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td>&nbsp;&nbsp;</td>
					<td align="left">
						<s:textfield name='ctrpDraftSectSponsRespJB.sponsorZip' id='ctrpDraftSectSponsRespJB.sponsorZip' 
							type="text" size="50" cssClass="readonly-input" readonly="true" />
					</td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_Country%>
						<span id="ctrpDraftSectSponsRespJB.sponsorCountry_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td>&nbsp;&nbsp;</td>
					<td align="left">
						<s:textfield name='ctrpDraftSectSponsRespJB.sponsorCountry' id='ctrpDraftSectSponsRespJB.sponsorCountry' 
							type="text" size="50" cssClass="readonly-input" readonly="true" />
					</td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_Email_Addr%><%--Modified for Bug 8360 :Yogendra Pratap Singh --%>
						<span id="ctrpDraftSectSponsRespJB.sponsorEmail_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td>&nbsp;&nbsp;</td>
					<td align="left">
						<s:textfield name='ctrpDraftSectSponsRespJB.sponsorEmail' id='ctrpDraftSectSponsRespJB.sponsorEmail' 
							type="text" size="50" cssClass="readonly-input" readonly="true" />
					</td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_Phone%></td>
					<td>&nbsp;&nbsp;</td>
					<td align="left">
						<s:textfield name='ctrpDraftSectSponsRespJB.sponsorPhone' id='ctrpDraftSectSponsRespJB.sponsorPhone' 
							type="text" size="50" cssClass="readonly-input" readonly="true" />
					</td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_TTY%></td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftSectSponsRespJB.sponsorTty' id='ctrpDraftSectSponsRespJB.sponsorTty' 
							type="text" size="50" maxlength="100" />
					</td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_Fax%></td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftSectSponsRespJB.sponsorFax' id='ctrpDraftSectSponsRespJB.sponsorFax' 
							type="text" size="50" maxlength="100" />
					</td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_Url_Upper%></td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftSectSponsRespJB.sponsorUrl' id='ctrpDraftSectSponsRespJB.sponsorUrl' 
							type="text" size="50" maxlength="100" />
					</td>
				</tr>
				
			</table>
		</td>
		<td width="50%" valign="top">
			<table>
				<tr>
					<td align="right" width="30%">
						<b><%=LC.CTRP_DraftResponsibleParty%></b>
						<span id="ctrpDraftSectSponsRespJB.responsibleParty_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td valign="top" width="1%">
						<FONT id="responsible_party_asterisk" class="Mandatory">* </FONT>
					</td>
					<td align="left">
						<s:radio name="ctrpDraftSectSponsRespJB.responsibleParty"
								id="ctrpDraftSectSponsRespJB.responsibleParty"
								list="ctrpDraftSectSponsRespJB.responsiblePartyList" 
							    listKey="radioCode" listValue="radioDisplay" value="ctrpDraftSectSponsRespJB.responsibleParty" />
					</td>
				</tr>
				<tr id="sponsor_contact_type_row" style="display:none">
					<td colspan="3">
						<table>
							<tr>
								<td align="right" width="30%"><%=LC.CTRP_DraftSponsorContactFromStudy%></td>
								<td width="1%">
									&nbsp;&nbsp;
								</td>
								<td align="left">
									<s:textfield name="ctrpDraftSectSponsRespJB.sponsorContactFromStudy"
											id="ctrpDraftSectSponsRespJB.sponsorContactFromStudy" 
											cssClass="readonly-input" readonly="true" size="50" />
								</td>
							</tr>
							<tr>
								<td align="right" width="30%"><b><%=LC.CTRP_DraftSponsorContactType%></b></td>
								<td width="1%">
									&nbsp;&nbsp;
								</td>
								<td align="left">
									<s:radio name="ctrpDraftSectSponsRespJB.sponsorContactType"
											id="ctrpDraftSectSponsRespJB.sponsorContactType"
											list="ctrpDraftSectSponsRespJB.sponsorContactTypeList" 
										    listKey="radioCode" listValue="radioDisplay" value="ctrpDraftSectSponsRespJB.sponsorContactType" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr id="sponsor_personal_contact" style="display:none">
					<td colspan="3">
						<table>
							<tr>
								<td align="right" width="30%"><b><%=LC.CTRP_DraftPersonalContactId%></b></td>
								<td width="1%">&nbsp;&nbsp;</td>
								<td align="left">
									<s:textfield name="ctrpDraftSectSponsRespJB.personalContactId" 
										id="ctrpDraftSectSponsRespJB.personalContactId" size="50" maxlength="50" />
								</td>
							</tr>
							<tr>
								<td>
									<span id="ctrpDraftSectSponsRespJB.personalContactId_error" class="errorMessage" style="display:none;"></span>
								</td>
								<td valign="top">
								<FONT id="personal_contact_id_asterisk" class="Mandatory">* </FONT>
								</td>
								<td>
									<p class="defComments"><FONT id="personal_contact_id_mandatory">
									<%=MC.CTRP_PersonalContactEnterOrSelect%>
									</FONT></p>
								</td>
							</tr>
							<tr>
								<td align="right">
									<a href="javascript:openSelectUser(constPersonalContact);" ><%=LC.L_Select %></a>
									<s:hidden name="ctrpDraftSectSponsRespJB.personalContactFkUser" 
										id="ctrpDraftSectSponsRespJB.personalContactFkUser" />
									<s:hidden name="ctrpDraftSectSponsRespJB.personalContactFkAdd" 
										id="ctrpDraftSectSponsRespJB.personalContactFkAdd" />
								</td>
							</tr>
							<tr>
								<td align="right"><%=LC.L_First_Name%></td>
								<td>&nbsp;&nbsp;</td>
								<td align="left"><s:textfield name='ctrpDraftSectSponsRespJB.personalContactFirstName' 
									id='ctrpDraftSectSponsRespJB.personalContactFirstName' 
									type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
							</tr>
							<tr>
								<td align="right"><%=LC.L_Middle_Name%></td>
								<td>&nbsp;&nbsp;</td>
								<td align="left"><s:textfield name='ctrpDraftSectSponsRespJB.personalContactMiddleName' 
									id='ctrpDraftSectSponsRespJB.personalContactMiddleName' 
									type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
							</tr>
							<tr>
								<td align="right"><%=LC.L_Last_Name%></td>
								<td>&nbsp;&nbsp;</td>
								<td align="left"><s:textfield name='ctrpDraftSectSponsRespJB.personalContactLastName' 
									id='ctrpDraftSectSponsRespJB.personalContactLastName' 
									type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
							</tr>
							<tr>
								<td align="right"><%=LC.L_Street_Address%>
									<span id="ctrpDraftSectSponsRespJB.personalContactStreetAddress_error" class="errorMessage" style="display:none;"></span>
								</td>
								<td>&nbsp;&nbsp;</td>
								<td align="left"><s:textfield name='ctrpDraftSectSponsRespJB.personalContactStreetAddress' id='ctrpDraftSectSponsRespJB.personalContactStreetAddress' type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
							</tr>
							<tr>
								<td align="right"><%=LC.L_City%>
									<span id="ctrpDraftSectSponsRespJB.personalContactCity_error" class="errorMessage" style="display:none;"></span>
								</td>
								<td>&nbsp;&nbsp;</td>
								<td align="left"><s:textfield name='ctrpDraftSectSponsRespJB.personalContactCity' id='ctrpDraftSectSponsRespJB.personalContactCity' type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
							</tr>
							<tr>
								<td align="right"><%=LC.CTRP_DraftStateProvince%>
									<span id="ctrpDraftSectSponsRespJB.personalContactState_error" class="errorMessage" style="display:none;"></span>
								</td>
								<td>&nbsp;&nbsp;</td>
								<td align="left"><s:textfield name='ctrpDraftSectSponsRespJB.personalContactState' id='ctrpDraftSectSponsRespJB.personalContactState' type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
							</tr>
							<tr>
								<td align="right"><%=LC.L_Zip_Code%>
									<span id="ctrpDraftSectSponsRespJB.personalContactZip_error" class="errorMessage" style="display:none;"></span>
								</td>
								<td>&nbsp;&nbsp;</td>
								<td align="left"><s:textfield name='ctrpDraftSectSponsRespJB.personalContactZip' id='ctrpDraftSectSponsRespJB.personalContactZip' type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
							</tr>
							<tr>
								<td align="right"><%=LC.L_Country%>
									<span id="ctrpDraftSectSponsRespJB.personalContactCountry_error" class="errorMessage" style="display:none;"></span>
								</td>
								<td>&nbsp;&nbsp;</td>
								<td align="left"><s:textfield name='ctrpDraftSectSponsRespJB.personalContactCountry' id='ctrpDraftSectSponsRespJB.personalContactCountry' type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
							</tr>
							<tr>
								<td align="right"><%=LC.L_Email_Addr%><%--Modified for Bug 8360 :Yogendra Pratap Singh --%>
									<span id="ctrpDraftSectSponsRespJB.personalContactEmail_error" class="errorMessage" style="display:none;"></span>
								</td>
								<td>&nbsp;&nbsp;</td>
								<td align="left"><s:textfield name='ctrpDraftSectSponsRespJB.personalContactEmail' id='ctrpDraftSectSponsRespJB.personalContactEmail' type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
							</tr>
							<tr>
								<td align="right"><%=LC.L_Phone%>
									<span id="ctrpDraftSectSponsRespJB.personalContactPhone_error" class="errorMessage" style="display:none;"></span>
								</td>
								<td>&nbsp;&nbsp;</td>
								<td align="left"><s:textfield name='ctrpDraftSectSponsRespJB.personalContactPhone' id='ctrpDraftSectSponsRespJB.personalContactPhone' type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
							</tr>
							<tr>
								<td align="right"><%=LC.L_TTY%></td>
								<td>&nbsp;&nbsp;</td>
								<td align="left"><s:textfield name='ctrpDraftSectSponsRespJB.personalContactTty' id='ctrpDraftSectSponsRespJB.personalContactTty' type="text" size="50" maxlength="100" /></td>
							</tr>
							<tr>
								<td align="right"><%=LC.L_Fax%></td>
								<td>&nbsp;&nbsp;</td>
								<td align="left"><s:textfield name='ctrpDraftSectSponsRespJB.personalContactFax' id='ctrpDraftSectSponsRespJB.personalContactFax' type="text" size="50" maxlength="100" /></td>
							</tr>
							<tr>
								<td align="right"><%=LC.L_Url_Upper%></td>
								<td>&nbsp;&nbsp;</td>
								<td align="left"><s:textfield name='ctrpDraftSectSponsRespJB.personalContactUrl' id='ctrpDraftSectSponsRespJB.personalContactUrl' type="text" size="50" maxlength="100" /></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr id="sponsor_generic_contact" style="display:none">
					<td colspan="3">
						<table>
							<tr>
								<td align="right" width="30%"><b><%=LC.CTRP_DraftGenericContactId%></b></td>
								<td valign="top" width="1%"><FONT id="generic_contact_id_asterisk" class="Mandatory">* </FONT></td>
								<td align="left"><s:textfield name='ctrpDraftSectSponsRespJB.genericContactId' id='ctrpDraftSectSponsRespJB.genericContactId' type="text" size="50" maxlength="100" /></td>
							</tr>
							<tr>
								<td align="right"><b><%=LC.CTRP_DraftGenericContactTitle%></b></td>
								<td>&nbsp;&nbsp;</td>
								<td align="left"><s:textfield name='ctrpDraftSectSponsRespJB.genericContactTitle' id='ctrpDraftSectSponsRespJB.genericContactTitle' type="text" size="50" maxlength="100" /></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr>
					<td align="right" width="30%"><b><%=LC.CTRP_DraftResponsiblePartyContact%></b></td>
					<td></td>
					<td><%=MC.CTRP_DraftProvideProfContact%></td>
				</tr>
				<tr>
					<td align="right" width="30%">
						<%=LC.L_Email_Addr%><%--Modified for Bug 8360 :Yogendra Pratap Singh --%>
						<span id="ctrpDraftSectSponsRespJB.responsiblePartyEmail_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td valign="top" width="1%">
						<FONT id="resp_party_email_id_asterisk" class="Mandatory">*&nbsp;</FONT>
					</td>
					<td align="left">
						<s:textfield name='ctrpDraftSectSponsRespJB.responsiblePartyEmail' id='ctrpDraftSectSponsRespJB.responsiblePartyEmail' type="text" size="50" maxlength="100" />
					</td>
				</tr>
				<tr>
					<td align="right" width="30%">
						<%=LC.L_Phone%>
						<span id="ctrpDraftSectSponsRespJB.responsiblePartyPhone_error" class="errorMessage" style="display:none;"></span>
						<span id="ctrpDraftSectSponsRespJB.responsiblePartyPhoneExt_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td valign="top">
						<FONT id="resp_party_email_id_asterisk" class="Mandatory">* </FONT>
					</td>
					<td align="left">
						<s:textfield name='ctrpDraftSectSponsRespJB.responsiblePartyPhone' id='ctrpDraftSectSponsRespJB.responsiblePartyPhone' type="text" size="20" maxlength="30" />
						&nbsp;<%=LC.L_Ext %>
						<s:textfield name='ctrpDraftSectSponsRespJB.responsiblePartyPhoneExt' id='ctrpDraftSectSponsRespJB.responsiblePartyPhoneExt' type="text" size="10" maxlength="10" />
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
