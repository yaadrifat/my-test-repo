<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC,com.velos.eres.service.util.DateUtil"%> 
<%@page import="com.velos.eres.widget.service.util.GadgetCache" %>
<%@page import="org.json.JSONObject" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
HttpSession tSession = request.getSession(false); 
if (!sessionmaint.isValidSession(tSession)) {
	return;
}
String userId = (String)tSession.getAttribute("userId");
GadgetCache.reloadGadgetData();
String jGadgetRegistry = GadgetCache.getGadgetRegistry();
%>
<link rel="stylesheet" type="text/css" href="styles/gadget.css">

<script>
var validationMessages = {};
var toggleGadgetListImgGlobal;
var nodeClickMaxGlobal;
var nodeClickMinGlobal;
var nodeClickCloseGlobal;
var nodeClickSettingsGlobal;
var setSettingsGlobal;
var reloadGadgetScreenGlobal;
var onMousemoveToolTip, onMouseleaveToolTip;
var tooltip, tipText;
var validatorRegistry = [];
var clearerRegistry = [];
var settingsOpenerRegistry = [];
var settingsGetterRegistry = [];
var settingsUpdatorRegistry = [];
var screenActionRegistry = [];
var truncateLongName = function(str, len) {
	if (!str || str.length < len) { return str; }
	return str.substring(0, len);
};

var replicateESign = function(id) {
	$('esign').value = $(id).value;
	ajaxvalidate('misc:esign', 4, id+'_eSign',
            '<%=LC.L_Valid_Esign%>', '<%=LC.L_Invalid_Esign%>', 'sessUserId');
};

var wrapDisplayMessage = function(msg) {
	if (!msg) { return ""; }
	return '<font color="red"><b>'+msg.replace("\'", "&#39;")+'</b></font>';
};

var customCurrencyFormatter = function(elCell, oRecord, oColumn, oData) {
	if (oData && oData.indexOf('E') > -1) {
		oData = parseFloat(oData);
	}
	if (!oData || oData.length < 1) {
		elCell.innerHTML = "-"; return;
	}
	if (oData[0] == '0') {
		try {
			if (parseFloat(oData) < 0.0000001) {
				elCell.innerHTML = "-"; return;
			}
		} catch(e) {}
	}
	elCell.innerHTML = oData;
	$j(elCell).formatCurrency({
		region:appNumberLocale,
		negativeFormat: '(%n)'
	});
};

var customCurrencySorter = function(a, b, desc, field) {
    // Deal with empty values
    if(!YAHOO.lang.isValue(a)) {
        return (!YAHOO.lang.isValue(b)) ? 0 : 1;
    } else if(!YAHOO.lang.isValue(b)) {
        return -1;
    }
    
    // ToDo - if currency symbol is added in the future, make sure to strip it first.
    
    var aFloat = 0.0;
    if (a.getData(field) == '-' || !a.getData(field)) {
    	aFloat = 0.0;
    } else if (a.getData(field).indexOf("(") > -1) {
    	try {
    		aFloat = parseFloat(a.getData(field).replace(/\(/, "").replace(/\)/,""));
    	} catch(e) {}
    } else {
    	try {
    		aFloat = parseFloat(a.getData(field));
    	} catch(e) {}
    }
    
    var bFloat = 1.0;
    if (b.getData(field) == '-' || !b.getData(field)) {
    	bFloat = 0.0;
    } else if (b.getData(field).indexOf("(") > -1) {
    	try {
    		bFloat = parseFloat(b.getData(field).replace(/\(/, "").replace(/\)/,""));
    	} catch(e) {}
    } else {
    	try {
    		bFloat = parseFloat(b.getData(field));
    	} catch(e) {}
    }
 
    var comp = YAHOO.util.Sort.compare;
    var compState = comp(aFloat, bFloat, desc);
    //var compState = comp(a.getData(field), b.getData(field), desc);
    return compState;
 
    // If values are equal, then compare by Column1
    // return (compState !== 0) ? compState : comp(a.getData("Column1"), b.getData("Column1"), desc);
};

var formatErrMsgGlobal = function(id) {
	try {
		var errMsgList = [];
		for (var key in validationMessages[id]) {
			if (!validationMessages[id].hasOwnProperty(key)) { break; }
			errMsgList[key] = '<ul class="errMsg">';
			for (var iX=0; iX<validationMessages[id][key].length; iX++) {
				errMsgList[key] += '<li style="padding-left:0px;" >'+validationMessages[id][key][iX].replace("\'", "&#39;")+'</li>';
			}
			errMsgList[key] += '</ul>';
			$(key+'_error').innerHTML = errMsgList[key];
		}
		for (var key in validationMessages[id]) {
			if (!validationMessages[id].hasOwnProperty(key)) { break; }
			$(key).focus();
			return false;
		}
	} catch(e) {
		// console.log(e.message);
		return false;
	}
	return true;
};
</script>

<div id="doc3" class="yui-skin-sam yui-dt yui-dt-liner yui3-skin-sam yui-t2 yui3-loading" onmouseout='return nd();'>
<jsp:include page="ui-include.jsp" flush="true" />
<!-- Insert gadget include JSP's here -->
<jsp:include page="gadgetSample1Include.jsp" flush="true" />
<jsp:include page="gadgetSample2Include.jsp" flush="true" />
<jsp:include page="gadgetSample3Include.jsp" flush="true" />
<jsp:include page="gadgetFinancialInclude.jsp" flush="true" />
<jsp:include page="gadgetPatSchedulesInclude.jsp" flush="true" />
<jsp:include page="gadgetQuickAccessInclude.jsp" flush="true" />
<jsp:include page="gadgetMyStudiesInclude.jsp" flush="true" />
<!-- End of gadget include JSP's -->

    <div id="pd" onmouseover='return nd();' onmouseout='return nd();'>
        <a id="gadgetListLink" href="javascript:if(toggleGadgetListImgGlobal) {toggleGadgetListImgGlobal();}"> 
            <img id="toggleGadgetList" title="<%=MC.M_Ggt_HideList%>" src="../images/jpg/ListCollapse.jpg" style="border:none;"/>
        </a>
    </div>
    <div id="bd" onmouseout='return nd();'>
        <div id="yui-main" class="yui-g" style="margin-top:0em">
                    <div id="play" style="margin-top:0em" onmouseout='return nd();'>
                        <ul style="float:left; " id="list0" >
                        <li style="list-style-type:none; " >
	                        <div id="gadgetList" >
                            <div id="feeds" class="gdt-glist" >
                            <p><%=MC.M_Ggt_DragFromList%></p>
                            <ul class="gdt-glist-enabled"></ul>
                            </div>
                            </div>
                        </li>
                        </ul>
                        <div class="gadgetContainer" >
                        <ul class="list" id="list1" onmouseout='return nd();'></ul>
                        <ul class="list" id="list2" onmouseout='return nd();'></ul>
                        </div>
                    </div>
	    </div>
    </div>
    <div id="ft" onmouseout='return nd();'>
        <a href="#" id="end_of_gadget_home"></a>
        <input id="esign" name="esign" type="hidden" ></input> 
    </div>
</div>

<div id="tooltip" style="z-index:100;"></div>

<script>

//Use loader to grab the modules needed
YUI().use('panel', 'dd-plugin', 'dd', 'anim', 'yql', 'cookie', 'json', 'node', 
		'io', 'io-base', 'overlay', 'event', 'widget-anim', function(Y) {
    //Make this an Event Target so we can bubble to it
    var Portal = function() {
        Portal.superclass.constructor.apply(this, arguments);
    };
    Portal.NAME = 'portal';
    Y.extend(Portal, Y.Base);
    //This is our new bubble target..
    Y.Portal = new Portal();
    
    // Handle toggle gadget list
    var toggleGadgetListImg = function(e) {
    	var myAnim = new Y.Anim({
    		node: Y.one('#list0'),
    		from: { width: function(node) { if (node._node.clientWidth > 100) { return '15%'; }  return '0%'; } },
    		to: { width: function(node) { if (node._node.clientWidth > 100) { return '0%'; }  return '15%'; } },
    		duration: '.50',
            easing: Y.Easing.easeOut
    	});
    	
		myAnim.run();
    	if ( $j('#toggleGadgetList').attr('src') == '../images/jpg/ListCollapse.jpg' ) {
    		setTimeout(hideGadgetListImg, 170)
    	} else {
    		setTimeout(showGadgetListImg, 50);
    	}
    	
    };
    toggleGadgetListImgGlobal = toggleGadgetListImg;
    var hideGadgetListImg = function() {
		$j("ul.list").css({width:"48%"});
		$j('#toggleGadgetList').attr('src', '../images/jpg/ListExpand.jpg');
		$j('#toggleGadgetList').attr('title', "<%=MC.M_Ggt_ShowList%>");
		Y.one('#list0').hide();
    }
    
    var showGadgetListImg = function() {
		$j('#toggleGadgetList').attr('src', '../images/jpg/ListCollapse.jpg');
		$j('#toggleGadgetList').attr('title', "<%=MC.M_Ggt_HideList%>");
		Y.one('#list0').show();
		$j("ul.list").css({width:"48%"});
    }
    
    Y.one('#toggleGadgetList').on('click', toggleGadgetListImg);
    if ( $j('#toggleGadgetList').attr('src') == '../images/jpg/ListCollapse.jpg' ) {
    	hideGadgetListImg();
    }
    
    // handle tooltip
    var waitingToShow = false;
    
    // array for text to be displayed in tooltips
    tipText = [
        {'name': 'gadgetSample2_a_help', 'text': "This is Gadget Sample 2 gadget."},
        {'name': 'gadgetSample2_studySearchButton', 'text': "<%=MC.M_EntPartial_CompleteText%>"},
        {'name': 'gadgetSample2_studyAdvancedSearchButton', 'text': "<%=MC.M_EntPartial_CompleteText%>"},
        {'name': 'gadgetSample2_patientSearchButton', 'text': "Enter partial or complete Patient ID without checking the checkbox "+
        	"to search for patient on Patient ID. To search for patient on a study, check the checkbox and select a study."},
        {'name': 'gadgetSample2_patientStudySearchButton', 'text': "Enter partial or complete text to search on Patient Study ID."},
        {'name': 'gadgetQuickAccess_studySearchButton', 'text': "<%=MC.M_EntPartial_CompleteText%>"},
        {'name': 'gadgetQuickAccess_studyAdvancedSearchButton', 'text': "<%=MC.M_EntPartial_CompleteText%>"},
        {'name': 'gadgetQuickAccess_patientSearchButton', 'text': "<%=MC.M_Ggt_QuickAccessPatientSearch%>"},
        {'name': 'gadgetQuickAccess_patientStudySearchButton', 'text': "<%=MC.M_Ggt_QuickAccessPatientStudySearch%>"},
        {'name': 'gadgetPatSchedules_saveButton', 'text': "<%=MC.M_SelSave_DefSearchFilter%>"},
        {'name': 'gadgetFinancial_addStudyButton', 'text': "<%=MC.M_EntPartial_CompleteStudy%>"},
        {'name': 'gadgetMyStudies_addStudyButton', 'text': "<%=MC.M_EntPartial_CompleteStudy%>"},
        {'name': 'gadgetMyStudies_studySearchButton', 'text': "<%=MC.M_EntPartial_CompleteText%>"},
        {'name': 'gadgetMyStudies_studyAdvancedSearchButton', 'text': "<%=MC.M_EntPartial_CompleteText%>"}
    ];
    
    tooltip = new Y.Overlay({
        srcNode: "#tooltip",
        visible: false
    }).plug(Y.Plugin.WidgetAnim);
    tooltip.anim.get('animHide').set('duration', 0.005);
    tooltip.anim.get('animShow').set('duration', 0.2);
    tooltip.render();
    
    Y.one('#tooltip').on('mouseleave', onMouseleaveToolTip);

    // handler that positions and shows the tooltip 
    onMousemoveToolTip = function (e) {
        var i;
        if (tooltip.get('visible') === false) {
            // while it's still hidden, move the tooltip adjacent to the cursor
            Y.one('#tooltip').setStyle('opacity', '0');
            var pageY = e.pageY;
            var scrollY = window.pageYOffset ? 
            		window.pageYOffset : document.documentElement.scrollTop ? 
            				document.documentElement.scrollTop : document.body.scrollTop;
            //pageY -= scrollY;
            if (e.pageX > 800) { e.pageX -= 150; }
            tooltip.move([(e.pageX + 10), (pageY + 20)]);
        }
        if (waitingToShow === false) {
            // wait half a second, then show tooltip
            setTimeout(function(){
                Y.one('#tooltip').setStyle('opacity', '1');
                tooltip.show();
            }, 100);
            
            // while waiting to show tooltip, don't let other
            // mousemoves try to show tooltip too.
            waitingToShow = true;
            
            // loop through the tipText array looking for a match between
            // the class name and the array literal `name`
            for (i = 0; i < tipText.length; i += 1) {
                if (e.currentTarget.hasClass(tipText[i].name)) {

                    // found a match, so set the content in the tooltip's body
                    tooltip.setStdModContent('body', tipText[i].text);
                    break;
                }
            }
        }
    };

    // handler that hides the tooltip
    onMouseleaveToolTip = function (e) {
    	setTimeout(function() {
    	tooltip.hide(); 
    	waitingToShow = false;
    	}, 100);
    };
    
    //Setup some private variables..
    var goingUp = false, lastY = 0, trans = {};
 
    //The list of feeds that we are going to use
    var gadgetRegistry = <%=jGadgetRegistry.toString()%>;
    /*
    {
        'ynews-id': {
            id: 'ynews-id',
            title: 'Yahoo! US News',
            url: 'rss.news.yahoo.com/rss/us',
            modal_title: 'YNews Header',
            modal_width: 300
        },
        'yui': {
            id: 'yui',
            title: 'YUI Blog',
            url: 'feeds.yuiblog.com/YahooUserInterfaceBlog'
        },
        'slashdot': {
            id: 'slashdot',
            title: 'Slashdot',
            url: 'rss.slashdot.org/Slashdot/slashdot'
        },
        'ajaxian': {
            id: 'ajaxian',
            title: 'Ajaxian',
            url: 'feeds.feedburner.com/ajaxian'
        },
        'daringfireball': {
            id: 'daringfireball',
            title: 'Daring Fireball',
            url: 'daringfireball.net/index.xml'
        },
        'gadgetSample1': {
            id: 'gadgetSample1',
            title: 'Gadget Sample 1',
            url: 'gadgetSample1.jsp',
            modal_title: 'Sample1 Header',
            modal_width: 250
        },
        'gadgetSample22222222': {
            id: 'gadgetSample22222222',
            title: 'Gadget Sample 22222222',
            url: 'gadgetSample22222222.jsp'
        }
    };
    */
 
    //Simple method for stopping event propagation
    //Using this so we can detach it later
    var stopper = function(e) {
        e.stopPropagation();
    };
 
    //Get the order, placement and minned state of the modules and save them to a cookie
    var _setCookies = function() {
        var dds = Y.DD.DDM._drags;
        var list = {};
        //Walk all the drag elements
        Y.each(dds, function(v, k) {
            var par = v.get('node').get('parentNode');
            //Find all the lists with drag items in them
            if (par.test('ul.list')) {
                if (!list[par.get('id')]) {
                    list[par.get('id')] = [];
                }
            }
        });
        //Walk the list
        var listExists = false;
        Y.each(list, function(v, k) {
            //Get all the li's in the list
            var lis = Y.all('#' + k + ' li.item');
            lis.each(function(v2, k2) {
            	listExists = true;
                //Get the drag instance for the list item
                var dd = Y.DD.DDM.getDrag('#' + v2.get('id'));
                //Get the mod node
                var mod = dd.get('node').one('div.mod');
                //Is it minimized
                var min = (mod.hasClass('minned')) ? true : false;
                //Is it maximized
                
                //Setup the cookie data
                var jobj = { id: dd.get('data').id, min: min };
                list[k][list[k].length] = jobj;
                
            });
        });
        
        if (!listExists) {
        	if ( $j('#toggleGadgetList').attr('src') == '../images/jpg/ListExpand.jpg' ) {
        		toggleGadgetListImg();
        	}
	        list['misc'] = [];
	        list['misc'][0] = { max: false };

        } else {
	        list['misc'] = [];
	        list['misc'][0] = { max: _isMaximized() };
        }
        
        //JSON encode the cookie data and save it in DB
        var cookie = Y.JSON.stringify(list);
        Y.io('gadgetCookie.jsp', {
            method: 'POST',
            data: 'type=typeSaveCookie&cookie='+cookie,
            on: {
                success: function (tranId, o) {
                	resetSessionWarningOnTimer();
                },
                failure: function (tranId, o) {
                }
            }
        }); 

        //Set the sub-cookie
        Y.Cookie.setSub('yui', 'portal', cookie);
    };
    
    var _isMaximized = function() {
    	var isMaxed = false;
    	try {
    		isMaxed = Y.one('#list1').getStyle('width') == '99%';
    	} catch(e) {}
    	return isMaxed;
    }
    
    var setSettings = function(id) {
        if (!settingsUpdatorRegistry[id]) { return; }
        var settings = settingsUpdatorRegistry[id].call(this);
        var jsettings = Y.JSON.stringify(settings);
        Y.io('gadgetCookie.jsp', {
            method: 'POST',
            data: 'type=typeSaveSettings&settings='+jsettings,
            sync: true,
            on: {
                success: function (tranId, o) {
                	resetSessionWarningOnTimer();
                },
                failure: function (tranId, o) {
                }
            }
        }); 
    };
    setSettingsGlobal = setSettings;
 
    //Helper method for creating the feed DD on the left
    var _createFeedDD = function(node, data) {
        //Create the DD instance
        var dd = new Y.DD.Drag({
            node: node,
            data: data,
            bubbles: Y.Portal
        }).plug(Y.Plugin.DDProxy, {
            moveOnEnd: false,
            borderStyle: 'none'
        });
        //Setup some stopper events
        dd.on('drag:start', _handleStart);
        dd.on('drag:end', stopper);
        dd.on('drag:drophit', stopper);
    };
    
    // Helper method to handle click event on the max icon
    var _nodeClickMax = function(div) {
    	var anim1 = new Y.Anim({
            node: '#list1'
        });
    	var anim2 = new Y.Anim({
            node: '#list2'
        });
    	
    	if (Y.one('#list1').getStyle('width') != '99%') {
    		hideGadgetListImg();
    		anim1.setAttrs({
    			from: { width: function(node) { return '48%'; } },
                to: { width: function(node) { return '99%'; } },
                duration: '.50',
                easing: Y.Easing.easeOut,
                iteration: 1
            });
    		anim2.setAttrs({
    			from: { width: function(node) { return '48%'; } },
                to: { width: function(node) { return '99%'; } },
                duration: '.50',
                easing: Y.Easing.easeOut,
                iteration: 1
            });
    		anim2.on('end', function() {
    	    	_setCookies();
    			var myNode = div.get('parentNode');
    			var myMatches = myNode.get('className').match(/\bvelos-[\S]+\b/g);
    			var gadgetId = null;
    			if (myMatches) {
    				gadgetId = myMatches[0].substring(6);
        			$('end_of_gadget_home').focus();
    				$('a_max_'+gadgetId).focus();
    			}
    		});
    	} else {
    		anim1.setAttrs({
    			from: { width: function(node) { return '99%'; } },
                to: { width: function(node) { return '48%'; } },
                duration: '.50',
                easing: Y.Easing.easeOut,
                iteration: 1
            });
    		anim2.setAttrs({
    			from: { width: function(node) { return '99%'; } },
                to: { width: function(node) { return '48%'; } },
                duration: '.50',
                easing: Y.Easing.easeOut,
                iteration: 1
            });
    		anim2.on('end', function() {
    	    	_setCookies();
    			var myNode = div.get('parentNode');
    			var myMatches = myNode.get('className').match(/\bvelos-[\S]+\b/g);
    			var gadgetId = null;
    			if (myMatches) {
    				gadgetId = myMatches[0].substring(6);
        			//$('end_of_gadget_home').focus();
    				$('a_max_'+gadgetId).focus();
    			}
    		});
    	}
		anim1.run();
		anim2.run();
    };
    
    // Helper method to handle click event on the min icon
    var _nodeClickMin = function(div) {
    	var anim = null;
        //Get some node references
        var ul = div.one('table') ? div.one('table') : div.one('ul');
        var h2 = div.one('h2'),
            h = h2.get('offsetHeight'),
            hUL = ul.get('offsetHeight')+7,
            inner = div.one('div.inner');
                        
        //Create an anim instance on this node.
        anim = new Y.Anim({
            node: inner
        });
        //Is it expanded?
        if (!div.hasClass('minned')) {
            //Set the vars for collapsing it
            anim.setAttrs({
                to: {
                    height: 0
                },
                duration: '.25',
                easing: Y.Easing.easeOut,
                iteration: 1
            });
            //On the end, toggle the minned class
            //Then set the cookies for state
            anim.on('end', function() {
                div.toggleClass('minned');
                _setCookies();
            });
        } else {
            //Set the vars for expanding it
            anim.setAttrs({
                to: {
                    height: (hUL)
                },
                duration: '.25',
                easing: Y.Easing.easeOut,
                iteration: 1
            });
            //Toggle the minned class
            //Then set the cookies for state
            anim.on('end', function() {
                inner.setStyle('height', 'auto');
                div.toggleClass('minned');
                _setCookies();
            });
        }
        //Run the animation
        anim.run();
    };
    
    // Helper method to handle click event on the close icon
    var _nodeClickClose = function(div) {
        //Get some Node references..
        var li = div.get('parentNode'),
            id = li.get('id'),
            dd = Y.DD.DDM.getDrag('#' + id),
            data = dd.get('data'),
            item = Y.one('#' + data.id);

    	var jButtons = $j(('[id*=Button_'+data.id+']'));
    	jButtons.each(function (index) {
    		$j(this).fadeOut(200);
    	});
    	var jTabs = $j(('[id*=Tabs_'+data.id+']'));
    	jTabs.each(function (index) {
    		$j(this).fadeOut(200);
    	});

        //Destroy the DD instance.
        dd.destroy();
        //Setup the animation for making it disappear
        var anim = new Y.Anim({
            node: div,
            to: {
                opacity: 0
            },
            duration: '.25',
            easing: Y.Easing.easeOut
        });
        anim.on('end', function() {
            //On end of the first anim, setup another to make it collapse
            var anim = new Y.Anim({
                node: div,
                to: {
                    height: 0
                },
                duration: '.25',
                easing: Y.Easing.easeOut
            });
            anim.on('end', function() {
                //Remove it from the document
                li.get('parentNode').removeChild(li);
                item.removeClass('gdt-glist-disabled');
                //Setup a drag instance on the feed list
                _createFeedDD(item, data);
                _setCookies();

            });
            //Run the animation
            anim.run();
        });
        //Run the animation
        anim.run();
    };
    
    var _nodeClickSettings = function(a) {
    	var myNode = a.get('parentNode').get('parentNode').get('parentNode'); 
    	var myMatches = myNode.get('className').match(/\bvelos-[\S]+\b/g);
    	var gadgetId = null;
    	if (myMatches) { 
    		gadgetId = myMatches[0].substring(6);
        	if (settingsOpenerRegistry[gadgetId]) { 
        		settingsOpenerRegistry[gadgetId].call(this);
        	}
    	}
    	//createModalDialog(gadgetId);
    };

    
    //Handle the node:click event
    var _nodeClick = function(e) {
        //Is the target an href?
        if (e.target.test('img')) {
            var a = e.target.get('parentNode'), div = a.get('parentNode').get('parentNode');
            
            //Was the max button clicked?
            if (a.hasClass('max')) {
            	_nodeClickMax(div);
            }
            
            //Was the min button clicked?
            if (a.hasClass('min')) {
            	_nodeClickMin(div);
            }
            
            //Was close clicked?
            if (a.hasClass('close')) {
            	_nodeClickClose(div);
            }
            
            //Was settings clicked?
            if (a.hasClass('setting')) {
            	_nodeClickSettings(a);
            }
            //Stop the click
            e.halt();
        }
    };
    
    nodeClickMaxGlobal = function(id) {
    	var a = Y.one('#a_max_'+id), div = a.get('parentNode').get('parentNode');
        if (a.hasClass('max')) {
        	_nodeClickMax(div);
        }
    };
    
    nodeClickMinGlobal = function(id) {
    	var a = Y.one('#a_min_'+id), div = a.get('parentNode').get('parentNode');
        if (a.hasClass('min')) {
        	_nodeClickMin(div);
        }
    };
    
    nodeClickCloseGlobal = function(id) {
    	var a = Y.one('#a_close_'+id), div = a.get('parentNode').get('parentNode');
        if (a.hasClass('close')) {
        	_nodeClickClose(div);
        }
    };
    
    nodeClickSettingsGlobal = function(id) {
    	var a = Y.one('#a_set_'+id), div = a.get('parentNode').get('parentNode');
        if (a.hasClass('setting')) {
        	_nodeClickSettings(a);
        }
    };

    var modalPanelRegistry = [];
    var createModalDialog = function(id) {
    	if (!$('gadget-modal-'+id)) { return; }
    	if (settingsGetterRegistry[id]) {
        	Y.io('gadgetCookie.jsp', {
            	method: 'POST',
            	data: 'type=typeGetSettings',
            	sync: true,
            	on: {
                	success: function (tranId, o) {
                		resetSessionWarningOnTimer();
                    	settingsGetterRegistry[id].call(this, o);
                    },
                    failure: function (tranId, o) {
                    }
                }
            });
    	}
    	$('gadget-modal-'+id).style.display = 'block';
    	var width = gadgetRegistry[id].modal_width;
    	var header = gadgetRegistry[id].modal_title;
    	if (!width) { width = 250; }
    	
    	if (!modalPanelRegistry[id]) {
	        var modalPanel = new Y.Panel({
	            srcNode      : '#gadget-modal-'+id,
	            headerContent: header,
	            width        : width,
	            zIndex       : 5,
	            centered     : true,
	            modal        : true,
	            visible      : false,
	            render       : true,
	            resizeable   : true,
	            plugins      : [Y.Plugin.Drag]
	        });
	        modalPanel.addButton({
	            value  : 'Cancel',
	            section: Y.WidgetStdMod.FOOTER,
	            action : function (e) {
	                e.preventDefault();
	                clearerRegistry[id].call(this);
	                this.hide();
	            }
	        });
	        modalPanel.addButton({
	            value  : 'OK',
	            section: Y.WidgetStdMod.FOOTER,
	            action : function (e) {
	                e.preventDefault();
	                if (submitModal(id)) {
	                    this.hide();
	                }
	            }
	        });

	        modalPanelRegistry[id] = modalPanel;
    	}
    	modalPanelRegistry[id].show();
    }
    
    var submitModal = function(id) {
    	var rVal = validatorRegistry[id].call(this);
    	if (!rVal) { return false; }
    	setSettings(id);
    	_sendYQL(id);
    	reloadGadgetScreen(id);
    	return true;
    };
    
    //This creates the module, either from a drag event or from the cookie load
    var setupModDD = function(mod, data, dd) {
        var node = mod;
        //Listen for the click so we can react to the buttons
        node.one('h2').on('click', _nodeClick);
        
        node.addClass('velos-'+data.id);
        
        //Remove the event's on the original drag instance
        dd.detachAll('drag:start');
        dd.detachAll('drag:end');
        dd.detachAll('drag:drophit');
        
        //It's a target
        dd.set('target', true);
        //Setup the handles
        dd.addHandle('h2').addInvalid('a');
        
        //Remove the mouse listeners on this node
        dd._unprep();
        //Update a new node
        dd.set('node', mod);
        //Reset the mouse handlers
        dd._prep();
        
        var inner = node.one('div.inner');
        var gadgetHTML = 
        		'<div class="inner">' +
                '    <div class="loading"><%=MC.M_Ggt_Loading%></div>' + 
                '</div>';
        inner.set('innerHTML', gadgetHTML);
        var proxy_inner;
        if (Y.DD.DDM.activeDrag) {
            //If we are still dragging, update the proxy element too..
            proxy_inner = Y.DD.DDM.activeDrag.get('dragNode').one('div.inner');
            proxy_inner.set('innerHTML', gadgetHTML);
        }
        
        _getGadgetHTML(data.id, data.url, inner, proxy_inner);
        
        _sendYQL(data.id);
 
    };
    
    var _sendYQL = function(id) {
    	var mod = Y.one('.velos-'+id);
    	var url = gadgetRegistry[id].url
        Y.YQL('select * from feed where url="http:/'+'/' + url + '"', (function(mod) {
            return function(r) {
                if (r && r.query && r.query.results) {
                    var inner = mod.one('div.inner'),
                    html = '';
                    
                    //Walk the list and create the news list
                    Y.each(r.query.results, function(items) {
                        Y.each(items, function(v, k) {
                            if (k < 5) {
                                if (v.title && v.title.content) {
                                    v.title = v.title.content;
                                }
                                if (v.link && (Y.Lang.isArray(v.link))) {
                                    v.link = v.link[0];
                                }
                                if (v.link && (Y.Lang.isObject(v.link))) {
                                    v.link = v.link.href;
                                }
                                html += Y.Lang.sub('<li style="padding-left: 20px; font-size: 80%; "><a href="{link}" target="_blank">{title}</a>', v);
                            }
                        });
                    });
                    //Set the innerHTML of the module
                    inner.set('innerHTML', '<ul>' + html + '</ul>');
                    if (Y.DD.DDM.activeDrag) {
                        //If we are still dragging, update the proxy element too..
                        var proxy_inner = Y.DD.DDM.activeDrag.get('dragNode').one('div.inner');
                        proxy_inner.set('innerHTML', '<ul>' + html + '</ul>');
                        
                    }
                    
                }
            }
        })(mod));
    };
    
    var _getGadgetHTML = function(id, url, inner, proxy_inner) {
    	jQuery.ajax({
    		url:url,
    		async: true,
    		cache: false,
    		success: function(resp) {
    			resetSessionWarningOnTimer();
    			//if (proxy_inner) { proxy_inner.set('innerHTML', resp); }
    			inner.set('innerHTML', resp);
    			if (screenActionRegistry[id]) {
    				screenActionRegistry[id].call(this);
    			}
    		}
    	});
    };
    
    // This only works for the same-site URL
    var reloadGadgetScreen = function(id) {
    	if (!Y.one('.velos-'+id) || !Y.one('.velos-'+id).one('div.inner')) { return; }
    	Y.one('.velos-'+id).one('div.inner').set('innerHTML', '');
    	var gadgetHTML;
    	var myUrl = gadgetRegistry[id].url;
    	var cfg = {
    		    method: "POST",
    		    data: "",
    		    sync: true,
                on: {
                    success: function (tranId, o) {
                    	resetSessionWarningOnTimer();
                    	if (o.responseText) {
                    		gadgetHTML = o.responseText; 
                    	}
                    },
                    failure: function (tranId, o) {
                    	gadgetHTML = '<div class="loading">Error occurred while updating gadget</div>';
                    }
                }
    	};
        Y.io(myUrl, cfg);
        if (gadgetHTML) {
        	Y.one('.velos-'+id).one('div.inner').set('innerHTML', gadgetHTML);
        }
    };
    reloadGadgetScreenGlobal = reloadGadgetScreen;
 
    //Helper method to create the markup for the module..
    var _createMod = function(feed) {
    	var myTitle = feed.title ? feed.title : '';
    	var myId = feed.id ? feed.id : '';
    	var mySettings = (settingsOpenerRegistry[myId]) ? 
                '<a id="a_set_'+myId+'" title="Configure settings" class="setting" href="javascript:nodeClickSettingsGlobal(\''+myId+'\')">'+
                '<img style="width:16px; height:16px; border:none; valign:bottom;" src="./images/setting.png"/></a>' : '';
        var str = '<li class="item" onmouseout="return nd();">' +
                    '<div class="mod">' ;

      if(myId == "gadgetFinancial")
      {
          str += '<h2 class="gadgetFinancial" style="background-position: 6px 50%;" >' + myTitle + mySettings +
              '<a id="'+myId+'_a_help" title="<%=MC.M_ThisIs_FinancialGadget%>" class="help '+myId+'_a_help" href="javascript:void(0);">';
      }
      else if(myId == "gadgetPatSchedules")
      {
          str += '<h2 class="gadgetPatSchedules" style="background-position: 6px 50%;"  >' + myTitle + mySettings +
              '<a id="'+myId+'_a_help" title="<%=MC.M_ThisIs_PatSchedulesGadget%>" class="help '+myId+'_a_help" href="javascript:void(0);">';
      }
      else if(myId == "gadgetQuickAccess")
      {
    	  str += '<h2 class="gadgetQuickAccess" style="background-position: 6px 50%;"  >' + myTitle + mySettings +
    	      '<a id="'+myId+'_a_help" title="<%=MC.M_Ggt_QuickAccessHelp%>" class="help '+myId+'_a_help" href="javascript:void(0);">';
      }
      else if (myId == "gadgetMyStudies")
      {
    	  str += '<h2 class="gadgetMyStudies" style="background-position: 6px 50%;"  >' + myTitle + mySettings +
	          '<a id="'+myId+'_a_help" title="<%=MC.M_ThisIs_MyStudiesGadget%>" class="help '+myId+'_a_help" href="javascript:void(0);">';
      }
      else
      {
          str += '<h2>' + myTitle + mySettings+
	          '<a id="'+myId+'_a_help" title="" class="help '+myId+'_a_help" href="javascript:void(0);">';
      }
         str += 
                        '<img style="width:16px; height:16px; border:none; valign:bottom;" src="../images/help.png"/></a>' +
                        '<a id="a_min_'+myId+'" title="<%=MC.M_Ggt_Minimize%>" class="min" href="javascript:nodeClickMinGlobal(\''+myId+'\')">'+
                        '<img style="width:16px; height:16px; border:none; valign:bottom;" src="./images/line.gif"/></a>' +
                        '<a id="a_max_'+myId+'" title="<%=MC.M_Ggt_Expand%>" class="max" href="javascript:nodeClickMaxGlobal(\''+myId+'\')">'+
                        '<img style="width:16px; height:16px; border:none; valign:bottom; " src="./images/maximize.gif"/></a>' +
                        '<a id="a_close_'+myId+'" title="<%=MC.M_Ggt_Close%>" class="close" href="javascript:nodeClickCloseGlobal(\''+myId+'\')">'+
                        '<img style="width:16px; height:16px; border:none; valign:bottom; " src="./images/delete.gif"/></a></h2>' +
                        '<div class="inner">' +
                        '    <div class="loading"><%=MC.M_Ggt_Loading%></div>' + 
                        '</div>' +
                    '</div>' +
                '</li>';
        return Y.Node.create(str);
    };
    
    var _parseCookie = function(obj) {
        //Walk the data
        Y.each(obj, function(v, k) {
            //Get the node for the list
            var list = Y.one('#' + k);
            if (k == 'misc') {
            	if (v[0].max) {
            		hideGadgetListImg();
            		Y.one('#list1').setStyle('width', '99%');
            		Y.one('#list2').setStyle('width', '99%');
            	}
            }
            //Walk the items in this list
            Y.each(v, function(v2, k2) {
                //Get the drag for it
                var drag = Y.DD.DDM.getDrag('#' + v2.id);
                if (!drag || !drag.get('data')) return;
                //Create the module
                var mod = _createMod(drag.get('data'));
                if (v2.min) {
                    //If it's minned add some CSS
                    mod.one('div.mod').addClass('minned');
                    mod.one('div.inner').setStyle('height', '0px');
                }
                //Add it to the list
                list.appendChild(mod);
                //Set the drag listeners
                drag.get('node').addClass('gdt-glist-disabled');
                drag.set('node', mod);
                drag.set('dragNode', Y.DD.DDM._proxy);
                drag.detachAll('drag:start');
                drag.detachAll('drag:end');
                drag.detachAll('drag:drophit');
                drag._unprep();
                //Setup the new Drag listeners
                setupModDD(mod, drag.get('data'), drag);
            });
        });
    };
    

    //Handle the start Drag event on the left side
    var _handleStart = function(e) {
        //Stop the event
        stopper(e);
        //Some private vars
        var drag = this,
            list3 = Y.one('#list1'),
            mod = _createMod(drag.get('data'));
        
        //Add it to the first list
        list3.appendChild(mod);
        //Set the item on the left column disabled.
        drag.get('node').addClass('gdt-glist-disabled');
        //Set the node on the instance
        drag.set('node', mod);
        //Add some styles to the proxy node.
        drag.get('dragNode').setStyles({
            opacity: '.5',
            borderStyle: 'none',
            width: '320px',
            height: '61px'
        });
        //Update the innerHTML of the proxy with the innerHTML of the module
        var srcId = drag.get('data').id;
        drag.get('dragNode').set('innerHTML', drag.get('node').get('innerHTML'));
        //set the inner module to hidden
        drag.get('node').one('div.mod').setStyle('visibility', 'hidden');
        //add a class for styling
        drag.get('node').addClass('moving');
        drag.get('node').addClass('velos-'+srcId);
        //Setup the DD instance
        setupModDD(mod, drag.get('data'), drag);
 
        //Remove the listener
        this.detach('drag:start', _handleStart);
    };
    
    //Walk through the feeds list and create the list on the left
    var feedList = Y.one('#feeds ul');
    Y.each(gadgetRegistry, function(v, k) {
		var li = "";
        if(v.id == "gadgetFinancial")
        	li = Y.Node.create('<li id="' + k + '" class="gadgetFinancial" >' + v.title + '</li>');
        else if(v.id == "gadgetQuickAccess")
        	li = Y.Node.create('<li id="' + k + '" class="gadgetQuickAccess" >' + v.title + '</li>');
        else if(v.id == "gadgetPatSchedules")
        	li = Y.Node.create('<li id="' + k + '" class="gadgetPatSchedules" >' + v.title + '</li>');
        else if(v.id == "gadgetMyStudies")
        	li = Y.Node.create('<li id="' + k + '" class="gadgetMyStudies" >' + v.title + '</li>');
        else 
       		li = Y.Node.create('<li id="' + k + '">' + v.title + '</li>');    
        feedList.appendChild(li);
        //Create the DD instance for this item
        _createFeedDD(li, v);
    });
 
    //This does the calculations for when and where to move a module
    var _moveMod = function(drag, drop) {
        if (drag.get('node').hasClass('item')) {
            var dragNode = drag.get('node'),
                dropNode = drop.get('node'),
                append = false,
                padding = 30,
                xy = drag.mouseXY,
                region = drop.region,
                middle1 = region.top + ((region.bottom - region.top) / 2),
                middle2 = region.left + ((region.right - region.left) / 2),
                dir = false,
                dir1 = false,
                dir2 = false;
                
                //We could do something a little more fancy here, but we won't ;)
                if ((xy[1] < (region.top + padding))) {
                    dir1 = 'top';
                }
                if ((region.bottom - padding) < xy[1]) {
                    dir1 = 'bottom';
                }
                if ((region.right - padding) < xy[0]) {
                    dir2 = 'right';
                }
                if ((xy[0] < (region.left + padding))) {
                    dir2 = 'left';
                }
                dir = dir2;
                if (dir2 === false) {
                    dir = dir1;
                }
                switch (dir) {
                    case 'top':
                        var next = dropNode.get('nextSibling');
                        if (next) {
                            dropNode = next;
                        } else {
                            append = true;
                        }
                        break;
                    case 'bottom':
                        break;
                    case 'right':
                    case 'left':
                        break;
                }
            
 
            if ((dropNode !== null) && dir) {
                if (dropNode && dropNode.get('parentNode')) {
                    if (!append) {
                        dropNode.get('parentNode').insertBefore(dragNode, dropNode);
                    } else {
                        dropNode.get('parentNode').appendChild(dragNode);
                    }
                }
            }
            //Resync all the targets because something moved..
            Y.Lang.later(50, Y, function() {
                Y.DD.DDM.syncActiveShims(true);
            });
        }
    };
 
    /*
    Handle the drop:enter event
    Now when we get a drop enter event, we check to see if the target is an LI, then we know it's out module.
    Here is where we move the module around in the DOM.    
    */
    Y.Portal.on('drop:enter', function(e) {
        if (!e.drag || !e.drop || (e.drop !== e.target)) {
            return false;
        }
        if (e.drop.get('node').get('tagName').toLowerCase() === 'li') {
            if (e.drop.get('node').hasClass('item')) {
                _moveMod(e.drag, e.drop);
            }
        }
    });
 
    //Handle the drag:drag event
    //On drag we need to know if they are moved up or down so we can place the module in the proper DOM location.
    Y.Portal.on('drag:drag', function(e) {
        var y = e.target.mouseXY[1];
        if (y < lastY) {
            goingUp = true;
        } else {
            goingUp = false;
        }
        lastY = y;
    });
 
    /*
    Handle the drop:hit event
    Now that we have a drop on the target, we check to see if the drop was not on a LI.
    This means they dropped on the empty space of the UL.
    */
    Y.Portal.on('drag:drophit', function(e) {
        var drop = e.drop.get('node'),
            drag = e.drag.get('node');
 
        if (drop.get('tagName').toLowerCase() !== 'li') {
            if (!drop.contains(drag)) {
                drop.appendChild(drag);
            }
        }
    });
 
    //Handle the drag:start event
    //Use some CSS here to make our dragging better looking.
    Y.Portal.on('drag:start', function(e) {
        var drag = e.target;
        if (drag.target) {
            drag.target.set('locked', true);
        }
        drag.get('dragNode').set('innerHTML', drag.get('node').get('innerHTML'));
        drag.get('dragNode').setStyle('opacity','.5');
        drag.get('node').one('div.mod').setStyle('visibility', 'hidden');
        drag.get('node').addClass('moving');
    });
 
    //Handle the drag:end event
    //Replace some of the styles we changed on start drag.
    Y.Portal.on('drag:end', function(e) {
        var drag = e.target;
        if (drag.target) {
            drag.target.set('locked', false);
        }
        drag.get('node').setStyle('visibility', '');
        drag.get('node').one('div.mod').setStyle('visibility', '');
        drag.get('node').removeClass('moving');
        drag.get('dragNode').set('innerHTML', '');
        _setCookies();
    });
    
 
    //Handle going over a UL, for empty lists
    Y.Portal.on('drop:over', function(e) {
        var drop = e.drop.get('node'),
            drag = e.drag.get('node');
 
        if (drop.get('tagName').toLowerCase() !== 'li') {
            if (!drop.contains(drag)) {
                drop.appendChild(drag);
                Y.Lang.later(50, Y, function() {
                    Y.DD.DDM.syncActiveShims(true);
                });
            }
        }
    });
 
    //Create simple targets for the main lists..
    var uls = Y.all('#play ul.list');
    uls.each(function(v, k) {
        var tar = new Y.DD.Drop({
            node: v,
            padding: '20 0',
            bubbles: Y.Portal
        });
    });
    
    
    //Get the cookie data
    var cookie = null;
    var getGadgetCookie = function() {
    	jQuery.ajax({
    		url:'gadgetCookie.jsp',
    		data:{type:'typeGetCookie'},
    		async: false,
    		cache: false,
    		success: function(resp) {
    			resetSessionWarningOnTimer();
    			cookie = resp;
    			if (cookie == null || jQuery.trim(cookie) == 'null') { cookie = null; }
    			// if cookie is null the very first time, add Quick Access gadget 
    			if (!cookie) {
    				cookie = '{"list1":[{"id":"gadgetQuickAccess","min":false}]}';
    			}
    			var parsedCookie = Y.JSON.parse(cookie);
    			if (!parsedCookie.list1 && !parsedCookie.list2) {
    				showGadgetListImg();
    			}
    		}
    	});
    }();
    
    if (!cookie) { cookie = Y.Cookie.getSub('yui', 'portal'); }
    
    if (cookie) {
        //JSON parse the stored data
    	_parseCookie(Y.JSON.parse(cookie));
    }
}); // End of YUI().use
$('gadgetListLink').focus();
$j('#feeds').attr('style','min-height:445px;');
$j('#list1').attr('style','min-height:450px;');
$j('#list2').attr('style','min-height:450px;');

if (navigator.userAgent.indexOf("MSIE") > -1) {
	if (navigator.userAgent.indexOf("MSIE 10") < 0
			&& $j('link[href="./styles/ie_1024.css"]')) {
		$j('link[href="./styles/ie_1024.css"]').remove();
	}
	if (navigator.userAgent.indexOf("MSIE 8") > -1) {
		$j('<link rel="stylesheet" type="text/css" href="styles/ie_1024.css" >').appendTo("head");
	}
}
$j(document).ready(function(){
	var screenWidth = screen.width;
	var screenHeight = screen.height;

	if(screenHeight<=768){
		$j("div.gadgetContainer").css({height:"550px"});
	} else {
		$j("div.gadgetContainer").css({height:"770px"});
	}
});

</script>
