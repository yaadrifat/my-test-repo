<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<title><%=LC.L_Acc_Blocked%><%-- Account Blocked*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="styles/login.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" type="image/x-icon" href="./favicon.ico" />
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<%@ page import="com.velos.eres.service.util.*"%>
<%@page import = "com.velos.eres.service.util.Configuration"%>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>


<%
String supportEmail = Configuration.SUPPORTEMAIL;
int failedAccessCount = Configuration.FAILEDACCESSCOUNT;
%>

<table width = "100%" border = 0>
<tr><td>
<p>
<%--<font font-family =  "arial" color="red"; font-size=14><b> Account Blocked</b>*****--%>
<font font-family =  "arial" color="red"; font-size=14><b> <%=LC.L_Acc_Blocked%></b>
</p>
</td></tr>
<tr><td><br><p class = "defComments">
<%--There have been <%=failedAccessCount%> failed login attempts to this account and the account is now Blocked. 
To reactivate your account, please contact your account administrator. 
If you are an account administrator, contact Customer Support at*****--%> 
<%=LC.L_There_HaveBeen%> <%=failedAccessCount%> <%=MC.M_FailLogin_AttemptAccBlock%> <a href="mailto:<%=supportEmail%>"><%=supportEmail%></a> </p>
</td></tr>
<tr><td align="center"><br><br><%--Modified By Tarun Kumar : Bug#10302 --%>
<!--<button onClick="window.self.close();"  style="text-align: center;" ><font color="white"><%=LC.L_Close %></font></button>-->
<button type="button" style="text-align: center;" onClick = "goBack();"><font color="white"><%=LC.L_Close %></font></button>
</td></tr>
</table>
<SCRIPT>
function goBack(){
	 document.location.href = "ereslogin.jsp";
}	  
</SCRIPT>

