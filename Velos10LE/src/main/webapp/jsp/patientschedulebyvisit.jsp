<%--
This JSP inherited most of the logic from patientschedule.jsp. This JSP gets called 
by patientschedule.jsp via AJAX for a particular visit and returns the events in that visit.
 --%>
 <%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <jsp:include page="localization.jsp" flush="true"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
<jsp:useBean id="eventAssocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<!--<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>-->
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="ctrl1" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="patB" scope="request" class="com.velos.eres.web.patStudyStat.PatStudyStatJB"/>
<jsp:useBean id="bRows" scope="request" class="com.velos.eres.web.browserRows.BrowserRowsJB"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>
<jsp:useBean id="protVisitB" scope="request" class="com.velos.esch.web.protvisit.ProtVisitJB"/><!--//JM: 05May2008, added for, Enh. #PS16-->
<jsp:useBean id="labServiceFactory" scope="session" class="com.velos.remoteservice.lab.LabServiceFactory" />
<%@page import="com.velos.eres.business.common.*,com.velos.eres.service.util.*"%>
<%@page import="com.velos.eres.web.objectSettings.ObjectSettingsCache,com.velos.eres.web.objectSettings.ObjectSettings"%>
<%@page import="com.velos.eres.web.studyRights.StudyRightsJB"%>
 <%@page import="com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@page import="com.velos.esch.business.common.*"%>
<%@page import="com.velos.remoteservice.lab.*,java.net.URLEncoder,com.velos.eres.business.section.*"%>
<%@page import="java.util.*,java.io.*"%>
<%@page import="org.json.*,org.w3c.dom.*"%>
<%!
private static final String UTFCharSet = "UTF-8";
private static final String OddRowRGB = "#D3D3D3";
private static final String EvenRowRGB = "#FFFFFF";
private static final String CoverageTypeForCodeList = "coverage_type";
private static final String PatSchedForms = "pat_sched_forms";
private static final String PatSchedSite = "pat_sched_site";
private static final String PatSchedCov = "pat_sched_cov";
private static final String PatSchedAddl = "pat_sched_addl";
private static final String CovTypeLegend = "covTypeLegend";
private static final String EptRight = "eptRight";
private static final String CtrlValue = "ctrlValue";
private static final String FormLibAppRight = "formLibAppRight";
private static final String CRF = "CRF";
private static final String CrfString = "crfString";
private static final String KitCount = "kitCount";
private static final String Org_id = "org_id";
private static final String Event_id = "event_id";
private static final String StorageRight = "storageRight"; /*YK14Sep2011 : Fixed Bug#6979 */

private String printConfigurableColHeader(ArrayList patSchedColList, HashMap headerMap) {
    StringBuffer sb1 = new StringBuffer();
    for (int iX=0; iX<patSchedColList.size(); iX++) {
        ObjectSettings setting = (ObjectSettings) patSchedColList.get(iX);
        if (PatSchedForms.equals(setting.getObjSubType())) {
            if (!"1".equals(headerMap.get(EptRight))) { continue; }
            if (!"1".equals(headerMap.get(CtrlValue))) { continue; }
            sb1.append("<TH ");
            if ("0".equals(setting.getObjVisible())) {
                sb1.append(" style='display:none' ");
            }
            sb1.append(">");
            String formLibAppRight = StringUtil.trueValue((String)headerMap.get(FormLibAppRight));
            if (formLibAppRight.compareTo("1") == 0) {
                sb1.append(setting.getObjDispTxt());
            } else {
                sb1.append(CRF);
            }
            sb1.append("</TH>");
        } else if (PatSchedSite.equals(setting.getObjSubType())) {
            sb1.append("<TH ");
            if ("0".equals(setting.getObjVisible())) {
                sb1.append(" style='display:none' ");
            }
            sb1.append(">").append(setting.getObjDispTxt()).append("</TH>");
        } else if (PatSchedCov.equals(setting.getObjSubType())) {
            sb1.append("<TH valign='middle' ");
            if ("0".equals(setting.getObjVisible())) {
                sb1.append(" style='display:none' ");
            }
            sb1.append(">");
            sb1.append(setting.getObjDispTxt());
            sb1.append("&nbsp;<img style='height:15px' src='../images/jpg/help.gif' border='0'");
            sb1.append("onmouseover=\"return overlib('").append(headerMap.get(CovTypeLegend)).append("',CAPTION,'");
            sb1.append(setting.getObjDispTxt()).append(" "+LC.L_Legend+"');\"");/*sb1.append(setting.getObjDispTxt()).append(" Legend');\"");*****/
            sb1.append("onmouseout='return nd();'></img>");
            sb1.append("</TH>");
        } else if (PatSchedAddl.equals(setting.getObjSubType())) {
            sb1.append("<TH ");
            if ("0".equals(setting.getObjVisible())) {
                sb1.append(" style='display:none' ");
            }
            sb1.append(">").append(setting.getObjDispTxt()).append("</TH>");
        }
    }
    headerMap.clear();
    return sb1.toString();
}

private String printConfigurableColData(ArrayList patSchedColList, HashMap dataMap) {	
    StringBuffer sb1 = new StringBuffer();      
    int storageRight=0; /*YK14Sep2011 : Fixed Bug#6979 */
    for (int iX=0; iX<patSchedColList.size(); iX++) {
        ObjectSettings setting = (ObjectSettings) patSchedColList.get(iX);
        if (PatSchedForms.equals(setting.getObjSubType())) {
            if (!"1".equals(dataMap.get(EptRight))) { continue; }
            if (!"1".equals(dataMap.get(CtrlValue))) { continue; }
            sb1.append("<TD style='padding: 0px 0px 0px 0px'");
            if ("0".equals(setting.getObjVisible())) {
                sb1.append(" style='display:none' ");
            }
            sb1.append(">");
            sb1.append(dataMap.get(CrfString));
            storageRight = Integer.parseInt((String)dataMap.get(StorageRight));
            if (dataMap.get(KitCount) != null) {
                int kitCount = ((Integer)dataMap.get(KitCount)).intValue();
                if (kitCount > 0 && storageRight>=4) { /*YK14Sep2011 : Fixed Bug#6979 */    //Akshi :Fixed for Bug#6979
                    sb1.append("<BR><A href=\"#\" onClick=\"f_getKit(");
                    sb1.append(dataMap.get(Org_id)).append(" ,").append(dataMap.get(Event_id));
                    sb1.append(");\">"+LC.L_ViewOrHide_StrgKit+"</A> &nbsp;");  /*Akshi : Fixed Bug#7026 and Bug#6979 */                       
                    sb1.append("<span id=spec_").append(dataMap.get(Org_id));
                    sb1.append(" style=\"display: none;\"></span>");
                }
            }
            sb1.append("</TD>");
        } else if (PatSchedSite.equals(setting.getObjSubType())) {
            sb1.append("<TD style='padding: 0px 0px 0px 8px'");
            if ("0".equals(setting.getObjVisible())) {
                sb1.append(" style='display:none' ");
            }
            sb1.append(">");
            sb1.append(dataMap.get(PatSchedSite));
            sb1.append("</TD>");
        } else if (PatSchedCov.equals(setting.getObjSubType())) {
            sb1.append("<TD style='padding: 0px 0px 0px 7px'");
            if ("0".equals(setting.getObjVisible())) {
                sb1.append(" style='display:none' ");
            }
            sb1.append(">");
            sb1.append(dataMap.get(PatSchedCov));
            sb1.append("</TD>");
        } else if (PatSchedAddl.equals(setting.getObjSubType())) {
            sb1.append("<TD style='padding: 0px 8px 0px 0px'");
            if ("0".equals(setting.getObjVisible())) {
                sb1.append(" style='display:none' ");
            }
            sb1.append(">");
            sb1.append(dataMap.get(PatSchedAddl));
            sb1.append("</TD>");
        }
    }
    dataMap.clear();
    return sb1.toString();
}
%>


  <%
   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))
   {

    int fkVisit = EJBUtil.stringToNum(request.getParameter("visitId"));
    if (fkVisit == 0) { fkVisit = -1; }
   
    String defUserGroup = (String) tSession.getAttribute("defUserGroup");

	int formLibSeq = 0;
	char formLibAppRight = '0';
	//put the values in session
	String study = request.getParameter("studyId");
	if(study == null || study.equals("null"))
	{
		study = (String) tSession.getValue("studyId");
	}
	else
	{
		tSession.setAttribute("studyId",study);
	}

	String enrollId = "";
	String statid = "";

	// Get coverage type legend
	SchCodeDao scd = new SchCodeDao();
	scd.getCodeValues("coverage_type");
	ArrayList covSubTypeList = scd.getCSubType();
	ArrayList covDescList = scd.getCDesc();
	ArrayList<String> covCodeHide = scd.getCodeHide();
	StringBuffer legendSB = new StringBuffer();
	for (int iX=0; iX<covSubTypeList.size(); iX++) {
	    String covSub1 = ((String)covSubTypeList.get(iX)).replaceAll("'", "\\\\'");
	    String covDesc1 = ((String)covDescList.get(iX)).replaceAll("'", "\\\\'");
	    String covIsHidden = covCodeHide.get(iX);
	    if(StringUtil.isEmpty(covIsHidden))covIsHidden="N";
	    if (covSub1 == null || covDesc1 == null) { continue; }
		if(covIsHidden.equalsIgnoreCase("Y")){
			legendSB.append("<b>").append(covSub1).append("</b>").append("=").append(covDesc1).append(" ["+LC.L_Hidden+"]").append("<br/>");
		}else{
			legendSB.append("<b>").append(covSub1).append("</b>").append("=").append(covDesc1).append("<br/>");
		}
	}
	String covTypeLegend = legendSB.toString();

	String calStatus= request.getParameter("calStatus"); //JM: 05May2008, added for, Enh. #PS16
	calStatus = (StringUtil.isEmpty(calStatus))? "A":calStatus;

	String usr = (String) tSession.getValue("userId");
	int accountId = EJBUtil.stringToNum((String)tSession.getValue("accountId"));
    int pageRight = 0;
 	int orgRight = 0;
 	int patStudyStatpk = 0;

 	ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
 	ArrayList patSchedColList = objCache.getAccountObjects(accountId,"pat_sched_col");

	//******************GET STUDY TEAM RIGHTS and put the same in session, this will be a separate obj as no study is selected //***************************************************************
    TeamDao teamDao = new TeamDao();
    teamDao.getTeamRights(EJBUtil.stringToNum(study),EJBUtil.stringToNum(usr));
    ArrayList tId = teamDao.getTeamIds();
    int patdetright = 0;
    if (tId.size() == 0)
	{
    	pageRight=0 ;
    }
	else
	{
    	stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));
    	 	ArrayList teamRights ;
						teamRights  = new ArrayList();
						teamRights = teamDao.getTeamRights();

						stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
						stdRights.loadStudyRights();

    	tSession.setAttribute("studyRights",stdRights);
    	if ((stdRights.getFtrRights().size()) == 0)
		{
    	 	pageRight= 0;
    		patdetright = 0;
    	}
		else
		{
    		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYMPAT"));
    		patdetright = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYVPDET"));
    		tSession.setAttribute("studyManagePat",new Integer(pageRight));
    		tSession.setAttribute("studyViewPatDet",new Integer(patdetright));
    	}
    }

	//boolean genSchedule = true;
	SchCodeDao cd = new SchCodeDao();
	int stTempPk;
	String evParmStatus = request.getParameter("dstatus");
	int tempStat = EJBUtil.stringToNum(evParmStatus);

	cd.getCodeValues("eventstatus",0);
	cd.setCType("eventstatus");
	cd.setForGroup(defUserGroup);
	cd.getHiddenCodelstDataForUser();

	String hideCode = "";
	String modRight = (String) tSession.getValue("modRight");
	int modlen = modRight.length();

	ctrl.getControlValues("module");
	int ctrlrows = ctrl.getCRows();

	ArrayList feature =  ctrl.getCValue();
	ArrayList ftrDesc = ctrl.getCDesc();
	ArrayList ftrSeq = ctrl.getCSeq();
	ArrayList ftrRight = new ArrayList();
	ArrayList crfNamesArr = new ArrayList();

	String strR;
	ArrayList ctrlValue = new ArrayList();

	for (int counter = 0; counter <= (modlen - 1);counter ++)
	{
		strR = String.valueOf(modRight.charAt(counter));
		ftrRight.add(strR);
	}

	grpRights.setGrSeq(ftrSeq);
        grpRights.setFtrRights(ftrRight);
	grpRights.setGrValue(feature);
	grpRights.setGrDesc(ftrDesc);
	int eptRight = 0;
	eptRight  = Integer.parseInt(grpRights.getFtrRightsByValue("MODEPT"));
	/*YK14Sep2011 : Fixed Bug#6979 */
	String sessionInventoryRight  ="";
	sessionInventoryRight  = (String) tSession.getAttribute("sessionInventoryRight");
	int storageRight = 0;
	if (StringUtil.isEmpty(sessionInventoryRight))
	{
		sessionInventoryRight= "0";
	} 	 
	/*YK 14Sep2011 : Fixed for Bug#6979*/
	if ("1".equals(sessionInventoryRight)){
	GrpRightsJB storageGrpRights = (GrpRightsJB) 	tSession.getAttribute("GRights");
	storageRight =  Integer.parseInt(storageGrpRights.getFtrRightsByValue("MSTORAGE"));
	}

	int personPK = 0;
	int siteId = 0;
	String patientId = "";

	personPK = EJBUtil.stringToNum(request.getParameter("pkey"));
	String pkey = request.getParameter("pkey");


    person.setPersonPKId(personPK);
	person.getPersonDetails();
	patientId = person.getPersonPId();

	siteId = EJBUtil.stringToNum(person.getPersonLocation());
	String userIdFromSession = (String) tSession.getValue("userId");

	orgRight = userSiteB.getMaxRightForStudyPatient(EJBUtil.stringToNum(userIdFromSession), personPK , EJBUtil.stringToNum(study) );
	if (orgRight > 0)
	{
		orgRight = 7;
	}

	if ((pageRight >= 4) && (orgRight >= 4))
	{

	String statDesc=request.getParameter("statDesc");
	String studyVer=request.getParameter("studyVer");
	if ( studyVer == null ) studyVer = "";
	String studyTitle = "";
	String studyNumber = "";
	studyB.setId(EJBUtil.stringToNum(study));
    studyB.getStudyDetails();
    studyTitle = studyB.getStudyTitle();
	studyNumber = studyB.getStudyNumber();
	String uName = (String) tSession.getValue("userName");
	String ipAdd = (String) tSession.getValue("ipAdd");
	int totalDefinedVisits = 0;

//	if ((pageRight >= 4) && (orgRight >= 4))
//	{

		ILabService labService = labServiceFactory.getService();
		boolean isLabServicePresent = (labService == null) ? false : true;

		String evDesc = "";
		String subType ="";
		String protocolId="";
		String startdt="";
		String status="";//G-Generate Schedule,S-Show Schedule also
		String schDate = "";
		String acDate = "";
		String scheduleStatus = "";

		String crfHrf="";
		StringBuffer sbAvailableSchedulesDropDown = new StringBuffer();
		ScheduleDao schDao = new ScheduleDao();
		int schedulesCount = 0;
		String availSchedulePatProtId = "";
		String selectedString = "";

		String selectedEnrollId = "";

		selectedEnrollId = request.getParameter("availableSch");

		patEnrollB.findCurrentPatProtDetails(EJBUtil.stringToNum(study),EJBUtil.stringToNum(pkey));

		enrollId = String.valueOf(patEnrollB.getPatProtId());

		if (enrollId.equals("0"))
		{
			enrollId = "";
		}

		tSession.setAttribute("enrollId",enrollId);

		schDao = patEnrollB.getAvailableSchedules(pkey,  study);

		if (StringUtil.isEmpty(selectedEnrollId))
		{
			selectedEnrollId = enrollId;//where enrollID is the current schedule
		}

		String selDay = "";

		if (selectedEnrollId != enrollId)
		{
			patEnrollB.setPatProtId(EJBUtil.stringToNum(selectedEnrollId)); //get patprot details for old patprot
			patEnrollB.getPatProtDetails();
		}

		startdt = patEnrollB.getPatProtStartDt();
		protocolId =patEnrollB.getPatProtProtocolId();
		selDay =  patEnrollB.getPatProtStartDay();
		scheduleStatus = patEnrollB.getPatProtStat();

		if (selDay == null || selDay.trim().equals(""))
		{
			selDay = "0";
		}

			 //FOR HANDLING PAGINATION
			String pagenum = "1";
			int curPage = 0;
			long startPage = 1;
			String stPage;
			long cntr = 0;
			curPage = EJBUtil.stringToNum(pagenum);
			String orderBy = "";
			orderBy = request.getParameter("orderBy");
			String orderType = "";
			orderType = request.getParameter("orderType");
		 	if (orderType == null)
			{
				orderType = "";
			}

	 		String scheduleSql = "";
			String countSql = "";
		    long rowsPerPage=0;
			long totalPages=0;
			long rowsReturned = 0;
			long showPages = 0;

			boolean hasMore = false;
			boolean hasPrevious = false;
			long firstRec = 0;
			long lastRec = 0;
			long totalRows = 0;

			rowsPerPage = Integer.MAX_VALUE;   //used to be Configuration.MOREBROWSERROWS ;
			totalPages = Integer.MAX_VALUE;    //used to be Configuration.PAGEPERBROWSER ;

			int evCRFCount = 0;

			ArrayList crfIds = new ArrayList();
			ArrayList crfNames = new ArrayList();
			ArrayList crfformEntryChars = new ArrayList();
			ArrayList crfLinkedFormIds = new ArrayList();
			ArrayList crfformSavedCounts = new ArrayList();

			ArrayList crfFormTypes = new ArrayList();
			ArrayList crfDisplayInSpecs = new ArrayList();

			Integer crfId = null;
			String crfName = "";
			String crfFormEntryChar = "";
			String crfformSavedCount = "";
			String crfLinkedForm = "";
			String crfString = "";
			String popCrfString = "";
			String protName= "";
			String protDur = "";
			Date st_date1 = null;
			Integer prot_id = null;
			String displayInSpec = "";

			String crfType = "";

		 if(protocolId != null && (!EJBUtil.isEmpty(selectedEnrollId)))
		 {
			eventAssocB.setEvent_id(EJBUtil.stringToNum(protocolId));
			eventAssocB.getEventAssocDetails();
			protName= 	eventAssocB.getName();
			protDur = 	eventAssocB.getDuration();
			prot_id = new Integer(protocolId);
		}
		else
		{
			protName = "No Associated Calendar";
		}

			String patStudyStat = "";
			String patStatSubType = "";

			CodeDao cdStat = new CodeDao();
			patB.getPatStudyCurrentStatus(study, pkey);
			patStudyStatpk = patB.getId();
			patStudyStat = patB.getPatStudyStat();
			statid = patStudyStat ;
			cdStat.getCodeValuesById(EJBUtil.stringToNum(patStudyStat));

			ArrayList arSubType = new ArrayList();
			arSubType = cdStat.getCSubType();

			if (arSubType.size() > 0 )
			{
					patStatSubType = (String) arSubType.get(0) ;
			}

	if ((pageRight >= 4) && (orgRight >= 4))
	{

if(protocolId != null && (!EJBUtil.isEmpty(selectedEnrollId)))
{

			//Modified by Manimaran for November Enhancement PS4.
			if((patStatSubType.trim().equalsIgnoreCase("lockdown")))
			{ %>
				<P class = "defComments"><FONT class="Mandatory"><%=MC.M_PatStdLkdown_CntChgStat%><%--Patient's <%=LC.Std_Study_Lower%> status is 'Lockdown'. You cannot change Event status.*****--%> </FONT>
				<A onclick="openWinStatus('<%=pkey%>','<%=patStudyStatpk%>','<%=pageRight%>','<%=study%>','<%=orgRight%>')"  href="#"  ><%=LC.L_Click_Here%><%--Click Here*****--%></A>
				<FONT class="Mandatory"> <%=MC.M_ToChg_TheStat%><%--to change the status.*****--%></FONT>
				</P>

		<%	}
			String event_id="";
			String chain_id="";
			String event_type="";
			String name="";
			String eventDisplayName = "";
			String eventDisplayNameTrim ="";
			String notes="";
			String eventNotes="";
			String cost="";
			String cost_description="";
			String duration="";
			String user_id="";
			String linked_uri="";
			String fuzzy_period="";
			String msg_to="";
			String sts="";
			String description="";
			String displacement="";
			String org_id="";
			String fuzzy_dates = "";
			StringBuffer sbFilterWhereClause = new StringBuffer();
			String scheduleWhereForCRF = "";
			Hashtable htSchedule = new Hashtable();
			Hashtable htCRF = new Hashtable();
			Hashtable htCRFVisit = new Hashtable();

//JM: blocked for consistency in P and S

			if (! EJBUtil.isEmpty(startdt))
			{
				startdt = startdt.toString().substring(6,10) + "-" + startdt.toString().substring(0,2) + "-" + startdt.toString().substring(3,5);
			}

			    String visit = request.getParameter("visit");
				String  month = request.getParameter("month");
				String year = request.getParameter("year");
				String dateRange = request.getParameter("date_range");
				String event = request.getParameter("event");
				String evtStatus = request.getParameter("dstatus");
				String visitxt = request.getParameter("visitext");
				String evttxt = request.getParameter("eventext");
				String ststxt ="All";
				String durtxt = request.getParameter("monthyrtext");
				ststxt = request.getParameter("statustext");

				if ( 	visitxt == null )  visitxt = "ALL";
				if ( 	durtxt == null ) durtxt = "ALL";
				if ( 	evttxt == null ) evttxt = "ALL" ;
				if ( 	ststxt == null )	ststxt = "ALL" ;
				if ( visit == null ) visit  = "0" ;
				if ( year == null )  year  = "0" ;
				if ( month == null )  month  = "0" ;
				if ( event == null )  event  = "All" ;
				if ( event == "" )  event  = "All" ;
				if ( evtStatus == null )  evtStatus   = "" ;
				event = event.trim();
				if ( dateRange == null) dateRange = "ALL";
				dateRange = StringUtil.htmlEncodeXss(dateRange.trim());

		 	com.velos.esch.business.common.EventdefDao eventdao = new com.velos.esch.business.common.EventdefDao();
	 		eventdao = eventdefB.getScheduleEventsVisits(EJBUtil.stringToNum(selectedEnrollId));
			totalDefinedVisits = eventdao.getTotalDefinedVisitsInSchedule(EJBUtil.stringToNum(selectedEnrollId));

			java.util.ArrayList visits = eventdao.getVisit();
			java.util.ArrayList visitnames = eventdao.getVisitName();

			String visitName = "";

			//get event names from eventdao - sonia
			java.util.ArrayList eventDescs = eventdao.getNames();
			//Virendra: Fixed Bug No.#4951, Removed duplicate entries
			HashSet hashSet = new HashSet(eventDescs);
			eventDescs = new ArrayList(hashSet) ;
	 		Collections.sort(eventDescs);

				if (totalDefinedVisits > 0) {

				 	scheduleSql=   "select ev.DESCRIPTION, to_char(START_DATE_TIME, 'yyyy-mm-dd') START_DATE_TIME, "
						+ " nvl(evass.FUZZY_PERIOD,0) FUZZY_PERIOD ,"
						+ " evass.EVENT_DURATIONBEFORE,evass.EVENT_FUZZYAFTER,evass.EVENT_DURATIONAFTER,"
						+ " ev.EVENT_ID, "
						+ " ev.ISCONFIRMED, "
						+ " ev.FK_ASSOC, "
						+ " ev.NOTES, "
						+ "	 to_char(ev.EVENT_EXEON, PKG_DATEUTIL.F_GET_DATEFORMAT) as EVENT_EXEON, "
						+ "	to_char(ev.ACTUAL_SCHDATE, 'yyyy-mm-dd')  ACTUAL_SCHDATE, "
						+ "	ev.EVENT_EXEBY, "
						+ "	ev.ADVERSE_COUNT, "
						+ " ev.visit, "
						+ " vis.visit_name,ev.fk_visit, "
						+ " ev.status,ev.event_notes, (select count(*) from sch_event_kit where fk_event = ev.fk_assoc) kitCount ,pkg_gensch.f_get_event_roles(ev.fk_assoc) event_roles, vis.no_interval_flag interval_flag,  "
						+ " ev.SERVICE_SITE_ID,ev.FACILITY_ID,ev.FK_CODELST_COVERTYPE, ev.REASON_FOR_COVERAGECHANGE "
						+ " FROM SCH_EVENTS1 ev"
						+ " , SCH_PROTOCOL_VISIT vis"
						+ " , EVENT_ASSOC evass"
						+ " WHERE  ev.fk_patprot = " + selectedEnrollId
						+ " and ev.fk_visit = " + fkVisit
						+ " and ev.fk_visit = vis.pk_protocol_visit "
						+ " and evass.EVENT_ID = ev.FK_ASSOC ";

		}

			sbFilterWhereClause.append(" and ev.fk_visit = " + fkVisit);
			if (!(visit.equals("0"))){
				sbFilterWhereClause.append(" and ev.visit = " + visit);
			}
			if (!(year.equals("0"))){
				sbFilterWhereClause.append(" and to_Char(ev.ACTUAL_SCHDATE,'yyyy') = " + year) ;
			}
			if (!(month.equals("0"))){
				sbFilterWhereClause.append(" and to_Char(ev.ACTUAL_SCHDATE,'mm') = " + month);
			}
			if ( !( event.equals("All")  )               )
			{
			   sbFilterWhereClause.append(" and UPPER(trim(ev.DESCRIPTION)) = UPPER(trim('" + event +"'))");

			}
			if (!(evtStatus.equals(""))){
				sbFilterWhereClause.append(" and ev.ISCONFIRMED = '" +evtStatus +"'");
			}
			if (!(dateRange.equals("ALL"))) {
				sbFilterWhereClause.append(" and ( ").append(com.velos.eres.service.util.EJBUtil.getRelativeTimeLong(
					"(select ACTUAL_SCHDATE from dual)", Calendar.getInstance(), dateRange));
				sbFilterWhereClause.append(" ) ");
			}

			scheduleSql = scheduleSql + sbFilterWhereClause.toString() ;

//  			scheduleSql += " ORDER BY fk_visit,ACTUAL_SCHDATE, START_DATE_TIME, EVENT_ID ";
  			//JM: 18Apr2008: blocked and modified
  			//scheduleSql += " ORDER BY ev.ACTUAL_SCHDATE, ev.fk_visit, ev.EVENT_ID ";
  			scheduleSql += " ORDER BY ev.ACTUAL_SCHDATE, ev.event_sequence ";

			//removing the check for status = 0, since now we are allowing multiple schedule management
			//JM: 18Apr2008: blocked and modified
			//scheduleWhereForCRF = 	" Where ev.fk_patprot = " + selectedEnrollId	+  sbFilterWhereClause.toString() + " ORDER BY ev.ACTUAL_SCHDATE, ev.START_DATE_TIME, ev.EVENT_ID ";
			scheduleWhereForCRF = 	" Where ev.fk_patprot = " + selectedEnrollId	+  sbFilterWhereClause.toString() + " ORDER BY ev.ACTUAL_SCHDATE, ev.event_sequence ";

			//KM: 12Nov09-- Modified for pagination
			countSql = " select count(*) from  ( "+ scheduleSql + ")";

			htSchedule  = bRows.getSchedulePageRows(curPage,rowsPerPage,scheduleSql,totalPages,countSql,orderBy,"null",scheduleWhereForCRF);
			BrowserRows br = (BrowserRows) htSchedule.get("schedule");
			htCRF = (Hashtable) htSchedule.get("eventcrf"); //contains CRF information
			htCRFVisit = (Hashtable) htSchedule.get("visitcrf"); //contains VISIT CRF information

			rowsReturned = br.getRowReturned();
			showPages = br.getShowPages();
			startPage = br.getStartPage();
			hasMore = br.getHasMore();
			hasPrevious = br.getHasPrevious();
			totalRows = br.getTotalRows();
			firstRec = br.getFirstRec();
			lastRec = br.getLastRec();

			String prevMonth="";
			String thisMonth="";
			Integer newVisit ;
			Integer prevVisit = null ;
			int prevYr = 0;

			int visitCnt = 0;
			String start_dt="";
			String prev_dt="00/00/0000";
			int crfCount = 0;
			Calendar cal2 = Calendar.getInstance();
			int currYear = cal2.get(cal2.YEAR);
			if (rowsReturned> 0)
			{  %>
<TABLE border=0 width="100%">
<%  if ( !(patStatSubType.trim().equalsIgnoreCase("lockdown") ) && eptRight == 1) {
	    protVisitB.setVisit_id(fkVisit);
	    protVisitB.getProtVisitDetails();
	    String selEvtUrl = "selecteventus.jsp?mode=M&patId="+personPK+"&patProtId="+selectedEnrollId+"&calledFromPage=patientEnroll&visitId="+fkVisit+"&displacement="+EJBUtil.stringToNum(protVisitB.getDisplacement())+"&duration="+protDur+"&protocolId="+protocolId+"&calledFrom=S&calStatus=A&cost=-1&calltime=New&calassoc=P&catId=All&catName=All";
%>
  <TR height="7">
    <TD colspan=8 align=right>
<%	//if a remote lab service plugin exists, add the link for lab details
        if (isLabServicePresent){
%>
      <A href="#" onClick="return viewlabdetails('viewLabDetails.jsp?patientId=<%=pkey%>&patProtId=<%=selectedEnrollId%>&visitId=<%=fkVisit%>&calledFrom=S')"><%=LC.L_ViewLab_Dets%><%--View Lab Details*****--%></a>&nbsp;
<%      }  %>
	  <A href="javascript:void(0);" onclick="return fnOpenEditVisitWindow(document.sch, '<%=selEvtUrl%>','<%=protVisitB.getName()%>','<%=protVisitB.getVisit_id()%>','<%=pageRight%>','<%=orgRight%>','<%=calStatus%>')"><%=LC.L_Edit_Visit%><%--EDIT VISIT*****--%></A>&nbsp;
      <A href="javascript:void(0);" onclick="return fnOpenEventsWindow(document.sch, '<%=selEvtUrl%>'+selectedVisit, '<%=protVisitB.getName()%>', '<%=pageRight%>','<%=orgRight%>','<%=calStatus%>')"><%=LC.L_AddUnsch_Evt%><%--ADD UNSCHEDULED EVENT*****--%></A>
    </TD>
  </TR>
  <TR><td><spacer height="4"></spacer></td></TR>
</TABLE>
<%} %>

<div id="scrollTableContainer<%=fkVisit%>" class="TableContainer" style="overflow: auto; height : auto; max-height: 300px; width : 100%;">
<TABLE name="scrollTableBody<%=fkVisit%>" id="scrollContent<%=fkVisit%>" <%-- id="scrollTab" --%> class="scrollTable" border="1" CELLSPACING="1" 
style="border-collapse:separate; background:969696;">
  <!-- <thead class="fixedHeader headerFormat" style="background:5c5c5c; "> -->
  <TR>
    <TH ><%=LC.L_Suggested_Date%><%--Suggested Date*****--%></TH>
<%
			visit ="";
			if(eptRight==1)
			{
%>
				<TH ><%=LC.L_Scheduled_Date%><%--Scheduled Date*****--%></TH>
<%
			}
%>
			 <TH ><%=LC.L_Event_Window%><%--Event Window*****--%></TH>
			 <TH><%=LC.L_Event%><%--Event*****--%></TH>
	    	 <TH ><%=LC.L_Event_Status%><%--Event Status*****--%></TH>
<%
			HashMap configHeaderMap = new HashMap();
	    	configHeaderMap.put(EptRight, String.valueOf(eptRight));
			if(eptRight==1)
			{
				ctrl1.getControlValues("crfmodule");
				ctrlValue = ctrl1.getCValue();
				acmod.getControlValues("module");
				ArrayList acmodfeature =  acmod.getCValue();
				ArrayList acmodftrSeq = acmod.getCSeq();
				formLibSeq = acmodfeature.indexOf("MODFORMS");
				formLibSeq = ((Integer) acmodftrSeq.get(formLibSeq)).intValue();
				formLibAppRight = modRight.charAt(formLibSeq - 1);
				configHeaderMap.put(CtrlValue, ctrlValue.get(0));
				if(ctrlValue.get(0).equals("1"))
				{
					configHeaderMap.put(FormLibAppRight, String.valueOf(formLibAppRight));
				}
			}
			configHeaderMap.put(CovTypeLegend, covTypeLegend);
	    	out.println(printConfigurableColHeader(patSchedColList, configHeaderMap));
%>
  </TR>
  <!--</thead>-->
	<%} %>
   <!-- <tbody name="scrollTableBody<%=fkVisit%>" id="scrollContent<%=fkVisit%>" class="scrollContent bodyFormat" style="overflow: auto;"> -->
	<%
	String st_exon = null;
	Calendar todayCal = Calendar.getInstance();
	boolean pastDate = false;
	String prevEventId = "";
	String hidden_notes = null;
	int rowCount =0;

	int eventSOSId =0;
	String siteOfService = "";

	int eventCoverType =0;
	String coverType = "";
	String reasonChange = "";
	String evt1Notes = null;
	
	String exe_by =  ""  ;
	String newVisitName="";
	Calendar cal ;
	java.sql.Date st_date ;
	int kitCount  = 0;
	String eventRoles = "";
	String acDateFlag = "";
	int year1 = 0;
	
	// ArrayLists used in Edit Visit dialog
	ArrayList editEventIDList = new ArrayList();
	ArrayList editEventNameList = new ArrayList();
	ArrayList editIsConfirmedList = new ArrayList();
	ArrayList editEventStatusDateList = new ArrayList();
	ArrayList editCoverageTypeList = new ArrayList();
	ArrayList editCoverageTypeHiddenList = new ArrayList();
	ArrayList editReasonForChangeList = new ArrayList();
	ArrayList editEvt1NotesList = new ArrayList();
	ArrayList editServiceSiteList = new ArrayList();
	SchCodeDao schCodeDaoForEdit = new SchCodeDao();
	schCodeDaoForEdit.getCodeValues(CoverageTypeForCodeList);
	
	for(int i=1; i<=rowsReturned ; i++)
	{

		 cal = Calendar.getInstance();
		 st_date =  java.sql.Date.valueOf(  br.getBValues(i,"START_DATE_TIME")  ) ;	          // (java.sql.Date) start_date_times.get(i);

		java.sql.Date actualDate = null;
		if( br.getBValues(i,"ACTUAL_SCHDATE") != null  )
			actualDate =  java.sql.Date.valueOf( br.getBValues(i,"ACTUAL_SCHDATE"));

		if(actualDate == null) {
			acDateFlag ="1";
			actualDate = st_date;
		}

		newVisit = Integer.valueOf((String)br.getBValues(i,"fk_VISIT") );	   ///  (java.sql.Date)actualDates.get(i); visit
		newVisitName = 	 br.getBValues(i,"VISIT_NAME"); //SV, 10/28/04, display name instead of visit #.
		newVisitName=(newVisitName==null)?"":newVisitName;
		////////////////////////////////////////changeds
		//java.sql.Date exe_on =java.sql.Date.valueOf( br.getBValues(i,"ACTUAL_SCHDATE")   );
		java.sql.Date exe_on =       null              ;  //  java.sql.Date.valueOf(br.getBValues(i,"EVENT_EXEON")  );

		String tmpExeOn = br.getBValues(i,"EVENT_EXEON") ;
		if (  tmpExeOn == null )
		tmpExeOn = "" ;
		
		String editEvtStatDate = null;
		if (tmpExeOn.matches(".*[a-zA-Z]+.*")) {
		    editEvtStatDate = DateUtil.dateToString(DateUtil.stringToDate(tmpExeOn));
		} else {
		    editEvtStatDate = tmpExeOn;
		}
		editEventStatusDateList.add(editEvtStatDate);

		exe_by =  br.getBValues(i,"EVENT_EXEBY");
		notes  =br.getBValues(i,"NOTES");
		evt1Notes = StringUtil.trueValue(br.getBValues(i,"NOTES"));
		/* Added by Sudhir Shukla for Bug #14885 on 15-Mar-2013 */
		editEvt1NotesList.add(evt1Notes.replaceAll("\\$","&#36;").replaceAll("<","&lt;").replaceAll(">","&gt;").replaceAll("\r",""));

		eventNotes=br.getBValues(i,"EVENT_NOTES");
		eventNotes=(eventNotes==null)?"":eventNotes;

		if(!acDateFlag.equals("1")) {
			cal.setTime(actualDate);
			pastDate = DateUtil.compareCalendar(todayCal,cal,">");

			int month1 = cal.get(cal.MONTH);
			 year1 = cal.get(cal.YEAR);
		} else {
		    pastDate = false;
		}

		java.sql.Date temp_date = (java.sql.Date)st_date.clone();
		java.sql.Date temp_date1 = (java.sql.Date)st_date.clone();
		java.sql.Date temp_date2 = (java.sql.Date)st_date.clone();
		String fuzzyPeriod = "";
		String fuzzyPeriodBefore = "";

		int day = 0;

		String currentEventStatId = "";

		name  = br.getBValues(i,"DESCRIPTION");
		eventDisplayName = br.getBValues(i,"DESCRIPTION");

		name= URLEncoder.encode(name, "UTF-8");
		sts  =   br.getBValues(i,"ISCONFIRMED") ;     //  (String)stat.get(i);
		editIsConfirmedList.add(sts);

		currentEventStatId  = br.getBValues(i,"ISCONFIRMED") ;

		Integer temp = Integer.valueOf(br.getBValues(i,"FK_ASSOC") ) ;

		kitCount = (Integer.valueOf(br.getBValues(i,"kitCount") )).intValue() ;
		eventRoles = br.getBValues(i,"event_roles") ;

		Integer ev_id= Integer.valueOf(  br.getBValues(i,"EVENT_ID")   );
		editEventIDList.add(ev_id);
		prevEventId = event_id;
		event_id = ev_id.toString();
		org_id = temp.toString();
		start_dt = actualDate.toString();

		eventSOSId = EJBUtil.stringToNum(br.getBValues(i,"SERVICE_SITE_ID"));
		editServiceSiteList.add(StringUtil.trueValue(br.getBValues(i,"SERVICE_SITE_ID")));

		if (eventSOSId != 0){
			siteB.setSiteId(eventSOSId);
			siteB.getSiteDetails();

			siteOfService = siteB.getSiteName();
		}else{
			siteOfService ="-";
		}

		eventCoverType = EJBUtil.stringToNum(br.getBValues(i,"FK_CODELST_COVERTYPE"));
		editCoverageTypeHiddenList.add(new Integer(eventCoverType));
		editCoverageTypeList.add(schCodeDaoForEdit.toPullDownWithShortDescription("covType"+(i-1), eventCoverType, "onChange='enableReasonOnChange("+(i-1)+");'"));
		if (eventCoverType != 0){
			SchCodeDao cdCoverType = new SchCodeDao();
			coverType=cdCoverType.getCodeSubtype(eventCoverType);
		}else{
			coverType="-";
		}

		reasonChange = br.getBValues(i,"REASON_FOR_COVERAGECHANGE");
		reasonChange = (reasonChange == null) ? "" : reasonChange;
		
		int cntInfoAvailable = 0;
		if (reasonChange.length()>0) { cntInfoAvailable++; }
		if (evt1Notes.length()>0) { cntInfoAvailable++; }
		
		// Build the Addl Info link
		String addlInfo = null;
		StringBuffer addlInfoSB = new StringBuffer();
		if (reasonChange.length() < 1 && evt1Notes.length() < 1) {
		    addlInfoSB.append("-");
		} else {
		    addlInfoSB.append("<a href=\"javascript:void(0);\"><img style='height:15px' src=\"../images/jpg/info.jpg\" border=\"0\" ");
		    addlInfoSB.append(" onmouseover=\"return overlib('[VEL_OVERLIB]',CAPTION,'");
		    addlInfoSB.append(LC.L_Addl_Info).append("');\"");/*addlInfoSB.append(LC.Evt_AddlInfo).append("');\"");*****/
		    addlInfoSB.append(" onmouseout=\"return nd();\"");
		    addlInfoSB.append(" onclick=\"openAddlInfoWindow(").append(cntInfoAvailable);
		    addlInfoSB.append(",").append(event_id).append(")\"");
		    addlInfoSB.append(" ></img></a>");
		    addlInfoSB.append("<div style='display:none' id='addlInfoForEvt").append(event_id);
		    addlInfoSB.append("' title='").append(LC.L_Addl_Info).append("'>[VEL_ADDL_DIV]</div>");/*addlInfoSB.append("' title='").append(LC.Evt_AddlInfo).append("'>[VEL_ADDL_DIV]</div>");*****/
		}
		addlInfo = addlInfoSB.toString();
		
		// Build the contents of reason and notes
		StringBuffer overlibSB = new StringBuffer();
		StringBuffer addlDivSB = new StringBuffer();
		if (reasonChange.length() > 0) {
			/* Added by Sudhir Shukla for Bug #14885 on 15-Mar-2013 */
		    reasonChange = reasonChange.replaceAll("\\$","&#36;").replaceAll("<","&lt;").replaceAll(">","&gt;").replaceAll("\r","");
		    String reasonChangeForOverlib = reasonChange.replaceAll("'","\\\\\\\\'").replaceAll("\"","&quot;")
		        .replaceAll("\n","<br/>").replaceAll("&lt;","&amp;lt;").replaceAll("&gt;","&amp;gt;");
		    overlibSB.append("<b>").append(LC.L_Reason_ForChange).append("</b><br/>");/*overlibSB.append("<b>").append(LC.Reason_For_Change_CT_Short).append("</b><br/>");*****/
		    overlibSB.append(reasonChangeForOverlib);
		    
		    addlDivSB.append("<b>").append(MC.M_ReasonChg_CvgType);/*addlDivSB.append("<b>").append(LC.Reason_For_Change_CT);*****/
		    addlDivSB.append("</b><br/><textarea rows='10' cols='83' readonly>");
		    addlDivSB.append(reasonChange.replaceAll("\\$","&#36;")).append("</textarea>");
		}
	    editReasonForChangeList.add(reasonChange.replaceAll("\\$","&#36;"));
		if (evt1Notes.length() > 0) {
		    evt1Notes = evt1Notes.replaceAll("<","&lt;").replaceAll(">","&gt;").replaceAll("\r","");
		    String evt1NotesForOverlib = evt1Notes.replaceAll("\\$","&#36;").replaceAll("'","\\\\\\\\'").replaceAll("\"","&quot;")
		        .replaceAll("\n","<br/>").replaceAll("&lt;","&amp;lt;").replaceAll("&gt;","&amp;gt;");
		    if (overlibSB.length() > 0) { overlibSB.append("<br/><br/>"); }
		    overlibSB.append("<b>"+LC.L_Notes/*Notes*****/+"</b><br/>");
		    overlibSB.append(evt1NotesForOverlib);
		    
		    if (addlDivSB.length() > 0) { addlDivSB.append("<br/><br/>"); }
		    addlDivSB.append("<b>"+LC.L_Notes/*Notes*****/+"</b><br/><textarea rows='10' cols='83' readonly>");
		    addlDivSB.append(evt1Notes.replaceAll("\\$","&#36;")).append("</textarea>");
		}
		
		// Put the contents of reason and notes in the Addl Info link
		addlInfo = addlInfo.replaceAll("\\[VEL_OVERLIB\\]", overlibSB.toString())
		    .replaceAll("\\[VEL_ADDL_DIV\\]", addlDivSB.toString());
		
		String link="";
		String cLink = "";//JM: 02Jul2008
		evDesc = cd.getDesc(sts);
		subType  = cd.getSubType(sts);

		//get CRF DATA for the event
		if (String.valueOf(formLibAppRight).compareTo("1") == 0){
		if (htCRF.containsKey(ev_id))
		{
			CrfDao evCrf = new  CrfDao();
      		evCrf = (CrfDao) htCRF.get(ev_id);

      		String crfManualString = "";
      		String crfFormsString = "";
      		String crfLinksString = "";

      		//Prepare CRF STRING

      		crfIds = evCrf.getCrfIds();
      		crfNames = evCrf.getCrfNames();
      		crfformEntryChars = evCrf.getCrfformEntryChars();
      		crfLinkedFormIds = evCrf.getCrfLinkedFormIds();
      		crfformSavedCounts = evCrf.getCrfformSavedCounts();
      		evCRFCount = crfIds.size();
      		crfFormTypes = evCrf.getCrfFormType();
      		crfDisplayInSpecs = evCrf.getDisplayInSpecs();

			//System.out.println("evCRFCount " + evCRFCount );

      		crfString = "";

		for (int p = 0; p < evCRFCount; p++)
      		{
      			crfName = "";

      			crfFormEntryChar = "";
      			crfLinkedForm = "";
				crfformSavedCount = "";

      		    crfId = (Integer) crfIds.get(p);
      			crfName = (String) crfNames.get(p);
      			
      			String crfNameShort = crfName;
				if (crfNameShort.length() > 30){
					crfNameShort = crfName.substring(0, 27) + "...";
				}
				String crfNameTitle = StringUtil.htmlDecode(crfName);
      			
      			crfLinkedForm = (String) crfLinkedFormIds.get(p);

      			crfFormEntryChar = (String)	crfformEntryChars.get(p);
      			crfformSavedCount = (String)crfformSavedCounts.get(p);

      			displayInSpec = (String) crfDisplayInSpecs.get(p);

      			if (StringUtil.isEmpty(crfformSavedCount))
      			{
      				crfformSavedCount = "<i>("+MC.M_NoResponseEntered/*No Response Entered*****/+")</i>";
      			}

      			crfLinkedForm = EJBUtil.isEmpty(crfLinkedForm) ? "0" : crfLinkedForm;
      			crfFormEntryChar = EJBUtil.isEmpty(crfFormEntryChar) ? "0" : crfFormEntryChar;

				crfType = (String) crfFormTypes.get(p);

				if (crfLinkedForm.equals("0")) {
				  //do not show edit and new links if crf has been added as text and not as a linkedform
				  if (crfType.equals("MCRF"))
				  {
				   	crfLinksString = crfLinksString + "<BR>" + crfName ;

				  }
				  else //patient links
				  {

 					 if (crfType.equals("adv"))
 					 {
				  		crfName = "<A HREF=\"adveventbrowser.jsp?srcmenu=tdMenuBarItem5&page=patientEnroll&selectedTab=6&pkey="+ personPK +"&visit=1&patProtId="+ selectedEnrollId + "&studyId=" + study +"\" title='"+crfNameTitle+"'>" + crfNameShort + "</A>";
					 }

					  if (crfType.equals("Patstat"))
 					 {
				  		crfName = "<A HREF=\"enrollpatient.jsp?mode=M&srcmenu=tdMenuBarItem5&page=patientEnroll&selectedTab=2&pkey="+ personPK +"&visit=1&patProtId="+ selectedEnrollId + "&patientCode="+ patientId +"&studyId=" + study +"\" title='"+crfNameTitle+"'>" + crfNameShort + "</A>";
					 }

					  if (crfType.equals("demo"))
 					 {
				  		crfName = "<A HREF=\"patientdetails.jsp?mode=M&srcmenu=tdMenuBarItem5&page=patientEnroll&selectedTab=1&pkey="+ personPK +"&studyId=" + study +"\" title='"+crfNameTitle+"'>" + crfNameShort + "</A>";
					 }

					if (crfType.equals("labs"))
 					 {
						crfName = "<A HREF=\"formfilledstdpatbrowser.jsp?formPullDown=lab&fromlab=pat&patStatSubType="+patStatSubType+"&mode=M&srcmenu=tdMenuBarItem5&page=patient&selectedTab=8&calledFrom=S&statDesc=&statid=&patientCode="+ patientId +"&pkey="+ personPK +"&patProtId="+ selectedEnrollId + "&studyId=" + study +"\" title='"+crfNameTitle+"'>" + crfNameShort + "</A>";
					 }
				  	crfLinksString = crfLinksString + "<BR>" + crfName;
				  }

				} else {
//				BK,FIX#5981-MAR-28-2010

						//if ((displayInSpec.equals("1") && kitCount == 0) || displayInSpec.equals("0"))
						//{
							crfHrf = "<A onClick=\" calledFromSchedule = true; fk_sch_events1 = " +event_id + ";   return openlinkedForm("+crfLinkedForm+",'"+crfFormEntryChar+"','"+crfType+"',document.sch)\" href=\"Javascript:void(0)\" title='"+crfNameTitle+"'>"+crfNameShort +"</A> ";
							crfLinksString = crfLinksString + "<BR>" + crfHrf + "<BR>&nbsp;&nbsp;&nbsp;" + crfformSavedCount  ;
						//}
				}

      		}	// end of for loop
				crfString = crfString +  crfLinksString  ;
		}
		else
		{
			crfString = LC.L_No_Crf/*"NO CRF"*****/;
		}

	}//end of if form right module

		if (exe_on!=null)
		{
			st_exon = exe_on.toString().substring(5,7) + "/" + exe_on.toString().substring(8,10) + "/" + exe_on.toString().substring(0,4);
		}

		if(notes!=null)
			{
		           	notes=notes.replace(' ','~');
			}
			else
			{
				notes = "";
			}
//JM:14Feb2008: introduced fk_eventId sch_eventusr.fk_Event
		  //KM:15Sep09-#4267
		  link = "MarkDone.jsp?event_id=" +event_id +"&mode=M&fk_eventId=" + org_id +"&protocol_id=" +prot_id +"&pkey=" +pkey + "&statDesc="+statDesc+ "&statid="+currentEventStatId+"&startdt="+DateUtil.dateToString(st_date)+"&calledFrom=S";
		  sts = evDesc;
		  st_exon = null;

		 fuzzy_period =     br.getBValues(i,"FUZZY_PERIOD");
		 if (StringUtil.isEmpty(fuzzy_period)) fuzzy_period="";

		//Added by Gopu to fix the Bugzilla issue #2347
		 String fuzzy_period_before_unit = br.getBValues(i,"EVENT_DURATIONBEFORE");
		 if (StringUtil.isEmpty(fuzzy_period_before_unit)) fuzzy_period_before_unit ="";

		 String fuzzy_period_after =br.getBValues(i,"EVENT_FUZZYAFTER");
		 if (StringUtil.isEmpty(fuzzy_period_after)) fuzzy_period_after="";

		 String fuzzy_period_after_unit =br.getBValues(i,"EVENT_DURATIONAFTER");
		 if (StringUtil.isEmpty(fuzzy_period_after_unit)) fuzzy_period_after_unit ="";

		String fuzzyPeriodB = fuzzy_period;
		String fuzzyPeriodA = fuzzy_period_after;

		String fuzzy_result_before = "0";
		String fuzzy_result_after = "0";
		//Added by Gopu to fix the Bugzilla Issues #2584 & #2347 (0 Months Before n Months After)
		String fuzzy_month ="0";


					if(fuzzy_period_before_unit!= null && fuzzy_period_before_unit.equals("H")){
						fuzzy_result_before = "-1";
					}

					if(fuzzy_period_before_unit != null && fuzzy_period_before_unit.equals("D")){
						fuzzy_result_before = "-"+fuzzyPeriodB.trim();
					}

					if(fuzzy_period_before_unit != null && fuzzy_period_before_unit.equals("W")){
						fuzzy_result_before = "-" + ((EJBUtil.stringToNum(fuzzyPeriodB.trim())  * 7));
					}



					if (fuzzy_period_after_unit!= null && fuzzy_period_after_unit.equals("H")){
						fuzzy_result_after ="1";
					}
					if (fuzzy_period_after_unit !=null && fuzzy_period_after_unit.equals("W")){
						fuzzy_result_after = "" +((EJBUtil.stringToNum(fuzzyPeriodA.trim())  * 7));
					}
					if (fuzzy_period_after_unit!= null && fuzzy_period_after_unit.equals("D")){
						fuzzy_result_after = fuzzyPeriodA.trim();
					}

				//Added by Gopu to fix the Bugzilla Issues #2584 & #2347 (0 Months Before n Months After)
			  if(fuzzy_period_before_unit.equals("M") && !fuzzyPeriodB.equals("0")){
					fuzzy_month = "1";
			  }


		fuzzy_dates = "-";
		String fuzzyResultBefore = "";//

			if ((fuzzy_period.trim().equals("0")) && (fuzzy_period_after.trim().equals("0"))){
		 		fuzzy_dates =  "-";
		 	 }			//modified by Gopu to fix the Bugzilla Issues #2584 & #2347 (0 Months Before n Months After)
			else if ((!(fuzzy_period.trim().equals("0"))) || ((!fuzzy_result_after.trim().equals("0"))) || ((!fuzzy_result_before.trim().equals("0")))|| fuzzy_month.equals("1"))
			{

				fuzzy_period = fuzzy_period.trim();
				Integer disp;
				java.util.Calendar Cal = java.util.Calendar.getInstance();

				if((fuzzy_period.substring(0,1)).equals("+"))
				{
					//fuzzy_dates = st_date.toString().substring(5,7) + "/" + st_date.toString().substring(8,10) + "/" + st_date.toString().substring(0,4);
					fuzzy_dates = DateUtil.dateToString(st_date);

					fuzzy_dates += " -- ";
					fuzzyPeriod = fuzzy_period.substring(1,fuzzy_period.length());
					disp = new Integer(fuzzyPeriod);
					day = disp.intValue();


					Cal.setTime(temp_date);
					//new
				if(fuzzy_period_after_unit != null){
					if (fuzzy_period_after_unit.equals("M")){
						Cal.add(java.util.Calendar.MONTH,EJBUtil.stringToNum(fuzzyPeriodA.trim()));
					}else{
						Cal.add(java.util.Calendar.DATE, day);
					}
					// --new
					temp_date1 = new java.sql.Date(Cal.get(java.util.Calendar.YEAR) -  1900,Cal.get(java.util.Calendar.MONTH),Cal.get(java.util.Calendar.DATE));
					fuzzy_dates += DateUtil.dateToString(temp_date1);
				} else {
					fuzzy_dates += "-";
				}
			}
			else if((fuzzy_period.substring(0,1)).equals("-"))
			{

				fuzzyPeriod = fuzzy_period.substring(1,fuzzy_period.length());
				fuzzyPeriodBefore = "-" + fuzzyPeriod;
				disp = new Integer(fuzzyPeriodBefore);
				day = disp.intValue();

				Cal.setTime(temp_date);
				// new
			if(fuzzy_period_before_unit !=null){

				if (fuzzy_period_before_unit.equals("M")){
						Cal.add(java.util.Calendar.MONTH, (new Integer("-"+fuzzyPeriodB.trim()).intValue()) );
				}else{
					Cal.add(java.util.Calendar.DATE, day);
				}
				temp_date1 = new java.sql.Date(Cal.get(java.util.Calendar.YEAR) -  1900,Cal.get(java.util.Calendar.MONTH),Cal.get(java.util.Calendar.DATE));
				fuzzy_dates = DateUtil.dateToString(temp_date1);
				fuzzy_dates += " -- ";
				fuzzy_dates += DateUtil.dateToString(st_date);
			}else{
			fuzzy_dates +="-";
			}
			} else
			{

				fuzzyPeriod = fuzzy_period.substring(0,fuzzy_period.length());
				fuzzyResultBefore = fuzzy_result_before ;

				disp = new Integer(fuzzyResultBefore);//

				day = disp.intValue();

				Cal.setTime(temp_date);
				if (fuzzy_period_before_unit!=null && fuzzy_period_before_unit.equals("M")){

				 //Added by Manimaran to fix the Bug 2586
				 day=EJBUtil.stringToNum(fuzzyPeriodB.trim())*(-30);
				 Cal.add(java.util.Calendar.DATE, day);
				}else{
					Cal.add(java.util.Calendar.DATE, day);
				}
				temp_date1 = new java.sql.Date(Cal.get(java.util.Calendar.YEAR) -  1900,Cal.get(java.util.Calendar.MONTH),Cal.get(java.util.Calendar.DATE));
				fuzzy_dates = DateUtil.dateToString(temp_date1);
				fuzzy_dates += "--";

				//Modified by Gopu to fix the Bugzilla issue #2347
				disp = new Integer(fuzzy_result_after);

				day = disp.intValue();
				Cal.setTime(temp_date);
				if (fuzzy_period_after_unit!=null &&  fuzzy_period_after_unit.equals("M")){

				 //Added by Manimaran to fix the Bug 2586
				 day=EJBUtil.stringToNum(fuzzyPeriodA.trim())*(30);
				 Cal.add(java.util.Calendar.DATE, day);

				}else{
						Cal.add(java.util.Calendar.DATE, day);
				}
				temp_date2 = new java.sql.Date(Cal.get(java.util.Calendar.YEAR) -  1900,Cal.get(java.util.Calendar.MONTH),Cal.get(java.util.Calendar.DATE));
				fuzzy_dates += DateUtil.dateToString(temp_date2);
			}
		}

		schDate = DateUtil.dateToString(st_date);
		acDate = DateUtil.dateToString(actualDate);


		if(eptRight==1)
		{
           	name=name.replace(' ','~');

			if (htCRFVisit.containsKey(newVisit))
			{
				//for sonika, make anobj of arraylist, get the CRF names from that arraylist
				crfCount = ((ArrayList) htCRFVisit.get(newVisit)).size();
				crfNamesArr = (ArrayList) (htCRFVisit.get(newVisit));
				popCrfString = "<UL>";
				for (int k=0;k<crfCount;k++) {
					popCrfString = popCrfString + "<LI>" + crfNamesArr.get(k) + "</LI>";
				}
				popCrfString = popCrfString + "</UL>";

			}
			else
			{
				crfCount = 0;
			}

	           	name=name.replace('~',' ');
		}

		if ((pastDate) && (eptRight==1)&& (subType.equals("ev_notdone")))//if current date is past act date
		{
			sts="Past Scheduled Date" ;
%>
			<TR class="pastSchRow" style="height:2em">
<%
		} else if(rowCount%2==0)
		{
%>
			<TR class="browserOddRow" style="height:2em">
<%
			}else
			{
%>
			<TR class="browserEvenRow" style="height:2em">
<%
			}
%>
			<%
			if( acDateFlag.equals("1"))
			{
				schDate =LC.L_Not_Defined/*"Not Defined"*****/;
			} %>

			<TD style="padding: 0px 0px 0px 4.8px"><%=schDate%></TD>

		<%
		//KM-11Dec09
		if(acDateFlag.equals("1"))
		{
				   acDate ="Null";
		}

		String datelink ="changedate.jsp?prevDate=" +acDate + "&eventId=" +event_id  +"&patProtId=" + 	selectedEnrollId + "&eventId=" +event_id + "&pkey=" + pkey + "&statDesc=" + statDesc+ "&statid=" + statid + "&studyVer=" +
		studyVer+"&patientCode="+StringUtil.encodeString(patientId)+"&org_id="+org_id+"&calledFrom=S&acDateFlag="+acDateFlag+"&protocol_id=" +prot_id  ;
		if(eptRight==1)
		{
			//user cannot change the event status if patient is lockdown
%>				<TD style="padding: 0px 0px 0px 5px">
	<table width="100%" cellpadding="0" cellspacing="0" align="center" border="0">
			<%	if ((pastDate) && (eptRight==1)&& (subType.equals("ev_notdone")))//if current date is past act date
			{%>
				<TR class="pastSchRow">
		<%
				} else
			{
		%>
					<TR>
		<%
			}
		%>
    	<td width="42%" style="border: 0px">
	    <% if(acDateFlag.equals("1"))
				{
				   acDate =LC.L_Not_Defined/*"Not Defined"*****/;
				} %>

				<%=acDate%>
    	</td>
			<td width="1%" style="border: 0px">
				<%
					//Modified by Manimaran for November Enhancement PS4.
					if(!(patStatSubType.trim().equalsIgnoreCase("lockdown")))
					{
				%>
					<A href=# title="<%=LC.L_Change_ActualDate%><%--Change Actual Date*****--%>" onClick='return changedate("<%=datelink%>",<%=pageRight%>,<%=orgRight%>)'><img border="0" title="<%=LC.L_Edit%>" alt="<%=LC.L_Edit%>" src="./images/edit.gif" ><%//=LC.L_C%><%--C*****--%></A>
				<%
					}
				%>
			</td>
    		</tr>
    	</table>
	</TD>
	<%
		}
	%>
			<% if( acDateFlag.equals("1"))
				{
				   fuzzy_dates = "-";
				} %>

			<td style="padding: 0px 0px 0px 9px"><%=fuzzy_dates%></td>

			<%
			     if(eventRoles == null){
			        eventRoles = "";
			     }
			     eventDisplayNameTrim = eventDisplayName;
			     if(eventDisplayNameTrim.length()>50 && eventDisplayNameTrim!=null)    // Bug #-4277  Fixed by Prabhat
			    	 eventDisplayNameTrim = eventDisplayNameTrim.substring(0, 50)+"...";
			     editEventNameList.add(eventDisplayNameTrim);
			 %>
		<td style="white-space:wrap; padding: 0px 0px 0px 13px; word-wrap: break-word;"><A href=# onmouseover="return overlib('<%=eventDisplayName%>',CAPTION,'<%=LC.L_Event_Name%><%--Event Name*****--%>');" onmouseout="return nd();" onClick='return viewevent("viewevent.jsp?eventId=<%=org_id%>&calledFrom=S")'><%=eventDisplayNameTrim%></A>
		<%if (eventNotes.length()>0){%>
		<img style='height:15px' src="../images/jpg/icon_clip.gif"></A>
		<%}%>
		<%if (eventRoles.length()>0){%>
		 	  <br>
		<%=LC.L_Resources%><%--Resources*****--%>:
			  <br>
			  <%=eventRoles%>
			<%}%>
		</td>
	<%
	////Modified by Manimaran for November Enhancement PS4.
	if(!(patStatSubType.trim().equalsIgnoreCase("lockdown")))
	{

%>
	<td style="padding: 0px 0px 0px 16px">
	
        <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0">
			<%	if ((pastDate) && (eptRight==1)&& (subType.equals("ev_notdone")))//if current date is past act date
			{%>
				<TR class="pastSchRow">
				<td width="55%" style="border: 0px">
		<%
				} else
			{
		%>
					<TR>
					<td width="40%" style="border: 0px">
		<%
			}
		%>
	    <A href=# onClick='return fopen("<%=link%>","<%=pageRight%>","<%=orgRight%>")'>  <%=sts%></A>&nbsp;&nbsp;
    	</td>
			<td style="border: 0px">
				<%        	name=name.replace(' ','~');
			newVisitName = StringUtil.encodeString(newVisitName)	;

			//JM: 02Jul2008, #3586
			//KM-4267
			cLink = "MarkDone.jsp?event_id=" +event_id +"&mode=N&fk_eventId=" + org_id +"&protocol_id=" +prot_id +"&pkey=" +pkey + "&statDesc="+statDesc+ "&statid="+currentEventStatId+"&calledFrom=S";
			%>
			<A href=# onClick='return fopen("<%=cLink%>","<%=pageRight%>","<%=orgRight%>")'><img border="0" title="<%=LC.L_Edit%>" alt="<%=LC.L_Edit%>" src="./images/edit.gif" ><%//=LC.L_C%><%--C*****--%></A>&nbsp;&nbsp;
			</td>
			<td  style="border: 0px">
			<% //} else { %>
				&nbsp;
			<% //}  %>

			<A title = "<%=LC.L_Evt_StatusHistory%><%--Event Status History*****--%>" href="eventstatushistory.jsp?selectedTab=3&mode=M&page=patientEnroll&patProtId=<%=selectedEnrollId%>&pkey=<%=personPK%>&visit=<%=newVisit%>&visitName=<%=newVisitName%>&eventId=<%=event_id%>&studyNum=<%=StringUtil.encodeString(studyNumber)%>&studyId=<%=StringUtil.encodeString(study)%>&calledFromPage=patientschedule&org_id=<%=org_id%>&calledFrom=S"><img border="0" title="<%=LC.L_History%>" alt="<%=LC.L_History%>" src="./images/History.gif" ><%//=LC.L_H%><%--H*****--%></A>
			<%

			//KM-14Sep09

				name=name.replace('~',' '); %>
                   <%=tmpExeOn%>&emsp;&emsp;&emsp;
			</td>
    		</tr>
    	</table>           
                   
	</td>

<%
	}else
	{
	out.print("<td style='padding: 0px 0px 0px 16px'> " + "&nbsp;" +sts + "</td>");
	}

	HashMap configDataMap = new HashMap();
	configDataMap.put(EptRight, String.valueOf(eptRight));
	configDataMap.put(StorageRight, String.valueOf(storageRight)); /*YK14Sep2011 : Fixed Bug#6979 */
	if (String.valueOf(formLibAppRight).compareTo("1") == 0){%>
<!--		<TD>&nbsp;</TD> -->

	<%}
		if(eptRight==1)
		{
			if (ctrlValue != null && ctrlValue.get(0) != null) {
			    configDataMap.put(CtrlValue, ctrlValue.get(0));
			}
			configDataMap.put(CrfString, crfString);
			configDataMap.put(KitCount, new Integer(kitCount));
			if (kitCount > 0) {
				configDataMap.put(Org_id, new Integer(org_id));
				configDataMap.put(Event_id, new Integer(event_id));
			}
		}
		configDataMap.put(PatSchedSite, siteOfService);
		configDataMap.put(PatSchedCov, coverType);
		configDataMap.put(PatSchedAddl, addlInfo);
		out.println(printConfigurableColData(patSchedColList, configDataMap));
	%>
		     </TR>
		<% rowCount++;
			prevMonth = thisMonth;
			prev_dt = start_dt;
			prevYr = year1;
			prevVisit = newVisit;
	} //for


		%>
	<!-- </tbody> -->
</TABLE>
</div>
<script>
if ($('scrollContent<%=fkVisit%>').offsetHeight>200) {
    $('scrollTableContainer<%=fkVisit%>').style.height = 230;
    $('scrollContent<%=fkVisit%>').style.height = 180;
}
if (navigator.userAgent.indexOf("MSIE") == -1){ 
	$E.onContentReady(makeMeFixedHeader('scrollTableBody<%=fkVisit%>'));
}
</script>

<%
    // Get event status DD
    SchCodeDao scdForEvtStat = new SchCodeDao();
    scdForEvtStat.getCodeValues("eventstatus", 0);
    scdForEvtStat.setCType("eventstatus");
    scdForEvtStat.setForGroup(defUserGroup);
    ArrayList evtStatList = new ArrayList();

    // Construct evtObjSB to pass into hiddenData
    JSONObject jsObj = new JSONObject();
    JSONArray evtObjArray = new JSONArray();
    for(int iEvent=0; iEvent<rowsReturned; iEvent++) {
        JSONObject evtObj = new JSONObject();
        evtObj.put("eId", (Integer)editEventIDList.get(iEvent));
        evtObj.put("eDate", (String)editEventStatusDateList.get(iEvent));
        evtObjArray.put(evtObj);
        // Next line is not for forming JSON; just using this iEvent loop
        evtStatList.add(scdForEvtStat.toPullDown("eventStatus"+iEvent, 
                EJBUtil.stringToNum((String)editIsConfirmedList.get(iEvent)),""));
    }
    jsObj.put("eventObjects", evtObjArray);


%>
<div id="hiddenData<%=fkVisit%>" style="display:none">
<%=jsObj.toString()%>
</div>
<div id="editVisitData<%=fkVisit%>" style="display:none">
<input type="hidden" name="enrolledId" value="<%=selectedEnrollId%>"></input>
<%--Modified By Tarun Kumar : Bug#10322 --%>
<table width="97%" border="0" cellspacing="0" style="border-style:none; border-width:0">
<%  for(int iEvent=0; iEvent<rowsReturned; iEvent++) {  %>
<tr bgcolor='<%=iEvent%2==0 ? EvenRowRGB : OddRowRGB%>'>
<td width="24%"><input type='checkbox' name='selEvent<%=iEvent%>' onclick='toggleRowEdit(<%=iEvent%>)'/>&nbsp;<b><%=editEventNameList.get(iEvent)%></b>
</td>
<td width="18%"><%=evtStatList.get(iEvent)%>
<input type="hidden" name="eventId<%=iEvent%>" value="<%=editEventIDList.get(iEvent)%>"></input>
<input type="hidden" name="oldStatus<%=iEvent%>" value="<%=editIsConfirmedList.get(iEvent)%>"></input>
<!-- Added by Sudhir Shukla for Bug #14885 on 15-Mar-2013  -->
<input type="hidden" name="notes<%=iEvent%>" value="<%=editEvt1NotesList.get(iEvent).toString().replaceAll("\\$","&#36;")%>"></input>
<input type="hidden" name="serviceSite<%=iEvent%>" value="<%=editServiceSiteList.get(iEvent)%>"></input>
</td>
<td width="15%" align="left"><input name="statusValid<%=iEvent%>" type="text" class="datefield" size=12 readonly value="<%=(String)editEventStatusDateList.get(iEvent) %>" /></td>
<td width="22%" align="left"><%=editCoverageTypeList.get(iEvent)%>
<input type="hidden" name="covTypeFor<%=iEvent%>" value="<%=editCoverageTypeHiddenList.get(iEvent)%>"></input>
</td>
<!-- Added by Sudhir Shukla for Bug #14885 on 15-Mar-20-->
<td width="18%"><textarea name='reason<%=iEvent%>'><%=editReasonForChangeList.get(iEvent).toString().replaceAll("\\$","&#36;")%></textarea></td>
</tr>
<%  } %>
</table>
</div>
<%
 }
 else if (EJBUtil.isEmpty(protocolId ) && ( !EJBUtil.isEmpty(selectedEnrollId)))
 {

%>
<P class="defComments">
	<%=MC.M_PatNotAssign_ToPcolCal%><%--This <%=LC.Pat_Patient_Lower%> has not been assigned to any Protocol Calendar.*****--%>
</P>
<%
  }
    else
    {
    	%>

	  	<P class = "defComments">
			<%=MC.M_NoStdStatAdd_PatAssignPCol%><%--There is no <%=LC.Std_Study_Lower%> status added for this <%=LC.Pat_Patient_Lower%>. Please add a <%=LC.Std_Study_Lower%> status before assigning a Protocol.*****--%>
		</P>

		<%
    }
//////////////////
%>
<%
 }else{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%
}
}
else{
	%>
	  <jsp:include page="accessdenied.jsp" flush="true"/>
	<%
	}
} //end of if session times out
else
{
%>
 <jsp:include page="timeout.html" flush="true"/>
  <%
} //end of else body for page right
%>
