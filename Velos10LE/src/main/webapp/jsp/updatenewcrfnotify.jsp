<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>




<BODY>
  <jsp:useBean id="crfNotifyB" scope="request" class="com.velos.esch.web.crfNotify.CrfNotifyJB"/>
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.MC"%>
  <%

	String src = null;
	src = request.getParameter("srcmenu");
	String eSign = request.getParameter("eSign");
	String addFlag = request.getParameter("addFlag");
	String crfNotifyId = request.getParameter("crfNotifyId");
	String crfId = request.getParameter("crfId");
	if(crfId.equals("0")){
		crfId = null;
	}
	String mode = request.getParameter("mode");
	String page1 = request.getParameter("page");
	String pkey = request.getParameter("pkey");
	String visit = request.getParameter("visit");
	String crfStat = request.getParameter("crfStat");
	String crfNotifyToId = request.getParameter("crfNotifyToId");
	String eventName = request.getParameter("eventName");
	String fromPage = request.getParameter("fromPage");
	String protocolId = request.getParameter("protocolId");

	String patProtId= request.getParameter("patProtId");
	String studyId = request.getParameter("studyId");	
	String statDesc=request.getParameter("statDesc");
	String statid=request.getParameter("statid");	
	String studyVer=request.getParameter("studyVer");	
	String patientCode =request.getParameter("patientCode");	


/*out.println("patProtId"+patProtId);
out.println("studyId "+studyId );
out.println("statDesc"+statDesc);
out.println("statid"+statid); */

	


HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))

   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
   	String oldESign = (String) tSession.getValue("eSign");
	String eventId = (String) tSession.getValue("eventId");

	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
	

	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");
%>
	
<%
	if(mode.equals("N")) {
		crfNotifyB.setCrfNotifyCrfId(crfId);
		crfNotifyB.setCrfNotifyCodelstNotStatId(crfStat);
		crfNotifyB.setCrfNotifyUsersTo(crfNotifyToId);
		crfNotifyB.setCrfNotStudyId(studyId);
		crfNotifyB.setCrfNotProtocolId(protocolId);
		crfNotifyB.setCrfNotPatProtId(patProtId);
				
		
		crfNotifyB.setCreator(usr);
		crfNotifyB.setIpAdd(ipAdd);
		
		crfNotifyB.setCrfNotifyDetails();
		
	}
	
	if(mode.equals("M")) {
		crfNotifyB.setCrfNotifyId(EJBUtil.stringToNum(crfNotifyId));
		crfNotifyB.getCrfNotifyDetails();
		
		crfNotifyB.setCrfNotifyCodelstNotStatId(crfStat);
		crfNotifyB.setCrfNotifyUsersTo(crfNotifyToId);
		crfNotifyB.setCrfNotifyCrfId(crfId);

		crfNotifyB.setModifiedBy(usr);
		crfNotifyB.setIpAdd(ipAdd);
		
		crfNotifyB.updateCrfNotify();
	}		
%>
<%--
src*<%=src%><br>
addFlag*<%=addFlag%><br>
crfNotifyId*<%=crfNotifyId%><br>
crfId*<%=crfId%><br>
mode*<%=mode%><br>
page*<%=page1%><br>
pkey*<%=pkey%><br>
visit*<%=visit%><br>
crfNotifyNotes*<%=crfNotifyNotes%><br>
crfStat*<%=crfStat%><br>
crfNotifyToId*<%=crfNotifyToId%><BR>
fromPage*<%=fromPage%><br>
eventName*<%=eventName%><br>
--%>
<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc %><%-- Data was saved successfully*****--%> </p>
<%
if(fromPage.equals("alertnotify")) {
	if(addFlag != null && addFlag.equals("Add")){
%>
	<META HTTP-EQUIV=Refresh CONTENT="2; URL=crfnotify.jsp?srcmenu=<%=src%>&page=patientEnroll&selectedTab=5&pkey=<%=pkey%>&visit=<%=visit%>&eventName=<%=eventName%>&crfId=<%=crfId%>&mode=N&fromPage=alertnotify&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&patProtId=<%=patProtId%>&studyVer=<%=studyVer%>&patientCode=<%=patientCode%>">
<%
	} else {
%>
	<META HTTP-EQUIV=Refresh CONTENT="2; URL=alertnotify.jsp?srcmenu=<%=src%>&page=patientEnroll&selectedTab=5&eventId=<%=eventId%>&pkey=<%=pkey%>&visit=<%=visit%>&eventname=<%=eventName%>&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&patProtId=<%=patProtId%>&studyVer=<%=studyVer%>&patientCode=<%=patientCode%>&mode=M">
<%
	}
} else if(fromPage.equals("crfnotifybrowser")) {
	if(addFlag != null && addFlag.equals("Add")){
%>
	<META HTTP-EQUIV=Refresh CONTENT="2; URL=crfnotify.jsp?srcmenu=<%=src%>&page=patientEnroll&selectedTab=3&pkey=<%=pkey%>&visit=<%=visit%>&eventName=<%=eventName%>&crfId=<%=crfId%>&mode=N&fromPage=crfnotifybrowser&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&patProtId=<%=patProtId%>&studyVer=<%=studyVer%>&patientCode=<%=patientCode%>">
<%
	} else {
%>
	<META HTTP-EQUIV=Refresh CONTENT="2; URL=crfnotifybrowser.jsp?srcmenu=<%=src%>&page=patientEnroll&selectedTab=3&pkey=<%=pkey%>&visit=<%=visit%>&eventName=<%=eventName%>&crfId=<%=crfId%>&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&patProtId=<%=patProtId%>&studyVer=<%=studyVer%>&patientCode=<%=patientCode%>">
<%
	}
} 
%>	
		
<%
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





