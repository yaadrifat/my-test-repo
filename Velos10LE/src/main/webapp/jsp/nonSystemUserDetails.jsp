<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<jsp:include page="velos_includes.jsp"></jsp:include>
<html>

<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<title><%=MC.M_Non_SysUserDets%><%--Non System User Details*****--%></title>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>

<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="addressUserB" scope="page" class="com.velos.eres.web.address.AddressJB" />
<jsp:useBean id="siteB" scope="page" class="com.velos.eres.web.site.SiteJB" />
<jsp:useBean id="addressSiteB" scope="page" class="com.velos.eres.web.address.AddressJB" />
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="ctrlB" scope="request" class="com.velos.eres.business.common.CtrlDao"/>

<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil"%>
<%@ page import="com.velos.eres.service.util.*" %>
</head>



<SCRIPT Language="javascript">
if (document.getElementById('eSign')) { document.getElementById('eSign').disabled=false; }
if (document.getElementById('submit_btn')) { document.getElementById('submit_btn').disabled=false; }

function  blockSubmit() {
	setTimeout('document.getElementById(\'eSign\').disabled=true', 10);
	setTimeout('document.getElementById(\'submit_btn\').disabled=true', 10);
}

function alertDeactivate(formobj,stat)
	{
		if (stat=='D')
		{
			if (confirm("<%=MC.M_ConfirmUsr_AllStdTeams%>") )/*if (confirm("Would you like to make this User inactive in all <%=LC.Std_Study%> Teams that they are part of?") )*****/
			{
				formobj.deactivateFromTeam.value = 1;
			}
			else
			{
				formobj.deactivateFromTeam.value = 0;
			}
		}
		else
		{
				formobj.deactivateFromTeam.value = 0;
		}
	}


 function  validate(formobj,userSiteId){

	 formobj=document.userdetails
	 checkQuote = "N";
     if (!(validate_col('User F Name',formobj.userFirstName))) return false
     if (!(validate_col('User LastName',formobj.userLastName))) return false


	 if (formobj.pname.value=='null'){
	     if (!(validate_col('Organization Name',formobj.accsites))) return false

	 }

	 if (!(validate_col('e-Sign',formobj.eSign)))return false;
if (formobj.pname.value=='null'){
	 if(formobj.mode.value == 'M' && formobj.accsites.value!=userSiteId && formobj.changeOrgFlg.value=='false')
	 {
	    formobj.changeOrgFlg.value="true";
	    msg=MC.M_ConfirmMulti_RgtReset/*"You are changing the User's Primary Organization. This users multiple facility rights will be reset to the new setting. Do you want to proceed?"*****/;
		if (!(confirm(msg))) {
		 //if the user presses cancel then set the organization to the original value
			formobj.accsites.value = userSiteId;
		}
	  }
	 }


	if(formobj.userEmail.value!="")
	 {
		if(formobj.userEmail.value.search("@") == -1) {
		 alert("<%=MC.M_EtrValid_EmailAddr%>");/*alert("Please enter a valid email address.");*****/
		 formobj.userEmail.focus()
		 return false;
		 }
	 }
	 if(isNaN(formobj.eSign.value) == true) {
		 alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
		}
	 
	 
	 return checkDupeUserAndEmail();
	 
	 
     }
 
 
 
 function checkDupeUserAndEmail(){
		
		var userFname = $j('input[name="userFirstName"]').val();
		var mode = $j('input[name="mode"]').val();
		var userLname = $j('input[name="userLastName"]').val();		
		var email = $j('input[name="userEmail"]').val();
		var userLogin=$j('input[name="userLogin"]').val();
		var mode = $j('input[name="mode"]').val();
		var userType = $j('input[name="userType"]').val();
		var JFlag = $j('input[name="JFlag"]').val();
		
		var countUserMails;
		var countUserNames;
		var userLogins;
		
		var urlParam="userFname="+userFname+"&mode="+mode+"&userLname="+userLname+"&email="+email;
		
		$j.ajax({
			url:'validateUserData.jsp',
			data:urlParam,		
			success:(function (data){
				
				countUserMails=data.countUserMails;
				countUserNames=data.countUserNames;
				}),type: "POST",
			async:false});
		
		
		
		if(mode=='M'){
			countUserNames=countUserNames-1;
			if(countUserNames>0){
				var proceed = window.confirm(M_UsrName_AldyExst);
				if(!proceed){
					return proceed;
				}
			}
		}else if(mode=='N'){
			if(countUserNames>0){
				var proceed = window.confirm(M_UsrName_AldyExst);
				if(!proceed){
					return proceed;
				}
			}
		}
		
		if(mode=='M'){
			countUserMails =countUserMails-1;
			if(countUserMails>0){
				var proceed = window.confirm(M_EmailIdExst_Cont);
				if(!proceed){
					return proceed;
				}
			}
		}else if(mode=='N'){
			if(countUserMails>0){
				var proceed = window.confirm(M_EmailIdExst_Cont);
				if(!proceed){
					return proceed;
				}
			}
		}
	}
 
 
 
 
 
//JM: 22Aug2007: issue #3121
function openWinMoreDetails(modId, mode, modName,pageRight)
	{
		if (mode == 'M'){
			windowname=window.open("moredetails.jsp?modId=" + modId +"&modName="+modName+"&pageRight="+pageRight,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=650,height=550 top=120,left=200 0, ");
			windowname.focus();
		} else if (mode == 'N')	{
			alert("<%=MC.M_SavePage_BeforeDets%>");/*alert("Please save the page first before entering More Details");*****/
			return false;
		}
	}


</SCRIPT>



<% String src;

String pname = null;

pname=request.getParameter("pname");
src= request.getParameter("srcmenu");
%>



<jsp:include page="panel.jsp" flush="true">

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>

<body style ="overflow:auto;">
<br>
<DIV class="formDefault" id = "div1">
  <%

 HttpSession tSession = request.getSession(true);

 String uName = (String) tSession.getValue("userName");
String sessUserId =	(String) tSession.getValue("userId");
 String acc = (String) tSession.getValue("accountId");



 if (sessionmaint.isValidSession(tSession))

	{

	 int accId = EJBUtil.stringToNum(acc);
	 int pageRight = 0;
	 int pageRightMngOrg = 0;

  	 char mngOrgAccRight = '0';
 	 String modRight = (String) tSession.getValue("modRight");
	 ctrlB.getControlValues("module"); //get extra modules
	 ArrayList acmodfeature =  ctrlB.getCValue();
 	 ArrayList acmodftrSeq = ctrlB.getCSeq();
	 int orgSeq = 0;
	 orgSeq = acmodfeature.indexOf("MODORG");
	 orgSeq = ((Integer) acmodftrSeq.get(orgSeq)).intValue();
	 mngOrgAccRight = modRight.charAt(orgSeq - 1);

  	 String mode = request.getParameter("mode");
	 int userId = 0;


	 GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
 	 pageRightMngOrg = Integer.parseInt(grpRights.getFtrRightsByValue("MSITES"));

	 if(mode.equals("N") || (mode.equals("M") && !(request.getParameter("userId").equals(sessUserId)))) {
	 	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MUSERS"));
	 } else {

		pageRight = 7;
	 }
%>
<BR>
<%
	if((pname !=null )&& pname.equals("ulinkBrowser")){
%>
  <P class="sectionHeadings"> <%=MC.M_PersonAcc_MyPro%><%--Personalize Account >> My Profile*****--%></P>
  <jsp:include page="personalizetabs.jsp" flush="true"/>
  <%
	}
	if ((mode.equals("M") && ((pageRight >=6) || (pageRight >=4))) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 )) )

	{



  	if (pname ==null ){
   %>
  <P class="sectionHeadings"> <%=MC.M_MngAcc_NonSysUsrDets%><%--Manage Account >> Non System Users >> Details*****--%></P>
  <% }   %>

<%
	String dJobType = "" ;


    CodeDao cd = new CodeDao();

	String userAddresId = "";

	CodeDao cd1 = new CodeDao();

	CodeDao cd2 = new CodeDao();

	CodeDao cd3 = new CodeDao();

	CodeDao cd4 = new CodeDao();

	String  userLastName="";

	String  userFirstName="";

	String  userMidName="";

	String  userAddPri="";

	String  userAddCity="";

	String  userAddState="";

	String  userAddZip="";

	String  userAddCountry="";

	String  userAddPhone="";

	String  userEmail = "";

	String userSession = "";

	String userSiteId = "";

	String  userJobType="";

	String  userSpl="";

	String  siteName="";

	String  siteAddPri="";

	String dAccSites="";

	String dPrimSpl = "";

	String uStat = "";

	String userHidden = "";//KM


	boolean selfFlag = false;


	//cd2.getAccountSites(accId);
	//KM-Added on 22Aug08
	cd2.getAccountSitesNoHidden(accId);

	cd.getCodeValues("job_type");

	cd3.getCodeValues("prim_sp");


	cd4.getTimeZones();


	if (mode.equals("M"))

	{


	userId = EJBUtil.stringToNum(request.getParameter("userId"));

	userB.setUserId(userId);

	userB.getUserDetails();
	
	if (StringUtil.stringToNum(userB.getUserAccountId()) != accId) {
	%>
		<jsp:include page="accessdenied.jsp" flush="true"/>
		<div class = "myHomebottomPanel">
		<jsp:include page="bottompanel.jsp" flush="true"/>
		</div>
		</div>
		<div class ="mainMenu" id = "emenu">
		<jsp:include page="getmenu.jsp" flush="true"/>
		</div>
		</body>
		</html>
	<%
			return;
	} // End of same account check

	uStat = userB.getUserStatus(); //KM
    if (uStat==null)
		uStat = "-";


	userHidden = userB.getUserHidden();//KM

	if (userB.getUserPerAddressId() != null) {

		addressUserB.setAddId(EJBUtil.stringToNum(userB.getUserPerAddressId()));

		addressUserB.getAddressDetails();

	}

	if (userB.getUserSiteId() != null) {

		siteB.setSiteId(EJBUtil.stringToNum(userB.getUserSiteId()));

		siteB.getSiteDetails();

	}


		userLastName =userB.getUserLastName();

		userFirstName= userB.getUserFirstName();

		userMidName= userB.getUserMidName();

		userAddPri= addressUserB.getAddPri();
		userAddPri = (   userAddPri  == null      )?"":(  userAddPri ) ;

		userAddCity =addressUserB.getAddCity();
		userAddCity = (   userAddCity  == null      )?"":(  userAddCity ) ;

		userAddState =addressUserB.getAddState();
		userAddState = (   userAddState  == null      )?"":(  userAddState ) ;

		userAddCountry= addressUserB.getAddCountry();
		userAddCountry = (   userAddCountry  == null      )?"":(  userAddCountry ) ;

		userAddZip= addressUserB.getAddZip();
		userAddZip = (   userAddZip  == null      )?"":(  userAddZip ) ;

		userAddPhone= addressUserB.getAddPhone();
		userAddPhone = (   userAddPhone  == null      )?"":(  userAddPhone ) ;

		userEmail = addressUserB.getAddEmail();
		if(userEmail==null)
		{
			userEmail = "";
		}

		userJobType =userB.getUserCodelstJobtype();

		userSpl= userB.getUserCodelstSpl();

		userSiteId = userB.getUserSiteId();

		siteName =siteB.getSiteName();

		siteAddPri =addressSiteB.getAddPri();

		userAddresId  = userB.getUserPerAddressId();

	      dAccSites = cd2.toPullDown("accsites",EJBUtil.stringToNum(userSiteId));

	      dJobType = cd.toPullDown("jobType", EJBUtil.stringToNum(userJobType));

	      dPrimSpl = cd3.toPullDown("primarySpeciality", EJBUtil.stringToNum(userSpl));


	}

	else

	{

	   cd1.getAccountGroups(accId);

	   dAccSites = cd2.toPullDown("accsites");

 	   dJobType = cd.toPullDown("jobType");

	   dPrimSpl = cd3.toPullDown("primarySpeciality");


	}


%>
<div>
<div style="border:1">
			
				<div id="userDetailsDIV" class="portlet portletstatus ui-widget ui-widget-border ui-helper-clearfix ui-corner-all">
					<div id="userDetailsTab1content" onclick="toggleDiv('userDetailsTab1')" class="portlet-header portletstatus ui-widget 
					ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all" style="background-color:rgb(140, 130, 130)">
						<span class="ui-icon ui-icon-triangle-1-s"></span>
			<%=LC.L_User_Dets%>
					</div>
				<div id='userDetailsTab1'>

  <Form name = "userdetails" method="post" action="updateNonSystemUser.jsp"
        onsubmit = "if (validate(document.userdetails,'<%=userSiteId%>')==false) {setValidateFlag('false');return false;} else {setValidateFlag('true'); blockSubmit(); return true;}" >
    <%




if (mode.equals("M"))

	{ %>

	<input type="hidden" name="userOldPwd" size = 20>
    <input type="hidden" name="userOldESign" size = 20>
    <input type="hidden" name="userId" size = 20  value = <%=userId%> >
    <input type="hidden" name="userAddId" size = 20  value = <%=userAddresId%> >
    <input type="hidden" name="userSession" size = 20  value = <%=userSession%> >


    <%
	if(EJBUtil.stringToNum((String)tSession.getValue("userId")) == userId) { selfFlag = true;}

} %>
    <input type="hidden" name="mode" size = 20  value = <%=mode%> >
    <input type="hidden" name="selfFlag" size = 20  value = <%=selfFlag%> >
    <input type="hidden" name="srcmenu" size = 20  value = <%=src%> >
    <input type="hidden" name="pname" size=20 value='<%=pname%>'>
    <P class = "sectionHeadings"> <%=LC.L_User_Information%><%--User Information*****--%> </P>

    <table width="100%" cellspacing="2" cellpadding="2" border="0" class="basetbl">
 

      <% if(selfFlag){
		String userCode = userB.getUserCode((new Integer(userId)).toString());
	%>
      <tr>
        <td width="200"> <%=LC.L_User_Id%><%--User ID*****--%>  </td>
        <td>
          <input type="text" name="userCode" size = 35 MAXLENGTH = 30 Value="<%=userCode%>" READONLY>
        </td>
      </tr>
     <%
	}
	%>
      <tr>
        <td width="200"> <%=LC.L_First_Name%><%--First Name*****--%> <FONT class="Mandatory">* </FONT> </td>
        <td>
          <input type="text" name="userFirstName" size = 35 MAXLENGTH = 30 Value="<%=userFirstName%>">
        </td>
      </tr>
      <tr>
        <td width="200"> <%=LC.L_Last_Name%><%--Last Name*****--%> <FONT class="Mandatory">* </FONT> </td>
        <td>
          <input type="text" name="userLastName" size = 35 MAXLENGTH = 30 Value="<%=userLastName%>">
        </td>
      </tr>
      <tr>
        <td width="200"> <%=LC.L_Address%><%--Address*****--%> </td>
        <td>
          <input type="text" name="userAddress" size = 35 MAXLENGTH = 50 Value="<%=userAddPri%>">
        </td>
      </tr>
      <tr>
        <td width="200"> <%=LC.L_City%><%--City*****--%> </td>
        <td>
          <input type="text" name="userCity" size = 35  MAXLENGTH = 30 Value="<%=userAddCity%>">
        </td>
      </tr>
      <tr>
        <td width="200"> <%=LC.L_State%><%--State*****--%> </td>
        <td>
          <input type="text" name="userState" size = 35 MAXLENGTH = 30 Value="<%=userAddState%>">
        </td>
      </tr>
      <tr>
        <td width="200"> <%=LC.L_ZipOrPostal_Code%><%--Zip/Postal Code*****--%> </td>
        <td>
          <input type="text" name="userZip" size = 15 MAXLENGTH = 15 Value="<%=userAddZip%>">
        </td>
      </tr>
      <tr>
        <td width="200"> <%=LC.L_Country%><%--Country*****--%> </td>
        <td>
          <input type="text" name="userCountry" size = 35 MAXLENGTH = 50 Value="<%=userAddCountry%>">
        </td>
      </tr>

      <tr>
        <td width="200"> <%=LC.L_Phone%><%--Phone*****--%> </td>
        <td>
          <input type="text" name="userPhone" size = 35 MAXLENGTH = 100 Value="<%=userAddPhone%>">
        </td>
      </tr>
      <tr>
        <td width="200"> <%=LC.L_EhypMail%><%--E-Mail*****--%>  </td>
        <td>
          <input type="text" name="userEmail" size = 35 MAXLENGTH = 100 Value="<%=userEmail%>">
        </td>
      </tr>
	  <tr>
      <td width="200"> <%=LC.L_Job_Type%><%--Job Type*****--%> </td>
      <td> <%=dJobType%> </td>
      </tr>

      <tr>
        <td width="200"> <%=LC.L_Primary_Speciality%><%--Primary Speciality*****--%> </td>
        <td > <%=dPrimSpl%> </td>
	 </tr>

	 <% if (pname ==null ){ //show Account Info section only when user comes form Manage Account menu %>


      	  <tr>
          <td><%=LC.L_Organization_Name%><%--Organization Name*****--%><FONT class="Mandatory">*</FONT></td>
          <td > <%=dAccSites%> </td>

          </tr>
  	<%} // (end of pname==null)%>


		<tr>

   <!-- Added  by Manimaran to track the nonsystem user status -->
	<td> <%=LC.L_User_Status%><%--User Status*****--%> </td>

					<% if (mode.equals("N"))
				{%>

				<td><input type="Radio" name="userStatus" value="A" CHECKED onClick="alertDeactivate(document.userdetails,'A')" ><%=LC.L_Active%><%--Active*****--%>
				<input type="Radio" name="userStatus" value="D" onClick="alertDeactivate(document.userdetails,'D')"><%=LC.L_Inactive%><%--Inactive*****--%> </td>

				<%	}
				else {
				 if (uStat.equals("A")){	%>
						<td><input type="Radio" name="userStatus" value="A" CHECKED onClick="alertDeactivate(document.userdetails,'A')"><%=LC.L_Active%><%--Active*****--%>
						<input type="Radio" name="userStatus" value="D" onClick="alertDeactivate(document.userdetails,'D')"><%=LC.L_Inactive%><%--Inactive*****--%> </td>


				<%
					}else{%>
						<td><input type="Radio" name="userStatus" value="A" onClick="alertDeactivate(document.userdetails,'A')"><%=LC.L_Active%><%--Active*****--%>
							<input type="Radio" name="userStatus" value="D" checked ><%=LC.L_Inactive%><%--Inactive*****--%> </td>
				<%}
				}
				%>
						<td>
					</td>

		</tr>

		 <!-- Added by Manimaran for Enh.#U11 -->
         <%
          String checkStr ="";
 		  if (userHidden.equals("1"))
 	   		checkStr = "checked";

         if (mode.equals("M")) {%>
		 <tr><td colspan=2><input type="checkbox" name="userHidden" <%=checkStr%>> <%=MC.M_HideUsrIn_LkUp%><%--Hide this User in User selection Lookups*****--%> </td></tr>
		 <%}%>
		 

</TABLE>
  
</div></div>



<div id="userMoreDetailsDIV" class="portlet portletstatus ui-widget ui-widget-border ui-helper-clearfix ui-corner-all">
		<div id="userDetailsTab2content" onclick="toggleDiv('userDetailsTab2');" class="portlet-header portletstatus ui-widget 
		ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all" style="background-color:rgb(140, 130, 130)">
			<span class="ui-icon ui-icon-triangle-1-s"></span>
		<%=LC.L_More_Dets%>
		</div>
		<div id='userDetailsTab2'>
			<jsp:include page="moredetails.jsp" >
				<jsp:param name="modId" value="<%=userId %>"/>
				<jsp:param name="modName" value="user"/>
				<jsp:param name="includeMode" value="Y"/>
			</jsp:include>
		</div>
</div>

</div>




		
		<%
		String showSubmit = "";
		String dispEsign = "";


		if (mode.equals("N") || ( !(mode.equals("N")) && (pageRight >= 6))){
			dispEsign = "Y";

		if (mode.equals("N")){
			showSubmit = "Y";
		}
		else {
			   showSubmit = "N";
			   if (pageRight >= 6){
				   showSubmit ="Y";
			   }
			   else
					{showSubmit ="N";}
			}

		%>
		<div style="float:left;width:90%">
		<jsp:include page="submitBar.jsp" flush="true">
				<jsp:param name="displayESign" value="<%=dispEsign%>"/>
				<jsp:param name="formID" value="copyverfrm"/>
				<jsp:param name="showDiscard" value="N"/>
				<jsp:param name="showSubmit" value="<%=showSubmit%>"/>
		</jsp:include>
</div>
		<%
		}

		%>

  <input type="hidden" name="userSiteId" value = <%=userSiteId%>>
  <input type="hidden" name="changeOrgFlg" value="false">
     <input type=hidden name="deactivateFromTeam" value="0" >


  </Form>

  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right


}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>

  <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu">
  <jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>


