<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head> 

<title><%=MC.M_MngAcc_GrpDets%><%-- Manage Account >> Group Details*****--%></title>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<%@ page import="com.velos.eres.service.util.*,com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.GrpRightsJB"%>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>



<SCRIPT Language="javascript">

 function  validate(formobj){

//     formobj=document.group

     if (!(validate_col('Group Name',formobj.groupName))) return false
     //Ankit:  Bug-10718 Date- 17-July-2012
     formobj.groupName.value = formobj.groupName.value.replace(/^\s+|\s+$/g,"");
     
//	if (!(validate_col('e-Signature',formobj.eSign))) return false

//	if(isNaN(formobj.eSign.value) == true) {
//	alert("Incorrect e-Signature. Please enter again");
//	formobj.eSign.focus();
//	return false;
//   }

   }

</SCRIPT>



<jsp:useBean id="groupB" scope="session" class="com.velos.eres.web.group.GroupJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>



<% String src;

src= request.getParameter("srcmenu");

%>



<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   



<body>
<DIV class="tabDefTopN" id="divTab"> 
	<jsp:include page="accounttabs.jsp" flush="true"> 
	<jsp:param name="selectedTab" value="2"/>
	</jsp:include>
</DIV>			
<DIV class="tabDefBotN" id="div1"> 
  <%

	int grpId  =0;

	String grpName ="";

	String grpDesc ="";

	String accId="";

	String mode="";
	
	String groupHidden = "";//KM

	HttpSession tSession = request.getSession(true); 

       

	if (sessionmaint.isValidSession(tSession))

	{



		String uName = (String) tSession.getValue("userName");

	        grpId = EJBUtil.stringToNum(request.getParameter("grpId"));

		mode = request.getParameter("mode");

		String fromPage = request.getParameter("fromPage");

		int pageRight = 0;

		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		

        	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CGRP"));



	

		if (mode.equals("M")) {

			groupB.setGroupId(grpId);

			groupB.getGroupDetails();

			grpName  = groupB.getGroupName();

			grpDesc= groupB.getGroupDesc ();
			
			if (grpDesc == null)
				grpDesc = "";

			accId = groupB.getGroupAccId();
			
			groupHidden = groupB.getGroupHidden();

   		}else{

			UserJB user = (UserJB) (tSession.getValue("currentUser"));

			accId = user.getUserAccountId();

		}

%>
  <%

	if ((mode.equals("M") && pageRight >=6) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 )) )

	{



%>
  <Form name="group" id="grpfrm" method="post" action="updategroup.jsp" onsubmit = "if (validate(document.group)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}" >
    <table width="400" cellspacing="0" cellpadding="0">
      <tr> 
        <td> 
          <P class = "defComments"> <%=MC.M_Etr_GrpDets%><%-- Please enter Group Details*****--%>: </P>
        </td>
      </tr>
    </table>
    <P class = "sectionHeadings"> 
      <%if (mode.equals("M")) {%>
      <A href="groupRights.jsp?mode=M&srcmenu=<%=src%>&groupId=<%=grpId%>&groupName=<%=grpName%>"><%=LC.L_Assign_GrpRights%><%--Assign 
      Group Rights*****--%></A> 
      <%}%>
    </P>
    <table width="98%" cellspacing="1" cellpadding="1">
      <tr> 
        <td width="25%" align="right"> <%=LC.L_Group_Name%><%-- Group Name*****--%> <FONT class="Mandatory">* </FONT> &nbsp;&nbsp;</td>
        <td> 
          <input type="text" name="groupName" size = 35 MAXLENGTH = 50 	value="<%=grpName%>" onblur="this.value = this.value.replace(/^\s+|\s+$/g,'')">
          <input type="hidden" name="groupNameOld" MAXLENGTH = 50 value="<%=grpName%>">
        </td>
      </tr>
      <tr> 
        <td align="right"> <%=LC.L_Group_Desc%><%-- Group Description*****--%> &nbsp;&nbsp;</td>
        <td> 
          <input type="text" name="groupDesc" size = 50 MAXLENGTH = 200 	value="<%=grpDesc%>">
        </td>
      </tr>
      <tr> 
        <td> 
          <input type="hidden" name="groupId" MAXLENGTH = 15 value="<%=grpId%>">
        </td>
      </tr>
      <tr> 
        <td> 
          <input type="hidden" name="groupAccId" MAXLENGTH = 15 value="<%=accId%>">
        </td>
      </tr>
      
    <tr>
	<td colspan="2">&nbsp;&nbsp;&nbsp;
	<% 
	//Added by Manimaran for Enh.#U11.
	String checkStr ="";
	if (groupHidden.equals("1"))
		checkStr = "checked";
	
	if (mode.equals("M")){ %>
	<input type="checkbox" name="groupHidden" <%=checkStr%>> <%=MC.M_HideGrp_InGrpLkUp%><%-- Hide this Group in Group and User selection Lookups*****--%>
	<%}%>
	
	</td>
	</tr>
    </table>

	<%
	if (!grpName.equalsIgnoreCase("admin"))
	{
	%>
	<table width="98%" cellspacing="0" cellpadding="0" bgcolor="#cccccc" >
	<!--   	<td width="50%" align="right" bgcolor="#cccccc"> <span id="eSignMessage"></span>
				e-Signature <FONT class="Mandatory">* </FONT>
	   </td>

	   <td width="20%" bgcolor="#cccccc">
		<input type="password" name="eSign" id="eSign" maxlength="8" onkeyup="ajaxvalidate('misc:'+this.id,4,'eSignMessage','Valid e-Sign','Invalid e-Sign','sessUserId')">
	    </td>	-->
	<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="N"/>
		<jsp:param name="formID" value="grpfrm"/>
		<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
</table>
    

	<%
	}
	else
	{	
	%>
	<table><tr><td>
	<FONT class="Mandatory"><%=MC.M_CntEdit_DetDefAdminGrp%><%-- You can not edit details of the default Admin group*****--%></FONT>
    </td></tr></table>
	<%
	}
	%>
	
    <input type="hidden" name="mode" MAXLENGTH = 15 value="<%=mode%>">
    <input type="hidden" name="src" MAXLENGTH = 15 value="<%=src%>">
    <input type="hidden" name="fromPage" MAXLENGTH = 15 value="<%=fromPage%>">	
    <br>
    <table width="400" >
      <tr> 
        <td align="right"> 		
         <!-- <input type="Submit" name="submit" value="Submit"> -->
	 <!--<input type="image" src="../images/jpg/Submit.gif" onClick = "return validate()" align="absmiddle" border="0"> -->	
        </td>
      </tr>
    </table>
  </Form>
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div><div class ="mainMenu" id = "emenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>

