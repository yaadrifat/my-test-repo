<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="personB" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="portalFormsB"  scope="request" class="com.velos.esch.web.portalForms.PortalFormsJB"/>

<%@page import="com.velos.eres.dashboard.business.DashboardDAO"%>
<%@page import="java.util.ArrayList,java.util.HashMap,org.json.*"%>
<html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=MC.M_MngPat_StdFrms%><%--Manage <%=LC.Pat_Patient%> >> <%=LC.Std_Study%> Forms*****--%></title>


<!--Important for Inner response Query design on 24/04/2015 By Bindu -->


<style type="text/css">
.ui-dialog .ui-dialog-titlebar-close span
{
padding : 0 0 0 0 ;
}
</style>




<script>
window.onunload = function(){
		var schevent;

		schevent = window.document.er_fillform1.schevent.value;
		if (document.getElementById('calledFromForm').value != 'dashboard'){
			if (schevent.length > 0 && (!isWhitespace( schevent ) ) && schevent != "null" )
			{
				if (window.opener != undefined)
				{
					try {
						window.opener.location.reload();
					} catch(e) {}
				}
			}
		}
	}
</script>

<SCRIPT>



function openUserSearch(formobj,colName) {
//alert(formobj);
//alert(colName);
  //  windowName=window.open("formusersearch.jsp?formobj="+formobj+"&colName="+colName,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
//	windowName.focus();
}

function openPrintWin(formId,filledFormId,patProtId,patientCode,pkey,studyId,protocolId,formLibVerNumber) {

   windowName=window.open("formprint.jsp?formId="+formId+"&filledFormId="+filledFormId+"&patProtId="+patProtId+"&formLibVerNumber="+formLibVerNumber+"&patientCode="+patientCode+"&pkey="+pkey+"&studyId="+studyId+"&protocolId="+protocolId+"&linkFrom=P","Information1","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
   windowName.focus();
}

function openQueries(formId,filledFormId,studyId) {
	  windowName=window.open("addeditquery.jsp?studyId="+studyId+"&formId="+formId+"&filledFormId="+filledFormId+"&from=4","Information1","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
   windowName.focus();

	  var timer = setInterval(function() {   
		    if(windowName.closed) {  
		        clearInterval(timer);  
		        location.reload(); 
		        parent.preview1.location.reload(); 
		    }  
		}, 500);

}

function openWinStatus(patId,patStatPk,pageRight,study)
{

	changeStatusMode = "yes";

	if (f_check_perm(pageRight,'N'))
	{
		windowName= window.open("patstudystatus.jsp?studyId="+study+"&changeStatusMode=yes&statid=" + patStatPk + "&pkey=" + patId,"patstatusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=550,height=560");
		windowName.focus();
	}
}

//JM: modified 112006
function  openReport(formobj,reportId,reportName,repFilledFormId,act,displayMode){

	formobj.repId.value = reportId;
	formobj.repName.value = reportName;
	formobj.repFilledFormId.value = repFilledFormId;
	formobj.filterType.value = "A";
	formobj.displayMode.value=displayMode;

	formobj.action="repRetrieve.jsp";
	formobj.target="_new";
	formobj.submit();
	formobj.action=act;
	//formobj.target="";

	}
function confirmBox(pageRight,isLockdown,formStatusDesc)
{
	if (f_check_perm(pageRight,'E') == true)
		{
			msg="<%=LC.L_Del_ThisResponse%>"/*Delete this Response?*****/;

			if(isLockdown==true){
				var paramArray = [formStatusDesc];
				alert(getLocalizedMessageString("M_CantDelete_LockDownFrmResp",paramArray));/*Lockdown status form response cannot be deleted.*****/
			      return false;}
			  else{
				if (confirm(msg)) 
				{
				   return true;
				}
				else
				{
				   return false;
				}
			  }
		}
		else
		{
			return false;
		}
}

function removeLocation(formobj, turn) {
	if (turn == 1){
		formobj.storageLoc.value = "";
		formobj.mainFKStorage.value ="";
	}else if(turn==2){
		formobj.storageLocation.value = "";
		formobj.mainFKStorageId.value ="";
	}else if(turn==3){
		formobj.eStorageLocation.value = "";
		formobj.eMainFKStorageId.value ="";
	}
}
</SCRIPT>

</head>


<%
String dashBoard=request.getParameter("dashBoard");
String teamRole="";
String src= request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
String statDesc=request.getParameter("statDesc");
String statid=request.getParameter("statid");
String patProtId=request.getParameter("patProtId");
String patientCode=request.getParameter("patientCode");
String mode = request.getParameter("mode");
String patientId = request.getParameter("pkey");
String studyId = request.getParameter("studyId");
String entryChar = request.getParameter("entryChar");
String formFillDt = request.getParameter("formFillDt");
String formPullDown = request.getParameter("formPullDown");
String fldMode = request.getParameter("fldMode");
String formId = request.getParameter("formId");
String formLibVer = request.getParameter("formLibVer");
String schevent = "";
boolean showSpecimenDetails = false;
String eSignRequired = "";

String fkStorageForSpecimen ="";
int createNewSpecimen = 0;

int personPK = StringUtil.stringToNum(patientId);
if (sessionmaint.isValidSession(request.getSession(false))) {
	personB.setPersonPKId(personPK);
	personB.getPersonDetails();
	if (!StringUtil.isEmpty(patientId)) {
		// Get patientCode from DB; don't always trust the request parameter
		patientCode = personB.getPersonPId();
	}
}
patientCode = StringUtil.encodeString(patientCode);

//if the form link is called from patient portal, grant all rights
		String ignoreRights = "";
		String pp_consenting = "";//portal


CodeDao cdaoRespStatus = new CodeDao();
	int completeStatus = 0;
	completeStatus = cdaoRespStatus.getCodeId("fillformstat", "complete");


	schevent = request.getParameter("schevent");
	fkStorageForSpecimen = request.getParameter("fkStorageForSpecimen");


	if ( StringUtil.isEmpty("schevent") )
	{
		schevent = "";
	}
//BK,FIX#5980-MAR-28-2010
	/*if ( StringUtil.isEmpty("fkStorageForSpecimen") )
	{
		fkStorageForSpecimen = "";
	} else
	{
		createNewSpecimen  = 1;
	}*/



	String calledFromForm = "";
     calledFromForm = request.getParameter("calledFromForm");

      if (StringUtil.isEmpty(calledFromForm))
      {
     	calledFromForm = "";
      }

     String outputTarget = ""; //if a target name is passed, open the new and edit page in this target
	  outputTarget = request.getParameter("outputTarget");

	 if (StringUtil.isEmpty(outputTarget))
	 {
	 	outputTarget = "";
	 }

	  String specimenPk = "";
    specimenPk = request.getParameter("specimenPk");

     if (StringUtil.isEmpty(specimenPk))
	 {
	 	specimenPk = "";
	 }


   if (calledFromForm.equals(""))
   {

%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
 <% }
 else
 { %>
 	<jsp:include page="include.jsp" flush="true"/>

 	<%
 }
 %>

<script>
checkQuote="N";
</script>

<jsp:include page="ui-include.jsp" flush="true"></jsp:include>

<%@page language = "java" import="com.velos.eres.business.common.*,com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.common.SaveFormDao,com.velos.eres.business.common.TeamDao,com.velos.eres.service.util.*,com.velos.eres.web.studyRights.StudyRightsJB,java.util.*,com.velos.eres.service.util.*,java.util.*,com.velos.eres.business.common.CodeDao, com.velos.eres.business.common.FormLibDao" %>
<%@ page import ="com.velos.eres.business.common.FormResponseDao" %>
<%@ page import ="com.velos.eres.web.study.StudyJB" %>
<%@ page import ="com.velos.eres.web.patLogin.PatLoginJB" %>
<%@ page import ="com.velos.esch.business.common.ScheduleDao" %>
<%@ page import ="com.velos.eres.business.common.PortalDesignDao" %>
<%@ page import ="com.velos.esch.business.common.PortalFormsDao" %>

<jsp:useBean id="lnkformsB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="eventAssocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="formLibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="patB" scope="request" class="com.velos.eres.web.patStudyStat.PatStudyStatJB"/>
<jsp:useBean id="portalJB" scope="request" class="com.velos.eres.web.portal.PortalJB"/>
<jsp:useBean id="portalDesignB" scope="request" class="com.velos.eres.web.portalDesign.PortalDesignJB"/>

<!--<body style="overflow:scroll">-->
<body>

<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;">
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
	</div>

<%
if (calledFromForm.equals(""))
  {
%>

<div class="BrowserTopn" id="div1">

<jsp:include page="patienttabs.jsp" flush="true">
<jsp:param name="pkey" value="<%=patientId%>"/>
<jsp:param name="statDesc" value="<%=statDesc%>"/>
<jsp:param name="statid" value="<%=statid%>"/>
<jsp:param name="patProtId" value="<%=patProtId%>"/>
<jsp:param name="patientCode" value="<%=patientCode%>"/>
<jsp:param name="fromPage" value=""/>
<jsp:param name="page" value="patientEnroll"/>

</jsp:include>
</div>
<DIV class="tabFormBotN tabFormBotN_adventnew_1" id="div2">
<%
   }else{%>
<!--<DIV class="popDefault" id="div2">-->
<DIV id="div2">
<%}
String filledFormId = "";
HttpSession tSession = request.getSession(true);
 if (sessionmaint.isValidSession(tSession))
 {
 if (studyId == null || studyId.equals("null")){
 	tSession.setAttribute("studyIdForFormQueries","");	
 }else{
 	tSession.setAttribute("studyIdForFormQueries",studyId);
 }
 ignoreRights = (String) tSession.getValue("pp_ignoreAllRights") ;
 if (StringUtil.isEmpty(ignoreRights))
 {
	ignoreRights = "false";
 }


 pp_consenting = (String) tSession.getValue("pp_consenting") ;

 
	// === Start of overall access rights ===
	
  	int pageRight=0,formRights=0;
  	SaveFormDao saveDao=new SaveFormDao();
  	String filledFromStatValue ="";
    String usr = (String) tSession.getValue("userId");
    
	//GET STUDY TEAM RIGHTS
    TeamDao teamDao = new TeamDao();
    teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(usr));
    ArrayList tId = teamDao.getTeamIds();
    
    Hashtable htMoreParams = new Hashtable();

   String roleCodePk = "";

    if (tId.size() == 0)
	{
    	pageRight=0 ;
    }
	else
	{
				ArrayList arRoles = new ArrayList();
				arRoles = teamDao.getTeamRoleIds();

				if (arRoles != null && arRoles.size() >0 )
				{
					roleCodePk = (String) arRoles.get(0);

					if (StringUtil.isEmpty(roleCodePk))
					{
						roleCodePk="";
					}
				}
				else
				{
					roleCodePk ="";
				}

			htMoreParams.put("teamRolePK",roleCodePk);

    		stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));
    		ArrayList teamRights ;
						teamRights  = new ArrayList();
						teamRights = teamDao.getTeamRights();

						stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
						stdRights.loadStudyRights();


    	if ((stdRights.getFtrRights().size()) == 0)
		{
    	 	pageRight= 0;
    	 	formRights=0;
    	}
		else
		{
    		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMACC"));
    		formRights = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
    	}
    }
    /// entered for having rights for study linked forms


	String responseId=request.getParameter("responseId");
   responseId=(responseId==null)?"":responseId;

   String parentId=request.getParameter("parentId");
   parentId=(parentId==null)?"":parentId;

   String userId = (String) tSession.getValue("userId");
   String findStr = "";
   String parentFlag="";
   StringBuffer sbuffer = null;
   int len = 0;
   int pos = 0;
   int siteId = 0;
   String patStatSubType = "";
   String patStudyStat =  "";
   int patStudyStatpk = 0;
   int fdaStudy = 0;


   String protocolId = "";
   String enrollId =(String) tSession.getValue("enrollId");


		patEnrollB.setPatProtId(EJBUtil.stringToNum(enrollId));
		patEnrollB.getPatProtDetails();
		protocolId =patEnrollB.getPatProtProtocolId();

		String protName = "";
		if(protocolId != null) {
		eventAssocB.setEvent_id(EJBUtil.stringToNum(protocolId));
		eventAssocB.getEventAssocDetails();
		protName = 	eventAssocB.getName();
		}

		String studyTitle = "";
		String studyNumber = "";
		studyB.setId(EJBUtil.stringToNum(studyId));

		studyB.getStudyDetails();
		studyTitle = studyB.getStudyTitle();
		studyNumber = studyB.getStudyNumber();

		String accessRight = "";
		int orgRight = 0;
		
		// Start access rights just for PP
		if (ignoreRights.equals("true"))
		{
			orgRight = 7;
			pageRight = 7;
			formRights = 7;
			// check if PP logged-in user is the form response creator
			PatLoginJB patLoginJB = (PatLoginJB)tSession.getAttribute("pp_currentLogin");
			try {
				String patientPortalPatId = patLoginJB.getPlId();
				if (!"N".equals(request.getParameter("mode"))) { // PP user cannot edit existing form responses
					throw new Exception();
				}
				if (StringUtil.stringToNum(request.getParameter("filledFormId")) > 0) {
					// Make sure patientPortalPatId = fk_per in form response
					String ppFkPer = lnkformsB.getFormResponseFkPer(
							StringUtil.stringToNum(request.getParameter("filledFormId")));
					// Make sure creator of form response matches with portal's audit user
					String creatorForPortal = lnkformsB.getCreatorForPortal();
					boolean auditUserMatchesWithCreator = portalJB.comparePortalAuditUserWith(
							StringUtil.stringToNum(creatorForPortal), 
							StringUtil.stringToNum(patLoginJB.getFkPortal()));
					System.out.println("patstudyformdetails.jsp: patientPortalPatId="+patientPortalPatId+" ppFkPer="+ppFkPer);
					pageRight = 0;
					formRights = 0;
					if (auditUserMatchesWithCreator &&
							StringUtil.stringToNum(ppFkPer) > 0 &&
							StringUtil.stringToNum(patientPortalPatId) ==
							StringUtil.stringToNum(ppFkPer)) {
						pageRight = 7;
						formRights = 7;
					}
				}
				
				// Make sure patientPortalPatId = pkey
				if (StringUtil.stringToNum(patientPortalPatId) != personPK) {
					pageRight = 0;
					formRights = 0;
				}
				
				// Make sure studyId is the fk_study of portal
				int fkPortal = StringUtil.stringToNum(patLoginJB.getFkPortal());
				portalJB.setPortalId(fkPortal);
				portalJB.getPortalDetails();
				int portalStudy = StringUtil.stringToNum(portalJB.getPortalStudy());
				if (StringUtil.stringToNum(portalJB.getPortalStudy()) != StringUtil.stringToNum(studyId)) {
					pageRight = 0;
					formRights = 0;
				}
				
				// Make sure formId is in portal design
				PortalDesignDao pdDao = portalDesignB.getPortalModValues(fkPortal);
				ArrayList fkIds = pdDao.getFkIds();
				ArrayList portalModTypes = pdDao.getPortalModTypes();
				boolean isValidFormId = false;
				for (int iX = 0; iX < portalModTypes.size(); iX++) {
					if (StringUtil.isEmpty(formId)) { break; }
					if ("EF".equals(portalModTypes.get(iX)) || "LF".equals(portalModTypes.get(iX))) {
						if (fkIds.get(iX) == null) { break; }
						String[] splitFormIds = ((String)fkIds.get(iX)).split(",");
						for (int iY = 0; iY < splitFormIds.length; iY++) {
							if (StringUtil.stringToNum(splitFormIds[iY]) == StringUtil.stringToNum(formId)) {
								isValidFormId = true;
								break;
							}
						}
					} else if ("SF".equals(portalModTypes.get(iX))) {
						if (fkIds.get(iX) == null) { break; }
						String[] splitCalIds = ((String)fkIds.get(iX)).split(",");
						for (int iY = 0; iY < splitCalIds.length; iY++) {
							PortalFormsDao pfDao = portalFormsB.getCheckedFormsForPortalCalendar(
									StringUtil.stringToNum(splitCalIds[iY]), fkPortal);
							ArrayList checkedForms = pfDao.getCheckedForms();
							if (checkedForms == null || checkedForms.size() < 1) { continue; }
							for (int iZ = 0; iZ < checkedForms.size(); iZ++) {
								if (StringUtil.isEmpty((String)checkedForms.get(iZ))) { continue; }
								if (StringUtil.stringToNum((String)checkedForms.get(iZ)) == StringUtil.stringToNum(formId)) {
									isValidFormId = true;
									break;
								}
							}
							if (isValidFormId) { break; }
						}
					}
					if (isValidFormId) { break; }
				}
				if (!isValidFormId) {
					pageRight = 0;
					formRights = 0;
				}
				
			} catch(Exception e) {
				pageRight = 0;
				formRights = 0;
			}
		} else {  // End of access rights just for PP and start of access rights just for eResearch
			//Bug# 6638
			int all_Patient_Forms_Access=0;
			int study_Specific_From_Access=0;
			GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
			StudyRightsJB stdRits =(StudyRightsJB) tSession.getValue("studyRights");
			if(null!=stdRits){
				study_Specific_From_Access = Integer.parseInt(stdRits.getFtrRightsByValue("STUDYFRMACC"));
			}
			if(null!=grpRights){
				all_Patient_Forms_Access = Integer.parseInt(grpRights.getFtrRightsByValue("PATFRMSACC"));
			}
			if(isAccessibleFor(all_Patient_Forms_Access,'E')){
				accessRight="E";
			}else if(isAccessibleFor(all_Patient_Forms_Access,'V') || isAccessibleFor(all_Patient_Forms_Access,'N')){
				accessRight="V";
			}
			if(null!=stdRits 
					&& isAccessibleFor(study_Specific_From_Access, 'E')){
				accessRight="E";
			}else {
				accessRight="V";
			}
			
			// Start of orgRight calculation
			orgRight = userSiteB.getUserPatientFacilityRight(EJBUtil.stringToNum(userId), EJBUtil.stringToNum(patientId) );
			// Start Fixing Bug# 14781
			if (StringUtil.stringToNum(request.getParameter("filledFormId")) > 0) {
		        String ppFkPer = lnkformsB.getFormResponseFkPer(
		    			StringUtil.stringToNum(request.getParameter("filledFormId")));
		        int temperedPatId = StringUtil.stringToNum(ppFkPer);
		        if(orgRight>0)
		        orgRight = userSiteB.getUserPatientFacilityRight(EJBUtil.stringToNum(userId), temperedPatId);
		    }
			
			// Fix For Bug#14800 
			if ( null!=patProtId && orgRight>0){
				patEnrollB.setPatProtId(StringUtil.stringToNum(patProtId));
				patEnrollB.getPatProtDetails();
				int patID =StringUtil.stringToNum(patEnrollB.getPatProtPersonId());
				orgRight = userSiteB.getUserPatientFacilityRight(StringUtil.stringToNum(userId), patID);
				if (patID > 0 && personPK != patID) {
					orgRight = 0; // patID derived from patProtId and pkey don't match => reject
				}
			}
			
			if (orgRight > 0){
				orgRight = userSiteB.getMaxRightForStudyPatient(StringUtil.stringToNum(userId),StringUtil.stringToNum(patientId) , StringUtil.stringToNum(studyId) );
				//System.out.println("patient schedule orgRight" + orgRight);
			}
			// End Fixing Bug# 14781
			// End of orgRight calculation
			
			// Validate that formId is in user's fK_account
			int formIdStr=0;
			StringTokenizer strTokenizers = new StringTokenizer(formPullDown,"*");
			formIdStr = StringUtil.stringToNum(strTokenizers.nextToken());
			int patFormIdInt = -1;
			if (!StringUtil.isEmpty(formId)) {
				UserJB userB1 = (UserJB) tSession.getAttribute("currentUser");
				LinkedFormsDao lnkFrmDao1 = lnkformsB.getPatientStudyForms(
						StringUtil.stringToNum((String)tSession.getAttribute("accountId")),
						StringUtil.stringToNum((String)patientId),
						StringUtil.stringToNum((String)studyId),
						StringUtil.stringToNum((String)tSession.getAttribute("userId")),
						StringUtil.stringToNum(userB1.getUserSiteId()),
						true); // true => includeHidden
				boolean isValidAccountFormId = false;
				try { patFormIdInt = Integer.parseInt(formId); } catch(Exception e) {}
				for (Object myFormId : lnkFrmDao1.getFormId()) {
					int myFormIdInt = ((Integer) myFormId).intValue();
					if (patFormIdInt == myFormIdInt && (patFormIdInt==formIdStr)) {
						isValidAccountFormId = true;
						break;
					}
				}
				if (!isValidAccountFormId) {
					pageRight = 0; // Invalid formId in request parameter => reject
				}
			} // End of validate formId in user's fk_account
		} // End of access rights just for eResearch
		
		// --- Start of access rights common to both PP and eResearch
		// Below this point, only set pageRight=0; do not set pageRight=7
						
		// Make sure formId and formPullDown are consistent
		StringTokenizer strtkzr =  new StringTokenizer(formPullDown,"*");
		String formIdFragment = strtkzr.nextToken();
		boolean isValidFormPullDown = false;
		if (!StringUtil.isEmpty(formId) && formId.equals(formIdFragment)) {
			isValidFormPullDown = true;
		}
		if (!isValidFormPullDown) {
			pageRight = 0;
			formRights = 0;
		}

		// --- Start of Bug 14659 fix
		// Make sure the formId and formLibVer combination is valid
		int formIdInt = -1;
		try { formIdInt = Integer.parseInt(formId); } catch(Exception e) {}
		if (!StringUtil.isEmpty(formLibVer)) {
			FormLibDao formLibDao1 = new FormLibDao();
			int fkFormLib = formLibDao1.getFkFormLibForPkFormLibVer(StringUtil.stringToNum(formLibVer));
			if (formIdInt > 0 && formIdInt != fkFormLib) {
				pageRight = 0; // Invalid formId and formLibVer combination in request parameters => reject
			}
		}
		// --- End of Bug 14659 fix
		
		// --- Start of Bug 14810 fix
		// Make sure that schevent is valid
		if (!StringUtil.isEmpty(schevent)) {
			ScheduleDao scheduleDao = new ScheduleDao();
			String schedPatientId = scheduleDao.getPatientIdForScheduledEvent(schevent);
			if (!StringUtil.isEmpty(schedPatientId) && !schedPatientId.equals(patientId)) {
				pageRight = 0;
			}
		}
		// --- End of Bug 14810 fix
		
		session.setAttribute("formQueryRight",""+pageRight);
	
		int filledFormUserAccessInt = lnkformsB.getFilledFormUserAccess(
				StringUtil.stringToNum(request.getParameter("filledFormId")),
				StringUtil.stringToNum(request.getParameter("formId")),
				StringUtil.stringToNum((String)tSession.getAttribute("userId")) );

	// --- End of access rights common to both PP and eResearch ---
	
	// === End of overall access rights ===

	if (pageRight >= 4 && filledFormUserAccessInt > 0 && orgRight > 0)
   {
    	//get current patient status and its subtype

			patStatSubType = patB.getPatStudyCurrentStatusWithCodeSubType(studyId, patientId);
			patStudyStat = patB.getPatStudyStat();
			patStudyStatpk = patB.getId();
			statid = patStudyStat ;
			if (EJBUtil.isEmpty(patStatSubType))
				patStatSubType= "";

			///////////////////////////////////


         filledFormId = request.getParameter("filledFormId");

       if (!StringUtil.isEmpty(filledFormId)){
    	   FormResponseDao respDao = new FormResponseDao();
    	   int linkedStudy = respDao.getStudyOfFormResponse(StringUtil.stringToNum(filledFormId));
    	   
    	   StudyJB linkedStudyB = new StudyJB();
    	   linkedStudyB.setId(linkedStudy);
    	   linkedStudyB.getStudyDetails();
		   fdaStudy = ("1".equals(linkedStudyB.getFdaRegulatedStudy()))? 1:0;
       }       
       int lockDownId=0;
       String childSystemId=""; String dispInSpec = "";

       lnkformsB.findByFormId(EJBUtil.stringToNum(formId));
       dispInSpec =lnkformsB.getLfDispInSpec();

       if (StringUtil.isEmpty(dispInSpec))
       {
       		dispInSpec = "";
       }
       //BK,FIX#5980-MAR-28-2010

/*       if (dispInSpec.equals("1") && mode.equals("N") &&  StringUtil.isEmpty(specimenPk))
       {
       		showSpecimenDetails = true;
       		createNewSpecimen = 1;
       }else
       {
       		createNewSpecimen = 0;
       }*/

       String formHtml = "";
	   String name= "";
	   String formLibVerNumber="";



	   formLibB.setFormLibId(EJBUtil.stringToNum(formId));
	   formLibB.getFormLibDetails();
	   name=formLibB.getFormLibName();
	   parentFlag=formLibB.getFormLibNextMode();

	   eSignRequired =formLibB.getEsignRequired();

	   if (StringUtil.isEmpty(eSignRequired))
	   {
	   		eSignRequired = "0";
	   }

	   String formStatus = formLibB.getFormLibStatus();

		// VA: This should be called through Service Layer
       //But we can;t change too many files right now because
       //  this code (7.1.2.2) change need to be deployed to 7.0.1.7 code
       //Minimal files are are way to go but this need to be optimized
	   if ((responseId.length()>0) && (parentId.length()>0)){
	   childSystemId=saveDao.getForeignKeyField(EJBUtil.stringToNum(formId),EJBUtil.stringToNum(parentId));
	   }

	    //Added by Manimaran for November Enhancement F6
	    //JM: 122206
	   //formLibVerNumber = formLibB.getFormLibVersionNumber(EJBUtil.stringToNum(formLibVer));
		//added by IA 11.05.2006
		FormLibDao fd = new FormLibDao();
		if ((formLibVer==null)|| formLibVer.equals("")){

			formLibVer = fd.getLatestFormLibVersionPK(EJBUtil.stringToNum(formId));

		}
		//end added
   	   formLibVerNumber = formLibB.getFormLibVersionNumber(EJBUtil.stringToNum(formLibVer));

	   CodeDao cdao = new CodeDao();
	   /******** find study team role for DashBoard(jupiter) only******* Begin */
	   if("jupiter".equals(dashBoard)){
			
			int codeLstPk=cdao.getStudyRolePK(StringUtil.stringToNum(studyId),StringUtil.stringToNum((String)tSession.getAttribute("userId")));
			roleCodePk=(codeLstPk==0)?"":StringUtil.integerToString(codeLstPk);
			//roleCodePk=StringUtil.integerToString(codeLstPk);
			htMoreParams.put("teamRolePK",roleCodePk);
		}
	   /*******find study team role for DashBoard(jupiter) only********** End*/
	   
       cdao.getCodeValuesById(EJBUtil.stringToNum(formStatus));
       ArrayList arrSybType = cdao.getCSubType();
       String statSubType = arrSybType.get(0).toString();

       lockDownId=cdao.getCodeId("fillformstat","lockdown");
       %>
      <!--JM: 112006: Added the form tag-->


		 	<Form name="formdata" METHOD="POST">
			<Input type="hidden" id="calledFromForm" name="calledFromForm" value="<%=calledFromForm%>">
 			<Input type="hidden" name="repName" value="">
			<Input type="hidden" name="repId" value="">
			<Input type="hidden" id="formRights" value="<%=formRights%>">
			<Input type="hidden" name="filterType" value = "A">
			<Input type="hidden" name="repFilledFormId" value="">
			<Input type="hidden" name="displayMode" value="">




       <table width=98% border="0">

      <%

	if (calledFromForm.equals(""))
  	{
	%>


    <tr>
	<td><font size="1">
	<%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>: <a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=studyId%>"><%=studyNumber%></a> &nbsp;&nbsp;&nbsp;&nbsp;
	<a href="#" onmouseover="return overlib('<%=StringUtil.escapeSpecialCharJS(studyTitle)%>',CAPTION,'<%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>');" onmouseout="return nd();"><img border="0" src="./images/View.gif"/></a>  &nbsp;&nbsp;&nbsp;&nbsp;
    	<%if(protocolId != null){%>
	<!--Protocol Calendar: <%=protName%><BR>-->
    	<%}

  	}
		//Modified by Manimaran for November Enhancement PS4.
		if((patStatSubType.trim().equalsIgnoreCase("lockdown")))
			{
			%>
				<FONT color="red"><%=MC.M_PatStdStatLkdwn_CntEdtFrm%><%--<%=LC.Pat_Patient%>'s <%=LC.Std_Study_Lower%> status is 'Lockdown'.You cannot edit the form response.*****--%> </FONT>

				<% if (calledFromForm.equals(""))
  					{
  				 %>
				<A onclick="openWinStatus('<%=patientId%>','<%=patStudyStatpk%>','<%=pageRight%>','<%=studyId%>')"  href="#"  ><%=LC.L_Click_Here%><%--Click Here*****--%></A>
				 <FONT class="Mandatory"> <%=MC.M_ToChg_TheStat%><%--to change the status.*****--%></FONT>
				 <%
				 	}
				 %>

		<%
		pageRight=4  ;
		}
		
               if (mode.equals("N"))
	        {
				formHtml =  lnkformsB.getFormHtmlforPreview(EJBUtil.stringToNum(formId),htMoreParams);
	        }
	        else
	        {
	        	saveDao =  lnkformsB.getFilledFormHtml(EJBUtil.stringToNum(filledFormId),"SP","Y",htMoreParams);
	        	
				formHtml =  saveDao.getHtmlStr();
				//get specimenpk
				Hashtable htOtherAttributes = new Hashtable ();

				htOtherAttributes = saveDao.getHtOtherAttributes();

				String fkSpecimenLinkedWithFilledForm ="";

				if (htOtherAttributes != null && htOtherAttributes.containsKey("fkSpecimen"))
				{
					fkSpecimenLinkedWithFilledForm = (String)htOtherAttributes.get("fkSpecimen");

					if (! StringUtil.isEmpty(fkSpecimenLinkedWithFilledForm))
					{
						specimenPk = fkSpecimenLinkedWithFilledForm;
					}
				}
	        }
	     		CodeDao codeLstStatus = new CodeDao();
			filledFromStatValue = codeLstStatus.getCodeDescription(saveDao.getFilledFormStat());
	 %>

     </tr>

     <%
     if (ignoreRights.equals("false")) {
     %>
		<tr bgcolor="#dcdcdc">
			 <td>  <!--KM-to fix the Bug 2809 -->
			 	 <b> <%=LC.L_Frm_Name%><%--Form Name*****--%>: </b><%=name%> &nbsp;&nbsp;&nbsp;&nbsp;
			 </td>

			 <td>
			 	<%if (mode.equals("M")){
			 	if("jupiter".equals(dashBoard)){%>
		 			&nbsp; <A style="display:none" onclick="openQueries(<%=formId%>,<%=filledFormId%>,<%=studyId%>)" href=#> <%=LC.L_AddOrEdit_Queries%><%--Add/Edit Queries*****--%></A>
				<%}else{ %>
					&nbsp; <A onclick="openQueries(<%=formId%>,<%=filledFormId%>,<%=studyId%>)" href=#> <%=LC.L_AddOrEdit_Queries%><%--Add/Edit Queries*****--%></A>
			 	<%}}%>
		 		&nbsp; <A onclick="openPrintWin(<%=formId%>,<%=filledFormId%>,<%=patProtId%>,'<%=patientCode%>',<%=patientId%>,
				<%=studyId%>,<%=protocolId%>,<%=formLibVerNumber%>)" href=#><img src="./images/printer.gif" border="0" title="<%=LC.L_Printer_FriendlyFormat%><%--Printer Friendly Format*****--%>" alt="<%=LC.L_Printer_FriendlyFormat%><%--Printer Friendly Format*****--%>"></img></A>&nbsp;

				<%if (!calledFromForm.equals("dashboard")){
						if (entryChar.equals("E") && (!StringUtil.isEmpty(filledFormId)) ){%>
		 		&nbsp; <A href="#" onclick="openReport(document.formdata,'99','Form Audit Trail','<%=filledFormId%>','formfilledstdpatbrowser.jsp?srcmenu=tdMenuBarItem5&page=1','')" title="<%=LC.L_ViewAud_Trial%><%--View Audit Trail*****--%>"><%=LC.L_Audit%><%--Audit*****--%></A>

		 		&nbsp; <A href="#" onclick="openReport(document.formdata,'99','Track Changes','<%=filledFormId%>','formfilledstdpatbrowser.jsp?srcmenu=tdMenuBarItem5&page=1','<%=accessRight%>')" title="<%=LC.L_Track_Changes%><%--Track Changes*****--%>"><%=LC.L_Track_Changes%><%--Track Changes*****--%></A>

				&nbsp;<A onclick="return confirmBox(<%=pageRight%>,<%=saveDao.isLocked()%>,'<%=filledFromStatValue%>');" href="formrespdelete.jsp?srcmenu=<%=src%>&calledFromForm=<%=calledFromForm%>&selectedTab=<%=selectedTab%>&formId=<%=formId%>&formLibVer=<%=formLibVer%>&mode=M&studyId=<%=studyId%>&pkey=<%=personPK%>&formDispLocation=SP&filledFormId=<%=filledFormId%>&patProtId=<%=patProtId%>&calledFrom=S&statDesc=<%=statDesc%>&statid=<%=statid%>&patientCode=<%=patientCode%>&entryChar=<%=entryChar%>&formFillDt=<%=formFillDt%>&formPullDown=<%=formPullDown%>&schevent=<%=schevent%>&fkStorageForSpecimen=<%=fkStorageForSpecimen%>&outputTarget=<%=outputTarget%>"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" /></A>

				<%		}
				}%>
			 </td>
		</tr>
		<% } //ignore rights
		%>

		<!--Added by Manimaran for the November Enhancement F6-->
		<!--tr>
		<td>
			 	 <b> Version Number: <%=formLibVerNumber%></b>
		</td>
		</tr-->
		</table>




		</form>


<%if (calledFromForm.equals("")) {  %>

<% }

       //SaveFormDao fDao = new SaveFormDao();

  	   //in case of view right, remove the submit button
	  //in case of lockdown status, remove the submit button

		//Replace Submit image with button
  		findStr ="<input id=\"submit_btn\" border=\"0\" align=\"absmiddle\" src=\"../images/jpg/Submit.gif\" type=\"image\">";
  	    len = findStr.length();
  	    pos = formHtml.lastIndexOf(findStr);

		System.out.println("PageRight-"+pageRight+" mode-"+mode+" entryChar-"+entryChar+" statSubType-"+statSubType+" saveDao.isLocked()-"+saveDao.isLocked()+" isSoftLock-"+saveDao.isSoftLock()+" isAllowedUnlock()-"+saveDao.isAllowedUnlock()+ "studyRoleCodePk- "+roleCodePk);
  	    if (pos > 0)
  	    {
  		    sbuffer = new StringBuffer(formHtml);
			if(pageRight==4  || (pageRight == 6 && mode.equals("N")) || (pageRight < 6 && mode.equals("M")) || (entryChar.equals("O")&& mode.equals("M")) || (statSubType.equals("L")) || (saveDao.isLocked())|| saveDao.isSoftLock() && !saveDao.isAllowedUnlock()){
  		    	sbuffer.replace(pos,pos+len,"");
  		    }else{
  		    	sbuffer.replace(pos,pos+len,"<button id=\"submit_btn\" type=\"submit\">"+LC.L_Submit+"</button>");
  		    }
  		    formHtml = sbuffer.toString();
  	    }
  	    
  	    findStr = "name=\"eSign\"";
  	    pos = formHtml.lastIndexOf(findStr);
  	    if(pos > 0)
  	    {
  	    	sbuffer = new StringBuffer(formHtml);
  	    	sbuffer.insert(pos-1," autocomplete=\"off\" ");
  	    	formHtml = sbuffer.toString();  	    	
  	    }
  	    
  	  	findStr ="cannot be greater than today's date";
	    len = findStr.length();
	    pos = formHtml.lastIndexOf(findStr);

	    if (pos > 0)
	    {
		    sbuffer = new StringBuffer(formHtml);
			sbuffer.replace(pos,pos+len,"cannot be a future date.");
		    formHtml = sbuffer.toString();
	    }

		if(pageRight==4  || (pageRight == 6 && mode.equals("N")) || (pageRight < 6 && mode.equals("M")) || (entryChar.equals("O")&& mode.equals("M")) || (statSubType.equals("L")) || (saveDao.isLocked()) || saveDao.isSoftLock() && !saveDao.isAllowedUnlock()){

		   // Disable the Form tag for issue 4632
		   formHtml = formHtml.replaceAll("onSubmit=\"return validationWithAjax\\(\\)\"","")
			  .replaceAll("action=\"updateFormData.jsp\"","action=\"javascript:void(0)\"")
			  .replaceAll("formobj.action=\"querymodal.jsp\"","formobj.action=\"javascript:void(0)\"");
		}

		   findStr = "</Form>";
		   len = findStr.length();
		   pos = formHtml.lastIndexOf(findStr);
		   sbuffer = new StringBuffer(formHtml);
		   StringBuffer paramBuffer = new StringBuffer();

		     //  by salil on 13 oct 2003 to see if the check box type multiple choice exists
     	  //  or not and to handle the case
	     // when no option is selected in a check box type multiple choice the element name does not
	  	// be  carried forward to the next page hence the "chkexists"  variable is used to see
		// if the page has a multiple choice field or not .

		   String chkexists ="";
		   String tochk="type=\"checkbox\"";
		   if(formHtml.indexOf(tochk) == -1 )
		   {
		     chkexists ="N";
		   }
		   else
		   {
		   	 chkexists ="Y";
		   }


		    paramBuffer.append("<input type=\"hidden\" id=\"formPatient\" name=\"formPatient\" value='"+patientId+"'/>");
		    paramBuffer.append("<input type=\"hidden\" name=\"formPatprot\" value='"+patProtId+"'/>");
		    paramBuffer.append("<input type=\"hidden\" id=\"patStudyId\" name=\"patStudyId\" value='"+studyId+"'/>");
		    paramBuffer.append("<input type=\"hidden\" name=\"statDesc\" value='"+statDesc+"'/>");
		    paramBuffer.append("<input type=\"hidden\" name=\"statid\" value='"+statid+"'/>");
		    paramBuffer.append("<input type=\"hidden\" name=\"patientCode\" value='"+patientCode+"'/>");
		    paramBuffer.append("<input type=\"hidden\" name=\"userId\" value='"+userId+"'/>");
		    paramBuffer.append("<input type=\"hidden\" name=\"formDispLocation\" value='SP'/>");
		    paramBuffer.append("<input type=\"hidden\" name=\"formFillMode\" value='"+mode+"'/>");
		    paramBuffer.append("<input type=\"hidden\" id=\"formId\" name=\"formId\" value='"+formId+"'/>");
		    paramBuffer.append("<input type=\"hidden\" name=\"srcmenu\" value='"+src+"'/>");
		   	paramBuffer.append("<input type=\"hidden\" name=\"selectedTab\" value='"+selectedTab+"'/>");
		   	paramBuffer.append("<input type=\"hidden\" name=\"formFillDt\" value='"+formFillDt+"'/>");
		    paramBuffer.append("<input type=\"hidden\" name=\"formPullDown\" value='"+formPullDown+"'/>");
            paramBuffer.append("<input type=\"hidden\" name=\"calledFromForm\" value='"+calledFromForm+"'/>");
		    paramBuffer.append("<input type=\"hidden\" name=\"fldMode\" value='"+fldMode+"'/>");
		    paramBuffer.append("<input type=\"hidden\" name=\"ignoreRights\" value='"+ignoreRights+"'/>");
		    paramBuffer.append("<input type=\"hidden\" name=\"parentId\" id=\"parentId\" value='"+parentId+"'/>");
		    paramBuffer.append("<input type=\"hidden\" name=\"childSystemId\" id=\"childSystemId\" value='"+childSystemId+"'/>");
		    paramBuffer.append("<input type=\"hidden\" name=\"responseId\" id=\"responseId\" value='"+responseId+"'/>");
			paramBuffer.append("<input type=\"hidden\" name=\"outputTarget\" value='"+outputTarget+"'/>");
			paramBuffer.append("<input type=\"hidden\" name=\"specimenPk\" value='"+specimenPk+"'/>");
			paramBuffer.append("<input type=\"hidden\" id=\"fdaStudy\" name=\"fdaStudy\" value='"+fdaStudy+"'/>");




		    if (ignoreRights.equals("false"))
		    {
     			paramBuffer.append("<br><table><tr><td>"+LC.L_Form_VersionNumber/*Form Version Number*****/+": "+formLibVerNumber+"</td></tr></table>");
     		}
     		else
			{
				paramBuffer.append("<input type=\"hidden\" name=\"completeStatus\" value='"+completeStatus+"'/>");
				paramBuffer.append("<input type=\"hidden\" name=\"pp_consenting\" value='"+pp_consenting+"'/>");
			}



			if (!mode.equals("N"))
		    {
		    	paramBuffer.append("<input type=\"hidden\" name=\"formFilledFormId\" value='"+filledFormId+"'/>");
		    	paramBuffer.append("<input type=\"hidden\" name=\"formLibVer\" value='"+formLibVer+"'/>");

			}


			//For CRF Forms
		    paramBuffer.append("<input type=\"hidden\" name=\"schevent\" value='"+schevent+"'/>");
		    paramBuffer.append("<input type=\"hidden\" name=\"fkStorageForSpecimen\" value='"+fkStorageForSpecimen+"'/>");

		    paramBuffer.append("<input type=\"hidden\" name=\"createNewSpecimen\" value='"+createNewSpecimen+"'/>");
		    paramBuffer.append("<input type=\"hidden\" name=\"eSignRequired\" value='"+eSignRequired+"'/>");



		   sbuffer.replace(pos,pos+len,"<input type=\"hidden\" name=\"mode\" value='"+mode+"'/> <input type=\"hidden\" name=\"chkexists\" value='"+chkexists+"'/>"+ paramBuffer.toString() + "  </Form>");
		   //sbuffer.replace(pos,pos+len,"<input type=\"hidden\" name=\"mode\" value='"+mode+"'/>  </Form>");
		   formHtml = sbuffer.toString();

		   if ((!"true".equals(ignoreRights)) && "L2_ON".equals(LC.L_Auth2_Switch)){
				findStr ="<td id=\"eSignLabel\" width=\"15%\">";
		  		pos = formHtml.lastIndexOf(findStr);
		  	    	  	
				if (pos > 0)
			  	{
					paramBuffer = new StringBuffer();
					paramBuffer.append("</tr><tr>");
					paramBuffer.append("<td width=\"15%\">").append(LC.L_UserName);
				 	paramBuffer.append("<FONT class=\"Mandatory\">* </FONT>");
					paramBuffer.append("</td>");
					paramBuffer.append("<td width=\"35%\"><input type=\"password\" id=\"userLogName\" name=\"userLogName\" value=\"\"");
					paramBuffer.append(" onkeyup=\"ajaxvalidate('misc:'+this.id,-1,'userLogNameMessage','"+ MC.M_Valid_UserName +"','"+ MC.M_Invalid_UserName +"','sessUserId')\"/>");
					paramBuffer.append("<span id=\"userLogNameMessage\"></span>");
					paramBuffer.append("</td>");
			
		  			sbuffer = new StringBuffer(formHtml);
					if(pageRight==4  || (pageRight == 6 && mode.equals("N")) || (pageRight < 6 && mode.equals("M")) || (entryChar.equals("O")&& mode.equals("M")) || (statSubType.equals("L")) || (saveDao.isLocked())|| saveDao.isSoftLock() && !saveDao.isAllowedUnlock() ){
		  				sbuffer.insert(pos,"");
			  		}else{
			  			sbuffer.insert(pos,paramBuffer.toString());
			  		}
		  			formHtml = sbuffer.toString();
		  	   	}
		 	}

	    if (!"N".equals(mode)){
			findStr ="<td width=\"15%\" id=\"def_form_status_label\">";
	  	    pos = formHtml.lastIndexOf(findStr);
	  	    	  	
		  	if (pos > 0)
	  	    {
		  		paramBuffer = new StringBuffer();
			  	paramBuffer.append("<hr/><td width=\"15%\">").append(LC.L_ReasonForChangeFDA);
			  	if (fdaStudy == 1){
			  		paramBuffer.append("<FONT class=\"Mandatory\">* </FONT>");
			  	}
			  	paramBuffer.append("</td>");
			  	if("jupiter".equals(dashBoard))
			  	{
			  		paramBuffer.append("<td width=\"25%\"><textarea id=\"remarks\" name=\"remarks\" rows=\"3\" cols=\"30\" MAXLENGTH=\"4000\"></textarea></td>");
			  	}
			  	else
			  	{
			  		paramBuffer.append("<td width=\"25%\"><textarea id=\"remarks\" name=\"remarks\" rows=\"3\" cols=\"50\" MAXLENGTH=\"4000\"></textarea></td>");
			  	}
			  	paramBuffer.append("</tr><tr>");
	
	  		    sbuffer = new StringBuffer(formHtml);
				if(pageRight==4  || (pageRight == 6 && mode.equals("N")) || (pageRight < 6 && mode.equals("M")) || (entryChar.equals("O")&& mode.equals("M")) || (statSubType.equals("L")) || (saveDao.isLocked()) || saveDao.isSoftLock() && !saveDao.isAllowedUnlock()  ){
	  		    	sbuffer.insert(pos,"");
	  		    }else{
	  		    	sbuffer.insert(pos,paramBuffer.toString());
	  		    }
	  		    formHtml = sbuffer.toString();
	  	    }
		  	if (mode.equals("M")){
			  	findStr ="onSubmit=\"";
			  	len = findStr.length();
		  	    pos = formHtml.lastIndexOf(findStr);
		  	    	  	
			  	if (pos > 0)
		  	    {
			  		paramBuffer = new StringBuffer();
			  		
			  		paramBuffer.append("onSubmit=\" var fdaVal = document.getElementById('fdaStudy').value; ");
			  		paramBuffer.append("if (fdaVal == '1'){");
			  		paramBuffer.append("	if (!(validate_col('"+LC.L_ReasonForChangeFDA+"',document.getElementById('remarks')))) return false; ");
			  		paramBuffer.append("}");
			  		
			  		sbuffer = new StringBuffer(formHtml);
					if(pageRight==4  || (pageRight == 6 && mode.equals("N")) || (pageRight < 6 && mode.equals("M")) || (entryChar.equals("O")&& mode.equals("M")) || (statSubType.equals("L")) || (saveDao.isLocked()) || saveDao.isSoftLock() && !saveDao.isAllowedUnlock()  ){
		  		    	sbuffer.replace(pos,pos+len,"");
		  		    }else{
		  		    	sbuffer.replace(pos,pos+len,paramBuffer.toString());
		  		    }
		  		    formHtml = sbuffer.toString();
		  	    }
		  	}
	    }

    %>
    
   
   <!--  New Enhancement for Span -->
    
    <% 

     if("jupiter".equals(dashBoard))
     {
    	 FormFieldDao fldDao=new  FormFieldDao();
    	 fldDao.getFormfieldDetails(StringUtil.stringToNum(formId));
     	ArrayList formFieldIds=fldDao.getFormFldId();
         ArrayList fldSystemId= fldDao.getFldSystemId();
         ArrayList formFldLinesNos=fldDao.getFormFldLinesNos();
     	ArrayList formFldDataType=fldDao.getFormFldDataType();
     	CodeDao cd2 = new CodeDao();
     	cd2.getCodeValues("form_query");
     	ArrayList subType=new ArrayList();
     	ArrayList subDesc=new ArrayList();
     	subType=cd2.getCSubType();
     	subDesc=cd2.getCDesc();
     	JSONObject formQueryJson = new JSONObject();
     	JSONObject formStatusJson = new JSONObject();
     	for(int i=0;i<cd2.getCRows();i++){
     		formQueryJson.put(subType.get(i).toString(),subDesc.get(i));	
     	}
     	CodeDao cd = new CodeDao();
     	cd.getCodeValues("query_status");
     	subType=cd.getCSubType();
     	subDesc=cd.getCDesc();
     	for(int i=0;i<cd.getCRows();i++){
     		formStatusJson.put(subType.get(i).toString(),subDesc.get(i));	
     	}
     	for(int i=0;i<fldSystemId.size();i++){
     	int findField=formHtml.indexOf(fldSystemId.get(i).toString());
     	if(findField>=0){
     		
     		//in case of you delete the field from form
     	
     	String strId="id=\"";
     	strId=strId+fldSystemId.get(i).toString();
     	String formFldDType=formFldDataType.get(i)==null?"":formFldDataType.get(i).toString();
     	int linesNo=formFldLinesNos.get(i)==null?0:Integer.parseInt(formFldLinesNos.get(i).toString());
     	if("MR".equals(formFldDType))
     	{
     		strId=strId+"_span";
     		int start=formHtml.indexOf(strId);
    		int clospan=formHtml.indexOf("<tr>",start);
    		int firstclosetd=formHtml.lastIndexOf("</td>",clospan);
     		String str4=formHtml.substring(0,firstclosetd);
     		String str5=formHtml.substring(firstclosetd,formHtml.length());
     		String rep="&nbsp;23153Flg";
     		formHtml=str4+rep+str5;	
     	}
     	
     	if("MC".equals(formFldDType))
     	{
     		//strId=strId+"_id";
     		strId=strId+"_span";
     		int start=formHtml.indexOf(strId);
    		int clospan=formHtml.indexOf("<tr>",start);
    		int firstclosetd=formHtml.lastIndexOf("</td>",clospan);
     		String str4=formHtml.substring(0,firstclosetd);
     		String str5=formHtml.substring(firstclosetd,formHtml.length());
     		String rep="&nbsp;23153Flg";
     		formHtml=str4+rep+str5;
     	}
     	
     	strId=strId+"\"";
         int indexof=formHtml.indexOf(strId);
         System.out.println("str-"+indexof);
         String str1=formHtml.substring(indexof+strId.length(),formHtml.length()-1);
         String indxCen="";
         int indexof1=0;
         if(formFldDType!=""){
         if("MD".equals(formFldDType)){
         	indxCen="</select>";
        	 indexof1=str1.indexOf(indxCen);
        }else if("MR".equals(formFldDType)){
     	   indxCen="</span>";
     	   indexof1=str1.indexOf(indxCen);
        }else if("MC".equals(formFldDType)){
     	   indxCen="</label>";
     	   indexof1=str1.indexOf(indxCen);
        }
        else if("ET".equals(formFldDType)&& linesNo>1){
     	   indxCen="</textarea>";
     	   indexof1=str1.indexOf(indxCen);
        }
         else{
     	    indxCen=">";
          indexof1=str1.indexOf(indxCen);
        }
         }
         String str2=formHtml.substring(0,indexof1+indexof+strId.length()+indxCen.length());
         String str3=formHtml.substring(str2.length(),formHtml.length()-1);
         StringBuffer sb=new StringBuffer(str2);
         String statusFlag="";
      	FormQueryDao fqDao=new FormQueryDao();
         fqDao.getCurrentQueryHistoryForField(fldSystemId.get(i).toString(),StringUtil.stringToNum(request.getParameter("filledFormId")));
         ArrayList formqueryIds=fqDao.getFormQueryIds();
     	ArrayList queryFieldIds=fqDao.getFieldIds();
     	
     	int patstudyforms=4;
     	FormQueryDao fqDao1=new FormQueryDao();
    	fqDao1.getQueryTypeAndStatus(formFieldIds.get(i).toString(),StringUtil.stringToNum(request.getParameter("filledFormId")),patstudyforms);
    	ArrayList queryTypes=fqDao1.getQueryType();
    	ArrayList queryStatus=fqDao1.getQueryStatus();
     	//ArrayList queryTypes=fqDao.getQueryType();
     	//ArrayList queryStatus=fqDao.getQueryStatus();
     	String queryStatusStr="";
    	String queryTypeStr="";
    	int redflag=0,orangeflag=0,yellowflag=0,blueflag=0,greenflag=0;
     	
    	if(queryFieldIds.size()>0){
    		
    		for(int count=0;count<queryTypes.size();count++){
				
				if((formStatusJson.get("open").toString().equalsIgnoreCase(queryStatus.get(count).toString())||formStatusJson.get("re-opened").toString().equalsIgnoreCase(queryStatus.get(count).toString())) && queryTypes.get(count).toString().equalsIgnoreCase(formQueryJson.get("high_priority").toString())){
					redflag=count+1;
					}
				else if(formStatusJson.get("mon_rev").toString().equalsIgnoreCase(queryStatus.get(count).toString()) && formQueryJson.get("high_priority").toString().equalsIgnoreCase(queryTypes.get(count).toString())){
					orangeflag=count+1;
					}
				else if(formStatusJson.get("mon_rev").toString().equalsIgnoreCase(queryStatus.get(count).toString()) && formQueryJson.get("normal").toString().equalsIgnoreCase(queryTypes.get(count).toString())){
					yellowflag=count+1;
					}
				else if((formStatusJson.get("open").toString().equalsIgnoreCase(queryStatus.get(count).toString())|| formStatusJson.get("re-opened").toString().equalsIgnoreCase(queryStatus.get(count).toString())) && queryTypes.get(count).toString().equalsIgnoreCase(formQueryJson.get("normal").toString())){
					blueflag=count+1;
					}
				else if(formStatusJson.get("resolved").toString().equalsIgnoreCase(queryStatus.get(count).toString())){
					greenflag=count+1;
					}
			}
				
				if(redflag!=0){
					queryTypeStr=queryTypes.get(redflag-1).toString();
					queryStatusStr=queryStatus.get(redflag-1).toString();
		    	}
				else if(redflag==0 && orangeflag!=0){
		    		queryTypeStr=queryTypes.get(orangeflag-1).toString();
					queryStatusStr=queryStatus.get(orangeflag-1).toString();
		    	}
				else if(redflag==0 && orangeflag==0 && yellowflag!=0){
		    		queryTypeStr=queryTypes.get(yellowflag-1).toString();
					queryStatusStr=queryStatus.get(yellowflag-1).toString();
		    	}
				else if(redflag==0 && orangeflag==0 && yellowflag==0 && blueflag!=0){
		    		queryTypeStr=queryTypes.get(blueflag-1).toString();
					queryStatusStr=queryStatus.get(blueflag-1).toString();
		    	}
				else if(redflag==0 && orangeflag==0 && yellowflag==0 && blueflag==0 && greenflag!=0){
		    		queryTypeStr=queryTypes.get(greenflag-1).toString();
					queryStatusStr=queryStatus.get(greenflag-1).toString();
		    	}
	
     		//queryStatusStr=queryStatus.get(0).toString();
     		/*for(int j=0; j<queryFieldIds.size();j++){
    			if(queryTypes.get(j)==null)
    				continue;
    			
    			queryTypeStr=queryTypes.get(j).toString();
    			break;
    		}*/
         //String queryStatusStr=queryStatus.get(0)==null?"":queryStatus.get(0).toString();
         //System.out.println("queryStatusStr-"+queryStatusStr);
        // String queryTypeStr=queryTypes.get(0)==null?"":queryTypes.get(0).toString();
         //System.out.println("queryTypeStr-"+queryTypeStr);
         	if(formFieldIds.get(i).equals(queryFieldIds.get(0))){
        		if((formStatusJson.get("open").toString().equalsIgnoreCase(queryStatusStr)||formStatusJson.get("re-opened").toString().equalsIgnoreCase(queryStatusStr))&& formQueryJson.get("high_priority").toString().equalsIgnoreCase(queryTypeStr)){
        			statusFlag="<span class='queryStatusFlagDlg' id=\""+fldSystemId.get(i)+"[Velsep]"+formqueryIds.get(0)+"\"><img border='0'onclick='openQueryDialog(\""+fldSystemId.get(i).toString()+"\")' src='./images/redflag.jpg'></span>";
        		}else if(formStatusJson.get("mon_rev").toString().equalsIgnoreCase(queryStatusStr)&&formQueryJson.get("normal").toString().equalsIgnoreCase(queryTypeStr)){
        			statusFlag="<span class='queryStatusFlagDlg' id=\""+fldSystemId.get(i)+"[Velsep]"+formqueryIds.get(0)+"\"><img border='0'onclick='openQueryDialog(\""+fldSystemId.get(i).toString()+"\")' src='./images/yellowflg.jpg'></span>";
        		}else if(formStatusJson.get("mon_rev").toString().equalsIgnoreCase(queryStatusStr)&&formQueryJson.get("high_priority").toString().equalsIgnoreCase(queryTypeStr)){
        			statusFlag="<span class='queryStatusFlagDlg' id=\""+fldSystemId.get(i)+"[Velsep]"+formqueryIds.get(0)+"\"><img border='0'onclick='openQueryDialog(\""+fldSystemId.get(i).toString()+"\")' src='./images/orangflg.jpg'></span>";
        		}else if((formStatusJson.get("open").toString().equalsIgnoreCase(queryStatusStr)||formStatusJson.get("re-opened").toString().equalsIgnoreCase(queryStatusStr)) && formQueryJson.get("normal").toString().equalsIgnoreCase(queryTypeStr)){
        			statusFlag="<span class='queryStatusFlagDlg' id=\""+fldSystemId.get(i)+"[Velsep]"+formqueryIds.get(0)+"\"><img border='0'onclick='openQueryDialog(\""+fldSystemId.get(i).toString()+"\")' src='./images/purplflg.jpg'></span>";
        		}else if(formStatusJson.get("resolved").toString().equalsIgnoreCase(queryStatusStr)){
        			statusFlag="<span class='queryStatusFlagDlg' id=\""+fldSystemId.get(i)+"[Velsep]"+formqueryIds.get(0)+"\"><img border='0'onclick='openQueryDialog(\""+fldSystemId.get(i).toString()+"\")' src='./images/greenflg.jpg'></span>";
        		}
         	}
        
     	}
         if(statusFlag.equals("")&& formFldDType!=""){
        	 statusFlag="<span class='queryStatusFlagDlg'><img border='0'onclick='openQueryDialog(\""+fldSystemId.get(i).toString()+"\","+"\"queryWin\""+")' src='./images/white.jpg'></span>";
         }
         	if((!"MR".equals(formFldDType))&&(!"MC".equals(formFldDType)))
         	{
         		sb.append(statusFlag);
         	}
        // sb.append("<span class='queryStatusFlagDlg'><img border='0'onclick='openQueryDialog(\""+fldSystemId.get(i).toString()+"\")' src='./images/redflag.jpg'></span>");
         formHtml=sb.toString()+str3;
         formHtml =  formHtml.replace("23153Flg",statusFlag);
     	}
     	}
    // formHtml = formHtml.replaceAll("<tr></tr>" ,"");
     //formHtml = formHtml.replaceAll("(\\r|\\n|\\r\\n)+", "");
     
     //formHtml = formHtml.replaceAll("<span>" ,"<span style='float:left;'>");
     
     //formHtml = formHtml.replaceAll("</span></td></tr>" ,"<span class='queryStatusFlagDlg'><img border='0' src='./images/redflag.jpg'></span></td></tr>");
     	int formEndTagIndex=formHtml.indexOf("</Form>");
	  	String htmlBeforeFormEndTag="";
	   	String htmlAfterFormEndTag="";
	    htmlBeforeFormEndTag=formHtml.substring(0,formEndTagIndex);
	    htmlAfterFormEndTag=formHtml.substring(formEndTagIndex,formHtml.length());
	    String hiddenStr="";
	    hiddenStr=hiddenStr.concat("<input name='dashBoard' type='hidden' value='").concat(dashBoard+"'>");
	    formHtml=htmlBeforeFormEndTag+hiddenStr+htmlAfterFormEndTag;
     }
    
     %>
 <%if(saveDao.isMonitor()){
	  if("jupiter".equals(dashBoard)){
		  formHtml=formHtml.replaceAll("hasDatepicker","");
		  teamRole="role_monit";
	  }
  }%>
    <%=formHtml%>


	 <%if(pageRight == 4){
			if (!calledFromForm.equals("dashboard")){%>
				<button type="button" onclick="window.history.back(); return false;"><%=LC.L_Back%></button>
			<%}
		}
	} //end of if body for page right
	else
	{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

  } //end of if for session

 else
 {  //else of if body for session
  %>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
 %>
<br>
<div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
<%if (calledFromForm.equals(""))
   {
%>




<DIV class="mainMenu" id = "emenu">
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>

<%
	}
%>
<script>
elementOverride=(document.getElementById("override_count"));
if (!(typeof(elementOverride)=="undefined") && (elementOverride!=null) ) {
if ((document.getElementById("override_count").value)>0) setDisableSubmit('false');
}




<% if (ignoreRights.equals("true"))
	{
%>
     if(document.forms['er_fillform1']){
	pp_consenting = document.forms['er_fillform1'].pp_consenting.value;



	if (! isEmpty(pp_consenting) && pp_consenting != null && pp_consenting != "null")
	{
		elemeConsRow = (document.getElementById("consenting_radio_tr"));

		if (!(typeof(elemeConsRow)=="undefined") && (elemeConsRow!=null) )
		{


				elemeConsRow.style.display = "block";
		}
	}

	elemeSign = (document.getElementById("eSign"));
	elemeSignLabel = (document.getElementById("eSignLabel"));

	if (!(typeof(elemeSign)=="undefined") && (elemeSign!=null) ) {
		elemeSign.style.visibility = "hidden";
		elemeSign.value="11";
	}

	if (!(typeof(elemeSignLabel)=="undefined") && (elemeSignLabel!=null) ) {

		elemeSignLabel.style.visibility = "hidden";
	}

	compStatus = document.forms['er_fillform1'].completeStatus.value;
	formFillMode = document.forms['er_fillform1'].formFillMode.value;

	//set override count to 0 so that query window does not open
	document.forms['er_fillform1'].override_count.value = 0;


	if (formFillMode == 'N')
	{
		document.forms['er_fillform1'].er_def_formstat.value = compStatus;
	}

	elem_def_formstat = (document.getElementById("er_def_formstat"));
	elem_def_form_status_label = (document.getElementById("def_form_status_label"));

	if (!(typeof(elem_def_formstat)=="undefined") && (elem_def_formstat!=null) )
	{
		elem_def_formstat.style.visibility = "hidden";

	}

	if (!(typeof(elem_def_form_status_label)=="undefined") && (elem_def_form_status_label!=null) )
	{
		elem_def_form_status_label.style.visibility = "hidden";

	}


	linkFormSubmit('fillform');
     }
<% }



if (eSignRequired.equals("0") && (!ignoreRights.equals("true")) )
{
  %>

	  	elemeSign = (document.getElementById("eSign"));
		elemeSignLabel = (document.getElementById("eSignLabel"));

		if (!(typeof(elemeSign)=="undefined") && (elemeSign!=null) ) {
			elemeSign.style.visibility = "hidden";
			elemeSign.value="11";
		}

		if (!(typeof(elemeSignLabel)=="undefined") && (elemeSignLabel!=null) ) {

			elemeSignLabel.style.visibility = "hidden";
		}
  <%

}
%>

</script>

<!-- Follwing javascript is executed for if parent-child link is implemented  -->
<script defer="defer">
  childSystemFld= document.getElementById("childSystemId");
  responseFld=document.getElementById("responseId");
  if (childSystemFld!=null)
 {
if (responseFld!=null) {

 if (childSystemFld.value.length>0){
 fldElement=document.getElementById(childSystemFld.value);
  if (fldElement.value.length==0)
 fldElement.value=responseFld.value;
 }
 }
  }
</script>

<%

 if ((! StringUtil.isEmpty(fkStorageForSpecimen)) || (! StringUtil.isEmpty(specimenPk) && mode.equals("M")) || showSpecimenDetails) { %>
	<script>
	var spanelem;

		spanelem = document.getElementById("spec_span");

		if (spanelem != null && spanelem!= undefined)
		{
		 	spanelem.style.display = 'block';
		 	formFillMode = document.forms['er_fillform1'].formFillMode.value;

		 	specimenPk = '';

		 	if (formFillMode == 'M')
		 	{
		 	  specimenPk = document.forms['er_fillform1'].specimenPk.value;
		 	}

		 	fkstorageforspecimen = document.forms['er_fillform1'].fkStorageForSpecimen.value ;
    //BK,FIX#5980-MAR-28-2010
	 	 /*  new VELOS.ajaxObject("getFormSpecimenData.jsp", {
	   		urlData:"fkStorageForSpecimen=" + fkstorageforspecimen + "&formFillMode="+formFillMode + "&specimenPk="+specimenPk  ,
			   reqType:"POST",
			   outputElement: spanelem }

	   ).startRequest();*/
	   }

	</script>
<% } %>



<script src='./dashboard/js/jquery-1.11.1.js'></script>
<script src='./dashboard/js/jquery-ui-1.11.0.js'></script>

<div id="queryStatusDialog">
	<div id='queryStatusDialogHtml'></div>
</div>

<script type="text/javascript">
       
	$("#queryStatusDialog").dialog({
	      autoOpen: false,
	      width : '410',
		  height:'175'  
      });
		


	function openQueryDialog(fieldSysId,windowType){
		$j.ajax({	  
	   		url: "fieldQueryHistory.jsp?filedId="+fieldSysId+"&windowType="+windowType+"&responseId="+"<%=filledFormId%>",   		
			success: function(data) {	
				$('#queryStatusDialog').dialog( "open" );			
				$("#queryStatusDialogHtml").html(data);			
				$('#queryStatusDialog').dialog( "open" );		
			}
					
	    	});
	}
	function saveQuery(fieldSysId){
		var fieldId='';
		var responseId="<%=filledFormId%>";
		var queryStatus=$("#cmbStatus").val();
		var queryTypeId=$("#cmbQueryResp").val();
		var instructions=$("#instructions").val();
		var index=fieldSysId.indexOf("er_def_date_01");	
		if(index>=0){
			$.ajax({
				 url: '/velos/jsp/findFieldDetails',
				 type: 'GET',
			     global: true,
			     dataType: 'json',
			     async: false,
			     data:{
				 'fieldId':fieldSysId,
				 'filledFormId':responseId,
				 'formType':'PS'
			     },
			     cache: false,
			     success: function (data) {
			    	 fieldId=data[0].er_def_date_01;
			     },
			     error: function (){
				        alert("error");
			        }
			});	
			
		}else{
		var fieldIdSegmentArr=fieldSysId.split('_');
		fieldId=fieldIdSegmentArr[2];
		}
		var formQueryType=2;
		var calledFrom=4;
		
		if(queryStatus==''){
			alert("Select Status");
		}
		else if(queryTypeId==''){
			alert("Select Query Type");
		}else{
		$.ajax({
			url: '/velos/jsp/patStudyQuerySave',
		    type: 'GET',
	        global: true,
	        dataType: 'json',
	        async: false,
	        data: {
	        'fieldId':fieldId,
	        'formQueryType':formQueryType,
	        'calledFrom':calledFrom,
	        'filledFormId':responseId,
	        'queryStatus':queryStatus,
	        'queryTypeId':queryTypeId,
	        'instructions':instructions
		},
		cache: true,
        success: function (data) {
	       
		        $('#queryStatusDialog').dialog('close');  
		        location.reload();
		        parent.preview1.location.reload();
        },
        error: function (){
	        alert("error");
        }
		});
		}
	}
	function saveQueryAndResponse(fieldSysId){
		var fieldStr='';
		var formQueryId='';
		var formQueryType=3;
		var instructions='';
		formQueryId =jQuery("input:radio[name=queryRadio]:checked").val();
		var queryStatus=$("#cmbStatus").val();
		instructions=$("#instructions").val();
		if(queryStatus!=''){
			if(formQueryId!=undefined){	
		$.ajax({
			    url: '/velos/jsp/saveQueryResponse',
			    type: 'GET',
		        global: true,
		        dataType: 'json',
		        async: false,
		        data: {
			    'formQueryId':formQueryId,
			    'queryStatus':queryStatus,
			    'formQueryType':formQueryType,
			    'instructions':instructions
		        },
		        cache: true,
		        success: function (data) {
			        if(data[0].rowImpact==0){
				        alert("data  not Saved Successfully");
			        }else{
				        $('#queryStatusDialog').dialog('close'); 
				        location.reload(); 
				        parent.preview1.location.reload(); 
			        }
		        },
		        error: function (){
			        alert("error");
		        }
		});
			}else{alert("Select Any one Selection");}
		}else{
			alert("Select Status");
		}
	}
	
	function formRespFldReadOnly(){
		$('#fillform input').attr('readonly', 'readonly');
		$('#eSign').attr('readonly', false);
		$(":radio").on("click", function(){
			  return false;
			});
		$(":checkbox").on("click", function(){
			  return false;
			});
		$('input[type="text"], textarea').attr('readonly','readonly');

		$("#fillform").find("select").each(function (i) {
			var selectId=$(this).attr('id');
			if(selectId !='er_def_formstat'){
				$(this).prop('disabled','disabled');
			}
			
		});
	 }

	if('<%=teamRole%>'=='role_monit'){
		formRespFldReadOnly();	
	}
		

</script>


</body>
</html>

<%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
%>

