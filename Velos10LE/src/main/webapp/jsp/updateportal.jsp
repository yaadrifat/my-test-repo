<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<!-- Bug#9941 29-May-2012 -Sudhir-->
<jsp:include page="include.jsp" flush="true"/>
<%@page import="com.aithent.audittrail.reports.EJBUtil,com.aithent.audittrail.reports.AuditUtils"%>
<HTML>
<HEAD>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="portalB" scope="page" class="com.velos.eres.web.portal.PortalJB" />
<jsp:useBean id="portalDsnB" scope="page" class="com.velos.eres.web.portalDesign.PortalDesignJB" />
<jsp:useBean id="statusB" scope="request" class="com.velos.eres.web.statusHistory.StatusHistoryJB"/>

<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,com.velos.esch.service.util.Rlog,java.text.*,com.velos.eres.service.util.*"%>
<%

	String eSign = request.getParameter("eSign");
	String portalName = request.getParameter("portalName").trim();
	String portalDesc = request.getParameter("portalDesc");
	String createdBy = request.getParameter("createdBy");
	String creatorId = request.getParameter("creatorId");

   if (creatorId.equals("") || creatorId == null)
               creatorId ="0";

	String portalPatPop = request.getParameter("portalPatPop");
	String emailNotify = request.getParameter("emailNotify");
	String portalId = request.getParameter("portalId");
	String histStatId = request.getParameter("histStatId");
	if (StringUtil.isEmpty(emailNotify ))
            emailNotify = "0";
    else
            emailNotify="1";


     String defPass = "";
     String createLoginsFlag = "";

    defPass = request.getParameter("defPass");
    defPass=(defPass==null)?"":defPass;
    createLoginsFlag = request.getParameter("createLoginsFlag");

    System.out.println("createLoginsFlag" + createLoginsFlag);

    if (StringUtil.isEmpty(createLoginsFlag ))
    {
            createLoginsFlag = "0";
         }

           //KN 3073
    	String portalPatPopStat = request.getParameter("popLevel");
		String mode = request.getParameter("mode");
	    String selStudyIds = request.getParameter("selStudyIds");
	    String selOrgIds = request.getParameter("selOrgIds");
	    HttpSession tSession = request.getSession(true);
	    String ipAdd = (String) tSession.getValue("ipAdd");

     	String usr = null;
	    int pid =0;

	   //JM:
	    String patId= null;
	    patId =request.getParameter("patientIds");

	   //code added for defect 2965 KN
	    String patmodFlag = request.getParameter("patmodFlag");
		String portalStat="";
	    usr = (String) tSession.getValue("userId");
	    String acc = (String) tSession.getValue("accountId");
	    String moduleId="";
        CodeDao codedao = new CodeDao();
	    int codeId=0;
	    String statusCodeId="";


		Date dt = new java.util.Date();
	    String stDate = DateUtil.dateToString(dt);

	    String resetFlag = request.getParameter("resetFlg");


	if (sessionmaint.isValidSession(tSession)) {
		%>
		<jsp:include page="sessionlogging.jsp" flush="true"/>
		<%
		String oldESign = (String) tSession.getValue("eSign");

		if(!oldESign.equals(eSign)) {
		%>
			<jsp:include page="incorrectesign.jsp" flush="true"/>
		<%
		} else {
			String accId = (String) tSession.getValue("accountId");
			String studyId = "";

			   if(mode.equals("M")) {
			 	portalB.setPortalId(EJBUtil.stringToNum(portalId));
				portalB.getPortalDetails();
			   }
			   portalB.setPortalName(portalName);
			   portalB.setAccountId(accId);
			   portalB.setPortalCreatedBy(creatorId);
			   portalB.setPortalDesc(portalDesc);

			   if(portalPatPop == null) {
			      portalPatPop = portalPatPopStat;
			  }

			   portalB.setPortalLevel(portalPatPop);
			   if (portalPatPop.equals("S"))
			   {
			   		int commaPos = -1;

			   		commaPos = selStudyIds.indexOf(",");


			   		if ( commaPos > 0)
			   		{
			   			studyId = selStudyIds.substring(0,commaPos);
			   		}
			   		else
			   		{
			   			studyId = selStudyIds;
			   		}

			   		portalB.setPortalStudy(studyId); //this is the fk_study being set in the er_portal
			   }
 			   //JM: added else condition in case, Study is not selected to persist in the er_portal.fk_study
			   else{
 				   portalB.setPortalStudy("");
	 		   }


		       portalB.setPortalNotificationFlag(emailNotify);
 			   portalB.setIpAdd(ipAdd);

			   portalB.setPortalCreateLogins(createLoginsFlag);
			   if (defPass.length()>0)  {defPass=Security.encryptSHA(defPass);
			   portalB.setPortalDefaultPassword(defPass);
			   }


			  if(mode.equals("N")) {
		          portalB.setPortalStatus("W");
			      portalB.setCreator(usr);
			      pid =portalB.setPortalDetails();
			  }

			  if(mode.equals("M")) {
			      portalB.setModifiedBy(usr);	//KM-Apr13
			      pid =portalB.updatePortal();
			  }




			int uId = -1;


			//JM: 21Aug2007: added: issue #3072
			 if (resetFlag.equals("1")){

			 try{
				//Modified for INF-18183 ::: Akshi
				 uId = portalDsnB.deletePortalDesignData(EJBUtil.stringToNum(portalId),AuditUtils.createArgs(session,"",LC.L_Manage_Acc));
			 }catch(Exception e){

				 Rlog.fatal("updateportal", "Error in  deleting rows, deletePortalDesignData()in updateportal.jsp page" + e);

			 }

			 }

//KM-to fix the issue 3039
if(pid >=0 )
 {

             if (portalPatPop.equals("S"))	{
				if (mode.equals("N")) {
				     portalB.insertPortalValues(pid,portalPatPop,selStudyIds,EJBUtil.stringToNum(usr),EJBUtil.stringToNum(usr),ipAdd);
				}
				if (mode.equals("M") && patmodFlag.equals("Y")) {
					//Modified for INF-18183 ::: Akshi
 			         portalB.deletePortalPop(EJBUtil.stringToNum(portalId),AuditUtils.createArgs(session,"",LC.L_Manage_Acc));
					 portalB.insertPortalValues(EJBUtil.stringToNum(portalId),portalPatPop,selStudyIds,EJBUtil.stringToNum(usr),EJBUtil.stringToNum(usr),ipAdd);
               }

			  }

			  if (portalPatPop.equals("O"))	{
                     if (mode.equals("N")){
				     portalB.insertPortalValues(pid,portalPatPop,selOrgIds,EJBUtil.stringToNum(usr),EJBUtil.stringToNum(usr),ipAdd);
				}
			        if (mode.equals("M")  &&  patmodFlag.equals("Y")) {
			        //Modified for INF-18183 ::: Akshi
			        portalB.deletePortalPop(EJBUtil.stringToNum(portalId),AuditUtils.createArgs(session,"",LC.L_Manage_Acc));
					portalB.insertPortalValues(EJBUtil.stringToNum(portalId),portalPatPop,selOrgIds,EJBUtil.stringToNum(usr),EJBUtil.stringToNum(usr),ipAdd);
		   	  }
			  }

			  if (portalPatPop.equals("A"))	{
				if (mode.equals("N")){
				    portalB.insertPortalValues(pid,portalPatPop,acc,EJBUtil.stringToNum(usr),EJBUtil.stringToNum(usr),ipAdd);
				}
			        if (mode.equals("M") && patmodFlag.equals("Y")) {
			        //Modified for INF-18183 ::: Akshi
					portalB.deletePortalPop(EJBUtil.stringToNum(portalId),AuditUtils.createArgs(session,"",LC.L_Manage_Acc));
					portalB.insertPortalValues(EJBUtil.stringToNum(portalId),portalPatPop,acc,EJBUtil.stringToNum(usr),EJBUtil.stringToNum(usr),ipAdd);
			   }

			  }

			  if (portalPatPop.equals("P"))	{
			       if (mode.equals("N"))
				   portalB.insertPortalValues(pid,portalPatPop,patId,EJBUtil.stringToNum(usr),EJBUtil.stringToNum(usr),ipAdd);
                                if (mode.equals("M") && patmodFlag.equals("Y")) {
                                	//Modified for INF-18183 ::: Akshi
                                   portalB.deletePortalPop(EJBUtil.stringToNum(portalId),AuditUtils.createArgs(session,"",LC.L_Manage_Acc));
				   portalB.insertPortalValues(EJBUtil.stringToNum(portalId),portalPatPop,patId,EJBUtil.stringToNum(usr),EJBUtil.stringToNum(usr),ipAdd);
				}
			  }

			codeId=codedao.getCodeId("portalstatus","W");
			statusCodeId=codeId+"";
			if(mode.equals("N")) {
    			   moduleId=pid+"";
			}
			statusB.setStatusModuleId(moduleId);
			statusB.setStatusModuleTable("er_portal");
			statusB.setStatusCodelstId(statusCodeId);
			statusB.setStatusStartDate(stDate);
			statusB.setIpAdd(ipAdd);
			statusB.setIsCurrentStat("1");

			if(mode.equals("N")) {
			     statusB.setCreator(usr);
			     statusB.setRecordType("N");
			     statusB.setStatusHistoryDetailsWithEndDate();
            }

%>
  <br>
  <Br>
  <br>
  <Br>
  <p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
  <META HTTP-EQUIV=Refresh CONTENT="1; URL=portal.jsp?mode=M">
<%
} else {

	String errMessage = "";

	if (pid == -3 )
	   {
	   	errMessage =MC.M_PortalDupl_BackChgName/*"Portal Name is duplicate. Please click on \"Back\" Button to change the name"*****/;
	   }
	else if (pid == -4)
	{
		errMessage =MC.M_AldyPortal_MngChkBack/*"You already have a portal with option : 'Provide Create "+LC.Pat_Patients+" Login button in Manage Logins' checked. Please click on \"Back\" Button to uncheck the option"*****/;
	}
	else
	{
		errMessage =MC.M_DataNotSvd_Succ/*"Data was not saved successfully"*****/;
	}
%>
<br>
  <Br>
  <br>
  <Br>

  <br><br><br><br><br><p class = "successfulmsg" align = center>
		 <%=errMessage%> <Br><Br>
		 <!-- Bug#9941 29-May-2012 -Sudhir-->
		<button name="back"  onclick="window.history.back();return false;"><%=LC.L_Back%></button></p>


<%

}

// end of if 			//}

		}// end of if body for e-sign
	}//end of if body for session
	else {
	%>
		<jsp:include page="timeout.html" flush="true"/>
	<%
	}
	%>
	 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</BODY>
</HTML>