<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<Link Rel=STYLESHEET HREF='./styles/yuilookandfeel/yuilookandfeel.css' type=text/css>
<%@ page language = "java" import = "java.util.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,java.sql.*,com.velos.eres.business.common.*,com.velos.eres.web.user.UserJB"%>
<%!
private String htmlEncode(String str) {
    return StringUtil.htmlEncodeXss(StringUtil.trueValue(str))
    .replaceAll("\\(","&#40;").replaceAll("\\)","&#41;");
}
private String htmlDecode(String str) {
    return StringUtil.htmlDecode(StringUtil.trueValue(str))
    .replaceAll("&#40;","\\(").replaceAll("&#41;","\\)").replaceAll("&quot;","\"");
}
private static final String LAST_CRITERIA = "LAST_CRITERIA";
private static final String LAST_SEARCH_STR = "LAST_SEARCH_STR";
private static final String CURROUT = "currout";
private static final String EMPTY_STRING = "";
%>
<%
try {
	if (!sessionmaint.isValidSession(request.getSession(false))) {
		out.println(EMPTY_STRING);
		return;
	}
} catch(Exception e) {}
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<script>
function resetGetLookupInput() {
	document.lookuppg.search_data.value = '';
	$('filter_criteria').innerHTML = '<%=LC.L_None%>';
	document.lookuppg.criteria_str.value = '<%=LC.L_None%>';
	document.lookuppg.lookup_filter.value = 'entlookup';
	document.lookuppg.lookup_column.selectedIndex = 1;
}
function loadOrgLookupResults(targetId) {
	resetGetLookupInput();
	document.lookuppg.targetId.value = targetId;
	$j.post('getlookupmodalresults.jsp',
			{ 
				viewId:"6014", keyword:"selorgId|site_name~paramorgId|pk_site|[VELHIDE]",
				dfilter:"orgLookup", targetId:targetId
			},
			function(data) {
				$j('#getLookupModalResultsDiv').html(data);
			}
	);
}
function reloadLookupResults() {
	if (!createString(document.lookuppg)) { return false; }
	$j.post('getlookupmodalresults.jsp',
			$j('#lookuppg').serialize(),
			function(data) {
				$j('#getLookupModalResultsDiv').html(data);
			}
	);
	return false;
}
function navigateLookupResults(nextUrl) {
	$j.post(nextUrl+'&search=[VEL_NAV]',
			$j('#lookuppg').serialize(),
			function(data) {
				$j('#getLookupModalResultsDiv').html(data);
			}
	);
	return false;
}
function validate(formobj){
	if (formobj.lookup_column.value == '0') {
		alert("<%=MC.M_Selc_AnOpt%>");
		formobj.lookup_column.focus();
		return false;
	}
	if (formobj.search_data.value && formobj.search_data.value.match(/^\s+$/)) {
		alert("<%=MC.M_EtrValueFor_ContSrch%>");
		formobj.search_data.value = '';
		formobj.search_data.focus();
		return false;
	}
	return true;
}
function createString(formobj){
	if (!validate(formobj)) { return false; }
	var lcriteria = formobj.criteria_str.value; // lcriteria = shorthand notation for formobj.criteria_str.value
	if (lcriteria == "<%=LC.L_None%>") { lcriteria=""; }
	
	// For blank search_data, treat it as Entire Lookup with no filter
	if (formobj.search_data.value == '')  {
		formobj.lookup_filter.value = 'entlookup';
		if (formobj.lookup_column.options.length > 1) {
			formobj.lookup_column.selectedIndex = 1;
		}
		formobj.search_data_val.value = '';
		formobj.search.value = '';
		formobj.criteria_str.value = "<%=LC.L_None%>";
		$('filter_criteria').innerHTML = formobj.criteria_str.value;
		return true;
	}
	
	// Entire Lookup with filter on search word
	if (formobj.lookup_filter.value == 'entlookup'){
		lcriteria = "["+formobj.lookup_column.options[formobj.lookup_column.selectedIndex].text +" "
				+ formobj.scriteria.options[formobj.scriteria.selectedIndex].text +" <b>" +formobj.search_data.value+"</b>]";
		formobj.criteria_str.value = lcriteria;
		$('filter_criteria').innerHTML = formobj.criteria_str.value;
		return true;
	}
	
	// Lookup Filter is "Current Output" below
	var array1 = lcriteria.match(/\] AND \[/g);
	if (array1 && array1.length > 9) {
		alert("<%=MC.Lkp_TooManyAnd%>");
		return false;
	}
	if (formobj.lookup_column.value!="0") {
		var lstr; // temporary string for contatenation
		lstr = "["+formobj.lookup_column.options[formobj.lookup_column.selectedIndex].text +" " 
				+ formobj.scriteria.options[formobj.scriteria.selectedIndex].text +" <b>" +formobj.search_data.value+"</b>]" ;	
		if (lcriteria.length>0){ 
			lcriteria=lcriteria + " AND " +lstr; 
		} else {
			lcriteria = lstr; 
		}
	}
	formobj.criteria_str.value=lcriteria;
	$('filter_criteria').innerHTML = formobj.criteria_str.value;
	return true;
}
 //orderBy column number
 function setOrder(formObj,orderBy,reset){
	var lorderType,lsearch_data,lscriteria;
	var llook_column;
	if (reset == 'Y') {
		formObj.orderType.value= "asc";
		lorderType = "asc";
	} else {
		if (formObj.orderType.value=="asc") {
			formObj.orderType.value= "desc";		
			lorderType="desc";
		} else 	if (formObj.orderType.value=="desc") {
			formObj.orderType.value= "asc";
			lorderType="asc";
		}
	}
	lsearch_data=formObj.search_data_val.value; 
	llook_column=formObj.lookup_column_val.value; 
	lscriteria=formObj.scriteria_val.value;
	if (orderBy) {
		formObj.orderBy.value = orderBy;
	}
	
	var serializedArgs = $j('#'+formObj.id).serialize()+
		"orderBy="+orderBy+"&orderType="+lorderType+
		"&lookup_column="+llook_column+"&scriteria="+lscriteria+"&search_data="+lsearch_data;
	$j.post('getlookupmodalresults.jsp',
			serializedArgs,
			function(data) {
				$j('#getLookupModalResultsDiv').html(data);
			}
	);
	return false;
}
function setValue(formobj,count,flag){
	var outputStr;
	if (!(flag=="remove")) {
		if (formobj.totStr.value) {
			outputStr=decodeString(formobj.totStr.value);
		} else if (formobj.totalrows.value>1){
			outputStr=decodeString(formobj.totStr[count-1].value);
		}
	} else if (flag=="remove") {
		outputStr=decodeString(formobj.outputString.value);
	}
	var valueStr="",keyword="";
	var lform=fnTrimSpaces(formobj.form.value);
	arrayStr=outputStr.split("[end]");    
	var data,lfdatacolumn,startIndex,endIndex,index,tempKey,tempvalue,tempdata,advDictionary;  
	for (var i=0;i<arrayStr.length;i++) {
		if (flag=="remove") {
			startIndex=arrayStr[i].indexOf("[field]")+ 7;
			endIndex=arrayStr[i].indexOf("[keyword]");
			lfdatacolumn=arrayStr[i].substring(startIndex,endIndex);
			valueStr = "remove";
		} else {
			data="";
			startIndex=arrayStr[i].indexOf("[field]")+ 7;
			endIndex=arrayStr[i].indexOf("[keyword]");
			lfdatacolumn=arrayStr[i].substring(startIndex,endIndex);
			startIndex=endIndex+9;
			endIndex=arrayStr[i].indexOf("[dbcol]");
			keyword=arrayStr[i].substring(startIndex,endIndex);	
			if (keyword.indexOf("[VELEXPR]")>=0){
				valuearray=keyword.split("*");
				for (var j=1;j<valuearray.length;j++){
					valuearray[j]=fnTrimSpaces(valuearray[j].replace('[',''));
					valuearray[j]=fnTrimSpaces(valuearray[j].replace(']',''));
					if ((valuearray[j].indexOf("=")) >=0) { 
						tempKey=valuearray[j].substring(0,valuearray[j].indexOf("="));
					} else { tempKey=valuearray[j]; }
					tempvalue=valuearray[j].substring(valuearray[j].indexOf("=")+1,valuearray[j].length);
					if (tempKey=="VELSTR")     data=data+tempvalue;
					if (tempKey=="VELSPACE") data=data+' ';
					if (tempKey=="VELCRET") data=data+"\r";
					if (tempKey=="VELNLINE") data=data+"\n";
					if (tempKey=="VELKEYWORD"){
						index=valueStr.indexOf(tempvalue);
						tempdata=valueStr.substring(index+tempvalue.length+1,valueStr.indexOf("]",index));
						data=data+tempdata;
					}	
				}
			} else {
				startIndex=arrayStr[i].indexOf("[value]")+7 ;
				endIndex=arrayStr[i].length ;
				data=arrayStr[i].substring(startIndex,endIndex);
			}
			if (data=="null") { data=""; }
			// alert(data);
			valueStr=valueStr+"["+keyword+"="+data+"]";
		}
	}
	fetchLookupDetails(formobj.targetId.value, valueStr);
}

function showoutput(form,count) {
	alert(form.totstr[count].value);
}
</script>

<%
	String ignoreRights = "";
	long startPage = 1;
	long cntr = 0;
	String tempSearch="";
	LookupDao lookupDao = new LookupDao();
	String ddlookup ="";
	String orderBy = "";
	orderBy = request.getParameter("orderBy");
	String orderType = "";
	orderType = request.getParameter("orderType");
	if (orderType == null){
		orderType = "asc"; /*YK 03JAN -  BUG # 5710*/
	}
	if (orderBy==null){
		orderBy="1";
	}
	String orderBy_str="";
	if (!orderBy.equals("") ){
		orderBy_str = " order by " + orderBy+" " +orderType; 
	}else {
		orderBy_str =  "";
	}
	String search=request.getParameter("search"); 
	if (search==null) search=""; else search=search.replace('~','%');
	String fDatacolumn=request.getParameter("fdatacolumn");
	String fDispcolumn=request.getParameter("fdispcolumn");
	String fCalculate = request.getParameter("fcalculate");
	String fCalcexpr = request.getParameter("fcalcexpr");
	String fkeyword_str=request.getParameter("keyword");
	String dfilter=FilterUtil.validateDFilter(request);
	String dfilterReq=request.getParameter("dfilter");
	if (dfilter!=null){
		if (dfilter.length()>0){
			dfilter=dfilter.replace('~','%');
		}
	} //to be check and remove unwanted bracket
	String search_data=request.getParameter("search_data");
	String search_data_req=request.getParameter("search_data");
	String search_data_val=request.getParameter("search_data_val");
	if ((search_data_req == null || search_data_req.length() == 0)
	        && search_data_val != null && search_data_val.length() > 0) {
	    search_data_req = htmlDecode(search_data_val);
	    search_data = htmlDecode(search_data_val);
	}
	if (search_data==null) search_data="";
	search_data=search_data.toLowerCase();
	search_data=FilterUtil.sanitizeText(search_data);
	String scriteria=request.getParameter("scriteria");
	if (scriteria==null) scriteria="";
	String lookup_column="",dfilter_search="",search_str="",criteria_str="";	
	String lookup_column_val=request.getParameter("lookup_column");  
	criteria_str=request.getParameter("criteria_str");
	if (criteria_str==null) criteria_str="";
	if (criteria_str.length() == 0) {
	    search_data_req = search_data = "";
	}
	HttpSession tSession = request.getSession(true); 
	int vlookup=0;
	String userId = (String) tSession.getValue("userId");
	String accountId=(String) tSession.getValue("accountId");
	UserJB user = (UserJB) tSession.getValue("currentUser");	
	String orgId=user.getUserSiteId();;
	orgId=(orgId==null)?"":orgId;
	if (lookup_column_val!=null){
		if ((lookup_column_val.equals("0")) || (lookup_column_val.length()==0)){
			vlookup = 0;
		} else {				
			vlookup = EJBUtil.stringToNum((request.getParameter("lookup_column"))
					.substring(0,((request.getParameter("lookup_column")).indexOf("~"))));
		}
	}
	int pageRight = 0,index=-1;
	String outputStr="", tempOutputStr="" ,viewName="";
	ignoreRights = (String) tSession.getValue("pp_ignoreAllRights") ;
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
	if (grpRights!=null) {	
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));
	}
	
	//for patient portal
	if (StringUtil.isEmpty(ignoreRights)) {
		ignoreRights = "false";
	}
	if (ignoreRights.equals("true")) {
		pageRight = 7;
		accountId=(String) tSession.getValue("pp_accountId");
	}
		
	String viewId=(request.getParameter("viewId"));
	viewId=(viewId==null)?"":(viewId);
	String viewNameKeyword = request.getParameter("viewNameKeyword");
	if (!StringUtil.isEmpty(viewNameKeyword)) {
		String viewIdNew = lookupDao.getViewIdByViewKeyword(viewNameKeyword);
		if (StringUtil.stringToNum(viewIdNew) > 0) {
			viewId = viewIdNew;
		}
	}
	if (viewId.length()==0)	{
		viewName=request.getParameter("viewName");
		System.out.println("viewName"+viewName);		
		viewId=lookupDao.getViewId(viewName);
	}
	lookupDao.getViewDetail(EJBUtil.stringToNum(viewId));	
	String viewFilter=lookupDao.getViewFilter();
	viewFilter=(viewFilter==null)?"":viewFilter;
	if (viewFilter.indexOf("[:ACCID]")>=0) 
		viewFilter=StringUtil.replace(viewFilter,"[:ACCID]",accountId);
	if (viewFilter.indexOf("[:USERID]")>=0) 
		viewFilter=StringUtil.replace(viewFilter,"[:USERID]",userId);
	if (viewFilter.indexOf("[:ORGID]")>=0) 
		viewFilter=StringUtil.replace(viewFilter,"[:ORGID]",orgId);	
	//System.out.println("viewid in getlookup.jsp"+viewId);
	lookupDao.getLookupView(viewId,"lookup_column",vlookup,"");
	lookupDao.getReturnValues(viewId);
	ddlookup = lookupDao.getLookupDropDown();
	String retDisp=lookupDao.getRetDispValue();
	String retData=lookupDao.getRetDataValue();
	ArrayList lKeyword=lookupDao.getLViewRetKeywords();
	ArrayList lColumns=lookupDao.getLViewRetColumns();
	ArrayList lkpCollen=lookupDao.getLViewColLen();
	ArrayList lkpIsDisp=lookupDao.getLViewIsDisplay();
	ArrayList retKeyword=new ArrayList();
	ArrayList retFields=new ArrayList();
	ArrayList urlKeywords=new ArrayList();
	ArrayList urlFields=new ArrayList();
	ArrayList dbKeywords=lookupDao.getLViewRetKeywords();
	ArrayList dbColumns=lookupDao.getLViewRetColumns();
	ArrayList genOutput=new ArrayList();
	ArrayList tempCol=new ArrayList();
	//populate return keywords arraylist
	StringTokenizer st_sep=null;
	StringTokenizer st = new StringTokenizer(fkeyword_str,"~");
	while (st.hasMoreTokens()) {
		st_sep= new StringTokenizer(st.nextToken(),"|");
		urlFields.add(st_sep.nextToken());
		urlKeywords.add((String)st_sep.nextToken());		 
	}
	
	for (int i=0;i<urlKeywords.size();i++)	{
		//System.out.println(i + (String)urlKeywords.get(i));	
		if ((dbKeywords.contains(urlKeywords.get(i))) || (((String)urlKeywords.get(i))).indexOf("[VELEXPR]") >= 0) {
			index=dbKeywords.indexOf(urlKeywords.get(i));
			//System.out.println(index);
			if ( index>=0 ) {
				outputStr="[field]"+urlFields.get(i)+"[keyword]"+urlKeywords.get(i)+"[dbcol]"+dbColumns.get(index);
				//System.out.println("adding value"+dbColumns.get(index));
				tempCol.add(((String)dbColumns.get(index)).toLowerCase());
			} else {
				outputStr="[field]"+urlFields.get(i)+"[keyword]"+urlKeywords.get(i)+"[dbcol]"+"[VELEXPR]";
				tempCol.add("[VELEXPR]");
			}  
			genOutput.add(outputStr);
		}  
	 }
	 //create seach string
	if (lookup_column_val!=null) {
		if ((lookup_column_val.equals("0")) || (lookup_column_val=="")) {}
		else{
			lookup_column=lookup_column_val.substring(((request.getParameter("lookup_column")).indexOf("~"))+1);
		}
	}
		
%>
<div id="lookupframe" style="height:44em"> 
<div id="lookuptop">
<br/>
<Form name="lookuppg" id="lookuppg" method="post" action="#" onsubmit="return reloadLookupResults();">
	<P class="sectionHeadings">&nbsp;&nbsp; <%=lookupDao.getViewName()%> </P>
	<input name="viewname" type="hidden" value="<%=lookupDao.getViewName()%>"/>
	<table width="99%" cellspacing="2" cellpadding="2" border="0" class="basetbl midalign">
		<tr>
			<td style="margin-top:2"><P class="defComments"><%=LC.L_Search%></P></td>
			<td><select size="1" name="lookup_filter"><option selected value="entlookup"><%=LC.L_Entire_Lookup%></option>
				<option value="currout"><%=LC.L_Cur_Output%></option></select></td>
			<td><%=ddlookup%></td>
 			<td><select size="1" name="scriteria">
		  		<option value="contains" <%if (scriteria.equals("contains")){%> Selected <%}%>><%=LC.L_Contains%></option>
  				<option value="isequalto" <%if (scriteria.equals("isequalto")){%> Selected <%}%>><%=LC.L_Is_EqualTo%></option>
			  	<option value="start" <%if (scriteria.equals("start")){%> Selected <%}%>><%=LC.L_Begins_With%></option>
			  	<option value="ends" <%if (scriteria.equals("ends")){%> Selected <%}%>><%=LC.L_Ends_With%></option>
			  	</select>
			</td>	
			<td width="20"><input name="search_data" type="text" ></td>
			<td width="10" height="2em" valign="bottom" style="margin-top:4; padding-top:8; margin-right:0; padding-right:0">
				<button style="height:2em" type="submit" onClick="return validate(document.lookuppg)"><%=LC.L_Search%></button>
			</td>
			<td width="40" style="margin-left:0; padding-left:0">
				<a href="javascript:void(0);" onClick="setOrder(document.lookuppg,'1','Y')" style="font-size:8pt; width:40;" ><%=LC.L_Reset_Sort%></a>
			</td>
		</tr>		
	</table>    	
	<%if (criteria_str.length() == 0) { criteria_str = LC.L_None; } %>
	<table>
		<tr>
			<td colspan="7">
			<P class = "defComments"><%=LC.L_Filter_Criteria%>:&nbsp;&nbsp;
			<span id="filter_criteria"><%=htmlEncode(criteria_str).replaceAll("&lt;/b&gt;","</b>").replaceAll("&lt;b&gt;","<b>")%></span>&nbsp;&nbsp;
			</P>
			</td>
		</tr>	
	</table>
	<Input type="hidden" name="targetId" id="targetId" value="">
    <input name="viewId" type="hidden" value="<%=StringUtil.htmlEncodeXss(viewId)%>">
	<Input type="hidden" name="orderBy" value="<%=orderBy%>">
	<Input type="hidden" name="orderType" value="<%=orderType.equals("")?"asc":orderType%>">
	<input type="hidden" name="criteria_str" value="<%=htmlEncode(criteria_str)%>">
	
    <input name="keyword" type="hidden" value="<%=fkeyword_str%>">
    <% if (lookup_column_val==null) lookup_column_val="";%>
    <input name="lookup_column_val" type="hidden" value="<%=lookup_column_val%>">	
    <input name="search_data_val" type="hidden" value="<%=htmlEncode(search_data_req)%>">		
    <input name="scriteria_val" type="hidden" value="<%=scriteria%>">		
    <input name="fdatacolumn" type="hidden" value="<%=fDatacolumn%>">		
    <input name="fdispcolumn" type="hidden" value="<%=fDispcolumn%>">		
   	<input name="dfilter" type="hidden" value="<%=StringUtil.htmlEncodeXss(dfilterReq).replace('%','~').replaceAll("\\(","&#40;").replaceAll("\\)","&#41;")%>">			
	<input name="search" type="hidden" value="">
    <input name="fcalculate" type="hidden" value="<%=fCalculate%>">			
    <input name="fcalcexpr" type="hidden" value="<%=fCalcexpr%>">
</Form>
</div>
<div id="getLookupModalResultsDiv">
	<br/>
	<p><%=LC.L_Loading%><img class="asIsImage_progbar" src="../images/jpg/loading_pg.gif" /></p>
	<br/>
</div>
</div>