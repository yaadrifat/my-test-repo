<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_Pat_TreatArmSubm%><%--<%=LC.Pat_Patient%> Treatment Arm Submit*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<%@page import="com.velos.eres.audit.web.AuditRowEresJB"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>


<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="patTXArmJB" scope="request"  class="com.velos.eres.web.patTXArm.PatTXArmJB"/>
<body><%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@ page import="com.velos.eres.service.util.*"%>
<DIV class="popDefault" id="div1">

<%
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{

%>

		<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%
		String eSign= request.getParameter("eSign");
    	String oldESign = (String) tSession.getValue("eSign");
	   if(oldESign.equals(eSign))
	   {


			int errorCode=0;
			String mode=request.getParameter("mode");
			if(mode==null)
			mode="";



			String studyId="";
			String patTXArmId = "";

		String patProtId=request.getParameter("patProtId");

  		String stdtxtarm= request.getParameter("stdtxtarm");

  		String drugInfo= request.getParameter("drugInfo");

  		String startDate= request.getParameter("startDate");
		String endDate= request.getParameter("endDate");
		String notes= request.getParameter("notes");



  		String accountId=(String)tSession.getAttribute("accountId");
  		int tempAccountId=EJBUtil.stringToNum(accountId);

			String creator="";
			String ipAdd="";

			creator=(String) tSession.getAttribute("userId");
			ipAdd=(String)tSession.getAttribute("ipAdd");
	     	accountId=(String)tSession.getAttribute("accountId");
			studyId=request.getParameter("studyId");




			patTXArmJB.setStudyTXArmId(stdtxtarm);
			patTXArmJB.setPatProtId(patProtId);
			patTXArmJB.setTXDrugInfo(drugInfo);
			patTXArmJB.setStartDate(startDate);
			patTXArmJB.setEndDate(endDate);
			patTXArmJB.setNotes(notes);


			if (mode.equals("N"))
			{
				patTXArmJB.setCreator(creator);
				errorCode=patTXArmJB.setPatTXArmDetails();
			}
			else if (mode.equals("M"))
			{
			    patTXArmId = request.getParameter("patTXArmId");


				patTXArmJB.setPatTXArmId(EJBUtil.stringToNum(patTXArmId));
				patTXArmJB.getPatTXArmDetails();
				patTXArmJB.setStudyTXArmId(stdtxtarm);
    			patTXArmJB.setPatProtId(patProtId);
    			patTXArmJB.setTXDrugInfo(drugInfo);
    			patTXArmJB.setStartDate(startDate);
    			patTXArmJB.setEndDate(endDate);
    			patTXArmJB.setNotes(notes);
				patTXArmJB.setModifiedBy(creator);
				errorCode = patTXArmJB.updatePatTXArm();
		 }


		if ( errorCode == -2  )
		{
%>

				<br><br><br><br><br>
	 			<p class = "sectionHeadings" ><%=MC.M_Data_NotSvdSucc%><%--Data not saved successfully*****--%> </p>

<%

		} //end of if for Error Code

		else

		{
			if (mode.equals("M")){
				String remarks = request.getParameter("remarks");
				if (!StringUtil.isEmpty(remarks)){
					int userId = StringUtil.stringToNum((String)tSession.getAttribute("userId"));
					AuditRowEresJB auditJB = new AuditRowEresJB();
					auditJB.setReasonForChangeOfTxArm(patTXArmJB.getPatTXArmId(), userId, remarks);
				}
			}
%>
		<br><br><br><br><br>
		<p class = "sectionHeadings"><%=MC.M_Data_SavedSucc%><%--Data Saved Successfully*****--%> </p>
	<script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
		</script>

<%
		}//end of else for Error Code Msg Display
	}//end of if old esign
	else{
%>
		<jsp:include page="incorrectesign.jsp" flush="true"/>
<%
			}//end of else of incorrect of esign
     }//end of if body for session

	else
	{

%>
		  <jsp:include page="timeout.html" flush="true"/>
<%

} //end of else of the Session


%>

</body>

</html>
