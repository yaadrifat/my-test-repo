<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Patient_Facility%><%--<%=LC.Pat_Patient%> Facility*****--%></title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,java.text.*" %>
<%@ page import="com.velos.eres.business.common.*,com.velos.eres.service.util.*,com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.user.*"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>

<jsp:include page="popupJS.js" flush="true"/>
<SCRIPT Language="javascript">

function removeSpeciality(formobj)
{
	formobj.txtSpeciality.value="";
	formobj.selSpecialityIds.value="";

}


function  validate(formobj){

     if (!(validate_date(formobj.regdate))) return false
	 if (!(validate_col('organization',formobj.patorganization))) return false
     if(!(validate_col('Pat Facility ID',formobj.patFacilityID))) return false
		  val = formobj.patFacilityID.value;
	 if(val.indexOf("%")!=-1)
		  {
		 var paramArray = ["%"];
			alert(getLocalizedMessageString("M_SplCharNotAllowed", paramArray));alert("Special Character(%) not allowed for this Field");
			  formobj.patFacilityID.focus();
			  return false;
		  }

	if (!(validate_col('e-Signature',formobj.eSign))) return false
	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }

}

function openwinUser() {
    windowName = window.open("usersearchdetails.jsp?fname=&lname=&from=patient","User","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
	windowName.focus();
}

function openSpecialityWindow(formobj) {

	specialityIds = formobj.selSpecialityIds.value;

	if(specialityIds=="null")
	{
		 specialityIds="";

	}
	windowName=window.open("multiselectcodepopup.jsp?selectedIds="+specialityIds+"&openerForm=patient&hidEleName=selSpecialityIds&eleName=txtSpeciality&codeType=prim_sp&ptitle=<%=LC.L_Speciality%>&heading=<%=LC.L_Mng_Pats%>","Specility","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=300");
	/*windowName=window.open("multiselectcodepopup.jsp?selectedIds="+specialityIds+"&openerForm=patient&hidEleName=selSpecialityIds&eleName=txtSpeciality&codeType=prim_sp&ptitle=Speciality&heading=Manage Patients","Specility","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=300");*****/

	windowName.focus();


}

</SCRIPT>




<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="usrSite" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="patFacility" scope="request" class="com.velos.eres.web.patFacility.PatFacilityJB"/>
<jsp:useBean id="CommonB" scope="page" class="com.velos.eres.web.common.CommonJB" />
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
	<jsp:include page="include.jsp" flush="true"/>
<body>
<DIV class="popDefault" id="div1">

  <%
	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))
	{
		String userIdFromSession = (String) tSession.getValue("userId");
		String accountId = (String) tSession.getValue("accountId");

		String facilityCount = request.getParameter("facilityCount");

		String siteId="0";
		String isDefault = "0";

		String patientFacilityId = "";
		String specialityNames = "";
		String  selSpecialityIds = "";
		String regBy = "";
		String regByName  = "";
		String phyOther = "";
		String regDate = "";
		String accessFlag = "7";
		CodeDao cdSpl = new CodeDao();
		int orgRight=0,index=-1;
		int pageRight  = 0;

		String mode  = "";
		String organization = "0";
		String isReadOnly = "0";

		UserSiteDao usd = new UserSiteDao ();

		ArrayList arSiteid = new ArrayList();
		ArrayList arSitedesc = new ArrayList();
		String ddPatSite = "";
		String patientId = "";
		String patFacilityPK = "";

		GrpRightsJB grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
	    pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));
		mode = request.getParameter("mode");
		patientId = request.getParameter("patientId");
		patFacilityPK = request.getParameter("patFacilityPK");

		ArrayList  patSitePks = new ArrayList ();

		PatFacilityDao patFacilityDao = new PatFacilityDao();

		patFacilityDao = patFacility.getPatientFacilitiesSiteIds(StringUtil.stringToNum(patientId));

		if (patFacilityDao == null)
		{
			patFacilityDao = new PatFacilityDao();
		}

		patSitePks = patFacilityDao.getPatientSite();
		if (patSitePks == null)
		{
			patSitePks = new ArrayList();
		}

		if (StringUtil.isEmpty(mode))
		{
		mode = "N";
		}

		String comRtStr = request.getParameter("c");
		int comRtIntVal = 0;

		if (!StringUtil.isEmpty(comRtStr))
		{
		  if (comRtStr.equals("nx")) // no right; weired code used to hide it from URL
		  {
			  comRtIntVal = 0;
		  }
		  else
		  {
			  comRtIntVal = 1;

		  }
		}
		else
		{
			comRtIntVal = 1;
		}



		String inputType = "text";
		String displayStar = "";

		if( comRtIntVal < 1 && mode.equals("M") )
		{
		 	inputType = "hidden";
			displayStar = "<font color = blue>*</font>";
		}


		 /**
   * Shows logged in user's primary organization, default selection

   */
		String userSiteId = "";
		int userId = 0;
		String siteName = "";
		StringBuffer dSites = new StringBuffer();



		userId = StringUtil.stringToNum(userIdFromSession);
		com.velos.eres.web.user.UserJB  userB=(com.velos.eres.web.user.UserJB)tSession.getAttribute("currentUser");

		com.velos.eres.web.user.UserJB  userProvider = new UserJB ();

		if (mode.equals("M"))
		{

			patFacility.setId(Integer.parseInt(patFacilityPK));
			patFacility.getPatFacilityDetails();
			organization = patFacility.getPatientSite();


			patientFacilityId = patFacility.getPatientFacilityId();
			regDate = patFacility.getRegDate();
			regBy = patFacility.getRegisteredBy();
			selSpecialityIds = patFacility.getPatSpecialtylAccess();
			accessFlag = patFacility.getPatAccessFlag();
			phyOther = patFacility.getRegisteredByOther();
			isDefault = patFacility.getIsDefault();

			isReadOnly = patFacility.getIsReadOnly();

			if (StringUtil.isEmpty(isReadOnly))
			{
				isReadOnly = "0";
			}

			if (StringUtil.isEmpty(phyOther))
			{
				phyOther = "";
			}


			if(! StringUtil.isEmpty(selSpecialityIds ))
			{
				 specialityNames =cdSpl.getCodeLstNames(selSpecialityIds );

				if(specialityNames.equals("error"))
				{
					specialityNames="";
				}
			}
			else
			{
					specialityNames="";
					selSpecialityIds = "";
			}

				if(! StringUtil.isEmpty(regBy))
				{
					userProvider.setUserId(StringUtil.stringToNum(regBy));
					userProvider.getUserDetails();

					regByName = userProvider.getUserFirstName() + " " + userProvider.getUserLastName();
				}



			orgRight = userSiteB.getUserPatientFacilityRight(userId, StringUtil.stringToNum(patientId));
			userSiteId = organization;
		}
		else
		{
			userSiteId = userB.getUserSiteId();
			userSiteId=(userSiteId==null)?"":userSiteId;
		}


	//show sites for which the user has new right i.e right to add a patient
  	//JM: 032406: modified to get the organization list without user right
  	//UserSiteDao userSiteDao = userSiteB.getSitesWithNewRight(StringUtil.stringToNum(accountId),StringUtil.stringToNum(userIdFromSession));
	UserSiteDao userSiteDao = userSiteB.getAllSitesInAccount(StringUtil.stringToNum(accountId));

	ArrayList siteIds = userSiteDao.getUserSiteSiteIds();
  	ArrayList siteDescs = userSiteDao.getUserSiteNames() ;

  	if ( siteIds == null)
  	{
  		siteIds = new ArrayList();
  		siteDescs = new ArrayList();
  	}
  	int len = siteIds.size();

	//int cnt =0 ;
	dSites.append("<SELECT NAME=patorganization >") ;
	for (int counter = 0; counter <= len-1 ; counter++) {
		siteId =  (String)siteIds.get(counter);

		// check if the patient is already registered to this site, if yes, do not add it to the dropdown

		if (!patSitePks.contains(siteId) )
		{
			if(userSiteId.equals(siteId)) {
				dSites.append("<OPTION value = "+ userSiteId +" selected>" + siteDescs.get(counter) +"</OPTION>");

			} else {
				dSites.append("<OPTION value = "+ siteId +">" + siteDescs.get(counter)+"</OPTION>");
			}
		}

		if(userSiteId.equals(siteId) && mode.equals("M") ) {
			dSites.append("<OPTION value = "+ userSiteId +" selected>" + siteDescs.get(counter) +"</OPTION>");

		}
	}

	if ( ! siteIds.contains(userSiteId))
	{
		dSites.append("<OPTION value='' SELECTED>"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");
	}
	else
	{
		dSites.append("<OPTION value='' >"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");
	}
	dSites.append("</SELECT>");


	if ((mode.equals("M") && (StringUtil.isAccessibleFor(pageRight, 'E') && StringUtil.isAccessibleFor(orgRight, 'E'))) 
			|| (mode.equals("N") && StringUtil.isAccessibleFor(pageRight, 'N'))) {
	%>
	<p class="sectionHeadings"><%=MC.M_RegPat_ToOrg %><%-- Register <%=LC.Pat_Patient%> to an Organization*****--%></p>
<form name="patient" id="patFacilityFrm" method="post" action="updatePatientFacility.jsp" onsubmit="if (validate(document.patient)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
		<table width=100% border="0">
	<tr>
    <td width="20%">
       <%=LC.L_Organizations%><%--Organization*****--%> <FONT class="Mandatory">* </FONT>
    </td>
    <td width="30%" >

	<%=dSites%>
    </td>
    </tr>
   <tr>
    <td width="20%">
       <%=LC.L_PatFacility_ID%><%--<%=LC.Pat_Patient%> Facility Id*****--%> <FONT class="Mandatory">* </FONT>
    </td>
    <td width="20%" >
		<input type=<%=inputType %> name="patFacilityID" size = 20 MAXLENGTH = 20 value='<%=patientFacilityId%>'><%=displayStar%>
	</td>

   </tr>
<tr>
	<td    width="30%" colspan=2>
 <%=MC.M_Specify_GrpPatAces%><%--Specify groups/departments with access to edit this <%=LC.Pat_Patient_Lower%>'s demographics.
		If left blank, all groups will have this right*****--%>
	</td>
	<td  colspan=2>

		<input type=text name=txtSpeciality  size=25 readOnly value="<%=specialityNames%>">
		<input type="hidden" name="selSpecialityIds" value="<%=selSpecialityIds%>">
		&nbsp;&nbsp;
		<A href="#" onClick="openSpecialityWindow(document.patient)"><%=LC.L_Select_Speciality%><%--Select Speciality*****--%></A>
	&nbsp;&nbsp;<A href="#" onClick="removeSpeciality(document.patient)"><%=LC.L_Rem_All%><%--Remove All*****--%></A>
	</td>

</tr>

	<tr>
<%-- INF-20084 Datepicker-- AGodara --%>	
		<td width="20%"><%=LC.L_Reg_Date%><%--Registration Date*****--%></td>
		<td width="20%"><input type=text name="regdate" class="datefield" size = 10 MAXLENGTH = 11 value="<%=regDate%>"></td>
	</tr>
	<tr>
	<td width="20%">
       <%=LC.L_Provider%><%--Provider*****--%>
    </td>
    <td width="20%">
    	<input type=hidden name=patregby value='<%=regBy%>'>
    	<input type=text name=patregbyname value="<%=regByName%>" readonly>
          <A HREF=# onClick=openwinUser() ><%=LC.L_Select_User%><%--Select User*****--%></A>
    </td>
	</tr>
  <tr>
  	<td width="20%"><%=LC.L_If_Other%><%--If Other*****--%></td>
  	<td  width="20%"><input type="text" name="phyOther" value="<%=phyOther%>">
  </td>
  </tr>
  	 <tr>

  	 <td colspan = 2  > <%=MC.M_AccPat_InfoOrg%><%--Access to <%=LC.Pat_Patient%> Information for users of this organization*****--%> </td>
  	  <td  ><input type=radio name="accessFlag" value="7" <%if (accessFlag.equals("7")) { %>
  	       CHECKED<%}%> >
  	  <%=LC.L_Granted%><%--Granted*****--%> &nbsp;
  	  <% if (StringUtil.stringToNum(facilityCount) ==1 && mode.equals("M")){%>
  	  <input type=radio name="accessFlag" value="0" disabled > <%=LC.L_Revoked%><%--Revoked*****--%>
  	  <%}else {%>
  	  <input type=radio name="accessFlag" value="0" <%if (accessFlag.equals("0")) { %> CHECKED<%}%> > <%=LC.L_Revoked%><%--Revoked*****--%>
  	  <%}%>
  	  </td></tr>
   	 <tr>
  <td colspan = 2  > <%=MC.M_SelOrg_DefOrg%><%--Is the selected Organization <%=LC.Pat_Patient_Lower%>'s default Organization ?*****--%> </td>
  	  <td  ><input type=radio name="isDefault" value="1" <%if (isDefault.equals("1")) { %>
  	       CHECKED<%}%> >
  	  <%=LC.L_Yes%><%--Yes*****--%> &nbsp;
  	  <input type=radio name="isDefault" value="0" <%if (isDefault.equals("0")) { %>
  	       CHECKED<%} else {%> DISABLED <%} %> >
  	  <%=LC.L_No%><%--No*****--%> </td></tr>


</table>

	<input type="hidden" name="mode"  value='<%=mode%>'>
	<input type="hidden" name="patientId"  value='<%=patientId%>'>
	<input type="hidden" name="patFacilityPK"  value='<%=patFacilityPK%>'>

		<p> <font class="Mandatory" size=1><%=MC.M_IfChgPatFclty_EnrlOrgUpdt%><%--If changing <%=LC.Pat_Patient_Lower%>'s default facility, please note that 'Enrolling Organization' for
		the <%=LC.Pat_Patient_Lower%> in any <%=LC.Std_Study_Lower%> will need to be updated manually (if required)*****--%></font></p>



	<%
	String showSubmit ="";
	if (isReadOnly.equals("0") && (mode.equals("M") && (StringUtil.isAccessibleFor(pageRight, 'E') && StringUtil.isAccessibleFor(orgRight, 'E'))) 
			|| (isReadOnly.equals("0") && (mode.equals("N") && StringUtil.isAccessibleFor(pageRight, 'N')))) {
			showSubmit = "Y";
	}
	else { showSubmit="N"; }
	%>


<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="patFacilityFrm"/>
		<jsp:param name="showDiscard" value="N"/>
		<jsp:param name="showSubmit" value="<%=showSubmit%>"/>
		<jsp:param name="noBR" value="Y"/>

</jsp:include>


    <br>


  </Form>

  <%
	} //end of if body for page right

else

{

%>

  <jsp:include page="accessdenied.jsp" flush="true"/>

  <%
} //end of else body for page right

}//end of if body for session

else
{
%>

 <jsp:include page="timeout_childwindow.jsp" flush="true"/>

  <%
}

%>

  <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</div>
</body>



</html>



