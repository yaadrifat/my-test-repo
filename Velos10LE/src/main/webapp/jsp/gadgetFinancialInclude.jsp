<!-- 
This JSP will be included in gadgetHome.jsp. All non-local javascript variables and functions 
will be global in the mother JSP. Therefore, make sure to suffix all non-local variables and functions
with the gadget ID.
 -->

<%@page import="com.velos.eres.service.util.VelosResourceBundle"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC,com.velos.eres.service.util.Security"%> 
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@page import="java.text.DecimalFormat" %>
<%
if (!sessionmaint.isValidSession(request.getSession(false))) {
	return;
}
boolean isIE = false;
HttpSession tSession = request.getSession(true); 
String keySessId = "";
if (sessionmaint.isValidSession(tSession)) {
	String sessId = tSession.getId();
	if (sessId.length()>8) { sessId = sessId.substring(0,8); }
	sessId = Security.encrypt(sessId);
	char[] chs = sessId.toCharArray();
	StringBuffer sb = new StringBuffer();
	DecimalFormat df = new DecimalFormat("000");
	for (int iX=0; iX<chs.length; iX++) {
	    sb.append(df.format((int)chs[iX]));
	}
	keySessId = sb.toString();
	try { isIE = request.getHeader("USER-AGENT").toUpperCase().indexOf("MSIE") != -1; } catch(Exception e) {}
}
%>
<!-- Data-specific style definitions -->
<style>
.yui-dt-col-invoiceable > div.yui-dt-liner { text-align:right; }
.yui-dt-col-invoiced > div.yui-dt-liner { text-align:right; }
.yui-dt-col-uninvoiced > div.yui-dt-liner { text-align:right; }
.yui-dt-col-collected > div.yui-dt-liner { text-align:right; }
.yui-dt-col-outstanding > div.yui-dt-liner { text-align:right; }
.yui-dt-col-receipts > div.yui-dt-liner { text-align:right; }
.yui-dt-col-disbursements > div.yui-dt-liner { text-align:right; }
.yui-dt-col-netCash > div.yui-dt-liner { text-align:right; }
</style>
<script>
//Declare all functions here
var gadgetFinancial = {
	settings: {
		allStudyData: [],
		data: {},
		formatMyStudiesHTML: {},
		setAutoComplete: {},
		screenAction: {},
		openSettings: {},
		getAllStudies: {},
		getSettings: {},
		updateSettings: {},
		submitSettings: {},
		cancelSettings: {},
		clearSettings: {},
		openDeleteDialogStudy: {},
		deleteStudy: {}
	},
	billing:{
		reloadBilling: {},
		data: {},
		formatMyBillingHTML: {},
		customLinkClick: {},
		customCurrencyLinkFormatter: {}
	},
	collection:{
		reloadCollection: {},
		data: {},
		customLinkClick: {},
		customCurrencyLinkFormatter: {}
	},
	payments:{
		reloadPayments: {},
		data: {},
		customLinkClick: {},
		customCurrencyLinkFormatter: {}
	},
	configure: {},
	screenAction: {},
	formatAnImage: {},
	clearErrMsg: {},
	getMyStudies: {},
	fnMouseOver: {},
	fnMouseOut: {}
};

gadgetFinancial.fnMouseOver = function(oArgs) {
    var target = oArgs.target;
	var column = this.getColumn(target);
	var oRecord = this.getRecord(target);
	
	if(column.key!='studyNumber'){
		return nd();
	}

	var studyId = oRecord.getData('fk_study');
	return fnGetStudyMouseOver(studyId);
};

gadgetFinancial.fnMouseOut = function(oArgs) {
	return nd();
};
gadgetFinancial.billing.customLinkClick = function(studyId, studyNum, calledFrom) {
	$('gadgetFinancialExport_EmbedDataHere').innerHTML = ''; // Wipe out last data for performance
	$j.post('gadgetFinancialDrillDown.jsp?studyId='+studyId+'&view=billing&calledFrom='+calledFrom +'&studyNum='+htmlEncode(studyNum),
		function(data) {
			var param = '';
			switch(calledFrom){
			case 'invoiceable':
				param = ["'" + studyNum + "' " + "<%=LC.L_Invoiceable%>"];
				break;
			case 'invoiced':
				param = ["'" + studyNum + "' " + "<%=LC.L_Invoiced%>"];
				break;
			case 'uninvoiced':
				param = ["'" + studyNum + "' " + "<%=LC.L_Uninvoiced%>"];
				break;
			}
			var paramArray = [param];

			$j('#gadgetFinancialExport_EmbedDataHere').html(data);
			$j("#gadgetFinancialExportDiv").dialog({ modal: true });
			$j("#gadgetFinancialExportDiv").dialog("open"); 
			$j("#gadgetFinancialExportDiv").dialog( "option", "title", getLocalizedMessageString("L_Parameter_Report", paramArray));
		}
	);
}

gadgetFinancial.formatAnImage = function(el, oRecord, oColumn, oData) {
	if (oData != null){
		var key = oColumn.key;

		switch (key){
		case 'studyNumber':
			//var studyAccess = oRecord.getData('studyAccess');
			//if (f_check_perm_noAlert(studyAccess,'V')){
				var studyNumber = oRecord.getData('studyNumber');
				el.innerHTML = (studyNumber.length > 15)? studyNumber.substring(0, 12) + "&nbsp;<img class='asIsImage' " 
					+ "onmouseover='return overlib(\""+studyNumber+"\",CAPTION,\""+L_Study_Number+"\");' "
					+ "onmouseout='return nd();' " 
					+ "src='./images/More.png' border='0' complete='complete'/>" : studyNumber;
			//}
			break;
		default:
			break;
		}
	}
};

gadgetFinancial.billing.customCurrencyLinkFormatter = function(elCell, oRecord, oColumn, oData) {
	if (oData && oData.indexOf('E') > -1) {
		oData = parseFloat(oData);
	}
	if (!oData || oData.length < 1) {
		elCell.innerHTML = "-"; return;
	}
	if (oData[0] == '0') {
		try {
			if (parseFloat(oData) < 0.0000001) {
				elCell.innerHTML = "-"; return;
			}
		} catch(e) {}
	}

	elCell.innerHTML = oData;
	$j(elCell).formatCurrency({
		region:appNumberLocale
	});

	var studyId = oRecord.getData('fk_study');
	var studyNum = oRecord.getData('studyNumber');
	var inHTML = elCell.innerHTML;
	var titleStr = "<%=MC.M_ClkToSee_BreakDown%>";
	
	elCell.innerHTML = 	"<A href='#' title='"+titleStr+"' alt='"+titleStr+"' " 
		+"onClick ='gadgetFinancial.billing.customLinkClick("+studyId+", \""+studyNum+"\", \""+oColumn.key+"\");'>"
		+inHTML 
		+"</A>";
};

// Implementation starts here
gadgetFinancial.billing.reloadBilling = function() {
	try {
		$("gadgetFinancial_myBillingGrid").innerHTML = '<%=LC.L_Loading%>...'
			+'<img class="asIsImage_progbar" src="../images/jpg/loading_pg.gif" />';
		jQuery.ajax({
			url:'gadgetFinancial_getBilling',
			async: true,
			cache: false,
			success: function(resp) {
				resetSessionWarningOnTimer();
				try {
					gadgetFinancial.billing.data.gridData = {
						colArray: resp.colArray,
						dataArray: resp.dataArray
					};
					var loadDataTableForBilling = function() {
						// Sepcify column settings
						var colArray = YAHOO.lang.JSON.parse(resp.colArray);
						var configureColArray = function () {
							var tmpColArray = [];
							for (var iX=0; iX<colArray.length; iX++) {
								if (colArray[iX].key == 'studyId') { continue; }
								if (colArray[iX].key == 'studyAccess') { continue; }
								tmpColArray.push(colArray[iX]);
							}
							colArray = tmpColArray;
							
							for (var iX=0; iX<colArray.length; iX++) {
								colArray[iX].resizeable = true;
								colArray[iX].sortable = true;

								if (colArray[iX].type && colArray[iX].type == 'image'){
									colArray[iX].formatter = gadgetFinancial.formatAnImage;
								}

								switch(colArray[iX].key) {
								case 'invoiceable':
								case 'invoiced':
								case 'uninvoiced':
									colArray[iX].formatter = gadgetFinancial.billing.customCurrencyLinkFormatter; 
									//customCurrencyFormatter; // <= defined in gadgetHome.jsp
									colArray[iX].sortOptions = { sortFunction: customCurrencySorter };
									break;
								case 'studyId':
								case 'studyAccess':
									//colArray[iX].hidden = false;
									break;
								default:
									break;
								}
							}
						}(); // Execute configureColArray()
						
						// Get data
						var dataArray = YAHOO.lang.JSON.parse(resp.dataArray);
						
						// Set dummy data
						/*
						var gadgetSample1_dummy_data = [
							{ fk_study: "ga-3475", studyNumber: "gadget long long long long long long",   invoiceable: "", invoiced: "" },
							{ fk_study: "sp-9980", studyNumber: "sprocket", invoiceable: "3.75", invoiced: "1.23456789E6" },
							{ fk_study: "wi-0650", studyNumber: "widget",   invoiceable: "4.25", invoiced: "-2.75" }
						];
						dataArray = gadgetSample1_dummy_data;
						*/
						
						var myDataSource = new YAHOO.util.DataSource(dataArray);		
						myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
						myDataSource.responseSchema = {
							fields: resp.fieldsArray
						};
						
						// Create datatable
						var myDataTable = new YAHOO.widget.DataTable(
							"gadgetFinancial_myBillingGrid", 
							colArray, myDataSource, 
							{ 
								sortedBy:{key:'studyNumber', dir:'asc'}
							});

						myDataTable.subscribe("mousemoveEvent", gadgetFinancial.fnMouseOver);
						myDataTable.subscribe("cellMouseoverEvent", gadgetFinancial.fnMouseOver);
						myDataTable.subscribe("cellMouseoutEvent", gadgetFinancial.fnMouseOut);
					}(); // Execute loadDataTableForBilling()
					$j(".yui-dt th").attr('style',"text-align:center;");
					$j(".yui-dt-label").attr('style',"margin-left:7px;");
				} catch(e) { alert(e.message); }
			} // End of success
		}); // End of jQuery.ajax
	} catch(e) {
		alert(getLocalizedMessageString("M_CommErr_CnctVelosSupp",{}));
	}
}
gadgetFinancial.settings.updateSettings = function() {
	var j = {};
	j.id = 'gadgetFinancial';
	j.studyIds = '['+jQuery.trim($('gadgetFinancial_studyIds').value)+']';
	return j;
}
gadgetFinancial.settings.submitSettings = function() {
	var myGadgetId = 'gadgetFinancial';
	setSettingsGlobal(myGadgetId);
	<%if(isIE){%>
		if (navigator.userAgent.indexOf("MSIE 8") > -1) {
			onMouseleaveToolTip(e);
		} else{ 
			onMouseleaveToolTip();
		}
	<%} else {%>
		onMouseleaveToolTip();
	<%}%>
	$j("#gadgetFinancial_settings").slideUp(200).fadeOut(200);
	setTimeout(function() { 
		reloadGadgetScreenGlobal(myGadgetId);
		gadgetFinancial.screenAction();
	}, 150);
	
	return true;
}
gadgetFinancial.settings.clearSettings = function() {
	if ($('gadgetFinancial_studyIds')) { $('gadgetFinancial_studyIds').value = ''; }
	if ($('gadgetFinancial_reallyDeleteHTML')) { $('gadgetFinancial_reallyDeleteHTML').innerHTML = ''; }
	gadgetFinancial.clearErrMsg();
};
gadgetFinancial.clearErrMsg = function() {
	if ($('gadgetFinancial_errMsg_studyIds')) { $('gadgetFinancial_errMsg_studyIds').innerHTML = ''; }
};

gadgetFinancial.settings.getAllStudies = function(){
	jQuery.ajax({
		url:'gadgetFinancial_getStudies',
		async: true,
		cache: false,
		success: function(resp) {
			resetSessionWarningOnTimer();
			gadgetFinancial.settings.data.allArray = resp.array;
			gadgetFinancial.settings.setAutoComplete();
		}
	});
};
gadgetFinancial.settings.getSettings = function(o) {
	try {
		var j = jQuery.parseJSON(o.responseText);

		jQuery.ajax({
    		url:'gadgetFinancial_getMyStudies',
    		async: true,
    		cache: false,
    		success: function(resp) {
    			resetSessionWarningOnTimer();
    			gadgetFinancial.settings.data.myArray = resp.array;
    			if (gadgetFinancial.settings.data.myArray) {
    				for(var iX=0; iX<gadgetFinancial.settings.data.myArray.length; iX++) {
    					if (iX > 5) { break; }
    					var dataJSON = gadgetFinancial.settings.data.myArray[iX];
   						if ($('gadgetFinancial_studyIds').value == '')
   							$('gadgetFinancial_studyIds').value += dataJSON.studyId;
   						else
   							$('gadgetFinancial_studyIds').value += ','+ dataJSON.studyId;
    				}
    			}
    			j.gadgetFinancial.studyIds = $('gadgetFinancial_studyIds').value;
    			gadgetFinancial.settings.formatMyStudiesHTML();
    			gadgetFinancial.settings.screenAction();
    		}
    	});

	} catch(e) {
		$j('#gadgetFinancial_studyIds').val("");
		gadgetFinancial.settings.formatMyStudiesHTML();
		gadgetFinancial.settings.screenAction();
	};
	gadgetFinancial.settings.getAllStudies();
};

gadgetFinancial.settings.formatMyStudiesHTML = function () {
	var str1 = '<table width="100%" cellspacing="0">';

	if (gadgetFinancial.settings.data.myArray){
		for(var iX=0; iX<gadgetFinancial.settings.data.myArray.length; iX++) {
			if (iX > 5) { break; }
			var dataJSON = gadgetFinancial.settings.data.myArray[iX];
			var studyNumber = dataJSON.studyNumber;
			studyNumber = (studyNumber.length > 15)? studyNumber.substring(0, 12) + "&nbsp;<img class='asIsImage' " 
				+ "onmouseover='return overlib(\""+studyNumber+"\",CAPTION,\""+L_Study_Number+"\");' "
				+ "onmouseout='return nd();' " 
				+ "src='./images/More.png' border='0' complete='complete'/>" : studyNumber;

			str1 += "<tr id='gadgetFinancial_myStudy-row-"+iX+"'><td>";
			str1 += "<a target=\"_blank\" id=\"gadgetFinancial_myStudy"+iX+"\"";
			str1 += " href=\""+dataJSON.studyId+"\"";
			str1 += " title=\""+dataJSON.studyId+"\"></a>";
			str1 += studyNumber;
			str1 += "</td><td nowrap='nowrap' align='right'>";
			str1 += "<a style=\"opacity:0; filter:alpha(opacity=0);\" href=\"javascript:gadgetFinancial.settings.openDeleteDialogStudy("+(iX+1);
			str1 += ",'";
			str1 += dataJSON.studyId;
			str1 += "');\" id=\"gadgetFinancial_delMyStudy"+iX+"\">";
			str1 += "<img id='gadgetFinancial_myStudy-del-img-"+iX;
			str1 += "' class='headerImage' style='border:none; opacity:0; filter:alpha(opacity=0);' title='";
			str1 += "Delete selection #".replace("\'", "&#39;")+(iX+1);
			str1 += "' src='./images/delete.gif' border=0 />";
			str1 += "</a>";
			str1 += "</td></tr>";
		}
	} else {
		str1 += '<tr><td><i>'+'No studies selected'+'</i></td></tr>';
	}
	str1 += '</table>';
	$('gadgetFinancial_myStudiesHTML').innerHTML = str1;
}
gadgetFinancial.settings.setAutoComplete = function (){
	gadgetFinancial.settings.allStudyData = [];
	if (gadgetFinancial.settings.data.allArray){	
		for(var iY=0; iY<gadgetFinancial.settings.data.allArray.length; iY++) {
			var dataJSON = gadgetFinancial.settings.data.allArray[iY];
			var j = {};
			j["id"] = dataJSON.allStudyId; 
			j["value"] = dataJSON.allStudyNumber;
			gadgetFinancial.settings.allStudyData[iY] = j;
		}
	}
	YUI().use('node', 'event-mouseenter', function(Y) {
		if ($('gadgetFinancial_study')){
			Y.one('#gadgetFinancial_study').on('focus', 
					openAuto('gadgetFinancial_study','gadgetFinancial_studyId',gadgetFinancial.settings.allStudyData,'Study'));
			Y.one('#gadgetFinancial_study').on('click', 
				openAuto('gadgetFinancial_study','gadgetFinancial_studyId',gadgetFinancial.settings.allStudyData,'Study'));
		}
	});
}

gadgetFinancial.settings.addStudyCriteria = function (){
	var existingStudies = $('gadgetFinancial_studyIds').value;
	existingStudies = existingStudies.replace(/\s/g , "");
	var addedStudy = $('gadgetFinancial_studyId').value;
	addedStudy = addedStudy.replace(/\s/g , "");

	if (addedStudy && addedStudy != ''){	
		if (existingStudies == '')
			$('gadgetFinancial_studyIds').value += addedStudy;
		else{
			if (existingStudies == addedStudy || (existingStudies.indexOf(','+addedStudy) > 0))
				return;
			else
				$('gadgetFinancial_studyIds').value += ','+ addedStudy;
		}
		$('gadgetFinancial_studyId').value ='';

		//gadgetFinancial.settings.updateSettings();
		gadgetFinancial.settings.submitSettings();
		setTimeout(function() { gadgetFinancial.settings.openSettings()}, 250);
	}
}

gadgetFinancial.settings.screenAction = function() {
	YUI({
	    gallery: 'gallery-2012.06.20-20-07'
	}).use('node', 'event-mouseenter', 'gallery-placeholder', function(Y) {
		if (Y.one('#gadgetFinancial_study')) {
			Y.one('#gadgetFinancial_study').plug(Y.Plugin.Placeholder, {
				text : 'Enter Study Number and Select',
				hideOnFocus : true
			});
		}

		var onMouseoverDelMyStudy = function(e) {
			var trNum = -1;
			if (e.target.get('tagName') == 'TD') {
				var pos1 = 'gadgetFinancial_myStudy-row-'.length;
				trNum = e.target.get('parentNode').get('id').substring(pos1,pos1+1);
			} else if (e.target.get('tagName') == 'A') {
				var pos1 = 'gadgetFinancial_myStudy-row-'.length;
				trNum = e.target.get('parentNode').get('parentNode').get('id').substring(pos1,pos1+1);
			} else if (e.target.get('tagName') == 'IMG') {
				var pos1 = 'gadgetFinancial_myStudy-del-img-'.length;
				trNum = e.target.get('id').substring(pos1,pos1+1);
			}
			if (trNum == -1 || !Y.one('#gadgetFinancial_myStudy-del-img-'+trNum)) { return; }
			Y.one('#gadgetFinancial_myStudy-del-img-'+trNum).setStyle('opacity', '1');
			Y.one('#gadgetFinancial_myStudy-del-img-'+trNum).get('parentNode').setStyle('opacity', '1');
			$j('#gadgetFinancial_myStudy-row-'+trNum).addClass('gdt-highlighted-row');
		}
		var onMouseoutDelMyStudy = function(e) {
			var trNum = -1;
			if (e.target.get('tagName') == 'TD') {
				var pos1 = 'gadgetFinancial_myStudy-row-'.length;
				trNum = e.target.get('parentNode').get('id').substring(pos1,pos1+1);
			} else if (e.target.get('tagName') == 'A') {
				var pos1 = 'gadgetFinancial_myStudy-row-'.length;
				trNum = e.target.get('parentNode').get('parentNode').get('id').substring(pos1,pos1+1);
			} else if (e.target.get('tagName') == 'IMG') {
				var pos1 = 'gadgetFinancial_myStudy-del-img-'.length;
				trNum = e.target.get('id').substring(pos1,pos1+1);
			}
			if (trNum == -1 || !Y.one('#gadgetFinancial_myStudy-del-img-'+trNum)) { return; }
			Y.one('#gadgetFinancial_myStudy-del-img-'+trNum).setStyle('opacity', '0');
			Y.one('#gadgetFinancial_myStudy-del-img-'+trNum).get('parentNode').setStyle('opacity', '0');
			$j('#gadgetFinancial_myStudy-row-'+trNum).removeClass('gdt-highlighted-row');
		}
		for(var iX=0; iX<5; iX++) {
			if (!Y.one('#gadgetFinancial_myStudy-row-'+iX)) { continue; }
			Y.one('#gadgetFinancial_myStudy-row-'+iX).on('mousemove', onMouseoverDelMyStudy);
			Y.one('#gadgetFinancial_myStudy-row-'+iX).on('mouseenter', onMouseoverDelMyStudy);
			Y.one('#gadgetFinancial_myStudy-row-'+iX).on('mouseover', onMouseoverDelMyStudy);
			Y.one('#gadgetFinancial_myStudy-row-'+iX).on('mouseleave', onMouseoutDelMyStudy);
			Y.one('#gadgetFinancial_myStudy-row-'+iX).on('mouseout', onMouseoutDelMyStudy);
			if (!Y.one('#gadgetFinancial_delMyStudy'+iX)) { continue; }
			Y.one('#gadgetFinancial_delMyStudy'+iX).setStyle('opacity', '0');
			Y.one('#gadgetFinancial_delMyStudy'+iX).setStyle('opacity', '0');
		}
		if ($('gadgetFinancial_study')){
			Y.one('#gadgetFinancial_study').on('focus', 
				openAuto('gadgetFinancial_study','gadgetFinancial_studyId',gadgetFinancial.settings.allStudyData,'Study'));
			Y.one('#gadgetFinancial_study').on('click', 
				openAuto('gadgetFinancial_study','gadgetFinancial_studyId',gadgetFinancial.settings.allStudyData,'Study'));
		}
	});
}
gadgetFinancial.settings.openDeleteDialogStudy = function(recNum,studyId) {
	$j("#gadgetFinancial_reallyDeleteHTML").fadeIn(400);
	$("gadgetFinancial_reallyDeleteHTML").innerHTML = '<table class="gdt-table" style="width:100%;">'
			+ '<tr><td>Really delete selection #'+recNum+'?'+'</td></tr>'
			+ '<tr><td align="center"><button type="button"' 
			+ ' id="gadgetFinancial_settings_delStudy" class="gadgetFinancial_settings_delStudy">Yes</button>&nbsp;&nbsp;'
			+ '<button type="button" id="gadgetFinancial_settings_delStudyCancel"'
			+ ' class="gadgetFinancial_settings_delStudyCancel">No</button>';
	$j("#gadgetFinancial_settings_delStudy").button();
	$j("#gadgetFinancial_settings_delStudy").click(function() {
		$j("#gadgetFinancial_reallyDeleteHTML").innerHTML = '';
		gadgetFinancial.settings.deleteStudy(studyId);
	});
	$j("#gadgetFinancial_settings_delStudyCancel").button();
	$j("#gadgetFinancial_settings_delStudyCancel").click(function() {
		$j("#gadgetFinancial_reallyDeleteHTML").fadeOut(200);
	});
}
gadgetFinancial.settings.deleteStudy = function(studyId) {
	var thisJ = {}
	$('gadgetFinancial_studyIds').value = '';
	try {
		for(var iX=0; iX<gadgetFinancial.settings.data.myArray.length; iX++) {
			if (iX > 5) { break; }
			var dataJSON = gadgetFinancial.settings.data.myArray[iX];
			if (dataJSON.studyId != studyId){
				if ($('gadgetFinancial_studyIds').value == '')
					$('gadgetFinancial_studyIds').value += dataJSON.studyId;
				else
					$('gadgetFinancial_studyIds').value += ','+ dataJSON.studyId;
			}
		}
		//gadgetFinancial.settings.updateSettings();
		gadgetFinancial.settings.submitSettings();
		setTimeout(function() { gadgetFinancial.settings.openSettings()}, 250);
	} catch(e) {}
}
gadgetFinancial.settings.openSettings = function() {
	$j("#gadgetFinancial_settings").slideDown(200).fadeIn(200);
	YUI({
	    gallery: 'gallery-2012.06.20-20-07'
	}).use('gallery-placeholder', 'node', 'io', function(Y) {
		//Y.one('#gadgetFinancial_saveButton').on('click', gadgetFinancial.settings.submitSettings);
    	Y.io('gadgetCookie.jsp', {
        	method: 'POST',
        	data: 'type=typeGetSettings',
        	sync: true,
        	on: {
            	success: function (tranId, o) {
            		resetSessionWarningOnTimer();
            		gadgetFinancial.settings.getSettings(o);
                },
                failure: function (tranId, o) {
                }
            }
        });
    	if ($('gadgetFinancial_addStudyButton')){
    		Y.one('#gadgetFinancial_addStudyButton').on('doubleclick', function(){return false;});
    		Y.one('#gadgetFinancial_addStudyButton').on('click', gadgetFinancial.settings.addStudyCriteria);
    	}
	});
}
gadgetFinancial.settings.cancelSettings = function() {
	gadgetFinancial.settings.clearSettings();
	$j("#gadgetFinancial_settings").slideUp(200).fadeOut(200);
}

gadgetFinancial.billing.formatMyBillingHTML = function () {
	var str1 = '<table width="100%" >';
	str1 += "<tr>"
	 	+ "<th>Study Number</th>"
	 	+ "<th>Invoiceables</th>"
	 	+ "<th>Invoiced</th>"
	 	+ "<th>Uninvoiced</th>"
	 	+ "</tr>";
	if (gadgetFinancial.billing.data.myArray){
		for(var iX=0; iX<gadgetFinancial.billing.data.myArray.length; iX++) {
			if (iX > 5) { break; }
			var dataJSON = gadgetFinancial.billing.data.myArray[iX];
			str1 += "<tr id='gadgetFinancial_myBilling-row-"+iX+"'><td>";
			str1 += "<a target=\"_blank\" id=\"gadgetFinancial_myBilling"+iX+"\"";
			str1 += " href=\""+dataJSON.studyId+"\"";
			str1 += " title=\""+dataJSON.studyId+"\"></a>";
			str1 += dataJSON.studyNumber;
			str1 += "</td>";
			str1 += "<td nowrap='nowrap' align='right'>" + dataJSON.invoiceable+"</td>";
			str1 += "<td nowrap='nowrap' align='right'>" + dataJSON.invoiced+"</td>";
			str1 += "<td nowrap='nowrap' align='right'>" + dataJSON.uninvoiced+"</td>";
			str1 += "</tr>";
		}
	}
	str1 += '</table>';
	$('gadgetFinancial_myBillingHTML').innerHTML = str1;
}

gadgetFinancial.configure = function() {
	try {
		$j("#gadgetFinancial_mainTabs").tabs({selected:2});
	} catch(e) {
		// console.log('In gadgetFinancial.configure:'+e);
	}
};

gadgetFinancial.screenAction = function() {
	YUI({
	    gallery: 'gallery-2012.06.20-20-07'
	}).use('gallery-placeholder', 'node', function(Y) {
		var addToolTipToButton = function(buttonId) {
			Y.one(buttonId).on('mousemove', onMousemoveToolTip);
			Y.one(buttonId).on('mouseleave', onMouseleaveToolTip);
			Y.one(buttonId).on('mouseout', onMouseleaveToolTip);
		};

		if ($('gadgetFinancial_addStudyButton')){
			addToolTipToButton('#gadgetFinancial_addStudyButton');
		}
	});
	
	$j(function() {
		$j("#gadgetFinancial_mainTabs").tabs({selected:0,
			select: function(event, ui) {
        		switch(ui.index){
        		case 0:
        			if (gadgetFinancial.getMyStudies() < 1) {
            			$('gadgetFinancial_myBillingGrid').innerHTML = '<p><i>'+'<%=MC.M_PlsUseSet_ToAddStudies%>'+'</i></p>';
        			} else {
            			gadgetFinancial.billing.reloadBilling();
            		}
        			break;
        		case 1:
        			if (gadgetFinancial.getMyStudies() < 1) {
        				$('gadgetFinancial_myCollectionGrid').innerHTML = '<p><i>'+'<%=MC.M_PlsUseSet_ToAddStudies%>'+'</i></p>';
        			} else {
            			gadgetFinancial.collection.reloadCollection();
            		}
        			break;
        		case 2:
        			if (gadgetFinancial.getMyStudies() < 1) {
        				$('gadgetFinancial_myPaymentsGrid').innerHTML = '<p><i>'+'<%=MC.M_PlsUseSet_ToAddStudies%>'+'</i></p>';
        			} else {
            			gadgetFinancial.payments.reloadPayments();
            		}
        			break;
        		}
	        }
		});
	});
	if ($('gadgetFinancial_addStudyButton')){
		$j("#gadgetFinancial_addStudyButton").button();
	}
	//$j("#gadgetFinancial_saveButton").button();
	$j("#gadgetFinancial_cancelButton").button();
	$j("#gadgetFinancial_cancelButton").click(function() {
		gadgetFinancial.settings.cancelSettings();
	});
	if (gadgetFinancial.getMyStudies() < 1) {
		$('gadgetFinancial_myBillingGrid').innerHTML = '<p><i>'+'<%=MC.M_PlsUseSet_ToAddStudies%>'+'</i></p>';
		$('gadgetFinancial_myCollectionGrid').innerHTML = '<p><i>'+'<%=MC.M_PlsUseSet_ToAddStudies%>'+'</i></p>';
		$('gadgetFinancial_myPaymentsGrid').innerHTML = '<p><i>'+'<%=MC.M_PlsUseSet_ToAddStudies%>'+'</i></p>';
	} else {
		gadgetFinancial.billing.reloadBilling();
	}
};

gadgetFinancial.getMyStudies = function() {
	var numStudies = 0;
	try {
		jQuery.ajax({
    		url:'gadgetFinancial_getMyStudies',
    		async: false,
    		cache: false,
    		success: function(resp) {
    			resetSessionWarningOnTimer();
    			numStudies = resp.array.length;
    		}
    	});
	} catch(e) { alert(e.message); }
	return numStudies;
};

gadgetFinancial.collection.customLinkClick = function(studyId, studyNum, calledFrom) {
	$('gadgetFinancialExport_EmbedDataHere').innerHTML = ''; // Wipe out last data for performance
	$j.post('gadgetFinancialDrillDown.jsp?studyId='+studyId+'&view=collection&calledFrom='+calledFrom +'&studyNum='+htmlEncode(studyNum),
		function(data) {
			var param = '';
			switch(calledFrom){
			case 'invoiced':
				param = ["'" + studyNum + "' " + "<%=LC.L_Invoiced%>"];
				break;
			case 'collected':
				param = ["'" + studyNum + "' " + "<%=LC.L_Collected%>"];
				break;
			case 'outstanding':
				param = ["'" + studyNum + "' " + "<%=LC.L_Outstanding%>"];
				break;
			}
			var paramArray = [param];

			$j('#gadgetFinancialExport_EmbedDataHere').html(data);
			$j("#gadgetFinancialExportDiv").dialog({ modal: true });
			$j("#gadgetFinancialExportDiv").dialog("open"); 
			$j("#gadgetFinancialExportDiv").dialog( "option", "title", getLocalizedMessageString("L_Parameter_Report", paramArray));
		}
	);
};

gadgetFinancial.collection.customCurrencyLinkFormatter = function(elCell, oRecord, oColumn, oData) {
	if (oData && oData.indexOf('E') > -1) {
		oData = parseFloat(oData);
	}
	if (!oData || oData.length < 1) {
		elCell.innerHTML = "-"; return;
	}
	if (oData[0] == '0') {
		try {
			if (parseFloat(oData) < 0.0000001) {
				elCell.innerHTML = "-"; return;
			}
		} catch(e) {}
	}

	elCell.innerHTML = oData;
	$j(elCell).formatCurrency({
		region:appNumberLocale
	});

	var studyId = oRecord.getData('fk_study');
	var studyNum = oRecord.getData('studyNumber');
	var inHTML = elCell.innerHTML;
	var titleStr = "<%=MC.M_ClkToSee_BreakDown%>";
	
	elCell.innerHTML = 	"<A href='#' title='"+titleStr+"' alt='"+titleStr+"' " 
		+"onClick ='gadgetFinancial.collection.customLinkClick("+studyId+", \""+studyNum+"\", \""+oColumn.key+"\");'>"
		+inHTML 
		+"</A>";
};

gadgetFinancial.collection.reloadCollection = function() {
	try {
		$("gadgetFinancial_myCollectionGrid").innerHTML = '<%=LC.L_Loading%>...'
			+'<img class="asIsImage_progbar" src="../images/jpg/loading_pg.gif" />';
		jQuery.ajax({
			url:'gadgetFinancial_getCollection',
			async: true,
			cache: false,
			success: function(resp) {
				resetSessionWarningOnTimer();
				try {
					gadgetFinancial.collection.data.gridData = {
						colArray: resp.colArray,
						dataArray: resp.dataArray
					};
					var loadDataTableForCollection = function() {
						// Sepcify column settings
						var colArray = YAHOO.lang.JSON.parse(resp.colArray);
						var configureColArray = function () {
							var tmpColArray = [];
							for (var iX=0; iX<colArray.length; iX++) {
								if (colArray[iX].key == 'studyId') { continue; }
								if (colArray[iX].key == 'studyAccess') { continue; }
								tmpColArray.push(colArray[iX]);
							}
							colArray = tmpColArray;
							
							for (var iX=0; iX<colArray.length; iX++) {
								colArray[iX].resizeable = true;
								colArray[iX].sortable = true;

								if (colArray[iX].type && colArray[iX].type == 'image'){
									colArray[iX].formatter = gadgetFinancial.formatAnImage;
								}

								switch(colArray[iX].key) {
								case 'invoiced':
								case 'collected':
								case 'outstanding':
									colArray[iX].formatter = gadgetFinancial.collection.customCurrencyLinkFormatter; 
									//customCurrencyFormatter; // <= defined in gadgetHome.jsp
									colArray[iX].sortOptions = { sortFunction: customCurrencySorter };
									break;
								case 'studyId':
								case 'studyAccess':
									//colArray[iX].hidden = false;
									break;
								default:
									break;
								}
							}
						}(); // Execute configureColArray()
						
						// Get data
						var dataArray = YAHOO.lang.JSON.parse(resp.dataArray);
						
						// Set dummy data
						/*
						var gadgetSample1_dummy_data = [
							{ fk_study: "ga-3475", studyNumber: "gadget long long long long long long",   invoiceable: "", invoiced: "" },
							{ fk_study: "sp-9980", studyNumber: "sprocket", invoiceable: "3.75", invoiced: "1.23456789E6" },
							{ fk_study: "wi-0650", studyNumber: "widget",   invoiceable: "4.25", invoiced: "-2.75" }
						];
						dataArray = gadgetSample1_dummy_data;
						*/
						
						var myDataSource = new YAHOO.util.DataSource(dataArray);		
						myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
						myDataSource.responseSchema = {
							fields: resp.fieldsArray
						};
						
						// Create datatable
						var myDataTable = new YAHOO.widget.DataTable(
							"gadgetFinancial_myCollectionGrid", 
							colArray, myDataSource, 
							{ 
								sortedBy:{key:'studyNumber', dir:'asc'}
							});

						myDataTable.subscribe("mousemoveEvent", gadgetFinancial.fnMouseOver);
						myDataTable.subscribe("cellMouseoverEvent", gadgetFinancial.fnMouseOver);
						myDataTable.subscribe("cellMouseoutEvent", gadgetFinancial.fnMouseOut);
					}(); // Execute loadDataTableForCollection()
					$j(".yui-dt th").attr('style',"text-align:center;");
					$j(".yui-dt-label").attr('style',"margin-left:7px;");
				} catch(e) { alert(e.message); }
			} // End of success
		}); // End of jQuery.ajax
	} catch(e) {
		alert(getLocalizedMessageString("M_CommErr_CnctVelosSupp",{}));
	}
};

gadgetFinancial.payments.customLinkClick = function(studyId, studyNum, calledFrom) {
	$('gadgetFinancialExport_EmbedDataHere').innerHTML = ''; // Wipe out last data for performance
	$j.post('gadgetFinancialDrillDown.jsp?studyId='+studyId+'&view=payments&calledFrom='+calledFrom +'&studyNum='+htmlEncode(studyNum),
		function(data) {
			var param = '';
			switch(calledFrom){
			case 'invoiced':
				param = ["'" + studyNum + "' " + "<%=LC.L_Invoiced%>"];
				break;
			case 'collected':
				param = ["'" + studyNum + "' " + "<%=LC.L_Collected%>"];
				break;
			case 'outstanding':
				param = ["'" + studyNum + "' " + "<%=LC.L_Outstanding%>"];
				break;
			}
			var paramArray = [param];

			$j('#gadgetFinancialExport_EmbedDataHere').html(data);
			$j("#gadgetFinancialExportDiv").dialog({ modal: true });
			$j("#gadgetFinancialExportDiv").dialog("open"); 
			$j("#gadgetFinancialExportDiv").dialog( "option", "title", getLocalizedMessageString("L_Parameter_Report", paramArray));
		}
	);
}

gadgetFinancial.payments.customCurrencyLinkFormatter = function(elCell, oRecord, oColumn, oData) {
	if (oData && oData.indexOf('E') > -1) {
		oData = parseFloat(oData);
	}
	if (!oData || oData.length < 1) {
		elCell.innerHTML = "-"; return;
	}
	if (oData[0] == '0') {
		try {
			if (parseFloat(oData) < 0.0000001) {
				elCell.innerHTML = "-"; return;
			}
		} catch(e) {}
	}

	elCell.innerHTML = oData;
	$j(elCell).formatCurrency({
		region:appNumberLocale
	});

	var studyId = oRecord.getData('fk_study');
	var studyNum = oRecord.getData('studyNumber');
	var inHTML = elCell.innerHTML;
	var titleStr = "<%=MC.M_ClkToSee_BreakDown%>";
	
	elCell.innerHTML = 	"<A href='#' title='"+titleStr+"' alt='"+titleStr+"' " 
		+"onClick ='gadgetFinancial.payments.customLinkClick("+studyId+", \""+studyNum+"\", \""+oColumn.key+"\");'>"
		+inHTML 
		+"</A>";
};

gadgetFinancial.payments.reloadPayments = function() {
	try {
		$("gadgetFinancial_myPaymentsGrid").innerHTML = '<%=LC.L_Loading%>...'
			+'<img class="asIsImage_progbar" src="../images/jpg/loading_pg.gif" />';
		jQuery.ajax({
			url:'gadgetFinancial_getPayments',
			async: true,
			cache: false,
			success: function(resp) {
				resetSessionWarningOnTimer();
				try {
					gadgetFinancial.payments.data.gridData = {
						colArrayPayments: resp.colArray,
						dataArray: resp.dataArray
					};
					var loadDataTableForPayments = function() {
						// Sepcify column settings
						var colArrayPayments = YAHOO.lang.JSON.parse(resp.colArray);
						var configureColArray = function () {
							var tmpColArray = [];
							for (var iX=0; iX<colArrayPayments.length; iX++) {
								if (colArrayPayments[iX].key == 'studyId') { continue; }
								if (colArrayPayments[iX].key == 'studyAccess') { continue; }
								tmpColArray.push(colArrayPayments[iX]);
							}
							colArrayPayments = tmpColArray;
							
							for (var iX=0; iX<colArrayPayments.length; iX++) {
								colArrayPayments[iX].resizeable = true;
								colArrayPayments[iX].sortable = true;

								if (colArrayPayments[iX].type && colArrayPayments[iX].type == 'image'){
									colArrayPayments[iX].formatter = gadgetFinancial.formatAnImage;
								}

								switch(colArrayPayments[iX].key) {
								case 'receipts':
								case 'disbursements':
								case 'netCash':
									colArrayPayments[iX].formatter = gadgetFinancial.payments.customCurrencyLinkFormatter; 
									//customCurrencyFormatter; // <= defined in gadgetHome.jsp
									colArrayPayments[iX].sortOptions = { sortFunction: customCurrencySorter };
									break;
								case 'studyId':
								case 'studyAccess':
									//colArrayPayments[iX].hidden = false;
									break;
								default:
									break;
								}
							}
						}(); // Execute configureColArray()
						
						// Get data
						var dataArray = YAHOO.lang.JSON.parse(resp.dataArray);
						
						// Set dummy data
						/*
						var gadgetSample1_dummy_data = [
							{ fk_study: "ga-3475", studyNumber: "gadget long long long long long long",   invoiceable: "", invoiced: "" },
							{ fk_study: "sp-9980", studyNumber: "sprocket", invoiceable: "3.75", invoiced: "1.23456789E6" },
							{ fk_study: "wi-0650", studyNumber: "widget",   invoiceable: "4.25", invoiced: "-2.75" }
						];
						dataArray = gadgetSample1_dummy_data;
						*/
						
						var myDataSource = new YAHOO.util.DataSource(dataArray);		
						myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
						myDataSource.responseSchema = {
							fields: resp.fieldsArray
						};
						
						// Create datatable
						var myDataTablePayments = new YAHOO.widget.DataTable(
							"gadgetFinancial_myPaymentsGrid", 
							colArrayPayments, myDataSource,
							{ 
								sortedBy:{key:'studyNumber', dir:'asc'}
							});

						myDataTablePayments.subscribe("mousemoveEvent", gadgetFinancial.fnMouseOver);
						myDataTablePayments.subscribe("cellMouseoverEvent", gadgetFinancial.fnMouseOver);
						myDataTablePayments.subscribe("cellMouseoutEvent", gadgetFinancial.fnMouseOut);
					}(); // Execute loadDataTableForPayments()
					$j(".yui-dt th").attr('style',"text-align:center; margin:5px;");
					$j(".yui-dt-label").attr('style',"margin-left:7px;");
				} catch(e) { alert(e.message); }
			} // End of success
		}); // End of jQuery.ajax
	} catch(e) {
		alert(getLocalizedMessageString("M_CommErr_CnctVelosSupp",{}));
	}
}

clearerRegistry['gadgetFinancial'] = gadgetFinancial.settings.clearSettings;
//validatorRegistry['gadgetFinancial'] = gadgetFinancial.validate;
settingsUpdatorRegistry['gadgetFinancial'] = gadgetFinancial.settings.updateSettings;
settingsGetterRegistry['gadgetFinancial'] = gadgetFinancial.settings.getSettings;
settingsOpenerRegistry['gadgetFinancial'] = gadgetFinancial.settings.openSettings;
screenActionRegistry['gadgetFinancial'] = gadgetFinancial.screenAction;
</script>

<%-- Start of Export modal dialog --%>
<div id="gadgetFinancialExportDiv" title="" class="" style="display:none; top: 0px;">
	<div id='gadgetFinancialExport_EmbedDataHere'></div>
</div>
<%-- End of Export modal dialog --%>

<script>
	//defaultHeight used for the modal dialog
	var defaultHeight = navigator.userAgent.indexOf("MSIE") != -1 ? 550 : 650;
	var calcHeight = defaultHeight;
	$j("#gadgetFinancialExportDiv").dialog( { height: calcHeight, width: 850, modal: true, 
		resizable: true, autoOpen: false, closeText: '',
		//dragStart: function(event, ui) { calcHeight = $j("#gadgetFinancialExportDiv").dialog( "option", "height" ); },
		//dragStop: function(event, ui) { $j("#gadgetFinancialExportDiv").dialog( "option", "height", calcHeight ); },
		beforeClose: function(event, ui) {},
		buttons: {
	      "<%=LC.L_Close%>": function() {
	      	$j("#gadgetFinancialExportDiv").dialog("close");
	      }
	  	}
	  } 
	);
</script>