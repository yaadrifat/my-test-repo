<!-- Created for INF-18669 for Password Strength and Rotation Error Check : Raviesh  -->

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@page import="com.velos.eres.service.util.Security,com.velos.eres.web.user.UserJB,com.velos.eres.web.portal.PortalJB"%>
<%@page import="org.apache.axis.wsdl.symbolTable.Undefined"%><jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/>

<%
	boolean checkPassRotation=true;

	String newPass = (request.getParameter("newPass")==null)?"":request.getParameter("newPass");
	String defPass = (request.getParameter("defPass")==null)?"":request.getParameter("defPass");
	String userPassword = (request.getParameter("userPassword")==null)?"":request.getParameter("userPassword");
	String newPass_expiry = (request.getParameter("newPass_expiry")==null)?"":request.getParameter("newPass_expiry");
	String userPwd = (request.getParameter("userPwd")==null)?"":request.getParameter("userPwd");
	int portalId = (request.getParameter("portalId")==null)?0:Integer.parseInt(request.getParameter("portalId"));
	int portalLoginId = (request.getParameter("portalLoginId")==null)?0:Integer.parseInt(request.getParameter("portalLoginId"));
	int comeFrom = Integer.parseInt(request.getParameter("comeFrom"));

	HttpSession tSession = request.getSession(true);
	UserJB curUser = (UserJB) tSession.getValue("currentUser");
	Integer userId=curUser.getUserId();
	Integer userAccId=Integer.parseInt(curUser.getUserAccountId());
	String encryptedPass = null;
	
	//Fetching Password Rotation Flag for User Password Rotation
	if(comeFrom==1){
		//Encryption of Password
		encryptedPass=Security.encryptSHA(newPass);
		checkPassRotation=new UserJB().checkPasswordRotation(encryptedPass,userId,userAccId);
	}
	//Fetching Password Rotation Flag for Portal Password Rotation
	else if(comeFrom==2){
		//Encryption of Password
		encryptedPass=Security.encryptSHA(defPass);
		checkPassRotation=new PortalJB().checkPasswordRotation(encryptedPass,portalId,userAccId);
	}
	//Fetching Password Rotation Flag for Portal Login Password Rotation
	else if(comeFrom==3){
		//Encryption of Password
		encryptedPass=Security.encryptSHA(defPass);
		checkPassRotation=new PortalJB().checkPortalLoginPwdRotation(encryptedPass,portalId,userAccId,portalLoginId);
	}
	//Fetching Password Rotation Flag for User Password Expiry Rotation
	else if(comeFrom==5){
		//Encryption of Password
		encryptedPass=Security.encryptSHA(newPass_expiry);
		checkPassRotation=new UserJB().checkPasswordRotation(encryptedPass,userId,userAccId);
	}
	//Fetching Password Rotation Flag for Account Registration Password Expiry Rotation
	else if(comeFrom==6){
		//Encryption of Password
		encryptedPass=Security.encryptSHA(userPwd);
		checkPassRotation=new UserJB().checkPasswordRotation(encryptedPass,userId,userAccId);
	}
	
	out.println(checkPassRotation);
%>
<input type="hidden" id="filter" value="<%=checkPassRotation%>" />