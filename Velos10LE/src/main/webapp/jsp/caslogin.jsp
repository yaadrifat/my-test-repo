<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<TITLE> <%=LC.L_Cas_Login%><%--CAS Login*****--%> </TITLE>

<HEAD>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- <style>
  .centerme{
	margin:auto;
	width:70%; 
	font: normal bold 12px arial;	
	}
</style>  -->
</HEAD>
<script>
var formWin = null;

function openHome(form,act){
var w = screen.availWidth-10;
var h = screen.availHeight-120;

document.login.target="formWin";
document.login.action=act;
//myHome = open('donotdelete.html','formWin','resizable=yes,menubar=yes,toolbar=yes,location=no,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=' + h + ',width=' + w)
myHome = window.open(act,formWin,'resizable=yes,menubar=yes,toolbar=yes,location=yes,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=' + h + ',width=' + w)
if (myHome && !myHome.closed) 
{
//document.login.submit();
//window.location="caslogin.jsp";
window.history.back();
//window.location.href="caslogin.jsp";
myHome.focus();
}
else {
	popupBlock();
}
/*if (myHome && !myHome.closed) myHome.focus();
else {
	alert("popup blocker,please check" + document.login);
	document.login.target="";
	document.login.action="sendtospace.jsp";
	alert(document.login.action);
	document.login.submit();
}*/

//form.submit();	
void(0);
}

function popupBlock()
{
	document.login.target="";
	logname=document.login.loguser.value;
	logurl=document.login.logurl.value;
	document.login.action="caslogin.jsp?mode=popup&logname="+logname+"&logurl="+logurl;
	document.login.submit();
}
	</script>	
<body>
<form name="login"   METHOD="POST" height=100%>


<jsp:useBean id="userLB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="userSessionB" scope="request" class="com.velos.eres.web.userSession.UserSessionJB"/>
<jsp:useBean id="accountB" scope="request" class="com.velos.eres.web.account.AccountJB"/>
<jsp:useBean id="grpRights" scope="session" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="alnot" scope="request" class="com.velos.esch.web.alertNotify.AlertNotifyJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="modCtlDao" scope="request" class="com.velos.eres.business.common.CtrlDao"/>

<!-- Gopu : import the objects for customized filed -->
<%@ page import="java.util.*,java.text.*,com.velos.eres.service.util.*,java.util.*,java.text.SimpleDateFormat,com.velos.eres.web.user.ConfigFacade, com.velos.eres.business.common.*,edu.yale.its.tp.cas.client.filter.CASFilter,com.velos.esch.business.common.SchCodeDao"%>	

<% String login;
 int output=0;
//modifed by Gopu for MAHI Enahncement on 8th Mar 2005
	ConfigFacade.getConfigFacade();

%>
<% login= request.getParameter("username"); 

HttpSession tempSession = request.getSession(false);


//Check if this user has been validated with CAS, if yes, skip the FLOW and let user log in
String ticket= (request.getParameter("ticket"));
ticket=(ticket==null)?"":ticket;
if (ticket.length()>0) {
//String CASUser=(String)tempSession.getAttribute("edu.yale.its.tp.cas.client.filter.user");
String CASUser=(String)tempSession.getAttribute(CASFilter.CAS_FILTER_USER);

CASUser=(CASUser==null)?"":CASUser;
System.out.println("CASUser"+CASUser);
output=5;
userLB.findUser(CASUser);
}

if(!(tempSession == null)){
	tempSession.invalidate();
}
//

%>

<%! String password; %>

<% password =request.getParameter("password"); 
	String scWidth = "";

	String scHeight = "";
	int permittedFailedAccessCount = 0;



	scHeight= request.getParameter("scHeight");

	scWidth = request.getParameter("scWidth");
	

	if (userLB.getUserId()<=0)
	{%>
	<DIV class="centerme">
	<br><br><br><br>
	<%=MC.M_NotAces_ToVeResApp%><%--You do not have access to Velos eResearch Application. Please contact your Velos eResearch Application Administrator<br> Or <br> Click on the link to try Again*****--%> <A href="caslogin.jsp"><%=MC.M_VeResAppLogin%><%--Velos eResearch Application Login*****--%></A>.
	</DIV>
<%	}
	else {
%>
<!-- commented by gopu to fix bugzilla issues # 2439 -->	

<input type="hidden" name="logurl" value=<%=request.getServerName()%>>
<input type="hidden" name="loguser" id="loguser" value="<%=login%>">





<% 
int velosid = 0;
permittedFailedAccessCount = Configuration.FAILEDACCESSCOUNT;
Configuration conf = new Configuration();
String validLoginId = conf.getVelosAdminId(conf.ERES_HOME+"eresearch.xml");
velosid = EJBUtil.stringToNum(validLoginId);


int failCount = 0; int userFound = 0; int origfailCount = 0;
String loginFlagStr = ""; 
int loginFlag = 0;
String failedUserStat = "";


output=userLB.getUserId();
loginFlagStr = userLB.getUserLoginFlag();
if ( (!(loginFlagStr == null )) && (!(loginFlagStr.equals(""))))
 loginFlag = EJBUtil.stringToNum(loginFlagStr);
 else
 loginFlag = 0;
 




String stat = null;
stat = userLB.getUserStatus();
failCount = EJBUtil.stringToNum(userLB.getUserAttemptCount());




 



if (stat.equals("A"))

{
%>
<%
	HttpSession thisSession = request.getSession(true); 
	
	
	thisSession.setAttribute("currentUser",userLB);
  	thisSession.setAttribute("loginname",  userLB.getUserLoginName());

	String uName;
	String sessionTime = null;
	String accSkin = null;
	int userSession = 0;

	accountB.setAccId(EJBUtil.stringToNum(userLB.getUserAccountId()));

	accountB.getAccountDetails();

	thisSession.setAttribute("totalUsersAllowed", accountB.getAccMaxUsr() );
	thisSession.setAttribute("totalPortalsAllowed", accountB.getAccMaxPortal() );//JM:
	thisSession.setAttribute("accMaxStorage", accountB.getAccMaxStorage() );	

	String accName = accountB.getAccName();

	accSkin  = accountB.getAccSkin();
	//if (accSkin == null) accSkin  ="default";
	
	if (accName == null) accName="default";
	
	thisSession.setAttribute("accName", accName );	

	uName = userLB.getUserFirstName() + " " + userLB.getUserLastName();
	
	String modRight = "";
	
	modRight = accountB.getAccModRight();
	thisSession.setAttribute("modRight", modRight );
	
	
	//set user's protocol management module access in session
	
	char protocolManagementRight = '0';
	int protocolMgmtSeq = 0,protocolMgmtModSeq = 0;
	
	modCtlDao.getControlValues("module"); //get extra modules
	
	ArrayList modCtlDaofeature =  modCtlDao.getCValue();
	ArrayList modCtlDaoftrSeq = modCtlDao.getCSeq();
	
	protocolMgmtModSeq = modCtlDaofeature.indexOf("MODPROTOCOL");
	protocolMgmtSeq = ((Integer) modCtlDaoftrSeq.get(protocolMgmtModSeq)).intValue();
	protocolManagementRight = modRight.charAt(protocolMgmtSeq - 1);	
	
	thisSession.setAttribute("protocolManagementRight", String.valueOf(protocolManagementRight) );
	

	String eSign = userLB.getUserESign();

	sessionTime = userLB.getUserSessionTime() ;
	
	thisSession.setAttribute("userName", uName );
	thisSession.setAttribute("eSign", eSign );
	thisSession.setAttribute("ipAdd",request.getRemoteAddr());
	thisSession.setAttribute("userId", String.valueOf(userLB.getUserId()));
	thisSession.setAttribute("accountId", userLB.getUserAccountId());
	thisSession.setAttribute("userSession", sessionTime);
	thisSession.setAttribute("studyId","");
   	thisSession.setAttribute("studyNo","");
	thisSession.setAttribute("userAddressId",userLB.getUserPerAddressId());
	//set screen width n height in session
	thisSession.setAttribute("appScHeight",scHeight);
   	thisSession.setAttribute("appScWidth",scWidth);
	thisSession.setAttribute("sessionName","er");
	thisSession.setAttribute("accSkin",accSkin);
	
	//user's default group
	thisSession.setAttribute("defUserGroup",userLB.getUserGrpDefault());
	
	
	
	//Sonia ABrol, 10/08/07 get the timezone desc from database
	//accessing the DAO directly because this patch is needed for Korea selectively and I want to change minimum java files
	
	SchCodeDao schCode = new SchCodeDao();
	String tzDesc = "";
	tzDesc = schCode.getDefaultTimeZone();
	
	if (StringUtil.isEmpty(tzDesc))
	{
		tzDesc = "";
	}
	thisSession.setAttribute("defTZDesc",tzDesc);
	// end of TZ change


	//bind the session for tracking after setting all the attributes
	EresSessionBinding eresSession = new EresSessionBinding(); 
	thisSession.setAttribute("eresSessionBinder",eresSession);

	userSession = Integer.parseInt(sessionTime) * 60;
	thisSession.setMaxInactiveInterval(userSession);

	//by salil
	grpRights.setId(Integer.parseInt(userLB.getUserGrpDefault()));
	grpRights.getGrpRightsDetails();
	thisSession.setAttribute("GRights",grpRights);
	//end change by salil
	
	
	
	grpRights.setId(Integer.parseInt(userLB.getUserGrpDefault()));
	grpRights.getGrpRightsDetails();
	thisSession.setAttribute("GRights",grpRights);
	
	userLB.setUserESignExpiryDate("01/01/9999");
	userLB.setUserPwdExpiryDate("01/01/9999");
	
	userLB.setUserAttemptCount("0");
	userLB.updateUser();
	

	
//Added by IA 12.4.2006
String theme = ""; 
String homepage = "";
//String description = "";
//CodeDao cd = new CodeDao();

theme = userLB.getUserTheme();
homepage = codeLst.getCodeCustomCol(EJBUtil.stringToNum(theme) );

//description = codeLst.getCodeDescription(EJBUtil.stringToNum(theme) );


if (homepage != null ) {

}else{
	homepage  = "myHome.jsp?srcmenu=tdMenuBarItem1";
}

%>

<SCRIPT>
	openHome('document.login',"<%=homepage%>");
</SCRIPT>
	



<%

}else
{%>
<DIV class="centerme">
	<br><br><br><br>
	<%=MC.M_AccBlocked_ContAdmin%><%--Your Velos eResearch Application account is either blocked or has been deactivated . Please contact your Velos eResearch Application Administrator<br> Or <br> Click on the link to try Again*****--%> <A href="caslogin.jsp"><%=MC.M_VeResAppLogin%><%--Velos eResearch Application Login*****--%></A>.
	</DIV>
<%	}
	}

%>
<!-- Added by gopu to fix bugzilla issues # 2439 -->	

</form>

</body>

</HTML>















 

