<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script>
/* YK 16MAR Enhancement No.2(Enter Key Working in Prepare Specimen)*/
var lastTimeSubmitted = 0;
function fnOnceEnterKeyPress(e) {
	if (e.keyCode == 13 || e.keyCode == 10) {
        	setVals(document.manageCartFrm,document.getElementById('eSign').value);
        	 return false;
        }
   
	}

</script>
<%@ page import="java.util.*,com.velos.eres.service.util.*" %>	

<table width="98%" border="0" cellspacing="0" cellpadding="0" bgcolor="<%=StringUtil.eSignBgcolor%>" >
	<tr valign="baseline" bgcolor="<%=StringUtil.eSignBgcolor%>">
	  <!-- ajax submit for manage cart invp 2.8 -->
	  <td width="50%" align="right" bgcolor="<%=StringUtil.eSignBgcolor%>">  	
		 <span id="eSignMessage"></span>
		 <%=LC.L_Esignature%><%--e-Signature*****--%> <FONT class="Mandatory">* </FONT>&nbsp;
	   </td>	 
	  <td width="20%" bgcolor="<%=StringUtil.eSignBgcolor%>">
		<input type="password" name="eSign" id="eSign" maxlength="8" autocomplete="off"
		   onkeyup="ajaxvalidate('misc:'+this.id,4,'eSignMessage','<%=LC.L_Valid_Esign%><%--Valid e-Sign*****--%>','<%=LC.L_Invalid_Esign%><%--Invalid e-Sign*****--%>','sessUserId')" onkeypress="return fnOnceEnterKeyPress(event)"/>
	  </td>	  
	  
	  <td width="20%" bgcolor="<%=StringUtil.eSignBgcolor%>">		
		<button type="submit" id="submitbutton"  ondblclick="return false" ><%=LC.L_Submit%></button>
	 </td> 
  
  </tr>
  </table>

