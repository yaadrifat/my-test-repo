<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*, org.json.*"%>
<%@ page import="com.velos.eres.gems.business.*"%>
<%@page import="org.apache.axis.types.Entities"%>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC" %>
<%@ page import="com.velos.eres.service.util.StringUtil" %>
<%@ page import="com.velos.eres.widget.business.common.UIFlxPageDao" %>
<%@ page import="com.velos.eres.business.common.StudyStatusDao" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyStatB" scope="request" class="com.velos.eres.web.studyStatus.StudyStatusJB"/>
<jsp:useBean id="codeB" scope="request" class="com.velos.eres.web.codelst.CodelstJB" />
<jsp:useBean id="repdao" scope="request" class="com.velos.eres.business.common.ReportDaoNew" />
<%@ page language = "java" import = "com.velos.eres.service.util.CFG,com.velos.eres.service.util.EJBUtil,
	com.velos.eres.service.util.DateUtil,com.velos.eres.business.common.CodeDao,com.velos.eres.service.util.VelosResourceBundle"%>
	<%@ page import="com.velos.eres.compliance.web.ProtocolChangeLog"%>
<link href="styles/studyVersion.css" rel="stylesheet" type="text/css" />
<%
HttpSession tSession = request.getSession(false);
if (!sessionmaint.isValidSession(tSession))
	return;

String studyId = request.getParameter("studyId");

String versionNum = "0";
int changeReqNum = 0;
int reportNum = 0;

String statStr = "";
String validDateStr = "";
String notesStr = "";
String repStr = "";

StringBuffer statBuff = new StringBuffer().append("[");
StringBuffer validDateBuff = new StringBuffer().append("[");
StringBuffer notesBuff = new StringBuffer().append("[");

if(StringUtil.stringToNum(studyId) > 0) {
	UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
	uiFlxPageDao.getHighestFlexPageFullVersion("er_study", Integer.parseInt(studyId));
	versionNum = String.valueOf(uiFlxPageDao.getMajorVer());
	if (uiFlxPageDao.getMinorVer() > 0) {
		versionNum += "."+uiFlxPageDao.getMinorVer();
	}
	
	String selsite = request.getParameter("selsite") ;
	String selOrg = request.getParameter("accsites") ;
	if(selOrg == null)
		selOrg = selsite;
	String userIdFromSession = (String) tSession.getValue("userId");
	int accountId = EJBUtil.stringToNum((String) tSession.getValue("accountId"));
	
	ArrayList<String> checkSubmission = new ArrayList<String>() {{add("crcSubmitted"); add("crcResubmitted"); add("irb_init_subm");}};
	ArrayList<String> checkStatus = new ArrayList<String>() {{add("crcReturn"); add("irb_add_info"); add("medAncRevPend"); }};
	
	StudyStatusDao studyStatDao = studyStatB.getStudyStatusDesc(Integer.parseInt(studyId), EJBUtil.stringToNum(selOrg), EJBUtil.stringToNum(userIdFromSession), accountId);
	ArrayList subtypes = studyStatDao.getSubTypeStudyStats();
	ArrayList statusArr = studyStatDao.getDescStudyStats();
	ArrayList validDateArr = studyStatDao.getStatStartDates();
	ArrayList notesArr = studyStatDao.getStatNotes();
	
	if(!checkSubmission.contains((String)subtypes.get(0))){
		int index = 0;
		countChangeRequest:
		while(index < subtypes.size()){
			if(!checkSubmission.contains((String)subtypes.get(index)) &&  checkStatus.contains((String)subtypes.get(index))){
				String notes = (String)((notesArr.get(index)==null)?"-":notesArr.get(index));
				//notes = notes.replaceAll("[^\\w\\s\\-_]", "").replaceAll("(\\r|\\n|\\r\\n)+", "<br>"); //To remove all special characters and fix line break
				notes = notes.replaceAll("(\\r|\\n|\\r\\n)+", "<br>");
				String startDate = (null == validDateArr.get(index))? "" : validDateArr.get(index).toString().substring(0,10);
				startDate = (StringUtil.isEmpty(startDate))? "" : DateUtil.dateToString(java.sql.Date.valueOf(startDate));

				if(changeReqNum==0){
					statBuff.append("'"+statusArr.get(index)+"'");
					validDateBuff.append("'"+ startDate +"'"); //validDateArr.get(index)
					notesBuff.append("'"+notes+"'");
				} else {
					statBuff.append(",'"+statusArr.get(index)+"'");
					validDateBuff.append(",'"+ startDate +"'");
					notesBuff.append(",'"+notes+"'");
				}
				++changeReqNum;
			}
			if(checkSubmission.contains((String)subtypes.get(index))){
				break countChangeRequest;
			}
			++index;
		}
	}
}

statStr = statBuff.append("]").toString();
validDateStr = validDateBuff.append("]").toString();
notesStr = notesBuff.append("]").toString();

CodeDao codeDao = codeB.getCodeDaoInstance();
codeDao.getCodeValues("report", "study", "codelst_seq");
ArrayList codeSubTypeList = codeDao.getCSubType();
ArrayList codeDescList = codeDao.getCDesc();
ArrayList repIds = new ArrayList();
ArrayList repNames = new ArrayList();
ArrayList repColumns = new ArrayList();
ArrayList repFilters = new ArrayList();
ArrayList reportFilterKeywords = new ArrayList();
ArrayList reportFilterApplicable = new ArrayList();
String repColumn = "", repFilter = "", reportFilterKeyword = "",repFilterApplicable="";
int repId = 0;
String acc = (String) tSession.getValue("accountId");
int accId = EJBUtil.stringToNum(acc);
String codeDescListStr ="";	
int startIndx =0;
int stopIndx =0;
String labelKeyword ="";
String labelString ="";
String messageKeyword ="";
String messageString ="";
String repNamesStr ="";
String messageKey ="";
String tobeReplaced ="";
String messageParaKeyword []=null;

StringBuffer repNameBuff = new StringBuffer().append("[");

for (int i = 0; i < codeSubTypeList.size(); i++) {
	repdao.getReports((String) codeSubTypeList.get(i),
			accId);
	repIds = repdao.getPkReport();
	repNames = repdao.getRepName();
	repColumns = repdao.getRepColumns();
	repFilters = repdao.getRepFilters();
	reportFilterKeywords = repdao.getReportFilterKeywords();
	reportFilterApplicable = repdao.getRepFilterApplicable();	
	reportNum += repIds.size();
	
	//Adding report names to the buffer
	for(int count = 0; count < repNames.size(); count++){
		if(i == 0 && count == 0)
			repNameBuff.append("'"+repNames.get(count)+"'");
		else
			repNameBuff.append(",'"+repNames.get(count)+"'");
	}
	
	codeDescListStr = (String)codeDescList.get(i);
	codeDescListStr = (codeDescListStr == null) ? "" :codeDescListStr;
	//KM-Study and Patient Label changes in codelst description for reports.
	while(codeDescListStr.contains("VELLABEL[")){
		startIndx = codeDescListStr.indexOf("VELLABEL[") + "VELLABEL[".length();
		stopIndx = codeDescListStr.indexOf("]",startIndx);
		labelKeyword = codeDescListStr.substring(startIndx, stopIndx);
		labelString = LC.getLabelByKey(labelKeyword);
		codeDescListStr=StringUtil.replaceAll(codeDescListStr, "VELLABEL["+labelKeyword+"]", labelString);
	}

	while(codeDescListStr.contains("VELMESSGE[")){
		startIndx = codeDescListStr.indexOf("VELMESSGE[") + "VELMESSGE[".length();
		stopIndx = codeDescListStr.indexOf("]",startIndx);
		messageKeyword = codeDescListStr.substring(startIndx, stopIndx);
		messageString = MC.getMessageByKey(messageKeyword);
		codeDescListStr=StringUtil.replaceAll(codeDescListStr, "VELMESSGE["+messageKeyword+"]", messageString);
	}
	while(codeDescListStr.contains("VELPARAMESSGE[")){
		startIndx = codeDescListStr.indexOf("VELPARAMESSGE[") + "VELPARAMESSGE[".length();
		stopIndx = codeDescListStr.indexOf("]",startIndx);
		messageKeyword = codeDescListStr.substring(startIndx, stopIndx);
		messageKey=messageKeyword.substring(0, messageKeyword.indexOf("||"));
		messageKey=messageKey.trim();
		messageParaKeyword=messageKeyword.substring(messageKeyword.indexOf("||")+2).split(java.util.regex.Pattern.quote("||"));
		messageString = VelosResourceBundle.getMessageString(messageKey,messageParaKeyword);
		codeDescListStr=StringUtil.replaceAll(codeDescListStr, "VELPARAMESSGE["+messageKeyword+"]", messageString);
	}
	if(!codeDescListStr.contains("word.GIF")){
		if(codeDescListStr.contains(LC.L_Word_Format)){
			tobeReplaced = "<img border=\"0\" title=\""+LC.L_Word_Format+"\" alt=\""+LC.L_Word_Format+"\" src=\"./images/word.GIF\"></img>";
			codeDescListStr=StringUtil.replace(codeDescListStr, LC.L_Word_Format, tobeReplaced);
		}
		if(codeDescListStr.contains(LC.L_Excel_Format)){
			tobeReplaced = "<img border=\"0\" title=\""+LC.L_Excel_Format+"\" alt=\""+LC.L_Excel_Format+"\" src=\"./images/excel.GIF\"></img>";
			codeDescListStr=StringUtil.replace(codeDescListStr,LC.L_Excel_Format, tobeReplaced);
		}
		if(codeDescListStr.contains(LC.L_Printer_FriendlyFormat)){
			tobeReplaced = "<img border=\"0\" src=\"./images/printer.gif\" title=\""+LC.L_Printer_FriendlyFormat+"\" alt=\""+LC.L_Printer_FriendlyFormat+"\"></img>";
			codeDescListStr=StringUtil.replace(codeDescListStr, LC.L_Printer_FriendlyFormat, tobeReplaced);
		}
	}
		
	for (int j = 0; j < repIds.size(); j++) {
		repId = ((Integer) repIds.get(j)).intValue();
		repColumn = (String) repColumns.get(j);
		repColumn = (repColumn == null) ? "" : repColumn;
		repFilter = (String) repFilters.get(j);
		repFilter = (repFilter == null) ? "" : repFilter;
		reportFilterKeyword = (String) reportFilterKeywords
		.get(j);
		reportFilterKeyword = (reportFilterKeyword == null) ? ""
		: reportFilterKeyword;
		repFilterApplicable=(String) reportFilterApplicable.get(j);
		repFilterApplicable=(repFilterApplicable==null)?"":repFilterApplicable;
		
		while (repColumn.contains("VELLABEL[")){
			startIndx = repColumn.indexOf("VELLABEL[") + "VELLABEL[".length();
			stopIndx = repColumn.indexOf("]",startIndx);
			labelKeyword = repColumn.substring(startIndx, stopIndx);
			labelString = LC.getLabelByKey(labelKeyword);
			repColumn=StringUtil.replaceAll(repColumn, "VELLABEL["+labelKeyword+"]", labelString);
		}
		while(repColumn.contains("VELMESSGE[")){
			startIndx = repColumn.indexOf("VELMESSGE[") + "VELMESSGE[".length();
			stopIndx = repColumn.indexOf("]",startIndx);
			messageKeyword = repColumn.substring(startIndx, stopIndx);
			messageString = MC.getMessageByKey(messageKeyword);
			repColumn=StringUtil.replaceAll(repColumn, "VELMESSGE["+messageKeyword+"]", messageString);
		}
		while(repColumn.contains("VELPARAMESSGE[")){
			startIndx = repColumn.indexOf("VELPARAMESSGE[") + "VELPARAMESSGE[".length();
			stopIndx = repColumn.indexOf("]",startIndx);
			messageKeyword = repColumn.substring(startIndx, stopIndx);
			messageKey=messageKeyword.substring(0, messageKeyword.indexOf("||"));
			messageKey=messageKey.trim();
			messageParaKeyword=messageKeyword.substring(messageKeyword.indexOf("||")+2).split(java.util.regex.Pattern.quote("||"));
			messageString = VelosResourceBundle.getMessageString(messageKey,messageParaKeyword);
			repColumn=StringUtil.replaceAll(repColumn, "VELPARAMESSGE["+messageKeyword+"]", messageString);
		}
		if(!repColumn.contains("word.GIF")){
			if(repColumn.contains(LC.L_Word_Format)){
				tobeReplaced = "<img border=\"0\" title=\""+LC.L_Word_Format+"\" alt=\""+LC.L_Word_Format+"\" src=\"./images/word.GIF\"></img>";
				repColumn=StringUtil.replace(repColumn, LC.L_Word_Format, tobeReplaced);
			}
			if(repColumn.contains(LC.L_Excel_Format)){
				tobeReplaced = "<img border=\"0\" title=\""+LC.L_Excel_Format+"\" alt=\""+LC.L_Excel_Format+"\" src=\"./images/excel.GIF\"></img>";
				repColumn=StringUtil.replace(repColumn,LC.L_Excel_Format, tobeReplaced);
			}
			if(repColumn.contains(LC.L_Printer_FriendlyFormat)){
				tobeReplaced = "<img border=\"0\" src=\"./images/printer.gif\" title=\""+LC.L_Printer_FriendlyFormat+"\" alt=\""+LC.L_Printer_FriendlyFormat+"\"></img>";
				repColumn=StringUtil.replace(repColumn, LC.L_Printer_FriendlyFormat, tobeReplaced);
			}
		}

		
		while (repFilter.contains("VELLABEL[")){
			startIndx = repFilter.indexOf("VELLABEL[") + "VELLABEL[".length();
			stopIndx = repFilter.indexOf("]",startIndx);
			labelKeyword = repFilter.substring(startIndx, stopIndx);
			labelString = LC.getLabelByKey(labelKeyword);
			repFilter=StringUtil.replaceAll(repFilter, "VELLABEL["+labelKeyword+"]", labelString);
		}
		while(repFilter.contains("VELMESSGE[")){
			startIndx = repFilter.indexOf("VELMESSGE[") + "VELMESSGE[".length();
			stopIndx = repFilter.indexOf("]",startIndx);
			messageKeyword = repFilter.substring(startIndx, stopIndx);
			messageString = MC.getMessageByKey(messageKeyword);
			repFilter=StringUtil.replaceAll(repFilter, "VELMESSGE["+messageKeyword+"]", messageString);
		}
		while(repFilter.contains("VELPARAMESSGE[")){
			startIndx = repFilter.indexOf("VELPARAMESSGE[") + "VELPARAMESSGE[".length();
			stopIndx = repFilter.indexOf("]",startIndx);
			messageKeyword = repFilter.substring(startIndx, stopIndx);
			messageKey=messageKeyword.substring(0, messageKeyword.indexOf("||"));
			messageKey=messageKey.trim();
			messageParaKeyword=messageKeyword.substring(messageKeyword.indexOf("||")+2).split(java.util.regex.Pattern.quote("||"));
			messageString = VelosResourceBundle.getMessageString(messageKey,messageParaKeyword);
			repFilter=StringUtil.replaceAll(repFilter, "VELPARAMESSGE["+messageKeyword+"]", messageString);
		}
		if(!repFilter.contains("word.GIF")){
			if(repFilter.contains(LC.L_Word_Format)){
				tobeReplaced = "<img border=\"0\" title=\""+LC.L_Word_Format+"\" alt=\""+LC.L_Word_Format+"\" src=\"./images/word.GIF\"></img>";
				repFilter=StringUtil.replace(repFilter, LC.L_Word_Format, tobeReplaced);
			}
			if(repFilter.contains(LC.L_Excel_Format)){
				tobeReplaced = "<img border=\"0\" title=\""+LC.L_Excel_Format+"\" alt=\""+LC.L_Excel_Format+"\" src=\"./images/excel.GIF\"></img>";
				repFilter=StringUtil.replace(repFilter,LC.L_Excel_Format, tobeReplaced);
			}
			if(repFilter.contains(LC.L_Printer_FriendlyFormat)){
				tobeReplaced = "<img border=\"0\" src=\"./images/printer.gif\" title=\""+LC.L_Printer_FriendlyFormat+"\" alt=\""+LC.L_Printer_FriendlyFormat+"\"></img>";
				repFilter=StringUtil.replace(repFilter, LC.L_Printer_FriendlyFormat, tobeReplaced);
			}
		}

		//KM-Study and Patient Label changes for report names.
		repNamesStr = (String)repNames.get(j);
		repNamesStr = (repNamesStr == null) ? "" : repNamesStr;
		while (repNamesStr.contains("VELLABEL[")){
			startIndx = repNamesStr.indexOf("VELLABEL[") + "VELLABEL[".length();
			stopIndx = repNamesStr.indexOf("]",startIndx);
			labelKeyword = repNamesStr.substring(startIndx, stopIndx);
			labelString = LC.getLabelByKey(labelKeyword);
			repNamesStr=StringUtil.replaceAll(repNamesStr, "VELLABEL["+labelKeyword+"]", labelString);
		}
		
		while(repNamesStr.contains("VELMESSGE[")){
			startIndx = repNamesStr.indexOf("VELMESSGE[") + "VELMESSGE[".length();
			stopIndx = repNamesStr.indexOf("]",startIndx);
			messageKeyword = repNamesStr.substring(startIndx, stopIndx);
			messageString = MC.getMessageByKey(messageKeyword);
			repNamesStr=StringUtil.replaceAll(repNamesStr, "VELMESSGE["+messageKeyword+"]", messageString);
		}
		while(repNamesStr.contains("VELPARAMESSGE[")){
			startIndx = repNamesStr.indexOf("VELPARAMESSGE[") + "VELPARAMESSGE[".length();
			stopIndx = repNamesStr.indexOf("]",startIndx);
			messageKeyword = repNamesStr.substring(startIndx, stopIndx);
			messageKey=messageKeyword.substring(0, messageKeyword.indexOf("||"));
			messageKey=messageKey.trim();
			messageParaKeyword=messageKeyword.substring(messageKeyword.indexOf("||")+2).split(java.util.regex.Pattern.quote("||"));
			messageString = VelosResourceBundle.getMessageString(messageKey,messageParaKeyword);
			repNamesStr=StringUtil.replaceAll(repNamesStr, "VELPARAMESSGE["+messageKeyword+"]", messageString);
		}
		if(!repNamesStr.contains("word.GIF")){
			if(repNamesStr.contains(LC.L_Word_Format)){
				tobeReplaced = "<img border=\"0\" title=\""+LC.L_Word_Format+"\" alt=\""+LC.L_Word_Format+"\" src=\"./images/word.GIF\"></img>";
				repNamesStr=StringUtil.replace(repNamesStr, LC.L_Word_Format, tobeReplaced);
			}
			if(repNamesStr.contains(LC.L_Excel_Format)){
				tobeReplaced = "<img border=\"0\" title=\""+LC.L_Excel_Format+"\" alt=\""+LC.L_Excel_Format+"\" src=\"./images/excel.GIF\"></img>";
				repNamesStr=StringUtil.replace(repNamesStr,LC.L_Excel_Format, tobeReplaced);
			}
			if(repNamesStr.contains(LC.L_Printer_FriendlyFormat)){
				tobeReplaced = "<img border=\"0\" src=\"./images/printer.gif\" title=\""+LC.L_Printer_FriendlyFormat+"\" alt=\""+LC.L_Printer_FriendlyFormat+"\"></img>";
				repNamesStr=StringUtil.replace(repNamesStr, LC.L_Printer_FriendlyFormat, tobeReplaced);
			}
		}
		repdao.resetDao();
	}
}

repStr = repNameBuff.append("]").toString();

ProtocolChangeLog protocolChangeLog = new ProtocolChangeLog();
//String changeVersion = protocolChangeLog.returnProtocolVersionDiff("1.0", "2.0", StringUtil.stringToNum(studyId), (String) tSession.getValue("defUserGroup")) ;
String changeVersion = protocolChangeLog.getAllProtocolVersions(StringUtil.stringToNum(studyId), (String) tSession.getValue("defUserGroup"));

%>

<SCRIPT LANGUAGE="JavaScript" SRC="./js/velos/excanvas.js"></SCRIPT>
<canvas id="static" style="display:none"></canvas>
<script type="text/javascript">
function f_open_version(){
	if(document.getElementById("versionDiv")==null)
		{
		var versionDiv = document.createElement('div');
		versionDiv.id = 'versionDiv';
		versionDiv.name = 'versionDiv';
		versionDiv.style.display = 'none';
		versionDiv.innerHTML="<%=changeVersion%>";
		(document.getElementsByTagName("form")[0]).appendChild(versionDiv);
		}
		
	var versionCount = <%=versionNum%>;
	if(versionCount>0){
		$j("#versionDiv").dialog({
			minHeight: 400,
			minWidth: 500,
			maxHeight: 400,
			maxWidth: 500,
	        modal: false,
	        draggable: true,
	        resizable: false,
	        closeOnEscape: true,
	        //stack: true,
	        title: 'Versions: ',
	        position: ['center', 'center'],
	        show: 'blind',
	        hide: 'blind',
	        width: 400,
	        dialogClass: 'ui-dialog-osx',
	        buttons: {
	            "Close": function() {
	                $j(this).dialog("close");
	            }
	        }
	    });
	}
}
function f_open_versionDetails(node) {
	if(document.getElementById("versionDetailsDiv")==null)
	{
	 var versionDetailsDiv = document.createElement('div');
	  versionDetailsDiv.id = 'versionDetailsDiv';
	  versionDetailsDiv.name = 'versionDetailsDiv';
	  versionDetailsDiv.style.overflowY = 'scroll';
	  (document.getElementsByTagName("form")[0]).appendChild(versionDetailsDiv);
	  versionDetailsDiv.style.display = 'none';
	}
		$j("#versionDetailsDiv").dialog({
				minHeight: 600,
				minWidth: 750,
				maxHeight: 600,
				maxWidth: 750,
				modal: false,
		        //stack: true,
		        draggable: false,
		        resizable: true,
		        closeOnEscape: true,
		        title: 'Version Details: ',
		        position: ['center', 'center'],
		        show: 'blind',
		        hide: 'blind',
		        width: 600,
		        height: 500,
		        dialogClass: 'ui-dialog-osx',
		        open :  function () {
		         	$j(this).load(node);
		        },		       
		        buttons: {
		            "Close": function() {
		                $j(this).dialog("close");
		            }
		        }
		    });
}

function f_open_versionChangeLog(node) {
	if(document.getElementById("versionChangeLogDiv")==null)
	{
	 var versionChangeLogDiv = document.createElement('div');
	 versionChangeLogDiv.id = 'versionChangeLogDiv';
	 versionChangeLogDiv.name = 'versionChangeLogDiv';
	 versionChangeLogDiv.style.overflowY = 'scroll';
	  (document.getElementsByTagName("form")[0]).appendChild(versionChangeLogDiv);
	  versionChangeLogDiv.style.display = 'none';
	}
		$j("#versionChangeLogDiv").dialog({
				minHeight: 600,
				minWidth: 750,
				maxHeight: 600,
				maxWidth: 750,
				modal: false,
		        //stack: true,
		        draggable: false,
		        resizable: true,
		        closeOnEscape: true,
		        title: 'Version Details: ',
		        position: ['center', 'center'],
		        show: 'blind',
		        hide: 'blind',
		        width: 600,
		        height: 500,
		        dialogClass: 'ui-dialog-osx',
		        open :  function () {
		         	$j(this).load(node);
		        },		       
		        buttons: {
		            "Close": function() {
		                $j(this).dialog("close");
		            }
		        }
		    });
}

function f_open_request(){
	var labelArr = ['Study Status', 'Status Date', 'Notes'];

	var statusArr = <%=statStr%>;
	var validDateArr = <%=validDateStr%>;
	var notesArr = <%=notesStr%>;
	
	var changeReqDiv = document.createElement('div');
	changeReqDiv.id = 'changeReqDiv';
	changeReqDiv.name = 'changeReqDiv';
	changeReqDiv.style.display = 'none';
	changeReqDiv.innerHTML='Change Request Data'+statusArr+validDateArr+notesArr;
	(document.getElementsByTagName("form")[0]).appendChild(changeReqDiv);
	
	var changeReqDivInHTML = '';
	changeReqDivInHTML = '<b>Details</b>';
	changeReqDivInHTML += '<table border="solid 1">';	
	changeReqDivInHTML += '<TR width="100%">';
	var factor = 100/(labelArr.length);
	for(var indx=0; indx < labelArr.length; indx++) {
		changeReqDivInHTML += '<TH width= '+factor+'%>';
		changeReqDivInHTML += labelArr[indx];
		changeReqDivInHTML += '</TH>';
	}
	changeReqDivInHTML += '</TR>';
	for(var indx=0; indx < statusArr.length; indx++) {
		changeReqDivInHTML += '<TR width=100%>';
		changeReqDivInHTML += '<TD width= '+factor+'%>';
		changeReqDivInHTML += statusArr[indx];
		changeReqDivInHTML += '</TD>';
		changeReqDivInHTML += '<TD width= '+factor+'% align=center>';
		changeReqDivInHTML += validDateArr[indx];
		changeReqDivInHTML += '</TD>';
		changeReqDivInHTML += '<TD width= '+factor+'%>';
		changeReqDivInHTML +=  notesArr[indx];
		changeReqDivInHTML += '</TD>';
		changeReqDivInHTML += '</TR>';
	}
	changeReqDivInHTML += '</table>';
	
	changeReqDiv.innerHTML= changeReqDivInHTML;
	
	var changeReqDiv = document.getElementById ('changeReqDiv');
	
	if(statusArr.length > 0){
		$j("#changeReqDiv").dialog({
			minHeight: 300,
			minWidth: 700,
			maxHeight: 400,
			maxWidth: 500,
	        modal: true,
	        draggable: true,
	        resizable: false,
	        closeOnEscape: true,
	        title: 'Messsages: ',
	        position: ['center', 'center'],
	        show: 'blind',
	        hide: 'blind',
	        width: 400,
	        dialogClass: 'ui-dialog-osx',
	        buttons: {
	            "Close": function() {
	                $j(this).dialog("close");
	            }
	        }
	    });
	}
}

function f_open_reports(){
	var reportNum = <%=reportNum%>;
	var repStr = <%=repStr%>;
	
	var repSelVal;
	
	var repDivInHTML = '';
	repDivInHTML = '<b>Select Report &nbsp</b>';
	repDivInHTML += '<select id="repSelect">';	
	for(var indx = 0; indx < repStr.length; indx++) {
		repDivInHTML += '<option value="'+repStr[indx]+'">'+repStr[indx]+'</option>';
	}
	repDivInHTML += '</select>';
	
	repDivInHTML += '<br><span id="repDispSpan">This is a test paragraph</>'
		
	var reportsDiv = document.createElement('div');
	reportsDiv.id = 'reportsDiv';
	reportsDiv.name = 'reportsDiv';
	reportsDiv.style.display = 'none';
	(document.getElementsByTagName("form")[0]).appendChild(reportsDiv);	
	reportsDiv.innerHTML = repDivInHTML;
	
	if(reportNum>0){
		$j("#reportsDiv").dialog({
			minHeight: 200,
			minWidth: 500,
			maxHeight: 400,
			maxWidth: 500,
	        modal: true,
	        draggable: true,
	        resizable: false,
	        closeOnEscape: true,
	        title: 'Reports: ',
	        position: ['center', 'center'],
	        show: 'blind',
	        hide: 'blind',
	        width: 400,
	        dialogClass: 'ui-dialog-osx',
	        buttons: {
	        	"Display": function() {
	            	//$j("#repDispSpan").slideToggle();
	        		//dispRep();
	        		$j(this).load('testTable.jsp');
	            },
	            "Toggle": function() {
	            	$j("#repDispSpan").slideToggle();
	            },
	            "Close": function() {
	                $j(this).dialog("close");
	            }
	        }
	    });
	}
}

function dispRep(){
	var selRep = document.getElementById("repSelect").value;

	var dispRepDiv = document.createElement('div');
	dispRepDiv.id = 'dispRepDiv';
	dispRepDiv.name = 'dispRepDiv';
	dispRepDiv.style.display = 'none';
	(document.getElementsByTagName("form")[0]).appendChild(dispRepDiv);	
	dispRepDiv.innerHTML =  $j('#dispRepDiv').load('reportDisplay.jsp');
	
	$j("#dispRepDiv").dialog({
		minHeight: 800,
		minWidth: 1000,
		maxHeight: 800,
		maxWidth: 1000,
        modal: true,
        draggable: true,
        resizable: false,
        closeOnEscape: true,
        title: selRep,
        position: ['center', 'center'],
        show: 'blind',
        hide: 'blind',
        width: 400,
        dialogClass: 'ui-dialog-osx',
        buttons: {
            "Close": function() {
                $j(this).dialog("close");
            }
        }
    });
    
	/*$j('#dispRepDiv').on('click', function() {
		   $j('#mydiv').load('mypage.jsp');
		   return false;
		});*/
}

jQuery(document).ready( function() {
	// Static
    var canvas = document.getElementById("static");
    if(typeof G_vmlCanvasManager !== "undefined")
    	G_vmlCanvasManager.initElement(canvas);
    
    var ctx = canvas.getContext("2d");
    ctx.fillStyle = "#FF0000";
	ctx.beginPath();
	ctx.arc(50,50,12,0,Math.PI*2,true);
	ctx.closePath();
	ctx.fill();

	var versionCount = <%=versionNum%>;
	var changeReqNum = <%=changeReqNum%>;
	var reportNum = <%=reportNum%>;
	
	$j("#progressIconsTab").append("<table id='progressIconsTbl' name='progressIconsTbl'>"
		+ "<tr>"
		+ "<td align='center' onclick='f_open_version()' onmouseover='' style='cursor: pointer;'>"
		+ "<input class='knob' onmouseover='' style='cursor: pointer;' id='versionCount' name='versionCount' data-height='50' data-width='50' data-min='0' data-max='50' value= '"+versionCount+"'/>"
		+ "<br>"
		+ "<label>Version</label>"
		+ "</td>"
		+ "<td align='center' onclick='f_open_request()' onmouseover='' style='cursor: pointer;'>"
		+ "<input class='knob' onmouseover='' style='cursor: pointer;' id='cRequestCount' name='cRequestCount' data-height='50' data-width='50' data-min='0' data-max='50' value='"+changeReqNum+"'/>"
		+ "<br>"
		+ "<label>Messages</label>"
		+ "</td>"
		+ "<td align='center' onclick='f_open_reports()' onmouseover='' style='cursor: pointer;'>"
		+ "<input class='knob' onmouseover='' style='cursor: pointer;' id='reportCount' name='reportCount' data-height='50' data-width='50' data-min='0' data-max='50' value='"+reportNum+"'/>"
		+ "<br>"
		+ "<label>Reports</label>"
		+ "</td>"
		+ "</tr>"
		+ "</table>");
	
	$j("#versionCount").knob({ 'max':50, readOnly: true });
	$j("#cRequestCount").knob({ 'max':50, readOnly: true });
	$j("#reportCount").knob({ 'max':50, readOnly: true });
	
	var offset = $j("#versionCount").offset();
    $j("#versionCount").css("position", "inline");
    $j("#versionCount").offset(offset);
	
	$j("#progressIconsTab").append('<table width="100%" height="2" class="tabBorder"><tbody><tr><td></td></tr></tbody></table>');
});
</script>