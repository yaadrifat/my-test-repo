<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Admin_Rpts%><%--Admin Reports*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT>
function fMakeVisible(type){
	document.all("year_t").style.visibility = "hidden";
	document.all("month_t").style.visibility = "hidden";
	document.all("date_t").style.visibility = "hidden";
	typ = document.all(type);
	typ.style.visibility = "visible";
}

function fOpenWindow(type,section,form){
	if (section==2) { //uncheck the upper filter range if lower range is selected
		for(i=0;i<form.filterType.length;i++)	{
			form.filterType[i].checked=false;
		}
	}
	
	if (section==1) { //uncheck the lower filter range if upper range is selected
		for(i=0;i<form.filterType2.length;i++)	{
			form.filterType2[i].checked=false;
		}
	}

	width=300;
	height= 300;
	if (type == "Y"){
		width=200;
		height=70;
	}
	if (type == "D"){
		width=520;
		height=320;
	}
	if (type == "M"){
		width=335;
		height=70;
	}		
	form.year.value ="";
	form.year1.value ="";
	form.month.value ="";
	form.dateFrom.value ="";
	form.dateTo.value ="";
	for (cnt=0;cnt <2;cnt++){
		form.range[cnt].value ="";
	}
	form.range[section - 1].value ="";
	if (type == 'A'){
			form.range[section - 1].value = "All"
	}
	 if (type != 'A'){
		windowName=window.open("selectDateRange.jsp?parm="+type +"&section=" +section,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=" +width +",height="+height)
		windowName.focus();
	}

}

function fValidate(form){
	reportChecked=false;
	for(i=0;i<form.reportName.length;i++){
		sel = form.reportName[i].checked;
		if (form.reportName[i].checked){
	   		reportChecked=true;
		}
	}
	if (reportChecked==false) {
		alert("<%=MC.M_Selc_Rpt%>");/*alert("Please select a Report");*****/
		return false;
	}
	
	reportNo = form.repId.value;

	switch (reportNo) {
		case "1": //Active Protocol
			case "6":
			dateChecked=false;
			/*for(i=0;i<document.reports.filterType.length;i++){
				sel = document.reports.filterType[i].checked;
				if (document.reports.filterType[i].checked){
	   				dateChecked=true;
				}
			}*/
			
//			alert("*" +document.reports.range[0].value +"*")
			if (form.range[0].value != ""){
	   				dateChecked=true;
			}
			
			if (dateChecked==false) {
				alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/
				return false;
			}
			break;
			
		case "2": //Protocol by user			
			if (form.selUser.value == "None") {
				alert("<%=MC.M_Selc_User%>");/*alert("Please select a User");*****/
				return false;
			}	
			break;
		case "5": //Protocol by Therapeutic Area
			dateChecked=false;
			/*for(i=0;i<document.reports.filterType3.length;i++){
				sel = document.reports.filterType3[i].checked;
				if (document.reports.filterType3[i].checked){
	   				dateChecked=true;
				}
			}*/

			if (form.range[1].value != ""){
   				dateChecked=true;
			}

			if (dateChecked==false) {
				alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/
				return false;
			}
			
			if (form.tArea.value == "") {
				alert("<%=MC.M_Selc_ThrpArea%>");/*alert("Please select a Therapeutic Area");*****/
				return false;
			}	
			
			break;			

		case "7": //Protocol by Organization
			if (form.orgn.value == "") {
				alert("<%=MC.M_Selc_AnOrg%>");/*alert("Please select an Organization");*****/
				return false;
			}	
			
			break;
			
		case "43": //User Profile
			if (form.selUser.value == "None") {
				alert("<%=MC.M_Selc_User%>");/*alert("Please select a User");*****/
				return false;
			}	
			
			break;
								
			for(i=0;i<form.filterType.length;i++)	{
				if (form.filterType[i].checked){
				
				if (form.filterType[i].value == "4") {
					if (form.dateFrom.value == "") {
						alert("<%=MC.M_Selc_FromDate%>");/*alert("Please select From Date");*****/
						return false;
					}
					if (form.dateTo.value == "") {
						alert("<%=MC.M_Selc_ToDate%>");/*alert("Please select To Date");*****/
						return false;
					}
	
				}
				}
			}	
		}	
		form.submit();	
}

function fSetId(ddtype, frmname){
	if (ddtype == "report") {//report Id and name are concatenated by %.Need to separate them
		//alert(frmname.name);		
		for(i=0;i<frmname.reportName.length;i++)	{
			if (frmname.reportName[i].checked){
				lsReport = frmname.reportName[i].value;	
				ind = lsReport.indexOf("%");
				frmname.repId.value = lsReport.substring(0,ind);	
				frmname.repName.value = lsReport.substring(ind+1,lsReport.length);
				break;
			}
		}
	}
}
</SCRIPT>

<SCRIPT language="JavaScript1.1">
function openwin12() {
      windowName=window.open("usersearchdetails.jsp?fname=&lname=","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400")
	windowName.focus();
;}
</SCRIPT>


</head>



<% String src="";

src= request.getParameter("srcmenu");

%>	





<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="repdao" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="repdao1" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="repdao2" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="repdao3" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>

<%@ page language = "java" import = "com.velos.eres.business.section.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@page import="com.velos.eres.service.util.LC"%><%@page import="com.velos.eres.service.util.MC"%>
<body>

<br>

<DIV class="browserDefault_veloshome" id="div1">
<% 
HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))

		{
     	StringBuffer orgn = new StringBuffer();	
     	StringBuffer user = new StringBuffer();
     	StringBuffer user1 = new StringBuffer();
     	Calendar cal = Calendar.getInstance();
     	int currYear = cal.get(cal.YEAR);
     
     	int userId=0;
     	int counter=0;
       	UserDao userDao = new UserDao();
     
     %>
	<jsp:include page="adminreporttabs.jsp" flush="true"/> 


	<%/* SV, 8/24/04, fix for bug 1673, _new is not a keyword, _blank is and it opens new window instance each time which is what we want for reports.*/%>
	
		 
	<Form Name="reports" method="post" action="adminrepRetrieve.jsp" target="_blank">
   
         <input type="hidden" name="srcmenu" value='<%=src%>'>
         
         <br>
         <table width="100%" cellspacing="0" cellpadding="0" border=0 >
         <tr>
          <td colspan = 3>
<%--         <p class = "defComments">Select a report from the list below, specify appropriate filters and click on the 'Display' button. </p> --%>
         <p class = "defComments"><%=MC.M_SelRpt_ClkDisp%><%--Select a report from the list below and click on the 'Display' button.*****--%> </p>
         <BR>
         </td>
         </tr>
         
         <tr>
          <td width=40%>
         	<Font class=numberFonts > 1.</font><Font class=reportText ><I><%=MC.M_Selc_YourRptType%><%--Select your report type*****--%> </I></font>
         </td>
<%--
          <td width=40% >
         	<Font class=numberFonts >2.</font> <Font class=reportText ><I>Select your report filters</I></font>
         </td>

          <td width=20% >
         	<Font class=numberFonts >3.</font> <Font class=reportText ><I>View your report</I></font>
         </td>
--%>		 
          <td width=20% >
         	<Font class=numberFonts >2.</font> <Font class=reportText ><I><%=LC.L_View_YourRpt%><%--View your report*****--%></I></font>
         </td>
         </tr>
         </Table>
         <HR class=blue></hr>
         <input type=hidden name=year>
         <input type=hidden name=month>
         <input type=hidden name=year1>
         <input type=hidden name=dateFrom>
         <input type=hidden name=dateTo>
                 
         
         <Table border=0 width=100%>
         <tr>
         	 <td colspan=3>
         	<p class = "reportHeadings"><%=LC.L_General_AccRpts%><%--General Account Reports*****--%></p>
         	</td>	
       	</tr>
         <%
         	CtrlDao ctrl = new CtrlDao();
         	ctrl.getControlValues("adminspecific");
         	String catId = (String) ctrl.getCValue().get(0);
   

         	repdao.getAllRep(EJBUtil.stringToNum(catId),0); //Account Reports
         	ArrayList repIds= repdao.getPkReport(); 
         	ArrayList names= repdao.getRepName();
           	ArrayList desc= repdao.getRepDesc();
         	
         	int repId=0; 
         	String name="";
         
         	int len= repIds.size() ; 
         	
%>
         	<tr>
         	<td width=40%>
         
         	<% for(counter = 0;counter<len;counter++)
         	{
         		repId = ((Integer)repIds.get(counter)).intValue();
         		name=((names.get(counter)) == null)?"-":(names.get(counter)).toString();
         	%>
         		<Input Type="radio" name="reportName" value="<%=repId%>%<%=name%>" onClick="fSetId('report', document.reports)"> <%=name%>
         		<br>
         	<%}%>
         </td>
<%--		 
         	<td width=50% >
         	<Input type="radio" name="filterType" value="1" onClick="fOpenWindow('A','1', document.reports)"> All  
         	<Input type="radio" name="filterType" value="2" onClick="fOpenWindow('Y','1', document.reports)"> Year
         	<Input type="radio" name="filterType" value="3" onClick="fOpenWindow('M','1', document.reports)"> Month
         	<Input type="radio" name="filterType" value="4" onClick="fOpenWindow('D','1', document.reports)"> Date Range
         	<BR><Font class=comments>Selected Date Filter:</Font>
         	<input type=text name=range size=30 READONLY>
         	</td>
--%>			
	 
         <td width=10%>
         </td>
         </table>
         
         
         
         <table width="100%" cellspacing="0" cellpadding="0" border=0 >
         <tr> 
         	<td colspan=2 align=right>
         		<!--<Input type="submit" name="submit" value="Display" >-->
         	  <A type="submit" onClick = "return fValidate(document.reports)" href="#"><%=LC.L_Display%></A>
         	</td>
         </tr>
         </table>
         
         <Input type = hidden  name=id >

         <Input type = hidden  name="val" >
         <Input type=hidden name="repId">
         <Input type=hidden name="repName">


<%
 }//end of if body for session

else

{
%>
  <jsp:include page="timeout_admin.html" flush="true"/>
  <%
}

%>        




<div>

<jsp:include page="bottompanel.jsp" flush="true"/> 



</div>

</form>
</div>




  <jsp:include page="velosmenus.htm" flush="true"/>   




</body>

</html>
