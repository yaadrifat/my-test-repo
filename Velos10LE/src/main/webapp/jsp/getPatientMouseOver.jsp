<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<%@page import="com.velos.eres.business.common.*,com.velos.eres.service.util.*"%>
<%@page import="com.velos.esch.business.common.*"%>
<%@page import="com.velos.remoteservice.lab.*,java.net.URLEncoder,com.velos.eres.business.section.*"%>
<%@page import="com.velos.eres.web.user.UserJB,com.velos.eres.web.studyRights.StudyRightsJB" %>
<%@page import="java.util.*,java.io.*"%>
<%@page import="org.json.*,org.w3c.dom.*"%>

  <%
   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))
   {

    int patientId = StringUtil.stringToNum(request.getParameter("patientId"));
    if (patientId == 0) { patientId = -1; }
    
	int userId = EJBUtil.stringToNum((String)tSession.getValue("userId"));
	int accountId = EJBUtil.stringToNum((String)tSession.getValue("accountId"));

    int studyId = 0;
   	studyId = StringUtil.stringToNum((String)request.getParameter("studyId"));
   	
   	if ((patientId > 0)){
		String mouseover = "";
		PatientDao patDao = new PatientDao();
		mouseover = patDao.getPatientOnStudyMouseOver(userId, studyId, patientId);
 		out.print("overlib('"+ mouseover + "',CAPTION,'"+LC.L_Patient_Details+"');");
	}
} //end of if session times out
else
{
%>
 <jsp:include page="timeout.html" flush="true"/>
  <%
} //end of else body for page right
%>
