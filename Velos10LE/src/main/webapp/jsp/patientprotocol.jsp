<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_PatPcol_Dets%><%--<%=LC.Pat_Patient%> Protocol Details*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<jsp:include page="popupJS.js" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
</head>

<SCRIPT>

function fshow(obj, val){

	if(val == 0)

	{

		//obj.selDay.disabled = true;
		obj.dflag.value  = "0";
		obj.selDay.value = null;
		obj.dispSelDay.value = ""; //SV, 8/6/04, reset whenever item 1 is selected.

	}
	else
	{
	 //obj.selDay.disabled = false;
	 obj.dflag.value = "1";

	}
}

function f_reset_selday(obj){

	//when calendar is changed, reset the selected day fields.
	obj.dayflag[0].checked  = true;	// select radio button 0.
	obj.dayflag[1].checked  = false;
	obj.dflag.value  = "0";
	obj.dispSelDay.value = "";
	obj.selDay.value = null;
}


</SCRIPT>

<%@ page language = "java" import = "com.velos.eres.service.util.EJBUtil,com.velos.eres.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.common.*,java.util.*,java.io.*"%>
<%@ page import="com.velos.eres.service.util.*"%>

<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
<jsp:useBean id="protVisitB" scope="request" class="com.velos.esch.web.protvisit.ProtVisitJB"/>
<jsp:useBean id="studyProt" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:include page="include.jsp" flush="true"/>
<SCRIPT Language="javascript">
  function fselDay(formobj) {

   if (document.getElementById) {
   	   prot = 	prot = formobj.protocolId.value ; }
   	   else {
  	prot =  formobj.protocolId[formobj.protocolId.selectedIndex].value ; }


	dayflag = formobj.dflag.value ;

	if (dayflag == '0') {

		  //KM-#3268
		  alert("<%=MC.M_SelOpt_ToSelVisit%>");/*alert("Please select appropriate option to select a Visit");*****/

		  return false;

	}

      	if(prot == null || prot =='' ) {

		alert("<%=MC.M_Selc_Pcol%>");/*alert("Please Select a Protocol")*****/

		return false;

	}



      	window.open("repRetrieve.jsp?showSelect=1&protId=" +prot+"&repId=107&repName=Mock Schedule:Select a Visit", "Information", "toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=700,height=600");

	return true;

  }

 function  validate(formobj){

	  if (!(validate_col('E-Signature',formobj.eSign))) return false

	  if(formobj.protocolId.value !='' && formobj.protocolId.value !=null){

	  if (!(validate_col('<%=LC.L_Pat_StartDt%>',formobj.protStDate))) return false/* if (!(validate_col('<%=LC.Pat_Patient%> Start Date',formobj.protStDate))) return false*****/

	  }

	if(isNaN(formobj.eSign.value) == true) {

		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect E-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;

	   }

	  //change form action depending on the mode and protocol parameters

	   if (!(formobj.protocolId.value=="")) {
		if (formobj.dayflag[1].checked) {
			 //SM:BugNo:3267, 5168
			//if (formobj.selDay.value != '') {
			if (formobj.dispSelDay.value == "") {
				alert("<%=MC.M_SelValidVisit_ForSch%>");/*alert("Please select a valid 'Visit' for the Schedule");*****/
				//formobj.selDay.focus();
				return false;
			}
		}
	  }

		if(formobj.prevProtocolId.value != "null")
		{
			if (document.getElementById) {
		    	//Removed all the if conditions for the Bug 20074.
			    formobj.action = "discdetails.jsp";
        	}

		}
	     return true;
	}


</SCRIPT>

<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1)
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {
%>
<body style="overflow:scroll;">
<%
	} else {
%>
<body>
<%
	}
%>

<br>

<DIV class="popDefault" id="div1">
<%

   HttpSession tSession = request.getSession(true);

   if (sessionmaint.isValidSession(tSession))
	{


		String userIdFromSession = (String) tSession.getValue("userId");
		String acc = (String) tSession.getValue("accountId");
		String studyId = request.getParameter("studyId");
		String patientId="";
		patientId = request.getParameter("pkey");



%>


<%
	int pageRight = 0;

	 //******************GET STUDY TEAM RIGHTS and put the same in session, this will be a separate obj as no study is selected //***************************************************************
   	TeamDao teamDao = new TeamDao();
   	teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));
   	ArrayList tId = teamDao.getTeamIds();
   	if (tId.size() == 0) {
   		pageRight=0 ;
   	}else {
   		stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));

   			ArrayList teamRights ;
						teamRights  = new ArrayList();
						teamRights = teamDao.getTeamRights();

						stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
						stdRights.loadStudyRights();

   		tSession.setAttribute("studyRights",stdRights);
   		if ((stdRights.getFtrRights().size()) == 0){
   		 	pageRight= 0;

   		}else{
   			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYMPAT"));
    		}
   	}

	//check whether the user has right to edit the patients data of the patient org
	person.setPersonPKId(EJBUtil.stringToNum(patientId));
	person.getPersonDetails();

	String organization = person.getPersonLocation();
	int orgRight = 0;

	orgRight = userSiteB.getMaxRightForStudyPatient(EJBUtil.stringToNum(userIdFromSession),EJBUtil.stringToNum(patientId),EJBUtil.stringToNum(studyId));

	if (orgRight > 0)
	{
		System.out.println("patient protocol" + orgRight);
		orgRight = 7;
	}
	
	//JM: 22Feb2011: #5864
	SchCodeDao scho = new SchCodeDao();
	
	String calStatDesc_A = "";
	
	calStatDesc_A = scho.getCodeDescription(scho.getCodeId("calStatStd","A"));


	if ( (pageRight >= 4) && (orgRight >= 4) )

	{


		String protStDate ="";
		int patProtId = 0;
		int ddProtCount = 0;
		String selDay = null;
		String dflag = "0";
		String check1 = "CHECKED";
		String check2 = "";
		String disableSel = "READONLY";
		String selectedDay="";
		String treatmentLoc= "";
		int num;
		String protocolId  = "";

		Integer i = null;

		patEnrollB.findCurrentPatProtDetails(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(patientId));

		patProtId   = patEnrollB.getPatProtId();
		protStDate = patEnrollB.getPatProtStartDt();
		protocolId =patEnrollB.getPatProtProtocolId();
		selDay = patEnrollB.getPatProtStartDay();

		if (selDay ==  null)
		{
			check1 = "CHECKED";
			check2 = "";
			dflag = "0";

		}else
		{
			check2 = "CHECKED";
			check1 = "";
			dflag = "1";
		}

		if (selDay!= null && (!selDay.trim().equals("")))
		{

			num = EJBUtil.stringToNum(selDay);
			//selectedDay	=  "Week " + (num/7 +1) + ", Day " + (num%7);
			//KM-#3268
			if (null == selDay){
				selectedDay = "";
			}else {
				selectedDay = VelosResourceBundle.getMessageString("M_CurrSch_GeneratedDay",selDay);
			}
		}
		else
		{
			selectedDay	= "";
		}

	EventAssocDao eventAssoc = studyProt.getStudyProts(EJBUtil.stringToNum(studyId),"A");
	ArrayList protocolIds = new ArrayList();
	ArrayList names = new ArrayList();

	protocolIds = eventAssoc.getEvent_ids();
	names = eventAssoc.getNames();

	 ddProtCount = protocolIds.size();

	StringBuffer protList = new StringBuffer();
	// SV, 8/6/04, fix for #1603, refresh the selected day field each time calendar is changed.
	protList.append("<SELECT name=protocolId  onchange=f_reset_selday(document.enroll)>");
	protList.append("<option value=''>"+LC.L_Select_AnOption/*Select an Option*****/+" </Option>");
	String protId="";
	for(int counter=0;counter<ddProtCount ;counter++){
	   			protId = (protocolIds.get(counter)).toString();
			   if (!(protocolId == null) && protocolId.equals(protId)){
					protList.append("<option value="+(protocolIds.get(counter)).toString() +" 			 SELECTED>" +(names.get(counter)).toString() +"</option>");
			   }else{
			protList.append("<option value="+(protocolIds.get(counter)).toString() +" 			 >" +(names.get(counter)).toString() +"</option>");
		   }
	}
	protList.append("</select>");
%>

<Form name="enroll" id="patenrolldet" method="post" action="updatepatenrolldetails.jsp" onsubmit="if (validate(document.enroll)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
<input type="hidden" name="pataccount" Value="<%=acc%>">
<Input type="hidden" name="patientId" value=<%=patientId%>>
<Input type="hidden" name="studyId" value=<%=studyId%>>
<Input type="hidden" name="patProtId" value=<%=patProtId%>>
<Input type="hidden" name="prevProtocolId" value=<%=protocolId%>>
<Input type="hidden" name="prevprotStDate" value=<%=protStDate%>>
<Input type="hidden" name="studyId" value=<%=studyId%>>
<Input type="hidden" name="dflag" value=<%=dflag%>>
<Input type="hidden" name="prevSelDay" value=<%=selDay%>>
<Input type="hidden" name="calledFrom" value="enroll">
<Input type="hidden" name="mode" value="M">
<%

  if ( protocolId == null) //if last protocol was disc by selecting 'select a protocol'
   {
%>
	   <Input type="hidden" name="retalerts" value='on'>

  <%

  }


%>

<Input type="hidden" name="personPK" value=<%=patientId%>>
<table>
  	<tr>

	<td COLSPAN=3><br>

	<p class = "sectionHeadings" ><%=LC.L_Treatment_Dets%><%--Treatment Details*****--%></p><br>

	</td>

	</tr>

<%

   if(ddProtCount > 0) {

%>

	<tr>

	<td  COLSPAN=3><P class = "defComments"> <%=MC.M_FilledOrder_SchPats%><%--The following fields must be filled in order to generate a schedule for the <%=LC.Pat_Patient_Lower%> and track events.*****--%>

          </P><BR>

      </td>

</tr>
<tr>
	    <td width="200">
	       	<%=LC.L_Protocol_Calendar%><%--Protocol Calendar*****--%>
	    </td>
    	    <td>
			<%=protList%>
	    </td>
	</tr>
	<tr>
	<td></td>
	<td >
		<P class = "defComments"> <%=MC.M_SelPcolCal_PatAssignedStd%><%--Select the specific protocol calendar that the <%=LC.Pat_Patient_Lower%> is assigned to for this <%=LC.Std_Study_Lower%>*****--%>
	      </P>
      </td>
    </tr>
  <tr>
    <td width="200">
       <%=LC.L_Pat_StartDt%><%--<%=LC.Pat_Patient%> Start Date*****--%>
    </td>
<%-- INF-20084 Datepicker-- AGodara --%>
    <td>
    	<input type=text name="protStDate" class="datefield" size = 10 MAXLENGTH = 10 	value='<%=protStDate%>' READONLY>
    </td>
	</tr>
	<tr>
	<td></td>
	<td >
		<P class = "defComments"> <%=MC.M_PatSch_GeneratedStDt%><%--<%=LC.Pat_Patient%>'s schedule will be generated based on this start date.*****--%>
    </P>
</td>
</tr>

 <tr>

    <td colspan = 2>
    <br>
    <Input type="radio" name="dayflag" value="0" onClick="fshow(document.enroll,0)" <%=check1%>> <%=MC.M_CalcuSch_OfFirstVisit%><%--Calculate Schedule from the First Visit of the Calendar Template*****--%>
    </td>
    <tr>
    <td colspan = 2>
    <Input type="radio" name="dayflag" value="1" onClick="fshow(document.enroll,1)"  <%=check2%>> <%=MC.M_CalcuSch_FstVstCal%><%--Calculate Schedule from a Visit other than the First Visit of the Calendar Template*****--%>
	&nbsp;&nbsp;  <A HREF="#" onClick="return fselDay(document.enroll)"><%=LC.L_Select_AVisit%><%--Select a Visit*****--%></A>
    </td>
    </tr>
    <tr>
    <td><%=LC.L_Selected_Visit%><%--Selected Visit*****--%></td>
    <td><input type=hidden name="selDay" size = 10 MAXLENGTH = 10  value = <%=selDay%>>
	<input type=text name="dispSelDay" size = 55 MAXLENGTH = 100  <%=disableSel%>  value = '<%=selectedDay%>'>
	<FONT class="Mandatory"> [<%=LC.L_Read_Only%><%--Read Only*****--%>] </FONT>
	</td>
    </tr>
   	</table>

<br>


<%String showSubmit="";
if ( ( pageRight > 4 ) && (orgRight > 4 ) )
{ showSubmit ="Y";}
else
   {showSubmit="N";}
%>

<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="patenrolldet"/>
		<jsp:param name="showDiscard" value="N"/>
		<jsp:param name="showSubmit" value="<%=showSubmit%>"/>
</jsp:include>

<%

   } else { //else for if ddProtCount>0

%>
	<table width="90%">
	<input type=hidden name=protocolId>
	<tr>
	<td COLSPAN=3><%Object[] arguments = {calStatDesc_A,calStatDesc_A}; %>
	<P class = "defComments"> <%=VelosResourceBundle.getMessageString("M_NoPcolCal_ChgStatPat",arguments)%><%--There is no There is no <%=calStatDesc_A%> protocol calendar associated to this study. Please associate a protocol calendar and change it's status to <%=calStatDesc_A%> before assigning this patient to a protocol calendar.*****--%>
    </P>
	</td>
	</tr>
	</table>



<%

   } //end for if ddProtCount>0

%>


</Form>
<%
	} //end of if body for page right

else
  {

	%>
	<jsp:include page="accessdenied.jsp" flush="true"/>

	<%
	}
 }//end of if body for session
else
{
%>
<jsp:include page="timeout.html" flush="true"/>
<%
}

%>

</div>

<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>

</html>



