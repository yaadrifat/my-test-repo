<!--JM: 22August2006, created the new JSP page for saving study level alert notifications July-Aug2006, Enhancement #S6-->
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>  
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/>


<%@ page language = "java" import = "com.velos.eres.business.common.*, com.velos.eres.service.util.EJBUtil"%>
<%


	int ret=2;
	String src =request.getParameter("srcmenu");   
	String eSign = request.getParameter("eSign");
	String studyId=request.getParameter("studyId");
	String from = request.getParameter("from");
	String selectedTab = request.getParameter("selectedTab");

	

	String saeNotifyUserPK = "";
	String saeNotifyMailPK = "" ;
	String saeDeathNotifyUserPK = "";
	String saeDeathNotifyEmailPK = "";

	String userIds = request.getParameter("userIds");
	userIds=(userIds==null)?"":userIds;
	
	//Commented by Manimaran to fix the Bug2715
	/*String userMobs = request.getParameter("userMobs");
	userMobs=(userMobs==null)?"":userMobs;
	*/
	String userIds1 = request.getParameter("userIds1");
	userIds1=(userIds1==null)?"":userIds1;
	
	/*String userMobs1 = request.getParameter("userMobs1");
	userMobs1=(userMobs1==null)?"":userMobs1;
        */ 
	saeNotifyUserPK=request.getParameter("saeNotifyUserPK");
	saeNotifyUserPK=(saeNotifyUserPK==null)?"0":saeNotifyUserPK;

	saeNotifyMailPK=request.getParameter("saeNotifyMailPK");
	saeNotifyMailPK=(saeNotifyMailPK==null)?"0":saeNotifyMailPK;

	saeDeathNotifyUserPK=request.getParameter("saeDeathNotifyUserPK");
	saeDeathNotifyUserPK=(saeDeathNotifyUserPK==null)?"0":saeDeathNotifyUserPK;

	saeDeathNotifyEmailPK=request.getParameter("saeDeathNotifyEmailPK");
	saeDeathNotifyEmailPK=(saeDeathNotifyEmailPK==null)?"0":saeDeathNotifyEmailPK;		
		




HttpSession tSession = request.getSession(true); 


 if (sessionmaint.isValidSession(tSession))
   {	
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
   String oldESign = (String) tSession.getValue("eSign");

	if(!oldESign.equals(eSign)) {
	%>
	<jsp:include page="incorrectesign.jsp" flush="true"/>	
	<%
	} else {
		 	 	

			String usr = (String) tSession.getValue("userId");
			String ipAdd = (String) tSession.getValue("ipAdd");	
		
				SettingsDao settingsDao = commonB.getSettingsInstance();	
				

				settingsDao.setSettingKeyword("SAE_NOTIFY_USER");
				settingsDao.setSettingValue(userIds);
				
				settingsDao.setSettingsModNum(studyId);
				settingsDao.setSettingsModName("3");
				
				settingsDao.setSettingIpadd(ipAdd);
				
				if (saeNotifyUserPK.length()>0)
				{
					settingsDao.setSettingPK(saeNotifyUserPK);
					settingsDao.setSettingModifiedBy(usr);
				}
				 else
				 { 
					 settingsDao.setSettingPK("0");
					 settingsDao.setSettingCreator(usr);
				}
				
	 
				//Commented by Manimaran to fix the Bug2715
				/*settingsDao.setSettingKeyword("SAE_NOTIFY_EMAIL");			
				 settingsDao.setSettingValue(userMobs);
				 settingsDao.setSettingsModNum(studyId);
				 settingsDao.setSettingsModName("3");
				 if (saeNotifyMailPK.length()>0)
					 settingsDao.setSettingPK(saeNotifyMailPK);
				 else 
					 settingsDao.setSettingPK("0");
                               */
				 settingsDao.setSettingKeyword("SAE_DEATH_NOTIFY_USER");
				 settingsDao.setSettingValue(userIds1);
				 settingsDao.setSettingsModNum(studyId);
				 settingsDao.setSettingsModName("3");
				 settingsDao.setSettingIpadd(ipAdd);
				 
				 if (saeDeathNotifyUserPK.length()>0)
				 {
					 settingsDao.setSettingPK(saeDeathNotifyUserPK);
					 settingsDao.setSettingModifiedBy(usr);
					} 
				 else
				 { 
					 settingsDao.setSettingPK("0");
					 settingsDao.setSettingCreator(usr);
				  }	 

				 //Commented by Manimaran to fix the Bug2715
				 /*settingsDao.setSettingKeyword("SAE_DEATH_NOTIFY_EMAIL");
				 settingsDao.setSettingValue(userMobs1);
				 settingsDao.setSettingsModNum(studyId);
				 settingsDao.setSettingsModName("3");
				 if (saeDeathNotifyEmailPK.length()>0)
					 settingsDao.setSettingPK(saeDeathNotifyEmailPK);
				 else 
					 settingsDao.setSettingPK("0");	
				*/

				ret = settingsDao.saveSettings();
				if (ret >= 0)
					{
				%>		
						<br><br><br><br><br>
						<p class = "successfulmsg" align = center><%=MC.M_Data_SvdSucc%><%--Data was saved successfully.*****--%></p>
						
						<META HTTP-EQUIV=Refresh CONTENT="1; URL=studynotification.jsp?srcmenu=tdmenubaritem3&selectedTab=9&studyId=<%=studyId%>">
						 

				<% 
					} else
					{
				%>		
						<br><br><br><br><br>
						<p class = "successfulmsg" align = center><%=MC.M_DataCnt_SvdSucc%><%--Data could not be saved successfully.*****--%></p>
						<script>
						
												 
							window.history.back();
							
							
						</script>

				<% 	
					} // end of if data saved				
} //end of if for eSign 

} //end of session 
else {   

%>
  <jsp:include page="timeout.html" flush="true"/>
<%
}
%> 
</BODY>
</HTML>
