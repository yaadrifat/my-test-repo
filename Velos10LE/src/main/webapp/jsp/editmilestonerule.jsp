<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<title><%=LC.L_Edit_Mstone%><%-- Edit Milestone*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<script>
function validate(formobj) {

 	if (!(validate_col('Milestone Status',formobj.milestoneStat))) return false;
	milestoneType = formobj.milestoneType.value

	//if(isNaN(formobj.milestoneAmount.value) == true) {
	//alert("Please enter a numeric value for Amount.");
	//formobj.milestoneAmount.focus();
	//return false;
	//}

	milestoneStat = formobj.milestoneStat.value;//KM-#DFIN-7

	//formobj.milestoneAmount.value = formobj.amountNum.value + "." + formobj.amountFrac.value;
	formobj.milestoneAmount.value = formobj.amountNum.value;

	if (milestoneType != 'SM' && milestoneType != 'AM' && milestoneType != 'EM' && milestoneType != 'VM')
	{
		if ( !isInteger(formobj.count.value) )
		{
			/*if (!isNegNum(formobj.count.value))  // commented by Hari Shankar Maurya  
			{
				formobj.count.focus();
				alert("<%=MC.M_Etr_ValidNum%>");alert("Please enter a valid Number");
			 	return false;
			}
			else
			{*/
				if (formobj.count.value == '-0')
				{
					formobj.count.focus();
					alert("<%=MC.M_InvalidNegVal_EtrVldCnt%>");/*alert("Invalid negative value. Please enter a valid 'Count'");*****/
				 	return false;
				}

				if (parseInt(formobj.count.value) < -1)
				{
					formobj.count.focus();
					alert("<%=MC.M_CntLess_EtrValidCount%>");/*alert("The count cannot be less than -1. Please enter a valid 'Count'");*****/
				 	return false;
				}
			//}
		}
	}



	//JM: 22May2007

	//JM: 18Dec2009: #4569
	var selRule;
	var milestoneRuleSubType;

	if (milestoneType=='VM' || milestoneType=='EM'){

	selRule  =  formobj.milestoneRuleId.selectedIndex;
	milestoneRuleSubType = formobj.milestoneRuleSubType[selRule].value;

	//KM:D-FIN7	
	 if ((formobj.milestoneStat.value!= formobj.actPkVal.value) || (formobj.milestoneStat.value!=formobj.inactPkVal.value)) {	//Active, inactive MS
	  if ( (milestoneRuleSubType == 'vm_4' || milestoneRuleSubType == 'em_4')  && formobj.dEventStatus.value!=''  ){
		  alert("<%=MC.M_CntSelEvt_MstoneRule%>");/*alert("You cannot select an Event Status with the selected Milestone Rule");*****/
			formobj.dEventStatus.focus();
			return false;
	  }

	 }


//JM: 04Oct2007: followoing block should be valid for milestoneType 'VM' or 'EM' only


		if ((!validate_col_no_alert('Event Status',formobj.dEventStatus )) ){

	    	if ( milestoneRuleSubType != 'vm_4' && milestoneRuleSubType != 'em_4'){
	    		alert("<%=MC.M_Selc_EvtStatus%>");/*alert("Please select Event Status");*****/
				formobj.dEventStatus.focus();
	    	  	return false;
	   		}
		}



	}

//	var index = formobj.amountNum.value.indexOf(".");
  //	  if (index != -1) {
  	//	alert("<%=LC.L_Invalid_Amount%>");/*alert("Invalid Amount.");*****/
	//		formobj.amountNum.focus();
	//		return false;
  //  	}
	//var index1 = formobj.amountFrac.value.indexOf(".");
  	 // if (index1 != -1) {
  	//	alert("<%=LC.L_Invalid_Amount%>");/*alert("Invalid Amount.");*****/
	//		formobj.amountFrac.focus();
	//		return false;
    //	}

		if (! isInteger(formobj.amountNum.value ) && !isNegNum(formobj.amountNum.value))
		{
				formobj.amountNum.focus();
				alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
			 	return false;
		}

		if (/^\d{0,11}?$/.test(formobj.amountNum.value) == false && !isEmpty(formobj.amountNum.value)) {
			alert("<%=MC.M_ValEtrFwg_AmtFmt11Dgt%>");/*alert("The value entered does not pass the following criteria:\n \n -'Amount' should be a number.\n -'Amount' should conform to format - ###########.## \n    i.e. max 11 digits followed by max 2 decimal places.\n \nPlease enter a valid value.");*****/
			formobj.amountNum.focus();
			return false;
		}
		if (/^\d{0,2}?$/.test(formobj.amountFrac.value) == false && !isEmpty(formobj.amountFrac.value)) {
			alert("<%=MC.M_ValEtrFwg_AmtFmt11Dgt%>");/*alert("The value entered does not pass the following criteria:\n \n -'Amount' should be a number.\n -'Amount' should conform to format - ###########.## \n    i.e. max 11 digits followed by max 2 decimal places.\n \nPlease enter a valid value.");*****/
			formobj.amountFrac.focus();
			return false;
   		}

		//if (! isInteger(formobj.amountFrac.value))
	//	{
		//		formobj.amountFrac.focus();
		//		alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
		//	 	return false;
		//}

    	if (milestoneType != 'AM') {
		   if (! isInteger(formobj.mLimit.value))
		   {
				formobj.mLimit.focus();
				alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
			 	return false;
		   }
		}
		else
		{
			if (!(validate_col('Description',formobj.description))) return false;
		}


if (!(validate_col('e-Signature',formobj.eSign))) return false;



    if(isNaN(formobj.eSign.value) == true) {
    	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
   }
return true;
}

function calculate(formobj){

if(formobj.milestoneAmount.value==""){
	formobj.milestoneAmount.value = "";
}else{
//formobj.milestoneAmount.value = Math.round(formobj.milestoneAmount.value *100)/100;
}
}
</script>


<BODY>
<DIV class="popDefault">
<P class="sectionHeadings"><%=MC.M_Mstone_EditMstone%><%-- Milestones >> Edit Milestones*****--%> </P>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.esch.business.common.*"%>
<jsp:useBean id="milestoneB" scope="request" class="com.velos.eres.web.milestone.MilestoneJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />

<jsp:useBean id="protVisit" scope="page" class="com.velos.esch.web.protvisit.ProtVisitJB" />

<%
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession)) {
	 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
		 <jsp:include page="include.jsp" flush="true"/>
<%
		String milestoneId = request.getParameter("milestoneId");
		milestoneB.setId(EJBUtil.stringToNum(milestoneId));
		milestoneB.getMilestoneDetails();
		String milestoneStudyId = milestoneB.getMilestoneStudyId();
		String milestoneType = milestoneB.getMilestoneType();
		String milestoneCalId = milestoneB.getMilestoneCalId();
		String milestoneRuleId = milestoneB.getMilestoneRuleId();
		String milestoneAmount = milestoneB.getMilestoneAmount();
		String milestoneVisit = milestoneB.getMilestoneVisit();
		String milestoneEvent = milestoneB.getMilestoneEvent();
		String milestoneCount = milestoneB.getMilestoneCount();
		String milestoneDelFlag = milestoneB.getMilestoneDelFlag();
		String milestoneUsersTo = milestoneB.getMilestoneUsersTo();
		String milestoneDescription = milestoneB.getMilestoneDescription();//KM
		String milestoneAchievedCount = milestoneB.getMilestoneAchievedCount();
		String milestoneStatFK = milestoneB.getMilestoneStatFK();//KM

		if (StringUtil.isEmpty(milestoneAchievedCount))
		{
			milestoneAchievedCount="0";
		}

		String limit = "";
		String paymentFor = "";
		String paymentType = "";
		String patStatus = "";
		String dStatus = "";
		String dpayType = "";
		String dpayFor = "";
		String acc = (String) tSession.getValue("accountId");
		int accId = 0;
		String eventStatus = "";

		String fldState = "";

		accId = EJBUtil.stringToNum(acc);

		CodeDao codeLstStatus = new CodeDao();
		CodeDao codeLstPayType = new CodeDao();
		CodeDao codeLstPayFor = new CodeDao();
		SchCodeDao cdEvent = new SchCodeDao();

		//KM-#D-FIN7
		CodeDao cdStat = new CodeDao();
		int wipPk = cdStat.getCodeId("milestone_stat","WIP");
		String wipPkStr = wipPk +"";

		int actPk = cdStat.getCodeId("milestone_stat","A");
		String actPkStr = actPk + "";

		int inactPk = cdStat.getCodeId("milestone_stat","IA");
		String inactPkStr = inactPk + "";

		
		String dEventStatus = "";

		limit = milestoneB.getMilestoneLimit() ;

		paymentFor =  milestoneB.getMilestonePayFor() ;

	    paymentType = milestoneB.getMilestonePayType() ;

	    patStatus = milestoneB.getMilestoneStatus();
	    eventStatus = milestoneB.getMilestoneEventStatus();


	     if (StringUtil.isEmpty(milestoneCount))
		milestoneCount = "";

	    if (StringUtil.isEmpty(milestoneStatFK))
		    milestoneStatFK = actPkStr;
		
		//KM-02Jun10-#4964
		if (milestoneStatFK.equals(actPkStr) || milestoneStatFK.equals(inactPkStr))
		{
			fldState = " DISABLED ";
		}

		if (StringUtil.isEmpty(limit))
		limit = "";

		if (StringUtil.isEmpty(paymentFor))
		paymentFor = "";

		if (StringUtil.isEmpty(patStatus))
		patStatus = "";

		if (StringUtil.isEmpty(eventStatus))
		eventStatus = "";

	    if (milestoneType.equals("SM"))
	    {
			codeLstStatus.getCodeValues("studystat");
		}
		else
		{
			codeLstStatus.getCodeValues("patStatus",accId);
		}


		dStatus = codeLstStatus.toPullDown("dStatus",EJBUtil.stringToNum(patStatus));


		 codeLstPayType.getCodeValues("milepaytype");
		 dpayType = codeLstPayType.toPullDown("dpayType" ,EJBUtil.stringToNum(paymentType));

		//KM-02Jun10-#4964
		if (milestoneStatFK.equals(actPkStr) || milestoneStatFK.equals(inactPkStr))
		{
			// replace the milestoneDD , to add disabled status
			dpayType = StringUtil.replace(dpayType,"<SELECT NAME=" , "<SELECT DISABLED NAME=");
			dStatus = StringUtil.replace(dStatus,"<SELECT NAME=" , "<SELECT DISABLED NAME=");

		}


		codeLstPayFor.getCodeValues("milePayfor");
		 dpayFor = codeLstPayFor.toPullDown("dpayFor" ,EJBUtil.stringToNum(paymentFor));




		String amountNumeric = "";
		String amountFractional = "";

		int decPos = 0;

		if(milestoneAmount == null || milestoneAmount.trim().equals("0.00")){
			milestoneAmount="";
		}
		//if(milestoneAmount.trim().equals("0.00")){
		//	milestoneAmount = "No Associated Amount";
		//}
	/*decPos = milestoneAmount.indexOf(".");
   	if(decPos == -1) decPos = milestoneAmount.length();
  	amountNumeric = milestoneAmount.substring(0,decPos);
   	if(decPos != milestoneAmount.length()) decPos++;
   	amountFractional = milestoneAmount.substring(decPos);*/


	String studyCurrency = "";
	ArrayList curr = new ArrayList();
	String currency = "";

	studyB.setId(EJBUtil.stringToNum(milestoneStudyId));
   	studyB.getStudyDetails();
   	studyCurrency = studyB.getStudyCurrency();
	SchCodeDao cdDesc = new SchCodeDao();
	cdDesc.getCodeValuesById(EJBUtil.stringToNum(studyCurrency));
	curr =  cdDesc.getCSubType();
	currency=(((curr.get(0)) ==null)?"-":(curr.get(0)).toString());




		CodeDao codeLst = new CodeDao();
		String milestoneDD = "";

		String calendarName = "";
		String eventName = "";

		String visitName = ""; //SV, 11/01

		MilestoneDao milestoneDao = new MilestoneDao();



%>

<Form class=formDefault id="editmilestonerule" name="milestone" action="updatemilestonerule.jsp" method="post" onSubmit="ripLocaleFromAll(); if (validate(document.milestone)== false) { setValidateFlag('false'); applyLocaleToAll(); return false; } else { setValidateFlag('true'); return true;}" >
<input type=hidden name='milestoneId' value=<%=milestoneId%>>
<input type="hidden" name="actPkVal" value =<%=actPk%>>
<input type="hidden" name="inactPkVal" value =<%=inactPk%>>

<table width="100%" border="0">
<%
//Modified by Manimaran for the Enh.#FIN10.

if(milestoneType.equals("AM")){%>
<tr>
	<td width=30%><%=LC.L_Description%><%-- Description*****--%>: <FONT class="Mandatory">* </FONT> </td>
	<td width=70%><input <%=fldState%> type=text name=description maxlength="4000" size=45  class="leftAlign" value='<%=milestoneDescription%>'></td>
</tr>
<% } %>

<%if(!milestoneType.equals("SM") && !milestoneType.equals("AM") && !milestoneType.equals("EM")&& !milestoneType.equals("VM")) {%>
<tr>
	<td width=30%><%=LC.L_Pat_Count%><%--<%=LC.Pat_Patient%> Count*****--%>: </td>
	<td width=70%><input <%=fldState%> type=text name=count size=5 maxlength=10 class="leftAlign numberfield" data-unitsymbol="" data-formatas="number" value='<%=milestoneCount%>'></td>
</tr>
<% } %>
<tr colspan=2>


	<td width=30%> <%Object[] arguments1 = {currency}; %><%=VelosResourceBundle.getLabelString("L_Amount_In",arguments1)%><%--Amount ( in <%=currency%>)*****--%>: </td>
<td>
	<table><tr>
	 <td align=left >
	 <INPUT    NAME="milestoneAmount" TYPE="hidden" value="<%=milestoneAmount%>" SIZE=20>
	<input <%=fldState%> type=text name=amountNum size=16 maxlength=16  class="leftAlign numberfield" data-unitsymbol="" data-formatas="currency" value=<%=milestoneAmount%>>
 </td><!-- 
  <td align=left %> <B>.</B> </td>
  <td align=left >
	<input  <%=fldState%> type=text name=amountFrac size=3 maxlength=2  class="leftAlign" value=<%=amountFractional%>>
 </td> -->
	</tr>
	</table>
</td>
</tr>


<%
	if(milestoneType.equals("PM")){
%>

<%
	} else if(milestoneType.equals("VM")){
		codeLst.getCodeValues("VM");
		milestoneDD = codeLst.toPullDown("milestoneRuleId", EJBUtil.stringToNum(milestoneRuleId));
		cdEvent.getCodeValues("eventstatus",0);
		//KM-22Sep10-#5117
		if (eventStatus.equals(""))
		    eventStatus = "-1";
		dEventStatus =   cdEvent.toPullDown("dEventStatus"  ,EJBUtil.stringToNum(eventStatus) );

		//KM-#5395
		if (eventStatus.equals("-1"))
			dEventStatus =  StringUtil.replace(dEventStatus, "</SELECT>", "<OPTION value = \"\" selected>"+LC.L_Select_AnOption/* Select an option*****/+"</OPTION></SELECT> ");
		else
			dEventStatus =  StringUtil.replace(dEventStatus, "</SELECT>", "<OPTION value = \"\">"+LC.L_Select_AnOption/* Select an option*****/+"</OPTION></SELECT> ");

		//KM-02Jun10-#4964
		if (milestoneStatFK.equals(actPkStr) || milestoneStatFK.equals(inactPkStr))
		{
			// replace the milestoneDD , to add disabled status
			milestoneDD = StringUtil.replace(milestoneDD,"<SELECT NAME=" , "<SELECT DISABLED NAME=");
			dEventStatus = StringUtil.replace(dEventStatus,"<SELECT NAME=" , "<SELECT DISABLED NAME=");

		}
 


		eventassocB.setEvent_id(EJBUtil.stringToNum(milestoneCalId));
		eventassocB.getEventAssocDetails();
		calendarName = eventassocB.getName();

		visitName= milestoneDao.getMilestoneVisitName(EJBUtil.stringToNum(milestoneId));

		if (StringUtil.isEmpty(visitName))
		{
			visitName = "All";
		}

%>
<tr>
	<td width=30%><%=LC.L_Protocol_Calendar%><%-- Protocol Calendar*****--%>:</td>
	<td width=70%>  <%=calendarName%></td>
</tr>
<tr>
	<td width=30%><%=LC.L_Visit%><%-- Visit*****--%>:</td>
<%--	<td width=70%><%=milestoneVisit%></td> --%>
	<td width=70%><%=visitName%></td>
</tr>
<tr>
	<td width=30%><%=LC.L_Milestone_Rule%><%-- Milestone Rule*****--%>:</td>
	<td width=70%> <%=milestoneDD%></td>
</tr>
<tr>
	<td width=30%><%=LC.L_Event_Status%><%-- Event Status*****--%>: </td>
	<td width=70%><%=dEventStatus%></td>
</tr>
<%
	} else if(milestoneType.equals("EM")){
		codeLst.getCodeValues("EM");

		String eventVisit = "";

		milestoneDD = codeLst.toPullDown("milestoneRuleId", EJBUtil.stringToNum(milestoneRuleId));

		cdEvent.getCodeValues("eventstatus",0);
		//KM-22Sep10-#5117
		if (eventStatus.equals(""))
		    eventStatus = "-1";
 		dEventStatus =   cdEvent.toPullDown("dEventStatus"  ,EJBUtil.stringToNum(eventStatus) );
		
		//KM-#5395
		if (eventStatus.equals("-1"))
			dEventStatus =  StringUtil.replace(dEventStatus, "</SELECT>", "<OPTION value = \"\" selected>"+LC.L_Select_AnOption/* Select an option*****/+"</OPTION></SELECT> ");
		else
			dEventStatus =  StringUtil.replace(dEventStatus, "</SELECT>", "<OPTION value = \"\">"+LC.L_Select_AnOption/* Select an option*****/+"</OPTION></SELECT> ");

 		//KM-02Jun10-#4964
		if (milestoneStatFK.equals(actPkStr) || milestoneStatFK.equals(inactPkStr))
		{
			// replace the milestoneDD , to add disabled status
			milestoneDD = StringUtil.replace(milestoneDD,"<SELECT NAME=" , "<SELECT DISABLED NAME=");
			dEventStatus = StringUtil.replace(dEventStatus,"<SELECT NAME=" , "<SELECT DISABLED NAME=");

		}



		eventassocB.setEvent_id(EJBUtil.stringToNum(milestoneCalId));
		eventassocB.getEventAssocDetails();
		calendarName = eventassocB.getName();

		eventassocB.setEvent_id(EJBUtil.stringToNum(milestoneEvent));
		eventassocB.getEventAssocDetails();
		eventName = eventassocB.getName();

		eventVisit = eventassocB.getEventVisit();

		protVisit.setVisit_id(EJBUtil.stringToNum(eventVisit));
		protVisit.getProtVisitDetails();
		//@Ankit 08Mar11 #5904
		//visitName = protVisit.getName();
		visitName= milestoneDao.getMilestoneVisitName(EJBUtil.stringToNum(milestoneId));

%>
<tr>
	<td width=30%><%=LC.L_Protocol_Calendar%><%-- Protocol Calendar*****--%>:</td>
	<td width=70%><%=calendarName%></td>
</tr>
<tr>
	<td width=30%><%=LC.L_Visit%><%-- Visit*****--%> :</td>
<%--	<td width=70%><%=milestoneVisit%></td>--%>
	<td width=70%><%=visitName%></td>
</tr>
<tr>
	<td width=30%><%=LC.L_Event_Name%><%-- Event Name*****--%>: </td>
	<td width=70%><%=eventName%></td>
</tr>
<tr>
	<td width=30%><%=LC.L_Milestone_Rule%><%-- Milestone Rule*****--%>: </td>
	<td width=70%><%=milestoneDD%></td>
</tr>
<tr>
	<td width=30%><%=LC.L_Event_Status%><%-- Event Status*****--%>: </td>
	<td width=70%><%=dEventStatus%></td>
</tr>

<%
	}
//JM: 22May2007
if(milestoneType.equals("VM") || milestoneType.equals("EM") ){
		CodeDao   ruleCodeDao= new CodeDao();
		ruleCodeDao.getCodeValues(milestoneType);

		ArrayList milestoneRuleSubTypes = null;
		milestoneRuleSubTypes = ruleCodeDao.getCSubType();

		if (milestoneRuleSubTypes != null )
		{
			for ( int i=0; i < milestoneRuleSubTypes.size(); i++){
			%>
		 			<input type=hidden name=milestoneRuleSubType value='<%=milestoneRuleSubTypes.get(i) %>'>
			<%

			}
		}
}




%>

<tr>
<% if (milestoneType.equals("SM")) {
%>
<td width=30%><%=LC.L_Study_Status%><%--<%=LC.Std_Study%> Status*****--%>:</td>
<%
}
else if (!milestoneType.equals("AM") && !milestoneType.equals("VM") && !milestoneType.equals("EM"))
{ %>
	<td width=30%><%=LC.L_Patient_Status%><%--<%=LC.Pat_Patient%> Status*****--%>:</td>
<% }

if(!milestoneType.equals("AM") ){
if(!milestoneType.equals("VM") && !milestoneType.equals("EM")){
%>
	<td width=70%><%=dStatus%></td>
<%}%>
</tr>
<tr>
	<td width=30%><%=LC.L_Limit%><%-- Limit*****--%>:</td>
	<td width=70%><input  <%=fldState%> type=text name="mLimit" size=3 maxlength=10  class="leftAlign numberfield" data-unitsymbol="" data-formatas="number" value='<%=limit%>'></td>
</tr>
<%}%>
<tr>
	<td width=30%><%=LC.L_Payment_Type%><%-- Payment Type*****--%>:</td>
	<td width=70%><%=dpayType%></td>
</tr>

<tr>
	<td width=30%><%=LC.L_Payment_For%><%-- Payment For*****--%>:</td>
	<td width=70%><%=dpayFor%></td>
</tr>
<tr>
<td width=30%><%=LC.L_Mstone_Status%><%-- Milestone Status*****--%> <FONT class="Mandatory">* </FONT></td>

<%
		//KM-D-FIN7
		
		CodeDao cdMsStatDao = new CodeDao();
		boolean withSelect = true;
		String msStat = "";
		String roleCodePk="";
		
		String userIdFromSession = (String) tSession.getValue("userId");
	    if (! StringUtil.isEmpty(milestoneStudyId) && EJBUtil.stringToNum(milestoneStudyId) > 0)
		{
			ArrayList tId = new ArrayList();
			
			TeamDao teamRoleDao = new TeamDao();
			teamRoleDao.getTeamRights(EJBUtil.stringToNum(milestoneStudyId),EJBUtil.stringToNum(userIdFromSession));
			tId = teamRoleDao.getTeamIds();
							
			if (tId != null && tId.size() > 0)
			{
				ArrayList arRoles = new ArrayList();
				arRoles = teamRoleDao.getTeamRoleIds();		
				
				if (arRoles != null && arRoles.size() >0 )	
				{
					roleCodePk = (String) arRoles.get(0);
					
					if (StringUtil.isEmpty(roleCodePk))
					{
						roleCodePk="";
					}
				}	
				else
				{
					roleCodePk ="";
				}
				
			}	 
			else
			{
				roleCodePk ="";
			}
		} else
			{			
			  roleCodePk ="";
			}  
		
		cdMsStatDao.getCodeValuesForStudyRole("milestone_stat",roleCodePk);
		msStat = cdMsStatDao.toPullDownMilestone("milestoneStat",EJBUtil.stringToNum(milestoneStatFK), milestoneAchievedCount );
%>
	<td>
		<%=msStat%> 
	</td>
</tr>
</table>

<input type="hidden" name="milestoneType"  value='<%=milestoneType%>'>

<br>
<table><tr><td>
<% if(milestoneType.equals("VM") || milestoneType.equals("EM") ){ %>
	<!--<font class="Mandatory" size"1">'Patient Count' and 'Patient Status' are not applicable for 'Administrative Calendars'</font>
	-->
<% } %>
</td></tr></table>

<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="editmilestonerule"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>
</Form>

<%--
<script>

	calculate(document.milestone);

</script>
--%>
<%

} else { //end of if body for session

%>
  <jsp:include page="timeout_childwindow.jsp" flush="true"/>
  <%
}

%>
</DIV>
<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</BODY>


