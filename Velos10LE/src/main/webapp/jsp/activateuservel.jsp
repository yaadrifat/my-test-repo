<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT> 

</HEAD>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="addressB" scope="page" class="com.velos.eres.web.address.AddressJB" />
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>

<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,java.text.*,com.velos.eres.service.util.*"%>

<BODY>
<DIV>

<%
 HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))
   {   
	String supportEmail = Configuration.SUPPORTEMAIL;
	   
 String ipAdd = (String) tSession.getValue("ipAdd");

 String userPass="";
 String usereSign="";
 String messageSubtext="";
 String addressId="";
 

 String messageFrom = "";
 String messageText = "";
 String messageSubject = "";
 String messageHeader = "";
 String messageFooter = "";		
 String completeMessage = "";

 String messageTo = "";
 int rows = 0;
 String	usrEmail="";
 String userLoginName ="";

 String userMailStatus = "";
 int metaTime = 1;

// For the bugzilla issues #2093 on Feb 2005 by gopu
	rows = 0;
	CtrlDao urlCtrl = new CtrlDao();	
	urlCtrl.getControlValues("site_url");
	rows = urlCtrl.getCRows();
   	if (rows > 0)
	   {
	   	siteUrl = (String) urlCtrl.getCValue().get(0);
	   }
// End

 int ret =0;
 double randomNumber=Math.random();
 String randomNum=String.valueOf(randomNumber);

 String userId=request.getParameter("userId");



 int user = EJBUtil.stringToNum(userId);

 userB.setUserId(user);
 userB.getUserDetails();
 addressId=userB.getUserPerAddressId();
 addressB.setAddId(EJBUtil.stringToNum(addressId));
 addressB.getAddressDetails();
 usrEmail=addressB.getAddEmail();
 userLoginName =userB.getUserLoginName();

 userPass=randomNum.substring(2,10);
 usereSign=randomNum.substring(11,15);
 messageSubtext=" \n"+LC.L_New_Pwd/*New Password*****/+" :   " + userPass +  "\n"+LC.L_New_Esign/*New e-Signature*****/+" :   " + usereSign;
 userB.setUserPwd(userPass);
 userB.setUserESign(usereSign);

 userB.setIpAdd(ipAdd);

 userB.setUserStatus("A");
 userB.setUserAttemptCount("0");
 
 ret = userB.updateUser();	


	
	messageTo = usrEmail;

	CtrlDao userCtrl = new CtrlDao();	
	userCtrl.getControlValues("eresuser");
	rows = userCtrl.getCRows();
	if (rows > 0)
	   {
	   	messageFrom = (String) userCtrl.getCDesc().get(0);
	   }
	
    	messageSubject = MC.M_ReqChg_Pwd/*Request for change of password/esign*****/;	
    	messageHeader =MC.M_Dear_VeResMem/*Dear Velos eResearch Member*****/+"," ; 
    
      /* messageText = "\n\nAs per your request your Password/e-Signature has been reset. Your new Password/e-Signature is:\n\nLogin ID           :    " + userLoginName + messageSubtext + "\nApplication URL : " + siteUrl + "\n\nYou may of course change your Password/e-Signature at any time should you have any concerns regarding the privacy of your information. To change your Password/e-Signature, you will need to login with the above ID , go to the Personalize Account section and then change your Password/e-Signature.\n\nTo begin using the Velos eResearch system, go to " + siteUrl + ", enter your login ID and password and click on the Login button.\n **** Important ****\nPlease remember that the above information needs to be kept in a secure place. Any one with access to the above information can access your account information.\n\nIf you have any questions or feedback, please feel free to contact us at " + supportEmail + ".\n\n Velos eResearch Customer Support.\n\nVelos, Inc.\n2201 Walnut Avenue, Suite 208\nFremont, CA. 94538" ;******/
        Object[] arguments = {userLoginName,messageSubtext,siteUrl,siteUrl,supportEmail};
       messageText = VelosResourceBundle.getMessageString("M_PwdEsign_Reset",arguments);
      
       /*messageFooter = "\n\nThis email transmission and any documents, files or previous email messages attached to it may contain information that is confidential or legally privileged. If you are not the intended recipient or a person responsible for delivering this transmission to the intended recipient you are hereby notified that you must not read this transmission and that any disclosure, copying, printing, distribution or use of this transmission is strictly prohibited. If you have received this transmission in error, please immediately notify the sender by telephone or return email and delete the original transmission and its attachments without reading or saving in any manner." ;*****/
       messageFooter = "\n\n"+MC.M_EmailContain_SecureInfo+"." ;
    	
    	completeMessage = messageHeader  + messageText + messageFooter ;

	try{
       	
    	    VMailer vm = new VMailer();
        	VMailMessage msgObj = new VMailMessage();
        				
    		msgObj.setMessageFrom(messageFrom);
    		msgObj.setMessageFromDescription(MC.M_VeResCustSrv/*Velos eResearch Customer Services*****/);
    		msgObj.setMessageTo(messageTo);
    		msgObj.setMessageSubject(messageSubject);
    		msgObj.setMessageSentDate(new Date());
    		msgObj.setMessageText(completeMessage);
    
    		vm.setVMailMessage(msgObj);
    		userMailStatus = vm.sendMail(); 			
  	
	  }
	 catch(Exception ex)
      {
     	  userMailStatus = ex.toString();
       }
%>
<br><br><br><br><br>

 <%
  if ( EJBUtil.isEmpty(userMailStatus) )
  {
  	metaTime = 1;
	
  %>
	<p class = "sectionHeadings" align = center> <%=MC.M_DataSvdSucc_NotficSent%><%--Data was saved successfully and Notification sent.*****--%> </p>

 <%
	 } else
	{
	 	metaTime = 10;
     %>
		<p class = "redMessage" align = center> <%=MC.M_DataSvdSucc_CustSuppMsg%><%--Data was saved successfully but there was an error in sending notification to the user.<br>Please contact Customer Support with the following message*****--%>:<BR><%=userMailStatus%></p>

	 <%
  		}
	%>	

<META HTTP-EQUIV=Refresh CONTENT="<%=metaTime%>; URL=velosmsgs.jsp?userStatus=D" >
  
  <%
	}//end of if body for session
	else
	{
	%>
	<jsp:include page="timeout_admin.html" flush="true"/>
	<%
	}
%>

 <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
  
</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page="velosmenus" flush="true"/>
</div>
</BODY>

</HTML>
