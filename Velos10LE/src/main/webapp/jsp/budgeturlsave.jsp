<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="bgtApndxB" scope="request" class="com.velos.esch.web.bgtApndx.BgtApndxJB"/>
<%@ page language = "java" import = "com.velos.esch.service.util.EJBUtil,com.velos.eres.service.util.MC"%>

<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

<% String src;
	src= request.getParameter("srcmenu");
	String budgetType = request.getParameter("budgetType");
%>

<BODY>
<br>

<%
String mode = request.getParameter("mode");
String budgetApndxId=request.getParameter("budgetApndxId");	  
String budgetId=request.getParameter("budgetId");
String budgetApndxDesc=request.getParameter("budgetApndxDesc");
String budgetApndxUri=request.getParameter("budgetApndxUri");
String selectedTab=request.getParameter("selectedTab");
String budgetMode=request.getParameter("budgetMode");
String budgetTemplate=request.getParameter("budgetTemplate");

String fromRt = request.getParameter("fromRt");
if (fromRt==null || fromRt.equals("null")) fromRt="";

String eSign = request.getParameter("eSign");
String eventdocId = "";

HttpSession tSession = request.getSession(true);  
String ipAdd = (String) tSession.getValue("ipAdd");
String usr = null;
usr = (String) tSession.getValue("userId");

if (sessionmaint.isValidSession(tSession)) {	
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<% 	
	String oldESign = (String) tSession.getValue("eSign");

	if (!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
%>
	<form method="POST">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
  	 <input type="hidden" name="budgetApndxId" value="<%=budgetApndxId%>">
  	 <input type="hidden" name="mode" value="<%=mode%>">
  	 <input type="hidden" name="budgetId" value="<%=budgetId%>">
  	 <input type="hidden" name="selectedTab" value="<%=selectedTab%>">
	<Input type="hidden" name="fromRt" value="<%=fromRt%>"/>
	</FORM>
<%
	
		if(mode.equals("M")) {
		   bgtApndxB.setBgtApndxId(EJBUtil.stringToNum(budgetApndxId));
		   bgtApndxB.getBgtApndxDetails();
	   	}

   		bgtApndxB.setBgtApndxBudget(budgetId); 
		bgtApndxB.setBgtApndxDesc(budgetApndxDesc);
   		bgtApndxB.setBgtApndxType("U");
		bgtApndxB.setBgtApndxUri(budgetApndxUri);	
   
	   bgtApndxB.setIpAdd(ipAdd);
     // commented By amarnadh for issue #3011
//	   bgtApndxB.setModifiedBy(usr);
	   
	   if(mode.equals("N")) {
	   	   bgtApndxB.setCreator(usr);  
		   bgtApndxB.setBgtApndxDetails();
		 }
		else {   
		// Added By Amar for Bugzilla issue #3130
			bgtApndxB.setModifiedBy(usr);
			bgtApndxB.updateBgtApndx();
		}
			
%>
<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%-- Data was saved successfully.*****--%> </p>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=budgetapndx.jsp?budgetId=<%=budgetId%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&budgetType=<%=budgetType%>&budgetTemplate=<%=budgetTemplate%>&mode=<%=budgetMode%>&fromRt=<%=fromRt%>" >

<%
	}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
 
</BODY>
</HTML>


