<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<title><%=LC.L_UpdateOrNew_Request%><%-- Update/New Request*****--%></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%String src = request.getParameter("srcmenu");%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<BODY>
<jsp:useBean id ="msgcntrB" scope="session" class="com.velos.eres.web.msgcntr.MsgcntrJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id ="studyB" scope="session" class="com.velos.eres.web.study.StudyJB"/>


<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.web.user.UserJB,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.ulink.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@ page import="com.velos.eres.service.util.LC"%>
<DIV class="browserDefault" id="div1">


<%
String studyId = request.getParameter("studyId");
String userTo = request.getParameter("authorId");
String userFrom = request.getParameter("userId");
String msgText = request.getParameter("msgText");
String reqType = request.getParameter("reqType");
String search = request.getParameter("search");
String searchOn = request.getParameter("searchOn");
String selectedTab = request.getParameter("selectedTab");
String msgStat = "U";
int verifyValue = 0;

HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
	{
String ipAdd = (String) tSession.getValue("ipAdd");
 String usr = null;
 usr = (String) tSession.getValue("userId");
 String uName =(String) tSession.getValue("userName");

StudyDao studyDao = new StudyDao();
studyDao = studyB.verifyStudyRequest(studyId, userFrom);
verifyValue = studyDao.getVerifyValue();
%>
<br>
 <P class = "userName"> <%= uName %> </P>



<%--
<%
if(verifyValue == 1) {
%>
<P class="defComments">
This study is for the account for which you are registered. Please contact your account administrator.
</P>
<%
} else

--%>
<%
if(verifyValue == 2 && reqType.equals("M")) {
%>
<P class="defComments">
<%=MC.M_UsrAcc_CntCopySame%><%-- Some user of your account already has a copy of this
<%=LC.Std_Study_Lower%> so you cannot be given a copy of the same.*****--%>
</P>
<%
} else if(verifyValue == 3 && reqType.equals("V")) {
%>
<P class="defComments">
<%=MC.M_AldyView_PermStd%><%-- You already have View permissions for this <%=LC.Std_Study_Lower%>.*****--%>
</P>
<%
} else if(verifyValue == 4) {
%>
<P class="defComments">
<%=MC.M_AldyModify_PermStd%><%--You already have Modify permissions for this <%=LC.Std_Study_Lower%>.*****--%>
</P>
<%
} else {
	msgcntrB.setMsgcntrStudyId(studyId);
	msgcntrB.setMsgcntrFromUserId(userFrom);
	msgcntrB.setMsgcntrToUserId(userTo);
	msgcntrB.setMsgcntrText(msgText);
	msgcntrB.setMsgcntrReqType(reqType);
	msgcntrB.setMsgcntrStatus(msgStat);
	msgcntrB.setCreator(usr);
	msgcntrB.setIpAdd(ipAdd);
	msgcntrB.setMsgcntrDetails();

%>
<P class="defComments"><%=MC.M_YourReq_LoggedSucc%><%--Your request has been logged successfully*****--%></P>
<%
}
%>
<A href = "studySearchAfterLogin.jsp?&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&search=<%=search%>&searchOn=<%=searchOn%> " type="submit"><%=LC.L_Back%></A>

<%

} //end of if body for session
else
{
%>

<jsp:include page="timeout.html" flush="true"/>
<%
}
%>

<div>
<jsp:include page="bottompanel.jsp" flush="true"/>

</div>

</div>

<div class ="mainMenu" id = "emenu">
	<jsp:include page="getmenu.jsp" flush="true"/>
</div>



</BODY>
</HTML>
