<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>


 
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
	
 function  validate(formobj)
 {	
  	//if specific study radio button is selected, then the study should also have been selected
	len = formobj.dispFormLink.length;
	
	for (i=0;i<len;i++) {
	    if (formobj.dispFormLink[i].checked) {
			
			radioSelected = formobj.dispFormLink[i].value;
			
			if(formobj.dispFormLink[i].disabled)
			{
				formobj.hdnDispFormLink.value=formobj.dispFormLink[i].value;
			
			}
			break;
		}
	}
	
//KM-2888
if(formobj.status.value!="Active" && formobj.status.value!="Offline" && formobj.status.value!="Lockdown" && formobj.status.value!="MIP"){
	
	if (formobj.formLinkedFrom.value=="A") {;
		if(formobj.mode.value!=null && (radioSelected == "SA" || radioSelected == "A" || radioSelected == "PS"|| radioSelected == "PR" || radioSelected == "PA"))
		{			
			if(formobj.accFormStudy.value!="")
			{ 
				if(radioSelected == "SA"){
					/*alert("Form is linked to '<%=LC.Std_Study%> Level (All)'. You cannot select a <%=LC.Std_Study%>.");*****/
					alert("<%=MC.M_FrmLinkStd_AllCntSelStd%>");
				}
				else if(radioSelected == "A")
				{
					/*alert("Form is linked to 'Account Level'. You cannot select a <%=LC.Std_Study%>.");*****/
					alert("<%=MC.M_FrmLnkToAccLvl_CntSelStd%>");
				}
				else if(radioSelected == "PS")
				{
					/*alert("Form is linked to '<%=LC.Pat_Patients%> Level (All <%=LC.Std_Studies%>)'. You cannot select a <%=LC.Std_Study%>.");*****/
					alert("<%=MC.M_FrmLinkPat_StdCntSelect%>");
				}
				else if(radioSelected == "PR")
				{
					/*alert("Form is linked to '<%=LC.Pat_Patients%> Level (All <%=LC.Std_Studies%> - Restricted)'. You cannot select a <%=LC.Std_Study%>.");*****/
					alert("<%=MC.M_FrmLinkPat_StdRestCntSel%>");
				}
				else if(radioSelected == "PA")
				{
					/*alert("Form is linked to '<%=LC.Pat_Patients%> Level (All)'. You cannot select a <%=LC.Std_Study%>.");*****/
					alert("<%=MC.M_FrmLinkPat_AllCntSelStd%>");
				}
				formobj.accFormStudy.focus();
				return false;
			}
			//formobj.accFormStudy.value = "";
		}
		
		if ((radioSelected=="S") || (radioSelected=="SP")) {			
		if((formobj.stdPageRight.value != 0) || (formobj.studyIdi.value == 0)){
  		if (!(validate_col('Study',formobj.accFormStudy))) return false;
			}
		}
	}  
}
    if (!(validate_col('e-Sign',formobj.eSign))) return false
	if(isNaN(formobj.eSign.value) == true) {
		/*alert("Incorrect e-Signature. Please enter again");*****/
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");
		formobj.eSign.focus();
		return false;
	}	

 }
 function changeStudyDropDown(formobj,lfDisplayType,linkedFormIdStr,statCodeId,changeVal){
		
	if(lfDisplayType=="A")
	 {		
	 	formobj.action="attributesForStudyAccount.jsp?linkedFormId="+linkedFormIdStr+"&statCodeId="+statCodeId+"&changeVal="+changeVal+"&mode=changeToA" ;
	 }
	 else if(lfDisplayType=="SA")
	 {
		 formobj.action="attributesForStudyAccount.jsp?linkedFormId="+linkedFormIdStr+"&statCodeId="+statCodeId+"&changeVal="+changeVal+"&mode=changeToSA" ;
	 }
	 else if(lfDisplayType=="PA")
	 {
		 formobj.action="attributesForStudyAccount.jsp?linkedFormId="+linkedFormIdStr+"&statCodeId="+statCodeId+"&changeVal="+changeVal+"&mode=changeToPA" ;
	 }
	 else if(lfDisplayType=="PS")
	 {
		 formobj.action="attributesForStudyAccount.jsp?linkedFormId="+linkedFormIdStr+"&statCodeId="+statCodeId+"&changeVal="+changeVal+"&mode=changeToPS" ;
	 }
	  else if(lfDisplayType=="PR")
	 {
		 formobj.action="attributesForStudyAccount.jsp?linkedFormId="+linkedFormIdStr+"&statCodeId="+statCodeId+"&changeVal="+changeVal+"&mode=changeToPR" ;
	 }
		formobj.submit();
 }
 
 function openOrgWindow(formobj)
  {
	var formorg1=formobj.formOrgIds.value;
	formorg1 = encodeString(formorg1);//KM-4Nov08
    windowName=window.open("selectOrg.jsp?openerform=selectForms&formRows=1&selectOrgs="+formorg1,"Information1","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=350,height=300");
	windowName.focus();
	
 }
 
 
 function openGroupWindow(formobj,counter,formRows) {
    var selGrpNames1 =formobj.selGrpIds.value;
	selGrpNames1 = encodeString(selGrpNames1);//KM-4Nov08
    windowName=window.open("selectGroup.jsp?openerform=selectForms&formRows=1&selectGroups="+selGrpNames1,"Information1","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=350,height=300");
	windowName.focus();
}
function getGroup(formobj,dispFld,dataFld) 
{
  var grpNames1=formobj.grpIds.value;
  grpNames1 = encodeString(grpNames1);//KM-4Nov08
  if (formobj.noshare.checked==true){
    windowName=window.open("selectGroup.jsp?openerform=selectForms&formRows=1&idFld="+dataFld+"&dispFld="+dispFld+"&selectGroups="+grpNames1,"Information1","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=350,height=300");
	windowName.focus();
	} else
	{
		/*alert("Please select 'Keep response private to author' before proceeding");*****/
		alert("<%=MC.M_SelKeepResp_BeforeProc%>");
	return;
	}
}

function getUser(formobj) 
{
	if (formobj.noshare.checked) 
	{

	  var names = formobj.usrNames.value;

	  var ids = formobj.usrIds.value;

	  
	  

	  

	  while(names.indexOf(' ') != -1){

	  	names = names.replace(' ','~');

	  }

	  

	

//alert(windowToOpen);

      window.open("multipleusersearchdetails.jsp?dataFld=usrIds&dispFld=usrNames&fname=&lname=&from=selectForms&mode=initial&ids=" + ids + "&names=" + names ,"User","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
} 
else {
	/*alert("Please select 'Keep response private to author' before proceeding");*****/
	alert("<%=MC.M_SelKeepResp_BeforeProc%>");
	return;
}

;}

function keepPrivate(formobj)
{
 if (formobj.noshare.checked)
 	formobj.noshareId.value="1";
	else 
	{
	formobj.noshareId.value="0";
	formobj.usrIds.value="";
	formobj.grpIds.value="";
	formobj.usrNames.value="";
	formobj.grpNames.value="";
	}
	
	
}


</SCRIPT>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
 
<jsp:useBean id="objShareB" scope="request" class="com.velos.eres.web.objectShare.ObjectShareJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="catLibJB" scope="request"  class="com.velos.eres.web.catLib.CatLibJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="formLibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id="linkedFormsJB" scope="request"  class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<!-- Virendra: Fixed Bug no. 4688, added import com.velos.eres.service.util.StringUtil  -->
<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.*"%>

<jsp:include page="include.jsp" flush="true"/>
<DIV class="popDefault" id="div1">

<%
 HttpSession tSession = request.getSession(true); 
 
 if (sessionmaint.isValidSession(tSession))
 {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/> 

<%

	 int accountId = EJBUtil.stringToNum((String) tSession.getValue("accountId"));
	 String userId = (String) tSession.getValue("userId");
	 String linkedFormIdStr  =request.getParameter("linkedFormId") ; 
	 
	 String statCodeId = request.getParameter("statCodeId");
	 
	 int istatus = EJBUtil.stringToNum(statCodeId);
	 String changeVal = request.getParameter("changeVal");
	 String status = "";
	 //Virendra: Fixed Bug no. 4688, added variable addAnotherWidth  -->
	 int addAnotherWidth = 0;
	
	CodeDao cd = new CodeDao();
		int wipId=cd.getCodeId("frmstat","W");
		int activeId=cd.getCodeId("frmstat","A");
		int lockdownId = cd.getCodeId("frmstat","L");
		int deactivateId = cd.getCodeId("frmstat","D");
		int offlineId = cd.getCodeId("frmstat","O");
		int migrationId = cd.getCodeId("frmstat","M"); //KM
		String makeDisabled = "";
		if(wipId==istatus)
			{
			status = "WIP";
			}
		else if(activeId==istatus)
			{
				status = "Active";
			}
		else if(lockdownId == istatus)
			{
				status = "Lockdown";			
			}
		else if(deactivateId == istatus)
			{
				status = "Deactivated";			
			}
		else if(offlineId == istatus)
			{
				status = "Offline";			
			}
		else if(migrationId == istatus)
		 {
			   status = "MIP";
		 }

	//KM-Modified for the issue #2888.
	if(status.equals("Offline") || status.equals("Lockdown")  || status.equals("Active") || status.equals("MIP"))
	{
		changeVal = "no";
		makeDisabled ="disabled";
	 }

	
	 String mode = request.getParameter("mode");
	 //String status = ""; 
	/* if(activeId.equals(statCodeId))
	 {
		status = "active";
	 }*/

	 int linkedFormId = EJBUtil.stringToNum( linkedFormIdStr);
	 int studyIdi  = 0 ;
	 LinkedFormsDao linkedFormsDao = new LinkedFormsDao( ); 
	 String lfDisplayType = "";
	 String lfEntry = "";
	 String studyId= "";
	 String orgNames = "";
	 String grpNames = "";
	 String formOrgIds="";
	 String grpIds="";
	 ArrayList studyIds = new ArrayList();
	 ArrayList studyNumbers = new ArrayList();
	 ArrayList formIds = new ArrayList();
	  
		
	 linkedFormsJB.setLinkedFormId(linkedFormId) ;
	 linkedFormsJB.getLinkedFormDetails(); 
	 lfDisplayType = linkedFormsJB.getLFDisplayType(); 
	 lfEntry = linkedFormsJB.getLFEntry();
	 if (lfEntry==null) lfEntry=""; 
	 studyId = linkedFormsJB.getStudyId() ;

	 if (studyId==null) studyId="0";
	 
	 String formLinkedFrom = linkedFormsJB.getLnkFrom();
	 studyIdi =  EJBUtil.stringToNum(studyId) ;
	 %>
	 <title>
			<%if (formLinkedFrom.equals("A")) {
			  if ((lfDisplayType.trim()).equals("PA") )
			{%>
				  <%-- Edit the Form Linked to All <%=LC.Pat_Patient%> Level ***** --%><%=MC.M_EdtFrmLink_PatLevel%>
			<%}
			else if((lfDisplayType.trim()).equals("A") )
			{%>
				 <%-- Edit the Form Linked to Account ***** --%><%=MC.M_EditFrm_LnkToAcc%>
			<%}
			else if((lfDisplayType.trim()).equals("SA") )
			{%>
				<%-- Edit the Form Linked to All <%=LC.Std_Study%> Level ***** --%><%=MC.M_EdtFrmLink_AllStd%>
			<%}
			else if((lfDisplayType.trim()).equals("PS") )
			{%>
				<%-- Edit the Form Linked to All <%=LC.Std_Study%> <%=LC.Pat_Patient%> Level ***** --%><%=MC.M_EdtFrmLinkAll_StdPat%>
			<%}else if((lfDisplayType.trim()).equals("PR") )
			{%>
				<%-- Edit the Form Linked to All <%=LC.Std_Study%> <%=LC.Pat_Patient%> - Restricted Level ***** --%><%=MC.M_EdtFrmLinkAll_StdPatRest%>
			<%}
			else if((lfDisplayType.trim()).equals("S") )
			{%>
				<%-- Edit the Form Linked to Specific <%=LC.Std_Study%> Level ***** --%><%=MC.M_EdtFrmLink_SpecStd%>
			<%}
			else if((lfDisplayType.trim()).equals("SP") )
			{%>
				<%-- Edit the Form Linked to Specific <%=LC.Std_Study%> <%=LC.Pat_Patient%> Level ***** --%><%=MC.M_EditFrm_ToStdPatLvl%>
			<%}
		}
		if (formLinkedFrom.equals("S")) {
		
		 if(( lfDisplayType.trim()).equals("S")){%> 
	          <%=MC.M_EdtFrmLink_Std%><%--Edit the Form Linked to <%=LC.Std_Study%>*****--%> 
			<%}
			if( lfDisplayType.equals("SP") ) 
			 { %>
			  <%=MC.M_EdtFrmLink_Pat%><%--Edit the Form Linked to <%=LC.Pat_Patient%>*****--%>			
			<%}
		
		}%>
			

	</title>	
		 <%
	 linkedFormsDao =  linkedFormsJB.getLinkedFormsDao(); 
 
	 orgNames=(  linkedFormsDao.getOrgNames()   == null)?"":linkedFormsDao.getOrgNames( ) ;
	 
	  
	 formOrgIds=(  linkedFormsDao.getOrgIds()   == null)?"":linkedFormsDao.getOrgIds( ) ;	 
	  
  	 grpNames=(  linkedFormsDao.getGrpNames()   == null)?"":linkedFormsDao.getGrpNames( ) ;
	 grpIds=(  linkedFormsDao.getGrpIds()   == null)?"":linkedFormsDao.getGrpIds( ) ;
		 
	 
	 int formId = EJBUtil.stringToNum(linkedFormsJB.getFormLibId());
	 formLibB.setFormLibId(formId);
	 formLibB.getFormLibDetails();
	 String formName = formLibB.getFormLibName();

	  //study dropdown
	 String study="";
	 StudyDao studyDao = new StudyDao();	
	 studyDao.getStudyValuesForUsers(userId);	
	 studyIds = studyDao.getStudyIds();
	 studyNumbers = studyDao.getStudyNumbers();	 
	 String userIdFromSession = (String) tSession.getValue("userId");
	 String accId=(String) tSession.getValue("accId");
	 
	 
	 /*retrieve object share ids*/
	 
	 objShareB.getObjectShareDetails(linkedFormId,"U","5",userIdFromSession, accId);
	
	    String usrNames=objShareB.getShrdWithNames();
	    String usrIds=objShareB.getShrdWithIds();
	    usrIds=(usrIds==null)?"":usrIds;
	    usrNames=(usrNames==null)?"":usrNames;
	    
	    usrNames=usrNames.replace(',',';');
	    
	    objShareB.getObjectShareDetails(linkedFormId,"G","4",userIdFromSession, accId);
	
	    String grpNamesResp=objShareB.getShrdWithNames();
	    String grpIdsResp=objShareB.getShrdWithIds();
	    grpIdsResp=(grpIdsResp==null)?"":grpIdsResp;
	    grpNamesResp=(grpNamesResp==null)?"":grpNamesResp;
	    
	    objShareB.getObjectShareDetails(linkedFormId,"P","6",userIdFromSession, accId);
	    String noshareId=objShareB.getShrdWithIds();
	    noshareId=(noshareId==null)?"":noshareId;
	    System.out.println("NoshareId retrieved" + noshareId);
	    int len=noshareId.length();
	    
	    
	    
	
	 //////
	
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

	int account_form_rights = 0;	
   
    int pageRight = 0;
	int stdPageRight = 0 ;

	ArrayList tId ;
	tId = new ArrayList();
	
	if(studyIdi >0)
	{									
		TeamDao teamDao = new TeamDao();
		teamDao.getTeamRights(studyIdi,EJBUtil.stringToNum(userIdFromSession));
		tId = teamDao.getTeamIds();

		if (tId.size() > 0) 	
		{
			StudyRightsJB stdRights = new StudyRightsJB();
			stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));
			
					ArrayList teamRights ;
					teamRights  = new ArrayList();
					teamRights = teamDao.getTeamRights();
						 
					stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
					stdRights.loadStudyRights();
					
		
			stdPageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
		}								
	}


 				if (formLinkedFrom.equals("S")) 
				{    
					pageRight = stdPageRight;
				}	 
	 		    else
  	           {
				   account_form_rights = Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMS"));
				   pageRight = account_form_rights;
			   }


    if (pageRight >= 4) 
    {

	String deFormOrg = "";
	String deFormOrgId = "";
	
	String deFormGrp = "";
	String deFormGrpId = "";
	
	String radioName = "";
	String formCharName = "";
	String accFormStudy = "";
	if(mode!=null)
		{
		  studyIdi=0;
		}

	accFormStudy = EJBUtil.createPullDown("accFormStudy", studyIdi, studyIds, studyNumbers);
	if(stdPageRight==0)
		{
		accFormStudy = EJBUtil.createPullDown("accFormStudy", 0, studyIds, studyNumbers);
		}
	
%>
		
   <p class="sectionHeadings"> <%-- Edit Forms >> Manage Account >> Form Management ***** --%><%=MC.M_EdtFrm_MngAccFrm%> </p>


<P class="defcomments"><%-- Form Name ***** --%><%=LC.L_Frm_Name%>: <%=formName%> </P>
<% String studyNum = "";
if(studyIdi!=0)
{
	if (formLinkedFrom.equals("S") || (formLinkedFrom.equals("A") && ((lfDisplayType.trim()).equals("S") || (lfDisplayType.trim()).equals("SP"))) ) {
		 studyB.setId(studyIdi);
		 studyB.getStudyDetails();
		 studyNum = studyB.getStudyNumber();
		 
		 %>
	<%if(stdPageRight>0){%>
	 <P class="defcomments"><%-- <%=LC.Std_Study%> Number***** --%><%=LC.L_Study_Number%>: <%=studyNum%> </P>
	<%}else{
		Object[] arguments = {studyNum};
	%>
	<P class = "defComments"><FONT class="Mandatory"> <%=VelosResourceBundle.getMessageString("M_NoRgtsStd_FormAssoc",arguments)%><%--You do not have rights to <b>Study Number:</b>, to which this form is associated.***** --%> </FONT></P>
		
<%
		 }
   }
}
if(studyNum.equals("")){
studyNum = "-";
}

 
 
 
	 
	 

%>

<Form name="selectForms" id="selFrmsId" method="post" action="attributesForStudyAccountSubmit.jsp" onsubmit="if (validate(document.selectForms)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

<input type="hidden" name="formLinkedFrom" value="<%=formLinkedFrom%>">

<input type="hidden" name="linkedFormId"  value="<%=linkedFormIdStr%>" >

<input type = "hidden" name="hdnDispFormLink" >

<input type="hidden" name="status" value="<%=status%>">

<input type="hidden" name="hdnCharName" value="<%=lfEntry%>">

	
<input type="hidden" name="mode" value="<%=mode%>">
	<table width = "99%" border = "0" cellspacing="0" cellpadding="0">
	 <tr class="browserEvenRow"> <td ><%-- Display Form Link ***** --%><%=LC.L_Disp_FormLink%></td>
	 <td>
	 <% 
		if(mode!=null)
		{
			if(mode.equals("changeToA"))
			{
				lfDisplayType ="A";
			}
			else if(mode.equals("changeToSA"))
			{
				lfDisplayType ="SA";
			}
			else if(mode.equals("changeToPS"))
			{
				lfDisplayType ="PS";
			}
			else if(mode.equals("changeToPR"))
			{
				lfDisplayType ="PR";
			}
			else if(mode.equals("changeToPA"))
			{
				lfDisplayType ="PA";
			}
		}


if (formLinkedFrom.equals("A")) 
{

	
	
	   if ((lfDisplayType.trim()).equals("PA") )
	   {%>
        <input type="radio" name="dispFormLink" value="PA" CHECKED <%=makeDisabled%> onclick="changeStudyDropDown(document.selectForms,'PA','<%=linkedFormIdStr%>','<%=statCodeId%>','<%=changeVal%>')"><%-- <%=LC.Pat_Patient%> Level (All)***** --%><%=LC.L_PatLvl_All%><br>
	  <%} else {%>
	    <input type="radio" name="dispFormLink" value="PA" <%=makeDisabled%> onclick="changeStudyDropDown(document.selectForms,'PA','<%=linkedFormIdStr%>','<%=statCodeId%>','<%=changeVal%>')"><%-- <%=LC.Pat_Patient%> Level (All)***** --%><%=LC.L_PatLvl_All%><br>	  
	  <%}
	 
  	  
	  if ((lfDisplayType.trim()).equals("SA") )
			           {%>
           			 <input type="radio" name="dispFormLink" value="A" <%=makeDisabled%> onclick="changeStudyDropDown(document.selectForms,'A','<%=linkedFormIdStr%>','<%=statCodeId%>','<%=changeVal%>')"><%-- Account Level ***** --%><%=LC.L_Acc_Level%><br>
           			 <input type="radio" name="dispFormLink" value="SA" CHECKED <%=makeDisabled%> onclick="changeStudyDropDown(document.selectForms,'SA','<%=linkedFormIdStr%>','<%=statCodeId%>','<%=changeVal%>')"  ><%-- <%=LC.Std_Study%> Level (All)***** --%><%=LC.L_StdLvl_All%><br>
 			 		 <input type="radio" name="dispFormLink" value="PS" <%=makeDisabled%> onclick="changeStudyDropDown(document.selectForms,'PS','<%=linkedFormIdStr%>','<%=statCodeId%>','<%=changeVal%>')"><%-- <%=LC.Pat_Patient%> Level (All <%=LC.Std_Studies%>)***** --%><%=MC.M_PatLvl_AllStd%><br>
					 <input type="radio" name="dispFormLink" value="PR"  <%=makeDisabled%> onclick="changeStudyDropDown(document.selectForms,'PR','<%=linkedFormIdStr%>','<%=statCodeId%>','<%=changeVal%>')"><%-- <%=LC.Pat_Patient%> Level (All <%=LC.Std_Studies%> - Restricted)***** --%><%=MC.M_PatLvl_AllStdRest%><br>

                  <%}
				else if ((lfDisplayType.trim()).equals("A") )

			               {%>
           			 <input type="radio" name="dispFormLink" value="A"  checked <%=makeDisabled%> onclick="changeStudyDropDown(document.selectForms,'A','<%=linkedFormIdStr%>','<%=statCodeId%>','<%=changeVal%>')"  ><%-- Account Level ***** --%><%=LC.L_Acc_Level%><br>
           			 <input type="radio" name="dispFormLink" value="SA" <%=makeDisabled%> onclick="changeStudyDropDown(document.selectForms,'SA','<%=linkedFormIdStr%>','<%=statCodeId%>','<%=changeVal%>')"><%-- <%=LC.Std_Study%> Level (All)***** --%><%=LC.L_StdLvl_All%><br>
					 <input type="radio" name="dispFormLink" value="PS" <%=makeDisabled%> onclick="changeStudyDropDown(document.selectForms,'PS','<%=linkedFormIdStr%>','<%=statCodeId%>','<%=changeVal%>')"><%-- <%=LC.Pat_Patient%> Level (All <%=LC.Std_Studies%>)***** --%><%=MC.M_PatLvl_AllStd%><br>
					 <input type="radio" name="dispFormLink" value="PR" <%=makeDisabled%> onclick="changeStudyDropDown(document.selectForms,'PR','<%=linkedFormIdStr%>','<%=statCodeId%>','<%=changeVal%>')"><%-- <%=LC.Pat_Patient%> Level (All <%=LC.Std_Studies%> - Restricted)***** --%><%=MC.M_PatLvl_AllStdRest%><br>
                  <%} 
			      else if ((lfDisplayType.trim()).equals("S") )
			               {%>
           			 <input type="radio" name="dispFormLink" value="A" <%=makeDisabled%> onclick="changeStudyDropDown(document.selectForms,'A','<%=linkedFormIdStr%>','<%=statCodeId%>','<%=changeVal%>')" ><%-- Account Level ***** --%><%=LC.L_Acc_Level%><br>
           			 <input type="radio" name="dispFormLink" value="SA" <%=makeDisabled%> onclick="changeStudyDropDown(document.selectForms,'SA','<%=linkedFormIdStr%>','<%=statCodeId%>','<%=changeVal%>')"><%-- <%=LC.Std_Study%> Level (All)***** --%><%=LC.L_StdLvl_All%><br>
					 <input type="radio" name="dispFormLink" value="PS" <%=makeDisabled%> onclick="changeStudyDropDown(document.selectForms,'PS','<%=linkedFormIdStr%>','<%=statCodeId%>','<%=changeVal%>')"><%-- <%=LC.Pat_Patient%> Level (All <%=LC.Std_Studies%>)***** --%><%=MC.M_PatLvl_AllStd%><br>
					 <input type="radio" name="dispFormLink" value="PR" <%=makeDisabled%> onclick="changeStudyDropDown(document.selectForms,'PR','<%=linkedFormIdStr%>','<%=statCodeId%>','<%=changeVal%>')"><%-- <%=LC.Pat_Patient%> Level (All <%=LC.Std_Studies%> - Restricted)***** --%><%=MC.M_PatLvl_AllStdRest%><br>
					 <%} 
			     else if ((lfDisplayType.trim()).equals("PS") )
			           {%>
           			 <input type="radio" name="dispFormLink" value="A" <%=makeDisabled%> onclick="changeStudyDropDown(document.selectForms,'A','<%=linkedFormIdStr%>','<%=statCodeId%>','<%=changeVal%>')"  ><%-- Account Level ***** --%><%=LC.L_Acc_Level%><br>
           			 <input type="radio" name="dispFormLink" value="SA" <%=makeDisabled%> onclick="changeStudyDropDown(document.selectForms,'SA','<%=linkedFormIdStr%>','<%=statCodeId%>','<%=changeVal%>')"><%-- <%=LC.Std_Study%> Level (All)***** --%><%=LC.L_StdLvl_All%><br>
				 <input type="radio" name="dispFormLink" value="PS" checked <%=makeDisabled%> onclick="changeStudyDropDown(document.selectForms,'PS','<%=linkedFormIdStr%>','<%=statCodeId%>','<%=changeVal%>')"><%-- <%=LC.Pat_Patient%> Level (All <%=LC.Std_Studies%>)***** --%><%=MC.M_PatLvl_AllStd%><br>
				 <input type="radio" name="dispFormLink" value="PR" <%=makeDisabled%> onclick="changeStudyDropDown(document.selectForms,'PR','<%=linkedFormIdStr%>','<%=statCodeId%>','<%=changeVal%>')"><%-- <%=LC.Pat_Patient%> Level (All <%=LC.Std_Studies%> - Restricted)***** --%><%=MC.M_PatLvl_AllStdRest%><br>
               <%}else if ((lfDisplayType.trim()).equals("PR") )
			           {%>
           			 <input type="radio" name="dispFormLink" value="A" <%=makeDisabled%> onclick="changeStudyDropDown(document.selectForms,'A','<%=linkedFormIdStr%>','<%=statCodeId%>','<%=changeVal%>')"  ><%-- Account Level ***** --%><%=LC.L_Acc_Level%><br>
           			 <input type="radio" name="dispFormLink" value="SA" <%=makeDisabled%> onclick="changeStudyDropDown(document.selectForms,'SA','<%=linkedFormIdStr%>','<%=statCodeId%>','<%=changeVal%>')"><%-- <%=LC.Std_Study%> Level (All)***** --%><%=LC.L_StdLvl_All%><br>
				 <input type="radio" name="dispFormLink" value="PS" <%=makeDisabled%> onclick="changeStudyDropDown(document.selectForms,'PS','<%=linkedFormIdStr%>','<%=statCodeId%>','<%=changeVal%>')"><%-- <%=LC.Pat_Patient%> Level (All <%=LC.Std_Studies%>)***** --%><%=MC.M_PatLvl_AllStd%><br>
				 <input type="radio" name="dispFormLink" value="PR" checked <%=makeDisabled%> onclick="changeStudyDropDown(document.selectForms,'PR','<%=linkedFormIdStr%>','<%=statCodeId%>','<%=changeVal%>')"><%-- <%=LC.Pat_Patient%> Level (All <%=LC.Std_Studies%> - Restricted)***** --%><%=MC.M_PatLvl_AllStdRest%><br>	
               <%} 
				  else 
				  {%>
           			 <input type="radio" name="dispFormLink" value="A" <%=makeDisabled%> onclick="changeStudyDropDown(document.selectForms,'A','<%=linkedFormIdStr%>','<%=statCodeId%>','<%=changeVal%>')" ><%-- Account Level ***** --%><%=LC.L_Acc_Level%><br>
           			 <input type="radio" name="dispFormLink" value="SA" <%=makeDisabled%> onclick="changeStudyDropDown(document.selectForms,'SA','<%=linkedFormIdStr%>','<%=statCodeId%>','<%=changeVal%>')"><%-- <%=LC.Std_Study%> Level (All)***** --%><%=LC.L_StdLvl_All%><br>
					 <input type="radio" name="dispFormLink" value="PS" <%=makeDisabled%> onclick="changeStudyDropDown(document.selectForms,'PS','<%=linkedFormIdStr%>','<%=statCodeId%>','<%=changeVal%>')"><%-- <%=LC.Pat_Patient%> Level (All <%=LC.Std_Studies%>)***** --%><%=MC.M_PatLvl_AllStd%><br>
					 <input type="radio" name="dispFormLink" value="PR" <%=makeDisabled%> onclick="changeStudyDropDown(document.selectForms,'PR','<%=linkedFormIdStr%>','<%=statCodeId%>','<%=changeVal%>')"><%-- <%=LC.Pat_Patient%> Level (All <%=LC.Std_Studies%> - Restricted)***** --%><%=MC.M_PatLvl_AllStdRest%><br>
				  <%}

			    if ((lfDisplayType.trim()).equals("SP") )
	            {%>
           			 <input type="radio" name="dispFormLink" value="S" <%=makeDisabled%> ><%-- Specific <%=LC.Std_Study%> Level***** --%><%=LC.L_Spec_StdLvl%><br>
           			 <input type="radio" name="dispFormLink" value="SP" checked  <%=makeDisabled%>><%-- Specific <%=LC.Std_Study%> <%=LC.Pat_Patient%> Level***** --%><%=MC.M_SpecStd_PatLvl%><br>
				     <br>
   			   	     <%if(changeVal.equals("no")){%>
							<%=LC.L_Study%>: <%=studyNum%>
					 <%}else{%>
							<%=LC.L_Study%> <%=accFormStudy%>						 			
						<%}%>						 			
                <%} else if ((lfDisplayType.trim()).equals("S") )
	            {%>
           			 <input type="radio" name="dispFormLink" value="S" checked  <%=makeDisabled%>><%-- Specific <%=LC.Std_Study%> Level***** --%><%=LC.L_Spec_StdLvl%><br>
           			 <input type="radio" name="dispFormLink" value="SP" <%=makeDisabled%>><%-- Specific <%=LC.Std_Study%> <%=LC.Pat_Patient%> Level***** --%><%=MC.M_SpecStd_PatLvl%><br>
				     <br>
   			   	     
					 <%if(changeVal.equals("no")){%>
							<%=LC.L_Study%>: <%=studyNum%>
						<%}else{%>
							<%=LC.L_Study%> <%=accFormStudy%>						 			
						 <%}%>					 			
  			    <%} else {%>
           			 <input type="radio" name="dispFormLink" value="S"  <%=makeDisabled%>><%-- Specific <%=LC.Std_Study%> Level***** --%><%=LC.L_Spec_StdLvl%><br>
           			 <input type="radio" name="dispFormLink" value="SP" <%=makeDisabled%>><%-- Specific <%=LC.Std_Study%> <%=LC.Pat_Patient%> Level***** --%><%=MC.M_SpecStd_PatLvl%><br>
				     <br>
					 
					<%if(changeVal.equals("no")){%>
							<%=LC.L_Study%>: <%=studyNum%>
						<%}else{%>
							<%=LC.L_Study%> <%=accFormStudy%>						 			
						 <%}%>
				<%}
				
  		 
		
} //formLinkedFrom=A 

else if (formLinkedFrom.equals("S")) 
{
%>
  <input type="hidden" name="accFormStudy" value=<%=studyId%>>
<%	  if(( lfDisplayType.trim()).equals("S")      ) 
	           { %>
			 <input type="radio" name="dispFormLink" value="S" Checked <%=makeDisabled%>><%=LC.L_Study%><br>
			 <input type="radio" name="dispFormLink" value="SP" <%=makeDisabled%>><%=LC.L_Patient%>
			<%}
			if( lfDisplayType.equals("SP") ) 
			 { %>
			 <input type="radio" name="dispFormLink" value="S"  <%=makeDisabled%>><%=LC.L_Study%><br>
			 <input type="radio" name="dispFormLink" value="SP" Checked <%=makeDisabled%>><%=LC.L_Patient%>
		  <%}
} //formLinkedFrom=S
%>

			  
		
				 			 			 			 
	
	 </td> 
	 </tr>
	 <tr class="browserOddRow"><td><%-- Characteristic ***** --%><%=LC.L_Characteristic%></td>
 	 <td> 
	 <table>
	 <%						
	 if ( lfEntry.equals("M")) 
	  { %>         <tr> 
				<td><input type="radio" name="charName" value="M" Checked <%=makeDisabled%>><%-- Multiple entry ***** --%><%=LC.L_Multi_Entry%></td></tr>
				<!--  <input type="radio" name="charName" value="O" >Only once<br> -->
				<tr><td><input type="radio" name="charName" value="E" <%=makeDisabled%>><%-- Only once (Editable)***** --%><%=LC.L_Only_OnceEditable%></td></tr>			
                  <%}/* if ( lfEntry.equals("O")) 
				   //{ %>
				  <!-- <input type="radio" name="charName" value="M">Multiple entry<br>
				 <input type="radio" name="charName" value="O" Checked>Only once<br>
				 <input type="radio" name="charName" value="E">Only once (Editable) -->
				  <%/*}*/ if ( lfEntry.equals("E")) 
				   { %>
				   <tr>
				  <td><input type="radio" name="charName" value="M" <%=makeDisabled%>><%-- Multiple entry ***** --%><%=LC.L_Multi_Entry%></td></tr>
				 <!-- <input type="radio" name="charName" value="O" >Only once<br> -->
				 <tr><td><input type="radio" name="charName" value="E" Checked <%=makeDisabled%>><%-- Only once (Editable)***** --%><%=LC.L_Only_OnceEditable%></td>
				 </tr>
				 <% } %>	
				 <tr>
				 <%if (len>0){%>
				 <td><input Type="Checkbox" name="noshare" onclick="keepPrivate(document.selectForms)" checked><%-- Keep response private to author ***** --%><%=MC.M_KeepResp_PrivateToAuth%></td>
				 <input type="hidden" name="noshareId" value="1">
				  <%}else{%>
				 <td><input Type="Checkbox" name="noshare" onclick="keepPrivate(document.selectForms)"><%-- Keep response private to author ***** --%><%=MC.M_KeepResp_PrivateToAuth%></td>
				 <input type="hidden" name="noshareId" value="0">
				 <%}%>
				 </tr>
				 <tr><td colspan=2><i>(<%-- If 'Keep response private to author' option is selected, specify user/group with access to all responses ***** --%><%=MC.M_IfKeepRespOpt_SpcfyAces%>)</i></td></tr>
		<tr>
		<td><%-- User ***** --%><%=LC.L_User%>&nbsp;&nbsp; <input type="text" name="usrNames"  size = "50" value="<%=usrNames%>" readonly >
		<A href="#" onClick="getUser(document.selectForms)"><%-- Select ***** --%><%=LC.L_Select%></A>	
		<input type="hidden" name="usrIds" value="<%=usrIds%>">
		</td>							
		</tr>
		<tr>
		<td><%-- Group ***** --%><%=LC.L_Group%>&nbsp;<input type="text" name="grpNames"  size = "50" value="<%=grpNamesResp%>" readonly >
		<A href="#" onClick="getGroup(document.selectForms,'grpNames','grpIds' )"><%-- Select ***** --%><%=LC.L_Select%></A>	
		<input type="hidden" name="grpIds" value="<%=grpIdsResp%>">
		</td>							
		</tr>
		
			
			<tr></tr></table>
		</td>	
	 </tr>
	 <tr class="browserEvenRow"><td ><%-- Filters***** --%><%=LC.L_Filters%></td>
	 <td>
	 <table>
	 <tr>
	 <td width="15%"><%-- Organization***** --%><%=LC.L_Organization%> </td>
	 <td><input type="text" name="formOrg"  size = "50" value="<%=orgNames%>" readonly >
	  <A href="#" onClick="openOrgWindow(document.selectForms)"><%-- Select ***** --%><%=LC.L_Select%></A>	<!--km-->
	 <input type="hidden" name="formOrgIds" value=<%=formOrgIds%>>
	 <input type="hidden" name="oldFormOrgIds" value=<%=formOrgIds%>>
	 <input type="hidden" name="stdPageRight" value=<%=stdPageRight%>>
	 <input type="hidden" name="studyIdi" value=<%=studyIdi%>>
	
 	 <br>	 	
	 </td>							
	 </tr>
	 <tr>
	 <td><%-- Group ***** --%><%=LC.L_Group%></td>
	 <td> <input type="text" name="selGrpNames"  size = "50" value="<%=grpNames%>" readonly >
	 <A href="#" onClick="openGroupWindow(document.selectForms)"><%-- Select ***** --%><%=LC.L_Select%></A></td>	<!--km-->
	 <input type="hidden" name="selGrpIds" value="<%=grpIds%>">
	 <input type="hidden" name="oldGrpIds" value="<%=grpIds%>">
	 </td>							
	 </tr>
	 </table>
	 </td>
	 </tr>
	 <tr><td><br></td></tr>
 	 <tr></tr>
	<%//if(!statCodeId.equals(changeVal)){
	if ((formLinkedFrom.equals("S") && (pageRight==4 || pageRight==5)) || (formLinkedFrom.equals("A") && (pageRight==4 || pageRight==5))) {}
	else if(!status.equals("Deactivated")){%>
	<tr valign=baseline>
		<!-- Virendra: Fixed Bug no. 4688, added td with addAnotherWidth  -->
		<td bgcolor="<%=StringUtil.eSignBgcolor%>" valign="baseline" align="left" width="85%" <%=addAnotherWidth > 0 ? "" : "colspan=5" %>>
			<jsp:include page="submitBar.jsp" flush="true"> 
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="selFrmsId"/>
				<jsp:param name="showDiscard" value="N"/>
				<jsp:param name="noBR" value="Y"/>
			</jsp:include>
		</td>
	</tr>
	<%}%>
</table> 
</Form>

	 <%
		
	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right
	}
else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
</div>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>

</html>


			
