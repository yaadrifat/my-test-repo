<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="include.jsp" flush="true"/> 
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<HTML>
<HEAD>
<title><%=LC.L_Payment_Dets%><%--Payment Details*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
	<%-- Nicholas : Start --%>
	<%--<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>--%>
	<%-- Nicholas : End --%>
<SCRIPT LANGUAGE="JavaScript" src="calendar.js"></SCRIPT>


</HEAD>

<script>

function validate(formobj) {
	 
	if (!(validate_col('e-Signature',formobj.eSign))) return false;
    if(isNaN(formobj.eSign.value) == true) {
    	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		formobj.subVal.value="Submit";
		return false;
	   }

return true;
} 
</script>

<BODY>

<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"> </div> 
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
<jsp:useBean id="invB" scope="request" class="com.velos.eres.web.invoice.InvoiceJB"/>


<%
  HttpSession tSession = request.getSession(true); 
  String studyId = "";
  String paymentPk = "";
  String mode = "";
  String orderBy = "";
  String orderType = "";
  String cntr = "";

 if (sessionmaint.isValidSession(tSession))
	{
%>
	<%-- Nicholas : Start --%>
	<jsp:include page="skinChoser.jsp" flush="true"/>
	<%-- Nicholas : End --%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%

		studyId = request.getParameter("studyId");
		String  pageRight = request.getParameter("pR");
		String src;
		src= request.getParameter("srcmenu");
		paymentPk = request.getParameter("paymentPk");
		orderBy = request.getParameter("orderBy");
		orderBy = (orderBy==null)?"":orderBy;
		orderType = request.getParameter("orderType");
		orderType = (orderType==null)?"":orderType;
		cntr = request.getParameter("page");
		cntr = (cntr==null)?"":cntr;

		


	String acc = (String) tSession.getAttribute("accountId");
	int accId = 0;

	
		accId = EJBUtil.stringToNum(acc);	
%>
  <jsp:include page="paymenttabs.jsp" flush="true">
	<jsp:param name="studyId" value="<%=studyId%>"/>
	<jsp:param name="selectedTab" value="1"/>
	<jsp:param name="paymentPk" value="<%=paymentPk%>"/>
	<jsp:param name="pR" value="<%=pageRight%>"/>
  </jsp:include>
<div id="containerDiv" style="overflow:auto; height:95%;"> 
<p class="sectionHeadings"> <%=LC.L_Select_AnInv%><%--Select an Invoice*****--%></p>



	  <jsp:include page="invbrowsercommon.jsp" flush="true">
		<jsp:param name="studyId" value="<%=studyId%>"/>
		<jsp:param name="pageRight" value="<%=pageRight%>"/>
		<jsp:param name="paymentPk" value="<%=paymentPk%>"/>
		<jsp:param name="orderBy" value="<%=orderBy%>"/>
		<jsp:param name="orderType" value="<%=orderType%>"/>
		<jsp:param name="page" value="<%=cntr%>"/>
		<jsp:param name="calledFrom" value="p"/>
	  </jsp:include>
	  
		
<br><br>


<%
	}
 else{%>
 <jsp:include page="timeout_childwindow.jsp" flush="true"/> 
 <%}%>

 <jsp:include page="bottompanel.jsp" flush="true"/> 
</DIV>
</BODY>
</HTML>