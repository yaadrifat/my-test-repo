<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Milestone_Open%><%--Milestone >> Open*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<%
String milestoneParams = StringUtil.htmlDecode(request.getParameter("milestoneParams"));
if (!StringUtil.isEmpty(milestoneParams) && !"null".equals(milestoneParams)) {
    milestoneParams = milestoneParams.replaceAll("&quot;","");
%>
    <script>
    if (document.milestone) {
        document.milestone.action = "milestone.jsp?"+"<%=milestoneParams%>";
        document.milestone.submit();
    }
    </script>
<%
}

String src;
src= request.getParameter("srcmenu");

String includeMenus="";

 includeMenus= request.getParameter("includeMenus");

 if (StringUtil.isEmpty(includeMenus))
 {
 	includeMenus="Y";
 }

 String bottomdivClass = "BrowserBotN BrowserBotN_M_2";


%>

<% if (includeMenus.equals("Y")) { %>
	<jsp:include page="panel.jsp" flush="true">
	<jsp:param name="src" value="<%=src%>"/>
	</jsp:include>

<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0">
<!-- Bug NO: 3976 fixed by PK -->
 <div id="overDiv" style="position:absolute; visibility:collapse; z-index:1000;"> </div>
 <script language="JavaScript" src="overlib.js"> <!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

	<% } else
		{

			bottomdivClass="popDefault";
			%>
				<body>
				<!-- Bug NO: 3976 fixed by PK -->
 <div id="overDiv" style="position:absolute; visibility:collapse; z-index:1000;"> </div>
 <script language="JavaScript" src="overlib.js"> <!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
				<%
		}
%>



<jsp:useBean id="milestoneB" scope="request" class="com.velos.eres.web.milestone.MilestoneJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="stdRightsB" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />

<%@ page language = "java" import = "com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.common.MilestoneDao,com.velos.eres.business.common.TeamDao,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*,com.velos.esch.business.common.*, com.velos.eres.business.common.*"%>

<%
 String selectedTab = request.getParameter("selectedTab");
 String study = request.getParameter("studyId");
//Added for INF-22500 by Yogendra Pratap Singh 24/01/13
 String mileFrom =(request.getParameter("mileFrom")==null) ? "" : (request.getParameter("mileFrom"));
if( mileFrom.equalsIgnoreCase("manageMilestone") ){
	mileFrom="manageMilestone";
}else{
	mileFrom="milestoneSearch";
}
%>
	<DIV class="BrowserTopn" id="div1">
<% if (includeMenus.equals("Y")) { %>

	  <jsp:include page="milestonetabs.jsp" flush="true">
		<jsp:param name="studyId" value="<%=study%>"/>
		<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
	  </jsp:include>

	<% } %>

		</div>
<%--Added for INF-22500 by Yogendra Pratap Singh 24/01/13 --%>
<jsp:include page="viewMilestones.jsp" flush="true">
	<jsp:param name="mileFrom" value="<%=mileFrom%>"/>
</jsp:include>

<% if (includeMenus.equals("Y")) { %>
	<div class ="mainMenu" id="emenu">
	  	<jsp:include page="getmenu.jsp" flush="true"/>
	</div>
<%}%>
</body>

</html>

