<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>



<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_User_Search%><%--User Search*****--%></title>

<SCRIPT Language="Javascript1.2">


function setOrder(formobj,orderBy) 
{
	
//	if (formObj.orderType.value == null) alert("OK");
	
	if (formobj.orderType.value=="asc") {
		formobj.orderType.value= "desc";		
	} else 	if (formobj.orderType.value=="desc") {
		formobj.orderType.value= "asc";
	}
	
	formobj.orderBy.value= orderBy;
	
	formobj.submit(); 
}



	

function back(frmName) {


  
  var totalRows = 0;

	totalRows = parseInt(frmName.totalRows.value);




	var names = "";

	var ids = "";

	if(totalRows > 1) {

  

		for (var i = 0; i < totalRows; i++) {



			if(frmName.usercheck[i].checked){

  

				names = names + frmName.username[i].value + ";";

				ids = ids + frmName.userid[i].value + ",";

				

			}		

		}

	

	}

	if(totalRows==1) {



  		if(frmName.usercheck.checked){  	

			names = names + frmName.username.value + ";";

			ids = ids + frmName.userid.value + ",";		

		

		}		



	}





	

	names = names.substring(0,names.length-1);
	ids = ids.substring(0,ids.length-1);
	
	 
	
	dispFld=frmName.dispFld.value;
	dataFld=frmName.dataFld.value;
	from=frmName.from.value;
	if ((dispFld.length>0) || (dataFld.length>0))
	{
	window.opener.document.forms[from].elements[dataFld].value = ids;
	window.opener.document.forms[from].elements[dispFld].value = names;
	} else {

	if (document.layers) {

	   

		if(frmName.from.value == "crfstatus") {

			window.opener.document.div1.document.crf.crfSentToId.value = ids;

			window.opener.document.div1.document.crf.crfSentToName.value = names;

		} else if(frmName.from.value == "crfnotify") {

			window.opener.document.div1.document.crfNotify.crfNotifyToId.value = ids;

			window.opener.document.div1.document.crfNotify.crfNotifyToName.value = names;

		} else if(frmName.from.value == "alertnotify") {

			var counter=frmName.rownum.value;	

			window.opener.document.div1.document.alertnotify.enteredByName[counter].value = names;

			window.opener.document.div1.document.alertnotify.enteredByIds[counter].value = ids ;

		} else if(frmName.from.value == "notification") {

			window.opener.document.div1.document.adverse.alertNotifyToId.value = ids;

			window.opener.document.div1.document.adverse.alertNotifyToNames.value = names;

		}
		
		//JM: 17August2006
		else if(frmName.from.value == "studynotifyform") {
		
			var counter=frmName.rownum.value;	

			window.opener.document.div1.document.studynotifyform.userNames[counter].value = names;

			window.opener.document.div1.document.studynotifyform.userIds[counter].value = ids ;
			
			//
			if(counter==1) 
			window.opener.document.div1.document.studynotifyform.userIds1[counter].value = ids ;

		}


	} else {

   

		if(frmName.from.value == "crfstatus") {

			window.opener.document.crf.crfSentToId.value = ids;

			window.opener.document.crf.crfSentToName.value = names;

		}else if(frmName.from.value == "crfnotify") {

			window.opener.document.crfNotify.crfNotifyToId.value = ids;

			window.opener.document.crfNotify.crfNotifyToName.value = names;

		}else if(frmName.from.value == "alertnotify") {

			var counter=frmName.rownum.value;	

			window.opener.document.alertnotify.enteredByName[counter].value = names;

			window.opener.document.alertnotify.enteredByIds[counter].value = ids;

		} else if(frmName.from.value == "notification") {

			window.opener.document.adverse.alertNotifyToId.value = ids;

			window.opener.document.adverse.alertNotifyToNames.value = names;				

		}
		//JM: 17August2006
		else if(frmName.from.value == "studynotifyform") {
		
			var counter=frmName.rownum.value;	

			window.opener.document.studynotifyform.userNames[counter].value = names;

			window.opener.document.studynotifyform.userIds[counter].value = ids ;
			
			//
			if(counter==1) 
			window.opener.document.studynotifyform.userIds1.value = ids ;

		}
	}	


}
		self.close();


}





</SCRIPT>

<SCRIPT type="text/javascript">
//JM: 25/08/05 
function checkall(formObj){
var len = 0;
var seluser=0;
len=formObj.rowsReturned.value;
seluser=formObj.selectedUsers.value;

if (len==1 && seluser==0){
	if (formObj.usercheckall.checked==true){
		formObj.usercheck.checked=true;
	}
	else if (formObj.usercheckall.checked==false){
		formObj.usercheck.checked=false;
	}
}

if (len==1 && seluser>0){
	if (formObj.usercheckall.checked==true){
		formObj.usercheck[0].checked=true;
	}
	else if (formObj.usercheckall.checked==false){
		formObj.usercheck[0].checked=false;
	}
}
else if (len>1){
	for (i=0; i<len; i++){
		if (formObj.usercheckall.checked==true){
			formObj.usercheck[i].checked=true;
		}
		else if (formObj.usercheckall.checked==false){
			formObj.usercheck[i].checked=false;
		}
	}
 }
}
</SCRIPT>






<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>





<% response.setHeader("Cache-Control", "max-age=1, must-revalidate");%>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



</head>
<%-- Nicholas : Start --%>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%-- Nicholas : End --%>


<%

int ienet = 2;



String agent1 = request.getHeader("USER-AGENT");

   if (agent1 != null && agent1.indexOf("MSIE") != -1) 

     ienet = 0; //IE

    else

	ienet = 1;

	if(ienet == 0) {	

%>

<body style="overflow:scroll;">

<%

	} else {

%>

<body>

<%

	}

%>

<%HttpSession tSession = request.getSession(true);
	String mode = request.getParameter("mode");

  if (sessionmaint.isValidSession(tSession))

{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/> 
	<jsp:include page="include.jsp" flush="true"/>

	<%
	String from=request.getParameter("from") ;
	String dispFld=request.getParameter("dispFld");
	dispFld=(dispFld==null)?"":dispFld;
	String dataFld=request.getParameter("dataFld");
	dataFld=(dataFld==null)?"":dataFld;
	
	String  orgName="";
	orgName=request.getParameter("accsites") ;
	
	
	//JM:
	String gName = "";
	gName = request.getParameter("usrgrps");
	gName=(gName==null)?"":gName;
	
	String stTeam = "";
	stTeam = request.getParameter("ByStudy");
	stTeam=(stTeam==null)?"":stTeam;
	
		
	
	
	
	String names = "";
	String ids = "";
	names = request.getParameter("names");
	ids = request.getParameter("ids");
	
	
	String rownum=request.getParameter("rownum");
	String accountId = (String) tSession.getValue("accountId");
	
		String userJobType = "";
		String dJobType = "";
		String dAccSites ="";
		String jobVal = request.getParameter("jobType");
		if(jobVal==null)
			jobVal = "";
		String siteVal = request.getParameter("accsites");
		if(siteVal == null)	siteVal = "";
		

		CodeDao cd = new CodeDao();
		CodeDao cd2 = new CodeDao();
		cd2.getAccountSites(EJBUtil.stringToNum(accountId)); 
    	cd.getCodeValues("job_type");
		userJobType =userB.getUserCodelstJobtype();
		
		UserDao userDao = new UserDao();


		if (jobVal.equals(""))			
		  dJobType=cd.toPullDown("jobType");
		 
		else
		  dJobType=cd.toPullDown("jobType",EJBUtil.stringToNum(jobVal),true);

		
		String userSiteId = "";
		int primSiteId = 0;

		userB=(com.velos.eres.web.user.UserJB)tSession.getAttribute("currentUser");
		userSiteId = userB.getUserSiteId();
		userSiteId=(userSiteId==null)?"":userSiteId;
		primSiteId= EJBUtil.stringToNum(userSiteId);

		if ((siteVal.length()==0) && (mode.equals("initial"))){
		   dAccSites=cd2.toPullDown("accsites",primSiteId,true);	
		   orgName=userSiteId;
		   mode="M";
		}	
		else
		  dAccSites=cd2.toPullDown("accsites",EJBUtil.stringToNum(siteVal),true);


	while(names.indexOf('~') != -1){

	  	names = names.replace('~',' ');

	}
	String fname = LC.L_All/*"All"*****/;
	String lname = LC.L_All/*"All"*****/;
	String jobtype = LC.L_All/*"All"*****/;
	String accsites = LC.L_All/*"All"*****/;
	
	String usrgrps  = LC.L_All/*"All"*****/;
	String ByStudy = LC.L_All/*"All"*****/;

	

	fname = StringUtil.htmlEncodeXss(request.getParameter("fname"));
	
	fname=(fname==null)?LC.L_All/*"All"*****/:fname;
	
	lname = StringUtil.htmlEncodeXss(request.getParameter("lname"));
	
	
	lname=(lname==null)?LC.L_All/*"All"*****/:lname;
	jobtype = request.getParameter("jobType");

	accsites =request.getParameter("accsites");

	String jobDesc = LC.L_All/*"All"*****/;
	String siteDesc = LC.L_All/*"All"*****/;
	String gDesc = LC.L_All/*"All"*****/;
	String tDesc = LC.L_All/*"All"*****/;
	
	
	//jobDesc = cd.getCodeDescription(EJBUtil.stringToNum(jobtype));
	jobDesc = cd.getCodeDesc(EJBUtil.stringToNum(jobtype));

	//siteDesc = cd.getCodeDescription(EJBUtil.stringToNum(accsites));

		if (accsites==null) 
		siteDesc = cd2.getCodeDesc(primSiteId);	
		else{		
		siteDesc = cd2.getCodeDesc(EJBUtil.stringToNum(accsites));
		}
		
		
//JM: 23/08/05 to make drop down of group
	    String dUsrGrps="";
	    String selGroup="";
	    int ac=EJBUtil.stringToNum(accountId);	
	    CodeDao cd1 = new CodeDao();    
	    cd1.getAccountGroups(ac); 
	    
	    selGroup=request.getParameter("usrgrps");
	    if (selGroup==null)  selGroup="";
	    if (selGroup.equals(""))	    
	    dUsrGrps = cd1.toPullDown("usrgrps");
	    else
	    dUsrGrps = cd1.toPullDown("usrgrps",EJBUtil.stringToNum(selGroup),true);
	    
	    usrgrps =request.getParameter("usrgrps");
	    gDesc=cd1.getCodeDesc(EJBUtil.stringToNum(usrgrps));
			
			
	    	    
	    
	    
//JM: 24/08/05	    
	 String dStudy="";
	 String selstudy="";
	 int selstudyId=0;
  	 selstudy=request.getParameter("ByStudy") ;
		if(selstudy==null ){
		selstudyId=0 ;
		}else { 
		selstudyId = EJBUtil.stringToNum(selstudy);
		}
		
	StudyDao studyDao = new StudyDao();
	String suserId = (String) tSession.getValue("userId");
	studyDao.getStudyValuesForUsers(suserId);
	ArrayList studyIds = studyDao.getStudyIds();
	ArrayList studyNumbers = studyDao.getStudyNumbers();
	
	//make dropdowns of study
	dStudy=EJBUtil.createPullDown("ByStudy",selstudyId,studyIds,studyNumbers);
		
	ByStudy =request.getParameter("ByStudy");
	//JM: get the study number from the selected study id
	tDesc=studyDao.getStudy(EJBUtil.stringToNum(ByStudy));
	
%>
<P class="sectionheadings"><%=MC.M_UsrSearch_Criteria%><%--User Search >> Search Criteria*****--%></P>
<DIV class="popDefault">

<Form  name="search1" method="POST" action="multipleusersearchdetails.jsp">

  <input type="hidden" name=from value="<%=from%>">
  <input type="hidden" name=names value="<%=names%>">
  <input type="hidden" name=ids value="<%=ids%>">	
  <Input type="hidden" name="selectedTab" value="1">
  <Input type="hidden" name=rownum value="<%=rownum%>">
  <Input type="hidden" name="mode" value="<%=mode%>">


  


  <input name="dispFld" type="hidden" value="<%=dispFld%>">
  <input name="dataFld" type="hidden" value="<%=dataFld%>">

    
 
	
  <table width="600" cellspacing="0" cellpadding="0" border="0" class="basetbl">
    <tr>
       <td width=15%> <%=LC.L_User_FirstName%><%--User First Name*****--%>:<br/>
		  <%if(fname.equals("All")){%>
		   <Input type=text name="fname" size=20>
		  <%}else{%>
		  <!--km -to fix Bug 2308 -->
        <Input type=text name="fname" size=20 value="<%=fname%>">
		  <%}%>
      </td>
      <td width=15%> <%=LC.L_User_Last_Name%><%--User Last Name*****--%>:<br/>
		   <%if(lname.equals("All")){%>
		  <Input type=text name="lname" size=20>
		  <%}else{%>
        <Input type=text name="lname" size=20 value="<%=lname%>">
		   <%}%>
      </td>
	  <td width=20%> <%=LC.L_Job_Type%><%--Job Type*****--%>:<br/>
		 <%=dJobType%> 
	  </td>
	

	  <td width=20%> <%=LC.L_Organization_Name%><%--Organization Name*****--%>:<br/> 
		 <%=dAccSites%> 
	  </td>
	  

      <!--td class=tdDefault> 

     <!--   <Input type="submit" name="submit" value="Go"> -->

	 <!--input type="image" src="../images/jpg/go_white.gif" align="bottom" border="0">

      </td-->
      <tr>
	    <td  width=20%> <%=LC.L_Group_Name%><%--Group Name*****--%>:<br/><%=dUsrGrps %> </td>
	    <td width=20%> <%=LC.L_Std_Team%><%--<%=LC.Std_Study%> Team*****--%>:<br/><%=dStudy%> </td>
	    <td >
		     <!--   <Input type="submit" name="submit" value="Go"> -->
			 <button type="submit"><%=LC.L_Search%></button>
	    </td>
    </tr>  

    

  </table>

</Form>


<%if(fname==null||fname.equals(""))
		fname = LC.L_All/*"All"*****/;
	if(lname==null||lname.equals(""))
		lname = LC.L_All/*"All"*****/;
	/*if(jobDesc==null)
		jobDesc = "All";
	if(siteDesc==null)
		siteDesc = "All";*/
	
	if(jobDesc==null || jobDesc.equals(""))
		jobDesc = LC.L_All/*"All"*****/;
	if(siteDesc==null || siteDesc.equals(""))
		siteDesc = LC.L_All/*"All"*****/;
	if(gDesc==null || gDesc.equals(""))
		gDesc = LC.L_All/*"All"*****/;
	if(tDesc==null || tDesc.equals(""))
		tDesc = LC.L_All/*"All"*****/;
				
		;%>
<P class="defComments"><%=MC.M_SelFilters_UsrFirstName%><%--The Selected Filters are: User First Name*****--%>: <B><I><u><%=fname%></u></I></B>&nbsp&nbsp <%=LC.L_User_Last_Name%><%--User Last Name*****--%>: <B><I><u><%=lname%></u></I></B>&nbsp&nbsp <%=LC.L_Job_Type%><%--Job Type*****--%>: <B><I><u><%=jobDesc%></u></I></B>&nbsp&nbsp <%=LC.L_Organization_Name%><%--Organization Name*****--%>: <B><I><u><%=siteDesc%></u></I></B>&nbsp&nbsp <%=LC.L_Group_Name%><%--Group Name*****--%>:<B><I><u><%=gDesc%></u></I></B>&nbsp&nbsp <%=LC.L_Std_Team%><%--<%=LC.Std_Study%> Team*****--%>:<B><I><u><%=tDesc%></u></I></B></p>


  <%
	String uName = (String) tSession.getValue("userName");
	int pageRight = 0;
	String accId = (String) tSession.getValue("accountId");
	
	String ufname = StringUtil.htmlEncodeXss(request.getParameter("fname")) ;
	
	ufname=(ufname==null)?"":ufname;
	
	String ulname=StringUtil.htmlEncodeXss(request.getParameter("lname")) ;
	
	ulname=(ulname==null)?"":ulname;
	
	String	jobTyp=request.getParameter("jobType") ;
		

	

	
	
	int iaccid = EJBUtil.stringToNum(accId);
	if(mode == null) mode = "";

	int i;

    String usrId = null;	

	String usrName= "";		

	

	

//	if ((ufname.equals("")) && (ulname.equals(""))){

	

		//UserDao userDao = new UserDao();
		//userDao.searchAccountUsers(EJBUtil.stringToNum(accId),ulname,ufname,jobTyp,orgName,ids);
	//	out.print(EJBUtil.stringToNum(accId) +ulname +ufname);
	    ArrayList usrLastNames;
      	ArrayList usrFirstNames=null;
	    ArrayList usrMidNames=null;
		ArrayList userSiteNames = null;
      	ArrayList siteNames=null;
	    ArrayList jobTypes=null;
      	ArrayList usrIds=null;	
	    ArrayList grpNames=null;	
      	String usrLastName = null;
	    String grpName = null;
		String usrFirstName = null;
	    String usrMidName = null;
      	String siteName = null;
	    String jobType = null;
		int counter = 0;
		String oldGrp = null;
		///FOR HANDLING PAGINATION
		int iaccId = EJBUtil.stringToNum(accId); 
		
		String pagenum = "";
		int curPage = 0;
		long startPage = 1;
		String stPage;
		long cntr = 0;
		pagenum = request.getParameter("page");
		
		
		if (pagenum == null)
		{
			pagenum = "1";
		}
		curPage = EJBUtil.stringToNum(pagenum);
		String orderBy = "";
		orderBy = request.getParameter("orderBy");
		
		if (EJBUtil.isEmpty(orderBy))
			orderBy = "lower(USR_LASTNAME)"; 		
		String orderType = "";
		orderType = request.getParameter("orderType");
		
			if (EJBUtil.isEmpty(orderType))
			{
			orderType = "asc";}
		String str2 ="";
		
		String str1 ="";
		String count1="";
		String count2="";
		String countSql = "";
		String formSql = "";	


		String lWhere = null;
 	 	String fWhere = null;
		String oWhere = null;
		String rWhere = null;
		String aWhere = null;
		
		String gWhere = null;
		String sWhere = null;
		
		String searchFNameForSQL= StringUtil.escapeSpecialCharSQL(ufname);
	    
		String searchLNameForSQL= StringUtil.escapeSpecialCharSQL(ulname);
	    
	    
		
 	 	StringBuffer completeSelect = new StringBuffer();
	/*	if ((orgName==null) && (mode.equals("initial")))
			orgName=userSiteId;

		out.println("orgName="+orgName);*/


/*
		str1 = "SELECT ER_USER.pk_user,ER_USER.USR_LASTNAME, " 
		       + "(select SITE_NAME from er_site where PK_SITE  = FK_SITEID) as site_name, " 
 			   + "ER_USER.USR_FIRSTNAME, ER_USER.USR_MIDNAME " 
 		       + " from ER_USER " 
 			   + " Where USR_STAT = 'A' and USR_TYPE <> 'X' and ER_USER.fk_account = "+iaccId ;	
*/
//JM: 11/10/2005 Modified: get the blocked user too 		
//KM: Modified for Hidden User.
	str1 = "SELECT distinct u.pk_user,u.USR_LASTNAME, " 
		+ " (select SITE_NAME from er_site where PK_SITE  = FK_SITEID) as site_name, " 
 		+ " u.USR_FIRSTNAME, u.USR_MIDNAME, "
	    + " u.USR_STAT, u.USR_TYPE "
	    + " from ER_USER u   "
 		+ " Where u.USR_STAT in ('A','B') and u.USR_TYPE <> 'X' and u.USR_TYPE <> 'P' and u.USER_HIDDEN <> 1 and u.fk_account = "+iaccId ;
			 
			 
			 
			 if ((orgName!= null) && (! orgName.trim().equals("") && ! orgName.trim().equals("null")))
		{
			oWhere = " and UPPER(u.FK_SITEID) = UPPER(" ;
			oWhere+=orgName;
			oWhere+=")";
		}



		if ((jobTyp != null) && (! jobTyp.trim().equals("") && ! jobTyp.trim().equals("null")))
		{
			rWhere = " and UPPER(u.FK_CODELST_JOBTYPE) = UPPER(" ;
			rWhere+=jobTyp;
			rWhere+=")";
		}		
		
		
		
		if ((ufname != null) && (! ufname.trim().equals("") && ! ufname.trim().equals("null")))
 		{
 			fWhere = " and UPPER(u.USR_FIRSTNAME) like UPPER('" ;
 			fWhere+=searchFNameForSQL.trim();
 			fWhere+="%')";
 		}
 		
 		if ((ulname != null) && (! ulname.trim().equals("")))
 		{
 			lWhere = " and UPPER(u.USR_LASTNAME) like UPPER('";
 			lWhere+=searchLNameForSQL.trim();
 			lWhere+="%')";
 		}
		
		//JM: 24/08/05 
		//km-All the User's belonging to the selected group should be retreived, default or not doesn't matter. (Bug:2306)	
		
		if ((gName != null) && (!gName.trim().equals("") && !gName.trim().equals("null")))
		{			
			//gWhere =" and UPPER(u.fk_grp_default) = upper(";
			gWhere=" and u.pk_user in(select fk_user from er_usrgrp where upper(fk_grp)=upper(";
			gWhere +=gName;
			gWhere +="))";
		}
		
		if ((stTeam != null) && (!stTeam.trim().equals("") && !stTeam.trim().equals("null")))
		{			
			 	sWhere =" and ( exists (select * from er_studyteam t where t.fk_user = pk_user and t.fk_study =  ";
				sWhere +=stTeam;
				sWhere +=" and nvl(t.study_team_usr_type,'D')='D' ) or pkg_superuser.F_Is_Superuser(pk_user, "+stTeam+") = 1  )";
			
			 
		}	
		
 		
 		completeSelect.append(str1);
		
 		if((ids != null) && (! ids.trim().equals("")  && ! ulname.trim().equals("null")))
 			completeSelect.append(" and u.pk_user not in (" + ids + ")" );
		
 		
 		if (fWhere != null)
 		{
 			completeSelect.append(fWhere);
 		}
 		
 		if (lWhere != null) 
 		{
 			completeSelect.append(lWhere);
 		}
 		
		
  	 	if (oWhere != null) 
 	 	{
 	 		completeSelect.append(oWhere);
 	 	}

 	 	if (rWhere != null) 
 	 	{
 	 		completeSelect.append(rWhere);
 	 	}
		
		//JM: 24/08/05
			
		if (gWhere != null) 
		{
			completeSelect.append(gWhere);
		}
		
		
		if (sWhere != null) 
		{
			completeSelect.append(sWhere);
		}			
		
				
 			// add order by

			str2 = completeSelect.toString();
			//completeSelect.append(" ORDER BY lower(ER_USER.USR_FIRSTNAME || ' ' || ER_USER.usr_lastname) ASC ");
			//chnaged by Jnanamay Majumdar
			//completeSelect.append(" ORDER BY lower( ER_USER.usr_lastname || ' ' || ER_USER.USR_FIRSTNAME ) ASC ");
			
			formSql = completeSelect.toString();
			count1 = "select count(*) from  ( " ;
			count2 = ")"  ;
			
		countSql = count1 + str2 + count2 ; 		
		
		long rowsPerPage=0;
		long totalPages=0;	
		long rowsReturned = 0;
		long showPages = 0;
			
	   boolean hasMore = false;
	   boolean hasPrevious = false;
	   long firstRec = 0;
	   long lastRec = 0;	   
	   long totalRows = 0;	
	   
	   String type ="" ;	
	   String type1="";	   	   
	   
		rowsPerPage =  Configuration.MOREBROWSERROWS ;
		totalPages =Configuration.PAGEPERBROWSER ; 
  


       BrowserRows br = new BrowserRows();
       br.getPageRows(curPage,rowsPerPage,formSql ,totalPages,countSql,orderBy,orderType);
   	   rowsReturned = br.getRowReturned();
	   showPages = br.getShowPages();
	   startPage = br.getStartPage();
	   hasMore = br.getHasMore();
	   hasPrevious = br.getHasPrevious();	 
	   totalRows = br.getTotalRows();	   
	   firstRec = br.getFirstRec();
	   lastRec = br.getLastRec();
	
%>



  <br>



<%// <Form name="accountBrowser" onsubmit="back(document.accountBrowser);">
%>

  <Form name="accountBrowser" id="acctBrowser" method="post" action="multipleusersearchdetails.jsp" onsubmit="if (back(document.accountBrowser)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
  
  <Input type="hidden" name="orderType" value="<%=orderType%>">
  <Input type="hidden" name="orderBy" value="<%=orderBy%>">
   <Input type="hidden" name="page" value="<%=pagenum%>">


  <input type="hidden" name=from value="<%=from%>">
  <input type="hidden" name=names value="<%=names%>">
  <input type="hidden" name=ids value="<%=ids%>">	
  <Input type="hidden" name=rownum value=<%=rownum%>>
  <input name="dispFld" type="hidden" value="<%=dispFld%>">
  <input name="dataFld" type="hidden" value="<%=dataFld%>">
  <input type="hidden" name="fname" value="<%=ufname%>">
  <Input type="hidden" name="lname" value="<%=ulname%>">
  <Input type="hidden" name="mode" value="M">
	<input type="hidden" name="jobType" value=<%=jobtype%>>
	<input type="hidden" name="accsites" value=<%=orgName%>>
	
	<input type="hidden" name="usrgrps" value=<%=gName%>>
	<input type="hidden" name="ByStudy" value=<%=stTeam%>>
	<input type="hidden" name="rowsReturned" value=<%=rowsReturned%>>  
  	


<%
	StringTokenizer namesST = new StringTokenizer(names,";");
	StringTokenizer idsST = new StringTokenizer(ids,",");
	int selectedUsers = 0;
	selectedUsers = idsST.countTokens();	
%> 	
	<input type="hidden" name="selectedUsers" value="<%=selectedUsers%>">
 

  
<%-- Nicholas : Start --%>
    <table class="comPopup outline midAlign" width="100%" border="0">

      <tr>  



	  <% if (rowsReturned > 0){
 %>

	  	<th width=15% align="center">

				<!--B>Search Result</B-->
				<INPUT TYPE="checkbox" NAME="usercheckall" onClick="checkall(document.accountBrowser);">
				&nbsp;&nbsp;
				<%=LC.L_Select_All%><%--Select All*****--%></th>

				<th width=25% onClick="setOrder(document.accountBrowser,'lower(USR_FIRSTNAME)')" align="center"><%=LC.L_First_Name%><%--First Name*****--%>
				</th>
				<th width=25%
				onClick="setOrder(document.accountBrowser,'lower(USR_LASTNAME)')"  align="center"><%=LC.L_Last_Name%><%--Last Name*****--%>
				</th>
				<th onClick="setOrder(document.accountBrowser,'lower(site_name)')"  align="center"><%=LC.L_Organization%><%--Organization*****--%>
				</th>
				<th align="center"><%=LC.L_User_Type%><%--User Type*****--%>
				</th>
<%-- Nicholas : End --%>

	  


	<%}else{%>

	        <td width=35% align =center><P class=defcomments> <%=MC.M_NoSrchResMatch_Criteria%><%--No search results matching given criteria found*****--%></P></td>

	<%}%>

      </tr>



      <%int count = 0;



		for(count  = 1 ; count  <= rowsReturned ; count++)

	  	{
			
			usrLastName=br.getBValues(count,"USR_LASTNAME");
			
			usrFirstName=br.getBValues(count,"USR_FIRSTNAME");
			
			usrId = br.getBValues(count,"pk_user");			
			
			String orgNam =  br.getBValues(count,"site_name");	
			if (orgNam == null){
				orgNam ="-";
				}
			//JM: 25/08/05
			type =br.getBValues(count,"usr_stat"); 
			type1 =br.getBValues(count,"usr_type");
			if (type.equals("A")){
			type=LC.L_ActAcc_Users/*"Active Account User"*****/;
			}
			else if (type.equals("B")){
			type=LC.L_Blocked_User/*"Blocked User"*****/;
			}
			else if (type.equals("D")){
			type=LC.L_Deactivated_User/*"Deactivated User"*****/;
			}
					
			type1 =  br.getBValues(count,"usr_type");
			if (type1.equals("N")){
			type=MC.M_Non_SystemUser/*"Non System User"*****/;
			}
							
					
				if ((count%2)==0) {

				%>
	
			  <tr class="browserEvenRow"> 

				<%

					}else{

				  %>

			  <tr class="browserOddRow"> 
				<% } 
			  usrName = usrFirstName + " " +usrLastName; 
			  
		   %>

		<td><input type=checkbox name='usercheck' >		
        <td width =200> <%=usrFirstName%></td>
		<td width =200> <%=usrLastName%> </td>
		<td width =200> <%=orgNam%> </td>
		<td width=200> <%= type%></td>
		<input type=hidden name='username' value="<%=usrFirstName.trim()%> <%=usrLastName.trim()%>">		
		<input type=hidden name='userid' value=<%=usrId%>>
      </tr>

      <%
		
	  	}

	    rowsReturned = rowsReturned + selectedUsers;
	    
	    
  	
	

	  %>



	  <input type=hidden name=totalRows value=<%=rowsReturned%>>

	  <tr>

<%

		if(selectedUsers > 0) {
	
%>	
 

	  	<td >

			<p class = "sectionHeadings" ><%=MC.M_Already_SelectedUsers%><%--Already Selected Users*****--%></p>

		</td>
			  	<td >

			<p class = "sectionHeadings" ><%=LC.L_First_Name%><%--First Name*****--%></p>

		</td>
			  	<td >

			<p class = "sectionHeadings" ><%=LC.L_Last_Name%><%--Last Name*****--%></p>

		</td>
			  	<td >

			<p class = "sectionHeadings" ><%=LC.L_Organization%><%--Organization*****--%></p>
		</td>
		<td >

			<p class = "sectionHeadings" ><%=LC.L_User_Type%><%--User Type*****--%></p>
		</td>

<%

		}

%>		



</tr>

	  <%

		for(i = 0 ; i < selectedUsers ; i++)

	  	{

			//usrName = namesST.nextToken();
			/*added by sonia,02/01/07 since we have all the values, recreat ethe names here. previously we were getting it from previous page 
and in that case it would always get truncated if the name had spl char & */

			usrId = idsST.nextToken();
			
			
			userDao = userB.getUsersDetails(usrId);
			ArrayList organizationNames = userDao.getUsrSiteNames();
			ArrayList uFname = userDao.getUsrFirstNames();
			ArrayList uLname = userDao.getUsrLastNames();
			
			
			ArrayList uStat = userDao.getUsrStats();//JM
			ArrayList uType = userDao.getUsrTypes();//jm
			
			
			
			
			String orgNam = ((organizationNames.get(0)) ==null)?"-":(organizationNames.get(0)).toString();
			String usrFname = ((uFname.get(0)) == null) ? "-":(uFname.get(0)).toString();
			String usrLname = ((uLname.get(0)) == null) ? "-":(uLname.get(0)).toString();
			
			 usrName = usrFname + " " +usrLname;   
			String usrStat = ((uStat.get(0))==null)? "-":(uStat.get(0)).toString();  
		
			if (usrStat.equals("A")){
			usrStat=LC.L_ActAcc_Users/*"Active Account User"*****/;
			}
			else if (usrStat.equals("B")){
			usrStat=LC.L_Blocked_User/*"Blocked User"*****/;
			}
			else if (usrStat.equals("D")){
			usrStat=LC.L_Deactivated_User/*"Deactivated User"*****/;
			}
			String usrType = ((uType.get(0))==null)? "-":(uType.get(0)).toString();
			
			if (usrType.equals("N")){
			usrStat=MC.M_Non_SystemUser/*"Non System User"*****/;
			}
			
						
			if ((i%2)==0) {

	%>

      <tr class="browserEvenRow"> 

        <%

			}else{

		  %>

      <tr class="browserOddRow"> 

        <% }  

		   %>

		<td><input type=checkbox name='usercheck' checked>

        	<td width =200> <%=usrFname%></td>
		<td width =200> <%=usrLname%> </td>
		<td width =200> <%=orgNam%> </td>
		<td width=200> <%=usrStat%></td>
		 
		 
		<input type=hidden name='username' value="<%=usrName%>">

		<input type=hidden name='userid' value=<%=usrId%>>

      </tr>

<%

		}

%>



	  <tr>

	  	<td colspan=2>

	
	<%
    if (sessionmaint.isValidSession(tSession))
	{

   		 i = 2;

		 agent1 = request.getHeader("USER-AGENT");
	
	if((agent1.indexOf("MSIE") != -1)  && 	(agent1.indexOf("Mac") != -1)){

          %>
	
		  <A onclick="back(document.accountBrowser);" href="#"> <img src="../images/jpg/Submit.gif" align="absmiddle" border="0"></A>
		  	 <%}
		  	 else {
		  	 	 %>
		
				 <jsp:include page="submitBar.jsp" flush="true"> 
						<jsp:param name="displayESign" value="N"/>
						<jsp:param name="formID" value="acctBrowser"/>
						<jsp:param name="showDiscard" value="N"/>
				 </jsp:include>


           	  <%}}%> 
	
	
		</td>

	  </tr>

    </table>

 <table>
		<tr><td>
		<% 
			if (totalRows==0) firstRec=0;
		
		if (rowsReturned > 0) 
		{		
		if (totalRows!=0){ Object[] arguments = {firstRec,lastRec,totalRows}; //JM:chaged for pagination
		
		%>
					<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%--<font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
		<%}
		} else {%>
			<font class="recNumber"> <%=MC.M_NoSrchResMatch_Criteria%><%--No search results matching given criteria found*****--%></font>	
		<%}%>	
		</td></tr>
		</table>
	<table align=center>
	<tr>
	<%
		if (curPage==1) startPage=1;
	    for ( count = 1; count <= showPages;count++)
		{
		
		 
   			cntr = (startPage - 1) + count;
	 		if ((count == 1) && (hasPrevious))
			{  
			  %>
				<td colspan = 2>
			  	<A href="multipleusersearchdetails.jsp?names=<%=names%>&rownum=<%=rownum%>&ids=<%=ids%>&from=<%=from%>&accountId=<%=accId%>&fname=<%=ufname%>&lname=<%=ulname%>&jobType=<%=jobTyp%>&accsites=<%=orgName%>&usrgrps=<%=gName%>&ByStudy=<%=stTeam%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&dataFld=<%=dataFld%>&dispFld=<%=dispFld%>&mode=\"M\"">< <%--Previous*****--%><%=LC.L_Previous%> <%=totalPages%> > </A>
				&nbsp;&nbsp;&nbsp;&nbsp;
				</td>	
				<%
  			}	%>
		<td>
		<%
 		 if (curPage  == cntr)
		 {	 
	    	 %>	   
				<FONT class = "pageNumber"><%= cntr %></Font>
       		<%
       	}else
        {	%>		
		<A href="multipleusersearchdetails.jsp?names=<%=names%>&rownum=<%=rownum%>&ids=<%=ids%>&from=<%=from%>&accountId=<%=accId%>&fname=<%=ufname%>&lname=<%=ulname%>&jobType=<%=jobTyp%>&accsites=<%=orgName%>&usrgrps=<%=gName%>&ByStudy=<%=stTeam%>&page=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&dataFld=<%=dataFld%>&dispFld=<%=dispFld%>&mode=\"M\""><%= cntr%></A>
       <%}%>
		</td>
		<%	}
		if (hasMore)
		{   %>
	   <td colspan = 3 align = center>&nbsp;&nbsp;&nbsp;&nbsp;
	   <A href="multipleusersearchdetails.jsp?names=<%=names%>&rownum=<%=rownum%>&ids=<%=ids%>&from=<%=from%>&accountId=<%=accId%>&fname=<%=ufname%>&lname=<%=ulname%>&jobType=<%=jobTyp%>&accsites=<%=orgName%>&usrgrps=<%=gName%>&ByStudy=<%=stTeam%>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&dataFld=<%=dataFld%>&dispFld=<%=dispFld%>&mode=\"M\"">< <%--Next*****--%><%=LC.L_Next%> <%=totalPages%>></A>
		</td>	
		<%	}	%>
 
	   </tr>
	  </table>

  </Form>

  <%//}//end of Parameter check





%>

<%

}//end of if body for session

else{

%>

  <jsp:include page="timeout_childwindow.jsp" flush="true"/>

  <%

}



%>



</div>

<div class ="mainMenu" id = "emenu"> </div>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

</body>

</html>


