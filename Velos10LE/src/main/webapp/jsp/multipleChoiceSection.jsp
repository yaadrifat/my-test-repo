<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="jqueryUtils.jsp"></jsp:include>
<jsp:include page="velos_includes.jsp"></jsp:include>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Fld_MultiChoice%><%--Field Multiple Choice*****--%> </title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>
<script type="text/javascript" src="./FCKeditor/fckeditor.js"></script>
<script type="text/javascript">
	checkQuote="N";
	var fck;
	var oFCKeditor;
     function init()
      {
       
      /*oFCKeditor = new FCKeditor("nameta") ;
      oFCKeditor.BasePath = './FCKeditor/';
      oFCKeditor.Width = editorWidth ;
      oFCKeditor.Height = editorHeight ; // 400 pixels
      oFCKeditor.ToolbarSet="Velos";
      //oFCKeditor.Config['SkinPath'] = '/FCKeditor/editor/skins/office2003/' ;
      oFCKeditor.ReplaceTextarea() ;*/
      if (document.multipleBoxField.section.type!="hidden")
	document.multipleBoxField.section.focus();
	else 
	document.multipleBoxField.sequence.focus();
      }
      function processFormat(formobj,mode)
      {
       
      	if (mode=="D")
	{
	 if (formobj.nameta.value.length>0)
	 formobj.nameta.value="[VELDISABLE]"+formobj.nameta.value;
	 //showHide("eformat","S");
	 //showHide("dformat","H");
	 document.poplayer=document.getElementById("dformat");
	 document.poplayer.innerHTML="<A href='javascript:void(0)' onClick='processFormat(document.multipleBoxField,\"E\")'><img src=\"./images/enableformat.gif\" alt=\"<%=LC.L_Enable_Formatting%>/*Enable Formatting*****/\" border = 0 align=absbotton></A>";
	 
	} else if (mode=="R")
	{
		if (confirm("<%=MC.M_RemFmt_ForFldName%>"))/*if (confirm("Remove formatting for field name?"))*****/
	   {
	    formobj.nameta.value="";
	    
	   }
	} else if(mode=="E")
	{
	 formobj.nameta.value=replaceSubstring(formobj.nameta.value,"[VELDISABLE]","");
	 //showHide("eformat","H");
	 //showHide("dformat","S");
	 document.poplayer=document.getElementById("dformat");
	 document.poplayer.innerHTML="<A href='javascript:void(0)' onClick='processFormat(document.multipleBoxField,\"D\")'><img src=\"./images/disableformat.gif\" alt=\"<%=LC.L_Disable_Formatting%>/*Disable Formatting*****/\" border = 0 align=absbotton></A>";
	}
	
      }
    </script>
	<script type="text/javascript">
     overRideChar("#");
   overRideChar("&");
   overRideFld("name");
   var editor;
    function FCKeditor_OnComplete(oFCKeditor)
{
	 fck= FCKeditorAPI.GetInstance('nameta');
	 
}
  </script>
  
<jsp:include page="include.jsp" flush="true"/>

</head>



<SCRIPT Language="javascript">
 window.onload=function()
 {
  init();
 }
 function confirmBox(respName,pgRight,totRows,offline) {
     //can't delete all the responses
    
    if(offline == 1)
     {
    	var paramArray = [respName];
        alert(getLocalizedMessageString("M_CntDelResp_OffEdtStat",paramArray));/*alert("You cannot Delete "+respName+ " from Responses in Offline For Editing status");*****/
        return false;
     }
    else{
        if (totRows==1) {
        	alert("<%=MC.M_CntDel_AllRespMultiFld%>");/*alert("You cannot delete all the reponses of Multiple Choice Field.");*****/
           return false;
        }
     
        if (f_check_perm(pgRight,'E') == true) {
            
        	var paramArray = [respName];
        	msg=getLocalizedMessageString("L_Del_FrmResp",paramArray);/*msg="Delete " + respName + " from Responses?";*****/

        if (confirm(msg)) 
        {
            return true;
        }
        else
        {
            return false;
        }
        } else {
            return false;
        }
     }
 }

 
 function  validate(formobj)
 {
 	 
	if (!(validate_col ('Section', formobj.section)  )  )  return false
    if (!(validate_col('Sequence',formobj.sequence))) return false
    
       
	 
	 if (!(validate_col('Field Name',formobj.name)))  return false;
	 
	 
	  /*if (checkChar(formobj.name.value,"'"))
	 {
	   alert("Special Character(') not allowed for this Field");
	    formobj.name.focus();
	    return false;
	 }*/
	     
    if ((formobj.name.value.length)>2000)
	{
    	alert("<%=MC.M_FldNameMax2000_CrtCont%>");/*alert("'Field Name' length exceeds the maximum allowed limit of 2000 characters.\n Please correct to continue.");*****/
	   formobj.name.focus();
	  return false;
	}
	
	/*if (formobj.nameta.value.length>0)
	formobj.nameta.value=htmlEncode(formobj.nameta.value);*/
	
	    
    if (!(validate_col_spl('Field Uniqueid',formobj.uniqueId))) return false
    
    if ((fnTrimSpaces((formobj.uniqueId.value).toLowerCase())=='er_def_date_01'))
	{
    	alert("<%=MC.M_ErDefDt01_SysKwrdFld%>");/*alert("'er_def_date_01' is a system reserved keyword. Please specify another Field Id.");*****/
	  formobj.uniqueId.focus();
	  return false;
	}
    
    	
    
	// @Gyanendra commented for bug #4421 if(!( validateDataSize(formobj.instructions,1000,'Field Help'))) return false
    
    if(isNaN(formobj.sequence.value) == true) 
    {
    	alert("<%=MC.M_SecSeq_EtrValidNum%>")/*Section Sequence has to be a number. Please enter a valid number.*****/
        formobj.sequence.focus();
        return false;
       }    
	  
	  if(isNaN(formobj.labelDisplayWidth.value) == true) 
      {
        alert("<%=MC.M_LabelDispWidth_HasToNum%>");/*alert("Label Display Width has to be a number. Please enter a valid number.");*****/
        formobj.labelDisplayWidth.focus();
        return false;
       }    


    
    if (! (splcharcheck(formobj.keyword.value)))
    {
          formobj.keyword.focus();
          return false;
    }
    if (! (checkquote(formobj.instructions.value)))
    {
          formobj.instructions.focus();
          return false;
    }
    
    if (! (splcharcheckForXSL(formobj.instructions.value)))
	{
		  formobj.instructions.focus();
		  return false;
	} 

  
	if(!(formobj.mandatory.checked) && formobj.overRideMandatory.checked)
	{
	  alert("<%=MC.M_ChkMndtFldFirst_ToOverride%>");/*alert("Please check Mandatory field first which needs to be overridden");*****/
	  return false;	
	}
	
    
      if(formobj.overRideMandatory.checked)
	{	
		formobj.overRideMandatory.value="1";
		}			
	else {
		formobj.overRideMandatory.value="0";
		}
		
    if   (  formobj.editBoxType[1].checked  )
    {
        
         if (!(validate_col('ColCount',formobj.colcount))) return false;
         if(isNaN(formobj.colcount.value) == true)
           {
        	 alert("<%=MC.M_Vldt_NumColumn%>");/*alert("Please enter a valid number in columns.");*****/
                 formobj.colcount.focus();
                 return false;
            }
         
         
    } 
      if(isNaN(formobj.colcount.value) == true)
           {
    	  alert("<%=MC.M_Vldt_NumColumn%>");/*alert("Please enter a valid number in columns.");*****/
                 formobj.colcount.focus();
                 return false;
            }
    if   ( formobj.editBoxType[2].checked) 
    {
        
         if (!(validate_col('ColCount',formobj.colcount))) return false;
           if(isNaN(formobj.colcount.value) == true)
           {
        	   alert("<%=MC.M_Vldt_NumColumn%>");/*alert("Please enter a valid number in columns.");*****/
                 formobj.colcount.focus();
                 return false;
            }
         
    } 
    	 // options 'hide display label' and 'field alignment top can not be selected together
	 	
	if(formobj.hideLabel.checked && formobj.align[3].checked)
	 {	
		 alert("<%=MC.M_HideFldLabel_CntSetAlign%>");/*alert("When option 'Hide Field Label' is selected, you can not set 'Align (Field Display Name)' to 'Top'. Please choose one of these options.");*****/
		 formobj.hideLabel.focus();
		 return false;
	 }	 
<%--    if (!(validate_col('e-Signature',formobj.eSign))) return false --%>
    	 
    if (formobj.mode.value=="N") 
    {
        return validateForNewMode(formobj);
    }
    
    totRows = formobj.totRows.value; 
    if (formobj.mode.value=="M") {
        respEntered = false;
		
		if (totRows > 1) {

        
 // checks duplicate display value entry 

    var len,currentValue1,currentValue2,currentDataValue1,currentDataValue2;	
	//len=document.multipleBoxField.txtDispVal.length;
	len=formobj.totRows.value;                          
	  
	  
	
	  
	var flagDispMod=0, flagDataMod = 0;
	

	for (ir=0;ir<len;ir++){
		currentValue1=fnTrimSpaces(document.multipleBoxField.txtDispVal[ir].value);
		
		currentDataValue1=fnTrimSpaces(document.multipleBoxField.txtDataVal[ir].value);
			
		if (currentValue1.length==0){
			document.multipleBoxField.txtDispVal[ir].value=""; 
			continue;
		}
		
		 		
	
		for(jr=0;jr<len;jr++){
			if (ir!=jr){
				currentValue2=fnTrimSpaces(document.multipleBoxField.txtDispVal[jr].value); 
				
				currentDataValue2=fnTrimSpaces(document.multipleBoxField.txtDataVal[jr].value);
				
				if(currentValue2.toLowerCase()==currentValue1.toLowerCase()){
					flagDispMod=flagDispMod+1;
					//return false;

			   }
			   
			   if (currentDataValue1.length > 0) {
				if(currentDataValue2.toLowerCase()==currentDataValue1.toLowerCase()){
					flagDataMod=flagDataMod + 1;
					//return false;

			      }
			   }
							   
			}
		}
	}

	

	
	// In case "add more responses", checks duplicate display value entry while  previous responses exist
	// modified by J. Majumdar on 14Feb05
	
	if(document.multipleBoxField.newTxtDispVal){ 
		
		if(document.multipleBoxField.newTxtDispVal.length>=2){
			len1=document.multipleBoxField.newTxtDispVal.length;				
		}
		else{
			len1=1;			
		}
		
		
		for (irn=0;irn<len;irn++){
			currentValue1=fnTrimSpaces(document.multipleBoxField.txtDispVal[irn].value);	
			
			currentDataValue1=fnTrimSpaces(document.multipleBoxField.txtDataVal[irn].value);
			
			if (currentValue1.length==0) {
				document.multipleBoxField.txtDispVal[irn].value="";
				continue;
			}
			
			 
			
			for(jrn=0;jrn<len1;jrn++){
				// "add more responses" are more than 1
				if(document.multipleBoxField.newTxtDispVal.length>=2){
				
				currentValue2=fnTrimSpaces(document.multipleBoxField.newTxtDispVal[jrn].value);
				
				currentDataValue2=fnTrimSpaces(document.multipleBoxField.newTxtDataVal[jrn].value);
				
				 
					if (currentValue2.length==0){
						document.multipleBoxField.newTxtDispVal[jrn].value="";
						continue;
					}
					 
					
				}	
				else { // "add more responses" is equal to 1 				
				currentValue2=fnTrimSpaces(document.multipleBoxField.newTxtDispVal.value);
				
				 currentDataValue2=fnTrimSpaces(document.multipleBoxField.newTxtDataVal.value);
				 
					if (currentValue2.length==0){
						document.multipleBoxField.newTxtDispVal.value="";
						continue;
					}
					
					 
					
				}

				
				if(currentValue2.toLowerCase()==currentValue1.toLowerCase()){
					flagDispMod=flagDispMod+1;
			   }
			  
			  if (currentDataValue1.length > 0) {
				   if(currentDataValue2.toLowerCase()==currentDataValue1.toLowerCase()){
						flagDataMod = flagDataMod+1;
				   }
				}
            }
		}
	}
	
	if(flagDispMod>0){
		alert("<%=MC.M_DupDisp_EtrDiffVal%>");/*alert("Duplicate Display Value exists. Please enter different value(s).");*****/
		return false;
	}

   if(flagDataMod>0){
	   alert("<%=MC.M_ValueExist_EtrDiff%>");/*alert("Duplicate Data Value exists. Please enter different value(s).");*****/
		return false;
	}

	        for(cnt = 0; cnt<totRows;cnt++){
            //check for valid sequence
                 if(isNaN(formobj.txtSeq[cnt].value) == true) {
                	 alert("<%=MC.M_InvalidSeq_EtrVldNum%>");/*alert("Invalid Sequence. Please enter a valid number.");*****/
                     formobj.txtSeq[cnt].focus();
                     return false;
                }
                
            //check for valid score
                if(isNaN(formobj.txtScore[cnt].value) == true) {
                	alert("<%=MC.M_InvalidScore_ReEtr%>");/*alert("Invalid Score. Please enter a valid number.");*****/
                     formobj.txtScore[cnt].focus();
                     return false;
                }
                
		    //check if display val and data value are entered if seq has been entered 
              if ((formobj.txtSeq[cnt].value != "")){				                
                    respEntered = true;
                    if(fnTrimSpaces(formobj.txtDispVal[cnt].value)=='' ){										
                    	alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
                        formobj.txtDispVal[cnt].focus()
                        return false;
						}
                    
                 }    
                 
                 //check if sequence and data value are entered if display value has been entered             
                  if ((formobj.txtDispVal[cnt].value != "")){
                    if(formobj.txtSeq[cnt].value=='' ){
                    	alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
                        formobj.txtSeq[cnt].focus()
                        return false;
                    }
                    
                 }
                 
                  //check if sequence and display value are entered if data value has been entered             
                  if ((formobj.txtDataVal[cnt].value != "")){
                    if(formobj.txtSeq[cnt].value=='' ){
                    	alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
                        formobj.txtSeq[cnt].focus()
                        return false;
                    }
                    if(formobj.txtDispVal[cnt].value=='' ){
                    	alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
                        formobj.txtDispVal[cnt].focus()
                        return false;
                    }                          
                 }
                 
            }//for

			 


           /* if (respEntered==false) 
            {
             alert("Please enter data in all mandatory fields.");
             formobj.txtSeq[0].focus();
             return false;
            }*/
        } else if ( totRows == 1) {//else if totRows
                //check for valid sequence

                
                 if(isNaN(formobj.txtSeq.value) == true) {
                	 alert("<%=MC.M_InvalidSeq_EtrVldNum%>");/*alert("Invalid Sequence. Please enter a valid number.");*****/
                     formobj.txtSeq.focus();
                     return false;
                }
                
            //check for valid score
                if(isNaN(formobj.txtScore.value) == true) {
                	alert("<%=MC.M_InvalidScore_ReEtr%>");/*alert("Invalid Score. Please enter a valid number.");*****/
                     formobj.txtScore.focus();
                     return false;
                }
                
            //check if display val and data value are entered if seq has been entered 
              if ((formobj.txtSeq.value != "")){
                    respEntered = true;
                    if(fnTrimSpaces(formobj.txtDispVal.value)=='' ){
                    	alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
                        formobj.txtDispVal.focus()
                        return false;
                    }
                    
                 }    
                 
                 //check if sequence and data value are entered if display value has been entered             
                  if ((formobj.txtDispVal.value != "")){
                    if(formobj.txtSeq.value=='' ){
                    	alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
                        formobj.txtSeq.focus()
                        return false;
                    }
                    
                 }
                 
                  //check if sequence and display value are entered if data value has been entered             
                  if ((formobj.txtDataVal.value != "")){
                    if(formobj.txtSeq.value=='' ){
                    	alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
                        formobj.txtSeq.focus()
                        return false;
                    }
                    if(formobj.txtDispVal.value=='' ){
                    	alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
                        formobj.txtDispVal.focus()
                        return false;
                    }                          
                 }
        
           /* if (respEntered==false) 
            {
             alert("Please enter data in all mandatory fields.");
             formobj.txtSeq.focus();
             return false;
            }*/
        
		
		// In case "add more responses", checks duplicate display value entry while existing response is equal to 1 
		//added by J. Majumdar date 14Feb05
			 var flagDispMod1=0,flagDataMod1=0;
				 if(document.multipleBoxField.newTxtDispVal){
					currentValue1=fnTrimSpaces(document.multipleBoxField.txtDispVal.value);
					
					currentDataValue1 =fnTrimSpaces(document.multipleBoxField.txtDataVal.value);
					
					if (currentValue1.length==0) 
					{
						document.multipleBoxField.txtDispVal.value="";
					}
					
					 

					if(document.multipleBoxField.newTxtDispVal.length>=2){
						len=document.multipleBoxField.newTxtDispVal.length
					}else{
						len=1;	
					}

					 for(jrne=0;jrne<len;jrne++){
							// "add more responses" are more than 1
							if(document.multipleBoxField.newTxtDispVal.length>=2){
								currentValue2=fnTrimSpaces(document.multipleBoxField.newTxtDispVal[jrne].value); 
								
								currentDataValue2=fnTrimSpaces(document.multipleBoxField.newTxtDataVal[jrne].value);
								
									if (currentValue2.length==0){			
									document.multipleBoxField.newTxtDispVal[jrne].value="";
									continue;
									}
									
									 
									
							}

							else{ // "add more responses" is equal to 1
							
								currentValue2=fnTrimSpaces(document.multipleBoxField.newTxtDispVal.value); 
								
								currentDataValue2=fnTrimSpaces(document.multipleBoxField.newTxtDataVal.value);
								
									if (currentValue2.length==0){			
									document.multipleBoxField.newTxtDispVal.value="";
									continue;
								}
								
									 
								
							}
						
						
							if(currentValue2.toLowerCase()==currentValue1.toLowerCase())
								{
									flagDispMod1=flagDispMod1+1;
							}
							
							if (currentDataValue1.length > 0) {
							if(currentDataValue2.toLowerCase()==currentDataValue1.toLowerCase())
								{
									flagDataMod1=flagDataMod1+1;
							}
						}
							
				     }
							
				if(flagDispMod1>0){
					alert("<%=MC.M_DupDisp_EtrDiffVal%>");/*alert("Duplicate Display Value exists. Please enter different value(s).");*****/
					return false;
				}
				
				if(flagDataMod1>0){
					alert("<%=MC.M_ValueExist_EtrDiff%>");/*alert("Duplicate Data Value exists. Please enter different value(s).");*****/
					return false;
				}
				
			}	
				
		
		}//end if totRows
    } //mode=M

    if (formobj.fromRefresh.value=="true") 
    {
        return validateForNewMode(formobj);
    }

<%--    
    if (!(validate_col('e-Signature',formobj.eSign))) return false

    if(isNaN(formobj.eSign.value) == true) 
    {
    	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
        formobj.eSign.focus();
        return false;
       }
--%>
       
           return true;
  }

  
function validateForNewMode(formobj)
{

	
     totRows = formobj.defRespNo.value;     

     respEntered = false;
     countDisp = 0;  


     if (totRows>1) {
        for(cnt = 0; cnt<totRows;cnt++){



        //check for valid sequence
             if(isNaN(formobj.newTxtSeq[cnt].value) == true) {
            	 alert("<%=MC.M_InvalidSeq_EtrVldNum%>");/*alert("Invalid Sequence. Please enter a valid number.");*****/
                 formobj.newTxtSeq[cnt].focus();
                 return false;
            }

            
        //check for valid score

            if(isNaN(formobj.newTxtScore[cnt].value) == true) {
            	alert("<%=MC.M_InvalidScore_ReEtr%>");/*alert("Invalid Score. Please enter a valid number.");*****/
                 formobj.newTxtScore[cnt].focus();
                 return false;
            }
            
        //check if display val and data value are entered if seq has been entered 
    
         /* if ((formobj.newTxtSeq[cnt].value != "")){
                respEntered = true;
                if(formobj.newTxtDispVal[cnt].value=='' ){
                    alert("inside new");
                    alert("Please enter data in all mandatory fields.")
                    formobj.newTxtDispVal[cnt].focus()
                    return false;
                }
                
             }*/    
             
             //check if sequence and data value are entered if display value has been entered             
              if ((formobj.newTxtDispVal[cnt].value != "")){
                if(formobj.newTxtSeq[cnt].value=='' ){
                	alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
                    formobj.newTxtSeq[cnt].focus()
                    return false;
                }
                
             }
             
              //check if sequence and display value are entered if data value has been entered             
              if ((formobj.newTxtDataVal[cnt].value != "")){
                if(formobj.newTxtSeq[cnt].value=='' ){
                	alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
                    formobj.newTxtSeq[cnt].focus()
                    return false;
                }
				if(fnTrimSpaces(formobj.newTxtDispVal[cnt].value)=='' ){
					alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
                    formobj.newTxtDispVal[cnt].focus()
                    return false;
                }                                      
             }
             
        }//for
	// checks duplicate display value entry, new mode

    var len,currentValue1,currentValue2, currentDataValue1,currentDataValue2;	
	len=totRows;
	var flagDispNew=0,flagDataNew=0;	

	for (ir=0;ir<len;ir++){
		currentValue1=fnTrimSpaces(document.multipleBoxField.newTxtDispVal[ir].value);
		
		currentDataValue1=fnTrimSpaces(document.multipleBoxField.newTxtDataVal[ir].value);

		if (currentValue1.length==0) {
			document.multipleBoxField.newTxtDispVal[ir].value="";
			continue;
		}
		 
		
		for(jr=0;jr<len;jr++){
			if (ir!=jr){
				currentValue2=fnTrimSpaces(document.multipleBoxField.newTxtDispVal[jr].value); 
				
				currentDataValue2=fnTrimSpaces(document.multipleBoxField.newTxtDataVal[jr].value);

				if (currentValue2.length==0){ 
						document.multipleBoxField.newTxtDispVal[jr].value="";
						continue;
					}
					
				 	
					
				if(currentValue2.toLowerCase()==currentValue1.toLowerCase()){
					flagDispNew=flagDispNew+1;
					//return false;

			   }
			   
			   if (currentDataValue1.length > 0) {
			   	if(currentDataValue2.toLowerCase()==currentDataValue1.toLowerCase()){
					flagDataNew=flagDataNew+1;
					//return false;

			     }
			   }
			   
			}
		}
	}

	if(flagDispNew>0){
		alert("<%=MC.M_DupDisp_EtrDiffVal%>");/*alert("Duplicate Display Value exists. Please enter different value(s).");*****/
		return false;
	}
	
	if(flagDataNew>0){
		alert("<%=MC.M_ValueExist_EtrDiff%>");/*alert("Duplicate Data Value exists. Please enter different value(s).");*****/
		return false;
	}

       /* if (respEntered==false) 
        {
            alert("resp false:");
         alert("Please enter data in all mandatory fields.");
         formobj.newTxtDispVal[0].focus();
         return false;
        }*/
 
            for(cnt = 0; cnt<totRows;cnt++){
                
                if (fnTrimSpaces(formobj.newTxtDispVal[cnt].value)=='')
                    {
                    countDisp++;
                }
            
            }
   
    
    if(countDisp == totRows)
    {
        
    	alert("<%=MC.M_EtrAtleast_OneResp%>");/*alert("Please enter atleast one Response");*****/
        formobj.newTxtDispVal[0].focus();
        return false;

    }
    } else {//else if totRows

        //check for valid sequence
             if(isNaN(formobj.newTxtSeq.value) == true) {
            	 alert("<%=MC.M_InvalidSeq_EtrVldNum%>");/*alert("Invalid Sequence. Please enter a valid number.");*****/
                 formobj.newTxtSeq.focus();
                 return false;
            }
            
        //check for valid score
            if(isNaN(formobj.newTxtScore.value) == true) {
            	alert("<%=MC.M_InvalidScore_ReEtr%>");/*alert("Invalid Score. Please enter a valid number.");*****/
                 formobj.newTxtScore.focus();
                 return false;
            }
            
        //check if display val and data value are entered if seq has been entered 
          /*if ((formobj.newTxtSeq.value != "")){
                respEntered = true;
                if(formobj.newTxtDispVal.value=='' ){
                    alert("Please enter data in all mandatory fields.")
                    formobj.newTxtDispVal.focus()
                    return false;
                }
                
             } */   
             
             //check if sequence and data value are entered if display value has been entered             
              if ((formobj.newTxtDispVal.value != "")){
                if(formobj.newTxtSeq.value=='' ){
                	alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
                    formobj.newTxtSeq.focus()
                    return false;
                }
                
             }
             
              //check if sequence and display value are entered if data value has been entered             
              if ((formobj.newTxtDataVal.value != "")){
                if(formobj.newTxtSeq.value=='' ){
                	alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
                    formobj.newTxtSeq.focus()
                    return false;
                }
                if(formobj.newTxtDispVal.value=='' ){
                	alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
                    formobj.newTxtDispVal.focus()
                    return false;
                }                          
             }
             
     /*   if (respEntered==false) 
        {
         alert("Please enter data in all mandatory fields.");
         formobj.newTxtSeq.focus();
         return false;
        }*/
    
    }//end if totRows

   return true;    
	 
}
  


function setDefVal(formobj,totRows,modRows) 
{
    
   modRows = formobj.totRows.value;
   
   if (totRows > 1 ) {
       
        for (i=0;i<totRows;i++)
        {
            if (formobj.newRdDefault[i].checked) 
                {
                    formobj.newHdDefault[i].value="1"
                } else {
                    formobj.newHdDefault[i].value="0"
                }
        }
    } else {
            if (formobj.newRdDefault.checked){
                formobj.newHdDefault.value="1"
            } else {
                formobj.newHdDefault.value="0"
            }
  } //if

    if (modRows == 1) {
    
        if (formobj.rdDefault.checked) {
            formobj.rdDefault.checked = false;
            formobj.hdDefault.value="0";                
        }
    } else  {
        for (i=0;i<modRows;i++) {
             if (formobj.rdDefault[i].checked) {
                 formobj.rdDefault[i].checked = false;
                 formobj.hdDefault[i].value="0";                     
             }
        }
    }            
  
}

function setHdDefVal(formobj,totRows) 
{ 
  newRows  = formobj.defRespNo.value;
  
  if (totRows > 1) { 
    for (i=0;i<totRows;i++)
    {
        
        if (formobj.rdDefault[i].checked) 
            {
                formobj.hdDefault[i].value="1"
            } else {
                formobj.hdDefault[i].value="0"
            }
    }
 } else {
        if (formobj.rdDefault.checked){
            formobj.hdDefault.value="1"
        } else {
            formobj.hdDefault.value="0"
        }    
 }//end if

 if (newRows==1) {    
    if (formobj.newRdDefault.value=="on") {
        formobj.newHdDefault.value="0";
         formobj.newRdDefault.checked = false;
        
    }
 } else  {
    for (i=0;i<newRows;i++) {
      if (formobj.newRdDefault[i].value=="on") {
           formobj.newHdDefault[i].value="0";
          formobj.newRdDefault[i].checked = false;
        
      }
    }
 } 
}
////////////////////////////////

function setFldRespRecordTypeVal(formobj,totRows) 
{ 
  newRows  = formobj.defRespNo.value;
  
  if (totRows > 1) { 
    for (i=0;i<totRows;i++)
    {
        
        if (formobj.respRecordTypeCheck[i].checked) 
            {
                formobj.hdRecordType[i].value="H";
            } else {
                formobj.hdRecordType[i].value="M";
            }
    }
 } else {
        if (formobj.respRecordTypeCheck.checked){
            formobj.hdRecordType.value="H";
        } else {
            formobj.hdRecordType.value="M";
        }    
 }//end if
 
}


/////////////////////
function refreshPage(formobj)
{
    if(formobj.codeStatus.value=="Freeze")
    {
        alert("<%=MC.M_YouCnt_FreezeForm%>");/*alert("You cannot add more responses to a Freeze form");*****/
        return false;
    }
 	// #5437,5439 02/02/2011 @Ankit
    if(formobj.codeStatus.value=="Deactivated")
    {
        alert("<%=MC.M_YouCnt_DeactForm%>");/*alert("You cannot add more responses to a Deactivated form");*****/
        return false;
    }

    if(formobj.codeStatus.value=="Active")
    {
        alert("<%=MC.M_YouCnt_ActiveForm%>");/*alert("You cannot add more responses to a Active form");*****/
        return false;
    }
    
    if(formobj.moreRespNo.value=='' ){
    	alert("<%=MC.M_PlsEtr_NumResp%>")/*alert("Please enter the number for More Responses.")*****/
       formobj.moreRespNo.focus()
       return false;
    }                          

     if(isNaN(formobj.moreRespNo.value) == true) {
    	 alert("<%=MC.M_PlsValid_NumResp%>");/*alert("Please enter a valid number in Add More Responses.");*****/
         formobj.moreRespNo.focus();
         return false;
    }
        
     if (confirm("<%=MC.M_Refreshing_RowsSvdata%>"))/*if (confirm("Refreshing the number of rows will save the data entered above. Do you wish to continue?"))*****/
     {
       formobj.refresh.value = "true";        
       if (validate(formobj))
        {
             formobj.submit();
          }
     } 
      else {
        return false;
    }
}

function removeDefault(formobj,totRows)
{
 newRows  = formobj.defRespNo.value;
 var count=0;
if (formobj.mode.value=="M") {
    if (totRows > 1) { 
        for (i=0;i<totRows;i++)
            {
            if (formobj.rdDefault[i].checked) 
                {
                    formobj.hdDefault[i].value="0"
                    formobj.rdDefault[i].checked= false
                    count++;
                }
            }
        }  

    else {
        if (formobj.rdDefault.checked){
            formobj.hdDefault.value="0"
            formobj.rdDefault.checked= false
            count++;
          } 
        }
}
else if (formobj.mode.value=="N") {

if (newRows==1) {    
    if (formobj.newRdDefault.checked) {
       formobj.newRdDefault.checked = false;
       formobj.newHdDefault.value="0"
       count++;
    }
 } else  {
    for (i=0;i<newRows;i++) {
      if (formobj.newRdDefault[i].checked) {
        formobj.newRdDefault[i].checked = false;
        formobj.newHdDefault[i].value="0"
        count++;
      }
     }
 }        
}
    if(count==0)
    {
    	alert("<%=MC.M_No_DefRespSelc%>");/*alert("No default response selected");*****/
    }
}

/***** @Gyanendra update for Bug#20544  *****/

var textEventLimitFunction = {		
		fixTextAreas:{}
	};

     textEventLimitFunction.fixTextAreas = function (frm) {
	
	limitChars('fldinst', 4000, 'countfldinst');
	
	$j("#fldinst").keyup(function(){
		var divId = 'count'+this.id;
		limitChars(this.id, 4000, divId);
	});	
};

$j(document).ready(function(){	
	
	textEventLimitFunction.fixTextAreas();
		
});

/***** End update @Gyanendra *****/
      
</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="fieldRespB" scope="request" class="com.velos.eres.web.fldResp.FldRespJB"/>
<jsp:useBean id="fieldLibJB" scope="request"  class="com.velos.eres.web.fieldLib.FieldLibJB"/>
<jsp:useBean id="catLibJB" scope="request"  class="com.velos.eres.web.catLib.CatLibJB"/>
<jsp:useBean id="formSecJB" scope="request"  class="com.velos.eres.web.formSec.FormSecJB"/>
<jsp:useBean id="formFieldJB" scope="request"  class="com.velos.eres.web.formField.FormFieldJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB, com.velos.eres.business.fieldLib.impl.*,com.velos.eres.web.studyRights.StudyRightsJB"%>
<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
       ienet = 0; //IE
else
    ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>
<%--For Bug# 8205:Yogendra--%>
<jsp:include page="popupPanel.jsp" flush="true"/>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div> 
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<script type="text/javascript" src="./FCKeditor/fckeditor.js"></script>

<DIV class="popDefault" id="div1"> 
 <%

    HttpSession tSession = request.getSession(true);
    int pageRight = 0;    
    
   if (sessionmaint.isValidSession(tSession))
   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>

<% 
     GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");        
     String calledFrom=request.getParameter("calledFrom");
     String lnkFrom=request.getParameter("lnkFrom");
     if(lnkFrom==null) 
        {lnkFrom="-";}

     
     String stdId=request.getParameter("studyId");
     int studyId= EJBUtil.stringToNum(stdId);            
    
        String userIdFromSession = (String) tSession.getValue("userId");
        
        if( calledFrom.equals("A")){ //from account form management
             if (lnkFrom.equals("S")) 
               {   
                  ArrayList tId ;
                 tId = new ArrayList();
                 if(studyId >0)
                 {
                     TeamDao teamDao = new TeamDao();
                    teamDao.getTeamRights(studyId,EJBUtil.stringToNum(userIdFromSession));
                    tId = teamDao.getTeamIds();
                    if (tId.size() <=0)
                     {
                        pageRight  = 0;
                      } else 
                      {
                          StudyRightsJB stdRights = new StudyRightsJB();
                        stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));
                         
                        	ArrayList teamRights ;
						teamRights  = new ArrayList();
						teamRights = teamDao.getTeamRights();
							 
						stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
						stdRights.loadStudyRights();
					         
                        if ((stdRights.getFtrRights().size()) == 0)
                        {                    
                          pageRight= 0;        
                        } else 
                        {                                
                            pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
                        }
                      }
                }
             }    
             else
            {         
                  pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMS"));
             }
          } 
        
         if( calledFrom.equals("L")) //form library
         {
            pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFRMLIB"));
         }
        
        if( calledFrom.equals("St")) //study
         {
            StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");    
              pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
        }
         

    if (pageRight >= 4)
        {
        
            //the number of defualt responses
            int fldRespCount = 0 ; 
          Configuration conf = new Configuration();
          fldRespCount = conf.getFldRespCount(conf.ERES_HOME +"eresearch.xml");
        
        String mode=request.getParameter("mode");
        String refresh=request.getParameter("refresh"); //indicates whether refresh has been clicked by the user
        if (refresh==null) refresh="false";
        int moreRespNo=EJBUtil.stringToNum(request.getParameter("moreRespNo")); //indicates the no of rows to be added                
        
        String fieldLibId="";
        //String fldCategory="";
        String fldName="";
	String fldNameFormat="";
	String formatDisable="N";
        String fldDesc = "";
        String fldUniqId="";
        String fldKeyword="";
        String fldInstructions="";
        String fldType="";
        String fldDataType="";
        String fldMoreColumns="";
        String fldColCount="";
        String fldCharsNo="";
        String codeStatus="WIP";
        String fldExpLabel="0";
		String mandatoryChk = "0";

        
        // THE STYLE OBJECT WHICH TAKES VALUES OF ATTRIBUTES OF THE FIELD  
        // LIKE BOLD, SAMELINE, ALIGN , ITALICS
        Style aStyle= new Style();
        String formId="";
        String formSecId="";
        String fldMandatory="0";
        String fldSeq="";
        String fldAlign="left";
        String samLinePrevFld="0";
        String fldBold="0";
        String fldItalics="0";
        String formFldId="";
        
            
        String lkpFilter = "";
        String lkpKeywords = "";
        String lkpView = "";
        String fldIsVisible="0";

        
        formId=request.getParameter("formId");        
        String accountId=(String)tSession.getAttribute("accountId");
        int tempAccountId=EJBUtil.stringToNum(accountId);
        String catLibType="C" ;
        
            
        

        int defVal = 0;
        int fldRespId=0;
        int totRows=0;
        int defRespNo=0;
        int seq = 0;
        String dispVal = "";
        String dataVal = "";
        String offLine = "";
        String offlineFlag = "";
        String score = "";
        String respRecordType = "";
        ArrayList arrRespId = new ArrayList();
        ArrayList arrSeq=new ArrayList();
        ArrayList arrScore=new ArrayList();
        ArrayList arrDispVal=new ArrayList();
        ArrayList arrDataVal=new ArrayList();
        ArrayList arrDefaultVal=new ArrayList();
        ArrayList arrOffline = new ArrayList();
		ArrayList arrRecordType = new ArrayList();

        ArrayList idSec= new ArrayList();
        ArrayList descSec= new ArrayList();        


        String pullDownSection;
    
        FormSecDao formSecDao= new FormSecDao();
        formSecDao = formSecJB.getAllSectionsOfAForm( EJBUtil.stringToNum(formId));
        idSec= formSecDao.getFormSecId();
        descSec=formSecDao.getFormSecName();
        pullDownSection=EJBUtil.createPullDown("section", 0, idSec, descSec);
        
        
        
        String inputClass = "inpDefault";
        String makeReadOnly = "";
        String makeGrey = "Black";
        String makeDisabled = "";
        String finputClass = "inpDefault";
        String fmakeReadOnly = "";
        String fmakeGrey = "Black";
        String fmakeDisabled = "";
        String foinputClass = "inpDefault";
        String fomakeReadOnly = "";
        String fomakeGrey = "Black";
        String fomakeDisabled = "";
        String ftextGrey = "textDefault";   
        
        String hideLabel = "";
	String labelDisplayWidth = "";	
	String hideResponseLabel = "";
	String responseAlign = "";
	String sortOrder="";

		
		
        codeStatus= request.getParameter("codeStatus");
        if(codeStatus==null)
            codeStatus="WIP";

        
                        
        if  (   mode.equals("M")   )
        { 
                
                
                formFldId=request.getParameter("formFldId");        
                codeStatus = request.getParameter("codeStatus");
                

                formFieldJB.setFormFieldId(Integer.parseInt(formFldId) );
                formFieldJB.getFormFieldDetails();
                fieldLibId=formFieldJB.getFieldId();
                offlineFlag = formFieldJB.getOfflineFlag();
                            
                fieldLibJB.setFieldLibId(Integer.parseInt(fieldLibId));
                fieldLibJB.getFieldLibDetails();
                fldExpLabel = ((fieldLibJB.getExpLabel())== null)?"":(fieldLibJB.getExpLabel()) ; 
                
   				hideLabel = ((fieldLibJB.getFldHideLabel())== null)?"":(fieldLibJB.getFldHideLabel()) ; 
				labelDisplayWidth = ((fieldLibJB.getFldDisplayWidth())== null)?"":(fieldLibJB.getFldDisplayWidth()) ; 
				hideResponseLabel = ((fieldLibJB.getFldHideResponseLabel())== null)?"":(fieldLibJB.getFldHideResponseLabel()) ; 
				responseAlign = ((fieldLibJB.getFldResponseAlign())== null)?"":(fieldLibJB.getFldResponseAlign()) ; 
				

                fldIsVisible=((fieldLibJB.getFldIsVisible()) == null)?"0":(fieldLibJB.getFldIsVisible()) ;
                

                formSecId=formFieldJB.getFormSecId();  
                fldMandatory=((formFieldJB.getFormFldMandatory()) == null)?"":(formFieldJB.getFormFldMandatory()) ;
                

                if(codeStatus.equals("Freeze"))
                {
                    fmakeReadOnly = "readonly";
                    finputClass = "inpGrey";
                    fmakeGrey = "Grey";
                    fmakeDisabled ="disabled";
                    ftextGrey = "textGrey";
                }
                 
                if((codeStatus.equals("Offline")  && !offlineFlag.equals("0")) || codeStatus.equals("Freeze"))
                {
                    fomakeReadOnly = "readonly";
                    foinputClass = "inpGrey";
                    fomakeGrey = "Grey";
                    fomakeDisabled ="disabled";

                }
                            
                //TO GIVE A SINGLE SECTION IN EDIT MODE
                ArrayList idSecNew = new ArrayList();
                ArrayList descSecNew = new ArrayList();
                
                int secId =EJBUtil.stringToNum(formSecId) ;
                 for(int j = 0 ; j < idSec.size() ; j++)
                 {
                     int  secIdNew =(   (Integer) idSec.get(j)  ).intValue();
                    if ( secIdNew  ==   secId )
                    {
                         idSecNew.add(   (  Integer )idSec.get(j) );
                        descSecNew.add( (String )descSec.get(j) ) ;
                        break ;    
                    }
                 }
                  


                 String sectionN = (String)descSecNew.get(0) ;
                pullDownSection = "<label>"+sectionN+"</label>  "
                                             + " <Input type=\"hidden\" name=\"section\" value="+(Integer)idSecNew.get(0)+" >  " ;
                
                //pullDownSection=EJBUtil.createPullDown("section", secId,idSecNew, descSecNew   );
                
                
                
                
                
                
                
                fldSeq=((formFieldJB.getFormFldSeq()) == null)?"":(formFieldJB.getFormFldSeq()) ;        
                
                
                fldName=fieldLibJB.getFldName();
		
		fldNameFormat=fieldLibJB.getFldNameFormat();
		fldNameFormat=(fldNameFormat==null)?"":fldNameFormat;
		
		if (fldNameFormat.length()>0)
			{
			 if (fldNameFormat.indexOf("[VELDISABLE]")>=0)
			 formatDisable="Y";
			}
		
                fldDesc = ((fieldLibJB.getFldDesc()) == null)?"":(fieldLibJB.getFldDesc());        
                fldUniqId=fieldLibJB.getFldUniqId();
                fldKeyword = ((fieldLibJB.getFldKeyword()) == null)?"":(fieldLibJB.getFldKeyword());        
                
                fldType=fieldLibJB.getFldType();
                fldDataType=fieldLibJB.getFldDataType();
				mandatoryChk = ((fieldLibJB.getOverRideMandatory()) == null)?"0":(fieldLibJB.getOverRideMandatory()) ;
                
                
                if (fldDataType == null)
                    fldDataType = "";
                fldColCount=fieldLibJB.getFldColCount();
                if (fldColCount == null)
                    fldColCount = "";
                
                fldInstructions = ((fieldLibJB.getFldInstructions()) == null)?"":(fieldLibJB.getFldInstructions()) ;
		
		
		sortOrder=fieldLibJB.getFldSortOrder();
		sortOrder=(sortOrder==null)?"":sortOrder;
                
                
                //  WHILE GETTING THE STYLE WE CHECK ITS VALUES TO DISPLAY APPROPRIATELY
                aStyle=fieldLibJB.getAStyle( ); 
                if ( aStyle != null )  
                {
                    
                    fldAlign = (  (   aStyle.getAlign()   ) == null)?"-":(   aStyle.getAlign()   ) ;    
                    fldBold = (  (   aStyle.getBold()   ) == null)?"-1":(   aStyle.getBold()   ) ;    
                    fldItalics = (  (   aStyle.getItalics()   ) == null)?"-1":(   aStyle.getItalics()   ) ;    
                    samLinePrevFld = (  (   aStyle.getSameLine()   ) == null)?"-1":(   aStyle.getSameLine()   ) ;    
                    }
                
                /*lkpKeywords = fieldLibJB.getLkpDisplayVal();
                    if (lkpKeywords == null ) lkpKeywords = "";
                lkpFilter = fieldLibJB.getLkpDataVal();    
                    if (lkpFilter  == null ) lkpFilter  = "";
                lkpView = fieldLibJB.getLookup();
                    if (lkpView == null ) lkpView = "";*/

                FldRespDao fldRespDao = fieldRespB.getResponsesForField(EJBUtil.stringToNum(fieldLibId));    
                arrRespId = fldRespDao.getArrFldRespIds();
                arrSeq=fldRespDao.getArrSequences();    
                arrScore=fldRespDao.getArrScores();  
				
                arrDispVal=fldRespDao.getArrDispVals();
                arrDataVal=fldRespDao.getArrDataVals();
		
		System.out.println("dispVal"+arrDispVal+"dataVal"+arrDataVal);
                arrDefaultVal=fldRespDao.getArrDefaultValues();
                arrOffline = fldRespDao.getArrOfflines();
                
                arrRecordType = fldRespDao.getArrRecordTypes(); 
                
                
                totRows=arrSeq.size();
    
        }
        
%>

    
    
  
    
    <Form name="multipleBoxField" id="multChoiceSec" method="post" action="multipleChoiceSectionSubmit.jsp" onsubmit=" if (validate(document.multipleBoxField)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
    <Input type="hidden" name="mode" value=<%=mode%> >
    <Input type="hidden" name="fieldLibId" value=<%=fieldLibId%> >
    <Input type="hidden" name="refresh" value="false" >
    <Input type="hidden" name="fromRefresh" value=<%=refresh%> >
    <Input type="hidden" name="formId" value=<%=formId%> >  
    <Input type="hidden" name="formFldId" value=<%=formFldId%> >
    <Input type="hidden" name="calledFrom" value=<%=calledFrom%> >            
    <Input type="hidden" name="lnkFrom" value=<%=lnkFrom%> >
    <Input type="hidden" name="studyId" value=<%=studyId%> >
    <Input type="hidden" name="codeStatus" value=<%=codeStatus%> >
	<Input type="hidden" name="hdnOverRideMandatory"> 	
        
         
    <table width="90%" cellspacing="1" cellpadding="2" border="0"  >
    		<tr height="25"><td colspan="3"><P class="sectionHeadings"> <%=MC.M_FldType_MultiCbox%><%--Field type: Multiple choice box*****--%> </td></tr>
        <tr class="browserEvenRow"> 
      <td width="20%"><Font class="<%=fmakeGrey%>"><%=LC.L_Section%><%--Section*****--%></font><FONT class="Mandatory">* </FONT></td>
        <td width="15%"><%=pullDownSection%> </td>
      </tr>
        
    <tr class="browserEvenRow"> 
      <td ><Font class="<%=fmakeGrey%>"><%=LC.L_Sequence%><%--Sequence*****--%></font><FONT class="Mandatory">* </FONT></td>
        <td ><input type="text" name="sequence" size = 10 MAXLENGTH = 10 value="<%=fldSeq%>" <%=fmakeReadOnly%>> </td>
    </tr>
<!--	</table>
	<table width="80%" cellspacing="0" cellpadding="0" border="0"  > -->
      <tr class="browserEvenRow">
        <td ><Font class="<%=fmakeGrey%>"><%=LC.L_Fld_Name%><%--Field Name*****--%></font><FONT class="Mandatory">* </FONT></td>       
        <!-- Commented and Modified by Gopu to fix the Bugzilla Issue #2595 -->
        <td><input type="text" name="name" size="60" MAXLENGTH="1000" value="<%=fldName%>"></td>
        
<!--	<TEXTAREA id="name" name="name" rows="" cols="200" style="width:400;height:100;"><%=fldName%></TEXTAREA> -->
	<input name="nameta" type="hidden" value="<%=fldNameFormat%>"> <!--KM-3941 -->
	</td>
	

	<td><A href="javascript:void(0);" onClick="openPopupEditor('multipleBoxField','name','nameta','Velos',2000)">
	<%=LC.L_Format%></A>
	<% if (fldNameFormat.length()>0)
	{
		%>
		<%if (formatDisable.equals("N")){%>
	<BR><DIV id="dformat"><A href="javascript:void(0);" onClick="processFormat(document.multipleBoxField,'D')"><img src="./images/disableformat.gif" alt="<%=LC.L_Disable_Formatting%><%--Disable Formatting*****--%>" border = 0 align=absbotton></A></DIV>
	<%} else if (formatDisable.equals("Y")){%>
	<BR><DIV id="dformat"><A href="javascript:void(0);" onClick="processFormat(document.multipleBoxField,'E')"><img src="./images/enableformat.gif" alt="<%=LC.L_Enable_Formatting%><%--Enable Formatting*****--%>" border = 0 ></A></DIV>
	<%}%>
	<A href="javascript:void(0);" onClick="processFormat(document.multipleBoxField,'R')"><img src="./images/removeformat.gif" alt="<%=LC.L_Rem_Format%><%--Remove Formatting*****--%>" border = 0 ></A></td>
	<%} %>
	</td>

	</tr>

        <tr class="browserEvenRow">
        <td><Font class="<%=fomakeGrey%>"><%=LC.L_Fld_Id%><%--Field ID*****--%></font><FONT class="Mandatory">* </FONT></td>
        <td><input type="text" name="uniqueId" size = 40 MAXLENGTH = 50 value="<%=fldUniqId%>" <%=fomakeReadOnly%>></td>
        </tr>


            <tr class="browserEvenRow"><td width="30%" ><Font class="<%=fomakeGrey%>">&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Multi_ChoiceType%><%--Multiple Choice Type*****--%> </font></td> <td>
            <% if (fldDataType.equals("")) 
            { %>
            <input type="radio" name="editBoxType" value="MD"  onclick=  " " CHECKED <%=fomakeDisabled%>><Font class="<%=fomakeGrey%>"> <%=LC.L_Drop_Down%><%--Drop Down*****--%></font>
            <input type="radio" name="editBoxType" value="MC"  onclick=  "" <%=fomakeDisabled%>> <Font class="<%=fomakeGrey%>"><%=LC.L_Checkbox%><%--Checkbox*****--%></font>
            <input type="radio" name="editBoxType" value="MR"  onclick=  " " <%=fomakeDisabled%>><Font class="<%=fomakeGrey%>"> <%=LC.L_Radio_Button%><%--Radio Button*****--%></font>
            
            <% } %>
            <% if (fldDataType.equals("MD")) 
            { %>
            <input type="radio" name="editBoxType" value="MD"  onclick=  " "CHECKED <%=fomakeDisabled%>> <Font class="<%=fomakeGrey%>"><%=LC.L_Drop_Down%><%--Drop Down*****--%></font>
            <input type="radio" name="editBoxType" value="MC"  onclick=  " " <%=fomakeDisabled%>> <Font class="<%=fomakeGrey%>"><%=LC.L_Checkbox%><%--Checkbox*****--%></font>
            <input type="radio" name="editBoxType" value="MR"  onclick=  "" <%=fomakeDisabled%>><Font class="<%=fomakeGrey%>"> <%=LC.L_Radio_Button%><%--Radio Button*****--%></font>
            <% } %> 
            <% if (fldDataType.equals("MC")) 
            { %>
            <input type="radio" name="editBoxType" value="MD"  onclick=  " " <%=fomakeDisabled%>> <Font class="<%=fomakeGrey%>"><%=LC.L_Drop_Down%><%--Drop Down*****--%></font>
            <input type="radio" name="editBoxType" value="MC"  onclick=  " " CHECKED <%=fomakeDisabled%>> <Font class="<%=fomakeGrey%>"><%=LC.L_Checkbox%><%--Checkbox*****--%></font>
            <input type="radio" name="editBoxType" value="MR"  onclick=  " " <%=fomakeDisabled%>> <Font class="<%=fomakeGrey%>"><%=LC.L_Radio_Button%><%--Radio Button*****--%>
            <% } %>
            <% if (fldDataType.equals("MR")) 
            { %>
            <input type="radio" name="editBoxType" value="MD"  onclick=  " " <%=fomakeDisabled%>><Font class="<%=fomakeGrey%>"> <%=LC.L_Drop_Down%><%--Drop Down*****--%></font>
            <input type="radio" name="editBoxType" value="MC"  onclick=  " " <%=fomakeDisabled%>><Font class="<%=fomakeGrey%>"><%=LC.L_Checkbox%><%--Checkbox*****--%></font>
            <input type="radio" name="editBoxType" value="MR"  onclick=  " "CHECKED <%=fomakeDisabled%>> <Font class="<%=fomakeGrey%>"><%=LC.L_Radio_Button%><%--Radio Button*****--%></font>

            <% } %>
		</td></tr>

<tr class="browserEvenRow"><td>&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Fld_Chars%><%--Field Characteristics*****--%></td><td>
        <input type="radio" name="isvisible" value="0" <%=fmakeDisabled%> <%if(fldIsVisible.equals("0")||fldIsVisible.equals("")){%> checked <%}%>><Font class="<%=fmakeGrey%>" ><%=LC.L_Visible%><%--Visible*****--%></font>
        
        <input type="radio" name="isvisible" align="center" value="1" <%=fmakeDisabled%><%if(fldIsVisible.equals("1")){%> checked <%}%>><Font class="<%=fmakeGrey%>"><%=LC.L_Hide%><%--Hide*****--%></font>        
        
        <input type="radio" name="isvisible" value="2" <%=fmakeDisabled%><%if(fldIsVisible.equals("2")){%> checked <%}%>><Font class="<%=fmakeGrey%>"><%=LC.L_Disable%><%--Disable*****--%></font>
     </td></tr>
		<tr class="browserEvenRow">
				  <td>&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Label_DispWidth%><%--Label Display Width*****--%></td>
				  <td><input type="text" name="labelDisplayWidth" size = 3 MAXLENGTH = 3 value="<%=labelDisplayWidth%>">%

              <%if(fldExpLabel.equals("1")){%>
         <input type="checkBox" name="expLabel" value="1" checked <%=fmakeDisabled%>><%=LC.L_Expand_Label%><%--Expand Label*****--%>
              <%}else{%>
         <input type="checkBox" name="expLabel" value="1" <%=fmakeDisabled%>><%=LC.L_Expand_Label%><%--Expand Label*****--%>
              <%}%>

			  		
			  	<%if(hideLabel.equals("1")){%>
		  <input type="checkBox" name="hideLabel" value="1" checked><%=LC.L_Hide_Label%><%--Hide Label*****--%>
			  <%}else{%>
		  <input type="checkBox" name="hideLabel" value="1" ><%=LC.L_Hide_Label%><%--Hide Label*****--%>
			  <%}%>  

    <%if (  ( fldBold.equals("-1") )  || ( fldBold.equals("0" ) ) ||  (fldBold.equals("") )    )
           {%>
            <input type="checkbox" name="bold" value="1" <%=fmakeDisabled%>><Font class="<%=fmakeGrey%>"><%=LC.L_Bold%><%--Bold*****--%></font>
         <%}%>
         <% if (  fldBold.equals("1")  ) 
           {%>
            <input type="checkbox" name="bold" value="1" CHECKED <%=fmakeDisabled%>><Font class="<%=fmakeGrey%>"><%=LC.L_Bold%><%--Bold*****--%></font>
         <%}%>
        <%if (  ( fldItalics.equals("-1") ) || (fldItalics.equals("0" )  ) ||  ( fldItalics.equals("") )     )
           {%>
            <input type="checkbox" name="italics" value="1" <%=fmakeDisabled%>><Font class="<%=fmakeGrey%>"><%=LC.L_Italics%><%--Italics*****--%></font>
         <%}%>
         <% if (   fldItalics.equals("1")  ) 
           {%>
            <input type="checkbox" name="italics" value="1" CHECKED <%=fmakeDisabled%>><Font class="<%=fmakeGrey%>"><%=LC.L_Italics%><%--Italics*****--%></font>
         <%}%>


				  </td>
		</tr>	



         <tr class="browserEvenRow">
        <td ><Font class="<%=fmakeGrey%>">&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Fld_Desc%><%--Field Description*****--%></font></td>
        <td ><input type="text" name="description" size = 60 MAXLENGTH = 500 value="<%=fldDesc%>" <%=fmakeReadOnly%>> </td>
        </tr>
	<tr class="browserEvenRow">
        <td ><font class="<%=fomakeGrey%>">&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Search_Keyword_S%><%--Search Keyword(s)*****--%></font> <img src="../images/jpg/help.jpg" onmouseover="return overlib('<%=MC.M_EtrOneOrMore_Kwrd%><%--Enter one or more keywords separated by a comma for use in querying.*****--%>',CAPTION,'<%=LC.L_Keyword_Help%><%--Keyword Help*****--%>');" onmouseout="return nd();"></td>
        <td ><input type="text" name="keyword" size = 60 MAXLENGTH = 255 value="<%=fldKeyword%>" <%=fomakeReadOnly%>>
	</td>

        
<!--    </table>
    <table width="40%" cellspacing="0" cellpadding="0" border="0"  align="right" > -->
    
          <!-- ************ @Gyanendra Update for Bug #4421 ************/ -->  
          
            <tr class="browserEvenRow"><td ><label>
            <Font class="<%=fmakeGrey%>">&nbsp;&nbsp;&nbsp;&nbsp;<%=MC.M_FldHelp_OnMouse%><%--Field Help <I> (On Mouse Over)</I>*****--%></font></I></label></td>
	<td > <textarea  id="fldinst" name="instructions" 
	cols="40" rows="3" MAXLENGTH=4000 ><%=fldInstructions%></textarea>
	<font class="Mandatory"><div id="countfldinst"><%=MC.M_Limit4000_CharLeft %></div>
			</td></tr>
             
             <!-- ************ Update for Bug #4421 end************/ -->
             
<!--    </table>
    <table  width="50%" cellspacing="0" cellpadding="0" border="0" > -->
        <%//Commented as per UCSF requirements*/%>
    <%//Now uncommented as per 19th April enhancements     %>
    <tr class="browserEvenRow"><td width="40%" ><Font class="<%=fmakeGrey%>"> &nbsp;&nbsp;&nbsp;&nbsp;<%=MC.M_AlignFld_DispName%><%--Align (Field Display Name)*****--%></font> </td><td>
    <% if  (  (fldAlign.equals("left")  )  || (  fldAlign.equals("") )   )
        { %>
        <input type="radio" name="align" value="left"  onclick=  " " CHECKED <%=fmakeDisabled%>><Font class="<%=fmakeGrey%>"><%=LC.L_Left%><%--Left*****--%></font>
        <input type="radio" name="align" value="right"  onclick=  " " <%=fmakeDisabled%>><Font class="<%=fmakeGrey%>"><%=LC.L_Right%><%--Right*****--%></font> 
        <input type="radio" name="align" value="center"  onclick=  " " <%=fmakeDisabled%>><Font class="<%=fmakeGrey%>"><%=LC.L_Center%><%--Center*****--%></font> 
		<input type="radio" name="align" value="top"  onclick=  " " <%=fmakeDisabled%>><Font class="<%=fmakeGrey%>"><%=LC.L_Top%><%--Top*****--%></font> 		
        <%}   if (   fldAlign.equals("right")   ) 
        {%>
        <input type="radio" name="align" value="left"  onclick=  " " <%=fmakeDisabled%>><Font class="<%=fmakeGrey%>"><%=LC.L_Left%><%--Left*****--%></font>
        <input type="radio" name="align" value="right"  onclick=  " " CHECKED <%=fmakeDisabled%>><Font class="<%=fmakeGrey%>"><%=LC.L_Right%><%--Right*****--%></font>
        <input type="radio" name="align" value="center"  onclick=  " " <%=fmakeDisabled%>><Font class="<%=fmakeGrey%>"><%=LC.L_Center%><%--Center*****--%></font>
        <input type="radio" name="align" value="top"  onclick=  " " <%=fmakeDisabled%>><Font class="<%=fmakeGrey%>"><%=LC.L_Top%><%--Top*****--%></font> 		
        <%} if (   fldAlign.equals("center")   ) 
        {%>
        <input type="radio" name="align" value="left"  onclick=  " " <%=fmakeDisabled%> ><Font class="<%=fmakeGrey%>"><%=LC.L_Left%><%--Left*****--%></font>
        <input type="radio" name="align" value="right"  onclick=  " " <%=fmakeDisabled%>><Font class="<%=fmakeGrey%>"><%=LC.L_Right%><%--Right*****--%></font>
        <input type="radio" name="align" value="center"  onclick=  " " CHECKED <%=fmakeDisabled%>><Font class="<%=fmakeGrey%>"><%=LC.L_Center%><%--Center*****--%></font>
        <input type="radio" name="align" value="top"  onclick=  " " <%=fmakeDisabled%>><Font class="<%=fmakeGrey%>"><%=LC.L_Top%><%--Top*****--%></font> 		
        <%} if (   fldAlign.equals("top")   ) 
        {%>
        <input type="radio" name="align" value="left"  onclick=  " " <%=fmakeDisabled%> ><Font class="<%=fmakeGrey%>"><%=LC.L_Left%><%--Left*****--%></font>
        <input type="radio" name="align" value="right"  onclick=  " " <%=fmakeDisabled%>><Font class="<%=fmakeGrey%>"><%=LC.L_Right%><%--Right*****--%></font>
        <input type="radio" name="align" value="center"  onclick=  " "  <%=fmakeDisabled%>><Font class="<%=fmakeGrey%>"><%=LC.L_Center%><%--Center*****--%></font>
        <input type="radio" name="align" value="top"  onclick=  " " CHECKED <%=fmakeDisabled%>><Font class="<%=fmakeGrey%>"><%=LC.L_Top%><%--Top*****--%></font> 		
        <%}  if (  fldAlign.equals("-")  )
         {%>
        <input type="radio" name="align" value="left"  onclick=  " " <%=fmakeDisabled%>><Font class="<%=fmakeGrey%>"><%=LC.L_Left%><%--Left*****--%></font>
        <input type="radio" name="align" value="right"  onclick=  " " <%=fmakeDisabled%>><Font class="<%=fmakeGrey%>"><%=LC.L_Right%><%--Right*****--%></font> 
        <input type="radio" name="align" value="center"  onclick=  " " <%=fmakeDisabled%>><Font class="<%=fmakeGrey%>"><%=LC.L_Center%><%--Center*****--%></font>
        <input type="radio" name="align" value="top"  onclick=  " " <%=fmakeDisabled%>><Font class="<%=fmakeGrey%>"><%=LC.L_Top%><%--Top*****--%></font> 		
        <%}%> 
        <%if (  ( samLinePrevFld.equals("-1")  ) || ( samLinePrevFld.equals("0" ) ) ||  (samLinePrevFld.equals("") ) )
           {%>
            <input type="checkbox" name="sameLine" value="1" <%=fmakeDisabled%>><Font class="<%=fmakeGrey%>"><%=MC.M_SameLine_AsPrevFld%><%--Same line as previous field*****--%></font>
         <%}%>
         <% if (   samLinePrevFld.equals("1")  ) 
           {%>
            <input type="checkbox" name="sameLine" value="1" CHECKED <%=fmakeDisabled%>><Font class="<%=fmakeGrey%>"><%=MC.M_SameLine_AsPrevFld%><%--Same line as previous field*****--%></font>
         <%}%>

	</td></tr>  





        


<!--    </table>
        <table width="60%" cellspacing="0" cellpadding="0" border="0"> -->


<!--        <table width="60%"> -->




<!--        </table>
        <table width="60%" cellspacing="0" cellpadding="0" border=" 0"> -->
       
        <tr class="browserEvenRow">
            <td width="20%"><Font class="<%=fmakeGrey%>">&nbsp;&nbsp;&nbsp;&nbsp;<%=MC.M_ChkBox_RadioDispBtn%><%--Checkbox/Radio Button Display*****--%></font></td>
            <% if (   mode.equals("N")  )  
                { %>
                <td width= "20%"><input type="text" name="colcount" size =2 MAXLENGTH = 2 value="1" <%=fmakeReadOnly%> ><Font class="<%=fmakeGrey%>"><%=LC.L_Columns%><%--Columns*****--%></font></td>
            <%} else 
                {%>
                <td width= "20%"><input type="text" name="colcount" size =2 MAXLENGTH = 2 value="<%=fldColCount%>" <%=fmakeReadOnly%>> <Font class="<%=fmakeGrey%>"><%=LC.L_Columns%><%--Columns*****--%></font></td>
            <%}%>    
        </tr></table>


	</table>
		<br>
		<table width="100%" cellspacing="0" cellpadding="0" border="0">
		<tr><td ><p class="sectionHeadingsFrm"><%=LC.L_Fld_Validations_Upper%><%--FIELD VALIDATIONS*****--%> </p></td>		
		<td colspan="2">
        <%if (  ( fldMandatory.equals("-1") ) || (fldMandatory.equals("0" )  ) ||  ( fldMandatory.equals("") )     )
           {%>
             <input type="checkbox" name="mandatory" value="1" <%=fmakeDisabled%>><%=LC.L_Mandatory%><%--Mandatory*****--%>
         <%}%> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         <% if (   fldMandatory.equals("1")  ) 
           {%>
                <input type="checkbox" name="mandatory" value="1" CHECKED <%=fmakeDisabled%>><%=LC.L_Mandatory%><%--Mandatory*****--%>
         <%}%>
		 	 
		 	 <%if(mandatoryChk.equals("1")){%>
		 	<input type = "checkBox" name="overRideMandatory" value = "1" checked><%=LC.L_Override_MandatoryValidation%><%--Override Mandatory Validation*****--%>
		 		<%}else{%>
		 	<input type = "checkBox" name="overRideMandatory" value = "0" <%if(fldUniqId.equals("er_def_date_01")){%>disabled<%}%>><%=LC.L_Override_MandatoryValidation%><%--Override Mandatory Validation*****--%>
		 	<%}%>

	</td></tr>
			
	</table>

	<table width="90%" cellspacing="0" cellpadding="0" border=" 0">
	<tr><br/></tr>
		<tr><td colspan="7"> <%=LC.L_Multi_ChoiceResponses_Upper%><%--MULTIPLE CHOICE: RESPONSES*****--%> </td></tr>
		 <tr>
		 <td>
	 <%if (sortOrder.length()>0){%>
	  <input type="checkBox" name="sortOrder" value="<%=sortOrder%>" checked>
	  <%} else{%>
	  <input type="checkBox" name="sortOrder" value="A"> 
	  <%}%> <%=MC.M_AutoSeq_Alpha%><%--Auto Sequence(alphabetically)*****--%>

		  <%if(hideResponseLabel.equals("1")){%>
		  <input type="checkBox" name="hideResponseLabel" value="1" checked>
			  <%}else{%>
		  <input type="checkBox" name="hideResponseLabel" value="1" >
			  <%}%>   <%=LC.L_Hide_Responses%><%--Hide Responses*****--%> 
			  	<%if(responseAlign.equals("top")){%>
		  <input type="checkBox" name="responseAlign" value="top" checked>
			  <%}else{%>
		  <input type="checkBox" name="responseAlign" value="top" >
			  <%}%>  
			  <%=LC.L_Top_AlignResponses%><%--Top Align Responses*****--%>


	 </td></tr>
    </table>
    


        
  	<table width="90%" cellspacing="0" cellpadding="0" border=" 0">
		<tr><br/></tr>
		<tr><td colspan="2"><P class="sectionHeadingsFrm"> <%=LC.L_Multi_ChoiceResponses%><%--Multiple Choice: Responses*****--%></P></td>
			<td>
       <!-- <td ><P class="defComments">Add More Responses </P></td>
      <td><input class="<%=finputClass%>" type="text" name="moreRespNo" size =3 MAXLENGTH = 3 value="<%=fldMoreColumns%>" <%=fmakeReadOnly%>></td>
      <td colspan="1"><A href=# onclick="refreshPage(document.multipleBoxField)">Refresh</A></td> -->
      <td colspan="2" align="right"><A href=# onclick="removeDefault(document.multipleBoxField,<%=totRows%>)"><%=LC.L_Rem_Default%><%--Remove Default*****--%></A></td>
  </tr>
  		  <tr><td>&nbsp;</td></tr>

  <tr>
    <th width="10%"><%=LC.L_Sequence%><%--Sequence*****--%> <FONT class="Mandatory">* </FONT></th>
    <th width="20%"><%=LC.L_Display_Val%><%--Display Value*****--%> <FONT class="Mandatory">* </FONT></th>
    <th width="20%"><%=LC.L_Data_Value%><%--Data Value*****--%></th>
    <th width="10%"><%=LC.L_Score%><%--Score*****--%></th>
    <th width="10%"><%=LC.L_Default%><%--Default*****--%></th>
    <%if(mode.equals("M")) { 
            %>
            	<th width="10%"><%=LC.L_Hidden%><%--Hidden*****--%></th>
<!-- Bug#9842 16-May-2012 -Sudhir-->
    <th width="10%"><%=LC.L_Delete%><%--Delete*****--%></th>
    <%
      }%>  
  </tr>
  
 <%
  //modify mode 
 
 if (mode.equals("M"))
{

 for(int i=0;i<totRows;i++) 
  {
    fldRespId = ((Integer)arrRespId.get(i)).intValue();
    seq = ((Integer)arrSeq.get(i)).intValue();
    dispVal = (String)arrDispVal.get(i);
    
    dataVal = (String)arrDataVal.get(i);
    dataVal = (dataVal == null)?"":( dataVal) ;
    
    respRecordType = (String)arrRecordType.get(i);
    if (StringUtil.isEmpty(respRecordType) || respRecordType.equals("0"))
    {
    	respRecordType = "N";
    }
    
    score = (String)arrScore.get(i);    
    if (EJBUtil.isEmpty(score))
		score = "";
	
    defVal = ((Integer)arrDefaultVal.get(i)).intValue();
    offLine = arrOffline.get(i).toString();
    
    

    
    if(offLine.equals("1"))
      {
                    makeReadOnly = "readonly";
                    inputClass = "inpGrey";
                    
      }
    
    else {
      
             inputClass = "inpDefault";
             makeReadOnly = "";		
      
      }
          
		 
 %>                    
      <tr> 
        <td>      
         <input class="<%=inputClass%>" name="fldRespId" type="hidden" value=<%=fldRespId%> >
           <input class="<%=inputClass%>" type="text" name="txtSeq" size = 5 MAXLENGTH = 50 value="<%=seq%>"  <%=makeReadOnly%>>
        </td>
    
        <td> 
        <input <%if(codeStatus.equals("Freeze") || offLine.equals("1")) {%>class="inpGrey"<%}%>="text" name="txtDispVal" size = 20 MAXLENGTH = 100 value="<%=dispVal%>" <%if(codeStatus.equals("Freeze") || offLine.equals("1")) {%>readOnly<%}%>>
        </td>
      
         <td>
         <input class="inpDefault" type="text" name="txtDataVal" size = 20 MAXLENGTH = 100 value="<%=dataVal%>"  >
        </td>
        
       <td> 
         <input class="inpDefault" type="text" name="txtScore" size = 10 MAXLENGTH = 20 value="<%=score%>">
        </td>
      
      <td> 
           <% if (defVal==1)     {   %>   
             <input type="radio" name="rdDefault" size = 20 MAXLENGTH = 50  CHECKED onclick="setHdDefVal(document.multipleBoxField,<%=totRows%>)" >            
         <%} else {%>
              <input type="radio" name="rdDefault" size = 20 MAXLENGTH = 50 onclick="setHdDefVal(document.multipleBoxField,<%=totRows%>)" >
         <%}%>
         
         <input type="hidden" name="hdDefault" value=<%=defVal%>> 
      </td>
      <td>
      		 
      		 
      		     <%if(respRecordType.equals("H")){%>
         <input type="checkBox" name="respRecordTypeCheck" value="H" checked onclick="setFldRespRecordTypeVal(document.multipleBoxField,<%=totRows%>)"  <%=fmakeDisabled%> >
              <%}else{%>
         <input type="checkBox" name="respRecordTypeCheck" value="M" onclick="setFldRespRecordTypeVal(document.multipleBoxField,<%=totRows%>)" <%=fmakeDisabled%>>
              <%}%>

      		  
      </td>		 
         
      <td> 
    <% if(!(mode.equals("RO")) && ((pageRight == 6 && mode.equals("M") ) || (pageRight == 5 && mode.equals("N")) || pageRight == 7)){
            if((calledFrom.equals("L") && !codeStatus.equals("WIP")) ||
                ( (calledFrom.equals("St") || calledFrom.equals("A")) && (codeStatus.equals("Lockdown") || codeStatus.equals("Deactivated") || codeStatus.equals("Active"))))  {}
            else{%>
          <A href="fldrespdelete.jsp?fieldRespId=<%=fldRespId%>&formId=<%=formId%>&formFldId=<%=formFldId%>&codeStatus=<%=codeStatus%>&calledFrom=<%=calledFrom%>&lnkFrom=<%=lnkFrom%>&studyId=<%=studyId%>" onClick="return confirmBox('<%=StringUtil.escapeSpecialCharJS(dispVal)%>',<%=pageRight%>,<%=totRows%>,'<%=offLine%>')"><%=LC.L_Delete%><%--Delete*****--%></A>
        <%}
      }%>        
      </td>
        <%if(mode.equals("N")) { 
            %>
        <input type="hidden" name="hdRecordType" value="N">
            
        <% } else if(mode.equals("M")) {
            %>
        <input type="hidden" name="hdRecordType" value="<%=respRecordType%>"> <!-- modified by Sonia Abrol, 11/17/06, to implement hide resp-->
            
        <% } else if(mode.equals("D")) {
           %>
        <input type="hidden" name="hdRecordType" value="<%=respRecordType%>">
             
    </tr>
        
        <%
        	} //else if
			} //for loop
    

} //mode=M
int seqVal = 0;
if((mode.equals("M")) && (refresh.equals("true")))  { 
    FldRespDao fldRespDao = new FldRespDao();
    fldRespDao = fieldRespB.getMaxSeqNumForMulChoiceFld(EJBUtil.stringToNum(formFldId));
    seqVal  = EJBUtil.stringToNum(fldRespDao.getMaxSeq());    


}
    
  if ((mode.equals("N")) || (refresh.equals("true")))   
  {
      int seqNum = 0;
  //new mode
 //defRespNo should come from XML
    defRespNo = fldRespCount;
    

  if (refresh.equals("true")) 
  {
        defRespNo = moreRespNo;  
  %>
      <Input type="hidden" name="respFromRefresh" value="true" >
  <%
  }
  
 for(int i=0;i<defRespNo;i++) 
 {%>
    <tr> 
        <td> 
           <input type="text" name="newTxtSeq" size = 5 MAXLENGTH = 50 <%if((mode.equals("N")) && !(refresh.equals("true"))){%>value=<%=++seqNum%><%}else if((mode.equals("M")) && (refresh.equals("true"))){%>value=<%=++seqVal%><%}%>>
        </td>
    
        <td>
        <input type="text" name="newTxtDispVal" size = 20 MAXLENGTH = 100 value="">
        </td>
      
         <td>		
		 <input type="text" name="newTxtDataVal" size = 20 MAXLENGTH = 100 value="">
        </td>
        
       <td> 
         <input type="text" name="newTxtScore" size = 10 MAXLENGTH = 50 value="0"></input>
        </td>
      
      <td>
         <%if ((i==0)&& (mode.equals("N"))) {%> 
         <input type="radio" name="newRdDefault"  size = 20 onclick="setDefVal(document.multipleBoxField,<%=defRespNo%>,<%=totRows%>)">
         <%}else {%>
         <input type="radio" name="newRdDefault"  size = 20 onclick="setDefVal(document.multipleBoxField,<%=defRespNo%>,<%=totRows%>)">
         <%}%>         
         <input type="hidden" name="newHdDefault" value="0">        
     </td>
          <input type="hidden" name="newHdRecordType" value="N">        
      <td> &nbsp;
     </td>
    </tr>
 
 
<% } //loop for despRespNo 

%> 
<% 
 } //mode=N
%>

  </table>
 <input type="hidden" name="totRows" value=<%=totRows%>>         
 <input type="hidden" name="defRespNo" value=<%=defRespNo%>>
    
    
   <table width="80%">
		<td width="28%"><P class="defComments"><%=MC.M_Add_MoreRespRows%><%--Add More Response Rows*****--%></P></td>
		 <td width="10%"><input class="<%=finputClass%>" type="text" name="moreRespNo" size =3 MAXLENGTH = 3 value="<%=fldMoreColumns%>" <%=fmakeReadOnly%>></td>
      <td width="60%"><% String Hyper_Link = "<A href=# onclick=\"refreshPage(document.multipleBoxField)\">"+LC.L_Refresh+"</A>"; Object[] arguments = {Hyper_Link}; %><%=VelosResourceBundle.getMessageString("M_EtrClk_Refresh",arguments) %> 
      <%--Enter the desired number of new rows in the box to the left, your e-Signature below and then click <A href=# onclick="refreshPage(document.multipleBoxField)">Refresh</A>*****--%></td>
         </table>        
    <br>
    <%
        
    //KM-to fix Bug2265 
    if(!(mode.equals("RO")) && ((pageRight == 6 && mode.equals("M") ) || (pageRight == 5 && mode.equals("N")) || pageRight == 7) || mode.equals("N") ){
    	// #5437,5439 02/02/2011 @Ankit
            if( ( (calledFrom.equals("St") || calledFrom.equals("A") || calledFrom.equals("L")) && (codeStatus.equals("Lockdown") || codeStatus.equals("Deactivated") ||  codeStatus.equals("Active") ||  codeStatus.equals("Freeze"))))  {}
            else{if(codeStatus.equals("WIP")){
						%>
						<table align="center"><tr>
	  <td align="center">
				<button type="submit" id="submit_btn" ondblclick="return false"><%=LC.L_Submit%></button>
			</td> 
	  </tr></table>
     
<%}else{ 
%>
	<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="editBoxFieldFrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include> 
<br>

 	 <table width="100%" cellspacing="0" cellpadding="0">
      <td align=right> 

      </td> 
      </tr>
  </table>
        <script language="javascript">
		<!-- Modified By Parminder singh Bug #10726-->
         enableSubmit(true);
		</script>
    <%}
    }
   } 
  %>
     <br>

      <table width="100%" cellspacing="0" cellpadding="0">
      <td align=right> 
    <!--    <input type="image" src="../images/jpg/Submit.gif" align="absmiddle" border="0" >-->
      </td> 
      </tr>
  </table>
     <input type ="hidden" name="editBoxType" value=<%=fldDataType%>>    
      <%
          if (fmakeDisabled.equals("disabled")) {%>          
         
          <input type ="hidden" name="isvisible" value=<%=fldIsVisible%>>          
            <input type ="hidden" name="expLabel" value=<%=fldExpLabel%>>
          <input type ="hidden" name="mandatory" value=<%=fldMandatory%>>
          <input type ="hidden" name="align" value=<%=fldAlign%>>
          <input type ="hidden" name="sameLine" value=<%=samLinePrevFld%>>
          <input type ="hidden" name="bold" value=<%=fldBold%>>
          <input type ="hidden" name="italics" value=<%=fldItalics%>>
      <%}%>               
  
 
 
 
 
 
  </Form>
  

<%                
            
    } //end of if body for page right
    else
    {
%>
 <jsp:include page="accessdenied.jsp" flush="true"/> 
<%

    } //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
<%

}

%>
 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</body>

</html>

<Link Rel=STYLESHEET HREF='./styles/velos_popup.css' type=text/css>






