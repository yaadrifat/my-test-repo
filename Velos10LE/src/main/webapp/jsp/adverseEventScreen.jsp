<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<html>
<head>
<title> <%=MC.M_MngPat_AdvEvtDets%><%-- Manage Patient >> Adverse Event Details*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<!--Important for Inner response Query design on 24/04/2015 By Bindu -->

<style type="text/css">
.ui-dialog .ui-dialog-titlebar-close span
{
padding : 0 0 0 0 ;
}
</style>


</head>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="eventAssocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="patB" scope="request" class="com.velos.eres.web.patStudyStat.PatStudyStatJB"/>

<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.esch.business.common.*"%>

<%@page import="com.velos.esch.business.common.SchCodeDao"%>
<%@page import="com.velos.eres.web.user.ConfigFacade"%>
<%@page import="com.velos.eres.web.user.ConfigDetailsObject"%>
<jsp:useBean id="adveventB" scope="request" class="com.velos.esch.web.advEve.AdvEveJB"/>
<% 
String advevntid= request.getParameter("adveventId");
String sub_type=request.getParameter("sub_type");
String p_key = request.getParameter("pkey");
String patprotid=(String) request.getParameter("patProtId");
String statId=request.getParameter("statid");
String stdyid=request.getParameter("studyId");

String src= request.getParameter("srcmenu");
/*dashBoard parameter  is comming form DashBoard(Jupiter Enhancement) in case of Adverse Event Tab(Edit Button)*/
   String dashBoard=request.getParameter("dashBoard");
    System.out.println("Dashboard="+dashBoard);
%>

<%--Include JSP --%>
<jsp:include page="adverseEventScreenInclude.jsp" flush="true">
<jsp:param name="dashBoard" value="<%=dashBoard %>"/>
<jsp:param name="sub_type" value="<%=sub_type %>"/>
<jsp:param name="adveventId" value="<%=advevntid%>"/>
<jsp:param name="pkey" value="<%=p_key%>"/>
<jsp:param name="patProtId" value="<%=patprotid %>"/>
<jsp:param name="statid" value="<%=statId %>"/>
<jsp:param name="studyId" value="<%=stdyid %>"/>
</jsp:include>
<%----%>
<%-- Remove the menu panel form Dashboard
:Vivek Kumar Jha
 --%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
<jsp:param name="dashBoard" value="<%=dashBoard%>"/>
</jsp:include>

<!--  New Enhancenent -->
<input type="hidden" id="dashboard" value=<%=dashBoard%> />
	


  <%
  String studyId="";
   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession)){
		int pageRight = 7;
		int orgRight = 0;
		/* Open the Adverse Event form from dashBoard(Jupiter Enhancement) without open any Study at first 
		:Vivek Kumar Jha
		:
		*/
		if(dashBoard==null || dashBoard.equals("")){
		 studyId = (String) tSession.getAttribute("studyId");
		 }else{
			 studyId =request.getParameter("studyId");
		 } /* End of Change*/
		String userIdFromSession = (String) tSession.getAttribute("userId");
		
		//GET STUDY TEAM RIGHTS
		TeamDao teamDao = new TeamDao();
		teamDao.getTeamRights(StringUtil.stringToNum(studyId),StringUtil.stringToNum(userIdFromSession));
		ArrayList tId = teamDao.getTeamIds();
		if (tId.size() == 0) {
			pageRight=0 ;
	    }else {
			stdRights.setId(StringUtil.stringToNum(tId.get(0).toString()));

		 	ArrayList teamRights = new ArrayList();
			teamRights = teamDao.getTeamRights();

			stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
			stdRights.loadStudyRights();

    	if ((stdRights.getFtrRights().size()) == 0){
    	 	pageRight= 0;
    	}else{
    		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYMPAT"));
    	}
    }
		String studyRoleCodePk = (String)teamDao.getTeamRoleIds().get(0); // Enhancement Form :: 22620
	
	String adveventId= request.getParameter("adveventId");
		
	String protocolId = "";
	String mode=request.getParameter("mode");
	String statid=request.getParameter("statid");
	String accId = (String) tSession.getAttribute("accountId");
	String studyVer = request.getParameter("studyVer");
	String studyNum = request.getParameter("studyNum");
	String patientId = "";
	String patProtId=(String) request.getParameter("patProtId");
	String eventId=(String) request.getParameter("eventId");
	String pkey = request.getParameter("pkey");
	String patStatSubType = "";
	String patStudyStat =  "";
	String patStudyId;

	int patStudyStatpk = 0;
	patEnrollB.setPatProtId(StringUtil.stringToNum(patProtId));
	patEnrollB.getPatProtDetails();
	protocolId =patEnrollB.getPatProtProtocolId();
	patStudyId = patEnrollB.getPatStudyId();
	String protName = "";
	if(protocolId != null) {
		eventAssocB.setEvent_id(StringUtil.stringToNum(protocolId));
		eventAssocB.getEventAssocDetails();
		protName = 	eventAssocB.getName();
	}

	String studyTitle = "";
	String studyNumber = "";
	studyB.setId(StringUtil.stringToNum(studyId));

	studyB.getStudyDetails();
	studyTitle = studyB.getStudyTitle();
	studyNumber = studyB.getStudyNumber();
	int fdaStudy = ("1".equals(studyB.getFdaRegulatedStudy()))? 1:0;

	int siteId = 0;
	String siteName = "";

	String advName = "";
	String grade = "";
	String gradeText = "";

	if(mode.equals("N"))
	{

	}else	{

		//get current patient status and its subtype
		patStatSubType = patB.getPatStudyCurrentStatusWithCodeSubType(studyId, pkey);
		patStudyStat = patB.getPatStudyStat();
		patStudyStatpk = patB.getId();
		statid = patStudyStat ;
		if (StringUtil.isEmpty(patStatSubType))
			patStatSubType= "";


		userB.setUserId(StringUtil.stringToNum(userIdFromSession));
        userB.getUserDetails();
		if (StringUtil.stringToNum(userB.getUserAccountId()) != StringUtil.stringToNum(accId)) {
%>	<body>
		<jsp:include page="accessdenied.jsp" flush="true"/>
	   	<div class = "myHomebottomPanel">
		<jsp:include page="bottompanel.jsp" flush="true"/>
		</div>
	</body>
	</html>
<%
			return;
		}

	}
%>
<body style="overflow:auto;">

<%
	
	String uName = (String) tSession.getAttribute("userName");
	int personPK = 0;
	personPK = StringUtil.stringToNum(pkey);
	person.setPersonPKId(personPK);
	person.getPersonDetails();
	patientId = person.getPersonPId();
	siteId = StringUtil.stringToNum(person.getPersonLocation());
	orgRight = userSiteB.getMaxRightForStudyPatient(StringUtil.stringToNum(userIdFromSession), personPK , StringUtil.stringToNum(studyId) );
	if (orgRight > 0){
		orgRight = 7;
	}
	if("jupiter".equals(dashBoard)){
	if(orgRight==0){
		orgRight = 7;
	}
	}
/* Remove the detail of  patId and patStudyId on Adverse Event form Dashboard(Jupiter Enhancement)
:Vivek kumar Jha
*/
if(dashBoard==null||dashBoard.equals("null")){
%>
<DIV class="BrowserTopn" id="div1">
	<jsp:include page="patienttabs.jsp" flush="true">
	<jsp:param name="pkey" value="<%=pkey%>"/>
	<jsp:param name="patientCode" value="<%=patientId%>"/>
	<jsp:param name="patProtId" value="<%=patProtId%>"/>
	<jsp:param name="studyId" value="<%=studyId%>"/>
	<jsp:param name="studyVer" value="<%=studyVer%>"/>
	<jsp:param name="studyNum" value="<%=studyNum%>"/>
	</jsp:include>
</div>
<%} %>
<SCRIPT LANGUAGE="JavaScript">




	 /*   To check whether Its jupiter Screen or not?
    Put Span in every field.
    Date:24th April 2015
    Created By:Bindu.
 */

    
	//	if($j('#dashboard').val()== 'jupiter')
	//	{
	//		
	//		$j(".baseTbl tr td:nth-child(2)").append("<span  class='queryStatusFlagDlg'><img border='0' src='./images/redflag.jpg'></span>");
			
			
	//		$j('input:checkbox').parents('tr td:nth-child(2)').find('span').html('');

		//	                 if($j('#aeSection3').length){
            		 
      		           //alert('found');
      		          // $j('#aeSection3').find(".baseTbl tr td:nth-child(2)").find('span').html('');
       	//	          $j('#aeSection3').find('span').html('');
        //             }
             
                // $j(".tdDefault").find('#notifyDate0').find('span').html('');
                
          //      $j("#pgcustomnotified").html("<span class='queryStatusFlagDlg'><img border='0' src='./images/redflag.jpg'></span>");
               // alert("hiiiii");
          //      $j(".sectionHeadings").append("<span class='queryStatusFlagDlg'><img border='0' src='./images/redflag.jpg'></span>");


                
        //       $j(".sectionHeadings").nextAll('td').find('span').html('');
                //$j('input:checkbox').find('[name="addInfo"]').next('td').html('');
                 //$j('input:checkbox').find('[name="advNotify"]').closest('tr').closest('tr').find('td').html("<span>aaa<img border='0' src='./images/redflag.jpg'></span>");
         //        $j(".tdDefault").find('[name^="notifyDate"]').next('span').html('');
                          
                 		        
			
			
         
	   //  }
	//	});


$j(document).ready(function()
{
	
	var screenWidth = screen.width;
	var screenHeight = screen.height;
			if(screenWidth>1280 || screenHeight>1024)
			{
				$j('#div2').css('height','68%');
			}
});


	

	



</SCRIPT>

  <%
  
  //System.out.println("error1 : " + studyId);
  //System.out.println("error2 : " + statid);
  //System.out.println("error3 : " + adveventId);
  //System.out.println("error4 : " + advName);
  //System.out.println("error5 : " + adveventB);
  
  %>
  

<!--  To open new dialog box while clicking on span  -->
<%if("jupiter".equals(dashBoard)) {%>
<script src='./dashboard/js/jquery-1.11.1.js'></script>
<%} %>
<script src='./dashboard/js/jquery-ui-1.11.0.js'></script>

<div id="queryStatusDialog" style="z-index:4">
	<div id='queryStatusDialogHtml'></div>
</div>








   
<%-- reduce the space form top when does open advese event form in dialog box form Dashboard(jupiter Enhancement)
:Vivek Kumar Jha 
--%>
<% if(dashBoard==null||dashBoard.equals("null")){%>
<DIV class="tabFormBotN tabFormBotN_adventnew_1" id="div2">

<table width="100%" >
 <tr>
	<td class=tdDefault width = 20% >
	<%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>: <a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=studyId%>"><%=studyNumber%></a> &nbsp;&nbsp;&nbsp;&nbsp;
	<a href="#" onmouseover="return overlib('<%=StringUtil.escapeSpecialCharJS(studyTitle)%>',CAPTION,'<%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>');" onmouseout="return nd();"><%=LC.L_View_Title%><%--View Title*****--%></a>  &nbsp;&nbsp;&nbsp;&nbsp;
<%if(protocolId != null) {%>
 	<!--Protocol Calendar: <%=protName%>-->
 </tr>
 		<tr height="8"><td></td></tr>
	<%}%>
 </table>
 <%}else{ %>
 <%-- it will work when call form Dashboard(Jupiter Enhancement) in case of Adverse Event form open after the click of edit button :Vivek --%>
<DIV class="tabFormBotN tabFormBotN_adventnew_1" id="div2"  style="display:inline; top:5%; height:95%;">
<%} %>
 <%
//Modified by Manimaran for November Enhancement PS4.
if((patStatSubType.trim().equalsIgnoreCase("lockdown"))){ %>
	<P class = "defComments"><FONT class="Mandatory"><%=MC.M_StdStatLkdwn_CntEdtAdvEvt%><%--<%=LC.Pat_Patient%>'s <%=LC.Std_Study_Lower%> status is 'Lockdown'.You cannot edit the Adverse Event.*****--%> </FONT>
		<A onclick="openWinStatus('<%=pkey%>','<%=patStudyStatpk%>','<%=pageRight%>','<%=studyId%>','<%=orgRight%>')"  href="#"  ><%=LC.L_Click_Here%><%--Click Here*****--%></A>
		<FONT class="Mandatory"> <%=MC.M_ToChg_TheStat%><%--to change the status.*****--%></FONT>
	</P>
<%} %>
<form name="adverseEventScreenForm" id="adverseEventScreenForm" method="post" action="#" onSubmit="if (aeScreenFunctions.validateAEScreen()== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
<%
  	boolean hasAccess = false;
	String acc = (String) tSession.getAttribute("accountId");
	 %>
	<div style="border:1">
	<div id="aeSection1" name="aeSection" class="portlet portletstatus ui-widget ui-widget-border ui-helper-clearfix ui-corner-all">
		<div id="aeTab1content" onclick="toggleDiv('aeTab1')" class="portlet-header portletstatus ui-widget 
		ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all" style="background-color:#CCCCCC;">
			<span class="ui-icon ui-icon-triangle-1-s"></span>
			<%=LC.L_Adverse_Event%>
		</div>
		<div id="aeTab1" width="99%">
			<jsp:include page="advevent_new.jsp" >
				<jsp:param name="adveventId" value="<%=adveventId%>"/>
				<jsp:param name="studyId" value="<%=studyId%>"/>
				<jsp:param name="srcmenu" value="<%=src%>"/>
				<jsp:param name="selectedTab" value="tdMenuBarItem5"/>
				<jsp:param name="page" value="patientEnroll"/>
				<jsp:param name="pkey" value="<%=pkey%>"/>
				<jsp:param name="visit" value="1"/>
				<jsp:param name="patProtId" value="<%=patProtId %>"/>
				<jsp:param name="patientCode" value="<%=patientId%>"/>
				<jsp:param name="includeMode" value="Y"/>
				<jsp:param name="dashBoard" value="<%=dashBoard%>"/>
			</jsp:include>
		</div>
	</div>
	<div id="aeSection2" name="aeSection" class="portlet portletstatus ui-widget ui-widget-border ui-helper-clearfix ui-corner-all">
		<div id="aeTab2content" onclick="toggleDiv('aeTab2');" class="portlet-header portletstatus ui-widget 
		ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all" style="background-color:#CCCCCC;">
			<span class="ui-icon ui-icon-triangle-1-s"></span>
			<%=MC.M_MoreAdvEvt_Dets%>
		</div>
		<div id='aeTab2'  width="99%">
			<jsp:include page="moredetails.jsp" >
				<jsp:param name="modId" value="<%=adveventId%>"/>
				<jsp:param name="modName" value="advtype"/>
				<jsp:param name="includeMode" value="Y"/>
			</jsp:include>
		</div>
	</div>
	<div id="aeSection3" name="aeSection" class="portlet portletstatus ui-widget ui-widget-border ui-helper-clearfix ui-corner-all">
	<div>
	<%
	orgRight = 0;
	personPK = 0;
	patStatSubType = "";
	String dFormStatus = "";

	String advFormStatus = adveventB.getFormStatus();
	accId = (String) tSession.getAttribute("accountId");
	pkey = request.getParameter("pkey");
	studyId = (String) tSession.getAttribute("studyId");
	userIdFromSession = (String) tSession.getAttribute("userId");
	mode = request.getParameter("mode");
    adveventId= request.getParameter("adveventId");
	
	personPK = StringUtil.stringToNum(pkey);
	
	orgRight = userSiteB.getMaxRightForStudyPatient(StringUtil.stringToNum(userIdFromSession), personPK , StringUtil.stringToNum(studyId) );
	studyB.setId(StringUtil.stringToNum(studyId));
	studyB.getStudyDetails();
	fdaStudy = ("1".equals(studyB.getFdaRegulatedStudy()))? 1:0;
	
	ConfigFacade cFacade=ConfigFacade.getConfigFacade();
	HashMap hashPgCustFld = new HashMap();

	ConfigDetailsObject cdoPgField = cFacade.populateObject(Integer.parseInt(accId), "adverseevent");

	if (cdoPgField.getPcfField()!= null && cdoPgField.getPcfField().size() > 0) {
		for (int i=0;i<cdoPgField.getPcfField().size();i++){
			hashPgCustFld.put((String)cdoPgField.getPcfField().get(i), String.valueOf(i));
	    }
	}

	if (orgRight > 0){
		System.out.println("patient adverse new orgRight" + orgRight);
		orgRight = 7;
	}
	if("jupiter".equals(dashBoard)){
		if(orgRight==0){
			orgRight = 7;
		}
		}
	adveventB.setAdvEveId(StringUtil.stringToNum(adveventId));
	adveventB.getAdvEveDetails();
	if(mode.equals("N"))
	{	
	}else{
		patStatSubType = patB.getPatStudyCurrentStatusWithCodeSubType(studyId, pkey);
		if (StringUtil.isEmpty(patStatSubType))
			patStatSubType= "";

	}
	
	String formStatAtt="";
	SchCodeDao cd = new SchCodeDao();
	
	 if (hashPgCustFld.containsKey("formstatus")) {
			int fldNumFormStat = Integer.parseInt((String)hashPgCustFld.get("formstatus"));
			formStatAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumFormStat));
			if(formStatAtt == null) formStatAtt ="";
   }
	//reset cd
	cd.resetObject();

	//cd.getCodeValues("fillformstat");
	cd.getCodeValuesForStudyRole("fillformstat",studyRoleCodePk);// for Enhancement Form 22620.
	
	if (formStatAtt.equals("1") || formStatAtt.equals("2"))
		   dFormStatus =  cd.toPullDown("formStatus",StringUtil.stringToNum(advFormStatus),false);
		else
		   dFormStatus =  cd.toPullDown("formStatus",StringUtil.stringToNum(advFormStatus));
	
		int index=-1;
    String studyLkpDict = studyB.getStudyAdvlkpVer();
    if (StringUtil.isEmpty(studyLkpDict))
		studyLkpDict = "0";
   int studyLkpDictId=StringUtil.stringToNum(studyLkpDict);
		LookupDao lookupDao = new LookupDao();
		lookupDao = studyB.getAllViewsForLkpType(StringUtil.stringToNum(accId) ,"NCI");
		ArrayList viewIds = lookupDao.getLViewIds();
		ArrayList viewNames = lookupDao.getLViewNames();
		String settingValue="";

	 if (studyLkpDictId==0)
	 {
		settingValue="";
	 }
	 else
	 {
	   index=viewIds.indexOf(new Integer(studyLkpDict));
	   settingValue=(String)viewNames.get(index);
	  }

	  if (StringUtil.isEmpty(settingValue))
	  {
	  	settingValue = LC.L_Free_TextEntry;/*settingValue = "Free Text Entry";*****/
	  }
	  %>
<table width="100%" cellspacing="0" cellpadding="0" bgcolor="#cccccc" class= "basetbl midAlign" border="0">
							<%
				//Modified by Manimaran for November Enhancement PS4.
			if ((mode.equals("M") && (pageRight >= 6) && (orgRight >= 6) && (!patStatSubType.equals("lockdown")) ) || (mode.equals("N") && (pageRight == 5  || pageRight == 7 || orgRight == 7) ))
				{
				%>
					<!--tr>
						<td class="tdDefault" width="12%">Form Status</td>
						<td width="13%"><%= dFormStatus %></td-->

				<tr valign="middle">
				 <%if (hashPgCustFld.containsKey("formstatus")) {
						int fldNumFormStat  = Integer.parseInt((String)hashPgCustFld.get("formstatus"));
						String formStatMand = ((String)cdoPgField.getPcfMandatory().get(fldNumFormStat));
						String formStatLable = ((String)cdoPgField.getPcfLabel().get(fldNumFormStat));
						formStatAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumFormStat));

						if(formStatAtt == null) formStatAtt ="";
						if(formStatMand == null) formStatMand ="";

						if(!formStatAtt.equals("0")) {
						if(formStatLable !=null){
						%> <td class="tdDefault" width="12%" >
						<%=formStatLable%>
						<%} else {%> <td class="tdDefault" width="12%" >
						 <%=LC.L_AE_STATUS%><%--Form Status*****--%>
						<%}

					if (formStatMand.equals("1")) {
						%>
					   <FONT class="Mandatory" id="pgcustomformstat">* </FONT>
					<% } %>

				</td>
				<td width="13%">
					<%= dFormStatus %>
					<%if(formStatAtt.equals("2")) {%>
						<input type="hidden" name="formStatus" value="<%=advFormStatus%>">
					<%}%>
				</td>
				<%} else if(formStatAtt.equals("0")) {%>

				<input type="hidden" name="formStatus" value ="<%=advFormStatus%>" >
				<%}} else {%>
				<td class="tdDefault" width="12%"><%=LC.L_AE_STATUS%><%--Form Status*****--%></td>
				<td width="13%"><%= dFormStatus %>
				<%if("jupiter".equals(dashBoard)){ %>
					<span class='queryStatusFlagDlg' id="fld_11037[Velsep]<%=LC.L_AE_STATUS%>" ><img border='0'onclick='openQueryDialog()' src='./images/white.jpg'></span>
					<%} %> 
				</td>

				<%}%>
				<%if ("L2_ON".equals(LC.L_Auth2_Switch)){%>
				<td>&nbsp;</td>
		  		<td width="12%"><%=LC.L_UserName%>&nbsp;<FONT class="Mandatory">* </FONT></td>
		  		<td>
					<input type="password" id="userLogName" name="userLogName" value=""
				  	onkeyup="ajaxvalidate('misc:'+this.id,-1,'userLogNameMessage','<%=MC.M_Valid_UserName%>','<%=MC.M_Invalid_UserName %>','sessUserId')"/>
				  	<span id="userLogNameMessage"></span>
		  		</td>
		  		<%}%>
				<input type="hidden" id="fdaStudy" name="fdaStudy" value="<%=fdaStudy%>">
				<input type= "hidden"  id="currentDictSetting" name="currentDictSetting" value="<%=settingValue%>">
			   </tr>
				<%
				}
				else
				{%>
				<tr>
				<td></td>
				<td></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				</tr>
			 <%}	%>
			
	</table>
	</div>
	</div>
	</div>
	

	
	
	
	
	
	<br>
	<%
	// Start :: enhancement Form 22620
	boolean isSoftLock=cd.isSoftLock(StringUtil.stringToNum(advFormStatus));
	boolean isUnlockAllow=cd.isAllowedUnlock(studyRoleCodePk);
	%>
	<%if  ((mode.equals("N") && StringUtil.isAccessibleFor(pageRight, 'N'))
			||(mode.equals("M") && StringUtil.isAccessibleFor(pageRight, 'E')
				&&(isSoftLock?isUnlockAllow:true)))
			//||(saveDao.isLocked(StringUtil.stringToNum(advFormStatus))) 
// End :: enhancement Form 22620			
			{%>
			
		
	<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="adverseEventScreenForm"/>
		<jsp:param name="showDiscard" value="N"/>
	</jsp:include> 
	<div class = "myHomebottomPanel">
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
	<div class ="mainMenu" id="emenu">
	  	<jsp:include page="getmenu.jsp" flush="true"/>
	</div>

	<%}else{ %>

	<div class = "myHomebottomPanel">
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
	<div class ="mainMenu" id="emenu">
	  	<jsp:include page="getmenu.jsp" flush="true"/>
	</div>


	<%} // End of access right%>
	<input type="hidden" id="studyNumber" value="<%=studyNumber%>"/>
	<input type="hidden" id="patProt_patStdId" value="<%=patStudyId %>"/>
</form>
<script type="text/javascript">

/** To open the response view Dialog Box*/   
	$("#queryStatusDialog").dialog({
	      autoOpen: false,
	      width : '410',
		  height:'175'  
      });
	var aEventField = [];

	$(function(){


		$('.queryStatusFlagDlg').each(function(i, el) {	
				aEventField.push($(this).attr('id'));
		}).delay('1000');
		
		if($j('#dashboard').val()== 'jupiter'){
			var responseId="<%=adveventId%>";
			$j.ajax({
				 url: '/velos/jsp/adversetColorFlagData',
				 type: 'GET',
			     global: true,
			     dataType: 'json',
			     async: false,
			     data:{
				'responseId':responseId
			},
			 cache: false,
		     success: function (data) {
				var redflag,orangeflag,yellowflag,purpleflag;
			     var field_id="";
			     var aEvent=[];
			     var temp=[];
			     for(var i=0;i<data.length;i++){
				     for(var j=0; j<aEventField.length;j++){
					     var idStr=aEventField[j].split('[Velsep]');					     
					     var idOfFld=idStr[0];
					     var tempField='fld_'+data[i].fieldId;   
					  if(tempField==idOfFld){
						  if(field_id!=tempField){
							  redflag=orangeflag=yellowflag=purpleflag=0;
							  if(("Open"==data[i].queryStatus || "Re-opened"==data[i].queryStatus) && "High Priority"==data[i].queryType){
									redflag=i+1;							
									var element = document.getElementById(aEventField[j]);
									var html="<img id='"+data[i].queryId+"' border='0' onclick='"+"openQueryDialog(\""+aEventField[j]+"\")' src='./images/redflag.jpg'>";
									//alert("html"+html);
									// htmlStr=element.innerHTML;
									// htmlStr=htmlStr.replace("white.jpg","redflag.jpg");
									element.innerHTML=html;
									 //$('.queryStatusFlagDlg img').each(function() {
									   //     alert($(this).attr('id'))
									   // });
								}
								else if("Ready for Monitor Review"==data[i].queryStatus && "Normal"==data[i].queryType){
									orangeflag=i+1;
									var element = document.getElementById(aEventField[j]);
									var html="<img id='"+data[i].queryId+"' border='0' onclick='"+"openQueryDialog(\""+aEventField[j]+"\")' src='./images/yellowflg.jpg'>";
									//htmlStr=element.innerHTML;
									 //htmlStr=htmlStr.replace("white.jpg","redflag.jpg");
									 element.innerHTML=html;
								}
								else if("Ready for Monitor Review"==data[i].queryStatus  && "High Priority"==data[i].queryType){
									yellowflag=i+1;
									var element = document.getElementById(aEventField[j]);
									var html="<img id='"+data[i].queryId+"' border='0' onclick='"+"openQueryDialog(\""+aEventField[j]+"\")' src='./images/orangflg.jpg'>";
									//htmlStr=element.innerHTML;
									// htmlStr=htmlStr.replace("white.jpg","orangflg.jpg");
									 element.innerHTML=html;
								}
								else if(("Open"==data[i].queryStatus || "Re-opened"==data[i].queryStatus) && "Normal"==data[i].queryType){
									purpleflag=i+1;
									var element = document.getElementById(aEventField[j]);
									var html="<img id='"+data[i].queryId+"' border='0' onclick='"+"openQueryDialog(\""+aEventField[j]+"\")' src='./images/purplflg.jpg'>";
									//htmlStr=element.innerHTML;
									 //htmlStr=htmlStr.replace("white.jpg","purplflg.jpg");
									 element.innerHTML=html;
								}
							else if("Resolved"==data[i].queryStatus ){
									var element = document.getElementById(aEventField[j]);
									var html="<img id='"+data[i].queryId+"' border='0' onclick='"+"openQueryDialog(\""+aEventField[j]+"\")' src='./images/greenflg.jpg'>";
									//htmlStr=element.innerHTML;
									 //htmlStr=htmlStr.replace("white.jpg","greenflg.jpg");
									 element.innerHTML=html;
								} 
						  }

					 else{						  						  
						if(("Open"==data[i].queryStatus || "Re-opened"==data[i].queryStatus) && "High Priority"==data[i].queryType){
								redflag=i+1;							
								var element = document.getElementById(aEventField[j]);
								var html="<img id='"+data[i].queryId+"' border='0' onclick='"+"openQueryDialog(\""+aEventField[j]+"\")' src='./images/redflag.jpg'>";
								//alert("html"+html);
								// htmlStr=element.innerHTML;
								// htmlStr=htmlStr.replace("white.jpg","redflag.jpg");
								element.innerHTML=html;
								//$('.queryStatusFlagDlg img').each(function() {
							    //     alert($(this).attr('id'))
							    // });
							}
						if(redflag==0){
								if("Ready for Monitor Review"==data[i].queryStatus && "Normal"==data[i].queryType){
									orangeflag=i+1;
									var element = document.getElementById(aEventField[j]);
									var html="<img id='"+data[i].queryId+"' border='0' onclick='"+"openQueryDialog(\""+aEventField[j]+"\")' src='./images/yellowflg.jpg'>";
									//htmlStr=element.innerHTML;
							 		//htmlStr=htmlStr.replace("white.jpg","redflag.jpg");
							 		element.innerHTML=html;
								}
							}
						if(redflag==0 && orangeflag==0){
								if("Ready for Monitor Review"==data[i].queryStatus  && "High Priority"==data[i].queryType){
									yellowflag=i+1;
									var element = document.getElementById(aEventField[j]);
									var html="<img id='"+data[i].queryId+"' border='0' onclick='"+"openQueryDialog(\""+aEventField[j]+"\")' src='./images/orangflg.jpg'>";
									//htmlStr=element.innerHTML;
									// htmlStr=htmlStr.replace("white.jpg","orangflg.jpg");
							 		element.innerHTML=html;
								}
							}
						if(redflag==0 && orangeflag==0 && yellowflag==0){
								if(("Open"==data[i].queryStatus || "Re-opened"==data[i].queryStatus) && "Normal"==data[i].queryType){
									purpleflag=i+1;
									var element = document.getElementById(aEventField[j]);
									var html="<img id='"+data[i].queryId+"' border='0' onclick='"+"openQueryDialog(\""+aEventField[j]+"\")' src='./images/purplflg.jpg'>";
									//htmlStr=element.innerHTML;
									 //htmlStr=htmlStr.replace("white.jpg","purplflg.jpg");
									 element.innerHTML=html;
								}
							}
						if(redflag==0 && orangeflag==0 && yellowflag==0 && purpleflag==0){
								if("Resolved"==data[i].queryStatus ){
									var element = document.getElementById(aEventField[j]);
									var html="<img id='"+data[i].queryId+"' border='0' onclick='"+"openQueryDialog(\""+aEventField[j]+"\")' src='./images/greenflg.jpg'>";
									//htmlStr=element.innerHTML;
							 		//htmlStr=htmlStr.replace("white.jpg","greenflg.jpg");
									 element.innerHTML=html;
								}
							}
						}
						  aEvent.push(j);
						  //aEventField  = aEventField.filter(function(elem){
							//   return elem != aEventField[j]; 
						  //});
						break;
					  }				    
				     }
				     field_id='fld_'+data[i].fieldId;
			     }
			     for(var i=0;i<aEvent.length;i++)
			     {
				     if(aEvent[i-1]==aEvent[i])
					     continue;
				     temp.push(aEvent[i]);
			     } 
			     var showWhiteflag;
			     for(var j=0;j<aEventField.length;j++){
			    	 showWhiteflag=0;
				     for(var i=0;i<temp.length;i++){
							if(temp[i]==j)
							{
								temp.splice(i,1);
								showWhiteflag=1;
								break;
							}		
					     }
				     if(showWhiteflag==0){
				     var element = document.getElementById(aEventField[j]);
					 var html="<img  border='0' onclick='"+"openQueryDialog(\""+aEventField[j]+"\","+"\"queryWin\""+")' src='./images/white.jpg'>";
					 element.innerHTML=html;
				     }
				 }
			},
			error: function (){
		        alert("error");
	        }
			});
		}
					}); /// ready function;


	function openQueryDialog(fieldSysId,windowType){
		$j.ajax({	  
	   		url: "fieldQueryHistory.jsp?formName=adverseForm&filedId="+fieldSysId+"&windowType="+windowType+"&responseId="+"<%=adveventId%>",   		
			success: function(data) {	
				$('#queryStatusDialog').dialog( "open" );			
				$("#queryStatusDialogHtml").html(data);			
				$('#queryStatusDialog').dialog( "open" );		
			}
					
	    	});
	}
	function saveQuery(fieldSysId){
		var fieldStrSplit=fieldSysId.split('[Velsep]');
		var fldStrSplit=fieldStrSplit[0].split('_');
		var fieldId=fldStrSplit[1];
		var responseId="<%=adveventId%>";
		var queryStatus=$("#cmbStatus").val();
		var queryTypeId=$("#cmbQueryResp").val();
		var instructions=$("#instructions").val();
		var formQueryType=2;
		var calledFrom=8;
		if(queryStatus==''){
			alert("Select Status");
		}
		else if(queryTypeId==''){
			alert("Select Query Type");
		}else{
			$.ajax({
				url: '/velos/jsp/patStudyQuerySave',
			    type: 'GET',
		        global: true,
		        dataType: 'json',
		        async: false,
		        data: {
		        'fieldId':fieldId,
		        'formQueryType':formQueryType,
		        'calledFrom':calledFrom,
		        'filledFormId':responseId,
		        'queryStatus':queryStatus,
		        'queryTypeId':queryTypeId,
		        'instructions':instructions
			},
			cache: true,
	        success: function (data) {
		       
		       $('#queryStatusDialog').dialog('close');  
		        location.reload();
		        parent.preview1.location.reload();
	        },
	        error: function (){
		        alert("error");
	        }
			});
		}
	}
	function saveQueryAndResponse(fieldStr,queryId){
		var instructions='';
		var formQueryId='';
		var queryStatus=$("#cmbStatus").val();
		instructions=$("#instructions").val();
		formQueryId =jQuery("input:radio[name=queryRadio]:checked").val();
		var formQueryType=3;
		if(queryStatus!=''){
			if(formQueryId!=undefined){	
			$.ajax({
				    url: '/velos/jsp/saveQueryResponse',
				    type: 'GET',
			        global: true,
			        dataType: 'json',
			        async: false,
			        data: {
				    'formQueryId':formQueryId,
				    'queryStatus':queryStatus,
				    'formQueryType':formQueryType,
				    'instructions':instructions
			        },
			        cache: true,
			        success: function (data) {
				        if(data[0].rowImpact==0){
					        alert("data  not Saved Successfully");
				        }else{
					        $('#queryStatusDialog').dialog('close'); 
					        location.reload();
					        parent.preview1.location.reload(); 
				        }
			        },
			        error: function (){
				        alert("error");
			        }
			});
			}else{alert("Select Any one Selection");}
			}else{
				alert("Select Status");
			}
	}

</script>
<%} //end of if session times out
	else
	{
	%>
		<jsp:include page="timeout.html" flush="true"/>
	<%} %>

</div>
<%--Custom JS inclusion --%>
<script type="text/javascript" src="./js/velosCustom/aeScreen.js"></script>
<%----%>
</body>
</html>
