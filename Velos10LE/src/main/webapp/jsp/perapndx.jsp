<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Patient_Appendix %><%-- <%=LC.Pat_Patient%> >> Appendix*****--%> </title>


<SCRIPT language="javascript">

function confirmBox(fileName,pgRight,orgRight) {
	if (f_check_perm_org(pgRight,orgRight,'E') == true) {
		var paramArray = [fileName];
		if (confirm(getLocalizedMessageString("M_Del_FrmPatApdx",paramArray))) {/*if (confirm("Delete " + fileName + " from <%=LC.Pat_Patient_Lower%> appendix?")) {*****/
		    return true;}
		else
		{
			return false;
		}
	} else {
		return false;
	}
}

function fdownload(formobj,pk,filename,dnldurl)
{
	formobj.file.value = filename;
	formobj.pkValue.value = pk ;
	formobj.dnldurl.value=dnldurl;
	formobj.moduleName.value="Patient";
	formobj.action="postFileDownload.jsp";
	//formobj.action=dnldurl;

	formobj.target = "_filedownload";
	formobj.method = "POST";
	formobj.submit();
}

</SCRIPT>

</head>

<%@ page language = "java" import = "com.velos.eres.business.common.PerApndxDao,java.util.*,com.aithent.file.uploadDownload.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="perApndxB" scope="page" class="com.velos.eres.web.perApndx.PerApndxJB"/>
<jsp:useBean id="personB" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>

<% String src;
src= request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<br>
<body>

<%
	int pageRight = 7;
	int orgRight = 0;
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{
		String patId=request.getParameter("pkey");
		String userIdFromSession = (String) tSession.getValue("userId");
     	int siteId = 0;
		int personPK = EJBUtil.stringToNum(patId);
 	    personB.setPersonPKId(personPK);
	    personB.getPersonDetails();
	    String patientCode = personB.getPersonPId();
	    patientCode = (patientCode==null)?"":patientCode;
		siteId = EJBUtil.stringToNum(personB.getPersonLocation());

		orgRight = userSiteB.getUserPatientFacilityRight(EJBUtil.stringToNum(userIdFromSession), personPK);

		String calledFrom = request.getParameter("calledFrom");
		if (calledFrom == null || calledFrom.equals(""))
		{
			calledFrom = "M"; //i.e to edit patient details as maintenance and not from a study
		}


		GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));



	 	String studyId = null;
 		String statDesc = null;
	 	String statid = null;
 		String studyVer = null;
 		studyId = request.getParameter("studyId");
	 	statDesc = request.getParameter("statDesc");
 		statid = request.getParameter("statid");
	 	studyVer = request.getParameter("studyVer");

		String enrollId = (String) tSession.getValue("enrollId");
%>
<DIV class="BrowserTopn" id="div1">
		<jsp:include page="patienttabs.jsp" flush="true">
		<jsp:param name="pkey" value="<%=patId%>"/>
		<jsp:param name="patientCode" value="<%=StringUtil.encodeString(patientCode)%>"/>
		<jsp:param name="studyId" value="<%=studyId%>"/>
		<jsp:param name="patProtId" value="<%=enrollId%>"/>
		</jsp:include>
</div>
<DIV class="BrowserBotN BrowserBotN_top1" id="div2">


<%
	  if (pageRight > 0 && orgRight >0){
			String mode = request.getParameter("mode");
			String incorrectFile = request.getParameter("incorrectFile");
			incorrectFile = (incorrectFile==null)?"":incorrectFile.toString();
			String space = request.getParameter("outOfSpace");

			space = (space==null)?"":space.toString();

			if(incorrectFile.equals("1")) {
			%>
				<br><br><br><br>
				<table width=100%>
				<tr><td align=center>
				<p class = "sectionHeadings">
					<%=MC.M_FileNotExist_ChkPath %><%-- File does not exist. Please check the file path and name.*****--%>
				</p>
				</td>
				</tr>
				<tr height=20></tr>
				<tr>
				<td align=center>
			<button onclick="window.history.go(-2);"><%=LC.L_Back%></button>
			</td>
			</tr>
		</table>
<%
		}
		else {

			if(space.equals("1")) {

			%>
				<br><br><br><br>
				<table width=100%>
				<tr><td align=center>
				<p class = "sectionHeadings">
				<%=MC.M_UploadSpace_ContAdmin %><%-- The space allotted to you for file uploading has exhausted. Please contact Velos Administrator.*****--%>
				</p>
				</td>
				</tr>

				<tr height=20></tr>
				<tr>
				<td align=center>
			<button onclick="window.history.go(-2);"><%=LC.L_Back%></button>
			</td>
			</tr>
		</table>
<%
				}
				else
				{ //else of if for space.equals("1")


	   	int count = 0;
	    String perApndxId="";
		String perApndxDate="";
   	    String perApndxDesc="";
   	    String perApndxUri = "";
		String perApndxType="";

		ArrayList perApndxIds = null;
		ArrayList perApndxDates =null;
		ArrayList perApndxDescs =null;
		ArrayList perApndxUris=null;
		ArrayList perApndxTypes=null;

		ArrayList perApndxFileIds =null;
		ArrayList perApndxFileDates =null;
		ArrayList perApndxFileDescs =null;
		ArrayList perApndxFileUris=null;

	    String perApndxFileId="";
		String perApndxFileDate="";
   	    String perApndxFileDesc="";
   	    String perApndxFileUri = "";
   	    String FileName        ="";

       %>

	<form METHOD=POST action="" name="per">
<%
if (pageRight == 4 || orgRight == 4) {
%>
   <P class = "defComments"><FONT class="Mandatory"><%=MC.M_YouOnly_ViewPerm %><%-- You have only View permission*****--%></Font></P>
<%}
	     PerApndxDao perApndxDao= perApndxB.getPerApndxUris(EJBUtil.stringToNum(patId));
		 perApndxIds = perApndxDao.getPerApndxIds();
 		 perApndxDates = perApndxDao.getPerApndxDates();
		 perApndxDescs = perApndxDao.getPerApndxDescs();
		 perApndxUris=perApndxDao.getPerApndxUris();
		 perApndxTypes=perApndxDao.getPerApndxTypes();

%>
<!-- Changes By Sudhir for Quick fixes needed after v9.0 UI internal review on 16-Mar-2012 -->
<!--<P class = "defComments"><%=MC.M_FlwWebPgDocu_LnkThisPat %><%-- The following web pages and documents are linked to this <%=LC.Pat_Patient%>*****--%>:<br> </P>-->

<TABLE width="100%">
<tr>
<th ><%=LC.L_My_Links %><%-- My Links*****--%></th>
</tr>
<!--<tr>
<td>
<P class = "defComments"><%//=MC.M_YouCanList_ImplLnkUrl %><%-- You can list your important links. Give full path of your URLs.*****--%>
<br><br> <%//=MC.M_EditLnkDet_ClkNameOpen %><%-- Select [Edit] against the name of a link to edit its details. Click on the name of the link to open it in a new browser window.*****--%></P>
</td>
</tr>-->
<!-- Changes By Sudhir for Quick fixes needed after v9.0 UI internal review on 16-Mar-2012 -->
</table>

<TABLE width="100%">
   <tr>
	<td colspan=3>&nbsp;</td>
	<td width=30% align=right>
<!--Modified by Manimaran to fix the Bug 2387 -->
<A href="addperurl.jsp?patId=<%=patId%>&mode=N&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&studyVer=<%=studyVer%>" onClick="return f_check_perm_org(<%=pageRight%>,<%=orgRight%>,'E')"><%=LC.L_Add_NewUrl%><%-- ADD NEW URL*****--%></A>
	</td>
   </tr>
</TABLE>
<TABLE width="100%" cellspacing="0" cellpadding="0" class="basetbl outline midalign">
	<tr>
		<th width='30%'><%=LC.L_Url_Upper%><%-- URL*****--%></th>
		<th width='35%'><%=LC.L_Description%><%-- Description*****--%></th>
		<th width='15%'><%=LC.L_Date%><%-- Date*****--%></th>
		<th width="15%"><%=LC.L_Edit%><%-- Edit*****--%></th>
		<th width="5%"><%=LC.L_Delete%><%-- Delete*****--%></th>
   </tr>

<%
   for(int i=0;i<perApndxIds.size(); i++) {

	perApndxId= (String)perApndxIds.get(i);
	perApndxDate = (String)perApndxDates.get(i);
	perApndxDesc=(String)perApndxDescs.get(i);
	perApndxUri =(String)perApndxUris.get(i);
	perApndxType=(String)perApndxTypes.get(i);
	if ((i%2)!=0) {
  %>
      <tr id="browserEvenRow">
        <%
		}
		else{
  %>
      <tr id="browserOddRow">
        <%
		}
  %>
	<td><A href="<%=perApndxUri%>" target="_new"><%=perApndxUri%></A></td>
	<td> <%=perApndxDesc%> </td>
	<td width=15%><%=perApndxDate%></td>
	<td width=10%>
	<A HREF="addperurl.jsp?patId=<%=patId%>&mode=M&srcmenu=<%=src%>&perApndxId=<%=perApndxId%>&selectedTab=<%=selectedTab%>&perApndxType=<%=perApndxType%>&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&studyVer=<%=studyVer%>" onClick="return f_check_perm_org(<%=pageRight%>,<%=orgRight%>,'E')"><img border="0" title="<%=LC.L_Edit%>" alt="<%=LC.L_Edit%>" src="./images/edit.gif" ><%//=LC.L_Edit %><%-- Edit*****--%></A>
	 </td>
	<td  align="center">
	<A HREF="perapndxdelete.jsp?perApndxId=<%=perApndxId%>&patId=<%=patId%>&delMode=null&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&perApndxType=<%=perApndxType%>&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&studyVer=<%=studyVer%>" onClick="return confirmBox('<%=perApndxUri%>',<%=pageRight%>,<%=orgRight%>)"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0"/></A>
	</td>
   </tr>
<%
   }

    PerApndxDao perApndxDaoFile = perApndxB.getPerApndxFiles(EJBUtil.stringToNum(patId));
	perApndxFileIds = perApndxDaoFile.getPerApndxIds();
	perApndxFileDates = perApndxDaoFile.getPerApndxDates();
    perApndxFileDescs = perApndxDaoFile.getPerApndxDescs();
	perApndxFileUris = perApndxDaoFile.getPerApndxUris();

%>

</TABLE>




<BR>
<TABLE width="100%">
<tr>
<th ><%=LC.L_My_Files %><%-- My Files*****--%></th>
</tr>
<!-- Changes By Sudhir for Quick fixes needed after v9.0 UI internal review on 16-Mar-2012 -->
<!--<tr>
<td>
<P class = "defComments"><%=MC.M_AttachDocu_ForPat %><%-- You can attach your important documents/forms for this <%=LC.Pat_Patient_Lower%>.*****--%>
<br><br> <%=MC.M_SelDel_RemAppx_ClkFile %><%-- Select [Delete] against the name of a file to remove it from appendix. Click on the file name to view it in a new browser window.*****--%></P>
</td>
</tr>-->
<!-- Changes By Sudhir for Quick fixes needed after v9.0 UI internal review on 16-Mar-2012 -->
</table>

<TABLE width="100%">
   <tr>
	<td colspan=3></td>
	<td width=20% align=right>
	<!--Modified by Manimaran to fix the Bug 2387 -->
	<A href="perapndxfile.jsp?patId=<%=patId%>&mode=N&selectedTab=<%=selectedTab%>&srcmenu=<%=src%>&perApndxType=F&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&studyVer=<%=studyVer%>" onClick="return f_check_perm_org(<%=pageRight%>,<%=orgRight%>,'E')"><%=LC.L_Upload_Docu%><%-- UPLOAD DOCUMENT*****--%></A>
	</td>
   </tr>
</TABLE>
<TABLE width="100%" cellspacing="0" cellpadding="0" class="basetbl outline midalign">
	<tr>
		<th width="30%"><%=LC.L_File_Name%><%-- File name*****--%></th>
		<th width="35%"><%=LC.L_Description%><%-- Description*****--%></th>
		<th width='15%'><%=LC.L_Date %><%-- Date*****--%></td>
		<th width="15%"><%=LC.L_Edit%><%-- Edit*****--%></th>
		<th width="5%"><%=LC.L_Delete%><%-- Delete*****--%></th>   
	</tr>

 <%
	String dnld;
	Configuration.readSettings("eres");
	Configuration.readUploadDownloadParam(Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "perapndx");
	dnld=Configuration.DOWNLOADSERVLET ;
	String modDnld = "";

   for(int i=0;i<perApndxFileIds.size(); i++) {
	perApndxFileId= (String)perApndxFileIds.get(i);
	perApndxFileDesc= (String)perApndxFileDescs.get(i);
	perApndxFileUri = (String)perApndxFileUris.get(i);
	perApndxFileDate = (String)perApndxFileDates.get(i);

	if ((i%2)!=0) {
  %>
      <tr id="browserEvenRow">
        <%
		}
		else{
  %>
      <tr id="browserOddRow">
        <%

		}
	modDnld = dnld + "?file=" + StringUtil.encodeString(perApndxFileUri) ;
	 // Fixed Bug#4018 BY: Prabhat
	// Commented By Rohit and added below script for Bug #4018
	/*
	int k=0;
	FileName = perApndxFileUri;
	for (int i1=0;i1<FileName.length();i1++){
		 if (FileName.charAt(i1)== '\\'){
		 	k=i1;
		}
	}
	FileName = FileName.substring(k+1,FileName.length()); */
	FileName = perApndxFileUri.substring(perApndxFileUri.lastIndexOf("\\")+1);


  %>


	<td>
	<A href="#" onClick="fdownload(document.per,<%=perApndxFileId%>,'<%=FileName%>','<%=dnld%>');return false;" >

	<%=FileName%></A> </td>
	<td width=25%> <%=perApndxFileDesc%> </td>

	<td width=15%><%=perApndxFileDate%></td>
	<td width=10%>
	<A HREF="perapndxfile.jsp?perApndxId=<%=perApndxFileId%>&patId=<%=patId%>&mode=M&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&studyVer=<%=studyVer%>" onClick="return f_check_perm_org(<%=pageRight%>,<%=orgRight%>,'E')"><img border="0" title="<%=LC.L_Edit%>" alt="<%=LC.L_Edit%>" src="./images/edit.gif" ><%//=LC.L_Edit %><%-- Edit*****--%></A>
	</td>

	<td  align="center">
	<A HREF="perapndxdelete.jsp?perApndxId=<%=perApndxFileId%>&patId=<%=patId%>&delMode=null&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&perApndxType=F&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&studyVer=<%=studyVer%>" onClick="return confirmBox('<%=FileName%>',<%=pageRight%>,<%=orgRight%>)"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0"/></A>

	</td>
   </tr>


<%
   }
%>

</TABLE>

<br>

 	<input type="hidden" name="tableName" value="PAT_PERAPNDX">
    <input type="hidden" name="columnName" value="PERAPNDX_FILEOBJ">
    <input type="hidden" name="pkColumnName" value="PK_PERAPNDX">
    <input type="hidden" name="module" value="perapndx">
    <input type="hidden" name="db" value="per">
    <input type="hidden" name="pkValue" value="">
    <input type="hidden" name="file" value="">
    <input type="hidden" name="dnldurl" value="">
    <input type="hidden" name="moduleName" value="">

</form>

<%
}// end of if for space

} //end of else of incorrect file

} //end of if body for page right

else
 {
	%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%
 } //end of else body for page right

} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>

  <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</div>
<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>
</html>

