<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
<jsp:useBean id ="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="userb" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />

<jsp:useBean id="personB" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="lnkformB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/>

<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.DateUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.common.*,com.velos.eres.service.util.*,com.velos.eres.web.user.UserJB,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.common.CtrlDao,com.velos.eres.business.common.SettingsDao,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration"%>

<%@ page import="com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings"%>

<%
	// added on 30th march for fixing the bug 2074
	HttpSession tS1 = request.getSession(true);
	// added by Ganapathy on 05-05-05
	int patDataDetail=0;
	int personPK=0;
	int usrGroup=0;
	String mode = "M";
	String selclass="unselectedTab";
	String userId="";
	String acc="";

	String page1 = request.getParameter("page");
	String patientFName="";
	String patientLName="";
	String patientName="";

	String statDesc=null;

	String statid=request.getParameter("statid");

	String studyNum=request.getParameter("studyNum");

	String fromPage=request.getParameter("fromPage");

	String enrollDate = "";

//added on 13Mar05 for Mahi-II enhancement

String skipLinkedForms = StringUtil.trueValue(request.getParameter("skipLinkedForms"));
String src= request.getParameter("srcmenu");
String selectedTab111 = request.getParameter("selectedTab");
int patId=EJBUtil.stringToNum(request.getParameter("pkey"));

	String calledFromForm = "";
       calledFromForm = request.getParameter("calledFromForm");
	    if (StringUtil.isEmpty(calledFromForm))
      {
     	calledFromForm = "";
      }

 String formFillDt = "";
 formFillDt = request.getParameter("formFillDt") ;
 if (formFillDt == null) formFillDt = "ALL";


	  //////////////////





	if (fromPage==null) {
		fromPage="-";
	}
	if(page1 == null)
	  page1 = "";
	String patientId="";
	String patientCode = "";
	String studyId = "";
	String studyVer = "";
	String patProtId="";
	String patStudyId = "";
	String enrollId = "";
	String pkey="";


	boolean fromStudy = true;

	boolean isEnrol = false;
	boolean isSchedule = false;
	boolean isAdv = false;
	boolean isAlert = false;
	boolean isForms = false;
	//km -to fix the Bug 2408.
	patientCode= StringUtil.decodeString(request.getParameter("patientCode"));	// added by Ganapathy on 05-05-05
	if(patientName==null)
	patientName="";
	if(patientCode==null)
		patientCode = "";

	int eptRight = 0;
%>

<%!
	/////Commonly used variables
	int itemCount = 0;
	int maxItemsInColumn = 12;
	String blankMenuItem = "<div class=\"context-menu-item\"><div style=\"\" class=\"context-menu-item-inner\">&nbsp;</div></div>";
	/////
	private StringBuffer addNewColumn(int arrayIndx, int tabCount) {
		StringBuffer sbNewColumn = new StringBuffer();
		itemCount++;

		//Last Element
		if (arrayIndx + 1 == tabCount){
			return sbNewColumn;
		}
		// Not a Last Element 
		if (itemCount % maxItemsInColumn == 0){
			sbNewColumn.append("</div></td>")
				.append("<td width='[colPercentWidth]' NOWRAP>")
				.append("<div class=\"context-menu context-menu-theme-default\">")
				.append(blankMenuItem);
			itemCount++;
		}
		return sbNewColumn;
	}
%>
<%
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{
		//IMP: Resetting itemCount for every single call
		itemCount = 0;
		acc = (String) tSession.getValue("accountId");
		patientId = (String) request.getParameter("pkey");
		pkey = (String) request.getParameter("pkey");
		studyId  = (String) request.getParameter("studyId");

		String studyIdFromSession =(String) tSession.getAttribute("studyId");
		if (!studyId.equals(studyIdFromSession)){
			tSession.setAttribute("studyId",studyId);
		}

		studyVer = request.getParameter("studyVer");
		String modRight = (String) tSession.getValue("modRight");
		int modlen = modRight.length();
		ctrl.getControlValues("module");
		int ctrlrows = ctrl.getCRows();
		ArrayList feature =  ctrl.getCValue();
		ArrayList ftrDesc = ctrl.getCDesc();
		ArrayList ftrSeq = ctrl.getCSeq();
		ArrayList ftrRight = new ArrayList();
		String strR;

		//KM
		int pageRight = 0;
		String patRightString = null;
		int patManageRight = 0;


		ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
		ArrayList tabList = objCache.getAccountObjects(EJBUtil.stringToNum(acc), "pat_tab");

		String protocolManagementRight = (String) tSession.getValue("protocolManagementRight");

		int protocolManagementRightInt = 0;
		protocolManagementRightInt = EJBUtil.stringToNum(protocolManagementRight);


			// added by Ganapathy on 05-05-05
		String userIdFromSession = (String) tSession.getValue("userId");
		userb.setUserId(EJBUtil.stringToNum(userIdFromSession));
		userb.getUserDetails();
		usrGroup=EJBUtil.stringToNum(userb.getUserGrpDefault());
		personPK=EJBUtil.stringToNum(request.getParameter("pkey"));			
		patDataDetail=person.getPatientCompleteDetailsAccessRight(EJBUtil.stringToNum(userIdFromSession),usrGroup,personPK);

	if(page1.equals("patientEnroll"))
	{

		enrollId= request.getParameter("patProtId");

		if (EJBUtil.isEmpty(enrollId)) // if there is no patProtId passed/is empty
			{
				//check if there is any current enrollment
				patEnrollB.findCurrentPatProtDetails(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(pkey) );
				enrollId =  String.valueOf(patEnrollB.getPatProtId()); //get patProtId from current enrollment
			}
		else
			{

				patEnrollB.setPatProtId(EJBUtil.stringToNum(enrollId));
				patEnrollB.getPatProtDetails();
			}

		patStudyId = patEnrollB.getPatStudyId();
		enrollDate = patEnrollB.getPatProtEnrolDt();

		if(StringUtil.isEmpty(patStudyId))
			{
				patStudyId="";
			}
		patProtId = enrollId;
	}


		for (int counter = 0; counter <= (modlen - 1);counter ++) {
			strR = String.valueOf(modRight.charAt(counter));
			ftrRight.add(strR);
		}
		grpRights.setGrSeq(ftrSeq);
		grpRights.setFtrRights(ftrRight);
		grpRights.setGrValue(feature);
		grpRights.setGrDesc(ftrDesc);
		eptRight  = Integer.parseInt(grpRights.getFtrRightsByValue("MODEPT"));
		studyVer = request.getParameter("studyVer");


		// To check for the account level rights
		//String modRight = (String) tSession.getValue("modRight");
		 int patProfileSeq = 0, formLibSeq = 0;
		 acmod.getControlValues("module");
		 ArrayList acmodfeature =  acmod.getCValue();
		 ArrayList acmodftrSeq = acmod.getCSeq();
		 char formLibAppRight = '0';
		 char patProfileAppRight = '0';
		 patProfileSeq = acmodfeature.indexOf("MODPATPROF");
		 formLibSeq = acmodfeature.indexOf("MODFORMS");
		 formLibSeq = ((Integer) acmodftrSeq.get(formLibSeq)).intValue();
		 patProfileSeq = ((Integer) acmodftrSeq.get(patProfileSeq)).intValue();
		 formLibAppRight = modRight.charAt(formLibSeq - 1);
		 patProfileAppRight = modRight.charAt(patProfileSeq - 1);


		GrpRightsJB grpRightsPat = (GrpRightsJB) tSession.getValue("GRights");

		if (grpRightsPat!= null)
		{
			patRightString = grpRightsPat.getFtrRightsByValue("PATFRMSACC");
			patManageRight = Integer.parseInt(grpRightsPat.getFtrRightsByValue("MPATIENTS"));
		}
		if (patRightString != null)
		{
			pageRight = EJBUtil.stringToNum((patRightString));
			
		}
		
		//GET STUDY TEAM RIGHTS 
	    TeamDao teamDao = new TeamDao();
		int pageRightTeam=0 ;
	    teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));
	    ArrayList tId = teamDao.getTeamIds();
	    if (tId.size() == 0) 
		{ 
	    	pageRightTeam=0 ;
	    }
		else 
		{
	    	stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));
	    	 
		 	ArrayList teamRights ;
					teamRights  = new ArrayList();
					teamRights = teamDao.getTeamRights();
						 
					stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
					stdRights.loadStudyRights();
					
	    	if ((stdRights.getFtrRights().size()) == 0)
			{
	    		pageRightTeam= 0;
	    	}
			else
			{
				pageRightTeam = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYMPAT"));
	    	}
	    }
	    

		PatStudyStatDao ps = new PatStudyStatDao();
		ps.getPatStatHistory(EJBUtil.stringToNum(patientId), EJBUtil.stringToNum(studyId));
		ArrayList patStatPks = new ArrayList();
		ArrayList currentStats =  new ArrayList();
		patStatPks = ps.getPatientStatusPk();
		currentStats = ps.getCurrentStats();
		int len= ps.getCRows();
		String currentStatId="";
		String currentVal="";
		String prevPatStudyStatPK = "";
		for (int i=0;i<len;i++) {
		     currentVal = (currentStats.get(i) == null)?"":(currentStats.get(i)).toString();
		     if ( currentVal.equals("1")) {
			  currentStatId = (String)patStatPks.get(i).toString();
		     }
   		}

		//get latest status
 	  	if (patStatPks.size() > 0 )
 	  	{
 	  		prevPatStudyStatPK = (patStatPks.get(0)).toString();
 	  	}

 	  	PatStudyStatDao pdao = new PatStudyStatDao();
 	  	String filEnrDt = "ALL";
 		String filVisit = null ;
 	 	String filDoneOn = "ALL";
 		String filNext = "ALL";
 		String pstat = request.getParameter("patstatus") ;
 		
 		String pOrganization="";
 		person.setPersonPKId(personPK);
		 person.getPersonDetails();

		pOrganization = person.getPersonLocation();
		
	   	pdao = studyB.getPatientStudiesWithVisits(EJBUtil.stringToNum(studyId), EJBUtil.stringToNum(pkey), EJBUtil.stringToNum(pstat) ,EJBUtil.stringToNum(userIdFromSession), EJBUtil.stringToNum(pOrganization),filEnrDt,filDoneOn,filNext,filVisit );
	   	ArrayList   statDescs = pdao.getPatientStatusDescs();
	   	statDesc=(String)statDescs.get(0);
	   	int orgRight=0;	   		
	   	orgRight = userSiteB.getMaxRightForStudyPatient(EJBUtil.stringToNum(userIdFromSession), personPK , EJBUtil.stringToNum(studyId) );

		/////Commonly used variables
		String blankMenuItem = "<div class=\"context-menu-item\"><div style=\"\" class=\"context-menu-item-inner\">&nbsp;</div></div>";
		int maxItemsInColumn = 12;
		/////

	int tabCount = tabList.size();
	StringBuilder menuOut= new StringBuilder();
	menuOut.append("<table id=\"contextMenuTable\" cellspacing=\"0\" cellpadding=\"0\" class=\"menuTable\" style=\"position: absolute; z-index: 9999; display: none;\"><tbody><tr><td width='[colPercentWidth]' NOWRAP>")
	.append("<div class=\"context-menu context-menu-theme-default\">")
	.append("<div title=\"\" class=\"context-menu-title\">")
	.append("<div class=\"context-menu-title-inner\"><b>").append(LC.L_Patient).append(" :&nbsp;").append(patientCode).append("</b>&nbsp;&nbsp;</div></div>")
	.append(addNewColumn(-1, tabCount));

	for (int iX=0; iX<tabList.size(); iX++) {
	ObjectSettings settings = (ObjectSettings)tabList.get(iX);
	selclass = "unselectedTab";

	if ("0".equals(settings.getObjVisible())) {
		continue;
	}

	boolean showThisTab = false;
	if ("1".equals(settings.getObjSubType())) {

			
			String href="<div id=\"actionValue\" style=\"display:none\">patientdetails.jsp?mode="+mode+"&srcmenu=tdmenubaritem5&selectedTab=1&pkey="+patientId+"&patProtId="+patProtId+"&patientCode="+StringUtil.encodeString(patientCode)+"&page="+page1+"&studyId="+studyId+"&generate=N&statid="+statid+"&studyVer="+studyVer+"</div><div id=\"displayValue\"><b>"+settings.getObjDispTxt()+"</b></div>";
			menuOut.append("<div title=\"\" id=\"noSubMenu\" class=\"context-menu-item\">")
			.append("<div style=\"\" class=\"context-menu-item-inner\">")
			.append(href)
			.append("</div></div>")
			.append(addNewColumn(iX, tabCount));
		
	}
	else if ("11".equals(settings.getObjSubType())) {
		if ( page1.equals("patientEnroll") || page1.equals("patient")) { //KM-#4099
			if ((String.valueOf(formLibAppRight).compareTo("1") == 0 || String.valueOf(patProfileAppRight).compareTo("1") == 0) &&(pageRight >=4)    )
			{
			String href="<div id=\"actionValue\" style=\"display:none\">formfilledpatbrowser.jsp?srcmenu=tdmenubaritem5&selectedTab=11&pkey="+patientId+"&patientCode="+StringUtil.encodeString(patientCode)+"</div><div id=\"displayValue\"><b>"+settings.getObjDispTxt()+"</b></div>";
			menuOut.append("<div title=\"\" id=\"noSubMenu\" class=\"context-menu-item\">")
			.append("<div style=\"\" class=\"context-menu-item-inner\">")
			.append(href)
			.append("</div></div>")
			.append(addNewColumn(iX, tabCount));

			}
		}
	}
	else if ("2".equals(settings.getObjSubType()) )  {
		if ( page1.equals("patientEnroll") || page1.equals("patient")) {
			if (StringUtil.isEmpty(patProtId))
				{
    		 	patProtId= request.getParameter("patProtId");
				}
			if (protocolManagementRightInt > 0) {
				
				StringBuffer subMenu = new StringBuffer();
				//StringBuffer spanElm= new StringBuffer();
				//spanElm.append("<span id=\"protSubMenu\" class=\"subMenus\" style=\"width:100%;display:none;\">");
				StringBuffer subSubMenu = new StringBuffer();
			  	String subParentValue="";
			  	String subSubMenuhref="";
			  	String parentValue="";
			  	String subMenuhref="";
			  	/*--------------------*/

			  	/*Header item*/
			  	String href="<div id=\"actionValue\" style=\"display:none\">patientstudies.jsp?srcmenu=tdMenuBarItem5&selectedTab=2&mode="+mode+"&pkey="+patientId+"&patProtId="+patProtId+"&patientCode="+StringUtil.encodeString(patientCode)+"&studyId="+studyId+"&statDesc="+statDesc+"&statid="+statid+"&studyVer="+studyVer+"&page=patientEnroll</div><div id=\"displayValue\"><b>"+settings.getObjDispTxt()+"</b></div>";
				menuOut.append("<div title=\"\" id=\"openProtsSubMenu\" class=\"context-menu-item\">")
					.append("<div style=\"\" class=\"context-menu-item-inner\">")
					.append(href)
					.append("</div></div>")
				  	.append(addNewColumn(-1, tabCount));
				
			  	/*Header item*/
				/*Patient Study Status*/
			  	subMenuhref="<div id=\"actionValue\" style=\"display:none\">enrollpatient.jsp?srcmenu=tdMenuBarItem5&selectedTab=2&mode="+mode+"&pkey="+patientId+"&patProtId="+patProtId+"&patientCode="+StringUtil.encodeString(patientCode)+"&studyId="+studyId+"&statDesc="+statDesc+"&statid="+statid+"&studyVer="+studyVer+"&page=patientEnroll</div><div id=\"displayValue\" style=\"margin-left:10px;\">"+LC.L_Pat_StdStatus+"</div>";
			  	subMenu.append("<div title=\"\" class=\"context-menu-item\">")
					.append("<div style=\"\" class=\"context-menu-item-inner\">")
					.append(subMenuhref)
					.append("</div></div>")
			  		.append(addNewColumn(-1, tabCount));
			  	
			  	/*Add Patient Study Status*/
			  	subParentValue="<div id=\"parentValue\" style=\"display:none\">enrollpatient.jsp?srcmenu=tdMenuBarItem5&selectedTab=2&mode="+mode+"&pkey="+patientId+"&patProtId="+patProtId+"&patientCode="+StringUtil.encodeString(patientCode)+"&studyId="+studyId+"&statDesc="+statDesc+"&statid="+statid+"&studyVer="+studyVer+"&page=patientEnroll</div>";
			  	subSubMenuhref="<div id=\"accessRights\" style=\"display:none\">N||"+pageRightTeam+"||"+orgRight+"</div><div id=\"popValue\" style=\"display:none\">patstudystatus.jsp?patStudiesEnrollingOrg=&studyId="+studyId+"&changeStatusMode=yes&statid=" + prevPatStudyStatPK + "&pkey=" + patientId+"&amp;currentStatId="+currentStatId+"</div><div id=\"displayValue\" style=\"margin-left:20px;\">"+LC.L_Add_PatStdStatus+"</div>"+subParentValue;
			  	subSubMenu.append("<div title=\"\" class=\"context-menu-item\">")
					.append("<div style=\"\" class=\"context-menu-item-inner\">")
					.append(subSubMenuhref)
					.append("</div></div>")
					.append(addNewColumn(iX, tabCount));
				
			  	subMenu.append(subSubMenu);
				
			  	/*--------------------*/

			  	/*Header item*/
			  	/*Schedule*/
			  	subMenuhref="<div id=\"actionValue\" style=\"display:none\">patientschedule.jsp?srcmenu=tdMenuBarItem5&selectedTab=3&mode="+mode+"&pkey="+patientId+"&patProtId="+patProtId+"&patientCode="+StringUtil.encodeString(patientCode)+"&studyId="+studyId+"&statDesc="+statDesc+"&statid="+statid+"&generate=N&studyVer="+studyVer+"&page=patientEnroll</div><div id=\"displayValue\" style=\"margin-left:10px;\">"+LC.L_Schedule+"</div>";
			  	subMenu.append("<div title=\"\" class=\"context-menu-item\">")
					.append("<div style=\"\" class=\"context-menu-item-inner\">")
					.append(subMenuhref)
					.append("</div></div>")
					.append(addNewColumn(iX, tabCount));
				
				/*Edit Event Status*/
			  	subParentValue="<div id=\"parentValue\" style=\"display:none\">patientschedule.jsp?srcmenu=tdMenuBarItem5&selectedTab=3&mode="+mode+"&pkey="+patientId+"&patProtId="+patProtId+"&patientCode="+StringUtil.encodeString(patientCode)+"&studyId="+studyId+"&statDesc="+statDesc+"&statid="+statid+"&generate=N&studyVer="+studyVer+"&page=patientEnroll</div>";
			  	subSubMenuhref="<div id=\"accessRights\" style=\"display:none\">V||4</div><div id=\"popValue\" style=\"display:none\">editMulEventDetails.jsp?patProtId="+patProtId+"&pkey="+patientId+"&studyId="+studyId+"&viewType=event</div><div id=\"displayValue\" style=\"margin-left:20px;\">"+LC.L_Edit_MultiEvts+"</div>"+subParentValue;
			  	subSubMenu = new StringBuffer();
			  	subSubMenu.append("<div title=\"\" class=\"context-menu-item\">")
					.append("<div style=\"\" class=\"context-menu-item-inner\">")
					.append(subSubMenuhref)
					.append("</div></div>")
					.append(addNewColumn(iX, tabCount));
				
				/*Edit Visit Status*/
			  	subParentValue="<div id=\"parentValue\" style=\"display:none\">patientschedule.jsp?srcmenu=tdMenuBarItem5&selectedTab=3&mode="+mode+"&pkey="+patientId+"&patProtId="+patProtId+"&patientCode="+StringUtil.encodeString(patientCode)+"&studyId="+studyId+"&statDesc="+statDesc+"&statid="+statid+"&generate=N&studyVer="+studyVer+"&page=patientEnroll</div>";
			  	subSubMenuhref="<div id=\"accessRights\" style=\"display:none\">V||4</div><div id=\"popValue\" style=\"display:none\">editMulEventDetails.jsp?patProtId="+patProtId+"&pkey="+patientId+"&studyId="+studyId+"</div><div id=\"displayValue\" style=\"margin-left:20px;\">Edit Visit Status</div>"+subParentValue;
			  	subSubMenu.append("<div title=\"\" class=\"context-menu-item\">")
					.append("<div style=\"\" class=\"context-menu-item-inner\">")
					.append(subSubMenuhref)
					.append("</div></div>")
					.append(addNewColumn(iX, tabCount));

				subMenu.append(subSubMenu);

			  	/*--------------------*/
			  	
			  	/*Header item*/
			  	/*Adverse Events*/
			  	subMenuhref="<div id=\"actionValue\" style=\"display:none\">adveventbrowser.jsp?srcmenu=tdMenuBarItem5&selectedTab=6&mode="+mode+"&pkey="+patientId+"&visit=1&patProtId="+patProtId+"&studyId="+studyId+"&studyNum="+studyNum+"&statDesc="+statDesc+"&statid="+statid+"&studyVer="+studyVer+"&page=patientEnroll</div><div id=\"displayValue\" style=\"margin-left:10px;\">"+LC.L_Adverse_Events+"</div>";
			  	subMenu.append("<div title=\"\" class=\"context-menu-item\">")
				.append("<div style=\"\" class=\"context-menu-item-inner\">")
				.append(subMenuhref)
				.append("</div></div>")
			  	.append(addNewColumn(iX, tabCount));

				/*Add New AE*/
			  	subSubMenuhref="<div id=\"actionValue\" style=\"display:none\">N||"+pageRightTeam+"||"+orgRight+"##adverseEventScreen.jsp?srcmenu=tdMenuBarItem5&selectedTab=6&mode=N&pkey="+patientId+"&visit=1&patProtId="+patProtId+"&studyId="+studyId+"&studyNum="+studyNum+"&statDesc="+statDesc+"&statid="+statid+"&studyVer="+studyVer+"&page=patientEnroll</div><div id=\"displayValue\" style=\"margin-left:20px;\">"+LC.L_AddNew_Ae+"</div>";
			  	subSubMenu = new StringBuffer();
			  	subSubMenu.append("<div title=\"\" class=\"context-menu-item\">")
					.append("<div style=\"\" class=\"context-menu-item-inner\">")
					.append(subSubMenuhref)
					.append("</div></div>")
				  	.append(addNewColumn(iX, tabCount));
				
				if(!("Y".equalsIgnoreCase(CFG.AE_DISABLE_MULAE_LINK))){
					/*Add Multiple AE's*/
				  	subParentValue="<div id=\"parentValue\" style=\"display:none\">adveventbrowser.jsp?srcmenu=tdMenuBarItem5&selectedTab=6&mode="+mode+"&pkey="+patientId+"&visit=1&patProtId="+patProtId+"&studyId="+studyId+"&studyNum="+studyNum+"&statDesc="+statDesc+"&statid="+statid+"&studyVer="+studyVer+"&page=patientEnroll</div>";
				  	subSubMenuhref="<div id=\"accessRights\" style=\"display:none\">N||"+pageRightTeam+"||"+orgRight+"</div><div id=\"popValue\" style=\"display:none\">addmultipleadvevent.jsp?srcmenu=tdMenuBarItem5&page=patientEnroll&selectedTab=6&studyId="+studyId+"&pkey="+patientId+"</div><div id=\"displayValue\" style=\"margin-left:20px;\">"+LC.L_Add_Multi_Aes+"</div>"+subParentValue;
				  	subSubMenu.append("<div title=\"\" class=\"context-menu-item\">")
						.append("<div style=\"\" class=\"context-menu-item-inner\">")
						.append(subSubMenuhref)
						.append("</div></div>")
					  	.append(addNewColumn(iX, tabCount));
				}
			  	subMenu.append(subSubMenu);

			  	/*--------------------*/

			  	int formRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMACC"));
			  	if (StringUtil.isAccessibleFor(formRight, 'V')){
				  	/*Forms*/
				  	subMenuhref="<div id=\"actionValue\" style=\"display:none\">formfilledstdpatbrowser.jsp?mode=M&srcmenu=tdmenubaritem5&selectedTab=8&pkey="+patientId+"&patProtId="+patProtId+"&studyId="+studyId+"&calledFrom=S&statDesc="+statDesc+"&statid="+statid+"&patientCode="+StringUtil.encodeString(patientCode)+"</div><div id=\"displayValue\" style=\"margin-left:10px;\">"+LC.L_Form+"</div>";
				  	subMenu.append("<div title=\"\" class=\"context-menu-item\">")
						.append("<div style=\"\" class=\"context-menu-item-inner\">")
						.append(subMenuhref)
						.append("</div></div>")
					  	.append(addNewColumn(iX, tabCount));
			  	}

			  	/*--------------------*/
			  	
				menuOut.append(subMenu);
			}
		}
	}
	else if ("4".equals(settings.getObjSubType())) {
		if ( page1.equals("patientEnroll") || page1.equals("patient")) {
			String href="<div id=\"actionValue\" style=\"display:none\">patientreports.jsp?srcmenu=tdMenuBarItem5&selectedTab=4&mode="+mode+"&pkey="+patientId+"&patProtId="+patProtId+"&patientCode="+StringUtil.encodeString(patientCode)+"&page="+page1+"&generate=N&studyId="+studyId+"&statDesc="+statDesc+"&statid="+statid+"&studyVer="+studyVer+"</div><div id=\"displayValue\"><b>"+settings.getObjDispTxt()+"</b></div>";
			menuOut.append("<div title=\"\" id=\"noSubMenu\" class=\"context-menu-item\">")
			.append("<div style=\"\" class=\"context-menu-item-inner\">")
			.append(href)
			.append("</div></div>")
		  	.append(addNewColumn(iX, tabCount));
		}
	}
	else if ("7".equals(settings.getObjSubType())) {
		if(eptRight==1){
	
			/*Header item*/
		  	/*Appendix*/
			String href="<div id=\"actionValue\" style=\"display:none\">perapndx.jsp?srcmenu=tdMenuBarItem5&selectedTab=7&mode="+mode+"&pkey="+patientId+"&page="+page1+"&studyId="+studyId+"&calledFrom=P&statDesc="+statDesc+"&statid="+statid+"&studyVer="+studyVer+"</div><div id=\"displayValue\"><b>"+settings.getObjDispTxt()+"</b></div>";
			menuOut.append("<div title=\"\" id=\"openApndxSubMenu\" class=\"context-menu-item\">")
			.append("<div style=\"\" class=\"context-menu-item-inner\">")
			.append(href)
			.append("</div></div>")
		  	.append(addNewColumn(iX, tabCount));
			
			StringBuffer subMenu= new StringBuffer();
			/*Upload Document*/
		  	String subMenuhref="<div id=\"actionValue\" style=\"display:none\">E||"+patManageRight+"##perapndxfile.jsp?patId="+patId+"&mode=N&selectedTab="+settings.getObjSubType()+"&srcmenu="+src+"&perApndxType=F&studyId="+studyId+"&statDesc="+statDesc+"&statid="+statid+"&studyVer="+studyVer+"</div><div id=\"displayValue\" style=\"margin-left:10px;\">"+LC.L_Upload_Docu+"</div>";
		  	subMenu.append("<div title=\"\" class=\"context-menu-item\">")
			.append("<div style=\"\" class=\"context-menu-item-inner\">")
			.append(subMenuhref)
			.append("</div></div>")
		  	.append(addNewColumn(iX, tabCount));
			
			menuOut.append(subMenu);
 		}
	} else {
			 menuOut.append("<div title=\"\" id=\"noSubMenu\" class=\"context-menu-item\">")
			.append("<div style=\"\" class=\"\">")
			.append("<b>"+LC.L_NoAccsRights+"</b>")
			.append("</div></div>");
			 
			 itemCount++;
	}
 }	


	if (itemCount > maxItemsInColumn){
		int itemsInLastColumn = (itemCount % maxItemsInColumn);
		if (itemsInLastColumn > 0){
			for(int item = 0; item < (maxItemsInColumn - itemsInLastColumn); item++){
				menuOut.append(blankMenuItem);
			}
		}
	}

	menuOut.append("</div></td></tr></tbody></table>");

	double colPercentWidth = 0;
	int extraColumn = ((itemCount % maxItemsInColumn) == 0) ? 0 : 1 ;
	colPercentWidth = 100 /((itemCount / maxItemsInColumn) + extraColumn);

	while (menuOut.indexOf("[colPercentWidth]") >= 0){
		int startIndx = menuOut.indexOf("[colPercentWidth]");
		int endIndx = menuOut.indexOf("[colPercentWidth]") + "[colPercentWidth]".length();
		menuOut.replace(startIndx, endIndx, colPercentWidth+"%");
	}

	out.println(menuOut.toString());
} //Session Valid

%>