<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>

<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=MC.M_Std_DetsInEres%><%--<%=LC.Std_Study%> Details in eResearch*****--%></title>

<% response.setHeader("Cache-Control", "max-age=1, must-revalidate");%>


<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.user.UserJB " %>

<%@ page import="com.velos.eres.business.user.*,com.velos.eres.service.userAgent.*,com.velos.eres.service.util.*" %>



<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />

<jsp:useBean id="appendixB" scope="page" class="com.velos.eres.web.appendix.AppendixJB" />

<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />

<jsp:useBean id="sectionB" scope="page" class="com.velos.eres.web.section.SectionJB" />
<jsp:useBean id="studyVerB" scope="request" class="com.velos.eres.web.studyVer.StudyVerJB"/>




<Link Rel=STYLESHEET HREF="common.css" type=text/css>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>



<% String src;

src= request.getParameter("srcmenu");

String selectedTab = request.getParameter("selectedTab");

%>

<body style="overflow:scroll;">
<BR>

<%

 HttpSession tSession = request.getSession(true);

 String uName = (String) tSession.getValue("userName");

 String userIdFromSession = (String) tSession.getValue("userId");

 String mode ="";

 String acc = (String) tSession.getValue("accountId");



if (sessionmaint.isValidSession(tSession))

{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%


   int accId = EJBUtil.stringToNum(acc);

   int studyId = 0;



   CodeDao cdTArea = new CodeDao();

   CodeDao cdPhase = new CodeDao();

   CodeDao cdResType = new CodeDao();

   CodeDao cdBlinding = new CodeDao();

   CodeDao cdRandom = new CodeDao();

   CodeDao cdType = new CodeDao();





   String studyNumber = "" ;

   String studyTitle = "" ;

   String studyObjective = "" ;

   String studySummary = "" ;

   String studyProduct = "" ;

   String studyTArea = "" ;

   String studySize = "0" ;

   String studyDuration = "0" ;

   String studyDurationUnit = "" ;

   String studyEstBgnDate = "" ;

   String studyPhase = "" ;

   String studyResType = "" ;

   String studyType = "" ;

   String studyBlinding = "" ;

   String studyRandom = "" ;

   String studySponsorId = "" ;

   String studyContactId = "" ;

   String studyOtherInfo = "" ;

   String studyPartCenters = "" ;

   String studyKeywrds = "" ;

   String studyAuthor = "" ;

   String studyPubCol="";

	char[] stdSecRights =null ;

   int counter = 0;

   int userId = 0;

   int siteId = 0;

  int secCount = 0;

   UserDao userDao = new UserDao();

   SiteDao siteDao = new SiteDao();



   studyId = EJBUtil.stringToNum(request.getParameter("studyId"));

   studyB.setId(studyId);

   studyB.getStudyDetails();

   studyNumber = studyB.getStudyNumber();

   stdRights.setId(studyId);



      studyTitle = studyB.getStudyTitle();

      studyObjective = studyB.getStudyObjective();

      studySummary = studyB.getStudySummary();

      studyProduct = studyB.getStudyProduct();

      studySize = studyB.getStudySize();

      studyDuration = studyB.getStudyDuration();

      studyDurationUnit = studyB.getStudyDurationUnit();

      studyEstBgnDate = studyB.getStudyEstBeginDate();

      studyOtherInfo = studyB.getStudyInfo();

      studyPartCenters = studyB.getStudyPartCenters();

      studyKeywrds = studyB.getStudyKeywords();

      studySponsorId = studyB.getStudySponsor();

      studyContactId = studyB.getStudyContact();

      studyAuthor = studyB.getStudyAuthor();


      /*UserRObj userRObj = null;       //Msgcntr Entity Bean Remote Object

      UserPK userPK;

      userPK = new UserPK(EJBUtil.stringToNum(studyAuthor));

      UserHome userHome = EJBUtil.getUserHome();

      userRObj = userHome.findByPrimaryKey(userPK); */

      UserJB uj  = new UserJB();
	  uj.setUserId(EJBUtil.stringToNum(studyAuthor));
	  uj.getUserDetails();

      String authorName = userRObj.getUserLastName() + ", " + userRObj.getUserFirstName();





	studyPubCol = studyB.getStudyPubCol();

	if (studyPubCol == null ){

		studyPubCol = "111"; // default rights for all section is Public

	}



	stdSecRights= studyPubCol.toCharArray();

	int totSec = studyPubCol.length();





	cdTArea.getCodeValuesById(EJBUtil.stringToNum(studyB.getStudyTArea()));

	if(cdTArea.getCDesc().size() > 0){

      		studyTArea = (String) cdTArea.getCDesc().get(0);

	}



	cdPhase.getCodeValuesById(EJBUtil.stringToNum(studyB.getStudyPhase()));

	if(cdPhase.getCDesc().size() > 0){

      		studyPhase = (String) cdPhase.getCDesc().get(0);

	}



	cdResType.getCodeValuesById(EJBUtil.stringToNum(studyB.getStudyResType()));

	if(cdResType.getCDesc().size() > 0){

      		studyResType = (String) cdResType.getCDesc().get(0);

	}





	cdBlinding.getCodeValuesById(EJBUtil.stringToNum(studyB.getStudyBlinding()));

	if(cdBlinding.getCDesc().size() > 0){

      		studyBlinding = (String) cdBlinding.getCDesc().get(0);

	}



	cdRandom.getCodeValuesById(EJBUtil.stringToNum(studyB.getStudyRandom()));

	if(cdRandom.getCDesc().size() > 0){

      		studyRandom = (String) cdRandom.getCDesc().get(0);

	}



	cdType.getCodeValuesById(EJBUtil.stringToNum(studyB.getStudyType()));

	if(cdType.getCDesc().size() > 0){

      		studyType = (String) cdType.getCDesc().get(0);

	}


      studyAuthor = userIdFromSession;



%>
<Form name="study" method="post" action="updateNewStudy.jsp" onsubmit="return validate()">
  <input type="hidden" name="srcmenu" size = 20  value = <%=src%> >
  <input type="hidden" name="selectedTab" size = 20  value = <%=selectedTab%> >
  <input type="hidden" name="studyId" size = 20  value = <%=studyId%> >
  <%

counter = 0;

%>
  <input type="hidden" name="mode" size = 20  value = <%=mode%> >
  <table width="100%" cellspacing="0" cellpadding="0">
    <tr>
      <td width="30%"> <%=LC.L_Std_DataManager%><%--<%=LC.Std_Study%> Data Manager*****--%> </td>
      <td width="70%"> <%=authorName%> </td>
    </tr>
  </table>
  <%if (stdSecRights[secCount] == '1') {

%>
  <BR>
  <table width="100%">
    <tr id="headSection" >
      <td> <%=LC.L_Std_Definition%><%--<%=LC.Std_Study%> Definition*****--%> </td>
    </tr>
  </table>
  <BR>
  <table width="100%" cellspacing="0" cellpadding="0" >
    <tr>
      <td width="30%"> <%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%> </td>
      <td width="70%"> <%=studyNumber%> </td>
    </tr>
    <tr>
      <td width="30%"> <%=LC.L_Title%><%--Title*****--%> </td>
      <td width="70%"> <%=studyTitle%> </td>
    </tr>
    <tr>
      <td width="30%"> <%=LC.L_Primary_Objective%><%--Primary Objective*****--%> </td>
      <td width="70%"> <%=studyObjective%> </td>
    </tr>
    <tr>
      <td width="30%"> <%=LC.L_Summary%><%--Summary*****--%> </td>
      <td width="70%"> <%=studySummary%> </td>
    </tr></tr>
  </table>
  <%} secCount++;

	if (stdSecRights[secCount] == '1') {  //Display Section 1 only if it is Public section: 1=Public 0=Non-Public



%>
  <BR>
  <table width="100%">
    <tr id="headSection" >
      <td> <%=LC.L_Study_Details%><%--<%=LC.Std_Study%> Details*****--%> </td>
    </tr>
  </table>
  <BR>
  <table width="100%" cellspacing="0" cellpadding="0" >
    <tr>
      <td width="30%"> <%=LC.L_Product_Name%><%--Product Name*****--%> </td>
      <td width="70%"> <%=studyProduct%> </td>
    </tr>
    <tr>
      <td width="30%"> <%=LC.L_Therapeutic_Area%><%--Therapeutic Area*****--%> </td>
      <td width="70%"> <%=studyTArea%> </td>
    </tr>
    <tr>
      <td width="30%"> <%=LC.L_Sample_Size%><%--Sample Size*****--%> </td>
      <td width="70%"> <%=studySize%> </td>
    </tr>
    <tr>
      <td width="30%"> <%=LC.L_Study_Duration%><%--<%=LC.Std_Study%> Durations*****--%> </td>
      <td width="70%"> <%=studyDuration%><%=studyDurationUnit%> </td>
    </tr>
    <tr>
      <td width="30%"> <%=LC.L_Estimated_BeginDate%><%--Estimated Begin Date*****--%> </td width="70%">
      <td colspan=2> <%=studyEstBgnDate%> </td>
  </table>
  <%}

secCount++;
if (stdSecRights[secCount] == '1' ) {

%>
  <BR>
  <table width="100%">
    <tr id="headSection" >
      <td> <%=LC.L_Study_Design%><%--<%=LC.Std_Study%> Design*****--%> </td>
    </tr>
  </table>
  <BR>
  <table width="100%" cellspacing="0" cellpadding="0">
    <td width="30%"> <%=LC.L_Phase%><%--Phase*****--%> </td>
    <td width="70%"> <%=studyPhase%> </td>
    </tr>
    <tr>
      <td width="30%"> <%=LC.L_Research_Type%><%--Research Type*****--%> </td>
      <td width="70%"> <%=studyResType%> </td>
    </tr>
    <tr>
      <td width="30%"> <%=LC.L_Study_Type%><%--<%=LC.Std_Study%> Type*****--%> </td>
      <td width="70%"> <%=studyType%> </td>
    </tr>
    <tr>
      <td width="30%"> <%=LC.L_Blinding%><%--Blinding*****--%> </td>
      <td width="70%"> <%=studyBlinding%> </td>
    </tr>
    <tr>
      <td width="30%"> <%=LC.L_Randomization%><%--Randomization*****--%> </td>
      <td width="70%"> <%=studyRandom%> </td>
    </tr>
  </table>
  <%}

%>
  <BR>
  <table width="100%">
    <tr id="headSection" >
      <td> <%=LC.L_Sponsor_Information%><%--Sponsor Information*****--%> </td>
    </tr>
  </table>
  <BR>

  <table width="100%" cellspacing="0" cellpadding="0">
    <td width="30%"> <%=LC.L_Sponsored_By%><%--Sponsored By*****--%> </td>
    <td width="70%"> <%=studySponsorId%></td>
    </tr>
    <tr>
      <td width="30%"> <%=LC.L_Contact%><%--Contact*****--%> </td>
      <td width="70%"> <%=studyContactId%></td>
    </tr>
    <tr>
      <td width="30%"> <%=LC.L_Other_Information%><%--Other Information*****--%> </td>
      <td width="70%"> <%=studyOtherInfo%> </td>
    </tr>
  </table>
  <BR>
  <table width="100%">
    <tr id="headSection" >
      <td> <%=LC.L_Participating_Centers%><%--Participating Centers*****--%> </td>
    </tr>
  </table>
  <BR>
  <table width="100%" cellspacing="0" cellpadding="0" >
    <tr>
      <td width="30%"> <%=LC.L_Participating_Centers%><%--Participating Centers*****--%> </td>
      <td width="70%"> <%=studyPartCenters%> </td>
    </tr>
    <tr>
      <td width="30%"> <%=LC.L_Keywords%><%--Keywords*****--%> </td>
      <td width="70%"> <%=studyKeywrds%> </td>
    </tr>
  </table>
  <%
  StudyVerDao studyVerDao = studyVerB.getAllVers(studyId);
  ArrayList studyVerIds = studyVerDao.getStudyVerIds();
  ArrayList studyVerNumbers = studyVerDao.getStudyVerNumbers();
  int verlen = studyVerIds.size();
  SectionDao sectionDao;
  ArrayList secNames;
  ArrayList secContents;
  AppendixDao appendixDao ;
  ArrayList appendixIds;
  ArrayList appendixFileUris;
  ArrayList appendixDescs;
  ArrayList appendixFiles;
  ArrayList appendixTypes;

  //get all study versions
  for(int i=0;i<verlen;i++) {
  	String stdVerId = ((studyVerIds.get(i))==null)?"-":(studyVerIds.get(i)).toString();
  	int studyVerId = EJBUtil.stringToNum(stdVerId);
  	String studyVerNum =((studyVerNumbers.get(i))==null)?"-":(studyVerNumbers.get(i)).toString();
%>
  <table width='100%' cellspacing='0' cellpadding='0'>
  	<tr height='10'>
  	</tr>
  	<tr>
  	<td class= 'labelheading'><%=LC.L_Version%><%--Version*****--%>: <%=studyVerNum%>
  	</td>
  	</tr>
  	</table>
  	<hr size=2 class='bluehr'>
 <%
  	appendixDao = appendixB.getAppendixValuesPublic(studyVerId);

	appendixIds = appendixDao.getAppendixIds();

	appendixFileUris = appendixDao.getAppendixFile_Uris();

	appendixDescs = appendixDao.getAppendixDescriptions();

	appendixFiles = appendixDao.getAppendixFiles();

	appendixTypes = appendixDao.getAppendixTypes();



	String appendixFileUri = "";

	String appendixDesc = "";

	String appendixFile = "";

	String appendixType = "";

	Integer appId;

	int len = appendixIds.size();



	boolean flag = true;
	boolean uflag = true;


	for(counter=0;counter<len;counter++) {
		appId = (Integer)appendixIds.get(counter);
		appendixFileUri = (String) appendixFileUris.get(counter);

		appendixDesc = (String) appendixDescs.get(counter);

		appendixFile = (String) appendixFiles.get(counter);

		appendixType = (String) appendixTypes.get(counter);

		appendixType = appendixType.trim();


		if(appendixType.equals("file")){
			if(flag == true){
				flag = false;
%>
  <BR>
  <table width="100%">
    <tr id="headSection" >
      <td> <%=LC.L_Appendix_Files%><%--Appendix Files*****--%> </td>
    </tr>
  </table>
  <BR>
  <table width="100%" cellspacing="0" cellpadding="0" >
    <tr id=tableheading>
      <th width="30%"> <%=LC.L_File_Name%><%--File Name*****--%> </th>
      <th width="70%"> <%=LC.L_File_Desc%><%--File Description*****--%> </th>
    </tr>
    <%

}

if ( flag == false) {
%>
    <tr>
      <td align="center">
        <%String dnld="";
Configuration.readAppendixParam(Configuration.ERES_HOME + "eresearch.xml");
dnld=Configuration.DOWNLOADSERVLET+"/"+ appendixFile +"";%>
        <A Href="<%=dnld%>?appndxId=<%=appId%>" target = _newapndx> <%=appendixFileUri%></A> </td>
      <td align="center"> <%=appendixDesc%> </td>
    </tr>
    <%

}}

if(appendixType.equals("url")){
 if (uflag == true) {
	uflag = false;

%>
  </table>
  <BR>
  <table width="100%">
    <tr id="headSection" >
      <td> <%=LC.L_Appendix_Urls%><%--Appendix URLs*****--%> </td>
    </tr>
  </table>
  <BR>
  <table width="100%" cellspacing="0" cellpadding="0" >
    <tr id="tableheading">
      <th width="30%"> <%=LC. L_Urls%><%--Urls*****--%> </th>
      <th width="70%"> <%=LC.L_Url_Desc%><%--Url Description*****--%> </th>
    </tr>
    <%

}

if (uflag == false) {
%>
    <tr>
      <td align="center"> <A Href="<%=appendixFileUri%>"> <%=appendixFileUri%>
        </A> </td>
      <td align="center"> <%=appendixDesc%> </td>
    </tr>
    <%}

}//end of if else if ladder

	}//end of for loop

%>
  </table>
  <%

	sectionDao = sectionB.getStudy(studyVerId, "Y");

	secNames = sectionDao.getSecNames();

	secContents = sectionDao.getContents();

	String secName = "";

	String secContent = "";

	len = secNames.size();

	flag = true;

	for(counter=0;counter<len;counter++) {

		secName = (String) secNames.get(counter);

		secContent = (String) secContents.get(counter);

		if(flag == true) {

			flag = false;

%>
  <BR>
  <table width="100%">
    <tr id="headSection" >
      <td> <%=LC.L_Std_Secs%><%--<%=LC.Std_Study%> Sections*****--%> </td>
    </tr>
  </table>
  <BR>
  <table width="100%" cellspacing="0" cellpadding="0" >

    <%

}

if(flag == false){

%>
    <tr height>
      <td align="left"><b><%=secName%></b><br><br></td>
    </tr>

	<tr>
      <td align="left"> <Pre><%=secContent%> </pre></td>
    </tr>

    <%

	} //end of if for checking value of flag

	}//end of for loop

} //for loop study version

%>
  </table>
</Form>
<%

}else{%>
<jsp:include page="timeout.html" flush="true"/>
<%}

%>


</body>

</html>

