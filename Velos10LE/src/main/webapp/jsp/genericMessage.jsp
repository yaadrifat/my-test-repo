<%@page import="com.velos.eres.web.patProt.PatProtJB"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.MC"%>
<%@page import="com.velos.eres.service.util.StringUtil, com.velos.eres.service.util.LC"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.velos.eres.web.intercept.InterceptorJB" %>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="ctrpDraftJB"  scope="request" class="com.velos.eres.ctrp.web.CtrpDraftJB"/>

<%!
private static String MESSAGE_KEY = "messageKey";
private static String ID = "id";
private static String[] URL_LIST  = {"ctrpDraftBrowser","ctrpDraftNonIndustrial","ctrpDraftNonIndustrial",
	"ctrpDraftIndustrial","ctrpDraftIndustrial","studyScreen.jsp","patientDemogScreen.jsp","patientDemogScreen.jsp","adverseEventScreen.jsp"};
%>
<%
// The only scriptlet in this JSP
String displayText = null;
String nextUrl = null;
Integer dPk = null;
HttpSession tSession = request.getSession(false);
try {
	if (!sessionmaint.isValidSession(tSession)) { return; }
	displayText = (String)MC.class.getField(request.getParameter(MESSAGE_KEY)).get(null);
	Integer id = StringUtil.stringToInteger(request.getParameter(ID));
	nextUrl = URL_LIST[id];
	dPk = ctrpDraftJB.getId();

	switch(id){
	case 1:
		//Mark Non-Indust Draft RFS
	case 4:
		//Mark Indust Draft RFS
		nextUrl += "?draftId="+dPk;
		break;
	case 2:
		//Save Non-Indust Draft
	case 3:
		//Save Indust Draft
		nextUrl += "?draftId="+dPk;
		nextUrl += "&saved=1";
		break;
	case 5:
		{//Save Study screen
			String studyId = (String)(request.getSession(false)).getAttribute("studyId");
			nextUrl += "?srcmenu=tdMenuBarItem3&selectedTab=1&mode=M&includeMode=N&studyId="+studyId;
			if ("A".equals((request.getSession(false)).getAttribute("createType"))) {
				if ("Yes".equals(request.getSession(false).getAttribute("isLindFtMode"))){
				  nextUrl = "flexStudyScreenLindFt?srcmenu=tdMenuBarItem3&selectedTab=irb_init_tab&mode=M&studyId="+studyId;
				}
				else{
				  nextUrl = "flexStudyScreen?srcmenu=tdMenuBarItem3&selectedTab=irb_init_tab&mode=M&studyId="+studyId;
				}
				
			}
		}
		break;
	case 6:
		{//Save Patient Enroll screen
			String personPK = (String)(request.getSession(false)).getAttribute("personPK");
			String patientCode = (String)(request.getSession(false)).getAttribute("patientCode");
			if (StringUtil.isEmpty(personPK) || StringUtil.isEmpty(patientCode)) return;

			String studyId = (String)(request.getSession(false)).getAttribute("studyId");
			if (StringUtil.stringToNum(studyId) > 0){
				nextUrl = "enrollpatient.jsp?mode=N&srcmenu=tdmenubaritem5&patientCode="+ patientCode + "&patProtId=&pkey="+personPK+"&page=patientEnroll&selectedTab=2&studyId="+studyId;
			} else {
				nextUrl += "?srcmenu=tdmenubaritem5&selectedTab=1&mode=M&includeMode=N&page=patient&patientCode="+patientCode+"&pkey="+personPK;
			}
		}
		break;
	case 7:
		{//Save Patient Demog screen
			String personPK = (String)(request.getSession(false)).getAttribute("personPK");
			String patientCode = (String)(request.getSession(false)).getAttribute("patientCode");
			if (StringUtil.isEmpty(personPK) || StringUtil.isEmpty(patientCode)) return;

			nextUrl += "?srcmenu=tdmenubaritem5&selectedTab=1&mode=M&includeMode=N&page=patient&patientCode="+patientCode+"&pkey="+personPK;

			// -- Add a hook for customization for after a patient demog update succeeds
			String DEFAULT_DUMMY_CLASS = "[Config_PatWflowNav_Interceptor_Class]";
			try {
				if (!StringUtil.isEmpty(LC.Config_PatWflowNav_Interceptor_Class) &&
						!DEFAULT_DUMMY_CLASS.equals(LC.Config_PatWflowNav_Interceptor_Class)) {
					Class clazz = Class.forName(LC.Config_PatWflowNav_Interceptor_Class);
					Object obj = clazz.newInstance();
					Object[] args = new Object[2];
					args[0] = (Object)tSession;
					args[1] = "patientDemogScreen";
					((InterceptorJB) obj).handle(args);
					nextUrl = (String) args[1];
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
			// -- End of hook
		}
		break;
	case 8:
		{//Save Adverse Event screen
		
			String pkey = (String)tSession.getAttribute("pkey");
			String adveventId = StringUtil.integerToString((Integer)tSession.getAttribute("adveventId"));
			String enrollId = (String)tSession.getAttribute("enrollId");
		
			if (StringUtil.isEmpty(pkey) || StringUtil.isEmpty(adveventId)) return;

			nextUrl += "?srcmenu=tdMenuBarItem5&selectedTab=6&mode=M&pkey="+pkey+"&adveventId="+adveventId+"&page=patientEnroll&includeMode=Y&patProtId="+enrollId;
		}
		break;
	}
} catch(Exception e) {
	e.printStackTrace();
	return;
}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Refresh" content="1; URL='<%=nextUrl%>' ">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="-1">
<meta http-equiv="Cache-Control" content="no-cache">
<title></title>
<jsp:include page="/jsp/localization.jsp"></jsp:include>
<jsp:include page="/jsp/panel.jsp"></jsp:include>
<jsp:include page="/jsp/velos_includes.jsp"></jsp:include>
</head>
<body>
<div class="BrowserBotN" >
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
<p class="sectionHeadings" align="center"><%=displayText%></p>
</div>
</body>
</html>
