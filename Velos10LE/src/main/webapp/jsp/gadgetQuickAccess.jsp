<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@page import="java.util.ArrayList,java.util.HashMap,org.json.*"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%> 
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
HttpSession tSession = request.getSession(false); 
if (!sessionmaint.isValidSession(tSession)) {
	return;
}
%>
<div>
    <table style="width:100%">
        <tr>
            <td colspan="2">
            <table style="width:100%">
                <tr>
                    <td>
                    <form name="gadgetQuickAccess_formStudy" id="gadgetQuickAccess_formStudy" 
                        onsubmit="gadgetQuickAccess.beforeSubmitStudy(this);"
                        method="POST" action="studybrowserpg.jsp">
                        <input type="text" class="gdt-input-text" size="30" maxlength="100" 
                                name="gadgetQuickAccess_study" id="gadgetQuickAccess_study" placeholder="<%=MC.M_StdTitle_Kword%>"/>
                        <input type="hidden" name="searchCriteria" id="searchCriteria" />
                        <button type="button" name="gadgetQuickAccess_studySearchButton" id="gadgetQuickAccess_studySearchButton" 
                                class="gadgetQuickAccess_studySearchButton"><%=LC.L_Search%></button>
                        <button type="button" name="gadgetQuickAccess_studyAdvancedSearchButton" id="gadgetQuickAccess_studyAdvancedSearchButton" 
                                class="gadgetQuickAccess_studyAdvancedSearchButton"><%=LC.L_Adva_Search%></button>
                    </form>
                    </td>
                    <td align="right" valign="middle" width = "20%">
                        <s:if test="%{gadgetQuickAccessJB.hasAcctFormsViewRights}" >
                        <a href="formfilledaccountbrowser.jsp?srcmenu=tdMenuBarItem1&srcpg=landingPage" ><%=LC.L_Acc_Forms%></a>
                        </s:if>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                    <form name="gadgetQuickAccess_formPatient" id="gadgetQuickAccess_formPatient" 
                            onsubmit="return gadgetQuickAccess.beforeSubmitPatient(this);"
                            method="POST" action="allPatient.jsp?srcmenu=tdmenubaritem5&selectedTab=1&searchFrom=initial">
                        <input type="text" class="gdt-input-text" size="20" maxlength="100" 
                                name="gadgetQuickAccess_patient" id="gadgetQuickAccess_patient" placeholder="<%=LC.L_Patient_Id%>"/>
                        <input type="hidden" name="studyId" id="studyId" />
                        <input type="hidden" name="patientid" id="patientid" />
                        <input type="hidden" name="searchPatient" id="searchPatient" />
                        <%=LC.L_On_Study_NonBrk%>
                        <s:checkbox id="gadgetQuickAccess_patientOnStudyTurnedOn" name="gadgetQuickAccess_patientOnStudyTurnedOn" />
                        <s:property escape="false" value="gadgetQuickAccessJB.studyMenuForPatSearch" />
                        <button type="button" name="gadgetQuickAccess_patientSearchButton" id="gadgetQuickAccess_patientSearchButton" 
                		        class="gadgetQuickAccess_patientSearchButton"><%=LC.L_Search%></button>
                    </form>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                    <form name="gadgetQuickAccess_formPatientStudy" id="gadgetQuickAccess_formPatientStudy" 
                            onsubmit="return gadgetQuickAccess.beforeSubmitPatientStudy(this);"
                            method="POST" action="studypatients.jsp?srcmenu=tdmenubaritem5&selectedTab=2&openMode=F">
                        <input type="text" class="gdt-input-text" size="20" maxlength="100" 
                                name="gadgetQuickAccess_patientStudy" id="gadgetQuickAccess_patientStudy" placeholder="<%=LC.L_Patient_StudyId%>"/>
                        <input type="hidden" name="studyId" id="studyId" />
                        <input type="hidden" name="pstudyid" id="pstudyid" />
                        <%=LC.L_On_Study_NonBrk%>
                        <input type="checkbox" style="visibility:hidden" />
                        <s:property escape="false" value="gadgetQuickAccessJB.studyMenuForPatStudySearch" />
                        <button type="button" name="gadgetQuickAccess_patientStudySearchButton" id="gadgetQuickAccess_patientStudySearchButton" 
                        		class="gadgetQuickAccess_patientStudySearchButton"><%=LC.L_Search%></button>
                    </form>
                    </td>
                </tr>
            </table>
            </td>
        </tr>
        <tr valign="top">
            <td width="50%">
                <table class="gdt-table" style="width:100%;">
                    <tr>
                        <td>
                        <table class="gdt-header-row" style="width:100%;">
                            <tr>
                            <td><%=LC.L_My_Links%></td>
                            <td align="right" valign="middle">
                                <a href="ulinkBrowser.jsp?srcmenu=tdMenuBarItem13&selectedTab=2" ><%=LC.L_All %><!-- <img 
                                    id="editMyLinksImg" title="Jump to Edit Links" align="middle" align="middle"
                                    src="./images/edit.gif" style="height:18px; width:18px; border:none; padding:1px"
                                /> --></a>
                                &nbsp;
                                <a href="javascript:gadgetQuickAccess.bothLinks.addNewLink('myLinks');" >
                                    <img id="addMyLinksImg" title="<%=LC.L_Add_NewLink%>" align="middle" align="middle"
                                            src="./images/Add.gif" style="height:18px; width:18px; border:none; padding:1px"/>
                                </a>
                            </td>
                        </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <div id="gadgetQuickAccess_myLinksNewLink" style="display:none">
                            <div id="gadgetQuickAccess_myLinksFormNewLinkDiv">
                            <s:form name="gadgetQuickAccess_formNewLink" id="gadgetQuickAccess_formNewLink" 
                                    onsubmit="return false;">
                            <table id="gadgetQuickAccess_myLinksFormNewLinkTable" class="gdt-table" style="width:100%;">
                            <tr>
                                <td>
                                    <table id="gadgetQuickAccess_myLinksFormNewLinkHeader" style="width:100%;">
                                    <tr>
                                        <td><b><%=LC.L_Add_NewLink%></b></td>
                                        <td align="right"><a href="linklist.jsp?mode=N&srcmenu=tdMenuBarItem13&selectedTab=2&lnkType=user"><%=LC.L_Add_Multi%></a></td>
                                    </tr>
                                    </table>
                                    <table id="gadgetQuickAccess_myLinksFormEditLinkHeader" style="width:100%; display:none">
                                    <tr>
                                        <td><div id='gadgetQuickAccess_myLinksFormEditLinkHeadderLabel'></div></td>
                                        <td align="right"></td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <div id="gadgetQuickAccessJB.myLinksLinkDisplay_error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <label for="gadgetQuickAccessJB.myLinksLinkDisplay"><%=LC.L_Display_Text%></label><FONT class="Mandatory"> *</FONT><br/>
                                
                                <s:textfield id="gadgetQuickAccessJB.myLinksLinkDisplay" cssClass="gdt-input-text"
                                        name="gadgetQuickAccessJB.myLinksLinkDisplay" type="text" size="30" maxlength="100" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <div id="gadgetQuickAccessJB.myLinksLinkUrl_error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <label for="gadgetQuickAccessJB.myLinksLinkUrl"><%=LC.L_Url%></label><FONT class="Mandatory"> *</FONT><br/>
                                <s:textfield id="gadgetQuickAccessJB.myLinksLinkUrl" cssClass="gdt-input-text"
                                        name="gadgetQuickAccessJB.myLinksLinkUrl" type="text" size="30" maxlength="100" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <div id="gadgetQuickAccessJB.myLinksLinkSection_error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <label for="gadgetQuickAccessJB.myLinksLinkSection"><%=LC.L_Section%></label><br/>
                                <s:textfield id="gadgetQuickAccessJB.myLinksLinkSection" cssClass="gdt-input-text"
                                        name="gadgetQuickAccessJB.myLinksLinkSection" type="text" size="30" maxlength="100" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <div id="gadgetQuickAccessJB.myLinksESign_error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <label for="gadgetQuickAccessJB.myLinksESign"><%=LC.L_EHypenSign%></label><FONT class="Mandatory"> *</FONT><br/>
                                <s:password id="gadgetQuickAccessJB.myLinksESign" cssClass="gdt-input-password"
                                        name="gadgetQuickAccessJB.myLinksESign" size="30" maxlength="8" 
                                        onkeyup="replicateESign(this.id);" />
                                <s:hidden id="gadgetQuickAccessJB.myLinksLinkId" 
                                        name="gadgetQuickAccessJB.myLinksLinkId" />
                                </td>
                            </tr>
                            <tr>
                                <td><div id="gadgetQuickAccess_formNewLink_resultMsg"></div></td>
                            </tr>
                            <tr>
                                <td>
                                    <span id="gadgetQuickAccessJB.myLinksESign_eSign"></span>
                                </td>
                            </tr>
                            <tr id="gadgetQuickAccess_newMyLinkButtonRow" align="right">
                                <td>
                                
                                <button type="submit" id="gadgetQuickAccess_saveNewMyLinkButton" 
             	                        class="gadgetQuickAccess_saveNewMyLinkButton"><%=LC.L_Save%></button>&nbsp;
                                <button type="button" id="gadgetQuickAccess_cancelNewMyLinkButton" 
             	                        class="gadgetQuickAccess_cancelNewMyLinkButton"><%=LC.L_Cancel%></button>
                                </td>
                            </tr>
                            </table>
                            </s:form>
                            </div>
                            <div style="height:5px;"></div>
                            <div id="gadgetQuickAccess_myLinksFormDeleteLinkDiv">
                            <s:form name="gadgetQuickAccess_formDeleteLink" id="gadgetQuickAccess_formDeleteLink" 
                                    onsubmit="gadgetQuickAccess.myLinks.deleteLink(); return false;">
                            <table id="gadgetQuickAccess_myLinksFormDeleteLinkTable" class="gdt-table" style="width:100%;">
                            <tr>
                                <td>
                                    <table style="width:100%; ">
                                    <tr>
                                        <td><div id='gadgetQuickAccess_myLinksFormDeleteLinkHeadderLabel'></div></td>
                                        <td align="right"></td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <div id="gadgetQuickAccessJB.myLinksDeleteESign_error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td><div id="gadgetQuickAccess_formDeleteLink_resultMsg"></div></td>
                            </tr>
                            <tr>
                                <td>
                                <label for="gadgetQuickAccessJB.myLinksDeleteLinkId"><%=LC.L_EHypenSign%></label><FONT class="Mandatory"> *</FONT><br/>
                                <s:hidden id="gadgetQuickAccessJB.myLinksDeleteLinkId" 
                                        name="gadgetQuickAccessJB.myLinksDeleteLinkId" />
                                <s:password id="gadgetQuickAccessJB.myLinksDeleteESign" cssClass="gdt-input-password"
                                        name="gadgetQuickAccessJB.myLinksDeleteESign" size="30" maxlength="8" 
                                        onkeyup="replicateESign(this.id);" />
                                </td>
                            </tr>
                            <tr id="gadgetQuickAccess_deleteMyLinkButtonRow" align="right">
                                <td>
                                <span id="gadgetQuickAccessJB.myLinksDeleteESign_eSign" style="font-size:8pt;"></span>&nbsp;&nbsp;
                                <button type="button" id="gadgetQuickAccess_deleteMyLinkButton" 
             	                        class="gadgetQuickAccess_deleteMyLinkButton"><%=LC.L_Delete%></button>&nbsp;
                                <button type="button" id="gadgetQuickAccess_cancelMyLinkButton" 
             	                        class="gadgetQuickAccess_cancelMyLinkButton"><%=LC.L_Cancel%></button>
                                </td>
                            </tr>
                            </table>
                            </s:form>
                            </div>
                        </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <div id="gadgetQuickAccess_myLinksHTML"></div>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="50%">
                <s:if test="%{gadgetQuickAccessJB.hasAcctLinksViewRights}" >
                <table class="gdt-table" style="width:100%;">
                    <tr>
                        <td>
                        <table class="gdt-header-row" style="width:100%;">
                            <tr>
                            <td><%=LC.L_Quick_Links%></td>
                            <td align="right" valign="middle">
                                <a href="accountlinkbrowser.jsp?srcmenu=tdMenuBarItem2&selectedTab=4" ><%=LC.L_All%><!-- <img 
                                    id="editMyLinksImg" title="Jump to Edit Links" align="middle" align="middle"
                                    src="./images/edit.gif" style="height:18px; width:18px; border:none; padding:1px"
                                /> --></a>
                                &nbsp;
                                <s:if test="%{gadgetQuickAccessJB.hasAcctLinksNewRights}" >
                                <a href="javascript:gadgetQuickAccess.bothLinks.addNewLink('acctLinks');" >
                                    <img id="addMyLinksImg" title="<%=LC.L_Add_NewLink%>" align="middle" align="middle"
                                            src="./images/Add.gif" style="height:18px; width:18px; border:none; padding:1px"/>
                                </a>
                                </s:if>
                            </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <div id="gadgetQuickAccess_acctLinksNewLink" style="display:none">
                            <div id="gadgetQuickAccess_acctLinksFormNewLinkDiv">
                            <s:form name="gadgetQuickAccess_formNewAcctLink" id="gadgetQuickAccess_formNewAcctLink" 
                                    onsubmit="return false;">
                            <table id="gadgetQuickAccess_acctLinksFormNewLinkTable" class="gdt-table" style="width:100%;">
                            <tr>
                                <td>
                                    <table id="gadgetQuickAccess_acctLinksFormNewLinkHeader" style="width:100%;">
                                    <tr>
                                        <td><b><%=LC.L_Add_NewLink%></b></td>
                                        <td align="right"></td>
                                    </tr>
                                    </table>
                                    <table id="gadgetQuickAccess_acctLinksFormEditLinkHeader" style="width:100%; display:none">
                                    <tr>
                                        <td><div id='gadgetQuickAccess_acctLinksFormEditLinkHeadderLabel'></div></td>
                                        <td align="right"></td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <div id="gadgetQuickAccessJB.acctLinksLinkDisplay_error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <label for="gadgetQuickAccessJB.acctLinksLinkDisplay"><%=LC.L_Display_Text%></label><FONT class="Mandatory"> *</FONT><br/>
                                
                                <s:textfield id="gadgetQuickAccessJB.acctLinksLinkDisplay" cssClass="gdt-input-text"
                                        name="gadgetQuickAccessJB.acctLinksLinkDisplay" type="text" size="30" maxlength="100" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <div id="gadgetQuickAccessJB.acctLinksLinkUrl_error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <label for="gadgetQuickAccessJB.acctLinksLinkUrl"><%=LC.L_Url%></label><FONT class="Mandatory"> *</FONT><br/>
                                <s:textfield id="gadgetQuickAccessJB.acctLinksLinkUrl" cssClass="gdt-input-text"
                                        name="gadgetQuickAccessJB.acctLinksLinkUrl" type="text" size="30" maxlength="100" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <div id="gadgetQuickAccessJB.acctLinksLinkSection_error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <label for="gadgetQuickAccessJB.acctLinksLinkSection"><%=LC.L_Section%></label><br/>
                                <s:textfield id="gadgetQuickAccessJB.acctLinksLinkSection" cssClass="gdt-input-text"
                                        name="gadgetQuickAccessJB.acctLinksLinkSection" type="text" size="30" maxlength="100" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <div id="gadgetQuickAccessJB.acctLinksESign_error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <label for="gadgetQuickAccessJB.acctLinksESign"><%=LC.L_EHypenSign%></label><FONT class="Mandatory"> *</FONT><br/>
                                <s:password id="gadgetQuickAccessJB.acctLinksESign" cssClass="gdt-input-password"
                                        name="gadgetQuickAccessJB.acctLinksESign" size="30" maxlength="8" 
                                        onkeyup="replicateESign(this.id);" />
                                <s:hidden id="gadgetQuickAccessJB.acctLinksLinkId" 
                                        name="gadgetQuickAccessJB.acctLinksLinkId" />
                                </td>
                            </tr>
                            <tr>
                                <td><div id="gadgetQuickAccess_formNewAcctLink_resultMsg"></div></td>
                            </tr>
                            <tr>
                                <td>
                                    <span id="gadgetQuickAccessJB.acctLinksESign_eSign"></span>
                                </td>
                            </tr>
                            <tr id="gadgetQuickAccess_newAcctLinkButtonRow" align="right">
                                <td>
                                
                                <button type="submit" id="gadgetQuickAccess_saveNewAcctLinkButton" 
             	                        class="gadgetQuickAccess_saveNewAcctLinkButton"><%=LC.L_Save%></button>&nbsp;
                                <button type="button" id="gadgetQuickAccess_cancelNewAcctLinkButton" 
             	                        class="gadgetQuickAccess_cancelNewAcctLinkButton"><%=LC.L_Cancel%></button>
                                </td>
                            </tr>
                            </table>
                            </s:form>
                            </div>
                            <div style="height:5px;"></div>
                            <div id="gadgetQuickAccess_acctLinksFormDeleteLinkDiv">
                            <s:form name="gadgetQuickAccess_formDeleteAcctLink" id="gadgetQuickAccess_formDeleteAcctLink" 
                                    onsubmit="gadgetQuickAccess.acctLinks.deleteLink(); return false;">
                            <table id="gadgetQuickAccess_acctLinksFormDeleteLinkTable" class="gdt-table" style="width:100%;">
                            <tr>
                                <td>
                                    <table style="width:100%; ">
                                    <tr>
                                        <td><div id='gadgetQuickAccess_acctLinksFormDeleteLinkHeadderLabel'></div></td>
                                        <td align="right"></td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <div id="gadgetQuickAccessJB.acctLinksDeleteESign_error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td><div id="gadgetQuickAccess_formDeleteAcctLink_resultMsg"></div></td>
                            </tr>
                            <tr>
                                <td>
                                <label for="gadgetQuickAccessJB.acctLinksDeleteLinkId"><%=LC.L_EHypenSign%></label><FONT class="Mandatory"> *</FONT><br/>
                                <s:hidden id="gadgetQuickAccessJB.acctLinksDeleteLinkId" 
                                        name="gadgetQuickAccessJB.acctLinksDeleteLinkId" />
                                <s:password id="gadgetQuickAccessJB.acctLinksDeleteESign" cssClass="gdt-input-password"
                                        name="gadgetQuickAccessJB.acctLinksDeleteESign" size="30" maxlength="8" 
                                        onkeyup="replicateESign(this.id);" />
                                </td>
                            </tr>
                            <tr id="gadgetQuickAccess_deleteAcctLinkButtonRow" align="right">
                                <td>
                                <span id="gadgetQuickAccessJB.acctLinksDeleteESign_eSign" style="font-size:8pt;"></span>&nbsp;&nbsp;
                                <button type="button" id="gadgetQuickAccess_deleteAcctLinkButton" 
             	                        class="gadgetQuickAccess_deleteAcctLinkButton"><%=LC.L_Delete%></button>&nbsp;
                                <button type="button" id="gadgetQuickAccess_cancelAcctLinkButton" 
             	                        class="gadgetQuickAccess_cancelAcctLinkButton"><%=LC.L_Cancel%></button>
                                </td>
                            </tr>
                            </table>
                            </s:form>
                            </div>
                        </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <div id="gadgetQuickAccess_acctLinksHTML"></div>
                        </td>
                    </tr>
                </table>
                </s:if>
            </td>
        </tr>
    </table>
    <div id="gadgetQuickAccess_end"></div>
</div>
