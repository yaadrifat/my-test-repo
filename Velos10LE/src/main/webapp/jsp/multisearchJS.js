	var xmlArray = new Array();
	var xslArray = new Array();
	var conArray = new Array();
    var sIndex = new Array();
    var propArray = new Array();
	var rNode=new Array();
	var cNodes=new Array();
	var iTotal = new Array();
	var sTyp ;
	var arrayPos ;
	
	var Select = L_Select;
	var To = L_To;
	var Of = L_Of;
	var More = L_More;
	var Configure = L_Configure;
	var Back = L_Back;

//All the values for each browser will be stored in a different position of array
function mainFunc(resultSet) {		
	if (resultSet==2) {
		arrayPos=resultSet-1;
		xmlNo=resultSet+2; //This will be used to refer W4
	}else{
		xmlNo=resultSet;
		arrayPos=resultSet-1; //This will be used to refer W1
	}
	
	xmlArray[arrayPos] = "W" + xmlNo;
	xmlNo++;
	xslArray[arrayPos]="W" + xmlNo;

	xmlNo++;

	conArray[arrayPos]="W" + xmlNo;
	


	sTyp = eval(conArray[arrayPos]).selectSingleNode("properties/property[@name='sortType']").getAttribute("value");
	rNode[arrayPos] = eval(xmlArray[arrayPos]).selectSingleNode("Result");
	iTotal[arrayPos] = parseInt(rNode[arrayPos].getAttribute("total"));        	

    cNodes[arrayPos] = rNode[arrayPos].selectNodes("Header/field");

	for(i=0; i<cNodes[arrayPos].length; i++)
     {
    	temp = cNodes[arrayPos].item(i);					
    	propArray[i] = new Properties2(temp.getAttribute("name"), temp.getAttribute("sorttype"), temp.getAttribute("order"));
		
	}

	var fRow = eval(rNode[arrayPos]).selectSingleNode("row");
    if (fRow==null)
	{
    	trCol = eval(xslArray[arrayPos]).createElement("td");
		trCol.setAttribute("colspan", "" + cNodes[arrayPos].length)
    	trCol.text = eval(conArray[arrayPos]).selectSingleNode("properties/property[@name='noMatch']").getAttribute("value");
    	trNode = eval(xslArray[arrayPos]).createElement("tr").appendChild(trCol);						
     	var tNode = eval(xslArray[arrayPos]).selectSingleNode("/xsl:stylesheet/xsl:template[@match='Result']/table");
    	var bNode = tNode.selectSingleNode("tbody");
    	bNode.replaceChild(trNode, bNode.selectSingleNode("xsl:for-each"));  									
    }

      else
    {
	
		sIndex[arrayPos] = parseInt(fRow.getAttribute("id"));																 
    	openPage2(1,resultSet); //(which page-next clicked,browser)
    } //fRow ends


    function conDisp2()
    {
    	/*window.open("Configure.html", "Configure");*****/
		window.open("Configure.html", Configure);
    }
    
    var reg = new RegExp('^[\\n,\\r,\\t, ]$');
    function trim2(sVal)
    {
    	return ltrim2(rtrim2(sVal));
    }
     
    function rtrim2(sVal)
    {
    	var len = sVal.length;
    	return (reg.test(sVal.substring(len - 1))? rtrim2(sVal.substring(0, len - 1)): sVal);
    }
     
    function ltrim2(sVal)
    {
    	var len = sVal.length;
    	return (reg.test(sVal.substring(0, 1))? ltrim2(sVal.substring(1)): sVal);
    }
    
    function filterResult2(val)
    {
    	document.forms.group.filter.value = val;
    
    	document.forms.group.submit();
    }


}//end mainFunc


function openPage2(pNum,pg) { //(pNum - which page: is changed when next is clicked, browser)
	if(pg==2) {
	 	arrayPos=1; 
	 } else {
	 	arrayPos=0;
	 }

	var pLength = parseInt(eval(conArray[arrayPos]).selectSingleNode("properties/property[@name='clientsize']").getAttribute("value"));

	var iNum = sIndex[arrayPos] + (pNum-1)*pLength;

	var iEnd = iNum + pLength - 1;
			
	var rLength = parseInt(eval(rNode[arrayPos]).lastChild.getAttribute("id"));
	var tNode = eval(xslArray[arrayPos]).selectSingleNode("/xsl:stylesheet/xsl:template[@match='Result']/table/tbody/xsl:for-each");
	/*tNode.setAttribute("select", "row[@id $ge$ " + iNum + " $and$ @id $le$ " + iEnd + "]");*****/
	tNode.setAttribute(Select, "row[@id $ge$ " + iNum + " $and$ @id $le$ " + iEnd + "]");
	/*var sMore = "<font face='Arial, Helvetica, sans-serif' color='#cc6633' size='2'>" + iNum + " to " + (rLength<iEnd? rLength: iEnd)+ " of " + iTotal[arrayPos] +" Record(s)<br/></font><center>";*****/
	var sMore = "<font face='Arial, Helvetica, sans-serif' color='#cc6633' size='2'>" + iNum + To + (rLength<iEnd? rLength: iEnd)+ Of + iTotal[arrayPos] +" Record(s)<br/></font><center>";
	ilastRecord = parseInt(eval(rNode[arrayPos]).selectNodes("row").length)

	var pCount = Math.ceil(ilastRecord/pLength);

	if(pCount>1)
		var pgnum = parseInt(rNode[pg-1].getAttribute("pagecount"));
		var ipgnum=0;
		if(pgnum==null || pgnum==1) {
			ipgnum=0;
		} else {
			ipgnum = (pgnum*pCount)-(pCount);
		}
		for(i=0;i<(pCount);i++){
			sMore += "<a href='javascript:openPage2(" + (i + 1) + ',' +pg+   ")'>" + (ipgnum + 1) + "</a> | ";
			ipgnum++;
		}
	sMore += "<br>";
	if(sIndex[arrayPos]!=1)
		/*sMore += "<input type='image' src='../images/jpg/Back.gif' onClick='scroll2(-1,"+pg+")' alt='Back'/>";*****/
		sMore += "<input type='image' src='../images/jpg/Back.gif' onClick='scroll2(-1,"+pg+")' alt='"+Back+"'/>";

	if(sIndex[arrayPos] + ilastRecord !=iTotal[arrayPos] + 1)
		/*sMore += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='image' src='../images/jpg/Next.gif' onClick='scroll2(1,"+pg+")' alt='More'/>";*****/
		sMore += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='image' src='../images/jpg/Next.gif' onClick='scroll2(1,"+pg+")' alt='"+More+"'/>";
	sMore += "</center>";
	
	eval('SResult'+pg).innerHTML = eval(xmlArray[arrayPos]).transformNode(eval(xslArray[arrayPos]).documentElement) + sMore;
	 
//			 SResult.innerHTML = xmldoc.transformNode(xsldoc.documentElement) + sMore;
}



function scroll2(val, spg) //(val is 1 in case of Next and -1 in case of back
{
	val += parseInt(rNode[spg-1].getAttribute("pagecount"));		
	var frms = document.forms
	para = "";
	i=spg + 2; //+2 has been hardcoded to refer to the Appropriate Form
		elmts = frms[i].elements
		if (elmts.length>0)
			{
				for(j=0; j<elmts.length; j++)
				{
					if ((elmts[j].type=="hidden")||(elmts[j].type=="text"))
						para += "&" + elmts[j].name+spg+ "=" + elmts[j].value;
					else if (elmts[j].type=="select-multiple")
					{
						arr = elmts[j].options
						for(k=0; k<arr.length; k++)
							if(arr[k].selected)
								para += "&" + elmts[j].name+spg + "=" + arr[k].value;
					}
					else if (elmts[j].type=="select-one")
						para += "&" + elmts[j].name + "=" + elmts[j].options[elmts[j].selectedIndex].value;
					else if ((elmts[j].type=="checkbox")||(elmts[j].type=="radio"))
						if (elmts[j].checked)
							para += "&" + elmts[j].name+spg+ "=" + elmts[j].value;
				}
			}										
var preval = findval(spg);
document.forms[i].action = opnrhref + "?scroll"+spg+"=" + val + para+ "&resultSetPg="+spg+"&"+preval;
    document.forms[i].submit();
}


function findval(sti) {
var sUrl = window.location.href;
if(sti==1){
	 sti+=1;
	 }
	 else{
	 sti=1;
	 }
var startpos = sUrl.indexOf("scroll"+sti);
if(startpos == -1) return "";
var endpos = sUrl.indexOf("scroll"+(sti+1));
if((endpos == -1) || (endpos < startpos)) endpos = sUrl.length;
return (sUrl.substring(startpos, endpos));
}


    function sortField(nam)
{
//		   	ind = getFldIndex2(nam);
	ind = 0;
	if (sTyp=='S')
	{
		document.forms[arrayPos].sortType.value = propArray[ind].stype
		document.forms[arrayPos].sortOrder.value = propArray[ind].order
		document.forms[arrayPos].sortCol.value = ind;
		document.forms[arrayPos].action = opnrhref;
		document.forms[arrayPos].submit();
	}
/*        	else
	{
		var tArray = new Array();
		chld = rNode[arrayPos].childNodes;
		len = chld.length - 1;
		for(i=0;i<len; i++)
		{
			tArray[i] = chld.item(1);
			rNode[arrayPos].removeChild(chld.item(1));
		}

		var tNew;
		fAsc = propArray[ind].order;
		switch(propArray[ind].stype)
		{
			case 'N':
				tNew = tArray.sort(sortNumbers2);
				break;
			case 'D':
				tNew = tArray.sort(sortDates2);
				break;
			default :
				tNew = tArray.sort(sortStrings2);
				break;
		}

		for(i=0;i<tArray.length;i++)
		{
			tArray[i].setAttribute("id", (sIndex[arrayPos] + i))
			rNode[arrayPos].appendChild(tArray[i]);
		}

		for(i=0;i<cNodes[arrayPos].length;i++)
		{
			cNodes[arrayPos].item(i).removeAttribute("order");
			propArray[i].order = 1;
		}
		propArray[ind].order = -1*fAsc;
		cNodes[arrayPos].item(ind).setAttribute("order", "" + -1*fAsc);

		rNode[arrayPos].selectSingleNode("Header/field[@name = '"+trim2(nam)+"']").setAttribute("order", (propArray[ind].order));
		openPage2(1);
	}*/ 		
}




    function Properties2(nam, typ, ord)
    {
		this.name=nam;
		this.order=(ord==null? '1': ord);
    	this.stype=typ;
    }
	


     
function getFldIndex2(val)
{ 
	for (i=0; i<cNodes[arrayPos].length; i++){
		if (trim2(val) == trim2(cNodes[arrayPos].item(i).getAttribute("name"))) return i;
	}
	return 0;
}

var ind, fAsc;


function sortNumbers2(a, b)
{
	aNode = a.selectSingleNode("field[" + ind + "]").text;
	bNode = b.selectSingleNode("field[" + ind + "]").text;
	return fAsc*(parseInt(aNode) - parseInt(bNode));
}

function sortDates2(a, b)
{
	aNode = a.selectSingleNode("field[" + ind + "]").text;
	bNode = b.selectSingleNode("field[" + ind + "]").text;
	return fAsc*((new Date(a) < new Date(b))? -1: 1);
}

function sortStrings2(a, b)
{
	aNode = a.selectSingleNode("field[" + ind + "]").text;
	bNode = b.selectSingleNode("field[" + ind + "]").text;
	return fAsc*(aNode < bNode? -1: 1);
}	
