<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title>Invoice Creation >> Step 2</title>
<%@ page import ="com.velos.eres.service.util.*,java.util.*, java.io.*, org.w3c.dom.*"%>
<%@ page language = "java" import ="com.velos.eres.service.util.StringUtil,com.velos.esch.business.common.*,java.net.URLEncoder,com.velos.eres.web.studyRights.StudyRightsJB"%>
<%@ page language = "java" import = "com.velos.eres.business.common.TeamDao"%>
<jsp:useBean id="sess" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<meta http-equiv="Content-type" content="text/html; charset=UTF-8">
<style>
.yui-skin-sam .yui-dt th.hiddenHeader{ 
	display: none;
}
.yui-skin-sam .yui-dt th.yui-dt-hidden,
.yui-skin-sam .yui-dt tr.yui-dt-odd .yui-dt-hidden,
.yui-skin-sam .yui-dt tr.yui-dt-even .yui-dt-hidden {
border:0px;
}
</style>
<%
String src;
src= request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<jsp:include page="ui-include.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<script Language="javascript">
function validateNumber(evt){
	if(evt.shiftKey) return false;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	//delete, backspace & arrow keys
	if (charCode == 8 || charCode == 46 || charCode == 37 || charCode == 39)
		return true;

	if (charCode > 31 && (!((47 < charCode && charCode  < 58) || (95 < charCode &&  charCode < 106))))
		return false;
	return true;
}
</script>
</head>
<body style = "overflow-x:hidden; overflow-y:auto" class="yui-skin-sam yui-dt yui-dt-liner">
<div id="overDiv" style="position:absolute;visibility:hidden; z-index:1100;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<%
HttpSession tSession = request.getSession(true);
StringBuffer urlParamSB = new StringBuffer();
int pageRight=0;

if (sess.isValidSession(tSession)){
	String calledFrom = request.getParameter("calledFrom");
	String tab = request.getParameter("selectedTab");
	String studyId =request.getParameter("studyId");
	String userId = (String) tSession.getValue("userId");	
	
	// Fetching the Page rights
	StudyRightsJB stdRightsB = new StudyRightsJB();
	pageRight = stdRightsB.getStudyRightsForModule(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userId), "MILESTONES");
	tSession.setAttribute("mileRight",(new Integer(pageRight)).toString());
	String invNumber = request.getParameter("invNumber");
	String invNumbers = request.getParameter("invNumbers");
	/* FIN-22382_0926012 Enhancement 31-Oct-2012 -Sudhir*/
	String studyNumber = request.getParameter("studyNumber");
	String paymentDueIn = request.getParameter("paymentDueIn");
	String paymentDueUnit = request.getParameter("paymentDueUnit");
	String addressedTo = request.getParameter("addressedTo");
	String sentFrom = request.getParameter("sentFrom");
	String invDate = request.getParameter("invDate");
	String milestoneReceivableStatus = request.getParameter("milestoneReceivableStatus");
	String dateRangeType = request.getParameter("dateRangeType");
	String invNotes = request.getParameter("invNotes");
	// Bug-13142,13159 Date- 28 Nov 2012 Ankit
	//String dtFilterDateFrom = request.getParameter("dtFilterDateFrom");
	//String dtFilterDateTo = request.getParameter("dtFilterDateTo");
	String dtFilterDateFrom = request.getParameter("drDateFrom");
	String dtFilterDateTo = request.getParameter("drDateTo");
	String yearFilter= request.getParameter("yearFilter");
	String monthFilterYear= request.getParameter("monthFilterYear");
	String monthFilter= request.getParameter("monthFilter");
	String dSites = request.getParameter("dSites");

	SchCodeDao schDao=new SchCodeDao();
	String codeSubType = null;
	String CoverType = null;
	StringBuilder sb = new StringBuilder();
	if(request.getParameter("CovType") != null){
		String[] ItemNames;		
		ItemNames = request.getParameterValues("CovType");
		int selChkboxLen=ItemNames.length;
		for(int i = 0; i < selChkboxLen; i++){
			if(!ItemNames[i].equalsIgnoreCase("All")){
				int pk_covtype=StringUtil.stringToNum(ItemNames[i]);
				codeSubType = ItemNames[i];
				if(sb.length()>0){
					sb.append(",");
				}
				sb.append(""+codeSubType+"");
			}else{
				sb.append(ItemNames[i].toString());
			}
		}
		CoverType = sb.toString();
	}
	
    urlParamSB = new StringBuffer();
	urlParamSB.append("studyId=").append(StringUtil.htmlEncodeXss(studyId))
		.append("&milestoneReceivableStatus=").append(StringUtil.htmlEncodeXss(milestoneReceivableStatus))
		.append("&dateRangeType=").append(StringUtil.htmlEncodeXss(dateRangeType))
		.append("&dtFilterDateFrom=").append(StringUtil.htmlEncodeXss(dtFilterDateFrom))
		.append("&dtFilterDateTo=").append(StringUtil.htmlEncodeXss(dtFilterDateTo))
		.append("&dSites=").append(StringUtil.htmlEncodeXss(dSites))
		.append("&yearFilter=").append(StringUtil.htmlEncodeXss(yearFilter))
		.append("&monthFilterYear=").append(StringUtil.htmlEncodeXss(monthFilterYear))
		.append("&monthFilter=").append(StringUtil.htmlEncodeXss(monthFilter))
		.append("&coverType=").append(StringUtil.htmlEncodeXss(CoverType));

%>
<script type="text/javascript">
<%if (StringUtil.isAccessibleFor(pageRight,'N')){%>
function reloadGenerateInvoiceGrid(filters) {
	
	YAHOO.example.generateInvoiceGrid= function() {
		var args = {
			urlParams: "<%=urlParamSB.toString()%>",
			dataTable: "generateInvoiceGrid",
			filters: ""
		};
	
    	myGenerateInvoiceGrid= new VELOS.generateInvoiceGrid('fetchInvoiceJSON.jsp', args);
    	myGenerateInvoiceGrid.startRequest();
    }();
}
YAHOO.util.Event.addListener(window, "load", function() {
	reloadGenerateInvoiceGrid();
});
<%}%>
</script>
<DIV class="BrowserTopN" id="div1"> 
	<jsp:include page="milestonetabs.jsp" flush="true">
	<jsp:param name="studyId" value="<%=studyId%>"/>
	<jsp:param name="selectedTab" value="<%=tab%>"/>
	</jsp:include>
</DIV>
<Form name="genInvoice" id="genInvoice" action="updateInvoiceDetails.jsp" method="post" >
<DIV class="BrowserBotN BrowserBotN_M_2" id="div1" style="overflow: scroll;"> 
	<input type="hidden" id ="pageRight" name="pageRight" value="<%=pageRight%>"/>
	<input type="hidden" id="calledFrom" name="calledFrom" value="<%=calledFrom%>"/>
	<input type="hidden" id ="invNumber" name="invNumber" value="<%=(invNumber==null || ("").equalsIgnoreCase(invNumber))?invNumbers:invNumber%>"/>
	<input type="hidden" id="studyNumber" name="studyNumber" value="<%= studyNumber%>" />
	<input type="hidden" id ="studyId" name="studyId" value="<%=studyId%>"/>
	<input type="hidden" id ="paymentDueIn" name="paymentDueIn" value="<%=paymentDueIn%>"/>
	<input type="hidden" id ="paymentDueUnit" name="paymentDueUnit" value="<%=paymentDueUnit%>"/>
	<input type="hidden" id ="addressedTo" name="addressedTo" value="<%=addressedTo%>"/>
	<input type="hidden" id ="sentFrom" name="sentFrom" value="<%=sentFrom%>"/>
	<input type="hidden" id ="invDate" name="invDate" value="<%=invDate%>"/>
	<input type="hidden" id ="milestoneReceivableStatus" name="milestoneReceivableStatus" value="<%=milestoneReceivableStatus%>"/>
	<input type="hidden" id ="dateRangeType" name="dateRangeType" value="<%=dateRangeType%>"/>
	<input type="hidden" id ="invNotes" name="invNotes" value="<%=invNotes%>"/>
	<input type="hidden" id ="dtFilterDateFrom" name="dtFilterDateFrom" value="<%=dtFilterDateFrom%>"/>
	<input type="hidden" id ="dtFilterDateTo" name="dtFilterDateTo" value="<%=dtFilterDateTo%>"/>
	<input type="hidden" id ="dSites" name="dSites" value="<%=dSites%>"/>
	<input type="hidden" id ="coverType" name="coverType" value="<%=CoverType%>"/>

	<div id="generateInvoiceGrid" ></div>
</DIV>
</Form>
<%
//end of session
} else{
%>

<jsp:include page="timeout.html" flush='true'></jsp:include>
<%}%>

	<div class = "myHomebottomPanel">
	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</body>

</html><%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
%>
