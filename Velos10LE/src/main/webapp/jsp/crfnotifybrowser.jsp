<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_Crf_NotifyBrowser%><%--CRF Notify Browser*****--%></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<% String src="";
src= request.getParameter("srcmenu");
%>	
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="crfNotifyB" scope="request" class="com.velos.esch.web.crfNotify.CrfNotifyJB"/>
<jsp:useBean id="crfB" scope="request" class="com.velos.esch.web.crf.CrfJB"/>
<jsp:useBean id="personB" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.DateUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.common.*,com.velos.esch.business.common.CrfNotifyDao,com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>

<body>
<%
  HttpSession tSession = request.getSession(true);
  if (sessionmaint.isValidSession(tSession))
   	{
		
		String patProtId= request.getParameter("patProtId");
		String studyId = request.getParameter("studyId");	
		String statDesc=request.getParameter("statDesc");
		String statid=request.getParameter("statid");	
		String studyVer = request.getParameter("studyVer");

		String crfId = request.getParameter("crfId");
		int personPK = 0;	
		personPK = EJBUtil.stringToNum(request.getParameter("pkey"));
	    personB.setPersonPKId(personPK);
 		personB.getPersonDetails();
		String uName = (String) tSession.getValue("userName");
		String patientId = personB.getPersonPId();
		String genderId = personB.getPersonGender();
		String dob = personB.getPersonDob();
		String gender = codeLst.getCodeDescription(EJBUtil.stringToNum(genderId));
		String yob = dob.substring(6,10);
		Calendar cal1 = new GregorianCalendar();		
		int age = cal1.get(Calendar.YEAR) - EJBUtil.stringToNum(yob);
		String visit = request.getParameter("visit");
		String eventName = request.getParameter("eventName");
		if(eventName == null){
			eventName = "";
		} else {
			eventName = eventName.replace('~',' ');
		}			
		
		crfB.setCrfId(EJBUtil.stringToNum(crfId));
		crfB.getCrfDetails();
		String crfNumber = crfB.getCrfNumber();
		String crfName = crfB.getCrfName();
%>
<br>
<DIV class="browserDefault" id="div1"> 
	
<P class = "userName"> <%= uName %> </P>
<P class="sectionHeadings"> <%=MC.M_MngPatsSch_CrfStat%><%--Manage <%=LC.Pat_Patients%> >> Schedule >> CRF Status*****--%> </P>
<jsp:include page="patienttabs.jsp" flush="true"> 
<jsp:param name="studyVer" value="<%=studyVer%>"/>
<jsp:param name="patientCode" value="<%=patientId%>"/>
</jsp:include>
<br>

<table width=100%>
 <tr>
	<td class=tdDefault width=30%>
		<B> <%=LC.L_Patient_Id%><%--<%=LC.Pat_Patient%> ID*****--%>:<%=patientId%> </B>
	</td>
	<td class=tdDefault width=20%>
		<B> <%=LC.L_Age%><%--Age*****--%>: <%=age%> <%=LC.L_Years_Lower%><%--years*****--%></B>
	</td>
	<td class=tdDefault width=20%>
		<B><%=LC.L_Gender%><%--Gender*****--%>: <%=gender%> </B>
	</td>
 </tr>
 <tr>
	<td width=20% colspan=3>
		<B><%=LC.L_Patient_Visit%><%--<%=LC.Pat_Patient%> Visit*****--%>: <%=visit%> </B>
	</td>
</tr>
<tr>	
	<td width=20% colspan=3>
		<B><%=LC.L_Event_Name%><%--Event Name*****--%>: <%=eventName%> </B>
	</td>
</tr>
<tr>	
	<td colspan=2>
		<B><%=LC.L_Crf_Num%><%--CRF Number*****--%>: <%=crfNumber%> </B>
		<BR>
		<B><%=LC.L_Crf_Name%><%--CRF Name*****--%>: <%=crfName%> </B>		
	</td>
	<td align=right>
<%
	eventName = eventName.replace(' ','~');
%>	
		<A HREF=crfnotify.jsp?srcmenu=<%=src%>&page=patientEnroll&selectedTab=3&pkey=<%=personPK%>&visit=<%=visit%>&eventName=<%=eventName%>&crfId=<%=crfId%>&mode=N&fromPage=crfnotifybrowser&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&patProtId=<%=patProtId%>&studyVer=<%=studyVer%>&patientCode=<%=patientId%>> <%=LC.L_Add_NewNotification%><%--Add New Notification*****--%> </A>
	</td>
</tr>
</table>
		

<TABLE border=0 cellPadding=1 cellSpacing=1 width="100%">
  <TR>
    <TH width=30%><%=LC.L_For_CrfStatus%><%--For CRF Status*****--%></TH>
    <TH width=70%><%=LC.L_Notify_Users%><%--Notify Users*****--%></TH>
  </TR>	 

<% 


  CrfNotifyDao crfNotifyDao = new CrfNotifyDao();
  crfNotifyDao = crfNotifyB.getCrfNotifyValues(EJBUtil.stringToNum(crfId)) ;
  
  ArrayList crfNotIds = null;
  ArrayList crfNotStats = null;
  ArrayList crfNotUsers = null; 
  
  crfNotIds = crfNotifyDao.getCrfNotIds();
  crfNotStats = crfNotifyDao.getCrfNotStats();
  crfNotUsers = crfNotifyDao.getCrfNotUsers();

  int lenCrf = crfNotifyDao.getCRows();
//  out.println("****" + lenCrf + "****");
	for(int i=0; i<lenCrf ; i++) 
	{
		String crfNotId = ((Integer) crfNotIds.get(i)).toString();
		String crfNotStat = (String)crfNotStats.get(i); 
		String crfNotUser = (String) crfNotUsers.get(i);
		
		crfNotId = (crfNotId == null)?"-":crfNotId;
		crfNotStat = (crfNotStat == null)?"-":crfNotStat;
		crfNotUser = (crfNotUser == null)?"-":crfNotUser;
		
	if(i%2==0){ 
%>	
	<TR class="browserEvenRow">	
<%
	}else{
%>
	<TR class="browserOddRow">
<% 
	} 
%>
	 
<%
		eventName = eventName.replace(' ','~');
%>	
	<td class=tdDefault><A HREF=crfnotify.jsp?srcmenu=<%=src%>&page=patientEnroll&selectedTab=3&pkey=<%=personPK%>&visit=<%=visit%>&eventName=<%=eventName%>&crfId=<%=crfId%>&crfNotifyId=<%=crfNotId%>&mode=M&fromPage=crfnotifybrowser&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&patProtId=<%=patProtId%>&studyVer=<%=studyVer%>&patientCode=<%=patientId%>> <%=crfNotStat%> </A></td>
<%
		eventName = eventName.replace('~',' ');
%>	
	<td class=tdDefault><%=crfNotUser%></td>
</tr>
		
<%	
	}
%>
  
  </table>

<%  
} //end of if session times out

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
<%

} //end of else body for session times out

%>
  

  <div> 
   <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>

<div class ="mainMenu" id = "emenu">

<jsp:include page="getmenu.jsp" flush="true"/>   

</div>



</body>

</html>


