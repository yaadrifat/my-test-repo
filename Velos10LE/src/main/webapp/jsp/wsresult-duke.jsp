<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<HTML>
<BODY>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*,com.velos.eres.business.common.CtrlDao,org.eclipse.jst.ws.*" %>
<jsp:useBean id="samplePatientDemographicsSoapProxyid" scope="session" class="com.velos.DHTS.PatientDemographicsSoapProxy" />
<%
//Set the values for needed parameters

if (request.getParameter("endpoint") != null && request.getParameter("endpoint").length() > 0)
samplePatientDemographicsSoapProxyid.setEndpoint(request.getParameter("endpoint"));
%>

<%
String mrn="",lastName="",givenName="",DOB="",sex="",birthCity="",birthState="",natnlty="",race="",deathIndicate=""
,bloodType="",personId="",responseMessage="",exceptionMsg="",rawDOB="",showMessage="";
int responseCode=-1;

String method = request.getParameter("method");
method=(method==null)?"":method;
int methodID = 0;
if (method.length() ==0) methodID = 26;


boolean gotMethod = false;

try {
switch (methodID){

case 26:
		String workStationDB="velos",userIdDB="WSVelos",pwdDB="HK75bc0fe3Jc";
		String ctrlKey="'WSUSER','WSPASSWORD'";
		/*CtrlDao ctlDao=new CtrlDao();
		ctlDao.getControlValues(ctrlKey);
		ArrayList wsValues=ctlDao.getCValue();
		ArrayList wsKeys=ctlDao.getCKey();
		userIdDB=(String)wsValues.get(wsKeys.indexOf("WSUSER"));
		pwdDB=(String)wsValues.get(wsKeys.indexOf("WSPASSWORD"));*/
		int agentIdDB=92;
        gotMethod = true;
        String forFutureUse_7id=  request.getParameter("forFutureUse49");
        java.lang.String forFutureUse_7idTemp  = forFutureUse_7id;
%>
        <jsp:useBean id="org1DukeHealth1DHTS1DemographicsParms_6id" scope="session" class="com.velos.DHTS.DemographicsParms" />
        <%
                org1DukeHealth1DHTS1DemographicsParms_6id.setForFutureUse(forFutureUse_7idTemp);
                String workstationID_10id=  request.getParameter("workstationID55");
                workstationID_10id=(workstationID_10id==null)?"":workstationID_10id;
                String workstationID = (workstationID_10id.length()==0)?workStationDB:workstationID_10id;
                String userID_11id=  request.getParameter("userID57");
                String userID_11idTemp  = (userID_11id==null)?userIdDB:userID_11id;
                String familyName_13id=  request.getParameter("familyName61");
                String familyName_13idTemp  = (familyName_13id==null)?"Velos":familyName_13id;
                String givenName_14id=  request.getParameter("givenName63");
                String givenName_14idTemp  = (givenName_14id==null)?"User":givenName_14id;
        %>
        <jsp:useBean id="org1DukeHealth1DHTS1PersonName_12id" scope="session" class="com.velos.DHTS.PersonName" />
        <%
                org1DukeHealth1DHTS1PersonName_12id.setFamilyName(familyName_13idTemp);
                org1DukeHealth1DHTS1PersonName_12id.setGivenName(givenName_14idTemp);
        %>
        <jsp:useBean id="org1DukeHealth1DHTS1ClientIdentifier_9id" scope="session" class="com.velos.DHTS.ClientIdentifier" />
        <%
                org1DukeHealth1DHTS1ClientIdentifier_9id.setWorkstationID(workstationID);
                org1DukeHealth1DHTS1ClientIdentifier_9id.setUserID(userID_11idTemp);
                org1DukeHealth1DHTS1ClientIdentifier_9id.setUserName(org1DukeHealth1DHTS1PersonName_12id);
                String mRN_16id=  request.getParameter("MRN");
                String mRN_16idTemp  = (mRN_16id==null)?"":mRN_16id;
                String hCFacility_17id=  request.getParameter("hCFacility");
                String hCFacility_17idTemp  = (hCFacility_17id==null)?"":hCFacility_17id;
        %>
        <jsp:useBean id="org1DukeHealth1DHTS1PatientIdentifier_15id" scope="session" class="com.velos.DHTS.PatientIdentifier" />
        <%
                org1DukeHealth1DHTS1PatientIdentifier_15id.setMRN(mRN_16idTemp);
                org1DukeHealth1DHTS1PatientIdentifier_15id.setHCFacility(hCFacility_17idTemp);
                String userID_19idTemp  = userID_11idTemp;
                String agentID_20id=  request.getParameter("agentID75");
                agentID_20id=(agentID_20id==null)?"":agentID_20id;
                int agentID_20idTemp  =(agentID_20id.length()==0)?agentIdDB:Integer.parseInt(agentID_20id);
                String pwd_21id=  request.getParameter("pwd77");
                String pwd_21idTemp  = (pwd_21id==null)?"":pwd_21id;
                pwd_21idTemp=(pwd_21idTemp.length()==0)?pwdDB:pwd_21idTemp;
        %>
        <jsp:useBean id="org1DukeHealth1DHTS1AgentIdentifier_18id" scope="session" class="com.velos.DHTS.AgentIdentifier" />
        <%
                org1DukeHealth1DHTS1AgentIdentifier_18id.setUserID(userID_19idTemp);
                org1DukeHealth1DHTS1AgentIdentifier_18id.setAgentID(agentID_20idTemp);
                org1DukeHealth1DHTS1AgentIdentifier_18id.setPwd(pwd_21idTemp);
        %>
        <jsp:useBean id="org1DukeHealth1DHTS1RequiredParms_8id" scope="session" class="com.velos.DHTS.RequiredParms" />
        <%
                org1DukeHealth1DHTS1RequiredParms_8id.setClient(org1DukeHealth1DHTS1ClientIdentifier_9id);
                org1DukeHealth1DHTS1RequiredParms_8id.setPatient(org1DukeHealth1DHTS1PatientIdentifier_15id);
                org1DukeHealth1DHTS1RequiredParms_8id.setAgent(org1DukeHealth1DHTS1AgentIdentifier_18id);
        %>
        <jsp:useBean id="org1DukeHealth1DHTS1DemographicsRequest_5id" scope="session" class="com.velos.DHTS.DemographicsRequest" />
        <%
                org1DukeHealth1DHTS1DemographicsRequest_5id.setDemographicsParms(org1DukeHealth1DHTS1DemographicsParms_6id);
                org1DukeHealth1DHTS1DemographicsRequest_5id.setRequiredInput(org1DukeHealth1DHTS1RequiredParms_8id);
                com.velos.DHTS.DemographicsResponse getPatientDemographics26mtemp = samplePatientDemographicsSoapProxyid.getPatientDemographics(org1DukeHealth1DHTS1DemographicsRequest_5id);
        if(getPatientDemographics26mtemp == null){
        %>
<%=getPatientDemographics26mtemp%>
<%
}else{
%>
<TABLE>
<%
if(getPatientDemographics26mtemp != null){
com.velos.DHTS.StatusReturn tebece0=getPatientDemographics26mtemp.getStatus();
if(tebece0 != null){
com.velos.DHTS.StatusEntry tebece1=tebece0.getPrimaryStatus();
if(tebece1 != null){
java.lang.String typelocation33 = tebece1.getLocation();
        String tempResultlocation33 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(typelocation33));
%>

        <%
        }}}
        %>

<TR>
<%
if(getPatientDemographics26mtemp != null){
com.velos.DHTS.StatusReturn tebece0=getPatientDemographics26mtemp.getStatus();
if(tebece0 != null){
com.velos.DHTS.StatusEntry tebece1=tebece0.getPrimaryStatus();
if(tebece1 != null){
java.util.Calendar typeeventDateTime35 = tebece1.getEventDateTime();
        java.text.DateFormat dateFormateventDateTime35 = java.text.DateFormat.getDateInstance();
        java.util.Date dateeventDateTime35 = typeeventDateTime35.getTime();
        String tempResulteventDateTime35 = org.eclipse.jst.ws.util.JspUtils.markup(dateFormateventDateTime35.format(dateeventDateTime35));
%>

        <%
        }}}
        %>

<%
if(getPatientDemographics26mtemp != null){
com.velos.DHTS.StatusReturn tebece0=getPatientDemographics26mtemp.getStatus();
if(tebece0 != null){
com.velos.DHTS.StatusEntry tebece1=tebece0.getPrimaryStatus();
if(tebece1 != null){
%>
<%
}}}
%>
<%
if(getPatientDemographics26mtemp != null){
com.velos.DHTS.StatusReturn tebece0=getPatientDemographics26mtemp.getStatus();
if(tebece0 != null){
com.velos.DHTS.StatusEntry tebece1=tebece0.getPrimaryStatus();
if(tebece1 != null){
java.lang.String typedescription39 = tebece1.getDescription();
        String tempResultdescription39 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(typedescription39));
        responseMessage=tempResultdescription39;
%>

        <%
        }}}
        %>
<%
if(getPatientDemographics26mtemp != null){
com.velos.DHTS.StatusReturn tebece0=getPatientDemographics26mtemp.getStatus();
if(tebece0 != null){
com.velos.DHTS.StatusEntry tebece1=tebece0.getPrimaryStatus();
if(tebece1 != null){
	responseCode=tebece1.getCode();
%>
<%
}}}
%>
<%
	if(getPatientDemographics26mtemp != null){
	com.velos.DHTS.DemographicsResponsePatientDemographicsData[] typepatientDemographics43 = getPatientDemographics26mtemp.getPatientDemographics();
	if (typepatientDemographics43!=null) {
        java.util.List listpatientDemographics43= java.util.Arrays.asList(typepatientDemographics43);
        com.velos.DHTS.DemographicsResponsePatientDemographicsData data=new com.velos.DHTS.DemographicsResponsePatientDemographicsData();
        if (listpatientDemographics43.size()>0)
        {
         data=(com.velos.DHTS.DemographicsResponsePatientDemographicsData)listpatientDemographics43.get(0);
         rawDOB=data.getDATEOFBIRTH();
         DOB=DateUtil.dateToString(DateUtil.stringToDate(data.getDATEOFBIRTH(),"yyyy-MM-dd"));
         givenName=(data.getGIVENNAME()==null)?"":data.getGIVENNAME();
         lastName=(data.getFAMILYNAME()==null)?"":data.getFAMILYNAME();
         birthCity=(data.getBIRTHCITY()==null)?"":data.getBIRTHCITY();
         birthState=(data.getBIRTHSTATE()==null)?"":data.getBIRTHSTATE();
         natnlty=(data.getNATIONALITY()==null)?"":data.getNATIONALITY();
         race=(data.getRACE()==null)?"":data.getRACE();
         deathIndicate=data.getDEATHINDICATOR();
         bloodType=data.getBLOODTYPE();
         personId=(data.getPERSONID()==null)?"":data.getPERSONID().toString();
         sex=data.getSEX();
         String temppatientDemographics43 = listpatientDemographics43.toString();
        }
	}
        else
        {

         showMessage="No"+LC.Pat_Patient+" Match Found for the Specified MRN and Organization.";
         }


%>


        <%
        }
        %>

<table width="100%" style="Font:arial;Font-size:12;font-weight:bold">
<% if (showMessage.length()>0) { %>
<tr width="100%">
<td colspan="3"><%=showMessage%></td>
</tr>
<%} if (personId.length()>0){ %>
<tr width="100%">
<td width="33%">Given Name:<%=givenName%></td><td align="center" width="33%">Last Name:<%=lastName %></td><td width="33%" align="right">Date Of Bith:<%=rawDOB%></td>
</tr>
<tr width="100%">
<td width="33%">Gender:<%=sex%></td><td align="center" width="33%">Race:<%=race%></td><td width="33%" align="right">Blood Type:<%=bloodType%></td>
</tr>
<tr width="100%">
<td width="33%">Birth City:<%=birthCity%></td><td align="center" width="33%">Birth State:<%=birthState%></td><td width="33%" align="right">Nationality:<%=natnlty%></td>
</tr>
<tr width="100%">
<td width="33%">Death Indicator:<%=deathIndicate%></td><td align="center" width="33%"></td><td width="33%" align="right"></td>
</tr>
<%} %>
</table>
<input type="hidden" id="wsrespCode" value=<%=responseCode%>>
<input type="hidden" id="wsrespMsg" value=<%=responseMessage%>>
<input type="hidden" id="wsDOB" value=<%=DOB%>>
<input type="hidden" id="wsgivenName" value="<%=givenName%>">
<input type="hidden" id="wslastName" value="<%=lastName%>">
<input type="hidden" id="wssex" value=<%=sex%>>
<input type="hidden" id="wsbirthCity" value="<%=birthCity%>">
<input type="hidden" id="wsbirthState" value="<%=birthState%>">
<input type="hidden" id="wsnatnlty" value=<%=natnlty%>>
<input type="hidden" id="wsrace" value=<%=race%>>
<input type="hidden" id="wsdeathIndicate" value=<%=deathIndicate%>>
<input type="hidden" id="wsbloodType" value=<%=bloodType%>>
<input type="hidden" id="wspersonId" value=<%=personId%>>




</TABLE>
<%
}
break;
case 79:
        gotMethod = true;
        String userID_22id=  request.getParameter("userID84");
        java.lang.String userID_22idTemp  = userID_22id;
        String password_23id=  request.getParameter("password86");
        java.lang.String password_23idTemp  = password_23id;
        String HCFacilityList_24id=  request.getParameter("HCFacilityList88");
        java.lang.String HCFacilityList_24idTemp  = HCFacilityList_24id;
        String MRNList_25id=  request.getParameter("MRNList90");
        java.lang.String MRNList_25idTemp  = MRNList_25id;
        String howmanyEncounters_26id=  request.getParameter("howmanyEncounters92");
        int howmanyEncounters_26idTemp  = Integer.parseInt(howmanyEncounters_26id);
        com.velos.DHTS.GetPatientInfoExResponseGetPatientInfoExResult getPatientInfoEx79mtemp = samplePatientDemographicsSoapProxyid.getPatientInfoEx(userID_22idTemp,password_23idTemp,HCFacilityList_24idTemp,MRNList_25idTemp,howmanyEncounters_26idTemp);
if(getPatientInfoEx79mtemp == null){
%>
<%=getPatientInfoEx79mtemp %>
<%
}else{
%>
<TABLE>
<TR>
<TD COLSPAN="3" ALIGN="LEFT">returnp:</TD>
<TR>
<TD WIDTH="5%"></TD>
<TD COLSPAN="2" ALIGN="LEFT">_any:</TD>
<TD>
<%
if(getPatientInfoEx79mtemp != null){
org.apache.axis.message.MessageElement[] type_any82 = getPatientInfoEx79mtemp.get_any();
        java.util.List list_any82= java.util.Arrays.asList(type_any82);
        String temp_any82 = list_any82.toString();
        %>
        <%=temp_any82%>
        <%
}%>
</TD>
</TABLE>
<%
}
break;
}
} catch (Exception e) {
	exceptionMsg=e.getMessage();
	e.printStackTrace();
%>
<b>
Exception: <%=e%>
</b>
<%
return;
}
if(!gotMethod){
%>
result: N/A
<%
}
%>
</BODY>


</HTML>