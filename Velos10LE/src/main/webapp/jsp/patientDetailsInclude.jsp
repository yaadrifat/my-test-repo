<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC" %>
<%@ page language = "java" import = "com.velos.remoteservice.demographics.*;"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
HttpSession tSession = request.getSession(true);
if (!sessionmaint.isValidSession(tSession))
	return;

String defUserGroup = (tSession.getAttribute("defUserGroup")).toString();
String mode = (String) request.getParameter("mode");

//look for a the remote service. If a remote service exists, it's
//return values will override the values of this page. If it does not
//exist, we will use default behavior.
IDemographicsService demographicsService = DemographicsServiceFactory.getService();

boolean isRemoteDemoInstalled = (demographicsService != null) ? true : false;
%>
<script>
var patientDetailsFunctions = {
	entryMode: '<%=mode%>',
	defUserGroup: <%=defUserGroup%>,
	formObj: {},

	f_SSN_NA: {},
	startDate1: {},
	fAddTxt: {},
	openCountryLookup: {},
	openSpecialityWindow: {},
	removeSpeciality: {},
	openEthnicityWindow: {},
	openRaceWindow: {},
	openwinpatdetails: {},
	openPatFacility: {},
	displayFldValMsg: {},
	fetchData: {},
	
	validate: {}
};
</script>

<script type="text/javascript">
jQuery(document).ready( function() {
	patientDetailsFunctions.formObj = document.patientDemogForm;
	<%if (isRemoteDemoInstalled){%>
	patientDetailsFunctions.fetchData();
	<%}%>
});
</script>

<script LANGUAGE="javascript">
var dd1, delay;

patientDetailsFunctions.f_SSN_NA = function(val){
	var formobj = patientDetailsFunctions.formObj;
	formobj.patssn.value ="";
	 if (val=='1'){
		formobj.patssn.disabled=true;
	 }else{
		formobj.patssn.disabled=false;
	}
};

patientDetailsFunctions.startDate1 = function(delay1) {
  var adate, date, amonth;
  delay = delay1;
  adate = new Date();
  date = adate.getDate();
  amonth = adate.getMonth()+1;
  if (amonth == 1) date += " January";
  else if (amonth == 2) date += " February";
  else if (amonth == 3) date += " March";
  else if (amonth == 4) date += " April";
  else if (amonth == 5) date += " May";
  else if (amonth == 6) date += " June";
  else if (amonth == 7) date += " July";
  else if (amonth == 8) date += " August";
  else if (amonth == 9) date += " September";
  else if (amonth == 10) date += " October";
  else if (amonth == 11) date += " November";
  else if (amonth == 12) date += " December";
  date += " " + adate.getFullYear();
  document.atime21.date.value = date;
  dd1 = setTimeout("startDate1(delay)",delay1);
};

patientDetailsFunctions.fAddTxt = function(){
	var formobj = patientDetailsFunctions.formObj;
	formobj.action="patientdetails.jsp" ;
	formobj.submit();
};

patientDetailsFunctions.openCountryLookup = function(formobj) {
	formobj.target="Lookup";
	formobj.method="post";
	formobj.action="multilookup.jsp?viewNameKeyword=codeLstData&form="+formobj.name+"&seperator=;"+
                  "&dfilter=countryCode&keyword=patcountry|CODEDESC&maxselect=1";

	formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');

	if (formWin && !formWin.closed) formWin.focus();
	formobj.submit();
	formobj.target="_self";
	formobj.action="#";
	void(0);
};

patientDetailsFunctions.openSpecialityWindow = function() {
	var formobj = patientDetailsFunctions.formObj;
	specialityIds = formobj.selSpecialityIds.value;	windowName=window.open("multiselectcodepopup.jsp?selectedIds="+specialityIds+"&openerForm="+formobj.name+"&hidEleName=selSpecialityIds&eleName=txtSpeciality&codeType=prim_sp&ptitle=<%=LC.L_Speciality%>&heading=<%=LC.L_Mng_Pats%>","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=350,height=500");
	/*specialityIds = formobj.selSpecialityIds.value;	windowName=window.open("multiselectcodepopup.jsp?selectedIds="+specialityIds+"&openerForm=patient&hidEleName=selSpecialityIds&eleName=txtSpeciality&codeType=prim_sp&ptitle=Speciality&heading=Manage Patients","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=350,height=300");*****/
	windowName.focus();
};

patientDetailsFunctions.removeSpeciality = function(){
	var formobj = patientDetailsFunctions.formObj;
	formobj.txtSpeciality.value="";
	formobj.selSpecialityIds.value="";
};

patientDetailsFunctions.openEthnicityWindow = function() {
	var formobj = patientDetailsFunctions.formObj;
	addEthnicityIds = formobj.selAddEthnicityIds.value;	windowName=window.open("multiselectcodepopup.jsp?selectedIds="+addEthnicityIds+"&openerForm="+formobj.name+"&hidEleName=selAddEthnicityIds&eleName=txtAddEthnicity&codeType=ethnicity&ptitle=<%=LC.L_Ethnicity%>&heading=<%=LC.L_Mng_Pats%>","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=500");
	/*addEthnicityIds = formobj.selAddEthnicityIds.value;	windowName=window.open("multiselectcodepopup.jsp?selectedIds="+addEthnicityIds+"&openerForm=patient&hidEleName=selAddEthnicityIds&eleName=txtAddEthnicity&codeType=ethnicity&ptitle=Ethnicity&heading=Manage Patients","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=300");*****/
	windowName.focus();
};

patientDetailsFunctions.openRaceWindow = function() {
	var formobj = patientDetailsFunctions.formObj;
	addRaceIds = formobj.selAddRaceIds.value;	windowName=window.open("multiselectcodepopup.jsp?selectedIds="+addRaceIds+"&openerForm="+formobj.name+"&hidEleName=selAddRaceIds&eleName=txtAddRace&codeType=race&ptitle=<%=LC.L_Race%>&heading=<%=LC.L_Mng_Pats%>","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=500");
	/*addRaceIds = formobj.selAddRaceIds.value;	windowName=window.open("multiselectcodepopup.jsp?selectedIds="+addRaceIds+"&openerForm=patient&hidEleName=selAddRaceIds&eleName=txtAddRace&codeType=race&ptitle=Race&heading=Manage Patients","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=300");*****/
	windowName.focus();
};

patientDetailsFunctions.openwinpatdetails = function(perid,pageRights){
	windowname=window.open("morePerDetails.jsp?perId="+perid,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=500,height=350 top=120,left=200 0, ");
	windowname.focus();
};

//JM: 12Dec2006
//patientDetailsFunctions.openPatFacility=  function(patId,patFacilityPK){
patientDetailsFunctions.openPatFacility = function(patId,patFacilityPK,facilityCount,viewCompRight ){
		var mode;
		if (patFacilityPK == 0)
		{
			mode='N'
		}
		else
		{
			mode='M'
		}

		var viewCRight ;
		var rtStr;

		viewCRight = parseInt(viewCompRight);

		if (viewCRight <=0)
		{
			rtStr = "nx";
		}
		else
		{

			rtStr = "dx";
		}



    windowName=window.open("patientFacility.jsp?patientId=" + patId+ "&mode="+mode + "&facilityCount=" + facilityCount+ "&patFacilityPK="+patFacilityPK+"&c="+rtStr,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=800,height=380 top=120,left=200 0, ");
	windowName.focus();
};

patientDetailsFunctions.validate = function(){
	var formobj = patientDetailsFunctions.formObj;
	 //if(!(validate_col('Pat ID',formobj.patID))) return false --KM
	 if (document.getElementById('mandpatid')) {
	     if (!(validate_col('Pat ID',formobj.patID))) return false;
	 }

	 if (document.getElementById('pgcustompatid')) {
	     if (!(validate_col('Pat ID',formobj.patID))) return false;
	 }

	//Virendra: Fixed Bug No. 4729, Added '\' validation for PatientID, Firstname, Lastname
	 patIDval = formobj.patID.value;
	 patfnameval = formobj.patfname.value;
	 patlnameval = formobj.patlname.value;

 	 if(patIDval.indexOf("%")!=-1 || patIDval.indexOf("\\")!=-1  )
	  {
 		var paramArray = ["%,\\"];
		alert(getLocalizedMessageString("M_SplCharNotAllowed", paramArray));/*alert("Special Character(%,\\) not allowed for this Field");*****/
		  formobj.patID.focus();
		  return false;
	  }
 	//Modified to not allowed the special character "," in "Patient ID"
	 if(patIDval.indexOf(",") != -1 ){
		var paramArray = [","];
		alert(getLocalizedMessageString("M_SplCharNotAllowed", paramArray));/*alert("Special Character(,) not allowed for this Field");*****/
		formobj.patID.focus();
		return false;
	 }
 	 if( patfnameval.indexOf("\\")!=-1  )
	  {
 		var paramArray = ["\\"];
		alert(getLocalizedMessageString("M_SplCharNotAllowed", paramArray));/*alert("Special Character(\\) not allowed for this Field");*****/
		  formobj.patfname.focus();
		  return false;
	  }
 	if( patlnameval.indexOf("\\")!=-1  )
	  {
 		var paramArray = ["\\"];
		alert(getLocalizedMessageString("M_SplCharNotAllowed", paramArray));/*alert("Special Character(\\) not allowed for this Field");*****/
		  formobj.patlname.focus();
		  return false;
	  }


	 if (document.getElementById('mandsstate')) {
	   if (!(validate_col('pat status',formobj.patstatus))) return false
	 }
	//FIX #6190 --START
	 if (document.getElementById('phDetailI').value=="true") {
	 
		 if (document.getElementById('mandpatdob')) {
		     if (!(validate_col('Date of Birth',formobj.patdob))) return false
		 }

		if (!(validate_date(formobj.patdob))) return false;

		 if (document.getElementById('pgcustompatsstate')) {
		     if (!(validate_col('pat status',formobj.patstatus))) return false
		 }

		 if (document.getElementById('pgcustompatdod')) {
		     if (!(validate_col('Date of Death',formobj.patdeathdate))) return false
		 }
		 // Added by JM on 040805
		 if (!(validate_date(formobj.patdeathdate))) return false;

		//JM: #4025, #4026
		todate = formatDate(new Date(),calDateFormat);

		if (formobj.patdob.value != null && formobj.patdob.value !=''){

			if (CompareDates(formobj.patdob.value,todate,'>')){
			  	alert ("<%=MC.M_BirthDtNotGrt_Today%>");/*alert ("Date of Birth should not be greater than Today's Date");*****/
				formobj.patdob.focus();
				return false;
			}
		 }
	  /*
		 if (formobj.patregdate.value != null && formobj.patregdate.value != '' )
		 {
				 if (CompareDates(formobj.patdob.value,formobj.patregdate.value,'>'))
			 {
			  	alert ("Registration Date should not be less than Patient's Date of Birth");
				return false;
			 }
		 }
		// Added by Gopu on 11th April for Validation for Registration Date should not be less than Patient's Death Date
		 if (formobj.patregdate.value != null && formobj.patregdate.value != '' ){
				 if (CompareDates(formobj.patregdate.value,todate,'>')){
			  	alert ("Registration Date should not be greater than the Today's Date");
				return false;
			 }
		 }*/
		// Added by Gopu on 08th April 2005 Validation for Death Date should not allow future date
		if (formobj.patdeathdate.value != null && formobj.patdeathdate.value !=''){
				if (CompareDates(formobj.patdeathdate.value,todate,'>')){
				  	alert ("<%=MC.M_DthDtNotGtr_TodayDt%>");/*alert ("Death Date should not be greater than Today's Date");*****/
					formobj.patdeathdate.focus();
					return false;
				}
				if (formobj.patdob.value != null && formobj.patdob.value !=''){
					if (CompareDates(formobj.patdob.value,formobj.patdeathdate.value,'>')){
						alert ("<%=MC.M_DthDtNotLess_PatBirthDt%>");/*alert ("Death Date should not be less than <%=LC.Pat_Patient%>'s Birth Date");*****/
						formobj.patdeathdate.focus();
						return false;
					}
				}
			}
		//check for death stat
				    
		patselstat = formobj.patstatus.value;
		deadstat =   formobj.deadStatPk.value;
		deathdate = formobj.patdeathdate.value;
		if(deadstat == patselstat){
			if (deathdate == null || deathdate == '' )
			{
				 alert("<%=MC.M_PlsEtr_PatDod%>");/*alert("Please enter <%=LC.Pat_Patient%>'s Date of Death");*****/
				 return false;
			}
		}
		if (deathdate != null && deathdate != '' ){
		 	if ( deadstat != patselstat)
		 	{
		 		alert("<%=MC.M_YouEtrPat_DodPlsChg%>");/*alert("You have entered <%=LC.Pat_Patient%>'s Date of Death. Please change the <%=LC.Pat_Patient%>'s Status to 'Dead'");*****/
		 		return false;
		 	}
		}
		// Added by Gopu on 08th April 2005 Validation for Death Date should not allow future date
		if (deathdate != null && deathdate !=''){
			if (CompareDates(deathdate,todate,'>')){
			  	alert ("<%=MC.M_DthDtNotGtr_TodayDt%>");/*alert ("Death Date should not be greater than Today's Date");*****/
				formobj.patdeathdate.focus();
				return false;
			}
		 }

		// Modified by Gopu on 8th March 2005 for fixed the bug no. 2036 make the "First Name" field in Patient Demographic as non-Mandatory
		 if (document.getElementById('pgcustompatfname')) {
		     if (!(validate_col('<%=LC.L_First_Name_Patient%>',formobj.patfname))) return false
		 }

		 if (document.getElementById('pgcustompatmidname')) {
		   if (!(validate_col('<%=LC.L_Middle_Name%>',formobj.patmname))) return false
		 }

		 if (document.getElementById('pgcustompatlname')) {
		   if (!(validate_col('<%=LC.L_Last_Name_Patient%>',formobj.patlname))) return false
		 }

		//if (!(validate_date(formobj.patdob))) return false
		 if (document.getElementById('pgcustompatdob')) {
		     if (!(validate_col('Date of Birth',formobj.patdob))) return false
		 }

		 if (document.getElementById('pgcustompatssn')) {
		   if (!(validate_col('ssn',formobj.patssn))) return false
		 }

		 if (document.getElementById('pgcustompatemail')) {
		   if (!(validate_col('email',formobj.patemail))) return false
		 }

		//JM: 10Sep2007: added for the issue #3080
		if (!isEmpty(formobj.patemail.value)) // added by sonia : the empty check
		 {
			if(formobj.patemail.value.search("@") == -1) {
					alert("<%=MC.M_EtrValid_EmailAddr%>");/*alert("Please enter a valid email address.");*****/
					formobj.patemail.focus()
					return false;
			}
		}

		 if (document.getElementById('pgcustompataddress1')) {
		   if (!(validate_col('Address1',formobj.patadd1))) return false
		 }

		 if (document.getElementById('pgcustompataddress2')) {
		   if (!(validate_col('Address2',formobj.patadd2))) return false
		 }

		 if (document.getElementById('pgcustompatcity')) {
		   if (!(validate_col('City',formobj.patcity))) return false
		 }

		 if (document.getElementById('pgcustompatstate')) {
		   if (!(validate_col('State',formobj.patstate))) return false
		 }

		 if (document.getElementById('pgcustompatcounty')) {
		   if (!(validate_col('County',formobj.patcounty))) return false
		 }

		 if (document.getElementById('pgcustompatzip')) {
		   if (!(validate_col('Zip',formobj.patzip))) return false
		 }

		 if (document.getElementById('pgcustompatcountry')) {
		   if (!(validate_col('Country',formobj.patcountry))) return false
		 }

		 if (document.getElementById('pgcustompathphone')) {
		   if (!(validate_col('HPhone',formobj.pathphone))) return false
		 }

		 if (document.getElementById('pgcustompatwphone')) {
		   if (!(validate_col('WPhone',formobj.patbphone))) return false
		 }
	 }
	 //FIX #6190 --END

	 patselstat = formobj.patstatus.value;
	 deadstat =   formobj.deadStatPk.value;
	 deathdate = formobj.patdeathdate.value;
	 if(formobj.dthCause.value != null && formobj.dthCause.value != ''){
		  if ( deadstat != patselstat)
		 	{
		 		alert("<%=MC.M_EtrPatDthCause_StatDead%>");/*alert("You have entered <%=LC.Pat_Patient%>'s Cause of Death. Please change the <%=LC.Pat_Patient%>'s Status to 'Dead'");*****/
		 		return false;
		 	}
	 }
		
	 if (document.getElementById('pgcustompatcod')) {
	     if (!(validate_col('Cause of Death',formobj.dthCause))) return false;
	 }

	 if (document.getElementById('pgcustompatspeccause')) {
	     if (!(validate_col('Specify Cause',formobj.othDthCause))) return false;
	 }

	 //gender mandatory/non-mand implementation
	 if (document.getElementById('pgcustompatgender')) {
	     if (!(validate_col('patgender',formobj.patgender))) return false;
	 }

	 if (document.getElementById('pgcustompatmarital')) {
	   if (!(validate_col('Marital Status',formobj.patmarital))) return false;
	 }

	 if (document.getElementById('pgcustompatblood')) {
	   if (!(validate_col('Blood Group',formobj.patblood))) return false;
	 }

	 //ethnicity mandatory/non-mand implementation

	 if (document.getElementById('pgcustompatethnicity')) {
	     if (!(validate_col('patethnicity',formobj.patethnicity))) return false;
	 }

	 //race mandatory/non-mand implementation
	 if (document.getElementById('pgcustompatrace')) {
	     if (!(validate_col('patrace',formobj.patrace))) return false;
	 }

	 if (document.getElementById('pgcustompatadditional1')) {
	   if (!(validate_col('<%=LC.L_Addl_Ethnicities%>',formobj.txtAddEthnicity))) return false;
	 }

	 if (document.getElementById('pgcustompatadditional2')) {
	   if (!(validate_col('<%=LC.L_Addl_Races%>',formobj.txtAddRace))) return false;
	 }

	 if (document.getElementById('pgcustompatemp')) {
	   if (!(validate_col('Employment',formobj.patemp))) return false;
	 }

	 if (document.getElementById('pgcustompatedu')) {
	   if (!(validate_col('Education',formobj.patedu))) return false
	 }

	 if (document.getElementById('pgcustompatnotes')) {
	   if (!(validate_col('Notes',formobj.patnotes))) return false;
	 }

     //if (!(validate_col('organization',formobj.patorganization))) return false
     //	 if (!(validate_col('pat status',formobj.patstatus))) return false


	 if (!(validate_col('Time Zone',formobj.timeZone))) return false;
	// if (!(validate_date(formobj.patregdate))) return false
	// Added by Gopu on 07th April 2005 DOB Validation DOB should not be greater than Today's Date
   // modified by GOpu for fixing the bug no. 2117
	// da= new Date();
	// var str1,str2;
	//str1=parseInt(da.getMonth());
	// str2=str1+1;
	//todate = str2+'-'+da.getDate()+'-'+da.getFullYear();

	var mVal = document.getElementById("mode").value;
 	if (mVal){
 	 	var fdaVal = document.getElementById("fdaStudy").value;
		if (fdaVal == "1" && mVal == "M"){
			if (  !(validate_col('<%=LC.L_ReasonForChangeFDA%>',formobj.remarks))) return false;
		}
 	}

	if (formobj.userLogName){
		if (  !(validate_col('<%=LC.L_UserName%>',formobj.userLogName))) return false;
	}	
	if (!(validate_col('e-Signature',formobj.eSign))) return false;
	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
   }
	
	return true;
};

patientDetailsFunctions.displayFldValMsg = function(name,minLen,msgTarget,passMsg,failMsg,addFldStr,msgMode) {
	var eleSpan = document.getElementById("showfldobMessage");

	eleSpan.getElementsByTagName('td')[0].innerHTML="";
	ajaxvalidate(name,minLen,msgTarget,passMsg,failMsg,addFldStr,msgMode);
    var valMsgEle= document.getElementById(msgTarget);
    eleSpan.style.display="none";
    var inHTML = (valMsgEle) ? ($j.trim((valMsgEle.innerHTML))) : "";

    if(inHTML.length>0){
    	eleSpan.style.display="block";
    }
    eleSpan.style.paddingTop="0px";
    eleSpan.style.paddingBottom="0px";
};

patientDetailsFunctions.openwin1 = function() {
    windowName = window.open("usersearchdetails.jsp?fname=&lname=&from=patient","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
	windowName.focus();
};

patientDetailsFunctions.fetchData = function(){
	toggleNotification('on',"");
	var patientid = $('patid').value;
	$j.ajax({
		type: "POST",
		url: "/velos/jsp/wsresultDemo.jsp",
		data: { patId: patientid, resulttype: "javascript", mode: "id" }	
	})
	.done(function() {
		$("wsDIV").style.visibility="hidden";
		$("wsDIV").innerHTML=transport.responseText;
		document.patient.patfname.value = $("wsPatFName").value;
		document.patient.patmname.value = $("wsPatMName").value;
		document.patient.patlname.value = $("wsPatLName").value;
		document.patient.patdob.value = $("wsPatDOB").value;
		document.patient.patmarital.value = $("wsPatMaritalStatus").value;  //name?
		document.patient.patethnicity.value = $("wsPatEthnicity").value; //name
		document.patient.patrace.value = $("wsPatRace").value;
		//$("patstatus").value = $("wsPatSurvivalStatus").value;
		document.patient.patblood.value = $("wsPatBloodGroup").value; //name
		document.patient.patssn.value = $("wsPatSSN").value; //name
		document.patient.patemail.value = $("wsPatEmail").value;
		document.patient.patadd1.value = $("wsPatAddress1").value; //name
		document.patient.patadd2.value = $("wsPatAddress2").value; //name
		document.patient.patcity.value = $("wsPatCity").value; //name
		document.patient.patcounty.value = $("wsPatCounty").value; //name
		document.patient.patstate.value = $("wsPatState").value; //name
		document.patient.patzip.value = $("wsPatZip").value;//name
		document.patient.patcountry.value = $("wsPatCountry").value; //name
		document.patient.pathphone.value = $("wsPatHomePhones").value; //name
		document.patient.patbphone.value = $("wsPatWorkPhones").value; //name
		//document.patient.patfname.value = $("wsPatPrimaryFacility").value;
		document.patient.patgender.value = $("wsPatSex").value;
	})
	.always(function() {
		toggleNotification('off',"");
	});
};
</script>