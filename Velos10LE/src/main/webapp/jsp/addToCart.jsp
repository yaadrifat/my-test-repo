<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.velos.eres.service.util.StringUtil"%><html>
<jsp:useBean id="sessionmaint" scope="session"
	class="com.velos.eres.service.util.SessionMaint" />
<%@page
	import="com.velos.eres.web.specimen.*,java.util.List,java.util.ArrayList,java.util.Arrays,com.velos.eres.service.util.LC"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Addto_Cart%><%--Add to Cart*****--%></title>
</head>
<body>

<%
	String theParamData;
	int columns;
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession)) {
		PreparationCartDto bPreparationCartDto = (PreparationCartDto) tSession
				.getAttribute("aPreparationCartDto");

		PreparationCartDto aPreparationCartDto = new PreparationCartDto(), finalPreparationCartDto = null;

		theParamData = request.getParameter("dataParam");
		columns = Integer.parseInt(request.getParameter("columns"));

		ArrayList patientIdList = new ArrayList();
		ArrayList studyNumberList = new ArrayList();
		ArrayList calendarList = new ArrayList();
		ArrayList visitList = new ArrayList();
		ArrayList dateList = new ArrayList();
		ArrayList eventList = new ArrayList();
		ArrayList kitList = new ArrayList();
		ArrayList pkStorageList = new ArrayList();
		ArrayList fkSchEventList = new ArrayList();
		ArrayList fkVisitList = new ArrayList();
		ArrayList fkStudyList = new ArrayList();
		ArrayList fkPerList = new ArrayList();
		ArrayList fkSiteList = new ArrayList();

		String aRowArray[] = StringUtil.strSplit(theParamData,
				"[ROWSEP]");
		String aColumnArray[][] = new String[aRowArray.length][columns];
		for (int i = 0; i < aRowArray.length; i++) {
			aColumnArray[i] = StringUtil.strSplit(aRowArray[i],
					"[COLSEP]");
		}
		for (int i = 0; i < aRowArray.length; i++) {
			for (int j = 0; j < aColumnArray[i].length; j++) {
				switch (j) {
				case 0:
					patientIdList.add(aColumnArray[i][j]);
					break;
				case 1:
					studyNumberList.add(aColumnArray[i][j]);
					break;
				case 2:
					calendarList.add(aColumnArray[i][j]);
					break;
				case 3:
					visitList.add(aColumnArray[i][j]);
					break;
				case 4:
					dateList.add(aColumnArray[i][j]);
					break;
				case 5:
					eventList.add(aColumnArray[i][j]);
					break;
				case 6:
					kitList.add(aColumnArray[i][j]);
					break;
				case 7:
					pkStorageList.add(aColumnArray[i][j]);
					break;
				case 8:
					fkSchEventList.add(aColumnArray[i][j]);
					break;
				case 9:
					fkVisitList.add(aColumnArray[i][j]);
					break;
				case 10:
					fkStudyList.add(aColumnArray[i][j]);
					break;
				case 11:
					fkPerList.add(aColumnArray[i][j]);
					break;
				case 12:
					fkSiteList.add(aColumnArray[i][j]);
					break;
				}
			}
		}
		//setting all lists to dto
		aPreparationCartDto.setPersonPIdList(patientIdList);
		aPreparationCartDto.setStudyNumberList(studyNumberList);
		aPreparationCartDto.setCalendarList(calendarList);
		aPreparationCartDto.setVisitList(visitList);
		aPreparationCartDto.setScheduledDateList(dateList);
		aPreparationCartDto.setEventList(eventList);
		aPreparationCartDto.setStorageKitList(kitList);
		aPreparationCartDto.setPkStorageList(pkStorageList);
		aPreparationCartDto.setFkSchEventList(fkSchEventList);
		aPreparationCartDto.setFkVisitList(fkVisitList);
		aPreparationCartDto.setFkStudyList(fkStudyList);
		aPreparationCartDto.setFkPerList(fkPerList);
		aPreparationCartDto.setFkSiteList(fkSiteList);
		if (bPreparationCartDto != null) {
			PreparationCartDto tempDto = ManageCart.checkDuplicate(
					aPreparationCartDto, bPreparationCartDto);
			if (tempDto != null) {
				finalPreparationCartDto = ManageCart.addDtoToCart(
						bPreparationCartDto, tempDto);
			} else {
				finalPreparationCartDto = bPreparationCartDto;
			}
		} else {
			finalPreparationCartDto = aPreparationCartDto;
		}
		tSession.setAttribute("aPreparationCartDto",
				finalPreparationCartDto);

	}
%>
</body>
</html>