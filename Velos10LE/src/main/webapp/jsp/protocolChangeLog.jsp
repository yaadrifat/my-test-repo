<%@page import="org.json.*"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true" />
<html>
<head>
<%
String isNewAmendment="false";
session.setAttribute("isNewAmendment", isNewAmendment);
boolean isIrb = "irb_check_tab".equals(request.getParameter("selectedTab")) ? true : false; 
if (isIrb) {
%>
<title><%=MC.M_ResCompApp_ChkSub%><%--Research Compliance >> New Application >> Check and Submit*****--%></title>
<% } else { %>
<title><%=MC.M_Std_CkhSubmit%><%--<%=LC.Std_Study%> >> Check and Submit*****--%></title>
<% } %>
<%@ page
	import="java.util.*,java.io.*,org.w3c.dom.*,org.json.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*"%>
<%@ page import="com.velos.esch.business.common.SchMsgTxtDao"%>
<%@ page import="com.velos.eres.compliance.web.ComplianceJB"%>
<%@ page
	import="com.velos.eres.widget.business.common.UIFlxPageDao,com.velos.eres.widget.service.util.FlxPageArchive"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ page import="com.velos.eres.compliance.web.ProtocolChangeLog"%>

</head>
<jsp:useBean id="sessionmaint" scope="session"
	class="com.velos.eres.service.util.SessionMaint" />
<jsp:useBean id="studyB" scope="page"
	class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="complianceJB" scope="request"
	class="com.velos.eres.compliance.web.ComplianceJB" />

<% 
String src= request.getParameter("srcmenu");
HttpSession tSession = request.getSession(true); 
String accountId = "";
String studyId = "";
String grpId = "";
String usrId = "";
ArrayList boardNameList = null;
ArrayList boardIdList = null;
ArrayList statusList = null;
ArrayList statusDateList = null;
EIRBDao eIrbDao = new EIRBDao();
int count = 0;


if (sessionmaint.isValidSession(tSession))
{
    accountId = (String) tSession.getValue("accountId");
    studyId = (String) tSession.getValue("studyId");
    grpId = (String) tSession.getValue("defUserGroup");
    usrId = (String) tSession.getValue("userId");
}
ProtocolChangeLog protocolChangeLog = new ProtocolChangeLog();
JSONArray versionDiffList = protocolChangeLog.fetchProtocolVersionDiff(StringUtil.stringToNum(studyId), grpId);


%>
<jsp:include page="include.jsp" flush="true" />
<jsp:include page="ui-include.jsp" flush="true"></jsp:include>
<%if (!"LIND".equals(CFG.EIRB_MODE)){ %>
<jsp:include page="panel.jsp" flush="true">
	<jsp:param name="src" value="<%=src%>" />
</jsp:include>
<%} %>
<link href="styles/studyVersion.css" rel="stylesheet" type="text/css" />
<SCRIPT language="javascript">

function submitForm(formobj) {

	var elements =   <%=versionDiffList%> ;
	var size = <%=versionDiffList.length()%> ;

	for (var iX=0; iX < size; iX++) {
		
		var str = elements[iX].fieldLabel;
			
		var notes = document.getElementById(str+"_notes");
		if(notes.value == "")
			{
				alert("<%=LC.Reason_For_Change_Mandatory%>"); 
				return false;
			}
		
		var reviewBoard = document.getElementById(str+"_revBoard");
		if(reviewBoard.value == "")
			{
				alert("<%=LC.Review_Board_Mandatory%>");
				return false;
			}
		elements[iX].notes = notes.value;
		elements[iX].revBoard = reviewBoard.value;
	}   
	
	if (!(validate_col('e-Signature',mainForm.eSign))) return false;
	document.getElementById("fieldDetails").value = JSON.stringify(elements);
	mainForm.action="updateNewSubmission.jsp?&srcmenu=<%=request.getParameter("srcmenu")%>&selectedTab=irb_check_tab"; 
	return true;
	
}


</SCRIPT>

<body>

	<div id="overDiv"
		style="position: absolute; visibility: hidden; z-index: 1000;"></div>
	<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
	<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
	<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

	<jsp:useBean id="studyVerB" scope="request"
		class="com.velos.eres.web.studyVer.StudyVerJB" />
	<jsp:useBean id="appendixB" scope="request"
		class="com.velos.eres.web.appendix.AppendixJB" />
	<%@ page language="java"
		import="com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil"%>
	<%
	
	if (sessionmaint.isValidSession(tSession))
	{
		int pageRight = 0;
        if(studyId == "" || studyId == null || "0".equals(studyId)) {
	    %>
	<jsp:include page="studyDoesNotExist.jsp" flush="true" />
	<%
	    } else {
        %>
	<form name="mainForm" id="mainForm" method="post"
		onsubmit="if (submitForm(this) == false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<input type="hidden" id="fieldDetails" name="fieldDetails" Value="">
		<% if (!"LIND".equals(CFG.EIRB_MODE)) {} else {
        	  
        %>
		<table width="90%" cellspacing="4" cellpadding="4" border="0"
			class="midAlign">
			<tr>
				<th><%=LC.Change_Version_Page_Header%></th>
			</tr>
		</table>
		<%
		int noOfRows = versionDiffList.length();
		
		if(noOfRows == 0) 
        {
        	%>
		<table width="90%" cellspacing="4" cellpadding="4" border="0"
			class="midAlign">
			<tr>
				<td align="center"><%=LC.Change_Version_No_Diff_Message%></td>
			</tr>
		</table>
		<%} else 
        	{%>

		<table class="TFtable" width="90%" cellspacing="4" cellpadding="4"
			border="1">
			<tr>
				<th><%=LC.Change_Version_Title%></th>
				<th><%=LC.Change_Version_Field_Name%></th>
				<th><%=LC.Change_Version_Old_Value%></th>
				<th><%=LC.Change_Version_New_Value%></th>
				<th><%=LC.Change_Version_Reason_for_Change%><FONT class="Mandatory" id="pgcustompatid">*  </FONT>  </th>
				<th><%=LC.Change_Version_Review_Board%><FONT class="Mandatory" id="pgcustompatid">*  </FONT>  </th>
			</tr>
			<%for(int i= 0; i < noOfRows; i++)
			{
				JSONObject fieldDetails = versionDiffList.getJSONObject(i);
				
				%>
			<%=fieldDetails.get("fieldRow")%>
			<%
			} %>
		</table>
		<%} %>

		<jsp:include page="submitBar.jsp" flush="true">
			<jsp:param name="displayESign" value="Y" />
			<jsp:param name="formID" value="irbcheckid" />
			<jsp:param name="showDiscard" value="N" />
		</jsp:include>
		<%
               } 
        %>
	</form>

	<%
		}// end of else of study null check
	}//end of if body for session
	else
	{
	%>
	<jsp:include page="timeout.html" flush="true" />
	<%
	}
	%>
	<div>
		<jsp:include page="bottompanel.jsp" flush="true" />
	</div>
	<%if (!"LIND".equals(CFG.EIRB_MODE)){ %>
	</div>
	<%} %>
	<div class="mainMenu" id="emenu">
		<jsp:include page="getmenu.jsp" flush="true" />
	</div>
</body>

</html>
