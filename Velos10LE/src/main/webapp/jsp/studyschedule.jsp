<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Std_Sch%><%--<%=LC.Std_Study%> Schedule*****--%></title>
<meta http-equiv=expires content="text/html; charset=UTF-8">
<SCRIPT language=javascript>

function RefreshPage(formobj)
{
formobj.schdate.value="";
formobj.submit();
}

function setFlag(formobj)
{
	formobj.buttonFlag.value ="S";
}

function openWinPage( selectedTab, src , cntr , orderBy , orderType ,   page1 ,  study , formobj)
{
		formobj.buttonFlag.value ="S";
		setFilter(formobj, "F" ) ;

		visitext  =formobj.visitext.value;
		ststxt  =  formobj.statustext.value ;
		durtxt =  formobj.monthyrtext.value ;
		evttxt   =  formobj.eventext.value ;


		filvisit = formobj.filVisit.value;
		event1 = formobj.filEvent.value;
		filmonth = formobj.filMonth.value;
		filyear = formobj.filYear.value;
		stat = formobj.filStatus.value;
		protocolId=formobj.calId.value;
		schdate=formobj.schdate.value;

		generate = formobj.generate.value ;

		param = "studyadmincal.jsp?selectedTab="+selectedTab+"&mode=M&dstatus="+stat+"&event="+event1+"&visit="+filvisit+"&month="+filmonth+"&year="+filyear+"&srcmenu="+src+"&nextpage="+cntr+"&orderBy="+orderBy+"&orderType="+orderType+"&page1="+page1+"&studyId="+study+"&generate="+generate+"&visitext="+visitext+"&eventext="+evttxt+"&statustext="+ststxt+"&monthyrtext="+durtxt+"&calId="+protocolId+"&schdate="+schdate ;
		windowName= window.open(param,"_self","toolbar=no,scrollbars=yes,resizable=yes,menubar=yes,status=yes,dependant=false,width=600,height=300");
		windowName.focus();
}
	function setFilter(formobj,forFiltersFlag)
	{

			if(formobj.buttonFlag.value != 'S') {
				var oldDate = formobj.oldSchDate.value;
				if (validateGenerate(formobj,oldDate) == false)
					return false;
			}
	 /*
		 In this jsp there is submit on 2 events
		 1. on "GO" for initiating the filter search
		 2. on "GENERATE" when a new patient has been added or a new calendar is allotted

		 On pressing "GO"  generateGoFlag gets the value of "P"
		 On pressing "GENERATE" generateGoFlag gets the value of "F"
		 Note that both the event can never happen simultaneously . So depending on the value of
		 generateGoFlag we set the value of "generate" variable and depending on that the new schedule is generated
		 or the filter criteria is passed on etc.

	*/ 					var submitFlag;
						if (typeof(formobj.generateGoFlag)!="undefined")
						submitFlag = formobj.generateGoFlag.value ;


							if ((typeof(formobj.visit)!="undefined")) {
							visitcnt = formobj.visit.options.selectedIndex;
							formobj.visitext.value = formobj.visit.options[visitcnt].text;
							}
							if ((typeof(formobj.event)!="undefined")) {
							eventscnt =formobj.event.options.selectedIndex;
							formobj.eventext.value = formobj.event.options[eventscnt].text;
							}
							if ((typeof(formobj.dstatus)!="undefined")) {
							statusscnt = formobj.dstatus.options.selectedIndex;
							formobj.statustext.value = formobj.dstatus.options[statusscnt].text;
							}
							if ((typeof(formobj.month)!="undefined")) {
							mcnt = formobj.month.options.selectedIndex;
							}
							if ((typeof(formobj.year)!="undefined")) {
							ycnt = formobj.year.options.selectedIndex;
							}
							if ((typeof(formobj.year)!="undefined") && (typeof(formobj.month)!="undefined")) {
							formobj.monthyrtext.value = formobj.month.options[mcnt].text + "/" + formobj.year.options[ycnt].text ;
							}


			//formobj.submit();
			if (typeof(formobj.generate)!="undefined")
				formobj.generate.value = "Y";


					return true;





	}

	function fopen(link,pageRight,orgRight)
	{
		if (f_check_perm(pageRight,'E'))
		{
			window.open(link,'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=640,height=500')
			return true;
		}
		else
		{
		return false;
		}
	}
	function viewevent(link)
	{
		window.open(link,'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=600,height=400')
		return true;
	}
	function editMultipleEvents(link,pageRight,orgRight)
	{
	if (f_check_perm(pageRight,'E'))
		{	window.open(link,'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=850,height=500,left=150')
		return true;
		}
		else
		{
		return false;
		}
	}

	function viewschedule(link)
	{
		window.open(link,'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=600,height=400')
		return true;
	}

	function changedate(link,pageRight,orgRight)
	{
		if (f_check_perm(pageRight,'E'))
		{
			window.open(link,'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=550,height=350')
			return true;
		} else
		{
			return false;
		}
	}

 function openWinStatus(patId,patStatPk,pageRight,study,orgRight)
{
	//windowName= window.open("patstudystatus.jsp?changeStatusMode="+changeStatusMode+"&statid=" + patStatPk + "&pkey=" + patId,"patstatusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=500,height=500");
	//windowName.focus();

	changeStatusMode = "yes";

	if (f_check_perm(pageRight,'N'))
	{
		windowName= window.open("patstudystatus.jsp?studyId="+study+"&changeStatusMode=yes&statid=" + patStatPk + "&pkey=" + patId,"patstatusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=550,height=560");
		windowName.focus();
	}
}





 function openWinProtocol(pageRight,orgRight, study,patient)
{
	if (f_check_perm(pageRight,'E'))
	{
		windowName= window.open("patientprotocol.jsp?studyId="+study+"&pkey=" + patient,"patprotocol","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=750,height=500");
		windowName.focus();
		return true;
	}
	else
		{
			return false;
		}

}

function validateGenerate(formobj,oldDate)
{

    newDate=formobj.schdate.value;

    if (newDate.length==0)
    {
    alert("<%=MC.M_EtrStartDt_ToGenSch%>");/*alert("Please enter a schedule start date to generate schedule");*****/
    return false;
    }

	if  (newDate==oldDate)
	{
	 alert("<%=MC.M_SchGenr_ForSchDt%>");/*alert("Schedule already generated for this schedule date");*****/
	 return false;
	}
	else
	{
		if(formobj.generateGoFlag){ //*YK 07APR2011 : Bug#5356*/
			formobj.generateGoFlag.value="G";}
	return true;
	}
}
</Script>
</head>


<% String src="";
src= request.getParameter("srcmenu");
String calAssoc=request.getParameter("calassoc");
calAssoc=(calAssoc==null)?"":calAssoc;


%>

<% if (!(calAssoc.equals("S"))) { %>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<%} %>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
<jsp:useBean id="eventAssocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="ctrl1" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="patB" scope="request" class="com.velos.eres.web.patStudyStat.PatStudyStatJB"/>
<jsp:useBean id="bRows" scope="request" class="com.velos.eres.web.browserRows.BrowserRowsJB"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>

<%@ page language = "java" import = "java.net.URLEncoder,com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.common.*"%>

<body >

<br>




  <%

   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))
   {
       String defUserGroup = (String) tSession.getAttribute("defUserGroup");
   		BrowserRows br = new BrowserRows();
		int formLibSeq = 0;
		char formLibAppRight = '0';
	//put the values in session
	String study = request.getParameter("studyId");
	if(study == null || study.equals("null"))
	{
		study = (String) tSession.getValue("studyId");
	}
	else
	{
		tSession.setAttribute("studyId",study);
	}

	String enrollId = "";
	String statid = "";
	//request.getParameter("patProtId");



	String usr = (String) tSession.getValue("userId");
    int pageRight = 0;
 	int orgRight = 0;
 	int patStudyStatpk = 0;

	//******************GET STUDY TEAM RIGHTS and put the same in session, this will be a separate obj as no study is selected //***************************************************************
    TeamDao teamDao = new TeamDao();
    teamDao.getTeamRights(EJBUtil.stringToNum(study),EJBUtil.stringToNum(usr));
    ArrayList tId = teamDao.getTeamIds();
    int patdetright = 0;
    if (tId.size() == 0)
	{
    	pageRight=0 ;
    }
	else
	{


    	stdRights =(StudyRightsJB) tSession.getValue("studyRights");

    	if ((stdRights.getFtrRights().size()) == 0)
		{
    	 	pageRight= 0;
    		patdetright = 0;
    	}
		else
		{
    		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
    		patdetright = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYVPDET"));
    		tSession.setAttribute("studyManagePat",new Integer(pageRight));
    		tSession.setAttribute("studyViewPatDet",new Integer(patdetright));
    	}
    }

	//boolean genSchedule = true;
	SchCodeDao cd = new SchCodeDao();
	String dEvStat = "";
	int stTempPk;

	String evParmStatus = request.getParameter("dstatus");
	int tempStat = EJBUtil.stringToNum(evParmStatus);

	cd.getCodeValues("eventstatus",0);
	cd.setForGroup(defUserGroup);
	cd.getHiddenCodelstDataForUser();


	String hideCode = "";

	//dEvStat	 =  cd.toPullDown("status");
	StringBuffer mainStr = new StringBuffer();
	int cdcounter = 0;
 	mainStr.append("<SELECT NAME='dstatus'><Option value='' Selected>"+LC.L_All/*All*****/+"</option>") ;
	for (cdcounter = 0; cdcounter <= cd.getCDesc().size() -1 ; cdcounter++)
	{
		stTempPk = ((Integer) cd.getCId().get(cdcounter)).intValue() ;
		hideCode = cd.getCodeHide(cdcounter);

		if (tempStat == stTempPk )
		{
		mainStr.append("<OPTION SELECTED value = "+ cd.getCId().get(cdcounter)+">" + cd.getCDesc().get(cdcounter)+ "</OPTION>");
		}
		else
		{
		   if (! StringUtil.isEmpty(hideCode) && (!hideCode.equals("Y")))
		   {
					mainStr.append("<OPTION value = "+ cd.getCId().get(cdcounter)+">" + cd.getCDesc().get(cdcounter)+ "</OPTION>");
			}

		}
	}
	//mainStr.append("<OPTION value='' SELECTED> Select an Option</OPTION>");
	mainStr.append("</SELECT>");
	dEvStat = mainStr.toString();
	String modRight = (String) tSession.getValue("modRight");
	int modlen = modRight.length();

	ctrl.getControlValues("module");
	int ctrlrows = ctrl.getCRows();

	ArrayList feature =  ctrl.getCValue();
	ArrayList ftrDesc = ctrl.getCDesc();
	ArrayList ftrSeq = ctrl.getCSeq();
	ArrayList ftrRight = new ArrayList();
	ArrayList crfNamesArr = new ArrayList();

	String strR;
	ArrayList ctrlValue = new ArrayList();

	for (int counter = 0; counter <= (modlen - 1);counter ++)
	{
		strR = String.valueOf(modRight.charAt(counter));
		ftrRight.add(strR);
	}

	grpRights.setGrSeq(ftrSeq);
        grpRights.setFtrRights(ftrRight);
	grpRights.setGrValue(feature);
	grpRights.setGrDesc(ftrDesc);
	int eptRight = 0;
	eptRight  = Integer.parseInt(grpRights.getFtrRightsByValue("MODEPT"));

	int personPK = 0;
	int siteId = 0;
	String patientId = "";

	personPK = EJBUtil.stringToNum(request.getParameter("pkey"));
	String pkey = request.getParameter("pkey");


    //person.setPersonPKId(personPK);
	//person.getPersonDetails();
	//patientId = person.getPersonPId();

	siteId = EJBUtil.stringToNum(person.getPersonLocation());
	String userIdFromSession = (String) tSession.getValue("userId");


	String statDesc=request.getParameter("statDesc");
	String studyVer=request.getParameter("studyVer");
	if ( studyVer == null ) studyVer = "";


	String studyTitle = "";
	String studyNumber = "";
	studyB.setId(EJBUtil.stringToNum(study));
        studyB.getStudyDetails();
        studyTitle = studyB.getStudyTitle();
	studyNumber = studyB.getStudyNumber();

	//String uId = (String) tSession.getValue("userId");
	//int userId = EJBUtil.stringToNum(uId);

	String uName = (String) tSession.getValue("userName");
	String ipAdd = (String) tSession.getValue("ipAdd");
	int totalDefinedVisits = 0;

	if ((pageRight >= 4))
	{
		String evDesc = "";
		String subType ="";
		String protocolId="";
		String startdt="";
		String status="";//G-Generate Schedule,S-Show Schedule also
		String schDate = "";
		String acDate = "";

		String page1 = request.getParameter("page1");

		String selectedTab =  request.getParameter("selectedTab");

		String generateGoFlag = "P";

		String generate = request.getParameter("generate");


		String newHrf="";
		String editHrf="";

		patEnrollB.findCurrentPatProtDetails(EJBUtil.stringToNum(study),EJBUtil.stringToNum(pkey));

		enrollId = String.valueOf(patEnrollB.getPatProtId());


		if (enrollId.equals("0"))
		{
			enrollId = "";
		}




		//patEnrollB.getPatProtDetails();
		protocolId =request.getParameter("calId");
		if (protocolId == null || "".equals(protocolId)) {
			protocolId= "0";
		}
		String selDay = null;

		//Retrieve the Ative protocol Calendar list

		EventAssocDao eventAssocDao = eventAssocB.getStudyAdminProts(EJBUtil.stringToNum(study),"A");
		ArrayList eventIds = eventAssocDao.getEvent_ids();
		ArrayList chainIds = eventAssocDao.getChain_ids();
		ArrayList names = eventAssocDao.getNames();
		
		
		//JM: 22Feb2011: #5860
		String calStatDesc_A = "";
		
		SchCodeDao scho = new SchCodeDao();
		
		calStatDesc_A = scho.getCodeDescription(scho.getCodeId("calStatStd","A"));
				
		

		//Create the DD
		Object[] arguments1 = {calStatDesc_A};
		String pullDown=VelosResourceBundle.getLabelString("L_No_Calendars",arguments1)/*"No "+calStatDesc_A+" Calendars"*****/;
		if (eventIds.size()>0) {
		    pullDown="<Select name=\"calId\" onChange=\"RefreshPage(document.sch)\">" ;
		 	for(int cnt=0; cnt<eventIds.size();cnt++)
			{

				if (new Integer(EJBUtil.stringToNum(protocolId)).equals(eventIds.get(cnt)) )
				{

						pullDown = pullDown +"<option value=" + ((Integer) eventIds.get(cnt)).toString() +" Selected> " + names.get(cnt)  +"</option>";

				}
				else
				{

						pullDown = pullDown +"<option value=" + ((Integer) eventIds.get(cnt)).toString() +" > " + names.get(cnt)  +"</option>";

				}
			}
		 	pullDown=pullDown+"</select>";

		}


		//End creating the DD


		//String pullDown=EJBUtil.createPullDownNoSelect("calId",EJBUtil.stringToNum(protocolId),eventIds,names);

		//If no protocol calendar is selected,select the first one by default if any in DD
		if (EJBUtil.stringToNum(protocolId)==0){
		  if (eventIds.size()>0)    protocolId=((Integer)eventIds.get(0)).toString();
		}

		if (EJBUtil.isEmpty(selDay) || "null".equals(selDay))
		{
			selDay = null;
		}

			 //FOR HANDLING PAGINATION
			String pagenum = "";
			int curPage = 0;
			long startPage = 1;
			String stPage;
			long cntr = 0;

			pagenum = request.getParameter("nextpage");

			if (StringUtil.isEmpty(pagenum))
			{
				pagenum = "1";
			}
			curPage = EJBUtil.stringToNum(pagenum);

			String orderBy = "";
			orderBy = request.getParameter("orderBy");
			String orderType = "";
			orderType = request.getParameter("orderType");
		 	if (orderType == null)
			{
				orderType = "";
			}

	 		String scheduleSql = "";
			String countSql = "";
		    long rowsPerPage=0;
			long totalPages=0;
			long rowsReturned = 0;
			long showPages = 0;

			boolean hasMore = false;
			boolean hasPrevious = false;
			long firstRec = 0;
			long lastRec = 0;
			long totalRows = 0;

			rowsPerPage =  Configuration.MOREBROWSERROWS ;
			totalPages =Configuration.PAGEPERBROWSER ;


			int evCRFCount = 0;

			String calSchDate=request.getParameter("schdate");
			calSchDate=(EJBUtil.isEmpty(calSchDate))?"":calSchDate;
			
			String buttonFlag=request.getParameter("buttonFlag");
			buttonFlag=(EJBUtil.isEmpty(buttonFlag))?"":buttonFlag;

			ArrayList crfIds = new ArrayList();
			ArrayList crfNames = new ArrayList();
			ArrayList crfformEntryChars = new ArrayList();
			ArrayList crfLinkedFormIds = new ArrayList();
			ArrayList crfformSavedCounts = new ArrayList();

			Integer crfId = null;
			String crfName = "";
			String crfFormEntryChar = "";
			String crfformSavedCount = "";
			String crfLinkedForm = "";
			String crfString = "";
			String popCrfString = "";
			String protName= "";
			String protDur = "";
			Date st_date1 = null;
			String protSchDate="";
			Integer prot_id = null;

			eventAssocB.setEvent_id(EJBUtil.stringToNum(protocolId));
			eventAssocB.getEventAssocDetails();
			protName= 	eventAssocB.getName();
			protDur = 	eventAssocB.getDuration();
			protSchDate=eventAssocB.getCalSchDate();
			String prevProtSchDate=protSchDate;
			prot_id = new Integer(EJBUtil.stringToNum(protocolId));


			if (calSchDate.length()>0 && !buttonFlag.equals("S"))
			{

			  if (!(DateUtil.stringToDate(calSchDate,null).equals(DateUtil.stringToDate(protSchDate,null))))

			  {
			        eventAssocB.setCalSchDate(calSchDate);
			        eventAssocB.updateEventAssoc();
			   	protSchDate=eventAssocB.getCalSchDate();

			  }


			}

			if (EJBUtil.isEmpty(protSchDate))   protSchDate="";




		    if (generate.equals("Y") && !buttonFlag.equals("S"))
			{
		        if ((calSchDate.length()>0))
		        {

			    Date d1=DateUtil.stringToDate(calSchDate,null);


		        Date d2=DateUtil.stringToDate(prevProtSchDate,null);
		     if   (!(d1.equals(d2)))
		     {
			 	//Deativate the previous one before generating a new schedule

 			 	eventdefB.DeactivateEvents(prot_id.intValue(),EJBUtil.stringToNum(study),DateUtil.dateToSqlDate(DateUtil.stringToDate(protSchDate,null)),"S");
				st_date1 = DateUtil.stringToDate(calSchDate,"");
				Integer selectedDisp = (null == selDay)? null : EJBUtil.stringToNum(selDay);
				java.sql.Date protStDate = DateUtil.dateToSqlDate(st_date1);
				eventdefB.GenerateSchedule(prot_id.intValue(),personPK,protStDate,EJBUtil.stringToNum(study),selectedDisp,usr,ipAdd);
				generate="F";

			}
		        }
			}


			String patStudyStat = "";
			String patStatSubType = "";


			CodeDao cdStat = new CodeDao();


			//patB.setId(EJBUtil.stringToNum(statid));
			//patB.getPatStudyStatDetails();





%>





<Form name="sch" id="stdSchFrm" method=get action="studyadmincal.jsp?nextpage=1&mode=M" onsubmit="if (setFilter(document.sch,'P' )== false) { setValidateFlag('false'); return false; } else { if(document.sch.buttonFlag.value=='S') setValidateFlag('false'); else setValidateFlag('true'); return true;}"   >

	<input type ="hidden" name="oldSchDate" value= <%=protSchDate%>>
	<input type ="hidden" name="buttonFlag" value="">	
	<table cellspacing="0" cellpadding="0" border="0" class="midAlign outline" width="99%">
	<tr height="7"><td colspan="3"></td></tr>
	<tr >
		<td class="lhsFont">
		<%=LC.L_Sch_Displayed%><%--Schedule Displayed*****--%>: <%=(pullDown==null)?MC.M_NoCalSelected/*No Calendar Selected*****/:pullDown%> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<%=LC.L_Sch_StartDate%><%--Schedule Start Date*****--%>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<%-- INF-20084 Datepicker-- AGodara --%>
		<input type="text" name="schdate" class="datefield" size = 10 MAXLENGTH = 20 value="<%=protSchDate%>" READONLY>


<!-- <A href="#" onClick="openWinProtocol('<%=pageRight%>','<%=orgRight%>', '<%=study%>','<%=pkey%>')">Edit Calendar/Date</A>-->



	<!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href="#" onClick="return viewschedule('patientpreschedule.jsp?patientId=<%=pkey%>&studyId=<%=study%>')">View Previous Schedule</A>-->
</td>

<td>&nbsp;
</td>

<td>
<% if ((pageRight > 4))
			{
				//genSchedule = true;
				generateGoFlag = "G" ;
			%>
				<jsp:include page="submitBar.jsp" flush="true">
						<jsp:param name="displayESign" value="N"/>
						<jsp:param name="formID" value="stdSchFrm"/>
						<jsp:param name="showDiscard" value="N"/>
				</jsp:include>

			<%} %>

</td>
</tr>


	</table>

	<% if (protocolId.equals("0")) {%>
	<table>
	<tr>
	<td>
	<br>
	<P class="defComments"><%=MC.M_SelCal_ToProc%><%--Please select a calendar to proceed.*****--%></P>
	</td>
	</tr>
	</table>


	<%
	}


	 if (!(protocolId.equals("0"))){
	%>







<Input type="hidden" name="srcmenu" value=<%=src%>>
<Input type="hidden" name="selectedTab" value=<%=selectedTab%>>
<Input type="hidden" name="status" value=S>
<Input type="hidden" name="generate" value=<%=generate%>>
<Input type="hidden" name="pkey" value=<%=pkey%>>
<Input type="hidden" name="statDesc" value=<%=statDesc%>>
<Input type="hidden" name="statid" value=<%=statid%>>
<Input type="hidden" name="studyVer" value=<%=studyVer%>>
<Input type="hidden" name="page1" value=<%=page1%>>
<Input type="hidden" name="patProtId" value=<%=enrollId%>>








		<!-- patient protoocl header was here-->

<%
			String event_id="";
			String chain_id="";
			String event_type="";
			String name="";
			String nameTrim = "";
			String notes="";
			String eventNotes="";
			String cost="";
			String cost_description="";
			String duration="";
			String user_id="";
			String linked_uri="";
			String fuzzy_period="";
			String msg_to="";
			String sts="";
			String description="";
			String displacement="";
			String org_id="";
			String fuzzy_dates = "";
			StringBuffer sbFilterWhereClause = new StringBuffer();
			String scheduleWhereForCRF = "";
			Hashtable htSchedule = new Hashtable();
			Hashtable htCRF = new Hashtable();
			Hashtable htCRFVisit = new Hashtable();
			String stDateToString = "";



			if (! EJBUtil.isEmpty(startdt))
			{
				startdt = startdt.toString().substring(6,10) + "-" + startdt.toString().substring(0,2) + "-" + startdt.toString().substring(3,5);
			}

			    String visit = request.getParameter("visit");
				String  month = request.getParameter("month");
				String year = request.getParameter("year");
				String event = request.getParameter("event");
				String evtStatus = request.getParameter("dstatus");
				String visitxt = request.getParameter("visitext");
				String evttxt = request.getParameter("eventext");
				String ststxt ="All";
				String durtxt = request.getParameter("monthyrtext");
				ststxt = request.getParameter("statustext");

				if ( EJBUtil.isEmpty(visitxt))  visitxt = "ALL";
				if ( 	EJBUtil.isEmpty(durtxt)) durtxt = "ALL";
				if ( 	EJBUtil.isEmpty(evttxt)) evttxt = "ALL" ;
				if ( 	EJBUtil.isEmpty(ststxt))	ststxt = "ALL" ;
				if ( EJBUtil.isEmpty(visit)) visit  = "0" ;
				if (EJBUtil.isEmpty(year)) year  = "0" ;
				if ( EJBUtil.isEmpty(month))  month  = "0" ;
				if ( EJBUtil.isEmpty(event))  event  = "All" ;
				if ( EJBUtil.isEmpty(evtStatus))  evtStatus   = "" ;
				event = event.trim();
%>


<%   		com.velos.esch.business.common.EventdefDao eventdao = new com.velos.esch.business.common.EventdefDao();
			eventdao = eventdefB.getStudyScheduleEventsVisits(EJBUtil.stringToNum(study),EJBUtil.stringToNum(protocolId));
			totalDefinedVisits = eventdao.getTotalDefinedVisitsInStudySchedule(EJBUtil.stringToNum(study),EJBUtil.stringToNum(protocolId));

			java.util.ArrayList visits = eventdao.getVisit();
			java.util.ArrayList visitnames = eventdao.getVisitName();

			System.out.println("visits"+visits+" visitnames "+visitnames);

			String visitDD="<option value=\"0\" Selected>"+LC.L_All/*All*****/+"</option>"  ;
			String visitName = "";


		 	for(int cnt=0; cnt< visits.size();cnt++)
			{
				int  tempVisit = Integer.parseInt(visit);
				if (totalDefinedVisits > 0)	{
					visitName = visitnames.get(cnt).toString();
				}
				else {
					visitName =  LC.L_Visit/*Visit*****/+cnt;
				}
				Integer integerVisit = new Integer(0);
				integerVisit = (Integer) visits.get(cnt) ;

				int intVisit = 0;
				intVisit = integerVisit.intValue();


				if ( tempVisit == intVisit  )
				{

						visitDD = visitDD +"<option value=" + intVisit +" Selected> " + visitName  +"</option>";


				}
				else
				{

						visitDD = visitDD +"<option value=" + intVisit +" > " + visitName  +"</option>";

				}
			}


%>
<%

			//get event names from eventdao - sonia

			java.util.ArrayList eventDescs = eventdao.getNames();

			//com.velos.esch.business.common.EventdefDao ctrldao = new com.velos.esch.business.common.EventdefDao();

			String eventsDD="<option value=\"All\" Selected>"+LC.L_All/*All*****/+"</option>"  ;
			//Prabhat: Fixed BUG #4277




		for( int cnt=1; cnt<=eventDescs.size();cnt++)
			{
			 	String tempEvent = eventDescs.get(cnt - 1).toString() ;
			 	//Fixed BUG# 4847 By Prabhat
			 	String tempEventTrim = tempEvent;
			 	if(tempEventTrim.length()>50){
			 		tempEventTrim = tempEventTrim.substring(0,50)+"...";
			 	}


				if ( tempEvent.trim().equals(event))
				{

					eventsDD = eventsDD +"<option value='" +eventDescs.get(cnt - 1) +"' Selected>" +tempEventTrim +" </option>";


				}
				else
				eventsDD = eventsDD +"<option value='" +eventDescs.get(cnt - 1) +"'>" +tempEventTrim +"</option>";
			}



	%>

<Input type="hidden" name="visitext" value="<%=visitxt%>">
<Input type="hidden" name="eventext"    value="<%=evttxt%>" >
<Input type="hidden" name="statustext"  value="<%=ststxt%>" >
<Input type="hidden" name="monthyrtext"  value="<%=durtxt%>" >


<Input type="hidden" name="filEvent"  value="<%=event%>" >
<Input type="hidden" name="filMonth"  value="<%=month%>" >
<Input type="hidden" name="filYear"  value="<%=year%>" >
<Input type="hidden" name="filVisit"  value="<%=visit%>" >
<Input type="hidden" name="filStatus"  value="<%=evtStatus%>" >


<%



				//if (evtStatus.equals("1")) ststxt = "Not Done";
				//if (evtStatus.equals("5")) ststxt = "Done";

		//SV, 10/29/04, very funny problem (bug #1855). If you are changing this SQL, particularly table aliases, it affects CRF SQL
		// in backend. Reason being, sbFilterWhereClause is being used in both this SQL and the crf SQL in stored procedure.
		// when I added ev as alias for sch_events1 to avoid ambiguity between description fields in sch_events and protocol_visit
		// it affected the where condition for crf, which failed due to the fact the alias wasn't defined there. added the alias in
		// v_crf_sql in sp_get_schedule_page_records in PKG_GENSCH. This is the only place we call BrowserRows.getSchedulePageRows which
		// calls this sp. If this scenario changes, pls. make sure this sql and PKG_GENSCH.sp_get_schedule_page_records are kept in synch.
		if (totalDefinedVisits > 0) {

	 	scheduleSql=   "select ev.DESCRIPTION, to_char(ev.START_DATE_TIME, 'yyyy-mm-dd') START_DATE_TIME, "
							+ " nvl(evass.fuzzy_period,0) fuzzy_period , "
							+ " evass.EVENT_DURATIONBEFORE,evass.EVENT_FUZZYAFTER,evass.EVENT_DURATIONAFTER,"
							+ " ev.EVENT_ID, "
							+ " ev.ISCONFIRMED, "
							+ " ev.FK_ASSOC, "
							+ " ev.NOTES, "
							+ "	to_char(ev.EVENT_EXEON, PKG_DATEUTIL.F_GET_DATEFORMAT)  EVENT_EXEON, "
							+ "	to_char(ev.ACTUAL_SCHDATE, 'yyyy-mm-dd')  ACTUAL_SCHDATE, "
							+ "	ev.EVENT_EXEBY, "
							+ "	ev.ADVERSE_COUNT, "
							+ " ev.visit, "
							+ " vis.visit_name,ev.fk_visit, "
							+ " ev.status,ev.event_notes "
							+ " FROM SCH_EVENTS1 ev"
							+ " , SCH_PROTOCOL_VISIT vis"
							+ " , EVENT_ASSOC evass"
							+ " WHERE  ev.fk_study =  " +study
							+ " and ev.session_id = lpad(to_char("+protocolId  +"),10,'0') "
							+ " and ev.fk_visit = vis.pk_protocol_visit "
							+ " and evass.event_id = ev.fk_assoc "
							+ " and ev.status = 0 ";
		}

		else {

			 	scheduleSql=   "select ev.DESCRIPTION, to_char(ev.START_DATE_TIME, 'yyyy-mm-dd') START_DATE_TIME, "
							+ " nvl(evass.fuzzy_period,0) fuzzy_period , ev.EVENT_ID, "
							+ " ev.ISCONFIRMED, "
							+ "	ev.FK_ASSOC, "
							+ "	ev.NOTES, "
							+ "	to_char(ev.EVENT_EXEON, PKG_DATEUTIL.F_GET_DATEFORMAT)  EVENT_EXEON, "
							+ "	to_char(ev.ACTUAL_SCHDATE, 'yyyy-mm-dd')  ACTUAL_SCHDATE, "
							+ "	ev.EVENT_EXEBY, "
							+ "	ev.ADVERSE_COUNT, "
							+ " ev.visit, ev.fk_visit, "
							+ " ev.status, ev.event_notes "
							+ " FROM SCH_EVENTS1 ev"
							+ " , EVENT_ASSOC evass"
							+ " WHERE  ev.fk_study = "+ study
							+ " and ev.session_id = lpad(to_char("+protocolId  +"),10,'0') "
							+ " and evass.event_id = ev.fk_assoc "
							+ " and ev.status = 0 ";

			}

				if (!(visit.equals("0"))){
					sbFilterWhereClause.append(" and ev.visit = " + visit);
				}
				if (!(year.equals("0"))){
					sbFilterWhereClause.append(" and to_Char(ev.ACTUAL_SCHDATE,'yyyy') = " + year) ;
				}
				if (!(month.equals("0"))){
					sbFilterWhereClause.append(" and to_Char(ev.ACTUAL_SCHDATE,'mm') = " + month);
				}
				if ( !( event.equals("All")  )               )
				{
				   sbFilterWhereClause.append(" and UPPER(trim(ev.DESCRIPTION)) = UPPER(trim('" + event +"'))");

				}
				if (!(evtStatus.equals(""))){
					sbFilterWhereClause.append(" and ev.ISCONFIRMED = '" +evtStatus +"'");
				}

				scheduleSql = scheduleSql + sbFilterWhereClause.toString() ;


				//scheduleSql += " ORDER BY ev.ACTUAL_SCHDATE, ev.fk_visit, ev.EVENT_ID ";
				//JM: 18Apr2008:blocked, and modified
				scheduleSql += " ORDER BY ev.ACTUAL_SCHDATE, ev.fk_visit, ev.event_sequence ";


				//JM: 18Apr2008: modified
				//scheduleWhereForCRF = 	" Where ev.fk_study = " + study	+ " and ev.status = 0 " + sbFilterWhereClause.toString() + " ORDER BY ev.ACTUAL_SCHDATE, ev.START_DATE_TIME, ev.EVENT_ID ";
				scheduleWhereForCRF = 	" Where ev.fk_study = " + study	+ " and ev.status = 0 " + sbFilterWhereClause.toString() + " ORDER BY ev.ACTUAL_SCHDATE, ev.START_DATE_TIME, ev.event_sequence";


		// ctrldao = eventdefB.FetchFilteredSchedule(prot_id.intValue(),personPK,startdt,visit,month,year,event,evtStatus);
		  countSql="select  count(*) "
					+ " FROM SCH_EVENTS1 ev"
					+ " WHERE  fk_study = "+study
					+ " and session_id = lpad(to_char("+protocolId  +"),10,'0') "
					+	" and status = 0 " ;

			countSql = countSql + sbFilterWhereClause.toString();

				htSchedule  = bRows.getSchedulePageRows(curPage,rowsPerPage,scheduleSql,totalPages,countSql,orderBy,"null",scheduleWhereForCRF);

				//get objects for Schedule, hashtable for event CRF and Visit CRF
				br = (BrowserRows) htSchedule.get("schedule");
				htCRF = (Hashtable) htSchedule.get("eventcrf");
				htCRFVisit = (Hashtable) htSchedule.get("visitcrf"); //contains VISIT CRF information

		   	   rowsReturned = br.getRowReturned();
			   showPages = br.getShowPages();
			   startPage = br.getStartPage();
			   hasMore = br.getHasMore();
			   hasPrevious = br.getHasPrevious();
			   totalRows = br.getTotalRows();
			   firstRec = br.getFirstRec();
			   lastRec = br.getLastRec();






%>
<!--			<table width=100%>
			<tr>
			<td width=20%>&nbsp;</td>
			<td width=20%>&nbsp;</td>
			<td width=20%>&nbsp;</td>
			<td width=40%>
			<% if ((pageRight > 4))
			{
				//genSchedule = true;
				generateGoFlag = "G" ;
			%>
				<Input type=image src="../images/jpg/Generate.gif" border="0" onClick="return validateGenerate(document.sch,'<%=protSchDate%>')">
			<%} %>
			</td>
			</tr>
			</table> -->


		    <Input type="hidden" name="generateGoFlag" value=<%=generateGoFlag%> >
<%
	 	if (        (   rowsReturned > 0) )
			{ %>
		<!--	<P class=defcomments> The Selected filters are:
			<U>Visit</u>: <B><I><%=visitxt%></I></B>&nbsp;&nbsp;&nbsp;&nbsp;<U>Month/Year</u>:<B><I><%=durtxt%></I></B>
			&nbsp;&nbsp;&nbsp;&nbsp;<u>Event</u>:<B><I><%=evttxt%></I></B>&nbsp;&nbsp;&nbsp;&nbsp;<u>Status</u>:<B><I><%=ststxt%></I></B>
			</P> -->
		  <Input type="hidden" name="generateGoFlag" value=<%=generateGoFlag%>>
		 <%}%>

<%

			String prevMonth="";
			String thisMonth="";
			Integer newVisit ;
			Integer prevVisit = null ;
			int prevYr = 0;
			ArrayList monthName = new	ArrayList();
			monthName.add(LC.L_January/*January*****/);
			monthName.add(LC.L_February/*February*****/);
			monthName.add(LC.L_March/*March*****/);
			monthName.add(LC.L_April/*April*****/);
			monthName.add(LC.L_May/*May*****/);
			monthName.add(LC.L_June/*June*****/);
			monthName.add(LC.L_July/*July*****/);
			monthName.add(LC.L_August/*August*****/);
			monthName.add(LC.L_September/*September*****/);
			monthName.add(LC.L_October/*October*****/);
			monthName.add(LC.L_November/*November*****/);
			monthName.add(LC.L_December/*December*****/);

			int visitCnt = 0;
			String start_dt="";
			String prev_dt="00/00/0000";
			 visit ="";
			int crfCount = 0;
			Calendar cal2 = Calendar.getInstance();
			int currYear = cal2.get(cal2.YEAR);
%>
<% 			//if ((event_ids.size() > 0) || (request.getParameter("generate").equals("F")))

			if (( rowsReturned>= 0))
			{  %>
				<div class="tmpHeight"></div>
				<TABLE width="99%" cellspacing="0" cellpadding="0" border="0" class="outline midAlign">
				<tr height="7"><td></td></tr>
				<tr>
				<td class="lhsFont">
				<%=LC.L_Visit%><%--Visit*****--%>:
				</td>
				<td class="lhsFont">
				<%=LC.L_MonthOrYear%><%--Month/Year*****--%>:
				</td>
				<td class="lhsFont">
				<%=LC.L_Event%><%--Event*****--%>:
				</td>
				<td class="lhsFont">
				<%=LC.L_Status%><%--Status*****--%>:
				</td>
				<td class="lhsFont">
				&nbsp;
				</td>
				</tr>
				<tr>
				<td class="lhsFont">
				<select name=visit>
				<%=visitDD%> </select>
				</td>
				<td class="lhsFont">
				<Select name=month >
				<option value="0" Selected><%=LC.L_All%><%--All*****--%></option>
			    <% int cnt = 0;
        		for (cnt= 1; cnt<= 12;cnt ++)
				{
		 			int tempMonth =Integer.parseInt(month) ;


					if ( tempMonth == cnt )
					{%>
							<option value=<%=cnt%> Selected><%=cnt%></option>
				<%}
					else
					{%>
	        		<option value=<%=cnt%>><%=cnt%></option>
				<%} //end else
    		   } //end for of month%>
					</Select>
					<Select name="year">
					<option value="0" Selected><%=LC.L_All%><%--All*****--%></option>
		    	<%
				int i = currYear - 50;
				currYear = currYear + 10;

			    int count = 0;
    			for (count= currYear; count >= i ;count--)
				{
						int tempYear = Integer.parseInt(year);

		   				if (count == currYear)
						{
										if ( count ==tempYear )
										{ %>
										<option value=<%=count%> Selected><%=count%></option>
										<%}
									 	else
									 	{ %>
				    						<option value=<%=count%> ><%=count%></option>
										<%}
				    	} else
						{
									if ( count ==tempYear )
									{ %>
									<option value=<%=count%> Selected><%=count%></option>
									<%}
									else
									{ %>
					    					<option value=<%=count%>><%=count%></option>
									<%}

			    	}%>



		    <%}  //end of for loop year%>
				</select>
			</td>
			<td class="lhsFont">
				<Select name=event>
				<%=eventsDD%>
				</Select>
				</td>
				<td>
				<%=dEvStat%></td>
				<td>
				 <button type="submit" onClick="setFlag(document.sch)"><%=LC.L_Search%></button>
				</td>
			</tr>


<%
			if(eptRight==1)
			{
%>

<!--				<td colspan=5 align=right>
				<A HREF="crfstatusbrowser.jsp?selectedTab=<%=selectedTab%>&srcmenu=<%=src%>&page=<%=page1%>&patProtId=<%=enrollId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&studyVer=<%=studyVer%>&eventId=<%=event_id%>&pkey=<%=pkey%>&visit=0&eventName=<%=name%>&formType=other">Form Status/Tracking</A>

				</td>
-->

<%
			}

%>

<tr height="7"><td></td></tr>
</Table>
<div class="tmpHeight"></div>
<TABLE width="99%" cellspacing="0" cellpadding="0" border="0" class="outline midAlign">
  <tr>
				<td colspan=6 align=right>




				<A HREF=# onClick='return editMultipleEvents("editStudyMulEventDetails.jsp?studyId=<%=study%>&calId=<%=protocolId%>","<%=pageRight%>","<%=orgRight%>")'><%=LC.L_Edit_MultiEvts_Upper %><%-- EDIT MULTIPLE EVENTS*****--%></A>

				</td>
	</tr>
	<TR>
	<TH width="10%"><%=LC.L_Visit%><%--Visit*****--%></TH>
    <TH width="10%"><%=LC.L_Suggested_Date%><%--Suggested Date*****--%></TH>
<%


			if(eptRight==1)
			{
%>
				<TH width="13%"><%=LC.L_ActualScheduled_Dt%><%--Actual Scheduled Date*****--%></TH>
<%
			}
%>
  					 <TH width="15%"><%=LC.L_Event_Window%><%--Event Window*****--%> </TH>
					 <TH width="25%"><%=LC.L_Events%><%--Events*****--%></TH>
			    	 <TH width="25%"><%=LC.L_Event_Status%><%--Event Status*****--%></TH>

<%
			if(eptRight==1)
			{

				ctrl1.getControlValues("crfmodule");
				ctrlValue = ctrl1.getCValue();

				acmod.getControlValues("module");
				ArrayList acmodfeature =  acmod.getCValue();
				ArrayList acmodftrSeq = acmod.getCSeq();
				formLibSeq = acmodfeature.indexOf("MODFORMS");
				formLibSeq = ((Integer) acmodftrSeq.get(formLibSeq)).intValue();
				formLibAppRight = modRight.charAt(formLibSeq - 1);

				if(ctrlValue.get(0).equals("1"))
				{
%>
				<%if (String.valueOf(formLibAppRight).compareTo("1") == 0){%>

				<!--					 <TH width="10%">CRF</TH>
				IA 10.16.2006
				Bug 2674 Hide CRF column from the admin schedule
				-->
					 <TH width="25%"  style="display:none"><%=LC.L_Linked_Frm%><%--Linked Forms21*****--%></TH>

					<%}
				else{%>

					 <TH width="25%"  style="display:none" ><%=LC.L_Crf_Upper%><%--CRF*****--%></TH>
					<%}
				}
			}
%>
  </TR>

	<% }

	String st_exon = null;
	Calendar todayCal = Calendar.getInstance();
	boolean pastDate = false;
	String prevEventId = "";
	String hidden_notes = null;
	int j =0;


	/////////////////////


	String exe_by =  ""  ;   //  (String) user_ids.get(i);
	String newVisitName="";
	Calendar cal ;
	java.sql.Date st_date ;


	int year1 = 0;
	String acDateFlag = "";

	for(int i=1; i<=rowsReturned ; i++)
	{

		 cal = Calendar.getInstance();


		 st_date =  java.sql.Date.valueOf(  br.getBValues(i,"START_DATE_TIME")  ) ;
		 stDateToString = st_date.toString().substring(5,7) + "/" + st_date.toString().substring(8,10) + "/" + st_date.toString().substring(0,4);

		java.sql.Date actualDate = null;

		if( br.getBValues(i,"ACTUAL_SCHDATE") != null  )
			actualDate =  java.sql.Date.valueOf( br.getBValues(i,"ACTUAL_SCHDATE"));

		if(actualDate == null) {
			acDateFlag ="1";
			actualDate = st_date;
		}

		newVisit =  Integer.valueOf( br.getBValues(i,"fk_VISIT")  );

		newVisitName = 	 br.getBValues(i,"VISIT_NAME"); //SV, 10/28/04, display name instead of visit #.
		newVisitName=(newVisitName==null)?"":newVisitName;
		java.sql.Date exe_on =       null              ;

		 String tmpExeOn = br.getBValues(i,"EVENT_EXEON") ;

		 if (  tmpExeOn == null )
		 tmpExeOn = "" ;



		exe_by =  br.getBValues(i,"EVENT_EXEBY");

		notes  =br.getBValues(i,"NOTES");

		eventNotes=br.getBValues(i,"EVENT_NOTES");
		eventNotes=(eventNotes==null)?"":eventNotes;


		if(!acDateFlag.equals("1")) {
		cal.setTime(actualDate);
		pastDate = DateUtil.compareCalendar(todayCal,cal,">");


		int month1 = cal.get(cal.MONTH);
		year1 = cal.get(cal.YEAR);
		thisMonth =(String) monthName.get(month1);
		}

		java.sql.Date temp_date = (java.sql.Date)st_date.clone();

		java.sql.Date temp_date1 = (java.sql.Date)st_date.clone();
		java.sql.Date temp_date2 = (java.sql.Date)st_date.clone();
		String fuzzyPeriod = "";
		String fuzzyPeriodBefore = "";

		int day = 0;

		name  =br.getBValues(i,"DESCRIPTION");
		sts  =   br.getBValues(i,"ISCONFIRMED") ;     //  (String)stat.get(i);

		Integer temp = Integer.valueOf(br.getBValues(i,"FK_ASSOC") ) ;

		Integer ev_id= Integer.valueOf(  br.getBValues(i,"EVENT_ID")   );


		prevEventId = event_id;
		event_id = ev_id.toString();
		org_id = temp.toString();
		start_dt = actualDate.toString();

		String str="";
		String link="";
		String cLink = "";//JM: 16Jul2008
		evDesc = cd.getDesc(sts);
		subType  = cd.getSubType(sts);


		//get CRF DATA for the event
	if (String.valueOf(formLibAppRight).compareTo("1") == 0){
		if (htCRF.containsKey(ev_id))
		{


			CrfDao evCrf = new  CrfDao();
      		evCrf = (CrfDao) htCRF.get(ev_id);

      		//Prepare CRF STRING

      		crfIds = evCrf.getCrfIds();

      		crfNames = evCrf.getCrfNames();
      		crfformEntryChars = evCrf.getCrfformEntryChars();
      		crfLinkedFormIds = evCrf.getCrfLinkedFormIds();
      		crfformSavedCounts = evCrf.getCrfformSavedCounts();
      		evCRFCount = crfIds.size();

      		crfString = "";

		for (int p = 0; p < evCRFCount; p++)
      		{
      			crfName = "";
      			crfFormEntryChar = "";
      			crfLinkedForm = "";
				crfformSavedCount = "";

      		    crfId = (Integer) crfIds.get(p);
      			crfName = (String) crfNames.get(p);

      			crfLinkedForm = (String) crfLinkedFormIds.get(p);

      			crfFormEntryChar = (String)	crfformEntryChars.get(p);


      			crfformSavedCount = (String)crfformSavedCounts.get(p);
      			crfformSavedCount = EJBUtil.isEmpty(crfformSavedCount) ? "0" : crfformSavedCount;//JM: added

      			crfLinkedForm = EJBUtil.isEmpty(crfLinkedForm) ? "0" : crfLinkedForm;
      			crfFormEntryChar = EJBUtil.isEmpty(crfFormEntryChar) ? "0" : crfFormEntryChar;

				if ((crfFormEntryChar.equals("M")) || ((crfFormEntryChar.equals("E")) && (crfformSavedCount.equals("0")))   )
				{
					newHrf = "<A href='crfformdetails.jsp?srcmenu="+src+"&selectedTab="+selectedTab+"&linkedFormId="+crfLinkedForm+"&mode=N&formDispLocation=C&entryChar="+crfFormEntryChar+"&fkcrf="+crfId+"&schevent="+event_id +"&pkey="+pkey+"&patProtId="+enrollId+"&studyId="+study+"&calledFrom=S&statDesc="+statDesc+"&statid="+statid+"&patientCode="+StringUtil.encodeString(patientId)+"'>"+LC.L_New/*New*****/+"</A>";
				} else newHrf = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

				editHrf="<A href='formfilledcrfbrowser.jsp?srcmenu="+src+"&selectedTab="+selectedTab+"&mode=M&linkedFormId="+crfLinkedForm+"&formDispLocation=C&entryChar="+crfFormEntryChar+"&fkcrf="+crfId+"&schevent="+event_id +"&pkey="+pkey+"&patProtId="+enrollId+"&studyId="+study+"&calledFrom=S&statDesc="+statDesc+"&statid="+statid+"&patientCode="+StringUtil.encodeString(patientId)+"'>"+LC.L_Edit/*Edit*****/+"</A>";

				if (crfLinkedForm.equals("0")) {
				  //do not show edit and new links if crf has been added as text and not as a linkedform
					crfString = crfString +  crfName + "<BR>";
				} else {
					crfString = crfString + crfName + "<BR>" + newHrf + "&nbsp;&nbsp;&nbsp;&nbsp;"+ editHrf + "<BR>";
				}

      		}
		}
		else
		{
			crfString = LC.L_No_Crf_Upper;/*crfString = "NO CRF";*****/
		}

	}//end of if form right module
		if (exe_on!=null)
		{
			st_exon = exe_on.toString().substring(5,7) + "/" + exe_on.toString().substring(8,10) + "/" + exe_on.toString().substring(0,4);
		}

		if(notes!=null)
			{
		           	notes=notes.replace(' ','~');
			}
			else
			{
				notes = "";
			}


//JM:14Feb2008: introduced fk_eventId sch_eventusr.fk_Event, 29Jul2008 added st_date
//KM:14Sep09- Event Name removed
		  link = "MarkDone.jsp?event_id=" +event_id +"&mode=M&fk_eventId=" + org_id +"&protocol_id=" +prot_id +"&pkey=" +pkey + "&statDesc="+statDesc+ "&statid="+sts+"&startdt="+DateUtil.dateToString(st_date)+"&schDate="+schDate+"&calledFrom=S&calAssoc=S";
		  sts = evDesc;
		  st_exon = null;

		 fuzzy_period =     br.getBValues(i,"fuzzy_period");

 		 if (StringUtil.isEmpty(fuzzy_period)) fuzzy_period="";


		 String fuzzy_period_before_unit = br.getBValues(i,"EVENT_DURATIONBEFORE");
		 if (StringUtil.isEmpty(fuzzy_period_before_unit)) fuzzy_period_before_unit ="";

		 String fuzzy_period_after =br.getBValues(i,"EVENT_FUZZYAFTER");
		 if (StringUtil.isEmpty(fuzzy_period_after)) fuzzy_period_after="";

		 String fuzzy_period_after_unit =br.getBValues(i,"EVENT_DURATIONAFTER");
		 if (StringUtil.isEmpty(fuzzy_period_after_unit)) fuzzy_period_after_unit ="";

		String fuzzyPeriodB = fuzzy_period;
		String fuzzyPeriodA = fuzzy_period_after;

		String fuzzy_result_before = "0";
		String fuzzy_result_after = "0";

		//Added by Gopu to fix the Bugzilla Issues #2584 & #2347 (0 Months Before n Months After)
		String fuzzy_month ="0";

					if(fuzzy_period_before_unit!= null && fuzzy_period_before_unit.equals("H")){
						fuzzy_result_before = "-1";
					}

					if(fuzzy_period_before_unit != null && fuzzy_period_before_unit.equals("D")){
						fuzzy_result_before = "-"+fuzzyPeriodB.trim();
					}

					if(fuzzy_period_before_unit != null && fuzzy_period_before_unit.equals("W")){
						fuzzy_result_before = "-" + ((EJBUtil.stringToNum(fuzzyPeriodB.trim())  * 7));
					}

					if (fuzzy_period_after_unit!= null && fuzzy_period_after_unit.equals("H")){
						fuzzy_result_after ="1";
					}

					if (fuzzy_period_after_unit !=null && fuzzy_period_after_unit.equals("W")){
						fuzzy_result_after = "" +((EJBUtil.stringToNum(fuzzyPeriodA.trim())  * 7));
					}
					if (fuzzy_period_after_unit!= null && fuzzy_period_after_unit.equals("D")){
						fuzzy_result_after = fuzzyPeriodA.trim();
					}

			//	Added by Gopu to fix the Bugzilla Issues #2584 & #2347 (0 Months Before n Months After)
			  if(fuzzy_period_before_unit.equals("M") && !fuzzyPeriodB.equals("0")){
					fuzzy_month = "1";
			  }

		fuzzy_dates = "-";

		String fuzzyResultBefore = "";//
			if ((fuzzy_period.trim().equals("0")) && (fuzzy_period_after.trim().equals("0"))){
		 		fuzzy_dates =  "-";
		 	 }
			else if ((!(fuzzy_period.trim().equals("0"))) || ((!fuzzy_result_after.trim().equals("0"))) || ((!fuzzy_result_before.trim().equals("0"))) || fuzzy_month.equals("1")  )
			{
				fuzzy_period = fuzzy_period.trim();
				Integer disp;
				java.util.Calendar Cal = java.util.Calendar.getInstance();


				if((fuzzy_period.substring(0,1)).equals("+"))
				{

					fuzzy_dates = DateUtil.dateToString(st_date);
					fuzzy_dates += " -- ";

					fuzzyPeriod = fuzzy_period.substring(1,fuzzy_period.length());
					disp = new Integer(fuzzyPeriod);
					day = disp.intValue();

					Cal.setTime(temp_date);
				if(fuzzy_period_after_unit != null){
					if (fuzzy_period_after_unit.equals("M")){
						Cal.add(java.util.Calendar.MONTH,EJBUtil.stringToNum(fuzzyPeriodA.trim()));
					}else{
						Cal.add(java.util.Calendar.DATE, day);
					}
					temp_date1 = new java.sql.Date(Cal.get(java.util.Calendar.YEAR) -  1900,Cal.get(java.util.Calendar.MONTH),Cal.get(java.util.Calendar.DATE));

					fuzzy_dates += DateUtil.dateToString(temp_date1);
				} else {
					fuzzy_dates += "-";
				}
			}
			else if((fuzzy_period.substring(0,1)).equals("-"))
			{
				fuzzyPeriod = fuzzy_period.substring(1,fuzzy_period.length());
				fuzzyPeriodBefore = "-" + fuzzyPeriod;
				disp = new Integer(fuzzyPeriodBefore);
				day = disp.intValue();

				Cal.setTime(temp_date);
				//////

				if(fuzzy_period_before_unit !=null){

				if (fuzzy_period_before_unit.equals("M")){
						Cal.add(java.util.Calendar.MONTH, (new Integer("-"+fuzzyPeriodB.trim()).intValue()) );
				}else{
					Cal.add(java.util.Calendar.DATE, day);
				}
				temp_date1 = new java.sql.Date(Cal.get(java.util.Calendar.YEAR) -  1900,Cal.get(java.util.Calendar.MONTH),Cal.get(java.util.Calendar.DATE));

				fuzzy_dates = DateUtil.dateToString(temp_date1);
				fuzzy_dates += " -- ";

				fuzzy_dates += DateUtil.dateToString(st_date);
			} else {
				fuzzy_dates +="-";
					}
			}else
			{
				fuzzyPeriod = fuzzy_period.substring(0,fuzzy_period.length());
				fuzzyPeriodBefore = fuzzy_result_before;
				//fuzzyPeriodBefore = "-" + fuzzyPeriod;
				disp = new Integer(fuzzyPeriodBefore);
				day = disp.intValue();


				Cal.setTime(temp_date);

				//Cal.add(java.util.Calendar.DATE, day);
				if (fuzzy_period_before_unit!=null && fuzzy_period_before_unit.equals("M")){
				//Cal.add(java.util.Calendar.MONTH, (new Integer("-"+fuzzyPeriodB.trim()).intValue()) );

				//Added by Manimaran to fix the Bug 2586
				day=EJBUtil.stringToNum(fuzzyPeriodB.trim())*(-30);
				Cal.add(java.util.Calendar.DATE, day);
				}else{
					Cal.add(java.util.Calendar.DATE, day);
				}
				temp_date1 = new java.sql.Date(Cal.get(java.util.Calendar.YEAR) -  1900,Cal.get(java.util.Calendar.MONTH),Cal.get(java.util.Calendar.DATE));

				fuzzy_dates = DateUtil.dateToString(temp_date1);
				fuzzy_dates += "--";
				//disp = new Integer(fuzzyPeriod);
				// Modified by gopu to fix the bugzilla Issue #2584
				disp = new Integer(fuzzy_result_after);
				day = disp.intValue();
				Cal.setTime(temp_date);
				if (fuzzy_period_after_unit!=null &&  fuzzy_period_after_unit.equals("M")){
				//Cal.add(java.util.Calendar.MONTH,EJBUtil.stringToNum(fuzzyPeriodA.trim()));
				//Added by Manimaran to fix the Bug 2586
				day=EJBUtil.stringToNum(fuzzyPeriodA.trim())*30;
				Cal.add(java.util.Calendar.DATE, day);

				}else{
						Cal.add(java.util.Calendar.DATE, day);
				}
				temp_date2 = new java.sql.Date(Cal.get(java.util.Calendar.YEAR) -  1900,Cal.get(java.util.Calendar.MONTH),Cal.get(java.util.Calendar.DATE));

				fuzzy_dates += DateUtil.dateToString(temp_date2);

			}
		}

		schDate = DateUtil.dateToString(st_date);
		acDate = DateUtil.dateToString(actualDate);

	//if(!intFlag.equals("1") && (!acDateFlag.equals("1"))   ) {
	if(!acDateFlag.equals("1"))   {
	if ((year1 != prevYr) || (! thisMonth.equals(prevMonth)))
	{%>
	<!-- IA 10.16.2006 Bug 2677 Hide the CRF column  -->
			<TR>
				<TD class=tdDefault Colspan=3><B><%=thisMonth%>&nbsp;&nbsp;<%=year1%></B></TD>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
			<TD style="display:none">&nbsp;</TD>
			</TR>
	<%	}}

		if(!newVisit.equals(prevVisit))
		{


			if(j%2==0){ %>
			<TR class="browserEvenRow">
		<%}else{%>
			<TR class="browserOddRow">
		<% } %>
				<%if (totalDefinedVisits > 0)	{%>
					<TD colspan="2"><%=newVisitName%></TD>
				<%}else {%>
					<TD><%=newVisit%></TD>
				<%}%>
<!-- IA 10.16.2006 Bug 2677 Hide the CRF column  -->
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
			<%if(eptRight==1){%>
			<TD style="display:none">&nbsp;</TD>
			<%}%>


<%

		if(eptRight==1)
		{
           	name=name.replace(' ','~');

			if (htCRFVisit.containsKey(newVisit))
			{
				//for sonika, make anobj of arraylist, get the CRF names from that arraylist
				crfCount = ((ArrayList) htCRFVisit.get(newVisit)).size();
				crfNamesArr = (ArrayList) (htCRFVisit.get(newVisit));
				popCrfString = "<UL>";
				for (int k=0;k<crfCount;k++) {
					popCrfString = popCrfString + "<LI>" + crfNamesArr.get(k) + "</LI>";
				}
				popCrfString = popCrfString + "</UL>";

			}
			else
			{
				crfCount = 0;
			}

			if(crfCount > 0)
			{
%>
<!--			<TD><A HREF="crfstatusbrowser.jsp?selectedTab=<%=selectedTab%>&srcmenu=<%=src%>&page=<%=page1%>&patProtId=<%=enrollId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&studyVer=<%=studyVer%>&eventId=<%=event_id%>&pkey=<%=pkey%>&visit=<%=newVisit%>&visitName=<%=newVisitName%>&eventName=<%=URLEncoder.encode(name, "UTF-8")%>&formType=crf" onmouseover="return overlib('<%=popCrfString%>',CAPTION,'Associated Forms');" onmouseout="return nd();">View Associated Forms (<%=crfCount%>)</A></TD> -->
<%
			} else
			 { //else of if(crfCount > 0)
			 if(!(patStatSubType.trim().equalsIgnoreCase("offstudy")))
			 {

%>
<!--			<TD><A  onClick="return f_check_perm_org(<%=pageRight%>,<%=orgRight%>,'N')"  HREF="crfstatus.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&mode=N&pkey=<%=pkey%>&visit=<%=newVisit%>&visitName=<%=newVisitName%>&eventName=<%=URLEncoder.encode(name, "UTF-8")%>&page=<%=page1%>&studyId=<%=study%>&statDesc=<%=statDesc%>&statid=<%=statid%>&patProtId=<%=enrollId%>&studyVer=<%=studyVer%>&patientCode=<%=patientId%>&formType=crf">No Associated Forms</A></TD> -->
<%
			 }
			 else
			 {
				%>
<!--				<TD>&nbsp;</TD> -->
				<%
			 }
			}
	           	name=name.replace('~',' ');
%>
  		    <TD>&nbsp;</TD>
			</TR>
<%

		}
			j++;

		}

		if ((pastDate) && (eptRight==1)&& (subType.equals("ev_notdone")))//if current date is past act date
		{
			sts=LC.L_PostSch_Dt/*sts="Past Scheduled Date"*****/ ;
%>
			<TR class="pastSchRow">
<%
		} else if(j%2==0)
		{
%>
			<TR class="browserEvenRow">
<%
			}else
			{
%>
			<TR class="browserOddRow">
<%
			}
%>
			<TD>&nbsp;</TD>
			<%
			if(acDateFlag.equals("1"))
			{
				schDate =LC.L_Not_Defined;/*schDate ="Not Defined";*****/

			} %>

			<TD><%=schDate%></TD>






		<%
		//KM- Event name size changes

		if(acDateFlag.equals("1"))
		{
				   acDate ="Null";
		}

		String datelink ="changedate.jsp?prevDate=" +acDate + "&eventId=" +event_id  +"&studyId=" + study + "&eventId=" +event_id + "&pkey=" + pkey + "&statDesc=" + statDesc+ "&statid=" + statid + "&studyVer=" +
		studyVer+"&patientCode="+StringUtil.encodeString(patientId)+"&calassoc=S&calId="+protocolId+"&acDateFlag="+acDateFlag+"&org_id="+org_id+"&calledFrom=S&protocol_id=" +prot_id ;

		%>

<%
		if(eptRight==1)
		{
			//user cannot change the event status if patient is offstudy

%>				<TD>
				
		<table width="100%" cellpadding="0" cellspacing="0" align="center" border="0">
			<%	if ((pastDate) && (eptRight==1)&& (subType.equals("ev_notdone")))//if current date is past act date
			{%>
				<TR class="pastSchRow">
		<%
				} else
			{
		%>
					<TR>
		<%
			}
		%>
    	<td width="42%" style="border: 0px">
	    <% if(acDateFlag.equals("1"))
				{
				   acDate =LC.L_Not_Defined;/*acDate ="Not Defined";*****/
				} %>
				<%=acDate%>
    	</td>
			<td width="12%" style="border: 0px">
			<%
					if(!(patStatSubType.trim().equalsIgnoreCase("offstudy")))
					{
				%>
					<A href=#  onClick='return changedate("<%=datelink%>",<%=pageRight%>,<%=orgRight%>)'><img border="0" src="./images/edit.gif" alt="<%=LC.L_Change_ActualDate%><%--Change Actual Date*****--%>" title="<%=LC.L_Change_ActualDate%><%--Change Actual Date*****--%>"/></A>
				<%
					}
				%>
			</td>
    		</tr>
    	</table>
	<%
		}
	%>
			<% if(acDateFlag.equals("1"))
				{
				   fuzzy_dates = "-";
				} %>

			<td><%=fuzzy_dates%></td>

			<%
			     EventuserDao eventUserDao = new EventuserDao();
			     String eventRoles = eventUserDao.getEventRolesResources(Integer.parseInt(org_id));

			     if(eventRoles == null){
			        eventRoles = "";
			     }
		//Prabhat: Fixed Bug  #4847
		nameTrim = name;
		if (nameTrim.length()>50){
			nameTrim = nameTrim.substring(0,50)+"...";
		}
		%>


		<td><A href=# onmouseover="return overlib('<%=name%>',CAPTION,'<%=LC.L_Event_Name%><%--Event Name*****--%>');" onmouseout="return nd();" onClick='return viewevent("viewevent.jsp?eventId=<%=org_id%>&calledFrom=S")'><%=nameTrim%></A>
		<%if (eventNotes.length()>0){%>
		<img src="../images/jpg/icon_clip.gif" title="<%=LC.L_ThisEventHasNote%>">
		<%}%>
		<br>
		<%if (eventRoles.length()>0){%>
		<%=LC.L_Resources%><%--Resources*****--%>:
			  <br>
			  <%=eventRoles%>
		<%}%>
		</td>
	<%



%>
	<td>
	<table width="100%" cellpadding="0" cellspacing="0" align="center" border="0">
	<!--  Modified By Parminder Singh Bug #10827 -->
<%	if ((pastDate) && (eptRight==1)&& (subType.equals("ev_notdone")))//if current date is past act date
		{%>
			<TR class="pastSchRow">
<%
		} else
			{
%>
			<TR>
<%
			}
%>
	<td width="42%" style="border: 0px">
	<A href=# onClick='return fopen("<%=link%>","<%=pageRight%>","<%=orgRight%>")'><%=str%> <%=sts%></A>

	<%        	name=name.replace(' ','~');

		//KM-15Sep09-#4267
		cLink = "MarkDone.jsp?event_id=" +event_id + "&mode=N&fk_eventId=" + org_id +"&protocol_id=" +prot_id +"&pkey=" +pkey + "&statDesc="+statDesc+ "&statid="+sts+"&calledFrom=S&calAssoc=S";

	%>
	</td>
	<td width="12%" style="border: 0px">
	<A href=# onClick='return fopen("<%=cLink%>","<%=pageRight%>","<%=orgRight%>")'><img border="0" src="./images/edit.gif" alt="<%=LC.L_Edit%>" title="<%=LC.L_Edit%>"/></A>
	</td>

		<!--KM-->
		<td width="12%" style="border: 0px">
		<A href="eventstatushistory.jsp?srcmenu=<%=src%>&pkey=<%=personPK%>&visit=<%=newVisit%>&visitName=<%=StringUtil.encodeString(newVisitName)%>&eventId=<%=event_id%>&studyNum=<%=StringUtil.encodeString(studyNumber)%>&calassoc=S&studyId=<%=study%>&calId=<%=protocolId%>&nextpage=<%=pagenum%>&calledFromPage=studyadmincal&org_id=<%=org_id%>&calledFrom=S"><img border="0" src="./images/History.gif" alt="<%=LC.L_Evt_StatusHistory%><%--Event Status History*****--%>" title = "<%=LC.L_Evt_StatusHistory%><%--Event Status History*****--%>"/></A>
		</td><td width="34%" style="border: 0px">
		
		<%              	name=name.replace('~',' '); %>
                <%=tmpExeOn%></td>
    </tr>
    </table>
	</td>

<%


	if (String.valueOf(formLibAppRight).compareTo("1") == 0){%>
<!--		<TD>&nbsp;</TD> -->

	<%}
		if(eptRight==1)
		{
	%>
	<!-- IA 10.16.2006
	Bug 2674, Hide CRF screen from the Admin Schedule
	-->

				<TD  style="display:none"><%=crfString%></TD>
	<%
		}
	%>

		     </TR>
		<% j++;
			prevMonth = thisMonth;
			prev_dt = start_dt;
			prevYr = year1;
			prevVisit = newVisit;
	} //for


		%>
</TABLE>




			<% if (totalRows > 0)
			{ Object[] arguments = {firstRec,lastRec,totalRows};
			%>
			<table>
			<tr><td>
						<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%-- <font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%></td></tr></table>
					<%}
					else {
					if   (       ! generate.equals("N")           )
	                  {  %>

					<font class="recNumber lhsFont"><%=MC.M_NoRecordsFound%><%--No Records Found*****--%></font></td></tr></table>
				<%	} //end of if of generate == N

		}

			if (curPage==1) startPage=1;

			%> <table align=center border="0" >
			<tr> <%


	    	for (int count = 1; count <= showPages;count++)
			{	cntr = (startPage - 1) + count;
	 			if ((count == 1) && (hasPrevious))
				{


				  %>
					<td colspan = 2>
				  	<A href="#" onclick="openWinPage( <%=selectedTab%>,'<%=src%>',<%=cntr-1%>,'<%=orderBy%>','<%=orderType%>', '<%=page1%>', <%=study%>, document.sch)" >< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>
					&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
					<%
  					}	%>
				<td>
				<%
 				 if (curPage  == cntr)
				 {
	    			 %>
					<FONT class = "pageNumber"><%= cntr %></Font>
		       		<%
       			}else
		        {	%>
        		 <A href="#" onclick="openWinPage( <%=selectedTab%>,'<%=src%>',<%=cntr%>,'<%=orderBy%>','<%=orderType%>','<%=page1%>',<%=study%>,document.sch)" ><%= cntr%></A>


			       <%}%>
				</td>
				<%	}
				if (hasMore)
				{  %>
				   <td colspan = 3 align = center>&nbsp;&nbsp;&nbsp;&nbsp;
				  <A href="#" onclick="openWinPage( <%=selectedTab%>,'<%=src%>',<%=cntr+1%>,'<%=orderBy%>','<%=orderType%>', '<%=page1%>', <%=study%>, document.sch)" >< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%> ></A>
				</td></tr></table>

				<%	}	%>



</Form>
<%
	 }//end for if protocolId
	 }else{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%
}
} //end of if session times out
else
{
%>
 <jsp:include page="timeout.html" flush="true"/>
  <%
} //end of else body for page right
%>
  <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</body>
</html>
