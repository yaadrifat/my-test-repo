<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!-- saved from url=(0022)http://internet.e-mail -->
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML><HEAD><TITLE><%=LC.L_Calculate_Toxicity%><%--Calculate Toxicity*****--%></TITLE>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</HEAD>

<script>

function validate(formobj) {

	//sv, 8/5/04, part of fix for bug#1344, added to get the site name before calling calculategrade.jsp
 	formobj.labName.value = (formobj.labSite.options.length>0)?formobj.labSite.options[formobj.labSite.selectedIndex].text:""; 
	 if (!(validate_col('Toxicity',formobj.dToxicityGroup))) return false
	 if (!(validate_col('Lab Result',formobj.labResult))) return false
 	 //if (!(validate_col('Lab Facility',formobj.labSite))) return false
	 	 
     
	return true;
}
 


</script>

<BODY>
<%-- INF-20084 Datepicker-- AGodara --%>
<jsp:include page="include.jsp" flush="true"/> 
<jsp:include page="ui-include.jsp" flush="true"></jsp:include>
<jsp:include page="jqueryUtils.jsp"></jsp:include>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>
<jsp:useBean id="advB" scope="request" class="com.velos.esch.web.advEve.AdvEveJB"/>
<jsp:useBean id="siteB" scope="page" class="com.velos.eres.web.site.SiteJB" />

<%
	HttpSession tSession = request.getSession(true); 
	 if (sessionmaint.isValidSession(tSession))
	{
	 %>
	<jsp:include page="sessionlogging.jsp" flush="true"/> 

<% 	
	 
	 SiteDao usd = new SiteDao();	 
 	 String ddSite = "";	 
	 
   	 String accountId = (String) tSession.getValue("accountId");

	 usd = siteB.getByAccountId(EJBUtil.stringToNum(accountId), "lab");

 	 ArrayList arSiteid = new ArrayList();
	 ArrayList arSiteName = new ArrayList();
	 
	 arSiteid = usd.getSiteIds();
	 arSiteName = usd.getSiteNames();


   	 	 
	 ddSite = EJBUtil.createPullDown("labSite",0,arSiteid,arSiteName);

	String nciVersion = request.getParameter("nciVersion");
	String patient = request.getParameter("pkey");

	String parentform = request.getParameter("parentform");
	String gradefld = request.getParameter("gradefld");
	String advnamefld =	request.getParameter("advnamefld");
	String dispfield =	request.getParameter("dispfield");

	
	NCILibDao nciLDao = advB.getNCILib(nciVersion);
	
	ArrayList toxicityName = nciLDao.getToxicityName(); 
	ArrayList toxicityGroup = nciLDao.getToxicityGroup(); 
	
	

%>
<Form class=formDefault name="calcGrade" action="calculateGrade.jsp" method="post" onSubmit="return validate(document.calcGrade)" >

<% 
	String userIdFromSession = (String) tSession.getValue("userId");
    String ddToxicity = "";
    ddToxicity = EJBUtil.createPullDown("dToxicityGroup",0,toxicityGroup,toxicityName);
    
    CodeDao cdLabs = new CodeDao();
    cdLabs.getCodeValues("labunit"); 
    
    ArrayList cDesc = cdLabs.getCDesc();
    // get pull down for labunits
    	StringBuffer unitStr = new StringBuffer();
	    unitStr.append("<SELECT NAME='labUnit'>") ;
	    unitStr.append("<OPTION value='' SELECTED> "+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");
		try	{
			int counter = 0;
			for (counter = 0; counter <= cDesc.size() -1 ; counter++){
				unitStr.append("<OPTION value = "+ cDesc.get(counter)+">" + cDesc.get(counter)+ "</OPTION>");
			}
		
		unitStr.append("</SELECT>");
	}catch (Exception e){
		e.printStackTrace();
		unitStr.append("</SELECT>");
	}
    
    	
    ////////////
%>
	<BR>
	<table >
	<tr>
	<td class=tdDefault>
		<%=LC.L_Toxicity_Name%><%--Toxicity Name*****--%> <font class=mandatory>*</font>
	</td>
	<td>
		<%= ddToxicity%>
	</td>
	</tr>
	<tr>
		<td class=tdDefault><%=LC.L_Lab_Date%><%--Lab Date*****--%> </td>
<%-- INF-20084 Datepicker-- AGodara --%>		
		<td class=tdDefault><INPUT type=text name=labDate class="datefield" READONLY size=10 value= ></td>
	</tr> 

	<tr>
	<td class=tdDefault><%=LC.L_Result_Units%><%--Result/Units*****--%><font class=mandatory>*</font></td>
	<td><INPUT type=text name=labResult size=10 maxLength = 10 > &nbsp;&nbsp;&nbsp;&nbsp;<%=unitStr.toString()%></td>
	
	</tr>	
	
	<tr>
	<td class=tdDefault><%=LC.L_Lab_Facility%><%--Lab Facility*****--%></td>
	<td><%=ddSite%></td>
	<td>
	<!-- SV, 8/5/04, added labName to be passed into calculateGrade for displaying lab name-->
	<INPUT type=hidden name=labName></td>
	</tr>

<tr>
	<td class=tdDefault>( <%=MC.M_IfOther_EtrLln%><%--If Other, Enter LLN*****--%> </td>
	<td><INPUT type=text name=otherLLN size=10 maxLength = 10 ></td>
	</tr>
		
	<tr>
	<td class=tdDefault>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_AndOr_Uln%><%--and/or ULN*****--%> </td>
	<td><INPUT type=text name=otherULN size=10 maxLength = 10 > &nbsp;)</td>
	</tr>
	
	<!--<tr>
	   <td class=tdDefault>
		e-Signature <FONT class="Mandatory">* </FONT>
	   </td >
	  <td class=tdDefault>
		<input type="password" name="eSign" maxlength="8">
	  </td>
	</tr> -->
	    
	   </table>		
	<br>
	<table width="300" cellspacing="0" cellpadding="0">
	<td align=right>
	<tr><td colspan=2 align=right><button type="submit"><%=LC.L_Calculate%></button></td>	</tr>
	</table>
	<INPUT type=hidden name= nciVersion value = <%=nciVersion%>>		
	<INPUT type=hidden name= patient value = <%=patient%>>		

   	<INPUT type=hidden name= parentform value = <%=parentform%>>		
   	<INPUT type=hidden name= gradefld value = <%=gradefld%>>		
   	<INPUT type=hidden name= dispfield value = <%=dispfield%>>				   
   	<INPUT type=hidden name= advnamefld value = <%=advnamefld%>>				   
		   
	
	</form>
<%}else{%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/> 
<%}%>
<%-- Ankit: Bug#10497 Date:10-July-2012 --%>
<jsp:include page="bottompanel.jsp" flush="true"/>
</BODY>
</HTML>


