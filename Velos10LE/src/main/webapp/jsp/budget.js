var SaveBgtFst_BefCalAttch=M_SaveBgtFst_BefCalAttch;
var AttachStd_BeforeSelPcol=M_AttachStd_BeforeSelPcol;
var EtrVld_FringeBenftVal=M_EtrVld_FringeBenftVal;
var Vldt_CostDiscount=M_Vldt_CostDiscount;
var EtrValid_IndirectsValue=M_EtrValid_IndirectsValue;
var IntegerPartCnt_Exceed=M_IntegerPartCnt_Exceed;
var EtrValidUnits=M_EtrValidUnits;
var Vldt_SponsorAmt=M_Vldt_SponsorAmt;
var Vldt_NumOfPatValue=M_Vldt_NumOfPatValue;
var Freeze_CntModified=M_Freeze_CntModified;
var Save_BgtFirst=M_Save_BgtFirst;
var FuncNtAval_PlsSelIndCal=M_FuncNtAval_PlsSelIndCal;
var PlsEtrCost_PatVal=M_PlsEtrCost_PatVal;  

function checkIfSubmit(formobj)
{
	
	
	if (validate(formobj)==false) 
	{
		setValidateFlag('false');
		
		return false;
	} else 
	{
		showMsg()
		setValidateFlag('true');
	//	document.getElementById("submit_btn").click();
	
		return true;
	}
	
}

function showMsg()
{
 /*toggleNotification('on','Budget Saving progress. Please wait...');*****/
   toggleNotification('on',M_BgtSvgProg_PlsWait);
 return true;	
}

function openLibprot(formobj, pgRight,fringeBenefit,fringeBenefitApply,costDiscount,costDiscountApply,sponsorOHead,sponsorOHeadApply)
{
    if (f_check_perm(pgRight,'E') == false)
    {
        return false;
    }
    if(formobj.mode.value=='M')
    {
        var budgetId=document.getElementById("budgetId").value;   	
        
        windowName=window.open("bgtprotocollist.jsp?" + "budgetId=" + budgetId + "&protType=L" +"&fringeBenefit="+fringeBenefit+"&fringeBenefitApply="+fringeBenefitApply+"&costDiscount="+costDiscount+"&costDiscountApply="+costDiscountApply+"&sponsorOHead="+sponsorOHead+"&sponsorOHeadApply="+sponsorOHeadApply ,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400");
        windowName.focus();
    }
    else
    {
       /* alert("Please save the budget first before attaching the calendar");*****/
          alert(SaveBgtFst_BefCalAttch);
		return false;
    }
}
function openEditPersonnelCostWin(formobj,mode,bgtcalId,bgtStatCode,budgetTemplate,from)
{
	if(bgtcalId==0){
	   /*alert("This functionality is not available for selection -'All'.\nPlease select individual calendar.");*****/
	    alert(FuncNtAval_PlsSelIndCal);
	}else{
		var budgetId=document.getElementById("budgetId").value;
		var pageRight=document.getElementById("pageRight").value;
		
	    param1="editPersonnelCost.jsp?mode="+mode+"&bgtStatCode="+ bgtStatCode+ "&budgetId="+budgetId+"&bgtcalId="+bgtcalId+"&pageMode=first&budgetTemplate="+budgetTemplate;
	    if (from=="combBgt") {
			param1 = param1 +"&fromRt="+pageRight;
		}
	    
	    windowName=window.open(param1,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=1000,height=500,top=20 ,left=20");
	    windowName.focus();
	}
}
function openSections(pageRight,includedIn,src,budgetId,budgetTemplate,selectedTab,
		bgtcalId,mode,bgtStat,from)
{
	if(bgtcalId==0){
		/*alert("This functionality is not available for selection -'All'.\nPlease select individual calendar.");*****/
		alert(FuncNtAval_PlsSelIndCal);
	}else{
		if (f_check_perm(pageRight,'V') == false) { return false; }
		param1 = "budgetsections.jsp?includedIn="+includedIn+"&srcmenu="+src+"&budgetId="+budgetId+
		  "&budgetTemplate="+budgetTemplate+"&selectedTab="+selectedTab+"&calId="+bgtcalId+
		  "&mode="+mode+"&bgtStat="+bgtStat;
		
		if (from=="combBgt") {
			param1 = param1 +"&fromRt="+pageRight;
		}
		
	    windowName=window.open(param1,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=820,height=500,top=150 ,left=250");
	    windowName.focus();
	}
}
function openAppendix(pageRight,includedIn,src,budgetId,budgetTemplate,selectedTab,mode,studyId,from)
{
	if (f_check_perm(pageRight,'V') == false) { return false; }
	param1 = "budgetapndx.jsp?includedIn="+includedIn+"&srcmenu="+src+"&budgetId="+budgetId+"&mode="+mode+
	  "&budgetTemplate="+budgetTemplate+"&selectedTab="+selectedTab+"&studyId="+studyId;
	if (from=="combBgt") {
		param1 = param1 +"&fromRt="+pageRight;
	}
    windowName=window.open(param1,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=820,height=500,top=150 ,left=250");
    windowName.focus();
}
function openAccessRights(pageRight,includedIn,src,budgetId,budgetTemplate,selectedTab,mode,studyId)
{
	if (f_check_perm(pageRight,'V') == false) { return false; }
	param1 = "budgetrights.jsp?includedIn="+includedIn+"&srcmenu="+src+"&budgetId="+budgetId+"&mode="+mode+
	  "&budgetTemplate="+budgetTemplate+"&selectedTab="+selectedTab+"&studyId="+studyId;
    windowName=window.open(param1,"Information1","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=820,height=500,top=150 ,left=250");
    windowName.focus();
}
function openDelete(bgtcalId,pageRight,from)
{
	if(bgtcalId==0){
		/*alert("This functionality is not available for selection -'All'.\nPlease select individual calendar.");*****/
		alert(FuncNtAval_PlsSelIndCal);
	}else{
	    if (f_check_perm(pageRight,'E') == false)
	    {
	        return false;
	    }
	    else
	    {
	        param1="deleteMultipleLineitems.jsp?bgtcalId="+bgtcalId+"&pageMode=first&from="+from;
	        if (from=="combBgt") {
	    		param1 = param1 +"&fromRt="+pageRight;
	    	}
	
	        windowName=window.open(param1,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=500,top=150 ,left=250");
	        windowName.focus();
	    }
	}
}

function openBudgetCalAttributes(bgtcalId,budgetPK,pageRight,bgtStatCode,repType)
{
    if (f_check_perm(pageRight,'E') == false)
    {
        return false;
    }
    else
    {
    	
    	if (bgtStatCode == 'Template' || bgtStatCode == 'Freeze' )
    	{
    		// #5747 02/10/2011 @Ankit
    		var budgetStatusDesc = "";
			try{
				budgetStatusDesc = document.getElementById("budgetStatusDesc").value;
			}
			catch(err){
				budgetStatusDesc = bgtStatCode;
			}
			 var paramArray = [budgetStatusDesc];
    		  alert(getLocalizedMessageString("M_BgtStat_CntChgBgt", paramArray));/*alert("Budget Status is '"+budgetStatusDesc +"'. You cannot make any changes to the budget");*****/
    		return false;
    	}
        param1="editBudgetCalAttributes.jsp?bgtcalId="+bgtcalId+"&pageRight="+ pageRight+"&budgetId="+budgetPK+"&repType="+repType;
        windowName=window.open(param1,"BudgetCal","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,top=150 ,left=250");
        windowName.focus();
    }
}

	
	
function openRepeatLineItems(formobj,mode,bgtcalId,bgtStatCode, budgetTemplate,from)
{
	if(bgtcalId==0){
	/*alert("This functionality is not available for selection -'All'.\nPlease select individual calendar.");*****/
		alert(FuncNtAval_PlsSelIndCal);
	}else{
		var budgetId=document.getElementById("budgetId").value;
		var pageRight=document.getElementById("pageRight").value;
	    
		param1="editRepeatLineItems.jsp?mode="+mode+"&bgtStatCode="+ bgtStatCode+ "&budgetId="+budgetId+"&bgtcalId="+bgtcalId+"&pageMode=first&budgetTemplate="+budgetTemplate;
	    if (from=="combBgt") {
			param1 = param1 +"&fromRt="+pageRight;
		}
	    
	    windowName=window.open(param1,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=1000,height=500,top=20 ,left=10");
	    windowName.focus();
	}
}

function openBudget(budgetId,includedIn,from)
{
   	var param1;
   	var pageRight=document.getElementById("pageRight").value;
   	
    param1="budget.jsp?mode=M&includedIn="+includedIn+"&budgetId="+budgetId+"&from="+from;
    if (from=="study") {
		param1 = param1 +"&fromRt="+pageRight;
	}
    windowName=window.open(param1,"BudgetDetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=1000,height=500,top=20 ,left=10");
    windowName.focus();
}

function openLineItem(bgtcalId,budgetId,bgtSectionId,bgtSectionName,bgtStat,lineitemmode,pageright,itemId,lineitemName,lineitemInPerSec,allowSubmit,templateType)
{
	var param1;
	var checkPermission;
	
	if (lineitemmode=="N")
	{
		checkPermission="N";
		
		if (bgtStat == 'Template' || bgtStat == 'Freeze' )
    	{
    		// #5747 02/10/2011 @Ankit
    		var budgetStatusDesc = "";
			try{
				budgetStatusDesc = document.getElementById("budgetStatusDesc").value;
			}
			catch(err){
				budgetStatusDesc = bgtStat;
			}
			var paramArray = [budgetStatusDesc];
  		    alert(getLocalizedMessageString("M_BgtStat_CntChgBgt", paramArray));/*alert("Budget Status is '"+budgetStatusDesc +"'. You cannot make any changes to the budget");*****/
    		return false;
    	}	
    	
	}else
		{
			checkPermission="E";
		}

	if (! f_check_perm(pageright,checkPermission))
	{
		return false;
	}
	
	 	
	param1= "additempatient.jsp?bgtcalId="+bgtcalId+"&budgetId="+budgetId+"&bgtSectionId="+bgtSectionId+"&bgtSectionName="+bgtSectionName+
		"&bgtStatCode="+bgtStat+"&lineitemMode="+lineitemmode+"&templateType="+templateType;
	
	if (lineitemmode=="M")
	{
		param1= param1+"&lineitemId="+itemId+"&lineitemName="+lineitemName+"&lineitemInPerSec="+lineitemInPerSec+"&allowSubmit="+allowSubmit;
	}
	windowName=window.open(param1,"Lineitem","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=1000,height=500,top=20 ,left=10");
    windowName.focus();
}

	
function openStudyprot(formobj, pgRight)
{
    if (f_check_perm(pgRight,'E') == false)
    {
        return false;
    }
    if(formobj.mode.value=='M')
    {
    	var budgetId=document.getElementById("budgetId").value;
        var studyId=document.getElementById("budgetStudyId").value;
        
        if(studyId=="null")
        {
            /*alert("Please attach a study before selecting protocol");*****/
			alert(AttachStd_BeforeSelPcol);
            return;
        }
        windowName=window.open("bgtprotocollist.jsp?" + "budgetId=" + budgetId + "&protType=S&studyId=" +studyId,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400");
        windowName.focus();
    }
    else
    {
        /*alert("Please save the budget first before attaching the calendar");*****/
		alert(SaveBgtFst_BefCalAttch);
        return false;
    }
}
function openBgtProt(formobj, pgRight)
{
    if (f_check_perm(pgRight,'E') == false)
    {
        return false;
    }
    var budgetId=document.getElementById("budgetId").value;
    var budgetStatus=document.getElementById("budgetStatus").value;
    windowName=window.open("editbgtprotocols.jsp?" + "budgetId=" + budgetId + "&budgetStatus=" + budgetStatus ,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400");
    windowName.focus();
}
function confirmBox(lineitemName,pgRight)
{
    if (f_check_perm(pgRight,'E') == true)
    {
          var paramArray = [decodeString(lineitemName)];
		  if (confirm(getLocalizedMessageString("L_Del_FrmBgt", paramArray))) /*  if (confirm("Delete " + decodeString(lineitemName) + " from Budget?"))*****/
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}
function validate(formobj)
{
	
	 
    if(calculate(formobj) == false)
    {
        return false;
    }
    
/*    
    if (!(validate_col('e-Signature',formobj.eSign))) return false;
    if(isNaN(formobj.eSign.value) == true)
    {
        alert("Incorrect e-Signature. Please enter again");
        formobj.eSign.focus();
        return false;
    }
*/
}



function openPrint(formobj, type)
{
	var budgetId=document.getElementById("budgetId").value;
	var bgtcalId=document.getElementById("bgtcalId").value;
    windowName=window.open("patientbudgetprint.jsp?&budgetId="+budgetId+"&bgtcalId="+bgtcalId+"&type="+type,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=yes,status=yes,width=600,height=460");
    windowName.focus();
}
function maxDigits(value)
{
    cnt = 0;
    for(i=0;i<value.length;i++)
    {
        if(value.charAt(i)=='.')
        {
            if(cnt <= 8)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            cnt = cnt+1;
        }
        if(cnt > 8)
        return false;
    }
}
function calculate(formobj)
{
	
	
    if(isNaN(formobj.fringeBenefit.value) == true)
    {
        /*alert("Please enter a valid Fringe Benefit value.");*****/
        alert(EtrVld_FringeBenftVal);
		formobj.fringeBenefit.focus();
        return false;
    }
    if(isNaN(formobj.costDiscount.value) == true)
    {
          /*alert("Please enter a valid Cost Discount value.");*****/
		  alert(Vldt_CostDiscount);
        formobj.costDiscount.focus();
        return false;
    }
    if(isNaN(formobj.sponsorOHead.value) == true)
    {
        /*alert("Please enter a valid Indirects value.");*****/
        alert(EtrValid_IndirectsValue);
		formobj.sponsorOHead.focus();
        return false;
    }
    var perCodeId = formobj.perCodeId.value;
    var excludeSOCApplyChecked=formobj.excludeSOCApply.checked;
    var budgetTemplate = formobj.budgetTemplate.value;
    var subTotalResearchCost = 0;
    var subTotalTtlCost = 0;
    var socSubTotalResearchCost = 0;
    var socSubTotalTtlCost = 0;
    var socSubTotalAppIndResCost = 0;
    var socSubTotalAppIndTtlCost = 0;
    var socSubTotalResIndNotApplied = 0;
    var socSubTotalTtlIndNotApplied = 0;
    var nsocSubTotalResearchCost = 0;
    var nsocSubTotalTtlCost = 0;
    var nsocSubTotalAppIndResCost = 0;
    var nsocSubTotalAppIndTtlCost = 0;
    var nsocSubTotalResIndNotApplied = 0;
    var nsocSubTotalTtlIndNotApplied = 0;
    var subTotalAppIndResCost = 0;
    var subTotalAppIndTtlCost = 0;
    var subTotalResIndNotApplied = 0;
    var subTotalTtlIndNotApplied = 0;
    var grandTotat = 0;
    var i = 0;
    var prevSectionRowsIndex = 0;
    var sectionRowsIndex = 0;
    var bgtSectionType;
    var	socSubTotalPatResearchCost = 0;
    var socSubTotalPatTtlCost = 0;
    var socSubTotalPatAppIndResCost = 0;
    var socSubTotalPatAppIndTtlCost = 0;
    var socSubTotalPatResIndNotApplied = 0;
    var socSubTotalPatTtlIndNotApplied = 0;
    var socTotalResearchCost = 0;
    var socTotalTtlCost = 0;
    var socTotalAppIndResCost= 0;
    var socTotalAppIndTtlCost= 0;
    var socTotalResIndNotApplied = 0;
    var socTotalTtlIndNotApplied = 0;
    var	nsocSubTotalPatResearchCost = 0;
    var nsocSubTotalPatTtlCost = 0;
    var nsocSubTotalPatAppIndResCost = 0;
    var nsocSubTotalPatAppIndTtlCost = 0;
    var nsocSubTotalPatResIndNotApplied = 0;
    var nsocSubTotalPatTtlIndNotApplied = 0;
    var nsocTotalResearchCost = 0;
    var nsocTotalTtlCost = 0;
    var nsocTotalAppIndResCost= 0;
    var nsocTotalAppIndTtlCost= 0;
    var nsocTotalResIndNotApplied = 0;
    var nsocTotalTtlIndNotApplied = 0;
    var	subTotalPatResearchCost = 0;
    var subTotalPatTtlCost = 0;
    var subTotalPatAppIndResCost = 0;
    var subTotalPatAppIndTtlCost = 0;
    var subTotalPatResIndNotApplied = 0;
    var subTotalPatTtlIndNotApplied = 0;
    var socTotalResearchCost = 0;
    var socTotalTtlCost = 0;
    var socTotalAppIndResCost= 0;
    var socTotalAppIndTtlCost= 0;
    var socTotalResIndNotApplied = 0;
    var socTotalTtlIndNotApplied = 0;
    var nsocTotalResearchCost = 0;
    var nsocTotalTtlCost = 0;
    var nsocTotalAppIndResCost= 0;
    var nsocTotalAppIndTtlCost= 0;
    var nsocTotalResIndNotApplied = 0;
    var nsocTotalTtlIndNotApplied = 0;
    var totalResearchCost = 0;
    var totalTtlCost = 0;
    var totalAppIndResCost= 0;
    var totalAppIndTtlCost= 0;
    var totalResIndNotApplied = 0;
    var totalTtlIndNotApplied = 0;
    var fringeResTotal = 0;
    var fringeTtlTotal = 0;
    var ttlCost = 0;
    var fringeBenefit = 0;
    var researchCost = 0;
    var ttlCost = 0;
    //Added by IA 11.03.2006
    var sponsorAmountM = 0;
    var	socSubTotalSponsorAmount = 0;
    var socSubTotalVariance = 0;
    var socTotalSponsorAmount = 0;
    var socTotalVariance = 0;
    var	nsocSubTotalSponsorAmount = 0;
    var nsocSubTotalVariance = 0;
    var nsocTotalVariance = 0;
    var nsocTotalSponsorAmount = 0;
    var totalSponosrAmount = 0;
    var totalVariance = 0;
    //end Added
    var researchFringeBenefit = 0;
    var ttlFringeBenefit = 0;
    var researchDiscount = 0;
    var ttlDiscount = 0;
    var salResTotal = "0.00";
    var salTtlTotal ="0.00" ;
    var discResTotal = 0;
    var	discTtlTotal = 0;
    var indResTotal = 0;
    var indTtlTotal = 0;
    var resInd = 0 ;
    var ttlInd = 0;
    var patNo;
    //added by IA 11.03.2006
    var bgtSectionTypePCntTotal = formobj.bgtSectionTypePCntTotal.value; 
    var bgtSectionTypeOCntTotal = formobj.bgtSectionTypeOCntTotal.value;
    sponsorOHead = formobj.sponsorOHead.value;
    sponsorOHeadApply = formobj.sponsorOHeadApply.checked;
    perPatientSectionCnt = formobj.perPatientSectionCnt.value; //checked; //document.getElementsByName("perPatientSectionCnt").value;
    oneSectionCnt = formobj.oneSectionCnt.value; // document.getElementsByName("oneSectionCnt").value;
    fringeBenefit = formobj.fringeBenefit.value;
    costDiscount = formobj.costDiscount.value;
    costDiscountApply =	formobj.costDiscountApply.checked;
    sponsorOHead = formobj.sponsorOHead.value;
    sponsorOHeadApply = formobj.sponsorOHeadApply.checked;
    
    //If there is only one lineitem
    if(formobj.rows.value == 1)
    {
        if(isNaN(formobj.unitCost.value) == true)
        {
            /*alert("Please enter a valid Unit Cost.");****/
			  alert(EtrValid_UnitCost);
			
            formobj.unitCost.focus();
            return false;
        }
        if(maxDigits(formobj.unitCost.value)==false)
        {
            /*alert("Integer part cannot exceed more than 8 digits.");*****/
			alert(IntegerPartCnt_Exceed);
            formobj.unitCost.focus();
            return false;
        }
        if(isNaN(formobj.noUnits.value) == true)
        {
            /*alert("Please enter valid # of Units.");*****/
			alert(EtrValidUnits);
            formobj.noUnits.focus();
            return false;
        }
        if(maxDigits(formobj.noUnits.value)==false)
        {
            /*alert("Integer part cannot exceed more than 8 digits.");*****/
			alert(IntegerPartCnt_Exceed);
            formobj.noUnits.focus();
            return false;
        }
        if(isNaN(formobj.researchCost.value) == true)
        {
           /* alert("Please enter a valid Cost/Patient value.");*****/
			 alert(PlsEtrCost_PatVal);
            formobj.researchCost.focus();
            return false;
        }

        //Added by IA 11.03.2006
        if ( budgetTemplate == "C")
        {
            if(isNaN(formobj.sponsorAmount.value) == true)
            {
                /*alert("Please enter a valid Sponsor Amount value.");*****/
				alert(Vldt_SponsorAmt);
                formobj.sponsorAmount.focus();
                return false;
            }
        }
        //End Added
        
        
        if(formobj.unitCost.value=="0.00" || formobj.noUnits.value=="0.00" || formobj.unitCost.value=="" || formobj.noUnits.value=="")
        {
            if(maxDigits(formobj.researchCost.value)==false)
            {
               /* alert("Integer part cannot exceed more than 8 digits.");*****/
				alert(IntegerPartCnt_Exceed);
                formobj.researchCost.focus();
                return false;
            }
        }
        if (formobj.discount.checked)
        {
            formobj.hidDiscountChkbox.value = "1";
        }
        else
        {
            formobj.hidDiscountChkbox.value = "0";
        }
        if (formobj.stdCareCost.checked)
        {
            if(formobj.unitCost.value != "")
            formobj.hidStdCareCost.value = formobj.unitCost.value;
            else
            formobj.hidStdCareCost.value = "0.00"
        }
        else
        {
            formobj.hidStdCareCost.value = "";
        }
        formobj.unitCost.value = (Math.round(formobj.unitCost.value *100)/100).toFixed(2);
        formobj.noUnits.value = (Math.round(formobj.noUnits.value *100)/100).toFixed(2);
        formobj.researchCost.value = (Math.round(formobj.researchCost.value *100)/100).toFixed(2);
        //Added by IA 11.03.2006
        if ( budgetTemplate == "C")
		{
        	formobj.sponsorAmount.value = (Math.round(formobj.sponsorAmount.value *100)/100).toFixed(2);
        }
        
        
        //end added
        var bgtSectionType = formobj.bgtSectionTypeRow.value;
        var secType = formobj.secType.value;
        if(secType == "P")
        {
            formobj.patNo.value = (Math.round(formobj.patNo.value *100)/100).toFixed(2);
            formobj.TtlCost.value = (parseFloat(formobj.patNo.value) * parseFloat(formobj.researchCost.value)).toFixed(2);
        }
        bgtSectionType = formobj.bgtSectionTypeRow.value;
        if(bgtSectionType == "P")
        {
            formobj.researchCost.value = (parseFloat(formobj.unitCost.value) * parseFloat(formobj.noUnits.value)).toFixed(2);
        }
        if(bgtSectionType == "O")
        {
            formobj.TtlCost.value = (parseFloat(formobj.unitCost.value) * parseFloat(formobj.noUnits.value)).toFixed(2);
        }
        if(formobj.cmbCtgry.value==perCodeId)
        {
            if ((formobj.stdCareCost.checked && !excludeSOCApplyChecked == true) || (!formobj.stdCareCost.checked))
            {
                salResTotal = (parseFloat(formobj.researchCost.value)).toFixed(2);
            }
        }
        if ((formobj.fringeBenefitApply.checked == true) && (formobj.cmbCtgry.value==perCodeId) )
        {
            if ((formobj.stdCareCost.checked && !excludeSOCApplyChecked == true) || (!formobj.stdCareCost.checked))
            {
                researchFringeBenefit = ((parseFloat(formobj.researchCost.value) * parseFloat(fringeBenefit)) / 100.0);
                ttlFringeBenefit = ((parseFloat(formobj.TtlCost.value) * parseFloat(fringeBenefit)) / 100.0);
            }
            formobj.totalFringeResCost.value = (parseFloat(researchFringeBenefit)).toFixed(2);
            formobj.totalFringeTtlCost.value = (parseFloat(ttlFringeBenefit)).toFixed(2);
            fringeResTotal = fringeResTotal + researchFringeBenefit;
            fringeTtlTotal = fringeTtlTotal + ttlFringeBenefit;
        }
        if ((costDiscountApply==true) && (!isNaN(costDiscount)) && (formobj.discount.checked == true))
        {
            if ((formobj.stdCareCost.checked && !excludeSOCApplyChecked == true) || (!formobj.stdCareCost.checked))
            {
                researchDiscount = (((parseFloat(formobj.researchCost.value) + researchFringeBenefit) * parseFloat(costDiscount)) / 100.0);
                ttlDiscount = (((parseFloat(formobj.TtlCost.value) + ttlFringeBenefit) * parseFloat(costDiscount)) / 100.0);
            }
            discResTotal = discResTotal + researchDiscount;
            discTtlTotal = discTtlTotal + ttlDiscount;
        }
        formobj.researchCost.value = (Math.round(formobj.researchCost.value *100)/100).toFixed(2);
        formobj.TtlCost.value = (Math.round(formobj.TtlCost.value *100)/100).toFixed(2);
        subTotalResearchCost = parseFloat(formobj.researchCost.value) + researchFringeBenefit - researchDiscount;
        if(formobj.appIndirects.checked == true && formobj.sponsorOHeadApply.checked == true)
        {
            subTotalResearchCost = subTotalResearchCost + ((parseFloat(subTotalResearchCost) * parseFloat(sponsorOHead)) / 100.0);
        }
        if (formobj.stdCareCost.checked)
        {
            if(!excludeSOCApplyChecked == true)
            {
                socSubTotalResearchCost = parseFloat(formobj.researchCost.value) + researchFringeBenefit - researchDiscount;
                if(formobj.appIndirects.checked == true && formobj.sponsorOHeadApply.checked == true)
                {
                    resInd = ((parseFloat(socSubTotalResearchCost) * parseFloat(sponsorOHead)) / 100.0);
                    socSubTotalResearchCost = socSubTotalResearchCost + ((parseFloat(socSubTotalResearchCost) * parseFloat(sponsorOHead)) / 100.0);
                }
            }
        }
        else
        {
            nsocSubTotalResearchCost = parseFloat(formobj.researchCost.value) + researchFringeBenefit - researchDiscount;
            if(formobj.appIndirects.checked == true && formobj.sponsorOHeadApply.checked == true)
            {
                resInd = ((parseFloat(nsocSubTotalResearchCost) * parseFloat(sponsorOHead)) / 100.0);
                nsocSubTotalResearchCost = nsocSubTotalResearchCost + resInd;
            }
        }
        indResTotal = indResTotal + resInd;
        if(bgtSectionType == "P")
        {
            formobj.TtlCost.value = (parseFloat(formobj.patNo.value) * parseFloat(formobj.researchCost.value)).toFixed(2);
        }
        //Added by IA 11.03.2006
        if ( budgetTemplate == "C")
        {
	        
	        //Taking care of excle SOC line Item from Totals Check Box
	        if(formobj.stdCareCost.checked && excludeSOCApplyChecked == true)
	        {
				formobj.variance.value =  parseFloat(parseFloat(formobj.sponsorAmount.value)- 0).toFixed(2);
            }
            else
            {
            	formobj.variance.value = parseFloat(parseFloat(formobj.sponsorAmount.value)- parseFloat(formobj.TtlCost.value)).toFixed(2);
            }
        }
        //end added
        
        subTotalTtlCost = parseFloat(formobj.TtlCost.value) + ttlFringeBenefit - ttlDiscount;
        if (formobj.stdCareCost.checked)
        {
            if(!excludeSOCApplyChecked == true)
            {
                socSubTotalTtlCost = parseFloat(formobj.TtlCost.value) + ttlFringeBenefit - ttlDiscount;
                if(formobj.appIndirects.checked == true && formobj.sponsorOHeadApply.checked == true)
                {
                    ttlInd = ((parseFloat(socSubTotalTtlCost) * parseFloat(sponsorOHead)) / 100.0);
                    socSubTotalTtlCost = socSubTotalTtlCost + ttlInd;
                }
            }
            
            //Added by IA 11.03.2006 Outside of the if becuase we don't care if that is selected or not
            if ( budgetTemplate == "C")
			{
	                socSubTotalVariance = parseFloat(formobj.variance.value) ;
                	socSubTotalSponsorAmount = parseFloat(formobj.sponsorAmount.value);
  
    		}
    	    //end added
        }
        else
        {
            nsocSubTotalTtlCost = parseFloat(formobj.TtlCost.value) + ttlFringeBenefit - ttlDiscount;
        	//Added by IA 11.03.2006
   	        if ( budgetTemplate == "C")
        	{   
        		nsocSubTotalSponsorAmount = parseFloat(formobj.sponsorAmount.value);
            	nsocSubTotalVariance = parseFloat(formobj.variance.value) ;
            }
            //end added
            if(formobj.appIndirects.checked == true && formobj.sponsorOHeadApply.checked == true)
            {
                ttlInd = ((parseFloat(nsocSubTotalTtlCost) * parseFloat(sponsorOHead)) / 100.0);
                nsocSubTotalTtlCost = nsocSubTotalTtlCost + ((parseFloat(nsocSubTotalTtlCost) * parseFloat(sponsorOHead)) / 100.0);
            }
        }
        indTtlTotal = indTtlTotal + ttlInd;
        if(formobj.appIndirects.checked == true)
        formobj.hidAppIndirects.value = "1";
        else
        formobj.hidAppIndirects.value = "0";
        if(formobj.appIndirects.checked == true && formobj.sponsorOHeadApply.checked == true)
        {
            if ((formobj.stdCareCost.checked && !excludeSOCApplyChecked == true) || (!formobj.stdCareCost.checked))
            {
                subTotalAppIndResCost = subTotalAppIndResCost + parseFloat(formobj.researchCost.value) + researchFringeBenefit - researchDiscount;
                subTotalAppIndTtlCost = subTotalAppIndTtlCost + parseFloat(formobj.TtlCost.value) + ttlFringeBenefit - ttlDiscount;
            }
            if(formobj.appIndirects.checked == true && formobj.sponsorOHeadApply.checked == true)
            {
                if ((formobj.stdCareCost.checked && !excludeSOCApplyChecked == true) || (!formobj.stdCareCost.checked))
                {
                    subTotalAppIndTtlCost = subTotalAppIndTtlCost + (((parseFloat(formobj.TtlCost.value) + ttlFringeBenefit - ttlDiscount) * parseFloat(sponsorOHead)) / 100.0);
                    subTotalAppIndResCost = subTotalAppIndResCost + (((parseFloat(formobj.researchCost.value) + researchFringeBenefit - researchDiscount) * parseFloat(sponsorOHead)) / 100.0);
                }
            }
            if (formobj.stdCareCost.checked)
            {
                if(!excludeSOCApplyChecked == true)
                {
                    socSubTotalAppIndResCost = socSubTotalAppIndResCost + parseFloat(formobj.researchCost.value) + researchFringeBenefit - researchDiscount;
                    socSubTotalAppIndTtlCost = socSubTotalAppIndTtlCost + parseFloat(formobj.TtlCost.value) + ttlFringeBenefit - ttlDiscount;
                    if(formobj.appIndirects.checked == true && formobj.sponsorOHeadApply.checked == true)
                    {
                        socSubTotalAppIndResCost = socSubTotalAppIndResCost + (((parseFloat(formobj.researchCost.value) + researchFringeBenefit - researchDiscount) * parseFloat(sponsorOHead)) / 100.0);
                        socSubTotalAppIndTtlCost = socSubTotalAppIndTtlCost + (((parseFloat(formobj.TtlCost.value) + ttlFringeBenefit - ttlDiscount) * parseFloat(sponsorOHead)) / 100.0);
                    }
                }
            }
            else
            {
                nsocSubTotalAppIndResCost = nsocSubTotalAppIndResCost + parseFloat(formobj.researchCost.value) + researchFringeBenefit - researchDiscount;
                nsocSubTotalAppIndTtlCost = nsocSubTotalAppIndTtlCost + parseFloat(formobj.TtlCost.value) + ttlFringeBenefit - ttlDiscount;
                if(formobj.appIndirects.checked == true && formobj.sponsorOHeadApply.checked == true)
                {
                    nsocSubTotalAppIndResCost = nsocSubTotalAppIndResCost + (((parseFloat(formobj.researchCost.value) + researchFringeBenefit - researchDiscount) * parseFloat(sponsorOHead)) / 100.0);
                    nsocSubTotalAppIndTtlCost = nsocSubTotalAppIndTtlCost + (((parseFloat(formobj.TtlCost.value) + ttlFringeBenefit - ttlDiscount) * parseFloat(sponsorOHead)) / 100.0);
                }
            }
        }
        else
        {
            subTotalResIndNotApplied = subTotalResIndNotApplied + parseFloat(formobj.researchCost.value) + researchFringeBenefit - researchDiscount;
            subTotalTtlIndNotApplied = subTotalTtlIndNotApplied + parseFloat(formobj.TtlCost.value) + ttlFringeBenefit - ttlDiscount;
            if (formobj.stdCareCost.checked)
            {
                if(!excludeSOCApplyChecked == true)
                {
                    socSubTotalResIndNotApplied = socSubTotalResIndNotApplied + parseFloat(formobj.researchCost.value) + researchFringeBenefit - researchDiscount;
                    socSubTotalTtlIndNotApplied = socSubTotalTtlIndNotApplied + parseFloat(formobj.TtlCost.value) + ttlFringeBenefit - ttlDiscount;
                }
            }
            else
            {
                nsocSubTotalResIndNotApplied = nsocSubTotalResIndNotApplied + parseFloat(formobj.researchCost.value) + researchFringeBenefit - researchDiscount;
                nsocSubTotalTtlIndNotApplied = nsocSubTotalTtlIndNotApplied + parseFloat(formobj.TtlCost.value) + ttlFringeBenefit - ttlDiscount;
            }
        }
        secType = formobj.secType.value;
        if(secType == "O")
        {
            formobj.socSubTotalResearchCost.value = (Math.round(socSubTotalResearchCost*100)/100).toFixed(2);
            formobj.socSubTotalTtlCost.value = (Math.round(socSubTotalTtlCost*100)/100).toFixed(2);
            formobj.nsocSubTotalResearchCost.value = (Math.round(nsocSubTotalResearchCost*100)/100).toFixed(2);
            formobj.nsocSubTotalTtlCost.value = (Math.round(nsocSubTotalTtlCost*100)/100).toFixed(2);
        }
        if (bgtSectionType=="P")
        {
            if(isNaN(formobj.patNo.value) == true)
            {
                /*alert("Please enter a valid Number of Patients value.");*****/
				alert(Vldt_NumOfPatValue);
                formobj.patNo.focus();
                return false;
            }
            if(maxDigits(formobj.patNo.value)==false)
            {
               /* alert("Integer part cannot exceed more than 8 digits.");*****/
				alert(IntegerPartCnt_Exceed);
                formobj.patNo.focus();
                return false;
            }
            var bgtSectionTypePCnt = formobj.bgtSectionTypePCnt.value;
            if(formobj.patNo.value == null || formobj.patNo.value == "")
            formobj.patNo.value = "0.00";
            subTotalPatResearchCost = subTotalResearchCost ;
            subTotalPatAppIndResCost = subTotalAppIndResCost;
            subTotalPatResIndNotApplied = subTotalResIndNotApplied;
            subTotalPatAppIndTtlCost = subTotalAppIndTtlCost;
            subTotalPatTtlIndNotApplied = subTotalTtlIndNotApplied;
            subTotalPatTtlCost = subTotalTtlCost;
            socSubTotalPatResearchCost = socSubTotalResearchCost ;
            socSubTotalPatAppIndResCost = socSubTotalAppIndResCost;
            socSubTotalPatResIndNotApplied = socSubTotalResIndNotApplied;
            socSubTotalPatAppIndTtlCost = socSubTotalAppIndTtlCost;
            socSubTotalPatTtlIndNotApplied = socSubTotalTtlIndNotApplied;
            socSubTotalPatTtlCost = socSubTotalTtlCost;
            nsocSubTotalPatResearchCost = nsocSubTotalResearchCost ;
            nsocSubTotalPatAppIndResCost = nsocSubTotalAppIndResCost;
            nsocSubTotalPatResIndNotApplied = nsocSubTotalResIndNotApplied;
            nsocSubTotalPatAppIndTtlCost = nsocSubTotalAppIndTtlCost;
            nsocSubTotalPatTtlIndNotApplied = nsocSubTotalTtlIndNotApplied;
            nsocSubTotalPatTtlCost = nsocSubTotalTtlCost;
            formobj.socSubTotalPatResearchCost.value =(Math.round(socSubTotalPatResearchCost*100)/100).toFixed(2) ;
            formobj.socSubTotalPatTtlCost.value = (Math.round(socSubTotalPatTtlCost*100)/100).toFixed(2);
            formobj.nsocSubTotalPatResearchCost.value =(Math.round(nsocSubTotalPatResearchCost*100)/100).toFixed(2) ;
            formobj.nsocSubTotalPatTtlCost.value = (Math.round(nsocSubTotalPatTtlCost*100)/100).toFixed(2);
            totalResearchCost = subTotalPatResearchCost ;
            totalAppIndResCost = subTotalPatAppIndResCost;
            totalResIndNotApplied = subTotalPatResIndNotApplied;
            totalAppIndTtlCost = subTotalPatAppIndTtlCost;
            totalTtlIndNotApplied = subTotalPatTtlIndNotApplied;
            totalTtlCost = subTotalPatTtlCost;
            socTotalResearchCost = socSubTotalPatResearchCost ;
            socTotalAppIndResCost = socSubTotalPatAppIndResCost;
            socTtotalResIndNotApplied = socSubTotalPatResIndNotApplied;
            socTotalAppIndTtlCost = socSubTotalPatAppIndTtlCost;
            socTotalTtlIndNotApplied = socSubTotalPatTtlIndNotApplied;
            socTotalTtlCost = socSubTotalPatTtlCost;
            nsocTotalResearchCost = nsocSubTotalPatResearchCost ;
            nsocTotalAppIndResCost = nsocSubTotalPatAppIndResCost;
            nsocTtotalResIndNotApplied = nsocSubTotalPatResIndNotApplied;
            nsocTotalAppIndTtlCost = nsocSubTotalPatAppIndTtlCost;
            nsocTotalTtlIndNotApplied = nsocSubTotalPatTtlIndNotApplied;
            nsocTotalTtlCost = nsocSubTotalPatTtlCost;
        }
        else
        {
            totalResearchCost = subTotalResearchCost ;
            totalAppIndResCost = subTotalAppIndResCost;
            totalResIndNotApplied = subTotalResIndNotApplied;
            totalAppIndTtlCost = subTotalAppIndTtlCost;
            totalTtlIndNotApplied = subTotalTtlIndNotApplied;
            totalTtlCost = subTotalTtlCost;
            socTotalResearchCost = socSubTotalResearchCost ;
            socTotalAppIndResCost = socSubTotalAppIndResCost;
            socTotalResIndNotApplied = socSubTotalResIndNotApplied;
            socTotalAppIndTtlCost = socSubTotalAppIndTtlCost;
            socTotalTtlIndNotApplied = socSubTotalTtlIndNotApplied;
            socTotalTtlCost = socSubTotalTtlCost;
            nsocTotalResearchCost = nsocSubTotalResearchCost ;
            nsocTotalAppIndResCost = nsocSubTotalAppIndResCost;
            nsocTotalResIndNotApplied = nsocSubTotalResIndNotApplied;
            nsocTotalAppIndTtlCost = nsocSubTotalAppIndTtlCost;
            nsocTotalTtlIndNotApplied = nsocSubTotalTtlIndNotApplied;
            nsocTotalTtlCost = nsocSubTotalTtlCost;
        }
        
        if ( budgetTemplate == "C")
		{
			
	        formobj.nsocSubTotalSponsorAmountCost.value = (Math.round(nsocSubTotalSponsorAmount*100)/100).toFixed(2);
		    //formobj.nsocSubTotalVariance.value = (Math.round(nsocSubTotalVariance*100)/100).toFixed(2);
			formobj.nsocSubTotalVariance.value = (Math.round(((formobj.nsocSubTotalSponsorAmountCost.value) - nsocSubTotalPatTtlCost  )*100)/100).toFixed(2);
	        formobj.socSubTotalSponsorAmountCost.value = (Math.round(socSubTotalSponsorAmount*100)/100).toFixed(2);
			//formobj.socSubTotalVariance.value = (Math.round(socSubTotalVariance*100)/100).toFixed(2);
	        formobj.socSubTotalVariance.value = (Math.round(((formobj.socSubTotalSponsorAmountCost.value) - socSubTotalPatTtlCost  )*100)/100).toFixed(2);
			
	        socTotalSponsorAmount = socSubTotalSponsorAmount;
	        nsocTotalSponsorAmount = nsocSubTotalSponsorAmount;
	        socTotalSponsorAmount = socSubTotalSponsorAmount;
	     	socTotalVariance = socSubTotalVariance;
	  		nsocTotalSponsorAmount = nsocSubTotalSponsorAmount;
	        nsocTotalVariance = nsocSubTotalVariance;
			socTotalVariance  =  socSubTotalVariance;
			nsocTotalVariance = nsocSubTotalVariance;
		}
		
        if(formobj.cmbCtgry.value==perCodeId)
        {
            if ((formobj.stdCareCost.checked && !excludeSOCApplyChecked == true) || (!formobj.stdCareCost.checked))
            {
                salTtlTotal = (parseFloat(formobj.TtlCost.value)).toFixed(2);
            }
        }
        if(formobj.stdCareCost.checked && excludeSOCApplyChecked == true)
        {
            formobj.researchCost.value = "0.00";
            formobj.TtlCost.value = "0.00";
        }
        formobj.totalSalResCost.value = (parseFloat(salResTotal)).toFixed(2);
        formobj.totalSalTtlCost.value = (parseFloat(salTtlTotal)).toFixed(2);
        formobj.totalFringeResCost.value = (parseFloat(fringeResTotal)).toFixed(2);
        formobj.totalFringeTtlCost.value = (parseFloat(fringeTtlTotal)).toFixed(2);
        formobj.totalDiscResCost.value = (parseFloat(discResTotal)).toFixed(2);
        formobj.totalDiscTtlCost.value = (parseFloat(discTtlTotal)).toFixed(2);
        formobj.totalIndirectsResCost.value = (parseFloat(indResTotal)).toFixed(2);
        formobj.totalIndirectsTtlCost.value = (parseFloat(indTtlTotal)).toFixed(2);
        //alert("enter to here the last part of the first");
    }
    else
    {//if there is more than one line item
        var arrayOfCosts=document.getElementsByName("unitCost");
        var arrayOfUnits=document.getElementsByName("noUnits");
        var arrayOfResCosts=document.getElementsByName("researchCost");
        var arrayOfDiscounts=document.getElementsByName("discount");
        var arrayOfHidDiscbox=document.getElementsByName("hidDiscountChkbox");
        var arrayOfStdCareCost=document.getElementsByName("stdCareCost");
        var arrayOfHidStdCareCost=document.getElementsByName("hidStdCareCost");
        var arrayOfSecTypes=document.getElementsByName("secType");
        var arrayOfCmbCtgry=document.getElementsByName("cmbCtgry");
        var arrayOfTtlCosts=document.getElementsByName("TtlCost");
        //Added by IA 11.03.2006
        var arrayOfSponsorAmount=document.getElementsByName("sponsorAmount");
        var arrayOfVariance=document.getElementsByName("variance");
        //end added
        
        for(i=0;i<arrayOfCosts.length;i++)
        {
            if(isNaN(arrayOfCosts[i].value) == true)
            {
                /*alert("Please enter a valid Unit Cost.");*****/
				 alert(EtrValid_UnitCost);
                arrayOfCosts[i].focus();
                return false;
            }
            if(maxDigits(arrayOfCosts[i].value)==false)
            {
               /* alert("Integer part cannot exceed more than 8 digits.");*****/
				alert(IntegerPartCnt_Exceed);
                arrayOfCosts[i].focus();
                return false;
            }
            if(isNaN(arrayOfUnits[i].value) == true)
            {
                /*alert("Please enter valid # of Units.");*****/
				 alert(EtrValidUnits);
				
                arrayOfUnits[i].focus();
                return false;
            }
            if(maxDigits(arrayOfUnits[i].value)==false)
            {
               /* alert("Integer part cannot exceed more than 8 digits.");*****/
				alert(IntegerPartCnt_Exceed);
                arrayOfUnits[i].focus();
                return false;
            }
            if(isNaN(arrayOfResCosts[i].value) == true)
            {
                /*alert("Please enter a valid Cost/Patient value.");*****/
				 alert(PlsEtrCost_PatVal);
                arrayOfResCosts[i].focus();
                return false;
            }
            if ( budgetTemplate == "C")
            {
                if(isNaN(arrayOfSponsorAmount[i].value) == true)
                {
                    /*alert("Please enter a valid Sponsor Amount value.");*****/
					 alert(Vldt_SponsorAmt);
                    arrayOfSponsorAmount[i].focus();
                    return false;
                }
            }
            if(arrayOfCosts[i].value=="0.00" || arrayOfUnits[i].value=="0.00" || arrayOfCosts[i].value=="" || arrayOfUnits[i].value=="")
            {
                if(maxDigits(arrayOfResCosts[i].value)==false)
                {
                    /*alert("Integer part cannot exceed more than 8 digits.");*****/
				alert(IntegerPartCnt_Exceed);
                    arrayOfResCosts[i].focus();
                    return false;
                }
            }
            if (arrayOfDiscounts[i].checked)
            {
                arrayOfHidDiscbox[i].value = "1";
            }
            else
            {
                arrayOfHidDiscbox[i].value = "0";
            }
            if (arrayOfStdCareCost[i].checked)
            {
                if(arrayOfCosts[i].value != "")
                arrayOfHidStdCareCost[i].value = arrayOfCosts[i].value;
                else
                arrayOfHidStdCareCost[i].value = "0.00"
            }
            else
            {
                arrayOfHidStdCareCost[i].value = "";
            }
            var secType = arrayOfSecTypes[i].value;
            var cmbCtgryValue=arrayOfCmbCtgry[i].value;
            var stdCareCostChecked=arrayOfStdCareCost[i].checked;
            var researchCost = arrayOfResCosts[i].value;
            var unitCostValue=arrayOfCosts[i].value;
            var noUnitsValue=arrayOfUnits[i].value;
            //Added by IA 11.03.2006
           if ( budgetTemplate == "C")
			{
	            var sponsorAmountCostValue=arrayOfSponsorAmount[i].value;
    		}
            //end added
            if(cmbCtgryValue==perCodeId)
            {
                if ((stdCareCostChecked && !excludeSOCApplyChecked == true) || (!stdCareCostChecked))
                {
                    salResTotal = salResTotal + parseFloat(researchCost);
                }
            }
            if(cmbCtgryValue==perCodeId)
            {
                if ((stdCareCostChecked && !excludeSOCApplyChecked == true) || (!stdCareCostChecked))
                {
                    salTtlTotal = salTtlTotal + parseFloat(arrayOfTtlCosts[i].value);
                }
            }
            arrayOfCosts[i].value = (Math.round(unitCostValue *100)/100).toFixed(2);
            arrayOfUnits[i].value = (Math.round(noUnitsValue *100)/100).toFixed(2);
            unitCostValue=arrayOfCosts[i].value;
            noUnitsValue=arrayOfUnits[i].value;
           
            if(secType == "P")
            {
				if (bgtSectionTypePCntTotal == "1")
				
				{	patNo = formobj.patNo.value;
					
				}
	            else
				{	
					if (bgtSectionTypeOCntTotal == "0" && perPatientSectionCnt == "1") 
					{
						patNo = formobj.patNo.value;
					}
					else
					{	
						if (bgtSectionTypeOCntTotal == "1" && perPatientSectionCnt == "1")
						{
							patNo = formobj.patNo.value;						
						}
						else
						{
							var patNoTemp = document.getElementsByName("patNo");
							if(patNoTemp[1].value == null || patNoTemp[1].value == "")
							{
								patNo = formobj.patNo.value;
							}
							else
							{
								patNo = patNoTemp[1].value;
							}
						}
					}
				}
	                
	                patNo = (Math.round(patNo*100)/100).toFixed(2);
	            	researchCost = (Math.round((parseFloat(unitCostValue) * parseFloat(noUnitsValue))*100)/100).toFixed(2);
	                ttlCost = researchCost * patNo;
	              //  alert("researchCost***"+researchCost);
	            //    alert("ttlCost***"+researchCost);

				            
            }
            else
            {
                if(secType == "P")
                {
                    researchCost = "0.00"
                    ttlCost ="0.00"
                }
            }
            if(secType == "O")
            {	
            //	alert("enter to o");
                ttlCost = (Math.round((parseFloat(unitCostValue) * parseFloat(noUnitsValue))*100)/100).toFixed(2);
                arrayOfTtlCosts[i].value = ttlCost;
            }
            if ((formobj.fringeBenefitApply.checked == true) && (cmbCtgryValue==perCodeId) )
            {
                if ((stdCareCostChecked && !excludeSOCApplyChecked == true) || (!stdCareCostChecked))
                {
                    researchFringeBenefit = researchFringeBenefit + ((parseFloat(researchCost) * parseFloat(fringeBenefit)) / 100.0);
                    ttlFringeBenefit = ttlFringeBenefit + ((parseFloat(ttlCost) * parseFloat(fringeBenefit)) / 100.0);
                }
                fringeResTotal = fringeResTotal + researchFringeBenefit;
                fringeTtlTotal = fringeTtlTotal + ttlFringeBenefit;
            }
            
            if(secType == "P")
            {
            
                if(researchCost!="")
                {
      			//	alert("entertoinsert researchcost");
      		//		alert("entertoinsert researchcost");
                    arrayOfResCosts[i].value = (parseFloat(researchCost)).toFixed(2);
                    arrayOfTtlCosts[i].value = (parseFloat(ttlCost)).toFixed(2);
                //    alert("totalcost " + arrayOfTtlCosts[i].value);
                }
                else
                {
                    arrayOfResCosts[i].value = "0.00";
                    arrayOfTtlCosts[i].value = "0.00";
                }
            }
            else
            {
                if(researchCost!="")
                arrayOfResCosts[i].value = (parseFloat(researchCost)).toFixed(2) ;
                else
                arrayOfResCosts[i].value = "0.00";
            }
            if(stdCareCostChecked && excludeSOCApplyChecked == true)
            {
                arrayOfResCosts[i].value = "0.00";
                arrayOfTtlCosts.value = "0.00";
            }
            researchCost = arrayOfResCosts[i].value;
            
       	     //added by IA 11.03.2006 //One section multiple lines
	       	if ( budgetTemplate == "C")
			{	
				var varianceValueCost = 0
					
	            if(stdCareCostChecked && excludeSOCApplyChecked == true)
            	{	
            	//	var varianceValueCost = (Math.round(sponsorAmountCostValue *100)/100).toFixed(2);
            		varianceValueCost	 = (Math.round( ( arrayOfSponsorAmount[i].value)*100)/100).toFixed(2);
		        }
		        else
		        {
            	//	var varianceValueCost = (Math.round(sponsorAmountCostValue *100)/100).toFixed(2);
            		varianceValueCost = (Math.round( (arrayOfSponsorAmount[i].value -  arrayOfTtlCosts[i].value )*100)/100).toFixed(2);

   		        }

           		arrayOfSponsorAmount[i].value =  (Math.round(sponsorAmountCostValue *100)/100).toFixed(2);
           		arrayOfVariance[i].value =   varianceValueCost;
				
				
				if (stdCareCostChecked == true)
				{
         			socSubTotalVariance = parseFloat(socSubTotalVariance) + parseFloat(varianceValueCost);
    				socSubTotalSponsorAmount = parseFloat(socSubTotalSponsorAmount) +  parseFloat(sponsorAmountCostValue);
    				
    				
				}
				else
				{
		
					nsocSubTotalVariance = (parseFloat(nsocSubTotalVariance)) + (parseFloat(varianceValueCost)) ; //(Math.round((arrayOfVariance[i].value)*100)/100).toFixed(2) ;
    				nsocSubTotalSponsorAmount = 	parseFloat(nsocSubTotalSponsorAmount) + parseFloat(sponsorAmountCostValue);
		
				}
				
			}
            //end added
        }//end for i
        
        formobj.totalSalResCost.value = (parseFloat(salResTotal)).toFixed(2);
        formobj.totalSalTtlCost.value = (parseFloat(salTtlTotal)).toFixed(2);
        salResTotal =0 ;
        salTtlTotal =0 ;
        fringeResTotal = 0;
        fringeTtlTotal = 0;
        discResTotal = 0;
        discTtlTotal = 0;
        indResTotal = 0;
        indTtlTotal = 0;
        //Getting subtotal for the first section
        if(formobj.sectionCount.value == 1)
        {//section count = 1
		
         	if(formobj.sectionRowsIndex.value == 0)
            {	
                subTotalResearchCost = subTotalResearchCost + researchFringeBenefit;
                subTotalTtlCost = subTotalTtlCost + ttlFringeBenefit;
                subTotalAppIndResCost = subTotalAppIndResCost + researchFringeBenefit;
                subTotalResIndNotApplied = subTotalResIndNotApplied + researchFringeBenefit;
                subTotalAppIndTtlCost = subTotalAppIndTtlCost + ttlFringeBenefit;
                subTotalTtlIndNotApplied = subTotalTtlIndNotApplied + ttlFringeBenefit;
                formobj.subTotalResearchCost.value = (Math.round(subTotalResearchCost*100)/100).toFixed(2);
                socSubTotalResearchCost = socSubTotalResearchCost + researchFringeBenefit;
                socSubTotalTtlCost = socSubTotalTtlCost + ttlFringeBenefit;
                socSubTotalAppIndResCost = socSubTotalAppIndResCost + researchFringeBenefit;
                socSubTotalResIndNotApplied = socSubTotalResIndNotApplied + researchFringeBenefit;
                socSubTotalAppIndTtlCost = socSubTotalAppIndTtlCost + ttlFringeBenefit;
                socSubTotalTtlIndNotApplied = socSubTotalTtlIndNotApplied + ttlFringeBenefit;
                formobj.socSubTotalResearchCost.value = (Math.round(socSubTotalResearchCost*100)/100).toFixed(2);
                formobj.socSubTotalTtlCost.value = (Math.round(socSubTotalTtlCost*100)/100).toFixed(2);
                nsocSubTotalResearchCost = nsocSubTotalResearchCost + researchFringeBenefit;
                nsocSubTotalTtlCost = nsocSubTotalTtlCost + ttlFringeBenefit;
                nsocSubTotalAppIndResCost = nsocSubTotalAppIndResCost + researchFringeBenefit;
                nsocSubTotalResIndNotApplied = nsocSubTotalResIndNotApplied + researchFringeBenefit;
                nsocSubTotalAppIndTtlCost = nsocSubTotalAppIndTtlCost + ttlFringeBenefit;
                nsocSubTotalTtlIndNotApplied = nsocSubTotalTtlIndNotApplied + ttlFringeBenefit;
                formobj.nsocSubTotalResearchCost.value = (Math.round(nsocSubTotalResearchCost*100)/100).toFixed(2);
                formobj.nsocSubTotalTtlCost.value = (Math.round(nsocSubTotalTtlCost*100)/100).toFixed(2);
				
				
                bgtSectionType = formobj.bgtSectionTypeRow.value;
                if (bgtSectionType=="P")
                {
                    if(isNaN(formobj.patNo.value) == true)
                    {
                        /*alert("Please enter a valid Number of Patients value.");*****/
						alert(Vldt_NumOfPatValue);
                        formobj.patNo.focus();
                        return false;
                    }
                    if(maxDigits(formobj.patNo.value)==false)
                    {
                        /*alert("Integer part cannot exceed more than 8 digits.");*****/
				alert(IntegerPartCnt_Exceed);
                        formobj.patNo.focus();
                        return false;
                    }
                    bgtSectionTypePCnt = formobj.bgtSectionTypePCnt.value;
                    if(formobj.patNo.value == null || formobj.patNo.value == "")
                    
                    formobj.patNo.value = "0.00";
                    patNo = (formobj.patNo.value).toFixed(2);
                //    alert("enter to here");
                    formobj.patNo.value = patNo;
                    if (bgtSectionType=="P")
                    {
                        formobj.TtlCost.value = (parseFloat(formobj.patNo.value) * parseFloat(formobj.researchCost.value)).toFixed(2);
                    }
                    else if (bgtSectionType=="O")
                    {
                        formobj.TtlCost.value = (Math.round((parseFloat(formobj.unitCost.value) * parseFloat(formobj.noUnits.value))*100)/100).toFixed(2);
                    }
                    subTotalPatResearchCost = subTotalResearchCost ;
                    subTotalPatAppIndResCost = subTotalAppIndResCost;
                    subTotalPatResIndNotApplied = subTotalResIndNotApplied;
                    subTotalPatAppIndTtlCost = subTotalAppIndTtlCost;
                    subTotalPatTtlIndNotApplied = subTotalTtlIndNotApplied;
                    subTotalPatTtlCost = subTotalTtlCost;
                    socSubTotalPatResearchCost = socSubTotalResearchCost ;
                    socSubTotalPatAppIndResCost = socSubTotalAppIndResCost;
                    socSubTotalPatResIndNotApplied = socSubTotalResIndNotApplied;
                    socSubTotalPatAppIndTtlCost = socSubTotalAppIndTtlCost;
                    socSubTotalPatTtlIndNotApplied = socSubTotalTtlIndNotApplied;
                    socSubTotalPatTtlCost = socSubTotalTtlCost;
                    nsocSubTotalPatResearchCost = nsocSubTotalResearchCost ;
                    nsocSubTotalPatAppIndResCost = nsocSubTotalAppIndResCost;
                    nsocSubTotalPatResIndNotApplied = nsocSubTotalResIndNotApplied;
                    nsocSubTotalPatAppIndTtlCost = nsocSubTotalAppIndTtlCost;
                    nsocSubTotalPatTtlIndNotApplied = nsocSubTotalTtlIndNotApplied;
                    nsocSubTotalPatTtlCost = nsocSubTotalTtlCost;
                    totalResearchCost = totalResearchCost + subTotalPatResearchCost;
                    totalTtlCost = totalTtlCost+ subTotalPatTtlCost;
                    totalAppIndResCost = totalAppIndResCost + subTotalPatAppIndResCost;
                    totalResIndNotApplied = totalResIndNotApplied + subTotalPatResIndNotApplied;
                    totalAppIndTtlCost = totalAppIndTtlCost + subTotalPatAppIndTtlCost;
                    totalTtlIndNotApplied = totalTtlIndNotApplied + subTotalPatTtlIndNotApplied;
                    formobj.socSubTotalPatResearchCost.value =(Math.round(socSubTotalPatResearchCost*100)/100).toFixed(2);
                    formobj.socSubTotalPatTtlCost.value = (Math.round(socSubTotalPatTtlCost*100)/100).toFixed(2);
                    socTotalResearchCost = socTotalResearchCost + socSubTotalPatResearchCost;
                    socTotalTtlCost = socTotalTtlCost+ socSubTotalPatTtlCost;
                    socTotalAppIndResCost = socTotalAppIndResCost + socSubTotalPatAppIndResCost;
                    socTotalResIndNotApplied = socTotalResIndNotApplied + socSubTotalPatResIndNotApplied;
                    socTotalAppIndTtlCost = socTotalAppIndTtlCost + socSubTotalPatAppIndTtlCost;
                    socTotalTtlIndNotApplied = socTotalTtlIndNotApplied + socSubTotalPatTtlIndNotApplied;
                    formobj.nsocSubTotalPatResearchCost.value =(Math.round(nsocSubTotalPatResearchCost*100)/100).toFixed(2);
                    formobj.nsocSubTotalPatTtlCost.value = (Math.round(nsocSubTotalPatTtlCost*100)/100).toFixed(2);
                    nsocTotalResearchCost = nsocTotalResearchCost + nsocSubTotalPatResearchCost;
                    nsocTotalTtlCost = nsocTotalTtlCost+ nsocSubTotalPatTtlCost;
                    nsocTotalAppIndResCost = nsocTotalAppIndResCost + nsocSubTotalPatAppIndResCost;
                    nsocTotalResIndNotApplied = nsocTotalResIndNotApplied + nsocSubTotalPatResIndNotApplied;
                    nsocTotalAppIndTtlCost = nsocTotalAppIndTtlCost + nsocSubTotalPatAppIndTtlCost;
                    nsocTotalTtlIndNotApplied = nsocTotalTtlIndNotApplied + nsocSubTotalPatTtlIndNotApplied;
                    if(formobj.stdCareCost.checked && excludeSOCApplyChecked == true)
                    {
                        formobj.researchCost.value = "0.00";
                        formobj.TtlCost.value = "0.00";
                    }
                }
                else
                {
                    totalResearchCost = totalResearchCost + subTotalResearchCost;
                    totalTtlCost = totalTtlCost+ subTotalTtlCost;
                    totalAppIndResCost = totalAppIndResCost + subTotalAppIndResCost;
                    totalResIndNotApplied = totalResIndNotApplied + subTotalResIndNotApplied;
                    totalAppIndTtlCost = totalAppIndTtlCost + subTotalAppIndTtlCost;
                    totalTtlIndNotApplied = totalTtlIndNotApplied + subTotalTtlIndNotApplied;
                    socTotalResearchCost = socTotalResearchCost + socSubTotalResearchCost;
                    socTotalTtlCost = socTotalTtlCost+ socSubTotalTtlCost;
                    socTotalAppIndResCost = socTotalAppIndResCost + socSubTotalAppIndResCost;
                    socTotalResIndNotApplied = socTotalResIndNotApplied + socSubTotalResIndNotApplied;
                    socTotalAppIndTtlCost = socTotalAppIndTtlCost + socSubTotalAppIndTtlCost;
                    socTotalTtlIndNotApplied = socTotalTtlIndNotApplied + socSubTotalTtlIndNotApplied;
                    nsocTotalResearchCost = nsocTotalResearchCost + nsocSubTotalResearchCost;
                    nsocTotalTtlCost = nsocTotalTtlCost+ nsocSubTotalTtlCost;
                    nsocTotalAppIndResCost = nsocTotalAppIndResCost + nsocSubTotalAppIndResCost;
                    nsocTotalResIndNotApplied = nsocTotalResIndNotApplied + nsocSubTotalResIndNotApplied;
                    nsocTotalAppIndTtlCost = nsocTotalAppIndTtlCost + nsocSubTotalAppIndTtlCost;
                    nsocTotalTtlIndNotApplied = nsocTotalTtlIndNotApplied + nsocSubTotalTtlIndNotApplied;
                }
            }
            else
            {
                var secionRowIndexValue=formobj.sectionRowsIndex.value;
                for(i=0;i<=secionRowIndexValue;i++)
                {
                    researchFringeBenefit = 0;
                    researchDiscount = 0;
                    ttlDiscount = 0;
                    resInd = 0;
                    ttlInd = 0;
                    stdCareDiscount = 0;
                    ttlFringeBenefit = 0;
                    var cmbCtgryValue=formobj.cmbCtgry[i].value;
                    var stdCareCostChecked=formobj.stdCareCost[i].checked;
                    var researchCostValue=formobj.researchCost[i].value;
                    var TtlCostValue=formobj.TtlCost[i].value;
                    if(cmbCtgryValue==perCodeId)
                    {
                        if ((stdCareCostChecked && !excludeSOCApplyChecked == true) || (!stdCareCostChecked))
                        {
                            salResTotal = salResTotal + parseFloat(researchCostValue);
                        }
                    }
                    if(cmbCtgryValue==perCodeId)
                    {
                        if ((stdCareCostChecked && !excludeSOCApplyChecked == true) || (!stdCareCostChecked))
                        {
                            salTtlTotal = salTtlTotal + parseFloat(TtlCostValue);
                        }
                    }
                    if ((formobj.fringeBenefitApply.checked == true) && (cmbCtgryValue==perCodeId) )
                    {
                        if ((stdCareCostChecked && !excludeSOCApplyChecked == true) || (!stdCareCostChecked))
                        {
                            researchFringeBenefit = ((parseFloat(researchCostValue) * parseFloat(fringeBenefit)) / 100.0);
                            ttlFringeBenefit = ((parseFloat(TtlCostValue) * parseFloat(fringeBenefit)) / 100.0);
                        }
                        fringeResTotal = fringeResTotal + researchFringeBenefit;
                        fringeTtlTotal = fringeTtlTotal + ttlFringeBenefit;
                    }
                    if ((costDiscountApply==true) && (!isNaN(costDiscount)) && (formobj.discount[i].checked == true))
                    {
                        if ((stdCareCostChecked && !excludeSOCApplyChecked == true) || (!stdCareCostChecked))
                        {
                            researchDiscount = (((parseFloat(researchCostValue) + researchFringeBenefit) * parseFloat(costDiscount)) / 100.0);
                            ttlDiscount = (((parseFloat(TtlCostValue) + ttlFringeBenefit) * parseFloat(costDiscount)) / 100.0);
                        }
                        discResTotal = discResTotal + researchDiscount;
                        discTtlTotal = discTtlTotal + ttlDiscount;
                    }
                    subTotalResearchCost = subTotalResearchCost + parseFloat(researchCostValue) + researchFringeBenefit - researchDiscount;
                    subTotalTtlCost = subTotalTtlCost + parseFloat(TtlCostValue) + ttlFringeBenefit - ttlDiscount;
                    var appIndirectsChecked=formobj.appIndirects[i].checked;
                    if(appIndirectsChecked == true && formobj.sponsorOHeadApply.checked == true)
                    {
                        subTotalResearchCost = subTotalResearchCost + (((parseFloat(researchCostValue) + researchFringeBenefit - researchDiscount) * parseFloat(sponsorOHead)) / 100.0);
                        subTotalTtlCost = subTotalTtlCost + (((parseFloat(TtlCostValue) + ttlFringeBenefit - ttlDiscount) * parseFloat(sponsorOHead)) / 100.0);
                    }
                    if (stdCareCostChecked)
                    {
                        if(!excludeSOCApplyChecked == true)
                        {
                            socSubTotalResearchCost = socSubTotalResearchCost + parseFloat(researchCostValue) + researchFringeBenefit - researchDiscount;
                            socSubTotalTtlCost = socSubTotalTtlCost + parseFloat(TtlCostValue) + ttlFringeBenefit - ttlDiscount;
                            if(appIndirectsChecked == true && formobj.sponsorOHeadApply.checked == true)
                            {
                                resInd = (((parseFloat(researchCostValue) + researchFringeBenefit - researchDiscount) * parseFloat(sponsorOHead)) / 100.0);
                                socSubTotalResearchCost = socSubTotalResearchCost + resInd;
                                ttlInd = (((parseFloat(TtlCostValue) + ttlFringeBenefit - ttlDiscount) * parseFloat(sponsorOHead)) / 100.0);
                                socSubTotalTtlCost = socSubTotalTtlCost + ttlInd;
                            }
                        }
                    }
                    else
                    {
                        nsocSubTotalResearchCost = nsocSubTotalResearchCost + parseFloat(researchCostValue) + researchFringeBenefit - researchDiscount;
                        nsocSubTotalTtlCost = nsocSubTotalTtlCost + parseFloat(TtlCostValue) + ttlFringeBenefit - ttlDiscount;
                        if(appIndirectsChecked == true && formobj.sponsorOHeadApply.checked == true)
                        {
                            resInd = (((parseFloat(researchCostValue) + researchFringeBenefit - researchDiscount) * parseFloat(sponsorOHead)) / 100.0);
                            nsocSubTotalResearchCost = nsocSubTotalResearchCost + resInd;
                            ttlInd = (((parseFloat(TtlCostValue) + ttlFringeBenefit - ttlDiscount) * parseFloat(sponsorOHead)) / 100.0)
                            nsocSubTotalTtlCost = nsocSubTotalTtlCost + ttlInd;
                        }
                    }
                    indResTotal = indResTotal + resInd;
                    indTtlTotal = indTtlTotal + ttlInd;
                    if(appIndirectsChecked == true)
                    formobj.hidAppIndirects[i].value = "1";
                    else
                    formobj.hidAppIndirects[i].value = "0";
                    if(appIndirectsChecked == true && formobj.sponsorOHeadApply.checked == true)
                    {
                        if ((stdCareCostChecked && !excludeSOCApplyChecked == true) || (!stdCareCostChecked))
                        {
                            subTotalAppIndResCost = subTotalAppIndResCost + parseFloat(researchCostValue) + researchFringeBenefit - researchDiscount;
                            subTotalAppIndTtlCost = subTotalAppIndTtlCost + parseFloat(TtlCostValue) + ttlFringeBenefit - ttlDiscount;
                        }
                        if(appIndirectsChecked == true && formobj.sponsorOHeadApply.checked == true)
                        {
                            if ((stdCareCostChecked && !excludeSOCApplyChecked == true) || (!stdCareCostChecked))
                            {
                                subTotalAppIndResCost = subTotalAppIndResCost + (((parseFloat(researchCostValue) + researchFringeBenefit - researchDiscount) * parseFloat(sponsorOHead)) / 100.0);
                                subTotalAppIndTtlCost = subTotalAppIndTtlCost + (((parseFloat(TtlCostValue) + ttlFringeBenefit - ttlDiscount) * parseFloat(sponsorOHead)) / 100.0);
                            }
                        }
                        if (stdCareCostChecked)
                        {
                            if(!excludeSOCApplyChecked == true)
                            {
                                socSubTotalAppIndResCost = socSubTotalAppIndResCost + parseFloat(researchCostValue) + researchFringeBenefit - researchDiscount;
                                socSubTotalAppIndTtlCost = socSubTotalAppIndTtlCost + parseFloat(TtlCostValue) + ttlFringeBenefit - ttlDiscount;
                                if(appIndirectsChecked == true && formobj.sponsorOHeadApply.checked == true)
                                {
                                    socSubTotalAppIndResCost = socSubTotalAppIndResCost + (((parseFloat(researchCostValue) + researchFringeBenefit - researchDiscount) * parseFloat(sponsorOHead)) / 100.0);
                                    socSubTotalAppIndTtlCost = socSubTotalAppIndTtlCost + (((parseFloat(TtlCostValue) + ttlFringeBenefit - ttlDiscount) * parseFloat(sponsorOHead)) / 100.0);
                                }
                            }
                        }
                        else
                        {
                            nsocSubTotalAppIndResCost = nsocSubTotalAppIndResCost + parseFloat(researchCostValue) + researchFringeBenefit - researchDiscount;
                            nsocSubTotalAppIndTtlCost = nsocSubTotalAppIndTtlCost + parseFloat(TtlCostValue) + ttlFringeBenefit - ttlDiscount;
                            if(appIndirectsChecked == true && formobj.sponsorOHeadApply.checked == true)
                            {
                                nsocSubTotalAppIndResCost = nsocSubTotalAppIndResCost + (((parseFloat(researchCostValue) + researchFringeBenefit - researchDiscount) * parseFloat(sponsorOHead)) / 100.0);
                                nsocSubTotalAppIndTtlCost = nsocSubTotalAppIndTtlCost + (((parseFloat(TtlCostValue) + ttlFringeBenefit - ttlDiscount) * parseFloat(sponsorOHead)) / 100.0);
                            }
                        }
                    }
                    else
                    {
                        subTotalResIndNotApplied = subTotalResIndNotApplied + parseFloat(researchCostValue) + researchFringeBenefit - researchDiscount;
                        subTotalTtlIndNotApplied = subTotalTtlIndNotApplied + parseFloat(TtlCostValue) + ttlFringeBenefit - ttlDiscount;
                        if (stdCareCostChecked)
                        {
                            if(!excludeSOCApplyChecked == true)
                            {
                                socSubTotalResIndNotApplied = socSubTotalResIndNotApplied + parseFloat(researchCostValue) + researchFringeBenefit - researchDiscount;
                                socSubTotalTtlIndNotApplied = socSubTotalTtlIndNotApplied + parseFloat(TtlCostValue) + ttlFringeBenefit - ttlDiscount;
                            }
                        }
                        else
                        {
                            nsocSubTotalResIndNotApplied = nsocSubTotalResIndNotApplied + parseFloat(researchCostValue) + researchFringeBenefit - researchDiscount;
                            nsocSubTotalTtlIndNotApplied = nsocSubTotalTtlIndNotApplied + parseFloat(TtlCostValue) + ttlFringeBenefit - ttlDiscount;
                        }
                        if(stdCareCostChecked && excludeSOCApplyChecked == true)
                        {
                            formobj.researchCost[i].value = "0.00";
                            formobj.TtlCost[i].value = "0.00";
                        }
                    }
                    bgtSectionType = formobj.bgtSectionTypeRow.value;
                    if (bgtSectionType=="P")
                    {
                        if(isNaN(formobj.patNo.value) == true)
                        {
                            /*alert("Please enter a valid Number of Patients value.");*****/
							alert(Vldt_NumOfPatValue);
                            formobj.patNo.focus();
                            return false;
                        }
                        if(maxDigits(formobj.patNo.value)==false)
                        {
                          /*  alert("Integer part cannot exceed more than 8 digits.");*****/
				alert(IntegerPartCnt_Exceed);
                            formobj.patNo.focus();
                            return false;
                        }
                        //changed from secion to per patient
                        if (formobj.sectionRowsIndex.value==1 || perPatientSectionCnt==1 )
                        {
                            bgtSectionTypePCnt = formobj.bgtSectionTypePCnt.value;
                        }
                        else
                        {	
                            bgtSectionTypePCnt = formobj.bgtSectionTypePCnt[i].value;
                        }
                     //   alert ("Valueis" + bgtSectionTypePCnt);
                        if(formobj.patNo.value == null || formobj.patNo.value == "")
                        formobj.patNo.value = "0.00";
                        //changed person for perpatient
                        if (perPatientSectionCnt==1)
                        {
                            patNo = Math.round((formobj.patNo.value *100)/100).toFixed(2);
                        }
                        else
                        {	
  //                      	alert ('Alert' + bgtSectionTypePCntTotal ) ;
                            if(formobj.patNo[bgtSectionTypePCnt].value == null || formobj.patNo[bgtSectionTypePCnt].value == "")
                            formobj.patNo[bgtSectionTypePCnt].value = "0.00";
                            patNo = (formobj.patNo[bgtSectionTypePCnt].value).toFixed(2);
                        }
                        formobj.patNo.value = Math.round((formobj.patNo.value *100)/100).toFixed(2);
                        subTotalPatResearchCost = subTotalResearchCost ;
                        subTotalPatAppIndResCost = subTotalAppIndResCost;
                        subTotalPatResIndNotApplied = subTotalResIndNotApplied;
                        subTotalPatAppIndTtlCost = subTotalAppIndTtlCost;
                        subTotalPatTtlIndNotApplied = subTotalTtlIndNotApplied;
                        subTotalPatTtlCost = subTotalTtlCost;
                        socSubTotalPatResearchCost = socSubTotalResearchCost ;
                        socSubTotalPatAppIndResCost = socSubTotalAppIndResCost;
                        socSubTotalPatResIndNotApplied = socSubTotalResIndNotApplied;
                        socSubTotalPatAppIndTtlCost = socSubTotalAppIndTtlCost;
                        socSubTotalPatTtlIndNotApplied = socSubTotalTtlIndNotApplied;
                        socSubTotalPatTtlCost = socSubTotalTtlCost;
                        nsocSubTotalPatResearchCost = nsocSubTotalResearchCost ;
                        nsocSubTotalPatAppIndResCost = nsocSubTotalAppIndResCost;
                        nsocSubTotalPatResIndNotApplied = nsocSubTotalResIndNotApplied;
                        nsocSubTotalPatAppIndTtlCost = nsocSubTotalAppIndTtlCost;
                        nsocSubTotalPatTtlIndNotApplied = nsocSubTotalTtlIndNotApplied;
                        nsocSubTotalPatTtlCost = nsocSubTotalTtlCost;
                        if (perPatientSectionCnt==1)
                        {
                            formobj.socSubTotalPatResearchCost.value = (Math.round(socSubTotalPatResearchCost*100)/100).toFixed(2) ;
                            formobj.socSubTotalPatTtlCost.value = (Math.round(socSubTotalPatTtlCost*100)/100).toFixed(2) ;
                            formobj.nsocSubTotalPatResearchCost.value = (Math.round(nsocSubTotalPatResearchCost*100)/100).toFixed(2) ;
                            formobj.nsocSubTotalPatTtlCost.value = (Math.round(nsocSubTotalPatTtlCost*100)/100).toFixed(2) ;
                              // GM0717 changed for CL8734 
                            //formobj.nsocSubTotalSponsorAmountCost.value = (Math.round(nsocSubTotalSponsorAmount*100)/100).toFixed(2) ; 
                        }
                        else
                        {
                            formobj.socSubTotalPatResearchCost[bgtSectionTypePCnt].value = (Math.round(socSubTotalPatResearchCost*100)/100).toFixed(2);
                            formobj.socSubTotalPatTtlCost[bgtSectionTypePCnt].value = (Math.round(socSubTotalPatTtlCost*100)/100).toFixed(2);
                            formobj.nsocSubTotalPatResearchCost[bgtSectionTypePCnt].value = (Math.round(nsocSubTotalPatResearchCost*100)/100).toFixed(2);
                            formobj.nsocSubTotalPatTtlCost[bgtSectionTypePCnt].value = (Math.round(nsocSubTotalPatTtlCost*100)/100).toFixed(2);
                        }
                        totalResearchCost = subTotalPatResearchCost;
                        totalAppIndResCost = subTotalPatAppIndResCost;
                        totalIndResNotApplied = subTotalPatResIndNotApplied;
                        totalAppIndTtlCost = subTotalPatAppIndTtlCost;
                        totalIndTtlNotApplied = subTotalPatTtlIndNotApplied;
                        totalTtlCost = subTotalPatTtlCost;
                        socTotalResearchCost = socSubTotalPatResearchCost;
                        socTotalAppIndResCost = socSubTotalPatAppIndResCost;
                        socTotalIndResNotApplied = socSubTotalPatResIndNotApplied;
                        socTotalAppIndTtlCost = socSubTotalPatAppIndTtlCost;
                        socTotalIndTtlNotApplied = socSubTotalPatTtlIndNotApplied;
                        socTotalTtlCost = socSubTotalPatTtlCost;
                        nsocTotalResearchCost = nsocSubTotalPatResearchCost;
                        nsocTotalAppIndResCost = nsocSubTotalPatAppIndResCost;
                        nsocTotalIndResNotApplied = nsocSubTotalPatResIndNotApplied;
                        nsocTotalAppIndTtlCost = nsocSubTotalPatAppIndTtlCost;
                        nsocTotalIndTtlNotApplied = nsocSubTotalPatTtlIndNotApplied;
                        nsocTotalTtlCost = nsocSubTotalPatTtlCost;
                    }
                    else
                    {
                        totalResearchCost = subTotalResearchCost;
                        totalAppIndResCost = subTotalAppIndResCost;
                        totalResIndNotApplied = subTotalResIndNotApplied;
                        totalAppIndTtlCost = subTotalAppIndTtlCost;
                        totalTtlIndNotApplied = subTotalTtlIndNotApplied;
                        totalTtlCost = subTotalTtlCost;
                        socTotalResearchCost = socSubTotalResearchCost;
                        socTotalAppIndResCost = socSubTotalAppIndResCost;
                        socTotalResIndNotApplied = socSubTotalResIndNotApplied;
                        socTotalAppIndTtlCost = socSubTotalAppIndTtlCost;
                        socTotalTtlIndNotApplied = socSubTotalTtlIndNotApplied;
                        socTotalTtlCost = socSubTotalTtlCost;
                        nsocTotalResearchCost = nsocSubTotalResearchCost;
                        nsocTotalAppIndResCost = nsocSubTotalAppIndResCost;
                        nsocTotalResIndNotApplied = nsocSubTotalResIndNotApplied;
                        nsocTotalAppIndTtlCost = nsocSubTotalAppIndTtlCost;
                        nsocTotalTtlIndNotApplied = nsocSubTotalTtlIndNotApplied;
                        nsocTotalTtlCost = nsocSubTotalTtlCost;
                    }
                }
                if(bgtSectionType == "O")
                {
                    formobj.socSubTotalResearchCost.value = (Math.round(socSubTotalResearchCost*100)/100).toFixed(2);
                    formobj.socSubTotalTtlCost.value = (Math.round(socSubTotalTtlCost*100)/100).toFixed(2);
                    formobj.nsocSubTotalResearchCost.value = (Math.round(nsocSubTotalResearchCost*100)/100).toFixed(2);
                    formobj.nsocSubTotalTtlCost.value = (Math.round(nsocSubTotalTtlCost*100)/100).toFixed(2);
                }
            }
            
            
            	
            //added by IA 11.03.2006	
	    	if ( budgetTemplate == "C")
			{	
				//		    	formobj.socSubTotalSponsorAmountCost.value = (Math.round(socSubTotalSponsorAmount*100)/100).toFixed(2);
				//	            formobj.socSubTotalVariance.value = (Math.round(socSubTotalVariance*100)/100).toFixed(2);
				//	            formobj.nsocSubTotalSponsorAmountCost.value = (Math.round(nsocSubTotalSponsorAmount*100)/100).toFixed(2);
				//	            formobj.nsocSubTotalVariance.value = (Math.round(nsocSubTotalVariance*100)/100).toFixed(2);
				
				socTotalSponsorAmount = socSubTotalSponsorAmount;
				nsocTotalSponsorAmount = nsocSubTotalSponsorAmount;
				socTotalVariance = socSubTotalVariance;
				nsocTotalVariance = nsocSubTotalVariance;
				 
				
				formobj.socSubTotalSponsorAmountCost.value = (Math.round(socSubTotalSponsorAmount*100)/100).toFixed(2);
				formobj.nsocSubTotalSponsorAmountCost.value = (Math.round(nsocSubTotalSponsorAmount*100)/100).toFixed(2);
				 
				formobj.nsocSubTotalVariance.value = (Math.round((parseFloat(formobj.nsocSubTotalSponsorAmountCost.value) - parseFloat(nsocTotalTtlCost))*100)/100).toFixed(2);
				
				formobj.socSubTotalVariance.value = (Math.round((parseFloat(formobj.socSubTotalSponsorAmountCost.value) - parseFloat(socTotalTtlCost))*100)/100).toFixed(2);
				
				formobj.nsocTotalSponsorAmount.value = (Math.round(nsocTotalSponsorAmount*100)/100).toFixed(2);
				formobj.socTotalSponsorAmount.value = (Math.round(socTotalSponsorAmount*100)/100).toFixed(2);
				
			
				
				
			 }		
	 			//end added
	 			
	            formobj.totalSalResCost.value = (parseFloat(salResTotal)).toFixed(2);
	            formobj.totalSalTtlCost.value = (parseFloat(salTtlTotal)).toFixed(2);
	            formobj.totalFringeResCost.value = (parseFloat(fringeResTotal)).toFixed(2);
	            formobj.totalFringeTtlCost.value = (parseFloat(fringeTtlTotal)).toFixed(2);
	            formobj.totalDiscResCost.value = (parseFloat(discResTotal)).toFixed(2);
	            formobj.totalDiscTtlCost.value = (parseFloat(discTtlTotal)).toFixed(2);
	            formobj.totalIndirectsResCost.value = (parseFloat(indResTotal)).toFixed(2);
	            formobj.totalIndirectsTtlCost.value = (parseFloat(indTtlTotal)).toFixed(2);
	       
	        
        }
        else
        {//if there is more than one section
            sectionRowsIndex = -1;
            var patNo;
            var sectionCount=formobj.sectionCount.value;
            var arrayOfBgtSectionType = document.getElementsByName("bgtSectionTypeRow");
            var arrayOfBgtSectionTypePCnt= document.getElementsByName("bgtSectionTypePCnt");
            var arrayOfPatNo=document.getElementsByName("patNo");
            var arrayOfSectionRowsIndex=document.getElementsByName("sectionRowsIndex");
            var arrayOfSocSubTtlResCost=document.getElementsByName("socSubTotalResearchCost");
            var arrayOfSocSubTtlTtlCost=document.getElementsByName("socSubTotalTtlCost");
            var arrayOfNSocSubTtlResCost=document.getElementsByName("nsocSubTotalResearchCost");
            var arrayOfNSocSubTtlTtlCost=document.getElementsByName("nsocSubTotalTtlCost");
            var arrayOfSocSubTotalPatResCost = document.getElementsByName("socSubTotalPatResearchCost");
            var arrayOfSocSubTotalPatTtlCost = document.getElementsByName("socSubTotalPatTtlCost");
            var arrayOfNSocSubTotalPatResCost = document.getElementsByName("nsocSubTotalPatResearchCost");
            var arrayOfNSocSubTotalPatTtlCost = document.getElementsByName("nsocSubTotalPatTtlCost");
            var arrayOfNSocSubTotalSponsorAmount = document.getElementsByName("nsocSubTotalSponsorAmountCost");
            var arrayOfNSocSubTotalVariance = document.getElementsByName("nsocSubTotalVariance");
            //Added by IA 11.03.2006
            var arrayOfSocSubTotalSponsorAmount = document.getElementsByName("socSubTotalSponsorAmountCost");
            var arrayOfSocSubTotalVariance = document.getElementsByName("socSubTotalVariance");
            //end added
            //for go to every section
            for(i=0;i<arrayOfBgtSectionType.length;i++)
            {
            //	alert("enter to more than section");
                bgtSectionType = arrayOfBgtSectionType[i].value;
                bgtSectionTypePCnt = arrayOfBgtSectionTypePCnt[i].value;
                if(bgtSectionType== "P" )
                { 
                	
 
	                
                	
                	
                	
                    if(perPatientSectionCnt == 1)
                    {
                    	
                    	if (bgtSectionTypePCntTotal == "1" && bgtSectionTypeOCntTotal == "0" )
					
						{	patNo = formobj.patNo.value;
					
						}
		            	else
						{	
					
						var patNoTemp1 = document.getElementsByName("patNo");
						patNo = patNoTemp1[i].value;
						}	
                       
        //               alert("i am here in more sections" + patNo);
                        patNo = (Math.round(patNo * 100)/100).toFixed(2);
                        formobj.patNo.value = patNo;
                    }
                    else
                    { 
                        if(arrayOfPatNo[i].value == null || arrayOfPatNo[i].value == "")
                        {
                            arrayOfPatNo[i].value = "0.00";
                        }
                        patNo = (Math.round(formobj.patNo[i].value* 100)/100).toFixed(2);
                        arrayOfPatNo[i].value = patNo;
                    }
                }
                prevSectionRowsIndex = parseInt(sectionRowsIndex) + 1;
                sectionRowsIndex = arrayOfSectionRowsIndex[i].value;
                subTotalResearchCost = 0;
                subTotalAppIndResCost = 0;
                subTotalResIndNotApplied = 0 ;
                subTotalAppIndTtlCost = 0;
                subTotalTtlIndNotApplied = 0 ;
                subTotalTtlCost = 0;
                subTotalPatResearchCost = 0;
                subTotalPatAppIndResCost = 0;
                subTotalPatResIndNotApplied = 0;
                subTotalPatAppIndTtlCost = 0;
                subTotalPatTtlIndNotApplied = 0;
                subTotalPatTtlCost = 0;
                socSubTotalResearchCost = 0;
                socSubTotalAppIndResCost = 0;
                socSubTotalResIndNotApplied = 0 ;
                socSubTotalAppIndTtlCost = 0;
                socSubTotalTtlIndNotApplied = 0 ;
                socSubTotalTtlCost = 0;
                socSubTotalPatResearchCost = 0;
                socSubTotalPatAppIndResCost = 0;
                socSubTotalPatResIndNotApplied = 0;
                socSubTotalPatAppIndTtlCost = 0;
                socSubTotalPatTtlIndNotApplied = 0;
                socSubTotalPatTtlCost = 0;
                nsocSubTotalResearchCost = 0;
                nsocSubTotalAppIndResCost = 0;
                nsocSubTotalResIndNotApplied = 0 ;
                nsocSubTotalAppIndTtlCost = 0;
                nsocSubTotalTtlIndNotApplied = 0 ;
                nsocSubTotalTtlCost = 0;
                nsocSubTotalPatResearchCost = 0;
                nsocSubTotalPatAppIndResCost = 0;
                nsocSubTotalPatResIndNotApplied = 0;
                nsocSubTotalPatAppIndTtlCost = 0;
                nsocSubTotalPatTtlIndNotApplied = 0;
                nsocSubTotalPatTtlCost = 0;
                //Added by IA 11.03.2006
                socSubTotalSponsorAmount = 0;
                socSubTotalVariance = 0;
                nsocSubTotalSponsorAmount = 0;
                nsocSubTotalVariance = 0;
                //end added


//++++++++++++++

                for(j=prevSectionRowsIndex;j<=sectionRowsIndex;j++)
                {


                    var cmbCtgryValue=formobj.cmbCtgry[j].value;
                    var stdCareCostChecked=formobj.stdCareCost[j].checked;
                    var researchCostValue=parseFloat(formobj.researchCost[j].value);



					//Added by IA 11.03.2006
//			        if ( budgetTemplate == "C")
//					{
//                    	var sponsorAmountValue = formobj.sponsorAmount[j].value;
//                    }
                    //end added
//                    if(bgtSectionType== "P" )
//                    {
//                        formobj.TtlCost[j].value = (parseFloat(patNo) * parseFloat(formobj.researchCost[j].value)).toFixed(2);
//                    }
//                    else if(bgtSectionType == "O")
//                    {
//                        formobj.TtlCost[j].value = (Math.round((parseFloat(formobj.unitCost[j].value) * parseFloat(formobj.noUnits[j].value))*100)/100).toFixed(2);
//                    }
                    
                    switch(bgtSectionType)
					{
                    case "P":
                    	formobj.TtlCost[j].value = (parseFloat(patNo) * parseFloat(formobj.researchCost[j].value)).toFixed(2);
                    	break;
                    case "O":
                        formobj.TtlCost[j].value = (Math.round((parseFloat(formobj.unitCost[j].value) * parseFloat(formobj.noUnits[j].value))*100)/100).toFixed(2);
                        break;
                	}
                    var TtlCostValue=parseFloat(formobj.TtlCost[j].value);
                    researchFringeBenefit = 0;
                    researchDiscount = 0;
                    ttlFringeBenefit = 0;
                    ttlDiscount = 0;
                    resInd = 0 ;
                    ttlInd = 0;
                    stdCareDiscount = 0;
                    
                    //Added by IA 11.03.2006
                    if ( budgetTemplate == "C")
                    {
                    	var sponsorAmountValue = formobj.sponsorAmount[j].value;
                    	if(stdCareCostChecked && excludeSOCApplyChecked == true)
                    	{
							formobj.variance[j].value = (Math.round((formobj.sponsorAmount[j].value)*100)/100).toFixed(2)
                        	var varianceValue = formobj.variance[j].value;
							formobj.researchCost[j].value = "0.00";
                        	formobj.TtlCost[j].value = "0.00";

                            	socSubTotalSponsorAmount = socSubTotalSponsorAmount + parseFloat(sponsorAmountValue);
                            	socSubTotalVariance = socSubTotalVariance + parseFloat(varianceValue);
                        }
                        else
                        {
							formobj.variance[j].value = (Math.round((formobj.sponsorAmount[j].value -  formobj.TtlCost[j].value)*100)/100).toFixed(2)
                        	var varianceValue = formobj.variance[j].value;
                        

                        }
                        
                        if(!stdCareCostChecked )
                        {
                        	nsocSubTotalSponsorAmount = parseFloat(nsocSubTotalSponsorAmount) + parseFloat(sponsorAmountValue);
                     	    //alert ("sponsorAmount**" + j + "***" + sponsorAmountValue + "**AddedAcomulativ***"+nsocSubTotalSponsorAmount);
                       		nsocSubTotalVariance = nsocSubTotalVariance + parseFloat(varianceValue);
                       	}	
                        
                        
                    }
                    //end added

var appIndirectsChecked=formobj.appIndirects[j].checked;

var condition_1 = ((stdCareCostChecked && !excludeSOCApplyChecked == true) || (!stdCareCostChecked)) ? true : false;
                
var condition_2 = (appIndirectsChecked == true && formobj.sponsorOHeadApply.checked == true) ? true : false;

sponsorOHead = parseFloat(sponsorOHead);
    
if (condition_1)
{
	if(cmbCtgryValue==perCodeId)
	{
        salResTotal = salResTotal + researchCostValue;
		salTtlTotal = salTtlTotal + TtlCostValue;
		if (formobj.fringeBenefitApply.checked == true)
		{
            researchFringeBenefit = ((researchCostValue * parseFloat(fringeBenefit)) / 100.0);
            ttlFringeBenefit = ((TtlCostValue * parseFloat(fringeBenefit)) / 100.0);
			fringeResTotal = fringeResTotal + researchFringeBenefit;
            fringeTtlTotal = fringeTtlTotal + ttlFringeBenefit;
		}
	}
	if ((costDiscountApply==true) && (! isNaN(costDiscount)) && (formobj.discount[j].checked == true))
	{
		researchDiscount = (((researchCostValue + researchFringeBenefit) * parseFloat(costDiscount)) / 100.0);
        ttlDiscount = (((TtlCostValue + ttlFringeBenefit) * parseFloat(costDiscount)) / 100.0);
        discResTotal = discResTotal + researchDiscount;
        discTtlTotal = discTtlTotal + ttlDiscount;
	}
	if(condition_2)
	{
		subTotalResearchCost = subTotalResearchCost + (((researchCostValue + researchFringeBenefit - researchDiscount) * sponsorOHead) / 100.0);
		subTotalTtlCost = subTotalTtlCost + (((TtlCostValue + ttlFringeBenefit - ttlDiscount) * sponsorOHead) / 100.0);
	}
}                    

subTotalResearchCost = subTotalResearchCost + researchCostValue + researchFringeBenefit - researchDiscount;
subTotalTtlCost = subTotalTtlCost + TtlCostValue + ttlFringeBenefit - ttlDiscount;


                    
/*
                    if(cmbCtgryValue==perCodeId)
                    {
                        if ((stdCareCostChecked && !excludeSOCApplyChecked == true) || (!stdCareCostChecked))
                        {
                            salResTotal = salResTotal + parseFloat(researchCostValue);
salTtlTotal = salTtlTotal + parseFloat(TtlCostValue);
                        }
                    }
*/
/*
                    if(cmbCtgryValue==perCodeId)
                    {
                        if ((stdCareCostChecked && !excludeSOCApplyChecked == true) || (!stdCareCostChecked))
                        {
                            salTtlTotal = salTtlTotal + parseFloat(TtlCostValue);
                        }
                    }
*/                    
/*
                    if ((formobj.fringeBenefitApply.checked == true) && (cmbCtgryValue==perCodeId) )
                    {
                        if ((stdCareCostChecked && !excludeSOCApplyChecked == true) || (!stdCareCostChecked))
                        {
                            researchFringeBenefit = ((parseFloat(researchCostValue) * parseFloat(fringeBenefit)) / 100.0);
                            ttlFringeBenefit = ((parseFloat(TtlCostValue) * parseFloat(fringeBenefit)) / 100.0);
                        }
                        fringeResTotal = fringeResTotal + researchFringeBenefit;
                        fringeTtlTotal = fringeTtlTotal + ttlFringeBenefit;
                    }
*/                    
/*
                    if ((costDiscountApply==true) && (costDiscount>0) && (formobj.discount[j].checked == true))
                    {
                        if ((stdCareCostChecked && !excludeSOCApplyChecked == true) || (!stdCareCostChecked))
                        {
                            researchDiscount = (((parseFloat(researchCostValue) + researchFringeBenefit) * parseFloat(costDiscount)) / 100.0);
                            ttlDiscount = (((parseFloat(TtlCostValue) + ttlFringeBenefit) * parseFloat(costDiscount)) / 100.0);
                        }
                        discResTotal = discResTotal + researchDiscount;
                        discTtlTotal = discTtlTotal + ttlDiscount;
                    }
*/                    
//                    subTotalResearchCost = subTotalResearchCost + parseFloat(researchCostValue) + researchFringeBenefit - researchDiscount;
//                    subTotalTtlCost = subTotalTtlCost + parseFloat(TtlCostValue) + ttlFringeBenefit - ttlDiscount;
//                    var appIndirectsChecked=formobj.appIndirects[j].checked;
/*
                    if(appIndirectsChecked == true && formobj.sponsorOHeadApply.checked == true)
                    {
                        if ((stdCareCostChecked && !excludeSOCApplyChecked == true) || (!stdCareCostChecked))
                        {
                            subTotalResearchCost = subTotalResearchCost + (((parseFloat(researchCostValue) + researchFringeBenefit - researchDiscount) * parseFloat(sponsorOHead)) / 100.0);
                            subTotalTtlCost = subTotalTtlCost + (((parseFloat(TtlCostValue) + ttlFringeBenefit - ttlDiscount) * parseFloat(sponsorOHead)) / 100.0);
                        }
                    }
*/                    
                    if (stdCareCostChecked)
                    {
                        if(!excludeSOCApplyChecked == true)
                        {
                            socSubTotalResearchCost = socSubTotalResearchCost + researchCostValue + researchFringeBenefit - researchDiscount;
                            socSubTotalTtlCost = socSubTotalTtlCost + TtlCostValue + ttlFringeBenefit - ttlDiscount;
                            if(condition_2)
                            {
                                resInd = (((researchCostValue + researchFringeBenefit - researchDiscount) * sponsorOHead) / 100.0);
                                socSubTotalResearchCost = socSubTotalResearchCost + resInd;
                                ttlInd = (((TtlCostValue + ttlFringeBenefit - ttlDiscount) * sponsorOHead) / 100.0);
                                socSubTotalTtlCost = socSubTotalTtlCost + ttlInd;
                            }
    
                        }
                        
                        //Added by IA 11.03.2006
                        //uncommented by sonia 07/23/08 - it was not setting SOC subtotals
					        if ( budgetTemplate == "C")
							{
								if(stdCareCostChecked && excludeSOCApplyChecked == false) //added by sonia to avoid double calc
                    				{
	                            		socSubTotalSponsorAmount = socSubTotalSponsorAmount + parseFloat(sponsorAmountValue);
	                            		socSubTotalVariance = socSubTotalVariance + parseFloat(varianceValue);
                            		}
                            	}
                       //End added
                        
                    }
                    else
                    {
                        nsocSubTotalResearchCost = nsocSubTotalResearchCost + researchCostValue + researchFringeBenefit - researchDiscount;
                        nsocSubTotalTtlCost = nsocSubTotalTtlCost + TtlCostValue + ttlFringeBenefit - ttlDiscount;
                        //Added by IA 11.03.2006
//				        if ( budgetTemplate == "C")
//						{
//                        	nsocSubTotalSponsorAmount = nsocSubTotalSponsorAmount + parseFloat(sponsorAmountValue);
                     	    //alert ("sponsorAmount**" + j + "***" + sponsorAmountValue + "**AddedAcomulativ***"+nsocSubTotalSponsorAmount);
//                       		nsocSubTotalVariance = nsocSubTotalVariance + parseFloat(varianceValue);
//                        }
                        //end added
                        if(condition_2)
                        {
                            resInd = (((researchCostValue + researchFringeBenefit - researchDiscount) * sponsorOHead) / 100.0);
                            nsocSubTotalResearchCost = nsocSubTotalResearchCost + resInd;
                            ttlInd = (((TtlCostValue + ttlFringeBenefit - ttlDiscount) * sponsorOHead) / 100.0);
                            nsocSubTotalTtlCost = nsocSubTotalTtlCost + ttlInd;
                        }
                    }
  
                    
                    indResTotal = indResTotal + resInd;
                    indTtlTotal = indTtlTotal +ttlInd;
                    if(appIndirectsChecked == true)
                    formobj.hidAppIndirects[j].value = "1";
                    else
                    formobj.hidAppIndirects[j].value = "0";
                    if(condition_2)
                    {
                        if (condition_1)
                        {
                            subTotalAppIndResCost = subTotalAppIndResCost + researchCostValue + researchFringeBenefit - researchDiscount;
                            subTotalAppIndTtlCost = subTotalAppIndTtlCost + TtlCostValue + ttlFringeBenefit - ttlDiscount;
                                subTotalAppIndResCost = subTotalAppIndResCost + (((researchCostValue + researchFringeBenefit - researchDiscount) * sponsorOHead) / 100.0);
                                subTotalAppIndTtlCost = subTotalAppIndTtlCost + (((TtlCostValue + ttlFringeBenefit - ttlDiscount) * sponsorOHead) / 100.0);
                        }
//                        if(appIndirectsChecked == true && formobj.sponsorOHeadApply.checked == true)
//                        {
//                            if ((stdCareCostChecked && !excludeSOCApplyChecked == true) || (!stdCareCostChecked))
//                            {
//                                subTotalAppIndResCost = subTotalAppIndResCost + (((parseFloat(researchCostValue) + researchFringeBenefit - researchDiscount) * parseFloat(sponsorOHead)) / 100.0);
//                                subTotalAppIndTtlCost = subTotalAppIndTtlCost + (((parseFloat(TtlCostValue) + ttlFringeBenefit - ttlDiscount) * parseFloat(sponsorOHead)) / 100.0);
//                            }
//                        }
                        if (stdCareCostChecked)
                        {
                            if(!excludeSOCApplyChecked == true)
                            {
                                socSubTotalAppIndResCost = socSubTotalAppIndResCost + researchCostValue + researchFringeBenefit - researchDiscount;
                                socSubTotalAppIndTtlCost = socSubTotalAppIndTtlCost + TtlCostValue + ttlFringeBenefit - ttlDiscount;
//                                if(appIndirectsChecked == true && formobj.sponsorOHeadApply.checked == true)
//                                {
                                    socSubTotalAppIndResCost = socSubTotalAppIndResCost + (((researchCostValue + researchFringeBenefit - researchDiscount) * sponsorOHead) / 100.0);
                                    socSubTotalAppIndTtlCost = socSubTotalAppIndTtlCost + (((TtlCostValue + ttlFringeBenefit - ttlDiscount) * sponsorOHead) / 100.0);
//                                }
                            }
                        }
                        else
                        {
                            nsocSubTotalAppIndResCost = nsocSubTotalAppIndResCost + researchCostValue + researchFringeBenefit - researchDiscount;
                            nsocSubTotalAppIndTtlCost = nsocSubTotalAppIndTtlCost + TtlCostValue + ttlFringeBenefit - ttlDiscount;
//                            if(appIndirectsChecked == true && formobj.sponsorOHeadApply.checked == true)
//                            {
                                nsocSubTotalAppIndResCost = nsocSubTotalAppIndResCost + (((researchCostValue + researchFringeBenefit - researchDiscount) * sponsorOHead) / 100.0);
                                nsocSubTotalAppIndTtlCost = nsocSubTotalAppIndTtlCost + (((TtlCostValue + ttlFringeBenefit - ttlDiscount) * sponsorOHead) / 100.0);
//                            }
                        }
                    }
                    else
                    {
                        subTotalResIndNotApplied = subTotalResIndNotApplied + researchCostValue + researchFringeBenefit - researchDiscount;
                        subTotalTtlIndNotApplied = subTotalTtlIndNotApplied + TtlCostValue + ttlFringeBenefit - ttlDiscount;
                        if (stdCareCostChecked)
                        {
                            if(!excludeSOCApplyChecked == true)
                            {
                                socSubTotalResIndNotApplied = socSubTotalResIndNotApplied + researchCostValue + researchFringeBenefit - researchDiscount;
                                socSubTotalTtlIndNotApplied = socSubTotalTtlIndNotApplied + TtlCostValue + ttlFringeBenefit - ttlDiscount;
                            }
                        }
                        else
                        {
                            nsocSubTotalResIndNotApplied = nsocSubTotalResIndNotApplied + researchCostValue + researchFringeBenefit - researchDiscount;
                            nsocSubTotalTtlIndNotApplied = nsocSubTotalTtlIndNotApplied + TtlCostValue + ttlFringeBenefit - ttlDiscount;
                        }
                    }
/*                    
                    if(stdCareCostChecked && excludeSOCApplyChecked == true)
                    {
                        formobj.researchCost[j].value = "0.00";
                        formobj.TtlCost[j].value = "0.00";
                    }
*/                    

                }
				
				
//++++++++++++++
				
                if(bgtSectionType == "O")
                {/*
                    if(oneSectionCnt == 1 && i==0 && perPatientSectionCnt == 1)
                    {
                        formobj.socSubTotalResearchCost.value = (Math.round(socSubTotalResearchCost*100)/100).toFixed(2);
                        formobj.socSubTotalTtlCost.value = (Math.round(socSubTotalTtlCost*100)/100).toFixed(2);
                        formobj.nsocSubTotalResearchCost.value = (Math.round(nsocSubTotalResearchCost*100)/100).toFixed(2);
                        formobj.nsocSubTotalTtlCost.value = (Math.round(nsocSubTotalTtlCost*100)/100).toFixed(2);
                        formobj.socSubTotalSponsorAmountCost.value = (Math.round(socSubTotalSponsorAmount*100)/100).toFixed(2);
                        formobj.socSubTotalVariance.value = (Math.round(socSubTotalVariance*100)/100).toFixed(2);
                      //  alert("enterto here" + nsocSubTotalSponsorAmount);
                        formobj.nsocSubTotalSponsorAmountCost.value = (Math.round(nsocSubTotalSponsorAmount*100)/100).toFixed(2);
 					//	alert("enterto here" + formobj.nsocSubTotalSponsorAmountCost.value);
                        formobj.nsocSubTotalVariance.value = (Math.round(nsocSubTotalVariance*100)/100).toFixed(2);
                     //   alert ("SubTotalNsocSponsorAmount**OneTimeFee**the first***" + i + "***" + nsocSubTotalSponsorAmount.value);
                    }
                    else
                    {*/
                        arrayOfSocSubTtlResCost[i].value = (Math.round(socSubTotalResearchCost*100)/100).toFixed(2);
                        arrayOfSocSubTtlTtlCost[i].value = (Math.round(socSubTotalTtlCost*100)/100).toFixed(2);
                        arrayOfNSocSubTtlResCost[i].value = (Math.round(nsocSubTotalResearchCost*100)/100).toFixed(2);
                        arrayOfNSocSubTtlTtlCost[i].value = (Math.round(nsocSubTotalTtlCost*100)/100).toFixed(2);
       			        if ( budgetTemplate == "C")
						{
		                    arrayOfNSocSubTotalSponsorAmount[i].value = (Math.round(nsocSubTotalSponsorAmount*100)/100).toFixed(2);
	                        //arrayOfNSocSubTotalVariance[i].value = (Math.round(nsocSubTotalVariance*100)/100).toFixed(2);
							
							arrayOfNSocSubTotalVariance[i].value = (Math.round((parseFloat(formobj.nsocSubTotalSponsorAmountCost[i].value) - parseFloat(nsocSubTotalTtlCost))*100)/100).toFixed(2);
							
	                        arrayOfSocSubTotalSponsorAmount[i].value = (Math.round(socSubTotalSponsorAmount*100)/100).toFixed(2);
	                        //arrayOfSocSubTotalVariance[i].value = (Math.round(socSubTotalVariance*100)/100).toFixed(2);
							
							arrayOfSocSubTotalVariance[i].value = (Math.round((parseFloat(formobj.socSubTotalSponsorAmountCost[i].value) - parseFloat(socSubTotalTtlCost))*100)/100).toFixed(2);
						}
		
                   // }
                }
                
                //alert("enter to more than section - middle");
                if (bgtSectionType=="P" )
                {
                    bgtSectionTypePCnt = arrayOfBgtSectionTypePCnt[i].value;
                    if(perPatientSectionCnt == 1 )
                    {
                    }
                    else
                    {
                        if(isNaN(arrayOfPatNo[i].value) == true)
                        {
                           /*alert("Please enter a valid Number of Patients value.");*****/
						   alert(Vldt_NumOfPatValue);
                            formobj.patNo[i].focus();
                            return false;
                        }
                        if(maxDigits(arrayOfPatNo[i].value)==false)
                        {
                           /* alert("Integer part cannot exceed more than 8 digits.");*****/
				alert(IntegerPartCnt_Exceed);
                            formobj.patNo[i].focus();
                            return false;
                        }
                        if(arrayOfPatNo[i].value == null || arrayOfPatNo[i].value == "")
                        arrayOfPatNo[i].value = "0.00";
                        patNo = (parseFloat(arrayOfPatNo[i].value)).toFixed(2);
                        arrayOfPatNo[i].value = patNo;
                    }
                    subTotalPatResearchCost = subTotalResearchCost ;
                    subTotalPatAppIndResCost = subTotalAppIndResCost;
                    subTotalPatResIndNotApplied = subTotalResIndNotApplied;
                    subTotalPatAppIndTtlCost = subTotalAppIndTtlCost;
                    subTotalPatTtlIndNotApplied = subTotalTtlIndNotApplied;
                    subTotalPatTtlCost = subTotalTtlCost ;
                    socSubTotalPatResearchCost = socSubTotalResearchCost ;
                    socSubTotalPatAppIndResCost = socSubTotalAppIndResCost;
                    socSubTotalPatResIndNotApplied = socSubTotalResIndNotApplied;
                    socSubTotalPatAppIndTtlCost = socSubTotalAppIndTtlCost;
                    socSubTotalPatTtlIndNotApplied = socSubTotalTtlIndNotApplied;
                    socSubTotalPatTtlCost = socSubTotalTtlCost ;
                    nsocSubTotalPatResearchCost = nsocSubTotalResearchCost ;
                    nsocSubTotalPatAppIndResCost = nsocSubTotalAppIndResCost;
                    nsocSubTotalPatResIndNotApplied = nsocSubTotalResIndNotApplied;
                    nsocSubTotalPatAppIndTtlCost = nsocSubTotalAppIndTtlCost;
                    nsocSubTotalPatTtlIndNotApplied = nsocSubTotalTtlIndNotApplied;
                    nsocSubTotalPatTtlCost = nsocSubTotalTtlCost ;
          /*        if (perPatientSectionCnt==1 && i==0 )
                    {
                        formobj.socSubTotalPatResearchCost.value = (Math.round(socSubTotalPatResearchCost*100)/100).toFixed(2);
                        formobj.socSubTotalPatTtlCost.value = (Math.round(socSubTotalPatTtlCost*100)/100).toFixed(2);
                        formobj.nsocSubTotalPatResearchCost.value = (Math.round(nsocSubTotalPatResearchCost*100)/100).toFixed(2);
                        formobj.nsocSubTotalPatTtlCost.value = (Math.round(nsocSubTotalPatTtlCost*100)/100).toFixed(2);
                        formobj.socSubTotalSponsorAmount.value = (Math.round(socSubTotalSponsorAmount*100)/100).toFixed(2);
                        formobj.socSubTotalVariance.value = (Math.round(socSubTotalVariance*100)/100).toFixed(2);
                        formobj.nsocSubTotalSponsorAmount.value = (Math.round(nsocSubTotalSponsorAmount*100)/100).toFixed(2);
                        formobj.nsocSubTotalVariance.value = (Math.round(nsocSubTotalVariance*100)/100).toFixed(2);
                        //alert ("SubTotalNsocSponsorAmount**PatientFee**the first***" + i + "***" + nsocSubTotalSponsorAmount.value);
                    }
                    else
                    {*/
                    
                    arrayOfSocSubTotalPatResCost[i].value = (Math.round(socSubTotalPatResearchCost*100)/100).toFixed(2) ;
                    
                   
                    
                    
                    arrayOfSocSubTotalPatTtlCost[i].value = (Math.round(socSubTotalPatTtlCost*100)/100).toFixed(2) ;
                     
                    arrayOfNSocSubTotalPatResCost[i].value = (Math.round(nsocSubTotalPatResearchCost*100)/100).toFixed(2) ;
                    
                                      
                    arrayOfNSocSubTotalPatTtlCost[i].value = (Math.round(nsocSubTotalPatTtlCost*100)/100).toFixed(2) ;
					if ( budgetTemplate == "C")
					{

	                    arrayOfNSocSubTotalSponsorAmount[i].value = (Math.round(nsocSubTotalSponsorAmount*100)/100).toFixed(2);
	                    //arrayOfNSocSubTotalVariance[i].value = (Math.round(nsocSubTotalVariance*100)/100).toFixed(2);
	                    
						arrayOfNSocSubTotalVariance[i].value = (Math.round((parseFloat(formobj.nsocSubTotalSponsorAmountCost[i].value) - parseFloat(nsocSubTotalPatTtlCost))*100)/100).toFixed(2);
	                    arrayOfSocSubTotalSponsorAmount[i].value = (Math.round(socSubTotalSponsorAmount*100)/100).toFixed(2);
	                    //arrayOfSocSubTotalVariance[i].value = (Math.round(socSubTotalVariance*100)/100).toFixed(2);
						arrayOfSocSubTotalVariance[i].value = (Math.round((parseFloat(formobj.socSubTotalSponsorAmountCost[i].value) - parseFloat(socSubTotalPatTtlCost))*100)/100).toFixed(2);
					}
                  //  }
                    

                    totalResearchCost = totalResearchCost + subTotalPatResearchCost;
                    totalAppIndResCost = totalAppIndResCost + subTotalPatAppIndResCost;
                    totalResIndNotApplied = totalResIndNotApplied + subTotalPatResIndNotApplied;
                    totalAppIndTtlCost = totalAppIndTtlCost + subTotalPatAppIndTtlCost;
                    totalTtlIndNotApplied = totalTtlIndNotApplied + subTotalPatTtlIndNotApplied;
                    totalTtlCost = totalTtlCost + subTotalPatTtlCost;
                    socTotalResearchCost = socTotalResearchCost + socSubTotalPatResearchCost;
                    socTotalAppIndResCost = socTotalAppIndResCost + socSubTotalPatAppIndResCost;
                    socTotalResIndNotApplied = socTotalResIndNotApplied + socSubTotalPatResIndNotApplied;
                    socTotalAppIndTtlCost = socTotalAppIndTtlCost + socSubTotalPatAppIndTtlCost;
                    socTotalTtlIndNotApplied = socTotalTtlIndNotApplied + socSubTotalPatTtlIndNotApplied;
                    socTotalTtlCost = socTotalTtlCost + socSubTotalPatTtlCost;
                    nsocTotalResearchCost = nsocTotalResearchCost + nsocSubTotalPatResearchCost;
                    nsocTotalAppIndResCost = nsocTotalAppIndResCost + nsocSubTotalPatAppIndResCost;
                    nsocTotalResIndNotApplied = nsocTotalResIndNotApplied + nsocSubTotalPatResIndNotApplied;
                    nsocTotalAppIndTtlCost = nsocTotalAppIndTtlCost + nsocSubTotalPatAppIndTtlCost;
                    nsocTotalTtlIndNotApplied = nsocTotalTtlIndNotApplied + nsocSubTotalPatTtlIndNotApplied;
                    nsocTotalTtlCost = nsocTotalTtlCost + nsocSubTotalPatTtlCost;
                }
                else
                {
                    totalResearchCost = totalResearchCost + subTotalResearchCost;
                    totalAppIndResCost = totalAppIndResCost + subTotalAppIndResCost ;
                    totalResIndNotApplied = totalResIndNotApplied + subTotalResIndNotApplied;
                    totalAppIndTtlCost = totalAppIndTtlCost + subTotalAppIndTtlCost ;
                    totalTtlIndNotApplied = totalTtlIndNotApplied + subTotalTtlIndNotApplied;
                    totalTtlCost = totalTtlCost + subTotalTtlCost;
                    socTotalResearchCost = socTotalResearchCost + socSubTotalResearchCost;
                    socTotalAppIndResCost = socTotalAppIndResCost + socSubTotalAppIndResCost ;
                    socTotalResIndNotApplied = socTotalResIndNotApplied + socSubTotalResIndNotApplied;
                    socTotalAppIndTtlCost = socTotalAppIndTtlCost + socSubTotalAppIndTtlCost ;
                    socTotalTtlIndNotApplied = socTotalTtlIndNotApplied + socSubTotalTtlIndNotApplied;
                    socTotalTtlCost = socTotalTtlCost + socSubTotalTtlCost;
                    nsocTotalResearchCost = nsocTotalResearchCost + nsocSubTotalResearchCost;
                    nsocTotalAppIndResCost = nsocTotalAppIndResCost + nsocSubTotalAppIndResCost ;
                    nsocTotalResIndNotApplied = nsocTotalResIndNotApplied + nsocSubTotalResIndNotApplied;
                    nsocTotalAppIndTtlCost = nsocTotalAppIndTtlCost + nsocSubTotalAppIndTtlCost ;
                    nsocTotalTtlIndNotApplied = nsocTotalTtlIndNotApplied + nsocSubTotalTtlIndNotApplied;
                    nsocTotalTtlCost = nsocTotalTtlCost + nsocSubTotalTtlCost;
                }
                //Added by IA 11.03.2006 TotalFor Budget
                if ( budgetTemplate == "C")
				{
	   				nsocTotalSponsorAmount= nsocTotalSponsorAmount + nsocSubTotalSponsorAmount;
	   				nsocTotalVariance= nsocTotalVariance + nsocSubTotalVariance;
	   				socTotalSponsorAmount= socTotalSponsorAmount + socSubTotalSponsorAmount;
	   				socTotalVariance= socTotalVariance + socSubTotalVariance;
   				
   				}
            }
            formobj.totalSalResCost.value = (parseFloat(salResTotal)).toFixed(2);
            formobj.totalSalTtlCost.value = (parseFloat(salTtlTotal)).toFixed(2);
            formobj.totalFringeResCost.value = (parseFloat(fringeResTotal)).toFixed(2);
            formobj.totalFringeTtlCost.value = (parseFloat(fringeTtlTotal)).toFixed(2);
            formobj.totalDiscResCost.value = (parseFloat(discResTotal)).toFixed(2);
            formobj.totalDiscTtlCost.value = (parseFloat(discTtlTotal)).toFixed(2);
            formobj.totalIndirectsResCost.value = (parseFloat(indResTotal)).toFixed(2);
            formobj.totalIndirectsTtlCost.value = (parseFloat(indTtlTotal)).toFixed(2);
            
       }
    }
    formobj.fringeBenefit.value = (Math.round(formobj.fringeBenefit.value *100)/100).toFixed(2);
    formobj.sponsorOHead.value = (Math.round(formobj.sponsorOHead.value *100)/100).toFixed(2);
    formobj.costDiscount.value = (Math.round(formobj.costDiscount.value *100)/100).toFixed(2);
    if(formobj.sponsorOHeadApply.checked == true)
    {
        socTotalResearchCost = socTotalResIndNotApplied + socTotalAppIndResCost	;
        socTotalTtlCost = socTotalTtlIndNotApplied + socTotalAppIndTtlCost;
        nsocTotalResearchCost = nsocTotalResIndNotApplied + nsocTotalAppIndResCost;
        nsocTotalTtlCost = nsocTotalTtlIndNotApplied + nsocTotalAppIndTtlCost;
    }
    else
    {
        formobj.totalIndirectsResCost.value = "0.00";
        formobj.totalIndirectsTtlCost.value = "0.00";
    }
    totalResearchCost = socTotalResearchCost + nsocTotalResearchCost;
    totalTtlCost = socTotalTtlCost + nsocTotalTtlCost;
    formobj.totalResearchCost.value = (Math.round(totalResearchCost*100)/100).toFixed(2);
    formobj.totalTtlCost.value = (Math.round(totalTtlCost*100)/100).toFixed(2);
    formobj.socTotalResearchCost.value = (Math.round(socTotalResearchCost*100)/100).toFixed(2);
    formobj.socTotalTtlCost.value = (Math.round(socTotalTtlCost*100)/100).toFixed(2);
    formobj.nsocTotalResearchCost.value = (Math.round(nsocTotalResearchCost*100)/100).toFixed(2);
    formobj.nsocTotalTtlCost.value = (Math.round(nsocTotalTtlCost*100)/100).toFixed(2);
    
    //added by IA 11.03.2006 Add the total to the functions
    if ( budgetTemplate == "C")
	{
	    totalSponsorAmount = socTotalSponsorAmount + nsocTotalSponsorAmount;
	    totalVariance = socTotalVariance + nsocTotalVariance;
	    formobj.TotalSponsorAmount.value = (Math.round(totalSponsorAmount*100)/100).toFixed(2);
	    //formobj.TotalVariance.value = (Math.round(totalVariance*100)/100).toFixed(2);
	    formobj.socTotalSponsorAmount.value = (Math.round(socTotalSponsorAmount*100)/100).toFixed(2);
	    formobj.nsocTotalSponsorAmount.value = (Math.round(nsocTotalSponsorAmount*100)/100).toFixed(2);
	    //formobj.socTotalVariance.value = (Math.round(socTotalVariance*100)/100).toFixed(2);
	    formobj.nsocTotalVariance.value = (Math.round((parseFloat(formobj.nsocTotalSponsorAmount.value) - parseFloat(formobj.nsocTotalTtlCost.value) )*100)/100).toFixed(2);
		formobj.socTotalVariance.value = (Math.round((parseFloat(formobj.socTotalSponsorAmount.value) - parseFloat(formobj.socTotalTtlCost.value) )*100)/100).toFixed(2);
		formobj.TotalVariance.value = (Math.round((parseFloat(formobj.TotalSponsorAmount.value) - parseFloat(formobj.totalTtlCost.value) )*100)/100).toFixed(2);
		
	}
    //end added
	
}
function fnMessage()
{
    /*alert(" Freeze / Template Status cannot be modified")*****/
	alert(Freeze_CntModified)
}
// #5747 02/10/2011 @Ankit
function fnMessage(statusDesc)
{
	var paramArray = [statusDesc];
	alert(getLocalizedMessageString("M_StatCnt_BeModf", paramArray))/*alert(statusDesc+" Status cannot be modified")*****/
}

function openMile(formobj, pgRight)
{
    if(formobj.mode.value=='M')
    {
    	var budgetId=document.getElementById("budgetId").value;
    	var studyId=document.getElementById("budgetStudyId").value;
    	/*FIN-22375 Date :16 Aug 2012 By : Yogendra Pratap */
    	var budgetTemplate = document.getElementById("budgetTemplate").value
    	if(budgetTemplate=="null" || budgetTemplate==null)budgetTemplate="";
        //currentBgtcalId = formobj.currentBgtcalId.value;
        //studyWithSelectedBudgetProtocolID =formobj.studyWithSelectedBudgetProtocolID.value;
        
        //selectedCalendarStatus = formobj.selectedCalendarStatus.value;
        
        if(studyId=="null")
        {
            /*alert("Please attach a study before selecting protocol");*****/
            alert(AttachStd_BeforeSelPcol);
			return;
        }
        
        /*if (studyWithSelectedBudgetProtocolID != studyId)
        {
        	alert("Milestones for this Calendar cannot be created. The Selected Calendar is not associated with the Study linked with the budget");
        	return;
        }
        
		if (selectedCalendarStatus != "A")
        {
        	//KM-13Jan10-4518--reverted as per Bugzilla requirement change
			//alert("Milestones for this Calendar cannot be created. The Selected Calendar is not 'Active'");
			alert("The selected Calendar is not 'Active'. Please select a different Calendar or uncheck 'Visit'/'Event' options");
        	return;
        }
		*/
        
        windowName=window.open("createMultiMilestones.jsp?" + "budgetId=" + budgetId + "&studyId=" +studyId+ "&budgetTemplate=" + budgetTemplate ,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=1000,height=500");
        windowName.focus();
    }
    else
    {
        /* alert("Please save the budget first");*****/
         alert(Save_BgtFirst);
        return false;
    }
}

function getBudgetEventMouseOver(eventId, calendarType) {
	var studyId = (document.getElementById("budgetStudyId"))
		? document.getElementById("budgetStudyId").value
		: "";
	studyId = (!studyId)? "":studyId;
	studyId = (studyId == "null")? "":studyId;

	calendarType = (!calendarType)? "L":calendarType;
	calendarType = (calendarType == "null")? "":calendarType;

	var calledFrom = (calendarType == "")? "L":calendarType;
	fnGetEventMouseOver(eventId, "A", calledFrom, studyId);
}