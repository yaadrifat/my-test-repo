<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id="appendixB" scope="request" class="com.velos.eres.web.appendix.AppendixJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:include page="include.jsp" flush="true"/>
<% 
String eSign = request.getParameter("eSign");
HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))
{
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<% 	
//	String oldESign = (String) tSession.getValue("eSign");
//	if(!oldESign.equals(eSign)) {
%>
  
<%
//	} else {
 	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = null;
	usr = (String) tSession.getValue("userId"); 

	String type= request.getParameter("type");

	String url= request.getParameter("name");

	String desc = request.getParameter("desc");

	String pubFlag= request.getParameter("pubflag");

	String studyId=request.getParameter("study");
	String studyVerId=request.getParameter("studyVerId");	

	String tab = request.getParameter("selectedTab");
	String src = request.getParameter("srcmenu");	

	appendixB.setAppendixType(type);

	appendixB.setAppendixUrl_File(url);

	appendixB.setAppendixPubFlag(pubFlag);

	appendixB.setAppendixDescription(desc);

	appendixB.setAppendixStudy(studyId);
	appendixB.setAppendixStudyVer(studyVerId);	
	appendixB.setCreator(usr); 
	appendixB.setIpAdd(ipAdd);
 	appendixB.setAppendixDetails();
%>
	<br>
	<br>
	<br>
	<br>
	<br><p class = "sectionHeadings" align = center> <%=MC.M_Data_SvdSucc%><%-- Data was saved successfully*****--%> </p>
	<META HTTP-EQUIV=Refresh CONTENT="1; URL=appendixbrowser.jsp?selectedTab=<%=tab%>&srcmenu=<%=src%>&studyVerId=<%=studyVerId%>">

<%	
//}//end of if for eSign check
}//end of if body for session
else
{
%>
<jsp:include page="timeout.html" flush="true"/> 
<%
}
%>
