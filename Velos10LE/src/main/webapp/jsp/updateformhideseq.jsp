<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.MC" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="linkedformsB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/>

<%@ page language = "java" import = "com.velos.esch.business.common.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.business.common.LinkedFormsDao"%>

<body>
<DIV> 

<%
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{
	
%>	
	<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%   
	   String eSign= request.getParameter("eSign");
       String oldESign = (String) tSession.getValue("eSign");
	   if(oldESign.equals(eSign)) 
	   {
		   int modifiedBy = EJBUtil.stringToNum((String) tSession.getAttribute("userId"));
		   String ipAdd=(String)tSession.getAttribute("ipAdd");
		   
		   String calledFrom=request.getParameter("calledfrom");
		   calledFrom=(calledFrom==null)?"":calledFrom;
		   
		   String studyId=request.getParameter("studyid");
		   studyId=(studyId==null)?"":studyId;
		   
		   int index=-1;
		   
   		   //this hidden element stores the lf_pks of all the forms
		   //the checkedValsArr array has only those ids whose hide is checked
		   //create another array for storing hide value for each form id
		   //search each pk_lf in checkedValsArr, if its found then set hide value in array to 1 else to 0
		   //the patProfileCheckedArr array has only those ids whose display in patient profile is checked
		   //create another array for storing display in patient profile value for each form id
		   //search each pk_lf in patProfileCheckedArr, if its found then set display in patient profile value in array to 1 else to 0		   
		   		   
		   String checkedValsArr[] = request.getParameterValues("hideCheckedVal");  		   
		   String allPksArr[] = request.getParameterValues("lfPk");
		   
		   String allPksArrStudy[] = request.getParameterValues("lfPkS");
		   
		   
		   String chkDispInSpecArr[] = request.getParameterValues("chkDispInSpec");
		   
		   if (allPksArr==null) allPksArr=new String[0];
		   if (allPksArrStudy==null) allPksArrStudy=new String[0];
		   
		   
		   
		   ArrayList dispInSpecVals = new ArrayList();	
		      
		   
		   //following 4 variables are used for storing hidden forms related to a study
		   String hideFormArr[]=request.getParameterValues("hideCheckedValS");
		   String hiddenFormArr[]=request.getParameterValues("hiddenformid");
		   if (hiddenFormArr==null) hiddenFormArr=new String[0];
		   if (hideFormArr==null) hideFormArr=new String[0];
		   
		   ArrayList hideList=new ArrayList(Arrays.asList(hideFormArr));
		   ArrayList hiddenFormList=new ArrayList(Arrays.asList(hiddenFormArr));
		   ArrayList delList=new ArrayList();
		   ArrayList insertList=new ArrayList();
		   
		   
		   String seqsArr[] = request.getParameterValues("dispSeq");
		   String patProfileCheckedArr[] = request.getParameterValues("patProfileCheckedVal");		   
		   String searchVal = "";
		   
		   int ret = 0;
		   String seq="",tempVal="";
		   int len = allPksArr.length;
		   
		   int speclen = 0;
		      	  if (calledFrom.equals("S"))
		             { 
		              //speclen = allPksArrStudy.length;
		              speclen = 0;
    		}
    		else
    		{
    		  speclen = len;
    		}
		   
		   String hideArr [] = new String[len];
   		   String patProfileArr [] = new String[len];	 
   		   String dispInSpecArr [] = new String[speclen];   

		   ArrayList checkedVals = new ArrayList();
		   ArrayList patProfileCheckedVals = new ArrayList();		   
		   int idx = 0;
		   
		   if (!(checkedValsArr==null)) {
		   	 checkedVals = EJBUtil.strArrToArrayList(checkedValsArr);
		   }		   
		   	   
		   //get the values hide for those checked by the user
		   for (int i=0;i<len;i++) 
		   {
	   		   if (!(checkedValsArr==null)) {
    		   	   searchVal = (String) allPksArr[i];
    		   	   idx = checkedVals.indexOf(searchVal);
			   }
			   else //user hasn't selected anyform to hide 
			   {
			   	  idx = -1;
			   }
    
   			   if (idx > -1) {
   			      hideArr[i] = "1";
   			   }
   			   else {
   			      hideArr[i] = "0";
   			   } 
		   }
		   
		   
   		   idx = 0;
		   
		   if (!(patProfileCheckedArr==null)) {
		   	 patProfileCheckedVals = EJBUtil.strArrToArrayList(patProfileCheckedArr);
		   }		   
		   	   
		   //get the values of display in patient profile for those checked by the user
		   for (int i=0;i<len;i++) 
		   {
	   		   if (!(patProfileCheckedArr==null)) {
    		   	   searchVal = (String) allPksArr[i];
    		   	   idx = patProfileCheckedVals.indexOf(searchVal);
			   }
			   else //user hasn't selected anyform to display in patient profile
			   {
			   	  idx = -1;
			   }
    
   			   if (idx > -1) {
   			      patProfileArr[i] = "1";
   			   }
   			   else {
   			      patProfileArr[i] = "0";
   			   } 
		   }
		   
		   //for display in specimen
		   
		    idx = 0;
		   
		   if (!(chkDispInSpecArr == null)) {
		   	 dispInSpecVals = EJBUtil.strArrToArrayList(chkDispInSpecArr);
		   }		   
		   	
		   	 
		   	   
		   //get the values of display in patient profile for those checked by the user
		   
		   
		   for (int i=0;i<speclen;i++) 
		   {
	   		   if (!(chkDispInSpecArr==null)) {
    		   	   
    		   	  if (calledFrom.equals("S"))
		             { 
    		   	       searchVal = (String) allPksArrStudy[i];
    		   	     }
    		   	     else
    		   	     {
    		   	       searchVal = (String) allPksArr[i];  
    		   	     } 
    		   	   
    		   	   idx = dispInSpecVals.indexOf(searchVal);
			   }
			   else //user hasn't selected anyform to display in patient profile
			   {
			   	  idx = -1;
			   }
    
   			   if (idx > -1) {
   			      dispInSpecArr[i] = "1";
   			   }
   			   else {
   			      dispInSpecArr[i] = "0";
   			   } 
		   }
		   ///////////

		   //send data for saving in arrays
		  ret= linkedformsB.updateHideSeqOfLF(allPksArr,hideArr,seqsArr,patProfileArr,ipAdd,modifiedBy,dispInSpecArr);
		 
		  if (calledFrom.equals("S"))
		  {
		  SettingsDao settingsDao=commonB.getSettingsInstance();
		   if (ret>=0)
		  {
		   for (int i=0;i<hiddenFormList.size();i++)
		   {
		      tempVal=(String)hiddenFormList.get(i);
		     index=hideList.indexOf(tempVal);
		     if (index<0) delList.add(tempVal);
		   }
		   for (int i=0;i<hideList.size();i++)
		   {
		      tempVal=(String)hideList.get(i);
		     index=hiddenFormList.indexOf(tempVal);
		     if (index<0) insertList.add(tempVal);
		   }
		   
		   //Create arrayslist to set to settingsDao and save/delete records
		   if (delList.size()>0) 
		   {
		     for (int i=0;i<delList.size();i++)
		     {
		      settingsDao.setSettingsModName("3");
		      settingsDao.setSettingsModNum(studyId);
		      settingsDao.setSettingKeyword("FORM_HIDE");
		     }
		     settingsDao.setSettingValue(delList);
		     
		    ret=settingsDao.purgeSettings();
		   }
		   if (ret>=0){
		   if (insertList.size()>0) 
		   { 
		     settingsDao.reset();
		     for (int i=0;i<insertList.size();i++)
		     {
		      settingsDao.setSettingsModName("3");
		      settingsDao.setSettingsModNum(studyId);
		      settingsDao.setSettingKeyword("FORM_HIDE");
		     }
		     settingsDao.setSettingValue(insertList);
		     
		    ret=settingsDao.insertSettings();
		   }
		    
		  }
		  }
		  
		  }//end calledFrom.equals("S")
		  
%>
<br>
<br>
<br>
<br>
<br>

<%
		  if (ret >= 0) {   		      
%>
  		  <P class="successfulmsg" align="center"><%=MC.M_Data_SavedSucc%><%-- Data saved successfully*****--%></P>
		  
		<script>
			setTimeout("self.close()",1000);
		</script>
		<%} else {%>
  		  <P class="successfulmsg"><%=MC.M_Data_NotSvdSucc%><%-- Data not saved successfully*****--%></P>		   
		<%}

 }//end of if for eSign check
else{  
%>
		<jsp:include page="incorrectesign.jsp" flush="true"/>
<%	  
			}//end of else of incorrect of esign

}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
</BODY>
</HTML>
