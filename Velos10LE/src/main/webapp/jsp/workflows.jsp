<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*,com.velos.eres.service.util.*"%>
<%@ page import="com.velos.eres.gems.business.*, org.json.JSONObject"%>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="formjs.js"></SCRIPT>
<script>
var workflowFunctions = {
	workflowInstances: [],
	createWorkflowDialog: {}
};

workflowFunctions.createWorkflowDialog = function (workflowInstanceId, prevWorkflowInstanceId){
	if (prevWorkflowInstanceId == 0){
		$j('#workflowTasks'+ workflowInstanceId +'Div').dialog({
			width: $j('#workflowTab').width()-1,
			minHeight: 50,
			closeOnEscape: false,
			resizable: false,
			closeText:'',
			open: function(event, ui) { $j(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').hide(); }
		}).dialog("widget").position({
			my: 'left top', 
			at: 'left top',
			of: $j('#workflowTab')
		});
	} else {
		$j('#workflowTasks'+ workflowInstanceId +'Div').dialog({
			width: $j('#workflowTab').width()-1,
			minHeight: 50,
			closeOnEscape: false,
			resizable: false,
			closeText:'',
			open: function(event, ui) { $j(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').hide(); }
		}).dialog("widget").position({
			my: 'top', 
			at: 'bottom',
			of: $j('#workflowTasks'+prevWorkflowInstanceId+'Div')
		});
		
	}
	return false;
};

</script>
<%
	String entityId = (String)request.getParameter("entityId");
	String workflow_types = (String)request.getParameter("workflowTypes");
	if (StringUtil.isEmpty(workflow_types) && StringUtil.isEmpty(workflow_types)){
		return;
	}
	
	String wfPageId = (String)request.getParameter("wfPageId");

	String [] workflowTypes = (StringUtil.isEmpty(workflow_types))? 
			null : workflow_types.split(",");

	if (workflowTypes.length <= 0) return;

	String moreParams = (String)request.getParameter("params");
	
	Integer prevWorkflowInstanceId = 0;

	if ("Y".equals(CFG.Workflows_Enabled)) {

		for(int wfIndx=0; wfIndx < workflowTypes.length; wfIndx++){
			String workflowType = workflowTypes[wfIndx];
			if (StringUtil.isEmpty(workflowType)) continue;

			if (StringUtil.stringToNum(entityId) < 0) continue;
			
			WorkflowJB workflowJB = new WorkflowJB();
			Workflow wf = new Workflow();
			wf = workflowJB.findWorkflowByType(workflowType);
			
			if (null == wf) {return;}
			
			WorkflowInstanceJB workflowInstanceJB = new WorkflowInstanceJB();
			Integer result = 0;
			String nextDestination = "";
			WorkflowInstance workflowInstance = new WorkflowInstance();
	
			workflowInstance = workflowInstanceJB.getWorkflowInstanceStatus(entityId, workflowType);

			Integer workflowInstanceId = 0;
			workflowInstanceId = workflowInstance.getPkWfInstanceId();
			
			Integer workflowInstanceStatus = 0;
			workflowInstanceStatus = workflowInstance.getWfIStatusFlag();
			workflowInstanceStatus = (null == workflowInstanceStatus)? 0 : workflowInstanceStatus;
	
			if (workflowInstanceId == null || workflowInstanceId == 0) {
				workflowInstanceId = workflowInstanceJB.startWorkflowInstance(entityId, workflowType);

				if (StringUtil.stringToNum(entityId) > 0){
					workflowInstanceJB.updateWorkflowInstance(entityId, workflowType, moreParams);
					if (workflowInstanceId != null || workflowInstanceId != 0) {
						//nextDestination = workflowInstanceJB.fetchNextTask(entityId, workflowType);
					}
				}
			} else {
				if (StringUtil.stringToNum(entityId) > 0){
					workflowInstanceJB.updateWorkflowInstance(entityId, workflowType, moreParams);
					//nextDestination = workflowInstanceJB.fetchNextTask(entityId, workflowType);
				}
			}
	
			ArrayList workflowTasksList = new ArrayList();
			workflowTasksList = workflowInstanceJB.getWorkflowInstanceTasks(workflowInstanceId);

			int divZIndex = (10000*(wfIndx+1));
		%>
		<%-- Start of Task modal dialog --%>
		<div id="workflowTasks<%=workflowInstanceId%>Div" title="<%=wf.getWorkflowName()%>" class="" style="display:none; top: 0px;">
			<div id='workflowTasks_EmbedDataHere'>
				<table class="workflowTasks" width="99%">
				<%
				int taskId = 0;
				String taskCompleteFlag = "0";
				HashMap<String, Object> record = new HashMap<String, Object>();
				for (int taskIndx = 0; taskIndx < workflowTasksList.size(); taskIndx++){
			        record = (HashMap<String, Object>) workflowTasksList.get(taskIndx);
			        taskId = StringUtil.stringToNum(""+record.get("PK_TASKID"));
			        if (taskId <= 0) continue;

			        %>
			        <!-- <td><a href='#' onclick="workflowFunctions.goToWFTask(<%=record.get("PK_TASKID")%>)">GoTo</a> -->
			        <%
				    taskCompleteFlag = (String)record.get("TASK_COMPLETEFLAG");
					if ("1".equals(taskCompleteFlag)){
				    %>
				       	<tr>
				       		<td width="80%"><%=record.get("WA_NAME")%></td>
				       		<td>&nbsp;<img id="taskImg<%=taskId%>" class="headerImage" align="bottom" src="images/greencheck.png" border="0" title="<%=LC.L_Done%>" alt="<%=LC.L_Done%>" /></td>
				       	</tr>
				    <%}%>
					<%if ("0".equals(taskCompleteFlag)){%>
			        	<tr>
			        		<td width="80%"><%=record.get("WA_NAME")%></td>
			        		<td>&nbsp;<img id="taskImg<%=taskId%>" class="headerImage" align="bottom" src="images/exclamation.png" border="0" title="<%=LC.L_Not_Done%>" alt="<%=LC.L_Not_Done%>"/></td>
			        	</tr>
						<%
				        String onclickJSONStr = (String)record.get("WA_ONCLICK_JSON");
						String onclickStr = "";
				        if (!StringUtil.isEmpty(onclickJSONStr)){
					        onclickStr = WorkflowUtil.getOnclickJS(wfPageId, taskId, onclickJSONStr);
					        %>
					        <%if (!StringUtil.isEmpty(onclickStr)){%>
								<script>
								<%=onclickStr%>
								</script>
							<%}%>
				        <%}%>
					<%}%>
			    <%}%>
				</table>
			</div>
			<input type='hidden' value="<%=entityId%>" id='wfEntityId<%=workflowInstanceId%>'></input>
			<input type='hidden' value="<%=workflowType%>" id='workflowType<%=workflowInstanceId%>'></input>
			<input type='hidden' value="<%=moreParams%>" id='moreParams<%=workflowInstanceId%>'></input>
		</div>
		<%-- End of Task modal dialog --%>
		
		<script>
			var len = (workflowFunctions.workflowInstances).length;
			workflowFunctions.workflowInstances[len] = <%=workflowInstanceId%>;
			$j(document).ready(function() {
				<%--workflowFunctions.createWorkflowDialog(<%=workflowInstanceId%>, <%=prevWorkflowInstanceId%>);--%>
			});
		</script>
		<%
		prevWorkflowInstanceId = workflowInstanceId;
		%>
	<%} %>
<%}%>