<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<Link Rel=STYLESHEET HREF="common.css" type=text/css>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id="studyStatB" scope="request" class="com.velos.eres.web.studyStatus.StudyStatusJB"/>
<jsp:useBean id="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="complianceJB" scope="request" class="com.velos.eres.compliance.web.ComplianceJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*, com.velos.eres.service.util.*"%>

<%
	int studyId=0;  
	String sessStudyId=null;
    int ret = 0;
	int studyStatId  =0;

 	String startDate =  "";

//  String appRenNum = "";

//	String appRenStatus = "";

	String protocolStatus ="";

	String studyStatusOutcome=""; //Amar

	String studyStatusType=""; //Amar

		/* A.Morales - Re-enable hspnnum */
	String hspnNum = "";

	String documentedById = "";

	String protocolNotes = "";

	String mode="";

	String msg="";

	String validDate = "";

	String meetingDate = ""; // Amar

	String src = null;
	String selectedTab = null;

	String lastStatusId = ""; 
	
	String revBoard = "";
	String assignedTo = "";
	
	
	String currentStat=request.getParameter("currStat");
	String currentStatId=request.getParameter("currentStatId");//KM
	src = request.getParameter("srcmenu");
	selectedTab = request.getParameter("selectedTab");

	String siteId = request.getParameter("accsites");
	
	studyStatId = Integer.parseInt(request.getParameter("studyStatId"));
	

	mode = request.getParameter("mode");
	revBoard = request.getParameter("revBoard");
	assignedTo = request.getParameter("assignedToId");
	
	if (StringUtil.isEmpty(assignedTo))
	{
		assignedTo = "";
	}
	
	if (StringUtil.isEmpty(revBoard))
	{
		revBoard = "";
	}
	
	//A.Morales - re-enable hspnNum
	hspnNum = request.getParameter("hspnNum");
	
	if (StringUtil.isEmpty(hspnNum))
	{
		hspnNum = "";
	}
	startDate = request.getParameter("startDate");
	Calendar cal = Calendar.getInstance(TimeZone.getDefault());

    /**** @Gyanendra updated for bug #20544 *******/
	String DATE_FORMAT = "HH:mm:ss a";
	java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(DATE_FORMAT);       
     
	sdf.setTimeZone(cal.getTimeZone());
	
    String time = sdf.format(cal.getTime());
    
    startDate = startDate +" "+ time;
    /**** @Gyanendra End modification *****/
  	validDate = request.getParameter("validDate");
   
    //Added by Amarnadh
	meetingDate = request.getParameter("meetingDate"); 

    studyStatusOutcome = request.getParameter("studyStatusOutcome"); 

	studyStatusType = request.getParameter("studyStatusType"); 

	//end added

	documentedById = request.getParameter("documentedById");
	
	String selOrgId = request.getParameter("selOrgId");

	protocolNotes = request.getParameter("protocolNotes");
	
	protocolNotes = (   protocolNotes  == null      )?"":(  protocolNotes ) ;	

	protocolStatus = request.getParameter("protocolStatus");
	

	if(mode.equals("M"))
	{
		protocolStatus = request.getParameter("hdnProtocolStatus");
		siteId = request.getParameter("hdnAccsitess");
		
	}
	int iStatusId = EJBUtil.stringToNum(protocolStatus);

		
	HttpSession tSession = request.getSession(true); 



	 String eSign = request.getParameter("eSign");

	if (sessionmaint.isValidSession(tSession))
	{
%>
	<jsp:include page="sessionlogging.jsp" flush="true"/>
<%	
	  	String oldESign = (String) tSession.getValue("eSign");
		if(!oldESign.equals(eSign)) {

	%>

	  <jsp:include page="incorrectesign.jsp" flush="true"/>	

	<%

	} else {

     	      
   		      
		String ipAdd = (String) tSession.getValue("ipAdd");
     	String usr = null;
     	usr = (String) tSession.getValue("userId");     
     	sessStudyId = (String) tSession.getValue("studyId");     
     	studyId = EJBUtil.stringToNum(sessStudyId);  
		int activePK =studyStatB.getFirstActiveEnrollPK(studyId);
		
		CodeDao cd = new CodeDao();
		int cdActiveId = cd.getCodeId("studystat", "active");
		
		int cdClosureId = cd.getCodeId("studystat", "prmnt_cls");
		
		String accId = (String) tSession.getValue("accountId");
		
		
//     	studyStatB.setStatAppRenNo(appRenNum);     

		if (mode.equals("M")){	
    	  studyStatB.setId(studyStatId);     
		  studyStatB.getStudyStatusDetails();
		}
		
     	studyStatB.setStatDocUser(documentedById);     
/* A.Morales - Re-enable hspnnum */
   	studyStatB.setStatHSPN(hspnNum);     
     	studyStatB.setStatNotes(protocolNotes);     
     	studyStatB.setStatStartDate(startDate); 
		studyStatB.setStatValidDate(validDate);
		studyStatB.setStudyStatus(protocolStatus);  
		//Added by Amarnadh
		studyStatB.setStatMeetingDate(meetingDate); 
		studyStatB.setStudyStatusOutcome(studyStatusOutcome);
		
		if(mode.equals("N")) //status type is not changed in modify mode
		{ 
			studyStatB.setStudyStatusType(studyStatusType);
		} 
		//end added
//     	studyStatB.setStatApproval(appRenStatus);     
     	studyStatB.setStatStudy(sessStudyId);
     	studyStatB.setSiteId(siteId);
     	
     	studyStatB.setStudyStatusReviewBoard(revBoard);
     	studyStatB.setStudyStatusAssignedTo(assignedTo);
     		
   
   
     	if (mode.equals("M")){	
			
     	    studyStatB.setModifiedBy(usr);
     	    studyStatB.setIpAdd(ipAdd);  
			studyStatB.setCurrentStat(currentStat);  //KM
     	    ret = studyStatB.updateStudyStatus(); 
		    //Added by Manimaran for September Enhancement S8. 
		    if (currentStat.equals("1") && (studyStatId!=EJBUtil.stringToNum(currentStatId))) {
			    studyStatB.setId(EJBUtil.stringToNum(currentStatId));     
				studyStatB.getStudyStatusDetails();
				studyStatB.setCurrentStat("0");
				studyStatB.setModifiedBy(usr);
				studyStatB.updateStudyStatus();      
		    }
     	  
	  /* if (ret == 0) {
     		msg = MC.M_StatSvd_Succ;
     %>
     <br>
     <br>
     <br>
     <br>
     <br>

     <!--<p class = "successfulmsg" align = center> Data was saved successfully </p>

     <META HTTP-EQUIV=Refresh CONTENT="1;    	   URL=studystatusbrowser.jsp?srcmenu=<%=src%>&selectedTab=4&studyId=<%=studyId%>"> -->
     <%
     	  /* }else {
     		msg = "Study Status not saved";*/
     %>

    <!--  <P><%=msg%></P> -->

     <%
     	   //}  
     	  
	 		if(studyStatId == activePK){
	 			
			/*	studyB.setId(EJBUtil.stringToNum(sessStudyId));
				studyB.getStudyDetails();
				studyB.setStudyActBeginDate(startDate);
				studyB.updateStudy();*/
			}

		}
		else
		{
			   
				studyStatB.setCreator(usr);
				studyStatB.setIpAdd(ipAdd);
				studyStatB.setCurrentStat(currentStat);//KM
				studyStatB.setStudyStatusDetails();
				//Added by Manimaran for September Enhancement S8.
				//KM-Modified based on code review
				if (currentStat.equals("1") && (studyStatId!=EJBUtil.stringToNum(currentStatId))) {
				   studyStatB.setId(EJBUtil.stringToNum(currentStatId));     
				   studyStatB.getStudyStatusDetails();
				   studyStatB.setCurrentStat("0");
				   studyStatB.setModifiedBy(usr);
				   studyStatB.updateStudyStatus(); 
				}
				/* Commented by sonia, end dates will be managed in session bean now
				//update previous status end date
				lastStatusId = request.getParameter("lastStatId");
				
				if ((lastStatusId != null) && (lastStatusId.trim()).length() > 0 )
				{ 
				
					studyStatB.setId(EJBUtil.stringToNum(lastStatusId));     
				studyStatB.getStudyStatusDetails();
				studyStatB.setStatEndDate(startDate);
				studyStatB.setModifiedBy(usr);
					studyStatB.setIpAdd(ipAdd);     
					ret = studyStatB.updateStudyStatus();
					
				}*/
				if("LIND".equals(CFG.EIRB_MODE)) {
					//update File Blob object for unlock study statuses				
					HashMap<String, Object> paramMap = new HashMap<String, Object>();    
					paramMap.put("userId", (usr.equals(null) ? null : Integer.valueOf(usr)));
					paramMap.put("accountId", (accId.equals(null) ? null : Integer.valueOf(accId)));
					paramMap.put("studyId", studyId);
					String studyStat = complianceJB.getCurrentStudyStatus(paramMap);
					if("crcReturn".equals(studyStat) || "irb_add_info".equals(studyStat) || "medAncRevPend".equals(studyStat)) {
						complianceJB.updateFileBlobObject(String.valueOf(studyId));
					}
					int irbCount = complianceJB.getIrbApprovedCount(paramMap);
					if(("app_CHR".equals(studyStat))  && (irbCount >= 2 )) {
						complianceJB.updateFileBlobObjectForIrbApproved(String.valueOf(studyId));
					}
				}
				
				
		}
     
	   String studyMinActiveDt= studyStatB.getMinActiveDate(EJBUtil.stringToNum(sessStudyId));
   		      if(studyMinActiveDt == null)
   		      	  studyMinActiveDt = "";
   		      
   		      
   		    
		
   	 	String studyMinClosureDt= studyStatB.getMinPermanentClosureDate(EJBUtil.stringToNum(sessStudyId));
   		      if(studyMinClosureDt == null)
   		      	  studyMinClosureDt = "";
   		         		       
   		         		       
   		         		      
     if(cdActiveId == iStatusId){

			studyB.setId(EJBUtil.stringToNum(sessStudyId));
   			studyB.getStudyDetails();
			studyB.setStudyActBeginDate(studyMinActiveDt);
		//Added by Gopu to fix the bugzilla Issue #2609 (While updating ER_Study with Actual/End Date last_modified_by is not getting updated.)
			studyB.setModifiedBy(usr);
			studyB.updateStudy();
		
		}
		
		if(cdClosureId == iStatusId){ 
     		
     	 	studyB.setId(EJBUtil.stringToNum(sessStudyId));
   			studyB.getStudyDetails();
			studyB.setStudyEndDate(studyMinClosureDt);
		//Added by Gopu to fix the bugzilla Issue #2609 (While updating ER_Study with Actual/End Date last_modified_by is not getting updated.)
			studyB.setModifiedBy(usr);
			studyB.updateStudy();
		
		}

     %>
     <br>
     <br>
     <br>
     <br>
     <br>
	<%if(ret!=-2){%>
     <p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%></p>
	 <%}else{%>
	<p class = "successfulmsg" align = center> <%=MC.M_DataNotSvd_Succ%><%--Data was not saved successfully*****--%></p>
		 <%}%>
	 
     <%-- //commented for study versioning enhancements
	 
     	Calendar rightNow = Calendar.getInstance();
     	int mon = 0;
		int yr = 0;
		int dt = 0;
		
		String smon, sdt;
     	mon = rightNow.get(rightNow.MONTH) + 1;
		yr = rightNow.get(rightNow.YEAR);
		dt = rightNow.get(rightNow.DATE);
		smon = String.valueOf(mon);
		if (smon.length() == 1)
		{
			smon = "0" + smon;
		}

     	sdt = String.valueOf(dt);
		if (sdt.length() == 1)
		{
			sdt = "0" + sdt;
		}
     	String curdt = null;
     	curdt =  String.valueOf(yr) + "." + smon  +  "." +  sdt;

     	//check for active status
     		if(studyStatB.checkStatus(sessStudyId) > 0 )
     		 {
     		  	//status is changed to active
     			//out.println("ACTIVE");

     			studyB.setId(EJBUtil.stringToNum(sessStudyId));

     			studyB.getStudyDetails();
     			//out.println(curdt);

     			studyB.setStudyVersion(curdt);

     			studyB.updateStudy();

     			tSession.setAttribute("studyVersion",curdt);
     		 } 
//commented for study version enhancements     --%>

<%if (mode.equals("N")){ %>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=studystatusbrowser.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>">
<%}else{ %>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=studystatusbrowser.jsp?srcmenu=<%=src%>&selsite=<%=selOrgId%>&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>">
     <%}
     }//end of if body for session
}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
<DIV> </div>
</BODY>
</HTML>