<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil,com.aithent.audittrail.reports.AuditUtils"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_DelMyLinks%><%--Delete My Links*****--%></title>
<jsp:useBean id="ulb" scope="request" class="com.velos.eres.web.ulink.ULinkJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

<% String src;
	src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<br>

<DIV class="formDefault" id="div1">
<%

HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))	{
	String lnk = request.getParameter("lnkId");
	String mode = request.getParameter("mode");
	int lnkPK =EJBUtil.stringToNum(lnk);

	String delMode=request.getParameter("delMode");
	if (delMode==null) {
		delMode="final";
%>
	<FORM name="deleteLnk" id="deleteLnkForm" method="post" action="deletelnk.jsp" onsubmit="if (validate(document.deleteLnk)==false){setValidateFlag('false');return false;} else {setValidateFlag('true'); return true;}">

	<br><br>
	<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
		<b><%=MC.M_PlsEtrEsign_Del%><%-- Please enter e-Signature to proceed with deletion*****--%> </b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>

		</table>

<!--JM: 19June2009: implementing common Submit Bar in all pages -->
	<jsp:include page="submitBar.jsp" flush="true">
	<jsp:param name="displayESign" value="Y"/>
	<jsp:param name="formID" value="deleteLnkForm"/>
	<jsp:param name="showDiscard" value="N"/>
	</jsp:include>

 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
 	 <input type="hidden" name="lnkId" value="<%=lnk%>">
   	 <input type="hidden" name="mode" value="<%=mode%>">

	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
			} else {
				String ipAdd = (String) tSession.getValue("ipAdd");
				String usr = (String) tSession.getValue("userId");
				ulb.setLnkId(lnkPK);
				ulb.getULinkDetails();
				ulb.setModifiedBy(usr);
				ulb.setIpAdd(ipAdd);
				ulb.updateUlink();
	
	int i=0;
	// Modified for INF-18183 ::: AGodara 
	ulb.setLnkId(lnkPK);
	i=ulb.deleteUlink(AuditUtils.createArgs(tSession,"",LC.L_Manage_Acc));
	// Added By RA Bug # 4599 Display message 'Data deleted successfully' at the time on successful delete
	%>
	<BR>
	<BR>
	<BR>
	<BR>
	<BR>
	<BR>
	<BR>
	<BR>
	<BR>
	<% if(i == 0) {%>
		<p class = "successfulmsg" align = center><%=MC.M_Data_DelSucc%><%-- Data deleted successfully*****--%></p>
	<%}%>
	<%

	if (mode.equals("a"))
	{
		out.println("<META HTTP-EQUIV=Refresh CONTENT=\"0; URL=accountlinkbrowser.jsp?selectedTab=6&srcmenu=tdmenubaritem2\">");
	}
	else
	{
		out.println("<META HTTP-EQUIV=Refresh CONTENT=\"0; URL=ulinkBrowser.jsp?selectedTab=2&srcmenu=tdmenubaritem2\">");

	}
	} //end esign
	} //end of delMode
}//end of if body for session

else {
%>
<jsp:include page="timeout.html" flush="true"/>
<%
}
%>

  <div class = "myHomebottomPanel">
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>

</BODY>
</HTML>
