<jsp:include page="localization.jsp" flush="true"/>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
 
 <%@ page language = "java" import = "com.velos.eres.service.util.*"%>
<html>
<head>
	<title><%=LC.L_File_Download%><%--File Download*****--%></title>
</head>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<body>
<%

//request.setCharacterEncoding("UTF-8");
//response.setContentType("text/html;charset=UTF-8");

String tableName=request.getParameter("tableName");
String columnName=request.getParameter("columnName");
String pkColumnName=request.getParameter("pkColumnName");
String module=request.getParameter("module");
String db=request.getParameter("db");
String pkValue=request.getParameter("pkValue");
String file=request.getParameter("file");
String dnldurl=request.getParameter("dnldurl");
String moduleName = request.getParameter("moduleName");
Integer repId = new Integer((request.getParameter("repId")==null) ? "0" : request.getParameter("repId"));
String requestURL = StringUtil.encodeString(request.getRequestURL().toString());
String queryString = request.getQueryString();

String url="";


url= dnldurl + "?file="+file + "&pkValue="+pkValue+"&tableName="+tableName+"&columnName="+columnName+"&pkColumnName="+pkColumnName+"&module="+module+"&db="+db+"&moduleName="+moduleName+"&repId="+repId+"&requestURL="+requestURL+"&queryString="+queryString;

//RequestDispatcher rd = request.getRequestDispatcher("../file/servlet/Download");
//rd.forward(request, response);
//response.sendRedirect(url);


%>
<form name="dummy" method="post" action = <%=dnldurl%>>
<div id="div1">
	<P class = "sectionHeadings"> <%=MC.M_CloseWindowAfter_FileDld%><%--Please close this window after downloading the file*****--%> </P>
	</div>
	
    <input type="hidden" name="tableName" value="<%=tableName%>">
    <input type="hidden" name="columnName" value="<%=columnName%>">
    <input type="hidden" name="pkColumnName" value="<%=pkColumnName%>">
    <input type="hidden" name="module" value="<%=module%>">
    <input type="hidden" name="db" value="<%=db%>">
    <input type="hidden" name="pkValue" value="<%=pkValue%>">
    <input type="hidden" name="file" value="<%=file%>">
    <input type="hidden" name="moduleName" value="<%=moduleName%>">
    <input type="hidden" name="repId" value="<%=repId%>">
    <input type ="hidden" name="requestURL" value="<%=requestURL%>" />
    <input type ="hidden" name="queryString" value="<%=queryString%>" />
   
</form>
<script>
document.dummy.submit();

//window.location=url;
//self.close();
</script>
 
	
</body>
</html>
