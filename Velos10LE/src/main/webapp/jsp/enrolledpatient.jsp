<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>



<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<script>
	function setOrder(formObj,orderBy,pgRight) //orderBy column number
{
	var lorderType;
	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";
		lorderType="desc";
	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
		lorderType="asc";
	}

	lsrc = formObj.srcmenu.value;
	lcurPage = formObj.page.value;
	lselectedtab= formObj.selectedTab.value;
	formObj.action="allPatient.jsp?srcmenu="+lsrc+"&page="+lcurPage+"&orderBy="+orderBy+"&orderType="+lorderType+"&selectedTab="+lselectedtab;
	formObj.submit();
}
 function setFilterText(form)
 {
 	/*  form.ptxt.value=form.patCode.value;
 	  form.atxt.value=form.patage[form.patage.selectedIndex].text ;
 	  if (form.patgender[form.patgender.selectedIndex].value != "")
 	       form.gtxt.value=form.patgender[form.patgender.selectedIndex].text ;
 	  if (form.patstatus[form.patstatus.selectedIndex].value != "")
 	   form.rtxt.value=form.patstatus[form.patstatus.selectedIndex].text ;
 	  form.rbytxt.value=form.regBy.value ;
	  */
	  // Handled for passing the organization id across pages in pagination
	   form.orgId.value=form.dPatSite.value ;

 }
	</script>


<title><%=LC.L_Search_Pats%><%--Search <%=LC.Pat_Patients%>*****--%></title>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.DateUtil,com.velos.eres.business.common.*" %>
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.person.*,com.velos.eres.service.util.*,com.velos.eres.web.user.*,com.velos.eres.web.grpRights.GrpRightsJB"%>

<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="codelst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="usrSite" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="commonB" scope="page" class="com.velos.eres.web.common.CommonJB" />
<jsp:useBean id="studyStatB" scope="page" class="com.velos.eres.web.studyStatus.StudyStatusJB" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<% String src;
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1)
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {
%>
<body style="overflow:scroll;">
<%
	} else {
%>
<body>
<%
	}
%>
<br>
<DIV class="browserDefault" id="div1">
<%
  HttpSession tSession = request.getSession(true);
  if (sessionmaint.isValidSession(tSession)) {
  userB = (com.velos.eres.web.user.UserJB) tSession.getValue("currentUser");
   	int pageRight = 0;
	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");
    pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));
 if (pageRight>0) {
	Calendar cal = new GregorianCalendar();
	int pstudy = 0;
	String searchFilter ="" ;
     String selectedTab = "" ;
     selectedTab=request.getParameter("selectedTab");
    String uName = (String) tSession.getValue("userName");
	UserJB user = (UserJB) tSession.getValue("currentUser");
    String userId = (String) tSession.getValue("userId");
   	String siteId = user.getUserSiteId();
   	String accountId = user.getUserAccountId();
   	pstudy=EJBUtil.stringToNum((request.getParameter("dStudy")));



   	String ddstudy ="";
   	StudyDao studyDao = studyB.getUserStudies(userId, "dStudy",pstudy,"active");
	ddstudy = studyDao.getStudyDropDown();
	ArrayList studyIds = studyDao.getStudyIds();
   	String active_status="" ;
    String dGender="" ;
    String gender="" ;
    String pstat="" ;
    String age ="" ;

	String selSite = null;
	selSite =  request.getParameter("dPatSite");

	// Changes for site
	String orgId = "" ;

	// if (!(selSite == null) &&         (        !(selSite.trim()).equals("")      )                )
	if (  ( selSite != null)    &&         (        !(selSite.trim()).equals("")      )     )
	{
			if ( ! selSite.equals("null"))
			 siteId = selSite;
	}
	else
	{
		 orgId = siteId ;
	}

	orgId =  request.getParameter("orgId");
	if ( orgId != null )
	{
		if ( ! orgId.equals("null"))
		siteId = orgId ;
	}

	 UserSiteDao usd = new UserSiteDao ();
	 ArrayList arSiteid = new ArrayList();
	 ArrayList arSitedesc = new ArrayList();
	 String ddSite = "";

	 usd = usrSite.getSitesWithViewRight(EJBUtil.stringToNum(accountId),EJBUtil.stringToNum(userId));

	 arSiteid = usd.getUserSiteSiteIds();
	 arSitedesc = usd.getUserSiteNames();

	 ddSite = EJBUtil.createPullDown("dPatSite",EJBUtil.stringToNum(siteId),arSiteid,arSitedesc);

   	if (pstudy==0)  {
		if(studyIds.size() > 0) {
		searchFilter=" and std.pk_study in (" ;
		for(int i=0; i < studyIds.size();i++ ) {
			if(i == 0) {
				searchFilter = searchFilter +  ((Integer) studyIds.get(i)).toString();
			} else {
				searchFilter= searchFilter + "," + ((Integer) studyIds.get(i)).toString() ;
			}
		}
		searchFilter = searchFilter + ")";
		} else {
			searchFilter = "";
		}

	} else {
	   	searchFilter = " and std.pk_study="+pstudy ;
	}


   String ageFilter ="" ;
 	String patCode = "";
 	String regBy ="", ptxt="All",atxt="All",gtxt="All",rtxt="All",rbytxt="All";


 	int lowLimit=0,highLimit=0 ;


 

String patientsql =  "select std.study_number study_num,std.pk_study,(SELECT to_char(MIN(er.PATPROT_ENROLDT),PKG_DATEUTIL.F_GET_DATEFORMAT) " +
                    "FROM ER_PATPROT er, ER_PER p WHERE std.STUDY_VERPARENT is null and er.fk_study=std.pk_study and p.fk_site = st.pk_site and p.pk_per = er.fk_per) enrolldate," +
				"(select count(pk_patprot) from ERV_PATSTUDYSTAT b Where b.FK_STUDY = std.pk_study and b.per_site = st.pk_site) total_status, " +
			"(select count(pk_patprot) from ERV_PATSTUDYSTAT b Where b.FK_STUDY = std.pk_study and b.per_site = st.pk_site and " +
			" trim(lower(STATUS_CODELST_SUBTYP)) = 'active') active_count, (select count(pk_patprot) from ERV_PATSTUDYSTAT b " +
			" Where b.FK_STUDY = std.pk_study and b.per_site = st.pk_site and trim(lower(STATUS_CODELST_SUBTYP)) = 'offstudy') offstudy_count," +
			" (select count(pk_patprot) from ERV_PATSTUDYSTAT b Where b.FK_STUDY = std.pk_study and b.per_site = st.pk_site and " +
			" trim(lower(STATUS_CODELST_SUBTYP)) = 'followup') followup_count, (select count(pk_patprot) from ERV_PATSTUDYSTAT b " +
			" Where b.FK_STUDY = std.pk_study and b.per_site = st.pk_site and trim(lower(STATUS_CODELST_SUBTYP)) = 'offtreat') offtreat_count  from er_site st, er_study std " +
			"where std.fk_account = " + accountId  + " and study_actualdt is not null "+  searchFilter +
			" and pk_site = " +  siteId  +
			" and exists (select * from er_studyteam where fk_user = "+userId +
			" and fk_study = std.pk_study) order by study_num ";

String countsql=  "select count(*) from er_site st, er_study std " +
			" where STUDY_VERPARENT is null and std.fk_account = " + accountId + " and study_actualdt is not null "+   searchFilter +
			" and pk_site = " + siteId +
			" and exists (select * from er_studyteam where fk_user = "+userId +
			" and fk_study = std.pk_study) ";

			//out.println("====countsql===="+countsql);
			//out.println("++++++patientsql"+patientsql);


			String pagenum = "";
			int curPage = 0;
			long startPage = 1;
			long cntr = 0;
			String pagenumView = "";
			int curPageView = 0;
			long startPageView = 1;
			long cntrView = 0;

			pagenum = request.getParameter("page");
			if (pagenum == null)
			{
			pagenum = "1";
			}
			curPage = EJBUtil.stringToNum(pagenum);

			String orderBy = "";
			orderBy = request.getParameter("orderBy");

			String orderType = "";
			orderType = request.getParameter("orderType");

			if (orderType == null)
			{
			orderType = "asc";
			}


			pagenumView = request.getParameter("pageView");
			if (pagenumView == null)
			{
			pagenumView = "1";
			}
			curPageView = EJBUtil.stringToNum(pagenumView);

			String orderByView = "";
			orderByView = request.getParameter("orderByView");

			String orderTypeView = "";
			orderTypeView = request.getParameter("orderTypeView");

			if (orderTypeView == null)
			{
			orderTypeView = "asc";
			}

			long rowsPerPage=0;
			long totalPages=0;
			long rowsReturned = 0;
			long showPages = 0;
			long totalRows = 0;
			long firstRec = 0;
			long lastRec = 0;
			boolean hasMore = false;
			boolean hasPrevious = false;

			rowsPerPage =  Configuration.ROWSPERBROWSERPAGE ;
			totalPages =Configuration.PAGEPERBROWSER ;

			BrowserRows br = new BrowserRows();


            br.getPageRows(curPage,rowsPerPage,patientsql,totalPages,countsql,orderBy,orderType);
			rowsReturned = br.getRowReturned();
			showPages = br.getShowPages();

		     startPage = br.getStartPage();

			hasMore = br.getHasMore();

			hasPrevious = br.getHasPrevious();


			totalRows = br.getTotalRows();


			firstRec = br.getFirstRec();

			lastRec = br.getLastRec();


%>









 <P class = "userName"> <%= uName %>
  </P>

 <P class="sectionHeadings"><%=MC.M_MngProd_EnrlPats%><%--Manage <%=LC.Pat_Patients%> >> Enrolled <%=LC.Pat_Patients%>*****--%></P>

  <Form name="results" method="post" action="enrolledpatient.jsp?page=1"  onSubmit="setFilterText(document.results)" >
  	    <jsp:include page="mgpatienttabs.jsp" flush="true"/>
    <br>

 <P class="defComments"> <%=MC.M_PlsSrchCrit_ViewThePatList%><%--Please specify the Search criteria and then click on ''Search'' to view the Patient List*****--%></P>
<!--<Form  name="search" method="post" action="allPatient.jsp">-->
<input type="hidden" name="srcmenu" Value="<%=src%>">
<input type="hidden" name="searchFrom" Value="search">


  <table class = tableDefault width="50%" cellspacing="0" cellpadding="0" border=0 >

    <tr><td><%=LC.L_Study%><%--Study*****--%></td>
	<td><%=LC.L_Organization%><%--Organization*****--%></td>
	</tr>
    <tr>
	<td class=tdDefault><%=ddstudy%></td>
	<td class=tdDefault><%=ddSite%></td>

 	<td class=tdDefault width=10%><button type="submit"><%=LC.L_Search%><%--<%=LC.L_Search%>*****--%></button></td>
    </tr>

  </table>

<!--</Form>-->

<input type="hidden" name="selectedTab" Value=<%=selectedTab%>>
<Input type="hidden" name="orderBy" value="<%=orderBy%>">
<Input type="hidden" name="page" value="<%=curPage%>">
<Input type="hidden" name="orderType" value="<%=orderType%>">
<input type="hidden" name="ptxt">
<input type="hidden" name="atxt">
<input type="hidden" name="gtxt">
<input type="hidden" name="rtxt">
<input type="hidden" name="rbytxt">
<input type="hidden" name="orgId">
<%
//Retrieve study count for this user


	ArrayList studyList=new ArrayList();



	if ((totalRows <= 0 ) || (rowsReturned == 0)) {


	} else {

	   int i = 0;


	   if(rowsReturned == 0) {


	} else {



	   String patientId = "";
       String studyNumber="" ;
       String totalEnroll="" ;







	%>





    <table class=tableDefault width="100%" border=0>
 <tr>
   	<th class=tdDefault width=20%><%=LC.L_Study_Number%><%--Study Number*****--%></th>

    <th class=tdDefault width=25%><%=LC.L_First_PatEnrolled%><%--First <%=LC.Pat_Patient%> Enrolled*****--%></th>
	<th class=tdDefault width=15%><%=LC.L_Total_Enrolled%><%--Total Enrolled*****--%></th>
 	<th class=tdDefault width=5%><%=LC.L_ActiveOrOn_Treatment%><%--Active/On Treatment*****--%></th>
	<th class=tdDefault width=10%><%=LC.L_Off_Study%><%--Off Study*****--%></th>
  	<th class=tdDefault width=10%><%=LC.L_In_FollowHypenUp%><%--In Follow-Up*****--%></th>
  	<th class=tdDefault width=10%><%=LC.L_Off_Treatment%><%--Off Treatment*****--%></th>
  	<th class=tdDefault width=15%><%=LC.L_Patient_List%><%--<%=LC.Pat_Patient%> List*****--%></th>

   </tr>

<%


        String tempPercode="" ;
        String creator="" ;
            int study_counter = 0 ;
            String enrollDate="";
            String activecount = "" ,offstudycount="",followcount="",offtreatcount="";

             String studypk="";
		for(i = 1 ; i <=rowsReturned ; i++)

	  	{

	  	    studyNumber = br.getBValues(i,"study_num")	 		   ;
	  	    studypk = br.getBValues(i,"pk_study")	 		   ;
            totalEnroll=  br.getBValues(i,"total_status")	 		   ;
            enrollDate=    br.getBValues(i,"enrolldate")	 		   ;
            if (enrollDate==null) enrollDate="No "+LC.Pat_Patient+" Enrolled" ;
	           activecount=    br.getBValues(i,"active_count")	 		   ;
    	       offstudycount =   br.getBValues(i,"offstudy_count")	 		   ;
        	   followcount =   br.getBValues(i,"followup_count")	 		   ;
	           offtreatcount =	 br.getBValues(i,"offtreat_count")	 		   ;



//			if (psk!= null)

//			{








	 		   if ((i%2)==0) {

%>

      <tr class="browserEvenRow">

<%

	 		   }else{

%>

      <tr class="browserOddRow">

<%

	 		   }

%>

        <td width =20%><%=studyNumber%></td>
        <td width =25%><%=enrollDate%></td>
        <td width =15%><%=totalEnroll%></td>
        <td width =5%><%=activecount%></td>
        <td width =10%><%=offstudycount%></td>
        <td width =10%><%=followcount%></td>
        <td width =5%><%=offtreatcount%></td>
         <td width=15%><A href="studypatients.jsp?selectedTab=2&srcmenu=tdmenubaritem5&studyId=<%=studypk%>&patid=&patstatus=&openMode=F&dPatSite=<%=siteId%>"><%=LC.L_View%><%--View*****--%></A></td>




      </tr>

	  <%

//	  		} //end for temppatient

	  }// end for for loop

	  } // end of if for length == 0

  } //end of if for the check of patientResults == null

	  %>

    </table>
    	<table>
	<tr>
	<td>
	<% if (totalRows > 0) { Object[] arguments = {firstRec,lastRec,totalRows}; %>
		<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%-- <font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
	<%} else {%>
		<br><font class="recNumber">No Records Found</font>
	<%}%>
	</td>
	</tr>
	</table>

	<table align=center>
	<tr>
<%

	if (curPage==1) startPage=1;

    for (int count = 1; count <= showPages;count++)
	{
   cntr = (startPage - 1) + count;

	if ((count == 1) && (hasPrevious))
	{
    %>
	<td colspan = 2>
  	<A href="enrolledpatient.jsp?selectedTab=1&orgId=<%=orgId%>&srcmenu=<%=src%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;
	</td>
	<%
  	}
	%>
	<td>
	<%

	 if (curPage  == cntr)
	 {
     %>
		<FONT class = "pageNumber"><%= cntr %></Font>
       <%
       }
      else
        {
       %>

	   <A href="enrolledpatient.jsp?selectedTab=2&orgId=<%=orgId%>&searchFrom=initial&srcmenu=<%=src%>&page=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>"><%= cntr%></A>
       <%
    	}
	 %>
	</td>
	<%
	  }

	if (hasMore)
	{
   %>
   <td colspan = 3 align = center>
  	&nbsp;&nbsp;&nbsp;&nbsp;<A href="enrolledpatient.jsp?selectedTab=2&orgId=<%=orgId%>&searchFrom=initial&srcmenu=<%=src%>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
	</td>
	<%
  	}
	%>
   </tr>
  </table>





  </Form>



 <% }	//} //end of else body for page right



}//end of if body for session

else{

%>

  <jsp:include page="timeout.html" flush="true"/>

  <%

}



%>

  <div>

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>
</div>
<div class ="mainMenu" id = "emenu">

 <jsp:include page="getmenu.jsp" flush="true"/>

</div>

</body>

</html>

