<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=MC.M_ResetEsignOrPwd%><%-- Reset User eSign/Password*****--%></title>

<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,com.velos.eres.business.user.*,com.velos.eres.service.util.*"%>
<jsp:include page="include.jsp" flush="true"/>
</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
 function  validate(formobj){
     formobj=document.resetpass; 

	if((formobj.password.checked==false)&&(formobj.esignBox.checked==false))
	{
		alert("<%=MC.M_PlsSelPwd_Esign%>");/*alert("Please select either password or e-signature.");*****/
	return false;    
	}
	
	if (formobj.fromPage.value=="User") {
	
	    if (!(validate_col('e-Sign',formobj.eSign))) return false
   
    	if(isNaN(formobj.eSign.value) == true) {
    		alert("<%=MC.M_IncorrEsign_EtrAgain%> ");/*alert("Incorrect e-Signature. Please enter again");*****/
			formobj.eSign.focus();
			return false;
		}
	}
	
}
</SCRIPT>


<%
 String src;
String pname = null;
pname=request.getParameter("pname");
src= request.getParameter("srcmenu");
String userId=request.getParameter("userId");
String fromPage=request.getParameter("fromPage");

userB.setUserId(EJBUtil.stringToNum(userId));
userB.getUserDetails();
String userLastName =userB.getUserLastName();
String userFirstName= userB.getUserFirstName();
String userName = userLastName + ", " + userFirstName ;  

%>
<body>
<DIV class="popDefault" id="div1">
<%
HttpSession tSession = request.getSession(true); 

	if (sessionmaint.isValidSession(tSession))
	{
		String accId = (String)tSession.getAttribute("accountId");
		if (StringUtil.stringToNum(userB.getUserAccountId()) != StringUtil.stringToNum(accId)) {
%>
</DIV>
<jsp:include page="accessdenied.jsp" flush="true"/>
<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>
</html>
<%
			return;
		}
%>


	<P class="sectionHeadings"> <%=MC.M_ResetEsignOrPwd%><%-- Reset eSign/Password*****--%></P>

	
<Form class="formDefault" id="resetPwd" name="resetpass" METHOD="POST" action="updateresetpass.jsp" onSubmit="if (validate(document.formDefault)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
<input type="hidden" name="fromPage" value=<%=fromPage%>> 
<table width=70%>
<tr>
	<td colspan = 4> <p class = "defComments"><font class=mandatory>(<%=MC.M_PrevPwdEsign_AfterReset%><%-- You are about to  reset  user's Password/e-Signature. User's previous Password/e-Signature will be removed. Please select the appropriate option.*****--%>)</font></p>
</td>
</tr>

<tr>
	<td class=tdDefault><%=LC.L_User_Name%><%-- User Name*****--%></td>
	<td class=tdDefault> <%= userName %></td>	
</tr>

<tr>
	<td class=tdDefault><%=LC.L_Reset_Pwd%><%-- Reset password*****--%></td>
	<td><input type=checkbox name="password" >
	<input type="hidden" name=userId value=<%=userId%>>
	</td>
</tr>

<tr>
	<td class=tdDefault><%=LC.L_Reset_Esign%><%-- Reset esign*****--%></td>
	<td><input type=checkbox name="esignBox" ></td>
</tr>
<%	 String eSignDisp ="";
	if (fromPage.equals("User")) { eSignDisp = "Y";} else {eSignDisp ="N";} %>
<tr>
	<td class=tdDefault colspan=2>
		<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="<%=eSignDisp%>" />
			<jsp:param name="formID" value="resetPwd"/>
			<jsp:param name="showDiscard" value="N"/>
		</jsp:include>
	</td>
</tr>
</table>
<%
} else {  //else of if body for session

%>
<% if (fromPage.equals("User")) {%>
  <jsp:include page="timeout_childwindow.jsp" flush="true"/>
<%
} else { %>
  <jsp:include page="timeout_adminchild.jsp" flush="true"/>
  <%
 }
}
%>
</DIV>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>
</html>
