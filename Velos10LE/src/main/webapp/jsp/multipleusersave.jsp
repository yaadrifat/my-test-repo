<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML> 
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
 </HEAD>
 
 
 <BODY> 
 <%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.util.*,javax.mail.*, javax.mail.internet.*, javax.activation.*"%> 
 <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
 <jsp:useBean id="milestoneB" scope="page" class="com.velos.eres.web.milestone.MilestoneJB" />
   <%  int ret = 0;
 	//String eSign = request.getParameter("eSign");
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))

	   {	
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   	

				String userId = (String) tSession.getValue("userId");
				String ipAdd = (String) tSession.getValue("ipAdd");
			 	String userIds = request.getParameter("userIds");
				String milestoneId = request.getParameter("milestoneId");
				
				String applyToAlFlag = request.getParameter("applyAll");
				String studyId = "";
				
				if (StringUtil.isEmpty(applyToAlFlag))
				{
				
				applyToAlFlag = "0";
				
				}
				
				milestoneB.setId(EJBUtil.stringToNum(milestoneId));		
				milestoneB.getMilestoneDetails();

				studyId = milestoneB.getMilestoneStudyId(); 
				milestoneB.setMilestoneUsersTo(userIds);
				milestoneB.setModifiedBy(userId);
				milestoneB.setIpAdd(ipAdd);

				ret = milestoneB.updateMilestone();
				
				if (applyToAlFlag.equals("1"))
				{
					milestoneB.applyNotificationSettingsToAll(milestoneId, userIds,  studyId, userId,  ipAdd ) ;
				}

   if (ret == 0) {
%>
<SCRIPT>
   	window.opener.location.reload();
	self.close();
</SCRIPT>	
<%	
   } else {
%>   
	<br><br><br><br><br>
	<p class = "sectionHeadings" align = center> <%=MC.M_DataCntSvd_ContacAdmin%><%--Data could not be saved. Please contact the administrator.*****--%> </p>
	<br>
	<button onClick = "self.close()"><%=LC.L_Close%></button>
<%	
   }
//}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY> 
</HTML>
