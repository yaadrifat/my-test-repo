<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>  
 

<script>
	
function confirmBox(dt){
var paramArray = [dt];
if (confirm(getLocalizedMessageString("M_WantDel_PvisoEtr",paramArray))) /*if (confirm("Do you want to delete Proviso entered on: "+ dt+"?" )) *****/
return true;
else
return false;
   
}

function fadd()
{
		if (document.getElementById("addprovisos")) {
				document.getElementById("addprovisos").style.visibility="visible";
			    
			}
} 				 		 	 

function toggleView(provisoPk)
{
	textelem = document.getElementById("fld_"+provisoPk);
	
		if (textelem) {
				
				divelem = document.getElementById("provisos_full");
				
				divelem.innerHTML="<table><tr><td><b> <%=LC.L_Full_Proviso%><%--Full Proviso*****--%>: </b ></td><td align=right><A href='#' onclick='fhideProviso()'>[<%=LC.L_Close%><%--Close*****--%>]</A></td></tr></table>"+textelem.value;
				divelem.style.visibility="visible";
			    
			}
} 	

function fhideProviso()
{
	divelem = document.getElementById("provisos_full");
	divelem.style.visibility="hidden";
	divelem.innerHTML="";
	

}
	
function fhideDetails()
{
   		if (document.getElementById("addprovisos")) {
				document.getElementById("addprovisos").style.visibility="hidden";
			    
			}

}
 
 </script>

<body  class="yui-skin-sam">
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/><!--km-->
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/><!--km--> 
<jsp:useBean id="submissionProvisoB" scope="request" class="com.velos.eres.web.submission.SubmissionProvisoJB"/>
	
   	<jsp:include page="include.jsp" flush="true"/> 
   	<jsp:include page="ui-include.jsp" flush="true"></jsp:include>


   <%@ page language = "java" import = "com.velos.eres.business.submission.impl.SubmissionProvisoBean,com.velos.eres.web.grpRights.GrpRightsJB,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,com.velos.eres.service.util.*,java.sql.*,com.velos.eres.business.common.*,com.velos.eres.web.codelst.CodelstJB"%>  

  
 

<%

 
 
 HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession)) {
    String userId ="";
    
    userId = (String) tSession.getValue("userId");
    
    String enteredBy = "";
    String enteredByName = "";
    
    enteredBy = userId;
    	
 
    String tabsubtype  = "";
    
    tabsubtype = request.getParameter("tabsubtype");
	String submissionPK = request.getParameter("submissionPK");
	String submissionBoardPK = request.getParameter("submissionBoardPK");

    
    
     
	 int pageRight = 0;
     pageRight = 7;
     

 
     
    if (pageRight > 0 )    {
        

    
    %>
        
    
     
    <input type="hidden" id="userId" value="<%=userId%>">
     
    <input type="hidden" id="tabsubtype" value="<%=tabsubtype%>">
    	
    	
    <P class="defComments"><%=MC.M_PrevProvisosList_DscnOrder%><%--Previously entered Proviso(s) listed in descending order of Date Entered*****--%>:</p>
    			
    <table width="100%"><tr><td><p class="sectionHeadings"> <%=LC.L_Provisos%><%--Provisos*****--%></p></td>
    
    	<td align="right"><p class="sectionHeadings"><A href="provisoDetails.jsp?mode=N&submissionPK=<%=submissionPK%>&submissionBoardPK=<%=submissionBoardPK%>&tabsubtype=<%=tabsubtype%>"><img title="<%=LC.L_Add_New%>" src="./images/Create.gif" border ="0"/></A></p></td></tr></table>
    <%
    	ArrayList arResults = new ArrayList();
    	int provisoPk  = 0;
    	String rowClass="";
    	boolean viewMore = false;
    	
 			arResults = submissionProvisoB.getSubmissionProvisos(EJBUtil.stringToNum(submissionPK),EJBUtil.stringToNum(submissionBoardPK));
 			
 			if (arResults != null && arResults.size() > 0)
 			{
 				%>
 				
 				<table width="100%">
 					    <th><%=LC.L_Date_Entered%><%--Date Entered*****--%></th>
					    <th><%=LC.L_Entered_By%><%--Entered By*****--%></th>
    					<th><%=LC.L_Proviso_Type%><%--Proviso Type*****--%></th>
   						<th><%=LC.L_Proviso%><%--Proviso*****--%></th> 		
   						<th></th>		

 					<%
 				for (int k = 0; k<arResults.size();k++)
 				{
 							if ((k%2)==0) {
									 rowClass="browserEvenRow";
									}
									else
									{
									  rowClass="browserOddRow";		
									}
									
									
					SubmissionProvisoBean sb = new SubmissionProvisoBean();
					CodelstJB codelstJB = new CodelstJB();
					
					String fullProviso = "";
 					String displayProviso="";
 					
 					viewMore = false;
 					
 					sb = (SubmissionProvisoBean)arResults.get(k);
 					
 					provisoPk = (sb.getId()).intValue();
 					fullProviso = sb.getSubmissionProviso();
 					
 					if (! StringUtil.isEmpty(fullProviso))
 					{
 						if (fullProviso.length() > 100)
 						{
 							displayProviso = fullProviso.substring(0,100) + "....";
 							viewMore = true;
 						}else
 							{
 							displayProviso = fullProviso;
 							}
 					}
 					
 					
 					%>
 					 	
 						<tr class="<%=rowClass%>">
 							<td> <%= DateUtil.dateToString(sb.getProvisoDate())%></td>
 							<td> <%= sb.getProvisoEnteredByName()%></td>
 							<td> <%= codelstJB.getCodeDescription(EJBUtil.stringToNum(sb.getProvisoType()))%></td>
 							<td><b><A href="provisoDetails.jsp?mode=M&tabsubtype=<%=tabsubtype%>&provisoId=<%=provisoPk%>"><img border="0" src="./images/edit.gif" title="<%=LC.L_Edit%>"/></A>
 							
 							&nbsp;<A id="html_<%=provisoPk%>" href="#" onClick="return toggleView('<%=provisoPk%>');"><img border="0" src="./images/View.gif" title="<%=LC.L_View%><%--View*****--%>"/></A>
 								
 								<input type="hidden" id="fld_<%=provisoPk%>" value="<%=fullProviso%>" >
 									
 							</td>
 							<td><A href="deleteProviso.jsp?submissionPK=<%=submissionPK%>&submissionBoardPK=<%=submissionBoardPK%>&tabsubtype=<%=tabsubtype%>&provisoId=<%=provisoPk%>" onClick="return confirmBox('<%= DateUtil.dateToString(sb.getProvisoDate())%>')"> 
 							<img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" align="left"/></A>
 							</b></td>
 							
 						</tr>
 							
 							
								
 						
 					<%
 					
 				
 				}
                    %>
                    </table>
                    			
    
                    			<div id="provisos_full" style="position:absolute;left:0px;top:0px;border-style:double;background-color:#FFFFCC;opacity:1;border-width:2px;visibility:hidden; z-index:1000;">
 									
 								</div>
 									
 								 								    	
                    
                 <% 			
 			}
    	%>

    
    
    
 
  <%
    } //end of if body for page right
    else{
%>
    <%@include file="accessdenied.jsp" %>
<%
    } //end of else body for page right
}//end of if body for session
else {
%>
    
    <jsp:include page="timeout.html" flush="true"/>
<%}
%>
<div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>

 

 
 