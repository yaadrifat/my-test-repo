<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
 
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<title><%=LC.L_User_Search%><%--User Search*****--%></title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<%-- Nicholas : Start --%>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<%--<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>--%>	  
<%-- Nicholas : End --%>
<SCRIPT type="text/javascript">


function setOrder(formobj,orderBy) 
{

	if (formobj.orderType.value=="asc") {
		formobj.orderType.value= "desc";		
	} else 	if (formobj.orderType.value=="desc") {
		formobj.orderType.value= "asc";
	}
	

	formobj.orderBy.value= orderBy;	
	formobj.action="multipleusersearchdetailswithsave.jsp";
	formobj.submit(); 
}

//JM: 11/09/2005: Added 

function checkall(formObj){

var len = 0;
var seluser=0;
//len=formObj.lenUsers.value;
len=formObj.rowsReturned.value;
seluser=formObj.selectedUsers.value;

if (len==1 && seluser==0){
	if (formObj.usercheckall.checked==true){
		formObj.usercheck.checked=true;
	}
	else if (formObj.usercheckall.checked==false){
		formObj.usercheck.checked=false;
	}
}

if (len==1 && seluser>0){
	if (formObj.usercheckall.checked==true){
		formObj.usercheck[0].checked=true;
		
	}
	else if (formObj.usercheckall.checked==false){
		formObj.usercheck[0].checked=false;
		
	}
}
else if (len>1){
	for (i=0; i<len; i++){
		if (formObj.usercheckall.checked==true){
			formObj.usercheck[i].checked=true;
		}
		else if (formObj.usercheckall.checked==false){
			formObj.usercheck[i].checked=false;
		}
	}
 }
}
</SCRIPT>




<SCRIPT>
function validate(frmName) {
  //var lenUsers = 0;
  var totalRows = 0;

	//lenUsers = parseInt(frmName.lenUsers.value);
	totalRows = parseInt(frmName.totalRows.value);

	var names = "";
	var ids = "";
	if(totalRows > 1) {
  
		for (var i = 0; i < totalRows; i++) {

			if(frmName.usercheck[i].checked){
  
				names = names + frmName.username[i].value + ";";
				ids = ids + frmName.userid[i].value + ",";
				
			}		
		}
	
	}
	if(totalRows==1) {

  		if(frmName.usercheck.checked){  	
			names = names + frmName.username.value + ";";
			ids = ids + frmName.userid.value + ",";		
		
		}		

	}

	
	names = names.substring(0,names.length-1);
	ids = ids.substring(0,ids.length-1);
	frmName.userIds.value = ids;
	
	frmName.submit();

}

</SCRIPT>


<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="teamB" scope="session" class="com.velos.eres.web.team.TeamJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import="com.velos.eres.service.util.*,com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil"%>
<jsp:include page="include.jsp" flush="true"/>

<% response.setHeader("Cache-Control", "max-age=1, must-revalidate");%>

<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1) 
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {	
%>
<body style="overflow:scroll;">
<%
	} else {
%>
<body>
<%
	}
%>
<%HttpSession tSession = request.getSession(true);
  if (sessionmaint.isValidSession(tSession))
{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String from=request.getParameter("from") ;
	String accId = (String) tSession.getValue("accountId");
	
	String milestoneId = "";
	String milestoneUserIds = "";
	String milestoneUserNames = "";	
	milestoneId = request.getParameter("milestoneId");
	milestoneUserIds = request.getParameter("milestoneUserIds");
	String studyId = request.getParameter("studyId");
	milestoneUserIds = ((milestoneUserIds == null) || (milestoneUserIds.equals("0")))?"":milestoneUserIds;
	milestoneUserNames = request.getParameter("milestoneUserNames");
	milestoneUserNames = ((milestoneUserNames == null) || (milestoneUserNames.equals("0")))?"":milestoneUserNames;		
	while(milestoneUserNames.indexOf('~') != -1){
	milestoneUserNames = milestoneUserNames.replace('~',' ');
	}	
	
		
		String userJobType = "";
		String dJobType = "";
		String dAccSites ="";
		
		String orgNam = "";
		UserDao userDao = new UserDao();
		//String userSiteId = "";
		//userSiteId = userB.getUserSiteId();
		
		
		//TeamDao teamDao = teamB.getTeamValues(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(accId));
	   	//ArrayList teamRoles = teamDao.getTeamRoleIds();
		//teamRole = ((teamRoles.get(0))==null)?"-":(teamRoles.get(0)).toString();
		CodeDao cd = new CodeDao();
		CodeDao cd2 = new CodeDao();
		cd2.getAccountSites(EJBUtil.stringToNum(accId)); 
    	cd.getCodeValues("job_type");
		userJobType =userB.getUserCodelstJobtype();
		String jobTyp=request.getParameter("jobType") ;
		String orgName=request.getParameter("accsites") ;
		

		if(jobTyp==null)
				jobTyp = "";
			if(orgName == null)
				orgName = "";

			if (jobTyp.equals(""))			
			   dJobType=cd.toPullDown("jobType");
			 
			else
			   dJobType=cd.toPullDown("jobType",EJBUtil.stringToNum(jobTyp),true);

	
		String siteVal = request.getParameter("accsites");
		if(siteVal == null) siteVal = "";
		String mode = request.getParameter("mode");
		if(mode == null) mode = "";			  
		String fname = LC.L_All/*"All"*****/;
		String lname = LC.L_All/*"All"*****/;
		String jobtype = LC.L_All/*"All"*****/;
		String accsites = LC.L_All/*"All"*****/;
		String usrgrps  = LC.L_All/*"All"*****/;
		String ByStudy = LC.L_All/*"All"*****/;
		
		String jobDesc = LC.L_All/*"All"*****/;
		String siteDesc = LC.L_All/*"All"*****/;   	    
		String gDesc = LC.L_All/*"All"*****/;
		String tDesc = LC.L_All/*"All"*****/;  
	
	
		fname = StringUtil.htmlEncodeXss(request.getParameter("fname"));
		
		fname=(fname==null)?LC.L_All/*"All"*****/:fname;
		
		lname = StringUtil.htmlEncodeXss(request.getParameter("lname"));
		
		lname=(lname==null)?LC.L_All/*"All"*****/:lname;
		jobtype = request.getParameter("jobType");
		accsites = request.getParameter("accsites");
				  		
		String userSiteId = "";
		int primSiteId = 0;

		userB=(com.velos.eres.web.user.UserJB)tSession.getAttribute("currentUser");
		userSiteId = userB.getUserSiteId();
		userSiteId=(userSiteId==null)?"":userSiteId;
		primSiteId= EJBUtil.stringToNum(userSiteId);
		
		/*if (orgName.equals(""))
		dAccSites=cd2.toPullDown("accsites");	  

		else
		dAccSites=cd2.toPullDown("accsites",EJBUtil.stringToNum(orgName),true);
		*/
		
		if ((siteVal.length()==0) && (mode.equals("initial"))){
			dAccSites=cd2.toPullDown("accsites",primSiteId,true);	
			orgName=userSiteId;
			mode="M";
		}	
		else
			dAccSites=cd2.toPullDown("accsites",EJBUtil.stringToNum(siteVal),true);		


		jobDesc = cd.getCodeDesc(EJBUtil.stringToNum(jobtype));	
	
	
		if (accsites==null) 
		siteDesc = cd2.getCodeDesc(primSiteId);	
		else{		
		siteDesc = cd2.getCodeDesc(EJBUtil.stringToNum(accsites));
		}	
	
		//JM: to make drop down of group
	    String dUsrGrps="";
	    String selGroup="";
	   	// int ac=EJBUtil.stringToNum(accountId);	
	    CodeDao cd1 = new CodeDao();    
	    cd1.getAccountGroups(EJBUtil.stringToNum(accId)); 
	    String gName=request.getParameter("usrgrps");
	    	    if (gName==null)  gName="";
	    selGroup=request.getParameter("usrgrps");
	    if (selGroup==null)  selGroup="";
	    if (selGroup.equals(""))	    
	    dUsrGrps = cd1.toPullDown("usrgrps");
	    else
	    dUsrGrps = cd1.toPullDown("usrgrps",EJBUtil.stringToNum(selGroup),true);
	    
	    usrgrps =request.getParameter("usrgrps");
	    gDesc=cd1.getCodeDesc(EJBUtil.stringToNum(usrgrps));
			
			
	
	    
		String dStudy="";
		String selstudy="";
		int selstudyId=0;
		selstudy=request.getParameter("ByStudy") ;
		if(selstudy==null ){
			selstudyId=0 ;
		}else { 
			selstudyId = EJBUtil.stringToNum(selstudy);
		}
		
		
		
		String stTeam = "";
		stTeam = request.getParameter("ByStudy");
		stTeam=(stTeam==null)?"":stTeam;
			
		StudyDao studyDao = new StudyDao();
		String suserId = (String) tSession.getValue("userId");
		studyDao.getStudyValuesForUsers(suserId);
		ArrayList studyIds = studyDao.getStudyIds();
		ArrayList studyNumbers = studyDao.getStudyNumbers();
		
		//make dropdowns of study
		dStudy=EJBUtil.createPullDown("ByStudy",selstudyId,studyIds,studyNumbers);
			
		ByStudy =request.getParameter("ByStudy");
		//JM: get the study number from the selected study id
		tDesc=studyDao.getStudy(EJBUtil.stringToNum(ByStudy));	
		
		
//JM: 11/28/2005: Added: column click sorting
		String orderBy = "";
		orderBy = request.getParameter("orderBy");
		

		if (EJBUtil.isEmpty(orderBy)){
			orderBy = " lower(u.USR_FIRSTNAME || ' ' || u.usr_lastname) "; 	
			
		}

		String orderType = "";
		orderType = request.getParameter("orderType");
		
		if (EJBUtil.isEmpty(orderType))
		{
		orderType = "asc";
		}
	
	
		
	
		
%>

<P class="sectionheadings">&nbsp;&nbsp;<%=MC.M_UsrSearch_Criteria%><%--Search Criteria*****--%></P>
  <Form  name="search1" method="post" id="multUsrFrm" action="multipleusersearchdetailswithsave.jsp" onSubmit="">
  <input type="hidden" name=from value="<%=from%>">
  <input type="hidden" name=milestoneId value="<%=milestoneId%>">
  <input type="hidden" name=milestoneUserIds value="<%=milestoneUserIds%>">
  <input type="hidden" name=milestoneUserNames value="<%=milestoneUserNames%>">
  <input type="hidden" name=studyId value="<%=studyId%>">    	
  <Input type="hidden" name="selectedTab" value="1">


  <table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl" >
    <tr>	  
      <td width=15%> <%=LC.L_User_FirstName%><%--User First Name*****--%>: 
      
      <%if(fname.equals("All")){%>
		<Input type=text name="fname" size=20>
	<%}else{%>
        	<!-- km- to fix Bug2308 -->
		<Input type=text name="fname" size=20 value="<%=fname%>">
        <%}%>
      </td>
      <td width=15%> <%=LC.L_User_Last_Name%><%--User Last Name*****--%>: 
        <% if (lname.equals("All")){%>
      	  	<Input type=text name="lname" size=20>
	<%}else{%>
		<Input type=text name="lname" size=20 value="<%=lname%>">
	<%}%>
      </td>  
	  
	  <td width=20%> <%=LC.L_Job_Type%><%--Job Type*****--%>:
		 <%=dJobType%> 
	  </td>


	  <td  width=20%> <%=LC.L_Organization_Name%><%--Organization Name*****--%>:
       
		 <%=dAccSites%> 
	 </td>
	  
    </tr>
    <tr>
	    <td width=20%> <%=LC.L_Group_Name%><%--Group Name*****--%>:         <%=dUsrGrps %> </td>
	    <td width=20%> <%=LC.L_Std_Team%><%--<%=LC.Std_Study%> Team*****--%>:         <%=dStudy%> </td>
	    <td > 
	     	 <button type="submit"><%=LC.L_Search%></button>
	    </td>
	</tr>  
  </table>
</Form>
<DIV > 
  <%
	String uName = (String) tSession.getValue("userName");
	int pageRight = 0;
	
	String ufname= StringUtil.htmlEncodeXss(request.getParameter("fname")) ;
	String ulname= StringUtil.htmlEncodeXss(request.getParameter("lname")) ;
	
	
	String userIdFromSession = (String) tSession.getValue("userId");
	  
	

	int i;
    String usrId = null;	
	String usrName= "";		

//JM: 11/28/2005: Blocked for pagination
		//JM: 11/10/2005 Modified: group and study parameter added 
		//userDao.searchTeamUsers(studyId,ulname,ufname,jobTyp,orgName,milestoneUserIds,accId,selGroup, selstudy);

		if(fname==null||fname.equals(""))
		fname = LC.L_All/*"All"*****/;
		if(lname==null||lname.equals(""))
			lname = LC.L_All/*"All"*****/;
		if(jobDesc==null || jobDesc.equals(""))
			jobDesc = LC.L_All/*"All"*****/;
		if(siteDesc==null || siteDesc.equals(""))
			siteDesc = LC.L_All/*"All"*****/;
		if(gDesc==null || gDesc.equals(""))
			gDesc = LC.L_All/*"All"*****/;
		if(tDesc==null || tDesc.equals(""))
			tDesc = LC.L_All/*"All"*****/;		
			

		String pagenum = "";
		int curPage = 0;
		long startPage = 1;
		String stPage;
		long cntr = 0;
		pagenum = request.getParameter("page");
		
		
		if (pagenum == null)
		{
			pagenum = "1";
		}
		curPage = EJBUtil.stringToNum(pagenum);
	

		String formSql = "";
		String countSql = "";
		StringBuffer completeSelect = new StringBuffer();
		String lWhere = null;
        String fWhere = null;
        String oWhere = null;
        String rWhere = null;
        String gWhere = null;
        String sWhere = null;
        String otWhere = null;

		
    String searchFNameForSQL= StringUtil.escapeSpecialCharSQL(ufname);
    String searchLNameForSQL= StringUtil.escapeSpecialCharSQL(ulname);
    
			//KM-to fix the issue 3371
			String selectSt = 
	  		   "SELECT DISTINCT u.pk_user,u.USR_LASTNAME, "
                    + "(select SITE_NAME from er_site where PK_SITE  = FK_SITEID) as site_name, "
                    + " u.USR_FIRSTNAME, u.USR_MIDNAME, u.USR_STAT, u.USR_TYPE   "
                    + " from er_site s, ER_USER u   "
                    + " where u.fk_account = " +accId
                    + " and u.fk_siteid = s.pk_site "
                    + " and u.USR_TYPE <> 'X' and u.USR_TYPE <> 'P' "
                    + " and u.usr_stat in('A','B') and u.USER_HIDDEN <> 1 ";
                    
         if ((orgName!= null) && (! orgName.trim().equals("") && ! orgName.trim().equals("null"))){
                oWhere = " and UPPER(u.FK_SITEID) = UPPER(";
                oWhere += orgName;
                oWhere += ")";
            }

            if ((jobTyp != null) && (! jobTyp.trim().equals("") && ! jobTyp.trim().equals("null"))){
                rWhere = " and UPPER(u.FK_CODELST_JOBTYPE) = UPPER(";
                rWhere += jobTyp;
                rWhere += ")";
            }

            if ((ufname != null) && (! ufname.trim().equals("") && ! ufname.trim().equals("null"))){
                fWhere = " and UPPER(u.USR_FIRSTNAME) like UPPER('";
                fWhere += searchFNameForSQL.trim();
                fWhere += "%')";
            }

            if ((ulname != null) && (! ulname.trim().equals("") && ! ulname.trim().equals("null"))) {
                lWhere = " and UPPER(u.USR_LASTNAME) like UPPER('";
                lWhere += searchLNameForSQL.trim();
                lWhere += "%')";
            }

            //km-All the User's belonging to the selected group should be retreived, default or not doesn't matter. (Bug:2306)
	    if ((gName != null) && (!gName.trim().equals("") && !gName.trim().equals("null")))
			{			
				//gWhere =" and UPPER(u.fk_grp_default) = upper(";
				gWhere=" and u.pk_user in(select fk_user from er_usrgrp where upper(fk_grp)=upper(";
				gWhere +=gName;
				gWhere +="))";
			}
			
			if ((stTeam != null) && (!stTeam.trim().equals("") && !stTeam.trim().equals("null")))
			{			
					sWhere =" and ( exists (select * from er_studyteam t where t.fk_user = pk_user and t.fk_study =  ";
				sWhere +=stTeam;
				sWhere +=" and nvl(t.study_team_usr_type,'D')='D' ) or pkg_superuser.F_Is_Superuser(pk_user, "+stTeam+") = 1  )";
			
			}		
	
            completeSelect.append(selectSt);


           	if ((milestoneUserIds != null) && (!milestoneUserIds.trim().equals(""))&& (! ulname.trim().equals("null")))
                completeSelect.append(" and u.pk_user not in (" + milestoneUserIds + ")");

            if (fWhere != null) {
                completeSelect.append(fWhere);
            }

            if (lWhere != null) {
                completeSelect.append(lWhere);
            }
            
            if (oWhere != null) {
                completeSelect.append(oWhere);
            }

            if (rWhere != null) {
                completeSelect.append(rWhere);
            }
            
            if (gWhere != null) {
                completeSelect.append(gWhere);
            }
            
            if (sWhere != null) {
                completeSelect.append(sWhere);
            }

            formSql = completeSelect.toString();
            countSql = "Select count(*) from ( " + formSql + ")";
			//out.println("countSql = "+countSql);
           
           
			long rowsPerPage=0;
			long totalPages=0;	
			long rowsReturned = 0;
			long showPages = 0;
				
			boolean hasMore = false;
			boolean hasPrevious = false;
			long firstRec = 0;
			long lastRec = 0;	   
			long totalRows = 0;	
			
			String type ="" ;	
			String type1="";	   	   
			  
			rowsPerPage =  Configuration.MOREBROWSERROWS ;
			totalPages =Configuration.PAGEPERBROWSER ; 
			
			System.out.println("formSql" + formSql);
			System.out.println("countSql" + countSql);
			
			BrowserRows br = new BrowserRows();
			br.getPageRows(curPage,rowsPerPage,formSql ,totalPages,countSql,orderBy,orderType);
			rowsReturned = br.getRowReturned();
			showPages = br.getShowPages();
			startPage = br.getStartPage();
			hasMore = br.getHasMore();
			hasPrevious = br.getHasPrevious();	 
			totalRows = br.getTotalRows();	   
			firstRec = br.getFirstRec();
			lastRec = br.getLastRec();
			
			int counter = 0;

			

%>
<P class="defComments"><%=MC.M_SelFilters_UsrFirstName%><%--The Selected Filters are: User First Name*****--%>: <B><I><u><%=fname%></u></I></B>&nbsp&nbsp <%=LC.L_User_Last_Name%><%--User Last Name*****--%>: <B><I><u><%=lname%></u></I></B>&nbsp&nbsp <%=LC.L_Job_Type%><%--Job Type*****--%>: <B><I><u><%=jobDesc%></u></I></B>&nbsp&nbsp <%=LC.L_Organization_Name%><%--Organization Name*****--%>: <B><I><u><%=siteDesc%></u></I></B>&nbsp&nbsp <%=LC.L_Group_Name%><%--Group Name*****--%>:<B><I><u><%=gDesc%></u></I></B>&nbsp&nbsp <%=LC.L_Std_Team%><%--<%=LC.Std_Study%> Team*****--%>:<B><I><u><%=tDesc%></u></I></B></p>
  <br>

  <Form name="userResult" method=post action="multipleusersave.jsp" onSubmit="return validate(document.userResult);">
  	 
  <input type="hidden" name=userIds value="">
  <input type="hidden" name=milestoneId value="<%=milestoneId%>">   
  <input type="hidden" name=from value="<%=from%>">
  <input type="hidden" name=milestoneUserIds value="<%=milestoneUserIds%>">  
  <input type="hidden" name=milestoneUserNames value="<%=milestoneUserNames%>">	
  <input type="hidden" name=orderBy value="<%=orderBy%>">  
  <input type="hidden" name=orderType  value="<%=orderType%>">
  		
  
  <input type="hidden" name="rowsReturned" value="<%=rowsReturned%>">  
  <Input type="hidden" name="mode" value="M">
  
  <input type="hidden" name=usrgrps  value="<%=gName%>">  
  <input type="hidden" name="fname" value="<%=ufname%>">
  <Input type="hidden" name="lname" value="<%=ulname%>"> 
  <input type="hidden" name="jobType" value="<%=jobTyp%>">
  <input type="hidden" name="accsites" value="<%=orgName%>">	
  <input type="hidden" name="ByStudy" value="<%=stTeam%>">
  <Input type="hidden" name="page" value="<%=pagenum%>">	
  

<%
	StringTokenizer namesST = new StringTokenizer(milestoneUserNames,";");
	StringTokenizer idsST = new StringTokenizer(milestoneUserIds,",");
	int selectedUsers = 0;
	selectedUsers = idsST.countTokens();
	

%>  
<%-- Nicholas : Start --%>  
    <table width="100%" border="0" class="basetbl">
    <input type="hidden"  name="selectedUsers" value=<%=selectedUsers%>>
	<tr>
	 	  
	  <% if (rowsReturned > 0){ %>
	  	<th width=15% align="center">		
		<INPUT TYPE="checkbox" NAME="usercheckall" onClick="checkall(document.userResult) ;">
		&nbsp;&nbsp;<%=LC.L_Select_All%><%--Select All*****--%></th>
		
		<th width=25% onClick="setOrder(document.userResult,'lower(USR_FIRSTNAME)')" align="centeselectedUsersr"><%=LC.L_First_Name%><%--First Name*****--%>
		</th>
		<th width=25% onClick="setOrder(document.userResult,'lower(USR_LASTNAME)')"  align="center"><%=LC.L_Last_Name%><%--Last Name*****--%>
		</th>
		<th onClick="setOrder(document.userResult,'lower(site_name)')"   align="center"><%=LC.L_Organization%><%--Organization*****--%>
		</th>
		<th onClick="setOrder(document.userResult,'lower(USR_STAT || USR_TYPE)')"   align="center"><%=LC.L_User_Type%><%--User Type*****--%>
		</th>
<%-- Nicholas : End --%>
      </tr>
		<tr>
      <%}
      else{%>
            <td width=35% align ="center"><P class="successfulmsg"> <%=MC.M_NoSrchResMatch_Criteria%><%--No search results matching given criteria found*****--%></P></td>
      
      <%}%>
      </tr>
      
<%      
	 

			
		int count = 0;
		
		String usrLastName = "";
		String usrFirstName = "";
		String usrMidName = "";
		String siteName = "";
		String userSiteName = "";
		String jobType = "";
		String usrStat = ""; 
		String usrType = "";


		for(count  = 1 ; count  <= rowsReturned ; count++)

	  	{
					
			usrLastName=br.getBValues(count,"USR_LASTNAME");
			usrLastName=(usrLastName==null)?"":usrLastName;
			
			usrFirstName=br.getBValues(count,"USR_FIRSTNAME");
			usrFirstName=(usrFirstName==null)?"":usrFirstName;
			
			orgNam=br.getBValues(count,"site_name");
			orgNam=(orgNam==null)?"":orgNam;
			
			usrId=br.getBValues(count,"pk_user");
			usrId=(usrId==null)?"":usrId;
			
			usrStat=br.getBValues(count,"USR_STAT");	
			usrStat=(usrStat==null)?"":usrStat;
			
			usrType=br.getBValues(count,"USR_TYPE");
			usrType=(usrType==null)?"":usrType;
			
			
			if (usrStat.equals("A")){
			usrStat=LC.L_Active_AccUser/*"Active Account User"*****/;
			}
			else if (usrStat.equals("B")){
			usrStat=LC.L_Blocked_User/*"Blocked User"*****/;
			}
			else if (usrStat.equals("D")){
			usrStat=LC.L_Deactivated_User/*"Deactivated User"*****/;
			}
			
					
			if (usrType.equals("N")){
			usrStat=MC.M_Non_SystemUser/*"Non System User"*****/;
			}
			
			
			
			if ((count%2)==0) {
	%>
      <tr class="browserEvenRow"> 
        <%
			}else{
		  %>
      <tr class="browserOddRow"> 
        <% } usrName = usrFirstName + " " +usrLastName; 
		   %>
		<td><input type=checkbox name='usercheck'>
        <td width =200> <%=usrFirstName%> </td>
		<td width =200> <%=usrLastName%> </td>
		<td width =200> <%=orgNam%></td>
		<td width=200> <%=usrStat%></td>
		<input type=hidden name='username' value="<%=usrFirstName%>&nbsp;<%=usrLastName%>">
		<input type=hidden name='userid' value=<%=usrId%>>
      </tr>
      <%
	  	}
	  	rowsReturned = rowsReturned + selectedUsers;
	 %>

	
	<input type=hidden name=totalRows value=<%=rowsReturned%>>  	 
	  
	  
	  <tr >
	  
	  	   
	<%

		if(selectedUsers > 0) {
%>	  
		<th >

			<%=MC.M_Already_SelectedUsers%><%--Already Selected Users*****--%>

		</th>
		<th >

			<%=LC.L_First_Name%><%--First Name*****--%>


		</th>
		<th >

			<%=LC.L_Last_Name%><%--Last Name*****--%>

		</th>
		<th >

			<%=LC.L_Organization%><%--Organization*****--%>
		</th>
		<th >

			<%=LC.L_User_Type%><%--User Type*****--%>
		</th>
		
<%
		}
%>		

</tr>
	  <%
		for(i = 0 ; i < selectedUsers ; i++)
	  	{
			//usrName = namesST.nextToken();
			usrId = idsST.nextToken();
			
			userDao = userB.getUsersDetails(usrId);
			
			
			ArrayList uFname = userDao.getUsrFirstNames();
			ArrayList uLname = userDao.getUsrLastNames();
			
			ArrayList uStatuses1 = userDao.getUsrStats();
			ArrayList uTypes1 = userDao.getUsrTypes();
			
			ArrayList organizationNames = userDao.getUsrSiteNames();
			orgNam = ((organizationNames.get(0)) ==null)?"-":(organizationNames.get(0)).toString();
			
			
			String usrFname = ((uFname.get(0)) == null) ? "-":(uFname.get(0)).toString();
			String usrLname = ((uLname.get(0)) == null) ? "-":(uLname.get(0)).toString();		
			
			usrName = usrFname + " " + usrLname;
			
			usrStat = ((uStatuses1 .get(0))==null)?"-":(uStatuses1 .get(0)).toString();  
		
			if (usrStat.equals("A")){
			usrStat=LC.L_ActAcc_Users/*"Active Account User"*****/;
			}
			else if (usrStat.equals("B")){
			usrStat=LC.L_Blocked_User/*"Blocked User"*****/;
			}
			else if (usrStat.equals("D")){
			usrStat=LC.L_Deactivated_User/*"Deactivated User"*****/;
			}
		
			usrType = ((uTypes1.get(0))==null)? "-":(uTypes1.get(0)).toString();
			
			if (usrType.equals("N")){
			usrStat=MC.M_Non_SystemUser/*"Non System User"*****/;
			}
			
			if ((i%2)==0) {
	%>
      <tr class="browserEvenRow"> 
        <%
			}else{
		  %>
      <tr class="browserOddRow"> 
        <% }  
		   %>
		<td><input type=checkbox name='usercheck' checked>
        <td width =200> <%=usrFname %> </td>
		<td width =200> <%=usrLname %> </td>
		<td width =200> <%=orgNam%> </td>
		<td width=200> <%=usrStat%></td>
		<input type=hidden name='username' value="<%=usrFname.trim()%><%=usrLname.trim()%>">	
		<input type=hidden name='userid' value=<%=usrId%>>		
		
      </tr>
	<%}
		if (totalRows==0) firstRec=0;
		if (rowsReturned > 0) {%>
	    
	  <tr>
     
	<td class=tdDefault colspan=3>		
		<input type = "checkbox" value=1 name = "applyAll">&nbsp; <b><%=MC.M_ApplyToAllMstone_OfStd%><%--Apply to all Milestones defined for this <%=LC.Std_Study%>*****--%> </b>&nbsp;&nbsp;&nbsp;&nbsp;<!--input type="image" src="../images/jpg/Submit.gif" align="absmiddle" border="0"-->
	</td>
	</tr>

	<tr>
	<jsp:include page="submitBar.jsp" flush="true"> 
				<jsp:param name="displayESign" value="N"/>
				<jsp:param name="formID" value="multUsrFrm"/>
				<jsp:param name="showDiscard" value="N"/>
	</jsp:include>	
	
	</tr>
	</table>
	
<%if (totalRows!=0){ Object[] arguments = {firstRec,lastRec,totalRows}; %>
	<table cellspacing="0" cellpadding="0" border="0">
	<tr><td>
	
		<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%--<font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
		<%
		}
		
		} 
		%>	
	</td></tr>
	</table>
	
	<table align=center cellspacing="0" cellpadding="0" border="0">
	<tr>
	<%
		if (curPage==1) startPage=1;
	    for ( count = 1; count <= showPages;count++)
		{
		
		 
   			cntr = (startPage - 1) + count;
	 		if ((count == 1) && (hasPrevious))
			{  
			  %>
				<td colspan = 2>
			  	<A href="multipleusersearchdetailswithsave.jsp?milestoneUserNames=<%=milestoneUserNames%>&milestoneId=<%=milestoneId%>&milestoneUserIds=<%=milestoneUserIds%>&from=<%=from%>&accountId=<%=accId%>&fname=<%=ufname%>&lname=<%=ulname%>&jobType=<%=jobTyp%>&accsites=<%=orgName%>&usrgrps=<%=gName%>&ByStudy=<%=stTeam%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&mode=\"M\"">< <%--Previous*****--%><%=LC.L_Previous%> <%=totalPages%> > </A>
				&nbsp;&nbsp;&nbsp;&nbsp;
				</td>	
				<%
  			}	%>
		<td>
		<%
 		 if (curPage  == cntr)
		 {	 
	    	 %>	   
				<FONT class = "pageNumber"><%= cntr %></Font>
       		<%
       	}else
        {	%>		
		<A href="multipleusersearchdetailswithsave.jsp?milestoneUserNames=<%=milestoneUserNames%>&milestoneId=<%=milestoneId%>&milestoneUserIds=<%=milestoneUserIds%>&from=<%=from%>&accountId=<%=accId%>&fname=<%=ufname%>&lname=<%=ulname%>&jobType=<%=jobTyp%>&accsites=<%=orgName%>&usrgrps=<%=gName%>&ByStudy=<%=stTeam%>&page=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&mode=\"M\""><%= cntr%></A>
       <%}%>
		</td>
		<%	}
		if (hasMore)
		{   %>
	   <td colspan = 3 align = center>&nbsp;&nbsp;&nbsp;&nbsp;
	   <A href="multipleusersearchdetailswithsave.jsp?milestoneUserNames=<%=milestoneUserNames%>&milestoneId=<%=milestoneId%>&milestoneUserIds=<%=milestoneUserIds%>&from=<%=from%>&accountId=<%=accId%>&fname=<%=ufname%>&lname=<%=ulname%>&jobType=<%=jobTyp%>&accsites=<%=orgName%>&usrgrps=<%=gName%>&ByStudy=<%=stTeam%>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&mode=\"M\"">< <%--Next*****--%><%=LC.L_Next%> <%=totalPages%>></A>
		</td>	
		<%	}	%>
 
	   </tr>
	</table>

	 
	   
    </Form>
  <%//}//end of Parameter check

%>
<%  

}//end of if body for session
else{
%>
  <jsp:include page="timeout_childwindow.jsp" flush="true"/>
  <%
}

%>

</div>
<div class ="mainMenu" id = "emenu"> </div>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>
</html>
