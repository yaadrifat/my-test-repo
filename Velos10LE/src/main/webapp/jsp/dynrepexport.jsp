<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title><%=LC.L_Form_RptPreview%><%--Form Report Preview*****--%></title>
	<%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB,java.lang.reflect.*"%>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="dynrepB" scope="page" class="com.velos.eres.web.dynrep.DynRepJB" />
	<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
	<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
</head>
<% String sql="",tempStr="",sqlStr="",name="",selValue="",fmt="";
   int strlen=0,firstpos=-1,secondpos=-1,patId=0,studyId=0;
   	int seq=0;
	String mode = request.getParameter("mode");
	String value="",patientID="",studyNumber="",orderStr="",order="",filter="",dataOrder="",fltrDD="",mapSecId="",prevSecId="",headAlign="",footAlign="";
	StringBuffer htmlString=new StringBuffer();
	int personPk=0,studyPk=0,repId=0,index=-1;
	String calledfrom = null;
	DynRepDao dynDao=new DynRepDao();
	DynRepDao dynDaoFltr=new DynRepDao();
	ArrayList selectedfld=null;
	ArrayList dataList=new ArrayList() ;
	ArrayList tempList=new ArrayList();
	ArrayList lfltrIds= new ArrayList();
	ArrayList lfltrNames= new ArrayList();
	ArrayList lfltrStrs= new ArrayList();
	ArrayList mapSecNames=new ArrayList();
	ArrayList mapSecIds=new ArrayList();
	ArrayList repFldNames=new ArrayList();
	ArrayList secFldCols=new ArrayList();
	ArrayList repFldNamesSeq=new ArrayList();
	ArrayList tempVal=new ArrayList();
	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))
	{ 
	calledfrom = request.getParameter("calledfrom");
	String repIdStr=request.getParameter("repId");
	int userId=EJBUtil.stringToNum((String)tSession.getAttribute("userId"));
	 fmt =request.getParameter("exp");
	if (fmt==null) fmt="";
	if (repIdStr==null) repIdStr="";
	 HashMap FldAttr=new HashMap();
	 HashMap TypeAttr=new HashMap();
	 HashMap FltrAttr=new HashMap();
	 HashMap SortAttr= new HashMap();
	 HashMap RepAttr= new HashMap();
	 HashMap attributes;
	 //This is done to remove the session varibale from previous report.
	 attributes=(HashMap)tSession.getAttribute("attributes");
	 if (attributes==null) attributes=new HashMap();
	 if (attributes.isEmpty()){
	 repId=(new Integer(repIdStr)).intValue();
	 if (repId<=0)
	  {
	   %>
	   <script>this.window.close();
	        if (this.opener.window!="undefined")
	     	this.opener.window.close();
	   </script>
	  <%}
	dynDao=dynrepB.populateHash(repIdStr);
	attributes=dynDao.getAttributes();
	tSession.setAttribute("attributes",attributes);
	 }
	 if (attributes.containsKey("FldAttr")) FldAttr=(HashMap)attributes.get("FldAttr");
	 if (attributes.containsKey("TypeAttr")) TypeAttr=(HashMap)attributes.get("TypeAttr");
	 if (attributes.containsKey("FltrAttr")) FltrAttr=(HashMap)attributes.get("FltrAttr");
	 if (attributes.containsKey("SortAttr")) SortAttr=(HashMap)attributes.get("SortAttr");
	 if (attributes.containsKey("RepAttr")) RepAttr=(HashMap)attributes.get("RepAttr");
	 
	/*String[] fldCol=request.getParameterValues("fldCol");
	ArrayList fldCols=EJBUtil.strArrToArrayList(fldCol);
	String[] fldName=request.getParameterValues("fldName");
	ArrayList fldNames=EJBUtil.strArrToArrayList(fldName);
	String[] fldDispName=request.getParameterValues("fldDispName");
	String[] fldOrder=request.getParameterValues("fldOrder");
	String[] fldWidth=request.getParameterValues("fldWidth");*/
	ArrayList fldCols=new ArrayList();
	ArrayList fldNames=new ArrayList();
	ArrayList fldDispName=new ArrayList();
	ArrayList fldWidth=new ArrayList();
	if (FldAttr.containsKey("sessCol")) fldCols=(ArrayList)FldAttr.get("sessCol");
	if (FldAttr.containsKey("sessColName")) fldNames=(ArrayList)FldAttr.get("sessColName");
	if (FldAttr.containsKey("sessColDisp")) fldDispName=(ArrayList)FldAttr.get("sessColDisp");
	if (FldAttr.containsKey("sessColWidth")) fldWidth=(ArrayList)FldAttr.get("sessColWidth");
	
	filter=request.getParameter("filter");
	filter=(filter==null)?"":filter;
	if (filter.length()==0){
	if (FltrAttr.containsKey("filter")) filter=(String)FltrAttr.get("filter");
	if (filter==null) filter="";
	}
	filter=StringUtil.decodeString(filter);
	String formType=(String)TypeAttr.get("dynType");
	
	
	
	formType=(formType==null)?"":formType;
	formType=(formType=="study")?"S":formType;
	formType=(formType=="pat")?"P":formType;
	orderStr=request.getParameter("order");
	if (orderStr==null) orderStr="";
	if (orderStr.length()==0){
	if (SortAttr.containsKey("repOrder")) dataOrder=(String)SortAttr.get("repOrder");
	if (dataOrder==null) dataOrder="";
	if (dataOrder.length()>0){
	dataOrder=StringUtil.replace(dataOrder,"[$$]",",");
	dataOrder=StringUtil.replace(dataOrder,"[$]"," ");
	orderStr=" order by "+ dataOrder;
	}
	}
	String repHeader=request.getParameter("repHeader");
	String repFooter=request.getParameter("repFooter");
	if (repHeader==null) repHeader="";
	if (repHeader.length()==0){
	if (RepAttr.containsKey("repHdr")) repHeader=(String)RepAttr.get("repHdr");
	if (repHeader==null) repHeader="";
	}
	//Extract alignment String from report Header  
       if ((repHeader.indexOf("[VELCENTER]")>=0)||(repHeader.indexOf("[VELLEFT]")>=0)||(repHeader.indexOf("[VELRIGHT]")>=0))  
        {  
          headAlign=repHeader.substring(0,(repHeader.indexOf(":",0)));  
          repHeader=repHeader.substring(headAlign.length()+1);  
          }  
          else  
          {  
              headAlign=request.getParameter("headerAlign");  
           if (headAlign==null) headAlign="center";  
          }  
            
          if (headAlign.equals("[VELCENTER]")) headAlign="center";  
          if (headAlign.equals("[VELLEFT]")) headAlign="left";  
          if (headAlign.equals("[VELRIGHT]")) headAlign="right";  
          
       //end extracting header  

	
	if (repFooter==null) repFooter="";
	if (repFooter.length()==0){
	if (RepAttr.containsKey("repFtr")) repFooter=(String)RepAttr.get("repFtr");
	if (repFooter==null) repFooter="";
	}
	
	//Extract alignment String from report Footer  
       if ((repFooter.indexOf("[VELCENTER]")>=0)||(repFooter.indexOf("[VELLEFT]")>=0)||(repFooter.indexOf("[VELRIGHT]")>=0))  
        {  
          footAlign=repFooter.substring(0,(repFooter.indexOf(":",0)));  
          repFooter=repFooter.substring(footAlign.length()+1);  
            
          }  
          else {   
          footAlign=request.getParameter("footerAlign");  
          if (footAlign==null) footAlign="center";  
          }  
          if (footAlign.equals("[VELCENTER]")) footAlign="center";  
          if (footAlign.equals("[VELLEFT]")) footAlign="left";  
          if (footAlign.equals("[VELRIGHT]")) footAlign="right";  
          
       //end extracting footer  

	String formId=request.getParameter("formId");
	if (formId==null) formId="";
	if (formId.length()==0) {
	if (TypeAttr.containsKey("formId")) formId=(String)TypeAttr.get("formId");
	}

	for (int i=0;i<fldCols.size();i++){
	  tempStr=(String)fldCols.get(i) ;
	  name=(String)fldNames.get(i);
	   if (name==null) name="";
	  if (name.length()>=32) name=name.substring(0,30);
	  if (name.indexOf("?")>=0) 
	   name=name.replace('?','q');
	   if (name.indexOf("\\")>=0)
	   name=name.replace('\\','S');
	  /*if (order.length()>0) 
	    if (orderStr.length()>0){
	    orderStr=orderStr + " , \"" + name+"\" " + order;
	    }
	    else{
	    orderStr="\""+name+ "\" " + order;
	    }*/
	  if (tempStr.indexOf("|")>=0){
	  secFldCols.add(tempStr);
	  repFldNames.add((String)fldDispName.get(i));
	  repFldNamesSeq.add(new Integer(i));
	  tempStr=tempStr.replace('|',',');
	  
	  tempStr=StringUtil.replaceAll(tempStr, ",", "||'[VELSEP]'||");
	  
	    if (sqlStr.length()==0)
	  	sqlStr=" ("+tempStr+") as \"" + name+ "\"" ;
		 else
		sqlStr=sqlStr+",  ("+tempStr+") as \"" + name +"\"";
	}
	else{
		if (sqlStr.length()==0)  sqlStr=tempStr +" as \"" +name +"\"";
		else sqlStr=sqlStr + ", " + tempStr + " as \"" + name+"\"";
	}
		
		 
	}
	//System.out.println("******************************************Filter Right before" + filter);
	filter=(filter.equals("undefined")?"":filter);
	if (formType.equals("S"))
	if ((filter.trim()).length()>0){
	sql =" select ID,fk_patprot as PATPROT,(SELECT study_number FROM ER_STUDY WHERE pk_study=ID) AS study_num,  " + sqlStr + " from er_formslinear where fk_form="+formId +"  and " + filter  ;
	}else {
	sql =" select ID,fk_patprot as PATPROT,(SELECT study_number FROM ER_STUDY WHERE pk_study=ID) AS study_num,  " + sqlStr + " from er_formslinear where fk_form="+formId ;
	}
	
		
	if (formType.equals("P"))
	if ((filter.trim()).length()>0){
	sql =" select ID,(select patprot_patstdid from er_patprot where pk_patprot = a.fk_patprot) as PATPROT,(SELECT per_code FROM ER_PER WHERE pk_per=ID ) pat_code,  " + sqlStr + " from er_formslinear a where fk_form="+formId +"  and " + filter  ;
	} else {
	sql =" select ID,(select patprot_patstdid from er_patprot where pk_patprot = a.fk_patprot) as PATPROT,(SELECT per_code FROM ER_PER WHERE pk_per=ID ) pat_code,  " + sqlStr + " from er_formslinear a where fk_form="+formId  ;
	}
	//attach patient /study id criteira
	if (formType.equals("P")) {
	/* Attach Organizartion check */
	 sql = sql + " AND ID IN (  SELECT pk_per FROM ER_PER c ,ER_USERSITE d  WHERE c.fk_site = d. fk_site AND d. fk_user=" + userId+
	 " AND d.usersite_right>0 )";
	if (TypeAttr.containsKey("perId")) patId=EJBUtil.stringToNum((String)TypeAttr.get("perId"));
	if (patId>0)	sql = sql+" and Id="+patId; 
	else if (studyId>0)	sql=sql +" and Id in (select fk_per from er_patprot where fk_study="+studyId+ " and patprot_stat=1)";
	
	/*put a check to restrcit data for those studies which user don't have access to*/
        //sql=sql + " AND " + userId +" IN ( SELECT fk_user FROM ER_STUDYTEAM WHERE fk_user="+ userId +" AND " +  
	//" fk_study=(SELECT fk_study FROM ER_PATPROT WHERE pk_patprot=a.fk_patprot) " +  
	//"  AND ((Pkg_Util.f_checkRight(study_team_rights,'STUDYFRMACC','study_rights')=1)  OR (study_team_usr_type='S')))";
	//sql=sql + " AND " + userId +" IN ( SELECT fk_user FROM ER_STUDYTEAM WHERE fk_user="+ userId +" AND " +  
	//" ((a.fk_patprot is null) or (fk_study=(SELECT fk_study FROM ER_PATPROT WHERE pk_patprot=a.fk_patprot))) " +  
	//"  AND ((Pkg_Util.f_checkRight(study_team_rights,'STUDYFRMACC','study_rights')=1)  OR (study_team_usr_type='S')))";
	sql=sql + " AND ((a.fk_patprot is null) or (" + userId +" IN ( SELECT fk_user FROM ER_STUDYTEAM WHERE fk_user="+ userId +" AND " +  
	" ((fk_study=(SELECT fk_study FROM ER_PATPROT WHERE pk_patprot=a.fk_patprot))) " +  
	"  AND ((Pkg_Util.f_checkRight(study_team_rights,'STUDYFRMACC','study_rights')=1)  OR (study_team_usr_type='S')))))";
	}
	if (formType.equals("S")) {
	if (TypeAttr.containsKey("studyId")) studyId=EJBUtil.stringToNum((String)TypeAttr.get("studyId"));
	if (studyId>0) {	
	sql = sql+" and Id="+studyId;
	}
	else 
	{ 
	 /* block studies to which user does not have access*/
	 sql=sql+" AND "+ userId +" IN ( SELECT fk_user FROM ER_STUDYTEAM WHERE fk_user="+ userId + " and fk_study=ID " + 
	 " AND ((Pkg_Util.f_checkRight(study_team_rights,'STUDYFRMACC','study_rights')=1)  OR (study_team_usr_type='S'))) " ;
	} 
	}
	
	//end attach criteria
	if (orderStr.length()>0)	sql =sql+ "  " + orderStr ;
	else {
	if (formType.equals("P")) sql =sql+ "  " + "order by pat_code" ;
	else if (formType.equals("S")) sql =sql+ "  " + "order by study_num" ;
	 }
	//sql="select "+ sqlStr+" from er_formslinear where fk_form="+formId; 
	System.out.println(sql+formType);
	dynDao=dynrepB.getReportData(sql,(( String [] ) fldNames.toArray ( new String [ fldNames.size() ] )),formType);
	//dynDao.getReportData(sql,fldName);
	dataList=dynDao.getDataCollection();
	//System.out.println(dataList.size());
	//Retrieve section information
	//DynRepDao dyndao=dynrepB.getMapData(formId);
	DynRepDao dyndao=new DynRepDao();
	if (secFldCols!=null){
	if (secFldCols.size()>0){
	dyndao=dynrepB.getSectionDetails(secFldCols,formId);
	dyndao.preProcessMap();
	mapSecNames=dyndao.getMapColSecNames();
	mapSecIds=dyndao.getMapColSecIds();
	}
	}
		
	//end retrieve section
	 
htmlString.append("<BR>");
if (repHeader.length()>0){
htmlString.append("<table width='100%'><tr><td class='reportName' align='"+headAlign+"'><pre>"+repHeader+"</pre></td></tr></table>");


}
//htmlString.append("<table width='100%'>");

//htmlString.append("</table>");
//htmlString.append("<hr class='thickLine'>");
htmlString.append("<table border='1' width='100%' cellspacing='0' cellpadding='0'>");
if (formType.equals("S")){
htmlString.append("<TH class='reportClrHead' width='5%'>"+LC.L_Study_Number/*Study Number*****/+"</TH>");
}
else if (formType.equals("P")){
  htmlString.append("<TH class='reportClrHead' width='5%'>"+LC.Pat_Patient+" ID</TH>");
  }
 for(int count=0;count<fldNames.size();count++){
  if ((repFldNamesSeq.indexOf(new Integer(count)))>=0)
    continue; 
 if (((String)fldDispName.get(count)).length()==0){
 
 htmlString.append(" <TH class='reportClrHead' width='"+(String)fldWidth.get(count)+"%'>"+(String)fldDispName.get(count)+"</TH>");
}else {
htmlString.append(" <TH class='reportClrHead' width='"+(String)fldWidth.get(count)+"%'>"+(String)fldDispName.get(count)+"</TH>");
}}
if (mapSecIds!=null){
if (mapSecIds.size()>0){
for (int count=0;count<mapSecIds.size();count++){
	mapSecId=(String)mapSecIds.get(count);
	mapSecId=(mapSecId==null)?"":mapSecId.trim();
	if (mapSecId.length()>0){
    if ((mapSecId).equals(prevSecId)){continue;}else{
	htmlString.append(" <TH class='reportClrHead'> " + mapSecNames.get(count) + "</TH> ");
	}
	prevSecId=(String)mapSecIds.get(count);
} else {
	htmlString.append("<TH class='reportClrHead'></TH>");

}
}
}
}
for (int i=0;i<dataList.size();i++){
  tempList = (ArrayList) dataList.get(i);
  tempVal=new ArrayList();
  //System.out.println(tempList);
  if ((i%2)==0){
  htmlString.append("<tr class='browserEvenRow'>");
  }else{
   htmlString.append("<tr class='browserOddRow'>");
   }if (formType.equals("P")){
	personPk=EJBUtil.stringToNum((String)tempList.get(0));
	if (!(personPk==0)) {	
    	person.setPersonPKId(personPk);
    	person.getPersonDetails();
    	patientID = person.getPersonPId();
    	patientID = (patientID==null)?"":patientID;
	//System.out.println("patientID"+ patientID);
	} else {
	patientID=(String)tempList.get(1);
	if (patientID==null) patientID="-";
	}
	htmlString.append("<td width='5%'>"+patientID+"</td>");
	
	}
   if (formType.equals("S")){
	studyPk=EJBUtil.stringToNum((String)tempList.get(0));
	if (!(studyPk==0)) {	
    	studyB.setId(studyPk);
	studyB.getStudyDetails();
	studyNumber = studyB.getStudyNumber();
    	studyNumber = (studyNumber==null)?"":studyNumber;
	//System.out.println("studyNumber"+ studyNumber);
	}
	htmlString.append("<td width='5%'>"+studyNumber+"</td>");

	}
	if (tempList.size()==0) continue;
	 int maxTokens=0,tempCountToken=0;
  for (int j=2;j<tempList.size();j++){
  	ArrayList valList=new ArrayList();
	seq=0;
  	value=(String)tempList.get(j);
	if (value!=null){
	if (value.indexOf("[VELSEP]")>=0){
	
	value=StringUtil.replaceAll(value,"[VELSEP]","~");
	
	StringTokenizer valSt=new StringTokenizer(value,"~");
	tempCountToken=valSt.countTokens();
	//valList.add(valSt);
	tempVal.add(valSt);
	if (tempCountToken>maxTokens)	maxTokens=tempCountToken;
	continue;
	
	}
	}
	else value="-"; 
	if (value.indexOf("[VELSEP1]")>=0){
	value=value.substring(0,(value.indexOf("[VELSEP1]")));
	}	
  
  htmlString.append("<td width='"+(String)fldWidth.get(j-2)+"%'>"+value+"</td>");
}
//start
  if (repFldNames.size()>0){
  htmlString.append("<td><table border=1>");
 int colCounter=0,counter=0,toCount=0,currCount=0;
  String currSec="",prevSec="";
  String dispVal="";
StringTokenizer tempSt;

while (colCounter<repFldNames.size())
{
 currSec=(String)mapSecNames.get(colCounter);
 //do the processing here to display data for the repeated fields
 toCount=(colCounter);

 
if ((colCounter>0) && !currSec.equals(prevSec))
{
if (maxTokens>0){

for (int tCount=1;tCount<=maxTokens;tCount++){
//for (int tCount=1;tCount<=toCount;tCount++){

//while (currCount<toCount) {

htmlString.append("<tr>");
counter=0;

//while (counter<tempVal.size()){

while ((counter + currCount )<toCount){
   dispVal="";
   tempSt=((StringTokenizer)tempVal.get(counter+currCount));
      if (tempSt.hasMoreTokens()) dispVal=(String)tempSt.nextToken();
      dispVal=(dispVal==null)?"":dispVal;
      //if (dispVal.length()==0) dispVal="-";
       htmlString.append("<td>"+dispVal+"</td>");
       counter++;
 }
 htmlString.append("</tr>");
  
 }
 currCount=toCount;
 //counter=toCount;
 }
//end processing here 
htmlString.append("</table></td><td><table border='1'>");
if (((String)repFldNames.get(colCounter)).length()>=10){ 
htmlString.append("<th><font size='1'>" +((String)repFldNames.get(colCounter))+"</font></th>");
} else {
htmlString.append("<th><font size='1'>"+((String)repFldNames.get(colCounter))+"</font></th>");
}
}
  else{
 if (((String)repFldNames.get(colCounter)).length()>=10){ 
htmlString.append("<th><font size='1'>"+((String)repFldNames.get(colCounter))+"</font></th>");
}else{
htmlString.append("<th><font size='1'>"+((String)repFldNames.get(colCounter))+"</font></th>");
}
}
 colCounter++;
 prevSec=currSec;
}
/*if (maxTokens>0){
for (int tCount=1;tCount<=maxTokens;tCount++){%>
<tr>
<%
while (counter<tempVal.size()){
 dispVal="";
   tempSt=((StringTokenizer)tempVal.get(counter));
      if (tempSt.hasMoreTokens()) dispVal=(String)tempSt.nextToken();
      dispVal=(dispVal==null)?"":dispVal;
      if (dispVal.length()==0) dispVal="-";
      if ((colCounter>0) && !currSec.equals(prevSec)){
       %>
  <td><%=dispVal%></td></tr></table></td><td><table border="1"><tr>
<%}else{%>
<td><%=dispVal%></td>
<%}counter++;
 }}}*/
  //do this processing to display the elements of last section to the end of list


if ((toCount+1) == repFldNames.size())
	if (maxTokens>0){
	for (int tCount=1;tCount<=maxTokens;tCount++){
	//for (int tCount=1;tCount<=toCount;tCount++){
	
	//while (currCount<toCount) {
	
	
htmlString.append("<tr>");
	
	counter=0;
	//while (counter<tempVal.size()){
	
	while ((counter + currCount )<toCount+1){
	   dispVal="";
	   tempSt=((StringTokenizer)tempVal.get(counter+currCount));
	      if (tempSt.hasMoreTokens()) dispVal=(String)tempSt.nextToken();
	      dispVal=(dispVal==null)?"":dispVal;
	      //if (dispVal.length()==0) dispVal="-";
	      
	     htmlString.append("<td>"+ dispVal + "</td>");
	counter++;
	 }
	 htmlString.append("</tr>");

	 
	 }
	 currCount=toCount;
	 //counter=toCount;
	 }
	
	

//End for do this processing to display the elements of last section to the end of list


 
htmlString.append("</table></td>");
}//end for repFldNames.size()>0

htmlString.append("</tr>");

}

htmlString.append("</table><table width='100%'><hr class='thickLine'><tr height='20'></tr>");
//htmlString.append("<table width='100%'><hr class='thickLine'><tr height='20'></tr>");
//htmlString.append("<table width='100%'><hr class='thickLine'><tr height='20'></tr>");
//htmlString.append("</table>");
if (repFooter.length()>0){
htmlString.append("<table width='100%'><tr><td class='reportFooter' align='"+footAlign+"'><pre>"+repFooter+"</pre></td></tr></table>");
}
//System.out.println("htmlString" + htmlString);
} else {%>  //else of if body for session
	<jsp:include page="timeout_childwindow.jsp" flush="true"/>
<%
}
String contents = null;  
if (fmt.equals("W")){
//String contents = null;
String str = htmlString.toString();
contents = ReportIO.saveReportToDoc(str,"doc","dyndoc");%>
<META HTTP-EQUIV=Refresh CONTENT="0; URL=<%=contents%>">
<%} else if (fmt.equals("E")){
//String contents = null;
String str = htmlString.toString();
contents = ReportIO.saveReportToDoc(str,"xls","dynexcel");%>
<META HTTP-EQUIV=Refresh CONTENT="0; URL=<%=contents%>">
<%}else{%>
<%=htmlString%>
<%}
	/*Changes for: INF-22402 Date:10/04/2012 By:Yogendra Pratap Singh*/
	if(EJBUtil.isEmpty(calledfrom)){
		calledfrom = LC.L_Rpt_Central;
	}
	int downloadFlag=0;
	String downloadFormat = null;
	if (fmt.equals("W")){
		downloadFormat = LC.L_Word_Format;
		downloadFlag=1;
	}else if (fmt.equals("E")){
		downloadFormat = LC.L_Excel_Format;
		downloadFlag=1;
	}else{
		downloadFormat = LC.L_Printer_FriendlyFormat;
		downloadFlag=1;
	}
	String fileDnPath = null;
	String filename = null;
	if(null!=contents){
		filename = contents.substring(contents.indexOf("file="));
		fileDnPath =com.aithent.file.uploadDownload.Configuration.DOWNLOADFOLDER; 
	}
	%>
	<jsp:include page="userReportLogging.jsp" flush="true">
		<jsp:param 	value="<%=repId%>" 				name="repId"/>
		<jsp:param 	value="<%=filename %>"			name="fileName"/>
		<jsp:param 	value="<%=fileDnPath %>"		name="filePath"/>
		<jsp:param 	value="<%=calledfrom%>"			name="moduleName"/>
		<jsp:param 	value="" 						name="repXml"/>
		<jsp:param 	value="<%=downloadFlag%>"		name="downloadFlag"/>
		<jsp:param  value="<%=downloadFormat %>" 	name="downloadFormat"/>
	</jsp:include>
</body>
</html>
