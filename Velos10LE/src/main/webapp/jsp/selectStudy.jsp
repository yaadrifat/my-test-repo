<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>


<title><%=LC.L_Select_Study%><%--Select Study*****--%></title>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
	function removeAll(formobj)
	{
	openerform = formobj.openerform.value;
	window.opener.document.forms[openerform].selStudyIds.value='';
	window.opener.document.forms[openerform].selStudy.value='';

	 self.close();

	}

	function getStudyTitles(formobj)
	{
	selIds = new Array();
	selStudy = new Array(); //array of selected study
	totrows = formobj.totalrows.value;
	checked = false;
	var k=0;
	if (totrows==1) {
		if (formobj.chkStudy.checked) {
			checked=true;
		}else {
			checked=false;
		}
	} else {
		for (i=0;i<totrows;i++) {
			if (formobj.chkStudy[i].checked) {
				checked=true;
				break;
			} else {
				checked=false;
			}
		}
	}

	if (!checked) {
		alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>.");*****/
		return false;
	}

	if (totrows==1) {
		if (formobj.chkStudy.checked) {
			selValue = formobj.chkStudy.value;
			pos = selValue.indexOf("*");
			selIds[0] = selValue.substring(0,pos);
			selStudy[0] = selValue.substring(pos+1);
			//selStudy[0] = selValue;
		}
	} else {
		j=0;
		for (i=0;i<totrows;i++) {
			if (formobj.chkStudy[i].checked) {

				selValue = formobj.chkStudy[i].value;
				pos = selValue.indexOf("*");
				selIds[j] = selValue.substring(0,pos);
				selStudy[j] = selValue.substring(pos+1);
				j++;
				var len=j;

			}
		}
	}
openerform = formobj.openerform.value;
if (document.layers) {
   if (openerform=="null") {
     window.opener.document.div1.document.formlib.selStudyIds.value = selIds;
	 window.opener.document.div1.document.formlib.selStudy.value = selStudy;
   }
   else
   {	   //form is passed as a string
	   eval("window.opener.document.div1.document."+openerform+".selStudyIds.value='"+selIds+"'");
	   eval("window.opener.document.div1.document."+openerform+".selStudy.value='"+selStudy+"'");
   }
} else {
   if (openerform=="null") {
	  window.opener.document.formlib.selStudyIds.value=selIds;
	  window.opener.document.formlib.selStudy.value=selStudy;
   }
   else
	{ //form is passed as a string
	   eval("window.opener.document."+openerform+".selStudyIds.value='"+selIds+"'" ) ;
	   eval("window.opener.document."+openerform+".selStudy.value='"+selStudy+"'" ) ;
	}
}

self.close();
	}



	</SCRIPT>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.UserSiteDao"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:include page="include.jsp" flush="true"/>
<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1)
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>


<div class="popDefault" style="width:100%;">
<P class="sectionHeadings"> <%=MC.M_FrmLib_SelStd%><%--Form Library >> Select Study*****--%> </P>
<p class="defcomments"><A href="#" onClick="removeAll(document.study)"><%=MC.M_Rem_SelcStd%><%--Remove Already Selected Study*****--%></A></p>

<%
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))	{
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>

<%
  		String openerform = request.getParameter("openerform");
		String selectStudy=request.getParameter("selectStudy");//km
		selectStudy=StringUtil.decodeString(selectStudy); //Amarnadh

		if (StringUtil.isEmpty(selectStudy))
		{
			selectStudy = "";
		}

		selectStudy = ","+ selectStudy + ",";



		String usrId="";
	    usrId =  (String)tSession.getValue("userId");

		ArrayList arrStudyTitles=null;
		ArrayList arrStudyNum=null;
		ArrayList arrStudyIds=null;
		String studyTitle="";
		String studyId="";
		String studyNum="";
		StudyDao studyDao=new StudyDao();
		studyDao.getReportStudyValuesForUsers(usrId);
		arrStudyNum=studyDao.getStudyNumbers();
		arrStudyTitles=studyDao.getStudyTitles();
		arrStudyIds=studyDao.getStudyIds();
		int studyLen=arrStudyTitles.size();%>

		<Form  name="study" method="post" id="selStudyId" action="" onsubmit="if (getStudyTitles(document.study)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
    <table cellspacing="2" cellpadding="0" border=0 >
      <tr>
		<th ><%=LC.L_Study_Number%><%--Study Number*****--%></th>
			<th ><%=LC.L_Study_Title%><%--Study Title*****--%></th>
        <th > &nbsp;&nbsp;</th>
      </tr>
		<%if(studyLen==0)
		{
		}
		else
		{
		boolean flag=false;
		for(int i=0;i<studyLen;i++)
			{
			studyTitle = StringUtil.htmlEncode((String) arrStudyTitles.get(i) );

			if (StringUtil.isEmpty(studyTitle))
			{
				studyTitle = "";
			}

			studyId=arrStudyIds.get(i).toString();
			studyNum= StringUtil.htmlEncode((String) arrStudyNum.get(i));

			if (StringUtil.isEmpty(studyNum))
			{
				studyNum = "";
			}

			if(i%2==0)
			{%>

          <tr class=browserEvenRow>
			<%}
			   else
			{%>
				<tr class=browserOddRow>
			<%}%>
			<td><%=studyNum%></td>
			<td ><%=studyTitle%></td>
			<%
		      if ( selectStudy.indexOf("," + studyId+",") < 0)
		      {
		      	flag = false;
		      }
		      else
		      {
		      	flag = true;
		      }

			 if (flag==true){   //km %>
		    <td><input type="checkbox" checked name="chkStudy" value="<%=studyId%>*<%=studyNum%>"></td>
		    <%}else {%>
		    <td><input type="checkbox"  name="chkStudy" value="<%=studyId%>*<%=studyNum%>"></td>

		   </tr>
		<%}}
		}%>
			<tr>

		<jsp:include page="submitBar.jsp" flush="true">
				<jsp:param name="displayESign" value="N"/>
				<jsp:param name="formID" value="selStudyId"/>
				<jsp:param name="showDiscard" value="N"/>
		</jsp:include>

	</tr>
    </table>
	<Input type="hidden" name="checkedrows" value=0>
	<Input type="hidden" name="totalrows" value=<%=studyLen%> >
    <input type="hidden" name="openerform" value="<%=openerform%>">
</Form>

<%
}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>
</body>

</html>

