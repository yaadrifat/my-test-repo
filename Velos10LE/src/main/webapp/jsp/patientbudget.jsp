<%@page import="com.velos.eres.business.common.CodeDao"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=MC.M_PatOrComp_Bgt%><%--<%=LC.Pat_Patient%>/ Comparative Budget*****--%></title>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss_skin.js"></SCRIPT>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.esch.business.common.*,com.velos.eres.business.common.ReportDaoNew,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.group.GroupJB,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.budgetcal.impl.BudgetcalBean"%>
<%@ page import="com.velos.eres.business.common.TeamDao" %>
<jsp:useBean id="sessionmaint" scope="session"	class="com.velos.eres.service.util.SessionMaint" />
<jsp:useBean id="budgetB" scope="request"	class="com.velos.esch.web.budget.BudgetJB" />
<jsp:useBean id="lineitemB" scope="request"	class="com.velos.esch.web.lineitem.LineitemJB" />
<jsp:useBean id="ctrl" scope="request"	class="com.velos.eres.business.common.CtrlDao" />
<jsp:useBean id="grpRights" scope="request"	class="com.velos.eres.web.grpRights.GrpRightsJB" />
<jsp:useBean id="codeLst" scope="request"	class="com.velos.esch.business.common.SchCodeDao" />
<jsp:useBean id="budgetUsersB" scope="request"	class="com.velos.esch.web.budgetUsers.BudgetUsersJB" />
<jsp:useBean id="userB" scope="request"	class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="grpB" scope="request"	class="com.velos.eres.web.group.GroupJB" />
<jsp:useBean id="teamB" scope="request"	class="com.velos.eres.web.team.TeamJB" />
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript">
function getElementsById(sId)
{
	var outArray = new Array();
	if(typeof(sId)!='string' || !sId)
	{
		return outArray;
	};

	if(document.evaluate)
	{
		var xpathString = "//*[@id='" + sId.toString() + "']"
		var xpathResult = document.evaluate(xpathString, document, null, 0, null);
		while ((outArray[outArray.length] = xpathResult.iterateNext())) { }
		outArray.pop();
	}
	else if(document.all)
	{

		for(var i=0,j=document.all[sId].length;i<j;i+=1){
		//outArray[i] =  document.all[sId][i];}

		outArray[i] =  document.getElementById(sId);}//JM: 31May2010: #4952

	}else if(document.getElementsByTagName)
	{

		var aEl = document.getElementsByTagName( '*' );
		for(var i=0,j=aEl.length;i<j;i+=1){

			if(aEl[i].id == sId )
			{
				outArray.push(aEl[i]);
			};
		};

	};

	return outArray;
}

//Modified for Bug#8400 : Raviesh
function hideIcons(){
	var whichOne="none";

	if(document.getElementById('hide').style.display=="block"){
		document.getElementById('hide').style.display="none";
		document.getElementById('unhide').style.display="block";
		document.getElementById('imageChange').src="./images/UnHideIcons.gif";
		whichOne="none";
	}else{
		document.getElementById('unhide').style.display="none";
		document.getElementById('hide').style.display="block";
		document.getElementById('imageChange').src="./images/HideIcons.gif";
		whichOne="block";
	}
	var tdList = document.getElementsByName('calIcons');

	if (tdList.length == 0){
		tdList = document.getElementById('calIcons');
		tdList.style.display=whichOne;
	}else{
		for (var i=0; i<tdList.length; i++){
			tdList[i].style.display=whichOne;
		}
	}
}

function openCBSections(budgetId, bgtcalId, budgetTemplate){
	var pgRight = document.getElementById("pageRight").value;
	var inclIn = document.getElementById("includedIn").value;
	var src = document.getElementById("srcmenu").value;
	var selTab = document.getElementById("selectedTab").value;
	var mode = document.getElementById("mode").value;
	var bgtStat = document.getElementById("bgtStat").value;

	return openSections(pgRight,inclIn,src,budgetId,budgetTemplate,selTab,bgtcalId,mode,bgtStat,"combBgt");
}

function openCBRepeatLineItems(budgetId, bgtcalId, budgetTemplate){
	var mode = document.getElementById("mode").value;
	var bgtStat = document.getElementById("budgetStatus").value;

	return openRepeatLineItems(document.budget,mode,bgtcalId,bgtStat,budgetTemplate,"combBgt");
}
function openCBEditPersonnelCost(budgetId, bgtcalId, budgetTemplate){
	var mode = document.getElementById("mode").value;
	var bgtStat = document.getElementById("budgetStatus").value;

	return openEditPersonnelCostWin(document.budget,mode,bgtcalId,bgtStat,budgetTemplate,"combBgt");
}

function openCBDelete(bgtcalId){
	var pgRight = document.getElementById("pageRight").value;
	var bgtStat = document.getElementById("budgetStatus").value;
	// #5747 02/10/2011 @Ankit
	var budgetStatusDesc = document.getElementById("budgetStatusDesc").value;
	if (bgtStat=='F' || bgtStat=='T'){ var paramArray = [budgetStatusDesc];
	alert(getLocalizedMessageString("M_StatCnt_BeModf",paramArray))/*alert(budgetStatusDesc+" Status cannot be modified")*****/
    	return;
	}else return openDelete(bgtcalId,pgRight,"combBgt");
}

function refreshCalBud(mode,calledFrom,protocolId){
var budtypedd = document.getElementById('budgetType');
var indx = budtypedd.options.selectedIndex;
var budTypeId = budtypedd.options[indx].value;
location.href="protLinkedBudget.jsp?selectedTab=4&mode="+mode+"&calledFrom="+calledFrom+"&protocolId="+protocolId+"&budgetType="+budTypeId+"&srcmenu=tdmenubaritem4";
}

function openUncheckIndirects(formobj,budgetId){
	var bgtStat = document.getElementById("budgetStatus").value;
	
	var budgetStatusDesc = document.getElementById("budgetStatusDesc").value;
	var patCareDesc = document.getElementById("patCareDesc").value;
	if (bgtStat=='F' || bgtStat=='T'){ var paramArray = [budgetStatusDesc];
	alert(getLocalizedMessageString("M_StatCnt_BeModf",paramArray))/*alert(budgetStatusDesc+" Status cannot be modified")*****/
    	return;
	}else{var paramArray = [patCareDesc];
		var r=confirm(getLocalizedMessageString("M_Uncheck_Indirects",paramArray));/*alert("Uncheck Indirects from Patient Care LineItems?");*****/
	 if (r==false)
	   {
	 return false;
	    }
	 else{
	 $j.ajax({
		        url: 'removeIndirects.jsp',
		        type: 'GET',
		        async: false,
		        data: {
		            'budgetId': budgetId
		        },
		        success:(function (data){
		        	var response=$j(data);
					var retval = response.filter('#retval').val();
					if(retval>=0){
					alert(M_Data_SvdSucc);
					self.location.reload(true);}
					else
					{alert(M_DataNotSvd_Succ);
					return false;}
					
				}),
				error:function(response) { alert(data); return false; }
		    });	
	 }
	}
}

function openCBBudReport(counter, budgetId, bgtcalId){
	counter=counter-1;
	var ddList = document.getElementsByName("dBudReport");

	var w = ddList[counter].selectedIndex;
	var repName = ddList[counter].options[w].text;
	var reppk = ""+ddList[counter].options[w].value;
	var budTemplate = document.getElementById('budgetTemplate').value;

	win = window.open("budrepretrieve.jsp?repId="+reppk +"&budgetPk="+budgetId+"&repName="+repName+"&protId="+bgtcalId+"&budgetTemplate="+budTemplate,'BudgetReport','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');
}
</script>
<script>

function openBudReport(formobj,budgetId,bgtcalId)
{
	if(bgtcalId==0){
		alert("<%=MC.M_FuncNtAval_PlsSelIndCal%>");/*alert("This functionality is not available for selection -'All'.\nPlease select individual calendar.");*****/
	}else{
		reppk = formobj.dBudReport.value;
		var bgdType=document.getElementById("budgetTemplate").value; //Ak:Fixed Bug#3698 3rdMay2011
			
		var w = formobj.dBudReport.selectedIndex;
		var repName = formobj.dBudReport.options[w].text;
	
		win = window.open("budrepretrieve.jsp?repId="+reppk +"&budgetPk="+budgetId+"&repName="+repName+"&protId="+bgtcalId+"&budgetTemplate="+bgdType,'BudgetReport','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');
	}
}
</script>
</head>

<% String src;
src= request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
String selectedBgtcalId = "";
String mode = request.getParameter("mode");

//variable pageMode can have initial and final as values. initial in case the page is opened from a page other than this one and final in case it is being opened from a link on this page itself
String pageMode = request.getParameter("pageMode");
if(pageMode == null) pageMode = "initial";

// includedIn will be passed as P when this page is included in Protocol Calendar tabs to show the default budget

String includedIn = request.getParameter("includedIn");
if(StringUtil.isEmpty(includedIn))
{
 includedIn = "";
}

int pageRight = 0;
int appendxRight=0;

//*** Added specifically for combined budget
String from = request.getParameter("from");
if(StringUtil.isEmpty(from)){
	from = "";
}

int repId = 0;
if(from.equals("study")){
	repId = 159;
}else{
	repId = 110;
}
//***

//if(pageMode.equals("final")) {
	selectedBgtcalId = request.getParameter("bgtcal");

//}
int studyId = 0;
String topdivClass="";
String bottomdivClass="";
String tabFormTopNClass="";
String tabFormheightStyle = "";
String bottomDivStyle = "";
String budgetType = "";
String calledFrom = request.getParameter("calledFrom");
String protocolId = request.getParameter("protocolId");
CodeDao cdBudgetType = new CodeDao();
int budCostType = cdBudgetType.getCodeId("budtype","cost_type");
int budCoverageType = cdBudgetType.getCodeId("budtype", "coverage_type");
cdBudgetType.resetDao();
cdBudgetType.getCodeValues("budtype");
String budType = request.getParameter("budgetType");
ArrayList hideBudType = cdBudgetType.getCodeHide();
String hideBudgetType = "";
if(StringUtil.isEmpty(budType)){
	for(int i=0;i<hideBudType.size();i++){
		hideBudgetType= (String)hideBudType.get(i);
	if(hideBudgetType.equals("N")){
	budType = String.valueOf(cdBudgetType.getCId().get(i));
	break;
	  }
	}
}
int budTypeId= StringUtil.stringToNum(budType);
if(budTypeId==0){
	budTypeId=budCostType;
}
	budgetType = cdBudgetType.toPullDown("budgetType",budTypeId);
%>
<% if ( includedIn.equals("P")) {

	topdivClass="";
	tabFormTopNClass="class='BgtTopGrey'";
	bottomDivStyle = "";
	if(StringUtil.isEmpty(from)){
		bottomdivClass="BgtComb";
	} else {
		bottomdivClass="BgtComb BgtComb_CombinedBgt";
	}
%>

<jsp:include page="include.jsp" flush="true"/>
<jsp:include page="ui-include.jsp" flush="true"></jsp:include>
<body style="overflow:hidden;">
<% } else
	{

	topdivClass="tabDefTopN";
	tabFormTopNClass="class='tabFormTopN'";
	tabFormheightStyle = "style='height:738px;'";
	//bottomdivClass="tabFormBotNInv";
	bottomdivClass="BgtTabFormBotN";
	bottomDivStyle = "";

	%>

	<body style="overflow:hidden;" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0">

    <% } %>

<%if (!from.equals("study")){ %>
	<% if (! includedIn.equals("P")) { %>
	<jsp:include page="panel.jsp" flush="true">
		<jsp:param name="src" value="<%=src%>" />
	</jsp:include>
	<% } %>
	<% if (! includedIn.equals("P")) { %>
	<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
	<%} %>
	<script language="JavaScript" src="overlib.js" type="text/javascript"><!-- overLIB (c) Erik Bosrup --></script>
	<script language="JavaScript" src="overlib_shadow.js" type="text/javascript"><!-- overLIB (c) Erik Bosrup --></script>
	<script language="JavaScript" src="overlib_hideform.js" type="text/javascript"><!-- overLIB (c) Erik Bosrup --></script>
<%}%>
<script language="JavaScript" src="js/velos/ui-include.js"></script>
<script language="JavaScript" src="budget.js"></script>
<SCRIPT LANGUAGE="JavaScript" SRC="js/velos/fixedHeaders.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="js/velos/patientBudgetFixedHeaders.js"></SCRIPT>
 <%
	HttpSession tSession = request.getSession(true);

	String budgetStudyId="";
	String bgtStatus="", bgtStat="", budgetTotalTotalCost="",budgetTotalCost="";
	SchCodeDao schD = new SchCodeDao();
	int perCodeId = schD.getCodeId("category","ctgry_per");
	//String budgetTemplate;
	if (sessionmaint.isValidSession(tSession))
	{
		String uName = (String) tSession.getValue("userName");
		String userIdFromSession = (String) tSession.getValue("userId");
		String acc = (String) tSession.getValue("accountId");


		int accId = EJBUtil.stringToNum(acc);
		int userId = EJBUtil.stringToNum(userIdFromSession);
		int budgetId = EJBUtil.stringToNum(request.getParameter("budgetId"));
		String budgetTemplate = request.getParameter("budgetTemplate");
		budgetTemplate=(budgetTemplate==null)?"":budgetTemplate;

		if(budgetId!=0)
		{
			if (!from.equals("study")){
			ctrl.getControlValues("bgt_rights");

			ArrayList feature =  ctrl.getCValue();
			ArrayList ftrDesc = ctrl.getCDesc();
			ArrayList ftrSeq = ctrl.getCSeq();
			ArrayList ftrRight = new ArrayList();
			String strR;
			
			//copy budget is controlled by budget new group right
			GrpRightsJB budgetGrpRights = (GrpRightsJB) tSession.getValue("GRights");
			int budgetGrpRight = 0;

			int stdRight =0;

			userB=(com.velos.eres.web.user.UserJB)tSession.getAttribute("currentUser");
			String defaultGrp=userB.getUserGrpDefault();
			grpB.setGroupId(EJBUtil.stringToNum(defaultGrp));
			grpB.getGroupDetails();

			//** get user rights for the budget and put it in the session for use in other budget tabs
			String superRightStr = "";
			
			if ("1".equals(grpB.getGroupSupBudFlag())) {
				superRightStr=grpB.getGroupSupBudRights();
			}
			int rightLen = superRightStr == null ? 0 : superRightStr.length();

			ArrayList grpftrRight = new ArrayList();
			
			for (int counter = 0; counter <= (rightLen - 1);counter ++)
			{
				strR = String.valueOf(superRightStr.charAt(counter));
				grpftrRight.add(strR);
			}

			//grpRights bean has the methods for parding the rights so use its set methods
			grpRights.setGrSeq(ftrSeq);
			grpRights.setGrValue(feature);
			grpRights.setGrDesc(ftrDesc);

			if (rightLen > 0) {
				grpRights.setFtrRights(grpftrRight);
				ftrRight = grpftrRight;
				budgetGrpRight = Integer.parseInt(grpRights.getFtrRightsByValue("BGTDET"));
			    appendxRight = Integer.parseInt(grpRights.getFtrRightsByValue("BGTAPNDX"));
			}
			//Group right is page right 
			pageRight = budgetGrpRight;

			int hasAccess=0;
			// Check if user is in the scope selected and the scope allows budget access
			int bgtScopeRight = -1;
			String bgtScope = budgetB.getBudgetRScope();
			if (!StringUtil.isEmpty(bgtScope)) {			
				if ("A".equals(bgtScope)) {  // Account
					if (EJBUtil.stringToNum(acc) == EJBUtil.stringToNum(budgetB.getBudgetAccountId())) {
					    hasAccess=1;
					}
				} else if ("O".equals(bgtScope)) { // Org
				    if (EJBUtil.stringToNum(userB.getUserSiteId()) ==
				        EJBUtil.stringToNum(budgetB.getBudgetSiteId())) {
					    hasAccess=1;
				    }
				} else if ("S".equals(bgtScope)) { // Study Team
				    TeamDao teamDao = teamB.findUserInTeam(EJBUtil.stringToNum(budgetB.getBudgetStudyId()),
						EJBUtil.stringToNum(userIdFromSession));
					if (teamDao.getCRows() > 0) {
					    hasAccess=1;
					}
					if (hasAccess != 1) {
					    teamDao = teamB.getSuperUserTeam(EJBUtil.stringToNum(budgetB.getBudgetStudyId()));
					    ArrayList userIdList = teamDao.getUserIds();
					    if (userIdList.contains(Integer.valueOf(userId))) {
						    hasAccess=1;
					    }
					}
				}
			}
			if (hasAccess==1){
				ArrayList scopeftrRight = new ArrayList();
				String scopeRightStr = "";
				scopeRightStr= budgetB.getBudgetRights();
				
				rightLen = scopeRightStr.length();
				for (int counter = 0; counter <= (rightLen - 1);counter ++)
				{
					strR = String.valueOf(scopeRightStr.charAt(counter));
					scopeftrRight.add(strR);
				}

			 	bgtScopeRight = Integer.parseInt(
			            StringUtil.trueValue(scopeRightStr).substring(0,1));
			 	
			 	if ((grpB.getGroupSupBudFlag()).equals("1")) {
					//If user is super user of budgets he will get access no matter what the budget scope is
					pageRight = budgetGrpRight;
					ftrRight = grpftrRight;
				}else{
					// Override grpRight with scope right for patient budget
				    pageRight = bgtScopeRight;
				    appendxRight = Integer.parseInt(
				            StringUtil.trueValue(scopeRightStr).substring(1,2));
				    ftrRight = scopeftrRight;
				}
			} else{
				bgtScopeRight = 0;
			}

			// individual user access right overrides grpRight and scopeRight
		    String userRightStr = budgetUsersB.getBudgetUserRight(budgetId,userId);
		    if (!StringUtil.isEmpty(userRightStr)) {
		        rightLen = userRightStr == null ? 0 : userRightStr.length();
		        ftrRight = new ArrayList();
				for (int counter = 0; counter <= (rightLen - 1);counter ++)
				{
					strR = String.valueOf(userRightStr.charAt(counter));
					ftrRight.add(strR);
				}
				grpRights.setFtrRights(ftrRight);
				// Override grpRight & scope right with individual right for patient budget
				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("BGTDET"));
				appendxRight = Integer.parseInt(grpRights.getFtrRightsByValue("BGTAPNDX"));
		    }

		  	//set the grpRights Bean in session so that it can be used in other
			//budget pages directly to get the pageRight.
			grpRights.setFtrRights(ftrRight);
			tSession.putValue("BRights",grpRights);
			
		}else{
			/*Combined Budget*/
			pageRight= EJBUtil.stringToNum(request.getParameter("pageRight"));
			appendxRight= EJBUtil.stringToNum(request.getParameter("appendxRight"));
		}
		}
		if ((mode.equals("M") && pageRight >=4) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 )) )
		{
	%>
<DIV class="<%=topdivClass%>" id="div1">
		<%
  	    if(budgetId>0)
  	    {
	  	    budgetB.setBudgetId(budgetId);
			budgetB.getBudgetDetails();
			budgetStudyId=budgetB.getBudgetStudyId();
			session.setAttribute("studyId", budgetStudyId);
			budgetTemplate = budgetB.getBudgetType();

			studyId = EJBUtil.stringToNum(budgetStudyId);
		}

  	     %>
  	     <% if (! includedIn.equals("P")) { %>

			<jsp:include page="budgettabs.jsp" flush="true">
				<jsp:param name="budgetTemplate" value="<%=budgetTemplate%>" />
				<jsp:param name="mode" value="<%=mode%>" />
				<jsp:param name="studyId" value="<%=budgetStudyId%>" />
			</jsp:include>
		<% } %>
</DIV>
<DIV <%=tabFormTopNClass%> id="div2" <%=tabFormheightStyle%> ><%
	if(budgetId==0) {
	%> <jsp:include page="budgetDoesNotExist.jsp" flush="true" /> <%
	} else {
		

		//** end of setting pageRight

			int i = 0;
			int j = 0;
			int sectionLineItemCount = 0;
			int prevSecLICount = 0;//SV, 8/30/04, added as part of fix for #1706.
				String budgetStatusDesc = "";

			BudgetDao budgetDao = budgetB.getBudgetInfo(budgetId);

			ArrayList bgtNames = budgetDao.getBgtNames();
			ArrayList bgtStats = budgetDao.getBgtStats();
			ArrayList bgtStudyTitles = budgetDao.getBgtStudyTitles();
			ArrayList bgtStudyNumbers = budgetDao.getBgtStudyNumbers();
			ArrayList bgtSites = budgetDao.getBgtSites();
			ArrayList bgtCurrs = budgetDao.getBgtCurrs();

			ArrayList bgtStatDescs= budgetDao.getBgtCodeListStatusesDescs();

			String bgtName =(String) bgtNames.get(0);
			bgtStatus = (String) bgtStats.get(0);

			budgetStatusDesc = (String) bgtStatDescs.get(0);
			SchCodeDao sch=new SchCodeDao();
		    int patCare= sch.getCodeId("category", "ctgry_patcare");
		    String patCare_desc= sch.getCodeDescription(patCare);

			String bgtStatCode = (String) bgtStats.get(0);
			String bgtStudyTitle = (String) bgtStudyTitles.get(0);
			String bgtStudyNumber = (String) bgtStudyNumbers.get(0);
			String bgtSite = (String) bgtSites.get(0);


			String currency = "";

			String bgtName_Disp = "";
			String bgtStudyNumber_Disp = "";
			String bgtSite_Disp = "";
			//String bgtStudyTitle_Disp = "";

			if (bgtCurrs.size() >0){

				String bgtCurr = (String) bgtCurrs.get(0);


				if(bgtCurr==null)
					bgtCurr ="0";
				int codeId = Integer.parseInt(bgtCurr);

				currency = codeLst.getCodeDescription(codeId);


			}


			boolean sectionFlag = true;

			bgtName = (bgtName == null || bgtName.equals(""))?"-":bgtName;
			bgtName = StringUtil.stripScript(bgtName);
			bgtStat = bgtStatus;
			bgtStat = (bgtStat == null || bgtStat.equals(""))?"-":bgtStat;
			if(bgtStat.equals("F")) {
				bgtStat = "Freeze";
			} else if(bgtStat.equals("W")) {
				bgtStat = "Work in Progress";
			}else if(bgtStat.equals("T")) {
				bgtStat = "Template";
				}

			bgtStudyTitle = (bgtStudyTitle == null || bgtStudyTitle.equals(""))?"-":bgtStudyTitle;
			bgtStudyNumber = (bgtStudyNumber == null || bgtStudyNumber.equals(""))?"-":bgtStudyNumber;
			bgtSite = (bgtSite == null || bgtSite.equals(""))?"-":bgtSite;
			BudgetcalDao budgetcalDao = new BudgetcalDao();
			budgetcalDao.getAllBgtCals(budgetId,0);
			ArrayList bgtcalIds = budgetcalDao.getBgtcalIds();
			ArrayList bgtcalProtNames = budgetcalDao.getBgtcalProtNames();
			ArrayList bgtcalProtIds = budgetcalDao.getBgtcalProtIds();
			String currentBgtcalId = "";
			int bgtcalId = 0;
			String selectedBudgetProtocolID = "";
			String studyWithSelectedBudgetProtocolID  = "";
			String selectedCalendarStatus = "";

			if(bgtcalIds.size() > 1){
				//currentBgtcalId = ((Integer) bgtcalIds.get(1)).toString();
				if (includedIn.equals("P")) {
					bgtcalId = ((Integer) bgtcalIds.get(1)).intValue();
				}
				selectedBudgetProtocolID = (String) bgtcalProtIds.get(1);
			}

			String protDD = MC.M_NoCalSelected/*"No Calendar Selected"*****/;

			if(bgtcalIds.size() > 1) {
				protDD = "<select id='bgtcal' name='bgtcal' style='width:177px;'>";

				//if (from.equals("study")){
					protDD = protDD + "<option value='0' selected>"+LC.L_All/*All*****/+"</option>";
				//}
				String bgtcalProtName = "", bgtcalProtTitle = "";
				for(i=0;i<bgtcalIds.size();i++) {
					if (!bgtcalProtIds.get(i).equals("0")) {
						bgtcalProtName = (String)bgtcalProtNames.get(i);
						bgtcalProtTitle ="";

						if ((!StringUtil.isEmpty(bgtcalProtName)) && bgtcalProtName.length() > 30){
							bgtcalProtName = (bgtcalProtName.substring(0, 27)) + "...";
							bgtcalProtTitle = "title ='"+bgtcalProtNames.get(i)+"'";
		                }
						
						if(pageMode.equals("initial")) {
							if(i == 0) {
								protDD = protDD + "<option value=" + bgtcalIds.get(i);
								if (!from.equals("study")){
									protDD = protDD + " selected" ;
									bgtcalId = ((Integer) bgtcalIds.get(i)).intValue();
									selectedBudgetProtocolID = (String) bgtcalProtIds.get(i);
								}else{
									bgtcalId =0;
									selectedBudgetProtocolID = "0";
								}
							} else {
								protDD = protDD + "<option value=" + bgtcalIds.get(i);
							}
						} else {
							currentBgtcalId = ((Integer) bgtcalIds.get(i)).toString();
							if(selectedBgtcalId.equals(currentBgtcalId)){
								protDD = protDD + "<option value=" + bgtcalIds.get(i);
								if (!from.equals("study")){
									protDD = protDD + " selected" ;
									bgtcalId = ((Integer) bgtcalIds.get(i)).intValue();
									selectedBudgetProtocolID = (String) bgtcalProtIds.get(i);
								}else{
									bgtcalId =0;
									selectedBudgetProtocolID = "0";
								}

							} else {
								protDD = protDD + "<option value=" + bgtcalIds.get(i);
							}
						}
						protDD = protDD + " " + bgtcalProtTitle + ">" + bgtcalProtName + "</option>";
					}
				} //for loop
				protDD = protDD + "</select>";
			 } else {
				currentBgtcalId = ((Integer) bgtcalIds.get(0)).toString();
				bgtcalId = ((Integer) bgtcalIds.get(0)).intValue();
				selectedBudgetProtocolID = (String) bgtcalProtIds.get(0);
				selectedBgtcalId =""+ bgtcalId;
			 }

			%>
			<input type="hidden" id="calDD" name="calDD" value="<%=protDD%>" />
			<%

			 	eventassocB.setEvent_id(EJBUtil.stringToNum(selectedBudgetProtocolID));
			 	eventassocB.getEventAssocDetails();
			 	studyWithSelectedBudgetProtocolID = eventassocB.getChain_id();
			 	//selectedCalendarStatus = eventassocB.getStatus();
				//KM-#DFin9
				String calStatusPk = eventassocB.getStatCode();
				SchCodeDao scho = new SchCodeDao();
				if (calStatusPk != null)
					selectedCalendarStatus = scho.getCodeSubtype(EJBUtil.stringToNum(calStatusPk)).trim();

			 	if (StringUtil.isEmpty(studyWithSelectedBudgetProtocolID))
			 	{
			 		studyWithSelectedBudgetProtocolID = "0";
			 		selectedCalendarStatus = "";
			 	}

%>
<%if(from.equals("study")){ %>
<Form id="budget" name="budget" method="post">
<%}else {%>
<Form id="budget" name="budget" method="post" action="patientbudget.jsp?mode=<%=mode%>&budgetId=<%=budgetId%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&budgetTemplate=<%=budgetTemplate%>">
<%}%>
<table id="hideTable" width="100%" cellpadding="0" cellspacing="0" border="0" class="midAlign">
	<tr>
	<%if ((!(bgtStat ==null)) && (bgtStat.equals("Freeze") || bgtStat.equals("Template") )&&(mode.equals("M"))) { Object[] arguments = {budgetStatusDesc}; %>
		<td >
		<P id="comment" name="comment" class="defComments"><FONT class="Mandatory"><%=VelosResourceBundle.getMessageString("M_BgtStat_CntChgBgt",arguments)%><%--Budget Status is '<%=budgetStatusDesc%>'.
		You cannot make any changes to the budget.*****--%></FONT></P>
	<%}%>
	<%if (pageRight == 4) {%>
		<td >
			<P id="comment" name="comment" class="defComments"><FONT class="Mandatory"><%=MC.M_YouOnly_ViewPerm%><%--You have only View permission*****--%></FONT></P>
		</td>
	<%}%>
	</tr>
</table>
	<input type="hidden" name="srcmenu" size=20 value=<%=src%>>
	<input type="hidden" name="selectedTab" size=20 value=<%=selectedTab%>>
	<input type="hidden" name="mode" size=20 value=<%=mode%>>
	<input type="hidden" name="pageMode" size=20 value="final">
	<input type="hidden" id="from" name="from" size=20 value=<%=from%>>
	<input type="hidden" id="budgetId" name="budgetId" size=20 value=<%=budgetId%>>
	<input type="hidden" id="bgtStat" name="bgtStat" size=20 value="<%=bgtStat%>"/>
	<input type="hidden" id="budgetStatus" name="budgetStatus" size=20 value="<%=bgtStatus%>"/>
	<input type="hidden" id="budgetStudyId" name="budgetStudyId" size=20 value="<%=budgetStudyId%>">
	<input type="hidden" name="currentBgtcalId" size=20 value=<%=currentBgtcalId%>>
	<input type="hidden" id="pageRight" name="pageRight" size=20 value=<%=pageRight%>>
	<input type="hidden" name="selectedBudgetProtocolID" size=20 value=<%=selectedBudgetProtocolID%>>
	<input type="hidden" name="studyWithSelectedBudgetProtocolID" size=20 value=<%=studyWithSelectedBudgetProtocolID%>>
	<input type="hidden" name="selectedCalendarStatus" size=20 value=<%=selectedCalendarStatus%>>
	<!-- #5747 02/10/2011 @Ankit-->
	<input type="hidden" name="budgetStatusDesc" id="budgetStatusDesc" value="<%=budgetStatusDesc%>">
	<input type="hidden" name="patCareDesc" id="patCareDesc" value="<%=patCare_desc%>">

	<table id="infoBudget" width="100%" cellspacing="0" cellpadding="0" border="0" valign="bottom">
		<tr valign="middle">
		<%if (! includedIn.equals("P")) { %>
			<td width="15%"><%=LC.L_Budget_Name%><%--Budget Name*****--%>:&nbsp;<BR/>
		<%
		 //JM: 14Aug2008: #3727
		 //BK: 06JUL2010: #5072 LENGTH IS REDUCED TO 22 CHARS IN ORDER TO REMOVE VISALISATION PROBLEM
			if(bgtName.length() > 22){
		   		bgtName_Disp = bgtName.substring(0,22);
		   		bgtName_Disp = bgtName_Disp + "...";
		   	%>
            <a href="#" title ="<%=bgtName%>"><%=bgtName_Disp%></a>
			<!-- <a href="#" onmouseover="return overlib('<%=bgtName%>',CAPTION,'Budget Name:' ,LEFT , ABOVE);" onmouseout="return nd();"><%=bgtName_Disp%></a> -->
			<%}else{%>
			<%=bgtName%>
			<%}%>

			</td>

			<td width="12%"><%=LC.L_Bgt_Status%><%--Budget Status*****--%>:&nbsp;<BR/><%=budgetStatusDesc%></td>
			<td width="17%">
			<table border="0" style="border:none" >
				<tr valign="middle">
					<td>
						<%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>:&nbsp;<BR/>
        	<% if (!StringUtil.isEmpty(budgetStudyId)) { %>
				<a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=budgetStudyId%>">
			<%
	       		 //JM: 14Aug2008: #3727
	       		 //BK: 06JUL2010: #5072 LENGTH IS REDUCED TO 22 CHARS IN ORDER TO REMOVE VISALISATION PROBLEM
				if(bgtStudyNumber.length() > 22){
			   		bgtStudyNumber_Disp = bgtStudyNumber.substring(0,22);
			   		bgtStudyNumber_Disp = bgtStudyNumber_Disp + "...";
			   		%>
		   		<a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=budgetStudyId%>" title = "<%=bgtStudyNumber%>"><%=bgtStudyNumber_Disp%>
				<%}else{%>
				<%=bgtStudyNumber%>
				<%}%>
				</a>
				</td>
				<td>
				<img alt= "<%=LC.L_Study_Title%>" src="../images/jpg/help.jpg" border="0"
				title= "<%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>: <%=StringUtil.escapeSpecialCharJS(bgtStudyTitle)%>" ></img>
       		 <% } else {  %>
       			 -
        	<% }  %>
        		</td>
        		</tr>
        	</table>
			</td>

			<td width="13%"><%=LC.L_Organization%><%--Organization*****--%>:&nbsp;<BR/>
			<%
		        //JM: 14Aug2008: #3727
		        //BK: 06JUL2010: #5072 LENGTH IS REDUCED TO 22 CHARS IN ORDER TO REMOVE VISALISATION PROBLEM
				if(bgtSite.length() > 22){
			   		bgtSite_Disp = bgtSite.substring(0,22);
			   		bgtSite_Disp = bgtSite_Disp + "...";
			   		%>
				<a href="#" title="<%=bgtSite%>"><%=bgtSite_Disp%></a>
				<%}else{%>
		        	<%=bgtSite%>
				<%}%>
			</td>
			<% if (!("Y".equals(budgetB.getBudgetCombinedFlag()))){ %>
			<td align="center" width="5%">
				<font size = -2 color="#696969"><b><%//=LC.L_Copy%><%--Copy*****--%></b></font>
				<A onClick="return f_check_perm(<%=pageRight%>,'N')"
				href="copybudget.jsp?from=initial&srcmenu=<%=src%>&budgetId=<%=budgetId%>"><img src="./images/copy.gif" alt="<%=LC.L_Copy_Bgt%><%--Copy Budget*****--%>" title="<%=LC.L_Copy%>" border ="0" align=absbotton></A></td>
			<% } %>

		<%} //if includedin=P
	%>
		<%if(!includedIn.equals("P")){
			//for reports dropdown

			ReportDaoNew repDao = new ReportDaoNew();
			if(budTypeId==budCostType)
				repDao.getReports("pat_bud",accId);
			else
				repDao.getReports("cov_bud",accId);
			String strBudRep = "";

			strBudRep = EJBUtil.createPullDownWithStrNoSelect("dBudReport","",repDao.getPkReport(),repDao.getRepName());
		%>
		<td align="center" width="20%">
			<table border="0" style="border:none">
				<tr valign="middle">
					<td>
						<%=LC.L_Reports%><%--Reports*****--%><BR/>
						<%=strBudRep%>
					</td>
					<td align="left">
						<A href="#"	onClick="return openBudReport(document.budget,<%=budgetId%>,<%=bgtcalId%>);">
						<img src="../images/jpg/preview.gif" align="absmiddle" border="0" align="absmiddle" border="0"	alt="<%=LC.L_Display_Report%><%--Display Report*****--%>" title="<%=LC.L_Display_Report%><%--Display Report*****--%>"></A>
					</td>
				</tr>
			</table>
		</td>
		<%}%>
	</tr>
	<!-- <tr>
		<td colspan="4"><font size="1"><B> <%=LC.Std_Study%> Title:</B>&nbsp;&nbsp;<%if (bgtStudyTitle.length() > 125){%><%=bgtStudyTitle.substring(1,125)%>...<%}else {%><%=bgtStudyTitle%><%}%></font></td>

	</tr> -->

</table>
<table id="infoBudget1" width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr vAlign="middle">
<%if (includedIn.equals("P")) { %>

	<td width="28%">
	<table border="0" style="padding:0 0;">
		<tr>
		<td>
			<%=LC.L_Budget_Name%><%--Budget Name*****--%>:&nbsp;<BR/>
		<%
		 //JM: 14Aug2008: #3727
		 //BK: 06JUL2010: #5072 LENGTH IS REDUCED TO 22 CHARS IN ORDER TO REMOVE VISALISATION PROBLEM
			if(bgtName.length() > 22){
		   		bgtName_Disp = bgtName.substring(0,22);
		   		bgtName_Disp = bgtName_Disp + "...";
		   	%>
            <a href="#" title ="<%=bgtName%>"><%=bgtName_Disp%></a>
			<!-- <a href="#" onmouseover="return overlib('<%=bgtName%>',CAPTION,'Budget Name:' ,LEFT , ABOVE);" onmouseout="return nd();"><%=bgtName_Disp%></a> -->
			<%}else{%>
			<%=bgtName%>
			<%}%>

			&nbsp;
		</td>
		<td>		
			<A href="#" onclick="openBudget(<%=budgetId%>,'P','<%=from%>')"><img title="<%=LC.L_Edit%>" src="./images/edit.gif" border ="0"><%//=LC.L_Edit_Dets%><%--Edit Details*****--%></A></td>
		</td>
		<%if(!from.equals("study")){ %>
		<td>
		<%=budgetType%>
		<button type="button" onClick="refreshCalBud('<%=mode%>','<%=calledFrom%>',<%=protocolId%>);"><%=LC.L_Go%></button>
		<button type="button" onclick="openUncheckIndirects(document.budget,<%=budgetId%>);">Remove Indirects</button>
		</td>
		<%} %>
		</tr>
	</table>
	</td>
	<td width="8%"> <%=LC.L_Bgt_Status%><%--Budget Status*****--%>:&nbsp;<BR/><%=budgetStatusDesc%></td>
	<td  width="1%">&nbsp;</td>
	<%if(!from.equals("study")){
			//for reports dropdown
			ReportDaoNew repDao = new ReportDaoNew();
		   if(budTypeId==budCostType)
			   repDao.getReports("pat_bud",accId);
		   else
				repDao.getReports("cov_bud",accId);
			String strBudRep = "";
			strBudRep = EJBUtil.createPullDownWithStrNoSelect("dBudReport","",repDao.getPkReport(),repDao.getRepName());
		%>
	<td align="center" width="14%">
		<table>
			<tr>
			<td>
				<font size ="1"><b><%=LC.L_Reports%><%--Reports*****--%></b></font>
				<%=strBudRep%>
			</td>
			<td>
				<A href="#"	onClick="return openBudReport(document.budget,<%=budgetId%>,<%=bgtcalId%>);">
				<img src="../images/jpg/preview.gif" align="absmiddle" border="0"	alt="<%=LC.L_Display_Report %><%-- Display Report*****--%>" title="<%=LC.L_Display_Report %><%-- Display Report*****--%>"></A>
			</td>
			</tr>
		</table>
		</td>
		<%}%>
	<%if(from.equals("study")){%>
	<td  width="15%" colspan=3> <%=LC.L_Organization%><%--Organization*****--%>:&nbsp;<BR/>
	<%
	   //JM: 14Aug2008: #3727
	   //BK: 06JUL2010: #5072 LENGTH IS REDUCED TO 22 CHARS IN ORDER TO REMOVE VISALISATION PROBLEM
		if(bgtSite.length() > 22){
	   		bgtSite_Disp = bgtSite.substring(0,22);
	   		bgtSite_Disp = bgtSite_Disp + "...";
		%>
		<a href="#" title="<%=bgtSite%>"><%=bgtSite_Disp%></a>
		<%}else{%>
	       	<%=bgtSite%>
		<%}%>
	</td>
	<%} %>
<%} %>
<% if (! includedIn.equals("P")) { %>

		<td  width="27%">
		<%if(bgtcalIds.size() > 1) {%>
			<B><%=LC.L_Select_Cal%><%--Select Calendar*****--%> </B> <BR><%=protDD%>
			<%=budgetType%>
			<button type="submit"><%=LC.L_Go%></button>
			<button type="button" onclick="openUncheckIndirects(document.budget,<%=budgetId%>);">Remove Indirects</button>
		<%}else {%>
			<%=protDD%>
		<%} %> </td>
   <%}else{
		if (from.equals("study")) { %>
		</tr>
	</table>
	<table id="infoCal" width="100%" border="0" style="padding:0 0;">
		<tr >
			<td width="27%" colspan="3"><B>
			<%if(bgtcalIds.size() > 1) {%>
				<%=LC.L_Select_Cal%><%--Select Calendar*****--%> </B> <%=protDD%>
				<%=budgetType%>
				<A type="submit" href="#" onClick=" var repId=0;
					var dd = document.getElementById('bgtcal');
					var indx = dd.options.selectedIndex;
					document.getElementById('bgtcalId').value = dd.options[indx].value;
					var budTypeId=0;
					if(document.getElementById('budgetType')){
					var budtypedd = document.getElementById('budgetType');
					    indx = budtypedd.options.selectedIndex;
					    budTypeId = budtypedd.options[indx].value;}
					else
					    budTypeId=<%=budTypeId%>;
					if (document.getElementById('bgtcalId').value =='0'){
						if(budTypeId==<%=budCostType%>)
						repId=159;
						else
						repId=184;
					}else{
						if(budTypeId==<%=budCostType%>)
						repId=110;
						else
						repId=183;
					}

					var act='repId='+repId+'&repName=&budgetPk='+document.getElementsByName('budgetId')[0].value+'&protId='+document.getElementById('bgtcalId').value;
					act=act+'&mode=M&budgetTemplate='+document.getElementById('budgetTemplate').value;
					act=act+'&bgtStat='+document.getElementById('budgetStatus').value+'&includedIn=P&pageRight='+document.getElementById('pageRight').value;
					act=act+'&from=study';

					var ao=new VELOS.ajaxObject('budrepretrieve.jsp', {
						urlData:act  ,
						reqType:'POST',
						cache:false,
						outputElement: 'repDisplay',
						success: '$E.onContentReady(patientBudgetFixedHeader());'
						}
					);
					ao.startRequest();"><%=LC.L_Go%></A> 
				<button type="button" onclick="openUncheckIndirects(document.budget,<%=budgetId%>);">Remove Indirects</button>
				<!--input type="image" src="../images/jpg/newgo.gif" align="absmiddle"	border="0"-->
			<%} else {%>
				<%=protDD%>
			<%} %>
			</td>
		<%	}
	}%>

	<%if (! includedIn.equals("P")) {
		if (!((!(bgtStat ==null)) && ( bgtStat.equals("Freeze") || bgtStat.equals("Template")   )&&(mode.equals("M")))) {%>

		<td   align="center" width="4%">
			<font size = -2 color="#696969"><b><%//=LC.L_Std_Cal%><%--<%=LC.Std_Study%> Cal*****--%></b></font>
			<%--Fixed for Bug# 7356:Akshi vinayak--%>
			<A href="javascript:void(0);" onclick="openStudyprot(document.budget, <%=pageRight%>)"><img src="./images/studysearch.jpg" alt="<%=MC.M_SelCal_Std%><%--Select Calendar from <%=LC.Std_Study%>*****--%>" title="<%=LC.L_Std_Cal%>" border ="0" align=absbotton></A>
		</td>

		<td  align="center" width="4%">
			<font size = -2 color="#696969"><b><%//=LC.L_Lib_Cal%><%--Lib. Cal*****--%></b></font>
			<%--Fixed for Bug# 7356:Akshi vinayak--%>
			<A href="javascript:void(0);" onclick="openLibprot(document.budget, <%=pageRight%>,'','','','','','')"><img src="./images/library.gif" alt="<%=MC.M_SelCal_Lib%><%--Select Calendar from Library*****--%>" title="<%=LC.L_Lib_Cal%>" border ="0" align=absbotton></A>
		</td>

		<td  align="center" width="4%">
			<font size = -2 color="#696969"><b><%//=LC.L_Del_Cal%><%--Delete Cal*****--%></b></font>
			<%--Fixed for Bug# 7356:Akshi vinayak--%>
			<A href="javascript:void(0);" onclick="openBgtProt(document.budget, <%=pageRight%>)"><img src="./images/disableformat.gif" alt="<%=LC.L_Del_Selected%><%--Delete Selected*****--%>" title="<%=LC.L_Del_Cal%>" border = "0" align=absbotton></A>
		</td>
		<%}%>
	<% }//include in P %>
	<%String labelStr ="";

	if (from.equals(("study"))){%>
	<td  align="center" width="4%">
		<label id="hide" style="display:block"><font size = -2 color="#696969"><b><%//=LC.L_Hide_Icons%><%--Hide Icons*****--%></b></font></label>
		<label id="unhide" style="display:none"><font size = -2 color="#696969"><b><%//=LC.L_Unhide_Icons%><%--Unhide Icons*****--%></b></font></label>
		<%--Fixed for Bug# 7356:Akshi vinayak--%>
		<A HREF="javascript:void(0);" onClick="hideIcons();">
		<img id="imageChange" src="./images/HideIcons.gif" title="<%=LC.L_Hide_Icons%>" alt="<%=LC.L_HideOrUnhide_Icons %><%-- Hide/Unhide Icons*****--%>" border = "0" align=absbotton></A></td>

	<%} %>
	<%if (from.equals(("study"))){%>
	<td  align="center" width="4%">
	<font size = -2 color="#696969"><b><%//=LC.L_Appendix%><%--Appendix*****--%></b></font>
	<%--Fixed for Bug# 7356:Akshi vinayak--%>
	<A HREF="javascript:void(0);" onClick="openAppendix(<%=appendxRight%>,'<%=includedIn%>','<%=src%>',<%=budgetId%>,
             '<%=budgetTemplate%>','<%=selectedTab%>','<%=mode%>','<%=studyId%>','combBgt')">
		<img src="./images/Appendix.gif" title="<%=LC.L_Appendix%>" alt="<%=MC.M_AddEdt_Appx%><%--Add and Edit Appendix*****--%>" border = "0" align=absbotton></A></td>
	<%} else { %>
	<td   align="center" width="4%">
	<font size = -2 color="#696969"><b><%//=LC.L_Appendix%><%--Appendix*****--%></b></font>
	<%--Fixed for Bug# 7356:Akshi vinayak--%>
		<A HREF="javascript:void(0);" onClick="openAppendix(<%=appendxRight%>,'<%=includedIn%>','<%=src%>',<%=budgetId%>,
             '<%=budgetTemplate%>','<%=selectedTab%>','<%=mode%>','<%=studyId%>','')">
		<img src="./images/Appendix.gif" title="<%=LC.L_Appendix%>" alt="<%=MC.M_AddEdt_Appx%><%--Add and Edit Appendix*****--%>" border = "0" align=absbotton></A></td>
	<%} %>
	<%if (!from.equals(("study"))){%>
	<td   align="center" width="4%">
	<font size = -2 color="#696969"><b><%//=LC.L_Access_Rights%><%--Access Rights*****--%></b></font>
	<%--Fixed for Bug# 7356:Akshi vinayak--%>
		<A HREF="javascript:void(0);" onClick="openAccessRights(<%=pageRight%>,'<%=includedIn%>','<%=src%>',<%=budgetId%>,
             '<%=budgetTemplate%>','<%=selectedTab%>','<%=mode%>','<%=studyId%>')">
		<img src="./images/AccessRights.gif" title="<%=LC.L_Access_Rights%>" alt="<%=MC.M_AddEdt_AccRgts%><%--Add and Edit Access Rights*****--%>" border = "0" align=absbotton></A></td>

	<td align="center" width="4%">
	<font size = -2 color="#696969"><b><%//=LC.L_Sections%><%--Sections*****--%></b></font>
	<%--Fixed for Bug# 7356:Akshi vinayak--%>
		<A HREF="javascript:void(0);" onClick="openSections(<%=pageRight%>,'<%=includedIn%>','<%=src%>',<%=budgetId%>,
             '<%=budgetTemplate%>','<%=selectedTab%>',<%=bgtcalId%>,'<%=mode%>','<%=bgtStat%>','')">
		<img src="./images/section.gif" title="<%=LC.L_Sections%>" alt="<%=MC.M_AddEdt_Sec%><%--Add and Edit Sections*****--%>" border = "0" align=absbotton></A></td>
<% 	}
   if (! includedIn.equals("P")) { %>
	<td   align="center" width="4%">
	<font size = -2 color="#696969"><b><%//=LC.L_Repeating%><%--Repeating*****--%></b></font>
	<%--Fixed for Bug# 7356:Akshi vinayak--%>
		<A href="javascript:void(0);" onClick="openRepeatLineItems(document.budget,'<%=mode%>',<%=bgtcalId%>,'<%=bgtStatCode%>','<%=budgetTemplate%>','')">
		<img src="./images/RepeatingLineItems.gif" title="<%=LC.L_Repeating%>" alt="<%=MC.M_AddEdt_RepLine%><%--Add and Edit Repeating Line Items--%>" border = "0" align=absbotton></A></td>

	<td   align="center" width="4%">
	<font size = -2 color="#696969"><b><%//=LC.L_Personnel_Costs%><%--Personnel Costs*****--%></b></font>
	<%--Fixed for Bug# 7356:Akshi vinayak--%>
		<A href="javascript:void(0);" onClick="openEditPersonnelCostWin(document.budget,'<%=mode%>',<%=bgtcalId%>,'<%=bgtStatCode%>','<%=budgetTemplate%>','')">
		<img src="./images/PersonnelCost.gif" title="<%=LC.L_Personnel_Costs%>" alt="<%=MC.M_AddEdt_Personnel%><%--Add and Edit Personnel Costs*****--%>" border = "0" align=absbotton></A></td>
<% } //for includeIN=P%>

	<%if (! from.equals("study")){  %>
		<!-- Added by gopu to fix the bugzilla Issue #2391 on 21-04-2006 -->
		<%if (!(bgtStat.equals("Freeze") || bgtStat.equals("Template"))) {%>
	<td   align="center" width="4%">
	<font size = -2 color="#696969"><b><%//=LC.L_Exclude_Multiple%><%--Exclude Multiple*****--%></b></font>
	<%--Fixed for Bug# 7356:Akshi vinayak--%>
			<A href="javascript:void(0);" onClick="openDelete(<%=bgtcalId%>,<%=pageRight%>,'')"><img src="./images/delete.gif" title="<%=LC.L_Exclude_Multiple%>" alt="<%=LC.L_Exclude_Multiple%><%--Exclude Multiple*****--%>" border = "0" align=absbotton></A>
		</td>
		<%} else{%>
		<td   align="center" width="4%">
			<font size = -2 color="#696969"><b><%//=LC.L_Exclude_Multiple%><%--Exclude Multiple*****--%></b></font>
			<!-- #5747 02/10/2011 @Ankit-->
			<A href="javascript:void(0);" onClick="return fnMessage('<%=budgetStatusDesc%>'); "><img src="./images/delete.gif" title="<%=LC.L_Exclude_Multiple%>" alt="<%=LC.L_Del_Multi%><%--Delete Multiple*****--%>" border = "0" align=absbotton></A> <%	}%>
		</td>
	<%} %>

		<td  align="center" width="4%">
	<font class="rhsFont" size = -2 color="#696969"><b><%//=LC.L_Milestones%><%--Milestones*****--%></b></font>
	<%--Fixed for Bug# 7356:Akshi vinayak--%>
			<A href="javascript:void(0);" onclick="openMile(document.budget, <%=pageRight%>)"><img src="./images/Milestone.gif" title="<%=LC.L_Milestones%>" alt="<%=LC.L_Create_Mstones%><%--Create Milestones*****--%>" border = "0" align=absbotton></A>
			</td>


		<%// sv, 08/10, fix for bug #1456, f_check_parm on pageRight %>
		<!--	<td class=tdDefault width="15%"> <A onClick="return f_check_perm(<%=pageRight%>,'N')" href="copybudget.jsp?from=initial&srcmenu=<%=src%>&budgetId=<%=budgetId%>">Copy this Budget</A></td>	-->
		<!--	 <A HREF="#" onclick="openPrint(document.bgtprot, 'P')">Print Budget</A> -->

		<%if(protDD.equals("No Calendar Selected")){%>
		<!-- <td align=left><B>
		<P class="defComments">Baseline Budget</P>
		</B></td> -->
		<%}%>
	</tr>
</table>
</Form>
</DIV>

<DIV class="<%=bottomdivClass%>" id="bottomDiv" <%=bottomDivStyle%> border="1">
<!--Form name="bgtprot" id="budgetForm" method=post action="savepatientbudget.jsp"-->
<!--Form name="bgtprot" id="bgtprot"-->
<!--Moved the hidden fields below-->
<!-- Modified By Parminder singh Bug #11366 -->

	<%


String ctgryPullDn = "";
SchCodeDao schDao = new SchCodeDao();
schDao.getCodeValues("category");
%>
<%if (from.equals("study")){%>
<div name="repDisplay" id="repDisplay" width="100%" height="100%" scrolling="yes" allowautotransparency="true"></div>
<%}else{ %>
	<%
	if (EJBUtil.isEmpty(selectedBgtcalId) || selectedBgtcalId.equals("0")){
		if(budTypeId==budCostType){
		if(includedIn.equals("P")) 
			repId  = 110;
		else repId  = 159;
		}
		else{
			if(includedIn.equals("P")) 
				repId  = 183;
			else repId  = 184;
		}
	}else{
		if(budTypeId==budCostType)
			repId  = 110;
		else
			repId  = 183;
		
	}
	%>
<!-- table width="100%">
	<TR><TD width="100%"-->
		<jsp:include page="budrepretrieve.jsp" flush="true">
			<jsp:param name="repId" value="<%=repId %>" />
			<jsp:param name="budgetPk" value="<%=budgetId%>" />
			<jsp:param name="repName" value="" />
			<jsp:param name="protId" value="<%=bgtcalId%>" />
			<jsp:param name="mode" value="M" />
			<jsp:param name="budgetTemplate" value="<%=budgetTemplate%>" />
			<jsp:param name="pageRight" value="<%=pageRight%>" />
			<jsp:param name="bgtStat" value="<%=bgtStat%>" />
			<jsp:param name="includedIn" value="<%=includedIn%>" />
		</jsp:include>
	<!--/TD></TR>
</table-->
<%} %>

<input type="hidden" id="mode" name="mode" value="<%=mode%>">
<input type="hidden" id="srcmenu" name="srcmenu" value="<%=src%>">
<input type="hidden" id="selectedTab" name="selectedTab" value="<%=selectedTab%>">
<input type="hidden" id="budgetId" name="budgetId" value="<%=budgetId%>">
<input type="hidden" id="bgtcalId" name="bgtcalId" value="<%=bgtcalId%>">
<input type="hidden" id="budgetTemplate" name="budgetTemplate" value="<%=budgetTemplate%>">
<input type="hidden" id="includedIn" name="includedIn" value="<%=includedIn%>">
<input type="hidden" name="perCodeId" value="<%=perCodeId%>">
<input type="hidden" id="repId" name="repId" value="<%=repId%>">
<!-- /Form-->
</DIV>
<%
 
 }//budgetexisits
 } else { //end of else body for page right
%> 
</DIV> 
<jsp:include page="accessdenied.jsp" flush="true" />

<%}}//end of if body for session
   	else
	{
	%> <jsp:include page="timeout.html" flush="true" /> <%
	}
	%>

<div><jsp:include page="bottompanel.jsp" flush="true" /></div>

<% if (! includedIn.equals("P")) { %>
<div class="mainMenu" id="emenu"><jsp:include page="getmenu.jsp" flush="true" /></div>
<%} %>

<% if ((from.equals("study"))&& (pageRight > 0 )) { %>
<script>
	/*if(document.getElementById('calDD').value != 'No Calendar Selected'){*****/
	if(document.getElementById('calDD').value != '<%=MC.M_NoCalSelected%>'){
		var dd = document.getElementById('bgtcal');
		var indx = dd.options.selectedIndex;
		document.getElementById('bgtcalId').value = dd.options[indx].value;
	}
	var budTypeId=0;
	if(document.getElementById('budgetType')){
	var budtypedd = document.getElementById('budgetType');
	var indx = budtypedd.options.selectedIndex;
	    budTypeId = budtypedd.options[indx].value;}
	else
	    budTypeId=<%=budTypeId%>;
	var repId=0;
	if (document.getElementById('bgtcalId').value =='0'){
		if(budTypeId==<%=budCostType%>)
		repId=159;
		else
		repId=184;
	}else{
		if(budTypeId==<%=budCostType%>)
		repId=110;
		else
		repId=183
	}

	var act='repId='+repId+'&repName=&budgetPk='+document.getElementsByName('budgetId')[0].value+'&protId='+document.getElementById('bgtcalId').value;
	act=act+'&mode=M&budgetTemplate='+document.getElementById('budgetTemplate').value;
	act=act+'&bgtStat='+document.getElementById('budgetStatus').value+'&includedIn=P&pageRight='+document.getElementById('pageRight').value;
	act=act+'&from=study';

	var ao=new VELOS.ajaxObject('budrepretrieve.jsp', {
		urlData:act  ,
		reqType:'POST',
		cache:false,   <%--Fixed for Bug# 7356:Akshi vinayak--%>
		outputElement: 'repDisplay',
		success: '$E.onContentReady(makeMeFixedHeader());'
		}
	);
	ao.startRequest();
</script>
<%} %>
<script>
document.ready=function(){
	var len = (document.getElementsByName("comment"))? 
			document.getElementsByName("comment").length:0;
	if (len >0){
		document.getElementById("bottomDiv").style.top = (parseInt(document.getElementById("bottomDiv").offsetTop,10)+16)+'px';
	}
	// fix for bug#:10436 
};
var len = 0;
len = (document.getElementsByName("comment"))? 
		document.getElementsByName("comment").length:0;

if (len == 0){
	if(document.getElementById("hideTable"))
	document.getElementById("hideTable").style.display="none";
}

if (document.getElementById("repDisplay")){
	//document.getElementById("repDisplay").style.top = document.getElementById("bottomDiv").style.top;
}
var budgtType
if(document.getElementById("budgetTemplate"))
budgtType=document.getElementById("budgetTemplate").value;//bug#:10436 
if (navigator.userAgent.indexOf("MSIE") == -1){ 
		$E.onContentReady(patientBudgetFixedHeader());
}else{
		$E.onContentReady(patientBudgetFixedHeader());	
}
//fix for bug#:10436 
function setComparativeBugetStyle(){
	if (navigator.userAgent.indexOf("MSIE") == -1){ 
		if (document.getElementById("budgetTemplate")){
			if(document.getElementById("budgetTemplate").value=='C' 
				|| document.getElementById("budgetTemplate").value=='P'){
				jQuery("#bottomDiv").css("border", "0px"); 
				//jQuery(".thickLine").hide();
				jQuery("#infoBudget").css("width", "98%"); 
				jQuery("#infoBudget1").css("width", "98%"); 
				jQuery("#infoCal").css("width", "98%");
				jQuery("#hideTable").css("width", "98%");
				jQuery(".fixedHeader").css("width", "100%"); 
				jQuery(".scrollContent").css("width", "100%"); 
				jQuery("#TableContainer").css("width", "100%");
				if(screen.width>1280 || screen.height>=1024){
					$j(".scrollContent").attr('style', "height:340px;");
					$j("#TableContainer").attr('style', "height:370;px; overflow-y:scroll;");
				}
				else{
					$j(".scrollContent").attr('style', "height:580px;");
					$j("#TableContainer").attr('style', "height:610;px");
				}
			}
		}
	}else{
		if (document.getElementById("budgetTemplate")){
			if(document.getElementById("budgetTemplate").value=='C' 
				|| document.getElementById("budgetTemplate").value=='P'){
				jQuery("#bottomDiv").css("border", "0px"); 
				//jQuery(".thickLine").hide();
				$j("#infoBudget").attr('style', "width:100%;");
				$j("#infoBudget1").attr('style', "width:100%;");
				$j("#infoCal").attr('style', "width:100%;");
				$j("#hideTable").attr('style', "width:100%;");
				$j(".scrollContent").attr('style', "width:99%;"); 
				$j("#TableContainer").attr('style', "width:99%;"); 
				if(screen.width>1280 || screen.height>=1024){
					$j(".scrollContent").attr('style', "height:330px;");
					$j("#TableContainer").attr('style', "height:360px; margin-top:0px; overflow-y:scroll;");
				}
				else{
					$j(".scrollContent").attr('style', "height:520px;");
					$j("#TableContainer").attr('style', "height:570px; margin-top:25px;");
				}
			}
		}
	} 
}


function setPatientBugetStyle(){
	if (navigator.userAgent.indexOf("MSIE") == -1){
		if(document.getElementById("budgetTemplate").value=='C' 
			|| document.getElementById("budgetTemplate").value=='P'){
		jQuery("#bottomDiv").css("border", "0px"); 
		//jQuery(".thickLine").hide();
		jQuery("#hideTable").css("width", "98%");
		jQuery(".scrollContent").css("width", "100%"); 
		if(screen.width>1280 || screen.height>=1024){
			$j(".scrollContent").attr('style', "height:370px;");
			$j("#infoBudget").attr('style',"width:99%;"); 
			$j("#infoBudget1").attr('style',"width:99%;");
			$j("#infoCal").attr('style',"width:99%;");

			$j("#TableContainer").attr('style', "height:400px; width:99.9%; overflow-y:scroll;");
			$j("#bottomDiv").attr('style', "height:68%; width:98%;");
			}
		else{
			$j(".scrollContent").attr('style', "height:600px;");
			$j("#TableContainer").attr('style', "height:640px; width:99.9%;");
			$j("#infoBudget").attr('style',"width:99%;"); 
			$j("#infoBudget1").attr('style',"width:99%;");
			$j("#infoCal").attr('style',"width:99%;");
			$j("#bottomDiv").attr('style', "height:90%; width:98%;");
			}
		}
	}else{
		if(document.getElementById("budgetTemplate").value=='C' 
			|| document.getElementById("budgetTemplate").value=='P'){
		jQuery("#bottomDiv").css("border", "0px"); 
		//jQuery(".thickLine").hide();
		$j("#infoBudget").attr('style', "width:100%;");
		$j("#infoBudget1").attr('style', "width:100%;");
		$j("#infoCal").attr('style', "width:100%;");
		$j("#hideTable").attr('style', "width:100%;");
		jQuery(".scrollContent").attr("width", "99%"); 
		jQuery("#TableContainer").attr("width", "99%");
		if(screen.width>1280 || screen.height>=1024){
			$j(".scrollContent").attr('style', "height:320px;");
			$j("#TableContainer").attr('style', "height:370px; margin-top:25px;overflow-y:scroll;");
			
			}
		else{
			$j(".scrollContent").attr('style', "height:550px;");
			$j("#TableContainer").attr('style', "height:600px; margin-top:25px;");
			}
		}
	}
}
</script>
</body>
</html>