<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*,com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.web.grpRights.GrpRightsJB" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>
<%
boolean isIrb = "irb_personnel_tab".equals(request.getParameter("selectedTab")) ? true : false;
if (isIrb) {
%>
<title><%=MC.M_ResComp_NewStdPers%><%--Research Compliance >> New Application >> <%=LC.Std_Study%> Personnel*****--%></title>
<% } else {  %>
<TITLE><%=LC.L_Std_AccessRights%><%--<%=LC.Std_Study%> Access Rights*****--%></TITLE>
<% }  %>



<SCRIPT Language="javascript">
var screenWidth = screen.width;
var screenHeight = screen.height;

function openwin1() {
	windowName=window.open("completepatientinfo.htm","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400");
}

function openSitesWin(userId, studyId,teamId) {

  param1="studySiteRights.jsp?userId="+userId+"&studyId="+studyId+"&teamId="+teamId;
	windowName = window.open(param1,'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=400')
	windowName.focus();
}

 function  validate(formobj){

//     formobj=document.stdRights;


	if (!(validate_col('e-Signature',formobj.eSign))) return false

     if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }

}

</SCRIPT>

<!--Added by Manimaran for June Enhancement #GL2-->
<SCRIPT Language="javascript">
    var arr = new Array()
	function checkAll(formobj) {
		totcount = formobj.totalrows.value;
		if(formobj.All.checked) {
		   for (i=0;i<totcount;i++) {
                if(arr[i].substring(0,2) == "H_"){
				formobj.newr[i].checked = false;
				formobj.edit[i].checked = false;
				formobj.view[i].checked = false;
				formobj.rights[i].value =0;
			   }
			   else
    			if(arr[i] == 'STUDYREP')
			   {
				 formobj.newr[i].checked = false;
				 formobj.edit[i].checked = false;
				 formobj.view[i].checked = true;
				 formobj.rights[i].value =4;

			   }
   		      else
    			if(arr[i] == 'STUDYVPDET')
			   {
				 formobj.newr[i].checked = false;
				 formobj.edit[i].checked = false;
				 formobj.view[i].checked = true;
				 formobj.rights[i].value =4;

			   }
				else
    			if(arr[i] == 'STUDYSUM')
			   {
				 formobj.newr[i].checked = false;
				 formobj.edit[i].checked = true;
				 formobj.view[i].checked = true;
				 formobj.rights[i].value =6;

			   }

			   else
    			if(arr[i] == 'STUDYNOTIFY')
			   {
				 formobj.newr[i].checked = true;
				 formobj.edit[i].checked = false;
				 formobj.view[i].checked = true;
				 formobj.rights[i].value =5;

			   }

               else
			   {
				formobj.newr[i].checked = true;
				formobj.edit[i].checked = true;
				formobj.view[i].checked = true;
				formobj.rights[i].value =7;

			   }

           }
        }

		else
		{
			for (i=0;i<totcount;i++) {
				formobj.newr[i].checked = false;
				formobj.edit[i].checked = false;
				formobj.view[i].checked = false;
				formobj.rights[i].value =0;
            }
		}
	}


	function changeRights(obj,row,formobj)

	{

	  selrow = row ;

	  totrows = formobj.totalrows.value;

	  if (totrows > 1)
		  rights =formobj.rights[selrow].value;

	  else 		rights =formobj.rights.value;

	  objName = obj.name;

	  if (obj.checked)
   	  {

       	if (objName == "newr")

	{

		rights = parseInt(rights) + 1;

		if (!formobj.view[selrow].checked)

		{

			formobj.view[selrow].checked = true;

			rights = parseInt(rights) + 4;

		}



	}


     	if (objName == "edit")
	{

		rights = parseInt(rights) + 2;

		if (!formobj.view[selrow].checked)

		{

			formobj.view[selrow].checked = true;

			rights = parseInt(rights) + 4;

		}

	}



     	if (objName == "view") rights = parseInt(rights) + 4;

	if (totrows > 1 )

		formobj.rights[selrow].value = rights;

	else

		formobj.rights.value = rights;

  }else

	{

       	if (objName == "newr") rights = parseInt(rights) - 1;

       	if (objName == "edit") rights = parseInt(rights) - 2;

       	if (objName == "view")
		{
			if (formobj.newr[selrow].checked)
			{
				alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
				formobj.view[selrow].checked = true;
			}

		    else if (formobj.edit[selrow].checked)
			{
		    	alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
				formobj.view[selrow].checked = true;
			}
			else
			{
			  rights = parseInt(rights) - 4;
			}
		}


	if (totrows > 1 ) {

		formobj.rights[selrow].value = rights;

    }
	else {

		formobj.rights.value = rights;

  	 }

  }
}

	function disableNewRight(row,formobj) {
		totrows = formobj.totalrows.value;
		if (totrows > 1 && totrows > row) {
			switch(parseInt(formobj.rights[row].value,10)) {
			case 7: formobj.rights[row].value = 6; break;
			case 5: formobj.rights[row].value = 4; break;
			case 3: formobj.rights[row].value = 2; break;
			case 1: formobj.rights[row].value = 0; break;
			}
		}
	}

</SCRIPT>



<%
String src;

String teamId;
String from = "team";
src= request.getParameter("srcmenu");

teamId= request.getParameter("teamId");

%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>



<BODY>
<br>
<jsp:useBean id="stdRights" scope="request" class="com.velos.eres.web.studyRights.StudyRightsJB"/>
<jsp:useBean id="modStdRights" scope="request" class="com.velos.eres.web.studyRights.StudyRightsJB"/>

<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="xtra" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>

  <DIV class="BrowserTopn"  id="div1"> <!--Ashu modified for BUG#5779 (9Feb11).-->
  <% String includeTabsJsp = isIrb ? "irbnewtabs.jsp" : "studytabs.jsp"; %>
  <jsp:include page="<%=includeTabsJsp%>" flush="true">
  <jsp:param name="from" value="<%=from%>"/>
  </jsp:include>
  </DIV>
  
<%
String stid= request.getParameter("studyId");
if(stid==null || stid==""){%>
<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024)
		document.write('<DIV class="BrowserBotN BrowserBotN_S_1" id="div1"  style="height:75%;">')
	else
		document.write('<DIV class="BrowserBotN BrowserBotN_S_1" id="div1"')
</SCRIPT>
	<%} else {%>
<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024)
		document.write('<DIV class="BrowserBotN BrowserBotN_S_3" id="div1" style="height:75%;" >')
	else
		document.write('<DIV class="BrowserBotN BrowserBotN_S_3" id="div1">')
</SCRIPT>
<%}

	HttpSession tSession = request.getSession(true);

	int pageRight = 0;



	if (sessionmaint.isValidSession(tSession))

	{
	String uName = (String) tSession.getValue("userName");

	String tab = request.getParameter("selectedTab");
	String userName = null;

	pageRight = EJBUtil.stringToNum(request.getParameter("right"));
	userName = request.getParameter("userName");


	//StudyRightsJB gRights = (GrpRightsJB) tSession.getValue("GRights");

     	//pageRight = Integer.parseInt(gRights.getFtrRightsByValue("STUDYTEAM"));

	//pageRight =7;



	if (pageRight > 0)

	{



%>

  <Form Name="stdRights" id="stdRightsFrmid" method="post" action="updatestudyrights.jsp" onSubmit=" if (validate(document.stdRights)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
    <%int stdId = EJBUtil.stringToNum(teamId) ;

	String studyId = request.getParameter("studyId");

	String userId = request.getParameter("userId");




	stdRights.setId(stdId);%>
    <%

 	out.print("<Input type=hidden name=\"fk_study\" value="+stdId +">");

	stdRights.getStudyRightsDetails();

	ctrl.getControlValues("study_rights");

	int rows = ctrl.getCRows();

//	out.print("Rows" +rows);

	ArrayList feature =  ctrl.getCValue();

	ArrayList ftrDesc = ctrl.getCDesc();
	ArrayList ftrSeq = ctrl.getCSeq();
	int prevSeq =0;
	String str="";


//get those rights which will be hidden according to the milestone module
	xtra.getControlValues("hid_rights");

	int xrows = xtra.getCRows();

	ArrayList xfeature =  xtra.getCValue();
	ArrayList xftrDesc = xtra.getCDesc();
	ArrayList xftrSeq = xtra.getCSeq();

	//get account module

	String modRight = (String) tSession.getValue("modRight");

	int modlen = modRight.length();

	acmod.getControlValues("module");
	int acmodrows = acmod.getCRows();

	ArrayList acmodfeature =  acmod.getCValue();
	ArrayList acmodftrDesc = acmod.getCDesc();
	ArrayList acmodftrSeq = acmod.getCSeq();
	ArrayList acmodftrRight = new ArrayList();
	String strR;

	ArrayList acmodValue = new ArrayList();


	for (int counter = 0; counter <= (modlen - 1);counter ++)
	{
		strR = String.valueOf(modRight.charAt(counter));
		acmodftrRight.add(strR);
	}

	modStdRights.setGrSeq(acmodftrSeq);
    modStdRights.setFtrRights(acmodftrRight);
	modStdRights.setGrValue(acmodfeature);
	modStdRights.setGrDesc(acmodftrDesc);

	// end of account module
   //Added by Manimaran for June Enhancement #GL2.
   for(int i = 0; i<feature.size();i++){
  %>
   <SCRIPT Language="javascript">
      arr[<%=i%>]='<%=feature.get(i).toString()%>'
   </SCRIPT>
  <%
  } Object[] arguments = {userName};
  %>

    <Input type="hidden" name="totalrows" value= <%=rows%> >
    <Input type="hidden" name="src" value= <%=src%> >
    <Input type="hidden" name="teamId" value= <%=teamId%> >
    <Input type="hidden" name="selectedTab" value= <%=tab%> >
    <P class="sectionHeadings"><%=VelosResourceBundle.getMessageString("M_Assign_AcesRightsTo",arguments)%><%-- Assign Access Rights to <%=userName%>*****--%>
	</P>
	<TABLE width="500" cellspacing="0" cellpadding="0">
	  <tr><td colspan=4 align=right><A href=# onClick=openwin1()><%=MC.M_What_CompletePatData%><%--What is Complete <%=LC.Pat_Patient%> Data*****--%></A></td></tr>
	  <tr><td height=10></td></tr>
	  <TH></TH>
      <TH><%=LC.L_New %><%-- New*****--%></TH>
      <TH><%=LC.L_Edit %><%-- Edit*****--%></TH>
      <TH><%=LC.L_View %><%-- View*****--%></TH>
    <tr><td>&nbsp</td></tr>
    <tr><td colspan=4 align=right><A href=# onClick="openSitesWin('<%=userId%>','<%=studyId%>','<%=teamId%>')"> <%=LC.L_Multi_OrgAccess%><%--Multiple Organization Access*****--%></A></td></tr>
	<!-- Modified by Amar for Bugzilla issue #3023 -->
	<tr><td align="left"><input type="checkbox" name="All"  onClick="checkAll(document.stdRights)">  <%=LC.L_SelOrDeSel_All%><%--Select / Deselect All*****--%></td> </tr>

 <%

	boolean showRight = false;

	 boolean showNew = true;
	 boolean showEdit = true;
	 boolean showView = true;

     int hidIndex = -1;
	 String xSeq = "-1";
	 String XRight = "-1";
	 String rights;

     for(int count=0;count<rows;count++){
     		String ftr = (String) feature.get(count);
			String desc = (String) ftrDesc.get(count);
			Integer seq = (Integer) ftrSeq.get(count);

			showNew = true;
			showEdit = true;
			showView = true;

			if ( (ftr.substring(0,2)).equals("H_") )
    		{
    			desc = "<br><b>" + desc + "</b>";
	    		showNew = false;
    			showEdit = false;
    			showView = false;
	    		rights = "0";
    	   	} else if ( (ftr.compareTo("STUDYSEC") == 0) || (ftr.compareTo("STUDYAPNDX") == 0)  || (ftr.compareTo("STUDYEUSR") == 0))
	    	{
    			desc = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + desc ;
    		}
		if (ftr.compareTo("STUDYEUSR") == 0)
		{
			desc = "";
		} 



			if ((stdRights.getFtrRights().size()) == 0)
			 	rights= "0";
			else rights = stdRights.getFtrRightsByValue(ftr);
				hidIndex = xfeature.indexOf(ftr); //

	if (hidIndex >= 0)
	{
		xSeq = ((Integer) xftrSeq.get(hidIndex)).toString(); //the sequence of the xtra module
  		XRight =  (String) modStdRights.getFtrRightsBySeq(xSeq);
		//look for this right in acMod String
		if (XRight.compareTo("1") == 0)
		{
			showRight = true;
		}
		else
		{
			showRight = false;
		}
	}
	else
	{
		showRight = true;
	}
	%>
         <Input type="hidden" name=rights value= <%=rights%> >

    <%

		if (!showRight)
		{
		%>
		<tr>
		<td>
		<Input type="hidden" name="newr">
		<Input type="hidden" name="edit">
		<Input type="hidden" name="view">
		</td>
		</tr>
		<%
		}
		else
		{
		if(desc.equalsIgnoreCase("")){
		%>
		<TR style="display:none;">
		<%}else{ %>
		<TR>
		<%} %>

        <TD>
		<%= desc%>
		</TD>
          <%
     		//check for special rights

	       	if ((ftr.compareTo("STUDYSUM") == 0) || (ftr.compareTo("STUDYVPDET") == 0)  || (ftr.compareTo("STUDYREP") == 0))
			{
			  showNew = false;
			}

			if ( (ftr.compareTo("STUDYNOTIFY") == 0)  || (ftr.compareTo("STUDYREP") == 0) || (ftr.compareTo("STUDYVPDET") == 0)  )
			{
			  showEdit = false;
			}

			if (ftr.compareTo("STUDYEUSR") == 0)
			{
			  showView = false;
			  showNew = false;
			  showEdit = false;
			} 


			//end of check


		  if (rights.compareTo("7") == 0){%>
           <TD align=center>
	  	    <%
			if (showNew)
			{
	  		%>
			   <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
			<%
		 	}
			else
			 {
			%>
           		<Input type="hidden" name="newr" value="0">
                <% if (ftr.compareTo("STUDYSUM") == 0) { %>
                <script>disableNewRight(<%=count%>,document.stdRights);</script>
                <% }  %>
			<%
			 }
			%>
		  </TD>
          <TD align=center>
		  <%
			if (showEdit)
			{
    		%>
		       <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
			<%
			}
			else
			{
			%>
        		<Input type="hidden" name="edit" value="0">
			<%
			}
			%>
         </TD>
		 <TD align=center>
		   <%
		if (showView)
			{
    		%>
         	  <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
		  	<%
			}
			else
			{
			%>
         		<Input type="hidden" name="view" value="0">
			<%
			}
			%>
        </TD>
        <%}else if (rights.compareTo("1") == 0){ %>
        <TD align=center>
		 <%
			if (showNew)
			{
	  		%>
	          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
			<%
		 	}
			else
			 {
			%>
           		<Input type="hidden" name="newr" value="0">
                <% if (ftr.compareTo("STUDYSUM") == 0) { %>
                <script>disableNewRight(<%=count%>,document.stdRights);</script>
                <% }  %>
			<%
			 }
			%>
		  </TD>
          <TD align=center>
		  <%
			if (showEdit)
			{
    		%>
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>,document.stdRights)" >
			<%
			}
			else
			{
			%>
        		<Input type="hidden" name="edit" value="0">
			<%
			}
			%>
         </TD>
		 <TD align=center>
		   <%
		if (showView)
			{
    		%>
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>,document.stdRights)">
		  <%
			}
			else
			{
			%>
         		<Input type="hidden" name="view" value="0">
			<%
			}
			%>
        </TD>
        <%}else if(rights.compareTo("3") == 0){%>
        <TD align=center>
		 <%
			if (showNew)
			{
	  		%>
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
   			<%
		 	}
			else
			 {
			%>
           		<Input type="hidden" name="newr" value="0">
                <% if (ftr.compareTo("STUDYSUM") == 0) { %>
                <script>disableNewRight(<%=count%>,document.stdRights);</script>
                <% }  %>
			<%
			 }
			%>
		  </TD>
          <TD align=center>
		  <%
			if (showEdit)
			{
    		%>

	      <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
			<%
			}
			else
			{
			%>
        		<Input type="hidden" name="edit" value="0">
			<%
			}
			%>
         </TD>
		 <TD align=center>
		   <%
		if (showView)
			{
    		%>
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>,document.stdRights)">
		  <%
			}
			else
			{
			%>
         		<Input type="hidden" name="view" value="0">
			<%
			}
			%>
        </TD>
        <%}else if (rights.compareTo("2") == 0){%>
	   <TD align=center>
	    <%
			if (showNew)
			{
	  		%>
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>,document.stdRights)">
			<%
		 	}
			else
			 {
			%>
           		<Input type="hidden" name="newr" value="0">
			<%
			 }
			%>
		  </TD>
          <TD align=center>
		  <%
			if (showEdit)
			{
    		%>

          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
			<%
			}
			else
			{
			%>
        		<Input type="hidden" name="edit" value="0">
			<%
			}
			%>
         </TD>
		 <TD align=center>
		   <%
		if (showView)
			{
    		%>
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>,document.stdRights)">
		  <%
			}
			else
			{
			%>
         		<Input type="hidden" name="view" value="0">
			<%
			}
			%>
        </TD>
        <%}else if (rights.compareTo("4") == 0){%>
        <TD align=center>
		 <%
			if (showNew)
			{
	  		%>
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>,document.stdRights)">
			<%
		 	}
			else
			 {
			%>
           		<Input type="hidden" name="newr" value="0">
			<%
			 }
			%>
		  </TD>
          <TD align=center>
		  <%
			if (showEdit)
			{
    		%>
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>,document.stdRights)">
			<%
			}
			else
			{
			%>
        		<Input type="hidden" name="edit" value="0">
			<%
			}
			%>
         </TD>
		 <TD align=center>
		   <%
		if (showView)
			{
    		%>
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
		  <%
			}
			else
			{
			%>
         		<Input type="hidden" name="view" value="0">
			<%
			}
			%>
        </TD>
        <%}else if (rights.compareTo("5") == 0){%>
        <TD align=center>
		 <%
			if (showNew)
			{
	  		%>
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
			<%
		 	}
			else
			 {
			%>
           		<Input type="hidden" name="newr" value="0">
                <% if (ftr.compareTo("STUDYSUM") == 0) { %>
                <script>disableNewRight(<%=count%>,document.stdRights);</script>
                <% }  %>
			<%
			 }
			%>
		  </TD>
          <TD align=center>
		  <%
			if (showEdit)
			{
    		%>
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>,document.stdRights)">
			<%
			}
			else
			{
			%>
        		<Input type="hidden" name="edit" value="0">
			<%
			}
			%>
         </TD>
		 <TD align=center>
		   <%
		if (showView)
			{
    		%>
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
		  <%
			}
			else
			{
			%>
         		<Input type="hidden" name="view" value="0">
			<%
			}
			%>
        </TD>
        <%}else if (rights.compareTo("6") == 0){%>
        <TD align=center>
		 <%
			if (showNew)
			{
	  		%>
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>,document.stdRights)" >
			<%
		 	}
			else
			 {
			%>
           		<Input type="hidden" name="newr" value="0">
			<%
			 }
			%>
		  </TD>
          <TD align=center>
		  <%
			if (showEdit)
			{
    		%>
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
			<%
			}
			else
			{
			%>
        		<Input type="hidden" name="edit" value="0">
			<%
			}
			%>
         </TD>
		 <TD align=center>
		   <%
		if (showView)
			{
    		%>
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>,document.stdRights)" CHECKED>
		  <%
			}
			else
			{
			%>
         		<Input type="hidden" name="view" value="0">
			<%
			}
			%>
        </TD>
        <%}else{ %>
        <TD align=center>
		 <%
			if (showNew)
			{
	  		%>
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>,document.stdRights)">
        			<%
		 	}
			else
			 {
			%>
           		<Input type="hidden" name="newr" value="0">
			<%
			 }
			%>
		  </TD>
          <TD align=center>
		  <%
			if (showEdit)
			{
    		%>

          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>,document.stdRights)">
			<%
			}
			else
			{
			%>
        		<Input type="hidden" name="edit" value="0">
			<%
			}
			%>
         </TD>
		 <TD align=center>
		   <%
		if (showView)
			{
    		%>
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>,document.stdRights)">
		  <%
			}
			else
			{
			%>
         		<Input type="hidden" name="view" value="0">
			<%
			}
			%>
        </TD>
        <%}%>
      </TR>
      <%}
	  	 }//end of show
	  %>
      <TR></tr>
	</table>

        <% if (pageRight > 4) {%>
<br /><br /><br /><br /><br /><br />
		<jsp:include page="submitBar.jsp" flush="true">
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="stdRightsFrmid"/>
				<jsp:param name="showDiscard" value="N"/>
		</jsp:include>


	    <%}%>


	<table>
      <TR>

        <TD COLSPAN =4 align="right">
          <% if (pageRight > 4)

		  { %>
         <!-- <Input Type=Submit Value=Submit name=Submit>-->
		 <br>
		 <!--<input type="image" src="../images/jpg/Submit.gif" align="absmiddle" onClick = "return validate()" border="0">-->
          <% }

		else { %>
          <!--<Input Type=Submit Value=Submit name=Submit DISABLED>-->
          <% } %>
        </TD>
      </TR>
    </TABLE>
  </Form>

  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right
}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu">
  <jsp:include page="getmenu.jsp" flush="true"/>
</div>
</BODY>

</HTML>







