<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Mstone_Browser%><%--Milestone Browser*****--%></title>
 
<script>
var screenWidth = screen.width;
var screenHeight = screen.height;

function setOrder(formObj,orderBy,pgRight) //orderBy column number
{
	var lorderType;
	 if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";		
		lorderType="desc";
	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
		lorderType="asc";	
	}  
	
if (orderBy==2) orderBy="lower(study_number)";
	lsrc = formObj.srcmenu.value;
	lcurPage = formObj.page.value;		
	searchCriteria=formObj.searchCriteria1.value;
	formObj.action="milestonehome.jsp?srcmenu="+lsrc+"&page="+lcurPage+"&orderBy="+orderBy+"&orderType="+lorderType+"&searchCriteria="+searchCriteria;
	formObj.submit(); 
} 

function openView(studyId,pgRight) 
{
	if (f_check_perm(pgRight,'V') == true) {  
		windowName= window.open("mileview.jsp?study="+studyId,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=600,height=500,top=100,left=200");
		windowName.focus();
	}
}
	
</script>




<%
String src;
src= request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div> 
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<body>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,java.sql.*,com.velos.eres.business.common.CtrlDao,java.util.*,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.*"%>
<br>

<div class="BrowserTopN" id="div1">




<%
String pagenum = "";
int curPage = 0;
long startPage = 1;
String stPage;
long cntr = 0;

pagenum = request.getParameter("page");

if (pagenum == null)
{
	pagenum = "1";
}
curPage = EJBUtil.stringToNum(pagenum);

String orderBy = "";
orderBy = request.getParameter("orderBy");


String orderType = "";
orderType = request.getParameter("orderType");


if (orderType == null)
{
	orderType = "asc";
}

String searchCriteria = request.getParameter("searchCriteria");
if (searchCriteria==null) {searchCriteria="";}

String searchCriteriaForSql = StringUtil.escapeSpecialCharSQL(searchCriteria);

searchCriteria = StringUtil.htmlEncodeXss(searchCriteria);

HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession)) {
	//get milestone module account right 
	String modRight = (String) tSession.getValue("modRight");
	int modlen = modRight.length();
	CtrlDao acmod = new CtrlDao();
	acmod.getControlValues("module");
	ArrayList acmodfeature =  acmod.getCValue();
	ArrayList acmodftrSeq = acmod.getCSeq();
	int mileSeq = acmodfeature.indexOf("MODMIL");
	mileSeq	= ((Integer) acmodftrSeq.get(mileSeq)).intValue();
	char mileAppRight = modRight.charAt(mileSeq - 1);

	String userId = (String) tSession.getValue("userId");
	String accId = (String) tSession.getValue("accountId");
	int pageRight = 0;
	//get milestone group right 
	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");		

//	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MILEST"));

	// pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));
	
		int viewPageRight = 0;
  		 grpRights = (GrpRightsJB) tSession.getValue("GRights");		
   		viewPageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MILEST"));


    	if (String.valueOf(mileAppRight).compareTo("1") == 0 && viewPageRight >= 4 )	{

		//////////////////end by salil


	
	   String mileSql = "select distinct PK_STUDY, STUDY_NUMBER, "
	   		//+" case when length(study_title) > 80 then substr(study_title,1,80) || '...'  else study_title  end as study_title , " --KM
			+ " study_title ,"
			+" case when ( (select count(*) from er_milestone where fk_study = pk_study and milestone_delflag = 'N') > 0) then 'Edit Details' else 'Create New' end as milestone_exists "
			+" from er_study "
			+" where er_study.fk_account= "+ accId
			+" and ( exists (select * from er_studyteam where fk_user = "+ userId + " and study_team_usr_type <> 'X' and fk_study = pk_study  ) "
			+" or  pkg_superuser.F_Is_Superuser("+ userId +", pk_study) = 1 ) and er_study.STUDY_VERPARENT is null "			 
			+" and (lower(study_number) like lower('%"+searchCriteriaForSql.trim()+"%') or lower(study_title) like lower('%"+searchCriteriaForSql.trim()+"%') or lower(study_keywrds) like lower('%"+searchCriteriaForSql.trim()+"%')) ";
//			+ " order by lower(study_number)" ;
		
	   String countSql = "select count(distinct PK_STUDY) "  
			+" from er_study "
			+" where er_study.fk_account= "+ accId
			+" and ( exists (select * from er_studyteam where fk_user = "+ userId + " and study_team_usr_type <> 'X' and fk_study = pk_study  ) "
			+ " or  pkg_superuser.F_Is_Superuser("+ userId +", pk_study) = 1 ) and er_study.STUDY_VERPARENT is null " 
			+" and (lower(study_number) like lower('%"+searchCriteriaForSql.trim()+"%') or lower(study_title) like lower('%"+searchCriteriaForSql.trim()+"%') or lower(study_keywrds) like lower('%"+searchCriteriaForSql.trim()+"%'))" ;		
			

	   long rowsPerPage=0;
   	   long totalPages=0;	
	   long rowsReturned = 0;
	   long showPages = 0;
	   
	   String studyNumber = null;
	   String studyTitle = null;
   	   String milestoneExists = null;
	   String milestoneSummary = null;	   
	   int studyId=0;
	   boolean hasMore = false;
	   boolean hasPrevious = false;
	   long firstRec = 0;
	   long lastRec = 0;	   
	   long totalRows = 0;	   	   
	   
	   rowsPerPage =  Configuration.ROWSPERBROWSERPAGE ;
	   totalPages =Configuration.PAGEPERBROWSER ;
  
       BrowserRows br = new BrowserRows();
	   if ((orderBy==null ) ||orderBy.equals("null") ) orderBy = "lower(study_number)";
	     
	   br.getPageRows(curPage,rowsPerPage,mileSql,totalPages,countSql,orderBy,orderType);
   	   rowsReturned = br.getRowReturned();
	   showPages = br.getShowPages();
	   startPage = br.getStartPage();
	   hasMore = br.getHasMore();
	   hasPrevious = br.getHasPrevious();	 
	   totalRows = br.getTotalRows();	   
	   firstRec = br.getFirstRec();
	   lastRec = br.getLastRec();	   	 	     
	   
%>
  
<Form name="milestonehome" method=post onsubmit="" action="milestonehome.jsp?srcmenu=<%=src%>&page=1">
	<table width="99%" class="outline midalign" border="0" cellspacing="0" cellpadding="0">
	<tr height="20">
	<td colspan="4" class="lhsFont" > <b> <%=LC.L_Search_By%><%--Search By*****--%></b> 
	</td>
	</tr>
<tr class="searchBg">
	<td width="25%"  class="lhsFont" > <%=MC.M_Num_OrStdTitle%><%--<%=LC.Std_Study%> Number or <%=LC.Std_Study%> Title*****--%>: </td>
	<td width="25%"  class="lhsFont" > <Input type=text name="searchCriteria" size="20"> </td>
    <td width="20%"  class="lhsFont" ><button type="submit"><%=LC.L_Search%></button></td>
	<td width="30%" align="right"  class="rhsFont" ><b><font size="1">
	<A href="milestonehome.jsp?srcmenu=<%=src%>"> <%=LC.L_Refresh_List_Upper%><%--REFRESH LIST*****--%></A></font></b> &nbsp;&nbsp;
	</td>
	</tr>
		<tr height="10">
	<td colspan="4" > 	</td>
	</tr>
	</table>
</div>	   	
<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024)
		document.write('<div class="BrowserBotN BrowserBotN_M_1" id="div1" style="height:76%;">')
	else
		document.write('<div class="BrowserBotN BrowserBotN_M_1" id="div1">')
</SCRIPT>	
    	
	<Input type="hidden" name="srcmenu" value="<%=src%>">
	<Input type="hidden" name="page" value="<%=curPage%>">
	<Input type="hidden" name="orderBy" value="<%=orderBy%>">
	<Input type="hidden" name="orderType" value="<%=orderType%>">
	<Input type="hidden" name="searchCriteria1" value="<%=searchCriteria%>">

	<table width=100% border="0">
	<tr>
		<td colspan="3">
		 <% if (!(searchCriteria.equals(""))) {%> 
          <P class = "defComments"><%=MC.M_StdsMatch_SrchCriteria%><%--The following <%=LC.Std_Studies%> match your search criteria*****--%>: "<%=searchCriteria%>"</P>
		 <%}%>
		</td>

	</tr>		
	</table>
	<div class="tmpHeight"></div>
    <table width="99%" class="outline midAlign lhsFont" border="0" cellspacing="0" cellpadding="0" >
      <tr> 
        <th width="30%" onClick="setOrder(document.milestonehome,2)"><%=LC.L_Mstones_ForStd%><%--Milestones For <%=LC.Std_Study%>*****--%> &loz;</th>
        <th width="50%" onClick="setOrder(document.milestonehome,3)"> <%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%> &loz;</th>
        <th width="18%" > <%=LC.L_Std_Info%><%--<%=LC.Std_Study%> Information*****--%> </th>
      </tr>
	  

	<!--KM-->
	<script language=javascript>
	   var varViewTitle =  new Array (<%=rowsReturned%>);
	</script>


      <%
    for(int counter = 1;counter<=rowsReturned;counter++)
	{	
		studyId=EJBUtil.stringToNum(br.getBValues(counter,"pk_study")) ;
		studyNumber=br.getBValues(counter,"study_number");
		studyTitle=br.getBValues(counter,"study_title");
		if (studyTitle == null)
			studyTitle ="-";
		
		//GM 03/14/08 to fix CL7364 . This is to render studytitle which contains special characters like ' < ' and ' > '.
	    //NOTE : we are also aware that this would create problems to render korean characters. We need to find a resolution which fixes BOTH the problems
	    
	    //Modified by Manimaran to display full title in mouseover.
		if(studyTitle!=null){
	    	//studyTitle = StringUtil.htmlEncode(studyTitle);
	    	studyTitle = StringUtil.escapeSpecialCharJS(studyTitle);

		}
		
		milestoneExists=br.getBValues(counter,"milestone_exists");
		//milestoneSummary=br.getBValues(counter,"milestone_summary");
	
		if ((counter%2)==0) {
  %>
      <tr class="browserEvenRow"> 
        <%
		}
		else{
  %>
      <tr class="browserOddRow"> 
        <%
		}
  %>
        <td> <A href="milestone.jsp?srcmenu=<%=src%>&selectedTab=1&studyId=<%=studyId%>"> <%=studyNumber%> </A> </td>
    	
		
		<script language=javascript>
			 varViewTitle[<%=counter%>] = htmlEncode('<%=studyTitle%>');
		</script>
  
		<td align="center"> <a href="#" onmouseover="return overlib(varViewTitle[<%=counter%>],CAPTION,'<%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>');" onmouseout="return nd();"><img border="0" src="./images/View.gif" title="<%=LC.L_View%><%--View*****--%>"/></a>   </td>
        <td align="center"> <A href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=studyId%>"> <img src="../images/jpg/study.gif" title="<%=LC.L_Std_Admin%><%--<%=LC.Std_Study%> Administration*****--%>" border=0 > </A></td>
				
      </tr>
      <%

		}
%>
    </table>
    
	<!-- Bug#9751 16-May-2012 Ankit -->
	<table width="98%" cellspacing="0" cellpadding="0" class="midalign">
	<tr>
	<td>
	<% if (totalRows > 0) { Object[] arguments = {firstRec,lastRec,totalRows}; %>
		<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%--<font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
	<%} else {%>
		<font class="recNumber"><%=MC.M_NoRecordsFound%><%--No Records Found*****--%></font>	
	<%}%>	
	</td>
	</tr>
	</table>
	
	
	<table align=center>
	<tr>
<%
	if (curPage==1) startPage=1;

    for (int count = 1; count <= showPages;count++)
	{
   cntr = (startPage - 1) + count;
	 
	if ((count == 1) && (hasPrevious))
	{   
    %>
	<td colspan = 2>
  	<A href="milestonehome.jsp?searchCriteria=<%=searchCriteria%>&srcmenu=<%=src%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;
	</td>	
	<%
  	}	
	%>
	<td>
	<%
 
	 if (curPage  == cntr)
	 {
     %>	   
		<FONT class = "pageNumber"><%= cntr %></Font>
       <%
       }
      else
        {
       %>		
		
	   <A href="milestonehome.jsp?searchCriteria=<%=searchCriteria%>&srcmenu=<%=src%>&page=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>"><%= cntr%></A>
       <%
    	}	
	 %>
	</td>
	<%
	  }

	if (hasMore)
	{   
   %>
   <td colspan = 3 align = center>
  	&nbsp;&nbsp;&nbsp;&nbsp;<A href="milestonehome.jsp?searchCriteria=<%=searchCriteria%>&srcmenu=<%=src%>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
	</td>	
	<%
  	}	
	%>
   </tr>
  </table>
  
  </Form>

	<%
	} //end of if body for page right
	else{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%
	} //end of else body for page right
}//end of if body for session
else {
%>
	<jsp:include page="timeout.html" flush="true"/>
<%}
%>
<br>
<div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>

<DIV class="mainMenu" id = "emenu"> 
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>

</body>
</html>

