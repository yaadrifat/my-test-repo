<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="skinChoser.jsp" flush="true"/>
<HTML>
<HEAD>
<title><%=LC.L_Payment_Dets%><%--Payment Details*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
	<%-- Nicholas : Start --%>
	<%--<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>--%>
	<%-- Nicholas : End --%>
<SCRIPT LANGUAGE="JavaScript" src="calendar.js"></SCRIPT>


</HEAD>

<script>

function validate(formobj) {
	 
	if (!(validate_col('e-Signature',formobj.eSign))) return false;
    if(isNaN(formobj.eSign.value) == true) {
    	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		formobj.subVal.value="Submit";
		return false;
	   }

return true;
} 
</script>

<BODY style="overflow:auto;">
<div>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>



<%
  HttpSession tSession = request.getSession(true); 
  String studyId = "";
  String paymentPk = "";
  String mode = "";

 if (sessionmaint.isValidSession(tSession))
	{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	<%-- Nicholas : Start --%>
	<jsp:include page="skinChoser.jsp" flush="true"/>
	<%-- Nicholas : End --%>
<%

		studyId = request.getParameter("studyId");
		String  pageRight = request.getParameter("pR");
		String src;
		src= request.getParameter("srcmenu");
		paymentPk = request.getParameter("paymentPk");

		


	String acc = (String) tSession.getAttribute("accountId");
	int accId = 0;

	
		accId = EJBUtil.stringToNum(acc);	
%>
  <jsp:include page="paymenttabs.jsp" flush="true">
	<jsp:param name="studyId" value="<%=studyId%>"/>
	<jsp:param name="selectedTab" value="3"/>
	<jsp:param name="paymentPk" value="<%=paymentPk%>"/>
	<jsp:param name="pR" value="<%=pageRight%>"/>
  </jsp:include>
 

	  <jsp:include page="budgetbrowserpg.jsp" flush="true">
		<jsp:param name="studyId" value="<%=studyId%>"/>
		<jsp:param name="pageRight" value="<%=pageRight%>"/>
		<jsp:param name="paymentPk" value="<%=paymentPk%>"/>
		<jsp:param name="calledFrom" value="payment"/>
	  </jsp:include>
	  
		
<br><br>


<%
	}
 %>
</DIV>
</BODY>


