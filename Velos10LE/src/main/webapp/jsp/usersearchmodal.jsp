<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>
<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="codeLst" scope="session" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<%@ page import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,com.velos.esch.business.common.*"%>
<%@ page import="com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.*"%>
<%@ page import="com.velos.eres.web.user.UserJB"%>

<%!
private static final String EMPTY_STRING = "";
%>
<%
try {
	if (!sessionmaint.isValidSession(request.getSession(false))) {
		out.println(EMPTY_STRING);
		return;
	}
} catch(Exception e) {}
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<script LANGUAGE="JavaScript" SRC="validations.js"></script>
<script>
var primaryOrgId = '';
function resetUserSearchInput() {
	document.userSearch.fname.value= '';
	document.userSearch.lname.value= '';
	document.userSearch.jobType.selectedIndex = document.userSearch.jobType.options.length-1;
	document.userSearch.usrgrps.selectedIndex = document.userSearch.usrgrps.options.length-1;
	document.userSearch.ByStudy.selectedIndex = document.userSearch.ByStudy.options.length-1;
	if (primaryOrgId != '') {
		var orgOpts = document.userSearch.accsites.options;
		for (var iX=0; iX<orgOpts.length; iX++) {
			if (orgOpts[iX].value == primaryOrgId) {
				document.userSearch.accsites.selectedIndex = iX;
				break;
			}
		}
	}
}
function loadUserSearchResults(targetId) {
	resetUserSearchInput();
	document.userSearch.targetId.value = targetId;
	$j.post('usersearchmodalresults.jsp',
			{ targetId:targetId },
			function(data) {
				$j('#userSearchModalResultsDiv').html(data);
			}
	);
}
function reloadUserSearchResults() {
	$j.post('usersearchmodalresults.jsp',
			$j('#userSearch').serialize(),
			function(data) {
				$j('#userSearchModalResultsDiv').html(data);
			}
	);
	return false;
}
function navigateUserSearchResults(nextUrl) {
	$j.post(nextUrl,
			$j('#userSearch').serialize(),
			function(data) {
				$j('#userSearchModalResultsDiv').html(data);
			}
	);
	return false;
}
</script>
<%
response.setHeader("Cache-Control", "max-age=1, must-revalidate");
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1)
     ienet = 0; //IE
    else
	ienet = 1;
HttpSession tSession = request.getSession(true);
String userIdFromSession = (String) tSession.getAttribute("userId");

	//get parameters to make this window usable by all pages without adding code here
	 String genOpenerFormName = request.getParameter("genOpenerFormName") ;
	 String genOpenerUserIdFld = request.getParameter("genOpenerUserIdFld") ;
	 String genOpenerUserNameFld  = request.getParameter("genOpenerUserNameFld") ;

	if(genOpenerFormName == null){
	   genOpenerFormName="";
	   }
	if(genOpenerUserIdFld == null){
	   genOpenerUserIdFld="";
	   }
  	if(genOpenerUserNameFld == null){
	   genOpenerUserNameFld="";
    }


	String from=request.getParameter("from") ;


	String orgName ="";
	orgName=request.getParameter("accsites") ;
	orgName=(orgName==null)?"":orgName;

	//JM:
	String gName = "";
	gName = request.getParameter("usrgrps");
	gName=(gName==null)?"":gName;

	String stTeam = "";
	stTeam = request.getParameter("ByStudy");
	stTeam=(stTeam==null)?"":stTeam;

	String accountId = (String) tSession.getValue("accountId");
	String fromTxt ="";

	if(from == null){
	   from="";
	}

	if(from.equals("enroll"))
	{
		fromTxt= request.getParameter("fromTxt");

	}
// Getting the default site
	String userSiteId = "";
	int primSiteId = 0;
	int userId = 0;


    userId = EJBUtil.stringToNum(userIdFromSession);
	//userB.setUserId(userId);
	//userB.getUserDetails();
	userB=(com.velos.eres.web.user.UserJB)tSession.getAttribute("currentUser");
	userSiteId = userB.getUserSiteId();
	System.out.println("userSiteID"+userSiteId);
	userSiteId=(userSiteId==null)?"":userSiteId;
	primSiteId= EJBUtil.stringToNum(userSiteId);


		String userJobType = "";
		String dJobType = "";
		String dAccSites ="";
		String jobVal = request.getParameter("jobType");

		if(jobVal==null)
			jobVal = "";
		String siteVal = request.getParameter("accsites");



		String mode="";
		mode=request.getParameter("mode");
		if (mode==null) mode="";



		if(siteVal == null)
			siteVal = "";
		CodeDao cd = new CodeDao();
		CodeDao cd2 = new CodeDao();
		cd2.getAccountSites(EJBUtil.stringToNum(accountId));
		cd.getCodeValues("job_type");
		userJobType =userB.getUserCodelstJobtype();


		if (jobVal.equals(""))
		  dJobType=cd.toPullDown("jobType");

		else
		  dJobType=cd.toPullDown("jobType",EJBUtil.stringToNum(jobVal),true);

//		if (siteVal.equals(""))
//		   dAccSites=cd2.toPullDown("accsites");


//		else
//		 if ((mode.length()==0) && (siteVal.length()==0)){
		 if( (siteVal.length()==0) && (mode.length()==0)){
		  dAccSites=cd2.toPullDown("accsites",primSiteId,true);
		  orgName=userSiteId;
		  mode="M";
		   }
		 else
		    dAccSites=cd2.toPullDown("accsites",EJBUtil.stringToNum(siteVal),true);







	String fname = LC.L_All;/*String fname = "All";*****/
	String lname = LC.L_All;/*String lname = "All";*****/
	String jobtype = LC.L_All ;/*String jobtype = "All" ;*****/
	String accsites = LC.L_All;/*String accsites = "All";*****/
	String usrgrps  = LC.L_All;/*String usrgrps  = "All";*****/
	String ByStudy = LC.L_All;/*String ByStudy = "All";*****/

	fname = StringUtil.trueValue(request.getParameter("fname"));
	lname = StringUtil.trueValue(request.getParameter("lname"));

	jobtype = request.getParameter("jobType");

	accsites =request.getParameter("accsites");


	String jobDesc = LC.L_All;/*String jobDesc = "All";*****/
	String siteDesc = LC.L_All;/*String siteDesc = "All";*****/
	String gDesc = LC.L_All ;/*String gDesc = "All" ;*****/
	String tDesc = LC.L_All;/*String tDesc = "All";*****/
	// Fix for bug #1547, getCodeDesc gets the description for the code from the Dao's ArrayLists. Changed for jobtype as well to reduce SQLs fired.
	jobDesc = cd.getCodeDesc(EJBUtil.stringToNum(jobtype));

	if (accsites==null) {
		siteDesc = cd2.getCodeDesc(primSiteId);
		//orgName=userSiteId;
		//mode="M";
		}
	else
		siteDesc = cd2.getCodeDesc(EJBUtil.stringToNum(accsites));

//JM: 23/08/05 to make drop down of group
	    String dUsrGrps="";
	    String selGroup="";
	    int ac=EJBUtil.stringToNum(accountId);
	    CodeDao cd1 = new CodeDao();
	    cd1.getAccountGroups(ac);

	    selGroup=request.getParameter("usrgrps");
	    if (selGroup==null)  selGroup="";
	    if (selGroup.equals(""))
	    dUsrGrps = cd1.toPullDown("usrgrps");
	    else
	    dUsrGrps = cd1.toPullDown("usrgrps",EJBUtil.stringToNum(selGroup),true);
	    usrgrps =request.getParameter("usrgrps");
	    gDesc=cd1.getCodeDesc(EJBUtil.stringToNum(usrgrps));


//JM: 24/08/05
	 String dStudy="";
	 String selstudy="";
	 int selstudyId=0;
  	 selstudy=request.getParameter("ByStudy") ;
		if(selstudy==null ){
		selstudyId=0 ;
		}else {
		selstudyId = EJBUtil.stringToNum(selstudy);
		}

	StudyDao studyDao = new StudyDao();
	String suserId = (String) tSession.getValue("userId");
	studyDao.getStudyValuesForUsers(suserId);
	ArrayList studyIds = studyDao.getStudyIds();
	ArrayList studyNumbers = studyDao.getStudyNumbers();

	//make dropdowns of study
	dStudy=EJBUtil.createPullDown("ByStudy",selstudyId,studyIds,studyNumbers);
	ByStudy =request.getParameter("ByStudy");
	//JM: get the study number from the selected study id
	tDesc=studyDao.getStudy(EJBUtil.stringToNum(ByStudy));

%>
<script>
primaryOrgId = "<%=primSiteId%>";
</script>
<BR>
<DIV>
<P class="sectionheadings"><%=LC.L_Search_Criteria%></P>
<Form id="userSearch" name="userSearch" method="post" action="#" onsubmit="return reloadUserSearchResults();">
  <Input type="hidden" name=from value=<%=from%>>
  <Input type="hidden" name=fromTxt value=<%=fromTxt%>>
  <Input type="hidden" name="selectedTab" value="1">
  <Input type="hidden" name="genOpenerFormName" value=<%=genOpenerFormName%>>
  <Input type="hidden" name="genOpenerUserIdFld" value=<%=genOpenerUserIdFld%>>
  <Input type="hidden" name="genOpenerUserNameFld" value=<%=genOpenerUserNameFld%>>
  <Input type="hidden" name="mode" value=<%=mode%>>
  <Input type="hidden" name="targetId" id="targetId" value="">


  <table width="99%" cellspacing="2" cellpadding="2" border="0" class="basetbl midalign" >
    <tr valign="bottom">

      <td width=25%> <%=LC.L_User_FirstName%><%--User First Name*****--%>:
		  <%if(fname.equals("All")){%>
		   <Input type=text name="fname" size=20>
		  <%}else{%>
	<!-- km- to fix Bug#2308 -->
        <Input type=text name="fname" size=20 value="<%=fname%>">
		  <%}%>
      </td>
      <td width=25%> <%=LC.L_User_Last_Name%><%--User Last Name*****--%>:
		   <%if(lname.equals("All")){%>
		  <Input type=text name="lname" size=20>
		  <%}else{%>
        <Input type=text name="lname" size=20 value="<%=lname%>">
		   <%}%>
      </td>

	  <td width=20%> <%=LC.L_Job_Type%><%--Job Type*****--%>:
		 <%=dJobType%>
	  </td>

	  <td width=30%> <%=LC.L_Organization_Name%><%--Organization Name*****--%>:
         <%=dAccSites%>
	  </td>

    </tr>

    <tr>
    <td width=25%> <%=LC.L_Group_Name%><%--Group Name*****--%>:         <%=dUsrGrps %> </td>
    <td width=25%> <%=LC.L_Std_Team%><%--<%=LC.Std_Study%> Team*****--%>:         <%=dStudy%> </td>
    <td valign="bottom">
     <br/>
	 <button type="submit"><%=LC.L_Search%></button>
    </td>
    </tr>
  </table>
</Form>

<p class="defcomments"><A href="#" 
  onClick="removeSelectedUser(document.userSearch.targetId.value);"><%=MC.M_Rem_SelcUser%><%--Remove Already Selected User*****--%></A></p>

</div>

<div id="userSearchModalResultsDiv" style="height:270">
	<br/>
	<p><%=LC.L_Loading%><img class="asIsImage_progbar" src="../images/jpg/loading_pg.gif" /></p>
	<br/>
</div>
