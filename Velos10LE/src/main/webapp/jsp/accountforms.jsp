<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.velos.eres.service.util.*"%><html>  
<head>
<title><%--Manage Account  >> Form Management***** --%> <%=MC.M_MngAcc_FrmMgmt%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<script>
function setOrder(formObj,orderBy,pgRight){ //orderBy column number
	var lorderType;
	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";		
		lorderType="desc";
	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
		lorderType="asc";	  
	}

	lsrc = formObj.srcmenu.value;
	lcurPage = formObj.page.value;
	formObj.action="accountforms.jsp?srcmenu="+lsrc+"&page="+lcurPage+"&orderBy="+orderBy+"&orderType="+lorderType;
	setFilterText(formObj);
	formObj.submit(); 
	
  }
function setFilterText(form){
	if (form.formname.value != '')
	{
 	 	form.ftxt.value=form.formname.value;//form name
 	}
 	else
 	{
 		form.ftxt.value = '<%=LC.L_All%>'/*String ftxt="All"*****/;
 	}	
 	if (form.ByStudy.options[form.ByStudy.selectedIndex].value != '')
 	{
		form.stxt.value= form.ByStudy.options[form.ByStudy.selectedIndex].text ;//study
	}
	else
	{
		form.stxt.value='<%=LC.L_All%>'/*String ftxt="All"*****/;
	}	 
	
	if (form.linked.options[form.linked.selectedIndex].text == '<%=LC.L_Select_AnOption%>')/*if (form.linked.options[form.linked.selectedIndex].text == 'Select an Option')*****/
	{
	 	form.ltxt.value= '<%=LC.L_All%>'/*String ftxt="All"*****/;
	}
	else
	{
		form.ltxt.value= form.linked.options[form.linked.selectedIndex].text ;//linked to
	}		
	
	if ( form.ddorg.options[form.ddorg.selectedIndex].value != '')
	{	
	 form.otxt.value= form.ddorg.options[form.ddorg.selectedIndex].text ;//for organization
	}
	else
	{
		form.otxt.value='<%=LC.L_All%>'/*String ftxt="All"*****/;
	}	 
	 if ( form.ddgrp.options[form.ddgrp.selectedIndex].value != '')
	 {
	 	form.gtxt.value=form.ddgrp.options[form.ddgrp.selectedIndex].text;//for Groups
	 }
	 else
	 {
	 	form.gtxt.value='<%=LC.L_All%>'/*String ftxt="All"*****/;
	 }		
 }
 
function confirmBox(formName,lfDataCount,acc_form_rights,studFormRight) {
	var paramArray = [formName];
	msg=getLocalizedMessageString("L_Delete_Form",paramArray);/*msg="Delete Form '" + formName+ "'?";*****/
	if(studFormRight<0){
		if(f_check_perm(acc_form_rights,'E')){
			if(lfDataCount > 0){
		      alert("<%=MC.M_FrmAns_CntRem%>");/*alert("Form has been answered and cannot be removed."); *****/
			  return false;
				}
			if (confirm(msg)) {
		   		return true;
					}
			else{
				return false;
			  	}
			}
			else{
				return false;
				}
			}
	else	{
		if(studFormRight>0) {
			if(f_check_perm(studFormRight,'E')){
				if(lfDataCount > 0)
				{
			      alert("<%=MC.M_FrmAns_CntRem%>");/*alert("Form has been answered and cannot be removed."); *****/
				  return false;
				}
				if (confirm(msg)) {
			   		return true;
					}
				else{
					return false;
				  }
				}
				else
				{
					return false;
				}    
			}
		else   {
			if(lfDataCount > 0)
				{
	      		alert("<%=MC.M_FrmAns_CntRem%>");/*alert("Form has been answered and cannot be removed."); *****/
		  		return false;
				}
				if (confirm(msg)) {
	   				return true;
					}
				else{
					return false;
		  		}
	
			}
	  }
}

function openhideseq(pageright) {	
	if(f_check_perm(pageright,'E')== true){
	  windowName=window.open("formhideseq.jsp?calledFrom=A","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=680,height=500, top = 100 , left = 100");
	  windowName.focus();
	 }
	else
	{
	return false;
	}
}


function openwin3(stdynotassoright) {
	if(f_check_perm(stdynotassoright,'N') == true){
	windowName=window.open("copyFormForAccount.jsp","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=850,height=600,left=150,top=60");
	windowName.focus();
	}
	else{
	return false;
	}
}


function openwin1(fid,pkLf) {
	windowName=window.open("formstatushistory.jsp?linkFrom=A&formId="+fid+"&linkedFormId="+pkLf,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=600,height=400,top=100,left=200")
	windowName.focus();
}
function openwin2(accformright) {
	if(f_check_perm(accformright,'N') == true){
    windowName=window.open("linkformstostudyacc.jsp?linkFrom=A","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=830,height=500,top=100,left=100")
	windowName.focus();}
	else{
	return false;
	}
}
	
	
function openwin4(pkLf,activeId,statCodeId) 
{
	param1="attributesForStudyAccount.jsp?linkFrom=A&linkedFormId="+pkLf+"&statCodeId="+statCodeId+"&changeVal=";
    windowName=window.open(param1,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=725,height=595,top=150,left=150") 
	windowName.focus();
}

function isNotMigrateStatus(migrateStatPK, statCodeId)
{

	if (migrateStatPK == statCodeId )
	{
		alert("<%=MC.M_AppMigrating_FrmResps%>");/*alert("The application is migrating the form's responses to the latest Version. The form cannot be edited at this point.");*****/
		return false; 
	}
	else
	{
		return true;
	}
}

function openFormPreview(formId)
{
	windowName=window.open("formpreview.jsp?formId="+formId,"Information","toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes,width=825,height=600,top=90,left=150");
	windowName.focus();
}

//Added by Manimaran for Enhancement F4
function saveLib(url, accformright, migrateStatPK, statCodeId)
{	
	//KM -21Oct08
	if(isNotMigrateStatus(migrateStatPK, statCodeId) == false)
		return false;
	
	if(f_check_perm(accformright,'N') == true){
		windowName=window.open(url,"SaveToLibrary","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,top=150, left=200,width=650,height=330"); 
		windowName.focus();
	}
	else{
	return false;
	}
}
	

</script>

<% String src="";
src= request.getParameter("srcmenu");
%>	

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   

<body>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="stdRightsB" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<!--jsp:useBean id ="userB" scope="page" class="com.velos.eres.web.user.UserJB"/-->
<!--jsp:useBean id ="userSiteB" scope="request" class="com.velos.eres.web.userSite.UserSiteJB"/-->
<onload="setfocus()">
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>



<%@ page language = "java" import = "com.velos.eres.business.common.CtrlDao,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,com.velos.eres.business.section.*,java.util.*,java.io.*,java.sql.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.StringUtil"%>


<DIV class="tabDefTopN" id = "div1"> 

<jsp:include page="accounttabs.jsp" flush="true"> 
	<jsp:param name="selectedTab" value="5"/>
	</jsp:include>
</DIV>			
<DIV class="tabDefBotN" id="div2"> 


<%
int curPage = 0; 
long startPage = 1;
long cntr=0;


String pagenum = "";
pagenum = request.getParameter("page");

if (pagenum == null)
{
	pagenum = "1";
}
curPage = EJBUtil.stringToNum(pagenum);

String orderBy = "";
orderBy = request.getParameter("orderBy");

if (EJBUtil.isEmpty(orderBy))
   orderBy = "15"; // changed by sonia for form_name_lower


String orderType = "";
orderType = request.getParameter("orderType");

if (orderType == null)
{
	orderType = "asc";
}


   HttpSession tSession = request.getSession(true); 
   if (sessionmaint.isValidSession(tSession))
   	{
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
		//int	study_not_associated_right = 7;//Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMSNOSTUDY"));
		//int study_associated_right = 7;//Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMSWSTUDY"));
		int account_form_rights = Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMS"));
		int formLibRights = Integer.parseInt(grpRights.getFtrRightsByValue("MFRMLIB"));
				
     
     
	     String modRight = (String) tSession.getValue("modRight");
		 int patProfileSeq = 0, formLibSeq = 0;	
		 CtrlDao acmod = new CtrlDao();
		 acmod.getControlValues("module");
		 ArrayList acmodfeature =  acmod.getCValue();
		 ArrayList acmodftrSeq = acmod.getCSeq();
		 char formLibAppRight = '0';
		 char patProAppRight = '0';
		 formLibSeq = acmodfeature.indexOf("MODFORMS");
		 patProfileSeq = acmodfeature.indexOf("MODPATPROF");	
	 	 formLibSeq = ((Integer) acmodftrSeq.get(formLibSeq)).intValue();
	 	 patProfileSeq = ((Integer) acmodftrSeq.get(patProfileSeq)).intValue();
	     formLibAppRight = modRight.charAt(formLibSeq - 1);
	     patProAppRight = modRight.charAt(patProfileSeq - 1);

	// module check 
	if( 
	((String.valueOf(formLibAppRight).compareTo("1") == 0)&&(account_form_rights>=4))  ||
	((String.valueOf(patProAppRight).compareTo("1") == 0)&&(account_form_rights>=4)) 
	 )
	{				
	String userId = (String) tSession.getValue("userId");
    String accountId = (String) tSession.getValue("accountId");		

	int accId=EJBUtil.stringToNum(accountId);
	/*
	userB.setUserId(EJBUtil.stringToNum(userId));
	userB.getUserDetails();
	int userPrimOrg = EJBUtil.stringToNum(userB.getUserSiteId());
     	*/
     String study="";
	 String dAccSites ="";
	 String dAccGroups = "";
	 
	 String frmname="";
	 String selstudy="";
	 String  site="";


	 String ftxt=LC.L_All; /*String ftxt="All";*****/
     String stxt=LC.L_All; /*String stxt="All";*****/
	 String ltxt=LC.L_All; /*String ltxt="All";*****/
	 String otxt=LC.L_All; /*String otxt="All";*****/
	 String gtxt=LC.L_All; /*String gtxt="All";*****/

	 
	 int siteId=0;
	 /*
	 int siteIdAcc=0;
	 int userSiteSiteId = 0;
         int userSiteRight = 0;
	 int rights=1;
	 */
	 int groupId = 0;
	 String group="";
	 int selstudyId=0;
	 String linkedto="";
	 frmname=request.getParameter("formname");
	 		if(frmname==null) 
	 		{frmname="";}
			selstudy=request.getParameter("ByStudy") ;
	 		if(selstudy==null )  
	 		{selstudyId=0 ;}
			 else 
			 { selstudyId = EJBUtil.stringToNum(selstudy); }
			 
	         linkedto=request.getParameter("linked");
			if(linkedto==null)
			{
			 if ((String.valueOf(formLibAppRight).compareTo("1") != 0)&&(String.valueOf(patProAppRight).compareTo("1") == 0))
			 {
				linkedto="'PA'";
			 }
			 else if ((String.valueOf(patProAppRight).compareTo("1") != 0)&&(String.valueOf(formLibAppRight).compareTo("1") == 0)) 			 
			 {        	         
				linkedto="'S','SP','A','SA','PS','PR'";						
		 	 }		  
			 else  			 
			 {        	         
				linkedto="'S','SP','A','SA','PS','PR','PA'";						
		 	 }			 
		    
		   }   
			System.out.println("linked to"+linkedto);
			 site=request.getParameter("ddorg");
			 if(site==null)
			 { siteId=0;}
			 else
			 { siteId=EJBUtil.stringToNum(site);}


			 group=request.getParameter("ddgrp");
			 if(group==null)
			 { groupId=0;}
			 else
			 { groupId=EJBUtil.stringToNum(group);
			 }
		
		if (( request.getParameter("ftxt"))!= null &&  (request.getParameter("ftxt").length() > 0)  )
			ftxt= request.getParameter("ftxt");	 
		if (( request.getParameter("stxt"))!= null &&  (request.getParameter("stxt").length() > 0)  )		     
			stxt= request.getParameter("stxt");	 
		if (( request.getParameter("ltxt"))!= null &&  (request.getParameter("ltxt").length() > 0)  )		     
			ltxt= request.getParameter("ltxt");	 
		if (( request.getParameter("otxt"))!= null &&  (request.getParameter("otxt").length() > 0)  )		     
			otxt= request.getParameter("otxt");	 
		if (( request.getParameter("gtxt"))!= null &&  (request.getParameter("gtxt").length() > 0)  )		     
			gtxt= request.getParameter("gtxt");	 
			 
		 
		 

		 StudyDao studyDao = new StudyDao();	
		 studyDao.getStudyValuesForUsers(userId);	
		 ArrayList studyIds = studyDao.getStudyIds();
		 ArrayList studyNumbers = studyDao.getStudyNumbers();
		 String tab = request.getParameter("selectedTab"); 

		//make dropdowns of study, organization and group
		 study=EJBUtil.createPullDown("ByStudy",selstudyId,studyIds,studyNumbers);

		 CodeDao cd2 = new CodeDao();
		 int activeId = cd2.getCodeId("frmstat","A");
		 int migrateStatPK = 0;
		 migrateStatPK  = cd2.getCodeId("frmstat","M");
		 
		 cd2.getAccountSites(EJBUtil.stringToNum(accountId));
		 dAccSites = cd2.toPullDown("ddorg",siteId,true);

		 CodeDao cd3 = new CodeDao();
		 cd3.getAccountGroups(EJBUtil.stringToNum(accountId));
		 dAccGroups = cd3.toPullDown("ddgrp",groupId,true);
		//return rcheck(<%=account_form_rights,accountformsearch.linked.value) 

		//Commented by Manimaran to fix the Bug.2565
		//km
		/*
		ArrayList userSiteIds = new ArrayList();
		ArrayList userSiteSiteIds = new ArrayList();	
     		ArrayList userSiteRights = new ArrayList();
		
		UserSiteDao userSiteDao = userSiteB.getUserSiteTree(EJBUtil.stringToNum(accountId),EJBUtil.stringToNum(userId));
		userSiteIds = userSiteDao.getUserSiteIds();
		userSiteSiteIds = userSiteDao.getUserSiteSiteIds();
		userSiteRights = userSiteDao.getUserSiteRights();
		int len1 = userSiteIds.size();
		*/
		
		StringBuffer sbSelect = new StringBuffer();
		StringBuffer sbFrom = new StringBuffer();
		StringBuffer sbWhere = new StringBuffer();
		
		String stdLabStr = LC.L_Study/*"LC.Std_Study"*****/;
		if (stdLabStr == "") stdLabStr = LC.L_Study/*"Study"*****/;
		String stdLabStrPl = LC.L_Studies/*LC.Std_Studies*****/;
		Object[] arguments2 = {stdLabStrPl};
		Object[] arguments3 = {stdLabStr};
		if (stdLabStrPl == "") stdLabStrPl = LC.L_Studies/*"Studies"*****/;

		sbSelect.append("select F1.PK_FORMLIB, F2.PK_LF, F1.FORM_NAME, F4.pk_formstat as formStatId, F1.FORM_DESC,"); //F5.STUDY_NUMBER,
		sbSelect.append(" F4.fk_codelst_stat as statCodeId , F2.LF_LNKFROM as linkfrom , F2.fk_study as formstudy,"); 
		sbSelect.append(" F2.lf_datacnt as lfDataCnt,F2.lf_displaytype,nvl(F2.lf_hide,0) lf_hide, nvl(F2.lf_seq,0) lf_seq,"); 
		sbSelect.append(" (select codelst_desc  from er_codelst where pk_codelst=F4.fk_codelst_stat) as form_status, ");
		sbSelect.append(" case when(F2.lf_displaytype='S') then '" + stdLabStr + "'");
		sbSelect.append(" when(F2.lf_displaytype='A') then '"+LC.L_Account+"'");/*sbSelect.append(" when(F2.lf_displaytype='A') then 'Account'");*****/
		sbSelect.append(" when(F2.lf_displaytype='SA') then ' "+ VelosResourceBundle.getLabelString("L_All_Dyna",arguments2) + "'");/*sbSelect.append(" when(F2.lf_displaytype='SA') then 'All "+ stdLabStrPl + "'");*****/
		sbSelect.append(" when(F2.lf_displaytype='PA') then ' "+LC.L_All_Patients+"'");/*sbSelect.append(" when(F2.lf_displaytype='PA') then 'All "+LC.L_Patients+"'");*****/
		sbSelect.append(" when(F2.lf_displaytype='PS') then '"+VelosResourceBundle.getLabelString("L_Pat_All",arguments2) +"'");/*sbSelect.append(" when(F2.lf_displaytype='PS') then '"+LC.L_Patient+" (All "+ stdLabStrPl +")'");*****/
		sbSelect.append(" when(F2.lf_displaytype='PR') then '"+VelosResourceBundle.getLabelString("L_Pat_AllRestricted",arguments2)+" '");/*sbSelect.append(" when(F2.lf_displaytype='PR') then '"+LC.L_Patient+" (All "+stdLabStrPl+" - Restricted)'");*****/
		sbSelect.append(" when(F2.lf_displaytype='SP') then '"+VelosResourceBundle.getLabelString("L_PatSpec",arguments3)+"' end as displaytype, lower(f1.form_name ) lower_form_name ");/*sbSelect.append(" when(F2.lf_displaytype='SP') then '"+LC.L_Patient+" (Specific "+stdLabStr+")' end as displaytype, lower(f1.form_name ) lower_form_name ");*****/
		
		//Added by Manimaran-to fix Bug 2297  Study Number NOT getting displayed for Study Specific Forms
		sbSelect.append(",(select study_number from er_study where pk_study = f2.fk_study) as study_number");
		// Query Added by gopu for Nov.2005 Enhancemet #4
		sbSelect.append(",(select usr_firstname ||''||usr_lastname from er_user where pk_user=f1.creator) as creator");
		sbSelect.append(",to_char(f1.created_on) as created_on");
		sbSelect.append(",(select usr_firstname ||''||usr_lastname from er_user where pk_user=f1.last_modified_by) as last_modified_by");
		sbSelect.append(",to_char(f1.last_modified_date) as last_modified_date");
						

		//changed by sonia, the default where clause without any user specified filter is added first to the stringbuffer	
		sbWhere.append(" WHERE  F2.FK_ACCOUNT = " +accId +" AND F2.FK_FORMLIB = F1.PK_FORMLIB ");
		sbWhere.append(" and F2.record_type <> 'D' ");
		//Added by Manimaran for two null end date issue 4149
		sbWhere.append (" AND F4.PK_FORMSTAT = (select max(pk_formstat) from er_formstat where fk_formlib = f1.pk_formlib and formstat_enddate is null) ");
		
		
		//km-to get forms which have access other than primary organization
		//String siteIds1="";
		//String siteIds2="";
		if (siteId==0) 
		{ 
			sbFrom.append(" FROM ER_FORMLIB F1, ER_LINKEDFORMS F2, er_formstat F4"); 
			//Commented by Manimaran to fix the Bug 2565
			/*	
			for (int counter=0;counter<len1;counter++) {
	 		userSiteSiteId = EJBUtil.stringToNum(((userSiteSiteIds.get(counter)) == null)?"-":(userSiteSiteIds.get(counter)).toString());
     			userSiteRight = EJBUtil.stringToNum(((userSiteRights.get(counter)) == null)?"-":(userSiteRights.get(counter)).toString());
			
			if (userPrimOrg!=userSiteSiteId && userSiteRight==0)
			   siteIds1=siteIds1+(userSiteSiteId+",");
			else
			   siteIds2=siteIds2+(userSiteSiteId+",");		
			}
		        if (siteIds1!="")
			{
			siteIds1=siteIds1.substring(0,siteIds1.lastIndexOf(","));
			siteIds2=siteIds2.substring(0,siteIds2.lastIndexOf(","));
			sbWhere.append("and f1.pk_formlib not in((select fk_formlib from er_formorgacc where fk_site in("+siteIds1+")) minus (select fk_formlib from er_formorgacc where fk_site in("+siteIds2+")))");
			}
			*/

		} else 
		{ 
			sbFrom.append("  FROM ER_FORMLIB F1  , ER_LINKEDFORMS  F2 ,er_formorgacc F3 , er_formstat F4"); 
			
			//Commented by Manimaran to fix the Bug 2565
			/*
			siteIdAcc=siteId;
			for (int counter=0;counter<len1;counter++) {
	 		userSiteSiteId = EJBUtil.stringToNum(((userSiteSiteIds.get(counter)) == null)?"-":(userSiteSiteIds.get(counter)).toString());
     			userSiteRight = EJBUtil.stringToNum(((userSiteRights.get(counter)) == null)?"-":(userSiteRights.get(counter)).toString());
			
			if ((siteId==userSiteSiteId) && (userPrimOrg!=userSiteSiteId))
			   rights=userSiteRight;
			}
			if(rights==0)
			    siteIdAcc=-1;
			*/
			sbWhere.append(" and F3.FK_SITE = "+siteId);
			sbWhere.append(" and F3.FK_FORMLIB = F1.PK_FORMLIB");

		} 
		if (groupId > 0) 
			{
				
			sbFrom.append(" , ER_FORMGRPACC F5" );
			sbWhere.append(" and F5.FK_GROUP = " + groupId );
			sbWhere.append(" and F5.FK_FORMLIB = F1.PK_FORMLIB" );
				
			}
				 
		if (selstudyId > 0)			   
			{
			sbWhere.append(" and F2.FK_STUDY = "+selstudyId);
			}			   			   

		if (! EJBUtil.isEmpty(frmname) )
			{
			  sbWhere.append(" and lower(F1.form_name) like lower('%"+frmname.trim()+"%')");
			}
		
		if(! EJBUtil.isEmpty(linkedto)) 
		{
		   sbWhere.append(" and lf_displaytype IN ( "+linkedto+" )");
		}
		
//		sbWhere.append(" order by lower(F1.form_name)");
			
		String mainsql= sbSelect.toString() + sbFrom.toString() + sbWhere.toString(); 
		
		String countSql = " select count(*) from (" + sbSelect.toString() + sbFrom.toString() + sbWhere.toString()+ ")";
		
	   long rowsPerPage=0;
   	   long totalPages=0;	
	   long rowsReturned = 0;
	   long totalRows = 0;	   
	   long showPages = 0;
	   long firstRec = 0;
	   long lastRec = 0;
	   	   
	   boolean hasMore = false;
	   boolean hasPrevious = false;
	   
		
		rowsPerPage=Configuration.MOREBROWSERROWS;
		totalPages =Configuration.PAGEPERBROWSER ;
             System.out.println("mainsql::"+mainsql);
        BrowserRows br = new BrowserRows();
	    br.getPageRows(curPage,rowsPerPage,mainsql,totalPages,countSql,orderBy,orderType);
	    rowsReturned = br.getRowReturned();
		totalRows = br.getTotalRows();
		showPages = br.getShowPages();
		startPage = br.getStartPage();
		hasMore = br.getHasMore();
		hasPrevious = br.getHasPrevious();
		firstRec = br.getFirstRec();
		lastRec = br.getLastRec();	
	   
		
		String formId = null;
		String formStatId = null;
		String studyId = null;
		String formLibId,statCodeId;
		String lfDataCnt = null;
		String lfPk	= "";
		int len	=(int)rowsReturned;
		
%>			


	<Form  name="accountformsearch" method="post" action="accountforms.jsp?page=1"  onSubmit="setFilterText(document.accountformsearch)">
   	<input type="hidden" name=srcmenu value="<%=src%>">
	  <div class="tmpHeight"></div>   	
	<table width="99%" cellspacing="0" cellpadding="0" class="basetbl midAlign">
      <tr height="20" >
	    <td colspan="6"><b><%=LC.L_Search_By%></b></td><%--<td colspan="6"><b>Search By</b></td>*****--%>
	</tr>
	<tr>
	
	  <td  width=12%> <%=LC.L_Frm_Name%>:</td> <%-- <td class=tdDefault width=12%> Form Name:</td> ***** --%>
		<td width=20%><Input type=text name="formname" size=20  value='<%=frmname%>'></td>
		<td  width=12% align="right"> <%=LC.L_Study%>:&nbsp; </td><%--<td class=tdDefault width=12% align="right"> <%=LC.Std_Study%>:&nbsp; </td>*****--%>
		<td width=20%><%=study%></td>
		<td  width=12% align="right"> <%=LC.L_Linked_To%>:&nbsp</td><%--<td class=tdDefault width=12% align="right"> Linked To:&nbsp;</td>*****--%>
		 
	<%if ((String.valueOf(formLibAppRight).compareTo("1") != 0)&&(String.valueOf(patProAppRight).compareTo("1") == 0))
	{
	  if(linkedto.equals("'PA'")){%>
	   <td><SELECT NAME=linked>
	<%-- <OPTION value = "'PA'" SELECTED>All <%=LC.Pat_Patients%>*****--%>
	     <OPTION value = "'PA'" SELECTED><%=LC.L_All_Patients%>
</OPTION>
	   </SELECT></td>
	 <%} else {%>
 	   <td><SELECT NAME=linked>
  <%-- <OPTION value = "'PA'">All <%=LC.Pat_Patients%>*****--%>
	   <OPTION value = "'PA'"><%=LC.L_All_Patients%>
</OPTION>
	   </SELECT></td>
	   <%}%>  
	<%}
	else if ((String.valueOf(formLibAppRight).compareTo("1") == 0)&&(String.valueOf(patProAppRight).compareTo("1") != 0))
 	{						
   	  if(linkedto.equals("'S'")){%>
	   <td><SELECT NAME=linked>
   <%--<OPTION value = "'S'" SELECTED><%=LC.Std_Study%></OPTION>*****--%>
	   <OPTION value = "'S'" SELECTED><%=LC.L_Study%></OPTION>
	  <%-- <OPTION value = "'SP'"><%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)</OPTION>*****--%>
	       <OPTION value = "'SP'"><%=LC.L_Pat_SpecStd%></OPTION>
	  <%-- <OPTION value = "'PS'"><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)</OPTION>*****--%>
	       <OPTION value = "'PS'"><%=LC.L_Pat_AllStd%></OPTION>
	  <%--<OPTION value = "'PR'"><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)</OPTION>*****--%>
	      <OPTION value = "'PR'"><%=LC.L_Pat_AllStdRest%></OPTION>
	<%-- <OPTION value = "'A'">Account</OPTION>*****--%>
	     <OPTION value = "'A'"><%=LC.L_Account%></OPTION>
	<%--  <OPTION value = "'SA'">All <%=LC.Std_Studies%></OPTION>*****--%>
   	      <OPTION value = "'SA'"><%=LC.L_All_Studies%></OPTION>
	 <%-- <OPTION value ="'S','SP','A','SA','PS','PR'" >Select an Option</OPTION>*****--%>
	       <OPTION value ="'S','SP','A','SA','PS','PR'" ><%=LC.L_Select_AnOption%></OPTION>
	  </SELECT></td>
	   <%}%>
	   <% if(linkedto.equals("'SP'")){%>
	  <td><SELECT NAME=linked>
	   <%-- <OPTION value = "'S'" ><%=LC.Std_Study%></OPTION>*****--%>
	        <OPTION value = "'S'" ><%=LC.L_Study%></OPTION>
	    <%--<OPTION value = "'SP'" SELECTED><%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)</OPTION>*****--%>
	       <OPTION value = "'SP'" SELECTED><%=LC.L_Pat_SpecStd%></OPTION>
	   <%--<OPTION value = "'PS'"><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)</OPTION>*****--%>
	       <OPTION value = "'PS'"><%=LC.L_Pat_AllStd%></OPTION>
	   <%--<OPTION value = "'PR'"><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)</OPTION>*****--%>
	      <OPTION value = "'PR'"><%=LC.L_Pat_AllStdRest%></OPTION>
	  <%--<OPTION value = "'A'">Account</OPTION>*****--%>  
	      <OPTION value = "'A'"><%=LC.L_Account%></OPTION>
	   <%--  <OPTION value = "'SA'">All <%=LC.Std_Studies%></OPTION>*****--%>
   	      <OPTION value = "'SA'"><%=LC.L_All_Studies%></OPTION>
	 <%-- <OPTION value ="'S','SP','A','SA','PS','PR'" >Select an Option</OPTION>*****--%>
	       <OPTION value ="'S','SP','A','SA','PS','PR'" ><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>
	   <%}%>
	   <% if(linkedto.equals("'A'")){%>
	   <td><SELECT NAME=linked>
	   <%-- <OPTION value = "'S'" ><%=LC.Std_Study%></OPTION>*****--%>
	        <OPTION value = "'S'" ><%=LC.L_Study%></OPTION>
	   <%--<OPTION value = "'SP'"><%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)</OPTION>*****--%>
	       <OPTION value = "'SP'"><%=LC.L_Pat_SpecStd%></OPTION>
       <%--<OPTION value = "'PS'"><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)</OPTION>*****--%>
	       <OPTION value = "'PS'"><%=LC.L_Pat_AllStd%></OPTION>
	   <%--<OPTION value = "'PR'"><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)</OPTION>*****--%>
	       <OPTION value = "'PR'"><%=LC.L_Pat_AllStdRest%></OPTION>
	  <%--<OPTION value = "'A'"SELECTED>Account</OPTION>*****--%>  
	      <OPTION value = "'A'"SELECTED><%=LC.L_Account%></OPTION>
	   <%--  <OPTION value = "'SA'">All <%=LC.Std_Studies%></OPTION>*****--%>
   	         <OPTION value = "'SA'"><%=LC.L_All_Studies%></OPTION>
	   <%-- <OPTION value ="'S','SP','A','SA','PS','PR'" >Select an Option</OPTION>*****--%>
	       <OPTION value ="'S','SP','A','SA','PS','PR'" ><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>
	   <%}%>
	   <% if(linkedto.equals("'SA'")){%>
	   <td><SELECT NAME=linked>
	   <%-- <OPTION value = "'S'" ><%=LC.Std_Study%></OPTION>*****--%>
	        <OPTION value = "'S'" ><%=LC.L_Study%></OPTION>
	   <%--<OPTION value = "'SP'"><%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)</OPTION>*****--%>
	       <OPTION value = "'SP'"><%=LC.L_Pat_SpecStd%></OPTION>
   <%--<OPTION value = "'PS'"><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)</OPTION>*****--%>
	   <OPTION value = "'PS'"><%=LC.L_Pat_AllStd%></OPTION>
	   <%--<OPTION value = "'PR'"><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)</OPTION>*****--%>
	       <OPTION value = "'PR'"><%=LC.L_Pat_AllStdRest%></OPTION>
	   <%--<OPTION value = "'A'">Account</OPTION>*****--%>  
	       <OPTION value = "'A'"><%=LC.L_Account%></OPTION>
	   <%--  <OPTION value = "'SA'"SELECTED>All <%=LC.Std_Studies%></OPTION>*****--%>
   	         <OPTION value = "'SA'"SELECTED><%=LC.L_All_Studies%></OPTION>
	   <%-- <OPTION value ="'S','SP','A','SA','PS','PR'" >Select an Option</OPTION>*****--%>
	       <OPTION value ="'S','SP','A','SA','PS','PR'" ><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>
	   <%}%>
	   <% if(linkedto.equals("'PS'")){%>
	   <td><SELECT NAME=linked>
	   <%-- <OPTION value = "'S'" ><%=LC.Std_Study%></OPTION>*****--%>
	        <OPTION value = "'S'" ><%=LC.L_Study%></OPTION>
	   <%--<OPTION value = "'SP'"><%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)</OPTION>*****--%>
	       <OPTION value = "'SP'"><%=LC.L_Pat_SpecStd%></OPTION>
	   <%--<OPTION value = "'PS'"SELECTED><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)</OPTION>*****--%>
	       <OPTION value = "'PS'"SELECTED><%=LC.L_Pat_AllStd%></OPTION>
	   <%--<OPTION value = "'PR'"><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)</OPTION>*****--%>
	       <OPTION value = "'PR'"><%=LC.L_Pat_AllStdRest%></OPTION>
	   <%--<OPTION value = "'A'">Account</OPTION>*****--%>  
	       <OPTION value = "'A'"><%=LC.L_Account%></OPTION>
	   <%--  <OPTION value = "'SA'">All <%=LC.Std_Studies%></OPTION>*****--%>
   	      <OPTION value = "'SA'"><%=LC.L_All_Studies%></OPTION>
	   <%-- <OPTION value ="'S','SP','A','SA','PS','PR'" >Select an Option</OPTION>*****--%>
	       <OPTION value ="'S','SP','A','SA','PS','PR'" ><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>
	   <%}%>
	   <% if(linkedto.equals("'PR'")){%>
	   <td><SELECT NAME=linked>
	   <%-- <OPTION value = "'S'" ><%=LC.Std_Study%></OPTION>*****--%>
	        <OPTION value = "'S'" ><%=LC.L_Study%></OPTION>
	   <%--<OPTION value = "'SP'"><%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)</OPTION>*****--%>
	       <OPTION value = "'SP'"><%=LC.L_Pat_SpecStd%></OPTION>
	   <%--<OPTION value = "'PS'"><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)</OPTION>*****--%>
	       <OPTION value = "'PS'"><%=LC.L_Pat_AllStd%></OPTION>
	   <%--<OPTION value = "'PR'"SELECTED><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)</OPTION>*****--%>
	       <OPTION value = "'PR'"SELECTED><%=LC.L_Pat_AllStdRest%></OPTION>
	 <%--<OPTION value = "'A'">Account</OPTION>*****--%>  
	       <OPTION value = "'A'"><%=LC.L_Account%></OPTION>
	  <%--  <OPTION value = "'SA'">All <%=LC.Std_Studies%></OPTION>*****--%>
   	      <OPTION value = "'SA'"><%=LC.L_All_Studies%></OPTION>
	   <%-- <OPTION value ="'S','SP','A','SA','PS','PR'" >Select an Option</OPTION>*****--%>
	       <OPTION value ="'S','SP','A','SA','PS','PR'" ><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>
	   <%}%>
	   
	   <% if(linkedto.equals("'S','SP','A','SA','PS','PR'")){%>
	   <td><SELECT NAME=linked>
	   <%-- <OPTION value = "'S'" ><%=LC.Std_Study%></OPTION>*****--%>
	        <OPTION value = "'S'" ><%=LC.L_Study%></OPTION>
	   <%--<OPTION value = "'SP'"><%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)</OPTION>*****--%>
	       <OPTION value = "'SP'"><%=LC.L_Pat_SpecStd%></OPTION>
	   <%--<OPTION value = "'PS'"><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)</OPTION>*****--%>
	       <OPTION value = "'PS'"><%=LC.L_Pat_AllStd%></OPTION>
	   <%--<OPTION value = "'PR'"><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)</OPTION>*****--%>
	       <OPTION value = "'PR'"><%=LC.L_Pat_AllStdRest%></OPTION>
	   <%--<OPTION value = "'A'">Account</OPTION>*****--%>  
	       <OPTION value = "'A'"><%=LC.L_Account%></OPTION>
	   <%--  <OPTION value = "'SA'">All <%=LC.Std_Studies%></OPTION>*****--%>
   	         <OPTION value = "'SA'"><%=LC.L_All_Studies%></OPTION>
	   <%-- <OPTION value ="'S','SP','A','SA','PS','PR'" SELECTED>Select an Option</OPTION>*****--%>
	       <OPTION value ="'S','SP','A','SA','PS','PR'" SELECTED><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>	
	<%	}	
		
	}
	else if ((String.valueOf(formLibAppRight).compareTo("1") == 0)&&(String.valueOf(patProAppRight).compareTo("1") == 0))
 	{						
   	  if(linkedto.equals("'S'")){%>
	   <td><SELECT NAME=linked>
	   <%-- <OPTION value = "'S'" SELECTED><%=LC.Std_Study%></OPTION>*****--%>
	        <OPTION value = "'S'" SELECTED><%=LC.L_Study%></OPTION>
	   <%--<OPTION value = "'SP'"><%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)</OPTION>*****--%>
	       <OPTION value = "'SP'"><%=LC.L_Pat_SpecStd%></OPTION>
	   <%--<OPTION value = "'PS'"><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)</OPTION>*****--%>
	       <OPTION value = "'PS'"><%=LC.L_Pat_AllStd%></OPTION>
	   <%--<OPTION value = "'PR'"><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)</OPTION>*****--%>
	       <OPTION value = "'PR'"><%=LC.L_Pat_AllStdRest%></OPTION>
	   <%--<OPTION value = "'A'">Account</OPTION>*****--%>  
	       <OPTION value = "'A'"><%=LC.L_Account%></OPTION>
	   <%--  <OPTION value = "'SA'">All <%=LC.Std_Studies%></OPTION>*****--%>
   	      <OPTION value = "'SA'"><%=LC.L_All_Studies%></OPTION>
		<%-- <OPTION value = "'PA'">All <%=LC.Pat_Patients%></OPTION>*****--%>
             <OPTION value = "'PA'"><%=LC.L_All_Patients%></OPTION>	  		 
	   <%--<OPTION value ="'S','SP','A','SA','PS','PR','PA'" >Select an Option</OPTION>*****--%>
	       <OPTION value ="'S','SP','A','SA','PS','PR','PA'" ><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>
	   <%}%>
	   <% if(linkedto.equals("'SP'")){%>
	   <td><SELECT NAME=linked>
	   <%-- <OPTION value = "'S'" ><%=LC.Std_Study%></OPTION>*****--%>
	        <OPTION value = "'S'" ><%=LC.L_Study%></OPTION>
	   <%--<OPTION value = "'SP'" SELECTED><%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)</OPTION>*****--%>
	       <OPTION value = "'SP'" SELECTED><%=LC.L_Pat_SpecStd%></OPTION>
	   <%--<OPTION value = "'PS'"><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)</OPTION>*****--%>
	       <OPTION value = "'PS'"><%=LC.L_Pat_AllStd%></OPTION>
	   <%--<OPTION value = "'PR'"><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)</OPTION>*****--%>
	       <OPTION value = "'PR'"><%=LC.L_Pat_AllStdRest%></OPTION>
	   <%--<OPTION value = "'A'">Account</OPTION>*****--%>  
	       <OPTION value = "'A'"><%=LC.L_Account%></OPTION>
	  <%--  <OPTION value = "'SA'">All <%=LC.Std_Studies%></OPTION>*****--%>
   	      <OPTION value = "'SA'"><%=LC.L_All_Studies%></OPTION>
        <%-- <OPTION value = "'PA'">All <%=LC.Pat_Patients%></OPTION>*****--%>
             <OPTION value = "'PA'"><%=LC.L_All_Patients%></OPTION>
	   <%--<OPTION value ="'S','SP','A','SA','PS','PR','PA'" >Select an Option</OPTION>*****--%>
	       <OPTION value ="'S','SP','A','SA','PS','PR','PA'" ><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>
	   <%}%>
	   <% if(linkedto.equals("'A'")){%>
	   <td><SELECT NAME=linked>
	     <%-- <OPTION value = "'S'" ><%=LC.Std_Study%></OPTION>*****--%>
	          <OPTION value = "'S'" ><%=LC.L_Study%></OPTION>
	   <%--<OPTION value = "'SP'"><%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)</OPTION>*****--%>
	       <OPTION value = "'SP'"><%=LC.L_Pat_SpecStd%></OPTION>
	   <%--<OPTION value = "'PS'"><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)</OPTION>*****--%>
	       <OPTION value = "'PS'"><%=LC.L_Pat_AllStd%></OPTION>
	   <%--<OPTION value = "'PR'"><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)</OPTION>*****--%>
	       <OPTION value = "'PR'"><%=LC.L_Pat_AllStdRest%></OPTION>
	   <%--<OPTION value = "'A'"SELECTED>Account</OPTION>*****--%>  
	       <OPTION value = "'A'"SELECTED><%=LC.L_Account%></OPTION>
	   <%--  <OPTION value = "'SA'">All <%=LC.Std_Studies%></OPTION>*****--%>
   	         <OPTION value = "'SA'"><%=LC.L_All_Studies%></OPTION>
        <%-- <OPTION value = "'PA'">All <%=LC.Pat_Patients%></OPTION>*****--%>
             <OPTION value = "'PA'"><%=LC.L_All_Patients%></OPTION>
	   <%--<OPTION value ="'S','SP','A','SA','PS','PR','PA'" >Select an Option</OPTION>*****--%>
	       <OPTION value ="'S','SP','A','SA','PS','PR','PA'" ><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>
	   <%}%>
	   <% if(linkedto.equals("'SA'")){%>
	   <td><SELECT NAME=linked>
	     <%-- <OPTION value = "'S'" ><%=LC.Std_Study%></OPTION>*****--%>
	          <OPTION value = "'S'" ><%=LC.L_Study%></OPTION>
	   <%--<OPTION value = "'SP'"><%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)</OPTION>*****--%>
	       <OPTION value = "'SP'"><%=LC.L_Pat_SpecStd%></OPTION>
	   <%--<OPTION value = "'PS'"><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)</OPTION>*****--%>
	       <OPTION value = "'PS'"><%=LC.L_Pat_AllStd%></OPTION>
	   <%--<OPTION value = "'PR'"><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)</OPTION>*****--%>
	       <OPTION value = "'PR'"><%=LC.L_Pat_AllStdRest%></OPTION>
	    <%--<OPTION value = "'A'">Account</OPTION>*****--%>  
	       <OPTION value = "'A'"><%=LC.L_Account%></OPTION>
	   <%--  <OPTION value = "'SA'" SELECTED>All <%=LC.Std_Studies%></OPTION>*****--%>
   	         <OPTION value = "'SA'" SELECTED><%=LC.L_All_Studies%></OPTION>
          <%-- <OPTION value = "'PA'">All <%=LC.Pat_Patients%></OPTION>*****--%>
              <OPTION value = "'PA'"><%=LC.L_All_Patients%></OPTION>
	   <%--<OPTION value ="'S','SP','A','SA','PS','PR','PA'" >Select an Option</OPTION>*****--%>
	       <OPTION value ="'S','SP','A','SA','PS','PR','PA'" ><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>
	   <%}%>
	   <% if(linkedto.equals("'PA'")){%>
	   <td><SELECT NAME=linked>
	     <%-- <OPTION value = "'S'" ><%=LC.Std_Study%></OPTION>*****--%>
	          <OPTION value = "'S'" ><%=LC.L_Study%></OPTION>
	   <%--<OPTION value = "'SP'"><%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)</OPTION>*****--%>
	       <OPTION value = "'SP'"><%=LC.L_Pat_SpecStd%></OPTION>
	   <%--<OPTION value = "'PS'"SELECTED><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)</OPTION>*****--%>
	       <OPTION value = "'PS'"SELECTED><%=LC.L_Pat_AllStd%></OPTION>
	   <%--<OPTION value = "'PR'"><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)</OPTION>*****--%>
	       <OPTION value = "'PR'"><%=LC.L_Pat_AllStdRest%></OPTION>
	   <%--<OPTION value = "'A'">Account</OPTION>*****--%>  
	       <OPTION value = "'A'"><%=LC.L_Account%></OPTION>
	   <%--  <OPTION value = "'SA'">All <%=LC.Std_Studies%></OPTION>*****--%>
   	      <OPTION value = "'SA'"><%=LC.L_All_Studies%></OPTION>
    <%-- <OPTION value = "'PA'" Selected>All <%=LC.Pat_Patients%></OPTION>*****--%>
         <OPTION value = "'PA'" Selected><%=LC.L_All_Patients%></OPTION>	  		   
	   <%--<OPTION value ="'S','SP','A','SA','PS','PR','PA'" >Select an Option</OPTION>*****--%>
	       <OPTION value ="'S','SP','A','SA','PS','PR','PA'" ><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>
	   <%}%>
	   <% if(linkedto.equals("'PS'")){%>
	   <td><SELECT NAME=linked>
	     <%-- <OPTION value = "'S'" ><%=LC.Std_Study%></OPTION>*****--%>
	        <OPTION value = "'S'" ><%=LC.L_Study%></OPTION>
	   <%--<OPTION value = "'SP'"><%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)</OPTION>*****--%>
	       <OPTION value = "'SP'"><%=LC.L_Pat_SpecStd%></OPTION>
	   <%--<OPTION value = "'PS'"SELECTED><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)</OPTION>*****--%>
	       <OPTION value = "'PS'"SELECTED><%=LC.L_Pat_AllStd%></OPTION>
	   <%--<OPTION value = "'PR'"><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)</OPTION>*****--%>
	       <OPTION value = "'PR'"><%=LC.L_Pat_AllStdRest%></OPTION>
	   <%--<OPTION value = "'A'">Account</OPTION>*****--%>  
	       <OPTION value = "'A'"><%=LC.L_Account%></OPTION>
	   <%--  <OPTION value = "'SA'">All <%=LC.Std_Studies%></OPTION>*****--%>
   	      <OPTION value = "'SA'"><%=LC.L_All_Studies%></OPTION>
        <%-- <OPTION value = "'PA'">All <%=LC.Pat_Patients%></OPTION>*****--%>
             <OPTION value = "'PA'"><%=LC.L_All_Patients%></OPTION>	
	   <%--<OPTION value ="'S','SP','A','SA','PS','PR','PA'" >Select an Option</OPTION>*****--%>
	       <OPTION value ="'S','SP','A','SA','PS','PR','PA'" ><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>
	   <%}%>
   <% if(linkedto.equals("'PR'")){%>
	   <td><SELECT NAME=linked>
	     <%-- <OPTION value = "'S'" ><%=LC.Std_Study%></OPTION>*****--%>
	        <OPTION value = "'S'" ><%=LC.L_Study%></OPTION>
	   <%--<OPTION value = "'SP'"><%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)</OPTION>*****--%>
	       <OPTION value = "'SP'"><%=LC.L_Pat_SpecStd%></OPTION>
	   <%--<OPTION value = "'PS'"><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)</OPTION>*****--%>
	       <OPTION value = "'PS'"><%=LC.L_Pat_AllStd%></OPTION>
	   <%--<OPTION value = "'PR'"SELECTED><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)</OPTION>*****--%>
	       <OPTION value = "'PR'"SELECTED><%=LC.L_Pat_AllStdRest%></OPTION>
	   <%--<OPTION value = "'A'">Account</OPTION>*****--%>  
	       <OPTION value = "'A'"><%=LC.L_Account%></OPTION>
	   <%--  <OPTION value = "'SA'">All <%=LC.Std_Studies%></OPTION>*****--%>
   	      <OPTION value = "'SA'"><%=LC.L_All_Studies%></OPTION>
  <%-- <OPTION value = "'PA'">All <%=LC.Pat_Patients%></OPTION>*****--%>
       <OPTION value = "'PA'"><%=LC.L_All_Patients%></OPTION>
	   <%--<OPTION value ="'S','SP','A','SA','PS','PR','PA'" >Select an Option</OPTION>*****--%>
	       <OPTION value ="'S','SP','A','SA','PS','PR','PA'" ><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>
	   <%}%>	   
	   <% if(linkedto.equals("'S','SP','A','SA','PS','PR','PA'")){%>
	   <td><SELECT NAME=linked>
	     <%-- <OPTION value = "'S'" ><%=LC.Std_Study%></OPTION>*****--%>
	        <OPTION value = "'S'" ><%=LC.L_Study%></OPTION>
	   <%--<OPTION value = "'SP'"><%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)</OPTION>*****--%>
	       <OPTION value = "'SP'"><%=LC.L_Pat_SpecStd%></OPTION>
	   <%--<OPTION value = "'PS'"><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)</OPTION>*****--%>
	       <OPTION value = "'PS'"><%=LC.L_Pat_AllStd%></OPTION>
	   <%--<OPTION value = "'PR'"><%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)</OPTION>*****--%>
	       <OPTION value = "'PR'"><%=LC.L_Pat_AllStdRest%></OPTION>
	   <%--<OPTION value = "'A'">Account</OPTION>*****--%>  
	       <OPTION value = "'A'"><%=LC.L_Account%></OPTION>
	   <%--  <OPTION value = "'SA'">All <%=LC.Std_Studies%></OPTION>*****--%>
   	      <OPTION value = "'SA'"><%=LC.L_All_Studies%></OPTION>
   	 <%-- <OPTION value = "'PA'">All <%=LC.Pat_Patients%></OPTION>*****--%>
       <OPTION value = "'PA'"><%=LC.L_All_Patients%></OPTION>
	   <%--<OPTION value ="'S','SP','A','SA','PS','PR','PA'" SELECTED>Select an Option</OPTION>*****--%>
	       <OPTION value ="'S','SP','A','SA','PS','PR','PA'" SELECTED><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>	
	<%	}	
		
	}%>

	</tr>
		  	
	   		<tr >
		 <%--<td class=tdDefault > <%=LC.L_Group%>:</td> *****--%>
	   		 <td > <%=LC.L_Group%>:</td>  
		   	<td><%=dAccGroups%></td>
	   		<td align="right"> <%--Organization*****--%> <%=LC.L_Organization%>:&nbsp;</td>
	   		<td colspan="2"><%=dAccSites%>
			</td>
		   	<td align="center">
			<button type="submit"><%=LC.L_Search%></button>
	        </td>
	        </tr>	 	
  </table>
  	
  	<div class="defComments" style="padding-left: 0px;"><%Object[] arguments1 = {ftxt,stxt,ltxt,otxt,gtxt}; %>
	    &nbsp;&nbsp;&nbsp;<%=VelosResourceBundle.getMessageString("M_SelcFilters_FrmNameStd",arguments1)%><%--Selected Filters are: Form Name:&nbsp;<B><I><u><%=ftxt%></u></I></B>
		&nbsp;&nbsp;Study:&nbsp;<B><I><u><%=stxt%></u></I></B>&nbsp;&nbsp;Linked To:&nbsp;<B><I><u><%=ltxt%></u></I></B>
		Organization:&nbsp;<B><I><u><%=otxt%></u></I></B>
		&nbsp;&nbsp;Group:&nbsp;<B><I><u><%=gtxt%></u></I></B></div> *****--%>
			
	<input type="hidden" name="ftxt" >
	<input type="hidden" name="stxt" >
	<input type="hidden" name="ltxt" >
	<input type="hidden" name="otxt" >
	<input type="hidden" name="gtxt" >


	<%
		if (totalRows<=0){
	%>
	<P class="defComments"><%--No Forms associated found for your search criteria.Please modify the criteria and click on 'Search'.*****--%><%=MC.M_NoFrmFound_InSrchCrit%></P>
		<table width="95%" cellspacing="0" cellpadding="0" border=0 >
		<td align="right">
	<p> <A href=# onClick="return  openwin2(<%=account_form_rights%>)"><%--Select a Form from your Library*****--%><%=MC.M_SelFrm_FromLib%></A> </p>
		<td>
	<% }
	else{
	if (rowsReturned==0){
	%>
	<P class="defComments"><%--No Forms associated found for your search criteria. Please modify the criteria and click on 'Search'.*****--%><%=MC.M_NoFrmFound_InSrchCrit%></P>
	

	<p> <A href=# onClick="return  openwin2(<%=account_form_rights%>)"><%--Select a Form from your Library*****--%><%=MC.M_SelFrm_FromLib%></A> </p>
	<% }
	else{
	
	}
	%>
	
	
<div class="tmpHeight"></div>
	  <div class="tmpHeight"></div>	
<table width="100%" cellspacing="0" cellpadding="0" border=0 >
   
<tr > 
      <td width = "15%"> 
        <P class = "defComments">&nbsp;<%--Currently associated forms are *****--%><%=MC.M_Cur_AssocFrm%>:</P>
      </td>
<td width = "20%">
<%if (len > 0) {%>
	<A href="#" onClick="return openwin3(<%=account_form_rights%>)"><%--COPY AN EXISTING FORM*****--%><%=MC.M_Copy_ExistingFrm_Upper%></A>
	<%}%>
</td>

	   <td width = "25%">
	   	<%if (len > 0) {%>
        <p> <A href=# onClick="openhideseq(<%=account_form_rights%>)"><%--DISPLAY AND SEQUENCING OPTIONS*****--%><%=MC.M_Disp_SeqOpts_Upper%></A> </p>
		<%}%>
      </td>	  	  
      <td width = "25%" align="right">
        <p> <A href=# onClick="return  openwin2(<%=account_form_rights%>)"><%--SELECT A FORM FROM YOUR LIBRARY*****--%><%=MC.M_Selc_FrmYourLib_Upper%></A>&nbsp;&nbsp; </p>
      </td>
    </tr>
    </table>

	<table class="outline midAlign" width="99%" cellpadding="0" cellspacing="0">
       <tr> 
        	<th width="20%" onClick="setOrder(document.accountformsearch,15)" align =center> <%--Name*****--%><%=LC.L_Name%> &loz;</th>
	        <th width="20%"  align =center> <%--Description*****--%><%=LC.L_Description%></th>
	        <th width="10%"  align =center> <%--Linked To*****--%><%=LC.L_Linked_To%></th>
		<th width="10%" align=center><%--<%=LC.Std_Study%>*****--%><%=LC.L_Study%></th>
	        <th width="15%"  align =center><%--Form Status*****--%><%=LC.L_Form_Status%> </th>
	        <th width="5%"> <%--Preview*****--%><%=LC.L_Preview%></th>			
	        <th width="5%"> <%--Delete*****--%><%=LC.L_Delete%></th>
		<th width="4%"> <%--Info*****--%><%=LC.L_Info%></th>
		<th width="7%"><%--Save to Library*****--%><%=LC.L_Save_ToLib%></th><!--KM-->			
      </tr>
      
	<Input type="hidden" name="page" value="<%=curPage%>">
	<Input type="hidden" name="orderBy" value="<%=orderBy%>">
	<Input type="hidden" name="orderType" value="<%=orderType%>">
	
	   
	   <%
	   	
		String formname="";
		String formdescription="";
		String formstatus="";
		String formstudyno="";//km
		String formlinkedto="";
		String formlkdfrom="";				
		String linkedToHref = "";
		String lfDisplayType = "";
		int stdFrmRight=0;

		String userIdFromSession = (String) tSession.getValue("userId");
		ArrayList tId ;
		tId = new ArrayList();
		int formStatusInt = 0;
		

		int counter=0;
	    for(counter=1;counter<=rowsReturned;counter++)
		{	
		formId=br.getBValues(counter,"PK_FORMLIB");
		studyId=br.getBValues(counter,"formstudy");
		formStatId = br.getBValues(counter,"formStatId"); //changed by sonia, the column name was incorrect
		
		
		statCodeId=br.getBValues(counter,"statCodeId");
		formStatusInt = EJBUtil.stringToNum(statCodeId);
		
		lfDataCnt = br.getBValues(counter,"lfDataCnt");
		formlkdfrom = br.getBValues(counter,"linkfrom");
		
		formname=((br.getBValues(counter,"FORM_NAME") == null)?"-":br.getBValues(counter,"FORM_NAME"));
		
		formname=StringUtil.escapeSpecialCharHTML(formname);
			
		formdescription=((br.getBValues(counter,"FORM_DESC") == null)?"-": br.getBValues(counter,"FORM_DESC"));
		
		formstudyno=((br.getBValues(counter,"STUDY_NUMBER")==null)?"":br.getBValues(counter,"STUDY_NUMBER"));//km
		
		formstatus=((br.getBValues(counter,"form_status")==null)?"-": br.getBValues(counter,"form_status"));
		lfPk =br.getBValues(counter,"PK_LF");
		formlinkedto=((br.getBValues(counter,"displaytype") == null)? "-" :br.getBValues(counter,"displaytype"));
		lfDisplayType = br.getBValues(counter,"lf_displaytype");
		int studFormRight= 0;
		
		ArrayList teamId = new ArrayList();
		if((lfDisplayType.indexOf("S")==0)){
		if(lfDisplayType.trim().equals("S") || lfDisplayType.trim().equals("SP")){
			TeamDao teamDao = new TeamDao();
			teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));
			teamId = teamDao.getTeamIds();
			if (teamId.size() <=0){
				studFormRight  = 0;
				}
			else {
				stdRightsB.setId(EJBUtil.stringToNum(teamId.get(0).toString()));
			 	ArrayList teamRights ;
				teamRights = new ArrayList();
				teamRights = teamDao.getTeamRights();

				stdRightsB.setSuperRightsStringForStudy((String)teamRights.get(0));
				stdRightsB.loadStudyRights();

				if ((stdRightsB.getFtrRights().size()) == 0){
					studFormRight= 0;
				}else{
					studFormRight = Integer.parseInt(stdRightsB.getFtrRightsByValue("STUDYFRMMANAG"));
					}
				}	
			}
		else{
			studFormRight = -1;
		    }
		}
		else
		{
			studFormRight = -1;
		}
// Added by Gopu for Nov.2005 Enhancement #4
			String creator="";
			String createdon="";
			String modifiedby="";
			String modifiedon="";
			creator=  br.getBValues(counter,"creator");			
			creator = (  creator ==null )?"-":( creator) ;				

			createdon=  br.getBValues(counter,"created_on");
			createdon = (  createdon ==null )?"-":( createdon) ;	
			
			modifiedby=  br.getBValues(counter,"last_modified_by");
			modifiedby = (  modifiedby ==null )?"-":( modifiedby) ;	
			
			modifiedon=  br.getBValues(counter,"last_modified_date");
			modifiedon = (  modifiedon ==null )?"-":( modifiedon) ;	

		if ( !  formlinkedto.equals("-") )
		{
			linkedToHref="<A href=# onClick=\"openwin4("+lfPk+","+activeId+","+statCodeId+")\" >" + formlinkedto+"</A>" ;

		}
		else 
		{
			linkedToHref = formlinkedto ;
		}

		
		if ((counter%2)==0) {   %>

      <tr class="browserEvenRow"> 
        <%

		} else {

  %>
      <tr class="browserOddRow"> 
  <%

		}


  %>	
        <td><A onClick = "return isNotMigrateStatus(<%=migrateStatPK%>, <%=formStatusInt%>)" href="formcreation.jsp?formLibId=<%=formId%>&mode=M&srcmenu=tdmenubaritem4&calledFrom=A&selectedTab=1&lnkFrom=<%=formlkdfrom%>&studyId=<%=studyId%>&lfPk=<%=lfPk%>"> 
		<%=formname%></A></td>
	   	<td><%=formdescription%></td>   
		<td><%=linkedToHref%></td>
		<td><%=formstudyno%></td>
        <td class="trheight1">
        	<p><A onClick = "return isNotMigrateStatus(<%=migrateStatPK%>, <%=formStatusInt%>)" href="formStatus.jsp?srcmenu=tdMenuBarItem1&formStatId=<%=formStatId%>&formId=<%=formId%>&from=A&lnkfrom=<%=formlkdfrom%>&studyId=<%=studyId%>"><%=formstatus%></A></p>
        	<p><A href=# onClick=openwin1(<%=formId%>,<%=lfPk%>)><%--H*****--%>
        <img border="0" title="<%=LC.L_History%>" alt="<%=LC.L_History%>" src="./images/History.gif" /><%//=LC.L_H%></A></p>
        </td>
		<td  align="center"><A href="#" onClick="openFormPreview(<%=formId%>)"><img src="../images/jpg/preview.gif" title="<%=LC.L_Preview%>" border="0"></A></td>
		<td  align="center">
			<% if (formStatusInt != migrateStatPK) { %>
		<A onClick="return confirmBox('<%=StringUtil.escapeSpecialCharJS(formname)%>','<%=lfDataCnt%>','<%=account_form_rights%>','<%=studFormRight%>')" href="formdelete.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&formId=<%=lfPk%>&delMode=null&from=A&lnkfrom=<%=formlkdfrom%>&studyId=<%=studyId%>&formStatusInt=<%=formStatusInt%>" ><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0"/></A>
		   <% } %>
		
		</td>
		<td  align="center">
		<%--<A><img src="../images/jpg/help.jpg" border="0" align="left" 
		onmouseover="return overlib('<%="<tr><td><font size=2><b>Creator : </b></font><font size=1>"+StringUtil.escapeSpecialCharJS(creator)+"</font></td></tr><tr><td><font size=2><b>Created On: </b></font><font size=1>"+createdon+"</font></td></tr><tr><td><font size=2><b>Last Modified By: </b></font><font size=1>"+StringUtil.escapeSpecialCharJS(modifiedby)+"</font></td></tr><tr><td><font size=2><b>Last Modified On : </b></font><font size=1>"+modifiedon+"</font></td></tr>"%>',CAPTION,'FORM',RIGHT,ABOVE)"; onmouseout="return nd();"></A>*****--%>
		<A><img src="../images/jpg/help.jpg" border="0"
		onmouseover="return overlib('<%="<tr><td><font size=2><b>"+LC.L_Creator+" : </b></font><font size=1>"+StringUtil.escapeSpecialCharJS(creator)+"</font></td></tr><tr><td><font size=2><b>"+LC.L_Created_On+": </b></font><font size=1>"+createdon+"</font></td></tr><tr><td><font size=2><b>"+LC.L_Last_ModifiedBy+": </b></font><font size=1>"+StringUtil.escapeSpecialCharJS(modifiedby)+"</font></td></tr><tr><td><font size=2><b>"+LC.L_Last_ModifiedOn +": </b></font><font size=1>"+modifiedon+"</font></td></tr>"%>',CAPTION,'<%=LC.L_Form_Upper%>',RIGHT,ABOVE)"; onmouseout="return nd();"></A>
		</td>
		<!--Modified By Amarnadh for issue #3219 -->
		<!--Added by Manimaran for Enhancement F4 -->
		<td  align="center"> <A href=# onClick="return  saveLib('formtolibrary.jsp?formLibId=<%=formId%>&formStatusInt=<%=formStatusInt%>&amp;name=<%=StringUtil.encodeString(formname)%>', <%=formLibRights%>,<%=migrateStatPK%>)">  <!--KM -21oct08 -->
		 <img src="./images/save.gif" alt="<%=LC.L_Save_ToLib %><%-- Save To Library*****--%>" border="0" title="<%=LC.L_Save_ToLib%>"></A></td>
      </tr>
 
	  
 
      <%
		}// end of for loop 
	}// end of if part if total rows>0
	  %>
    </table>
   	<!-- Bug#9751 16-May-2012 Ankit -->
	<table width="98%" cellspacing="0" cellpadding="0" class="midalign">
	<tr>
	<td>
	<% if (totalRows > 0) { Object[] arguments = {firstRec,lastRec,totalRows}; %>
		<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%--<font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
	<%} %>
	</td>
	</tr>
	</table>
	
	<div align="center" class="midalign">

	<%
	if (curPage==1) startPage=1;

    for (int count = 1; count <= showPages;count++)
	{
     cntr = (startPage - 1) + count;
	 
	if ((count == 1) && (hasPrevious))
	{   
    %>
  	<A href="accountforms.jsp?formname=<%=frmname%>&ByStudy=<%=selstudyId%>&linked=<%=linkedto%>&ddorg=<%=siteId%>&ddgrp=<%=groupId%>&srcmenu=<%=src%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&ftxt=<%=ftxt%>&stxt=<%=stxt%>&ltxt=<%=ltxt%>&otxt=<%=otxt%>&gtxt=<%=gtxt%>">< <%--Previous*****--%><%=LC.L_Previous%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;
	<%
  	}	

	 if (curPage  == cntr)
	 {
     %>	   
		<FONT class = "pageNumber"><%= cntr %></Font>
       <%
       }
      else
        {
       %>		
		
	   <A href="accountforms.jsp?formname=<%=frmname%>&ByStudy=<%=selstudyId%>&linked=<%=linkedto%>&ddorg=<%=siteId%>&ddgrp=<%=groupId%>&srcmenu=<%=src%>&page=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&ftxt=<%=ftxt%>&stxt=<%=stxt%>&ltxt=<%=ltxt%>&otxt=<%=otxt%>&gtxt=<%=gtxt%>"><%= cntr%></A>
       <%
    	}	
	  }
 
	if (hasMore)
	{   
   %>
  	&nbsp;&nbsp;&nbsp;&nbsp;<A href="accountforms.jsp?formname=<%=frmname%>&ByStudy=<%=selstudyId%>&linked=<%=linkedto%>&ddorg=<%=siteId%>&ddgrp=<%=groupId%>&srcmenu=<%=src%>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&ftxt=<%=ftxt%>&stxt=<%=stxt%>&ltxt=<%=ltxt%>&otxt=<%=otxt%>&gtxt=<%=gtxt%>">< <%--Next*****--%><%=LC.L_Next%> <%=totalPages%>></A>
	<%
  	}	
	%>
	</div>
  </Form>
  

	 
	<%
	
	}// end of page right if 
	else
	{// begin of page right else
	 %>
     <jsp:include page="accessdenied.jsp" flush="true"/>
    <%
	
	}// end of page right else 
	
	
	
	
	} //end of if session times out

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

} //end of else body for session time out 

%>
<br>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
      </div>
</div>
<div class ="mainMenu" id="emenu"> 
  	<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>
</html>
