<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=MC.M_EvtLib_EvtApdx%><%--Event Library >> Event Appendix*****--%> </title>
<!-- Akshi:Added for bug #6948 -->
<%-- Commented by Yogendra Pratap Singh : Bug# 6927 and 6947--%>
<%-- <link type="text/css" href="./styles/bethematch/common.css" rel="STYLESHEET">
<link type="text/css" href="./styles/ns_gt1024.css" rel="STYLESHEET">--%>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>






 

<% if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt"))

{ %>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>	
<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>
	
 <% } //end for if ((request.getParameter("fromPage")).equals("selectEvent"))

	

	%>	



<SCRIPT language="javascript">



function confirmBox(fileName,pgRight) {

	if (f_check_perm(pgRight,'E') == true) {
		var paramArray = [fileName];
		if (confirm(getLocalizedMessageString("L_Del_FromEvt",paramArray))) {/*if (confirm("Delete " + fileName + " from Events?")) {*****/

		    return true;}

		else

		{

			return false;

		}

	} else {

		return false;

	}

}

function fdownload(formobj,pk,filename,dnldurl)
{
	formobj.file.value = filename;
	formobj.pkValue.value = pk ;
	formobj.dnldurl.value=dnldurl;
	formobj.moduleName.value="Event";
	formobj.action="postFileDownload.jsp";
	//formobj.action=dnldurl;
	
	formobj.target = "_filedownload";
	formobj.method = "POST";
	formobj.submit();
}

</SCRIPT> 



</head>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.esch.business.common.EventInfoDao,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB,com.aithent.file.uploadDownload.*"%>

<jsp:useBean id="codelstB" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>


<% String src;



src= request.getParameter("srcmenu");

//SV, commented on 10/28/04, changes: eventname, propagation flags
String eventName = request.getParameter("eventName");
eventName = StringUtil.encodeString(eventName);

String calAssoc = request.getParameter("calassoc");
calAssoc = (calAssoc == null) ? "" : calAssoc;

%>


<%-- Modified by Yogendra Pratap Singh : Bug# 6927 and 6947 --%>
<% if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){%>
<jsp:include page="include.jsp" flush="true"/>
<%}else{

%>

<jsp:include page="panel.jsp" flush="true"> 



<jsp:param name="src" value="<%=src%>"/>



</jsp:include>   

<%}%>



<body id="forms">

<% if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){%>

		<DIV class="popDefault" id="div1"> 

	<%	} 

else { %>

<DIV class="browserDefault" id="div1"> 

<%}%>	

<%

	int pageRight = 0;

	String duration = request.getParameter("duration");   

	String protocolId = request.getParameter("protocolId");   

	String calledFrom = request.getParameter("calledFrom");   

	String eventId = request.getParameter("eventId");

	String mode = request.getParameter("mode");

	String selectedTab=request.getParameter("selectedTab");

	String fromPage = request.getParameter("fromPage");

	String calStatus = request.getParameter("calStatus");

	String eventmode = request.getParameter("eventmode");

	String displayDur=request.getParameter("displayDur");

	String displayType=request.getParameter("displayType");

    String m="";
	String incorrectFile = "";	

	HttpSession tSession = request.getSession(true); 


	if (sessionmaint.isValidSession(tSession))

	{
	String space = request.getParameter("outOfSpace");

	space = (space==null)?"":space.toString();
	incorrectFile = (incorrectFile==null)?"":incorrectFile.toString();	

	if(space.equals("1")) {

%>

<br><br><br><br>

<table width=100%>

<tr>

<td align=center>



<p class = "sectionHeadings">



<%=MC.M_UploadSpace_ContAdmin%><%--The space allotted to you for file uploading has exhausted. Please contact Velos Administrator.*****--%> 



</p>

</td>

</tr>



<tr height=20></tr>

<tr>

<td align=center>



		<button onclick="window.history.go(-2);"><%=LC.L_Back%></button>

</td>		

</tr>		

</table>		





<%		

	} else { //else of if for space.equals("1")

////////////////////////

	if(incorrectFile.equals("1")) {

%>

<br><br><br><br>

<table width=100%>

<tr>

<td align=center>



<p class = "sectionHeadings">



<%=MC.M_UploadingFilesNotExst_Chk%><%--Either the file specified for uploading does not exist or the file is empty. Please check the file and path and try again.*****--%> 



</p>

</td>

</tr>



<tr height=20></tr>

<tr>

<td align=center>



		<button onClick="window.history.go(-1);"><%=LC.L_Back%></button>

</td>		

</tr>		

</table>		





<%		

	} else { //else of if for incorrectFile.equals("1")





		if (calledFrom.equals("S")) {

   	   	   StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");	 

	  	   if ((stdRights.getFtrRights().size()) == 0){

			 	pageRight= 0;

		   }else{

				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));

    	   }	
		   
		   

		}else if (calledFrom.equals("P")) {

				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		

				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
		}

		else if(calledFrom.equals("L")){

				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		

				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("EVLIB"));
		
		}

	

	   int count = 0;


	   EventInfoDao eventinfoDao = eventdefB.getEventDefInfo(com.velos.eres.service.util.EJBUtil.stringToNum(eventId));

	   ArrayList docIds= eventinfoDao.getDocIds();

	   ArrayList docNames= eventinfoDao.getDocNames();

	   ArrayList docTypes= eventinfoDao.getDocTypes();

	   ArrayList docDescs= eventinfoDao.getDocDescs();

	   String docId="";

	   String docName="";

   	   String docType="";

   	   String docDesc="";	


%>



<%
	String calName = "";

	if (fromPage.equals("eventbrowser") || fromPage.equals("eventlibrary") || fromPage.equals("patientschedule") )
	{
%>
	<!--P class="sectionHeadings"> Protocol Calendar >> Event Appendix </P-->
<%
	}
	else
	{
		calName = (String) tSession.getValue("protocolname");
%>
	<P class="sectionHeadings"> <%Object[] arguments = {calName}; %>
	    <%=VelosResourceBundle.getMessageString("M_PcolCal_EvtApdx",arguments)%><%--Protocol Calendar [ {0} ] >> Event Appendix*****--%>  </P>
<%
	}
%>

<jsp:include page="eventtabs.jsp" flush="true"> 

<jsp:param name="duration" value="<%=duration%>"/>

<jsp:param name="protocolId" value="<%=protocolId%>"/>

<jsp:param name="calledFrom" value="<%=calledFrom%>"/>

<jsp:param name="fromPage" value="<%=fromPage%>"/>

<jsp:param name="mode" value="<%=mode%>"/>

<jsp:param name="calStatus" value="<%=calStatus%>"/>

<jsp:param name="displayDur" value="<%=displayDur%>"/>

<jsp:param name="displayType" value="<%=displayType%>"/>

<jsp:param name="eventId" value="<%=eventId%>"/>

<jsp:param name="src" value="<%=src%>"/>

<jsp:param name="eventName" value="<%=eventName%>"/>

<jsp:param name="calassoc" value="<%=calAssoc%>"/>


  </jsp:include>



<%


 if(eventId == "" || eventId == null || eventId.equals("null")  || eventId.equals("")) {

	%>

	  <jsp:include page="eventDoesNotExist.jsp" flush="true"/>

  <%

	}else {

%>



<%



if(fromPage.equals("eventbrowser")) {

%>



<form name="eventbrowser" METHOD=POST action="eventbrowser.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calassoc=<%=calAssoc%>">



<%

} else {

%>

<form form name="eventbrowser" METHOD=POST action="fetchProt.jsp?protocolId=<%=protocolId%>&srcmenu=<%=src%>&selectedTab=5&mode=<%=mode%>&calledFrom=<%=calledFrom%>&calStatus=<%=calStatus%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>&calassoc=<%=calAssoc%>">



<%

}

%>





<%         

//if ((calStatus.equals("A")) || (calStatus.equals("F"))){%>

         <!--  <P class = "defComments"><FONT class="Mandatory">Changes cannot be made to Event Interval and Event Details for an Active/Frozen Protocol</Font></P> -->

<%//}



if (pageRight == 4) {

%>

   <P class = "defComments"><FONT class="Mandatory"><%=MC.M_OnlyViewPerm_ForEvt%><%--You have only View permission for the Event.*****--%></Font></P>

<%}%>





<TABLE width="90%">

   <tr>

	<td colspan=3> <P class = "defComments"><%=MC.M_LnkAssoc_WithEvt%><%--Links associated with this event*****--%>: <br> </P></td>

	<td width=15% align=left> <P class = "defComments">

	<%         

	/*if ((calStatus.equals("A")) || (calStatus.equals("F")))

	{

	}else

		{*/
		
	if (fromPage.equals("eventlibrary")){
	  m="N"; 
	}
	else
	{
	if (mode.equals("N")){m="N";}else{m="E";}
	}
	 %>	

	 <A href="addeventurl.jsp?docmode=N&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&selectedTab=4&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>&calassoc=<%=calAssoc%>" onClick="return f_check_perm(<%=pageRight%>,'<%=m%>')"><%=LC.L_Link_Url%><%--Link URL*****--%></A>

	<%

	//}

	%> 

	 </P></td>

   </tr>

<% 

   for(int i=0;i<docIds.size(); i++) {



	docId= (String)docIds.get(i);

	docDesc= (String)docDescs.get(i); 

	docName = (String)docNames.get(i); 

	docType = (String)docTypes.get(i); 

	if(docType.equals("F"))

	continue;





		if ((i%2)==0) {



  %>

      <tr class="browserEvenRow"> 

        <%



		}



		else{



  %>

      <tr class="browserOddRow"> 

        <%



		}



  %>

	<td><A href=<%=docName%> target="_new"><%=docName%></A></td>

	<td> <%=docDesc%> </td>

	<td width=15%>

       


	<A HREF="addeventurl.jsp?docmode=M&docId=<%=docId%>&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&selectedTab=4&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>&calassoc=<%=calAssoc%>" onClick="return f_check_perm(<%=pageRight%>,'E')"><img src="../jsp/images/edit.gif" style="border-style: none;" title="<%=LC.L_Edit%>"/></A>


 </td>

	<td width=15%>

	<A HREF="urlsave.jsp?docType=U&docId=<%= docId%>&docmode=D&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>&calassoc=<%=calAssoc%>" onClick="return confirmBox('<%=docName%>',<%=pageRight%>)"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" align="left"/></A>


	</td>

   </tr>

<%

   }

%>

</TABLE>



<BR>

<TABLE width="90%">

   <tr>

	<td colspan=3> <P class = "defComments"><%=MC.M_DocuLnk_ToThisEvt%><%--Documents linked to this event*****--%>:<br> </P></td>

	<td width=15% align=left> <P class = "defComments">

<%         

	/*if ((calStatus.equals("A")) || (calStatus.equals("F")))

	{

	}else

		{*/%>

	<!--KM-#3207 -->

	<A href="addeventfile.jsp?docmode=N&eventId=<%=eventId%>&srcmenu=<%=src%>&selectedTab=4&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=M&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>&calassoc=<%=calAssoc%>" onClick="return f_check_perm(<%=pageRight%>,'<%=m%>')"><%=LC.L_Upload_Docu%><%--Upload Document*****--%></A>

	<%

	//}

	%> 

	

	</P></td>

   </tr>

<% 
	String dnld;
	com.aithent.file.uploadDownload.Configuration conf = new com.aithent.file.uploadDownload.Configuration();

	conf.readSettings("sch");
	conf.readUploadDownloadParam(conf.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "eventapndx");
	dnld=conf.DOWNLOADSERVLET ;
	String modDnld = "";
	
	
   for(int i=0;i<docIds.size(); i++) {
	docId=   (String)docIds.get(i);
	docDesc= (String)docDescs.get(i); 
	docName = (String)docNames.get(i); 
	docType = (String)docTypes.get(i); 

	if(docType.equals("U"))
	continue;

		if ((i%2)==0) {
  %>
      <tr class="browserEvenRow"> 
        <%
		} else{
  %>
      <tr class="browserOddRow"> 
        <%
		}
modDnld = dnld + "?file=" + StringUtil.encodeString(docName) ;		
//out.println(StringUtil.encodeString(modDnld));
  %>

	<td>
	
	<A href="#" onClick="fdownload(document.eventbrowser,<%=docId%>,'<%=docName%>','<%=dnld%>');return false;" >
	
 	 <%=docName%></A> </td>

	<td> <%=docDesc%> </td>

	<td width=15%> 


	<A HREF="editeventfile.jsp?docType=F&docId=<%=docId%>&docmode=M&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>&calassoc=<%=calAssoc%>" onClick="return f_check_perm(<%=pageRight%>,'E')"><img src="../jsp/images/edit.gif" style="border-style: none;" title="<%=LC.L_Edit%>"/><A>

	</td>

	<td width=15%>



	<A HREF="urlsave.jsp?docType=F&docId=<%=docId%>&docmode=D&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>&calassoc=<%=calAssoc%>" onClick="return confirmBox('<%=docName%>',<%=pageRight%>)"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" align="left"/></A>


	</td>

   </tr>

<%

   }

%>

</TABLE>



<br>

<table>

<tr>

  <td> 

    <%

  	if (fromPage.equals("selectEvent")) {

         %>   

		<A type="submit" href="selecteventus.jsp?fromPage=NewEvent"><%=LC.L_Back%></A>

 <%

 	}

 %> 

  <%

  	if (fromPage.equals("eventbrowser")) {

  %>

		<A type="submit" href="eventbrowser.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%>"><%=LC.L_Back%></A>

 <%

 	}

  	if (fromPage.equals("eventlibrary")) {

 %>

		<A type="submit" href="eventlibrary.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%>"><%=LC.L_Back%></A>

 <%

 	}

  	//if (fromPage.equals("fetchProt")) {

  %>

		<!--<A href="fetchProt.jsp?srcmenu=<%=src%>&selectedTab=4&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&pageNo=1&displayType=<%=displayType%>&displayDur=<%=displayDur%>"  type="submit"><%=LC.L_Back%></A>-->

		<!-- <A href="fetchProt.jsp?srcmenu=<%=src%>&selectedTab=4&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&pageNo=1&displayType=<%=displayType%>&displayDur=<%=displayDur%>" >Back to Protocol Calendar : Customize Event Details</A>	 -->																			

 <%

 	//}

 %>



 </td>  

</tr>

</table>


	 
	
	<input type="hidden" name="tableName" value="SCH_DOCS">
    <input type="hidden" name="columnName" value="DOC">
    <input type="hidden" name="pkColumnName" value="PK_DOCS">
    <input type="hidden" name="module" value="eventapndx">
    <input type="hidden" name="db" value="sch">
    <input type="hidden" name="pkValue" value="">
    <input type="hidden" name="file" value="">
    <input type="hidden" name="dnldurl" value="">
    <input type="hidden" name="moduleName" value="">

</form>



<%



	} //event
} // end of if for incorrectFile
}// end of if for space

} else {  //else of if body for session



%>

  <jsp:include page="timeout.html" flush="true"/>

  <%



}



%>



  <%if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){}
else {

%>



</div>

<div class ="mainMenu" id = "emenu">

<jsp:include page="getmenu.jsp" flush="true"/>

</div>

	<% }%>
<!-- Akshi:Added for bug #6948 -->
</div>
<div class = "myHomebottomPanel"> 

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>
</body>



</html>





