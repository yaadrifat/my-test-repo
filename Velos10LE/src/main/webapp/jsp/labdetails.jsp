<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!-- saved from url=(0022)http://internet.e-mail -->
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML><HEAD><TITLE><%=LC.L_More_Info%><%--More Information*****--%></TITLE>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</HEAD>

<script>

function validate(formobj) {
    count=formobj.count.value;
    totcount=formobj.totcount.value;
    
    if (totcount==1){
    window.opener.document.patlab.tstatus.value=formobj.tstatus.value;
	window.opener.document.patlab.LLN.value=formobj.LLN.value;
 	window.opener.document.patlab.ULN.value=formobj.ULN.value;
	window.opener.document.patlab.accnumber.value=formobj.accnum.value;
	window.opener.document.patlab.notes.value=formobj.notes.value;
    	
    }
    else{
	window.opener.document.patlab.tstatus[count].value=formobj.tstatus.value;
	window.opener.document.patlab.LLN[count].value=formobj.LLN.value;
 	window.opener.document.patlab.ULN[count].value=formobj.ULN.value;
	window.opener.document.patlab.accnumber[count].value=formobj.accnum.value;
	window.opener.document.patlab.notes[count].value=formobj.notes.value;
	}
	
	this.close();
}
 
 
</script>

<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%><%@page import="com.velos.eres.service.util.LC"%>
<jsp:include page="include.jsp" flush="true"/>
<%
	HttpSession tSession = request.getSession(true); 
	 if (sessionmaint.isValidSession(tSession))
	{
		String status=request.getParameter("status");
		String lln=request.getParameter("lln");
		if (lln==null) lln="";
		String uln=request.getParameter("uln");
		if (uln==null) uln="";
		String accnum=request.getParameter("accnum");
		if (accnum==null) accnum="";

		String count=request.getParameter("count");
		String totcount=request.getParameter("totcount");
	CodeDao cdLabs1=new CodeDao(); 
	cdLabs1.getCodeValues("tststat");
	String tstatus=cdLabs1.toPullDown("tstatus",EJBUtil.stringToNum(status));
	 %>

<Form class=formDefault id="labdetailsfrm" name="detail" action="" method="post" onSubmit="if (validate(document.detail)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}" >


	<BR>
	<input type="hidden" name="count" value="<%=count%>">
			<input type="hidden" name="totcount" value="<%=totcount%>">
	<table >
	<tr>
	<td class=tdDefault><%=LC.L_Accession_Number%><%--Accession number*****--%></td>
	<td><INPUT type=text name="accnum" size="10" maxLength ="10" value="<%=accnum%>" ></td>
	</tr>	
	
	<tr>
	<td class=tdDefault> <%=LC.L_Lln%><%--LLN*****--%> </td>
	<td><INPUT type=text name=LLN size=10 maxLength = 10 value="<%=lln%>"></td>
	</tr>
		
	<tr>
	<td class=tdDefault> <%=LC.L_Uln%><%--ULN*****--%> </td>
	<td><INPUT type=text name=ULN size=10 maxLength = 10 value="<%=uln%>"></td>
	</tr>
	<tr>
	<td class=tdDefault>
		<%=LC.L_Lab_Status%><%--Lab Status*****--%> 
	</td>
	<td>
		<%= tstatus%>
	</td>
	</tr>
		<tr>
	<td class=tdDefault> <%=LC.L_Notes%><%--Notes*****--%> </td>
	<td><textarea rows="10" name="notes" cols="59"></textarea></td>
	</tr>
		<script>document.detail.notes.value=window.opener.document.patlab.hidenotes.value</script>
	   </table>		
	<br>
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="N"/>
			<jsp:param name="formID" value="labdetailsfrm"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
	</form>
<%}else{%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/> 
<%}%>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
