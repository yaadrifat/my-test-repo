<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%--Copy Forms*****--%><%=LC.L_Copy_Forms%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<script>checkQuote="N";</script>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<script>
if (document.getElementById('eSign')) { document.getElementById('eSign').disabled=false; } 
if (document.getElementById('submit_btn')) { document.getElementById('submit_btn').disabled=false; }
function  blockSubmit() {
	 setTimeout('document.getElementById(\'eSign\').disabled=true', 10);
	 setTimeout('document.getElementById(\'submit_btn\').disabled=true', 10);
}

function  validate(formobj, formobj1, numRows)
 {
 	 
 	// formobj.newFormType.value = formobj1.newFormType.value ;
	 formobj.formName.value  = formobj1.name.value ;
	 /* Bug#10963 09-Aug-2012 -Sudhir*/
	 //if (!(allsplcharcheck(formobj.formName.value))) return false;
	flag = false ;
	
	if(numRows > 1)
	{
		for ( count = 0 ; count < numRows ; count ++ )
		{
			
			if (  formobj.selectedForms[count].checked )
			{
				flag = true ;
				break ;
			}
		} 
	}
	
	else if (numRows==1)
	{
		if (  formobj.selectedForms.checked )
				{
					flag = true ;
					
				}
	}
 
 
    if ( flag == false ) 
	{
		/* alert("Please select a form to be copied ");*****/
		alert("<%=MC.M_SelFrm_ToBeCopied%>");
		return false ;
	}
	
	//if (   !(validate_col (' Category', formobj1.newFormType)  )  )  return false;
		
 	if (   !(validate_col (' Form Name', formobj1.name)  )  )  return false;
	
		
     if (  !(validate_col('e-Signature',formobj.eSign)     )   ) return false;
	 
	

  }
  

	
</script>


<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="ctrldao" scope="request" class="com.velos.esch.business.common.EventAssocDao"/>
<jsp:useBean id="linkedformsdao" scope="request" class="com.velos.eres.business.common.LinkedFormsDao"/>
<jsp:useBean id="linkedformsB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="catLibJB" scope="request"  class="com.velos.eres.web.catLib.CatLibJB"/>
<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*"%>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:include page="include.jsp" flush="true"/>

<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>







<DIV class="popDefault" id="div1"> 

  <%
   HttpSession tSession = request.getSession(true); 
   if (sessionmaint.isValidSession(tSession))
   {
      int pageRight = 7;
	  String userId = (String) tSession.getValue("userId");
      String accountId = (String) tSession.getValue("accountId");		
	 
	 // module check salil
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
		int account_form_rights = Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMS"));

     String modRight = (String) tSession.getValue("modRight");
	 int patProfileSeq = 0, formLibSeq = 0;					 
	 acmod.getControlValues("module");
	 ArrayList acmodfeature =  acmod.getCValue();
	 ArrayList acmodftrSeq = acmod.getCSeq();
	 char formLibAppRight = '0';
	 char patProAppRight = '0';
	 formLibSeq = acmodfeature.indexOf("MODFORMS");
	 patProfileSeq = acmodfeature.indexOf("MODPATPROF");	
 	 formLibSeq = ((Integer) acmodftrSeq.get(formLibSeq)).intValue();
 	 patProfileSeq = ((Integer) acmodftrSeq.get(patProfileSeq)).intValue();
     formLibAppRight = modRight.charAt(formLibSeq - 1);
     patProAppRight = modRight.charAt(patProfileSeq - 1);

	//end  module check by salil
	
	   int iaccId=EJBUtil.stringToNum(accountId);

     String study="";
	 String group="";
	 String dAccSites ="";
	 String dAccGroups = "";
	 	 
	 CodeDao cd2 = new CodeDao();
 	 cd2.getAccountSites(EJBUtil.stringToNum(accountId));
	 String frmname="";
	 String selstudy="";
	 String  site="";
	 int siteId=0;
 	 int groupId=0;
	 int selstudyId=0;
	 String linkedto="";
	 frmname=request.getParameter("formname");
	 if(frmname==null) 
	 {frmname="";}
	 selstudy=request.getParameter("ByStudy") ;
	 if(selstudy==null )  
	 {selstudyId=0 ;}
	 else 
	  { selstudyId = EJBUtil.stringToNum(selstudy); }
	 linkedto=request.getParameter("linked");
	 
	 site=request.getParameter("ddorg");
	 if(site==null)
	 { siteId=0;}
	 else
	 { siteId=EJBUtil.stringToNum(site);}
	 
	 
	 group=request.getParameter("ddgrp");
	 if(group==null)
	 { groupId=0;
	 }
	 else
	 { groupId=EJBUtil.stringToNum(group);}
			 
	 CodeDao cd3 = new CodeDao();
 	 cd3.getAccountGroups(EJBUtil.stringToNum(accountId));
 	 dAccGroups = cd3.toPullDown("ddgrp",groupId,true);
			 
	 StudyDao studyDao = new StudyDao();	
	 studyDao.getStudyValuesForUsers(userId);	
	 ArrayList studyIds = studyDao.getStudyIds();
	 ArrayList studyNumbers = studyDao.getStudyNumbers();	
	  ////////////////////////////////////////////////
	 String txtName=request.getParameter("name");
	 if(txtName==null) 
	 txtName="";

	 study=EJBUtil.createPullDown("ByStudy",selstudyId,studyIds,studyNumbers);
	 
	 int formTypeId = 0 ;
	 String formType=request.getParameter("newFormType");
	
	 if (formType == null)
		 formTypeId = 0;
	 else 
	     formTypeId = EJBUtil.stringToNum(formType);
	 
	 //For the category of the new form
	 
	 ArrayList ids= new ArrayList();
	 ArrayList names= new ArrayList();
	 CatLibDao catLibDao=new CatLibDao();
	
	 String pullDown= "";
	 String catLibType="T" ; 
	 catLibDao= catLibJB.getAllCategories(iaccId ,catLibType);
	 ids = catLibDao.getCatLibIds();
	 names= catLibDao.getCatLibNames();
	 //pullDown=EJBUtil.createPullDown("newFormType", formTypeId, ids, names);

     if (pageRight >=6)  
	 {
%>
		<Form  name="copyFormForAccountSearch" method="post" action="copyFormForAccount.jsp">
	  
	    <table width="99%" border="0" cellspacing="0" cellpadding="0" >
		<tr>
	    <p class="defcomments"><%-- Copy Form: Enter the following details for your new form***** --%><%=MC.M_CopyFrm_EtrDetsForNewFrm%></p>
		</tr>
		<tr>
		<td width="25%" align="right"><%-- Form Name***** --%><%=LC.L_Frm_Name%> <FONT class="Mandatory">* </FONT></td>
		<td width="75%"><input type="text" name="name" size = 40 MAXLENGTH = 50 value="<%=txtName%>"> </td>
		</tr>
		<!-- <tr>
		<td width="25%" align="right"><FONT class="Mandatory">* </FONT> Form Type</td>
		<td ><%=pullDown%> </td>
		</tr>	 -->
		</table>
	<P class="defComments"><%-- Search Form(s) to be Copied, By***** --%><%=MC.M_SrchFrms_ToCopy%></P>
    <table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl outline midAlign" >
       
		<tr >
		<td width=12%> <%-- Form Name***** --%><%=LC.L_Frm_Name%></td>
		<td width=20%><Input type=text name="formname" size=20  value='<%=frmname%>'></td>
		<td width=12% align="right"> <%-- <%=LC.Std_Study%>***** --%><%=LC.L_Study%>:&nbsp; </td>
		<td width=20%><%=study%></td>
		<td width=12% align="right"> <%-- Linked To***** --%><%=LC.L_Linked_To%>:&nbsp;</td>

		 <% if(linkedto==null)
		{
			 if ((String.valueOf(formLibAppRight).compareTo("1") != 0)&&(String.valueOf(patProAppRight).compareTo("1") == 0))
			 {
				linkedto="'PA'";
			 }
			 else if ((String.valueOf(patProAppRight).compareTo("1") != 0)&&(String.valueOf(formLibAppRight).compareTo("1") == 0)) 			 
			 {        	         
				linkedto="'S','SP','A','SA','PS','PR'";						
		 	 }
			 else  			 
			 {        	         
				linkedto="'S','SP','A','SA','PS','PR','PA'";						
		 	 }			 
		       // end of check
		}	//end by salil for linkedto%>   
			
				   <%
	   if ((String.valueOf(formLibAppRight).compareTo("1") != 0)&&(String.valueOf(patProAppRight).compareTo("1") == 0))
	{
	  if(linkedto.equals("'PA'")){%>
	   <td><SELECT NAME=linked>
	   <OPTION value = "'PA'" SELECTED><%-- All <%=LC.Pat_Patients%>***** --%><%=LC.L_All_Patients%></OPTION>
	   </SELECT></td>
	 <%} else {%>
 	   <td><SELECT NAME=linked>
	   <OPTION value = "'PA'"><%-- All <%=LC.Pat_Patients%>***** --%><%=LC.L_All_Patients%></OPTION>
	   </SELECT></td>
	   <%}%>  
	<%}
	else if ((String.valueOf(formLibAppRight).compareTo("1") == 0)&&(String.valueOf(patProAppRight).compareTo("1") != 0))
 	{						
   	  if(linkedto.equals("'S'")){%>
	   <td><SELECT NAME=linked>
	   <OPTION value = "'S'" SELECTED><%-- <%=LC.Std_Study%>***** --%><%=LC.L_Study%></OPTION>
	   <OPTION value = "'SP'"><%-- <%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)***** --%><%=LC.L_Pat_SpecStd%></OPTION>
	   <OPTION value = "'PS'"><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)***** --%><%=LC.L_Pat_AllStd%></OPTION>
	   <OPTION value = "'PR'"><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)***** --%><%=LC.L_Pat_AllStdRest%></OPTION>
	   <OPTION value = "'A'"><%-- Account***** --%><%=LC.L_Account%></OPTION>
	   <OPTION value = "'SA'"><%-- All <%=LC.Std_Studies%> ***** --%><%=LC.L_All_Studies%></OPTION>
   	   <OPTION value ="'S','SP','A','SA','PS','PR'" ><%-- Select an Option***** --%><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>
	   <%}%>
	   <% if(linkedto.equals("'SP'")){%>
	  <td><SELECT NAME=linked>
	   <OPTION value = "'S'" ><%-- <%=LC.Std_Study%>***** --%><%=LC.L_Study%></OPTION>
	   <OPTION value = "'SP'" SELECTED><%-- <%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)***** --%><%=LC.L_Pat_SpecStd%></OPTION>
	   <OPTION value = "'PS'"><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)***** --%><%=LC.L_Pat_AllStd%></OPTION>
	   <OPTION value = "'PR'"><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)***** --%><%=LC.L_Pat_AllStdRest%></OPTION>
	   <OPTION value = "'A'"><%-- Account***** --%><%=LC.L_Account%></OPTION>
	   <OPTION value = "'SA'"><%-- All <%=LC.Std_Studies%> ***** --%><%=LC.L_All_Studies%></OPTION>
   	   <OPTION value ="'S','SP','A','SA','PS','PR'" ><%-- Select an Option***** --%><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>
	   <%}%>
	   <% if(linkedto.equals("'A'")){%>
	   <td><SELECT NAME=linked>
	   <OPTION value = "'S'" ><%-- <%=LC.Std_Study%>***** --%><%=LC.L_Study%></OPTION>
	   <OPTION value = "'SP'"><%-- <%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)***** --%><%=LC.L_Pat_SpecStd%></OPTION>
	   <OPTION value = "'PS'"><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)***** --%><%=LC.L_Pat_AllStd%></OPTION>
	   <OPTION value = "'PR'"><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)***** --%><%=LC.L_Pat_AllStdRest%></OPTION>
	   <OPTION value = "'A'" SELECTED><%-- Account***** --%><%=LC.L_Account%></OPTION>
	   <OPTION value = "'SA'"><%-- All <%=LC.Std_Studies%> ***** --%><%=LC.L_All_Studies%></OPTION>
   	   <OPTION value ="'S','SP','A','SA','PS','PR'" ><%-- Select an Option***** --%><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>
	   <%}%>
	   <% if(linkedto.equals("'SA'")){%>
	   <td><SELECT NAME=linked>
	   <OPTION value = "'S'" ><%-- <%=LC.Std_Study%>***** --%><%=LC.L_Study%></OPTION>
	   <OPTION value = "'SP'"><%-- <%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)***** --%><%=LC.L_Pat_SpecStd%></OPTION>
   	   <OPTION value = "'PS'"><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)***** --%><%=LC.L_Pat_AllStd%></OPTION>
	   <OPTION value = "'PR'"><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)***** --%><%=LC.L_Pat_AllStdRest%></OPTION>
	   <OPTION value = "'A'"><%-- Account***** --%><%=LC.L_Account%></OPTION>
	   <OPTION value = "'SA'" SELECTED><%-- All <%=LC.Std_Studies%> ***** --%><%=LC.L_All_Studies%></OPTION>
   	   <OPTION value ="'S','SP','A','SA','PS','PR'" ><%-- Select an Option***** --%><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>
	   <%}%>
	   <% if(linkedto.equals("'PS'")){%>
	   <td><SELECT NAME=linked>
	   <OPTION value = "'S'" ><%-- <%=LC.Std_Study%>***** --%><%=LC.L_Study%></OPTION>
	   <OPTION value = "'SP'"><%-- <%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)***** --%><%=LC.L_Pat_SpecStd%></OPTION>
   	   <OPTION value = "'PS'" SELECTED><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)***** --%><%=LC.L_Pat_AllStd%></OPTION>
	   <OPTION value = "'PR'"><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)***** --%><%=LC.L_Pat_AllStdRest%></OPTION>
	   <OPTION value = "'A'"><%-- Account***** --%><%=LC.L_Account%></OPTION>
	   <OPTION value = "'SA'"><%-- All <%=LC.Std_Studies%> ***** --%><%=LC.L_All_Studies%></OPTION>
   	   <OPTION value ="'S','SP','A','SA','PS','PR'" ><%-- Select an Option***** --%><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>
	   <%}%>
	   <% if(linkedto.equals("'PR'")){%>
	   <td><SELECT NAME=linked>
	   <OPTION value = "'S'" ><%-- <%=LC.Std_Study%>***** --%><%=LC.L_Study%></OPTION>
	   <OPTION value = "'SP'"><%-- <%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)***** --%><%=LC.L_Pat_SpecStd%></OPTION>
   	   <OPTION value = "'PS'"><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)***** --%><%=LC.L_Pat_AllStd%></OPTION>
	   <OPTION value = "'PR'" SELECTED><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)***** --%><%=LC.L_Pat_AllStdRest%></OPTION>
	   <OPTION value = "'A'"><%-- Account***** --%><%=LC.L_Account%></OPTION>
	   <OPTION value = "'SA'"><%-- All <%=LC.Std_Studies%> ***** --%><%=LC.L_All_Studies%></OPTION>
   	   <OPTION value ="'S','SP','A','SA','PS','PR'" ><%-- Select an Option***** --%><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>
	   <%}%>
	   
	   <% if(linkedto.equals("'S','SP','A','SA','PS','PR'")){%>
	   <td><SELECT NAME=linked>
	   <OPTION value = "'S'"><%-- <%=LC.Std_Study%>***** --%><%=LC.L_Study%></OPTION>
	   <OPTION value = "'SP'"><%-- <%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)***** --%><%=LC.L_Pat_SpecStd%></OPTION>
	   <OPTION value = "'PS'"><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)***** --%><%=LC.L_Pat_AllStd%></OPTION>
	   <OPTION value = "'PR'"><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)***** --%><%=LC.L_Pat_AllStdRest%></OPTION>
	   <OPTION value = "'A'"><%-- Account***** --%><%=LC.L_Account%></OPTION>
	   <OPTION value = "'SA'"><%-- All <%=LC.Std_Studies%> ***** --%><%=LC.L_All_Studies%></OPTION>
   	   <OPTION value = "'S','SP','A','SA','PS','PR'" SELECTED><%-- Select an Option***** --%><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>	
	<%	}	
		
	}
	else if ((String.valueOf(formLibAppRight).compareTo("1") == 0)&&(String.valueOf(patProAppRight).compareTo("1") == 0))
 	{						
   	  if(linkedto.equals("'S'")){%>
	   <td><SELECT NAME=linked>
	   <OPTION value = "'S'" SELECTED><%-- <%=LC.Std_Study%>***** --%><%=LC.L_Study%></OPTION>
	   <OPTION value = "'SP'"><%-- <%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)***** --%><%=LC.L_Pat_SpecStd%></OPTION>
	   <OPTION value = "'PS'"><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)***** --%><%=LC.L_Pat_AllStd%></OPTION>
	   <OPTION value = "'PR'"><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)***** --%><%=LC.L_Pat_AllStdRest%></OPTION>
	   <OPTION value = "'A'"><%-- Account***** --%><%=LC.L_Account%></OPTION>
	   <OPTION value = "'SA'"><%-- All <%=LC.Std_Studies%> ***** --%><%=LC.L_All_Studies%></OPTION>
	   <OPTION value = "'PA'"><%-- All <%=LC.Pat_Patients%>***** --%><%=LC.L_All_Patients%></OPTION>	   
   	   <OPTION value ="'S','SP','A','SA','PS','PR','PA'" ><%-- Select an Option***** --%><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>
	   <%}%>
	   <% if(linkedto.equals("'SP'")){%>
	   <td><SELECT NAME=linked>
	   <OPTION value = "'S'" ><%-- <%=LC.Std_Study%>***** --%><%=LC.L_Study%></OPTION>
	   <OPTION value = "'SP'" SELECTED><%-- <%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)***** --%><%=LC.L_Pat_SpecStd%></OPTION>
	   <OPTION value = "'PS'"><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)***** --%><%=LC.L_Pat_AllStd%></OPTION>	  
	  <OPTION value = "'PR'"><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)***** --%><%=LC.L_Pat_AllStdRest%></OPTION>		   
	   <OPTION value = "'A'"><%-- Account***** --%><%=LC.L_Account%></OPTION>
	   <OPTION value = "'SA'"><%-- All <%=LC.Std_Studies%> ***** --%><%=LC.L_All_Studies%></OPTION>
	   <OPTION value = "'PA'"><%-- All <%=LC.Pat_Patients%>***** --%><%=LC.L_All_Patients%></OPTION>	   
   	   <OPTION value ="'S','SP','A','SA','PS','PR','PA'" ><%-- Select an Option***** --%><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>
	   <%}%>
	   <% if(linkedto.equals("'A'")){%>
	   <td><SELECT NAME=linked>
	   <OPTION value = "'S'" ><%-- <%=LC.Std_Study%>***** --%><%=LC.L_Study%></OPTION>
	   <OPTION value = "'SP'"><%-- <%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)***** --%><%=LC.L_Pat_SpecStd%></OPTION>
	   <OPTION value = "'PS'"><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)***** --%><%=LC.L_Pat_AllStd%></OPTION>
	   <OPTION value = "'PR'"><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)***** --%><%=LC.L_Pat_AllStdRest%></OPTION>
	   <OPTION value = "'A'" SELECTED><%-- Account***** --%><%=LC.L_Account%></OPTION>
	   <OPTION value = "'SA'"><%-- All <%=LC.Std_Studies%> ***** --%><%=LC.L_All_Studies%></OPTION>
	   <OPTION value = "'PA'"><%-- All <%=LC.Pat_Patients%>***** --%><%=LC.L_All_Patients%></OPTION>	   
   	   <OPTION value ="'S','SP','A','SA','PS','PR','PA'" ><%-- Select an Option***** --%><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>
	   <%}%>
	   <% if(linkedto.equals("'SA'")){%>
	   <td><SELECT NAME=linked>
	   <OPTION value = "'S'" ><%-- <%=LC.Std_Study%>***** --%><%=LC.L_Study%></OPTION>
	   <OPTION value = "'SP'"><%-- <%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)***** --%><%=LC.L_Pat_SpecStd%></OPTION>
   	   <OPTION value = "'PS'"><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)***** --%><%=LC.L_Pat_AllStd%></OPTION>
	   <OPTION value = "'PR'"><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)***** --%><%=LC.L_Pat_AllStdRest%></OPTION>
	   <OPTION value = "'A'"><%-- Account***** --%><%=LC.L_Account%></OPTION>
	   <OPTION value = "'SA'" SELECTED><%-- All <%=LC.Std_Studies%> ***** --%><%=LC.L_All_Studies%></OPTION>
	   <OPTION value = "'PA'"><%-- All <%=LC.Pat_Patients%>***** --%><%=LC.L_All_Patients%></OPTION>	   
   	   <OPTION value ="'S','SP','A','SA','PS','PR','PA'" ><%-- Select an Option***** --%><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>
	   <%}%>
	   <% if(linkedto.equals("'PS'")){%>
	   <td><SELECT NAME=linked>
	   <OPTION value = "'S'" ><%-- <%=LC.Std_Study%>***** --%><%=LC.L_Study%></OPTION>
	   <OPTION value = "'SP'"><%-- <%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)***** --%><%=LC.L_Pat_SpecStd%></OPTION>
   	   <OPTION value = "'PS'" SELECTED><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)***** --%><%=LC.L_Pat_AllStd%></OPTION>
	   <OPTION value = "'PR'"><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)***** --%><%=LC.L_Pat_AllStdRest%></OPTION>
	   <OPTION value = "'A'"><%-- Account***** --%><%=LC.L_Account%></OPTION>
	   <OPTION value = "'SA'"><%-- All <%=LC.Std_Studies%> ***** --%><%=LC.L_All_Studies%></OPTION>
	   <OPTION value = "'PA'"><%-- All <%=LC.Pat_Patients%>***** --%><%=LC.L_All_Patients%></OPTION>	   
   	   <OPTION value ="'S','SP','A','SA','PS','PR','PA'" ><%-- Select an Option***** --%><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>
	   <%}%>
	   <% if(linkedto.equals("'PR'")){%>
	   <td><SELECT NAME=linked>
	   <OPTION value = "'S'" ><%-- <%=LC.Std_Study%>***** --%><%=LC.L_Study%></OPTION>
	   <OPTION value = "'SP'"><%-- <%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)***** --%><%=LC.L_Pat_SpecStd%></OPTION>
   	   <OPTION value = "'PS'"><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)***** --%><%=LC.L_Pat_AllStd%></OPTION>
	   <OPTION value = "'PR'"  SELECTED><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)***** --%><%=LC.L_Pat_AllStdRest%></OPTION>
	   <OPTION value = "'A'"><%-- Account***** --%><%=LC.L_Account%></OPTION>
	   <OPTION value = "'SA'"><%-- All <%=LC.Std_Studies%> ***** --%><%=LC.L_All_Studies%></OPTION>
	   <OPTION value = "'PA'"><%-- All <%=LC.Pat_Patients%>***** --%><%=LC.L_All_Patients%></OPTION>	   
   	   <OPTION value ="'S','SP','A','SA','PS','PR','PA'" ><%-- Select an Option***** --%><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>
	   <%}%>
   <% if(linkedto.equals("'PA'")){%>
	   <td><SELECT NAME=linked>
	   <OPTION value = "'S'" ><%-- <%=LC.Std_Study%>***** --%><%=LC.L_Study%></OPTION>
	   <OPTION value = "'SP'"><%-- <%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)***** --%><%=LC.L_Pat_SpecStd%></OPTION>
   	   <OPTION value = "'PS'"><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)***** --%><%=LC.L_Pat_AllStd%></OPTION>	  
	   <OPTION value = "'PR'"><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)***** --%><%=LC.L_Pat_AllStdRest%></OPTION>
	   <OPTION value = "'A'"><%-- Account***** --%><%=LC.L_Account%></OPTION>
	   <OPTION value = "'SA'"><%-- All <%=LC.Std_Studies%> ***** --%><%=LC.L_All_Studies%></OPTION>
	   <OPTION value = "'PA'" SELECTED><%-- All <%=LC.Pat_Patients%>***** --%><%=LC.L_All_Patients%></OPTION>	   
   	   <OPTION value ="'S','SP','A','SA','PS','PR','PA'" ><%-- Select an Option***** --%><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>
	   <%}%>	   
	   <% if(linkedto.equals("'S','SP','A','SA','PS','PR','PA'")){%>
	   <td><SELECT NAME=linked>
	   <OPTION value = "'S'"><%-- <%=LC.Std_Study%>***** --%><%=LC.L_Study%></OPTION>
	   <OPTION value = "'SP'"><%-- <%=LC.Pat_Patient%> (Specific <%=LC.Std_Study%>)***** --%><%=LC.L_Pat_SpecStd%></OPTION>
	   <OPTION value = "'PS'"><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%>)***** --%><%=LC.L_Pat_AllStd%></OPTION>
	   <OPTION value = "'PR'"><%-- <%=LC.Pat_Patient%> (All <%=LC.Std_Studies%> - Restricted)***** --%><%=LC.L_Pat_AllStdRest%></OPTION>
	   <OPTION value = "'A'"><%-- Account***** --%><%=LC.L_Account%></OPTION>
	   <OPTION value = "'SA'"><%-- All <%=LC.Std_Studies%> ***** --%><%=LC.L_All_Studies%></OPTION>
	   <OPTION value = "'PA'"><%-- All <%=LC.Pat_Patients%>***** --%><%=LC.L_All_Patients%></OPTION>	   
   	   <OPTION value = "'S','SP','A','SA','PS','PR','PA'" SELECTED><%-- Select an Option***** --%><%=LC.L_Select_AnOption%></OPTION>
	   </SELECT></td>	
	<%	}	
		
	}%>

		   	</tr>
		  	
	   		<tr >
	   		<td > <%-- Group***** --%><%=LC.L_Group%>:</td>  
		   	<td><%=dAccGroups%></td>
	   		<td  align="right"> <%-- Organization ***** --%><%=LC.L_Organization%>:&nbsp;</td>
	   		<td colspan="2"><%dAccSites = cd2.toPullDown("ddorg",siteId,true);%>
			<%=dAccSites%>
			</td>
		   	<td align="center">
				<button type="submit"><%=LC.L_Search%></button>
	        </td>
	  </table>
	  </FORM>
	<%			

				int accId=EJBUtil.stringToNum(accountId);
				linkedformsdao=linkedformsB.getFormsLinkedToAccount(accId,frmname.trim(),selstudyId,linkedto,siteId,groupId);
				ArrayList formIds=linkedformsdao.getFormId();
				ArrayList formnames= linkedformsdao.getFormName(); 
				ArrayList formdescriptions= linkedformsdao.getFormDescription(); 
				ArrayList formstatuss= linkedformsdao.getFormStatus(); 
				ArrayList formlinkedtos=linkedformsdao.getFormDisplayType();
	   	  		int len=formIds.size();
				int counter;
	 			Integer formId;
		%>

		
	<Form name="copyFormForAccount" method="POST" id="cpyFrmAcct" action="copyFormForAccountSubmit.jsp"  onSubmit="if (validate(document.copyFormForAccount, document.copyFormForAccountSearch,'<%=len%>')== false) { setValidateFlag('false'); return false; } else { blockSubmit(); setValidateFlag('true'); return true;}">
	<input type="hidden" name="formName" >
<!-- 	<input type="hidden" name="newFormType" > -->
		<table width="99%" border="0" cellspacing="0" cellpadding="0" class="basetbl midAlign" >
	       <tr> 
	        	<th width="28%"> <%-- Name***** --%><%=LC.L_Name%> </th>
			    <th width="30%"> <%-- Description ***** --%><%=LC.L_Description%> </th>
			   <th width="22%"> <%-- Selected***** --%><%=LC.L_Selected%> </th>	
	      </tr>
	       <%
				String formname="";
				String formdescription="";
				
	    for(counter = 0;counter<len;counter++)
		{	
			// salil 
			formId = (Integer)formIds.get(counter);
			formname=((formnames.get(counter)) == null)?"-":(formnames.get(counter)).toString();
			formdescription=((formdescriptions.get(counter)) == null)?"-":(formdescriptions.get(counter)).toString();
			
			if ((counter%2)==0) 
			{%>
	      <tr class="browserEvenRow"> 
	        <%

			}else
			{ %>
	        <tr class="browserOddRow"> 
	        <% }%>
	        <td> <%=formname%></td>
	     	<td><%=formdescription%></td>   
			<td ><input type="checkbox" name="selectedForms" value="<%=formId%>" onClick=""></td>		
	      </tr>
	      <% } //end of for loop

	%>
	    </table>

		<jsp:include page="submitBar.jsp" flush="true"> 
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="cpyFrmAcct"/>
				<jsp:param name="showDiscard" value="N"/>
		</jsp:include>
	  </Form>
 <%
		
	} //end of if body for page right

else
{%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%
} //end of else body for page right
	}
else
{ %>
 <jsp:include page="timeout.html" flush="true"/>
<%}%>
<div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>  
</body>

</html>


			

