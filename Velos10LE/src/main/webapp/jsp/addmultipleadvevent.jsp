<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<%--

	Project Name:		Velos eResearch
	Author:				Jnanamay Majumdar
	Created on Date:	4Dec2007
	Purpose:			GUI for Add multiple Adverse Events
	File Name:			addmultipleadvevent.jsp

--%>
<title> <%=MC.M_Add_MultiAdvEvt%><%--Add Multiple Adverse Events*****--%></title>

<%@ page import ="java.util.*, java.io.*, org.w3c.dom.*,com.velos.eres.business.common.*"%>
<meta http-equiv="Content-type" content="text/html; charset=UTF-8">
<jsp:include page="include.jsp" flush="true"/>

</head> 

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>

<script Language="javascript"> 

/* Added By Amarnadh for JS error while clicking 'Select from Dictionary' and 'Calculate' links. */
  
function openwindowdict(studyAdvlkpVer,versionnum,rowid){
	//KM-07Oct09
	currentDictSettingValue = document.advevent.currentDictSetting.value;	
	if (studyAdvlkpVer == "0") {		
	 	windowName = window.open("defaultGrade.jsp?parentform=advevent&gradefld=grade"+rowid+"&advnamefld=advName"+rowid+"&dispfield=advGradeText"+rowid+"&advDictionaryField=advDictionary&mulAdEvent=mulAdEvent&currentDictSettingValue="+currentDictSettingValue,"myWin","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=500,height=350 top=200,left=200"); 
		windowName.focus(); 		
	} else{	 
   		var calc = "Grade : fdatacolumn fdispcolumn"; 
		if (versionnum == 3) {		
	 		var keyexpr = "advGradeText"+rowid+"|[VELEXPR]*[VELSTR=Grade:]*[VELSPACE]*[VELKEYWORD=nci3_grade]*[VELSPACE]*[=nci3_shrtname]~grade"+rowid+"|nci3_grade~advName"+rowid+"|nci3_shrtname~MedDRAcode"+rowid+"|nci3_meddra~aeToxicity"+rowid+"|nci3_toxicity~aeToxicityDesc"+rowid+"|nci3_toxicityDesc~aeCategory"+rowid+"|nci3_category~aeGradeDesc"+rowid+"|nci3_desc";
		 	}
	 	else if(versionnum==4){
	 		var keyexpr = "advGradeText"+rowid+"|[VELEXPR]*[VELSTR=Grade:]*[VELSPACE]*[VELKEYWORD=adv_lkp_grade]*[VELSPACE]*[VELKEYWORD=adv_lkp_toxic]~grade"+rowid+"|adv_lkp_grade~advName"+rowid+"|adv_lkp_tshort~MedDRAcode"+rowid+"|adv_lkp_meddra~aeToxicity"+rowid+"|adv_lkp_toxic~aeToxicityDesc"+rowid+"|adv_lkp_tdesc~aeCategory"+rowid+"|adv_lkp_category~aeGradeDesc"+rowid+"|adv_lkp_gdesc";	 
	 	    }
		else{
		 		var keyexpr = "advGradeText"+rowid+"|[VELEXPR]*[VELSTR=Grade:]*[VELSPACE]*[VELKEYWORD=adv_lkp_grade]*[VELSPACE]*[VELKEYWORD=adv_lkp_toxic]~grade"+rowid+"|adv_lkp_grade~advName"+rowid+"|adv_lkp_toxic~MedDRAcode"+rowid+"|adv_lkp_meddra~aeToxicity"+rowid+"|adv_lkp_toxic~aeToxicityDesc"+rowid+"|adv_lkp_tdesc~aeCategory"+rowid+"|adv_lkp_category~aeGradeDesc"+rowid+"|adv_lkp_desc";
			}	 		 
		//windowName = window.open('getlookup.jsp?viewId=' +studyAdvlkpVer + '&form=advevent&dfilter=&keyword='+keyexpr ,'Information','toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=900,height=550 top=100,left=100') 	 		
		//windowName.focus();
		
		document.advevent.target="Lookup";
		document.advevent.method="post";
	                  
		document.advevent.action="getlookup.jsp?viewId=" +studyAdvlkpVer + "&form=advevent&dfilter=&seperator=,"+
	                   "&keyword="+keyexpr+"|LKP_PK|[VELHIDE]&maxselect=1";                  
	           
		formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');	
			
		if (formWin && !formWin.closed) formWin.focus();
		document.advevent.submit();
		document.advevent.target="_self";
		document.advevent.action="savemultipleadvevent.jsp";
		void(0);	
	} 	
}


function openwindowcalc(nciVersion,pkey,row){

	if (nciVersion == '0' || nciVersion == ''){
		alert("<%=MC.M_NoAdvEvtDictAssoc_Std%>")/*alert("There is no Adverse Event Dictionary associated with the <%=LC.Std_Study%>")*****/
		return ;	
	}
	else
	{	
		//windowname=window.open("getAdvToxicity.jsp?nciVersion=" + nciVersion + "&pkey=" + pkey + "&parentform=advevent&gradefld=grade"+row+"&advnamefld=advName"+row+"&dispfield=advGradeText"+row ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=500,height=350 top=120,left=2000"); 
		//windowname.focus();
		
		document.advevent.target="Lookup";
		document.advevent.method="post";                   
		document.advevent.action = "getAdvToxicity.jsp?nciVersion=" + nciVersion + "&pkey=" + pkey + "&parentform=advevent&gradefld=grade"+row+"&advnamefld=advName"+row+"&dispfield=advGradeText"+row  ;                                
	           
		formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');	
			
		if (formWin && !formWin.closed) formWin.focus();
		document.advevent.submit();
		document.advevent.target="_self";
		document.advevent.action="savemultipleadvevent.jsp";
		void(0);	
	
	}
}



/* End added */

function openWin() {
      windowName = window.open("","Information","toolbar=yes,location=yes,scrollbars=yes,resizable=yes,menubar=yes,width=600,height=300")
	windowName.focus();
}

function validateme(formObj){
 	counter = 0; 
 	totRows = formObj.cntNumrows.value;

for(cnt = 0; cnt<totRows; cnt++){
	advEtype = "formObj.advEtype" + cnt;
	advEtypes = eval(advEtype).value; 
	startDt = "formObj.startDt" + cnt;
	startDts=eval(startDt).value;
	stopDt = "formObj.stopDt" + cnt;//KM
	stopDts = eval(stopDt).value;


if (advEtypes =='' && startDts==''){
        counter++;
          
	    if (counter == totRows){
	        alert("<%=MC.M_EtrAtleastOne_AdvEvt%>");/*alert("Please enter atleast one Adverse Event");*****/
	    	return false;
	    }
	}
			
 	//KM-Modified
	if (advEtypes != '' && startDts ==''){
    alert("<%=MC.M_Etr_StartDt%>");/*alert("Please enter the Start Date");*****/
    return false;     
    }
    if (advEtypes == '' && startDts !=''){
    alert("<%=MC.M_Sel_AdvEvtTyp%>");/*alert("Please select the Adverse Event Type");*****/     
    return false;     
    }

	//KM-#3255
	if (!(validate_date(eval("formObj.startDt"+cnt)))) return false	
    if (!(validate_date(eval("formObj.stopDt"+cnt)))) return false	


	if ((startDts != null && startDts != '' )&&(stopDts != null && stopDts !=''))
	{
		
		if (CompareDates(startDts,stopDts, '>'))
		{
		  	alert ("<%=MC.M_StartDtCnt_GtrThanStopDt%>");/*alert ("Start Date can not be greater than Stop Date");*****/
			eval("formObj.startDt"+cnt).focus();
			return false;
		}
		
	}
 
 }
 
 if(!(validate_col('e-Signature',formObj.eSign))) return false
	if (isNaN(formObj.eSign.value) == true){
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formObj.eSign.focus();
		return false;		
	}

}
</script>

<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.StringUtil"%>
<%@ page language = "java" import="com.velos.esch.business.common.*,com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.person.*,com.velos.eres.service.util.*,com.velos.eres.web.user.*,com.velos.eres.web.grpRights.GrpRightsJB"%>

<jsp:useBean id="sess" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="adveventB" scope="request" class="com.velos.esch.web.advEve.AdvEveJB"/>
<BODY>
<%
HttpSession tSession = request.getSession(true);
if (sess.isValidSession(tSession)){	

	String studyId =request.getParameter("studyId");	
	String pkey = request.getParameter("pkey");
	String accId = (String) tSession.getValue("accountId");//KM
		
	SchCodeDao cd = new SchCodeDao();	
	//reset cd 
	cd.resetObject();	
	cd.getCodeValues("adve_type");
	String advEtype = "";	
	
	SchCodeDao cd_drugproc = new SchCodeDao();
	//reset cd 
	cd_drugproc.resetObject();	
	cd_drugproc.getCodeValues("adve_relation"); 
	String adveRelation = "";
	
	SchCodeDao cd_actiondesc = new SchCodeDao(); 
	//reset cd 
	cd_actiondesc.resetObject();	
	cd_actiondesc.getCodeValues("outaction"); 
	String action = "";
	
	SchCodeDao cd_recoverydesc = new SchCodeDao(); 	
	//reset cd 
	cd_recoverydesc.resetObject();	
	cd_recoverydesc.getCodeValues("adve_recovery"); 
	String recovery = "";
	
	
	
	//JM: 31Jan2008	
	studyB.setId(EJBUtil.stringToNum(studyId));
	studyB.getStudyDetails();
	
	String studyAdvlkpVer = "0";
	String studyAdvlkpVerNumber = "0";

	//get study lookup view for adverse events
	studyAdvlkpVer = studyB.getStudyAdvlkpVer();
		
		if (EJBUtil.isEmpty(studyAdvlkpVer))
		{
			studyAdvlkpVer  = "0";
		}	
		else
		{
			studyAdvlkpVerNumber = studyB.getLkpTypeVersionForView(EJBUtil.stringToNum(studyAdvlkpVer) );
		}


		if (EJBUtil.isEmpty(studyAdvlkpVerNumber))
		{
			studyAdvlkpVerNumber  = "0";
		}	

		//KM-#4295
		int index=-1;
		String studyLkpDict = studyB.getStudyAdvlkpVer();
		if (EJBUtil.isEmpty(studyLkpDict))
			studyLkpDict = "0";
	    int studyLkpDictId=EJBUtil.stringToNum(studyLkpDict);
		LookupDao lookupDao = new LookupDao();
		lookupDao = studyB.getAllViewsForLkpType(EJBUtil.stringToNum(accId) ,"NCI");
		ArrayList viewIds = lookupDao.getLViewIds();
		ArrayList viewNames = lookupDao.getLViewNames();
		String settingValue="";

		 if (studyLkpDictId==0)
		 {
			settingValue="";
		 }
		 else
		 {
		   index=viewIds.indexOf(new Integer(studyLkpDict));
		   settingValue=(String)viewNames.get(index);
		 }

		 if (StringUtil.isEmpty(settingValue))
		 {
			settingValue = LC.L_Free_TextEntry/*Free Text Entry*****/;
		 }

%>
<br>
<form name="advevent" id="addmultadvevt" method=post action="savemultipleadvevent.jsp" onsubmit="if (validateme(document.advevent)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
<input type="hidden" name=studyId value=<%=studyId%>>
<input type="hidden" name="pkey"  value=<%=pkey%>>
<input type="hidden" maxLength="30"  size="30" name="advDictionary"  value='' readonly> 
<input type= hidden   name=currentDictSetting value="<%=settingValue%>"> <!--KM-#4295-->
<DIV class="popDefault" id="div1" style="display:table;">
<table width="100%" >
	
	<tr>
	<th width="10%"> <%=LC.L_AdverseEvent_Type%><%--Adverse Event Type*****--%><FONT class="Mandatory">* </FONT></th> <!--KM-->
	<th colspan="2"> <%=LC.L_AeOrGrading%><%--AE/Grading*****--%>  </th>
	<th width="8%"> <%=LC.L_Tx_Course%><%--Tx Course*****--%> </th>
<%-- INF-20084 Datepicker-- AGodara --%>	
	<th colspan="1"> <%=LC.L_Start%><%--Start*****--%> <FONT class="Mandatory">* </FONT></th>	
	<th colspan="1"> <%=LC.L_Stop%><%--Stop*****--%> </th>
	<th width="10%"> <%=LC.L_Attribution%><%--Attribution*****--%>  </th>				
	<th width="15%"> <%=LC.L_Action%><%--Action*****--%> </th>
	<th width="15%"> <%=LC.L_Recovery%><%--Recovery*****--%> </th>		
	</tr>

<%	
	int numrows = 10;	
	for (int i=0; i <  numrows; i++){ %>
<tr >	
	<td width="10%">
	<%
	advEtype = "advEtype"+ i;
	String dadvEveType =  cd.toPullDown(""+ advEtype+ "");
%>
           <!--Modified By Amarnadh for #3253 -->
<%=dadvEveType%>
	</td>
	<td class=tdDefault width="10%">
		<input maxLength="200" READONLY size="15" name="advGradeText<%=i%>" value = "" >
		<input type = "hidden" maxLength="30" size="10" name="grade<%=i%>" value = "" >
		<input type = "hidden" maxLength="200" size="10" name="advName<%=i%>" value = "" >
		<input type = "hidden" maxLength="500" size="10" name="aeToxicity<%=i%>" value = "" >
		<input type = "hidden" maxLength="4000" size="10" name="aeToxicityDesc<%=i%>" value = "" >
		<input type = "hidden" maxLength="4000" size="10" name="aeGradeDesc<%=i%>" value = "" >
		<input type="hidden" maxLength="30"  size="30" name="MedDRAcode<%=i%>"  value= "">
		<input type="hidden" maxLength="500"  size="50" name="aeCategory<%=i%>"  value= "">
			
	</td>
	<td class=tdDefault width="10%">			
		<A href="#" onClick="return openwindowdict(<%=studyAdvlkpVer%>,<%=studyAdvlkpVerNumber%>,<%=i%>);"><%=LC.L_Select%><%--Select*****--%></A> 
		<A href="#" onClick="openwindowcalc('<%=studyAdvlkpVerNumber%>',<%=pkey%>,<%=i%>);"><%=LC.L_Calc%><%--Calc*****--%></A>
</td>
	
	<td class=tdDefault width="8%">	
	<input type="text" name="treatment<%=i%>" maxlength="150" size="6">
	</td>
<%-- INF-20084 Datepicker-- AGodara --%>
	<td width="8%">
		<Input type = "text" name="startDt<%=i%>" class="datefield" size=10 align ="middle" >	<!--KM-3255 -->
	</td>
	<td width="8%">
		<Input type = "text" name="stopDt<%=i%>" class="datefield" size=10 align ="middle" >
	</td>
	<td width="10%">	
	<%
		adveRelation = "adveRelation"+ i;	
		String dCur_drugproc =  cd_drugproc.toPullDown("" + adveRelation );
	%><%=dCur_drugproc%>	
	</td>	
	
<td width="15%">	
	<%
	action = "action"+ i;
	String dCur_actiondesc = cd_actiondesc.toPullDown(""+ action);
	%>
	<%=dCur_actiondesc%>
</td>
	<td width="15%">	
	<%
	recovery = "recovery"+ i;
	String dCur_recoverydesc = cd_recoverydesc.toPullDown(""+ recovery);
	%>
	<%=dCur_recoverydesc%>
		</td>
 
<%} %>
</tr></table>
<input type="hidden" name="cntNumrows" value="<%=numrows%>">
<br>

<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="addmultadvevt"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>

</div>

</form>

<%

}
//end of session
else{
%>

<jsp:include page="timeout.html" flush='true'></jsp:include>
<%}%>
<div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
  	  </BODY>

</html> 