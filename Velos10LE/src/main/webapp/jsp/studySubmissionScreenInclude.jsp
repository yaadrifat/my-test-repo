<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>
<script>
var studySubmScreenFunctions = {
	formObj: {},
	
	setDDFlex: {},
	expandAll: {},
	collapseAll: {},
	toggleExpandCollapseAllClick: {}
};
jQuery(document).ready(function() {
	studySubmScreenFunctions.formObj = document.studyScreenForm;
});
</script>
<script>

studySubmScreenFunctions.expandAll = function (){
	//$j('#toggleExpandCollapseAll').attr('src', './images/collapseIcon.png');
	$j('#toggleExpandCollapseAll').attr('title', "Collapse All");

	$j(".ui-accordion-content").show();
	
	for (var indx=pageSectionRegistry.length; indx > 0; indx--){
		var sectionNo = indx;
		sectionHeaderClick(sectionNo);
	}

	var inHTML = $j('#toggleExpandCollapseAll').html();
	inHTML = inHTML.replace("Expand", "Collapse");
	$j('#toggleExpandCollapseAll').html(inHTML);
	return false;
};

studySubmScreenFunctions.collapseAll = function (){
	//$j('#toggleExpandCollapseAll').attr('src', './images/expandIcon.png');
	$j('#toggleExpandCollapseAll').attr('title', "Expand All");

	$j(".ui-accordion-content").hide();
	$j('#sectionHead1').focus();

	var inHTML = $j('#toggleExpandCollapseAll').html();
	inHTML = inHTML.replace("Collapse", "Expand");
	$j('#toggleExpandCollapseAll').html(inHTML);

	return false;
};

studySubmScreenFunctions.toggleExpandCollapseAllClick = function (){
	//if ( $j('#toggleExpandCollapseAll').attr('src') == './images/expandIcon.png' ) {
	if ( $j('#toggleExpandCollapseAll').attr('title') == "Expand All" ) {
		studySubmScreenFunctions.expandAll();
	} else {
		studySubmScreenFunctions.collapseAll();
	}
	$j('#sectionHead1').focus();
	return false;
};

studySubmScreenFunctions.setDDFlex = function(iElementId){
	if (!document.getElementsByName("ddlist"+iElementId)[0]) return;
	var optvalue=(document.getElementsByName("ddlist"+iElementId)[0]).value;
	if (optvalue.length>0)
	{
		var arrayofStrings=optvalue.split("||");
		if (arrayofStrings.length>1) {
			for (var j=0;j<arrayofStrings.length;j++)
			{
				var ddStr=arrayofStrings[j];
				var arrayofDD=ddStr.split(":");
				var ddcount=arrayofDD[0];
				var selvalue=arrayofDD[1];
				var ddFld = studySubmScreenFunctions.formObj['alternateId'+ddcount];
				// ddFld.id = ddFld.name;
				if (ddFld && ddFld.options) {
					var opt = ddFld.options;
					for (var i=0;i<opt.length;i++){
						if (opt[i].value==selvalue){
							ddFld.selectedIndex=i ;
						}
					}
				}
			}
		} else {
			var ddStr=arrayofStrings[0];
			var arrayofDD=ddStr.split(":");
			var ddcount=arrayofDD[0];
			var selvalue=arrayofDD[1];
			var ddFld = studySubmScreenFunctions.formObj['alternateId'+ddcount];
			// ddFld.id = ddFld.name;
			if (ddFld && ddFld.options) {
			 	var opt = ddFld.options; 
				if (opt == undefined)
			    	opt = ddFld.options; 
				for (var i=0;i<opt.length;i++){
					if (opt[i].value==selvalue){
						ddFld.selectedIndex=i ;
					}
				}
			}
		}// end else
	}//optvalue.length>0	
};
</script>

