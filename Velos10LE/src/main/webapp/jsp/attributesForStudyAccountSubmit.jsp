<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>

<BODY>
 
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="catLibJB" scope="request"  class="com.velos.eres.web.catLib.CatLibJB"/>
<jsp:useBean id="linkedFormsJB" scope="request"  class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="objShareB" scope="request" class="com.velos.eres.web.objectShare.ObjectShareJB"/>
<jsp:useBean id="objShare" scope="request" class="com.velos.eres.web.objectShare.ObjectShareJB"/>
<%@page import="com.velos.eres.business.common.*,java.util.*, com.velos.eres.service.util.EJBUtil" %>
<%@page import="com.velos.eres.service.util.MC"%>
<%@page import="com.velos.eres.service.util.LC"%>
<%@page import="com.velos.eres.service.util.StringUtil"%>
<DIV class="popDefault" id="div1">
<%
HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession)) {
 int iret = -1;
 int iupret = 0;
 int iformLibId=0;
 String mode =request.getParameter("mode");
 String eSign = request.getParameter("eSign");
 String linkedFormId =request.getParameter("linkedFormId");
 //out.println("linkedFormId:"+linkedFormId);
 String accId = (String) tSession.getValue("accountId");
 //out.println("accId:"+accId);
 String ipAdd = (String) tSession.getValue("ipAdd");
 //out.println("ipAdd:"+ipAdd);
 String usr = (String) tSession.getValue("userId");
 //out.println("usr:"+usr);
 String  formOrgIds = request.getParameter("formOrgIds");
 //out.println("formOrgIds:"+formOrgIds);
 String selGrpIds  = request.getParameter("selGrpIds");
String status =request.getParameter("status");

//user ids for response hiding
String usrIds=request.getParameter("usrIds");
usrIds=(usrIds==null)?"":usrIds;

//group ids for response hiding
String grpIds=request.getParameter("grpIds");
grpIds=(grpIds==null)?"":grpIds;

String  oldFormOrgIds = request.getParameter("oldFormOrgIds");
String  oldGrpIds = request.getParameter("oldGrpIds");
//if the responses are private
int noshareId=EJBUtil.stringToNum(request.getParameter("noshareId"));

//out.println("status:"+status);
String lfDisplayType ="";
String lfEntry = "";
String studyId = "";
if(status.equals("Offline") || status.equals("Lockdown")  || status.equals("Active") || status.equals("MIP")) 
	{
	//out.println("inside active:");
	 lfDisplayType = request.getParameter("hdnDispFormLink");
	 lfEntry = request.getParameter("hdnCharName");
	 studyId = request.getParameter("studyIdi"); 
	}
	else{
	//out.println("not active:");
	lfDisplayType =request.getParameter("dispFormLink");
	lfEntry = request.getParameter("charName");
	studyId= request.getParameter("accFormStudy");
	}
	//out.println("lfDisplayType:"+lfDisplayType);
 //out.println("selGrpIds:"+selGrpIds);
 LinkedFormsDao linkedFormsDao = new LinkedFormsDao( ); 

 
 
 //out.println("lfEntry:"+lfEntry);

 //out.println("studyId:"+studyId);
 String stdPageRight = request.getParameter("stdPageRight");
 //out.println("stdPageRight:"+stdPageRight);
 if(stdPageRight==null)
	 stdPageRight="-";

 /* If user does not have right for the study athe no study is selected in the study drop down*/
 if(stdPageRight.equals("0") && studyId.equals("")){
	studyId = request.getParameter("studyIdi"); 
 }

 if(studyId.equals("0"))
	{
		studyId = null;
	}
 /*
   out.println("SONIA formOrgIds"+formOrgIds);
	out.println(" lfDisplayType " +lfDisplayType  );
	out.println(" lfEntrylfEntry " +lfEntry);
	out.println(" studyIdstudyId 12--"+ studyId);
	out.println(" linkedFormId 13--"+ linkedFormId);  */  

%>
	
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%	
String oldESign = (String) tSession.getValue("eSign");

	if(!oldESign.equals(eSign)) {

%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	} 
else
	{
	


linkedFormsJB.setLinkedFormId(EJBUtil.stringToNum(  linkedFormId   )) ;
	 linkedFormsJB.getLinkedFormDetails(); 
		
		int formNameCount = linkedFormsDao.getFormNameCount(
				StringUtil.stringToNum(linkedFormId), lfDisplayType, 
				StringUtil.stringToNum((String)tSession.getAttribute("accountId")),
				StringUtil.stringToNum(studyId));
		if (formNameCount > 0) {
%>
<jsp:include page="duplicateformname.jsp" flush="true">
<jsp:param value="1" name="verbose"/>
</jsp:include>
<jsp:include page="bottompanel.jsp" flush="true"/>
</DIV>
</body>
</html>
<%
			return;
		}
	 linkedFormsDao.setUserId(usr);
	 linkedFormsDao.setOrgIds( formOrgIds );
	 linkedFormsDao.setOldOrgIds(oldFormOrgIds);
	 linkedFormsDao.setGrpIds( selGrpIds );	
	 linkedFormsDao.setOldGrpIds(oldGrpIds);
	 linkedFormsDao.setIpAdd(ipAdd);
	 linkedFormsJB.setLinkedFormsDao(linkedFormsDao);
	 linkedFormsJB.setLFDisplayType( lfDisplayType ); 
	 linkedFormsJB.setLFEntry( lfEntry );
	 linkedFormsJB.setStudyId(studyId) ;
	 iret = linkedFormsJB.updateLinkedForm() ;
   	iformLibId = iret ;
	
	if (iret>=0)
	{
	
	objShareB.setIpAdd(ipAdd);
	objShareB.setCreator(usr);
	objShareB.setObjNumber("4");//HardCoded for form response filtering rights
	objShareB.setFkObj(linkedFormId);// formId, CalId, reportId
	objShareB.setObjSharedId(grpIds);// shared ids
	objShareB.setObjSharedType("G");// U-user id,A -account id,S - study id,G - group id,O - organization id 
	objShareB.setRecordType("N");
	objShareB.setModifiedBy(usr);	
	int ret = objShareB.setObjectShareDetails();
	System.out.println(ret);
	
	
	objShare.setIpAdd(ipAdd);
	objShare.setCreator(usr);
	objShare.setObjNumber("5");//HardCoded for form response filtering rights for users
	objShare.setFkObj(linkedFormId);// formId, CalId, reportId
	objShare.setObjSharedId(usrIds);// shared ids
	objShare.setObjSharedType("U");// U-user id,A -account id,S - study id,G - group id,O - organization id 
	objShare.setRecordType("N");
	objShare.setModifiedBy(usr);	
	 ret = objShare.setObjectShareDetails();
	System.out.println(ret);
	
	
	objShare.setIpAdd(ipAdd);
	objShare.setCreator(usr);
	objShare.setObjNumber("6");//HardCoded for form response filtering rights for private mode
	objShare.setFkObj(linkedFormId);// formId, CalId, reportId
	System.out.println("****************NoShareIDs" + noshareId);
	if (noshareId>0) objShare.setObjSharedId(usr);
	else objShare.setObjSharedId("");// shared ids
	objShare.setObjSharedType("P");// U-user id,A -account id,S - study id,G - group id,O - organization id 
	objShare.setRecordType("N");
	objShare.setModifiedBy(usr);	
	ret = objShare.setObjectShareDetails();
	System.out.println(ret);
	
	// Check if this user has a P and 6
	objShare.getObjectShareDetails(StringUtil.stringToNum(linkedFormId), "P", "6", usr, accId);
	String[] arrIds = StringUtil.trueValue(objShare.getShrdWithIds()).split(",");
	boolean isPrivateChecked = false;
	for (int iX=0; iX<arrIds.length; iX++) {
		if (arrIds[iX].matches(usr)) {
			isPrivateChecked = true;
		}
	}
	if (isPrivateChecked) {
		System.out.println("Add all form resp authors for linkedFormId="+linkedFormId+" of form type="+lfDisplayType);
		objShare.setFkObj(linkedFormId);
		objShare.setIpAdd(ipAdd);
		objShare.setCreator(usr);
		objShare.insertIntoObjectShareForRespAuthors(lfDisplayType);
	}
	
	} //end (iret=0)
	
	
	if ( (iret < 0 ) )
	 { %>
					
		<br><br><br><br><br>
		<p class = "sectionHeadings" align = center> Data not saved successfully </p>
		<input type="Button" name="back" value="Back" onclick="window.history.back()"></p>
		
	<%}else {%>
		<br><br><br><br><br><p class = "sectionHeadings" align = center>
		Data saved successfully</p>	
	  <script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
		</script>	 	
				
	<%}
	}
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>

