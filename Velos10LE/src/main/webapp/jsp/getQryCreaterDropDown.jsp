<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*"%>

<jsp:useBean id ="sessionmaint2" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="teamB" scope="session" class="com.velos.eres.web.team.TeamJB"/>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.studyRights.StudyRightsJB" %>
<jsp:useBean id="studyB" scope="session" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<%

   HttpSession tSession2 = request.getSession(true);
   if (sessionmaint2.isValidSession(tSession2))
	{
		String ddString = null ,studyId = null;
		String teamRole = null;
		String teamUserLastName = null;
		String teamUserFirstName = null;
		int teamId = 0;
		StringBuffer mainStr = new StringBuffer();
		int stdyID=0;
		if(request.getParameter("studyId")!=null && request.getParameter("studyId")!="" && !(request.getParameter("studyId")).equalsIgnoreCase("undefined")){
			studyId = request.getParameter("studyId");
			stdyID =Integer.parseInt(studyId);
		}
		String UserID = (String) tSession2.getValue("userId");		
		//System.out.println("UserID ==== "+UserID);
		
		String dmSiteId = "";
		if(UserID!=null && !UserID.equals("")){
			userB.setUserId(EJBUtil.stringToNum(UserID));
			userB.getUserDetails();
			dmSiteId = userB.getUserSiteId();
		//	System.out.println("dmSiteId ======== "+dmSiteId);
		}
		
		String acc = (String) tSession2.getValue("accountId");
		int accId = EJBUtil.stringToNum(acc);
		
		String selOrg = request.getParameter("accsites") ;
		if(dmSiteId == null || dmSiteId.equals(""))
			dmSiteId = "0";
		int selOrgId = EJBUtil.stringToNum(dmSiteId);
		
		//System.out.println("selOrgId=>  "+selOrgId+" accId=>  "+acc+" stdyID=> "+stdyID);
		CodeDao codedao =new CodeDao();
		TeamDao teamDao = new TeamDao();
		teamDao = teamB.getTeamValuesBySite(stdyID, accId, 0);
		//teamDao = teamB.getTeamValues(stdyID, accId);
		ArrayList teamRoleIds		= teamDao.getTeamRoleIds();
		ArrayList teamIds 		= teamDao.getTeamIds();
		ArrayList teamRoles 		= teamDao.getTeamRoles();		
		ArrayList teamUserLastNames 	= teamDao.getTeamUserLastNames();
		ArrayList teamUserFirstNames = teamDao.getTeamUserFirstNames();
		ArrayList teamUserIds = teamDao.getUserIds();
		/*ArrayList userSiteNames = teamDao.getUsrSiteNames();
		ArrayList teamRights 	= teamDao.getTeamRights();
		ArrayList inexUsers	 	= teamDao.getInexUsers();
		ArrayList userIds = teamDao.getUserIds();
		ArrayList siteIds2 = teamDao.getSiteIds();
		ArrayList userPhones = teamDao.getUserPhone();
		ArrayList userEmails = teamDao.getUserEmail();
		ArrayList userTypes = teamDao.getUserType();
		ArrayList teamStatus =teamDao.getTeamStatus();
		ArrayList teamStatusIds=teamDao.getTeamStatusIds();
		ArrayList teamUserTypes = teamDao.getTeamUserTypes();*/
		int len = teamIds.size();
		//System.out.println("len "+len);
		int counter =0;
		String teamRoleVal="";
		mainStr.append("<SELECT NAME='fqCreatorIdDD' id='fqCreatorIdDD' onChange='javascript:fSetParams();'");
		mainStr.append("><OPTION id ='0' value = 'role_All'>"+LC.L_All_Upper/*ALL*****/+"</OPTION>");
		mainStr.append("<OPTION id ='1' value = 'role_system'>"+LC.L_Sys_Generated/*System Generated*****/+"</OPTION>");
		for(counter = 0;counter < len; counter++){
			teamId=EJBUtil.stringToNum(((teamRoleIds.get(counter)) == null)?"-":(teamRoleIds.get(counter)).toString());
			teamRoleVal =codedao.getCodeSubtype(teamId);
			teamRole=((teamRoles.get(counter)) == null)?"-":(teamRoles.get(counter)).toString();
			teamUserLastName=((teamUserLastNames.get(counter)) == null)?"-":(teamUserLastNames.get(counter)).toString();
			teamUserFirstName=((teamUserFirstNames.get(counter)) == null)?"-":(teamUserFirstNames.get(counter)).toString();
			//System.out.println("teamRole "+teamId+"=> "+teamRole+" - "+teamUserFirstName+" "+teamUserLastName);
			//mainStr.append("<option id ="+teamId+" value="+teamRoleVal+">" +teamRole+" - "+teamUserFirstName+" "+teamUserLastName+" </option>");
			//Ankit:- Modified for enhancement DAS-20895-1, Date:- 20/03/13
			mainStr.append("<option id ="+teamId+" value="+teamUserIds.get(counter).toString()+">" +teamRole+" - "+teamUserFirstName+" "+teamUserLastName+" </option>");
		} 
		mainStr.append("</select><input TYPE=\"hidden\" id =\"selfqCreatorId\" NAME=\"selfqCreatorId\" value=\"ALL\"/><input TYPE=\"hidden\" id =\"paramfqCreatorId\" NAME=\"paramfqCreatorId\" value=\"role_All\"/>");
		ddString = mainStr.toString();
		
//System.out.println("mainStr.toString() "+mainStr.toString());
	
%>
<%= ddString %>

<% } %>