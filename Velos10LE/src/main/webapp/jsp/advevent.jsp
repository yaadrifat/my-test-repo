<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_Adverse_Event%><%--Adverse Event*****--%></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT language="JavaScript1.1">

function openwin1() {
      windowName = window.open("usersearchdetails.jsp?fname=&lname=&from=advevent","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
	windowName.focus();
;}


function openWin() {

      windowName = window.open("","Information","toolbar=yes,location=yes,scrollbars=yes,resizable=yes,menubar=yes,width=600,height=300")
	windowName.focus();

;}


function  validate(formobj){    

     if (!(validate_col('Adverse-Eventtype',formobj.adve_type))) return false
     if (!(validate_col('EnteredBy',formobj.enteredByName))) return false     
     if (!(validate_col('e-Signature',formobj.eSign))) return false



	var test="";
	for(var i=0;i<formobj.outcomeType.length;i++)
	{	
		if(formobj.outcomeType[i].checked==true)
		{
		test+="1" ;
		} else
		{
		test+="0";
		}	
	}
	formobj.outcomeString.value=test;
//	alert(formobj.outcomeString.value);


	test="";
	for(var i=0;i<formobj.addInfo.length;i++)
	{	
		if(formobj.addInfo[i].checked==true)
		{
		test+="1" ;
		} else
		{
		test+="0";
		}	
	}
	formobj.addInfoString.value=test;
	//alert(formobj.addInfoString.value);
//	alert(formobj.enteredBy.value);


	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
			 return false;
     }


	if(formobj.outcomeType[0].checked == true)
	{
	if( confirm("<%=MC.M_SelDthPat_FutureNotific%>")){/*if( confirm("You have selected 'Death' as an outcome for this <%=LC.Pat_Patient%>
	. To prevent any further action connected to this <%=LC.Pat_Patient%>
	, such as future notifications, would you like to deactivate this <%=LC.Pat_Patient%>
	's schedule now?")){*****/
	formobj.death.value = "Yes";	
 	}
	else{
	formobj.death.value = "No";   
	}
	}
return true;
}


function checkoutcome(formobj,boxval)
{

if(boxval==0)
{

	for( i = 1;i < formobj.outcomeType.length ; i++)
	{ 	  
	  if(formobj.outcomeType[i].checked == true)
	  {	 
  	alert("<%=MC.M_CntSel_DthOptWithOth%> ");/*alert("You cannot select Death option along with any other option ");*****/
	  formobj.outcomeType[0].checked=false;
	  return false;	
	  }	 

	 }
	
	if(formobj.outcomeType[0].checked == false)
	{
	formobj.outcomeDt.value ="";
	}	 
}
  else{


		if((formobj.outcomeType[0].checked)==true)
		{
		alert("<%=MC.M_CntSelOpt_WithDthOpt%>");/*alert("You cannot select any option along with Death option");*****/
		formobj.outcomeType[boxval].checked=false;
		return false;
		}

}
	

//alert(boxval);
}




</SCRIPT>

</head>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="adveventB" scope="request" class="com.velos.esch.web.advEve.AdvEveJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="ulinkB" scope="request" class="com.velos.eres.web.ulink.ULinkJB"/>


<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.DateUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.common.*,com.velos.eres.web.user.UserJB,com.velos.eres.business.common.*,com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>


<% String src="";
src= request.getParameter("srcmenu");
%>	

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   


<body>
<br>

<DIV class="formDefault" id="div1"> 
  <%

   HttpSession tSession = request.getSession(true); 

   if (sessionmaint.isValidSession(tSession))
   {
	int pageRight = 7;   
   	Integer rt = null;
	rt = (Integer)tSession.getValue("studyManagePat");
    pageRight = rt.intValue();

	String death = "";
   String descripton="";
   String startDt ="";
   String stopDt = "";
   String enteredBy="";
   String outcomeString="";
   String outcomeDt ="";
   String addInfoString="";
   String notes ="";     
   String outcomeNotes="";
   String codelstId="";	
   String adveventId= request.getParameter("adveventId");
   String mode=request.getParameter("mode");
   char strIndex='0';	
   String status="";	
   String enteredByName="";
   ArrayList outcomeIds=null;
   String outcomeId="";
   String studyId = (String) tSession.getValue("studyId");	
   String statDesc=request.getParameter("statDesc");
   String statid=request.getParameter("statid");	
   String accId = (String) tSession.getValue("accountId");
	String studyVer = request.getParameter("studyVer");
	String studyNum = request.getParameter("studyNum");


	//boolean genSchedule = true;
	   
	SchCodeDao cd = new SchCodeDao();
	
	String dCur = "";

	cd.getCodeValues("adve_type"); 

	if(mode.equals("N"))
	{
		dCur =  cd.toPullDown("adve_type",0);	
		String uName = (String) tSession.getValue("userName");
		String userIdFromSession = (String) tSession.getValue("userId");
	    enteredByName = uName;
		enteredBy=userIdFromSession;
	}
	else
	{
		adveventB.setAdvEveId(EJBUtil.stringToNum(adveventId));
		adveventB.getAdvEveDetails();
		codelstId=adveventB.getAdvEveCodelstAeTypeId();
		descripton=adveventB.getAdvEveDesc();
		startDt = adveventB.getAdvEveStDate();
		stopDt =  adveventB.getAdvEveEndDate();
	   	enteredBy= adveventB.getAdvEveEnterBy();
		outcomeString=adveventB.getAdvEveOutType();
		outcomeDt =adveventB.getAdvEveOutDate();
		outcomeNotes= adveventB.getAdvEveOutNotes();
		addInfoString=adveventB.getAdvEveAddInfo();
		notes=adveventB.getAdvEveNotes();
		dCur =  cd.toPullDown("adve_type",EJBUtil.stringToNum(codelstId));	
		userB.setUserId(EJBUtil.stringToNum(enteredBy));
	    userB.getUserDetails();
	    enteredByName = userB.getUserFirstName() + " " + userB.getUserLastName();
	}	
	String uName = (String) tSession.getValue("userName");
%>
<%
	int personPK = 0;
	String patientId = "";
	String dob = "";
	String gender = "";
	String genderId = "";
	String yob = "";
	int age = 0;
	 Calendar cal1 = new GregorianCalendar();
	String patProtId=(String) request.getParameter("patProtId");
	String study = (String) tSession.getValue("studyId");	

	String eventId=(String) request.getParameter("eventId");
	String pkey = request.getParameter("pkey");
	String eventName=request.getParameter("eventName");
	String visit=request.getParameter("visit");		

	person.setPersonPKId(EJBUtil.stringToNum(pkey));
	person.getPersonDetails();
	patientId = person.getPersonPId();
	genderId = person.getPersonGender();
	dob = person.getPersonDob();
	gender = codeLst.getCodeDescription(EJBUtil.stringToNum(genderId));
	if (gender==null){ gender=""; }
	yob = dob.substring(6,10);
	age = cal1.get(Calendar.YEAR) - EJBUtil.stringToNum(yob); 	



%>

	<P class = "userName"> <%= uName %> </P>
	<P class="sectionHeadings"> <%=MC.M_MngPatSch_AdvEvtDets%><%--Manage <%=LC.Pat_Patients%>
 >> Schedule >> Adverse Event Details*****--%> </P>
	<jsp:include page="patienttabs.jsp" flush="true"> 
	<jsp:param name="pkey" value="<%=pkey%>"/>
	<jsp:param name="patientCode" value="<%=patientId%>"/>
	<jsp:param name="patProtId" value="<%=patProtId%>"/>
	<jsp:param name="studyId" value="<%=study%>"/>
	<jsp:param name="studyVer" value="<%=studyVer%>"/>
	<jsp:param name="studyNum" value="<%=studyNum%>"/>
	</jsp:include>
	<br>
	<Form name="advevent" method=post action="updateadvevent.jsp" onsubmit="return validate(document.advevent)">
		<table width=100%>
			<tr>
			 	<td class=tdDefault width=20%>	<B> <%=LC.L_Patient_Id%><%--<%=LC.Pat_Patient%> Id*****--%>
 : <%=patientId%> </B>	</td>
				<td class=tdDefault width=20%>	<B> <%=LC.L_Age%><%--Age*****--%>: <%=age%> </B>	</td>
				<td class=tdDefault width=20%>	<B><%=LC.L_Gender%><%--Gender*****--%>: <%=gender%> </B>	</td>
				<td class=browserEvenRow width=20%>	</td>
			</tr>
		</table>
		<br>
		<br>
			<table width=94% cellspacing="0" cellpadding="0" >
				<tr> 
			    	<td class=tdDefault >
				       <%=LC.L_AdverseEvent_Type%><%--Adverse Event Type*****--%> <FONT class="Mandatory">* </FONT>
					</td>
					<td>	<%=dCur%> </td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>	
					<td class=tdDefault ><%=LC.L_Description%><%--Description*****--%> </td>
					<td class=tdDefault>	
						<TEXTAREA id=descripton name=descripton  rows=3 cols=30><%=descripton%></TEXTAREA>
					</td>
				</tr>
				<tr>
					<td class=tdDefault><%=LC.L_Start_Date%><%--Start Date*****--%></td>
<%-- INF-20084 Datepicker-- AGodara --%>
					<td class=tdDefault><INPUT type=text name=startDt class="datefield" size=10 READONLY value=<%=startDt%>></td>
				</tr>
				<tr>
					<td class=tdDefault><%=LC.L_Stop_Date%><%--Stop Date*****--%></td>
					<td class=tdDefault><INPUT type=text name=stopDt class="datefield" size=10 READONLY value=<%=stopDt%>></td>
				</tr>
				<tr> 
					<td><%=LC.L_Entered_By%><%--Entered By*****--%> <FONT class="Mandatory">* </FONT>
					</td>
					<td>
						<input name="death" type=hidden value=<%=death%>>
						<input name="eventId" type=hidden value=<%=eventId %>>
					    <input name="pkey" type=hidden value=<%=pkey%>>
						<input name="eventName" type=hidden value=<%=eventName%>>
					    <input name="visit" type=hidden value=<%=visit%>>
					    <input name="srcmenu" type=hidden value=<%=src%>>
					    <input name="adveventId" type=hidden value=<%=adveventId%>>
						<input name="mode" type=hidden value=<%=mode%>>	   	
						<input type=hidden name=enteredBy value=<%=enteredBy%>>
						<input type="hidden" name=studyId value=<%=studyId%>>
						<input type="hidden" name=statDesc value=<%=statDesc%>>
						<input type="hidden" name=statid value=<%=statid%>>
						<input type="hidden" name=patProtId value=<%=patProtId%>>
						<input type=hidden name=studyVer value=<%=studyVer%>>
						<input type=hidden name=studyNum value=<%=studyNum%>>
						<input type=hidden name=patientCode value=<%=patientId%>>
						<input type=text name=enteredByName readonly value="<%=enteredByName%>">
							<A HREF=# onClick=openwin1() ><%=LC.L_Select_User%><%--Select User*****--%></A> 
					</td>		
			  	</tr>
			</table>
			<table width=94% cellspacing="0" cellpadding="0" >
				<tr>
					<td COLSPAN=2><br>
						<p class = "sectionHeadings" ><%=LC.L_Outcome_Info%><%--Outcome Information*****--%></p><br>
					</td>
					<td>&nbsp;</td>
				</tr>
				<%SchCodeDao outcome = new SchCodeDao();	
				outcome.getCodeValues("outcome");  
				ArrayList codelstDescs=outcome.getCDesc();
				outcomeIds=outcome.getCId();
				String codelstdesc="";
				int length=codelstDescs.size();	
				%>
			   <input  type="hidden" name="outcomeString"> 	
				<% for (int i=0;i<length; i++) {
					codelstdesc=(String)  codelstDescs.get(i);	
					outcomeId=(String) outcomeIds.get(i).toString();
					if(mode.equals("M")){
						strIndex=outcomeString.charAt(i);
						if(strIndex=='1')
							{status="Checked=true" ;}
						else{status="" ;}
					}%>
					<tr>
						<td>
							<% if (i==0) {
								out.println(LC.L_Outcome_Type/*"Outcome Type"*****/);
							}
							%>
						</td>
						<td>
						   <input type="checkbox" name="outcomeType" value=<%=outcomeId%> <%=status%> onclick="checkoutcome(document.advevent,<%= i %>,<%=length%>)"> <%=codelstdesc%>
							<% if (i==0) {%>
<%-- INF-20084 Datepicker-- AGodara --%>							
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <%=LC.L_Date%><%--Date*****--%> &nbsp; <INPUT type=text name=outcomeDt class="datefield" size=10 READONLY value=<%=outcomeDt%>>

							<%}%>
						</td>
					</tr>
				<%}%>
				<tr>
					<td> &nbsp;
					</td>
					<td> &nbsp;
					</td>
				</tr>
				<tr>
			    	<td>
			    	<%=LC.L_Outcome_Notes%><%--Outcome Notes*****--%>
			    	</td>
			    	<td>
				    	<INPUT type=text name="outcomeNotes" size=30 value='<%=outcomeNotes %>'>
			    	</td>
				</tr>
			</table>


			<table width=94% cellspacing="0" cellpadding="0" >
				<tr>
					<td COLSPAN=2><br>
						<p class = "sectionHeadings" ><%=LC.L_Addl_Info%><%--Additional Information*****--%></p>
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<%SchCodeDao addInfo= new SchCodeDao();	
				addInfo.getCodeValues("adve_info");  
				codelstDescs=addInfo.getCDesc();
				codelstdesc="";
				length=codelstDescs.size();	
				%>
				<input type="hidden" name="addInfoString">
				<% for (int i=0;i<length; i++) {
					codelstdesc=(String)  codelstDescs.get(i);	
					if(mode.equals("M")){
						strIndex=addInfoString.charAt(i);	
					if(strIndex=='1')
						{status="Checked=true" ;}
					else{status="" ;}
					}%>
					<tr>
						<td>
							<input type="checkbox" name="addInfo" <%=status%> > <%=codelstdesc%>
						</td>
						<td>&nbsp </td>
					</tr>
				<%}%>
				<tr>
					<td>&nbsp </td>
					<td>&nbsp </td>
				</tr>
				<tr>		
					<td class=tdDefault valign=top><%=LC.L_Notes%><%--Notes*****--%> </td>
					<td class=tdDefault>	
						<br>
						<TEXTAREA id=notes name=notes rows=3 cols=30><%= notes%> </TEXTAREA>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>	 	

			

			</table>
			
			
			<table width=100%>
							<%
				if ((mode.equals("M") && (pageRight >= 6)) || (mode.equals("N") && (pageRight == 5  || pageRight == 7 ) ))
				{
				%>	
					<tr>
						<td class=tdDefault width=26%>
							<%=LC.L_EHypenSign%><%--E-Signature*****--%> <FONT class="Mandatory">* </FONT>
						</td >
						
						<td class=tdDefaultwidth=74%>
							<input type="password" name="eSign" maxlength="8" autocomplete="off">
							<button type="submit"><%=LC.L_Submit%></button>
						</td>
					</tr>
				<%
				}
				%>
			</table>			

			
			

	</Form>
	
  		<DIV class="myLinksmgPat"> 	
			<%
			UsrLinkDao usrLinkDao = ulinkB.getULinkValuesByAccountId(EJBUtil.stringToNum(accId),"lnk_adv");
		   	ArrayList lnksIds = usrLinkDao.getLnksIds(); 
			ArrayList lnksUris = usrLinkDao.getLnksUris();
			ArrayList lnksDescs = usrLinkDao.getLnksDescs();
			ArrayList lnksGrps = usrLinkDao.getLnksGrpNames();
			int len = lnksIds.size();
			int counter = 0;
	   		String lnkUri = "";
			String lnkDesc = "";
   			String oldGrp = "";	
	   		String lnkGrp = "";		

			%>
			<DIV class="accLinksmgpat"> 
			    <Form name="acclnkbrowser" method="post" action="" onsubmit="">
		    		<table width="190" cellspacing="0" cellpadding="0" border="0">
    			        <tr > 
    				        <td width=19 height=20> <img src= "../images/link_upleft.jpg" height=20> 
    				        </td>
    				        <td width="130" height=20 > 
    				            <P class = "sectionHeadings"> <%=LC.L_Important_Links%><%--Important Links*****--%> </P>
    				        </td>
    						<td width=22>&nbsp</td>
    				        <td  width=18 height=20 align="right"> <img src= "../images/link_upright.jpg" width=23 height=20> 
    				        </td>
    			        </tr>
    
    			        <%
    				    for(counter = 0;counter<len;counter++)
    					{		
    						lnkUri=((lnksUris.get(counter))==null)?"-":(lnksUris.get(counter)).toString();
    						lnkDesc=((lnksDescs.get(counter))==null)?"-":(lnksDescs.get(counter)).toString();
    						lnkGrp = ((lnksGrps.get(counter))==null)?"-":(lnksGrps.get(counter)).toString();
    						%>
    				        <% if ( ! lnkGrp.equals(oldGrp)){ %>
    					        <tr height = 15> 
    						        <td width=19>&nbsp; </td>
    						        <td width="130">&nbsp; </td>
    						        <td width=22>&nbsp; </td>
    						        <td  width=18 >&nbsp; </td>
    					        </tr>
    					        <tr > 
    						        <td width=19>&nbsp; </td>
    						        <td width="130"> <%= lnkGrp %> </td>
    						        <td width=22>&nbsp; </td>
    						        <td  width=18 > &nbsp;</td>
    					        </tr>
    						<%}%>
    				        <tr> 
    					        <td width=19>&nbsp; </td>
    				        	<td width="130"> <A href="<%= lnkUri%>" target="Information" onClick="openWin()" ><%= lnkDesc%></A>   </td>
    					        <td width=22>&nbsp; </td>
    					        <td  width=18 >&nbsp; </td>
    				        </tr>
    				        <%
    						oldGrp = lnkGrp;
    			 		}
    					%>
    			        <tr> 
    				        <td width=19 height=20 align="bottom"> <img src= "../images/link_lowleft.jpg" align="absbottom"> 
    				        </td>
    				        <td width="130" height=20>&nbsp;</td>
    				        <td width=22 height=20>&nbsp;</td>
    				        <td width=18 height=20 align="right"> <img src= "../images/link_lowright.jpg" align="absbottom" height=21> 
    				        </td>
    			        </tr>
					</table>
				</Form>
			</div>
		</div>
	<%
	} //end of if session times out
	else
	{
	%>
		<jsp:include page="timeout.html" flush="true"/>
	<%
	} //end of else body for page right

	%>
   	<div> 
    	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</div>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>   
</div>
</body>
</html>
