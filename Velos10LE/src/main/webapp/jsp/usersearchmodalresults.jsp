<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>
<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="codeLst" scope="session" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<%@ page import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,com.velos.esch.business.common.*"%>
<%@ page import="com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.*"%>
<%@ page import="com.velos.eres.web.user.UserJB"%>

<%!
private static final String EMPTY_STRING = "";
%>
<%
String skin = "default";
try {
	HttpSession tSession = request.getSession(false);
	if (!sessionmaint.isValidSession(tSession)) {
		out.println(EMPTY_STRING);
		return;
	}
} catch(Exception e) {
	e.printStackTrace();
}
%>

<script LANGUAGE="JavaScript" SRC="validations.js"></script>
<jsp:include page="sessionlogging.jsp" flush="true"/>

<%
response.setHeader("Cache-Control", "max-age=1, must-revalidate");
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1)
     ienet = 0; //IE
    else
	ienet = 1;
HttpSession tSession = request.getSession(true);
String userIdFromSession = (String) tSession.getAttribute("userId");

	//get parameters to make this window usable by all pages without adding code here
	 String genOpenerFormName = request.getParameter("genOpenerFormName") ;
	 String genOpenerUserIdFld = request.getParameter("genOpenerUserIdFld") ;
	 String genOpenerUserNameFld  = request.getParameter("genOpenerUserNameFld") ;

	if(genOpenerFormName == null){
	   genOpenerFormName="";
	   }
	if(genOpenerUserIdFld == null){
	   genOpenerUserIdFld="";
	   }
  	if(genOpenerUserNameFld == null){
	   genOpenerUserNameFld="";
    }


	String from=request.getParameter("from") ;


	String orgName ="";
	orgName=request.getParameter("accsites") ;
	orgName=(orgName==null)?"":orgName;

	//JM:
	String gName = "";
	gName = request.getParameter("usrgrps");
	gName=(gName==null)?"":gName;

	String stTeam = "";
	stTeam = request.getParameter("ByStudy");
	stTeam=(stTeam==null)?"":stTeam;

	String accountId = (String) tSession.getValue("accountId");
	String fromTxt ="";

	if(from == null){
	   from="";
	}

	if(from.equals("enroll"))
	{
		fromTxt= request.getParameter("fromTxt");

	}
// Getting the default site
	String userSiteId = "";
	int primSiteId = 0;
	int userId = 0;


    userId = EJBUtil.stringToNum(userIdFromSession);
	//userB.setUserId(userId);
	//userB.getUserDetails();
	userB=(com.velos.eres.web.user.UserJB)tSession.getAttribute("currentUser");
	userSiteId = userB.getUserSiteId();
	userSiteId=(userSiteId==null)?"":userSiteId;
	primSiteId= EJBUtil.stringToNum(userSiteId);


		String userJobType = "";
		String dJobType = "";
		String dAccSites ="";
		String jobVal = request.getParameter("jobType");

		if(jobVal==null)
			jobVal = "";
		String siteVal = request.getParameter("accsites");



		String mode="";
		mode=request.getParameter("mode");
		if (mode==null) mode="";



		if(siteVal == null)
			siteVal = "";
		CodeDao cd = new CodeDao();
		CodeDao cd2 = new CodeDao();
		cd2.getAccountSites(EJBUtil.stringToNum(accountId));
		cd.getCodeValues("job_type");
		userJobType =userB.getUserCodelstJobtype();


		if (jobVal.equals(""))
		  dJobType=cd.toPullDown("jobType");

		else
		  dJobType=cd.toPullDown("jobType",EJBUtil.stringToNum(jobVal),true);

//		if (siteVal.equals(""))
//		   dAccSites=cd2.toPullDown("accsites");


//		else
//		 if ((mode.length()==0) && (siteVal.length()==0)){
		 if( (siteVal.length()==0) && (mode.length()==0)){
		  dAccSites=cd2.toPullDown("accsites",primSiteId,true);
		  orgName=userSiteId;
		  mode="M";
		   }
		 else
		    dAccSites=cd2.toPullDown("accsites",EJBUtil.stringToNum(siteVal),true);







	String fname = LC.L_All;/*String fname = "All";*****/
	String lname = LC.L_All;/*String lname = "All";*****/
	String jobtype = LC.L_All ;/*String jobtype = "All" ;*****/
	String accsites = LC.L_All;/*String accsites = "All";*****/
	String usrgrps  = LC.L_All;/*String usrgrps  = "All";*****/
	String ByStudy = LC.L_All;/*String ByStudy = "All";*****/

	fname = StringUtil.htmlEncodeXss(StringUtil.trueValue(request.getParameter("fname")));
	lname = StringUtil.htmlEncodeXss(StringUtil.trueValue(request.getParameter("lname")));

	jobtype = request.getParameter("jobType");
	accsites =request.getParameter("accsites");

	String jobDesc = LC.L_All;/*String jobDesc = "All";*****/
	String siteDesc = LC.L_All;/*String siteDesc = "All";*****/
	String gDesc = LC.L_All ;/*String gDesc = "All" ;*****/
	String tDesc = LC.L_All;/*String tDesc = "All";*****/
	// Fix for bug #1547, getCodeDesc gets the description for the code from the Dao's ArrayLists. Changed for jobtype as well to reduce SQLs fired.
	jobDesc = cd.getCodeDesc(EJBUtil.stringToNum(jobtype));

	if (accsites==null) {
		siteDesc = cd2.getCodeDesc(primSiteId);
		//orgName=userSiteId;
		//mode="M";
		}
	else
		siteDesc = cd2.getCodeDesc(EJBUtil.stringToNum(accsites));

//JM: 23/08/05 to make drop down of group
	    String dUsrGrps="";
	    String selGroup="";
	    int ac=EJBUtil.stringToNum(accountId);
	    CodeDao cd1 = new CodeDao();
	    cd1.getAccountGroups(ac);

	    selGroup=request.getParameter("usrgrps");
	    if (selGroup==null)  selGroup="";
	    if (selGroup.equals(""))
	    dUsrGrps = cd1.toPullDown("usrgrps");
	    else
	    dUsrGrps = cd1.toPullDown("usrgrps",EJBUtil.stringToNum(selGroup),true);
	    usrgrps =request.getParameter("usrgrps");
	    gDesc=cd1.getCodeDesc(EJBUtil.stringToNum(usrgrps));


//JM: 24/08/05
	 String dStudy="";
	 String selstudy="";
	 int selstudyId=0;
  	 selstudy=request.getParameter("ByStudy") ;
		if(selstudy==null ){
		selstudyId=0 ;
		}else {
		selstudyId = EJBUtil.stringToNum(selstudy);
		}

	StudyDao studyDao = new StudyDao();
	String suserId = (String) tSession.getValue("userId");
	studyDao.getStudyValuesForUsers(suserId);
	ArrayList studyIds = studyDao.getStudyIds();
	ArrayList studyNumbers = studyDao.getStudyNumbers();

	//make dropdowns of study
	dStudy=EJBUtil.createPullDown("ByStudy",selstudyId,studyIds,studyNumbers);
	ByStudy =request.getParameter("ByStudy");
	//JM: get the study number from the selected study id
	tDesc=studyDao.getStudy(EJBUtil.stringToNum(ByStudy));

%>

<%
String uName = (String) tSession.getValue("userName");


	int pageRight = 0;
	String accId = "";
	String jobTyp = "";


	accId = request.getParameter("accountId");
	if(accId == null){
		accId = (String) tSession.getValue("accountId");
	}
	String ufname=request.getParameter("fname") ;
	if(ufname == null){
	   ufname="";
	}

	String ulname=request.getParameter("lname") ;
	if(ulname == null){
	   ulname="";
	}

	jobTyp=request.getParameter("jobType") ;


		///FOR HANDLING PAGINATION
		int iaccId = EJBUtil.stringToNum(accId);
		String pagenum = "";
		int curPage = 0;
		long startPage = 1;
		String stPage;
		long cntr = 0;
		pagenum = request.getParameter("page");

		if (pagenum == null)
		{
			pagenum = "1";
		}
		curPage = EJBUtil.stringToNum(pagenum);

		String orderBy = "";
		orderBy = request.getParameter("orderBy");

		String orderType = "";
		orderType = request.getParameter("orderType");

		String str2 ="";




			if (EJBUtil.isEmpty(orderBy))
			orderBy = "lower(usr_lastname)";



			if (EJBUtil.isEmpty(orderType))
			{
			orderType = "asc";
			}

//			String orderByColumnCode = "";
//			orderByColumnCode = request.getParameter("orderByColumnCode");
//			if (orderByColumnCode == null)
//			{
//			orderByColumnCode = "USR_LASTNAME"; //patient last name
//			}

		String str1 ="";
		String count1="";
		String count2="";
		String countSql = "";
		String formSql = "";


		String lWhere = null;
 	 	String fWhere = null;
		String oWhere = null;
		String rWhere = null;
		String aWhere = null;

		String gWhere = null;
		String sWhere = null;

 	 	StringBuffer completeSelect = new StringBuffer();

//JM: 24/08/05 modified

//KM:03/12/2008 --#U11
str1 = "SELECT distinct u.pk_user,u.USR_LASTNAME, "
			  + " (select SITE_NAME from er_site where PK_SITE  = FK_SITEID) as site_name, "
			  + " u.USR_FIRSTNAME, u.USR_MIDNAME, u.USR_LOGNAME, u.USR_STAT, u.USR_TYPE "
			  + " , a.ADDRESS, a.ADD_CITY, a.ADD_STATE, a.ADD_ZIPCODE, a.ADD_EMAIL, a.ADD_PHONE "
			  + " from  ER_USER u, ER_ADD a "
 	 		  + " Where u.fk_peradd = a.pk_add and u.USR_STAT in('A','B','D') and (u.USR_TYPE <>'X' and u.USR_TYPE <> 'P' ) and u.USER_HIDDEN <> 1";


			if ((orgName != null) && (! orgName.trim().equals("") && ! orgName.trim().equals("null")))
 			{
 				oWhere = " and UPPER(u.FK_SITEID) = UPPER(" ;
 				oWhere+=orgName;
 				oWhere+=")";
 			}
			if ((jobTyp != null) && (! jobTyp.trim().equals("") && ! jobTyp.trim().equals("null")))
 			{
 				rWhere = " and UPPER(u.FK_CODELST_JOBTYPE) = UPPER(" ;
 				rWhere+=jobTyp;
 				rWhere+=")";
 			}
 	 		if ((ufname != null) && (! ufname.trim().equals("") && ! ufname.trim().equals("null")))
 	 		{
 	 			fWhere = " and UPPER(u.USR_FIRSTNAME) like UPPER('" ;
 	 			fWhere+=ufname.trim();
 	 			fWhere+="%')";
 	 		}
 	 		if ((ulname != null) && (! ulname.trim().equals("")  && ! ulname.trim().equals("null")))
 	 		{
 	 			lWhere = " and UPPER(u.USR_LASTNAME) like UPPER('";
 	 			lWhere+=ulname.trim();
 	 			lWhere+="%')"	;
 	 		}


			//JM: 24/08/05
			//km-All the User's belonging to the selected group should be retreived, default or not doesn't matter. (Bug:2306)

			if ((gName != null) && (!gName.trim().equals("") && !gName.trim().equals("null")))
			{
				//gWhere =" and UPPER(u.fk_grp_default) = upper(";
				gWhere=" and u.pk_user in(select fk_user from er_usrgrp where upper(fk_grp)=upper(";
				gWhere +=gName;
				gWhere +="))";
			}

			if ((stTeam != null) && (!stTeam.trim().equals("") && !stTeam.trim().equals("null")))
			{

	   			sWhere =" and ( exists (select * from er_studyteam t where t.fk_user = pk_user and t.fk_study =  ";
				sWhere +=stTeam;
				sWhere +=" ) or pkg_superuser.F_Is_Superuser(pk_user, "+stTeam+") = 1  )";

			}



			completeSelect.append(str1);
			if (iaccId > 0){
				aWhere = " and u.fk_account = " + iaccId ;

				completeSelect.append(aWhere);
			}
			if (fWhere != null)
 	 		{
 	 			completeSelect.append(fWhere);
 	 		}

 	 		if (lWhere != null)
 	 		{
 	 			completeSelect.append(lWhere);
 	 		}

  	 		if (oWhere != null)
 	 		{
 	 			completeSelect.append(oWhere);
 	 		}

 	 		if (rWhere != null)
 	 		{
 	 			completeSelect.append(rWhere);
 	 		}

 			//JM: 24/08/05

			if (gWhere != null)
 	 		{
 	 			completeSelect.append(gWhere);
 	 		}


			if (sWhere != null)
 	 		{
 	 			completeSelect.append(sWhere);
 	 		}




			// add order by

			str2 = completeSelect.toString();
//			completeSelect.append(" ORDER BY lower(ER_USER.usr_lastname || ' ' || ER_USER.USR_FIRSTNAME ) ASC ");


			formSql = completeSelect.toString();
			count1 = "select count(*) from  ( " ;
			count2 = ")"  ;
			//System.out.println(formSql);
		countSql = count1 + str2 + count2 ;

		long rowsPerPage=0;
		long totalPages=0;
		long rowsReturned = 0;
		long showPages = 0;

	   boolean hasMore = false;
	   boolean hasPrevious = false;
	   long firstRec = 0;
	   long lastRec = 0;
	   long totalRows = 0;

		rowsPerPage =  Configuration.MOREBROWSERROWS ;
		totalPages =Configuration.PAGEPERBROWSER ;



       BrowserRows br = new BrowserRows();
       br.getPageRows(curPage,rowsPerPage,formSql ,totalPages,countSql,orderBy,orderType);
   	   rowsReturned = br.getRowReturned();
	   showPages = br.getShowPages();
	   startPage = br.getStartPage();
	   hasMore = br.getHasMore();
	   hasPrevious = br.getHasPrevious();
	   totalRows = br.getTotalRows();
	   firstRec = br.getFirstRec();
	   lastRec = br.getLastRec();







      	String usrLastName = null;
	    String grpName = null;
		String usrFirstName = null;
	    String usrMidName = null;
      	String siteName = null;
	    String jobType = null;
      	String usrId = null;
		int counter = 0;
		String oldGrp = null;
		String usrName= "";
		String usr_id = "";
		String orgNam = "";
		String type ="" ;	//jm
		String type1="";	//jm
	    String encodedName = "";
		String usrEmail = null;
		String usrPhone = null;
		String usrAddress = null;
		String usrCity = null;
		String usrState = null;
		String usrZipCode = null;


%>
  <Form name="search" method="post" action="usersearchdetails.jsp">
	<input type="hidden" name="orderBy" value="<%=orderBy%>">
	<input type="hidden" name="orderType" value="<%=orderType%>">
	<input type="hidden" name="page" value="<%=pagenum%>">
	<input type="hidden" name=fname value=<%=fname%>>
	<input type="hidden" name=lname value=<%=lname%>>
	<input type="hidden" name=jobType value=<%=jobtype%>>
	<input type="hidden" name=accsites value=<%=orgName%>>

	<input type="hidden" name=usrgrps value=<%=gName%>>
	<input type="hidden" name=ByStudy value=<%=stTeam%>>

	<input type="hidden" name="mode" value=<%=mode%>>
	<Input type="hidden" name="genOpenerFormName" value=<%=genOpenerFormName%>>
	<Input type="hidden" name="genOpenerUserIdFld" value=<%=genOpenerUserIdFld%>>
	<Input type="hidden" name="genOpenerUserNameFld" value=<%=genOpenerUserNameFld%>>

	<input type="hidden" name=from value=<%=from%>>

    <table width="100%" border=0>
      <tr>
      <%

		int i;

	//	out.print(lenUsers);

	if(fname.equals(""))
		fname = LC.L_All;/*fname = "All";*****/
	if(lname.equals(""))
		lname = LC.L_All;/*lname = "All";*****/
	if(jobDesc==null || jobDesc.equals(""))
		jobDesc = LC.L_All;/*jobDesc = "All";*****/
	if(siteDesc==null || siteDesc.equals(""))
		siteDesc = LC.L_All;/*siteDesc = "All";*****/
	if(gDesc==null || gDesc.equals(""))
		gDesc = LC.L_All;/*gDesc = "All";*****/
	if(tDesc==null || tDesc.equals(""))
		tDesc = LC.L_All;/*tDesc = "All";*****/


		%>
	<P class="defComments"><%=MC.M_SelFilters_UsrFirstName%><%--The Selected Filters are: User First Name*****--%>: <B><I><u><%=fname%></u></I></B>&nbsp&nbsp <%=LC.L_User_Last_Name%><%--User Last Name*****--%>: <B><I><u><%=lname%></u></I></B>&nbsp&nbsp <%=LC.L_Job_Type%><%--Job Type*****--%>: <B><I><u><%=jobDesc%></u></I></B>&nbsp&nbsp <%=LC.L_Organization_Name%><%--Organization Name*****--%>: <B><I><u><%=siteDesc%></u></I></B>
	&nbsp&nbsp <%=LC.L_Group_Name%><%--Group Name*****--%>:<B><I><u><%=gDesc%></u></I></B>&nbsp&nbsp <%=LC.L_Std_Team%><%--<%=LC.Std_Study%> Team*****--%>:<B><I><u><%=tDesc%></u></I></B></p>

      </tr>
	  </table>
<%-- Nicholas : Start --%>
	<table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl midAlign">
<%-- Nicholas : End --%>
	  <tr>
	  <th width="10%" >
	  <th width="23%" ><%=LC.L_First_Name%><%--First Name*****--%></font></th>
	  <th width="23%" ><%=LC.L_Last_Name%><%--Last Name*****--%></th>
	  <th width="22%" ><%=LC.L_Organization%><%--Organization*****--%></th>
	  <th width="22%" ><%=LC.L_User_Type%><%--User Type*****--%></th>
	  </tr>

      <%int count = 0;

		for(count = 1 ; count <= rowsReturned ; count++)
	  	{
			usrLastName= br.getBValues(count,"USR_LASTNAME");
			usrFirstName=br.getBValues(count,"USR_FIRSTNAME");
			usrId = br.getBValues(count,"pk_user");
			orgNam = br.getBValues(count,"site_name");

			type =br.getBValues(count,"usr_stat"); //jm
			type1 =br.getBValues(count,"usr_type"); //jm
			if (type.equals("A")){
			type=LC.L_Active_AccUser;/*type="Active Account User";*****/
			}
			else if (type.equals("B")){
			type=LC.L_Blocked_User;/*type="Blocked User";*****/
			}
			else if (type.equals("D")){
			type=LC.L_Deactivated_User;/*type="Deactivated User";*****/
			}
			/////

			type1 =  br.getBValues(count,"usr_type");//JM

			//Modified by Manimaran to display Deactivated Non system user in the browser
			if (type1.equals("N")){
				type =br.getBValues(count,"usr_stat");
				if(type.equals("D"))
					type=MC.M_DeactNon_SysUsr;/*type="Deactivated Non System User";*****/
				else
					type=MC.M_Non_SystemUser;/*type="Non System User";*****/
			}
			////

			if (orgNam == null){
				orgNam ="-";
			}

			usrEmail = br.getBValues(count, "add_email");
			usrPhone = br.getBValues(count, "add_phone");
			StringBuffer contactBuf = new StringBuffer("<b>Email/Phone:</b> ");
			StringBuffer emailBuf = new StringBuffer("");
			if (usrEmail != null) { emailBuf.append(usrEmail); }
			if (usrPhone != null) {
			    if (emailBuf.length() > 0) { emailBuf.append(" "); }
			    emailBuf.append(usrPhone);
			}
			contactBuf.append(emailBuf).append("<br><b>Address:</b> ");

			StringBuffer addrBuf = new StringBuffer();
			usrAddress = br.getBValues(count, "address");
			usrCity = br.getBValues(count, "add_city");
			usrState = br.getBValues(count, "add_state");
			usrZipCode = br.getBValues(count, "add_zipcode");
			if (usrAddress != null) { addrBuf.append(usrAddress); }
			if (usrCity != null) {
			    if (addrBuf.length() > 0) { addrBuf.append(" "); }
			    addrBuf.append(usrCity);
			}
			if (usrState != null) {
			    if (addrBuf.length() > 0) { addrBuf.append(" "); }
			    addrBuf.append(usrState);
			}
			if (usrZipCode != null) {
			    if (addrBuf.length() > 0) { addrBuf.append(" "); }
			    addrBuf.append(usrZipCode);
			}
			contactBuf.append(addrBuf);
			String contactStr = contactBuf.toString().replaceAll("'","[VELQUOTE]");

			if ((count%2)==0) {
	%>
      <tr class="browserEvenRow">
        <%
			}else{
		  %>
      <tr class="browserOddRow">
        <% } usrName = usrFirstName+ " " +usrLastName;

	encodedName = StringUtil.encodeString(usrName);
		   %>
          <td width="10%" align="center"><a href="#" onClick="fetchUserDetails('<%=request.getParameter("targetId")%>','<%=usrId%>');$j('#selectUserDialog' ).dialog('destroy');"><%=LC.L_Select%></a></td>
          <td width="23%"> <%= usrFirstName%></td>
		  <td width="23%"> <%=usrLastName%></td>
		  <td width="22%"> <%=orgNam%></td>
		  <td width="22%"> <%=type%></td>

      </tr>
      <%}%>
    </table>
	<table>
		<tr><td>
		<% if (totalRows > 0)
		{ Object[] arguments = {firstRec,lastRec,totalRows};
		%>
					<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%-- <font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
		<%} else {%>
			<font class="recNumber"> <%=MC.M_NoSrchResMatch_Criteria%><%--No search results matching given criteria found*****--%></font>
		<%}%>
		</td></tr>
	</table>
	<table align=center>
	<tr>
	<%
		if (curPage==1) startPage=1;
	    for ( count = 1; count <= showPages;count++)
		{


   			cntr = (startPage - 1) + count;
	 		if ((count == 1) && (hasPrevious))
			{
			  %>
				<td colspan = 2>
			  	<A href="javascript:void(0);" onclick="navigateUserSearchResults('usersearchmodalresults.jsp?from=<%=from%>&genOpenerFormName=<%=genOpenerFormName%>&genOpenerUserIdFld=<%=genOpenerUserIdFld%>&genOpenerUserNameFld=<%=genOpenerUserNameFld%>&fromTxt=<%=fromTxt%>&accountId=<%=accId%>&fname=<%=ufname%>&lname=<%=ulname%>&jobType=<%=jobTyp%>&accsites=<%=orgName%>&usrgrps=<%=gName%>&ByStudy=<%=stTeam%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&mode=<%=mode%>');">< Previous <%=totalPages%> > </A>
				&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<%
  			}	%>
		<td>
		<%
 		 if (curPage  == cntr)
		 {
	    	 %>
				<FONT class = "pageNumber"><%= cntr %></Font>
       		<%
       	}else
        {	%>
		<A href="javascript:void(0);" onclick="navigateUserSearchResults('usersearchmodalresults.jsp?from=<%=from%>&genOpenerFormName=<%=genOpenerFormName%>&genOpenerUserIdFld=<%=genOpenerUserIdFld%>&genOpenerUserNameFld=<%=genOpenerUserNameFld%>&fromTxt=<%=fromTxt%>&accountId=<%=accId%>&fname=<%=ufname%>&lname=<%=ulname%>&jobType=<%=jobTyp%>&accsites=<%=orgName%>&usrgrps=<%=gName%>&ByStudy=<%=stTeam%>&page=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&mode=<%=mode%>');"><%= cntr%></A>
       <%}%>
		</td>
		<%	}
		if (hasMore)
		{   %>
	   <td colspan = 3 align = center>&nbsp;&nbsp;&nbsp;&nbsp;
	   <A href="javascript:void(0);" onclick="navigateUserSearchResults('usersearchmodalresults.jsp?from=<%=from%>&genOpenerFormName=<%=genOpenerFormName%>&genOpenerUserIdFld=<%=genOpenerUserIdFld%>&genOpenerUserNameFld=<%=genOpenerUserNameFld%>&fromTxt=<%=fromTxt%>&accountId=<%=accId%>&fname=<%=ufname%>&lname=<%=ulname%>&jobType=<%=jobTyp%>&accsites=<%=orgName%>&usrgrps=<%=gName%>&ByStudy=<%=stTeam%>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&mode=<%=mode%>');">< Next <%=totalPages%>></A>
		</td>
		<%	}	%>
	   </tr>
	  </table>

  </Form>
