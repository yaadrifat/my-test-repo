<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Del_MstoneAppx%><%-- Delete Milestone Appendix*****--%></title>
<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>
<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.EJBUtil,com.aithent.audittrail.reports.AuditUtils,com.velos.eres.service.util.*"%>
<jsp:useBean id="mileApndxB" scope="request" class="com.velos.eres.web.mileApndx.MileApndxJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 

<% String src;
src= request.getParameter("srcmenu");

%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  

<BODY> 
<br>

<DIV class="formDefault" id="div1">
<% 
	String studyId= "";
	String mileApndxId= "";
	String selectedTab="";
	
HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))	{ 	
		mileApndxId= request.getParameter("mileApndxId");
		studyId= request.getParameter("studyId");
		selectedTab=request.getParameter("selectedTab");
		
		int ret=0;
		
		String delMode=request.getParameter("delMode");
	
		if (delMode.equals("null")) {
			delMode="final";
%>
	<FORM name="mileApndxDelete" id="mileapndxdelfrm" method="post" action="mileapndxdelete.jsp" onSubmit="if (validate(document.mileApndxDelete)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>
	
	<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
		<b><%=MC.M_PlsEtrEsign_Del%><%-- Please enter e-Signature to proceed with deletion*****--%> </b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
	
		</table>
			
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="mileapndxdelfrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>

 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
  	 <input type="hidden" name="mileApndxId" value="<%=mileApndxId%>">
  	 <input type="hidden" name="studyId" value="<%=studyId%>">

   	 <input type="hidden" name="selectedTab" value="<%=selectedTab%>">

	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {
			// Modified for INF-18183 ::: AGodara 
			ret =mileApndxB.removeMileApndx(EJBUtil.stringToNum(mileApndxId),AuditUtils.createArgs(tSession,"",LC.L_Milestones));  
			if (ret==-2) {%>
				<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_LnkOrFileNt_DelSucc%><%-- Link/File not deleted successfully*****--%></p>			
			<%}else { %>
				<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_LnkOrFile_DelSucc%><%-- Link/File deleted successfully*****--%></p>
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=mileapndx.jsp?srcmenu=<%=src%>&studyId=<%=studyId%>&selectedTab=<%=selectedTab%>">				
			<%}
			} //end esign
	} //end of delMode	
  }//end of if body for session 
else { %>
 <jsp:include page="timeout.html" flush="true"/> 
 <% } %>
 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
  
</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/> 
</div>

</BODY>
</HTML>
