<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="codeLst" scope="session" class="com.velos.eres.web.codelst.CodelstJB"/>
<%@ page language = "java" import = "java.util.*,com.velos.eres.web.user.UserJB, com.velos.eres.service.util.*"%>
<head>

<SCRIPT LANGUAGE="JavaScript" SRC="whichcss_skin.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="formjs.js"></SCRIPT>
<%--
// Removing prototype.js because it causes problems with new browsers (IE10+ and FF20+)
<SCRIPT LANGUAGE="JavaScript" SRC="js/velos/prototype.js"></SCRIPT>
 --%>
<SCRIPT LANGUAGE="JavaScript" SRC="js/dojo/dojo.js"></SCRIPT>
<script type="text/javascript" src="js/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<SCRIPT LANGUAGE="JavaScript" SRC="js/velos/ajaxengine.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="js/velos/onload.js"></SCRIPT>
<link rel="shortcut icon" href="favicon.ico" />

</head>
<jsp:include page="sessionwarning.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp"></jsp:include> 
<body onLoad="onLoad();">


<div id="notification" style="visibility:hidden"></div>
<script>toggleNotification('on',"");</script>
<%HttpSession tSession = request.getSession(true); 
tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))
	{


 
  
   UserJB userB = (UserJB) tSession.getValue("currentUser");      
    int userId=userB.getUserId();
  
   String accName = (String) tSession.getValue("accName");
   String accId = (String) tSession.getValue("accountId");
   
	accName=(accName==null)?"default":accName;
	String accSkinId = ""; 
	String usersSkinId = "";
	String userSkin = "";
	String accSkin = "";
	String skin = "default";
	accSkinId = 	(String) tSession.getValue("accSkin");
//	userB1.setUserId(userId);
//	userB1.getUserDetails();

	
	usersSkinId = userB.getUserSkin();
	userSkin= codeLst.getCodeCustomCol(EJBUtil.stringToNum(usersSkinId) );

	
	accSkin= codeLst.getCodeCustomCol(EJBUtil.stringToNum(accSkinId) );

	if (accSkin == null || accSkin.equals("") || accSkin.equals("default") ){
		
		accSkin = "accSkin";
		
	}
	else{
		
		
		skin = accSkin;
	}
	
	if (userSkin != null && !userSkin.equals("") ){
		
		skin = userSkin;
					
	}
	
	//	System.out.println("*******skinname...."+skin);

		
%>
<script> whichcss_skin("<%=skin%>");</script>
   
   <form name="include" method="POST">
   <input id="sessUserId" name="sessUserId" value="<%=userId%>" type="hidden">
   <input type="hidden" id="sessAccId" value="<%=accId%>">
   <input type="hidden" name="accSkin" id="accSkin" value="<%=skin%>">
   </form>
   
   <%}%>
   </body>
   </html>
   
<Link Rel=STYLESHEET HREF='./styles/velos_popup.css' type=text/css>
<script>
if (navigator.userAgent.indexOf("MSIE") > -1) {
	if (navigator.userAgent.indexOf("MSIE 10") > -1) {
		$j('<link rel="stylesheet" type="text/css" href="./styles/ie_10plus.css" >').appendTo("head");
	}
}
</script>