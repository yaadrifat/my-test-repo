<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.aithent.audittrail.reports.AuditUtils,com.velos.eres.service.util.*"%>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=MC.M_Bgt_LineItemDel%><%--Budget Line Item Delete*****--%></title>
<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>
<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.esch.business.common.*"%>
<jsp:useBean id="lineitemB" scope="request" class="com.velos.esch.web.lineitem.LineitemJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 

<% String src;
	src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  

<BODY> 
<br>

<DIV class="formDefault" id="div1">
<% 
String lineitemId= "";
String selectedTab = request.getParameter("selectedTab");
		
HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))	{

	lineitemId= request.getParameter("lineitemId");
	String budgetId = request.getParameter("budgetId");
	String bgtcalId = request.getParameter("bgtcalId");
	String budgetTemplate = request.getParameter("budgetTemplate");
	//ArrayList lineItemIds = new ArrayList();
	String[] lineItemIds= null ;
	

	int ret=0;
		
	String delMode=request.getParameter("delMode");
	
	if (delMode == null) {
		delMode="final";
%>
	<FORM name="lineitemdelete" id="lineitemdelfrm" method="post" action="lineitemdelete.jsp" onSubmit="if (validate(document.lineitemdelete)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
		<br><br>

	<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
		<b><%=MC.M_PlsEtrEsign_Del%><%--Please enter e-Signature to proceed with deletion*****--%> </b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
	
		</table>
		
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="lineitemdelfrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>

	<input type="hidden" name="delMode" value="<%=delMode%>">
	<input type="hidden" name="srcmenu" value="<%=src%>">
	<input type="hidden" name="lineitemId" value="<%=lineitemId%>">
	<input type="hidden" name="budgetId" value="<%=budgetId%>">  
	<input type="hidden" name="bgtcalId" value="<%=bgtcalId%>">  
	<input type="hidden" name="selectedTab" value="<%=selectedTab%>">
	<input type="hidden" name="budgetTemplate" value="<%=budgetTemplate%>">

	

	</FORM>
<%
	} else {
  

			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>

 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {

//			ret = lineitemB.lineitemDelete(EJBUtil.stringToNum(lineitemId));
			lineitemB.setLineitemId(EJBUtil.stringToNum(lineitemId));
			lineitemB.getLineitemDetails();
			lineitemB.setLineitemDelFlag("Y");
			lineitemB.setModifiedBy((String)tSession.getValue("userId"));
			//Modified for INF-18183 ::: Akshi
			ret = lineitemB.updateLineitem(AuditUtils.createArgs(session,"",LC.L_Budget));	
			
			//IA Bug  #2779 12.29.2006
			lineItemIds= new String[1];
			lineItemIds[0]=lineitemId ;
			//lineItemIds.add(lineitemId);
			LineitemDao lineDao = new LineitemDao();
			lineDao.clearSubTotals(lineItemIds);
			
			//end bug

  
			if (ret==-2) {%>
				<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_LineNotRem_FromSec%><%-- Line Item not removed from Section.*****--%> </p>			
			<%}else { 
				
				tSession.setAttribute("lineItemDeleted", "Y");  
				if (budgetTemplate.equals("S"))
					{%>
				<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_LineItem_RemFromSec%><%-- Line Item removed from Section.*****--%> </p>
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=studybudget.jsp?srcmenu=<%=src%>&budgetId=<%=budgetId%>&bgtcalId=<%=bgtcalId%>&pageMode=final&mode=M&selectedTab=<%=selectedTab%>&budgetTemplate=<%=budgetTemplate%>">				
				<%	}else{
			
			%>
				<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_LineItem_RemFromSec%><%-- Line Item removed from Section.*****--%> </p>
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=patientbudget.jsp?srcmenu=<%=src%>&budgetId=<%=budgetId%>&bgtcalId=<%=bgtcalId%>&pageMode=final&mode=M&selectedTab=<%=selectedTab%>&budgetTemplate=<%=budgetTemplate%>">				
			<%
					}
			}

			
			} //end esign

	} //end of delMode
	
  }//end of if body for session 
else { %>
 <jsp:include page="timeout.html" flush="true"/> 
 <% } %>
<div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
  
</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/> 
</div>

</body>
</HTML>


