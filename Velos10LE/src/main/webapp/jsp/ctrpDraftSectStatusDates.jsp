<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC" %>

<script>
$j(document).ready(function(){
	$j('#ctrpDraftJB\\.studyStopReason').keyup(function(){
		limitChars('ctrpDraftJB.studyStopReason', 4000, 'charlimitinfo');
	});
	limitChars('ctrpDraftJB.studyStopReason', 4000, 'charlimitinfo');
});

function limitChars(textid, limit, infodiv){
	
	var text = $(document.getElementById(textid)).value;
	var textlength = text.length;
	if(textlength > limit){
		var paramArray = [0];
		$j('#'+infodiv).html(getLocalizedMessageString("L_NoOfCharactersLeft",paramArray));
		$(document.getElementById(textid)).value = (text.substr(0,limit));
		return false;
	}
	else
	{
		var paramArray = [(limit - textlength)];
		$j('#'+infodiv).html(getLocalizedMessageString("L_NoOfCharactersLeft",paramArray));
		return true;
	}
}
</script>

<table>
	<tr>
		<td width="50%">
			<table border="0">
				<tr>
					<td align="right" width="30%">
						<%=LC.CTRP_DraftCurrTrialStatus%>
						<span id="ctrpDraftJB.trialStatus_error" class="errorMessage" style="display:none;"></span>
						<FONT id="draft_study_phase_menu_asterisk" style="visibility:visible" class="Mandatory">* </FONT>
					</td>
					<td width="1%" valign="top">&nbsp;</td>
					<td align="left" valign="top" >
						<s:property escape="false" value="ctrpDraftJB.draftTrialStatusMenu" />
					</td>
				</tr>
				<tr>
					<td align="right">
						<%=LC.CTRP_DraftCurrTrialStatusDate %>
						<span id="ctrpDraftJB.trialStatusDate_error" class="errorMessage" style="display:none;"></span>
						<FONT id="draft_pilot_asterisk" class="Mandatory">* </FONT>
					</td>
					<td valign="top">&nbsp;</td>
					<td valign="top" >
						<s:textfield name="ctrpDraftJB.trialStatusDate" id="ctrpDraftJB.trialStatusDate" cssClass="datefield" />
					</td>
				</tr>
				<tr id="ctrp_draft_purpose_other_row">
					<td align="right">
						<%=LC.CTRP_DraftWhyTrialStopped %>
						<span id="ctrpDraftJB.studyStopReason_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td>&nbsp;</td>
					<td >
						<s:textarea name="ctrpDraftJB.studyStopReason" id="ctrpDraftJB.studyStopReason" cols="40" rows="4"/>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td align="right">
						<div id="charlimitinfo"></div>
					</td>
					<td>&nbsp;</td>
				</tr>
					
				<tr>
					<td align="right" style="padding-bottom:10px">
						<%=LC.CTRP_DraftTrialStartDate %>
						<span id="ctrpDraftJB.trialStartDate_error" class="errorMessage" style="display:none;"></span>
						<FONT id="draft_pilot_asterisk" class="Mandatory">* </FONT>
					</td>
					<td valign="top">&nbsp;</td>
					<td valign="top" height="35px" >
						<s:textfield name="ctrpDraftJB.trialStartDate" id="ctrpDraftJB.trialStartDate" cssClass="datefield" />
						<s:radio name="ctrpDraftJB.trialStartType" id="ctrpDraftJB.trialStartType" 
							list="ctrpDraftJB.trialStartTypeList" listKey="radioCode" listValue="radioDisplay" 
							value="ctrpDraftJB.trialStartType" />
					</td>
				</tr>
				<tr>
					<td align="right" style="padding-bottom:10px">
						<%=LC.CTRP_DraftPrimaryCompletionDate %>
						<span id="ctrpDraftJB.primaryCompletionDate_error" class="errorMessage" style="display:none;"></span>
						<FONT id="draft_pilot_asterisk" class="Mandatory">* </FONT>
					</td>
					<td valign="top">&nbsp;</td>
					<td valign="top" height="35px">
						<s:textfield name="ctrpDraftJB.primaryCompletionDate" id="ctrpDraftJB.primaryCompletionDate" cssClass="datefield" />
						<s:radio name="ctrpDraftJB.primaryCompletionType" id="ctrpDraftJB.primaryCompletionType" 
							list="ctrpDraftJB.primaryCompletionTypeList" listKey="radioCode" listValue="radioDisplay" 
							value="ctrpDraftJB.primaryCompletionType" />
					</td>
				</tr>
			</table>
		</td>
		<td width="50%">
			<table border="0">
				<tr>
					<td align="right" width="30%">
						<%=LC.L_CurStd_Status%>
					</td>
					<td width="1%">&nbsp;</td>
					<td height="30px" >
						<s:textfield name="ctrpDraftJB.currStudyStatus" cssClass="readonly-input" size="50" readonly="true" />
					</td>
				</tr>
				<tr>
					<td align="right">
						<%=LC.L_CurStat_Date %>
					</td>
					<td>&nbsp;</td>
					<td height="30px" >
						<s:textfield name="ctrpDraftJB.statusValidFrm" cssClass="readonly-input" size="50" readonly="true" />
					</td>
				</tr>
				<tr><td colspan="3" height="65px">&nbsp;</td></tr>
				<tr>
					<td align="right">
						<%=MC.M_DtStd_Active %>
					</td>
					<td>&nbsp;</td>
					<td height="30px" >
						<s:textfield name="ctrpDraftJB.studyActiveDate" cssClass="readonly-input" size="50" readonly="true" />
					</td>
				</tr>
				<tr>
					<td align="right">
						<%=MC.M_DtStdClosed_Perm %>
					</td>
					<td>&nbsp;</td>
					<td height="30px" >
						<s:textfield name="ctrpDraftJB.studyEndDate" cssClass="readonly-input" size="50" readonly="true" />
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

