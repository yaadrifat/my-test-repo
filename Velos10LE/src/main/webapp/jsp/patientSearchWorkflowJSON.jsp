<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8");
response.setContentType("application/json");%>
<%@ page import="com.velos.eres.web.user.UserJB,org.json.*,com.velos.eres.business.common.TeamDao,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*, com.velos.eres.business.common.PatSearchDAO, com.velos.service.*, com.velos.remoteservice.*, com.velos.remoteservice.demographics.*" %>
<jsp:useBean id="sessionmaint" scope="session"
	class="com.velos.eres.service.util.SessionMaint" />
<jsp:useBean id="commonB" scope="page"
	class="com.velos.eres.web.common.CommonJB" />
<jsp:useBean id="accB" scope="request"
	class="com.velos.eres.web.account.AccountJB" />
<jsp:useBean id="person" scope="request"
	class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="userBFromSession" scope="page"
	class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="studyRights" scope="page"
	class="com.velos.eres.web.studyRights.StudyRightsJB" />
<%
	final String METHOD_SEARCH_PAT_LIST = "searchPatList";
	final String METHOD_SEARCH_PAT_IN_STUDY = "searchPatInStudy";
	final String METHOD_SEARCH_PAT_IN_DB = "searchPatInDB";
	String returnString = null;
	String errorString = "";

	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{
		int account = EJBUtil.stringToNum((String) tSession.getAttribute("accountId"));
		String method = request.getParameter("method");
		if (method == null) method = "";
		/*
		We support two modes of search:
			"list" is a name-based search that returns minimal
			data for a list of patients.

			"single" is a search that returns
			detailed information about a single patient.
		*/


		if (method.equalsIgnoreCase(METHOD_SEARCH_PAT_LIST)){
			Date DOB = DateUtil.stringToDate(request.getParameter("dob"), "MM/dd/yyyy");
			String facility = request.getParameter("facility");
			String patId = request.getParameter("patId");
			try{
				Map searchParameters = new HashMap();
				searchParameters.put("facility", facility);
				searchParameters.put("patId", patId);
			  	//find the remote service
			  	String accountNumStr = (String)tSession.getAttribute("accountId");
			  	int accountNo = EJBUtil.stringToNum(accountNumStr);
			  	IDemographicsService demographicsService = DemographicsServiceFactory.getService(accountNo);
			  	try {
					JSONArray jsPatList = new JSONArray();
					List patients = demographicsService.getPatientDemographics(searchParameters);

					for (int patCnt = 0; patCnt < patients.size(); patCnt++){
						IPatientDemographics patient = (IPatientDemographics)patients.get(patCnt);

						JSONObject jsPatient = new JSONObject(patient);
						//adjust dates from java date to milliseconds from epoch...
						//JSONObject serialization of date incompatible with javscript Date object

						if (patient.getDateOfDeath() != null){
							jsPatient.put("dateOfDeath", patient.getDateOfDeath().getTime());
						}
						if (patient.getDOB() != null){
							jsPatient.put("DOB", patient.getDOB().getTime());
						}
						jsPatient.put("searchIndex", patCnt);
						jsPatient.put("patId", patient.getMRN());
						jsPatient.put("isRemote", true);
						jsPatList.put(jsPatient);


					}
					tSession.setAttribute("jsPatList", jsPatList);
					returnString = jsPatList.toString();


			   	} catch (PatientNotFoundException e) {
				    errorString = LC.Pat_Patient+" Not Found.";
			   	}

			}
			catch(Throwable t){
				errorString = "Oops. An error has occurred searching for "+LC.Pat_Patient+" in "+LC.Std_Study_Lower;
				Rlog.fatal("patSearchWorkflowJSON","error has occurred searching for patient in "+LC.Std_Study_Lower+": " + t.getMessage());
			}
		}

	/*
		if (method.equalsIgnoreCase(METHOD_SEARCH_PAT_IN_STUDY)){
			String facility = request.getParameter("facility");
			String patId = request.getParameter("patId");
			String studyId = request.getParameter("studyId");

			try{
				PatSearchDAO psearch = new PatSearchDAO();
				//retrieve a list of person primary keys that
				List patients =
					psearch.searchPatInStudy(
						patId,
						EJBUtil.stringToNum(facility),
						account,
						EJBUtil.stringToNum(studyId));

				if (patients != null && patients.size() > 0){

					//instantiate the person bean so that we can check access rights
					String userIdFromSession = (String) tSession.getAttribute("userId");
					userBFromSession = (UserJB) tSession.getAttribute("currentUser");

					int usrGroup =EJBUtil.stringToNum( userBFromSession.getUserGrpDefault());



					JSONArray jsPatients = new JSONArray();

					for (int x=0; x < patients.size(); x++){
						PatSearchDAO.PatientHolder patient = (PatSearchDAO.PatientHolder)patients.get(x);
						int patDataDetail =
							person.getPatientCompleteDetailsAccessRight(
									EJBUtil.stringToNum(userIdFromSession),
									usrGroup,
									patient.getPk_per().intValue() );

						JSONObject jsPatient = new JSONObject(patient);

						jsPatient.put("searchIndex", x);
						jsPatient.put("primaryFacility", facility);

						if (patDataDetail < 4){
							jsPatient = scrubPatientDetails(jsPatient);
						}
						//if user doesn't have access rights to view PHI, blank it out


						jsPatients.put(jsPatient);
					}
					returnString = jsPatients.toString();
				}
				else{
					returnString = "";
				}
			}
			catch(Throwable t){
				errorString = "Oops. An error has occurred searching for patient in "+LC.Std_Study_Lower+";
				Rlog.fatal("patSearchWorkflowJSON","error has occurred searching for patient in "+LC.Std_Study_Lower+": " + t.getMessage());
			}
		}

		*/

		if (method.equalsIgnoreCase(METHOD_SEARCH_PAT_IN_DB)){
			String facility = request.getParameter("facility");
			String patId = request.getParameter("patId");
			String studyId = request.getParameter("studyId");
			try{
				String userIdFromSession = (String) tSession.getAttribute("userId");
				PatSearchDAO psearch = new PatSearchDAO();
				//retrieve a list of person primary keys that
				List patients =
					psearch.searchPatInDB(
						EJBUtil.stringToNum(userIdFromSession),
						patId,
						EJBUtil.stringToNum(facility),
						account,
						EJBUtil.stringToNum(studyId));
				if (patients != null && patients.size() > 0){
					//get the user's study team rights


					//GET STUDY TEAM RIGHTS
					//code copied from studycentricpatentroll.jsp
					TeamDao teamDao = new TeamDao();
					teamDao.getTeamRights(EJBUtil.stringToNum(studyId), EJBUtil.stringToNum(userIdFromSession));
					ArrayList tId = teamDao.getTeamIds();
					int managePatientTeamRights = 0;
					if (tId.size() == 0) {
						managePatientTeamRights=0 ;
					}
					else{
						studyRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));

						ArrayList teamRights ;
						teamRights  = new ArrayList();
						teamRights = teamDao.getTeamRights();

						studyRights.setSuperRightsStringForStudy((String)teamRights.get(0));
						studyRights.loadStudyRights();

						if ((studyRights.getFtrRights().size()) == 0){
							managePatientTeamRights= 0;
						}else{
							managePatientTeamRights = Integer.parseInt(studyRights.getFtrRightsByValue("STUDYMPAT"));


						}

					}
					//instantiate the person bean so that we can check access rights
					userBFromSession = (UserJB) tSession.getAttribute("currentUser");
					int usrGroup =EJBUtil.stringToNum( userBFromSession.getUserGrpDefault());

					JSONArray jsPatients = new JSONArray();
					for (int x=0; x < patients.size(); x++){

						PatSearchDAO.PatientHolder patient = (PatSearchDAO.PatientHolder)patients.get(x);
						//used to check whether the patient details need to be scrubbed
						int patDataDetail =
							person.getPatientCompleteDetailsAccessRight(
									EJBUtil.stringToNum(userIdFromSession),
									usrGroup,
									patient.getPk_per().intValue());

						//if user site rights == 0 if enrolled don't show
						//unless user has and have team access and the patient is enrolled.
						if (patient.getUserSiteRights() == 0){
							if(managePatientTeamRights == 0 ||
									!patient.isOnStudy()){
								break;
							}

						}


						JSONObject jsPatient = new JSONObject(patient);
						jsPatient.put("searchIndex", x);
						jsPatient.put("primaryFacility", facility);
						jsPatient.put("isRemote", false);
						if (patDataDetail < 4){
							jsPatient = scrubPatientDetails(jsPatient);
						}

						jsPatients.put(jsPatient);
					}
					returnString = jsPatients.toString();
				}
				else{
					returnString = "";
				}
			}
			catch(Throwable t){
				errorString = "Oops. An error has occurred searching for "+LC.Pat_Patient_Lower+" in database";
				Rlog.fatal("patSearchWorkflowJSON","error has occurred searching for "+LC.Pat_Patient_Lower+" in database: " + t.getMessage());
			}
		}
	}
	else{
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");

	}


%><%=returnString%><%!
public String clean(String input){
	if (input == null){
		return "";
	}
	return input;
}

public JSONObject scrubPatientDetails(JSONObject jsPatient) throws JSONException{
	jsPatient.put("firstName", "*");
	jsPatient.put("lastName", "*");
	jsPatient.put("DOB", "*");
	return jsPatient;
}

public String getCodePK(
		String codeType,
		String remoteCode,
		ArrayList keyList,
		ArrayList remoteCodeList,
		ArrayList typeList){

	for (int x = 0; x < keyList.size(); x++){
		String type = (String)typeList.get(x);
		if (type.equalsIgnoreCase(codeType)){
			String testRemoteCode = (String)remoteCodeList.get(x);
			if (remoteCode.equalsIgnoreCase(testRemoteCode)){
				return (String)keyList.get(x);
			}
		}
	}
	return null;
}

/*
public JSONObject getInputParams(HttpServletRequest request){
	BufferedReader reader = request.getReader();
	String line = null;
	StringBuilder requestContent = new StringBuilder();
	while((line = reader.readLine()) != null){
		requestContent.append(line);
	}
	JSONObject obj = new JSONObject(requestContent.toString());
	return obj;
}
*/
%>