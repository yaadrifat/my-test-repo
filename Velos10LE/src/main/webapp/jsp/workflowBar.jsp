<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ page import="com.velos.eres.gems.business.*"%>
<%@page import="com.velos.eres.widget.service.util.FlxPageCache"%>
<%@page import="org.apache.axis.types.Entities"%>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC, com.velos.eres.service.util.WorkflowUtil" %>
<%@ page import="com.velos.eres.service.util.StringUtil" %>
<%@page import="com.velos.eres.web.study.StudyJB"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.service.util.CFG"%>
<%
HttpSession tSession = request.getSession(false);
if (!sessionmaint.isValidSession(tSession))
	return;

if (!"Y".equals(CFG.Workflows_Enabled)) {
	return;
}

String wfPageId = (String) request.getParameter("wfPageId");
int entityId = 0;
String workflowTypes = null, workflowFormName = null;

if (!StringUtil.isEmpty(wfPageId)) {
	entityId = WorkflowUtil.getEntityId(wfPageId, tSession);
	workflowTypes = WorkflowUtil.getWorkflowTypes(wfPageId);
	workflowFormName = WorkflowUtil.getWorkflowFormName(wfPageId);
	workflowFormName = (StringUtil.isEmpty(workflowFormName)? "" : workflowFormName);
}

if (entityId < 1 || StringUtil.isEmpty(workflowTypes)){
	return;
}
%>
<%if ("Y".equals(CFG.Workflows_Enabled)) {%>
	<jsp:include page="workflows.jsp" flush="true">
		<jsp:param name="entityId" value="<%=entityId%>" />
		<jsp:param name="workflowTypes" value="<%=workflowTypes%>" />
		<jsp:param name="params" value="<%=entityId%>" />
		<jsp:param name="wfPageId" value="<%=wfPageId%>" />
	</jsp:include>
<%}%>
<script>
var workflowBarFunctions = {
	createWorkflowDialogBar: {}
};

workflowBarFunctions.createWorkflowDialogBar = function (){
	var workflowInstanceId = 0, prevWorkflowInstanceId = 0;

	for (var indx=0; indx < (workflowFunctions.workflowInstances).length; indx++){
		workflowInstanceId = workflowFunctions.workflowInstances[indx];
		prevWorkflowInstanceId = (indx == 0)? 0 : workflowFunctions.workflowInstances[indx-1];

		if (prevWorkflowInstanceId == 0){
			$j('#workflowTasks'+ workflowInstanceId +'Div').dialog({
				width: $j('#workflowTab').width()-1,
				minHeight: 50,
				closeOnEscape: false,
				resizable: false,
				closeText:'',
				open: function(event, ui) { $j(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').hide(); }
			}).dialog("widget").position({
				my: 'left top', 
				at: 'left top',
				of: $j('#workflowTab')
			});
		} else {
			$j('#workflowTasks'+ workflowInstanceId +'Div').dialog({
				width: $j('#workflowTab').width()-1,
				minHeight: 50,
				closeOnEscape: false,
				resizable: false,
				closeText:'',
				open: function(event, ui) { $j(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').hide(); }
			}).dialog("widget").position({
				my: 'top', 
				at: 'bottom',
				of: $j('#workflowTasks'+prevWorkflowInstanceId+'Div')
			});
			
		}
	}
	return false;
};

$j(document).ready(function() {
	workflowBarFunctions.createWorkflowDialogBar();
});
</script>
