<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*,com.velos.eres.service.util.*" %>

	<%
		String formID = "";
		String displayESign = "";
		String showDiscard = "";
		String showSubmit = "";
		String noBR = "";
		String showSave = "";

		formID = request.getParameter("formID");
		displayESign = request.getParameter("displayESign");
		showDiscard= request.getParameter("showDiscard");
		showSubmit = request.getParameter("showSubmit");
		noBR = request.getParameter("noBR");
		showSave = request.getParameter("showSave");
		    
		if (showSubmit == null)
			showSubmit = "";
		
		if (showSave == null)
			showSave = "";

		if (StringUtil.isEmpty(displayESign))
		{
			displayESign="Y";
		}


		if (StringUtil.isEmpty(showDiscard))
		{
			showDiscard="N";
		}

		if (!showDiscard.equals("LKP") && !("Y".equals(noBR))) {
		%>
	    <%}%>
<script>
var lastTimeSubmitted = 0;
function fnOnceEnterKeyPress(e) {
	try {
        if (e.keyCode == 13 || e.keyCode == 10) {
            var thisTimeSubmitted = new Date();
            if (!lastTimeSubmitted) { return true; }
            if (!thisTimeSubmitted) { return true; }
            if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) {
                return false;
            }
            lastTimeSubmitted = thisTimeSubmitted;
        }
	} catch(e) {}
	return true;
}

function refreshPanel()
{
	parent.document.getElementById("preview").contentWindow.location.reload();
}

function fnClickOnce(e) {
	try {
		<%if(formID!=null && formID.equalsIgnoreCase("addevtvisitfrm")){%>
			if (validate(document.addeventvisit)== false) {
				setValidateFlag('false'); return false; 
			} else {
				setValidateFlag('true'); return true;
			}
		<%}%>
        var thisTimeSubmitted = new Date();
        if (!lastTimeSubmitted) { return true; }
        if (!thisTimeSubmitted) { return true; }
        if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) {
            return false;
        }
        lastTimeSubmitted = thisTimeSubmitted;
	} catch(e) {}
    return true;
}

</script>


<table id="submitBarTable" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	  <% if (displayESign.equals("Y")) { %>
	  <td width="50%" align="right">  	
		 <span id="eSignMessage"></span>
		 <%=LC.L_Esignature %><%-- e-Signature*****--%> <FONT class="Mandatory">* </FONT>&nbsp;
	   </td>
	  <% } %>

	  <% if (displayESign.equals("Y")) { %>
	  <td width="19%" >
		<%-- <input type="password" name="eSign" id="eSign" maxlength="8"
		   onkeyup="ajaxvalidate('misc:'+this.id,4,'eSignMessage','Valid e-Sign','Invalid e-Sign','sessUserId')" onkeypress="return fnOnceEnterKeyPress(event)">*****--%>
		<input type="password" name="eSign" id="eSign" maxlength="8" autocomplete="off"
		   onkeyup="ajaxvalidate('misc:'+this.id,4,'eSignMessage','<%=LC.L_Valid_Esign %>','<%=LC.L_Invalid_Esign %>','sessUserId')" onkeypress="return fnOnceEnterKeyPress(event)">
	  </td>
	  <% } %>
		<td width="2%" >&nbsp;&nbsp;</td>
	  <% if(!showSubmit.equals("N")) {%>
	  <td width="19%" >
		<button type="submit" id="submit_btn" onclick="if(fnClickOnce(event)){ refreshPanel();return true } else{ return false }" ondblclick="return false"><%=LC.L_Submit%></button>
	  </td>
	  <!-- added to fix the bug id :20539 :submit should be save to avoid confusion -->
	  <%} else if (showSubmit.equals("N") && showSave.equals("Y")) { %>
	  <td width="19%" >
		<button type="submit" id="submit_btn" onclick="return fnClickOnce(event)" ondblclick="return false"><%=LC.L_Save%></button>
	  </td>
		<%}%>
	  

	<!--KM -->  
		<td width="10%" >
	    	<% if (showDiscard.equals("Y")) { %>
	    	<button type="button" onClick="window.close()"><%=LC.L_Close%></button>
			<% } else if (showDiscard.equals("LKP")) { %>
			<button type="button" onClick="window.parent.close();"><%=LC.L_Close%></button>
			<%	} else if (showDiscard.equals("Org")) { %>
			<button type="button" onClick="window.close();window.opener.location.reload();"><%=LC.L_Close%></button>
		<%}%>

	  </td>
	<!--KM -->

  </tr>
</table>

	  <script>
	  <%if(formID!=null && !(formID.equalsIgnoreCase("addevtvisitfrm"))){%>
	  	linkFormSubmit('<%=formID%>');
	  <%}%>

	  </script>
