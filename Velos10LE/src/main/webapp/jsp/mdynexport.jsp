<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<html>
<SCRIPT LANGUAGE="JavaScript" src="formjs.js"></SCRIPT>
<script  charset='utf-8'>


function openFilterView() {

   windowName = window.open("mdynfilterview.jsp", "AdHocExpFilPreview","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=850,height=750 top=100,left=100");
   windowName.focus();
   return true;

}

function jumpToExp(formobj,p_formType)
{
	if (p_formType == 'P')
	{
		if (!validate_col('patcode',formobj.patcode)) return false;
	}

	formobj.action="dynreplookup.jsp";
	//formobj.submit();
}

function validate(formobj)
{
	if (formobj.lookupMode.value == 'yes' &&  formobj.formType.value == 'P')
	{
		if (!validate_col('patcode',formobj.patcode))
		{
		 return false;
		 }
		formobj.action="dynreplookup.jsp";
	}

}

function changeFilterExp(formobj)
{
	if (typeof(formobj.patRecStartCounterStr)!="undefined")
 		formobj.patRecStartCounterStr.value = "0";
	formobj.action="dynreplookup.jsp&moduleName="+formobj.calledfrom.value;
	//formobj.submit();
}

function changeFilterLinearExp(formobj)
{
	formobj.action="mdynexport.jsp?exp=L&calledfrom="+formobj.calledfrom.value;
	//formobj.submit();
}

function switchToDownload(formobj)
{
	formobj.action = "/velos/servlet/Download";
	formobj.submit();
}


</script>
<head>


	<title><%=LC.L_Form_RptPreview%><%--Form Report Preview*****--%></title>
	<%@ page language = "java" import = "java.util.*,java.sql.*,com.velos.eres.web.dynrep.holder.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>
	<%@ page language = "java" import = "java.text.DecimalFormat"%>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="dynrepB" scope="page" class="com.velos.eres.web.dynrep.DynRepJB" />
	<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
	<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />

<%-- Nicholas : Start --%>
<%--<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>--%>
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%-- Nicholas : End --%>
<% String src,sql="",tempStr="",sqlStr="",name="",selValue="",repFldStr="",fldKey="";
   int strlen=0,firstpos=-1,secondpos=-1,patId=0,studyId=0;
//src= request.getParameter("srcmenu");
%>
<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1)
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {
%>
<body style="overflow:scroll;">
<%
	} else {
%>
<body style="overflow:auto; height:98%;">
<%
	}
%>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<form  name="preview" onSubmit="return validate(document.preview)" method="post">
<%


	int seq=0;
	String eolString="";
	String repFooter="",formId="",formName="";

	String value="",patientID="",studyNumber="",orderStr="",order="",filter="",dataOrder="",fltrDD="",prevSecId="",mapSecId="",headAlign="",footAlign="";
 	String allFformIdStr = "";
 	boolean repeatFormHeader = true;

 	StringBuffer sbOutput = new StringBuffer();
 	StringBuffer sbOutputMain = new StringBuffer();
 	StringBuffer sbRow =  new StringBuffer();

	String respAccessRightWhereClause = "";
	int personPk=0,studyPk=0,repId=0,index=-1;
	DynRepDao dynDao=new DynRepDao();
	DynRepDao dynDaoFltr=new DynRepDao();
	ArrayList selectedfld=null;
	ArrayList dataList=new ArrayList() ;
	SummaryHolder repSummaryHolder = new SummaryHolder ();
	ArrayList tempVal=new ArrayList();
	ArrayList tempList=new ArrayList();
	ArrayList lfltrIds= new ArrayList();
	ArrayList lfltrNames= new ArrayList();
	ArrayList lfltrStrs= new ArrayList();
	int formSequenceCounter = 0;

	String rightsWhereClause = "";
	boolean bIsEmptySQL = false;



	//for repeat column crash, 04/06/06
	int repColCount  = 0;
	int repColDataUpperLimit = 0;
	int repColCharAvailable = 0;

		ArrayList multiFltrIds=new ArrayList();
	ArrayList multiFltrNames=new ArrayList();


	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))
	{
		%>
		<jsp:include page="sessionlogging.jsp" flush="true"/>
		<%
	String calledFrom = request.getParameter("calledfrom");
	if(EJBUtil.isEmpty(calledFrom)){
		calledFrom = LC.L_Rpt_Central;
	}
	String sessId = tSession.getId();
	if (sessId.length()>8) { sessId = sessId.substring(0,8); } // Keep the leading digits only
	sessId = Security.encrypt(sessId);
	char[] chs = sessId.toCharArray();
	StringBuffer sb = new StringBuffer();
	DecimalFormat df = new DecimalFormat("000");
	for (int iX=0; iX<chs.length; iX++) {
	    sb.append(df.format((int)chs[iX]));
	}
	String keySessId = "&key="+sb.toString();
	sessId = sb.toString();

	 String defUserGroup = (String) tSession.getAttribute("defUserGroup");

	ReportDataHolder rdHolder = new ReportDataHolder(); // report data holder object
	String fileNameForLinear = "";



	ReportHolder container=null;
	String from=request.getParameter("from");
	String patcodeFilter = request.getParameter("patcode");
	String studyNumFilter = request.getParameter("studynumber");

	if (StringUtil.isEmpty(patcodeFilter))
	{
		patcodeFilter = "";
	}

	if (StringUtil.isEmpty(studyNumFilter))
	{
		studyNumFilter = "";
	}



		/** changed by Sonia Abrol, 02/21/06, to make patient adHoc patient context sensitive, formPatient parameter
	 will be passed from patient forms */

	String formPatient = request.getParameter("formPatient");
	if (StringUtil.isEmpty(formPatient))
	{
		formPatient = "";
	}


	String fmt = "";

	fmt =request.getParameter("exp");
	from=(from==null)?"":from;
	String fltrId=request.getParameter("fltrId");
	if (fltrId==null) fltrId="";
	String repIdStr=request.getParameter("repId");
	String containerReportId = "";

	if (repIdStr==null) repIdStr="";

	int userId=EJBUtil.stringToNum((String)tSession.getAttribute("userId"));

	String fileNamePrefixForLiear = userId + "_" + repIdStr;

	fileNameForLinear = EJBUtil.getUniqueFileName(fileNamePrefixForLiear ,".vel" );

	respAccessRightWhereClause = " and ( pkg_filledform.fn_getUserAccess (fk_form," + userId +",creator) > 0 )"	;

	String accId = (String) tSession.getValue("accountId");

	String fldWidthStr = "";

	//for lookup
	String lookupMode = "";
	Vector urlKeywords = new Vector(20);
	ArrayList urlFields = new  ArrayList();

	String outputStr = "";
	StringTokenizer st_sep=null;
	String fkeyword_str=request.getParameter("fkeyword_str");
	String lookupformName = request.getParameter("lookupformName");


	if (lookupformName==null) lookupformName="";
	if (fkeyword_str==null) fkeyword_str="";

	lookupMode = request.getParameter("lookupMode");
	if (StringUtil.isEmpty(lookupMode))
	{
		lookupMode = "";
	}
	if (lookupMode.equals("yes"))
	{
		StringTokenizer st = new StringTokenizer(fkeyword_str,"~");
	     while (st.hasMoreTokens())
	     {
	      	 st_sep= new StringTokenizer(st.nextToken(),"|");
			 urlFields.add(st_sep.nextToken());
			 urlKeywords.add((String)st_sep.nextToken());
	     }

	}



	 HashMap FldAttr=new HashMap();
	 HashMap TypeAttr=new HashMap();
	 HashMap FltrAttr=new HashMap();
	 HashMap SortAttr= new HashMap();
	 HashMap RepAttr= new HashMap();
	 HashMap attributes=new HashMap();
	 //This is done to remove the session varibale from previous report.

	 if (from.equals("browse")){
		// container=(ReportHolder)tSession.getAttribute("reportHolder");
		//System.out.println("Browse mode container is null");
		from = "browsePreview";

	 }else{
	 //attributes=(HashMap)tSession.getAttribute("attributes");
	 container=(ReportHolder)tSession.getAttribute("reportHolder");

	 }
	  //System.out.println("container::::::::::::::::::"+container);

	 if (container != null)
	 {
	 	//check if container's reportID is same as the one passed as parameter
	 		containerReportId = container.getReportId();
	 		if (StringUtil.isEmpty(containerReportId))
	 				containerReportId = "";

	 		//System.out.println("^^^^^containerReportId^^^^^" + containerReportId);

	 		if (! StringUtil.isEmpty(containerReportId))
	 		{
	 			if (! containerReportId.equals(repIdStr))
	 			{
	 				//container object is for a different report
	 				System.out.println("Different report get the container again");
	 				container = null;
	 			}
	 		}
	  }


	 if (container==null){
	 DynRepDao mDynDao=new DynRepDao();
	if (repIdStr.length() > 0)
		repId=(new Integer(repIdStr)).intValue();
		//dyndao=dynrepB.fillReportContainer(repIdStr);
		mDynDao.fillReportContainer(repIdStr);
		container=mDynDao.getReportContainer();
		container.DisplayDetail();
		tSession.setAttribute("reportHolder",container);
	 }



	multiFltrIds=container.getFilterIds();



	multiFltrNames=container.getFilterNames();

	//System.out.println("*******************multiFltrIds" + multiFltrIds.size());

	if (multiFltrIds!=null){
		if (multiFltrIds.size()>= 1)
		{
			if (StringUtil.isEmpty(fltrId))
			{
			 	fltrId = (String)multiFltrIds.get(0);
			}
			fltrDD=EJBUtil.createPullDownWithStr("fltrId",fltrId,multiFltrIds,multiFltrNames);
		}
	}



	ArrayList secFldCols=new ArrayList();

	ArrayList formIdList=new ArrayList();
	ArrayList formNameList = new ArrayList();
	FilterContainer fltContainer = null;
	String defDateRangeFilterType = "";
	String defDateRangeFilterStr = "";
	String coreDefDateRangeFilterStr = "";
	String origFormFilterSQL = "";
	String ignoreDRFilter = "";

	ArrayList selectedCoreLookupformIds = new ArrayList();
	String  mapTablePK  = "";
 	String  mapPKTableName = "";
 	String  mapTableFilterCol = "";
 	String  mapStudyColumn = "";
 	boolean checkForStudyRights = false;
	String fldUseDataValue;
 	int fldDataSeperatorPos = 0;
 	String fldColName = "";
 	String lookupOutputStr = "";

	checkForStudyRights = container.getCheckForStudyRights();


	String repHeader=request.getParameter("repHeader");
	repFooter=request.getParameter("repFooter");
	if (repHeader==null) repHeader="";
	if (repHeader.length()==0){
	 repHeader=container.getReportHeader();
	if (repHeader==null) repHeader="";
	}
	//Extract alignment String from report Header
	if ((repHeader.indexOf("[VELCENTER]")>=0)||(repHeader.indexOf("[VELLEFT]")>=0)||(repHeader.indexOf("[VELRIGHT]")>=0))
	 {
	   headAlign=repHeader.substring(0,(repHeader.indexOf(":",0)));
	   repHeader=repHeader.substring(headAlign.length()+1);
	   }
	   else
	   {
	   	headAlign=request.getParameter("headerAlign");
		if (headAlign==null) headAlign="center";
	   }

	   if (headAlign.equals("[VELCENTER]")) headAlign="center";
	   if (headAlign.equals("[VELLEFT]")) headAlign="left";
	   if (headAlign.equals("[VELRIGHT]")) headAlign="right";

	//end extracting header

	if (repFooter==null) repFooter="";
	if (repFooter.length()==0)
	{
	repFooter=container.getReportFooter();
	if (repFooter==null) repFooter="";
	}

	//Extract alignment String from report Footer
	if ((repFooter.indexOf("[VELCENTER]")>=0)||(repFooter.indexOf("[VELLEFT]")>=0)||(repFooter.indexOf("[VELRIGHT]")>=0))
	 {
	   footAlign=repFooter.substring(0,(repFooter.indexOf(":",0)));
	   repFooter=repFooter.substring(footAlign.length()+1);

	 }
	   else {
	   footAlign=request.getParameter("footerAlign");
	   if (footAlign==null) footAlign="center";
	   }
	   if (footAlign.equals("[VELCENTER]")) footAlign="center";
	   if (footAlign.equals("[VELLEFT]")) footAlign="left";
	   if (footAlign.equals("[VELRIGHT]")) footAlign="right";

	//end extracting footer

	//System.out.println("^^^^^^^^^^^^^^^^^^^^^^fltrId:::"+fltrId);

	// if fltrId is empty, get the first filter from the container.filterObject

	if (EJBUtil.isEmpty(fltrId))
	{
		Collection fltrSet = (container.getFilterObjects()).values();
		Iterator itrFltr = fltrSet.iterator();

		if (itrFltr.hasNext()) {
			fltContainer = (FilterContainer) itrFltr.next();
			fltContainer.Display();
			fltrId = fltContainer.getFilterId();
		}
	}
	else
	{
		fltContainer=container.getFilterObject(fltrId);
	}

	if (fltContainer!=null)
	{
 		filter=fltContainer.getFilterStr();
		defDateRangeFilterType = fltContainer.getFilterDateRangeType();
		if (StringUtil.isEmpty(defDateRangeFilterType))
		{
			defDateRangeFilterType = "";
		}
	}


	if (filter==null) filter="";

	filter=filter.trim();
	filter=StringUtil.decodeString(filter);


	 //end
	 if (dynDaoFltr!=null){
	 lfltrIds=dynDaoFltr.getFltrIds();
	 lfltrNames=dynDaoFltr.getFltrNames();
	 lfltrStrs=dynDaoFltr.getFltrStrs();
	  }

	CommonDAO common=new CommonDAO();
	ArrayList ids=new ArrayList();
	Connection conn;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	String sqlId="";
	String sqlIdTemp = "";
	if (fltContainer!=null)

	sqlId=fltContainer.getFilterStr();

	sqlId=(sqlId==null)?"":sqlId;
	origFormFilterSQL = sqlId;
	sqlId=StringUtil.decodeString(sqlId);


	formIdList = container.getFormIds();
	formNameList = container.getFormNames();

	selectedCoreLookupformIds = container.getSelectedCoreLookupformIds();

	allFformIdStr = container.getAllFormIdStr();


	String formType=container.getReportType();

	String recordCountString = "";
	String formIdComma = "";
		String patStudyFilter = "";

	formType=(formType==null)?"":formType;
	formType=(formType=="study")?"S":formType;
	formType=(formType=="pat")?"P":formType;

	studyId = EJBUtil.stringToNum(container.getStudyId());
	patId = EJBUtil.stringToNum(container.getPatientId());

	//changed by Sonia Abrol, 02/21/06, to make patient adHoc patient context sensitive
	if((! StringUtil.isEmpty(formPatient)) && patId == 0)
	{
		patId = EJBUtil.stringToNum(formPatient);
	}



	//get a comma separated list of formids (normal forms)

	 for (int k=0;k<formIdList.size();k++)
	 {
	 	 if ( ! ((String)formIdList.get(k)).startsWith("C") )
	 	 {
			formIdComma = formIdComma + "," + formIdList.get(k);
		 }
	 }
	if (!  StringUtil.isEmpty(formIdComma) )
	{
		if (formIdComma.startsWith(","))
		{
			formIdComma = formIdComma.substring(1); //remove first comma
		}

	}

	//get a comma separated list of patientids
	if (sqlId.length()==0)
	{
	 sqlId="";
	 bIsEmptySQL = true;

	 if (formType.equals("P") && studyId > 0)
	 {
	 	patStudyFilter = " and ( (fk_patprot IS NULL) OR " + studyId + " = (SELECT fk_study FROM ER_PATPROT WHERE pk_patprot=fk_patprot)) " ;
	 }

	 if (! StringUtil.isEmpty(formIdComma))
	 {
		 for (int k=0;k<formIdList.size();k++)
		 {
		 	 if ( ! ((String)formIdList.get(k)).startsWith("C") )
		   if ( sqlId.length()==0)
		   	  sqlId="select distinct ID from er_formslinear where fk_form="+formIdList.get(k) + patStudyFilter +  respAccessRightWhereClause;
		   else
		    sqlId=sqlId+" union select ID from er_formslinear where fk_form=" + formIdList.get(k) + patStudyFilter +  respAccessRightWhereClause;
		 }

	 }
	}
		else
	{
		//replace loggedinuserid place holder

		sqlId = StringUtil.replace(sqlId,"[:loggedinuser]",String.valueOf(userId));
			sqlId = StringUtil.replace(sqlId,"[:VELACCOUNT]",accId);

	}
	//see if the main id sql is empty and if there are core lookup tables selected, add basic 'select id'
 	//the sqlId is empty
	 if ( bIsEmptySQL )
	  {

	 	 	if (selectedCoreLookupformIds != null && selectedCoreLookupformIds.size() > 0)
	 	 	{
	 	 			if (fltContainer !=null)
	 	 			{
						defDateRangeFilterStr = fltContainer.getDateRangeFilterString()	;
					}

					if (! StringUtil.isEmpty(defDateRangeFilterStr))
						{
							defDateRangeFilterStr = " Where " + defDateRangeFilterStr;
           				    defDateRangeFilterStr = StringUtil.decodeString(defDateRangeFilterStr);
           				}

	 	 		//create the sql Id
	 	 		for (int ctr = 0; ctr < selectedCoreLookupformIds.size(); ctr++)
	 	 			{
	 	 				FieldContainer fldCTemp =container.getFldObject((String)selectedCoreLookupformIds.get(ctr));

	 	 				coreDefDateRangeFilterStr = defDateRangeFilterStr;

							if (fldCTemp !=null)
								{
	 								 mapTablePK = fldCTemp.getPkColName();
	 								 mapPKTableName = fldCTemp.getPkTableName();
									 mapStudyColumn = fldCTemp.getMapStudyColumn();

									if (formType.equals("S")) //in case of study lookups, table pk is pk/fk_study
									{
										mapStudyColumn = mapTablePK;
									}

	 								if ( sqlId.length()==0)
	 								 {
	 								 	sqlId = "Select distinct " + mapTablePK + " as ID from " + mapPKTableName;
	 								 }
	 								 else
	 								 {
	 								 	sqlId = sqlId + " union Select distinct " + mapTablePK + " as ID from " + mapPKTableName;
	 								 }
	 								 if (formType.equals("A")) //for account forms
	 								 {
	 								 	sqlId = sqlId + "  where " +  mapTablePK + " =  " + accId;
	 								 }


	 								ignoreDRFilter = container.getIgnoreFilters((String)selectedCoreLookupformIds.get(ctr));
									if (!StringUtil.isEmpty(ignoreDRFilter))
									{
										coreDefDateRangeFilterStr = "";
									}

	 								if (formType.equals("P") && (! StringUtil.isEmpty(coreDefDateRangeFilterStr)) )
									{
										if (defDateRangeFilterType.equals("PS"))
										{
											coreDefDateRangeFilterStr = defDateRangeFilterStr;

											if (! StringUtil.isEmpty(mapStudyColumn))
	 								 		{
	 								 			if (! StringUtil.isEmpty(coreDefDateRangeFilterStr))
								            	{
								            		coreDefDateRangeFilterStr = StringUtil.replace(coreDefDateRangeFilterStr,"[VELPAT]",mapTablePK);
								            		coreDefDateRangeFilterStr = StringUtil.replace(coreDefDateRangeFilterStr,"[VELSTUDY]",mapStudyColumn);

								            	}
				   					        }
				   					        else
				   					        {
				   					        	// if study rights are not checked, and study id is provided
				   					        	if (studyId > 0)
				   					        	{
				   					        		coreDefDateRangeFilterStr = StringUtil.replace(coreDefDateRangeFilterStr,"[VELPAT]",mapTablePK);
								            		coreDefDateRangeFilterStr = StringUtil.replace(coreDefDateRangeFilterStr,"[VELSTUDY]",String.valueOf(studyId));

				   					        	}
				   					        	else
				   					        	{
				   					        		coreDefDateRangeFilterStr = "";
				   					        	}


				   					        }

										}
									}


	 								sqlId = sqlId + coreDefDateRangeFilterStr;

	 								if (!StringUtil.isEmpty(mapStudyColumn) &&  studyId > 0)
	 								{
										if (! StringUtil.isEmpty(coreDefDateRangeFilterStr))
		 								{
		 									sqlId = sqlId  + " and " + mapStudyColumn + " = " + studyId;
		 								}
		 								else
		 								{
		 									sqlId = sqlId  + " Where " + mapStudyColumn + " = " + studyId;
		 								}
										
										sqlId = sqlId + " and "+ mapPKTableName +".FK_ACCOUNT =  " + accId;

	 								}else{
	 									if (formType.equals("S")) //for study Ad-hoc queries
		 								 {
		 								 	sqlId = sqlId + " where "+ mapPKTableName +".FK_ACCOUNT =  " + accId;
		 								 }
	 								}

	 								if (patId > 0 && formType.equals("P") )
	 								{
										if (! StringUtil.isEmpty(coreDefDateRangeFilterStr))
		 								{
		 									sqlId = sqlId  + " and " + mapTablePK + " = " + patId;
		 								}
		 								else
		 								{
		 									sqlId = sqlId  + " Where " + mapTablePK + " = " + patId;
		 								}
	 								}



	 						}
	 	 							////////////
	 	 			}


	 	 	}
	 	 	mapTablePK = "";
	 		mapPKTableName = "";
	 		System.out.println("Now main sql" +  sqlId);

	}

	//if preview mode and the report is not saved, recalculate checkForStudyRights  flag


	 	 	if ((! from.equals("browsePreview") ) && selectedCoreLookupformIds != null && selectedCoreLookupformIds.size() > 0)
	 	 	{
	 	 		System.out.println("recalculating checkForStudyRights flag");
	 	 		container.setCheckForStudyRights(false);
	 	 		checkForStudyRights = false;


	 	 		//create the sql Id
	 	 		for (int ctr = 0; ctr < selectedCoreLookupformIds.size(); ctr++)
	 	 			{
	 	 				FieldContainer fldCTemp =container.getFldObject((String)selectedCoreLookupformIds.get(ctr));
							if (fldCTemp !=null)
								{
	 								 mapStudyColumn = fldCTemp.getMapStudyColumn();

	 								 if (! StringUtil.isEmpty(mapStudyColumn))
	 								 {
	 								 	 container.setCheckForStudyRights(true);
	 								 	 checkForStudyRights = true;
	 								 	 break;
	 								 }
	 							}
	 	 				////////////
	 	 			}

	 	 	}
	/////////////////////////////////////////////////////////



	//////////////////////append the access rights filters to the main ID sql//////////////////////////////////

	if (formType.equals("P"))
	{
			// get the patient stataus date range filter string only if the original filter sql was empty
		if (defDateRangeFilterType.equals("PS") && StringUtil.isEmpty(origFormFilterSQL))
           	{
           		if (fltContainer != null)
           		{
           			defDateRangeFilterStr = fltContainer.getDateRangeFilterString()	;
           		}
           	}
           	else
           	{
           		defDateRangeFilterStr = "";
           	}

		 /* Attach Organization check */

		 /** filter on patient code ----------sonia,12/22*/
		 if (checkForStudyRights || studyId > 0 )
		 {
			 int count = 0;
			 for (int ctr = 0; ctr < selectedCoreLookupformIds.size(); ctr++){
			   String frmId = (String)selectedCoreLookupformIds.get(ctr);
			   if(!frmId.startsWith("C")){
				   count++; 
			   }
				
			 }
			 if (count>0){
					rightsWhereClause = "  exists ( SELECT * FROM ER_PER c, ER_PATPROT pp "+
				 	 " WHERE c.fk_account = " + accId + " and  c.pk_per = a.ID and pp.fk_per = c.pk_per and pp.patprot_stat = 1 and (0 < pkg_user.f_chk_right_for_patprotsite(pp.pk_patprot,"+userId+") )";
					}else{
					rightsWhereClause = "  exists ( SELECT * FROM ER_PER c, ER_PATPROT pp "+
				 	 " WHERE c.fk_account = " + accId + " and  c.pk_per = ID and pp.fk_per = c.pk_per and pp.patprot_stat = 1 and (0 < pkg_user.f_chk_right_for_patprotsite(pp.pk_patprot,"+userId+") )";
					
					}
				if (studyId > 0)
				{
					rightsWhereClause = rightsWhereClause + " and pp.fk_study = " + studyId ;
				}

				if (patId>0)
				 {
				 	rightsWhereClause = rightsWhereClause+ " and fk_per = "+ patId ;
				 }

				 // attach patcode filter , sonia 12/22

				if (! StringUtil.isEmpty(patcodeFilter))
				{
					rightsWhereClause  = rightsWhereClause  +
					 " and ( lower(c.per_code) like '%" + patcodeFilter.toLowerCase() +"%' OR pp.fk_per in ( select fk_per from er_patfacility where lower(pat_facilityid) like '%" + patcodeFilter.toLowerCase() +"%'))";
				}

				rightsWhereClause = rightsWhereClause + " )";

		 }
		 else
		 {

		 	/*rightsWhereClause = " exists ( SELECT * FROM ER_PER c  WHERE c.fk_account = " + accId + " and c.pk_per = ID and  "   +
				 	"pkg_user.f_right_forpatsites(pk_per,"+ userId +") > 0"; */

			rightsWhereClause = " pkg_user.f_right_forpatsites(id,"+ userId +") > 0   ";


		 	if (patId>0)
				 {
				 	rightsWhereClause  = rightsWhereClause  + " and id  = "+ patId ;
				 }



			//rightsWhereClause = rightsWhereClause + " )";

		 }

	} //end of formType equals to P


	if (formType.equals("S"))
	{
		 if (! StringUtil.isEmpty(sqlId))
		 {
		 	//sqlId = sqlId + " INTERSECT ";

		 	sqlId = " Select ID from ( " + sqlId + " ) ";

		 	sqlId = sqlId + " Where (Pkg_Util.f_checkStudyTeamRight(ID,"+userId+",'STUDYFRMACC')) = 1 ";
		 }

		 if (studyId > 0)
		{
			sqlId = sqlId + " and  ID = " + studyId ;
		}




	}

	/////////////////////////////////////////////////////////////////////////////////////////
	if(formType.equals("P"))
	{
		//get patient Code
		sqlId = "select  id  from ( " + sqlId + " ) out ";
		sqlId = sqlId + " Where " + rightsWhereClause ;
	}

	System.out.println("SQL MAIN"+sqlId);


	////////////
	//System.out.println("lookupMode" + lookupMode);

	if (lookupMode.equals("yes") )
	{

			if (formType.equals("P") ) // add a search for patient type reports
			{
		%>
		    <p class="defComments"><%=MC.M_EtrPatId_ToSrchRec%><%--Please enter Patient Id to search the lookup records*****--%></p>

				<%=LC.L_Patient_Id%><%--<%=LC.Pat_Patient%> Id*****--%>: <font class="mandatory" size=2>*</font> &nbsp;<input name="patcode" value="<%=patcodeFilter%>" type="text" />

			<%

			} else if (formType.equals("S") ) // add a search for study type reports
			{
		%>
		    <p class="defComments"><%=MC.M_YouCanSrch_StdNum%><%--You can search on <%=LC.Std_Study%> Number to narrow down the search results*****--%></p>

				<%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>: &nbsp;<input name="studynumber" value="<%=studyNumFilter%>" type="text" />

			<%

			}

			if (  StringUtil.isEmpty(fltrDD) ) { %>
				&nbsp;&nbsp;<button type="submit" onClick = "return jumpToExp(document.preview,'<%=formType%>');"><%=LC.L_Go%></button>
			<% } %>
		<%


	 if ( ! StringUtil.isEmpty(fltrDD) ) { %>
	 	<%=LC.L_Filter%><%--Filter*****--%> : <%=fltrDD%> &nbsp;&nbsp;
     	<button type="submit" onClick = "return changeFilterExp(document.preview);"><%=LC.L_Go%></button>
    	&nbsp; <A href="#" onClick="openFilterView()"><%=LC.L_View_Filters%><%--View Filters*****--%></A>&nbsp;&nbsp;&nbsp;&nbsp;

     <% }

	}

	if(fmt.equals("L")) //show filters dropdown in linear format
	{
		 if ( ! StringUtil.isEmpty(fltrDD) ) { %>
	 	<%=LC.L_Filter%><%--Filter*****--%> : <%=fltrDD%> &nbsp;&nbsp;
     	<button type="submit" onClick = "return changeFilterLinearExp(document.preview);"><%=LC.L_Go%></button>
    	&nbsp; <A href="#" onClick="openFilterView()"><%=LC.L_View_Filters%><%--View Filters*****--%></A>&nbsp;&nbsp;&nbsp;&nbsp;

     <% }

	}

	sbOutput.append("<hr class=\"thickLine\">");
	sbOutput.append("<table width=\"100%\" border=\"0\">");
	if (repHeader.length()>0)
	{
		sbOutput.append("<tr><td class=\"reportName\" align=\""+headAlign+"\"><pre>"+ repHeader+"</pre></td></tr>");
		sbOutput.append("<tr height=\"20\"></tr>");
	}
//JM: 17Dec2009: #3217
	if (formType.equals("P") || formType.equals("S") || formType.equals("A")){
	 	sbOutput.append("<tr><td align=center><font class=\"comments\">"+MC.M_SummaryInfo_DataDisp/*Summary Information is based on the data displayed on this page*****/+"</font></td></tr>");
	 	sbOutput.append("</table>");
	}else{
	 	sbOutput.append("<tr><td align=center><font class=\"comments\">"+MC.M_QryToLkUp_NotExist/*Ad-Hoc Query linked to this Lookup does not exist*****/+"</font></td></tr>");
	 	sbOutput.append("</table>");
	}

	sbOutputMain.append(sbOutput.toString());
	boolean encodeAll = true;
	String str = sbOutput.toString();
	//to encode special characters e.g.  ®,©,™,²
	str = StringUtil.htmlEncode(str, encodeAll);
	if (fmt.equals("H"))
	{
		out.println(str);
	}
	sbOutput.delete(0, sbOutput.length());

	for (int z=0;z<formIdList.size();z++)
	{
		boolean anyMaskedColumn = false;
		ArrayList arMaskedColumnList = new ArrayList();

		ArrayList fldKeywords=new ArrayList();
		ArrayList fldCols=new ArrayList();
		ArrayList fldNames=new ArrayList();
		ArrayList fldDispName=new ArrayList();
		ArrayList tmpFldNames=new ArrayList();
		ArrayList tmpFldDispName=new ArrayList();
		ArrayList fldWidth=new ArrayList();
		ArrayList repFldNames=new ArrayList();
		ArrayList repFldNamesSeq=new ArrayList();
		ArrayList mapSecNames=new ArrayList();
		ArrayList fieldRepeatNumber =new ArrayList();

		ArrayList mapSecIds=new ArrayList();
		ArrayList fieldTypes = new ArrayList();
		ArrayList fieldDisplayDataValue = new ArrayList();
		ArrayList repFieldDisplayDataValue = new ArrayList();

		ArrayList fldCalcSum = new ArrayList();
		ArrayList fldCalcPer = new ArrayList();
				Hashtable moreParameters  = new Hashtable();
		ArrayList arCompleteFieldList = new ArrayList();


		formId=(String)formIdList.get(z);

		if (StringUtil.isEmpty(formId))
		{
			continue;
		}

		if (formId.startsWith("C"))
		{
			//check if it exists in selectedCoreLookupformIds, if no then continue

			if (! selectedCoreLookupformIds.contains(formId))
			{
				continue;
			}

		}


	sbOutputMain.append(sbOutput.toString());
	if (fmt.equals("H"))
	{
		out.println(sbOutput.toString());
	}
	sbOutput.delete(0, sbOutput.length());


	formName = (String)formNameList.get(z);

	if (fltContainer!=null)
	{
	 filter=fltContainer.getChoppedFilter(formId);
	 filter=filter.trim();


	 if (StringUtil.isEmpty(filter) && (!defDateRangeFilterType.equals("PS")) )
	 {
	 	 //get the date range filter (if defined)
	 	  ignoreDRFilter = container.getIgnoreFilters(formId);
	 	  if (StringUtil.isEmpty(ignoreDRFilter))
			{
	 			filter = fltContainer.getDateRangeFilterString();
	 		}
	 }

	 filter=StringUtil.decodeString(filter);
	 filter=(filter==null)?"":filter;
	 }
 	else
	 {
	 	defDateRangeFilterType  = "";
	 }

	SortContainer sortContainer=container.getSortObject(formId);
	if (sortContainer!=null)
	{
		dataOrder=sortContainer.getSortStr();
		if (dataOrder==null) dataOrder="";
		if (dataOrder.length()>0){
		dataOrder=StringUtil.replace(dataOrder,"[$$]",",");
		dataOrder=StringUtil.replace(dataOrder,"[$]"," ");
		orderStr=" order by "+ dataOrder;
		}
	}
	else
	{

		orderStr = "";
	}


	FieldContainer fldContainer=container.getFldObject(formId);
	if (fldContainer!=null)
	{
		 fldKeywords = fldContainer.getFieldKeyword();

		 fldCols=fldContainer.getFieldColId();
		 fldNames=fldContainer.getFieldName();
		 fldDispName=fldContainer.getFieldDisplayName();
		 fldWidth=fldContainer.getFieldWidth();
	 	 fieldTypes = fldContainer.getFieldType();
		 fieldDisplayDataValue = fldContainer.getUseDataValues();
		 fldCalcSum = fldContainer.getCalcSums();
 		 fldCalcPer = fldContainer.getCalcPers();
 		 fieldRepeatNumber = fldContainer.getFieldRepeatNumber();
		 moreParameters.put("fldCalcSum", fldCalcSum);
 		 moreParameters.put("fldCalcPer", fldCalcPer);

		 mapTablePK = fldContainer.getPkColName();
		 mapPKTableName = fldContainer.getPkTableName();
		 mapTableFilterCol = fldContainer.getMainFilterColName();
		 mapStudyColumn = fldContainer.getMapStudyColumn();

		 arCompleteFieldList =  fldContainer.getFieldList();

	}
	else
	{
	}

	 if (fldCols.size() <= 0)
	 {

	 	sbOutput.append("<table border=\"1\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">");
		sbOutput.append("<TR class=\"browserOddRow\" > <td><B>"+LC.L_Form/*Form*****/ +": " + formName + "</B></td></TR>");
		sbOutput.append("</table><P class=\"redMessage\">"+MC.M_NoFldsSelected/*No Fields Selected*****/+"</P>");

		sbOutputMain.append(sbOutput.toString());
		if (fmt.equals("H"))
		{
			out.println(sbOutput.toString());
		}
		sbOutput.delete(0, sbOutput.length());

		continue;
	 }

	tmpFldNames=fldNames;
	tmpFldDispName=fldDispName;
	mapSecNames.clear();
	mapSecIds.clear();

	repFldNames.clear();
	repFieldDisplayDataValue.clear();
	secFldCols.clear();

	sql="";
	sqlStr="";

	int idxInMaster = -1;
	for (int i=0;i<fldCols.size();i++){

		if (formId.startsWith("C"))
		{


			//find the col in arCompleteFieldList
			idxInMaster = arCompleteFieldList.indexOf((String)fldCols.get(i));

			if (idxInMaster >= 0)
			{
				fldKey = (String) fldKeywords.get(idxInMaster);
			//	 System.out.println("fldKey" +fldKey);

				if (fldKey.startsWith("MASK_"))
				{
				  //System.out.println("madkcol" +(String)fldCols.get(i));
					anyMaskedColumn = true;
					arMaskedColumnList.add((String)fldCols.get(i));

				}
			}
		}

	   tempStr=(String)fldCols.get(i) ;
	  name=(String)fldCols.get(i);//use col name instead of display name
	   if (name==null) name="";
	  if (name.length()>30) name=name.substring(0,30);
	  if (name.indexOf("?")>=0)
	   name=name.replace('?','q');
	   if (name.indexOf("\\")>=0)
	   name=name.replace('\\','S');
	  if (tempStr.indexOf("|")>=0)
	  {
	  //store the repeated fields names in an arraylist and the sequence to remove the repeated fldnames from original fldNames list
	  //populate secfldCols to retirve section for only repeated fields.
	  secFldCols.add(tempStr);
	  repFldNames.add( StringUtil.decodeString((String)fldDispName.get(i)) );
	  repFldNamesSeq.add(new Integer(i));
	  repFieldDisplayDataValue.add((String)fieldDisplayDataValue.get(i));

	  //Sonia Abrol, 04/06/06 , for repeat field crash, if concatenated string's data is  is more than 4000 char
	  	String[] arRepColsTemp = StringUtil.chopChop(tempStr,'|');
		repColCount  = 0;

	  	if (arRepColsTemp != null)
	  	{
	  		repColCount = arRepColsTemp.length;

	  		if (repColCount > 1)
	  		{
		  		repColCharAvailable = 4000 - (repColCount * 8); //substract (number of columns * 8) 8 for [VELSEP]

			  	//System.out.println("repColCount " + repColCount + "repColCharAvailable  " + repColCharAvailable  + "char" + repColCharAvailable /repColCount + "*");
	  			repColDataUpperLimit = (int) Math.floor(repColCharAvailable /repColCount) ;
			  	//System.out.println("repColDataUpperLimit " + repColDataUpperLimit  + "*");
	  		}
	  		else
	  		{
	  			repColDataUpperLimit  = 4000;
	  		}
	  	}
	  	else
	  	{
	  		repColDataUpperLimit = 4000;
	  	}
		//////////////


	  tempStr=tempStr.replace('|',',');

	   //tempStr=StringUtil.replaceAll(tempStr, ",", "||'[VELSEP]'||");

	  tempStr=StringUtil.replaceAll(tempStr, ",", ",'-') , 1,"+ repColDataUpperLimit +") ||'[VELSEP]'|| substr(nvl(");
	  tempStr = "substr(nvl( " + tempStr + ",'-') ,1,"+ repColDataUpperLimit +")";

	     if (sqlStr.length()==0)
	  	sqlStr=" ("+tempStr+") as \"" + name+ "\"" ;
		 else
		sqlStr=sqlStr+",  ("+tempStr+") as \"" + name +"\"";
	}
	else{
		if (sqlStr.length()==0)  sqlStr=tempStr +" as \"" +name +"\"";
		else sqlStr=sqlStr + ", " + tempStr + " as \"" + name+"\"";
	}
	}

	//if anyMaskedColumn then append access rights column

	if (anyMaskedColumn)
	{
		sqlStr = sqlStr + ", pkg_studystat.f_get_patientright("+userId+", " + defUserGroup+ " , " + mapTablePK +") as pat_detail_right";
	}

	 /* check if the filter is 'undefined' and set it to blank
	 */
	 filter=(filter.equals("undefined")?"":filter);

	 if ((filter.trim()).length()>0)
	 {
			filter= StringUtil.decodeString(filter);
			filter = " and ( " + filter + " ) ";
	 }

	 if (StringUtil.isEmpty(filter))
	 {
	 	 filter = "";
	 }

	if (formType.equals("A"))
	{
	if (! formId.startsWith("C") )
		{
		sql =" select ID ,FK_PATPROT as PATPROT, " + sqlStr + " from er_formslinear where fk_form = "+ formId + filter +
		 " and fk_form in (select PK_FORMLIB from er_formlib a,er_linkedforms b,er_formstat c " +
		" Where b.fk_account = "+ accId+"  and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and  b.record_type<>'D' and "+
		"((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
		"   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+
		" and (c.formstat_enddate is null) and  "+
		"b.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=" +userId +" and usersite_right <> 0)) and "+
		"b.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user="+userId +" )) "+
		" AND ((LF_DISPLAYTYPE = 'A' ))   union " +
		"select PK_FORMLIB from er_formlib a,er_linkedforms b,er_formstat c " +
		" where b.fk_account = "+accId+" and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and b.record_type<>'D'and "+
		" ((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
		"   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+
		" and (c.formstat_enddate is null) and  "+
		" (b.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user="+userId+" )) "+
		" or b.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user="+userId+" and usersite_right <> 0))) "+
		" AND ((LF_DISPLAYTYPE = 'A' )) )  " + respAccessRightWhereClause  ;
		}
		else //core table
		{

			if (! StringUtil.isEmpty(sqlStr))
			{
				sqlStr = " , " + sqlStr;
			}

			sql =" select "+ mapTablePK + ",'' as PATPROT " + sqlStr + " from " + mapPKTableName + " Where " +  mapTablePK + "="+accId +  filter  ;
		}

	}

	if (formType.equals("S"))
	{
		if (! StringUtil.isEmpty(sqlStr))
		{
			sqlStr = " , " + sqlStr;
		}

		if (! formId.startsWith("C") )
		{
			sql =" select ID,fk_patprot as PATPROT " + sqlStr + " from er_formslinear where fk_form="+formId + filter + respAccessRightWhereClause;
		}
		else //core table
		{
			sql =" select "+ mapTablePK + ",'' as PATPROT " + sqlStr + " from " + mapPKTableName + " Where " +  mapTablePK + " > 0  "  +  filter  ;
		}


		if (studyId > 0)
		{
			sql = sql + " and "+ mapTablePK+ " = " + studyId + " and 1 = (Pkg_Util.f_checkStudyTeamRight("+studyId+","+userId+",'STUDYFRMACC')) ";
		}

		/*else
		{

		 	sql = sql + " and  1 = (Pkg_Util.f_checkStudyTeamRight("+ mapTablePK +","+userId+",'STUDYFRMACC') ) " ;
		}*/

	}


	if (formType.equals("P"))
	{
			defDateRangeFilterStr = "";

		if (defDateRangeFilterType.equals("PS") && StringUtil.isEmpty(filter))
           	{
           		if (fltContainer != null)
           		{
           		  		defDateRangeFilterStr = fltContainer.getDateRangeFilterString()	;
           		}
           	}
           	else
           	{
           		defDateRangeFilterStr = "";
           	}


	 	ignoreDRFilter = container.getIgnoreFilters(formId);

		if (!StringUtil.isEmpty(ignoreDRFilter))
			{
				defDateRangeFilterStr = "";

			}


		if (! StringUtil.isEmpty(sqlStr))
		{
			sqlStr = " , " + sqlStr;
		}

		if (! formId.startsWith("C") )
		{
			sql =" select ID,fk_patprot as PATPROT   " + sqlStr + " from er_formslinear a where fk_form = "+formId  + filter + respAccessRightWhereClause;
		}
		else
		{
			sql =" select "+ mapTablePK + ",'' as PATPROT " + sqlStr + " from " + mapPKTableName+ " Where " +  mapTablePK + " > 0 " ;

            if (! StringUtil.isEmpty(mapStudyColumn) )
            {
            	//check study rights on basis of this column

            	sql = sql + " and (0 < pkg_user.f_chk_studyright_using_pat("+mapTablePK +","+mapStudyColumn+" ,"+userId+") ) " ;

            	  	//if defDateRangeFilterType = 'PS', and defDateRangeFilterStr is not empty, replace values

            	if (! StringUtil.isEmpty(defDateRangeFilterStr))
            	{
            		defDateRangeFilterStr = StringUtil.replace(defDateRangeFilterStr,"[VELPAT]",mapTablePK);
            		defDateRangeFilterStr = StringUtil.replace(defDateRangeFilterStr,"[VELSTUDY]",mapStudyColumn);
            		sql = sql + " and " + defDateRangeFilterStr ;
            	}



            	if (studyId>0)
		 		{
		 			sql = sql + " and "+mapStudyColumn+ " = " + studyId;
		 		}

            }

            sql = sql + filter ;
		}

		//commented by sonia, 07/11/05, we dont need to check it in outer sql as we have already checked this in Inner ID sql
		/*if ((! checkForStudyRights) && (studyId <= 0 ))
		{

		 sql = sql +  " and " + mapTablePK + " in (SELECT pk_per FROM ER_PER c ,ER_USERSITE d  WHERE c.fk_site = d. fk_site AND d. fk_user = "
		 	 + userId+  " AND d.usersite_right > 0 )";
		}*/


		 if (patId>0)
		 {
		 	sql = sql + " and "+ mapTablePK + "="+ patId ;
		 }
		 else if (studyId > 0 && StringUtil.isEmpty(mapStudyColumn))
		 {
		 	 sql = sql  + " and " + mapTablePK + " in (select fk_per from er_patprot where fk_study="+studyId+ " and patprot_stat=1 and (0 < pkg_user.f_chk_right_for_patprotsite(pk_patprot,"+ userId +") ))";

			if (! StringUtil.isEmpty(defDateRangeFilterStr))
            	{
            		defDateRangeFilterStr = StringUtil.replace(defDateRangeFilterStr,"[VELPAT]",mapTablePK);
            		defDateRangeFilterStr = StringUtil.replace(defDateRangeFilterStr,"[VELSTUDY]",String.valueOf(studyId));
            		sql = sql + " and " + defDateRangeFilterStr ;

            	}

		 }

		if (! formId.startsWith("C") )
		{
			if (studyId>0)
		 	{
		 		sql = sql + " and ( (a.fk_patprot is null) or " + studyId + " = (SELECT fk_study FROM ER_PATPROT WHERE pk_patprot=a.fk_patprot)) ";
		 	}
			 sql = sql + " AND ((a.fk_patprot is null) or  (0 < pkg_user.f_chk_right_for_patprotsite(a.fk_patprot,"+ userId +") ) )"  ;

			if (! StringUtil.isEmpty(defDateRangeFilterStr))
            	{
            		defDateRangeFilterStr = StringUtil.replace(defDateRangeFilterStr,"[VELPAT]","ID");

            		if (studyId>0)
            		{
            			defDateRangeFilterStr = StringUtil.replace(defDateRangeFilterStr,"[VELSTUDY]",String.valueOf(studyId));
            			sql = sql + " and " + defDateRangeFilterStr  ;
            		}
            		else
            		{
            			defDateRangeFilterStr = StringUtil.replace(defDateRangeFilterStr,"[VELSTUDY]","(SELECT fk_study FROM ER_PATPROT WHERE pk_patprot=a.fk_patprot)");
            			sql = sql + " and ((a.fk_patprot is null) or " + defDateRangeFilterStr + " )";
            		}



            	}

		}

	}

	//append id sql at the end of the SQL to filter appropriate patients

	//KM-#5069
	if (formType.equals("A") && formId.startsWith("C"))
	{
		sqlId =  "select ID from ( " + sqlId + " )";
	}
	sql = 	sql + " and " + mapTablePK +" in (" +  sqlId + ")";

////////////////////////////////

// 04/18/2007 - Sonia - Study Adhoc Optimization - took the study number check outside the main sql
if (formType.equals("S")) {

if (! StringUtil.isEmpty(studyNumFilter))
			{
				sql = sql + " AND "+mapTablePK +" in ( select pk_study from er_study where fk_account = "+ accId+
				" and  lower(study_number) like  '%" + studyNumFilter.toLowerCase() + "%' )";
			}}

/////////////////////////////////////



// 04/18/2007 - Sonia - patient Adhoc Optimization - took the patient code check outside the main sql
if (formType.equals("P")) {


			if (! StringUtil.isEmpty(patcodeFilter))
			{
				sql = sql + " and "+ mapTablePK + " in ( select pk_per from er_per where fk_account = " + accId + " and ( lower(per_code) like '%" + patcodeFilter.toLowerCase() +
				 "%' OR pk_per in ( select fk_per from er_patfacility where lower(pat_facilityid) like '%" +
				 patcodeFilter.toLowerCase() +"%')) ) ";

			}


}


////////////////////////////////////

	if (orderStr.length()>0) sql = sql+ "  " + orderStr ;
	else
	{
		//changed to column number
		if (formType.equals("P")) sql = sql+ "  " + " order by 1" ;
		else if (formType.equals("S")) sql =sql+ "  " + " order by 1" ;
	}

	//sql="select "+ sqlStr+" from er_formslinear where fk_form="+formId;
	System.out.println("sonia sql!!" + sql+formType);

	 String fileNameForMappingHash = "";
	 fileNameForMappingHash = fileNameForLinear.substring(0,(fileNameForLinear.length())-4);
	 fileNameForMappingHash = fileNameForMappingHash + "-" + formId + ".vel";


	if (formType.equals("P") && lookupMode.equals("yes") && (StringUtil.isEmpty(patcodeFilter)) ) // add a search for patient type reports
	{
		System.out.println("sonia sql!! not executed");
	}
	else
	{
		System.out.println("sonia sql!! executing");


		 moreParameters.put("fileNameForMappingHash", fileNameForMappingHash);
		 moreParameters.put("formId", formId);
		 moreParameters.put("expFormat", fmt);

		 if (anyMaskedColumn)
		 {
		 	moreParameters.put("arMaskedColumnList", arMaskedColumnList);
		 	moreParameters.put("maskedReferenceColumn", "pat_detail_right");
		 }


		dynDao=dynrepB.getReportData(sql,(( String [] ) fldNames.toArray ( new String [ fldNames.size() ] )),formType ,moreParameters);
	}
	formSequenceCounter = formSequenceCounter  +1;


	//dynDao.getReportData(sql,fldName);
	dataList=dynDao.getDataCollection();
	repSummaryHolder = dynDao.getRepSummaryHolder();

	//System.out.println("size of data" + dataList.size());
	//loop through fldNames to remove the repested field names,since they are stoes in repFldNames


	//Retrieve section information
	//DynRepDao dyndao=dynrepB.getMapData(formId);

	//System.out.println("DataList:\n" + dataList + "\n mapSecNames\n" + mapSecNames + "\n mapSecIds\n" + mapSecIds + "\nfldDispName:\n" + fldDispName+ "\n repFldAMES:\n" + repFldNames);
	//end retrieve section
	DynRepDao dyndao=new DynRepDao();
	//System.out.println("secFldCols*********************************"+ secFldCols);
	if (secFldCols!=null){
	if (secFldCols.size()>0){
		dyndao=dynrepB.getSectionDetails(secFldCols,formId);
		dyndao.preProcessMap();
		mapSecNames=dyndao.getMapColSecNames();
		mapSecIds=dyndao.getMapColSecIds();

	}
	}

	//set Report data Holder

	if (dataList == null)
	{
		dataList = new ArrayList();
	}

	if (fmt.equals("L") || fmt.equals("X") )
	{


		if ( formSequenceCounter  > 1) //z is the form counter, in case this is second form, serialize /deserialize it
		{
			//rdHolder = (ReportDataHolder) EJBUtil.readSerializedObject(fileNameForLinear);
		}

		rdHolder.setArFormIds(formId);
		rdHolder.setArFormNames(formName);

		rdHolder.setHtFormData(formId, dataList);
		rdHolder.setHtFormFieldTypes(formId, fieldTypes);
		rdHolder.setHtFormFldDisplayDataValue(formId,fieldDisplayDataValue );
		rdHolder.setHtFormfldWidth(formId,fldWidth);
		rdHolder.setHtFormRepFldNames(formId,repFldNames);
		rdHolder.setHtFormMapSecNames(formId,mapSecNames);
		rdHolder.setHtFormMapSecIds(formId, mapSecIds);
		rdHolder.setHtFormRepFldDisplayDataValue(formId,repFieldDisplayDataValue );
		rdHolder.setHtActualFormFieldNames(formId, fldDispName);
		rdHolder.setHtSummaryHolder(formId, repSummaryHolder);
		rdHolder.setHtFileNameForIDROWMappingHash(formId, fileNameForMappingHash);
		rdHolder.setHtFormfieldCount(formId,fldDispName, fieldRepeatNumber);
		//serialize the obj
		//EJBUtil.serializeMe(rdHolder,fileNameForLinear);
		//rdHolder = null;
	}

	/*System.out.println("fldNames..........." + fldNames);
	System.out.println("repFldNames..........." + repFldNames);
	System.out.println("mapSecNames..........." + mapSecNames);
	System.out.println("mapSecIds..........." + mapSecIds); */

	if ( (! fmt.equals("L")) && (! fmt.equals("X")) )
	{


			sbOutput.append("<table border=\"1\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">");
			sbOutput.append("<TR class=\"browserOddRow\" > <td><B>"+LC.L_Form/*Form*****/ +": " + formName + " </B></td><td><p class=\"redMessage\"> "+MC.M_TotRec_Fnd/*Total Records Found*****/+": &nbsp; "+ NumberUtil.formatToNumber(dataList.size())+ "</p></td></TR>");
			sbOutput.append("</table>");


			sbOutput.append("<table border=\"1\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">");
				//sbOutput.append("<TR class=\"browserEvenRow\" > <td colspan=3><B>Summary Information:</B></td></TR>");
			sbOutput.append("<TR class=\"browserEvenRow\" > <td ><b>"+LC.L_Fld_Name/*Field Name*****/+"</b></td><td><b>"+LC.L_Basic_Stats/*Basic Stats*****/+"</b></td><td ><b>"+LC.L_CntOrPerc/*Count/Percentage*****/+"</b></td></TR>");

				int countSummaryField = 0;
					for (Enumeration e = (repSummaryHolder.getHtHolders()).keys() ; e.hasMoreElements() ;)
				    {
				         SummaryHolder sd = new SummaryHolder();
				         String keyName = (String)e.nextElement();
				         countSummaryField++;

				         sd = (SummaryHolder) repSummaryHolder.getHtHolders().get( keyName);

				         if (sd != null)
				         {
				         	sbOutput.append("<TR class=\"browserOddRow\" > <td>" +fldDispName.get(Integer.parseInt(keyName)) + "</td> " );

				         	if (sd.getInfoType().equals("sum") || sd.getInfoType().equals("both"))
				         	{
				         		sbOutput.append("<td><b>"+LC.L_Min/*Min*****/+":</b>" + sd.getMinValue() + "&nbsp;<b>"+LC.L_Max/*Max*****/+":</b>" + sd.getMaxValue() + "&nbsp;<b>"+LC.L_Median/*Median*****/+":</b>" + sd.getMedianValue() + "&nbsp;<b>"+LC.L_Avg/*Avg*****/+":</b>" + sd.getAverageValue() + "</td> " );

				         	  if(!sd.getInfoType().equals("both"))
				     			{

				     				sbOutput.append("<td>"+LC.L_NORA/*N/A*****/+"</td>");


				     			}
				         	}
				         	if (sd.getInfoType().equals("per") || sd.getInfoType().equals("both"))
				         	{
				      	     if(!sd.getInfoType().equals("both"))
				     			{

				     			sbOutput.append("<td>"+LC.L_NORA/*N/A*****/+"</td>");

				       			}

				         		sbOutput.append("<td>");

				         		for (Enumeration p = (sd.getHtValueCounts()).keys() ; p.hasMoreElements() ;)
				         			{
				         				String dispValResp = "";
				         				dispValResp = (String)p.nextElement();

				         				sbOutput.append("&nbsp;<b>" + dispValResp + "</b>:" + sd.getHtValueCounts(dispValResp) + "(" + String.valueOf(sd.getHtValueCountPercentage(dispValResp))  + "%)  ");

				         			}
				         		sbOutput.append("</td> ");

				         	}

				         	sbOutput.append("</TR>");

				         }
				     }


						if (countSummaryField == 0)
						{
							sbOutput.append("<tr><td colspan=3>***"+MC.M_NoFldSumm_InfoFnd/*No Fields selected for Summary Information/No Data Found*****/+"***</td></tr>");  
						}
			sbOutput.append("</table><BR>");

			StringBuffer sbTempHeader = new StringBuffer();
			int tableWidth = 0;
			int colInTableHeader = 0;

			for(int count=0;count<fldNames.size();count++){
		   index= (repFldNamesSeq.indexOf(new Integer(count)));
		   if (index>=0)
		   {
		   	continue;
		   }
		    fldWidthStr = (String)fldWidth.get(count);

					if (EJBUtil.stringToNum(fldWidthStr) == 0)
					{
						 fldWidthStr = "15";
					}
			//temp fix

			fldWidthStr = "150";

			tableWidth = tableWidth + EJBUtil.stringToNum(fldWidthStr) ;
			colInTableHeader = colInTableHeader + 1;

			sbTempHeader.append("<TH class=\"whiteBack\" width=\"" + fldWidthStr +"\">"+ StringUtil.decodeString((String)fldDispName.get(count)) + "</TH>");
			}





			int secIndex=-1;


		int totalRepFldCount = 0;
		int totalRepFldSecCount = 0;
		totalRepFldCount = mapSecIds.size();
		Hashtable htSecColumnCoumt = new Hashtable();



		if (mapSecIds!=null){
		if (mapSecIds.size()>0)
			{
				int repFldCountInASec = 0;
				int repSecCounter = 0;

			for (int count=0;count<mapSecIds.size();count++){
				mapSecId=(String)mapSecIds.get(count);
				mapSecId=(mapSecId==null)?"":mapSecId.trim();

				repSecCounter = count;

				if (mapSecId.length()>0){

				if (count==0 && mapSecIds.size() > 1)
				{
					prevSecId=(String)mapSecIds.get(count);
				}

				/*System.out.println("mapSecId" + mapSecId);
				System.out.println("prevSecId" + prevSecId);
				System.out.println("count" + count);
				System.out.println("(mapSecIds.size() - 1)" + (mapSecIds.size() - 1));*/

			    if ((mapSecId).equals(prevSecId) && (count < (mapSecIds.size() - 1)) )
			    	{
			    		repFldCountInASec = repFldCountInASec + 1;
			    		continue;
			    	}else
			    	{
			    		if ( mapSecIds.size() == 1) //one field one section
			    		{
			    			repFldCountInASec = 1;
			    		}else if ( count == (mapSecIds.size() - 1) ) //last field
			    		{
			    			if ( (mapSecId).equals(prevSecId))
			    			{
			    				repFldCountInASec = repFldCountInASec + 1;
			    			}

			    		}

			    		if (repSecCounter <= 0)
			    		{
			    			repSecCounter = 1;
			    		}
			    		tableWidth = tableWidth + (150 * repFldCountInASec);
			    		sbTempHeader.append("<TH class=\"whiteBack\"  width=\""+ (150 * repFldCountInASec) +"\" > " +  mapSecNames.get(repSecCounter -1) + " </TH>");
			    		htSecColumnCoumt.put((String)mapSecIds.get(repSecCounter -1), new Integer(repFldCountInASec) );

			    		totalRepFldSecCount = totalRepFldSecCount + 1;

			    		repFldCountInASec = 1;
			    		colInTableHeader = colInTableHeader + 1;

			    		if ((! (mapSecId).equals(prevSecId)) && mapSecIds.size() >1 && repFldCountInASec  == 1 && ((mapSecIds.size()-1) == repSecCounter )) //last field, and the section has only one field
			    		{

			    			tableWidth = tableWidth + (150 * repFldCountInASec);
			    			sbTempHeader.append("<TH class=\"whiteBack\"  width=\""+ (150 * repFldCountInASec) +"\" > " +   mapSecNames.get(repSecCounter) + " </TH>");
			    			htSecColumnCoumt.put((String)mapSecIds.get(repSecCounter), new Integer(repFldCountInASec) );
			    			colInTableHeader = colInTableHeader + 1;
			    			totalRepFldSecCount =totalRepFldSecCount + 1;

			    		}


			    	}

					} else {
					tableWidth = tableWidth + 150 ;
					colInTableHeader = colInTableHeader + 1  ;
					sbTempHeader.append("<TH class=\"whiteBack\" width=\"150\"></TH>");
					}

				prevSecId=(String)mapSecIds.get(count);

			  }

			}
		}
			if (lookupMode.equals("yes"))
			{
				 sbTempHeader.append("<TH width=\"150\">"+LC.L_Select/*Select*****/+"</TH>");
				 tableWidth = tableWidth + 150 ;
				 colInTableHeader = colInTableHeader + 1;
			}

			//sonia 02/08/07
			//for actual tyable width, add the number of columns + 1, this will include the space used by borders
			tableWidth = tableWidth + colInTableHeader + 1 ;
			if (totalRepFldCount > 1)
			{
			 	tableWidth  = tableWidth  + ( totalRepFldCount -1) ;
			}

			sbOutput.append("<table border=\"1\" width=\"" + tableWidth + "\" cellspacing=\"0\" cellpadding=\"0\">" + sbTempHeader.toString());

			sbOutput.append("</table>");

			sbOutputMain.append(sbOutput.toString());
			if (fmt.equals("H"))
			{
				out.println(sbOutput.toString());
			}
			sbOutput.delete(0, sbOutput.length());


		for (int i=0;i<dataList.size();i++)
		{
		  tempList = (ArrayList) dataList.get(i);
		  tempVal=new ArrayList();
		  lookupOutputStr = "";

		   if ((i%2)==0)
		   {
		  	 	sbOutput.append("<table border=\"1\" width=\"" + tableWidth + "\"  cellspacing=\"0\" cellpadding=\"0\"><tr class=\"plainRow\">");
		     }else
		     {
		     	 sbOutput.append("<table border=\"1\" width=\"" + tableWidth + "\"  cellspacing=\"0\" cellpadding=\"0\"><tr class=\"plainRow\">");
		      }

		   %>

		  <%if (tempList.size()==0) continue;
		   int maxTokens=0,tempCountToken=0;

		   for (int j=2;j<tempList.size();j++){
		  	ArrayList valList=new ArrayList();
		  	String fieldTypeStr = "";
		  	seq=0;

		  	fldColName = (String) fldCols.get(j-2) ;
		  	value=(String)tempList.get(j);
		  	fieldTypeStr = (String) fieldTypes.get(j-2);
		  	fldUseDataValue = (String) fieldDisplayDataValue.get(j-2)	;
		  	if (StringUtil.isEmpty(fldUseDataValue))
		  	{
		  		fldUseDataValue = "0";
		  	}

		  	fldWidthStr = (String)fldWidth.get(j-2);
			if (StringUtil.isEmpty(fldWidthStr) || fldWidthStr.equals("0"))
				{
				 fldWidthStr = "15";
				}

			//temp fix
			fldWidthStr = "150";

			if (value!=null){
			if (value.indexOf("[VELSEP]")>=0){
			value=StringUtil.replaceAll(value,"[VELSEP]","~");
			StringTokenizer valSt=new StringTokenizer(value,"~");
			tempCountToken=valSt.countTokens();
			//valList.add(valSt);
			tempVal.add(valSt);
			if (tempCountToken>maxTokens)	maxTokens=tempCountToken;
			continue;
			//value=EJBUtil.replaceAll(value,"[VELSEP]",", ");
			}
			 //check if its a date field , format the date




			}
			//System.out.println("EOL"+System.getProperty("line.separator"));
			//if (value!=null) value=StringUtil.replace(value,"[VELSEP]","<br>");
			else value="&#160;";
			fldDataSeperatorPos = value.indexOf("[VELSEP1]");


			if (fldDataSeperatorPos >=0){
				if (fldUseDataValue.equals("0"))
		 		{
					value=value.substring(0,fldDataSeperatorPos);
		 		}
				else
				{
					value=value.substring(fldDataSeperatorPos + 9);
				}
		 	}
			value=StringUtil.replaceAll(value,"[VELCOMMA]",",");


			sbOutput.append("<td style=\"word-wrap:break-word\"");
			//System.out.println("fldWidthStr" + fldWidthStr + "*");
			 if (!(fldWidthStr.trim().equals("0")) )
			 {
				sbOutput.append("width=\""+fldWidthStr+"\"");
			}

			sbOutput.append(">"+value+"</td>");
			/////////INSERT HERE

			// in case of lookupMode
				if (lookupMode.equals("yes"))
				{
					int lIndex = 0;
					int lastIndex = -1;

					//see if fldColName exists in the keyword arrayList

					while ( lIndex >=0 )
					{

						lIndex = urlKeywords.indexOf(fldColName,lIndex);
						lastIndex = urlKeywords.lastIndexOf(fldColName);

						if (lIndex  == -1) break;
						if (lIndex >= 0)
						{
							if (lookupOutputStr.length()>0)
							lookupOutputStr =lookupOutputStr +"[end]"+ "[field]"+urlFields.get(lIndex)+"[keyword]"+urlKeywords.get(lIndex)+"[dbcol]"+urlKeywords.get(lIndex) + "[value]"+ value;
						else lookupOutputStr = "[field]"+urlFields.get(lIndex)+"[keyword]"+urlKeywords.get(lIndex)+"[dbcol]"+urlKeywords.get(lIndex) + "[value]"+ value;
						}
						lIndex = lIndex + 1;
					}
				}

		  }

		 if (repFldNames.size()>0){
		 	 	fldWidthStr = "146";
		 	 	String repCurSecId = "";
		 	 	int repFieldsInThisSection = 0;

		 	 	repCurSecId =  (String)mapSecIds.get(0);
		 	 	repFieldsInThisSection = ((Integer)htSecColumnCoumt.get(repCurSecId)).intValue();
		 	 	if (repFieldsInThisSection == 0)
		 	 	{
		 	 		repFieldsInThisSection = 1;
		 	 	}

				sbOutput.append("<td width=" + (repFieldsInThisSection * 150) + " valign=top><table border=\"0\"   cellspacing=\"0\" cellpadding=\"0\" >");

		 		int colCounter=0,counter=0,toCount=0,currCount=0;
		  		String currSec="",prevSec="";

		  		String dispVal="";
		  		StringTokenizer tempSt;

				while (colCounter<repFldNames.size())
				{
		 		if (mapSecNames.size()>0)
		 		currSec=(String)mapSecNames.get(colCounter);
		 		//do the processing here to display data for the repeated fields
		 		toCount=(colCounter);


				if ((colCounter>0) && !currSec.equals(prevSec))
				{
					if (maxTokens>0)
					{
						for (int tCount=1;tCount<=maxTokens;tCount++)
						{
							sbOutput.append("<tr>");
							 counter=0;



							while ((counter + currCount )<toCount){
		   					dispVal="";
		   					tempSt=((StringTokenizer)tempVal.get(counter+currCount));
		      				if (tempSt.hasMoreTokens()) dispVal=(String)tempSt.nextToken();
		      				dispVal=(dispVal==null)?"":dispVal;
		      				 fldUseDataValue = (String) repFieldDisplayDataValue.get(counter+currCount);
						     fldDataSeperatorPos = dispVal.indexOf("[VELSEP1]");

								if (fldDataSeperatorPos >=0){
									if (fldUseDataValue.equals("0"))
									{
										dispVal = dispVal.substring(0,fldDataSeperatorPos);
									}
									else
									{
										dispVal = dispVal.substring(fldDataSeperatorPos + 9);
									}
								}
						     dispVal = StringUtil.replaceAll(dispVal,"[VELCOMMA]",",");

		      				sbOutput.append("<td style=\"word-wrap:break-word\"");

							   if (!(fldWidthStr.trim().equals("0")) )
								 {
									sbOutput.append("width=\""+fldWidthStr+"\"");
								}


		      				sbOutput.append(">"+dispVal+"</td>");

							counter++;
		 				}
		 			sbOutput.append("</tr>");
				 	}
		 	currCount=toCount;
		  }
		//end processing here
			sbOutput.append("</table></td>");

			repCurSecId =  (String)mapSecIds.get(colCounter);
		 	 repFieldsInThisSection = ((Integer)htSecColumnCoumt.get(repCurSecId)).intValue();
		 	 	if (repFieldsInThisSection == 0)
		 	 	{
		 	 		repFieldsInThisSection = 1;
		 	 	}

			sbOutput.append("<td width= " + (repFieldsInThisSection * 150) + " valign=top><table border=\"0\"   cellspacing=\"0\" cellpadding=\"0\">");

		if (((String)repFldNames.get(colCounter)).length()>=10)
		{
			sbOutput.append("<th width=\"" + fldWidthStr + "\"><font size=\"1\">" + ((String)repFldNames.get(colCounter)).substring(0,10) + "</font></th>");
		}
		 else {
		 	 sbOutput.append("<th width=\"" + fldWidthStr + "\" ><font size=\"1\">" + ((String)repFldNames.get(colCounter)) + "</font></th>");

		 }
		}
		 else
		  {
			 if (((String)repFldNames.get(colCounter)).length()>=10){
			 	 sbOutput.append("<th width=\"" + fldWidthStr + "\"><font size=\"1\"> " + ((String)repFldNames.get(colCounter)).substring(0,10) + "</font></th>");
			}else{
				sbOutput.append("<th width=\"" + fldWidthStr + "\"><font size=\"1\">" + ((String)repFldNames.get(colCounter))+ "</font></th>");
			}

		}
		 colCounter++;
		 prevSec=currSec;
		}

		 //do this processing to display the elements of last section to the end of list

		if ((toCount+1) == repFldNames.size())
			if (maxTokens>0){
			for (int tCount=1;tCount<=maxTokens;tCount++){
			sbOutput.append("<tr>");
			%>

			<%
			counter=0;

			while ((counter + currCount )<toCount+1){
			   dispVal="";
			   tempSt=((StringTokenizer)tempVal.get(counter+currCount));
			   fldUseDataValue = (String) repFieldDisplayDataValue.get(counter+currCount);

			     if (tempSt.hasMoreTokens()) dispVal=(String)tempSt.nextToken();
			     dispVal=(dispVal==null)?"":dispVal;
			           fldDataSeperatorPos = dispVal.indexOf("[VELSEP1]");

					if (fldDataSeperatorPos >=0){
						if (fldUseDataValue.equals("0"))
						{
							dispVal = dispVal.substring(0,fldDataSeperatorPos);
						}
						else
						{
							dispVal = dispVal.substring(fldDataSeperatorPos + 9);
						}
					}
			      dispVal = StringUtil.replaceAll(dispVal,"[VELCOMMA]",",");


			     sbOutput.append("<td style=\"word-wrap:break-word\"");

			      if (!(fldWidthStr.trim().equals("0")) )
					 {
						sbOutput.append("width=\""+fldWidthStr+"\"");
					}


			     sbOutput.append(">"+dispVal+"</td>");

				counter++;
			 }
			 sbOutput.append("</tr>");

			 }
			 currCount=toCount;
			 //counter=toCount;
			 }



		//End for do this processing to display the elements of last section to the end of list

			sbOutput.append("</table></td>");
		 }//end for repFldNames.size()>0

		if (lookupMode.equals("yes"))
			{
				sbOutput.append("<td><A HREF='#' onClick=\"setValue(document.preview,'" + StringUtil.encodeString(lookupOutputStr) +"','')\">"+LC.L_Select/*Select*****/+"</A> </td>");
			}

		sbOutput.append("</tr></table>");

		sbOutputMain.append(sbOutput.toString());
		 if (fmt.equals("H"))
			{
				out.println(sbOutput.toString());
			}
		sbOutput.delete(0, sbOutput.length());

		}
		//sbOutput.append("</table>");
	} // table for patient and form .........end for formids loops

		sbOutput.append("<BR>");
		sbOutputMain.append(sbOutput.toString());
		 if (fmt.equals("H"))
			{
				out.println(sbOutput.toString());
			}
		sbOutput.delete(0, sbOutput.length());


	} //for "L" and "X" check

		 String xmlStr = "";

	if (fmt.equals("L") || fmt.equals("X") )
	{
		//rdHolder = (ReportDataHolder) EJBUtil.readSerializedObject(fileNameForLinear);
 		//xmlStr = rdHolder.getReportXML();
 		xmlStr = rdHolder.getLinearXML();
 		xmlStr = StringUtil.replace(xmlStr,"&amp;#","&#");

 	}
 		//System.out.println(xmlStr );

%>

<input type="hidden" name="tempdata_look" value="">
<input type="hidden" name="lookupformName"  value="<%=lookupformName%>">
<input type="hidden" name="from"  value="<%=from%>">
<input type="hidden" name="repId"  value=<%=repIdStr%>>
<input type="hidden" name="filter"  value='<%=StringUtil.encodeString(filter)%>'>
<input type="hidden" name="exp"  value="<%=fmt%>">
<input type="hidden" name="fltrId"  value=<%=fltrId%>>

<input type="hidden" name="repHeader"  value="<%=repHeader%>">
<input type="hidden" name="repFooter"  value="<%=repFooter%>">
<input type="hidden" name="headerAlign"  value="<%=headAlign%>">
<input type="hidden" name="footerAlign"  value="<%=footAlign%>">
<input type="hidden" name="sess" value="keep">

<input type="hidden" name="fkeyword_str"  value="<%=fkeyword_str%>">
<input type="hidden" name="lookupMode"  value="<%=lookupMode%>">
<input type="hidden" name="form"  value="<%=lookupformName%>"> <!-- for lookup-->
<input type="hidden" name="keyword"  value="<%=fkeyword_str%>">	<!-- for lookup-->
<input type="hidden" name="formPatient"  value="<%=formPatient%>">
<input type="hidden" name="formType"  value="<%=formType%>">
<input type="hidden" name="calledfrom" value="<%=calledFrom %>" />

<%
	sbOutput.append("<table width=\"100%\">");
	sbOutput.append("<hr class=\"thickLine\">");
	sbOutput.append("<tr height=\"20\"></tr>");

if (repFooter.length()>0)
{
		sbOutput.append("<tr><td class=\"reportFooter\" align=\"" + footAlign+"\"><pre>"+repFooter+"</pre></td></tr>");
}
	sbOutput.append("</table>");

	sbOutput.append("</form>");

	sbOutputMain.append(sbOutput.toString());
	str = sbOutput.toString();
	//to encode special characters e.g.  ®,©,™,²
	str = StringUtil.htmlEncode(str, encodeAll);
	if (fmt.equals("H"))
	{
		out.println(str);
	}
	sbOutput.delete(0, sbOutput.length());
	String filename = "";
	String contents = null;
	String fileDnPath = null;
	String requestURL  = (request.getRequestURL()==null) ? "" : (request.getRequestURL().toString());
	String queryString = (request.getQueryString()==null) ? "" : (request.getQueryString().toString());
if (fmt.equals("W"))
{
	//String contents = null;
	str = sbOutputMain.toString();
	//to encode special characters e.g.  ®,©,™,²
	str = StringUtil.htmlEncode(str, encodeAll);
	contents = ReportIO.saveReportToDoc(str,"doc","dyndoc");
	//String filename = "";
	if (contents.indexOf("file=") > 0 && contents.length() > contents.indexOf("file=")) {
	    filename = contents.substring(contents.indexOf("file=")+5);
	    fileDnPath =StringUtil.encodeString(com.aithent.file.uploadDownload.Configuration.DOWNLOADFOLDER); 
	}
	%>
    <input type="hidden" id="file" name="file" value="<%=filename %>" />
    <input type="hidden" id="key" name="key" value="<%=sessId %>" />
    <input type="hidden" name="repId" value="<%=repIdStr %>" />
    <input type="hidden" name="requestURL" value="<%=requestURL %>" />
    <input type="hidden" name="queryString" value="<%=queryString %>" />
    <input type="hidden" name="filePath" value="<%=fileDnPath %>" />
    <input type="hidden" name="moduleName" value="<%=calledFrom%>" />
	<script>
	switchToDownload(document.preview);
	</script>
	<%
	} else if (fmt.equals("E"))
	{
	//String contents = null;
	str = sbOutputMain.toString();
	//to encode special characters e.g.  ®,©,™,²
	str = StringUtil.htmlEncode(str, encodeAll);
	contents = ReportIO.saveReportToDoc(str,"xls","dynexcel");
	//String filename = "";
	if (contents.indexOf("file=") > 0 && contents.length() > contents.indexOf("file=")) {
	    filename = contents.substring(contents.indexOf("file=")+5);
	    fileDnPath =StringUtil.encodeString(com.aithent.file.uploadDownload.Configuration.DOWNLOADFOLDER); 
	}
	%>
    <input type="hidden" id="file" name="file" value="<%=filename %>" />
    <input type="hidden" id="key" name="key" value="<%=sessId %>" />
    <input type="hidden" name="repId" value="<%=repIdStr %>" />
    <input type="hidden" name="requestURL" value="<%=requestURL %>" />
    <input type="hidden" name="queryString" value="<%=queryString %>" />
    <input type="hidden" name="filePath" value="<%=fileDnPath %>" />
    <input type="hidden" name="moduleName" value="<%=calledFrom%>" />
	<script>
	switchToDownload(document.preview);
	</script>
<%}
	else if (fmt.equals("L")) //linear format (for showing all forms in one table-row format)
	{
		String xslPath = "";
		xslPath = Configuration.ERES_HOME + "adHocLinearNew.xsl";
		//System.out.println("xmlStr" + xmlStr);

	%>
		<jsp:include page="transformAdHoc.jsp" flush="true">
	     	<jsp:param name="xmlString" value="<%=xmlStr %>"/>
			<jsp:param name="xslFileName" value="<%=xslPath%>"/>
		 	<jsp:param name="xslSrc" value="file"/>
		 	<jsp:param name="repHeader" value="<%=repHeader%>" />
		 	<jsp:param name="repFooter" value="<%=repFooter%>" />
			<jsp:param name="calledfrom" value="<%=calledFrom%>" />
			<jsp:param name="repId" value="<%=repIdStr %>" />
		</jsp:include>

  <%
	} else if (fmt.equals("X")) //xml file
	{
		//String contents = null;
		contents = ReportIO.saveReportToDoc(xmlStr,"xml","dynxml");
		//String filename = "";
		if (contents.indexOf("file=") > 0 && contents.length() > contents.indexOf("file=")) {
		    filename = contents.substring(contents.indexOf("file=")+5);
		    fileDnPath =StringUtil.encodeString(com.aithent.file.uploadDownload.Configuration.DOWNLOADFOLDER); 
		}
		%>
        <input type="hidden" id="file" name="file" value="<%=filename %>" />
        <input type="hidden" id="key" name="key" value="<%=sessId %>" />
        <input type="hidden" name="repId" value="<%=repIdStr %>" />
        <input type="hidden" name="requestURL" value="<%=requestURL %>" />
    	<input type="hidden" name="queryString" value="<%=queryString %>" />
    	<input type="hidden" name="filePath" value="<%=fileDnPath %>" />
    	<input type="hidden" name="moduleName" value="<%=calledFrom%>" />
		<script>
		switchToDownload(document.preview);
		</script>
	<%
	}
	else
	{%>

	<%
	}
/*Changes for: INF-22402 Date:10/04/2012 By:Yogendra Pratap Singh*/
	int downloadFlag=0;
	String downloadFormat = null;
	String isPreview = request.getParameter("isPreview");
	if(null==isPreview)isPreview="";
	if ( fmt.equals("H")){
		downloadFormat = LC.L_Printer_FriendlyFormat;
		downloadFlag=1;
	}
	
	if (fmt.equals("H") && !fmt.equals("L")){
	if(!isPreview.equalsIgnoreCase("Preview")){
	%>
	<jsp:include page="bottompanel.jsp" flush="true"/>
	<jsp:include page="userReportLogging.jsp" flush="true">
		<jsp:param 	value="<%=repIdStr %>" 		name="repId"/>
		<jsp:param 	value="<%=filename %>"		name="fileName"/>
		<jsp:param 	value="<%=fileDnPath %>"	name="filePath"/>
		<jsp:param 	value="<%=calledFrom%>"		name="moduleName"/>
		<jsp:param 	value="" 					name="repXml"/>
		<jsp:param 	value="<%=downloadFlag%>"	name="downloadFlag"/>
		<jsp:param value="<%=downloadFormat %>" name="downloadFormat"/>
	</jsp:include>
<%
	}
	}
} else {  //else of if body for session
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
</body>
</html>

