<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Del_FormResponse%><%--Delete Form Response*****--%></title>
<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>
<SCRIPT>

$j(document).ready(function(){
	$j('#reason_del').keyup(function(){
		limitChars('reason_del', 4000, 'charlimitinfo');
	});
});

function  validate(formobj){
	var fdaRegulated = document.getElementById("FDARegulated").value;
	var reasonDel = document.getElementById("reason_del").value;
	reasonDel=reasonDel.replace(/^\s+|\s+$/g,"");
	if(fdaRegulated=="1" && reasonDel.length<=0)
	{
		alert("<%=MC.M_Etr_MandantoryFlds%>");
		document.getElementById("reason_del").focus();
		return false;
			
	}
	
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.aithent.audittrail.reports.AuditUtils"%>
<%@ page language = "java" import="com.velos.eres.web.user.UserJB"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 
<jsp:useBean id="lnkformsB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />

<% String src;
src= request.getParameter("srcmenu");
String calledFromForm = "";
       calledFromForm = request.getParameter("calledFromForm");	 
     
      if (StringUtil.isEmpty(calledFromForm))
      {
     	calledFromForm = "";
      }
      
            
	String showPanel= "";
       showPanel= request.getParameter("showPanel");	 
     
      if (StringUtil.isEmpty(showPanel))
      {
     	showPanel= "true";
      }

	String formCategory = request.getParameter("formCategory");	 
		 String submissionType = request.getParameter("submissionType");	 

    if (StringUtil.isEmpty(formCategory))
			 {
			 	 formCategory="";
			 }
			 
			 if (StringUtil.isEmpty(submissionType))
			 {
			 	 submissionType="";
			 }
			


   
   if (calledFromForm.equals("") && showPanel.equals("true"))   
   {

%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  
 <% }
 else
 { %>
 	<jsp:include page="include.jsp" flush="true"/>
 	<jsp:include page="popupJS.js" flush="true"/>
 		<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
	<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
	<SCRIPT LANGUAGE="JavaScript" SRC="formjs.js"></SCRIPT>	
 	<%	
 }
 %>
<BODY> 
<br>
<%
	 String divClass = "";
	 String idClass= "";
	 
	if (calledFromForm.equals(""))   
    {
    	divClass = "formDefault";
    	idClass = "div1";
    	
    }
    else
    {
        divClass = "popDefault";
    }	
%>
	<DIV class="<%=divClass%>" id="<%=idClass%>">

<% 

HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))	{ 	
		int ret=0;
 	    String filledFormId = request.getParameter("filledFormId");
 	    String formDispLocation = request.getParameter("formDispLocation");						

 	    String selectedTab = request.getParameter("selectedTab");
 	    String patientId = request.getParameter("pkey");
 	    String patProtId = request.getParameter("patProtId");
 	    String statDesc = request.getParameter("statDesc");
 	    String statid = request.getParameter("statid");
 	    String patientCode = request.getParameter("patientCode");
 	    String formFillDt = request.getParameter("formFillDt");
 	    String formPullDown = request.getParameter("formPullDown");
 	    String studyId = request.getParameter("studyId");	
 	    
 	    
 	   String responseId=request.getParameter("responseId");
 		responseId=(responseId==null)?"":responseId;
 		
 		String outputTarget = "";
		outputTarget = request.getParameter("outputTarget");
		
		if(StringUtil.isEmpty(outputTarget))
		{
			outputTarget = "";
		}   
	
 		
 		String parentId=request.getParameter("parentId");
 		parentId=(parentId==null)?"":parentId;

 	    String schevent = "";
 	    schevent = request.getParameter("schevent");		
		
		if (StringUtil.isEmpty(schevent ))
		{
			schevent = "";
		}
				
														
		
		String fdaRegulated="";
		studyId = studyId==null?"0":studyId;
		studyB.setId(EJBUtil.stringToNum(studyId));
	    	studyB.getStudyDetails();
	    	fdaRegulated=studyB.getFdaRegulatedStudy();
		fdaRegulated = fdaRegulated==null?"0":fdaRegulated;
		String delMode=request.getParameter("delMode");
		if (delMode==null) delMode="initial";
	
		if (delMode.equals("initial")) {
			delMode="final";				
%>
  

	<FORM name="formrespdelete" id="frmresdel" method="post" action="formrespdelete.jsp" onSubmit="if (validate(document.formrespdelete)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr>
	<td align="right" valign="top" width="50%"><%=LC.L_Reason_ForDelete %>
	<%if(fdaRegulated.equals("1")) {%>
	<font class="Mandatory">*</font>
	<%} %>&nbsp;
	</td>
	<Input type="hidden" name="FDARegulated" id="FDARegulated" value="<%=fdaRegulated %>">
	<td><textarea maxlength="4000" rows = "4" name="reason_del" id="reason_del"></textarea>
	<br><font class="Mandatory"><div id="charlimitinfo"><%=MC.M_Limit4000_CharLeft %></div></font>
	</td>
	</tr>
	</table>
	<P class="defComments"><%=MC.M_Etr_Esign_ToProc%><%--Please enter e-Signature to proceed*****--%></P>
		
<%
        UserJB userB = (UserJB) tSession.getValue("currentUser");
        int userId=userB.getUserId();
	    String accId = (String) tSession.getValue("accountId");
%>	
	
   <input id="sessUserId" name="sessUserId" value="<%=userId%>" type="hidden">
   <input type="hidden" id="sessAccId" value="<%=accId%>">
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="frmresdel"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
	

 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
   	 <input type="hidden" name="selectedTab" value="<%=selectedTab%>">
   	 <input type="hidden" name="filledFormId" value="<%=filledFormId%>">
   	 <input type="hidden" name="formDispLocation" value="<%=formDispLocation%>">	 	 	 

 	 <input type="hidden" name="pkey" value="<%=patientId%>">
  	 <input type="hidden" name="patProtId" value="<%=patProtId%>">
   	 <input type="hidden" name="statDesc" value="<%=statDesc%>">
   	 <input type="hidden" name="statid" value="<%=statid%>">
   	 <input type="hidden" name="patientCode" value="<%=patientCode%>">	 	 	 
   	 <input type="hidden" name="formFillDt" value="<%=formFillDt%>">
   	 <input type="hidden" name="formPullDown" value="<%=formPullDown%>">
   	 <input type="hidden" name="studyId" value="<%=studyId%>">	 
   	 <input type="hidden" name="calledFromForm" value=<%=calledFromForm%>>	 	 
   	 <input type="hidden" name="schevent" value=<%=schevent%>>	 	 
   	 <input type="hidden" name="responseId" value=<%=responseId%>>
   	 <input type="hidden" name="parentId" value=<%=parentId%>>
   	 <input type="hidden" name="outputTarget" value=<%=outputTarget%>>
   	 <input type="hidden" name="showPanel" value=<%=showPanel%>>
   	   <input type="hidden" name="submissionType" value=<%=submissionType%>>
		<input type="hidden" name="formCategory" value=<%=formCategory%>>
   	 
	 
	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {
			// Modified for INF-18183 ::: AGodara
			String reason_del = request.getParameter("reason_del");
			reason_del = reason_del.trim();
			reason_del = reason_del==null?"":reason_del;
			ret = lnkformsB.deleteFilledFormResp(filledFormId,formDispLocation,AuditUtils.createArgs(tSession,reason_del,LC.L_Study));   //Changed for Bug#7604 : Raviesh

			if (ret==-1) {%>
			<br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_Data_NotRemSucc%><%--Data not removed successfully*****--%> </p>			
			<%}else { 
			 //incase of deletion, reduce the number of entries in form
		
			 if ((!(formPullDown==null)) && (!(formPullDown.equals("null")))) {
			 	StringTokenizer strTokenizer = new StringTokenizer(formPullDown,"*");
	 	     	String formId = strTokenizer.nextToken();
			    String entryChar = strTokenizer.nextToken();
		        int numEntries = EJBUtil.stringToNum(strTokenizer.nextToken()) - 1;
			    formPullDown = formId + "*"+ entryChar + "*" + numEntries;
			}	   %>
			
			<br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_Data_DelSucc%><%--Data deleted successfully*****--%> </p>
			
					
				<% 
				if (! StringUtil.isEmpty(outputTarget))
				{
					%>
						<script>
							  if (window.opener != undefined)
							  {
							  	<%
							  		out.println(" obj = window.opener.document.getElementById('" + outputTarget + "');" );
							  		out.println(" obj.click();");
								%>	
								 
								 window.opener = null;					  	
							     setTimeout("self.close()",100);
							  }	
						</script>
					
					<%
				
				} 
			%>
			
			
			
			<%}
			if (formDispLocation.equals("PA"))
			{
		%>
		    <META HTTP-EQUIV=Refresh CONTENT="2; URL=formfilledpatbrowser.jsp?calledFromForm=<%=calledFromForm%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&pkey=<%=patientId%>&patProtId=<%=patProtId%>&calledFrom=S&statDesc=<%=statDesc%>&statid=<%=statid%>&patientCode=<%=patientCode%>&formFillDt=<%=formFillDt%>&formPullDown=<%=formPullDown%>">
		<%
			}
		else if(formDispLocation.equals("S") || formDispLocation.equals("SA") )
			 {
		     String irbReviewForm = "";
		     if (tSession.getAttribute("IRBParams")!= null) { irbReviewForm = "true"; }
		%>
			<META HTTP-EQUIV=Refresh CONTENT="2; URL=formfilledstudybrowser.jsp?formCategory=<%=formCategory%>&submissionType=<%=submissionType%>&irbReviewForm=<%=irbReviewForm%>&showPanel=<%=showPanel%>&calledFromForm=<%=calledFromForm%>&srcmenu=<%=src%>&studyId=<%=studyId%>&selectedTab=<%=selectedTab%>&formFillDt=<%=formFillDt%>&formPullDown=<%=formPullDown%>">
		<%
			}
		else if (formDispLocation.equals("A"))//called from Account
			{
		%> 
		<META HTTP-EQUIV=Refresh CONTENT="2; URL=formfilledaccountbrowser.jsp?calledFromForm=<%=calledFromForm%>&srcmenu=<%=src%>&formFillDt=<%=formFillDt%>&formPullDown=<%=formPullDown%>"> 
		<%
			} 
		else if (formDispLocation.equals("SP"))
			{
		
			if (! StringUtil.isEmpty(schevent) ) // deleted for CRF form
			{
				%>
				<script>
					window.opener.location.reload();
					
				</script>
				
			<% } %>	
		<META HTTP-EQUIV=Refresh CONTENT="2; URL=formfilledstdpatbrowser.jsp?calledFromForm=<%=calledFromForm%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&pkey=<%=patientId%>&mode=M&patProtId=<%=patProtId%>&studyId=<%=studyId%>&calledFrom=S&statDesc=<%=statDesc%>&statid=<%=statid%>&patientCode=<%=patientCode%>&formFillDt=<%=formFillDt%>&formPullDown=<%=formPullDown%>&fk_sch_events1=<%=schevent%>&responseId=<%=responseId%>&parentId=<%=parentId%>">
		<%
			}
	    //added by salil for crf forms
		else if (formDispLocation.equals("C"))
			{
		%>		
		<META HTTP-EQUIV=Refresh CONTENT="2; URL=patientschedule.jsp?calledFromForm=<%=calledFromForm%>&srcmenu=tdMenuBarItem5&selectedTab=3&mode=M&pkey=<%=patientId%>&patProtId=<%=patProtId%>&patientCode=<%=patientCode%>&page=patientEnroll&studyId=<%=studyId%>&generate=N&statDesc=<%=statDesc%>&statid=<%=statid%>&studyVer=null">		
		<%
		    }
		} //end esign
	} //end of delMode	
  }//end of if body for session 
else { %>
 <jsp:include page="timeout.html" flush="true"/> 
 <% } %>
 
 <%if (calledFromForm.equals("") || "formpreview".equals(calledFromForm))   
   {
%>	

 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
  <%
 } 
  %>
</DIV>

  <%if (calledFromForm.equals(""))   
   {
%>	

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/> 
</div>
<% } %>
</body>
</HTML>


