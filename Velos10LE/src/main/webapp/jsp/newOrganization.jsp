<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>



<title>
	<%=MC.M_StdTeam_OrgDets%><%--<%=LC.Std_Study%> >> Team >> Organization Details*****--%>
</title>



</head>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
 <SCRIPT Language="javascript">
   // Added by RA Bug #4079 Confirmation message at the time of delete
	function confirmBox(fileName,pgRight,orgRight) {
		var paramArray = [fileName];
		if (confirm(getLocalizedMessageString("M_Del_FrmAtchmt",paramArray))) {/*if (confirm("Delete " + fileName + " from attachments?")) {*****/
		    return true;}
		else
		{
			return false;
		}

	}

	function fdownload(formobj,pk,filename,dnldurl)
	{
		formobj.file.value = filename;
		formobj.pkValue.value = pk ;
		formobj.dnldurl.value=dnldurl;
		formobj.action="postFileDownload.jsp";
		//formobj.action=dnldurl;

		formobj.target = "_filedownload";
		formobj.method = "POST";
		formobj.submit();
		formobj.target="";
	}

	function openLookup(formobj,url) {

	 // process the depend String to see if current lookup depend on some other values

	var tempValue;
	var urlL="";
		formobj.target="Lookup";
	formobj.method="post";
	urlL="multilookup.jsp?"+url;
	 formobj.action=urlL;

	formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');
	if (formWin && !formWin.closed) formWin.focus();
	formobj.submit();
	formobj.target="";
	}

	  function getUser(formobj,dispFld,idFld)
   {

         var names = dispFld.value;
         var ids = idFld.value;

         var idFldName ;
         var dispFldName;
         var formname = formobj.name;

         idFldName = idFld.name;
         dispFldName = dispFld.name;

         while(names.indexOf(' ') != -1){

             names = names.replace(' ','~');

         }
         window.open("multipleusersearchdetails.jsp?dataFld=" + idFldName + "&dispFld="+ dispFldName  +"&fname=&lname=&from="+formname+"&mode=initial&ids=" + ids + "&names=" + names ,"User","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")  ;


   }


function setDD(formobj)
{
	optvalue=formobj.ddlist.value;

	if (optvalue.length>0)
	{

		arrayofStrings=optvalue.split("||");

		if (arrayofStrings.length>1) {
			for (var j=0;j<arrayofStrings.length;j++)
				{
				var ddStr=arrayofStrings[j];

				arrayofDD=ddStr.split(":");

				var ddcount=arrayofDD[0];
				var selvalue=arrayofDD[1];
				var opt = formobj.data[ddcount].options;
				for (var i=0;i<opt.length;i++){
				if (opt[i].value==selvalue){
				formobj.data[ddcount].selectedIndex=i ;
					}
			}
		}
	} else
	{
	var ddStr=arrayofStrings[0];
	arrayofDD=ddStr.split(":");
	var ddcount=arrayofDD[0];
	var selvalue=arrayofDD[1];
	var opt = formobj.data[ddcount].options;
	for (var i=0;i<opt.length;i++){
	if (opt[i].value==selvalue){
	formobj.data[ddcount].selectedIndex=i ;
	}
	}
	}// end else
	}//optvalue.length>0
}


   function setValue(formobj,count,cbcount){
		value=formobj.data[count].value;
		totalcbcount=formobj.cbcount.value;
		if (value=="Y") {
		formobj.data[count].value="N";
		if (totalcbcount==1) formobj.datachk.checked=false;
		else formobj.datachk[cbcount-1].checked=false;
		}
		if ((value.length==0) || (value=="N"))  {
		formobj.data[count].value="Y";
		if (totalcbcount==1) formobj.datachk.checked=true;
		else formobj.datachk[cbcount-1].checked=true;
		}
}


    function toRefresh(formobj)
{
	//	if(formobj.toRefresh.value=="yes")
	//	{
			window.opener.location.reload();
	//	}
}

    function openwin() {
		    window.open("","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=600,height=450")
		;}



    function checkToOpen(mode)
    {
    	if(mode == "M")
    		return true;
    	else{
    		alert("<%=MC.M_Save_OrgDetsFirst%>");/*alert("Please Save the Organization Details first");*****/
    		return false;
    	}

    }

 function  validate(formobj){
	document.neworg.flag.value = "2"
    if (!(validate_col('Organization',formobj.accsites))) return false
  	  if(isNaN(formobj.sampleSize.value) == true)
		   {
	     		alert("<%=MC.M_ValidNum_SampleSize%>");/*alert("Please enter a valid number in Local Sample Size.");*****/
				 formobj.sampleSize.focus();
				 return false;
			}
  	  if(formobj.repFlag.checked == true){

  	    formobj.repFlag.value= "1";
  }
  else{

  	  formobj.repFlag.value= "0";

  }


    if (!(validate_col('e-Signature',formobj.eSign))) return false


	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   	}

   	formobj.method="post";
	formobj.action="saveNewOrganization.jsp";


   }

   //Added by Manimaran to fix the Bug2791
   function flag0() {
       document.neworg.flag.value = "2"
   }

   function forrefresh() {

	   if (document.neworg.flag.value == "0"  || document.neworg.cnt.value == 1 || document.neworg.cnt1.value == 1 ) { //KM
	   	   window.opener.location.reload();
	   }

	   if (document.neworg.cnt.value == 0 && document.neworg.cnt1.value == 0 ) { //KM
	   	   window.opener.location.reload();
	   }

   }


	function changeTitle (flag){

	document.title=flag
	}

	//Added for Bug#8878 : Raviesh
	function enableSubmit()
	 {
		 document.getElementByID("submit_btn").disabled=false;
	 }
	 
</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="studySiteB" scope="request" class="com.velos.eres.web.studySite.StudySiteJB"/>

<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>

<jsp:useBean id="studySiteApndxB" scope="request" class="com.velos.eres.web.studySiteApndx.StudySiteApndxJB"/>

<jsp:useBean id="addInfo" scope="request" class="com.velos.eres.web.stdSiteAddInfo.StdSiteAddInfoJB"/>

<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/>


<%@ page language = "java" import = "com.velos.eres.business.common.*,com.aithent.file.uploadDownload.*,java.util.*,com.velos.eres.business.user.impl.*,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,java.util.*,java.io.*,org.w3c.dom.*"%>
<%@ page import="com.velos.eres.service.util.LC" %><%@ page import="com.velos.eres.service.util.MC" %>

<% String src;

//src= request.getParameter("srcmenu");


int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1)
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {
%>
<body style="overflow:scroll;" onLoad="setDD(document.neworg)" onUnload="forrefresh()"> <!--KM-->
<%
	} else {
%>
<body onLoad="setDD(document.neworg)" onUnload="forrefresh()">
<%
	}
%>
<br>
<DIV class="popDefault" id="div1">

  <%

	HttpSession tSession = request.getSession(true);



	//String categoryName ="";
	//String categoryDesc ="";
	String mode=request.getParameter("mode");

	int pageRight = 4;
	String dAccSites="";

	//int catLibId=0;

	if (sessionmaint.isValidSession(tSession))
	{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>
<%
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
	CodeDao cd = new CodeDao();
	CodeDao cdType= new CodeDao();
	if (pageRight >=4)
	{

		 String studyId= request.getParameter("studyId");


		 String txtdesc="";
		 String reportFlag = "";
		 String publicFlag = "";
		 String siteType = "";
		 String siteId = request.getParameter("siteId");
		 String sampleSize = "";
		 String siteName = "";

		 int studySiteId = 0;
		 if(siteId == null)
		 	 siteId = "";


		 String acc = (String) tSession.getValue("accountId");
		 String dSiteType = "";
		 int accId = EJBUtil.stringToNum(acc);
		 cd.getAccountSites(accId);
		 dAccSites = cd.toPullDown("accsites");

		 cdType.getCodeValues("studySiteType");
         dSiteType = cdType.toPullDown("siteType");

         String enrollmentType = "0";
         String sendEnrNotificationTo = "";
         String sendEnrStatusNotificationTo = "";
         String checkedString = "";
         String sendEnrNotificationNames = "";
         String sendEnrStatusNotificationNames = "";

		//Added by Manimaran to fix the Bug2791
         String flag = request.getParameter("flag");
		 if (flag == null)
			  flag ="3";

		if (mode.equals("M"))
		{
		%>
		<SCRIPT>
		document.title="<%=LC.L_Edit_Org%>";/*document.title="Edit Organization";*****/

		</Script>
		<%
			studySiteId= EJBUtil.stringToNum(request.getParameter("studySiteId"));

			studySiteB.setStudySiteId(studySiteId);


			studySiteB.getStudySiteDetails();
			siteId = studySiteB.getSiteId();

			dAccSites = cd.toPullDown("accsites",EJBUtil.stringToNum(siteId));
			siteB.setSiteId(EJBUtil.stringToNum(siteId));
			siteB.getSiteDetails();
			siteName = siteB.getSiteName();
			siteType = studySiteB.getStudySiteType();
			dSiteType = cdType.toPullDown("siteType",EJBUtil.stringToNum(siteType));
			sampleSize = studySiteB.getSampleSize();
			if(sampleSize == null)
				sampleSize = "";
			publicFlag = studySiteB.getStudySitePublic();
			if(publicFlag == null)
				publicFlag = "0";
			reportFlag = studySiteB.getRepInclude();

			if(reportFlag == null)
				reportFlag = "0";

			enrollmentType = studySiteB.getIsReviewBasedEnrollment();
	        sendEnrNotificationTo = studySiteB.getSendEnrNotificationTo() ;
    	    sendEnrStatusNotificationTo = studySiteB.getSendEnrStatNotificationTo() ;

			if (StringUtil.isEmpty(enrollmentType))
			{
				enrollmentType = "0";
			}

			if (enrollmentType.equals("1"))
			{
				checkedString = " CHECKED ";
			}

			if (StringUtil.isEmpty(sendEnrNotificationTo))
			{
				sendEnrNotificationTo = "";
			}
			else
			{
				sendEnrNotificationNames = userB.getUserNamesFromIds(sendEnrNotificationTo);
			}

			if (StringUtil.isEmpty(sendEnrStatusNotificationTo))
			{
				sendEnrStatusNotificationTo = "";
			}
			else
			{
				sendEnrStatusNotificationNames = userB.getUserNamesFromIds(sendEnrStatusNotificationTo);
			}


		}
		if(mode.equals("N"))
		{


			dAccSites = cd.toPullDown("accsites",EJBUtil.stringToNum(siteId));
			checkedString = "";
		}

	 ///  more info
		 StdSiteAddInfoDao sDao = new StdSiteAddInfoDao();
	 	 sDao = addInfo.getStudySiteAddInfo(studySiteId);
	 	 ArrayList addInfoType  = new ArrayList();
		 ArrayList addInfoTypesDesc = new ArrayList();
		 ArrayList data = new ArrayList();
		 ArrayList id = new ArrayList();
	 	 ArrayList recordType = new ArrayList();
		 ArrayList dispType=new ArrayList();
		 ArrayList dispData=new ArrayList();

		 id = sDao.getId();
		 addInfoType =  sDao.getAddInfoType();
		 addInfoTypesDesc = sDao.getAddInfoTypesDesc();
		 data= sDao.getData();
		 recordType = sDao.getRecordType ();
		 dispType=sDao.getDispType();
		 dispData=sDao.getDispData();


		 String strAddInfoTypesDesc ;
	 	 String strData ;
	  	 String strRecordType ;
	  	 Integer intAddInfoType;
	   	 Integer intId;
		 String disptype="";
		 String dispdata="";
		 String ddStr="";

	 	////	%>



  <Form name="neworg" id="neworgid" method="post" action="saveNewOrganization.jsp" onsubmit="if (validate(document.neworg)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}" >
<!--Mukul Bug: 4170 Fixed Form id was passing instead of Form name -->
  <input type="hidden" name="studyId" value="<%=studyId%>">
<input type="hidden" name="flag" value="<%=flag%>"> <!--KM-->

  <table width="98%" cellspacing="1" cellpadding="1">
  	   <tr><td colspan="2" ><p class="sectionheadings"><%=MC.M_AddOrEdt_OrgDets%><%--Add/ Edit Organization Details*****--%></p> </td></tr>
  	  <tr><td colspan="2">&nbsp;</td></tr>
      	<tr >
        <td width="20%"><%=LC.L_Organization%><%--Organization*****--%> <FONT class="Mandatory">* </FONT></td>
        <% if(mode.equals("N")) {%>

        <td width="45%"><%=dAccSites%></td>

        <%}else{ %>
        <input type="hidden" name="accsites" value="<%=siteId%>"> <!-- KM-->
        <td><input type=text name="siteNameMod" value="<%=siteName%>"  READONLY  maxlength=50></td>

        <%} %>
      </tr>
      <tr>
        <td ><%=LC.L_Type%><%--Type*****--%></td>
        <td ><%=dSiteType%></td>
      </tr>
      <tr>
        <td width="20%"><%=LC.L_Local_SampleSize%><%--Local Sample Size*****--%></td>
        <td><input type=text name="sampleSize" value="<%=sampleSize%>"  maxlength="10"></td> <%-- YK BUG#5564 --%>
      </tr>
      <tr><td>&nbsp;</td></tr>
    	<tr>
        <td colspan=2> <%=MC.M_DoYouWant_InfoAvalPublic%><%--Do you want this Information to be available to the public?*****--%> </td>
	</tr>
	<tr>
	    <td colspan=2>

		        <%if(publicFlag.equals("1")){%>
		        <input type="Radio" name="pubFlag" value='1' Checked><%=LC.L_Yes%><%--Yes*****--%>
		        <input type="Radio" name="pubFlag" value='0' ><%=LC.L_No%><%--No*****--%>
		        	<%}
		       else if(publicFlag.equals("") || publicFlag.equals("0")){%>
		       	   <input type="Radio" name="pubFlag" value='1' ><%=LC.L_Yes%><%--Yes*****--%>
		        <input type="Radio" name="pubFlag" value='0' Checked><%=LC.L_No%><%--No*****--%>
		        	<%}%>

			&nbsp; <A href="pubvsnonpub.htm" target="Information" onClick="openwin()"><%=MC.M_WhatPub_NonPubInfo%><%--What is Public vs Non Public Information?*****--%></A>
		</td>
	</tr>
<tr><td>&nbsp;</td></tr>
      <tr><td>&nbsp;</td></tr>
      	  </table>

      <table>
	  <tr><td colspan=2><P class="sectionHeadings"><%=LC.L_Addl_Info%><%--Additional Information*****--%></P></td></tr>
	  	  <tr> <td width="45%">
	  	  <%=LC.L_Include_InRpts%><%--Include in Reports*****--%></td><td>


	  	  <%

		  if(reportFlag.equals("") || reportFlag.equals("1")){%>
	  	  <input type=checkBox name="repFlag" value='1' checked>
	  	  	  <%} else{%>
	  	  	  	 <input type=checkBox name="repFlag" value='0' >
	  	  	  	 	 <%}%>
	  	  	</td>

	  </tr>

	  	 <%////////
	  	  	int count = 0,cbcount=0;
	for (count = 0; count <= addInfoType.size() -1 ; count++)
		{
				strAddInfoTypesDesc = (String) addInfoTypesDesc.get(count);
 	 			strData = (String) data.get(count);
 	 			disptype=(String) dispType.get(count);
				dispdata=(String) dispData.get(count);
				if (disptype==null) disptype="";
				if (dispdata==null) dispdata="";

 	 			if (strData == null)
 	 				strData= "";

  	 			strRecordType = (String) recordType.get(count);
				intAddInfoType = (Integer) addInfoType.get(count) ;
   	 			intId = (Integer) id.get(count) ;

	%>

	<tr>
		     <td class=tdDefault  >
				<%= strAddInfoTypesDesc %>
			 </td>
		     <td class=tdDefault >
				<% if ((disptype.toLowerCase()).equals("chkbox")){
				cbcount=cbcount+1;
				%>
				<input type = "hidden" name = "data" value = "<%=strData.trim()%>" size = "25" maxlength = "100" >
						<%	 if ((strData.trim()).equals("Y")){%>
				 <input type="checkbox" name="datachk" value="<%=strData.trim()%>" onClick="setValue(document.neworg,<%=count%>,<%=cbcount%>)" checked>
				  <% }else{%>
				  <input type="checkbox"  name="datachk" value="<%=strData.trim()%>" onClick="setValue(document.neworg,<%=count%>,<%=cbcount%>)">

				<%}} else if ((disptype.toLowerCase()).equals("dropdown")) {
				if (ddStr.length()==0) ddStr=(count)+":"+strData;
					else ddStr=ddStr+"||"+(count)+":"+strData;
				   %><%=dispdata%>
				 <%}
				else{%>
				<input type = "text" name = "data" value = "<%=strData.trim()%>" size = "25" maxlength = "100" >
				<%}%>
				<input type = "hidden" name = "recordType" value = "<%=strRecordType %>" >
				<input type = "hidden" name = "id" value = "<%=intId%>" >
				<input type = "hidden" name = "addInfoType" value = "<%=intAddInfoType%>" >
				<input type = "hidden" name = "studySiteId" value = "<%=studySiteId%>" >
			 </td>
	</tr>

	<%
		}
///////////////////////	%>
		<tr><td colspan=2><p class="defComments"><BR><%=MC.M_FlwOptAval_VelosNetwork%><%--The following options are only available for Velos Grid Enabled Networks*****--%></P></td></tr>
 	  <tr> <td width="45%"><%=LC.L_Review_BasedEnrollment%><%--Review Based Enrollment*****--%></td>
 	  <td>
 	  		 <input type=checkBox name="isReviewBased" value='1' <%= checkedString %> >
 	  </td>

 	  </tr>
 	   <tr> <td width="45%"><%=MC.M_NewEnrlNotific_SentTo%><%--New Enrollment Notification Sent To*****--%></td>
 	  <td>
 	   	   	<Input TYPE="text" NAME="selEnrNotUser" SIZE="20" READONLY VALUE="<%=sendEnrNotificationNames%>">
	       	<Input TYPE="hidden" NAME="selEnrNotId" VALUE="<%=sendEnrNotificationTo%>">
			<FONT class = "comments"><A href="javascript:void(0);" onClick="getUser(document.neworg,document.neworg.selEnrNotUser,document.neworg.selEnrNotId) "><%=LC.L_Select_User%><%--Select User*****--%></A> </FONT>

 	  </td>
 	  </tr>
 	   	   <tr> <td width="45%"><%=MC.M_EnrlApp_NotofySent%><%--Enrollment Approved/Denied Notification Sent To*****--%></td>
 	  <td>
 	   	   	<Input TYPE="text" NAME="selEnrNotStatusUser" SIZE="20" READONLY VALUE="<%=sendEnrStatusNotificationNames%>">
	       	<Input TYPE="hidden" NAME="selEnrNotStatusId" VALUE="<%=sendEnrStatusNotificationTo%>">
	       	<FONT class = "comments"><A href="javascript:void(0);" onClick="getUser(document.neworg,document.neworg.selEnrNotStatusUser,document.neworg.selEnrNotStatusId)"><%=LC.L_Select_User%><%--Select User*****--%></A> </FONT>


 	  </td>
 	  </tr>

	<input type="hidden" name="cbcount" value=<%=cbcount%>>
	<input type="hidden" name="ddlist" value="<%=ddStr%>">


  </table>


<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="neworgid"/>
		<jsp:param name="showDiscard" value="Org"/>
</jsp:include>


	   <br>
  	  <%////////////////// app//////%>

    <table width="100%" >
      <tr >
        <td width = "100%">
          <P class = "sectionHeadings"><%=LC.L_Attachments%><%--Attachments*****--%></P>
        </td>
      </tr>

      <% //if (verStatus.equals("W")) {%>
      <tr>
        <td align=right>
          <P class = "defComments">
		  <A href="studySiteApndxURL.jsp?mode=N&type=U&studySiteId=<%=studySiteId%>&studyId=<%=studyId%>" onClick="return checkToOpen('<%=mode%>')" ><%=MC.M_ClkHere_AddNewLink%><%--Click Here</A> to add a new Link.*****--%>
		  </P>
        </td>
      </tr>
	  <%//}%>

      <tr height=5>
        <td> </td>
      </tr>
    </table>
    <table width="100%">
	<th width="30%"><%=LC.L_Url_Upper%><%--URL*****--%></th>
	<th width="60%"><%=LC.L_Description%><%--Description*****--%></th>
	<th width="5%"><%=LC.L_Edit%><%--Edit*****--%></th>
	<th width="5%"><%=LC.L_Delete%><%--Delete*****--%></th>
      <%
       ArrayList studySiteApndxIds = null;
       ArrayList studySiteApndxNames = null;
       ArrayList studySiteApndxDescs = null;

       ArrayList studySiteApndxIds1 = null;
       ArrayList studySiteApndxNames1 = null;
       ArrayList studySiteApndxDescs1 = null;

       String studySiteApndxId = "";
       String name = "";
       String desc = "";



      StudySiteApndxDao studySiteApndxDao = new StudySiteApndxDao();
		studySiteApndxDao = studySiteApndxB.getStudySitesApndxValues(studySiteId,"U");
		int counter = 0;
		int len = studySiteApndxDao.getCRows();
		studySiteApndxIds = studySiteApndxDao.getStudySiteApndxIds();
		studySiteApndxNames = studySiteApndxDao.getAppendixNames();
		studySiteApndxDescs = studySiteApndxDao.getAppendixDescriptions();



	for(counter = 0;counter<len;counter++)
	{
	studySiteApndxId = studySiteApndxIds.get(counter).toString();
	name = studySiteApndxNames.get(counter).toString();
	desc = studySiteApndxDescs.get(counter).toString();
	if ((counter%2)==0) {
		%>
      <tr class="browserEvenRow">
        <%

			}

		else{

		%>
      <tr class="browserOddRow">
        <%

			}

		%>
        <td width="30%">

		    	<A href="<%=name%>" target="_new"><%=name%></A>
        </td>

        <td width="60%">
        	<%=desc%>



        </td>

		 <td width="5%"><A href ="studySiteApndxURL.jsp?mode=M&type=U&studySiteId=<%=studySiteId%>&studySiteApndxId=<%=studySiteApndxId%>&studyId=<%=studyId%>"><img border="0" src="./images/edit.gif" title="<%=LC.L_Edit%>"></A></td>
		 <!-- // Added by RA Bug #4079 Confirmation message at the time of delete -->
		 <td width="5%"><A href ="deleteStudySiteApndx.jsp?studySiteApndxId=<%=studySiteApndxId%>&studySiteId=<%=studySiteId%>&studyId=<%=studyId%>" onClick="return confirmBox('<%=name%>',4,4)"><img border="0" title="<%=LC.L_Delete%>" src="./images/delete.gif"></A></td>

      </tr>
      <%



		}

	%>
    </table>
    <%
 		StudySiteApndxDao studySiteApndxDao1 = new StudySiteApndxDao();

		studySiteApndxDao1 = studySiteApndxB.getStudySitesApndxValues(studySiteId,"F");
		int counter1 = 0;
		int len1 = studySiteApndxDao1.getCRows();
		studySiteApndxIds1 = studySiteApndxDao1.getStudySiteApndxIds();
		studySiteApndxNames1 = studySiteApndxDao1.getAppendixNames();
		studySiteApndxDescs1 = studySiteApndxDao1.getAppendixDescriptions();

%>
    <BR>
    <table width="100%" >


      <tr>
        <td align=right>
          <P class = "defComments">
		  <A href="studySiteApndxFile.jsp?mode=N&type=F&studySiteId=<%=studySiteId%>&studyId=<%=studyId%>" onClick="return checkToOpen('<%=mode%>')"><%=MC.M_ClkHere_AddNewFile%><%--Click Here</A> to add a new File.*****--%>
		  </P>
        </td>
      </tr>


    </table>
    <table width="100%">
	<th width="30%"><%=LC.L_File_Name%><%--File name*****--%></th>
	<th width="60%"><%=LC.L_Description%><%--Description*****--%></th>
	<th width="5%"><%=LC.L_Edit%><%--Edit*****--%></th>
	<th width="5%"><%=LC.L_Delete%><%--Delete*****--%></th>
      <%
    String dnld;
	Configuration.readSettings("eres");
	Configuration.readUploadDownloadParam(Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "studysite");
	dnld=Configuration.DOWNLOADSERVLET ;
	String modDnld = "";

	for(counter1 = 0;counter1<len1; counter1++)

	{

	studySiteApndxId = studySiteApndxIds1.get(counter1).toString();

	name = studySiteApndxNames1.get(counter1).toString();
	desc = studySiteApndxDescs1.get(counter1).toString();


		if ((counter1%2)==0) {

		%>
   <tr class="browserEvenRow">
        <%

		}

		else{

		%>
      <tr class="browserOddRow">
        <%



			}
		 modDnld = dnld + "?file=" + name ;

		%>

	<td>

	<A href="#" onClick="fdownload(document.neworg,<%=studySiteApndxId%>,'<%=name%>','<%=dnld%>');return false;" >

	<%=name %></A>

	 </td>
	<td> <%=desc%> </td>


	<td width=5%>
	<A HREF="studySiteApndxFile.jsp?studySiteApndxId=<%=studySiteApndxId%>&studySiteId=<%=studySiteId%>&studyId=<%=studyId%>&mode=M&type=F"><img border="0" src="./images/edit.gif" title="<%=LC.L_Edit%>"/></A>
	</td>

	<td width=15%>
	<!-- // Added by RA Bug #4079 Confirmation message at the time of delete -->
	<A HREF="deleteStudySiteApndx.jsp?studySiteApndxId=<%=studySiteApndxId%>&studySiteId=<%=studySiteId%>&studyId=<%=studyId%>"  onClick="return confirmBox('<%=name%>',4,4)"><img border="0" title="<%=LC.L_Delete%>" src="./images/delete.gif"/></A>

	</td>
   </tr>
<%}%>
    </table>



<%//if((pageRight == 5 && mode.equals("N")) || (pageRight == 6 && mode.equals("M")) || pageRight == 7){%>

		  <%//}%>




	<input type="hidden" name="studySiteId" value="<%=studySiteId%>">
	<input type="hidden" name="mode" value="<%=mode%>">
	<input type="hidden" name="siteId" value="<%=siteId%>">
	<input type="hidden" name="cnt" value="<%=len%>">
	<input type="hidden" name="cnt1" value="<%=len1%>">

	 	<input type="hidden" name="tableName" value="ER_STUDYSITES_APNDX">
    <input type="hidden" name="columnName" value="APNDX_FILE">
    <input type="hidden" name="pkColumnName" value="PK_STUDYSITES_APNDX">
    <input type="hidden" name="module" value="studysite">
    <input type="hidden" name="db" value="eres">
    <input type="hidden" name="pkValue" value="">
    <input type="hidden" name="file" value="">
    <input type="hidden" name="dnldurl" value="">
	<script>
		enableSubmit();
	</script>

</Form>
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>

<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>

</html>


