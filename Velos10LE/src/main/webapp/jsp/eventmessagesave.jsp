<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<jsp:include page="include.jsp" flush="true"/>
</HEAD>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>
<%response.setHeader("Cache-Control", "max-age=1, must-revalidate");%>
<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<%@ page language = "java" import = "com.velos.esch.service.util.EJBUtil"%>
<%@ page language="java" import="com.velos.esch.service.util.EJBUtil,com.velos.eres.service.util.*,com.velos.eres.web.studyRights.StudyRightsJB"%>

<%
String src = request.getParameter("srcmenu");
String duration = request.getParameter("duration");
String protocolId = request.getParameter("protocolId");
String calledFrom = request.getParameter("calledFrom");
String eventmode = request.getParameter("eventmode");
String fromPage = request.getParameter("fromPage");
String mode = request.getParameter("mode");
String calStatus = request.getParameter("calStatus");
String displayDur=request.getParameter("displayDur");
String displayType=request.getParameter("displayType");	

String propagateInVisitFlag = request.getParameter("propagateInVisitFlag"); //SV, 9/16/04, cal-enh-05
String propagateInEventFlag = request.getParameter("propagateInEventFlag");

String categoryId = "";
String eventId = request.getParameter("eventId");
//String eventName = request.getParameter("eventName"); 
String eventName = ""; //KM
String eventDesc = "";
String eventDuration = "";  
String eventFuzzyPerDays = "";
String eventNotes = "";


%>
<%
HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession)) {	
    
    String calAssoc = request.getParameter("calassoc");
    calAssoc = (calAssoc == null) ? "" : calAssoc;

	if (calledFrom.equals("P")||calledFrom.equals("L"))
    {
       	  eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));
       	  eventdefB.getEventdefDetails();
          eventName = eventdefB.getName();
    }else{	
       	  eventassocB.setEvent_id(EJBUtil.stringToNum(eventId));
          eventassocB.getEventAssocDetails();     
       	  eventName = eventassocB.getName(); 
     }			



 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<% 	
   String eSign = request.getParameter("eSign");	
    String oldESign = (String) tSession.getValue("eSign");

	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {

   String ipAdd = (String) tSession.getValue("ipAdd");
   String usr = (String) tSession.getValue("userId");

   String accountId = (String) tSession.getValue("accountId");


//SV, 10/31/04, I don't know why this fetches rows from event_def for a study mode
// while coming from event browser : if ((calledFrom.equals("P")) || (calledFrom.equals("L")) || ((calledFrom.equals("S") && (fromPage.equals("eventbrowser")) || fromPage.equals("selectEvent") )) )
if ((calledFrom.equals("P")) || (calledFrom.equals("L")) || (calledFrom.equals("S") && fromPage.equals("selectEvent") ))
   
{
		eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));
		eventdefB.getEventdefDetails();
	}else {
		eventassocB.setEvent_id(EJBUtil.stringToNum(eventId));
		eventassocB.getEventAssocDetails();
	}
	
	String eventMsgDaysBpat = request.getParameter("eventMsgDaysBpat");
	eventMsgDaysBpat=(eventMsgDaysBpat==null)?"":eventMsgDaysBpat;
	eventMsgDaysBpat=eventMsgDaysBpat.trim();
	if (eventMsgDaysBpat.length()==0) {
		eventMsgDaysBpat = "0";
	}
  	String eventMsgBpat = request.getParameter("eventMsgBpat"); 

	String eventMsgDaysApat = request.getParameter("eventMsgDaysApat");
	eventMsgDaysApat=(eventMsgDaysApat==null)?"":eventMsgDaysApat;

	eventMsgDaysApat=eventMsgDaysApat.trim();
	if (eventMsgDaysApat.length()==0) {
		eventMsgDaysApat = "0";
	}

  	String eventMsgApat = request.getParameter("eventMsgApat");
	
	String eventMsgDaysBuser = request.getParameter("eventMsgDaysBuser");
	eventMsgDaysBuser=eventMsgDaysBuser.trim();
	if (eventMsgDaysBuser.length()==0) {
		eventMsgDaysBuser = "0";
	}

	String eventMsgBuser = request.getParameter("eventMsgBuser");

	 
	String eventMsgDaysAuser = request.getParameter("eventMsgDaysAuser");
	eventMsgDaysAuser=eventMsgDaysAuser.trim();
	if (eventMsgDaysAuser.length()==0) {
		eventMsgDaysAuser = "0";
	}

	String eventMsgAuser = request.getParameter("eventMsgAuser");


	//SV, 10/31/04, I don't know why this fetches rows from event_def for a study mode
	// while coming from event browser : if ((calledFrom.equals("P")) || (calledFrom.equals("L")) || ((calledFrom.equals("S") && (fromPage.equals("eventbrowser")) || fromPage.equals("selectEvent") )) )
	if ((calledFrom.equals("P")) || (calledFrom.equals("L")) || (calledFrom.equals("S") && fromPage.equals("selectEvent") ))
	{		
		

           eventdefB.setPatDaysBefore(eventMsgDaysBpat);
		   eventdefB.setPatDaysAfter(eventMsgDaysApat);
   		   eventdefB.setPatMsgBefore(eventMsgBpat);
		   eventdefB.setPatMsgAfter(eventMsgApat);
		   eventdefB.setUsrDaysBefore(eventMsgDaysBuser);
		   eventdefB.setUsrDaysAfter(eventMsgDaysAuser);
		   eventdefB.setUsrMsgBefore(eventMsgBuser);
		   eventdefB.setUsrMsgAfter(eventMsgAuser);
		   eventdefB.setModifiedBy(usr);
		   eventdefB.setIpAdd(ipAdd);		
	       eventdefB.updateEventdef();
		} else {
	       eventassocB.setPatDaysBefore(eventMsgDaysBpat);
		   eventassocB.setPatDaysAfter(eventMsgDaysApat);
		   eventassocB.setUsrDaysBefore(eventMsgDaysBuser);
		   eventassocB.setUsrDaysAfter(eventMsgDaysAuser);
		   eventassocB.setPatMsgBefore(eventMsgBpat);
		   eventassocB.setPatMsgAfter(eventMsgApat);
		   eventassocB.setUsrMsgBefore(eventMsgBuser);
		   eventassocB.setUsrMsgAfter(eventMsgAuser);
		   eventassocB.setModifiedBy(usr);
		   eventassocB.setIpAdd(ipAdd);
		   eventassocB.updateEventAssoc();	      
	}
   	//SV, 10/12/04, I think, new mode is only relevant when new events are created and thus propagate only in "M" mode.
	if (fromPage.equals("eventbrowser") || fromPage.equals("fetchProt")) {
		int ret = 0;
		//DEBUG System.out.println("eventmessagesave: prop:" + propagateInVisitFlag + ":" + propagateInVisitFlag);
		if ((propagateInVisitFlag == null) || (propagateInVisitFlag.equals("")))
			propagateInVisitFlag = "N";

		if ((propagateInEventFlag == null) || (propagateInEventFlag.equals("")))
			propagateInEventFlag = "N";
		
		if ( (propagateInVisitFlag.equals("Y")) || (propagateInEventFlag.equals("Y"))) {
			if (calledFrom.equals("P"))  {
				ret=eventdefB.propagateEventmessage(propagateInVisitFlag, propagateInEventFlag);
			}
			else 	if (calledFrom.equals("S")) {
				ret=eventassocB.propagateEventMessage(propagateInVisitFlag, propagateInEventFlag);
			}
		}
	}

	%>
	

<br>

<br>

<br>

<br>

<br>

<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
 <META HTTP-EQUIV=Refresh CONTENT="1; URL=eventmessage.jsp?eventmode=<%=eventmode%>&mode=<%=mode%>&duration=<%=duration%>&srcmenu=tdmenubaritem3&selectedTab=2&eventId=<%=eventId%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&calassoc=<%=calAssoc%>"> 
<%
}// end of if body for eSign
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>
</HTML>


