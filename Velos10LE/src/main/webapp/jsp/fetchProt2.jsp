<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<style type="text/css">
.yui-dt-liner { white-space:normal; } 
.yui-skin-sam .yui-dt td.up { background-color: #efe; } 
.yui-skin-sam .yui-dt td.down { background-color: #fee; }
</style>
<%@page import="java.text.DecimalFormat" %>
<%@page import="com.velos.eres.web.objectSettings.ObjectSettingsCache,com.velos.eres.web.objectSettings.ObjectSettings"%>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@page import="com.velos.esch.business.common.SchCodeDao"%>
<%@page import="com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*" %>
<%@page import="java.text.DecimalFormat" %>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<%
StringBuffer urlParamSBForCACal = null;
String keySessId =  "";
ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
ArrayList tabList = null;
String titleStr = LC.L_Calendar;/*String titleStr = "Calendar";*****/
if (sessionmaint.isValidSession(request.getSession(true))){
    String accId = (String) request.getSession(true).getValue("accountId");
    
    ArrayList tempList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "protocol_tab");
    for (int iX=0; iX<tempList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings) tempList.get(iX);
        if ("6".equals(settings.getObjSubType())) {
            if (settings.getObjDispTxt() != null) {
            	Object[] arguments = {titleStr,settings.getObjDispTxt()};
            	titleStr = VelosResourceBundle.getLabelString("L_Gtgt",arguments);
                /*titleStr +=  " >> " + settings.getObjDispTxt();*****/
            }
            break;
        } 
    }
    tempList = null;
}
String src= StringUtil.htmlEncodeXss(request.getParameter("srcmenu"));
String mode= StringUtil.htmlEncodeXss(request.getParameter("mode")); //Yk: Added for Enhancement :PCAL-19743

HttpSession tSession = request.getSession(true); 
String vclick1=request.getParameter("vclick");
vclick1 = (vclick1==null)?"1":vclick1;
String accountId = "";
String studyId = "";
String grpId = "";
String usrId = "";
if (sessionmaint.isValidSession(tSession)) {
	accountId = (String) tSession.getValue("accountId");
	studyId = (String) tSession.getValue("studyId");
	grpId = (String) tSession.getValue("defUserGroup");
	usrId = (String) tSession.getValue("userId");
	
	if ("N".equals(request.getParameter("mode"))) {
		studyId = "";
		tSession.removeAttribute("studyId");
	}

	String from = "version";
    String calName=request.getParameter("calName");
    StringBuffer urlParamSB = new StringBuffer();
     urlParamSBForCACal = new StringBuffer();
    String sessId = tSession.getId();
    if (sessId.length()>8) { sessId = sessId.substring(0,8); }
    sessId = Security.encrypt(sessId);
	char[] chs = sessId.toCharArray();
    StringBuffer sb2 = new StringBuffer();
    DecimalFormat df = new DecimalFormat("000");
	for (int iX=0; iX<chs.length; iX++) {
	    sb2.append(df.format((int)chs[iX]));
	}
	keySessId = sb2.toString();
   String displayDur=request.getParameter("displayDur");
   String displayType=request.getParameter("displayType");
   String pageNo=request.getParameter("pageNo");
	String tab = request.getParameter("selectedTab");
    String calassoc = StringUtil.trueValue(request.getParameter("calassoc"));
	String studyIdForTabs = request.getParameter("studyId");
    String includeTabsJsp = "irbnewtabs.jsp";
    int pageRight = 0;
	String calledFrom = request.getParameter("calledFrom");
	if (calledFrom.equals("S")) {
		StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
		if ((stdRights.getFtrRights().size()) == 0){
			pageRight= 0;
		} else {
			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
		}
	} else if ((calledFrom.equals("P")) || (calledFrom.equals("L"))) {
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
		studyId = "";
		tSession.removeAttribute("studyId");
	}
	
	if (pageRight > 0) {

	} // end of pageRight > 0
	String protocolId = request.getParameter("protocolId");
	if(protocolId == null || protocolId == "" || protocolId.equals("null") || protocolId.equals("")) {
	} else {  
		  int finDetRight = 0;
			if (calledFrom.equals("S")) {
				StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
				if ((stdRights.getFtrRights().size()) == 0){
					pageRight= 0;
					finDetRight = pageRight;
				} else {
					pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
					finDetRight=Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFIN"));
				}
			} else if ((calledFrom.equals("P")) || (calledFrom.equals("L"))) {
				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
				finDetRight = pageRight;
			}
		
		String tableName = null;
		if (pageRight > 0) {
		    
	 		if("L".equals(calledFrom) || "P".equals(calledFrom)){
		   		tableName ="event_def";
				eventdefB.setEvent_id(EJBUtil.stringToNum(protocolId));
				eventdefB.getEventdefDetails();
		   	} else {
		   		tableName ="event_assoc";
				eventassocB.setEvent_id(EJBUtil.stringToNum(protocolId));
				eventassocB.getEventAssocDetails();
		   	}
	 	}
		
		String duration = StringUtil.trueValue(request.getParameter("duration"));
		 urlParamSBForCACal = new StringBuffer();
		 urlParamSBForCACal.append("protocolId=").append(StringUtil.htmlEncodeXss(protocolId))
		.append("&calledFrom=").append(StringUtil.htmlEncodeXss(calledFrom))
		.append("&calledfrom=").append(StringUtil.htmlEncodeXss(calledFrom))
		.append("&duration=").append(StringUtil.htmlEncodeXss(duration))
		.append("&calassoc=").append(StringUtil.htmlEncodeXss(calassoc))
		.append("&calstatus=").append(StringUtil.htmlEncodeXss(request.getParameter("calStatus")))
		.append("&mode=").append(StringUtil.htmlEncodeXss(request.getParameter("mode")))
		.append("&calProtocolId=").append(StringUtil.htmlEncodeXss(protocolId))
		.append("&tableName=").append(tableName)
		.append("&pr=").append(pageRight)
		.append("&fin=").append(finDetRight)
		.append("&fin=").append(finDetRight)
		;
		
		
	String calStatus = null;
	String calStatusPk = "";
	String calStatDesc = "";
	String protocolName = ""; // YK : Added for Bug#7528
	SchCodeDao scho = new SchCodeDao();
	if (pageRight > 0) {
 		if("L".equals(calledFrom) || "P".equals(calledFrom)){
	   		tableName ="event_def";
			eventdefB.setEvent_id(EJBUtil.stringToNum(protocolId));
			eventdefB.getEventdefDetails();
			//KM-#DFin9
			// YK : Added for Bug#7528
			protocolName = eventdefB.getName();
			calStatusPk = eventdefB.getStatCode();
			calStatus = scho.getCodeSubtype(EJBUtil.stringToNum(calStatusPk)).trim();
			calStatDesc = scho.getCodeDescription(EJBUtil.stringToNum(calStatusPk));

	   	} else {
	   		tableName ="event_assoc";
			eventassocB.setEvent_id(EJBUtil.stringToNum(protocolId));
			eventassocB.getEventAssocDetails();
			//KM-#DFin9
			// YK : Added for Bug#7528
			protocolName = eventassocB.getName();
			calStatusPk = eventassocB.getStatCode();
			calStatus = scho.getCodeSubtype(EJBUtil.stringToNum(calStatusPk)).trim();
			calStatDesc = scho.getCodeDescription(EJBUtil.stringToNum(calStatusPk));

	   	}
 		// YK : Added for Bug#7528
 		tSession.setAttribute("protocolname",protocolName);
 		
		duration = StringUtil.trueValue(request.getParameter("duration"));
		urlParamSB = new StringBuffer();
		urlParamSB.append("protocolId=").append(StringUtil.htmlEncodeXss(protocolId))
		.append("&calledFrom=").append(StringUtil.htmlEncodeXss(calledFrom))
		.append("&calledfrom=").append(StringUtil.htmlEncodeXss(calledFrom))
		.append("&duration=").append(StringUtil.htmlEncodeXss(duration))
		.append("&calassoc=").append(StringUtil.htmlEncodeXss(calassoc))
		.append("&calstatus=").append(StringUtil.htmlEncodeXss(request.getParameter("calStatus")))
		.append("&mode=").append(StringUtil.htmlEncodeXss(request.getParameter("mode")))
		.append("&calProtocolId=").append(StringUtil.htmlEncodeXss(protocolId))
		.append("&tableName=").append(tableName)
		.append("&doGet=").append(pageRight);
		;
	}}
}
%>
<title><%=titleStr %></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META CONTENT="no-cache" HTTP-EQUIV="PRAGMA">
<META CONTENT="private" HTTP-EQUIV="CACHE-CONTROL">
<META CONTENT="no-store" HTTP-EQUIV="CACHE-CONTROL">
<META CONTENT="must-revalidate" HTTP-EQUIV="CACHE-CONTROL">
<META CONTENT="post-check=0,pre-check=0" HTTP-EQUIV="CACHE-CONTROL">
<META CONTENT="-1" HTTP-EQUIV="Expires">
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<jsp:include page="ui-include.jsp" flush="true"/>
<script  type="text/javascript" src="js/jquery/json2.min.js"></script>
<script type="text/javascript" src="js/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
 <style>
 
 table.viewallcass th {
	white-space: normal;
	font-weight: normal;
	font-size: 92%;
	border-style: none solid none none;
	padding-right: 4px;
	padding-top: 4px;
	height : 40px;
	word-wrap: break-word !important;         /* All browsers since IE 5.5+ */
    overflow-wrap: break-word !important;     /* Renamed property in CSS3 draft spec */
}
#firstTd{
margin-right: 10px;
font-size: 100%;
font-weight: normal;
vertical-align: middle;
}
 
  table.viewallcass td  {
		border-style: none solid none none;
}

th p.std{
	width: 128px ;
	
}
td p.std{
	width: 128px ;	
}  

th  p.setwidth {
	width: 213px  !important;	
}

td p.setwidth {
	overflow: hidden !important;
	width: 213px  !important;
    word-wrap: break-word !important;         /* All browsers since IE 5.5+ */
    overflow-wrap: break-word !important;     /* Renamed property in CSS3 draft spec */
} 	 
 
</style>
<link type="text/css" href="js/yui/build/datatable/assets/skins/sam/datatable.css" />
</head>
<script type="text/javascript">
var viewAllFlag=1;
var totalEventCount=<%=LC.Config_CovAnal_Event_records%>;
var totalVisitCount=<%=LC.Config_CovAnal_Visit_records%>;
var initEventSize=1;
var limitEventSize=totalEventCount;
var initVisitSize=1;
var limitVisitSize=totalVisitCount;


function fnOnceEnterKeyPress(e) {
	var evt = (e) || window.event;
    if (!evt) { return 0; }
	try {
        var code = evt.charCode || evt.keyCode;
        if (code == 13 || code == 10) {
            var thisTimeSubmitted = new Date();
            if (!lastTimeSubmitted) { lastTimeSubmitted = 0; }
            if (!thisTimeSubmitted) { return 0; }
            if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) {
                return -1;
            }
            lastTimeSubmitted = thisTimeSubmitted;
            return 1;
        }
	} catch(e) {}
	return 0;
}
function searchClear(){
	$j("#eventName").val('');
	reloadDataGrid('asc');
}
function reloadGridAsFetch(eventType){
	var totalEventRecords = $j("#totalEventRecords").val();
	var totalVisitRecords = $j("#totalVisitRecords").val();

		if(eventType=='previousEvents' && initEventSize>totalEventCount){
			
		initEventSize=initEventSize-totalEventCount;
	
		limitEventSize=limitEventSize-totalEventCount;
		reloadDataGrid('asc'); 
		}else if(eventType=='nextEvents' && limitEventSize<totalEventRecords){
		initEventSize=initEventSize+totalEventCount;
		limitEventSize=limitEventSize+totalEventCount;
		reloadDataGrid('asc');
		}else if(eventType=='firstEvents' && initEventSize>totalEventCount ){
		initEventSize=1;
		limitEventSize=totalEventCount;
		reloadDataGrid('asc');
		}else if(eventType=='lastEvents' && limitEventSize<totalEventRecords){
		var rem = totalEventRecords%totalEventCount;
		if(rem==0)
		rem= totalEventCount;
		initEventSize = totalEventRecords-rem+1;
		limitEventSize = totalEventRecords-rem+totalEventCount;
		reloadDataGrid('asc');
		}else if(eventType=='previousVisits' && limitVisitSize>totalVisitCount){
		initVisitSize=initVisitSize-totalVisitCount;
		limitVisitSize=limitVisitSize-totalVisitCount;
		reloadDataGrid('asc'); 
		}else if(eventType=='nextVisits' && limitVisitSize<totalVisitRecords){
		initVisitSize=initVisitSize+totalVisitCount;
		limitVisitSize=limitVisitSize+totalVisitCount;
		reloadDataGrid('asc');
		}else if(eventType=='firstVisits' && initVisitSize>totalVisitCount){
		initVisitSize=1;
		limitVisitSize=totalVisitCount;
		reloadDataGrid('asc');
		}else if(eventType=='lastVisits' && limitVisitSize<totalVisitRecords){
		var rem = totalVisitRecords%totalVisitCount;
		if(rem==0)
		rem= totalVisitCount;
		initVisitSize = totalVisitRecords-rem+1;
		limitVisitSize = totalVisitRecords-rem+totalVisitCount;
		reloadDataGrid('asc');
		}
		
	}
//YK: Added for PCAL-20461
var defaultHeight = navigator.userAgent.indexOf("MSIE") != -1 ? 650 : 500;
jQuery.noConflict();
function openSequenceDialog(calStatus) //YK: Added for PCAL-20461 - Opens the Dialog Window
{
	if(calStatus!='A' && calStatus!='F' && calStatus!='D') {
		jQuery("#editVisitDiv").dialog({ 
		height: defaultHeight,width: 700,position: 'center' ,modal: true,
		closeText:'',
		buttons: [
		          {
		              text: L_Close,
		              click: function() { 
		        	  jQuery(this).dialog("close");
		              }
		          }
		      ],
		close: function() { $j("#editVisitDiv").dialog("destroy"); },
		beforeClose: function(event, ui) { if(!checkBeforeClose()){ return false;} }
        });
	}else {
	alert(M_SrySeqDisp_ModActDact);
	return false;
		}
  
}
//YK: Added for PCAL-20461 - Loads the Visit of the Calender
var loadModifySequenceDialog = function(calStatus, pgRight,calledFrom, protocolId,calassoc,visit) {

	var needToCall = false;
	if(VELOS.dataGrid.checkDataChange())
	{
		alert(M_PlsPrewSave_MoveFwd);
		return false;
	}
	if (f_check_perm(pgRight,'E')){
		if(calStatus!='A' && calStatus!='F' && calStatus!='D') {
			needToCall=true;
		}else{
			needToCall=false;
			alert(M_SrySeqDisp_ModActDact);
			return false;
		}
	
		var myObj = jQuery('#embedDataHere');
		jQuery('#embedDataHere').html("<p><%=LC.L_Loading%>...<img class=\"asIsImage_progbar\" src=\"../images/jpg/loading_pg.gif\" /></p>"); // Wipe out last data for performance
			
		if (navigator.userAgent.indexOf("MSIE") != -1) {
			// For IE it's faster to open dialog and then load
			openSequenceDialog(calStatus); 
		    jQuery('#embedDataHere').load("manageEventSequence.jsp?calledFrom="+calledFrom+"&protocolId="+protocolId+"&calStatus="+calStatus+"&calassoc="+calassoc+"&visitSel="+visit+"" );
		} else {
			// For FF, it's faster to load and then open
			jQuery('#embedDataHere').load("manageEventSequence.jsp?calledFrom="+calledFrom+"&protocolId="+protocolId+"&calStatus="+calStatus+"&calassoc="+calassoc+"&visitSel="+visit+""  );
			openSequenceDialog(calStatus);
		}
		    
	}
}
//Yk: Added for Enhancement :PCAL-19743
function openEvtDetailsPopUp(link) {
	windowName=window.open(link,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=800,height=610,top=70 ,left=150");
    windowName.focus();
}

//PCAL-22322	AGODARA
function addEventsToVisitsWindow(link,pageRight,calStatDesc) {
	link=link+"&vclick="+vclick;
	var calStatus = document.getElementById('calStatus').value;
	
	if (calStatus == 'W' || calStatus == 'O') {
		if (f_check_perm(pageRight,'E') == false) {
			 return false;
		}
	}
	if ((calStatus == 'F') || (calStatus == 'A')|| (calStatus == 'D')) {
		alert(M_CntEvt_VisitCal + " " + calStatDesc);
		return false;
	}
	windowName=window.open(link,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=1000,height=575 top=300,left=250");
	windowName.focus();
}
</script>
<% 
tSession = request.getSession(true); 
vclick1=request.getParameter("vclick");
vclick1 = (vclick1==null)?"1":vclick1;
accountId = "";
studyId = "";
grpId = "";
usrId = "";
if (sessionmaint.isValidSession(tSession)) {
	accountId = (String) tSession.getValue("accountId");
	studyId = (String) tSession.getValue("studyId");
	grpId = (String) tSession.getValue("defUserGroup");
	usrId = (String) tSession.getValue("userId");
	
	if ("N".equals(request.getParameter("mode"))) {
		studyId = "";
		tSession.removeAttribute("studyId");
	}
%>
<body class="yui-skin-sam yui-dt yui-dt-liner" style="overflow:hidden;">
<div id="overDiv" style="position:absolute;visibility:hidden; z-index:1100;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<%
	String from = "version";
   String calName=request.getParameter("calName");
  
   String displayDur=request.getParameter("displayDur");
   String displayType=request.getParameter("displayType");
   String pageNo=request.getParameter("pageNo");
	String tab = request.getParameter("selectedTab");
    String calassoc = StringUtil.trueValue(request.getParameter("calassoc"));
	String studyIdForTabs = request.getParameter("studyId");
    String includeTabsJsp = "irbnewtabs.jsp";
    int pageRight = 0;
	String calledFrom = request.getParameter("calledFrom");
	if (calledFrom.equals("S")) {
		StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
		if ((stdRights.getFtrRights().size()) == 0){
			pageRight= 0;
		} else {
			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
		}
	} else if ((calledFrom.equals("P")) || (calledFrom.equals("L"))) {
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
		studyId = "";
		//tSession.removeAttribute("studyId");
	}
	%>
	<DIV class="BrowserTopn" id = "div1">
		<jsp:include page="protocoltabs.jsp" flush="true">
		<jsp:param name="calassoc" value="<%=StringUtil.htmlEncodeXss(calassoc)%>" />
		<jsp:param name="selectedTab" value="<%=StringUtil.htmlEncodeXss(tab)%>" />
		</jsp:include>
	</DIV>
	<DIV class="BrowserBotN BrowserBotN_CL_1" id = "div2" style="overflow: auto;">
	<%
	if (pageRight > 0) {

	} // end of pageRight > 0
	String protocolId = request.getParameter("protocolId");
	if(protocolId == null || protocolId == "" || protocolId.equals("null") || protocolId.equals("")) {
%>
	<jsp:include page="calDoesNotExist.jsp" flush="true"/>
<%
	} else {
	String calStatus = null;
	String calStatusPk = "";
	String calStatDesc = "";
	String protocolName = ""; // YK : Added for Bug#7528
	SchCodeDao scho = new SchCodeDao();
	if (pageRight > 0) {
	    String tableName = null;
 		if("L".equals(calledFrom) || "P".equals(calledFrom)){
	   		tableName ="event_def";
			eventdefB.setEvent_id(EJBUtil.stringToNum(protocolId));
			eventdefB.getEventdefDetails();
			//KM-#DFin9
			// YK : Added for Bug#7528
			protocolName = eventdefB.getName();
			calStatusPk = eventdefB.getStatCode();
			calStatus = scho.getCodeSubtype(EJBUtil.stringToNum(calStatusPk)).trim();
			calStatDesc = scho.getCodeDescription(EJBUtil.stringToNum(calStatusPk));

	   	} else {
	   		tableName ="event_assoc";
			eventassocB.setEvent_id(EJBUtil.stringToNum(protocolId));
			eventassocB.getEventAssocDetails();
			//KM-#DFin9
			// YK : Added for Bug#7528
			protocolName = eventassocB.getName();
			calStatusPk = eventassocB.getStatCode();
			calStatus = scho.getCodeSubtype(EJBUtil.stringToNum(calStatusPk)).trim();
			calStatDesc = scho.getCodeDescription(EJBUtil.stringToNum(calStatusPk));

	   	}
 		// YK : Added for Bug#7528
 		tSession.setAttribute("protocolname",protocolName);
 		
		String duration = StringUtil.trueValue(request.getParameter("duration"));
		StringBuffer urlParamSB = new StringBuffer();
		urlParamSB.append("protocolId=").append(StringUtil.htmlEncodeXss(protocolId))
		.append("&calledFrom=").append(StringUtil.htmlEncodeXss(calledFrom))
		.append("&calledfrom=").append(StringUtil.htmlEncodeXss(calledFrom))
		.append("&duration=").append(StringUtil.htmlEncodeXss(duration))
		.append("&calassoc=").append(StringUtil.htmlEncodeXss(calassoc))
		.append("&calstatus=").append(StringUtil.htmlEncodeXss(request.getParameter("calStatus")))
		.append("&mode=").append(StringUtil.htmlEncodeXss(request.getParameter("mode")))
		.append("&calProtocolId=").append(StringUtil.htmlEncodeXss(protocolId))
		.append("&tableName=").append(tableName)
		.append("&doGet=").append(pageRight);
		;
%>
<script>
var screenWidth = screen.width;
var screenHeight = screen.height;
var viewAllFlag=1;
var oldEventName;
var vclick="<%=vclick1%>";
//Yk: Modified for Enhancement :PCAL-19743
function reloadDataGrid(sortOrder) {
	$("datagrid").innerHTML = "";
	 var eventName=$j("#eventName").val();
	 if(oldEventName!=eventName){
			initEventSize=1;	
			limitEventSize=totalEventCount;
			oldEventName = eventName;
			}
	var urlParamSB = "<%=urlParamSB.toString()%>";
	if(viewAllFlag){
		if(vclick==1){
			urlParamSB=urlParamSB+"&eventName="+eventName+"&viewAllFlag=1&sortOrder="+sortOrder;
			reloadDataTableGrid(urlParamSB);
			incomingUrlParams=urlParamSB;
	        		return false;
		}
		else{
			urlParamSB= urlParamSB+"&eventName="+eventName+"&sortOrder="+sortOrder+"&initEventSize="+initEventSize+"&limitEventSize="+limitEventSize+"&initVisitSize="+initVisitSize+"&limitVisitSize="+limitVisitSize;
		}
		
	}
	YAHOO.example.DataGrid = function() {

		
		
		var args = {	
			urlParams: urlParamSB,
			dataTable: "datagrid"
		};
		
		myDataGrid = new VELOS.dataGrid('fetchProtJSON.jsp', args);
		myDataGrid.startRequest();
    }();
}
YAHOO.util.Event.addListener(window, "load", function() {
	reloadDataGrid('asc');       //Akshi: Added for Bug#7646
});
function reloadDataTableGrid(urlParam){
	showPanel();
	   var urlParamSB=urlParam;
	   urlParamSB=urlParamSB+"&eUrl=srcmenu=" + document.getElementById("srcmenu").value + "@duration=" + document.getElementById("duration").value + "@mode=" + document.getElementById("mode").value;
	$j.ajax({
		 url: 'fetchProtJSON.jsp',
		 type: 'POST',
	     	 async: false,
	     	 dataType: 'json',
	     	 data: urlParamSB,
	     	 cache: false,
		 success: function (data) {
		var visitIdList = [];
		var offlineArray = [];
		var offlineFlagJson='';
		var colArray=data.colArray;
		var dataArray=data.dataArray;
		var displacementArray=data.displacements;
		var visitsHideFlagArray=data.visitsHideFlag;
		var offlineKeys=data.offlineKeys;
		var offlineValues= data.offlineValues;
		dataGridChangeList=[];
		visitLabelArray = [];
		eventLabelArray = [];
		visitDisplacementArray = [];
		visitHideFlag = [];
		var createTab='';
		var brow='mozilla';
    	jQuery.each(jQuery.browser, function(i, val) {
    	if(val==true){

    	brow=i.toString();
    	}
    	});
		createTab="<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" ><tr><th id=\"firstTd\" style=\"background: #D8D8DA url(\"../../../../assets/skins/sam/sprite.png\") repeat-x scroll 0px 0px;background-image: url(\"../../images/skin_default/bg-tbheading.jpg\");background-repeat: repeat-x;\">"+colArray[0].label+"</th><th style=\"background: #D8D8DA url(\"../../../../assets/skins/sam/sprite.png\") repeat-x scroll 0px 0px;background-image: url(\"../../images/skin_default/bg-tbheading.jpg\");background-repeat: repeat-x;\">";
		if(brow=='msie'){
			if(screenWidth>1280 || screenHeight>1024){
				createTab=createTab + "<div id='divHeader'  class='hdiv'  style='overflow:hidden;max-width:950px;' align='left'><TABLE  class='viewallcass'>";
		}
			else{
				createTab=createTab + "<div id='divHeader'  class='hdiv'  style='overflow:hidden;width:990px;' align='left'><TABLE  class='viewallcass'>";
				}  
			}
			else{
				if(screenWidth>1280 || screenHeight>1024){
					createTab=createTab + "<div id='divHeader'  class='hdiv'  style='overflow:hidden;max-width:1060px;' align='left'><TABLE  class='viewallcass'>";
			}else{				
				createTab=createTab + "<div id='divHeader'  class='hdiv'  style='overflow:hidden;max-width:1002.4px;' align='left'><TABLE  class='viewallcass'>";
				}
				}
			if (offlineKeys && offlineValues) {
			offlineFlagJson=JSON.stringify(offlineKeys);
			for (var iX = 0; iX < offlineKeys.length; iX++) {
				offlineArray[offlineKeys[iX].key] = offlineValues[iX].value
			}
		}		
		var i=0;
		 if (colArray.length > 0) {
			 createTab=createTab+'<tr>';
		 
			 for (var iX=0; iX<colArray.length; iX++) {
				var key = colArray[iX].key;
				 if(key=='eventId' || key == 'event'){continue;}
				 if (!colArray[iX].label) { continue; }
				var inputPos = colArray[iX].label.indexOf(" <input type");
				visitLabelArray[colArray[iX].key] = colArray[iX].label.slice(0, inputPos);
				
				if (displacementArray) {var iZ=iX-2;
					visitDisplacementArray[displacementArray[iZ].key] = displacementArray[iZ].value;}
					if (visitsHideFlagArray) {
					visitHideFlag[visitsHideFlagArray[iZ].key] = visitsHideFlagArray[iZ].value;}

				 if(iX ==0){
				 }else{
				 createTab=createTab+'<th style="background: #D8D8DA url("../../../../assets/skins/sam/sprite.png") repeat-x scroll 0px 0px;background-image: url("../../images/skin_default/bg-tbheading.jpg");background-repeat: repeat-x;"><div class="tableHeader"><p class="std">'+colArray[iX].label;
				 }
				 
				 if (key.match(/^v[0-9]+$/)) {
		            	visitIdList[i]=key; 
		            	i++;
		                     }
			}
			 createTab=createTab+'</p></div></th></tr>';
		 }
			 createTab=createTab+'</table></div></th></tr>';




			 createTab=createTab+"<tr><td valign=\"top\"><div id=\"firstcol\" style='overflow:hidden;width:220px;max-height:350px;px;float:left' class='cdiv'><table>";			 
			
			 createTab=createTab+"replaceIt";	
			 
			 createTab=createTab+"</table></div></td><td valign=\"top\">";	


				var replaceIt=''; 
			if(brow=='msie'){
				if(screenWidth>1280 || screenHeight>1024){				
					createTab=createTab+"<div id='table_div'  class='vdiv' onscroll='return  myscroll();' style='overflow:scroll;max-height:370px;max-width:950px;position:relative;' align='left'><TABLE cellspacing='0px'  class='viewallcass'>";
				}

				else{					
					createTab=createTab+"<div id='table_div'  class='vdiv' onscroll='return  myscroll();' style='overflow:scroll;overflow-y:hidden;width:990px;position:relative;' align='left'><TABLE cellspacing='0px'  class='viewallcass'>";
					}
				}
			else{
				if(screenWidth>1280 || screenHeight>1024){
					createTab=createTab+"<div id='table_div'  class='vdiv' onscroll='return  myscroll();' style='overflow:scroll;max-height:370px;max-width:1080px;position:relative;' align='left'><TABLE cellspacing='0px'  class='viewallcass'>";
			}
				else{
					createTab=createTab+"<div id='table_div'  class='vdiv' onscroll='return  myscroll();' style='overflow:scroll;max-height:370px;max-width:1022.4px;position:relative;' align='left'><TABLE cellspacing='0px'  class='viewallcass'>";
					}
				}
				   if(dataArray.length>0){
			 for (var iX=0; iX<dataArray.length; iX++) {
				 eventLabelArray['e'+dataArray[iX].eventId] = dataArray[iX].event;
				 var checkedVisit=JSON.stringify(dataArray[iX]);
				 var eId=dataArray[iX].eventId;
				 if ((iX%2)==0) {
					 if(iX==0)
					 	createTab=createTab+'<tr id="yui-rec'+iX+'" class=\"browserEvenRow\">';
					 else
						 createTab=createTab+'<tr id="yui-rec'+iX+'" class=\"browserEvenRow\">'; 
					 replaceIt = replaceIt +'<tr class=\"browserEvenRow\" id="yui-rect'+iX+'"><td class="tableFirstCol"  class="yui-dt-col-eventId"><p class="setwidth"><input type="checkbox" name="all_e'+eId+'"  id="all_e'+eId+'"  onclick="VELOS.dataGrid.eventAll(\'yui-rec'+iX+'\','+eId+');"/>'+dataArray[iX].event+'</p></td></tr>';
			      		}
			      		else{
			      	 replaceIt = replaceIt +'<tr class=\"browserOddRow\" id="yui-rect'+iX+'"><td class="tableFirstCol"  class="yui-dt-col-eventId"><p class="setwidth"><input type="checkbox" name="all_e'+eId+'"  id="all_e'+eId+'"  onclick="VELOS.dataGrid.eventAll(\'yui-rec'+iX+'\','+eId+');"/>'+dataArray[iX].event+'</p></td></tr>';
			      		createTab=createTab+'<tr id="yui-rec'+iX+'" class=\"browserOddRow\">';
			 	      }
				 for (var iV=0; iV<visitIdList.length; iV++) {
					 var disable='';
					 var visidId = visitIdList[iV];
					 var offLineFlagKey='e'+eId+visidId;
					 if(offlineFlagJson.search(offLineFlagKey)>0){
						 if(offlineArray[offLineFlagKey]=='1'||offlineArray[offLineFlagKey]=='2'){
                            disable='disabled';
						 }
					 } 
					 var vId = YAHOO.lang.trim(visidId.slice(visidId.indexOf('v')+1));				 
					 if(checkedVisit.search(visitIdList[iV])>0){
						createTab+='<td id="col-'+iV+'"  class="yui-dt-col-'+visidId+' yui-dt-col-eventId"><p class="std"><input id="yui-dt-e'+eId+visidId+'" class="yui-dt-checkbox" type="checkbox" checked="checked" onclick="VELOS.dataGrid.savedCheckedData('+vId+','+eId+');"'+disable+'/>'+dataArray[iX][visidId]+'</p><div class="yui-dt-liner" style="display: none;" >'+eId+'</div></td>';
					 }
					 else
					 	createTab+='<td id="col-'+iV+'"   class="yui-dt-col-'+visidId+' yui-dt-col-eventId"><p class="std"><input id="yui-dt-e'+eId+visidId+'" class="yui-dt-checkbox" type="checkbox" onclick="VELOS.dataGrid.savedCheckedData('+vId+','+eId+');"'+disable+'/></p><div class="yui-dt-liner" style="display: none;" >'+eId+'</div></td>';
											
				 }
				 createTab+='</tr>';
			 }
		   }
		   else{
			   createTab=createTab+'<tr><td colspan="'+colArray.length+1+'">No Records Found</td></tr>'
		   }

		   
		   createTab = createTab.replace("replaceIt",replaceIt);
		createTab=createTab+ '</table></div></td></tr></table>';
		document.getElementById('datagrid').innerHTML = '';
		document.getElementById('datagrid').innerHTML = createTab;
		hidePanel();
		var colCount = $j('#firstcol>table tr').length;
		$j('.tableFirstCol').css("width",$j('#firstTd').width());
		for(var i=0;i<colCount;i++){
			if($j('#yui-rect'+i).height()>$j('#yui-rec'+i).height()){
				$j('#yui-rec'+i).css("height",$j('#yui-rect'+i).height());
			}
			else{
				$j('#yui-rect'+i).css("height",$j('#yui-rec'+i).height());
			}
		}		      		
	},
	error: function (xhr, status, errorThrown) {
	}
	 });
	return false;
}
// YK : Added for Bug#7470 	
function resetSort()
{
	reloadDataGrid('insert_order');
}

function reloadGridAsFetchViewAll(){
	togglePaginationHide();
	reloadDataGrid('asc');
}
function togglePaginationHide(){
	if(vclick==0){
		document.getElementById("viewAllVisit").innerHTML="View By Pagination";
	$j('#visitsSpan').css('display', 'none');
	$j('#eventsSpan').css('display', 'none');
	}
	if(vclick==1){
		$j('#visitsSpan').css('display', 'block');
		$j('#eventsSpan').css('display', 'block');
		document.getElementById("viewAllVisit").innerHTML="View All";
	}
	if(vclick==0){
		vclick=1;
	}else{
		vclick=0;
	}
}
</script>

<%
%>
<div class="tmpHeight"></div>

<%--
class="basetbl midAlign"
@commented for bug 21942 
--%>
  <table width="99%" cellspacing="0" cellpadding="2px" border="0" style="margin-left: 5px">
  <tr height="20" >
  	<td colspan="2"><P class="defComments">
  		<%=LC.L_Cal_Status%><%--Calendar Status*****--%>: <%=calStatDesc%>&nbsp;&nbsp;</P>
  	</td>
  	<td colspan="2">
		<%if ((!protocolId.equals("null"))) {%>
			<%=LC.L_Preview_Calendar%><%--<%=LC.L_Preview%> <%=LC.L_Calendar%> Preview*****--%>: 
		<%}%>	  	
	</td>
  </tr>
  <tr align="left">
  <!--KM-#DFin9-->
  <td>
   	<td>
    <%if ((calStatus.equals("A")) || (calStatus.equals("F")) || (calStatus.equals("D"))){%>
	       <FONT class="Mandatory"><%=MC.M_Changes_CntSvd%><%--Changes cannot be saved*****--%></Font>&nbsp;&nbsp;&nbsp;&nbsp;
			<%}%>
	<%if(calStatus.equals("F") || (calStatus.equals("A"))|| (calStatus.equals("D"))){%>
	<button id="save_changes" name="save_changes" disabled style="display: none;"><%=LC.L_Preview_AndSave%><%--Preview and Save*****--%></button>
	<button id="add_eve_tovisits" name="add_eve_tovisits" disabled style="display: none;"><%=MC.M_AddMul_EvtToVisits%></button>
	<%}else{%>
	<button id="save_changes" name="save_changes" onclick="if (f_check_perm(<%=pageRight%>,'E')) { VELOS.dataGrid.saveDialog(); }"><%=LC.L_Preview_AndSave%><%--Preview and Save*****--%></button>		
	<% String veLink="addevtvisits.jsp?mode="+mode+"&srcmenu="+src+"&duration="+duration+"&protocolid="+protocolId+"&calledfrom="+calledFrom+"&calstatus="+calStatus+"&calltime=new"+"&calassoc="+calassoc+"&selectedTab="+tab+"&displayType="+displayType+"&pageNo="+pageNo+"&displayDur="+displayDur+"&calName="+calName;	%>
	<button id="add_eve_tovisits" name="add_eve_tovisits" onclick="addEventsToVisitsWindow('<%=veLink%>',<%=pageRight%>,'<%=calStatDesc%>')"><%=MC.M_AddMul_EvtToVisits%></button>
	<%}%>
	
	<%--//YK: Added for PCAL-20461 - Button to Access the Modify Sequence/Display window --%>
	<%if (!calStatus.equals("A") && !calStatus.equals("F") && !calStatus.equals("D"))
		{%>
    <button id="openNewWindow" name="openNewWindow" onclick="loadModifySequenceDialog('<%=calStatus%>','<%=pageRight %>','<%=calledFrom%>','<%=protocolId%>','<%=calassoc%>','ALL')"><%=LC.L_ModSeq_Disp%></button>
    <%}else{
    	%>
    <button id="openNewWindow" name="openNewWindow" disabled  style="display: none;"><%=LC.L_ModSeq_Disp%></button>
        <%
    }%>
    
&nbsp;<span id="searchEvent"><input type="text"  size="10"  name="eventName" id="eventName" onkeypress="if (fnOnceEnterKeyPress(event)>0) {reloadDataGrid('asc');} " >&nbsp;<button type="submit"   onClick="reloadDataGrid('asc');"><%=LC.L_Search%>&nbsp;<%=LC.L_Event%></button>&nbsp;<button type="submit" onClick="searchClear();" ><%=LC.L_Clear %>&nbsp;<%=LC.L_Search%></button></span>&nbsp;&nbsp;&nbsp;
&nbsp;<%if(("N".equalsIgnoreCase(LC.VIEW_ALL))){ %>
<a id="viewAllVisit" href="javascript:void(0);"  onclick="reloadGridAsFetchViewAll('viewAllVisit');">View All</a> 
<%} else{ %>
<a id="viewAllVisit" href="javascript:void(0);" style=" display: none;" onclick="reloadGridAsFetchViewAll('viewAllVisit');">View All</a>
<%} %>
</td>
  </td>
  <td colspan="3" align="right">
  
  <table>
  	<tr>
  	
 
   
   
    <td><a href="javascript:void(0);" onclick="resetSort();"><%=LC.L_Reset_Sort%></a></td> <%-- YK : Modified for Bug#7470 --%>
    <%if ((!protocolId.equals("null"))) {%>
			<td><!-- Ak:Added for enhancement PCAL-20071 -->
			<%
			String acId=(String)tSession.getValue("accountId");
			
			ReportDaoNew repDao = new ReportDaoNew();
			repDao.getReports("rep_calendar",EJBUtil.stringToNum(acId));
			
			StringBuffer sb = new StringBuffer();
	
			sb.append("<SELECT id='calReps' NAME='calReps' onchange='setdropdownTest(this)' >") ;
			for (int cnt = 0; cnt <= repDao.getPkReport().size() -1 ; cnt++)
			{
				sb.append("<OPTION value = '"+ repDao.getPkReport().get(cnt)+"' >" + repDao.getRepName().get(cnt)+ "</OPTION>");
	
			}
			sb.append("</SELECT>");
			String repdD = sb.toString();
			%>
			<%=repdD%></td>
			<td>
			<button onClick="openExport('html')"><%=LC.L_Display%></button>
			</td>
			</tr>
			<tr align="right">
			<td colspan="5" align="left">
				
				<div align="right">
					  	<span id="visitsSpan"><a id="firstVisits" href="#" onclick="reloadGridAsFetch('firstVisits');"><%=LC.L_First%></a>
					    &nbsp;<a id="previousVisits" href="#" onclick="reloadGridAsFetch('previousVisits');"><%=LC.L_Previous_Visits%></a>
					    &nbsp;<a id="nextVisits" href="#" onclick="reloadGridAsFetch('nextVisits');"><%=LC.L_Next_Visits%></a>
					    &nbsp;<a id="lastVisits" href="#" onclick="reloadGridAsFetch('lastVisits');"><%=LC.L_Last%></a></span>
			    </div>
			</td>
			</tr>
	<%}%>
  	</tr>
  </table>
  </td>	
  </tr>
  </table>
  <table >
  <tr>
  	<td colspan="6" >
  		<div id="datagrid" onmouseout="return nd();" class="yui-dt yui-dt-scrollable"></div>
  	</td>
  </tr>
  <tr>
  <td width="100%">
   <span id="eventsSpan">&nbsp;&nbsp;<a id="firstEvents" href="#" onclick="reloadGridAsFetch('firstEvents');"><%=LC.L_First%></a>
    &nbsp;&nbsp;<a id="previousEvents" href="#" onclick="reloadGridAsFetch('previousEvents');"><%=LC.L_Previous_Events%></a>
    &nbsp;&nbsp;<a id="nextEvents" href="#" onclick="reloadGridAsFetch('nextEvents');"><%=LC.L_Next_Events%></a>
    &nbsp;&nbsp;<a id="lastEvents" href="#" onclick="reloadGridAsFetch('lastEvents');"><%=LC.L_Last%></a></span>
  </td>
  </tr>
  </table>
  
  <%-- YK: Added for PCAL-20461 Start of Edit Visit modal dialog --%>
<div id="editVisitDiv" title="<%=LC.L_ModSeq_Disp%>" style="display:none">
<div id='embedDataHere'>
</div>
</div>
<%-- Yk: Modified for Enhancement :PCAL-19743 --%>
<input type="hidden" name="sortOrder" id="sortOrder" value="asc">     <%-- Akshi: Added for Bug#7646 --%>
<input type="hidden" name="srcmenu" id="srcmenu" value="<%=src%>">
<input type="hidden" name="duration" id="duration" value="<%=duration%>">
<input type="hidden" name="protocolId" id="protocolId" value="<%=protocolId%>">
<input type="hidden" name="calledFrom" id="calledFrom" value="<%=calledFrom%>">
<input type="hidden" name="studyId" id="studyId" value="<%=studyId%>"/>
<input type="hidden" name="mode" id="mode" value="<%=mode%>">
<input type="hidden" name="calStatus" id="calStatus" value="<%=calStatus%>">
<input type="hidden" name="calassoc" id="calassoc" value="<%=calassoc%>">
<%-- End of Edit Visit modal dialog --%>
<%
  } // end of pageRight
  else {
%>
<!--12-04-2011 #6013 @Ankit  -->
	<jsp:include page="accessdenied.jsp" flush="true"/>
<%
  } // end of else of pageRight
} // end of else of invalid protocolId
} else { // else of valid session
%>
  <jsp:include page="timeout.html" flush="true"/>
<%
} // end of else of valid session
%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu" > 
  <jsp:include page="getmenu.jsp" flush="true"/> 
</div>
<script>
if(1=="<%=vclick1%>"){
	document.getElementById("viewAllVisit").innerHTML="View By Pagination";
	$j('#visitsSpan').css('display', 'none');
	$j('#eventsSpan').css('display', 'none');
}

function myscroll(){
    $j(".hdiv").scrollLeft($j(".vdiv").scrollLeft());
    $j(".cdiv").scrollTop($j(".vdiv").scrollTop());
};//end of my scroll


var dropdownTest='';

function setdropdownTest(that){
	dropdownTest = that.options[that.selectedIndex].innerHTML;	
}
function openExport(format) {
	if( dropdownTest == 'Study Calendar Report'){
		var newWin = window.open("donotdelete.html","exportWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=yes,status=no,left=20,top=20");
		newWin.document.write('<html>');
		newWin.document.write("<head><title><%=LC.L_Coverage_AnalysisExport%></title></head><body>");/*newWin.document.write('<head><title>Coverage Analysis Export</title></head><body>');*****/
		newWin.document.write('<form name="dummy" target="_self" action="fetchCoverageExport.jsp?exportToFlag=exportToFlagofprscode&');
		newWin.document.write('format='+format+'&');
		newWin.document.write('<%=urlParamSBForCACal.toString()%>');
		newWin.document.write('" method="POST">');
		newWin.document.write('<input type="hidden" id="key" name="key" ');
		newWin.document.write(' value="<%=keySessId%>"/>');	
		newWin.document.write('</form>');
		newWin.document.write('</body></html>');
		newWin.document.close();
		newWin.document.dummy.submit();
	}else{

		validateReport(document.protocoltab);
		}	
}
</script>
</body>
</html>
<%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
%>
