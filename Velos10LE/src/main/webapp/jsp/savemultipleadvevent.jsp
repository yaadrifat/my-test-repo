<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<%--

	Project Name:		Velos eResearch
	Author:				Jnanamay Majumdar
	Created on Date:	4Dec2007
	Purpose:			update page for Add multiple Adverse Events
	File Name:			savemultipleadvevent.jsp

--%>
<jsp:include page="localization.jsp" flush="true"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<body>
  

  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>  
  <jsp:useBean id="advnotifyB" scope="request" class="com.velos.esch.web.advNotify.AdvNotifyJB"/>
  <jsp:useBean id="advinfoB" scope="request" class="com.velos.esch.web.advInfo.AdvInfoJB"/>
  <jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
  <%@ page language = "java" import = "com.velos.esch.business.common.*, com.velos.esch.web.advEve.AdvEveJB,com.velos.eres.web.specimen.SpecimenJB,com.velos.eres.web.specimenStatus.SpecimenStatusJB,com.velos.eres.business.common.*,java.util.*,java.text.*,com.velos.eres.service.util.*,java.text.SimpleDateFormat, java.text.Format"%>  
  
  
  <%

	String eSign = request.getParameter("eSign");	
		
	HttpSession tSession = request.getSession(true); 

	if (sessionmaint.isValidSession(tSession))

   {
   
    String user = null;
	user = (String) tSession.getValue("userId");

	String accId = (String) tSession.getValue("accountId");	
	
	String ipAdd = null;
	ipAdd = (String) tSession.getValue("ipAdd");
   
   
     %>
     <jsp:include page="sessionlogging.jsp" flush="true"/> 

     <%   
   
   	String oldESign = (String) tSession.getValue("eSign");
	
	if(!oldESign.equals(eSign)) 
	{
%>
  	   <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} 
	else 
  	{
  	
  			
  	
  	 String studyId =request.getParameter("studyId");
  	 
  	 studyB.setId(EJBUtil.stringToNum(studyId));

	  studyB.getStudyDetails();
  	 	
	 String pkey = request.getParameter("pkey");
  	
  	 
  	 ArrayList advNotifyCodeIds = null;
  	 ArrayList  outcomeIds = null;  
  	 ArrayList  addInfoIds = null;  	 
  	 ArrayList codelstDescs = null;	
  	 
  	 String advInfoOutcomeType = "O";
  	 String advInfoAddInfo = "A";	   
	 
	 
	 String advEveOutTypeStr = "";
  	 String advEveAddInfoStr = "";  
	 //above 2 string not supposed to be hardcoded
	 
	 SchCodeDao addInfo= new SchCodeDao();	
	 addInfo.getCodeValues("adve_info");
	 int infoRows = 0;
	 
	 infoRows = addInfo.getCRows();
	 
	 //prepare String
	 
	 for (int k=0;k<infoRows;k++)
	 {
	 	advEveAddInfoStr = advEveAddInfoStr + "0" 	;
	 } 
	 
	 SchCodeDao outcome = new SchCodeDao();	
	  outcome.getCodeValues("outcome"); 
	 
	 infoRows = outcome.getCRows();
	 
	 //prepare String
	 
	 for (int k=0;k<infoRows;k++)
	 {
	 	advEveOutTypeStr = advEveOutTypeStr + "0" 	;
	 } 
	 
	    
  	 
  	 String advNotifyCodeId = "";	
  	 String outcomeId = "";
  	 String addInfoId = "";
  	 
  	 char strIndex='0';
  	 int cnt =0;
  	 int len = 0;
  	 int length = 0;
				
  	 int advEventId = -1;
  	 int countRows = 0;
  	
  	countRows = EJBUtil.stringToNum(request.getParameter("cntNumrows"));  	
  	
   	
  	String[] advEtyp = new String[countRows];
  	String[] gradeTxt = new String[countRows];
  	String[] grade = new String[countRows];
  	String[] advName = new String[countRows];
  	String[] medDRAcode  = new String[countRows];
  	String[] advRecovery = new String[countRows];
  	String[] trtCourse = new String[countRows];
  	String[] startDate = new String[countRows];
  	String[] stopDate = new String[countRows];
  	String[] adveReln = new String[countRows];
  	String[] advAct = new String[countRows];  
  	String[] aetoxicity = new String[countRows];
  	String[] aetoxicityDesc = new String[countRows];
  	String[] aegradeDesc = new String[countRows];
  	String[] aecategory = new String[countRows];
  
  	//get AE dictionary settings
  	
  	int index=-1;
    String studyLkpDict = studyB.getStudyAdvlkpVer();
    if (EJBUtil.isEmpty(studyLkpDict))
		studyLkpDict = "0";
   int studyLkpDictId=EJBUtil.stringToNum(studyLkpDict);			
		LookupDao lookupDao = new LookupDao();
		lookupDao = studyB.getAllViewsForLkpType(EJBUtil.stringToNum(accId) ,"NCI");
		ArrayList viewIds = lookupDao.getLViewIds();
		ArrayList viewNames = lookupDao.getLViewNames();		
		String settingValue="";
		
	 if (studyLkpDictId==0)
	 {
		settingValue="";
	 }
	 else 
	 {
	   index=viewIds.indexOf(new Integer(studyLkpDict));
	   settingValue=(String)viewNames.get(index);
	  }
	  
	  if (StringUtil.isEmpty(settingValue))
	  {
	  	settingValue =LC.L_Free_TextEntry/*Free Text Entry*****/;
	  }
  	
  	////////////
  	  	  	  	  	  	  	  	  	
 for (int g=0 ; g<countRows ; g++) {


 	  advEtyp[g] = request.getParameter("advEtype"+g); 		  	  	  	  	  	  	
   	  gradeTxt[g] = request.getParameter("advGradeText"+g)==null?"":request.getParameter("advGradeText"+g);
   	  grade[g] = request.getParameter("grade"+g)==null?"":request.getParameter("grade"+g); 	
   	  advName[g] = request.getParameter("advName"+g); 	
   	  medDRAcode[g] = request.getParameter("MedDRAcode"+g); 	
	  trtCourse[g] = request.getParameter("treatment"+g); 		  
  	  startDate[g] = request.getParameter("startDt"+g); 
  	  stopDate[g] = request.getParameter("stopDt"+g); 
  	  adveReln[g] = request.getParameter("adveRelation"+g); 
  	  advAct[g] = request.getParameter("action"+g); 
  	  advRecovery[g] = request.getParameter("recovery"+g); 
  	  aetoxicity[g] = request.getParameter("aeToxicity"+g);	  	  	  	  	  	  	
  	  aetoxicityDesc[g] = request.getParameter("aeToxicityDesc"+g);
  	  aegradeDesc[g] = request.getParameter("aeGradeDesc"+g);
  	  aecategory[g] = request.getParameter("aeCategory"+g);
  	 
  	if((!gradeTxt[g].equalsIgnoreCase("")) && (grade[g].equalsIgnoreCase(""))){
  	  String[] parts=gradeTxt[g].split(":");
  	  String strgrade = parts[0];
	  String strsevere= parts[1];
  	  grade[g]= strgrade; 	
	  aegradeDesc[g]=strsevere;
  	
  	  	}
  	 
 }
 for (int f=0 ; f<countRows ; f++) {
  	
	 gradeTxt[f] = request.getParameter("advGradeText"+f);
  	AdvEveJB adveventB = new AdvEveJB();  	

  	
  	if (!advEtyp[f].equals("")){  	

  		//KV:3961
        if(!StringUtil.isEmpty( gradeTxt[f])){
        adveventB.setAdvEveDictionary(settingValue);//advdictionary
 		}
 		else{
 		adveventB.setAdvEveDictionary("");
 		}	
            
	   adveventB.setAdvEveCodelstAeTypeId(advEtyp[f]);   
	   adveventB.setAdvEveRelationship(adveReln[f]);
	   adveventB.setAdvEveRecoveryDesc(advRecovery[f]);   	   
	   adveventB.setAdvEveTreatment(trtCourse[f]);
	   adveventB.setAdvEveStDate(startDate[f]);
	   adveventB.setAdvEveEndDate(stopDate[f]);  
	   adveventB.setAdvEveOutType(advEveOutTypeStr);
	   adveventB.setAdvEveAddInfo(advEveAddInfoStr);	   
	   adveventB.setFkStudy(studyId);
	   adveventB.setFkPer(pkey);
	   adveventB.setAdvEveEnterBy(user); 
	   adveventB.setFkOutcomeAction(advAct[f]);
	   if ( (gradeTxt[f] == null) || (gradeTxt[f].trim().equals(""))){
		   	grade[f] = null ;
	   		advName[f] = "";   
	   }

	
   		adveventB.setAdvEveGrade(grade[f]);
  		adveventB.setAdvEveName(advName[f]);  		
		adveventB.setAdvEveMedDRA(medDRAcode[f]);
		adveventB.setAeToxicity(aetoxicity[f]);
		adveventB.setAeToxicityDesc(aetoxicityDesc[f]);
		adveventB.setAeGradeDesc(aegradeDesc[f]);
		adveventB.setAeCategory(aecategory[f]);
		adveventB.setCreator(user);
		adveventB.setIpAdd(ipAdd);   
		advEventId = adveventB.setAdvEveDetails();
		
				//JM:23Jan2008: #3254: Adv evt notification 		
			
					SchCodeDao advNotify= new SchCodeDao();					
					advNotify.getCodeValues("adve_notify"); 
					codelstDescs=advNotify.getCDesc(); 				
					advNotifyCodeIds=advNotify.getCId();				
					
				
				 	length=codelstDescs.size();	
				
				for(cnt =0;cnt< length; cnt++ ){					
					
					advNotifyCodeId=(String) advNotifyCodeIds.get(cnt).toString();					
					
					advnotifyB.setAdvNotifyAdverseId((new Integer(advEventId)).toString());
					advnotifyB.setAdvNotifyCodelstNotTypeId(advNotifyCodeId);						
					advnotifyB.setAdvNotifyValue(""+strIndex);						
					advnotifyB.setCreator(user);
					advnotifyB.setIpAdd(ipAdd);
												
					advnotifyB.setAdvNotifyDetails();		
				}
				
				//JM:23Jan2008: #3254: Outcome Information 					
	
					  
					codelstDescs=outcome.getCDesc();
					outcomeIds=outcome.getCId();
								
					len=codelstDescs.size();	
				
				for(cnt =0;cnt< len; cnt++ ){	
					
					outcomeId=(String) outcomeIds.get(cnt).toString();	
										
					advinfoB.setAdvInfoAdverseId((new Integer(advEventId)).toString());
					advinfoB.setAdvInfoCodelstInfoId(outcomeId);
					advinfoB.setAdvInfoType(advInfoOutcomeType);					
					advinfoB.setAdvInfoValue(""+strIndex);					
					advinfoB.setCreator(user);
					advinfoB.setIpAdd(ipAdd);
					
					advinfoB.setAdvInfoDetails();		
				}
				
					//JM:23Jan2008: #3254: Additional Information
					 		
					 
					codelstDescs=addInfo.getCDesc();
					addInfoIds=addInfo.getCId();
					
					len=codelstDescs.size();	
					
					String advInfoAddId =null;
				
				
				for(cnt =0;cnt< len; cnt++ ){
					
					addInfoId = (String) addInfoIds.get(cnt).toString();					
					
					advinfoB.setAdvInfoAdverseId((new Integer(advEventId)).toString());
					advinfoB.setAdvInfoCodelstInfoId(addInfoId);
					advinfoB.setAdvInfoType(advInfoAddInfo);
					advinfoB.setAdvInfoValue(""+strIndex);					
					advinfoB.setCreator(user);
					advinfoB.setIpAdd(ipAdd);   
							
					advinfoB.setAdvInfoDetails();
		
				}
		
		
	}//if advEtyp is not null, end loop
 }
	  	
  	
	if(advEventId>=0){		
%>
	  <br>
      <br>
      <br>
      <br>
      <br>
      <p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully.*****--%></p>   
      
     
     <script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
	  </script>	 
    
	  
<%} 

	else if(advEventId==-3){		
	%>
		
	  <br>
      <br>
      <br>
      <br>
      <br>
      <p class = "successfulmsg" align = "center" > <%=MC.M_DataCnt_Svd%><%--Data could not be saved*****--%></p>
      <table width="70%" >
      <tr>
      <td width="10%"></td>
      <td width="60%" align="center">&nbsp;&nbsp;
      <button onclick="history.go(-1);"><%=LC.L_Back%></button>
</td></tr>	</table>
		
		<%
		
       }else{%>

      <br>
      <br>
      <br>
      <br>
      <br>
      <p class = "successfulmsg" align = "center" > <%=MC.M_DataCnt_Svd%><%--Data could not be saved*****--%></p>
      <table width="70%" >
      <tr>
      <td width="10%"></td>
      <td width="60%" align="center">&nbsp;&nbsp;
      <button onclick="history.go(-1);"><%=LC.L_Back%></button>
</td></tr>	</table> 
     
<%}
		

}//end of eSign check

}//end session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

	<div class = "myHomebottomPanel"> 
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>



</BODY>

</HTML>
