<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%--My Reports*****--%><%=LC.L_My_Rpts%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT>
function fMakeVisible(type){
	document.all("year_t").style.visibility = "hidden";
	document.all("month_t").style.visibility = "hidden";
	document.all("date_t").style.visibility = "hidden";
	typ = document.all(type);
	typ.style.visibility = "visible";
}

function fOpenWindow(type,section,form){
	if (section==2) { //uncheck the upper filter range if lower range is selected
		for(i=0;i<form.filterType.length;i++)	{
			form.filterType[i].checked=false;
		}
	}
	
	if (section==1) { //uncheck the lower filter range if upper range is selected
		for(i=0;i<form.filterType2.length;i++)	{
			form.filterType2[i].checked=false;
		}
	}

	width=300;
	height= 300;
	if (type == "Y"){
		width=200;
		height=70;
	}
	if (type == "D"){
		width=520;
		height=320;
	}
	if (type == "M"){
		width=335;
		height=70;
	}		
	form.year.value ="";
	form.year1.value ="";
	form.month.value ="";
	form.dateFrom.value ="";
	form.dateTo.value ="";
	for (cnt=0;cnt <2;cnt++){
		form.range[cnt].value ="";
	}
	form.range[section - 1].value ="";
	if (type == 'A'){
			form.range[section - 1].value = "<%=LC.L_All%> "/*form.range[section - 1].value = "All"*****/
	}
	 if (type != 'A'){
		windowName=window.open("selectDateRange.jsp?parm="+type +"&section=" +section,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=" +width +",height="+height)
		windowName.focus();
	}

}

function fValidate(form){
	reportChecked=false;
	for(i=0;i<form.reportName.length;i++){
		sel = form.reportName[i].checked;
		if (form.reportName[i].checked){
	   		reportChecked=true;
		}
	}
	if (reportChecked==false) {
		/*alert("Please select a Report");*****/
		alert("<%=MC.M_Selc_Rpt%>");
		return false;
	}
	
	reportNo = form.repId.value;

	switch (reportNo) {
		case "1": //Active Protocol
			case "6":
			dateChecked=false;
			/*for(i=0;i<document.reports.filterType.length;i++){
				sel = document.reports.filterType[i].checked;
				if (document.reports.filterType[i].checked){
	   				dateChecked=true;
				}
			}*/
			
//			alert("*" +document.reports.range[0].value +"*")
			if (form.range[0].value != ""){
	   				dateChecked=true;
			}
			
			if (dateChecked==false) {
				/*alert("Please select a Date Range");*****/
				alert("<%=MC.M_Selc_DtRange%>");
				return false;
			}
			break;
			
		case "2": //Protocol by user			
			if (form.selUser.value == "None") {
				/*alert("Please select a User");*****/
				alert("<%=MC.M_Selc_User%>");
				return false;
			}	
			break;
		case "5": //Protocol by Therapeutic Area
			dateChecked=false;
			/*for(i=0;i<document.reports.filterType3.length;i++){
				sel = document.reports.filterType3[i].checked;
				if (document.reports.filterType3[i].checked){
	   				dateChecked=true;
				}
			}*/

			if (form.range[1].value != ""){
   				dateChecked=true;
			}

			if (dateChecked==false) {
				/*alert("Please select a Date Range");*****/
				alert("<%=MC.M_Selc_DtRange%>");
				return false;
			}
			
			if (form.tArea.value == "") {
				/*alert("Please select a Therapeutic Area");*****/
				alert("<%=MC.M_Selc_ThrpArea%>");
				return false;
			}	
			
			break;			

		case "7": //Protocol by Organization
			if (form.orgn.value == "") {
				/*alert("Please select an Organization");*****/
				alert("<%=MC.M_Selc_AnOrg%>");
				return false;
			}	
			
			break;
			
		case "43": //User Profile
			if (form.selUser.value == "None") {
				/*alert("Please select a User");*****/
				alert("<%=MC.M_Selc_User%>");
				return false;
			}	
			
			break;
								
			for(i=0;i<form.filterType.length;i++)	{
				if (form.filterType[i].checked){
				
				if (form.filterType[i].value == "4") {
					if (form.dateFrom.value == "") {
						/*alert("Please select From Date");*****/
						alert("<%=MC.M_Selc_FromDate%>");
						return false;
					}
					if (form.dateTo.value == "") {
						/*alert("Please select To Date");*****/
						alert("<%=MC.M_Selc_ToDate%>");
						return false;
					}
	
				}
				}
			}	
		}	
		form.submit();	
}

function fSetId(ddtype, frmname){
	if (ddtype == "report") {//report Id and name are concatenated by %.Need to separate them
		//alert(frmname.name);		
		for(i=0;i<frmname.reportName.length;i++)	{
			if (frmname.reportName[i].checked){
				lsReport = frmname.reportName[i].value;	
				ind = lsReport.indexOf("%");
				frmname.repId.value = lsReport.substring(0,ind);	
				frmname.repName.value = lsReport.substring(ind+1,lsReport.length);
				break;
			}
		}
	}
}
</SCRIPT>

<SCRIPT language="JavaScript1.1">
function openwin12() {
      windowName=window.open("usersearchdetails.jsp?fname=&lname=","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
	windowName.focus();
;}
</SCRIPT>


</head>



<% String src="";

src= request.getParameter("srcmenu");

%>	



<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   


<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="repdao" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="repdao1" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="repdao2" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="repdao3" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>

<%@ page language = "java" import = "com.velos.eres.business.section.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>

<body>

<br>
<DIV class="formDefault" id="div1">

<%

   HttpSession tSession = request.getSession(true); 

   if (sessionmaint.isValidSession(tSession))

   {
   
   	  int pageRight = 0;

      GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");		

      pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("REPORTS"));
	  
	  if (pageRight > 0 )
	  {
	  
       	String uName =(String) tSession.getValue("userName");	 
      	String acc = (String) tSession.getValue("accountId");
     	String tab = request.getParameter("selectedTab");	
     	StringBuffer orgn = new StringBuffer();	
     	StringBuffer user = new StringBuffer();
     	StringBuffer user1 = new StringBuffer();
     	Calendar cal = Calendar.getInstance();
     	int currYear = cal.get(cal.YEAR);
     
     	int userId=0;
     	int counter=0;
     	int tAreaId =0;
     	StringBuffer tArea=new StringBuffer();	
     	int siteId=0;	
     	CodeDao cdTArea = new CodeDao();   
     	SiteDao siteDao = new SiteDao();	
     	cdTArea.getCodeValues("tarea"); 
       	    UserDao userDao = new UserDao();
     
     	tArea.append("<SELECT NAME=tArea>") ;
     	for (counter = 0; counter <= (cdTArea.getCDesc()).size() -1 ; counter++){
     		tAreaId = ((Integer)((cdTArea.getCId()).get(counter))).intValue(); 
     		tArea.append("<OPTION value = "+ tAreaId +">" + (cdTArea.getCDesc()).get(counter) + "</OPTION>");
     	}
     
     	tArea.append("</SELECT>");
     	
     	siteDao.getSiteValues(EJBUtil.stringToNum(acc));
     	orgn.append("<SELECT NAME=orgn >") ;
     	for (counter = 0; counter <= (siteDao.getSiteIds()).size() -1 ; counter++){
     		siteId = ((Integer)((siteDao.getSiteIds()).get(counter))).intValue(); 
     		orgn.append("<OPTION value = "+ siteId +">" + (siteDao.getSiteNames()).get(counter) + "</OPTION>");
     	}
     	orgn.append("</SELECT>");
     %>
		<%/* SV, 8/24/04, fix for bug 1673, _new is not a keyword, _blank is and it opens new window instance each time which is what we want for reports.*/%>
	
     	 
		<Form Name="reports" method="post" action="repRetrieve.jsp" target="_blank" >
		
	      <P class = "userName"><%= uName %></P>
         
         <jsp:include page="reporttabs.jsp" flush="true"/>
         
         <input type="hidden" name="srcmenu" value='<%=src%>'>
         
         <input type="hidden" name="selectedTab" value='<%=tab%>'>
         
         <br>
         <table width="100%" cellspacing="0" cellpadding="0" border=0 >
         <tr>
          <td colspan = 3>
         <%--<p class = "defComments">Select a report from the list below, specify appropriate filters and click on the 'Display' button. </p>*****--%>
		 <p class = "defComments"><%=MC.M_SelRpt_ListBelow%> </p>
         <BR>
         </td>
         </tr>
         
         <tr>
          <td width=40%>
         	<Font class=numberFonts ><%--1.*****--%><%=LC.L_1%>.</font><Font class=reportText ><I><%--Select your report type*****--%><%=MC.M_Selc_YourRptType%> </I></font>
         </td>
          <td width=40%>
         	<Font class=numberFonts ><%--2.*****--%><%=LC.L_2%>.</font> <Font class=reportText ><I><%--Select your report filters*****--%><%=MC.M_Selc_YourRptFilters%></I></font>
         </td>
          <td width=20%>
         	<Font class=numberFonts ><%--3.*****--%><%=LC.L_3%>.</font> <Font class=reportText ><I><%--View your report*****--%><%=LC.L_View_YourRpt%></I></font>
         </td>
         </tr>
         </Table>
         <HR class=blue></hr>
         <input type=hidden name=year>
         <input type=hidden name=month>
         <input type=hidden name=year1>
         <input type=hidden name=dateFrom>
         <input type=hidden name=dateTo>
                 
         
         <Table border=0 width=100%>
         <tr>
         	 <td colspan=3>
         	<p class = "reportHeadings"><%--General Account Reports*****--%><%=LC.L_General_AccRpts%></p>
         	</td>	
         	</tr>
         <%
         	CtrlDao ctrl = new CtrlDao();
         	ctrl.getControlValues("accspecific");
         	String catId = (String) ctrl.getCValue().get(0);
         
         	int accId = EJBUtil.stringToNum(acc);
         	repdao.getAllRep(EJBUtil.stringToNum(catId),accId); //Account Reports
         	ArrayList repIds= repdao.getPkReport(); 
         	ArrayList names= repdao.getRepName();
            	ArrayList desc= repdao.getRepDesc();
         	
         	int repId=0; 
         	String name="";
         
         	int len= repIds.size() ; 
         	
         	/*************************************************************/
         	CtrlDao ctrl1 = new CtrlDao();
         	ctrl1.getControlValues("usrspecific");
         	catId = (String) ctrl1.getCValue().get(0);
         	repdao1.getAllRep(EJBUtil.stringToNum(catId),accId); //User Related Reports
         	ArrayList repIdsUserRelated = repdao1.getPkReport(); 
         	ArrayList namesUserRelated = repdao1.getRepName();
            	ArrayList descUserRelated= repdao1.getRepDesc();
         
         	int repIdUserRelated=0; 
         	String nameUserRelated="";
         
         	int lenUserRelated = repIdsUserRelated.size() ;
         	
         	CtrlDao ctrl2 = new CtrlDao();
         	ctrl2.getControlValues("orgspecific");
         	catId = (String) ctrl2.getCValue().get(0);
         	repdao2.getAllRep(EJBUtil.stringToNum(catId),accId); //Orgn Related Reports
         	ArrayList repIdsOrgRelated = repdao2.getPkReport(); 
         	ArrayList namesOrgRelated = repdao2.getRepName();
            	ArrayList descOrgRelated= repdao2.getRepDesc();
         
         	int repIdOrgRelated=0; 
         	String nameOrgRelated="";
         
         	int lenOrgRelated = repIdsOrgRelated.size() ;
         	
         	CtrlDao ctrl3 = new CtrlDao();
         	ctrl3.getControlValues("tareaspecific");
         	catId = (String) ctrl3.getCValue().get(0);
         	repdao3.getAllRep(EJBUtil.stringToNum(catId),accId); //TArea Related Reports
         	ArrayList repIdsTareaRelated = repdao3.getPkReport(); 
         	ArrayList namesTareaRelated = repdao3.getRepName();
            	ArrayList descTareaRelated= repdao3.getRepDesc();
         
         	int repIdTareaRelated=0; 
         	String nameTareaRelated="";
         
         	int lenTareaRelated = repIdsTareaRelated.size() ;
         	
         	   
         	/*************************************************************/
         	%>
         	<tr>
         	<td width=40%>
         
         	<% for(counter = 0;counter<len;counter++)
         	{
         		repId = ((Integer)repIds.get(counter)).intValue();
         		name=((names.get(counter)) == null)?"-":(names.get(counter)).toString();
         	%>
         		<Input Type="radio" name="reportName" value="<%=repId%>%<%=name%>" onClick="fSetId('report', document.reports)"> <%=name%>
         		<br>
         	<%}%>
         </td>
         	<td width=50%>
         	<Input type="radio" name="filterType" value="1" onClick="fOpenWindow('A','1', document.reports)"> <%--All*****--%> <%=LC.L_All%> 
         	<Input type="radio" name="filterType" value="2" onClick="fOpenWindow('Y','1', document.reports)"> <%--Year*****--%><%=LC.L_Year%>
         	<Input type="radio" name="filterType" value="3" onClick="fOpenWindow('M','1', document.reports)"> <%--Month*****--%><%=LC.L_Month%>
         	<Input type="radio" name="filterType" value="4" onClick="fOpenWindow('D','1', document.reports)"> <%--Date Range*****--%><%=LC.L_Date_Range%>
         	<BR><Font class=comments><%--Selected Date Filter*****--%><%=LC.L_Selected_DateFilter%>:</Font>
         	<input type=text name=range size=30 READONLY>
         	</td>
	 
         <td width=10%>
         </td>
         </table>
         
         <HR class=blue></hr>
         <Br>
         <table border=0 width=100%>
         	<tr>
         	 <td colspan=3>
         	<p class = "reportHeadings"><%--User Specific Reports*****--%><%=LC.L_User_SpecificRpts%></p>
         	</td>	
         	</tr>
         	<tr>
         	<td width=40%>
         
         	<% for(counter = 0;counter < lenUserRelated;counter++)
         	{
         		repId = ((Integer)repIdsUserRelated.get(counter)).intValue();
         		name=((namesUserRelated.get(counter)) == null)?"-":(namesUserRelated.get(counter)).toString();
         	%>
         		<Input Type="radio" name="reportName" value="<%=repId%>%<%=name%>" onClick="fSetId('report', document.reports)"> <%=name%>
         		<br>
         	<%}%>
         </td>
         	<td width=50%>
         		<%--<p class = "defComments"> For user specific reports, clicking on the link will bring up the <A href="#" onclick="openwin12()">User Search</A> window. Currently the selected user is *****--%>
				<p class = "defComments"> <%=MC.M_UsrSpecificRpts_ClkOnLnk%> <A href="#" onclick="openwin12()"><%=LC.L_User_Search%></A> <%=MC.M_WinCurr_SelcUsr%>: 
         		<Input type=text name=selUser size=20 READONLY value=None></p>
         	 </td>
		 
         <td width=10%></td>
         </table>
         <HR class=blue></hr>
         <Br>
         <table border=0 width=100%>
         	<tr>
         	 <td colspan=3>
         	<p class = "reportHeadings"><%--Organization Specific Reports*****--%><%=LC.L_Org_SpecificRpts%></p>
         	</td>	
         	</tr>
         	<tr>
         	<td width=40%>
         
         	<% for(counter = 0;counter<lenOrgRelated;counter++)
         	{
         		repId = ((Integer)repIdsOrgRelated.get(counter)).intValue();
         		name=((namesOrgRelated.get(counter)) == null)?"-":(namesOrgRelated.get(counter)).toString();
         	%>
         		<Input Type="radio" name="reportName" value="<%=repId%>%<%=name%>" onClick="fSetId('report', document.reports)"> <%=name%>
         		<br>
         	<%}%>
         </td>
	  	<td width=50%><Font class=comments>
	      	<%--Select Organization*****--%><%=LC.L_Select_Org%> <%=orgn%></Font>
      	</td>
         <td width=10%></td>
         </table>
         
         <HR class=blue></hr>
         <Br>
         <table border=0 width=100%>
         	<tr>
         	 <td colspan=3>
         	<p class = "reportHeadings"><%--Therapeutic Area Specific Reports*****--%><%=MC.M_ThrpAreaSpecificRpts%></p>
         	</td>	
         	</tr>
         	<tr>

         	 </td>
         	</td>
         	<td width=40%>
         
         	<% for(counter = 0;counter<lenTareaRelated;counter++)
         	{
         		repId = ((Integer)repIdsTareaRelated.get(counter)).intValue();
         		name=((namesTareaRelated.get(counter)) == null)?"-":(namesTareaRelated.get(counter)).toString();
         	%>
         		<Input Type="radio" name="reportName" value="<%=repId%>%<%=name%>" onClick="fSetId('report', document.reports)"> <%=name%>
         		<br>
         	<%}%>
         </td>
	       	<td width=50%><Font class=comments>
         	<%--Therapeutic Area*****--%><%=LC.L_Therapeutic_Area%> <%=tArea%></Font>
         	<BR>
         	<Input type="radio" name="filterType2" value="1"  onClick="fOpenWindow('A','2',document.reports)"><%--All*****--%> <%=LC.L_All%> 
         	<Input type="radio" name="filterType2" value="2" onClick="fOpenWindow('Y','2',document.reports)"> <%--Year*****--%><%=LC.L_Year%>
         	<Input type="radio" name="filterType2" value="3" onClick="fOpenWindow('M','2',document.reports)"> <%--Month*****--%><%=LC.L_Month%>
         	<Input type="radio" name="filterType2" value="4" onClick="fOpenWindow('D','2',document.reports)"> <%--Date Range*****--%><%=LC.L_Date_Range%>
         	<BR><Font class=comments><%--Selected Date Filter*****--%><%=LC.L_Selected_DateFilter%>:</Font>
         	<input type=text name=range size=30 READONLY>
         	</td>		 
         <td width=10%></td>
         </table>
         
         
         <table width="100%" cellspacing="0" cellpadding="0" border=0 >
         <tr> 
         	<td colspan=2 align=right>
         		<!--<Input type="submit" name="submit" value="Display" >-->
         	  <A onClick = "return fValidate(document.reports)" href="#"> <image src="../images/jpg/Display.gif"  align="absmiddle" border="0"> </A>
         	</td>
         </tr>
         </table>
         
         <Input type = hidden  name=id >
         <Input type = hidden  name=accId value="<%=acc%>">
         <Input type = hidden  name="val" >
         <Input type=hidden name="repId">
         <Input type=hidden name="repName">
         
</form>
<%

} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right


} //end of if session times out

else

{

%>

<jsp:include page="timeout.html" flush="true"/> 



<%

} 

%>


<div>

<jsp:include page="bottompanel.jsp" flush="true"> 

</jsp:include>   

</div>


</div>



<DIV class="mainMenu" id = "emenu">

  <jsp:include page="getmenu.jsp" flush="true"/>   

</DIV>



</body>

</html>
