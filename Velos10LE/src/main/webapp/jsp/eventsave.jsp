<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<%@ page language = "java" import = "com.velos.esch.service.util.EJBUtil,com.velos.eres.service.util.*"%>

<%

String src = request.getParameter("srcmenu");
String duration = request.getParameter("duration");
String protocolId = request.getParameter("protocolId");
String calledFrom = request.getParameter("calledFrom");
String eventmode = request.getParameter("eventmode");
String fromPage = request.getParameter("fromPage");
String mode = request.getParameter("mode");
String calStatus = request.getParameter("calStatus");
String categoryId = "";
String eventId = "";

String ename = "";
ename = request.getParameter("eventName");

 String eSign = request.getParameter("eSign");

%>
<%
HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession)) {	
	String oldESign = (String) tSession.getValue("eSign");


	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
   String ipAdd = (String) tSession.getValue("ipAdd");
   String usr = (String) tSession.getValue("userId");
   String accountId = (String) tSession.getValue("accountId");
   if(eventmode.equals("N")) {
	categoryId = request.getParameter("categoryId");
	if (calledFrom.equals("P")){
		eventdefB.setChain_id(categoryId);
	}
	if (calledFrom.equals("S")){
		eventdefB.setChain_id(categoryId);
	}


   } else {
	eventId = request.getParameter("eventId");
	if (fromPage.equals("eventbrowser") || (fromPage.equals("fetchProt") && calledFrom.equals("P"))){
		eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));
		eventdefB.getEventdefDetails();
	}else {
		eventassocB.setEvent_id(EJBUtil.stringToNum(eventId));
		eventassocB.getEventAssocDetails();
	}
   }
   int eventDuration = eventDuration = EJBUtil.stringToNum(request.getParameter("eventDuration"));
   if(request.getParameter("eventDurDays").equals("Weeks"))
   {
	eventDuration = eventDuration*7;
   }

   String eventFuzzyPer = request.getParameter("eventFuzzyPer");	 
   String eventFuzzyPerDays = request.getParameter("eventFuzzyPerDays");	 
   if(eventFuzzyPer.equals("0")) {
	eventFuzzyPerDays = "0";
   } else {
	String eventFuzzyPerBA = request.getParameter("eventFuzzyPerBA");
	if(eventFuzzyPerBA.equals("Before")) {
	   eventFuzzyPerDays = "-"+ eventFuzzyPerDays;}
	else if(eventFuzzyPerBA.equals("After")) {
	   eventFuzzyPerDays = "+" + eventFuzzyPerDays;
	}
   }


   String eventMsgPerDays = request.getParameter("eventMsgPerDays");	 
   String eventMsgPerBA = request.getParameter("eventMsgPerBA");
   if(eventMsgPerBA.equals("Before")) {
	eventMsgPerDays = "-"+ eventMsgPerDays;
	} else if(eventMsgPerBA.equals("After")) {
	 	eventMsgPerDays = "+"+ eventMsgPerDays;
   }


	if (fromPage.equals("eventbrowser") || (fromPage.equals("fetchProt") && calledFrom.equals("P"))){
		eventdefB.setName(request.getParameter("eventName"));
		eventdefB.setDescription(request.getParameter("eventDesc"));	
		eventdefB.setDuration((new Integer(eventDuration)).toString());
		eventdefB.setFuzzy_period(eventFuzzyPerDays);
	        eventdefB.setNotes(request.getParameter("eventNotes"));
		if(request.getParameter("eventMsgFlag").equals("1")) { 
	      	   eventdefB.setEvent_msg(request.getParameter("eventMsg"));
		   eventdefB.setMsg_to(request.getParameter("eventMsgTo"));
		   eventdefB.setMsg_details(eventMsgPerDays);
		} else { 
		   eventdefB.setEvent_msg("");
		   eventdefB.setMsg_to("");
		   eventdefB.setMsg_details("");
		}
	}else {
		eventassocB.setName(request.getParameter("eventName"));
		eventassocB.setDescription(request.getParameter("eventDesc"));	
		eventassocB.setDuration((new Integer(eventDuration)).toString());
	      	eventassocB.setFuzzy_period(eventFuzzyPerDays);
            	eventassocB.setNotes(request.getParameter("eventNotes"));
		if(request.getParameter("eventMsgFlag").equals("1")) { 
            	   eventassocB.setEvent_msg(request.getParameter("eventMsg"));
                   eventassocB.setMsg_to(request.getParameter("eventMsgTo"));
		   eventassocB.setMsg_details(eventMsgPerDays);
		} else { 
	    	   eventassocB.setEvent_msg("");
               	   eventassocB.setMsg_to("");
		   eventassocB.setMsg_details("");
		}
	}
   if(eventmode.equals("N")) {
	eventdefB.setUser_id(accountId);
	eventdefB.setEvent_type("E");
	eventdefB.setCreator(usr); 
	eventdefB.setIpAdd(ipAdd);
	eventdefB.setEventdefDetails();
	eventId = (new Integer(eventdefB.getEvent_id())).toString();
	if (eventdefB.getEvent_id()== -1){%>
			<br><br><br><br><br><p class = "sectionHeadings" align = center>
			<%=MC.M_EvtNameDupli_ClkBack%><%--Event Name is duplicate. Click on "Back" Button to change the name*****--%> <Br><Br>
			<button onclick="window.history.back();"><%=LC.L_Back%></button></p>
		<% return;}
   } else {
	if (fromPage.equals("eventbrowser") || (fromPage.equals("fetchProt") && calledFrom.equals("P"))){
		eventdefB.setModifiedBy(usr);
		eventdefB.setIpAdd(ipAdd);
		eventdefB.updateEventdef();
	}else {
		eventassocB.setModifiedBy(usr);
		eventassocB.setIpAdd(ipAdd);
		eventassocB.updateEventAssoc();
	}
   }

tSession.putValue("eventname",ename);
%>

<META HTTP-EQUIV=Refresh CONTENT="1; URL=addeventinfo.jsp?srcmenu=<%=src%>&duration=<%=duration%>&eventId=<%=eventId%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&eventname=<%=ename%>">
<%
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>


</BODY>
</HTML>


