/*
 * Classname : EventAssocAgentRObj
 *
 * Version information: 1.0
 *
 * Date: 06/12/2001
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana
 */

package com.velos.esch.service.eventassocAgent;

/* Import Statements */
import java.util.ArrayList;
import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.esch.business.common.EventAssocDao;
import com.velos.esch.business.common.EventInfoDao;
import com.velos.esch.business.eventassoc.impl.EventAssocBean;

@Remote
public interface EventAssocAgentRObj {

    EventAssocBean getEventAssocDetails(int eventassocId);

    public int setEventAssocDetails(EventAssocBean account);

    public int updateEventAssoc(EventAssocBean usk);
    
    public int updateEventAssoc(EventAssocBean usk, boolean checkEvtFlag);
    
    public int createEventsAssoc(String[] eIdList, String[] vIdList, String[] seqList,
    		String[] dispList, String[] hideList,
    		int userId, String ipAdd);

    public int removeEventAssocChild(int event_id);

    public int removeEventAssocHeader(int chain_id);

    public int removeEventAssocChilds(int event_ids[]);

    public int removeEventAssocHeaders(int chain_ids[]);

    public int AddProtToStudy(String protocol_id, String study_id,
            String userId, String ipAdd,String type);
       
    public int createProtocolBudget(int protocol_id, int bgtTemplate, String bgtName,
            int userId, String ipAdd, int studyId);
    
    public EventAssocDao getStudyProts(int study_id);

//  JM: 04May2007
    public EventAssocDao getStudyProtsActive(int study_id);

    public EventAssocDao getStudyProts(int study_id, int budgetId);

    public void AddEventsToProtocol(String chain_id, String[][] events_disps,
            int fromDisp, int toDisp, String userId, String ipAdd);

    public int DeleteProtocolEvents(String chain_id);

    public int DeleteProtocolEventsPastDuration(String chain_id, int duration); // SV,
                                                                                // 8/19

    public EventAssocDao getFilteredEvents(int user_id, int protocol_id);

    public EventAssocDao getAllProtAndEvents(int event_id);

    public EventAssocDao getPrevStatus(int protocolId);

    //Modified by Manimaran to fix the issue #3394
    public EventAssocDao getProtSelectedEvents(int protocolId, String orderType, String orderBy);

    public EventAssocDao getFilteredRangeEvents(int protocolId, int beginDisp,
            int endDisp);

    public EventAssocDao getProtRowEvents(int protocolId, int rownum,
            int beginDisp, int endDisp);

    public int RemoveProtEventInstance(int eventId, String whichTable);

    public int copyStudyProtocol(String protocolId, String protocolName,
            String userId, String ipAdd,String accId, String calAssoc);

    public EventAssocDao fetchCalTemplate(String protocolId);

    public EventInfoDao getAlertNotifyValues(int patprotId);

    public int getMaxCost(String chainId);

    public int studyCalendarDelete(int studyId, int calendarId);
    
    public int studyCalendarDelete(int studyId, int calendarId, Hashtable<String, String> argsvalues);//Overloaded for INF-18183 ::: Ankit

    public EventAssocDao getStudyProtsWithAlnotStat(int study_id);

    public EventAssocDao getStudyAdminProts(int study_id,String status);

    public EventAssocDao getStudyProts(int study_id, String status);



    // / // Changes Made for enhancements Phase II
    // / By Sonia Kaura
    // Date : 11/3/2003
    //public EventAssocDao getAllProtSelectedEvents(int protocolId);
    //JM: 16Apr2008
    public EventAssocDao getAllProtSelectedEvents(int protocolId, String visitId, String evtName);
    //YK: FOR PCAL-20461
    public EventAssocDao getAllProtSelectedVisitsEvents(int protocolId, String visitId, String evtName);
    
    public EventAssocDao getProtSelectedAndGroupedEvents(int protocolId, boolean isExport, int finDetRight,String eventName);
    

    public EventAssocDao getProtSelectedAndGroupedEvents(int protocolId, boolean isExport, int finDetRight,int initEventSize,int limitEventSize,String visitids,String eventName);
    public EventAssocDao getProtSelectedAndGroupedEvents(int protocolId, boolean isExport, int finDetRight);
    
    /*YK :: DFIN12 FIX FOR BUG#5954-- RESOLVES  #5947 #5943 BUGS ALSO*/
     public EventAssocDao getEventVisitMileStoneGrid(int protocolId);
    //Yk: Added for Enhancement :PCAL-19743
     public EventAssocDao getProtSelectedAndVisitEvents(int protocolId);
     public EventAssocDao getProtSelectedAndVisitEvents(int protocolId,String eventName);
     public EventAssocDao getProtSelectedAndVisitEvents(int protocolId,int initEventSize,int initVisitSize,int limitEventSize,int limitVisitSize,String visitIds,String eventName );
    
    /** Refreshes the messages for all patients enrolled on this calendar (only current schedules) */
    /* Sonia Abrol, 10/30/06*/
    public int refreshCalendarMessages(String protocolId, String studyId, String modifiedBy,
            String ipAdd);

    public EventAssocDao getProtVisitAndEvents(int protocolId, String search, String resetSort) ;//JM: 10April2008 /*YK 04Jan- SUT_1_Requirement_18489*/

    public int getMaxSeqForVisitEvents(int visitId, String eventName, String tableName);//JM: 10April2008

    //JM: 17Apr2008
    public int CopyEventDetails(String old_event_id, String new_event_id, String crfInfo, String userId, String ipAdd);

    public void updateOfflnFlgForEventsVisits(int event_id, String offlineFlag);//JM: 23Apr2008, added for, Enh. #C9
    public int hideUnhideEventsVisits(String[] eventIds, String hide_flag, String[] flags);//JM: 25Apr2008, added for, Enh. #C9
    public void updatePatientSchedulesNow(int calId, int checkedVal, int statusId, java.sql.Date selDate, int userId, String ipAdd );//JM: 28Apr2008, added for, Enh. #C9

  //JM: 06May2008, added for, Enh. #PS16
    //public void updateUnscheduledEvents(String chain_id, String[][] events_disps,
      //      int fromDisp, int toDisp, int seq, String userId, String ipAdd);
    public String[] updateUnscheduledEvents(String chain_id, String[][] events_disps,
            int fromDisp, int toDisp, int seq, String userId, String ipAdd);

    public void updateUnscheduledEvents1(int patId, int patProtId, int calId, int userId, String ipAdd, String[] eventsArr); //JM: 06May2008, added for, Enh. #PS16

    public  Hashtable getVisitEventForScheduledEvent(String schevents1_pk);

//  JM: 16July2008, added for the issue #3586
    
    // Overloaded for INF-18183 ::: Akshi
    public int deleteEventStatusHistory(int eventStatusHistId,Hashtable<String, String> args);
    
    public int deleteEventStatusHistory(int eventStatusHistId);
    public void updateCurrentStatus( int evtId );
    
    public int getEventFormAccees(int crfLinkedForm, int assocId, int portalId);
    
  //Added by Virendra to create new calendar from eSP
    public int setEventAssocCalendarDetails(EventAssocBean account);
    
    //Added by Ankit to identify the role assigned to the user/team.
    public  String getStudyUserRole(String studyId, String userId);
}