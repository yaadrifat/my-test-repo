/*
 * Classname : AlertNotifyABean
 * 
 * Version information: 1.0
 *
 * Date: 12/11/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Arvind
 */

package com.velos.esch.service.alertNotifyAgent.impl;

/* IMPORT STATEMENTS */

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.esch.business.alertNotify.impl.AlertNotifyBean;
import com.velos.esch.business.common.EventAssocDao;
import com.velos.esch.business.common.EventInfoDao;
import com.velos.esch.business.common.EventdefDao;
import com.velos.esch.service.alertNotifyAgent.AlertNotifyAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;
import com.velos.esch.web.eventassoc.EventAssocJB;

// import com.velos.esch.business.eventdef.*;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP
 * AlertNotifyBean.<br>
 * <br>
 * 
 */
@Stateless
public class AlertNotifyAgentBean implements AlertNotifyAgentRObj {
    @PersistenceContext(unitName = "esch")
    protected EntityManager em;

    /**
     * Calls getAlertNotifyDetails() on AlertNotify Entity Bean AlertNotifyBean.
     * Looks up on the AlertNotify id parameter passed in and constructs and
     * returns a Alert state holder. containing all the summary details of the
     * AlertNotify<br>
     * 
     * @param alNotId
     *            the AlertNotify id
     * @ee AlertNotifyBean
     */
    public AlertNotifyBean getAlertNotifyDetails(int alNotId) {
        AlertNotifyBean alert = null;

        try {

            alert = (AlertNotifyBean) em.find(AlertNotifyBean.class,
                    new Integer(alNotId));
        } catch (Exception e) {
            Rlog.debug("alertNotify",
                    "Error in getAlertNotifyDetails() in AlertNotifyAgentBean"
                            + e);
        }

        return alert;
    }

    /**
     * Calls setAlertNotifyDetails() on AlertNotify Entity Bean
     * alertNotifyBean.Creates a new AlertNotify with the values in the
     * StateKeeper object.
     * 
     * @param alert
     *            a alertNotify state keeper containing alertNotify attributes
     *            for the new AlertNotify.
     * @return int - 0 if successful; -2 for Exception;
     */

    public int setAlertNotifyDetails(AlertNotifyBean edsk) {
        String patProtId = "";
        try {
            EventAssocJB eventAssocB = new EventAssocJB();
            patProtId = edsk.getAlertNotifyPatProtId();

            em.merge(edsk);

            // update notification flag (orig_cal) for the study calendar for
            // which
            // notifications are being added
            try {
                if (com.velos.eres.service.util.EJBUtil.isEmpty(patProtId)) {
                    eventAssocB.setEvent_id(com.velos.eres.service.util.EJBUtil
                            .stringToNum(edsk.getAlertNotifyProtocol()));
                    eventAssocB.getEventAssocDetails();
                    eventAssocB.setOrigCal("1");
                    eventAssocB.updateEventAssoc();
                }
            } catch (Exception e) {
                Rlog.fatal("alert",
                        "AlertAgentBean.setAlertDetails Exception in setting Eventassoc orig_cal flag"
                                + e);
            }

            return (edsk.getAlNotId());
        } catch (Exception e) {
            Rlog.fatal("alert",
                    "Error in setAlertNotifyDetails() in AlertNotifyAgentBean "
                            + e);
        }
        return 0;
    }

    public int updateAlertNotify(AlertNotifyBean edsk) {

        int output;
        EventAssocJB eventAssocB = new EventAssocJB();
        String patProtId = edsk.getAlertNotifyPatProtId();
        AlertNotifyBean alert = null;
        try {
            alert = em.find(AlertNotifyBean.class, edsk.getAlNotId());
            if (alert == null) {
                return -2;
            }
            output = alert.updateAlertNotify(edsk);
            // update notification flag (orig_cal) for the study calendar for
            // which
            // notifications are being updated
            try {
                if (com.velos.eres.service.util.EJBUtil.isEmpty(patProtId)) {
                    eventAssocB.setEvent_id(com.velos.eres.service.util.EJBUtil
                            .stringToNum(edsk.getAlertNotifyProtocol()));
                    eventAssocB.getEventAssocDetails();
                    eventAssocB.setOrigCal("1");
                    eventAssocB.updateEventAssoc();
                }
            } catch (Exception e) {
                Rlog
                        .fatal(
                                "alert",
                                "AlertAgentBean.updateAlertNotify Exception in setting Eventassoc orig_cal flag"
                                        + e);
            }
        } catch (Exception e) {
            Rlog.fatal("alertNotify",
                    "Error in updateAlertNotify in AlertNotifyAgentBean" + e);
            return -2;
        }
        return output;
    }

    public int updateAlertNotifyList(String[] anIds, String[] flags,
            String[] userIds, String[] mobIds, String lastModifiedBy,
            String ipAdd, String studyId, String protocolId) {

        int output = 0;

        String alertnotify = "";
        EventAssocJB eventAssocB = new EventAssocJB();

        try {
            for (int i = 0; i < anIds.length; i++) {
                AlertNotifyBean alert = null;

                alertnotify = anIds[i];

                alert = em.find(AlertNotifyBean.class, EJBUtil
                        .stringToNum(alertnotify));
                if (alert == null) {
                    return -2;
                }
                alert.setAlertNotifyAlNotFlag(flags[i]);
                alert.setAlertNotifyAlNotUsers(userIds[i]);
                alert.setAlertNotifyAlNotMobEve(mobIds[i]);
                alert.setModifiedBy(lastModifiedBy);
                alert.setIpAdd(ipAdd);
                alert.setAlertNotifyStudy(studyId);
                alert.setAlertNotifyProtocol(protocolId);
                output = 0;
            }
            
            // update notification flag (orig_cal) for the study calendar for
            // which
            // notifications are being added
            try {
                    eventAssocB.setEvent_id(com.velos.eres.service.util.EJBUtil
                            .stringToNum(protocolId));
                    eventAssocB.getEventAssocDetails();
                    eventAssocB.setOrigCal("1");
                    eventAssocB.updateEventAssoc();
                
            } catch (Exception e) {
                Rlog.fatal("alert",
                        "AlertAgentBean.updateAlertNotifyList Exception in setting Eventassoc orig_cal flag"
                                + e);
            }


        } catch (Exception e) {
            Rlog.fatal("alertNotify",
                    "Error in updateAlertNotify in AlertNotifyAgentBean" + e);
            return -2;
        }
        return output;
    }

    public EventInfoDao getStudyAlertNotify(int studyId, int protocolId) {
        String alSetting = "N";
        EventInfoDao alDao = new EventInfoDao();

        try {
            alDao.getStudyAlertNotifyValues(studyId, protocolId);

        } catch (Exception e) {
            Rlog.fatal("alertNotify",
                    "EXCEPTION IN getting study ALERT NOTIFY DETAILS agent bean"
                            + e);
        }
        return alDao;

    }

    public String getANGlobalFlag(int studyId, int protocolId) {
        String anGlobalFlag = "";
        EventInfoDao alDao = new EventInfoDao();

        try {
            anGlobalFlag = alDao.getANGlobalFlag(studyId, protocolId);

        } catch (Exception e) {
            Rlog
                    .fatal("alertNotify",
                            "EXCEPTION IN getting study getANGlobalFlag agent bean"
                                    + e);
        }
        return anGlobalFlag;

    }

    public int updateStudyAlertNotify(int studyId, int protocolId,
            int modifiedBy, String ipAdd, String globFlag, int lockValue) {
        EventdefDao evDao = new EventdefDao();
        int returnVal = 0;

        try {
            returnVal = evDao.updateStudyAlertNotify(studyId, protocolId,
                    modifiedBy, ipAdd, globFlag, lockValue);

        } catch (Exception e) {
            Rlog.fatal("alertNotify",
                    "EXCEPTION IN updating study ALERT NOTIFY settings ** agent bean"
                            + e);
            returnVal = -1;
        }
        return returnVal;

    }

    public int notifyAdmin(int userId, String ipAdd, int velosUser,
            String failDate, String failTime) {
        EventdefDao evDao = new EventdefDao();
        int returnVal = 0;

        try {
            returnVal = evDao.notifyAdmin(userId, ipAdd, velosUser, failDate,
                    failTime);

        } catch (Exception e) {
            Rlog.fatal("alertNotify",
                    "EXCEPTION IN notifyAdmin ** alertnotify agent bean" + e);
            returnVal = -1;
        }
        return returnVal;

    }

    public int setDefStudyAlnot(String studyid, String protocolid,
            String creator, String ipAdd) {

        EventAssocDao evDao = new EventAssocDao();
        int returnVal = 0;

        try {
            evDao.setDefStudyAlnot(studyid, protocolid, creator, ipAdd);

        } catch (Exception e) {
            Rlog.fatal("alertNotify",
                    "EXCEPTION IN setDefStudyAlnot ** alertnotify agent bean"
                            + e);
            returnVal = -1;
        }
        return returnVal;

    }

}// end of class
