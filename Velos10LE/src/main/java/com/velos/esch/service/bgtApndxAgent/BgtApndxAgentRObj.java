/* 
 * Classname			BgtApndxAgentRObj.class
 * 
 * Version information
 *
 * Date					03/25/2002
 *
 * Author 				Sajal
 * Copyright notice
 */

package com.velos.esch.service.bgtApndxAgent;

/* Import Statements */

import java.util.Hashtable;
import javax.ejb.Remote;

import com.velos.esch.business.bgtApndx.impl.BgtApndxBean;
import com.velos.esch.business.common.BgtApndxDao;

@Remote
public interface BgtApndxAgentRObj {

    BgtApndxBean getBgtApndxDetails(int bgtApndxId);

    public int setBgtApndxDetails(BgtApndxBean bask);

    public int updateBgtApndx(BgtApndxBean bask);

    public BgtApndxDao getBgtApndxUrls(int budgetId);

    public BgtApndxDao getBgtApndxFiles(int budgetId);

    public int bgtApndxDelete(int bgtApndxId);
    
    // Overloaded for INF-18183 ::: AGodara
    public int bgtApndxDelete(int bgtApndxId,Hashtable<String, String> auditInfo);

}