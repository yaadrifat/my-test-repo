/*
 * Classname:			SubCostItemAgentRObj
 * 
 * Version information : 1.0  
 *
 * Date					01/19/2011
 * 
 * Copyright notice : Velos Inc
 */
package com.velos.esch.service.subCostItemAgent;

import javax.ejb.Remote;
import java.util.ArrayList;
import java.util.Hashtable;

import com.velos.esch.business.common.SubCostItemDao;
import com.velos.esch.business.subCostItem.impl.SubCostItemBean;

/**
 * Remote interface for SubCostItemAgentBean session EJB
 * 
 * @author Manimaran
 * @version 1.0, 01/19/2011
 */
@Remote
public interface SubCostItemAgentRObj {
	public SubCostItemBean getSubCostItemDetails(int subCostItemId);

    public int setSubCostItemDetails(SubCostItemBean subCostItem);
    
    public int updateSubCostItem(SubCostItemBean subCostItem);
    /*YK Bug 5821 & # 5818
	 * Code Changed to Remove physically from Table.
	 * */
    
    public int removeSubCostItem(int itemId);
    
    // removeSubCostItem() Overloaded for INF-18183 ::: Raviesh
    public int removeSubCostItem(int itemId,Hashtable<String, String> args);
    
    
    
    public SubCostItemDao getProtSelectedAndGroupedItems(int protocolId);
    
    //KM-#5949
    public int deleteSubCostItems(ArrayList visitIds, ArrayList itemIds, int user);
    
    
    // removeSubCostItem() Overloaded for INF-18183 ::: Raviesh
    public int deleteSubCostItems(ArrayList visitIds, ArrayList itemIds, int user,Hashtable<String, String> args);   
}