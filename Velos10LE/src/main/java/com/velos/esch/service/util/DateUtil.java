/** Class provides the various functions on Date like converting string to date
 and java.util.Date to java.sql.Date etc.,
 Autor : Dharani Babu on 07/27/2000.
 */

package com.velos.esch.service.util;

import java.text.SimpleDateFormat;

public class DateUtil {
    /*
     * Converts the string in the given format to the java.util.Date.
     * (e.g)stringToDate("05/23/1978","MM/dd/yyyy") gives the Date object with
     * date as 23rd may 1978.
     */
    public static java.util.Date stringToDate(String dtString, String format) {
        java.util.Date dt = null;

        Rlog.debug("common", "{ORIG FORMAT:}" + format);

        if (format == null || format.equals(""))
            format = "MM/dd/yyyy";

        Rlog.debug("common", "{CHANGED FORMAT:}" + format);
        Rlog.debug("common", "{ORIG DATE STR:}" + dtString);

        if (dtString == null || dtString.equals(""))
            return null; // here exception should be thrown

        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Rlog.debug("common", "{GOT SIMPLE DT FORMAT:}" + sdf);
        try {
            dt = sdf.parse(dtString);
            Rlog.debug("common", "{DT:}" + dt);
        } catch (Exception e) {
            Rlog.debug("common", "{EXCEPTION IN CONVERTING TO DATE:}" + e);
        }
        return dt;
    }

    /** 
     */

    public static String dateToString(java.util.Date dt) {
        try {
            Rlog.debug("common", "{Date VALUE:}" + dt);

            String sReturn = "";

            if (dt == null) {
                return sReturn;
            } else {
                // changed by Sajal to fix the unparseable date error
                SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                sReturn = df.format(dt);
                // sReturn = dt.toString();
                // till here
                Rlog.debug("common", "{String VALUE:}" + sReturn);
                return sReturn;
            }
        } catch (Exception e) {
            Rlog.debug("common", "{EXCEPTION IN CONVERTING DATE TO STRING:}"
                    + e);
            return null;
        }

    }

    /* Method converts date to string with the specified format */

    public static String dateToString(java.util.Date dt, String format) {
        try {
            String sReturn = "";
            if (dt == null) {
                return sReturn;
            } else

            {
                SimpleDateFormat df = new SimpleDateFormat(format);
                sReturn = df.format(dt);
                Rlog.debug("date", "{String VALUE:}" + sReturn);
                return sReturn;
            }
        } catch (Exception e) {
            Rlog.fatal("date", "{EXCEPTION IN CONVERTING DATE TO STRING:}" + e);
            return null;
        }
    }

    public static java.sql.Date dateToSqlDate(java.util.Date dt) {
        java.sql.Date sqlDt = null;
        try {
            sqlDt = new java.sql.Date(dt.getTime());
        } catch (Exception e) {
            Rlog.debug("common", "dateToSQlDate:ex:" + e.getMessage());
        }
        return sqlDt;
    }

    public static java.sql.Timestamp dateToSqlTimestamp(java.util.Date dt) {
        java.sql.Timestamp sqlDt = null;
        try {
            sqlDt = new java.sql.Timestamp(dt.getTime());
        } catch (Exception e) {
            Rlog.debug("common", "dateToSQlTimestamp:ex:" + e.getMessage());
        }
        return sqlDt;
    }
}
