/*
 * Classname : EventcostAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 06/20/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana 
 */

package com.velos.esch.service.eventcostAgent.impl;

/* IMPORT STATEMENTS */

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.esch.business.crflib.impl.CrflibBean;
import com.velos.esch.business.eventcost.impl.EventcostBean;
import com.velos.esch.service.eventcostAgent.EventcostAgentRObj;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP
 * EventcostBean.<br>
 * <br>
 * 
 */
@Stateless
public class EventcostAgentBean implements EventcostAgentRObj {
    @PersistenceContext(unitName = "esch")
    protected EntityManager em;

    /**
     * Calls getEventcostDetails() on Eventcost Entity Bean EventcostBean. Looks
     * up on the Eventcost id parameter passed in and constructs and returns a
     * Eventcost state holder. containing all the summary details of the
     * Eventcost<br>
     * 
     * @param eventcostId
     *            the Eventcost id
     * @ee EventcostBean
     */
    public EventcostBean getEventcostDetails(int eventcostId) {
    	EventcostBean eventcost=null;//vivek
        try {
        	eventcost=(EventcostBean) em.find(EventcostBean.class, new Integer(
                    eventcostId));
        	em.clear();//vivek
//            return (EventcostBean) em.find(EventcostBean.class, new Integer(
//                    eventcostId));
        	return eventcost;
        } catch (Exception e) {
            System.out
                    .print("Error in getEventcostDetails() in EventcostAgentBean"
                            + e);
            return null;
        }

    }

    /**
     * Calls setEventcostDetails() on Eventcost Entity Bean
     * EventcostBean.Creates a new Eventcost with the values in the StateKeeper
     * object.
     * 
     * @param eventcost
     *            a eventcost state keeper containing eventcost attributes for
     *            the new Eventcost.
     * @return int - 0 if successful; -2 for Exception;
     */

    public int setEventcostDetails(EventcostBean ecsk) {
        try {
        	
        	EventcostBean  cbean = new EventcostBean();
        	
        	cbean.updateEventcost(ecsk);

            em.persist(cbean);
            return (cbean.getEventcostId());
            
            
        } catch (Exception e) {
            System.out
                    .print("Error in setEventcostDetails() in EventcostAgentBean "
                            + e);
        }
        return 0;
    }

    public int updateEventcost(EventcostBean ecsk) {

        EventcostBean retrieved = null; // Eventcost Entity Bean Remote Object
        int output;

        try {

            retrieved = (EventcostBean) em.find(EventcostBean.class, ecsk
                    .getEventcostId());

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateEventcost(ecsk);

        } catch (Exception e) {
            Rlog.debug("eventcost",
                    "Error in updateEventcost in EventcostAgentBean" + e);
            return -2;
        }
        return output;
    }

}// end of class
