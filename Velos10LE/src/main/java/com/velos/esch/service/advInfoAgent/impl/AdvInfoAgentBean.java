/*
 * Classname : AdvInfoAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 12/10/2002
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Arvind
 */

package com.velos.esch.service.advInfoAgent.impl;

/* IMPORT STATEMENTS */

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.esch.business.advInfo.impl.AdvInfoBean;
import com.velos.esch.service.advInfoAgent.AdvInfoAgentRObj;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Stateless session EJB acting as a facade for the CMP entity bean AdvInfoBean
 * and other adverse event info services.
 * 
 * <p>
 * In eResearch Architecture, the class resides at the Service layer.
 * </p>
 * 
 * @author Arvind Kumar
 * @version %I%, %G%
 */
@Stateless
public class AdvInfoAgentBean implements AdvInfoAgentRObj {
    @PersistenceContext(unitName = "esch")
    protected EntityManager em;

    /*
     * * returns an AdvInfoStateKeeper object with Adverse Event Info details.
     * <p> Calls getAdvInfoDetails() on Entity Bean AdvInfoBean and creates and
     * returns an AdvInfoStateKeeper. containing all the details of the
     * AdvInfoBean<br> </p>
     * 
     * @param advInfoId the adverse event info id
     * 
     * @see AdvInfoStateKeeper
     */

    public AdvInfoBean getAdvInfoDetails(int advInfoId) {

        try {

            return (AdvInfoBean) em.find(AdvInfoBean.class, new Integer(
                    advInfoId));

        } catch (Exception e) {
            System.out.print("Error in getAdvInfoDetails() in AdvInfoAgentBean"
                    + e);
            return null;
        }

    }

    /**
     * Calls setAdvInfoDetails() on Entity Bean AdvInfoBean and creates new
     * Adverse Event Info.
     * 
     * @param edsk
     *            AdvInfoStateKeeper object with Adverse Event Info details
     * @return success flag
     *         <ul>
     *         <li> id of new adverse event info</li>
     *         <li> -2 in case of a failure</li>
     *         </ul>
     * @see AdvInfoStateKeeper
     * 
     */

    public int setAdvInfoDetails(AdvInfoBean edsk) {
        try {

            em.merge(edsk);
            return (edsk.getAdvInfoId());
        } catch (Exception e) {
            Rlog.fatal("advInfo",
                    "Error in setAdvInfoDetails() in AdvInfoAgentBean " + e);
        }
        return 0;
    }

    /**
     * Calls updateAdvInfo() on Entity Bean AdvInfoBean and updates an Adverse
     * Event Info.
     * 
     * @param edsk
     *            AdvInfoStateKeeper object with Adverse Event Info details
     * @return success flag
     *         <ul>
     *         <li> 0 if successful</li>
     *         <li> -2 in case of a failure</li>
     *         </ul>
     * @see AdvInfoStateKeeper
     * 
     */

    public int updateAdvInfo(AdvInfoBean edsk) {

        AdvInfoBean retrieved = null; // AdvInfo Entity Bean Remote Object
        int output;

        try {

            retrieved = (AdvInfoBean) em.find(AdvInfoBean.class, edsk
                    .getAdvInfoId());

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateAdvInfo(edsk);

        } catch (Exception e) {
            Rlog.debug("advInfo", "Error in updateAdvInfo in AdvInfoAgentBean"
                    + e);
            return -2;
        }
        return output;
    }

}// end of class
