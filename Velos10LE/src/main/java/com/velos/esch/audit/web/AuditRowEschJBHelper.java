package com.velos.esch.audit.web;

import java.util.ArrayList;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.esch.audit.service.AuditRowEschAgent;
import com.velos.esch.business.common.AuditRowEschDao;
import com.velos.esch.service.util.JNDINames;
import com.velos.eres.service.util.Rlog;

/**
 * A helper class for AuditRowJB. The purpose of this class is to reduce the code size of AuditRowJB
 * by delegating some methods. This class object requires a reference to a AuditRowJB object. When
 * executing a method, this class fills the AuditRowJB object with the data retrieved from DB.
 * 
 * @author Isaac Huang
 */
public class AuditRowEschJBHelper {
	private AuditRowEschJB AuditRowJB = null;
	
	@SuppressWarnings("unused")
	private AuditRowEschJBHelper() {}

	public AuditRowEschJBHelper(AuditRowEschJB AuditRowJB) {
		this.AuditRowJB = AuditRowJB;
	}
	
	private AuditRowEschAgent getAuditRowEschAgent() throws NamingException {
		InitialContext ic = new InitialContext();
		return (AuditRowEschAgent)ic.lookup(JNDINames.AuditRowEschAgentHome);
	}
	public ArrayList getRID(String[] pkey, String tableName, String pkColumnName) {
		AuditRowEschDao as=new AuditRowEschDao();
		ArrayList rs=null;		
		try {
			rs = as.getRID(pkey, tableName, pkColumnName);
		} catch (Exception e) {
			Rlog.fatal("audit", "Exception in AuditRowJBHelper.getRID for pKey="+pkey+" :tableName="+tableName+" "+e);
			rs = null;
		}
		return rs;
	}

	public ArrayList getRID(Integer[] pkey, String tableName, String pkColumnName) {
			AuditRowEschDao as=new AuditRowEschDao();
			ArrayList rs=null;		
			try {
				rs = as.getRID(pkey, tableName, pkColumnName);
			} catch (Exception e) {
				Rlog.fatal("audit", "Exception in AuditRowJBHelper.getRID for pKey="+pkey+" :tableName="+tableName+" "+e);
				rs = null;
			}
			return rs;
		}
	public int getRID(int pkey, String tableName, String pkColumnName) {
		AuditRowEschAgent auditAgent = null;
		int output = 0;
		
		try {
			auditAgent = this.getAuditRowEschAgent();
			output = auditAgent.getRID(pkey, tableName, pkColumnName);
		} catch (Exception e) {
			Rlog.fatal("audit", "Exception in AuditRowJBHelper.getRID for pKey="+pkey+" :tableName="+tableName+" "+e);
			output = -1;
		}
		return output;
	}
	
	public int findLatestRaid(int rid, int userId) {
		AuditRowEschAgent auditAgent = null;
		int output = 0;
		
		try {
			auditAgent = this.getAuditRowEschAgent();
			output = auditAgent.findLatestRaid(rid, userId);
		} catch (Exception e) {
			Rlog.fatal("audit", "Exception in AuditRowJBHelper.findLatestRaid "+e);
			output = -1;
		}
		return output;
	}
	
	public ArrayList findLatestRaid(Integer[] rid, int userId) {
		AuditRowEschAgent auditAgent = null;
		ArrayList output = null;
		
		try {
			AuditRowEschDao ad=new AuditRowEschDao();
			output = ad.findletst(rid, userId);
		} catch (Exception e) {
			Rlog.fatal("audit", "Exception in AuditRowJBHelper.findLatestRaid "+e);
			output =null;
		}
		return output;
	}

	public int setReasonForChange(Integer[] raid, String remarks) {
		AuditRowEschAgent auditAgent = null;
		int output = 0;
		
		try {
			auditAgent = this.getAuditRowEschAgent();
			if (raid.length > 0)
				output = auditAgent.setReasonForChange(raid,remarks);
		} catch (Exception e) {
			Rlog.fatal("audit", "Exception in AuditRowJBHelper.setReasonForChange for raid="+raid+" "+e);
			output = -1;
		}
		return output;
	}

	public int setReasonForChange(int raid, String remarks) {
		AuditRowEschAgent auditAgent = null;
		int output = 0;
		
		try {
			auditAgent = this.getAuditRowEschAgent();
			if (raid > 0)
				output = auditAgent.setReasonForChange(raid,remarks);
		} catch (Exception e) {
			Rlog.fatal("audit", "Exception in AuditRowJBHelper.setReasonForChange for raid="+raid+" "+e);
			output = -1;
		}
		return output;
	}
}
