package com.velos.esch.web.protvisit;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.velos.eres.business.common.SettingsDao;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.common.CommonJB;
import com.velos.esch.business.common.EventAssocDao;
import com.velos.esch.business.common.EventdefDao;
import com.velos.esch.business.common.ProtVisitDao;
import com.velos.esch.business.common.SchCodeDao;
import com.velos.esch.web.eventassoc.EventAssocJB;
import com.velos.esch.web.eventdef.EventdefJB;

public class ProtVisitCoverageJB {

    public ProtVisitCoverageJB() {
    }
    
    public String fetchCoverageJSON (String protocolId, String calledFrom, int finDetRight) throws JSONException {
        return fetchCoverageJSON(protocolId, calledFrom, finDetRight, null);
    }
    
    public String fetchCoverageJSON(String protocolId, String calledFrom, int finDetRight, String accountId) 
    throws JSONException {
    	boolean isExport = (accountId == null)?true:false;

        JSONObject jsObj = new JSONObject();
        StringBuffer sb = new StringBuffer();
        boolean isLibCal = false;
        boolean isStudyCal = false;
        String protocolName = null;
        String calStatus = null;
        int errorCode = 0;
        ArrayList eventIntersectIds = null, eventCPTs = null, eventAddCodes = null;
        ArrayList eventIds = null, eventNames = null;
        ArrayList eventVisitIds = null;
        ArrayList covFkCodelst = null;
        ArrayList covNotesListIn = null;
        
        if ("P".equals(calledFrom) || "L".equals(calledFrom) ) {
            isLibCal = true;
        } else if ("S".equals(calledFrom)) {
            isStudyCal = true;
        } else {
            jsObj.put("error", new Integer(-3));
            jsObj.put("errorMsg", "Calendar Type is missing.");
            return jsObj.toString();
        }
        
        int accId = accountId == null ? 0 : EJBUtil.stringToNum((String) accountId);
        CommonJB commonB = new CommonJB();
        SettingsDao setDao = commonB.getSettingsInstance();
        setDao.getSettingsValue("COVERAGE_ANALYSIS_MODE", accId, 0, 0);
        ArrayList setValArray = setDao.getSettingValue();
        String covMode = null;
        if (setValArray != null && setValArray.size() > 0) {
            covMode = (String)setValArray.get(0);
        }
        if (covMode == null || covMode.length() == 0) { covMode = "V"; } // Make view-first the default mode
        
        EventdefJB eventdefB = new EventdefJB();
        EventAssocJB eventassocB = new EventAssocJB();
        SchCodeDao statDao = new SchCodeDao();
        if (isLibCal) {
            EventdefDao defdao = eventdefB.getProtSelectedAndGroupedEvents(EJBUtil.stringToNum(protocolId), isExport, finDetRight);
            eventIntersectIds = defdao.getNew_event_ids();
            eventIds = defdao.getEvent_ids();
            eventVisitIds = defdao.getFk_visit();
            eventNames = defdao.getNames();
            eventCPTs = defdao.getCptCodes();
            eventAddCodes = defdao.getCodeDescriptions();
            covFkCodelst = defdao.getEventCoverageTypes();
            covNotesListIn = defdao.getEventCoverageNotes();
            eventdefB.setEvent_id(EJBUtil.stringToNum(protocolId));
            eventdefB.getEventdefDetails();
            protocolName = eventdefB.getName();
            
            //JM: 11FEB2011, D-FIN9
            //calStatus = eventdefB.getStatus();          
            
            calStatus = statDao.getCodeSubtype(EJBUtil.stringToNum(eventdefB.getStatCode()));

        } else if (isStudyCal) {
        	System.out.println("fine");
            EventAssocDao assocdao = eventassocB.getProtSelectedAndGroupedEvents(EJBUtil.stringToNum(protocolId), isExport, finDetRight);
           eventIntersectIds = assocdao.getNew_event_ids();
           eventIds = assocdao.getEvent_ids();
           eventVisitIds = assocdao.getEventVisits();
           eventNames = assocdao.getNames();
           eventCPTs = assocdao.getCptCodes();
           eventAddCodes = assocdao.getCodeDescriptions();
           covFkCodelst = assocdao.getEventCoverageTypes();
           covNotesListIn = assocdao.getEventCoverageNotes();
           eventassocB.setEvent_id(EJBUtil.stringToNum(protocolId));
           eventassocB.getEventAssocDetails();
           protocolName = eventassocB.getName();
          
//            //JM: 11FEB2011, D-FIN9
//            //calStatus = eventassocB.getStatus();            
           calStatus = statDao.getCodeSubtype(EJBUtil.stringToNum(eventassocB.getStatCode()));
        }

        ArrayList visitIds = null;
        ArrayList visitNames = null;
        ArrayList visitDisplacements = null;
        String visitId = null;
        String visitName = null;
        
        ProtVisitJB protVisitB = new ProtVisitJB();
        ProtVisitDao visitdao = protVisitB.getProtocolVisits(EJBUtil.stringToNum(protocolId));
        visitIds = visitdao.getVisit_ids();
        visitNames = visitdao.getNames();
        visitDisplacements = visitdao.getDisplacements();

        int noOfEvents = eventIds == null ? 0 : eventIds.size();
        if (protocolName == null) { protocolName = ""; }
        int noOfVisits = visitIds == null ? 0 : visitIds.size();
        
        
        jsObj.put("error", errorCode);
        jsObj.put("errorMsg", "");
        jsObj.put("noOfEvents", noOfEvents);
        jsObj.put("covMode", covMode);
        
        SchCodeDao schDao = new SchCodeDao();
        schDao.getCodeValues("coverage_type");
        ArrayList covSubTypeList = schDao.getCSubType();
        ArrayList covDescList = schDao.getCDesc();
        ArrayList covPkList = schDao.getCId();
        ArrayList<String> covCodeHide=schDao.getCodeHide();
        StringBuffer sb1 = new StringBuffer();
        for (int iX=0; iX<covSubTypeList.size(); iX++) {
            String covSub1 = ((String)covSubTypeList.get(iX)).replaceAll("'", "\\\\'");
            String covDesc1 = ((String)covDescList.get(iX)).replaceAll("'", "\\\\'");
            String covCodeIsHidden =covCodeHide.get(iX);
            if (covSub1 == null || covDesc1 == null) { continue; }
            if(covCodeIsHidden.equalsIgnoreCase("Y")){
            	sb1.append("<b>").append(covSub1).append("</b>").append("=").append(covDesc1).append(" ["+LC.L_Hidden+"]").append("<br/>");
			}else{
				sb1.append("<b>").append(covSub1).append("</b>").append("=").append(covDesc1).append("<br/>");
			}
        }
        String covTypeLegend = sb1.toString();
        jsObj.put("covTypeLegend", covTypeLegend);
        HashMap covMap = new HashMap();
        for (int iX=0; iX<covPkList.size(); iX++) {
        	String covCodeIsHidden =covCodeHide.get(iX);
            if (covCodeIsHidden.equalsIgnoreCase("Y")) { continue; }
            covMap.put(""+covPkList.get(iX), covSubTypeList.get(iX));
        }
        JSONArray covSubTypes = new JSONArray();
        for (int iX=0; iX<covSubTypeList.size(); iX++) {
        	String covCodeIsHidden =covCodeHide.get(iX);
            if (covCodeIsHidden.equalsIgnoreCase("Y")) { continue; }
            JSONObject jsObj1 = new JSONObject();
            jsObj1.put("key", covSubTypeList.get(iX));
            covSubTypes.put(jsObj1);
        }
        jsObj.put("covSubTypes", covSubTypes);
        JSONArray covDescs = new JSONArray();
        for (int iX=0; iX<covDescList.size(); iX++) {
        	String covCodeIsHidden =covCodeHide.get(iX);
            if (covCodeIsHidden.equalsIgnoreCase("Y")) { continue; }
            JSONObject jsObj1 = new JSONObject();
            jsObj1.put("key", covDescList.get(iX));
            covDescs.put(jsObj1);
        }
        jsObj.put("covDescs", covDescs);
        JSONArray covPks = new JSONArray();
        for (int iX=0; iX<covPkList.size(); iX++) {
        	String covCodeIsHidden =covCodeHide.get(iX);
            if (covCodeIsHidden.equalsIgnoreCase("Y")) { continue; }
            JSONObject jsObj1 = new JSONObject();
            jsObj1.put("key", covPkList.get(iX));
            covPks.put(jsObj1);
        }
        jsObj.put("covPks", covPks);
        
        JSONArray jsEvents = new JSONArray();
        for (int iX=0; iX<eventIds.size(); iX++) {
            JSONObject jsObj1 = new JSONObject();
            jsObj1.put(String.valueOf(eventIds.get(iX)), eventNames.get(iX));
            jsEvents.put(jsObj1);
        }
        jsObj.put("events", jsEvents);
        jsObj.put("protocolName", protocolName.replaceAll("<", "&lt;").replaceAll(">", "&gt;"));
        jsObj.put("calStatus", calStatus);
        jsObj.put("noOfVisits", noOfVisits);
        
        JSONArray jsColArray = new JSONArray();
        {
            JSONObject jsObjTemp1 = new JSONObject();
            jsObjTemp1.put("key", "event");
            jsObjTemp1.put("label", LC.L_Event);
            jsColArray.put(jsObjTemp1);
            
            if (StringUtil.isAccessibleFor(finDetRight, 'V')){
	            jsObjTemp1 = new JSONObject();
	            jsObjTemp1.put("key", "eventCPT");
	            jsObjTemp1.put("label", LC.L_Cpt_Code);
	            jsColArray.put(jsObjTemp1);
            }
            if (isExport){
	            jsObjTemp1 = new JSONObject();
	            jsObjTemp1.put("key", "eventAddCode");
	            jsObjTemp1.put("label", LC.L_Addl_Codes);
	            jsObjTemp1.put("hidden", "true");
	            jsColArray.put(jsObjTemp1);
            }

        	jsObjTemp1 = new JSONObject();
            jsObjTemp1.put("key", "eventCoverage");
            jsObjTemp1.put("label", LC.L_Coverage_Type);
            jsObjTemp1.put("hidden", "true");
            jsObjTemp1.put("hideExport", "true");
            jsColArray.put(jsObjTemp1);
            
            JSONObject jsObjTemp3 = new JSONObject();
            if (!"V".equals(covMode)) {
                jsObjTemp3.put("key", "selectAll");
                jsObjTemp3.put("label", LC.L_ApplyAll_Vsts);
                jsColArray.put(jsObjTemp3);
            }
            JSONObject jsObjTemp2 = new JSONObject();
            jsObjTemp2.put("key", "eventId");
            jsObjTemp2.put("label", "Event ID");
            jsColArray.put(jsObjTemp2);
        }
        JSONArray jsDisplacements = new JSONArray();
        for (int iX=0; iX<noOfVisits; iX++) {
            JSONObject jsObj1 = new JSONObject();
            jsObj1.put("key", "v"+String.valueOf(visitIds.get(iX)));
            jsObj1.put("value", visitDisplacements.get(iX));
            jsDisplacements.put(jsObj1);
            JSONObject jsObjTemp = new JSONObject();
            jsObjTemp.put("key", "v"+(visitIds.get(iX)));
            jsObjTemp.put("label", visitNames.get(iX));
            jsObjTemp.put("seq", "v"+(iX+1));
            jsColArray.put(jsObjTemp);
        }
        {
        	JSONObject jsObjTemp1 = new JSONObject();
            jsObjTemp1.put("key", "eventCovNotes");
            jsObjTemp1.put("label", LC.L_Coverage_Notes);
            jsColArray.put(jsObjTemp1);
        }
        jsObj.put("displacements", jsDisplacements);
        jsObj.put("colArray", jsColArray);
        
        ArrayList dataList = new ArrayList();
        ArrayList covList = new ArrayList(); // List of coverage types for performance reasons
        ArrayList covNotesListOut = new ArrayList(); // List of coverage notes for performance reasons
        JSONObject jsCovNotesHash = new JSONObject();
        for (int iX=0; iX<eventIds.size(); iX++) {
            if (((Integer)eventIds.get(iX)).intValue() < 1) { continue; }
            String covSub2 = null;
            if (covFkCodelst.get(iX) != null) { covSub2 = (String)covMap.get((String)covFkCodelst.get(iX)); }
            if (covSub2 == null || covSub2.length() == 0) { covSub2 = "<b>X</b>"; }
            if (!"<b>X</b>".equals(covSub2)) {
                HashMap map1 = new HashMap();
                map1.put("eventId", eventIds.get(iX));
                map1.put("v"+eventVisitIds.get(iX), covSub2);
                covList.add(map1);
            }
            String covNotes = (String)covNotesListIn.get(iX);
            if (covNotes != null && covNotes.trim().length() > 0) {
                HashMap map1 = new HashMap();
                map1.put("eventId", eventIds.get(iX));
                map1.put("v"+eventVisitIds.get(iX), "Y");
                covNotesListOut.add(map1);
                covNotes = covNotes.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
                jsCovNotesHash.put(eventIds.get(iX)+"v"+eventVisitIds.get(iX), covNotes);
            }
            if (covNotes == null) { covNotes = ""; } 
            if (dataList.size() == 0) {
                HashMap map1 = new HashMap();
                map1.put("event", (String)eventNames.get(iX));
                map1.put("eventId", eventIds.get(iX));

                String cptCode = "&nbsp;";
                if (StringUtil.isAccessibleFor(finDetRight, 'V')){
                	cptCode = (eventCPTs.get(iX)== null)?"&nbsp;":(String)eventCPTs.get(iX);
                }
                map1.put("eventCPT", cptCode);

                String addCode = "&nbsp;";
                if (isExport){
                	if (StringUtil.isAccessibleFor(finDetRight, 'V')){
                		addCode = (eventAddCodes.get(iX)== null)?"&nbsp;":(String)eventAddCodes.get(iX);
                	}
                }
                map1.put("eventAddCode", addCode);
                map1.put("eventCoverage", (covFkCodelst.get(iX)== null)?"":(String)covMap.get((String)covFkCodelst.get(iX)));
               	map1.put("eventCovNotes", covNotes);
                map1.put("v"+eventVisitIds.get(iX), covSub2);
                map1.put("notes_v"+eventVisitIds.get(iX), covNotes);
                map1.put("intersectId"+eventVisitIds.get(iX), eventIntersectIds.get(iX));
                dataList.add(map1);
            } else {
                HashMap map1 = (HashMap)dataList.get(dataList.size()-1);
                if (((Integer)map1.get("eventId")).intValue() == ((Integer)eventIds.get(iX)).intValue()) {
                    map1.put("v"+eventVisitIds.get(iX), covSub2);
                    map1.put("notes_v"+eventVisitIds.get(iX), covNotes);
                    map1.put("intersectId"+eventVisitIds.get(iX), eventIntersectIds.get(iX));
                    dataList.set(dataList.size()-1, map1);
                } else {
                    HashMap map2 = new HashMap();
                    map2.put("event", (String)eventNames.get(iX));
                    map2.put("eventId", eventIds.get(iX));

                    String cptCode = "&nbsp;";
                    if (StringUtil.isAccessibleFor(finDetRight, 'V')){
                    	cptCode = (eventCPTs.get(iX)== null)?"&nbsp;":(String)eventCPTs.get(iX);
                    }
                    map2.put("eventCPT", cptCode);

                    String addCode = "&nbsp;";
                    if (isExport){
                    	if (StringUtil.isAccessibleFor(finDetRight, 'V')){
                    		addCode = (eventAddCodes.get(iX)== null)?"&nbsp;":(String)eventAddCodes.get(iX);
                    	}
                    }
                    map2.put("eventAddCode", addCode);                   
                    map2.put("eventCoverage", (covFkCodelst.get(iX)== null)?"":(String)covMap.get((String)covFkCodelst.get(iX)));
                   	map2.put("eventCovNotes", covNotes);
                    map2.put("v"+eventVisitIds.get(iX), covSub2);
                    map2.put("notes_v"+eventVisitIds.get(iX), covNotes);
                    map2.put("intersectId"+eventVisitIds.get(iX), eventIntersectIds.get(iX));
                    dataList.add(map2);
                }
            }
        }
        
        dataList = eventassocB.sortListByKeyword(dataList, "event");
        JSONArray jsEventVisits = new JSONArray();
        for (int iX=0; iX<dataList.size(); iX++) {
            jsEventVisits.put(EJBUtil.hashMapToJSON((HashMap)dataList.get(iX)));
        }
        jsObj.put("dataArray", jsEventVisits);
        
        jsObj.put("covNotesHash", jsCovNotesHash);
        
        for (int iC=0; iC<covList.size(); iC++) {
            Integer covEvent = (Integer)((HashMap)covList.get(iC)).get("eventId");
            for (int iD=0; iD<dataList.size(); iD++) {
                Integer dataEvent = (Integer)((HashMap)dataList.get(iD)).get("eventId");
                if (covEvent.equals(dataEvent)) {
                    HashMap map1 = (HashMap)covList.get(iC);
                    map1.put("eventIndex", new Integer(iD));
                    break;
                }
            }
        }
        JSONArray jsCovArray = new JSONArray();
        for (int iX=0; iX<covList.size(); iX++) {
            jsCovArray.put(EJBUtil.hashMapToJSON((HashMap)covList.get(iX)));
        }
        jsObj.put("coverageArray", jsCovArray);
        
        for (int iC=0; iC<covNotesListOut.size(); iC++) {
            Integer covEvent = (Integer)((HashMap)covNotesListOut.get(iC)).get("eventId");
            for (int iD=0; iD<dataList.size(); iD++) {
                Integer dataEvent = (Integer)((HashMap)dataList.get(iD)).get("eventId");
                if (covEvent.equals(dataEvent)) {
                    HashMap map1 = (HashMap)covNotesListOut.get(iC);
                    map1.put("eventIndex", new Integer(iD));
                    break;
                }
            }
        }
        JSONArray jsCovNotesArray = new JSONArray();
        for (int iX=0; iX<covNotesListOut.size(); iX++) {
            jsCovNotesArray.put(EJBUtil.hashMapToJSON((HashMap)covNotesListOut.get(iX)));
        }
        jsObj.put("coverageNotes", jsCovNotesArray);
        
        return jsObj.toString();
    }
    
    
    
    public String fetchCoverageJSON(String protocolId, String calledFrom, int finDetRight, String accountId,String eventName) 
    throws JSONException {
    	boolean isExport = (accountId == null)?true:false;

        JSONObject jsObj = new JSONObject();
        StringBuffer sb = new StringBuffer();
        boolean isLibCal = false;
        boolean isStudyCal = false;
        String protocolName = null;
        String calStatus = null;
        int errorCode = 0;
        ArrayList eventIntersectIds = null, eventCPTs = null, eventAddCodes = null;
        ArrayList eventIds = null, eventNames = null;
        ArrayList eventVisitIds = null;
        ArrayList covFkCodelst = null;
        ArrayList covNotesListIn = null;
        
        if ("P".equals(calledFrom) || "L".equals(calledFrom) ) {
            isLibCal = true;
        } else if ("S".equals(calledFrom)) {
            isStudyCal = true;
        } else {
            jsObj.put("error", new Integer(-3));
            jsObj.put("errorMsg", "Calendar Type is missing.");
            return jsObj.toString();
        }
        
        int accId = accountId == null ? 0 : EJBUtil.stringToNum((String) accountId);
        CommonJB commonB = new CommonJB();
        SettingsDao setDao = commonB.getSettingsInstance();
        setDao.getSettingsValue("COVERAGE_ANALYSIS_MODE", accId, 0, 0);
        ArrayList setValArray = setDao.getSettingValue();
        String covMode = null;
        if (setValArray != null && setValArray.size() > 0) {
            covMode = (String)setValArray.get(0);
        }
        if (covMode == null || covMode.length() == 0) { covMode = "V"; } // Make view-first the default mode
        
        EventdefJB eventdefB = new EventdefJB();
        EventAssocJB eventassocB = new EventAssocJB();
        SchCodeDao statDao = new SchCodeDao();
        if (isLibCal) {
        	EventdefDao defdao = eventdefB.getProtSelectedAndGroupedEvents(EJBUtil.stringToNum(protocolId), isExport, finDetRight,eventName);
            eventIntersectIds = defdao.getNew_event_ids();
            eventIds = defdao.getEvent_ids();
            eventVisitIds = defdao.getFk_visit();
            eventNames = defdao.getNames();
            eventCPTs = defdao.getCptCodes();
            eventAddCodes = defdao.getCodeDescriptions();
            covFkCodelst = defdao.getEventCoverageTypes();
            covNotesListIn = defdao.getEventCoverageNotes();
            eventdefB.setEvent_id(EJBUtil.stringToNum(protocolId));
            eventdefB.getEventdefDetails();
            protocolName = eventdefB.getName();
            
            //JM: 11FEB2011, D-FIN9
            //calStatus = eventdefB.getStatus();          
            
            calStatus = statDao.getCodeSubtype(EJBUtil.stringToNum(eventdefB.getStatCode()));

        } else if (isStudyCal) {
        	 EventAssocDao assocdao = eventassocB.getProtSelectedAndGroupedEvents(EJBUtil.stringToNum(protocolId), isExport, finDetRight ,eventName);
            eventIntersectIds = assocdao.getNew_event_ids();
            eventIds = assocdao.getEvent_ids();
            eventVisitIds = assocdao.getEventVisits();
            eventNames = assocdao.getNames();
            eventCPTs = assocdao.getCptCodes();
            eventAddCodes = assocdao.getCodeDescriptions();
            covFkCodelst = assocdao.getEventCoverageTypes();
            covNotesListIn = assocdao.getEventCoverageNotes();
            eventassocB.setEvent_id(EJBUtil.stringToNum(protocolId));
            eventassocB.getEventAssocDetails();
            protocolName = eventassocB.getName();
            
            //JM: 11FEB2011, D-FIN9
            //calStatus = eventassocB.getStatus();            
            calStatus = statDao.getCodeSubtype(EJBUtil.stringToNum(eventassocB.getStatCode()));
        }

        ArrayList visitIds = null;
        ArrayList visitNames = null;
        ArrayList visitDisplacements = null;
        String visitId = null;
        String visitName = null;
        
        ProtVisitJB protVisitB = new ProtVisitJB();
        ProtVisitDao visitdao = protVisitB.getProtocolVisits(EJBUtil.stringToNum(protocolId));
        visitIds = visitdao.getVisit_ids();
        visitNames = visitdao.getNames();
        visitDisplacements = visitdao.getDisplacements();

        int noOfEvents = eventIds == null ? 0 : eventIds.size();
        if (protocolName == null) { protocolName = ""; }
        int noOfVisits = visitIds == null ? 0 : visitIds.size();
        
        
        jsObj.put("error", errorCode);
        jsObj.put("errorMsg", "");
        jsObj.put("noOfEvents", noOfEvents);
        jsObj.put("covMode", covMode);
        
        SchCodeDao schDao = new SchCodeDao();
        schDao.getCodeValues("coverage_type");
        ArrayList covSubTypeList = schDao.getCSubType();
        ArrayList covDescList = schDao.getCDesc();
        ArrayList covPkList = schDao.getCId();
        ArrayList<String> covCodeHide=schDao.getCodeHide();
        StringBuffer sb1 = new StringBuffer();
        for (int iX=0; iX<covSubTypeList.size(); iX++) {
            String covSub1 = ((String)covSubTypeList.get(iX)).replaceAll("'", "\\\\'");
            String covDesc1 = ((String)covDescList.get(iX)).replaceAll("'", "\\\\'");
            String covCodeIsHidden =covCodeHide.get(iX);
            if (covSub1 == null || covDesc1 == null) { continue; }
            if(covCodeIsHidden.equalsIgnoreCase("Y")){
            	sb1.append("<b>").append(covSub1).append("</b>").append("=").append(covDesc1).append(" ["+LC.L_Hidden+"]").append("<br/>");
			}else{
				sb1.append("<b>").append(covSub1).append("</b>").append("=").append(covDesc1).append("<br/>");
			}
        }
        String covTypeLegend = sb1.toString();
        jsObj.put("covTypeLegend", covTypeLegend);
        HashMap covMap = new HashMap();
        for (int iX=0; iX<covPkList.size(); iX++) {
        	String covCodeIsHidden =covCodeHide.get(iX);
            if (covCodeIsHidden.equalsIgnoreCase("Y")) { continue; }
            covMap.put(""+covPkList.get(iX), covSubTypeList.get(iX));
        }
        JSONArray covSubTypes = new JSONArray();
        for (int iX=0; iX<covSubTypeList.size(); iX++) {
        	String covCodeIsHidden =covCodeHide.get(iX);
            if (covCodeIsHidden.equalsIgnoreCase("Y")) { continue; }
            JSONObject jsObj1 = new JSONObject();
            jsObj1.put("key", covSubTypeList.get(iX));
            covSubTypes.put(jsObj1);
        }
        jsObj.put("covSubTypes", covSubTypes);
        JSONArray covDescs = new JSONArray();
        for (int iX=0; iX<covDescList.size(); iX++) {
        	String covCodeIsHidden =covCodeHide.get(iX);
            if (covCodeIsHidden.equalsIgnoreCase("Y")) { continue; }
            JSONObject jsObj1 = new JSONObject();
            jsObj1.put("key", covDescList.get(iX));
            covDescs.put(jsObj1);
        }
        jsObj.put("covDescs", covDescs);
        JSONArray covPks = new JSONArray();
        for (int iX=0; iX<covPkList.size(); iX++) {
        	String covCodeIsHidden =covCodeHide.get(iX);
            if (covCodeIsHidden.equalsIgnoreCase("Y")) { continue; }
            JSONObject jsObj1 = new JSONObject();
            jsObj1.put("key", covPkList.get(iX));
            covPks.put(jsObj1);
        }
        jsObj.put("covPks", covPks);
        
        JSONArray jsEvents = new JSONArray();
        for (int iX=0; iX<eventIds.size(); iX++) {
            JSONObject jsObj1 = new JSONObject();
            jsObj1.put(String.valueOf(eventIds.get(iX)), eventNames.get(iX));
            jsEvents.put(jsObj1);
        }
        jsObj.put("events", jsEvents);
        jsObj.put("protocolName", protocolName.replaceAll("<", "&lt;").replaceAll(">", "&gt;"));
        jsObj.put("calStatus", calStatus);
        jsObj.put("noOfVisits", noOfVisits);
        
        JSONArray jsColArray = new JSONArray();
        {
            JSONObject jsObjTemp1 = new JSONObject();
            jsObjTemp1.put("key", "event");
            jsObjTemp1.put("label", LC.L_Event);
            jsColArray.put(jsObjTemp1);
            
            if (StringUtil.isAccessibleFor(finDetRight, 'V')){
	            jsObjTemp1 = new JSONObject();
	            jsObjTemp1.put("key", "eventCPT");
	            jsObjTemp1.put("label", LC.L_Cpt_Code);
	            jsColArray.put(jsObjTemp1);
            }
            if (isExport){
	            jsObjTemp1 = new JSONObject();
	            jsObjTemp1.put("key", "eventAddCode");
	            jsObjTemp1.put("label", LC.L_Addl_Codes);
	            jsObjTemp1.put("hidden", "true");
	            jsColArray.put(jsObjTemp1);
            }

        	jsObjTemp1 = new JSONObject();
            jsObjTemp1.put("key", "eventCoverage");
            jsObjTemp1.put("label", LC.L_Coverage_Type);
            jsObjTemp1.put("hidden", "true");
            jsObjTemp1.put("hideExport", "true");
            jsColArray.put(jsObjTemp1);
            
            JSONObject jsObjTemp3 = new JSONObject();
            if (!"V".equals(covMode)) {
                jsObjTemp3.put("key", "selectAll");
                jsObjTemp3.put("label", LC.L_ApplyAll_Vsts);
                jsColArray.put(jsObjTemp3);
            }
            JSONObject jsObjTemp2 = new JSONObject();
            jsObjTemp2.put("key", "eventId");
            jsObjTemp2.put("label", "Event ID");
            jsColArray.put(jsObjTemp2);
        }
        JSONArray jsDisplacements = new JSONArray();
        for (int iX=0; iX<noOfVisits; iX++) {
            JSONObject jsObj1 = new JSONObject();
            jsObj1.put("key", "v"+String.valueOf(visitIds.get(iX)));
            jsObj1.put("value", visitDisplacements.get(iX));
            jsDisplacements.put(jsObj1);
            JSONObject jsObjTemp = new JSONObject();
            jsObjTemp.put("key", "v"+(visitIds.get(iX)));
            jsObjTemp.put("label", visitNames.get(iX));
            jsObjTemp.put("seq", "v"+(iX+1));
            jsColArray.put(jsObjTemp);
        }
        {
        	JSONObject jsObjTemp1 = new JSONObject();
            jsObjTemp1.put("key", "eventCovNotes");
            jsObjTemp1.put("label", LC.L_Coverage_Notes);
            jsColArray.put(jsObjTemp1);
        }
        jsObj.put("displacements", jsDisplacements);
        jsObj.put("colArray", jsColArray);
        
        ArrayList dataList = new ArrayList();
        ArrayList covList = new ArrayList(); // List of coverage types for performance reasons
        ArrayList covNotesListOut = new ArrayList(); // List of coverage notes for performance reasons
        JSONObject jsCovNotesHash = new JSONObject();
        for (int iX=0; iX<eventIds.size(); iX++) {
            if (((Integer)eventIds.get(iX)).intValue() < 1) { continue; }
            String covSub2 = null;
            if (covFkCodelst.get(iX) != null) { covSub2 = (String)covMap.get((String)covFkCodelst.get(iX)); }
            if (covSub2 == null || covSub2.length() == 0) { covSub2 = "<b>X</b>"; }
            if (!"<b>X</b>".equals(covSub2)) {
                HashMap map1 = new HashMap();
                map1.put("eventId", eventIds.get(iX));
                map1.put("v"+eventVisitIds.get(iX), covSub2);
                covList.add(map1);
            }
            String covNotes = (String)covNotesListIn.get(iX);
            if (covNotes != null && covNotes.trim().length() > 0) {
                HashMap map1 = new HashMap();
                map1.put("eventId", eventIds.get(iX));
                map1.put("v"+eventVisitIds.get(iX), "Y");
                covNotesListOut.add(map1);
                covNotes = covNotes.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
                jsCovNotesHash.put(eventIds.get(iX)+"v"+eventVisitIds.get(iX), covNotes);
            }
            if (covNotes == null) { covNotes = ""; } 
            if (dataList.size() == 0) {
                HashMap map1 = new HashMap();
                map1.put("event", (String)eventNames.get(iX));
                map1.put("eventId", eventIds.get(iX));

                String cptCode = "&nbsp;";
                if (StringUtil.isAccessibleFor(finDetRight, 'V')){
                	cptCode = (eventCPTs.get(iX)== null)?"&nbsp;":(String)eventCPTs.get(iX);
                }
                map1.put("eventCPT", cptCode);

                String addCode = "&nbsp;";
                if (isExport){
                	if (StringUtil.isAccessibleFor(finDetRight, 'V')){
                		addCode = (eventAddCodes.get(iX)== null)?"&nbsp;":(String)eventAddCodes.get(iX);
                	}
                }
                map1.put("eventAddCode", addCode);
                map1.put("eventCoverage", (covFkCodelst.get(iX)== null)?"":(String)covMap.get((String)covFkCodelst.get(iX)));
               	map1.put("eventCovNotes", covNotes);
                map1.put("v"+eventVisitIds.get(iX), covSub2);
                map1.put("notes_v"+eventVisitIds.get(iX), covNotes);
                map1.put("intersectId"+eventVisitIds.get(iX), eventIntersectIds.get(iX));
                dataList.add(map1);
            } else {
                HashMap map1 = (HashMap)dataList.get(dataList.size()-1);
                if (((Integer)map1.get("eventId")).intValue() == ((Integer)eventIds.get(iX)).intValue()) {
                    map1.put("v"+eventVisitIds.get(iX), covSub2);
                    map1.put("notes_v"+eventVisitIds.get(iX), covNotes);
                    map1.put("intersectId"+eventVisitIds.get(iX), eventIntersectIds.get(iX));
                    dataList.set(dataList.size()-1, map1);
                } else {
                    HashMap map2 = new HashMap();
                    map2.put("event", (String)eventNames.get(iX));
                    map2.put("eventId", eventIds.get(iX));

                    String cptCode = "&nbsp;";
                    if (StringUtil.isAccessibleFor(finDetRight, 'V')){
                    	cptCode = (eventCPTs.get(iX)== null)?"&nbsp;":(String)eventCPTs.get(iX);
                    }
                    map2.put("eventCPT", cptCode);

                    String addCode = "&nbsp;";
                    if (isExport){
                    	if (StringUtil.isAccessibleFor(finDetRight, 'V')){
                    		addCode = (eventAddCodes.get(iX)== null)?"&nbsp;":(String)eventAddCodes.get(iX);
                    	}
                    }
                    map2.put("eventAddCode", addCode);                   
                    map2.put("eventCoverage", (covFkCodelst.get(iX)== null)?"":(String)covMap.get((String)covFkCodelst.get(iX)));
                   	map2.put("eventCovNotes", covNotes);
                    map2.put("v"+eventVisitIds.get(iX), covSub2);
                    map2.put("notes_v"+eventVisitIds.get(iX), covNotes);
                    map2.put("intersectId"+eventVisitIds.get(iX), eventIntersectIds.get(iX));
                    dataList.add(map2);
                }
            }
        }
        
        dataList = eventassocB.sortListByKeyword(dataList, "event");
        JSONArray jsEventVisits = new JSONArray();
        for (int iX=0; iX<dataList.size(); iX++) {
            jsEventVisits.put(EJBUtil.hashMapToJSON((HashMap)dataList.get(iX)));
        }
        jsObj.put("dataArray", jsEventVisits);
        
        jsObj.put("covNotesHash", jsCovNotesHash);
        
        for (int iC=0; iC<covList.size(); iC++) {
            Integer covEvent = (Integer)((HashMap)covList.get(iC)).get("eventId");
            for (int iD=0; iD<dataList.size(); iD++) {
                Integer dataEvent = (Integer)((HashMap)dataList.get(iD)).get("eventId");
                if (covEvent.equals(dataEvent)) {
                    HashMap map1 = (HashMap)covList.get(iC);
                    map1.put("eventIndex", new Integer(iD));
                    break;
                }
            }
        }
        JSONArray jsCovArray = new JSONArray();
        for (int iX=0; iX<covList.size(); iX++) {
            jsCovArray.put(EJBUtil.hashMapToJSON((HashMap)covList.get(iX)));
        }
        jsObj.put("coverageArray", jsCovArray);
        
        for (int iC=0; iC<covNotesListOut.size(); iC++) {
            Integer covEvent = (Integer)((HashMap)covNotesListOut.get(iC)).get("eventId");
            for (int iD=0; iD<dataList.size(); iD++) {
                Integer dataEvent = (Integer)((HashMap)dataList.get(iD)).get("eventId");
                if (covEvent.equals(dataEvent)) {
                    HashMap map1 = (HashMap)covNotesListOut.get(iC);
                    map1.put("eventIndex", new Integer(iD));
                    break;
                }
            }
        }
        JSONArray jsCovNotesArray = new JSONArray();
        for (int iX=0; iX<covNotesListOut.size(); iX++) {
            jsCovNotesArray.put(EJBUtil.hashMapToJSON((HashMap)covNotesListOut.get(iX)));
        }
        jsObj.put("coverageNotes", jsCovNotesArray);
        
        return jsObj.toString();
    }
    
    public String fetchCoverageJSON(String protocolId, String calledFrom, int finDetRight, String accountId,int initEventSize,int limitEventSize,int initVisitSize,int limitVisitSize,String eventName) 
    throws JSONException {
    	boolean isExport = (accountId == null)?true:false;

        JSONObject jsObj = new JSONObject();
        StringBuffer sb = new StringBuffer();
        boolean isLibCal = false;
        boolean isStudyCal = false;
        String protocolName = null;
        String calStatus = null;
        int totalEventRecords=0;
        int totalVisitRecords=0;
        int errorCode = 0;
        ArrayList eventIntersectIds = null, eventCPTs = null, eventAddCodes = null;
        ArrayList eventIds = null, eventNames = null;
        ArrayList eventVisitIds = null;
        ArrayList covFkCodelst = null;
        ArrayList covNotesListIn = null;
        
        if ("P".equals(calledFrom) || "L".equals(calledFrom) ) {
            isLibCal = true;
        } else if ("S".equals(calledFrom)) {
            isStudyCal = true;
        } else {
            jsObj.put("error", new Integer(-3));
            jsObj.put("errorMsg", "Calendar Type is missing.");
            return jsObj.toString();
        }
        
        int accId = accountId == null ? 0 : EJBUtil.stringToNum((String) accountId);
        CommonJB commonB = new CommonJB();
        SettingsDao setDao = commonB.getSettingsInstance();
        setDao.getSettingsValue("COVERAGE_ANALYSIS_MODE", accId, 0, 0);
        ArrayList setValArray = setDao.getSettingValue();
        String covMode = null;
        if (setValArray != null && setValArray.size() > 0) {
            covMode = (String)setValArray.get(0);
        }
        if (covMode == null || covMode.length() == 0) { covMode = "V"; } // Make view-first the default mode
        
        EventdefJB eventdefB = new EventdefJB();
        EventAssocJB eventassocB = new EventAssocJB();
        SchCodeDao statDao = new SchCodeDao();
        ArrayList visitIds = null;
        ArrayList visitNames = null;
        ArrayList visitDisplacements = null;
        
        ProtVisitJB protVisitB = new ProtVisitJB();
        ProtVisitDao visitdao = protVisitB.getProtocolVisits(EJBUtil.stringToNum(protocolId),initVisitSize,limitVisitSize);
        visitIds = visitdao.getVisit_ids();
        visitNames = visitdao.getNames();
        visitDisplacements = visitdao.getDisplacements();
        totalVisitRecords = visitdao.getTotalRecords();
        String visitids = visitIds.toString().replace("[", "").replace("]", "")
        .replace(", ", ",");
        if (isLibCal) {
            EventdefDao defdao = eventdefB.getProtSelectedAndGroupedEvents(EJBUtil.stringToNum(protocolId), isExport, finDetRight,initEventSize,limitEventSize,visitids,eventName);
            eventIntersectIds = defdao.getNew_event_ids();
            eventIds = defdao.getEvent_ids();
            eventVisitIds = defdao.getFk_visit();
            eventNames = defdao.getNames();
            eventCPTs = defdao.getCptCodes();
            eventAddCodes = defdao.getCodeDescriptions();
            covFkCodelst = defdao.getEventCoverageTypes();
            covNotesListIn = defdao.getEventCoverageNotes();
            eventdefB.setEvent_id(EJBUtil.stringToNum(protocolId));
            eventdefB.getEventdefDetails();
            protocolName = eventdefB.getName();
            totalEventRecords = defdao.getTotalRecords();
            //JM: 11FEB2011, D-FIN9
            //calStatus = eventdefB.getStatus();          
            
            calStatus = statDao.getCodeSubtype(EJBUtil.stringToNum(eventdefB.getStatCode()));

        } else if (isStudyCal) {
            EventAssocDao assocdao = eventassocB.getProtSelectedAndGroupedEvents(EJBUtil.stringToNum(protocolId), isExport, finDetRight,initEventSize,limitEventSize,visitids,eventName);
            eventIntersectIds = assocdao.getNew_event_ids();
            eventIds = assocdao.getEvent_ids();
            eventVisitIds = assocdao.getEventVisits();
            eventNames = assocdao.getNames();
            eventCPTs = assocdao.getCptCodes();
            eventAddCodes = assocdao.getCodeDescriptions();
            covFkCodelst = assocdao.getEventCoverageTypes();
            covNotesListIn = assocdao.getEventCoverageNotes();
            eventassocB.setEvent_id(EJBUtil.stringToNum(protocolId));
            eventassocB.getEventAssocDetails();
            protocolName = eventassocB.getName();
            totalEventRecords = assocdao.getTotalRecords();
            //JM: 11FEB2011, D-FIN9
            //calStatus = eventassocB.getStatus();            
            calStatus = statDao.getCodeSubtype(EJBUtil.stringToNum(eventassocB.getStatCode()));
        }

        int noOfEvents = eventIds == null ? 0 : eventIds.size();
        if (protocolName == null) { protocolName = ""; }
        int noOfVisits = visitIds == null ? 0 : visitIds.size();
        
        
        jsObj.put("error", errorCode);
        jsObj.put("errorMsg", "");
        jsObj.put("noOfEvents", noOfEvents);
        jsObj.put("covMode", covMode);
        
        SchCodeDao schDao = new SchCodeDao();
        schDao.getCodeValues("coverage_type");
        ArrayList covSubTypeList = schDao.getCSubType();
        ArrayList covDescList = schDao.getCDesc();
        ArrayList covPkList = schDao.getCId();
        ArrayList<String> covCodeHide=schDao.getCodeHide();
        StringBuffer sb1 = new StringBuffer();
        for (int iX=0; iX<covSubTypeList.size(); iX++) {
            String covSub1 = ((String)covSubTypeList.get(iX)).replaceAll("'", "\\\\'");
            String covDesc1 = ((String)covDescList.get(iX)).replaceAll("'", "\\\\'");
            String covCodeIsHidden =covCodeHide.get(iX);
            if (covSub1 == null || covDesc1 == null) { continue; }
            if(covCodeIsHidden.equalsIgnoreCase("Y")){
            	sb1.append("<b>").append(covSub1).append("</b>").append("=").append(covDesc1).append(" ["+LC.L_Hidden+"]").append("<br/>");
			}else{
				sb1.append("<b>").append(covSub1).append("</b>").append("=").append(covDesc1).append("<br/>");
			}
        }
        String covTypeLegend = sb1.toString();
        jsObj.put("covTypeLegend", covTypeLegend);
        HashMap covMap = new HashMap();
        for (int iX=0; iX<covPkList.size(); iX++) {
        	String covCodeIsHidden =covCodeHide.get(iX);
            if (covCodeIsHidden.equalsIgnoreCase("Y")) { continue; }
            covMap.put(""+covPkList.get(iX), covSubTypeList.get(iX));
        }
        JSONArray covSubTypes = new JSONArray();
        for (int iX=0; iX<covSubTypeList.size(); iX++) {
        	String covCodeIsHidden =covCodeHide.get(iX);
            if (covCodeIsHidden.equalsIgnoreCase("Y")) { continue; }
            JSONObject jsObj1 = new JSONObject();
            jsObj1.put("key", covSubTypeList.get(iX));
            covSubTypes.put(jsObj1);
        }
        jsObj.put("covSubTypes", covSubTypes);
        JSONArray covDescs = new JSONArray();
        for (int iX=0; iX<covDescList.size(); iX++) {
        	String covCodeIsHidden =covCodeHide.get(iX);
            if (covCodeIsHidden.equalsIgnoreCase("Y")) { continue; }
            JSONObject jsObj1 = new JSONObject();
            jsObj1.put("key", covDescList.get(iX));
            covDescs.put(jsObj1);
        }
        jsObj.put("covDescs", covDescs);
        JSONArray covPks = new JSONArray();
        for (int iX=0; iX<covPkList.size(); iX++) {
        	String covCodeIsHidden =covCodeHide.get(iX);
            if (covCodeIsHidden.equalsIgnoreCase("Y")) { continue; }
            JSONObject jsObj1 = new JSONObject();
            jsObj1.put("key", covPkList.get(iX));
            covPks.put(jsObj1);
        }
        jsObj.put("covPks", covPks);
        
        JSONArray jsEvents = new JSONArray();
        for (int iX=0; iX<eventIds.size(); iX++) {
            JSONObject jsObj1 = new JSONObject();
            jsObj1.put(String.valueOf(eventIds.get(iX)), eventNames.get(iX));
            jsEvents.put(jsObj1);
        }
        jsObj.put("events", jsEvents);
        jsObj.put("protocolName", protocolName.replaceAll("<", "&lt;").replaceAll(">", "&gt;"));
        jsObj.put("calStatus", calStatus);
        jsObj.put("noOfVisits", noOfVisits);
        jsObj.put("totalEventRecords", totalEventRecords);
        jsObj.put("totalVisitRecords", totalVisitRecords);
        
        
        JSONArray jsColArray = new JSONArray();
        {
            JSONObject jsObjTemp1 = new JSONObject();
            jsObjTemp1.put("key", "event");
            jsObjTemp1.put("label", LC.L_Event);
            jsColArray.put(jsObjTemp1);
            
            if (StringUtil.isAccessibleFor(finDetRight, 'V')){
	            jsObjTemp1 = new JSONObject();
	            jsObjTemp1.put("key", "eventCPT");
	            jsObjTemp1.put("label", LC.L_Cpt_Code);
	            jsColArray.put(jsObjTemp1);
            }
            if (isExport){
	            jsObjTemp1 = new JSONObject();
	            jsObjTemp1.put("key", "eventAddCode");
	            jsObjTemp1.put("label", LC.L_Addl_Codes);
	            jsObjTemp1.put("hidden", "true");
	            jsColArray.put(jsObjTemp1);
            }

        	jsObjTemp1 = new JSONObject();
            jsObjTemp1.put("key", "eventCoverage");
            jsObjTemp1.put("label", LC.L_Coverage_Type);
            jsObjTemp1.put("hidden", "true");
            jsObjTemp1.put("hideExport", "true");
            jsColArray.put(jsObjTemp1);
            
            JSONObject jsObjTemp3 = new JSONObject();
            if (!"V".equals(covMode)) {
                jsObjTemp3.put("key", "selectAll");
                jsObjTemp3.put("label", LC.L_ApplyAll_Vsts);
                jsColArray.put(jsObjTemp3);
            }
            JSONObject jsObjTemp2 = new JSONObject();
            jsObjTemp2.put("key", "eventId");
            jsObjTemp2.put("label", "Event ID");
            jsColArray.put(jsObjTemp2);
        }
        JSONArray jsDisplacements = new JSONArray();
        for (int iX=0; iX<noOfVisits; iX++) {
            JSONObject jsObj1 = new JSONObject();
            jsObj1.put("key", "v"+String.valueOf(visitIds.get(iX)));
            jsObj1.put("value", visitDisplacements.get(iX));
            jsDisplacements.put(jsObj1);
            JSONObject jsObjTemp = new JSONObject();
            jsObjTemp.put("key", "v"+(visitIds.get(iX)));
            jsObjTemp.put("label", visitNames.get(iX));
            jsObjTemp.put("seq", "v"+(iX+1));
            jsColArray.put(jsObjTemp);
        }
        {
        	JSONObject jsObjTemp1 = new JSONObject();
            jsObjTemp1.put("key", "eventCovNotes");
            jsObjTemp1.put("label", LC.L_Coverage_Notes);
            jsColArray.put(jsObjTemp1);
        }
        jsObj.put("displacements", jsDisplacements);
        jsObj.put("colArray", jsColArray);
        
        ArrayList dataList = new ArrayList();
        ArrayList covList = new ArrayList(); // List of coverage types for performance reasons
        ArrayList covNotesListOut = new ArrayList(); // List of coverage notes for performance reasons
        JSONObject jsCovNotesHash = new JSONObject();
        for (int iX=0; iX<eventIds.size(); iX++) {
            if (((Integer)eventIds.get(iX)).intValue() < 1) { continue; }
            String covSub2 = null;
            if (covFkCodelst.get(iX) != null) { covSub2 = (String)covMap.get((String)covFkCodelst.get(iX)); }
            if (covSub2 == null || covSub2.length() == 0) { covSub2 = "<b>X</b>"; }
            if (!"<b>X</b>".equals(covSub2)) {
                HashMap map1 = new HashMap();
                map1.put("eventId", eventIds.get(iX));
                map1.put("v"+eventVisitIds.get(iX), covSub2);
                covList.add(map1);
            }
            String covNotes = (String)covNotesListIn.get(iX);
            if (covNotes != null && covNotes.trim().length() > 0) {
                HashMap map1 = new HashMap();
                map1.put("eventId", eventIds.get(iX));
                map1.put("v"+eventVisitIds.get(iX), "Y");
                covNotesListOut.add(map1);
                covNotes = covNotes.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
                jsCovNotesHash.put(eventIds.get(iX)+"v"+eventVisitIds.get(iX), covNotes);
            }
            if (covNotes == null) { covNotes = ""; } 
            if (dataList.size() == 0) {
                HashMap map1 = new HashMap();
                map1.put("event", (String)eventNames.get(iX));
                map1.put("eventId", eventIds.get(iX));

                String cptCode = "&nbsp;";
                if (StringUtil.isAccessibleFor(finDetRight, 'V')){
                	cptCode = (eventCPTs.get(iX)== null)?"&nbsp;":(String)eventCPTs.get(iX);
                }
                map1.put("eventCPT", cptCode);

                String addCode = "&nbsp;";
                if (isExport){
                	if (StringUtil.isAccessibleFor(finDetRight, 'V')){
                		addCode = (eventAddCodes.get(iX)== null)?"&nbsp;":(String)eventAddCodes.get(iX);
                	}
                }
                map1.put("eventAddCode", addCode);
                map1.put("eventCoverage", (covFkCodelst.get(iX)== null)?"":(String)covMap.get((String)covFkCodelst.get(iX)));
               	map1.put("eventCovNotes", covNotes);
                map1.put("v"+eventVisitIds.get(iX), covSub2);
                map1.put("notes_v"+eventVisitIds.get(iX), covNotes);
                map1.put("intersectId"+eventVisitIds.get(iX), eventIntersectIds.get(iX));
                dataList.add(map1);
            } else {
                HashMap map1 = (HashMap)dataList.get(dataList.size()-1);
                if (((Integer)map1.get("eventId")).intValue() == ((Integer)eventIds.get(iX)).intValue()) {
                    map1.put("v"+eventVisitIds.get(iX), covSub2);
                    map1.put("notes_v"+eventVisitIds.get(iX), covNotes);
                    map1.put("intersectId"+eventVisitIds.get(iX), eventIntersectIds.get(iX));
                    dataList.set(dataList.size()-1, map1);
                } else {
                    HashMap map2 = new HashMap();
                    map2.put("event", (String)eventNames.get(iX));
                    map2.put("eventId", eventIds.get(iX));

                    String cptCode = "&nbsp;";
                    if (StringUtil.isAccessibleFor(finDetRight, 'V')){
                    	cptCode = (eventCPTs.get(iX)== null)?"&nbsp;":(String)eventCPTs.get(iX);
                    }
                    map2.put("eventCPT", cptCode);

                    String addCode = "&nbsp;";
                    if (isExport){
                    	if (StringUtil.isAccessibleFor(finDetRight, 'V')){
                    		addCode = (eventAddCodes.get(iX)== null)?"&nbsp;":(String)eventAddCodes.get(iX);
                    	}
                    }
                    map2.put("eventAddCode", addCode);                   
                    map2.put("eventCoverage", (covFkCodelst.get(iX)== null)?"":(String)covMap.get((String)covFkCodelst.get(iX)));
                   	map2.put("eventCovNotes", covNotes);
                    map2.put("v"+eventVisitIds.get(iX), covSub2);
                    map2.put("notes_v"+eventVisitIds.get(iX), covNotes);
                    map2.put("intersectId"+eventVisitIds.get(iX), eventIntersectIds.get(iX));
                    dataList.add(map2);
                }
            }
        }
        
        dataList = eventassocB.sortListByKeyword(dataList, "event");
        JSONArray jsEventVisits = new JSONArray();
        for (int iX=0; iX<dataList.size(); iX++) {
            jsEventVisits.put(EJBUtil.hashMapToJSON((HashMap)dataList.get(iX)));
        }
        jsObj.put("dataArray", jsEventVisits);
        
        jsObj.put("covNotesHash", jsCovNotesHash);
        
        for (int iC=0; iC<covList.size(); iC++) {
            Integer covEvent = (Integer)((HashMap)covList.get(iC)).get("eventId");
            for (int iD=0; iD<dataList.size(); iD++) {
                Integer dataEvent = (Integer)((HashMap)dataList.get(iD)).get("eventId");
                if (covEvent.equals(dataEvent)) {
                    HashMap map1 = (HashMap)covList.get(iC);
                    map1.put("eventIndex", new Integer(iD));
                    break;
                }
            }
        }
        JSONArray jsCovArray = new JSONArray();
        for (int iX=0; iX<covList.size(); iX++) {
            jsCovArray.put(EJBUtil.hashMapToJSON((HashMap)covList.get(iX)));
        }
        jsObj.put("coverageArray", jsCovArray);
        
        for (int iC=0; iC<covNotesListOut.size(); iC++) {
            Integer covEvent = (Integer)((HashMap)covNotesListOut.get(iC)).get("eventId");
            for (int iD=0; iD<dataList.size(); iD++) {
                Integer dataEvent = (Integer)((HashMap)dataList.get(iD)).get("eventId");
                if (covEvent.equals(dataEvent)) {
                    HashMap map1 = (HashMap)covNotesListOut.get(iC);
                    map1.put("eventIndex", new Integer(iD));
                    break;
                }
            }
        }
        JSONArray jsCovNotesArray = new JSONArray();
        for (int iX=0; iX<covNotesListOut.size(); iX++) {
            jsCovNotesArray.put(EJBUtil.hashMapToJSON((HashMap)covNotesListOut.get(iX)));
        }
        jsObj.put("coverageNotes", jsCovNotesArray);
        
        return jsObj.toString();
    }
    
    public String fetchCoverageJSONExportToFlag(String protocolId, String calledFrom, int finDetRight) throws JSONException {
        return fetchCoverageJSONExportToFlag(protocolId, calledFrom, finDetRight, null);
    }

    public String fetchCoverageJSONExportToFlag(String protocolId, String calledFrom, int finDetRight, String accountId) throws JSONException {

    	boolean isExport = (accountId == null)?true:false;

        JSONObject jsObj = new JSONObject();
        StringBuffer sb = new StringBuffer();
        boolean isLibCal = false;
        boolean isStudyCal = false;
        String protocolName = null;
        String calStatus = null;
        int errorCode = 0;
        ArrayList eventIntersectIds = null, eventCPTs = null;
        ArrayList eventIds = null, eventNames = null;
        ArrayList eventVisitIds = null;
        ArrayList covFkCodelst = null;
        ArrayList covNotesListIn = null;
        ArrayList servicecodes = null;
        ArrayList prscodes = null;
        if ("P".equals(calledFrom) || "L".equals(calledFrom) ) {
            isLibCal = true;
        } else if ("S".equals(calledFrom)) {
            isStudyCal = true;
        } else {
            jsObj.put("error", new Integer(-3));
            jsObj.put("errorMsg", "Calendar Type is missing.");
            return jsObj.toString();
        }
        
        int accId = accountId == null ? 0 : EJBUtil.stringToNum((String) accountId);
        CommonJB commonB = new CommonJB();
        SettingsDao setDao = commonB.getSettingsInstance();
        setDao.getSettingsValue("COVERAGE_ANALYSIS_MODE", accId, 0, 0);
        ArrayList setValArray = setDao.getSettingValue();
        String covMode = null;
        if (setValArray != null && setValArray.size() > 0) {
            covMode = (String)setValArray.get(0);
        }
        if (covMode == null || covMode.length() == 0) { covMode = "V"; } // Make view-first the default mode
        
        EventdefJB eventdefB = new EventdefJB();
        EventAssocJB eventassocB = new EventAssocJB();
        SchCodeDao statDao = new SchCodeDao();
        if (isLibCal) {
            EventdefDao defdao = eventdefB.getProtSelectedAndGroupedEvents(EJBUtil.stringToNum(protocolId), isExport, finDetRight);
            eventIntersectIds = defdao.getNew_event_ids();
            eventIds = defdao.getEvent_ids();
            eventVisitIds = defdao.getFk_visit();
            eventNames = defdao.getNames();
            eventCPTs = defdao.getCptCodes();
            covFkCodelst = defdao.getEventCoverageTypes();
            covNotesListIn = defdao.getEventCoverageNotes();
            prscodes = defdao.getPrscodes();
            servicecodes = defdao.getServicecodes();
            eventdefB.setEvent_id(EJBUtil.stringToNum(protocolId));
            eventdefB.getEventdefDetails();
            protocolName = eventdefB.getName();
            
            //JM: 11FEB2011, D-FIN9
            //calStatus = eventdefB.getStatus();          
            
            calStatus = statDao.getCodeSubtype(EJBUtil.stringToNum(eventdefB.getStatCode()));

        } else if (isStudyCal) {
        	System.out.println("fine");
            EventAssocDao assocdao = eventassocB.getProtSelectedAndGroupedEvents(EJBUtil.stringToNum(protocolId), isExport, finDetRight);
           eventIntersectIds = assocdao.getNew_event_ids();
           eventIds = assocdao.getEvent_ids();
           eventVisitIds = assocdao.getEventVisits();
           eventNames = assocdao.getNames();
           eventCPTs = assocdao.getCptCodes();
           covFkCodelst = assocdao.getEventCoverageTypes();
           covNotesListIn = assocdao.getEventCoverageNotes();
           prscodes = assocdao.getPrscodes();
           servicecodes = assocdao.getServicecodes();
           eventassocB.setEvent_id(EJBUtil.stringToNum(protocolId));
           eventassocB.getEventAssocDetails();
           protocolName = eventassocB.getName();
          
//            //JM: 11FEB2011, D-FIN9
//            //calStatus = eventassocB.getStatus();            
           calStatus = statDao.getCodeSubtype(EJBUtil.stringToNum(eventassocB.getStatCode()));
        }

        ArrayList visitIds = null;
        ArrayList visitNames = null;
        ArrayList visitDisplacements = null;
        String visitId = null;
        String visitName = null;
        ArrayList winBeforeNumber = null;
        ArrayList winBeforeUnit = null;
        ArrayList winAfterNumber = null;
        ArrayList winAfterUnit = null;
        
        ProtVisitJB protVisitB = new ProtVisitJB();
        ProtVisitDao visitdao = protVisitB.getProtocolVisits(EJBUtil.stringToNum(protocolId));
        visitIds = visitdao.getVisit_ids();
        visitNames = visitdao.getNames();
        visitDisplacements = visitdao.getDisplacements();
        winBeforeNumber = visitdao.getWinBeforeNumber();
        winBeforeUnit = visitdao.getWinBeforeUnit();
        winAfterNumber = visitdao.getWinAfterNumber();
        winAfterUnit = visitdao.getWinAfterUnit();
        int noOfEvents = eventIds == null ? 0 : eventIds.size();
        if (protocolName == null) { protocolName = ""; }
        int noOfVisits = visitIds == null ? 0 : visitIds.size();
        
        
        jsObj.put("error", errorCode);
        jsObj.put("errorMsg", "");
        jsObj.put("noOfEvents", noOfEvents);
        jsObj.put("covMode", covMode);
        
        SchCodeDao schDao = new SchCodeDao();
        schDao.getCodeValues("coverage_type");
        ArrayList covSubTypeList = schDao.getCSubType();
        ArrayList covDescList = schDao.getCDesc();
        ArrayList covPkList = schDao.getCId();
        ArrayList<String> covCodeHide=schDao.getCodeHide();
        StringBuffer sb1 = new StringBuffer();
        for (int iX=0; iX<covSubTypeList.size(); iX++) {
            String covSub1 = ((String)covSubTypeList.get(iX)).replaceAll("'", "\\\\'");
            String covDesc1 = ((String)covDescList.get(iX)).replaceAll("'", "\\\\'");
            String covCodeIsHidden =covCodeHide.get(iX);
            if (covSub1 == null || covDesc1 == null) { continue; }
            if(covCodeIsHidden.equalsIgnoreCase("Y")){
            	sb1.append("<b>").append(covSub1).append("</b>").append("=").append(covDesc1).append(" ["+LC.L_Hidden+"]").append("<br/>");
			}else{
				sb1.append("<b>").append(covSub1).append("</b>").append("=").append(covDesc1).append("<br/>");
			}
        }
        String covTypeLegend = sb1.toString();
        jsObj.put("covTypeLegend", covTypeLegend);
        HashMap covMap = new HashMap();
        for (int iX=0; iX<covPkList.size(); iX++) {
        	String covCodeIsHidden =covCodeHide.get(iX);
            if (covCodeIsHidden.equalsIgnoreCase("Y")) { continue; }
            covMap.put(""+covPkList.get(iX), covSubTypeList.get(iX));
        }
        JSONArray covSubTypes = new JSONArray();
        for (int iX=0; iX<covSubTypeList.size(); iX++) {
        	String covCodeIsHidden =covCodeHide.get(iX);
            if (covCodeIsHidden.equalsIgnoreCase("Y")) { continue; }
            JSONObject jsObj1 = new JSONObject();
            jsObj1.put("key", covSubTypeList.get(iX));
            covSubTypes.put(jsObj1);
        }
        jsObj.put("covSubTypes", covSubTypes);
        JSONArray covDescs = new JSONArray();
        for (int iX=0; iX<covDescList.size(); iX++) {
        	String covCodeIsHidden =covCodeHide.get(iX);
            if (covCodeIsHidden.equalsIgnoreCase("Y")) { continue; }
            JSONObject jsObj1 = new JSONObject();
            jsObj1.put("key", covDescList.get(iX));
            covDescs.put(jsObj1);
        }
        jsObj.put("covDescs", covDescs);
        JSONArray covPks = new JSONArray();
        for (int iX=0; iX<covPkList.size(); iX++) {
        	String covCodeIsHidden =covCodeHide.get(iX);
            if (covCodeIsHidden.equalsIgnoreCase("Y")) { continue; }
            JSONObject jsObj1 = new JSONObject();
            jsObj1.put("key", covPkList.get(iX));
            covPks.put(jsObj1);
        }
        jsObj.put("covPks", covPks);
        
        JSONArray jsEvents = new JSONArray();
        for (int iX=0; iX<eventIds.size(); iX++) {
            JSONObject jsObj1 = new JSONObject();
            jsObj1.put(String.valueOf(eventIds.get(iX)), eventNames.get(iX));
            jsEvents.put(jsObj1);
        }
        jsObj.put("events", jsEvents);
        jsObj.put("protocolName", protocolName.replaceAll("<", "&lt;").replaceAll(">", "&gt;"));
        jsObj.put("calStatus", calStatus);
        jsObj.put("noOfVisits", noOfVisits);
        
        JSONArray jsColArray = new JSONArray();
        {
            JSONObject jsObjTemp1 = new JSONObject();
            jsObjTemp1.put("key", "event");
            jsObjTemp1.put("label", LC.L_Event);
            jsColArray.put(jsObjTemp1);
            
            if (StringUtil.isAccessibleFor(finDetRight, 'V')){
	            jsObjTemp1 = new JSONObject();
	            jsObjTemp1.put("key", "eventCPT");
	            jsObjTemp1.put("label", LC.L_Cpt_Code);
	            jsColArray.put(jsObjTemp1);
            }
            if (isExport){
            
            	
            
	            jsObjTemp1 = new JSONObject();
	            jsObjTemp1.put("key", "eventPRSCode");
	            jsObjTemp1.put("label", LC.L_PRS_Codes);
	            jsObjTemp1.put("hidden", "true");
	            jsColArray.put(jsObjTemp1);
	            
	            jsObjTemp1 = new JSONObject();
	            jsObjTemp1.put("key", "eventServiceCode");
	            jsObjTemp1.put("label", LC.L_Service_Codes);
	            jsObjTemp1.put("hidden", "true");
	            jsColArray.put(jsObjTemp1);   
            }

        	jsObjTemp1 = new JSONObject();
            jsObjTemp1.put("key", "eventCoverage");
            jsObjTemp1.put("label", LC.L_Coverage_Type);
            jsObjTemp1.put("hidden", "true");
            jsObjTemp1.put("hideExport", "true");
            jsColArray.put(jsObjTemp1);
            
            JSONObject jsObjTemp3 = new JSONObject();
            if (!"V".equals(covMode)) {
                jsObjTemp3.put("key", "selectAll");
                jsObjTemp3.put("label", LC.L_ApplyAll_Vsts);
                jsColArray.put(jsObjTemp3);
            }
            JSONObject jsObjTemp2 = new JSONObject();
            jsObjTemp2.put("key", "eventId");
            jsObjTemp2.put("label", "Event ID");
            jsColArray.put(jsObjTemp2);
        }
        JSONArray jsDisplacements = new JSONArray();
        for (int iX=0; iX<noOfVisits; iX++) {
            JSONObject jsObj1 = new JSONObject();
            jsObj1.put("key", "v"+String.valueOf(visitIds.get(iX)));
            jsObj1.put("value", visitDisplacements.get(iX));
            jsDisplacements.put(jsObj1);
            JSONObject jsObjTemp = new JSONObject();
            jsObjTemp.put("key", "v"+(visitIds.get(iX)));
            
            
            String winBeforeUnitStr=(String) winBeforeUnit.get(iX);
            String winAfterUnitStr=(String) winAfterUnit.get(iX);
            
            
            
            if(winBeforeUnitStr==null){
            	winBeforeUnitStr = "Days";	
            }else if(winBeforeUnitStr.equalsIgnoreCase("D")){
            	winBeforeUnitStr = "Days";
            }else if(winBeforeUnitStr.equalsIgnoreCase("W")){
            	winBeforeUnitStr = "Weeks";
            }else if(winBeforeUnitStr.equalsIgnoreCase("M")){
            	winBeforeUnitStr = "Months";
            }else if(winBeforeUnitStr.equalsIgnoreCase("Y")){
            	winBeforeUnitStr = "Years";
            }
            
            
            if(winAfterUnitStr==null){
            	winAfterUnitStr = "Days";	
            }else if(winAfterUnitStr.equalsIgnoreCase("D")){
            	winAfterUnitStr = "Days";
            }else if(winAfterUnitStr.equalsIgnoreCase("W")){
            	winAfterUnitStr = "Weeks";
            }else if(winAfterUnitStr.equalsIgnoreCase("M")){
            	winAfterUnitStr = "Months";
            }else if(winAfterUnitStr.equalsIgnoreCase("Y")){
            	winAfterUnitStr = "Years";
            }
            
            
            jsObjTemp.put("label", visitNames.get(iX)+"<br/>" +  winBeforeNumber.get(iX) +"&nbsp;"+ winBeforeUnitStr+"&nbsp; Before &nbsp;" +   winAfterNumber.get(iX)+ "&nbsp;" + winAfterUnitStr+ "&nbsp;  After" );
            
            jsObjTemp.put("seq", "v"+(iX+1));
            jsColArray.put(jsObjTemp);
        }
        {
        	JSONObject jsObjTemp1 = new JSONObject();
            jsObjTemp1.put("key", "eventCovNotes");
            jsObjTemp1.put("label", LC.L_Coverage_Notes);
            jsColArray.put(jsObjTemp1);
        }
        jsObj.put("displacements", jsDisplacements);
        jsObj.put("colArray", jsColArray);
        
        ArrayList dataList = new ArrayList();
        ArrayList covList = new ArrayList(); // List of coverage types for performance reasons
        ArrayList covNotesListOut = new ArrayList(); // List of coverage notes for performance reasons
        JSONObject jsCovNotesHash = new JSONObject();
        for (int iX=0; iX<eventIds.size(); iX++) {
            if (((Integer)eventIds.get(iX)).intValue() < 1) { continue; }
            String covSub2 = null;
            if (covFkCodelst.get(iX) != null) { covSub2 = (String)covMap.get((String)covFkCodelst.get(iX)); }
            if (covSub2 == null || covSub2.length() == 0) { covSub2 = "<b>X</b>"; }
            if (!"<b>X</b>".equals(covSub2)) {
                HashMap map1 = new HashMap();
                map1.put("eventId", eventIds.get(iX));
                map1.put("v"+eventVisitIds.get(iX), covSub2);
                covList.add(map1);
            }
            String covNotes = (String)covNotesListIn.get(iX);
            if (covNotes != null && covNotes.trim().length() > 0) {
                HashMap map1 = new HashMap();
                map1.put("eventId", eventIds.get(iX));
                map1.put("v"+eventVisitIds.get(iX), "Y");
                covNotesListOut.add(map1);
                covNotes = covNotes.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
                jsCovNotesHash.put(eventIds.get(iX)+"v"+eventVisitIds.get(iX), covNotes);
            }
            if (covNotes == null) { covNotes = ""; } 
            if (dataList.size() == 0) {
                HashMap map1 = new HashMap();
                map1.put("event", (String)eventNames.get(iX));
                map1.put("eventId", eventIds.get(iX));

                String cptCode = "&nbsp;";
                if (StringUtil.isAccessibleFor(finDetRight, 'V')){
                	cptCode = (eventCPTs.get(iX)== null)?"&nbsp;":(String)eventCPTs.get(iX);
                }
                map1.put("eventCPT", cptCode);

               
                
                         
                	if(!servicecodes.isEmpty()){
                		map1.put("servicecodes", servicecodes.get(iX)==null?"&nbsp;":(String)servicecodes.get(iX));
                	}else{
                		map1.put("servicecodes","&nbsp;");
                	}
                    if(!prscodes.isEmpty()){
                    	map1.put("prscodes", prscodes.get(iX)==null?"&nbsp;":(String)prscodes.get(iX));
                    }else{
                    	map1.put("prscodes","&nbsp");
                    }
                
                map1.put("eventCoverage", (covFkCodelst.get(iX)== null)?"":(String)covMap.get((String)covFkCodelst.get(iX)));
               	map1.put("eventCovNotes", covNotes);
                map1.put("v"+eventVisitIds.get(iX), covSub2);
                map1.put("notes_v"+eventVisitIds.get(iX), covNotes);
                map1.put("intersectId"+eventVisitIds.get(iX), eventIntersectIds.get(iX));
                dataList.add(map1);
            } else {
                HashMap map1 = (HashMap)dataList.get(dataList.size()-1);
                if (((Integer)map1.get("eventId")).intValue() == ((Integer)eventIds.get(iX)).intValue()) {
                    map1.put("v"+eventVisitIds.get(iX), covSub2);
                    map1.put("notes_v"+eventVisitIds.get(iX), covNotes);
                    map1.put("intersectId"+eventVisitIds.get(iX), eventIntersectIds.get(iX));
                    dataList.set(dataList.size()-1, map1);
                } else {
                    HashMap map2 = new HashMap();
                    map2.put("event", (String)eventNames.get(iX));
                    map2.put("eventId", eventIds.get(iX));

                    String cptCode = "&nbsp;";
                    if (StringUtil.isAccessibleFor(finDetRight, 'V')){
                    	cptCode = (eventCPTs.get(iX)== null)?"&nbsp;":(String)eventCPTs.get(iX);
                    }
                    map2.put("eventCPT", cptCode);

                    
	                    
                    if(!servicecodes.isEmpty()){
                    	map2.put("servicecodes", servicecodes.get(iX)==null?"":(String)servicecodes.get(iX));
                    }else{
                    	map2.put("servicecodes","");
                    }
	                
	                
	                if(!prscodes.isEmpty()){
	                    	map2.put("prscodes", prscodes.get(iX)==null?"":(String)prscodes.get(iX));
	                }else{
	                	map2.put("prscodes","");
	                	
	                }
                    
                                       		
                    map2.put("eventCoverage", (covFkCodelst.get(iX)== null)?"":(String)covMap.get((String)covFkCodelst.get(iX)));
                   	map2.put("eventCovNotes", covNotes);
                    map2.put("v"+eventVisitIds.get(iX), covSub2);
                    map2.put("notes_v"+eventVisitIds.get(iX), covNotes);
                    map2.put("intersectId"+eventVisitIds.get(iX), eventIntersectIds.get(iX));
                    dataList.add(map2);
                }
            }
        }
        
        dataList = eventassocB.sortListByKeyword(dataList, "event");
        JSONArray jsEventVisits = new JSONArray();
        for (int iX=0; iX<dataList.size(); iX++) {
            jsEventVisits.put(EJBUtil.hashMapToJSON((HashMap)dataList.get(iX)));
        }
        jsObj.put("dataArray", jsEventVisits);
        
        jsObj.put("covNotesHash", jsCovNotesHash);
        
        for (int iC=0; iC<covList.size(); iC++) {
            Integer covEvent = (Integer)((HashMap)covList.get(iC)).get("eventId");
            for (int iD=0; iD<dataList.size(); iD++) {
                Integer dataEvent = (Integer)((HashMap)dataList.get(iD)).get("eventId");
                if (covEvent.equals(dataEvent)) {
                    HashMap map1 = (HashMap)covList.get(iC);
                    map1.put("eventIndex", new Integer(iD));
                    break;
                }
            }
        }
        JSONArray jsCovArray = new JSONArray();
        for (int iX=0; iX<covList.size(); iX++) {
            jsCovArray.put(EJBUtil.hashMapToJSON((HashMap)covList.get(iX)));
        }
        jsObj.put("coverageArray", jsCovArray);
        
        for (int iC=0; iC<covNotesListOut.size(); iC++) {
            Integer covEvent = (Integer)((HashMap)covNotesListOut.get(iC)).get("eventId");
            for (int iD=0; iD<dataList.size(); iD++) {
                Integer dataEvent = (Integer)((HashMap)dataList.get(iD)).get("eventId");
                if (covEvent.equals(dataEvent)) {
                    HashMap map1 = (HashMap)covNotesListOut.get(iC);
                    map1.put("eventIndex", new Integer(iD));
                    break;
                }
            }
        }
        JSONArray jsCovNotesArray = new JSONArray();
        for (int iX=0; iX<covNotesListOut.size(); iX++) {
            jsCovNotesArray.put(EJBUtil.hashMapToJSON((HashMap)covNotesListOut.get(iX)));
        }
        jsObj.put("coverageNotes", jsCovNotesArray);
        
        return jsObj.toString();
    }
}
