/* 
 * Classname			BudgetUsersJB.class
 * 
 * Version information
 *
 * Date					03/21/2002
 *
 * Author 				Sonika Talwar
 * Copyright notice
 */

package com.velos.esch.web.budgetUsers;

/* IMPORT STATEMENTS */

import java.util.ArrayList;
import java.util.Hashtable;
import com.velos.esch.business.budgetUsers.impl.BudgetUsersBean;
import com.velos.esch.business.common.BudgetUsersDao;
import com.velos.esch.service.budgetUsersAgent.BudgetUsersAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side Java Bean for BudgetUsers
 * 
 * @author Sonika Talwar
 * @version 1.0 03/21/2002
 */

public class BudgetUsersJB {
    /**
     * the budget user Id
     */
    private int bgtUsersId;

    /**
     * the Budget
     */
    private String bgtUsersBudget;

    /**
     * the Users of budget
     */
    private String bgtUsers;

    /**
     * the Users of budget
     */
    private ArrayList userIds;

    /**
     * the budget users rights
     */
    private String bgtUsersRights;

    /**
     * the budget users type
     */

    private String bgtUsersType;

    /*
     * creator
     */
    private String creator;

    /*
     * last modified by
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;

    // GETTER SETTER METHODS

    /**
     * @param bgtUsersId
     *            The value that is required to be registered as primary key of
     *            budget users id
     */

    public void setBgtUsersId(int bgtUsersId) {
        this.bgtUsersId = bgtUsersId;
    }

    /**
     * @return budget user id
     */
    public int getBgtUsersId() {
        return this.bgtUsersId;
    }

    /**
     * @param budget
     *            The value that is required to be registered as the budget for
     *            which users are being given rights
     */

    public void setBgtUsersBudget(String bgtUsersBudget) {
        this.bgtUsersBudget = bgtUsersBudget;
    }

    /**
     * @return budget id
     */
    public String getBgtUsersBudget() {
        return this.bgtUsersBudget;
    }

    /**
     * @param bgtUsers
     *            The value that is required to be registered as the user of the
     *            budget
     */

    public void setBgtUsers(String bgtUsers) {
        this.bgtUsers = bgtUsers;
    }

    public void setBgtUsers(ArrayList userIds) {
        this.userIds = userIds;
    }

    /**
     * @return user id
     */
    public String getBgtUsers() {
        return this.bgtUsers;
    }

    /**
     * @param bgtUsersRights
     *            The value that is required to be registered as access rights
     *            of the budget users
     */

    public void setBgtUsersRights(String bgtUsersRights) {
        this.bgtUsersRights = bgtUsersRights;
    }

    /**
     * @return budget user right
     */

    public String getBgtUsersRights() {
        return this.bgtUsersRights;
    }

    /**
     * @param bgtUserType
     *            The value that is required to be registered as type of user
     *            (S-special user,N-normal user)
     */

    public void setBgtUsersType(String bgtUsersType) {
        this.bgtUsersType = bgtUsersType;
    }

    /**
     * @return budget user type
     */
    public String getBgtUsersType() {
        return this.bgtUsersType;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */

    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return last modified by
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * @return IP
     */
    public String getIpAdd() {
        return this.ipAdd;

    }

    // END OF GETTER SETTER METHODS

    /**
     * Constructor; Accepts BudgetUsersId
     * 
     * @param budgetUsersId
     *            Id of the BudgetUser
     */
    public BudgetUsersJB(int bgtUsersId) {
        setBgtUsersId(bgtUsersId);
    }

    /**
     * Default Constructor
     * 
     */
    public BudgetUsersJB() {
        Rlog.debug("BudgetUser", "BudgetUsersJB.BudgetUsersJB() ");
    }

    public BudgetUsersJB(int bgtUsersId, String bgtUsersBudget,
            String bgtUsers, String bgtUsersRights, String bgtUsersType,
            String creator, String modifiedBy, String ipAdd) {
        setBgtUsersId(bgtUsersId);
        setBgtUsersBudget(bgtUsersBudget);
        setBgtUsers(bgtUsers);
        setBgtUsersRights(bgtUsersRights);
        setBgtUsersType(bgtUsersType);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);

        Rlog.debug("BudgetUser", "BudgetUsersJB.BudgetUsersJB(all parameters)");
    }

    /**
     * Calls getBudgetUsersDetails() of BudgetUsers Session Bean:
     * BudgetUsersAgentBean
     * 
     * @return The Details of the budgetUsers in the BudgetUsers StateKeeper
     *         Object
     * @See BudgetUsersAgentBean
     */

    public BudgetUsersBean getBudgetUsersDetails() {
        BudgetUsersBean busk = null;

        try {
            BudgetUsersAgentRObj budgetUsersAgent = EJBUtil
                    .getBudgetUsersAgentHome();
            busk = budgetUsersAgent.getBudgetUsersDetails(this.bgtUsersId);
            Rlog.debug("BudgetUser",
                    "BudgetUsersJB.getBudgetUsersDetails() BudgetUsersStateKeeper "
                            + busk);
        } catch (Exception e) {
            Rlog.fatal("BudgetUser",
                    "Error in getBudgetUsersDetails() in BudgetUsersJB " + e);
        }
        if (busk != null) {
            this.bgtUsersId = busk.getBgtUsersId();
            this.bgtUsersBudget = busk.getBgtUsersBudget();
            this.bgtUsers = busk.getBgtUsers();
            this.bgtUsersRights = busk.getBgtUsersRights();
            this.bgtUsersType = busk.getBgtUsersType();
            this.creator = busk.getCreator();
            this.modifiedBy = busk.getModifiedBy();
            this.ipAdd = busk.getIpAdd();
        }
        return busk;
    }

    /**
     * Calls setBudgetUsersDetails() of BudgetUsers Session Bean:
     * BudgetUsersAgentBean
     * 
     */

    public void setBudgetUsersDetails() {

        try {
            BudgetUsersAgentRObj budgetUsersAgentR = EJBUtil
                    .getBudgetUsersAgentHome();
            this.setBgtUsersId(budgetUsersAgentR.setBudgetUsersDetails(this
                    .createBudgetUsersStateKeeper()));
            Rlog.debug("BudgetUser", "BudgetUsersJB.setBudgetUsersDetails()");
        } catch (Exception e) {
            Rlog.fatal("BudgetUser",
                    "Error in setBudgetUsersDetails() in BudgetUsersJB " + e);
        }
    }

    /**
     * Calls updateBudgetUsers() of BudgetUsers Session Bean:
     * BudgetUsersAgentBean
     * 
     * @return -2 in case of exception
     */
    public int updateBudgetUsers() {
        int output;
        try {
            BudgetUsersAgentRObj budgetUsersAgentRObj = EJBUtil
                    .getBudgetUsersAgentHome();
            output = budgetUsersAgentRObj.updateBudgetUsers(this
                    .createBudgetUsersStateKeeper());
        } catch (Exception e) {
            Rlog
                    .fatal("BudgetUser",
                            "EXCEPTION IN SETTING BUDGET USERS DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * 
     * @return the Budget Users StateKeeper Object with the current values of
     *         the Bean
     */

    public BudgetUsersBean createBudgetUsersStateKeeper() {
        return new BudgetUsersBean(userIds, bgtUsersId, bgtUsersBudget,
                bgtUsers, bgtUsersRights, bgtUsersType, creator, modifiedBy,
                ipAdd);
    }

    /**
     * Search Users which are not in budget
     * 
     * @return the Budget DAO
     */

    public BudgetUsersDao getUsersNotInBudget(int accountId, int budgetId,
            String lname, String fname, String role, String organization,
            String userType) {
        BudgetUsersDao bgtUsersDao = new BudgetUsersDao();
        try {
            BudgetUsersAgentRObj budgetUsersAgentRObj = EJBUtil
                    .getBudgetUsersAgentHome();
            bgtUsersDao = budgetUsersAgentRObj.getUsersNotInBudget(accountId,
                    budgetId, lname, fname, role, organization, userType);
        } catch (Exception e) {
            Rlog.fatal("BudgetUser",
                    "EXCEPTION IN getting users not in budget ");
            e.printStackTrace();
            return bgtUsersDao;
        }
        return bgtUsersDao;
    }

    /**
     * Get Users which are in budget
     * 
     * @return the Budget DAO
     */

    public BudgetUsersDao getUsersInBudget(int budgetId) {
        BudgetUsersDao bgtUsersDao = new BudgetUsersDao();
        try {

            BudgetUsersAgentRObj budgetUsersAgentRObj = EJBUtil
                    .getBudgetUsersAgentHome();
            bgtUsersDao = budgetUsersAgentRObj.getUsersInBudget(budgetId);
        } catch (Exception e) {
            Rlog.fatal("BudgetUser", "EXCEPTION IN getting users in budget ");
            e.printStackTrace();
            return bgtUsersDao;
        }
        return bgtUsersDao;
    }

    /**
     * Delete user from budget
     * 
     * @return 0 for successful delete, -2 in case of exception
     */

    public int removeBudgetUser(int bgtUserId) {
        int ret = 0;
        try {
            Rlog.debug("BudgetUser", "In BudgteUsersBean removeBudgetUser()");

            BudgetUsersAgentRObj budgetUsersAgentRObj = EJBUtil
                    .getBudgetUsersAgentHome();

            ret = budgetUsersAgentRObj.removeBudgetUser(bgtUserId);
            return ret;
        }

        catch (Exception e) {
            Rlog.fatal("BudgetUser", "In BudgteUsersBean - remove budget user"
                    + e);
            return -2;
        }

    }
    
    // Overloaded for INF-18183 ::: AGodara
    public int removeBudgetUser(int bgtUserId,Hashtable<String, String> auditInfo) {
        int ret = 0;
        try {
        	BudgetUsersAgentRObj budgetUsersAgentRObj = EJBUtil
                    .getBudgetUsersAgentHome();
        	ret = budgetUsersAgentRObj.removeBudgetUser(bgtUserId,auditInfo);
            return ret;
        }catch (Exception e) {
            Rlog.fatal("BudgetUser", "In BudgteUsersBean - removeBudgetUser(int bgtUserId,Hashtable<String, String> auditInfo)"
                    + e);
            return -2;
        }

    }

    

    /**
     * Gets User Rights for the Budget
     * 
     * @param budgetId
     * @param userId
     * @return String of rights, null in case of exception
     */
    public String getBudgetUserRight(int budgetId, int userId) {
        String userRight;
        try {
            BudgetUsersAgentRObj budgetUsersAgentRObj = EJBUtil
                    .getBudgetUsersAgentHome();
            userRight = budgetUsersAgentRObj.getBudgetUserRight(budgetId,
                    userId);
        } catch (Exception e) {
            Rlog.fatal("BudgetUser",
                    "EXCEPTION IN getting user right for budgetbudget ");
            e.printStackTrace();
            return null;
        }
        return userRight;

    }

}// end of class
