/*

 * Classname : 				SubCostItemJB

 * 

 * Version information : 	1.0

 *

 * Date 					01/19/11

 * 

 * Copyright notice: 		Velos Inc

 * 

 * Author 					Manimaran

 */
package com.velos.esch.web.subCostItem;

import java.util.ArrayList;
import java.util.Hashtable;

import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;
import com.velos.esch.business.common.SubCostItemDao;
import com.velos.esch.business.subCostItem.impl.SubCostItemBean;
import com.velos.esch.service.subCostItemAgent.SubCostItemAgentRObj;

public class SubCostItemJB {
	
	/**
	 * Subject Cost Item Id
	 */
	private int subCostItemId;
	
	/**
	 * fkCalendar
	 */
	
	private int fkCalendar;
	
	/**
	 * subCostItemName
	 */
	private String subCostItemName;
	
	/**
	 * subCostItemCost
	 */
	private String subCostItemCost;
	
	/**
	 * subCostItemUnit
	 */
	private String subCostItemUnit;
	
	/**
	 * subCostItemSeq
	 */
	private String subCostItemSeq;
	
	/**
	 * fkCodelstCategory
	 */
	private Integer fkCodelstCategory;
	
	/**
	 * fkCodelstCostType 
	 */
	private String fkCodelstCostType;
	
	/*YK Bug 5821 & # 5818
	 * SUBCOST_ITEM_DELFLAG Removed from Table Not required.
	 * */
	/**
     * creator
     */
    private String creator;
    
    /**
     * last modifiedBy
     */
    private String modifiedBy;
    
    /**
     * IP Address
     */
    private String ipAdd;

	
    
    public int getSubCostItemId() {
		return this.subCostItemId;
	}

	public void setSubCostItemId(int subCostItemId) {
		this.subCostItemId = subCostItemId;
	}

	public int getFkCalendar() {
		return this.fkCalendar;
	}

	public void setFkCalendar(int fkCalendar) {
		this.fkCalendar = fkCalendar;
	}

	public String getSubCostItemName() {
		return this.subCostItemName;
	}

	public void setSubCostItemName(String subCostItemName) {
		this.subCostItemName = subCostItemName;
	}

	public String getSubCostItemCost() {
		return this.subCostItemCost;
	}

	public void setSubCostItemCost(String subCostItemCost) {
		//if (subCostItemCost != null){
			this.subCostItemCost = subCostItemCost;
		//}
	}

	public String getSubCostItemUnit() {
		return this.subCostItemUnit;
	}

	public void setSubCostItemUnit(String subCostItemUnit) {
		//if (subCostItemUnit != null){
			this.subCostItemUnit = subCostItemUnit;
		//}
	}

	public String getSubCostItemSeq() {
		return this.subCostItemSeq;
	}

	public void setSubCostItemSeq(String subCostItemSeq) {
		this.subCostItemSeq = subCostItemSeq;
	}

	public Integer getFkCodelstCategory() {
		return this.fkCodelstCategory;
	}

	public void setFkCodelstCategory(int fkCodelstCategory) {
		this.fkCodelstCategory = fkCodelstCategory;
	}

	public String getFkCodelstCostType() {
		return this.fkCodelstCostType;
	}

	public void setFkCodelstCostType(String fkCodelstCostType) {
		this.fkCodelstCostType = fkCodelstCostType;
	}

	
	public String getCreator() {
		return this.creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getIpAdd() {
		return this.ipAdd;
	}

	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}
   
	public SubCostItemJB(int subCostItemId) {
		setSubCostItemId(subCostItemId);
    }
	
	// DEFAULT CONSTRUCTOR
    public SubCostItemJB() {
    }
    
    public SubCostItemJB(int subCostItemId, int fkCalendar,
			String subCostItemName, String subCostItemCost,
			String subCostItemUnit, String subCostItemSeq,
			Integer fkCodelstCategory, String fkCodelstCostType,
			String creator, String modifiedBy,
			String ipAdd) {
		setSubCostItemId(subCostItemId);
		setFkCalendar(fkCalendar);
		setSubCostItemName(subCostItemName);
		setSubCostItemCost(subCostItemCost);
		setSubCostItemUnit(subCostItemUnit);
		setSubCostItemSeq(subCostItemSeq);
		setFkCodelstCategory(fkCodelstCategory);
		setFkCodelstCostType(fkCodelstCostType);
		setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }
    	
    public SubCostItemBean getSubCostItemDetails() {

    	SubCostItemBean scsk = null;

        try {

        	SubCostItemAgentRObj subCostAgent = EJBUtil
                    .getSubCostItemAgentHome();
            scsk = subCostAgent.getSubCostItemDetails(this.subCostItemId);
        } catch (Exception e) {
            Rlog.fatal("subcostitem",
                    "EXception in calling session bean in subject cost item" + e);

        }
        if (scsk != null) {

    		this.subCostItemId = scsk.getSubCostItemId();
    		this.fkCalendar = scsk.getFkCalendar();
    		this.subCostItemName = scsk.getSubCostItemName();
    		this.subCostItemCost = scsk.getSubCostItemCost();
    		this.subCostItemUnit = scsk.getSubCostItemUnit();
    		this.subCostItemSeq = scsk.getSubCostItemSeq();
    		this.fkCodelstCategory = scsk.getFkCodelstCategory();
    		this.fkCodelstCostType = scsk.getFkCodelstCostType();
    		this.creator = scsk.getCreator();
    		this.modifiedBy = scsk.getModifiedBy();
    		this.ipAdd = scsk.getIpAdd();
        	
        }
        return scsk;
    }
    
    public int setSubCostItemDetails() {
        int output = 0;
        try {
        	SubCostItemAgentRObj subCostAgent = EJBUtil.getSubCostItemAgentHome();
            output = subCostAgent.setSubCostItemDetails(this
                    .createSubCostItemStateKeeper());
        	           
            this.setSubCostItemId(output);
            Rlog.debug("subcostitem", "SubCostItemJB.setSubCostItemDetails()");

        }catch (Exception e) {
            Rlog
                    .fatal("subcostitem", "Error in setSubCostItemDetails() in SubCostItemJB "
                            + e);
          return -2;
        }
        return output;
    }
    
    
    public int updateSubCostItem() {
        int output;
        try {
        	SubCostItemAgentRObj subCostAgent = EJBUtil.getSubCostItemAgentHome();
            output = subCostAgent.updateSubCostItem(this
                    .createSubCostItemStateKeeper());
        } catch (Exception e) {
            Rlog.debug("subcostitem",
                    "EXCEPTION IN SETTING SUBJECT COST ITEM DETAILS TO  SESSION BEAN"
                            + e.getMessage());

            return -2;
        }
        return output;
    }
    
  
    public SubCostItemBean createSubCostItemStateKeeper() {
        return new SubCostItemBean(this.subCostItemId, this.fkCalendar, this.subCostItemName, 
        		this.subCostItemCost, this.subCostItemUnit, this.subCostItemSeq, this.fkCodelstCategory,
        		this.fkCodelstCostType, this.creator, this.modifiedBy, this.ipAdd);
    }
    
    public SubCostItemDao getProtSelectedAndGroupedItems(int protocolId) {
        SubCostItemDao subCostItemDao = null;
        try {
            SubCostItemAgentRObj subCostItemAgentRObj = EJBUtil.getSubCostItemAgentHome();
            subCostItemDao = subCostItemAgentRObj
                    .getProtSelectedAndGroupedItems(protocolId);
        } catch (Exception e) {
            Rlog.fatal("SubCostItem",
                    "In SubCostItemJB.getProtSelectedAndGroupedItems: "+e);
            subCostItemDao = new SubCostItemDao();
        }
        return subCostItemDao;
    }
    
    //KM-#5949
    
    public int deleteSubCostItems(ArrayList visitIds, ArrayList itemIds, int user) {

    	int output = 0;
        try {

            SubCostItemAgentRObj subCostAgent = EJBUtil.getSubCostItemAgentHome();
            output = subCostAgent.deleteSubCostItems(visitIds, itemIds, user);
        } catch (Exception e) {
            Rlog.fatal("subCostItem",
                    "Error in deletesubCostItems(itemIds)  in SubCostItemJB "
                            + e);
            return -1;
        }
        return output;


    }

    
    // deleteSubCostItems() Overloaded for INF-18183 ::: Raviesh
    public int deleteSubCostItems(ArrayList visitIds, ArrayList itemIds, int user,Hashtable<String, String> args) {

    	int output = 0;
        try {

            SubCostItemAgentRObj subCostAgent = EJBUtil.getSubCostItemAgentHome();
            output = subCostAgent.deleteSubCostItems(visitIds, itemIds, user,args);
        } catch (Exception e) {
            Rlog.fatal("subCostItem",
                    "Error in deletesubCostItems(itemIds)  in SubCostItemJB "
                            + e);
            return -1;
        }
        return output;


    }

    
    
    /*YK Bug 5821 & # 5818
	 * Code Changed to Remove physically from Table.
	 * */
    

    public int removeSubCostItem(int itemId) {
        int output;
        try {
        	SubCostItemAgentRObj subCostAgent = EJBUtil.getSubCostItemAgentHome();
            output = subCostAgent.removeSubCostItem(itemId);
        } catch (Exception e) {
            Rlog.debug("subcostitem",
                    "EXCEPTION IN REMOVING SUBJECT COST ITEM DETAILS"
                            + e.getMessage());

            return -2;
        }
        return output;
    }
    
    
    // removeSubCostItem() Overloaded for INF-18183 ::: Raviesh
    public int removeSubCostItem(int itemId ,Hashtable<String, String> args) {
        int output;
        try {
        	SubCostItemAgentRObj subCostAgent = EJBUtil.getSubCostItemAgentHome();
            output = subCostAgent.removeSubCostItem(itemId,args);
        } catch (Exception e) {
            Rlog.debug("subcostitem",
                    "EXCEPTION IN REMOVING SUBJECT COST ITEM DETAILS"
                            + e.getMessage());

            return -2;
        }
        return output;
    }
    

    
}
