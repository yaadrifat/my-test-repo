/*
 * Classname : BudgetcalBean
 * 
 * Version information: 1.0
 *
 * Date: 03/19/2002
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana 
 */

package com.velos.esch.business.budgetcal.impl;

/* Import Statements */
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

/* End of Import Statements */

@Entity
@Table(name = "sch_bgtcal")
public class BudgetcalBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3977581398557275957L;

    /**
     * budgetcalId
     */
    public int budgetcalId;

    /**
     * budgetId
     */
    public Integer budgetId;

    /**
     * budgetProtId
     */
    public Integer budgetProtId;

    /**
     * budgetProtType
     */
    public String budgetProtType;

    /**
     * budgetDelFlag
     */
    public String budgetDelFlag;

    /**
     * spOverHead
     */
    public Float spOverHead;

    /**
     * clOverHead
     */
    public Float clOverHead;

    /**
     * spFlag
     */
    public String spFlag;

    /**
     * clFlag
     */
    public String clFlag;

    /**
     * indirectCost
     */
    public Float indirectCost;

    /*
     * creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * IP Address
     */
    public String ipAdd;

    /*
     * Fringe benefit
     */
    public Float budgetFrgBenefit;

    /*
     * Fringe Flag
     */
    public Integer budgetFrgFlag;

    /*
     * Budget discount
     */
    public Float budgetDiscount;

    /*
     * Budget discount flag
     */
    public Integer budgetDiscountFlag;

    /*
     * Exclude SOC flag
     */
    public Integer budgetExcldSOCFlag;

  
    //Added By IA 9/18/2006
    /*
     * Grand Research Total (cost/patient)
     */
    public String budgetResearchCost;

    /*
     * Grand Research Total (cost/patient)
     */
    public String budgetResearchTotalCost;

    /*
     * Grand Research Total (cost/patient)
     */
    public String budgetSOCCost;
    
    /*
     * Grand Research Total (cost/patient)
     */
    public String budgetSOCTotalCost;
    
    /*
     * Grand Research Total (cost/patient)
     */
    public String budgetTotalCost;
    
    /*
     * Grand Research Total (cost/patient)
     */
    public String budgetTotalTotalCost;
    
    /*
     * Grand Research Total (cost/patient)
     */
    public String budgetSalaryCost;
    
    /*
     * Grand Research Total (cost/patient)
     */
    public String budgetSalaryTotalCost;
    
    /*
     * Grand Research Total (cost/patient)
     */
    public String budgetFringeCost;
  
    /*
     * Grand Research Total (cost/patient)
     */
    public String budgetFringeTotalCost;
    
    /*
     * Grand Research Total (cost/patient)
     */
    public String budgetDiscontinueCost;
 
    /*
     * Grand Research Total (cost/patient)
     */
    public String budgetDiscontinueTotalCost;
 
    /*
     * Grand Research Total (cost/patient)
     */
    public String budgetIndirectCost;
    
    /*
     * Grand Research Total (cost/patient)
     */
    public String budgetIndirectTotalCost;
    
    //End Added By IA 9/18/2006
    
    
    //Added by IA 11.03.2006 Comparative Budget
    
    public String budgetResearchSponsorAmount;
    
    /*
     * Grand Research Total Sponsor Amount
     */
    
    public String budgetResearchVariance;
    
    /*
     * Grand Research Total Variance
     */
    
    public String budgetSOCSponsorAmount;
    
    /*
     * Grand SOC  Sponsor Amount
     *      */
    
    public String budgetSOCVariance;
    
    /*
     * Grand SOC Variance
     */
    
    public String budgetTotalSponsorAmount;
    
    /*
     * Grand Total Sponsor Amount (cost/patient)
     */
    
    public String budgetTotalVariance;
    
    /*
     * Grand Total Variance 
     */
    
    //End Added
 
    
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_SCH_BGTCAL", allocationSize=1)
    @Column(name = "PK_BGTCAL")
    public int getBudgetcalId() {
        return this.budgetcalId;
    }

    public void setBudgetcalId(int budgetcalId) {
        this.budgetcalId = budgetcalId;
    }

    @Column(name = "FK_BUDGET")
    public String getBudgetId() {
        return StringUtil.integerToString(this.budgetId);
    }

    public void setBudgetId(String budgetId) {
        if (budgetId != null) {
            this.budgetId = Integer.valueOf(budgetId);
        }
    }

    @Column(name = "BGTCAL_PROTID")
    public String getBudgetProtId() {
        return StringUtil.integerToString(this.budgetProtId);
    }

    public void setBudgetProtId(String budgetProtId) {

        if (budgetProtId != null) {
            this.budgetProtId = Integer.valueOf(budgetProtId);
        }
    }

    @Column(name = "BGTCAL_PROTTYPE")
    public String getBudgetProtType() {
        return this.budgetProtType;
    }

    public void setBudgetProtType(String budgetProtType) {
        this.budgetProtType = budgetProtType;
    }

    @Column(name = "BGTCAL_DELFLAG")
    public String getBudgetDelFlag() {
        return this.budgetDelFlag;
    }

    public void setBudgetDelFlag(String budgetDelFlag) {
        this.budgetDelFlag = budgetDelFlag;
    }

    @Column(name = "BGTCAL_SPONSOROHEAD")
    public String getSpOverHead() {

        return StringUtil.floatToString(this.spOverHead);
        

    }

    public void setSpOverHead(String spOverHead) {

        if (spOverHead != null) {
            this.spOverHead = Float.valueOf(spOverHead);
        }
    }

    @Column(name = "BGTCAL_CLINICOHEAD")
    public String getClOverHead() {
        return StringUtil.floatToString(this.clOverHead);
    }

    public void setClOverHead(String clOverHead) {
        if (clOverHead != null) {
            this.clOverHead = Float.valueOf(clOverHead);
        }
    }

    @Column(name = "BGTCAL_SPONSORFLAG")
    public String getSpFlag() {
        return this.spFlag;
    }

    public void setSpFlag(String spFlag) {
        this.spFlag = spFlag;
    }

    @Column(name = "BGTCAL_CLINICFLAG")
    public String getClFlag() {
        return this.clFlag;
    }

    public void setClFlag(String clFlag) {
        this.clFlag = clFlag;
    }

    @Column(name = "BGTCAL_INDIRECTCOST")
    public String getIndirectCost() {
        return StringUtil.floatToString(this.indirectCost);
    }

    public void setIndirectCost(String indirectCost) {

        if (indirectCost != null) {
            this.indirectCost = Float.valueOf(indirectCost);
        }
    }

    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    @Column(name = "BGTCAL_FRGBENEFIT")
    public String getBudgetFrgBenefit() {
        return StringUtil.floatToString(this.budgetFrgBenefit);
    }

    public void setBudgetFrgBenefit(String budgetFrgBenefit) {
        if (budgetFrgBenefit != null) {
            this.budgetFrgBenefit = Float.valueOf(budgetFrgBenefit);
        }
    }

    @Column(name = "BGTCAL_FRGFLAG")
    public String getBudgetFrgFlag() {
        return StringUtil.integerToString(this.budgetFrgFlag);
    }

    public void setBudgetFrgFlag(String budgetFrgFlag) {
        if (budgetFrgFlag != null) {
            this.budgetFrgFlag = Integer.valueOf(budgetFrgFlag);
        }

    }

    @Column(name = "BGTCAL_DISCOUNT")
    public String getBudgetDiscount() {
        return StringUtil.floatToString(this.budgetDiscount);
    }

    public void setBudgetDiscount(String budgetDiscount) {
        if (budgetDiscount != null) {
            this.budgetDiscount = Float.valueOf(budgetDiscount);
        }
    }

    @Column(name = "BGTCAL_DISCOUNTFLAG")
    public String getBudgetDiscountFlag() {
        return StringUtil.integerToString(this.budgetDiscountFlag);
    }

    public void setBudgetDiscountFlag(String budgetDiscountFlag) {
        if (budgetDiscountFlag != null) {
            this.budgetDiscountFlag = Integer.valueOf(budgetDiscountFlag);
        }

    }

    @Column(name = "BGTCAL_EXCLDSOCFLAG")
    public String getBudgetExcldSOCFlag() {
        return StringUtil.integerToString(this.budgetExcldSOCFlag);
    }

    public void setBudgetExcldSOCFlag(String budgetExcldSOCFlag) {

        this.budgetExcldSOCFlag = StringUtil.stringToInteger(budgetExcldSOCFlag);

    }
    
    
    //Aded by IA 9/18/2006
    
    @Column(name = "GRAND_RES_COST")
    public String getBudgetResearchCost() {

    	
        return this.budgetResearchCost;
        //return ((float)Math.round(this.lineitemTotalCost)/100 );
    }

    public void setBudgetResearchCost(String budgetResearchCost) {

        if (budgetResearchCost != null) {
            this.budgetResearchCost =budgetResearchCost;
        }
    }
    
    @Column(name = "GRAND_RES_TOTALCOST")
    public String getBudgetResearchTotalCost() {

        return this.budgetResearchTotalCost;

    }

    public void setBudgetResearchTotalCost(String budgetResearchTotalCost) {

        if (budgetResearchTotalCost != null) {
            this.budgetResearchTotalCost =budgetResearchTotalCost;
        }
    }
    
    
    @Column(name = "GRAND_SOC_COST")
    public String getBudgetSOCCost() {

        return this.budgetSOCCost;

    }

    public void setBudgetSOCCost(String budgetSOCCost) {

        if (budgetSOCCost != null) {
            this.budgetSOCCost =budgetSOCCost;
        }
    }
    
    
    
    @Column(name = "GRAND_SOC_TOTALCOST")
    public String getBudgetSOCTotalCost() {

        return this.budgetSOCTotalCost;

    }

    public void setBudgetSOCTotalCost(String budgetSOCTotalCost) {

        if (budgetSOCTotalCost != null) {
            this.budgetSOCTotalCost =budgetSOCTotalCost;
        }
    }
    
    
    
    @Column(name = "GRAND_TOTAL_COST")
    public String getBudgetTotalCost() {

        return this.budgetTotalCost;

    }

    public void setBudgetTotalCost(String budgetTotalCost) {

        if (budgetTotalCost != null) {
            this.budgetTotalCost = budgetTotalCost;
        }
    }
    
    
    @Column(name = "GRAND_TOTAL_TOTALCOST")
    public String getBudgetTotalTotalCost() {

        return this.budgetTotalTotalCost;

    }

    public void setBudgetTotalTotalCost(String budgetTotalTotalCost) {

        if (budgetTotalTotalCost != null) {
            this.budgetTotalTotalCost = budgetTotalTotalCost;
        }
    }
    
    
    @Column(name = "GRAND_SALARY_COST")
    public String getBudgetSalaryCost() {

        return (this.budgetSalaryCost);

    }

    public void setBudgetSalaryCost(String budgetSalaryCost) {

        if (budgetSalaryCost != null) {
            this.budgetSalaryCost =budgetSalaryCost ;
        }
    }
    
    @Column(name = "GRAND_SALARY_TOTALCOST")
    public String getBudgetSalaryTotalCost() {

        return (this.budgetSalaryTotalCost);

    }

    public void setBudgetSalaryTotalCost(String budgetSalaryTotalCost) {

        if (budgetSalaryTotalCost != null) {
            this.budgetSalaryTotalCost = budgetSalaryTotalCost;
        }
    }
    
    @Column(name = "GRAND_FRINGE_COST")
    public String getBudgetFringeCost() {

        return (this.budgetFringeCost);

    }

    public void setBudgetFringeCost(String budgetFringeCost) {

        if (budgetFringeCost != null) {
            this.budgetFringeCost =budgetFringeCost;
        }
    }
    
    
    @Column(name = "GRAND_FRINGE_TOTALCOST")
    public String getBudgetFringeTotalCost() {

        return (this.budgetFringeTotalCost);

    }

    public void setBudgetFringeTotalCost(String budgetFringeTotalCost) {

        if (budgetFringeTotalCost != null) {
            this.budgetFringeTotalCost =budgetFringeTotalCost;
        }
    }
    
    
    @Column(name = "GRAND_DISC_COST")
    public String getBudgetDiscontinueCost() {

        return (this.budgetDiscontinueCost);

    }

    public void setBudgetDiscontinueCost(String budgetDiscontinueCost) {

        if (budgetDiscontinueCost != null) {
            this.budgetDiscontinueCost =budgetDiscontinueCost;
        }
    }
    
    
    @Column(name = "GRAND_DISC_TOTALCOST")
    public String getBudgetDiscontinueTotalCost() {

        return (this.budgetDiscontinueTotalCost);

    }

    public void setBudgetDiscontinueTotalCost(String budgetDiscontinueTotalCost) {

        if (budgetDiscontinueTotalCost != null) {
            this.budgetDiscontinueTotalCost =budgetDiscontinueTotalCost;
        }
    }

    @Column(name = "GRAND_IND_COST")
    public String getBudgetIndirectCost() {

        return (this.budgetIndirectCost);

    }

    public void setBudgetIndirectCost(String budgetIndirectCost) {

        if (budgetIndirectCost != null) {
            this.budgetIndirectCost =budgetIndirectCost;
        }
    }
    
    
    @Column(name = "GRAND_IND_TOTALCOST")
    public String getBudgetIndirectTotalCost() {

        return (this.budgetIndirectTotalCost);

    }

    public void setBudgetIndirectTotalCost(String budgetIndirectTotalCost) {

        if (budgetIndirectTotalCost != null) {
            this.budgetIndirectTotalCost = budgetIndirectTotalCost;
        }
    }
    //End Adde by IA 9/18/2006

    //Added by IA 11.03.2006

    @Column(name = "GRAND_RES_SPONSOR")
    public String getBudgetResearchSponsorAmount() {

        return (this.budgetResearchSponsorAmount);

    }

    public void setBudgetResearchSponsorAmount(String budgetResearchSponsorAmount) {

        if (budgetResearchSponsorAmount != null) {
            this.budgetResearchSponsorAmount = budgetResearchSponsorAmount;
        }
    }    

    @Column(name = "GRAND_RES_VARIANCE")
    public String getBudgetResearchVariance() {

        return (this.budgetResearchVariance);

    }

    public void setBudgetResearchVariance(String budgetResearchVariance) {

        if (budgetResearchVariance != null) {
            this.budgetResearchVariance = budgetResearchVariance;
        }
    }    
    
    
    @Column(name = "GRAND_SOC_SPONSOR")
    public String getBudgetSOCSponsorAmount() {

        return (this.budgetSOCSponsorAmount);

    }

    public void setBudgetSOCSponsorAmount(String budgetSOCSponsorAmount) {

        if (budgetSOCSponsorAmount != null) {
            this.budgetSOCSponsorAmount = budgetSOCSponsorAmount;
        }
    }    
    
    
    @Column(name = "GRAND_SOC_VARIANCE")
    public String getBudgetSOCVariance() {

        return (this.budgetSOCVariance);

    }

    public void setBudgetSOCVariance(String budgetSOCVariance) {

        if (budgetSOCVariance != null) {
            this.budgetSOCVariance = budgetSOCVariance;
        }
    }    
    
    
    @Column(name = "GRAND_TOTAL_SPONSOR")
    public String getBudgetTotalSponsorAmount() {

        return (this.budgetTotalSponsorAmount);

    }

    public void setBudgetTotalSponsorAmount(String budgetTotalSponsorAmount) {

        if (budgetTotalSponsorAmount != null) {
            this.budgetTotalSponsorAmount = budgetTotalSponsorAmount ;
        }
    }    
    
    
    @Column(name = "GRAND_TOTAL_VARIANCE")
    public String getBudgetTotalVariance() {

        return (this.budgetTotalVariance);

    }

    public void setBudgetTotalVariance(String budgetTotalVariance) {

        if (budgetTotalVariance != null) {
            this.budgetTotalVariance = budgetTotalVariance;
        }
    }    
    
    
    //End added
    
    
    // END OF GETTER SETTER METHODS

    /**
     * returns the state holder object associated with this budgetcal
     */

    /*
     * public BudgetcalStateKeeper getBudgetcalStateKeeper() {
     * Rlog.debug("budgetcal", "BudgetcalBean.getBudgetcalStateKeeper"); return
     * new BudgetcalStateKeeper(getBudgetcalId(), getBudgetId(),
     * getBudgetProtId(), getBudgetProtType(), getBudgetDelFlag(),
     * getSpOverHead(), getClOverHead(), getSpFlag(), getClFlag(),
     * getIndirectCost(), getCreator(), getModifiedBy(), getIpAdd(),
     * getBudgetFrgBenefit(), getBudgetFrgFlag(), getBudgetDiscount(),
     * getBudgetDiscountFlag(), getBudgetExcldSOCFlag()); }
     */

    /**
     * sets up a state keeper containing details of the budgetcal
     * 
     * @param bcsk
     */

    /*
     * public void setBudgetcalStateKeeper(BudgetcalStateKeeper bcsk) {
     * GenerateId Id = null;
     * 
     * try {
     * 
     * Connection conn = null; conn = getConnection(); Rlog.debug("budgetcal",
     * "BudgetcalBean.setBudgetcalStateKeeper() Connection :" + conn);
     * budgetcalId = Id.getId("SEQ_SCH_BGTCAL", conn); conn.close(); //
     * setBudgetcalId(bcsk.getBudgetcalId()); setBudgetId(bcsk.getBudgetId());
     * 
     * Rlog.debug("budgetcal", "budgetid is :" + bcsk.getBudgetId());
     * 
     * setBudgetProtId(bcsk.getBudgetProtId());
     * setBudgetProtType(bcsk.getBudgetProtType());
     * setBudgetDelFlag(bcsk.getBudgetDelFlag());
     * setSpOverHead(bcsk.getSpOverHead()); setClOverHead(bcsk.getClOverHead());
     * setSpFlag(bcsk.getSpFlag()); setClFlag(bcsk.getClFlag());
     * setIndirectCost(bcsk.getIndirectCost()); setCreator(bcsk.getCreator());
     * setModifiedBy(bcsk.getModifiedBy()); setIpAdd(bcsk.getIpAdd());
     * setBudgetFrgBenefit(bcsk.getBudgetFrgBenefit());
     * setBudgetFrgFlag(bcsk.getBudgetFrgFlag());
     * setBudgetDiscount(bcsk.getBudgetDiscount());
     * setBudgetDiscountFlag(bcsk.getBudgetDiscountFlag());
     * setBudgetExcldSOCFlag(bcsk.getBudgetExcldSOCFlag()); } catch (Exception
     * e) { Rlog.fatal("budgetcal", "Error in setBudgetcalStateKeeper() in
     * BudgetcalBean " + e); } }
     */

    /**
     * updates the budgetcal details
     * 
     * @param bcsk
     * @return 0 if successful; -2 for exception
     */
    public int updateBudgetcal(BudgetcalBean bcsk) {
        try {

            // setBudgetcalId(bcsk.getBudgetcalId());
            setBudgetId(bcsk.getBudgetId());
            setBudgetProtId(bcsk.getBudgetProtId());
            setBudgetProtType(bcsk.getBudgetProtType());
            setBudgetDelFlag(bcsk.getBudgetDelFlag());
            setSpOverHead(bcsk.getSpOverHead());
            setClOverHead(bcsk.getClOverHead());
            setSpFlag(bcsk.getSpFlag());
            setClFlag(bcsk.getClFlag());
            setIndirectCost(bcsk.getIndirectCost());
            setCreator(bcsk.getCreator());
            setModifiedBy(bcsk.getModifiedBy());
            setIpAdd(bcsk.getIpAdd());
            setBudgetFrgBenefit(bcsk.getBudgetFrgBenefit());
            setBudgetFrgFlag(bcsk.getBudgetFrgFlag());
            setBudgetDiscount(bcsk.getBudgetDiscount());
            setBudgetDiscountFlag(bcsk.getBudgetDiscountFlag());
            setBudgetExcldSOCFlag(bcsk.getBudgetExcldSOCFlag());            
            setBudgetResearchCost(bcsk.getBudgetResearchCost());
            setBudgetResearchTotalCost(bcsk.getBudgetResearchTotalCost());
            setBudgetSOCCost(bcsk.getBudgetSOCCost());
            setBudgetSOCTotalCost(bcsk.getBudgetSOCTotalCost());
            setBudgetTotalCost(bcsk.getBudgetTotalCost());
            setBudgetTotalTotalCost(bcsk.getBudgetTotalTotalCost());
            setBudgetSalaryCost(bcsk.getBudgetSalaryCost());
            setBudgetSalaryTotalCost(bcsk.getBudgetSalaryTotalCost());
            setBudgetFringeCost(bcsk.getBudgetFringeCost());
            setBudgetFringeTotalCost(bcsk.getBudgetFringeTotalCost());
            setBudgetDiscontinueCost(bcsk.getBudgetDiscontinueCost());
            setBudgetDiscontinueTotalCost(bcsk.getBudgetDiscontinueTotalCost());
            setBudgetIndirectCost(bcsk.getBudgetIndirectCost());
            setBudgetIndirectTotalCost(bcsk.getBudgetIndirectTotalCost());
            
            //Added by IA 11.03.2006 Comparative Budget
            setBudgetResearchSponsorAmount(bcsk.getBudgetResearchSponsorAmount());
            setBudgetResearchVariance(bcsk.getBudgetResearchVariance());
            setBudgetSOCSponsorAmount(bcsk.getBudgetSOCSponsorAmount());
            setBudgetSOCVariance(bcsk.getBudgetSOCVariance());
            setBudgetTotalSponsorAmount(bcsk.getBudgetTotalSponsorAmount());
            setBudgetTotalVariance(bcsk.getBudgetTotalVariance());
            
            //end Added by IA 11.03.2006
            

            return 0;
        } catch (Exception e) {
            Rlog.fatal("budgetcal",
                    " error in BudgetcalBean.updateBudgetcal : " + e);
            return -2;
        }
    }

    public BudgetcalBean() {

    }

    public BudgetcalBean(int budgetcalId, String budgetId, String budgetProtId,
            String budgetProtType, String budgetDelFlag, String spOverHead,
            String clOverHead, String spFlag, String clFlag,
            String indirectCost, String creator, String modifiedBy,
            String ipAdd, String budgetFrgBenefit, String budgetFrgFlag,
            String budgetDiscount, String budgetDiscountFlag,
            String budgetExcldSOCFlag, String budgetResearchCost, String budgetResearchTotalCost, 
            String budgetSOCCost, String budgetSOCTotalCost, String budgetTotalCost, 
            String budgetTotalTotalCost, String budgetSalaryCost, String budgetSalaryTotalCost, 
            String budgetFringeCost, String budgetFringeTotalCost, String budgetDiscontinueCost, 
            String budgetDiscontinueTotalCost, String budgetIndirectCost, String budgetIndirectTotalCost, String budgetResearchSponsorAmount, String budgetResearchVariance,
            String budgetSOCSponsorAmount, String budgetSOCVariance, String budgetTotalSponsorAmount, String budgetTotalVariance) {
        super();
        // TODO Auto-generated constructor stub

        setBudgetcalId(budgetcalId);
        setBudgetId(budgetId);
        setBudgetProtId(budgetProtId);
        setBudgetProtType(budgetProtType);
        setBudgetDelFlag(budgetDelFlag);
        setSpOverHead(spOverHead);
        setClOverHead(clOverHead);
        setSpFlag(spFlag);
        setClFlag(clFlag);
        setIndirectCost(indirectCost);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setBudgetFrgBenefit(budgetFrgBenefit);
        setBudgetFrgFlag(budgetFrgFlag);
        setBudgetDiscount(budgetDiscount);
        setBudgetDiscountFlag(budgetDiscountFlag);
        setBudgetExcldSOCFlag(budgetExcldSOCFlag);        
        setBudgetResearchCost(budgetResearchCost);
        setBudgetResearchTotalCost(budgetResearchTotalCost);
        setBudgetSOCCost(budgetSOCCost);
        setBudgetSOCTotalCost(budgetSOCTotalCost);
        setBudgetTotalCost(budgetTotalCost);
        setBudgetTotalTotalCost(budgetTotalTotalCost);
        setBudgetSalaryCost(budgetSalaryCost);
        setBudgetSalaryTotalCost(budgetSalaryTotalCost);
        setBudgetFringeCost(budgetFringeCost);
        setBudgetFringeTotalCost(budgetFringeTotalCost);
        setBudgetDiscontinueCost(budgetDiscontinueCost);
        setBudgetDiscontinueTotalCost(budgetDiscontinueTotalCost);
        setBudgetIndirectCost(budgetIndirectCost);
        setBudgetIndirectTotalCost(budgetIndirectTotalCost);

        //added by IA 11.03.2006
        setBudgetResearchSponsorAmount(budgetResearchSponsorAmount);
        setBudgetResearchVariance(budgetResearchVariance);
        setBudgetSOCSponsorAmount(budgetSOCSponsorAmount);
        setBudgetSOCVariance(budgetSOCVariance);
        setBudgetTotalSponsorAmount(budgetTotalSponsorAmount);
        setBudgetTotalVariance(budgetTotalVariance);
        
        
        //end added 11.03.2006
        
    }

}// end of class
