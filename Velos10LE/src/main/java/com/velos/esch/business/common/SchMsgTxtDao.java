package com.velos.esch.business.common;

import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.velos.eres.service.util.Rlog;

@SuppressWarnings("serial")
public class SchMsgTxtDao extends CommonDAO implements java.io.Serializable {
    
    public String getMsgTxtLongByType(String type, int fkAccount) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlBuffer = new StringBuffer();
        Clob templateCLob = null;
        String tempMsg = null;
        try {
            conn = getConnection();
            sqlBuffer.append(" select MSGTXT_LONG from sch_msgtxt where ");
            sqlBuffer.append(" MSGTXT_TYPE = '").append(type).append("' ");
            sqlBuffer.append(" and (fk_account = ? )");
            pstmt = conn.prepareStatement(sqlBuffer.toString());
            pstmt.setInt(1, fkAccount);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                templateCLob = rs.getClob("MSGTXT_LONG");
                if (!(templateCLob == null)) {
                    tempMsg = templateCLob.getSubString(1,
                            (int) templateCLob.length());
                }                 
            }
            if (tempMsg != null) return tempMsg;
            sqlBuffer.setLength(sqlBuffer.length()-1);
            sqlBuffer.append(" or fk_account is null) ");
            pstmt = conn.prepareStatement(sqlBuffer.toString());
            pstmt.setInt(1, 0);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                templateCLob = rs.getClob("MSGTXT_LONG");
                if (!(templateCLob == null)) {
                    tempMsg = templateCLob.getSubString(1,
                            (int) templateCLob.length());
                }                 
            }
            return tempMsg;
        } catch (SQLException ex) {
            Rlog.fatal("SchMsgTxtDao",
                    "SchMsgTxtDao.getMsgTxtLongByType"
                            + ex);
        } finally {
            try {
                if (pstmt != null) { pstmt.close(); }
            } catch (Exception e) {}
            try {
                if (conn != null) { conn.close(); }
            } catch (Exception e) {}
        }
        return "";
    }

}
