/*
 * Classname : EventuserDao
 * 
 * Version information : 1.0 
 *
 * Date: 05/21/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sonia Sahni
 */

package com.velos.esch.business.common;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.esch.service.util.Rlog;

/* End of Import Statements */

public class EventuserDao extends CommonDAO implements java.io.Serializable {

    ArrayList eventUserIds;

    ArrayList userTypes;

    ArrayList userIds;

    ArrayList eventId;

    ArrayList duration;

    ArrayList notes;

    ArrayList resourceName;

    public EventuserDao() {

        eventUserIds = new ArrayList();
        userTypes = new ArrayList();
        userIds = new ArrayList();
        duration = new ArrayList();
        notes = new ArrayList();
        resourceName = new ArrayList();

    }

    public ArrayList getEventUserIds() {
        return this.eventUserIds;
    }

    public void setEventUserIds(ArrayList eventUserIds) {
        this.eventUserIds = eventUserIds;
    }

    public ArrayList getUserIds() {
        return this.userIds;
    }

    public void setUserIds(ArrayList userIds) {
        this.userIds = userIds;
    }

    public ArrayList getUserTypes() {
        return this.userTypes;
    }

    public void setUserTypes(ArrayList userTypes) {
        this.userTypes = userTypes;
    }

    public ArrayList getDuration() {
        return this.duration;
    }

    public void setDuration(ArrayList duration) {
        this.duration = duration;
    }

    public ArrayList getNotes() {
        return this.notes;
    }

    public void setNotes(ArrayList notes) {
        this.notes = notes;
    }

    public ArrayList getResourceName() {
        return this.resourceName;
    }

    public void setResourceName(ArrayList resourceName) {
        this.resourceName = resourceName;
    }

    public void setEventUserIds(String eventUserId) {
        eventUserIds.add(eventUserId);
    }

    public void setUserIds(String userId) {
        userIds.add(userId);
    }

    public void setUserTypes(String userType) {
        userTypes.add(userType);
    }

    public void setNotes(String notes) {
        this.notes.add(notes);
    }

    public void setDuration(String duration) {
        this.duration.add(duration);
    }

    public void setResourceName(String resourceName) {
        this.resourceName.add(resourceName);
    }

    // KN for PS13
    public String getEventRolesResources(int eventId){
    	
        String resources = "";
        String sql = " select  pkg_gensch.f_get_event_roles(?) from dual";  
                           
        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();

            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1,eventId);
            

            ResultSet rs = pstmt.executeQuery();

            rs.next();
            resources = rs.getString(1);
            
           
            
        } catch (SQLException ex) {

            Rlog.fatal("event",
                    "EventDao.getEventRolesResources EXCEPTION IN FETCHING FROM eventDaoUser table"
                            + ex);

        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            	
            	Rlog.fatal("event",
                        "EventDao.getEventRolesResources EXCEPTION IN FETCHING FROM eventDaoUser table"
                                + e);
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            	
            	Rlog.fatal("event",
                        "EventDao.getEventRolesResources EXCEPTION IN FETCHING FROM eventDaoUser table"
                                + e);
            }

        }
        return resources;

   }

    public void getEventRoles(int eventId) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs;

        try {
            conn = getConnection();

            String sql = "select pk_eventusr, eventusr, codelst_desc, eventusr_duration, eventusr_notes "
                    + " FROM sch_eventusr , er_codelst where FK_EVENT= ? and eventusr_type = 'R' and "
                    + " pk_codelst = eventusr and  codelst_type = 'role' order by codelst_desc";

            pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, eventId);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                System.out.println("eventinfo line 1");

                setEventUserIds(rs.getString("pk_eventusr"));
                setUserIds(rs.getString("eventusr"));
                setResourceName(rs.getString("codelst_desc"));
                setUserTypes("R");
                setDuration(rs.getString("eventusr_duration"));
                setNotes(rs.getString("eventusr_notes"));
                Rlog.debug("eventinfo", "line 4");
            }

        } catch (SQLException ex) {
            Rlog.fatal("eventuser",
                    "EventuserDao EXCEPTION IN FETCHING FROM EVENT USER table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * 
     * @param eventId
     *            event Id
     */

    public void getEventUsers(int eventId) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs;

        try {
            conn = getConnection();

            String sql = " select pk_eventusr, eventusr, usr_firstname || ' ' || usr_lastname as description , eventusr_duration, eventusr_notes "
                    + " FROM sch_eventusr , er_user where FK_EVENT= ? and eventusr_type = 'U' and "
                    + " pk_user = eventusr order by description ";

            pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, eventId);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                setEventUserIds(rs.getString("pk_eventusr"));
                setUserIds(rs.getString("eventusr"));
                setResourceName(rs.getString("description"));
                setUserTypes("U");
                setDuration(rs.getString("eventusr_duration"));
                setNotes(rs.getString("eventusr_notes"));
            }

        } catch (SQLException ex) {
            Rlog.fatal("eventuser",
                    "EventuserDao EXCEPTION IN FETCHING FROM EVENT USER table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getAllRolesForEvent(int eventId) {
        int rows = 0;

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            StringBuffer sb = new StringBuffer();

            sb
                    .append("select pk_codelst, codelst_desc, '1' as selected , eventusr_duration, eventusr_notes ");
            sb.append(" from  er_codelst , sch_eventusr ");
            sb.append(" where 	fk_event = ? and 	eventusr_type = 'R' and");
            sb.append(" pk_codelst = eventusr and codelst_type = 'role' ");
            sb.append(" union ");
            sb
                    .append(" select pk_codelst, codelst_desc, '0' as selected ,'' eventusr_duration , '' eventusr_notes");
            sb.append(" from 	er_codelst ");
            sb.append(" where 	codelst_type = 'role' and 	");
            sb.append(" pk_codelst not in	(select	eventusr from	sch_eventusr ");
            sb.append(" where 	fk_event = ? and eventusr_type = 'R')");
            sb.append(" order by 2");

            pstmt = conn.prepareStatement(sb.toString());

            pstmt.setInt(1, eventId);
            pstmt.setInt(2, eventId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setUserIds(rs.getString("pk_codelst"));
                setResourceName(rs.getString("codelst_desc"));
                setDuration(rs.getString("eventusr_duration"));
                setNotes(rs.getString("eventusr_notes"));
                setEventUserIds(rs.getString("selected"));
            }

        } catch (SQLException ex) {
            Rlog.fatal("eventuser",
                    "getAllRolesForEvent - EXCEPTION IN FETCHING FROM event resources "
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

    // end of class

}