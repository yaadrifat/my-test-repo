/*
 * Classname			:	EventCrfBean.class
 * 
 * Version information	:	1.0	
 *
 * Date					:	06/26/2006
 * 
 * Copyright notice		:	Velos Inc.
 * 
 */

package com.velos.esch.business.eventCrf.impl;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.esch.service.util.StringUtil;
import com.velos.esch.service.util.Rlog;

/**
 * The Eventcrf CMP entity bean.<br>
 * <br>
 * 
 * @author Manimaran
 */

@Entity
@Table(name = "sch_event_crf")
public class EventCrfBean implements Serializable {
    
    private static final long serialVersionUID = 3546356240990286140L;

    /**
     * the eventcrf Id
     */
    public int eventCrfId;

    /**
     * the eventcrf fkForm
     */
    
	public Integer fkForm;

	/**
	 * the eventcrf  fkEvent
	 */
	
	public int fkEvent;
	
	 /**
      * the ctrltab formType
      */	
	
	public String formType;
	
	/**
     * the ctrltab otherLink
     */	
	
	public String otherLink;
			
	/**
     * the record creator
     */
    
	public Integer eventCrfCreator;

    /**
     * last modified by
     */
    
	public Integer eventCrfLastModBy;

    /**
     * the IP Address
     */
    public String eventCrfIPAdd;


    // GETTER SETTER METHODS
  
    
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_SCH_EVENT_CRF", allocationSize=1)
    @Column(name = "PK_EVENTCRF")
    public int getEventCrfId() {
        return this.eventCrfId;
    }

    public void setEventCrfId(int eventCrfId) {
        this.eventCrfId = eventCrfId;
    }

    
    @Column(name = "FK_EVENT")
    public int getFkEvent() {
        return this.fkEvent;
    }

    public void setFkEvent(int fkEvent) {
        
            this.fkEvent = fkEvent;
    }

    
    @Column(name = "FK_FORM")
    public String getFkForm() {
        return StringUtil.integerToString(this.fkForm);
    }

    public void setFkForm(String fkForm) {
        if (fkForm!="")
            this.fkForm = Integer.valueOf(fkForm);
    }
    

    @Column(name = "FORM_TYPE")
    public String getFormType() {
        return this.formType;
    }

    
    public void setFormType(String formType) {
        this.formType = formType;
    }
    
    
    @Column(name = "OTHER_LINKS")
    public String getOtherLink() {
        return this.otherLink;
    }

    public void setOtherLink(String otherLink) {
        this.otherLink = otherLink;
    }

    
    
    /**
     * @return The Id of the creator of the record
     */
    
    @Column(name = "CREATOR")
    public String getEventCrfCreator() {
        return StringUtil.integerToString(this.eventCrfCreator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setEventCrfCreator(String eventCrfCreator) {
        if (eventCrfCreator != null) {
            this.eventCrfCreator = Integer.valueOf(eventCrfCreator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getEventCrfLastModBy() {
        return StringUtil.integerToString(this.eventCrfLastModBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
   
    public void setEventCrfLastModBy(String eventCrfLastModBy) {
        if (eventCrfLastModBy != null) {
            this.eventCrfLastModBy = Integer.valueOf(eventCrfLastModBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "IP_ADD")
    public String getEventCrfIpAdd() {
        return this.eventCrfIPAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setEventCrfIpAdd(String eventCrfIPAdd) {
        this.eventCrfIPAdd = eventCrfIPAdd;
    }

    // END OF GETTER SETTER METHODS

    
    public EventCrfBean() {

    }

    public EventCrfBean(int eventCrfId, String fkForm, int fkEvent, String formType,
    		
    		String otherLink, String eventCrfCreator , String eventCrfLastModBy , String eventCrfIPAdd) {
        super();
        // TODO Auto-generated constructor stub
        setEventCrfId(eventCrfId);
        setFkForm(fkForm);
        setFkEvent(fkEvent);
        setFormType(formType);
        setOtherLink(otherLink);
        setEventCrfCreator(eventCrfCreator);
        setEventCrfLastModBy(eventCrfLastModBy);
        setEventCrfIpAdd(eventCrfIPAdd);
    }
   
  
    public int updateEventCrf(EventCrfBean eventcrf) {

        try {
        	setFkForm(eventcrf.getFkForm());
        	setFkEvent(eventcrf.getFkEvent());
        	setFormType(eventcrf.getFormType());
        	setOtherLink(eventcrf.getOtherLink());
        	setEventCrfCreator(eventcrf.getEventCrfCreator());
        	setEventCrfLastModBy(eventcrf.getEventCrfLastModBy());
        	setEventCrfIpAdd(eventcrf.getEventCrfIpAdd());
            Rlog.debug("eventcrf", "EventCrfBean.updateEvent");
            return 0;
        } catch (Exception e) {
            Rlog.fatal("event", " error in EventCrfBean.updateEventCrf");
            return -2;
        }
    }

}// end of class
 
