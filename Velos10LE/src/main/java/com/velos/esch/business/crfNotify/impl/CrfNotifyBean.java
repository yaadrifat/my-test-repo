/*
 * Classname : CrfNotifyBean
 * 
 * Version information: 1.0
 *
 * Date: 11/27/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Arvind
 */

package com.velos.esch.business.crfNotify.impl;

/* Import Statements */
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

// import java.sql.*;

/* End of Import Statements */
@Entity
@Table(name = "sch_crfNotify")
public class CrfNotifyBean {

    /**
     * crf Notify
     */

    public int crfNotifyId;

    /**
     * Crf id
     */
    public Integer crfNotifyCrfId;

    /**
     * crf number
     */
    public Integer crfNotifyCodelstNotStatId;

    /**
     * crf name
     */

    public String crfNotifyUsersTo;

    /**
     * crf name
     */

    public String crfNotifyNotes;

    public Integer crfNotStudyId;

    public Integer crfNotProtocolId;

    public Integer crfNotPatProtId;

    public String crfNotGlobalFlag;

    /*
     * creator
     */
    public Integer creator;

    /*
     * modified by
     */
    public Integer modifiedBy;

    /*
     * IP Address
     */
    public String ipAdd;

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_SCH_CRFNOTIFY", allocationSize=1)
    @Column(name = "PK_CRFNOT")
    public int getCrfNotifyId() {
        return this.crfNotifyId;
    }

    public void setCrfNotifyId(int crfNotifyId) {
        this.crfNotifyId = crfNotifyId;
    }

    @Column(name = "FK_CRF")
    public String getCrfNotifyCrfId() {
        return StringUtil.integerToString(this.crfNotifyCrfId);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCrfNotifyCrfId(String crfNotifyCrfId) {
        if (crfNotifyCrfId != null) {
            this.crfNotifyCrfId = Integer.valueOf(crfNotifyCrfId);
        } else {
            this.crfNotifyCrfId = null;
        }

    }

    @Column(name = "crfNotifyCodelstNotStatId")
    public String getCrfNotifyCodelstNotStatId() {
        return StringUtil.integerToString(this.crfNotifyCodelstNotStatId);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCrfNotifyCodelstNotStatId(String crfNotifyCodelstNotStatId) {
        if (crfNotifyCodelstNotStatId != null) {
            this.crfNotifyCodelstNotStatId = Integer
                    .valueOf(crfNotifyCodelstNotStatId);
        }
    }

    @Column(name = "CRFNOT_USERSTO")
    public String getCrfNotifyUsersTo() {
        return this.crfNotifyUsersTo;
    }

    public void setCrfNotifyUsersTo(String crfNotifyUsersTo) {
        this.crfNotifyUsersTo = crfNotifyUsersTo;
    }

    @Column(name = "CRFNOT_NOTES")
    public String getCrfNotifyNotes() {
        return this.crfNotifyNotes;
    }

    public void setCrfNotifyNotes(String crfNotifyNotes) {
        this.crfNotifyNotes = crfNotifyNotes;
    }

    @Column(name = "FK_STUDY")
    public String getCrfNotStudyId() {
        return StringUtil.integerToString(this.crfNotStudyId);
    }

    public void setCrfNotStudyId(String crfNotStudyId) {
        if (crfNotStudyId != null) {
            this.crfNotStudyId = Integer.valueOf(crfNotStudyId);
        }
    }

    @Column(name = "FK_PROTOCOL")
    public String getCrfNotProtocolId() {
        return StringUtil.integerToString(this.crfNotProtocolId);
    }

    public void setCrfNotProtocolId(String crfNotProtocolId) {
        if (crfNotProtocolId != null) {
            this.crfNotProtocolId = Integer.valueOf(crfNotProtocolId);
        }
    }

    @Column(name = "FK_PATPROT")
    public String getCrfNotPatProtId() {
        return StringUtil.integerToString(this.crfNotPatProtId);
    }

    public void setCrfNotPatProtId(String crfNotPatProtId) {
        if (crfNotPatProtId != null) {
            this.crfNotPatProtId = Integer.valueOf(crfNotPatProtId);
        }
    }

    @Column(name = "CRFNOT_GLOBALFLAG")
    public String getCrfNotGlobalFlag() {
        return this.crfNotGlobalFlag;
    }

    public void setCrfNotGlobalFlag(String crfNotGlobalFlag) {
        this.crfNotGlobalFlag = crfNotGlobalFlag;
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return Modified By
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modified
     *            by
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // / END OF GETTER SETTER METHODS

    /**
     * returns the state holder object associated with this crfNotify
     */

    /*
     * public CrfNotifyStateKeeper getCrfNotifyStateKeeper() {
     * Rlog.debug("crfNotify", "CrfNotifyBean.getCrfNotifyStateKeeper"); return
     * new CrfNotifyStateKeeper(getCrfNotifyId(), getCrfNotifyCrfId(),
     * getCrfNotifyCodelstNotStatId(), getCrfNotifyUsersTo(),
     * getCrfNotifyNotes(), getCrfNotStudyId(), getCrfNotProtocolId(),
     * getCrfNotPatProtId(), getCrfNotGlobalFlag(), getCreator(),
     * getModifiedBy(), getIpAdd()); }
     */

    /**
     * sets up a state keeper containing details of the crfNotify
     * 
     * @param edsk
     */

    /*
     * public void setCrfNotifyStateKeeper(CrfNotifyBean usk) { GenerateId cfId =
     * null;
     * 
     * try { Connection conn = null; conn = getConnection(); Rlog .debug("crf",
     * "CrfBean.setCrfStateKeeper() Connection :" + conn); crfNotifyId =
     * cfId.getId("SEQ_SCH_CRFNOTIFY", conn); conn.close();
     * 
     * setCrfNotifyId(crfNotifyId); setCrfNotifyCrfId(usk.getCrfNotifyCrfId());
     * setCrfNotifyCodelstNotStatId(usk.getCrfNotifyCodelstNotStatId());
     * setCrfNotifyUsersTo(usk.getCrfNotifyUsersTo());
     * setCrfNotifyNotes(usk.getCrfNotifyNotes());
     * setCrfNotStudyId(usk.getCrfNotStudyId());
     * setCrfNotProtocolId(usk.getCrfNotProtocolId());
     * setCrfNotPatProtId(usk.getCrfNotPatProtId());
     * setCrfNotGlobalFlag(usk.getCrfNotGlobalFlag());
     * setCreator(usk.getCreator()); setModifiedBy(usk.getModifiedBy());
     * setIpAdd(usk.getIpAdd());
     * 
     * Rlog.debug("crfNotify", "CrfBean.setCrfNotifyStateKeeper() crfNotifyId :" +
     * crfNotifyId); } catch (Exception e) { Rlog.fatal("crfNotify", "Error in
     * setCrfNotifyStateKeeper() in crfNotifyId " + e); } }
     */

    /**
     * updates the crfNotify details
     * 
     * @param edsk
     * @return 0 if successful; -2 for exception
     */
    public int updateCrfNotify(CrfNotifyBean edsk) {
        try {

            setCrfNotifyId(edsk.getCrfNotifyId());
            setCrfNotifyCrfId(edsk.getCrfNotifyCrfId());
            setCrfNotifyCodelstNotStatId(edsk.getCrfNotifyCodelstNotStatId());
            setCrfNotifyUsersTo(edsk.getCrfNotifyUsersTo());
            setCrfNotifyNotes(edsk.getCrfNotifyNotes());
            setCrfNotStudyId(edsk.getCrfNotStudyId());
            setCrfNotProtocolId(edsk.getCrfNotProtocolId());
            setCrfNotPatProtId(edsk.getCrfNotPatProtId());
            setCrfNotGlobalFlag(edsk.getCrfNotGlobalFlag());
            setCreator(edsk.getCreator());
            setModifiedBy(edsk.getModifiedBy());
            setIpAdd(edsk.getIpAdd());

            return 0;
        } catch (Exception e) {
            Rlog.fatal("crfNotify",
                    " error in CrfNotifyBean.updateCrfNotify : " + e);
            return -2;
        }
    }

    public CrfNotifyBean(int crfNotifyId, String crfNotifyCrfId,
            String crfNotifyCodelstNotStatId, String crfNotifyUsersTo,
            String crfNotifyNotes, String crfNotStudyId,
            String crfNotProtocolId, String crfNotPatProtId,
            String crfNotGlobalFlag, String creator, String modifiedBy,
            String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setCrfNotifyId(crfNotifyId);
        setCrfNotifyCrfId(crfNotifyCrfId);
        setCrfNotifyCodelstNotStatId(crfNotifyCodelstNotStatId);
        setCrfNotifyUsersTo(crfNotifyUsersTo);
        setCrfNotifyNotes(crfNotifyNotes);
        setCrfNotStudyId(crfNotStudyId);
        setCrfNotProtocolId(crfNotProtocolId);
        setCrfNotPatProtId(crfNotPatProtId);
        setCrfNotGlobalFlag(crfNotGlobalFlag);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

}// end of class
