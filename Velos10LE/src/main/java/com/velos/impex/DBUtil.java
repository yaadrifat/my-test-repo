/**
 * __astcwz_cmpnt_det: Author: Sonia Sahni Date and Time Created: Fri Jan 30
 * 14:11:35 IST 2004 Note: DO NOT DELETE OR MODIFY KONESA CODEWIZARD COMMENTS
 * STARTING WITH ///__astcwz_ and /**__astcwz_
 */
// /__astcwz_model_idt#(0)
// /__astcwz_maxim_idt#(2037)
// /__astcwz_packg_idt#(1046,1047,1048~,,)
package com.velos.impex;

import java.io.OutputStream;
import java.io.Writer;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.velos.eres.business.common.CommonDAO;

import oracle.jdbc.driver.OraclePreparedStatement;
import oracle.jdbc.driver.OracleResultSet;
import oracle.jdbc.driver.OracleStatement;
import oracle.jdbc.driver.OracleTypes;
import oracle.sql.BLOB;
import oracle.sql.CLOB;

// /__astcwz_class_idt#(1096!n)
public class DBUtil {
    // /__astcwz_opern_idt#(1097)
    public static byte[] convertBlobToByteArray(oracle.sql.BLOB argBlob) {
        try {
            int index = 1;
            int count = 30000;
            byte[] byteArrayData = null;
            int iLength = (int) argBlob.length();
            int iCopyStart = 0;
            byteArrayData = new byte[iLength];

            if (iLength > count) {
                while (iLength > 0) {
                    byte[] bTempbyteArrayData = argBlob.getBytes(index, count);
                    java.lang.System.arraycopy(bTempbyteArrayData, 0,
                            byteArrayData, iCopyStart,
                            bTempbyteArrayData.length);
                    bTempbyteArrayData = null;
                    index = count + index;
                    iLength = iLength - count;
                    iCopyStart += count;
                }
            } else {
                byteArrayData = argBlob.getBytes(1, iLength);
            }
            return byteArrayData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // /__astcwz_opern_idt#(1099)
    public boolean UpdateClobData(Connection argConn, String argTableName,
            String argPrimaryKeyName, int primarykeyid, int modelid, HashMap map) {
        try {
            boolean bCommitFlag = true;
            if (map.size() > 0) {
                Iterator it = map.keySet().iterator();
                while (it.hasNext()) {
                    String columnname = (String) it.next();
                    String argClobText = (String) map.get(columnname);
                    if (argClobText.trim().length() == 0)
                        continue;

                    PreparedStatement oPreparedStatement = null;
                    OracleStatement oOracleStatement = null;
                    try {
                        String sClobSelectQuery = " SELECT " + columnname
                                + " FROM " + argTableName + " WHERE "
                                + argPrimaryKeyName + " = " + primarykeyid
                                + " AND MODELID		= " + modelid + " FOR UPDATE ";

                        String sClobUpdateQuery = " UPDATE " + argTableName
                                + " SET " + columnname + " = ?" + " WHERE "
                                + argPrimaryKeyName + " = " + primarykeyid
                                + " AND MODELID		= " + modelid;

                        // Update Operation
                        oPreparedStatement = (PreparedStatement) argConn
                                .prepareStatement(sClobUpdateQuery);
                        oOracleStatement = (OracleStatement) argConn
                                .createStatement();

                        if (oOracleStatement != null) {
                            OracleResultSet oOracleResultSet = (OracleResultSet) oOracleStatement
                                    .executeQuery(sClobSelectQuery);

                            if (oOracleResultSet != null) {
                                oracle.sql.CLOB oclob = null;
                                while (oOracleResultSet.next())
                                    oclob = oOracleResultSet.getCLOB(1);

                                if (oclob != null) {
                                    StringBuffer sb = new StringBuffer(
                                            argClobText);
                                    int ilength = argClobText.length();
                                    int iBufferPos = 0;
                                    int ichunkSize = 2048; // Constants.MAX_CLOB_LENGTH;
                                    // //(2048)Reason
                                    // has to be find
                                    // out for this size
                                    int iclobpos = 1;
                                    int icounter = 0;

                                    char[] dst = null;
                                    while (ilength > 0) {
                                        if (ilength < ichunkSize) {
                                            dst = new char[ilength];
                                            sb.getChars(iBufferPos, iBufferPos
                                                    + ilength, dst, 0);
                                        } else {
                                            dst = new char[ichunkSize];
                                            sb.getChars(iBufferPos, iBufferPos
                                                    + ichunkSize, dst, 0);
                                            icounter++;
                                        }

                                        oclob.putChars((long) iclobpos, dst);
                                        iBufferPos = iBufferPos + ichunkSize;
                                        iclobpos = iclobpos + ichunkSize;
                                        ilength = ilength - ichunkSize;
                                    }
                                    oPreparedStatement.setClob(1, (Clob) oclob);
                                    oPreparedStatement.execute();
                                } else {
                                    bCommitFlag = false;
                                }
                                oOracleResultSet.close();
                            } else {
                                bCommitFlag = false;
                            }
                            oPreparedStatement.close();
                            oOracleStatement.close();
                        } else {
                            bCommitFlag = false;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        return false;
                    }
                }
            }
            return bCommitFlag;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    // /__astcwz_opern_idt#(1100)
    public boolean UpdateBlobData(Connection argConn, String argTableName,
            String argPrimaryKeyName, int primarykeyid, int modelid, HashMap map) {
        OraclePreparedStatement oPreparedStatement = null;
        OracleStatement oOracleStatement = null;

        try {
            boolean bCommitFlag = true;
            if (map.size() > 0) {
                Iterator it = map.keySet().iterator();
                while (it.hasNext()) {
                    try {
                        String columnname = (String) it.next();
                        byte[] columnvalue = (byte[]) map.get(columnname);
                        String sBlobSelectQuery = " SELECT " + columnname
                                + " FROM " + argTableName + " WHERE "
                                + argPrimaryKeyName + " = " + primarykeyid
                                + " AND MODELID		= " + modelid + " FOR UPDATE ";

                        String sBlobUpdateQuery = " UPDATE " + argTableName
                                + " SET " + columnname + " = ?" + " WHERE "
                                + argPrimaryKeyName + " = " + primarykeyid
                                + " AND MODELID		= " + modelid;

                        oPreparedStatement = (OraclePreparedStatement) argConn
                                .prepareStatement(sBlobUpdateQuery);
                        oOracleStatement = (OracleStatement) argConn
                                .createStatement();

                        if (oOracleStatement != null) {
                            OracleResultSet oOracleResultSet = (OracleResultSet) oOracleStatement
                                    .executeQuery(sBlobSelectQuery);

                            if (oOracleResultSet != null) {
                                oracle.sql.BLOB oblob = null;
                                while (oOracleResultSet.next())
                                    oblob = oOracleResultSet.getBLOB(1);

                                if (oblob != null) {
                                    int ilength = columnvalue.length;
                                    int iBufferPos = 0;
                                    int ichunkSize = 2048; // Reason has to be
                                    // find out for this
                                    // size
                                    int iblobpos = 1;
                                    int icounter = 0;

                                    byte[] dst = null;

                                    while (ilength > 0) {
                                        if (ilength < ichunkSize) {
                                            dst = new byte[ilength];
                                            java.lang.System.arraycopy(
                                                    columnvalue, iBufferPos,
                                                    dst, 0, ilength);
                                        } else {
                                            dst = new byte[ichunkSize];
                                            java.lang.System.arraycopy(
                                                    columnvalue, iBufferPos,
                                                    dst, 0, ichunkSize);
                                            icounter++;
                                        }

                                        oblob.putBytes((long) iblobpos, dst);
                                        iBufferPos = iBufferPos + ichunkSize;
                                        iblobpos = iblobpos + ichunkSize;
                                        ilength = ilength - ichunkSize;
                                    }
                                    oPreparedStatement.setBlob(1, oblob);
                                    oPreparedStatement.execute();
                                } else {
                                    bCommitFlag = false;
                                }
                                oOracleResultSet.close();
                            } else {
                                bCommitFlag = false;
                            }
                            oPreparedStatement.close();
                            oOracleStatement.close();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        bCommitFlag = false;
                    }
                }
            } else {
            }

            return bCommitFlag;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    // /__astcwz_opern_idt#(1101)
    public static String convertClobToString(Clob argClob) {
        try {
            int index = 1;
            int count = 16000;
            String sTagText = "";
            int iLength = (int) argClob.length();

            if (iLength <= 0)
                return "";

            if (iLength > count) {
                StringBuffer sbTagText = new StringBuffer();
                while (iLength > 0) {
                    if (iLength < count)
                        count = iLength;

                    sbTagText.append(argClob.getSubString(index, count));
                    index = count + index;
                    iLength = iLength - count;
                }
                sTagText = sbTagText.toString();
                sbTagText = null;
            } else {
                sTagText = argClob.getSubString(1, iLength);
            }
            return sTagText;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    // /__astcwz_opern_idt#(2037)

    public static int createDBObject(Connection conn, String objectType,
            String objectName, String ddlStatement) {
        /* Excecutes the DDL statement to create an object */

        CallableStatement cstmt = null;
        int intOUT = -1;

        try {

            DBLogger.log2DB("debug", "MSG",
                    "IMPEX --> DBUtil.createDBObject started .. ");

            cstmt = conn
                    .prepareCall("{call pkg_impexcommon.SP_CREATE_DB_OBJECT( ? , ? , ? ,?)} ");

            cstmt.setString(1, objectType);
            cstmt.setString(2, objectName);
            cstmt.setString(3, ddlStatement);
            cstmt.registerOutParameter(4, OracleTypes.INTEGER);

            // execute the procedure after setting the input and output
            // parameters
            cstmt.execute();

            DBLogger.log2DB("debug", "MSG",
                    "IMPEX --> DBUtil.createDBObject : procedure executed");

            // if there are output parameters, read the parameters
            intOUT = cstmt.getInt(4);
            return intOUT;

        }// end of try
        catch (Exception ex) {
            DBLogger.log2DB("fatal", "Exception",
                    "IMPEX --> exception in DBUtil.createDBObject: "
                            + ex.toString());
            return intOUT;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
        }
    }

    /*
     * This method creates BLOB object using temporary blob and returns the
     * same.
     */
    public static BLOB getBLOB(Object blobData, java.sql.Connection conn)
            throws Exception {
        BLOB tempBlob = null;
        // Connection conn2 = null;
        // CommonDAO cDao = new CommonDAO();

        try {

            // conn2 = cDao.getConnection();

            // DBLogger.log2DB ("debug", "MSG", "in IMPEX DBUtil.getBLOB() -->
            // Method started");
            // create a new temporary BLOB
            tempBlob = oracle.sql.BLOB.createTemporary(conn, true,
                    BLOB.DURATION_SESSION);

            // DBLogger.log2DB ("debug", "MSG", "in IMPEX DBUtil.getBLOB() -->
            // Got temporary blob");

            // Open the temporary BLOB in readwrite mode to enable writing
            tempBlob.open(BLOB.MODE_READWRITE);

            // Get the output stream to write
            OutputStream out = tempBlob.getBinaryOutputStream();

            // Write the data into the temporary BLOB

            out.write((byte[]) blobData);
            out.flush();
            out.close();

            DBLogger.log2DB("debug", "MSG",
                    "in IMPEX DBUtil.getBLOB() --> written the temp blob");

            // Close the temporary CLOB
            tempBlob.close();
            DBLogger.log2DB("debug", "MSG",
                    "in IMPEX DBUtil.getBLOB() --> size of temp blob = "
                            + tempBlob.length());

        } catch (Exception exp) {
            tempBlob.freeTemporary();
            DBLogger
                    .log2DB("fatal", "Exception",
                            "Exception in IMPEX DBUtil.getBLOB() --> "
                                    + exp.toString());
            return null;

        } finally {
            /*
             * if (conn2 != null) { conn2.close(); }
             */
        }
        return tempBlob;
    }

    /*
     * This method creates CLOB object using temporary clob and returns the
     * same.
     */
    public static CLOB getCLOB(String clobData, java.sql.Connection conn)
            throws Exception {
        CLOB tempClob = null;
        CommonDAO cDao=new CommonDAO();
        
                   return cDao.getCLOB(clobData,conn);
    }

    public static int executeDynamicSQL(Connection conn, String dynSQL,
            ArrayList arPlaceHolderDatatypes, ArrayList arData) {

        PreparedStatement stmt = null;
        int returnVal = -1;
        int rowCount = 0;

        int totalRows = 0;
        int placeHolderCount = 0;
        ArrayList arDataValues = null;
        String phType = null;
        Object dataValueObj = new Object();
        String dataValueString;
        BLOB blValue = null;
        CLOB clValue = null;
        int updateStatus = 0;

        try {
            stmt = conn.prepareStatement(dynSQL);

            // statement prepared ; execute this statement for each row.

            totalRows = arData.size(); // total rows to be inserted/updated

            placeHolderCount = arPlaceHolderDatatypes.size();

            for (int ctr = 0; ctr < totalRows; ctr++) {

                arDataValues = new ArrayList();

                arDataValues = (ArrayList) arData.get(ctr);

                // got the arDataValues

                // iterate through the placeholder arraylist to determine the
                // data type for each object in arDataValues
                
                //check if connection is null, get it again
                
                if (conn == null)
                {
                	ModuleData md = new ModuleData();
                	int attct = 0;
                	while(conn==null && attct < 11)
                	{
                		attct++;
                		
                		System.out.println("Trying to get db connection...");
                	   
                	    
                		conn = md.getConnectionForImport();
                   	}	
                }

                for (int dataCounter = 0; dataCounter < placeHolderCount; dataCounter++) {
                    // get datatype of the placeholder
                    phType = (String) arPlaceHolderDatatypes.get(dataCounter);

                    // DBLogger.log2DB ("debug", "MSG", "IMPEX -->
                    // DBUtil.executeDynamicSQL : phType " + phType);

                    if (phType.equalsIgnoreCase("BLOB")) {
                        dataValueObj = (Object) arDataValues.get(dataCounter);

                        blValue = null;

                        if (dataValueObj != null) {
                            blValue = getBLOB(dataValueObj, conn);

                        }

                        stmt.setBlob(dataCounter + 1, blValue);

                    } else if (phType.equalsIgnoreCase("CLOB")) {
                        dataValueString = (String) arDataValues
                                .get(dataCounter);
                        clValue = null;
                        clValue = getCLOB(dataValueString, conn);

                        stmt.setClob(dataCounter + 1, clValue);
                    } else {
                        // for phType.equalsIgnoreCase("VARCHAR2") ||
                        // phType.equalsIgnoreCase("NUMBER") ||
                        // phType.equalsIgnoreCase("CHAR") ||
                        // phType.equalsIgnoreCase("LONG") ||
                        // phType.equalsIgnoreCase("DATE")

                        dataValueString = (String) arDataValues
                                .get(dataCounter);

                        if (dataValueString.equals("NULL")) {
                            dataValueString = null;
                        }

                        // DBLogger.log2DB ("debug", "MSG", "IMPEX -->
                        // DBUtil.executeDynamicSQL : dataValueString " +
                        // dataValueString);

                        if (phType.equalsIgnoreCase("DATE") || phType.equalsIgnoreCase("TIMESTAMP")) {
                            java.sql.Timestamp ts = null;
                            if ((dataValueString != null)
                                    && dataValueString.length() > 0) {
                                ts = Timestamp.valueOf(dataValueString);
                            }
                            stmt.setTimestamp(dataCounter + 1, ts);

                        } else {
                            stmt.setString(dataCounter + 1, dataValueString);
                        }

                    }

                } // end of for - for placeholder loop

                updateStatus = 0;

                try {

                    updateStatus = stmt.executeUpdate();
                    
                   // DBLogger.log2DB("debug", "MSG","IMPEX --> DBUtil.executeDynamicSQL : Row Inserted "
                    //                + (ctr + 1));
                    System.out.println("IMPEX --> DBUtil.executeDynamicSQL : Row Inserted "  + (ctr + 1) );
                    
                } catch (SQLException sqlEx) {
                    DBLogger.log2DB("fatal", "Exception",
                            "SQLException in IMPEX DBUtil.executeDynamicSQL() --> "
                                    + sqlEx.toString()
                                    + " Could not process row " + (ctr + 1));
                }

                //DBLogger.log2DB("debug", "MSG",
                //      "IMPEX --> DBUtil.executeDynamicSQL : updateStatus "
                //            + updateStatus);
                

                if (updateStatus > 0) {
                    rowCount++;
                } else {
                    DBLogger.log2DB("fatal", "Exception",
                            "Exception in IMPEX DBUtil.executeDynamicSQL() --> Could not process row "
                                    + (ctr + 1));
                }
                
                conn.commit();

            } // end of for - for data rows
            returnVal = rowCount;
            conn.commit();

        } catch (Exception exp) {
            DBLogger.log2DB("fatal", "Exception",
                    "Exception in IMPEX DBUtil.executeDynamicSQL() --> "
                            + exp.toString());
            returnVal = -1;
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

        return returnVal;

    }

    // end of class

    // /////////////
    /**
     * executes multiple SQL statements in a batch. The method accepts a
     * connection object as an argument. It does not closes the connection. The
     * caller needs to close the connection explicitly
     * 
     * @param conn
     *            The connection object using which the SQLs needs to be
     *            executed
     * @param arSQL
     *            An ArrayList object containing SQL
     * @return int[] Returns an array of update counts. The int elements of the
     *         array that is returned are ordered to correspond to the commands
     *         in the batch, which are ordered according to the order in which
     *         they were added to the batch. The elements in the array returned
     *         by the method executeBatch may be one of the following: <BR>
     *         1. A number greater than or equal to zero -- indicates that the
     *         command was processed successfully and is an update count giving
     *         the number of rows in the database that were affected by the
     *         command's execution <BR>
     *         2. A value of SUCCESS_NO_INFO -- indicates that the command was
     *         processed successfully but that the number of rows affected is
     *         unknown
     */
    public static int[] executeBatch(Connection conn, ArrayList arSQL) {

        Statement stmtBatch = null;
        int ret[] = null;
        try {

            stmtBatch = conn.createStatement();

            for (int i = 0; i < arSQL.size(); i++) {
                String sqlStr = "";
                sqlStr = (String) arSQL.get(i);
                stmtBatch.addBatch(sqlStr);

            }
            ret = stmtBatch.executeBatch();
            return ret;

        } catch (SQLException sqlEx) {
            DBLogger.log2DB("fatal", "Exception",
                    "SQLException in DBUtil.executeBatch --> "
                            + sqlEx.toString());
            return ret;
        } finally {
            try {
                if (stmtBatch != null)
                    stmtBatch.close();
            } catch (Exception e) {
            }

        }

        // end of method
    }

    // /////////////

}
