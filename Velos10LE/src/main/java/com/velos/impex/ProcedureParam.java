/**
 * __astcwz_cmpnt_det: Author: Sonia Sahni Date and Time Created: Thu Jan 29
 * 11:42:53 IST 2004 Note: DO NOT DELETE OR MODIFY KONESA CODEWIZARD COMMENTS
 * STARTING WITH ///__astcwz_ and /**__astcwz_
 */
// /__astcwz_model_idt#(0)
// /__astcwz_maxim_idt#(2031)
// /__astcwz_packg_idt#(1046,1047,1048~,,)
package com.velos.impex;

import java.util.ArrayList;

// /__astcwz_class_idt#(2022!n)
public class ProcedureParam implements java.io.Serializable {
    // /__astcwz_attrb_idt#(2023)
    private ArrayList paramSequence;

    // /__astcwz_attrb_idt#(2024)
    private ArrayList paramDataType;

    // /__astcwz_attrb_idt#(2025)
    private ArrayList paramType;

    private int outParamCount;

    private int inParamCount;

    // /__astcwz_default_constructor
    ProcedureParam() {
        paramSequence = new ArrayList();
        paramDataType = new ArrayList();
        paramType = new ArrayList();
    }

    // /__astcwz_opern_idt#()
    public ArrayList getParamSequence() {
        return paramSequence;
    }

    // /__astcwz_opern_idt#()
    public ArrayList getParamDataType() {
        return paramDataType;
    }

    // /__astcwz_opern_idt#()
    public ArrayList getParamType() {
        return paramType;
    }

    public String getParamType(int index) {
        return (String) paramType.get(index);
    }

    public String getParamDataType(int index) {
        return (String) paramDataType.get(index);
    }

    public int getParamSequence(int index) {
        Integer seq;
        seq = (Integer) paramSequence.get(index);
        return seq.intValue();
    }

    // /__astcwz_opern_idt#()
    public void setParamSequence(ArrayList aparamSequence) {
        paramSequence = aparamSequence;
    }

    // /__astcwz_opern_idt#()
    public void setParamDataType(ArrayList aparamDataType) {
        paramDataType = aparamDataType;
    }

    // /__astcwz_opern_idt#()
    public void setParamType(ArrayList aparamType) {
        paramType = aparamType;
    }

    public void setParamType(String aparamType) {
        paramType.add(aparamType);
    }

    public void setParamDataType(String aparamDataType) {
        paramDataType.add(aparamDataType);
    }

    public void setParamSequence(Integer aparamSequence) {
        paramSequence.add(aparamSequence);
    }

    public int getInParamCount() {
        return this.inParamCount;
    }

    public void setInParamCount(int inParamCount) {
        this.inParamCount = inParamCount;
    }

    public int getOutParamCount() {
        return this.outParamCount;
    }

    public void setOutParamCount(int outParamCount) {
        this.outParamCount = outParamCount;
    }

}
