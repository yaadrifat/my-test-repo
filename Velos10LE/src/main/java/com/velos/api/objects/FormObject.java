package com.velos.api.objects;

import java.util.ArrayList;

import com.velos.api.execption.APIException;
import com.velos.api.execption.MissingValueException;
import com.velos.api.execption.NoAccessException;
import com.velos.eres.business.common.DynRepDao;
import com.velos.eres.business.saveForm.SaveFormStateKeeper;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;

/**
 * @author vishal
 *
 */

public class FormObject implements java.io.Serializable {

	/**
	 * StateKeeper object for the form response
	 */
	private SaveFormStateKeeper stateKeeper;

	/**
	 * ArrayList of 'System IDs' of all the fields in the form. The data for a field needs to be mapped to its System ID.<br>
	 * Use getFormFieldNames to get the field names and map the System Ids in teh same sequence
	 */
	private ArrayList formFieldSysIds;

	/**
	 * ArrayList of all the fields in the form
	 */
	private ArrayList formFieldNames;

	/**
	 * ArrayList of data types of all the fields in the form. Possible values are: <br>
	 * ET - Edit Box, Text <br>
	 * EN - Edit Box, Number <br>
	 * ED - Edit Box, Date <br>
	 * MD - Multiple Choice, Dropdown<br>
	 * MR - Multiple Choice, Radio Button<br>
	 * MC - Multiple Choice, checkboxes<br>
	 */
	
	private ArrayList formFieldDataTypes;

	/**
	 * Default Constructor 
	 */
	public FormObject() {
		stateKeeper = new SaveFormStateKeeper();
		formFieldSysIds = new ArrayList();
		formFieldNames = new ArrayList();
		formFieldDataTypes = new ArrayList();
	}

	/**Returns account Id
	 * @return Account Id
	 */
	public String getAccountId() {
		return stateKeeper.getAccountId();
	}

	/**sets account Id
	 * @param accountId
	 */
	public void setAccountId(String accountId) {
		stateKeeper.setAccountId(accountId);
	}

	/** returns the Link Type code for the form. Possible values are:<br>
	 * 
	 * A - Account form <br>
	 * S - Study Form <br>
	 * SA - All studies form <br>
	 * SP - Specific Study Patient form <br> 
     * PA - ALL patients form <br>
     * PS - All Studies Patient form <br>
     * PR - ALL Studies Patient form (RESTRICTED) <br>
	 * @return
	 */
	public String getFormLinkType() {
		return stateKeeper.getDisplayType();
	}

	/** sets the LInk Type code for the form. Possible values are:<br>
	 * 
	 * A - Account form <br>
	 * S - Study Form <br>
	 * SA - All studies form <br>
	 * SP - Specific Study Patient form <br> 
     * PA - ALL patients form <br>
     * PS - All Studies Patient form <br>
     * PR - ALL Studies Patient form (RESTRICTED) <br>
	 * @param displayType
	 */
	public void setFormLinkType(String displayType) {
		if (displayType.equals("PS") || displayType.equals("PR")) {
			displayType = "SP";
		}
		stateKeeper.setDisplayType(displayType);
	}

	/** gets the Creator of the form response. This ID maps to the primary key of the table ER_USER : PK_USER
	 * @return creator of the form
	 */
	public String getCreator() {
		return stateKeeper.getCreator();
	}

	/** sets the Creator of the form response. This ID maps to the primary key of the table ER_USER : PK_USER
	 * @param creator
	 */
	public void setCreator(String creator) {
		stateKeeper.setCreator(creator);
	}

	/** gets the Status of the Form Response. Maps to the primary key of table ER_CODELIST : PK_CODELST. The code_type for <br>
	 * form status is 'fillformstat' 
	 * @return codelist value for the selected Form status
	 */
	public String getFormCompleted() {
		return stateKeeper.getFormCompleted();
	}

	/**  sets the Status of the Form Response. Maps to the primary key of table ER_CODELIST : PK_CODELST. The code_type for <br>
	 * form status is 'fillformstat'
	 * @param formCompleted
	 */
	public void setFormCompleted(String formCompleted) {
		stateKeeper.setFormCompleted(formCompleted);
	}

	/** gets the form response's 'Data Entry Date' 
	 * @return Form's Data Entry Date
	 */
	public String getFormFillDate() {
		return stateKeeper.getFormFillDate();
	}

	/** sets the form response's 'Data Entry Date'
	 * @param formFillDate
	 */
	public void setFormFillDate(String formFillDate) {
		stateKeeper.setFormFillDate(formFillDate);
	}

	/** gets the form's Primary Key. Maps to the primary key of table ER_FORMLIB : PK_FORMLIB. <br> This key is important to identify the form.
	 * @return Form Id
	 */
	public String getFormId() {
		return stateKeeper.getFormId();
	}

	/** sets the form's Primary Key. Maps to the primary key of table ER_FORMLIB : PK_FORMLIB. <br> This key is important to identify the form.
	 * @param formId
	 */
	public void setFormId(String formId) throws NoAccessException {
		if ((EJBUtil.stringToNum(formId)!=2047)|| (EJBUtil.stringToNum(formId)!=2049))
		{
			throw new NoAccessException("You do not have API Access to this Form");
	
		}
		stateKeeper.setFormId(formId);
	}

	/** Loads the form's definition in the FormObject's object. The method works for the specified form PK.<br>
	 * Before calling this method, please set the value of form PK by calling method setFormId  
	 * @throws MissingValueException
	 * @throws APIException
	 */
	public void loadFormDefinition() throws MissingValueException, APIException {
		if (EJBUtil.isEmpty(this.getFormId()))
			throw new MissingValueException(
					"Form AbstractIdentifier is not set.Form AbstractIdentifier must be initialized");
		DynRepDao dynDao = new DynRepDao();
		dynDao.getFormDefinition(EJBUtil.stringToNum(this.getFormId()));
		if ((dynDao.getMapColIds() == null)
				&& (dynDao.getMapColIds().size() == 0))
			throw new APIException("Error in Loading Form Definition.");
		this.setFormLinkType(dynDao.getFormLinkedType().trim());
		this.setFormFieldSysIds(dynDao.getMapSysIds());
		this.setFormFieldNames(dynDao.getMapDispNames());
		this.setFormFieldDataTypes(dynDao.getMapColTypes());
	}

	/** returns the form XML
	 * @return XML generated for the Form
	 */
	public String getFormXML() {
		return stateKeeper.getFormXML();
	}

	/** returns the primary key of the form response. The value maps to the primary of the respective form Response table. <br>
	 * Patient Forms - ER_PATFORMS : PK_PATFORMS<br>
	 * Study Forms - ER_STUDYFORMS : PK_STUDYFORMS<br>
	 * Account Forms - ER_ACCTFORMS : PK_ACCTFORMS<br>
	 * @return Primary Key generated for the Response
	 */
	public int getFilledFormId() {
		return stateKeeper.getId();
	}

	/** sets the primary key of the form response. The value maps to the primary of the respective form Response table. <br>
	 * Patient Forms - ER_PATFORMS : PK_PATFORMS<br>
	 * Study Forms - ER_STUDYFORMS : PK_STUDYFORMS<br>
	 * Account Forms - ER_ACCTFORMS : PK_ACCTFORMS<br>
	 * @param id
	 */
	public void setFilledFormId(int id) {
		stateKeeper.setId(id);
	}

	/** gets IP Address of the user who created/modified the form response 
	 * @return
	 */
	public String getIpAdd() {
		return stateKeeper.getIpAdd();
	}

	/** sets IP Address of the user who created/modified the form response
	 * @param ipAdd
	 */
	public void setIpAdd(String ipAdd) {
		stateKeeper.setIpAdd(ipAdd);
	}

	/** gets the ID of the user who last modified the form response. This ID maps to the primary key of the table ER_USER : PK_USER 
	 * @return Velos UserId
	 */
	public String getLastModifiedBy() {
		return stateKeeper.getLastModifiedBy();
	}

	/** sets the ID of the user who last modified the form response. This ID maps to the primary key of the table ER_USER : PK_USER
	 * @param lastModifiedBy
	 */
	public void setLastModifiedBy(String lastModifiedBy) {
		stateKeeper.setLastModifiedBy(lastModifiedBy);
	}

	/** gets the date when the form's response was modified
	 * @returnModify date for the form 
	 */
	public String getLastModifiedDate() {
		return stateKeeper.getLastModifiedDate();
	}

	/** sets the date when the form's response was modified
	 * @param lastModifiedDate
	 */
	public void setLastModifiedDate(String lastModifiedDate) {
		stateKeeper.setLastModifiedDate(lastModifiedDate);
	}

	/** sets the value for a field. 
	 * @param fldId field's System ID
	 * @param value field's value
	 * 
	 * Use getFormFieldSysIds and getFormFieldNames to identify the correct System ID & Name  for a field 
		
	 */
	public void setFieldValue(String fldId, String value) {
		stateKeeper.setParams(fldId);
		stateKeeper.setParamValues(value);
	}

	/** sets the value for a 'multiple choice-check box' field.
	 * @param fldId field's System ID
	 * @param value field's value
	 * @param seperator separator String to separate the check box responses
	 * In case of checkbox field, pass the data values of all the checkbox options selected separated by a separator.<br>
	 * For example, if two check boxes for a field are checked, pass a string like: red|blue, where red and blue <br>
	 * are the data values of the check box options and '|' is the separator.  
	 */
	public void setFieldValue(String fldId, String value, String seperator) {
		String valueStr = "";
		if (value.indexOf(seperator) >= 0) {
			String[] valArray = StringUtil.strSplit(value, seperator, false);
			if (valArray != null) {
				for (int i = 0; i < valArray.length; i++) {
					if (valueStr.length() > 0)
						valueStr = valueStr + "[VELCOMMA]" + valArray[i];
					else
						valueStr = valArray[i];
				}
			}
		}
		stateKeeper.setParams(fldId);
		if (valueStr.length() > 0)
			stateKeeper.setParamValues(valueStr);
		else
			stateKeeper.setParamValues(value);
	}

	/** returns an ArrayList of System IDs of all the fields in the form
	 * @return Return List of all the Field system Ids for the form
	 */
	public ArrayList getFieldIds() {
		return stateKeeper.getParams();
	}

	/** sets the field Attributes' pairs
	 * @param fldIds ArrayList of System IDs of all the fields of the form
	 * @param fldValues ArrayList of values of all the fields of the form
	 */
	public void setFieldAttributes(ArrayList fldIds, ArrayList fldValues) {
		stateKeeper.setParams(fldIds);
		stateKeeper.setParamValues(fldValues);
	}

	/** returns an ArrayList of values of all the fields in the form
	 * @return
	 */
	public ArrayList getFieldValues() {
		return stateKeeper.getParamValues();
	}

	/** gets the ID of the patient for which the form response is saved. (only in case of patient related forms)<br>
	 * Maps to the Primary Key of table ER_PER : PK_PER
	 * @return Patient Id
	 */
	public String getPatientId() {
		return stateKeeper.getPatientId();
	}

	/** sets the ID of the patient for which the form response is saved. (only in case of patient related forms)<br>
	 * Maps to the Primary Key of table ER_PER : PK_PER
	 * @param Primary Key linked to patient
	 */
	public void setPatientId(String patientId) {
		stateKeeper.setPatientId(patientId);
	}

	/** gets the ID of the patient enrollment for which the form response is saved. (only in case of patient study related forms)<br>
	 * Maps to the Primary Key of table ER_PATPROT : PK_PATPROT
	 * @return Patient Protocol Id
	 */
	public String getPatprotId() {
		return stateKeeper.getPatprotId();
	}

	/** sets the ID of the patient enrollment for which the form response is saved. (only in case of patient study related forms)<br>
	 * Maps to the Primary Key of table ER_PATPROT : PK_PATPROT
	 * @param patprotId
	 */
	public void setPatprotId(String patprotId) {
		stateKeeper.setPatprotId(patprotId);
	}

	/** gets the record type of the form response. Possible Values:<br>
	 * N : New<br>
	 * M : Modified<br>
	 * D : Deleted <br>
	 * @return
	 */
	public String getRecordType() {
		return stateKeeper.getRecordType();
	}

	/** sets the record type of the form response. Possible Values:<br>
	 * N : New<br>
	 * M : Modified<br>
	 * D : Deleted<br>
	 * @param recordType
	 */
	public void setRecordType(String recordType) {
		stateKeeper.setRecordType(recordType);
	}

	/** gets the ID of the study for which the form response is saved. (only in case of study/study patient related forms)<br>
	 * Maps to the Primary Key of table ER_STUDY : PK_STUDY
	 * @return Study Id linked to form
	 */
	public String getStudyId() {
		return stateKeeper.getStudyId();
	}

	/** sets the ID of the study for which the form response is saved. (only in case of study/study patient related forms)<br>
	 * Maps to the Primary Key of table ER_STUDY : PK_STUDY
	 * @param studyId
	 */
	public void setStudyId(String studyId) {
		stateKeeper.setStudyId(studyId);
	}

	/** gets the response's Form Version. Maps to the Primary Key of table ER_FORMLIBVER : PK_FORMLIBVER 
	 * @return
	 */
	public String getFormLibVer() {
		return stateKeeper.getFormLibVer();
	}

	/** gets an ArrayList of the names of all the fields in the form 
	 * @return Return List of all the Field Names for the form
	 */
	public ArrayList getFormFieldNames() {
		return formFieldNames;
	}

	/** sets an ArrayList of the names of all the fields in the form
	 * @param Return List of all the Field system Ids for the form
	 */
	private void setFormFieldNames(ArrayList fieldNames) {
		this.formFieldNames = fieldNames;
	}

	/** gets an ArrayList of the System IDs of all the fields in the form
	 * @return Return List of all the Field system Ids for the form
	 */
	public ArrayList getFormFieldSysIds() {
		return formFieldSysIds;
	}

	/** sets an ArrayList of the System IDs of all the fields in the form
	 * @param sysIds
	 */
	private void setFormFieldSysIds(ArrayList sysIds) {
		this.formFieldSysIds = sysIds;
	}

	/** gets an ArrayList of the Data Types of all the fields in the form. Possible values are: <br>
	 * ET - Edit Box, Text<br>
	 * EN - Edit Box, Number<br>
	 * ED - Edit Box, Date<br>
	 * MD - Multiple Choice, Dropdown<br>
	 * MR - Multiple Choice, Radio Button<br>
	 * MC - Multiple Choice, checkboxes<br>
	 * @return
	 */
	public ArrayList getFormFieldDataTypes() {
		return formFieldDataTypes;
	}

	/** sets an ArrayList of the Data Types of all the fields in the form. Possible values are: <br>
	 * ET - Edit Box, Text<br>
	 * EN - Edit Box, Number<br>
	 * ED - Edit Box, Date<br>
	 * MD - Multiple Choice, Dropdown<br>
	 * MR - Multiple Choice, Radio Button<br>
	 * MC - Multiple Choice, checkboxes<br>
	 * @param formFieldDataTypes
	 */
	private void setFormFieldDataTypes(ArrayList formFieldDataTypes) {
		this.formFieldDataTypes = formFieldDataTypes;
	}

	/** sets the response's Form Version. Maps to the Primary Key of table ER_FORMLIBVER : PK_FORMLIBVER
	 * @param formLibVer
	 */
	public void setFormLibVer(String formLibVer) {
		stateKeeper.setFormLibVer(formLibVer);
	}

	/** gets the StateKeeper object for the form response
	 * @return form StateKeeper Object 
	 */
	public SaveFormStateKeeper getStateKeeper() {
		return this.stateKeeper;
	}
}