package com.velos.api.execption;


import org.apache.log4j.Logger;
/**
 * Represents any problem encountered when a mandatory valus is missing
 * 
 * @author Vishal Abrol
 */
public class MissingValueException extends Exception {
    private static Logger logger = Logger.getLogger(MissingValueException.class
            .getName());

    /**
     * Constructs an instance of <code>ApplicationException</code> with the
     * specified detail message.
     */
    public MissingValueException(String msg) {
        super(msg);
        logger.fatal(msg);
    }
}
