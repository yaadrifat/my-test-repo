package com.velos.api.execption;


import org.apache.log4j.Logger;
/**
 * Represents any problem encountered during processing of an API request
 * @author Vishal Abrol
 */
public class APIException extends Exception {
    private static Logger logger = Logger.getLogger(APIException.class
            .getName());

    /**
     * Constructs an instance of <code>ApplicationException</code> with the
     * specified detail message.
     */
    public APIException(String msg) {
        super(msg);
        logger.fatal(msg);
    }
}
