package com.velos.service; 
 

import com.velos.impex.dispatcher.ReverseA2ADispatcher;

import org.jboss.system.ServiceMBeanSupport;

public class A2AService extends ServiceMBeanSupport implements A2AServiceMBean { 

 
    
  public void init() throws Exception { } 

  public void startService() throws Exception {
        ReverseA2ADispatcher.startDispatcher();
 
  } 
  public void stopService() throws Exception { 
      ReverseA2ADispatcher.stopDispatcher();
 
  } 
   
  } 