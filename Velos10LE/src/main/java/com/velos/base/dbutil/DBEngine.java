package com.velos.base.dbutil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.logicalcobwebs.proxool.ProxoolException;
import org.logicalcobwebs.proxool.ProxoolFacade;
import org.logicalcobwebs.proxool.configuration.PropertyConfigurator;

/**
 * Class acts as a db interface.
 * 
 * @author Vishal Abrol
 */

public class DBEngine {
    private static Logger logger = Logger.getLogger(DBEngine.class.getName());

    private DBEngine() {
        logger.info("DBEngine instantiated at : " + new java.util.Date());
    }

    private static DBEngine INSTANCE = null;

    public static synchronized DBEngine getEngine() {
        if (INSTANCE == null)
            INSTANCE = new DBEngine();
        return INSTANCE;
    }

    private static boolean initialized = false;

    private static String v_alias = null;

    /**
     * Initializes the Proxool database pool.
     * 
     * @param propLoc
     *            the relative path t private static boolean initialized =
     *            false;o the file containing the db strings for jdbc connection
     * @param alias
     *            the connection pool alias
     */
    public synchronized static void initializePool(String propLoc, String alias) {
        if (!initialized) {
            v_alias = alias;
            try {
                PropertyConfigurator.configure(propLoc);
            } catch (ProxoolException ex) {
                logger.fatal("failed to configure database pool!", ex);
            }
            initialized = true;
            logger.info("DBEngine initialized at: " + new java.util.Date());
        } else {
            logger.warn("DBEngine already initialized");
        }
    }

    /**
     * Obtains a single <tt>Connection</tt> from the database pool.
     * <tt>Connection</tt>s obtained in this fashion must manually be
     * returned using <tt>returnConnection(Connection)</tt>.
     * 
     * @return a <tt>Connection</tt> to the database
     */
    public static Connection getConnection() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection("proxool." + v_alias);
        } catch (SQLException ex) {
            logger.error("could not obtain Connection from pool", ex);
            return null;
        }

        if (conn == null) {
            logger.error("Connection from pool is null");
            return null;
        }

        return conn;
    }
    public static Connection getConnection(String l_alias) {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection("proxool." + l_alias);
        } catch (Exception ex) {
            logger.error("could not obtain Connection from pool", ex);
            ex.printStackTrace();
            return null;
        }

        if (conn == null) {
            logger.error("Connection from pool is null");
            return null;
        }

        return conn;
    }

    /**
     * Returns a manually obtained <tt>Connection</tt> to the object pool.
     * 
     * @param conn
     *            the <tt>Connection</tt> to return to the object pool
     */
    public static void returnConnection(Connection conn) {
        if (conn == null) {
            logger.debug("Connection to return is already null");
            return;
        }
        try {
            // this alone will return the connection to the proxool pool
            conn.close();
        } catch (SQLException ex) {
            logger.error("failed to close connection!", ex);
        }
    }
    
    public static Connection getDelegateConnection(Connection conn)
    {   
        
         try {
            return ProxoolFacade.getDelegateConnection(conn) ;
        } catch (ProxoolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Returns a database value retrieved from the database. or returns "error"
     * if there was an error processing request.
     * 
     * @param sql
     *            as <tt>String</tt> to process
     * @param valList
     *            as <tt>Arraylist</tt> contains the bind values for SQL,if
     *            any.
     */
    public static String getDBValue(String sql, ArrayList valList) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String output = null;
        String setValue = "";
        Connection conn = getConnection();
        try {
            pstmt = conn.prepareStatement(sql);
            logger.debug("Processing SQL: " + sql + " with Parameters "
                    + valList);
            for (int i = 0; i < valList.size(); i++) {
                setValue = (String) valList.get(i);
                pstmt.setString(i + 1, setValue);
            }
            rs = pstmt.executeQuery();
            while (rs.next()) {
                output = (rs.getString(1));
            }
            if (output == null)
                output = "";

        } catch (SQLException e) {
            logger.fatal("SQLException processing DB Request " + e
                    + e.getMessage());
            return "error";
        } catch (Exception ex) {
            logger.fatal("Exception processing DB Request " + ex
                    + ex.getMessage());
            return "error";
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
                if (rs != null)
                    rs.close();
                if (conn != null)
                    returnConnection(conn);

            } catch (SQLException e) {
                logger.error("Error in closing the PreparedStatement Object"
                        + e.getMessage());
            }
            try {
                if (rs != null)
                    rs.close();

            } catch (SQLException e) {
                logger.error("Error in closing the ResultStatement Object"
                        + e.getMessage());
            }

        }
        logger.debug("Returning value:  " + output);
        return output;
    }

    /**
     * Returns database update status or returns -1 if there was an error
     * processing request.
     * 
     * @param sql
     *            as <tt>String</tt> to process
     * @param valList
     *            as <tt>Arraylist</tt> contains the bind values for SQL,if
     *            any.
     */

    public static int setDBValue(String sql, ArrayList valList) {
        PreparedStatement pstmt = null;
        int output = -1;
        String setValue = "";
        Connection conn = getConnection();
        try {
            conn.setAutoCommit(false);
            pstmt = conn.prepareStatement(sql);
            logger.debug("Processing SQL: " + sql + " with Parameters "
                    + valList);
            for (int i = 0; i < valList.size(); i++) {
                setValue = (String) valList.get(i);
                logger.debug("Set the valList" + setValue);
                pstmt.setObject(i + 1, setValue);
            }
            output = pstmt.executeUpdate();
            if (output >= 0) {
                logger.debug("Comitting Transaction");
                conn.commit();
            } else
                conn.rollback();
        } catch (SQLException e) {
            logger.fatal("SQLException processing DB Request " + e
                    + e.getMessage());
            return -1;
        } catch (Exception ex) {
            logger.fatal("Exception processing DB Request " + ex
                    + ex.getMessage());
            return -1;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
                if (conn != null)
                    returnConnection(conn);

            } catch (SQLException e) {
                logger.error("Error in closing the PreparedStatement Object"
                        + e.getMessage());
            }

        }
        logger.debug("Returning value:  " + output);
        return output;
    }
    
    /**
     * Returns database update status or returns -1 if there was an error
     * processing request.
     * 
     * @param sql
     *            as <tt>String</tt> to process
     * @param valList
     *            as <tt>Arraylist</tt> contains the bind values for SQL,if
     *            any.
     */

    public static int setDBValue(String sql, ArrayList valList,String l_alias) {
        PreparedStatement pstmt = null;
        int output = -1;
        Object setValue = "";
        Connection conn = getConnection(l_alias);
        try {
            conn.setAutoCommit(false);
            pstmt = conn.prepareStatement(sql);
            logger.debug("Processing SQL: " + sql + " with Parameters "
                    + valList);
            for (int i = 0; i < valList.size(); i++) {
                setValue =  valList.get(i);
                logger.debug("Set the valList" + setValue);
                pstmt.setObject(i + 1, setValue);
            }
            output = pstmt.executeUpdate();
            if (output >= 0) {
                logger.debug("Comitting Transaction");
                conn.commit();
            } else
                conn.rollback();
        } catch (SQLException e) {
            logger.fatal("SQLException processing DB Request " + e
                    + e.getMessage());
            e.printStackTrace();
            return -1;
        } catch (Exception ex) {
            logger.fatal("Exception processing DB Request " + ex
                    + ex.getMessage());
            ex.printStackTrace();
            return -1;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
                if (conn != null)
                    returnConnection(conn);

            } catch (SQLException e) {
                logger.error("Error in closing the PreparedStatement Object"
                        + e.getMessage());
                e.printStackTrace();
            }

        }
        logger.debug("Returning value:  " + output);
        return output;
    }


    public boolean IsInitialized() {
        return initialized;
    }
    

}
