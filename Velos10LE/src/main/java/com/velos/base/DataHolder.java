package com.velos.base;

import java.util.ArrayList;

import com.velos.base.dbutil.DBEngine;
import com.velos.eres.service.util.PatternMatcher;

public class DataHolder {

   private String PROCESS_FILE="";
   private String BAD_FILE="";
   private int INSERT_COUNT=0;
   private int UPDATE_COUNT=0;
   private int BAD_COUNT=0;
   private int MSG_COUNTER=0;
   private int CURRENTID=0;
   private int LASTID=0;
   private String MSG_LEVEL="";
   private int rejectFlag =0;
   private int preCount=0;
   private String processMessage="System is Idle";
   private static DataHolder INSTANCE = null;
   private ArrayList publishData=new ArrayList();
   private PatternMatcher patternMatcher=null;
   private String eventName="";
    






/**
 * @return the event
 */
public String getEventName() {
	return eventName;
}

/**
 * @param event the event to set
 */
public void setEventName(String event) {
	this.eventName = event;
}

private DataHolder(){
    patternMatcher=new PatternMatcher();
      }

public static synchronized DataHolder getDataHolder() {
    if (INSTANCE == null)
        INSTANCE = new DataHolder();
    return INSTANCE;
}

public int getBAD_COUNT() {
    return BAD_COUNT;
}

public void setBAD_COUNT(int bad_count) {
    BAD_COUNT = bad_count;
}

public String getBAD_FILE() {
    return BAD_FILE;
}

public void setBAD_FILE(String bad_file) {
    BAD_FILE = bad_file;
    publish();
}

public int getINSERT_COUNT() {
    return INSERT_COUNT;
}

public void setINSERT_COUNT(int insert_count) {
    INSERT_COUNT = insert_count;
}

public String getPROCESS_FILE() {
    return PROCESS_FILE;
}

public void setPROCESS_FILE(String process_file) {
    PROCESS_FILE = process_file;
    publish();
}

public int getUPDATE_COUNT() {
    return UPDATE_COUNT;
}

public void setUPDATE_COUNT(int update_count) {
    UPDATE_COUNT = update_count;
}

public int getMSG_COUNTER() {
    return MSG_COUNTER;
}

public void setMSG_COUNTER(int msg_counter) {
    MSG_COUNTER = msg_counter;
}


public void addToInsertCount()
{
    INSERT_COUNT=INSERT_COUNT+1;
    publish();
}

public void addToUpdateCount()
{
    UPDATE_COUNT=UPDATE_COUNT+1;
    publish();
}

public void addToBadCount()
{
    BAD_COUNT = BAD_COUNT+1;
    publish();
}

public void addToMsgCount()
{
     MSG_COUNTER = MSG_COUNTER+1;
     publish();
}

public int getCURRENTID() {
    return CURRENTID;
}

public void setCURRENTID(int currentid) {
    CURRENTID = currentid;
}

public int getLASTID() {
    return LASTID;
}

public void setLASTID(int lastid) {
       LASTID = lastid;
}
public String getMSG_LEVEL() {
    return MSG_LEVEL;
}

public void setMSG_LEVEL(String msg_level) {
       MSG_LEVEL = msg_level; 
}
public int getRejectFlag() {
    return rejectFlag;
}

public void setRejectFlag(int rejectFlag) {
        this.rejectFlag = rejectFlag;
}

public int getPreCount() {
    
    return preCount;
}

public void setPreCount(int preCount) {
    publish();
    this.preCount = preCount;
}

public String getProcessMessage() {
    return processMessage;
}

public void setProcessMessage(String processMessage) {
    publish();
    this.processMessage = processMessage;
}

public void appendProcessMessage(String msg) {
    publish();
    this.processMessage = processMessage+"\r\n"+msg;
}

public PatternMatcher getPatternMatcher() {
    return patternMatcher;
}

public void setPatternMatcher(PatternMatcher patternMatcher) {
    this.patternMatcher = patternMatcher;
}

public void reset()
{
 
    PROCESS_FILE="";
    BAD_FILE="";
    INSERT_COUNT=0;
    UPDATE_COUNT=0;
    BAD_COUNT=0;
    MSG_COUNTER=0;
    CURRENTID=0;
    LASTID=0;
    MSG_LEVEL="";
    rejectFlag=0;
    preCount=0;
    this.processMessage="System is Idle";
    this.eventName="";
    publish();
}

public void publish()
{
 String sql="update log_snapshot set processfile=?,badfile=?,badcount=?,insertcount=?,msgcounter=?,precount=?,updatecount=?,processmsg=? where keyword='SNAPSHOT'";
 publishData.clear();
 publishData.add(this.getPROCESS_FILE());
 publishData.add(this.getBAD_FILE());
 publishData.add(this.getBAD_COUNT());
 publishData.add(this.getINSERT_COUNT());
 publishData.add(this.getMSG_COUNTER());
 publishData.add(this.getPreCount());
 publishData.add(this.getUPDATE_COUNT());
 publishData.add(this.getProcessMessage());
 DBEngine.getEngine().setDBValue(sql,publishData,"logger" );  
 

}



}