package com.velos.DHTS;

public class PatientDemographicsSoapProxy implements com.velos.DHTS.PatientDemographicsSoap {
  private String _endpoint = null;
  private com.velos.DHTS.PatientDemographicsSoap patientDemographicsSoap = null;
  
  public PatientDemographicsSoapProxy() {
    _initPatientDemographicsSoapProxy();
  }
  
  public PatientDemographicsSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initPatientDemographicsSoapProxy();
  }
  
  private void _initPatientDemographicsSoapProxy() {
    try {
      patientDemographicsSoap = (new com.velos.DHTS.PatientDemographicsLocator()).getPatientDemographicsSoap();
      if (patientDemographicsSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)patientDemographicsSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)patientDemographicsSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (patientDemographicsSoap != null)
      ((javax.xml.rpc.Stub)patientDemographicsSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.velos.DHTS.PatientDemographicsSoap getPatientDemographicsSoap() {
    if (patientDemographicsSoap == null)
      _initPatientDemographicsSoapProxy();
    return patientDemographicsSoap;
  }
  
  public com.velos.DHTS.GetPatientInfoResponseGetPatientInfoResult getPatientInfo(java.lang.String userID, java.lang.String pwd, java.lang.String HCFacility, java.lang.String MRN) throws java.rmi.RemoteException{
    if (patientDemographicsSoap == null)
      _initPatientDemographicsSoapProxy();
    return patientDemographicsSoap.getPatientInfo(userID, pwd, HCFacility, MRN);
  }
  
  public com.velos.DHTS.DemographicsResponse getPatientDemographics(com.velos.DHTS.DemographicsRequest rInput) throws java.rmi.RemoteException{
    if (patientDemographicsSoap == null)
      _initPatientDemographicsSoapProxy();
    return patientDemographicsSoap.getPatientDemographics(rInput);
  }
  
  public com.velos.DHTS.GetPatientInfoExResponseGetPatientInfoExResult getPatientInfoEx(java.lang.String userID, java.lang.String password, java.lang.String HCFacilityList, java.lang.String MRNList, int howmanyEncounters) throws java.rmi.RemoteException{
    if (patientDemographicsSoap == null)
      _initPatientDemographicsSoapProxy();
    return patientDemographicsSoap.getPatientInfoEx(userID, password, HCFacilityList, MRNList, howmanyEncounters);
  }
  
  
}