/**
 * GetPatientDemographicsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.velos.DHTS;

public class GetPatientDemographicsResponse  implements java.io.Serializable {
    private com.velos.DHTS.DemographicsResponse getPatientDemographicsResult;

    public GetPatientDemographicsResponse() {
    }

    public GetPatientDemographicsResponse(
           com.velos.DHTS.DemographicsResponse getPatientDemographicsResult) {
           this.getPatientDemographicsResult = getPatientDemographicsResult;
    }


    /**
     * Gets the getPatientDemographicsResult value for this GetPatientDemographicsResponse.
     * 
     * @return getPatientDemographicsResult
     */
    public com.velos.DHTS.DemographicsResponse getGetPatientDemographicsResult() {
        return getPatientDemographicsResult;
    }


    /**
     * Sets the getPatientDemographicsResult value for this GetPatientDemographicsResponse.
     * 
     * @param getPatientDemographicsResult
     */
    public void setGetPatientDemographicsResult(com.velos.DHTS.DemographicsResponse getPatientDemographicsResult) {
        this.getPatientDemographicsResult = getPatientDemographicsResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPatientDemographicsResponse)) return false;
        GetPatientDemographicsResponse other = (GetPatientDemographicsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getPatientDemographicsResult==null && other.getGetPatientDemographicsResult()==null) || 
             (this.getPatientDemographicsResult!=null &&
              this.getPatientDemographicsResult.equals(other.getGetPatientDemographicsResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetPatientDemographicsResult() != null) {
            _hashCode += getGetPatientDemographicsResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPatientDemographicsResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", ">GetPatientDemographicsResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getPatientDemographicsResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "GetPatientDemographicsResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "DemographicsResponse"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
