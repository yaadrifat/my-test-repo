/**
 * PatientIdentifier.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.velos.DHTS;

public class PatientIdentifier  implements java.io.Serializable {
    private java.lang.String HCFacility;

    private java.lang.String MRN;

    public PatientIdentifier() {
    }

    public PatientIdentifier(
           java.lang.String HCFacility,
           java.lang.String MRN) {
           this.HCFacility = HCFacility;
           this.MRN = MRN;
    }


    /**
     * Gets the HCFacility value for this PatientIdentifier.
     * 
     * @return HCFacility
     */
    public java.lang.String getHCFacility() {
        return HCFacility;
    }


    /**
     * Sets the HCFacility value for this PatientIdentifier.
     * 
     * @param HCFacility
     */
    public void setHCFacility(java.lang.String HCFacility) {
        this.HCFacility = HCFacility;
    }


    /**
     * Gets the MRN value for this PatientIdentifier.
     * 
     * @return MRN
     */
    public java.lang.String getMRN() {
        return MRN;
    }


    /**
     * Sets the MRN value for this PatientIdentifier.
     * 
     * @param MRN
     */
    public void setMRN(java.lang.String MRN) {
        this.MRN = MRN;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PatientIdentifier)) return false;
        PatientIdentifier other = (PatientIdentifier) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.HCFacility==null && other.getHCFacility()==null) || 
             (this.HCFacility!=null &&
              this.HCFacility.equals(other.getHCFacility()))) &&
            ((this.MRN==null && other.getMRN()==null) || 
             (this.MRN!=null &&
              this.MRN.equals(other.getMRN())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getHCFacility() != null) {
            _hashCode += getHCFacility().hashCode();
        }
        if (getMRN() != null) {
            _hashCode += getMRN().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PatientIdentifier.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "PatientIdentifier"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HCFacility");
        elemField.setXmlName(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "HCFacility"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MRN");
        elemField.setXmlName(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "MRN"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
