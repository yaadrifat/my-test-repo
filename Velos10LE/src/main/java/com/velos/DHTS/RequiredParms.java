/**
 * RequiredParms.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.velos.DHTS;

public class RequiredParms  implements java.io.Serializable {
    private com.velos.DHTS.AgentIdentifier agent;

    private com.velos.DHTS.ClientIdentifier client;

    private com.velos.DHTS.PatientIdentifier patient;

    public RequiredParms() {
    }

    public RequiredParms(
           com.velos.DHTS.AgentIdentifier agent,
           com.velos.DHTS.ClientIdentifier client,
           com.velos.DHTS.PatientIdentifier patient) {
           this.agent = agent;
           this.client = client;
           this.patient = patient;
    }


    /**
     * Gets the agent value for this RequiredParms.
     * 
     * @return agent
     */
    public com.velos.DHTS.AgentIdentifier getAgent() {
        return agent;
    }


    /**
     * Sets the agent value for this RequiredParms.
     * 
     * @param agent
     */
    public void setAgent(com.velos.DHTS.AgentIdentifier agent) {
        this.agent = agent;
    }


    /**
     * Gets the client value for this RequiredParms.
     * 
     * @return client
     */
    public com.velos.DHTS.ClientIdentifier getClient() {
        return client;
    }


    /**
     * Sets the client value for this RequiredParms.
     * 
     * @param client
     */
    public void setClient(com.velos.DHTS.ClientIdentifier client) {
        this.client = client;
    }


    /**
     * Gets the patient value for this RequiredParms.
     * 
     * @return patient
     */
    public com.velos.DHTS.PatientIdentifier getPatient() {
        return patient;
    }


    /**
     * Sets the patient value for this RequiredParms.
     * 
     * @param patient
     */
    public void setPatient(com.velos.DHTS.PatientIdentifier patient) {
        this.patient = patient;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RequiredParms)) return false;
        RequiredParms other = (RequiredParms) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.agent==null && other.getAgent()==null) || 
             (this.agent!=null &&
              this.agent.equals(other.getAgent()))) &&
            ((this.client==null && other.getClient()==null) || 
             (this.client!=null &&
              this.client.equals(other.getClient()))) &&
            ((this.patient==null && other.getPatient()==null) || 
             (this.patient!=null &&
              this.patient.equals(other.getPatient())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAgent() != null) {
            _hashCode += getAgent().hashCode();
        }
        if (getClient() != null) {
            _hashCode += getClient().hashCode();
        }
        if (getPatient() != null) {
            _hashCode += getPatient().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RequiredParms.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "RequiredParms"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("agent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "Agent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "AgentIdentifier"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("client");
        elemField.setXmlName(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "Client"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "ClientIdentifier"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("patient");
        elemField.setXmlName(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "Patient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "PatientIdentifier"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
