package com.velos.browser;

import java.util.HashMap;

import com.velos.eres.service.util.StringUtil;

public class base {

 private String mainSQL;
 private String countSQL;
 private HashMap paramMap;
 
 



public base()
 {
   mainSQL="";
   countSQL="";
   paramMap=new HashMap();
 
 }

/**
 * @return the mainSQL
 */
public String getMainSQL() {
	return mainSQL;
}

/**
 * @param mainSQL the mainSQL to set
 */
public void setMainSQL(String mainSQL) {
	this.mainSQL = StringUtil.htmlUnicodePoint(mainSQL);
}

/**
 * @return the countSQL
 */
public String getCountSQL() {
	return countSQL;
}

/**
 * @param countSQL the countSQL to set
 */
public void setCountSQL(String countSQL) {
	this.countSQL = countSQL;
}

/**
 * @return the paramMap
 */
public HashMap getParamMap() {
	return paramMap;
}

/**
 * @param paramMap the paramMap to set
 */
public void setParamMap(HashMap paramMap) {
	this.paramMap = paramMap;
}


 
}
