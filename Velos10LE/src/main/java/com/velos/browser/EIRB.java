package com.velos.browser;
 
import java.util.HashMap;


import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;

public class EIRB extends base{

public EIRB()
 {
  
 }



  public  void getApplicationSQL()
	    {
	   
	      
	     String countSql = "";
	     String appSQL = "";
	     String module =(String)getParamMap().get("module");
	     
	      
	     if ("irbpend".equals(module)) {
	         appSQL  = getPendingSubmissionSQL();
	     } else if ("irbongoing".equals(module)) {
	         appSQL = getOngoingStudiesSQL();
	     } else if ("irbmeeting".equals(module)) {
             appSQL = getMeetingStudiesSQL();
	     } else if ("irbmeetingtopic".equals(module)) {
             appSQL = getMeetingTopicsSQL();
         } else if ("irbreview".equals(module))
	     {
	    	 appSQL  = getPendingSubmissionSQL();
	     }
	     else
	     {
	         appSQL  = getNewSubmissionSQL();
	     }
	 	
	 	
	   	countSql= "select count(*) from ( " + appSQL + " )";
	   		   	
	     this.setMainSQL(appSQL );
	     
	     // System.out.println("appSQL=" +appSQL);
	     this.setCountSQL(countSql);
	   	 
	    }


		  private String getBasicApplicationSQL()
		  {
		  	
			HashMap hmap = getParamMap();
		 	
		 	String accId=(String)hmap.get("accountId");
		 	String user =(String)hmap.get("user");
		 	String group = (String)hmap.get("group");

		 	String submissionType = (String)hmap.get("submissionType");

		 	String revType = (String)hmap.get("revType");
		 	
		 	String tabsubtype =(String)hmap.get("tabsubtype");
		 	
		 	if (StringUtil.isEmpty(tabsubtype))
		 	{
		 		tabsubtype = "";
		 	}
		 			 	
		 	String studyPI = "";
		 	
		 	studyPI = (String)hmap.get("studyPI");
		 	
		 	if (studyPI==null && StringUtil.isEmpty(studyPI))
		 	{
		 		studyPI = "";
		 		
		 	}
		 	
			StringBuffer sqlBuffer = new StringBuffer();
		  	
		  	sqlBuffer.append(" Select st.rowid rowcount, study_number, study_title, F_CODELST_DESC(fk_codelst_tarea) tarea_desc,fk_codelst_tarea, " );
		  	sqlBuffer.append(" (select usr_firstname ||' ' || usr_lastname from er_user where pk_user = study_prinv) study_prinv_name," );
		  	sqlBuffer.append("  F_CODELST_DESC(submission_type) submission_type_desc,submission_type," );
		  	sqlBuffer.append("   F_CODELST_DESC(st.submission_status) submission_status_desc,st.submission_status, " );
		  	
		  	sqlBuffer.append("to_char(st.submission_status_date,pkg_dateutil.f_get_dateformat) submission_status_date,");
		  	
			sqlBuffer.append("  F_CODELST_DESC(sb.submission_review_type) review_type_desc,sb.submission_review_type,(select review_board_name from er_review_board where pk_review_board = fk_review_board) submission_board_name, fk_review_board, pk_study ,pk_submission, pk_submission_board" );
			
			sqlBuffer.append(" , (select usr_firstname ||' ' || usr_lastname from er_user where pk_user = submission_assigned_to) assigned_to ");
			
			sqlBuffer.append(" , (select usr_firstname ||' ' || usr_lastname from er_user where pk_user = submission_entered_by) entered_by ");
			
			sqlBuffer.append(" , (select codelst_subtyp from er_codelst where pk_codelst = submission_type ) submission_type_subtype ");
			
		  	sqlBuffer.append("  from er_submission sub,er_submission_board sb,er_study s,er_submission_status st");
		  	sqlBuffer.append(" where sub.fk_study = s.pk_study and sub.pk_submission = sb.fk_submission and st.fk_submission_board = sb.pk_submission_board and is_current = 1 " );

		  	sqlBuffer.append("  and fk_review_board in  (select pk_review_board from er_review_board erb where erb.fk_account = "+accId+" and ','|| erb.board_group_access || ',' like '%," + group+ ",%' )");
		  	
		  	if (EJBUtil.stringToNum(revType)>0)
		  	{
		  		sqlBuffer.append("  and submission_review_type = " + revType);
		  	}
		  	
			if (EJBUtil.stringToNum(submissionType)>0)
		  	{
		  		sqlBuffer.append("  and submission_type = " + submissionType);
		  	}
			
			if (!StringUtil.isEmpty(studyPI))
			{
				sqlBuffer.append("  and s.study_prinv = " + studyPI);
				
			}
			
		  	sqlBuffer.append("  and s.fk_account = "+ accId + "  and  ( exists ( select * from er_studyteam t where t.fk_study = s.pk_study and "); 
		  	sqlBuffer.append("  t.fk_user = "+user+" and  nvl(t.study_team_usr_type,'D')='D') or pkg_superuser.F_Is_Superuser("+user+", pk_study) = 1    ) ");
	   		
		  	
		  	return sqlBuffer.toString();
		  	
		  }

		  /** Returns appropriate Submission related SQL depending on parameters passed in the attribute paramMap
		   * Supported parameters to put the hashmap:
		   * accountId : PK of er_Account
		   * user : User, PK of er_user
		   * group : Group, PK of er_grps
		   * tabsubtype : the tab SQL is needed for,the sql changes accordingly. pass object_subtype from table er_object_settings to identify the IRB tab 
		   * */
	  public String getNewSubmissionSQL()
	  {
	  	String sqlNewSub = "";
	  	sqlNewSub = getBasicApplicationSQL();
	  	
	  	StringBuffer sbFinal = new StringBuffer();
	  	//add filter
	  	
	 	HashMap hmap = getParamMap();
	 	
	 	String accId=(String)hmap.get("accountId");
	 	String user =(String)hmap.get("user");
	 	String group = (String)hmap.get("group");
	 	String tabsubtype =(String)hmap.get("tabsubtype");
        String searchCriteria = (String)hmap.get("searchCriteria");
	 	String statusToCheck = "";
	 	
	 	
	 	if (tabsubtype.equals("irb_new_tab"))//new submissions
	 	{
	 		
	 		sqlNewSub= sqlNewSub + " and ( st.submission_status = (select F_CODELST_ID('subm_status', 'submitted') from dual) "+
	 		" or st.submission_status = (select F_CODELST_ID('subm_status', 'resubmitted') from dual) ) " ;
	 	}	
	 	else
	 	{	
		 	if (tabsubtype.equals("irb_assigned_tab")) //assigned submissions
		 	{
		 		statusToCheck = "admin_review";
		 	}
		 	
		 	if (tabsubtype.equals("irb_compl_tab")) //submission completed
		 	{
		 		statusToCheck = "admin_rev_comp";
		 	}
		 	
		 	if (tabsubtype.equals("irb_post_tab")) 
		 	{
		 		statusToCheck = "asgn_pr"; // post review submissions
		 	}
		 	
			if (tabsubtype.equals("irb_pend_tab")) 
		 	{
		 		statusToCheck = "pi_resp_req"; // pending PI submissions
		 	}
			
			if (tabsubtype.equals("irb_pend_rev")) 
		 	{
		 		statusToCheck = "reviewer"; // pending PI submissions
				
				//add a check for review type not null
				
				sqlNewSub= sqlNewSub + " and submission_review_type is not null ";
		 	}
			
		 	
		 	if (! StringUtil.isEmpty(statusToCheck))
			{
				sqlNewSub= sqlNewSub + " and st.submission_status = (select F_CODELST_ID('subm_status', '" + statusToCheck + "') from dual) " ;
			}	
	 	}

	 	/* do not show applications that has an overall status marke as final status*/
	 	
	 	sbFinal.append("  and not exists ");
	 	sbFinal.append("   (select 1 from er_submission_status where ");
	 	sbFinal.append("   fk_submission = sub.pk_submission and fk_submission_board is null and ");
	 	sbFinal.append("   is_current = 1 and submission_status in (select pk_codelst from er_codelst where ");
	 	sbFinal.append("      ','||codelst_custom_col1||',' like '%,final,%' ) ) ");

	 	sqlNewSub= sqlNewSub + sbFinal.toString();
         
	 	if (searchCriteria != null) {
            StringBuffer sqlBuffer = new StringBuffer();
            sqlBuffer.append("  and (lower(s.study_title) like lower('%").append(searchCriteria.replaceAll("'", "''")).append("%') or ");
            sqlBuffer.append("  lower(s.study_number) like lower('%").append(searchCriteria.replaceAll("'", "''")).append("%') ) ");
            sqlNewSub= sqlNewSub + sqlBuffer.toString();
        }
	  	
	  	return sqlNewSub;
	  }
 
	  
	  public static String getIRBFormSubmissionTypeSQL(String callingTab,String formCategory, String submissionSubtype)
	  {
	  	StringBuffer sbSQL= new StringBuffer();
	  	
	  	sbSQL.append(" select FORM_SUBMISSION_SUBTYPE from er_irbforms_setup where IRB_TAB_SUBTYPE = ?");
	  	sbSQL.append(" and  CODELST_SUBMISSION_SUBTYPE = ? and FORM_CATLIB_SUBTYPE = ?");
	  	
	  	return sbSQL.toString();
	  }
	  ///
	  
    public String getPendingSubmissionSQL() {
        HashMap hmap = getParamMap();
        if ("irb_saved_tab".equals((String)hmap.get("tabsubtype"))) {
            return getPendingSavedItemSQL();
        }

        String accId=(String)hmap.get("accountId");
        String user =(String)hmap.get("user");
        String group = (String)hmap.get("group");
        String submissionType = (String)hmap.get("submissionType");
        String revType = (String)hmap.get("revType");
        String searchCriteria = (String)hmap.get("searchCriteria");
        String module =(String)hmap.get("module");
	    String reviewersColumn = "";
	    
	    String studyPI = "";
	 	
	 	studyPI = (String)hmap.get("studyPI");
	 	
	 	if (studyPI==null && StringUtil.isEmpty(studyPI))
	 	{
	 		studyPI = "";
	 		
	 	}

	    
	     if ("irbreview".equals(module))
	     {
	    	 reviewersColumn = " , USR_LST(sb.submission_reviewer) as SUBMISSION_REVIEWERS, F_CODELST_DESC(fk_codelst_tarea) tarea_desc,(select usr_firstname ||' ' || usr_lastname from er_user where pk_user = study_prinv) study_prinv_name " ; 
	    	  
	     }

        StringBuffer sqlBuffer = new StringBuffer();
        sqlBuffer.append(" Select st.rowid rowcount, study_number, study_title, " );
        sqlBuffer.append("  (select codelst_subtyp from er_codelst where pk_codelst = submission_type) submission_type_subtype, ");
        sqlBuffer.append("  F_CODELST_DESC(submission_type) submission_type_desc,submission_type," );
        sqlBuffer.append("  F_CODELST_DESC(st.submission_status) submission_status_desc,st.submission_status,to_char((select meeting_date from er_review_meeting where pk_review_meeting = sb.fk_review_meeting),pkg_dateutil.f_get_dateformat) submission_meeting_date, " ); 
        sqlBuffer.append("  F_CODELST_DESC(sb.submission_review_type) review_type_desc,sb.submission_review_type,(select review_board_name from er_review_board where pk_review_board = fk_review_board) submission_board_name, fk_review_board, pk_study ,pk_submission, pk_submission_board, " );
        sqlBuffer.append("  st.submission_status_date, ");
        sqlBuffer.append("  (select usr_firstname ||' '|| usr_lastname from er_user where pk_user = st.submission_entered_by) entered_by, ");
        sqlBuffer.append("  pk_submission_status, st.submission_notes  ");
        
        sqlBuffer.append(reviewersColumn);
        
        sqlBuffer.append("  from er_submission sub,er_submission_board sb,er_study s,er_submission_status st");
        sqlBuffer.append(" where sub.fk_study = s.pk_study and sub.pk_submission = sb.fk_submission and st.fk_submission_board = sb.pk_submission_board and is_current = 1 " );
        sqlBuffer.append("  and fk_review_board in  (select pk_review_board from er_review_board erb where erb.fk_account = "+accId+" and ','|| erb.board_group_access || ',' like '%," + group+ ",%' )");
        if (EJBUtil.stringToNum(revType)>0) {
            sqlBuffer.append("  and submission_review_type = " + revType);
        }
        if (EJBUtil.stringToNum(submissionType)>0) {
            sqlBuffer.append("  and submission_type = " + submissionType);
        }
        

        if (!StringUtil.isEmpty(studyPI))
			{
				sqlBuffer.append("  and s.study_prinv = " + studyPI);
				
			}

        
        if ("irb_act_tab".equals((String)hmap.get("tabsubtype"))) {
            sqlBuffer.append("  and st.submission_status = ").append(" ( select F_CODELST_ID('subm_status','pi_resp_req') from dual)");
        } else if ("irb_items_tab".equals((String)hmap.get("tabsubtype"))) {
            sqlBuffer.append("  and st.submission_status <> ").append(" ( select F_CODELST_ID('subm_status','pi_resp_req') from dual ) ");
            sqlBuffer.append("  and st.submission_status not in ").append(" (select c.pk_codelst from er_codelst c where c.codelst_type = 'subm_status' ");
            sqlBuffer.append("      and ',' || c.codelst_custom_col1 || ',' like '%,final,%') ");
        } else if ("irb_appr_tab".equals((String)hmap.get("tabsubtype"))) {
            // sqlBuffer.append("  and st.submission_status = ").append(" ( select F_CODELST_ID('subm_status','approved') from dual)");
            sqlBuffer.append("  and st.submission_status in (select pk_codelst from er_codelst where ");
            sqlBuffer.append("      ','||codelst_custom_col1||',' like '%,final,%' ) ");

        }
        sqlBuffer.append("  and s.fk_account = "+ accId + "  and  ( exists ( select * from er_studyteam t where t.fk_study = s.pk_study and "); 
        sqlBuffer.append("  t.fk_user = "+user+" and  nvl(t.study_team_usr_type,'D')='D') or pkg_superuser.F_Is_Superuser("+user+", pk_study) = 1    ) ");
        
        if ("irbpend".equals(module) && (!"irb_appr_tab".equals((String)hmap.get("tabsubtype")) ) ) {
            sqlBuffer.append("  and not exists ");
            sqlBuffer.append("   (select 1 from er_submission_status where ");
            sqlBuffer.append("   fk_submission = sub.pk_submission and fk_submission_board is null and ");
            sqlBuffer.append("   is_current = 1 and submission_status in (select pk_codelst from er_codelst where ");
            sqlBuffer.append("      ','||codelst_custom_col1||',' like '%,final,%' ) ) ");

        }
        if (searchCriteria != null) {
            sqlBuffer.append("  and (lower(s.study_title) like lower('%").append(searchCriteria.replaceAll("'", "''")).append("%') or ");
            sqlBuffer.append("  lower(s.study_number) like lower('%").append(searchCriteria.replaceAll("'", "''")).append("%') ) ");
        }
        
         
        return sqlBuffer.toString();
    }
    
    private String getPendingSavedItemSQL() {
        HashMap hmap = getParamMap();
        String accId=(String)hmap.get("accountId");
        String user =(String)hmap.get("user");
        String group = (String)hmap.get("group");
        String submissionType = (String)hmap.get("submissionType");
        String revType = (String)hmap.get("revType");
        String searchCriteria = (String)hmap.get("searchCriteria");

        StringBuffer sqlBuffer = new StringBuffer();
        sqlBuffer.append(" select c.codelst_seq,mainq.* from ( ");
        sqlBuffer.append(" select s.rowid rowcount, s.pk_study, s.study_title, s.study_number, ");
        sqlBuffer.append(" F_CODELST_DESC((select F_CODELST_ID('submission', 'new_app') from dual)) submission_type_desc, ");
        sqlBuffer.append(" null submission_status_desc, ");
        sqlBuffer.append(" null submission_meeting_date, ");
        sqlBuffer.append(" null submission_board_name, ");
        sqlBuffer.append(" (select F_CODELST_ID('submission','new_app') from dual) SUBMISSION_TYPE, ");
        sqlBuffer.append(" null SUBMISSION_STATUS, ");
        sqlBuffer.append(" null SUBMISSION_REVIEW_TYPE, ");
        sqlBuffer.append(" null FK_REVIEW_BOARD, ");
        sqlBuffer.append(" null ACTION_LINK, ");
        sqlBuffer.append(" null PK_SUBMISSION, ");
        sqlBuffer.append(" null PK_SUBMISSION_BOARD, ");
        sqlBuffer.append(" 'new_app' SUBMISSION_TYPE_SUBTYPE, ");
        sqlBuffer.append(" null fk_formlib, ");
        sqlBuffer.append(" null lf_entrychar, ");
        sqlBuffer.append(" null SAVEDCOUNT ");
        sqlBuffer.append(" from er_study s ");
        sqlBuffer.append(" where s.study_creation_type = 'A' and s.fk_account = ").append(accId).append("  and  ");
        sqlBuffer.append(" ( exists ( select * from er_studyteam t where t.fk_study = s.pk_study and ");
        sqlBuffer.append(" t.fk_user = ").append(user).append(" and  nvl(t.study_team_usr_type,'D')='D') or ");
        sqlBuffer.append(" pkg_superuser.F_Is_Superuser(").append(user).append(", pk_study) = 1    ) ");
        sqlBuffer.append(" and exists ( ");
        sqlBuffer.append("         select pk_review_board from er_review_board erb where fk_account = ").append(accId);
        sqlBuffer.append("         and pk_review_board not in ( ");
        sqlBuffer.append("           select sb.fk_review_board from er_submission_board sb ");
        sqlBuffer.append("           inner join er_submission sub on (sb.fk_submission = sub.pk_submission and sub.submission_flag = 1) ");
        sqlBuffer.append("           where sub.fk_study = s.pk_study ");
        sqlBuffer.append("         ) ");
        sqlBuffer.append("       ) ");
        sqlBuffer.append(" and not exists ( ");
        sqlBuffer.append("     select pk_studystat from er_studystat stat ");
        sqlBuffer.append("     where stat.fk_study = s.pk_study and ( ");
        sqlBuffer.append("     stat.fk_codelst_studystat = (select F_CODELST_ID ('studystat', (select CTRL_VALUE from ER_CTRLTAB where CTRL_KEY = 'irb_app_stat' and rownum<=1)) from dual ) OR ");
        sqlBuffer.append("     stat.fk_codelst_studystat = (select F_CODELST_ID ('studystat', 'active') from dual ) ) ");
        sqlBuffer.append("    ) ");
        sqlBuffer.append(" and not exists (select 1 from er_submission su ");
        sqlBuffer.append("     inner join er_submission_status ss on su.pk_submission = ss.fk_submission ");
        sqlBuffer.append("     where fk_study = s.pk_study and ss.is_current = 1 ");
        sqlBuffer.append("     and ss.fk_submission_board is null ");
        sqlBuffer.append("     and ss.submission_status = (select F_CODELST_ID ('subm_status', 'approved') from dual) ) ");
        sqlBuffer.append(" UNION ALL ");
        sqlBuffer.append(" select s.rowid rowcount, s.pk_study, s.study_title, s.study_number, ");
        sqlBuffer.append(" CASE (lf.lf_submission_type) ");
        sqlBuffer.append(" WHEN 'irb_ongo_amd'  THEN F_CODELST_DESC((select F_CODELST_ID('submission', 'study_amend') from dual)) ");
        sqlBuffer.append(" WHEN 'irb_ongo_prob' THEN F_CODELST_DESC((select F_CODELST_ID('submission', 'prob_rpt') from dual)) ");
        sqlBuffer.append(" WHEN 'irb_ongo_cont' THEN F_CODELST_DESC((select F_CODELST_ID('submission', 'cont_rev') from dual)) ");
        sqlBuffer.append(" WHEN 'irb_ongo_clos' THEN F_CODELST_DESC((select F_CODELST_ID('submission', 'closure') from dual)) ");
        sqlBuffer.append(" ELSE null ");
        sqlBuffer.append(" END submission_type_desc, ");
        sqlBuffer.append(" null submission_status_desc, ");
        sqlBuffer.append(" null submission_meeting_date, ");
        sqlBuffer.append(" null submission_board_name, ");
        sqlBuffer.append(" CASE (lf.lf_submission_type) ");
        sqlBuffer.append(" WHEN 'irb_ongo_amd'  THEN (select F_CODELST_ID ('submission', 'study_amend') from dual) ");
        sqlBuffer.append(" WHEN 'irb_ongo_prob' THEN (select F_CODELST_ID ('submission', 'prob_rpt') from dual) ");
        sqlBuffer.append(" WHEN 'irb_ongo_cont' THEN (select F_CODELST_ID ('submission', 'cont_rev') from dual) ");
        sqlBuffer.append(" WHEN 'irb_ongo_clos' THEN (select F_CODELST_ID ('submission', 'closure') from dual) ");
        sqlBuffer.append(" ELSE null ");
        sqlBuffer.append(" END submission_type, ");
        sqlBuffer.append(" null SUBMISSION_STATUS, ");
        sqlBuffer.append(" null SUBMISSION_REVIEW_TYPE, ");
        sqlBuffer.append(" null FK_REVIEW_BOARD, ");
        sqlBuffer.append(" null ACTION_LINK, ");
        sqlBuffer.append(" null PK_SUBMISSION, ");
        sqlBuffer.append(" null PK_SUBMISSION_BOARD, ");
        sqlBuffer.append(" lf.lf_submission_type SUBMISSION_TYPE_SUBTYPE, ");
        sqlBuffer.append(" sf.fk_formlib, ");
        sqlBuffer.append(" lf.lf_entrychar, ");
        sqlBuffer.append(" (SELECT COUNT(*) FROM ER_STUDYFORMS a WHERE sf.fk_formlib = a.fk_formlib and ");
        sqlBuffer.append(" a.FK_STUDY = s.pk_study and a.RECORD_TYPE <> 'D') SAVEDCOUNT ");
        sqlBuffer.append(" from er_study s ");
        sqlBuffer.append(" inner join er_studyforms sf ");
        sqlBuffer.append(" on sf.fk_study = s.pk_study ");
        sqlBuffer.append(" inner join er_linkedforms lf ");
        sqlBuffer.append(" on sf.fk_formlib = lf.fk_formlib ");
        sqlBuffer.append(" where s.fk_account = ").append(accId).append("  and  ");
        sqlBuffer.append("      lf.fk_account = ").append(accId).append("  and  ");
        sqlBuffer.append(" ( exists ( select * from er_studyteam t where t.fk_study = s.pk_study and ");
        sqlBuffer.append(" t.fk_user = ").append(user).append(" and  nvl(t.study_team_usr_type,'D')='D') or ");
        sqlBuffer.append(" pkg_superuser.F_Is_Superuser(").append(user).append(", pk_study) = 1    ) ");
        sqlBuffer.append(" and exists ( ");
        sqlBuffer.append("     select pk_studystat from er_studystat stat ");
        sqlBuffer.append("     where stat.fk_study = s.pk_study and  ( ");
        sqlBuffer.append("      stat.fk_codelst_studystat in ( ");
        sqlBuffer.append("       select pk_codelst from ER_CODELST where "); 
        sqlBuffer.append("       CODELST_TYPE = 'studystat'  and CODELST_SUBTYP = ");
        sqlBuffer.append("       (select CTRL_VALUE from ER_CTRLTAB where CTRL_KEY = 'irb_app_stat' and rownum<=1) ");
        sqlBuffer.append("       ) ");
        sqlBuffer.append("      OR stat.fk_codelst_studystat = (select F_CODELST_ID ('studystat','active') from dual) ");
        sqlBuffer.append("     ) )  ");
        sqlBuffer.append(" and lf.lf_submission_type in ");
        sqlBuffer.append("     ('irb_ongo_amd','irb_ongo_prob','irb_ongo_cont','irb_ongo_clos') ");
        sqlBuffer.append(" and ( EXISTS( select 1 from er_studyforms sf1 where ");
        sqlBuffer.append("       (select F_CODELST_ID ('fillformstat','complete') from dual) <>  sf1.form_completed and "); 
        sqlBuffer.append("       sf1.fk_formlib = sf.fk_formlib and sf1.fk_study = sf.fk_study) ");
        sqlBuffer.append("     and sf.pk_studyforms = (select F_GET_LATEST_FSTAT(s.pk_study, sf.fk_formlib) from dual) ) ");
        sqlBuffer.append(" and not exists (select 1 from er_submission su ");
        sqlBuffer.append("     inner join er_submission_status ss on su.pk_submission = ss.fk_submission ");
        sqlBuffer.append("     where fk_study = s.pk_study and ss.is_current = 1 ");
        sqlBuffer.append("     and ss.fk_submission_board is null ");
        sqlBuffer.append("     and ss.submission_status = (select F_CODELST_ID ('subm_status', 'approved') from dual) ) ");
        sqlBuffer.append(" ) mainq inner join er_codelst c on submission_type = c.pk_codelst ");
        sqlBuffer.append(" where 1 = 1 ");
        if (EJBUtil.stringToNum(revType)>0) {
            sqlBuffer.append("  and submission_review_type = " + revType);
        }
        if (EJBUtil.stringToNum(submissionType)>0) {
            sqlBuffer.append("  and submission_type = " + submissionType);
        }
        if (searchCriteria != null) {
            sqlBuffer.append("  and (lower(study_title) like lower('%").append(searchCriteria.replaceAll("'", "''")).append("%') or ");
            sqlBuffer.append("  lower(study_number) like lower('%").append(searchCriteria.replaceAll("'", "''")).append("%') ) ");
        }
        sqlBuffer.append(" order by codelst_seq, study_number ");
        
        return sqlBuffer.toString();
    }
    
    private String getOngoingStudiesSQL() {
        HashMap hmap = getParamMap();
        String accId=(String)hmap.get("accountId");
        String user =(String)hmap.get("user");
        String group = (String)hmap.get("group");
        String studyStatusSubtype1 = (String)hmap.get("studyStatusSubtype1"); // This is "irb_approved"
        String studyStatusSubtype2 = (String)hmap.get("studyStatusSubtype2");
        String searchCriteria = (String)hmap.get("searchCriteria");

        StringBuffer sqlBuffer = new StringBuffer();
        sqlBuffer.append(" select s.rowid rowcount, s.pk_study, s.study_title, s.study_number, ");
        sqlBuffer.append(" (select null from dual) action_link, ");
        sqlBuffer.append("  'Ongoing Study' submission_type_desc ");
        sqlBuffer.append(" from er_study s ");
         sqlBuffer.append(" where s.fk_account = ").append(accId).append("  and  ");
        sqlBuffer.append(" ( exists ( select * from er_studyteam t where t.fk_study = s.pk_study and ");
        sqlBuffer.append(" t.fk_user = ").append(user).append(" and  nvl(t.study_team_usr_type,'D')='D') or ");
        sqlBuffer.append(" pkg_superuser.F_Is_Superuser(").append(user).append(", pk_study) = 1    ) ");
        
        //sqlBuffer.append(" and not exists (SELECT pk_submission FROM er_submission WHERE fk_study = s.pk_study) ");
        
        sqlBuffer.append(" AND EXISTS (select pk_studystat from er_studystat stat where stat.fk_study = s.pk_study ");
        if (studyStatusSubtype1 != null && studyStatusSubtype2 != null)
        {
            sqlBuffer.append(" and ( stat.fk_codelst_studystat in ( ");
            sqlBuffer.append("       select pk_codelst from ER_CODELST where "); 
            sqlBuffer.append("       CODELST_TYPE = 'studystat'  and CODELST_SUBTYP = ");
            sqlBuffer.append("       (select CTRL_VALUE from ER_CTRLTAB where CTRL_KEY = 'irb_app_stat' and rownum<=1) ");
            sqlBuffer.append("       ) ");
            sqlBuffer.append("    or stat.fk_codelst_studystat = (select F_CODELST_ID('studystat', '").append(studyStatusSubtype2).append("') from dual) ) ");
        }
        else if (studyStatusSubtype1 != null)
        {
            sqlBuffer.append(" and stat.fk_codelst_studystat in ( ");
            sqlBuffer.append("       select pk_codelst from ER_CODELST where "); 
            sqlBuffer.append("       CODELST_TYPE = 'studystat'  and CODELST_SUBTYP = ");
            sqlBuffer.append("       (select CTRL_VALUE from ER_CTRLTAB where CTRL_KEY = 'irb_app_stat' and rownum<=1) ");
            sqlBuffer.append("       ) ");
        }
        else if (studyStatusSubtype2 != null)
        {
            sqlBuffer.append(" and stat.fk_codelst_studystat = (select F_CODELST_ID('studystat', '").append(studyStatusSubtype2).append("') from dual ");
        }
        sqlBuffer.append(" )  "); // this goes with "AND EXISTS..." above
        
        if (searchCriteria != null)
        {
            sqlBuffer.append("  and (lower(s.study_title) like lower('%").append(searchCriteria.replaceAll("'", "''")).append("%') or ");
            sqlBuffer.append("  lower(s.study_number) like lower('%").append(searchCriteria.replaceAll("'", "''")).append("%') ) ");
        }

        // System.out.println(sqlBuffer.toString());
        
        return sqlBuffer.toString();
        
        
    }

    private String getMeetingStudiesSQL() {
        HashMap hmap = getParamMap();
        String accId=(String)hmap.get("accountId");
        String user =(String)hmap.get("user");
        String meetingDate = (String)hmap.get("meetingDate");

        StringBuffer sqlBuffer = new StringBuffer();
        sqlBuffer.append(" select s.rowid rowcount, s.pk_study, s.study_title, s.study_number, ");
        sqlBuffer.append(" (select usr_firstname ||' '||usr_lastname from er_user where pk_user = s.study_prinv) study_prinv, ");
        sqlBuffer.append(" F_CODELST_DESC(sub.submission_type) submission_type_desc , sub.PK_SUBMISSION, esb.PK_SUBMISSION_BOARD, (select codelst_subtyp from er_codelst where pk_codelst = submission_type ) submission_type_subtype , ");
        
        sqlBuffer.append(" (select codelst_subtyp from er_codelst where pk_codelst = submission_review_type ) submission_review_subtype ");
        
        sqlBuffer.append(" , USR_LST(submission_reviewer) as SUBMISSION_REVIEWER ");
        
        sqlBuffer.append(" from er_study s ");
        sqlBuffer.append(" inner join er_submission sub on sub.fk_study = s.pk_study ");
        sqlBuffer.append(" inner join er_submission_board esb on esb.fk_submission = sub.pk_submission ");
        sqlBuffer.append(" where s.fk_account = ").append(accId).append(" ");
        sqlBuffer.append(" AND (EXISTS ");
        sqlBuffer.append("   (SELECT * FROM er_studyteam t ");
        sqlBuffer.append("   where t.fk_study = s.pk_study and ");
        sqlBuffer.append("   t.fk_user = ").append(user).append(" and  nvl(t.study_team_usr_type,'D')='D') or ");
        sqlBuffer.append("   pkg_superuser.F_Is_Superuser(").append(user).append(", pk_study) = 1    ) ");
        sqlBuffer.append(" AND esb.fk_review_meeting = ").append(meetingDate).append(" ");
        return sqlBuffer.toString();
    }

    private String getMeetingTopicsSQL() {
        HashMap hmap = getParamMap();
        String accId=(String)hmap.get("accountId");
        String user =(String)hmap.get("user");
        String meetingDate = (String)hmap.get("meetingDate");
        StringBuffer sqlBuffer = new StringBuffer();
        sqlBuffer.append(" select t.rowid rowcount, t.pk_review_meeting_topic, t.topic_number, t.meeting_topic ");
        sqlBuffer.append(" from er_review_meeting_topic t ");
        sqlBuffer.append(" where fk_review_meeting = ").append(meetingDate).append(" ");
        return sqlBuffer.toString();
    }

    /* returns a sql for returning all form submission types for a submission*/
    public static String getIRBFormSubmissionTypeSQL(String submissionSubtype)
	  {
	  	StringBuffer sbSQL= new StringBuffer();
	  	
	  	sbSQL.append(" select FORM_SUBMISSION_SUBTYPE from er_irbforms_setup where CODELST_SUBMISSION_SUBTYPE = '"+submissionSubtype + "'");
	  	
	  	return sbSQL.toString();
	  }
	  
}
