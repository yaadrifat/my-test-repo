/**
 * 
 */
package com.velos.remoteservice.util;

import java.util.Map;

/**
 * @author dylan
 *
 */
public class AccountConfig extends AbstractConfigParams {

	private static AccountConfig accountConfig= null;
	
	private AccountConfig(){
	
	}
	
	public synchronized static AccountConfig getInstance(){
		if (accountConfig == null){
			accountConfig = new AccountConfig();
			accountConfig.setAllParams();
		}
	
		return accountConfig;
	}
	
	@Override
	protected  void readDB(Map<String, String> params) {
		// TODO Auto-generated method stub
		super.readDB(params);
	}

	@Override
	protected  void readEnvironment(Map<String, String> params) {
		super.readEnvironment(params);
	}

	@Override
	protected  void readFile(Map<String, String> params) {
		super.readFile(params);
	}

	@Override
	protected  void readSystem(Map<String, String> params) {
		super.readSystem(params);
	}

}
