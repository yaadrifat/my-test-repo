package com.velos.remoteservice;

public class MappingException extends VelosServiceException {

	public MappingException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MappingException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public MappingException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public MappingException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
