package com.velos.remoteservice;

import java.net.URI;

public class AuditLogger {
	
	public enum Severity{NORMAL, SEVERE}
	
	public static void logAudit(
			Class<?> callingClass, 
			Severity severity, 
			URI endpoint, 
			int userId,
			String message){
		System.out.println(callingClass + " " + severity.name() + " " + message);		
	}
}
