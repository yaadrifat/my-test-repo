package com.velos.remoteservice.demographics;


import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import com.velos.eres.business.common.SettingsDao;
import com.velos.eres.web.common.CommonJB;
import com.velos.remoteservice.AbstractService;
import com.velos.remoteservice.AbstractServiceFactory;
import com.velos.remoteservice.VelosServiceException;

/**
 * Factory class for creating IDemographicsService implementations. 
 * @author dylan
 *
 */
public class DemographicsServiceFactory extends AbstractServiceFactory {
	static final public String DEMOGRAPHICS_SERVICE_CLASS_FIELD_NAME = 
		"com.velos.remoteservice.demographics.service";
	
	@Deprecated
	static private IDemographicsService serviceSingleton = null;
	
	/** Hashtable to cache demographics service implementations, one per account*/
	static private Hashtable<Integer, IDemographicsService> accountServiceMap = new Hashtable<Integer, IDemographicsService>();
	@Deprecated	
	public static IDemographicsService getService() throws ClassNotFoundException{
		//This sweet code will only work with Java 1.6. For 1.5, we need the
		//Class.newInstance()
		//		final  ServiceLoader<ILabService> labServiceLoader =
		//			ServiceLoader.load(ILabService.class);
		//		
		//		for (ILabService labService : labServiceLoader){
		//			return labService;
		//		}
		//		return null;

		if (serviceSingleton == null){
			Class<IDemographicsService>  demoServiceClass = IDemographicsService.class;
			if (demoServiceClass == null) return null;
			for (IDemographicsService service : load(demoServiceClass)){
				serviceSingleton = service;
				break;
			}
		}
		return serviceSingleton;

	}
	
	public static synchronized IDemographicsService getService(int accountPK)
	throws VelosServiceException{
		IDemographicsService accountDemoService = accountServiceMap.get(accountPK);
		if (accountDemoService == null){
			CommonJB commonBean = new CommonJB();
			SettingsDao settingsDao = 
				commonBean.retrieveSettings(accountPK, 1);
			String svcClassName = null;
			int x=0;
			for (Object key : settingsDao.getSettingKeyword()){
				String keyStr = (String)key;
				if (keyStr.equals(DEMOGRAPHICS_SERVICE_CLASS_FIELD_NAME)){
					svcClassName = (String)settingsDao.getSettingValue().get(x);
				}
				
				x++;
			}
			
			if (svcClassName == null){
				throw new VelosServiceException("classname for Demographics Remote Service not found it er_settings");
			}
			AbstractService service = 
				(AbstractService)loadRemoteService(IDemographicsService.class, svcClassName);
			
			service.setAccountPK(accountPK);
			if (service instanceof IDemographicsService){
				accountServiceMap.put(accountPK, (IDemographicsService)service);
				return (IDemographicsService)service;
			}
			else{
					throw new VelosServiceException("Service found, but not IDemographicsService implementation");
			}
		}
		return (IDemographicsService)accountDemoService; 
	}

	
}
