/**
 * Created On Sep 7, 2011
 */
package com.velos.webservices;


import javax.jws.WebService;
import com.velos.services.OperationException;
import com.velos.services.client.FormRemoteObj;

import com.velos.services.model.FormDesign;
import com.velos.services.model.FormIdentifier;
import com.velos.services.model.FormList;
import com.velos.services.model.LinkedFormDesign;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.StudyFormDesign;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatientFormDesign;
//import com.velos.services.model.FormInfo;
//import com.velos.services.model.FormList;
//import com.velos.services.model.LinkedFormDesign;
//import com.velos.services.model.NumberFieldValidations;
//import com.velos.services.model.StudyFormDesign;
//import com.velos.services.model.StudyIdentifier;
//import com.velos.services.model.StudyPatientFormDesign;
//import com.velos.services.model.TextFieldValidations;
import com.velos.services.model.StudyPatientScheduleFormDesign;

/**
 * @author Kanwaldeep
 *
 */
@WebService(
		serviceName="FormService",
		endpointInterface="com.velos.webservices.FormSEI",
		targetNamespace="http://velos.com/services/")

public class FormWS implements FormSEI {

	public FormDesign getLibraryFormDesign(FormIdentifier formIdentifier, boolean includeFormatting, String formName)
			throws OperationException {
		return FormRemoteObj.getLibraryFormDesign(formIdentifier, includeFormatting, formName); 
		}

	/* (non-Javadoc)
	 * @see com.velos.webservices.FormSEI#getStudyFormDesign(com.velos.services.model.FormIdentifier, com.velos.services.model.StudyIdentifier)
	 */
	public StudyFormDesign getStudyFormDesign(FormIdentifier formIdentifier,
			StudyIdentifier studyIdentifier, String formName, boolean includeFormatting) throws OperationException {
		return FormRemoteObj.getStudyFormDesign(formIdentifier, studyIdentifier, formName, includeFormatting);
	}
//
//	/* (non-Javadoc)
//	 * @see com.velos.webservices.FormSEI#getAccountFormDesign(com.velos.services.model.FormIdentifier, boolean, java.lang.String)
//	 */
//	public LinkedFormDesign getAccountFormDesign(FormIdentifier formIdentifier,
//			boolean includeFormatting, String formName)
//			throws OperationException {
//		// TODO Auto-generated method stub
//		return FormRemoteObj.getAccountFormDesign(formIdentifier, formName);
//	}
//
//	/* (non-Javadoc)
//	 * @see com.velos.webservices.FormSEI#getPatientFormDesign(com.velos.services.model.FormIdentifier, boolean, java.lang.String)
//	 */
/*	public FormDesign getPatientFormDesign(FormIdentifier formIdentifier,
			 String formName,boolean includeFormatting)
		throws OperationException {
//		// TODO Auto-generated method stub
		return FormRemoteObj.getPatientFormDesign(formIdentifier, formName,includeFormatting);
	}*/
//	
	public StudyPatientFormDesign getStudyPatientFormDesign(FormIdentifier formIdentifier,
			StudyIdentifier studyIdentifier,
			String formName,
			boolean includeFormatting) 
	throws OperationException
	{
		return FormRemoteObj.getStudyPatientFormDesign(formIdentifier, studyIdentifier, formName, includeFormatting); 
	}
//
//	/* (non-Javadoc)
//	 * @see com.velos.webservices.FormSEI#getAllFormsForStudy(com.velos.services.model.StudyIdentifier)
//	 */
//	public FormList getAllFormsForStudy(StudyIdentifier studyIdentifier)
//			throws OperationException {
//		return FormRemoteObj.getAllFormsForStudy(studyIdentifier);
//	}
	
	public LinkedFormDesign getPatientFormDesign(FormIdentifier formIdentifier,
			 String formName,boolean includeFormatting)
		throws OperationException {
//		// TODO Auto-generated method stub
		return FormRemoteObj.getPatientFormDesign(formIdentifier, formName,includeFormatting);
	}

	public StudyPatientScheduleFormDesign getStudyPatientScheduleFormDesign(
			FormIdentifier formIdentifier, StudyIdentifier studyIdentifier,
			String formName, boolean includeFormatting)
			throws OperationException {
		return FormRemoteObj.getStudyPatientScheduleFormDesign(formIdentifier, studyIdentifier, formName, includeFormatting);
	}
	
	public FormList getListOfStudyPatientForms(com.velos.services.model.PatientIdentifier patientIdentifier, StudyIdentifier studyIdentifier, int maxNumberOfResults, boolean formHasResponses)
			throws OperationException {
		return FormRemoteObj.getListOfStudyPatientForms(patientIdentifier, studyIdentifier, maxNumberOfResults, formHasResponses);
	}

	@Override
	public FormList getListOfPatientForms(PatientIdentifier patientIdentifier, boolean formHasResponses, boolean ignoreDisplayInPatFlag, 
			int pageNumber, int pageSize) 
			throws OperationException {
		return FormRemoteObj.getListOfPatientForms(patientIdentifier, formHasResponses, ignoreDisplayInPatFlag, pageNumber, pageSize);
	}
}
