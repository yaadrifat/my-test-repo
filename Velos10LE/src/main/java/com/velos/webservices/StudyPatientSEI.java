package com.velos.webservices;

import java.util.List;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.CreateMultiPatientStudyStatuses;
import com.velos.services.model.Patient;
import com.velos.services.model.PatientEnrollmentDetails;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientStudyStatusIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatient;
import com.velos.services.model.StudyPatientStatuses;
import com.velos.services.model.UpdateMPatientStudyStatuses;
/**
 * Service Endpoint Interface dealing with generation of Operations
 *  of StudyPatient(Patients in a study) services 
 * @author Virendra
 *
 */
@WebService(
		 targetNamespace="http://velos.com/services/")

	public interface StudyPatientSEI {
		/**
		 * public method of Study Patient Service Endpoint Interface
		 * which calls getStudyPatient method of StudyPatient webservice
		 * and returns List of StudyPatient
		 * @param StudyId
		 * @return List<StudyPatient>
		 * @throws OperationException
		 */
		@WebResult(name = "StudyPatient" )
		@POST
		@Path("/getstudypatients/")
		public List<StudyPatient> getStudyPatients(
		@WebParam(name = "StudyIdentifier")		
		StudyIdentifier StudyId)
				throws OperationException;
		
		@POST
		@Path("/enrollpatienttostudy/")
		@WebResult(name = "Response")
		public abstract ResponseHolder enrollPatientToStudy(
				@WebParam(name="PatientIdentifier")
				PatientIdentifier patientIdentifier,
				@WebParam(name = "StudyIdentifier")
				 StudyIdentifier studyIdentifier, 
				@WebParam(name = "PatientEnrollmentDetails") 
				PatientEnrollmentDetails patientEnrollmentDetails)
				throws OperationException, OperationRolledBackException;
		
		@POST
		@Path("/createAndEnrollPatient/")
		@WebResult(name="Response")
		public abstract ResponseHolder createAndEnrollPatient(
				@WebParam(name ="Patient")
				Patient patient,
				@WebParam(name = "StudyIdentifier")
				 StudyIdentifier studyIdentifier, 
				@WebParam(name = "PatientEnrollmentDetails")
				PatientEnrollmentDetails patientEnrollmentDetails)
				throws OperationException, OperationRolledBackException; 
		
		@POST
		@Path("/deletestudypatientstatus/")
		@WebResult(name="Response")
		public ResponseHolder deleteStudyPatientStatus(
				@WebParam(name="PatientStudyStatusIdentifier") PatientStudyStatusIdentifier patientStudyStatusIdentifier, 
				@WebParam(name="reasonForDelete")String reasonForDelete)
				throws OperationException;
		
		@POST
		@Path("/updatestudypatientstatus/")
		@WebResult(name="Response")
		public ResponseHolder updateStudyPatientStatus(
				@WebParam(name="PatientStudyStatusIdentifier")
				PatientStudyStatusIdentifier patientStudyStatusIdentifier,
				@WebParam(name="PatientEnrollmentDetails")
				PatientEnrollmentDetails patientEnrollmentDetails)
		throws OperationException;
		
		@POST
		@Path("/addstudypatientstatus/")
		@WebResult(name="Response")
		public ResponseHolder addStudyPatientStatus(
				@WebParam(name="PatientStudyStatus")
				PatientEnrollmentDetails patientStudyStatus, 
				@WebParam(name="PatientIdentifier")
				PatientIdentifier patientIdentifier,
				@WebParam(name="StudyIdentifier")
				StudyIdentifier studyIdentifier)
		throws OperationException; 
		
		@POST
		@Path("/getstudypatientstatushistory/")
		@WebResult(name="PatientStudyStatuses")
		public StudyPatientStatuses getStudyPatientStatusHistory(
				@WebParam(name="StudyIdentifier")
				StudyIdentifier studyIdentifier, 
				@WebParam(name="PatientIdentifier")
				PatientIdentifier patientIdentifier)
		throws OperationException;
		
		@POST
		@Path("/getstudypatientstatus/")
		@WebResult(name="PatientEnrollmentDetails")
		public PatientEnrollmentDetails getStudyPatientStatus(
				@WebParam(name="PatientStudyStatusIdentifier")
				PatientStudyStatusIdentifier patientStudyStatusIdentifier)
		throws OperationException;
		
		@POST
		@Path("/creatempatientstudystatus/")
		@WebResult(name="Response")
		public ResponseHolder createMPatientStudyStatus(
				@WebParam(name="CreateMultiPatientStudyStatuses")
				CreateMultiPatientStudyStatuses createMPatientStudyStatuses
				)
		throws OperationException;

		
		@POST
		@Path("/updateMPatientStudyStatus/")
		@WebResult(name="Response")
		public ResponseHolder updateMPatientStudyStatus(
				@WebParam(name="UpdateMPatientStudyStatus")
				UpdateMPatientStudyStatuses updateMPatientStudyStatuses
				)
		throws OperationException;
}