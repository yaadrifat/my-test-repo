package com.velos.webservices;

import java.util.ArrayList;
import java.util.HashMap;

import javax.jws.WebService;

import org.apache.log4j.Logger;

import com.velos.services.OperationException;
import com.velos.services.client.PatientScheduleClient;
import com.velos.services.client.StudyMashupClient;
import com.velos.services.model.EventStatusHistory;
import com.velos.services.model.FilterParams;
import com.velos.services.model.Modules;
import com.velos.services.model.StudyFilterFormResponse;
import com.velos.services.model.StudyFormResponse;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyMashup;
import com.velos.services.model.StudyMashupDetailsResponse;

/**
 * WebService class for StudyMashup
 * @author Tarandeep Singh Bali
 *
 */

@WebService(
		serviceName="StudyMashupScheduleService",
		endpointInterface="com.velos.webservices.StudyMashupSEI",
		targetNamespace="http://velos.com/services/")

public class StudyMashupWS implements StudyMashupSEI {
	
	private static Logger logger = Logger.getLogger(StudyMashupWS.class.getName());
	
	public StudyMashupWS(){
	}

	@Override
	public StudyMashup getStudyMashup(StudyIdentifier studyId)
			throws OperationException {
		StudyMashup studyMashup = StudyMashupClient.getStudyMashup(studyId);
		return studyMashup;
	}
	
	public StudyFormResponse getStudyFormResponse(Integer formPK)
			throws OperationException {
		StudyFormResponse studyFormResponse = StudyMashupClient.getStudyFormResponse(formPK);
		return studyFormResponse;
	}
	
	public StudyFilterFormResponse getFilterStudyFormResponse(FilterParams formFilterParams)
			throws OperationException {
		StudyFilterFormResponse studyFilterFormResponse = StudyMashupClient.getFilterStudyFormResponse(formFilterParams);
		return studyFilterFormResponse;
	}
	
	public StudyMashupDetailsResponse getStudyMashupDetails(StudyIdentifier studyIdentifier, Integer formPK, Modules modules)
			throws OperationException {
		StudyMashupDetailsResponse studyMashupDetailsResponse = StudyMashupClient.getStudyMashupDetails(studyIdentifier, formPK, modules);
		return studyMashupDetailsResponse;
	}

}
