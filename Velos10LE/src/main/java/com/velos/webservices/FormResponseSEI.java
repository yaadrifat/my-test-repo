/**
 * Created On Sep 1, 2011
 */
package com.velos.webservices;
import javax.jws.WebService;
import javax.jws.WebResult;
import javax.jws.WebParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlSeeAlso;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.CalendarIdentifier;
import com.velos.services.model.DateFieldValidations;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.FormIdentifier;
import com.velos.services.model.FormResponse;
import com.velos.services.model.NumberFieldValidations;
import com.velos.services.model.PatientFormResponse;
import com.velos.services.model.PatientFormResponseIdentifier;
import com.velos.services.model.PatientFormResponses;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.StudyFormResponse;
import com.velos.services.model.StudyFormResponseIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatientFormResponse;
import com.velos.services.model.StudyPatientFormResponseIdentifier;
import com.velos.services.model.StudyPatientFormResponses;
import com.velos.services.model.StudyPatientScheduleFormResponse;
import com.velos.services.model.TextFieldValidations;
import com.velos.services.model.VisitIdentifier;

/**
 * @author Kanwaldeep
 *
 */

@WebService(
		 targetNamespace="http://velos.com/services/")
@XmlSeeAlso({TextFieldValidations.class,
	DateFieldValidations.class,
	NumberFieldValidations.class
	})
@Produces({"application/json", "application/xml"})
public interface FormResponseSEI {
	
	@POST
	@Consumes("application/json")
	@Path("/createstudyformresponse/")
	@WebResult(name = "Response")
	public ResponseHolder createStudyFormResponse(
			@WebParam(name = "StudyFormResponse")
			StudyFormResponse studyFormResponse)
	throws OperationException; 
	
	
	@POST
	@Consumes("application/json")
	@Path("/removestudyformresponse/")
	@WebResult(name= "Response")
	public ResponseHolder removeStudyFormResponse(
			@WebParam(name = "FormResponseIdentifier")
			StudyFormResponseIdentifier formResponseIdentifier)
	throws OperationException; 

	@POST
	@Consumes("application/json")
	@Path("/getstudyformresponse")
	@WebResult(name="StudyFormResponse")
	public StudyFormResponse getStudyFormResponse(
			@WebParam(name = "FormResponseIdentifier")
			StudyFormResponseIdentifier formResponseIdentifier)
	throws OperationException;
	
	@POST
	@Consumes("application/json")
	@Path("/createstudypatientformresponse/")
	@WebResult(name= "Response")
	public ResponseHolder createStudyPatientFormResponse(
			@WebParam(name = "StudyPatientFormResponse")
			StudyPatientFormResponse studyPatientFormResponse)
	throws OperationException; 
	@POST
	@Consumes("application/json")
	@Path("/getstudypatientformresponse/")
	@WebResult(name= "StudyPatientFormResponse")
	public  StudyPatientFormResponse getStudyPatientFormResponse(
			@WebParam(name = "FormResponseIdentifier")
			StudyPatientFormResponseIdentifier formResponseIdentifier)
	throws OperationException; 
	
	
	@POST
	@Consumes("application/json")
	@Path("/removestudypatientformresponse/")
	@WebResult(name= "Response")
	public  ResponseHolder removeStudyPatientFormResponse(
			@WebParam(name = "FormResponseIdentifier")
			StudyPatientFormResponseIdentifier formResponseIdentifier)
	throws OperationException; 
//	
//	@WebResult(name= "Response")
//	public ResponseHolder createAccountFormResponse(
//			@WebParam(name = "AccountFormResponse")
//			AccountFormResponse accountFormResponse)
//	throws OperationException; 
//	
//	@WebResult(name= "Response")
//	public ResponseHolder removeAccountFormResponse(
//			@WebParam(name = "FormResponseIdentifier")
//			AccountFormResponseIdentifier formResponseIdentifier)
//	throws OperationException; 
//	
//	public AccountFormResponse getAccountFormResponse(
//			@WebParam(name = "FormResponseIdentifier")
//			AccountFormResponseIdentifier formResponseIdentifier)
//	throws OperationException; 
//	
/*
	@WebResult(name= "Response")
	public ResponseHolder createPatientFormResponse(
			@WebParam(name = "PatientFormResponse")
			PatientFormResponse patientFormResponse)
	throws OperationException; */
	@POST
	@Consumes("application/json")
	@Path("/createpatientformresponse/")
	@WebResult(name= "Response")
	public ResponseHolder createPatientFormResponse(
			@WebParam(name = "PatientFormResponse")
			PatientFormResponse patientFormResponse)
	throws OperationException; 
	
//	@WebResult(name= "Response")
//	public ResponseHolder createPatientFormResponse(
//			@WebParam(name = "PatientFormResponse")
//			PatientFormResponse patientFormResponse)
//	throws OperationException; 
	@POST
	@Consumes("application/json")
	@Path("/removepatientformresponse/")
	@WebResult(name= "Response")
	public ResponseHolder removePatientFormResponse(
			@WebParam(name = "FormResponseIdentifier")
			PatientFormResponseIdentifier formResponseIdentifier)
	throws OperationException; 
//	
	public PatientFormResponse getPatientFormResponse(
			@WebParam(name = "FormResponseIdentifier")
			PatientFormResponseIdentifier formResponseIdentifier)
	throws OperationException; 
	@POST
	@Consumes("application/json")
	@Path("/updatepatientformresponse/")
	@WebResult(name= "Response")
	public ResponseHolder updatePatientFormResponse(
			@WebParam(name = "FormResponseIdentifier")
			PatientFormResponseIdentifier formResponseIdentfier,
			@WebParam(name = "PatientFormResponse")
			PatientFormResponse patientFormResponse)
	throws OperationException; 
	@POST
	@Consumes("application/json")
	@Path("/updatepatientformresponse/")
	@WebResult(name= "Response")
	public ResponseHolder updateStudyPatientFormResponse(
			@WebParam(name = "FormResponseIdentifier")
			StudyPatientFormResponseIdentifier formResponseIdentifier,
			@WebParam(name = "StudyPatientFormResponse")
			FormResponse studyPatientFormResponse)
	throws OperationException; 
	@POST
	@Consumes("application/json")
	@Path("/updatestudyformresponse/")
	@WebResult(name= "Response")
	public ResponseHolder updateStudyFormResponse(
			@WebParam(name = "FormResponseIdentifier")
			StudyFormResponseIdentifier formResponseIdentifier,
			@WebParam(name = "StudyFormResponse")
			StudyFormResponse studyFormResponse)
	throws OperationException; 
//	
//	@WebResult(name= "Response")
//	public ResponseHolder updateAccountFormResponse(
//			@WebParam(name = "FormResponseIdentifier")
//			AccountFormResponseIdentifier formResponseIdentifier,
//			@WebParam(name = "AccountFormResponse")
//			AccountFormResponse accountFormResponse)
//	throws OperationException; 
//	@WebResult(name="Response")
//	public ResponseHolder getStudyPatientScheduleFormResponse(
//			@WebParam(name="FormResponseIdentifier")
//			StudyPatientScheduleFormResponseIdentifier formResponseIdentifier)
//	throws OperationException;
	
	@POST
	@Consumes("application/json")
	@Path("/createstudypatientscheduleformresponse/")
	@WebResult(name="Response")
	public ResponseHolder createStudyPatientScheduleFormResponse(
			@WebParam(name = "StudyPatientScheduleFormResponse")
			StudyPatientScheduleFormResponse studyPatientScheduleFormResponse,
			@WebParam(name="CalendarIdentifier")
			CalendarIdentifier calendarIdentifier,
			@WebParam(name="CalendarName")
			String calendarName,
			@WebParam(name="VisitIdentifier")
			VisitIdentifier visitIdentifier,
			@WebParam(name="VisitName")
			String visitName,
			@WebParam(name="EventIdentifier")
			EventIdentifier eventIdentifier,
			@WebParam(name = "EventName")
			String eventName			
			)
	throws OperationException; 
	@POST
	@Consumes("application/json")
	@Path("/getstudypatientscheduleformresponse/")
	@WebResult(name="StudyPatientScheduleFormResponse")
	public StudyPatientScheduleFormResponse getStudyPatientScheduleFormResponse(
			@WebParam(name = "StudyPatientScheduleFormResponseIdentifier")
			StudyPatientFormResponseIdentifier studyPatientFormResponseIdentifier)
	throws OperationException;
	@POST
	@Consumes("application/json")
	@Path("/updatestudypatientscheduleformresponse")
	@WebResult(name="Response")
	public ResponseHolder updateStudyPatientScheduleFormResponse(
			@WebParam(name="StudyPatientScheduleFormResponseIdentifier")
			StudyPatientFormResponseIdentifier formResponseIdentifier,
			@WebParam(name="StudyPatientScheduleFormResponse")
			StudyPatientScheduleFormResponse studyPatientScheduleFormResponse)
	throws OperationException;
	
	@POST
	@Consumes("application/json")
	@Path("/removestudypatientscheduleformresponse")
	@WebResult(name="Response")
	public ResponseHolder removeStudyPatientScheduleFormResponse(
			@WebParam(name="StudyPatientScheduleFormResponseIdentifier")
			StudyPatientFormResponseIdentifier studyPatientFormResponseIdentifier)
	throws OperationException;
	@POST
	@Consumes("application/json")
	@Path("/getlistofstudypatientformresponses/")
	@WebResult(name="StudyPatientFormResponse")
	public StudyPatientFormResponses getListOfStudyPatientFormResponses(
			@WebParam(name="FormIdentifier")
			FormIdentifier formIdentifier,
			@WebParam(name="PatientIdentifier")
			PatientIdentifier patientIdentifier,
			@WebParam(name="StudyIdentifier")
			StudyIdentifier studyIdentifier,
			@WebParam(name="PageNumber")
			int pageNumber,
			@WebParam(name="PageSize")
			int pageSize) throws OperationException;
	@POST
	@Consumes("application/json")
	@Path("/getlistofpatientformresponse/")
	@WebResult(name="PatientFormResponse")
	public PatientFormResponses getListOfPatientFormResponses(
			@WebParam(name="FormIdentifier")
			FormIdentifier formIdentifier,
			@WebParam(name="PatientIdentifier")
			PatientIdentifier patientIdentifier,
			@WebParam(name="pageNumber")
			int pageNumber,
			@WebParam(name="pageSize")
			int pageSize
			) throws OperationException;
}
