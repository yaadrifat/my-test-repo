package com.velos.webservices;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.servlet.ServletContext;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import org.apache.log4j.Logger;

import com.velos.eres.service.util.Configuration;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.client.MonitoringClient;
import com.velos.services.model.MessageTraffic;
import com.velos.services.model.MonitorVersion;
/**
 * WebServices class dealing with all Monitoring Services operations.
 * @author virendra
 *
 */
@WebService(
		serviceName = "MonitoringService", 
		endpointInterface = "com.velos.webservices.MonitoringSEI", 
		targetNamespace = "http://velos.com/services/")	 
public class MonitoringWS implements MonitoringSEI{
	
	private static Logger logger = Logger.getLogger(MonitoringWS.class.getName());
	ResponseHolder response = new ResponseHolder();
	
	@Resource
	private WebServiceContext context;
	
	@Resource
	private org.apache.cxf.jaxrs.ext.MessageContext jaxrsContext;
	
	public MonitoringWS() { 
		
	}
	/**
	 * gets the runtime status of eSP database.
	 * @return response
	 */
	public ResponseHolder getHeartBeat()
			throws OperationException, OperationRolledBackException {
		
		ResponseHolder response = new ResponseHolder();
		response = MonitoringClient.getHeartBeat();
		return response;
	}

	/**
	 * getMonitorVersion() reads the MANIFEST.MF file inside webservices.war appended
	 * for build version during Ant Build and returns the MonitorVersion object with
	 * eSPBuildNumber,eSPBuildDate,eSPVersionNumber,eResearchCompatibilityVersionNumber
	 * @return MonitorVersion
	 * 
	 */
	public MonitorVersion getMonitorVersion() throws OperationException,
			OperationRolledBackException {
		
		Manifest manifest = null;
		InputStream inputStream = null;
		Issue issue = new Issue();
		
		ServletContext sContext; 
		if(context != null)
		{
			 sContext = 
			(ServletContext)context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
		}else
		{
			 sContext = jaxrsContext.getServletContext();
		}
		
		try{
			inputStream = sContext.getResourceAsStream("/META-INF/MANIFEST.MF");
			manifest = new Manifest(inputStream);
		}
		catch(IOException e){
			issue = new Issue(IssueTypes.EXECPTION_WHILE_READING_MANIFEST_FILE, 
					"Exception while reading manifest file"+inputStream+","+manifest );
			response.addIssue(issue);
			throw new OperationException();
		}
		
		Attributes attributes = manifest.getMainAttributes();
		//if null, add issue and throw operation exception
		if(attributes == null){
				issue = new Issue(IssueTypes.EXECPTION_WHILE_GETTING_ATTRIBUTES_FROM_MANIFEST_FILE,
						"Exception while getting attributes from manifest file:"+attributes);
				response.addIssue(issue);
				throw new OperationException();
			}
			String eSPBuildNumber = attributes.getValue("esp-build-number");
			String eSPBuildDate = attributes.getValue("esp-build-date");
			//Virendra:#5928, Converting the date format into standard format.
			String YYYY= "20"+eSPBuildDate.substring(4, 6);
			String DD=eSPBuildDate.substring(2, 4);
			String MM=eSPBuildDate.substring(0, 2);
			String espBuildDate1=YYYY+"-"+MM+"-"+DD;
			
			String eSPVersionNumber = attributes.getValue("esp-version-number");
			String eResearchCompatibilityVersionNumber = attributes.getValue("eresearch-compatibility-version");
			MonitorVersion monitorVersion = new MonitorVersion();
			monitorVersion.seteSPBuildNumber(eSPBuildNumber);
			monitorVersion.seteSPBuildDate(espBuildDate1);  
			monitorVersion.seteSPVersionNumber(eSPVersionNumber);
			monitorVersion.seteResearchCompatibilityVersionNumber(eResearchCompatibilityVersionNumber);
			monitorVersion.setDateFormat(Configuration.getAppDateFormat());
			
		return monitorVersion;
		}
	/**
	 * Returns message traffic with for All messages count, Succeeded messages count, 
	 * and Failed messages count within specified date range.
	 * @return MessageTraffic
	 */
	public MessageTraffic getMessageTraffic(Date fromDate, Date toDate)
		throws OperationException, OperationRolledBackException {
		
		MessageTraffic messageTraffic = MonitoringClient.getMessageTraffic(fromDate, toDate);
	
	return messageTraffic;
	}

	

}
