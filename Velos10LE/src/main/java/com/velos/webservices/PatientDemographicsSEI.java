package com.velos.webservices;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.Patient;
import com.velos.services.model.PatientDemographics;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientIdentifiers;
import com.velos.services.model.PatientOrganization;
import com.velos.services.model.PatientOrganizationIdentifier;
import com.velos.services.model.PatientOrganizations;
import com.velos.services.model.PatientSearch;
import com.velos.services.model.PatientSearchResponse;
import com.velos.services.model.PatientSurvivalStatuses;
import com.velos.services.model.Patients;
import com.velos.services.model.UpdatePatientDemographics;
/**
 * Service Endpoint Interface dealing with generation of Operations
 *  of Patient demographics services
 * @author Virendra
 *
 */
@WebService(
		 targetNamespace="http://velos.com/services/")
@Produces({"application/json","application/xml"})
@Consumes
	public interface PatientDemographicsSEI {
		/***
		 * public method takes PatientIdentifier as input
		 * and returns PatientDemographics object for the patient 
		 * @param patientId
		 * @return
		 * @throws OperationException
		 */
		@POST
		@Path("/getpatientdemographics/")
		@WebResult(name = "PatientDemographics" )
		public abstract PatientDemographics getPatientDemographics(
		@WebParam(name = "PatientIdentifier")		
		PatientIdentifier patientId)
				throws OperationException;
		
		@POST
		@Path("/createpatient/")
		@WebResult(name = "Response")
		public abstract ResponseHolder createPatient(
				@WebParam(name = "Patient")
				Patient patient)
		throws OperationException, 
		OperationRolledBackException; 
		
		@POST
		@Path("/checkAuthForUpdatePatient/")
		@WebResult(name="ResponseHolder")
		public abstract ResponseHolder checkAuthForUpdatePatient(
				  @WebParam(name="PatientIdentifier") 
				  PatientIdentifier paramPatientIdentifer, 
				  @WebParam(name="PatientDemographics") 
				  UpdatePatientDemographics paramPatientUpdateDemographics)
		throws OperationException, OperationRolledBackException;

		@POST
		@Path("/updatepatient/")
		@WebResult(name="ResponseHolder")
		public abstract ResponseHolder updatePatient(
				  @WebParam(name="PatientIdentifier") 
				  PatientIdentifier paramPatientIdentifer, 
				  @WebParam(name="PatientDemographics") 
				  UpdatePatientDemographics paramPatientUpdateDemographics)
		throws OperationException, OperationRolledBackException;
		
		@POST
		@Path("/addpatientfacility/")
		@WebResult(name="addPatientFacility")
		public ResponseHolder addPatientFacility(
				@WebParam(name="PatientIdentifier")
				PatientIdentifier patId,
				@WebParam(name="PatientOrganization")
				PatientOrganization patOrg) 
		throws OperationException, OperationRolledBackException;
		
		@POST
		@Path("/updatepatientfacility/")
		@WebResult(name="updatePatientFacility")
		public ResponseHolder updatePatientFacility(
				@WebParam(name="PatientIdentifier")
				PatientIdentifier patId,
				@WebParam(name="OrganizationIdentifier")
				OrganizationIdentifier orgID,
				@WebParam(name="PatientOrganizationIdentifier")
				PatientOrganizationIdentifier pat,
				@WebParam(name="PatientOrganization")
				PatientOrganization patOrgIdentifier)
		throws OperationException,OperationRolledBackException;
		
		@POST
		@WebResult(name="PatientSearchResponse")
		@Path("/searchpatient/")
		public PatientSearchResponse searchPatient(
				@WebParam(name="PatientSearch") 
				PatientSearch paramPatientIdentifier)
		throws OperationException;
		
		/**
		 * Retrieve survival statuses for multiple patients 
		 * @param patientIdentifiers
		 * @return
		 * @throws OperationException
		 */
		@POST
		@Path("/getmpatientsurvivalstatus/")
		@WebResult(name="getMPatientSurvivalStatus")
		public PatientSurvivalStatuses getMPatientSurvivalStatus(
				@WebParam(name="PatientIdentifiers")
				PatientIdentifiers patientIdentifiers)
		throws OperationException;

		
		/**
		 * Retrieve patients facilities 
		 * @param patientIdentifier
		 * @return
		 * @throws OperationException
		 */
		
		@POST
		@WebResult(name="updateMPatientDemographics")
		@Path("/updatempatientdemographics/")
		public ResponseHolder updateMPatientDemographics(
				@WebParam(name="Patients")
				Patients patients) 
		throws OperationException;

		@POST
		@Path("/getpatientfacilities/")
		@WebResult(name="PatientFacilities")
		public PatientOrganizations getPatientFacilities(
				@WebParam(name="PatientIdentifier")
				PatientIdentifier patId) 
		throws OperationException, OperationRolledBackException;
		
		@POST
		@Path("/getPatientDetails")
		@WebResult(name="PatientDetails")
		public Patient getPatientDetails(
				@WebParam(name="PatientIdentifier")
				PatientIdentifier patId)
		throws OperationException;
}