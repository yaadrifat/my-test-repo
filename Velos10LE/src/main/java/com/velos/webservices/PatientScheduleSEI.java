package com.velos.webservices;

import java.util.ArrayList;
import java.util.Date;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;


import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.EventStatus;
//import com.velos.services.model.MEventStatuses;
import com.velos.services.model.CalendarIdentifier;
import com.velos.services.model.MEventStatuses;
import com.velos.services.model.MPatientScheduleList;
import com.velos.services.model.MPatientSchedules;
import com.velos.services.model.MultipleEvents;
import com.velos.services.model.PatientProtocolIdentifier;
import com.velos.services.model.PatientSchedule;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientSchedules;
import com.velos.services.model.ScheduleEventStatuses;
import com.velos.services.model.SitesOfService;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.VisitIdentifier;
/**
 * Service Endpoint Interface dealing with generation of Operations
 *  of Patient Schedule Web Services
 * @author Virendra
 *
 */
@WebService(
		 targetNamespace="http://velos.com/services/")
@Produces({"application/json", "application/xml"})
@Consumes
	/**
	 * 
	 */
	public interface PatientScheduleSEI {
		/**
		 * public method of Patient Schedule Service Endpoint Interface
		 * which calls getPatientSchedule method of PatientSchedule webservice
		 * and returns Patient Schedule
		 * @param patientId
		 * @param studyId
		 * @return PatSchedule
		 * @throws OperationException
		 */
	@POST
	@Path("/patientschedulelist/")
	@WebResult(name="PatientSchedules")
	    public PatientSchedules getPatientScheduleList(	  
		@WebParam(name = "PatientIdentifier")		
		PatientIdentifier patientId,
		@WebParam(name = "StudyIdentifier")		
		StudyIdentifier studyId
		)
		throws OperationException;
	
	@POST
	@Path("/getpatientschedule/")
	@WebResult(name="PatientSchedule")
	    public PatientSchedule getPatientSchedule(
		@WebParam(name = "ScheduleIdentifier")		
		PatientProtocolIdentifier scheduleOID
		)
		throws OperationException;
	
	@POST
	@Path("/getcurrentpatientschedule/")
	@WebResult(name="CurrentPatientSchedule")
        public PatientSchedule getCurrentPatientSchedule(
	    @WebParam(name = "PatientIdentifier")		
	    PatientIdentifier patientId,
	    @WebParam(name= "StudyIdentifier")
	    StudyIdentifier studyId,
	    @WebParam(name = "startDate")		
	    Date startDate,
	    @WebParam(name = "endDate")		
	    Date endDate
	    )
	    throws OperationException;
	
	@POST
	@Path("/addeventstatus/")
	@WebResult(name="addScheduleEventStatus")
        public ResponseHolder addScheduleEventStatus(
        @WebParam(name= "EventIdentifier")
        EventIdentifier eventIdentifier,
        @WebParam(name= "EventStatus")
        EventStatus eventStatus
        )
        throws OperationException;
	
	@GET
	@Path("/getsitesofservice/")
	@WebResult(name="getSitesOfService")
    public SitesOfService getSitesOfService()
    throws OperationException;
	
	@POST
	@Path("/addunscheduleevents")
	@WebResult(name="addUnscheduledEvents")
	public ResponseHolder addUnscheduledEvents(
			@WebParam(name="ScheduleIdentifier")
			PatientProtocolIdentifier scheduleID,
			@WebParam(name="StudyIdentifier")
			StudyIdentifier studyIdentifier,
			@WebParam(name="PatientIdentifier")
			PatientIdentifier patientIdentifier,
			@WebParam(name="Events")
			MultipleEvents events,
			@WebParam(name="VisitIdentifier")
			VisitIdentifier visitIdentifier) 
	throws OperationException; 
	
	@POST
	@Path("/adduncheduleadminevents")
	@WebResult(name="addUnscheduledAdminEvents")
	public ResponseHolder addUnscheduledAdminEvents(
			@WebParam(name="ScheduleIdentifier")
			CalendarIdentifier calendarIdentifier,
			@WebParam(name="Events")
			MultipleEvents events,
			@WebParam(name="VisitIdentifier")
			VisitIdentifier visitIdentifier)
	throws OperationException; 
	
	@POST
	@Path("/updatemeventstatus")
	@WebResult(name="updatemeventstatus")
	public ResponseHolder updateMEventStatus(
			@WebParam(name="ScheduleEventStatuses")
			ScheduleEventStatuses scheduleEventStatuses)
	throws OperationException; 
//	
	@POST
	@Path("/createmeventstatus/")
	@WebResult(name="response")
	public ResponseHolder createMEventStatus(
			@WebParam(name="MEventStatuses")
			MEventStatuses mEventStatuses)
	throws OperationException;
	
	@POST
	@Path("/addmpatientschedules")
	@WebResult(name="addmpatientschedules")
	public MPatientScheduleList addMPatientSchedules(
			@WebParam(name="PatientSchedules")
			MPatientSchedules mPatientSchedules)
	throws OperationException;
	
//	@POST
//	@Path("/geteventstatushistory")
//	@WebResult(name="geteventstatushistory")
//	public EventStatusHistory getEventStatusHistory(
//			@WebParam(name="EventIdentifier")
//			EventIdentifier eventIdentifier,
//			@WebParam(name="sortBy")
//			String sortBy,
//			@WebParam(name="orderBy")
//			String orderBy)
//	throws OperationException;

}