package com.velos.webservices;

/**
 * This JAAS callback handler verifies that the user in a WS-Security
 * environment. Currently, only supports UsernameToken PasswordText 
 * style passwords.
 * See https://cwiki.apache.org/CXF20DOC/ws-security.html for details.	
 * @author dylan
 *
 */

import java.io.IOException;
import java.sql.Connection;

import javax.ejb.EJB;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.ws.security.WSPasswordCallback;

import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.EnvUtil;
import com.velos.eres.web.user.UserJB;

import com.velos.services.model.User.UserStatus;
/**
 * JAAS CallbackHandler used in a WS-Security profile. Supports
 * UsernameToken, PasswordText style authentication for Web Services.
 * @author dylan
 *
 */
public class WSCallbackHandler implements CallbackHandler {


	@EJB
	private UserAgentRObj userAgent;
	protected final Log logger = LogFactory.getLog(getClass());

	public WSCallbackHandler(){
	}
	
	/**
	 * Handle the password request. Only supports USERNAME_TOKEN_PASSWORD 
	 * authentication tokens.
	 */
	public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {


		
		if (!(callbacks[0] instanceof WSPasswordCallback)){
			throw new IOException("Unsupported Authentication Type");
		}
		WSPasswordCallback pc =  (WSPasswordCallback)callbacks[0];
		
		//Current support is only for text passwords. 
		if (pc.getUsage() != WSPasswordCallback.USERNAME_TOKEN_UNKNOWN){
			throw new IOException("Unsupported Password Type");
		}
		
		if (logger.isInfoEnabled()) logger.info("WSPasswordCallback attempt: " + pc.getIdentifier());

		UserJB userJB = new UserJB();
		UserBean user = null;
		Connection conn = null;
		try{
			//Virendra:#5933 For Monitoring Service Requirement
			//If cannot get database connection before authentication.
			//Throw an error, Database connection not found.
			conn = EnvUtil.getConnection();
			if(conn == null){
				throw new IOException("Error communicating to Database");				
			}
			//Digest authentication requires retriving the password in clear text
			//from the database. But we store encrypted passwords in Velos, so and
			//we don't have access to the unencrypted password from code,
			//so we don't support digest. 
			//If we can't validate, we'll throw an exception.
			user = userJB.validateUser(pc.getIdentifier(), pc.getPassword(), true);
			
			//add the userbean to the cxf interceptor chain message. This is
			//not optimum, we would really really like to limit ourselves to 
			//jaxws code, and not have anything specific to the implementation (cxf) in
			//out code. But we can see no other way of communicating the user 
			//information later in the chain. This ends up being accessible
			//from the MessageContext class in JAX-WS code.
			Message cxfMessage = PhaseInterceptorChain.getCurrentMessage();
			cxfMessage.put(Constants.USER_BEAN_KEY, user);
		
		}
		catch(Throwable t){
			throw new IOException(t.getMessage());
		}
        finally {
            try { if (conn != null) { conn.close(); } } catch (Exception e){}
        }
        
		if (user == null){
			throw new IOException("User not authenticated");
		}
		
		//DRM - fix for 5448 and 5453 - block authentication if the user
		//is not Active
		if (!user.userStatus.equalsIgnoreCase(UserStatus.ACTIVE.getLegacyString())){
			throw new IOException("User is Not Active");
		}
		
		LoginContext lctx;

		try {
	
			// invokes the standard jboss client login module...required
			// for all EJB client communication in JBOss
			lctx = new LoginContext("client-login",
					new SimpleAuthCallbackHandler(
							pc.getIdentifier()));
			lctx.login();

		} catch (LoginException e) {
			throw new IOException(e.getMessage());
		}
	}


}
