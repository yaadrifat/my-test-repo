/**
 * 
 */
package com.velos.webservices;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.xml.bind.annotation.XmlSeeAlso;

import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.CalendarEvent;
import com.velos.services.model.CalendarEventSummary;
import com.velos.services.model.CalendarEvents;
import com.velos.services.model.CalendarIdentifier;
import com.velos.services.model.CalendarSummary;
import com.velos.services.model.CalendarVisit;
import com.velos.services.model.CalendarVisitSummary;
import com.velos.services.model.CalendarVisits;
import com.velos.services.model.Cost;
import com.velos.services.model.EventCostIdentifier;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.EventIdentifiers;
import com.velos.services.model.EventNameIdentifier;
import com.velos.services.model.EventNames;
import com.velos.services.model.StudyCalendar;
import com.velos.services.model.StudyCalendarsList;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.VisitIdentifier;
import com.velos.services.model.VisitIdentifiers;
import com.velos.services.model.VisitNameIdentifier;
import com.velos.services.model.VisitNames;

/**
 * @author dylan 
 *
 */
@WebService(
		 targetNamespace="http://velos.com/services/")
@XmlSeeAlso({EventNameIdentifier.class,
	VisitNameIdentifier.class
	})
public interface StudyCalendarSEI {

	@POST
	@Path("/createstudycalendar/")
	@WebResult(name = "Response")
	public abstract ResponseHolder createStudyCalendar(
			@WebParam(name = "StudyCalendar") 
			StudyCalendar study)
			throws OperationException,OperationRolledBackException;
	
	@POST
	@Path("/updatestudycalendarsummary/")
	@WebResult(name = "Response")
	public abstract ResponseHolder updateStudyCalendarSummary(
			@WebParam(name="CalendarIdentifier")
			CalendarIdentifier studyCalendarIdentifier,
			@WebParam(name="StudyIdentifier")
			StudyIdentifier studyIdentifier, 
			@WebParam(name = "CalendarName")
			String calendarName, 
			@WebParam(name = "CalendarSummary") 
			CalendarSummary study)
			throws OperationException, OperationRolledBackException;

	@POST
	@Path("/getstudycalendar/")
	@WebResult(name = "StudyCalendar")
	public abstract StudyCalendar getStudyCalendar(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier studyCalendarIdentifier, 
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier studyIdentifier, 
			@WebParam(name = "CalendarName")
			String calendarName) 
		throws OperationException;
	
	
	//Visit Methods
	@POST
	@Path("/updatestudycalendarvisitdetails/")
	@WebResult(name = "Response")
	public abstract ResponseHolder updateStudyCalendarVisitDetails(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier studyCalendarIdentifier,
			@WebParam(name = "VisitName")
			String visitName,
			@WebParam(name = "VisitIdentifier")
			VisitIdentifier visitIdentifier,
			@WebParam(name = "Visit") 
			CalendarVisitSummary studyCalendarVisit) 
	throws OperationException,OperationRolledBackException;
	
	

	@POST
	@Path("/getstudycalendarvisit/")
	@WebResult(name="Visit")
	public abstract CalendarVisit getStudyCalendarVisit(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier calendarIdentifier,
			@WebParam(name="VisitIdentifier")
			VisitIdentifier visitIdentifier,
			@WebParam(name = "VisitName")
			String visitName) 
	throws OperationException;
	
	@POST
	@Path("/addvisitstostudycalendar/")
	@WebResult(name = "Response")
	public abstract ResponseHolder addVisitsToStudyCalendar(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier calendarIdentifier,
			@WebParam(name = "Visits") 
			CalendarVisits newVisit) 
	throws OperationException,OperationRolledBackException;
	
	@POST
	@Path("/removevisitsfromstudycalendar/")
	@WebResult(name = "Response")
	public abstract ResponseHolder removeVisitsFromStudyCalendar(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier calendarIdentifier,
			@WebParam(name= "VisitIdentifiers")
			VisitIdentifiers visitIdentifiers,
			@WebParam(name = "VisitNames")
			VisitNames visitNames) 
	throws OperationException,OperationRolledBackException;
	
	
	//Event Methods

	@POST
	@Path("/updatestudycalendareventdetails/")
	@WebResult(name = "Response")
	public abstract ResponseHolder updateStudyCalendarEventDetails(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier studyCalendarIdentifier,
			@WebParam(name = "VisitIdentifier")
			VisitIdentifier visitIdentifier, 
			@WebParam(name = "VisitName")
			String visitName,
			@WebParam(name="EventIdentifier")
			EventIdentifier eventIdentifier, 
			@WebParam(name = "EventName")
			String eventName,
			@WebParam(name = "Event") 
			CalendarEventSummary studyCalendarEvent) 
	throws OperationException,OperationRolledBackException;
	
	@POST
	@Path("/getstudycalendarevent/")
	@WebResult(name="Event")
	public abstract CalendarEvent getStudyCalendarEvent(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier calendarIdentifier,
			@WebParam(name = "VisitIdentifier")
			VisitIdentifier visitIdentifier, 
			@WebParam(name = "VisitName")
			String visitName,
			@WebParam(name = "EventIdentifier")
			EventIdentifier eventIdentifier, 
			@WebParam(name = "EventName")
			String eventName) 
	throws OperationException;
	
	@POST
	@Path("/addeventstostudycalendarvisit/")
	@WebResult(name = "Response")
	public abstract ResponseHolder addEventsToStudyCalendarVisit(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier calendarIdentifier,
			@WebParam(name = "VisitName")
			String visitName,
			@WebParam(name = "VisitIdentifer")
			VisitIdentifier visitIdentifier, 
			@WebParam(name = "Events") 
			CalendarEvents studyCalendarEvent) 
	throws OperationException,OperationRolledBackException;
	
	@POST
	@Path("/removeeventsfromstudycalendarvisit/")
	@WebResult(name = "Response")
	public abstract ResponseHolder removeEventsFromStudyCalendarVisit(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier calendarIdentifier,
			@WebParam(name= "VisitIdentifier")
			VisitIdentifier visitIdentifiers,
			@WebParam(name = "VisitName")
			String visitName,
			@WebParam(name = "EventIdentifiers")
			EventIdentifiers eventIdentifiers,
			@WebParam(name = "EventName")
			EventNames eventName) 
	throws OperationException,OperationRolledBackException;

	@POST
	@Path("/getstudycalendarsummary/")
	@WebResult(name = "CalendarSummary")
	public abstract CalendarSummary getStudyCalendarSummary(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier studyCalendarIdentifier, 
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier studyIdentifier, 
			@WebParam(name = "CalendarName")
			String calendarName) 
		throws OperationException;
	
	@POST
	@Path("/removestudycalendar/")
	@WebResult(name = "Response")
    public abstract ResponseHolder removeStudyCalendar(
    		@WebParam(name = "CalendarIdentifier")    		
    		CalendarIdentifier calendarIdentifier,
    		@WebParam(name = "StudyIdentifier")
    		StudyIdentifier studyIdentifier,
    		@WebParam(name = "CalendarName")
    		String calendarName)
	throws OperationException; 
	
	@POST
	@Path("/addeventcost/")
	@WebResult(name = "Response")
    public abstract ResponseHolder addEventCost(
    		@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier studyCalendarIdentifier,
			@WebParam(name = "VisitIdentifier")
			VisitIdentifier visitIdentifier, 
			@WebParam(name = "VisitName")
			String visitName,
			@WebParam(name="EventIdentifier")
			EventIdentifier eventIdentifier, 
			@WebParam(name = "EventName")
			String eventName,
    		@WebParam(name = "Cost")
    		Cost cost)
	throws OperationException; 
	
	@POST
	@Path("/updateeventcost/")
	@WebResult(name = "Response")
    public abstract ResponseHolder updateEventCost(
    		@WebParam(name = "EventCostIdentifier")    		
    		EventCostIdentifier eventCostIdentifier,
    		@WebParam(name = "Cost")
    		Cost cost)
	throws OperationException; 
	
	@POST
	@Path("/removeeventcost/")
	@WebResult(name = "Response")
    public abstract ResponseHolder removeEventCost(
    		@WebParam(name = "EventCostIdentifier")    		
    		EventCostIdentifier eventCostIdentifier)
	throws OperationException; 
	
	@POST
	@Path("/getstudycalendarList/")
	@WebResult(name = "StudyCalendarList")
	public abstract StudyCalendarsList getStudyCalendarList(
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier studyIdentifier) 
		throws OperationException;
	
}

