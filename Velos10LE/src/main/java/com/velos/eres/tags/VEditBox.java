/**__astcwz_cmpnt_det:
 * Author: Konesa CodeWizard
 * Date and Time Created: Wed Jul 09 14:45:39 IST 2003
 * Note: DO NOT DELETE OR MODIFY KONESA CODEWIZARD COMMENTS
 * 	STARTING WITH ///__astcwz_ and /**__astcwz_
 */
///__astcwz_model_idt#(0) 
///__astcwz_maxim_idt#(1587) 
///__astcwz_packg_idt#(1580,1582,1583,1584~,,,)
/*
 Modified by : Vishal Modified On:06/22/2004   
 */
package com.velos.eres.tags;

import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

public class VEditBox extends VInputBase {

    private int maxLength;

    private int size;

    private String dataType;

    private String format;

    private String dateCheck;

    private String formatOverRide;

    private String dateOverRide;

    private String rangeOverRide;

    private String mandatoryOverRide;

    public VEditBox() {
        this.inputLabel.setForRequired(true);
        this.dataType = "";
        this.format = "";
        this.dateCheck = "";
        this.formatOverRide = "";
        this.dateOverRide = "";
        this.rangeOverRide = "";
        this.mandatoryOverRide = "";
    }

    public VEditBox(String name, String type, VLabel editLabel) {
        this.dataType = "";
        this.format = "";
        this.dateCheck = "";
        this.formatOverRide = "";
        this.dateOverRide = "";
        this.rangeOverRide = "";
        this.mandatoryOverRide = "";
        this.name = name;
        this.type = type;
        this.inputLabel = editLabel;
        this.inputLabel.setForRequired(true);

    }

    public VEditBox(String name, String type, VLabel editLabel, String dataType) {
        this.format = "";
        this.dateCheck = "";
        this.formatOverRide = "";
        this.dateOverRide = "";
        this.rangeOverRide = "";
        this.mandatoryOverRide = "";

        this.name = name;
        this.type = type;
        this.inputLabel = editLabel;
        this.dataType = dataType;
        this.inputLabel.setForRequired(true);
    }

    public int getMaxLength() {
        return maxLength;
    }

    public int getSize() {
        return size;
    }

    public void setMaxLength(int amaxLength) {
        maxLength = amaxLength;
    }

    public void setSize(int asize) {
        size = asize;
    }

    public String getDataType() {
        return this.dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getFormat() {
        return this.format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getDateCheck() {
        return this.dateCheck;
    }

    public void setDateCheck(String dateCheck) {
        this.dateCheck = dateCheck;
    }

    public String getDateOverRide() {
        return this.dateOverRide;
    }

    public void setDateOverRide(String dateOverRide) {
        this.dateOverRide = dateOverRide;
    }

    public String getFormatOverRide() {
        return this.formatOverRide;
    }

    public void setFormatOverRide(String formatOverRide) {
        this.formatOverRide = formatOverRide;
    }

    public String getRangeOverRide() {
        return this.rangeOverRide;
    }

    public void setRangeOverRide(String rangeOverRide) {
        this.rangeOverRide = rangeOverRide;
    }

    public String getMandatoryOverRide() {
        return this.mandatoryOverRide;
    }

    public void setMandatoryOverRide(String mandatoryOverRide) {
        this.mandatoryOverRide = mandatoryOverRide;
    }

    public String getSizeAttribute() {

        // get size
        if (this.size > 0) {
            return " size = \"" + this.size + "\"";
        } else {
            return "";

        }

    }

    public String getMaxLengthAttribute() {

        // get name
        if (this.maxLength > 0) {
            return " maxlength = \"" + this.maxLength + "\"";
        } else {
            return "";

        }

    }

    public String getDataTypeAttribute() {
        return " flddatatype = \"" + this.dataType + "\"";
    }

    public String getValueAttribute() {

        // get value attribute
        if (!EJBUtil.isEmpty(this.name)) {
            // return " value = \"{$" + this.name + "}\"" ;

        	//INF-19817 Number formatting
        	/*if(("N").equalsIgnoreCase(this.dataType) && (this.format==null || this.format.isEmpty())){
        		return "<xsl:attribute name=\"value\"><xsl:value-of select=\"format-number("
        		+ this.name + ",$floatFormat,'floatFormatDef')\"/></xsl:attribute>";
        	}else{*/
        		return "<xsl:attribute name=\"value\"><xsl:value-of select=\""
                    + this.name + "\"/></xsl:attribute>";
        	//}
        } else {
            return "";

        }

    }

    /*
     * public String getHiddenValueAttribute(String str) { String hdnAtt =
     * "hdn_" +str+ "_" +this.name; //get value attribute if (!
     * EJBUtil.isEmpty(this.name)) { //return " value = \"{$" + this.name +
     * "}\"" ;
     * 
     * return "<xsl:attribute name=\"value\"><xsl:value-of select=\"" +hdnAtt +
     * "\"/></xsl:attribute>" ;
     *  } else { return "";
     *  }
     *  }
     */
    public String getValueVariableTag() {

        // get value variable tag
        if (!EJBUtil.isEmpty(this.name)) {
            return " <xsl:variable name =  \"" + this.name
                    + "\" select = \"rowset/" + this.name + "\" />";
        } else {
            return "";

        }

    }

    /*
     * *************************History*********************************
     * Modified by : Vishal Modified On:06/22/2004 Comment: Changed the paths of
     * calender.gif. Url path should be specified with "/" descriptor instead of
     * "\\"
     * 
     * Modified by : Chennai Modified On: 12/28/2004 Comment: Changed the scroll
     * bar movement
     * 
     * *************************END****************************************
     */
    public String getDateLink() {

        // check if editbox type is Date, return link for date

        if (dataType.equals("D")) {
            // return "<A href= \"#\" onClick=\"return
            // fnShowCalendar(document.er_fillform1." + this.name + ")\"><img
            // src=\"../jsp/images/calendar.jpg\" border=\"0\" ></img></A>" ;
        	
        	//System.out.println("new");
        	
            return "<A onClick=\"javascript:return fnShowCalendar(document.er_fillform1."
                    + this.name
                    + ")\"><img src=\"../jsp/images/calendar.jpg\" border=\"0\" onMouseover=\"this.style.cursor='hand'\" onMouseOut=\"this.style.cursor='arrow'\"></img></A>";

        } else {
            return "";

        }

    }

    /**
     * Method to draw XSL for Edit Box Field *
     * 
     * @return xslString
     */
    /*
     * *************************History*********************************
     * Modified by : Anu Modified On:06/17/2004 Comment: Removed the calendar
     * gif incase the date field is disabled.
     * 
     * *************************END****************************************
     */

    public String drawXSL() {

        StringBuffer xslStr = new StringBuffer();
        String str = "";

        // for attribute required incase of editbox
        this.inputLabel.setForRequired(true);

        Rlog.debug("common", "**** in ED .drawxsl align ::" + align);
        if (StringUtil.isEmpty(align)) {
            align = "";
        }

        // if field label is hidden, alignment=top is not applicable

        String hideLabelStr = "";
        hideLabelStr = getHideLabel();
        if (StringUtil.isEmpty(hideLabelStr)) {
            hideLabelStr = "0";
        }

        if (hideLabelStr.equals("1") && align.equals("top")) {
            align = "left";
        }

        // get input label XSL
        if (!align.equals("top")) {
            xslStr.append(getInputLabelXSL());
        }
        xslStr.append("<td>");

        // get input label XSL
        if (align.equals("top")) {
            xslStr.append(getInputLabelXSL());
        }

        xslStr.append("<span id=\""+this.name+"_span\"><input ");
        xslStr.append(getTagClass());
        Rlog.debug("common", "inside drawxsl getTagClass::" + getTagClass());
        xslStr.append(getStyle());
        Rlog.debug("common", "inside drawxsl getStyle()::" + getStyle());
        // xslStr.append(getTitle());
        xslStr.append(getToolTip());
        Rlog.debug("common", "inside drawxsl getToolTip()::" + getToolTip());
        xslStr.append(getId());
        Rlog.debug("common", "inside drawxsl getId()::" + getId());

        xslStr.append(getDisabledAttribute());
        Rlog.debug("common", "inside drawxsl getDisabledAttribute()::"
                + getDisabledAttribute());

        xslStr.append(getNameAttribute());
        Rlog.debug("common", "inside drawxsl getNameAttribute()::"
                + getNameAttribute());

        xslStr.append(getReadOnlyAttribute());
        Rlog.debug("common", "inside drawxsl getReadOnlyAttribute()::"
                + getReadOnlyAttribute());

        xslStr.append(getDataTypeAttribute());
        
        xslStr.append(getTypeAttribute());
        Rlog.debug("common", "inside drawxsl getTypeAttribute()::"
                + getTypeAttribute());
        // xslStr.append(getAlignAttribute());
        xslStr.append(getMaxLengthAttribute());
        Rlog.debug("common", "inside drawxsl getMaxLengthAttribute()::"
                + getMaxLengthAttribute());
        xslStr.append(getSizeAttribute());
        Rlog.debug("common", "inside drawxsl getSizeAttribute()::"
                + getSizeAttribute());

        // sonia 05/11/04 for field Actions
        xslStr.append(getFieldActionJavaScript());
        Rlog.debug("common", "inside drawxsl getFieldActionJavaScript()::"
                + getFieldActionJavaScript());
        
        xslStr.append(getFieldActionJavaScriptForDate());
        //JUL-20-2011,BK,Datepicker
        if (dataType.equals("D")){ //SM-FIX #7156
	        if (!getDisabled()) {
	            // xslStr.append(getDateLink());
	         	xslStr.append( getDatePickerActionClass());
	         }
        }/*else if(dataType.equals("N") && (format==null ||format.isEmpty())){
        	if (!getDisabled()) {
        		xslStr.append( getNumFormatClass());
        	}
        }*/
        xslStr.append(" >");
     
        xslStr.append(getValueAttribute());
        Rlog.debug("common", "inside drawxsl getValueAttribute()::"
               + getValueAttribute());
        xslStr.append("</input>");
        
        /*Modified by Sonia Abrol, 11/03/05, add the date link after the input tag is finished, otherwise it gives problem in xalan/oracle java based parsers*/
        /*Modified again on 12/13/05, the previous change was rollbacked*/
        xslStr.append("    </span>"); //for input element
        
        Rlog.debug("common", "inside drawxsl xslStr()::" + xslStr.toString());
        // xslStr.append(getHelpIcon());
        Rlog.debug("common", "inside drawxsl mandatoryOverRide::"
                + mandatoryOverRide);
        Rlog
                .debug("common", "inside drawxsl formatOverRide::"
                        + formatOverRide);
        Rlog.debug("common", "inside drawxsl dateOverRide::" + dateOverRide);
        Rlog.debug("common", "inside drawxsl rangeOverRide::" + rangeOverRide);

        if (mandatoryOverRide == null)
            mandatoryOverRide = "0";
        if (mandatoryOverRide.equals("1")) {
            Rlog.debug("common", "inside mandatoryOverRide*****");
            str = "mnd";
            /*
             * strBuf = appendHiddenFields(xslStr, str); xslBuf = ""; xslBuf =
             * xslBuf.append(strBuf);
             */
            xslStr.append("<input ");
            Rlog.debug("common", "inside drawxsl 1 getHiddenId(str)::"
                    + getHiddenId(str));
            xslStr.append(getHiddenId(str));
            Rlog.debug("common",
                    "inside drawxsl 1 getHiddenNameAttribute(str)::"
                            + getHiddenNameAttribute(str));
            xslStr.append(getHiddenNameAttribute(str));
            Rlog.debug("common", "inside drawxsl 1 getHiddenType(str)::"
                    + getHiddenType());
            xslStr.append(getHiddenType());
            xslStr.append(" >");
            Rlog.debug("common",
                    "inside drawxsl 1 getHiddenValueAttribute(str)::"
                            + getHiddenValueAttribute(str));
            xslStr.append(getHiddenValueAttribute(str));
            xslStr.append("</input>");
            Rlog.debug("common", "inside drawxsl before to xsl");
            Rlog.debug("common", "inside drawxsl 1 xslStr()::"
                    + xslStr.toString());
            Rlog.debug("common", "inside drawxsl after to xsl");

            // changed by sonia, add an input fld for reason

            xslStr.append("<input ");
            xslStr.append(getHiddenReasonId(str));
            xslStr.append(getHiddenReasonNameAttr(str));
            xslStr.append(getHiddenType());
            xslStr.append(" >");
            xslStr.append("</input>");
            Rlog.debug("common", "inside drawxsl after adding reason"
                    + xslStr.toString());

        }
        if (formatOverRide == null)
            formatOverRide = "0";
        if (formatOverRide.equals("1")) {
            Rlog.debug("common", "inside formatOverRide*****");
            str = "fmt";
            xslStr.append("<input ");
            xslStr.append(getHiddenId(str));
            xslStr.append(getHiddenNameAttribute(str));
            xslStr.append(getHiddenType());
            xslStr.append(" >");
            xslStr.append(getHiddenValueAttribute(str));
            xslStr.append("</input>");

            // changed by sonia, add an input fld for reason

            xslStr.append("<input ");
            xslStr.append(getHiddenReasonId(str));
            xslStr.append(getHiddenReasonNameAttr(str));
            xslStr.append(getHiddenType());
            xslStr.append(" >");
            xslStr.append("</input>");
            Rlog.debug("common", "inside drawxsl after adding reason"
                    + xslStr.toString());

        }
        if (dateOverRide == null)
            dateOverRide = "0";
        if (dateOverRide.equals("1")) {
            Rlog.debug("common", "inside dateOverRide*****");
            str = "dt";
            xslStr.append("<input ");
            xslStr.append(getHiddenId(str));
            xslStr.append(getHiddenNameAttribute(str));
            xslStr.append(getHiddenType());
            xslStr.append(" >");
            xslStr.append(getHiddenValueAttribute(str));
            xslStr.append("</input>");
            // changed by sonia, add an input fld for reason

            xslStr.append("<input ");
            xslStr.append(getHiddenReasonId(str));
            xslStr.append(getHiddenReasonNameAttr(str));
            xslStr.append(getHiddenType());
            xslStr.append(" >");
            xslStr.append("</input>");
            Rlog.debug("common", "inside drawxsl after adding reason"
                    + xslStr.toString());
        }
        if (rangeOverRide == null)
            rangeOverRide = "0";
        if (rangeOverRide.equals("1")) {
            Rlog.debug("common", "inside rangeOverRide*****");
            str = "rng";
            xslStr.append("<input ");
            xslStr.append(getHiddenId(str));
            xslStr.append(getHiddenNameAttribute(str));
            xslStr.append(getHiddenType());
            xslStr.append(" >");
            xslStr.append(getHiddenValueAttribute(str));
            xslStr.append("</input>");

            // changed by sonia, add an input fld for reason

            xslStr.append("<input ");
            xslStr.append(getHiddenReasonId(str));
            xslStr.append(getHiddenReasonNameAttr(str));
            xslStr.append(getHiddenType());
            xslStr.append(" >");
            xslStr.append("</input>");
            Rlog.debug("common", "inside drawxsl after adding reason"
                    + xslStr.toString());
        }

        Rlog.debug("common", "inside drawxsl bofore closing td");
        xslStr.append("</td>");
        Rlog.debug("common", "inside drawxsl xslStr.toString::"
                + xslStr.toString());

        return xslStr.toString();

    }

    public String getJavaScript(boolean isRequired) {
        StringBuffer js = new StringBuffer();
        String str = "";
        String validateStr = "";
        if (formatOverRide == null)
            formatOverRide = "0";
        if (dateOverRide == null)
            dateOverRide = "0";
        if (rangeOverRide == null)
            rangeOverRide = "0";
        if (mandatoryOverRide == null)
            mandatoryOverRide = "0";

        if (isRequired && mandatoryOverRide.equals("1")) {

            str = "mnd";
            validateStr = "\"Please enter data in all mandatory fields.[VELSEPLABEL]"
                    + (getFldNameForMessages()) + "\"";
            js.append(" if (!(validate_col_no_alert(\""
                    + StringUtil.replace(getFldNameForMessages(), "\r\n", "")
                    + "\",formobj." + this.name + ")))");
            js.append("formobj." + getHiddenFldName(str) + ".value = "
                    + validateStr + " ;");
            js
                    .append("else formobj." + getHiddenFldName(str)
                            + ".value = '' ;");

        } else if (isRequired && mandatoryOverRide.equals("0")) {
            js.append(" if (!(validate_col(\""
                    + StringUtil.replace(getFldNameForMessages(), "\r\n", "")
                    + "\",formobj." + this.name + "))) return false ;");
        }

        if (!EJBUtil.isEmpty(this.dataType)) {

            if (dataType.equals("N")) {
                js.append(" if (! formobj." + this.name
                        + ".disabled) { if(isDecimal(formobj." + this.name
                        + ".value) == false) { ");
                js.append(" alert(\"Please enter a valid Number\"); ");
                js.append(" formobj." + this.name + ".focus(); ");
                js.append(" return false; } }");

                // add the javascript for format check
                if (!EJBUtil.isEmpty(this.format) && formatOverRide.equals("1")) {
                    str = "fmt";
                    validateStr = "\"Please enter data in "
                            + this.format
                            + " format.[VELSEPLABEL]"
                            + StringUtil.replace(getFldNameForMessages(),
                                    "\r\n", "") + "\"";

                    js.append(" if (!(validateDataFormatNoAlert(formobj."
                            + this.name + ",\"" + this.format + "\")))  ");
                    js.append("formobj." + getHiddenFldName(str) + ".value = "
                            + validateStr + " ;");
                    js.append("else formobj." + getHiddenFldName(str)
                            + ".value = '';");
                } else if (!EJBUtil.isEmpty(this.format)
                        && formatOverRide.equals("0")) {
                    js.append(" if (!(validateDataFormat(formobj." + this.name
                            + ",\"" + this.format + "\"))) return false ; ");
                }

            } else if (dataType.equals("D")) {
                js.append(" if (! formobj." + this.name
                        + ".disabled) { if (!(validate_date(formobj."
                        + this.name + "))) return false ; }");
                // check if date check needs to be done
                if (!EJBUtil.isEmpty(this.dateCheck)) {
                    if (this.dateCheck.equals("1") && dateOverRide.equals("1")) {
                        str = "dt";
                        validateStr = "\"Date cannot be a future date.[VELSEPLABEL]"
                                + (getFldNameForMessages()) + "\"";

                        js
                                .append(" if (! formobj."
                                        + this.name
                                        + ".disabled) { if (CompareDates(formobj."
                                        + this.name
                                        + ".value,fnGetFullCurrentDate(),'&gt;')) { ");
                        js.append("formobj." + getHiddenFldName(str)
                                + ".value =  " + validateStr + " ; }");
                        js.append("else formobj." + getHiddenFldName(str)
                                + ".value = ''; } else { formobj."
                                + getHiddenFldName(str) + ".value = ''; }");

                    } else if (this.dateCheck.equals("1")
                            && dateOverRide.equals("0")) {
                        js
                                .append(" if (! formobj."
                                        + this.name
                                        + ".disabled) { if (CompareDates(formobj."
                                        + this.name
                                        + ".value,fnGetFullCurrentDate(),'&gt;')) { ");
                        js
                                .append(" alert(\"'"
                                        + (getFldNameForMessages())
                                        + "' cannot be a future date.\");");
                        js.append(" formobj." + this.name + ".focus(); ");
                        js.append(" return false; } }");
                    }

                }
            }

        }
        return js.toString();

    }

    /*
     * public String appendHiddenFields(StringBuffer strBuf, String str) {
     * 
     * strBuf.append("<input "); strBuf.append(getHiddenId(str));
     * strBuf.append(getHiddenNameAttribute(str));
     * strBuf.append(getHiddenType()); strBuf.append(" >");
     * strBuf.append(getHiddenValueAttribute(str)); strBuf.append("</input>");
     * return strBuf.toString(); }
     */

    
    public String getFieldActionJavaScriptForDate() {
        /*
         * 05/11/04 - by Sonia Sahni Implemented for Field Actions; returns
         * default onChange() script for fields
         */
        String strJS = "";
        
        if (dataType.equals("D"))
        {
        	strJS = getFieldActionJavaScript();
        	strJS =  StringUtil.replace(strJS,"onChange","onBlur");
        }
        
        return strJS;
    }
    // end of class
}
