/**
 * __astcwz_cmpnt_det: Author: Konesa CodeWizard Date and Time Created: Wed Jul
 * 09 14:42:35 IST 2003 Note: DO NOT DELETE OR MODIFY KONESA CODEWIZARD COMMENTS
 * STARTING WITH ///__astcwz_ and /**__astcwz_
 */
// /__astcwz_model_idt#(0)
// /__astcwz_maxim_idt#(1587)
// /__astcwz_packg_idt#(1580,1582,1583,1584~,,,)
package com.velos.eres.tags;

import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

// /__astcwz_class_idt#(1392!n)
public class VInputBase extends VTag {
    // /__astcwz_attrb_idt#(1406)
    protected String name;

    // /__astcwz_attrb_idt#(1407)
    protected String type;

    // /__astcwz_attrb_idt#(1408)
    protected String value;

    // /__astcwz_attrb_idt#(1411)
    protected String align;

    // /__astcwz_attrb_idt#(1412)
    protected String alt;

    // /__astcwz_attrb_idt#(1414)
    protected boolean disabled;

    // /__astcwz_attrb_idt#(1463)
    // protected String imageSrc;
    // /__astcwz_attrb_idt#(1523)
    protected boolean readOnly;

    // /__astcwz_asscn_idt#(1509|1510%:ASSOCIATION^AGGREGATION)

    protected VLabel inputLabel;

    /** The actual field name specified by the user without any formatting */
    protected String fldNameForMessages;

    /**
     * flag to hide field label in preview, Possible values - 0:No, 1:Yes
     */
    protected String hideLabel;

    // /__astcwz_default_constructor
    VInputBase() {
        align = "";
        name = "";
        type = "";
        value = "";
        alt = "";
        this.inputLabel = new VLabel();

    }

    // /__astcwz_opern_idt#()
    public String getName() {
        return name;
    }

    // /__astcwz_opern_idt#()
    public String getType() {
        return type;
    }

    // /__astcwz_opern_idt#()
    public String getValue() {
        return value;
    }

    // /__astcwz_opern_idt#()
    public String getAlign() {
        return align;
    }

    // /__astcwz_opern_idt#()
    public String getAlt() {
        return alt;
    }

    // /__astcwz_opern_idt#()
    public boolean getDisabled() {
        return disabled;
    }

    // /__astcwz_opern_idt#()

    // public String getImageSrc(){
    // return imageSrc;
    // }

    // /__astcwz_opern_idt#()
    public boolean getReadOnly() {
        return readOnly;
    }

    // /__astcwz_opern_idt#()
    public void setName(String aname) {
        name = aname;
    }

    // /__astcwz_opern_idt#()
    public void setType(String atype) {
        type = atype;
    }

    // /__astcwz_opern_idt#()
    public void setValue(String avalue) {
        value = avalue;
    }

    // /__astcwz_opern_idt#()
    public void setAlign(String aalign) {
        if (StringUtil.isEmpty(aalign)) {
            aalign = "";
        }
        align = aalign;
    }

    // /__astcwz_opern_idt#()
    public void setAlt(String aalt) {
        alt = aalt;
    }

    // /__astcwz_opern_idt#()
    public void setDisabled(boolean adisabled) {
        disabled = adisabled;

        // set the input class
        if (adisabled) {
            this.tagClass = "inpGrey";
        } else {
            this.tagClass = "inpDefault";
        }
    }

    /**
     * Returns the value of hideLabel.
     */
    public String getHideLabel() {
        return hideLabel;
    }

    /**
     * Sets the value of hideLabel.
     * 
     * @param hideLabel
     *            The value to assign hideLabel.
     */
    public void setHideLabel(String hideLabel) {
        this.hideLabel = hideLabel;
    }

    /**
     * Returns the value of fldNameForMessages.
     */
    public String getFldNameForMessages() {
        if (!StringUtil.isEmpty(fldNameForMessages)) {
            return StringUtil.replace(fldNameForMessages, "\r\n", "");
        } else {

            return StringUtil.replace(this.getInputLabel().getText(), "\r\n",
                    "");
        }
    }

    /**
     * Sets the value of fldNameForMessages.
     * 
     * @param fldNameForMessages
     *            The value to assign fldNameForMessages.
     */
    public void setFldNameForMessages(String fldNameForMessages) {
        this.fldNameForMessages = fldNameForMessages;
    }

    // /__astcwz_opern_idt#()

    // public void setImageSrc(String aimageSrc){
    // imageSrc = aimageSrc;
    // }

    // /__astcwz_opern_idt#()
    public void setReadOnly(boolean areadOnly) {
        readOnly = areadOnly;

        /*
         * commented by sonia abrol, 03/11/05, do not need to change color for
         * readonly fields //set the input class if (areadOnly) {
         * this.tagClass="inpGrey"; }else { this.tagClass="inpDefault"; }
         */
    }

    public VLabel getInputLabel() {
        return this.inputLabel;
    }

    public void setInputLabel(VLabel inputLabel) {
        this.inputLabel = inputLabel;
    }

    public String getNameAttribute() {

        // get name
        if (!EJBUtil.isEmpty(this.name)) {
            return " name = \"" + this.name + "\"";
        } else {
            return "";

        }

    }

    public String getHiddenNameAttribute(String str) {
        String fldName = "hdn_" + str + "_" + this.name;
        // get name
        if (!EJBUtil.isEmpty(this.name)) {
            return " name = \"" + fldName + "\"";

        } else {
            return "";

        }

    }

    public String getHiddenReasonNameAttr(String str) {
        String fldName = "rsn_" + str + "_" + this.name;
        // get name
        if (!EJBUtil.isEmpty(this.name)) {
            return " name = \"" + fldName + "\"";

        } else {
            return "";

        }

    }

    public String getHiddenFldName(String str) {
        String fldName = "hdn_" + str + "_" + this.name;
        // get name
        if (!EJBUtil.isEmpty(this.name)) {
            return fldName;

        } else {
            return "";

        }

    }

    public String getHiddenReasonFldName(String str) {
        String fldName = "rsn_" + str + "_" + this.name;
        // get name
        if (!EJBUtil.isEmpty(this.name)) {
            return fldName;

        } else {
            return "";

        }

    }

    public String getTypeAttribute() {

        // get type
        if (!EJBUtil.isEmpty(this.type)) {
            return " type = \"" + this.type + "\"";
        } else {
            return "";

        }

    }

    public String getHiddenType() {

        // get type
        // if (! EJBUtil.isEmpty(this.type))
        // {
        return " type = \"hidden\"";
        /*
         * } else { return "";
         *  }
         */

    }

    public String getReadOnlyAttribute() {

        // get readOnly
        if (this.readOnly) {
            return " readonly = \"true\"";
        } else {
            return "";

        }

    }

    public String getDisabledAttribute() {

        // get disabled
        if (this.disabled) {
            return " disabled = \"true\"";
        } else {
            return "";

        }

    }

    public String getAlignAttribute() {
        // changed by Sonika

        if (!EJBUtil.isEmpty(this.align)) {
            // this indicates alignment of text which is achieved thru style

            // return " style=\"text-align:"+ this.align + "\"";

            return " align = \"" + this.align + "\"";
        } else {
            return "";

        }

    }

    public String getHiddenValueAttribute(String str) {
        String hdnAtt = "hdn_" + str + "_" + this.name;
        // get value attribute
        if (!EJBUtil.isEmpty(this.name)) {
            // return " value = \"{$" + this.name + "}\"" ;

            return "<xsl:attribute name=\"value\"><xsl:value-of select=\""
                    + hdnAtt + "\"/></xsl:attribute>";

        } else {
            return "";

        }

    }

    /**
     * Returns the XSL for Field Label
     */
    /*
     * Modified by Sonia ABrol , 02/22/05, 1. hide the field label if hideLabel
     * is set to 1 2. if align=top, put a break after label and don't keep it in
     * TD
     */
    public String getInputLabelXSL() {
        VLabel vl = getInputLabel();
        String xslStr = "";

        String hideLabelStr = "";

        hideLabelStr = getHideLabel();

        if (StringUtil.isEmpty(hideLabelStr)) {
            hideLabelStr = "0";
        }

        if (hideLabelStr.equals("0")) {
            if (vl != null) {
                if (!this.align.equals("top")) {
                    xslStr = vl.drawXSL(true);
                } else {
                    xslStr = vl.drawXSL(false);
                    xslStr = xslStr + " <BR/> ";
                }

            }
        } else {
            vl.setText("&#xa0;");
            xslStr = vl.drawXSL(true);
        }

        return xslStr;

    }

    /*
     * Modified by Sonia Abrol, to pass field's label text in the validate
     * functions
     */
    public String getJavaScript(boolean isRequired) {
        String js = "";

        Rlog.debug("common", "In VInputBase getJavaScript type " + type
                + " Required " + isRequired);

        if (isRequired) {

            if (type == null)
                type = "";
            // call a separate function incase of checkbox mandatory validation
            if (type.equals("checkbox") || type.equals("radio")) {
                js = " if (!(validate_chk_radio(\"" + (getFldNameForMessages())
                        + "\",formobj." + this.name + "))) return false ;  ";
            } else {
                Rlog.debug("common",
                        "In VInputBase getJavaScript type dropdown");
                js = " if (!(validate_col(\"" + (getFldNameForMessages())
                        + "\",formobj." + this.name + "))) return false ;  ";
            }
        }

        return js;

    }

    public String getJavaScript(boolean isRequired, String mandatoryOverRide) {
        String js = "";
        String str = "";
        String validateStr = "";

        if (mandatoryOverRide == null)
            mandatoryOverRide = "0";

        if (isRequired && mandatoryOverRide.equals("1")) {

            str = "mnd";
            validateStr = "\"Please enter data in all mandatory fields.[VELSEPLABEL]"
                    + getFldNameForMessages() + "\"";
            /*
             * js.append(" if (!(validate_col_no_alert(\"" +
             * inputLabel.getText() + "\",formobj." + this.name + ")))");
             * js.append("formobj."+getHiddenFldName(str)+".value =
             * "+validateStr+" ;"); js.append("else
             * formobj."+getHiddenFldName(str)+".value = '' ;");
             */
            if (type.equals("checkbox") || type.equals("radio")) {
                js = "if (!(validate_chk_radio_no_alert(\""
                        + (getFldNameForMessages()) + "\",formobj." + this.name
                        + "))) formobj." + getHiddenFldName(str) + ".value = "
                        + validateStr + " ;else formobj."
                        + getHiddenFldName(str) + ".value = '' ; ";
            } else {
                js = "if (!(validate_col_no_alert(\""
                        + (getFldNameForMessages()) + "\",formobj." + this.name
                        + "))) formobj." + getHiddenFldName(str) + ".value = "
                        + validateStr + " ;else formobj."
                        + getHiddenFldName(str) + ".value = '' ; ";
            }

        }

        if (isRequired && mandatoryOverRide.equals("0")) {

            if (type == null)
                type = "";
            // call a separate function incase of checkbox mandatory validation
            if (type.equals("checkbox") || type.equals("radio")) {
                js = " if (!(validate_chk_radio(\"" + (getFldNameForMessages())
                        + "\",formobj." + this.name + "))) return false ;  ";
            } else {
                Rlog.debug("common",
                        "In VInputBase getJavaScript type dropdown");
                js = " if (!(validate_col(\"" + (getFldNameForMessages())
                        + "\",formobj." + this.name + "))) return false ;  ";
            }
        }

        return js;

    }

    public String getFieldActionJavaScript() {
        /*
         * 05/11/04 - by Sonia Sahni Implemented for Field Actions; returns
         * default onChange() script for fields
         */
        String strJS = " onChange=\"performAction(document.er_fillform1,document.er_fillform1."
                + this.name + ")\" ";
        return strJS;

    }

   
    /**
     *     	Addition of datepicker class.
     */
     //JUL-20-2011,BK,Datepicker 
    public String getDatePickerActionClass() {
        String strJS = "class=\"datefield\"";
        return strJS;

    }
    
    //INF-19817 number formatting
    public String getNumFormatClass() {
        String strJS = "class=\"numberfield\"";
        return strJS;

    }
    
    public String getUnCheckString()
    {
    	String str = "";
    	
    	if (type.equals("radio"))
    	{
    		str = " ondblclick= \"this.checked = false; performAction(document.er_fillform1,document.er_fillform1."
                + this.name + "); \"";

    	}		
    	return str ;
    	
    }
}
