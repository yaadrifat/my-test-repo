/**
 * __astcwz_cmpnt_det: Author: Konesa CodeWizard Date and Time Created: Wed Jul
 * 09 14:46:02 IST 2003 Note: DO NOT DELETE OR MODIFY KONESA CODEWIZARD COMMENTS
 * STARTING WITH ///__astcwz_ and /**__astcwz_
 */
// /__astcwz_model_idt#(0)
// /__astcwz_maxim_idt#(1587)
// /__astcwz_packg_idt#(1580,1582,1583,1584~,,,)
package com.velos.eres.tags;

import com.velos.eres.service.util.StringUtil;

// /__astcwz_class_idt#(1419!n)
public class VMultiInput extends VInputBase {

    // /__astcwz_default_constructor
    public VMultiInput() {
        hideResponseLabel = "";
        alignResponseLabel = "";
        fldColCount = 1;

    }

    /**
     * flag to hide response label in form preview, Possible values - 0:No,
     * 1:Yes
     */
    private String hideResponseLabel;

    private String alignResponseLabel;

    private String mandatoryOverRide;

    private int fldColCount;

    public VMultiInput(String name, String type, VLabel mLabel) {
        this.name = name;
        this.type = type;
        this.inputLabel = mLabel;
        this.mandatoryOverRide = "";
        hideResponseLabel = "";
        alignResponseLabel = "";
        fldColCount = 1;

    }

    /**
     * Returns the value of hideResponseLabel.
     */
    public String getHideResponseLabel() {
        return hideResponseLabel;
    }

    /**
     * Sets the value of hideResponseLabel.
     * 
     * @param hideResponseLabel
     *            The value to assign hideResponseLabel.
     */
    public void setHideResponseLabel(String hideResponseLabel) {
        this.hideResponseLabel = hideResponseLabel;
    }

    /**
     * Returns the value of alignResponseLabel.
     */
    public String getAlignResponseLabel() {
        return alignResponseLabel;
    }

    /**
     * Sets the value of alignResponseLabel.
     * 
     * @param alignResponseLabel
     *            The value to assign alignResponseLabel.
     */
    public void setAlignResponseLabel(String alignResponseLabel) {
        this.alignResponseLabel = alignResponseLabel;
    }

    /**
     * Returns the value of fldColCount.
     */
    public int getFldColCount() {
        return fldColCount;
    }

    /**
     * Sets the value of fldColCount.
     * 
     * @param fldColCount
     *            The value to assign fldColCount.
     */
    public void setFldColCount(int fldColCount) {
        this.fldColCount = fldColCount;
    }

    public String getMandatoryOverRide() {
        return this.mandatoryOverRide;
    }

    public void setMandatoryOverRide(String mandatoryOverRide) {
        this.mandatoryOverRide = mandatoryOverRide;
    }

    /**
     * creates XSL for a Multiple Choice Field
     */
    /*
     * Modified by Sonia Abrol, 02/22/2005, hide response label if
     * hideResponseLabelFlag = 1
     */
    public String drawXSL() {

        // declare variable
        StringBuffer xslStr = new StringBuffer();
        String str = "";

        String hideResponseLabelFlag = "";
        String alignResponseLabelStr = "";

        int tdWidth = 100;

        if (fldColCount == 0) {
            fldColCount = 1;
        }

        tdWidth = tdWidth / fldColCount;

        hideResponseLabelFlag = getHideResponseLabel();

        if (StringUtil.isEmpty(hideResponseLabelFlag)) {
            hideResponseLabelFlag = "0";
        }

        alignResponseLabelStr = getAlignResponseLabel();

        if (StringUtil.isEmpty(alignResponseLabelStr)) {
            alignResponseLabelStr = "right";
        }

        xslStr.append("<xsl:variable name = \"colcount\" select = \""
                + this.name + "/@colcount\" />");

        // get input label XSL
        // check whether the field label has to be expanded

        /*
         * String expLabel = getInputLabel().getDisplayStyle().getExpLabel(); if
         * (expLabel.equals("1")) { xslStr.append("<td>"); } else {
         * xslStr.append("<td width='15%' >"); }
         * 
         * xslStr.append(getInputLabelXSL()); xslStr.append("</td> <td>");
         */

        // get input label XSL
        // if field label is hidden, alignment=top is not applicable
        String hideLabelStr = "";
        hideLabelStr = getHideLabel();
        if (StringUtil.isEmpty(hideLabelStr)) {
            hideLabelStr = "0";
        }

        if (hideLabelStr.equals("1") && align.equals("top")) {
            align = "left";
        }

        if (!align.equals("top")) {
            xslStr.append(getInputLabelXSL());
        }
        xslStr.append("<td>");

        // get input label XSL
        if (align.equals("top")) {
            xslStr.append(getInputLabelXSL());
        }

        xslStr.append("<table width=\"100%\" border = \"0\"><tr>");

        xslStr.append("<td><span id=\""+this.name+"_span\"><table width=\"100%\">");

        xslStr.append("<xsl:for-each select = \"" + this.name + "/resp\">");

        // set alignment of response label
        if (alignResponseLabelStr.equals("left")
                && hideResponseLabelFlag.equals("0")) {
            xslStr.append(" <td width=\"" + tdWidth
                    + "%\"> <xsl:value-of select=\"@dispval\"/> </td>");
        }

        xslStr.append("<td width=\"" + tdWidth + "%\">");

        if (alignResponseLabelStr.equals("top")
                && hideResponseLabelFlag.equals("0")) {
            xslStr.append(" <xsl:value-of select=\"@dispval\"/> <BR/>");
        }

        xslStr.append("<input ");
        xslStr.append(getTagClass());
        xslStr.append(getStyle());
        // xslStr.append(getTitle());
        xslStr.append(getToolTip());
        // xslStr.append(getId());
        xslStr.append(getDisabledAttribute());
        xslStr.append(getNameAttribute());
        xslStr.append(getReadOnlyAttribute());
        xslStr.append(getTypeAttribute());
        xslStr.append(this.getUnCheckString());

        // sonia 05/11/04 for field Actions
        xslStr.append(getFieldActionJavaScript());

        xslStr.append(" >");

        xslStr
                .append("<xsl:attribute name=\"value\"><xsl:value-of select=\"@dataval\"/></xsl:attribute>");
        xslStr
                .append("<xsl:attribute name=\"id\"><xsl:value-of select=\"@dispval\"/></xsl:attribute>");

        xslStr.append("<xsl:if test=\"@checked='1'\">");
        xslStr.append("<xsl:attribute name=\"checked\"/>");
        xslStr.append("</xsl:if>");

        // changed by Sonika on Dec 03, 2003
        // align will change the position of text of checkbox or radiobutton

        if ((alignResponseLabelStr.equals("right"))
                || (alignResponseLabelStr.equals("center"))) {
            if (hideResponseLabelFlag.equals("0")) {
                xslStr.append("  <xsl:value-of select=\"@dispval\"/>");
            }

        }

        xslStr.append("</input>");//span for input

        xslStr.append("</td>");
        xslStr
                .append("<xsl:if test = \"number(position() mod $colcount)=0\"><tr></tr></xsl:if>");
        xslStr.append("</xsl:for-each>");
        xslStr.append("</table></span>");
        // madatory override check
        if (mandatoryOverRide == null)
            mandatoryOverRide = "0";
        if (mandatoryOverRide.equals("1")) {

            str = "mnd";
            /*
             * strBuf = appendHiddenFields(xslStr, str); xslBuf = ""; xslBuf =
             * xslBuf.append(strBuf);
             */
            xslStr.append("<input ");

            xslStr.append(getHiddenId(str));

            xslStr.append(getHiddenNameAttribute(str));

            xslStr.append(getHiddenType());
            xslStr.append(" >");

            xslStr.append(getHiddenValueAttribute(str));
            xslStr.append("</input>");

            xslStr.append("<input ");
            xslStr.append(getHiddenReasonId(str));
            xslStr.append(getHiddenReasonNameAttr(str));
            xslStr.append(getHiddenType());
            xslStr.append(" >");
            xslStr.append("</input>");

        }
        xslStr.append("</td>");
        xslStr.append("</tr></table>");
        xslStr.append("</td>");

        return xslStr.toString();

    }

    /*
     * public String getId() { return " id = \"<xsl:value-of
     * select=\"@dispval\"/>\"" ; }
     */

    public String getFieldActionJavaScript() {
        /*
         * 05/19/04 - by Sonia Sahni Implemented for Field Actions; returns
         * default onClick() script for fields overriding the base class method
         * as onClick event needs to be trapped for checkboxes and radio buttons
         */
    	
        String strJS = " onClick=\"performAction(document.er_fillform1,document.er_fillform1."
             + this.name + ")\" ";
         
              
         return strJS;

    }

}
