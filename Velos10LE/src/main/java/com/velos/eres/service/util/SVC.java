package com.velos.eres.service.util;

import java.lang.reflect.Field;

/**
 * SVC stands for "Service Const". Class to hold all the key strings in serviceBundle.properties file.
 * Make all key strings public static.
 * Start each key with a category prefix, and arrange them alphabetically.
*/
public class SVC {
	public static String getValueByKey(String key) {
		String messageText ="" ;
		synchronized(SVC.class) {
			Field field;
			try {
				field = SVC.class.getField(key);
				SVC objectMC = new SVC();

				try {
					Object value = field.get(objectMC);
					messageText = value.toString(); 
				} catch(Exception e) {
					System.out.println(e.getMessage());
				}
			} catch (SecurityException e1) {
				e1.printStackTrace();
			} catch (NoSuchFieldException e1) {
				e1.printStackTrace();
			}
		}
		return messageText;
	}

	public static void reload() {
		synchronized(SVC.class) {
			Field[] fields = SVC.class.getFields();
			for (Field field : fields) {
				try {
					if (field.getName() != null && field.getName().endsWith("_Upper")) {
						field.set(null, VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE,
								field.getName().replaceAll("_Upper", "")).toUpperCase());
					} else if (field.getName() != null && field.getName().endsWith("_Lower")) {  
						field.set(null, VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE,
								field.getName().replaceAll("_Lower", "")).toLowerCase());
					} else {
						field.set(null, VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE,field.getName()));
					}
				} catch(Exception e) {
					System.out.println(e.getMessage());
				}
			}
		}
	}
	

	public static String Svc_PatientEnrollment_StatusCode_Mandatory = VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_StatusCode_Mandatory");
	public static String Svc_PatientEnrollment_StatusReason_Mandatory = VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_StatusReason_Mandatory");
	public static String Svc_PatientEnrollment_StatusDate_Mandatory = VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_StatusDate_Mandatory"); 
	public static String Svc_PatientEnrollment_Notes_Mandatory = VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_Notes_Mandatory"); 
	public static String Svc_PatientEnrollment_RandomNumber_Mandatory = VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_RandomNumber_Mandatory"); 
	public static String Svc_PatientEnrollment_EnrolledBy_Mandatory =  VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_EnrolledBy_Mandatory"); 
	public static String Svc_PatientEnrollment_AssignedTo_Mandatory = VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_AssignedTo_Mandatory");
	public static String Svc_PatientEnrollment_Physician_Mandatory = VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_Physician_Mandatory"); 
	public static String Svc_PatientEnrollment_EnrollingSite_Mandatory= VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_EnrollingSite_Mandatory");
	public static String Svc_PatientEnrollment_TreatmentLocation_Mandatory = VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_TreatmentLocation_Mandatory");
	public static String Svc_PatientEnrollment_TreatingOrg_Mandatory = VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_TreatingOrg_Mandatory");
	public static String Svc_PatientEnrollment_EvaluationFlag_Mandatory = VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_EvaluationFlag_Mandatory");
	public static String Svc_PatientEnrollment_EvaluationStatusCode_Mandatory = VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_EvaluationStatusCode_Mandatory");
	public static String Svc_PatientEnrollment_InevaluationStatusCode_Mandatory = VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_InevaluationStatusCode_Mandatory");
	public static String Svc_PatientEnrollment_SurvivalStatus_Mandatory = VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_SurvivalStatus_Mandatory");
	public static String Svc_PatientEnrollment_DateOfDeath_Mandatory = VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_DateOfDeath_Mandatory"); 
	public static String Svc_PatientEnrollment_CauseOfDeath_Mandatory = VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_CauseOfDeath_Mandatory");
	public static String Svc_PatientEnrollment_SpecifyCause_Mandatory = VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_SpecifyCause_Mandatory"); 
	public static String Svc_PatientEnrollment_DeathRelatedToTrial_Mandatory = VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_DeathRelatedToTrial_Mandatory");
	public static String Svc_PatientEnrollment_DeathDtBirthDt_Camparison = VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_DeathDtBirthDt_Camparison");
	public static String Svc_PatientEnrollment_BirthDtStatusDt_Camparison = VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_BirthDtStatusDt_Camparison");
	public static String Svc_PatientEnrollment_DeathDate_SurvivalStatus = VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_DeathDate_SurvivalStatus");
	public static String Svc_PatientEnrollment_CauseOfDeath_SurvivalStatus = VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_CauseOfDeath_SurvivalStatus");
	public static String Svc_PatientEnrollment_DeathRelatedToTrial_SurvivalStatus = VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_DeathRelatedToTrial_SurvivalStatus");
	public static String Svc_PatientEnrollment_ScreenNumber_Mandatory = VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_ScreenNumber_Mandatory"); 
	public static String Svc_PatientEnrollment_ScreenBy_Mandatory = VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_ScreenBy_Mandatory"); 
	public static String Svc_PatientEnrollment_ScreeningOutcome_Mandatory = VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_ScreeningOutcome_Mandatory");
	public static String Svc_PatientEnrollment_NextFollowUpDate_Mandatory = VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_NextFollowUpDate_Mandatory");
	public static String Svc_PatientEnrollment_ICVersionNumber_Mandatory = VelosResourceBundle.getString(VelosResourceBundle.SERVICE_BUNDLE, "Svc_PatientEnrollment_ICVersionNumber_Mandatory"); 

}