/*
 * Classname			GroupAgentBean.class
 *
 * Version information	1.0
 *
 * Date					02/25/2001
 *
 * Copyright notice		Velos Inc.
 */

package com.velos.eres.service.groupAgent.impl;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.common.GroupDao;
import com.velos.eres.business.group.impl.GroupBean;
import com.velos.eres.service.groupAgent.GroupAgentRObj;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The stateless session EJB acting as a facade for the entity BMP.<br>
 * <br>
 *
 * @author sajal
 * @version 1.0 02/25/2001
 * @ejbHome GroupAgentHome
 * @ejbRemote GroupAgentRObj
 */

@Stateless
public class GroupAgentBean implements GroupAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * Looks up on the Group id parameter passed in and constructs and returns a
     * Group state keeper. containing all the summary details of the group<br>
     *
     * @param groupId
     *            the Group id
     */
    public GroupBean getGroupDetails(int groupId) {
        GroupBean groupBean = null;
        try {
            groupBean = (GroupBean) em.find(GroupBean.class, new Integer(
                    groupId));
        } catch (Exception e) {
            Rlog.fatal("group", "Error in getGroupDetails() in GroupAgentBean"
                    + e);
        }
        return groupBean;
    }

    /**
     * Calls setGroupDetails() of Group Entity Bean: GroupBean
     *
     * @param an
     *            group state keeper containing group attributes to be set.
     * @return 0 when updation is successful and -2 when exception occurs
     * @see GroupBean
     */

    public int setGroupDetails(GroupBean group) {
        try {
            GroupBean grp = new GroupBean();
            grp.updateGroup(group);
            em.persist(grp);

            return grp.getGroupId();
        } catch (Exception e) {
            Rlog.fatal("group", "Error in setGroupDetails() in GroupAgentBean "
                    + e);
            return -1;
        }

    }

    /**
     * Calls updateGroup() of Group Entity Bean: GroupBean For Updating the
     * values of the group <br>
     *
     * @param an
     *            group state keeper containing group attributes to be set.
     * @return 0 when updation is successful and -2 when exception occurs
     * @see GroupBean
     */
    public int updateGroup(GroupBean gsk) {

        String oldSuperUserFlag = "";
        String newSuperUserFlag = "";
        GroupDao gdao = new GroupDao();
        // int success = 0;
        GroupBean groupBean = null;

        int output = 0;

        try {

            groupBean = (GroupBean) em.find(GroupBean.class, gsk.getGroupId());
            if (groupBean == null) {
                return -2;
            }

            // for super user access rights, get old access status

            oldSuperUserFlag = groupBean.getGroupSuperUserFlag();
            newSuperUserFlag = gsk.getGroupSuperUserFlag();

            if (StringUtil.isEmpty(oldSuperUserFlag))
                oldSuperUserFlag = "0";

            if (StringUtil.isEmpty(newSuperUserFlag))
                newSuperUserFlag = "0";

            output = groupBean.updateGroup(gsk);


            //commented by sonia,10/22/07, changed super user implementation
            /*if (!oldSuperUserFlag.equals(newSuperUserFlag)) {
                output = -1;
                if (newSuperUserFlag.equals("1")) {
                    output = gdao.grantSupUser(gsk.getGroupId(), gsk
                            .getGroupAccId(), gsk.getCreator(), gsk.getIpAdd(),
                            gsk.getGroupSuperUserRights());
                } else if (newSuperUserFlag.equals("0")) {
                    output = gdao.revokeSupUser(gsk.getGroupId(), gsk
                            .getGroupAccId());
                }

            } */

            // return success;

        } catch (Exception e) {
            Rlog.debug("group", "Exception in updateGroup in GroupAgentBean"
                    + e);
            return -2;
        }
        return output;
    }

    /**
     * Returns the GroupDao object containing the list of groups for an Account.
     *
     * @param accountId
     *            Account Id for which list of groups is required
     * @return Data Access Object for Group which holds the list of Account
     *         Groups
     * @see GroupDao
     */

    public GroupDao getByAccountId(int accountId) {
        try {
            Rlog.debug("group", "In getByAccountId in GroupAgentBean - 0");
            GroupDao groupDao = new GroupDao();
            Rlog.debug("group", "In getByAccountId in GroupAgentBean - 1");
            groupDao.getGroupValues(accountId);
            Rlog.debug("group", "In getByAccountId in GroupAgentBean - 2");
            return groupDao;
        } catch (Exception e) {
            Rlog.fatal("group",
                    "Exception In findByAccountId in GroupAgentBean " + e);
        }
        return null;
    }

    /**
     * Calls getCount() on GroupDao;
     *
     * @param groupName
     *            Group Name
     * @param accId
     *            Account Id
     * @return int with count of number of groups with the same group name as
     *         groupName
     * @see GroupDao
     */

    public int getCount(String groupName, String accId) {
        int count = 0;
        try {
            Rlog.debug("group", "In getCount line number 0");
            GroupDao groupDao = new GroupDao();
            Rlog.debug("group", "In getCount line number 1");
            count = groupDao.getCount(groupName, accId);
            Rlog.debug("group", "In getCount line number 2");
        } catch (Exception e) {
            Rlog.fatal("group", "Exception In getCount in GroupAgentBean " + e);
        }
        return count;
    }

    /**
     * Calls getGroupDaoInstance() on GroupDao; *
     *
     * @return GroupDao Object
     * @see GroupDao
     */

    public GroupDao getGroupDaoInstance() {
        return new GroupDao();

    }

    /**
     * Calls getDefaultStudySuperUserRights() on GroupDao;
     *
     * @param user
     *            User PK

     * @see GroupDao
     */

    public String getDefaultStudySuperUserRights(String p_user) {
        String rights ;
        try {

             rights = GroupDao.getDefaultStudySuperUserRights(p_user);

        } catch (Exception e) {
            Rlog.fatal("group", "Exception In getDefaultStudySuperUserRights(String p_user) in GroupAgentBean " + e);
            rights = "";
        }
        return rights;
    }


  /**
   * This method calls copyRoleDefRightsToGrpSupUserRights() on GroupDao()
   * @param roleId
   * @param grpId
   * @return 0 if successfully updated
   */
    public int copyRoleDefRightsToGrpSupUserRights(int roleId, int grpId){

    	  int success = 0;

        try {
        	GroupDao groupDao = new GroupDao();
        	success = groupDao.copyRoleDefRightsToGrpSupUserRights(roleId, grpId);

        } catch (Exception e) {
            Rlog.fatal("group",
                    "Error in copyRoleDefRightsToGrpSupUserRights() in GroupAgentBean " + e);
        }

        return success;


    }


}// end of class

