/*
 * Classname : FormFieldAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 17/07/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: SoniaKaura
 */

package com.velos.eres.service.formFieldAgent.impl;

/* Import Statements */

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.common.FormFieldDao;
import com.velos.eres.business.formField.impl.FormFieldBean;
import com.velos.eres.business.formNotify.impl.FormNotifyBean;
import com.velos.eres.service.formFieldAgent.FormFieldAgentRObj;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.web.formField.FormFieldJB;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import java.util.Hashtable;
import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.business.audit.impl.AuditBean;

/* End of Import Statements */

/**
 * The stateless session EJB acting as a facade for the entity CMP -
 * FormFieldAgentBean.<br>
 * <br>
 * 
 * @author SoniaKaura
 * @see FormNotifyBean
 * @version 1.0 17/07/2003
 * @ejbHome FormFieldAgentHome
 * @ejbRemote FormFieldAgentRObj
 */
@Stateless
public class FormFieldAgentBean implements FormFieldAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    @Resource private SessionContext context;

    /**
     * 
     * Looks up on the FormField id parameter passed in and constructs and
     * returns a FormField state keeper. containing all the details of the Form
     * Notification<br>
     * 
     * @param formFieldId
     *            the Form Field id
     */

    public FormFieldBean getFormFieldDetails(int formFieldId) {
        FormFieldBean retrieved = null; // Form Notify Entity Bean Remote Object

        try {
            retrieved = (FormFieldBean) em.find(FormFieldBean.class,
                    new Integer(formFieldId));

        } catch (Exception e) {
            Rlog.fatal("formfield",
                    "Error in getFormFieldDetails() in FormFieldAgentBean" + e);
        }

        return retrieved;

    }

    /**
     * Creates FormField record.
     * 
     * @param a
     *            State Keeper containing the FormField attributes to be set.
     * @return int for successful:0; e\Exception : -2
     */

    public int setFormFieldDetails(FormFieldBean formFldsk) {
        try {
            FormFieldBean fb = new FormFieldBean();

            fb.updateFormField(formFldsk);
            em.persist(fb);
            return fb.getFormFieldId();
        } catch (Exception e) {
            Rlog
                    .fatal("formfield",
                            "Error in setFormFieldDetails() in FormFieldAgentBean "
                                    + e);
            return -2;
        }

    }

    /**
     * Updates a FormField record.
     * 
     * @param a
     *            FormField state keeper containing FormField attributes to be
     *            set.
     * @return int for successful:0; e\Exception : -2
     */
    public int updateFormField(FormFieldBean formFldsk) {

        FormFieldBean retrieved = null; // Entity Bean Remote Object
        int output;

        try {
            retrieved = (FormFieldBean) em.find(FormFieldBean.class,
                    new Integer(formFldsk.getFormFieldId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateFormField(formFldsk);
            em.merge(retrieved);
        } catch (Exception e) {
            Rlog.fatal("formfield",
                    "Error in updateFormField in FormFieldAgentBean" + e);
            return -2;
        }
        return output;
    }
    
    //Overloaded for INF-18183 ::: Ankit
    public int updateFormField(FormFieldBean formFldsk, Hashtable<String, String> auditInfo) {

        FormFieldBean retrieved = null; // Entity Bean Remote Object
        int output;

        try {
            retrieved = (FormFieldBean) em.find(FormFieldBean.class,
                    new Integer(formFldsk.getFormFieldId()));

            if (retrieved == null) {
                return -2;
            }
            
            if(formFldsk!=null && "D".equals(formFldsk.getRecordType()))
            {
	            AuditBean audit=null;
	            String currdate =DateUtil.getCurrentDate();
	            String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
	            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
	            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
	            String moduleName = (String)auditInfo.get(AuditUtils.APP_MODULE);
	            String getRidValue= AuditUtils.getRidValue("ER_FORMFLD","eres","PK_FORMFLD="+formFldsk.getFormFieldId());/*Fetches the RID/PK_VALUE*/ 
	            audit = new AuditBean("ER_FORMFLD",String.valueOf(formFldsk.getFormFieldId()),getRidValue,
	        			userID,currdate,moduleName,ipAdd,reason);  /*POPULATE THE AUDIT BEAN*/ 
	        	em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
            }
            output = retrieved.updateFormField(formFldsk);
            em.merge(retrieved);
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("formfield",
                    "Error in updateFormField in FormFieldAgentBean" + e);
            return -2;
        }
        return output;
    }
    /**
     * Removes a FormField record.
     * 
     * @param a
     *            FormField Primary Key
     * @return int for successful:0; e\Exception : -2
     */

    public int removeFormField(int formFieldId) {
        int output;
        FormFieldBean retrieved = null;

        try {
            retrieved = (FormFieldBean) em.find(FormFieldBean.class,
                    new Integer(formFieldId));
            em.remove(retrieved);
            Rlog.debug("formfield", "Removed FormField row with FormFieldID = "
                    + formFieldId);
            return 0;
        } catch (Exception e) {
            Rlog.fatal("formfield", "Exception in removeFormField " + e);
            return -1;

        }

    }

    // /////////
    /**
     * method added by Chennai dev team on 03/12/2004
     * 
     */

    public int removeFormField(int formId, String[] formFieldId1,
            String lastModifiedBy) {
        int ret = 0;
        int formFieldId = 0;

        try {
            Rlog
                    .debug(
                            "formfield",
                            "in try bloc of FormField agent:removeFormField(int formId,String[] formFieldId1,String lastModifiedBy)");

            for (int i = 0; i < formFieldId1.length; i++) {
                formFieldId = Integer.parseInt(formFieldId1[i]);

                FormFieldJB sb = new FormFieldJB();
                sb.setFormFieldId(formFieldId);
                sb.getFormFieldDetails();
                sb.setModifiedBy(lastModifiedBy);
                sb.setRecordType("D");
                ret = sb.updateFormField();

                Rlog.debug("formfield",
                        "Removed removeFormFieldrow with FormFieldID = "
                                + formFieldId);
            }
            return ret;
        } catch (Exception e) {
            Rlog.fatal("formfield", "Exception in removeFormField" + e);
            return -1;

        }

    }

    //Overloaded for INF-18183 ::: Ankit
    public int removeFormField(int formId, String[] formFieldId1,
            String lastModifiedBy, Hashtable<String, String> auditInfo) {
        int ret = 0;
        int formFieldId = 0;

        try {
            Rlog
                    .debug(
                            "formfield",
                            "in try bloc of FormField agent:removeFormField(int formId,String[] formFieldId1,String lastModifiedBy)");

            for (int i = 0; i < formFieldId1.length; i++) {
                formFieldId = Integer.parseInt(formFieldId1[i]);

                FormFieldJB sb = new FormFieldJB();
                sb.setFormFieldId(formFieldId);
                sb.getFormFieldDetails();
                sb.setModifiedBy(lastModifiedBy);
                sb.setRecordType("D");
                ret = sb.updateFormField(auditInfo);

                Rlog.debug("formfield",
                        "Removed removeFormFieldrow with FormFieldID = "
                                + formFieldId);
            }
            return ret;
        } catch (Exception e) {
            Rlog.fatal("formfield", "Exception in removeFormField" + e);
            return -1;

        }

    }
    public int repeatLookupField(int pkField, int formField, int section) {
        int ret = 0;

        try {
            FormFieldDao fd = new FormFieldDao();

            ret = fd.repeatLookupField(pkField, formField, section);

            return ret;
        } catch (Exception e) {
            Rlog.fatal("formfield", "Exception in repeatLookupField " + e);
            return -1;

        }

    }

    public FormFieldDao findFieldByUID(int formId, String fieldUID) {
        try {
            FormFieldDao fd = new FormFieldDao();
            fd.findFieldByUID(formId, fieldUID);

            return fd;
        } catch (Exception e) {
            Rlog.fatal("formfield", "Exception in repeatLookupField " + e);
            return null;

        }

    }
}
