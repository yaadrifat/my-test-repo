/*
 * Classname			UpdateSchedulesAgent.class
 * 
 * Version information   
 *
 * Date					03/05/2008
 * 
 * Author	: Manimaran
 * 
 */

package com.velos.eres.service.updateSchedulesAgent;


import javax.ejb.Remote;

import com.velos.eres.service.updateSchedulesAgent.impl.UpdateSchedulesAgentBean;

@Remote
public interface UpdateSchedulesAgentRObj {
   
    public int discShedulesAllPatient(int discSchCal, String discDate, String discReason, int newSchCal, String newSchStartDate, int usr, String ipAdd);
    
    public int updateEventStatusCurrAndDisc(int eveStatDiscCal, String scheduleFlag, String dateOccurDisc, String allEveDate, int dstatusDisc, String allEveStatDate, int usr, String ipAdd );
    
    public int generateNewSchedules(int newSchAllCal, String patSchStartDate, int patEnrollDtCheck, int usr, String ipAdd );
}
