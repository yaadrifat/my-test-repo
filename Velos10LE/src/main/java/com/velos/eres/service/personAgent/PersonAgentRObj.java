/*
 * Classname : PersonAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 06/03/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sonia Sahni
 */

package com.velos.eres.service.personAgent;

/* Import Statements */
import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.eres.business.common.PatientDao;
import com.velos.eres.business.person.impl.PersonBean;

/* End of Import Statements */

/**
 * Remote interface for PersonAgent session EJB
 * 
 * @author Sonia Sahni
 * @version : 1.0 06/03/2001
 */
@Remote
public interface PersonAgentRObj {
    /**
     * gets the Person record
     */
    PersonBean getPersonDetails(int personPKId);

    /**
     * creates a new PREF
     */
    public PersonBean setPersonDetails(PersonBean psk);

    /**
     * updates PREF
     */
    public int updatePerson(PersonBean psk);

    /**
     * remove Person
     */
    public int removePerson(int personPKId);

    public PatientDao getPatients(int usrOrganization, String patientID,
            int patStatus);

    public PatientDao searchPatients(String siteId, String studyId,
            String patientCode);

    public PatientDao searchPatientsForEnr(String siteId, String studyId,
            String patientCode, String age, String genderVal, String regBy,
            String pstat, String pName, String pStudyExists);

    public int getPatientCompleteDetailsAccessRight(int userId, int groupId,
            int patientId);
 // Overloaded for INF-18183 ::: Akshi
    public void deletePatient(int patientId,Hashtable<String, String> args);

    public void deletePatient(int patientId);
	/**
	 * 
	 */
	public void flush();

}
