/*
 * Classname			CatLibAgentRObj
 * 
 * Version information : 1.0  
 *
 * Date					06/25/2003
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.catLibAgent;

import javax.ejb.Remote;

import com.velos.eres.business.catLib.impl.CatLibBean;
import com.velos.eres.business.common.CatLibDao;

/**
 * Remote interface for CatLibAgentBean session EJB
 * 
 * @author Anu
 * @version 1.0, 06/25/2003
 */
@Remote
public interface CatLibAgentRObj {
    public CatLibBean getCatLibDetails(int catLibId);

    public int setCatLibDetails(CatLibBean catlib);

    public int updateCatLib(CatLibBean clsk);

    /**
     * get the DAO from the table with all the categories for a particular
     * account id
     * 
     */

    public CatLibDao getAllCategories(int accountId, String type);

    /**
     * get the DAO from the table with the ALL option
     * 
     */

    public CatLibDao getCategoriesWithAllOption(int accountId, String type,
            int forLastAll);

}
