/*
 * Classname			SavedRepAgentBean.class 
 * 
 * Version information
 *
 * Date				05/31/2002
 * 
 * Copyright notice
 */

package com.velos.eres.service.savedRepAgent.impl;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.common.SavedRepDao;
import com.velos.eres.business.savedRep.impl.SavedRepBean;
import com.velos.eres.service.savedRepAgent.SavedRepAgentRObj;
import com.velos.eres.service.util.Rlog;

/**
 * The stateless session EJB acting as a facade for the entity BMP.<br>
 * <br>
 * 
 * @author Arvind
 */
@Stateless
public class SavedRepAgentBean implements SavedRepAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * Looks up on the SavedRep id parameter passed in and constructs and
     * returns a SavedRep state keeper. containing all the details of the
     * savedRep<br>
     * 
     * @param savedRepId
     *            the SavedRep id
     */
    public SavedRepBean getSavedRepDetails(int savedRepId) {

        try {
            return (SavedRepBean) em.find(SavedRepBean.class, new Integer(
                    savedRepId));
        } catch (Exception e) {
            Rlog.fatal("savedRep",
                    "Error in getSavrdRepDetails() in SavedRepAgentBean" + e);
            return null;
        }

    }

    /**
     * Sets an SavedRep.
     * 
     * @param a
     *            savedRep state keeper containing savedRep attributes to be
     *            set.
     * @return void
     */

    public int setSavedRepDetails(SavedRepBean srsk) {
        try {
            SavedRepBean sBean = new SavedRepBean();
            sBean.updateSavedRep(srsk);
            em.persist(sBean);
            return sBean.getSavedRepId();
        } catch (Exception e) {
            Rlog.fatal("savedRep",
                    "Error in setSavedRepDetails() in SavedRepAgentBean " + e);
        }
        return 0;
    }

    public int updateSavedRep(SavedRepBean srsk) {

        SavedRepBean retrieved = null; // User Entity Bean Remote Object
        int output;

        try {

            retrieved = (SavedRepBean) em.find(SavedRepBean.class, srsk
                    .getSavedRepId());

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateSavedRep(srsk);
            em.merge(retrieved);

        } catch (Exception e) {
            Rlog.debug("savedRep",
                    "Error in updateSavedRep in SavedRepAgentBean" + e);
            return -2;
        }
        return output;
    }

    /**
     * Gets all saved reports
     * 
     * @param studyId.
     * @return SavedRepDao
     */

    public SavedRepDao getAllReps(int studyId) {
        try {
            Rlog.debug("savedRep", "In getAllReps in SavedRepAgentBean - 0");
            SavedRepDao savedRepDao = new SavedRepDao();
            savedRepDao.getAllReps(studyId);
            return savedRepDao;
        } catch (Exception e) {
            Rlog.fatal("savedRep",
                    "Exception In getAllReps in savedRepAgentBean " + e);
        }
        return null;
    }

    /**
     * Gets report content
     * 
     * @param saved
     *            report id
     * @return SavedRepDao
     */

    public SavedRepDao getReportContent(int savedRepId) {
        try {
            Rlog.debug("savedRep",
                    "In getReportContent in SavedRepAgentBean - 0");
            SavedRepDao savedRepDao = new SavedRepDao();
            savedRepDao.getReportContent(savedRepId);
            return savedRepDao;
        } catch (Exception e) {
            Rlog.fatal("savedRep",
                    "Exception In getReportContent in savedRepAgentBean " + e);
        }
        return null;
    }

    public int removeSavedRep(int savedRepId) {
        SavedRepBean retrieved = null;
        try {
            retrieved = (SavedRepBean) em.find(SavedRepBean.class, new Integer(
                    savedRepId));
            em.remove(retrieved);
            return 0;
        } catch (Exception e) {
            Rlog.fatal("savedRep", "Exception in removeSavedRepDetails()" + e);
            return -1;
        }
    }

    /**
     * Validation of unique report name in study
     * 
     * @param studyId -
     *            Study Id
     * @param savedRepName -
     *            user entered name of saved report
     */

    public SavedRepDao validateReportName(int studyId, String savedRepName) {
        try {
            Rlog.debug("savedRep",
                    "In validateReportName in SavedRepAgentBean - 0");
            SavedRepDao savedRepDao = new SavedRepDao();
            savedRepDao.validateReportName(studyId, savedRepName);
            return savedRepDao;
        } catch (Exception e) {
            Rlog
                    .fatal("savedRep",
                            "Exception In validateReportName in savedRepAgentBean "
                                    + e);
        }
        return null;
    }

}// end of class

