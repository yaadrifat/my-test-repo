/*
 * Classname			PatProtAgentBean
 * 
 * Version information : 1.0
 *
 * Date					06/16/2001
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.patProtAgent.impl;

/* IMPORT STATEMENTS */
import java.sql.Array;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.velos.eres.business.common.PatFacilityDao;

import javax.ejb.SessionContext;
import com.velos.eres.business.patProt.impl.PatProtBean;
import com.velos.eres.service.patProtAgent.PatProtAgentRObj;
import com.velos.eres.service.util.Rlog;
import com.velos.esch.business.common.ScheduleDao;
import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.StringUtil;

import com.velos.esch.service.util.EJBUtil;
import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.business.audit.impl.AuditBean;
import javax.annotation.Resource;
/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP.
 * 
 * @author sajal
 * @version 1.0, 06/16/2001
 * @ejbHome PatProtAgentHome
 * @ejbRemote PatProtAgentRObj
 */

@Stateless
public class PatProtAgentBean implements PatProtAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    @Resource private SessionContext context;

    /**
     * Looks up on the PatProt id parameter passed in and constructs and returns
     * a PatProtStateKeeper containing all the summary details of the patProt
     * 
     * @param patProtId
     *            The PatProt Id
     * @see PatProtStateKeeper
     */
    public PatProtBean getPatProtDetails(int patProtId) {
        PatProtBean retrieved = null; // PatProt Entity Bean Remote Object

        try {

            retrieved = (PatProtBean) em.find(PatProtBean.class, new Integer(
                    patProtId));
            em.clear();

        } catch (Exception e) {
            Rlog.fatal("patProt",
                    "Error in getPatProtDetails() in PatProtAgentBean" + e);
        }

        return retrieved;
    }

    /**
     * Add the PatProt details in the PatProtStateKeeper Object to the database
     * 
     * @param patProt
     *            An PatProtStateKeeper containing patProt attributes to be set.
     * @return The PatProt Id for the patProt just created <br>
     *         0 - if the method fails -3 - if enrollment record already exist
     */

    public int setPatProtDetails(PatProtBean patProt) {
        int found = 0;
        PatProtBean patProtBean=new PatProtBean();
        String dateFormat = Configuration.getAppDateFormat();
        if (dateFormat.indexOf("MMM") != -1)
        	dateFormat = StringUtil.replace(dateFormat,"MMM","Mon");
        try {

            try {

                Query query = em.createNamedQuery("findByEnrollDate");
                query.setParameter("fk_study", StringUtil.stringToNum(patProt
                        .getPatProtStudyId()));
                query.setParameter("fk_per", StringUtil.stringToNum(patProt
                        .getPatProtPersonId()));
                query.setParameter("enroldt", patProt.returnPatProtEnrolDt());
                query.setParameter("date_format",dateFormat);

                ArrayList list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    // patient enrollment record exists
                    Rlog
                            .debug("patProt",
                                    "PatProtAgentBean.setPatProtDetails :patient is already enrolled");
                    found = 1;
                    return -3;
                }
            } catch (Exception ex) {
                Rlog.fatal("patProt", "Exception thrown:" + ex);
                found = 0; // New record
            }
            patProtBean.updatePatProt(patProt);
            em.persist(patProtBean);
            return (patProtBean.getPatProtId());

        } catch (Exception e) {
            Rlog.fatal("patProt",
                    "Error in setPatProtDetails() in PatProtAgentBean " + e);
        }
        return 0;
    }

    /**
     * Update the PatProt details contained in the PatProtStateKeeper Object to
     * the database
     * 
     * @param ppsk
     *            An PatProtStateKeeper containing patProt attributes to be
     *            updated.
     * @return 0 - if the update is successful<br>
     *         -2 - if the update fails
     */
    public int updatePatProt(PatProtBean ppsk) {

        PatProtBean retrieved = null;
        int output;

        try {
            retrieved = (PatProtBean) em.find(PatProtBean.class, new Integer(
                    ppsk.getPatProtId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updatePatProt(ppsk);
            em.merge(retrieved);
        } catch (Exception e) {
            Rlog.debug("patProt", "Error in updatePatProt in PatProtAgentBean"
                    + e);
            return -2;
        }
        return output;
    }
    
    /**
     * Find oldest patient study enrollment and constructs and returns a
     * PatProtStateKeeper containing all the summary details of the patProt
     * 
     * @param patProtId
     *            The PatProt Id
     * @see com.velos.eres.business.patProt.PatProtStateKeeper
     */
    public PatProtBean findOldestPatProtDetails(int studyId, int patientId) {
        PatProtBean retrieved = new PatProtBean();

        try {

            try {
                Query query = em.createNamedQuery("findAllEnroll");
                query.setParameter("fk_study", studyId);
                query.setParameter("fk_per", patientId);

                ArrayList list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    // patient enrollment record exists
                    retrieved = (PatProtBean) list.get(0);
                }

            } catch (Exception ex) {
                // the finder failed, no remote obj found
                retrieved = null;
            }

        } catch (Exception e) {
            Rlog.fatal("patProt",
                    "Error in findOldestPatProtDetails() in PatProtAgentBean"
                            + e);
        }
        return retrieved;
    }

    /**
     * Find current patient study enrollment and constructs and returns a
     * PatProtStateKeeper containing all the summary details of the patProt
     * 
     * @param patProtId
     *            The PatProt Id
     * @see com.velos.eres.business.patProt.PatProtStateKeeper
     */
    public PatProtBean findCurrentPatProtDetails(int studyId, int patientId) {
        PatProtBean retrieved = new PatProtBean();

        try {

            try {
                Query query = em.createNamedQuery("findByEnroll");
                query.setParameter("fk_study", studyId);
                query.setParameter("fk_per", patientId);

                ArrayList list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    // patient enrollment record exists
                    retrieved = (PatProtBean) list.get(0);
                }

            } catch (Exception ex) {
                // the finder failed, no remote obj found
                retrieved = null;
            }

        } catch (Exception e) {
            Rlog.fatal("patProt",
                    "Error in findCurrentPatProtDetails() in PatProtAgentBean"
                            + e);
        }
        return retrieved;
    }

    public ScheduleDao getAvailableSchedules(String patientId, String studyId)
    {
    	   	    	  
    	ScheduleDao sd = new ScheduleDao();
    	try{
	    	sd.getAvailableSchedules(patientId, studyId);
	    	return sd;
	      	}catch (Exception ex)
	      	{
	      		Rlog.fatal("patProt",
	                    "Exception in PatProtAgentBean : getAvailableSchedules(String patientId, String studyId)" + ex);
	      		return sd;
    	}
     	
    	
    }
  //KV:BugNo:5111
    public int deleteSchedules(String[] deleteIds, int userId) {
        ScheduleDao scheduleDao = new ScheduleDao();
        int ret = 0;       
        try {        	
            Rlog.debug("schedule", "PatProtAgentBean- 0");
            ret = scheduleDao.deleteSchedules(deleteIds,userId);
            
        } catch (Exception e) {
            Rlog.fatal("schedule", "PatProtAgentBean-Exception In deleteschedules "
                    + e);
            return -1;
        }

        return ret;

    }

    // deleteSchedules() Overloaded for INF-18183 ::: Raviesh
    public int deleteSchedules(String[] deleteIds, int userId,Hashtable<String, String> args) {
        ScheduleDao scheduleDao = new ScheduleDao();
        int ret = 0;
        String condition="";
        try {
        	AuditBean audit=null;
        	String pkVal = "";
        	String ridVal = "";
           
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String appModule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the App Module Name from the Hashtable*/
            
            for(int i=0; i<deleteIds.length; i++)
        		condition = condition + "," + deleteIds[i];  
        	// Fixed Bug #7382 AGodara
        	condition="PK_PATPROT IN ("+condition.substring(1)+") ";

        	Hashtable<String, ArrayList<String>> getRowValues = AuditUtils.getRowValues("ER_PATPROT",condition,"eres");/*Fetches the RID/PK_VALUE*/
        	ArrayList<String> rowPK  = (ArrayList<String>)getRowValues.get(AuditUtils.ROW_PK_KEY);
    		ArrayList<String> rowRID  = (ArrayList<String>)getRowValues.get(AuditUtils.ROW_RID_KEY);
    		                      
    		for(int i=0; i<rowPK.size(); i++)
			{
    			pkVal = rowPK.get(i);
    			ridVal = rowRID.get(i);
    			audit = new AuditBean("ER_PATPROT",pkVal,ridVal,
        			userID,currdate,appModule,ipAdd,reason);
    			em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
			}
                	
            Rlog.debug("schedule", "PatProtAgentBean- 0");
            ret = scheduleDao.deleteSchedules(deleteIds,userId);
            if(ret!=0){
            	context.setRollbackOnly();
        	} /*Checks whether the actual delete has occurred with return 0 else, rollback */

        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("schedule", "PatProtAgentBean-Exception In deleteschedules "
                    + e);
            return -1;
        }

        return ret;

    }
}// end of class

