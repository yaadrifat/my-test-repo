/*
 * Classname			MilestoneAgentBean
 *
 * Version information 	1.0
 *
 * Date					05/28/2002
 *
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.service.milestoneAgent.impl;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.json.JSONArray;
import org.json.JSONObject;

import com.velos.eres.business.common.MileAchievedDao;
import com.velos.eres.business.common.MilestoneDao;
import com.velos.eres.business.milestone.impl.MilestoneBean;
import com.velos.eres.service.milestoneAgent.MilestoneAgentRObj;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.business.audit.impl.AuditBean;

/**
 * The stateless session EJB acting as a facade for the entity CMP.<br>
 * <br>
 *
 * @author Sonika
 * @version 1.0 05/28/2002
 */
@Stateless
public class MilestoneAgentBean implements MilestoneAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    @Resource private SessionContext context;

    /**
     * getMilestoneDetails Calls getMilestoneDetails() on entity bean -
     * MilestoneBean Looks up on the Milestone id parameter passed in and
     * constructs and returns a Milestone state holder.
     *
     * @param milestoneId
     *            the Milestone id
     * @see MilestoneBean
     */
    public MilestoneBean getMilestoneDetails(int milestoneId) {

        try {
        	// Date:-08-Feb-2012 Bug#8169 Ankit
        	MilestoneBean mileBeanObj = null;
        	mileBeanObj  = (MilestoneBean) em.find(MilestoneBean.class, new Integer(
                    milestoneId));
        	context.setRollbackOnly(); 
            return mileBeanObj;
        } catch (Exception e) {
            Rlog.debug("milestone",
                    "Error in getMilestoneDetails() in MilestoneAgentBean" + e);
            return null;
        }

    }

    /**
     * Creates a new a Milestone. Calls setMilestoneDetails() on entity bean
     * MilestoneBean
     *
     * @param an
     *            Milestone state holder containing Milestone attributes to be
     *            set.
     * @return generated milestone id
     * @see MilestoneBean
     */

    public int setMilestoneDetails(MilestoneBean msk) {
        MilestoneBean mBean = new MilestoneBean();
        try {
            mBean.updateMilestone(msk);
            em.persist(mBean);
            return mBean.getId();
        } catch (Exception e) {
            System.out
                    .print("Error in setMilestoneDetails() in MilestoneAgentBean "
                            + e);
            return -1;
        }

    }

    /**
     * Updates Milestone Details. Calls updateMilestone() on entity bean
     * MilestoneBean
     *
     * @param an
     *            Milestone state holder containing Milestone attributes to be
     *            set.
     * @return int 0 for successful ; -2 for exception
     * @see MilestoneBean
     */

    public int updateMilestone(MilestoneBean msk) {

        MilestoneBean milestoneRObj = null; // Milestone Entity Bean Remote
        // Object
        int output = 0;

        try {

            milestoneRObj = (MilestoneBean) em.find(MilestoneBean.class,
                    new Integer(msk.getId()));
            if (milestoneRObj == null) {
                return -2;
            }
            output = milestoneRObj.updateMilestone(msk);
            Rlog.debug("milestone",
                    "MilestoneAgentBean.updateMilestone line 7 and output is "
                            + output);
        } catch (Exception e) {
            Rlog.fatal("milestone",
                    "Error in updateMilestone in MilestoneAgentBean" + e);
            return -2;
        }
        return output;
    }

    
    // updateMilestone() Overloaded for INF-18183 ::: Raviesh
    public int updateMilestone(MilestoneBean msk,Hashtable<String, String> args) {

        MilestoneBean milestoneRObj = null; // Milestone Entity Bean Remote
        // Object
        int output = 0;

        try {

        	 AuditBean audit=null; //Audit Bean for AppDelete
             String currdate =DateUtil.getCurrentDate(); /*gets the current date from DateUtil*/
             String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
             String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
             String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
             String appModule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the App Module Name from the Hashtable*/
             
             String getRidValue= AuditUtils.getRidValue("ER_MILESTONE","eres","PK_MILESTONE="+msk.getId());/*Fetches the RID/PK_VALUE*/ 
         	audit = new AuditBean("ER_MILESTONE",String.valueOf(msk.getId()),getRidValue,
         			userID,currdate,appModule,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
         	em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
         	 
         	
            milestoneRObj = (MilestoneBean) em.find(MilestoneBean.class,
                    new Integer(msk.getId()));
            if (milestoneRObj == null) {
            	context.setRollbackOnly();
                return -2;
            }
            output = milestoneRObj.updateMilestone(msk);
            Rlog.debug("milestone",
                    "MilestoneAgentBean.updateMilestone line 7 and output is "
                            + output);
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("milestone",
                    "Error in updateMilestone in MilestoneAgentBean" + e);
            return -2;
        }
        return output;
    }
    
    /**
     * Calls getMilestoneRows() on MilestoneDao
     *
     * @param StudyId.
     *
     * @return MilestoneDao with resultset
     * @see MilestoneDao
     */

    public MilestoneDao getMilestoneRows(int studyId) {
        try {
            Rlog.debug("milestone",
                    "In getMilestoneRows in MilestoneAgentBean - 0");
            MilestoneDao milestoneDao = new MilestoneDao();
            Rlog.debug("milestone",
                    "In getMilestoneRows in MilestoneeAgentBean - 1");
            milestoneDao.getMilestoneRows(studyId);
            Rlog.debug("milestone",
                    "In getMilestoneRows in MilestoneAgentBean - 2");
            return milestoneDao;
        } catch (Exception e) {
            Rlog.fatal("milestone",
                    "Exception In getMilestoneRows in MilestoneAgentBean " + e);
        }
        return null;

    }

    /**
     * Calls getEventsForNew() on MilestoneDao
     *
     * @param StudyId.
     * @param CalendarId.
     * @return MilestoneDao with resultset
     * @see MilestoneDao
     */

    public MilestoneDao getEventsForNew(int studyId, int calId) {
        try {
            Rlog.debug("milestone",
                    "In getEventsForNew in MilestoneAgentBean - 0");
            MilestoneDao milestoneDao = new MilestoneDao();
            Rlog.debug("milestone",
                    "In getEventsForNew in MilestoneeAgentBean - 1");
            milestoneDao.getEventsForNew(studyId, calId);
            Rlog.debug("milestone",
                    "In getEventsForNew in MilestoneAgentBean - 2");
            return milestoneDao;
        } catch (Exception e) {
            Rlog.fatal("milestone",
                    "Exception In getEventsForNew in MilestoneAgentBean " + e);
        }
        return null;

    }

    /**
     * Calls getEventsForNewVisitMilestone() on MilestoneDao
     *
     * @param StudyId.
     * @param CalendarId.
     * @return MilestoneDao with resultset
     * @see MilestoneDao
     */

    public MilestoneDao getEventsForNewVisitMilestone(int studyId, int calId) {
        try {
            Rlog
                    .debug("milestone",
                            "In getEventsForNewVisitMilestone in MilestoneAgentBean - 0");
            MilestoneDao milestoneDao = new MilestoneDao();
            Rlog
                    .debug("milestone",
                            "In getEventsForNewVisitMilestone in MilestoneeAgentBean - 1");
            milestoneDao.getEventsForNewVisitMilestone(studyId, calId);
            Rlog
                    .debug("milestone",
                            "In getEventsForNewVisitMilestone in MilestoneAgentBean - 2");
            return milestoneDao;
        } catch (Exception e) {
            Rlog.fatal("milestone",
                    "Exception In getEventsForNewVisitMilestone in MilestoneAgentBean "
                            + e);
        }
        return null;

    }

    public int setMilestoneDetails(MilestoneDao mdo, String creator,
            String ipAdd) {
        try {
            int totRows = mdo.getCRows();

            MilestoneBean msk = null;

            Rlog.debug("milestone",
                    "MilestoneAgentBean.setMilestoneDetails totRows" + totRows);
            for (int i = 0; i < totRows; i++) {
                Rlog.debug("milestone",
                        "MilestoneAgentBean.setMilestoneDetails i" + i);
                msk = mdo.createMilestoneObj(i);
                msk.setCreator(creator);
                msk.setIpAdd(ipAdd);

                this.setMilestoneDetails(msk);
                Rlog.debug("milestone",
                        "MilestoneAgentBean.setMilestoneDetails testing after create"
                                + i);
            }
            // Rlog.debug("milestone","MilestoneAgentBean.setMilestoneDetails
            // MilestoneStateKeeper " + milestoneRObj);
            // return 2;
            return 0;
        } catch (Exception e) {
            Rlog.fatal("milestone",
                    "Exception in setMilestoneDetails() in MilestoneAgentBean "
                            + e);
            return -1;
        }

    }

    /**
     * Gets Achieved milestones
     *
     * @param mileStoneId
     *            mileStoneId
     */
    public MileAchievedDao getAchievedMilestones(int mileStoneId , String user)
    {
    	MileAchievedDao mile = new MileAchievedDao();

    	try {
            Rlog.debug("milestone","In getAchievedMilestones in MilestoneAgentBean - 0");

            mile.getAchievedMilestones(mileStoneId,user);
            Rlog.debug("milestone","In getAchievedMilestones in MilestoneAgentBean - 2");
            return mile;
        } catch (Exception e) {
            Rlog.fatal("milestone","Exception In getAchievedMilestones in MilestoneAgentBean "
                            + e);
        }
        return null;

    }

    public MilestoneDao getMilestoneRows(int studyId, String mileStoneType, String payType,String milestoneReceivableStatus)
    {
    	 try {

             MilestoneDao milestoneDao = new MilestoneDao();

             milestoneDao.getMilestoneRows(studyId, mileStoneType, payType,milestoneReceivableStatus,0);
             Rlog.debug("milestone","In getMilestoneRows in MilestoneAgentBean - 2");
             return milestoneDao;
         } catch (Exception e) {
             Rlog.fatal("milestone","Exception In getMilestoneRows (int studyId, String mileStoneType, String payType,String milestoneReceivableStatus) in MilestoneAgentBean " + e);
         }
         return null;

    }



//JM: 20MAR2008: #FIN13

    public MilestoneDao getMilestoneRows(int studyId, String mileStoneType, String payType,String milestoneReceivableStatus, String selStatus, String selMilePayType, String srchStrCalName, String srchStrVisitName, String srchStrEvtName, String inActiveFlag, String orderBy, String orderType)
    {
   	 try {

            MilestoneDao milestoneDao = new MilestoneDao();

            milestoneDao.getMilestoneRows(studyId, mileStoneType, payType,milestoneReceivableStatus, selStatus, selMilePayType, srchStrCalName, srchStrVisitName, srchStrEvtName, inActiveFlag, orderBy, orderType);
            Rlog.debug("milestone","In getMilestoneRows in MilestoneAgentBean - 2");
            return milestoneDao;
        } catch (Exception e) {
            Rlog.fatal("milestone","Exception In getMilestoneRows (int studyId, String mileStoneType, String payType,String milestoneReceivableStatus, String selStatus, String selMilePayType, String srchStrCalName, String srchStrVisitName, String srchStrEvtName, String inActiveFlag,String orderBy, String orderType) in MilestoneAgentBean " + e);
        }
        return null;

   }

//JM:

    public MilestoneDao getMilestoneRowsForStudy(int studyId){
		 try {
			 MilestoneDao milestoneDao = new MilestoneDao();
		     milestoneDao.getMilestoneRowsForStudy(studyId);
		     Rlog.debug("milestone","In getMilestoneRowsForStudy in MilestoneAgentBean - 2");
		     return milestoneDao;
		 } catch (Exception e) {
		     Rlog.fatal("milestone","Exception In getMilestoneRowsForStudy (int studyId) in MilestoneAgentBean " + e);
		 }
		 return null;
    }
    
    /*Added for INF-22500 by Yogendra Pratap Singh 24/01/13*/
    public MilestoneDao getMilestoneRowsForSearch(int studyId,HashMap<String,String> hMap){
    	try {
			 MilestoneDao milestoneDao = new MilestoneDao();
		     milestoneDao.getMilestoneRowsForSearch( studyId,  hMap);
		     Rlog.debug("milestone","In getMilestoneRowsForBySearch in MilestoneAgentBean - 2");
		     return milestoneDao;
		 } catch (Exception e) {
		     Rlog.fatal("milestone","Exception In getMilestoneRowsForBySearch in MilestoneAgentBean " + e);
		 }
		 return null;
    }
    
    /*YK 29Mar2011 -DFIN20 */
    public MilestoneDao getStudyMileStoneForSetStatus(int studyId){
    	try {
    		MilestoneDao milestoneDao = new MilestoneDao();
    		milestoneDao.getStudyMileStoneForSetStatus(studyId);
    		Rlog.debug("milestone","In getStudyMileStoneForSetStatus in MilestoneAgentBean - 2");
    		return milestoneDao;
    	} catch (Exception e) {
    		Rlog.fatal("milestone","Exception In getStudyMileStoneForSetStatus (int studyId) in MilestoneAgentBean " + e);
    	}
    	return null;
    }
 
    /* Sonia Abrol, 12/07/06
	 * Apply user notification settings of a milestone to all milestones in a study*/
    public int applyNotificationSettingsToAll(String milestoneID, String userIds, String studyId, String modifiedBy, String ipAdd )
    {
   	 try {
         int retValue = 0;
         MilestoneDao milestoneDao = new MilestoneDao();

         retValue = milestoneDao.applyNotificationSettingsToAll(milestoneID, userIds, studyId, modifiedBy, ipAdd );
         Rlog.debug("milestone","In applyNotificationSettingsToAll in MilestoneAgentBean - 2");
         return retValue;
     } catch (Exception e) {
         Rlog.fatal("milestone","Exception In applyNotificationSettingsToAll(String milestoneID, String userIds, String studyId, String modifiedBy, String ipAdd ) in MilestoneAgentBean " + e);
         return -1;
     }



    }

    public int createMultiMilestones(int userId, String ipAdd,MilestoneDao md, int pkbudget, int study,int bgtcalId)
    {
      	 try {
            int retValue = 0;
            retValue = MilestoneDao.createMultiMilestones( userId, ipAdd,md, pkbudget, study,bgtcalId);


            return retValue;
        } catch (Exception e) {
            Rlog.fatal("milestone","Exception In createMultiMilestones(int userId, String ipAdd,MilestoneDao md, int pkbudget, int study) in MilestoneAgentBean " + e);
            return -1;
        }



       }



    /**
     *
     * @author Sonia Abrol
     * date - 08/26/08
     *
     * Gets milestone Achievemnt details
     *
     * @param studyId
     *            study id
     * @param   milestoneType milestone type code
     * @param htParams ashtable of additional parameters (for later use)
     */
    public MileAchievedDao getALLAchievedMilestones(int studyId, String milestoneType , Hashtable htParams)
      {
    	MileAchievedDao mile = new MileAchievedDao();

    	try {


            mile.getALLAchievedMilestones(studyId, milestoneType , htParams);

            return mile;
        } catch (Exception e) {
            Rlog.fatal("milestone","Exception In getALLAchievedMilestones in MilestoneAgentBean "
                            + e);
        }
        return null;

    }

    /** Sonia Abrol, 08/27/08
	 * delete milestione achievement records from er_mileachieved
	 * @param arPkmileAch Array of PKs to be deleted */

    public int deleteMSAch(String[] arPkmileAch )
    {
    	int ret = 0;

    	try {
             MilestoneDao mile = new MilestoneDao();

             ret = mile.deleteMSAch(arPkmileAch);

            return ret;
        } catch (Exception e) {
            Rlog.fatal("milestone","Exception In deleteMSAch in MilestoneAgentBean "
                            + e);
            return -1;
        }


    }
    
    //Overloaded for INF-18183 ::: Ankit
    public int deleteMSAch(String[] arPkmileAch, Hashtable<String, String> args )
    {
    	int ret = 0;
    	String condition = "";
    	try {
	    		for(int i=0; i<arPkmileAch.length; i++)
	        		condition = condition + ", " + arPkmileAch[i];  
	        	
	        	condition = "pk_mileachieved IN ("+condition.substring(1)+")";
	        	
	        	AuditBean audit=null;
	        	String pkVal = "";
	        	String ridVal = "";
	            String currdate =DateUtil.getCurrentDate();
	            String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
	            String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
	            String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
	            String moduleName = (String)args.get(AuditUtils.APP_MODULE);
	            Hashtable<String, ArrayList<String>> rowValues = AuditUtils.getRowValues("ER_MILEACHIEVED",condition,"eres");/*Fetches the RID/PK_VALUE*/
	            ArrayList<String> rowPK  = (ArrayList<String>)rowValues.get(AuditUtils.ROW_PK_KEY);
	    		ArrayList<String> rowRID  = (ArrayList<String>)rowValues.get(AuditUtils.ROW_RID_KEY);
	    		
	    		for(int i=0; i<rowPK.size(); i++)
				{
	    			pkVal = rowPK.get(i);
	    			ridVal = rowRID.get(i);
	    			audit = new AuditBean("ER_MILEACHIEVED",pkVal,ridVal,
	        			userID,currdate,moduleName,ipAdd,reason);
	    			em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
				}
	    		
             MilestoneDao mile = new MilestoneDao();
             ret = mile.deleteMSAch(arPkmileAch);

            return ret;
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("milestone","Exception In deleteMSAch in MilestoneAgentBean "
                            + e);
            return -1;
        }


    }
	//DFIN-13 BK MAR-16-2011
    
    /**
     * Create milestone records having  parameters in form of JSON array String.
     * @param dataGridArray It contains the string in form of json array of datatable.     
     * @return integer 
     */
    public int createMilestonesUsingJSON(String dataGridString,MilestoneBean msk){
    	JSONArray dataGridArray;    	
    	JSONObject gridRecord = null;
    	MilestoneBean mileBean = null;
    	int output = 0;
    	try{
    		dataGridArray = new JSONArray(dataGridString);   		
    			for (int i = 0; i < dataGridArray.length(); ++i) { 
    				gridRecord = dataGridArray.getJSONObject(i);
    				if(gridRecord.getInt("mileId")==0){    					
    					if(!gridRecord.getBoolean("delete_"+gridRecord.getString("mileTypeId"))){
    						mileBean = new MilestoneBean();
    						mileBean.setIpAdd(msk.getIpAdd());
    						mileBean.setCreator(msk.getCreator());
    						mileBean.setMilestoneStudyId(msk.getMilestoneStudyId());
    						mileBean.setMilestoneDelFlag(msk.getMilestoneDelFlag());
    						mileBean.setMileStoneBeanJSON(gridRecord,'C'); 
    						em.persist(mileBean);
    						output = mileBean.getId();
    						//System.out.println("created record --"+gridRecord);		
    					}    		 	
    					
    				}
    			}
    		}    		

     catch(Exception e){
    	 Rlog.fatal("milestone","Exception In updateMilestonesUsingJSON in MilestoneAgentBean "
                 + e);
    	 	return -1;
     }
       
         	return output;
    }
  
     
    
    /**
     * Update milestone records having  parameters in form of JSON array.
     * @param dataGridArray JSONArray
     * @param updateArray   JSONArray
     * @return
     */

    public int updateMilestonesUsingJSON(String dataGridString,String updateString){
    	int recIndex;
    	JSONArray dataGridArray;
    	JSONArray updateArray;
    	JSONObject rec = null;
    	JSONObject gridRecord = null;
    	MilestoneBean mileBean = null;
    	int output = 0;
    	try{
    		dataGridArray = new JSONArray(dataGridString); 
    		if(!"[]".equals(updateString) || !StringUtil.isEmpty(updateString)){
    			updateArray = new JSONArray(updateString);    		
    			for (int i = 0; i < updateArray.length(); ++i) { 
    				rec = updateArray.getJSONObject(i);
    				//System.out.println("update"+rec.get(""+i));
    				recIndex = rec.getInt(""+i);
    				recIndex--;
    				gridRecord = dataGridArray.getJSONObject(recIndex);    				
    				if(gridRecord.getInt("mileId")!=0){    					
    					if(!gridRecord.getBoolean("delete_"+gridRecord.getString("mileTypeId"))){
    						mileBean = (MilestoneBean) em.find(MilestoneBean.class,new Integer(gridRecord.getInt("mileId")));
    						output = mileBean.setMileStoneBeanJSON(gridRecord,'U');
    						//System.out.println("update record --"+gridRecord);	
    			            em.merge(mileBean);
    					}   		 	
    				}
    			}
    		}      		

    	}	
     catch(Exception e){
    	 Rlog.fatal("milestone","Exception In updateMilestonesUsingJSON in MilestoneAgentBean "
                 + e);
    	 	return -1;
     }
       
         	return output;
    }
    
    /**
     * Overloaded Method for setting the Values of Modified By and IP Address
     * Update milestone records having  parameters in form of JSON array.
     * @param dataGridArray JSONArray
     * @param updateArray   JSONArray
     * @return
     */
    
    public int updateMilestonesUsingJSON(String dataGridString,String updateString,MilestoneBean msk){
    	int recIndex;
    	JSONArray dataGridArray;
    	JSONArray updateArray;
    	JSONObject rec = null;
    	JSONObject gridRecord = null;
    	MilestoneBean mileBean = null;
    	int output = 0;
    	try{
    		dataGridArray = new JSONArray(dataGridString); 
    		if(!"[]".equals(updateString) || !StringUtil.isEmpty(updateString)){
    			updateArray = new JSONArray(updateString);    		
    			for (int i = 0; i < updateArray.length(); ++i) { 
    				rec = updateArray.getJSONObject(i);
    				//System.out.println("update"+rec.get(""+i));
    				recIndex = rec.getInt(""+i);
    				recIndex--;
    				gridRecord = dataGridArray.getJSONObject(recIndex);    				
    				if(gridRecord.getInt("mileId")!=0){    					
    					if(!gridRecord.getBoolean("delete_"+gridRecord.getString("mileTypeId"))){
    						mileBean = (MilestoneBean) em.find(MilestoneBean.class,new Integer(gridRecord.getInt("mileId")));
    						mileBean.setModifiedBy(msk.getCreator());
    						mileBean.setIpAdd(msk.getIpAdd());
    						output = mileBean.setMileStoneBeanJSON(gridRecord,'U');
    						//System.out.println("update record --"+gridRecord);	
    						em.merge(mileBean);
    					}   		 	
    				}
    			}
    		}      		
    		
    	}	
    	catch(Exception e){
    		Rlog.fatal("milestone","Exception In updateMilestonesUsingJSON in MilestoneAgentBean "
    				+ e);
    		return -1;
    	}
    	
    	return output;
    }
    
    
    /**
     * Delete milestone records having  parameters in form of JSON array String.
     * @param dataGridArray It contains the string in form of json array of datatable.     
     * @return integer 
     */
     
     public int deleteMilestonesUsingJSON(String dataGridString){
    	JSONArray dataGridArray;    	
    	JSONObject gridRecord = null;
    	MilestoneBean mileBean = null;
    	int output = 0;
    	try{
    		dataGridArray = new JSONArray(dataGridString);   		
    			    		
    			for (int i = 0; i < dataGridArray.length(); ++i) { 
    				gridRecord = dataGridArray.getJSONObject(i);
    				if(gridRecord.getInt("mileId")!=0){    					
    					if(gridRecord.getBoolean("delete_"+gridRecord.getString("mileTypeId"))){
    						mileBean = (MilestoneBean) em.find(MilestoneBean.class,new Integer(gridRecord.getInt("mileId")));
    						mileBean.setMilestoneDelFlag("Y");
    						em.merge(mileBean);
    						output = 0 ;
    						//System.out.println("delete record --"+gridRecord);	    						
    					}     	
    				}
    			}
    		}    		
     catch(Exception e){
    	 Rlog.fatal("milestone","Exception In updateMilestonesUsingJSON in MilestoneAgentBean "
                 + e);
    	 	return -1;
     }
         	return output;
    }
 
    // Overloaded for INF-18183 ::: Akshi
    public int deleteMilestonesUsingJSON(String dataGridString,Hashtable<String, String> args){
    	JSONArray dataGridArray;    	
    	JSONObject gridRecord = null;
    	MilestoneBean mileBean = null;
    	int output = 0;
    	String condition="";
    	try{
    		AuditBean audit=null;
        	String pkVal = "";
        	String ridVal = "";           
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/           
            String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/           
            String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/ 
            String appmodule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the application module name from the Hashtable*/
            dataGridArray = new JSONArray(dataGridString);   	    		
			
            for(int j=0; j<dataGridArray.length(); j++)
            {
            	gridRecord = dataGridArray.getJSONObject(j);                        
    		condition = condition + ", " + gridRecord.getInt("mileId");  
            }
            
			condition = "PK_MILESTONE IN ("+condition.substring(1)+")";
			
        	Hashtable<String, ArrayList<String>> getRowValues = AuditUtils.getRowValues("ER_MILESTONE",condition,"eres");/*Fetches the RID/PK_VALUE*/
        	ArrayList<String> rowPK  = (ArrayList<String>)getRowValues.get(AuditUtils.ROW_PK_KEY);
    		ArrayList<String> rowRID  = (ArrayList<String>)getRowValues.get(AuditUtils.ROW_RID_KEY);
    		//String getRidValue= AuditUtils.getRidValues("ER_PATPROT","PK_PATPROT="+condition);/*Fetches the RID/PK_VALUE*/ 
            for(int i=0; i<rowPK.size(); i++)
			{
    			pkVal = rowPK.get(i);
    			ridVal = rowRID.get(i);
    			audit = new AuditBean("ER_MILESTONE",pkVal,ridVal,
        			userID,currdate,appmodule,ipAdd,reason);
    			em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
			}
    		dataGridArray = new JSONArray(dataGridString);   		
    			    		
    			for (int i = 0; i < dataGridArray.length(); ++i) { 
    				gridRecord = dataGridArray.getJSONObject(i);
    				if(gridRecord.getInt("mileId")!=0){    					
    					if(gridRecord.getBoolean("delete_"+gridRecord.getString("mileTypeId"))){
    						mileBean = (MilestoneBean) em.find(MilestoneBean.class,new Integer(gridRecord.getInt("mileId")));
    						mileBean.setMilestoneDelFlag("Y");
    						mileBean.setModifiedBy(userID);
    						mileBean.setIpAdd(ipAdd);
    						em.merge(mileBean);
    						output = 0 ;    								
    					}     	
    				}
    			}
    			if(output!=0)
    			{context.setRollbackOnly();}; /*Checks whether the actual delete has occurred with return 0 else, rollback */
    	      	}    	
    	
     catch(Exception e){
    	 context.setRollbackOnly();
    	 Rlog.fatal("milestone","Exception In updateMilestonesUsingJSON in MilestoneAgentBean "
                 + e);
    	 	return -1;
     }
         	return output;
    }
    
    /* FIN-22373 09-Oct-2012 -Sudhir*/
    public MilestoneDao getMilestoneRows(int studyId, String mileStoneType, String payType,String milestoneReceivableStatus, String selStatus, String selMilePayType, String srchStrCalName, String srchStrVisitName, String srchStrEvtName, String inActiveFlag, String orderBy, String orderType , String datefrom, String dateto)
    {
   	 try {

            MilestoneDao milestoneDao = new MilestoneDao();

            milestoneDao.getMilestoneRows(studyId, mileStoneType, payType,milestoneReceivableStatus, selStatus, selMilePayType, srchStrCalName, srchStrVisitName, srchStrEvtName, inActiveFlag, orderBy, orderType , datefrom, dateto);
            Rlog.debug("milestone","In getMilestoneRows in MilestoneAgentBean - 2");
            return milestoneDao;
        } catch (Exception e) {
            Rlog.fatal("milestone","Exception In getMilestoneRows (int studyId, String mileStoneType, String payType,String milestoneReceivableStatus, String selStatus, String selMilePayType, String srchStrCalName, String srchStrVisitName, String srchStrEvtName, String inActiveFlag,String orderBy, String orderType) in MilestoneAgentBean " + e);
        }
        return null;

   }

   /* FIN-22373 09-Oct-2012 -Sudhir*/ 
 }// end of class
