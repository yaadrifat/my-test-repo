package com.velos.eres.service.util;

import java.lang.reflect.Field;

/**
 * CFG stands for Config. Class to hold all the key strings in configBundle.properties file.
 * Make all key strings public static.
 * Start each key with a category prefix, and arrange them alphabetically.
 *
 */
public class CFG {
	private static final String EMPTY_STR = "";
	
	public static String getValueByKey(String key) {
		String messageText = EMPTY_STR;
		synchronized(CFG.class) {
			Field field;
			try {
				field = CFG.class.getField(key);
				CFG objectMC = new CFG();

				try {
					Object value = field.get(objectMC);
					messageText = value.toString(); 
				} catch(Exception e) {
					System.out.println(e.getMessage());
				}
			} catch (SecurityException e1) {
				e1.printStackTrace();
			} catch (NoSuchFieldException e1) {
				e1.printStackTrace();
			}
		}
		return messageText;
	}
	
	public static void reload() {
		synchronized(CFG.class) {
			Field[] fields = CFG.class.getFields();
			for (Field field : fields) {
				try {
					if (field.getName() != null && field.getName().endsWith("_Upper")) {
						field.set(null, VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE,
								field.getName().replaceAll("_Upper", "")).toUpperCase());
					} else if (field.getName() != null && field.getName().endsWith("_Lower")) {  
						field.set(null, VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE,
								field.getName().replaceAll("_Lower", "")).toLowerCase());
					} else {
						field.set(null, VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE,field.getName()));
					}
				} catch(Exception e) {
					System.out.println(e.getMessage());
				}
			}
		}
	}
	
	public static String Study_TAreaDiseaseSite = VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE, "Study_TAreaDiseaseSite");

	public static String Workflows_Enabled = VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE, "Workflows_Enabled");
	public static String Workflow_PatEnroll_WfSwitch = VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE, "Workflow_PatEnroll_WfSwitch");
	public static String Workflow_PatEnroll_WfType = VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE, "Workflow_PatEnroll_WfType");
	public static String Workflow_FlexStudy_WfTypes = VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE, "Workflow_FlexStudy_WfTypes");

	public static String AE_DISABLE_MULAE_LINK = VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE, "AE_DISABLE_MULAE_LINK");
	public static String Config_Submission_Interceptor_Class = VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE,
			"Config_Submission_Interceptor_Class");
	public static String Config_Submission_Validation_Interceptor_Class = VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE,
			"Config_Submission_Validation_Interceptor_Class");
	public static String Submission_Teaugo_Url = VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE, "Submission_Teaugo_Url");
	public static String Submission_Username = VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE, "Submission_Username");
	public static String Submission_Token = VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE, "Submission_Token");
	
	public static String EIRB_MODE = VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE, "EIRB_MODE");
	public static String EIRB_DEFAULT_STUDY_STAT_ORG = VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE, "EIRB_DEFAULT_STUDY_STAT_ORG");
	public static String EIRB_CAT_NEW_LINK = VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE, "EIRB_CAT_NEW_LINK");
	public static String EIRB_CAT_VIEW_LINK = VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE, "EIRB_CAT_VIEW_LINK");

}
