/*
 *  Classname : DynRepAgentBean
 *
 *  Version information: 1.0
 *
 *  Date: 11/05/2003
 *
 *  Copyright notice: Velos, Inc
 *
 *  Author: Vishal Abrol
 */

/*Modified by Sonia Abrol. 01/26/05, added a new attribute formType to methodd  - updateAllCols,setAllCols. 
 This attribute is added to distinguish between application forms and core table lookup forms*/

package com.velos.eres.service.dynrepdtAgent.impl;

/*
 * Import Statements
 */

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.dynrepdt.impl.DynRepDtBean;
import com.velos.eres.service.dynrepdtAgent.DynRepDtAgentRObj;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/*
 * End of Import Statements
 */

/**
 * The stateless session EJB acting as a facade for the entity CMP - AgentBean.
 * <br>
 * <br>
 * 
 * 
 * @author Vishal Abrol
 * @created October 25, 2004
 * @see Dyn
 * @version 1.0 11/05/2003
 * @ejbHome DynRepDtAgentHome
 * @ejbRemote DynRepDtAgentRObj
 */

@Stateless
public class DynRepDtAgentBean implements DynRepDtAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * Gets the dynRepDtDetails attribute of the DynRepDtAgentBean object
     * 
     * @param id
     *            Description of the Parameter
     * @return The dynRepDtDetails value
     */
    public DynRepDtBean getDynRepDtDetails(int id) {
        DynRepDtBean retrieved = null;

        try {
            retrieved = (DynRepDtBean) em.find(DynRepDtBean.class, new Integer(
                    id));
            return retrieved;

        } catch (Exception e) {
            Rlog.fatal("dynrep",
                    "Exception in getDynRepDtDetails() in DynRepDtAgentBean"
                            + e);
        }

        return retrieved;
    }

    /**
     * Creates dynrep record.
     * 
     * @param dsk
     *            The new dynRepDtDetails value
     * @return int for successful:0; e\Exception : -2
     */

    public int setDynRepDtDetails(DynRepDtBean dsk) {
        try {
            DynRepDtBean dt = new DynRepDtBean();
            dt.updateDynRepDt(dsk);
            em.persist(dt);

            return dt.getId();
        } catch (Exception e) {
            Rlog.fatal("dynrep",
                    "Error in setDynRepDtDetails() in DynRepDTAgentBean " + e);
            return -2;
        }

    }

    /**
     * Updates a dynrep record.
     * 
     * @param dsk
     *            Description of the Parameter
     * @return int for successful:0 ; e\Exception : -2
     */
    public int updateDynRepDt(DynRepDtBean dsk) {

        DynRepDtBean retrieved = null;
        int output;

        try {
            retrieved = (DynRepDtBean) em.find(DynRepDtBean.class, new Integer(
                    dsk.getId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateDynRepDt(dsk);
            em.merge(retrieved);
        } catch (Exception e) {
            Rlog.fatal("dynrep", "Error in updateDynRepDt in DynRepDtAgentBean"
                    + e);
            return -2;
        }
        return output;
    }

    /**
     * Removes a dynrep record.
     * 
     * @param id
     *            Description of the Parameter
     * @return int for successful:0; e\Exception : -2
     */

    public int removeDynRepDt(int id) {
        int output;
        DynRepDtBean retrieved = null;

        try {

            retrieved = (DynRepDtBean) em.find(DynRepDtBean.class, new Integer(
                    id));

            em.remove(retrieved);
            return 0;
        } catch (Exception e) {
            Rlog.fatal("dynrep", "Exception in removeDynrep" + e);
            return -1;
        }

    }

    /**
     * Sets the allCols attribute of the DynRepDtAgentBean object
     * 
     * @param repId
     *            The new allCols value
     * @param repCols
     *            The new allCols value
     * @param colSeqs
     *            The new allCols value
     * @param colWidths
     *            The new allCols value
     * @param colFormats
     *            The new allCols value
     * @param colDisps
     *            The new allCols value
     * @param usr
     *            The new allCols value
     * @param ipadd
     *            The new allCols value
     * @param colNames
     *            The new allCols value
     * @param colDataTypes
     *            The new allCols value
     * @param formId
     *            The new allCols value
     * @param formType
     *            The new allCols value
     * @param useUniqueId
     *            The new allCols value
     * @param useDataValues
     *            The new allCols value
     * 
     * @return Description of the Return Value
     */
    public int setAllCols(String repId, ArrayList repCols, ArrayList colSeqs,
            ArrayList colWidths, ArrayList colFormats, ArrayList colDisps,
            String usr, String ipadd, ArrayList colNames,
            ArrayList colDataTypes, String formId, String formType,
            ArrayList useUniqueIds, ArrayList useDataValues ,ArrayList calcSums, ArrayList calcPers,ArrayList repeatNumbers) {
        Rlog.debug("dynrep", "SetAllCols in DynRepDtAgentBean line 1" + repId
                + "**" + ipadd + "**" + usr);
        int i = 0;
        int rows = repCols.size();
        try {
            for (i = 0; i < rows; i++) {
                // lsk = this.getLineitemDetails(lineitemId);
                DynRepDtBean dsk = new DynRepDtBean();
                dsk.setRepId(repId);
                dsk.setRepCol((String) repCols.get(i));
                dsk.setColSeq((String) colSeqs.get(i));
                dsk.setColWidth((String) colWidths.get(i));
                dsk.setColFormat((String) colFormats.get(i));
                dsk.setColDisp((String) colDisps.get(i));
                dsk.setColName((String) colNames.get(i));
                dsk.setColType((String) colDataTypes.get(i));
                dsk.setCreator(usr);
                dsk.setIpAdd(ipadd);
                dsk.setFormId(formId);
                dsk.setFormType(formType);
                dsk.setUseUniqueId((String) useUniqueIds.get(i));
                dsk.setUseDataValue((String) useDataValues.get(i));
                dsk.setCalcSum((String) calcSums.get(i));
                dsk.setCalcPer((String) calcPers.get(i));
                dsk.setRepeatNumber((String) repeatNumbers.get(i));

                if (this.setDynRepDtDetails(dsk) == -2) {
                    return -2;
                }
            }
        } catch (Exception e) {
            Rlog.fatal("dynrep", "Error in setAllCols in DynRepDtAgentBean "
                    + e);
            return -2;
        }
        return 0;
    }

    /**
     * Description of the Method
     * 
     * @param dyncolIds
     *            Description of the Parameter
     * @param repId
     *            Description of the Parameter
     * @param repCols
     *            Description of the Parameter
     * @param colSeqs
     *            Description of the Parameter
     * @param colWidths
     *            Description of the Parameter
     * @param colFormats
     *            Description of the Parameter
     * @param colDisps
     *            Description of the Parameter
     * @param usr
     *            Description of the Parameter
     * @param ipadd
     *            Description of the Parameter
     * @param colNames
     *            Description of the Parameter
     * @param colDataTypes
     *            Description of the Parameter
     * @param formId
     *            The new allCols value
     * @param formType
     *            The new allCols value
     * @param useUniqueId
     *            The new allCols value
     * @param useDataValues
     *            The new allCols value
     * 
     * @return Description of the Return Value
     */
    public int updateAllCols(ArrayList dyncolIds, String repId,
            ArrayList repCols, ArrayList colSeqs, ArrayList colWidths,
            ArrayList colFormats, ArrayList colDisps, String usr, String ipadd,
            ArrayList colNames, ArrayList colDataTypes, String formId,
            String formType, ArrayList useUniqueIds, ArrayList useDataValues ,ArrayList calcSums, ArrayList calcPers ,ArrayList repeatNumbers) {
        int i = 0;

        int rows = dyncolIds.size();
        int dyncolId = 0;
        try {
            for (i = 0; i < rows; i++) {
                DynRepDtBean dsk = new DynRepDtBean();
                dyncolId = StringUtil.stringToNum((String) dyncolIds.get(i));
                Rlog.debug("dynrep",
                        "dynrepDtAgentBean:updateAllcols:dyncolId:" + dyncolId);
                if (dyncolId == 0) {
                    dsk.setRepId(repId);
                    Rlog.debug("dynrep",
                            "dynrepDtAgentBean:updateAllcols:repId:" + repId);
                    dsk.setRepCol((String) repCols.get(i));
                    Rlog.debug("dynrep",
                            "dynrepDtAgentBean:updateAllcols:repcols:"
                                    + repCols.get(i));
                    dsk.setColSeq((String) colSeqs.get(i));
                    Rlog.debug("dynrep",
                            "dynrepDtAgentBean:updateAllcols:repseq:"
                                    + colSeqs.get(i));
                    dsk.setColWidth((String) colWidths.get(i));
                    Rlog.debug("dynrep",
                            "dynrepDtAgentBean:updateAllcols:rwidth"
                                    + colWidths.get(i));
                    dsk.setColFormat((String) colFormats.get(i));
                    Rlog.debug("dynrep",
                            "dynrepDtAgentBean:updateAllcols:format:"
                                    + colFormats.get(i));
                    dsk.setColDisp((String) colDisps.get(i));
                    Rlog.debug("dynrep",
                            "dynrepDtAgentBean:updateAllcols:disp:"
                                    + colDisps.get(i));
                    dsk.setColName((String) colNames.get(i));
                    Rlog.debug("dynrep",
                            "dynrepDtAgentBean:updateAllcols:colNames:"
                                    + colNames.get(i));
                    dsk.setColType((String) colDataTypes.get(i));
                    Rlog.debug("dynrep",
                            "dynrepDtAgentBean:updateAllcols:colTypes:"
                                    + colDataTypes.get(i));
                    dsk.setCreator(usr);
                    Rlog.debug("dynrep", "dynrepDtAgentBean:updateAllcols:usr:"
                            + usr);
                    dsk.setIpAdd(ipadd);
                    Rlog.debug("dynrep",
                            "dynrepDtAgentBean:updateAllcols:ipadd:" + ipadd);
                    dsk.setFormId(formId);
                    Rlog.debug("dynrep",
                            "dynrepDtAgentBean:updateAllcols:formId:" + formId);
                    dsk.setFormType(formType);
                    Rlog.debug("dynrep",
                            "dynrepDtAgentBean:updateAllcols:formType:"
                                    + formType);

                    dsk.setUseUniqueId((String) useUniqueIds.get(i));
                    dsk.setUseDataValue((String) useDataValues.get(i));
                    
                    dsk.setCalcSum((String) calcSums.get(i));
                    dsk.setCalcPer((String) calcPers.get(i));
                    dsk.setRepeatNumber((String) repeatNumbers.get(i));


                    if (this.setDynRepDtDetails(dsk) == -2) {
                        return -2;
                    }
                }
                // end for if clause
                else if (dyncolId < 0) {
                    dyncolId = StringUtil.stringToNum(StringUtil.integerToString(
                            new Integer(dyncolId)).substring(1));
                    this.removeDynRepDt(dyncolId);

                } else {
                    dsk = this.getDynRepDtDetails(dyncolId);
                    dsk.setRepId(repId);
                    Rlog.debug("dynrep",
                            "dynrepDtAgentBean:updateAllcols:repId:" + repId);
                    dsk.setRepCol((String) repCols.get(i));
                    Rlog.debug("dynrep",
                            "dynrepDtAgentBean:updateAllcols:repcols:"
                                    + repCols.get(i));
                    dsk.setColSeq((String) colSeqs.get(i));
                    Rlog.debug("dynrep",
                            "dynrepDtAgentBean:updateAllcols:repseq:"
                                    + colSeqs.get(i));
                    dsk.setColWidth((String) colWidths.get(i));
                    Rlog.debug("dynrep",
                            "dynrepDtAgentBean:updateAllcols:rwidth"
                                    + colWidths.get(i));
                    dsk.setColFormat((String) colFormats.get(i));
                    Rlog.debug("dynrep",
                            "dynrepDtAgentBean:updateAllcols:format:"
                                    + colFormats.get(i));
                    dsk.setColDisp((String) colDisps.get(i));
                    Rlog.debug("dynrep",
                            "dynrepDtAgentBean:updateAllcols:disp:"
                                    + colDisps.get(i));
                    dsk.setColName((String) colNames.get(i));
                    Rlog.debug("dynrep",
                            "dynrepDtAgentBean:updateAllcols:colNames:"
                                    + colNames.get(i));
                    dsk.setColType((String) colDataTypes.get(i));
                    Rlog.debug("dynrep",
                            "dynrepDtAgentBean:updateAllcols:colTypes:"
                                    + colDataTypes.get(i));
                    dsk.setModifiedBy(usr);
                    dsk.setIpAdd(ipadd);
                    dsk.setFormId(formId);
                    dsk.setFormType(formType);
                    dsk.setUseUniqueId((String) useUniqueIds.get(i));
                    dsk.setUseDataValue((String) useDataValues.get(i));
                    
                    dsk.setCalcSum((String) calcSums.get(i));
                    dsk.setCalcPer((String) calcPers.get(i));
                    dsk.setRepeatNumber((String) repeatNumbers.get(i));


                    Rlog.debug("dynrep",
                            "dynrepDtAgentBean:updateAllcols:formType:"
                                    + formType);
                    if (this.updateDynRepDt(dsk) == -2) {
                        return -2;
                    }
                }
            }
        } catch (Exception e) {
            Rlog.debug("dynrep", "Error in updateAllCols in DynRepDtAgentBean "
                    + e);
            return -2;
        }
        return 0;
    }

    /**
     * Description of the Method
     * 
     * @param Ids
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public int removeAllCols(ArrayList Ids) {
        Rlog.debug("dynrep", "RemoveAllCols in DynRepDtAgentBean line 1" + Ids);
        int i = 0;
        int rows = Ids.size();

        try {
            for (i = 0; i < rows; i++) {
                if (this.removeDynRepDt(StringUtil
                        .stringToNum((String) Ids.get(i))) < 0) {
                    return -2;
                }
            }
        } catch (Exception e) {
            Rlog.fatal("dynrep", "Error in RemoveAllCols in DynRepDtAgentBean "
                    + e);
            return -2;
        }
        return 0;
    }

}
