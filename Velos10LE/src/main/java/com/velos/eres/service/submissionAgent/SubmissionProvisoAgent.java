package com.velos.eres.service.submissionAgent;

import java.util.ArrayList;
import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.eres.business.patTXArm.impl.PatTXArmBean;
import com.velos.eres.business.submission.impl.SubmissionProvisoBean;

@Remote
public interface SubmissionProvisoAgent {
    public SubmissionProvisoBean getSubmissionProvisoDetails(int id);
    public Integer createSubmissionProviso(SubmissionProvisoBean SubmissionProvisoBean);
    

    /**
     * updates the SubmissionProviso Record
     */
    public int updateSubmissionProviso(SubmissionProvisoBean ptk);

    /**
     * remove Submission Proviso Record
     * 
     */
    public int removeSubmissionProviso(int provisoPK);
    
 // Overloaded for INF-18183 ::: AGodara
    public int removeSubmissionProviso(int provisoPK,Hashtable<String, String> auditInfo);

    /** Returns Provisos added for a submission and Submission board */
    public ArrayList getSubmissionProvisos(int fkSubmission, int fkSubmissionBoard );
    
}
