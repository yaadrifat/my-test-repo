/*
 * Classname			UserSessionAgentBean
 * 
 * Version information 	1.0
 *
 * Date					03/04/2003
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.service.userSessionAgent.impl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.userSession.impl.UserSessionBean;
import com.velos.eres.service.userSessionAgent.UserSessionAgentRObj;
import com.velos.eres.service.util.Rlog;

/**
 * The stateless session EJB acting as a facade for the entity CMP.<br>
 * <br>
 * 
 * @author Sonia Sahni
 * @version 1.0 03/04/2003
 */
@Stateless
@Remote( { UserSessionAgentRObj.class })
public class UserSessionAgentBean implements UserSessionAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * 
     */
    private static final long serialVersionUID = 3761690095664575026L;

    /**
     * getUserSessionDetails Calls getUserSessionDetails() on entity bean -
     * UserSessionBean Looks up on the UserSession id parameter passed in and
     * constructs and returns a UserSessionStateKeeper.
     * 
     * @param userSessionId
     * @see UserSessionBean
     */
    public UserSessionBean getUserSessionDetails(int userSessionId) {

        UserSessionBean userSessionBean = null;

        try {
            userSessionBean = (UserSessionBean) em.find(UserSessionBean.class,
                    new Integer(userSessionId));
            return userSessionBean;
        } catch (Exception e) {
            Rlog.fatal("usersession",
                    "Error in getUserSessionDetails() in UserSessionAgentBean"
                            + e);
            return userSessionBean;
        }

    }

    /**
     * Creates a new a UserSession. Calls setUserSessionDetails() on entity bean
     * UserSession
     * 
     * @param a
     *            UserSessionStateKeeper containing UserSession attributes to be
     *            set.
     * @return generated UserSession id
     * @see UserSessionBean
     */

    public int setUserSessionDetails(UserSessionBean usk) {
        UserSessionBean usb = new UserSessionBean();

        try {
            usb.updateUserSession(usk);
            em.persist(usb);

            return usb.getId();
        } catch (Exception e) {
            Rlog.fatal("usersession",
                    "Error in setUserSessionDetails() in UserSessionAgentBean"
                            + e);
            e.printStackTrace();
            return -1;
        }

    }

    /**
     * Updates UserSession Details. Calls updateUserSession() on entity bean
     * UserSessionBean
     * 
     * @param a
     *            UserSessionStateKeeper containing UserSession attributes to be
     *            set.
     * @return int 0 for successful ; -2 for exception
     * @see UserSessionBean
     */

    public int updateUserSession(UserSessionBean usk) {

        UserSessionBean userSessionBean = null;
        int output;

        try {

            userSessionBean = (UserSessionBean) em.find(UserSessionBean.class,
                    usk.getId());
            if (userSessionBean == null) {
                return -2;
            }
            output = userSessionBean.updateUserSession(usk);
            em.merge(userSessionBean);
            return output;
        } catch (Exception e) {
            Rlog.debug("usersession",
                    "Error in updateUserSession in UserSessionAgentBean" + e);
            return -2;
        }

    }

}// end of class
