/*
 * Classname			ExtURightsAgentBean.class
 * 
 * Version information
 *
 * Date					03/16/2001
 * 
 * Copyright notice
 */

package com.velos.eres.service.extURightsAgent.impl;

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.common.CtrlDao;
import com.velos.eres.business.extURights.impl.ExtURightsBean;
import com.velos.eres.service.extURightsAgent.ExtURightsAgentRObj;
import com.velos.eres.service.util.Rlog;

/**
 * The stateless session EJB acting as a facade for the entity CMP.<br>
 * <br>
 * 
 * @author
 */
@Stateless
public class ExtURightsAgentBean implements ExtURightsAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * Looks up on the id parameter passed in and constructs and returns a state
     * holder. containing the details of the External User Rights<br>
     * 
     * @param id
     *            the External User Rights id
     */
    public ExtURightsBean getExtURightsDetails(int id) {
        ExtURightsBean retrieved = null;

        String extURight = null;
        CtrlDao cntrl;
        ArrayList cVal, cDesc, cSeq;
        cVal = new ArrayList();
        cVal = new ArrayList();
        cSeq = new ArrayList();
        int sLength = 0;
        int counter = 0;

        cntrl = new CtrlDao();
        try {

            retrieved = (ExtURightsBean) em.find(ExtURightsBean.class,
                    new Integer(id));
            Rlog.debug("exturights", "remote Object For External User Rights"
                    + retrieved);
            extURight = retrieved.getExtRights(); // Getting the Rights
            // assigned to external
            // users on each study
            // feature

            sLength = extURight.length();
            Rlog.debug("exturights", "Length Of the External User Rights"
                    + sLength);
            for (counter = 0; counter <= (sLength - 1); counter++) {
                retrieved.setFtrRights(String
                        .valueOf(extURight.charAt(counter)));
                retrieved.setGrSeq(Integer.toString(counter));
            }

            cntrl.getControlValues("extu_rights");
            cVal = cntrl.getCValue();
            cDesc = cntrl.getCDesc();
            retrieved.setGrValue(cVal);
            retrieved.setGrDesc(cDesc);
        } catch (Exception e) {
            e.printStackTrace();
            Rlog.fatal("exturights", "EXCEPTION" + e);
        }

        return retrieved;
    }

    /**
     * Sets External User Rights.
     * 
     * @param External
     *            User Rights state holder containing Rights attributes to be
     *            set.
     * @return int
     */

    public int setExtURightsDetails(ExtURightsBean esk) {
        int stdTeamId = 0;
        int counter = 0;
        int sLength = 0;
        ArrayList extURht = new ArrayList();
        String rights = "";
        String studyId, userId; // for checking the duplicacy of records
        ExtURightsBean eb = new ExtURightsBean();
        try {

            studyId = esk.getStudyId();
            userId = esk.getUserId();

            /*
             * Enumeration enum_velos = extURightsHome.findByStudyUser(studyId,
             * userId); if (enum_velos.hasMoreElements()) return -1; // check
             * the duplicacy of records for study and user // and return -1 if
             * record already exists
             */

            extURht = esk.getFtrRights();
            sLength = extURht.size();

            Rlog.debug("exturights", "Size of Array List External User Rights "
                    + sLength);

            for (counter = 0; counter <= (sLength - 1); counter++) {
                rights += extURht.get(counter).toString();
            }

            esk.setExtRights(rights);

            eb.updateExtURights(esk);
            em.persist(eb);

        } catch (Exception e) {
            Rlog.fatal("exturights", "EXCEPTION" + e);
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * Updates External User Rights.
     * 
     * @param External
     *            User Rights state holder containing Rights attributes to be
     *            set.
     * @return int
     */

    public int updateExtURights(ExtURightsBean esk) {
        int stdTeamId = 0;
        int counter = 0;
        int sLength = 0;
        ArrayList extURht = new ArrayList();
        String rights = "";
        ExtURightsBean retrieved;
        int result = 0;
        try {

            retrieved = (ExtURightsBean) em.find(ExtURightsBean.class,
                    new Integer(esk.getId()));

            extURht = retrieved.getFtrRights();

            sLength = extURht.size();
            Rlog.debug("exturights", "Size of Array List External User Rights "
                    + sLength);
            for (counter = 0; counter <= (sLength - 1); counter++) {
                rights += extURht.get(counter).toString();
            }
            Rlog
                    .debug("exturights", "String of External User Rights "
                            + rights);

            esk.setExtRights(rights);

            result = retrieved.updateExtURights(esk);
            em.merge(retrieved);
        } catch (Exception e) {
            Rlog.fatal("exturights", "EXCEPTION" + e);
            e.printStackTrace();
            return -2;
        }
        return result;
    }

}// end of class

