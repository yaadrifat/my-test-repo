/**
 * This is the FormStatAgentBean
 * @author Salil
 * Date 07/29/2003
 **/

package com.velos.eres.service.formStatAgent.impl;

/* Import Statements */

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.common.FormStatusDao;
import com.velos.eres.business.formStat.impl.FormStatBean;
import com.velos.eres.service.formStatAgent.FormStatAgentRObj;
import com.velos.eres.service.util.Rlog;

/* End of Import Statements */

@Stateless
public class FormStatAgentBean implements FormStatAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    // /Business Methods

    public FormStatBean getFormStatDetails(int fromStatId) {
        FormStatBean retrieved = null; // Form Stat Entity Bean Remote
        // Object

        try {
            retrieved = (FormStatBean) em.find(FormStatBean.class, new Integer(
                    fromStatId));

        } catch (Exception e) {
            Rlog
                    .fatal(
                            "formstat",
                            "Exception in getting form status  Details in getFormStatDetails in FormStatAgentBean"
                                    + e);
        }

        return retrieved;
    }

    public int updateFormStatDetails(FormStatBean fssk) {

        FormStatBean form = null; // FormStat Entity Bean Remote
        // Object
        int output;

        try {
            form = (FormStatBean) em.find(FormStatBean.class, new Integer(fssk
                    .getFormStatId()));

            if (form == null) {
                return -2;
            }
            output = form.updateFormStatDetails(fssk);

        } catch (Exception e) {
            Rlog
                    .fatal(
                            "formstat",
                            "Exception in updating form status details in updateFormStatDetails in FormStatAgentBean"
                                    + e);
            return -2;
        }
        return output;
    }

    // //this is a method to set the form status

    public int setFormStatDetails(FormStatBean fssk) {
        try {
            em.merge(fssk);
            return (fssk.getFormStatId());
        } catch (Exception e) {
            Rlog
                    .fatal(
                            "formstat",
                            "Exception in setting form status details in setFormStatDetails in FormStatAgentBean"
                                    + e);
            return 0;
        }

    }

    /**
     * Method to get the Form Status
     * 
     * @return FormStatusDao
     */

    public FormStatusDao getFormStatusHistory(int formId) {
        FormStatusDao formStatusDao = new FormStatusDao();

        try {
            formStatusDao.getFormStatusHistory(formId);
            return formStatusDao;
        } catch (Exception e) {
            Rlog.fatal("formstatagent",
                    "Exception in getFormStatusHistory in FormStatAgentBean "
                            + e);
            return null;
        }
    }

}// end of class

