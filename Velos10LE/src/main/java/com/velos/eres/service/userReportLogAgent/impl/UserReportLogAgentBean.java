/**
 * 
 */
package com.velos.eres.service.userReportLogAgent.impl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.userReportLog.impl.UserReportLogBean;
import com.velos.eres.service.userReportLogAgent.UserReportLogAgentRObj;
import com.velos.eres.service.util.Rlog;

/**
 * The stateless session EJB <br>
 * 
 * @author Yogendra Pratap Singh
 * @version 1.0 09/18/2012
 */

@Stateless
@Remote( { UserReportLogAgentRObj.class })
public class UserReportLogAgentBean implements UserReportLogAgentRObj {
	@PersistenceContext(unitName = "eres")
	protected EntityManager em;

	/**
	 * 
	 */
	private static final long serialVersionUID = -1914622153382982374L;
	private static final String USR_RPT_LOG = "User Report Logging";

	/**
	 * @return int
	 */
	public int setUserReportLogDetails(UserReportLogBean userReportLogBean) {
		Rlog.info(USR_RPT_LOG, "UserReportLogAgentBean.setUserReportLogDetails() method start");
		try {
			Rlog.info(USR_RPT_LOG, "Before persisting the values");
			em.persist(userReportLogBean);
			Rlog.info(USR_RPT_LOG,"UserReportLogAgentBean.setUserReportLogDetails() method end with PK_USR_RPT_LOG :: "+ userReportLogBean.getPkUserReportLog());
			return userReportLogBean.getPkUserReportLog();
		} catch (Exception e) {
			Rlog.fatal(USR_RPT_LOG,	"Error in setUserReportLogDetails() in UserReportLogAgentBean" + e);
			e.printStackTrace();
			return -1;
		}
	}

}
