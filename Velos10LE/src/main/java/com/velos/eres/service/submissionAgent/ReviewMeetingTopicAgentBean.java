package com.velos.eres.service.submissionAgent;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;

import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.submission.impl.ReviewMeetingTopicBean;
import com.velos.eres.business.submission.impl.SubmissionProvisoBean;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;

@Stateless
@Remote( { ReviewMeetingTopicAgent.class } )

public class ReviewMeetingTopicAgentBean implements ReviewMeetingTopicAgent {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    
    @Resource
    private SessionContext context;

    public int createMeetingTopic(ReviewMeetingTopicBean reviewMeetingTopicBean) {
        int output = 0;
        try {
            ReviewMeetingTopicBean newBean = new ReviewMeetingTopicBean();
            newBean.setDetails(reviewMeetingTopicBean);
            em.persist(newBean);
            output = newBean.getId();
        } catch(Exception e) {
            Rlog.fatal("submission", "Exception in SubmissionAgentBean.createSubmission "+e);
            output = -1;
        }
        return output;
    }
    
    public ReviewMeetingTopicBean getMeetingTopic(int id) {
        return (ReviewMeetingTopicBean) em.find(ReviewMeetingTopicBean.class, id);
    }

    public int updateMeetingTopic(ReviewMeetingTopicBean reviewMeetingTopicBean) {
        int output = 0;
        ReviewMeetingTopicBean bean = 
            (ReviewMeetingTopicBean) em.find(ReviewMeetingTopicBean.class, 
                    reviewMeetingTopicBean.getId());
        if (bean == null) { return output;}
        try {
            if (reviewMeetingTopicBean.getFkReviewMeeting() != null &&
                    reviewMeetingTopicBean.getFkReviewMeeting() > 0) { 
                bean.setFkReviewMeeting(reviewMeetingTopicBean.getFkReviewMeeting());
            }
            bean.setTopicNumber(reviewMeetingTopicBean.getTopicNumber());
            bean.setMeetingTopic(reviewMeetingTopicBean.getMeetingTopic());
            bean.setLastModifiedBy(reviewMeetingTopicBean.getLastModifiedBy());
            bean.setLastModifiedDate(Calendar.getInstance().getTime());
            em.merge(bean);
            output = bean.getId();
        } catch (Exception e) {
            Rlog.fatal("submission", "Exception in ReviewMeetingTopicAgentBean.updateMeetingTopic "+e);
            output = -1;
        }
        return output;
    }
    
    public ArrayList findMeetingTopicsByFkReviewMeeting(int fkReviewMeeting) {
        Query query = em.createNamedQuery("findByFkReviewMeeting");
        query.setParameter("fkReviewMeeting", fkReviewMeeting);
        ArrayList list = (ArrayList) query.getResultList();
        if (list == null || list.size() == 0) { return null; }
        return list;
    }

    public ReviewMeetingTopicBean findByFkReviewMeetingAndNumber(int fkReviewMeeting, 
            String topicNumber) {
        Query query = em.createNamedQuery("findByFkReviewMeetingAndTopicNumber");
        query.setParameter("fkReviewMeeting", fkReviewMeeting);
        query.setParameter("topicNumber", topicNumber);
        ArrayList list = (ArrayList) query.getResultList();
        if (list == null || list.size() == 0) { return null; }
        return (ReviewMeetingTopicBean)list.get(0);
    }

    /**
     * remove ReviewMeetingTopi Record
     * 
     */
    public int removeReviewMeetingTopic(int topicPK)
    {
    	   int output;
    	   ReviewMeetingTopicBean rtb = null;

           try {

               rtb = (ReviewMeetingTopicBean) em.find(ReviewMeetingTopicBean.class,
                       new Integer(topicPK));
               em.remove(rtb);
               return 0;
           } catch (Exception e) {
               Rlog.fatal("submission", "Exception in removeReviewMeetingTopic" + e);
               return -1;
           }
   }
 // Overloaded for INF-18183 ::: AGodara
    public int removeReviewMeetingTopic(int topicPK,Hashtable<String, String> auditInfo)
    {
    	   int output;
    	   ReviewMeetingTopicBean rtb = null;

           try {

        		AuditBean audit=null;
               
            	String currdate =DateUtil.getCurrentDate();
            	String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); //Fetches the User ID from the Hashtable
                String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); //Fetches the IP Address from the Hashtable
                String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); //Fetches the reason for deletion from the Hashtable
                String app_module=(String)auditInfo.get(AuditUtils.APP_MODULE); //Fetches the reason for deletion from the Hashtable
                String rid= AuditUtils.getRidValue("ER_REVIEW_MEETING_TOPIC","eres","PK_REVIEW_MEETING_TOPIC="+topicPK); //Fetches the RID
                
                //POPULATE THE AUDIT BEAN 
                audit = new AuditBean("ER_REVIEW_MEETING_TOPIC",String.valueOf(topicPK),rid,userID,currdate,app_module,ipAdd,reason);
        	    rtb = (ReviewMeetingTopicBean) em.find(ReviewMeetingTopicBean.class,
                       new Integer(topicPK));
                em.persist(audit); //PERSIST THE DATA IN AUDIT TABLE
   			   	em.remove(rtb);
               return 0;
           } catch (Exception e) {
        	   context.setRollbackOnly();
               Rlog.fatal("submission", "Exception in removeReviewMeetingTopic" + e);
               return -1;
           }
    }
}
