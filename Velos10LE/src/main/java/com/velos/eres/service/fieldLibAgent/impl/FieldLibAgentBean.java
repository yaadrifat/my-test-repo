/*
 * Classname : FieldLibAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 03/07/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sonia Kaura
 */

package com.velos.eres.service.fieldLibAgent.impl;

/* Import Statements */

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.common.FieldActionDao;
import com.velos.eres.business.common.FieldLibDao;
import com.velos.eres.business.common.FldRespDao;
import com.velos.eres.business.common.LookupDao;
import com.velos.eres.business.fieldLib.impl.FieldLibBean;
import com.velos.eres.business.fldValidate.impl.FldValidateBean;
import com.velos.eres.service.fieldLibAgent.FieldLibAgentRObj;
import com.velos.eres.service.fldValidateAgent.FldValidateAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* End of Import Statements */

/**
 * The stateless session EJB acting as a facade for the entity CMP -
 * FieldLibAgentBean.<br>
 * <br>
 * 
 * @author SoniaKaura
 * @see FieldLibBean
 * @version 1.0 24/06/2003
 * @ejbHome FieldLibAgentHome
 * @ejbRemote FieldLibAgentRObj
 */

@Stateless
public class FieldLibAgentBean implements FieldLibAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * 
     * Looks up on the FieldLib id parameter passed in and constructs and
     * returns a FieldLib state keeper. containing all the details of the Field
     * Lib <br>
     * 
     * @param fieldLibId
     *            the FieldLib id
     */

    public FieldLibBean getFieldLibDetails(int fieldLId) {
        FieldLibBean retrieved = null;

        try {

            retrieved = (FieldLibBean) em.find(FieldLibBean.class, new Integer(
                    fieldLId));
            Rlog
                    .debug("fieldlib",
                            "FieldLibAgentBean.getFieldLibDetails after getFieldStateKeeper:");
            return retrieved;

        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "Error in getFieldLibDetails() in FieldLibAgentBean" + e);
        }

        return retrieved;

    }

    /**
     * Creates FieldLib record.
     * 
     * @param a
     *            State Keeper containing the FieldLib attributes to be set.
     * @return int for successful:0; e\Exception : -2
     */

    public int setFieldLibDetails(FieldLibBean fieldLsk) {
        try {
            FieldLibBean fb = new FieldLibBean();

            fb.updateFieldLib(fieldLsk);
            em.persist(fb);

            return fb.getFieldLibId();

        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "Error in setFieldLibDetails() in FieldLibAgentBean " + e);

        }
        return -2;

    }

    /**
     * This method is an overloaded method. Would be called in case of multiple
     * choice fields will first update the field and then its responses
     * responses are stored in DAO Calls setFieldLibDetails() of FieldLib
     * Session Bean: FieldLibAgentBean
     * 
     * @param a
     *            State Keeper containing the FieldLib attributes to be set.
     * @param FldRespDao -
     *            dao with multiple responses
     * @return int for successful:PK of field; e\Exception : -2
     */

    public int setFieldLibDetails(FieldLibBean fieldLsk, FldRespDao fldRespDao) {
        try {
            int fieldLibId = 0;
            FieldLibBean fb = new FieldLibBean();

            fb.updateFieldLib(fieldLsk);
            em.persist(fb);

            fieldLibId = fb.getFieldLibId();
            return fieldLibId;

        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "Error in setFieldLibDetails() in FieldLibAgentBean " + e);
            return -2;
        }

    }

    /**
     * Updates a FieldLib record.
     * 
     * @param a
     *            FieldLib state keeper containing FieldLib attributes to be
     *            set.
     * @return int for successful:0; e\Exception : -2
     */
    public int updateFieldLib(FieldLibBean fieldLsk) {

        FieldLibBean retrieved = null; // Entity Bean Remote Object
        int output;

        try {

            retrieved = (FieldLibBean) em.find(FieldLibBean.class, new Integer(
                    fieldLsk.getFieldLibId()));

            if (retrieved == null) {
                return -2;
            }

            output = retrieved.updateFieldLib(fieldLsk);
            em.merge(retrieved);

        } catch (Exception e) {
            Rlog.debug("fieldlib",
                    "Error in updateFieldLib in FieldLibAgentBean" + e);
            return -2;
        }
        return output;
    }

    /**
     * Removes a FieldLib record.
     * 
     * @param a
     *            FieldLib Primary Key
     * @return int for successful:0; e\Exception : -2
     */

    public int removeFieldLib(int fieldLId) {
        int output;
        FieldLibBean retrieved = null;

        try {

            Rlog.debug("fieldlib",
                    "in try bloc of FieldLib agent:removeFieldLib");

            retrieved = (FieldLibBean) em.find(FieldLibBean.class, new Integer(
                    fieldLId));

            em.remove(retrieved);
            Rlog.debug("fieldlib", "Removed FieldLib row with FieldLibID = "
                    + fieldLId);
            return 0;
        } catch (Exception e) {
            Rlog.debug("fieldlib", "Exception in removeFieldLib " + e);
            return -1;

        }

    }

    /**
     * updates FieldLib record.
     * 
     * @param a
     *            FieldLib Primary Key,flag,user,ipaddress
     * @return int for successful:0; e\Exception : -2
     */

    public int updateFieldLibRecord(int pkField, String libFlag, String user,
            String ipAdd) {
        int ret;
        try {
            Rlog.debug("fieldlib",
                    "In updateFieldLibRecord in FieldLibRespAgentBean - 0");
            FieldLibDao fldLibRDao = new FieldLibDao();

            ret = fldLibRDao
                    .updateFieldLibRecord(pkField, libFlag, user, ipAdd);

        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "Exception In updateFieldLibRecord in FieldLibAgentBean "
                            + e);
            return -2;
        }

        return ret;
    }

    /**
     * 
     * 
     * @param fkCatlib
     * @param fkAccount
     * @param fkFormId
     * @param name
     * @param keyword
     * @return FieldLibDao
     */
    public FieldLibDao getFieldsFromSearch(int fkCatlib, int fkAccount,
            int fkFormId, String name, String keyword) {

        try {
            Rlog.debug("fieldlib",
                    "FieldLibAgentBean.getFieldsFromSearch starting");
            FieldLibDao fieldLibDao = new FieldLibDao();
            fieldLibDao.getFieldsFromSearch(fkCatlib, fkAccount, fkFormId,
                    name, keyword);
            return fieldLibDao;
        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "Exception In getFieldsFromSearch in FieldLibAgentBean "
                            + e);
        }
        return null;
    }

    // //////////////////////////////////////////////////////////////////////////

    public int insertToFormField(FieldLibBean fieldLsk) {

        int pk_fld = 0;
        int pk_fldVal = 0;
        String sysId = "";

        try {

            Rlog.debug("fieldlib", "IN FIELD LIB AGENT THE SECTION VALUE "
                    + fieldLsk.getFormSecId());
            pk_fld = setFieldLibDetails(fieldLsk);

        }

        catch (Exception e) {

            Rlog
                    .fatal(
                            "fieldlib",
                            "Exception insertToFormField(FieldLibStateKeeper fieldLsk	) in FieldLibAgentBean:INSERT INTO ER_FLDLIB "
                                    + e);
        }

        return pk_fld;

    }

    // //////////////////////////////////////////
    public int updateToFormField(FieldLibBean fieldLsk) {

        int pk_fld = 0;
        int pk_fldVal = 0;

        Rlog
                .debug("fieldlib",
                        "In updateToFormField(FieldLibStateKeeper fieldLsk in FieldLinAgentBean	");

        try {

            pk_fld = updateFieldLib(fieldLsk);

            Rlog.debug("fieldlib", "SONIA SONIA--error code	" + pk_fld);
            /*
             * After the attributes of the fieldLib are updated, it updates the
             * fldValidate attributes.It calls updateFldValidate() of
             * fldValidate Session Bean: FldValidateAgentBean
             */
            if (pk_fld > 0) {
                Rlog
                        .debug("fieldlib",
                                "updateToFormField:Inside when pk_fld is greater than 0");

                FldValidateBean fvsk = fieldLsk.getFldValidateBean();
                FldValidateAgentRObj fldValidateAgentRObj = EJBUtil
                        .getFldValidateAgentHome();

                Rlog.debug("fieldlib", "updateToFormField:After statekeeper");
                pk_fldVal = fldValidateAgentRObj.updateFldValidate(fvsk);
                Rlog.debug("fieldlib", "updateToFormField:Value of pk_fldval"
                        + pk_fldVal);
            }

        }

        catch (Exception e) {

            Rlog
                    .fatal(
                            "fieldlib",
                            "Exception updateToFormField(FieldLibStateKeeper fieldLsk	) in FieldLibAgentBean:INSERT INTO ER_FLDLIB "
                                    + e);
        }

        return pk_fld;

    }

    public int copyMultipleFields(String secId, String[] idArray,
            String creator, String ipAdd) {
        int ret = 0;
        try {
            Rlog.debug("fieldlib",
                    "In copyMultipleFields in FieldLibAgentBean - 0");
            FieldLibDao fieldLibDao = new FieldLibDao();
            ret = fieldLibDao
                    .copyMultipleFields(secId, idArray, creator, ipAdd);

        } catch (Exception e) {
            Rlog
                    .fatal("fieldlib",
                            "Exception In copyMultipleFields in FieldLibAgentBean "
                                    + e);
        }
        return ret;

    }

    public FieldLibDao getFieldNames(int formId) {

        try {
            Rlog.debug("fieldlib", "In getFieldNames in getFieldNames - 0");
            FieldLibDao fieldLibDao = new FieldLibDao();

            fieldLibDao.getFieldNames(formId);
            return fieldLibDao;

        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "Exception In getFieldNames in FieldLibAgentBean " + e);

        }

        return null;
    }

    public FieldLibDao getFltrdFieldNames(int formId) {

        try {
            Rlog.debug("fieldlib", "In getFieldNames in getFieldNames - 0");
            FieldLibDao fieldLibDao = new FieldLibDao();

            fieldLibDao.getFltrdFieldNames(formId);
            return fieldLibDao;

        } catch (Exception e) {
            Rlog
                    .fatal("fieldlib",
                            "Exception In getFltrdFieldNames in FieldLibAgentBean "
                                    + e);

        }

        return null;
    }

    public FieldLibDao getFieldsInformation(int formId) {
        try {
            Rlog.debug("fieldlib",
                    "In getFieldsInformation in getFieldsInformation - 0");
            FieldLibDao fieldLibDao = new FieldLibDao();

            fieldLibDao.getFieldsInformation(formId);
            return fieldLibDao;

        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "Exception In getFieldsInformation in FieldLibAgentBean "
                            + e);

        }

        return null;

    }

    public int updateBrowserFlag(String browserOne, String browserZero,
            int lastModBy, String recordType, String ipAdd)

    {
        int ret;
        try {
            Rlog.debug("fieldlib",
                    "In updateBrowserFlag in FieldLibRespAgentBean - 0");
            FieldLibDao fldLibRDao = new FieldLibDao();
            ret = fldLibRDao.updateBrowserFlag(browserOne, browserZero,
                    lastModBy, recordType, ipAdd);
        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "Exception In updateFieldLibRecord in FieldLibAgentBean "
                            + e);
            return -2;
        }

        return ret;
    }

    // //////////////////////

    /**
     * 
     * 
     * @param fkCatlib
     * @param fkAccount
     * 
     * @param name
     * @param keyword
     * @return FieldLibDao
     */
    public FieldLibDao getFieldsForCopy(int fkCatlib, int fkAccount,
            String name, String keyword) {

        try {
            Rlog.debug("fieldlib",
                    "FieldLibAgentBean.getFieldsForCopy starting");
            FieldLibDao fieldLibDao = new FieldLibDao();
            fieldLibDao.getFieldsForCopy(fkCatlib, fkAccount, name, keyword);
            return fieldLibDao;
        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "Exception In getFieldsForCopy in FieldLibAgentBean " + e);
        }
        return null;
    }

    /**
     * 
     * @param fkCatlib
     * @param fkAccount
     * @param fkFormId
     * @param name
     * @param keyword
     * @return FieldLibDao
     */
    public FieldLibDao getFieldNamesForSection(int formId, int sectionId,
            String fldType) {

        try {
            Rlog.debug("fieldlib", "FieldLibAgentBean.getFieldNamesForSection");
            FieldLibDao fieldLibDao = new FieldLibDao();
            fieldLibDao.getFieldNamesForSection(formId, sectionId, fldType);
            return fieldLibDao;
        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "Exception In getFieldNamesForSection in FieldLibAgentBean "
                            + e);
        }
        return null;
    }

    /**
     * 
     * @param account
     * @return LookupDao
     */
    public LookupDao getAllLookpUpViews(String account) {
        try {

            LookupDao lkpDao = new LookupDao();
            lkpDao.getAllViews(EJBUtil.stringToNum(account));
            return lkpDao;
        } catch (Exception e) {
            Rlog
                    .fatal("fieldlib",
                            "Exception In getAllLookpUpViews in FieldLibAgentBean "
                                    + e);
        }
        return null;
    }

    /**
     * 
     * @param viewId
     * @return LookupDao
     */
    public LookupDao getLookpUpViewColumns(String viewId) {
        try {
            LookupDao lkpDao = new LookupDao();
            lkpDao.getReturnValues(viewId);
            return lkpDao;
        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "Exception In getLookpUpViewColumns in FieldLibAgentBean "
                            + e);
        }
        return null;
    }

    public FieldLibDao getEditMultipleFields(int formId) {
        try {
            Rlog.debug("fieldlib",
                    "In getEditMultipleFields in getFieldsInformation - 0");
            FieldLibDao fieldLibDao = new FieldLibDao();

            fieldLibDao.getEditMultipleFields(formId);
            return fieldLibDao;

        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "Exception In getEditMultipleFields in FieldLibAgentBean "
                            + e);

        }

        return null;

    }

    
    /**
     * returns true if field as any inter field action associated with the field
     * 
     * @param fieldId
     * 
     *            
     */

    public boolean hasInterFieldActions(String fieldId) 
    {
        try {
            Rlog.debug("fieldlib","In hasInterFieldActions in FieldLibAgentBean - 0");
           
            return FieldActionDao.hasInterFieldActions(fieldId);
            

        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "Exception In hasInterFieldActions in FieldLibAgentBean "
                            + e);
            return true;
        }
        
    }

    
}
