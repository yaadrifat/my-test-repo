package com.velos.eres.service.util;

import javax.servlet.http.HttpSession;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.velos.eres.web.study.StudyJB;

public final class WorkflowUtil {
	public static int getEntityId(String wfPageId, HttpSession tSession){
		int entityId = 0;

		if (wfPageId.equals(FlxPageUtil.getFlexStudyPageName())) {
			StudyJB studyJB = null;
			try {
				studyJB = (StudyJB)(tSession.getAttribute("studyJB"));
			} catch (Exception e){
				return 0;
			}
			if (null != studyJB){
				entityId = studyJB.getId();
			} else return 0;
		}
		return entityId;
	}

	public static String getWorkflowTypes(String wfPageId){
		String workflowTypes = null;

		if (wfPageId.equals(FlxPageUtil.getFlexStudyPageName())) {
			final String DUMMY_StudyWorkflowTypes ="[Workflow_FlexStudy_WfTypes]";
			if (!DUMMY_StudyWorkflowTypes.equals(CFG.Workflow_FlexStudy_WfTypes)){
				workflowTypes = CFG.Workflow_FlexStudy_WfTypes;
			}
		}
		return workflowTypes;
	}

	public static String getWorkflowFormName(String wfPageId){
		String workflowFormName = null;

		if (wfPageId.equals(FlxPageUtil.getFlexStudyPageName())) {
			workflowFormName = "document.studyScreenForm";
		}
		return workflowFormName;
	}
	
    public static String getOnclickJS(Object...args) {
    	String wfPageId = (String) args[0];
    	int taskId = StringUtil.stringToNum(""+args[1]);
    	String onclickJSONStr = (String)args[2];
    	
    	String onclickJS ="";
    	if (StringUtil.isEmpty(onclickJSONStr)){
    		return onclickJS;
    	}

    	try {
			JSONObject onclickJSON = new JSONObject(onclickJSONStr);
			String moduleId = onclickJSON.getString("moduleId");
	    	if (StringUtil.isEmpty(moduleId)){
	    		return onclickJS;
	    	}

	    	if ("ERES_FORM".equals(moduleId)){
	    		/*
	    		 * Config JSON example- {"moduleId":"ERES_FORM", "moduleDet":{"formId":123, }}
	    		 * */
	    		JSONObject moduleDet = onclickJSON.getJSONObject("moduleDet");
	    		if (null == moduleDet) return onclickJS;

	    		int formId = moduleDet.getInt("formId");
	    		if (formId <= 0) return onclickJS;
	    		String htmlFormName = getWorkflowFormName(wfPageId);

		    	onclickJS = "$j('#taskImg"+taskId+"').click(function(){"
		    		+ "	return openlinkedForm('"+formId+"','M','SA',"+ htmlFormName +",'workflow','');"
		    		+ "});";
		    	
		    	onclickJS += "$j('#taskImg"+taskId+"').hover(function(){"
		    		+ "$j(this).css('cursor','hand');"
		    		+ "});";
	    	}
		} catch (JSONException e) {
			e.printStackTrace();
		}

    	return onclickJS;
    }
}
