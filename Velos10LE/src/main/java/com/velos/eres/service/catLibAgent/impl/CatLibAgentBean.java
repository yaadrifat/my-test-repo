/*
 * Classname			CatLibAgentBean
 * 
 * Version information : 
 *
 * Date					06/25/2003
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.catLibAgent.impl;

/* IMPORT STATEMENTS */

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.velos.eres.business.catLib.impl.CatLibBean;
import com.velos.eres.business.common.CatLibDao;
import com.velos.eres.service.catLibAgent.CatLibAgentRObj;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP.
 * 
 * @author Anu
 * @version 1.0, 06/25/2003
 * @ejbHome CatLibAgentHome
 * @ejbRemote CatLibAgentRObj
 */

@Stateless
public class CatLibAgentBean implements CatLibAgentRObj {

    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * Looks up on the Category id parameter passed in and constructs and
     * returns a CatLibStateKeeper containing all the summary details of the
     * category
     * 
     * @param accId
     *            The Account id
     * @see AccountStateKeeper
     */
    public CatLibBean getCatLibDetails(int catLibId) {
        CatLibBean cb = null; // Category Entity Bean Remote Object
        try {

            cb = (CatLibBean) em.find(CatLibBean.class, new Integer(catLibId));

        } catch (Exception e) {
            Rlog.fatal("catlib",
                    "Error in getCatLibDetails() in CatLibAgentBean" + e);
        }

        return cb;
    }

    /**
     * Add the category details in the CatLibStateKeeper Object to the database
     * 
     * @param catlib
     *            An CatLibStateKeeper containing category attributes to be set.
     * @return The category Id for the category just created <br>
     *         0 - if the method fails
     */

    // Modified by Manimaran to find the duplicate Category or FormType Name on
    // May25,2005 for May Enhancement.
    public int setCatLibDetails(CatLibBean catlib) {

        try {

            String catLibName = "";
            String catLibType = "";
            String recordType = "";
            int accId = 0;

            CatLibBean cl = new CatLibBean();

            accId = StringUtil.stringToNum(catlib.getCatLibAcc());
            catLibName = catlib.getCatLibName();
            catLibType = catlib.getCatLibType();
            recordType = catlib.getRecordType();

            if (!StringUtil.isEmpty(catLibName)) {

                Query query = em.createNamedQuery("findByCatLibIdentifier");

                query.setParameter("fk_account", accId);
                query.setParameter("CATLIB_NAME", catLibName);
                query.setParameter("CATLIB_TYPE", catLibType);

                ArrayList list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    Rlog
                            .debug("catlib",
                                    "CatLibAgentBean.setCatLibDetails: the category not unique");
                    return -3;
                }

            }

            cl.updateCatLib(catlib);
            em.persist(cl);
            return (cl.getCatLibId());
        } catch (Exception e) {
            Rlog.fatal("catlib",
                    "Error in setCatLibDetails() in CatLibAgentBean " + e);
        }
        return 0;
    }

    /**
     * Update the category details contained in the CatLibStateKeeper Object to
     * the database
     * 
     * @param clsk
     *            An CatLibStateKeeper containing category attributes to be
     *            updated.
     * @return 0 - if the update is successful<br>
     *         -2 - if the update fails
     */

    // Modified by Manimaran to find the duplicate Category or FormType name on
    // May25,2005 for May Enhancement.
    public int updateCatLib(CatLibBean clsk) {

        CatLibBean retrieved = null; // User Entity Bean Remote Object
        int output;

        String catLibName = "";
        String catLibType = "";
        String catLibName1 = "";
        String recordType = "";

        try {

            retrieved = (CatLibBean) em.find(CatLibBean.class, new Integer(clsk
                    .getCatLibId()));

            if (retrieved == null) {
                return -2;
            }

            catLibName = clsk.getCatLibName();
            catLibType = clsk.getCatLibType();
            recordType = clsk.getRecordType();
            catLibName = catLibName.trim();

            catLibName1 = retrieved.getCatLibName();

            if (StringUtil.isEmpty(catLibName1)) {
                catLibName1 = "";
            } else {
                catLibName1 = catLibName1.trim();
            }

            if (!catLibName1.equalsIgnoreCase(catLibName)) {
                if (!StringUtil.isEmpty(catLibName))

                {
                    Query query = em.createNamedQuery("findByCatLibIdentifier");
                    query.setParameter("fk_account", StringUtil.stringToNum(clsk
                            .getCatLibAcc()));
                    query.setParameter("CATLIB_NAME", catLibName);
                    query.setParameter("CATLIB_TYPE", catLibType);

                    ArrayList list = (ArrayList) query.getResultList();
                    if (list == null)
                        list = new ArrayList();
                    if (list.size() > 0) {
                        Rlog
                                .debug("catlib",
                                        "CatLibAgentBean.setCatLibDetails: the category not unique");
                        return -3;
                    }
                    // ///////

                }

            }

            output = retrieved.updateCatLib(clsk);
            em.merge(retrieved);
        } catch (Exception e) {
            Rlog
                    .debug("catlib", "Error in updateCatLib in CatLibAgentBean"
                            + e);
            return -2;
        }
        return output;
    }

    /**
     * 
     * 
     * @param accountId
     * @param type
     * @return CatLibDao
     */
    public CatLibDao getCategoriesWithAllOption(int accountId, String type,
            int forLastAll) {

        try {
            Rlog.debug("catlib",
                    "CatLibAgentBean.getCategoriesWithAllOption starting");
            CatLibDao catLibDao = new CatLibDao();
            catLibDao.getCategoriesWithAllOption(accountId, type, forLastAll);
            return catLibDao;
        } catch (Exception e) {
            Rlog.fatal("catlib",
                    "Exception In getCategoriesWithAllOption in CatLibAgentBean "
                            + e);
        }
        return null;
    }

    /**
     * 
     * 
     * @param accountId
     * @param type
     * @return CatLibDao
     */
    public CatLibDao getAllCategories(int accountId, String type) {

        try {
            Rlog.debug("catlib",
                    "CatLibAgentBean.getCategoriesWithAllOption starting");
            CatLibDao catLibDao = new CatLibDao();
            catLibDao.getAllCategories(accountId, type);
            return catLibDao;
        } catch (Exception e) {
            Rlog.fatal("catlib",
                    "Exception In getAllCategories in CatLibAgentBean " + e);
        }
        return null;
    }

}// end of class

