/*
 * Classname			StorageAgentRObj
 *
 * Version information : 1.0
 *
 * Date					06/25/2007
 *
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.storageAgent;

import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.eres.business.storage.impl.StorageBean;
import com.velos.eres.business.common.StorageDao;

/**
 * Remote interface for StorageAgentBean session EJB
 *
 * @author Manimaran
 * @version 1.0, 06/25/2007
 */
@Remote
public interface StorageAgentRObj {
    public StorageBean getStorageDetails(int pkStorage);

    public int setStorageDetails(StorageBean storage);

    public int updateStorage(StorageBean storage);

    public int deleteStorages(String[] deleteIds,int flag, int user);//KM
    
    // Overloaded for INF-18183 ::: Akshi
    public int deleteStorages(String[] deleteIds,int flag, int user,Hashtable<String, String> args);//KM

    public int getCount(String storageName);

    public int getCountId(String storageId, String accId);//KM

    public String getStorageIdAuto() ;

    public String getStorageIdAuto(String fkCodelstStorageType, int accId);

    public StorageDao getStorageValue(int pkStorage);

    public String findAllChildStorageUnitIds(String storageId);//JM: 28Dec2007

    public String findParentStorageType(String storageId); //JM: 22Jan2008

    public int copyStorageUnitsAndTemplates(String[] strgPks,String[] copyCounts,String[] strgIds,String[] strgNames,String[] templates, String usr, String ipAdd);//KM

    public StorageDao searchStorage(Hashtable args,int account) ;

    public StorageDao getImmediateChildren(int storagePK);

    public StorageDao getAllChildren(int pkStorageKit);//JM: 21Aug2009

    public StorageDao getSpecimensAndPatients(int pkStorage, int account);
    
    public int getStorageCount(String fkStorage); // BT: For INV 21675
    
    public int getChildCount(String fkStorage);

 }
