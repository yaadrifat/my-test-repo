/*
 * Classname			PatLoginAgentRObj
 * 
 * Version information : 1.0  
 *
 * Date					04/05/2007
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.patLoginAgent;

import javax.ejb.Remote;


import com.velos.eres.business.patLogin.impl.PatLoginBean;
import com.velos.eres.business.user.impl.UserBean;



/**
 * Remote interface for PatLoginAgentBean session EJB
 * 
 * @author Jnanamay Majumdar
 * @version 1.0, 04/05/2007
 */
@Remote
public interface PatLoginAgentRObj {
    
	public PatLoginBean getPatLoginDetails(int pb);

    public int setPatLoginDetails(PatLoginBean pb);    
  
    public int updatePatLogin(PatLoginBean pb);   
    
    public void notifyPatient(String patientPK,String loginName,String password,String user,String portalPK);
    
    public int deletePatLogin(int id);
    
    public PatLoginBean validateLogin(String LoginName);
    
    /**
     * Checks if there is a patient login for the given Login name and Password
     * 
     * @param patlogin
     *             patient login name
     * @password 
     * 			    patient password            
     * @return PatLoginBean object with login details in case its a valid
     *         login. Returns null object in case of invalid login/pwd combination
     */

    public PatLoginBean validateLoginPassword(String loginname, String password,String ipAdd);
            
}
