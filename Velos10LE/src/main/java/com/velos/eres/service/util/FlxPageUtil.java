package com.velos.eres.service.util;

public final class FlxPageUtil {
	private static final String FLEX_STUDY_PAGE_NAME = "protocol";

	public static String getFlexStudyPageName() {
		return FLEX_STUDY_PAGE_NAME;
	}
}
