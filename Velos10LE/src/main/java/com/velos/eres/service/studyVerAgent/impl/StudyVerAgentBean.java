/*
 * Classname			StudyVerAgentBean.class 
 * 
 * Version information
 *
 * Date				11/25/2002
 * 
 * Copyright notice
 */

package com.velos.eres.service.studyVerAgent.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Hashtable;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.StudyVerDao;
import com.velos.eres.business.studyVer.impl.StudyVerBean;
import com.velos.eres.service.studyVerAgent.StudyVerAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The stateless session EJB acting as a facade for the entity BMP.<br>
 * <br>
 * 
 * @author Arvind
 */

@Stateless
public class StudyVerAgentBean implements StudyVerAgentRObj {

    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    
	@Resource
	private SessionContext context;

    /**
     * Looks up on the StudyVer id parameter passed in and constructs and
     * returns a StudyVer state keeper. containing all the details of the
     * studyVer<br>
     * 
     * @param studyVerId
     *            the StudyVer id
     */
    public StudyVerBean getStudyVerDetails(int studyVerId) {
        StudyVerBean retrieved = null;

        try {
            retrieved = (StudyVerBean) em.find(StudyVerBean.class, new Integer(
                    studyVerId));

        } catch (Exception e) {
            Rlog.fatal("studyVer",
                    "Error in getStudyVerDetails() in StudyVerAgentBean" + e);
        }

        return retrieved;
    }

    /**
     * Sets an StudyVer.
     * 
     * @param a
     *            studyVer state keeper containing studyVer attributes to be
     *            set.
     * @return void
     */

    public int setStudyVerDetails(StudyVerBean srsk) {
        try {

        	Query query = em.createNamedQuery("findStudyVerNum");
            query.setParameter("FK_STUDY", StringUtil.stringToNum(srsk.getStudyVerStudyId()));
//JM: 051906            
            //query.setParameter("STUDYVER_NUMBER", srsk.getStudyVerNumber());
            query.setParameter("studyVerNumber", srsk.getStudyVerNumber());
            query.setParameter("studyVerCategory", srsk.getStudyVercat());
            ArrayList list = (ArrayList) query.getResultList();
            if (list == null)
                list = new ArrayList();
            if (list.size() > 0) {
                // same version number exists
                Rlog
                        .debug("studyVer",
                                "StudyVerAgentBean.setStudyVerDetails : the version number is not unique");
                return -3;
            }

            em.merge(srsk);
            return srsk.getStudyVerId();
        } catch (Exception e) {
            Rlog.fatal("studyVer",
                    "Error in setStudyVerDetails() in StudyVerAgentBean " + e);
        }        
               	
        return 0;
    }
    
    
    

    public int updateStudyVer(StudyVerBean srsk) {

    	
    	StudyVerBean retrieved = null; // User Entity Bean Remote Object
        int output;

        try {

            retrieved = (StudyVerBean) em.find(StudyVerBean.class, new Integer(
                    srsk.getStudyVerId()));                     
            
            if (retrieved == null) {
                return -2;
            }
            
            String retrievedVercat = StringUtil.trueValue(retrieved.getStudyVercat());
            String srskVercat = srsk.getStudyVercat();

            if (!((retrieved.getStudyVerNumber()).toUpperCase())
                    .equals((srsk.getStudyVerNumber()).toUpperCase()) 
                    || !retrievedVercat.equals(srskVercat)) {

                try {

                	Query query = em.createNamedQuery("findStudyVerNum");
                    query.setParameter("FK_STUDY", StringUtil.stringToNum(srsk.getStudyVerStudyId()));
                    //query.setParameter("STUDYVER_NUMBER", srsk.getStudyVerNumber());//JM Blocked
                    query.setParameter("studyVerNumber", srsk.getStudyVerNumber());
                    query.setParameter("studyVerCategory", srsk.getStudyVercat());
                    ArrayList list = (ArrayList) query.getResultList();
                    if (list == null)
                        list = new ArrayList();
                    if (list.size() > 0) {                  	
                        // same version number exists
                        Rlog
                                .debug("studyVer",
                                        "StudyVerAgentBean.setStudyVerDetails : the version number is not unique"); 
                        return -3;
                    }

                } catch (Exception e) {
                    Rlog.debug("studyVer", "This Version number does not exist in database" + e);
                }
            }

            output = retrieved.updateStudyVer(srsk);

        } catch (Exception e) {
            Rlog.debug("studyVer",
                    "Error in updateStudyVer in StudyVerAgentBean" + e);
            return -2;
        }        
           
        return output;
    }

    /**
     * Gets all study versions
     * 
     * @param studyId.
     * @return StudyVerDao
     */

    public StudyVerDao getAllVers(int studyId) {
        try {
            StudyVerDao studyVerDao = new StudyVerDao();
            studyVerDao.getAllVers(studyId);
            if (studyVerDao == null) {
                Rlog
                        .debug("studyVer",
                                "In getAllVers in StudyVerAgentBean - 3");
            }
            return studyVerDao;
        } catch (Exception e) {
            Rlog.fatal("studyVer",
                    "Exception In getAllVers in studyVerAgentBean " + e);
        }
        return null;
    }

    public StudyVerDao getAllVers(int studyId, String status) {
        try {
            StudyVerDao studyVerDao = new StudyVerDao();
            studyVerDao.getAllVers(studyId, status);
            if (studyVerDao == null) {
                Rlog
                        .debug("studyVer",
                                "In getAllVers in StudyVerAgentBean - 3");
            }
            return studyVerDao;
        } catch (Exception e) {
            Rlog.fatal("studyVer",
                    "Exception In getAllVers in studyVerAgentBean " + e);
        }
        return null;
    }
 /*   
     //JM: 11/17/2005: Added: new method
    
    public StudyVerDao getAllStudyVers(int studyId,String studyVerNumber,String verCat, String verType, String verStat, String orderBy, String orderType) {
        
        try {
        	StudyVerDao studyVerDao = new StudyVerDao();
        	studyVerDao.getAllStudyVers(studyId,studyVerNumber,verCat, verType, verStat, orderBy, orderType);
        	if (studyVerDao == null) {
                Rlog
                        .debug("studyVer",
                                "In getAllStudyVers in StudyVerAgentBean - 3");
            }
        	return studyVerDao;
        } catch (Exception e) {
            Rlog.fatal("studyVer",
                    "Error in getAllStudyVers(int studyId,String studyVerNumber,String verCat, String verType, String verStat, String orderBy, String orderType) in StudyVerJB " + e);
            return null;
        }
    }

*/
    public int removeStudyVer(int studyVerId) {
        StudyVerBean retrieved = null;
        try {

            retrieved = (StudyVerBean) em.find(StudyVerBean.class, new Integer(
                    studyVerId));
            em.remove(retrieved);
            return 0;
        } catch (Exception e) {
            Rlog.fatal("studyVer", "Exception in removeStudyVerDetails()" + e);
            return -1;
        }
    }
    
    // Overloaded for INF-18183 ::: AGodara
    public int removeStudyVer(int studyVerId,Hashtable<String, String> auditInfo) {
        
    	StudyVerBean retrieved = null;
    	AuditBean audit=null;
        try {
        	String currdate =DateUtil.getCurrentDate();
        	String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); //Fetches the User ID from the Hashtable
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); //Fetches the IP Address from the Hashtable
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); //Fetches the reason for deletion from the Hashtable
            String app_module=(String)auditInfo.get(AuditUtils.APP_MODULE);// Fetches the APP_Module from the Hashtable
            String rid= AuditUtils.getRidValue("ER_STUDYVER","eres","PK_STUDYVER="+studyVerId); //Fetches the RID
            
            //POPULATE THE AUDIT BEAN 
            audit = new AuditBean("ER_STUDYVER",String.valueOf(studyVerId),rid,userID,currdate,app_module,ipAdd,reason);
        	retrieved = (StudyVerBean) em.find(StudyVerBean.class, new Integer(
                    studyVerId));
            
        	em.persist(audit); //PERSIST THE DATA IN AUDIT TABLE
            em.remove(retrieved);
            return 0;
            
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("studyVer", "removeStudyVer(int studyVerId,Hashtable<String, String> auditInfo)" + e);
            return -1;
        }
    }

    /**
     * Copy study version
     * 
     * @param studyId
     *            studyId
     * @param studyVerId
     *            study version id which has to be copied
     * @param newVerNum
     *            New version number
     * @param versionDate
     * 			  Version Date which has to be copied	           
     * @param studyVercat
     * 			  Version Category which has to be coopied
     * @param studyVertype
     * 			  Version Type which has to be copied
     * @param usr
     *            user to which copy is to be given
     * @return 1 if copy successful, -1 if unsuccessful copy
     */
/**
 * JM: 11/15/2005: Modified: String versionDate,String studyVercat, String studyVertype parameters
 */
    public int copyStudyVersion(int studyVerId, String newVerNum, String verDate,String verCat, String verType ,int usr) {
        try {
            Rlog.debug("studyVer",
                    "In copyStudyVersion in StudyVerAgentBean - 0");
            CallableStatement cstmt = null;
            Connection conn = null;
            int ret = 0;
            try {
                CommonDAO cmDao = new CommonDAO();
                conn = CommonDAO.getConnection();
                cstmt = conn
                        .prepareCall("{call PKG_STUDYSTAT.SP_COPYSTUDYVERSION(?,?,?,?,?,?,?)}");
                cstmt.setInt(1, studyVerId);
                cstmt.setString(2, newVerNum);
                cstmt.setString(3, verDate);
                cstmt.setString(4, verCat);
                cstmt.setString(5, verType);
                
                cstmt.setInt(6, usr);
                cstmt.registerOutParameter(7, java.sql.Types.INTEGER);
                cstmt.execute();
                ret = cstmt.getInt(7);
                return ret;
            } catch (Exception e) {

                Rlog.fatal("studyVer",
                        "Exception In copyStudyVersion in studyVerAgentBean "
                                + e);
                return -1;
            } finally {
                try {
                    if (cstmt != null)
                        cstmt.close();
                } catch (Exception e) {
                }
                try {
                    if (conn != null)
                        conn.close();
                } catch (Exception e) {
                }
            }
        } catch (Exception e) {

            Rlog.fatal("studyVer",
                    "Exception In copyStudyVersion in studyVerAgentBean " + e);
            return -1;
        }
    }

    /*
     * Method to find the very first matching version
     */
    public int findStudyVersion(int studyId, int versionNum, int versionCategory){
    	int ret = 0;
    	try {

			Query query = em.createNamedQuery("findStudyVerNum");
			query.setParameter("FK_STUDY", studyId);
			query.setParameter("studyVerNumber", versionNum);
			query.setParameter("studyVerCategory", versionCategory);

			ArrayList list = (ArrayList) query.getResultList();
			if (list == null)
				list = new ArrayList();
			if (list.size() > 0) {
				StudyVerBean verBean = null;
				verBean = (StudyVerBean) list.get(0);
				ret = verBean.getStudyVerId();
			}

			return ret;
    	} catch (Exception e) {
    		Rlog.fatal("studyVer","Error in findStudyVersion() in StudyVerAgentBean " + e);
    	}
    	return 0;
    }

    /**
     * Create new study version
     * 
     * @param studyId
     *            studyId
     * @param verNum
     *            version number
     * @param verNotes
     *            version notes
     * @param versionDate
     * 			  Version Date            
     * @param studyVercat
     * 			  Version Category 
     * @param studyVertype
     * 			  Version Type           
     * @param usr
     *            user Id
     * @return 1 if successful, -3 if unsuccessfull
     */
    /**
     * JM: 11/15/2005: Modified: String versionDate,String studyVercat, String studyVertype parameters
     */
    public int newStudyVersion(int studyId, String verNum, String verNotes, String versionDate, String studyVercat, String studyVertype,
            int usr) {
        try {

        	
        	CallableStatement cstmt = null;
            Connection conn = null;
            int ret = 0;
            try {
                CommonDAO cmDao = new CommonDAO();
                conn = CommonDAO.getConnection();
                cstmt = conn
                        .prepareCall("{call PKG_STUDYSTAT.SP_NEWSTUDYVERSION(?,?,?,?,?,?,?,?)}");
                cstmt.setInt(1, studyId);
                cstmt.setString(2, verNum);
                cstmt.setString(3, verNotes);
                cstmt.setString(4, versionDate);            
                cstmt.setString(5, studyVercat);
                cstmt.setString(6, studyVertype);
                cstmt.setInt(7, usr);
                cstmt.registerOutParameter(8, java.sql.Types.INTEGER);
                cstmt.execute();
                ret = cstmt.getInt(8);
                return ret;
            } catch (Exception e) {

                Rlog.fatal("studyVer",
                        "Exception In newStudyVersion in studyVerAgentBean "
                                + e);
                return -1;
            } finally {
                try {
                    if (cstmt != null)
                        cstmt.close();
                } catch (Exception e) {
                }
                try {
                    if (conn != null)
                        conn.close();
                } catch (Exception e) {
                }
            }
        } catch (Exception e) {

            Rlog.fatal("studyVer",
                    "Exception In newStudyVersion in studyVerAgentBean " + e);
            return -1;
        }
    }

}// end of class

