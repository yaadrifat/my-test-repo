/* 
 * $Id: Configuration.java,v 1.8 2012/10/05 05:18:00 agodara Exp $
 * Copyright 2000 Velos, Inc. All rights reserved.
 */

package com.velos.eres.service.util;

/**
 * This class reads the configuration parameters from xml files and stores in
 * the staic variables.when the starts up the function of this class is called
 * to setup the values.
 */

import java.io.IOException;
import java.util.Hashtable;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Configuration {

    private static Hashtable appServerParam;

    private static Hashtable classURLs;

    private static Hashtable businessLogicClass;

    public static String DBDriverName;

    public static String DBUrl;

    public static String DBUser;

    public static String DBPwd;

    public static String dsJndiName;

    public static String UPLOADFOLDER;

    public static String DOWNLOADFOLDER;

    public static String UPLOADSERVLET;

    public static String DOWNLOADSERVLET;

    public static String REPTEMPFILEPATH;

    public static String REPDWNLDPATH;

    public static String JSPPATH;

    public static String ERES_HOME;

    public static String BGTUPLOADFOLDER;

    public static String BGTDOWNLOADFOLDER;

    public static String BGTUPLOADSERVLET;

    public static String BGTDOWNLOADSERVLET;

    public static String SUPPORTEMAIL;

    public static int FAILEDACCESSCOUNT;

    // /added by salil for morebrowserrows

    public static int MOREBROWSERROWS;

    public static int ROWSPERBROWSERPAGE;

    public static int PAGEPERBROWSER;

    // for patient database

    public static String PATDBUrl;

    public static String PATDBUser;

    public static String PATDBPwd;

    public static String PATDBDriverName;
    
    private static String appDateFormat;
    
    private static String tempImagePath ;
    
    private static String tempImageDownloadPath ;
    
    private static String staticImagePath;
    
    private static String numFormatLocale;

    /**
     * 
     * 
     * This method is called when the server starts up, typically from startup
     * servelt.
     * 
     * 
     */

    public static void readSettings() {

        String fileName = null;
        String eHome = null;
        try {
            eHome = EnvUtil.getEnvVariable("ERES_HOME");
            eHome = (eHome == null) ? "" : eHome;
            System.out.println("eHome" + eHome);
            if (eHome.trim().equals("%ERES_HOME%"))
                eHome = System.getProperty("ERES_HOME");
            eHome = eHome.trim();
            ERES_HOME = eHome;
            Rlog.debug("common", "Read Settings ERES home is:" + ERES_HOME);
            fileName = eHome + "eresearch.xml";
            Rlog.debug("common", "File Name is:" + fileName);
            Document root = DOMUtil.readDocument(fileName);
            /**
             * read the application server parameters
             */
            readAppserverParam(root);

            /**
             * read the class & url parameters
             */
            readClassURLs(root);

            readDBParam(root);

            readPatientDBParam(root);

            // read the customer support email
            readCustomerSupportEmail(root);

            // read # of failed access counts
            readFailedAccessCount(root);

            // read # of more browserRows
            readMoreBrowserRows(root);

            readRowsPerBrowserPage(root);

            readTotalPagesPerBrowser(root);
            
            readAppDateFormat(root);
            
            readTempImagePath(root);
            
            readNumFormatLocale(root);

        } catch (IOException ie) {
            Rlog.fatal("common", "io ex" + ie);
        } catch (SAXException se) {
            Rlog.fatal("common", "sax ex" + se);
        } catch (Exception e) {
            Rlog.fatal("common", "e ex" + e);
            e.printStackTrace();
        }
    }

    /**
     * 
     * 
     * Reads the application server parameters. first read the vendor name then
     * based
     * 
     * 
     * on the vendor name, reads the vendor specific server parameters.
     * 
     * 
     */

    private static void readAppserverParam(Document root) {

        String name, value;

        appServerParam = new Hashtable();

        Node appServer = DOMUtil.findNode(root, "appserver");

        Node vendor = DOMUtil.findNode(appServer, "vendor");

        String venName = (vendor.getFirstChild()).getNodeValue();

        Rlog.debug("common", "Application Server Param Vendor Name" + venName);

        Node venNode = DOMUtil.findNode(appServer, venName);

        Node paramNode = null;

        NodeList nl = venNode.getChildNodes();

        int len = nl.getLength();

        for (int i = 0; i < len; i++) {

            paramNode = nl.item(i);

            if (paramNode == null || paramNode.toString().trim().equals(""))
                continue;

            // if (paramNode.toString().trim().equals("dsjndi")){

            // dsJndiName=DOMUtil.findNode(paramNode,"name").getFirstChild().getNodeValue();

            // }else

            name = DOMUtil.findNode(paramNode, "name").getFirstChild()
                    .getNodeValue();

            if (name.equals("dsjndi"))

            {

                dsJndiName = DOMUtil.findNode(paramNode, "value")
                        .getFirstChild().getNodeValue();

                Rlog.debug("common", "DAtasource Jndi Name" + dsJndiName);

            }

            else

            {

                value = DOMUtil.findNode(paramNode, "value").getFirstChild()
                        .getNodeValue();

                appServerParam.put(name, value);

            }

        }

    }

    /**
     * 
     * 
     * read the url & class parameters for all modules.
     * 
     * 
     */

    private static void readClassURLs(Document root) {

        classURLs = new Hashtable();

        businessLogicClass = new Hashtable();

        Node module = DOMUtil.findNode(root, "modules");

        NodeList nl = module.getChildNodes();

        NamedNodeMap attr;

        findURLNode(nl);

    }

    private static void findURLNode(NodeList nl) {

        String name = null, value = null, logicClass = null;

        Node paramNode = null;

        Node attr = null;

        int len = nl.getLength();

        for (int i = 0; i < len; i++) {

            paramNode = nl.item(i);

            if (paramNode == null || paramNode.toString().trim().equals(""))
                continue;

            // get the url tag and corresponding class and logic class values

            if ((paramNode.getNodeName().equals("url"))) {

                name = paramNode.getFirstChild().getNodeValue();

                try {

                    attr = (paramNode.getAttributes()).getNamedItem("class");

                    if (attr != null)
                        value = attr.getNodeValue();

                } catch (Exception e) {

                }

                try {

                    attr = (paramNode.getAttributes()).getNamedItem("logic");

                    if (attr != null)
                        logicClass = attr.getNodeValue();

                } catch (Exception e) {

                }

                // store all values in hash table

                classURLs.put(name, value);

                businessLogicClass.put(name, logicClass);

            } else {

                findURLNode(paramNode.getChildNodes());

            }

        }

        return;

    }

    /**
     * 
     * 
     * reads database connection parameters.
     * 
     * 
     */

    private static void readDBParam(Document root) {

        Node dbNode = null;

        Node CurrentBDNode = null;
        dbNode = DOMUtil.findNode(root, "DBURL");
        CurrentBDNode = DOMUtil.findNode(dbNode, "DB");

        DBDriverName = DOMUtil.findNode(CurrentBDNode, "DRIVER")
                .getFirstChild().getNodeValue();

        Rlog.debug("common", "Current DB Node " + CurrentBDNode
                + "DBDriverName " + DBDriverName);

        DBUrl = DOMUtil.findNode(CurrentBDNode, "DB_URL").getFirstChild()
                .getNodeValue();

        DBUser = DOMUtil.findNode(CurrentBDNode, "USERID").getFirstChild()
                .getNodeValue();

        DBPwd = DOMUtil.findNode(CurrentBDNode, "PWD").getFirstChild()
                .getNodeValue();
        Rlog.debug("common", "DBUrl " + DBUrl + " DBUser " + DBUser + " DBPwd "
                + DBPwd);
    }

    public static void readAppendixParam(String file) {

        try {

            Document root = DOMUtil.readDocument(file);

            Node module = DOMUtil.findNode(root, "modules");

            Node appendix = DOMUtil.findNode(module, "appendix");

            Node fileupload = DOMUtil.findNode(appendix, "fileupload");

            Node name = DOMUtil.findNode(fileupload, "name");

            String name_val = (name.getFirstChild()).getNodeValue();

            Rlog.debug("common", "name:" + name_val);

            if (name_val.equals("folder"))

            {

                UPLOADFOLDER = DOMUtil.findNode(fileupload, "value")
                        .getFirstChild().getNodeValue();

                Rlog.debug("common", "value" + UPLOADFOLDER);

            }

            Node us = DOMUtil.findNode(fileupload, "servlet");

            UPLOADSERVLET = (us.getFirstChild()).getNodeValue();

            Rlog.debug("common", "us:" + UPLOADSERVLET);

            Node filedownload = DOMUtil.findNode(appendix, "filedownload");

            Node name1 = DOMUtil.findNode(filedownload, "name");

            String name1_val = (name1.getFirstChild()).getNodeValue();

            if (name1_val.equals("folder"))

            {

                DOWNLOADFOLDER = DOMUtil.findNode(filedownload, "value")
                        .getFirstChild().getNodeValue();

                Rlog.debug("common", "value1" + DOWNLOADFOLDER);

            }

            Node ds = DOMUtil.findNode(filedownload, "servlet");

            DOWNLOADSERVLET = (ds.getFirstChild()).getNodeValue();

            Rlog.debug("common", "ds:" + DOWNLOADSERVLET);

            Node jsppath = DOMUtil.findNode(appendix, "jsppath");

            JSPPATH = (jsppath.getFirstChild()).getNodeValue();

            Rlog.debug("common", "jsp path=" + JSPPATH);

        }

        catch (IOException ie) {

            Rlog.fatal("common", "io ex" + ie);

        } catch (SAXException se) {

            Rlog.fatal("common", "sax ex" + se);

        }

    }

    public static void readReportParam(String file) {

        try {

            Document root = DOMUtil.readDocument(file);

            Node module = DOMUtil.findNode(root, "modules");

            Node report = DOMUtil.findNode(module, "report");

            Node tempfilepath = DOMUtil.findNode(report, "tempfilepath");
            String tempfilepath_val = (tempfilepath.getFirstChild())
                    .getNodeValue();
            Rlog.debug("common", "tempfilepath_val:" + tempfilepath_val);
            REPTEMPFILEPATH = tempfilepath_val;

            Node dwnldpath = DOMUtil.findNode(report, "dwnldpath");
            String dwnldpath_val = (dwnldpath.getFirstChild()).getNodeValue();
            Rlog.debug("common", "dwnldpath_val:" + dwnldpath_val);
            REPDWNLDPATH = dwnldpath_val;
        }

        catch (IOException ie) {

            Rlog.fatal("common", "io ex" + ie);

        } catch (SAXException se) {

            Rlog.fatal("common", "sax ex" + se);

        }

    }

    public String getVelosAdminId(String file) {

        try {

            Document root = DOMUtil.readDocument(file);

            Node applicationDefault = DOMUtil.findNode(root,
                    "applicationDefaults");

            Node velosAdminId = DOMUtil.findNode(applicationDefault,
                    "velosAdminId");

            return (velosAdminId.getFirstChild()).getNodeValue();

        } catch (Exception ie) {

            Rlog.fatal("common", "Configuration.getVelosAdminId" + ie);

            return null;

        }
    }

    public String getAdminTimeout(String file) {

        try {

            Document root = DOMUtil.readDocument(file);

            Node applicationDefault = DOMUtil.findNode(root,
                    "applicationDefaults");

            Node velosAdminId = DOMUtil.findNode(applicationDefault,
                    "sessiontimeout");

            return (velosAdminId.getFirstChild()).getNodeValue();

        } catch (Exception ie) {

            Rlog.fatal("common", "Configuration.getVelosAdminId" + ie);

            return null;

        }
    }

    public String getEditStudyCount(String file) {

        try {

            Document root = DOMUtil.readDocument(file);
            Node module = DOMUtil.findNode(root, "modules");
            Node studyModule = DOMUtil.findNode(module, "study");
            Node editCount = DOMUtil.findNode(studyModule, "editcount");
            return (editCount.getFirstChild()).getNodeValue();

        } catch (Exception ie) {

            Rlog.fatal("common", "Configuration.getVelosAdminId" + ie);
            return null;
        }
    }

    public String getViewStudyCount(String file) {

        try {

            Document root = DOMUtil.readDocument(file);
            Node module = DOMUtil.findNode(root, "modules");
            Node studyModule = DOMUtil.findNode(module, "study");
            Node viewCount = DOMUtil.findNode(studyModule, "viewcount");
            return (viewCount.getFirstChild()).getNodeValue();

        } catch (Exception ie) {

            Rlog.fatal("common", "Configuration.getVelosAdminId" + ie);
            return null;
        }
    }

    public String getVelosAdminAccountId(String file) {

        try {

            Document root = DOMUtil.readDocument(file);

            Node applicationDefault = DOMUtil.findNode(root,
                    "applicationDefaults");

            Node velosAdminAccountId = DOMUtil.findNode(applicationDefault,
                    "velosAdminAccountId");

            return (velosAdminAccountId.getFirstChild()).getNodeValue();

        } catch (Exception ie) {

            Rlog.fatal("common", "Configuration.getVelosAdminAccountId" + ie);

            return null;

        }

    }

    /* This function will read the version no from the xml and returns the value */

    public String getAppVersion(String file) {

        try {

            Document root = DOMUtil.readDocument(file);

            Node applicationDefault = DOMUtil.findNode(root,
                    "applicationDefaults");

            Node velosVersion = DOMUtil.findNode(applicationDefault,
                    "appVersion");

            return (velosVersion.getFirstChild()).getNodeValue();

        } catch (Exception ie) {

            Rlog.fatal("common", "Configuration.getAppVersion" + ie);

            return null;

        }

    }

    public String getDefaultGroup(String file) {

        try {

            Document root = DOMUtil.readDocument(file);

            Node applicationDefault = DOMUtil.findNode(root,
                    "applicationDefaults");

            Node defaultGroup = DOMUtil.findNode(applicationDefault,
                    "defaultGroup");

            return (defaultGroup.getFirstChild()).getNodeValue();

        } catch (Exception e) {

            Rlog.fatal("common", "Configuration.getDefaultGroup" + e);

            return null;

        }

    }

    // Get the description of the Default user :Admin

    public String getDefaultDesc(String file) {

        try {

            Document root = DOMUtil.readDocument(file);

            Node applicationDefault = DOMUtil.findNode(root,
                    "applicationDefaults");

            Node defaultDesc = DOMUtil.findNode(applicationDefault,
                    "defaultDesc");

            return (defaultDesc.getFirstChild()).getNodeValue();

        } catch (Exception e) {

            Rlog.fatal("common", "Configuration.getDefaultDesc" + e);

            return null;

        }

    }

    // get # of rows to be displayed in browser pages with pagination
    public int getRowsPerBrowserPage(String file) {

        try {
            Document root = DOMUtil.readDocument(file);
            Node applicationDefault = DOMUtil.findNode(root,
                    "applicationDefaults");
            Node rowsPerBrowserPage = DOMUtil.findNode(applicationDefault,
                    "rowsPerBrowserPage");
            return StringUtil.stringToNum((rowsPerBrowserPage.getFirstChild())
                    .getNodeValue());
        } catch (Exception e) {
            Rlog.fatal("common", "Configuration.getRowsPerBrowserPage" + e);
            return 0;
        }
    }

    // get # of pages to be displayed in one shot in a browser with pagination
    public int getTotalPagesPerBrowser(String file) {

        try {
            Document root = DOMUtil.readDocument(file);
            Node applicationDefault = DOMUtil.findNode(root,
                    "applicationDefaults");
            Node totalPagesPerBrowser = DOMUtil.findNode(applicationDefault,
                    "totalPagesPerBrowser");
            return StringUtil.stringToNum((totalPagesPerBrowser.getFirstChild())
                    .getNodeValue());
        } catch (Exception e) {
            Rlog.fatal("common", "Configuration.getTotalPagesPerBrowser" + e);
            return 0;
        }
    }

    // get # of failed access counts after which the user would be locked
    public static void readFailedAccessCount(Document root) {

        try {
            Node applicationDefault = DOMUtil.findNode(root,
                    "applicationDefaults");
            Node failedAccessCount = DOMUtil.findNode(applicationDefault,
                    "failedAccessCount");
            FAILEDACCESSCOUNT = StringUtil.stringToNum((failedAccessCount
                    .getFirstChild()).getNodeValue());
        } catch (Exception e) {
            Rlog.fatal("common", "Configuration.getFailedAccessCount" + e);
        }
    }

    // get email of customer support
    public static void readCustomerSupportEmail(Document root) {

        try {
            Node applicationDefault = DOMUtil.findNode(root,
                    "applicationDefaults");
            Node customerSupportEmail = DOMUtil.findNode(applicationDefault,
                    "customerSupport");
            SUPPORTEMAIL = (customerSupportEmail.getFirstChild())
                    .getNodeValue();
        } catch (Exception e) {
            Rlog.fatal("common", "Configuration.getCustomerSupportEmail" + e);
        }
    }

    /**
     * 
     * 
     * reurns the application server parameter hash table
     * 
     * 
     */

    public static Hashtable getServerParam() {

        return appServerParam;

    }

    /**
     * 
     * 
     * reurns the url & class parameter hash table
     * 
     * 
     */

    public static Hashtable getClassURLs() {

        return classURLs;

    }

    public static Hashtable getLogicClass() {

        return businessLogicClass;

    }

    // to read patient db parameters

    private static void readPatientDBParam(Document root) {

        Node dbNode = null;

        Node CurrentBDNode = null;

        dbNode = DOMUtil.findNode(root, "PDBURL");

        CurrentBDNode = DOMUtil.findNode(dbNode, "PDB");

        Rlog.debug("common", "Current PDB Node" + CurrentBDNode);

        PATDBDriverName = DOMUtil.findNode(CurrentBDNode, "PDRIVER")
                .getFirstChild().getNodeValue();

        Rlog.debug("common", "PDBDriverName" + PATDBDriverName);

        PATDBUrl = DOMUtil.findNode(CurrentBDNode, "PDB_URL").getFirstChild()
                .getNodeValue();

        Rlog.debug("common", "PATPDBUrl" + PATDBUrl);

        PATDBUser = DOMUtil.findNode(CurrentBDNode, "PUSERID").getFirstChild()
                .getNodeValue();

        Rlog.debug("common", "PATDBUser" + PATDBUser);

        PATDBPwd = DOMUtil.findNode(CurrentBDNode, "PPWD").getFirstChild()
                .getNodeValue();

        Rlog.debug("common", "PATDBPwd" + PATDBPwd);

    }

    // /

    // read budget parameters

    public static void readBudAppendixParam(String file) {

        try {

            Document root = DOMUtil.readDocument(file);

            Node module = DOMUtil.findNode(root, "modules");

            Node appendix = DOMUtil.findNode(module, "bgtapndx");

            Node fileupload = DOMUtil.findNode(appendix, "fileupload");

            Node name = DOMUtil.findNode(fileupload, "name");

            String name_val = (name.getFirstChild()).getNodeValue();

            Rlog.debug("common", "name:" + name_val);

            if (name_val.equals("folder"))

            {

                BGTUPLOADFOLDER = DOMUtil.findNode(fileupload, "value")
                        .getFirstChild().getNodeValue();

                Rlog.debug("common", "value" + BGTUPLOADFOLDER);

            }

            Node us = DOMUtil.findNode(fileupload, "servlet");

            BGTUPLOADSERVLET = (us.getFirstChild()).getNodeValue();

            Rlog.debug("common", "us:" + BGTUPLOADSERVLET);

            Node filedownload = DOMUtil.findNode(appendix, "filedownload");

            Node name1 = DOMUtil.findNode(filedownload, "name");

            String name1_val = (name1.getFirstChild()).getNodeValue();

            Rlog.debug("common", "name1:" + name1_val);

            if (name1_val.equals("folder")) {

                BGTDOWNLOADFOLDER = DOMUtil.findNode(filedownload, "value")
                        .getFirstChild().getNodeValue();
                Rlog.debug("common", "value1" + BGTDOWNLOADFOLDER);

            }

            Node ds = DOMUtil.findNode(filedownload, "servlet");
            BGTDOWNLOADSERVLET = (ds.getFirstChild()).getNodeValue();

            Rlog.debug("common", "ds:" + BGTDOWNLOADSERVLET);

            Rlog.debug("common", "before getting jsppath");
            Node jsppath = DOMUtil.findNode(appendix, "jsppath");
            JSPPATH = (jsppath.getFirstChild()).getNodeValue();
            Rlog.debug("common", "jsp path=" + JSPPATH);

        }

        catch (IOException ie) {
            Rlog.fatal("common", "io ex" + ie);
        } catch (SAXException se) {
            Rlog.fatal("common", "sax ex" + se);

        }

    }

    // ///////////////
    public int getFieldNumInFrmBrowser(String file) {

        try {

            Document root = DOMUtil.readDocument(file);

            Node applicationDefault = DOMUtil.findNode(root,
                    "applicationDefaults");

            Node fieldNumInFrmBrowser = DOMUtil.findNode(applicationDefault,
                    "fieldNumInFrmBrowser");

            return StringUtil.stringToNum((fieldNumInFrmBrowser.getFirstChild())
                    .getNodeValue());

        } catch (Exception ie) {

            Rlog.fatal("common", "Configuration.getFieldNumInFrmBrowser" + ie);

            return -1;

        }
    }

    // //////////////////////////////

    public int getFldRespCount(String file) {

        try {

            Document root = DOMUtil.readDocument(file);

            Node applicationDefault = DOMUtil.findNode(root,
                    "applicationDefaults");

            Node fldRespCount = DOMUtil.findNode(applicationDefault,
                    "fldRespCount");

            return StringUtil.stringToNum((fldRespCount.getFirstChild())
                    .getNodeValue());

        } catch (Exception ie) {

            Rlog.fatal("common", "Configuration.getFldRespCount" + ie);

            return -1;

        }
    }

    //

    // by salil
    // get # of more browser rows
    public static void readMoreBrowserRows(Document root) {

        try {
            Node applicationDefault = DOMUtil.findNode(root,
                    "applicationDefaults");
            Node moreBrowserRows = DOMUtil.findNode(applicationDefault,
                    "moreBrowserRows");
            MOREBROWSERROWS = StringUtil.stringToNum((moreBrowserRows
                    .getFirstChild()).getNodeValue());
        } catch (Exception e) {
            Rlog.fatal("common", "Configuration.readMoreBrowserRows" + e);
        }
    }

    // /end by salil

    // by sonia kaura
    // get number of browser rows
    public static void readRowsPerBrowserPage(Document root) {

        try {
            Node applicationDefault = DOMUtil.findNode(root,
                    "applicationDefaults");
            Node rowsPerBrowserPage = DOMUtil.findNode(applicationDefault,
                    "rowsPerBrowserPage");
            ROWSPERBROWSERPAGE = StringUtil.stringToNum((rowsPerBrowserPage
                    .getFirstChild()).getNodeValue());
        } catch (Exception e) {
            Rlog.fatal("common", "Configuration.readRowsPerBrowserPage" + e);
        }
    } // end of the browser rows

    // by sonia kaura
    // get pages per browser
    public static void readTotalPagesPerBrowser(Document root) {

        try {
            Node applicationDefault = DOMUtil.findNode(root,
                    "applicationDefaults");
            Node totalPagesPerBrowser = DOMUtil.findNode(applicationDefault,
                    "totalPagesPerBrowser");
            PAGEPERBROWSER = StringUtil.stringToNum((totalPagesPerBrowser
                    .getFirstChild()).getNodeValue());
        } catch (Exception e) {
            Rlog.fatal("common", "Configuration.readTotalPagesPerBrowser" + e);
        }
    }

    /**
     * XML document root
     * sets the 'date format' setting for the application
     * */
    public static void readAppDateFormat(Document root) {

        try {
            Node applicationDefault = DOMUtil.findNode(root,
                    "applicationDefaults");
            Node appDateFormatN = DOMUtil.findNode(applicationDefault,
                    "appDateFormat");
            appDateFormat = (appDateFormatN.getFirstChild()).getNodeValue();
            
        } catch (Exception e) {
            Rlog.fatal("common", "Configuration.readRowsPerBrowserPage" + e);
            appDateFormat = "MM/dd/yyyy";
        }
        
        if ( StringUtil.isEmpty(appDateFormat ))
        {
        	
        	appDateFormat = "MM/dd/yyyy";
        }
        
    } // end of the browser rows

    //gets the application date format
    
	public static String getAppDateFormat() {
		if (StringUtil.isEmpty(appDateFormat))
		{
			readSettings();
		}
		return appDateFormat;
	}

	public static void setAppDateFormat(String appDateFormat) {
		Configuration.appDateFormat = appDateFormat;
	}

	
    /**
     * XML document root
     * gets the 'temporary image folder' setting for the application
     * */
    public static void readTempImagePath(Document root) {

        try {
            Node applicationDefault = DOMUtil.findNode(root,
                    "applicationDefaults");
            
            Node nPath = DOMUtil.findNode(applicationDefault,
                    "tempImagePath");
            
            tempImagePath = (nPath.getFirstChild()).getNodeValue();
            
            
            Node nDownPath = DOMUtil.findNode(applicationDefault,
            "tempImagePathDownload");
    
            
            tempImageDownloadPath= (nDownPath.getFirstChild()).getNodeValue();
            

            Node sImagePath = DOMUtil.findNode(applicationDefault,
            "staticImagePath");
            
            staticImagePath = (sImagePath.getFirstChild()).getNodeValue();
            
        } catch (Exception e) {
            Rlog.fatal("common", "Configuration.readTempImagePath" + e);
             
        }
        
         
    } 
    
    public static String getTempImagePath() {
		if (StringUtil.isEmpty(tempImagePath))
		{
			readSettings();
		}
		return tempImagePath;
	}
    
    public static String getTempImageDownloadPath() {
		if (StringUtil.isEmpty(tempImageDownloadPath))
		{
			readSettings();
		}
		return tempImageDownloadPath;
	}
    
    public static String getStaticImagePath() {
        if (StringUtil.isEmpty(staticImagePath))
        {
            readSettings();
        }
        return staticImagePath;
    }
    
        
    /**
     * XML document root
     * sets the 'number formatting' setting for the application
     * */
    public static void readNumFormatLocale(Document root) {

        try {
            Node applicationDefault = DOMUtil.findNode(root,
                    "applicationDefaults");
            Node numFormatLocaleN = DOMUtil.findNode(applicationDefault,
                    "numFormatLocale");
            numFormatLocale = (numFormatLocaleN.getFirstChild()).getNodeValue();
            
        } catch (Exception e) {
            Rlog.fatal("common", "Configuration.readRowsPerBrowserPage" + e);
            numFormatLocale = "en-US";
        }
        
        if ( StringUtil.isEmpty(numFormatLocale ))
        {
        	
        	numFormatLocale = "en-US";
        }
        
    } 

    //gets the application number format locale
    
	public static String getNumFormatLocale() {
		if (StringUtil.isEmpty(numFormatLocale))
		{
			readSettings();
		}
		return numFormatLocale;
	}

	public static void setNumFormatLocale(String numFormatLocale) {
		Configuration.numFormatLocale = numFormatLocale;
	}


    
}