/*
 * Classname			ReportAgentBean.class
 * 
 * Version information
 *
 * Date					06/01/2001
 * 
 * Copyright notice Aithent
 */

package com.velos.eres.service.reportAgent.impl;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.common.ReportDaoNew;
import com.velos.eres.service.reportAgent.ReportAgentRObj;
import com.velos.eres.service.util.Rlog;

/**
 * The stateless session EJB acting as a facade for the entity CMP.<br>
 * <br>
 * 
 * @author
 */
@Stateless
public class ReportAgentBean implements ReportAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    // method implementation as specified in interface
    public ReportDaoNew getRepXml(int repId, int accId, String args) {
        try {
            Rlog.debug("Reports",
                    "In getRepXML in ReportAgentBean line number 0");
            ReportDaoNew rD = new ReportDaoNew();
            rD.getXMLFromSProc(repId, accId, args);
            return rD;
        } catch (Exception e) {
            Rlog.fatal("Reports", "Exception In getRepXML in ReportAgentBean "
                    + e);
            return null;
        }
    }

    public ReportDaoNew getRepXsl(int xslId) {
        try {
            Rlog.debug("Reports",
                    "In getRepXsl in ReportAgentBean line number 0");
            ReportDaoNew rD = new ReportDaoNew();
            rD.getRepXsl(xslId);
            return rD;
        } catch (Exception e) {
            Rlog.fatal("Reports", "Exception In getRepXsL in ReportAgentBean "
                    + e);
            return null;
        }
    }

    public ReportDaoNew getRepXslList(int repId) {
        try {
            Rlog.debug("Reports",
                    "In getRepXslList in ReportAgentBean line number 0");
            ReportDaoNew rD = new ReportDaoNew();
            rD.getRepXslList(repId);
            return rD;
        } catch (Exception e) {
            Rlog.fatal("Reports",
                    "Exception In getRepXsLlist in ReportAgentBean " + e);
            return null;
        }

    }

    // get a list of all reports

    public ReportDaoNew getAllRep(int accId) {
        try {
            Rlog.debug("Reports",
                    "In getAllRep in ReportAgentBean line number 0");
            ReportDaoNew rD = new ReportDaoNew();
            Rlog.debug("Reports",
                    "In getAllRep in ReportAgentBean line number 1");
            rD.getAllReports(accId);
            Rlog.debug("Reports",
                    "In getAllRep in ReportAgentBean line number 2");
            return rD;
        } catch (Exception e) {
            Rlog.fatal("Reports", "Exception In getAllRep in ReportAgentBean "
                    + e);
            return null;
        }

    } // end of getAllRep

    // get a specific report
    public ReportDaoNew getReport(int accountId, int reportId) {
        try {
            ReportDaoNew rD = new ReportDaoNew();
            rD.getReport(accountId, reportId);
            return rD;
        } catch (Exception e) {
            Rlog.fatal("Reports", "Exception In getReport in ReportAgentBean "
                    + e);
            return null;
        }

    } // end of getReport

    public ReportDaoNew getFilterColumns(String repCat) {
        try {
            ReportDaoNew rD = new ReportDaoNew();
            rD.getFilterColumns(repCat);
            return rD;
        } catch (Exception e) {
            Rlog.fatal("Reports",
                    "Exception In getFilterColumns in ReportAgentBean " + e);
            e.printStackTrace();
            return null;
        }

    }

    public ReportDaoNew getRepHdFtr(int repId, int accId, int userId) {
        try {
            Rlog.debug("Reports",
                    "In getRepHdFtr in ReportAgentBean line number 0");
            ReportDaoNew rD = new ReportDaoNew();
            Rlog.debug("Reports",
                    "In getRepHdFtr in ReportAgentBean line number 1");
            int i = rD.getRepHdFtr(repId, accId, userId);
            Rlog.debug("Reports",
                    "In getRepHdFtr in ReportAgentBean line number 2");
            return rD;
        } catch (Exception e) {
            Rlog.fatal("Reports",
                    "Exception In getAllHdFtr in ReportAgentBean " + e);
            return null;
        }
    }

    /**
     * Get Milestone Xml
     * 
     * @param reportType
     *            F(Financial report), S(Study wise report), P(Patient wise
     *            report)
     */

    public ReportDaoNew getRepMileXml(String reportType, int studyId,
            int patId, String startDate, String endDate, String intervalType,
            int orgId) {
        try {
            Rlog.debug("Reports",
                    "In getRepMileXml in ReportAgentBean line number 0");
            ReportDaoNew rD = new ReportDaoNew();

            if (reportType.equals("F")) {
                rD.getMileXmlFromFinSProc(studyId, orgId, startDate, endDate,
                        intervalType);
            } else if (reportType.equals("S")) {
                rD.getMileXmlFromStudySProc(studyId, orgId, startDate, endDate);
            } else if (reportType.equals("P")) {
                rD.getMileXmlFromPatSProc(studyId, patId, startDate, endDate,
                        orgId);
            } else if (reportType.equals("R")) {
                rD.getMileXmlFromFinPaymentSProc(studyId, startDate, endDate,
                        intervalType);
            } else if (reportType.equals("FR")) { // forecast
                rD.getMileXmlFromForecastSProc(studyId, startDate, endDate,
                        intervalType);
            }

            return rD;
        } catch (Exception e) {
            Rlog.fatal("Reports",
                    "Exception In getRepMileXml in ReportAgentBean " + e);
            e.printStackTrace();
            return null;
        }
    }

}// end of class

