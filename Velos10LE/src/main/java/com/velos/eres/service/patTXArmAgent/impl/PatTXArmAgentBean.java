/**
 * Classname : PatTXArmAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 05/02/2005
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.service.patTXArmAgent.impl;

/* Import Statements */

import java.util.Hashtable;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.business.audit.impl.AuditBean;

import com.velos.eres.business.common.PatTXArmDao;
import com.velos.eres.business.patTXArm.impl.PatTXArmBean;
import com.velos.eres.service.patTXArmAgent.PatTXArmAgentRObj;
import com.velos.eres.service.util.Rlog;
import javax.ejb.SessionContext;
import javax.annotation.Resource;

/* End of Import Statements */

/**
 * The stateless session EJB acting as a facade for the entity CMP -
 * PatTXArmAgentBean.<br>
 * <br>
 * 
 * @author Anu Khanna
 * @see PatTXArmAgentBean
 * @ejbHome PatTXArmAgentHome
 * @ejbRemote PatTXArmAgentRObj
 */
@Stateless
public class PatTXArmAgentBean implements PatTXArmAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    @Resource private SessionContext context;

    /*
     * * Looks up on the id parameter passed in and constructs and returns a
     * PatTXArm state keeper. containing all the details of the PatTXArm <br>
     * 
     * @param PatTXArm Id
     */

    public PatTXArmBean getPatTXArmDetails(int patTXArmId) {

        try {
            return (PatTXArmBean) em.find(PatTXArmBean.class, new Integer(
                    patTXArmId));

        } catch (Exception e) {
            Rlog.fatal("patTXArm",
                    "Exception in PatTXArmStateKeeper() in PatTXArmAgentBean"
                            + e);
            return null;
        }

    }

    /**
     * Creates PatTXArm record.
     * 
     * @param a
     *            State Keeper containing the patTXArmId attributes to be set.
     * @return int for successful:0; e\Exception : -2
     */

    public int setPatTXArmDetails(PatTXArmBean ptk) {
        try {
            em.merge(ptk);
            return ptk.getPatTXArmId();
        } catch (Exception e) {
            Rlog.fatal("patTXArm",
                    "Error in setPatTXArmDetails() in PatTXArmAgentBean " + e);
            return -2;
        }

    }

    /**
     * Updates a PatTXArm record.
     * 
     * @param a
     *            PatTXArm state keeper containing PatTXArm attributes to be
     *            set.
     * @return int for successful:0 ; e\Exception : -2
     */

    public int updatePatTXArm(PatTXArmBean ptk) {

        PatTXArmBean retrieved = null; // Entity Bean Remote Object
        int output;

        try {

            retrieved = em.find(PatTXArmBean.class, ptk.getPatTXArmId());
            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updatePatTXArm(ptk);

        } catch (Exception e) {
            Rlog.fatal("PatTXArm",
                    "Error in updatePatTXArm in PatTXArmAgentBean" + e);
            return -2;
        }
        return output;
    }

    /**
     * Removes a PatTXArm record.
     * 
     * @param a
     *            PatTXArm Primary Key
     * @return int for successful:0; e\Exception : -2
     */
    
    // Overloaded for INF-18183 ::: Akshi
    public int removePatTXArm(int patTXArmId,Hashtable<String, String> args) {
        PatTXArmBean patTXArmRObj = null;

        try {

        	
        	 AuditBean audit=null;
             String currdate =DateUtil.getCurrentDate();
             String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
             String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
             String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
             String appmodule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the application module name from the Hashtable*/
             String getRidValue= AuditUtils.getRidValue("ER_PATTXARM","eres","PK_PATTXARM="+patTXArmId);/*Fetches the RID/PK_VALUE*/ 
             audit = new AuditBean("ER_PATTXARM",String.valueOf(patTXArmId),getRidValue,
         			userID,currdate,appmodule,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
         	em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
        	
        	
        	
            patTXArmRObj = (PatTXArmBean) em.find(PatTXArmBean.class,
                    new Integer(patTXArmId));
            em.remove(patTXArmRObj);
            return 0;
        } catch (Exception e) {
        	
        	context.setRollbackOnly();
            Rlog.fatal("PatTXArm", "Exception in removePatTXArm" + e);
            return -1;

        }

    }
    
    public int removePatTXArm(int patTXArmId) {
         PatTXArmBean patTXArmRObj = null;

         try {
        	         	
         	          	
             patTXArmRObj = (PatTXArmBean) em.find(PatTXArmBean.class,
                     new Integer(patTXArmId));
             em.remove(patTXArmRObj);
             return 0;
         } catch (Exception e) {
         	         	
             Rlog.fatal("PatTXArm", "Exception in removePatTXArm" + e);
             return -1;

         }

     }

    public PatTXArmDao getPatTrtmtArms(int patProtId) {

        try {

            PatTXArmDao patTXArmDao = new PatTXArmDao();
            patTXArmDao.getPatTrtmtArms(patProtId);
            return patTXArmDao;
        } catch (Exception e) {
            Rlog.fatal("patTXArm",
                    "Exception In getPatTrtmtArms in PatTXArmAgentBean " + e);
        }
        return null;
    }
}
