/*
 * Classname			StudyStatusAgentBean.class
 * 
 * Version information
 *
 * Date					02/26/2001
 * 
 * Copyright notice
 */

package com.velos.eres.service.studyStatusAgent.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.StringTokenizer;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import oracle.sql.BLOB;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.appendix.impl.AppendixBean;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.codelst.impl.CodelstBean;
import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.EIRBDao;
import com.velos.eres.business.common.StudyApndxDao;
import com.velos.eres.business.common.StudyStatusDao;
import com.velos.eres.business.common.StudyVerDao;
import com.velos.eres.business.study.impl.StudyBean;
import com.velos.eres.business.studyStatus.impl.StudyStatusBean;
import com.velos.eres.business.studyVer.impl.StudyVerBean;
import com.velos.eres.business.submission.impl.ReviewBoardBean;
import com.velos.eres.business.submission.impl.SubmissionBoardBean;
import com.velos.eres.business.submission.impl.SubmissionStatusBean;
import com.velos.eres.compliance.web.ComplianceJB;
import com.velos.eres.service.appendixAgent.AppendixAgentRObj;
import com.velos.eres.service.studyStatusAgent.StudyStatusAgentRObj;
import com.velos.eres.service.studyVerAgent.StudyVerAgentRObj;
import com.velos.eres.service.submissionAgent.SubmissionAgent;
import com.velos.eres.service.submissionAgent.SubmissionBoardAgent;
import com.velos.eres.service.submissionAgent.SubmissionStatusAgent;
import com.velos.eres.service.util.CFG;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.statusHistory.StatusHistoryJB;
import com.velos.eres.web.studyVer.StudyVerJB;
import com.velos.eres.widget.business.common.UIFlxPageDao;

/**
 * The stateless session EJB StudyStatusAgentBean acting as a facade for the
 * StudyStatus entity CMP.
 * 
 * @author sonia sahni
 */

@Stateless
public class StudyStatusAgentBean implements StudyStatusAgentRObj {
	
	static final HashMap<String , String> revBoardSubTypeMapping = new HashMap<String , String>() {{
         put("crcReturn",    "unlock_crc");  
         put("irb_add_info" , "unlock_irb");
         put("medAncRevPend", "unlock_med");
         put("medAncRevCmp", "unlock_medcmp");
         
         put("crcInRev", "crc_in_rev");
         put("crcApproved", "crc_approved");
         put("crcDisapproved", "crc_disapproved");
         put("crcTempHold", "crc_temp_hold");
         put("crcSubmitted", "crc_submd");
         put("crcResubmitted", "crc_resubmd");
         
         put("irb_in_rev", "irb_in_rev_subm");         
         put("irb_human", "irb_human_subm");
         put("irb_not_human", "irbnothum_subm");         
         put("irb_defer", "irb_defer_subm");
         put("irb_terminated", "irb_termin_subm");
         put("app_CHR", "approved");         
         put("rej_CHR", "rejected");
         put("irb_withdrawn", "withdrawn");        
    }};
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    @Resource private SessionContext context;
    private static final String VERSION_STATUS = "versionStatus";
    private static final String LETTER_F = "F";
    private static final String ER_STUDYVER_TABLE = "er_studyver";

    public StudyStatusBean getStudyStatusDetails(int id) {
        StudyStatusBean retrieved = null; // 

        try {
            Rlog.debug("studystatus", "IN TRY OF SESSION BEAN");

            retrieved = (StudyStatusBean) em.find(StudyStatusBean.class,
                    new Integer(id));

        } catch (Exception e) {
            // e.printStackTrace();
            Rlog.fatal("studystatus", "EXCEPTION IN GETTING STUDY STATUS" + e);
        }

        return retrieved;
    }

    /**
     * Calls setStudyStatusDetails() on StudyStatus Entity Bean. Creates a new
     * status with the values in the StateKeeper object.
     * 
     * @param ssk
     *            a StudyStatusStateKeeper object containing Study Status
     *            attributes.
     */
    /*
     * Modified by Sonia, 08/14/04, added logic to manage end dates of study
     * status records, added javadoc
     */
    /*
     * Modified by Sonia, 08/30/04 added logic to find the latest status, and
     * set its end date to null if its not null, this way we should have atleast
     * one cuurrent status
     */
    /*
     * Modified by Dylan, 12/14/10 added a new query for old study statuses that 
     * checks for a study status with current flag of one when the provided StudyStatusBean
     * has a current flag of one (since we should only have 1 current status.)
     * 
     * Also, changed signature to return primary key of new status. Returns -1 on failure.
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public int setStudyStatusDetails(StudyStatusBean ssk) {    	
    	int newPk = 0;
    	int studyId=0;
  	  	int irbApprovedCount=0;
  	  	int amendCount=0;  	    
  	  
    	HashMap<String, Object> paramMap = new HashMap<String, Object>(); 
    	String dateFormat = Configuration.getAppDateFormat();
        if (dateFormat.indexOf("MMM") != -1)
        	dateFormat = StringUtil.replace(dateFormat,"MMM","Mon");
    	try {

            //get the last isCurrent Status and update it to 0
            String newStatIsCurrentFlag = ssk.getCurrentStat();
            
            if (StringUtil.isEmpty(newStatIsCurrentFlag))
            {
            	newStatIsCurrentFlag="0";
            }

            if ( newStatIsCurrentFlag.equals("1") )
            {

            	Query query = em.createNamedQuery("findByIsCurrentFlag");

            	query.setParameter("FK_STUDY", ssk.getStatStudy());
            	query.setParameter("IS_CURRENT_FLAG", ssk.getCurrentStat());

            	ArrayList<StudyStatusBean> list = (ArrayList<StudyStatusBean>) query.getResultList();
            	if (list == null)
            		list = new ArrayList<StudyStatusBean>();

            	if (list.size() > 0)
            	{
            		//get the bean

            		StudyStatusBean oldIsCurrentFlagStat = (StudyStatusBean) list.get(0);

            		//set the flag to 0
            		oldIsCurrentFlagStat.setCurrentStat("0");
            		oldIsCurrentFlagStat.setModifiedBy(ssk.getCreator());
            		em.merge(oldIsCurrentFlagStat);
            	}

            } // end of if newStatIsCurrentFlag.equals("1")
    		
            StudyStatusBean ssbNew = new StudyStatusBean();

            ssbNew.updateStudyStatus(ssk);
            em.persist(ssbNew);

            StudyStatusBean prevCurStat = null;
            StudyStatusBean nextStat = null;
            String latestEndDate = "";
            StudyStatusBean latestStat = null;
            ArrayList list = new ArrayList();
            Query query = null;

            // find the last current status and update its end date
            Rlog.debug("studystatus", "before getid*****");

            newPk = ssbNew.getId();

            Rlog.debug("studystatus", "new pk******" + newPk);
            studyId = StringUtil.stringToNum(ssbNew.getStatStudy());

            Rlog.debug("studystatus", "studyId ******" + studyId);
            // study pk
            query = em.createNamedQuery("findByPreviousCurrentStatusProt");
            query.setParameter("fk_study", studyId);
            query.setParameter("begindate", ssbNew.returnStatStartDate());
            query.setParameter("pk_studystat", newPk);
            query.setParameter("date_format",dateFormat);
            
            list = (ArrayList) query.getResultList();
            if (list == null)
                list = new ArrayList();
            if (list.size() > 0) {
                prevCurStat = (StudyStatusBean) list.get(0);
                prevCurStat.setModifiedBy(ssbNew.getCreator());
                prevCurStat.updateStatEndDate(ssbNew.returnStatStartDate());
            } else // the start date of this status exists between 2 existing
            // status records or is the first record
            {

                // get the previous record to change its end date

                // find the a previous status from this new start date and
                // update the previous status's end date
                Rlog.debug("studystatus", "in else ******");

                query = em.createNamedQuery("findByPreviousStatusProt");
                query.setParameter("fk_study", studyId);
                query.setParameter("begindate", ssbNew.returnStatStartDate());
                query.setParameter("pk_studystat", newPk);
                query.setParameter("date_format",dateFormat);
                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    prevCurStat = (StudyStatusBean) list.get(0);
                    prevCurStat.updateStatEndDate(ssbNew.returnStatStartDate());
                }
                // ////////

                query = em.createNamedQuery("findByNextStatusProt");
                query.setParameter("fk_study", studyId);
                query.setParameter("begindate", ssbNew.returnStatStartDate());
                query.setParameter("pk_studystat", newPk);
                query.setParameter("date_format",dateFormat);
                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    nextStat = (StudyStatusBean) list.get(0);
                    // a next status found,
                    // update the new status's end date with the next status's
                    // start date
                    ssbNew.updateStatEndDate(nextStat.returnStatStartDate());
                }

                // ////////////
            }
            // find the latest status, and set its end date to null if its not
            // null

            try {
                query = em.createNamedQuery("findByLatestStatusProt");
                query.setParameter("fk_study", studyId);

                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    latestStat = (StudyStatusBean) list.get(0);

                    // a latest status found, get its end date
                    latestEndDate = latestStat.returnStatEndDate();

                    if (!StringUtil.isEmpty(latestEndDate)) {
                        Rlog.debug("studystatus", "in if latestEndDate");
                        latestStat.updateStatEndDate("");
                        Rlog.debug("studystatus", "in if after setStatEndDate");
                    }

                }

            } catch (Exception ex) {
                latestStat = null;
            }
            // end of try       
   
            // This code is added to create submission_status entry when study status is Returned for additional information..Mapping is stored static Hashmap :revBoardSubTypeMapping
            if("LIND".equals(CFG.EIRB_MODE)) {
            	
	            String stat = ssk.getStudyStatus();            
	            String revBStr = ssk.getStudyStatusReviewBoard();
	            String submSubtype="";	
	            ReviewBoardBean rBean=null;   
                String submBoardStr="";  
                Integer subBoardId=0;
	          
	            CodelstBean cbean = em.find(CodelstBean.class, new Integer(stat));    
	            
	            if( cbean != null ) {
	            	String subType = cbean.getClstSubType();	            	
	                
	                for(Map.Entry<String, String> mappingSubTypesEntry : revBoardSubTypeMapping.entrySet()){	                    
	                    if(subType.equals(mappingSubTypesEntry.getKey())) {
	                    	submSubtype = mappingSubTypesEntry.getValue();
	                    	break;
	                    }
	                }
	                
	                if(revBoardSubTypeMapping.containsKey(subType)) {   	
	            		            		
	            		 SubmissionAgent submissionAgent = EJBUtil.getSubmissionAgentHome();
	                     Integer subId = submissionAgent.getExistingSubmissionByFkStudy(studyId); 
	                     
	                     SubmissionBoardBean subBoardBean = new SubmissionBoardBean();
	                     
	            		 if( (revBStr != null) && (revBStr.length() > 0)) {	
	                         try {
	    	                     query = em.createNamedQuery("findByFkCodelstReview");
	    	                     query.setParameter("fkCodelstRevboard", Integer.valueOf(revBStr));	    	                     
	    	                     
	    	                     list = (ArrayList) query.getResultList();	    	                   
	    	                     //Assumption: each review board has only one row in the table
	    	                     if (list.size() > 0) {
	    	                         rBean = (ReviewBoardBean)list.get(0);	
	    	                     }
	                         } catch(Exception e) {
	                        	 Rlog.error("studystatus","EXCEPTION IN Named Query: setStudyStatusDetails:findByFkCodelstReview" + e);
	                             e.printStackTrace();
	                         }
	                         
	                         int boardPk=0;
	                         SubmissionBoardAgent submissionBoardAgent = EJBUtil.getSubmissionBoardAgentHome();
	                         boardPk = submissionBoardAgent.getReviewBoardIfExistsForStudy(subId,rBean.getId());
	                         
	                         if( boardPk == 0 ) {	                         	
		                         subBoardBean.setFkSubmission(subId);
		                         subBoardBean.setFkReviewBoard(rBean.getId());
		                         subBoardBean.setCreator(Integer.valueOf(ssk.getCreator()));
		                         subBoardBean.setLastModifiedBy((ssk.getModifiedBy()) == null ? null : new Integer(ssk.getModifiedBy()));
		                         subBoardBean.setIpAdd(ssk.getIpAdd());
		                         subBoardBean.setFkReviewMeeting(null);
		                         subBoardBean.setSubmissionReviewer(null);
		                         subBoardBean.setSubmissionReviewType((ssk.getStudyStatusReviewBoard()) == null ? null : Integer.valueOf(ssk.getStudyStatusReviewBoard()));           			    
		                         
		                         subBoardId = submissionBoardAgent.createSubmissionBoard(subBoardBean);	                         
	                         
	            			    if (subBoardId.intValue() < 1) {
	            			    	Rlog.info("StudyStatusAgentBean:setStudyStatusDetails", "Submission board is not created.");            			    	                               
	            			    } else {
	            			    	Rlog.info("StudyStatusAgentBean:setStudyStatusDetails", "Submission board is created successfully.");
	            			    	if(subBoardId.intValue() == 0) {
	        	            			submBoardStr = null;
	        	            		 } else {
	        	            			 submBoardStr = String.valueOf(subBoardId);
	        	            		 }
	            			    }
	                         } else {
	                        	 submBoardStr=String.valueOf(boardPk);
	                         }
	            		 }            		 
	            		 
	            		 
	            		 String assignTo = ssk.getStudyStatusAssignedTo();
	            		 Integer assignToInt=0;
	            		 if((assignTo != null) && (assignTo.length() > 0)) {
	            			 assignToInt = Integer.valueOf(assignTo);
	            		 }else {
	            			 assignToInt=null; 
	            		 }
	            		 
	            		 EIRBDao eIRBDao = new EIRBDao();
	            		 int defaultSubmissionBoardId = eIRBDao.getDefaultSubmissionBoardId(subId);
	            		 
	            		 SubmissionStatusBean subStatusBean = new SubmissionStatusBean();
	            		 
	            		 subStatusBean.setFkSubmission(subId);
	            		 subStatusBean.setFkSubmissionBoard(defaultSubmissionBoardId);
	            		 subStatusBean.setSubmissionEnteredBy(Integer.valueOf(ssk.getCreator()));
	            		 subStatusBean.setCreator(Integer.valueOf(ssk.getCreator()));
	            		 subStatusBean.setLastModifiedBy((ssk.getModifiedBy()) == null ? null : new Integer(ssk.getModifiedBy()));
	            		 subStatusBean.setIpAdd(ssk.getIpAdd());
	            		 subStatusBean.setSubmissionStatusDate(ssk.getStatValidDt());
	            		 subStatusBean.setSubmissionAssignedTo(assignToInt);
	            		 subStatusBean.setSubmissionCompletedBy(Integer.valueOf(ssk.getCreator()));
	            		 subStatusBean.setSubmissionNotes(ssk.getStatNotes());            		 
	            		 subStatusBean.setSubmissionStatusBySubtype(submSubtype);
	            		 subStatusBean.setIsCurrent(1);
	            		 subStatusBean.setFkStudyStat(ssbNew.getId());
	            		 
	            		 SubmissionStatusAgent submissionStatAgent = EJBUtil.getSubmissionStatusAgentHome();
	                     Integer subStatId = submissionStatAgent.createSubmissionStatus(subStatusBean) ;
	                     
	                     if( subStatId.intValue() < 1 ) {
	                     	Rlog.info("StudyStatusAgentBean:setStudyStatusDetails", "Submission status is not created.");
	                     } else {
	                     	Rlog.info("StudyStatusAgentBean:setStudyStatusDetails", "Submission status is created successfully.");
	                     	
	                     	//After unlocking study add previous version attachments to next version
	                     	if("unlock_crc".equals(submSubtype) || "unlock_irb".equals(submSubtype) || "unlock_med".equals(submSubtype)) {		                     	
	                     		copyPreviousVerDocstoNextVersion(studyId, ssk.getCreator(), subType);	                     	
	                     	}		                     	
		                    
	                     }
	                     
	            	}
	                
	                HashMap<String, Object> pMap= new HashMap<String, Object>();
	                if( !(StringUtil.isEmpty(String.valueOf(studyId)))) {
	      	            ComplianceJB compB = new ComplianceJB();
	      	            pMap = getParameters(studyId, Integer.valueOf(ssk.getCreator()));
	      	            irbApprovedCount=compB.getIrbApprovedCount(pMap);	           
	      		        if (("app_CHR".equals(subType)  && irbApprovedCount >= 1 )) {	
	      		        	insertLockedVersionForAmendment(pMap, studyId, Integer.valueOf(ssk.getCreator()), subType);	        	
	      		        }
	                }
	            }
            }            
        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "EXCEPTION IN SETTING STUDY STATUS: setStudyStatusDetails"
                            + e);
            e.printStackTrace();
            newPk = -1;
        }    	
    	
        return newPk;
    }
    
    public void copyPreviousVerDocstoNextVersion(int studyId, String userId, String studyStatus) {
    	
    	updateVersioninStudyVersionTable(studyId, "1");
    	UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
 		int version = uiFlxPageDao.getHighestFlexPageVersion("er_study", studyId);
 		int minorVersion = uiFlxPageDao.getHighestMinorFlexPageVersion(version,"er_study", studyId);
 		String versionStr = String.valueOf(version);
 		String minorVerStr = String.valueOf(minorVersion);
 		String verStr = versionStr + "." + minorVerStr;
 		double latestVersion= Double.valueOf(verStr);
 		ArrayList<StudyVerBean> studyVerList = new ArrayList<StudyVerBean>(); 
 		HashMap<String, Object> paramMap = getParameters(studyId, Integer.valueOf(userId)); 		 
 		String latestVersionStr="";
 		ComplianceJB compJB = new ComplianceJB();
 		int aprovedVer=0;
 		boolean isIrbDisApproved = compJB.isIrbDisapprovalExistsInStudyStatusHistory(paramMap); 		
 		if("rej_CHR".equals(studyStatus) || isIrbDisApproved || compJB.getIsIrbDisapproved()) {
	    	aprovedVer = uiFlxPageDao.getApprovedVersionForStudy(studyId);	    	
	    	verStr= String.valueOf(aprovedVer) + "." + "0";
 		}
 		try {
     	    Query query1 = em.createNamedQuery("getDetailedStudyVersion");
            query1.setParameter("FK_STUDY", studyId);		           
            query1.setParameter("studyVerNumber", verStr);	
           
            studyVerList  = (ArrayList) query1.getResultList();		                     	
            
 	    } catch(Exception e) {
 	    	 Rlog.error("studystatus","EXCEPTION IN Named Query: setStudyStatusDetails:getDetailedStudyVersion" + e);
             e.printStackTrace();
 	    }

 		int vNum = uiFlxPageDao.getHighestFlexPageVersion("er_study", studyId);	    
	    int mVerNum=uiFlxPageDao.getHighestMinorFlexPageVersion(vNum, "er_study", studyId);	
	    int rowExists = uiFlxPageDao.checkLockedVersionExists(studyId, vNum);
	   	    
	    if(mVerNum > 0) {
	    	if(rowExists == 1) {
	    		vNum++;
		    	mVerNum=1;
		    	latestVersionStr = String.valueOf(vNum) + "." + String.valueOf(mVerNum);
	    	} else if("crc_amend".equals(studyStatus)) {
		    	vNum++;
		    	mVerNum=1;
		    	latestVersionStr = String.valueOf(vNum) + "." + String.valueOf(mVerNum);
		    } else {
	    		mVerNum++;
	    		latestVersionStr = String.valueOf(vNum) + "." + String.valueOf(mVerNum);
	    	}	    	
	    
	    } else if("crc_amend".equals(studyStatus)) {
	    	vNum++;
	    	mVerNum=1;
	    	latestVersionStr = String.valueOf(vNum) + "." + String.valueOf(mVerNum);
	    } else {
	    	vNum++;
	    	latestVersionStr = String.valueOf(vNum) + "." + "0";	    
	    }	     
	    latestVersion=Double.valueOf(latestVersionStr);
 		
     	StudyApndxDao sApndxdao = new StudyApndxDao();		
 	    AppendixAgentRObj apndxAgent = EJBUtil.getAppendixAgentHome();		                     	
     	ArrayList<AppendixBean> apndxBeanList = new ArrayList<AppendixBean>();		
     	StudyVerBean svBean=null;
     	int verPk=0;
     	StudyVerAgentRObj studyVerAgent = (StudyVerAgentRObj)EJBUtil.getStudyVerAgentHome();
     	for(int l=0; l<studyVerList.size(); l++) {
     		svBean = studyVerList.get(l);
     		verPk = svBean.getStudyVerId();
     		ArrayList apndxIds=null;
     		
     		apndxIds = (ArrayList) sApndxdao.getPkByStudyIdAndFkStudyVer(studyId, verPk);
     		if(apndxIds == null || apndxIds.size() == 0) {
     			continue;
     		}
     		int result = studyVerAgent.newStudyVersion(studyId, new Double(latestVersion).toString(), "Added after unlocking study", "", (studyVerList.get(l)).getStudyVercat() , (studyVerList.get(l)).getStudyVertype() , Integer.valueOf(userId).intValue());
     		if( result < 1 ) {
              	Rlog.info("StudyStatusAgentBean:setStudyStatusDetails", "Study Version is not created.");
             } else {
              	Rlog.info("StudyStatusAgentBean:setStudyStatusDetails", "Study Version is created successfully.");                     	           	            	
             	
              	
              	for( int j=0; j<apndxIds.size(); j++ ) {
             		apndxBeanList.add(apndxAgent.getAppendixDetails((Integer)apndxIds.get(j)));
             	} 
             	AppendixBean apndxBean = new AppendixBean();			                     
             	AppendixBean apndxBean1 = new AppendixBean();
             	for( int m=0; m<apndxBeanList.size(); m++) {
             		apndxBean = apndxBeanList.get(m);
             		String studyVer = apndxBean.getAppendixStudyVer();
             		if(svBean.getStudyVerId() == Double.valueOf(studyVer)) {
             			
                 		apndxBean1.setAppendixStudy((new Integer(studyId)).toString());
                 		apndxBean1.setAppendixStudyVer((new Integer(result)).toString());
                 		apndxBean1.setCreator(String.valueOf(userId));
                 		apndxBean1.setFileSize(apndxBean.getFileSize());
                 		apndxBean1.setModifiedBy(String.valueOf(userId));                    		
                 		
                 		apndxBean1.setAppendixDescription(apndxBean.getAppendixDescription());
                 		apndxBean1.setAppendixPubFlag(apndxBean.getAppendixPubFlag());
                 		apndxBean1.setAppendixSavedFileName(apndxBean.getAppendixSavedFileName());
                 		apndxBean1.setAppendixType(apndxBean.getAppendixType());
                 		apndxBean1.setAppendixUrl_File(apndxBean.getAppendixUrl_File());
                 		
                 		int res=0;
                 		res = apndxAgent.setAppendixDetails(apndxBean1);
                 		if( res < 1 ) {
	                     	Rlog.info("StudyStatusAgentBean:setStudyStatusDetails", "Study Apndx is not created.");
	                    } else {
	                     	Rlog.info("StudyStatusAgentBean:setStudyStatusDetails", "Study Apndx is created successfully.");	                     	
	                    }
             		}
             	
             	}
             }   		
     		
     	}
    	
    }
    
    public void updateVersioninStudyVersionTable(int studyId, String verNum) {
    	ArrayList studyVerList = new ArrayList();
    	StudyVerDao sVerDao= new StudyVerDao();
    	try {
     	    Query query1 = em.createNamedQuery("getDetailedStudyVersion");
            query1.setParameter("FK_STUDY", studyId);		           
            query1.setParameter("studyVerNumber", verNum);	
           
            studyVerList  = (ArrayList) query1.getResultList();	
            
            for(int j=0 ; j < studyVerList.size() ; j++ ) {
            	StudyVerBean sBean = (StudyVerBean)studyVerList.get(j);
            	if(sBean.getStudyVerNumber().equals(verNum)) {            	
	            	int result = sVerDao.updateVersionInStudyVersionTable(studyId,verNum);
	            	if( result > 0 ) {
	            		Rlog.info("StudyStatusAgentBean:updateVersioninStudyVersionTable", "Study Version updated successfully.");
	            	}
            	}
            }
            
 	    } catch(Exception e) {
 	    	 Rlog.error("studystatus","EXCEPTION IN Named Query: setStudyStatusDetails:getDetailedStudyVersion" + e);
             e.printStackTrace();
 	    }
    }
    
    public HashMap<String, Object> getParameters(int studyId, int userId) {  	
    	
    	String accId="";
    	try{
	    	Query query = em.createNamedQuery("findAccountNum");
			query.setParameter("studyNum", studyId);
	        
			StudyBean studyBean=null;
	        ArrayList list = (ArrayList) query.getResultList();
	        if (list == null)
	            list = new ArrayList();
	        if (list.size() > 0) {
	            studyBean = (StudyBean) list.get(0);
	            accId = studyBean.getAccount();
	        }
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
	        
        HashMap<String, Object> paramMap = new HashMap<String, Object>();    
	    paramMap.put("userId", new Integer(userId));
		paramMap.put("accountId", Integer.valueOf(accId).intValue());
		paramMap.put("studyId", new Integer(studyId));
		
		return paramMap;   	
    	
    }
    
    public void insertLockedVersionForAmendment(HashMap<String, Object> paramMap, int studyId, int userId, String studyStatus ) {    	

    	copyPreviousVerDocstoNextVersion(studyId, String.valueOf(userId), studyStatus);
    	ComplianceJB compB = new ComplianceJB();
      	UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
		int verNum = uiFlxPageDao.getHighestFlexPageVersion("er_study", studyId);
		int minorVerNum = uiFlxPageDao.getHighestMinorFlexPageVersion(verNum, "er_study", studyId);		
		HashMap<String, Object> resMap = uiFlxPageDao.getDetailedFlexPage(studyId, verNum, minorVerNum);
		minorVerNum=0;		
		HashMap<String, Object> argMap = new HashMap<String, Object>(); 
		int rowExists = uiFlxPageDao.checkLockedVersionExists(studyId, verNum);
		if(rowExists <= 0 ) {
			if((resMap.get("pageVer")).equals(String.valueOf(verNum))) {
				argMap.put(UIFlxPageDao.MOD_TABLE_STR, "er_study");
				argMap.put(UIFlxPageDao.MOD_PK_STR, new Integer(studyId));
				argMap.put(UIFlxPageDao.PAGE_VER_STR, new Integer(verNum));
				argMap.put(UIFlxPageDao.PAGE_ID_STR, resMap.get("pageId"));
				argMap.put(UIFlxPageDao.PAGE_DEF_STR, resMap.get("pageDef"));
				argMap.put(UIFlxPageDao.PAGE_JS_STR, resMap.get("pageJs"));
				argMap.put(UIFlxPageDao.PAGE_JSON_STR, resMap.get("pageJson"));
				argMap.put(UIFlxPageDao.PAGE_HTML_STR, resMap.get("pageHtml"));
				argMap.put(UIFlxPageDao.PAGE_MINOR_VER, new Integer(minorVerNum));
				argMap.put(UIFlxPageDao.FK_SUBMISSION, resMap.get("fkSubmission"));
				int retNum = uiFlxPageDao.insertFlexPageVer(argMap);		
				if( retNum < 1 ) {
		          	Rlog.info("StudyStatusAgentBean:insertLockedVersionForAmendment", "Version is not created.");
		         } else {
		          	Rlog.info("StudyStatusAgentBean:insertLockedVersionForAmendment", "Version is created successfully.");
		         }
			}    	
			verNum = uiFlxPageDao.getHighestFlexPageVersion("er_study", studyId);
			minorVerNum = uiFlxPageDao.getHighestMinorFlexPageVersion(verNum, "er_study", studyId); 
			minorVerNum++;
			String version = String.valueOf(verNum) + "." + String.valueOf(minorVerNum);
			updateVersioninStudyVersionTable(studyId, version);		
			freezeDocumentForRevisedVersion(studyId, verNum, userId);
		}
    }    
    
    public void freezeDocumentForRevisedVersion(int studyId, int verNumer, int userId) {
    	 // Freeze all categories of this version of this study
	    StudyVerJB studyVerJB = new StudyVerJB();
	    StudyVerDao studyVerDao = studyVerJB.getAllVers(studyId);
	    ArrayList studyVerIds = studyVerDao.getStudyVerIds();
	    ArrayList studyVerNumbers = studyVerDao.getStudyVerNumbers();
	    // Get a codelst item for Freeze status
	    CodeDao statDao = new CodeDao();
	    int freezeStat = statDao.getCodeId(VERSION_STATUS, LETTER_F);
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(Configuration.getAppDateFormat());
		String versionNum = String.valueOf(verNumer);		
	    for (int iX = 0; iX < studyVerNumbers.size(); iX++) {
	    	String svNumStr = (String)studyVerNumbers.get(iX);
	    	double svNum = Double.valueOf(svNumStr).doubleValue();
	    	double vNum = Double.valueOf(versionNum).doubleValue();
	    	if( verNumer > 0 ) {
    			if( svNum == vNum) {
		    		// add a Freeze row to ER_STATUS_HISTORY
		    		StatusHistoryJB statusHistoryJB = new StatusHistoryJB();
		    		statusHistoryJB.setStatusModuleTable(ER_STUDYVER_TABLE);
		    		int modulePk = (Integer)studyVerIds.get(iX);
		    		statusHistoryJB.setStatusModuleId(String.valueOf(modulePk));
		    		statusHistoryJB.setStatusCodelstId(String.valueOf(freezeStat));
		    		statusHistoryJB.setStatusStartDate(sdf.format(cal.getTime()));
		    		statusHistoryJB.setStatusEnteredBy(String.valueOf(userId));
		    		statusHistoryJB.setCreator(String.valueOf(userId));
		    		statusHistoryJB.setStatusHistoryDetails();
    			}
	    	}
	    }
    }

    /**
     * Calls updateStudyStatus() on StudyStatus Entity Bean. Updates the study
     * status with the values in the StudyStatusStateKeeper object.
     * 
     * @param ssk
     *            A StudyStatusStateKeeper object containing StudyStatus
     *            attributes
     * @return int - 0 if successful; -2 for Exception;
     */
    /*
     * Modified by Sonia, 08/14/04, added logic to manage end dates of study
     * status records, added javadoc
     */
    /*
     * Modified by Sonia, 08/30/04 added logic to find the latest status, and
     * set its end date to null if its not null, this way we should have atleast
     * one cuurrent status
     */
    /*
     * Modified by Sonia, 09/14/04 when finding the next status do not set its
     * stataus end date to null (if any status is found)
     */
    public int updateStudyStatus(StudyStatusBean ssk) {
        StudyStatusBean ssObj = null; // 
        // Object
        int ret = 0;
        int studystatPkId = 0;
        int studyId = 0;

        StudyStatusBean prevCurStat = null;
        StudyStatusBean nextStat = null;
        StudyStatusBean latestStat = null;
        String oldStartDate = "";
        String newStartDate = "";
        String oldEndDate = "";
        String latestEndDate = "";
        ArrayList list = new ArrayList();
        Query query = null;
        String dateFormat = Configuration.getAppDateFormat();
        if (dateFormat.indexOf("MMM") != -1)
        	dateFormat = StringUtil.replace(dateFormat,"MMM","Mon");

        try {

            ssObj = (StudyStatusBean) em.find(StudyStatusBean.class, ssk
                    .getId());

            // get old start date and end date
            oldStartDate = ssObj.returnStatStartDate();
            oldEndDate = ssObj.returnStatEndDate();
            // get new start date

            newStartDate = ssk.returnStatStartDate();

            ret = ssObj.updateStudyStatus(ssk);

            // if there is a change in the start date

            studystatPkId = ssk.getId();
            studyId = StringUtil.stringToNum(ssk.getStatStudy());

            if (!newStartDate.equals(oldStartDate)) {
                // find the a previous status from this new start date and
                // update the previous status's end date

                query = em.createNamedQuery("findByPreviousStatusProt");
                query.setParameter("fk_study", studyId);
                query.setParameter("begindate", newStartDate);
                query.setParameter("pk_studystat", studystatPkId);
                query.setParameter("date_format",dateFormat);
                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    prevCurStat = (StudyStatusBean) list.get(0);
                    prevCurStat.updateStatEndDate(ssObj.returnStatStartDate());
                }

                // see if the the start date of this status exists between 2
                // existing status records

                query = em.createNamedQuery("findByNextStatusProt");
                query.setParameter("fk_study", studyId);
                query.setParameter("begindate", newStartDate);
                query.setParameter("pk_studystat", studystatPkId);
                query.setParameter("date_format",dateFormat);
                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    nextStat = (StudyStatusBean) list.get(0);
                    // a next status found,
                    // update the new status's end date with the next status's
                    // start date
                    ssObj.updateStatEndDate(nextStat.returnStatStartDate());
                } else // now this status has become the current status because
                // there is no next record
                {
                    ssObj.updateStatEndDate(null);
                    Rlog
                            .debug("studystatus",
                                    "***update study stat*** findByNextStatusProt status, did not get any next status ");
                }

                // find the latest status, and set its end date to null if its
                // not null

                try {
                    query = em.createNamedQuery("findByLatestStatusProt");
                    query.setParameter("fk_study", studyId);

                    list = (ArrayList) query.getResultList();
                    if (list == null)
                        list = new ArrayList();
                    if (list.size() > 0) {
                        latestStat = (StudyStatusBean) list.get(0);

                        // a latest status found, get its end date
                        latestEndDate = latestStat.returnStatEndDate();

                        if (!StringUtil.isEmpty(latestEndDate)) {
                            Rlog.debug("studystatus", "in if latestEndDate");
                            latestStat.updateStatEndDate("");
                            Rlog.debug("studystatus",
                                    "in if after setStatEndDate");
                        }

                    }

                } catch (Exception ex) {
                    latestStat = null;
                    Rlog
                            .debug(
                                    "studystatus",
                                    "***update study stat*** findByLatestStatusProt status, did not get any latest status");
                }

            } // end of if for checking if status is changed

            return ret;
        } catch (Exception e) {
            // e.printStackTrace();
            Rlog.fatal("studystatus", "EXCEPTION IN UPDATING STUDY STATUS" + e);
            return -2;
        }
    }

    // added on 04/10/01 to fetch the details of studystatus
    // with a perticular study id dinesh

    public StudyStatusDao getStudy(int study) {
        try {
            Rlog.debug("studystatus", "session bean: outside try block");

            StudyStatusDao studystatusDao = new StudyStatusDao();
            Rlog.debug("studystatus", "before calling the get study");
            studystatusDao.getStudy(study);
            Rlog.debug("studystatus", "before returning from the getStudy");

            return studystatusDao;
        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Exception In getStudy in StudyStatusAgentBean " + e);
        }
        return null;
    }

    public StudyStatusDao getStudyStatusDesc(int study, int siteId, int usr,
            int accId) {
        try {
            Rlog.debug("studystatus", "session bean: outside try block");

            StudyStatusDao studystatusDao = new StudyStatusDao();
            Rlog.debug("studystatus", "before calling the get study");
            studystatusDao.getStudyStatusDesc(study, siteId, usr, accId);
            Rlog.debug("studystatus", "before returning from the getStudy");
            return studystatusDao;
        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Exception In getStudy in StudyStatusAgentBean " + e);
        }
        return null;
    }

    public int checkStatus(String studyId) {
        try {
            Rlog.debug("studystatus",
                    "StudyStatusAgentBean.checkStatus : inside try block");
            StudyStatusDao studystatusDao = new StudyStatusDao();
            Rlog
                    .debug("studystatus",
                            "StudyStatusAgentBean.checkStatus before calling the get study");
            return studystatusDao.checkStatus(studyId);
        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Exception In StudyStatusAgentBean.checkStatus  " + e);
        }
        return 0;
    }

    /**
     * Removes Study Status *
     * 
     * @param studyStatusId
     *            Study Status Id to be removed
     * @return int :0 if successful else it returns -1
     */
    /*
     * Modified by Sonia, 08/14/04, added logic to manage end dates of study
     * status records, added javadoc
     */
    /*
     * Modified by Sonia, 08/30/04 added logic to find the latest status, and
     * set its end date to null if its not null, this way we should have atleast
     * one cuurrent status
     */ 
     
    public int studyStatusDelete(int studyStatusId) {
        StudyStatusBean ssObj = null; // Study Status Entity Bean Remote

        ArrayList list = new ArrayList();
        Query query = null;
        // Object

        // int maxId = 0;
        String statusType = "";
        int studyId = 0;
        String nextStartDate = "";
        Enumeration enum_velos;

        StudyStatusBean prevCurStat = null;
        StudyStatusBean nextStat = null;
        StudyStatusBean latestStat = null;
        String latestEndDate = "";
        String dateFormat = Configuration.getAppDateFormat();
        if (dateFormat.indexOf("MMM") != -1)
        	dateFormat = StringUtil.replace(dateFormat,"MMM","Mon");

        try {
            ssObj = (StudyStatusBean) em.find(StudyStatusBean.class,
                    studyStatusId);

            // update end dates
            studyId = StringUtil.stringToNum(ssObj.getStatStudy());

            // see if the the start date of the deleted status exists between 2
            // existing status records

            query = em.createNamedQuery("findByNextStatusProt");
            query.setParameter("fk_study", studyId);
            query.setParameter("begindate", ssObj.returnStatStartDate());
            query.setParameter("pk_studystat", studyStatusId);
            query.setParameter("date_format",dateFormat);
            list = (ArrayList) query.getResultList();
            if (list == null)
                list = new ArrayList();
            if (list.size() > 0) {
                nextStat = (StudyStatusBean) list.get(0);
                // a next status found,
                // get next start date
                nextStartDate = nextStat.returnStatStartDate();
            }

            // find the a previous status from the deleted start date and update
            // the previous status's end date

            query = em.createNamedQuery("findByPreviousStatusProt");
            query.setParameter("fk_study", studyId);
            query.setParameter("begindate", ssObj.returnStatStartDate());
            query.setParameter("pk_studystat", studyStatusId);
            query.setParameter("date_format",dateFormat);
            list = (ArrayList) query.getResultList();
            if (list == null)
                list = new ArrayList();
            if (list.size() > 0) {
                prevCurStat = (StudyStatusBean) list.get(0);
                prevCurStat.updateStatEndDate(nextStartDate);
            }

            // /////////

            StudyStatusDao studyStDao = new StudyStatusDao();
            /* method to get the codelst subtype of the status to be deleted */
            statusType = studyStDao.getCodelstStatusType(studyStatusId, StringUtil
                    .stringToNum(ssObj.getStatStudy()));
            em.remove(ssObj);

            // find the latest status, and set its end date to null if its not
            // null

            try {
                query = em.createNamedQuery("findByLatestStatusProt");
                query.setParameter("fk_study", studyId);

                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    latestStat = (StudyStatusBean) list.get(0);

                    // a latest status found, get its end date
                    latestEndDate = latestStat.returnStatEndDate();

                    if (!StringUtil.isEmpty(latestEndDate)) {
                        Rlog.debug("studystatus", "in if latestEndDate");
                        latestStat.updateStatEndDate("");
                        Rlog.debug("studystatus",
                                "in if after updateStatEndDate");
                    }

                }

            } catch (Exception ex) {
                latestStat = null;
            }
            // //////////////

            // if status is active then call method setActualDateToNull which
            // further calls SP
            if (statusType.equals("active")) {
                studyStDao.setActualDateToNull(studyId, statusType);
            }
            Rlog.debug("studystatus",
                    "StudyStatusAgentBean.studyStatusDelete line 5");
            return 0;
        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Exception in studyStatusDelete() in StudyStatusAgentBean"
                            + e);
            return -1;
        }

    }   
    
 // Overloaded for INF-18183 ::: Akshi
    public int studyStatusDelete(int studyStatusId,Hashtable<String, String> args) {
        StudyStatusBean ssObj = null; // Study Status Entity Bean Remote
        ArrayList list = new ArrayList();
        Query query = null;
        // Object
        // int maxId = 0;
        String statusType = "";
        int studyId = 0;
        String nextStartDate = "";
        Enumeration enum_velos;
        StudyStatusBean prevCurStat = null;
        StudyStatusBean nextStat = null;
        StudyStatusBean latestStat = null;
        String latestEndDate = "";
        String dateFormat = Configuration.getAppDateFormat();
        if (dateFormat.indexOf("MMM") != -1)
        	dateFormat = StringUtil.replace(dateFormat,"MMM","Mon");
        try {
        	AuditBean audit=null;
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String appmodule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the application module name from the Hashtable*/
            String getRidValue= AuditUtils.getRidValue("ER_STUDYSTAT","eres","PK_STUDYSTAT="+studyStatusId);/*Fetches the RID/PK_VALUE*/
            audit = new AuditBean("ER_STUDYSTAT",String.valueOf(studyStatusId),getRidValue,
        			userID,currdate,appmodule,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
            em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/           
            ssObj = (StudyStatusBean) em.find(StudyStatusBean.class,
                    studyStatusId);
            // update end dates
            studyId = StringUtil.stringToNum(ssObj.getStatStudy());
            // see if the the start date of the deleted status exists between 2
            // existing status records

            query = em.createNamedQuery("findByNextStatusProt");
            query.setParameter("fk_study", studyId);
            query.setParameter("begindate", ssObj.returnStatStartDate());
            query.setParameter("pk_studystat", studyStatusId);
            query.setParameter("date_format",dateFormat);
            list = (ArrayList) query.getResultList();
            if (list == null)
                list = new ArrayList();
            if (list.size() > 0) {
                nextStat = (StudyStatusBean) list.get(0);
                // a next status found,
                // get next start date
                nextStartDate = nextStat.returnStatStartDate();
            }
            // find the a previous status from the deleted start date and update
            // the previous status's end date

            query = em.createNamedQuery("findByPreviousStatusProt");
            query.setParameter("fk_study", studyId);
            query.setParameter("begindate", ssObj.returnStatStartDate());
            query.setParameter("pk_studystat", studyStatusId);
            query.setParameter("date_format",dateFormat);
            list = (ArrayList) query.getResultList();
            if (list == null)
                list = new ArrayList();
            if (list.size() > 0) {
                prevCurStat = (StudyStatusBean) list.get(0);
                prevCurStat.updateStatEndDate(nextStartDate);
            }
            // /////////
            StudyStatusDao studyStDao = new StudyStatusDao();
            /* method to get the codelst subtype of the status to be deleted */
            statusType = studyStDao.getCodelstStatusType(studyStatusId, StringUtil
                    .stringToNum(ssObj.getStatStudy()));
            em.remove(ssObj);

            // find the latest status, and set its end date to null if its not
            // null

            try {
                query = em.createNamedQuery("findByLatestStatusProt");
                query.setParameter("fk_study", studyId);

                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    latestStat = (StudyStatusBean) list.get(0);

                    // a latest status found, get its end date
                    latestEndDate = latestStat.returnStatEndDate();

                    if (!StringUtil.isEmpty(latestEndDate)) {
                        Rlog.debug("studystatus", "in if latestEndDate");
                        latestStat.updateStatEndDate("");
                        Rlog.debug("studystatus",
                                "in if after updateStatEndDate");
                    }
                }
            } catch (Exception ex) {
                latestStat = null;
            }
            // //////////////

            // if status is active then call method setActualDateToNull which
            // further calls SP
            if (statusType.equals("active")) {
                studyStDao.setActualDateToNull(studyId, statusType);
            }
            Rlog.debug("studystatus",
                    "StudyStatusAgentBean.studyStatusDelete line 5");
            return 0;
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("studystatus",
                    "Exception in studyStatusDelete() in StudyStatusAgentBean"
                            + e);
            return -1;
        }
    }
    
    public int getFirstActiveEnrollPK(int studyId) {
        int ret = 0;
        try {

            StudyStatusDao studystatusDao = new StudyStatusDao();

            ret = studystatusDao.getFirstActiveEnrollPK(studyId);

        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Exception In getStudy in StudyStatusAgentBean " + e);
        }
        return ret;
    }

    public String getMinActiveDate(int studyId) {
        String ret = "";
        try {

            StudyStatusDao studystatusDao = new StudyStatusDao();

            ret = studystatusDao.getMinActiveDate(studyId);

        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Exception In getMinActiveDate in StudyStatusAgentBean "
                            + e);
        }
        return ret;
    }

    public String getMinPermanentClosureDate(int studyId) {
        String ret = "";
        try {
            StudyStatusDao studystatusDao = new StudyStatusDao();

            ret = studystatusDao.getMinPermanentClosureDate(studyId);

        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Exception In getMinPermanentClosureDate in StudyStatusAgentBean "
                            + e);
        }
        return ret;
    }

    public int getCountStudyStat(int studyId) {
        int ret = 0;
        try {
            StudyStatusDao studystatusDao = new StudyStatusDao();

            ret = studystatusDao.getCountStudyStat(studyId);

        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Exception In getCountStudyStat in StudyStatusAgentBean "
                            + e);
        }
        return ret;
    }

    public StudyStatusBean findByAtLeastOneStatus(int studyId, String codelstId) {
        ArrayList list = null;
        Query query = null;
        CodeDao codeDao=new CodeDao();

        query = em.createNamedQuery("findByAtLeastOneStatusProt");
        query.setParameter("fk_study", new Integer(studyId));
        query.setParameter("pk_codelst", codeDao.getCodeId("studystat",codelstId));
        

        list = (ArrayList) query.getResultList();
        if (list == null)
            list = new ArrayList();
        if (list.size() > 0) {
            return (StudyStatusBean) list.get(0);
        } else
            return null;

    }
    
    public int getCountByOrgStudyStat(int studyId,int orgId,String studyTypeList,String flag) {
        int ret = 0;
        try {
            StudyStatusDao studystatusDao = new StudyStatusDao();

            ret = studystatusDao.getCountByOrgStudyStat(studyId,orgId,studyTypeList, flag);

        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Exception In getCountByOrgStat in StudyStatusAgentBean "
                            + e);
        }
        return ret;
    }

    
    public StudyStatusDao getDAOByOrgStudyStat(int studyId,int orgId,String studyTypeList,String flag) {
        
        try {
            StudyStatusDao studystatusDao = new StudyStatusDao();

             studystatusDao.getCountByOrgStudyStat(studyId,orgId,studyTypeList, flag);
             
             return studystatusDao;

        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Exception In getCountByOrgStat in StudyStatusAgentBean "
                            + e);
        }
        return null;
    }
    
    public void updateBlobfileObject(HashMap<String, Integer> hMap, int studyId) {
    	StudyApndxDao sApndxdao = new StudyApndxDao();
    	for (Map.Entry<String, Integer> entry : hMap.entrySet())
    	{
    	    System.out.println(entry.getKey() + " : " + entry.getValue());
    	    String str = entry.getKey();
    	    String oldpk="";
    	    String filePath="";
    	    StringTokenizer st = new StringTokenizer(str , "*");
    	    while(st.hasMoreElements()) {
    	    	oldpk = st.nextToken();
    	    	filePath = st.nextToken();
    	    }
    	    BLOB blobObj = sApndxdao.getStudyApndixFileObject(studyId, Integer.parseInt(oldpk));
    	    long fileSize=0;
        	File blobFile=null;
        	try {
        		blobFile = new File(filePath);
        		FileOutputStream outStream = new FileOutputStream(blobFile);
        		//InputStream inStream = blobObj.getStream();
        		InputStream inStream = blobObj.getBinaryStream();
        		
        		int length = -1;
        		int size = blobObj.getBufferSize();
        		byte[] buffer = new byte[size];

        		while ((length = inStream.read(buffer)) != -1) {
        			outStream.write(buffer, 0, length);
        			outStream.flush();
        		}
        		
        		fileSize = blobFile.length();

        		inStream.close();
        		outStream.close();
        	} catch (Exception e) {
        		e.printStackTrace();
        		Rlog.info("StudyStatusAgentBean:getAndsetBlobObjectForFile", "Unable to export:" + filePath);    		
        	}
        	
        	sApndxdao.updateStudyApndixFileObject(studyId, (entry.getValue()).intValue(), filePath, blobObj);
    	}
    	
    }
    


}// end of class

