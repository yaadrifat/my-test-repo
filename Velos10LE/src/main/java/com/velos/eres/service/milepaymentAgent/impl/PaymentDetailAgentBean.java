/*
 * Classname			PaymentDetailAgentBean
 * 
 * Version information 	1.0
 *
 * Date					05/28/2002
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.service.milepaymentAgent.impl;

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.common.PaymentDetailsDao;
import com.velos.eres.business.milepayment.impl.PaymentDetailBean;
import com.velos.eres.service.milepaymentAgent.PaymentDetailAgentRObj;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The stateless session EJB acting as a facade for the entity CMP.<br>
 * <br>
 * 
 * @author Sonika
 * @version 1.0 05/28/2002
 */
@Stateless
public class PaymentDetailAgentBean implements PaymentDetailAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /*
     * getPaymentDetail Calls getPaymentDetail() on entity bean -
     * PaymentDetailBean Looks up on the Milepayment id parameter passed in and
     * constructs and returns a Milepayment state holder.
     * 
     * @param paymentDetailId the Milepayment id
     * 
     * @see PaymentDetailBean
     */
    public PaymentDetailBean getPaymentDetail(int paymentDetailId) {

        try {
            return (PaymentDetailBean) em.find(PaymentDetailBean.class,
                    new Integer(paymentDetailId));
        } catch (Exception e) {
            Rlog.debug("paymentDetail",
                    "Error in getPaymentDetail() in PaymentDetailAgentBean"
                            + e);
            return null;
        }

    }

    /**
     * Creates a new a Milepayment. Calls setMilepaymentDetails() on entity bean
     * PaymentDetailBean
     * 
     * @param an
     *            Milepayment state holder containing Milepayment attributes to
     *            be set.
     * @return generated paymentDetail id
     * @see PaymentDetailBean
     */

    public int setPaymentDetail(PaymentDetailBean msk) {
        PaymentDetailBean mPaymentBean = new PaymentDetailBean();

        try {
            mPaymentBean.updatePaymentDetail(msk);
            em.persist(mPaymentBean);
            return mPaymentBean.getId();
 
        } catch (Exception e) {
            System.out
                    .print("Error in setPaymentDetail() in PaymentDetailAgentBean "
                            + e);
        }
        return -1;
    }

    /**
     * Updates Milepayment Details. Calls updateMilepayment() on entity bean
     * PaymentDetailBean
     * 
     * @param an
     *            PaymentDetailBean containing Milepayment attributes to
     *            be set.
     * @return int 0 for successful ; -2 for exception
     * @see PaymentDetailBean
     */

    public int updatePaymentDetail(PaymentDetailBean msk) {

        PaymentDetailBean paymentDetailRObj = null; // Milepayment Entity Bean
        // Remote Object
        int output;

        try {

            paymentDetailRObj = (PaymentDetailBean) em.find(PaymentDetailBean.class,
                    new Integer(msk.getId()));
            if (paymentDetailRObj == null) {
                return -2;
            }
            output = paymentDetailRObj.updatePaymentDetail(msk);
        } catch (Exception e) {
            Rlog.debug("paymentDetail",
                    "Error in updatePaymentDetail in PaymentDetailAgentBean" + e);
            return -2;
        }
        return output;
    }


    /**
     * sets the milepayment details using values from  PaymentDetailsDao, updates a record if already exists , otherwise creates new records
     */
    public int setPaymentDetail(PaymentDetailsDao  pd)
    {
        
        
        int ct = 0;
        int ret = 0;
        int idPk = 0;
    	ArrayList id = new ArrayList();
    	ArrayList amount  = new ArrayList();
    	ArrayList creator = new ArrayList();
    	ArrayList fkPayment= new ArrayList();
    	ArrayList iPAdd = new ArrayList();
    	ArrayList lastModifiedBy = new ArrayList();
		
    	ArrayList linkedTo = new ArrayList();
    	ArrayList linkToId = new ArrayList();
    	ArrayList linkToLevel1 = new ArrayList();
    	ArrayList linkToLevel2 = new ArrayList();
    	ArrayList arLinkMileAchieved = new ArrayList();;
    	ArrayList<Boolean> arHoldBackFlag = new ArrayList<Boolean>();;
    	ArrayList<Float> arapplyAmounttoHoldBack = new ArrayList<Float>();;
    	String lastMod = "";
        
    	linkedTo = pd.getLinkedTo();
        ct = linkedTo.size();
        arHoldBackFlag= pd.getHoldBackFlag();
        amount  = pd.getAmount();
    	creator = pd.getCreator();
    	fkPayment= pd.getFkPayment();
    	iPAdd = pd.getIPAdd();
    	lastModifiedBy = pd.getLastModifiedBy();
    	arapplyAmounttoHoldBack=pd.getHoldBackAppliedDueToDateList();
		
    	linkToId = pd.getLinkToId();
    	linkToLevel1 = pd.getLinkToLevel1();
    	linkToLevel2 = pd.getLinkToLevel2();
    	id = pd.getId();
    	
    	arLinkMileAchieved = pd.getLinkMileAchieved();

        try {
        	
        	for (int i = 0; i < ct; i++)
        	{
        		lastMod = (String)lastModifiedBy.get(i);
        		idPk = StringUtil.stringToNum((String) id.get(i));
        		PaymentDetailBean mPaymentBean =null;
        		if (idPk == 0)
        		{
        			lastMod = null;
        		}
        		
 
        		if(arHoldBackFlag!=null && !arHoldBackFlag.isEmpty() && arHoldBackFlag.get(i)!=null && arHoldBackFlag.get(i).equals(Boolean.TRUE)){
        			mPaymentBean	= new PaymentDetailBean((String)amount.get(i), (String)creator.get(i), (String)fkPayment.get(i), idPk, (String)iPAdd.get(i), lastMod, (String)linkedTo.get(i), (String)linkToId.get(i),
               			 (String)linkToLevel1.get(i),(String)linkToLevel2.get(i),(String) arLinkMileAchieved.get(i),arapplyAmounttoHoldBack.get(i));
        		}else{
        			mPaymentBean	= new PaymentDetailBean((String)amount.get(i), (String)creator.get(i), (String)fkPayment.get(i), idPk, (String)iPAdd.get(i), lastMod, (String)linkedTo.get(i), (String)linkToId.get(i),
        			 (String)linkToLevel1.get(i),(String)linkToLevel2.get(i),(String) arLinkMileAchieved.get(i));
        		}
        	 if (idPk <= 0)
     		{
        		 //System.out.println("creating new");
        		 setPaymentDetail(mPaymentBean);
     		}
        	 else
        	 {
        		 //System.out.println("updatine" + idPk);
        		  updatePaymentDetail(mPaymentBean);
        		 
        	 }
        	}
        	
        	return 1;
 
        } catch (Exception e) {
            System.out
                    .print("Error in  setPaymentDetail(PaymentDetailsDao  pd) in PaymentDetailAgentBean "
                            + e);
        }
        return -1;
    	
    }
    
    /**
     *   if payment details are added, gets payment detail records with previous totals, else 
     *  gets previous payment totals
     * @param payment the paymentPk for which detail records are retrieved. If payment == 0, then just get the totals of previous payments for the same linkToId 
     * @param linkToId the linkToId, main record type for which payment detail records are retrieved
     * @param linkType the type of payment link. possible values: M-Milestones, I-Invoice, B-Budgets        
     */

    public PaymentDetailsDao getPaymentDetailsWithTotals(int payment, int linkToId,String linkType )
    {
    	PaymentDetailsDao pd = new PaymentDetailsDao();
    	
    	try{
    		pd.getPaymentDetailsWithTotals(payment,linkToId,linkType );
    	}
    		catch (Exception e) {
            System.out
                    .print("Error in  getPaymentDetailsWithTotals(int payment, int linkToId ) in PaymentDetailAgentBean "
                            + e);
        }
    	
    	return pd;
    	
    }


    /**
     *   get linked payment browser info
     * @param payment 
     * @param linkType        
     */

    public PaymentDetailsDao getLinkedPaymentBrowser(int payment, String linkType)
    {
    	PaymentDetailsDao pd = new PaymentDetailsDao();
    	
    	try{
    		pd.getLinkedPaymentBrowser(payment,linkType );
    	}
    		catch (Exception e) {
            System.out
                    .print("Error in  getLinkedPaymentBrowser(int payment, int linkType ) in PaymentDetailAgentBean "
                            + e);
        }
    	
    	return pd;
    	
    }

    
}// end of class
