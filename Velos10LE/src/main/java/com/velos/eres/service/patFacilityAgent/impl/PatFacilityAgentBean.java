/*
 * Classname			PatFacilityAgentBean
 * 
 * Version information : 
 *
 * Date					06/25/2003
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.patFacilityAgent.impl;

/* IMPORT STATEMENTS */

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.velos.eres.business.common.PatFacilityDao;
import com.velos.eres.business.patFacility.impl.PatFacilityBean;
import com.velos.eres.service.patFacilityAgent.PatFacilityAgentRObj;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the PatientFacility POJO.
 * 
 * @author Sonia Abrol
 * @version 1.0, 09/22/2005
  */

@Stateless
public class PatFacilityAgentBean implements PatFacilityAgentRObj {

    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * Looks up on the id parameter passed in and constructs and
     * returns a PatFacilityBean containing all the Patient Facility Details 
     * @param id   The Patient Facility Id
     */
     
    public PatFacilityBean getPatFacilityDetails(int id) {
        PatFacilityBean pb = null; // Category Entity Bean Remote Object
        try {

           pb = (PatFacilityBean) em.find(PatFacilityBean.class, new Integer(id));

        } catch (Exception e) {
            Rlog.fatal("PatFacility",
                    "Error in getPatFacilityDetails() in PatFacilityAgentBean" + e);
        }

        return pb;
    }

    /**
     * Creates a new Patient Facility record
     * 
     * @param patFacility
     *            A PatFacilityBean containing patient facility details.
     * @return The Patient facility Id for the new record <br>
     *         0 - if the method fails
     */

    public int setPatFacilityDetails(PatFacilityBean patFacility) {

        try {

            String isNewDefault = "0";
            
            PatFacilityBean pf = new PatFacilityBean();
            
            isNewDefault = patFacility.getIsDefault();
            
            if (StringUtil.isEmpty(isNewDefault))
            	isNewDefault = "0";
            
            //if the organization is default, then turn off isDefault flag of previous
            // organizations (if any)
            if (isNewDefault.equals("1"))
            {
                Query query = em.createNamedQuery("findPreviousDefaultSite");
                query.setParameter("patientPk", patFacility.getPatientPK());
			    
			    ArrayList list = (ArrayList) query.getResultList();
			    if (list == null)
			        list = new ArrayList();
			    
			    if (list.size() > 0) 
			    {
			    	PatFacilityBean pBean=(PatFacilityBean )list.get(0);
			         pBean.setIsDefault("0");
			         pBean.setLastModifiedBy(patFacility.getCreator());
			         em.merge(pBean);
			        
			    }
            }	    
            pf.updatePatFacility(patFacility);
            em.persist(pf);
            return (pf.getId());
        } catch (Exception e) {
            Rlog.fatal("PatFacility",  "Error in setPatFacilityDetails() in PatFacilityAgentBean " + e);
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * Updates the Patient Facility record with data passes in  PatFacilityBean object 
     * 
     * @param clsk
     *            An PatFacilityStateKeeper containing category attributes to be
     *            updated.
     * @return 0 - if the update is successful<br>
     *         -2 - if the update fails
     */

    public int updatePatFacility(PatFacilityBean pb) {

        PatFacilityBean retrieved = null; 
        int output;
        String isNewDefault = "0";
        String isOldDefault = "0";
        try {

            retrieved = (PatFacilityBean) em.find(PatFacilityBean.class, new Integer(pb.getId()));

            if (retrieved == null) {
                return -2;
            }
            ////////////////////
            
            isNewDefault = pb.getIsDefault();
            
            if (StringUtil.isEmpty(isNewDefault))
            	isNewDefault = "0";
            
            isOldDefault = retrieved.getIsDefault();
            
            if (StringUtil.isEmpty(isOldDefault))
            	isOldDefault = "0";
            
            //if the organization is default, then turn off isDefault flag of other
            // organizations (if any)
            if (isNewDefault.equals("1") && isOldDefault.equals("0"))
            {
                Query query = em.createNamedQuery("findPreviousDefaultSite");
                query.setParameter("patientPk", pb.getPatientPK());
			    
			    ArrayList list = (ArrayList) query.getResultList();
			    if (list == null)
			        list = new ArrayList();
			    
			    if (list.size() > 0) 
			    {
			    	PatFacilityBean pBean=(PatFacilityBean )list.get(0);
			         pBean.setIsDefault("0");
			         pBean.setLastModifiedBy(pb.getLastModifiedBy());
			         //em.merge(pBean);
			        
			    }
            }
            
            //////////////////
            output = retrieved.updatePatFacility(pb);
            em.merge(retrieved);
        } catch (Exception e) {
            Rlog.debug("PatFacility", "Error in updatePatFacility in PatFacilityAgentBean"
                            + e);
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * Deletes a patient facility record 
     * @param id  The Patient Facility Id
     */
     
    public int  deletePatFacility(int id) {
        PatFacilityBean pb = null; // Category Entity Bean Remote Object
        try {

           pb = (PatFacilityBean) em.find(PatFacilityBean.class, new Integer(id));
	   em.remove(pb);
	   return 0;
        } catch (Exception e) {
            Rlog.fatal("PatFacility",
                    "Error in getPatFacilityDetails() in PatFacilityAgentBean" + e);
	 return -2;
        }

        
    }
    
    /**
     * Gets Patient facilities
     * 
     * @param perPK
     */

    public PatFacilityDao getPatientFacilities(int perPk) {
    	
    	PatFacilityDao pd = new PatFacilityDao();
    	try{
	    	pd.getPatientFacilities(perPk);
	    	return pd;
	      	}catch (Exception ex)
	      	{
	      		Rlog.fatal("PatFacility",
	                    "Error in getPatientFacilitiesin PatFacilityAgentBean" + ex);
	      		return pd;
    	}
	      	
    	
       }
    
    
    /**
     * Gets Patient facilities Site Ids
     * 
     * @param perPK
     */

    public PatFacilityDao getPatientFacilitiesSiteIds(int perPk) {
    	PatFacilityDao pd = new PatFacilityDao();
    	try{
	    	pd.getPatientFacilitiesSiteIds(perPk);
	    	return pd;
	      	}catch (Exception ex)
	      	{
	      		Rlog.fatal("PatFacility",
	                    "Error in getPatientFacilitiesSiteIds PatFacilityAgentBean" + ex);
	      		return pd;
    	}
     	
    	
       
    }

    /** Returns the access right flag for user's specialty. Specialty filters for all patient facilities are checked.
    
     * @param splId Speciality Id
     * */
    
    public int getUserFacilitySpecialtyAccessRight(int splId, int perPk) {
    	PatFacilityDao pd = new PatFacilityDao();
    	int right = 0;
    	try{
    		right = pd.getUserFacilitySpecialtyAccessRight(splId, perPk);
	    	return right ;
	      	}catch (Exception ex)
	      	{
	      		Rlog.fatal("PatFacility",
	                    "Error in getUserFacilitySpecialtyAccessRight PatFacilityAgentBean" + ex);
	      		return right;
    	}
     	
    		
    }
    
}// end of class

