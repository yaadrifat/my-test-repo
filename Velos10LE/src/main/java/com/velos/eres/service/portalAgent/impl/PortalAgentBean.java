/*
 * Classname			PortalAgentBean
 * 
 * Version information : 
 *
 * Date					04/04/2007
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.portalAgent.impl;

/* IMPORT STATEMENTS */

import java.util.ArrayList;
import java.util.Hashtable;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.PortalDao;
import com.velos.eres.business.portal.impl.PortalBean;
import com.velos.eres.service.portalAgent.PortalAgentRObj;
import com.velos.eres.business.portalPwdHistory.impl.PortalPwdHistoryBean;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP.
 * 
 * @author Manimaran
 * @version 1.0, 04/04/2007
 * @ejbHome PortalAgentHome
 * @ejbRemote PortalAgentRObj
 */

@Stateless
public class PortalAgentBean implements PortalAgentRObj {

    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    @Resource private SessionContext context;

    public PortalBean getPortalDetails(int portalId) {
        PortalBean ap = null;
        try {

            ap = (PortalBean) em.find(PortalBean.class, new Integer(portalId));
            em.clear(); //Bug #7813
            CommonDAO cd = new CommonDAO();
            cd.populateClob( "er_portal","portal_header"," where pk_portal = " + portalId);
            ap.setPortalHeader(cd.getClobData());
            
            cd = new CommonDAO();
            cd.populateClob( "er_portal","portal_footer"," where pk_portal = " + portalId);
            ap.setPortalFooter(cd.getClobData());
            

        } catch (Exception e) {
            Rlog.fatal("portal",
                    "Error in getPortalDetails() in PortalAgentBean" + e);
        }

        return ap;
    }
    
    public int setPortalDetails(PortalBean pb) {
    	int retCode= 0;
        try {
        	retCode = validatePortal(pb);
        	
        	if (retCode < 0)
        	{
        		return retCode;
        	}
        	
        	PortalBean ad = new PortalBean();
            ad.updatePortal(pb);
            em.persist(ad);
            return ad.getPortalId();
        } catch (Exception e) {
            Rlog.fatal("portal",
                    "Error in setPortalDetails() in PortalAgentBean " + e);
        }
        return 0;
    }
    
    public int updatePortal(PortalBean ap) {
    	PortalBean adm = null;
    	int retCode = 0;
    	
        int output;
        try {
            adm = (PortalBean) em.find(PortalBean.class, new Integer(
                    ap.getPortalId()));
            if (adm == null) {
                return -2;
            }
            
            retCode = validatePortal(ap);
        	
        	if (retCode < 0)
        	{
        		return retCode;
        	}
        	
            output = adm.updatePortal(ap);
            em.merge(adm);
        } catch (Exception e) {
            Rlog.debug("portal", "Error in updatePortal in PortalAgentBean"
                    + e);
            return -2;
        }
        return output;
    }
    
    
    public PortalDao getPortalValues(int accountId, int userId) {

        try {
        
            Rlog.debug("portal",
                    "PortalAgentBean.getPortalValues starting");
            PortalDao portalDao = new PortalDao();        
            portalDao.getPortalValues(accountId,userId);            
            return portalDao;
        } catch (Exception e) {
            Rlog.fatal("portal",
                    "Exception In getPortalValues in PortalAgentBean " + e);
        }
        return null;
    }
    
    public PortalDao getPortalValue(int portalId) {

        try {
        
            Rlog.debug("portal",
                    "PortalAgentBean.getPortalValue starting");
            PortalDao portalDao = new PortalDao();        
            portalDao.getPortalValue(portalId);            
            return portalDao;
        } catch (Exception e) {
            Rlog.fatal("portal",
                    "Exception In getPortalValue in PortalAgentBean " + e);
        }
        return null;
    }
    
    public PortalDao getPortalAuditUser(int portalId) {
    	PortalDao portalDao = new PortalDao();
    	portalDao.getPortalAuditUser(portalId);
    	return portalDao;
    }
    
    public PortalDao insertPortalValues(int pid,String portalPatPop,String selIds,int usr,int user,String ipAdd) {

        try {
        
            Rlog.debug("portal",
                    "PortalAgentBean.insertPortalValues starting");
            PortalDao portalDao = new PortalDao();        
            portalDao.insertPortalValues(pid,portalPatPop,selIds,usr,user,ipAdd);            
            return portalDao;
        } catch (Exception e) {
            Rlog.fatal("portal",
                    "Exception In insertPortalValues in PortalAgentBean " + e);
        }
        return null;
    }
    
    public PortalDao deletePortalPop(int portalId) {

        try {
        
            Rlog.debug("portal",
                    "PortalAgentBean.deletePortalPop starting");
            PortalDao portalDao = new PortalDao();        
            portalDao.deletePortalPop(portalId);            
            return portalDao;
        } catch (Exception e) {
            Rlog.fatal("portal",
                    "Exception In deletePortalPop in PortalAgentBean " + e);
        }
        return null;
    }

 // Overloaded for INF-18183 ::: Raviesh
    public PortalDao deletePortalPop(int portalId,Hashtable<String, String> args) {

        try {
        
        	AuditBean audit=null;
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String appModule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the application module name from the Hashtable*/
            
            Hashtable<String, ArrayList<String>> getRowValues = AuditUtils.getRowValues("ER_PORTAL_POPLEVEL","FK_PORTAL="+portalId, "eres");/*Fetches the RID/PK_VALUE*/
            ArrayList<String> rowPK  = (ArrayList<String>)getRowValues.get(AuditUtils.ROW_PK_KEY);
    		ArrayList<String> rowRID  = (ArrayList<String>)getRowValues.get(AuditUtils.ROW_RID_KEY);
    		
    		String pkVal = "";
        	String ridVal = "";
        	
    		for(int i=0; i<rowPK.size(); i++)
			{
    			pkVal = rowPK.get(i);
    			ridVal = rowRID.get(i);
    			audit = new AuditBean("ER_PORTAL_POPLEVEL",pkVal,ridVal,
        			userID,currdate,appModule,ipAdd,reason);
    			em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
			}
            
            Rlog.debug("portal",
                    "PortalAgentBean.deletePortalPop starting");
            PortalDao portalDao = new PortalDao();        
            portalDao.deletePortalPop(portalId);            
            return portalDao;
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("portal",
                    "Exception In deletePortalPop in PortalAgentBean " + e);
        }
        return null;
    } 
    
    public void deletePortal(int portalId) {
        try {
            Rlog.debug("portal",
                    "In deletePortal in PortalAgentBean line number 0");
            PortalDao portalDao = new PortalDao();
            Rlog.debug("portal",
                    "In deletePortal in PortalAgentBean line number 1");
            portalDao.deletePortal(portalId);
            Rlog.debug("portal",
                    "In deletePortal in PortalAgentBean line number 2");
        } catch (Exception e) {
            Rlog.fatal("portal",
                    "Exception In deletePortal in PortalAgentBean " + e);
        }
    }

 // Overloaded for INF-18183 ::: Akshi
    public void deletePortal(int portalId,Hashtable<String, String> args) {
        try {
        	
        	AuditBean audit=null;
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String appmodule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the application module name from the Hashtable*/
            String getRidValue= AuditUtils.getRidValue("ER_PORTAL","eres","PK_PORTAL="+portalId);/*Fetches the RID/PK_VALUE*/ 
            audit = new AuditBean("ER_PORTAL",String.valueOf(portalId),getRidValue,
        			userID,currdate,appmodule,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
            em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
            Rlog.debug("portal",
                    "In deletePortal in PortalAgentBean line number 0");
            PortalDao portalDao = new PortalDao();
            Rlog.debug("portal",
                    "In deletePortal in PortalAgentBean line number 1");
            int ret = portalDao.deletePortal(portalId, userID, ipAdd);
            Rlog.debug("portal",
                    "In deletePortal in PortalAgentBean line number 2");
            if(ret != 0){  /*if Dao returns result without exception*/
             context.setRollbackOnly();
            }
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("portal",
                    "Exception In deletePortal in PortalAgentBean " + e);
        }
    }
   
	// Modified for PP-18306 AGodara
	public PortalDao getDesignPreview(int portalId, int patProtProtocolId) {
		
		try {
			// Fetching all CalIds of the portal 			
			PortalDao portalDao = new PortalDao();
			String calIds = portalDao.getPortalCalIds(portalId);
				 
			String[] calIdsArray = calIds.split(",");
			boolean flag =false;
			if(patProtProtocolId!=0){
				// Checking If patient's current calendar is among calendars associated with portal
				for (String calId : calIdsArray) {
					if (calId.equalsIgnoreCase(String.valueOf(patProtProtocolId))) {
						flag=true;
						break;
					}
				}	
			}else{
				flag=false;
			}
			
			// passing true if we want patient forms and calendars both. For patient forms only, passing false.
			portalDao.getDesignPreview(portalId, patProtProtocolId, flag);
			return portalDao;
		} catch (Exception e) {
			Rlog.fatal("portal",
					"Exception In getDesignPreview in PortalAgentBean " + e);
		}
		return null;
	}

    /* create default logins for the portal according to patient population option*/
    public int createPatientLogins(int portalId, int creator, String ipAdd) 
    {
    	int ret = 0;
        try {
            PortalDao portalDao = new PortalDao();
            ret = portalDao.createPatientLogins(portalId,creator,ipAdd);
            
        } catch (Exception e) {
            Rlog.fatal("portal",
                    "Exception In createPatientLogins in PortalAgentBean " + e);
        }
        return ret;
    }
    
    /**validates portal bean
     * @return int 0 : validations passed
     * 			   -3 : Duplicate Portal Name
     * 			   -4 : Portal with 'Provide Create Patients Login button in 'Manage Logins'' already exists 		
     */
    public int validatePortal(PortalBean pb) {    
    	PortalBean pBean = null;
    	int retCode = 0;
    	
        try {

            Query queryName = em.createNamedQuery("checkDuplicateName");
            queryName.setParameter("pname", pb.getPortalName());
            queryName.setParameter("fkacc", pb.getAccountId());
            
            pBean = (PortalBean)queryName.getSingleResult();
            
            if (pBean != null && pBean.getPortalId() > 0 )//found a record
            {	
		            if (pb.getPortalId() > 0 ) //modify mode
		            	{
		            		if ( pb.getPortalId() == pBean.getPortalId() ) //returned the same portal
		            		{
		            			retCode = 0;
		            		}
		            		else
		            		{
		            			retCode = -3;
		            			
		            		}
		            	}
		            else
		            {
		            	retCode = -3;
		            }
            
            }
            else
            {
            	retCode = 0;
            }
            

        } catch (Exception e) {
            Rlog.fatal("portal", "Exception in validatePortal() in PortalAgentBean" + e);
            retCode = 0;
        }

        if (retCode < 0) // the first validaton failed
        {
        	return retCode; 
        }
    
        //second validation
         
        
        if (EJBUtil.stringToNum(pb.getPortalCreateLogins()) ==  1)
        {
	        try {
	
	            Query queryFlag = em.createNamedQuery("checkCreateLoginFlag");
	            queryFlag.setParameter("fkacc", pb.getAccountId());
	            
	            pBean = (PortalBean)queryFlag.getSingleResult();
	            
	            if (pBean != null && pBean.getPortalId() > 0 )//found a record
	            {	
			            if (pb.getPortalId() > 0 ) //modify mode
			            	{
			            		if ( pb.getPortalId() == pBean.getPortalId() ) //returned the same portal
			            		{
			            			retCode = 0;
			            		}
			            		else
			            		{
			            			retCode = -4;
			            			
			            		}
			            	}
			            else
			            {
			            	retCode = -4;
			            }
	            
	            }
	            else
	            {
	            	retCode = 0;
	            }
	            
	
	        } catch (Exception e) {
	            Rlog.fatal("portal", "Exception in validatePortal() in PortalAgentBean" + e);
	            retCode = 0;
	        }
        }
        
        return retCode;
    }
    
    /**
     * To Check Portal Password Rotation Error
     * 
     * @author Raviesh
     * @Enhancement INF-18669
     * 
     * @param encryptedPass: Default Password.
     * @param portalId: Portal Id.
     * @param userAccId: Account id of the user.
     * 
     */
    public Boolean checkPasswordRotation(String encryptedPass,Integer portalId,Integer userAccId){
    	try{
    		Query query=em.createNamedQuery("portalRotation");
    		query.setMaxResults(10);
    		query.setParameter(1, portalId);
    		query.setParameter(2, userAccId);
    		ArrayList result=(ArrayList)query.getResultList();
    		if (result == null || result.size() == 0) { return false; }
    		for(Object o:result){
    			PortalPwdHistoryBean portalPwdHistoryBean = (PortalPwdHistoryBean)o;
    			if(encryptedPass.equals(portalPwdHistoryBean.getUserPwd())){
    				return true;
    			}
    		}
    	}catch (Exception e) {
    		Rlog.fatal("user", "Exception in method checkPasswordRotation (PortalAgentBean) " + e);
		}
		return false;
    }
    /**
     * To Check Portal Login Password Rotation Error
     * 
     * @author Raviesh
     * @Enhancement INF-18669
     * 
     * @param encryptedPass: Default Password.
     * @param portalId: Portal Id.
     * @param userAccId: Account id of the user.
     * 
     */
    public Boolean checkPortalLoginPwdRotation(String encryptedPass,Integer portalId,Integer userAccId,Integer portalLogin){
    	try{
    		Query query=em.createNamedQuery("portalLoginRotation");
    		query.setMaxResults(10);
    		query.setParameter(1, portalId);
    		query.setParameter(2, portalLogin);
    		ArrayList result=(ArrayList)query.getResultList();
    		if (result == null || result.size() == 0) { return false; }
    		for(Object o:result){
    			PortalPwdHistoryBean portalPwdHistoryBean = (PortalPwdHistoryBean)o;
    			if(encryptedPass.equals(portalPwdHistoryBean.getUserPwd())){
    				return true;
    			}
    		}  		
    	}catch (Exception e) {
    		Rlog.fatal("user", "Exception in method checkPortalLoginPwdRotation (PortalAgentBean) " + e);
		}
		return false;
    }
}   