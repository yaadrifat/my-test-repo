/*
 * Classname			SpecimenStatusAgentBean
 *
 * Version information :
 *
 * Date					10/15/2007
 *
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.specimenStatusAgent.impl;

/* IMPORT STATEMENTS */

import java.util.Hashtable;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.specimenStatus.impl.SpecimenStatusBean;
import com.velos.eres.service.specimenStatusAgent.SpecimenStatusAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.common.SpecimenStatusDao;
import com.velos.eres.business.common.CommonDAO;


/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP.
 *
 * @author Jnanamay Majumdar
 * @version 1.0, 10/15/2007
 * @ejbHome SpecimenStatusAgentHome
 * @ejbRemote SpecimenStatusAgentRObj
 */

@Stateless
public class SpecimenStatusAgentBean implements SpecimenStatusAgentRObj {

    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    
    @Resource
    private SessionContext context;

    public SpecimenStatusBean getSpecimenStatusDetails(int  pkSpecStat) {
    	SpecimenStatusBean ap = null;
        try {

            ap = (SpecimenStatusBean) em.find(SpecimenStatusBean.class, new Integer(pkSpecStat));
            CommonDAO cdao = new CommonDAO();
            cdao.populateClob("ER_SPECIMEN_STATUS","SS_NOTES"," where PK_SPECIMEN_STATUS = " + pkSpecStat );
            ap.setSsNotes(cdao.getClobData());

        } catch (Exception e) {
            Rlog.fatal("specimenStatus",
                    "Error in getSpecimenStatusDetails() in SpecimenStatusAgentBean" + e);
        }

        return ap;
    }

    public int setSpecimenStatusDetails(SpecimenStatusBean admin) {

    	SpecimenStatusBean ad = new SpecimenStatusBean();
    	int rowfound = 0;

    	try {
    		Rlog.debug("specimenStatus", "in try bloc of specimenStatusAgent");

    		ad.updateSpecimenStatus(admin);
    		em.persist(ad);
    		return ad.getPkSpecimenStat();
    	} catch (Exception e) {
    		Rlog.fatal("specimenStatus",
    				"Error in setSpecimenStatusDetails() in SpecimenStatusAgentBean " + e);
    	}
    	return 0;
    }

    public int updateSpecimenStatus(SpecimenStatusBean ap) {
    	SpecimenStatusBean adm = null;
    	int rowfound = 0;
        int output;
        try {
            adm = (SpecimenStatusBean) em.find(SpecimenStatusBean.class, new Integer(ap.getPkSpecimenStat()));
            if (adm == null) {
                return -2;
            }
            output = adm.updateSpecimenStatus(ap);
            em.merge(adm);
        } catch (Exception e) {
            Rlog.debug("specimenStatus", "Error in updateSpecimenStatus in SpecimenStatusAgentBean" + e);
            return -2;
        }
        return output;
    }


    public SpecimenStatusDao getSpecimenStausValues(int specId) {

    	try {
    		SpecimenStatusDao specStatDao = new SpecimenStatusDao();

    		specStatDao.getSpecimenStausValues(specId);
            return specStatDao;

        } catch (Exception e) {
            Rlog.fatal("specimenStatus", "Exception In getSpecimenStausValues in SpecimenStatusAgentBean " + e);
        }
        return null;


    }

  //JM: #INV 2.29 added method
    public SpecimenStatusDao getSpecimenStausProcessValues(int specId) {

    	try {
    		SpecimenStatusDao specStatDao = new SpecimenStatusDao();

    		specStatDao.getSpecimenStausProcessValues(specId);
            return specStatDao;

        } catch (Exception e) {
            Rlog.fatal("specimenStatus", "Exception In getSpecimenStausProcessValues in SpecimenStatusAgentBean " + e);
        }
        return null;


    }

    //Added by Manimaran for deletion of specimen status
    public int delSpecimenStatus(int pkSpecimenStat) {
    	int ret = 0;
    	try {
    		SpecimenStatusDao specStatDao = new SpecimenStatusDao();
    		ret = specStatDao.delSpecimenStatus(pkSpecimenStat);

    	} catch (Exception e) {
    		Rlog.fatal("specimenStatus", "Exception In delSpecimenStatus in SpeciemenStatusAgentBean " + e);
    		return -1;
    	}
    	return ret;


    }

    // Overloaded for INF-18183 ::: AGodara
    public int delSpecimenStatus(int pkSpecimenStat,Hashtable<String, String> auditInfo) {
    	
    	int ret = 0;
    	AuditBean audit=null;
    	try {
    		
        	String currdate =DateUtil.getCurrentDate();
        	String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); //Fetches the User ID from the Hashtable
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); //Fetches the IP Address from the Hashtable
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); //Fetches the reason for deletion from the Hashtable
            String app_module=(String)auditInfo.get(AuditUtils.APP_MODULE);// Fetches the APP_Module from the Hashtable
            String rid= AuditUtils.getRidValue("ER_SPECIMEN_STATUS","eres","pk_specimen_status="+pkSpecimenStat); //Fetches the RID
            audit = new AuditBean("ER_SPECIMEN_STATUS",String.valueOf(pkSpecimenStat),rid,userID,currdate,app_module,ipAdd,reason);
        	em.persist(audit); //PERSIST THE DATA IN AUDIT TABLE
        	
    		SpecimenStatusDao specStatDao = new SpecimenStatusDao();
    		ret = specStatDao.delSpecimenStatus(pkSpecimenStat);
    		if(ret==-1){
    			context.setRollbackOnly();                
    		}	
    	} catch (Exception e) {
    		context.setRollbackOnly();
    		Rlog.fatal("specimenStatus", "Exception In delSpecimenStatus in SpeciemenStatusAgentBean " + e);
    		return -1;
    	}
    	return ret;
}
    public SpecimenStatusDao getLatestSpecimenStaus(int specimenId) {

    	try {
    		SpecimenStatusDao specStatDao = new SpecimenStatusDao();

    		specStatDao.getLatestSpecimenStaus(specimenId);
            return specStatDao;

        } catch (Exception e) {
            Rlog.fatal("specimenStatus", "Exception In getLatestSpecimenStaus in SpecimenStatusAgentBean " + e);
        }
        return null;


    }
    // Ankit: Bug#9971,  Date# 02-July-2012
    public int updateSpecimenProcessType(SpecimenStatusBean ap) {
    	SpecimenStatusBean adm = null;
    	int rowfound = 0;
        int output = 0;
        try {
            adm = (SpecimenStatusBean) em.find(SpecimenStatusBean.class, new Integer(ap.getPkSpecimenStat()));
            if (adm == null) {
                return -2;
            }
            adm.setSsProcType(ap.getSsProcType());
            adm.setSsProcDate(ap.getSsProcDate());
            em.merge(adm);
        } catch (Exception e) {
            Rlog.debug("specimenStatus", "Error in updateSpecimenStatus in SpecimenStatusAgentBean" + e);
            output = -2;
        }
        return output;
    }


}
