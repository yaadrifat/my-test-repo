/*
 * Classname : StudyNIHGrantAgentBean
 *
 * Version information: 1.0
 *
 * Date: 11/22/2011
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Ashu Khatkar
 */
package com.velos.eres.service.studyNIHGrantAgent.impl;

/* Import Statements */
import java.util.Hashtable;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.common.StudyNIHGrantDao;
import com.velos.eres.business.studyNIHGrant.impl.StudyNIHGrantBean;
import com.velos.eres.service.studyNIHGrantAgent.StudyNIHGrantAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;

/**
 * The stateless session EJB acting as a facade for the entity CMP -
 * StudyNIHGrantAgentBean.<br>
 * <br>
 *
 * @author Ashu Khatkar
 * @see StudyNIHGrantAgentBean
 * @ejbRemote StudyNIHGrantAgentRObj
 */

@Stateless
public class StudyNIHGrantAgentBean  implements StudyNIHGrantAgentRObj{
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
	@Resource private SessionContext context;
	
	/**
    *
    * Looks up on the id parameter passed in and constructs and returns a
    * StudyNIHGrant state keeper. containing all the details of the StudyNIHGrant <br>
    *
    * @param study Id
    */

   public StudyNIHGrantBean getStudyNIHGrantInfo(int studyId) {
	   StudyNIHGrantBean retrieved = null;

       try {
           retrieved = (StudyNIHGrantBean) em.find(StudyNIHGrantBean.class,
                   new Integer(studyId));

       } catch (Exception e) {
           Rlog.fatal("StudyNIHGrant",
                   "Exception in StudyNIHGrantStateKeeper() in StudyNIHGrantAgentBean"
                           + e);
       }

       return retrieved;

   }

   /**
    * Creates StudyNIHGrant record.
    *
    * @param a
    *            State Keeper containing the studyNihGrantId attributes to be set.
    * @return int for successful:0; e\Exception : -2
    */

   public int setStudyNIHGrantInfo(StudyNIHGrantBean ssk) {
       try {
    	   StudyNIHGrantBean stdNIHGrantBean=new StudyNIHGrantBean();
    	   stdNIHGrantBean.updateStudyNIHGrant(ssk);
            em.persist(stdNIHGrantBean);
            return stdNIHGrantBean.getStudyNIHGrantId();
       } catch (Exception e) {
           Rlog
                   .fatal("studyNIHGrant",
                           "Error in setStudyNIHGrantInfo() in StudyNIHGrantAgentBean "
                                   + e);
           return -2;
       }

   }
   /**
    * return StudyNIHGrantDao
    */
   
   public StudyNIHGrantDao getStudyNIHGrantRecords(int fkStudyId) {
       try {
       	StudyNIHGrantDao studyNIHGrantDao = new StudyNIHGrantDao();
       	
       	studyNIHGrantDao.getStudyNIHGrantRecords(fkStudyId);
       	
           return studyNIHGrantDao;
       } catch (Exception e) {
           Rlog.fatal("studyNIHGrant", "Exception in getStudyNIHGrantRecords in StudyNIHGrantAgentBean " + e);
       }
       return null;
   }
   /**
    * @param StudyNIHGrantBean
    *  return updatedNIHGrantID
    */
   
   public int updateStudyNIHGrant(StudyNIHGrantBean as) {
	   StudyNIHGrantBean asm = null;
   	int rowfound = 0;
       int output;
       try {
    	   asm = (StudyNIHGrantBean) em.find(StudyNIHGrantBean.class, new Integer(
    			   as.getStudyNIHGrantId()));
           if (asm == null) {
               return -2;
           }
           output = asm.updateStudyNIHGrant(as);
           em.merge(asm);
       } catch (Exception e) {
           Rlog.debug("StudyNIHGrant", "Error in updateStudyNIHGrant in StudyNIHGrantAgentBean"
                   + e);
           return -2;
       }
       return output;
   }
   
   public int deleteStudyNIHGrantRecord(int studyNIHGrantId ,Hashtable<String, String> argsvalues) {

	   StudyNIHGrantBean retrieved = null;

       int output = 0;

       try {

           retrieved = (StudyNIHGrantBean) em.find(StudyNIHGrantBean.class, new Integer(
        		   studyNIHGrantId));

           if (retrieved == null) {
               return -2;
           }
           
           AuditBean audit= null;
           String currdate =DateUtil.getCurrentDate();
           String userID=(String)argsvalues.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
           String ipAdd=(String)argsvalues.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
           String reason=(String)argsvalues.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
           String moduleName = (String)argsvalues.get(AuditUtils.APP_MODULE);
           String getRidValue= AuditUtils.getRidValue("ER_STUDY_NIH_GRANT","eres","PK_STUDY_NIH_GRANT="+studyNIHGrantId);/*Fetches the RID/PK_VALUE*/ 
           audit = new AuditBean("ER_STUDY_NIH_GRANT",String.valueOf(studyNIHGrantId),getRidValue,
       	      userID,currdate,moduleName,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
           
        	em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
            
           em.remove(retrieved);
           
       } catch (Exception e) {
    	   context.setRollbackOnly();
           Rlog.debug("StudyNIHGrant", "Exception in deleteStudyNIHGrantRecord in StudyNIHGrantAgentBean"
                   + e);
           output = -2;
       }
      return output;
   }


   
   
}
