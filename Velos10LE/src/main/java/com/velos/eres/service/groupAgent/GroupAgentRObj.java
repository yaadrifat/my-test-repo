/*
 * Classname			GroupAgentRObj.class
 *
 * Version information   1.0
 *
 * Date					02/25/2001
 *
 * Copyright notice     Velos Inc.
 */

package com.velos.eres.service.groupAgent;

import javax.ejb.Remote;

import com.velos.eres.business.common.GroupDao;
import com.velos.eres.business.group.impl.GroupBean;
import com.velos.eres.service.groupAgent.impl.GroupAgentBean;

/**
 * Remote interface for GroupAgent session EJB
 *
 * @author sajal
 * @version 1.0 02/25/2001
 * @see GroupAgentBean
 */

@Remote
public interface GroupAgentRObj {
    /**
     * gets the group details
     */
    GroupBean getGroupDetails(int groupId);

    /**
     * sets the group details
     */
    public int setGroupDetails(GroupBean group);

    public int updateGroup(GroupBean gsk);

    public GroupDao getByAccountId(int accId);

    public int getCount(String groupName, String accId);

    public GroupDao getGroupDaoInstance();

    public String getDefaultStudySuperUserRights(String p_user);

    public int copyRoleDefRightsToGrpSupUserRights(int roleId, int grpId);
}
