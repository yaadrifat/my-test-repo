/*
 * Classname : FieldLibAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 03/07/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: SoniaKaura
 */

package com.velos.eres.service.fieldLibAgent;

/* Import Statements */

import javax.ejb.Remote;

import com.velos.eres.business.common.FieldLibDao;
import com.velos.eres.business.common.FldRespDao;
import com.velos.eres.business.common.LookupDao;
import com.velos.eres.business.fieldLib.impl.FieldLibBean;

/* End of Import Statements */

/**
 * Remote interface for FieldLibAgent session EJB
 * 
 * @author SoniaKaura
 * @version : 1.0 03/07/2003
 */

@Remote
public interface FieldLibAgentRObj {
    /**
     * gets the FieldLib record
     */
    FieldLibBean getFieldLibDetails(int fieldLibId);

    /**
     * creates a new FieldLib Record
     */
    public int setFieldLibDetails(FieldLibBean fieldLsk);

    /**
     * creates a new FieldLib Record and inserts multiple responses
     */
    public int setFieldLibDetails(FieldLibBean fieldLsk, FldRespDao fldRespDao);

    /**
     * updates the FieldLib Record
     */
    public int updateFieldLib(FieldLibBean fieldLsk);

    /**
     * remove FieldLib Record
     */
    public int removeFieldLib(int fieldLibId);

    /**
     * updates FieldLib records
     */
    public int updateFieldLibRecord(int pkField, String libFlag, String user,
            String ipAdd);

    /**
     * get the DAO from the table
     * 
     */

    public FieldLibDao getFieldsFromSearch(int fkCatlib, int fkAccount,
            int fkFormId, String name, String keyword);

    /**
     * Get the DAO from jsp and send it to the session bean using JB
     */

    public int insertToFormField(FieldLibBean fieldLsk);

    /**
     * Get the DAO from jsp and send it to the session bean using JB
     */

    public int updateToFormField(FieldLibBean fieldLsk);

    /**
     * Get the int from jsp and send it to the session bean using JB
     */
    public int copyMultipleFields(String secId, String[] idArray,
            String creator, String ipAdd);

    /**
     * Get ids and from jsp and send it to the session bean using JB
     */

    public FieldLibDao getFieldNames(int formId);

    public FieldLibDao getFltrdFieldNames(int formId);

    public FieldLibDao getFieldsInformation(int formId);

    public int updateBrowserFlag(String browserOne, String browserZero,
            int lastModBy, String recordType, String ipAdd);

    /**
     * get the DAO from the table
     * 
     */

    public FieldLibDao getFieldsForCopy(int fkCatlib, int fkAccount,
            String name, String keyword);

    public FieldLibDao getFieldNamesForSection(int formId, int sectionId,
            String fldType);

    public LookupDao getAllLookpUpViews(String account);

    public LookupDao getLookpUpViewColumns(String viewId);

    public FieldLibDao getEditMultipleFields(int formId);
    
    /**
     * returns true if field as any inter field action associated with the field
     * 
     * @param fieldId
     * 
     *            
     */

    public boolean hasInterFieldActions(String fieldId) ; 


}
