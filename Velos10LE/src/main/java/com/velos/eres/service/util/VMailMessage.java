package com.velos.eres.service.util;

import java.util.Date;

/**
 * Class: com.velos.eres.service.util.VMailMessage Author: Sonia Sahni Date :
 * 02/27/04 Purpose : StateKeeper patter to hold mail message information
 * 
 */

public class VMailMessage {

    private String messageFrom;

    private String messageFromDescription;

    private String messageTo;

    private String messageCC;

    private String messageBCC;

    private String messageSubject;

    private Date messageSentDate;

    private String messageText;

    public String getMessageFrom() {
        return this.messageFrom;
    }

    public void setMessageFrom(String messageFrom) {
        this.messageFrom = messageFrom;
    }

    public String getMessageFromDescription() {
        return this.messageFromDescription;
    }

    public void setMessageFromDescription(String messageFromDescription) {
        this.messageFromDescription = messageFromDescription;
    }

    public Date getMessageSentDate() {
        return this.messageSentDate;
    }

    public void setMessageSentDate(Date messageSentDate) {
        this.messageSentDate = messageSentDate;
    }

    public String getMessageSubject() {
        return this.messageSubject;
    }

    public void setMessageSubject(String messageSubject) {
        this.messageSubject = messageSubject;
    }

    public String getMessageText() {
        return this.messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getMessageTo() {
        return this.messageTo;
    }

    public void setMessageTo(String messageTo) {
        this.messageTo = messageTo;
    }

    public String getMessageCC() {
        return this.messageCC;
    }

    public void setMessageCC(String messageCC) {
        this.messageCC = messageCC;
    }

    public String getMessageBCC() {
        return this.messageBCC;
    }

    public void setMessageBCC(String messageBCC) {
        this.messageBCC = messageBCC;
    }
}
