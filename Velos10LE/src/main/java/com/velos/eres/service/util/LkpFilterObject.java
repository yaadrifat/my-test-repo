package com.velos.eres.service.util;

public class LkpFilterObject {
    private String lkpFilterKey = null;
    private String lkpFilterSQL = null;
    private String lkpFilterParamType = null;
    
    public LkpFilterObject(String lkpFilterKey, String lkpFilterSQL, String lkpFilterParamType) {
        this.lkpFilterKey = lkpFilterKey;
        this.lkpFilterSQL = lkpFilterSQL;
        this.lkpFilterParamType = lkpFilterParamType;
    }
    
    public String getLkpFilterKey() {
        return lkpFilterKey;
    }

    public String getLkpFilterSQL() {
        return lkpFilterSQL;
    }

    public String getLkpFilterParamType() {
        return lkpFilterParamType;
    }

}
