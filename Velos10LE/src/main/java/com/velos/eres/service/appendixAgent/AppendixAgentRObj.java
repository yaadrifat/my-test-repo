/*
 * Classname			AppendixAgent.class
 * 
 * Version information   
 *
 * Date					03/03/2001
 * 
 * Copyright notice
 */
package com.velos.eres.service.appendixAgent;
import java.util.Hashtable;
import javax.ejb.Remote;
import com.velos.eres.business.appendix.impl.AppendixBean;
import com.velos.eres.business.common.AppendixDao;
/**
 * 
 * 
 * Remote interface for AppendixAgent session EJB
 * 
 * 
 * @author
 * 
 * 
 */
@Remote
public interface AppendixAgentRObj
{
    /**
     * 
     * 
     * gets the Appendix details
     * 
     * 
     */
    AppendixBean getAppendixDetails(int id);
    /**
     * 
     * 
     * sets the Appendix details
     * 
     * 
     */
    public int setAppendixDetails(AppendixBean ask);
    public int removeAppendix(int id);
    // Overloaded for INF-18183 ::: AGodara
    public int removeAppendix(int id,Hashtable<String, String> auditInfo);
    public int updateAppendix(AppendixBean appsk);
    public AppendixDao getByStudyVerId(int studyVerId);
    public AppendixDao getByStudyIdAndType(int studyVerId, String type);
    public AppendixDao getAppendixValuesPublic(int studyVerId);
    /** gets latest document for version category */
    public AppendixDao getLatestDocumentForCategory(String studyId,String versionCategory);
    
}
