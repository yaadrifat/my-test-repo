/*
 * Classname			StudyAgentBean.class
 * 
 * Version information
 *
 * Date					02/16/2001
 * 
 * Copyright notice
 */

package com.velos.eres.service.studyAgent.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Hashtable;
import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.json.JSONArray;
import org.json.JSONObject;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.LookupDao;
import com.velos.eres.business.common.PatStudyStatDao;
import com.velos.eres.business.common.StudyDao;
import com.velos.eres.business.study.UserStudiesStateKeeper;
import com.velos.eres.business.study.impl.StudyBean;
import com.velos.eres.business.studyStatus.impl.StudyStatusBean;
import com.velos.eres.service.studyAgent.StudyAgent;
import com.velos.eres.service.studyRightsAgent.StudyRightsAgentRObj;
import com.velos.eres.service.studyStatusAgent.StudyStatusAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.MC;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.esch.business.common.EventAssocDao;

/**
 * The stateless session EJB acting as a facade for the entity CMP.<br>
 * <br>
 */
/*
 * Modified by Sonia, 10/01/04, added checks for 'Active' and 'Permanent
 * Closure' study status in updateStudy. If even one 'Active/Enrolling' study
 * status exists the 'Actual Study Start Date' cannot be set to empty and if
 * even one 'Permanent Closure' study status exists the 'Study End Date' cannot
 * be set to empty.
 */
@Stateless
@Remote( { StudyAgent.class })
public class StudyAgentBean implements StudyAgent {

    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    @Resource private SessionContext context;

    private static final String STUDYID_KEY = "fk_study";
    private static final String STUDYNUMBER_KEY = "studyNumber";
    private static final String QUICKACCESS_KEY = "quickAccess";
    private static final String STUDYTITLE_KEY = "studyTitle";
    private static final String STUDYSTATUS_KEY = "studyStatus";
    private static final String STUDYICON_KEY = "studyIcon";
    private static final String PATIENTICON_KEY = "patientIcon";
    private static final String BUDGETICON_KEY = "budgetIcon";
    private static final String CALENDARICON_KEY = "calendarIcon";

    /**
     * Looks up on the Study number parameter passed in and constructs and
     * returns a state holder. containing all the summary details of the study<br>
     * 
     * @param resNum
     *            the reservation number
     */
    public StudyBean getStudyDetails(int id) {
        if (id == 0) {
            return null;
        }

        StudyBean studyBean = null;

        try {
            studyBean = (StudyBean) em.find(StudyBean.class, new Integer(id));
            
            //JM: 28Sep2006
            
            CommonDAO cObjDao = new CommonDAO();       
            CommonDAO cSumDao = new CommonDAO(); 
            
            cObjDao.populateClob("er_study","study_obj_clob"," where pk_study = " + id );
            studyBean.setStudyObjective(cObjDao.getClobData());
            
            cSumDao.populateClob("er_study","study_sum_clob"," where pk_study = " + id );
            studyBean.setStudySummary(cSumDao.getClobData());
            
            ////
            
            return studyBean;
        } catch (Exception e) {
            // e.printStackTrace();
            Rlog.fatal("study", "EXCEPTION IN GETTING STUDY DATA" + e);
            return studyBean;
        }

    }

    /**
     * Sets a Study.
     * 
     * @param study
     *            a state holder containing study attributes to be set.
     * @return String
     */

    public int setStudyDetails(StudyBean study) {
        int studyId = 0;
        CallableStatement cstmt = null;
        Connection conn = null;
        int usr = StringUtil.stringToNum(study.getCreator());
        String ip = study.getIpAdd();
        int rowfound = 0;
        StudyBean newStudyBean = new StudyBean();

        Rlog.debug("study", "in setStudyDetails Agent bean usr and ip " + usr
                + " " + ip);
        try {
            Rlog.debug("study", "in try bloc of study agent");

            try {

                Query querySiteIdentifier = em.createNamedQuery("findStudyNum");
                querySiteIdentifier.setParameter("accountId", study
                        .getAccount());
                querySiteIdentifier.setParameter("studyNum", (study
                        .getStudyNumber()).toUpperCase());
                ArrayList list = (ArrayList) querySiteIdentifier
                        .getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    rowfound = 1;
                    return -3; // study number exists
                }

            } catch (Exception ex) {
                Rlog.fatal("study",
                        "In setStudyDetails call to  FIND STUDY NUM EXCEPTION"
                                + ex);
                rowfound = 0; // study number does not exist
            }

            // Study s = studyHome.create(study);

            newStudyBean.updateStudy(study);
            em.persist(newStudyBean);
            studyId = newStudyBean.getId();

            // insert a default verion in table er_studyver for every new study

            return studyId;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * sets up a state holder containing details of the studies on which user
     * has rights
     */
    public UserStudiesStateKeeper getUserStudies(int usrId) {
        StudyBean stdRobj = null; // Study Entity Bean Remote Object
        UserStudiesStateKeeper usk = new UserStudiesStateKeeper();
        ArrayList enum_velos;
        String studyNo, title;
        int studyId = 0;

        try {

            // enum_velos = studyHome.findByUser(usrId);
            Query querySiteIdentifier = em.createNamedQuery("findByUser");
            querySiteIdentifier.setParameter("userId", usrId);
            enum_velos = (ArrayList) querySiteIdentifier.getResultList();

            // while (enum_velos.hasMoreElements()) {
            for (int i = 0; i < enum_velos.size(); i++) {
                stdRobj = (StudyBean) enum_velos.get(i);
                title = stdRobj.getStudyTitle();
                studyNo = stdRobj.getStudyNumber();
                studyId = stdRobj.getId();
                usk.setUserStudyDetails(studyId, title, studyNo);
            }
            Rlog.debug("study", "SSK VALUE" + usk);
        } catch (Exception e) {
            Rlog.fatal("study", "EXCEPTION IN GETTING USER STUDIES DATA" + e);
        }
        Rlog.debug("study", "RETURNING FROM SESSION BEAN" + usk);
        return usk;
    }

    /**
     * Calls updateStudy() on Entity Bean StudyBean and updates a Study.
     * 
     * @param study
     *            A StudyStateKeeper object with Study details
     * @return success flag:
     *         <ul>
     *         <li> 0 if successful</li>
     *         <li> -3 : failure - if study number is not unique </li>
     *         <li> -4 : failure - if study actual start date is set to empty
     *         and there is an existing 'Active' study status </li>
     *         <li> -5 : failure - if study end date is set to empty and there
     *         is an existing 'Permanent Closure' study status </li>
     *         <li> -2 in case of any other failure</li>
     *         </ul>
     * @see com.velos.esch.business.study.StudyBean
     */
    public int updateStudy(StudyBean study) {

        StudyBean studyBean = null; // User Entity Bean Remote Object

        String newStudyActBeginDate = null;
        String newStudyEndDate = null;
        String oldStudyActBeginDate = null;
        String oldStudyEndDate = null;

        int output;
        int rowfound = 0;

        try {

            // pkstudy = new StudyPK(study.getId());
            // StudyHome studyHome = EJBUtil.getStudyHome();

            // retrieved = studyHome.findByPrimaryKey(pkstudy);
            studyBean = (StudyBean) em.find(StudyBean.class, study.getId());
            if (studyBean == null) {
                return -2;
            }

            if (!((studyBean.getStudyNumber()).toUpperCase()).equals((study
                    .getStudyNumber()).toUpperCase())) {
                // study number has changed

                try {
                    // Study ret;
                    // ret =
                    // studyHome.findStudyNum(EJBUtil.stringToNum(study.getAccount()),
                    // (study.getStudyNumber()).toUpperCase());
                    Query querySiteIdentifier = em
                            .createNamedQuery("findStudyNum");
                    querySiteIdentifier.setParameter("accountId", study
                            .getAccount());
                    querySiteIdentifier.setParameter("studyNum", (study
                            .getStudyNumber()).toUpperCase());
                    ArrayList list = (ArrayList) querySiteIdentifier
                            .getResultList();
                    if (list == null)
                        list = new ArrayList();
                    if (list.size() > 0) {
                        rowfound = 1;
                        return -3; // study number exists
                    }

                } catch (Exception ex) {
                    Rlog.fatal("study",
                            "In updateStudyDetails call to  FIND STUDY NUM EXCEPTION"
                                    + ex);
                    rowfound = 0; // study number does not exist
                }

            } // end check if study number is changed

            // check if study start and end dates can be set to empty

            oldStudyActBeginDate = studyBean.getStudyActBeginDate();
            oldStudyEndDate = studyBean.getStudyEndDate();

            newStudyActBeginDate = study.getStudyActBeginDate();
            newStudyEndDate = study.getStudyEndDate();

            // if actual begin date is changed to empty
            if (StringUtil.isEmpty(newStudyActBeginDate)
                    && (!oldStudyActBeginDate.equals(newStudyActBeginDate))) {
                Rlog.debug("study", "updateStudy:studyActBeginDate is changed");

                // check if there is atleast one 'Active' status for the study,
                // if it is,
                // the actual begin date can not be changed to empty

                try {
                    StudyStatusAgentRObj studyStatROBJ = getStudyStatusAgentHome();
                    StudyStatusBean statBean = studyStatROBJ
                            .findByAtLeastOneStatus(study.getId(), "active");
                    if (statBean != null) {
                        // Found an active status, so studyActBeginDate can not
                        // be set to empty
                        return -4;
                    }

                }
                // no active status found, so no problem, study actual start
                // date can be updated
                catch (Exception e) {
                    Rlog.fatal("study",
                            "Exception in updateStudy in Study AgentBean" + e);
                    return -2;
                }

            }

            if (StringUtil.isEmpty(newStudyEndDate)
                    && (!oldStudyEndDate.equals(newStudyEndDate))) {
                Rlog.debug("study", "updateStudy:studyEndDate is changed");

                // check if there is atleast one 'Permanent Closure' status for
                // the study, if it is,
                // the end date can not be changed to empty

                try {
                    StudyStatusAgentRObj studyStatROBJ = getStudyStatusAgentHome();
                    StudyStatusBean statBean = studyStatROBJ
                            .findByAtLeastOneStatus(study.getId(), "prmnt_cls");
                    if (statBean != null) {
                        // Found a permanent Closure Status, so studyEndDate can
                        // not be set to empty
                        return -5;
                    }

                } catch (Exception e) {
                    Rlog.fatal("study",
                            "Exception in updateStudy in Study AgentBean" + e);
                    return -2;
                }

            }

            output = studyBean.updateStudy(study);
            em.merge(study);

        } catch (Exception e) {
            Rlog.fatal("study", "Exception in updateStudy in Study AgentBean"
                    + e);
            return -2;
        }
        return output;
    }

    public StudyDao getStudyValuesKeyword(String search) {
        try {
            Rlog.debug("study",
                    "In getStudyValuesKeyword in StudyAgentBean line number 0");
            StudyDao studyDao = new StudyDao();
            Rlog.debug("study",
                    "In getStudyValuesKeyword in StudyAgentBean line number 1");
            studyDao.getStudyValuesKeyword(search);
            Rlog.debug("study",
                    "In getStudyValuesKeyword in StudyAgentBean line number 2");
            return studyDao;
        } catch (Exception e) {
            Rlog
                    .fatal("study",
                            "Exception In getStudyValuesKeyword in StudyAgentBean "
                                    + e);
        }
        return null;

    }

    public StudyDao getStudyValuesKeywordAll(ArrayList search) {
        try {
            Rlog
                    .debug("study",
                            "In getStudyValuesKeywordAll in StudyAgentBean line number 0");
            StudyDao studyDao = new StudyDao();
            Rlog
                    .debug("study",
                            "In getStudyValuesKeywordAll in StudyAgentBean line number 1");
            studyDao.getStudyValuesKeywordAll(search);
            Rlog
                    .debug("study",
                            "In getStudyValuesKeywordAll in StudyAgentBean line number 2");
            return studyDao;
        } catch (Exception e) {
            Rlog.fatal("study",
                    "Exception In getStudyValuesKeywordAll in StudyAgentBean "
                            + e);
        }
        return null;

    }

    public StudyDao getStudyValuesAuthor(String search) {
        try {
            Rlog.debug("study",
                    "In getStudyValuesAuthor in StudyAgentBean line number 0");
            StudyDao studyDao = new StudyDao();
            Rlog.debug("study",
                    "In getStudyValuesAuthor in StudyAgentBean line number 1");
            studyDao.getStudyValuesAuthor(search);
            Rlog.debug("study",
                    "In getStudyValuesAuthor in StudyAgentBean line number 2");
            return studyDao;
        } catch (Exception e) {
            Rlog.fatal("study",
                    "Exception In getStudyValuesAuthor in StudyAgentBean " + e);
        }
        return null;

    }

    public StudyDao getStudyValuesTitle(String search) {
        try {
            Rlog.debug("study",
                    "In getStudyValuesTitle in StudyAgentBean line number 0");
            StudyDao studyDao = new StudyDao();
            Rlog.debug("study",
                    "In getStudyValuesTitle in StudyAgentBean line number 1");
            studyDao.getStudyValuesTitle(search);
            Rlog.debug("study",
                    "In getStudyValuesTitle in StudyAgentBean line number 2");
            return studyDao;
        } catch (Exception e) {
            Rlog.fatal("study",
                    "Exception In getStudyValuesTitle in StudyAgentBean " + e);
        }
        return null;

    }

    public StudyDao getStudyValuesTarea(String search) {
        try {
            Rlog.debug("study",
                    "In getStudyValuesTarea in StudyAgentBean line number 0");
            StudyDao studyDao = new StudyDao();
            Rlog.debug("study",
                    "In getStudyValuesTarea in StudyAgentBean line number 1");
            studyDao.getStudyValuesTarea(search);
            Rlog.debug("study",
                    "In getStudyValuesTarea in StudyAgentBean line number 2");
            return studyDao;
        } catch (Exception e) {
            Rlog.fatal("study",
                    "Exception In getStudyValuesTarea in StudyAgentBean " + e);
        }
        return null;

    }

    public StudyDao getStudyValuesForUsers(String userId) {
        try {
            Rlog
                    .debug("study",
                            "In getStudyValuesForUsers in StudyAgentBean line number 0");
            StudyDao studyDao = new StudyDao();
            Rlog
                    .debug("study",
                            "In getStudyValuesForUsers in StudyAgentBean line number 1");
            studyDao.getStudyValuesForUsers(userId);
            Rlog
                    .debug("study",
                            "In getStudyValuesForUsers in StudyAgentBean line number 2");
            return studyDao;
        } catch (Exception e) {
            Rlog.fatal("study",
                    "Exception In getStudyValuesForUsers in StudyAgentBean "
                            + e);
        }
        return null;

    }

    // /overloaded function

    public StudyDao getStudyValuesForUsers(String userId, String studycount) {
        try {
            Rlog
                    .debug("study",
                            "In getStudyValuesForUsers in StudyAgentBean line number 0");
            StudyDao studyDao = new StudyDao();
            Rlog
                    .debug("study",
                            "In getStudyValuesForUsers in StudyAgentBean line number 1");
            studyDao.getStudyValuesForUsers(userId, studycount);
            Rlog
                    .debug("study",
                            "In getStudyValuesForUsers in StudyAgentBean line number 2");
            return studyDao;
        } catch (Exception e) {
            Rlog.fatal("study",
                    "Exception In getStudyValuesForUsers in StudyAgentBean "
                            + e);
        }
        return null;

    }

    public JSONArray getStudyValuesForGadget(String userId, String studyIds) {
    	JSONArray myStudiesData = new JSONArray();
        try {
            StudyDao studyDao = new StudyDao();
            if (StringUtil.isEmpty(studyIds)){
            	studyDao.getStudyValuesForUsers(userId, "5");
            } else {
            	studyDao.getSelectedStudyValuesForUsers(userId, studyIds);

            	StudyDao tempStudyDao = new StudyDao();
            	tempStudyDao = studyDao.reorderStudies(studyIds);
            	studyDao = new StudyDao();
            	studyDao = tempStudyDao;
            }
            
            ArrayList arrStudyIds = new ArrayList();
            arrStudyIds = studyDao.getStudyIds();
            
            for (int iX=0; iX < arrStudyIds.size(); iX++) {		
    			String studyId = ""+arrStudyIds.get(iX);
    			
    			JSONObject aMyStudy = new JSONObject();
    			try{
    				aMyStudy.put(STUDYID_KEY, studyId);
    				aMyStudy.put(STUDYNUMBER_KEY, String.valueOf(studyDao.getStudyNumbers().get(iX)));
    				aMyStudy.put(QUICKACCESS_KEY, "1");

    				aMyStudy.put(STUDYTITLE_KEY, String.valueOf(studyDao.getStudyTitles().get(iX)));
    				aMyStudy.put(STUDYSTATUS_KEY, String.valueOf(studyDao.getStudyStatuses().get(iX)));
    				String studyStatusSubtype = (String)studyDao.getStudySubtypes().get(iX);
    		    	String hyperLink ="";
    		    	String studyStatusNote = (String)studyDao.getStudyStatNotes().get(iX);
    		    	studyStatusNote = (null == studyStatusNote) ? "-" : studyStatusNote ;

    		    	String hyperLinkStart = "<A href=\"studystatusbrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=3&studyId="+studyId+"\" "
    		    	+ "onMouseOver=\"return overlib('<b>"+LC.L_Note+":</b> "+studyStatusNote+"<br>"
    		    	+ "<b>"+LC.L_Organization+":</b> "+studyDao.getStudySites().get(iX)+"',"
    		    	+ "CAPTION,'"+studyDao.getStudyStatDates().get(iX)+"');\" onMouseOut=\"return nd();\">";
    		    	
    		    	String hyperLinkEnd ="</A>";
    		    	
    		    	if (studyStatusSubtype.equals("temp_cls")) {
    		    		hyperLink = "<img src=\"../images/help.png\" border=\"0\"/>"
    		    			+ hyperLinkStart
    		    			+ "<FONT class=\"Red\">"
    		    			+ String.valueOf(studyDao.getStudyStatuses().get(iX))
    		    			+ "</FONT>"
    		    			+ hyperLinkEnd;
    				} else if (studyStatusSubtype.equals("active")) {
    					hyperLink = hyperLinkStart
		    			+ "<FONT class=\"Green\">"
		    			+ String.valueOf(studyDao.getStudyStatuses().get(iX))
		    			+ "</FONT>"
		    			+ hyperLinkEnd;
    				} else if(studyStatusSubtype!=null && studyStatusSubtype.equals("-")){
    					hyperLink = "<div align=\"center\">" 
    					+ "<A href=\"studystatusbrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=3&studyId="+studyId+"\" "
    					+ "onMouseOver=\"return overlib('"+ MC.M_Status_Cannot_Be_Displayed +"',CAPTION,'"+ LC.L_Study_Status +"');\""
    					+ "onMouseOut=\"return nd();\"><img src=\"../images/help.png\" border=\"0\"/>"
    					+ "</A></div>";										
    				}else{
    					hyperLink =hyperLinkStart
    		    			+ String.valueOf(studyDao.getStudyStatuses().get(iX))
    		    			+ hyperLinkEnd;
    					
    				}
    		    	aMyStudy.put(STUDYSTATUS_KEY, hyperLink);

    				aMyStudy.put(STUDYICON_KEY, "1");
    				
    				int patientAccess = 0;
    		    	StudyRightsAgentRObj srightsAgent = EJBUtil.getStudyRightsAgentHome();
    		    	patientAccess = srightsAgent.getStudyRightsForModule(StringUtil.stringToNum(studyId), 
    		    			StringUtil.stringToNum(userId), "STUDYMPAT");

		    		aMyStudy.put(PATIENTICON_KEY, "0");
    		    	if (StringUtil.isAccessibleFor(patientAccess, 'V')){
    		    		String aStudyActBeginDate = (String)studyDao.getStudyActBeginDates().get(iX);
    		    		if (null != aStudyActBeginDate && (!aStudyActBeginDate.equals("-"))){
    		    			aMyStudy.put(PATIENTICON_KEY, "1");
    		    		}
    		    	}
    		    	
    		    	aMyStudy.put(BUDGETICON_KEY, "1");
    		    	
    		    	EventAssocDao eventassocDao = new EventAssocDao();
    	            eventassocDao.getStudyProts(StringUtil.stringToNum(studyId));
    	            
    	            ArrayList eventIds = new ArrayList();
    	            eventIds = eventassocDao.getEvent_ids();
    	            
    	            aMyStudy.put(CALENDARICON_KEY, "0");

    		    	if (null != eventIds){
						if (eventIds.size() > 0){
							aMyStudy.put(CALENDARICON_KEY, "1");
						}
    		    	}
    		    } catch(Exception e) {
    				Rlog.fatal("myStudiesGadget", "aMyStudy.put error: " + e);
    			}
    			try {
    				myStudiesData.put(aMyStudy);
    			} catch(Exception e) {
    				Rlog.fatal("myStudiesGadget", "jArray.put error: " + e);
    			}
    		}
            return myStudiesData;
        } catch (Exception e) {
            Rlog.fatal("study",
                    "Exception In getStudyValuesJSONForUsers in StudyAgentBean "
                            + e);
        }
        return null;

    }

    public StudyDao verifyStudyRequest(String studyId, String userId) {
        try {
            Rlog.debug("study",
                    "In verifyStudyRequest in StudyAgentBean line number 0");
            StudyDao studyDao = new StudyDao();
            Rlog.debug("study",
                    "In verifyStudyRequest in StudyAgentBean line number 1");
            studyDao.verifyStudyRequest(studyId, userId);
            Rlog.debug("study",
                    "In verifyStudyRequest in StudyAgentBean line number 2");
            return studyDao;
        } catch (Exception e) {
            Rlog.fatal("study",
                    "Exception In verifyStudyRequest in StudyAgentBean " + e);
        }
        return null;

    }

    public int getStudyCount(String accountId) {
        int count = 0;
        try {
            Rlog.debug("study",
                    "In getStudyCount in StudyAgentBean line number 0");
            StudyDao studyDao = new StudyDao();
            Rlog.debug("study",
                    "In getStudyCount in StudyAgentBean line number 1");
            count = studyDao.getStudyCount(accountId);
            Rlog.debug("study",
                    "In getStudyCount in StudyAgentBean line number 2");
        } catch (Exception e) {
            Rlog.fatal("study", "Exception In getStudyCount in StudyAgentBean "
                    + e);
        }
        return count;

    }

    public void copyStudy(int studyId, int xUser, int author,
            String permission, int msgId) {
        try {
            Rlog.debug("study", "In copyStudy in StudyAgentBean line number 0");
            StudyDao studyDao = new StudyDao();
            Rlog.debug("study", "In copyStudy in StudyAgentBean line number 1");
            studyDao.copyStudy(studyId, xUser, author, permission, msgId);
            Rlog.debug("study", "In copyStudy in StudyAgentBean line number 2");
        } catch (Exception e) {
            Rlog
                    .fatal("study", "Exception In copyStudy in StudyAgentBean "
                            + e);
        }
    }

    public int createVersion(int studyId, int xUser) {
        int ret = 0;

        try {
            Rlog.debug("study",
                    "In createVersion in StudyAgentBean line number 0");
            StudyDao studyDao = new StudyDao();
            Rlog.debug("study",
                    "In createVersion in StudyAgentBean line number 1");
            ret = studyDao.createVersion(studyId, xUser);
            return ret;

        } catch (Exception e) {
            Rlog.fatal("study", "Exception In createVersion in StudyAgentBean "
                    + e);
            return -1;
        }
    }

    public StudyDao getUserStudies(String userId, String dName, int selVal,
            String flag) {
        try {
            Rlog.debug("study",
                    "In getUserStudies in StudyAgentBean line number 0");
            StudyDao studyDao = new StudyDao();
            Rlog.debug("study",
                    "In getUserStudies in StudyAgentBean line number 1");
            studyDao.getUserStudies(userId, dName, selVal, flag);
            return studyDao;

        } catch (Exception e) {
            Rlog.fatal("study",
                    "Exception In getUserStudies in StudyAgentBean " + e);
            return null;
        }
    }

    public PatStudyStatDao getStudyPatientResults(int studyId,
            String patientID, int patStatus, int user, int userSite) {
        try {
            Rlog
                    .debug("study",
                            "In getStudyPatientResults in StudyAgentBean line number 0");
            PatStudyStatDao pDao = new PatStudyStatDao();
            Rlog
                    .debug("study",
                            "In getStudyPatientResults in StudyAgentBean line number 1");
            pDao.getStudyPatientResults(studyId, patientID, patStatus, user,
                    userSite);
            Rlog
                    .debug("study",
                            "In getStudyPatientResults in StudyAgentBean line number 2");
            return pDao;
        } catch (Exception e) {
            Rlog.fatal("study",
                    "Exception In getStudyPatientResults in StudyAgentBean "
                            + e);
        }
        return null;

    }

    /**
     * Gets Patients associated with a study. Filters data according to the
     * parameters passed. If no study is <br>
     * passed, gets data for all the studies
     * 
     * @param studyId
     *            Study Id
     * @param patientID
     *            Patient Code
     * @param patStatus
     *            Patient current status in Study
     * @param user
     *            Logged in user id
     * @param userSite
     *            Organization id
     * @param enrolDt
     *            Enrollment date
     * @param lastDoneDt
     *            Last visit done on
     * @param nextVisitDate
     *            Next visit date
     * @param lastVisit
     *            last Visit Number
     * @param htMoreFilterParams
     *            A Hashtable object with more filter parameters.
     * @return A PatStudyStatDao object with patient enrollment data
     */

    // patStudyId is added by Manimaran on 21Apr2005 for Enrolled PatientStudyId
    // search
    // public PatStudyStatDao getStudyPatientResultsWithVisits(int studyId,
    // String patientID, int patStatus, int user, String userSite, String
    // enrolDt, String lastDoneDt, String nextVisitDate, String
    // lastVisit,Hashtable htMoreFilterParams)
    public PatStudyStatDao getStudyPatientResultsWithVisits(int studyId,
            String patientID, String patStudyId, int patStatus, int user,
            String userSite, String enrolDt, String lastDoneDt,
            String nextVisitDate, String lastVisit, Hashtable htMoreFilterParams) {
        try {
            PatStudyStatDao pDao = new PatStudyStatDao();
            pDao.getStudyPatientResultsWithVisits(studyId, patientID,
                    patStudyId, patStatus, user, userSite, enrolDt, lastDoneDt,
                    nextVisitDate, lastVisit, htMoreFilterParams);
            return pDao;

        } catch (Exception e) {
            Rlog.fatal("study",
                    "Exception In getStudyPatientResults in StudyAgentBean "
                            + e);
        }
        return null;

    }

    // /////////////////////////////////////////////////////////

    public int validateStudyNumber(String accId, String studyNum) {

         
        int rowfound = 0;
        ArrayList list = null;
        Query query = null;
        try {
            query = em.createNamedQuery("findStudyNum");
            query.setParameter("accountId", accId);
            query.setParameter("studyNum", studyNum.toUpperCase());

            list = (ArrayList) query.getResultList();
            if (list != null && list.size() > 0 )
                rowfound = list.size(); // study number exists


        } catch (Exception e) {
            e.printStackTrace();
            rowfound = -1;
        }

        return rowfound;

    }

    /*public int copyStudySelective(int origstudy, int statflag, int teamflag,
            String[] verarr, String[] calarr, String studynum, int studydm,
            String title, int userId, String ipAdd) {
        int retValue = 0;
        try {

            StudyDao studyDao = new StudyDao();
            retValue = studyDao.copyStudySelective(origstudy, statflag,
                    teamflag, verarr, calarr, studynum, studydm, title, userId,
                    ipAdd);
            return retValue;

        } catch (Exception e) {
            Rlog.fatal("study",
                    "Exception In copyStudySelective in StudyAgentBean " + e);
            return -1;
        }
    }*/

    //JM: 31July2006: Modified
    public int copyStudySelective(int origstudy, int statflag, int teamflag,int dictflag,
            String[] verarr, String[] calarr, String[] txarr, String[] formsarr, String studynum, int studydm,
            String title, int userId, String ipAdd) {    	
        int retValue = 0;
        try {

            StudyDao studyDao = new StudyDao();
            retValue = studyDao.copyStudySelective(origstudy, statflag,teamflag, dictflag,
                    verarr, calarr, txarr, formsarr, studynum, studydm, title, userId,
                    ipAdd);
            return retValue;

        } catch (Exception e) {
            Rlog.fatal("study",
                    "Exception In copyStudySelective in StudyAgentBean " + e);
            return -1;
        }
    }
    
    
    public PatStudyStatDao getPatientStudiesWithVisits(int studyId, int pkey,
            int patStatus, int user, int userSite, String enrolDt,
            String lastDoneDt, String nextVisitDate, String lastVisit) {
        try {
            PatStudyStatDao pDao = new PatStudyStatDao();
            pDao.getPatientStudiesWithVisits(studyId, pkey, patStatus, user,
                    userSite, enrolDt, lastDoneDt, nextVisitDate, lastVisit);
            return pDao;
        } catch (Exception e) {
            Rlog.fatal("study",
                    "Exception In getPatientStudiesWithVisits in StudyAgentBean "
                            + e);
        }
        return null;

    }

    public PatStudyStatDao getPatientStudies(int pkey, int user) {
        try {
            PatStudyStatDao pDao = new PatStudyStatDao();
            pDao.getPatientStudies(pkey, user);
            return pDao;
        } catch (Exception e) {
            Rlog.fatal("study",
                    "Exception In getPatientStudies in StudyAgentBean " + e);
        }
        return null;

    }

    public String getLkpTypeVersionForView(int viewpk) {
        String retStr = "";
        try {
            LookupDao lDao = new LookupDao();
            retStr = lDao.getLkpTypeVersionForView(viewpk);
            return retStr;
        } catch (Exception e) {
            Rlog.fatal("study",
                    "Exception In getLkpTypeVersionForView in StudyAgentBean "
                            + e);
        }
        return null;

    }

    public LookupDao getAllViewsForLkpType(int account, String lkptype) {

        try {
            LookupDao lDao = new LookupDao();
            lDao.getAllViewsForLkpType(account, lkptype);
            return lDao;
        } catch (Exception e) {
            Rlog
                    .fatal("study",
                            "Exception In getAllViewsForLkpType in StudyAgentBean "
                                    + e);
        }
        return null;

    }

    public StudyDao getUserStudiesWithRights(String userId, String dName,
            int selVal, String siteId) {
        try {

            StudyDao studyDao = new StudyDao();

            studyDao.getUserStudiesWithRights(userId, dName, selVal, siteId);
            return studyDao;

        } catch (Exception e) {
            Rlog.fatal("study",
                    "Exception In getUserStudiesWithRights in StudyAgentBean "
                            + e);
            return null;
        }
    }

    public int getOrgAccessForStudy(int userId, int studyId, int orgId) {
        int count = 0;
        try {

            StudyDao studyDao = new StudyDao();
            count = studyDao.getOrgAccessForStudy(userId, studyId, orgId);

        } catch (Exception e) {
            Rlog.fatal("study",
                    "Exception In getOrgAccessForStudy in StudyAgentBean " + e);
        }
        return count;

    }

    public int getStudyCompleteDetailsAccessRight(int userId, int groupId,
            int studyId) {
        int returnRight = 0;
        try {

            StudyDao studyDao = new StudyDao();
            returnRight = studyDao.getStudyCompleteDetailsAccessRight(userId,
                    groupId, studyId);
            return returnRight;

        } catch (Exception e) {
            Rlog.fatal("study",
                    "Exception In getStudyCompleteDetailsAccessRight in StudyAgentBean "
                            + e);

        }
        return 0;

    }

    // to delete a study
    // km
    public void deleteStudy(int studyId) {
        try {
            Rlog.debug("study",
                    "In deleteStudy in StudyAgentBean line number 0");
            StudyDao studyDao = new StudyDao();
            Rlog.debug("study",
                    "In deleteStudy in StudyAgentBean line number 1");
            studyDao.deleteStudy(studyId,"","");
            Rlog.debug("study",
                    "In deleteStudy in StudyAgentBean line number 2");
        } catch (Exception e) {
            Rlog.fatal("study", "Exception In deleteStudy in StudyAgentBean "
                    + e);
        }
    }

    // deleteStudy() Overloaded for INF-18183 ::: Raviesh
    public void deleteStudy(int studyId,Hashtable<String, String> args) {
        try {
        	 AuditBean audit=null; //Audit Bean for AppDelete
             String currdate =DateUtil.getCurrentDate(); /*gets the current date from DateUtil*/
             String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
             String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
             String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
             String appModule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the App Module Name from the Hashtable*/
             
             String getRidValue= AuditUtils.getRidValue("ER_STUDY","eres","PK_STUDY="+studyId);/*Fetches the RID/PK_VALUE*/ 
         	audit = new AuditBean("ER_STUDY",String.valueOf(studyId),getRidValue,
         			userID,currdate,appModule,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
         	em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
               	
            Rlog.debug("study",
                    "In deleteStudy in StudyAgentBean line number 0");
            StudyDao studyDao = new StudyDao();
            Rlog.debug("study",
                    "In deleteStudy in StudyAgentBean line number 1");
            studyDao.deleteStudy(studyId, userID, ipAdd);
            Rlog.debug("study",
                    "In deleteStudy in StudyAgentBean line number 2");
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("study", "Exception In deleteStudy in StudyAgentBean "
                    + e);
        }
    }
    
    // km

    // to delete a patient from a particular study
    // km

    public void deletePatStudy(int studyId, int patId) {
        try {
            Rlog.debug("study",
                    "In deletePatStudy in StudyAgentBean line number 0");
            StudyDao studyDao = new StudyDao();
            Rlog.debug("study",
                    "In deletePatStudy in StudyAgentBean line number 1");
            studyDao.deletePatStudy(studyId, patId);
            Rlog.debug("study",
                    "In deletePatStudy in StudyAgentBean line number 2");
        } catch (Exception e) {
            Rlog.fatal("study",
                    "Exception In deletePatStudy in StudyAgentBean " + e);
        }
    }
    // Overloaded for INF-18183 ::: AGodara
    public void deletePatStudy(int studyId, int patId,Hashtable<String, String> auditInfo) {
    	String pkVal;
		String ridVal;
    	try {
                 	
        	AuditBean audit=null;
            String condition = "FK_STUDY="+studyId+" AND FK_PER="+patId;
        	String currdate =DateUtil.getCurrentDate();
        	String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); //Fetches the User ID from the Hashtable
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); //Fetches the IP Address from the Hashtable
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); //Fetches the reason for deletion from the Hashtable
            String app_module=(String)auditInfo.get(AuditUtils.APP_MODULE); //Fetches the reason for deletion from the Hashtable
         
            Hashtable<String, ArrayList<String>> rowValues = AuditUtils.getRowValues("ER_PATPROT",condition,"eres");/*Fetches the RID/PK_VALUE*/
            ArrayList<String> rowPK  = (ArrayList<String>)rowValues.get(AuditUtils.ROW_PK_KEY);
    		ArrayList<String> rowRID  = (ArrayList<String>)rowValues.get(AuditUtils.ROW_RID_KEY);
    	   	
    		for(int i=0; i<rowPK.size(); i++)
			{
    			pkVal = rowPK.get(i);
    			ridVal = rowRID.get(i);
    			audit = new AuditBean("ER_PATPROT",pkVal,ridVal,
        			userID,currdate,app_module,ipAdd,reason);
    			em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
			}
    		
            StudyDao studyDao = new StudyDao();
            int result = studyDao.deletePatStudy(studyId, patId);
            if(result==-1){
            	context.setRollbackOnly();
            }
          
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("study",
                    "Exception In deletePatStudy in StudyAgentBean " + e);
        }
    }
    
    public StudyDao getStudyAutoRows(String userId ,String actId, String studyIds, int criterion) {
        try {
            StudyDao studyDao = new StudyDao();
            studyDao.getStudyAutoRows(userId, actId, studyIds, criterion);
            return studyDao;
        } catch (Exception e) {
            Rlog.fatal("study",
                    "Exception In getStudyAutoRows in StudyAgentBean "
                            + e);
            return null;
        }
    }

    private static final String StudyStatusAgentHome = "velos/StudyStatusAgentBean/remote";
    private StudyStatusAgentRObj getStudyStatusAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (StudyStatusAgentRObj) initial.lookup(StudyStatusAgentHome);
        } catch (Exception e) {
            Rlog.fatal("studyAgent", "EXCEPTION IN getStudyStatusAgentHome:"+e);
            return null;
        }
    }
  
    /**
     * Public flush method that invokes the EJB's EntityManager's flush() method.
     * 
     * This helps callers address an issue with transactions that use EJBs
     * packaged separately. Sub methods in the same transaction do not see a flushed 
     * state in the database. Use this method with caution, it may be deprecated 
     * very soon.
     */
    public void flush(){
    	em.flush();
    }
}// end of class

