/*
 * Classname : StatusHistoryAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 08/09/2004
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.service.statusHistoryAgent.impl;

/* IMPORT STATEMENTS */

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.common.StatusHistoryDao;
import com.velos.eres.business.statusHistory.impl.StatusHistoryBean;
import com.velos.eres.service.statusHistoryAgent.StatusHistoryAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.StringUtil;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP
 * StatusHistoryBean.<br>
 * <br>
 * 
 * @author Anu Khanna
 * @version 1.0 08/09/2004
 * @ejbHome StatusHistoryAgentHome
 * @ejbRemote StatusHistoryAgentRObj
 * @see StatusHistoryBean
 */

@Stateless
public class StatusHistoryAgentBean implements StatusHistoryAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    @Resource private SessionContext context;

    /**
     * Calls getStatusHistoryDetails() on StatusHistory Entity Bean
     * StatusHistoryBean. Looks up on the status id parameter passed in and
     * constructs and returns a status state holder. containing all the summary
     * details of the statusr<br>
     * 
     * @param status
     *            id
     * @see StatusHistoryBean
     */
    public StatusHistoryBean getStatusHistoryDetails(int statusId) {

        StatusHistoryBean retrieved = null;

        try {

            retrieved = (StatusHistoryBean) em.find(StatusHistoryBean.class,
                    statusId);

        } catch (Exception e) {
            Rlog.fatal("statusHistory",
                    "Error in getStatusHistoryDetails() in StatusHistoryAgentBean"
                            + e);
        }

        return retrieved;
    }

    /**
     * Calls setStatusHistoryDetails() on StatusHistory Entity Bean
     * StatusHistoryBean.Creates a new status with the values in the StateKeeper
     * object.
     * 
     * @param status
     *            a statusHistory state keeper containing statusHistory
     *            attributes for the new status.
     * @return int - 0 if successful; -2 for Exception;
     */
    /*
     * Modified by Sonia, 08/31/04 added logic to find the latest status, and
     * set its end date to null if its not null, this way we should have atleast
     * one cuurrent status
     */
    /*
     * Modified by Sonia, 09/14/04 when finding the next status do not set its
     * stataus end date to null (if any status is found)
     */

    /*
     * Modified by Dylan, 11/23/10 setting iscurrent to 0 for most recent 
     * status. 5428
     */
    public int setStatusHistoryDetails(StatusHistoryBean status) {
        
    	 String dateFormat = Configuration.getAppDateFormat();
         if (dateFormat.indexOf("MMM") != -1)
         	dateFormat = StringUtil.replace(dateFormat,"MMM","Mon");
    	
    	try {

            int newStatus = 0;

            Enumeration enum_velos;
            StatusHistoryBean prevCurStat = null;
            StatusHistoryBean nextStat = null;
           
            
            ArrayList list = new ArrayList();
            Query query = null;
            
            String newStatIsCurrentFlag = "";
            
          //get the last isCurrent Status and update it to 0
            newStatIsCurrentFlag = status.getIsCurrentStat();
            
            if (StringUtil.isEmpty(newStatIsCurrentFlag))
            {
            	newStatIsCurrentFlag="0";
            }

            if ( newStatIsCurrentFlag.equals("1") )
            {

            	query = em.createNamedQuery("findByPreviousIsCurrentFlag");

            	query.setParameter("STATUS_MODPK", status.getStatusModuleId());
            	query.setParameter("STATUS_MODTABLE", status
            			.getStatusModuleTable());

            	list = (ArrayList) query.getResultList();
            	if (list == null)
            		list = new ArrayList();

            	if (list.size() > 0)
            	{
            		//get the bean

            		StatusHistoryBean oldIsCurrentFlagStat = (StatusHistoryBean) list.get(0);

            		//set the flag to 0
            		oldIsCurrentFlagStat.setIsCurrentStat("0");
            		em.merge(oldIsCurrentFlagStat);
            	}

            } // end of if newStatIsCurrentFlag.equals("1")


            StatusHistoryBean newStat = new StatusHistoryBean();

            newStat.updateStatusHistory(status);
            em.persist(newStat);

            StatusHistoryBean latestStat = null;
            String latestEndDate = "";

            newStatus = newStat.getStatusId();
           

            // find the last current status and update its end date
            query = em.createNamedQuery("findByPreviousCurrentStatusInHistory");
            query.setParameter("STATUS_MODPK", status.getStatusModuleId());
            query
                    .setParameter("STATUS_MODTABLE", status
                            .getStatusModuleTable());
            query.setParameter("begindate", status.returnStatusStartDate());
            query.setParameter("pk_status", newStatus);
            query.setParameter("date_format",dateFormat);
            
            list = (ArrayList) query.getResultList();
          
            if (list == null)
                list = new ArrayList();

            if (list.size() > 0) {
                prevCurStat = (StatusHistoryBean) list.get(0);

                // a previous current status found, update its end date with the
                // new status's start date
                
                
                prevCurStat.updateStatusEndDate(status.returnStatusStartDate());
                em.merge(prevCurStat);

            } else // the start date of this status exists between 2 existing
            // status records or is the first record
            {

                // get the previous record to change its end date

                // find the a previous status from this new start date and
                // update the previous status's end date

                query = em.createNamedQuery("findByPreviousStatusInHistory");

                query.setParameter("STATUS_MODPK", status.getStatusModuleId());
                query.setParameter("STATUS_MODTABLE", status
                        .getStatusModuleTable());
                query.setParameter("begindate", status.returnStatusStartDate());
                query.setParameter("pk_status", newStatus);
                query.setParameter("date_format",dateFormat);

                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();

                if (list.size() > 0) {
                    prevCurStat = (StatusHistoryBean) list.get(0);
                    
                    // a previous current status found, update its end date with
                    // the new status's start date

                    prevCurStat
                            .updateStatusEndDate(status.returnStatusStartDate());
                    em.merge(prevCurStat);

                }

                //
                query = em.createNamedQuery("findByNextStatusInHistory");
                query.setParameter("STATUS_MODPK", status.getStatusModuleId());
                query.setParameter("STATUS_MODTABLE", status
                        .getStatusModuleTable());
                query.setParameter("begindate", status.returnStatusStartDate());
                query.setParameter("pk_status", newStatus);
                query.setParameter("date_format",dateFormat);

                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();

                if (list.size() > 0) {
                    nextStat = (StatusHistoryBean) list.get(0);
                    // a next status found,
                    // update the new status's end date with the next status's
                    // start date
                    
                    newStat.updateStatusEndDate(nextStat.returnStatusStartDate());
                    em.merge(newStat);

                }

            }

            // find the latest status, and set its end date to null if its not
            // null

            try {
            if(status.getStatusModuleTable().equalsIgnoreCase("er_ctrp_nci_stat")  ){
            	 query = em.createNamedQuery("findByLatestStatusInHistoryForUpdate");
            }else{
                query = em.createNamedQuery("findByLatestStatusInHistory");
            }
                query.setParameter("STATUS_MODPK", status.getStatusModuleId());
                query.setParameter("STATUS_MODTABLE", status
                        .getStatusModuleTable());

                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();

                if (list.size() > 0) {
                    latestStat = (StatusHistoryBean) list.get(0);
                    // a latest status found, get its end date
                    latestEndDate = latestStat.returnStatusEndDate();
                    // if end date is not empty, set it to empty
                    if (!StringUtil.isEmpty(latestEndDate)) {
                        latestStat.updateStatusEndDate("");
                        em.merge(latestStat);
                    }

                }
                 
                
                
            } catch (EJBException ex) {
            	Rlog.fatal("statusHistory",
                        "Error in setStatusHistoryDetails() in StatusHistoryAgentBean "
                                + ex);
                latestStat = null;
            }
            
            

            return newStatus;
        } catch (Exception e) {
            Rlog.fatal("statusHistory",
                    "Error in setStatusHistoryDetails() in StatusHistoryAgentBean "
                            + e);
        }
        return 0;
    }

   
    //  Added by Manimaran for the July-August Enhancement S4.
    public int setStatusHistoryDetailsWithEndDate(StatusHistoryBean status) {
        try {

            int newStatus = 0;

            StatusHistoryBean newStat = new StatusHistoryBean();

            newStat.updateStatusHistoryWithEndDate(status);
            em.persist(newStat);
            newStatus = newStat.getStatusId();
            return newStatus;
        } catch (Exception e) {
            Rlog.fatal("statusHistory",
                    "Error in setStatusHistoryDetailsWithEndDate() in StatusHistoryAgentBean "
                            + e);
            return 0;
        }
       
    }

    
    /**
     * Calls updateStatusHistory() on StatusHistory Entity Bean
     * StatusHistoryBean.Updates the status with the values in the StateKeeper
     * object.
     * 
     * @param status :
     *            state keeper containing StatusHistory attributes for the
     *            status.
     * @return int - 0 if successful; -2 for Exception;
     */
    /*
     * Modified by Sonia, 08/31/04 added logic to find the latest status, and
     * set its end date to null if its not null, this way we should have atleast
     * one cuurrent status
     */

    public int updateStatusHistory(StatusHistoryBean hsk) {

        StatusHistoryBean retrieved = null;

        int output;
        String dateFormat = Configuration.getAppDateFormat();
        if (dateFormat.indexOf("MMM") != -1)
        	dateFormat = StringUtil.replace(dateFormat,"MMM","Mon");
        try {
            ArrayList list = new ArrayList();
            Query query = null;

            String oldStartDate = "";
            String newStartDate = "";
            String oldEndDate = "";
            Enumeration enum_velos;
            StatusHistoryBean prevCurStat = null;
            StatusHistoryBean nextStat = null;

            StatusHistoryBean latestStat = null;
            String latestEndDate = "";

            retrieved = (StatusHistoryBean) em.find(StatusHistoryBean.class,
                    hsk.getStatusId());

            if (retrieved == null) {
                return -2;
            }

            // get old start date and end date
            oldStartDate = retrieved.returnStatusStartDate();
            oldEndDate = retrieved.returnStatusEndDate();

            // get new start date

            newStartDate = hsk.returnStatusStartDate();

            output = retrieved.updateStatusHistory(hsk);
            em.merge(retrieved);

            // if there is a change in the start date

            if (!newStartDate.equals(oldStartDate)) {
                // find the a previous status from this new start date and
                // update the previous status's end date

                query = em.createNamedQuery("findByPreviousStatusInHistory");

                query.setParameter("STATUS_MODPK", hsk.getStatusModuleId());
                query.setParameter("STATUS_MODTABLE", hsk
                        .getStatusModuleTable());
                query.setParameter("begindate", hsk.returnStatusStartDate());
                query.setParameter("pk_status", hsk.getStatusId());
                query.setParameter("date_format",dateFormat);

                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();

                if (list.size() > 0) {
                    prevCurStat = (StatusHistoryBean) list.get(0);

                    // a previous current status found, update its end date with
                    // the new status's start date

                    prevCurStat.updateStatusEndDate(retrieved
                            .returnStatusStartDate());
                    em.merge(prevCurStat);

                }
                // //////////////////////////////////////////////////

                // see if the the start date of this status exists between 2
                // existing status records

                query = em.createNamedQuery("findByNextStatusInHistory");
                query.setParameter("STATUS_MODPK", hsk.getStatusModuleId());
                query.setParameter("STATUS_MODTABLE", hsk
                        .getStatusModuleTable());
                query.setParameter("begindate", hsk.returnStatusStartDate());
                query.setParameter("pk_status", hsk.getStatusId());
                query.setParameter("date_format",dateFormat);

                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();

                if (list.size() > 0) {
                    nextStat = (StatusHistoryBean) list.get(0);
                    // a next status found,
                    // update the new status's end date with the next status's
                    // start date
                    retrieved
                            .updateStatusEndDate(nextStat.returnStatusStartDate());
                    em.merge(retrieved);

                } else // now this status has become the current status because
                // there is no next record
                {
                    retrieved.updateStatusEndDate(null);
                }

                // find the latest status, and set its end date to null if its
                // not null

                try {
                if(hsk.getStatusModuleTable().equalsIgnoreCase("er_ctrp_nci_stat")){
                	query = em.createNamedQuery("findByLatestStatusInHistoryForUpdate");
                }else{
                    query = em.createNamedQuery("findByLatestStatusInHistory");
                }
                    query.setParameter("STATUS_MODPK", hsk.getStatusModuleId());
                    query.setParameter("STATUS_MODTABLE", hsk
                            .getStatusModuleTable());

                    list = (ArrayList) query.getResultList();
                    if (list == null)
                        list = new ArrayList();

                    if (list.size() > 0) {
                        latestStat = (StatusHistoryBean) list.get(0);
                        // a latest status found, get its end date
                        latestEndDate = latestStat.returnStatusEndDate();
                        // if end date is not empty, set it to empty
                        if (!StringUtil.isEmpty(latestEndDate)) {
                            latestStat.updateStatusEndDate("");
                            em.merge(latestStat);
                        }

                    }
                    // ////////

                } catch (EJBException ex) {
                    latestStat = null;
                }

            } // end of if for checking if status is changed

        } catch (Exception e) {
            Rlog.debug("statusHistory",
                    "Error in updateStatusHistory in StatusHistoryAgentBean"
                            + e);
            return -2;
        }
        return output;
    }

    
    
    //Added by Manimaran for the July-August Enhancement S4.
    public int updateStatusHistoryWithEndDate(StatusHistoryBean hsk) {

        StatusHistoryBean retrieved = null;

        int output;

        try {

            retrieved = (StatusHistoryBean) em.find(StatusHistoryBean.class,
                    hsk.getStatusId());
            
            if (retrieved == null) {
                return -2;
            }

            output = retrieved.updateStatusHistoryWithEndDate(hsk);
            em.merge(retrieved);

          
        } catch (Exception e) {
            Rlog.debug("statusHistory",
                    "Error in updateStatusHistory in StatusHistoryAgentBean"
                            + e);
            return -2;
        }
        return output;
    }

    
    /**
     * remove status *
     * 
     * @param status :
     *            state keeper containing status attributes for the status.
     * @return int :0 if successful <BR> : -2 if unsuccessful <BR> : -3 if the
     *         status can not be deleted as this is the only status left for the
     *         module PK
     */

    /*
     * Modified by Sonia, 08/31/04 added logic to find the latest status, and
     * set its end date to null if its not null, this way we should have atleast
     * one cuurrent status
     */
    public int deleteStatusHistory(StatusHistoryBean hsk) {

        StatusHistoryBean statusHistory = null;
        int ret = 0;
        String dateFormat = Configuration.getAppDateFormat();
        if (dateFormat.indexOf("MMM") != -1)
        	dateFormat = StringUtil.replace(dateFormat,"MMM","Mon");
        try {

            ArrayList list = new ArrayList();
            Query query = null;

            String nextStartDate = "";
            Enumeration enum_velos;

            StatusHistoryBean prevCurStat = null;
            StatusHistoryBean nextStat = null;

            StatusHistoryBean latestStat = null;
            StatusHistoryBean otherStatRObj = null;

            String latestEndDate = "";

            String modulePk = "";
            String moduleTable = "";

            statusHistory = (StatusHistoryBean) em.find(
                    StatusHistoryBean.class, hsk.getStatusId());

            modulePk = statusHistory.getStatusModuleId();
            moduleTable = statusHistory.getStatusModuleTable();

            // check if there is atleast one more status besides the one that is
            // passed

            try {
                query = em.createNamedQuery("findByAtLeastOneStatusInHistory");
                query.setParameter("STATUS_MODPK", modulePk);
                query.setParameter("STATUS_MODTABLE", moduleTable);
                query.setParameter("pk_status", hsk.getStatusId());

                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();

            } catch (Exception ex) {
                Rlog.fatal("statusHistory",
                        "trying to delete status record, no other status found "
                                + ex);
                // no other status found
                return -3;
            }

            if (list.size() == 0) {
                Rlog
                        .fatal("statusHistory",
                                "trying to delete status record, no exception but otherStatRObj null ");
                return -3;
            }

            // update end dates

            // see if the the start date of the deleted status exists between 2
            // existing status records

            query = em.createNamedQuery("findByNextStatusInHistory");
            query.setParameter("STATUS_MODPK", statusHistory
                    .getStatusModuleId());
            query.setParameter("STATUS_MODTABLE", statusHistory
                    .getStatusModuleTable());
            query.setParameter("begindate", statusHistory
                    .returnStatusStartDate());
            query.setParameter("pk_status", statusHistory.getStatusId());
            query.setParameter("date_format",dateFormat);

            list = (ArrayList) query.getResultList();
            if (list == null)
                list = new ArrayList();

            if (list.size() > 0) {
                nextStat = (StatusHistoryBean) list.get(0);
                // a next status found,
                // get next start date
                nextStartDate = nextStat.returnStatusStartDate();
            }

            // find the a previous status from the deleted start date and update
            // the previous status's end date
            query = em.createNamedQuery("findByPreviousStatusInHistory");

            query.setParameter("STATUS_MODPK", statusHistory
                    .getStatusModuleId());
            query.setParameter("STATUS_MODTABLE", statusHistory
                    .getStatusModuleTable());
            query.setParameter("begindate", statusHistory
                    .returnStatusStartDate());
            query.setParameter("pk_status", statusHistory.getStatusId());
            query.setParameter("date_format",dateFormat);

            list = (ArrayList) query.getResultList();
            if (list == null)
                list = new ArrayList();

            if (list.size() > 0) {

                prevCurStat = (StatusHistoryBean) list.get(0);

                // a previous current status found, update its end date with the
                // start date of the nextstatus found from the deleted status
                // record

                prevCurStat.updateStatusEndDate(nextStartDate);
                em.merge(prevCurStat);

            }

            em.remove(statusHistory);

            // find the latest status, and set its end date to null if its not
            // null

            try {

                query = em.createNamedQuery("findByLatestStatusInHistory");
                query.setParameter("STATUS_MODPK", modulePk);
                query.setParameter("STATUS_MODTABLE", moduleTable);

                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();

                if (list.size() > 0) {
                    latestStat = (StatusHistoryBean) list.get(0);
                    // a latest status found, get its end date
                    latestEndDate = latestStat.returnStatusEndDate();
                    // if end date is not empty, set it to empty
                    if (!StringUtil.isEmpty(latestEndDate)) {
                        latestStat.updateStatusEndDate("");
                        em.merge(latestStat);
                    }

                }

            } catch (EJBException ex) {
                latestStat = null;
            }

            return 0;
        }

        catch (Exception e) {
            e.printStackTrace();
            Rlog.fatal("statusHistory", "EXCEPTION IN DELETING status" + e);
            return -2;

        }

    }
    
    // deleteStatusHistory() Overloaded for INF-18183 ::: Raviesh
    public int deleteStatusHistory(StatusHistoryBean hsk,Hashtable<String, String> args) {

        StatusHistoryBean statusHistory = null;
        int ret = 0;
        String dateFormat = Configuration.getAppDateFormat();
        if (dateFormat.indexOf("MMM") != -1)
        	dateFormat = StringUtil.replace(dateFormat,"MMM","Mon");
        try {

        	 AuditBean audit=null; //Audit Bean for AppDelete
             String currdate =DateUtil.getCurrentDate(); /*gets the current date from DateUtil*/
             String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
             String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
             String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
             String appModule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the App Module Name from the Hashtable*/
             
             String getRidValue= AuditUtils.getRidValue("ER_STATUS_HISTORY","eres","PK_STATUS="+hsk.getStatusId());/*Fetches the RID/PK_VALUE*/ 
         	audit = new AuditBean("ER_STATUS_HISTORY",String.valueOf(hsk.getStatusId()),getRidValue,
         			userID,currdate,appModule,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
         	//em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
         	
            ArrayList list = new ArrayList();
            Query query = null;

            String nextStartDate = "";
            Enumeration enum_velos;

            StatusHistoryBean prevCurStat = null;
            StatusHistoryBean nextStat = null;

            StatusHistoryBean latestStat = null;
            StatusHistoryBean otherStatRObj = null;

            String latestEndDate = "";

            String modulePk = "";
            String moduleTable = "";

            statusHistory = (StatusHistoryBean) em.find(
                    StatusHistoryBean.class, hsk.getStatusId());

            modulePk = statusHistory.getStatusModuleId();
            moduleTable = statusHistory.getStatusModuleTable();

            // check if there is atleast one more status besides the one that is
            // passed

            try {
                query = em.createNamedQuery("findByAtLeastOneStatusInHistory");
                query.setParameter("STATUS_MODPK", modulePk);
                query.setParameter("STATUS_MODTABLE", moduleTable);
                query.setParameter("pk_status", hsk.getStatusId());

                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();

            } catch (Exception ex) {
            	context.setRollbackOnly();
            	  Rlog.fatal("statusHistory",
                        "trying to delete status record, no other status found "
                                + ex);
                // no other status found
                return -3;
            }

            if (list.size() == 0) {
            	context.setRollbackOnly();
                Rlog
                        .fatal("statusHistory",
                                "trying to delete status record, no exception but otherStatRObj null ");
                return -3;
            }

            // update end dates

            // see if the the start date of the deleted status exists between 2
            // existing status records

            query = em.createNamedQuery("findByNextStatusInHistory");
            query.setParameter("STATUS_MODPK", statusHistory
                    .getStatusModuleId());
            query.setParameter("STATUS_MODTABLE", statusHistory
                    .getStatusModuleTable());
            query.setParameter("begindate", statusHistory
                    .returnStatusStartDate());
            query.setParameter("pk_status", statusHistory.getStatusId());
            query.setParameter("date_format",dateFormat);

            list = (ArrayList) query.getResultList();
            if (list == null)
                list = new ArrayList();

            if (list.size() > 0) {
                nextStat = (StatusHistoryBean) list.get(0);
                // a next status found,
                // get next start date
                nextStartDate = nextStat.returnStatusStartDate();
            }

            // find the a previous status from the deleted start date and update
            // the previous status's end date
            query = em.createNamedQuery("findByPreviousStatusInHistory");

            query.setParameter("STATUS_MODPK", statusHistory
                    .getStatusModuleId());
            query.setParameter("STATUS_MODTABLE", statusHistory
                    .getStatusModuleTable());
            query.setParameter("begindate", statusHistory
                    .returnStatusStartDate());
            query.setParameter("pk_status", statusHistory.getStatusId());
            query.setParameter("date_format",dateFormat);

            list = (ArrayList) query.getResultList();
            if (list == null)
                list = new ArrayList();

            if (list.size() > 0) {

                prevCurStat = (StatusHistoryBean) list.get(0);

                // a previous current status found, update its end date with the
                // start date of the nextstatus found from the deleted status
                // record

                prevCurStat.updateStatusEndDate(nextStartDate);
                em.merge(prevCurStat);

            }

            
            em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
            em.remove(statusHistory);
            

            // find the latest status, and set its end date to null if its not
            // null

            try {

                query = em.createNamedQuery("findByLatestStatusInHistory");
                query.setParameter("STATUS_MODPK", modulePk);
                query.setParameter("STATUS_MODTABLE", moduleTable);

                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();

                if (list.size() > 0) {
                    latestStat = (StatusHistoryBean) list.get(0);
                    // a latest status found, get its end date
                    latestEndDate = latestStat.returnStatusEndDate();
                    // if end date is not empty, set it to empty
                    if (!StringUtil.isEmpty(latestEndDate)) {
                        latestStat.updateStatusEndDate("");
                        em.merge(latestStat);
                    }

                }

            } catch (EJBException ex) {            	
            	latestStat = null;
            }

            return 0;
            
        }

        catch (Exception e) {
        	context.setRollbackOnly();
            e.printStackTrace();
            Rlog.fatal("statusHistory", "EXCEPTION IN DELETING status" + e);
            return -2;

        }

    }

    /**
     * 
     * This method get the records in er_status_history for a particular module
     * id and module table
     * 
     * @param module
     *            pk : pk of module for which status history record are to be
     *            fetched
     * @param moduleTable:
     *            module table for which status history records are to b fetched
     * @return StatusHistoryDao: object of status history dao if successful else
     *         it returns null
     */

    public StatusHistoryDao getStatusHistoryInfo(int modulePK,
            String moduleTable) {
        try {

            StatusHistoryDao statusHistoryDao = new StatusHistoryDao();

            statusHistoryDao.getStatusHistoryInfo(modulePK, moduleTable);
            return statusHistoryDao;

        } catch (Exception e) {
            Rlog.fatal("statusHistory",
                    "Exception In getStatusHistoryInfo in StatusHistoryAgentBean "
                            + e);

        }

        return null;

    }
    
    public StatusHistoryDao getStatusHistoryCTRP(int modulePK,
            String moduleTable) {
        try {

            StatusHistoryDao statusHistoryDao = new StatusHistoryDao();

            statusHistoryDao.getStatusHistoryCTRP(modulePK, moduleTable);
            return statusHistoryDao;

        } catch (Exception e) {
            Rlog.fatal("statusHistory",
                    "Exception In getStatusHistoryCTRP in StatusHistoryAgentBean "
                            + e);

        }

        return null;

    }

    /**
     * Method to get the latest status for a particular moduleId and
     * moduleTable.
     * 
     * @param modulePK
     *            PK of module for which latest status is to be retrieved
     * @param moduleTable :
     *            module table name for which latest status is to be retrieved
     * @returns String: codelst subtype value if successful else return null
     */

    public String getLatestStatus(int modulePK, String moduleTable) {
        String success = null;
        try {

            StatusHistoryDao statusHistoryDao = new StatusHistoryDao();

            success = statusHistoryDao.getLatestStatus(modulePK, moduleTable);

        } catch (Exception e) {
            Rlog.fatal("statusHistory",
                    "Exception In getStatusHistoryInfo in StatusHistoryAgentBean "
                            + e);

        }

        return success;

    }

    /**
     * Method to get the latest status history ID for a particular modulePK and moduleTable.
     * 
     * @param modulePK
     *            PK of module for which the latest status is to be retrieved
     * @param moduleTable :
     *            Table name for which the latest status is to be retrieved
     * @returns int: PK of the ER_STATUS_HISTORY table
     */
    public int getLatestStatusId(int modulePK, String moduleTable) {
    	StatusHistoryDao statusHistoryDao = new StatusHistoryDao();
    	return statusHistoryDao.getLatestStatusId(modulePK, moduleTable);
    }

}// end of class
