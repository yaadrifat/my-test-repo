/**
 *@author Sonika Talwar 
 * date Aug 28, 2002
 *@version 1.0
 */

package com.velos.eres.service.perApndxAgent.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.aithent.audittrail.reports.AuditUtils;


import com.velos.eres.business.common.PerApndxDao;
import com.velos.eres.business.perApndx.impl.PerApndxBean;


import com.velos.eres.service.perApndxAgent.PerApndxAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;


@Stateless
public class PerApndxAgentBean implements PerApndxAgentRObj {
    @PersistenceContext(unitName = "epat")
    protected EntityManager em;
    
    @Resource
    private SessionContext context;

    public int setPerApndxDetails(PerApndxBean pask) {
        PerApndxBean perApndxBean=new PerApndxBean();
        try {
            
            perApndxBean.updatePerApndx(pask);
            em.persist(perApndxBean);
            return perApndxBean.getId();
        } catch (Exception e) {
            Rlog.fatal("perapndx", "Exception in setPerApndxDetails........." + e);
            return -1;
        }
    }

    public PerApndxBean getPerApndxDetails(int perApndxId) {
        try {
            return (PerApndxBean) em.find(PerApndxBean.class, new Integer(
                    perApndxId));
        } catch (Exception e) {
            Rlog.fatal("perapndx", "Exception in getPerApndxDetails" + e);
            return null;
        }
    }

    public int updatePerApndx(PerApndxBean pask) {
        int output = 0;
        PerApndxBean perApndx = null;
        try {
            perApndx = (PerApndxBean) em.find(PerApndxBean.class, pask.getId());
            output = perApndx.updatePerApndx(pask);
            em.merge(perApndx);
            return output;
        } catch (Exception e) {
            Rlog.fatal("perapndx", "Excepltion in updatePerApndxDetails" + e);
            return -1;
        }
    }

    public int removePerApndx(int perApndxId) {
        PerApndxBean perApndx = null;
        try {
            perApndx = (PerApndxBean) em.find(PerApndxBean.class, new Integer(
                    perApndxId));
            em.remove(perApndx);
            return 0;
        } catch (Exception e) {
            Rlog.fatal("perapndx", "Exception in removePerApndx" + e);
            return -1;
        }
    }

    // Overloaded for INF-18183 ::: AGodara
    public int removePerApndx(int perApndxId,Hashtable<String, String> auditInfo) {
        
    	PerApndxBean perApndx = null;
    	Connection conn = null;    
        try {
        	
        	String app_module=(String)auditInfo.get(AuditUtils.APP_MODULE); //Fetches the APP_MODULE for deletion from the Hashtable
        	String condition ="PK_PERAPNDX="+perApndxId;
        	Hashtable<String, ArrayList<String>> rowValues = AuditUtils.getRowValues("PAT_PERAPNDX", condition, "epat");
        	conn = AuditUtils.insertAuditRow("PAT_PERAPNDX", rowValues, app_module, auditInfo, "epat");
        	
        	perApndx = (PerApndxBean) em.find(PerApndxBean.class, new Integer(
                    perApndxId));
        	em.remove(perApndx);
        	conn.commit();
            return 0;
        } catch (Exception e) {
        	try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
        	context.setRollbackOnly();
            Rlog.fatal("perapndx", "Exception in removePerApndx(int perApndxId,Hashtable<String, String> auditInfo)" + e);
            return -1;
        }finally{
        	try {
				conn.close();
			} catch (SQLException e) {
					e.printStackTrace();
			}
        }
    }
     
    /**
     * Returns the PerApndxDao object containing the list of Uris for the
     * patient
     * 
     * @param patId
     *            Patient Id for which list of uris is required
     * @return Data Access Object for PerApndx
     * @see PerApndxDao
     */

    public PerApndxDao getPerApndxUris(int patId) {
        try {
            Rlog.debug("perapndx",
                    "In getPerApndxUris in PerApndxAgentBean - 0");
            PerApndxDao perApndxDao = new PerApndxDao();
            Rlog.debug("perapndx",
                    "In getPerApndxUris in PerApndxAgentBean - 1");
            perApndxDao.getPerApndxUris(patId);
            Rlog.debug("perapndx",
                    "In getPerApndxUris in PerApndxAgentBean - 2");
            return perApndxDao;
        } catch (Exception e) {
            Rlog.fatal("perapndx",
                    "Exception In getPerApndxUris in PerApndxAgentBean " + e);
        }
        return null;
    }

    /**
     * Returns the PerApndxDao object containing the list of Files for the
     * patient
     * 
     * @param patId
     *            Patient Id for which list of files is required
     * @return Data Access Object for PerApndx
     * @see PerApndxDao
     */

    public PerApndxDao getPerApndxFiles(int patId) {
        try {
            Rlog.debug("perapndx",
                    "In getPerApndxFiles in PerApndxAgentBean - 0");
            PerApndxDao perApndxDao = new PerApndxDao();
            Rlog.debug("perapndx",
                    "In getPerApndxFiles in PerApndxAgentBean - 1");
            perApndxDao.getPerApndxFiles(patId);
            Rlog.debug("perapndx",
                    "In getPerApndxFiles in PerApndxAgentBean - 2");
            return perApndxDao;
        } catch (Exception e) {
            Rlog.fatal("perapndx",
                    "Exception In getPerApndxFiles in PerApndxAgentBean " + e);
        }
        return null;
    }

}
