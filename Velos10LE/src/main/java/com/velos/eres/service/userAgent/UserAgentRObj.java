/*
 * Classname : UserAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 02/23/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana
 */

package com.velos.eres.service.userAgent;

/* Import Statements */

import java.util.Hashtable;
import javax.ejb.Remote;

import com.velos.eres.business.common.UserDao;
import com.velos.eres.business.user.impl.UserBean;

/* End of Import Statements */

/**
 * Remote interface for UserAgent session EJB
 * 
 * @author dinesh
 */

/*
 * Modified by Sonia - 08/06/04 Changed method Name
 * getActivatedDeactivatedUsers() to getUsersSelectively()
 */
@Remote
public interface UserAgentRObj {

    public UserBean getUserDetails(int userId);

    public int setUserDetails(UserBean usb);

    public UserBean validateUser(String username, String password);

    public UserBean validateUser(String username, String password, boolean enableSessionMode);

    public UserBean findUser(String username);

    public int updateUser(UserBean usk);
    // Overloaded for INF-18183 ::: AGodara
    public int updateUser(UserBean usk,Hashtable<String, String> auditInfo);

    public UserDao getByGroupId(int groupId);

    public UserDao getAccountUsers(int accId);
 
    public UserDao getAccountUsers(int accId, String userStatus, String firstName, String lastName);//KM
   
    public UserDao getUsersSelectively(int accId, String status, String firstName, String lastName );//KM

    public UserDao getNonSystemUsers(int accId, String userType, String firstName, String lastName);//KM

    public UserDao getUserValuesByLoginName(String logName);

    public int getCount(String userLogin);
    
    public int getCount(String userLname, String userFname, String accId, String mode, String userId);    
    public int getCount(String email, String accId, String mode, String userId);    

    public String getUserCode(String userId);

    public UserDao getUsersWithStudyCopy(String studyId, String accountId);

    public UserDao getUsersForEvent(int eventId, String userType);

    public UserDao getAvailableEventUsers(int accId, String lName,
            String fName, String role, String organization, String userType,
            int eventId);

    public UserDao getUsersDetails(String usersList);

    public int getUsersInAccount(int accId);

    public int getActiveUsersInAccount(int accId);

    public UserDao getAccountUsersDetails(int accId);
    
    public UserDao getAccountUsersDetailsForLookup(int accId);

    public UserDao getSiteUsers(int siteId);

    // public void getGroupUsers(String groupIds) ;

    // public void getStudyUsers(String studyIds) ;

    // public void getOrganizationUsers(String orgIds) ;

    public int deleteUser(UserBean tsk);
    
    //JM: 12/10/05   
    public void changeUserType(int userId, int group, String usrLoginName,String usrPass,String usrESign,String usrMail);
    
    /** Added by Sonia Abrol, 03/07/06*/
    
    /** Gets list of user names
     * @param ids comma separated list of user ids 
     * @return list of user names*/
     public String getUserNamesFromIds(String ids);
     
     /**
      * Calls deactivateUser() on UserDao;
      * 
      * @param  userPk: user to be deactivated
      * @param creator: logged in user for audit
      * @param ipAdd: ip address of logged in user for audit
      *            
      * @return int returns success flag: 0:successful;-1:unsuccessful
      *         
      * @see UserDao
      */
//APR-04-2011,MSG-TEMPLAGE FOR USER INFO V-CTMS-1
     public int deactivateUser(String userPk, String creator, String ipAdd);
     /**
      * Calls notifyUserResetInfo() on UserDao;
      * 
      * @param  userPk: user to be deactivated
      * @param creator: logged in user for audit
      * @param ipAdd: ip address of logged in user for audit
      *            
      * @return int returns success flag: 0:successful;-1:unsuccessful
      *         
      * @see UserDao
      */

     public int notifyUserResetInfo(String pWord, String url,String eSign,Integer userId,String userLogin,String infoFlag);
     
     public void flush();

     /**
      * To Check User Password Rotation Error
      * Calls checkPasswordRotation on UserAgentBean;
      * 
      * @author Raviesh
      * @Enhancement INF-18669 
      * 
      * @param encryptedPass: New Password.
      * @param userId: user id information.
      * @param userAccId: Account id of the user.  
      * */
	public Boolean checkPasswordRotation(String encryptedPass, Integer userId, Integer userAccId);
    
    
}
