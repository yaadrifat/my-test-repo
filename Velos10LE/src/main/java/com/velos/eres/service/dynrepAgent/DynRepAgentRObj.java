/*
 * Classname : DynRepAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 09/28/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Vishal Abrol		
 */

package com.velos.eres.service.dynrepAgent;

/* Import Statements */

import java.util.ArrayList;
import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.eres.business.common.DynRepDao;
import com.velos.eres.business.dynrep.impl.DynRepBean;

/* End of Import Statements */

/**
 * Remote interface for dynrepAgent session EJB
 * 
 * @author Vishal Abrol
 * @version : 1.0 09/28/2003
 */
@Remote
public interface DynRepAgentRObj {
    public DynRepBean getDynRepDetails(int id);

    public int setDynRepDetails(DynRepBean dsk);

    public int updateDynRep(DynRepBean dsk);

    public int removeDynRep(int id);
    
    // removeDynRep() Overloaded for INF-18183 ::: Raviesh
    public int removeDynRep(int id,Hashtable<String, String> args);

    public DynRepDao getMapData(String formId);

    public DynRepDao getReportData(String sql, String[] fldNames,
            String formType  , Hashtable moreParameters);

    public DynRepDao getReportData(String sql, String[] fldNames,
            String formType, ArrayList fldTypes  , Hashtable moreParameters);

    public DynRepDao getReportDetails(String repIdStr);

    public DynRepDao populateHash(String repIdStr);

    public int setFilterDetails(DynRepDao dynDao, String mode);

    public DynRepDao getFilterDetails(String fltrId);

    public DynRepDao getFilter(String repId);
    
    public int removeFilters(ArrayList fltrIds);
    
    // Overloaded for INF-18183 ::: Akshi
    public int removeFilters(ArrayList fltrIds,Hashtable<String, String> args);
   
    public DynRepDao getSectionDetails(ArrayList paramCols, String formId);

    public DynRepDao fillReportContainer(String repIdStr);

    public DynRepDao getFormNames(String formIdStr, String delimiter);
    
    public int copyDynRep(String repName, int id, String usrId);

}
