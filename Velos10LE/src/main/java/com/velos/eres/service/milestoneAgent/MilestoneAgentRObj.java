/*
 * Classname			MilestoneAgentRObj
 *
 * Version information 	1.0
 *
 * Date					05/28/2002
 *
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.service.milestoneAgent;

import java.util.HashMap;
import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.eres.business.common.MileAchievedDao;
import com.velos.eres.business.common.MilestoneDao;
import com.velos.eres.business.milestone.impl.MilestoneBean;

/**
 * Remote interface for MilestoneAgent session EJB
 *
 * @author Sonika Talwar
 */
@Remote
public interface MilestoneAgentRObj {
    /**
     * gets the milestone details
     */
    MilestoneBean getMilestoneDetails(int milestoneId);

    /**
     * sets the milestone details
     */
    public int setMilestoneDetails(MilestoneBean milestone);

    public int updateMilestone(MilestoneBean ssk);
    
    // updateMilestone() Overloaded for INF-18183 ::: Raviesh
    public int updateMilestone(MilestoneBean ssk,Hashtable<String, String> args);

    public MilestoneDao getMilestoneRows(int studyId);

    public MilestoneDao getEventsForNew(int studyId, int calId);

    public MilestoneDao getEventsForNewVisitMilestone(int studyId, int calId);

    public int setMilestoneDetails(MilestoneDao mdo, String creator,
            String ipAdd);

    /**
     * Gets Achieved milestones
     *
     * @param mileStoneId
     *            mileStoneId
     */
    public MileAchievedDao getAchievedMilestones(int mileStoneId , String user );

    public MilestoneDao getMilestoneRows(int studyId, String mileStoneType, String payType,String milestoneReceivableStatus);
    
    public MilestoneDao getMilestoneRowsForStudy(int studyId);    
    
     /**
     * Gets Milestones Other than Active and In-Active
     *
     * @param studyId
     *            studyId
     */
    /*YK 29Mar2011 -DFIN20 */
    public MilestoneDao getStudyMileStoneForSetStatus(int studyId);    


//  JM: 20MAR2008:
    public MilestoneDao getMilestoneRows(int studyId, String mileStoneType, String payType,String milestoneReceivableStatus, String selStatus, String selMilePayType, String srchStrCalName, String srchStrVisitName, String srchStrEvtName, String inActiveFlag, String orderBy, String orderType);

    /* Sonia Abrol, 12/07/06
	 * Apply user notification settings of a milestone to all milestones in a study*/
    public int applyNotificationSettingsToAll(String milestoneID, String userIds, String studyId, String modifiedBy, String ipAdd );

    public int createMultiMilestones(int userId, String ipAdd,MilestoneDao md, int pkbudget, int study,int bgtcalId);



    /**
     *
     * @author Sonia Abrol
     * date - 08/26/08
     *
     * Gets milestone Achievemnt details
     *
     * @param studyId
     *            study id
     * @param   milestoneType milestone type code
     * @param htParams ashtable of additional parameters (for later use)
     */
    public MileAchievedDao getALLAchievedMilestones(int studyId, String milestoneType , Hashtable htParams);

    /** Sonia Abrol, 08/27/08
	 * delete milestione achievement records from er_mileachieved
	 * @param arPkmileAch Array of PKs to be deleted */

    public int deleteMSAch(String[] arPkmileAch );
    
    //Overloaded for INF-18183 ::: Ankit
    public int deleteMSAch(String[] arPkmileAch, Hashtable<String, String> args);
    
    
    
	//DFIN-13 BK MAR-16-2011
    
    /**
     * Create milestone records having  parameters in form of JSON array.
     * @param dataGridString String in form of JSONArray.
     * @return
     */
    public int createMilestonesUsingJSON(String dataGridString,MilestoneBean mileBean);
    /**
     * Update milestone records having  parameters in form of JSON array.
     * @param dataGridString String in form of JSONArray.
     * @param updateString   String in form of JSONArray.
     * @return integer
     */

    public int updateMilestonesUsingJSON(String dataGridString,String updateString);
    
    public int updateMilestonesUsingJSON(String dataGridString,String updateString,MilestoneBean mileBean);
    
    /**
     * Delete milestone records having  parameters in form of JSON array.
     * @param dataGridString String in form of JSONArray.
     * @return integer
     */
    public int deleteMilestonesUsingJSON(String dataGridString);
 
    // Overloaded for INF-18183 ::: Akshi
    public int deleteMilestonesUsingJSON(String dataGridString,Hashtable<String, String> args);

    /* FIN-22373 09-Oct-2012 -Sudhir*/
    public MilestoneDao getMilestoneRows(int studyId, String mileStoneType, String payType,String milestoneReceivableStatus, String selStatus, String selMilePayType, String srchStrCalName, String srchStrVisitName, String srchStrEvtName, String inActiveFlag, String orderBy, String orderType , String datefrom, String dateto);
    
    /*Added for INF-22500 by Yogendra Pratap Singh 24/01/13*/
	public MilestoneDao getMilestoneRowsForSearch(int studyId,HashMap<String,String> hMap);


}
