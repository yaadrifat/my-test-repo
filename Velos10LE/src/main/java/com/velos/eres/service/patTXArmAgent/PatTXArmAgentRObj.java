/**
 * Classname : PatTXArmAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 05/02/2005
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.service.patTXArmAgent;

/* Import Statements */
import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.eres.business.common.PatTXArmDao;
import com.velos.eres.business.patTXArm.impl.PatTXArmBean;

/* End of Import Statements */

/**
 * Remote interface for PatTXArmAgent session EJB
 * 
 * @author Anu Khanna
 */
@Remote
public interface PatTXArmAgentRObj {
    /**
     * gets the PatTXArm record
     */
    PatTXArmBean getPatTXArmDetails(int patTXArmId);

    /**
     * creates a new PatTXArm Record
     */
    public int setPatTXArmDetails(PatTXArmBean ptk);

    /**
     * updates the PatTXArm Record
     */
    public int updatePatTXArm(PatTXArmBean ptk);

    /**
     * remove PatTXArm Record
     */
    
    // Overloaded for INF-18183 ::: Akshi
    public int removePatTXArm(int PatTXArmId,Hashtable<String, String> args);

    public int removePatTXArm(int PatTXArmId);

    public PatTXArmDao getPatTrtmtArms(int patProtId);

}
