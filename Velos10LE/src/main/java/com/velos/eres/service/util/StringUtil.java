/*
 *  String Functions
 */
package com.velos.eres.service.util;

/*
 * Import all the exceptions can be thrown
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Random;
import java.util.Vector;
import java.util.regex.Pattern;

/**
 * StringUtil helper class for String processing in the application. It
 * encapsulte the most of the string processing capability required in the
 * application.
 * 
 * @author vishal
 * @created April 5, 2005
 */
/*
 * Modified by Sonia Abrol, 01/13/05 added function isEmpty
 */
/*
 * Modified by Sonia Abrol, 03/18/05 added method String getFormattedString() to
 * create a formatted string
 */
public final class StringUtil {
    /**
     * encodeString method encodes any string to application internal
     * encoding.All the special chracters are replaced with applications's
     * internal codes to avoid any failure duing processing. For Decoding from
     * application encoded String,refer {@link #decodeString}
     * 
     * @param InputStr
     *            Description of the Parameter
     * @return String - application encoded String.
     */

    public static String encodeString(String InputStr) {
        if (InputStr != null) {
            if (InputStr.indexOf("'") >= 0) {
                InputStr = replaceAll(InputStr, "\'", "[SQuote]");
            }
            if (InputStr.indexOf("\"") >= 0) {
                InputStr = replaceAll(InputStr, "\"", "[DQuote]");
            }
            if (InputStr.indexOf("\r") >= 0) {
                InputStr = replaceAll(InputStr, "\r", "[CRet]");

            }
            if (InputStr.indexOf("\n") >= 0) {
                InputStr = replaceAll(InputStr, "\n", "[NLine]");

            }
            if (InputStr.indexOf("\t") >= 0) {
                InputStr = replaceAll(InputStr, "\t", "[HTab]");
            }
            if (InputStr.indexOf("\\") >= 0) {
                InputStr = replaceAll(InputStr, "\\", "[BSlash]");

            }
            if (InputStr.indexOf("#") >= 0) {
                InputStr = replaceAll(InputStr, "#", "[VelHash]");

            }
            if (InputStr.indexOf(",") >= 0) {
                InputStr = replaceAll(InputStr, ",", "[VELCOMMA]");

            }
            if (InputStr.indexOf("%") >= 0) {
                InputStr = replaceAll(InputStr, "%", "[VELPERCENT]");

            }

            if (InputStr.indexOf("&") >= 0) {
                InputStr = replaceAll(InputStr, "&", "[VELAMP]");

            }

            if (InputStr.indexOf(";") >= 0) {
                InputStr = replaceAll(InputStr, ";", "[VELSEMICOLON]");

            }
            if (InputStr.indexOf(">") >= 0) {
                InputStr = replaceAll(InputStr, ">", "[VELGTSIGN]");

            }
            if (InputStr.indexOf("<") >= 0) {
                InputStr = replaceAll(InputStr, "<", "[VELLTSIGN]");

            }
            /*Added By Amarnadh for Bugzilla issue #3231 */
            if (InputStr.indexOf("+") >= 0) {
                InputStr = replaceAll(InputStr, "+", "[VELPLUS]");

            }
            
            if (InputStr.indexOf(" ") >= 0) {
                InputStr = replaceAll(InputStr, " ", "[VELSP]");

            }
            
            
        }
        return InputStr;
    }

    /**
     * decodeString method decodes only string which are encoded by {@link
     * #encodeString}. For encoding refer {@link #encodeString}
     * 
     * @param InputStr
     *            Description of the Parameter
     * @return String - application encoded String.
     */

    public static String decodeString(String InputStr) {
        if (InputStr != null) {
            if (InputStr.indexOf("[SQuote]") >= 0) {
                InputStr = replaceAll(InputStr, "[SQuote]", "\'");
            }
            if (InputStr.indexOf("[DQuote]") >= 0) {
                InputStr = replaceAll(InputStr, "[DQuote]", "\"");
            }
            if (InputStr.indexOf("[CRet]") >= 0) {
                InputStr = replaceAll(InputStr, "[CRet]", "\r");

            }
            if (InputStr.indexOf("[NLine]") >= 0) {
                InputStr = replaceAll(InputStr, "[NLine]", "\n");

            }
            if (InputStr.indexOf("[HTab]") >= 0) {
                InputStr = replaceAll(InputStr, "[HTab]", "\t");
            }
            if (InputStr.indexOf("[BSlash]") >= 0) {
                InputStr = replaceAll(InputStr, "[BSlash]", "\\");

            }
            if (InputStr.indexOf("[VelHash]") >= 0) {
                InputStr = replaceAll(InputStr, "[VelHash]", "#");
            }
            if (InputStr.indexOf("[VELCOMMA]") >= 0) {
                InputStr = replaceAll(InputStr, "[VELCOMMA]", ",");
            }

            if (InputStr.indexOf("[VELPERCENT]") >= 0) {
                InputStr = replaceAll(InputStr, "[VELPERCENT]", "%");
            }
            if (InputStr.indexOf("[VELAMP]") >= 0) {
                InputStr = replaceAll(InputStr, "[VELAMP]", "&");
            }
            
            //  Added by Manimaran to fix the Bug2974
            if (InputStr.indexOf("[VELSEMICOLON]") >= 0) {
                InputStr = replaceAll(InputStr, "[VELSEMICOLON]", ";");
            }
            
            if (InputStr.indexOf("[VELGTSIGN]") >= 0) {
                InputStr = replaceAll(InputStr, "[VELGTSIGN]", ">");
            }
            
            if (InputStr.indexOf("[VELLTSIGN]") >= 0) {
                InputStr = replaceAll(InputStr, "[VELLTSIGN]", "<");
            }
            /*Added By Amarnadh for Bugzilla issue #3231 */
            if (InputStr.indexOf("[VELPLUS]") >= 0) {
                InputStr = replaceAll(InputStr, "[VELPLUS]", "+");
            }
            
            if (InputStr.indexOf("[VELSP]") >= 0) {
                InputStr = replaceAll(InputStr, "[VELSP]", " ");
            }
            
            
        }
        return InputStr;
    }

    /**
     * Method to split up the string . This class expects the delimiter as
     * prefix and suffix e.g.
     * [VELCOMMA]Test1[VELCOMMA]test2[VELCOMMA]test3[VELCOMMA]test4[VELCOMMA]
     * will return array of items [Test1,test2,test3,test4] .
     * 
     * @param str -
     *            complete String that would be parsed.
     * @param delim -
     *            delimiter.
     * @return string[]- array of string with all the tokens.
     */

    public static String[] strSplit(String str, String delim) {
        return strSplit(str, delim, true);
    }

    /**
     * Method to split up the string . This class does not expect the delimiter
     * as prefix and suffix if passed, ismbedded flag must be set to 'true'.\n\n
     * e.g. delimiters embedded :
     * [VELCOMMA]Test1[VELCOMMA]test2[VELCOMMA]test3[VELCOMMA]test4[VELCOMMA]
     * will return array of items [Test1,test2,test3,test4] . without delimiters
     * :Test1[VELCOMMA]test2[VELCOMMA]test3[VELCOMMA]test4 return array of items
     * [Test1,test2,test3,test4] .
     * 
     * @param str -
     *            complete String that would be parsed.
     * @param delim -
     *            delimiter.
     * @param isembedded -
     *            If the String is embedded in delimiters.
     * @return string[]- array of string with all the tokens
     */

    public static String[] strSplit(String str, String delim, boolean isembedded) {
        Vector vec = new Vector();
        str = (str == null) ? "" : str;
        if (!isembedded) {
            if (delim.equals(",")) {
                return chopChop(str, (char) 44);
            }
        }
        if (str.indexOf(delim) >= 0) {

            String tempStr = new String();
            int i = 0;
            int j = 0;
            int cnt = 0;
            if (!isembedded) {
                if (i < str.length()) {
                    tempStr = str.substring(i, str.indexOf(delim, i));
                    vec.add(tempStr);
                }
            }
            while ((i = str.indexOf(delim, i)) != -1) {
                cnt++;
                if (cnt % 2 == 1) {

                    i = j = i + delim.length();

                    continue;
                }

                tempStr = str.substring(j, i);
                vec.add(tempStr);

                tempStr = new String();
                j = i + delim.length();

            }
            if (!isembedded) {
                if (j < str.length()) {
                    tempStr = str.substring(j, str.length());
                    vec.add(tempStr);
                }
            } else {
                if (j < str.length() - 1) {
                    tempStr = str.substring(j, str.length());
                    vec.add(tempStr);
                }
            }
            String[] retArray = new String[vec.size()];
            for (int k = 0; k < retArray.length; k++) {
                retArray[k] = new String(((String) vec.elementAt(k)).trim());
            }
            return retArray;
        } else {
            // return the element in array if no delimiter found

            if (str.length() > 0) {
                vec.add(str);
            }
            String[] retArray = new String[vec.size()];
            for (int k = 0; k < retArray.length; k++) {
                retArray[k] = new String(((String) vec.elementAt(k)).trim());
            }
            return retArray;
        }

    }

    /**
     * This method retunr the number of time a substring appear within another
     * string e.g. countOf("testtest","s") will return 2.
     * 
     * @param source -
     *            String that is parsed for occurences of substring.
     * @param find -
     *            find string
     * @return int - number of occurences of substring in String.
     */

    public static int countOf(String source, String find) {

        StringBuffer destination = new StringBuffer();

        int last_index = 0;

        int numCount = 0;

        boolean finished = false;

        while (!finished) {

            int index = source.indexOf(find, last_index);

            if (index >= 0) {
                // if found next match

                // increase count

                numCount++;

                last_index = index + find.length();

            } else {
                // if no more matches

                // copy from last index to end of string

                destination.append(source
                        .substring(last_index, source.length()));

                finished = true;

            }

        }

        return numCount;
    }

    /**
     * Returns a new string resulting from replacing all occurrences of
     * <code>oldString</code> in this string with <code>newString</code>.
     * 
     * @param strOriginal -
     *            Original String
     * @param strOld -
     *            old substing to find.
     * @param strNew
     *            Description of the Parameter
     * @return a new derived string from original string by replacing all
     *         occurences of old String with new ones.
     * @deprecated - Please use {@link #replace(String source, String find,
     *             String replace)}
     */

    public static String replaceAll(String strOriginal, String strOld,
            String strNew) {
        strOriginal = replace(strOriginal, strOld, strNew);
        return strOriginal;
    }

    /**
     * Returns a new string resulting from replacing all occurrences of
     * <code>oldString</code> in this string with <code>newString</code>.
     * 
     * @param source -
     *            Original String
     * @param find -
     *            old substing to find.
     * @param replace -
     *            new string to replace.
     * @return a new derived string from original string by replacing all
     *         occurences of old String with new ones.
     */
    public static String replace(String source, String find, String replace) {
        StringBuffer destination = new StringBuffer();
        int last_index = 0;
        if (isEmpty(source))
            return "";
        boolean finished = false;

        // if the find string is not in main string, just return main string
        find = (find == null) ? "" : find;
        if (find.length() == 0) {
            return source;
        }
        if (source.indexOf(find) < 0) {
            return source;
        }

        while (!finished) {
            int index = source.indexOf(find, last_index);

            if (index >= 0) {
                // if found next match

                // copy substring from last index to new index
                destination.append(source.substring(last_index, index));
                destination.append(replace);
                last_index = index + find.length();
            } else {
                // if no more matches

                // copy from last index to end of string
                destination.append(source
                        .substring(last_index, source.length()));
                finished = true;
            }
        }

        return destination.toString();
    }

    /**
     * This method overcomes the limitation of strSplit. If the string parsed
     * with delimiters has data value that also contains the delimiter string.
     * the results are not proper. e.g.
     * strSplit("test,\"test2,test3\",test4",",") will return 3 elements [test ,
     * "test2 , test3" ,test4] and
     * chopChop("test,\"test2,test3\",test4",(char)44) will return 3 elements
     * [test, "test2,test3", test4] Method chopChop checks if the token is
     * enclosed in quotes and there is a delimiter character in quotes, then
     * delimiter is ignored.
     * 
     * @param _field_delimiter -
     *            delimiter e.g. (char)44 for comman.
     * @param s
     *            Description of the Parameter
     * @return string[] - Array of resulting tokens.
     */

    public static String[] chopChop(String s, char _field_delimiter) {

        String _null_string = null;
        boolean _empty_string_as_null = false;
        boolean _ignore_quotes = true;
        // char _field_delimiter = (char)44;
        if (s == null) {
            Rlog.debug("common", "chopChop:Cannot Parse a Null String.");
        }

        char[] cs = s.toCharArray();
        int len = cs.length;
        char[] buf = new char[len + 1];

        Vector strings = new Vector();

        int j = 0;
        boolean in_quotes = false;
        boolean quoted = false;
        //
        // parse string into a vector of strings
        //

        for (int i = 0; i <= len; i++) {
            //
            // check for end of the char buffer
            //

            if (i == len) {
                String ss = new String(buf, 0, j);

                //
                // catch null string values and empty strings
                // unless the value was quoted
                //

                if (!quoted) {
                    if ((_null_string != null && ss.equals(_null_string))
                            || (_empty_string_as_null && ss.equals(""))) {
                        ss = null;
                    }
                }

                strings.addElement(ss);
                j = 0;
                quoted = false;
                break;
            }
            //
            // if this is a double quotes char and we are ignoring
            // quotes then detect beginning or end of quoted string
            //
            else if (cs[i] == (char) 34 && _ignore_quotes) {

                if (in_quotes) {
                    in_quotes = false;
                    quoted = true;
                } else {
                    in_quotes = true;
                }
            }
            //
            // otherwise if this is a delimiter and we are
            // not within a quoted string then terminate current
            // buffer and add this to our vector
            //
            else if ((cs[i] == _field_delimiter) && !in_quotes) {

                String ss = new String(buf, 0, j);

                //
                // catch null string values and empty strings
                // unless the value was quoted
                //

                if (!quoted) {
                    if ((_null_string != null && ss.equals(_null_string))
                            || (_empty_string_as_null && ss.equals(""))) {
                        ss = null;
                    }
                }

                strings.addElement(ss);
                j = 0;
                quoted = false;
            }
            //
            // otherwise add this character to the current buffer
            //
            else {
                buf[j++] = cs[i];
            }
        }

        //
        // if still in quotes then this is an error
        //

        if (in_quotes) {
            Rlog.debug("common", "chopChop: Mis-matched quote in string");

        }

        //
        // convert vector to an array and return
        //

        String[] string_array = new String[strings.size()];
        strings.copyInto(string_array);

        return string_array;
    }

    /**
     * Checks is the passed string is empty. Checks for null or blank string.
     * 
     * @param strParam
     *            Description of the Parameter
     * @return The empty value
     */
    public static boolean isEmpty(String strParam) {
        if ((!(strParam == null)) && (!(strParam.trim()).equals(""))
                && ((strParam.trim()).length() > 0)) {
            return false;
        } else {
            return true;
        }

    }
   
    public static String trueValue(String value)
    {
    	if (isEmpty(value))
    		value="";
    	
    	return value;
    }
   
    /*
     * Added by Sonia Abrol, 03/18/05 method String getFormattedString() to
     * create a formatted string
     */
    /**
     * Method to create a formatted string
     * 
     * @param prefixString
     *            Prefix for the formatted string
     * @param suffixString
     *            suffix for the formatted string
     * @param numberFormat
     *            format for the number part of the formatted string
     * @param numberForNumberFormat
     *            number for the string. the numberFormat will be applied to
     *            this number <BR>
     *            to generate the final number <BR>
     *            Usage: you can leave prefixString or suffixString blank in
     *            order to set the position of the number.<BR>
     *            if you need format: 22ABCSITE, leave prefix blank and pass
     *            ABCSITE as suffix<BR>
     *            if you need format: PROT22ABCSITE, pass PROT as prefix and
     *            ABCSITE as suffix<BR>
     *            if you need format: ABC22, leave suffix blank and pass ABC as
     *            prefix <BR>
     *            if you do not need a number, pass numberFormat as -1 and
     *            numberForNumberFormat blank <BR>
     *            For generating a random number - Pass numberForNumberFormat as
     *            -1 and pass numberFormat (a random number will be generated of
     *            numberFormat's size) <BR>
     *            if you need to use the number as it is and do not require any
     *            formatting, pass numberFormat as blank
     * @return The formattedString value
     */
    public static String getFormattedString(String prefixString,
            String suffixString, String numberFormat, int numberForNumberFormat) {
        StringBuffer sbFinalString = new StringBuffer();
        String finalNumberString = "";
        int paddingLength = 0;
        String paddingString = "";
        Random randomNumber = new Random();
        String randomNumberUpperLimit = "";

        String numberForNumberFormatString = "";
        int numberForNumberFormatLength = 0;

        if (!StringUtil.isEmpty(prefixString)) {
            sbFinalString.append(prefixString);
        }

        // create the number string

        if (!StringUtil.isEmpty(numberFormat)) {
            paddingLength = numberFormat.length();

            if (numberForNumberFormat < 0) {
                // if user does not pass a number but passes a format

                for (int k = 0; k < paddingLength; k++) {
                    randomNumberUpperLimit = randomNumberUpperLimit + "0";
                }

                randomNumberUpperLimit = "1" + randomNumberUpperLimit;
                numberForNumberFormat = randomNumber.nextInt(Integer
                        .parseInt(randomNumberUpperLimit));
                // a random number is generated
            }

            numberForNumberFormatString = String.valueOf(numberForNumberFormat);
            numberForNumberFormatLength = numberForNumberFormatString.length();

            // if the number's length (generated or passed by user) is smaller
            // than the format
            if (paddingLength > numberForNumberFormatLength) {

                paddingLength = paddingLength - numberForNumberFormatLength;
                for (int k = 0; k < paddingLength; k++) {
                    paddingString = paddingString + "0";
                }

                finalNumberString = paddingString + numberForNumberFormatString;
            } else {
                // if the number's length is bigger then cut it according to the
                // format length

                finalNumberString = numberForNumberFormatString.substring(0,
                        paddingLength);

            }
        } else {
            // no numberFormat is passed, use the number as it is
            if (numberForNumberFormat >= 0) {
                finalNumberString = String.valueOf(numberForNumberFormat);
            }
        }

        sbFinalString.append(finalNumberString);

        if (!StringUtil.isEmpty(suffixString)) {
            sbFinalString.append(suffixString);
        }

        return sbFinalString.toString();
    }

    /**
     * Replaces the special character with the HTML encoding using html codes
     * 
     * @param str
     *            String to encode
     * @return Returns the encoded String. null if str is null
     */
    public static String htmlEncode(String str) {
        if (str == null)
            return null;
        if (str.length() == 0)
            return "";
        if (str.indexOf("&") >= 0)
            str = StringUtil.replace(str, "&", "&amp;");
        if (str.indexOf("<") >= 0)
            str = StringUtil.replace(str, "<", "&lt;");
        if (str.indexOf(">") >= 0)
            str = StringUtil.replace(str, ">", "&gt;");
      
        return str;
    }
    
    /**
     * Replaces the special characters e.g. dollar sign, pond sign, yen sign etc. with the HTML encoding using html codes
     * 
     * @param str
     *            String to encode
     * @return Returns the encoded String. null if str is null
     */
    public static String htmlEncode(String str, boolean encodeAll) {
        if (str == null)
            return null;
        if (str.length() == 0)
            return "";
        if(encodeAll==true){
        if (str.indexOf("\u00A9") >= 0)
            str = StringUtil.replace(str,"\u00A9","&copy;");
        if (str.indexOf("\u00AE") >= 0)
            str = StringUtil.replace(str,"\u00AE","&reg;");
        if (str.indexOf("\u00B2") >= 0)
            str = StringUtil.replace(str,"\u00B2","&sup2;");
        if (str.indexOf("\u20AC") >= 0)
            str = StringUtil.replace(str,"\u20AC","&euro;");
        if (str.indexOf("\u00A3") >= 0)
            str = StringUtil.replace(str,"\u00A3","&pound;");
        if (str.indexOf("\u00A5") >= 0)
            str = StringUtil.replace(str,"\u00A5","&yen;");
        if (str.indexOf("\u00B5") >= 0)
            str = StringUtil.replace(str,"\u00B5","&micro;");
        if (str.indexOf("\u2122") >= 0)
            str = StringUtil.replace(str,"\u2122","&trade;");
        if (str.indexOf("\u0099") >= 0)
            str = StringUtil.replace(str,"\u0099","&trade;");
        StringUtil.htmlEncode(str);
        }
        else
        {
        	StringUtil.htmlEncode(str);
        }
      
        return str;
    }
    
    /**
     * Extends htmlEncode to better handle cross-site scripting (xss)
     * 
     * @param str
     *            String to encode
     * @return Returns the encoded String. null if str is null
     */
    public static String htmlEncodeXss(String str) {
        if (str == null) { return null; }
        return StringUtil.htmlEncode(str).replaceAll("\n", "").replaceAll("\t","")
        .replaceAll("\r","").replaceAll("\f","").replaceAll("\u0007","").replaceAll("\u001B","")
        .replaceAll("\u000e","").replaceAll("\\x0e","").replaceAll("\"","&quot;");
    }
    
    /**
     * Strips &lt;script&gt; and &lt;/script&gt; tags (case insensitive on the word script) to prevent cross-site scripting (xss).
     * 
     * @param str
     *            String to be stripped
     * @return Returns the String stripped of the script tags; null if str is null.
     */
    public static String stripScript(String str) {
    	if (str == null) { return null; }
		String outStr = Pattern.compile("[<]script[>]", Pattern.CASE_INSENSITIVE).matcher(str).replaceAll("");
		return Pattern.compile("[<][/]script[>]", Pattern.CASE_INSENSITIVE).matcher(outStr).replaceAll("");
    }

    /**
     * Replaces the HTML encoding to special characters using html codes mapping
     * 
     * @param str
     *            String to decode
     * @return Returns the decoded String. null if str is null
     */
    public static String htmlDecode(String str) {
        if (str == null)
            return null;
        if (str.length() == 0)
            return "";
        if (str.indexOf("&#034;")>=0)
    		str=  StringUtil.replace(str,"&#034;","\"");
        if (str.indexOf("&amp;") >= 0)
            str = StringUtil.replace(str, "&amp;", "&");
        if (str.indexOf("&lt;") >= 0)
            str = StringUtil.replace(str, "&lt;", "<");
        if (str.indexOf("&gt;") >= 0)
            str = StringUtil.replace(str, "&gt;", ">");
        return str;
    }

    /**
     * Escape special character with escape character '\' in a string
     * 
     * @param String
     *            aTagFragment - String to Escape
     * @return String escaped String
     */

    public static String escapeSpecialChar(String str) {

        if (str == null)
            return null;
        if (str.length() == 0)
            return "";
        if (str.indexOf("'") >= 0)
            str = StringUtil.replace(str, "'", "\'");
        if (str.indexOf("\"") >= 0)
            str = StringUtil.replace(str, "\"", "\\\"");
        /*
         * if (str.indexOf("\r\n")>=0) str=StringUtil.replace(str,"\r\n","<br/>");
         */
        return str;

        /*
         * final StringBuffer result = new StringBuffer();
         * 
         * final StringCharacterIterator iterator = new
         * StringCharacterIterator(aTagFragment); char character =
         * iterator.current(); while (character != StringCharacterIterator.DONE ){
         * if (character == '\"') { result.append("\\\""); } else if (character ==
         * '\'') { result.append("\\'"); } else { //the char is not a special
         * one //add it to the result as is result.append(character); }
         * character = iterator.next(); }
         * 
         * return result.toString();
         */
    }

    /**
     * Removing escaping of special characters with '\'
     * 
     * @param String
     *            str - String to unEscape
     * @return String unEscaped String
     */
    public static String unEscapeSpecialChar(String str) {
        if (str == null)
            return null;
        if (str.length() == 0)
            return "";
        if (str.indexOf("\\'") >= 0)
            str = StringUtil.replace(str, "\\'", "'");
        if (str.indexOf("\\\"") >= 0)
            str = StringUtil.replace(str, "\\\"", "\"");
        return str;
    }

    /**
     * Escape special character with escape character '\' and html codes
     * 
     * @param String
     *            aTagFragment - String to Escape
     * @return String escaped String
     */

    public static String escapeSpecialCharHTML(String str) {
        if (str == null)
            return null;
        if (str.length() == 0)
            return "";
        if (str.indexOf("\"") >= 0)
            str = StringUtil.replace(str, "\"", "&quot;");
        return str;
    }

    /**
     * Escape special character with escape character '\' for javascripts
     * 
     * @param String
     *            aTagFragment - String to Escape
     * @return String escaped String
     */

    public static String escapeSpecialCharJS(String str) {
        if (str == null)
            return null;
        if (str.length() == 0)
            return "";
        if (str.indexOf("\\") >= 0)
            str = StringUtil.replace(str, "\\", "\\\\");
        if (str.indexOf("'") >= 0)
            str = StringUtil.replace(str, "'", "\\'");
        if (str.indexOf("\r\n") >= 0)
            str = StringUtil.replace(str, "\r\n", " ");

        return str;
    }
    
    /**
     * Escape special character for SQLS
     * 
     * @param String
     *            aTagFragment - String to Escape
     * @return String escaped String
     */

    public static String escapeSpecialCharSQL(String str) {
        if (str == null)
            return null;
        if (str.length() == 0)
            return "";
        if (str.indexOf("'") >= 0)
            str = StringUtil.replace(str, "'", "''");
        if (str.indexOf("\r\n") >= 0)
            str = StringUtil.replace(str, "\r\n", " ");

        return str;
    }

    /**
     * Removing escaping of special characters with '\' and html codes
     * 
     * @param String
     *            str - String to unEscape
     * @return String unEscaped String
     */
    public static String unEscapeSpecialCharHTML(String str) {
        if (str == null)
            return null;
        if (str.length() == 0)
            return "";
        if (str.indexOf("\\'") >= 0)
            str = StringUtil.replace(str, "\\'", "'");
        if (str.indexOf("&quot;") >= 0)
            str = StringUtil.replace(str, "&quot;", "\"");
        return str;
    }
    
    // List of string literals, but in the future it is better to use resource bundle
    public static final String eSignBgcolor = "#cccccc";
    
    

    /**
     * Convert an array of strings to one string.
     * 
     * @param Array of String
     *            
     * @param String
     *           a- separator 
     * @return String concatenated string
     */
    public static String arrayToString(String[] a, String separator) {
    	StringBuffer result = new StringBuffer();
    	if (a.length > 0) {
    		result.append(a[0]);
    		for (int i=1; i<a.length; i++) {
    			result.append(separator);
    			result.append(a[i]);
    		}
    	}
    	return result.toString();
    }

    static int cReturn;
    static float cFloatReturn;

    public static int stringToNum(String str) {
    	int iReturn = 0;
    	try {
    		if ((str == null) || str.equals("null")) {
    			return cReturn;
    		} else if (str.equals("")) {
    			return cReturn;
    		} else {
    			iReturn = Integer.parseInt(str);
    			return iReturn;
    		}
    	} catch (Exception e) {
    		Rlog.fatal("common", "{EXCEPTION IN CONVERTING TO NUMBER:}" + e);
    		return iReturn;
    	}
    }

    public static float stringToFloat(String str) {
    	float iReturn = 0;
    	try {

    		if (str == null) {
    			return cFloatReturn;
    		} else if (str == "") {
    			return cFloatReturn;
    		} else {
    			iReturn = Float.parseFloat(str);
    			return iReturn;
    		}
    	} catch (Exception e) {
    		Rlog.fatal("common", "{EXCEPTION IN CONVERTING TO FLOAT:}" + e);
    		return iReturn;
    	}
    }
    
    public static String floatToString(Float i) {
        String returnString = null;
        try {
            if (i == null) {
                return returnString;
            }
            returnString = i.toString();
            return returnString;
        } catch (Exception e) {
            Rlog.fatal("common", "{EXCEPTION IN CONVERTING TO String:}" + e);
            return returnString;
        }
    }
    
    public static String integerToString(Integer i) {

    	String returnString = null;
    	try {
    		if (i == null) {
    			return returnString;
    		}

    		returnString = i.toString();
    		return returnString;

    	} catch (Exception e) {
    		Rlog.fatal("common", "{EXCEPTION IN CONVERTING TO String:}" + e);
    		return returnString;
    	}

    }

    public static Integer stringToInteger(String str) {
    	Integer returnInteger = null;

    	// Rlog.debug("common","EJBUtil stringToInteger value of Str" + str);

    	try {

    		if (str != null) {
    			// Rlog.debug("common","inside str!=null");
    			str = str.trim();
    			if (str.length() > 0) {
    				// Rlog.debug("common","inside str.length>0");
    				returnInteger = Integer.valueOf(str);
    			}
    		}
    		return returnInteger;
    	} catch (Exception e) {
    		Rlog.fatal("common", "{EXCEPTION IN CONVERTING TO Integer:} " + e);
    		return returnInteger;
    	}
    }
    
    public static ArrayList strArrToArrayList(String[] param) {
        return new ArrayList(Arrays.asList(param));
    }
    
    /*
     * Method to create a drop down with String as value @param name of the HTML
     * element select @param already selected value @param Ids of data value of
     * dropdown @param Desc - display value of dropdown
     */
    public static String createPullDownWithStr(String dName, String selValue,
            ArrayList id, ArrayList desc) {
        String idVal = "";

        StringBuffer mainStr = new StringBuffer();
        String objType = "java.lang.Integer";
        Integer integerCodeId;
        boolean isString = false;

        try {
            int counter = 0;
            // check whether ID is of type Integer or String

            mainStr.append("<SELECT NAME=" + dName + ">");

            for (counter = 0; counter <= desc.size() - 1; counter++) {
                objType = (((Object) id.get(counter)).getClass()).getName();
                if ((objType.toUpperCase()).equals("JAVA.LANG.STRING")) {
                    isString = true;
                } else {
                    isString = false;
                }

                if (isString) {
                    idVal = (String) id.get(counter);
                } else {
                    integerCodeId = (Integer) id.get(counter);
                    idVal = integerToString(integerCodeId);
                }

                if (selValue.equals(idVal)) {
                    mainStr.append("<OPTION value = '" + idVal + "' SELECTED>"
                            + desc.get(counter) + "</OPTION>");
                } else {
                    mainStr.append("<OPTION value = '" + idVal + "'>"
                            + desc.get(counter) + "</OPTION>");
                }
            }
            if (selValue == null || selValue.equals("")
                    || selValue.equals("null") || selValue.equals("0")) {
                mainStr
                		.append("<OPTION value='' SELECTED>").append(LC.L_Select_AnOption).append("</OPTION>");
            } else
                mainStr.append("<OPTION value='' >").append(LC.L_Select_AnOption).append("</OPTION>");

            mainStr.append("</SELECT>");
        } catch (Exception e) {
            e.printStackTrace();
            return "Exception in createPullDown()" + e;

        }
        return mainStr.toString();
    }
    
    /*
     * Method to create a drop down @param name of the HTML element select
     * @param already selected value @param Ids of data value of dropdown @param
     * Desc - display value of dropdown
     */
//  Modified by Gopu to fix the bugzilla issue #2818 (Resize and make size static for the Study Number dropdown.)
    public static String createPullDown(String dName, int selValue,
            ArrayList id, ArrayList desc) {
        String strCodeId;
        Integer integerCodeId;

        StringBuffer mainStr = new StringBuffer();

        try {
            int counter = 0;
            String objType = "java.lang.Integer";
            boolean isString = false;
            int intcode = 0;

            // check whether ID is of type Integer or String
            if (id.size() > 0) {
                objType = (((Object) id.get(counter)).getClass()).getName();
            }

            if ((objType.toUpperCase()).equals("JAVA.LANG.STRING")) {
                isString = true;
            }

            mainStr.append("<SELECT NAME=" + dName + " STYLE='WIDTH:177px' >");

            Integer val = new java.lang.Integer(selValue);

            for (counter = 0; counter <= desc.size() - 1; counter++) {

                if (isString) {
                    strCodeId = (String) id.get(counter);
                    intcode = stringToNum(strCodeId);
                } else {
                    integerCodeId = (Integer) id.get(counter);
                    intcode = integerCodeId.intValue();

                }

                if (selValue == intcode) {
                    mainStr.append("<OPTION value = " + intcode + " SELECTED>"
                            + desc.get(counter) + "</OPTION>");
                } else {
                    mainStr.append("<OPTION value = " + intcode + ">"
                            + desc.get(counter) + "</OPTION>");
                }
            }
            if (val.toString().equals("") || val.toString().equals(null)
                    || val.toString().equals("0")) {
                mainStr
                		.append("<OPTION value='' SELECTED>").append(LC.L_Select_AnOption).append("</OPTION>");
            } else
                mainStr.append("<OPTION value='' >").append(LC.L_Select_AnOption).append("</OPTION>");

            mainStr.append("</SELECT>");
        } catch (Exception e) {
            return "Exception in createPullDown()" + e;
        }
        return mainStr.toString();
    }
    
    public static String getRelativeTime(Calendar cal, String timeCode) {
        //
        int month = 0;
        int yr = 0;
        int day = 0;
        int maxmonthday = 0;
        int day_of_week = 0;
        String returnStr = "";

        month = cal.get(Calendar.MONTH) + 1;
        yr = cal.get(Calendar.YEAR);
        day = cal.get(Calendar.DAY_OF_MONTH);

        // THIS MONTH

        if (timeCode.equals("TM")) {
            maxmonthday = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
            returnStr = " between  to_date('" + month + "/01/" + yr
                    + "','mm/dd/yyyy') and to_date('" + month + "/"
                    + maxmonthday + "/" + yr + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // THIS WEEK

        if (timeCode.equals("TW")) {
            day_of_week = cal.get(Calendar.DAY_OF_WEEK);
            cal.add(Calendar.DAY_OF_MONTH, -(day_of_week - 1));
            returnStr = " between to_date('" + (cal.get(Calendar.MONTH) + 1)
                    + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";
            // for end date of the week
            cal.add(Calendar.DAY_OF_MONTH, 6);
            returnStr = returnStr + " and to_date('"
                    + (cal.get(Calendar.MONTH) + 1) + "/"
                    + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // THIS YEAR

        if (timeCode.equals("TY")) {
            returnStr = " between  to_date('01/01/" + yr
                    + "','mm/dd/yyyy') and to_date('12/31/" + yr
                    + "','mm/dd/yyyy')";
            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // TODAY

        if (timeCode.equals("T")) {
            returnStr = " = to_date('" + month + "/" + day + "/" + yr
                    + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // LAST WEEK

        if (timeCode.equals("LW")) {

            cal.add(Calendar.WEEK_OF_MONTH, -1);
            day_of_week = cal.get(Calendar.DAY_OF_WEEK);
            cal.add(Calendar.DAY_OF_MONTH, -(day_of_week - 1));
            returnStr = " between to_date('" + (cal.get(Calendar.MONTH) + 1)
                    + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";
            cal.add(Calendar.DAY_OF_MONTH, 6);
            returnStr = returnStr + " and to_date('"
                    + (cal.get(Calendar.MONTH) + 1) + "/"
                    + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // LAST MONTH
        if (timeCode.equals("LM")) {
            cal.add(Calendar.MONTH, -1);
            maxmonthday = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
            returnStr = "  between  to_date('" + (cal.get(Calendar.MONTH) + 1)
                    + "/01/" + cal.get(Calendar.YEAR)
                    + "','mm/dd/yyyy') and to_date('"
                    + (cal.get(Calendar.MONTH) + 1) + "/" + maxmonthday + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // LAST YEAR
        if (timeCode.equals("LY")) {
            cal.add(Calendar.YEAR, -1);
            returnStr = " between  to_date('01/01/" + cal.get(Calendar.YEAR)
                    + "','mm/dd/yyyy') and to_date('12/31/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // YESTERDAY

        if (timeCode.equals("Y")) {
            cal.add(Calendar.DAY_OF_MONTH, -1);
            returnStr = "  = to_date('" + (cal.get(Calendar.MONTH) + 1) + "/"
                    + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";
            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // TOMORROW

        if (timeCode.equals("TO")) {
            cal.add(Calendar.DAY_OF_MONTH, +1);
            returnStr = "  = to_date('" + (cal.get(Calendar.MONTH) + 1) + "/"
                    + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";
            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // NEXT WEEK

        if (timeCode.equals("NW")) {

            cal.add(Calendar.WEEK_OF_MONTH, +1);
            day_of_week = cal.get(Calendar.DAY_OF_WEEK);
            cal.add(Calendar.DAY_OF_MONTH, -(day_of_week - 1));
            returnStr = " between to_date('" + (cal.get(Calendar.MONTH) + 1)
                    + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";
            cal.add(Calendar.DAY_OF_MONTH, 6);
            returnStr = returnStr + " and to_date('"
                    + (cal.get(Calendar.MONTH) + 1) + "/"
                    + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // NEXT MONTH
        if (timeCode.equals("NM")) {
            cal.add(Calendar.MONTH, +1);
            maxmonthday = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
            returnStr = " between  to_date('" + (cal.get(Calendar.MONTH) + 1)
                    + "/01/" + cal.get(Calendar.YEAR)
                    + "','mm/dd/yyyy') and to_date('"
                    + (cal.get(Calendar.MONTH) + 1) + "/" + maxmonthday + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // NEXT YEAR
        if (timeCode.equals("NY")) {
            cal.add(Calendar.YEAR, +1);
            returnStr = "  between  to_date('01/01/" + cal.get(Calendar.YEAR)
                    + "','mm/dd/yyyy') and to_date('12/31/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        return returnStr;

        // end of method

    }

    /**
     * More explicit version of getRelativeTime. It serves the same purpose as getRelativeTime
     * but this method is intended to get around some Oracle bugs by not using "between".
     *
     * @param lhs - "left-hand side" of an equation: like in lhs >= something
     * @param cal - Calendar with today's date for calculating the range
     * @param timeCode - keyword such as "TM" for This Month, "TW" for This Week, etc.
     * @return an expression that can be used in the where clause of SQL
     */
    public static String getRelativeTimeLong(String lhs, Calendar cal, String timeCode) {

        int month = 0;
        int yr = 0;
        int day = 0;
        int maxmonthday = 0;
        int day_of_week = 0;
        String returnStr = "";

        month = cal.get(Calendar.MONTH) + 1;
        yr = cal.get(Calendar.YEAR);
        day = cal.get(Calendar.DAY_OF_MONTH);

        // THIS MONTH
        if (timeCode.equals("TM")) {
            maxmonthday = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
            returnStr = " " + lhs + " >= to_date('" + month + "/01/" + yr + "','mm/dd/yyyy') "
                    + " and " + lhs + " <= to_date('" + month + "/"
                    + maxmonthday + "/" + yr + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;
        }

        // THIS WEEK
        if (timeCode.equals("TW")) {
            day_of_week = cal.get(Calendar.DAY_OF_WEEK);
            cal.add(Calendar.DAY_OF_MONTH, -(day_of_week - 1));
            returnStr = " " + lhs + " >= to_date('" + (cal.get(Calendar.MONTH) + 1)
                    + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";
            // for end date of the week
            cal.add(Calendar.DAY_OF_MONTH, 6);
            returnStr += " and " + lhs + " <= to_date('"
                    + (cal.get(Calendar.MONTH) + 1) + "/"
                    + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // THIS YEAR
        if (timeCode.equals("TY")) {
            returnStr = " " + lhs + " >= to_date('01/01/" + yr + "','mm/dd/yyyy') "
                    +" and " + lhs +" <= to_date('12/31/" + yr + "','mm/dd/yyyy') ";
            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // TODAY
        if (timeCode.equals("T")) {
            returnStr = " " + lhs + " = to_date('" + month + "/" + day + "/" + yr
                    + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // LAST WEEK
        if (timeCode.equals("LW")) {

            cal.add(Calendar.WEEK_OF_MONTH, -1);
            day_of_week = cal.get(Calendar.DAY_OF_WEEK);
            cal.add(Calendar.DAY_OF_MONTH, -(day_of_week - 1));
            returnStr = " " + lhs + " >= to_date('" + (cal.get(Calendar.MONTH) + 1)
                    + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";
            cal.add(Calendar.DAY_OF_MONTH, 6);
            returnStr += " and " + lhs + " <= to_date('"
                    + (cal.get(Calendar.MONTH) + 1) + "/"
                    + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // LAST MONTH
        if (timeCode.equals("LM")) {
            cal.add(Calendar.MONTH, -1);
            maxmonthday = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
            returnStr = " " + lhs + " >=  to_date('" + (cal.get(Calendar.MONTH) + 1)
                    + "/01/" + cal.get(Calendar.YEAR)
                    + "','mm/dd/yyyy') and " + lhs + " <= to_date('"
                    + (cal.get(Calendar.MONTH) + 1) + "/" + maxmonthday + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // LAST YEAR
        if (timeCode.equals("LY")) {
            cal.add(Calendar.YEAR, -1);
            returnStr = " " + lhs + " >= to_date('01/01/" + cal.get(Calendar.YEAR)
                    + "','mm/dd/yyyy') and " + lhs + " <= to_date('12/31/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // YESTERDAY
        if (timeCode.equals("Y")) {
            cal.add(Calendar.DAY_OF_MONTH, -1);
            returnStr = " " + lhs + " = to_date('" + (cal.get(Calendar.MONTH) + 1) + "/"
                    + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";
            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // TOMORROW
        if (timeCode.equals("TO")) {
            cal.add(Calendar.DAY_OF_MONTH, +1);
            returnStr = " " + lhs + " = to_date('" + (cal.get(Calendar.MONTH) + 1) + "/"
                    + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";
            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // NEXT WEEK
        if (timeCode.equals("NW")) {

            cal.add(Calendar.WEEK_OF_MONTH, +1);
            day_of_week = cal.get(Calendar.DAY_OF_WEEK);
            cal.add(Calendar.DAY_OF_MONTH, -(day_of_week - 1));
            returnStr = " " + lhs +" >= to_date('" + (cal.get(Calendar.MONTH) + 1)
                    + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";
            cal.add(Calendar.DAY_OF_MONTH, 6);
            returnStr += " and " + lhs + " <= to_date('"
                    + (cal.get(Calendar.MONTH) + 1) + "/"
                    + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // NEXT MONTH
        if (timeCode.equals("NM")) {
            cal.add(Calendar.MONTH, +1);
            maxmonthday = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
            returnStr = " " + lhs + " >= to_date('" + (cal.get(Calendar.MONTH) + 1)
                    + "/01/" + cal.get(Calendar.YEAR)
                    + "','mm/dd/yyyy') and " + lhs + " <= to_date('"
                    + (cal.get(Calendar.MONTH) + 1) + "/" + maxmonthday + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // NEXT YEAR
        if (timeCode.equals("NY")) {
            cal.add(Calendar.YEAR, +1);
            returnStr = " " + lhs + " >= to_date('01/01/" + cal.get(Calendar.YEAR)
                    + "','mm/dd/yyyy') and " + lhs + " <= to_date('12/31/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }
        
        // NEXT 7 DAYS
        if (timeCode.equals("N7")) {
            cal.add(Calendar.DAY_OF_MONTH, 1);
            returnStr = " " + lhs +" >= to_date('" + (cal.get(Calendar.MONTH) + 1)
                    + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";
            cal.add(Calendar.DAY_OF_MONTH, 6);
            returnStr += " and " + lhs + " <= to_date('"
                    + (cal.get(Calendar.MONTH) + 1) + "/"
                    + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;
        }
        
        // NEXT 14 DAYS
        if (timeCode.equals("N14")) {
            cal.add(Calendar.DAY_OF_MONTH, 1);
            returnStr = " " + lhs +" >= to_date('" + (cal.get(Calendar.MONTH) + 1)
                    + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";
            cal.add(Calendar.DAY_OF_MONTH, 13);
            returnStr += " and " + lhs + " <= to_date('"
                    + (cal.get(Calendar.MONTH) + 1) + "/"
                    + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;
        }

        return returnStr;
    }
 
 /**
  * Convert every non-extended ASCII character in a string to unicode in "&#NNNNN;" format.
  * This format is how these characters are stored in database.
  * Extended ASCII characters (including ASCII characters) are left as they are.
  *  
  * @param str - input string
  * @return String with non-extended ASCII characters converted as unicode points.
  */
 private static String htmlRefPrefix = "&#";
 private static String htmlRefSuffix = ";";
 public static String htmlUnicodePoint(String str) {
     if (str == null || str.length() < 1) { return str; }
     StringBuffer sb = new StringBuffer();
     for (int iX=0; iX<str.length(); iX++) {
         if (str.codePointAt(iX) > 255) {
             sb.append(htmlRefPrefix).append(str.codePointAt(iX)).append(htmlRefSuffix);
         } else {
             sb.append(str.charAt(iX));
         }
     }
     return sb.toString();
 }
 public static boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    	case 'V': return ((pageRight & 0x0004) >> 2) == 1;
	    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
	    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
 }
}
