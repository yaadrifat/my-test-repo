/*
 * Classname			UpdateSchedulesAgentBean.class
 * 
 * Version information
 *
 * Date					03/05/2008
 * 
 * Author : Manimaran
 * 
 */

package com.velos.eres.service.updateSchedulesAgent.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.velos.eres.business.common.UpdateSchedulesDao;
import com.velos.eres.service.updateSchedulesAgent.UpdateSchedulesAgentRObj;
import com.velos.eres.service.util.Rlog;


@Stateless
public class UpdateSchedulesAgentBean implements UpdateSchedulesAgentRObj {
    
    public int discShedulesAllPatient(int discSchCal, String discDate, String discReason, int newSchCal, String newSchStartDate, int usr, String ipAdd) {
    	int ret = 0;
    	try{
    		UpdateSchedulesDao schDao = new UpdateSchedulesDao();
    		ret = schDao.discShedulesAllPatient(discSchCal, discDate, discReason, newSchCal, newSchStartDate, usr, ipAdd);
    		return ret;
    	} catch(Exception e) {
    		Rlog.fatal("updateSchedules", "Exception in discShedulesAllPatients" + e);
    		
    	}
    	return -1;
    }

    
    
    public int updateEventStatusCurrAndDisc(int eveStatDiscCal, String scheduleFlag, String dateOccurDisc, String allEveDate, int dstatusDisc, String allEveStatDate, int usr, String ipAdd ) {
    	int ret = 0;
    	try{
    		UpdateSchedulesDao schDao = new UpdateSchedulesDao();
    		ret = schDao.updateEventStatusCurrAndDisc(eveStatDiscCal, scheduleFlag, dateOccurDisc, allEveDate, dstatusDisc, allEveStatDate, usr, ipAdd);
    		return ret;
    	} catch(Exception e) {
    		Rlog.fatal("updateSchedules", "Exception in updateEventStatusCurrAndDisc" + e);
    		
    	}
    	return -1;
    }
    
    
    public int generateNewSchedules(int newSchAllCal, String patSchStartDate, int patEnrollDtCheck, int usr, String ipAdd ) {
    	int ret = 0;
    	try{
    		UpdateSchedulesDao schDao = new UpdateSchedulesDao();
    		ret = schDao.generateNewSchedules(newSchAllCal, patSchStartDate, patEnrollDtCheck, usr, ipAdd);
    		return ret;
    	} catch(Exception e) {
    		Rlog.fatal("updateSchedules", "Exception in updateEventStatusCurrAndDisc" + e);
    		
    	}
    	return -1;
    }      



}// end of class

