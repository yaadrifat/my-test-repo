package com.velos.eres.widget.service.util;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.SectionDao;
import com.velos.eres.business.common.StudyIdDao;
import com.velos.eres.business.section.impl.SectionBean;
import com.velos.eres.service.sectionAgent.SectionAgent;
import com.velos.eres.service.studyVerAgent.StudyVerAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.MC;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.codelst.CodelstJB;
import com.velos.eres.web.study.StudyJB;
import com.velos.eres.web.studyId.StudyIdJB;
import com.velos.eres.web.user.UserJB;
import com.velos.eres.widget.business.common.UIFlxPageDao;

public class FlxPageFieldHtml {
	private static final String FIELD_TYPE = "fieldType";
	private static final String EMPTY_STRING = "";
	private static final String ID = "id";
	private static final String NAME = "name";
	private static final String Str_input = "input", Str_lookup = "lookup", Str_chkbox = "chkbox", Str_checkbox = "checkbox", 
			Str_dropdown = "dropdown", Str_splfld = "splfld", Str_date = "date", Str_RO_input = "readonly-input", 
			Str_Hidden_input = "hidden-input", Str_fields = "fields", Str_flxFieldId = "flxFieldId", Str_keyword = "keyword",
			Str_type = "type", Str_STD = "STD", Str_mandatory = "mandatory";
	private static final String Str_selection_single = "single", Str_selection_multi = "multi";
	private static final String Str_tarea="textarea";
	private static final String Str_tarea_RO="readonly-textarea";
	
	private JSONArray jsSectionArray = null;
	
	private HashMap<FlxPageFields, JSONObject> flxFieldStdJsonMap = null;
	
	public FlxPageFieldHtml() {}
	
	public FlxPageFieldHtml(JSONArray jsSectionArray) {
		this.jsSectionArray = jsSectionArray;
		flxFieldStdJsonMap = new HashMap<FlxPageFields, JSONObject>();
		try {
			for (int iX = 0; iX < this.jsSectionArray.length(); iX++) {
				JSONObject section = this.jsSectionArray.getJSONObject(iX);
				JSONArray fieldArray = section.getJSONArray(Str_fields);
				for (int iY = 0; iY < fieldArray.length(); iY++) {
					JSONObject field = fieldArray.getJSONObject(iY);
					if (!Str_STD.equals(field.getString(Str_type))) { continue; }
					FlxPageFields key = FlxPageFields.getFlxPageFieldByKey(field.getString(Str_flxFieldId));
					if (key == null) { continue; }
					flxFieldStdJsonMap.put(key, field);
				}
			}
			JSONObject flxField = flxFieldStdJsonMap.get(FlxPageFields.STUDY_PIAUTHOR);
			//System.out.println(flxField.getString(Str_keyword));
		} catch(Exception e) {}
	}
	

	public JSONObject getFlxPageFieldHtmlMd(Object...args) { 
		String fldPage = (String) args[0];
		String fldPageCodeSubtype = (String) args[1];
		if (StringUtil.isEmpty(fldPage) || StringUtil.isEmpty(fldPageCodeSubtype)) { return null; }

		JSONObject fldJSON = new JSONObject();
		if ("STUDY".equals(fldPage)) {
			StudyIdDao sidDao = new StudyIdDao();
			sidDao = (StudyIdDao)args[2];
			return getFlxPageFieldHTML_StudyMD(fldPageCodeSubtype, sidDao);
		}
		String fldHTML = EMPTY_STRING;//pageField.buildFlxPageFieldHTML_STD(fldHTMLSpecJSON);
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}

	public JSONObject getPageFieldHtmlStd(FlxPageFields field, StudyJB studyJB, HttpSession session) {
		switch(field) {
		case STUDY_STUDYNUMBER:
			return doStudyNumber(field, studyJB);
		case STUDY_STUDYTITLE:
			return doStudyTitle(field, studyJB);
		case STUDY_DISEASESITES:
			return doStudyDiseaseSites(field, studyJB);
		case STUDY_DATAMANAGER:
			return doStudyDataManager(field, studyJB, session);
		case STUDY_OBJECTIVE:
			return doStudyObjective(field, studyJB);
		case STUDY_STUDYSUMM:
			return doStudySummary(field, studyJB);
		case STUDY_PIAUTHOR:
			return doStudyPiAuthor(field, studyJB);
		case STUDY_PIOTHER:
			return doStudyPiOther(field, studyJB);
		case STUDY_STUDYCONTACT:
			return doStudyStudyContact(field, studyJB);
		case STUDY_PINVCHECK:
			return doStudyCheck(field, studyJB);
		case STUDY_CRTP_REPORTABLE:
			return doStudyCtrpReportable(field, studyJB);
		case STUDY_FDA_REGULATED:
			return doStudyFdaRegulated(field, studyJB);
		case STUDY_NCTNUMBER:
			return doStudyNctNumber(field, studyJB);
		case STUDY_PPURPOSE:
			return doStudyPurpose(field, studyJB);
		case STUDY_TAREA:
			return doStudyTArea(field, studyJB);
		case STUDY_AGENTDEVICE:
			return doStudyAgentDevice(field, studyJB);
		case STUDY_SPECIFICSITES1:
			return doStudySpecificSites1(field, studyJB);
		case STUDY_SPECIFICSITES2:
			return doStudySpecificSites2(field, studyJB);
		case STUDY_SAMPLESIZE:
			return doStudySampleSize(field, studyJB);
		case STUDY_STUDYDURN:
			return doStudyDuration(field, studyJB);
		case STUDY_STUDYDURN_DROP:
			return doStudyDurationDropDown(field, studyJB);
		case STUDY_DIVISION:
			return doStudyDivision(field, studyJB);
		case STUDY_CCSG:
			return doStudyCcsgReportable(field, studyJB);
		case STUDY_ESTIMATEDBEGINDATE:
			return doStudyEstimatedBeginDate(field, studyJB);
		case STUDY_PHASE:
			return doStudyPhase(field, studyJB);
		case STUDY_RESEARCHTYPE:
			return doStudyResearchType(field, studyJB);
		case STUDY_STUDYSCOPE:
			return doStudyScope(field, studyJB);
		case STUDY_STUDYTYPE:
			return doStudyType(field, studyJB);
		case STUDY_LINKEDTO:
			return doStudyLinkedTo(field, studyJB);
		case STUDY_BLINDING:
			return doStudyBlinding(field, studyJB);
		case STUDY_RANDOMIZATION:
			return doStudyRandomization(field, studyJB);
		case STUDY_SPONSOR:
			return doStudySponsor(field, studyJB);
		case STUDY_SPONSOR_IFOTHER:
			return doStudySponsorIfOther(field, studyJB);
		case STUDY_SPONSORID:
			return doStudySponsorId(field, studyJB);
		case STUDY_CONTACT:
			return doStudyContact(field, studyJB);
		case STUDY_KEYWORDS:
			return doStudyKeywords(field, studyJB);
		default:
		}
		return null;
	}

	private JSONObject doStudyNumber(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() + "<font id=\"mandsnumber\" class=\"Mandatory\"> **</font></TD>";
		fldHTML += "<TD>";
		String fldValue = (studyJB == null || studyJB.getStudyNumber() == null) ? EMPTY_STRING : studyJB.getStudyNumber();
		try {
			fldJSON.put("fldValue", fldValue);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if ("input".equals(fldType)) {
			fldHTML += "<input type='text' ";
			try {
				fldHTML += "id='"+ fldHTMLSpecJSON.get(ID) +"' ";
				fldHTML += "name='"+ fldHTMLSpecJSON.get(NAME) +"' ";
				fldHTML += "size='"+ fldHTMLSpecJSON.get("size") +"' ";
				fldHTML += "maxlength='"+ fldHTMLSpecJSON.get("maxlength") +"' ";
				fldHTML += "value='"+ fldValue +"' ";
			} catch (JSONException e) {
				e.printStackTrace();
			}
			fldHTML += "/>";
			fldHTML += "<input type='hidden' id='studyNumberOld' name='studyNumberOld' value='"+ fldValue +"'/>";
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}
	
	private JSONObject doStudyDiseaseSites(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldRowHTML = "";
		String fldHTML = "";
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = "";
			e.printStackTrace();
		}
		fldRowHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldRowHTML += "<TD>";
		String disSiteId = null;
		try {
			disSiteId = (String)fldHTMLSpecJSON.getString("value");
		} catch(Exception e) {}
		if (disSiteId == null) {
			disSiteId = EMPTY_STRING;
		}
		fldHTML +="<input type='hidden' id='disSiteid' name='disSiteid' value='" + disSiteId + "'/>";
		if ("lookup".equals(fldType)) {
			fldHTML += "<input type='text' ";
			try {
				fldHTML += "class ='readonly-input'";
				fldHTML += "id='"+ fldHTMLSpecJSON.get(ID) +"' ";
				fldHTML += "name='"+ fldHTMLSpecJSON.get(NAME) +"' ";
				fldHTML += "size='"+ fldHTMLSpecJSON.get("size") +"' ";
				fldHTML += " READONLY ";
			} catch (JSONException e) {
				e.printStackTrace();
				fldRowHTML += "</TD>";
				try {
					fldJSON.put("fldHTML", fldRowHTML);
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
				return fldJSON;
			}
			String fldValue = EMPTY_STRING;
			if(studyJB != null && studyJB.getDisSite() != null){
				StringBuffer sb1 = new StringBuffer();
				String[] pkDisease = studyJB.getDisSite().split(",");
				CodeDao cdDao = new CodeDao();
				for (int iX = 0; iX < pkDisease.length; iX++) {
					int pkDis = 0;
					try {
						pkDis = Integer.parseInt(pkDisease[iX]);
						if (pkDis < 1) { continue; }
						sb1.append(cdDao.getCodeDescription(pkDis)).append(";");
					} catch(Exception e) {}
				}
				if (sb1.length() > 0) {
					sb1.deleteCharAt(sb1.length()-1);
				}
				fldValue = sb1.toString();
			}
			fldHTML += "value='"+ fldValue + "' ";
			fldHTML += "/>";
			
			try {
				fldJSON.put("fldValue", fldValue);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}

			//lookup link
			String linkHref = "";
			String linkText = "";
			String linkOnClick = "";
			try {
				linkHref = fldHTMLSpecJSON.getString("linkHref");
				linkHref = (StringUtil.isEmpty(linkHref))? "#" : linkHref;
		 		linkText = fldHTMLSpecJSON.getString("linkText");
		 		linkText = (StringUtil.isEmpty(linkText))? LC.L_Select : linkText;
		 		linkOnClick = fldHTMLSpecJSON.getString("onClick");
		 		linkOnClick = (StringUtil.isEmpty(linkOnClick))? " " : linkOnClick; 
			} catch (JSONException e) {
				linkHref = (StringUtil.isEmpty(linkHref))? "#" : linkHref;
		 		linkText = (StringUtil.isEmpty(linkText))? LC.L_Select_DiseaseSites : linkText;
		 		linkOnClick = ""; 
			}
			fldHTML += "&nbsp;<a href='"+ linkHref + "' "
			 		+ "onclick='" + linkOnClick + "'>"
			 		+ linkText
			 		+"</a>"; 
		}
		fldRowHTML += fldHTML + "</TD>";
		fldRowHTML += "<script>document.studyScreenForm.disSiteid.value=\""
				+((studyJB == null || studyJB.getDisSite()==null)?EMPTY_STRING:studyJB.getDisSite())+"\"</script>";

		try {
			fldJSON.put("fldHTML", fldRowHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}
	
	private JSONObject doStudyTitle(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() + "<font id=\"mandtitle\" class=\"Mandatory\"> **</font></TD>";
		fldHTML += "<TD>";
		String fldValue = (studyJB == null || studyJB.getStudyTitle() == null) ? EMPTY_STRING : studyJB.getStudyTitle();
		try {
			fldJSON.put("fldValue", fldValue);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if ("textarea".equals(fldType)) {
			fldHTML += "<textarea type='text' ";
			try {
				fldHTML += "id='"+ fldHTMLSpecJSON.get(ID) +"' ";
				fldHTML += "name='"+ fldHTMLSpecJSON.get(NAME) +"' ";
				fldHTML += "rows='"+ fldHTMLSpecJSON.get("rows") +"' ";
				fldHTML += "cols='"+ fldHTMLSpecJSON.get("cols") +"' ";
				fldHTML += "maxlength='"+ fldHTMLSpecJSON.get("maxlength") +"' ";
			} catch (JSONException e) {
				e.printStackTrace();
			}
			fldHTML += ">" + fldValue + "</textarea>";
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}
	
	private JSONObject doStudyDataManager(FlxPageFields field, StudyJB studyJB, HttpSession session) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldRowHTML = null;
		String fldHTML = null;
		String fldType;
		boolean isNewStudy = false;
		
		int userId = StringUtil.stringToNum((String)session.getAttribute("userId"));
		if (userId <= 0){
			return null;
		}		
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldRowHTML = "<TD width='25%'>"+ field.getFieldLabel() + "<font id=\"mandstudyent\" class=\"Mandatory\"> **</font></TD>";
		fldRowHTML += "<TD>";
		int fldPK = 0;
 		try {
 			String authorStr = (studyJB == null || studyJB.getStudyAuthor() == null) ? "0" : studyJB.getStudyAuthor();
 			fldPK = StringUtil.stringToNum(authorStr);
 			if (fldPK == 0) {
 				isNewStudy = true;
 			}
 		} catch (Exception e) {
 			e.printStackTrace();
 			fldPK = 0;
 		}
 		if (isNewStudy) {
 			fldPK = userId;
 		}
 		fldHTML = "<input type='hidden' id='dataManager' name='dataManager' value='"+ fldPK +"' />"; 
		
		if ("lookup".equals(fldType)) {
			fldHTML += "<input type='text' ";
			try {
				fldHTML += "class='readonly-input'";
				fldHTML += "id='"+ fldHTMLSpecJSON.get(ID) +"' ";
				fldHTML += "name='"+ fldHTMLSpecJSON.get(NAME) +"' ";
				fldHTML += " READONLY ";
			} catch (JSONException e) {
				e.printStackTrace();
				fldRowHTML += "</TD";
				try {
					fldJSON.put("fldHTML", fldRowHTML);
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
				return fldJSON;
			}
			String fldValue = EMPTY_STRING;
			if(fldPK > 0) {
				UserJB userB = new UserJB();
				userB.setUserId(fldPK);
				userB.getUserDetails();
				fldValue = userB.getUserFirstName() + " " + userB.getUserLastName();
				fldValue = (null == fldValue)? "":fldValue;
			}
			
			fldHTML += "value='" + fldValue + "' ";
			fldHTML += "/>";
			try {
				fldJSON.put("fldValue", fldValue);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}

			//lookup link
			String linkHref = "";
			String linkText = "";
			String linkOnClick = "";
			try {
				linkHref = fldHTMLSpecJSON.getString("linkHref");
				linkHref = (StringUtil.isEmpty(linkHref))? "#" : linkHref;
				linkText = fldHTMLSpecJSON.getString("linkText");
				linkText = (StringUtil.isEmpty(linkText))? LC.L_Select : linkText;
				linkOnClick = fldHTMLSpecJSON.getString("onClick");
				linkOnClick = (StringUtil.isEmpty(linkOnClick))? " " : linkOnClick;
			} catch (JSONException e) {
				e.printStackTrace();
				linkHref = (StringUtil.isEmpty(linkHref))? "#" : linkHref;
				linkText = (StringUtil.isEmpty(linkText))? LC.L_Select : linkText;
				linkOnClick = "";
			}
			fldHTML += "&nbsp;<a href='"+ linkHref + "' "
				+ "onclick='" + linkOnClick + "'>"
				+ linkText
				+"</a>";
		}
		fldRowHTML += fldHTML + "</TD>";
		try {
			fldJSON.put("fldHTML", fldRowHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}

	private JSONObject doStudyObjective(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType = null;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		String fldValue = (studyJB == null || studyJB.getStudyObjective() == null) ? EMPTY_STRING : studyJB.getStudyObjective();
		try {
			fldJSON.put("fldValue", fldValue);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if ("textarea".equals(fldType)) {
			fldHTML += "<textarea type='text' ";
			try {
				fldHTML += "id='"+ fldHTMLSpecJSON.get(ID) +"' ";
				fldHTML += "name='"+ fldHTMLSpecJSON.get(NAME) +"' ";
				fldHTML += "rows='"+ fldHTMLSpecJSON.get("rows") +"' ";
				fldHTML += "cols='"+ fldHTMLSpecJSON.get("cols") +"' ";
				fldHTML += "maxlength='"+ fldHTMLSpecJSON.get("maxlength") +"' ";
			} catch (JSONException e) {
				e.printStackTrace();
			}
			fldHTML += ">" + fldValue + "</textarea>";
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}
	
	private JSONObject doStudySummary(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		if ("textarea".equals(fldType)) {
			fldHTML += "<textarea type='text' ";
			try {
				fldHTML += "id='"+ fldHTMLSpecJSON.get(ID) +"' ";
				fldHTML += "name='"+ fldHTMLSpecJSON.get(NAME) +"' ";
				fldHTML += "rows='"+ fldHTMLSpecJSON.get("rows") +"' ";
				fldHTML += "cols='"+ fldHTMLSpecJSON.get("cols") +"' ";
				fldHTML += "maxlength='"+ fldHTMLSpecJSON.get("maxlength") +"' ";
			} catch (JSONException e) {
				e.printStackTrace();
			}
			String fldValue = (studyJB == null || studyJB.getStudySummary() == null) ? EMPTY_STRING : studyJB.getStudySummary();
			try {
				fldJSON.put("fldValue", fldValue);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}

			fldHTML += ">" + fldValue + "</textarea>";
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}

	private JSONObject doStudyPiAuthor(FlxPageFields field, StudyJB studyJB) {
		JSONObject piAuthorJson = flxFieldStdJsonMap.get(field);
		int mandatory = 0;
		try {
			mandatory = piAuthorJson.getInt(Str_mandatory);
		} catch(Exception e) {}
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		String fldRowHTML = null;
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldRowHTML = "<TD width='25%'>"+ field.getFieldLabel();
		if (mandatory == 1) { fldRowHTML +="<font id=\"mandsnumber\" class=\"Mandatory\"> *</font>"; }
		fldRowHTML += "</TD>";
		fldRowHTML += "<TD>";
		int fldPK = 0;
		try {
			String primaryInv = (studyJB == null || studyJB.getStudyPrimInv() == null) ? EMPTY_STRING : studyJB.getStudyPrimInv();
			fldPK = StringUtil.stringToNum(primaryInv);
		} catch (Exception e) {
			e.printStackTrace();
			fldPK = 0;
		}
		fldHTML = "<input type='hidden' id='prinInv' name='prinInv' value='"+ fldPK +"' />";
		if ("lookup".equals(fldType)) {
			fldHTML += "<input type='text' ";
			try {
				fldHTML += "class='readonly-input' ";
				fldHTML += "id='"+ fldHTMLSpecJSON.get(ID) +"' ";
				fldHTML += "name='"+ fldHTMLSpecJSON.get(NAME) +"' ";
				fldHTML += " READONLY ";
			} catch (JSONException e) {
				e.printStackTrace();
				fldRowHTML += "</TD>";
				try {
					fldJSON.put("fldHTML", fldRowHTML);
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
				return fldJSON;
			}
			String fldValue = EMPTY_STRING;
			if (fldPK > 0){
				UserJB userB = new UserJB();
				userB.setUserId(fldPK);
				userB.getUserDetails();
				fldValue =  userB.getUserFirstName() + " " +userB.getUserLastName(); 
				fldValue = (null==fldValue)? "" : fldValue;
			}
			fldHTML += "value='"+ fldValue +"' ";
			fldHTML += "/>";

			try {
				fldJSON.put("fldValue", fldValue);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}

			//lookup link
			String linkHref = "";
			String linkText = "";
			String linkOnClick = "";
			try {
				linkHref = fldHTMLSpecJSON.getString("linkHref");
				linkHref = (StringUtil.isEmpty(linkHref))? "#" : linkHref;
				linkText = fldHTMLSpecJSON.getString("linkText");
				linkText = (StringUtil.isEmpty(linkText))? LC.L_Select : linkText;
				linkOnClick = fldHTMLSpecJSON.getString("onClick");
				linkOnClick = (StringUtil.isEmpty(linkOnClick))? " " : linkOnClick;
			} catch (JSONException e) {
				e.printStackTrace();
				linkHref = (StringUtil.isEmpty(linkHref))? "#" : linkHref;
				linkText = (StringUtil.isEmpty(linkText))? LC.L_Select : linkText;
				linkOnClick = "";
			}
			fldHTML += "&nbsp;<a href='"+ linkHref + "' "
				+ "onclick='" + linkOnClick + "'>"
				+ linkText
				+"</a>";
		}
		fldRowHTML += fldHTML + "</TD>";
		try {
			fldJSON.put("fldHTML", fldRowHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	
	}
	
	private JSONObject doStudyPiOther(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		String fldValue = (studyJB == null || studyJB.getStudyOtherPrinv() == null) ? EMPTY_STRING : studyJB.getStudyOtherPrinv();
		try {
			fldJSON.put("fldValue", fldValue);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if ("input".equals(fldType)) {
			fldHTML += "<input type='text' ";
			try {
				fldHTML += "id='"+ fldHTMLSpecJSON.get(ID) +"' ";
				fldHTML += "name='"+ fldHTMLSpecJSON.get(NAME) +"' ";
				fldHTML += "value='"+ fldValue +"' ";
			} catch (JSONException e) {
				e.printStackTrace();
			}
			fldHTML += "/>";
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}
	
	private JSONObject doStudyStudyContact(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldRowHTML = null;
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldRowHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldRowHTML += "<TD>";
		int fldPK= 0;
		try {
			String studyContact = (studyJB == null || studyJB.getStudyCoordinator() == null) ? EMPTY_STRING : studyJB.getStudyCoordinator();
			fldPK = StringUtil.stringToNum(studyContact);
		} catch (Exception e) {
			e.printStackTrace();
			fldPK = 0;
		}
		fldHTML =  "<input type ='hidden' id ='studyco' name ='studyco' value ='"+ fldPK +"'/>";
		if ("lookup".equals(fldType)) {
			fldHTML += "<input type='text' ";
			try {
				fldHTML += "class='readonly-input' ";
				fldHTML += "id='"+ fldHTMLSpecJSON.get(ID) +"' ";
				fldHTML += "name='"+ fldHTMLSpecJSON.get(NAME) +"' ";
				fldHTML += " READONLY";
			} catch (JSONException e) {
				e.printStackTrace();
				fldRowHTML += "</TD>";
				try {
					fldJSON.put("fldHTML", fldRowHTML);
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
				return fldJSON;
			}
			String fldValue = EMPTY_STRING;
			if(fldPK > 0){
				UserJB userB = new UserJB();
				userB.setUserId(fldPK);
				userB.getUserDetails();
				fldValue = userB.getUserFirstName() + " " + userB.getUserLastName();
				fldValue = (null==fldValue)?"":fldValue;
			}
			fldHTML += " value='" + fldValue + "' ";
			fldHTML += "/>";
			try {
				fldJSON.put("fldValue", fldValue);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}

			//lookup link
			String linkHref = "";
			String linkText = "";
			String linkOnClick = "";
			try {
				linkHref = fldHTMLSpecJSON.getString("linkHref");
				linkHref = (StringUtil.isEmpty(linkHref)) ? "#" : linkHref;
				linkText = fldHTMLSpecJSON.getString("linkText");
				linkText = (StringUtil.isEmpty(linkText)) ? LC.L_Select_User : linkText;
				linkOnClick = fldHTMLSpecJSON.getString("onClick");
				linkOnClick = (StringUtil.isEmpty(linkOnClick))? " " : linkOnClick;
			} catch (JSONException e) {
				//e.printStackTrace();
				linkHref = (StringUtil.isEmpty(linkHref))? "#" : linkHref;
				linkText = (StringUtil.isEmpty(linkText)) ? LC.L_Select_User : linkText;
				linkOnClick = " ";
			}
			fldHTML += "&nbsp;<a href='"+ linkHref + "' "
			 		+ "onclick='" + linkOnClick + "'>"
			 		+ linkText
			 		+"</a>";
		}
		fldRowHTML += fldHTML + "</TD>";
		try {
			fldJSON.put("fldHTML", fldRowHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}
	
	private JSONObject doStudyCheck(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		if ("checkbox".equals(fldType)) {
			String fldValue = (studyJB == null || studyJB.getMajAuthor() == null) ? 
					EMPTY_STRING : studyJB.getMajAuthor();
			try {
				fldJSON.put("fldValue", fldValue);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}

			fldHTML += "<input type='checkbox' ";
			try {
				fldHTML += "id='"+ fldHTMLSpecJSON.get(ID) +"' ";
				fldHTML += "name='"+ fldHTMLSpecJSON.get(NAME) +"' ";
				fldHTML += ("Y".equals(fldValue)) ? " checked " : EMPTY_STRING ;
				fldHTML += " onclick=\"studyFunctions.setValue(studyFunctions.formObj)\" ";
			} catch (JSONException e) {
				e.printStackTrace();
			}
			fldHTML += "/>";
			fldHTML += "<input type='hidden' value='"+fldValue+"' name='author' />";
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}
	
	private JSONObject doStudyCtrpReportable(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		if ("checkbox".equals(fldType)) {
			String fldValue = (studyJB == null || studyJB.getStudyCtrpReportable() == null) ? 
					EMPTY_STRING : studyJB.getStudyCtrpReportable();
			try {
				fldJSON.put("fldValue", fldValue);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}

			fldHTML += "<input type='checkbox' ";
			try {
				fldHTML += "id='"+ fldHTMLSpecJSON.get(ID) +"' ";
				fldHTML += "name='"+ fldHTMLSpecJSON.get(NAME) +"' ";
				fldHTML += ("1".equals(fldValue)) ? " checked " : EMPTY_STRING ;
				fldHTML += " onclick=\"studyFunctions.fnAddNciNctRow()\" ";
			} catch (JSONException e) {
				e.printStackTrace();
			}
			fldHTML += "/>";
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}
	
	private JSONObject doStudyFdaRegulated(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		if ("checkbox".equals(fldType)) {
			String fldValue = (studyJB == null || studyJB.getFdaRegulatedStudy() == null) ? 
					EMPTY_STRING : studyJB.getFdaRegulatedStudy();
			try {
				fldJSON.put("fldValue", fldValue);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}

			fldHTML += "<input type='checkbox' ";
			try {
				fldHTML += "id='"+ fldHTMLSpecJSON.get(ID) +"' ";
				fldHTML += "name='"+ fldHTMLSpecJSON.get(NAME) +"' ";
				fldHTML += ("1".equals(fldValue)) ? " checked " : EMPTY_STRING ;
			} catch (JSONException e) {
				e.printStackTrace();
			}
			fldHTML += "/>";
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}
	
	private JSONObject doStudyNctNumber(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		String fldValue = (studyJB == null || studyJB.getNctNumber() == null) ? EMPTY_STRING : studyJB.getNctNumber();
		try {
			fldJSON.put("fldValue", fldValue);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if ("input".equals(fldType)) {
			fldHTML += "<input type='text' ";
			try {
				fldHTML += "id='"+ fldHTMLSpecJSON.get(ID) +"' ";
				fldHTML += "name='"+ fldHTMLSpecJSON.get(NAME) +"' ";
				fldHTML += "size='"+ fldHTMLSpecJSON.get("size") +"' ";
				fldHTML += "maxlength='"+ fldHTMLSpecJSON.get("maxlength") +"' ";
				fldHTML += "value='"+ fldValue +"' ";
			} catch (JSONException e) {
				e.printStackTrace();
			}
			fldHTML += "/>";
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}

	private JSONObject doStudyPurpose(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		String fldValue = (studyJB == null || studyJB.getStudyPurpose() == null) ? EMPTY_STRING : studyJB.getStudyPurpose();
		try {
			fldJSON.put("fldValue", fldValue);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if ("dropdown".equals(fldType)) {
			try {
				String codeType = EMPTY_STRING;
				codeType = (String) fldHTMLSpecJSON.get("codelst_type");
				if (StringUtil.isEmpty(codeType))
					return null; 
				CodeDao cdDao = new CodeDao();
				cdDao.getCodeValues(codeType);
				String fldId = (String) fldHTMLSpecJSON.get(ID);
				fldHTML += cdDao.toPullDown(fldId, StringUtil.stringToNum(fldValue));
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}
	
	private JSONObject doStudyTArea(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		fldHTML += "<span id='span_tarea'>";
		String studyDivision = (null == studyJB)? "0" : studyJB.getStudyDivision() ;
		studyDivision = (StringUtil.isEmpty(studyDivision))? "0" : studyDivision; 
		String fldValue = (studyJB == null || studyJB.getStudyTArea() == null) ? EMPTY_STRING : studyJB.getStudyTArea();
		try {
			fldJSON.put("fldValue", fldValue);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if ("dropdown".equals(fldType)) {
			try {
				String codeType = EMPTY_STRING;
				codeType = (String) fldHTMLSpecJSON.get("codelst_type");
				if (StringUtil.isEmpty(codeType))
					return null; 
				CodeDao cdDao = new CodeDao();
				cdDao.getCodeValuesForCustom1(codeType, studyDivision);
				String fldId = (String) fldHTMLSpecJSON.get(ID);
				fldHTML += cdDao.toPullDown(fldId, StringUtil.stringToNum(fldValue));
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		fldHTML += "</span>";
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}

	private JSONObject doStudyAgentDevice(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		String fldValue = (studyJB == null || studyJB.getStudyProduct() == null) ? EMPTY_STRING : studyJB.getStudyProduct();
		try {
			fldJSON.put("fldValue", fldValue);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if ("input".equals(fldType)) {
			fldHTML += "<input type='text' ";
			try {
				fldHTML += "id='"+ fldHTMLSpecJSON.get(ID) +"' ";
				fldHTML += "name='"+ fldHTMLSpecJSON.get(NAME) +"' ";
				fldHTML += "size='"+ fldHTMLSpecJSON.get("size") +"' ";
				fldHTML += "maxlength='"+ fldHTMLSpecJSON.get("maxlength") +"' ";
				fldHTML += "value='"+ fldValue +"' ";
			} catch (JSONException e) {
				e.printStackTrace();
			}
			fldHTML += "/>";
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}
	
	private JSONObject doStudySpecificSites1(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldRowHTML = "";
		String fldHTML = "";
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = "";
			e.printStackTrace();
		}
		fldRowHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldRowHTML += "<TD>";
		//int fldPK = 0;
		if ("lookup".equals(fldType)) {
			fldHTML += "<input type='text' ";
			try {
				fldHTML += "class ='readonly-input'";
				fldHTML += "id='"+ fldHTMLSpecJSON.get(ID) +"' ";
				fldHTML += "name='"+ fldHTMLSpecJSON.get(NAME) +"' ";
				fldHTML += "size='"+ fldHTMLSpecJSON.get("size") +"' ";
				fldHTML += " READONLY";
			} catch (JSONException e) {
				e.printStackTrace();
				fldRowHTML += "</TD>";
				try {
					fldJSON.put("fldHTML", fldRowHTML);
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
				return fldJSON;
			}
			String fldValue = EMPTY_STRING;
			if (studyJB != null && studyJB.getStudyICDCode1() != null) {
				fldValue = studyJB.getStudyICDCode1();
			}
			fldValue = (fldValue == null)?"":fldValue;
			try {
				fldJSON.put("fldValue", fldValue);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}

			fldHTML +=" value='" + fldValue + "' ";
			fldHTML += "/>";
			//lookup link
			String linkHref = "";
			String linkText = "";
			String linkOnClick = "";
			try {
				linkHref = fldHTMLSpecJSON.getString("linkHref");
				linkHref = (StringUtil.isEmpty(linkHref)) ? "#" : linkHref;
				linkText = fldHTMLSpecJSON.getString("linkText");
				linkText = (StringUtil.isEmpty(linkText)) ? LC.L_Select : linkText;
				linkOnClick = fldHTMLSpecJSON.getString("linkOnClick");
				linkOnClick = (StringUtil.isEmpty(linkOnClick)) ? " " : linkOnClick;
			} catch (JSONException e) {
				linkHref = (StringUtil.isEmpty(linkHref)) ? "#" : linkHref;
				linkText = (StringUtil.isEmpty(linkText)) ? LC.L_Select_Site1 : linkText;
				linkOnClick = "";
			}
			fldHTML += "&nbsp;<a href='"+ linkHref + "' " 
					+ "onclick='" + linkOnClick + "'>" 
					+ linkText +"</a>"; 
		}
		fldRowHTML += fldHTML + "</TD>";
		try {
			fldJSON.put("fldHTML", fldRowHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}
	
	private JSONObject doStudySpecificSites2(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldRowHTML = "";
        String fldHTML = "";
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE); //here
		} catch (JSONException e) {
			fldType = "";
			e.printStackTrace();
		}
		fldRowHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>"; //here
		fldRowHTML += "<TD>";
        if ("lookup".equals(fldType)){
        	fldHTML += "<input type='text' ";
            try {
            	fldHTML += "class ='readonly-input'";
                fldHTML += "id='"+ fldHTMLSpecJSON.get(ID) +"' ";
				fldHTML += "name='"+ fldHTMLSpecJSON.get(NAME) +"' ";
				fldHTML += "size='"+ fldHTMLSpecJSON.get("size") +"' ";
				fldHTML += " READONLY";
            } catch (JSONException e) {
				e.printStackTrace();
				fldRowHTML += "</TD>";
				try {
					fldJSON.put("fldHTML", fldRowHTML);
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
				return fldJSON;
			}
            String fldValue = EMPTY_STRING;
            if (studyJB != null && studyJB.getStudyICDCode2() != null) {
            fldValue = studyJB.getStudyICDCode2();
            }
            fldValue = (fldValue == null)?"":fldValue;
            try {
				fldJSON.put("fldValue", fldValue);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}

			fldHTML +=" value='" + fldValue + "' ";
			fldHTML += "/>";
            //lookup link
			String linkHref = "";
			String linkText = "";
			String linkOnClick = "";
			try {
				linkHref = fldHTMLSpecJSON.getString("linkHref");
				linkHref = (StringUtil.isEmpty(linkHref)) ? "#" : linkHref;
				linkText = fldHTMLSpecJSON.getString("linkText");
				linkText = (StringUtil.isEmpty(linkText)) ? LC.L_Select : linkText;
				linkOnClick = fldHTMLSpecJSON.getString("linkOnClick");
				linkOnClick = (StringUtil.isEmpty(linkOnClick)) ? " " : linkOnClick;
			} catch (JSONException e) {
				linkHref = (StringUtil.isEmpty(linkHref)) ? "#" : linkHref;
				linkText = (StringUtil.isEmpty(linkText)) ? LC.L_Select_Site2 : linkText;
				linkOnClick = "";
			}
			fldHTML += "&nbsp;<a href='"+ linkHref + "' " 
					+ "onclick='" + linkOnClick + "'>" 
					+ linkText +"</a>"; 
		}
		fldRowHTML += fldHTML + "</TD>";
		try {
			fldJSON.put("fldHTML", fldRowHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}
	
	private JSONObject doStudySampleSize(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		String fldValue = (studyJB == null || studyJB.getStudyNSamplSize() == null) ? EMPTY_STRING : studyJB.getStudyNSamplSize();
		try {
			fldJSON.put("fldValue", fldValue);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if ("lookup".equals(fldType)) {
			fldHTML += "<input type='text' ";
			try {
				fldHTML += "id='"+ fldHTMLSpecJSON.get(ID) +"' ";
				fldHTML += "name='"+ fldHTMLSpecJSON.get(NAME) +"' ";
				fldHTML += "size='"+ fldHTMLSpecJSON.get("size") +"' ";
				fldHTML += "maxlength='"+ fldHTMLSpecJSON.get("maxlength") +"' ";
				fldHTML += "value='"+ fldValue +"' ";
			} catch (JSONException e) {
				e.printStackTrace();
			}
			fldHTML += "/>";
			//lookup link
			String linkHref = "";
			String linkText = "";
			String linkOnClick = "";
			try {
				linkHref = fldHTMLSpecJSON.getString("linkHref");
				linkHref = (StringUtil.isEmpty(linkHref))? "#" : linkHref;
				linkText = fldHTMLSpecJSON.getString("linkText");
				linkText = (StringUtil.isEmpty(linkText))? LC.L_Select : linkText;
				linkOnClick = fldHTMLSpecJSON.getString("onClick");
				linkOnClick = (StringUtil.isEmpty(linkOnClick))? " " : linkOnClick;
			} catch (JSONException e) {
				e.printStackTrace();
				linkHref = (StringUtil.isEmpty(linkHref))? "#" : linkHref;
				linkText = (StringUtil.isEmpty(linkText))? LC.L_Select : linkText;
				linkOnClick = "";
			}
			fldHTML += "&nbsp;<a href='"+ linkHref + "' "
				+ "onclick='" + linkOnClick + "'>"
				+ linkText
				+"</a>";
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}

	private JSONObject doStudyDuration(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		String fldValue = (studyJB == null || studyJB.getStudyDuration() == null) ? EMPTY_STRING : studyJB.getStudyDuration();
		try {
			fldJSON.put("fldValue", fldValue);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if ("input".equals(fldType)) {
			fldHTML += "<input type='text' ";
			try {
				fldHTML += "id='"+ fldHTMLSpecJSON.get(ID) +"' ";
				fldHTML += "name='"+ fldHTMLSpecJSON.get(NAME) +"' ";
				fldHTML += "size='"+ fldHTMLSpecJSON.get("size") +"' ";
				fldHTML += "maxlength='"+ fldHTMLSpecJSON.get("maxlength") +"' ";
				fldHTML += "value='"+ fldValue +"' ";
			} catch (JSONException e) {
				e.printStackTrace();
			}
			fldHTML += "/>";
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}
	
	private JSONObject doStudyDurationDropDown(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		String fldValue = (studyJB == null || studyJB.getStudyDurationUnit() == null) ? EMPTY_STRING : studyJB.getStudyDurationUnit();
		try {
			fldJSON.put("fldValue", fldValue);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if ("dropdown".equals(fldType)) {
			try {
				fldHTML += "<select id='"+fldHTMLSpecJSON.get(ID)+"' name='"+fldHTMLSpecJSON.get(NAME)+"'>";
				fldHTML += "<option value='days' "+(("days".equals(fldValue)) ? "selected" : EMPTY_STRING)+">"+ LC.L_Days +"</option>";
				fldHTML += "<option value='weeks' "+(("weeks".equals(fldValue)) ? "selected" : EMPTY_STRING)+">"+ LC.L_Weeks +"</option>";
				fldHTML += "<option value='months' "+(("months".equals(fldValue)) ? "selected" : EMPTY_STRING)+">"+ LC.L_Months +"</option>";
				fldHTML += "<option value='years' "+(("years".equals(fldValue)) ? "selected" : EMPTY_STRING)+">"+ LC.L_Years +"</option>";
				fldHTML += "<option value='' "+((EMPTY_STRING.equals(fldValue)) ? "selected" : EMPTY_STRING)+">"+ LC.L_Select_AnOption +"</option>";
				fldHTML += "</select>";
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}
	
	private JSONObject doStudyDivision(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = "";
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		//String fldValue = (studyJB == null || studyJB.getStudyDivision() == null) ? EMPTY_STRING : studyJB.getStudyDivision();
		String fldValue = (studyJB == null || studyJB.getStudyDivision() == null) ? "0" : studyJB.getStudyDivision();
		try {
			fldJSON.put("fldValue", fldValue);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if ("dropdown".equals(fldType)) {
			try {
				String codeType = EMPTY_STRING;
				codeType = (String) fldHTMLSpecJSON.get("codelst_type");
				if (StringUtil.isEmpty(codeType))
					return null; 
				CodeDao cdDao = new CodeDao();
				cdDao.getCodeValues(codeType);
				String fldId = (String) fldHTMLSpecJSON.get(ID);
				//fldHTML += cdDao.toPullDown(fldId, StringUtil.stringToNum(fldValue));
				//fldHTML += cdDao.toPullDown("studyDivision",0, " onChange='flexStudyScreenJS.callAjaxGetTareaDD(flexStudyScreenJS.formObj);'");
				fldHTML += cdDao.toPullDown(fldId, StringUtil.stringToNum(fldValue), "onChange='studyFunctions.callAjaxGetTareaDD(studyFunctions.formObj); studyFunctions.setValueCCSGReport(studyFunctions.formObj)'");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}
	
	private JSONObject doStudyCcsgReportable(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		String fldValue = (studyJB == null || studyJB.getCcsgReportableStudy() == null) ? EMPTY_STRING : studyJB.getCcsgReportableStudy();
		try {
			fldJSON.put("fldValue", fldValue);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if ("checkbox".equals(fldType)) {
			fldHTML += "<input type='checkbox' ";
			try {
				fldHTML += "id='"+ fldHTMLSpecJSON.get(ID) +"' ";
				fldHTML += "name='"+ fldHTMLSpecJSON.get(NAME) +"' ";
				fldHTML += ("1".equals(fldValue) ? "checked" : EMPTY_STRING);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			fldHTML += "/>";
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}
	
	private JSONObject doStudyEstimatedBeginDate(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		String fldValue = (studyJB == null || studyJB.getStudyEstBeginDate() == null) ? EMPTY_STRING : studyJB.getStudyEstBeginDate();
		try {
			fldJSON.put("fldValue", fldValue);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if ("input".equals(fldType)) {
			fldHTML += "<input type='text' class='datefield' ";
			try {
				fldHTML += "id='"+ fldHTMLSpecJSON.get(ID) +"' ";
				fldHTML += "name='"+ fldHTMLSpecJSON.get(NAME) +"' ";
				fldHTML += "size='"+ fldHTMLSpecJSON.get("size") +"' ";
				fldHTML += "maxlength='"+ fldHTMLSpecJSON.get("maxlength") +"' ";
				fldHTML += "value='"+ fldValue +"' ";
			} catch (JSONException e) {
				e.printStackTrace();
			}
			fldHTML += "/>";
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}
	
	private JSONObject doStudyPhase(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		String fldValue = (studyJB == null || studyJB.getStudyPhase() == null) ? EMPTY_STRING : studyJB.getStudyPhase();
		try {
			fldJSON.put("fldValue", fldValue);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if ("dropdown".equals(fldType)) {
			try {
				String codeType = EMPTY_STRING;
				codeType = (String) fldHTMLSpecJSON.get("codelst_type");
				if (StringUtil.isEmpty(codeType))
					return null; 
				CodeDao cdDao = new CodeDao();
				cdDao.getCodeValues(codeType);
				String fldId = (String) fldHTMLSpecJSON.get(ID);
				fldHTML += cdDao.toPullDown(fldId, StringUtil.stringToNum(fldValue));
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}
	
	private JSONObject doStudyResearchType(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		String fldValue = (studyJB == null || studyJB.getStudyResType() == null) ? EMPTY_STRING : studyJB.getStudyResType();
		try {
			fldJSON.put("fldValue", fldValue);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if ("dropdown".equals(fldType)) {
			try {
				String codeType = EMPTY_STRING;
				codeType = (String) fldHTMLSpecJSON.get("codelst_type");
				if (StringUtil.isEmpty(codeType))
					return null; 
				CodeDao cdDao = new CodeDao();
				cdDao.getCodeValues(codeType);
				String fldId = (String) fldHTMLSpecJSON.get(ID);
				fldHTML += cdDao.toPullDown(fldId, StringUtil.stringToNum(fldValue));
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}
	
	private JSONObject doStudyScope(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		String fldValue = (studyJB == null || studyJB.getStudyScope() == null) ? EMPTY_STRING : studyJB.getStudyScope();
		try {
			fldJSON.put("fldValue", fldValue);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if ("dropdown".equals(fldType)) {
			try {
				String codeType = EMPTY_STRING;
				codeType = (String) fldHTMLSpecJSON.get("codelst_type");
				if (StringUtil.isEmpty(codeType))
					return null;
				CodeDao cdDao = new CodeDao();
				cdDao.getCodeValues(codeType);
				String fldId = (String) fldHTMLSpecJSON.get(ID);
				fldHTML += cdDao.toPullDown(fldId, StringUtil.stringToNum(fldValue));
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}
	
	private JSONObject doStudyType(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		String fldValue = (studyJB == null || studyJB.getStudyType() == null) ? EMPTY_STRING : studyJB.getStudyType();
		try {
			fldJSON.put("fldValue", fldValue);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if ("dropdown".equals(fldType)) {
			try {
				String codeType = EMPTY_STRING;
				codeType = (String) fldHTMLSpecJSON.get("codelst_type");
				if (StringUtil.isEmpty(codeType))
					return null; 
				CodeDao cdDao = new CodeDao();
				cdDao.getCodeValues(codeType);
				String fldId = (String) fldHTMLSpecJSON.get(ID);
				fldHTML += cdDao.toPullDown(fldId, StringUtil.stringToNum(fldValue));
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}
	
	private JSONObject doStudyLinkedTo(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		String fldValue = null;
		int fldPK = 0;
		if (studyJB == null || studyJB.getStudyNumber() == null) {
			fldValue = EMPTY_STRING;
		} else {
			String studyAssoc = studyJB.getStudyAssoc();
			if (!StringUtil.isEmpty(studyAssoc)) {
				StudyJB studyAssocB = new StudyJB();
				studyAssocB.setId(StringUtil.stringToNum(studyAssoc));
				studyAssocB.getStudyDetails();
				fldValue = studyAssocB.getStudyNumber();
				fldPK = studyAssocB.getId();
			} else {
				fldValue = EMPTY_STRING;
			}
		}
		try {
			fldJSON.put("fldValue", fldValue);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if ("input".equals(fldType)) {
			fldHTML += "<input type ='hidden' id='studyAssoc' name='studyAssoc' value='"+((fldPK==0)?EMPTY_STRING:String.valueOf(fldPK))+"'/>";
			fldHTML += "<input type='text' ";
			try {
				fldHTML += "class='readonly-input' ";
				fldHTML += "id='"+ fldHTMLSpecJSON.get(ID) +"' ";
				fldHTML += "name='"+ fldHTMLSpecJSON.get(NAME) +"' ";
				fldHTML += "maxlength='"+ fldHTMLSpecJSON.get("maxlength") +"' ";
				fldHTML += "value='"+ fldValue +"' ";
				fldHTML += " READONLY";
			} catch (JSONException e) {
				e.printStackTrace();
			}
			fldHTML += "/>";
			String linkHref = "";
			String linkText = "";
			String linkOnClick = "";
			try {
				linkHref = fldHTMLSpecJSON.getString("linkHref");
				linkHref = (StringUtil.isEmpty(linkHref)) ? "#" : linkHref;
				linkText = fldHTMLSpecJSON.getString("linkText");
				linkText = (StringUtil.isEmpty(linkText)) ? LC.L_Select_Study : linkText;
				linkOnClick = fldHTMLSpecJSON.getString("onClick");
				linkOnClick = (StringUtil.isEmpty(linkOnClick))? " " : linkOnClick;
			} catch (JSONException e) {
				//e.printStackTrace();
				linkHref = (StringUtil.isEmpty(linkHref))? "#" : linkHref;
				linkText = (StringUtil.isEmpty(linkText)) ? LC.L_Select_Study : linkText;
				linkOnClick = " ";
			}
			fldHTML += "&nbsp;<a href='"+ linkHref + "' "
			 		+ "onclick='" + linkOnClick + "'>"
			 		+ linkText
			 		+"</a>";
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}
	
	private JSONObject doStudyBlinding(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		String fldValue = (studyJB == null || studyJB.getStudyBlinding() == null) ? EMPTY_STRING : studyJB.getStudyBlinding();
		try {
			fldJSON.put("fldValue", fldValue);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if ("dropdown".equals(fldType)) {
			try {
				String codeType = EMPTY_STRING;
				codeType = (String) fldHTMLSpecJSON.get("codelst_type");
				if (StringUtil.isEmpty(codeType))
					return null; 
				CodeDao cdDao = new CodeDao();
				cdDao.getCodeValues(codeType);
				String fldId = (String) fldHTMLSpecJSON.get(ID);
				fldHTML += cdDao.toPullDown(fldId, StringUtil.stringToNum(fldValue));
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}

	private JSONObject doStudyRandomization(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		String fldValue = (studyJB == null || studyJB.getStudyRandom() == null) ? EMPTY_STRING : studyJB.getStudyRandom();
		try {
			fldJSON.put("fldValue", fldValue);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if ("dropdown".equals(fldType)) {
			try {
				String codeType = EMPTY_STRING;
				codeType = (String) fldHTMLSpecJSON.get("codelst_type");
				if (StringUtil.isEmpty(codeType))
					return null; 
				CodeDao cdDao = new CodeDao();
				cdDao.getCodeValues(codeType);
				String fldId = (String) fldHTMLSpecJSON.get(ID);
				fldHTML += cdDao.toPullDown(fldId, StringUtil.stringToNum(fldValue));
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}

	private JSONObject doStudySponsor(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		String fldValue = (studyJB == null || studyJB.getStudySponsorName() == null) ? EMPTY_STRING : studyJB.getStudySponsorName();
		try {
			fldJSON.put("fldValue", fldValue);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if ("lookup".equals(fldType)) {
			try {
				String codeType = EMPTY_STRING;
				codeType = (String) fldHTMLSpecJSON.get("codelst_type");
				if (StringUtil.isEmpty(codeType))
					return null; 
				CodeDao cdDao = new CodeDao();
				cdDao.getCodeValues(codeType);
				String fldId = (String) fldHTMLSpecJSON.get(ID);
				fldHTML += cdDao.toPullDown(fldId, StringUtil.stringToNum(fldValue));
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}

	private JSONObject doStudySponsorIfOther(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		String fldValue = (studyJB == null || studyJB.getStudySponsor() == null) ? EMPTY_STRING : studyJB.getStudySponsor();
		try {
			fldJSON.put("fldValue", fldValue);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if ("input".equals(fldType)) {
			fldHTML += "<input type='text' ";
			try {
				fldHTML += "id='"+ fldHTMLSpecJSON.get(ID) +"' ";
				fldHTML += "name='"+ fldHTMLSpecJSON.get(NAME) +"' ";
				fldHTML += "size='"+ fldHTMLSpecJSON.get("size") +"' ";
				fldHTML += "maxlength='"+ fldHTMLSpecJSON.get("maxlength") +"' ";
				fldHTML += "value='"+ fldValue +"' ";
			} catch (JSONException e) {
				e.printStackTrace();
			}
			fldHTML += "/>";
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}
	
	private JSONObject doStudySponsorId(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		String fldValue = (studyJB == null || studyJB.getStudySponsorIdInfo() == null) ? EMPTY_STRING : studyJB.getStudySponsorIdInfo();
		try {
			fldJSON.put("fldValue", fldValue);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if ("input".equals(fldType)) {
			fldHTML += "<input type='text' ";
			try {
				fldHTML += "id='"+ fldHTMLSpecJSON.get(ID) +"' ";
				fldHTML += "name='"+ fldHTMLSpecJSON.get(NAME) +"' ";
				fldHTML += "size='"+ fldHTMLSpecJSON.get("size") +"' ";
				fldHTML += "maxlength='"+ fldHTMLSpecJSON.get("maxlength") +"' ";
				fldHTML += "value='"+ fldValue +"' ";
			} catch (JSONException e) {
				e.printStackTrace();
			}
			fldHTML += "/>";
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}
	
	private JSONObject doStudyContact(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		String fldValue = (studyJB == null || studyJB.getStudyContact() == null) ? EMPTY_STRING : studyJB.getStudyContact();
		try {
			fldJSON.put("fldValue", fldValue);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if ("input".equals(fldType)) {
			fldHTML += "<input type='text' ";
			try {
				fldHTML += "id='"+ fldHTMLSpecJSON.get(ID) +"' ";
				fldHTML += "name='"+ fldHTMLSpecJSON.get(NAME) +"' ";
				fldHTML += "size='"+ fldHTMLSpecJSON.get("size") +"' ";
				fldHTML += "maxlength='"+ fldHTMLSpecJSON.get("maxlength") +"' ";
				fldHTML += "value='"+ fldValue +"' ";
			} catch (JSONException e) {
				e.printStackTrace();
			}
			fldHTML += "/>";
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}

	private JSONObject doStudyKeywords(FlxPageFields field, StudyJB studyJB) {
		JSONObject fldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		String fldType;
		try {
			fldType = (String) fldHTMLSpecJSON.get(FIELD_TYPE);
		} catch (JSONException e) {
			fldType = null;
			e.printStackTrace();
		}
		fldHTML = "<TD width='25%'>"+ field.getFieldLabel() +"</TD>";
		fldHTML += "<TD>";
		String fldValue = (studyJB == null || studyJB.getStudyKeywords() == null) ? EMPTY_STRING : studyJB.getStudyKeywords();
		try {
			fldJSON.put("fldValue", fldValue);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if ("input".equals(fldType)) {
			fldHTML += "<input type='text' ";
			try {
				fldHTML += "id='"+ fldHTMLSpecJSON.get(ID) +"' ";
				fldHTML += "name='"+ fldHTMLSpecJSON.get(NAME) +"' ";
				fldHTML += "size='"+ fldHTMLSpecJSON.get("size") +"' ";
				fldHTML += "maxlength='"+ fldHTMLSpecJSON.get("maxlength") +"' ";
				fldHTML += "value='"+ fldValue +"' ";
			} catch (JSONException e) {
				e.printStackTrace();
			}
			fldHTML += "/>";
		}
		fldHTML += "</TD>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}
	
	private JSONObject getFlxPageFieldHTML_StudyMD(String fldCodeSubtype, StudyIdDao sidDao) {
		if (null == sidDao) return null;

		ArrayList alternateIdKey = sidDao.getAlternateIdKey();
		if (null == alternateIdKey) return null;

		int counter = alternateIdKey.indexOf(fldCodeSubtype);
		if (counter < 0) return null;

		JSONObject fldJSON = new JSONObject();
		String fldHTML = EMPTY_STRING;
		
		ArrayList id = sidDao.getId();
		ArrayList studyIdType = sidDao.getStudyIdType();
		ArrayList studyIdTypesDesc = sidDao.getStudyIdTypesDesc();
		ArrayList alternateId = sidDao.getAlternateId();
		ArrayList recordType = sidDao.getRecordType ();
		ArrayList dispTypes = sidDao.getDispType();
		ArrayList dispData = sidDao.getDispData();

		String strStudyIdTypesDesc = (String) studyIdTypesDesc.get(counter);
		String strAlternateId = (String) alternateId.get(counter);
		int intStudyIdType = (Integer) studyIdType.get(counter) ; 
		String disptype=(String) dispTypes.get(counter);

		JSONObject moreStudyDetCodeJSON = new JSONObject();
		try {
			moreStudyDetCodeJSON.put("pk", EMPTY_STRING+intStudyIdType);
			moreStudyDetCodeJSON.put("fieldType", (StringUtil.isEmpty(disptype))? Str_input : disptype);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}


		String dispdata=(String) dispData.get(counter);
		if (disptype==null) disptype = EMPTY_STRING;
			disptype=disptype.toLowerCase();
		try {
			fldJSON.put("fldType", (StringUtil.isEmpty(disptype))? Str_input : disptype);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		if (dispdata==null) dispdata  =EMPTY_STRING;
		if (disptype.equals(Str_dropdown)) {
			if (null != dispdata) {
				dispdata = StringUtil.replace(dispdata, "alternateId", "alternateId"+intStudyIdType);
			}
		}

		if (strAlternateId == null) strAlternateId = EMPTY_STRING;
		try {
			fldJSON.put("fldValue", strAlternateId.trim());
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		String strRecordType = (String) recordType.get(counter);
		
		fldHTML = "<td width='25%' >"+strStudyIdTypesDesc+"</td>";
		fldHTML += "<td>";

		int cbcount=0;
		String ddStr = EMPTY_STRING;
		int intId = (Integer) id.get(counter) ;
		if (Str_Hidden_input.equals(disptype)) {
			fldHTML += "<input type='hidden' readonly id='alternateId"+intStudyIdType+"' name='alternateId"+intStudyIdType+"' value='"+strAlternateId.trim()+"' size='25' maxlength='100'/>"
			+ "<input type = 'hidden' name = 'recordType"+intStudyIdType+"' value = '"+ strRecordType +"' />"
			+ "<input type = 'hidden' name = 'id"+intStudyIdType+"' value = '"+ intId +"' />"
			+ "<input type = 'hidden' name = 'studyIdType' value = '"+ intStudyIdType +"' />"
			//+ "<input type = 'hidden' name = 'studyId' value = '"+ studyId +"' />"
			;
			try {
				fldJSON.put("fldHTML", fldHTML);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return fldJSON;
		}	
	
		if (Str_lookup.equals(disptype)) {
			//lookup
		    String lkpKeyworkStr = EMPTY_STRING;
		    int lkpPK = 0;
		    try{
			    JSONObject lookupJSON = new JSONObject(dispdata);
			    lkpPK = StringUtil.stringToNum(EMPTY_STRING+lookupJSON.get("lookupPK"));
			    if (lkpPK <= 0) {
			    	System.out.println("Study More Details: invalid lookup PK");
			    	return null;
			    }
	
			    JSONArray mappingArray = (JSONArray)lookupJSON.get("mapping");
				for (int indx=0; indx < mappingArray.length(); indx++) {
					JSONObject checkboxJSON = (JSONObject)mappingArray.get(indx);
			        try{
			        	String source = checkboxJSON.getString("source").trim();
			            String target = checkboxJSON.getString("target").trim();
			            if (StringUtil.isEmpty(source) || StringUtil.isEmpty(target)) continue;
	
			            target = ("alternateId".equals(target))? "alternateId"+intStudyIdType : target;
	
			            if (!StringUtil.isEmpty(lkpKeyworkStr))
							lkpKeyworkStr += "~";
			            lkpKeyworkStr += target+"|"+source;
			        }catch(Exception e) {
			        	System.out.println("More Study Details: invalid lookup mapping");
			        }
				}
				fldHTML += "<input class='readonly-input' type='text' id='alternateId"+intStudyIdType+"' name='alternateId"+intStudyIdType+"' readonly value='"+strAlternateId.trim()+"' size='25' maxlength='100'/>";
				if (lkpPK > 0) {
					String selection = EMPTY_STRING;
					try {
			    		selection = (String)lookupJSON.get("selection");
					} catch (Exception e) {}
			    	selection = (StringUtil.isEmpty(selection))? Str_selection_single : selection;
			    	String lkpKeyworkStrWithQuotes = "\"" + lkpKeyworkStr + "\"";
			    	if (Str_selection_single.equals(selection)) {
			    		fldHTML += "<A id='alternateId"+intStudyIdType+"Link' href='#' onClick='moreStudyDetFunctions.openLookup("+lkpPK+","+lkpKeyworkStrWithQuotes+")'>"+LC.L_Select+"</A>";
					}
					if (Str_selection_multi.equals(selection)) {
						fldHTML += "<A id='alternateId"+intStudyIdType+"Link' href='#' onClick='moreStudyDetFunctions.openMultiLookup("+lkpPK+","+lkpKeyworkStrWithQuotes+")'>"+LC.L_Select+"</A>";
					} 
				}
		    } catch (Exception e) {
		    	//e.printStackTrace();
		    	System.out.println("More Study Details: lookup configuration error!");
		    } 
		} else if (Str_chkbox.equals(disptype) || Str_checkbox.equals(disptype)) {
			//checkbox
			if (!StringUtil.isEmpty(dispdata)) {
				//checkbox-group
				String[] strAlternateIds = strAlternateId.split(",");
		        try{
		        	JSONObject chkConfigJSON = new JSONObject(dispdata);
		        	int chkColCnt = 1;
		        	try {
		        		chkColCnt = StringUtil.stringToNum(""+chkConfigJSON.get("chkColCnt"));
		        	}catch(Exception e1){}
		        	
		        	try {
		        		JSONArray checkboxArray = chkConfigJSON.getJSONArray("chkArray");
	
						for (int indx=0; indx < checkboxArray.length(); indx++) {
							JSONObject checkboxJSON = (JSONObject)checkboxArray.get(indx);
					        try{
								String data = checkboxJSON.getString("data");
								String display = checkboxJSON.getString("display");
								String checked = EMPTY_STRING;
								if((","+strAlternateId+",").indexOf(","+data+",") > -1) {
									checked = "checked";
								}
								if (indx % chkColCnt == 0) {
									fldHTML += "<br>";
								}
				             fldHTML += "<input type='checkbox' id='alternateId"+intStudyIdType+"Checks"+indx+"' name='alternateId"+intStudyIdType+"Checks' value='"+data.trim()+"'"
				             		+ " onClick='moreStudyDetFunctions.setValue4ChkBoxGrp(this,"+intStudyIdType+")' size = '25' maxlength = '100' "+checked+"> "+display+EMPTY_STRING;
					       	}catch(Exception e) {}
						}
		        	}catch(Exception e1){}
					
		        }catch(Exception e) {
		        	//e.printStackTrace();
			    	System.out.println("More Study Details: checkbox configuration error!");
		        }
				fldHTML += "<input type='hidden' id='alternateId"+intStudyIdType+"' name='alternateId"+intStudyIdType+"' value='"+strAlternateId.trim()+"' size = '25' maxlength = '100' />";
			} else {
				cbcount=cbcount+1;
				fldHTML += "<input type = 'hidden' id = 'alternateId"+intStudyIdType+"' name = 'alternateId"+intStudyIdType+"' value='"+strAlternateId.trim()+"' size = '25' maxlength = '100' />";
				if ((strAlternateId.trim()).equals("Y")) {
					fldHTML += "<input type='checkbox' name='alternate' value='"+strAlternateId.trim()+"' onClick='moreStudyDetFunctions.setValue(moreStudyDetFunctions.formObj,"+intStudyIdType+","+cbcount+")' checked/>";
				}else{
					fldHTML += "<input type='checkbox'  name='alternate' value='"+strAlternateId.trim()+"' onClick='moreStudyDetFunctions.setValue(moreStudyDetFunctions.formObj,"+intStudyIdType+","+cbcount+")'/>";
				}
			}
		} else if (Str_dropdown.equals(disptype)) {
			//dropdown
			if (ddStr.length()==0) ddStr=(intStudyIdType)+":"+strAlternateId;
			else ddStr=ddStr+"||"+(intStudyIdType)+":"+strAlternateId;
			fldHTML += dispdata;
			 if (fldCodeSubtype.equals("studyCond2")){
				 CodeDao cdDao = new CodeDao();
				 cdDao.getCodeValues("tarea");
					fldHTML += cdDao.toPullDown("alternateId"+intStudyIdType);
				 
			 }
			fldHTML += "<input type=\"hidden\" name=\"ddlist"+intStudyIdType+"\" value=\""+ddStr+"\">";

			fldHTML += "<script>jQuery(document).ready(function() {"
					+ "studySubmScreenFunctions.setDDFlex("+intStudyIdType+");"
					+ "});"
					+ "</script>";
		} else if (Str_splfld.equals(disptype)) {
			//Special Field
			fldHTML += dispdata;
		}else if (Str_date.equals(disptype)) {
			//Date input
			fldHTML += "<input id='alternateId"+intStudyIdType+"' name='alternateId"+intStudyIdType+"' type='text' class='datefield' size='10' readOnly  value='"+strAlternateId.trim()+"'>";
		}else if (Str_RO_input.equals(disptype)) {
			//Read-only input 
			fldHTML += "<input type='text' class='readonly-input' readonly id='alternateId"+intStudyIdType+"' name='alternateId"+intStudyIdType+"' value='"+strAlternateId.trim()+"' size='25' maxlength='100'/>";
		} else if(Str_tarea.equals(disptype)) {
			fldHTML += "<textarea class='mdTextArea' name = 'alternateId"+intStudyIdType+"'  rows='4' cols='50' >"+strAlternateId.trim()+"</textarea>";
		} else if(Str_tarea_RO.equals(disptype)) {
			fldHTML += "<textarea class='mdTextArea' readonly name = 'alternateId"+intStudyIdType+"'  rows='4' cols='50' >"+strAlternateId.trim()+"</textarea>";
		} else{
			//Basic input
			fldHTML += "<input type = 'text' id='alternateId"+intStudyIdType+"' name='alternateId"+intStudyIdType+"' value = '"+strAlternateId.trim()+"' size = '25' maxlength = '100' />";
		}
		fldHTML += "<input type = 'hidden' name = 'recordType"+intStudyIdType+"' value = '"+strRecordType +"' />";
		fldHTML += "<input type = 'hidden' name = 'id"+intStudyIdType+"' value = '"+intId+"' />";
		fldHTML += "<input type = 'hidden' name = 'studyIdType' value = '"+intStudyIdType+"' />";
		//fldHTML += "<input type = 'hidden' name = 'studyId' value = '"+studyId+"' />";

		fldHTML += "</td>";
		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}
	
	public JSONObject getFlxPageStudySectionHtml(Object...args) { 
		JSONObject fldJSON = new JSONObject();
		String fldHTML = null;
		
		String studySectionKeyword = (String)args[0];
		HttpSession tSession = (HttpSession)args[1];
		if (StringUtil.isEmpty(studySectionKeyword) || null == tSession){
			return null;
		}

		int userId = StringUtil.stringToNum((String)tSession.getAttribute("userId"));
		if (userId <= 0){
			return null;
		}		

		StudyJB studyJB = new StudyJB();
		studyJB = (StudyJB)args[2];
		
		CodeDao cdVerCat=new CodeDao();
		int studyVerCategoryId = cdVerCat.getCodeId("studyvercat", "protocol");
		
		CodeDao cdSection=new CodeDao();
		cdSection.getCodeId("section", studySectionKeyword);		
		String studySectionName = cdSection.getCodeDescription();
		
		int studyId = 0;
		int nextVer = 0; nextVer++;//IMP
		int studyVersionPK = 0;
		if (null != studyJB){
			studyId = studyJB.getId();
		
			UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
			nextVer = uiFlxPageDao.getHighestFlexPageVersion("er_study", studyId);
			nextVer++;//IMP

			StudyVerAgentRObj studyVerAgent = EJBUtil.getStudyVerAgentHome();
			studyVersionPK = studyVerAgent.findStudyVersion(studyId, nextVer, studyVerCategoryId);
		}
		
		SectionAgent studySectionAgent = EJBUtil.getSectionAgentHome();
		int studyVerSectionPK = 0;
		if (studyVersionPK > 0){
			SectionDao ssDao = studySectionAgent.getStudySection(studyVersionPK, "S", studySectionName);
			studyVerSectionPK = (null == ssDao.getIds() || (ssDao.getIds()).size() == 0)? 0 :
				(Integer) (ssDao.getIds()).get(0);
		}
		
		int sectionId = 0;
		String sectionContents = "";
		SectionBean secBean = null;
		if (studyVerSectionPK > 0){
			studySectionAgent = EJBUtil.getSectionAgentHome();
			secBean = studySectionAgent.getSectionDetails(studyVerSectionPK);
			
			sectionId = secBean.getId();
			sectionContents = secBean.getContents();
		}
		
		try {
			fldJSON.put("fldValue", sectionContents);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		fldHTML = "<td wdth='25%' colspan='2'><b>"+ StringUtil.htmlEncode(studySectionName) +"</b>"
			+ "<input type='hidden' name='sectionId' size='20' value ='"+ sectionId +"' />"
			+ "<input type='hidden' name='sectionName' size=20 MAXLENGTH = 100 value='"+ StringUtil.htmlEncode(studySectionName) +"'/>"
			+ "</td>"
			+ "</tr>";

		fldHTML += "<tr>"
			+ "<td colspan=2>"
			+ "<TextArea id='sectionContentsta' name='sectionContentsta' rows=5 cols=80 wrap='hard' style='width:800;height:190;'>"+StringUtil.htmlEncode(sectionContents) +"</TextArea>"
			+ "<input name='sectionContents' type='hidden' value='"+ StringUtil.htmlEncode(sectionContents) +"'/>"
			+ "</td>"
			+ "</tr>";

		fldHTML += "<tr>"
			+ "<td colspan=2>"
			+ "<p class = 'defComments'>"+ MC.M_SecLimit_20000Char +"</P>"
			+ "</td>"
			+ "</tr>";

		try {
			fldJSON.put("fldHTML", fldHTML);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fldJSON;
	}

	public JSONArray getUnusedMDFieldsForPage_StudyMD(String page, ArrayList<String> pageSectionMSDFieldsList) {
		if (StringUtil.isEmpty(page)) return null;

		JSONArray pagefieldJSArray = new JSONArray();
		JSONObject fieldJSON = null;
		
		StudyIdJB studyIdB = new StudyIdJB();
		StudyIdDao studyIdDao = studyIdB.getStudyIds(0, "0");

		ArrayList studyIdCodePks= studyIdDao.getStudyIdType();
		ArrayList studyIdTypesDescs = studyIdDao.getStudyIdTypesDesc();
		ArrayList studyIdKeys = studyIdDao.getAlternateIdKey();

		int seq = 0;
		for (int indx = 0; indx < studyIdCodePks.size(); indx++) {
			fieldJSON = new JSONObject();
			String studyIdCodeSubtype = (null == studyIdKeys.get(indx))? "" : (studyIdKeys.get(indx)).toString();

			if (StringUtil.isEmpty(studyIdCodeSubtype)) continue;
			if (pageSectionMSDFieldsList.indexOf(studyIdCodeSubtype) >= 0) continue;
			
        	try {
        		fieldJSON.put("seq", seq);
        		fieldJSON.put("type", "MSD");
        		fieldJSON.put("flxFieldId", studyIdCodeSubtype);
	        	fieldJSON.put("keyword", StringUtil.trueValue(studyIdCodeSubtype));
	        	fieldJSON.put("label", studyIdTypesDescs.get(indx));
				pagefieldJSArray.put(fieldJSON);
				seq++;
			} catch (JSONException e) {
				e.printStackTrace();
			}
	    }
		return pagefieldJSArray;
	}

	public JSONArray getUnusedStudySectionsForPage(int accId, ArrayList<String> pageSectionStudySectionList) {
		JSONArray pageStudySectionJSArray = new JSONArray();
		JSONObject stdSectionJSON = null;
		
		CodelstJB codelstB = new CodelstJB();
		CodeDao codeDao = codelstB.getCodelstData(accId, "section");

		ArrayList codePks= codeDao.getCId();
		ArrayList codeDescs = codeDao.getCDesc();
		ArrayList codeSubTypes = codeDao.getCSubType();

		int seq = 0;
		for (int indx = 0; indx < codePks.size(); indx++) {
			stdSectionJSON = new JSONObject();
			String codeSubType = ((String)codeSubTypes.get(indx));

			if (StringUtil.isEmpty(codeSubType)) continue;
			if (pageSectionStudySectionList.indexOf(codeSubType) >= 0) continue;
			
        	try {
        		stdSectionJSON.put("seq", seq);
        		stdSectionJSON.put("type", "studySection");
        		stdSectionJSON.put("flxFieldId", codeSubType);
        		stdSectionJSON.put("keyword", codeSubType);
        		stdSectionJSON.put("label", codeDescs.get(indx));
        		pageStudySectionJSArray.put(stdSectionJSON);
				seq++;
			} catch (JSONException e) {
				e.printStackTrace();
			}
	    }
		return pageStudySectionJSArray;
	}
}
