package com.velos.eres.widget.web;

import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.velos.eres.business.common.UserDao;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.widget.service.util.GadgetUtil;


public class GadgetPatSchedulesAction extends ActionSupport {
	private static final long serialVersionUID = 7739335183881422065L;
	private HttpServletRequest request = null;
	private HttpSession tSession = null;
	private GadgetPatSchedulesJB gadgetPatSchedulesJB = null;
	private LinkedHashMap<String, Object> jsonData = null;
	private static final String USER_ID = "userId";
	private static final String GROUP_ID = "grpId";
	private static final String STUDIES = "gdtPatSchStudies";
	private static final String ACCOUNT_ID = "accountId";
	
	public GadgetPatSchedulesAction() {
		ActionContext ac = ActionContext.getContext();
		request = (HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
		tSession = request.getSession(false);
		gadgetPatSchedulesJB = new GadgetPatSchedulesJB(getUserIdInSession());
	}
	
	public GadgetPatSchedulesJB getGadgetPatSchedulesJB(){
		return this.gadgetPatSchedulesJB;
	}

	public void setGadgetPatSchedulesJB(GadgetPatSchedulesJB gadgetPatSchedulesJB){
		this.gadgetPatSchedulesJB = gadgetPatSchedulesJB;
	}

	public String getGadgetPatSchedules() { 
		return SUCCESS; 
	}
	
	public LinkedHashMap<String, Object> getJsonData() { return jsonData; }
	
	private String getGroupIdForUser(String userId) {
		String grpId = null;
		try {
			UserDao usrDao = new UserDao();
			usrDao.getUsersDetails(""+userId);
			grpId = (String)(usrDao.getUsrDefGrps()).get(0);
		} finally {
		}
		return grpId;
	}
	
	public String getUserIdInSession() {
		String userId = null;
		try {
			userId = (String) request.getSession(false).getAttribute(USER_ID);
		} finally {
			userId = StringUtil.trueValue(userId);
		}
		return userId;
	}

	private String getStudiesInSession() {
		String studyIds = null;
		try {
			studyIds = (String) request.getSession(false).getAttribute(STUDIES);
		} finally {
			studyIds = StringUtil.trueValue(studyIds);
		}
		return studyIds;
	}
	
	public String getStudies() {
		String accId = (String)tSession.getAttribute(ACCOUNT_ID);
		if (StringUtil.isEmpty(accId)) { return null; }
		
		String userId = (String)tSession.getAttribute(USER_ID);
		if (StringUtil.isEmpty(userId)) { return null; }
		
		String exceptStudies = this.gadgetPatSchedulesJB.getSettingsStudyIds();
		
		jsonData = GadgetUtil.getStudies(userId, accId, exceptStudies, 0);
		return SUCCESS;
	}

	public String getMyStudies() {
		String accId = (String)tSession.getAttribute(ACCOUNT_ID);
		if (StringUtil.isEmpty(accId)) { return null; }
		
		String userId = (String)tSession.getAttribute(USER_ID);
		if (StringUtil.isEmpty(userId)) { return null; }
		
		String onlyStudies = this.gadgetPatSchedulesJB.getSettingsStudyIds();
		if (StringUtil.isEmpty(onlyStudies)) { return null; }
		
		jsonData = GadgetUtil.getStudies(userId, accId, onlyStudies, 1);
		return SUCCESS;
	}
	
	public String getPatSchedules() {
		String userId = (String)tSession.getAttribute(USER_ID);
		if (StringUtil.isEmpty(userId)) { return ERROR; }
		
		UserDao usrDao = new UserDao();
		usrDao.getUsersDetails(""+userId);
		
		String grpId = this.getGroupIdForUser(userId);
		if (StringUtil.isEmpty(grpId)) { return ERROR; }
		
		request.setAttribute("gdtUserId", userId);
		request.setAttribute("gdtGroupId", grpId);
		
		String visitFilter = request.getParameter(this.gadgetPatSchedulesJB.VISITFILTER_KEY);		
		this.gadgetPatSchedulesJB.setVisitFilter(visitFilter);
		request.setAttribute("gdtVisitFilter", visitFilter);
		
		String settingSortBy = this.gadgetPatSchedulesJB.getSettingsSortBy();
		String settingSortDir = this.gadgetPatSchedulesJB.getSettingsSortDir();

		String onlyStudies = this.gadgetPatSchedulesJB.getSettingsStudyIds();
		//if (StringUtil.isEmpty(onlyStudies)) { return null; }

		LinkedHashMap<String, Object> jsonGrid = new LinkedHashMap<String, Object>();
		try{
			jsonData = new LinkedHashMap<String, Object>();
			jsonData.put("colArray", gadgetPatSchedulesJB.getPatSchedulesColumns());
			jsonData.put("dataArray", gadgetPatSchedulesJB.getPatSchedules(userId, onlyStudies, 
					settingSortBy, settingSortDir));
			jsonData.put("showMoreLink", gadgetPatSchedulesJB.getShowMoreLink());
		} catch (Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
}
