package com.velos.eres.widget.service.util;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.service.util.FlxPageUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.widget.business.common.UIFlxPageDao;
/**
 * @author Sampada Mhalagi
 * Class to cache all the UI Flex Page data.
 *
 */
public class FlxPageCache {
	private FlxPageCache() {}
	private static UIFlxPageDao uiFlxPageDao = null;
	private static String studyPageRegistry = null;	
	private static JSONArray studyPageFieldRegistry = null;
	private static JSONArray studyPageSectionRegistry = null;
	private static JSONArray studyPageStudySectionRegistry = null;

	private static String studyPageSections = null;
	private static String getFlxPageJSString = null;
	private static final String FLEX_STUDY_MODULE_NAME = "STUDY";
	
	private static String getFlexStudyPageName(){
		return FlxPageUtil.getFlexStudyPageName();
	}

	public static void reloadStudyPage() {
		if (uiFlxPageDao == null) {
			uiFlxPageDao = new UIFlxPageDao();
		}
		final String PAGE_NAME = getFlexStudyPageName(); 
		JSONObject jsObjPageRec;
		try {
			jsObjPageRec = uiFlxPageDao.flexPageData(PAGE_NAME);
			if (jsObjPageRec == null) {
				synchronized(FlxPageCache.class) {
					uiFlxPageDao.insertFlexPage(PAGE_NAME);
					jsObjPageRec = uiFlxPageDao.flexPageData(PAGE_NAME);
				}
			}
			JSONObject jsObjPageConfig = uiFlxPageDao.getFlxPageCookie(jsObjPageRec);
			synchronized(FlxPageCache.class) {
				studyPageRegistry = jsObjPageConfig.toString();

				try{
					studyPageSections = jsObjPageConfig.getString("sections");
					if (StringUtil.isEmpty(studyPageSections) || "[]".equals(studyPageSections)){
						studyPageSections = "[{seq:1, title:'Study Identifiers', secType:'generic', secSpecial:true, "
								+ "fields:["
								+ FlxPageFields.getFieldSpec(FLEX_STUDY_MODULE_NAME,"STD","STUDY_STUDYNUMBER")
								+ ","
								+ FlxPageFields.getFieldSpec(FLEX_STUDY_MODULE_NAME,"STD","STUDY_STUDYTITLE")
								+"]}]";
					}
				} catch(JSONException e){
					studyPageSections = "[{seq:1, title:'Study Identifiers', secType:'generic', secSpecial:true, "
							+ "fields:["
							+ FlxPageFields.getFieldSpec(FLEX_STUDY_MODULE_NAME,"STD","STUDY_STUDYNUMBER")
							+ ","
							+ FlxPageFields.getFieldSpec(FLEX_STUDY_MODULE_NAME,"STD","STUDY_STUDYTITLE")
							+"]}]";
					e.printStackTrace();
				}
			}
			getFlxPageJSString = uiFlxPageDao.getFlxPageJS(jsObjPageRec);
			getFlxPageJSString = (StringUtil.isEmpty(getFlxPageJSString))? "" : getFlxPageJSString;
			synchronized(FlxPageCache.class) {
				String studyPageJS = getFlxPageJSString.toString();
					if(StringUtil.isEmpty(studyPageJS) || "".equals(studyPageJS)){
						studyPageJS = "var studyScreenJS = {validate: {}}; studyScreenJS.validate = function(){ return true;};";
					}
			}
		} catch (Exception e1) {
			e1.printStackTrace();
			studyPageRegistry = null;
			getFlxPageJSString = null;
			studyPageSections = "[{seq:1, title:'Study Identifiers', secType:'generic', secSpecial:true, "
					+ "fields:["
					+ FlxPageFields.getFieldSpec(FLEX_STUDY_MODULE_NAME,"STD","STUDY_STUDYNUMBER")
					+ ","
					+ FlxPageFields.getFieldSpec(FLEX_STUDY_MODULE_NAME,"STD","STUDY_STUDYTITLE")
					+"]}]";
			
		}
	}

	public static String getStudyPage() {
		if (studyPageRegistry == null) {
			reloadStudyPage();
		}
		return studyPageRegistry;
	}

	public static JSONArray getStudyPageSectionRegistryForConfig() {
		if (studyPageSectionRegistry == null) {
			studyPageSectionRegistry = FlxPageSections.getSectionsForPage(FLEX_STUDY_MODULE_NAME);
		}
		return studyPageSectionRegistry;
	}
	
	public static JSONArray getStudyPageStudySectionRegistryForConfig(int accId) {
		if (studyPageStudySectionRegistry == null) {
			reloadStudyPage();
		}
		return studyPageStudySectionRegistry;
	}

	private static JSONArray getUnusedFieldsForStudy(ArrayList<String> pageSectionFieldsList){
		JSONArray pagefieldJSArray = new JSONArray();

		JSONArray pageSTDfieldJSArray = new JSONArray();
		pageSTDfieldJSArray = FlxPageFields.getUnusedFieldsForPage_STD(FLEX_STUDY_MODULE_NAME, pageSectionFieldsList);
		
		JSONArray pageMSDfieldJSArray = new JSONArray();
		FlxPageFieldHtml flxPageFieldHtml = new FlxPageFieldHtml();
		pageMSDfieldJSArray = flxPageFieldHtml.getUnusedMDFieldsForPage_StudyMD(FLEX_STUDY_MODULE_NAME, pageSectionFieldsList);
		
		int seq = pageSTDfieldJSArray.length();
		pagefieldJSArray = pageSTDfieldJSArray;
		for (int indx = 0; indx < pageMSDfieldJSArray.length(); indx++){
			try {
				JSONObject jsObj = pageMSDfieldJSArray.getJSONObject(indx);
				if (null != jsObj){
					jsObj.putOpt("seq", seq++);
					pagefieldJSArray.put(jsObj);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return pagefieldJSArray;
	}
	
	public static JSONArray getStudyPageFieldRegistryForConfig(Object...args) {
		HttpSession session = (HttpSession) (args[0]);
		if (null == session){
			return null;
		}
		int accId = StringUtil.stringToNum((String)session.getAttribute("accountId"));
		if (accId <= 0){
			return null;
		}

		try {
			JSONArray sectionJSArray = new JSONArray(studyPageSections);
			ArrayList<String> pageSectionFieldsList = new ArrayList<String>();
			ArrayList<String> pageSectionStudySectionList = new ArrayList<String>();
			
			CodeDao cdMSD = new CodeDao();
			cdMSD.getCodeValues("studyidtype");
			ArrayList<String> cdMSD_Subtype = cdMSD.getCSubType();
			ArrayList<String> cdMSD_Descs = cdMSD.getCDesc();
			
			CodeDao cdStudySections = new CodeDao();
			cdStudySections.getCodeValues("section", accId);
			ArrayList<String> cdSS_Subtype = cdStudySections.getCSubType();
			ArrayList<String> cdSS_Descs = cdStudySections.getCDesc();

			for (int iSection = 0; iSection < sectionJSArray.length(); iSection++){
				JSONObject sectionObject = (JSONObject) sectionJSArray.get(iSection);
				JSONArray fieldArray = null;
				try{
					fieldArray = sectionObject.getJSONArray("fields");

					for (int iField = 0; iField < fieldArray.length(); iField++){
						JSONObject fieldObject = (JSONObject) fieldArray.get(iField);
						String flxFieldId = fieldObject.getString("flxFieldId");
						
						if (StringUtil.isEmpty(flxFieldId)) continue;

						String fieldType = fieldObject.getString("type");

						if ("STD".equals(fieldType)) {
							if (pageSectionFieldsList.indexOf(flxFieldId) < 0){
								pageSectionFieldsList.add(flxFieldId);
								
								fieldObject.put("keyword", flxFieldId);//Replacing keyword by flxFieldId value
								fieldObject.put("label", (FlxPageFields.getFlxPageFieldByKey(flxFieldId)).getFieldLabel());
								fieldArray.put(iField, fieldObject);
							}
						} else if ("MSD".equals(fieldType)) {
							flxFieldId = fieldObject.getString("keyword");
							fieldObject.put("flxFieldId", flxFieldId);//Replacing flxFieldId by keyword value
							if (pageSectionFieldsList.indexOf(flxFieldId) < 0){
								pageSectionFieldsList.add(flxFieldId);
								if (null != cdMSD_Subtype){
									int indx = cdMSD_Subtype.indexOf(flxFieldId);
									if (indx >= 0){
										String codeDesc = (indx > (cdMSD_Descs.size()))? "" : (String) cdMSD_Descs.get(indx);
										fieldObject.put("label", codeDesc);
										fieldArray.put(iField, fieldObject);
									}
								}
							}
						} else if ("studySection".equals(fieldType)){
							if (pageSectionStudySectionList.indexOf(flxFieldId) < 0){
								pageSectionStudySectionList.add(flxFieldId);
								if (null != cdSS_Subtype){
									int indx = cdSS_Subtype.indexOf(flxFieldId);
									if (indx >= 0){
										String codeDesc = (indx > (cdSS_Descs.size()))? "" : (String) cdSS_Descs.get(indx);
										fieldObject.put("label", codeDesc);
										fieldArray.put(iField, fieldObject);
									}
								}
							}
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
					fieldArray = new JSONArray();
				}
				sectionObject.put("fields", fieldArray);
				sectionJSArray.put(iSection, sectionObject);
			}
			synchronized(FlxPageCache.class) {
				studyPageSections = sectionJSArray.toString();
				studyPageFieldRegistry = getUnusedFieldsForStudy(pageSectionFieldsList);
				studyPageStudySectionRegistry = getUnusedStudySectionsForStudy(accId, pageSectionStudySectionList);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return studyPageFieldRegistry;
	}

	private static JSONArray getUnusedStudySectionsForStudy(int accId, ArrayList<String> pageSectionSectionList){
		JSONArray pageStudySectionJSArray = new JSONArray();
		
		FlxPageFieldHtml flxPageFieldHtml = new FlxPageFieldHtml();
		pageStudySectionJSArray = flxPageFieldHtml.getUnusedStudySectionsForPage(accId, pageSectionSectionList);
		
		return pageStudySectionJSArray;
	}

	public static JSONArray getStudyPageFieldRegistry() {
		if (studyPageFieldRegistry == null) {
			reloadStudyPage();
		}
		return studyPageFieldRegistry;
	}

	public static String getStudyPageSections() {
		if (studyPageSections == null) {
			reloadStudyPage();
		}
		return studyPageSections;
	}
	
	public static String getStudyPageFieldJS() {
		if (getFlxPageJSString == null) {
			reloadStudyPage();
		}
		return getFlxPageJSString;
	}
}

