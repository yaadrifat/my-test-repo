package com.velos.eres.widget.service.util;

import org.json.JSONArray;
import org.json.JSONObject;

import com.velos.eres.widget.business.common.UIGadgetDao;

/**
 * @author Isaac Huang
 * Class to cache all the UI Gadget data.
 *
 */
public class GadgetCache {
	private GadgetCache() {}
	private static UIGadgetDao uiGadgetDao = null;
	private static String gadgetRegistry = null;
	private static String ONE_STR = "1";
	public static void reloadGadgetData() {
		if (uiGadgetDao == null) {
			uiGadgetDao = new UIGadgetDao();
		}
		JSONArray jArray = uiGadgetDao.retrieveGadgetRegistry();
		synchronized(GadgetCache.class) {
			StringBuffer sb1 = new StringBuffer("{");
			try {
				for(int iX=0; iX<jArray.length(); iX++) {
					JSONObject jObj = (JSONObject)jArray.get(iX);
					if (!ONE_STR.equals(jObj.get(UIGadgetDao.VISIBLE_STR))) { continue; }
					String myId = (String)jObj.get(UIGadgetDao.ID_STR);
					sb1.append("'").append(myId).append("'").append(":").append(jObj.toString()).append(",");
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
			if (sb1.length() > 1) { sb1.deleteCharAt(sb1.length()-1); }
			sb1.append("}");
			gadgetRegistry = sb1.toString();
		}
	}
	public static String getGadgetRegistry() {
		if (gadgetRegistry == null) {
			reloadGadgetData();
		}
		return gadgetRegistry;
	}
}
