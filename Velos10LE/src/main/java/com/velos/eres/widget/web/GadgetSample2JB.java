package com.velos.eres.widget.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.StudyDao;
import com.velos.eres.business.common.UsrLinkDao;
import com.velos.eres.business.ulink.impl.ULinkBean;
import com.velos.eres.service.codelstAgent.CodelstAgentRObj;
import com.velos.eres.service.ulinkAgent.ULinkAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.MC;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.study.StudyJB;

public class GadgetSample2JB {
	
	public GadgetSample2JB() {}
	
	private static final String GADGET_SAMPLE2JB_DOT = "gadgetSample2JB.";
    private static final String TRUE_STR = "true";
    private static final String FALSE_STR = "false";
    private static final String ADD_MORE_KEY = "addMore";
    private static final String URI_KEY = "uri";
    private static final String DESC_KEY = "desc";
    private static final String LNK_ID_KEY = "lnkid";
    private static final String ARRAY_KEY = "array";
    private static final String SECTION_KEY = "section";
    private static final String ACCESS_KEY = "access";
    private static final String CODELST_TYPE_LNK_TYPE = "lnk_type";
    private static final String CODELST_SUBTYP_LNK_GEN = "lnk_gen";
    private static final String ACTIVE_FOR_ENRL = "activeForEnrl";
    private static final String STUDY_FOR_PAT_SEARCH = "studyForPatSearch";
    private static final String STUDY_FOR_PAT_STUDY_SEARCH = "studyForPatStudySearch";

    public static final String LNK_GEN = "lnk_gen";

	private String accountId = null;
	private String ipAdd = null;
	private String userId = null;
	
	private String studyForPatSearch = null;
	private String studyMenuForPatSearch = null;
	private String studyForPatStudySearch = null;
	private String studyMenuForPatStudySearch = null;
	public String getStudyForPatSearch() {
		return this.studyForPatSearch;
	}
	public void setStudyForPatSearch(String studyForPatSearch) {
		this.studyForPatSearch = studyForPatSearch;
	}
	public String getStudyMenuForPatSearch() { return this.studyMenuForPatSearch; }
	public String getStudyForPatStudySearch() {
		return this.studyForPatStudySearch;
	}
	public void setStudyForPatStudySearch(String studyForPatStudySearch) {
		this.studyForPatStudySearch = studyForPatStudySearch;
	}
	public String getStudyMenuForPatStudySearch() { return this.studyMenuForPatStudySearch; }

	private Integer acctLinksAccessRights = 0;
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Integer getAcctLinksAccessRights() {
		return acctLinksAccessRights;
	}
	public void setAcctLinksAccessRights(Integer acctLinksAccessRights) {
		this.acctLinksAccessRights = acctLinksAccessRights;
	}
	public Boolean getHasAcctLinksNewRights() {
		if (acctLinksAccessRights == null) { return false; }
		return this.acctLinksAccessRights == 5 || this.acctLinksAccessRights == 7; 
	}
	
	public Boolean getHasAcctLinksEditRights() {
		if (acctLinksAccessRights == null) { return false; }
		return this.acctLinksAccessRights == 6 || this.acctLinksAccessRights == 7; 
	}
	
	public void loadStudyDD(String usrId) {
		StudyJB studyJB = new StudyJB();
		StudyDao studyDaoForPatStudySearch = studyJB.getUserStudies(usrId, 
				GADGET_SAMPLE2JB_DOT+STUDY_FOR_PAT_STUDY_SEARCH, 0, ACTIVE_FOR_ENRL);
		this.studyMenuForPatStudySearch = studyDaoForPatStudySearch.getStudyDropDown();
		StudyDao studyDaoForPatSearch = studyJB.getUserStudies(usrId, 
				GADGET_SAMPLE2JB_DOT+STUDY_FOR_PAT_SEARCH, 0, ACTIVE_FOR_ENRL);
		this.studyMenuForPatSearch = studyDaoForPatSearch.getStudyDropDown();
	}

	// -- Start of My Links section
	private String myLinksLinkId;
	private String myLinksLinkUrl;
	private String myLinksLinkDisplay;
	private String myLinksLinkSection;
	private String myLinksESign;
	private String myLinksDeleteLinkId;
	private String myLinksDeleteESign;

	// FLDs are used for validation
	public static final String FLD_MY_LINKS_LINK_URL = GADGET_SAMPLE2JB_DOT+"myLinksLinkUrl";
	public static final String FLD_MY_LINKS_LINK_DISPLAY = GADGET_SAMPLE2JB_DOT+"myLinksLinkDisplay";
	public static final String FLD_MY_LINKS_ESIGN = GADGET_SAMPLE2JB_DOT+"myLinksESign";
	public static final String FLD_MY_LINKS_DELETE_ESIGN = GADGET_SAMPLE2JB_DOT+"myLinksDeleteESign";
	public static final String FLD_MY_LINKS_DELETE_LINK_ID = GADGET_SAMPLE2JB_DOT+"myLinksDeleteLinkId";

	public String getMyLinksLinkId() {
		return myLinksLinkId;
	}
	public void setMyLinksLinkId(String myLinksLinkId) {
		this.myLinksLinkId = myLinksLinkId;
	}
	public String getMyLinksLinkUrl() {
		return myLinksLinkUrl;
	}
	public void setMyLinksLinkUrl(String myLinksLinkUrl) {
		this.myLinksLinkUrl = myLinksLinkUrl;
	}
	public String getMyLinksLinkDisplay() {
		return myLinksLinkDisplay;
	}
	public void setMyLinksLinkDisplay(String myLinksLinkDisplay) {
		this.myLinksLinkDisplay = myLinksLinkDisplay;
	}
	public String getMyLinksLinkSection() {
		return myLinksLinkSection;
	}
	public void setMyLinksLinkSection(String myLinksLinkSection) {
		this.myLinksLinkSection = myLinksLinkSection;
	}
	public String getMyLinksESign() {
		return myLinksESign;
	}
	public void setMyLinksESign(String myLinksESign) {
		this.myLinksESign = myLinksESign;
	}
	public String getMyLinksDeleteLinkId() {
		return myLinksDeleteLinkId;
	}
	public void setMyLinksDeleteLinkId(String myLinksDeleteLinkId) {
		this.myLinksDeleteLinkId = myLinksDeleteLinkId;
	}
	public String getMyLinksDeleteESign() {
		return myLinksDeleteESign;
	}
	public void setMyLinksDeleteESign(String myLinksDeleteESign) {
		this.myLinksDeleteESign = myLinksDeleteESign;
	}
	// -- End of My Links section
	
	// -- Start of Account Links section
	private String acctLinksLinkId;
	private String acctLinksLinkUrl;
	private String acctLinksLinkDisplay;
	private String acctLinksLinkSection;
	private String acctLinksESign;
	private String acctLinksDeleteLinkId;
	private String acctLinksDeleteESign;
	private String acctLinksLinkType;
	
	// FLDs are used for validation
	public static final String FLD_ACCT_LINKS_LINK_URL = GADGET_SAMPLE2JB_DOT+"acctLinksLinkUrl";
	public static final String FLD_ACCT_LINKS_LINK_DISPLAY = GADGET_SAMPLE2JB_DOT+"acctLinksLinkDisplay";
	public static final String FLD_ACCT_LINKS_ESIGN = GADGET_SAMPLE2JB_DOT+"acctLinksESign";
	public static final String FLD_ACCT_LINKS_DELETE_ESIGN = GADGET_SAMPLE2JB_DOT+"acctLinksDeleteESign";
	public static final String FLD_ACCT_LINKS_DELETE_LINK_ID = GADGET_SAMPLE2JB_DOT+"acctLinksDeleteLinkId";

    public String getAcctLinksLinkId() {
		return acctLinksLinkId;
	}
	public void setAcctLinksLinkId(String acctLinksLinkId) {
		this.acctLinksLinkId = acctLinksLinkId;
	}
	public String getAcctLinksLinkUrl() {
		return acctLinksLinkUrl;
	}
	public void setAcctLinksLinkUrl(String acctLinksLinkUrl) {
		this.acctLinksLinkUrl = acctLinksLinkUrl;
	}
	public String getAcctLinksLinkDisplay() {
		return acctLinksLinkDisplay;
	}
	public void setAcctLinksLinkDisplay(String acctLinksLinkDisplay) {
		this.acctLinksLinkDisplay = acctLinksLinkDisplay;
	}
	public String getAcctLinksLinkSection() {
		return acctLinksLinkSection;
	}
	public void setAcctLinksLinkSection(String acctLinksLinkSection) {
		this.acctLinksLinkSection = acctLinksLinkSection;
	}
	public String getAcctLinksESign() {
		return acctLinksESign;
	}
	public void setAcctLinksESign(String acctLinksESign) {
		this.acctLinksESign = acctLinksESign;
	}
	public String getAcctLinksDeleteLinkId() {
		return acctLinksDeleteLinkId;
	}
	public void setAcctLinksDeleteLinkId(String acctLinksDeleteLinkId) {
		this.acctLinksDeleteLinkId = acctLinksDeleteLinkId;
	}
	public String getAcctLinksDeleteESign() {
		return acctLinksDeleteESign;
	}
	public void setAcctLinksDeleteESign(String acctLinksDeleteESign) {
		this.acctLinksDeleteESign = acctLinksDeleteESign;
	}
	// -- End of Account Links section
	
	public UsrLinkDao getULinkValuesByUserId(String usrId) {
    	UsrLinkDao usrLinkDao = null;
        try {
            ULinkAgentRObj uLinkAgentRObj = EJBUtil.getULinkAgentHome();
            usrLinkDao = uLinkAgentRObj.getULinkValuesByUserId(StringUtil.stringToNum(usrId));
        } catch (Exception e) {
            Rlog.fatal("gadget",
                    "Error in GadgetSample2JB.getULinkValuesByUserId: "+e);
        }
        return usrLinkDao;
    }
    
    public UsrLinkDao getULinkValuesByAccountId(String acctId, String linkType) {
    	UsrLinkDao usrLinkDao = null;
        try {
            ULinkAgentRObj uLinkAgentRObj = EJBUtil.getULinkAgentHome();
            usrLinkDao = uLinkAgentRObj.getULinkValuesByAccountId(StringUtil.stringToNum(acctId), linkType);
        } catch (Exception e) {
            Rlog.fatal("gadget",
                    "Error in GadgetSample2JB.getULinkValuesByAccountId: "+e);
        }
        return usrLinkDao;
    }
    
    public LinkedHashMap<String, Object> getMyLinksData(String usrId) {
    	LinkedHashMap<String, Object> myLinksData = new LinkedHashMap<String, Object>();
    	if (StringUtil.isEmpty(usrId)) { return myLinksData; }
		UsrLinkDao usrLinkDao = this.getULinkValuesByUserId(usrId);
    	myLinksData.put(ADD_MORE_KEY, usrLinkDao.getLnksDescs().size() > 10 ? TRUE_STR : FALSE_STR);
    	ArrayList<HashMap<String, String>> jArray = new ArrayList<HashMap<String, String>>();
    	for (int iX=0; iX<usrLinkDao.getLnksDescs().size(); iX++) {
			if (iX > 9) { break; }
			HashMap<String, String> aLnk = new HashMap<String, String>();
			aLnk.put(URI_KEY, (String)usrLinkDao.getLnksUris().get(iX));
			aLnk.put(DESC_KEY, (String)usrLinkDao.getLnksDescs().get(iX));
			aLnk.put(LNK_ID_KEY, String.valueOf(usrLinkDao.getLnksIds().get(iX)));
			aLnk.put(SECTION_KEY, (String)usrLinkDao.getLnksGrpNames().get(iX));
			try {
				jArray.add(aLnk);
			} catch(Exception e) {
				Rlog.fatal("ulink", "jArray.put error: e");
			}
		}
    	myLinksData.put(ARRAY_KEY, jArray);
    	return myLinksData;
    }
    
    public LinkedHashMap<String, Object> getAcctLinksData(String acctId, String linkType) {
    	LinkedHashMap<String, Object> acctLinksData = new LinkedHashMap<String, Object>();
    	if (StringUtil.isEmpty(acctId)) { return acctLinksData; }
		UsrLinkDao usrLinkDao = this.getULinkValuesByAccountId(acctId, linkType);
    	acctLinksData.put(ADD_MORE_KEY, usrLinkDao.getLnksDescs().size() > 10 ? TRUE_STR : FALSE_STR);
    	acctLinksData.put(ACCESS_KEY, this.acctLinksAccessRights);
    	ArrayList<HashMap<String, String>> jArray = new ArrayList<HashMap<String, String>>();
    	for (int iX=0; iX<usrLinkDao.getLnksDescs().size(); iX++) {
			if (iX > 9) { break; }
			HashMap<String, String> aLnk = new HashMap<String, String>();
			aLnk.put(URI_KEY, (String)usrLinkDao.getLnksUris().get(iX));
			aLnk.put(DESC_KEY, (String)usrLinkDao.getLnksDescs().get(iX));
			aLnk.put(LNK_ID_KEY, String.valueOf(usrLinkDao.getLnksIds().get(iX)));
			aLnk.put(SECTION_KEY, (String)usrLinkDao.getLnksGrpNames().get(iX));
			try {
				jArray.add(aLnk);
			} catch(Exception e) {
				Rlog.fatal("ulink", "jArray.put error: e");
			}
		}
    	acctLinksData.put(ARRAY_KEY, jArray);
    	return acctLinksData;
    }
    
    public void validateMyLinksForDelete(GadgetSample2Action action) {
    	if (action == null) { return; }
		int myLinkIdInt = -1;
		try {
			myLinkIdInt = Integer.parseInt(this.myLinksDeleteLinkId);
		} catch(Exception e) {
			Rlog.fatal("gadget", "In GadgetSample2JB.validateMyLinksForDelete: "+e);
		}
		if (StringUtil.isEmpty(this.myLinksDeleteLinkId) || myLinkIdInt < 0) {
			action.addFieldError(FLD_MY_LINKS_DELETE_LINK_ID, "Invalid Link ID");
		}
    }
    
	public void validateMyLinksForSave(GadgetSample2Action action) {
		if (action == null) { return; }
		if (StringUtil.isEmpty(this.myLinksLinkUrl)) {
			action.addFieldError(FLD_MY_LINKS_LINK_URL, MC.M_FieldRequired);
		}
		if (StringUtil.isEmpty(this.myLinksLinkDisplay)) {
			action.addFieldError(FLD_MY_LINKS_LINK_DISPLAY, MC.M_FieldRequired);
		}
	}
	
	public void validateAcctLinksForDelete(GadgetSample2Action action) {
		if (action == null) { return; }
		int acctLinkIdInt = -1;
		try {
			acctLinkIdInt = Integer.parseInt(this.acctLinksDeleteLinkId);
		} catch(Exception e) {
			Rlog.fatal("gadget", "In GadgetSample2JB.validateAcctLinksForDelete: "+e);
		}
		if (StringUtil.isEmpty(this.acctLinksDeleteLinkId) || acctLinkIdInt < 0) {
			action.addFieldError(FLD_ACCT_LINKS_DELETE_LINK_ID, "Invalid Link ID");
		}
	}

	public void validateAcctLinksForSave(GadgetSample2Action action) {
		if (action == null) { return; }
		if (StringUtil.isEmpty(this.acctLinksLinkUrl)) {
			action.addFieldError(FLD_ACCT_LINKS_LINK_URL, MC.M_FieldRequired);
		}
		if (StringUtil.isEmpty(this.acctLinksLinkDisplay)) {
			action.addFieldError(FLD_ACCT_LINKS_LINK_DISPLAY, MC.M_FieldRequired);
		}
	}
	
    public int deleteUlinkForMyLinks(Hashtable<String, String> userInfo) {
    	int output = -1;
    	this.myLinksLinkId = this.myLinksDeleteLinkId;
        try {
            ULinkAgentRObj uLinkAgentRObj = EJBUtil.getULinkAgentHome();
            ULinkBean uLinkBean = this.createULinkBeanForMyLinks();
            output = uLinkAgentRObj.deleteULink(uLinkBean, userInfo);
        } catch (Exception e) {
            Rlog.fatal("gadget", "In GadgetSample2JB.deleteUlinkForMyLinks: " + e);
            return -2;
        }
        return output;
    }
    
    public int deleteUlinkForAcctLinks(GadgetSample2Action action,
    		Hashtable<String, String> userInfo) {
		if (!this.getHasAcctLinksEditRights()) {
			action.addActionError(MC.M_InsuffAccRgt_CnctSysAdmin);
			return -1;
		}
    	int output = -1;
    	this.acctLinksLinkId = this.acctLinksDeleteLinkId;
        try {
            ULinkAgentRObj uLinkAgentRObj = EJBUtil.getULinkAgentHome();
            ULinkBean uLinkBean = this.createULinkBeanForAcctLinks();
            output = uLinkAgentRObj.deleteULink(uLinkBean, userInfo);
        } catch (Exception e) {
            Rlog.fatal("gadget", "In GadgetSample2JB.deleteUlinkForAcctLinks: " + e);
            return -2;
        }
        return output;
    }
    
    public int saveUlinkForMyLinks() {
    	int output = -1;
    	try {
    		ULinkAgentRObj uLinkAgentRObj = EJBUtil.getULinkAgentHome();
    		ULinkBean uLinkBean = this.createULinkBeanForMyLinks();
    		if (uLinkBean.getLnkId() < 1) {
    			output = uLinkAgentRObj.setULinkDetails(uLinkBean);
    		} else {
    			output = uLinkAgentRObj.updateULink(uLinkBean);
    		}
    	} catch (Exception e) {
            Rlog.fatal("gadget", "In GadgetSample2JB.saveUlinkForMyLinks: " + e);
            output = -2;
    	}
    	return output;
    }
    
    public int saveUlinkForAcctLinks(GadgetSample2Action action) {
    	// Check access rights
    	if (StringUtil.isEmpty(this.acctLinksLinkId)) {
    		if (!this.getHasAcctLinksNewRights()) {
    			action.addActionError(MC.M_InsuffAccRgt_CnctSysAdmin);
    			return -1;
    		}
    	} else {
    		if (!this.getHasAcctLinksEditRights()) {
    			action.addActionError(MC.M_InsuffAccRgt_CnctSysAdmin);
    			return -1;
    		}
    	}
    	// Get Link Type from code list
    	try {
    		CodelstAgentRObj codelstAgentRObj = EJBUtil.getCodelstAgentHome();
    		this.acctLinksLinkType = String.valueOf(
    				codelstAgentRObj.getCodeId(CODELST_TYPE_LNK_TYPE, CODELST_SUBTYP_LNK_GEN));
    	} catch (Exception e) {
            Rlog.fatal("gadget", "In GadgetSample2JB.saveUlinkForAcctLinks: " + e);
            return -1;
    	}
    	// Do the actual save
    	int output = -1;
    	try {
    		ULinkAgentRObj uLinkAgentRObj = EJBUtil.getULinkAgentHome();
    		ULinkBean uLinkBean = this.createULinkBeanForAcctLinks();
    		if (uLinkBean.getLnkId() < 1) {
    			output = uLinkAgentRObj.setULinkDetails(uLinkBean);
    		} else {
    			output = uLinkAgentRObj.updateULink(uLinkBean);
    		}
    	} catch (Exception e) {
            Rlog.fatal("gadget", "In GadgetSample2JB.saveUlinkForAcctLinks: " + e);
            output = -1;
    	}
    	return output;
    }
    
    private ULinkBean createULinkBeanForMyLinks() {
    	ULinkBean uLinkBean = new ULinkBean();
    	uLinkBean.setIpAdd(this.ipAdd);
    	uLinkBean.setLnkAccId(this.accountId);
    	uLinkBean.setLnkDesc(this.myLinksLinkDisplay);
    	uLinkBean.setLnkURI(this.myLinksLinkUrl);
    	uLinkBean.setLnkGrpName(this.myLinksLinkSection);
    	uLinkBean.setLnkUserId(this.userId);
    	if (StringUtil.stringToNum(this.myLinksLinkId) < 1) {
    		uLinkBean.setCreator(this.userId);
    	} else {
        	uLinkBean.setLnkId(StringUtil.stringToNum(this.myLinksLinkId));
    		uLinkBean.setModifiedBy(this.userId);
    	}
    	return uLinkBean;
    }

    private ULinkBean createULinkBeanForAcctLinks() {
    	ULinkBean uLinkBean = new ULinkBean();
    	uLinkBean.setIpAdd(this.ipAdd);
    	uLinkBean.setLnkAccId(this.accountId);
    	uLinkBean.setLnkDesc(this.acctLinksLinkDisplay);
    	uLinkBean.setLnkURI(this.acctLinksLinkUrl);
    	uLinkBean.setLnkGrpName(this.acctLinksLinkSection);
    	uLinkBean.setLnkType(this.acctLinksLinkType);
    	if (StringUtil.stringToNum(this.acctLinksLinkId) < 1) {
    		uLinkBean.setCreator(this.userId);
    	} else {
        	uLinkBean.setLnkId(StringUtil.stringToNum(this.acctLinksLinkId));
    		uLinkBean.setModifiedBy(this.userId);
    	}
    	return uLinkBean;
    }
}
