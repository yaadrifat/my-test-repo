package com.velos.eres.widget.web;

import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.velos.eres.business.common.UsrLinkDao;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.grpRights.GrpRightsJB;
import com.velos.eres.web.ulink.ULinkJB;
import com.velos.eres.widget.service.util.GadgetUtil;


public class GadgetFinancialAction extends ActionSupport {

	private static final long serialVersionUID = -1426225417025575644L;
	private HttpServletRequest request = null;
	private HttpSession tSession = null;
	private GadgetFinancialJB gadgetFinancialJB = null;
	private LinkedHashMap<String, Object> jsonData = null;
	private static final String USER_ID = "userId";
	private static final String STUDIES = "gdtFinStudies";
	private static final String ACCOUNT_ID = "accountId";

	
	public GadgetFinancialAction() {
		ActionContext ac = ActionContext.getContext();
		request = (HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
		tSession = request.getSession(false);
		gadgetFinancialJB = new GadgetFinancialJB(getUserIdInSession());
	}
	
	public GadgetFinancialJB getGadgetFinancialJB(){
		return this.gadgetFinancialJB;
	}

	public void setGadgetFinancialJB(GadgetFinancialJB gadgetFinancialJB){
		this.gadgetFinancialJB = gadgetFinancialJB;
	}
	
	public String getGadgetFinancial() { return SUCCESS; }
	
	public LinkedHashMap<String, Object> getJsonData() { return jsonData; }

	private String getUserIdInSession() {
		String userId = null;
		try {
			userId = (String) request.getSession(false).getAttribute(USER_ID);
		} finally {
			userId = StringUtil.trueValue(userId);
		}
		return userId;
	}
	
	private String getStudiesInSession() {
		String studyIds = null;
		try {
			studyIds = (String) request.getSession(false).getAttribute(STUDIES);
		} finally {
			studyIds = StringUtil.trueValue(studyIds);
		}
		return studyIds;
	}
	
	public String getStudies() {
		String accId = (String)tSession.getAttribute(ACCOUNT_ID);
		if (StringUtil.isEmpty(accId)) { return null; }
		
		String userId = (String)tSession.getAttribute(USER_ID);
		if (StringUtil.isEmpty(userId)) { return null; }
		
		String exceptStudies = gadgetFinancialJB.getSettingsStudyIds();
		
		jsonData = GadgetUtil.getStudies(userId, accId, exceptStudies, 0);
		return SUCCESS;
	}

	public String getMyStudies() {
		String accId = (String)tSession.getAttribute(ACCOUNT_ID);
		if (StringUtil.isEmpty(accId)) { return null; }
		
		String userId = (String)tSession.getAttribute(USER_ID);
		if (StringUtil.isEmpty(userId)) { return null; }
		
		String onlyStudies = gadgetFinancialJB.getSettingsStudyIds();
		  /******* @Gyanendra update for bug #202553/20251 *********/
		if (null != onlyStudies && StringUtil.isEmpty(onlyStudies)) { 
			
			return null; }
		
		jsonData = GadgetUtil.getStudies(userId, accId, onlyStudies, 1);
		
		return SUCCESS;
	}
	       /******* end update *********/
	public String getBilling() {
		String accId = (String)tSession.getAttribute(ACCOUNT_ID);
		if (StringUtil.isEmpty(accId)) { return null; }
		
		String userId = (String)tSession.getAttribute(USER_ID);
		if (StringUtil.isEmpty(userId)) { return null; }
		/*******  @Gyanendra update for bug #202553/20251  *********/
		String onlyStudies = gadgetFinancialJB.getSettingsStudyIds();
		if (null != onlyStudies && StringUtil.isEmpty(onlyStudies)) { return null; }
		
		jsonData = new LinkedHashMap<String, Object>();
		jsonData.put("colArray", gadgetFinancialJB.getBillingColumns());
		     /******* end update *********/
		GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getAttribute("GRights");
		int milestoneRight=0;
		milestoneRight = Integer.parseInt(grpRights.getFtrRightsByValue("MILEST"));
		
		if(milestoneRight >= 4){
			jsonData.put("dataArray", gadgetFinancialJB.getBilling(accId, userId, onlyStudies));
		}else{
			jsonData.put("dataArray", gadgetFinancialJB.getBilling(accId, "0", onlyStudies));
		}
		return SUCCESS;
	}

	public String getCollection() {
		String accId = (String)tSession.getAttribute(ACCOUNT_ID);
		if (StringUtil.isEmpty(accId)) { return null; }

		String userId = (String)tSession.getAttribute(USER_ID);
		if (StringUtil.isEmpty(userId)) { return null; }

		String onlyStudies = gadgetFinancialJB.getSettingsStudyIds();
		if (StringUtil.isEmpty(onlyStudies)) { return null; }

		jsonData = new LinkedHashMap<String, Object>();
		jsonData.put("colArray", gadgetFinancialJB.getCollectionColumns());
		GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getAttribute("GRights");
		int milestoneRight=0;
		milestoneRight = Integer.parseInt(grpRights.getFtrRightsByValue("MILEST"));
		
		if(milestoneRight >= 4){
			jsonData.put("dataArray", gadgetFinancialJB.getCollection(accId, userId, onlyStudies));
		}else{
			jsonData.put("dataArray", gadgetFinancialJB.getCollection(accId, "0", onlyStudies));
		}
		return SUCCESS;
	}

	public String getPayments() {
		String accId = (String)tSession.getAttribute(ACCOUNT_ID);
		if (StringUtil.isEmpty(accId)) { return null; }

		String userId = (String)tSession.getAttribute(USER_ID);
		if (StringUtil.isEmpty(userId)) { return null; }

		String onlyStudies = gadgetFinancialJB.getSettingsStudyIds();
		if (StringUtil.isEmpty(onlyStudies)) { return null; }
		
		jsonData = new LinkedHashMap<String, Object>();
		jsonData.put("colArray", gadgetFinancialJB.getPaymentsColumns());
		GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getAttribute("GRights");
		int milestoneRight=0;
		milestoneRight = Integer.parseInt(grpRights.getFtrRightsByValue("MILEST"));
		
		if(milestoneRight >= 4){
			jsonData.put("dataArray", gadgetFinancialJB.getPayments(accId, userId, onlyStudies));
		}else{
			jsonData.put("dataArray", gadgetFinancialJB.getPayments(accId, "0", onlyStudies));
			
		}
		return SUCCESS;
	}
}
