package com.velos.eres.widget.service.util;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.velos.eres.business.common.StudyIdDao;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.study.StudyJB;
import com.velos.eres.web.studyId.StudyIdJB;

import javax.servlet.http.HttpSession;

public class FlxPageJB {
	
	private JSONArray jsSectionArray = null;
	private String studyId = null;
	private String defUserGroup = null;
	private StudyJB studyJB = null;
	private HttpSession tSession = null;
	private StudyIdDao sidDao = null;
	private HashMap<Integer, Integer> dataCountHash = null;
	private HashMap<Integer, Integer> secFldCountHash = null;
	private FlxPageFieldHtml flxPageFieldHtml = null;
	
	private static String EMPTY_STRING = "";
	private static String SEC_TYPE = "secType";
	private static String SEC_MODULE = "secModule";
	private static String GENERIC_STR = "generic";
	private static String PREDEF_STR = "predef";
	private static String FIELDS_STR = "fields";
	private static String LABEL_ONLY = "labelOnly";
	private static String TYPE_STR = "type";
	private static String FLX_FIELD_ID = "flxFieldId";
	private static String STUDY_SECTION = "studySection";
	private static String STD_STR = "STD";
	private static String MSD_STR = "MSD";
	private static String FLD_HTML = "fldHTML";
	private static String FLD_VALUE = "fldValue";
	private static String FLD_TYPE = "fldType";
	private static String SPL_FLD = "splfld";
	private static String STUDY_STR = "STUDY";
	private static String INVALID_SECTION_MSG = "<P class=\"defComments\">Invalid Section Number</P>";
	private static String TITLE_STR = "title";
	
	public FlxPageJB() {
		String jPageAvailSections = FlxPageCache.getStudyPageSections();
		try {
			jsSectionArray = (StringUtil.isEmpty(jPageAvailSections)? new JSONArray() :  new JSONArray(jPageAvailSections));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		flxPageFieldHtml = new FlxPageFieldHtml(jsSectionArray);
		dataCountHash = new HashMap<Integer, Integer>();
		secFldCountHash = new HashMap<Integer, Integer>();
	}
	
	public String getStudyId() {
		return studyId;
	}

	public void setStudyId(String studyId) {
		this.studyId = studyId;
	}

	public String getDefUserGroup() {
		return defUserGroup;
	}

	public void setDefUserGroup(String defUserGroup) {
		this.defUserGroup = defUserGroup;
	}
	
	public StudyJB getStudyJB() {
		return studyJB;
	}

	public void setStudyJB(StudyJB studyJB) {
		this.studyJB = studyJB;
	}

	public HttpSession getHttpSession() {
		return tSession;
	}

	public void setHttpSession(HttpSession tSession) {
		this.tSession = tSession;
	}

	public String generateFlxHtml() {
		StringBuffer sb1 = new StringBuffer();
		StudyIdJB altId = new StudyIdJB();
		sidDao = altId.getStudyIds(StringUtil.stringToNum(studyId), defUserGroup);

		for (int iX = 0; iX < jsSectionArray.length(); iX++) {
			int sectionNo = iX+1;
			int secFldCount = 0;
			int lablesOnlyCount=0;
			
			JSONObject jsSectionObject = null;
			try {
				jsSectionObject = jsSectionArray.getJSONObject(iX);
			} catch (JSONException e) {
				e.printStackTrace();
				jsSectionObject = null;
			}
			if (jsSectionObject == null) { continue; }
			String secType = null;
			try {
				secType = jsSectionObject.getString(SEC_TYPE);
			} catch (JSONException e) {
				secType = null;
			}
			if (StringUtil.isEmpty(secType)) { secType = GENERIC_STR; }
			if (GENERIC_STR.equals(secType)) {
				JSONArray sectionFldArray = null;
				try {
					sectionFldArray = jsSectionObject.getJSONArray(FIELDS_STR);
				} catch (JSONException e) {
					e.printStackTrace();
					sectionFldArray = null;
				}
				if (sectionFldArray == null) {
					sectionFldArray = new JSONArray();
				}
				secFldCount = sectionFldArray.length();
				for (int indx1 = 0; indx1 < sectionFldArray.length(); indx1++){
					JSONObject sectionFldArrayObject = null;
					try {
						sectionFldArrayObject = sectionFldArray.getJSONObject(indx1);
					} catch (JSONException e) {
						e.printStackTrace();
						sectionFldArrayObject = null;
					}
					int labelonly = 0;
					try {
						labelonly = sectionFldArrayObject.getInt(LABEL_ONLY);
					} catch (JSONException e) {
						labelonly = 0;
					}
					if(labelonly == 1) {
						lablesOnlyCount++;
					}
				}
				secFldCount -= lablesOnlyCount;
			} else {
				secFldCount = 0;
			}
			
			secFldCountHash.put(sectionNo, secFldCount);
			
			boolean isSpecialSection = false;
			if (PREDEF_STR.equals(secType)) {
				String secModule = null;
				try {
					secModule = jsSectionObject.getString(SEC_MODULE);
				} catch (JSONException e) {
					e.printStackTrace();
					secModule = EMPTY_STRING;
				}
				if (!StringUtil.isEmpty(secModule)) {
					if (secModule.equals((FlxPageSections.STUDY_STUDYTEAM).name())) {
						// Add study team here
						sb1.append("<div id=\"sectionAccordion").append(sectionNo).append("\" onClick=\"sectionHeaderForStudyTeam(")
						.append(sectionNo).append(");\">");
						isSpecialSection = true;
					} else if (secModule.equals((FlxPageSections.STUDY_STUDYINDIDE).name())) {
						// Add study IND/IDE here
						sb1.append("<div id=\"sectionAccordion").append(sectionNo).append("\" onClick=\"sectionHeaderForIndIde(")
						.append(sectionNo).append(");\">");
						isSpecialSection = true;
					}				
				}
			}
			if (!isSpecialSection) {
				sb1.append("<div id=\"sectionAccordion").append(sectionNo).append("\" onClick=\"sectionHeaderClick(")
					.append(sectionNo).append(");\">");
			}
			sb1.append("<div id=\"sectionHead").append(sectionNo).append("\" class=\"group\">");
			sb1.append("<h3 id=\"sectionHeader").append(sectionNo).append("\">");
			sb1.append("<table width=\"100%\">");
			sb1.append("<tr>");
			sb1.append("<td width=\"70%\">");
			sb1.append("<input type=\"hidden\" id=\"sectionSeq\" name=\"sectionSeq\" value=\"").append(sectionNo).append("\"/>");
			String title = null;
			try {
				title = jsSectionObject.getString("title");
			} catch (JSONException e) {
				e.printStackTrace();
				title = null;
			}
			sb1.append("<a id=\"sectionAnchor").append(sectionNo).append("\" href=\"javascript:void(0);\" >").append(StringUtil.trueValue(title))
				.append("&nbsp;&nbsp;");
			sb1.append("<span id=\"sectionTitleSpan").append(sectionNo).append("\" style=\"display:none\">");
			sb1.append(" <input type=\"text\" id=\"sectionTitle\" name=\"sectionTitle\" value=\"").append(title).append("\"/></span>&nbsp;");
			sb1.append("<input type=\"hidden\" id=\"secFldWDataCount").append(sectionNo).append(" value=\"\"/>");
			sb1.append("<input type=\"hidden\" id=\"secFldCount").append(sectionNo).append(" value=").append(secFldCount).append("\"/>");
			sb1.append("</a>");
			sb1.append("</td>");
			sb1.append("<td id=\"secFldWDataCountTD").append(sectionNo).append("\" align=\"right\" width=\"10%\"></td>");
			sb1.append("<td align=\"right\" width=\"10%\">");
			sb1.append("<div id=\"progressBar").append(sectionNo).append("\"></div>");
			sb1.append("</td>");
			sb1.append("<td id=\"secFldCountTD").append(sectionNo).append(" align=\"left\" width=\"5%\">");
			sb1.append(secFldCount == 0 ? EMPTY_STRING : secFldCount).append("</td>");
			sb1.append("</tr>");
			sb1.append("</table>");
			sb1.append("</h3>");
			sb1.append("<div id=\"sectionBody").append(sectionNo).append("\">");
			sb1.append(this.generateFlxSection(sectionNo));
			sb1.append("</div>");
			sb1.append("</div>");
			sb1.append("</div>");
		}
		sb1.append("<script>");
		for (int iX = 0; iX < jsSectionArray.length(); iX++) {
			int sectionNo = iX+1;
			Integer fldWDataCount = 0;
			Integer secFldCount2 = 0;
			try {
				fldWDataCount = dataCountHash.get(sectionNo);
				if (fldWDataCount == null) { fldWDataCount = 0;}
				sb1.append("fldWDataCountArray['").append(sectionNo).append("']=").append(fldWDataCount).append(";");
			} catch(Exception e) {}
			try {
				secFldCount2 = secFldCountHash.get(sectionNo);
				if (secFldCount2 == null) { secFldCount2 = 0; }
				sb1.append("secFldCountArray['").append(sectionNo).append("']=").append(secFldCount2).append(";");
			} catch(Exception e) {}
		}
		sb1.append("</script>");
		return sb1.toString();
	}

	private String generateFlxSection(int sectionNo) {
		if (sectionNo < 1) {
			return INVALID_SECTION_MSG;
		}
		StringBuffer sb1 = new StringBuffer();
		JSONObject jsSectionObject = null;
		try {
			jsSectionObject = jsSectionArray.getJSONObject(sectionNo-1);
		} catch (JSONException e) {
			e.printStackTrace();
			jsSectionObject = null;
		}
		if (jsSectionObject == null) {
			return sb1.toString();
		}
		
		String secType = null;
		try {
			secType = jsSectionObject.getString(SEC_TYPE);
		} catch (JSONException e){
			secType = GENERIC_STR;
		}
		secType = (StringUtil.isEmpty(secType))? GENERIC_STR : secType;
		String title = EMPTY_STRING;
		try {
			title = jsSectionObject.getString(TITLE_STR);
		} catch (JSONException e){
			title = EMPTY_STRING;
		}

		
		if (PREDEF_STR.equals(secType)) {
			String secModule = null;
			try {
				secModule = jsSectionObject.getString(SEC_MODULE);
			} catch (JSONException e) {
				e.printStackTrace();
				secModule = EMPTY_STRING;
			}
			if (!StringUtil.isEmpty(secModule)) {
				if (secModule.equals((FlxPageSections.STUDY_STUDYTEAM).name())) {
					// Add study team here
				} else if (secModule.equals((FlxPageSections.STUDY_STUDYINDIDE).name())) {
					// Add study IND/IDE here
				}				
			}
		} else if (GENERIC_STR.equals(secType)) {
			JSONArray jsSectFldArray = null;
			try {
				jsSectFldArray = jsSectionObject.getJSONArray(FIELDS_STR);
			} catch(JSONException e) {}
			jsSectFldArray = (null == jsSectFldArray)? new JSONArray() : jsSectFldArray;
			sb1.append("<div id=\"tableContainer").append(sectionNo).append("\">");
			sb1.append("<TABLE class=\"basetbl\" border=\"0\" width=\"100%\">");
			Integer labelOnlyCount = 0;

			for(int i=0; i < jsSectFldArray.length(); i++) {
				JSONObject jsSectFldObject = null;
				String fieldType = null;
				String flxFieldId = null;
				try {
					jsSectFldObject = jsSectFldArray.getJSONObject(i);
					fieldType = jsSectFldObject.getString(TYPE_STR);
					flxFieldId = (jsSectFldObject.get(FLX_FIELD_ID)).toString();
				} catch (JSONException e) {}
				if (StringUtil.isEmpty(flxFieldId) && !STUDY_SECTION.equals(fieldType)) {
					continue;
				}
				if(title.equals(LC.L_MedInvestigDevices)) {				
					String labelonly = null;
					try {
						labelonly = jsSectFldObject.getString(LABEL_ONLY);
					} catch(Exception e) {
						labelonly = null;
					}
					if( labelonly != null ) {
						if(Integer.parseInt(labelonly) == 1) {
							labelOnlyCount++;
						}
					}
				}
				sb1.append(this.generateTableInFlxSection(sectionNo, fieldType, flxFieldId));
			}
			Integer fldWDataCount = 0;
			try {
				fldWDataCount = dataCountHash.get(sectionNo);
			} catch(Exception e) {}
			if (fldWDataCount == null) { fldWDataCount = 0; }
			if (fldWDataCount >= labelOnlyCount) {
				fldWDataCount -= labelOnlyCount;
			}
			dataCountHash.put(sectionNo, fldWDataCount);
			sb1.append("</TABLE>");
			sb1.append("<div id=\"hiddenData").append(sectionNo).append("\" style=\"display:none\"></div>");
		}
		return sb1.toString();
	}
	
	private String generateTableInFlxSection(int sectionNo, String fieldType, String flxFieldId) {
		Integer fldWDataCount = 0;
		try {
			fldWDataCount = dataCountHash.get(sectionNo);
		} catch(Exception e) {}
		if (fldWDataCount == null) { fldWDataCount = 0; }
		StringBuffer sb1 = new StringBuffer();
		sb1.append("<TR>");
		String fldHTML = EMPTY_STRING, fldValue = EMPTY_STRING, fldType = EMPTY_STRING;
		if (STD_STR.equals(fieldType)){
			FlxPageFields enumFlxPageField = FlxPageFields.getFlxPageFieldByKey(flxFieldId);
			JSONObject fldHTMLJON = flxPageFieldHtml.getPageFieldHtmlStd(enumFlxPageField, studyJB, tSession);

			if (null != fldHTMLJON) {
				try {
					fldHTML = fldHTMLJON.getString(FLD_HTML);
					fldValue = fldHTMLJON.getString(FLD_VALUE);
				} catch(JSONException e) {}
			}
			if (!StringUtil.isEmpty(fldValue)) fldWDataCount++;
		} else if (MSD_STR.equals(fieldType)) {
			JSONObject fldHTMLJON = flxPageFieldHtml.getFlxPageFieldHtmlMd(STUDY_STR, flxFieldId, sidDao);
			if (null != fldHTMLJON){
				try {
					fldHTML = fldHTMLJON.getString(FLD_HTML);
					fldValue = fldHTMLJON.getString(FLD_VALUE);
					fldType = fldHTMLJON.getString(FLD_TYPE);
				} catch(JSONException e) {}
			}
			if (!StringUtil.isEmpty(fldValue) || SPL_FLD.equals(fldType)) fldWDataCount++;					
		} else if (STUDY_SECTION.equals(fieldType)) {
			JSONObject fldHTMLJON = flxPageFieldHtml.getFlxPageStudySectionHtml(flxFieldId, tSession, studyJB);
			if (null != fldHTMLJON){
				try {
					fldHTML = fldHTMLJON.getString(FLD_HTML);
					fldValue = fldHTMLJON.getString(FLD_VALUE);
				} catch(JSONException e) {}
			}
			if (!StringUtil.isEmpty(fldValue)) fldWDataCount++;
		}
		dataCountHash.put(sectionNo, fldWDataCount);
		sb1.append(fldHTML);
		sb1.append("</TR>");
		return sb1.toString();
	}
}
