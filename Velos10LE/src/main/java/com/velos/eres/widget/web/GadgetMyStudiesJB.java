package com.velos.eres.widget.web;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.velos.eres.business.common.StudyDao;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.ctrp.web.RadioOption;
import com.velos.eres.service.studyAgent.StudyAgent;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.MC;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.widget.business.common.UIGadgetDao;

public class GadgetMyStudiesJB {
	
	public GadgetMyStudiesJB() {}
	
	private static final String GADGET_MYSTUDIES = "gadgetMyStudies";

	//Column Keywords
	private static final String STUDYDISPLAYOPTION = "displayOpt";
    private static final String STUDYIDS = "studyIds";
    private static final String STUDYID_KEY = "fk_study";
    private static final String QUICKACCESS_KEY = "quickAccess";
    private static final String STUDYNUMBER_KEY = "studyNumber";
    private static final String STUDYTITLE_KEY = "studyTitle";
    private static final String STUDYSTATUS_KEY = "studyStatus";
    private static final String STUDYICON_KEY = "studyIcon";
    private static final String PATIENTICON_KEY = "patientIcon";
    private static final String BUDGETICON_KEY = "budgetIcon";
    private static final String CALENDARICON_KEY = "calendarIcon";
	
    private int settingsDisplayOption = -1;
    private String settingsStudyIds = null;
	private int settingsStudyCount = 0;
	
	private List<RadioOption> displayOptionList = null;
	
	private LinkedHashMap<String, Object> settingsHash = null;
    JSONArray myStudiesColumns  = null;
    JSONArray myStudiesData  = null;
	
	private String accountId = null;
	private String userId = null;
	
	private void loadGadgetSettings(String userId){
		this.userId = userId;

		UserAgentRObj usrAgent = EJBUtil.getUserAgentHome();
		UserBean uBean = usrAgent.getUserDetails(StringUtil.stringToNum(this.userId));
		this.accountId = uBean.getUserAccountId();

		displayOptionList = new ArrayList<RadioOption>();
		displayOptionList.add(new RadioOption(MC.M_Disp_RecentMod_5Studies + "<br>", 0));
		displayOptionList.add(new RadioOption(LC.L_Select_UpTo_5Studies, 1));
	    
    	UIGadgetDao uiGadgetDao = new UIGadgetDao();
    	settingsHash = uiGadgetDao.getGadgetInstanceSettingsHash(userId, GADGET_MYSTUDIES);
    	
    	if (null != settingsHash){
    		String studyDisplayOption = (String)settingsHash.get(STUDYDISPLAYOPTION);
    		
    		if (StringUtil.isEmpty(studyDisplayOption)){
    			settingsDisplayOption = -1;
    		} else {
    			settingsDisplayOption = StringUtil.stringToNum((String)settingsHash.get(STUDYDISPLAYOPTION));
    		}
    		if (settingsDisplayOption == 0){
    			settingsStudyIds = null;
        		settingsStudyCount = 0;    			
    		} 
    		if (settingsDisplayOption == 1){
		    	settingsStudyIds = (String)settingsHash.get(STUDYIDS);
		    	if (!StringUtil.isEmpty(settingsStudyIds)){
		    		settingsStudyIds=settingsStudyIds.substring(settingsStudyIds.indexOf("[")+1, settingsStudyIds.indexOf("]"));
	
		    		if (!StringUtil.isEmpty(settingsStudyIds)){
			    		StudyAgent studyAgent =  EJBUtil.getStudyAgentHome();
			        	StudyDao studyDao1 = studyAgent.getStudyAutoRows(this.userId, this.accountId, 
			        			this.settingsStudyIds, 1);
			        	ArrayList arrSettingStudyIds = studyDao1.getStudyAutoIds();
	
			        	if (null != arrSettingStudyIds){
			        		settingsStudyIds = arrSettingStudyIds.toString();
			        		settingsStudyIds=settingsStudyIds.substring(settingsStudyIds.indexOf("[")+1, settingsStudyIds.indexOf("]"));
	
				        	settingsStudyCount = arrSettingStudyIds.size();
			        	} else {
			        		settingsStudyIds = null;
			        		settingsStudyCount = 0;
			        	}
		    		} else {
		    			settingsStudyIds = null;
		        		settingsStudyCount = 0;
		    		}
		    	} else{
		    		settingsStudyIds = null;
		        	settingsStudyCount = 0;
		    	}
    		}
    	} else {
    		settingsDisplayOption = -1;
    		settingsStudyIds = null;
        	settingsStudyCount = 0;
    	}
	}

	public GadgetMyStudiesJB(String userId) {
		this.loadGadgetSettings(userId);
	}
	
	public void setSettingsHash(LinkedHashMap<String, Object>  settingsHash) {
		this.settingsHash = settingsHash; 
	}

	public LinkedHashMap<String, Object> getSettingsHash() {
		return settingsHash;
	}

	public void setSettingDisplayOption(int displayOption) {
		this.settingsDisplayOption = displayOption;
	}

	public int getsettingsDisplayOption() {
		return settingsDisplayOption;
	}
	
	public List<RadioOption> getDisplayOptionList() {
		return displayOptionList;
	}

	public void setDisplayOptionList(List<RadioOption> displayOptionList) {
		this.displayOptionList = displayOptionList;
	}

	public void setSettingsStudyIds(String settingsStudyIds) {
		this.settingsStudyIds = settingsStudyIds;
	}

	public String getSettingsStudyIds() {
		return settingsStudyIds;
	}
	
	public int getSettingsStudyCount() {
		return settingsStudyCount;
	}
	
	public void setSettingsStudyCount(int settingsStudyCount) {
		this.settingsStudyCount = settingsStudyCount;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getMyStudiesColumns(){
		myStudiesColumns = new JSONArray();

		try{
			JSONObject aColumn = new JSONObject();	
						
			aColumn.put("key", STUDYID_KEY);
			aColumn.put("label","Study ID");
			aColumn.put("hidden","true");
			this.myStudiesColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", QUICKACCESS_KEY);
			aColumn.put("label",LC.L_QuickAccess);
			aColumn.put("type","image");
			this.myStudiesColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", STUDYNUMBER_KEY);
			aColumn.put("label",LC.L_Study_Number);
			this.myStudiesColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", STUDYTITLE_KEY);
			aColumn.put("label",LC.L_Study_Title);
			aColumn.put("type","image");
			this.myStudiesColumns.put(aColumn);

			aColumn = new JSONObject();
			aColumn.put("key", STUDYSTATUS_KEY);
			aColumn.put("label",LC.L_Study_Status);
			this.myStudiesColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", STUDYICON_KEY);
			aColumn.put("label",STUDYICON_KEY);
			aColumn.put("hidden","true");
			this.myStudiesColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", PATIENTICON_KEY);
			aColumn.put("label",PATIENTICON_KEY);
			aColumn.put("hidden","true");
			this.myStudiesColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", BUDGETICON_KEY);
			aColumn.put("label",BUDGETICON_KEY);
			aColumn.put("hidden","true");
			this.myStudiesColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", CALENDARICON_KEY);
			aColumn.put("label",CALENDARICON_KEY);
			aColumn.put("hidden","true");
			this.myStudiesColumns.put(aColumn);
		} catch (Exception e){
			Rlog.fatal("myStudiesColumns", "myStudiesColumns.put error: " + e);
		}
		
		return myStudiesColumns.toString();
	}

    public String getMyStudiesData(String accId, String userId){
    	myStudiesData = new JSONArray();
    	if (StringUtil.isEmpty(accId)) { return null; }
    	if (StringUtil.isEmpty(userId)) { return null; }
    	if (settingsDisplayOption < 0) { return null; }
		
		String studyIds = getSettingsStudyIds();
		if (settingsDisplayOption == 1){
			if (StringUtil.isEmpty(studyIds)) { return null; }
		}
		
		StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
		myStudiesData = studyAgent.getStudyValuesForGadget(userId, studyIds);
    	
    	return myStudiesData.toString();
	}
   
}
