package com.velos.eres.widget.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.OraclePreparedStatement;

import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.Rlog;

public class UIFlxPageDao extends CommonDAO implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private static final String getFlexPageCookie = "select page_def, page_js from UI_FLX_PAGE where PAGE_ID=?";
	//private static final String getFlexPageJS = "select page_js from UI_FLX_PAGE where PAGE_ID=?";
	private static final String insertFlexPageSql = "insert into UI_FLX_PAGE(PAGE_ID, PAGE_DEF, PAGE_JS) values (?,?,?) ";
	private static final String upateFlexPageSql = "update UI_FLX_PAGE set PAGE_DEF = ? where PAGE_ID = ?";
	private static final String insertFlexPageVerSql = "insert into UI_FLX_PAGEVER "+
			" (PAGE_MOD_TABLE, PAGE_MOD_PK, PAGE_VER, PAGE_ID, PAGE_DEF, PAGE_JS, PAGE_JSON, PAGE_HTML, PAGE_MINOR_VER, FK_SUBMISSION) values "+
			" (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
	private static final String getHighestFlexPageVersionSql = "select nvl(max(PAGE_VER), 0) from UI_FLX_PAGEVER where "+
			"PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? ";
	private static final String getHighestMinorFlexPageVersionSql = "select nvl(max(PAGE_MINOR_VER), 0) from UI_FLX_PAGEVER where "+
			"PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? and page_ver= ?";
	private static final String getFlexPageHtmlOfHighestVersionSql = 
			"select PAGE_HTML, PAGE_VER, PAGE_MINOR_VER from UI_FLX_PAGEVER where "+
			" PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? "+
			" and PAGE_VER = (select nvl(max(PAGE_VER), 0) from UI_FLX_PAGEVER where "+
			" PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ?) "+
			" and PAGE_MINOR_VER = (select nvl(max(PAGE_MINOR_VER), 0) from UI_FLX_PAGEVER where "+
			" PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? and PAGE_VER = ("+
			" select max(PAGE_VER) from UI_FLX_PAGEVER where PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? "+
			" )) ";
	private static final String getFlexPageHtmlOfHighestApprovedVersionSql = 
			"select PAGE_HTML from UI_FLX_PAGEVER where "+
			" PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? "+
			" and PAGE_MINOR_VER = 0";
	private static final String getFlexPageVerOfHighestApprovedVersionSql = 
			"select max(PAGE_VER) from UI_FLX_PAGEVER where "+
			" PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? "+
			" and PAGE_MINOR_VER = 0";
	private static final String getHighestFlexPageFullVersionSql = 
			"select PAGE_VER, PAGE_MINOR_VER, FK_SUBMISSION from UI_FLX_PAGEVER where PAGE_MOD_TABLE = ? "+ 
			" and PAGE_MOD_PK = ? order by PAGE_VER desc, PAGE_MINOR_VER desc nulls last  ";
	
	private static final String getFlexPageLatestSubmissionStatusSql = 
			"SELECT SUBMISSION_STATUS, PK_SUBMISSION_STATUS FROM ER_SUBMISSION_STATUS WHERE PK_SUBMISSION_STATUS = "+
            " (SELECT MAX (PK_SUBMISSION_STATUS) FROM ER_SUBMISSION_STATUS "+
            " WHERE SUBMISSION_STATUS IS NOT NULL AND FK_SUBMISSION = "+
            " (SELECT PK_SUBMISSION from (SELECT PK_SUBMISSION FROM ER_SUBMISSION WHERE FK_STUDY = ? ORDER BY PK_SUBMISSION DESC) where ROWNUM <= 1)) ";
	private static final String getDetailedFlexPageSql =
			" select * from ui_flx_pagever where page_mod_pk=? and page_ver= ? and page_minor_ver=? ";	
	
	private static final String getFlexPageVersionDiffSql = "SELECT PAGE_JSON FROM ui_flx_pagever WHERE PAGE_MOD_TABLE = ? and page_mod_pk =? and page_ver = ?";
	
	private static final String chkPageVersionSubmited = "select count (*) from  UI_FLX_PAGEVER where"
			+ " PAGE_VER = (select nvl( max(PAGE_VER), 0) PAGE_VER from UI_FLX_PAGEVER where PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ?) "
			+ " and (page_minor_ver = (select nvl( max(page_minor_ver), 0) page_minor_ver from UI_FLX_PAGEVER where PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ?) OR page_minor_ver IS NULL)"
			+ " and PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? and page_verdiff is not null";
	
	private static final String updateCurrentPageVersionChangesSql = "update UI_FLX_PAGEVER set page_verdiff = ?, fk_submission = ? where"
			+ " PAGE_VER = (select nvl( max(PAGE_VER), 0) PAGE_VER from UI_FLX_PAGEVER where PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ?) "
			+ " and (page_minor_ver = (select nvl( max(page_minor_ver), 0) page_minor_ver from UI_FLX_PAGEVER where PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ?) OR page_minor_ver IS NULL)"
			+ " and PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ?";
	
	public static final String getAllVersionsForProtocolSql = "select page_ver,  page_minor_ver, F_CODELST_DESC(er.submission_type) submission_type_desc from ui_flx_pagever ui, er_submission er"
			+ "  where PAGE_MOD_TABLE = ? and page_mod_pk = ? and er.pk_submission = ui.fk_submission order by page_ver desc, page_minor_ver desc";
	
	private static final String getFlexPageHtmlOfVersionSql = 
			"select PAGE_HTML, PAGE_VER from UI_FLX_PAGEVER where "+
			" PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? "+
			" and PAGE_VER = ? and PAGE_MINOR_VER = ?";
	
	private static final String getFlexPageVersionDiffDetailsSql = 
			"select page_verdiff from UI_FLX_PAGEVER where "+
			" PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? "+
			" and PAGE_VER = ? and PAGE_MINOR_VER = ?";
	
	public static final String MOD_TABLE_STR = "modTable", MOD_PK_STR = "modPK", PAGE_VER_STR = "pageVer", PAGE_ID_STR = "pageId";
	public static final String PAGE_DEF_STR = "pageDef", PAGE_JS_STR = "pageJs", PAGE_JSON_STR = "pageJson", PAGE_HTML_STR = "pageHtml";
	public static final String FLX_PAGE_COOKIE = "FLX_PAGE_COOKIE";
	public static final String PAGE_DEF = "page_def";
	public static final String PAGE_JS = "page_js";
	public static final String EMPTY_STRING = "";
	public static final String PAGE_MINOR_VER = "page_minor_ver";
	public static final String FK_SUBMISSION = "fk_submission";

	public UIFlxPageDao() {
	}
	
	private Integer submissionStatusPk = 0;
	
	public Integer getSubmissionStatusPk() {
		return submissionStatusPk;
	}

	public void insertFlexPage(String pageId) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(insertFlexPageSql);
			pstmt.setString(1, pageId);
			((OraclePreparedStatement) pstmt).setStringForClob(2,
					"{name:\"Study Screen\",sections:[{fields:[]}]}");
			((OraclePreparedStatement) pstmt).setStringForClob(3,
					"var studyScreenJS = {};");

			pstmt.executeUpdate();
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.testInsert EXCEPTION: "
					+ ex);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (Exception e) {
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
			}
		}
	}

	public void updateFlexPageCookie(String pageId, HashMap hash) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		String pageDef = (String) hash.get(FLX_PAGE_COOKIE);
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(upateFlexPageSql);
			((OraclePreparedStatement) pstmt).setStringForClob(1, pageDef);
			pstmt.setString(2, pageId);
			pstmt.executeUpdate();
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.testInsert EXCEPTION: "
					+ ex);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (Exception e) {
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
			}
		}
	}
	
	public int insertFlexPageVer(HashMap<String, Object> argMap) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int retNum = 0;		
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(insertFlexPageVerSql);
			pstmt.setString(1, (String)argMap.get(MOD_TABLE_STR));
			pstmt.setInt(2, (Integer)argMap.get(MOD_PK_STR));
			Integer pageVer = (Integer)argMap.get(PAGE_VER_STR);
			Integer pageMinorVer = (Integer)argMap.get(PAGE_MINOR_VER);
			if (pageVer == null) { pageVer = 1; }
			pstmt.setInt(3, pageVer);
			pstmt.setString(4, (String)argMap.get(PAGE_ID_STR));
			((OraclePreparedStatement) pstmt).setStringForClob(5, (String)argMap.get(PAGE_DEF_STR));
			((OraclePreparedStatement) pstmt).setStringForClob(6, (String)argMap.get(PAGE_JS_STR));
			((OraclePreparedStatement) pstmt).setStringForClob(7, (String)argMap.get(PAGE_JSON_STR));
			((OraclePreparedStatement) pstmt).setStringForClob(8, (String)argMap.get(PAGE_HTML_STR));
			pstmt.setInt(9, pageMinorVer);
			pstmt.setInt(10, (Integer)argMap.get(FK_SUBMISSION));
			retNum = pstmt.executeUpdate();
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.insertFlexPageVer EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return retNum;
	}
	
	public int getHighestFlexPageVersion(String tablename, int tablePk) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int retNum = 0;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getHighestFlexPageVersionSql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, (Integer)tablePk);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				retNum = rs.getInt(1);
			}
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getHighestFlexPageVersion EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return retNum;
	}

	public String getFlexPageHtmlOfHighestVersion(String tablename, int tablePk) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		String html = EMPTY_STRING;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getFlexPageHtmlOfHighestVersionSql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, (Integer)tablePk);
			pstmt.setString(3, (String)tablename);
			pstmt.setInt(4, (Integer)tablePk);
			pstmt.setString(5, (String)tablename);
			pstmt.setInt(6, (Integer)tablePk);
			pstmt.setString(7, (String)tablename);
			pstmt.setInt(8, (Integer)tablePk);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				html = rs.getString(1);
			}
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getFlexPageHtmlOfHighestVersion EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return html;
	}
	
	public int getFlexLatestSubmissionStatus(int fkStudy) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int submPk = 0;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getFlexPageLatestSubmissionStatusSql);
			pstmt.setInt(1, fkStudy);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				submPk = rs.getInt(1);
				submissionStatusPk = rs.getInt(2);
			}
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getFlexLatestSubmissionStatus EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return submPk;
	}
	
	public JSONObject getFlxPageCookie(JSONObject jsObjPageRec) {
		JSONObject newJSONObj = null;
		try {
			newJSONObj = flexPageConfigJson(jsObjPageRec);
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao",
					"UIFlxPageDao.getFlxPageCookie EXCEPTION: " + ex);
		}
		if (newJSONObj == null) {
			newJSONObj = new JSONObject();
		}
		return newJSONObj;
	}

	public String getFlxPageJS(JSONObject jsObjPageRec) {
		String newFlexPageJS = null;
		try {
			newFlexPageJS = flexPageJS(jsObjPageRec);
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao",
					"UIFlxPageDao.getFlxPageJS EXCEPTION: " + ex);
		}
		return newFlexPageJS;
	}
	
	public JSONObject flexPageData(String pageId) throws Exception {
		JSONObject jsObjPageRec = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getFlexPageCookie);
			pstmt.setString(1, pageId);

			ResultSet rs = pstmt.executeQuery();
			if (rs == null) {
				return jsObjPageRec;
			}
			while (rs.next()) {
				jsObjPageRec = new JSONObject();

				String myPageDef = rs.getString(PAGE_DEF);
				jsObjPageRec.put(PAGE_DEF, myPageDef);
				
				String myPageJS = rs.getString(PAGE_JS);
				jsObjPageRec.put(PAGE_JS, myPageJS);
				return jsObjPageRec;
			}
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao",
					"UIFlxPageDao.flexPageData EXCEPTION: " + ex);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (Exception e) {
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
			}
		}
		return jsObjPageRec;
	}

	private JSONObject flexPageConfigJson(JSONObject jsObj) throws Exception {
		JSONObject newJSONObj = null;
		if (jsObj == null) {
			return newJSONObj;
		}
		String myPageDef = jsObj.getString(PAGE_DEF);
		newJSONObj = new JSONObject(myPageDef);
		return newJSONObj;
	}

	private String flexPageJS(JSONObject jsObj) throws Exception {
		String newFlexPageJS = null;
		if(jsObj == null) {
			return newFlexPageJS;
		}

		newFlexPageJS = jsObj.getString(PAGE_JS);
		return newFlexPageJS;
	}
	
	public static void main(String[] args) {
		UIFlxPageDao flxDao = new UIFlxPageDao();
		System.setProperty("ERES_HOME", "C:/Velos/eResearch_jboss510/conf/");
		HashMap<String, Object> argMap = new HashMap<String, Object>();
		try {
			String pageId = "protocol";
			JSONObject protocolJson = flxDao.flexPageData(pageId);
			argMap.put(PAGE_ID_STR, pageId);
			argMap.put(PAGE_DEF_STR, protocolJson.get(PAGE_DEF));
			argMap.put(PAGE_JS_STR, protocolJson.get(PAGE_JS));
		} catch (Exception e) {
			e.printStackTrace();
		}
		argMap.put(MOD_TABLE_STR, "er_study");
		argMap.put(MOD_PK_STR, 876);
		argMap.put(PAGE_JSON_STR, "{test:123}");
		argMap.put(PAGE_HTML_STR, "<html></html>");
		int retNum = flxDao.insertFlexPageVer(argMap);
		System.out.println("retNum="+retNum);
	}
	public int getHighestMinorFlexPageVersion(int version, String tablename, int tablePk) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int retNum = 0;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getHighestMinorFlexPageVersionSql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, (Integer)tablePk);
			pstmt.setInt(3, version);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				retNum = rs.getInt(1);
			}
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getHighestMinorFlexPageVersion EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return retNum;
	}
		
	public String getFlexPageHtmlOfHighestApprovedVersion(String tablename, int tablePk) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		String html = EMPTY_STRING;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getFlexPageHtmlOfHighestApprovedVersionSql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, (Integer)tablePk);			
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				html = rs.getString(1);
			}
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getFlexPageHtmlOfHighestApprovedVersionSql EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return html;
	}
	
	private Integer majorVer = 0;
	private Integer minorVer = 0;
	private Integer submissionPk = 0;
	public Integer getMajorVer() { return majorVer; }
	public Integer getMinorVer() { return minorVer; }
	public Integer getSubmissionPk() { return submissionPk; }

	public void getHighestFlexPageFullVersion(String tablename, int tablePk) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getHighestFlexPageFullVersionSql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, (Integer)tablePk);			
			rs = pstmt.executeQuery();
			while(rs.next()) {
				majorVer = rs.getInt(1);
				minorVer = rs.getInt(2);
				submissionPk = rs.getInt(3);
				if (submissionPk == null) {
					submissionPk = 0;
				}
				break;
			}
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getHighestFlexPageFullVersion EXCEPTION:"+ ex);
			majorVer = 0;
			minorVer = 0;
			submissionPk = 0;
		} finally {
			try { if (rs != null) { rs.close(); } } catch (Exception e) {}
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
	}
	
	public String getFlexPageVerOfHighestApprovedVersion(String tablename, int tablePk) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		String pageVer = EMPTY_STRING;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getFlexPageVerOfHighestApprovedVersionSql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, (Integer)tablePk);			
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				pageVer = rs.getString(1);
			}
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getFlexPageVerOfHighestApprovedVersionSql EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return pageVer;
	}
	
	public HashMap<String, Object> getDetailedFlexPage(int studyId, int pageVer, int pageMinorVer) {
		HashMap<String, Object> resMap = new HashMap<String, Object>();
		PreparedStatement pstmt = null;
		Connection conn = null;
		String pageId = EMPTY_STRING;
		String pageDef = EMPTY_STRING;
		String pageJs = EMPTY_STRING;
		String pageJson = EMPTY_STRING;
		String pageHtml = EMPTY_STRING;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getDetailedFlexPageSql);
			pstmt.setInt(1, studyId);
			pstmt.setInt(2, pageVer);	
			pstmt.setInt(3, pageMinorVer);			
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				resMap.put("pageModTable", rs.getString("PAGE_MOD_TABLE"));
				resMap.put("pageModPk", rs.getString("PAGE_MOD_PK"));
				resMap.put("pageVer", rs.getString("PAGE_VER"));
				resMap.put("pageMinorVer", rs.getString("PAGE_MINOR_VER"));
				resMap.put("pageId", rs.getString("PAGE_ID"));
				resMap.put("pageDef", rs.getString("PAGE_DEF"));
				resMap.put("pageJs", rs.getString("PAGE_JS"));
				resMap.put("pageJson", rs.getString("PAGE_JSON"));
				resMap.put("pageHtml", rs.getString("PAGE_HTML"));	
				resMap.put("fkSubmission", rs.getInt("FK_SUBMISSION"));		
			}
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getDetailedFlexPage EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return resMap;
	}

	public String getPageVersionDetails(String tablename, int studyId, String studyVersion) {
		String json = "";
		String[] studyVersionString;
		int page_ver = -1;
		int page_minor_ver = -1;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try {

			if (studyVersion.contains(".")) {
				studyVersionString = studyVersion.split("\\.");
				if (studyVersionString.length == 2) {
					page_ver = Integer.parseInt(studyVersionString[0]);
					page_minor_ver = Integer.parseInt(studyVersionString[1]);
				}
			} else {
				page_ver = Integer.parseInt(studyVersion);
			}

			conn = getConnection();

			String mysql = getFlexPageVersionDiffSql;
			if (page_minor_ver != -1) {
				mysql += " and page_minor_ver = ?";
			}

			pstmt = conn.prepareStatement(mysql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, studyId);
			pstmt.setInt(3, page_ver);
			if (page_minor_ver != -1) {
				pstmt.setInt(4, page_minor_ver);
			}
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				json = rs.getString("PAGE_JSON");
			}

		} catch (SQLException e) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getPageVersionDetails EXCEPTION:"+ e);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
		return json;
	}
	
	public int chkPageVersionSubmited(String tableName, int studyId) {
		int versionSubmitted = -1;
		int highestVersion = getHighestFlexPageVersion(tableName, studyId);
		if(highestVersion == 0)
		{
			versionSubmitted = -1;
		}
		else
		{
			int count = 0;
			String[] studyVersionString;
			int page_ver = -1;
			int page_minor_ver = -1;
			PreparedStatement pstmt = null;
			Connection conn = null;
			try {

				conn = getConnection();

				pstmt = conn.prepareStatement(chkPageVersionSubmited);

				pstmt.setString(1, tableName);
				pstmt.setInt(2, studyId);
				pstmt.setString(3, tableName);
				pstmt.setInt(4, studyId);
				pstmt.setString(5, tableName);
				pstmt.setInt(6, studyId);
			
				ResultSet rs = pstmt.executeQuery();

				while (rs.next()) {
					count = rs.getInt(1);
				}

			}  catch (Exception e) {
				Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.chkPageVersionSubmited EXCEPTION:"+ e);
			} finally {
				try {
					if (pstmt != null)
						pstmt.close();
				} catch (Exception e) {
				}
				try {
					if (conn != null)
						conn.close();
				} catch (Exception e) {
				}
			}
			if(count > 0)
				versionSubmitted = 1;
			else
				versionSubmitted = 0;
		}
		return versionSubmitted;
	}

	
	public int updateCurrentPageVersionChanges(String tableName, int studyId, String changelog, int fk_submission) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int retNum = 0;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(updateCurrentPageVersionChangesSql);
			pstmt.setString(1, changelog);
			pstmt.setInt(2, fk_submission);
			pstmt.setString(3, tableName);
			pstmt.setInt(4, studyId);
			pstmt.setString(5, tableName);
			pstmt.setInt(6, studyId);
			pstmt.setString(7, tableName);
			pstmt.setInt(8, studyId);
			ResultSet rs = pstmt.executeQuery();
			
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getScndHighestFlexPageVersionSql EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return retNum;
	}
	
	public int checkLockedVersionExists(int studyId, int verNum) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int retNum = 0;
		try {
			conn = getConnection();
			String Sql = "select rownum from ui_flx_pagever where page_mod_pk=? and page_ver=? and page_minor_ver=0";
			pstmt = conn.prepareStatement(Sql);
			pstmt.setInt(1, studyId);
			pstmt.setInt(2, verNum);			
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				retNum = rs.getInt("rownum");
			}
			
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.checkLockedVersionExists EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return retNum;
	}	
	public int getApprovedVersionForStudy(int studyId) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int retNum = 0;
		try {
			conn = getConnection();
			String Sql = "select page_ver from ui_flx_pagever where page_mod_pk=? and page_minor_ver=0 order by page_ver ASC";
			pstmt = conn.prepareStatement(Sql);
			pstmt.setInt(1, studyId);
					
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				retNum = rs.getInt("page_ver");
			}
			
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getApprovedVersionForStudy EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return retNum;
	}	
	
	public ArrayList<HashMap<String, String>> getAllVersionsForProtocol(String tablename, int tablePk) {
		ArrayList<HashMap<String, String>> resList = new ArrayList<HashMap<String, String>>();

		PreparedStatement pstmt = null;
		Connection conn = null;
		try {
			conn = getConnection();

			String mysql = getAllVersionsForProtocolSql;

			pstmt = conn.prepareStatement(mysql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, tablePk);
		
			ResultSet rs = pstmt.executeQuery();

			while(rs.next()) {
				HashMap<String, String> resMap = new HashMap<String, String>();
				resMap.put("page_ver", rs.getString("PAGE_VER"));
				resMap.put("page_minor_ver", rs.getString("PAGE_MINOR_VER"));
				resMap.put("submissionTypeDesc", rs.getString("SUBMISSION_TYPE_DESC"));
				resList.add(resMap);
			}

		} catch (SQLException e) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getAllVersionsForProtocol EXCEPTION:"+ e);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
		return resList;
	}
	
	public String getFlexPageHtmlOfVersion(String tablename, int tablePk, int pageVer, int pageMinorVer) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		String html = EMPTY_STRING;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getFlexPageHtmlOfVersionSql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, (Integer)tablePk);
			pstmt.setInt(3, pageVer);
			pstmt.setInt(4, pageMinorVer);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				html = rs.getString(1);
			}
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getFlexPageHtmlOfVersion EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return html;
	}
	
	public String getFlexPageVersionDiffDetails(String tablename, int tablePk, int pageVer, int pageMinorVer) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		String verDiff = EMPTY_STRING;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getFlexPageVersionDiffDetailsSql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, (Integer)tablePk);
			pstmt.setInt(3, pageVer);
			pstmt.setInt(4, pageMinorVer);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				verDiff = rs.getString(1);
			}
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getFlexPageVersionDiffDetails EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return verDiff;
	}
}
