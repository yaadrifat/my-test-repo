package com.velos.eres.widget.service.util;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.study.StudyJB;

public enum  FlxPageSections {
	//Study Team
	STUDY_STUDYTEAM("STUDY", "STUDYTEAM", "Study Team"),
    //Study Documents
  	//STUDY_STUDYDOCS("STUDY", "STUDYDOCS", "Documents"),
  	 //Study IND/IDE
  	STUDY_STUDYINDIDE("STUDY", "STUDYINDIDE", "IND/IDE"),
	;
	
	private String pageKeyword;
	private String sectionKeyword;
	private String sectionLabel;
	
	private final static String SECTION_TYPE = "sectionType";
	private final static String EMPTY_STRING = "";
	
	private final static String ID = "id";
	private final static String NAME = "name";
	static final String Str_selection_single = "single", Str_selection_multi = "multi";

	public String getFlxPageSectionKey() { return this.toString(); }
	public String getPageKeyword() { return pageKeyword; }
	public String getSectionKeyword() { return sectionKeyword;}
	public String getSectionLabel() { return sectionLabel;}

	FlxPageSections(){}
	FlxPageSections(String page, String sectionKeyword, String sectionLabel) {
		this.pageKeyword = page;
		this.sectionKeyword = sectionKeyword;
		this.sectionLabel = sectionLabel;
	}

	public static FlxPageSections getFlxPageSectionByKey(String sectEnumKey) { 
		for (FlxPageSections pageSection : FlxPageSections.values()) {
			
	        if (pageSection.name().equals(sectEnumKey)) {
				return pageSection;
	        }
		}
		return null;
	}

	public static JSONArray getSectionsForPage(String page){
		JSONArray pageSectionJSArray = new JSONArray();
		if (StringUtil.isEmpty(page)) return null;
		int indx = 0;
		JSONObject sectionJSON = null;
		
		for (FlxPageSections pageSection : FlxPageSections.values()) {
			sectionJSON = new JSONObject();
			
	        if (pageSection.getPageKeyword().equals(page)) {
	        	try {
	        		sectionJSON.put("seq", indx);
	        		sectionJSON.put("type", "STD");
	        		sectionJSON.put("flxSectionId", ""+pageSection.name());
	        		sectionJSON.put("label", pageSection.getSectionLabel());
		        	pageSectionJSArray.put(sectionJSON);
					indx++;
				} catch (JSONException e) {
					//e.printStackTrace();
				}
	        }
	    }
		return pageSectionJSArray;
	}
}
