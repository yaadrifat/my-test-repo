package com.velos.eres.widget.web;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.velos.eres.business.common.StudyDao;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.financials.service.FinancialsAgent;
import com.velos.eres.service.studyAgent.StudyAgent;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.widget.business.common.UIGadgetDao;

public class GadgetFinancialJB {
	
	public GadgetFinancialJB() {}
	
	private static final String GADGET_FINANCIAL = "gadgetFinancial";
	private static final String GADGET_FINANCIALJB_DOT = "gadgetFinancialJB.";

	//Column Keywords
    private static final String STUDYIDS = "studyIds";
    private static final String STUDYID_KEY = "fk_study";
    private static final String STUDYNUMBER_KEY = "studyNumber";
    private static final String INVOICEABLE_KEY = "invoiceable";
    private static final String INVOICED_KEY = "invoiced";
    private static final String UNINVOICED_KEY = "uninvoiced";
    private static final String COLLECTED_KEY = "collected";
    private static final String OUTSTANDING_KEY = "outstanding";
    private static final String RECEIPTS_KEY = "receipts";
    private static final String DISBURSEMENTS_KEY = "disbursements";
    private static final String NETCASH_KEY = "netCash";

    //View Keywords
    private static final String BILLING_KEY = "billing";
    private static final String COLLECTION_KEY = "collection";
    private static final String PAYMENTS_KEY = "payments";
	
    private String settingsStudyIds = null;
	private int settingsStudyCount = 0;
	
	private LinkedHashMap<String, Object> settingsHash = null;
    JSONArray myBillingColumns  = null;
    JSONArray myBillingData  = null;
    
    JSONArray myCollectionColumns  = null;
    JSONArray myCollectionData  = null;

    JSONArray myPaymentsColumns  = null;
    JSONArray myPaymentsData  = null;

	private String accountId = null;
	private String userId = null;
	
	private void loadGadgetSettings(String userId){
		this.userId = userId;

		UserAgentRObj usrAgent = EJBUtil.getUserAgentHome();
		UserBean uBean = usrAgent.getUserDetails(StringUtil.stringToNum(this.userId));
		this.accountId = uBean.getUserAccountId();

    	UIGadgetDao uiGadgetDao = new UIGadgetDao();
    	settingsHash = uiGadgetDao.getGadgetInstanceSettingsHash(userId, GADGET_FINANCIAL);
    	
    	if (null != settingsHash){
	    	settingsStudyIds = (String)settingsHash.get(STUDYIDS);
	    	if (!StringUtil.isEmpty(settingsStudyIds)){
	    		settingsStudyIds=settingsStudyIds.substring(settingsStudyIds.indexOf("[")+1, settingsStudyIds.indexOf("]"));

	    		if (!StringUtil.isEmpty(settingsStudyIds)){
		    		StudyAgent studyAgent =  EJBUtil.getStudyAgentHome();
		        	StudyDao studyDao1 = studyAgent.getStudyAutoRows(this.userId, this.accountId, 
		        			this.settingsStudyIds, 1);
		        	ArrayList arrSettingStudyIds = studyDao1.getStudyAutoIds();

		        	if (null != arrSettingStudyIds){
		        		settingsStudyIds = arrSettingStudyIds.toString();
		        		settingsStudyIds=settingsStudyIds.substring(settingsStudyIds.indexOf("[")+1, settingsStudyIds.indexOf("]"));

			        	settingsStudyCount = arrSettingStudyIds.size();
		        	} else {
		        		settingsStudyIds = null;
		        		settingsStudyCount = 0;
		        	}
	    		} else {
	    			settingsStudyIds = null;
	        		settingsStudyCount = 0;
	    		}
	    	} else{
	    		settingsStudyIds = null;
	        	settingsStudyCount = 0;
	    	}
    	} else {
    		settingsStudyIds = null;
        	settingsStudyCount = 0;
    	}
	}

	public GadgetFinancialJB(String userId) {
		this.loadGadgetSettings(userId);
	}
	
	public void setSettingsHash(LinkedHashMap<String, Object>  settingsHash) {
		this.settingsHash = settingsHash; 
	}

	public LinkedHashMap<String, Object> getSettingsHash() {
		return settingsHash;
	}

	public void setSettingsStudyIds(String settingsStudyIds) {
		this.settingsStudyIds = settingsStudyIds;
	}

	public int getSettingsStudyCount() {
		return settingsStudyCount;
	}
	
	public void setSettingsStudyCount(int settingsStudyCount) {
		this.settingsStudyCount = settingsStudyCount;
	}

	public String getSettingsStudyIds() {
		return settingsStudyIds;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBillingColumns(){
		myBillingColumns = new JSONArray();

		try{
			JSONObject aColumn = new JSONObject();	
						
			aColumn.put("key", STUDYID_KEY);
			aColumn.put("label","Study ID");
			aColumn.put("hidden","true");
			this.myBillingColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", STUDYNUMBER_KEY);
			aColumn.put("label",LC.L_Study_Number);
			aColumn.put("type","image");
			this.myBillingColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", "studyAccess");
			aColumn.put("label","Study Access");
			aColumn.put("hidden","true");
			this.myBillingColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", INVOICEABLE_KEY);
			aColumn.put("label",LC.L_Invoiceable);
			this.myBillingColumns.put(aColumn);

			aColumn = new JSONObject();
			aColumn.put("key", INVOICED_KEY);
			aColumn.put("label",LC.L_Invoiced);
			this.myBillingColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", UNINVOICED_KEY);
			aColumn.put("label",LC.L_Uninvoiced);
			this.myBillingColumns.put(aColumn);
		} catch (Exception e){
			Rlog.fatal("finBillingColumns", "myBillingColumns.put error: " + e);
		}
		
		return myBillingColumns.toString();
	}
	
    public String getBilling(String accId, String userId, String studyIds){
    	myBillingData = new JSONArray();
    	if (StringUtil.isEmpty(accId)) { return null; }
    	if (StringUtil.isEmpty(userId)) { return null; }
    	if (StringUtil.isEmpty(studyIds)) { return null; }
    	
		FinancialsAgent gdtFinAgent = EJBUtil.getFinancialsAgentHome();
		myBillingData = gdtFinAgent.getBilling(accId, userId, studyIds);
    	return myBillingData.toString();
	}

    public JSONArray getBillingDrillDownColumns(String calledFrom) 
    throws JSONException {
       JSONArray jsColArray = new JSONArray();
        {
            JSONObject jsObjTemp1 = new JSONObject();
            jsObjTemp1.put("key", "mileType");
            jsObjTemp1.put("label", LC.L_Milestone_Type);
            jsColArray.put(jsObjTemp1);

            jsObjTemp1 = new JSONObject();
            jsObjTemp1.put("key", "enrollingSite");
            jsObjTemp1.put("label", LC.L_Organization);
            jsColArray.put(jsObjTemp1);

            jsObjTemp1 = new JSONObject();
            jsObjTemp1.put("key", "patientName");
            jsObjTemp1.put("label", LC.L_Pat_Name);
            jsColArray.put(jsObjTemp1);

        	jsObjTemp1 = new JSONObject();
            jsObjTemp1.put("key", "patStudyId");
            jsObjTemp1.put("label", LC.L_Patient_StudyId);
            jsColArray.put(jsObjTemp1);
            
            jsObjTemp1 = new JSONObject();
            jsObjTemp1.put("key", "patientCode");
            jsObjTemp1.put("label",LC.L_Std_Stat_Date);
            jsColArray.put(jsObjTemp1);
            
            jsObjTemp1 = new JSONObject();
            jsObjTemp1.put("key", "calendar");
            jsObjTemp1.put("label", LC.L_Calendar);
            jsColArray.put(jsObjTemp1);
            
            jsObjTemp1 = new JSONObject();
            jsObjTemp1.put("key", "visit");
            jsObjTemp1.put("label", LC.L_Visit);
            jsColArray.put(jsObjTemp1);
            
            jsObjTemp1 = new JSONObject();
            jsObjTemp1.put("key", "coverageType");
            jsObjTemp1.put("label", LC.L_Coverage_Analysis);
            jsColArray.put(jsObjTemp1);
            
            jsObjTemp1 = new JSONObject();
            jsObjTemp1.put("key", "descService");
            jsObjTemp1.put("label", LC.L_Description);
            jsColArray.put(jsObjTemp1);
            
            if (INVOICEABLE_KEY.equals(calledFrom)){
	            jsObjTemp1 = new JSONObject();
	            jsObjTemp1.put("key", INVOICEABLE_KEY);
	            jsObjTemp1.put("label", LC.L_Invoiceable);
	            jsObjTemp1.put("isNumber", "Y");
	            jsColArray.put(jsObjTemp1);
            }
            if (INVOICED_KEY.equals(calledFrom)){
	            jsObjTemp1 = new JSONObject();
	            jsObjTemp1.put("key", INVOICED_KEY);
	            jsObjTemp1.put("label", LC.L_Invoiced);
	            jsObjTemp1.put("isNumber", "Y");
	            jsColArray.put(jsObjTemp1);
            }
            if (UNINVOICED_KEY.equals(calledFrom)){
	            jsObjTemp1 = new JSONObject();
	            jsObjTemp1.put("key", UNINVOICED_KEY);
	            jsObjTemp1.put("label", LC.L_Uninvoiced);
	            jsObjTemp1.put("isNumber", "Y");
	            jsColArray.put(jsObjTemp1);
            }
        }

        return jsColArray;
    }

    public JSONArray getBillingDrillDownData(String studyId, String calledFrom) 
    throws JSONException {
    	JSONArray jsMileAchieveData = new JSONArray();
    	
    	FinancialsAgent gdtFinAgent = EJBUtil.getFinancialsAgentHome();
    	jsMileAchieveData = gdtFinAgent.getBillingDrillDownData(this.userId, studyId, calledFrom);
    	return jsMileAchieveData;
    }
    
    public JSONArray getCollectionDrillDownColumns(String calledFrom) 
    throws JSONException {
       JSONArray jsColArray = new JSONArray();
        {
            JSONObject jsObjTemp1 = new JSONObject();
            jsObjTemp1.put("key", "invoiceNumber");
            jsObjTemp1.put("label", LC.L_Inv_Number);
            jsColArray.put(jsObjTemp1);
            
            jsObjTemp1 = new JSONObject();
            jsObjTemp1.put("key", "invoiceDate");
            jsObjTemp1.put("label", LC.L_Invoice_Date);
            jsColArray.put(jsObjTemp1);
            
            jsObjTemp1 = new JSONObject();
            jsObjTemp1.put("key", "paymentDueDate");
            jsObjTemp1.put("label", LC.L_Payment_DueDt);
            jsColArray.put(jsObjTemp1);
            
            if (INVOICED_KEY.equals(calledFrom)){
	            jsObjTemp1 = new JSONObject();
	            jsObjTemp1.put("key", INVOICED_KEY);
	            jsObjTemp1.put("label", LC.L_Invoiced_Amt);
	            jsObjTemp1.put("isNumber", "Y");
	            jsColArray.put(jsObjTemp1);
            }
            
            if (COLLECTED_KEY.equals(calledFrom)){
            	jsObjTemp1 = new JSONObject();
	            jsObjTemp1.put("key", "paymentDate");
	            jsObjTemp1.put("label", LC.L_Payment_Date);
	            jsColArray.put(jsObjTemp1);
	            
	            jsObjTemp1 = new JSONObject();
	            jsObjTemp1.put("key", COLLECTED_KEY);
	            jsObjTemp1.put("label", LC.L_Collected);
	            jsObjTemp1.put("isNumber", "Y");
	            jsColArray.put(jsObjTemp1);
            }
            
            if (OUTSTANDING_KEY.equals(calledFrom)){
	            jsObjTemp1 = new JSONObject();
	            jsObjTemp1.put("key", OUTSTANDING_KEY);
	            jsObjTemp1.put("label", LC.L_Outstanding);
	            jsObjTemp1.put("isNumber", "Y");
	            jsColArray.put(jsObjTemp1);
            }
        }

        return jsColArray;
    }
    public JSONArray getCollectionDrillDownData(String studyId, String calledFrom) 
    throws JSONException {
    	JSONArray jsCollectionData = new JSONArray();
    	
    	FinancialsAgent gdtFinAgent = EJBUtil.getFinancialsAgentHome();
    	jsCollectionData = gdtFinAgent.getCollectionDrillDownData(this.userId, studyId, calledFrom);
    	return jsCollectionData;
    }
    
    public JSONArray getPaymentsDrillDownColumns(String calledFrom) 
    throws JSONException {
       JSONArray jsColArray = new JSONArray();
        {
            JSONObject jsObjTemp1 = new JSONObject();
            jsObjTemp1.put("key", "paymentDate");
            jsObjTemp1.put("label", LC.L_Payment_Date);
            jsColArray.put(jsObjTemp1);
            
            if (!NETCASH_KEY.equals(calledFrom)){
	            jsObjTemp1 = new JSONObject();
	            jsObjTemp1.put("key", "paymentDesc");
	            jsObjTemp1.put("label", LC.L_Description);
	            jsColArray.put(jsObjTemp1);
	            
	            jsObjTemp1 = new JSONObject();
	            jsObjTemp1.put("key", "paymentNotes");
	            jsObjTemp1.put("label", LC.L_Notes);
	            jsColArray.put(jsObjTemp1);
            }

            if (RECEIPTS_KEY.equals(calledFrom)){
	            jsObjTemp1 = new JSONObject();
	            jsObjTemp1.put("key", RECEIPTS_KEY);
	            jsObjTemp1.put("label", LC.L_Receipts);
	            jsObjTemp1.put("isNumber", "Y");
	            jsColArray.put(jsObjTemp1);
            }
            
            if (DISBURSEMENTS_KEY.equals(calledFrom)){
	            jsObjTemp1 = new JSONObject();
	            jsObjTemp1.put("key", DISBURSEMENTS_KEY);
	            jsObjTemp1.put("label", LC.L_Disbursements);
	            jsObjTemp1.put("isNumber", "Y");
	            jsColArray.put(jsObjTemp1);
            }
            
            if (NETCASH_KEY.equals(calledFrom)){
            	jsObjTemp1 = new JSONObject();
 	            jsObjTemp1.put("key", RECEIPTS_KEY);
 	            jsObjTemp1.put("label", LC.L_Receipts);
 	            jsObjTemp1.put("isNumber", "Y");
 	            jsColArray.put(jsObjTemp1);
 	            
 	            jsObjTemp1 = new JSONObject();
	            jsObjTemp1.put("key", DISBURSEMENTS_KEY);
	            jsObjTemp1.put("label", LC.L_Disbursements);
	            jsObjTemp1.put("isNumber", "Y");
	            jsColArray.put(jsObjTemp1);
 	            
	            jsObjTemp1 = new JSONObject();
	            jsObjTemp1.put("key", NETCASH_KEY);
	            jsObjTemp1.put("label", LC.L_NetCash);
	            jsObjTemp1.put("isNumber", "Y");
	            jsColArray.put(jsObjTemp1);
            }
        }

        return jsColArray;
    }
    public JSONArray getPaymentsDrillDownData(String studyId, String calledFrom) 
    throws JSONException {
    	JSONArray jsPaymentsData = new JSONArray();
    	
    	FinancialsAgent gdtFinAgent = EJBUtil.getFinancialsAgentHome();
    	jsPaymentsData = gdtFinAgent.getPaymentsDrillDownData(this.userId, studyId, calledFrom);
    	return jsPaymentsData;
    }
    public String fetchInvoiceablesJSON(String studyId) 
    	throws JSONException {
    	return fetchDrillDownJSON(studyId, INVOICEABLE_KEY, BILLING_KEY);
    }

    public String fetchInvoicedJSON(String studyId, String viewName) 
		throws JSONException {
		return fetchDrillDownJSON(studyId, INVOICED_KEY, viewName);
    }
    
    public String fetchUninvoicedJSON(String studyId) 
		throws JSONException {
		return fetchDrillDownJSON(studyId, UNINVOICED_KEY, BILLING_KEY);
	}
    
    public String fetchCollectedJSON(String studyId) 
		throws JSONException {
		return fetchDrillDownJSON(studyId, COLLECTED_KEY, COLLECTION_KEY);
	}

    public String fetchOutstandingJSON(String studyId) 
		throws JSONException {
		return fetchDrillDownJSON(studyId, OUTSTANDING_KEY, COLLECTION_KEY);
	}
    
    public String fetchReceiptsJSON(String studyId) 
		throws JSONException {
		return fetchDrillDownJSON(studyId, RECEIPTS_KEY, PAYMENTS_KEY);
	}
	
	public String fetchDisbursementsJSON(String studyId) 
		throws JSONException {
		return fetchDrillDownJSON(studyId, DISBURSEMENTS_KEY, PAYMENTS_KEY);
	}

	public String fetchNetCashJSON(String studyId) 
		throws JSONException {
		return fetchDrillDownJSON(studyId, NETCASH_KEY, PAYMENTS_KEY);
	}

    public String fetchDrillDownJSON(String studyId, String calledFrom , String viewName) 
    throws JSONException {
        JSONObject jsObj = new JSONObject();

        if (StringUtil.isEmpty(calledFrom)){
            jsObj.put("error", new Integer(-3));
            jsObj.put("errorMsg", "Invalid Call.");
            return jsObj.toString();
        }

        JSONArray jsColArray = new JSONArray();
        JSONArray jsMileAchieveData = new JSONArray();

	    if (BILLING_KEY.equals(viewName)){
	        if (INVOICEABLE_KEY.equals(calledFrom) || INVOICED_KEY.equals(calledFrom) || UNINVOICED_KEY.equals(calledFrom)){
	        	jsColArray = getBillingDrillDownColumns(calledFrom);
	            jsMileAchieveData = getBillingDrillDownData(studyId, calledFrom);
	        }
	    }
	    if (COLLECTION_KEY.equals(viewName)){
	        if (INVOICED_KEY.equals(calledFrom) || COLLECTED_KEY.equals(calledFrom) || OUTSTANDING_KEY.equals(calledFrom)){
	        	jsColArray = getCollectionDrillDownColumns(calledFrom);
	        	jsMileAchieveData = getCollectionDrillDownData(studyId, calledFrom);
	        }
	    }
	    if (PAYMENTS_KEY.equals(viewName)){
	        if (RECEIPTS_KEY.equals(calledFrom) || DISBURSEMENTS_KEY.equals(calledFrom) || NETCASH_KEY.equals(calledFrom)){
	        	jsColArray = getPaymentsDrillDownColumns(calledFrom);
	        	jsMileAchieveData = getPaymentsDrillDownData(studyId, calledFrom);
	        }
	    }
	    
        jsObj.put("colArray", jsColArray);
    	jsObj.put("dataArray", jsMileAchieveData);
    	jsObj.put("colArray", jsColArray);
        
        return jsObj.toString();
    }
    
    public String getCollectionColumns(){
		myCollectionColumns = new JSONArray();

		try{
			JSONObject aColumn = new JSONObject();	
						
			aColumn.put("key", STUDYID_KEY);
			aColumn.put("label","Study ID");
			aColumn.put("hidden","true");
			this.myCollectionColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", STUDYNUMBER_KEY);
			aColumn.put("label",LC.L_Study_Number);
			aColumn.put("type","image");
			this.myCollectionColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", "studyAccess");
			aColumn.put("label","Study Access");
			aColumn.put("hidden","true");
			this.myCollectionColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", INVOICED_KEY);
			aColumn.put("label",LC.L_Invoiced);
			this.myCollectionColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", COLLECTED_KEY);
			aColumn.put("label",LC.L_Collected);
			this.myCollectionColumns.put(aColumn);

			aColumn = new JSONObject();
			aColumn.put("key", OUTSTANDING_KEY);
			aColumn.put("label",LC.L_Outstanding);
			this.myCollectionColumns.put(aColumn);
		} catch (Exception e){
			Rlog.fatal("finCollectionColumns", "myCollectionColumns.put error: " + e);
		}
		
		return myCollectionColumns.toString();
	}
	
    public String getCollection(String accId, String userId, String studyIds){
    	myCollectionData = new JSONArray();
    	if (StringUtil.isEmpty(accId)) { return null; }
    	if (StringUtil.isEmpty(userId)) { return null; }
    	if (StringUtil.isEmpty(studyIds)) { return null; }
    	
		FinancialsAgent gdtFinAgent = EJBUtil.getFinancialsAgentHome();
		myCollectionData = gdtFinAgent.getCollection(accId, userId, studyIds);
    	return myCollectionData.toString();
	}

    public String getPaymentsColumns(){
		myPaymentsColumns = new JSONArray();

		try{
			JSONObject aColumn = new JSONObject();	
						
			aColumn.put("key", STUDYID_KEY);
			aColumn.put("label","Study ID");
			aColumn.put("hidden","true");
			this.myPaymentsColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", STUDYNUMBER_KEY);
			aColumn.put("label",LC.L_Study_Number);
			aColumn.put("type","image");
			this.myPaymentsColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", "studyAccess");
			aColumn.put("label","Study Access");
			aColumn.put("hidden","true");
			this.myPaymentsColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", RECEIPTS_KEY);
			aColumn.put("label",LC.L_Receipts);
			this.myPaymentsColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", DISBURSEMENTS_KEY);
			aColumn.put("label",LC.L_Disbursements);
			this.myPaymentsColumns.put(aColumn);

			aColumn = new JSONObject();
			aColumn.put("key", NETCASH_KEY);
			aColumn.put("label",LC.L_NetCash);
			this.myPaymentsColumns.put(aColumn);
		} catch (Exception e){
			Rlog.fatal("finCollectionColumns", "myPaymentsColumns.put error: " + e);
		}
		
		return myPaymentsColumns.toString();
	}
	
    public String getPayments(String accId, String userId, String studyIds){
    	myPaymentsData = new JSONArray();
    	if (StringUtil.isEmpty(accId)) { return null; }
    	if (StringUtil.isEmpty(userId)) { return null; }
    	if (StringUtil.isEmpty(studyIds)) { return null; }
    	
		FinancialsAgent gdtFinAgent = EJBUtil.getFinancialsAgentHome();
		myPaymentsData = gdtFinAgent.getPayments(accId, userId, studyIds);
    	return myPaymentsData.toString();
	}
}
