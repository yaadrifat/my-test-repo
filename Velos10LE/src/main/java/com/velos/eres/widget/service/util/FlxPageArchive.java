package com.velos.eres.widget.service.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.StudyIdDao;
import com.velos.eres.compliance.web.ComplianceJB;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.study.StudyJB;
import com.velos.eres.web.submission.SubmissionJB;
import com.velos.eres.web.user.UserJB;
import com.velos.eres.widget.business.common.UIFlxPageDao;

public class FlxPageArchive {
	public static final String EMPTY_STR = "", EMPTY_STRING = "", TITLE_STR = "title", FIELDS_STR = "fields", SECTIONS_STR = "sections";
	public static final String FLX_FIELD_ID_STR = "flxFieldId", STD_STR = "STD", MSD_STR = "MSD", TYPE_STR = "type", VERSION_STR = "version";
	public static final String JSON_STR = "json", HTML_STR = "html", LABEL_STR = "label", CODE_PK_STR = "codePk", DATA_STR = "data", CODE_SUBTYP_STR = "codeSubtype";
	private static final String Str_dropdown = "dropdown", Str_Hidden_input = "hidden-input";
	private static final String ER_STUDY_STR = "er_study", PROTOCOL_STR = "protocol";
	private static final String Str_chkbox = "chkbox", Str_checkbox = "checkbox";
	
	public static void main(String[] args) {
		System.setProperty("ERES_HOME", "C:/Velos/eResearch_jboss510/conf/");
		StudyJB studyJB = new StudyJB();
		studyJB.setStudyNumber("IH-A1b");
		studyJB.setStudyTitle("IH A1b title");
		StudyIdDao sidDao = new StudyIdDao();
		FlxPageArchive archive = new FlxPageArchive();
		JSONObject archiveJson = archive.createArchiveJson("protocol", studyJB, sidDao, 1, 0);
		String archiveHtml = archive.createArchiveHtml("protocol", archiveJson);
		System.out.println(archiveHtml);
	}
	
	private JSONArray fetchPageSections(String pageId) {
		UIFlxPageDao flxDao = new UIFlxPageDao();
		try {
			JSONObject pageDefObj = flxDao.flexPageData(pageId);
			JSONObject sectionsObj = flxDao.getFlxPageCookie(pageDefObj);
			return sectionsObj.getJSONArray(SECTIONS_STR);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new JSONArray();
	}
	
	public String createArchive(StudyJB studyJB, StudyIdDao studyIdDao, HashMap paramMap) {
	    int retNum = 0;
		if (studyJB == null || studyIdDao == null) { return String.valueOf(retNum); }
	    UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
	    JSONObject flexPageData = null;
	    String pageDef = null;
	    String pageJS = null;
	    try {
	    	flexPageData = uiFlxPageDao.flexPageData(PROTOCOL_STR);
	    	pageDef = (String)flexPageData.get(UIFlxPageDao.PAGE_DEF);
	    	pageJS = (String)flexPageData.get(UIFlxPageDao.PAGE_JS);
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
 		/*ComplianceJB compJB = new ComplianceJB();
		String fullVersion = compJB.getNextVersion(paramMap);
	    int versionNum = 0;
	    int minorVerNum = 0;;
		if (fullVersion == null) {
			versionNum = 1;
		} else if (fullVersion.indexOf(".") > -1) {
			try {
				String numbers[] = fullVersion.split("\\.");
				versionNum = Integer.parseInt(numbers[0]);
				minorVerNum = Integer.parseInt(numbers[1]);
			} catch(Exception e) {
				versionNum = 1;
				minorVerNum = 0;
			}
		} else {
			try {
				versionNum = Integer.parseInt(fullVersion);
			} catch(Exception e) {
				versionNum = 1;
			}
		}*/	    
	    
	    int minorVerNum=0;
	    ComplianceJB compJB = new ComplianceJB();
	    String curStatus = compJB.getCurrentStudyStatus(paramMap);
 		int versionNum = uiFlxPageDao.getHighestFlexPageVersion("er_study",  studyJB.getId());	    
	    minorVerNum=uiFlxPageDao.getHighestMinorFlexPageVersion(versionNum, "er_study",  studyJB.getId());	
	    int rowExists = uiFlxPageDao.checkLockedVersionExists(studyJB.getId(), versionNum);
	   
	    if(minorVerNum > 0) {
	    	if(rowExists == 1) {
	    		versionNum++;
		    	minorVerNum=1;	
	    	} else if("crc_amend".equals(curStatus)) {
		    	versionNum++;
		    	minorVerNum=1;	    	
		    } else {
	    		minorVerNum++;
	    	}	    		    	
	    } else if("crc_amend".equals(curStatus)) {
	    	versionNum++;
	    	minorVerNum=1;	    	
	    } else {
	    	versionNum++;	    	
	    }
	      
	      
	    SubmissionJB submissionJB = new SubmissionJB();
	    int submissionId = submissionJB.getExistingSubmissionByFkStudy(String.valueOf(studyJB.getId()));
		JSONObject archiveJson = this.createArchiveJson("protocol", studyJB, studyIdDao, versionNum, minorVerNum);
		String archiveHtml = this.createArchiveHtml("protocol", archiveJson);
		HashMap<String, Object> argMap = new HashMap<String, Object>();
		argMap.put(UIFlxPageDao.MOD_TABLE_STR, ER_STUDY_STR);
		argMap.put(UIFlxPageDao.MOD_PK_STR, studyJB.getId());
		argMap.put(UIFlxPageDao.PAGE_VER_STR, versionNum);
		argMap.put(UIFlxPageDao.PAGE_ID_STR, PROTOCOL_STR);
		argMap.put(UIFlxPageDao.PAGE_DEF_STR, pageDef);
		argMap.put(UIFlxPageDao.PAGE_JS_STR, pageJS);
		argMap.put(UIFlxPageDao.PAGE_JSON_STR, archiveJson.toString());
		argMap.put(UIFlxPageDao.PAGE_HTML_STR, archiveHtml);
		argMap.put(UIFlxPageDao.PAGE_MINOR_VER, minorVerNum);
		argMap.put(UIFlxPageDao.FK_SUBMISSION, submissionId);
		retNum = uiFlxPageDao.insertFlexPageVer(argMap);		
		if (retNum < 1) { return String.valueOf(retNum); }
		String fullVersion = null;
		if (minorVerNum > 0) {
			fullVersion = versionNum+"."+minorVerNum;
		} else {
			fullVersion = String.valueOf(versionNum);
		}
	    return fullVersion;
	}
	
	public String createArchiveHtml(String pageId, JSONObject dataJson) {
		StringBuffer sb1 = new StringBuffer("<html>");
		sb1.append("<head>");
		sb1.append("<style>");
		sb1.append(".flex-section-field { border:1px solid #aaaaaa; color:#111111; padding:3px; margin:3px; } ");
		sb1.append(" tr { border:1px; color:#212121; padding:3px; margin:3px; } ");
		sb1.append(" td { border:1px; color:#212121; } ");
		sb1.append("</style>");
		sb1.append("</head><body>");
		sb1.append("<table class='flex-top-table' width='100%'>");
		try {
			sb1.append("<tr class='flex-section-header'><td align='right' colspan='2'>Version: ");
			sb1.append(dataJson.get(VERSION_STR));
			sb1.append("</td></tr>");
			JSONArray sectionDataArray = dataJson.getJSONArray(SECTIONS_STR);
			for (int iS = 0; iS < sectionDataArray.length(); iS++) {
				JSONObject section = sectionDataArray.getJSONObject(iS);
				sb1.append("<tr class='flex-section-header ui-accordion-header ui-helper-reset ui-state-active ui-corner-top'><td colspan='2'>");
				String sectionTitle = null;
				try {
					sectionTitle = (String)section.get(TITLE_STR);
				} catch(Exception e) {}
				sb1.append(sectionTitle).append("</td></tr>");
				JSONArray fieldArray = null;
				try {
					fieldArray = section.getJSONArray(FIELDS_STR);
				} catch(Exception e) {
					fieldArray = null;
				}
				if (fieldArray == null) {
					continue; // to next section
				}
				for (int iF = 0; iF < fieldArray.length(); iF++) {
					JSONObject fieldObj = (JSONObject)fieldArray.get(iF);
					try {
						if (STD_STR.equals(fieldObj.get(TYPE_STR))) {
							String flexKey = null;
							Iterator<?> flexKeys = fieldObj.keys();
							while(flexKeys.hasNext()) {
								String key = (String)flexKeys.next();
								if (!TYPE_STR.equals(key)) {
									flexKey = String.valueOf(key);
									break;
								}
							}
							FlxPageFields field = FlxPageFields.getFlxPageFieldByKey(flexKey);
							sb1.append("<tr class='flex-section-field'><td width='40%'>").append(field.getFieldLabel());
							sb1.append("</td><td width='60%'>").append(fieldObj.get(flexKey)).append("</td>");
							sb1.append("</tr>");
						} else if (MSD_STR.equals(fieldObj.get(TYPE_STR))) {
							sb1.append("<tr class='flex-section-field'><td width='40%'>").append(fieldObj.get(LABEL_STR));
							sb1.append("</td><td width='60%'>").append(fieldObj.get(DATA_STR)).append("</td>");
							sb1.append("</tr>");
						}
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		sb1.append("</table>");
		sb1.append("</body></html>");
		return sb1.toString();
	}
	
	public JSONObject createArchiveJson(String pageId, StudyJB studyJB, StudyIdDao studyIdDao, 
			int version, int minorVer) {
		JSONObject outputObj = new JSONObject();
		JSONArray sectionInputArray = null;
		JSONArray sectionOutputArray = new JSONArray();
		try {
			sectionInputArray = fetchPageSections(pageId);
			for (int iS = 0; iS < sectionInputArray.length(); iS++) {
				JSONObject section = sectionInputArray.getJSONObject(iS);
				String sectionTitle = null;
				try {
					sectionTitle = (String)section.get(TITLE_STR);
				} catch(Exception e) {}
				if (sectionTitle == null) { sectionTitle = EMPTY_STR; }
				JSONArray fieldArray = null;
				try {
					fieldArray = section.getJSONArray(FIELDS_STR);
				} catch(Exception e) {
					fieldArray = null;
				}
				if (fieldArray == null) {
					continue; // to next section
				}
				JSONArray fieldsOutputArray = new JSONArray();
				for (int iF = 0; iF < fieldArray.length(); iF++) {
					JSONObject fieldObj = (JSONObject)fieldArray.get(iF);
					if (STD_STR.equals(fieldObj.get(TYPE_STR))) {
						FlxPageFields field = FlxPageFields.getFlxPageFieldByKey((String)fieldObj.get(FLX_FIELD_ID_STR));
						JSONObject fieldJson = createPageFieldJsonStd(field, studyJB);
						if (fieldJson != null) {
							fieldJson.put(TYPE_STR, STD_STR);
							fieldsOutputArray.put(fieldJson);
						}
					} else if (MSD_STR.equals(fieldObj.get(TYPE_STR))) {
						JSONObject fieldJson = createPageFieldJsonMsd((String)fieldObj.get(FLX_FIELD_ID_STR), studyIdDao);
						if (fieldJson != null) {
							fieldJson.put(TYPE_STR, MSD_STR);
							fieldsOutputArray.put(fieldJson);
						}
					}
				}
				JSONObject sectionOutput = new JSONObject();
				sectionOutput.put(TITLE_STR, sectionTitle);
				sectionOutput.put(FIELDS_STR, fieldsOutputArray);
				sectionOutputArray.put(sectionOutput);
			}
			outputObj.put(SECTIONS_STR, sectionOutputArray);
			String fullVersion = String.valueOf(version);
			if (minorVer > 0) {
				fullVersion += "."+minorVer;
			}
			outputObj.put(VERSION_STR, fullVersion);
		} catch (Exception e) {
			e.printStackTrace();
		}
		//System.out.println(outputObj.toString());
		return outputObj;
	}
	
	public int chkPageVersionSubmited(int studyId) {
		int versionSubmitted = -1;
		 UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
		 versionSubmitted = uiFlxPageDao.chkPageVersionSubmited("er_study", studyId);	    
		 return versionSubmitted;
	}
	private JSONObject createPageFieldJsonMsd(String fldCodeSubtype, StudyIdDao sidDao) {
		if (null == sidDao) return null;
		
		ArrayList alternateIdKey = sidDao.getAlternateIdKey();
		if (null == alternateIdKey) return null;
		int counter = alternateIdKey.indexOf(fldCodeSubtype);
		if (counter < 0) return null;
		
		ArrayList studyIdType = sidDao.getStudyIdType();
		ArrayList id = sidDao.getId();

		ArrayList studyIdTypesDesc = sidDao.getStudyIdTypesDesc();
		ArrayList alternateId = sidDao.getAlternateId();
		ArrayList recordType = sidDao.getRecordType ();
		ArrayList dispTypes = sidDao.getDispType();
		ArrayList dispData = sidDao.getDispData();
		String strStudyIdTypesDesc = (String) studyIdTypesDesc.get(counter);
		String strAlternateId = (String) alternateId.get(counter);
		int intStudyIdType = (Integer) studyIdType.get(counter) ; 
		String disptype=(String) dispTypes.get(counter);
		String dispdata=(String) dispData.get(counter);
		if (disptype==null) disptype = EMPTY_STR;
		disptype=disptype.toLowerCase();
		if (dispdata==null) dispdata = EMPTY_STR;
		if (disptype.equals(Str_dropdown)) {
			if (null != dispdata) {
				dispdata = StringUtil.replace(dispdata, "alternateId", "alternateId"+intStudyIdType);
			}
		}
		if (strAlternateId == null) strAlternateId = EMPTY_STR;
		String strRecordType = (String) recordType.get(counter);
		// System.out.println("fldCodePKey="+fldCodePKey+" strStudyIdTypesDesc="+strStudyIdTypesDesc);
		int cbcount=0;
		int intId = (Integer) id.get(counter);
		JSONObject json = new JSONObject();
		try {
			json.put(LABEL_STR, strStudyIdTypesDesc);
			if (Str_Hidden_input.equals(disptype)) {
				System.out.println("intId="+intId+" studyIdType="+intStudyIdType);
			}
			else {
				json.put(CODE_SUBTYP_STR, fldCodeSubtype);
				
				if (Str_chkbox.equals(disptype) || Str_checkbox.equals(disptype)) {
					if(!EJBUtil.isEmpty(dispdata))
					{
						JSONObject chkConfigJSON = new JSONObject(dispdata);
				    	int chkColCnt = 1;
				    	try {
				    		chkColCnt = StringUtil.stringToNum(""+chkConfigJSON.get("chkColCnt"));
				    	}catch(Exception e1){}
				    	StringBuilder description = new StringBuilder();
				    	String delim = "";
				    	try {
				    		JSONArray checkboxArray = chkConfigJSON.getJSONArray("chkArray");
				    		
							for (int indx=0; indx < checkboxArray.length(); indx++) {
								JSONObject checkboxJSON = (JSONObject)checkboxArray.get(indx);
						        try{
									String data = checkboxJSON.getString("data");
									String display = checkboxJSON.getString("display");
									String checked = EMPTY_STRING;
									if((","+strAlternateId+",").indexOf(","+data+",") > -1) {
										description.append(delim);
										description.append(display);
										delim = ",";
									}
							 	}catch(Exception e) {}
							}
				    	}catch(Exception e1){}
				    	
				    	json.put(DATA_STR, description.toString().trim());
					}
					else
						json.put(DATA_STR, strAlternateId.trim());
				}
				else
				{
					json.put(DATA_STR, strAlternateId.trim());
				}
				return json;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private JSONObject createPageFieldJsonStd(FlxPageFields field, StudyJB studyJB) {
		if (studyJB == null) { return null; }
		try {
			switch(field) {
			case STUDY_STUDYNUMBER:
				return doStudyNumber(field, studyJB);
			case STUDY_STUDYTITLE:
				return doStudyTitle(field, studyJB);
			case STUDY_DISEASESITES:
				return doStudyDiseaseSites(field, studyJB);
			case STUDY_DATAMANAGER:
				return doStudyDataManager(field, studyJB);
			case STUDY_OBJECTIVE:
				return doStudyObjective(field, studyJB);
			case STUDY_STUDYSUMM:
				return doStudySummary(field, studyJB);
			case STUDY_PIAUTHOR:
				return doStudyPiAuthor(field, studyJB);
			case STUDY_PIOTHER:
				return doStudyPiOther(field, studyJB);
			case STUDY_STUDYCONTACT:
				return doStudyStudyContact(field, studyJB);
			case STUDY_PINVCHECK:
				return doStudyCheck(field, studyJB);
			case STUDY_CRTP_REPORTABLE:
				return doStudyCtrpReportable (field, studyJB);
			case STUDY_FDA_REGULATED:
				return doStudyFdaRegulated(field, studyJB);
			case STUDY_NCTNUMBER:
				return doStudyNctNumber(field, studyJB);
			case STUDY_PPURPOSE:
				return doStudyPurpose(field, studyJB);
			case STUDY_TAREA:
				return doStudyTArea(field, studyJB);
			case STUDY_AGENTDEVICE:
				return doStudyAgentDevice(field, studyJB);
			case STUDY_SPECIFICSITES1:
				return doStudySpecificSites1(field, studyJB);
			case STUDY_SPECIFICSITES2:
				return doStudySpecificSites2(field, studyJB);
			case STUDY_SAMPLESIZE:
				return doStudySampleSize(field, studyJB);
			case STUDY_STUDYDURN:
				return doStudyDuration(field, studyJB);
			case STUDY_STUDYDURN_DROP:
				return doStudyDurationDropDown(field, studyJB);
			case STUDY_DIVISION:
				return doStudyDivision(field, studyJB);
			case STUDY_CCSG:
				return doStudyCcsgReportable(field, studyJB);
			case STUDY_ESTIMATEDBEGINDATE:
				return doStudyEstimatedBeginDate(field, studyJB);
			case STUDY_PHASE:
				return doStudyPhase(field, studyJB);
			case STUDY_RESEARCHTYPE:
				return doStudyResearchType(field, studyJB);
			case STUDY_STUDYSCOPE:
				return doStudyScope(field, studyJB);
			case STUDY_STUDYTYPE:
				return doStudyType(field, studyJB);
			case STUDY_LINKEDTO:
				return doStudyLinkedTo(field, studyJB);
			case STUDY_BLINDING:
				return doStudyBlinding(field, studyJB);
			case STUDY_RANDOMIZATION:
				return doStudyRandomization(field, studyJB);
			case STUDY_SPONSOR:
				return doStudySponsor(field, studyJB);
			case STUDY_SPONSOR_IFOTHER:
				return doStudySponsorIfOther(field, studyJB);
			case STUDY_SPONSORID:
				return doStudySponsorId(field, studyJB);
			case STUDY_CONTACT:
				return doStudyContact(field, studyJB);
			case STUDY_KEYWORDS:
				return doStudyKeywords(field, studyJB);
			default:
			}
		} catch(JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private JSONObject doStudyNumber(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		json.put(field.getFlxPageFieldKey(), studyJB.getStudyNumber());
		return json;
	}
	
	private JSONObject doStudyTitle(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		json.put(field.getFlxPageFieldKey(), studyJB.getStudyTitle());
		return json;
	}
	
	private JSONObject doStudyDiseaseSites(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = EMPTY_STR;
		if(studyJB != null && studyJB.getDisSite() != null){
			StringBuffer sb1 = new StringBuffer();
			String[] pkDisease = studyJB.getDisSite().split(",");
			CodeDao cdDao = new CodeDao();
			for (int iX = 0; iX < pkDisease.length; iX++) {
				int pkDis = 0;
				try {
					pkDis = Integer.parseInt(pkDisease[iX]);
					if (pkDis < 1) { continue; }
					sb1.append(cdDao.getCodeDescription(pkDis)).append(";");
				} catch(Exception e) {}
			}
			if (sb1.length() > 0) {
				sb1.deleteCharAt(sb1.length()-1);
			}
			fldValue = sb1.toString();
		}
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudyDataManager(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		int fldPK = 0;
 		try {
 			String authorStr = (studyJB == null || studyJB.getStudyAuthor() == null) ? EMPTY_STR : studyJB.getStudyAuthor();
 			fldPK = StringUtil.stringToNum(authorStr);
 		} catch (Exception e) {
 			e.printStackTrace();
 			fldPK = 0;
 		}
		String fldValue = EMPTY_STRING;
		if(fldPK > 0) {
			UserJB userB = new UserJB();
			userB.setUserId(fldPK);
			userB.getUserDetails();
			fldValue = userB.getUserFirstName() + " " + userB.getUserLastName();
			fldValue = (null == fldValue)? "":fldValue;
		}
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}

	private JSONObject doStudyObjective(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyObjective() == null) ? EMPTY_STRING : studyJB.getStudyObjective();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}

	private JSONObject doStudySummary(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudySummary() == null) ? EMPTY_STRING : studyJB.getStudySummary();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}

	private JSONObject doStudyPiAuthor(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		int fldPK = 0;
		try {
			String primaryInv = (studyJB == null || studyJB.getStudyPrimInv() == null) ? EMPTY_STRING : studyJB.getStudyPrimInv();
			fldPK = StringUtil.stringToNum(primaryInv);
		} catch (Exception e) {
			e.printStackTrace();
			fldPK = 0;
		}
		String fldValue = EMPTY_STRING;
		if (fldPK > 0){
			UserJB userB = new UserJB();
			userB.setUserId(fldPK);
			userB.getUserDetails();
			fldValue =  userB.getUserFirstName() + " " +userB.getUserLastName(); 
			fldValue = (null==fldValue)? "" : fldValue;
		}
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudyPiOther(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = studyJB.getStudyOtherPrinv() == null ? EMPTY_STRING : studyJB.getStudyOtherPrinv();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudyStudyContact(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		int fldPK= 0;
		try {
			String studyContact = (studyJB.getStudyCoordinator() == null) ? EMPTY_STRING : studyJB.getStudyCoordinator();
			fldPK = StringUtil.stringToNum(studyContact);
		} catch (Exception e) {
			e.printStackTrace();
			fldPK = 0;
		}
		String fldValue = EMPTY_STRING;
		if(fldPK > 0){
			UserJB userB = new UserJB();
			userB.setUserId(fldPK);
			userB.getUserDetails();
			fldValue = userB.getUserFirstName() + " " + userB.getUserLastName();
			fldValue = (null==fldValue)?"":fldValue;
		}
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudyCheck(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getMajAuthor() == null) ? EMPTY_STRING : studyJB.getMajAuthor();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}

	private JSONObject doStudyCtrpReportable (FlxPageFields field, 
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyCtrpReportable() == null) ? 
				EMPTY_STRING : studyJB.getStudyCtrpReportable();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudyFdaRegulated(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getFdaRegulatedStudy() == null) ? 
				EMPTY_STRING : studyJB.getFdaRegulatedStudy();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudyNctNumber(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getNctNumber() == null) ? EMPTY_STRING : studyJB.getNctNumber();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	private JSONObject doStudyPurpose(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyPurpose() == null) ? EMPTY_STRING : studyJB.getStudyPurpose();
		CodeDao cdDao = new CodeDao();
		int codeId = 0;
		try { codeId = Integer.parseInt(fldValue); } catch(Exception e) {}
		String codeDesc = cdDao.getCodeDescription(codeId);
		if (codeDesc == null) { codeDesc = EMPTY_STRING; } 
		json.put(field.getFlxPageFieldKey(), codeDesc);
		return json;
	}

	private JSONObject doStudyTArea(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyTArea() == null) ? EMPTY_STRING : studyJB.getStudyTArea();
		CodeDao cdDao = new CodeDao();
		int codeId = 0;
		try { codeId = Integer.parseInt(fldValue); } catch(Exception e) {}
		String codeDesc = cdDao.getCodeDescription(codeId);
		if (codeDesc == null) { codeDesc = EMPTY_STRING; } 
		json.put(field.getFlxPageFieldKey(), codeDesc);
		return json;
	}
	
	private JSONObject doStudyAgentDevice(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyProduct() == null) ? EMPTY_STRING : studyJB.getStudyProduct();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}

	private JSONObject doStudySpecificSites1(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyICDCode1() == null) ? EMPTY_STRING : studyJB.getStudyICDCode1();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}

	private JSONObject doStudySpecificSites2(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyICDCode2() == null) ? EMPTY_STRING : studyJB.getStudyICDCode2();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudySampleSize(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyNSamplSize() == null) ? EMPTY_STRING : studyJB.getStudyNSamplSize();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}

	private JSONObject doStudyDuration(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyDuration() == null) ? EMPTY_STRING : studyJB.getStudyDuration();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudyDurationDropDown(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyDurationUnit() == null) ? EMPTY_STRING : studyJB.getStudyDurationUnit();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudyDivision(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyDivision() == null) ? EMPTY_STRING : studyJB.getStudyDivision();
		CodeDao cdDao = new CodeDao();
		int codeId = 0;
		try { codeId = Integer.parseInt(fldValue); } catch(Exception e) {}
		String codeDesc = cdDao.getCodeDescription(codeId);
		if (codeDesc == null) { codeDesc = EMPTY_STRING; } 
		json.put(field.getFlxPageFieldKey(), codeDesc);
		return json;
	}
	
	private JSONObject doStudyCcsgReportable(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getCcsgReportableStudy() == null) ? EMPTY_STRING : studyJB.getCcsgReportableStudy();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudyEstimatedBeginDate(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyEstBeginDate() == null) ? EMPTY_STRING : studyJB.getStudyEstBeginDate();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudyPhase(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyPhase() == null) ? EMPTY_STRING : studyJB.getStudyPhase();
		CodeDao cdDao = new CodeDao();
		int codeId = 0;
		try { codeId = Integer.parseInt(fldValue); } catch(Exception e) {}
		String codeDesc = cdDao.getCodeDescription(codeId);
		if (codeDesc == null) { codeDesc = EMPTY_STRING; } 
		json.put(field.getFlxPageFieldKey(), codeDesc);
		return json;
	}
	
	private JSONObject doStudyResearchType(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyResType() == null) ? EMPTY_STRING : studyJB.getStudyResType();
		CodeDao cdDao = new CodeDao();
		int codeId = 0;
		try { codeId = Integer.parseInt(fldValue); } catch(Exception e) {}
		String codeDesc = cdDao.getCodeDescription(codeId);
		if (codeDesc == null) { codeDesc = EMPTY_STRING; } 
		json.put(field.getFlxPageFieldKey(), codeDesc);
		return json;
	}
	
	private JSONObject doStudyScope(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyScope() == null) ? EMPTY_STRING : studyJB.getStudyScope();
		CodeDao cdDao = new CodeDao();
		int codeId = 0;
		try { codeId = Integer.parseInt(fldValue); } catch(Exception e) {}
		String codeDesc = cdDao.getCodeDescription(codeId);
		if (codeDesc == null) { codeDesc = EMPTY_STRING; } 
		json.put(field.getFlxPageFieldKey(), codeDesc);
		return json;
	}
	
	private JSONObject doStudyType(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyType() == null) ? EMPTY_STRING : studyJB.getStudyType();
		CodeDao cdDao = new CodeDao();
		int codeId = 0;
		try { codeId = Integer.parseInt(fldValue); } catch(Exception e) {}
		String codeDesc = cdDao.getCodeDescription(codeId);
		if (codeDesc == null) { codeDesc = EMPTY_STRING; } 
		json.put(field.getFlxPageFieldKey(), codeDesc);
		return json;
	}
	
	private JSONObject doStudyLinkedTo(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyAssoc() == null) ? EMPTY_STRING : studyJB.getStudyAssoc();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudyBlinding(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyBlinding() == null) ? EMPTY_STRING : studyJB.getStudyBlinding();
		CodeDao cdDao = new CodeDao();
		int codeId = 0;
		try { codeId = Integer.parseInt(fldValue); } catch(Exception e) {}
		String codeDesc = cdDao.getCodeDescription(codeId);
		if (codeDesc == null) { codeDesc = EMPTY_STRING; } 
		json.put(field.getFlxPageFieldKey(), codeDesc);
		return json;
	}
	
	private JSONObject doStudyRandomization(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyRandom() == null) ? EMPTY_STRING : studyJB.getStudyRandom();
		CodeDao cdDao = new CodeDao();
		int codeId = 0;
		try { codeId = Integer.parseInt(fldValue); } catch(Exception e) {}
		String codeDesc = cdDao.getCodeDescription(codeId);
		if (codeDesc == null) { codeDesc = EMPTY_STRING; } 
		json.put(field.getFlxPageFieldKey(), codeDesc);
		return json;
	}
	
	private JSONObject doStudySponsor(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudySponsorName() == null) ? EMPTY_STRING : studyJB.getStudySponsorName();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;	
	}
	
	private JSONObject doStudySponsorIfOther(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudySponsor() == null) ? EMPTY_STRING : studyJB.getStudySponsor();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudySponsorId(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudySponsorIdInfo() == null) ? EMPTY_STRING : studyJB.getStudySponsorIdInfo();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudyContact(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyContact() == null) ? EMPTY_STRING : studyJB.getStudyContact();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudyKeywords(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyKeywords() == null) ? EMPTY_STRING : studyJB.getStudyKeywords();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
}
