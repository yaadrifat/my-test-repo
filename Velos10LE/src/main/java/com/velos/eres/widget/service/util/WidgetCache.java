package com.velos.eres.widget.service.util;

import java.util.List;

import com.velos.eres.widget.business.common.UIWidgetDao;
import com.velos.eres.widget.business.domain.Widget;

/**
 * @author Isaac Huang
 * Class to cache all the UI Widget data in a singleton.
 *
 */
public class WidgetCache {
	
	private WidgetCache() {}
	
	private static WidgetCache widgetCache = new WidgetCache();
	
	private static UIWidgetDao uiWidgetDao = null;
	
	public synchronized void reloadWidgetInfo() {
		if (uiWidgetDao == null) { uiWidgetDao = new UIWidgetDao(); }
		uiWidgetDao.clearWidgetData();
		uiWidgetDao.getWidgetData();
	}
	
	public static WidgetCache getInstance() {
		if (uiWidgetDao == null) {
			widgetCache.reloadWidgetInfo();
		}
		return widgetCache;
	}
	
	public synchronized List<Widget> getWidgetListByPageCode(String pageCode) {
		Long pageCodeLong = -999999999L;
		try {
			pageCodeLong = Long.parseLong(pageCode);
			return uiWidgetDao.getWidgetMap().get(pageCodeLong);
		} catch (Exception e) {}
		return null;
	}

}
