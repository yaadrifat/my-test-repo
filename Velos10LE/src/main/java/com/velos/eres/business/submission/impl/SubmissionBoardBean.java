package com.velos.eres.business.submission.impl;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @ejbHomeJNDIname ejb.SubmissionBoard
 */

@Entity
@Table(name = "er_submission_board")
@NamedQueries( {
    @NamedQuery(name = "findByFkSubmissionAndBoard",
            query = "SELECT OBJECT(sb) FROM SubmissionBoardBean sb where "+
            " sb.fkSubmission = :fkSubmission and sb.fkReviewBoard = :fkReviewBoard "),
    @NamedQuery(name = "findByPkSubmissionBoard",
            query = "SELECT OBJECT(sb) FROM SubmissionBoardBean sb where "+
            " sb.id = :pkSubmissionBoard "),
    @NamedQuery(name = "findPkSubmissionBoardBySubmissionAndRevBoard",
		    query = "SELECT OBJECT(sb) FROM SubmissionBoardBean sb where "+
		    "  fk_review_board= :fkRevBoard and fk_submission= :submissionId ") 
})
public class SubmissionBoardBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private Integer fkSubmission;
    private Integer fkReviewBoard;
    private String  submissionReviewer;
    private Integer submissionReviewType;
    private Integer creator;
    private Integer lastModifiedBy;
    private Date    lastModifiedDate;
    private String  ipAdd;
    private Integer fkReviewMeeting;


    public SubmissionBoardBean() {}
    
    public SubmissionBoardBean(Integer id, Integer fkSubmission, Integer fkReviewBoard, 
            String submissionReviewer, Integer submissionReviewType,
            Integer creator, Integer lastModifiedBy, String ipAdd,
            Integer fkReviewMeeting) {
        setId(id); setFkSubmission(fkSubmission); setFkReviewBoard(fkReviewBoard);
        setSubmissionReviewer(submissionReviewer);
        setSubmissionReviewType(submissionReviewType); setCreator(creator);
        setLastModifiedBy(lastModifiedBy); setIpAdd(ipAdd);
        setFkReviewMeeting(fkReviewMeeting);
    }
    
    public void setDetails(SubmissionBoardBean anotherBean) {
        setFkSubmission(anotherBean.getFkSubmission());
        setFkReviewBoard(anotherBean.getFkReviewBoard());
        setSubmissionReviewer(anotherBean.getSubmissionReviewer());
        setSubmissionReviewType(anotherBean.getSubmissionReviewType());
        setCreator(anotherBean.getCreator());
        setLastModifiedBy(anotherBean.getLastModifiedBy());
        setIpAdd(anotherBean.getIpAdd());
        setFkReviewMeeting(anotherBean.getFkReviewMeeting());
    }
    
    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_SUBMISSION_BOARD", allocationSize=1)
    @Column(name = "PK_SUBMISSION_BOARD")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "FK_SUBMISSION")
    public Integer getFkSubmission() {
        return fkSubmission;
    }

    public void setFkSubmission(Integer fkSubmission) {
        this.fkSubmission = fkSubmission;
    }

    @Column(name = "FK_REVIEW_BOARD")
    public Integer getFkReviewBoard() {
        return fkReviewBoard;
    }

    public void setFkReviewBoard(Integer fkReviewBoard) {
        this.fkReviewBoard = fkReviewBoard;
    }

    @Column(name = "SUBMISSION_REVIEWER")
    public String getSubmissionReviewer() {
        return submissionReviewer;
    }

    public void setSubmissionReviewer(String submissionReviewer) {
        this.submissionReviewer = submissionReviewer;
    }

    @Column(name = "SUBMISSION_REVIEW_TYPE")
    public Integer getSubmissionReviewType() {
        return submissionReviewType;
    }

    public void setSubmissionReviewType(Integer submissionReviewType) {
        this.submissionReviewType = submissionReviewType;
    }

    @Column(name = "CREATOR")
    public Integer getCreator() {
        return creator;
    }

    public void setCreator(Integer creator) {
        this.creator = creator;
    }

    @Column(name = "LAST_MODIFIED_DATE")
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Column(name = "LAST_MODIFIED_BY")
    public Integer getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(Integer lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    @Column(name = "FK_REVIEW_MEETING")
    public Integer getFkReviewMeeting() {
        return fkReviewMeeting;
    }

    public void setFkReviewMeeting(Integer fkReviewMeeting) {
        this.fkReviewMeeting = fkReviewMeeting;
    }

}
