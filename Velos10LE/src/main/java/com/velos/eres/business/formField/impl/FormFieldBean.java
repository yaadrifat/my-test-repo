/*
 * Classname : FormFieldBean
 * 
 * Version information: 1.0
 *
 * Date: 17/07/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: SoniaKaura
 */

package com.velos.eres.business.formField.impl;

/* Import Statements */

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The FormField CMP entity bean.<br>
 * <br>
 * 
 * @author Sonia Kaura
 * @vesion 1.0 17/07/2003
 * @ejbHome FormFieldHome
 * @ejbRemote FormFieldRObj
 */

@Entity
@Table(name = "er_formfld")
public class FormFieldBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3977579212334839600L;

    public int formFieldId;

    /**
     * The foreign key reference from the Field : FK_FORMSEC
     */
    public Integer formSecId;

    /**
     * The Msg Type, Pop or Notification : FK_FIELD
     */

    public Integer fieldId;

    /**
     * The Sequence number of the Field: FORMFLD_SEQ
     */
    public Integer formFldSeq;

    /**
     * Flag to identify whether field is mandatory: 0 - false , 1 - true
     * :FORMFLD_MANDATORY
     * 
     */

    public Integer formFldMandatory;

    /**
     * Flag to identify whether field will be displayed in filled forms'
     * browser: 0 - false 1 - true :FORMFLD_BROWSERFLG
     * 
     */
    public Integer formFldBrowserFlg;

    /**
     * Stores XSL for the field :FORMFLD_XSL
     * 
     */

    public String formFldXsl;

    /**
     * Stores JavaScript for the field. Initially it will be used for 'Required'
     * attribute :FORMFLD_JAVASCR
     * 
     */

    public String formFldJavaScr;

    /**
     * The Record Type : Record_Type
     */

    public String recordType;

    public Integer creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    public Integer modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    public String ipAdd;

    /***************************************************************************
     * The Date set through the entity bean
     */
    public java.sql.Timestamp lastModifiedDate;

    /**
     * offline Flag
     */
    public Integer offlineFlag;

    // //////////////////////////////////////////////////////////////////////////////////////
    public FormFieldBean() {

    }

    public FormFieldBean(int formFieldId, String formSecId, String fieldId,
            String formFldSeq, String formFldMandatory,
            String formFldBrowserFlg, String formFldXsl, String formFldJavaScr,
            String recordType, String creator, String modifiedBy, String ipAdd,
            String offlineFlag) {
        super();
        // TODO Auto-generated constructor stub
        setFormFieldId(formFieldId);
        setFormSecId(formSecId);
        setFieldId(fieldId);
        setFormFldSeq(formFldSeq);
        setFormFldMandatory(formFldMandatory);
        setFormFldBrowserFlg(formFldBrowserFlg);
        setFormFldXsl(formFldXsl);
        setFormFldJavaScr(formFldJavaScr);
        setRecordType(recordType);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        // setLastModifiedDate(lastModifiedDate);
        setOfflineFlag(offlineFlag);
    }

    /**
     * 
     * 
     * @return formFieldId
     */

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_FORMFLD", allocationSize=1)
    @Column(name = "PK_FORMFLD")
    public int getFormFieldId() {
        return formFieldId;
    }

    /**
     * 
     * 
     * @param formFieldId
     */
    public void setFormFieldId(int formFieldId) {
        this.formFieldId = formFieldId;
    }

    /**
     * 
     * 
     * @return formSecId
     */
    @Column(name = "FK_FORMSEC")
    public String getFormSecId() {
        return StringUtil.integerToString(this.formSecId);

    }

    /**
     * 
     * 
     * @param formSecId
     */
    public void setFormSecId(String formSecId) {
        this.formSecId = StringUtil.stringToInteger(formSecId);
    }

    /**
     * 
     * 
     * @return fieldId
     */
    @Column(name = "FK_FIELD")
    public String getFieldId() {
        return StringUtil.integerToString(this.fieldId);

    }

    /**
     * 
     * 
     * @param fieldId
     */
    public void setFieldId(String fieldId) {

        this.fieldId = StringUtil.stringToInteger(fieldId);

    }

    /**
     * 
     * 
     * @return formFldSeq
     */
    @Column(name = "FORMFLD_SEQ")
    public String getFormFldSeq() {

        return StringUtil.integerToString(this.formFldSeq);

    }

    /**
     * 
     * 
     * @param formFldSeq
     */
    public void setFormFldSeq(String formFldSeq) {

        this.formFldSeq = StringUtil.stringToInteger(formFldSeq);

    }

    /**
     * 
     * 
     * @return formFldMandatory
     */
    @Column(name = "FORMFLD_MANDATORY")
    public String getFormFldMandatory() {

        return StringUtil.integerToString(this.formFldMandatory);

    }

    /**
     * 
     * 
     * @param formFldMandatory
     */
    public void setFormFldMandatory(String formFldMandatory) {

        this.formFldMandatory = StringUtil.stringToInteger(formFldMandatory);

    }

    /**
     * 
     * 
     * @return formFldBrowserFlg
     */
    @Column(name = "FORMFLD_BROWSERFLG")
    public String getFormFldBrowserFlg() {

        return StringUtil.integerToString(this.formFldBrowserFlg);
    }

    /**
     * 
     * 
     * @param formFldBrowserFlg
     */
    public void setFormFldBrowserFlg(String formFldBrowserFlg) {

        this.formFldBrowserFlg = StringUtil.stringToInteger(formFldBrowserFlg);

    }

    /**
     * 
     * 
     * @return formFldXsl
     */
    @Column(name = "FORMFLD_XSL")
    public String getFormFldXsl() {

        return formFldXsl;

    }

    /**
     * 
     * 
     * @param formFldXsl
     */
    public void setFormFldXsl(String formFldXsl) {
        this.formFldXsl = formFldXsl;

    }

    /**
     * 
     * 
     * @return formFldJavaScr
     */
    @Column(name = "FORMFLD_JAVASCR")
    public String getFormFldJavaScr() {

        return formFldJavaScr;
    }

    /**
     * 
     * 
     * @param formFldJavaScr
     */
    public void setFormFldJavaScr(String formFldJavaScr) {

        this.formFldJavaScr = formFldJavaScr;

    }

    /**
     * @return the Record Type : m: modified n:new d: delet
     */

    @Column(name = "RECORD_TYPE")
    public String getRecordType() {
        return this.recordType;
    }

    /**
     * @param set
     *            the Record Type to be modified,new or delete
     */
    public void setRecordType(String recordType) {
        this.recordType = recordType;

    }

    /**
     * @return the Creator
     */
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);

    }

    /**
     * @param takes
     *            a string
     */

    public void setCreator(String creator) {

        this.creator = StringUtil.stringToInteger(creator);

    }

    /**
     * @return the unique ID of the User
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {

        return StringUtil.integerToString(this.modifiedBy);

    }

    /**
     * @param the
     *            set unique ID of the User
     */

    public void setModifiedBy(String modifiedBy) {

        this.modifiedBy = StringUtil.stringToInteger(modifiedBy);

    }

    /**
     * @return the IP Address of the Form Notification request
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param set
     *            the IP Address of the Form Notification request
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /***************************************************************************
     * @param To
     *            change the Last_modified_date for ER_FORMFIELD This is added
     *            to make the trigger on LAST_MODIFIED_DATE to work so that any
     *            change in either the field or the formfield gets reflected.
     * 
     **************************************************************************/
    @Column(name = "LAST_MODIFIED_DATE")
    public java.sql.Timestamp getLastModifiedDate() {
        return this.lastModifiedDate;
    }

    public void setLastModifiedDate(java.sql.Timestamp lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Column(name = "FORMFLD_OFFLINE")
    public String getOfflineFlag() {
        return StringUtil.integerToString(this.offlineFlag);

    }

    /**
     * @param takes
     *            a string
     */

    public void setOfflineFlag(String offlineFlag) {

        this.offlineFlag = StringUtil.stringToInteger(offlineFlag);

    }

    // END OF THE GETTER AND SETTER METHODS

    // ////////////////////////////////////////////////////////////////////

    /*
     * public FormFieldStateKeeper getFormFieldStateKeeper() {
     * Rlog.debug("formfield", "FormFieldyBean.getFormFieldStateKeeper");
     * Rlog.debug("formfield", "FormFieldJB.getOfflineFlag()" +
     * getOfflineFlag()); return new FormFieldStateKeeper(getFormFieldId(),
     * getFormSecId(), getFieldId(), getFormFldSeq(), getFormFldMandatory(),
     * getFormFldBrowserFlg(), getFormFldXsl(), getFormFldJavaScr(),
     * getRecordType(), getCreator(), getModifiedBy(), getIpAdd(),
     * getOfflineFlag()); }
     */

    /**
     * sets up a state keeper containing details of the form field
     */

    /*
     * public void setFormFieldStateKeeper(FormFieldStateKeeper formFldsk) {
     * GenerateId formFldId = null; try { Connection conn = null; conn =
     * getConnection(); Rlog.debug("formfield",
     * "FormFieldBean.setFormFieldStateKeeper() Connection :" + conn);
     * this.formFieldId = formFldId.getId("SEQ_ER_FORMFLD", conn);
     * Rlog.debug("formfield", "FormFieldBean.setFormFieldStateKeeper()
     * formFieldId :" + this.formFieldId); conn.close(); //
     * Rlog.debug("formfield","formFldsk.getFormSecId() :"+ //
     * formFldsk.getFormSecId()); setFormSecId(formFldsk.getFormSecId()); //
     * Rlog.debug("formfield","formFldsk.getFieldId() :"+ //
     * formFldsk.getFieldId()); setFieldId(formFldsk.getFieldId()); //
     * Rlog.debug("formfield","formFldsk.getFormFldSeq() :"+ //
     * formFldsk.getFormFldSeq()); setFormFldSeq(formFldsk.getFormFldSeq()); //
     * Rlog.debug("formfield","formFldsk.getFormFldMandatory() :"+ //
     * formFldsk.getFormFldMandatory());
     * setFormFldMandatory(formFldsk.getFormFldMandatory());
     * 
     * Rlog.debug("formfield", "formFldsk.getFormFldBrowserFlg() $%&:" +
     * formFldsk.getFormFldBrowserFlg());
     * setFormFldBrowserFlg(formFldsk.getFormFldBrowserFlg());
     * 
     * Rlog.debug("formfield", "formFldsk.getFormFldXsl():" +
     * formFldsk.getFormFldXsl()); setFormFldXsl(formFldsk.getFormFldXsl()); //
     * Rlog.debug("formfield","formFldsk.getFormFldJavaScr() :"+ //
     * formFldsk.getFormFldJavaScr());
     * setFormFldJavaScr(formFldsk.getFormFldJavaScr()); //
     * Rlog.debug("formfield","formFldsk.getRecordType() :"+ //
     * formFldsk.getRecordType()); setRecordType(formFldsk.getRecordType()); //
     * Rlog.debug("formfield","formFldsk.getCreator() :"+ //
     * formFldsk.getCreator()); setCreator(formFldsk.getCreator()); //
     * Rlog.debug("formfield","formFldsk.getModifiedBy() :"+ //
     * formFldsk.getModifiedBy()); // setModifiedBy(formFldsk.getModifiedBy()); //
     * Rlog.debug("formfield","formFldsk.getIpAdd() :"+ //
     * formFldsk.getIpAdd()); setIpAdd(formFldsk.getIpAdd());
     * 
     * setOfflineFlag(formFldsk.getOfflineFlag()); } catch (Exception e) {
     * Rlog.fatal("formfield", "Error in setFormFieldStateKeeper() in
     * FormFieldBean " + e); } }
     */

    /**
     * updates the Form Field Record
     * 
     * @param formFldsk
     * @return int 0: in case of successful updation; -2 in caseof exception
     */
    public int updateFormField(FormFieldBean formFldsk) {
        try {

            setFormFieldId(formFldsk.getFormFieldId());
            setFormSecId(formFldsk.getFormSecId());
            setFieldId(formFldsk.getFieldId());
            setFormFldSeq(formFldsk.getFormFldSeq());
            setFormFldMandatory(formFldsk.getFormFldMandatory());
            Rlog.debug("formfield",
                    "In updateFormField() of FormFieldBean formFldsk.getFormFldBrowserFlg() "
                            + formFldsk.getFormFldBrowserFlg());
            setFormFldBrowserFlg(formFldsk.getFormFldBrowserFlg());
            setFormFldXsl(formFldsk.getFormFldXsl());
            setFormFldJavaScr(formFldsk.getFormFldJavaScr());
            setRecordType(formFldsk.getRecordType());
            // setCreator(formFldsk.getCreator());
            Rlog.debug("formfield",
                    "In updateFormField() of FormFieldBean formFldsk.getFormFldBrowserFlg() "
                            + formFldsk.getModifiedBy());
            setModifiedBy(formFldsk.getModifiedBy());
            Rlog.debug("formfield",
                    "In updateFormField() of FormFieldBean formFldsk.getFormFldBrowserFlg() "
                            + formFldsk.getModifiedBy());
            setIpAdd(formFldsk.getIpAdd());
            setOfflineFlag(formFldsk.getOfflineFlag());

            setCreator(formFldsk.getCreator());	
            // To change the last_modidied_date for er_formfield
            java.util.Date sysdate = (Calendar.getInstance()).getTime();
            java.sql.Timestamp t = new java.sql.Timestamp(sysdate.getTime());
            setLastModifiedDate(t);

            return 0;
        } catch (Exception e) {
            Rlog.fatal("formfield", " error in FormFieldBean.updateFormField");
            return -2;
        }
    }

}
