/**
 * Classname			StdSiteRightsBean
 * 
 * Version information	1.0
 *
 * Date				11/17/2003
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.stdSiteRights.impl;

/* Import Statements */
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The StdSiteRights CMP entity bean.<br>
 * <br>
 * 
 * @author Anu Khanna
 * @version 1.0
 */
@Entity
@Table(name = "ER_STUDY_SITE_RIGHTS")
public class StdSiteRightsBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * the study site rights Id
     */

    public int stdSiteRightsId;

    /**
     * the site Id
     */
    public Integer siteId;

    /**
     * the study Id
     */
    public Integer studyId;

    /**
     * the user Id
     */

    public Integer userId;

    /**
     * the study site right
     */

    public Integer studySiteRights;

    /**
     * the record creator
     */
    public Integer creator;

    /**
     * last modified by
     */
    public Integer modifiedBy;

    /**
     * the IP Address
     */
    public String ipAdd;

    // setter getter methods over here

    /**
     * Returns the Study Site Rights Id
     * 
     * @return stdSiteRightsId
     */
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_STUDYSITE_RIGHTS", allocationSize=1)
    @Column(name = "PK_STUDY_SITE_RIGHTS")
    public int getStdSiteRightsId() {
        return this.stdSiteRightsId;
    }

    public void setStdSiteRightsId(int stdRightId) {
        this.stdSiteRightsId = stdRightId;
    }

    /**
     * Returns the Site id
     * 
     * @return String
     */
    @Column(name = "FK_SITE")
    public String getSiteId() {
        return StringUtil.integerToString(this.siteId);
    }

    /**
     * Sets the Site Id
     * 
     * @param siteId
     */
    public void setSiteId(String siteId) {
        this.siteId = StringUtil.stringToInteger(siteId);
    }

    /**
     * Returns the Study id
     * 
     * @return String
     */
    @Column(name = "FK_STUDY")
    public String getStudyId() {
        return StringUtil.integerToString(this.studyId);
    }

    /**
     * Sets the Study Id
     * 
     * @param studyId
     */
    public void setStudyId(String studyId) {
        this.studyId = StringUtil.stringToInteger(studyId);
    }

    /**
     * Returns the user id
     * 
     * @return String
     */
    @Column(name = "FK_USER")
    public String getUserId() {
        return StringUtil.integerToString(this.userId);
    }

    /**
     * Sets the user id
     * 
     * @param userId
     *            user Id
     */
    public void setUserId(String userId) {
        this.userId = StringUtil.stringToInteger(userId);
    }

    /**
     * Returns the study site rights
     * 
     * @return String studySiteRights
     */
    @Column(name = "USER_STUDY_SITE_RIGHTS")
    public String getStudySiteRights() {
        return StringUtil.integerToString(this.studySiteRights);
    }

    /**
     * Sets the rights in the user site
     * 
     * @param studySiteRights
     */
    public void setStudySiteRights(String studySiteRights) {
        this.studySiteRights = StringUtil.stringToInteger(studySiteRights);
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Returns the state holder object associated with this study site rights
     * 
     * @return StdSiteRightsStateKeeper
     */
    /*
     * public StdSiteRightsStateKeeper getStdSiteRightsStateKeeper() {
     * Rlog.debug("stdSiteRights", "GET STUDY SITE RIGHTS KEEPER"); return (new
     * StdSiteRightsStateKeeper(getStdSiteRightsId(), getSiteId(), getStudyId(),
     * getUserId(), getStudySiteRights(), getCreator(), getModifiedBy(),
     * getIpAdd())); }
     */

    /**
     * sets up a state holder containing details of the study site rights
     * 
     * @param ssk
     *            StdSiteRightsStateKeeper
     */

    /*
     * public void setStdSiteRightsStateKeeper(StdSiteRightsStateKeeper ssk) {
     * Rlog.debug("stdSiteRights", "In
     * stdSiteRightsBean.setStdSiteRightsStateKeeper"); GenerateId genId = null;
     * try { Connection conn = null; conn = getConnection();
     * Rlog.debug("stdSiteRights", "Connection :" + conn); stdSiteRightsId =
     * genId.getId("SEQ_ER_STUDYSITE_RIGHTS", conn); conn.close();
     * 
     * setSiteId(ssk.getSiteId()); setStudyId(ssk.getStudyId());
     * setUserId(ssk.getUserId()); setStudySiteRights(ssk.getStudySiteRights());
     * setCreator(ssk.getCreator()); setModifiedBy(ssk.getModifiedBy());
     * setIpAdd(ssk.getIpAdd()); Rlog.debug("stdSiteRights", "saving the data in
     * the std Site rights bean"); } catch (Exception e) {
     * Rlog.fatal("stdSiteRights", "EXCEPTION
     * StdSiteRightsBean.setStdSiteRightsStateKeeper" + e); } }
     */

    /**
     * Update the Study Site Rights State Keeper in User Site and returs 0 if
     * success else -2
     * 
     * @param ssk
     *            StdSiteRightsStateKeeper
     * @return 0 or -2 depending on the updation
     */
    public int updateStdSiteRights(StdSiteRightsBean ssk) {
        try {
            setSiteId(ssk.getSiteId());
            setStudyId(ssk.getStudyId());
            setUserId(ssk.getUserId());
            setStudySiteRights(ssk.getStudySiteRights());
            setModifiedBy(ssk.getModifiedBy());
            setIpAdd(ssk.getIpAdd());
            setCreator(ssk.getCreator());
            Rlog
                    .debug("stdSiteRights",
                            "StdSiteRightsBean.updateStdSiteRights");
            return 0;
        } catch (Exception e) {
            Rlog.fatal("stdSiteRights",
                    " error in StdSiteRightsBean.updateStdSiteRights");
            return -2;
        }
    }

    public StdSiteRightsBean() {

    }

    public StdSiteRightsBean(int stdSiteRightsId, String siteId,
            String studyId, String userId, String studySiteRights,
            String creator, String modifiedBy, String ipAdd) {
        // TODO Auto-generated constructor stub
        setStdSiteRightsId(stdSiteRightsId);
        setSiteId(siteId);
        setStudyId(studyId);
        setUserId(userId);
        setStudySiteRights(studySiteRights);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

}// end of class
