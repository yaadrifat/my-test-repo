/*
 * Classname			CatLib.class
 * 
 * Version information 	1.0
 *
 * Date					04/07/2003
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * CatLibDao for getting CatLib records
 * 
 * @author Sonia Kaura
 * @version : 1.0 05/07/2003
 */

public class CatLibDao extends CommonDAO implements java.io.Serializable {

    private ArrayList catLibIds; // this is the primary key of the Catgory

    // Library/FormTypes Table

    private ArrayList catLibNames;

    private int rows; // to get the number of rows of the incoming result set

    public CatLibDao() {
        catLibIds = new ArrayList();
        catLibNames = new ArrayList();

    }

    // THE GET AND SEND METHODS

    /**
     * @return catLibId
     */
    public ArrayList getCatLibIds() {
        return catLibIds;
    }

    /**
     * @return
     */
    public ArrayList getCatLibNames() {
        return catLibNames;
    }

    /**
     * @param list
     */

    public void setCatLibId(ArrayList catLibIds) {
        this.catLibIds = catLibIds;
    }

    /**
     * @param list
     */
    public void setCatLibNames(ArrayList list) {
        catLibNames = list;
    }

    /**
     * @return rows
     */
    public int getRows() {
        return this.rows;
    }

    /**
     * @param rows
     */
    public void setRows(int rows) {
        this.rows = rows;
    }

    // // // // // /
    // ////////////////////////////////////////////////////////////////////

    public void setCatLibId(Integer catLibId) {
        this.catLibIds.add(catLibId);
    }

    public void setCatLibName(String catLibName) {
        this.catLibNames.add(StringUtil.stripScript(catLibName));
    }

    // END OF SETTERS AND GETTERS

    /**
     * This method returns the list of all the categoy names/formtypes and ids
     * for a particular Account ID
     * 
     * @param the
     *            accountId
     * @param the
     *            string type Field Category(C) or Form Type ( F)
     * @returns
     * 
     * 
     */
    //km- Query modified to sort the options for 'By Form Type' field in ascending order.
    public void getAllCategories(int accountId, String type) {

        int rows = 0;

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select  PK_CATLIB,CATLIB_NAME "
                    + " from ER_CATLIB "
                    + " where FK_ACCOUNT = ? AND CATLIB_TYPE = ? "
                    + " AND RECORD_TYPE <> 'D'  ORDER BY LOWER(CATLIB_NAME) ");
            	//+ " AND RECORD_TYPE <> 'D'  ORDER BY PK_CATLIB ");
            		
            pstmt.setInt(1, accountId);
            pstmt.setString(2, type);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setCatLibId(new Integer(rs.getInt("PK_CATLIB")));
                setCatLibName(rs.getString("CATLIB_NAME"));
                rows++;
                Rlog.debug("catlib", "CatLibDao.getAllCategories rows " + rows);
            }

            setRows(rows);

        } catch (SQLException ex) {
            Rlog.fatal("catlib",
                    "CatLibDao.getAllCategories EXCEPTION IN FETCHING FROM ER_CATLIB TABLE"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * This method returns the list of all the categoy names/form type and ids
     * for a particular Account ID and also for the drop down of the Categories
     * it shows the option of All
     * 
     * @param the
     *            accountId
     * @param the
     *            string type Field Category(C) or Form Type ( F)
     * 
     * 
     * 
     */
	
    //km-Query modified to sort the options for 'By Form Type' field in ascending order.
    public void getCategoriesWithAllOption(int accountId, String type,
            int forLastAll) {

        int rows = 0;

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select  PK_CATLIB,CATLIB_NAME "
                    + " from ER_CATLIB "
                    + " where FK_ACCOUNT = ? AND CATLIB_TYPE = ? "
                    + " AND RECORD_TYPE <> 'D'  ORDER BY LOWER(CATLIB_NAME)");
            	//+ " AND RECORD_TYPE <> 'D'  ORDER BY PK_CATLIB");
					
            pstmt.setInt(1, accountId);
            pstmt.setString(2, type);

            ResultSet rs = pstmt.executeQuery();

            // For setting in the returning ArrayList the option ALL
            if (forLastAll == 0) {
                setCatLibId(new Integer(-1));
                setCatLibName(LC.L_All_Upper);
                rows++;
            }

            while (rs.next()) {
                setCatLibId(new Integer(rs.getInt("PK_CATLIB")));
                setCatLibName(rs.getString("CATLIB_NAME"));
                rows++;
                Rlog.debug("catlib", "CatLibDao.getAllCategories rows " + rows);
            }

            if (forLastAll == 1) {

                setCatLibId(new Integer(-1));
                setCatLibName(LC.L_All_Upper);
            }

            setRows(rows);

        } catch (SQLException ex) {
            Rlog.fatal("catlib",
                    "CatLibDao.getAllCategories EXCEPTION IN FETCHING FROM ER_CATLIB TABLE"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

}
