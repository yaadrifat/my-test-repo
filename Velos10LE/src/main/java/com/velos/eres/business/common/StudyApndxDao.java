package com.velos.eres.business.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import oracle.sql.BLOB;
import oracle.jdbc.driver.OracleResultSet;

import com.velos.eres.service.util.Rlog;

public class StudyApndxDao extends CommonDAO implements java.io.Serializable {

	private static final long serialVersionUID = 1794672892034414268L;

	private static final String COMMA_STR = ",";
	
	private static final String GetUriByPkSql = "select STUDYAPNDX_URI from ER_STUDYAPNDX where PK_STUDYAPNDX = ? ";
	
	private static final String GetStudyApndxPksForVrsnSql = "SELECT sa.PK_STUDYAPNDX"
			+ " FROM ER_STUDYVER a ,"
			+ " (SELECT CODELST_DESC, CODELST_SUBTYP, PK_STATUS, STATUS_MODPK, FK_CODELST_STAT"
			+ " FROM er_studyver i, ER_STATUS_HISTORY, er_codelst c"
			+ " WHERE i.fk_study     = ?  AND i.PK_STUDYVER    = STATUS_MODPK  AND STATUS_MODTABLE  = 'er_studyver'"
			+ " AND STATUS_END_DATE IS NULL  AND c.pk_codelst     = FK_CODELST_STAT  ) stat,  ER_STUDYAPNDX sa"
			+ " WHERE a.FK_STUDY       = ? AND STATUS_MODPK (+) = pk_studyver AND a.PK_STUDYVER = sa.FK_STUDYVER"
			+ " AND a.studyver_number = to_number(?)";
	
	private static final String GetStudyApndxPksForAllVrsnSql = "SELECT sa.PK_STUDYAPNDX"
			+ " FROM ER_STUDYVER a ,"
			+ " (SELECT CODELST_DESC, CODELST_SUBTYP, PK_STATUS, STATUS_MODPK, FK_CODELST_STAT"
			+ " FROM er_studyver i, ER_STATUS_HISTORY, er_codelst c"
			+ " WHERE i.fk_study     = ?  AND i.PK_STUDYVER    = STATUS_MODPK  AND STATUS_MODTABLE  = 'er_studyver'"
			+ " AND STATUS_END_DATE IS NULL  AND c.pk_codelst     = FK_CODELST_STAT  ) stat,  ER_STUDYAPNDX sa"
			+ " WHERE a.FK_STUDY       = ? AND STATUS_MODPK (+) = pk_studyver AND a.PK_STUDYVER = sa.FK_STUDYVER";
	
	private static final String GetStudyApndxMaxVrsnSql = "SELECT MAX(TO_NUMBER(STUDYVER_NUMBER)) STUDYVER_NUMBER"
			+ " FROM ER_STUDYVER a , (SELECT CODELST_DESC, CODELST_SUBTYP, PK_STATUS, STATUS_MODPK, FK_CODELST_STAT"
			+ " FROM er_studyver i, ER_STATUS_HISTORY, er_codelst c WHERE i.fk_study     = ? AND i.PK_STUDYVER    = STATUS_MODPK"
			+ " AND STATUS_MODTABLE  = 'er_studyver' AND STATUS_END_DATE IS NULL AND c.pk_codelst     = FK_CODELST_STAT ) stat,  ER_STUDYAPNDX sa"
			+ " WHERE a.FK_STUDY       = ? AND STATUS_MODPK (+) = pk_studyver AND a.PK_STUDYVER = sa.FK_STUDYVER";
;
	private static final String checkNumericVrsnsSql = "SELECT COUNT(*) FROM"
			+ " (SELECT REGEXP_SUBSTR(STUDYVER_NUMBER,'^\\d*\\.{0,1}\\d+$') STUDYVER_NUMBER"
			+ " FROM ER_STUDYVER a , (SELECT CODELST_DESC, CODELST_SUBTYP, PK_STATUS, STATUS_MODPK, FK_CODELST_STAT"
			+ " FROM er_studyver i, ER_STATUS_HISTORY, er_codelst c WHERE i.fk_study     = ?"
			+ " AND i.PK_STUDYVER    = STATUS_MODPK AND STATUS_MODTABLE  = 'er_studyver' AND STATUS_END_DATE IS NULL"
			+ " AND c.pk_codelst     = FK_CODELST_STAT ) stat WHERE a.FK_STUDY     = ?"
			+ " AND STATUS_MODPK (+) = pk_studyver ) STUDYVER WHERE STUDYVER_NUMBER IS NULL" ;
	
	private static final String GetStudyApndxDetails = "select "
			+ " a.pk_studyapndx, v.studyver_number, c.codelst_subtyp, a.studyapndx_uri, a.studyapndx_desc from  "
			+ " er_studyapndx a, er_studyver v, er_codelst c where c.pk_codelst = v.studyver_category and "
			+ " a.fk_studyver = v.pk_studyver and a.pk_studyapndx in (%s) ";
	
	private String studyApndxUri = null;
	private ArrayList<Integer> studyApndxPkList = new ArrayList<Integer>();
	private ArrayList<String> studyApndxVersionList = new ArrayList<String>();
	private ArrayList<String> studyApndxCategorySubtypeList = new ArrayList<String>();
	private ArrayList<String> studyApndxFilenameList = new ArrayList<String>();
	private ArrayList<String> studyApndxDescList = new ArrayList<String>();
	
	public String getStudyApndxUri() { return studyApndxUri; }
	public ArrayList<Integer> getStudyApndxPkList() { return studyApndxPkList; }
	public ArrayList<String> getStudyApndxVersionList() { return studyApndxVersionList; }
	public ArrayList<String> getStudyApndxCategorySubtypeList() { return studyApndxCategorySubtypeList; }
	public ArrayList<String> getStudyApndxFilenameList() { return studyApndxFilenameList; }
	public ArrayList<String> getStudyApndxDescList() { return studyApndxDescList; }
	
	public void getStudyApndxDetails(List<Integer> pkList) {
		if (pkList == null || pkList.size() < 1) {
			return;
		}
		StringBuffer pkListSB = new StringBuffer();
		for (Integer aPk : pkList) {
			pkListSB.append(aPk).append(COMMA_STR);
		}
		if (pkListSB.length() > 1) {
			pkListSB.deleteCharAt(pkListSB.length()-1);
		}
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(String.format(GetStudyApndxDetails, pkListSB.toString()));
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	studyApndxPkList.add(rs.getInt(1));
            	studyApndxVersionList.add(rs.getString(2));
            	studyApndxCategorySubtypeList.add(rs.getString(3));
            	studyApndxFilenameList.add(rs.getString(4));
            	studyApndxDescList.add(rs.getString(5));
            }
        } catch(Exception e) {
        	Rlog.fatal("objectMap", "Exception in StudyApndxDao.getStudyApndxDetails:"+e);
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
	}
	
	public void getUriByPk(int pk) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(GetUriByPkSql);
            pstmt.setInt(1, pk);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	studyApndxUri = rs.getString(1);
            }
        } catch(Exception e) {
        	Rlog.fatal("objectMap", "Exception in StudyApndxDao.getUriByPk:"+e);
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
	}
	
	public List<Integer> getStudyApndxPks(int studyPK, String version) {
		List<Integer> pk_List = new ArrayList<Integer>();
		PreparedStatement pstmt = null;
		Connection connCheckNumeric = null;
		Connection connAllVersions = null;
		Connection connMaxVersion = null;
		Connection connVersion = null;
		ResultSet rs = null;
		int nonNumericVersion = 0;
		try {
			connCheckNumeric = getConnection();

			pstmt = connCheckNumeric.prepareStatement(checkNumericVrsnsSql);
			pstmt.setInt(1, studyPK);
			pstmt.setInt(2, studyPK);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				nonNumericVersion = rs.getInt(1);
			}

			if (nonNumericVersion > 0 || version == null || "".equals(version)) {
				connAllVersions = getConnection();
				pstmt = connAllVersions
						.prepareStatement(GetStudyApndxPksForAllVrsnSql);
				pstmt.setInt(1, studyPK);
				pstmt.setInt(2, studyPK);
				rs = pstmt.executeQuery();
				while (rs.next()) {
					pk_List.add(rs.getInt(1));
				}
			} else {
				connVersion = getConnection();
				pstmt = connVersion
						.prepareStatement(GetStudyApndxPksForVrsnSql);
				pstmt.setInt(1, studyPK);
				pstmt.setInt(2, studyPK);
				pstmt.setString(3, version);
				rs = pstmt.executeQuery();
				while (rs.next()) {
					pk_List.add(rs.getInt(1));
				}
			}
		} catch (Exception e) {
			Rlog.fatal("objectMap",
					"Exception in StudyApndxDao.getStudyApndxPks:" + e);
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (Exception e) {
			}
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (connCheckNumeric != null)
					connCheckNumeric.close();
			} catch (Exception e) {
			}
			try {
				if (connAllVersions != null)
					connAllVersions.close();
			} catch (Exception e) {
			}
			try {
				if (connVersion != null)
					connVersion.close();
			} catch (Exception e) {
			}
			try {
				if (connMaxVersion != null)
					connMaxVersion.close();
			} catch (Exception e) {
			}
		}
		return pk_List;
	}
	
	public ArrayList<Integer> getPkByStudyIdAndFkStudyVer(int studyId, int studyVerNum) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        int studyApndxPk=0;
        
        ArrayList<Integer> arList = new ArrayList<Integer>();
        try {
            conn = getConnection();
            String sql = "select pk_studyapndx from er_studyapndx where fk_study=? and fk_studyver=?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, studyId);
            pstmt.setInt(2, studyVerNum);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	studyApndxPk = rs.getInt("pk_studyapndx");
            	arList.add(new Integer(studyApndxPk));
            }
        } catch(Exception e) {
        	Rlog.fatal("objectMap", "Exception in StudyApndxDao.getPkByStudyIdAndFkStudyVer:"+e);
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
        return arList;
	}
	
	public BLOB getStudyApndixFileObject(int studyId, int pkStudyApndx) {	       
        PreparedStatement pstmt = null;
        Connection conn = null;
        BLOB fileObj=null;
        try {
            conn = getConnection();            
            pstmt = conn.prepareStatement("select studyapndx_fileobj from er_studyapndx where fk_study=? and pk_studyapndx=?");
           
            pstmt.setInt(1, studyId);
            pstmt.setInt(2, pkStudyApndx);
            ResultSet rs = pstmt.executeQuery();           
            while (rs.next()) {
               fileObj = (BLOB)rs.getObject(1);   
            }           
        } catch (SQLException ex) {
            Rlog.fatal("Appendix", "AppendixDao.getStudyApndixFileObject EXCEPTION IN FETCHING FROM Appendix table"  + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return fileObj;
    }
    
  
	public void updateStudyApndixFileObject(int studyId, int newpk , String path, BLOB blobObj) {
		PreparedStatement pstmt = null;
        Connection conn = null;     
      
    	try {
			File file = new File(path);         
	        InputStream istr = new FileInputStream(file);	       
	        conn = getConnection(); 
	        pstmt = conn.prepareStatement("Update er_studyapndx set studyapndx_fileobj = empty_blob() where pk_studyapndx = " + newpk);
	        pstmt.execute();
	        pstmt.close();
	        // get the empty blob from the row saved above
	        Statement b_stmt = conn.createStatement();	  
	        String sqlStr = "SELECT studyapndx_fileobj from er_studyapndx where pk_studyapndx = " + newpk + " FOR UPDATE";
	         
	        ResultSet sBlob = b_stmt.executeQuery(sqlStr);
	        	        
	        if (sBlob.next()) {
	            // Get the BLOB locator and open output stream for the BLOB
	            BLOB filBLOB = ((OracleResultSet) sBlob).getBLOB(1);
	            @SuppressWarnings("deprecation")
				OutputStream l_blobOutputStream = filBLOB
	                    .getBinaryOutputStream();
	            byte[] l_buffer = new byte[10 * 1024];
	            int l_nread = 0; // Number of bytes read
	            while ((l_nread = istr.read(l_buffer)) != -1)
	                // Read from file
	                l_blobOutputStream.write(l_buffer, 0, l_nread); // Write to
	            // BLOB
	            // Close both streams
	            istr.close();
	            l_blobOutputStream.close();	            
	        }  
	        conn.commit();	        
	        sBlob.close();
	        b_stmt.close();	       
	        // delete the temporary file that was created on the server
	        file.delete();
    	} catch (Exception ex) {
            Rlog.fatal("Appendix", "AppendixDao.updateStudyApndixFileObject EXCEPTION : " + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }		
	} 
	
	public HashMap<String, Integer> getStudyApndxIdForFileBlob(String version , String prevVersion, int studyId) {		
		HashMap<String, Integer> hMap = new HashMap<String, Integer>();
    	PreparedStatement pstmt = null;
		PreparedStatement pstmt1 = null;
        Connection conn = null;
        ResultSet rs = null;
        ResultSet rs1 = null;
        int studyApndxPk=0;
        int studyApndxPk1=0;
        String filePath="";
               
        try {
            conn = getConnection();
            String sql = "select pk_studyapndx , studyapndx_file from er_studyver es , er_studyapndx esa where es.pk_studyver = esa.fk_studyver  and es.fk_study =? and studyver_number=?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, studyId);
            pstmt.setString(2, prevVersion);
            rs = pstmt.executeQuery();           
            while (rs.next()) {
            	studyApndxPk = rs.getInt("pk_studyapndx");
            	filePath = rs.getString("studyapndx_file");
            	String sql1 = "select pk_studyapndx from er_studyver es , er_studyapndx esa where  es.pk_studyver=esa.fk_studyver and es.fk_study=? and esa.studyapndx_file=? and es.studyver_number=?";
                pstmt1 = conn.prepareStatement(sql1);
                pstmt1.setInt(1, studyId);
                pstmt1.setString(2, filePath);
                pstmt1.setString(3, version);
                rs1 = pstmt1.executeQuery();
            	
                while (rs1.next()) {
                	studyApndxPk1 = rs1.getInt("pk_studyapndx");
                }
            	hMap.put(new Integer(studyApndxPk).toString() + "*" + filePath , new Integer(studyApndxPk1) );
            }           
        } catch(Exception e) {
        	Rlog.fatal("objectMap", "Exception in StudyApndxDao.getStudyApndxIdForFileBlob:"+e);
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}
        	try { if (rs1 != null) rs1.close(); } catch (Exception e) {}
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
        	try { if (pstmt1 != null) pstmt1.close(); } catch (Exception e) {}
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }       
		return hMap;		
	}
	
	public ArrayList<String> getStudyVerNumber(int studyId) {		
	 	PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        String studyverNumber="";	        
        Double sVerNum=0.0;	        
        ArrayList<String> arList = new ArrayList<String>();
        try {
            conn = getConnection();
            String sql = "select distinct studyver_number from er_studyver  where fk_study=? order by studyver_number desc";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, studyId);	           
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	studyverNumber = rs.getString("studyver_number");
            	//sVerNum = Double.valueOf(studyverNumber);
            	//arList.add(sVerNum);
            	arList.add(studyverNumber);
            }
        } catch(Exception e) {
        	Rlog.fatal("objectMap", "Exception in StudyApndxDao.getStudyVerNumber:"+e);
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
        return arList;		
	} 

}
