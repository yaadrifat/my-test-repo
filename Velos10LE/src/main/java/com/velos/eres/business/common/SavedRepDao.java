/*
 * Classname			SavedRepDao.class
 * 
 * Version information 	1.0
 *
 * Date					06/03/2002				
 * 
 * Copyright notice		Velos, Inc.
 * by Arvind Kumar		
 */

package com.velos.eres.business.common;

import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import oracle.jdbc.driver.OracleResultSet;
import oracle.sql.CLOB;

import com.velos.eres.service.util.Rlog;

/**
 * SavedRepDao for getting Report records for a study
 * 
 * @author Arvind
 * @version : 1.0 06/03/2002
 */

public class SavedRepDao extends CommonDAO implements java.io.Serializable {
    private ArrayList savedRepIds;

    private ArrayList savedRepStudyIds;

    private ArrayList savedRepReportIds;

    private ArrayList savedRepRepNames;

    private ArrayList savedRepAsOfDates;

    private ArrayList savedRepUserIds;

    private ArrayList savedRepUserNames;

    private ArrayList savedRepPatIds;

    private ArrayList savedRepPerCodes;

    private ArrayList savedRepFilters;

    private ArrayList creators;

    private ArrayList ipAdds;

    private int cRows;

    public SavedRepDao() {
        savedRepIds = new ArrayList();
        savedRepStudyIds = new ArrayList();
        savedRepReportIds = new ArrayList();
        savedRepRepNames = new ArrayList();
        savedRepAsOfDates = new ArrayList();
        savedRepUserIds = new ArrayList();
        savedRepPatIds = new ArrayList();
        savedRepFilters = new ArrayList();
        creators = new ArrayList();
        ipAdds = new ArrayList();

    }

    // Getter and Setter methods

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public ArrayList getSavedRepIds() {
        return this.savedRepIds;
    }

    public void setSavedRepIds(ArrayList savedRepIds) {
        this.savedRepIds = savedRepIds;
    }

    public ArrayList getSavedRepStudyIds() {
        return this.savedRepStudyIds;
    }

    public void setSavedRepStudyIds(ArrayList savedRepStudyIds) {
        this.savedRepStudyIds = savedRepStudyIds;
    }

    public ArrayList getSavedRepReportIds() {
        return this.savedRepReportIds;
    }

    public void setSavedRepReportIds(ArrayList savedRepReportIds) {
        this.savedRepReportIds = savedRepReportIds;
    }

    public ArrayList getSavedRepRepNames() {
        return this.savedRepRepNames;
    }

    public void setSavedRepRepNames(ArrayList savedRepRepNames) {
        this.savedRepRepNames = savedRepRepNames;
    }

    public ArrayList getSavedRepAsOfDates() {
        return this.savedRepAsOfDates;
    }

    public void setSavedRepAsOfDates(ArrayList savedRepAsOfDates) {
        this.savedRepAsOfDates = savedRepAsOfDates;
    }

    public ArrayList getSavedRepUserIds() {
        return this.savedRepUserIds;
    }

    public void setSavedRepUserIds(ArrayList savedRepUserIds) {
        this.savedRepUserIds = savedRepUserIds;
    }

    public ArrayList getSavedRepUserNames() {
        return this.savedRepUserNames;
    }

    public void setSavedRepUserNames(ArrayList savedRepUserNames) {
        this.savedRepUserNames = savedRepUserNames;
    }

    public ArrayList getSavedRepPatIds() {
        return this.savedRepPatIds;
    }

    public void setSavedRepPatIds(ArrayList savedRepPatIds) {
        this.savedRepPatIds = savedRepPatIds;
    }

    public ArrayList getSavedRepPerCodes() {
        return this.savedRepPerCodes;
    }

    public void setSavedRepPerCodes(ArrayList savedRepPerCodes) {
        this.savedRepPerCodes = savedRepPerCodes;
    }

    public ArrayList getSavedRepFilters() {
        return this.savedRepFilters;
    }

    public void setSavedRepFilters(ArrayList savedRepFilters) {
        this.savedRepFilters = savedRepFilters;
    }

    public ArrayList getCreators() {
        return this.creators;
    }

    public void setCreators(ArrayList creators) {
        this.creators = creators;
    }

    public ArrayList getIpAdds() {
        return this.ipAdds;
    }

    public void setIpAdds(ArrayList ipAdds) {
        this.ipAdds = ipAdds;
    }

    //

    public void setSavedRepIds(Integer repId) {
        savedRepIds.add(repId);
    }

    public void setSavedRepStudyIds(Integer studyId) {
        savedRepStudyIds.add(studyId);
    }

    public void setSavedRepReportIds(Integer reportId) {
        savedRepReportIds.add(reportId);
    }

    public void setSavedRepRepNames(String repName) {
        savedRepRepNames.add(repName);
    }

    public void setSavedRepFilters(String savedRepFilter) {
        savedRepFilters.add(savedRepFilter);
    }

    public void setSavedRepAsOfDates(String asOfDate) {
        savedRepAsOfDates.add(asOfDate);
    }

    public void setSavedRepUserNames(String savedRepUserName) {
        savedRepUserNames.add(savedRepUserName);
    }

    public void setSavedRepUserIds(Integer usrId) {
        savedRepUserIds.add(usrId);
    }

    public void setSavedRepPatIds(Integer patId) {
        savedRepPatIds.add(patId);
    }

    public void setSavedRepPerCodes(String savedRepPerCode) {
        savedRepPerCodes.add(savedRepPerCode);
    }

    public void setCreators(Integer creator) {
        creators.add(creator);
    }

    public void setIpAdds(String ipAdd) {
        ipAdds.add(ipAdd);
    }

    // end of getter and setter methods

    /**
     * Gets all Reports for a Study.
     * 
     * @param StudyId -
     *            Id of the Study
     */

    public void getAllReps(int studyId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            String mysql = "SELECT PK_SAVEDREP,"
                    + "FK_STUDY,"
                    + "FK_REPORT,"
                    + "SAVEDREP_NAME,"
                    + "SAVEDREP_ASOFDATE,"
                    + "FK_USER,"
                    + "b.usr_lastname || b.usr_firstname as user_name, "
                    + "FK_PAT,"
                    + "c.per_code,"
                    + "a.SAVEDREP_FILTER,"
                    + "CREATOR,"
                    + "IP_ADD"
                    + " FROM ER_SAVEDREP a, ER_USER b, er_per c "
                    + " WHERE FK_STUDY =? AND b.PK_USER = a.FK_USER and c.pk_per = a.fk_pat";

            Rlog.debug("SavedRep", "SavedRepDao.getAllReps SQL " + mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, studyId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setSavedRepIds(new Integer(rs.getInt("PK_SAVEDREP")));
                setSavedRepStudyIds(new Integer(rs.getInt("FK_STUDY")));
                setSavedRepReportIds(new Integer(rs.getInt("FK_REPORT")));
                setSavedRepRepNames(rs.getString("SAVEDREP_NAME"));
                setSavedRepAsOfDates(rs.getString("SAVEDREP_ASOFDATE"));
                setSavedRepUserIds(new Integer(rs.getInt("FK_USER")));
                setSavedRepUserNames(rs.getString("USER_NAME"));
                setSavedRepPatIds(new Integer(rs.getInt("FK_PAT")));
                setSavedRepPerCodes(rs.getString("PER_CODE"));
                setSavedRepFilters(rs.getString("SAVEDREP_FILTER"));
                setCreators(new Integer(rs.getInt("CREATOR")));
                setIpAdds(rs.getString("IP_ADD"));

                rows++;
                Rlog.debug("SavedRep", "SavedRepDao.getAllReps " + rows);
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("SavedRep",
                    "SavedRepDao.getAllReps  EXCEPTION IN FETCHING FROM er_savedrep table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Update the report html(clob)
     * 
     * @param savedRepId -
     *            Id of the saved report
     * @param repContent -
     *            The content of the saved report
     * @return 1 if successful, -1 in case of error
     */

    public int updateReportContent(int savedRepId, String repContent) {
        int rows = 0;

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            conn.setAutoCommit(false);

            // insert empty clob for the first time
            pstmt = conn
                    .prepareStatement("Update er_savedrep set  SAVEDREP_CONTENTS = empty_clob() where PK_SAVEDREP =  ?");

            pstmt.setInt(1, savedRepId);
            pstmt.execute();

            pstmt.close();
            Rlog.debug("SavedRep", "SavedRepDao.updateReportContent rep id "
                    + savedRepId);

            PreparedStatement c_stmt = conn
                    .prepareStatement("select SAVEDREP_CONTENTS from er_savedrep where  PK_SAVEDREP = "
                            + savedRepId + " FOR UPDATE");
            ResultSet savedClob = c_stmt.executeQuery();

            Rlog.debug("SavedRep", "SavedRepDao.updateReportContent 1");

            // Retrieve CLOB

            if (savedClob.next()) {

                Rlog.debug("SavedRep", "SavedRepDao.updateReportContent 2");

                // Get the CLOB locator and open output stream for the CLOB
                CLOB rCLOB = ((OracleResultSet) savedClob).getCLOB(1);
                Writer l_clobWriter = rCLOB.getCharacterOutputStream();

                Rlog.debug("SavedRep", "SavedRepDao.updateReportContent 3");

                l_clobWriter.write(repContent); // Write to CLOB

                // Close the writer
                l_clobWriter.close();
            }
               conn.commit(); 
            savedClob.close();
            c_stmt.close();

            return 1;
        } catch (Exception ex) {
            Rlog.fatal("SavedRep",
                    "SavedRepDao.UpdateReport  EXCEPTION IN updating clob er_savedrep table"
                            + ex);
            return -1;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Get the report html(clob)
     * 
     * @param savedRepId -
     *            Id of the saved report
     */

    public String getReportContent(int savedRepId) {
        int rows = 0;
        String reportContent = null;

        Connection conn = null;
        try {
            conn = getConnection();

            Rlog.debug("SavedRep", "SavedRepDao.updateReportContent rep id "
                    + savedRepId);

            String mysql = "select SAVEDREP_CONTENTS from er_savedrep where  PK_SAVEDREP = ?";

            PreparedStatement c_stmt = conn.prepareStatement(mysql);
            c_stmt.setInt(1, savedRepId);

            ResultSet savedClob = c_stmt.executeQuery();

            Rlog.debug("SavedRep", "SavedRepDao.updateReportContent 1");

            // Retrieve CLOB

            if (savedClob.next()) {

                Rlog.debug("SavedRep", "SavedRepDao.updateReportContent 2");

                // Get the CLOB locator and open output stream for the CLOB
                CLOB rCLOB = ((OracleResultSet) savedClob).getCLOB(1);
                if (!(rCLOB == null)) {
                    reportContent = rCLOB.getSubString(1, (int) rCLOB.length());
                }

                Rlog.debug("SavedRep",
                        "SavedRepDao.updateReportContent rep content "
                                + reportContent);
            }

            savedClob.close();
            c_stmt.close();
            return reportContent;

        } catch (Exception ex) {
            Rlog.fatal("SavedRep",
                    "SavedRepDao.UpdateReport  EXCEPTION IN getting clob er_savedrep table"
                            + ex);
            return reportContent;
        } finally {

            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Validation of unique report name in study
     * 
     * @param studyId -
     *            Study Id
     * @param savedRepName -
     *            user entered name of saved report
     */

    public int validateReportName(int studyId, String savedRepName) {
        int cnt = 0;

        Connection conn = null;
        try {
            conn = getConnection();

            Rlog.debug("SavedRep", "SavedRepDao.validateReportName rep name "
                    + savedRepName);

            String valSql = "select count(*) as count from eR_savedrep where lower(trim(SAVEDREP_NAME))= lower(trim(?)) and fk_study=? ";

            PreparedStatement pstmt = conn.prepareStatement(valSql);
            pstmt.setString(1, savedRepName);
            pstmt.setInt(2, studyId);
            ResultSet cntResultSet = pstmt.executeQuery();

            if (cntResultSet.next()) {
                cnt = cntResultSet.getInt("count");
                Rlog.debug("SavedRep", "SavedRepDao.validateReportName count "
                        + cnt);
            }

            pstmt.close();
            return cnt;

        } catch (Exception ex) {
            Rlog
                    .fatal(
                            "SavedRep",
                            "SavedRepDao.validateReportName  EXCEPTION IN performing name validation er_savedrep table"
                                    + ex);
            return -1;
        } finally {

            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

}
