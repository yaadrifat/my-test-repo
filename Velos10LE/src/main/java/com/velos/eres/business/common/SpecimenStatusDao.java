
/*
 * Classname			SpecimenStatusDao.class
 *
 * Version information 	1.0
 *
 * Date					10/15/2007
 *
 * Copyright notice		Velos, Inc.
 *
 * Author 				Jnanamay Majumdar
 */

package com.velos.eres.business.common;

import java.sql.Connection;
import com.velos.eres.service.util.Rlog;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Clob;


/**
 * SpecimenStatusDao for retrieving specimen status records
 *
 * @author Jnanamay Majumdar
 * @version : 1.0 10/15/2007
 */


public class SpecimenStatusDao extends CommonDAO implements java.io.Serializable {


    private ArrayList pkSpecimenStats;
    private ArrayList fkSpecimens;
    private ArrayList ssDates;
    private ArrayList fkCodelstSpecimenStats;
    private ArrayList ssQuantitys;
    private ArrayList ssQuantityUnits;
    private ArrayList ssActions;
    private ArrayList fkStudys;
    private ArrayList fkUserRecpts;
    private ArrayList trackingNums;
    private ArrayList specimenNotes;
    private ArrayList ssStatusBys;
    private ArrayList statusDesc;
    private ArrayList ssFlags;
    private ArrayList ssProcType;




    public SpecimenStatusDao() {
    	pkSpecimenStats = new ArrayList();
    	fkSpecimens = new ArrayList();
    	ssDates = new ArrayList();
    	fkCodelstSpecimenStats = new ArrayList();
    	ssQuantitys = new ArrayList();
    	ssQuantityUnits = new ArrayList();
    	ssActions = new ArrayList();
    	fkStudys = new ArrayList();
    	fkUserRecpts = new ArrayList();
    	trackingNums = new ArrayList();
    	specimenNotes = new ArrayList();
    	ssStatusBys = new ArrayList();
    	statusDesc= new ArrayList();
    	ssFlags= new ArrayList();
    	ssProcType = new ArrayList();

   }


//  Getter and Setter methods

    public ArrayList getPkSpecimenStats() {
        return this.pkSpecimenStats;
    }

    public void setPkSpecimenStats(ArrayList pkSpecimenStats) {
        this.pkSpecimenStats = pkSpecimenStats;
    }

    public void setPkSpecimenStats(Integer pkSpecimenStat) {
        this.pkSpecimenStats.add(pkSpecimenStat);
    }


    public ArrayList getFkSpecimens() {
        return this.fkSpecimens;
    }

    public void setFkSpecimens(ArrayList fkSpecimens) {
        this.fkSpecimens = fkSpecimens;
    }

    public void setFkSpecimens(Integer fkSpecimen) {
        this.fkSpecimens.add(fkSpecimen);
    }


    public ArrayList getSsDates() {
        return this.ssDates;
    }

    public void setSsDates(ArrayList ssDates) {
        this.ssDates = ssDates;
    }

    public void setSsDates(String ssDate) {
        this.ssDates.add(ssDate);
    }

    public ArrayList getFkCodelstSpecimenStats() {
        return this.fkCodelstSpecimenStats;
    }

    public void setFkCodelstSpecimenStats(ArrayList fkCodelstSpecimenStats) {
        this.fkCodelstSpecimenStats = fkCodelstSpecimenStats;
    }

    public void setFkCodelstSpecimenStats(Integer fkCodelstSpecimenStat) {
        this.fkCodelstSpecimenStats.add(fkCodelstSpecimenStat);
    }


    public ArrayList getSsQuantitys() {
        return this.ssQuantitys;
    }

    public void setSsQuantitys(ArrayList ssQuantitys) {
        this.ssQuantitys = ssQuantitys;
    }

    //public void setSsQuantitys(String ssQuantity) {
    	public void setSsQuantitys(float ssQuantity) {
        this.ssQuantitys.add(ssQuantity);
    }


    public ArrayList getSsQuantityUnits() {
        return this.ssQuantityUnits;
    }

    public void setSsQuantityUnits(ArrayList ssQuantityUnits) {
        this.ssQuantityUnits = ssQuantityUnits;
    }

    public void setSsQuantityUnits(Integer ssQuantityUnit) {
        this.ssQuantityUnits.add(ssQuantityUnit);
    }


    public ArrayList getSsActions() {
        return this.ssActions;
    }

    public void setSsActions(ArrayList ssActions) {
        this.ssActions = ssActions;
    }

    public void setSsActions(Integer ssAction) {
        this.ssActions.add(ssAction);
    }


    public ArrayList getFkStudys() {
        return this.fkStudys;
    }

    public void setFkStudys(ArrayList fkStudys) {
        this.fkStudys = fkStudys;
    }

    public void setFkStudys(Integer fkStudy) {
        this.fkStudys.add(fkStudy);
    }



    public ArrayList getFkUserRecpts() {
        return this.fkUserRecpts;
    }

    public void setFkUserRecpts(ArrayList fkUserRecpts) {
        this.fkUserRecpts = fkUserRecpts;
    }

    public void setFkUserRecpts(Integer fkUserRecpt) {
        this.fkUserRecpts.add(fkUserRecpt);
    }


    public ArrayList getTrackingNums() {
        return this.trackingNums;
    }

    public void setTrackingNums(ArrayList trackingNums) {
        this.trackingNums = trackingNums;
    }

    public void setTrackingNums(String trackingNum) {
        this.trackingNums.add(trackingNum);
    }



    public ArrayList getSpecimenNotes() {
        return this.specimenNotes;
    }

    public void setSpecimenNotes(ArrayList specimenNotes) {
        this.specimenNotes = specimenNotes;
    }

    public void setSpecimenNotes(String specimenNote) {
        this.specimenNotes.add(specimenNote);
    }



    public ArrayList getSsStatusBys() {
        return this.ssStatusBys;
    }

    public void setSsStatusBys(ArrayList ssStatusBys) {
        this.ssStatusBys = ssStatusBys;
    }

    public void setSsStatusBys(Integer ssStatusBy) {
        this.ssStatusBys.add(ssStatusBy);
    }

    public ArrayList getSsFlags() {
        return this.ssFlags;
    }

    public void setSsFlags(ArrayList ssFlags) {
        this.ssFlags = ssFlags;
    }

    public void setSsFlags(String ssFlag) {
        this.ssFlags.add(ssFlag);
    }

//JM: 02Sep2009: #4206:
    public ArrayList getSsProcTypes() {
        return this.ssProcType;
    }

    public void setSsProcTypes(ArrayList ssProcType) {
        this.ssProcType = ssProcType;
    }

    public void setSsProcTypes(String ssProcTyp) {
        this.ssProcType.add(ssProcTyp);
    }

    public void getSpecimenStausValues(int specimenId) {

    	PreparedStatement pstmt = null;
        Connection conn = null;
        String notesStr ="";
        Clob notesClob = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement("Select PK_SPECIMEN_STATUS,FK_SPECIMEN,SS_DATE,FK_CODELST_STATUS,SS_QUANTITY,SS_QUANTITY_UNITS,SS_ACTION,FK_STUDY,FK_USER_RECEPIENT,SS_TRACKING_NUMBER,SS_STATUS_BY,SS_NOTES,(SELECT CODELST_CUSTOM_COL FROM ER_CODELST WHERE PK_CODELST = FK_CODELST_STATUS) SS_FLAG from ER_SPECIMEN_STATUS where FK_SPECIMEN = ? and FK_CODELST_STATUS not in  (select PK_CODELST from er_Codelst where CODELST_TYPE = 'specimen_stat' and CODELST_CUSTOM_COL1 ='process') order by  SS_DATE desc, PK_SPECIMEN_STATUS desc");
            pstmt.setInt(1, specimenId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {



            	setPkSpecimenStats(new Integer(rs.getInt("PK_SPECIMEN_STATUS")));
            	setFkSpecimens(rs.getInt("FK_SPECIMEN"));
            	setSsDates(rs.getString("SS_DATE"));
            	setFkCodelstSpecimenStats(rs.getInt("FK_CODELST_STATUS"));
//JM: 11Sep2009: Issue No- 4289
            	//setSsQuantitys(rs.getString("SS_QUANTITY"));
            	setSsQuantitys(rs.getFloat("SS_QUANTITY"));

            	setSsQuantityUnits(rs.getInt("SS_QUANTITY_UNITS"));
            	setSsActions(rs.getInt("SS_ACTION"));
            	setFkStudys(rs.getInt("FK_STUDY"));
            	setFkUserRecpts(rs.getInt("FK_USER_RECEPIENT"));
            	setTrackingNums(rs.getString("SS_TRACKING_NUMBER"));
            	setSsStatusBys(rs.getInt("SS_STATUS_BY"));
            	setSpecimenNotes(rs.getString("SS_NOTES"));
            	setSsFlags(rs.getString("SS_FLAG"));


            }

       } catch (SQLException ex) {
            Rlog.fatal("specimenStatus",
                    "SpecimenStatusDao.getSpecimenStausValues EXCEPTION IN FETCHING FROM ER_SPECIMEN_STATUS table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            	Rlog.fatal("specimenStatus", "SpecimenStatusDao.getSpecimenStausValues EXCEPTION in closing the pstmt" +e);
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            	Rlog.fatal("specimenStatus", "SpecimenStatusDao.getSpecimenStausValues EXCEPTION in closing the connection" +e);
            }

        }

    }
    //JM: #INV 2.29 added method
    public void getSpecimenStausProcessValues(int specimenId) {

    	PreparedStatement pstmt = null;
        Connection conn = null;
        String notesStr ="";
        Clob notesClob = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement("Select PK_SPECIMEN_STATUS,FK_SPECIMEN,SS_DATE,FK_CODELST_STATUS,SS_QUANTITY,SS_QUANTITY_UNITS,SS_ACTION,FK_STUDY,FK_USER_RECEPIENT,SS_TRACKING_NUMBER,SS_STATUS_BY,SS_NOTES,(SELECT CODELST_CUSTOM_COL FROM ER_CODELST WHERE PK_CODELST = FK_CODELST_STATUS) SS_FLAG, (select codelst_desc from er_codelst where pk_codelst= SS_PROC_TYPE) proc_type from ER_SPECIMEN_STATUS where FK_SPECIMEN = ? and FK_CODELST_STATUS in ( select PK_CODELST from er_Codelst where CODELST_TYPE = 'specimen_stat' and CODELST_CUSTOM_COL1 ='process'	)order by  SS_DATE asc, PK_SPECIMEN_STATUS asc");

            pstmt.setInt(1, specimenId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {



            	setPkSpecimenStats(new Integer(rs.getInt("PK_SPECIMEN_STATUS")));
            	setFkSpecimens(rs.getInt("FK_SPECIMEN"));
            	setSsDates(rs.getString("SS_DATE"));
            	setFkCodelstSpecimenStats(rs.getInt("FK_CODELST_STATUS"));
//JM: 11Sep2009: Issue No- 4289
            	//setSsQuantitys(rs.getString("SS_QUANTITY"));
            	setSsQuantitys(rs.getFloat("SS_QUANTITY"));

            	setSsQuantityUnits(rs.getInt("SS_QUANTITY_UNITS"));
            	setSsActions(rs.getInt("SS_ACTION"));
            	setFkStudys(rs.getInt("FK_STUDY"));
            	setFkUserRecpts(rs.getInt("FK_USER_RECEPIENT"));
            	setTrackingNums(rs.getString("SS_TRACKING_NUMBER"));
            	setSsStatusBys(rs.getInt("SS_STATUS_BY"));
            	setSpecimenNotes(rs.getString("SS_NOTES"));
            	setSsFlags(rs.getString("SS_FLAG"));
            	setSsProcTypes(rs.getString("proc_type"));


            }

       } catch (SQLException ex) {
            Rlog.fatal("specimenStatus",
                    "SpecimenStatusDao.getSpecimenStausProcessValues EXCEPTION IN FETCHING FROM ER_SPECIMEN_STATUS table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            	Rlog.fatal("specimenStatus", "SpecimenStatusDao.getSpecimenStausProcessValues EXCEPTION in closing the pstmt" +e);
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            	Rlog.fatal("specimenStatus", "SpecimenStatusDao.getSpecimenStausProcessValues EXCEPTION in closing the connection" +e);
            }

        }

    }


//  Added by Manimaran for deletion of Specimen status.
    public int delSpecimenStatus(int pkSpecimenStat) {

        PreparedStatement pstmt = null;
        Connection conn = null;

        try {

            conn = getConnection();

            pstmt = conn
                    .prepareStatement("delete from er_specimen_status where pk_specimen_status = ? ");

            Rlog.debug("specimenstatus", "in delSpecimenStatus of SpecimenStatusDao");

            pstmt.setInt(1,pkSpecimenStat);

            ResultSet rs = pstmt.executeQuery();

        } catch (Exception ex) {
            Rlog.fatal("specimenstatus",
                    "Error in delSpecimenStatus in SpecimenStatusDao " + ex);
            return -1;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();

            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();

            } catch (Exception e) {
            }
        }
        return 0;

    }











    public void getLatestSpecimenStaus(int specimenId) {

    	PreparedStatement pstmt = null;
        Connection conn = null;

        StringBuffer sbSelect = new StringBuffer();


        try {
            conn = getConnection();

            sbSelect.append(" select  (SELECT codelst_desc FROM er_codelst WHERE pk_codelst = fk_codelst_status ) status_desc,FK_CODELST_STATUS," +
            		"o.SS_QUANTITY,o.SS_QUANTITY_UNITS, o.PK_SPECIMEN_STATUS,o.SS_ACTION,o.fk_study,to_char(o.SS_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) SS_DATE from ER_SPECIMEN_STATUS o ");
            sbSelect.append(" where  o.fk_specimen = ?");
        	sbSelect.append(" and o.pk_specimen_status= (select max(i.PK_SPECIMEN_STATUS) from ER_SPECIMEN_STATUS i where i.fk_specimen=o.fk_specimen ");
            sbSelect.append(" and i.SS_DATE =(select max(k.SS_DATE) from ER_SPECIMEN_STATUS k where k.fk_specimen=i.fk_specimen ))  ") ;



            pstmt = conn.prepareStatement(sbSelect.toString());
            pstmt.setInt(1, specimenId);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
               	setPkSpecimenStats(new Integer(rs.getInt("PK_SPECIMEN_STATUS")));
            	setFkSpecimens(specimenId);
            	setSsDates(rs.getString("SS_DATE"));
            	setFkCodelstSpecimenStats(rs.getInt("FK_CODELST_STATUS"));
//JM: 11Sep2009: Issue No- 4289
            	//setSsQuantitys(rs.getString("SS_QUANTITY"));
            	setSsQuantitys(rs.getFloat("SS_QUANTITY"));

            	setSsQuantityUnits(rs.getInt("SS_QUANTITY_UNITS"));
            	setSsActions(rs.getInt("SS_ACTION"));
            	setFkStudys(rs.getInt("FK_STUDY"));
            	setStatusDesc(rs.getString("status_desc"));

            }

       } catch (SQLException ex) {
            Rlog.fatal("specimenStatus",
                    "SpecimenStatusDao.getLatestSpecimenStaus EXCEPTION IN FETCHING FROM ER_SPECIMEN_STATUS table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            	Rlog.fatal("specimenStatus", "SpecimenStatusDao.getLatestSpecimenStaus EXCEPTION in closing the pstmt" +e);
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            	Rlog.fatal("specimenStatus", "SpecimenStatusDao.getLatestSpecimenStaus EXCEPTION in closing the connection" +e);
            }

        }

    }


	public ArrayList getStatusDesc() {
		return statusDesc;
	}


	public void setStatusDesc(ArrayList statusDesc) {
		this.statusDesc = statusDesc;
	}

	public void setStatusDesc(String status) {
		this.statusDesc.add(status);
	}


	//end of class
}
