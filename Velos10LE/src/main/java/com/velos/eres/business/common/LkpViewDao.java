/*
 * Classname : LkpViewDao
 *
 * Version information: 1.0
 *
 * Date: 02/16/2011
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Isaac Huang
 */

package com.velos.eres.business.common;

/* IMPORT STATEMENTS */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */
public class LkpViewDao extends CommonDAO implements java.io.Serializable {

    /**
	 * Generated Serial Number
	 */
	private static final long serialVersionUID = -4780742684972732742L;
	private ArrayList<Integer> pkLkpView;
    private ArrayList<String> lkpViewKey;
    private ArrayList<String> lkpViewName;
    private ArrayList<String> lkpType;
    private ArrayList<String> lkpViewFilterSQL;
    private ArrayList<String> lkpViewIgnoreFilter;

    public LkpViewDao () {
        pkLkpView =  new ArrayList<Integer>();
        lkpViewKey = new ArrayList<String>();
        lkpViewName = new ArrayList<String>();
        lkpType = new ArrayList<String>();
        lkpViewFilterSQL = new ArrayList<String>();
        lkpViewIgnoreFilter = new ArrayList<String>();
    }

    //Then the getter and setter methods goes here
    public ArrayList<Integer> getPkLkpView() {
        return this.pkLkpView;
    }
    private void setPkLkpView(Integer pkLkpView) {
        this.pkLkpView.add(pkLkpView);
    }

    public ArrayList<String> getLkpViewKey() {
        return this.lkpViewKey;
    }
    private void setLkpViewKey(String lkpViewKey) {
        this.lkpViewKey.add(lkpViewKey);
    }

    public ArrayList<String> getLkpViewName() {
        return this.lkpViewName;
    }
    private void setLkpViewName(String lkpViewName) {
        this.lkpViewName.add(lkpViewName);
    }

    public ArrayList<String> getLkpType() {
        return this.lkpType;
    }
    private void setLkpType(String lkpType) {
        this.lkpType.add(lkpType);
    }

    public ArrayList<String> getLkpViewFilterSQL() {
        return this.lkpViewFilterSQL;
    }
    private void setLkpFilterSQL(String lkpFilterSQL) {
        this.lkpViewFilterSQL.add(lkpFilterSQL);
    }

    public ArrayList<String> getLkpViewIgnoreFilter() {
        return this.lkpViewIgnoreFilter;
    }
    public void setLkpViewIgnoreFilter(String lkpViewIgnoreFilter) {
        this.lkpViewIgnoreFilter.add(lkpViewIgnoreFilter);
    }
    //end of getter and setter methods

    public void getLkpViewData() {
        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            pstmt = conn.prepareStatement("select PK_LKPVIEW, LKPVIEW_KEYWORD, LKPVIEW_NAME, "
            		+ "LKPTYPE_NAME, LKPVIEW_FILTER, LKPVIEW_IGNOREFILTER from ER_LKPLIB, ER_LKPVIEW "
                    + "WHERE PK_LKPLIB = FK_LKPLIB");
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("LkpView", "LkpViewDao.getLkpViewData... ");
            while (rs.next()) {
                setPkLkpView(rs.getInt("PK_LKPVIEW"));
                setLkpViewKey(rs.getString("LKPVIEW_KEYWORD"));
                setLkpViewName(rs.getString("LKPVIEW_NAME"));
                setLkpType(rs.getString("LKPTYPE_NAME"));
                setLkpFilterSQL(rs.getString("LKPVIEW_FILTER"));
                setLkpViewIgnoreFilter(rs.getString("LKPVIEW_IGNOREFILTER"));
            }
        } catch(SQLException ex){
            Rlog.fatal("LkpView",
                    "LkpViewDao.getLkpViewData EXCEPTION IN FETCHING FROM ER_LkpView table... "
                    + ex);
        } finally {
            try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
            try { if (conn != null) { conn.close(); } } catch (Exception e) {}
        }
    }
}
