/*
 * Classname			LinkedFormBean
 * 
 * Version information : 1.0
 *
 * Date					 08/08/2003
 * 
 * Copyright notice :    Velos Inc
 */

package com.velos.eres.business.linkedForms.impl;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.business.common.LinkedFormsDao;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* END OF IMPORT STATEMENTS */

/**
 * The linked forms CMP entity bean.<br>
 * <br>
 * 
 * @author Anu Khanna
 * 
 * @version 1.0, 08/08/2003
 * 
 * @ejbHome LinkedFormHome
 * 
 * @ejbRemote LinkedFormRObj
 */
@Entity
@Table(name = "er_linkedforms")
@NamedQueries( {
        @NamedQuery(name = "findByCrfEventId", query = "SELECT OBJECT(lf) FROM LinkedFormsBean lf  where fk_crf = :fk_crf  AND fk_event = :fk_event "),
        @NamedQuery(name = "findByFormId", query = "SELECT OBJECT(lf) FROM LinkedFormsBean lf  where fk_formlib = :fk_formlib  ")

})
public class LinkedFormsBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3906082360670565945L;

    /**
     * the linkedform id
     */
    public int linkedFormId;

    /**
     * the form id
     */
    public Integer formLibId;

    /**
     * the form's display setting
     */
    public String lfDisplayType;

    /**
     * number of times the form can be filled
     */
    public String lfEntry;

    /**
     * the account id
     */
    public Integer accountId;

    /**
     * the study id
     */
    public Integer studyId;

    /**
     * to indicate location of association of form
     */
    public String lnkFrom;

    /**
     * the calender id
     */
    public Integer calenderId;

    /**
     * the event id
     */
    public Integer eventId;

    /**
     * the crf
     */
    public Integer crfId;

    /**
     * last modified by
     */
    public Integer lastModifiedBy;

    /**
     * the Creator
     */
    public Integer creator;

    /**
     * the Ip address of the creator
     */
    public String ipAdd;

    /**
     * Record Type
     */
    public String recordType;

    /**
     * Data count
     */

    public Integer lfDataCnt;

    /**
     * Form Hide
     */

    public Integer lfHide;

    /**
     * Form Sequence
     */

    public Integer lfSeq;

    /**
     * Form to be Display in Patient Profile
     */

    public Integer lfDispInPat;

    
    /**
     * Form to be Display in Specimen module
     */

    public String lfDispInSpec;

    private LinkedFormsDao linkedFormsDao;

    // GETTER SETTER METHODS

    public LinkedFormsBean(int linkedFormId, String formLibId,
            String lfDisplayType, String lfEntry, String accountId,
            String studyId, String lnkFrom, String calenderId, String eventId,
            String crfId, String lastModifiedBy, String creator, String ipAdd,
            String recordType, String lfDataCnt, String lfHide, String lfSeq,
            String lfDispInPat, String lfDispinSpec) {
        super();
        // TODO Auto-generated constructor stub
        setLinkedFormId(linkedFormId);
        setFormLibId(formLibId);
        setLFDisplayType(lfDisplayType);
        setLFEntry(lfEntry);
        setAccountId(accountId);
        setStudyId(studyId);
        setLnkFrom(lnkFrom);
        setCalenderId(calenderId);
        setEventId(eventId);
        setCrfId(crfId);
        setLastModifiedBy(lastModifiedBy);
        setCreator(creator);
        setIpAdd(ipAdd);
        setRecordType(recordType);
        setLfDataCnt(lfDataCnt);
        setLfHide(lfHide);
        setLfSeq(lfSeq);
        setLfDispInPat(lfDispInPat);
        setLfDispInSpec(lfDispinSpec);

    }

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_LINKEDFORMS", allocationSize=1)
    @Column(name = "PK_LF")
    public int getLinkedFormId() {
        return this.linkedFormId;
    }

    public void setLinkedFormId(int id) {
        this.linkedFormId = id;
    }

    @Column(name = "FK_ACCOUNT")
    public String getAccountId() {
        return StringUtil.integerToString(this.accountId);

    }

    public void setAccountId(String accountId) {
        this.accountId = StringUtil.stringToInteger(accountId);

    }

    @Column(name = "FK_CALENDAR")
    public String getCalenderId() {
        return StringUtil.integerToString(this.calenderId);
    }

    public void setCalenderId(String calenderId) {

        this.calenderId = StringUtil.stringToInteger(calenderId);

    }

    @Column(name = "FK_CRF")
    public String getCrfId() {
        return StringUtil.integerToString(this.crfId);
    }

    public void setCrfId(String crfId) {
        this.crfId = StringUtil.stringToInteger(crfId);

    }

    @Column(name = "FK_EVENT")
    public String getEventId() {
        return StringUtil.integerToString(this.eventId);
    }

    public void setEventId(String eventId) {
        this.eventId = StringUtil.stringToInteger(eventId);

    }

    @Column(name = "LF_DATACNT")
    public String getLfDataCnt() {
        return StringUtil.integerToString(this.lfDataCnt);
    }

    public void setLfDataCnt(String lfDataCnt) {
        this.lfDataCnt = StringUtil.stringToInteger(lfDataCnt);

    }

    @Column(name = "LF_HIDE")
    public String getLfHide() {
        return StringUtil.integerToString(this.lfHide);
    }

    public void setLfHide(String lfHide) {
        this.lfHide = StringUtil.stringToInteger(lfHide);
    }

    @Column(name = "LF_SEQ")
    public String getLfSeq() {
        return StringUtil.integerToString(this.lfSeq);
    }

    public void setLfSeq(String lfSeq) {
        this.lfSeq = StringUtil.stringToInteger(lfSeq);
    }

    @Column(name = "LF_DISPLAY_INPAT")
    public String getLfDispInPat() {
        return StringUtil.integerToString(this.lfDispInPat);
    }

    public void setLfDispInPat(String lfDispInPat) {
        this.lfDispInPat = StringUtil.stringToInteger(lfDispInPat);
    }

    @Column(name = "FK_FORMLIB")
    public String getFormLibId() {
        return StringUtil.integerToString(this.formLibId);
    }

    public void setFormLibId(String formLibId) {
        this.formLibId = StringUtil.stringToInteger(formLibId);

    }

    @Column(name = "LF_DISPLAYTYPE")
    public String getLFDisplayType() {
        return this.lfDisplayType;
    }

    public void setLFDisplayType(String lfDisplayType) {
        this.lfDisplayType = lfDisplayType;
    }

    @Column(name = "LF_ENTRYCHAR")
    public String getLFEntry() {
        return this.lfEntry;
    }

    public void setLFEntry(String lfEntry) {
        this.lfEntry = lfEntry;
    }

    @Column(name = "LF_LNKFROM")
    public String getLnkFrom() {
        return this.lnkFrom;
    }

    public void setLnkFrom(String lnkFrom) {
        this.lnkFrom = lnkFrom;
    }

    @Column(name = "RECORD_TYPE")
    public String getRecordType() {
        return this.recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    @Column(name = "FK_STUDY")
    public String getStudyId() {
        return StringUtil.integerToString(this.studyId);
    }

    public void setStudyId(String studyId) {

        this.studyId = StringUtil.stringToInteger(studyId);
        Rlog.debug("lnkform", "**LinkedFormBean this.studyId" + this.studyId);
    }

    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
        Rlog.debug("lnkform", "LinkedFormBean setCreator" + creator);
        this.creator = StringUtil.stringToInteger(creator);

    }

    @Column(name = "LAST_MODIFIED_BY")
    public String getLastModifiedBy() {
        return StringUtil.integerToString(this.lastModifiedBy);
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = StringUtil.stringToInteger(lastModifiedBy);

    }

    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    @Transient
    public LinkedFormsDao getLinkedFormsDao() {
        return this.linkedFormsDao;
    }

    public void setLinkedFormsDao(LinkedFormsDao linkedFormsDao) {
        this.linkedFormsDao = linkedFormsDao;
    }

    public LinkedFormsBean() {
        linkedFormsDao = new LinkedFormsDao();
    }

    // END OF GETTER SETTER METHODS

    /*
     * public LinkedFormsStateKeeper getLinkedFormStateKeeper() {
     * Rlog.debug("lnkform", "LinkedFormBean.getLinkedFormStateKeeper ");
     * 
     * return (new LinkedFormsStateKeeper(getLinkedFormId(), getFormLibId(),
     * getLFDisplayType(), getLFEntry(), getAccountId(), getStudyId(),
     * getLnkFrom(), getCalenderId(), getEventId(), getCrfId(), getRecordType(),
     * getCreator(), getLastModifiedBy(), getIpAdd(), getLfDataCnt(),
     * getLfHide(), getLfSeq(), getLfDispInPat())); }
     */

    /**
     * Sets up a state keeper containing details of linked forms
     */
    /*
     * public void setLinkedFormStateKeeper(LinkedFormsStateKeeper lfsk) {
     * GenerateId generateId = null; try {
     * 
     * Connection conn = null; conn = getConnection(); this.linkedFormId =
     * generateId.getId("SEQ_ER_LINKEDFORMS", conn); Rlog.debug("lnkform",
     * "LinkedFormBean.setLinkedFormStateKeeper() LinkedFormId :" +
     * linkedFormId); conn.close();
     * 
     * setFormLibId(lfsk.getFormLibId());
     * setLFDisplayType(lfsk.getLFDisplayType()); setLFEntry(lfsk.getLFEntry());
     * setAccountId(lfsk.getAccountId()); setStudyId(lfsk.getStudyId());
     * setLnkFrom(lfsk.getLnkFrom()); setCalenderId(lfsk.getCalenderId());
     * setEventId(lfsk.getEventId()); setCrfId(lfsk.getCrfId());
     * setCreator(lfsk.getCreator()); setRecordType(lfsk.getRecordType());
     * setLastModifiedBy(lfsk.getLastModifiedBy()); setIpAdd(lfsk.getIpAdd());
     * setLfDataCnt(lfsk.getLfDataCnt()); setLfHide(lfsk.getLfHide());
     * setLfSeq(lfsk.getLfSeq()); setLfDispInPat(lfsk.getLfDispInPat()); } catch
     * (Exception e) { Rlog.fatal("lnkform", "Error in
     * setLinkedFormStateKeeper() in LinkedFormBean " + e); } }
     */

    /**
     * Saves the linked forms details in the LinkedFormStateKeeper to the
     * database and returns 0 if the update is successful and -2 if it fails
     * 
     * @param lfsk
     *            linked forms State Keeper with the updated form library
     *            Information
     * 
     * @return 0 - if update is successful <br>
     *         -2 - if update fails
     * 
     * @see LinkedFormStateKeeper
     * 
     */
    public int updateLinkedForm(LinkedFormsBean lfsk) {
        try {
            setFormLibId(lfsk.getFormLibId());

            setLFDisplayType(lfsk.getLFDisplayType());

            setLFEntry(lfsk.getLFEntry());

            setAccountId(lfsk.getAccountId());

            setStudyId(lfsk.getStudyId());
            Rlog.debug("lnkform", "**LinkedFormBean.updateLinkedForm studyid"
                    + lfsk.getStudyId());
            setLnkFrom(lfsk.getLnkFrom());

            setCalenderId(lfsk.getCalenderId());

            setEventId(lfsk.getEventId());

            setCrfId(lfsk.getCrfId());

            setCreator(lfsk.getCreator());
            setRecordType(lfsk.getRecordType());

            setLastModifiedBy(lfsk.getLastModifiedBy());

            setIpAdd(lfsk.getIpAdd());

            setLfDataCnt(lfsk.getLfDataCnt());

            setLfHide(lfsk.getLfHide());
            setLfSeq(lfsk.getLfSeq());
            setLfDispInPat(lfsk.getLfDispInPat());
            setLfDispInSpec(lfsk.getLfDispInSpec());

            Rlog.debug("lnkform", "LinkedFormBean.updateLinkedForm");
            return 0;
        } catch (Exception e) {
            Rlog.fatal("lnkform", " error in LinkedFormBean.updateLinkedForm "
                    + e);
            return -2;
        }
    }

    @Column(name = "LF_DISPLAY_INSPEC")
	public String getLfDispInSpec() {
		return lfDispInSpec;
	}

	
	public void setLfDispInSpec(String lfDispInSpec) {
		this.lfDispInSpec = lfDispInSpec;
	}

}// end of class
