/**
 *@author Sonika Talwar 
 * date Aug 27, 2002
 *@version 1.0
 */

package com.velos.eres.business.perApndx.impl;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

@Entity
@Table(name = "pat_perapndx")
public class PerApndxBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3617574920440133687L;

    public int perApndxId;

    public Integer fkPer;

    public java.util.Date perApndxDate;

    public String perApndxDesc;

    public String perApndxUri;

    public String perApndxType;

    public Integer creator;

    public Integer modifiedBy;

    public String ipAdd;

    // getter/setter methods
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "eres.SEQ_PAT_PERAPNDX", allocationSize=1)
    @Column(name = "PK_PERAPNDX")
    public int getId() {
        return this.perApndxId;
    }

    public void setId(int id) {
        this.perApndxId = id;
    }

    @Column(name = "FK_PER")
    public String getFkPer() {
        return StringUtil.integerToString(this.fkPer);
    }

    public void setFkPer(String fkPer) {
        if (fkPer != null)
            this.fkPer = Integer.valueOf(fkPer);
    }

    @Column(name = "PERAPNDX_DATE")
    public Date getPerApndxDt() {

        return this.perApndxDate;

    }

    public void setPerApndxDt(Date perApndxDate) {

        this.perApndxDate = perApndxDate;
    }

    @Transient
    public String getPerApndxDate() {

        return DateUtil.dateToString(getPerApndxDt());

    }

    public void setPerApndxDate(String perApndxDate) {

        setPerApndxDt(DateUtil.stringToDate(perApndxDate, null));
    }

    @Column(name = "PERAPNDX_DESC")
    public String getPerApndxDesc() {
        return this.perApndxDesc;
    }

    public void setPerApndxDesc(String perApndxDesc) {
        this.perApndxDesc = perApndxDesc;
    }

    @Column(name = "PERAPNDX_TYPE")
    public String getPerApndxType() {
        return this.perApndxType;
    }

    public void setPerApndxType(String perApndxType) {
        this.perApndxType = perApndxType;
    }

    @Column(name = "PERAPNDX_URI")
    public String getPerApndxUri() {
        return this.perApndxUri;
    }

    public void setPerApndxUri(String perApndxUri) {
        this.perApndxUri = perApndxUri;
    }

    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
    	if (! StringUtil.isEmpty(creator))
    	{
    	        this.creator = Integer.valueOf(creator);
    	}
    	else
    	{
    		this.creator = null;
    		
    	}
    }

    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    public void setModifiedBy(String modifiedBy) {
    	
    	if (! StringUtil.isEmpty(modifiedBy))
    	{
    		this.modifiedBy = Integer.valueOf(modifiedBy);
    	}
    	else
    	{
    		this.modifiedBy = null;
    		
    	}
    }

    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // End of getter/setter methods

    /*
     * public PerApndxStateKeeper getPerApndxStateKeeper() { return new
     * PerApndxStateKeeper(getId(), getFkPer(), getPerApndxDate(),
     * getPerApndxDesc(), getPerApndxType(), getPerApndxUri(), getCreator(),
     * getModifiedBy(), getIpAdd()); }
     */

    /*
     * public void setPerApndxStateKeeper(PerApndxStateKeeper pask) { try {
     * Connection conn = null; conn = getConnection(); perApndxId =
     * GenerateId.getId("SEQ_PAT_PERAPNDX", conn); conn.close();
     * 
     * setFkPer(pask.getFkPer()); setPerApndxDesc(pask.getPerApndxDesc());
     * setPerApndxUri(pask.getPerApndxUri());
     * setPerApndxType(pask.getPerApndxType()); Rlog.debug("perapndx", "Date is " +
     * pask.getPerApndxDate()); setPerApndxDate(pask.getPerApndxDate());
     * setCreator(pask.getCreator()); setModifiedBy(pask.getModifiedBy());
     * setIpAdd(pask.getIpAdd()); } catch (Exception e) { Rlog.fatal("perapndx",
     * "exception in setPerApndxStateKeeper" + e); } }
     */

    public int updatePerApndx(PerApndxBean pask) {
        try {
            setFkPer(pask.getFkPer());
            Rlog.debug("perapndx", "pask.getFkPer()" +pask.getFkPer());
            setPerApndxDesc(pask.getPerApndxDesc());
            setPerApndxUri(pask.getPerApndxUri());
            setPerApndxType(pask.getPerApndxType());
            Rlog.debug("perapndx", "pask.getPerApndxType()" +pask.getPerApndxType());
            setPerApndxDate(pask.getPerApndxDate());
            Rlog.debug("perapndx", "pask.getPerApndxDate()" +pask.getPerApndxDate());
            setCreator(pask.getCreator());
            Rlog.debug("perapndx", "pask.getCreator()" +pask.getCreator());
            setModifiedBy(pask.getModifiedBy());
            Rlog.debug("perapndx", "pask.getModifiedBy()" +pask.getModifiedBy());
            setIpAdd(pask.getIpAdd());
        } catch (Exception e) {
            Rlog.fatal("perapndx", "exception in updatePerApndx" + e);
            return -1;
        }
        return 0;
    }

    public PerApndxBean() {

    }

    public PerApndxBean(int perApndxId,String fkPer, String perApndxDate,
            String perApndxDesc, String perApndxUri, String perApndxType,
            String creator, String modifiedBy, String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        this.setId(perApndxId);
        setFkPer(fkPer);
        setPerApndxDate(perApndxDate);
        setPerApndxDesc(perApndxDesc);
        setPerApndxUri(perApndxUri);
        setPerApndxType(perApndxType);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

}