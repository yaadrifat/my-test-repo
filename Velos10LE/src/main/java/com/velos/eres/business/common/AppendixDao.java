package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

public class AppendixDao extends CommonDAO implements java.io.Serializable {
    private ArrayList appendixids;

    private ArrayList appendixstudyids;

    private ArrayList appendixStudyVerIds;

    private ArrayList appendixfile_uris;

    private ArrayList appendixdescriptions;

    private ArrayList appendixpubflags;

    private ArrayList appendixfiles;

    private ArrayList appendixtypes;

    private int cRows;

    public AppendixDao() {
        appendixids = new ArrayList();
        appendixstudyids = new ArrayList();
        appendixStudyVerIds = new ArrayList();
        appendixfile_uris = new ArrayList();
        appendixdescriptions = new ArrayList();
        appendixpubflags = new ArrayList();
        appendixfiles = new ArrayList();
        appendixtypes = new ArrayList();
    }

    // Getter and Setter methods
    public ArrayList getAppendixIds() {
        return this.appendixids;
    }

    public void setAppendixIds(ArrayList appendixids) {
        this.appendixids = appendixids;
    }

    public ArrayList getAppendixStudyIds() {
        return this.appendixstudyids;
    }

    public void setAppendixStudyIds(ArrayList appendixstudyids) {
        this.appendixstudyids = appendixstudyids;
    }

    public ArrayList getAppendixStudyVers() {
        return this.appendixStudyVerIds;
    }

    public void setAppendixStudyVers(ArrayList appendixStudyVerIds) {
        this.appendixStudyVerIds = appendixStudyVerIds;
    }

    public ArrayList getAppendixTypes() {
        return this.appendixtypes;
    }

    public void setAppendixTypes(ArrayList appendixtypes) {
        this.appendixtypes = appendixtypes;
    }

    public ArrayList getAppendixDescriptions() {
        return this.appendixdescriptions;
    }

    public void setAppendixDescriptions(ArrayList appendixdescriptions) {
        this.appendixdescriptions = appendixdescriptions;
    }

    public ArrayList getAppendixFile_Uris() {
        return this.appendixfile_uris;
    }

    public void setAppendixFile_Uris(ArrayList appendixfile_uris) {
        this.appendixfile_uris = appendixfile_uris;
    }

    public ArrayList getAppendixFiles() {
        return this.appendixfiles;
    }

    public void setAppendixFiles(ArrayList appendixfiles) {
        this.appendixfiles = appendixfiles;
    }

    public ArrayList getAppendixPubFlags() {
        return this.appendixpubflags;
    }

    public void setAppendixPubFlags(ArrayList appendixpubflags) {
        this.appendixpubflags = appendixpubflags;
    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public void setAppendixIds(Integer appendixid) {
        appendixids.add(appendixid);
    }

    public void setAppendixStudyIds(String appendixstudyid) {
        appendixstudyids.add(appendixstudyid);
    }

    public void setAppendixStudyVerIds(String appendixStudyVerId) {
        appendixStudyVerIds.add(appendixStudyVerId);
    }

    public void setAppendixTypes(String appendixtype) {
        appendixtypes.add(appendixtype);
    }

    public void setAppendixPubFlags(String appendixpubflag) {
        appendixpubflags.add(appendixpubflag);
    }

    public void setAppendixFile_Uris(String appendixfile_uri) {
        appendixfile_uris.add(appendixfile_uri);
    }

    public void setAppendixFiles(String appendixfile) {
        appendixfiles.add(appendixfile);
    }

    public void setAppendixDescriptions(String appendixdescription) {
        appendixdescriptions.add(appendixdescription);
    }

    // end of getter and setter methods

    public void getAppendixValues(int studyVerId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("Select pk_studyapndx, fk_study, fk_studyver, studyapndx_uri, studyapndx_desc, studyapndx_pubflag, studyapndx_file, studyapndx_type from er_studyapndx where fk_studyver = ? order by studyapndx_type");

            pstmt.setInt(1, studyVerId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                Rlog.debug("appendix", "AppendixDao.id "
                        + rs.getInt("pk_studyapndx"));
                setAppendixIds(new Integer(rs.getInt("pk_studyapndx")));
                setAppendixStudyIds(rs.getString("fk_study"));
                setAppendixStudyVerIds(rs.getString("fk_studyver"));
                setAppendixFile_Uris(rs.getString("studyapndx_uri"));
                setAppendixDescriptions(rs.getString("studyapndx_desc"));
                setAppendixPubFlags(rs.getString("studyapndx_pubflag"));
                setAppendixFiles(rs.getString("studyapndx_file"));
                setAppendixTypes(rs.getString("studyapndx_type"));

                rows++;
                Rlog.debug("appendix", "AppendixDao.getAppendixValues row# "
                        + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("Appendix",
                    "AppendixDao.getAppendixValues EXCEPTION IN FETCHING FROM Appendix table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // ////

    public void getAppendixValuesPublic(int studyVerId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement("Select pk_studyapndx, "
                    + "studyapndx_uri, " + "studyapndx_desc, "
                    + "studyapndx_file, " + "studyapndx_type "
                    + "from er_studyapndx " + "where fk_studyver= ?  "
                    + "and studyapndx_pubflag = 'Y' "
                    + "order by studyapndx_type");

            pstmt.setInt(1, studyVerId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                Rlog.debug("appendix", "AppendixDao.id "
                        + rs.getInt("pk_studyapndx"));
                setAppendixIds(new Integer(rs.getInt("pk_studyapndx")));
                setAppendixFile_Uris(rs.getString("studyapndx_uri"));
                setAppendixDescriptions(rs.getString("studyapndx_desc"));
                setAppendixFiles(rs.getString("studyapndx_file"));
                setAppendixTypes(rs.getString("studyapndx_type"));

                rows++;
                Rlog.debug("appendix",
                        "AppendixDao.getAppendixValuesPublic row# " + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("Appendix",
                    "AppendixDao.getAppendixValuesPublic EXCEPTION IN FETCHING FROM Appendix table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getAppendixValues(int studyVerId, String type) {
        Rlog.debug("appendix", "Value of studyVerid " + studyVerId);
        Rlog.debug("appendix", "Value of type " + type);
        // String test = "Select pk_studyapndx, fk_study, studyapndx_uri,
        // studyapndx_desc, studyapndx_pubflag, studyapndx_file, studyapndx_type
        // from er_studyapndx where fk_study="+studyid +" and studyapndx_type='"
        // +type+"'";
        // Rlog.debug("appendix", "Value of test " + test);

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            //JM: 09Jul2008, order by added, #3462
            pstmt = conn
                    .prepareStatement("Select pk_studyapndx, fk_study, fk_studyver,studyapndx_uri, studyapndx_desc, studyapndx_pubflag, studyapndx_file, studyapndx_type from er_studyapndx where fk_studyver= ? and studyapndx_type= ?  order by trim(lower(studyapndx_uri)) asc");
            Rlog.debug("appendix", pstmt.toString());
            pstmt.setInt(1, studyVerId);
            pstmt.setString(2, type);

            ResultSet rs = pstmt.executeQuery();

            Rlog.debug("appendix", "executed");
            while (rs.next()) {
                Rlog.debug("appendix", "AppendixDao.id "
                        + rs.getInt("pk_studyapndx"));
                setAppendixIds(new Integer(rs.getInt("pk_studyapndx")));
                setAppendixStudyIds(rs.getString("fk_study"));
                setAppendixStudyVerIds(rs.getString("fk_studyver"));
                setAppendixFile_Uris(rs.getString("studyapndx_uri"));
                setAppendixDescriptions(rs.getString("studyapndx_desc"));
                setAppendixPubFlags(rs.getString("studyapndx_pubflag"));
                setAppendixFiles(rs.getString("studyapndx_file"));
                setAppendixTypes(rs.getString("studyapndx_type"));

                rows++;
                Rlog.debug("appendix", "AppendixDao.getAppendixValues row# "
                        + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("Appendix",
                    "AppendixDao.getAppendixValues EXCEPTION IN FETCHING FROM Appendix table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /** gets latest document for version category */
    public void getLatestDocumentForCategory(String studyId,String versionCategory) {
 
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer versionSql = new StringBuffer();
        
        versionSql.append("select m.pk_studyapndx,o.studyapndx_uri,o.fk_studyver,o.studyapndx_desc,o.studyapndx_file,o.studyapndx_pubflag ");
        versionSql.append(" from ( Select max(pk_studyapndx) pk_studyapndx");
        
        versionSql.append(" from er_studyapndx x,er_studyver v where v.fk_study = ? and v.studyver_category = ? and ");
        versionSql.append(" fk_studyver= pk_studyver  and studyapndx_type='file' ) m, er_studyapndx o where m.pk_studyapndx = o.pk_studyapndx ");
        
        try {
            conn = getConnection();
            
            pstmt = conn
                    .prepareStatement(versionSql.toString());

            pstmt.setString(1, studyId);
            pstmt.setString(2, versionCategory);

            ResultSet rs = pstmt.executeQuery();

             
            while (rs.next()) {
                setAppendixIds(new Integer(rs.getInt("pk_studyapndx")));
                setAppendixStudyIds(studyId);
                setAppendixStudyVerIds(rs.getString("fk_studyver"));
                setAppendixFile_Uris(rs.getString("studyapndx_uri"));
                setAppendixDescriptions(rs.getString("studyapndx_desc"));
                setAppendixPubFlags(rs.getString("studyapndx_pubflag"));
                setAppendixFiles(rs.getString("studyapndx_file"));
                setAppendixTypes("file");

                rows++;
                 
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("Appendix",
                    "AppendixDao.getLatestDocumentForCategory EXCEPTION IN FETCHING FROM Appendix table"
                            + ex);
            ex.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
}
