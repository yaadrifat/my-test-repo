

package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Hashtable;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.milestone.MilestoneJB;

/**
 * MileAchievedDao - data Access class for Milestones Achieved module 
 * 
 * @author Sonia Abrol
 * @created 11/04/2005
 */
public class MileAchievedDao extends CommonDAO implements java.io.Serializable {

	 

	private ArrayList  id;
	
	private ArrayList  milestone;
	
	private ArrayList  milestoneDesc;
	
	private ArrayList  study;
	
	private ArrayList  patient;
	
	private ArrayList  patientCode;
	
	private ArrayList  patprot;
	
	private ArrayList  achievedOn;
	
	private ArrayList  totalAchieved;
	
	private ArrayList  amountDue;
	
	private ArrayList  amountInvoiced;
	
	private ArrayList  amountHoldback;
	
	private Hashtable htAchieved;
	
	private MilestoneDao milestoneDao;
	
	private ArrayList arInvCount;
	
	private ArrayList arPaymentCount;
	
	// Added for FIN-22378
	private ArrayList  patientStudyIds;
	
	private ArrayList  milestonePaymentTypes;
	
	private ArrayList  milestonePaymentFors;
	
	private ArrayList  mileTypes;
	
	private ArrayList  calDescs;
	
	private ArrayList  visitNames;
	
	private ArrayList  coverAnalysis;
	private ArrayList  enrollingSite;
	private ArrayList  patFirstName;
	private ArrayList  patLastName;
	   
    
    public MileAchievedDao() {
		super();
		// TODO Auto-generated constructor stub
		intial();
		milestoneDao = new MilestoneDao();
			}       

    public MileAchievedDao(MileAchievedDao md) {
    		intial();
    		this.setAchievedOn(md.getAchievedOn());
    		this.setAmountDue(md.getAmountDue());
    		this.setAmountInvoiced(md.getAmountInvoiced());
    		this.setAmountHoldback(md.getAmountHoldback());
    		this.setId(md.getId());
    		this.setMilestone(md.getMilestone());
    		this.setMilestoneDesc(md.getMilestoneDesc());
    		this.setPatient(md.getPatient());
    		this.setPatprot(md.getPatprot());
    		this.setStudy(md.getStudy());
    		this.setTotalAchieved(md.getTotalAchieved());
    		this.setPatientCode(md.getPatientCode());
    		this.setArPaymentCount(md.getArPaymentCount() );
    		this.setArInvCount(md.getArInvCount());
    		
    		// Added for FIN-22378
    		this.setPatientStudyIds(md.getPatientStudyIds());
    		this.setMilestonePaymentTypes(md.getMilestonePaymentTypes());
    		this.setMilestonePaymentFors(md.getMilestonePaymentFors());
    		this.setMileTypes(md.getMileTypes());
    		this.setCalDescs(md.getCalDescs());
    		this.setVisitNames(md.getVisitNames());
    		this.setCoverAnalysis(md.getCoverAnalysis());
    		this.setEnrollingSite(md.getEnrollingSite());
    		this.setPatFirstName(md.getPatFirstName());
    		this.setPatLastName(md.getPatLastName());
    		
    		Rlog.debug("milestone",
                    "MileAchievedDao got new obj" + (this.getId()).size());
    		 
		}

    	private void intial()
    	{
    		this.id = new ArrayList ();
    		milestone = new ArrayList ();
    		milestoneDesc = new ArrayList ();
    		study = new ArrayList ();
    		patient = new ArrayList ();
    		patprot = new ArrayList ();
    		achievedOn = new ArrayList ();
    		amountDue = new ArrayList ();
    		amountInvoiced = new ArrayList ();
    		amountHoldback = new ArrayList ();
    		htAchieved = new Hashtable();
    		patientCode = new ArrayList ();
    		arInvCount = new ArrayList();
    		arPaymentCount = new ArrayList();
    		// Added for FIN-22378
    		patientStudyIds = new ArrayList();
    		milestonePaymentTypes = new ArrayList();
    		milestonePaymentFors = new ArrayList();
    		mileTypes = new ArrayList();
    		calDescs = new ArrayList();
    		visitNames = new ArrayList();
    		coverAnalysis = new ArrayList();
    		enrollingSite = new ArrayList();
    		patFirstName = new ArrayList();
    		patLastName = new ArrayList();
    	}
    	
    	/* does not reset htAchieved*/
       	private void reset()
    	{
    		this.id.clear();
    		milestone.clear();
    		milestoneDesc.clear();
    		study.clear();
    		patient.clear();
    		patprot.clear();
    		achievedOn.clear();
    		amountDue.clear();
    		amountInvoiced.clear();
    		amountHoldback.clear();
    		arInvCount.clear();
    		arPaymentCount.clear();
    		
    		// Added for FIN-22378
    		patientStudyIds.clear();
    		milestonePaymentTypes.clear();
    		milestonePaymentFors.clear();
    		mileTypes.clear();
    		calDescs.clear();
    		visitNames.clear();
    		coverAnalysis.clear();
    		enrollingSite.clear();
    		patFirstName.clear();
    		patLastName.clear();
    	}

    	/* removes after index*/
       	private void removeFrom(long index)
    	{
       		int idx = 0;
       		
       		idx = (int) index;
       		
       		Rlog.debug("milestone","index" + idx);
       		Rlog.debug("milestone","id.size()" + id.size());
       		
       		while (id.size() > idx)
       		{
       			
       			Rlog.debug("milestone","old size:" +  id.size());
       			id.remove(idx);
       			

       			if (milestone.size() > idx)
       			{
       				milestone.remove(idx);
       			}	
       			
        		//milestoneDesc.remove(idx);
        		
       			if (study.size() > idx)
       			{
       				study.remove(idx);
       			}
       			
       			if (patient.size() > idx)
       			{
       				patient.remove(idx);
       			}
       			
       			if (patprot.size() > idx)
       			{
       				patprot.remove(idx);
       			}
       			
       			if (achievedOn.size() > idx)
       			{
       				achievedOn.remove(idx);
       			}	
        		
        		if (amountDue.size() > idx)
       			{
       			 	amountDue.remove(idx);
       			}		
        		
        		if (amountInvoiced.size() > idx)
       			{
        			amountInvoiced.remove(idx);
       			}
        		
        		if (amountHoldback.size() > idx)
       			{
        			amountHoldback.remove(idx);
       			}
        		
        		if (arInvCount.size() > idx)
       			{
        			arInvCount.remove(idx);
       			}
        		
        		if (arPaymentCount.size() > idx)
       			{
        			arPaymentCount.remove(idx);
       			}
        		
        		// Added for FIN-22378
        		if (patientStudyIds.size() > idx)
       			{
        			patientStudyIds.remove(idx);
       			}
        		
        		if (milestonePaymentTypes.size() > idx)
       			{
        			milestonePaymentTypes.remove(idx);
       			}
        		
        		if (milestonePaymentFors.size() > idx)
       			{
        			milestonePaymentFors.remove(idx);
       			}
        		
        		Rlog.debug("milestone","new size:" + id.size());
        		
       		}	
    		
    	}

       	

	public MilestoneDao getMilestoneDao() {
			return milestoneDao;
		}

		public void setMilestoneDao(MilestoneDao milestoneDao) {
			this.milestoneDao = milestoneDao;
		}

	public Hashtable getHtAchieved() {
		return htAchieved;
	}





	public void setHtAchieved(Hashtable htAchieved) {
		this.htAchieved = htAchieved;
	}



	public void setHtAchieved(String milestone, MileAchievedDao  mileAchieved) {
		this.htAchieved.put(milestone,mileAchieved);
	}


	public ArrayList getAchievedOn() {
		return achievedOn;
	}





	public void setAchievedOn(ArrayList achievedOn) {
		this.achievedOn = achievedOn;
	}


	public void setAchievedOn(String achievedOn) {
		this.achievedOn.add(achievedOn);
	}



	public ArrayList getAmountDue() {
		return amountDue;
	}





	public void setAmountDue(ArrayList amountDue) {
		this.amountDue = amountDue;
	}


	public void setAmountDue(String amountDue) {
		this.amountDue.add(amountDue);
	}



	public ArrayList getAmountInvoiced() {
		return amountInvoiced;
	}



	public void setAmountInvoiced(ArrayList amountInvoiced) {
		this.amountInvoiced = amountInvoiced;
	}


	public void setAmountInvoiced(String amountInvoiced) {
		this.amountInvoiced.add(amountInvoiced);
	}

	public ArrayList getAmountHoldback() {
		return amountHoldback;
	}

	public void setAmountHoldback(ArrayList amountHoldback) {
		this.amountHoldback = amountHoldback;
	}
    
	public void setAmountHoldback(String amountHoldback) {
		this.amountHoldback.add(amountHoldback);
	}
    


	public ArrayList getId() {
		return id;
	}





	public void setId(ArrayList id) {
		this.id = id;
	}

	public void setId(String id) {
		this.id.add(id);
	}




	public ArrayList getMilestone() {
		return milestone;
	}





	public void setMilestone(ArrayList milestone) {
		this.milestone = milestone;
	}


	public void setMilestone(String milestone) {
		this.milestone.add(milestone);
	}


	public ArrayList getMilestoneDesc() {
		return milestoneDesc;
	}


	public void setMilestoneDesc(ArrayList milestoneDesc) {
		this.milestoneDesc = milestoneDesc;
	}

	public void setMilestoneDesc(String milestoneDesc) {
		this.milestoneDesc.add(milestoneDesc);
	}




	public ArrayList getPatient() {
		return patient;
	}





	public void setPatient(ArrayList patient) {
		this.patient = patient;
	}

	public void setPatient(String patient) {
		this.patient.add(patient);
	}




	public ArrayList getPatprot() {
		return patprot;
	}

	public void setPatprot(ArrayList patprot) {
		this.patprot = patprot;
	}

	public void setPatprot(String patprot) {
		this.patprot.add(patprot);
	}




	public ArrayList getStudy() {
		return study;
	}


	public void setStudy(ArrayList study) {
		this.study = study;
	}

	public void setStudy(String study) {
		this.study.add(study);
	}




	public ArrayList getTotalAchieved() {
		return totalAchieved;
	}





	public void setTotalAchieved(ArrayList totalAchieved) {
		this.totalAchieved = totalAchieved;
	}

	public void setTotalAchieved(String totalAchieved) {
		this.totalAchieved.add(totalAchieved);
	}
	
	
	
	
	// Added for FIN-22378
	public ArrayList getPatientStudyIds() {
		return patientStudyIds;
	}

	public void setPatientStudyIds(ArrayList patientStudyIds) {
		this.patientStudyIds = patientStudyIds;
	}

	public void setPatientStudyIds(String patientStudyId) {
		this.patientStudyIds.add(patientStudyId);
	}
	
	
	
	public ArrayList getMilestonePaymentTypes() {
		return milestonePaymentTypes;
	}

	public void setMilestonePaymentTypes(ArrayList milestonePaymentTypes) {
		this.milestonePaymentTypes = milestonePaymentTypes;
	}

	public void setMilestonePaymentTypes(String milestonePaymentType) {
		this.milestonePaymentTypes.add(milestonePaymentType);
	}
	
	
	
	public ArrayList getMilestonePaymentFors() {
		return milestonePaymentFors;
	}

	public void setMilestonePaymentFors(ArrayList milestonePaymentFors) {
		this.milestonePaymentFors = milestonePaymentFors;
	}

	public void setMilestonePaymentFors(String milestonePaymentFor) {
		this.milestonePaymentFors.add(milestonePaymentFor);
	}
	
	public ArrayList getMileTypes() {
		return mileTypes;
	}

	public void setMileTypes(ArrayList mileTypes) {
		this.mileTypes = mileTypes;
	}

	public void setMileTypes(String mileType) {
		this.mileTypes.add(mileType);
	}

	public ArrayList getCalDescs() {
		return calDescs;
	}

	public void setCalDescs(ArrayList calDescs) {
		this.calDescs = calDescs;
	}

	public void setCalDescs(String calDesc) {
		this.calDescs.add(calDesc);
	}
	
	public ArrayList getVisitNames() {
		return visitNames;
	}

	public void setVisitNames(ArrayList visitNames) {
		this.visitNames = visitNames;
	}

	public void setVisitNames(String visitName) {
		this.visitNames.add(visitName);
	}
	
	public ArrayList getCoverAnalysis() {
		return coverAnalysis;
	}

	public void setCoverAnalysis(ArrayList coverAnalysis) {
		this.coverAnalysis = coverAnalysis;
	}

	public void setCoverAnalysis(String coverAna) {
		this.coverAnalysis.add(coverAna);
	}
	
//

	public ArrayList getEnrollingSite() {
		return enrollingSite;
	}

	public void setEnrollingSite(ArrayList enrollingSite) {
		this.enrollingSite = enrollingSite;
	}
	public void setEnrollingSite(String enrollingSite) {
		this.enrollingSite.add(enrollingSite);
	}

	public ArrayList getPatFirstName() {
		return patFirstName;
	}

	public void setPatFirstName(ArrayList patFirstName) {
		this.patFirstName = patFirstName;
	}
	public void setPatFirstName(String patFirstName) {
		this.patFirstName.add(patFirstName);
	}

	public ArrayList getPatLastName() {
		return patLastName;
	}

	public void setPatLastName(ArrayList patLastName) {
		this.patLastName = patLastName;
	}
	public void setPatLastName(String patLastName) {
		this.patLastName.add(patLastName);
	}

	/**
     * Gets Achieved milestones
     * 
     * @param studyId
     *            study id
     */
    public void getAchievedMilestones(int studyId, String rangeFrom,String rangeTo, String milestoneReceivableStatus,String milestoneType, String site) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlDetail = new StringBuffer();
        String milestone = "";
        String prevMilestone = "";
        MilestoneDao mileDao = new MilestoneDao(); 
         MileAchievedDao mdTemp = new MileAchievedDao();
         String payAbleStatus = "";
         int sitePk = 0;
         
        
        try {
        	
        	sitePk = StringUtil.stringToNum(site);
        	
        	if (StringUtil.isEmpty(milestoneReceivableStatus)) //from payment module, to get info for all types of milestones
        	{
        		
        		payAbleStatus = "";
        	}
        	else
        	{
        		payAbleStatus = "rec"; // for receivable milestones, for invoicing
        		
        	}
           	
            conn = getConnection();
            
            sqlDetail.append("select pk_mileachieved, milestone_count, fk_milestone, nvl(fk_per,0) fk_per , m.fk_study, fk_patprot, to_char(ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT) ach_date, milestone_type,milestone_amount ");
            
            if (! StringUtil.isEmpty(payAbleStatus)) // otherwise we dont need calculated invoice amount
            {	//FIN-21727
            	sqlDetail.append(", nvl((SELECT sum(CASE ")
            	.append("  WHEN (det.DETAIL_TYPE = 'H' AND nvl(milestone_count,1) > 0) THEN (amount_invoiced/milestones_achieved)/nvl(milestone_count,1) ")
            	.append("  WHEN (det.DETAIL_TYPE = 'H' AND nvl(milestone_count,1) <= 0) THEN (amount_invoiced/milestones_achieved)  ")
            	.append("  WHEN det.DETAIL_TYPE = 'D' THEN (amount_invoiced)  END) as amount_invoiced FROM er_invoice_detail det ")
            	.append("  WHERE det.fk_milestone = m.fk_milestone ")
            	.append("  AND ((det.DETAIL_TYPE = 'H' AND NVL(det.fk_mileachieved,0) = 0 AND DISPLAY_DETAIL <> 1) ")
            	.append("  OR (det.DETAIL_TYPE = 'D' AND det.fk_mileachieved = m.pk_mileachieved AND DISPLAY_DETAIL = 1))),0) amount_invoiced " );
            }	
            else
            {
            	sqlDetail.append(", '' amount_invoiced " ); 
            }
            
            if (milestoneType.equals("SM")){
            	sqlDetail.append(", (select to_char(studystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) from er_studystat where pk_studystat=fk_per) study_status_date ");
        	} else {
                sqlDetail.append(", nvl(pkg_patient.f_get_patcodes(fk_per),' ') patfacilityIds");
           	}

            sqlDetail.append(" from er_milestone,er_mileachieved m " );
            
            if (payAbleStatus.equals("rec"))
            {
            	sqlDetail.append(", er_codelst p ");
            }	
            
            sqlDetail.append(" where m.fk_study = ? and pk_milestone = fk_milestone and milestone_achievedcount > 0 and m.is_complete  = 1 and milestone_Type = ?  ");
            
            if (! StringUtil.isEmpty(rangeFrom))
            {
            	sqlDetail.append(" AND TO_DATE(TO_CHAR(ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) BETWEEN TO_DATE(?,PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(?,PKG_DATEUTIL.F_GET_DATEFORMAT) ");
            	
            }
            
            if (payAbleStatus.equals("rec"))
            {
            	sqlDetail.append(" and p.pk_codelst = milestone_paytype and trim(p.codelst_subtyp) = 'rec'  ");
            }	
            
            if (milestoneReceivableStatus.equals("UI"))
            {
            	
            	sqlDetail.append(" and not exists (select * from er_invoice_detail det where det.fk_milestone = pk_milestone and det.fk_mileachieved = pk_mileachieved)  ");
            	
            }
            
            if (sitePk > 0) //add filter for site
            {
            	
            	sqlDetail.append(" and ((fk_per is null) or ( 1 =  pkg_patient.f_is_enrollingsite(fk_per, m.fk_study, ?)  ) ) ");
            	
            }
            
            sqlDetail.append(" order by fk_milestone,pk_mileachieved ");
            
            Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones SQL" + sqlDetail.toString());
            
            pstmt = conn.prepareStatement(sqlDetail.toString());
            pstmt.setInt(1, studyId);
            pstmt.setString(2, milestoneType);
            
            if (! StringUtil.isEmpty(rangeFrom))
            {
            	pstmt.setString(3,rangeFrom );
            	pstmt.setString(4,rangeTo );
            	
            	if (sitePk > 0) //add filter for site
                {
            		pstmt.setInt(5,sitePk);
                }	
            	
            }
            else
            {
            	if (sitePk > 0) //add filter for site
                {
            		pstmt.setInt(3,sitePk);
                }
            	
            }
            

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	
            	milestone = rs.getString("fk_milestone");
            	
            	 
            	
            	if (rows == 0)
            	{
            		prevMilestone = milestone;
            		
            	}
            	
            	if ( ! prevMilestone.equals(milestone) )
            	{
            		//set it to hashtable
            		 Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones adding to hashtable" +prevMilestone);
            		 this.setHtAchieved(prevMilestone, new MileAchievedDao(mdTemp));
            		 mdTemp = new MileAchievedDao();
            		//this.reset(); //clear data arraylists
            		  
            	}
            	
            	 
            	 
            	
            	mdTemp.setId( rs.getString("pk_mileachieved"));
            	mdTemp.setMilestone(milestone);
                mdTemp.setStudy(rs.getString("fk_study"));
            	mdTemp.setPatient(rs.getString("fk_per"));
            	mdTemp.setPatprot(rs.getString("fk_patprot"));
            	mdTemp.setAchievedOn(rs.getString("ach_date"));
            	mdTemp.setAmountDue(rs.getString("milestone_amount"));
            	mdTemp.setAmountInvoiced(rs.getString("amount_invoiced"));
            	//System.out.println("amount-inv.************.."  );
            	//System.out.println("amount-inv" + rs.getString("amount_invoiced"));
            	//System.out.println("amount-inv.....***********........"  );
            	//Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones setAmountInvoiced "+ rs.getString("amount_invoiced") );

            	if (milestoneType.equals("SM")){
            		mdTemp.setPatientCode(rs.getString("study_status_date"));
            	} else {
            		mdTemp.setPatientCode(rs.getString("patfacilityIds"));
               	}

                prevMilestone = milestone;
                
                Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones"
                        + mdTemp.id.size());
                
                rows++;
                
            }
            
            this.setHtAchieved(milestone, new MileAchievedDao(mdTemp));
            mdTemp = new MileAchievedDao();
            
            
            // get milestones for the milestone type and recievable status
            
            mileDao.getMilestoneRows(studyId, milestoneType, payAbleStatus, milestoneReceivableStatus,0);
            
            //iterate throughthe milestones, and calculate the number of milestones achieved for the given date range
            
            ArrayList mileStoneIds = new ArrayList();
            ArrayList milestoneCounts = new ArrayList();
            int mileCount = 0;
            String mileStoneId  = "";
            int countPerMileStone = 0;
            int achDetailCount = 0;
            long totalMilestonesAchieved = 0;
            long calculateAchievedRows = 0;
            
            
            mileStoneIds = mileDao.getMilestoneIds();
            
            mileCount = mileStoneIds.size();
            milestoneCounts = mileDao.getMilestoneCounts ();
            
            for (int i =0; i < mileCount ; i++)
     		{
     		
     			mileStoneId = String.valueOf(((Integer)mileStoneIds.get(i)).intValue());
     			
     			//count required to make one milestone
     			countPerMileStone = StringUtil.stringToNum((String) milestoneCounts.get(i));
     			
    			MileAchievedDao ach = new MileAchievedDao();
    			ArrayList ids = new ArrayList();

    			if (htAchieved.containsKey(mileStoneId))
    			{
    			
    				ach = (MileAchievedDao) htAchieved.get(mileStoneId);
    				ids = ach.getId();
    				achDetailCount = ids.size();
    				
    				if (countPerMileStone > 1 &&  achDetailCount > 0)
    				{
    					//calculate the milestones achieved
    					
    					totalMilestonesAchieved = Math.round(Math.floor(achDetailCount/countPerMileStone));
    					
    					calculateAchievedRows = totalMilestonesAchieved * countPerMileStone;
    					
    				}
    				else
    				{
    					totalMilestonesAchieved = achDetailCount;
    					calculateAchievedRows  = achDetailCount;
    					
    				}
    				
    				// remove extra achieved rows from MileAchieveddao if any
    				
    				if (calculateAchievedRows  < achDetailCount)
    				{
    					 
    					 Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones, have to delete extra rows for milestone: " +mileStoneId + " remove rows: " + (achDetailCount - calculateAchievedRows));
    					 //remove after index calculateAchievedRows
    					 
    					 ach.removeFrom(calculateAchievedRows);
    					 Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones, left: " + (ach.getId()).size());
    					    					  
    		            htAchieved.put(mileStoneId,ach);
    		            Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones, removed extra rows");
    		            	
    				}
    				
    			}
    			else
    			{
    				achDetailCount = 0;
    				totalMilestonesAchieved = 0;
    			}
    			
    			mileDao.setMilestoneAchievedCounts(String.valueOf(totalMilestonesAchieved));
    			
    			
    		    			
     		}
            
            this.setMilestoneDao(mileDao);

            
        } catch (Exception ex) {
            Rlog.fatal("milestone",
                    "MileAchievedDao.getAchievedMilestones EXCEPTION IN FETCHING FROM er_invoice table"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }

	public ArrayList getPatientCode() {
		return patientCode;
	}

	public void setPatientCode(ArrayList patientCode) {
		this.patientCode = patientCode;
	}
	
	public void setPatientCode(String patCode) {
		this.patientCode.add(patCode);
	}
	
	/**
     * Gets Achieved Milestone Details
     * 
     * @param mileAchieveId
     *            PK_MILEACHIEVED
     */
    public void getAchievementDetails() {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlDetail = new StringBuffer();
        try {
        	int mileAchieveId = 0;
        	ArrayList achiveIds = getId();
        	if (null == achiveIds || null == achiveIds.get(0))
        		return;
        	mileAchieveId = StringUtil.stringToNum((String)achiveIds.get(0));
            conn = getConnection();

            sqlDetail.append("select PK_MILEACHIEVED, FK_MILESTONE, NVL(FK_PER,0) FK_PER, FK_STUDY, FK_PATPROT, to_char(ACH_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) as ACH_DATE ");
            sqlDetail.append("from er_mileachieved " );
            sqlDetail.append("where PK_MILEACHIEVED = ? and IS_COMPLETE = 1 ");

            pstmt = conn.prepareStatement(sqlDetail.toString());
            pstmt.setInt(1, mileAchieveId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	setId( rs.getString("PK_MILEACHIEVED"));
            	setMilestone(rs.getString("FK_MILESTONE"));
            	setStudy(rs.getString("FK_STUDY"));
            	setPatient(rs.getString("FK_PER"));
            	setPatprot(rs.getString("FK_PATPROT"));
            	setAchievedOn(rs.getString("ACH_DATE"));                
                rows++;
            }
        } catch (Exception ex) {
            Rlog.fatal("milestone",
                    "MileAchievedDao.getAchievementDetails(mileAchieveId) EXCEPTION :"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }
	
	///////////
	/**
     * Gets Achieved milestones
     * 
     * @param mileStoneId
     *            mileStoneId
     */
    public void getAchievedMilestones(int mileStoneId, String user) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlDetail = new StringBuffer();
        String milestone = "";
        MilestoneJB mileJB = new MilestoneJB();
    	mileJB.setId(mileStoneId);
    	mileJB.getMilestoneDetails();
    	String milestoneType = mileJB.getMilestoneType();
        
        MilestoneDao mileDao = new MilestoneDao(); 
         MileAchievedDao mdTemp = new MileAchievedDao();
         int userPk = 0;
         
         long calculateAchievedRows = 0;
         long countPerMileStone = 0;
        
         long totalMilestonesAchieved = 0;
         
        try {
        	
        	userPk = StringUtil.stringToNum(user) ;
        	
            conn = getConnection();
            if(milestoneType.equals("AM"))
            	sqlDetail.append("select 0 AS pk_mileachieved, milestone_count, pk_milestone, 0 AS fk_per, fk_study, 0 AS fk_patprot, ' ' AS ach_date, milestone_type, milestone_amount ");
            else
            	sqlDetail.append("select pk_mileachieved,milestone_count,  fk_milestone, nvl(fk_per,0) fk_per , m.fk_study, fk_patprot, to_char(ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT) ach_date, milestone_type,milestone_amount ");
            if ((milestoneType.equals("AM")))
            {
            	sqlDetail.append(", ' ' AS patfacilityIds");
            }
            else
            {
            	sqlDetail.append(", CASE WHEN milestone_type = 'SM' THEN ");
            	sqlDetail.append(" nvl((select to_char(studystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) from er_studystat where pk_studystat=fk_per),' ') ");
            	sqlDetail.append(" else nvl((SELECT PATPROT_PATSTDID FROM er_patprot patprot WHERE patprot.fk_per = m.fk_per and patprot.fk_study = m.fk_study and PATPROT_STAT=1),' ') end as patfacilityIds");
            }
            if (milestoneType.equals("AM"))
            {
            	sqlDetail.append(" from er_milestone " );
            }
            else
            {
            	sqlDetail.append(" from er_milestone p,er_mileachieved m " );
            }
            if (milestoneType.equals("AM"))
            {
            	sqlDetail.append(" where pk_milestone = ? and NVL(MILESTONE_DELFLAG,'Z') <> 'Y' and milestone_amount > 0 ");            
            }
            else
            {
            	sqlDetail.append(" where pk_milestone = ? and pk_milestone = fk_milestone and milestone_achievedcount > 0 and m.is_complete = 1 ");
            }
           /* if (userPk > 0) //add filter for site
            {
            	
            	sqlDetail.append(" and ((fk_per is null) or ( 0 <  pkg_user.f_chk_studyright_using_pat(fk_per, p.fk_study, ?)  ) ) ");
            	
            }*/
            
            Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones SQL" + sqlDetail.toString());
            
            pstmt = conn.prepareStatement(sqlDetail.toString());
            pstmt.setInt(1, mileStoneId);
            
        	/*if (userPk > 0) //add filter for site
            {
        		pstmt.setInt(2,userPk);
            }*/	
            
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            if(milestoneType.equals("AM"))
            	milestone = rs.getString("pk_milestone");
            else
            	milestone = rs.getString("fk_milestone");
            	
            
            	mdTemp.setId( rs.getString("pk_mileachieved"));
            	mdTemp.setMilestone(milestone);
                
            	mdTemp.setStudy(rs.getString("fk_study"));
            	mdTemp.setPatient(rs.getString("fk_per"));
            	mdTemp.setPatprot(rs.getString("fk_patprot"));
            	mdTemp.setAchievedOn(rs.getString("ach_date"));
            	mdTemp.setAmountDue(rs.getString("milestone_amount"));
            	
            	mdTemp.setPatientCode(rs.getString("patfacilityIds"));
                
                Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones"
                        + mdTemp.id.size());
                
                rows++;
                
            }
            
            Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones SQL rows" + rows);
            // get milestones for the milestone type and recievable status
            
            mileDao.getMilestoneDetails(mileStoneId);
            
            
            ArrayList achCounts = mileDao.getMilestoneAchievedCounts();
            ArrayList countPerMilestone = mileDao.getMilestoneCounts();
            if(milestoneType.equals("AM"))
            	achCounts.add(0,"1");
            if (achCounts != null && achCounts.size() > 0)
            {
            	totalMilestonesAchieved = StringUtil.stringToNum((String)(achCounts.get(0)));
            	countPerMileStone = StringUtil.stringToNum((String)(countPerMilestone.get(0)));
            }
            
            if (countPerMileStone > 1)
            {
            	calculateAchievedRows =  totalMilestonesAchieved * countPerMileStone;
            }
            else
            {
            	calculateAchievedRows =  totalMilestonesAchieved ;
            	
            }
            Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones SQL calculateAchievedRows" + calculateAchievedRows + "rows" + rows);
           //          remove extra achieved rows from MileAchieveddao if any
			
			if (calculateAchievedRows  < rows)
			{
				 
				 Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones, have to delete extra rows for milestone: " +mileStoneId + " remove rows: " + (rows - calculateAchievedRows));
				 //remove after index calculateAchievedRows
				 
				 mdTemp.removeFrom(calculateAchievedRows);
				 Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones, left: " + (mdTemp.getId()).size());
				    					  
	            Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones, removed extra rows");
	            	
			}
            
		   this.setHtAchieved(milestone, new MileAchievedDao(mdTemp));
	        this.setMilestoneDao(mileDao);

            
        } catch (Exception ex) {
            Rlog.fatal("milestone",
                    "MileAchievedDao.getAchievedMilestones(milestoneId) EXCEPTION :"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }
    
    
    /**
     * Gets Achieved milestones For user
     * 
     * @param studyId
     *            study id
     */
    public void getAchievedMilestonesForUser(int studyId, String rangeFrom,String rangeTo, String milestoneReceivableStatus,String milestoneType, String user) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlDetail = new StringBuffer();
        String milestone = "";
        String prevMilestone = "";
        MilestoneDao mileDao = new MilestoneDao(); 
         MileAchievedDao mdTemp = new MileAchievedDao();
         String payAbleStatus = "";
         int userPk = 0;
        
        try {
        	
        	userPk = StringUtil.stringToNum(user);
        	
        	if (StringUtil.isEmpty(milestoneReceivableStatus)) //from payment module, to get info for all types of milestones
        	{
        		
        		payAbleStatus = "";
        	}
        	else
        	{
        		payAbleStatus = "rec"; // for receivable milestones, for invoicing
        		
        	}
           	
            conn = getConnection();
            if (milestoneType.equals("AM"))
            {
            	sqlDetail.append("select 0 AS pk_mileachieved, milestone_count, pk_milestone, 0 AS fk_per, fk_study, 0 AS fk_patprot, ' ' AS ach_date, milestone_type, milestone_amount ");
            }
            else
            {
            	sqlDetail.append("select pk_mileachieved, fk_milestone, nvl(fk_per,0) fk_per , m.fk_study, fk_patprot, to_char(ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT) ach_date, milestone_type,milestone_amount ");
            }
            if (! StringUtil.isEmpty(payAbleStatus)) // otherwise we dont need calculated invoice amount
            {
            	sqlDetail.append(", nvl((select sum(amount_invoiced) from er_invoice_detail det where det.fk_milestone = m.fk_milestone and det.fk_per = m.fk_per ),0) amount_invoiced ");
            }	
            else
            {
            	sqlDetail.append(", '' amount_invoiced " ); 
            }
            if ((milestoneType.equals("AM")))
            {
            	sqlDetail.append(", ' ' AS patfacilityIds");
            }
            else
            {
            if (milestoneType.equals("SM")){
            	sqlDetail.append(", nvl((select to_char(studystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) from er_studystat where pk_studystat=fk_per),' ') study_status_date ");
        	} else {
        		sqlDetail.append(", nvl((SELECT PATPROT_PATSTDID FROM er_patprot patprot WHERE patprot.fk_per = m.fk_per and patprot.fk_study = m.fk_study and PATPROT_STAT=1),' ') patfacilityIds");
           	}
            }
            if (milestoneType.equals("AM"))
            {
            	sqlDetail.append(" from er_milestone " );
            }
            else
            {
            	sqlDetail.append(" from er_milestone,er_mileachieved m " );
            }
            if (payAbleStatus.equals("rec") || milestoneType.equals("AM"))
            {
            	sqlDetail.append(", er_codelst p ");
            }	
            if (milestoneType.equals("AM"))
            {
            	sqlDetail.append(" where fk_study = ? and NVL(MILESTONE_DELFLAG,'Z') <> 'Y' and milestone_amount > 0 and  milestone_Type = ?  ");            
            }
            else
            {
            	sqlDetail.append(" where m.fk_study = ? and pk_milestone = fk_milestone and milestone_achievedcount > 0 and m.is_complete = 1 and  milestone_Type = ?  ");
            }
            if (! StringUtil.isEmpty(rangeFrom))
            {
            	sqlDetail.append(" AND TO_DATE(TO_CHAR(ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) BETWEEN TO_DATE(?,PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(?,PKG_DATEUTIL.F_GET_DATEFORMAT) ");
            	
            }
            
            if (payAbleStatus.equals("rec"))
            {
            	sqlDetail.append(" and p.pk_codelst = milestone_paytype and trim(p.codelst_subtyp) = 'rec'  ");
            }	
            if (milestoneType.equals("AM"))
            {
            	sqlDetail.append(" and p.pk_codelst = fk_codelst_milestone_stat and trim(p.codelst_subtyp) = 'A'  ");
            }
            if (milestoneReceivableStatus.equals("UI"))
            {
            	
            	sqlDetail.append(" and not exists (select * from er_invoice_detail det where det.fk_milestone = pk_milestone)  ");
            	
            }
            
            /*if (userPk > 0) //add filter for site
            {
            	
            	sqlDetail.append(" and ((fk_per is null) or ( 0 <  pkg_user.f_chk_studyright_using_pat(fk_per, m.fk_study, ?)  ) ) ");
            	
            }*/
            if (milestoneType.equals("AM"))
            {
            	sqlDetail.append(" order by pk_milestone ");
            }
            else
            {
            	 sqlDetail.append(" order by fk_milestone ");
            }
            
            Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestonesForUser SQL" + sqlDetail.toString());
            
            pstmt = conn.prepareStatement(sqlDetail.toString());
            pstmt.setInt(1, studyId);
            pstmt.setString(2, milestoneType);
            
            if (! StringUtil.isEmpty(rangeFrom))
            {
            	pstmt.setString(3,rangeFrom );
            	pstmt.setString(4,rangeTo );
            	
            	/*if (userPk > 0) //add filter for site
                {
            		pstmt.setInt(5,userPk);
                }*/	
            	
            }
            else
            {
            	/*if (userPk > 0) //add filter for site
                {
            		pstmt.setInt(3,userPk);
                }*/
            	
            }
            

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	if (milestoneType.equals("AM"))
            		milestone = rs.getString("pk_milestone");
            	else
            		milestone = rs.getString("fk_milestone");
            	
            	if (rows == 0)
            	{
            		prevMilestone = milestone;
            		
            	}
            	
            	if ( ! prevMilestone.equals(milestone) )
            	{
            		//set it to hashtable
            		 Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestonesForUser adding to hashtable" +prevMilestone);
            		 this.setHtAchieved(prevMilestone, new MileAchievedDao(mdTemp));
            		 mdTemp = new MileAchievedDao();
            		//this.reset(); //clear data arraylists
            	}
            		
            	
            	mdTemp.setId( rs.getString("pk_mileachieved"));
            	mdTemp.setMilestone(milestone);
                
            	mdTemp.setStudy(rs.getString("fk_study"));
            	mdTemp.setPatient(rs.getString("fk_per"));
            	mdTemp.setPatprot(rs.getString("fk_patprot"));
            	mdTemp.setAchievedOn(rs.getString("ach_date"));
            	mdTemp.setAmountDue(rs.getString("milestone_amount"));
            	mdTemp.setAmountInvoiced(rs.getString("amount_invoiced"));
            	Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestonesForUser setAmountInvoiced "+ rs.getString("amount_invoiced") );
            	
            	if (milestoneType.equals("SM")){
            		mdTemp.setPatientCode(rs.getString("study_status_date"));
            	} else {
            		mdTemp.setPatientCode(rs.getString("patfacilityIds"));
               	}

                prevMilestone = milestone;
                
                Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestonesForUser"
                        + mdTemp.id.size());
                
                rows++;
                
            }
            
            this.setHtAchieved(milestone, new MileAchievedDao(mdTemp));
            mdTemp = new MileAchievedDao();
            
            
            // get milestones for the milestone type and recievable status
            
            mileDao.getMilestoneRows(studyId, milestoneType, payAbleStatus, milestoneReceivableStatus, 0);
            
            //iterate throughthe milestones, and calculate the number of milestones achieved for the given date range
            
            ArrayList mileStoneIds = new ArrayList();
            ArrayList milestoneCounts = new ArrayList();
            int mileCount = 0;
            String mileStoneId  = "";
            int countPerMileStone = 0;
            int achDetailCount = 0;
            long totalMilestonesAchieved = 0;
            long calculateAchievedRows = 0;
            
            mileStoneIds = mileDao.getMilestoneIds();
            
            mileCount = mileStoneIds.size();
            milestoneCounts = mileDao.getMilestoneCounts ();
            
            for (int i =0; i < mileCount ; i++)
     		{
     		
     			mileStoneId = String.valueOf(((Integer)mileStoneIds.get(i)).intValue());
     			
     			//count required to make one milestone
     			countPerMileStone = StringUtil.stringToNum((String) milestoneCounts.get(i));
     			
    			MileAchievedDao ach = new MileAchievedDao();
    			ArrayList ids = new ArrayList();

    			if (htAchieved.containsKey(mileStoneId))
    			{
    			
    				ach = (MileAchievedDao) htAchieved.get(mileStoneId);
    				ids = ach.getId();
    				achDetailCount = ids.size();
    				
    				if (countPerMileStone > 1 &&  achDetailCount > 0)
    				{
    					//calculate the milestones achieved
    					
    					totalMilestonesAchieved = Math.round(Math.floor(achDetailCount/countPerMileStone));
    					calculateAchievedRows = totalMilestonesAchieved * countPerMileStone;

    					
    				}
    				else
    				{
    					totalMilestonesAchieved = achDetailCount;
    					calculateAchievedRows  = achDetailCount;
    				}
    				
    			}
    			else
    			{
    				achDetailCount = 0;
    				totalMilestonesAchieved = 0;
    			}
    			
    			//    			 remove extra achieved rows from MileAchieveddao if any
				
				if (calculateAchievedRows  < achDetailCount)
				{
					 
					 Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones, have to delete extra rows for milestone: " +mileStoneId + " remove rows: " + (achDetailCount - calculateAchievedRows));
					 //remove after index calculateAchievedRows
					 
					 ach.removeFrom(calculateAchievedRows);
					 Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones, left: " + (ach.getId()).size());
					    					  
		            htAchieved.put(mileStoneId,ach);
		            Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones, removed extra rows");
		            	
				}
				
    			mileDao.setMilestoneAchievedCounts(String.valueOf(totalMilestonesAchieved));
    			
    			
    		    			
     		}
            
            this.setMilestoneDao(mileDao);

            
        } catch (Exception ex) {
            Rlog.fatal("milestone",
                    "MileAchievedDao.getAchievedMilestonesForUser EXCEPTION IN FETCHING FROM er_invoice table"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }
    
    /**
     * 
     * @author Sonia Abrol
     * date - 08/26/08
     *  
     * Gets milestone Achievemnt details
     * 
     * @param studyId
     *            study id
     * @param   milestoneType milestone type code
     * @param htParams ashtable of additional parameters (for later use)        
     */
    public void getALLAchievedMilestones(int studyId, String milestoneType , Hashtable htParams) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlDetail = new StringBuffer();
        String milestone = "";
        String prevMilestone = "";
        String mileDesc = "";
        String prevMileDesc = "";
        String prevAchCount = "";
        String achCount = "";
        String prevCount = "";
        String count = "";
        String mileAmount = "";
        String prevMileAmount = "";
        
        
          
         MileAchievedDao mdTemp = new MileAchievedDao();
          
        
        try {
        	
        	 
            conn = getConnection();
            
            sqlDetail.append("select pk_mileachieved, milestone_count, pk_milestone, nvl(fk_per,0) fk_per , m.fk_study, fk_patprot, to_char(ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT) ach_date, milestone_type,milestone_amount ");
            
            	sqlDetail.append(" ,nvl((select patprot_patstdid from er_patprot ep where ep.fk_per = m.fk_per and ep.FK_STUDY = m.FK_STUDY and PATPROT_STAT=1),' ') patfacilityIds, ");
            sqlDetail.append(" (select count(*) from er_invoice_detail invd where invd.fk_milestone = pk_milestone" 
            	+ " AND ((DISPLAY_DETAIL <> 1)"
            	+ " OR (DISPLAY_DETAIL = 1 AND (nvl(invd.fk_mileachieved , 0) > 0 and invd.fk_mileachieved = pk_mileachieved)))) inv_count ,");

            sqlDetail.append("(select count(*) from er_milepayment_details where fk_mileachieved = pk_mileachieved and nvl(mp_amount,0) > 0 ) pay_count, Pkg_Milestone_New.f_getMilestoneDesc( PK_MILESTONE) mileDesc,milestone_achievedcount ");
            sqlDetail.append(", (decode(?,'SM',(select to_char(studystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) from er_studystat where pk_studystat=fk_per ),' ')) study_status_date ");
            
            sqlDetail.append(" from er_milestone,er_mileachieved m " );
            
            //KM-#D-FIN7
            sqlDetail.append(" where er_milestone.fk_study = ? and m.fk_milestone (+)= pk_milestone  and milestone_Type = ? and fk_codelst_milestone_stat = (select pk_codelst from er_codelst where codelst_type ='milestone_stat' and codelst_subtyp='A') ");
            
            sqlDetail.append(" and NVL(MILESTONE_DELFLAG,'Z') !='Y' ");
            
            sqlDetail.append(" order by pk_milestone,ach_date,pk_mileachieved ");
            
            Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones SQL" + sqlDetail.toString());
            
            pstmt = conn.prepareStatement(sqlDetail.toString());
            
            pstmt.setString(1, milestoneType);
            pstmt.setInt(2, studyId);
            pstmt.setString(3, milestoneType);


            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	
            	milestone = rs.getString("pk_milestone");
            	mileDesc= rs.getString("mileDesc");
            	achCount =  rs.getString("milestone_achievedcount");
            	mileAmount =  rs.getString("milestone_amount");
            	count = rs.getString("milestone_count");
            	
            	if (rows == 0)
            	{
            		prevMilestone = milestone;
            		prevMileDesc = mileDesc;
            		prevAchCount = achCount;
            		prevMileAmount = mileAmount;
            		prevCount = count ;
            	}
            	
            	if ( ! prevMilestone.equals(milestone) )
            	{
            		//set it to hashtable
            		  
            		 this.setHtAchieved(prevMilestone, new MileAchievedDao(mdTemp));
            		 this.setMilestone(prevMilestone);
            		 this.setMilestoneDesc(prevMileDesc);
            		 milestoneDao.setMilestoneAchievedCounts(prevAchCount);
            		 milestoneDao.setMilestoneAmounts(prevMileAmount);
            		 milestoneDao.setMilestoneCounts(prevCount);
            		 
            		 mdTemp = new MileAchievedDao();
            		//this.reset(); //clear data arraylists
            		  
            	}
            	
            	 
            	 
            	
            	mdTemp.setId( rs.getString("pk_mileachieved"));
            	mdTemp.setMilestone(milestone);
                mdTemp.setStudy(rs.getString("fk_study"));
            	mdTemp.setPatient(rs.getString("fk_per"));
            	mdTemp.setPatprot(rs.getString("fk_patprot"));
            	mdTemp.setAchievedOn(rs.getString("ach_date"));
            	//mdTemp.setAmountDue(rs.getString("milestone_amount"));
             
            	if (milestoneType.equals("SM"))
            	{
            		mdTemp.setPatientCode(rs.getString("study_status_date"));
            	}
            	else
            	{
            		mdTemp.setPatientCode(rs.getString("patfacilityIds"));
               	}
            	
            	mdTemp.setArInvCount(new Integer(rs.getInt("inv_count")));
            	mdTemp.setArPaymentCount(new Integer(rs.getInt("pay_count")));
            	 
               
                prevMilestone = milestone;
                prevAchCount = achCount;
                prevMileDesc = mileDesc;
                prevMileAmount = mileAmount;
                prevCount = count; 
                
                Rlog.debug("milestone", "MileAchievedDao.getALLAchievedMilestones"
                        + mdTemp.id.size());
                
                rows++;
                
            }
            
            
            
            if (rows > 0)
            {
            	this.setHtAchieved(milestone, new MileAchievedDao(mdTemp));
                this.setMilestone(milestone);
                this.setMilestoneDesc(mileDesc);
                
            	milestoneDao.setMilestoneAchievedCounts(achCount);
            	milestoneDao.setMilestoneAmounts(mileAmount);
            	milestoneDao.setMilestoneCounts(count);
            }
            
            mdTemp = new MileAchievedDao();
              
            //System.out.println("end of method");
        } catch (Exception ex) {
            Rlog.fatal("milestone",
                    "MileAchievedDao.getALLAchievedMilestones EXCEPTION IN FETCHING data"
                            + ex );
            ex.printStackTrace();
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }

	public ArrayList getArInvCount() {
		return arInvCount;
	}

	public void setArInvCount(ArrayList arInvCount) {
		this.arInvCount = arInvCount;
	}
	
	public void setArInvCount(Integer invCount) {
		this.arInvCount.add(invCount);
	}

	public ArrayList getArPaymentCount() {
		return arPaymentCount;
	}

	public void setArPaymentCount(ArrayList arPaymentCount) {
		this.arPaymentCount = arPaymentCount;
	}

	public void setArPaymentCount(Integer paymentCount) {
		this.arPaymentCount.add(paymentCount);
	}
	/**
     * Check Achieved milestones
     * @param mileStoneId, studyId
     * 16Mar2011 @Ankit #5924
     */
    public boolean checkAchievedMilestones(int mileStoneId, int studyId) {
        
    	PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlDetail = new StringBuffer();
        String milestone = "";
        boolean flage = false;
        try {
        	
        	conn = getConnection();
            
            sqlDetail.append("select pk_mileachieved,milestone_count,  fk_milestone, nvl(fk_per,0) fk_per , m.fk_study, fk_patprot, to_char(ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT) ach_date, milestone_type,milestone_amount ");
            sqlDetail.append(", CASE WHEN milestone_type = 'SM' THEN ");
           	sqlDetail.append(" (select to_char(studystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) from er_studystat where pk_studystat=fk_per) ");
            sqlDetail.append(" else nvl(pkg_patient.f_get_patcodes(fk_per),' ') end as patfacilityIds");
            sqlDetail.append(" from er_milestone p,er_mileachieved m " );
            sqlDetail.append(" where pk_milestone = ? and pk_milestone = fk_milestone and m.fk_study = ? ");
            
            Rlog.debug("milestone", "MileAchievedDao.checkAchievedMilestones SQL" + sqlDetail.toString());
            
            pstmt = conn.prepareStatement(sqlDetail.toString());
            pstmt.setInt(1, mileStoneId);
            pstmt.setInt(2, studyId);
        	
            ResultSet rs = pstmt.executeQuery();

            if(rs.next()) {
            	flage = true;
            }
            
            Rlog.debug("milestone", "MileAchievedDao.checkAchievedMilestones SQL rows" + flage);
            
        } catch (Exception ex) {
            Rlog.fatal("milestone",
                    "MileAchievedDao.checkAchievedMilestones(milestoneId,StudyId) EXCEPTION :"
                            + ex);
           return flage;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
        return flage;
    }

    // Added for FIN-22378(Start)
    /**
     * FIN-20599 ::: Raviesh
     * Gets Achieved Event Milestones and Additional Milestones
     * @param CovType
     * @param monthFilterYear 
     * @param monthFilter 
     * @param yearFilter 
     * @param dateRangeType 
     */
    public void getAchievedMilestonesEM(int studyId, String rangeFrom,String rangeTo, String milestoneReceivableStatus, String site, String CovType, String yearFilter, String monthFilter, String monthFilterYear, String dateRangeType) {
    	
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlDetail = new StringBuffer();
        String milestone = "";
        String prevMilestone = "";
        MilestoneDao mileDao = new MilestoneDao(); 
         MileAchievedDao mdTemp = new MileAchievedDao();
         String payAbleStatus = "";
         int sitePk = 0;
         
        
        try {
        	
        	sitePk = StringUtil.stringToNum(site);
        	
        	if (StringUtil.isEmpty(milestoneReceivableStatus)) //from payment module, to get info for all types of milestones
        	{
        		payAbleStatus = "";
        	}
        	else
        	{
        		payAbleStatus = "rec"; // for receivable milestones, for invoicing
        	}
           	
            conn = getConnection();
            
            sqlDetail.append("select pk_mileachieved, milestone_count, fk_milestone, nvl(fk_per,0) fk_per ,")
            		.append(" (select PERSON_FNAME from PERSON where PK_PERSON=m.fk_per) patFirstName," )
       				.append(" (select PERSON_LNAME from PERSON where PK_PERSON=m.fk_per) patLastName, ")
       				.append(" (select SITE_NAME from ER_SITE where PK_SITE=(select PER_SITE from erv_studypat_by_visit erv , erv_person p where erv.fk_study  = "+studyId+" and P.pk_person = erv.fk_per and PK_PERSON=m.fk_per) ) enrollingSite,")
            		.append(" m.fk_study, m.fk_patprot, to_char(ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT) ach_date, milestone_type,milestone_amount ");
            
            if (! StringUtil.isEmpty(payAbleStatus)) // otherwise we don't need calculated invoice amount
            {	//FIN-21727
            	sqlDetail.append(", nvl((SELECT sum(CASE ")
            	.append("  WHEN (det.DETAIL_TYPE = 'H' AND milestone_count > 0) THEN (amount_invoiced/milestones_achieved)/milestone_count ")
            	.append("  WHEN (det.DETAIL_TYPE = 'H' AND milestone_count <= 0) THEN (amount_invoiced/milestones_achieved)  ")
            	.append("  WHEN det.DETAIL_TYPE = 'D' THEN (amount_invoiced)  END) as amount_invoiced FROM er_invoice_detail det, er_invoice inv ")
            	.append("  WHERE det.fk_milestone = m.fk_milestone ")
            	.append("  AND inv.pk_invoice = det.fk_inv ")
            	.append("  AND ((det.DETAIL_TYPE = 'H' AND NVL(det.fk_mileachieved,0) = 0 AND DISPLAY_DETAIL <> 1) ")
            	.append("  OR (det.DETAIL_TYPE = 'D' AND det.fk_mileachieved = m.pk_mileachieved AND DISPLAY_DETAIL = 1))),0) amount_invoiced " );
            	
            	sqlDetail.append(", nvl((SELECT sum(CASE ")
            	.append("  WHEN (det.DETAIL_TYPE = 'H' AND milestone_count > 0) THEN (amount_holdback/milestones_achieved)/milestone_count ")
            	.append("  WHEN (det.DETAIL_TYPE = 'H' AND milestone_count <= 0) THEN (amount_holdback/milestones_achieved)  ")
            	.append("  WHEN det.DETAIL_TYPE = 'D' THEN (amount_holdback)  END) as amount_holdback FROM er_invoice_detail det, er_invoice inv ")
            	.append("  WHERE det.fk_milestone = m.fk_milestone ")
            	.append("  AND inv.pk_invoice = det.fk_inv ")
            	.append("  AND ((det.DETAIL_TYPE = 'H' AND NVL(det.fk_mileachieved,0) = 0 AND DISPLAY_DETAIL <> 1) ")
            	.append("  OR (det.DETAIL_TYPE = 'D' AND det.fk_mileachieved = m.pk_mileachieved AND DISPLAY_DETAIL = 1))),0) amount_holdback " );
            }	
            else
            {
            	sqlDetail.append(", '' amount_invoiced " ); 
            	sqlDetail.append(", '' amount_holdback " );
            }
            
            sqlDetail.append(" ,nvl(pkg_patient.f_get_patcodes(fk_per),' ') patfacilityIds");
            
            // Added for FIN-22378(Start)
            sqlDetail.append(",(select clst.codelst_desc from er_codelst clst where clst.codelst_type = 'milestone_type' and codelst_subtyp = milestone_type) miletype  ");
            sqlDetail.append(",nvl((select patprot_patstdid from er_patprot pat where pat.fk_study = m.fk_study and pat.fk_per = m.fk_per and patprot_stat = 1),'-')patstudy_id  ");
            sqlDetail.append(",nvl((select clst.codelst_desc from er_codelst clst where clst.pk_codelst = milestone_paytype),'-') paytype  ");
            sqlDetail.append(",nvl((select clst.codelst_desc from er_codelst clst where clst.pk_codelst = milestone_payfor),'-') payfor  ");
            
            sqlDetail.append(",nvl((select cal.name from event_assoc cal where cal.event_id = fk_cal),'-') cal_desc ");
            sqlDetail.append(",nvl((select v.visit_name from sch_protocol_visit  v where a.fk_visit = v.pk_protocol_visit ),'-') visit_name  ");
            sqlDetail.append("," +
        		" CASE WHEN a.FK_EVENTASSOC in (select s.fk_assoc from sch_events1 s " +
        		" where ((m.FK_PATPROT IS NULL AND s.FK_PATPROT IS NULL) OR (m.FK_PATPROT IS NOT NULL and (m.FK_PATPROT = s.FK_PATPROT)))) " +
        		" THEN (select s.fk_codelst_covertype from sch_events1 s " +
        		" where a.FK_EVENTASSOC = s.fk_assoc" +
        		" and ((m.FK_PATPROT IS NULL AND s.FK_PATPROT IS NULL) OR (m.FK_PATPROT IS NOT NULL and (m.FK_PATPROT = s.FK_PATPROT))) " +
            	" )" +
        		" ELSE 0 END FK_CODELST_COVERTYPE ");
            // Added for FIN-22378(End)
            
            sqlDetail.append(" from er_milestone a,er_mileachieved m, event_Assoc ev " );
            
            if (payAbleStatus.equals("rec"))
            {
            	sqlDetail.append(", er_codelst p ");
            }	
           
            sqlDetail.append(" where m.fk_study = ? and pk_milestone = fk_milestone and milestone_achievedcount > 0 and m.is_complete  = 1 and milestone_Type = 'EM' ");
            sqlDetail.append(" and (a.FK_EVENTASSOC = ev.event_id) ");
			sqlDetail.append(" and a.milestone_amount > 0 and NVL(a.MILESTONE_DELFLAG,'Z') <> 'Y' ");

			 if(dateRangeType!=null && !dateRangeType.equals("") && dateRangeType.equals("DR")){
	            	if (! StringUtil.isEmpty(rangeFrom))
	                {
	            		sqlDetail.append(" AND TO_DATE(TO_CHAR(ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) BETWEEN TO_DATE(?,PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(?,PKG_DATEUTIL.F_GET_DATEFORMAT) ");
	                	
	                }
	            }else if(dateRangeType!=null && !dateRangeType.equals("") && dateRangeType.equals("M")){
	            	sqlDetail.append(" AND to_char(ach_date, 'MM')=? ");
	            	sqlDetail.append(" AND to_char(ach_date, 'YYYY')=? ");
	            }else if(dateRangeType!=null && !dateRangeType.equals("") && dateRangeType.equals("Y"))	{
	            	sqlDetail.append(" AND to_char(ach_date, 'YYYY')=? ");
	            }
	            
            
            if (payAbleStatus.equals("rec"))
            {
            	sqlDetail.append(" and p.pk_codelst = milestone_paytype and trim(p.codelst_subtyp) = 'rec'  ");
            }	
            
            if (milestoneReceivableStatus.equals("UI"))
            {
            	sqlDetail.append(" and not exists (select * from er_invoice_detail det where det.fk_milestone = pk_milestone and det.fk_mileachieved = pk_mileachieved)  ");
            }
            
            if (sitePk > 0) //add filter for site
            {
            	sqlDetail.append(" and ((fk_per is null) or ( 1 =  pkg_patient.f_is_enrollingsite(fk_per, m.fk_study, ?)  ) ) ");
            }
            sqlDetail.append(" and a.FK_CODELST_MILESTONE_STAT = (SELECT pk_codelst FROM er_codelst WHERE CODELST_TYPE = 'milestone_stat' AND CODELST_SUBTYP = 'A') ");
            
            StringBuffer tempSqlDetail = new StringBuffer();
            tempSqlDetail.append("select oPTable.*, ")
	    		.append("DECODE(FK_CODELST_COVERTYPE, 0, '-', (select trim(CODELST_SUBTYP) from sch_codelst where pk_codelst = FK_CODELST_COVERTYPE)) as coverage_type ")
	    		.append("from (")
	    		.append(sqlDetail)
	    		.append(") oPTable ");

            if(null!=CovType && !CovType.equalsIgnoreCase("All")){
            	tempSqlDetail.append(" WHERE FK_CODELST_COVERTYPE IN ("+CovType+")");;
            }

            tempSqlDetail.append(" order by fk_milestone,pk_mileachieved ");
            
            Rlog.fatal("milestone", "MileAchievedDao.getAchievedMilestonesEM SQL" + sqlDetail.toString());
            
            pstmt = conn.prepareStatement(tempSqlDetail.toString());
            int paramSeq = 1;
            pstmt.setInt(paramSeq, studyId); paramSeq++;
            
            if(dateRangeType!=null && !dateRangeType.equals("") && dateRangeType.equals("DR")){
            	if (! StringUtil.isEmpty(rangeFrom))
                {
                	pstmt.setString(paramSeq,rangeFrom );paramSeq++;
                	pstmt.setString(paramSeq,rangeTo );paramSeq++;
                }
            }else if(dateRangeType!=null && !dateRangeType.equals("") && dateRangeType.equals("M")){
            	pstmt.setInt(paramSeq,StringUtil.stringToInteger(monthFilter));paramSeq++;
            	pstmt.setInt(paramSeq,StringUtil.stringToInteger(monthFilterYear));paramSeq++;

            }else if(dateRangeType!=null && !dateRangeType.equals("") && dateRangeType.equals("Y"))	{
            	pstmt.setInt(paramSeq,StringUtil.stringToInteger(yearFilter));paramSeq++;
            }
            if (sitePk > 0) //add filter for site
            {
            	pstmt.setInt(paramSeq,sitePk);
            }

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	
            	milestone = rs.getString("fk_milestone");
            	
            	if (rows == 0)
            	{
            		prevMilestone = milestone;
            	}
            	
            	if ( ! prevMilestone.equals(milestone) )
            	{
            		//set it to hashtable
            		 Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestonesEM adding to hashtable" +prevMilestone);
            		 this.setHtAchieved(prevMilestone, new MileAchievedDao(mdTemp));
            		 mdTemp = new MileAchievedDao();
            		//this.reset(); //clear data arraylists
            	}

            	mdTemp.setId( rs.getString("pk_mileachieved"));
            	mdTemp.setMilestone(milestone);
                mdTemp.setStudy(rs.getString("fk_study"));
            	mdTemp.setPatient(rs.getString("fk_per"));
            	mdTemp.setPatFirstName(rs.getString("patFirstName"));
            	mdTemp.setPatLastName(rs.getString("patLastName"));
            	mdTemp.setEnrollingSite(rs.getString("enrollingSite"));
            	
            	mdTemp.setPatprot(rs.getString("fk_patprot"));
            	mdTemp.setAchievedOn(rs.getString("ach_date"));
            	mdTemp.setAmountDue(rs.getString("milestone_amount"));
            	mdTemp.setAmountInvoiced(rs.getString("amount_invoiced"));
            	mdTemp.setAmountHoldback(rs.getString("amount_holdback"));
            	
            	mdTemp.setPatientCode("-");
            	
            	// Added for FIN-22378
            	mdTemp.setPatientStudyIds(rs.getString("patstudy_id"));
            	mdTemp.setMilestonePaymentTypes(rs.getString("paytype"));
            	mdTemp.setMilestonePaymentFors(rs.getString("payfor"));
            	mdTemp.setMileTypes(rs.getString("miletype"));
            	
            	mdTemp.setCalDescs(rs.getString("cal_desc"));
            	mdTemp.setVisitNames(rs.getString("visit_name"));
            	mdTemp.setCoverAnalysis(rs.getString("coverage_type"));
            	
                prevMilestone = milestone;
                
                Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestonesEM"
                        + mdTemp.id.size());
                
                rows++;
                
            }
            
            this.setHtAchieved(milestone, new MileAchievedDao(mdTemp));
            mdTemp = new MileAchievedDao();
            
            
            // get milestones for the milestone type,receivable status and Coverage Type
           	mileDao.getMilestoneRowsEM(studyId, payAbleStatus, milestoneReceivableStatus , CovType, 1);            
            
            //iterate throughthe milestones, and calculate the number of milestones achieved for the given date range
            ArrayList mileStoneIds = new ArrayList();
            ArrayList milestoneCounts = new ArrayList();
            int mileCount = 0;
            String mileStoneId  = "";
            int countPerMileStone = 0;
            int achDetailCount = 0;
            long totalMilestonesAchieved = 0;
            long calculateAchievedRows = 0;
            
            
            mileStoneIds = mileDao.getMilestoneIds();
            
            mileCount = mileStoneIds.size();
            milestoneCounts = mileDao.getMilestoneCounts ();
            
            for (int i =0; i < mileCount ; i++)
     		{
     		
     			mileStoneId = String.valueOf(((Integer)mileStoneIds.get(i)).intValue());
     			
     			//count required to make one milestone
     			countPerMileStone = StringUtil.stringToNum((String) milestoneCounts.get(i));
     			
    			MileAchievedDao ach = new MileAchievedDao();
    			ArrayList ids = new ArrayList();

    			if (htAchieved.containsKey(mileStoneId))
    			{
    			
    				ach = (MileAchievedDao) htAchieved.get(mileStoneId);
    				ids = ach.getId();
    				achDetailCount = ids.size();
    				
    				if (countPerMileStone > 1 &&  achDetailCount > 0)
    				{
    					//calculate the milestones achieved
    					totalMilestonesAchieved = Math.round(Math.floor(achDetailCount/countPerMileStone));
    					calculateAchievedRows = totalMilestonesAchieved * countPerMileStone;
    				}
    				else
    				{
    					totalMilestonesAchieved = achDetailCount;
    					calculateAchievedRows  = achDetailCount;
    				}
    				
    				// remove extra achieved rows from MileAchieveddao if any
    				
    				if (calculateAchievedRows  < achDetailCount)
    				{
    					 Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestonesEM, have to delete extra rows for milestone: " +mileStoneId + " remove rows: " + (achDetailCount - calculateAchievedRows));
    					 //remove after index calculateAchievedRows
    					 
    					 ach.removeFrom(calculateAchievedRows);
    					 Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestonesEM, left: " + (ach.getId()).size());
    					    					  
    					 htAchieved.put(mileStoneId,ach);
    					 Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestonesEM, removed extra rows");
    		            	
    				}
    				
    			}
    			else
    			{
    				achDetailCount = 0;
    				totalMilestonesAchieved = 0;
    			}
    			
    			mileDao.setMilestoneAchievedCounts(String.valueOf(totalMilestonesAchieved));
    			
     		}
            
            this.setMilestoneDao(mileDao);

            
        } catch (Exception ex) {
            Rlog.fatal("milestone",
                    "MileAchievedDao.getAchievedMilestonesEM EXCEPTION IN FETCHING FROM er_invoice table"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }

    public void getAchievedMilestonesAM(int studyId, String rangeFrom,String rangeTo, String milestoneReceivableStatus, String site , String CovType, String yearFilter, String monthFilter, String monthFilterYear, String dateRangeType) {
    	
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer achievements = new StringBuffer();
        StringBuffer patientRelAchievements = new StringBuffer();
        StringBuffer nonPatientRelAchievements = new StringBuffer();
        String milestone = "";
        String prevMilestone = "";
        MilestoneDao mileDao = new MilestoneDao(); 
         MileAchievedDao mdTemp = new MileAchievedDao();
         String payAbleStatus = "";
         int sitePk = 0;
         
        
        try {
        	
        	sitePk = StringUtil.stringToNum(site);
        	
        	if (StringUtil.isEmpty(milestoneReceivableStatus)) //from payment module, to get info for all types of milestones
        	{        		
        		payAbleStatus = "";
        	}
        	else
        	{
        		payAbleStatus = "rec"; // for receivable milestones, for invoicing
        	}
           	
            conn = getConnection();
            
            /*** Patient related Additional Milestones (Created by adding unscheduled events to patient schedule) ***/
            
            patientRelAchievements.append("select 0 as pk_mileachieved, milestone_count, pk_milestone, ");
            patientRelAchievements.append("nvl(To_number(sch.patient_id),0) fk_per ," )
    			.append(" (select PERSON_FNAME from PERSON where PK_PERSON=sch.patient_id) patFirstName," )
				.append(" (select PERSON_LNAME from PERSON where PK_PERSON=sch.patient_id) patLastName, ")
				.append(" (select SITE_NAME from ER_SITE where PK_SITE=(select PER_SITE from erv_studypat_by_visit erv , erv_person p where erv.fk_study  = "+studyId+" and P.pk_person = erv.fk_per and PK_PERSON=sch.patient_id) ) enrollingSite,")		
            	.append(" a.fk_study, sch.fk_patprot fk_patprot, ");
            patientRelAchievements.append("to_char(sch.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT) ach_date, milestone_type,milestone_amount ");

            if (! StringUtil.isEmpty(payAbleStatus)) // otherwise we don't need calculated invoice amount
            {	//FIN-21727
            	patientRelAchievements.append(", nvl((SELECT sum(amount_invoiced) as amount_invoiced FROM er_invoice_detail det, er_invoice inv ")
            	.append("  WHERE det.fk_milestone = pk_milestone ")
            	.append("  AND inv.pk_invoice = det.fk_inv ")
            	.append("  AND det.DETAIL_TYPE = 'H' AND NVL(det.fk_mileachieved,0) = 0),0) amount_invoiced " );
            	
            	patientRelAchievements.append(", nvl((SELECT sum(amount_holdback) as amount_holdback FROM er_invoice_detail det, er_invoice inv ")
            	.append("  WHERE det.fk_milestone = pk_milestone ")
            	.append("  AND inv.pk_invoice = det.fk_inv ")
            	.append("  AND det.DETAIL_TYPE = 'H' AND NVL(det.fk_mileachieved,0) = 0),0) amount_holdback " );
            }	
            else
            {
            	patientRelAchievements.append(", '' amount_invoiced " ); 
            	patientRelAchievements.append(", '' amount_holdback " );
            }
            
            patientRelAchievements.append(" ,nvl(pkg_patient.f_get_patcodes(patient_id),' ') patfacilityIds");
            
            // Added for FIN-22378(Start)
            patientRelAchievements.append(",(select clst.codelst_desc from er_codelst clst where clst.codelst_type = 'milestone_type' and codelst_subtyp = milestone_type) miletype  ");
            patientRelAchievements.append(",nvl((select patprot_patstdid from er_patprot pat where pat.pk_patprot = sch.fk_patprot),'-')patstudy_id  ");
            patientRelAchievements.append(",nvl((select clst.codelst_desc from er_codelst clst where clst.pk_codelst = milestone_paytype),'-') paytype  ");
            patientRelAchievements.append(",nvl((select clst.codelst_desc from er_codelst clst where clst.pk_codelst = milestone_payfor),'-') payfor  ");
            
            patientRelAchievements.append(",nvl((select cal.name from event_assoc cal where cal.event_id = ev.chain_id),'-') cal_desc ");
            patientRelAchievements.append(",nvl((select v.visit_name from sch_protocol_visit  v where sch.fk_visit = v.pk_protocol_visit ),'-') visit_name  ");
            patientRelAchievements.append(",nvl((select trim(CODELST_SUBTYP) from sch_codelst where pk_codelst = sch.fk_codelst_covertype),'-') coverage_type ");
            // Added for FIN-22378(End)
            
            patientRelAchievements.append(" from er_milestone a, event_assoc ev, sch_events1 sch " );
            
            if (payAbleStatus.equals("rec"))
            {
            	patientRelAchievements.append(", er_codelst p ");
            }	
            
            patientRelAchievements.append(" where a.fk_study = ? and milestone_Type = 'AM' ");
            patientRelAchievements.append(" and (a.FK_EVENTASSOC = ev.event_id and ev.event_id = sch.fk_assoc) ");
            patientRelAchievements.append(" and a.milestone_amount > 0 and NVL(a.MILESTONE_DELFLAG,'Z') <> 'Y' ");
            patientRelAchievements.append(" and FK_CODELST_MILESTONE_STAT = (SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'milestone_stat' AND codelst_subtyp = 'A') ");

            if (payAbleStatus.equals("rec"))
            {
            	patientRelAchievements.append(" and p.pk_codelst = milestone_paytype and trim(p.codelst_subtyp) = 'rec'  ");
            }	
            
            if (milestoneReceivableStatus.equals("UI"))
            {          	
            	patientRelAchievements.append(" and not exists (select * from er_invoice_detail det where det.fk_milestone = pk_milestone)  ");	
            }
            
            if (sitePk > 0) //add filter for site
            {
            	patientRelAchievements.append(" and ((patient_id is null) or ( 1 =  pkg_patient.f_is_enrollingsite(patient_id, a.fk_study, ?)  ) ) ");
            }
            /*** End of Patient related Achievements ***/
            
            /*** Non Patient related Additional Milestones ***/
            nonPatientRelAchievements.append("select 0 as pk_mileachieved, milestone_count, pk_milestone, ");
            nonPatientRelAchievements.append("0 as fk_per ,   '' AS patFirstName , '' AS patLastName , '' AS enrollingSite , a.fk_study, 0 as fk_patprot, ");
            nonPatientRelAchievements.append("'' as ach_date, milestone_type, milestone_amount ");
                        
            if (! StringUtil.isEmpty(payAbleStatus)) // otherwise we don't need calculated invoice amount
            {	//FIN-21727
            	nonPatientRelAchievements.append(", nvl((SELECT sum(amount_invoiced) as amount_invoiced FROM er_invoice_detail det, er_invoice inv ")
            	.append("  WHERE det.fk_milestone = pk_milestone ")
            	.append("  AND inv.pk_invoice = det.fk_inv ")
            	.append("  AND det.DETAIL_TYPE = 'H' AND NVL(det.fk_mileachieved,0) = 0),0) amount_invoiced " );
            	
            	nonPatientRelAchievements.append(", nvl((SELECT sum(amount_holdback) as amount_holdback FROM er_invoice_detail det, er_invoice inv ")
            	.append("  WHERE det.fk_milestone = pk_milestone ")
            	.append("  AND inv.pk_invoice = det.fk_inv ")
            	.append("  AND det.DETAIL_TYPE = 'H' AND NVL(det.fk_mileachieved,0) = 0),0) amount_holdback " );
            }	
            else
            {
            	nonPatientRelAchievements.append(", '' as amount_invoiced " ); 
            	nonPatientRelAchievements.append(", '' as amount_holdback " ); 
            }
            
            nonPatientRelAchievements.append(" ,'' as patfacilityIds");
            
            // Added for FIN-22378(Start)
            nonPatientRelAchievements.append(",(select clst.codelst_desc from er_codelst clst where clst.codelst_type = 'milestone_type' and codelst_subtyp = milestone_type) miletype  ");
            nonPatientRelAchievements.append(",'-' as patstudy_id  ");
            nonPatientRelAchievements.append(",nvl((select clst.codelst_desc from er_codelst clst where clst.pk_codelst = milestone_paytype),'-') paytype  ");
            nonPatientRelAchievements.append(",nvl((select clst.codelst_desc from er_codelst clst where clst.pk_codelst = milestone_payfor),'-') payfor  ");
            
            nonPatientRelAchievements.append(",'-' as cal_desc ");
            nonPatientRelAchievements.append(",'-' as visit_name  ");
            nonPatientRelAchievements.append(",'-' as coverage_type ");
            // Added for FIN-22378(End)
            
            nonPatientRelAchievements.append(" from er_milestone a " );
            
            if (payAbleStatus.equals("rec"))
            {
            	nonPatientRelAchievements.append(", er_codelst p ");
            }	
            
            nonPatientRelAchievements.append(" where a.fk_study = ? and milestone_Type = 'AM' and FK_EVENTASSOC is null ");
            nonPatientRelAchievements.append(" and milestone_amount > 0 and NVL(a.MILESTONE_DELFLAG,'Z') <> 'Y' ");
            nonPatientRelAchievements.append(" and FK_CODELST_MILESTONE_STAT = (SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'milestone_stat' AND codelst_subtyp = 'A') ");
            
            //rangeFrom check does not apply to these milestones
            
            if (payAbleStatus.equals("rec"))
            {
            	nonPatientRelAchievements.append(" and p.pk_codelst = milestone_paytype and trim(p.codelst_subtyp) = 'rec' ");
            }	
            
            if (milestoneReceivableStatus.equals("UI"))
            {
            	nonPatientRelAchievements.append(" and not exists (select * from er_invoice_detail det where det.fk_milestone = pk_milestone)  ");
            }
            
            //site filter not required
            /*** End of Non Patient related Achievements ***/
            
            achievements = patientRelAchievements
            	.append(" UNION ")
            	.append(nonPatientRelAchievements)
            	.append(" order by pk_milestone,pk_mileachieved ");
            
            Rlog.fatal("milestone", "MileAchievedDao.getAchievedMilestonesAM SQL" + achievements.toString());
            
            pstmt = conn.prepareStatement(achievements.toString());
            
            int paramSeq = 1;
            pstmt.setInt(paramSeq, studyId); paramSeq++;
            
            if (sitePk > 0) //add filter for site
            {
            	pstmt.setInt(paramSeq,sitePk); paramSeq++;
            }	
            pstmt.setInt(paramSeq, studyId); 

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	
            	milestone = rs.getString("pk_milestone");

            	if (rows == 0)
            	{
            		prevMilestone = milestone;
            	}
            	
            	if ( ! prevMilestone.equals(milestone) )
            	{
            		//set it to hashtable
            		 Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestonesAM adding to hashtable" +prevMilestone);
            		 this.setHtAchieved(prevMilestone, new MileAchievedDao(mdTemp));
            		 mdTemp = new MileAchievedDao();
            		//this.reset(); //clear data arraylists
            	}
            	
            	mdTemp.setId( rs.getString("pk_mileachieved"));
            	mdTemp.setMilestone(milestone);
                mdTemp.setStudy(rs.getString("fk_study"));
            	mdTemp.setPatient(rs.getString("fk_per"));
            	mdTemp.setPatFirstName(rs.getString("patFirstName"));
            	mdTemp.setPatLastName(rs.getString("patLastName"));
            	mdTemp.setEnrollingSite(rs.getString("enrollingSite"));
            	mdTemp.setPatprot(rs.getString("fk_patprot"));
            	mdTemp.setAchievedOn(rs.getString("ach_date"));
            	mdTemp.setAmountDue(rs.getString("milestone_amount"));
            	mdTemp.setAmountInvoiced(rs.getString("amount_invoiced"));
            	mdTemp.setAmountHoldback(rs.getString("amount_holdback"));
       
            	mdTemp.setPatientCode("-");
            	
            	// Added for FIN-22378
            	mdTemp.setPatientStudyIds(rs.getString("patstudy_id"));
            	mdTemp.setMilestonePaymentTypes(rs.getString("paytype"));
            	mdTemp.setMilestonePaymentFors(rs.getString("payfor"));
            	mdTemp.setMileTypes(rs.getString("miletype"));
            	
            	mdTemp.setCalDescs(rs.getString("cal_desc"));
            	mdTemp.setVisitNames(rs.getString("visit_name"));
            	mdTemp.setCoverAnalysis(rs.getString("coverage_type"));

                prevMilestone = milestone;
                
                Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestonesAM"
                        + mdTemp.id.size());
                
                rows++;
                
            }
            
            this.setHtAchieved(milestone, new MileAchievedDao(mdTemp));
            mdTemp = new MileAchievedDao();
            
            
            // get milestones for the milestone type,receivable status and Coverage Type
            mileDao.getMilestoneRowsAM(studyId, payAbleStatus, milestoneReceivableStatus , CovType);
            
            //iterate throughthe milestones, and calculate the number of milestones achieved for the given date range
            ArrayList mileStoneIds = new ArrayList();
            ArrayList milestoneCounts = new ArrayList();
            int mileCount = 0;
            String mileStoneId  = "";
            int countPerMileStone = 0;
            int achDetailCount = 0;
            long totalMilestonesAchieved = 0;
            long calculateAchievedRows = 0;

            mileStoneIds = mileDao.getMilestoneIds();
            mileCount = mileStoneIds.size();
            milestoneCounts = mileDao.getMilestoneCounts ();
            
            for (int i =0; i < mileCount ; i++)
     		{
     			mileStoneId = String.valueOf(((Integer)mileStoneIds.get(i)).intValue());
     			
     			//count required to make one milestone
     			countPerMileStone = StringUtil.stringToNum((String) milestoneCounts.get(i));
     			
    			MileAchievedDao ach = new MileAchievedDao();
    			ArrayList ids = new ArrayList();

    			if (htAchieved.containsKey(mileStoneId))
    			{
    			
    				ach = (MileAchievedDao) htAchieved.get(mileStoneId);
    				ids = ach.getId();
    				achDetailCount = ids.size();
    				
    				if (countPerMileStone > 1 &&  achDetailCount > 0)
    				{
    					//calculate the milestones achieved
    					totalMilestonesAchieved = Math.round(Math.floor(achDetailCount/countPerMileStone));
    					calculateAchievedRows = totalMilestonesAchieved * countPerMileStone;
    				}
    				else
    				{
    					totalMilestonesAchieved = achDetailCount;
    					calculateAchievedRows  = achDetailCount;
    				}
    				
    				// remove extra achieved rows from MileAchieveddao if any
    				if (calculateAchievedRows  < achDetailCount)
    				{
    					 Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestonesAM, have to delete extra rows for milestone: " +mileStoneId + " remove rows: " + (achDetailCount - calculateAchievedRows));
    					 //remove after index calculateAchievedRows
    					 
    					 ach.removeFrom(calculateAchievedRows);
    					 Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestonesAM, left: " + (ach.getId()).size());
    					    					  
    					 htAchieved.put(mileStoneId,ach);
    					 Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestonesAM, removed extra rows");
    				}
    				
    			}
    			else
    			{
    				achDetailCount = 0;
    				totalMilestonesAchieved = 0;
    			}
    			
    			mileDao.setMilestoneAchievedCounts(String.valueOf(totalMilestonesAchieved));
     		}
            
            this.setMilestoneDao(mileDao);

            
        } catch (Exception ex) {
            Rlog.fatal("milestone",
                    "MileAchievedDao.getAchievedMilestonesAM EXCEPTION IN FETCHING FROM er_invoice table"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }
    
    public void getAchievedMilestonesNew(int studyId, String rangeFrom,String rangeTo, String milestoneReceivableStatus,String milestoneType, String site, String yearFilter, String monthFilter, String monthFilterYear, String dateRangeType) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlDetail = new StringBuffer();
        String milestone = "";
        String prevMilestone = "";
        MilestoneDao mileDao = new MilestoneDao(); 
         MileAchievedDao mdTemp = new MileAchievedDao();
         String payAbleStatus = "";
         int sitePk = 0;
         
        
        try {
        	
        	sitePk = StringUtil.stringToNum(site);
        	
        	if (StringUtil.isEmpty(milestoneReceivableStatus)) //from payment module, to get info for all types of milestones
        	{
        		
        		payAbleStatus = "";
        	}
        	else
        	{
        		payAbleStatus = "rec"; // for receivable milestones, for invoicing
        		
        	}
           	
            conn = getConnection();
            
            sqlDetail.append("select pk_mileachieved, milestone_count, fk_milestone, nvl(fk_per,0) fk_per ,")
            	.append(" DECODE(m.TABLE_NAME, 'er_studystat', '',(select PERSON_FNAME from PERSON where PK_PERSON=fk_per)) patFirstName," )
				.append(" DECODE(m.TABLE_NAME, 'er_studystat', '',(select PERSON_LNAME from PERSON where PK_PERSON=fk_per)) patLastName, ")
				.append(" DECODE(m.TABLE_NAME, 'er_studystat', '',(select SITE_NAME from ER_SITE where PK_SITE=(select PER_SITE from erv_studypat_by_visit erv , erv_person p where erv.fk_study  = "+studyId+" and P.pk_person = erv.fk_per and PK_PERSON=m.fk_per) )) enrollingSite,")		
            	.append(" m.fk_study, fk_patprot, to_char(ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT) ach_date, milestone_type,milestone_amount ");
            
            if (! StringUtil.isEmpty(payAbleStatus)) // otherwise we don't need calculated invoice amount
            {	//FIN-21727
            	sqlDetail.append(", nvl((SELECT sum(CASE ")
            	.append("  WHEN (det.DETAIL_TYPE = 'H' AND nvl(milestone_count,1) > 0) THEN (amount_invoiced/milestones_achieved)/nvl(milestone_count,1) ")
            	.append("  WHEN (det.DETAIL_TYPE = 'H' AND nvl(milestone_count,1) <= 0) THEN (amount_invoiced/milestones_achieved)  ")
            	.append("  WHEN det.DETAIL_TYPE = 'D' THEN (amount_invoiced)  END) as amount_invoiced FROM er_invoice_detail det, er_invoice inv ")
            	.append("  WHERE det.fk_milestone = m.fk_milestone ")
            	.append("  AND inv.pk_invoice = det.fk_inv ")
            	.append("  AND ((det.DETAIL_TYPE = 'H' AND NVL(det.fk_mileachieved,0) = 0 AND DISPLAY_DETAIL <> 1) ")
            	.append("  OR (det.DETAIL_TYPE = 'D' AND det.fk_mileachieved = m.pk_mileachieved AND DISPLAY_DETAIL = 1))),0) amount_invoiced " );
            	
            	sqlDetail.append(", nvl((SELECT sum(CASE ")
            	.append("  WHEN (det.DETAIL_TYPE = 'H' AND nvl(milestone_count,1) > 0) THEN (amount_holdback/milestones_achieved)/nvl(milestone_count,1) ")
            	.append("  WHEN (det.DETAIL_TYPE = 'H' AND nvl(milestone_count,1) <= 0) THEN (amount_holdback/milestones_achieved)  ")
            	.append("  WHEN det.DETAIL_TYPE = 'D' THEN (amount_holdback)  END) as amount_holdback FROM er_invoice_detail det, er_invoice inv ")
            	.append("  WHERE det.fk_milestone = m.fk_milestone ")
            	.append("  AND inv.pk_invoice = det.fk_inv ")
            	.append("  AND ((det.DETAIL_TYPE = 'H' AND NVL(det.fk_mileachieved,0) = 0 AND DISPLAY_DETAIL <> 1) ")
            	.append("  OR (det.DETAIL_TYPE = 'D' AND det.fk_mileachieved = m.pk_mileachieved AND DISPLAY_DETAIL = 1))),0) amount_holdback " );
            }	
            else
            {
            	sqlDetail.append(", '' amount_invoiced " ); 
            	sqlDetail.append(", '' amount_holdback " ); 
            }
            
            if (milestoneType.equals("SM")){
            	sqlDetail.append(", (select to_char(studystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) from er_studystat where pk_studystat=fk_per) study_status_date ");
        	} else {
        		sqlDetail.append(", nvl(pkg_patient.f_get_patcodes(fk_per),'') patfacilityIds");
           	}

            // Added for FIN-22378(Start)
            
            sqlDetail.append(",(select clst.codelst_desc from er_codelst clst where clst.codelst_type = 'milestone_type' and codelst_subtyp = milestone_type) miletype  ");
            sqlDetail.append(",DECODE(m.TABLE_NAME, 'er_studystat', '',nvl((select patprot_patstdid from er_patprot pat where pat.fk_study = m.fk_study and pat.fk_per = m.fk_per and patprot_stat = 1),''))patstudy_id  ");
            sqlDetail.append(",nvl((select clst.codelst_desc from er_codelst clst where clst.pk_codelst = milestone_paytype),'-') paytype  ");
            sqlDetail.append(",nvl((select clst.codelst_desc from er_codelst clst where clst.pk_codelst = milestone_payfor),'-') payfor  ");
            
            if(milestoneType !=null && milestoneType.equalsIgnoreCase("VM")){
            	sqlDetail.append(",nvl((select cal.name from event_assoc cal where cal.event_id = fk_cal),'-') cal_desc ");
            	sqlDetail.append(",nvl((select v.visit_name from sch_protocol_visit  v where a.fk_visit = v.pk_protocol_visit ),'-') visit_name ");
            }
            
            // Added for FIN-22378(End)
            
            sqlDetail.append(" from er_milestone a,er_mileachieved m " );
            
            if (payAbleStatus.equals("rec"))
            {
            	sqlDetail.append(", er_codelst p ");
            }	
            sqlDetail.append(" where m.fk_study = ? and pk_milestone = fk_milestone and milestone_achievedcount > 0 and m.is_complete  = 1 and milestone_Type = ?  ");
            sqlDetail.append(" and a.milestone_amount > 0 ");
            
            
            
            if(dateRangeType!=null && !dateRangeType.equals("") && dateRangeType.equals("DR")){
            	if (! StringUtil.isEmpty(rangeFrom))
                {
                	sqlDetail.append(" AND TO_DATE(TO_CHAR(ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) BETWEEN TO_DATE(?,PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(?,PKG_DATEUTIL.F_GET_DATEFORMAT) ");
                	
                }
            }else if(dateRangeType!=null && !dateRangeType.equals("") && dateRangeType.equals("M")){
            	sqlDetail.append(" AND to_char(ach_date, 'MM')=? ");
            	sqlDetail.append(" AND to_char(ach_date, 'YYYY')=? ");
            }else if(dateRangeType!=null && !dateRangeType.equals("") && dateRangeType.equals("Y"))	{
            	sqlDetail.append(" AND to_char(ach_date, 'YYYY')=? ");
            }
            
            if (payAbleStatus.equals("rec"))
            {
            	sqlDetail.append(" and p.pk_codelst = milestone_paytype and trim(p.codelst_subtyp) = 'rec'  ");
            }	
            
            if (milestoneReceivableStatus.equals("UI"))
            {
            	
            	sqlDetail.append(" and not exists (select * from er_invoice_detail det where det.fk_milestone = pk_milestone and det.fk_mileachieved = pk_mileachieved)  ");
            	
            }
            
            if (sitePk > 0) //add filter for site
            {
            	
            	sqlDetail.append(" and ((fk_per is null) or ( 1 =  pkg_patient.f_is_enrollingsite(fk_per, m.fk_study, ?)  ) ) ");
            	
            }
            sqlDetail.append(" and a.FK_CODELST_MILESTONE_STAT = (SELECT pk_codelst FROM er_codelst WHERE CODELST_TYPE = 'milestone_stat' AND CODELST_SUBTYP = 'A') ");
            sqlDetail.append(" order by fk_milestone,pk_mileachieved ");
            
            Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones SQL" + sqlDetail.toString());
            
            pstmt = conn.prepareStatement(sqlDetail.toString());
            pstmt.setInt(1, studyId);
            pstmt.setString(2, milestoneType);
            
            if(dateRangeType!=null && !dateRangeType.equals("") && dateRangeType.equals("DR")){
            	if (! StringUtil.isEmpty(rangeFrom))
                {
                	pstmt.setString(3,rangeFrom );
                	pstmt.setString(4,rangeTo );
                	if (sitePk > 0) //add filter for site
                    {
                		pstmt.setInt(5,sitePk);
                    }
                	
                }
            }else if(dateRangeType!=null && !dateRangeType.equals("") && dateRangeType.equals("M")){
            	pstmt.setInt(3,StringUtil.stringToInteger(monthFilter));
            	pstmt.setInt(4,StringUtil.stringToInteger(monthFilterYear));
            	if (sitePk > 0) //add filter for site
                {
            		pstmt.setInt(5,sitePk);
                }

            }else if(dateRangeType!=null && !dateRangeType.equals("") && dateRangeType.equals("Y"))	{
            	pstmt.setInt(3,StringUtil.stringToInteger(yearFilter ));
            	if (sitePk > 0) //add filter for site
                {
            		pstmt.setInt(4,sitePk);
                }
            }else
            {
            	if (sitePk > 0) //add filter for site
                {
            		pstmt.setInt(3,sitePk);
                }
            	
            }
            

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	
            	milestone = rs.getString("fk_milestone");
            	
            	 
            	
            	if (rows == 0)
            	{
            		prevMilestone = milestone;
            		
            	}
            	
            	if ( ! prevMilestone.equals(milestone) )
            	{
            		//set it to hashtable
            		 Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones adding to hashtable" +prevMilestone);
            		 this.setHtAchieved(prevMilestone, new MileAchievedDao(mdTemp));
            		 mdTemp = new MileAchievedDao();
            		//this.reset(); //clear data arraylists
            		  
            	}
            	
            	 
            	 
            	
            	mdTemp.setId( rs.getString("pk_mileachieved"));
            	mdTemp.setMilestone(milestone);
                mdTemp.setStudy(rs.getString("fk_study"));
            	mdTemp.setPatient(rs.getString("fk_per"));
            	mdTemp.setPatFirstName(rs.getString("patFirstName"));
            	mdTemp.setPatLastName(rs.getString("patLastName"));
            	mdTemp.setEnrollingSite(rs.getString("enrollingSite"));
            	mdTemp.setPatprot(rs.getString("fk_patprot"));
            	mdTemp.setAchievedOn(rs.getString("ach_date"));
            	mdTemp.setAmountDue(rs.getString("milestone_amount"));
            	mdTemp.setAmountInvoiced(rs.getString("amount_invoiced"));
            	mdTemp.setAmountHoldback(rs.getString("amount_holdback"));
       
            	//System.out.println("amount-inv.************.."  );
            	//System.out.println("amount-inv" + rs.getString("amount_invoiced"));
            	//System.out.println("amount-inv.....***********........"  );
            	//Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones setAmountInvoiced "+ rs.getString("amount_invoiced") );
            	if (milestoneType.equals("SM")){
            		//Reverted For Bug15084
            		//Removed for Bug # 15728 - Tarun Kumar
            		mdTemp.setPatientCode(rs.getString("study_status_date"));
            		//mdTemp.setPatientCode("-");
            	} else {
            		mdTemp.setPatientCode("-");
               	}

            	// Added for FIN-22378
            	mdTemp.setPatientStudyIds(rs.getString("patstudy_id"));
            	mdTemp.setMilestonePaymentTypes(rs.getString("paytype"));
            	mdTemp.setMilestonePaymentFors(rs.getString("payfor"));
            	mdTemp.setMileTypes(rs.getString("miletype"));
            	
            	mdTemp.setCoverAnalysis("-");
            	if(milestoneType !=null && milestoneType.equalsIgnoreCase("VM")){
	            	mdTemp.setCalDescs(rs.getString("cal_desc"));
	            	mdTemp.setVisitNames(rs.getString("visit_name"));
            	}
            	else{
            		mdTemp.setCalDescs("-");
                	mdTemp.setVisitNames("-");
            	}
            	            	            	
                prevMilestone = milestone;
                
                Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones"
                        + mdTemp.id.size());
                
                rows++;
                
            }
            
            this.setHtAchieved(milestone, new MileAchievedDao(mdTemp));
            mdTemp = new MileAchievedDao();
            
            
            // get milestones for the milestone type and recievable status
            
            mileDao.getMilestoneRows(studyId, milestoneType, payAbleStatus, milestoneReceivableStatus, 1);
            
            //iterate throughthe milestones, and calculate the number of milestones achieved for the given date range
            
            ArrayList mileStoneIds = new ArrayList();
            ArrayList milestoneCounts = new ArrayList();
            int mileCount = 0;
            String mileStoneId  = "";
            int countPerMileStone = 0;
            int achDetailCount = 0;
            long totalMilestonesAchieved = 0;
            long calculateAchievedRows = 0;
            
            
            mileStoneIds = mileDao.getMilestoneIds();
            
            mileCount = mileStoneIds.size();
            milestoneCounts = mileDao.getMilestoneCounts ();
            
            for (int i =0; i < mileCount ; i++)
     		{
     		
     			mileStoneId = String.valueOf(((Integer)mileStoneIds.get(i)).intValue());
     			
     			//count required to make one milestone
     			countPerMileStone = StringUtil.stringToNum((String) milestoneCounts.get(i));
     			
    			MileAchievedDao ach = new MileAchievedDao();
    			ArrayList ids = new ArrayList();

    			if (htAchieved.containsKey(mileStoneId))
    			{
    			
    				ach = (MileAchievedDao) htAchieved.get(mileStoneId);
    				ids = ach.getId();
    				achDetailCount = ids.size();
    				
    				if (countPerMileStone > 1 &&  achDetailCount > 0)
    				{
    					//calculate the milestones achieved
    					
    					totalMilestonesAchieved = Math.round(Math.floor(achDetailCount/countPerMileStone));
    					
    					calculateAchievedRows = totalMilestonesAchieved * countPerMileStone;
    					
    				}
    				else
    				{
    					totalMilestonesAchieved = achDetailCount;
    					calculateAchievedRows  = achDetailCount;
    					
    				}
    				
    				// remove extra achieved rows from MileAchieveddao if any
    				
    				if (calculateAchievedRows  < achDetailCount)
    				{
    					 
    					 Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones, have to delete extra rows for milestone: " +mileStoneId + " remove rows: " + (achDetailCount - calculateAchievedRows));
    					 //remove after index calculateAchievedRows
    					 
    					 ach.removeFrom(calculateAchievedRows);
    					 Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones, left: " + (ach.getId()).size());
    					    					  
    		            htAchieved.put(mileStoneId,ach);
    		            Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones, removed extra rows");
    		            	
    				}
    				
    			}
    			else
    			{
    				achDetailCount = 0;
    				totalMilestonesAchieved = 0;
    			}
    			
    			mileDao.setMilestoneAchievedCounts(String.valueOf(totalMilestonesAchieved));
    			
    			
    		    			
     		}
            
            this.setMilestoneDao(mileDao);

            
        } catch (Exception ex) {
            Rlog.fatal("milestone",
                    "MileAchievedDao.getAchievedMilestones EXCEPTION IN FETCHING FROM er_invoice table"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }
    // Added for FIN-22378(End)
    
    
    
    // end of class
    
}
