
/*
 * Classname			StorageStatusDao.class
 *
 * Version information 	1.0
 *
 * Date					09/06/2007
 *
 * Copyright notice		Velos, Inc.
 *
 * Author 				Jnanamay Majumdar
 */

package com.velos.eres.business.common;

import java.sql.Connection;
import com.velos.eres.service.util.Rlog;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Clob;


/**
 * StorageStatusDao for retrieving storage status records
 *
 * @author Jnanamay Majumdar
 * @version : 1.0 09/06/2007
 */


public class StorageStatusDao extends CommonDAO implements java.io.Serializable {
	private ArrayList pkStorageStatuses;
	private ArrayList fkStorages;
    private ArrayList storageStartDates;
    private ArrayList storageEndDates;
    private ArrayList fkCodelstStorageStatuses;
    private ArrayList storageNotes;
    private ArrayList storageUsers;
    private ArrayList storageSsTrackingNumbers;
    private ArrayList storageFkStudys;



    public StorageStatusDao() {
    	pkStorageStatuses = new ArrayList();
    	fkStorages = new ArrayList();
    	storageStartDates = new ArrayList();
    	storageEndDates = new ArrayList();
    	fkCodelstStorageStatuses = new ArrayList();
    	storageNotes = new ArrayList();
    	storageUsers = new ArrayList();
    	storageSsTrackingNumbers = new ArrayList();
    	storageFkStudys = new ArrayList();

   }


//  Getter and Setter methods

    public ArrayList getPkStorageStatuses() {
        return this.pkStorageStatuses;
    }

    public void setPkStorageStatuses(ArrayList pkStorageStatuses) {
        this.pkStorageStatuses = pkStorageStatuses;
    }

    public void setPkStorageStatuses(Integer pkStorageStatus) {
        this.pkStorageStatuses.add(pkStorageStatus);
    }

    public ArrayList getFkStorages() {
        return this.fkStorages;
    }

    public void setFkStorages(ArrayList fkStorages) {
        this.fkStorages = fkStorages;
    }

    public void setFkStorages(String fkStorage) {
        this.fkStorages.add(fkStorage);
    }

    public ArrayList getStorageStartDates() {
        return this.storageStartDates;
    }

    public void setStorageStartDates(ArrayList storageStartDates) {
        this.storageStartDates = storageStartDates;
    }

    public void setStorageStartDates(String storageStartDate) {
        this.storageStartDates.add(storageStartDate);
    }

    public ArrayList getStorageEndDates() {
        return this.storageEndDates;
    }

    public void setStorageEndDates(ArrayList storageEndDates) {
        this.storageEndDates = storageEndDates;
    }

    public void setStorageEndDates(String storageEndDate) {
        this.storageEndDates.add(storageEndDate);
    }

    public ArrayList getFkCodelstStorageStatuses() {
        return this.fkCodelstStorageStatuses;
    }

    public void setFkCodelstStorageStatuses(ArrayList fkCodelstStorageStatuses) {
        this.fkCodelstStorageStatuses = fkCodelstStorageStatuses;
    }

    public void setFkCodelstStorageStatuses(String fkCodelstStorageStatus) {
        this.fkCodelstStorageStatuses.add(fkCodelstStorageStatus);
    }

    public ArrayList getStorageNotes() {
        return this.storageNotes;
    }

    public void setStorageNotes(ArrayList storageNotes) {
        this.storageNotes = storageNotes;
    }

    public void setStorageNotes(String storageNote) {
        this.storageNotes.add(storageNote);
    }

    public ArrayList getStorageUsers() {
        return this.storageUsers;
    }

    public void setStorageUsers(ArrayList storageUsers) {
        this.storageUsers = storageUsers;
    }

    public void setStorageUsers(String storageUser) {
        this.storageUsers.add(storageUser);
    }


    public ArrayList getStorageSsTrackingNumbers() {
        return this.storageSsTrackingNumbers;
    }

    public void setStorageSsTrackingNumbers(ArrayList storageSsTrackingNumbers) {
        this.storageSsTrackingNumbers = storageSsTrackingNumbers;
    }

    public void setStorageSsTrackingNumbers(String storageSsTrackingNumber) {
        this.storageSsTrackingNumbers.add(storageSsTrackingNumber);
    }

    public ArrayList getStorageFkStudys() {
        return this.storageFkStudys;
    }

    public void setStorageFkStudys(ArrayList storageFkStudys) {
        this.storageFkStudys = storageFkStudys;
    }

    public void setStorageFkStudys(String storageFkStudy) {
        this.storageFkStudys.add(storageFkStudy);
    }





    public void getStorageStausValues(int storageId) {

    	PreparedStatement pstmt = null;
        Connection conn = null;
        String notesStr ="";
        Clob notesClob = null;
        try {
            conn = getConnection();
            //JM: 17Sep2007, PK_STORAGE_STATUS desc added to the query
            //JM: 29Sep2007: SS_END_DATE removed
            pstmt = conn.prepareStatement("Select PK_STORAGE_STATUS, FK_STORAGE, SS_START_DATE, FK_CODELST_STORAGE_STATUS, SS_NOTES, FK_USER, SS_TRACKING_NUMBER, FK_STUDY	from ER_STORAGE_STATUS where FK_STORAGE = ?  order by  SS_START_DATE desc, PK_STORAGE_STATUS desc");
            pstmt.setInt(1, storageId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {

            	setPkStorageStatuses(new Integer(rs.getInt("PK_STORAGE_STATUS")));
            	setFkStorages(rs.getString("FK_STORAGE"));
            	setStorageStartDates(rs.getString("SS_START_DATE"));
            	//setStorageEndDates(rs.getString("SS_END_DATE"));
            	setFkCodelstStorageStatuses(rs.getString("FK_CODELST_STORAGE_STATUS"));
            	notesClob = rs.getClob("SS_NOTES"); //KM-3171
            	if(notesClob!=null){
            		notesStr = notesClob.getSubString(1,(int) notesClob.length());
            	}
            	else
            		notesStr ="";
            	setStorageNotes(notesStr);
            	setStorageUsers(rs.getString("FK_USER"));
            	setStorageSsTrackingNumbers(rs.getString("SS_TRACKING_NUMBER"));
            	setStorageFkStudys(rs.getString("FK_STUDY"));

            }

       } catch (SQLException ex) {
            Rlog.fatal("storagestatus",
                    "StorageStatusDao.getStorageStausValues EXCEPTION IN FETCHING FROM ER_STORAGE_STATUS table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            	Rlog.fatal("storagestatus", "StorageStatusDao.getStorageStausValues EXCEPTION in closing the pstmt" +e);
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            	Rlog.fatal("storagestatus", "StorageStatusDao.getStorageStausValues EXCEPTION in closing the connection" +e);
            }

        }

    }


    //Added by Manimaran for deletion of Storage status.
    public int delStorageStatus(int pkStorStat) {

        PreparedStatement pstmt = null;
        Connection conn = null;

        try {

            conn = getConnection();

            pstmt = conn
                    .prepareStatement("delete from er_storage_status where pk_storage_status = ? ");

            Rlog.debug("storagestatus", "in delStorageStatus of StorageStatusDao");

            pstmt.setInt(1,pkStorStat);

            ResultSet rs = pstmt.executeQuery();

        } catch (Exception ex) {
            Rlog.fatal("storagestatus",
                    "Error in delStorageStatus in StorageStatusDao " + ex);
            return -1;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();

            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();

            } catch (Exception e) {
            }
        }
        return 0;

    }

//JM: 23Jul2009: #4147
    public boolean isStorageAllocated(int storageId, int accId) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        int count = 0;

        try {

            conn = getConnection();

            pstmt = conn.prepareStatement("select count(*) allotedStorageCount from er_Specimen where fk_storage=? and fk_account = ?");

            Rlog.debug("storagestatus", "in isStorageAllocated of StorageStatusDao");

            pstmt.setInt(1,storageId);
            pstmt.setInt(2,accId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	count = rs.getInt("allotedStorageCount");
            }

        } catch (Exception ex) {
            Rlog.fatal("storagestatus",
                    "Error in isStorageAllocated in StorageStatusDao " + ex);
            return false;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();

            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();

            } catch (Exception e) {
            }
        }
        if (count>0)
        return true;
        else
        return false;

    }


//JM: 19JAN2010: #4638

    public int getDefStatStudyId(int storageId) {

        PreparedStatement pstmt = null;
        Connection conn = null;

        int retStudy = 0;

        try {

            conn = getConnection();

            //pstmt = conn.prepareStatement("select fk_study from ER_STORAGE_STATUS where PK_STORAGE_STATUS=(select min(PK_STORAGE_STATUS) from ER_STORAGE_STATUS where FK_STORAGE = ? ) ");

            pstmt = conn.prepareStatement("select fk_study  from ER_STORAGE_STATUS where PK_STORAGE_STATUS = " +
            		"(select max(a.PK_STORAGE_STATUS) from (Select PK_STORAGE_STATUS, " +
            	 	" SS_START_DATE from ER_STORAGE_STATUS where FK_STORAGE = ? " +
            	 	"order by  SS_START_DATE desc, PK_STORAGE_STATUS desc)a) ");

            Rlog.debug("storagestatus", "in getDefStatStudyId() of StorageStatusDao");

            pstmt.setInt(1,storageId);

            ResultSet rs = pstmt.executeQuery();


            while (rs.next()) {
            	retStudy = rs.getInt("fk_study");
            }

        } catch (Exception ex) {
            Rlog.fatal("storagestatus",
                    "Error in getDefStatStudyId() in StorageStatusDao " + ex);
            return -1;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();

            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();

            } catch (Exception e) {
            }
        }
        return retStudy;

    }



	//end of class
}
