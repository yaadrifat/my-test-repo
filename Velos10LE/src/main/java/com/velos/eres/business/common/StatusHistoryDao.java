/*
 * Classname : 		   StatusHistoryDao
 * 
 * Version information: 1.0 
 *
 * Date: 				08/10/2004
 * 
 * Copyright notice: 	Velos, Inc
 *
 * Author: 				Anu Khanna
 */

package com.velos.eres.business.common;

/* Import Statements */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.velos.eres.service.util.Rlog;

/* End of Import Statements */

/**
 * The StatusHistoryDAO.<br>
 * <br>
 * This class implementents JDBC for reading pattern to expose methods for
 * getting <br>
 * Status History data.
 * 
 * @author Anu Khanna
 * @vesion 1.0
 */
/*
 * Modified by sonia 09/10/04, changed sorting order in getStatusHistoryInfo, to
 * be sorted on status_date desc ,pk_status desc
 */
public class StatusHistoryDao extends CommonDAO implements java.io.Serializable {

    private ArrayList statusIds;

    private ArrayList moduleIds;

    private ArrayList moduleTables;

    private ArrayList codelstIds;

    private ArrayList codelstDescs;

    private ArrayList codelstSubtypes;

    private ArrayList statusDates;

    private ArrayList statusEndDates;

    private ArrayList statusNotes;

    private ArrayList statusCustoms;

    private ArrayList statusEnteredBy;
    
    //Added by Manimaran for the July-August Enhancement S4.
    private ArrayList isCurrentStats;

    private int cRows;

    /**
     * Default Constructor
     * 
     */

    public StatusHistoryDao() {
        statusIds = new ArrayList();
        moduleIds = new ArrayList();
        moduleTables = new ArrayList();
        codelstIds = new ArrayList();
        codelstDescs = new ArrayList();
        codelstSubtypes = new ArrayList();
        statusDates = new ArrayList();
        statusEndDates = new ArrayList();
        statusNotes = new ArrayList();
        statusCustoms = new ArrayList();
        statusEnteredBy = new ArrayList();
        isCurrentStats = new ArrayList();
        int cRows;
    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    /**
     * 
     * 
     * @return ArrayList of status Ids
     */

    public ArrayList getStatusIds() {
        return this.statusIds;
    }

    /**
     * 
     * 
     * @param status
     *            ids
     */

    public void setStatusIds(ArrayList statusIds) {
        this.statusIds = statusIds;
    }

    /**
     * 
     * 
     * @return ArrayList of module Ids
     */

    public ArrayList getModuleIds() {
        return this.moduleIds;
    }

    /**
     * 
     * 
     * @param module
     *            Ids
     */

    public void setModuleIds(ArrayList moduleIds) {
        this.moduleIds = moduleIds;
    }

    /**
     * 
     * 
     * @return ArrayList of module table
     */

    public ArrayList getModuleTables() {
        return this.moduleTables;
    }

    /**
     * 
     * 
     * @param module
     *            tables
     */

    public void setModuleTables(ArrayList moduleTables) {
        this.moduleTables = moduleTables;
    }

    /**
     * 
     * 
     * @return ArrayList of codelst Ids
     */

    public ArrayList getCodelstIds() {
        return this.codelstIds;
    }

    /**
     * 
     * 
     * @param codelst
     *            Ids
     */

    public void setCodelstIds(ArrayList codelstIds) {
        this.codelstIds = codelstIds;
    }

    /**
     * 
     * 
     * @return ArrayList of codelst desc
     */

    public ArrayList getCodelstDescs() {
        return this.codelstDescs;
    }

    /**
     * 
     * 
     * @param codelst
     *            descriptions
     */

    public void setCodelstDescs(ArrayList codelstDescs) {
        this.codelstDescs = codelstDescs;
    }

    /**
     * 
     * 
     * @return ArrayList of codelst subtypes
     */

    public ArrayList getCodelstSubTypes() {
        return this.codelstSubtypes;
    }

    /**
     * 
     * 
     * @param codelst
     *            subtypes
     */

    public void setCodelstSubTypes(ArrayList codelstSubtypes) {
        this.codelstSubtypes = codelstSubtypes;
    }

    /**
     * 
     * 
     * @return ArrayList of status start dates
     */

    public ArrayList getStatusDates() {
        return this.statusDates;
    }

    /**
     * 
     * 
     * @param staus
     *            start dates
     */

    public void setStatusDates(ArrayList statusDates) {
        this.statusDates = statusDates;
    }

    /**
     * 
     * 
     * @return ArrayList of status end dates
     */

    public ArrayList getStatusEndDates() {
        return this.statusEndDates;
    }

    /**
     * 
     * 
     * @param status
     *            end dates
     */

    public void setStatusEndDates(ArrayList statusEndDates) {
        this.statusEndDates = statusEndDates;
    }

    /**
     * 
     * 
     * @return ArrayList of status notes
     */

    public ArrayList getStatusNotes() {
        return this.statusNotes;
    }

    /**
     * 
     * 
     * @param status
     *            Notes
     */

    public void setStatusNotes(ArrayList statusNotes) {
        this.statusNotes = statusNotes;
    }

    public ArrayList getStatusCustoms() {
        return this.statusCustoms;
    }

    public void setStatusCustoms(ArrayList statusCustoms) {
        this.statusCustoms = statusCustoms;
    }

    /**
     * 
     * 
     * @return ArrayList of status entered by
     */

    public ArrayList getStatusEnteredBy() {
        return this.statusEnteredBy;
    }

    /**
     * 
     * 
     * @param status
     *            entered by
     */

    public void setStatusEnteredBy(ArrayList statusEnteredBy) {
        this.statusEnteredBy = statusEnteredBy;
    }

    //  Added by Manimaran for the July-August Enhancement S4.
    /**
     * @return ArrayList of isCurrentStats
     */

    public ArrayList getIsCurrentStats() {
        return this.isCurrentStats;
    }

    /**
     * @param isCurrentStats
     */

    public void setIsCurrentStats(ArrayList isCurrentStats) {
        this.isCurrentStats = isCurrentStats;
    }

    
    public void setStatusIds(Integer statusId) {
        statusIds.add(statusId);
    }

    public void setModuleIds(String moduleId) {
        moduleIds.add(moduleId);
    }

    public void setModuleTables(String moduleTable) {
        moduleTables.add(moduleTable);
    }

    public void setCodelstIds(String codelstId) {
        codelstIds.add(codelstId);
    }

    public void setCodelstDescs(String codelstDesc) {
        codelstDescs.add(codelstDesc);
    }

    public void setCodelstSubTypes(String codelstSubType) {
        codelstSubtypes.add(codelstSubType);
    }

    public void setStatusDates(String statusDate) {
        statusDates.add(statusDate);
    }

    public void setStatusEndDates(String statusEndDate) {
        statusEndDates.add(statusEndDate);
    }

    public void setStatusNotes(String statusNote) {
        statusNotes.add(statusNote);
    }

    public void setStatusCustoms(String statusCustom) {
        statusCustoms.add(statusCustom);
    }

    public void setStatusEnteredBy(String statEnteredBy) {
        statusEnteredBy.add(statEnteredBy);
    }
    
    public void setIsCurrentStats(Integer isCurrentStat) {
        isCurrentStats.add(isCurrentStat);
    }

    /**
     * This method gets the records of the Status history for a particular
     * moduleId and moduleTable.
     * 
     * @param modulePK
     *            PK of module for which status history is to be retrieved
     * @param moduleTable :
     *            module table name for which status history is to be retrieved
     * @returns the arraylist of the result set to retrive all the records from
     *          the table ER_STATUS_HISTORY and ER_CODELST
     * 
     */

   
    //Query modified by Manimaran for the July-August Enhancement S4.
    public void getStatusHistoryInfo(int modulePK, String moduleTable) {

        Rlog.debug("statusHistory", "inside method 	getStatusHistoryInfo");
        int ret = 1;
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";
        try {

            conn = getConnection();
            sql = "select pk_status, status_date,c.codelst_desc description,h.fk_codelst_stat codelstPK, "
                    + " c.codelst_subtyp subtype, status_end_date, status_notes, "
                    + " status_custom1, status_entered_by, status_iscurrent "
                    + " from er_status_history h, er_codelst c  "
                    + " where STATUS_MODPK= ? "
                    + " and STATUS_MODTABLE= ? "
                    + " and c.pk_codelst = h.fk_codelst_stat "
                    + " order by status_date desc ,pk_status desc";

            Rlog.debug("statusHistory",
                    "inside method 	getStatusHistoryInfo sql" + sql);

            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, modulePK);
            pstmt.setString(2, moduleTable);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                setStatusIds(new Integer(rs.getInt("pk_status")));
                setCodelstIds(rs.getString("codelstPK"));
                setCodelstDescs(rs.getString("description"));
                setCodelstSubTypes(rs.getString("subtype"));
                setStatusDates(rs.getString("status_date"));
                setStatusEndDates(rs.getString("status_end_date"));
                setStatusNotes(rs.getString("status_notes"));
                setStatusCustoms(rs.getString("status_custom1"));
                setStatusEnteredBy(rs.getString("status_entered_by"));
                setIsCurrentStats(new Integer(rs.getInt("status_iscurrent")));
                rows++;
            }
            setCRows(rows);
            Rlog.debug("statusHistory", "number of rows" + rows);

        } catch (SQLException ex) {
            Rlog.fatal("statusHistory", "error in getStatusHistoryInfo" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Method to get the latest status for a particular moduleId and
     * moduleTable.
     * 
     * @param modulePK
     *            PK of module for which latest status is to be retrieved
     * @param moduleTable :
     *            module table name for which latest status is to be retrieved
     * @returns String: codelst subtype value
     */

    public String getLatestStatus(int modulePK, String moduleTable) {

        int ret = 1;
        String subType = null;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";
        try {

            conn = getConnection();
            sql = "select codelst_subtyp 	"
                    + " from er_status_history, er_codelst "
                    + " where STATUS_MODPK = ? "
                    + " and FK_CODELST_STAT = pk_codelst "
                    + " and STATUS_MODTABLE = ? "
                    + " and STATUS_END_DATE is null ";

            Rlog.debug("statusHistory", "inside method 	getLatestStatus sql"
                    + sql);

            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, modulePK);
            pstmt.setString(2, moduleTable);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                subType = rs.getString("codelst_subtyp");
            }

        } catch (SQLException ex) {
            Rlog.fatal("statusHistory", "error in getStatusHistoryInfo" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return subType;

    }

    /**
     * Method to get the latest status history ID for a particular modulePK and moduleTable.
     * 
     * @param modulePK
     *            PK of module for which the latest status is to be retrieved
     * @param moduleTable :
     *            Table name for which the latest status is to be retrieved
     * @returns int: PK of the ER_STATUS_HISTORY table
     */
    public int getLatestStatusId(int modulePK, String moduleTable) {
        int ret = -1;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            String sql = "select pk_status from er_status_history "
            	+ " where STATUS_MODPK = ? and STATUS_MODTABLE = ? "
            	+ " and STATUS_END_DATE is null and  STATUS_ISCURRENT = '1' order by pk_status ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, modulePK);
            pstmt.setString(2, moduleTable);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
            	ret = rs.getInt("pk_status");
            }
        } catch (SQLException ex) {
            Rlog.fatal("statusHistory", "error in getLatestStatusId " + ex);
        } finally {
            try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
            try { if (conn != null) { conn.close(); } } catch (Exception e) {}
        }
        return ret;
    }
    
    public Map<String, String> getStatHisotryMap(){
    	
    	
    	HashMap<String, String> statMap  = new HashMap<String,String>();
    	
    	
    		StringBuilder statIdsSB = new StringBuilder();
    		for(Object o: this.statusIds){
    			if(statIdsSB.length() > 0){
    				statIdsSB.append(',');
    			}
    			statIdsSB.append(o);
    		}
		
    		StringBuilder statDateSB = new StringBuilder();
    		for(Object o: this.statusDates){
    			if(statDateSB.length() > 0){
    				statDateSB.append(',');
    			}
    			statDateSB.append(o);
    		}
    		
    		StringBuilder statDescSB = new StringBuilder();
    		for(Object o: this.codelstDescs){
    			if(statDescSB.length() > 0){
    				statDescSB.append(',');
    			}
    			statDescSB.append(o);
    		}
    		
    		StringBuilder statEnteredBySB = new StringBuilder();
    		for(Object o: this.statusEnteredBy){
    			if(statEnteredBySB.length() > 0){
    				statEnteredBySB.append(',');
    			}
    			statEnteredBySB.append(o);
    		}
    		
    		StringBuilder statNotesSB = new StringBuilder();
    		for(Object o: this.statusNotes){
    			if(statNotesSB.length() > 0){
    				statNotesSB.append(',');
    			}
    			statNotesSB.append(o);
     		}
    		
    		StringBuilder isCurrSB = new StringBuilder();
    		for(Object o: this.isCurrentStats){
    			if(isCurrSB.length() > 0){
    				isCurrSB.append(',');
    			}
    			isCurrSB.append(o);
    		}
    		
    		statMap.put("statIds",statIdsSB.toString());
			statMap.put("statDate",statDateSB.toString());
			statMap.put("statDesc",statDescSB.toString());
			statMap.put("statEnteredBy",statEnteredBySB.toString());
			statMap.put("statNotes",statNotesSB.toString());
			statMap.put("isCurr",isCurrSB.toString());
	
			return statMap;	
    }
    
    public void getStatusHistoryCTRP(int modulePK, String moduleTable) {

    	int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";
        try {

            conn = getConnection();
            sql = "select pk_status, to_char(status_date,PKG_DATEUTIL.f_get_dateformat) status_date,c.codelst_desc description,h.fk_codelst_stat codelstPK, "
                    + " c.codelst_subtyp subtype, status_end_date, status_notes, "
                    + " status_custom1,decode(RECORD_TYPE,'N',(Select eu.usr_firstname || eu.usr_lastname from er_user eu where eu.pk_user=h.creator)"
                    + " ,'M',(Select eu.usr_firstname || eu.usr_lastname from er_user eu where eu.pk_user=h.LAST_MODIFIED_BY),'-')"
                    + " AS status_entered_by, status_iscurrent "
                    + " from er_status_history h, er_codelst c  "
                    + " where STATUS_MODPK= ? "
                    + " and STATUS_MODTABLE= ? "
                    + " and c.pk_codelst = h.fk_codelst_stat AND h.RECORD_TYPE <>'D'"
                    + " order by pk_status desc";

            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, modulePK);
            pstmt.setString(2, moduleTable);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                setStatusIds(new Integer(rs.getInt("pk_status")));
                setCodelstIds(rs.getString("codelstPK"));
                setCodelstDescs(rs.getString("description"));
                setCodelstSubTypes(rs.getString("subtype"));
                setStatusDates(rs.getString("status_date"));
                setStatusEndDates(rs.getString("status_end_date"));
                setStatusNotes(rs.getString("status_notes"));
                setStatusCustoms(rs.getString("status_custom1"));
                setStatusEnteredBy(rs.getString("status_entered_by"));
                setIsCurrentStats(new Integer(rs.getInt("status_iscurrent")));
                rows++;
            }
            setCRows(rows);
            Rlog.debug("statusHistory", "number of rows" + rows);

        } catch (SQLException ex) {
            Rlog.fatal("statusHistory", "error in getStatusHistoryCTRP" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

}
