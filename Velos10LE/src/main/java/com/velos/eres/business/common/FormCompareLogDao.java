/*
 * Classname : FormCompareUtilityDao
 * 
 * Version information: 1.0 
 *
 * Date: 12/13/2011
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sonia Abrol
 */

 package com.velos.eres.business.common;

/* Import Statements */
import java.io.StringReader;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;

 
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The Save Forms Compare Utility Log Dao.<br>
 *  
 * @author Sonia Abrol
 * @vesion 1.0
 */

 public class FormCompareLogDao extends CommonDAO implements java.io.Serializable {
    
 	public ArrayList<String> colDispNameAr;

    public Hashtable htRows; 
         
    public ArrayList getColDispNameAr() {
		return colDispNameAr;
	}




	public void setColDispNameAr(ArrayList colDispNameAr) {
		this.colDispNameAr = colDispNameAr;
	}




	public Hashtable getHtRows() {
		return htRows;
	}




	public void setHtRows(Hashtable htRows) {
		this.htRows = htRows;
	}




	private String selectSqlForCompareLog = " select pk_complog, fk_form, (select form_name from er_formlib where pk_formlib = fk_form) form_name,form_type, records_processed, to_char(comp_start_date,pkg_dateutil.f_get_datetimeformat) comp_start_date, to_char(comp_end_date ,pkg_dateutil.f_get_datetimeformat) comp_end_date from ER_RESP_COMPLOG order by pk_complog desc ";
    
    private String selectSqlForCompareLogDetail = " select  pk_complogdet , fk_resp_complog , fk_per ," +
    		" fk_patprot, fk_filledform,field_name_section, field_uid, xml_val,lin_val,to_char(fill_date_xml,pkg_dateutil.f_get_datetimeformat) fill_date_xml , " +
    		" to_char(formresp_lmd,pkg_dateutil.f_get_datetimeformat) formresp_lmd , to_char(formresp_created_on,pkg_dateutil.f_get_datetimeformat) formresp_created_on, fk_study, fk_account from ER_RESP_COMPLOGDET  where  fk_resp_complog = ? order by fk_per, fk_filledform "  ;

	public FormCompareLogDao() {
		colDispNameAr = new ArrayList();
		htRows = new Hashtable();
         
             }

	 
	
     
    public void getCompareLog() {
        Connection conn = null;
        PreparedStatement pstmt = null;
        int ctr = 0;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(selectSqlForCompareLog);
            
            ResultSet rs = pstmt.executeQuery();

            colDispNameAr.add("pk_complog");
            colDispNameAr.add("fk_form");
            colDispNameAr.add("form_name");
        	
            colDispNameAr.add("form_type");
            colDispNameAr.add("records_processed");
        	
            colDispNameAr.add("comp_start_date");
            colDispNameAr.add("comp_end_date");
        
            
            while (rs.next()) {
            	
            	ArrayList dataAr = new ArrayList();
            	
            	ctr = ctr+1;
            	
            	dataAr.add(rs.getString("pk_complog"));
            	dataAr.add(rs.getString("fk_form"));
            	dataAr.add(rs.getString("form_name"));
            	
            	dataAr.add(rs.getString("form_type"));
            	dataAr.add(rs.getString("records_processed"));
            	
            	dataAr.add(rs.getString("comp_start_date"));
            	dataAr.add(rs.getString("comp_end_date"));
            	
            	htRows.put(new Integer(ctr), dataAr);
                
            }
        } catch (SQLException ex) {
            Rlog.fatal("FormCompareLogDao", "EXCEPTION IN  FormCompareLogDao.getCompareLog"
                    + ex);
            ex.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    
    public void getCompareLogDetails(int  logReqPK) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        int ctr = 0;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(selectSqlForCompareLogDetail);
            
            pstmt.setInt(1, logReqPK);
            
            ResultSet rs = pstmt.executeQuery();

            colDispNameAr.add("pk_complogdet");
            colDispNameAr.add("fk_per");
            colDispNameAr.add("fk_patprot");
        	
            colDispNameAr.add("fk_filledform");
            colDispNameAr.add("field_name_section");
        	
            colDispNameAr.add("field_uid");
            colDispNameAr.add("xml_val");
            colDispNameAr.add("lin_val");
            colDispNameAr.add("fill_date_xml");
            colDispNameAr.add("formresp_lmd");
            colDispNameAr.add("formresp_created_on");
            colDispNameAr.add("fk_study");
            colDispNameAr.add("fk_account");
        
            
             while (rs.next()) {
            	
            	ArrayList dataAr = new ArrayList();
            	
            	ctr = ctr+1;
            	
            	dataAr.add(rs.getString("pk_complogdet"));
            	dataAr.add(rs.getString("fk_per"));
            	dataAr.add(rs.getString("fk_patprot"));
            	
            	dataAr.add(rs.getString("fk_filledform"));
            	dataAr.add(rs.getString("field_name_section"));
            	
            	dataAr.add(rs.getString("field_uid"));
            	dataAr.add(rs.getString("xml_val"));
            	
            	dataAr.add(rs.getString("lin_val"));
            	dataAr.add(rs.getString("fill_date_xml"));
            	dataAr.add(rs.getString("formresp_lmd"));
            	dataAr.add(rs.getString("formresp_created_on"));
            	dataAr.add(rs.getString("fk_study"));
            	dataAr.add(rs.getString("fk_account"));
            	
            	htRows.put(new Integer(ctr), dataAr);
                
            }
        } catch (SQLException ex) {
            Rlog.fatal("FormCompareLogDao", "EXCEPTION IN  FormCompareLogDao.getCompareLogDetails"
                    + ex);
            ex.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
     
}// end of class
