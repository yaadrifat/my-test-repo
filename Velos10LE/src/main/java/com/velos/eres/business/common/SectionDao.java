//Dinesh dt 04/07/2001

package com.velos.eres.business.common;

import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

public class SectionDao extends CommonDAO implements java.io.Serializable {

    private ArrayList ids;

    private ArrayList secStudys;

    private ArrayList secNames;

    private ArrayList secNums;

    private ArrayList contents;

    private ArrayList secPubFlags;

    private ArrayList secSequences;

    public SectionDao() {
        ids = new ArrayList();
        secStudys = new ArrayList();
        secNames = new ArrayList();
        secNums = new ArrayList();
        contents = new ArrayList();
        secPubFlags = new ArrayList();
        secSequences = new ArrayList();
    }

    // Getter and Setter methods

    public ArrayList getIds() {
        return this.ids;
    }

    public void setIds(ArrayList ids) {
        this.ids = ids;
    }

    public ArrayList getSecStudys() {
        return this.secStudys;
    }

    public void setSecStudys(ArrayList secStudys) {
        this.secStudys = secStudys;
    }

    public ArrayList getSecNames() {
        return this.secNames;
    }

    public void setSecNames(ArrayList secNames) {
        this.secNames = secNames;
    }

    public ArrayList getSecNums() {
        return this.secNums;
    }

    public void setSecNums(ArrayList secNums) {
        this.secNums = secNums;
    }

    public ArrayList getContents() {
        return this.contents;
    }

    public void setContents(ArrayList contents) {
        this.contents = contents;
    }

    public ArrayList getSecPubFlags() {
        return this.secPubFlags;
    }

    public void setSecPubFlags(ArrayList secPubFlags) {
        this.secPubFlags = secPubFlags;
    }

    public ArrayList getSecSequences() {
        return this.secSequences;
    }

    public void setSecSequences(ArrayList secSequences) {
        this.secSequences = secSequences;
    }

    // /overloaded setter methods

    public void setIds(Integer id) {
        ids.add(id);
    }

    public void setSecStudys(String secStudy) {
        secStudys.add(secStudy);
    }

    public void setSecNames(String secName) {
        secNames.add(secName);
    }

    public void setSecNums(String secNum) {
        secNums.add(secNum);
    }

    public void setContents(String cont) {
        contents.add(cont);
    }

    public void setSecPubFlags(String secPubFlag) {
        secPubFlags.add(secPubFlag);
    }

    public void setSecSequences(String secSequence) {
        secSequences.add(secSequence);
    }

    // end of getter and setter methods

    public int validateSection(String name, String studyId) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        int ret = 0;
        try {
            conn = getConnection();
            Rlog.debug("section", "got connection" + conn);
            String mysql = "SELECT COUNT(PK_STUDYSEC) AS SECCOUNT FROM ER_STUDYSEC WHERE STUDYSEC_NAME= ? AND FK_STUDY = ? ";

            Rlog.debug("section", mysql);
            pstmt = conn.prepareStatement(mysql);

            Rlog.debug("section", "pstmt " + pstmt);
            pstmt.setString(1, name);
            pstmt.setInt(2, StringUtil.stringToNum(studyId));
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("section", "got resultset" + rs);

            while (rs.next()) {
                ret = rs.getInt("SECCOUNT");
            }

            return ret;
        } catch (SQLException ex) {
            Rlog.fatal("section",
                    "SectionDao.validateSection EXCEPTION IN validating in section table"
                            + ex);
            return -2;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    public void getStudySection(int studyVer, String Flag, String sectionName) {

        String finalStr = "";
        String s1 = "";
        String s2 = "";
        String s3 = "";
        String s4 = "";
        String s5 = "";
        sectionName = sectionName.toUpperCase();

        char flag = Flag.charAt(0);
        Rlog.debug("section", "in DAO: Section");
        PreparedStatement pstmt = null;
        Clob secCLob = null;
        
        Connection conn = null;
        try {
            conn = getConnection();
            Rlog.debug("section", "got connection" + conn);
            String mysql = "SELECT PK_STUDYSEC,FK_STUDY,STUDYSEC_NAME,STUDYSEC_NUM,STUDYSEC_PUBFLAG,STUDYSEC_SEQ, STUDYSEC_TEXT FROM ER_STUDYSEC WHERE ";
            switch (flag) {

            case 'y':
            case 'Y':
                mysql += "STUDYSEC_PUBFLAG IN('y','Y') AND FK_STUDYVER=? ";
                break;

            case 'n':
            case 'N':
                mysql += "STUDYSEC_PUBFLAG IN('n','N') AND FK_STUDYVER=? ";
                break;

            case 'a':
            case 'A':
                if (sectionName.equals("")) {
                    mysql += "FK_STUDYVER=? ";
                } else {
                    mysql += "FK_STUDYVER=? AND UPPER(STUDYSEC_TEXT) LIKE '%"
                            + sectionName + "%'";
                }
                break;
            case 'S':
                if (sectionName.equals("")) {
                    mysql += "FK_STUDYVER=? ";
                } else {
                    mysql += "FK_STUDYVER=? AND UPPER(STUDYSEC_NAME) LIKE '%"+ sectionName + "%'";
                }
                break;
            default:
            }

            mysql += "ORDER BY STUDYSEC_SEQ, STUDYSEC_NUM ";
            // String mysql = "select PK_MSGCNTR,FK_STUDY, FK_USERX,
            // FK_USERTO,MSGCNTR_TEXT, MSGCNTR_REQTYPE, MSGCNTR_STAT from
            // ER_MSGCNTR where FK_USERTO = ?";
            Rlog.debug("section", mysql);
            pstmt = conn.prepareStatement(mysql);

            Rlog.debug("section", "pstmt " + pstmt);
            pstmt.setInt(1, studyVer);
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("section", "got resultset" + rs);

            while (rs.next()) {

                setIds(new Integer(rs.getInt("PK_STUDYSEC")));
                setSecStudys(rs.getString("FK_STUDY"));
                setSecNames(rs.getString("STUDYSEC_NAME"));
                setSecNums(rs.getString("STUDYSEC_NUM"));
                setSecPubFlags(rs.getString("STUDYSEC_PUBFLAG"));
                setSecSequences(rs.getString("STUDYSEC_SEQ"));

                secCLob  = rs.getClob("STUDYSEC_TEXT");
                finalStr = "";
                if (!(secCLob == null)) {
                	finalStr= secCLob.getSubString(1, (int) secCLob.length());
                }
                 
                
                /*s2 = rs.getString("STUDYSEC_CONTENTS2");
                s3 = rs.getString("STUDYSEC_CONTENTS3");
                s4 = rs.getString("STUDYSEC_CONTENTS4");
                s5 = rs.getString("STUDYSEC_CONTENTS5");

                if (s1 == null)
                    s1 = "";
                if (s2 == null)
                    s2 = "";
                if (s3 == null)
                    s3 = "";
                if (s4 == null)
                    s4 = "";
                if (s5 == null)
                    s5 = "";

                finalStr = s1 + s2 + s3 + s4 + s5; */

                Rlog.debug("section", "SectionDao.getStudy rows finalStr" + finalStr);
                setContents(finalStr);

            }
            Rlog.debug("section", "SectionDao.getStudy rows ");
        } catch (SQLException ex) {
            Rlog.fatal("msgcntr",
                    "msgcntrDao.getMsgcntr EXCEPTION IN FETCHING FROM msgcntr table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }
}