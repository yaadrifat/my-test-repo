/*
 * Classname			StdSiteRightsDao
 * 
 * Version information	1.0
 *
 * Date				11/17/2003
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import oracle.jdbc.driver.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import com.velos.eres.service.util.Rlog;

/**
 * StdSiteRightsDao class
 * 
 * @author Anu Khanna
 * @version 1.0 04/23/2003
 */

public class StdSiteRightsDao extends CommonDAO implements java.io.Serializable {

    private ArrayList stdSiteRightsIds;

    private ArrayList siteIds;

    private ArrayList studyIds;

    private ArrayList userIds;

    private ArrayList stdSiteRights;

    private ArrayList studySiteNames;

    private ArrayList studySiteIndents;

    /*
     * userSiteParents - this is id of the main head parent site whose sub sites
     * have been formed eg if B is child of A and C is child of B then the main
     * parent is A for both
     */
    private ArrayList studySiteParents;

    private int cRows;

    public StdSiteRightsDao() {
        stdSiteRightsIds = new ArrayList();
        siteIds = new ArrayList();
        studyIds = new ArrayList();
        userIds = new ArrayList();
        stdSiteRights = new ArrayList();
        studySiteNames = new ArrayList();
        studySiteIndents = new ArrayList();
        studySiteParents = new ArrayList();
    }

    // Getter and Setter methods

    public ArrayList getStdSiteRightsIds() {
        return this.stdSiteRightsIds;
    }

    public void setStdSiteRightsIds(ArrayList stdSiteRightsIds) {
        this.stdSiteRightsIds = stdSiteRightsIds;
    }

    public ArrayList getStudySiteNames() {
        return this.studySiteNames;
    }

    public void setStudySiteNames(ArrayList studySiteNames) {
        this.studySiteNames = studySiteNames;
    }

    public ArrayList getStdSiteRights() {
        return this.stdSiteRights;
    }

    public void setStdSiteRights(ArrayList stdSiteRights) {
        this.stdSiteRights = stdSiteRights;
    }

    public ArrayList getUserIds() {
        return this.userIds;
    }

    public void setUserIds(ArrayList userIds) {
        this.userIds = userIds;
    }

    public ArrayList getSiteIds() {
        return this.siteIds;
    }

    public void setSiteIds(ArrayList siteIds) {
        this.siteIds = siteIds;
    }

    public ArrayList getStudyIds() {
        return this.studyIds;
    }

    public void setStudyIds(ArrayList studyIds) {
        this.studyIds = studyIds;
    }

    public ArrayList getStudySiteIndents() {
        return this.studySiteIndents;
    }

    public void setStudySiteIndents(ArrayList studySiteIndents) {
        this.studySiteIndents = studySiteIndents;
    }

    public ArrayList getStudySiteParents() {
        return this.studySiteParents;
    }

    public void setStudySiteParents(ArrayList studySiteParents) {
        this.studySiteParents = studySiteParents;
    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public void setStdSiteRightsIds(Integer stdSiteRightsId) {
        this.stdSiteRightsIds.add(stdSiteRightsId);
    }

    public void setSiteIds(String siteId) {
        this.siteIds.add(siteId);
    }

    public void setStudySiteNames(String siteName) {
        this.studySiteNames.add(siteName);
    }

    public void setStdSiteRights(String stdSiteRight) {
        this.stdSiteRights.add(stdSiteRight);
    }

    public void setUserIds(String userId) {
        this.userIds.add(userId);
    }

    public void setStudyIds(String studyId) {
        this.studyIds.add(studyId);
    }

    public void setStudySiteIndents(String studyIndent) {
        this.studySiteIndents.add(studyIndent);
    }

    public void setStudySiteParents(String studySiteParent) {
        this.studySiteParents.add(studySiteParent);
    }

    /**
     * To get the parent child hierarchy of organizations for a particulat
     * studySite calls Stored procedure
     * 
     * @param int
     *            accId - Account Id
     * @param int
     *            userId - User Id
     * @param int
     *            studyId - Study Id
     */
    public void getStudySiteTree(int accId, int userId, int studyId) {
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_USER.SP_STUDYSITETREE(?,?,?,?)}");
            cstmt.setInt(1, accId);
            cstmt.setInt(2, userId);
            cstmt.setInt(3, studyId);
            cstmt.registerOutParameter(4, OracleTypes.CURSOR);
            cstmt.execute();
            ResultSet rs = (ResultSet) cstmt.getObject(4);
            while (rs.next()) {
                setStdSiteRightsIds(new Integer(rs.getInt("siteid")));
                setSiteIds(rs.getString("usersiteid"));
                setStdSiteRights(rs.getString("siteright"));
                setStudySiteNames(rs.getString("sitename"));
                setStudySiteIndents(rs.getString("indent"));
                /*
                 * parentid is id of the main head parent site whose sub sites
                 * have been formed eg if B is child of A and C is child of B
                 * then the main parent is A for both
                 */
                setStudySiteParents(rs.getString("parentid"));
            }

            rs.close();

        } catch (Exception e) {
            Rlog.fatal("stdSiteRights",
                    "EXCEPTION in getUserSiteTree, excecuting Stored Procedure "
                            + e);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    /**
     * To update the rights for all the organizations of a particualr studySite
     * user,
     * 
     * @param Array
     *            of pkStudySites
     * @param Array
     *            of corresponding site right(0-if rights are not given, 1-if
     *            rights are given)
     * @param Logged
     *            in user Id
     * @param IP
     *            of logged in user
     * @returns 0 for successful update and -1 for error
     */
    public int updateStudySite(String[] pkStudySites, String[] siteIds,
            String[] rights, int studyId, int userId, String ipAdd, String creator) {
        int ret = 1;
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            Rlog.debug("stdSiteRights", "In StdSiteDao upadetStudySite() ");
            ArrayDescriptor inPkStudySites = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);

            ARRAY pkStudySitesArray = new ARRAY(inPkStudySites, conn,
                    pkStudySites);

            ArrayDescriptor inSiteIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY studyIdsArray = new ARRAY(inSiteIds, conn, siteIds);

            ArrayDescriptor inRights = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY rightsArray = new ARRAY(inRights, conn, rights);

            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_USER.SP_UPDATESTUDYSITERIGHTS(?,?,?,?,?,?,?,?)}");
            cstmt.setArray(1, pkStudySitesArray);
            cstmt.setArray(2, studyIdsArray);
            cstmt.setArray(3, rightsArray);
            cstmt.setInt(4, studyId);
            cstmt.setInt(5, userId);
            cstmt.setString(6, ipAdd);
            cstmt.registerOutParameter(7, java.sql.Types.INTEGER);
            cstmt.setString(8, creator);
            cstmt.execute();
            ret = cstmt.getInt(7);

            return ret;
        } catch (Exception e) {
            Rlog.fatal("stdSiteRights",
                    "EXCEPTION in upadetStudySite, excecuting Stored Procedure "
                            + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    /**
     * Gets all sites with New right for an account user
     * 
     * @param Account
     *            Id
     * @param User
     *            Id
     */
    /*
     * public void getSitesWithNewRight(int accId, int userId) { int rows = 0;
     * PreparedStatement pstmt = null; Connection conn = null;
     * 
     * try { conn = getConnection(); pstmt = conn.prepareStatement( "select
     * pk_site, site_name from erv_usersites where fk_account = ? and fk_user = ?
     * and (usersite_right = 5 or usersite_right = 7) order by SITE_NAME ");
     * pstmt.setInt(1,accId); pstmt.setInt(2,userId); ResultSet rs =
     * pstmt.executeQuery();
     * 
     * while (rs.next()){ setUserSiteSiteIds(rs.getString("pk_site"));
     * setUserSiteNames(rs.getString("site_name")); }
     * 
     * }catch (SQLException ex) { Rlog.fatal("codelst","EXCEPTION IN FETCHING
     * FROM ERV_USERSITE in getSitesWithNewRight in UserSiteDao " + ex);
     * }finally{ try{ if (pstmt != null) pstmt.close(); }catch (Exception e) { }
     * try { if (conn!=null) conn.close(); }catch (Exception e) {} } }
     */

    /**
     * Get organizations with view access for an account user for a particular
     * study (right>=4)
     * 
     * @param int
     *            accId - Account Id
     * @param int
     *            userId - User Id
     * @param int
     *            studyId - Study Id
     */
    public void getStudySitesWithViewRight(int accId, int userId, int studyId) {
        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("select pk_study_site_rights, pk_site, site_name "
                            + " from er_study_site_rights, er_site "
                            + " where fk_site = pk_site "
                            + " and fk_account=? and fk_user=? "
                            + " and fk_study=? and user_study_site_rights = 1 ");

            pstmt.setInt(1, accId);
            pstmt.setInt(2, userId);
            pstmt.setInt(3, studyId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                setStdSiteRightsIds(new Integer(rs
                        .getInt("pk_study_site_rights")));
                setSiteIds(rs.getString("pk_site"));
                setStudySiteNames(rs.getString("site_name"));
            }

            rs.close();
        } catch (Exception e) {
            Rlog.fatal("stdSiteRights",
                    "EXCEPTION in getStudySitesWithViewRight " + e);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    // ------------

    /**
     * Calls pkg_user.SP_CREATE_STUDYSITE_DATA() * to create er_studySite data
     * for all the organizations(sites) for user's study site *
     * 
     * @param user :
     *            Pk_user *
     * @param account :
     *            pk of user account *
     * @param studyId :
     *            pk of study *
     * @param creator :
     *            pk of creator (fk_user) *
     * @param ipAdd :
     *            ip add of client machine * returns : 0 for successful update,
     *            -1 for error
     */

    public int createStudySiteRightsData(int user, int account, int studyId,
            int creator, String ipAdd) {
        CallableStatement cstmt = null;
        Connection conn = null;
        int ret = 0;

        try {
            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_USER.SP_CREATE_STUDYSITE_DATA(?,?,?,?,?,?)}");
            cstmt.setInt(1, user);
            cstmt.setInt(2, account);
            cstmt.setInt(3, studyId);
            cstmt.setInt(4, creator);
            cstmt.setString(5, ipAdd);

            cstmt.registerOutParameter(6, java.sql.Types.INTEGER);

            cstmt.execute();

            Rlog.debug("stdSiteRights",
                    "*****in createStudySiteRightsData after procedure***");

            ret = cstmt.getInt(6);

            Rlog.debug("stdSiteRights",
                    "*****in createStudySiteRightsData after procedure ret val***"
                            + ret);
            return ret;

        } catch (Exception e) {
            Rlog.fatal("usersite",
                    "EXCEPTION in SP_CREATE_STUDYSITE_DATA, excecuting Stored Procedure "
                            + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    // ------------

    public int deleteStdSiteRights(int studyId, int userId) {
        Rlog.debug("stdSiteRights", "*****studyId ***" + studyId);
        Rlog.debug("stdSiteRights", "*****userId ***" + userId);
        PreparedStatement pstmt = null;
        Connection conn = null;
        int ret = 0;
        Rlog.debug("stdSiteRights", "*****inside deleteStdSiteRights ***");
        try {

            conn = getConnection();

            pstmt = conn
                    .prepareStatement("delete from er_study_site_rights where fk_user = ? and fk_study = ?");

            pstmt.setInt(1, userId);
            pstmt.setInt(2, studyId);

            ResultSet rs = pstmt.executeQuery();

        } catch (Exception ex) {
            Rlog.fatal("stdSiteRights", "Error in deleteStdSiteRights  " + ex);
            ret = -1;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();

            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();

            } catch (Exception e) {
            }
        }
        return ret;

    }
    
    public int stdSiteRightOfOrgTouser(String studyId,String userId,String siteId){
    	 PreparedStatement pstmt = null;
         Connection conn = null;
         ResultSet rs=null;
         int ret = 0;
         String sql="";
       
         try{
        	 conn = getConnection(); 
        	 sql=" select user_study_site_rights from er_study_site_rights" +
        	 		" where fk_study=? and fk_site=? and fk_user=? ";
        	 pstmt = conn.prepareStatement(sql);
        	 pstmt.setString(1, studyId);
        	 pstmt.setString(2, siteId);
        	 pstmt.setString(3, userId);
        	 rs=pstmt.executeQuery();
        	 while(rs.next()){
        		 ret=rs.getInt("user_study_site_rights");
        	 }
         }catch (Exception ex) {
             Rlog.fatal("stdSiteRights", "Error in deleteStdSiteRights  " + ex);
             ret = -1;
         } finally {
             try {
                 if (pstmt != null)
                     pstmt.close();

             } catch (Exception e) {
             }
             try {
                 if (conn != null)
                     conn.close();

             } catch (Exception e) {
             }
         } 
         return ret;
    }

    /*
     * get all the organizations of a study for which multiple access rights are
     * given for the loggedin user. select pk_site, site_name from er_site,
     * er_study_site_rights where pk_site = fk_site and fk_account=50 and
     * fk_user=100 and fk_study = 120 and user_study_site_rights =1;
     */

} // end of main class

