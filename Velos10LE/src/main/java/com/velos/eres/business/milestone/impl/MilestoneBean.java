/*
 * Classname		MilestoneBean.class
 * 
 * Version information	1.0
 *
 * Date					05/28/2002
 * 
 * Copyright notice    Velos Inc.
 */

package com.velos.eres.business.milestone.impl;

import java.io.Serializable;
import java.sql.Date;
import java.text.DecimalFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.json.JSONObject;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The Rights CMP entity bean.<br>
 * <br>
 * 
 * @author Sonika
 * @version 1.0
 * @ejbHome MilestoneHome
 * @ejbRemote MilestoneRObj
 */
@Entity
@Table(name = "er_milestone")
public class MilestoneBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3257288036927813172L;

    /**
     * the Milestone Id
     */
    public int id;

    /**
     * the Study Id to which Milestone is related
     */
    public Integer milestoneStudyId;

    /**
     * the Milestone Type
     */
    public String milestoneType;

    /**
     * the Milestone Calendar
     */
    public Integer milestoneCalId;

    /**
     * the Milestone Rule
     */
    public Integer milestoneRuleId;

    /**
     * the Milestone Amount
     */
    public Double milestoneAmount;

    /**
     * the Milestone Visit
     */
    public Integer milestoneVisit;

    /**
     * the Milestone Event
     */
    public Integer milestoneEvent;

    /**
     * the Milestone Count
     */
    public Integer milestoneCount;

    /**
     * the Milestone Delflag
     */
    public String milestoneDelFlag;

    /**
     * the Milestone UserTo
     */
    public String milestoneUsersTo;

    /*
     * creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * IP Address
     */
    public String ipAdd;

    /**
     * the Milestone Visit foreign key
     */
    public Integer milestoneVisitFK;

    
    /**
     * Milestone Limit
     */
    public String milestoneLimit;

    
    /**
     * Milestone Rule Status type
     */
    public String milestoneStatus ;

    
    /**
     * Milestone Rule Payment type
     */
    public String milestonePayType ;

    /**
     * Milestone Rule Payment Due by number
     */
    public String milestonePaydDueBy ;
    
    /**
     * Milestone Rule Payment Due by Unit
     */
    public String milestonePayByUnit ;
    
    /**
     * Milestone Rule Payment For
     */
    public String  milestonePayFor;
    
    /**
     * Milestone Rule Event Status
     */
    public String  milestoneEventStatus;
    
    /**
     * Milestone Rule Achieved Count
     */
    public String  milestoneAchievedCount;
    
    /**
     * Milestone Rule Last Checked On
     */
    public Date lastCheckedOn;
 
    //Added by Manimaran for enh#FIN10.
    
    /**
     * Description for Additional Milestone
     */
    
    public String milestoneDescription;
    
    /**
     * The Milestone Status foreign key
     */
    public Integer milestoneStatFK;
    
    /**
     * Milestone DateFrom
     */
    
    public java.util.Date milestoneDateFrom;
    
    /**
     * Milestone DateTo
     */
    
    public java.util.Date milestoneDateTo;
    
	/**
     * Milestone Holdback
     */
    
    public Float holdBack;

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_MILESTONE", allocationSize=1)
    @Column(name = "PK_MILESTONE")
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "MILESTONE_AMOUNT")
    public String getMilestoneAmount() {
        // return StringUtil.floatToString(this.milestoneAmount);

        double db = milestoneAmount.doubleValue();
        DecimalFormat df = new DecimalFormat("#######0.00");
        return df.format(db);
    }

    public void setMilestoneAmount(String milestoneAmount) {

        // if(milestoneAmount != null && milestoneAmount.trim().length() > 0 ) {
        // this.milestoneAmount =Float.valueOf(milestoneAmount);
        // }
        if (milestoneAmount != null) {
            this.milestoneAmount = new Double(milestoneAmount);
        }

    }

    @Column(name = "FK_CAL")
    public String getMilestoneCalId() {
        return StringUtil.integerToString(this.milestoneCalId);
    }

    public void setMilestoneCalId(String milestoneCalId) {
        if (milestoneCalId != null && milestoneCalId.trim().length() > 0) {
            this.milestoneCalId = Integer.valueOf(milestoneCalId);
        }
    }

    @Column(name = "MILESTONE_COUNT")
    public String getMilestoneCount() {
        return StringUtil.integerToString(this.milestoneCount);
    }

    public void setMilestoneCount(String milestoneCount) {
        if (milestoneCount != null && milestoneCount.trim().length() > 0) {
            this.milestoneCount = Integer.valueOf(milestoneCount);
        }
    }

    @Column(name = "MILESTONE_DELFLAG")
    public String getMilestoneDelFlag() {
        return this.milestoneDelFlag;
    }

    public void setMilestoneDelFlag(String milestoneDelFlag) {
        this.milestoneDelFlag = milestoneDelFlag;
    }

    @Column(name = "FK_EVENTASSOC")
    public String getMilestoneEvent() {
        return StringUtil.integerToString(this.milestoneEvent);
    }

    public void setMilestoneEvent(String milestoneEvent) {
        if (milestoneEvent != null && milestoneEvent.trim().length() > 0) {
            this.milestoneEvent = Integer.valueOf(milestoneEvent);
        }
    }

    @Column(name = "FK_CODELST_RULE")
    public String getMilestoneRuleId() {
        return StringUtil.integerToString(this.milestoneRuleId);
    }

    public void setMilestoneRuleId(String milestoneRuleId) {
        if (milestoneRuleId != null && milestoneRuleId.trim().length() > 0) {
            this.milestoneRuleId = Integer.valueOf(milestoneRuleId);
        }
    }

    @Column(name = "FK_STUDY")
    public String getMilestoneStudyId() {
        return StringUtil.integerToString(this.milestoneStudyId);
    }

    public void setMilestoneStudyId(String milestoneStudyId) {
        if (milestoneStudyId != null) {
            this.milestoneStudyId = Integer.valueOf(milestoneStudyId);
        }
    }

    @Column(name = "MILESTONE_TYPE")
    public String getMilestoneType() {
        return this.milestoneType;
    }

    public void setMilestoneType(String milestoneType) {
        this.milestoneType = milestoneType;
    }

    @Column(name = "MILESTONE_USERSTO")
    public String getMilestoneUsersTo() {
        return this.milestoneUsersTo;
    }

    public void setMilestoneUsersTo(String milestoneUsersTo) {
        this.milestoneUsersTo = milestoneUsersTo;
    }

    @Column(name = "MILESTONE_VISIT")
    public String getMilestoneVisit() {
        return StringUtil.integerToString(this.milestoneVisit);
    }

    public void setMilestoneVisit(String milestoneVisit) {
        if (milestoneVisit != null && milestoneVisit.trim().length() > 0) {
            this.milestoneVisit = Integer.valueOf(milestoneVisit);
        }
    }

    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null && modifiedBy.trim().length() > 0) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }

    }

    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
        if (creator != null && creator.trim().length() > 0) {
            this.creator = Integer.valueOf(creator);
        }

    }

    @Column(name = "FK_VISIT")
    public String getMilestoneVisitFK() {
        return StringUtil.integerToString(this.milestoneVisitFK);
    }

    public void setMilestoneVisitFK(String milestoneVisitFK) {
        if (milestoneVisitFK != null && milestoneVisitFK.trim().length() > 0) {

            this.milestoneVisitFK = Integer.valueOf(milestoneVisitFK);
        }
    }
    
    //Added by Manimaran for enh#FIN10.
    @Column(name = "MILESTONE_DESCRIPTION")
    public String getMilestoneDescription() {
        return this.milestoneDescription;
    }

    public void setMilestoneDescription(String milestoneDescription) {
        this.milestoneDescription = milestoneDescription;
    }
    
    //KM- #D-FIN7
    @Column(name = "FK_CODELST_MILESTONE_STAT")
    public String getMilestoneStatFK() {
        return StringUtil.integerToString(this.milestoneStatFK);
    }

    public void setMilestoneStatFK(String milestoneStatFK) {
        if (milestoneStatFK != null && milestoneStatFK.trim().length() > 0) {

            this.milestoneStatFK = Integer.valueOf(milestoneStatFK);
        }
    }
    
    @Column(name = "MILESTONE_DATE_FROM")
    public java.util.Date getMilestoneDtFrom() {
        return this.milestoneDateFrom;
    }

    public void setMilestoneDtFrom(java.util.Date milestoneDateFrom) {
        this.milestoneDateFrom = milestoneDateFrom;
    }
    
    @Column(name = "MILESTONE_DATE_TO")
    public java.util.Date getMilestoneDtTo() {
        return this.milestoneDateTo;
    }

    public void setMilestoneDtTo(java.util.Date milestoneDateTo) {
        this.milestoneDateTo = milestoneDateTo;
    }
    
    @Transient
    public String getMilestoneDateFrom() {

        return DateUtil.dateToString(getMilestoneDtFrom());
    }

    public void setMilestoneDateFrom(String milestoneDateFrom) {

    	setMilestoneDtFrom(DateUtil.stringToDate(milestoneDateFrom,
                null));
    }
    
    @Transient
    public String getMilestoneDateTo() {

        return DateUtil.dateToString(getMilestoneDtTo());
    }

    public void setMilestoneDateTo(String milestoneDateTo) {

    	setMilestoneDtTo(DateUtil.stringToDate(milestoneDateTo,
                null));
    }
    
	@Column(name = "MILESTONE_HOLDBACK")
    public Float getHoldBack() {
    	return holdBack;
    }

    public void setHoldBack(Float holdBack) {
    	this.holdBack = holdBack;
    }
    
    // END OF GETTER SETTER METHODS

        /**
     * Updates the Milestone in the Database
     * 
     * @param msk
     *            State Kepper of milestone
     * @return 0 when updation is successful and -2 when some exception occurs
     */
    public int updateMilestone(MilestoneBean msk) {
        Rlog.debug("milestone", "MilestoneBean.updateMilestone line 1");
        try {
            Rlog.debug("milestone", "MilestoneBean.updateMilestone line 2");
            setMilestoneStudyId(msk.getMilestoneStudyId());
            Rlog.debug("milestone",
                    "MilestoneBean.updateMilestone line 3 getMilestoneStudyId "
                            + getMilestoneStudyId());
            setMilestoneType(msk.getMilestoneType());
            Rlog.debug("milestone",
                    "MilestoneBean.updateMilestone line 4 getMilestoneType "
                            + getMilestoneType());
            setMilestoneCalId(msk.getMilestoneCalId());
            Rlog.debug("milestone",
                    "MilestoneBean.updateMilestone line 5 getMilestoneCalId "
                            + getMilestoneCalId());
            setMilestoneRuleId(msk.getMilestoneRuleId());
            Rlog.debug("milestone",
                    "MilestoneBean.updateMilestone line 6 getMilestoneRuleId "
                            + getMilestoneRuleId());
            setMilestoneAmount(msk.getMilestoneAmount());
            Rlog.debug("milestone",
                    "MilestoneBean.updateMilestone line 7 getMilestoneAmount "
                            + getMilestoneAmount());
            setMilestoneVisit(msk.getMilestoneVisit());
            Rlog.debug("milestone",
                    "MilestoneBean.updateMilestone line 8 getMilestoneVisit "
                            + getMilestoneVisit());
            setMilestoneEvent(msk.getMilestoneEvent());
            Rlog.debug("milestone",
                    "MilestoneBean.updateMilestone line 9 getMilestoneEvent "
                            + getMilestoneEvent());
            setMilestoneCount(msk.getMilestoneCount());
            Rlog.debug("milestone",
                    "MilestoneBean.updateMilestone line 10 getMilestoneCount"
                            + getMilestoneCount());
            setMilestoneUsersTo(msk.getMilestoneUsersTo());
            Rlog.debug("milestone",
                    "MilestoneBean.updateMilestone line 11 getMilestoneUsersTo "
                            + getMilestoneUsersTo());
            setMilestoneDelFlag(msk.getMilestoneDelFlag());
            Rlog.debug("milestone",
                    "MilestoneBean.updateMilestone line 12 getMilestoneDelFlag "
                            + getMilestoneDelFlag());
            setCreator(msk.getCreator());
            Rlog.debug("milestone",
                    "MilestoneBean.updateMilestone line 13 getCreator "
                            + getCreator());
            setModifiedBy(msk.getModifiedBy());
            Rlog.debug("milestone",
                    "MilestoneBean.updateMilestone line 14 getModifiedBy "
                            + getModifiedBy());
            setIpAdd(msk.getIpAdd());
            Rlog.debug("milestone",
                    "MilestoneBean.updateMilestone line 15 getIpAdd "
                            + getIpAdd());
            setMilestoneVisitFK(msk.getMilestoneVisitFK());
            Rlog.debug("milestone",
                    "MilestoneBean.updateMilestone line 8 getMilestoneVisitFK "
                            + getMilestoneVisitFK());
            
            
            //setMilestoneAchievedCount(msk.getMilestoneAchievedCount()) ;
            setMilestoneEventStatus( msk.getMilestoneEventStatus());
            setMilestoneLimit( msk.getMilestoneLimit()) ;
            setMilestonePayByUnit( msk.getMilestonePayByUnit()) ;
            Rlog.debug("milestone", "set all milestone values 1"); 
            setMilestonePaydDueBy( msk.getMilestonePaydDueBy()) ;
            setMilestonePayFor( msk.getMilestonePayFor()) ;
            setMilestonePayType( msk.getMilestonePayType()) ;
            setMilestoneStatus( msk.getMilestoneStatus());
            setMilestoneDescription(msk.getMilestoneDescription());
            setMilestoneStatFK(msk.getMilestoneStatFK());
            //Fin-22373
            setMilestoneDateFrom(msk.getMilestoneDateFrom());
            setMilestoneDateTo(msk.getMilestoneDateTo());
			setHoldBack(msk.getHoldBack());
            Rlog.debug("milestone", "set all milestone values"); 
            

            return 0;
        } catch (Exception e) {
            Rlog.fatal("milestone",
                    "EXCEPTION IN MilestoneBean.updateMilestone " + e);
            return -2;
        }
    }

    public MilestoneBean() {

    }

    public MilestoneBean(int id, String milestoneStudyId, String milestoneType,
            String milestoneCalId, String milestoneRuleId,
            String milestoneAmount, String milestoneVisit,
            String milestoneEvent, String milestoneCount,
            String milestoneDelFlag, String milestoneUsersTo, String creator,
            String modifiedBy, String ipAdd, String milestoneVisitFK, 
            String milestoneAchievedCount,String milestoneEventStatus, 
            String milestoneLimit,String milestonePayByUnit,
            String milestonePaydDueBy,  String milestonePayFor, String milestonePayType,String milestoneStatus,
            String milestoneDescription, String milestoneStatFK, String dateFrom, String dateTo, Float holdBack ) {
        super();
        // TODO Auto-generated constructor stub
        Rlog.debug("milestone", "set all milestone values,constructor"); 
        setId(id);
        setMilestoneStudyId(milestoneStudyId);
        setMilestoneType(milestoneType);
        setMilestoneCalId(milestoneCalId);
        
        
        
        setMilestoneRuleId(milestoneRuleId);
        
        setMilestoneAmount(milestoneAmount);
        
        setMilestoneVisit(milestoneVisit);
        
        setMilestoneEvent(milestoneEvent);
        setMilestoneCount(milestoneCount);
        
        setMilestoneDelFlag(milestoneDelFlag);
        setMilestoneUsersTo(milestoneUsersTo);
        setCreator(creator);
        
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setMilestoneVisitFK(milestoneVisitFK);
        
        setMilestoneAchievedCount(milestoneAchievedCount) ;
        setMilestoneEventStatus( milestoneEventStatus);
        setMilestoneLimit( milestoneLimit) ;
        setMilestonePayByUnit( milestonePayByUnit) ;
        setMilestonePaydDueBy( milestonePaydDueBy) ;
        setMilestonePayFor( milestonePayFor) ;
        setMilestonePayType( milestonePayType) ;
        setMilestoneStatus( milestoneStatus);
        setMilestoneDescription(milestoneDescription);
        setMilestoneStatFK(milestoneStatFK);
        // Fin-22373
        setMilestoneDateFrom(dateFrom);
        setMilestoneDateTo(dateTo);
		setHoldBack(holdBack);
    }

  	public void setLastCheckedOn(Date lastCheckedOn) {
		this.lastCheckedOn = lastCheckedOn;
	}

	@Column(name = "milestone_achievedcount")
	public String getMilestoneAchievedCount() {
		return milestoneAchievedCount;
	}

	
	public void setMilestoneAchievedCount(String milestoneAchievedCount) {
		this.milestoneAchievedCount = milestoneAchievedCount;
	}

	@Column(name = "milestone_eventstatus")
	public String getMilestoneEventStatus() {
		return milestoneEventStatus;
	}

	public void setMilestoneEventStatus(String milestoneEventStatus) {
		this.milestoneEventStatus = milestoneEventStatus;
	}

	@Column(name = "milestone_limit")
	public String getMilestoneLimit() {
		return milestoneLimit;
	}

	public void setMilestoneLimit(String milestoneLimit) {
		this.milestoneLimit = milestoneLimit;
	}

	//not in use
	@Transient
		public String getMilestonePayByUnit() { 
		return milestonePayByUnit;
	}

	public void setMilestonePayByUnit(String milestonePayByUnit) {
		this.milestonePayByUnit = milestonePayByUnit;
	}
	
	
//	not in use
	@Transient
	public String getMilestonePaydDueBy() {
		return milestonePaydDueBy;
	}

	public void setMilestonePaydDueBy(String milestonePaydDueBy) {
		this.milestonePaydDueBy = milestonePaydDueBy;
	}

	@Column(name = "milestone_payfor")
	public String getMilestonePayFor() {
		return milestonePayFor;
	}

	public void setMilestonePayFor(String milestonePayFor) {
		this.milestonePayFor = milestonePayFor;
	}

	@Column(name = "milestone_paytype")
	public String getMilestonePayType() {
		return milestonePayType;
	}

	public void setMilestonePayType(String milestonePayType) {
		this.milestonePayType = milestonePayType;
	}

	
    
    @Column(name = "milestone_status")
    public String getMilestoneStatus() {
		return milestoneStatus;
	}

	public void setMilestoneStatus(String milestoneStatus) {
		this.milestoneStatus = milestoneStatus;
	}


	   @Transient
		public Date getLastCheckedOn() {
			return lastCheckedOn;
		}
	   
/**
 * Set the bean properties from data record; 
 * @param dataRecord JSONObject.
 * @param op operation type ie C: Create U: update
 * @return
 */
	   //DFIN-13 BK MAR-16-2011
	   public int setMileStoneBeanJSON(JSONObject dataRecord,char op){
		   try{	   
			String amount = dataRecord.getString("amount");
			amount = (StringUtil.isEmpty(amount) ||
					amount.equals("undefined") || amount.equals("null"))?null:amount;
			/*To remove all the comma from amount */ 
			if(null!=amount && amount.indexOf(",")!=-1)
				 amount=amount.replaceAll(",", "");
			this.setMilestoneAmount(amount);
			
			String holdBack = dataRecord.getString("holdBack");
			holdBack = (StringUtil.isEmpty(holdBack) ||
					holdBack.equals("undefined") || holdBack.equals("null"))?"0.00":holdBack;	
			//Modified for Bug#14847 and Bug#15036 : Raviesh
			this.setHoldBack(((holdBack.equals("0"))||(holdBack.equals(".00"))||(holdBack.equals("0.00")))?null:(Float.parseFloat(holdBack)));
			this.setMilestonePayType(dataRecord.getString("payTypeId"));
			this.setMilestonePayFor(dataRecord.getString("payForId"));
			this.setMilestoneStatFK(dataRecord.getString("mileStatusId"));
			/* FIN-22373 09-Oct-2012 -Sudhir*/
			if(!("AM".equalsIgnoreCase(dataRecord.getString("mileTypeId")))){
				if(dataRecord.getString("datefrom")!=null && !dataRecord.getString("datefrom").equalsIgnoreCase("undefined"))
					this.setMilestoneDtFrom(DateUtil.stringToDate(dataRecord.getString("datefrom"),null));//"MM/dd/yyyy"));
				else
					this.setMilestoneDtTo(null);
				
				if(dataRecord.getString("dateto")!=null && !dataRecord.getString("dateto").equalsIgnoreCase("undefined"))
					this.setMilestoneDtTo(DateUtil.stringToDate(dataRecord.getString("dateto"),null));//"MM/dd/yyyy"));
				else
					this.setMilestoneDtTo(null);
			 }
			if(op == 'C'){
	    		  this.setMilestoneType(dataRecord.getString("mileTypeId"));
	    		  
	    	  }
		    if("PM".equals(dataRecord.getString("mileTypeId"))){	
		    	 String limit = dataRecord.getString("limit");
				 limit = (StringUtil.isEmpty(limit) ||
					limit.equals("undefined") || limit.equals("null"))?null:limit;
				
		    	 this.setMilestoneLimit(limit);
		    	 this.setMilestoneStatus(dataRecord.getString("patStatusId"));
		    	 
		    	 String patCount = dataRecord.getString("patCount");
		    	 patCount = (StringUtil.isEmpty(patCount) ||
		    			 patCount.equals("undefined") || patCount.equals("null"))?null:patCount;
		    	 this.setMilestoneCount(patCount);
		    	  
		     	}
		    else if("VM".equals(dataRecord.getString("mileTypeId")) || "EM".equals(dataRecord.getString("mileTypeId"))){
		    	String limit = dataRecord.getString("limit");
		    	limit = (StringUtil.isEmpty(limit) ||
						limit.equals("undefined") || limit.equals("null"))?null:limit;
				
		    	 this.setMilestoneLimit(limit);    	 
		    	 this.setMilestoneStatus(dataRecord.getString("patStatusId"));
		    	 
		    	 String patCount = dataRecord.getString("patCount");
		    	 patCount = (StringUtil.isEmpty(patCount) ||
		    			 patCount.equals("undefined") || patCount.equals("null"))?null:patCount;
		    	 this.setMilestoneCount(patCount);
		    	 
		    	 this.setMilestoneRuleId(dataRecord.getString("mileRuleId"));
		    	 this.setMilestoneEventStatus(dataRecord.getString("evtStatusId"));		    	 
		    	 this.setMilestoneRuleId(dataRecord.getString("mileRuleId"));
		    	 if(op == 'C'){
		    	 this.setMilestoneCalId(dataRecord.getString("calendarId"));
		    	 this.setMilestoneVisitFK(dataRecord.getString("visitId"));		    	 
		    	 if("EM".equals(dataRecord.getString("mileTypeId"))){
		    		this.setMilestoneEvent(dataRecord.getString("eventId")); 
		    	 }
		    	}
		    	 		    	 
		     }
		    else if("SM".equals(dataRecord.getString("mileTypeId"))){	
		    	String limit = dataRecord.getString("limit");
		    	limit = (StringUtil.isEmpty(limit) ||
						limit.equals("undefined") || limit.equals("null"))?null:limit;
				
		    	 this.setMilestoneLimit(limit);    	 
		    	 this.setMilestoneStatus(dataRecord.getString("stdStatusId"));
		    	 		    	 
		     }
		    else{
		    	this.setMilestoneDescription(dataRecord.getString("mileDesc"));	 
		    }      
		    
		   }
		   catch(Exception e){
			   Rlog.fatal("milestone",
	                    "EXCEPTION IN MilestoneBean.setMileStoneBean(JSONObject,char) " + e);
	            return -2;
			   
		   }
		   return 0;		   
		   
	   }
	   
}// end of class

