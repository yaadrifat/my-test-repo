/*
 * Classname : 				ERSQLS
 *
 * Version information		1.0
 *
 * Date 					03/06/2001
 *
 * Copyright notice: 		Velos Inc
 *
 * Author 					Sonia Sahni
 */

package com.velos.eres.business.common;

/**
 * This class is to be used as a central repository to store the SQLs used in ER
 *
 * @author Sonia
 * @version 1.0 03/06/2001
 */

public interface ERSQLS {
    /* SQLS */

    public static final String erCodeLst = "select PK_CODELST, CODELST_SUBTYP, CODELST_DESC , CODELST_SEQ,codelst_hide,codelst_custom_col1,codelst_custom_col  from er_codelst where rtrim(CODELST_TYPE) = ? order by  CODELST_SEQ";
    public static final String erCodeLstWithoutHide = "select PK_CODELST, CODELST_SUBTYP, CODELST_DESC , CODELST_SEQ,codelst_hide,codelst_custom_col1,codelst_custom_col  from er_codelst where rtrim(CODELST_TYPE) = ? AND codelst_hide = 'N' order by  CODELST_SEQ";

    public static final String erCodeLstWithCustom = "select PK_CODELST, CODELST_SUBTYP, CODELST_DESC , CODELST_SEQ ,codelst_hide from er_codelst where rtrim(CODELST_TYPE) = ? and codelst_custom_col = ? order by   CODELST_SEQ ";

    public static final String erCodeLstWithLikeCustomFilter = "select PK_CODELST, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ, codelst_hide from er_codelst where rtrim(CODELST_TYPE) = ? and ',' || codelst_custom_col || ',' like ? order by CODELST_SEQ ";

    // Added by RK: 05/06/2005 - to incorporate accont based codelst retrieval
    public static final String erCodeLstByAccount = "SELECT pk_codelst, codelst_subtyp, codelst_desc, codelst_seq  ,codelst_hide "
            + "FROM ER_CODELST WHERE codelst_type = ? AND ? NOT IN (SELECT DISTINCT fk_account FROM ER_CODELST_ACC "
            + "WHERE  fk_codelst IN (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ?)) UNION SELECT pk_codelst, "
            + "codelst_subtyp, NVL(b.codelst_desc,a.codelst_desc),codelst_seq ,codelst_hide FROM ER_CODELST a, ER_CODELST_ACC b "
            + "WHERE a.codelst_type = ? AND Pk_codelst = fk_codelst AND isvisible = 1 AND b.fk_account = ? ORDER BY  codelst_seq";

    //KM-to hiding groups in the dropdown
    public static final String accGroups = "select PK_GRP, GRP_NAME   from er_grps where fk_account = ? and grp_hidden <> 1 order by  GRP_NAME";

    //KM- 22Aug08:to get all groups in the dropdown
    public static final String accGroupsNoHidden = "select PK_GRP, GRP_NAME   from er_grps where fk_account = ? order by  GRP_NAME";

    //KM-to hiding organizations in the dropdown
    public static final String accSites = "select PK_SITE, SITE_NAME from er_site where fk_account = ? and site_hidden <> 1 order by  lower(SITE_NAME)";

    //KM- 22Aug08:to get all organizations in the dropdown
    public static final String accSitesNoHidden = "select PK_SITE, SITE_NAME from er_site where fk_account = ?  order by  lower(SITE_NAME)";


    public static final String usrGroups = "select PK_GRP, GRP_NAME from er_grps ,er_usrgrp WHERE er_usrgrp.fk_user = ? and  er_grps.pk_grp = er_usrgrp.fk_grp and er_grps.fk_account = ? order by  GRP_NAME";

    public static final String erCodeLstById = "select PK_CODELST, CODELST_SUBTYP, CODELST_DESC , CODELST_SEQ  ,codelst_hide from er_codelst where pk_codelst = ? order by   CODELST_SEQ";

    public static final String erCodeLstAvTAreas = "select PK_CODELST, CODELST_SUBTYP, CODELST_DESC , CODELST_SEQ  ,codelst_hide from er_codelst where rtrim(CODELST_TYPE) = ? and PK_CODELST not in (select FK_CODELST_TAREA from er_studynotify where FK_USER = ?) order by  CODELST_DESC, CODELST_SEQ";

    public static final String erCodeLstWithCustom_1 = "select PK_CODELST, CODELST_SUBTYP, CODELST_DESC , CODELST_SEQ  ,codelst_hide from er_codelst " +
    		" where rtrim(CODELST_TYPE) = ? and ',' || codelst_custom_col1 || ',' like ? order by   CODELST_SEQ ";

    public static final String erCodeLstSaveKit = "select PK_CODELST, CODELST_SUBTYP, CODELST_DESC , CODELST_SEQ,codelst_hide,codelst_custom_col1,codelst_custom_col  from er_codelst where rtrim(CODELST_TYPE) = ? and  rtrim(CODELST_SUBTYP) <> 'Kit' order by  CODELST_SEQ";

    public static final String schCodeLstWithCustom_1 = "select PK_CODELST, CODELST_SUBTYP, CODELST_DESC , CODELST_SEQ  ,codelst_hide from sch_codelst " +
	" where rtrim(CODELST_TYPE) = ? and ',' || codelst_custom_col1 || ',' like ? order by   CODELST_SEQ ";

    public static final String erCodeLstWithStudyRole = "select PK_CODELST, CODELST_SUBTYP, CODELST_DESC , CODELST_SEQ  ,codelst_hide from er_codelst " +
	" where rtrim(CODELST_TYPE) = ? and (',' || CODELST_STUDY_ROLE || ',' like ?)" + 
	" order by   CODELST_SEQ ";

    public static final String erCodeLstWith2Filter = "select PK_CODELST, CODELST_SUBTYP, CODELST_DESC , CODELST_SEQ  ,codelst_hide from er_codelst " +
	" where rtrim(CODELST_TYPE) = ? and ',' || codelst_custom_col1 || ',' like ? " +
	" AND rtrim(CODELST_TYPE) = ?  and (',' || CODELST_STUDY_ROLE || ',' like ?)" +
	" order by   CODELST_SEQ ";
    
    public static final String schCodeLstWithStudyRole = "select PK_CODELST, CODELST_SUBTYP, CODELST_DESC , CODELST_SEQ  ,codelst_hide from sch_codelst " +
	" where rtrim(CODELST_TYPE) = ? and (',' || CODELST_STUDY_ROLE || ',' like ?) order by   CODELST_SEQ ";

    
}

