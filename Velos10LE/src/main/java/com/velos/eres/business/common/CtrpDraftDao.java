//sajal dt 03/29/2001

package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


import com.velos.eres.service.util.MC;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.web.codelst.CodelstJB;
import com.velos.eres.web.user.UserJB;

public class CtrpDraftDao extends CommonDAO implements java.io.Serializable {

	private static final long serialVersionUID = 7158101413194792668L;
	public static final String STUDY_STATUS = "studystat";
	private CodeDao StudyStatusDao = null;
	private ArrayList studyStatId;

    private ArrayList fkCodelst;

    private ArrayList fkUser;
    
    
    public ArrayList getStudyStatId() {
		return studyStatId;
	}
	public void setStudyStatId(ArrayList studyStatId) {
		this.studyStatId = studyStatId;
	}
	public ArrayList getFkCodelst() {
		return fkCodelst;
	}
	public void setFkCodelst(ArrayList fkCodelst) {
		this.fkCodelst = fkCodelst;
	}
	public ArrayList getFkUser() {
		return fkUser;
	}
	public void setFkUser(ArrayList fkUser) {
		this.fkUser = fkUser;
	}
 
	  public void setStudyStatId(Integer sStudyStatId) {
	        this.studyStatId.add(sStudyStatId);
	    }
 
	    public void setFkCodelst(Integer sFkCodelst) {
	        this.fkCodelst.add(sFkCodelst);
	    }
	    public void setFkUser(Integer sFkUser) {
	        this.fkUser.add(sFkUser);
	    }
   
 
	 public CtrpDraftDao() {
	    	studyStatId = new ArrayList();
	    	fkCodelst = new ArrayList();
	    	fkUser = new ArrayList();
	    }
    public int unmarkStudyCtrpReportable(String studyIds) {
    	 PreparedStatement pstmt = null;
         Connection conn = null;
         int ret=0;
         String sql="";
         try{
        	 conn = getConnection();
        	 sql="UPDATE ER_STUDY SET STUDY_CTRP_REPORTABLE=0 WHERE PK_STUDY IN "+studyIds;
        	 pstmt = conn.prepareStatement(sql);
       		 ret=pstmt.executeUpdate();
       		 conn.commit();
       	 return ret;
         }catch (SQLException ex) {
             Rlog.fatal("studyCtrp",
                     "CtrpDraftDao.unmarkStudyCtrpReportable EXCEPTION IN UPDATING Study table "
                             + ex);
             return ret;
         } finally {
             try {
                 if (pstmt != null)
                     pstmt.close();
             } catch (Exception e) {
             }
             try {
                 if (conn != null)
                     conn.close();
             } catch (Exception e) {
             }

         }
    	
    }
    public int deleteStudyDrafts(String studyIds,String draftIds) {
    	PreparedStatement pstmt = null;
    	Connection conn = null;
    	int ret=0;
    	String sql="";
    	try{
    		conn = getConnection();
    		sql="UPDATE ER_CTRP_DRAFT SET DELETED_FLAG=1 WHERE PK_CTRP_DRAFT IN "+draftIds;
    		pstmt = conn.prepareStatement(sql);
    		ret=pstmt.executeUpdate();
    		conn.commit();
    		return ret;
    	}catch (SQLException ex) {
    		Rlog.fatal("studyCtrp",
    				"CtrpDraftDao.deleteStudyDrafts EXCEPTION IN Deleting Draft from ER_CTRP_DRAFT table "
    				+ ex);
    		return ret;
    	} finally {
    		try {
    			if (pstmt != null)
    				pstmt.close();
    		} catch (Exception e) {
    		}
    		try {
    			if (conn != null)
    				conn.close();
    		} catch (Exception e) {
    		}
    		
    	}
    	
    }
    public boolean getUserStatusValues() {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement("select DISTINCT(FK_CODELST_STUDYSTAT) from ER_STUDYSTAT_NOTIF");
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
               
            	setFkCodelst(new Integer(rs.getInt("FK_CODELST_STUDYSTAT")));
                rows++;
            }
            return true;
        } catch (SQLException ex) {
            
            Rlog.fatal("ctrp", "EXCEPTION IN FETCHING TABLE "
                    + ex);
            return false;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }
    
    public String getUserValues(Integer fkcodelst,Integer loggedUser) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        UserJB user=new UserJB();
        user.setUserId(loggedUser);
       	user.getUserDetails();
       	String fkSiteId=user.getUserSiteId();
        String option="";
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement("select FK_USER, CREATOR from ER_STUDYSTAT_NOTIF where FK_CODELST_STUDYSTAT = ?");
            pstmt.setInt(1, fkcodelst);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
            	user.setUserId(new Integer(rs.getInt("CREATOR")));
	           	user.getUserDetails();
	           	if(fkSiteId.equals(user.getUserSiteId()))
	           	   {
            	user.setUserId(new Integer(rs.getInt("FK_USER")));
                user.getUserDetails();
            	option=option+"<OPTION value = " + new Integer(rs.getInt("FK_USER")) + ">"+ user.getUserFirstName()+" "+user.getUserLastName() + "</OPTION>";
                rows++;
	           	   }
	           }
            return option;
        } catch (SQLException ex) {
            
            Rlog.fatal("ctrp", "EXCEPTION IN FETCHING TABLE "
                    + ex);
            return "";
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }
	

    public String toStatusUserList(String sName,String dName,Integer loggedUser) {
        ArrayList fkcodelst1=fkCodelst;
        CtrpDraftDao userStatusDao=new CtrpDraftDao();
        CodelstJB codelst=new CodelstJB();
        UserJB user=new UserJB();
        StringBuffer mainStr = new StringBuffer();
        String option="";
        int counter = 0;
        try {
            
            for (int i = 0; i <= fkcodelst1.size()-1; i++) {
            	option=userStatusDao.getUserValues((Integer)fkcodelst1.get(i), loggedUser);
            	codelst.setClstId((Integer)fkcodelst1.get(i));
            	codelst.getCodelstDetails();
            	if(!option.equals("")){
            	mainStr.append("<tr>");
                mainStr.append("<td align='left' width='100%'>");
                mainStr.append(codelst.getClstDesc());
                mainStr.append("<input type='hidden' name="+sName+" id="+sName+" class="+sName+" value="+(Integer)fkcodelst1.get(i)+">");
                mainStr.append("</td>");
                mainStr.append("<td align='left' >");
                mainStr.append("<SELECT ID=" +dName+counter + " NAME="+dName +counter + " class="+dName+" multiple='multiple'  size=\"3\" >");
                mainStr.append(option);
                mainStr.append("</SELECT>");
                mainStr.append("</td>");
                mainStr.append("</tr>");
                counter++;
            	}   
            }            
        } catch (Exception e) {
            return "Exception in toStatusUserList" + e;
        }
        return mainStr.toString();
    }

    public int deleteDraftNotiUser(Integer statusId,String userId ) {
        String sql = null;
        boolean check=false;
        int output = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            sql = "delete from ER_STUDYSTAT_NOTIF where FK_CODELST_STUDYSTAT  = ? and FK_USER in ("+userId+")";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, statusId);
            check=pstmt.execute();
            if(!check)
            {
            	output=1;
            }
            else
            {
            	output=-1;
            }
            return output;

        } catch (SQLException ex) {
            Rlog.fatal("ctrp",
                    "CtrpDraftDao.deleteDraftNotiUser EXCEPTION IN DAO" + ex);
            ex.printStackTrace();
            return -1;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    //22472
    public int deleteCtrpDrftStats(String ctrpDrftStatIds, String draftId) {
        String sql = null;
        int check=0;
        int output = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
        	conn = getConnection();
        	conn.setAutoCommit(false);
            sql = "UPDATE ER_STATUS_HISTORY SET RECORD_TYPE='D',STATUS_ISCURRENT=0 where PK_STATUS IN ("+ctrpDrftStatIds+")";
            pstmt = conn.prepareStatement(sql);
            check=pstmt.executeUpdate();
            if(check>0){
            	output=1;
            	sql = "Update er_status_history set status_iscurrent=1,STATUS_END_DATE=null where pk_status=(select max(pk_status) from er_status_history"
            		+ " where status_modtable='er_ctrp_nci_stat' and status_modpk=? AND RECORD_TYPE <> 'D')";
            	pstmt = conn.prepareStatement(sql);
            	pstmt.setString(1,draftId);
            	check=pstmt.executeUpdate();
            	if(check==0){
            		output = -2;
            	}
            }else{
            	output=-1;
            }
            return output;

        } catch (SQLException ex) {
        	try {
				conn.rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
            Rlog.fatal("ctrp",
                    "CtrpDraftDao.deleteCtrpDrftStats EXCEPTION IN DAO" + ex);
            ex.printStackTrace();
            return -1;
        } finally {
            try {
            	conn.commit();
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }
	
}  
