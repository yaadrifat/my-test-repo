/*
 *  Classname : LabBean
 *
 *  Version information: 1.0
 *
 *  Date: 09/28/2003
 *
 *  Copyright notice: Velos, Inc
 *
 *  Author: Vishal Abrol
 */
package com.velos.eres.business.dynrep.impl;

/*
 * Import Statements
 */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/*
 * End of Import Statements
 */
/**
 * The DynRepBean CMP entity bean.<br>
 * <br>
 * 
 * 
 * @author vishal abrol
 * @created October 25, 2004
 * @vesion 1.0 12/15/2003
 * @ejbHome DynRepHome
 * @ejbRemote DynRepRObj
 */
/*
 * Modified by Sonia Abrol, 04/14/05, added new attributes, repUseUniqueId,
 * repUseDataValue
 * 
 * 
 * JM: 13Jul2006 added NamedQuery
 */
@Entity
@Table(name = "er_dynrep")
@NamedQuery(name = "findDynRepName", query = "SELECT OBJECT(dRep) FROM DynRepBean dRep where LOWER(trim(DYNREP_NAME)) = LOWER(trim(:repName)) AND FK_ACCOUNT = to_number(:accountId)")
public class DynRepBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4050486694535116088L;

    /**
     * The primary key:pk_dynrep
     */

    public int id;

    /**
     * The foreign key reference to Form Lib table.
     */
    public Integer formId;

    /**
     * report name
     */
    public String repName;

    /**
     * report Description
     */
    public String repDesc;

    /**
     * report Filter
     */

    public String repFilter;

    /**
     * report orderby
     */
    public String repOrder;

    /**
     * Foreign key to table er_user
     */
    public Integer userId;

    /**
     * Foreign key to table er_account
     */
    public Integer accId;

    /**
     * report header name
     */
    public String repHdr;

    /**
     * report Footer name
     */
    public String repFtr;

    /**
     * The creator
     */

    public Integer creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    public Integer modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    public String ipAdd;

    /**
     * Description of the Field
     */
    public String repType;

    /**
     * Description of the Field
     */
    public Integer studyId;

    /**
     * Description of the Field
     */
    public Integer perId;

    /**
     * Description of the Field
     */
    public String shareWith;

    /**
     * Stores whether Report is single form report pr multiform
     */
    public String reportCategory;

    /**
     * formId String in case of multiform report. Stores multiple formids
     * delimited by delimiter
     */
    public String formIdStr;

    /**
     * Flag for the report, to Use Unique Field ID as Display Name. However,
     * user will have an option to have a different setting for each field.
     * possible values: 0:true,1:false
     */
    public String repUseUniqueId;

    /**
     * Flag for the report, to Display Data value of multiple choice fields.
     * However, user will have an option to have a different setting for each
     * field. possible values: 0:true,1:false (default, display value will be
     * shown)
     */
    public String repUseDataValue;

    // /////////////////////////////////////////////////////////////////////////////////
    public DynRepBean() {
    }

    public DynRepBean(int id, String formId, String repName, String repDesc,
            String repFilter, String repOrder, String userId, String accId,
            String repHdr, String repFtr, String creator, String modifiedBy,
            String ipAdd, String repType, String studyId, String perId,
            String shareWith, String reportCategory, String formIdStr,
            String repUseUniqueId, String repUseDataValue) {
        super();
        // TODO Auto-generated constructor stub
        setId(id);
        setFormId(formId);
        setRepName(repName);
        setRepDesc(repDesc);
        setRepFilter(repFilter);
        setRepOrder(repOrder);
        setUserId(userId);
        setAccId(accId);
        setRepHdr(repHdr);
        setRepFtr(repFtr);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setRepType(repType);
        setStudyId(studyId);
        setPerId(perId);
        setShareWith(shareWith);
        setReportCategory(reportCategory);
        setFormIdStr(formIdStr);
        setRepUseUniqueId(repUseUniqueId);
        setRepUseDataValue(repUseDataValue);
    }

    /**
     * Gets the id attribute of the DynRepBean object
     * 
     * @return The id value
     */

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_DYNREP", allocationSize=1)
    @Column(name = "PK_DYNREP")
    public int getId() {
        return id;
    }

    /**
     * Sets the id attribute of the DynRepBean object
     * 
     * @param id
     *            The new id value
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the formId attribute of the DynRepBean object
     * 
     * @return The formId value
     */
    @Column(name = "FK_FORM")
    public String getFormId() {
        return StringUtil.integerToString(formId);
    }

    /**
     * Sets the formId attribute of the DynRepBean object
     * 
     * @param formId
     *            The new formId value
     */
    public void setFormId(String formId) {
        if (!StringUtil.isEmpty(formId)) {
            this.formId = Integer.valueOf(formId);
        }
    }

    /**
     * Gets the repName attribute of the DynRepBean object
     * 
     * @return The repName value
     */

    @Column(name = "DYNREP_NAME")
    public String getRepName() {
        return repName;
    }

    /**
     * Sets the repName attribute of the DynRepBean object
     * 
     * @param repName
     *            The new repName value
     */
    public void setRepName(String repName) {
        this.repName = repName;
    }

    /**
     * Gets the repDesc attribute of the DynRepBean object
     * 
     * @return The repDesc value
     */
    @Column(name = "DYNREP_DESC")
    public String getRepDesc() {
        return repDesc;
    }

    /**
     * Sets the repDesc attribute of the DynRepBean object
     * 
     * @param repDesc
     *            The new repDesc value
     */
    public void setRepDesc(String repDesc) {
        this.repDesc = repDesc;
    }

    /**
     * Gets the repFilter attribute of the DynRepBean object
     * 
     * @return The repFilter value
     */
    @Column(name = "DYNREP_FILTER")
    public String getRepFilter() {
        return repFilter;
    }

    /**
     * Sets the repFilter attribute of the DynRepBean object
     * 
     * @param repFilter
     *            The new repFilter value
     */

    public void setRepFilter(String repFilter) {
        this.repFilter = repFilter;
    }

    /**
     * Gets the repOrder attribute of the DynRepBean object
     * 
     * @return The repOrder value
     */
    @Column(name = "DYNREP_ORDERBY")
    public String getRepOrder() {
        return repOrder;
    }

    /**
     * Sets the repOrder attribute of the DynRepBean object
     * 
     * @param repOrder
     *            The new repOrder value
     */
    public void setRepOrder(String repOrder) {
        this.repOrder = repOrder;
    }

    /**
     * Gets the repHdr attribute of the DynRepBean object
     * 
     * @return The repHdr value
     */
    @Column(name = "DYNREP_HDRNAME")
    public String getRepHdr() {
        return repHdr;
    }

    /**
     * Sets the repHdr attribute of the DynRepBean object
     * 
     * @param repHdr
     *            The new repHdr value
     */
    public void setRepHdr(String repHdr) {
        this.repHdr = repHdr;
    }

    /**
     * Gets the repFtr attribute of the DynRepBean object
     * 
     * @return The repFtr value
     */
    @Column(name = "DYNREP_FTRNAME")
    public String getRepFtr() {
        return repFtr;
    }

    /**
     * Sets the repFtr attribute of the DynRepBean object
     * 
     * @param repFtr
     *            The new repFtr value
     */
    public void setRepFtr(String repFtr) {
        this.repFtr = repFtr;
    }

    /**
     * Gets the userId attribute of the DynRepBean object
     * 
     * @return The userId value
     */
    @Column(name = "FK_USER")
    public String getUserId() {
        return StringUtil.integerToString(userId);
    }

    /**
     * Sets the userId attribute of the DynRepBean object
     * 
     * @param userId
     *            The new userId value
     */
    public void setUserId(String userId) {
        if (!StringUtil.isEmpty(userId)) {
            this.userId = Integer.valueOf(userId);
        }
    }

    /**
     * Gets the accId attribute of the DynRepBean object
     * 
     * @return The accId value
     */
    @Column(name = "FK_ACCOUNT")
    public String getAccId() {
        return StringUtil.integerToString(accId);
    }

    /**
     * Sets the accId attribute of the DynRepBean object
     * 
     * @param accId
     *            The new accId value
     */
    public void setAccId(String accId) {
        if (!StringUtil.isEmpty(accId)) {
            this.accId = Integer.valueOf(accId);
        }
    }

    /**
     * @return the Creator
     */
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The new creator value
     */

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return the unique ID of the User
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {

        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The new modifiedBy value
     */

    public void setModifiedBy(String modifiedBy) {

        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return the IP Address of the Form Notification request
     */

    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The new ipAdd value
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * @return the report_type
     */
    @Column(name = "REPORT_TYPE")
    public String getRepType() {
        return this.repType;
    }

    /**
     * @param repType
     *            The new repType value
     */

    public void setRepType(String repType) {
        this.repType = repType;
    }

    /**
     * Sets the studyId attribute of the DynRepBean object
     * 
     * @param studyId
     *            The new studyId value
     */
    public void setStudyId(String studyId) {
        if (!StringUtil.isEmpty(studyId)) {
            this.studyId = Integer.valueOf(studyId);
        }
    }

    /**
     * Gets the studyId attribute of the DynRepBean object
     * 
     * @return The studyId value
     */
    @Column(name = "FK_STUDY")
    public String getStudyId() {
        return StringUtil.integerToString(studyId);
    }

    /**
     * Sets the perId attribute of the DynRepBean object
     * 
     * @param perId
     *            The new perId value
     */
    public void setPerId(String perId) {
        if (!StringUtil.isEmpty(perId)) {
            this.perId = Integer.valueOf(perId);
        }
    }

    /**
     * Gets the perId attribute of the DynRepBean object
     * 
     * @return The perId value
     */
    @Column(name = "FK_PERSON")
    public String getPerId() {
        return StringUtil.integerToString(perId);
    }

    /**
     * Returns the value of shareWith.
     * 
     * @return The shareWith value
     */
    @Column(name = "DYNREP_SHAREWITH")
    public String getShareWith() {
        return shareWith;
    }

    /**
     * Sets the value of shareWith.
     * 
     * @param shareWith
     *            The value to assign shareWith.
     */
    public void setShareWith(String shareWith) {
        this.shareWith = shareWith;
    }

    /**
     * Returns the value of reportCategory.
     * 
     * @return The reportCategory value
     */
    @Column(name = "DYNREP_TYPE")
    public String getReportCategory() {
        return reportCategory;
    }

    /**
     * Sets the value of reportCategory.
     * 
     * @param reportCategory
     *            The value to assign reportCategory.
     */
    public void setReportCategory(String reportCategory) {
        this.reportCategory = reportCategory;
    }

    /**
     * Returns the value of formIdStr.
     * 
     * @return The formIdStr value
     */
    @Column(name = "DYNREP_FORMLIST")
    public String getFormIdStr() {
        return formIdStr;
    }

    /**
     * Sets the value of formIdStr.
     * 
     * @param formIdStr
     *            The value to assign formIdStr.
     */
    public void setFormIdStr(String formIdStr) {
        this.formIdStr = formIdStr;
    }

    /**
     * Returns the value of repUseUniqueId.
     */
    @Column(name = "DYNREP_USEUNIQUEID")
    public String getRepUseUniqueId() {
        return repUseUniqueId;
    }

    /**
     * Sets the value of repUseUniqueId.
     * 
     * @param repUseUniqueId
     *            The value to assign repUseUniqueId.
     */
    public void setRepUseUniqueId(String repUseUniqueId) {
        if (StringUtil.isEmpty(repUseUniqueId)) {
            repUseUniqueId = "0";
        }
        this.repUseUniqueId = repUseUniqueId;
    }

    /**
     * Returns the value of repUseDataValue.
     */
    @Column(name = "DYNREP_USEDATAVAL")
    public String getRepUseDataValue() {
        return repUseDataValue;
    }

    /**
     * Sets the value of repUseDataValue.
     * 
     * @param repUseDataValue
     *            The value to assign repUseDataValue.
     */
    public void setRepUseDataValue(String repUseDataValue) {
        if (StringUtil.isEmpty(repUseDataValue)) {
            repUseDataValue = "0";
        }

        this.repUseDataValue = repUseDataValue;
    }

    // END OF THE GETTER AND SETTER METHODS

    // ////////////////////////////////////////////////////////////////////

    /**
     * Gets the dynRepStateKeeper attribute of the DynRepBean object
     * 
     * @return The dynRepStateKeeper value
     */
    /*
     * public DynRepStateKeeper getDynRepStateKeeper() { Rlog.debug("dynrep",
     * "DynRepBean.getDynRepStateKeeper"); Rlog.debug("dynrep", "repType in
     * DynRepBean:getDynRepDetails" + getRepType()); return new
     * DynRepStateKeeper(getId(), getFormId(), getRepName(), getRepDesc(),
     * getRepFilter(), getRepOrder(), getRepHdr(), getRepFtr(), getUserId(),
     * getAccId(), getCreator(), getModifiedBy(), getIpAdd(), getRepType(),
     * getStudyId(), getPerId(), getShareWith(), getReportCategory(),
     * getFormIdStr(), getRepUseUniqueId(), getRepUseDataValue()); }
     */

    /**
     * sets up a state keeper containing details of the dynrep
     * 
     * @param dsk
     *            The new dynRepStateKeeper value
     */

    /*
     * public void setDynRepStateKeeper(DynRepStateKeeper dsk) { GenerateId stId =
     * null; try { Connection conn = null; conn = getConnection();
     * Rlog.debug("dynrep", "DynRepBean.setDynRepStateKeeper() DynRepSTATEKEEPER :" +
     * dsk); Rlog.debug("dynrep", "DynRepBean.setDynRepStateKeeper() Connection :" +
     * conn);
     * 
     * this.id = stId.getId("SEQ_ER_DYNREP", conn); Rlog.debug("dynrep",
     * "DynRepBean.Id " + this.id); conn.close();
     * 
     * setFormId(dsk.getFormId()); Rlog.debug("lab", "LabBean.setFormId() " +
     * dsk.getFormId()); setRepName(dsk.getRepName()); Rlog.debug("dynrep",
     * "DynRepBean.getRepName() " + dsk.getRepName());
     * setRepDesc(dsk.getRepDesc()); Rlog.debug("dynrep",
     * "DynRepBean.getRepDesc() " + dsk.getRepDesc());
     * setRepFilter(dsk.getRepFilter()); Rlog.debug("dynrep",
     * "DynRepBean.getRepFilter() " + dsk.getRepFilter());
     * setRepOrder(dsk.getRepOrder()); Rlog.debug("dynrep",
     * "DynRepBean.getRepOrder() " + dsk.getRepOrder());
     * setRepHdr(dsk.getRepHdr()); Rlog.debug("dynrep", "DynRepBean.getRepHdr() " +
     * dsk.getRepHdr()); setRepFtr(dsk.getRepFtr()); Rlog.debug("dynrep",
     * "DynRepBean.getRepFtr() " + dsk.getRepFtr()); setUserId(dsk.getUserId());
     * Rlog.debug("dynrep", "DynRepBean.getUserId() " + dsk.getUserId());
     * setAccId(dsk.getAccId()); Rlog.debug("dynrep", "DynRepBean.getAccId() " +
     * dsk.getAccId()); setCreator(dsk.getCreator()); Rlog.debug("dynrep",
     * "DynRepBean.getCreator() " + dsk.getCreator()); setIpAdd(dsk.getIpAdd());
     * Rlog.debug("dynrep", "DynRepBean.getIpAdd() " + dsk.getIpAdd());
     * setRepType(dsk.getRepType()); Rlog.debug("dynrep",
     * "DynRepBean.getRepName() " + dsk.getRepType());
     * setStudyId(dsk.getStudyId()); Rlog.debug("dynrep",
     * "DynRepBean.getStudyId() " + dsk.getStudyId()); setPerId(dsk.getPerId());
     * Rlog.debug("dynrep", "DynRepBean.getPerId() " + dsk.getPerId());
     * setShareWith(dsk.getShareWith()); Rlog.debug("dynrep",
     * "DynRepBean.getShareWith() " + dsk.getShareWith());
     * setReportCategory(dsk.getReportCategory()); Rlog.debug("dynrep",
     * "DynRepBean.getReportCategory() " + dsk.getReportCategory());
     * setFormIdStr(dsk.getFormIdStr()); Rlog.debug("dynrep",
     * "DynRepBean.getFormIdStr() " + dsk.getFormIdStr());
     * 
     * setRepUseUniqueId(dsk.getRepUseUniqueId());
     * setRepUseDataValue(dsk.getRepUseDataValue()); } catch (Exception e) {
     * Rlog.fatal("dynrep", "Error in setDynRepStateKeeper() in DynRepBean " +
     * e); } }
     */

    /**
     * updates the Record
     * 
     * @param dsk
     * @return int 0: in case of successful updation; -2 in caseof exception
     */
    public int updateDynRep(DynRepBean dsk) {
        try {
            setFormId(dsk.getFormId());
            setRepName(dsk.getRepName());
            setRepDesc(dsk.getRepDesc());
            setRepFilter(dsk.getRepFilter());
            setRepOrder(dsk.getRepOrder());
            setRepHdr(dsk.getRepHdr());
            setRepFtr(dsk.getRepFtr());
            setUserId(dsk.getUserId());
            setAccId(dsk.getAccId());
            setModifiedBy(dsk.getModifiedBy());
            setIpAdd(dsk.getIpAdd());
            setRepType(dsk.getRepType());
            setStudyId(dsk.getStudyId());
            setPerId(dsk.getPerId());
            setShareWith(dsk.getShareWith());
            setReportCategory(dsk.getReportCategory());
            setFormIdStr(dsk.getFormIdStr());
            setRepUseUniqueId(dsk.getRepUseUniqueId());
            setRepUseDataValue(dsk.getRepUseDataValue());
            setCreator(dsk.getCreator());
            return 0;
        } catch (Exception e) {
            Rlog.fatal("dynrep", " error in DynRepBean.updateDynRep" + e);
            return -2;
        }
    }

}
