/*
 * Classname : 			FldRespDao
 *
 * Version information: 1.0
 *
 * Date:    			07/03/2003
 *
 * Copyright notice: 	Velos, Inc
 *
 * Author: 				Anu Khanna
 */

package com.velos.eres.business.common;

/* Import Statements */
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The Field response Dao.<br>
 * <br>
 * This class has various methods to get resultsets of user data for different
 * parameters.
 *
 * @author Anu Khanna
 * @vesion 1.0
 */

public class FldRespDao extends CommonDAO implements java.io.Serializable {

    private ArrayList arrFldRespIds;

    private ArrayList arrSeqs;

    private ArrayList arrFields;

    private ArrayList arrDispVals;

    private ArrayList arrDataVals;

    private ArrayList arrScores;

    private ArrayList arrDefaultVals;

    private ArrayList arrRecordTypes;

    private ArrayList arrOfflines;

    private String creator;

    private String ipAdd;

    private String lastModifiedBy;

    private String field;

    private String seqs;

    private String recordType;

    private String maxSeq;

    private int cRows;

    private String[] saFldRespIds;

    private String[] saSeqs;

    private String[] saFields;

    private String[] saRecordTypes;

    private String[] saDispVals;

    private String[] saDataVals;

    private String[] saScores;

    private String[] saDefaultValues;

    public FldRespDao() {
        arrFldRespIds = new ArrayList();
        arrSeqs = new ArrayList();
        arrFields = new ArrayList();
        arrDispVals = new ArrayList();
        arrDataVals = new ArrayList();
        arrScores = new ArrayList();
        arrDefaultVals = new ArrayList();
        arrRecordTypes = new ArrayList();
        arrOfflines = new ArrayList();

        String ipAdd;
        String recordType;

        int creator;
        int lastModifiedBy;
        int field;
        int maxSeq;
        int cRows;

    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public ArrayList getArrFldRespIds() {
        return this.arrFldRespIds;
    }

    public void setArrFldRespIds(ArrayList arrFldRespIds) {
        this.arrFldRespIds = arrFldRespIds;
    }

    public ArrayList getArrSequences() {
        return this.arrSeqs;
    }

    public void setArrSequences(ArrayList arrSeqs) {
        this.arrSeqs = arrSeqs;
    }

    public ArrayList getArrFields() {
        return this.arrFields;
    }

    public void setArrFields(ArrayList arrFields) {
        this.arrFields = arrFields;
    }

    public void setArrDispVals(ArrayList arrDispVals) {
        this.arrDispVals = arrDispVals;
    }

    public ArrayList getArrDispVals() {
        return this.arrDispVals;
    }

    public void setArrDataVals(ArrayList arrDataVals) {
        this.arrDataVals = arrDataVals;
    }

    public ArrayList getArrDataVals() {
        return this.arrDataVals;
    }

    public void setArrScores(ArrayList arrScores) {
        this.arrScores = arrScores;
    }

    public ArrayList getArrScores() {
        return this.arrScores;
    }

    public void setArrDefaultValues(ArrayList arrDefaultVals) {
        this.arrDefaultVals = arrDefaultVals;
    }

    public ArrayList getArrDefaultValues() {
        return this.arrDefaultVals;
    }

    public void setArrRecordTypes(ArrayList arrRecordTypes) {
        this.arrRecordTypes = arrRecordTypes;
    }

    public void setArrRecordTypes(String arrRecordType) {
        this.arrRecordTypes.add(arrRecordType);
    }

    public ArrayList getArrRecordTypes() {
        return this.arrRecordTypes;
    }

    public void setArrOfflines(ArrayList arrOfflines) {
        this.arrOfflines = arrOfflines;
    }

    public ArrayList getArrOfflines() {
        return this.arrOfflines;
    }

    public String getField() {
        return this.field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getCreator() {
        return this.creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getLastModifiedBy() {
        return this.lastModifiedBy;
    }

    public void setIPAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    public String getIPAdd() {
        return this.ipAdd;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getRecordType() {
        return this.recordType;
    }

    public void setArrSequences(Integer seq) {
        arrSeqs.add(seq);
    }

    public void setArrFldRespIds(Integer fldRespId) {
        arrFldRespIds.add(fldRespId);
    }

    public void setArrDispVals(String dispVal) {
        // arrDispVals.add((dispVal));
        arrDispVals.add(StringUtil.escapeSpecialCharHTML(dispVal));

    }

    public void setArrDataVals(String dataVal) {

        arrDataVals.add(StringUtil.escapeSpecialCharHTML(dataVal));
        // arrDataVals.add((dataVal));
    }

    public void setArrScores(String score) {
        arrScores.add(score);
    }

    public void setArrDefaultValues(Integer defaultVal) {
        arrDefaultVals.add(defaultVal);
    }

    public void setArrOfflines(Integer offline) {
        arrOfflines.add(offline);
    }

    /* getter setter methods for String arrays */

    public void setSequences(String[] saSeqs) {
        this.saSeqs = saSeqs;
    }

    public String[] getSequences() {
        return this.saSeqs;
    }

    public String[] getFldRespIds() {
        return this.saFldRespIds;
    }

    public void setFldRespIds(String[] saFldRespIds) {
        this.saFldRespIds = saFldRespIds;
    }

    public void setRecordTypes(String[] saRecordTypes) {
        this.saRecordTypes = saRecordTypes;
    }

    public String[] getRecordTypes() {
        return this.saRecordTypes;
    }

    public void setDispVals(String[] saDispVals) {
        this.saDispVals = saDispVals;
    }

    public String[] getDispVals() {
        return this.saDispVals;
    }

    public void setDataVals(String[] saDataVals) {
        this.saDataVals = saDataVals;
    }

    public String[] getDataVals() {
        return this.saDataVals;
    }

    public void setScores(String[] saScores) {
        this.saScores = saScores;
    }

    public String[] getScores() {
        return this.saScores;
    }

    public void setDefaultValues(String[] saDefaultValues) {
        this.saDefaultValues = saDefaultValues;
    }

    public String[] getDefaultValues() {
        return this.saDefaultValues;
    }

    public String getMaxSeq() {
        return this.maxSeq;
    }

    public void setMaxSeq(String maxSeq) {
        this.maxSeq = maxSeq;
    }

    public void getResponsesForField(int field) {

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement("select pk_fldresp,fldresp_seq, fldresp_dispval, fldresp_dataval, fldresp_score, fldresp_isdefault, record_type, fldresp_offline "
                            + "  from  er_fldresp "
                            + "  where fk_field=? "
                            + "  and nvl(record_type,'Z') <> 'D' order by fldresp_seq ");

            pstmt.setInt(1, field);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setArrFldRespIds(new Integer(rs.getInt("pk_fldresp")));
                setArrSequences(new Integer(rs.getInt("fldresp_seq")));
                setArrDispVals(rs.getString("fldresp_dispval"));
                setArrDataVals(rs.getString("fldresp_dataval"));
                setArrScores(rs.getString("fldresp_score"));
                setArrDefaultValues(new Integer(rs.getInt("fldresp_isdefault")));
                setArrOfflines(new Integer(rs.getInt("fldresp_offline")));
                setArrRecordTypes(rs.getString("record_type"));
                rows++;
                Rlog.debug("fldresp", "FldRespDao.getResponsesForFields rows "
                        + rows);
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("fldresp",
                    "FldRespDao.getResponsesForFields EXCEPTION IN FETCHING FROM ER_FLDRESP table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * This method inserts multiple reponses for a field responses are stored in
     * DAO
     *
     * @param FldRespDao -
     *            dao with multiple responses
     * @returns -1 in case of error
     */
    public int insertMultipleResponses(FldRespDao fldRespDao) {
        Rlog.debug("fldresp", "inside insertMultipleResponses in FldRespDao");

        String strField = fldRespDao.getField();

        String strLastModifiedBy = fldRespDao.getLastModifiedBy();

        String strIpAdd = fldRespDao.getIPAdd();

        String[] saSeqs = fldRespDao.getSequences();

        String[] saRecordTypes = fldRespDao.getRecordTypes();
        String[] saDispVals = fldRespDao.getDispVals();
        String[] saDataVals = fldRespDao.getDataVals();
        String[] saScores = fldRespDao.getScores();
        String[] saDefaultValues = fldRespDao.getDefaultValues();
        int ret = 1;
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();

            ArrayDescriptor inSeqs = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY seqsArray = new ARRAY(inSeqs, conn, saSeqs);

            ArrayDescriptor inRecordTypes = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY recordTypesArray = new ARRAY(inRecordTypes, conn,
                    saRecordTypes);

            ArrayDescriptor inDispVals = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY dispValsArray = new ARRAY(inDispVals, conn, saDispVals);

            ArrayDescriptor inDataVals = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY dataValsArray = new ARRAY(inDataVals, conn, saDataVals);

            ArrayDescriptor inScores = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY scoresArray = new ARRAY(inScores, conn, saScores);

            ArrayDescriptor inDefaultVals = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY defaultValsArray = new ARRAY(inDefaultVals, conn,
                    saDefaultValues);

            cstmt = conn
                    .prepareCall("{call PKG_FORM.SP_INSERT_FLDRESPONSES(?,?,?,?,?,?,?,?,?,?)}");

            Rlog.debug("fldresp",
                    "after prepare call of sp_insert_fldresponses");

            cstmt.setString(1, strField);
            cstmt.setArray(2, seqsArray);
            cstmt.setArray(3, dispValsArray);
            cstmt.setArray(4, dataValsArray);
            cstmt.setArray(5, scoresArray);
            cstmt.setArray(6, defaultValsArray);
            cstmt.setArray(7, recordTypesArray);
            cstmt.setString(8, strLastModifiedBy);
            cstmt.setString(9, strIpAdd);
            cstmt.registerOutParameter(10, java.sql.Types.INTEGER);

            cstmt.execute();

            Rlog.debug("fldresp", " after execute of sp_insert_fldresponses");
            ret = cstmt.getInt(10);
            Rlog.debug("fldresp", " after execute of ##$$RETURN VALUE" + ret);

            return ret;
        } catch (Exception e) {
            Rlog.fatal("fldresp",
                    "EXCEPTION in insertMultipleResponses, excecuting Stored Procedure "
                            + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    /**
     * This method updates multiple reponses for a field responses are stored in
     * DAO
     *
     * @param FldRespDao -
     *            dao with multiple responses
     * @returns -1 in case of error
     */

    public int updateMultipleResponses(FldRespDao fldRespDao) {

        String[] saRespIds = fldRespDao.getFldRespIds();
        String[] saSeqs = fldRespDao.getSequences();
        String[] saDispVals = fldRespDao.getDispVals();
        String[] saDataVals = fldRespDao.getDataVals();
        String[] saScores = fldRespDao.getScores();
        String[] saDefaultValues = fldRespDao.getDefaultValues();
        String[] saRecordTypes = fldRespDao.getRecordTypes();
        String strLastModifiedBy = fldRespDao.getLastModifiedBy();
        String strIpAdd = fldRespDao.getIPAdd();

        int ret = 1;
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            Rlog.debug("fldresp",
                    "In updateMultipleResponses updateMultipleResponses() ");

            ArrayDescriptor inRespIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);

            ARRAY respIdsArray = new ARRAY(inRespIds, conn, saRespIds);

            ArrayDescriptor inSeqs = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY seqsArray = new ARRAY(inSeqs, conn, saSeqs);

            ArrayDescriptor inRecordTypes = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY recordTypesArray = new ARRAY(inRecordTypes, conn,
                    saRecordTypes);

            ArrayDescriptor inDispVals = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY dispValsArray = new ARRAY(inDispVals, conn, saDispVals);

            ArrayDescriptor inDataVals = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY dataValsArray = new ARRAY(inDataVals, conn, saDataVals);

            ArrayDescriptor inScores = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY scoresArray = new ARRAY(inScores, conn, saScores);

            ArrayDescriptor inDefaultVals = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY defaultValsArray = new ARRAY(inDefaultVals, conn,
                    saDefaultValues);

            Rlog
                    .debug("fldresp",
                            "In updateMultipleResponses updateMultipleResponses() Row 6");

            cstmt = conn
                    .prepareCall("{call PKG_FORM.SP_UPDATE_FLDRESPONSES(?,?,?,?,?,?,?,?,?,?)}");
            cstmt.setArray(1, respIdsArray);
            cstmt.setArray(2, seqsArray);
            cstmt.setArray(3, dispValsArray);
            cstmt.setArray(4, dataValsArray);
            cstmt.setArray(5, scoresArray);
            cstmt.setArray(6, defaultValsArray);
            cstmt.setArray(7, recordTypesArray);
            cstmt.setString(8, strLastModifiedBy);
            cstmt.setString(9, strIpAdd);
            cstmt.registerOutParameter(10, java.sql.Types.INTEGER);

            cstmt.execute();
            ret = cstmt.getInt(10);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("fldresp",
                    "EXCEPTION in updateMultipleResponses, excecuting Stored Procedure "
                            + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

    /*
     * Method to find the maximum value of sequence for a specific multiple
     * choice field
     */

    public void getMaxSeqNumForMulChoiceFld(int formfldId) {
        Rlog.debug("fldresp", "In value of formfldId" + formfldId);

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select max(fldresp_seq) respmaxseq"
                    + "  from  er_fldresp  fr, er_formfld ff"
                    + "  where fr.fk_field= ff.fk_field "
                    + "  and pk_formfld = ?");

            pstmt.setInt(1, formfldId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setMaxSeq(rs.getString("respmaxseq"));

            }

        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "fldresp",
                            "FldRespDao.getMaxSeqNumForMulChoiceFld EXCEPTION IN FETCHING FROM ER_FLDRESP table"
                                    + ex);

        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /* Method to get the maximum response sequence number for a field in library */
    public void getMaxRespSeqForFldInLib(int fldId) {
        Rlog.debug("fldresp", "In value of fldId" + fldId);

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select max(FLDRESP_SEQ) respmaxseq"
                    + "  from er_fldresp" + "  where " + "  FK_FIELD = ?");

            pstmt.setInt(1, fldId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setMaxSeq(rs.getString("respmaxseq"));

            }

        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "fldresp",
                            "FldRespDao.getMaxRespSeqForFldInLib EXCEPTION IN FETCHING FROM ER_FLDRESP table"
                                    + ex);

        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }


  //JM: 24Jun2008, added for the issue #3309 Need to delete record physically from ER_FLDRESP on deletion.


  	    public int deleteFldResp(int fldRespId){

  	        PreparedStatement pstmt = null;
  	        Connection conn = null;
  	        ResultSet rs = null;

  	        try {
  	            conn = getConnection();
  	            String mysql = "delete from ER_FLDRESP "
  	                    + "WHERE  PK_FLDRESP= ?";

  	            pstmt = conn.prepareStatement(mysql);
  	            pstmt.setInt(1, fldRespId);
  	            rs = pstmt.executeQuery();
  	            rs.next();

  	        } catch (SQLException ex) {
  	            Rlog.fatal("fldresp", "FldRespDao.deleteFldResp()" + ex);
  	            return -2;
  	        }finally {
  	            try {
  	                if (pstmt != null)
  	                    pstmt.close();
  	            } catch (Exception e) {
  	            }
  	            try {
  	                if (conn != null)
  	                    conn.close();
  	            } catch (Exception e) {
  	            }
  	        }
  	        return 1;
  	    }






}