/*
 * Classname : StudyTXArmBean
 * 
 * Version information: 1.0
 *
 * Date: 05/02/2005
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.business.studyTXArm.impl;

/* Import Statements */
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The StudyTXArm CMP entity bean.<br>
 * <br>
 * 
 * @author Anu
 * @vesion 1.0 05/02/2005
 * @ejbHome StudyTXArmHome
 * @ejbRemote StudyTXArmRObj
 */

@Entity
@Table(name = "ER_STUDYTXARM")
public class StudyTXArmBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3760840173227161650L;

    /**
     * the Study TX Arm id
     */
    public int studyTXArmId;

    /**
     * the study Id
     */
    public Integer studyId;

    /**
     * the txName
     */

    public String txName;

    /**
     * the txDrugInfo
     */

    public String txDrugInfo;

    /**
     * the txDesc
     */

    public String txDesc;

    /**
     * creator
     */
    public Integer creator;

    /**
     * last modified by
     */
    public Integer modifiedBy;

    /**
     * IP Address
     */
    public String ipAdd;

    // GETTER SETTER METHODS

    public StudyTXArmBean() {

    }

    public StudyTXArmBean(int studyTXArmId, String studyId, String txName,
            String txDrugInfo, String txDesc, String creator,
            String modifiedBy, String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setStudyTXArmId(studyTXArmId);
        setStudyId(studyId);
        setTXName(txName);
        setTXDrugInfo(txDrugInfo);
        this.setTXDesc(txDesc);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

    /**
     * 
     * Get Study treatment Id
     * 
     * @return the studyTXArmId
     */

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_STUDY_TX_ARM", allocationSize=1)
    @Column(name = "PK_STUDYTXARM")
    public int getStudyTXArmId() {
        return this.studyTXArmId;
    }

    /**
     * 
     * Set Study treatment Id
     * 
     * @return the studyTXArmId
     */
    public void setStudyTXArmId(int id) {
        this.studyTXArmId = id;
    }

    /**
     * Get study Id
     * 
     * @return studyId
     */

    @Column(name = "FK_STUDY")
    public String getStudyId() {
        return StringUtil.integerToString(this.studyId);
    }

    /**
     * sets study Id
     * 
     * @param studyId
     */
    public void setStudyId(String studyId) {
        this.studyId = StringUtil.stringToInteger(studyId);
    }

    /**
     * Get tx Name
     * 
     * @return txName
     */
    @Column(name = "TX_NAME")
    public String getTXName() {
        return this.txName;
    }

    /**
     * sets txName
     * 
     * @param txName
     */
    public void setTXName(String txName) {
        this.txName = txName;
    }

    /**
     * Get tx drug info
     * 
     * @return txDrugInfo
     */
    @Column(name = "TX_DRUG_INFO")
    public String getTXDrugInfo() {
        return this.txDrugInfo;
    }

    /**
     * sets txDrugInfo
     * 
     * @param txDrugInfo
     */
    public void setTXDrugInfo(String txDrugInfo) {
        this.txDrugInfo = txDrugInfo;
    }

    /**
     * Get treatment Description
     * 
     * @return String txDesc
     */
    @Column(name = "TX_DESC")
    public String getTXDesc() {
        return this.txDesc;
    }

    /**
     * sets txDesc
     * 
     * @param txDesc
     */
    public void setTXDesc(String txDesc) {
        this.txDesc = txDesc;
    }

    /**
     * Get Status creator
     * 
     * @return The Id of the creator of the record
     */
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {

        this.creator = StringUtil.stringToInteger(creator);
    }

    /**
     * Get Status modified by
     * 
     * @return The Id of the user modifying this record
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {

        this.modifiedBy = StringUtil.stringToInteger(modifiedBy);
    }

    /**
     * @return IP Address
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    /*
     * public StudyTXArmStateKeeper getStudyTXArmStateKeeper() {
     * 
     * return new StudyTXArmStateKeeper(getStudyTXArmId(), getStudyId(),
     * getTXName(), getTXDrugInfo(), getTXDesc(), getCreator(), getModifiedBy(),
     * getIpAdd()); }
     */

    /**
     * sets up a state keeper containing details of the studyarm
     * 
     * @param sqk
     *            StudyTXArmStateKeeper
     */

    /*
     * public void setStudyTXArmStateKeeper(StudyTXArmStateKeeper stk) {
     * GenerateId studyTXArmId = null;
     * 
     * try { Connection conn = null; conn = getConnection();
     * 
     * this.studyTXArmId = studyTXArmId.getId("SEQ_ER_STUDY_TX_ARM", conn);
     * conn.close(); setStudyId(stk.getStudyId()); setTXName(stk.getTXName());
     * setTXDrugInfo(stk.getTXDrugInfo()); setTXDesc(stk.getTXDesc());
     * setCreator(stk.getCreator()); setModifiedBy(stk.getModifiedBy());
     * setIpAdd(stk.getIpAdd()); } catch (Exception e) { Rlog
     * .fatal("studyTXArm", "Exception in
     * StudyTXArmBean.setStudyTXArmStateKeeper" + e); } }
     */

    /**
     * updates the study tx arm details
     * 
     * @param stk
     *            StudyTXArmStateKeeper
     * @return 0 if successful; -2 for exception
     */
    public int updateStudyTXArm(StudyTXArmBean stk) {
        try {

            setStudyId(stk.getStudyId());
            setTXName(stk.getTXName());
            setTXDrugInfo(stk.getTXDrugInfo());
            setTXDesc(stk.getTXDesc());
            setCreator(stk.getCreator());
            setModifiedBy(stk.getModifiedBy());
            setIpAdd(stk.getIpAdd());

            return 0;
        } catch (Exception e) {
            Rlog.fatal("studyTXArm",
                    "Exception in  studytxarm.updateStudyTXArm" + e);
            return -2;
        }
    }

}// end of class
