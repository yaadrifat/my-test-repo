package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import com.velos.eres.service.util.Rlog;

public class StudySiteDao extends CommonDAO implements java.io.Serializable {

    private int cRows;

    /** for study site ids */

    private ArrayList studySiteIds;

    /** for study site apndx ids */

    private ArrayList studySiteApndxCnts;

    /** for site ids */

    private ArrayList siteIds;

    /** for study site types */

    private ArrayList studySiteTypes;

    /** for study site types */

    private ArrayList siteNames;

    /** for local sample size */

    private ArrayList lSampleSize;

    private ArrayList siteAccess;

    private ArrayList currStats;

    private ArrayList altIds;

    /** restricted by er_site.site_restrict or not **/
    private ArrayList isRestricteds;

    public StudySiteDao() {
        studySiteIds = new ArrayList();
        siteIds = new ArrayList();
        studySiteTypes = new ArrayList();
        siteNames = new ArrayList();
        studySiteApndxCnts = new ArrayList();
        lSampleSize = new ArrayList();
        siteAccess = new ArrayList();
        currStats = new ArrayList();
        isRestricteds = new ArrayList();
        altIds = new ArrayList();
    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        Rlog.debug("studySite", "inside method getLatestStatus cRows" + cRows);
        this.cRows = cRows;
    }

    /**
     * Returns an Arraylist containing Integer objects with study site Ids
     *
     * @return an Arraylist containing Integer objects with study site Ids
     */
    public ArrayList getStudySiteIds() {
        return this.studySiteIds;
    }

    /**
     * Sets the Arraylist containing the study siteIds with the ArrayList passed
     * as the parameter
     *
     * @param teamIds
     *            An ArrayList with Integer objects having the study site Ids
     */
    public void setStudySiteIds(ArrayList studySiteIds) {
        this.studySiteIds = studySiteIds;
    }

    /**
     * Returns an Arraylist containing Integer objects with site Ids
     *
     * @return an Arraylist containing Integer objects with site Ids
     */
    public ArrayList getSiteIds() {
        return this.siteIds;
    }

    /**
     * Sets the Arraylist containing the siteIds with the ArrayList passed as
     * the parameter
     *
     * @param teamIds
     *            An ArrayList with Integer objects having the study site Ids
     */
    public void setSiteIds(ArrayList siteIds) {
        this.siteIds = siteIds;
    }

    /**
     * Returns an Arraylist containing Integer objects with study site types
     *
     * @return an Arraylist containing Integer objects with study site types
     */
    public ArrayList getStudySiteTypes() {
        return this.studySiteTypes;
    }

    /**
     * Sets the Arraylist containing the study site types with the ArrayList
     * passed as the parameter
     *
     * @param teamIds
     *            An ArrayList with Integer objects having the study site types
     */
    public void setStudySiteTypes(ArrayList studySiteTypes) {
        this.studySiteTypes = studySiteTypes;
    }

    /**
     * Returns an Arraylist containing local sample size
     *
     * @return an Arraylist containing Local sample size
     */
    public ArrayList getLSampleSize() {
        return this.lSampleSize;
    }

    /**
     * Sets the Arraylist containing the study site types with the ArrayList
     * passed as the parameter
     *
     * @param lSampleSize
     *            An ArrayList with Integer objects having the study site types
     */
    public void setLSampleSize(ArrayList lSampleSize) {
        this.lSampleSize = lSampleSize;
    }

    /**
     * Returns an Arraylist containing site access
     *
     * @return siteAccess
     */
    public ArrayList getSiteAccess() {
        return this.siteAccess;
    }

    /**
     * Sets the Arraylist containing the site access
     *
     * @param siteAccess
     */
    public void setSiteAccess(ArrayList siteAccess) {
        this.siteAccess = siteAccess;
    }

    /**
     * Returns an Arraylist containing Integer objects with study site apndx Ids
     *
     * @return an Arraylist containing Integer objects with study site apndx Ids
     */
    public ArrayList getStudySiteApndxCnts() {
        return this.studySiteApndxCnts;
    }

    /**
     * Sets the Arraylist containing the study site apndx Ids with the ArrayList
     * passed as the parameter
     *
     * @param teamIds
     *            An ArrayList with Integer objects having the study site apndx
     *            Ids
     */
    public void setStudySiteApndxIds(ArrayList studySiteApndxCnts) {
        this.studySiteApndxCnts = studySiteApndxCnts;
    }

    /**
     * Adds the object to the Arraylist containing the site type
     *
     * @param setStudySiteType
     *            A String object that needs to be added to the Arraylist
     *            containing the Team User First Names
     */
    public void setStudySiteTypes(String studySiteType) {
        studySiteTypes.add(studySiteType);
    }

    /**
     * Adds the object to the Arraylist containing the study site id
     *
     * @param teamId
     *            An Integer object that needs to be added to the Arraylist
     *            containing the Team Ids
     */
    public void setStudySiteIds(Integer studySiteId) {
        studySiteIds.add(studySiteId);
    }

    /**
     * Adds the object to the Arraylist containing the study site apndx id
     *
     * @param teamId
     *            An Integer object that needs to be added to the Arraylist
     *            containing the Team Ids
     */
    public void setStudySiteApndxCnts(Integer studySiteApndxCnt) {
        studySiteApndxCnts.add(studySiteApndxCnt);
    }

    public void setSiteIds(Integer siteId) {
        siteIds.add(siteId);
    }

    public ArrayList getSiteNames() {
        return this.siteNames;
    }

    /**
     * Sets the Arraylist containing the site names with the ArrayList passed as
     * the parameter
     *
     * @param site
     *            name An ArrayList
     */
    public void setSiteNames(ArrayList siteNames) {
        this.siteNames = siteNames;
    }

    /**
     * Adds the object to the Arraylist containing the site type
     *
     * @param setStudySiteType
     *            A String object that needs to be added to the Arraylist
     *            containing the Team User First Names
     */
    public void setSiteNames(String siteName) {
        siteNames.add(siteName);
    }

    /**
     * Adds the object to the Arraylist containing the site restricted flags
     *
     * @param isRestricted
     *            A Boolean object that needs to be added to the Arraylist
     *            containing the site restricted flag
     */
    public void setIsRestricteds(String isRestricted){
    	isRestricteds.add(isRestricted);
    }

    /**
     * Sets the Arraylist containing the site restricted flags with the ArrayList passed as
     * the parameter
     *
     * @param site
     *            name An ArrayList
     */
    public void setIsRestricteds(ArrayList isRestricteds){
    	this.isRestricteds = isRestricteds;
    }

    /**
     * Returns a list of site restricted flags from er_site.site_restrict
     * @return
     */
    public ArrayList getIsRestricteds(){
    	return isRestricteds;
    }


    /**
     * Adds the object to the Arraylist containing the local sample size
     *
     * @param setLSampleSize
     *            A String object that needs to be added to the Arraylist
     *            containing the Team User First Names
     */
    public void setLSampleSize(String lSmpleSize) {
        lSampleSize.add(lSmpleSize);
    }

    public void setSiteAccess(String orgAccess) {
        siteAccess.add(orgAccess);
    }

    //Added by Manimaran to fix the Bug2730

    /**
     * Returns an Arraylist containing Integer objects with current status values
     *
     * @return an Arraylist containing Integer objects with current status values
     */
    public ArrayList getCurrStats() {
        return this.currStats;
    }

    /**
     * Sets the Arraylist containing the study current status values with the ArrayList passed
     * as the parameter
     *
     * @param currStats
     *            An ArrayList with Integer objects having the study current status values
     */
    public void setCurrStats(ArrayList currStats) {
        this.currStats = currStats;
    }

    /**
     * Adds the object to the Arraylist containing the current study status value
     *
     * @param currStat
     *            An Integer object that needs to be added to the Arraylist
     *            containing the Team Ids
     */
    public void setCurrStats(Integer currStat) {
        currStats.add(currStat);
    }

    public ArrayList getAltIds() {
		return altIds;
	}

	public void setAltIds(ArrayList altIds) {
		this.altIds = altIds;
	}

	public void setAltIds(String altId) {
		altIds.add(altId);
	}

    // end of getter and setter methods



	/**
     * Method to find number of rows returned for a study and site in
     * er_studysites
     *
     * @param pk
     *            of study
     * @param pk
     *            of the organization
     *
     */

    public void getCntForSiteInStudy(int studyId, int siteId) {

        Rlog.debug("studySite",
                "inside method getCntForSiteTypeInStudy studyId" + studyId);

        Rlog.debug("studySite", "inside method getCntForSiteTypeInStudy siteId"
                + siteId);
        String subType = null;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";
        try {

            conn = getConnection();
            sql = "select count(*) count " + " from er_studysites where "
                    + " FK_SITE = ?  " + " and FK_STUDY = ? ";

            Rlog.debug("studySite", "inside method getCntForSiteInStudy sql"
                    + sql);

            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, siteId);
            pstmt.setInt(2, studyId);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                setCRows(rs.getInt("count"));
            }

        } catch (SQLException ex) {
            Rlog.fatal("studySite", "error in getCntForSiteTypeInStudy" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Gets the Study Team details corresponding to a studyId and organization
     * id in ArrayLists
     *
     * @param studyId
     *            Pk of the study
     * @param orgId
     *            Pk of the organization
     */

    /* modified by sonia abrol for new super user implementation*/

    public void getStudySiteTeamValues(int studyId, int orgId, int userId,
            int accId) {

        int rows = 0;
        String sql1 = "";
        String sql2 = "";
        String sql3 = "";
        String sql = "";
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();


            //Query is Modified by Manimaran to fix the Bug2730.
            sql1 = " select PK_STUDYSITES, s.FK_SITE,  "
                    + " (select SITE_NAME from er_site where  PK_SITE  = s.FK_SITE) as site_name, "
                    + " (select count(*) from er_studysites_apndx where fk_studysites = pk_studysites) as cnt_studysites_apndx, "
                    + " (select CODELST_DESC from er_codelst where  PK_CODELST = FK_CODELST_STUDYSITETYPE) as type,  "
                    + " STUDYSITE_LSAMPLESIZE, case when ( pkg_user.f_chk_right_for_studysite(s.fk_study,?,s.fk_site) > 0   )  "
                    + " then 1  else 0  end as site_access,  "
                    + " case when 1 in (select current_stat from er_studystat where fk_site = s.fk_site and fk_study = s.fk_study ) then 1 else 0 end as current_stat" //KM
                    + " from er_studysites s  where s.fk_study = ? ";

            if (orgId != 0) {
                sql2 = " and FK_SITE = ?" ;
            } else {
                sql3 = " order by FK_SITE";
            }

            sql = sql1 + sql2 + sql3;

            Rlog.debug("studySite", "StudySiteDao.getStudySiteTeamValues sql:"
                    + sql);
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, userId);
            pstmt.setInt(2, studyId);

            if (orgId != 0) {
            	pstmt.setInt(3, orgId);
            }

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setStudySiteIds(new Integer(rs.getInt("PK_STUDYSITES")));
                setSiteIds(new Integer(rs.getInt("FK_SITE")));
                setStudySiteApndxCnts(new Integer(rs
                        .getInt("cnt_studysites_apndx")));
                setStudySiteTypes(rs.getString("type"));
                setSiteNames(rs.getString("site_name"));
                setLSampleSize(rs.getString("STUDYSITE_LSAMPLESIZE"));
                setSiteAccess(rs.getString("site_access"));
                setCurrStats(new Integer(rs.getInt("current_stat")));//KM
                rows++;
                Rlog.debug("studySite",
                        "studySiteDao.getStudySiteTeamValues rows " + rows);

            }

            setCRows(rows);

        } catch (SQLException ex) {
            Rlog.fatal("studySite",
                    "StudySiteDao.getStudySiteTeamValues EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Gets the All study team sites corresponding to a studyId and account id
     * in ArrayLists
     *
     * @param studyId
     *            Pk of the study
     * @param accId
     *            Pk of the organization
     */

    public void getAllStudyTeamSites(int studyId, int accId) {

        int rows = 0;
        String sql = "";
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            sql = "select "
                    + " FK_SITE as site_id, "
                    + " (select SITE_NAME from er_site where PK_SITE  = FK_SITE) as site_name, "
                    + " FK_CODELST_STUDYSITETYPE, (select codelst_desc from er_codelst where codelst_type = 'studySiteType' and pk_codelst = nvl(FK_CODELST_STUDYSITETYPE,0)) as SITETYPE "
                    + " from er_studysites where fk_study = ? "
                    + " union "
                    + " select "
                    + " (select FK_SITEID from er_user where er_user.pk_user = er_studyteam.FK_USER) as site_id , "
                    + " (select site_name from er_site where pk_site = (select FK_SITEID from er_user where er_user.pk_user = er_studyteam.FK_USER)) as site_name, "
                    + " 0, '' as SITETYPE "
                    + " from er_studyteam "
                    + " where FK_STUDY = ? "
                    + " and nvl(STUDY_TEAM_USR_TYPE,'D') = 'D' "
                    + " and fk_user in (select pk_user from er_user where fk_account = ? ) ";

            Rlog.debug("studySite", "StudySiteDao.getAllStudyTeamSites sql:"
                    + sql);
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, studyId);
            pstmt.setInt(2, studyId);
            pstmt.setInt(3, accId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setSiteIds(new Integer(rs.getInt("site_id")));
                setSiteNames(rs.getString("site_name"));
                setStudySiteTypes(rs.getString("SITETYPE"));
                rows++;
                Rlog.debug("studySite",
                        "studySiteDao.getAllStudyTeamSites rows " + rows);

            }

            setCRows(rows);

        } catch (SQLException ex) {
            Rlog.fatal("studySite",
                    "StudySiteDao.getAllStudyTeamSites EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Gets the number of sites for user
     *
     * @param studyId
     *            Pk of the study
     * @param userId
     *            Pk of the user
     * @param accId
     *            Pk of the account
     *            @deprecated
     */

    public int getCountSitesForUserStudy(int studyId, int userId, int accountId) {

        int rows = 0;
        int total_sites = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();

            /*
             * String mysql = "select count(*) as total_sites from
             * er_study_site_rights, er_user " + " where pk_user=fk_user " + "
             * and fk_user = ? " + " and fk_study = ?" + " and fk_account = ?";
             */

            String mysql = "	 select count(*) as total_sites from er_user u, er_studyteam t "
                    + " where  "
                    + " pk_user = t.fk_user "
                    + " and pk_user = t.fk_user "
                    + " and t.fk_study = t.fk_study "
                    + " and (nvl(study_team_usr_type,'Y') = 'S' or fk_user = ?) "
                    + " and t.fk_study = ? " + "  and fk_account = ? ";

            //System.out.println(mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, userId);
            pstmt.setInt(2, studyId);
            pstmt.setInt(3, accountId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                total_sites = rs.getInt("total_sites");

            }

        } catch (SQLException ex) {
            Rlog.fatal("studySite",
                    "studySiteDao.getCountSitesForUserStudy EXCEPTION IN FETCHING from table"
                            + ex);
            return 0;

        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();

            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();

            } catch (Exception e) {
            }

        }
        return total_sites;
    }

    /**
     * Gets All site Ids and site names for a specific study of a user where the
     * site rights are given.
     *
     * @param studyId
     *            Pk of the study
     * @param userId
     *            Pk of the user
     * @param accId
     *            Pk of the account
     */

    public void getSitesForUserStudy(int studyId, int userId, int accountId) {
        PreparedStatement pstmt = null;
        Connection conn = null;

        int total_sites = 0;
        try {
            conn = getConnection();

            String mysql = "select distinct fk_site, "
                    + "(select site_name from er_site where pk_site=fk_site ) as site_name "
                    + " from er_study_site_rights, er_user "
                    + " where pk_user=fk_user " + " and fk_user = ? "
                    + " and fk_study = ?" + " and fk_account = ?"
                    + " and USER_STUDY_SITE_RIGHTS = 1 ";

            //System.out.println(mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, userId);
            pstmt.setInt(2, studyId);
            pstmt.setInt(3, accountId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setSiteIds(new Integer(rs.getInt("fk_site")));
                setSiteNames(rs.getString("site_name"));

            }
        } catch (SQLException ex) {
            Rlog.fatal("studySite",
                    "studySiteDao.getSitesForUserStudy EXCEPTION IN FETCHING FROM  table"
                            + ex);

        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();

            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();

            } catch (Exception e) {
            }

        }

    }

    /**
     * Gets Study Sites and there Local sample size
     *
     * @param studyId
     *            Pk of the study
     */

    public void getStdSitesLSampleSize(int studyId) {
        PreparedStatement pstmt = null;
        Connection conn = null;

        int total_sites = 0;
        try {
            conn = getConnection();

            String mysql = "select pk_studysites, "
                    + " fk_site, "
                    + " (select site_name from er_site where pk_site=fk_site) as site_name,"
                    + " STUDYSITE_LSAMPLESIZE " + " from er_studysites  "
                    + " where fk_study = ? ";

            //System.out.println(mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, studyId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setStudySiteIds(new Integer(rs.getInt("pk_studysites")));
                setSiteIds(new Integer(rs.getInt("fk_site")));
                setSiteNames(rs.getString("site_name"));
                setLSampleSize(rs.getString("STUDYSITE_LSAMPLESIZE"));

            }
        } catch (SQLException ex) {
            Rlog.fatal("studySite",
                    "studySiteDao.getStdSitesLSampleSize EXCEPTION IN FETCHING FROM  table"
                            + ex);

        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();

            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();

            } catch (Exception e) {
            }

        }

    }

    /**
     * Updates all studySite's Local sample size
     *
     * @param studySiteIds
     *            Pks of the study sites
     * @param localSamples
     *            local sample sizes
     * @param modifiedBy
     * @param ipAdd
     */

    public int updateLSampleSizes(String[] studySiteIds, String[] localSamples,
            int modifiedBy, String ipAdd) {

        CallableStatement cstmt = null;
        Connection conn = null;
        int ret = -1;

        try {

            conn = getConnection();

            ArrayDescriptor dStudySites = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inStudySites = new ARRAY(dStudySites, conn, studySiteIds);

            ArrayDescriptor dLocalSamples = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inLocalSamples = new ARRAY(dLocalSamples, conn, localSamples);

            cstmt = conn
                    .prepareCall("{call pkg_studystat.sp_update_local_sample_size(?,?,?,?,?)}");

            cstmt.setArray(1, inStudySites);

            cstmt.setArray(2, inLocalSamples);

            cstmt.setInt(3, modifiedBy);

            cstmt.setString(4, ipAdd);

            cstmt.registerOutParameter(5, java.sql.Types.INTEGER);

            cstmt.execute();

            ret = cstmt.getInt(5);

            return ret;

        }

        catch (SQLException ex) {
            Rlog.fatal("studySite",
                    "In updateLSampleSizes in studySitedao line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return ret;

    }

    // /////////////////////////////////////
    /**
     * Gets site Ids and site names for a specific study of a user where the
     * site rights are given and the site is added in the study
     *
     * @param studyId
     *            Pk of the study
     * @param userId
     *            Pk of the user
     * @param accId
     *            Pk of the account
     *            @deprecated
     *
     */

    public void getAddedAccessSitesForStudy(int studyId, int userId,
            int accountId) {
        PreparedStatement pstmt = null;
        Connection conn = null;

        int total_sites = 0;
        try {
            conn = getConnection();

            String mysql = " select distinct fk_site, "
                    + " (select site_name from er_site where pk_site=fk_site ) as site_name "
                    + " from er_study_site_rights, er_user "
                    + " where pk_user=fk_user  "
                    + " and fk_user = ? "
                    + " and fk_study = ?  "
                    + " and fk_account = ? "
                    + " and USER_STUDY_SITE_RIGHTS = 1 "
                    + " and fk_site "
                    + " in(select fk_site from er_studysites where fk_study = ?) "
                    + " union "
                    + " select  distinct fk_site,(select site_name from er_site where pk_site=fk_site ) as site_name "
                    + " from er_study_site_rights, er_user "
                    + " where pk_user=fk_user  "
                    + " and (select count(*) from er_studyteam, er_user where "
                    + " pk_user = fk_user "
                    + " and fk_account = ? "
                    // + " and fk_user = ? "
                    + " and fk_study= ? "
                    + " and nvl(study_team_usr_type,'Y') = 'S') > 0 "
                    + " and fk_study = ?  "
                    + " and fk_account = ? "
                    + " and USER_STUDY_SITE_RIGHTS = 1 "
                    + " and fk_site  "
                    + " in(select fk_site from er_studysites where fk_study = ?) ";

            //System.out.println(mysql);

            pstmt = conn.prepareStatement(mysql);

            pstmt.setInt(1, userId);
            pstmt.setInt(2, studyId);
            pstmt.setInt(3, accountId);
            pstmt.setInt(4, studyId);
            pstmt.setInt(5, accountId);

            pstmt.setInt(6, studyId);
            pstmt.setInt(7, studyId);
            pstmt.setInt(8, accountId);
            pstmt.setInt(9, studyId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setSiteIds(new Integer(rs.getInt("fk_site")));
                setSiteNames(rs.getString("site_name"));

            }
        } catch (SQLException ex) {
            Rlog.fatal("studySite",
                    "studySiteDao.getAddedAccessSitesForStudy EXCEPTION IN FETCHING FROM  table"
                            + ex);

        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();

            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();

            } catch (Exception e) {
            }

        }

    }

    // /anu /**
    /*
     * * Gets users of an organization in study team rights are given and the
     * site is added in the study
     *
     * @param studyId Pk of the study
     *
     * @param accId Pk of the account @param siteId Pk of the user
     */

    public int deleteOrgFromStudyTeam(int studyId, int accId, int siteId, int userId) {

        CallableStatement cstmt = null;
        Connection conn = null;
        int ret = 0;

        try {

            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call pkg_user.SP_DELETE_SITE_AND_USERS(?,?,?,?)}");
            cstmt.setInt(1, studyId);
            cstmt.setInt(2, accId);
            cstmt.setInt(3, siteId);
            cstmt.setInt(4, userId);

            cstmt.execute();
        }

        catch (SQLException ex) {
            Rlog
                    .fatal(
                            "studySite",
                            "In deleteOrgFromStudyTeam in studySitedao line EXCEPTION IN calling the procedure"
                                    + ex);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return ret;

    }

    // /////////
    public int checkSuperUser(int studyId, int userId, int accountId) {

        int rows = 0;
        int total_sites = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();

            String mysql = " select  nvl(pkg_superuser.F_Is_Superuser(?,?),0) from dual ";

            //System.out.println(mysql);

            pstmt = conn.prepareStatement(mysql);

            pstmt.setInt(1, userId);
            pstmt.setInt(2, studyId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                total_sites = rs.getInt(1);

            }

        } catch (SQLException ex) {
            Rlog.fatal("studySite",
                    "studySiteDao.checkSuperUser EXCEPTION IN FETCHING from table"
                            + ex);
            return 0;

        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();

            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();

            } catch (Exception e) {
            }

        }
        return total_sites;
    }

    // /////////////////////////////////////
    /**
     * Gets site Ids and site names :When superuser is not added in study team.
     * Then organization which are added in study team and are given rights in
     * user details page of loggedin user will be available in Study Status and
     * Manage Patient >> Enrolled pages When superuser is added in study team
     * then all those org will appear for the user in study status and Manage
     * Patients >> Enrolled page which are added in study team and the org to
     * which loggedin user is given rights through 'multiple org access rights'.
     * Here rights given through user details will not come into action. As per
     * new requirements .. Anu..
     *
     * @param studyId
     *            Pk of the study
     * @param userId
     *            Pk of the user
     * @param accId
     *            Pk of the account
     */

    /*
     * As per change in requirements, 06/28/05, this method will be used only in
     * manage patients - enrolled page For study status page we need the same
     * organizations as in study team page - Sonia Abrol
     */
    /* modified by sonia on 11/2/07 for super user implementtion change*/
    public void getSitesForStatAndEnrolledPat(int studyId, int userId, int accId) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sbSql = new StringBuffer();

        try {
            conn = getConnection();


            sbSql.append(" SELECT DISTINCT ss.fk_site, site_name, site_altid, site_restrict, LOWER(site_name) AS sn ");
            sbSql.append(" FROM er_studysites ss, er_site ");
            sbSql.append(" WHERE ss.fk_study = ? AND pk_site = ss.fk_site");
            sbSql.append(" and ( pkg_user.f_chk_right_for_studysite(ss.fk_study,?,ss.fk_site) > 0  )  ORDER BY sn ");

            Rlog.fatal("studySite",
                    "studySiteDao.getSitesForStatAndEnrolledPat" +  sbSql.toString());

            pstmt = conn.prepareStatement(sbSql.toString());


            pstmt.setInt(1, studyId);
            pstmt.setInt(2, userId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setSiteIds(new Integer(rs.getInt("fk_site")));
                setSiteNames(rs.getString("site_name"));
                setIsRestricteds(rs.getString("site_restrict"));
                setAltIds(rs.getString("site_altid"));
            }
        } catch (SQLException ex) {
            Rlog.fatal("studySite",
                    "studySiteDao.getSitesForStatAndEnrolledPat EXCEPTION IN FETCHING FROM  table"
                            + ex);

        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();

            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();

            } catch (Exception e) {
            }

        }

    }
    
    public String getSiteNameForSiteId(int siteId) {
    	
        Rlog.debug("studySite", "inside method getSiteNameForSiteId siteId" + siteId);
        
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";
        String siteName="";
        try {
            conn = getConnection();
            sql = "select site_name from er_site where " + " pk_site = ?  " ;
            Rlog.debug("studySite", "inside method getCntForSiteInStudy sql" + sql);
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, siteId);           
            ResultSet rs = pstmt.executeQuery();            
            while (rs.next()) {
                siteName = rs.getString("site_name");
            }            
        } catch (SQLException ex) {
            Rlog.fatal("studySite", "error in getSiteNameForSiteId" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }        
        return siteName;
    }
    
    public int getPkSiteBySiteName(String siteName) {    	
        Rlog.debug("studySite", "inside method getPkSiteBySiteName siteId" + siteName);        
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";
        int pkSite=0;
        try {
            conn = getConnection();
            sql = "select pk_site from er_site where site_name= ? ";            
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, siteName);           
            ResultSet rs = pstmt.executeQuery();            
            while (rs.next()) {
                pkSite = rs.getInt("pk_site");
            }            
        } catch (SQLException ex) {
            Rlog.fatal("studySite", "error in getPkSiteBySiteName" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }        
        return pkSite;
    }

}
