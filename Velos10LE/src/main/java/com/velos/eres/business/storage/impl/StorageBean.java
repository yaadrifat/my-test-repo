/*
 * Classname			StorageBean.class
 *
 * Version information
 *
 * Date					06/26/2007
 *
 * Copyright notice
 */

package com.velos.eres.business.storage.impl;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.sql.Clob;
import javax.persistence.NamedQuery;
import javax.persistence.NamedQueries;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The Storage BMP entity bean.<br>
 * <br>
 *
 * @author Manimaran
 * @version 1.0 06/26/2007
 */
@Entity
@Table(name = "er_storage")
//Query modified by Manimaran to fix the issue3158
@NamedQueries({
	@NamedQuery(name = "findStorageName", query = "SELECT OBJECT(storage) FROM StorageBean storage where UPPER(trim(STORAGE_NAME)) = UPPER(trim(:storageName)) and UPPER(trim(STORAGE_ID)) = UPPER(trim(:storageId))"),
	@NamedQuery(name = "findStorageId", query = "SELECT OBJECT(storage) FROM StorageBean storage where UPPER(trim(STORAGE_ID)) = UPPER(trim(:storageId)) and FK_ACCOUNT = :accountId")
})

public class StorageBean implements Serializable {

    private static final long serialVersionUID = 3761410811205333553L;

    /**
     * PKStorage
     */
    public int pkStorage;

    /**
     * Storage Id
     */
    public String storageId;

    /**
     * Storage Name
     */
    public String storageName;

    /**
     * fkCodelstStorageType
     */
    public Integer fkCodelstStorageType;

    /**
     * fkStorage
     */
    public Integer fkStorage;

    /**
     * storageCapNum
     */
    public Integer storageCapNum;

    /**
     * fkCodelstCapUnits
     */
    public Integer fkCodelstCapUnits;

    //  KM-Removal of Storage status column in table.
    /**
     * storageStatus
     */
    //public String storageStatus;

    /**
     * storageDim1CellNum
     */
    public Integer storageDim1CellNum;


    /**
     * storageDim1Naming
     */
    public String storageDim1Naming;

    /**
     * storageDim1Order
     */
    public String storageDim1Order;

    /**
     * storageDim2CellNum
     */
    public Integer storageDim2CellNum;


    /**
     * storageDim2Naming
     */
    public String storageDim2Naming;

    /**
     * storageDim2Order
     */
    public String storageDim2Order;

    /**
     * storageNotes
     */
    public String storageNotes;

    /**
     * storageAvailUnitCnt
     */
    public Integer storageAvailUnitCnt;

    /**
     * storageCoordinateX
     */
    public Integer storageCoordinateX;

    /**
     * storageCoordinateY
     */
    public Integer storageCoordinateY;


    /**
     * storageLabel
     */
    public String storageLabel;

    /**
     * fkCodelstChildStype
     */
    public Integer fkCodelstChildStype;

    /**
     * the record creator
     */
    public Integer creator;

    /**
     * last modified by
     */
    public Integer modifiedBy;

    /**
     * the IP Address
     */
    public String ipAdd;

    /**
     * Account Id
     */
    public Integer accountId;

    /**
     * Template
     */

    public Integer template;
    
    /**
     * fk storage kit.
     */
   Integer fkStorageKit;

    //JM: 30 Jul2009: #invp2.15

    public Integer storageTemplateType;
    public String storageAlternateId;
    public Integer storageUnitClass;
    public Integer storageKitCategory;
    public Integer multiSpecimen;


    // GETTER SETTER METHODS

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "seq_er_storage", allocationSize=1)

    @Column(name = "PK_STORAGE")
    public int getPkStorage() {
        return this.pkStorage;
    }

    public void setPkStorage(int pkStorage) {
        this.pkStorage = pkStorage;
    }

    @Column(name = "STORAGE_ID")
    public String getStorageId() {
        return this.storageId;
    }

    public void setStorageId(String storageId) {
        this.storageId = storageId;
    }

    @Column(name = "STORAGE_NAME")
    public String getStorageName() {
        return this.storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    @Column(name = "FK_CODELST_STORAGE_TYPE")
    public String getFkCodelstStorageType() {
    	return StringUtil.integerToString(this.fkCodelstStorageType);
    }


    public void setFkCodelstStorageType(String fkCodelstStorageType) {
        this.fkCodelstStorageType = StringUtil.stringToInteger(fkCodelstStorageType);
    }

    @Column(name = "FK_STORAGE")
    public String getFkStorage() {
    	return StringUtil.integerToString(this.fkStorage);
    }


    public void setFkStorage(String fkStorage) {
        this.fkStorage = StringUtil.stringToInteger(fkStorage);
    }

    @Column(name = "STORAGE_CAP_NUMBER")
    public String getStorageCapNum() {
    	return StringUtil.integerToString(this.storageCapNum);
    }


    public void setStorageCapNum(String storageCapNum) {
        this.storageCapNum = StringUtil.stringToInteger(storageCapNum);
    }

    @Column(name = "FK_CODELST_CAPACITY_UNITS")
    public String getFkCodelstCapUnits() {
    	return StringUtil.integerToString(this.fkCodelstCapUnits);
    }


    public void setFkCodelstCapUnits(String fkCodelstCapUnits) {
        this.fkCodelstCapUnits = StringUtil.stringToInteger(fkCodelstCapUnits);
    }


    @Column(name = "STORAGE_DIM1_CELLNUM")
    public String getStorageDim1CellNum() {
    	return StringUtil.integerToString(this.storageDim1CellNum);
    }


    public void setStorageDim1CellNum(String storageDim1CellNum) {
        this.storageDim1CellNum = StringUtil.stringToInteger(storageDim1CellNum);
    }

    @Column(name = "STORAGE_DIM1_NAMING")
    public String getStorageDim1Naming() {
        return this.storageDim1Naming;
    }

    public void setStorageDim1Naming(String storageDim1Naming) {
        this.storageDim1Naming = storageDim1Naming;
    }

    @Column(name = "STORAGE_DIM1_ORDER")
    public String getStorageDim1Order() {
        return this.storageDim1Order;
    }

    public void setStorageDim1Order(String storageDim1Order) {
        this.storageDim1Order = storageDim1Order;
    }

    @Column(name = "STORAGE_DIM2_CELLNUM")
    public String getStorageDim2CellNum() {
    	return StringUtil.integerToString(this.storageDim2CellNum);
    }


    public void setStorageDim2CellNum(String storageDim2CellNum) {
        this.storageDim2CellNum = StringUtil.stringToInteger(storageDim2CellNum);
    }

    @Column(name = "STORAGE_DIM2_NAMING")
    public String getStorageDim2Naming() {
        return this.storageDim2Naming;
    }

    public void setStorageDim2Naming(String storageDim2Naming) {
        this.storageDim2Naming = storageDim2Naming;
    }

    @Column(name = "STORAGE_DIM2_ORDER")
    public String getStorageDim2Order() {
        return this.storageDim2Order;
    }

    public void setStorageDim2Order(String storageDim2Order) {
        this.storageDim2Order = storageDim2Order;
    }


    @Transient
    public String getStorageNotes() {
    	return this.storageNotes;
    }

    public void setStorageNotes(String storageNotes) {
        this.storageNotes = storageNotes;
    }

    @Column(name = "STORAGE_AVAIL_UNIT_COUNT")
    public String getStorageAvailUnitCnt() {
    	return StringUtil.integerToString(this.storageAvailUnitCnt);
    }


    public void setStorageAvailUnitCnt(String storageAvailUnitCnt) {
        this.storageAvailUnitCnt = StringUtil.stringToInteger(storageAvailUnitCnt);
    }

    @Column(name = "STORAGE_COORDINATE_X")
    public String getStorageCoordinateX() {
    	return StringUtil.integerToString(this.storageCoordinateX);
    }


    public void setStorageCoordinateX(String storageCoordinateX) {
        this.storageCoordinateX = StringUtil.stringToInteger(storageCoordinateX);
    }

    @Column(name = "STORAGE_COORDINATE_Y")
    public String getStorageCoordinateY() {
    	return StringUtil.integerToString(this.storageCoordinateY);
    }


    public void setStorageCoordinateY(String storageCoordinateY) {
        this.storageCoordinateY = StringUtil.stringToInteger(storageCoordinateY);
    }

    @Column(name = "STORAGE_LABEL")
    public String getStorageLabel() {
        return this.storageLabel;
    }

    public void setStorageLabel(String storageLabel) {
        this.storageLabel = storageLabel;
    }


    @Column(name = "FK_CODELST_CHILD_STYPE")
    public String getFkCodelstChildStype() {
    	return StringUtil.integerToString(this.fkCodelstChildStype);
    }


    public void setFkCodelstChildStype(String fkCodelstChildStype) {
        this.fkCodelstChildStype = StringUtil.stringToInteger(fkCodelstChildStype);
    }


    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    //KM-09182007-AccountId added.
    @Column(name = "FK_ACCOUNT")
    public String getAccountId() {
    	return StringUtil.integerToString(this.accountId);
    }

    public void setAccountId(String accountId) {
    	if (accountId != null && accountId.trim().length() > 0)
            this.accountId = Integer.valueOf(accountId);
    }

    //KM-Template added.
    @Column(name = "STORAGE_ISTEMPLATE")
    public String getTemplate() {
    	return StringUtil.integerToString(this.template);

    }

    public void setTemplate(String template){
    	 this.template = StringUtil.stringToInteger(template);
    }

    //JM: 30Jul2009: invp2.15 (part-1)
    @Column(name = "storage_template_type")
    public String getStorageTemplateType() {
    	return StringUtil.integerToString(this.storageTemplateType);

    }

    public void setStorageTemplateType(String storageTemplateType){
    	 this.storageTemplateType = StringUtil.stringToInteger(storageTemplateType);
    }


    @Column(name = "storage_alternaleId")
    public String getStorageAlternateId() {
        return this.storageAlternateId;
    }

    public void setStorageAlternateId(String storageAlternateId) {
        this.storageAlternateId = storageAlternateId;
    }


    @Column(name = "storage_unit_class")
    public String getStorageUnitClass() {
    	return StringUtil.integerToString(this.storageUnitClass);

    }

    public void setStorageUnitClass(String storageUnitClass){
    	 this.storageUnitClass = StringUtil.stringToInteger(storageUnitClass);
    }

  //JM: 11Aug2009, invp2.15.3
    @Column(name = "STORAGE_KIT_CATEGORY")
    public String getStorageKitCategory() {
    	return StringUtil.integerToString(this.storageKitCategory);

    }

    public void setStorageKitCategory(String storageKitCategory){
    	 this.storageKitCategory = StringUtil.stringToInteger(storageKitCategory);
    }

//Bikash Fk_storage Kit for Invp 2.8
    
    @Column(name = "FK_STORAGE_KIT")
    public String getFkStorageKit() {
    	return StringUtil.integerToString(this.fkStorageKit);

    }

    public void setFkStorageKit(String fkStorageKit){
    	 this.fkStorageKit = StringUtil.stringToInteger(fkStorageKit);
    }

    @Column(name = "STORAGE_MULTI_SPECIMEN")
    public String getMultiSpecimenStorage() {
    	return StringUtil.integerToString(this.multiSpecimen);
    }
    public void setMultiSpecimenStorage(String multiSpecimen){
    	this.multiSpecimen = StringUtil.stringToInteger(multiSpecimen);
    }
    // END OF GETTER SETTER METHODS

    public int updateStorage(StorageBean ssk) {
        try {


            setPkStorage(ssk.getPkStorage());
            setStorageId(ssk.getStorageId());
            setStorageName(ssk.getStorageName());
            setFkCodelstStorageType(ssk.getFkCodelstStorageType());
    		setFkStorage(ssk.getFkStorage());
    		setStorageCapNum(ssk.getStorageCapNum());
    		setFkCodelstCapUnits(ssk.getFkCodelstCapUnits());
    		setStorageDim1CellNum(ssk.getStorageDim1CellNum());
    		setStorageDim1Naming(ssk.getStorageDim1Naming());
    		setStorageDim1Order(ssk.getStorageDim1Order());
    		setStorageDim2CellNum(ssk.getStorageDim2CellNum());
    		setStorageDim2Naming(ssk.getStorageDim2Naming());
    		setStorageDim2Order(ssk.getStorageDim2Order());
    		//setStorageNotes(ssk.getStorageNotes());
    		setStorageAvailUnitCnt(ssk.getStorageAvailUnitCnt());
    		setStorageCoordinateX(ssk.getStorageCoordinateX());
    		setStorageCoordinateY(ssk.getStorageCoordinateY());
    		setStorageLabel(ssk.getStorageLabel());
    		setFkCodelstChildStype(ssk.getFkCodelstChildStype());
            setCreator(ssk.getCreator());
            setModifiedBy(ssk.getModifiedBy());
            setIpAdd(ssk.getIpAdd());
            setAccountId(ssk.getAccountId());
            setTemplate(ssk.getTemplate());
            setStorageTemplateType(ssk.getStorageTemplateType());
            setStorageAlternateId(ssk.getStorageAlternateId());
            setStorageUnitClass(ssk.getStorageUnitClass());
            setStorageKitCategory(ssk.getStorageKitCategory());
            this.setFkStorageKit(ssk.getFkStorageKit());
            setMultiSpecimenStorage(ssk.getMultiSpecimenStorage());
            Rlog.debug("storage", "StorageBean.updateStorage");
            return 0;
        } catch (Exception e) {

            Rlog.fatal("storage", " error in StorageBean.updateStorage");
            return -2;
        }
    }

    public StorageBean() {

    }

    public StorageBean(int pkStorage, String storageId, String storageName, String fkCodelstStorageType, String fkStorage, String storageCapNum,
    		String fkCodelstCapUnits,  String storageDim1CellNum, String storageDim1Naming, String storageDim1Order,
    		String storageDim2CellNum, String storageDim2Naming, String storageDim2Order, String storageNotes, String storageAvailUnitCnt,
    		String storageCoordinateX, String storageCoordinateY, String storageLabel,String fkCodelstChildStype, String creator, String modifiedBy, String ipAdd, String accountId, String template,
    		String storageTemplateType, String storageAlternateId, String storageUnitClass, String storageKitCategory,String fkStorageKit,String multiSpecimen) {
        super();
        // TODO Auto-generated constructor stub
        setPkStorage(pkStorage);
		setStorageId(storageId);
		setStorageName(storageName);
		setFkCodelstStorageType(fkCodelstStorageType);
		setFkStorage(fkStorage);
		setStorageCapNum(storageCapNum);
		setFkCodelstCapUnits(fkCodelstCapUnits);
		setStorageDim1CellNum(storageDim1CellNum);
		setStorageDim1Naming(storageDim1Naming);
		setStorageDim1Order(storageDim1Order);
		setStorageDim2CellNum(storageDim2CellNum);
		setStorageDim2Naming(storageDim2Naming);
		setStorageDim2Order(storageDim2Order);
		//setStorageNotes(storageNotes);
		setStorageAvailUnitCnt(storageAvailUnitCnt);
		setStorageCoordinateX(storageCoordinateX);
		setStorageCoordinateY(storageCoordinateY);
		setStorageLabel(storageLabel);
		setFkCodelstChildStype(fkCodelstChildStype);
		setCreator(creator);
		setModifiedBy(modifiedBy);
		setIpAdd(ipAdd);
		setAccountId(accountId);
		setTemplate(template);
		setStorageTemplateType(storageTemplateType);
        setStorageAlternateId(storageAlternateId);
        setStorageUnitClass(storageUnitClass);
        setStorageKitCategory(storageKitCategory);
        setFkStorageKit(fkStorageKit);
        setMultiSpecimenStorage(multiSpecimen);

    }
}
