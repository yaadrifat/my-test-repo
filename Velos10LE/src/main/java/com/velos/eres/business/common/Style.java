/*
 * Classname			Style.class
 * 
 * Version information 	1.0
 *
 * Date					27/06/2003
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.common;

import java.io.Serializable;

import com.velos.eres.service.util.StringUtil;

/**
 * Style for keeping some common information of the fields, etc
 * 
 * @author Sonia Kaura
 * @version : 1.030/06/2003
 */

public class Style implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3907213749596467762L;

    public String bold;

    public String italics;

    public String sameLine;

    public String align;

    public String underline;

    public String color;

    public String font;

    public String fontSize;

    public String bgColor;

    public String expLabel;

    // GETTERS AND SETTERS

    /**
     * 
     * 
     * @return bold
     */
    public String getBold() {
        return this.bold;
    }

    /**
     * 
     * 
     * @param bold
     */
    public void setBold(String bold) {
        this.bold = bold;
    }

    /**
     * 
     * 
     * @return italics
     */
    public String getItalics() {
        return this.italics;
    }

    /**
     * 
     * 
     * @param italics
     */
    public void setItalics(String italics) {
        this.italics = italics;
    }

    /**
     * 
     * 
     * @return sameLine
     */
    public String getSameLine() {
        return this.sameLine;
    }

    /**
     * 
     * 
     * @param sameLine
     */
    public void setSameLine(String sameLine) {
        this.sameLine = sameLine;
    }

    /**
     * 
     * 
     * @return align
     */
    public String getAlign() {
        return this.align;
    }

    /**
     * 
     * 
     * @param align
     */
    public void setAlign(String align) {
        this.align = align;
    }

    /**
     * 
     * 
     * @return underline
     */
    public String getUnderline() {
        return this.underline;
    }

    /**
     * 
     * 
     * @param underline
     */
    public void setUnderline(String underline) {
        this.underline = underline;
    }

    /**
     * 
     * 
     * @return color
     */
    public String getColor() {
        return this.color;
    }

    /**
     * 
     * 
     * @param color
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * 
     * 
     * @return font
     */
    public String getFont() {
        return this.font;
    }

    /**
     * 
     * 
     * @param font
     */
    public void setFont(String font) {
        this.font = font;
    }

    /**
     * 
     * 
     * @return fontSize
     */
    public String getFontSize() {
        return this.fontSize;
    }

    /**
     * 
     * 
     * @param fontSize
     */
    public void setFontSize(String fontSize) {
        this.fontSize = fontSize;
    }

    /**
     * 
     * 
     * @return bgColor
     */
    public String getBgColor() {
        return this.bgColor;
    }

    /**
     * 
     * 
     * @param bgColor
     */
    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    /**
     * @return whether Field Label has to be expanded or not
     */
    public String getExpLabel() {
        if (StringUtil.isEmpty(this.expLabel))
            this.expLabel = "0";
        return this.expLabel;
    }

    /**
     * 
     * @param Field
     *            Label has to be expanded or not
     */
    public void setExpLabel(String expLabel) {
        this.expLabel = expLabel;
    }

    public Style() {
        this.bold = "0";
        this.italics = "0";
        this.sameLine = "0";
        this.align = "left";
        this.underline = "0";
        this.color = "";
        this.font = "";
        this.fontSize = "";
        this.bgColor = "";
        this.expLabel = "0";

    }

    public Style(String bold, String italics, String sameLine, String align,
            String underline, String color, String font, String fontSize,
            String bgColor, String expLabel) {

        setBold(bold);
        setItalics(italics);
        setSameLine(sameLine);
        setAlign(align);
        setUnderline(underline);
        setColor(color);
        setFont(font);
        setFontSize(fontSize);
        setBgColor(bgColor);
        setExpLabel(expLabel);

    }

    /**
     * 
     * 
     * @param style
     */
    public void setStyle(Style style) {
        setBold(style.getBold());
        setItalics(style.getItalics());
        setSameLine(style.getSameLine());
        setAlign(style.getAlign());
        setUnderline(style.getUnderline());
        setColor(style.getColor());
        setFont(style.getFont());
        setFontSize(style.getFontSize());
        setBgColor(style.getBgColor());
        setExpLabel(style.getExpLabel());

    }

    /**
     * 
     * 
     * @return
     */
    public Style getStyle() {
        return new Style(bold, italics, sameLine, align, underline, color,
                font, fontSize, bgColor, expLabel);
    }

    public boolean equals(Object anObject) {
        if (!(anObject instanceof Style))
            return false;
        Style aStyle = (Style) anObject;

        return (aStyle.getBold().equals(getBold())
                && aStyle.getItalics().equals(getItalics())
                && aStyle.getSameLine().equals(getSameLine())
                && aStyle.getAlign().equals(getAlign())
                && aStyle.getUnderline().equals(getUnderline())
                && aStyle.getColor().equals(getColor())
                && aStyle.getFont().equals(getFont())
                && aStyle.getFontSize().equals(getFontSize())
                && aStyle.getBgColor().equals(getBgColor()) && aStyle
                .getExpLabel().equals(getExpLabel()));

    }

    public String getAlignVal() {
        if (!StringUtil.isEmpty(this.align))
            return "align = \"" + this.align + "\"";
        else
            return "";

    }

    public String getDisplayStartTags() {

        StringBuffer sbStyle = new StringBuffer();

        // check color

        if (!StringUtil.isEmpty(this.color)) {
            sbStyle.append("<font color=\"" + color + "\">");

        }

        // check bold

        if (!StringUtil.isEmpty(this.bold)) {
            if (this.bold.equals("1")) {
                sbStyle.append("<b>");

            }

        }

        // check italics

        if (!StringUtil.isEmpty(this.italics)) {
            if (this.italics.equals("1")) {
                sbStyle.append("<i>");

            }

        }

        // check underline
        if (!StringUtil.isEmpty(this.underline)) {
            if (this.underline.equals("1")) {
                sbStyle.append("<u>");

            }

        }

        return sbStyle.toString();

    }

    public String getDisplayEndTags() {

        StringBuffer sbStyle = new StringBuffer();

        // check underline
        if (!StringUtil.isEmpty(this.underline)) {
            if (this.underline.equals("1")) {
                sbStyle.append("</u>");

            }

        }

        // check italics

        if (!StringUtil.isEmpty(this.italics)) {
            if (this.italics.equals("1")) {
                sbStyle.append("</i>");

            }

        }

        // check bold

        if (!StringUtil.isEmpty(this.bold)) {
            if (this.bold.equals("1")) {
                sbStyle.append("</b>");

            }

        }

        // check color

        if (!StringUtil.isEmpty(this.color)) {
            sbStyle.append("</font>");

        }

        return sbStyle.toString();

    }

    // end of class

}