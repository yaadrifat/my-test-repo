/*
 * Classname			CatLibBean
 * 
 * Version information : 1.0
 *
 * Date					06/25/2003
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.business.catLib.impl;

/* IMPORT STATEMENTS */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* END OF IMPORT STATEMENTS */

/**
 * The Category CMP entity bean.<br>
 * <br>
 * 
 * @author
 * 
 * @version 1.0, 06/25/2003
 * 
 * @ejbHome CatLibHome
 * 
 * @ejbRemote CatLibRObj
 */
@Entity
@Table(name = "er_catlib")
//Modified by Manimaran to fix the bugs 2317&2318, to avoid duplicate Field category and Form type name.
@NamedQuery(name = "findByCatLibIdentifier", query = "SELECT OBJECT(cat) FROM CatLibBean cat where fk_account = :fk_account AND UPPER(trim(CATLIB_NAME)) = UPPER(trim(:CATLIB_NAME)) AND CATLIB_TYPE = :CATLIB_TYPE AND RECORD_TYPE <> 'D'")
public class CatLibBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3258407339848971831L;

    /*
     * the Category Id
     */
    public int catLibId;

    /*
     * the category account
     */
    public Integer catLibAcc;

    /*
     * the category and Form Type ( C-category, F-Form Type)
     */
    public String catLibType;

    /*
     * the category name
     */
    public String catLibName;

    /*
     * the category description
     */
    public String catLibDesc;

    /*
     * the category creator
     */
    public Integer catLibCreator;

    /*
     * last modified by
     */
    public Integer lastModifiedBy;

    /*
     * the IP address of the creator
     */
    public String iPAdd;

    /*
     * Record Type
     */

    public String recordType;

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_CATLIB", allocationSize=1)
    @Column(name = "PK_CATLIB")
    public int getCatLibId() {
        return this.catLibId;
    }

    public void setCatLibId(int catLibId) {
        this.catLibId = catLibId;
    }

    @Column(name = "FK_ACCOUNT")
    public String getCatLibAcc() {
        return StringUtil.integerToString(this.catLibAcc);

    }

    public void setCatLibAcc(String catLibAccount) {
        if (catLibAccount != null && catLibAccount.trim().length() > 0)
            this.catLibAcc = Integer.valueOf(catLibAccount);

    }

    @Column(name = "CATLIB_TYPE")
    public String getCatLibType() {
        return this.catLibType;

    }

    public void setCatLibType(String catLibType) {
        this.catLibType = catLibType;
    }

    @Column(name = "CATLIB_NAME")
    public String getCatLibName() {
        return this.catLibName;
    }

    public void setCatLibName(String catLibName) {
        this.catLibName = catLibName;
    }

    @Column(name = "CATLIB_DESC")
    public String getCatLibDesc() {
        return this.catLibDesc;
    }

    public void setCatLibDesc(String catLibDesc) {
        this.catLibDesc = catLibDesc;
    }

    @Column(name = "CREATOR")
    public String getCatLibCreator() {
        return StringUtil.integerToString(this.catLibCreator);

    }

    public void setCatLibCreator(String catLibCreator) {
        if (catLibCreator != null)
            this.catLibCreator = Integer.valueOf(catLibCreator);

    }

    @Column(name = "LAST_MODIFIED_BY")
    public String getLastModifiedBy() {
        return StringUtil.integerToString(this.lastModifiedBy);
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        if (lastModifiedBy != null)
            this.lastModifiedBy = Integer.valueOf(lastModifiedBy);
    }

    @Column(name = "IP_ADD")
    public String getIPAdd() {
        return this.iPAdd;
    }

    public void setIPAdd(String iPAdd) {
        this.iPAdd = iPAdd;
    }

    @Column(name = "RECORD_TYPE")
    public String getRecordType() {
        return this.recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Creates a category in the database taking the values for various fields
     * from the CatLibStateKeeper Object, clsk
     * 
     * @param clsk
     *            The StateKeeper containing details of the category
     * 
     * @see CatLibStateKeeper
     */

    /**
     * Retrieves the details of a category in CatLibStateKeeper Object
     * 
     * @return CatLibStateKeeper consisting of the category details
     * 
     * @see CatLibStateKeeper
     */
    /*
     * public CatLibStateKeeper getCatLibStateKeeper() { Rlog.debug("account",
     * "CatLibBean.getCatLibStateKeeper getCatLibId() " + getCatLibId()); return
     * (new CatLibStateKeeper(getCatLibId(), getCatLibAcc(), getCatLibType(),
     * getCatLibName(), getCatLibDesc(), getCatLibCreator(),
     * getLastModifiedBy(), getIPAdd(), getRecordType())); }
     */

    /**
     * Sets up a state keeper containing details of category
     */

    /*
     * public void setCatLibStateKeeper(CatLibStateKeeper clsk) { GenerateId
     * generateId = null; try { Connection conn = null; conn = getConnection();
     * Rlog.debug("catlib", "CatLibBean.setCatLibStateKeeper() Connection :" +
     * conn); this.catLibId = generateId.getId("SEQ_ER_CATLIB", conn);
     * Rlog.debug("catlib", "CatLibBean.setCatLibStateKeeper() catLibId :" +
     * catLibId); conn.close();
     * 
     * setCatLibAcc(clsk.getCatLibAcc()); setCatLibType(clsk.getCatLibType());
     * setCatLibName(clsk.getCatLibName()); setCatLibDesc(clsk.getCatLibDesc());
     * setCatLibCreator(clsk.getCatLibCreator());
     * setLastModifiedBy(clsk.getLastModifiedBy()); setIPAdd(clsk.getIPAdd());
     * setRecordType(clsk.getRecordType()); } catch (Exception e) {
     * Rlog.fatal("catlib", "Error in setCatLibStateKeeper() in CatLibBean " +
     * e); } }
     */

    /**
     * Saves the category details in the CatLibStateKeeper to the database and
     * returns 0 if the update is successful and -2 if it fails
     * 
     * @param clsk
     *            Category StateKeeper with the updated Category Information
     * 
     * @return 0 - if update is successful <br>
     *         -2 - if update fails
     * 
     * @see CatLibStateKeeper
     * 
     */
    public int updateCatLib(CatLibBean clsk) {
        try {
            setCatLibAcc(clsk.getCatLibAcc());
            setCatLibType(clsk.getCatLibType());
            setCatLibName(clsk.getCatLibName());
            setCatLibDesc(clsk.getCatLibDesc());
            setCatLibCreator(clsk.getCatLibCreator());
            setLastModifiedBy(clsk.getLastModifiedBy());
            setIPAdd(clsk.getIPAdd());
            setRecordType(clsk.getRecordType());
            Rlog.debug("catlib", "CatLibBean.updateAccount");
            return 0;
        } catch (Exception e) {
            Rlog.fatal("catlib", " error in CatLibBean.updateAccount");
            return -2;
        }
    }

    public CatLibBean() {

    }

    public CatLibBean(int catLibId, String catLibAcc, String catLibType,
            String catLibName, String catLibDesc, String catLibCreator,
            String lastModifiedBy, String iPAdd, String recordType) {
        super();
        // TODO Auto-generated constructor stub
        setCatLibId(catLibId);
        setCatLibAcc(catLibAcc);
        setCatLibType(catLibType);
        setCatLibName(catLibName);
        setCatLibDesc(catLibDesc);
        setCatLibCreator(catLibCreator);
        setLastModifiedBy(lastModifiedBy);
        setIPAdd(iPAdd);
        setRecordType(recordType);
    }

}// end of class
