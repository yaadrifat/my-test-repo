/*
 * Classname			PaymentDetailsDao
 * 
 * Version information 	1.0
 *
 * Date					11/17/2005
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * MilepaymentDao for getting milestone payments records
 * 
 * @author Sonia Abrol
 * @version : 1.0  11/17/2005
 */

public class PaymentDetailsDao extends CommonDAO implements java.io.Serializable {
	/**
	 * the PaymentDetail Primary Key
	 */
	public ArrayList id;

	/**
	 * the payment
	 */
	public ArrayList fkPayment;

	/**
	 * payment detail linked to
	 */
	public ArrayList linkedTo;

	/**
	 * the payment link to id
	 */
	public ArrayList linkToId;
	
	/**
	 * the payment link to level 1 id
	 */
	public ArrayList linkToLevel1;
	
	/**
	 * the payment link to level 2 id
	 */
	public ArrayList linkToLevel2;


	/**
	 * the payment detail amount
	 */
	public ArrayList amount;

	/**
	 * the id of the user who created the record
	 */
	public ArrayList creator;

	/**
	 * Id of the user who last modified the record
	 */
	public ArrayList lastModifiedBy;

	/**
	 * the IP address of the user who created or modified the record
	 */
	public ArrayList iPAdd;


	/**
	 * the total applied amount for the detail type 
	 */
	public ArrayList appliedTotal;


	/** stores mapping of level 2 with level. Key: level 1 id, value: PaymentDetailsDao object with level 2 data*/
	public Hashtable level2Mapping; 
	
	/**
	 * contains PaymentDetailsDao with level 1 data
	 */
	public PaymentDetailsDao arLevel1;
	
	/**
	 * the total applied amount for the detail type 
	 */
	public ArrayList paymentLinkDescription;

	/**
	 *ArrayList  FK to er_mileachieved, will be used for milestone related records. <br>
	 * (this will help identify the exact milestone achievement record for multiple achievements for the same patient and milestone combination)
	 */
	public ArrayList linkMileAchieved;

	public ArrayList<Float> holdBackAppliedDueToDateList;
	
	public Float holdBackAppliedDueToDate;
	
	public ArrayList<Boolean> holdBackFlag;
	
	public int paymentDetailRowCount ;
	
	public ArrayList<Double> prevHoldBackAmount;
	
	public int getPaymentDetailRowCount() {
		return paymentDetailRowCount;
	}


	public void setPaymentDetailRowCount(int paymentDetailRowCount) {
		this.paymentDetailRowCount = paymentDetailRowCount;
	}


	public PaymentDetailsDao getArLevel1() {
		return arLevel1;
	}


	public void setArLevel1(PaymentDetailsDao pd) {
		this.arLevel1 = pd;
	}


	
	public Hashtable getLevel2Mapping() {
		return level2Mapping;
	}


	public void setLevel2Mapping(Hashtable level2Mapping) {
		this.level2Mapping = level2Mapping;
	}

	public void setLevel2Mapping(String key, PaymentDetailsDao pd) {
		this.level2Mapping.put(key,pd);
	}

	public ArrayList getAppliedTotal() {
		return appliedTotal;
	}


	public void setAppliedTotal(ArrayList appliedTotal) {
		this.appliedTotal = appliedTotal;
	}

	public void setAppliedTotal(String appliedTotal) {
		this.appliedTotal.add(appliedTotal);
	}


	public PaymentDetailsDao() {
    	initial();
    }
	
	private void initial()
	{
		id = new ArrayList();
    	amount  = new ArrayList();
		creator = new ArrayList();
		fkPayment= new ArrayList();
		iPAdd = new ArrayList();
		lastModifiedBy = new ArrayList();
		linkedTo = new ArrayList();
		linkToId = new ArrayList();
		linkToLevel1 = new ArrayList();
		linkToLevel2 = new ArrayList();
		appliedTotal = new ArrayList();
		level2Mapping = new Hashtable();
		paymentLinkDescription = new ArrayList();
		linkMileAchieved = 	new ArrayList();
		holdBackAppliedDueToDateList=new ArrayList<Float>();
		holdBackFlag=new ArrayList<Boolean>();
		prevHoldBackAmount=new ArrayList<Double>();
	}
	
	
	public PaymentDetailsDao(PaymentDetailsDao pd) 
	{ 
		initial();
		
		setAppliedTotal(pd.getAppliedTotal());
    	setLinkToLevel1(pd.getLinkToLevel1());
    	setLinkToLevel2(pd.getLinkToLevel2());
    	setLinkedTo(pd.getLinkedTo());
    	setId(pd.getId());
    	setAmount(pd.getAmount());
    	setLinkToId(pd.getLinkToId());
    	setPaymentLinkDescription(pd.getPaymentLinkDescription());
    	setLinkMileAchieved(pd.getLinkMileAchieved());
    	setPrevHoldBackAmount(pd.getPrevHoldBackAmount());
    }


//  Getter and Setter methods
	public ArrayList getAmount() {
		return amount;
	}



	public void setAmount(ArrayList amount) {
		this.amount = amount;
	}

	public void setAmount(String amount) {
		this.amount.add(amount);
	}

	public ArrayList getCreator() {
		return creator;
	}



	public void setCreator(ArrayList creator) {
		this.creator = creator;
	}

	public void setCreator(String creator) {
		this.creator.add(creator);
	}

	public ArrayList getFkPayment() {
		return fkPayment;
	}



	public void setFkPayment(ArrayList fkPayment) {
		this.fkPayment = fkPayment;
	}

	public void setFkPayment(String fkPayment) {
		this.fkPayment.add(fkPayment);
	}

	public ArrayList getId() {
		return id;
	}



	public void setId(ArrayList id) {
		this.id = id;
	}


	public void setId(String id) {
		this.id.add(id);
	}

	public ArrayList getIPAdd() {
		return iPAdd;
	}



	public void setIPAdd(ArrayList add) {
		iPAdd = add;
	}

	public void setIPAdd(String add) {
		iPAdd.add(add);
	}


	public ArrayList getLastModifiedBy() {
		return lastModifiedBy;
	}



	public void setLastModifiedBy(ArrayList lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy.add(lastModifiedBy);
	}

	public ArrayList getLinkedTo() {
		return linkedTo;
	}



	public void setLinkedTo(ArrayList linkedTo) {
		this.linkedTo = linkedTo;
	}


	public void setLinkedTo(String linkedTo) {
		this.linkedTo.add(linkedTo);
	}

	public ArrayList getLinkToId() {
		return linkToId;
	}



	public void setLinkToId(ArrayList linkToId) {
		this.linkToId = linkToId;
	}

	public void setLinkToId(String linkToId) {
		this.linkToId.add(linkToId);
	}

	public ArrayList getLinkToLevel1() {
		return linkToLevel1;
	}



	public void setLinkToLevel1(ArrayList linkToLevel1) {
		this.linkToLevel1 = linkToLevel1;
	}

	public void setLinkToLevel1(String linkToLevel1) {
		this.linkToLevel1.add(linkToLevel1);
	}

	public ArrayList getLinkToLevel2() {
		return linkToLevel2;
	}



	public void setLinkToLevel2(ArrayList linkToLevel2) {
		this.linkToLevel2 = linkToLevel2;
	}

	public void setLinkToLevel2(String linkToLevel2) {
		this.linkToLevel2.add(linkToLevel2);
	}
    

    // end of getter and setter methods

    /**
     *   if payment details are added, gets payment detail records with previous totals, else 
     *  gets previous payment totals
     * @param payment the paymentPk for which detail records are retrieved. If payment == 0, then just get the totals of previous payments for the same linkToId 
     * @param linkToId the linkToId, main record type for which payment detail records are retrieved
     * @param linkType the type of payment link. possible values: M-Milestones, I-Invoice, B-Budgets        
     */

    public void getPaymentDetailsWithTotals(int payment, int linkToId, String linkType ) {
        int rows = 0;
        StringBuffer sql = new StringBuffer();
        PreparedStatement pstmt = null;
        Connection conn = null;
        boolean paymentExists = false;
        
        String totalAmount = "";
        String mp_LinkToId = "";
        
        String mp_level1_id = "";
        String old_mp_level1_id = "";
        String mp_level2_id = "";
        String mp_linkto_type = "";
        String pk_mp_details = "";
        String mp_amount = "";
        String mileAch = "";
        Float holdBackAppliedDueToDate=null;
        PaymentDetailsDao level1 = new PaymentDetailsDao();
        PaymentDetailsDao level2 = new PaymentDetailsDao();
        Hashtable htLevel2 = new Hashtable();
        double prevHoldBackAmount=0f;
        
        int counter = 0;
        try {
            conn = getConnection();

            if ( payment > 0)
            {	
	           sql.append("SELECT (SELECT SUM(mp_amount) FROM ER_MILEPAYMENT_DETAILS WHERE mp_linkto_type = i.mp_linkto_type ");
	           sql.append(" AND mp_linkto_id = i.mp_linkto_id  AND mp_level1_id = i.mp_level1_id AND " );
	           sql.append(" mp_level2_id =i.mp_level2_id and nvl(fk_mileachieved,0) = nvl(i.fk_mileachieved,0)) total, mp_linkto_id, mp_level1_id ,mp_level2_id,mp_linkto_type,pk_mp_details,mp_amount,fk_mileachieved "); 
	           sql.append(" , (  SELECT SUM(mp_holdback_amount)  FROM ER_MILEPAYMENT_DETAILS  WHERE mp_linkto_type       = i.mp_linkto_type  AND mp_linkto_id           = i.mp_linkto_id ");
	           sql.append(" AND mp_level1_id           = i.mp_level1_id  AND mp_level2_id           =i.mp_level2_id  AND NVL(fk_mileachieved,0) = NVL(i.fk_mileachieved,0)   ) mp_holdback_amount_total , mp_holdback_amount "); 
	           sql.append(" FROM  ER_MILEPAYMENT_DETAILS i WHERE fk_milepayment = ? and mp_linkto_id = ? and mp_linkto_id in ( ");
	           sql.append(" Select p.mp_linkto_id FROM  ER_MILEPAYMENT_DETAILS p Where p.fk_milepayment = ? and p.mp_linkto_id = ? and p.mp_linkto_type = ? ) ");
	           sql.append(" ORDER BY mp_linkto_id, mp_level1_id ,mp_level2_id,fk_mileachieved ");
            }
            else
            {
            	sql.append("SELECT (SELECT SUM(mp_amount) FROM ER_MILEPAYMENT_DETAILS WHERE mp_linkto_type = i.mp_linkto_type ");
 	            sql.append(" AND mp_linkto_id = i.mp_linkto_id  AND mp_level1_id = i.mp_level1_id AND " );
 	            sql.append(" mp_level2_id =i.mp_level2_id and nvl(fk_mileachieved,0) = nvl(i.fk_mileachieved,0)) total,");
 	            sql.append("(SELECT SUM(mp_holdback_amount) FROM ER_MILEPAYMENT_DETAILS WHERE mp_linkto_type = i.mp_linkto_type ");
 	            sql.append(" AND mp_linkto_id = i.mp_linkto_id  AND mp_level1_id = i.mp_level1_id AND " );
 	            sql.append(" mp_level2_id =i.mp_level2_id and nvl(fk_mileachieved,0) = nvl(i.fk_mileachieved,0)) mp_holdback_amount_total,mp_holdback_amount , ");
            	sql.append("  mp_linkto_id   , mp_level1_id ,mp_level2_id,mp_linkto_type,0 pk_mp_details,0 mp_amount ,fk_mileachieved");
            	sql.append("  FROM  ER_MILEPAYMENT_DETAILS i WHERE mp_linkto_id = ?");
            	sql.append(" and mp_linkto_id in ( Select p.mp_linkto_id FROM  ER_MILEPAYMENT_DETAILS p Where p.mp_linkto_id = ? and p.mp_linkto_type = ? ) ");
            	sql.append(" ORDER BY mp_linkto_id,mp_level1_id ,mp_level2_id,fk_mileachieved");
            	
            }

            Rlog.debug("milepayment",
                    "PaymentDetailsDao.getPaymentDetailsWithTotals, sql.toString()" + sql.toString() );
            
           pstmt = conn.prepareStatement(sql.toString());
            
           if ( payment > 0)
           {	
	         
        	   pstmt.setInt(1, payment);
        	   pstmt.setInt(2, linkToId);
        	   
        	   pstmt.setInt(3, payment);
        	   pstmt.setInt(4, linkToId);
        	   
        	   pstmt.setString(5, linkType);
           }
           else
           {
        	   pstmt.setInt(1, linkToId);
        	   
        	   pstmt.setInt(2, linkToId);
        	   pstmt.setString(3, linkType);
        	   
           }
           
           
           
           ResultSet rs = pstmt.executeQuery();

           mp_LinkToId = String.valueOf(linkToId);
           
            while (rs.next()) {
            	
            	counter++;
            	
            	if (counter == 1)
            		{ //set the flag when first row is read
            		paymentExists = true;
            		
            		}
            	
            	totalAmount = rs.getString("total");
                                
                mp_level1_id = rs.getString("mp_level1_id");
                mp_level2_id = rs.getString("mp_level2_id");
                mp_linkto_type = rs.getString("mp_linkto_type");
                pk_mp_details = rs.getString("pk_mp_details");
                mp_amount = rs.getString("mp_amount");
                mileAch =  rs.getString("fk_mileachieved");
                holdBackAppliedDueToDate=rs.getFloat("mp_holdback_amount_total");
                prevHoldBackAmount=rs.getFloat("mp_holdback_amount");
                // the main linked record is set on the object's arraylist, level1 and level2 data is set on other composite objects
                
                if (StringUtil.stringToNum(mp_level2_id) == 0 && StringUtil.stringToNum(mp_level1_id)== 0 ) // its a level 0 record
                {
                	//set the object's arraylists
                	setAppliedTotal(totalAmount);
                	setLinkToLevel1(mp_level1_id);
                	setLinkToLevel2(mp_level2_id);
                	setLinkedTo(mp_linkto_type);
                	setId(pk_mp_details);
                	setAmount(mp_amount);
                	setLinkToId(mp_LinkToId);
                	setLinkMileAchieved(mileAch);
                	setHoldBackAppliedDueToDateList(holdBackAppliedDueToDate);
                	setPrevHoldBackAmount(prevHoldBackAmount);
                	Rlog.debug("milepayment",
                              "PaymentDetailsDao.getPaymentDetailsWithTotals, main record totalAmount" + totalAmount + " pk_mp_details " + pk_mp_details );
                	
                }
                
                if (StringUtil.stringToNum(mp_level2_id) == 0 || StringUtil.stringToNum(mileAch) == 0) // its a level 1 record
                {
                	//set to level1 paymentDetails type object
                	
                	level1.setAppliedTotal(totalAmount);
                	level1.setLinkToLevel1(mp_level1_id);
                	level1.setLinkToLevel2(mp_level2_id);
                	level1.setLinkedTo(mp_linkto_type);
                	level1.setId(pk_mp_details);
                	level1.setAmount(mp_amount);
                	level1.setLinkToId(mp_LinkToId);
                	level1.setLinkMileAchieved(mileAch);
                	level1.setHoldBackAppliedDueToDateList(holdBackAppliedDueToDate);
                	level1.setPrevHoldBackAmount(prevHoldBackAmount);
                	  Rlog.debug("milepayment",
                              "PaymentDetailsDao.getPaymentDetailsWithTotals, level1 totalAmount" + totalAmount + " pk_mp_details " + pk_mp_details );
                }
                
                
                if (StringUtil.isEmpty(old_mp_level1_id) || StringUtil.stringToNum(old_mp_level1_id)== 0 )
            	{
            		old_mp_level1_id = mp_level1_id;
            	}
            	
            	if (!(old_mp_level1_id.equals(mp_level1_id)))
            	{
            		
            		 Rlog.debug("milepayment",
                             "PaymentDetailsDao.getPaymentDetailsWithTotals, Adding old_mp_level1_id" + old_mp_level1_id + "to HT");
            		htLevel2.put(old_mp_level1_id, new PaymentDetailsDao(level2));
            		level2 = new PaymentDetailsDao();
            		
            	}
            	
                if (StringUtil.stringToNum(mp_level2_id) > 0 || StringUtil.stringToNum(mileAch) > 0) // its a level 2 record
                {
                	
                	  Rlog.debug("milepayment",
                              "PaymentDetailsDao.getPaymentDetailsWithTotals, level2 totalAmount" + totalAmount + " pk_mp_details " + pk_mp_details );
                	level2.setAppliedTotal(totalAmount);
                	level2.setLinkToLevel1(mp_level1_id);
                	level2.setLinkToLevel2(mp_level2_id);
                	level2.setLinkedTo(mp_linkto_type);
                	level2.setId(pk_mp_details);
                	level2.setAmount(mp_amount);
                	level2.setLinkToId(mp_LinkToId);
                	level2.setLinkMileAchieved(mileAch);
                	level2.setHoldBackAppliedDueToDateList(holdBackAppliedDueToDate);
                	level2.setPrevHoldBackAmount(prevHoldBackAmount);
                }
                
                old_mp_level1_id = mp_level1_id;
            	
            }
            
            // for last mp_level1_id
            if (! StringUtil.isEmpty(mp_level1_id) && counter > 0)
            {
            	if(!level2.getId().isEmpty())
            		htLevel2.put(mp_level1_id, new PaymentDetailsDao(level2));
            	else
            		htLevel2.put(mp_level1_id, new PaymentDetailsDao(level1));
            	
            }
            //set the level data attributes        
            this.setLevel2Mapping(htLevel2);
            this.setArLevel1(level1);
            setPaymentDetailRowCount(counter);
            
        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "milepayment",
                            "PaymentDetailsDao.getPaymentDetailsWithTotals EXCEPTION IN FETCHING FROM payment details table"
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    
    
    // end of getter and setter methods

    /**
     *   get linked payment browser info
     * @param payment 
     * @param linkType        
     */

    public void getLinkedPaymentBrowser(int payment, String linkType) {
        int rows = 0;
        StringBuffer sql = new StringBuffer();
        PreparedStatement pstmt = null;
        Connection conn = null;
        
        
    
        try {
            conn = getConnection();

            if ( linkType.equals("I"))
            {	
	           sql.append("SELECT pk_mp_details, mp_linkto_id, mp_amount,inv_number linkDesc "); 
	           sql.append(" FROM  ER_MILEPAYMENT_DETAILS i , er_invoice inv WHERE fk_milepayment = ? and mp_linkto_type = 'I' and mp_linkto_id = pk_invoice ");
	           sql.append(" order by lower(inv_number )");
            }
            
            if ( linkType.equals("M"))
            {	
	           sql.append(" SELECT pk_mp_details, mp_linkto_id, mp_amount,  Pkg_Milestone_New.f_getMilestoneDesc( mp_linkto_id) linkDesc"); 
	           sql.append(" FROM  ER_MILEPAYMENT_DETAILS i WHERE fk_milepayment = ? and mp_linkto_type = 'M' AND nvl(mp_level1_id,0) = 0 AND nvl(mp_level2_id,0) = 0 ");
	           sql.append(" order by LOWER(linkDesc)");
            }
            
            if ( linkType.equals("B"))
            {	
	           sql.append("SELECT pk_mp_details, mp_linkto_id, mp_amount,budget_name linkDesc "); 
	           sql.append(" FROM  ER_MILEPAYMENT_DETAILS i , sch_budget inv WHERE fk_milepayment = ? and mp_linkto_type = 'B' and mp_linkto_id = pk_budget ");
	           sql.append(" order by lower(linkDesc )");
            }
            
            Rlog.debug("milepayment",
                    "PaymentDetailsDao.getPaymentDetailsWithTotals, sql.toString()" + sql.toString() );
            
           pstmt = conn.prepareStatement(sql.toString());
            
           if ( payment > 0)
           {	
	          pstmt.setInt(1, payment);
        	  
           }
           
           
           
           ResultSet rs = pstmt.executeQuery();
           
           
            while (rs.next()) {
            	setId(rs.getString("pk_mp_details"));
            	setLinkToId(rs.getString("mp_linkto_id"));
                setAmount(rs.getString("mp_amount"));
                setPaymentLinkDescription(rs.getString("linkDesc"));
               
            }
                        
            
        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "milepayment",
                            "PaymentDetailsDao.getLinkedPaymentBrowser EXCEPTION IN FETCHING FROM payment details table"
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }


	public ArrayList getPaymentLinkDescription() {
		return paymentLinkDescription;
	}


	public void setPaymentLinkDescription(ArrayList paymentLinkDescription) {
		this.paymentLinkDescription = paymentLinkDescription;
	}
	
	public void setPaymentLinkDescription(String paymentLinkDescription) {
		this.paymentLinkDescription.add(paymentLinkDescription);
	}


	public ArrayList getLinkMileAchieved() {
		return linkMileAchieved;
	}


	public void setLinkMileAchieved(ArrayList linkMileAchieved) {
		this.linkMileAchieved = linkMileAchieved;
	}
   
	public void setLinkMileAchieved(String lma) {
		this.linkMileAchieved.add(lma);
	}


	public ArrayList<Float> getHoldBackAppliedDueToDateList() {
		return holdBackAppliedDueToDateList;
	}


	public void setHoldBackAppliedDueToDateList(
			Float holdBackAppliedDueToDate) {
		this.holdBackAppliedDueToDateList.add(holdBackAppliedDueToDate); 
	}


	public ArrayList<Boolean> getHoldBackFlag() {
		return holdBackFlag;
	}


	public void setHoldBackFlag(String holdBackFlag) {
		Boolean holdback=false;
		if(holdBackFlag!=null && !holdBackFlag.equals(""))
		 holdback=Boolean.parseBoolean(holdBackFlag);
		this.holdBackFlag.add(holdback);
	}


	public ArrayList<Double> getPrevHoldBackAmount() {
		return prevHoldBackAmount;
	}


	public void setPrevHoldBackAmount(double prevHoldBackAmount) {
		this.prevHoldBackAmount.add(prevHoldBackAmount);
	}


	public void setPrevHoldBackAmount(ArrayList<Double> prevHoldBackAmount) {
		this.prevHoldBackAmount = prevHoldBackAmount;
	}
	
	
	
}
