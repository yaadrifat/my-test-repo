/*
 * Classname			StudyVerBean.class
 * 
 * Version information
 *
 * Date					11/25/2002 
 * 
 * Copyright notice
 */

package com.velos.eres.business.studyVer.impl;

/**
 * @ejbHomeJNDIname ejb.StudyVer
 */

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The StudyVer BMP entity bean.<br>
 * <br>
 * 
 * @author Arvind
 */
//@NamedQuery(name = "findStudyVerNum", queryString = "SELECT OBJECT(ver) FROM StudyVerBean ver where FK_STUDY = :FK_STUDY  AND UPPER(trim(STUDYVER_NUMBER)) = UPPER(trim(:STUDYVER_NUMBER))")
//JM: 051906 Changed the Named Query
@Entity
@Table(name = "er_studyver")
@NamedQueries( {
@NamedQuery(name = "findStudyVerNum", query = "SELECT OBJECT(ver) FROM StudyVerBean ver where FK_STUDY = :FK_STUDY  AND UPPER(trim(STUDYVER_NUMBER)) = UPPER(trim(:studyVerNumber)) " +
        " AND STUDYVER_CATEGORY = :studyVerCategory ") ,
@NamedQuery(name = "getDetailedStudyVersion", query = "SELECT OBJECT(ver) FROM StudyVerBean ver where FK_STUDY = :FK_STUDY  AND UPPER(trim(STUDYVER_NUMBER)) = :studyVerNumber) " )  
})
public class StudyVerBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3688789176258605106L;

    /**
     * the studyVer Id
     */
    public int studyVerId;

    /**
     * the studyVer Study Id
     */
    public Integer studyVerStudyId;

    /**
     * the studyVer Number
     */
    public String studyVerNumber;

    /**
     * the studyVer Status
     */
    public String studyVerStatus;

    /**
     * the studyVer Notes
     */
    public String studyVerNotes;

    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;
    
    
    
    
    
    /*
     * the Version Date
     */
    //public String versionDate;//JM
    public Date versionDate;//JM
    
    /*
     * Vesion Category
     */
    private String studyVercat;//JM
    
    /*
     * Vesion Type
     */
    private String studyVertype;//JM

    

    // GETTER SETTER METHODS

    public StudyVerBean() {

    }

    public StudyVerBean(int studyVerId, String studyVerStudyId,
            String studyVerNumber, String studyVerStatus, String studyVerNotes,
            String creator, String modifiedBy, String ipAdd, String versionDate,String studyVercat, String studyVertype) {
        super();
        // TODO Auto-generated constructor stub
        setStudyVerId(studyVerId);
        setStudyVerStudyId(studyVerStudyId);
        setStudyVerNumber(studyVerNumber);
        setStudyVerStatus(studyVerStatus);
        setStudyVerNotes(studyVerNotes);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        updateVersionDate(versionDate);//JM
        setStudyVercat(studyVercat);
        setStudyVertype(studyVertype);
    }

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_STUDYVER", allocationSize=1)
    @Column(name = "PK_STUDYVER")
    public int getStudyVerId() {
        return this.studyVerId;
    }

    public void setStudyVerId(int id) {
        this.studyVerId = id;
    }

    @Column(name = "FK_STUDY")
    public String getStudyVerStudyId() {
        return StringUtil.integerToString(this.studyVerStudyId);
    }

    public void setStudyVerStudyId(String studyVerStudyId) {
        if (studyVerStudyId != null) {
            this.studyVerStudyId = Integer.valueOf(studyVerStudyId);
        }
    }

    @Column(name = "STUDYVER_NUMBER")
    public String getStudyVerNumber() {
        return this.studyVerNumber;
    }

    public void setStudyVerNumber(String studyVerNumber) {
        this.studyVerNumber = studyVerNumber;
    }

    @Column(name = "STUDYVER_STATUS")
    public String getStudyVerStatus() {
        return this.studyVerStatus;
    }

    public void setStudyVerStatus(String studyVerStatus) {
        this.studyVerStatus = studyVerStatus;
    }

    @Column(name = "STUDYVER_NOTES")
    public String getStudyVerNotes() {
        return this.studyVerNotes;
    }

    public void setStudyVerNotes(String studyVerNotes) {
        this.studyVerNotes = studyVerNotes;
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }
    
    /**
     * @return Study Version Category
     */
    @Column(name = "STUDYVER_CATEGORY")
    public String getStudyVercat(){
    	return this.studyVercat;
    }
    
    /**
     * @param studyVercat
     *            The Study Version Category
     */
    public void setStudyVercat(String studyVercat){
    	this.studyVercat = studyVercat;
    }
    /**
     * @return Study Version Type
     */

    @Column(name = "STUDYVER_TYPE")
    public String getStudyVertype(){
    	return this.studyVertype;
    }

    /**
     * @param studyVertype
     *            The Study Version Type
     */
    public void setStudyVertype(String studyVertype){
    	this.studyVertype = studyVertype;
    }
    
//JM: 11/18/2005
    /**
     * @return Study Version Date
     */

	//  @Transient
    @Column(name = "STUDYVER_DATE")
    public Date getVersionDate() {
        // return DateUtil.dateToString(getStatStartDt());
        return this.versionDate;

    }
    /**
     * @param versionDate
     *            The Study Version Date
     */
    
    public void setVersionDate(Date versionDate) {

         
        this.versionDate = versionDate;

    }
    
    /**
     * @return Study Version Date in String Format
     */
    public String returnVersionDate() {
        // return DateUtil.dateToString(getStatStartDt());
        return DateUtil.dateToString(this.versionDate);

    }
    /**
     * @param versionDate
     *           
     */
    
    public void updateVersionDate(String versionDate) {

 
        this.versionDate = DateUtil.stringToDate(versionDate, null);

    }

    
    
    
    // END OF GETTER SETTER METHODS

   
    

    

    public int updateStudyVer(StudyVerBean srsk) {
        try {
            setStudyVerStudyId(srsk.getStudyVerStudyId());
            setStudyVerNumber(srsk.getStudyVerNumber());
            setStudyVerStatus(srsk.getStudyVerStatus());
            setStudyVerNotes(srsk.getStudyVerNotes());           
            updateVersionDate(srsk.returnVersionDate());//JM
            setStudyVercat(srsk.getStudyVercat());//JM
            setStudyVertype(srsk.getStudyVertype());//JM
            setCreator(srsk.getCreator());
            setModifiedBy(srsk.getModifiedBy());
            Rlog.debug("studyVer", "StudyVerBean.updateStudyVer");
            return 0;
        } catch (Exception e) {
            Rlog.fatal("studyVer", " error in StudyVerBean.updateStudyVer");
            return -2;
        }
    }

}// end of class
