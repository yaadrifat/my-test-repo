/*
 * Classname			SavedRepBean.class
 * 
 * Version information
 *
 * Date					05/30/2001 
 * 
 * Copyright notice
 */

package com.velos.eres.business.savedRep.impl;

/**
 * @ejbHomeJNDIname ejb.SavedRep
 */

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The SavedRep BMP entity bean.<br>
 * <br>
 * 
 * @author Arvind
 */
@Entity
@Table(name = "er_savedrep")
public class SavedRepBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3257853181542150448L;

    /**
     * the savedRep Id
     */
    public int savedRepId;

    /**
     * the savedRep Study Id
     */
    public Integer savedRepStudyId;

    /**
     * the savedRep Report Id
     */
    public Integer savedRepReportId;

    /**
     * the savedRep Report Name
     */
    public String savedRepRepName;

    /**
     * the savedRep As Of Date
     */
    public java.util.Date savedRepAsOfDate;

    /**
     * the savedRep User Id
     */
    public Integer savedRepUserId;

    /**
     * the savedRep Patient Id
     */
    public Integer savedRepPatId;

    /**
     * the savedRep Filter
     */
    public String savedRepFilter;

    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_SAVEDREP", allocationSize=1)
    @Column(name = "PK_SAVEDREP")
    public int getSavedRepId() {
        return this.savedRepId;
    }

    public void setSavedRepId(int repId) {
        this.savedRepId = repId;
    }

    @Column(name = "FK_STUDY")
    public String getSavedRepStudyId() {
        return StringUtil.integerToString(this.savedRepStudyId);
    }

    public void setSavedRepStudyId(String savedRepStudyId) {
        if (savedRepStudyId != null) {
            this.savedRepStudyId = Integer.valueOf(savedRepStudyId);
        }
    }

    @Column(name = "FK_REPORT")
    public String getSavedRepReportId() {
        return StringUtil.integerToString(this.savedRepReportId);
    }

    public void setSavedRepReportId(String savedRepReportId) {
        if (savedRepReportId != null) {
            this.savedRepReportId = Integer.valueOf(savedRepReportId);
        }
    }

    @Column(name = "SAVEDREP_NAME")
    public String getSavedRepRepName() {
        return this.savedRepRepName;
    }

    public void setSavedRepRepName(String savedRepRepName) {
        this.savedRepRepName = savedRepRepName;
    }

    @Column(name = "SAVEDREP_ASOFDATE")
    public Date getSavedRepAsOfDt() {
        return this.savedRepAsOfDate;
    }

    public void setSavedRepAsOfDt(Date savedRepAsOfDate) {

        this.savedRepAsOfDate = savedRepAsOfDate;

    }

    @Transient
    public String getSavedRepAsOfDate() {
        return DateUtil.dateToString(getSavedRepAsOfDt());
    }

    public void setSavedRepAsOfDate(String savedRepAsOfDate) {

        setSavedRepAsOfDt(DateUtil.stringToDate(savedRepAsOfDate, null));
    }

    @Column(name = "FK_USER")
    public String getSavedRepUserId() {
        return StringUtil.integerToString(this.savedRepUserId);
    }

    public void setSavedRepUserId(String savedRepUserId) {
        if (savedRepUserId != null) {
            this.savedRepUserId = Integer.valueOf(savedRepUserId);
        }
    }

    @Column(name = "FK_PAT")
    public String getSavedRepPatId() {
        return StringUtil.integerToString(this.savedRepPatId);
    }

    public void setSavedRepPatId(String savedRepPatId) {
        if (savedRepPatId != null) {
            this.savedRepPatId = Integer.valueOf(savedRepPatId);
        }
    }
    
    @Column(name = "SAVEDREP_FILTER")
    public String getSavedRepFilter() {
        return this.savedRepFilter;
    }

    public void setSavedRepFilter(String savedRepFilter) {
        this.savedRepFilter = savedRepFilter;
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    /**
     * returns the state keeper object associated with this savedRep
     */
    /*
     * public SavedRepStateKeeper getSavedRepStateKeeper() {
     * Rlog.debug("savedRep", "SavedRepBean.getSavedRepStateKeeper"); return
     * (new SavedRepStateKeeper(getSavedRepId(), getSavedRepStudyId(),
     * getSavedRepReportId(), getSavedRepRepName(), getSavedRepAsOfDate(),
     * getSavedRepUserId(), getSavedRepPatId(), getSavedRepFilter(),
     * getCreator(), getModifiedBy(), getIpAdd())); }
     */

    /**
     * sets up a state keeper containing details of the savedRep
     */
    /*
     * public void setSavedRepStateKeeper(SavedRepStateKeeper srsk) { GenerateId
     * savedRepId = null; try { Connection conn = null; conn = getConnection();
     * Rlog.debug("savedRep", "StudyNotifyBean.setStudyNotifyStateKeeper()
     * Connection :" + conn);
     * 
     * this.savedRepId = savedRepId.getId("SEQ_ER_SAVEDREP", conn);
     * Rlog.debug("savedRep", "SavedRepBean.setSavedRepStateKeeper() addId :" +
     * savedRepId); conn.close();
     * 
     * setSavedRepStudyId(srsk.getSavedRepStudyId()); Rlog.debug("savedRep",
     * "SavedRepBean.setSavedRepStateKeeper() savedRepStudyId :" +
     * savedRepStudyId); setSavedRepReportId(srsk.getSavedRepReportId());
     * Rlog.debug("savedRep", "SavedRepBean.setSavedRepStateKeeper()
     * savedRepReportId :" + savedRepReportId);
     * setSavedRepRepName(srsk.getSavedRepRepName()); Rlog.debug("savedRep",
     * "SavedRepBean.setSavedRepStateKeeper() savedRepRepName :" +
     * savedRepRepName); setSavedRepAsOfDate(srsk.getSavedRepAsOfDate());
     * Rlog.debug("savedRep", "SavedRepBean.setSavedRepStateKeeper()
     * savedRepAsOfDate :" + savedRepAsOfDate);
     * setSavedRepUserId(srsk.getSavedRepUserId()); Rlog.debug("savedRep",
     * "SavedRepBean.setSavedRepStateKeeper() savedRepUserId :" +
     * savedRepUserId); setSavedRepPatId(srsk.getSavedRepPatId());
     * Rlog.debug("savedRep", "SavedRepBean.setSavedRepStateKeeper()
     * savedRepPatId :" + savedRepPatId);
     * setSavedRepFilter(srsk.getSavedRepFilter()); Rlog.debug("savedRep",
     * "SavedRepBean.setSavedRepStateKeeper() savedRepFilter :" +
     * savedRepFilter);
     * 
     * setCreator(srsk.getCreator()); Rlog .debug("savedRep",
     * "SavedRepBean.setSavedRepStateKeeper() creator :" + creator);
     * setModifiedBy(srsk.getModifiedBy()); Rlog.debug("savedRep",
     * "SavedRepBean.setSavedRepStateKeeper() modifiedBy :" + modifiedBy);
     * setIpAdd(srsk.getIpAdd()); Rlog.debug("savedRep",
     * "SavedRepBean.setSavedRepStateKeeper() ipAdd :" + ipAdd); } catch
     * (Exception e) { Rlog.fatal("savedRep", "Error in setSavedRepStateKeeper()
     * in SavedRepBean " + e); } }
     */

    public int updateSavedRep(SavedRepBean srsk) {
        try {
            setSavedRepStudyId(srsk.getSavedRepStudyId());
            setSavedRepReportId(srsk.getSavedRepReportId());
            setSavedRepRepName(srsk.getSavedRepRepName());
            setSavedRepAsOfDate(srsk.getSavedRepAsOfDate());
            setSavedRepUserId(srsk.getSavedRepUserId());
            setSavedRepPatId(srsk.getSavedRepPatId());
            setSavedRepFilter(srsk.getSavedRepFilter());
            
            setCreator(srsk.getCreator());
            Rlog.debug("savedRep", "SavedRepBean.updateSavedRep");
            return 0;
        } catch (Exception e) {
            Rlog.fatal("savedRep", " error in SavedRepBean.updateSavedRep");
            return -2;
        }
    }

    public SavedRepBean() {

    }

    public SavedRepBean(int savedRepId, String savedRepStudyId,
            String savedRepReportId, String savedRepRepName,
            String savedRepAsOfDate, String savedRepUserId,
            String savedRepPatId, String savedRepFilter, String creator,
            String modifiedBy, String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setSavedRepId(savedRepId);
        setSavedRepStudyId(savedRepStudyId);
        setSavedRepReportId(savedRepReportId);
        setSavedRepRepName(savedRepRepName);
        setSavedRepAsOfDate(savedRepAsOfDate);
        setSavedRepUserId(savedRepUserId);
        setSavedRepPatId(savedRepPatId);
        setSavedRepFilter(savedRepFilter);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

}// end of class
