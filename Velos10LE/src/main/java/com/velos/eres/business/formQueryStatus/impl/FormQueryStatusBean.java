/**
 * Classname : FormQueryStatusBean
 *
 * Version information: 1.0
 *
 * Date: 01/03/2005
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.business.formQueryStatus.impl;

/* Import Statements */

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The Form query status CMP entity bean.<br>
 * <br>
 *
 * @author Anu
 * @vesion 1.0 08/09/2001
 * @ejbHome FormQueryStatusHome
 * @ejbRemote FormQueryStatusRObj
 */

@Entity
@Table(name = "er_formquerystatus")
public class FormQueryStatusBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3832620690233766963L;

    /**
     * the form query Id
     */
    public int formQueryStatusId;

    /**
     * the from query id
     */
    public Integer formQueryId;

    /**
     * the form query type
     */

    public Integer formQueryType;

    /**
     * the query type id
     */

    public Integer queryTypeId;

    /**
     * the query notes
     */

    public String queryNotes;

    /**
     * Entered by
     */
    public Integer enteredBy;

    /**
     * the Entered On
     */
    public java.util.Date enteredOn;

    /**
     * the query status id
     */

    public Integer queryStatusId;

    /**
     * creator
     */
    public Integer creator;

    /**
     * last modified by
     */
    public Integer modifiedBy;

    /**
     * IP Address
     */
    public String ipAdd;

    // GETTER SETTER METHODS

    /**
     *
     * Get form Query Status Id
     *
     * @return the formQueryStatusId
     */
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_FORM_QUERY_STATUS", allocationSize=1)
    @Column(name = "PK_FORMQUERYSTATUS")
    public int getFormQueryStatusId() {
        return this.formQueryStatusId;
    }

    public void setFormQueryStatusId(int id) {
        this.formQueryStatusId = id;
    }

    /**
     * Get form Query Id
     *
     * @return formQueryId
     */
    @Column(name = "FK_FORMQUERY")
    public String getFormQueryId() {
        return StringUtil.integerToString(this.formQueryId);
    }

    /**
     * sets form Query Id
     *
     * @param formQueryId
     */
    public void setFormQueryId(String formQueryId) {
        this.formQueryId = StringUtil.stringToInteger(formQueryId);
    }

    /**
     * Get form Query Type
     *
     * @return formQueryType
     */
    @Column(name = "FORMQUERY_TYPE")
    public String getFormQueryType() {
        return StringUtil.integerToString(this.formQueryType);
    }

    /**
     * sets form Query Type
     *
     * @param formQueryType
     */
    public void setFormQueryType(String formQueryType) {
        this.formQueryType = StringUtil.stringToInteger(formQueryType);
    }

    /**
     * Get query Type Id
     *
     * @return queryTypeId
     */
    @Column(name = "FK_CODELST_QUERYTYPE")
    public String getQueryTypeId() {
        return StringUtil.integerToString(this.queryTypeId);
    }

    /**
     * sets query Type Id
     *
     * @param queryTypeId
     */
    public void setQueryTypeId(String queryTypeId) {
        this.queryTypeId = StringUtil.stringToInteger(queryTypeId);
    }

    /**
     * Get query Notes
     *
     * @return String queryNotes
     */
    @Column(name = "QUERY_NOTES")
    public String getQueryNotes() {
        return this.queryNotes;
    }

    /**
     * sets query Notes
     *
     * @param queryNotes
     */
    public void setQueryNotes(String queryNotes) {
        this.queryNotes = queryNotes;
    }

    /**
     * sets form Query entered by
     *
     * @param enteredBy
     */
    public void setEnteredBy(String enteredBy) {
        this.enteredBy = StringUtil.stringToInteger(enteredBy);
    }

    /**
     * Get form Query enteredBy
     *
     * @return enteredBy
     */
    @Column(name = "ENTERED_BY")
    public String getEnteredBy() {
        return StringUtil.integerToString(this.enteredBy);
    }

    /**
     * Get query status entered on
     *
     * @return String enteredOn
     */
    @Transient
    public String getEnteredOn() {
        return DateUtil.dateToString(getEnteredOnPersistent(), null);

    }

    /**
     * sets enteredOn
     *
     * @param enteredOn
     */

    public void setEnteredOn(String enteredOn) {
    	setEnteredOnPersistent(DateUtil.stringToTimeStamp(enteredOn, null));
    }
    // Database Trigger would enter SYSDATE to maintain consistency across application 
    //@Column(name = "ENTERED_ON")
    @Transient
    public Date getEnteredOnPersistent() {
        return this.enteredOn;

    }

    public void setEnteredOnPersistent(Date enteredOn) {
        this.enteredOn = enteredOn;

    }

    /**
     * Get query status Id
     *
     * @return queryStatusId
     */
    @Column(name = "FK_CODELST_QUERYSTATUS")
    public String getQueryStatusId() {
        return StringUtil.integerToString(this.queryStatusId);
    }

    /**
     * sets query status Id
     *
     * @param queryStatusId
     */
    public void setQueryStatusId(String queryStatusId) {
        this.queryStatusId = StringUtil.stringToInteger(queryStatusId);
    }

    /**
     * Get Status creator
     *
     * @return The Id of the creator of the record
     */
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {

        this.creator = StringUtil.stringToInteger(creator);
    }

    /**
     * Get Status modified by
     *
     * @return The Id of the user modifying this record
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {

        this.modifiedBy = StringUtil.stringToInteger(modifiedBy);
    }

    /**
     * @return IP Address
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    /**
     * @return the state holder object associated with this form query status
     */

    /*
     * public FormQueryStatusStateKeeper getFormQueryStatusStateKeeper() {
     *
     * return new FormQueryStatusStateKeeper(getFormQueryStatusId(),
     * getFormQueryId(), getFormQueryType(), getQueryTypeId(), getQueryNotes(),
     * getEnteredBy(), getEnteredOn(), getQueryStatusId(), getCreator(),
     * getModifiedBy(), getIpAdd()); }
     */

    /**
     * sets up a state keeper containing details of the form query
     *
     * @param fqsk
     *            FormQueryStatusStateKeeper
     */

    /*
     * public void setFormQueryStatusStateKeeper(FormQueryStatusStateKeeper
     * fqsk) { GenerateId formQueryStatusId = null;
     *
     * try { Connection conn = null; conn = getConnection();
     *
     * this.formQueryStatusId = formQueryStatusId.getId(
     * "SEQ_ER_FORM_QUERY_STATUS", conn); conn.close();
     * setFormQueryId(fqsk.getFormQueryId());
     * setFormQueryType(fqsk.getFormQueryType());
     * setQueryTypeId(fqsk.getQueryTypeId());
     * setQueryNotes(fqsk.getQueryNotes()); setEnteredBy(fqsk.getEnteredBy());
     * setEnteredOn(fqsk.getEnteredOn());
     * setQueryStatusId(fqsk.getQueryStatusId()); setCreator(fqsk.getCreator());
     * setModifiedBy(fqsk.getModifiedBy()); setIpAdd(fqsk.getIpAdd()); } catch
     * (Exception e) { Rlog.fatal("formQueryStatus", "Exception in
     * FormQueryStatusBean.setFormQueryStatusStateKeeper" + e); } }
     */

    /**
     * updates the from query status details
     *
     * @param fqk
     *            FormQueryStatusStateKeeper
     * @return 0 if successful; -2 for exception
     */
    public int updateFormQueryStatus(FormQueryStatusBean fqsk) {
        try {

            setFormQueryId(fqsk.getFormQueryId());
            setFormQueryType(fqsk.getFormQueryType());
            setQueryTypeId(fqsk.getQueryTypeId());
            setQueryNotes(fqsk.getQueryNotes());
            setEnteredBy(fqsk.getEnteredBy());
            setEnteredOn(fqsk.getEnteredOn());
            setQueryStatusId(fqsk.getQueryStatusId());
            setCreator(fqsk.getCreator());
            setModifiedBy(fqsk.getModifiedBy());
            setIpAdd(fqsk.getIpAdd());

            return 0;
        } catch (Exception e) {
            Rlog.fatal("fromQueryStatus",
                    "Exception in  FormQueryStatusBean.updateFormQueryStatus"
                            + e);
            return -2;
        }
    }

    public FormQueryStatusBean() {

    }

    public FormQueryStatusBean(int formQueryStatusId, String formQueryId,
            String formQueryType, String queryTypeId, String queryNotes,
            String enteredBy, String enteredOn, String queryStatusId,
            String creator, String modifiedBy, String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setFormQueryStatusId(formQueryStatusId);
        setFormQueryId(formQueryId);
        setFormQueryType(formQueryType);
        setQueryTypeId(queryTypeId);
        setQueryNotes(queryNotes);
        setEnteredBy(enteredBy);
        setEnteredOn(enteredOn);
        setQueryStatusId(queryStatusId);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

}// end of class
