/*
 * Classname         StudyGroupRights.class
 * 
 * Version information
 *
 * Date                  08/22/2005
 * 
 * Copyright notice
 */

package com.velos.eres.business.studyRights.impl;

/**
 * @ejbHomeJNDIname ejb.Rights
 */

import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The Study Rights
 * 
 * @author sonia sahni
 */
@Entity
@Table(name = "er_grps")
public class StudyGroupRights implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3256999951945709107L;

    /**
     * the primary key of the Group Rights
     */
    public int id;

    /**
     * Rights
     */
    public String teamRights;

    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;

    /**
     * Array List of Feature Sequence
     */
    private ArrayList grSeq;

    /**
     * the Array List of Feature Rights
     */
    private ArrayList ftrRights;

    /**
     * the Array List containing reference name of the feature
     */
    private ArrayList grValue;

    /**
     * the Array List containing user friendly name of the feature
     */
    private ArrayList grDesc;

    // GETTER SETTER METHODS

    /**
     * 
     * 
     * @return
     */
    @Column(name = "PK_GRP")
    @Id
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Get Group Rights
     * 
     * @return
     */
    @Column(name = "GRP_SUPUSR_RIGHTS")
    public String getTeamRights() {
        return this.teamRights;
    }

    /**
     * Set Group Rights
     * 
     * @param user
     */
    public void setTeamRights(String rights) {
        if (rights != null) {
            this.teamRights = rights;
        }

    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * 
     * 
     * @return
     */
    @Transient
    public ArrayList getGrSeq() {
        return this.grSeq;
    }

    /**
     * 
     * 
     * @param seq
     */
    public void setGrSeq(String seq) {
        grSeq.add(seq);
    }

    /**
     * 
     * 
     * @param seq
     */
    public void setGrSeq(ArrayList seq) {
        this.grSeq = seq;
    }

    /**
     * 
     * 
     * @return
     */
    @Transient
    public ArrayList getFtrRights() {
        return this.ftrRights;
    }

    /**
     * Sets the Rights
     * 
     * @param rights
     */
    public void setFtrRights(String rights) {
        Rlog
                .debug("studyrights", "Setting the Rights in State Keeper"
                        + rights);
        try {
            this.ftrRights.add(rights);
        } catch (Exception e) {
            Rlog.fatal("studyrights", "EXCEPTION IN String Value Set"
                    + this.ftrRights + "*" + e);
        }
        Rlog.debug("studyrights", "String Value Set" + this.ftrRights);
    }

    /**
     * 
     * 
     * @param rights
     */

    public void setFtrRights(ArrayList rights) {
        Rlog.debug("studyrights",
                "Setting the Rights in State Keeper (Array List)" + rights);
        this.ftrRights = rights;
        Rlog.debug("studyrights", "Array List Value Set" + this.ftrRights);
    }

    /**
     * 
     * 
     * @return
     */
    @Transient
    public ArrayList getGrValue() {
        return this.grValue;
    }

    /**
     * 
     * 
     * @param val
     */
    public void setGrValue(String val) {
        grValue.add(val);
    }

    /**
     * 
     * 
     * @param val
     */
    public void setGrValue(ArrayList val) {
        this.grValue = val;
    }

    /**
     * 
     * 
     * @return
     */
    @Transient
    public ArrayList getGrDesc() {
        return this.grDesc;
    }

    /**
     * 
     * 
     * @param desc
     */
    public void setGrDesc(String desc) {
        grDesc.add(desc);
    }

    /**
     * 
     * 
     * @param desc
     */
    public void setGrDesc(ArrayList desc) {
        this.grDesc = desc;
    }

    // END OF GETTER SETTER METHODS

    /**
     * returns the state holder object associated with this study rights
     * 
     * @returns StudyRightsStateKeeper
     */
    /*
     * public StudyRightsStateKeeper getStudyRightsStateKeeper() {
     * Rlog.debug("studyrights", "GET STUDY RIGHTS KEEPER");
     * StudyRightsStateKeeper srk = new StudyRightsStateKeeper();
     * srk.setId(getId()); return srk; }
     */

    /**
     * Updates the Study Rights for a user in the Database
     * 
     * @param srk
     */
    /*
     * public void setStudyRightsStateKeeper(StudyRightsStateKeeper srk) {
     * 
     * Rlog.debug("studyrights", "IN Entity Bean - StudyRights STATE HOLDER");
     * GenerateId genId = null;
     * 
     * try { Connection conn = null; conn = getConnection();
     * 
     * Rlog.debug("studyrights", "Connection :" + conn); id =
     * genId.getId("SEQ_ER_STUDYTEAM_RIGHTS", conn); conn.close();
     * 
     * setStudyTeamId(srk.getStudyTeamId()); // All other attributes are // set
     * in Session Bean setCreator(srk.getCreator());
     * setModifiedBy(srk.getModifiedBy()); setIpAdd(srk.getIpAdd());
     * 
     * Rlog.debug("studyrights", "GOT Study Rights ID" + id); } catch (Exception
     * e) { Rlog.fatal("studyrights", "EXCEPTION IN ASSOCIATING RIGHTS TO STUDY" +
     * e); } }
     */

    /**
     * Updates the Study Rights in the Database
     * 
     * @param ssk
     */
    public int updateStudyRights(StudyRightsBean srk, String rights) {
        Rlog.debug("studyrights", "IN study Rights STATE HOLDER");
        try {
            // setGrpId(Integer.toString(gsk.getId())); //All other attributes
            // are set in Session Bean
            setTeamRights(rights);
            setCreator(srk.getCreator());
            setModifiedBy(srk.getModifiedBy());
            setIpAdd(srk.getIpAdd());

            Rlog.debug("studyrights", "UPDATED");
            return 0;
        } catch (Exception e) {
            Rlog
                    .fatal("studyrights", "EXCEPTION IN UPDATING STUDY RIGHTS "
                            + e);
            return -2;
        }
    }

    public StudyGroupRights() {
        grSeq = new ArrayList();
        ftrRights = new ArrayList();
        grValue = new ArrayList();
        grDesc = new ArrayList();
    }

    public StudyGroupRights(int id, ArrayList grSeqList,
            ArrayList ftrRightList, ArrayList grValueList,
            ArrayList grDescList, String creator, String modifiedBy,
            String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        grSeq = new ArrayList();
        ftrRights = new ArrayList();
        grValue = new ArrayList();
        grDesc = new ArrayList();
        setId(id);
        setTeamRights(teamRights);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        this.setGrSeq(grSeqList);
        this.setFtrRights(ftrRightList);
        this.setGrValue(grValueList);
        this.setGrDesc(grDescList);
    }

}// end of class

