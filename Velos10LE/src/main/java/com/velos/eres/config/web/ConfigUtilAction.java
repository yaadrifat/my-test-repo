package com.velos.eres.config.web;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.velos.eres.compliance.web.ComplianceJB;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.study.StudyJB;

public class ConfigUtilAction extends ActionSupport  {
	private static final long serialVersionUID = 1L;
	private HttpServletRequest request = null;
	private HttpServletResponse response = null;
	private static final String CONFIG_STUDY_SCREEN_TILE = "configStudyScreenTile";
	private static final String FLEX_STUDY_SCREEN_TILE = "flexStudyScreenTile";	
	private static final String FLEX_STUDY_SCREEN_LIND_FT_TILE = "flexStudyScreenLindFtTile";	
	private static final String STUDY_CHECK_N_SUBMIT_TILE = "studyCheckNSubmitTile";
	private static final String FLEX_PROTOCOL_CHANGE_LOG = "flexProtocolChangeLogTitle";
	private static final String STUDY_ID = "studyId";
	private StudyJB studyJB = null;
	private ComplianceJB complianceJB = null;
	private Integer userId = null;
	private Integer accountId = null;
	private String isNewAmendment="false";

	public String getIsNewAmendment() {
		return isNewAmendment;
	}

	public void setIsNewAmendment(String isNewAmendment) {
		this.isNewAmendment = isNewAmendment;
	}

	public ComplianceJB getComplianceJB() {
		return complianceJB;
	}

	public void setComplianceJB(ComplianceJB complianceJB) {
		this.complianceJB = complianceJB;
	}

	public ConfigUtilAction() {
		ActionContext ac = ActionContext.getContext();
		request = (HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
		response = (HttpServletResponse)ac.get(ServletActionContext.HTTP_RESPONSE);
		if (!StringUtil.isEmpty(request.getParameter(STUDY_ID))) {
			int iStudyId = StringUtil.stringToNum(request.getParameter(STUDY_ID).trim());
			if (iStudyId > 0) {
				studyJB = new StudyJB();
				studyJB.setId(iStudyId);
				studyJB.getStudyDetails();
				try {
					request.getSession(false).setAttribute("studyJB", studyJB);
				} catch(Exception e) {}
			}
		}
		complianceJB = new ComplianceJB();
		userId = StringUtil.stringToInteger((String) request.getSession(false).getAttribute("userId"));
		accountId = StringUtil.stringToInteger((String) request.getSession(false).getAttribute("accountId"));
		isNewAmendment =(String)request.getAttribute("isNewAmendment");	
		if(isNewAmendment == null ) {
			isNewAmendment="false";
		}
		setIsNewAmendment(isNewAmendment);
		
	}
	
	public String getConfigStudyScreen() {
		return CONFIG_STUDY_SCREEN_TILE;
	}

	public String getFlexStudyScreen() {
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userId", userId);
		paramMap.put("accountId", accountId);
		int pkStudy = StringUtil.stringToNum((String)request.getParameter("studyId"));
		paramMap.put("studyId", pkStudy);
		complianceJB.retrieveLockStudyFlag(paramMap);
		if("true".equals(this.getIsNewAmendment()) ){
			int newStudyStatusId=0;			
			newStudyStatusId = complianceJB.addStudyStatus(paramMap);			
		}		
		return FLEX_STUDY_SCREEN_TILE;
	}
	//adding this to support the mapping for a new view of the protocol study management page for lind FT 
	public String getFlexStudyScreenLindFt() {
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userId", userId);
		paramMap.put("accountId", accountId);
		int pkStudy = StringUtil.stringToNum((String)request.getParameter("studyId"));
		paramMap.put("studyId", pkStudy);
		complianceJB.retrieveLockStudyFlag(paramMap);
		if("true".equals(this.getIsNewAmendment()) ){
			int newStudyStatusId=0;			
			newStudyStatusId = complianceJB.addStudyStatus(paramMap);			
		}		
		return FLEX_STUDY_SCREEN_LIND_FT_TILE;
	}
	public String getFlexProtocolChangeLog() {
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userId", userId);
		paramMap.put("accountId", accountId);
		int pkStudy = StringUtil.stringToNum((String)request.getParameter("studyId"));
		paramMap.put("studyId", pkStudy);
		complianceJB.retrieveLockStudyFlag(paramMap);
		if("true".equals(isNewAmendment) ){
			int newStudyStatusId=0;			
			newStudyStatusId = complianceJB.addStudyStatus(paramMap);			
		}		
		return FLEX_PROTOCOL_CHANGE_LOG;
	}
	
	public String getStudyCheckNSubmit() {
		return STUDY_CHECK_N_SUBMIT_TILE;
	}

	public StudyJB getStudyJB() {
		return studyJB;
	}	
}
