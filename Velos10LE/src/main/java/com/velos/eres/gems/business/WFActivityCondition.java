package com.velos.eres.gems.business;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Date;


/**
 * The persistent class for the WFACTIVITY_CONDITION database table.
 * 
 */
@Entity
@Table(name="WFACTIVITY_CONDITION")
public class WFActivityCondition implements Serializable {
	
	private Integer pkWfacondition;
	private Integer auditid;
	private Integer createdby;
	private Date createdon;
	private Integer deletedflag;
	private String eaArithmeticoperator;
	private String eaColumnname;
	private String eaEndbrace;
	private String eaKeycondition;
	private String eaLogicaloperator;
	private String eaStartbrace;
	private String eaTablename;
	private String eaType;
	private String eaValue;
	private WorkflowActivity workflowActivity;

	public WFActivityCondition() {
	}


	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SQ_WFACTIVITYCONDITION", allocationSize=1)
    @Column(name="PK_WFACONDITION")
	public Integer getPkWfacondition() {
		return this.pkWfacondition;
	}

	public void setPkWfacondition(Integer pkWfacondition) {
		this.pkWfacondition = pkWfacondition;
	}


	@Column(precision=22)
	public Integer getAuditid() {
		return this.auditid;
	}

	public void setAuditid(Integer auditid) {
		this.auditid = auditid;
	}


	@Column(precision=22)
	public Integer getCreatedby() {
		return this.createdby;
	}

	public void setCreatedby(Integer createdby) {
		this.createdby = createdby;
	}


	public Date getCreatedon() {
		return this.createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}


	@Column(precision=22)
	public Integer getDeletedflag() {
		return this.deletedflag;
	}

	public void setDeletedflag(Integer deletedflag) {
		this.deletedflag = deletedflag;
	}


	@Column(name="EA_ARITHMETICOPERATOR", length=100)
	public String getEaArithmeticoperator() {
		return this.eaArithmeticoperator;
	}

	public void setEaArithmeticoperator(String eaArithmeticoperator) {
		this.eaArithmeticoperator = eaArithmeticoperator;
	}


	@Column(name="EA_COLUMNNAME", length=100)
	public String getEaColumnname() {
		return this.eaColumnname;
	}

	public void setEaColumnname(String eaColumnname) {
		this.eaColumnname = eaColumnname;
	}


	@Column(name="EA_ENDBRACE", length=100)
	public String getEaEndbrace() {
		return this.eaEndbrace;
	}

	public void setEaEndbrace(String eaEndbrace) {
		this.eaEndbrace = eaEndbrace;
	}


	@Column(name="EA_KEYCONDITION", length=200)
	public String getEaKeycondition() {
		return this.eaKeycondition;
	}

	public void setEaKeycondition(String eaKeycondition) {
		this.eaKeycondition = eaKeycondition;
	}


	@Column(name="EA_LOGICALOPERATOR", length=100)
	public String getEaLogicaloperator() {
		return this.eaLogicaloperator;
	}

	public void setEaLogicaloperator(String eaLogicaloperator) {
		this.eaLogicaloperator = eaLogicaloperator;
	}


	@Column(name="EA_STARTBRACE", length=100)
	public String getEaStartbrace() {
		return this.eaStartbrace;
	}

	public void setEaStartbrace(String eaStartbrace) {
		this.eaStartbrace = eaStartbrace;
	}


	@Column(name="EA_TABLENAME", length=100)
	public String getEaTablename() {
		return this.eaTablename;
	}

	public void setEaTablename(String eaTablename) {
		this.eaTablename = eaTablename;
	}


	@Column(name="EA_TYPE", length=100)
	public String getEaType() {
		return this.eaType;
	}

	public void setEaType(String eaType) {
		this.eaType = eaType;
	}


	@Column(name="EA_VALUE", length=500)
	public String getEaValue() {
		return this.eaValue;
	}

	public void setEaValue(String eaValue) {
		this.eaValue = eaValue;
	}


	//bi-directional many-to-one association to WorkflowActivity
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="FK_WORKFLOWACTIVITY")
	public WorkflowActivity getWorkflowActivity() {
		return this.workflowActivity;
	}

	public void setWorkflowActivity(WorkflowActivity workflowActivity) {
		this.workflowActivity = workflowActivity;
	}

}