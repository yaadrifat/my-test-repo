package com.velos.eres.gems.business;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Date;

import java.util.List;


/**
 * The persistent class for the WORKFLOW_ACTIVITY database table.
 * 
 */
@Entity
@Table(name="WORKFLOW_ACTIVITY")
@NamedQuery(name="WorkflowActivity.findAll", query="SELECT w FROM WorkflowActivity w")
public class WorkflowActivity implements Serializable {
	private Integer pkWorkflowactivity;
	private Integer auditid;
	private Integer createdby;
	private Date createdon;
	private Integer deletedflag;
	private Integer fkUserorroleid;
	private String waCallaction;
	private String waDefaultforwardaction;
	private String waDisplayquery;
	private String waKeycolumn;
	private String waKeytable;
	private String waName;
	private Integer waSequence;
	private String waSucessforwardaction;
	private Integer waTerminateflag;
	private String waUserorroletype;
	private Workflow workflow;
	private List<WFActivityCondition> wFActivityConditions;
	private Activity activity;
	private String waOnclickJSON;

	public WorkflowActivity() {
	}


	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SQ_WORKFLOWACTIVITY", allocationSize=1)    
	@Column(name="PK_WORKFLOWACTIVITY")
	public Integer getPkWorkflowactivity() {
		return this.pkWorkflowactivity;
	}

	public void setPkWorkflowactivity(Integer pkWorkflowactivity) {
		this.pkWorkflowactivity = pkWorkflowactivity;
	}


	@Column(precision=22)
	public Integer getAuditid() {
		return this.auditid;
	}

	public void setAuditid(Integer auditid) {
		this.auditid = auditid;
	}


	@Column(precision=22)
	public Integer getCreatedby() {
		return this.createdby;
	}

	public void setCreatedby(Integer createdby) {
		this.createdby = createdby;
	}


	public Date getCreatedon() {
		return this.createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}


	@Column(precision=22)
	public Integer getDeletedflag() {
		return this.deletedflag;
	}

	public void setDeletedflag(Integer deletedflag) {
		this.deletedflag = deletedflag;
	}


	@Column(name="FK_USERORROLEID", precision=22)
	public Integer getFkUserorroleid() {
		return this.fkUserorroleid;
	}

	public void setFkUserorroleid(Integer fkUserorroleid) {
		this.fkUserorroleid = fkUserorroleid;
	}


	@Column(name="WA_CALLACTION", length=500)
	public String getWaCallaction() {
		return this.waCallaction;
	}


	public void setWaCallaction(String waCallaction) {
		this.waCallaction = waCallaction;
	}


	@Column(name="WA_DEFAULTFORWARDACTION", length=500)
	public String getWaDefaultforwardaction() {
		return this.waDefaultforwardaction;
	}

	public void setWaDefaultforwardaction(String waDefaultforwardaction) {
		this.waDefaultforwardaction = waDefaultforwardaction;
	}


	@Column(name="WA_DISPLAYQUERY", length=4000)
	public String getWaDisplayquery() {
		return this.waDisplayquery;
	}

	public void setWaDisplayquery(String waDisplayquery) {
		this.waDisplayquery = waDisplayquery;
	}


	@Column(name="WA_KEYCOLUMN", length=100)
	public String getWaKeycolumn() {
		return this.waKeycolumn;
	}

	public void setWaKeycolumn(String waKeycolumn) {
		this.waKeycolumn = waKeycolumn;
	}


	@Column(name="WA_KEYTABLE", length=100)
	public String getWaKeytable() {
		return this.waKeytable;
	}

	public void setWaKeytable(String waKeytable) {
		this.waKeytable = waKeytable;
	}


	@Column(name="WA_NAME", length=500)
	public String getWaName() {
		return this.waName;
	}

	public void setWaName(String waName) {
		this.waName = waName;
	}


	@Column(name="WA_SEQUENCE", precision=22)
	public Integer getWaSequence() {
		return this.waSequence;
	}

	public void setWaSequence(Integer waSequence) {
		this.waSequence = waSequence;
	}


	@Column(name="WA_SUCESSFORWARDACTION", length=500)
	public String getWaSucessforwardaction() {
		return this.waSucessforwardaction;
	}

	public void setWaSucessforwardaction(String waSucessforwardaction) {
		this.waSucessforwardaction = waSucessforwardaction;
	}


	@Column(name="WA_TERMINATEFLAG", precision=22)
	public Integer getWaTerminateflag() {
		return this.waTerminateflag;
	}

	public void setWaTerminateflag(Integer waTerminateflag) {
		this.waTerminateflag = waTerminateflag;
	}


	@Column(name="WA_USERORROLETYPE", length=500)
	public String getWaUserorroletype() {
		return this.waUserorroletype;
	}

	public void setWaUserorroletype(String waUserorroletype) {
		this.waUserorroletype = waUserorroletype;
	}


	//bi-directional many-to-one association to Workflow
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="FK_WORKFLOWID", referencedColumnName="PK_WORKFLOW")
	public Workflow getWorkflow() {
		return this.workflow;
	}

	public void setWorkflow(Workflow workflow) {
		this.workflow = workflow;
	}


	//bi-directional many-to-one association to WFActivityCondition
	@OneToMany(mappedBy="workflowActivity")
	public List<WFActivityCondition> getWFActivityConditions() {
		return this.wFActivityConditions;
	}

	public void setWFActivityConditions(List<WFActivityCondition> wFActivityConditions) {
		this.wFActivityConditions = wFActivityConditions;
	}

	public WFActivityCondition addWFActivityCondition(WFActivityCondition wFActivityCondition) {
		getWFActivityConditions().add(wFActivityCondition);
		wFActivityCondition.setWorkflowActivity(this);

		return wFActivityCondition;
	}

	public WFActivityCondition removeWFActivityCondition(WFActivityCondition wFActivityCondition) {
		getWFActivityConditions().remove(wFActivityCondition);
		wFActivityCondition.setWorkflowActivity(null);

		return wFActivityCondition;
	}


	//uni-directional many-to-one association to Activity
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PK_ACTIVITY")
	public Activity getActivity() {
		return this.activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}
	
	@Column(name="WA_ONCLICK_JSON")
	public String getWaOnclickJSON() {
		return this.waOnclickJSON;
	}

	public void setWaOnclickJSON(String waOnclickJSON) {
		this.waOnclickJSON = waOnclickJSON;
	}
}