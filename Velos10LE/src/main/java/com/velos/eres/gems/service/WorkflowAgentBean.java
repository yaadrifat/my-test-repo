package com.velos.eres.gems.service;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.velos.eres.gems.business.*;

@Stateless
@Remote({ WorkflowAgentRObj.class })
public class WorkflowAgentBean implements WorkflowAgentRObj

{
	@PersistenceContext(unitName = "eres")
	protected EntityManager em;
	@Resource
	private SessionContext context;

	public Workflow findWorkflowByType(String workflowType)
			throws RemoteException, Exception {

		Query query = null;
		List<Workflow> resultList = null;

		Workflow wf = new Workflow();
		try {
			query = em.createNamedQuery("Workflow.findByType");
			query.setParameter("workflowType", workflowType);

			resultList = query.getResultList();

			if (resultList.size() == 0) {
				return null;
			} else {
				wf = resultList.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			wf = null;
		}
		return wf;
	}
}