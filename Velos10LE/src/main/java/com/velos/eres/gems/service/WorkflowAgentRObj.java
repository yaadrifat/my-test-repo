package com.velos.eres.gems.service;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import com.velos.eres.gems.business.*;

import javax.ejb.Remote;

@Remote
public interface WorkflowAgentRObj {
	public Workflow findWorkflowByType(String workflowType)
			throws RemoteException, Exception;
}