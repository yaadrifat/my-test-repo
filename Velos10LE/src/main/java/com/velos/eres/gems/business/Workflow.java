package com.velos.eres.gems.business;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Date;
import java.util.List;


/**
 * The persistent class for the WORKFLOW database table.
 * 
 */
@Entity
@Table(name="WORKFLOW")
@NamedQuery(name = "Workflow.findByType", query = "SELECT w FROM Workflow w WHERE w.workflowType=:workflowType")
public class Workflow implements Serializable {
	
	private Integer pkWorkflow;
	private Integer createdby;
	private Date createdon;
	private Integer deletedflag;
	private Integer organizationid;
	private String workflowDesc;
	private String workflowFailedemailid;
	private String workflowModule;
	private String workflowName;
	private String workflowStartmode;
	private String workflowType;
	private List<WorkflowActivity> workflowActivities;

	public Workflow() {
	}


	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_GEN")
	@SequenceGenerator(name = "SEQ_GEN", sequenceName = "SQ_WORKFLOW", allocationSize = 1)
	@Column(name = "PK_WORKFLOW")
	public Integer getPkWorkflow() {
		return this.pkWorkflow;
	}

	public void setPkWorkflow(Integer pkWorkflow) {
		this.pkWorkflow = pkWorkflow;
	}


	@Column(precision=22)
	public Integer getCreatedby() {
		return this.createdby;
	}

	public void setCreatedby(Integer createdby) {
		this.createdby = createdby;
	}


	public Date getCreatedon() {
		return this.createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}


	@Column(precision=22)
	public Integer getDeletedflag() {
		return this.deletedflag;
	}

	public void setDeletedflag(Integer deletedflag) {
		this.deletedflag = deletedflag;
	}


	@Column(precision=22)
	public Integer getOrganizationid() {
		return this.organizationid;
	}

	public void setOrganizationid(Integer organizationid) {
		this.organizationid = organizationid;
	}


	@Column(name="WORKFLOW_DESC", length=500)
	public String getWorkflowDesc() {
		return this.workflowDesc;
	}

	public void setWorkflowDesc(String workflowDesc) {
		this.workflowDesc = workflowDesc;
	}


	@Column(name="WORKFLOW_FAILEDEMAILID", length=100)
	public String getWorkflowFailedemailid() {
		return this.workflowFailedemailid;
	}

	public void setWorkflowFailedemailid(String workflowFailedemailid) {
		this.workflowFailedemailid = workflowFailedemailid;
	}


	@Column(name="WORKFLOW_MODULE", length=50)
	public String getWorkflowModule() {
		return this.workflowModule;
	}

	public void setWorkflowModule(String workflowModule) {
		this.workflowModule = workflowModule;
	}


	@Column(name="WORKFLOW_NAME", length=200)
	public String getWorkflowName() {
		return this.workflowName;
	}

	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}


	@Column(name="WORKFLOW_STARTMODE", length=100)
	public String getWorkflowStartmode() {
		return this.workflowStartmode;
	}

	public void setWorkflowStartmode(String workflowStartmode) {
		this.workflowStartmode = workflowStartmode;
	}


	@Column(name="WORKFLOW_TYPE", length=100)
	public String getWorkflowType() {
		return this.workflowType;
	}

	public void setWorkflowType(String workflowType) {
		this.workflowType = workflowType;
	}


	//bi-directional many-to-one association to WorkflowActivity
	@OneToMany(mappedBy="workflow")
	public List<WorkflowActivity> getWorkflowActivities() {
		return this.workflowActivities;
	}

	public void setWorkflowActivities(List<WorkflowActivity> workflowActivities) {
		this.workflowActivities = workflowActivities;
	}

	public WorkflowActivity addWorkflowActivity(WorkflowActivity workflowActivity) {
		getWorkflowActivities().add(workflowActivity);
		workflowActivity.setWorkflow(this);

		return workflowActivity;
	}

	public WorkflowActivity removeWorkflowActivity(WorkflowActivity workflowActivity) {
		getWorkflowActivities().remove(workflowActivity);
		workflowActivity.setWorkflow(null);

		return workflowActivity;
	}

}