package com.velos.eres.gems.business;

import java.util.HashMap;
import java.util.Map;

public class Workflowkeys {
	
	public static Map<String,String> wfJSPNames;
	static {
		wfJSPNames = new HashMap<String,String>();
		wfJSPNames.put(Workflowkeys.WF_Pat_Enrolled,"enrollpatient.jsp");
		wfJSPNames.put(Workflowkeys.WF_Pat_Created,"enrollpatient.jsp");
		wfJSPNames.put(Workflowkeys.WF_Pat_Demographics,"patientDemogScreen.jsp");
		wfJSPNames.put(Workflowkeys.WF_Pat_More_Details,"patientDemogScreen.jsp");		
	}
	
		public static final String WF_Pat_Enrolled="WF_Pat_Enrolled";
		public static final String WF_Pat_Created="WF_Pat_Created";
		public static final String WF_Pat_Demographics="WF_Pat_Demographics";
		public static final String WF_Pat_More_Details="WF_Pat_More_Details";
	
	
}
