package com.velos.eres.compliance.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;
import com.velos.eres.business.common.EIRBDao;
import com.velos.eres.business.common.StudyApndxDao;
import com.velos.eres.business.common.StudySiteDao;
import com.velos.eres.business.common.StudyStatusDao;
import com.velos.eres.compliance.business.ComplianceDAO;
import com.velos.eres.compliance.service.ComplianceAgent;
import com.velos.eres.service.util.CFG;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.widget.business.common.UIFlxPageDao;
import com.velos.esch.service.util.Rlog;

@SuppressWarnings("rawtypes")
public class ComplianceJB {
	private boolean isBlockedForSubmission = false;
	private boolean isLockedStudy = false;
	private boolean isIrbApproved = false;
	private boolean isIrbDisapproved = false;
	private static final String MED_ANC_REV_PEND = "medAncRevPend";
	private static final String MED_ANC_REV_CMP = "medAncRevCmp";
	private static final String CRC_MOD_REQ = "crcReturn";
	private static final String IRB_MOD_REQ = "irb_add_info";
	private static final String CRC_SUBMITTED = "crcSubmitted";
	private static final String CRC_RESUBMITTED = "crcResubmitted";
	private static final String IRB_APPROVED = "app_CHR";
	private static final String IRB_DISAPPROVED = "rej_CHR";
	private static final String NEW_AMENDMENT = "crc_amend";
	
	// Getters and setters
	public boolean getIsBlockedForSubmission() {
		return isBlockedForSubmission;
	}

	public void setBlockedForSubmission(boolean isBlockedForSubmission) {
		this.isBlockedForSubmission = isBlockedForSubmission;
	}

	public boolean getIsLockedStudy() {
		return isLockedStudy;
	}

	public void setLockedStudy(boolean isLockedStudy) {
		this.isLockedStudy = isLockedStudy;
	}
	
	public boolean getIsIrbApproved() {
		return isIrbApproved;
	}

	public void setIrbAppoved(boolean isIrbApproved) {
		this.isIrbApproved = isIrbApproved;
	}
	
	public boolean getIsIrbDisapproved() {
		return isIrbDisapproved;
	}

	public void setIrbDisappoved(boolean isIrbApproved) {
		this.isIrbDisapproved = isIrbApproved;
	}
	// End of getters and setters
	
	public ArrayList<HashMap<String, Object>> fetchStdSubmissionData(
			HashMap<String, String> paramMap) {
		// TODO route through EJB Service
		ComplianceDAO compDAO = new ComplianceDAO();
		//return compDAO.fetchStdSubmissionData(paramMap);
		return compDAO.fetchStdStatData(paramMap);
	}
	
	private static final String ER_STUDY = "er_study";
	
	public String getNextVersion(HashMap<String, Object> paramMap) {
		String nextVer = "1";
		int studyId = (Integer)paramMap.get("studyId");
		int userId = (Integer)paramMap.get("userId");
		int accountId = (Integer)paramMap.get("accountId");
	    StudySiteDao ssDao = new StudySiteDao();
	    ssDao.getAllStudyTeamSites(studyId, accountId);
	    ArrayList siteIdList = ssDao.getSiteIds();
	    ArrayList siteNameList = ssDao.getSiteNames();
	    String defaultOrgName = StringUtil.trueValue(CFG.EIRB_DEFAULT_STUDY_STAT_ORG);
	    int siteId = 0;
	    for (int iX = 0; iX < siteIdList.size(); iX++) {
	    	if (!defaultOrgName.equals((String)siteNameList.get(iX))) {
	    		continue;
	    	}
	    	siteId = (Integer)siteIdList.get(iX);
	    	break;
	    }
	    if (siteId == 0) {
	    	return nextVer;
	    }
	    boolean isAmendment = false;
	    // Go thru history to determine blocking 
		StudyStatusDao studyStatusDao = new StudyStatusDao();
		studyStatusDao.getStudyStatusDesc(studyId, siteId, userId, accountId);
		ArrayList subtypeList = studyStatusDao.getSubTypeStudyStats();
		for (int iY = 0; iY < subtypeList.size(); iY++) {
			String studyStatSubtype = (String)subtypeList.get(iY);
			if (IRB_APPROVED.equals(studyStatSubtype)) {
				isAmendment = true;
				break;
			}
		}
		UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
		uiFlxPageDao.getHighestFlexPageFullVersion(ER_STUDY, studyId);
		int majorVer = uiFlxPageDao.getMajorVer();
		int minorVer = uiFlxPageDao.getMinorVer();
		if (majorVer == 0 && minorVer == 0) {
			return nextVer;
		}
		int submissionPk = uiFlxPageDao.getSubmissionPk();
		if (submissionPk > 0 && EIRBDao.checkIfSubmissionIsApproved(submissionPk)) {
			nextVer = String.valueOf(++majorVer)+".1";
		} else if (isAmendment) {
			nextVer = String.valueOf(majorVer)+"."+String.valueOf(++minorVer);
		} else {
			nextVer = String.valueOf(++majorVer);
		}
		return nextVer;
	}
	
	public static void main(String[] args) {
		ComplianceJB complianceJB = new ComplianceJB();
		System.setProperty("ERES_HOME", "C:/Velos/eResearch_jboss510/conf/");
		HashMap<String, Object> argMap = new HashMap<String, Object>();
		argMap.put("studyId", 1344);
		argMap.put("userId", 226);
		argMap.put("accountId", 53);
		String fullVer = complianceJB.getNextVersion(argMap);
		System.out.println("fullVer="+fullVer);
	}
	
	public void retrieveBlockSubmissionFlag(HashMap<String, Object> paramMap) {
		int studyId = (Integer)paramMap.get("studyId");
		int userId = (Integer)paramMap.get("userId");
		int accountId = (Integer)paramMap.get("accountId");
	    StudySiteDao ssDao = new StudySiteDao();
	    ssDao.getAllStudyTeamSites(studyId, accountId);
	    ArrayList siteIdList = ssDao.getSiteIds();
	    ArrayList siteNameList = ssDao.getSiteNames();
	    String defaultOrgName = StringUtil.trueValue(CFG.EIRB_DEFAULT_STUDY_STAT_ORG);
	    isBlockedForSubmission = false;
	    // Go thru history to determine blocking 
	    for (int iX = 0; iX < siteIdList.size(); iX++) {
	    	if (!defaultOrgName.equals((String)siteNameList.get(iX))) {
	    		continue;
	    	}
	    	int siteId = (Integer)siteIdList.get(iX);
			StudyStatusDao studyStatusDao = new StudyStatusDao();
			studyStatusDao.getStudyStatusDesc(studyId, siteId, userId, accountId);
			ArrayList subtypeList = studyStatusDao.getSubTypeStudyStats();
			for (int iY = 0; iY < subtypeList.size(); iY++) {
				String studyStatSubtype = (String)subtypeList.get(iY);
				if (NEW_AMENDMENT.equals(studyStatSubtype)) {
					isBlockedForSubmission = false;
					return;
				}
				if (CRC_SUBMITTED.equals(studyStatSubtype) || 
						CRC_RESUBMITTED.equals(studyStatSubtype)) {
					isBlockedForSubmission = true;
					return;
				}
				if (CRC_MOD_REQ.equals(studyStatSubtype)) {
					isBlockedForSubmission = false;
					return;
				}
				if (IRB_MOD_REQ.equals(studyStatSubtype)) {
					isBlockedForSubmission = false;
					return;
				}
				if (MED_ANC_REV_CMP.equals(studyStatSubtype)) {
					isBlockedForSubmission = false;
					return;
				}
				if (MED_ANC_REV_PEND.equals(studyStatSubtype)) {
					isBlockedForSubmission = true;
					return;
				}
			}
	    }
	}

	public void retrieveLockStudyFlag(HashMap<String, Object> paramMap) {
		int studyId = (Integer)paramMap.get("studyId");
		int userId = (Integer)paramMap.get("userId");
		int accountId = (Integer)paramMap.get("accountId");
	    StudySiteDao ssDao = new StudySiteDao();
	    ssDao.getAllStudyTeamSites(studyId, accountId);
	    ArrayList siteIdList = ssDao.getSiteIds();
	    ArrayList siteNameList = ssDao.getSiteNames();
	    String defaultOrgName = StringUtil.trueValue(CFG.EIRB_DEFAULT_STUDY_STAT_ORG);
	    isLockedStudy = false;
	    isIrbApproved = false;
		isIrbDisapproved = false;
	    
	    // Go thru history to determine locking 
	    for (int iX = 0; iX < siteIdList.size(); iX++) {
	    	if (!defaultOrgName.equals((String)siteNameList.get(iX))) {
	    		continue;
	    	}
	    	int siteId = (Integer)siteIdList.get(iX);
			StudyStatusDao studyStatusDao = new StudyStatusDao();
			studyStatusDao.getStudyStatusDesc(studyId, siteId, userId, accountId);
			ArrayList subtypeList = studyStatusDao.getSubTypeStudyStats();
			for (int iY = 0; iY < subtypeList.size(); iY++) {
				String studyStatSubtype = (String)subtypeList.get(iY);				
				if (IRB_APPROVED.equals(studyStatSubtype)) {
					isIrbApproved = true;
					isLockedStudy = true;
					return;
				}
				if (IRB_DISAPPROVED.equals(studyStatSubtype)) {
					isIrbDisapproved = true;
					isLockedStudy = true;
					return;
				}
				if (NEW_AMENDMENT.equals(studyStatSubtype)) {
					isLockedStudy = false;
					return;
				}
				if (CRC_SUBMITTED.equals(studyStatSubtype) || 
						CRC_RESUBMITTED.equals(studyStatSubtype)) {
					isLockedStudy = true;
					return;
				}
				if (CRC_MOD_REQ.equals(studyStatSubtype)) {
					isLockedStudy = false;
					return;
				}
				if (IRB_MOD_REQ.equals(studyStatSubtype)) {
					isLockedStudy = false;
					return;
				}
				if (MED_ANC_REV_PEND.equals(studyStatSubtype)) {
					isLockedStudy = false;
					return;
				}
			}
	    }
	}
	
	public boolean isIrbApprovalExistsInStudyStatusHistory(HashMap<String, Object> paramMap) {		
		int studyId = (Integer)paramMap.get("studyId");
		int userId = (Integer)paramMap.get("userId");
		int accountId = (Integer)paramMap.get("accountId");
	    StudySiteDao ssDao = new StudySiteDao();
	    ssDao.getAllStudyTeamSites(studyId, accountId);
	    ArrayList siteIdList = ssDao.getSiteIds();	    
	   
	    for (int iX = 0; iX < siteIdList.size(); iX++) {
	    	int siteId = (Integer)siteIdList.get(iX);
	    	String siteName = ssDao.getSiteNameForSiteId(siteId);
	    	if(CFG.EIRB_DEFAULT_STUDY_STAT_ORG.equals(siteName)) {
	    		StudyStatusDao studyStatusDao = new StudyStatusDao();
				studyStatusDao.getStudyStatusDesc(studyId, siteId, userId, accountId);
				ArrayList subtypeList = studyStatusDao.getSubTypeStudyStats();
				for (int iY = 0; iY < subtypeList.size(); iY++) {
					String studyStatSubtype = (String)subtypeList.get(iY);
						
					if (IRB_APPROVED.equals(studyStatSubtype)) {
						isIrbApproved = true;						
						break;
					}
				}
				
	    	}
	    }	
	   
	    return isIrbApproved;
	}
	
	public int addStudyStatus(HashMap paramMap) {		
		int pk=0;
		try {
			int studyId = ((Integer)paramMap.get("studyId")).intValue();    	
			Integer usrId = (Integer)paramMap.get("userId");			
			
			ComplianceAgent compAgent = (ComplianceAgent)EJBUtil.getComplianceAgentHome();	 
	    	pk = compAgent.addStudyStatusForAmendment(studyId, usrId.intValue());	    	
	    	
	    	if( pk != 0 ) {
	    		ComplianceJB compB = new ComplianceJB();	    		
	    		compAgent.addSubmissionForAmendment(studyId, usrId, pk);	    		
	    		
	    		boolean isFrozen = getIsLockedStudy();
				if(isFrozen) {
					unlockStudy(paramMap);
					Rlog.info("ComplianceJB:addStudyStatus", "Unlocked Study successfully");
				}
	    		compAgent.makeAttachmentsEditableForAmendment(studyId, usrId);
	    		updateFileBlobObject(String.valueOf(studyId));
	    		boolean isBlockedForSubmission = getIsBlockedForSubmission();
			    if(isBlockedForSubmission) {
			    	unblockStudy(paramMap);
			    	Rlog.info("ComplianceJB:addStudyStatus", "Unblocked Study successfully.");
			    }
	    	}
		} catch(Exception e) {
			Rlog.error("ComplianceJB:addStudyStatus", "Exception : " + e.getMessage());
		}
    	return pk;
    }
	
	public void unlockStudy(HashMap paramMap) {
		int studyId = (Integer)paramMap.get("studyId");
		int userId = (Integer)paramMap.get("userId");
		int accountId = (Integer)paramMap.get("accountId");
	    StudySiteDao ssDao = new StudySiteDao();
	    ssDao.getAllStudyTeamSites(studyId, accountId);
	    ArrayList siteIdList = ssDao.getSiteIds();
	   
	    for (int iX = 0; iX < siteIdList.size(); iX++) {
	    	int siteId = (Integer)siteIdList.get(iX);
	    	String siteName = ssDao.getSiteNameForSiteId(siteId);
	    	if(CFG.EIRB_DEFAULT_STUDY_STAT_ORG.equals(siteName)) {
	    		StudyStatusDao studyStatusDao = new StudyStatusDao();
				studyStatusDao.getStudyStatusDesc(studyId, siteId, userId, accountId);
				ArrayList subtypeList = studyStatusDao.getSubTypeStudyStats();
				for (int iY = 0; iY < subtypeList.size(); iY++) {
					String studyStatSubtype = (String)subtypeList.get(iY);
					if (NEW_AMENDMENT.equals(studyStatSubtype)) {
						isLockedStudy = false;
						return;
					}
				}
	    	}
	    }	
	}
	
	public void unblockStudy(HashMap paramMap) {
		int studyId = (Integer)paramMap.get("studyId");
		int userId = (Integer)paramMap.get("userId");
		int accountId = (Integer)paramMap.get("accountId");
		StudySiteDao ssDao = new StudySiteDao();
	    ssDao.getAllStudyTeamSites(studyId, accountId);
	    ArrayList siteIdList = ssDao.getSiteIds();
	   
	    for (int iX = 0; iX < siteIdList.size(); iX++) {
	    	int siteId = (Integer)siteIdList.get(iX);
	    	String siteName = ssDao.getSiteNameForSiteId(siteId);
	    	if(CFG.EIRB_DEFAULT_STUDY_STAT_ORG.equals(siteName)) {
	    		StudyStatusDao studyStatusDao = new StudyStatusDao();
				studyStatusDao.getStudyStatusDesc(studyId, siteId, userId, accountId);
				ArrayList subtypeList = studyStatusDao.getSubTypeStudyStats();
				for (int iY = 0; iY < subtypeList.size(); iY++) {
					String studyStatSubtype = (String)subtypeList.get(iY);
					if (NEW_AMENDMENT.equals(studyStatSubtype)) {
						isBlockedForSubmission = false;
						return;
					}
				}
	    	}
	    }	
		
	}
	
	public String getCurrentStudyStatus(HashMap paramMap) {
		int studyId = (Integer)paramMap.get("studyId");
		int userId = (Integer)paramMap.get("userId");
		int accountId = (Integer)paramMap.get("accountId");
		String studyStatSubtype="";
		StudySiteDao ssDao = new StudySiteDao();
	    ssDao.getAllStudyTeamSites(studyId, accountId);
	    ArrayList siteIdList = ssDao.getSiteIds();
	   
	    for (int iX = 0; iX < siteIdList.size(); iX++) {
	    	int siteId = (Integer)siteIdList.get(iX);
	    	String siteName = ssDao.getSiteNameForSiteId(siteId);
	    	if(CFG.EIRB_DEFAULT_STUDY_STAT_ORG.equals(siteName)) {
	    		StudyStatusDao studyStatusDao = new StudyStatusDao();
				studyStatusDao.getStudyStatusDesc(studyId, siteId, userId, accountId);
				ArrayList subtypeList = studyStatusDao.getSubTypeStudyStats();
				
				if(subtypeList.size() > 0 ) {
					studyStatSubtype = (String)subtypeList.get(0);
					break;
				}				
	    	}
	    }
	    return studyStatSubtype;		
	}
	
	public int getAmendCount(HashMap<String, Object> paramMap) {		
		int studyId = (Integer)paramMap.get("studyId");
		int userId = (Integer)paramMap.get("userId");
		int accountId = (Integer)paramMap.get("accountId");
	    StudySiteDao ssDao = new StudySiteDao();
	    ssDao.getAllStudyTeamSites(studyId, accountId);
	    ArrayList siteIdList = ssDao.getSiteIds();
	    
	    int amendCount=0;
	    for (int iX = 0; iX < siteIdList.size(); iX++) {
	    	int siteId = (Integer)siteIdList.get(iX);
	    	String siteName = ssDao.getSiteNameForSiteId(siteId);
	    	if(CFG.EIRB_DEFAULT_STUDY_STAT_ORG.equals(siteName)) {
	    		StudyStatusDao studyStatusDao = new StudyStatusDao();
				studyStatusDao.getStudyStatusDesc(studyId, siteId, userId, accountId);
				ArrayList subtypeList = studyStatusDao.getSubTypeStudyStats();
				amendCount=0;
				for (int iY = 0; iY < subtypeList.size(); iY++) {
					String studyStatSubtype = (String)subtypeList.get(iY);
					if("crc_amend".equals(studyStatSubtype)) {
						amendCount++;
					}					
					if (IRB_APPROVED.equals(studyStatSubtype)) {											
						break;
					}
				}
				
	    	}
	    }	    
	    return amendCount;
	}
	
	public int getIrbApprovedCount(HashMap<String, Object> paramMap) {		
		int studyId = (Integer)paramMap.get("studyId");
		int userId = (Integer)paramMap.get("userId");
		int accountId = (Integer)paramMap.get("accountId");
	    StudySiteDao ssDao = new StudySiteDao();
	    ssDao.getAllStudyTeamSites(studyId, accountId);
	    ArrayList siteIdList = ssDao.getSiteIds();
	    
	    int irbApprovedCount=0;
	    for (int iX = 0; iX < siteIdList.size(); iX++) {
	    	int siteId = (Integer)siteIdList.get(iX);
	    	String siteName = ssDao.getSiteNameForSiteId(siteId);
	    	if(CFG.EIRB_DEFAULT_STUDY_STAT_ORG.equals(siteName)) {
	    		StudyStatusDao studyStatusDao = new StudyStatusDao();
				studyStatusDao.getStudyStatusDesc(studyId, siteId, userId, accountId);
				ArrayList subtypeList = studyStatusDao.getSubTypeStudyStats();
				irbApprovedCount=0;
				for (int iY = 0; iY < subtypeList.size(); iY++) {
					String studyStatSubtype = (String)subtypeList.get(iY);
					if (IRB_APPROVED.equals(studyStatSubtype)) {
						irbApprovedCount++;
					}
				}
				
	    	}
	    }	    
	    return irbApprovedCount;
	}
	
	public boolean isIrbDisapprovalExistsInStudyStatusHistory(HashMap<String, Object> paramMap) {		
		int studyId = (Integer)paramMap.get("studyId");
		int userId = (Integer)paramMap.get("userId");
		int accountId = (Integer)paramMap.get("accountId");
	    StudySiteDao ssDao = new StudySiteDao();
	    ssDao.getAllStudyTeamSites(studyId, accountId);
	    ArrayList siteIdList = ssDao.getSiteIds();	    
	   
	    for (int iX = 0; iX < siteIdList.size(); iX++) {
	    	int siteId = (Integer)siteIdList.get(iX);
	    	String siteName = ssDao.getSiteNameForSiteId(siteId);
	    	if(CFG.EIRB_DEFAULT_STUDY_STAT_ORG.equals(siteName)) {
	    		StudyStatusDao studyStatusDao = new StudyStatusDao();
				studyStatusDao.getStudyStatusDesc(studyId, siteId, userId, accountId);
				ArrayList subtypeList = studyStatusDao.getSubTypeStudyStats();
				for (int iY = 0; iY < subtypeList.size(); iY++) {
					String studyStatSubtype = (String)subtypeList.get(iY);
						
					if (IRB_DISAPPROVED.equals(studyStatSubtype)) {
						isIrbDisapproved = true;						
						break;
					}
				}
				
	    	}
	    }	
	   
	    return isIrbApproved;
	}
	
	public void updateFileBlobObject(String studyId) {
		
		HashMap<String, Integer> hMap= new HashMap<String, Integer>();
		ComplianceAgent compAgent = (ComplianceAgent)EJBUtil.getComplianceAgentHome();		
		StudyApndxDao sadpndx = new StudyApndxDao();
		ArrayList<String> arList = sadpndx.getStudyVerNumber(Integer.valueOf(studyId));
		String verStr = (String)arList.get(0);
		String prevVerStr = (String)arList.get(1);
		hMap = sadpndx.getStudyApndxIdForFileBlob(verStr ,prevVerStr, Integer.parseInt(studyId));
    	compAgent.updateFileBlob(hMap, Integer.valueOf(studyId));
	}
	
	public void updateFileBlobObjectForIrbApproved(String studyId) {
		HashMap<String, Integer> hMap= new HashMap<String, Integer>();
		ComplianceAgent compAgent = (ComplianceAgent)EJBUtil.getComplianceAgentHome();	
		StudyApndxDao sadpndx = new StudyApndxDao();
		ArrayList arList = sadpndx.getStudyVerNumber(Integer.valueOf(studyId));
		String prevVer = (String)arList.get(0);
		StringTokenizer str = new StringTokenizer(prevVer, ".");
		String ver="";
		String dec ="";
		while(str.hasMoreElements()) {
			ver = str.nextToken();
			dec = str.nextToken();
		}		
		String version = ver + "." + "0";
		hMap = sadpndx.getStudyApndxIdForFileBlob(version ,prevVer.toString(), Integer.parseInt(studyId));
    	compAgent.updateFileBlob(hMap, Integer.valueOf(studyId));
	}

}
