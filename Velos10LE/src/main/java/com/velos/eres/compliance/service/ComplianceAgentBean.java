/*
 * Classname : ComplianceAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 07/31/2014
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Ashwani Godara
 */

package com.velos.eres.compliance.service;

/* Import Statements */
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.velos.eres.business.appendix.impl.AppendixBean;
import com.velos.eres.business.codelst.impl.CodelstBean;
import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.StudyApndxDao;
import com.velos.eres.business.common.StudySiteDao;
import com.velos.eres.business.common.StudyVerDao;
import com.velos.eres.business.studyStatus.impl.StudyStatusBean;
import com.velos.eres.business.studyVer.impl.StudyVerBean;
import com.velos.eres.business.submission.impl.SubmissionBean;
import com.velos.eres.business.submission.impl.SubmissionStatusBean;
import com.velos.eres.service.appendixAgent.AppendixAgentRObj;
import com.velos.eres.service.studyStatusAgent.StudyStatusAgentRObj;
import com.velos.eres.service.studyVerAgent.StudyVerAgentRObj;
import com.velos.eres.service.submissionAgent.SubmissionAgent;
import com.velos.eres.service.submissionAgent.SubmissionStatusAgent;
import com.velos.eres.service.util.CFG;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/**
 * The stateless session EJB acting as a facade for the entity CMP -
 * ComplianceAgentBean.<br>
 * <br>
 *
 * @author Ashwani Godara
 * @see ComplianceAgentBean
 * @ejbRemote ComplianceAgent
 */
@Stateless
@Remote( { ComplianceAgent.class })
@SuppressWarnings({ "unchecked", "rawtypes" })
public class ComplianceAgentBean implements ComplianceAgent
{

	@PersistenceContext(unitName="eres")
	protected EntityManager em;

	@Resource
	private SessionContext context;

	public int addStudyStatusForAmendment(int studyId, int usrId) {

		int newStatusPK =0; 	                    	
		int pkCodelst=0;
		//Get the pk of er_codelst for crc_amend subtype
		try {
			Query query = em.createNamedQuery("findByCodelstSubtype");
			query.setParameter("codelst_subtyp", "crc_amend");	    	                     

			ArrayList list = (ArrayList) query.getResultList();
			CodelstBean cBean = null;

			if (list.size() > 0) {
				cBean= (CodelstBean)list.get(0);
				pkCodelst=cBean.clstId;	    	  
			}
		} catch(Exception e) {
			Rlog.error("studystatus","EXCEPTION IN Named Query: ComplianceAgentBean:findByCodelstSubtype" + e);
			e.printStackTrace();
		}

		//Get the pk of er_site for default org
		StudySiteDao ssDao = new StudySiteDao();
		int pkSite=0;
		String pkSiteStr="";
		pkSite = ssDao.getPkSiteBySiteName(CFG.EIRB_DEFAULT_STUDY_STAT_ORG);

		if(pkSite != 0 ) {
			pkSiteStr = (Integer.valueOf(pkSite)).toString();
		}	  

		//Create and populate StudyStatusBean
		StudyStatusBean statBean = new StudyStatusBean();

		statBean.setStudyStatus(new Integer(pkCodelst).toString());
		statBean.setCreator(Integer.toString(usrId));
		statBean.setModifiedBy(Integer.toString(usrId));
		statBean.setStatStudy(Integer.toString(studyId));
		statBean.setSiteId(pkSiteStr);
		statBean.setCurrentStat("1");
		statBean.setStatStartDate(new Date(System.currentTimeMillis()));

		//Add crc_amend study status 
		StudyStatusAgentRObj studyStatusAgent = (StudyStatusAgentRObj)EJBUtil.getStudyStatusAgentHome();	   
		newStatusPK = studyStatusAgent.setStudyStatusDetails(statBean);

		if (newStatusPK < 1) {
			Rlog.info("ComplianceAgentBean:addStudyStatusForAmendment", "Study Status is not created.");            			    	                               
		} else {
			Rlog.info("ComplianceAgentBean:addStudyStatusForAmendment", "Study Status is created successfully." + newStatusPK);
		}
		return newStatusPK;
	}

	public void makeAttachmentsEditableForAmendment(int studyId, int usrId) {
		StudyStatusAgentRObj studyStatusAgent = (StudyStatusAgentRObj)EJBUtil.getStudyStatusAgentHome();	   
		studyStatusAgent.copyPreviousVerDocstoNextVersion(studyId, String.valueOf(usrId), "crc_amend");
	}	
	
	public void addSubmissionForAmendment(int studyId, int usrId, int studyStatId) {		
		SubmissionAgent submissionAgent = EJBUtil.getSubmissionAgentHome();	
		Integer exists = submissionAgent.getExistingSubmissionByFkStudyAndFlag(studyId, new Integer(2));		
		if((exists == null) || (exists.intValue() <= 0) ) {			
			Integer submissionType = null;
			Integer submissionStatus= null;	
			CodeDao codeDao = new CodeDao();
			codeDao.getCodeValues("submission");
			ArrayList codeSubtypes = codeDao.getCSubType();
			ArrayList codePks = codeDao.getCId();
			for (int iX=0; iX<codeSubtypes.size(); iX++) {
				if ("study_amend".equals(codeSubtypes.get(iX))) {
					submissionType = (Integer)codePks.get(iX);
					break;
				}
			}
			codeDao = null;
			codeSubtypes = null;
			codePks = null;
	
			codeDao = new CodeDao();
			codeDao.getCodeValues("subm_status");
			codeSubtypes = codeDao.getCSubType();
			codePks = codeDao.getCId();
			for (int iX=0; iX<codeSubtypes.size(); iX++) {
				if ("submitted".equals(codeSubtypes.get(iX))) {
					submissionStatus = (Integer)codePks.get(iX);
					break;
				}
			}			
			SubmissionBean submB = new SubmissionBean();	
	
			submB.setCreator(new Integer(usrId));
			submB.setFkStudy(studyId);
			submB.setLastModifiedBy(new Integer(usrId));
			submB.setSubmissionStatus(submissionStatus);
			submB.setSubmissionType(submissionType);
			submB.setSubmissionFlag(2);
	
			int newSubmId = submissionAgent.createSubmission(submB);
			if( newSubmId <= 0 ) {
				Rlog.info("ComplianceAgentBean:addSubmissionForAmendment", "Submission is not created.");            			    	                               
			} else {
				Rlog.info("ComplianceAgentBean:addSubmissionForAmendment", "Submission is created successfully." + newSubmId);
				SubmissionStatusBean submStatB = new SubmissionStatusBean();
				submStatB.setFkSubmission(newSubmId);
				submStatB.setFkStudyStat(studyStatId);
				SubmissionStatusAgent submissionStatAgent = EJBUtil.getSubmissionStatusAgentHome();
				submissionStatAgent.createSubmissionStatus(submStatB);
			}
		}

	}
	
	public void updateFileBlob(HashMap<String, Integer> hMap , int studyId ) {
		StudyStatusAgentRObj studyStatusAgent = (StudyStatusAgentRObj)EJBUtil.getStudyStatusAgentHome();	   
		studyStatusAgent.updateBlobfileObject(hMap, studyId);
	}

}