package com.velos.eres.compliance.business;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.service.util.VelosResourceBundle;

public class ComplianceDAO {

	public ArrayList<HashMap<String, Object>> fetchStdStatData(
			HashMap<String, String> paramMap) {


		String pk_statuses = paramMap.get("pk_statuses");
		String accountId = paramMap.get("accountId");
		String userId = paramMap.get("userId");
		String organization = paramMap.get("organization");
		
		StringBuffer sqlBuffer = new StringBuffer();
		
		
		sqlBuffer.append(" (SELECT st.rowid rowcount, study_number, study_title, F_CODELST_DESC(st.FK_CODELST_STUDYSTAT) submission_status_desc,"
				+ " DECODE(submission_type, null, '-', F_CODELST_DESC(submission_type)) submission_type_desc,"
				+ " DECODE(submission_type, null, '-', (select codelst_subtyp from er_codelst where pk_codelst = submission_type)) submission_type_subtype,"
				+ " st.FK_CODELST_STUDYSTAT, (SELECT codelst_subtyp FROM er_codelst WHERE pk_codelst = st.FK_CODELST_STUDYSTAT ) submission_status_subtype,"
				+ " F_CODELST_DESC(st.FK_CODELST_REVBOARD)submission_board_name, st.FK_CODELST_REVBOARD, pk_study , TO_CHAR(st.created_on, 'MON-DD-YYYY') submission_status_date,"
				+ " (ROUND(sysdate - st.created_on)) || ' days' submission_since, to_char(s.created_on, 'MON-DD-YYYY') created_on,"
				+ " DECODE((SELECT NVL(MAX(PAGE_VER), 0) FROM UI_FLX_PAGEVER WHERE PAGE_MOD_TABLE = 'er_study' AND PAGE_MOD_PK      = pk_study and fk_submission = ss.pk_submission"
				+ " ),0, 0, (SELECT NVL(MAX(PAGE_VER), 0) FROM UI_FLX_PAGEVER WHERE PAGE_MOD_TABLE = 'er_study' AND PAGE_MOD_PK      = pk_study and fk_submission = ss.pk_submission"
				+ " ) ||'.' ||(SELECT NVL(MAX(PAGE_MINOR_VER), 0) FROM UI_FLX_PAGEVER WHERE PAGE_MOD_TABLE = 'er_study' AND PAGE_MOD_PK      = pk_study and fk_submission = ss.pk_submission"
				+ " AND PAGE_VER = (SELECT NVL(MAX(PAGE_VER), 0) FROM UI_FLX_PAGEVER WHERE PAGE_MOD_TABLE = 'er_study' AND PAGE_MOD_PK      = pk_study and fk_submission = ss.pk_submission))) current_version,"
				+ " (SELECT NVL(TO_CHAR(MAX(created_on), 'MON-DD-YYYY'),'')  FROM er_studystat"
				+ " WHERE fk_study = s.pk_study AND FK_CODELST_STUDYSTAT   = (SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'studystat' AND codelst_subtyp = 'crcSubmitted'"
				+ " ) ) submitted_date,"
				+ " (SELECT NVL(TO_CHAR(MAX(created_on), 'MON-DD-YYYY'),'') FROM er_studystat WHERE fk_study = s.pk_study AND FK_CODELST_STUDYSTAT   ="
				+ " (SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'studystat'  AND codelst_subtyp = 'app_CHR' ) ) approved_date,"
				+ " (SELECT DECODE(codelst_subtyp, 'app_CHR' , (ROUND(sysdate - st.created_on)), (ROUND(sysdate - s.created_on))) FROM er_codelst"
				+ " WHERE pk_codelst = st.FK_CODELST_STUDYSTAT ) || ' days' total_elapsed_days");		
		sqlBuffer.append("  FROM er_study s, er_studystat st, er_submission ss, er_submission_status sst");
		sqlBuffer.append(" WHERE ss.fk_study = s.pk_study AND ss.pk_submission = sst.fk_submission and sst.FK_STUDYSTAT = st.pk_STUDYSTAT"
				+ " and sst.PK_SUBMISSION_STATUS = (select max(PK_SUBMISSION_STATUS) from er_submission_status where fk_submission = ss.pk_submission)"
				+ " AND st.FK_CODELST_STUDYSTAT  IN (" + pk_statuses+ ")"
				+ " AND s.fk_account = " + accountId + " and s.study_creation_type = 'A' and st.fk_site = (select pk_site from er_site where site_name = '" + organization +"' )"
				+ " AND ( EXISTS (SELECT * FROM er_studyteam t WHERE t.fk_study = s.pk_study  AND t.fk_user = "+ userId + " AND NVL(t.study_team_usr_type,'D')='D' )"
				+ " OR pkg_superuser.F_Is_Superuser(" + userId + ", pk_study) = 1 ))") ;
		
		sqlBuffer.append(" UNION ");
		
		sqlBuffer.append("(SELECT st.rowid rowcount, study_number, study_title,  F_CODELST_DESC(st.FK_CODELST_STUDYSTAT) submission_status_desc,"
				+ " '-' submission_type_desc, '-' submission_type_subtype, st.FK_CODELST_STUDYSTAT, (SELECT codelst_subtyp FROM er_codelst WHERE pk_codelst = st.FK_CODELST_STUDYSTAT"
				+ " ) submission_status_subtype, F_CODELST_DESC(st.FK_CODELST_REVBOARD)submission_board_name, st.FK_CODELST_REVBOARD, pk_study ,"
				+ " TO_CHAR(st.created_on, 'MON-DD-YYYY') submission_status_date, (ROUND(sysdate - st.created_on)) || ' days' submission_since, "
				+ " TO_CHAR(s.created_on, 'MON-DD-YYYY') created_on, DECODE( (SELECT NVL(MAX(PAGE_VER), 0) FROM UI_FLX_PAGEVER WHERE PAGE_MOD_TABLE = 'er_study'"
				+ " AND PAGE_MOD_PK = pk_study ),0, 0, (SELECT NVL(MAX(PAGE_VER), 0) FROM UI_FLX_PAGEVER WHERE PAGE_MOD_TABLE = 'er_study' AND PAGE_MOD_PK"
				+ " = pk_study ) ||'.' || (SELECT NVL(MAX(PAGE_MINOR_VER), 0) FROM UI_FLX_PAGEVER WHERE PAGE_MOD_TABLE = 'er_study' AND PAGE_MOD_PK = pk_study"
				+ " AND PAGE_VER  = (SELECT NVL(MAX(PAGE_VER), 0) FROM UI_FLX_PAGEVER WHERE PAGE_MOD_TABLE = 'er_study' AND PAGE_MOD_PK = pk_study"
				+ " ))) current_version,(SELECT NVL(TO_CHAR(MAX(created_on), 'MON-DD-YYYY'),'') "
				+ " FROM er_studystat WHERE fk_study = s.pk_study AND FK_CODELST_STUDYSTAT = (SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'studystat'"
				+ " AND codelst_subtyp = 'crcSubmitted' ) ) submitted_date, (SELECT NVL(TO_CHAR(MAX(created_on), 'MON-DD-YYYY'),'') FROM er_studystat WHERE fk_study = s.pk_study"
				+ " AND FK_CODELST_STUDYSTAT = (SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'studystat' AND codelst_subtyp = 'app_CHR' )"
				+ " ) approved_date, (SELECT DECODE(codelst_subtyp, 'app_CHR' , (ROUND(sysdate - st.created_on)), (ROUND(sysdate - s.created_on))) FROM er_codelst"
				+ " WHERE pk_codelst = st.FK_CODELST_STUDYSTAT ) || ' days' total_elapsed_days");
		sqlBuffer.append(" FROM er_study s, er_studystat st");
		sqlBuffer.append(" WHERE st.fk_study = s.pk_study AND st.CURRENT_STAT = 1 AND st.FK_CODELST_STUDYSTAT IN (" + pk_statuses+ ")"
				+ " AND s.fk_account = " + accountId + " AND s.study_creation_type    = 'A' and st.fk_site = (select pk_site from er_site where site_name = '" + organization +"' )"
				+ " AND ( NOT EXISTS (SELECT * FROM er_submission WHERE fk_study = s.pk_study ))"
				+ " AND ( EXISTS (SELECT * FROM er_studyteam t WHERE t.fk_study = s.pk_study  AND t.fk_user = "+ userId + " AND NVL(t.study_team_usr_type,'D')='D' )"
				+ " OR pkg_superuser.F_Is_Superuser(" + userId + ", pk_study) = 1 ))") ;
		
		ArrayList<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> dataMap = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		try {
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(sqlBuffer.toString());
			rs = pstmt.executeQuery();

			while (rs.next()) {
				dataMap = new HashMap<String, Object>();
				dataMap.put("rowcount", rs.getString(("rowcount")));
				dataMap.put("submission_status_desc", rs
						.getString("submission_status_desc"));
				dataMap.put("colorcode",VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE, "studystat."+rs.getString("submission_status_subtype")+".colorcode"));				
				
				dataMap.put("study_number", rs.getString(("study_number")));
				dataMap.put("study_title", rs.getString("study_title"));
				dataMap.put("submission_type_desc", rs
						.getString("submission_type_desc"));
				dataMap
				.put("review_type_desc", "");
				dataMap
				.put("submission_board_name", rs
						.getString("submission_board_name"));
				dataMap
				.put("submission_status_date", rs
						.getString("submission_status_date"));
				dataMap
				.put("current_version", rs
						.getString("current_version"));
				dataMap
				.put("submission_since", rs
						.getString("submission_since"));
				dataMap
				.put("created_on", rs
						.getString("created_on"));
				dataMap
				.put("submitted_date", rs
						.getString("submitted_date"));
				dataMap
				.put("approved_date", rs
						.getString("approved_date"));
				dataMap
				.put("total_elapsed_days", rs
						.getString("total_elapsed_days"));
				
				dataMap
				.put("checkboxPlaceHolder", "");
				
				dataMap.put("pk_study", rs
						.getString("pk_study"));
				dataMap.put("submission_type_subtype", rs
						.getString("submission_type_subtype"));
				dataMap.put("submission_type_desc", rs
						.getString("submission_type_desc"));

				maplist.add(dataMap);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return maplist;
	
	}
	
	public ArrayList<HashMap<String, Object>> fetchStdSubmissionData(
			HashMap<String, String> paramMap) {

		String pk_statuses = paramMap.get("pk_statuses");
		String newApplication = paramMap.get("newApplication");
		String accountId = paramMap.get("accountId");
		String userId = paramMap.get("userId");
		// String status = paramMap.get("status");
		//String submissionType = paramMap.get("submissionType");
		//String reviewType = paramMap.get("reviewType");
		// String creationStartDate = paramMap.get("creationStartDate");
		// String creationEndDate = paramMap.get("creationEndDate");

		StringBuffer sqlBuffer = new StringBuffer();
		sqlBuffer
				.append(" Select st.rowid rowcount, study_number, study_title, ");
		sqlBuffer
				.append("  (select codelst_subtyp from er_codelst where pk_codelst = submission_type) submission_type_subtype, ");
		sqlBuffer
				.append("  F_CODELST_DESC(submission_type) submission_type_desc,submission_type,");
		sqlBuffer
				.append("  F_CODELST_DESC(st.submission_status) submission_status_desc,st.submission_status,");
		sqlBuffer
		.append("(select codelst_subtyp from er_codelst where pk_codelst = st.submission_status) submission_status_subtype,");
		sqlBuffer
				.append("  F_CODELST_DESC(sb.submission_review_type) review_type_desc,sb.submission_review_type,(select review_board_name from er_review_board where pk_review_board = fk_review_board) submission_board_name, fk_review_board, pk_study ,pk_submission, pk_submission_board, ");
		sqlBuffer.append("  to_char(st.submission_status_date, 'MON-DD-YYYY') submission_status_date, ");
		sqlBuffer.append("  (round(sysdate - st.submission_status_date)) || ' days' submission_since, ");
		//Added by Aman for Lind FTE
		sqlBuffer
		.append("  (select nvl(max(PAGE_VER), 0) from UI_FLX_PAGEVER where PAGE_MOD_TABLE = 'er_study' and PAGE_MOD_PK = pk_study) current_version,");
		sqlBuffer
		.append(" to_char(s.created_on, 'MON-DD-YYYY') created_on,");
		sqlBuffer
		.append(" (select NVL(TO_CHAR(max(submission_status_date), 'MON-DD-YYYY'),'') from er_submission_status where fk_submission = sub.pk_submission and fk_submission_board = st.fk_submission_board and submission_status = (select pk_codelst from er_codelst where codelst_type = 'subm_status' and codelst_subtyp = 'submitted' ) ) submitted_date,");
		sqlBuffer
		.append(" (select NVL(TO_CHAR(max(submission_status_date), 'MON-DD-YYYY'),'') from er_submission_status where fk_submission = sub.pk_submission and fk_submission_board = st.fk_submission_board and submission_status = (select pk_codelst from er_codelst where codelst_type = 'subm_status' and codelst_subtyp ='approved' ) ) approved_date,");
		sqlBuffer
		.append(" (SELECT decode(codelst_subtyp, 'approved' , (ROUND(sysdate - st.submission_status_date)), (ROUND(sysdate - s.created_on))) FROM er_codelst WHERE pk_codelst = st.submission_status) || ' days' total_elapsed_days,");
		sqlBuffer.append("  pk_submission_status, st.submission_notes  ");

		sqlBuffer
				.append("  from er_submission sub,er_submission_board sb,er_study s,er_submission_status st");
		sqlBuffer
				.append(" where sub.fk_study = s.pk_study and sub.pk_submission = sb.fk_submission and st.fk_submission_board = sb.pk_submission_board and is_current = 1 ");
		sqlBuffer
				.append("  and fk_review_board in  (select pk_review_board from er_review_board erb where erb.fk_account = "
						+ accountId + ")");
		sqlBuffer
		.append("  and st.submission_status in (" + pk_statuses+ ")");
		/*if (StringUtil.stringToInteger(reviewType) > 0) {
			sqlBuffer.append("  and submission_review_type = " + reviewType);
		}
		if (StringUtil.stringToInteger(submissionType) > 0) {
			sqlBuffer.append("  and submission_type = " + submissionType);
		}*/
		sqlBuffer
		.append(" AND st.submission_status_date = (select max(submission_status_date) from er_submission_status sti, er_submission_board sbi where sti.fk_submission = sub.pk_submission and sti.fk_submission_board = sbi.pk_submission_board and sbi.fk_review_board       IN"
				+ "  (SELECT pk_review_board"
				+ "  FROM er_review_board erb"
				+ "	  WHERE erb.fk_account = "+ accountId
				+ "  )) ");
		sqlBuffer
				.append("  and s.fk_account = "
						+ accountId
						+ "  and  ( exists ( select * from er_studyteam t where t.fk_study = s.pk_study and ");
		sqlBuffer
				.append("  t.fk_user = "
						+ userId
						+ " and  nvl(t.study_team_usr_type,'D')='D') or pkg_superuser.F_Is_Superuser("
						+ userId + ", pk_study) = 1    ) ");

		ArrayList<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> dataMap = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		if("Y".equals(newApplication))
		{
			fetchStdSubmissionDataNewApp(paramMap, maplist);
		}
		try {
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(sqlBuffer.toString());
			rs = pstmt.executeQuery();

			while (rs.next()) {
				dataMap = new HashMap<String, Object>();
				dataMap.put("rowcount", rs.getString(("rowcount")));
				dataMap.put("submission_status_desc", rs
						.getString("submission_status_desc"));
				dataMap.put("colorcode",VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE, "subm_status."+rs.getString("submission_status_subtype")+".colorcode"));				
				
				dataMap.put("study_number", rs.getString(("study_number")));
				dataMap.put("study_title", rs.getString("study_title"));
				dataMap.put("submission_type_desc", rs
						.getString("submission_type_desc"));
				dataMap
						.put("review_type_desc", rs
								.getString("review_type_desc"));
				dataMap
				.put("submission_board_name", rs
						.getString("submission_board_name"));
				dataMap
				.put("submission_status_date", rs
						.getString("submission_status_date"));
				dataMap
				.put("current_version", rs
						.getString("current_version"));
				dataMap
				.put("submission_since", rs
						.getString("submission_since"));
				dataMap
				.put("created_on", rs
						.getString("created_on"));
				dataMap
				.put("submitted_date", rs
						.getString("submitted_date"));
				dataMap
				.put("approved_date", rs
						.getString("approved_date"));
				dataMap
				.put("total_elapsed_days", rs
						.getString("total_elapsed_days"));
				
				dataMap
				.put("checkboxPlaceHolder", "");
				
				dataMap.put("pk_study", rs
						.getString("pk_study"));
				dataMap.put("submission_type_subtype", rs
						.getString("submission_type_subtype"));
				dataMap.put("submission_type_desc", rs
						.getString("submission_type_desc"));

				maplist.add(dataMap);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return maplist;
	}
	
	public ArrayList<HashMap<String, Object>> fetchStdSubmissionDataNewApp(
			HashMap<String, String> paramMap, ArrayList<HashMap<String, Object>> maplist) {

        String accId=(String)paramMap.get("accountId");
        String user =(String)paramMap.get("userId");
        String group = (String)paramMap.get("group");
        String submissionType = (String)paramMap.get("submissionType");
        String revType = (String)paramMap.get("revType");
        String searchCriteria = (String)paramMap.get("searchCriteria");

        StringBuffer sqlBuffer = new StringBuffer();
        sqlBuffer.append(" select c.codelst_seq,mainq.* from ( ");
        sqlBuffer.append(" select s.rowid rowcount, s.pk_study, s.study_title, s.study_number, ");
        sqlBuffer.append(" F_CODELST_DESC((select F_CODELST_ID('submission', 'new_app') from dual)) submission_type_desc, ");
        sqlBuffer.append(" null submission_status_desc, ");
        sqlBuffer.append(" null submission_meeting_date, ");
        sqlBuffer.append(" null submission_board_name, ");
        sqlBuffer.append(" (select F_CODELST_ID('submission','new_app') from dual) SUBMISSION_TYPE, ");
        sqlBuffer.append(" null SUBMISSION_STATUS, ");
        sqlBuffer.append(" null SUBMISSION_REVIEW_TYPE, ");
        sqlBuffer.append(" null FK_REVIEW_BOARD, ");
        sqlBuffer.append(" null ACTION_LINK, ");
        sqlBuffer.append(" null PK_SUBMISSION, ");
        sqlBuffer.append(" null PK_SUBMISSION_BOARD, ");
        sqlBuffer.append(" 'new_app' SUBMISSION_TYPE_SUBTYPE, ");
        sqlBuffer.append(" null fk_formlib, ");
        sqlBuffer.append(" null lf_entrychar, ");
        sqlBuffer
		.append(" to_char(s.created_on, 'MON-DD-YYYY') created_on,");
        sqlBuffer.append("  (round(sysdate - s.created_on)) || ' days' submission_since, ");
        sqlBuffer.append(" null SAVEDCOUNT ");
        sqlBuffer.append(" from er_study s ");
        sqlBuffer.append(" where s.study_creation_type = 'A' and s.fk_account = ").append(accId).append("  and  ");
        sqlBuffer.append(" ( exists ( select * from er_studyteam t where t.fk_study = s.pk_study and ");
        sqlBuffer.append(" t.fk_user = ").append(user).append(" and  nvl(t.study_team_usr_type,'D')='D') or ");
        sqlBuffer.append(" pkg_superuser.F_Is_Superuser(").append(user).append(", pk_study) = 1    ) ");
        sqlBuffer.append(" and exists ( ");
        sqlBuffer.append("         select pk_review_board from er_review_board erb where fk_account = ").append(accId);
        sqlBuffer.append("         and pk_review_board not in ( ");
        sqlBuffer.append("           select sb.fk_review_board from er_submission_board sb ");
        sqlBuffer.append("           inner join er_submission sub on (sb.fk_submission = sub.pk_submission and sub.submission_flag = 1) ");
        sqlBuffer.append("           where sub.fk_study = s.pk_study ");
        sqlBuffer.append("         ) ");
        sqlBuffer.append("       ) ");
        sqlBuffer.append(" and not exists ( ");
        sqlBuffer.append("     select pk_studystat from er_studystat stat ");
        sqlBuffer.append("     where stat.fk_study = s.pk_study and ( ");
        sqlBuffer.append("     stat.fk_codelst_studystat = (select F_CODELST_ID ('studystat', (select CTRL_VALUE from ER_CTRLTAB where CTRL_KEY = 'irb_app_stat' and rownum<=1)) from dual ) OR ");
        sqlBuffer.append("     stat.fk_codelst_studystat = (select F_CODELST_ID ('studystat', 'active') from dual ) ) ");
        sqlBuffer.append("    ) ");
        sqlBuffer.append(" and not exists (select 1 from er_submission su ");
        sqlBuffer.append("     inner join er_submission_status ss on su.pk_submission = ss.fk_submission ");
        sqlBuffer.append("     where fk_study = s.pk_study and ss.is_current = 1 ");
        sqlBuffer.append("     and ss.fk_submission_board is null ) ");
        sqlBuffer.append(" UNION ALL ");
        sqlBuffer.append(" select s.rowid rowcount, s.pk_study, s.study_title, s.study_number, ");
        sqlBuffer.append(" CASE (lf.lf_submission_type) ");
        sqlBuffer.append(" WHEN 'irb_ongo_amd'  THEN F_CODELST_DESC((select F_CODELST_ID('submission', 'study_amend') from dual)) ");
        sqlBuffer.append(" WHEN 'irb_ongo_prob' THEN F_CODELST_DESC((select F_CODELST_ID('submission', 'prob_rpt') from dual)) ");
        sqlBuffer.append(" WHEN 'irb_ongo_cont' THEN F_CODELST_DESC((select F_CODELST_ID('submission', 'cont_rev') from dual)) ");
        sqlBuffer.append(" WHEN 'irb_ongo_clos' THEN F_CODELST_DESC((select F_CODELST_ID('submission', 'closure') from dual)) ");
        sqlBuffer.append(" ELSE null ");
        sqlBuffer.append(" END submission_type_desc, ");
        sqlBuffer.append(" null submission_status_desc, ");
        sqlBuffer.append(" null submission_meeting_date, ");
        sqlBuffer.append(" null submission_board_name, ");
        sqlBuffer.append(" CASE (lf.lf_submission_type) ");
        sqlBuffer.append(" WHEN 'irb_ongo_amd'  THEN (select F_CODELST_ID ('submission', 'study_amend') from dual) ");
        sqlBuffer.append(" WHEN 'irb_ongo_prob' THEN (select F_CODELST_ID ('submission', 'prob_rpt') from dual) ");
        sqlBuffer.append(" WHEN 'irb_ongo_cont' THEN (select F_CODELST_ID ('submission', 'cont_rev') from dual) ");
        sqlBuffer.append(" WHEN 'irb_ongo_clos' THEN (select F_CODELST_ID ('submission', 'closure') from dual) ");
        sqlBuffer.append(" ELSE null ");
        sqlBuffer.append(" END submission_type, ");
        sqlBuffer.append(" null SUBMISSION_STATUS, ");
        sqlBuffer.append(" null SUBMISSION_REVIEW_TYPE, ");
        sqlBuffer.append(" null FK_REVIEW_BOARD, ");
        sqlBuffer.append(" null ACTION_LINK, ");
        sqlBuffer.append(" null PK_SUBMISSION, ");
        sqlBuffer.append(" null PK_SUBMISSION_BOARD, ");
        sqlBuffer.append(" lf.lf_submission_type SUBMISSION_TYPE_SUBTYPE, ");
        sqlBuffer.append(" sf.fk_formlib, ");
        sqlBuffer.append(" lf.lf_entrychar, ");
        sqlBuffer
		.append(" to_char(s.created_on, 'MON-DD-YYYY') created_on,");
        sqlBuffer.append("  (round(sysdate - s.created_on)) || ' days' submission_since, ");
        sqlBuffer.append(" (SELECT COUNT(*) FROM ER_STUDYFORMS a WHERE sf.fk_formlib = a.fk_formlib and ");
        sqlBuffer.append(" a.FK_STUDY = s.pk_study and a.RECORD_TYPE <> 'D') SAVEDCOUNT ");
        sqlBuffer.append(" from er_study s ");
        sqlBuffer.append(" inner join er_studyforms sf ");
        sqlBuffer.append(" on sf.fk_study = s.pk_study ");
        sqlBuffer.append(" inner join er_linkedforms lf ");
        sqlBuffer.append(" on sf.fk_formlib = lf.fk_formlib ");
        sqlBuffer.append(" where s.fk_account = ").append(accId).append("  and  ");
        sqlBuffer.append("      lf.fk_account = ").append(accId).append("  and  ");
        sqlBuffer.append(" ( exists ( select * from er_studyteam t where t.fk_study = s.pk_study and ");
        sqlBuffer.append(" t.fk_user = ").append(user).append(" and  nvl(t.study_team_usr_type,'D')='D') or ");
        sqlBuffer.append(" pkg_superuser.F_Is_Superuser(").append(user).append(", pk_study) = 1    ) ");
        sqlBuffer.append(" and exists ( ");
        sqlBuffer.append("     select pk_studystat from er_studystat stat ");
        sqlBuffer.append("     where stat.fk_study = s.pk_study and  ( ");
        sqlBuffer.append("      stat.fk_codelst_studystat in ( ");
        sqlBuffer.append("       select pk_codelst from ER_CODELST where "); 
        sqlBuffer.append("       CODELST_TYPE = 'studystat'  and CODELST_SUBTYP = ");
        sqlBuffer.append("       (select CTRL_VALUE from ER_CTRLTAB where CTRL_KEY = 'irb_app_stat' and rownum<=1) ");
        sqlBuffer.append("       ) ");
        sqlBuffer.append("      OR stat.fk_codelst_studystat = (select F_CODELST_ID ('studystat','active') from dual) ");
        sqlBuffer.append("     ) )  ");
        sqlBuffer.append(" and lf.lf_submission_type in ");
        sqlBuffer.append("     ('irb_ongo_amd','irb_ongo_prob','irb_ongo_cont','irb_ongo_clos') ");
        sqlBuffer.append(" and ( EXISTS( select 1 from er_studyforms sf1 where ");
        sqlBuffer.append("       (select F_CODELST_ID ('fillformstat','complete') from dual) <>  sf1.form_completed and "); 
        sqlBuffer.append("       sf1.fk_formlib = sf.fk_formlib and sf1.fk_study = sf.fk_study) ");
        sqlBuffer.append("     and sf.pk_studyforms = (select F_GET_LATEST_FSTAT(s.pk_study, sf.fk_formlib) from dual) ) ");
        sqlBuffer.append(" and not exists (select 1 from er_submission su ");
        sqlBuffer.append("     inner join er_submission_status ss on su.pk_submission = ss.fk_submission ");
        sqlBuffer.append("     where fk_study = s.pk_study and ss.is_current = 1 ");
        sqlBuffer.append("     and ss.fk_submission_board is null ) ");
        sqlBuffer.append(" ) mainq inner join er_codelst c on submission_type = c.pk_codelst ");
        sqlBuffer.append(" where 1 = 1 ");
        if (EJBUtil.stringToNum(revType)>0) {
            sqlBuffer.append("  and submission_review_type = " + revType);
        }
        if (EJBUtil.stringToNum(submissionType)>0) {
            sqlBuffer.append("  and submission_type = " + submissionType);
        }
        if (searchCriteria != null) {
            sqlBuffer.append("  and (lower(study_title) like lower('%").append(searchCriteria.replaceAll("'", "''")).append("%') or ");
            sqlBuffer.append("  lower(study_number) like lower('%").append(searchCriteria.replaceAll("'", "''")).append("%') ) ");
        }
        sqlBuffer.append(" order by codelst_seq, study_number ");
        
        HashMap<String, Object> dataMap = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;

		try {
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(sqlBuffer.toString());
			rs = pstmt.executeQuery();

			while (rs.next()) {
				dataMap = new HashMap<String, Object>();
				dataMap.put("rowcount", rs.getString(("rowcount")));
				dataMap.put("submission_status_desc", VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE, "study.savedItems"));
				dataMap.put("colorcode",VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE, "subm_status.savedItems.colorcode"));				
				
				dataMap.put("study_number", rs.getString("study_number"));
				dataMap.put("study_title", rs.getString("study_title"));
				dataMap.put("submission_type_desc", rs
						.getString(("submission_type_desc")));
				dataMap
						.put("review_type_desc", "");
				dataMap
				.put("submission_board_name", rs
						.getString("submission_board_name"));
				dataMap
				.put("submission_status_date", rs
						.getString("created_on"));
				dataMap
				.put("current_version", "");
				dataMap
				.put("submission_since", rs
						.getString(("submission_since")));
				dataMap
				.put("created_on", rs
						.getString("created_on"));
				dataMap
				.put("submitted_date", "");
				dataMap
				.put("approved_date", "");
				dataMap
				.put("total_elapsed_days", rs
						.getString(("submission_since")));
				
				dataMap
				.put("checkboxPlaceHolder", "");
				
				dataMap.put("pk_study", rs
						.getString(("pk_study")));
				dataMap.put("submission_type_subtype", rs
						.getString(("submission_type_subtype")));
				dataMap.put("submission_type_desc", rs
						.getString(("submission_type_desc")));

				maplist.add(dataMap);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
        return maplist;
    
	}
}