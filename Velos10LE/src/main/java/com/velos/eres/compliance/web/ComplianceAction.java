package com.velos.eres.compliance.web;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.velos.eres.service.util.CFG;
import com.velos.eres.service.util.StringUtil;

public class ComplianceAction extends ActionSupport {

	private static final long serialVersionUID = 3119016483759522560L;
	public static final String dashboardHome = "dashboardHome";
	private static final String STUDY_CHECK_N_SUBMIT_TILE = "studyCheckNSubmitTile";
	private HttpServletRequest request = null;
	private HttpSession tSession = null;
	private ArrayList<HashMap<String, Object>> jsonData = null;
	private Integer userId = null;
	private Integer accountId = null;
	private String pk_statuses = null;
	private String newApplication = "N";
	private String organization = "";
	private Integer group = null;
	private ComplianceJB complianceJB = null;
	private static final String AMENDMENTS_TILE = "amendmentsTile";
	private static final String STUDY_ATTACHMENTS_TILE = "studyAttachmentsTile";
	private static final String STUDY_ATTACHMENTS_LIND_FT_TILE = "studyAttachmentsTileLindFt";
	private static final String APPROVED_VERSION_TILE = "approvedVersionTile";
	
	public ComplianceJB getComplianceJB() {
		return complianceJB;
	}

	public void setComplianceJB(ComplianceJB complianceJB) {
		this.complianceJB = complianceJB;
	}

	public void setJsonData(ArrayList<HashMap<String, Object>> jsonData) {
		this.jsonData = jsonData;
	}

	public ArrayList<HashMap<String, Object>> getJsonData() {
		return jsonData;
	}

	public ComplianceAction() {
		ActionContext ac = ActionContext.getContext();
		request = (HttpServletRequest) ac
				.get(ServletActionContext.HTTP_REQUEST);
		tSession = request.getSession(false);
		
		complianceJB = new ComplianceJB();

		String statuses[] = request.getParameterValues("statues");
		if (statuses != null) {
			StringBuilder pk_statusesBuilder = new StringBuilder();
			String delim = "";
			for(String status : statuses) {
					pk_statusesBuilder.append(delim);
					pk_statusesBuilder.append(status);
					delim = ",";
				
			}
			pk_statuses = pk_statusesBuilder.toString();
		}
		
		userId = StringUtil.stringToInteger((String) tSession
				.getAttribute("userId"));
		accountId = StringUtil.stringToInteger((String) tSession
				.getAttribute("accountId"));
		group = StringUtil.stringToInteger((String) tSession
				.getAttribute("defUserGroup"));
		
		organization = CFG.EIRB_DEFAULT_STUDY_STAT_ORG;
		
	}

	public String fetchStdSubmissionData() {
		HashMap<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("userId", StringUtil.integerToString(userId));
		paramMap.put("accountId", StringUtil.integerToString(accountId));
		paramMap.put("group", StringUtil.integerToString(group));
		paramMap.put("pk_statuses", pk_statuses);
		paramMap.put("newApplication", newApplication);
		paramMap.put("organization", organization);
		setJsonData(complianceJB.fetchStdSubmissionData(paramMap));
		return SUCCESS;
	}
	
	public String getStudyCheckNSubmit() {
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userId", userId);
		paramMap.put("accountId", accountId);
		int pkStudy = StringUtil.stringToNum((String)request.getParameter("studyId"));
		paramMap.put("studyId", pkStudy);
		complianceJB.retrieveBlockSubmissionFlag(paramMap);
		return STUDY_CHECK_N_SUBMIT_TILE;
	}
	
	public static String getAmendmentsTile() {
		return AMENDMENTS_TILE;
	}
	
	public String getStudyAttachments() {
		return STUDY_ATTACHMENTS_TILE;
	}	
	
	public static String getStudyAttachmentsLindFt() {
		return STUDY_ATTACHMENTS_LIND_FT_TILE;
	}

	public String getApprovedVersionTile() {
		return APPROVED_VERSION_TILE;
	}

}
