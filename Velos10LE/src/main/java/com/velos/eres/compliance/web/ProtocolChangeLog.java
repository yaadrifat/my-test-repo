package com.velos.eres.compliance.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.LC;
import com.velos.eres.widget.business.common.UIFlxPageDao;
import com.velos.eres.widget.service.util.FlxPageFields;

public class ProtocolChangeLog {

	public String getAllProtocolVersions(int studyId, String grpId) {
		StringBuilder allVersions = new StringBuilder();
		UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
		ArrayList<HashMap<String, String>> resList = uiFlxPageDao
				.getAllVersionsForProtocol("er_study", studyId);

		allVersions
				.append("<table class='TFtable' width='100%' cellspacing='4' cellpadding='4' border='1'>");
		allVersions.append("<tr><th>" + LC.Version_Header + "</th><th>"
				+ LC.Change_Log_Justification + "</th>" + " <th>"
				+ LC.Submission_Type_Description + "</th>" + " </tr>");

		int totalVersions = resList.size();
		for (HashMap<String, String> resMap : resList) {
			String pageVer = resMap.get("page_ver");
			String pageMinorVer = resMap.get("page_minor_ver");
			String submissionTypeDesc = resMap.get("submissionTypeDesc");
			String fullVer = pageVer;
			if (!"0".equals(pageMinorVer)) {
				fullVer += "." + pageMinorVer;
			}
			String versionSummary = "studyVersionDetails.jsp?studyId="
					+ studyId + "&pageVer=" + pageVer + "&pageMinorVer="
					+ pageMinorVer;
			String versionChangeLog = "studyVersionChangeLog.jsp?studyId="
					+ studyId + "&pageVer=" + pageVer + "&pageMinorVer="
					+ pageMinorVer;
			allVersions.append("<tr>");
			/*
			 * allVersions.append("<td>" +
			 * "<A href='studyVersionDetails.jsp?studyId="
			 * +studyId+"&pageVer="+pageVer
			 * +"&pageMinorVer="+pageMinorVer+"'>"+"Version " + fullVer +"</A>"
			 * + "</td>");
			 */
			allVersions.append("<td>"
					+ "<A href='javascript:f_open_versionDetails(&quot;"
					+ versionSummary + "&quot;);'>" + "Version " + fullVer
					+ "</A>" + "</td>");

			if (totalVersions == 1) {
				allVersions.append("<td>" + "NA" + "</td>");
			} else {
				allVersions.append("<td>"
						+ "<A href='javascript:f_open_versionChangeLog(&quot;"
						+ versionChangeLog + "&quot;);'>" + "Change Log"
						+ "</A>" + "</td>");
			}

			allVersions.append("<td>" + submissionTypeDesc + "</td>");
			allVersions.append("</tr>");
			totalVersions--;
		}
		allVersions.append("</table>");
		return allVersions.toString();
	}

	public JSONArray fetchProtocolVersionDiff(int studyId, String grpId) {
		UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();

		int versionNum = uiFlxPageDao.getHighestFlexPageVersion("er_study",
				studyId);
		int minorVerNum = uiFlxPageDao.getHighestMinorFlexPageVersion(
				versionNum, "er_study", studyId);
		int versionNumOld = -1;
		int minorVerNumOld = -1;

		if (minorVerNum > 1) {
			minorVerNumOld = minorVerNum - 1;
			versionNumOld = versionNum;
		} else {
			versionNumOld = versionNum - 1;
			minorVerNumOld = uiFlxPageDao.getHighestMinorFlexPageVersion(
					versionNumOld, "er_study", studyId);
		}

		return fetchProtocolVersionDetails(
				versionNumOld + "." + minorVerNumOld, versionNum + "."
						+ minorVerNum, studyId, grpId);
	}

	public String fetchProtocolVersionDiffForVersion(int versionNum,
			int minorVerNum, int studyId, String grpId) {
		UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
		CodeDao codeDao = new CodeDao();
		if (versionNum == 0) {
			versionNum = uiFlxPageDao.getHighestFlexPageVersion("er_study",
					studyId);
			minorVerNum = uiFlxPageDao.getHighestMinorFlexPageVersion(
					versionNum, "er_study", studyId);
		}

		StringBuilder changeVersion = new StringBuilder();

		changeVersion
				.append("<table class='TFtable' width='100%' cellspacing='4' cellpadding='4' border='1'>");
		changeVersion.append("<tr><th>" + LC.Change_Version_Title + "</th><th>"
				+ LC.Change_Version_Field_Name + "</th>" + " <th>"
				+ LC.Change_Version_Old_Value + "</th>" + " <th>"
				+ LC.Change_Version_New_Value + "</th>" +  " <th>"
				+ LC.Change_Version_Reason_for_Change + "</th>"	+ " <th>"
				+ LC.Change_Version_Review_Board + "</th></tr>");

		String verDiff = uiFlxPageDao.getFlexPageVersionDiffDetails("er_study",
				studyId, versionNum, minorVerNum);
		if(EJBUtil.isEmpty(verDiff))
		{
			return LC.Change_Version_No_Diff_Message;
		}
		try {
			JSONArray versionDiffList = new JSONArray(verDiff);
			int noOfFields = versionDiffList.length();
			if(noOfFields == 0){
				return LC.Change_Version_No_Diff_Message;
			}
			for (int i = 0; i < noOfFields; i++) {
				changeVersion
				.append("<tr>");
				changeVersion
				.append("<td>" + versionDiffList.getJSONObject(i).get("title") + "</td>");
				changeVersion
				.append("<td>" + versionDiffList.getJSONObject(i).get("fieldName") + "</td>");
				changeVersion
				.append("<td>" + versionDiffList.getJSONObject(i).get("oldValue") + "</td>");
				changeVersion
				.append("<td>" + versionDiffList.getJSONObject(i).get("newValue") + "</td>");
				changeVersion
				.append("<td>" + versionDiffList.getJSONObject(i).get("notes") + "</td>");
				changeVersion
				.append("<td>" + codeDao.getCodeDescription(Integer.parseInt((String) versionDiffList.getJSONObject(i).get("revBoard"))) + "</td>");
				changeVersion
				.append("</tr>");
			}
		} catch (JSONException e) {
			return LC.Change_Version_No_Diff_Message;
		}

		changeVersion.append("</table>");
		return changeVersion.toString();
	}

	public String returnProtocolVersionDiff(String versionOld,
			String versionNew, int studyId, String grpId) {
		UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();

		int versionNum = uiFlxPageDao.getHighestFlexPageVersion("er_study",
				studyId);
		int minorVerNum = uiFlxPageDao.getHighestMinorFlexPageVersion(
				versionNum, "er_study", studyId);
		int versionNumOld = -1;
		int minorVerNumOld = -1;

		if (minorVerNum > 1) {
			minorVerNumOld = minorVerNum - 1;
			versionNumOld = versionNum;
		} else {
			versionNumOld = versionNum - 1;
			minorVerNumOld = uiFlxPageDao.getHighestMinorFlexPageVersion(
					versionNumOld, "er_study", studyId);
		}

		JSONArray versionDiffList = fetchProtocolVersionDetails(versionNumOld
				+ "." + minorVerNumOld, versionNum + "." + minorVerNum,
				studyId, grpId);
		StringBuilder changeVersion = new StringBuilder();
		int noOfRows = versionDiffList.length();
		changeVersion
				.append("<table class='TFtable' width='90%' cellspacing='4' cellpadding='4' border='1'>");
		changeVersion
				.append("<tr><th>"
						+ LC.Change_Version_Title
						+ "</th><th>"
						+ LC.Change_Version_Field_Name
						+ "</th>"
						+ " <th>"
						+ LC.Change_Version_Old_Value
						+ "</th>"
						+ " <th>"
						+ LC.Change_Version_New_Value
						+ "</th>"
						+ " <th>"
						+ LC.Change_Version_Reason_for_Change
						+ "<FONT class='Mandatory' id='pgcustompatid'>*  </FONT>  </th>"
						+ " <th>"
						+ LC.Change_Version_Review_Board
						+ "<FONT class='Mandatory' id='pgcustompatid'>*  </FONT>  </th>"
						+ " </tr>");

		for (int i = 0; i < noOfRows; i++) {
			try {
				JSONObject fieldDetails = versionDiffList.getJSONObject(i);
				changeVersion.append(fieldDetails.get("fieldRow"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		changeVersion.append("</table>");
		return changeVersion.toString();
	}

	public JSONArray fetchProtocolVersionDetails(String versionOld,
			String versionNew, int studyId, String grpId) {
		UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();

		// Old Version Details
		HashMap<String, HashMap<String, String>> oldVersionFields = new HashMap<String, HashMap<String, String>>();

		oldVersionFields = getProtocolVersion(studyId, versionOld);

		// New Version Details
		HashMap<String, HashMap<String, String>> newVersionFields = new HashMap<String, HashMap<String, String>>();

		newVersionFields = getProtocolVersion(studyId, versionNew);

		// Comparing the versions and extracting the difference between the two
		// versions.
		// List<HashMap<String, String>> versionDiffList = new
		// ArrayList<HashMap<String, String>>();
		List<JSONObject> versionDiffList = new ArrayList<JSONObject>();
		JSONArray list = new JSONArray();

		HashSet<String> fields = new HashSet<String>();

		Iterator i = oldVersionFields.keySet().iterator();
		CodeDao cdReviewBoard = new CodeDao();

		cdReviewBoard.getCodeValues("rev_board");
		cdReviewBoard.setCType("rev_board");
		cdReviewBoard.setForGroup(grpId);

		String oldVersionKey = "";
		StringBuilder fldHTML = new StringBuilder();
		HashSet<String> versionDiff = new HashSet<String>();
		try {
			while (i.hasNext()) {
				oldVersionKey = (String) i.next();
				// HashMap<String, String> versionDiffMap = new HashMap<String,
				// String>();
				JSONObject versionDiffMap = new JSONObject();
				if (!isEmpty(oldVersionFields.get(oldVersionKey))) {
					String fieldLabel = oldVersionFields.get(oldVersionKey)
							.get("label").replaceAll(" ", "__");
					versionDiff.add(oldVersionKey);
					if (newVersionFields.containsKey(oldVersionKey)) {
						if (!isEqual(oldVersionFields.get(oldVersionKey),
								newVersionFields.get(oldVersionKey))) {
							versionDiffMap.put("field", oldVersionKey);
							versionDiffMap.put("fieldName", oldVersionFields
									.get(oldVersionKey).get("label"));
							versionDiffMap.put("oldValue", oldVersionFields
									.get(oldVersionKey).get("data"));
							versionDiffMap.put("newValue", newVersionFields
									.get(oldVersionKey).get("data"));
							versionDiffMap.put(
									"title",
									oldVersionFields.get(oldVersionKey).get(
											"title"));
							versionDiffMap.put("fieldLabel", fieldLabel);
							versionDiffMap
									.put("fieldRow",
											"<tr>" + "<td>"
													+ oldVersionFields.get(
															oldVersionKey).get(
															"title")
													+ "</td>"
													+ "<td>"
													+ oldVersionFields.get(
															oldVersionKey).get(
															"label")
													+ "</td>"
													+ "<td>"
													+ oldVersionFields.get(
															oldVersionKey).get(
															"data")
													+ "</td>"
													+ "<td>"
													+ newVersionFields.get(
															oldVersionKey).get(
															"data")
													+ "</td>"
													+ "<td>"
													+ "<textarea type='text' id='"
													+ fieldLabel
													+ "_"
													+ "notes"
													+ "' "
													+ "name='"
													+ fieldLabel
													+ "_"
													+ "notes"
													+ "' "
													+ "rows='2' maxlength='50'></textarea>"
													+ "</td>"
													+ "<td>"
													+ cdReviewBoard
															.toPullDown(fieldLabel
																	+ "_"
																	+ "revBoard")
													+ "</td>" + "</tr>");

							versionDiffList.add(versionDiffMap);
							list.put(versionDiffMap);
							/*
							 * fldHTML.append("<tr>" + "<td>" +
							 * oldVersionFields.get(oldVersionKey).get("title")
							 * + "</td>" + "<td>" +
							 * oldVersionFields.get(oldVersionKey).get("label")
							 * + "</td>" + "<td>" +
							 * oldVersionFields.get(oldVersionKey).get("data") +
							 * "</td>" + "<td>" +
							 * newVersionFields.get(oldVersionKey).get("data") +
							 * "</td>" + "<td>" + "<textarea type='text' id='"+
							 * fieldLabel +"_"+ "notes" +"' "+ "name='"+
							 * fieldLabel +"_"+ "notes" +"' " +
							 * "rows='2' maxlength='50'></textarea>" + "</td>" +
							 * "<td>" + cdReviewBoard.toPullDown(fieldLabel +"_"
							 * +"revBoard") + "</td>" + "</tr>");
							 */

						}
					} else {
						versionDiffMap.put("field", oldVersionKey);
						versionDiffMap.put("fieldName",
								newVersionFields.get(oldVersionKey)
										.get("label"));
						versionDiffMap.put("oldValue", "");
						versionDiffMap
								.put("newValue",
										newVersionFields.get(oldVersionKey)
												.get("data"));
						versionDiffMap.put("title",
								newVersionFields.get(oldVersionKey)
										.get("title"));
						versionDiffMap.put("fieldLabel", fieldLabel);
						versionDiffMap.put(
								"fieldRow",
								"<tr>" + "<td>"
										+ oldVersionFields.get(oldVersionKey)
												.get("title")
										+ "</td>"
										+ "<td>"
										+ oldVersionFields.get(oldVersionKey)
												.get("label")
										+ "</td>"
										+ "<td>"
										+ oldVersionFields.get(oldVersionKey)
												.get("data")
										+ "</td>"
										+ "<td>"
										+ ""
										+ "</td>"
										+ "<td>"
										+ "<textarea type='text' id='"
										+ fieldLabel
										+ "_"
										+ "notes"
										+ "' "
										+ "name='"
										+ fieldLabel
										+ "_"
										+ "notes"
										+ "' "
										+ "rows='2' maxlength='50'></textarea>"
										+ "</td>"
										+ "<td>"
										+ cdReviewBoard.toPullDown(fieldLabel
												+ "_" + "revBoard") + "</td>"
										+ "</tr>");
						versionDiffList.add(versionDiffMap);
						list.put(versionDiffMap);
						/*
						 * fldHTML.append("<tr>" + "<td>" +
						 * oldVersionFields.get(oldVersionKey).get("title") +
						 * "</td>" + "<td>" +
						 * oldVersionFields.get(oldVersionKey).get("label") +
						 * "</td>" + "<td>" +
						 * oldVersionFields.get(oldVersionKey).get("data") +
						 * "</td>" + "<td>" + "" + "</td>" + "<td>" +
						 * "<textarea type='text' id='"+ fieldLabel +"_"+
						 * "notes" +"' "+ "name='"+ fieldLabel +"_"+ "notes"
						 * +"' " + "rows='2' maxlength='50'></textarea>" +
						 * "</td>" + "<td>" +
						 * cdReviewBoard.toPullDown(fieldLabel +"_" +"revBoard")
						 * + "</td>" + "</tr>");
						 */
					}
				}

			}

			i = newVersionFields.keySet().iterator();

			String newVersionKey = "";

			while (i.hasNext()) {
				newVersionKey = (String) i.next();
				if (!isEmpty(newVersionFields.get(newVersionKey))) {
					if (!versionDiff.contains(newVersionKey)) {
						// HashMap<String, String> versionDiffMap = new
						// HashMap<String, String>();
						JSONObject versionDiffMap = new JSONObject();
						versionDiff.add(newVersionKey);
						String fieldLabel = newVersionFields.get(newVersionKey)
								.get("label").replaceAll(" ", "__");
						if (oldVersionFields.containsKey(newVersionKey)) {
							if (!isEqual(oldVersionFields.get(newVersionKey),
									newVersionFields.get(newVersionKey))) {
								versionDiffMap.put("field", newVersionKey);
								versionDiffMap.put("fieldName",
										newVersionFields.get(newVersionKey)
												.get("label"));
								versionDiffMap.put("oldValue", oldVersionFields
										.get(newVersionKey).get("data"));
								versionDiffMap.put("newValue", newVersionFields
										.get(newVersionKey).get("data"));
								versionDiffMap.put("title", newVersionFields
										.get(newVersionKey).get("title"));
								versionDiffMap.put("fieldLabel", fieldLabel);
								versionDiffMap
										.put("fieldRow",
												"<tr>" + "<td>"
														+ newVersionFields.get(
																newVersionKey)
																.get("title")
														+ "</td>"
														+ "<td>"
														+ newVersionFields.get(
																newVersionKey)
																.get("label")
														+ "</td>"
														+ "<td>"
														+ oldVersionFields.get(
																newVersionKey)
																.get("data")
														+ "</td>"
														+ "<td>"
														+ newVersionFields.get(
																newVersionKey)
																.get("data")
														+ "</td>"
														+ "<td>"
														+ "<textarea type='text' id='"
														+ fieldLabel
														+ "_"
														+ "notes"
														+ "' "
														+ "name='"
														+ fieldLabel
														+ "_"
														+ "notes"
														+ "' "
														+ "rows='2' maxlength='50'></textarea>"
														+ "</td>"
														+ "<td>"
														+ cdReviewBoard
																.toPullDown(fieldLabel
																		+ "_"
																		+ "revBoard")
														+ "</td>" + "</tr>");

								versionDiffList.add(versionDiffMap);
								list.put(versionDiffMap);
								/*
								 * fldHTML.append("<tr>" + "<td>" +
								 * newVersionFields
								 * .get(newVersionKey).get("title") + "</td>" +
								 * "<td>" +
								 * newVersionFields.get(newVersionKey).get
								 * ("label") + "</td>" + "<td>" +
								 * oldVersionFields
								 * .get(newVersionKey).get("data") + "</td>" +
								 * "<td>" +
								 * newVersionFields.get(newVersionKey).get
								 * ("data") + "</td>" + "<td>" +
								 * "<textarea type='text' id='"+ fieldLabel
								 * +"_"+ "notes" +"' "+ "name='"+ fieldLabel
								 * +"_"+ "notes" +"' " +
								 * "rows='2' maxlength='50'></textarea>" +
								 * "</td>" + "<td>" +
								 * cdReviewBoard.toPullDown(fieldLabel +"_"
								 * +"revBoard") + "</td>" + "</tr>");
								 */
							}
						} else {
							versionDiffMap.put("field", newVersionKey);
							versionDiffMap.put("fieldName", newVersionFields
									.get(newVersionKey).get("label"));
							versionDiffMap.put("oldValue", "");
							versionDiffMap.put("newValue", newVersionFields
									.get(newVersionKey).get("data"));
							versionDiffMap.put(
									"title",
									newVersionFields.get(newVersionKey).get(
											"title"));
							versionDiffMap.put("fieldLabel", fieldLabel);
							versionDiffMap
									.put("fieldRow",
											"<tr>" + "<td>"
													+ newVersionFields.get(
															newVersionKey).get(
															"title")
													+ "</td>"
													+ "<td>"
													+ newVersionFields.get(
															newVersionKey).get(
															"label")
													+ "</td>"
													+ "<td>"
													+ ""
													+ "</td>"
													+ "<td>"
													+ newVersionFields.get(
															newVersionKey).get(
															"data")
													+ "</td>"
													+ "<td>"
													+ "<textarea type='text' id='"
													+ fieldLabel
													+ "_"
													+ "notes"
													+ "' "
													+ "name='"
													+ fieldLabel
													+ "_"
													+ "notes"
													+ "' "
													+ "rows='2' maxlength='50'></textarea>"
													+ "</td>"
													+ "<td>"
													+ cdReviewBoard
															.toPullDown(fieldLabel
																	+ "_"
																	+ "revBoard")
													+ "</td>" + "</tr>");

							versionDiffList.add(versionDiffMap);
							list.put(versionDiffMap);
							/*
							 * fldHTML.append("<tr>" + "<td>" +
							 * newVersionFields.get(newVersionKey).get("title")
							 * + "</td>" + "<td>" +
							 * newVersionFields.get(newVersionKey).get("label")
							 * + "</td>" + "<td>" + "" + "</td>" + "<td>" +
							 * newVersionFields.get(newVersionKey).get("data") +
							 * "</td>" + "<td>" + "<textarea type='text' id='"+
							 * fieldLabel +"_"+ "notes" +"' "+ "name='"+
							 * fieldLabel +"_"+ "notes" +"' " +
							 * "rows='2' maxlength='50'></textarea>" + "</td>" +
							 * "<td>" + cdReviewBoard.toPullDown(fieldLabel +"_"
							 * +"revBoard") + "</td>" + "</tr>");
							 */
						}

					}
				}
			}
		} catch (Exception e) {

		}
		System.out.println("Finish");
		// return versionDiffList;
		return list;
	}

	public HashMap<String, HashMap<String, String>> getProtocolVersion(
			int studyId, String studyVersion) {
		HashMap<String, HashMap<String, String>> versionFields = new HashMap<String, HashMap<String, String>>();

		try {
			UIFlxPageDao uIFlxPageDao = new UIFlxPageDao();
			String json = uIFlxPageDao.getPageVersionDetails("er_study",
					studyId, studyVersion);
			JSONObject result = new JSONObject(json);
			JSONArray sections = result.getJSONArray("sections");

			JSONObject jsonObj = new JSONObject();

			List<JSONArray> listFields = new ArrayList<JSONArray>();

			List<String> listTitle = new ArrayList<String>();
			CodeDao codeDao = new CodeDao();
			int codeId = 0;
			for (int t = 0; t < sections.length(); t++) {

				listFields
						.add(sections.getJSONObject(t).getJSONArray("fields"));
				JSONArray listFieldDetails = sections.getJSONObject(t)
						.getJSONArray("fields");
				for (int i = 0; i < listFieldDetails.length(); i++) {
					HashMap<String, String> fieldDetails = new HashMap<String, String>();
					JSONObject fields = listFieldDetails.getJSONObject(i);
					String type = fields.getString("type");
					if ("STD".equals(type)) {
						Iterator stdField = fields.keys();
						while (stdField.hasNext()) {
							String field = stdField.next().toString();
							if (!"type".equals(field)) {
								fieldDetails.put("data",
										fields.getString(field));
								fieldDetails.put("label", FlxPageFields
										.getFlxPageFieldByKey(field)
										.getFieldLabel());
								fieldDetails.put("title", sections
										.getJSONObject(t).getString("title"));
								versionFields.put(FlxPageFields
										.getFlxPageFieldByKey(field)
										.getFieldKeyword(), fieldDetails);
							}
						}
					} else if ("MSD".equals(type)) {

						fieldDetails.put("data", fields.getString("data"));
						// codeDao.getCodeId("studyidtype",fields.getString("codeSubtype"));
						// fieldDetails.put("label",
						// codeDao.getCodeDescription());
						fieldDetails.put("label", fields.getString("label"));
						fieldDetails.put("title", sections.getJSONObject(t)
								.getString("title"));
						versionFields.put(fields.getString("codeSubtype"),
								fieldDetails);
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return versionFields;
	}

	public boolean isEmpty(HashMap<String, String> fieldsMap) {
		String data = fieldsMap.get("data");
		if ((!(data == null)) && (!(data.trim()).equals(""))
				&& ((data.trim()).length() > 0))
			return false;
		else
			return true;
	}

	public boolean isEqual(HashMap<String, String> oldVersion,
			HashMap<String, String> newVersion) {
		if (oldVersion.get("data").equals(newVersion.get("data"))) {
			return true;
		}
		return false;
	}

	public boolean contains(List<HashMap<String, String>> versionDiffList,
			String key) {
		Iterator i = versionDiffList.iterator();
		while (i.hasNext()) {
			HashMap<String, String> field = (HashMap<String, String>) i.next();
			if (field.containsValue(key))
				return true;
		}
		return false;
	}
}
