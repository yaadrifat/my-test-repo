/*
 * Classname : DashboardAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 07/31/2014
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Ashwani Godara
 */

package com.velos.eres.querymanagement.service;
/* Import Statements */
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


/**
 * The stateless session EJB acting as a facade for the entity CMP -
 * DashboardAgentBean.<br>
 * <br>
 *
 * @author Ashwani Godara
 * @see QueryManagementAgentBean
 * @ejbRemote DashboardAgent
 */
@Stateless
public class QueryManagementAgentBean
  implements QueryManagementAgent
{

  @PersistenceContext(unitName="eres")
  protected EntityManager em;

  @Resource
  private SessionContext context;
  
 
}