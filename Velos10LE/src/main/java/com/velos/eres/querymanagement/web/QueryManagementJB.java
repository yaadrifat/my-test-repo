package com.velos.eres.querymanagement.web;

import java.util.ArrayList;
import java.util.HashMap;

import com.velos.eres.querymanagement.business.QueryManagementDAO;


public class QueryManagementJB {

	public ArrayList<HashMap<String, Object>> fetchFormQueries(
			HashMap<String, String> paramMap, int dispLength, int dispStart,int sortCol,String sortDir) {
		// TODO route through EJB Service
		QueryManagementDAO qmDAO = new QueryManagementDAO();
		return qmDAO.fetchFormQueries(paramMap,dispLength,dispStart,sortCol,sortDir);
	}
	
	public ArrayList<HashMap<String, Object>> fetchAdvEventQueriesData(
			HashMap<String, String> paramMap, int dispLength, int dispStart,int sortCol,String sortDir) {
		// TODO route through EJB Service
		QueryManagementDAO qmDAO = new QueryManagementDAO();
		return qmDAO.fetchAdvEventQueriesData(paramMap,dispLength,dispStart,sortCol,sortDir);
	}
	
	public ArrayList<HashMap<String, Object>> fetchPatStdStatesData(
			HashMap<String, String> paramMap, int dispLength, int dispStart,int sortCol,String sortDir) {
		// TODO route through EJB Service
		QueryManagementDAO qmDAO = new QueryManagementDAO();
		return qmDAO.fetchPatStdStatesData(paramMap,dispLength,dispStart,sortCol,sortDir);
	}
	
	public ArrayList<HashMap<String, Object>> fetchPatDemographicsData(
			HashMap<String, String> paramMap, int dispLength, int dispStart,int sortCol,String sortDir) {
		// TODO route through EJB Service
		QueryManagementDAO qmDAO = new QueryManagementDAO();
		return qmDAO.fetchPatDemographicsData(paramMap,dispLength,dispStart,sortCol,sortDir);
	}
	
	public int updateBulkFormStatusData(
			HashMap<String, String> paramMap) {
		// TODO route through EJB Service
		QueryManagementDAO qmDAO = new QueryManagementDAO();
		return qmDAO.updateBulkFormStatusData(paramMap);
	}
}