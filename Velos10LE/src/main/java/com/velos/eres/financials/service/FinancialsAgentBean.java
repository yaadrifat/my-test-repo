/*
 * Classname : FinancialsAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 12/20/2011
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Ashu Khatkar
 */

package com.velos.eres.financials.service;
/* Import Statements */
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Hashtable;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.json.JSONArray;
import org.json.JSONObject;

import com.velos.eres.business.common.MileAchievedDao;
import com.velos.eres.business.common.MilestoneDao;
import com.velos.eres.business.common.PatientDao;
import com.velos.eres.business.common.StudyDao;
import com.velos.eres.financials.business.common.FinancialsDao;
import com.velos.eres.service.studyRightsAgent.StudyRightsAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.NumberUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.invoice.InvoiceJB;
import com.velos.eres.web.study.StudyJB;
import com.velos.eres.web.user.UserJB;


/**
 * The stateless session EJB acting as a facade for the entity CMP -
 * FinancialsAgentBean.<br>
 * <br>
 *
 * @author Sampada Mhalagi
 * @see FinancialsAgentBean
 * @ejbRemote FinancialsAgent
 */
@Stateless
public class FinancialsAgentBean
  implements FinancialsAgent
{

  @PersistenceContext(unitName="eres")
  protected EntityManager em;

  @Resource
  private SessionContext context;
  
  private static final String STUDYID_KEY = "fk_study";
  private static final String STUDYNUMBER_KEY = "studyNumber";
  private static final String INVOICEABLE_KEY = "invoiceable";
  private static final String INVOICEABLEAMT_KEY = "invoiceableAmount";
  private static final String INVOICED_KEY = "invoiced";
  private static final String INVOICEDAMT_KEY = "invoicedAmount";
  private static final String UNINVOICED_KEY = "uninvoiced";
  private static final String UNINVOICEDAMT_KEY = "uninvoicedAmount";
  private static final String COLLECTED_KEY = "collected";
  private static final String COLLECTEDAMT_KEY = "collectedAmount";
  private static final String OUTSTANDING_KEY = "outstanding";
  private static final String OUTSTANDINGAMT_KEY = "outstandingAmount";
  private static final String RECEIPTS_KEY = "receipts";
  private static final String DISBURSEMENTS_KEY = "disbursements";
  private static final String NETCASH_KEY = "netCash";

	@Override
	public FinancialsDao getMyInvoiceables(String studyIds) {
		FinancialsDao finDao = null;
		try {
			finDao = new FinancialsDao();
	        finDao.getMyInvoiceables(studyIds);
		} catch (Exception e) {
			Rlog.fatal("ctrp", "Exception in FinancialsAgentBean.getMyInvoiceables: "+e);
		}
		return finDao;
	}

	@Override
	public FinancialsDao getMyInvoiced(String studyIds) {
		FinancialsDao finDao = null;
		try {
			finDao = new FinancialsDao();
	        finDao.getMyInvoiced(studyIds);
		} catch (Exception e) {
			Rlog.fatal("ctrp", "Exception in FinancialsAgentBean.getMyInvoiced: "+e);
		}
		return finDao;
	}

	@Override
	public FinancialsDao getMyCollected(String studyIds) {
		FinancialsDao finDao = null;
		try {
			finDao = new FinancialsDao();
	        finDao.getMyCollected(studyIds);
		} catch (Exception e) {
			Rlog.fatal("ctrp", "Exception in FinancialsAgentBean.getMyCollected: "+e);
		}
		return finDao;
	}

	@Override
	public FinancialsDao getMyReceipts(String studyIds) {
		FinancialsDao finDao = null;
		try {
			finDao = new FinancialsDao();
	        finDao.getMyPayments(studyIds, RECEIPTS_KEY);
		} catch (Exception e) {
			Rlog.fatal("ctrp", "Exception in FinancialsAgentBean.getMyCollected: "+e);
		}
		return finDao;
	}

	@Override
	public FinancialsDao getMyDisbursements(String studyIds) {
		FinancialsDao finDao = null;
		try {
			finDao = new FinancialsDao();
	        finDao.getMyPayments(studyIds, DISBURSEMENTS_KEY);
		} catch (Exception e) {
			Rlog.fatal("ctrp", "Exception in FinancialsAgentBean.getMyCollected: "+e);
		}
		return finDao;
	}

	@Override
	public JSONArray getBilling(String accId, String userId, String studyIds) {
		JSONArray myBillingData = new JSONArray();

		StudyJB studyJB1 = new StudyJB();
		StudyDao studyDao1 = studyJB1.getStudyAutoRows(userId, accId, studyIds, 1);
		ArrayList arrStudyIds = studyDao1.getStudyAutoIds();
		if (arrStudyIds == null) { return myBillingData; }

		FinancialsDao finDao = new FinancialsDao(); 
		finDao = getMyInvoiceables(studyIds);

		ArrayList invoiceables = new ArrayList();
		invoiceables = finDao.getInvoiceables();
		ArrayList invoiceableStudies = new ArrayList();
		invoiceableStudies = finDao.getStudies();
		
		finDao = new FinancialsDao();
		finDao = getMyInvoiced(studyIds);

		ArrayList invoiced = new ArrayList();
		invoiced = finDao.getInvoiced();
		ArrayList invoicedStudies = new ArrayList();
		invoicedStudies = finDao.getStudies();
		
		JSONArray jArray = new JSONArray();
		
		for (int iX=0; iX < arrStudyIds.size(); iX++) {		
			String studyId = (String)arrStudyIds.get(iX);
			StudyDao studyDao = new StudyDao();
			studyDao.getStudy(StringUtil.stringToNum(studyId));
			
			JSONObject aBilling = new JSONObject();
			try{
				aBilling.put(STUDYID_KEY, studyId);
				aBilling.put(STUDYNUMBER_KEY, String.valueOf(studyDao.getStudyNumbers().get(0)));
				
				int moduleAccess = 0;
		    	StudyRightsAgentRObj srightsAgent = EJBUtil.getStudyRightsAgentHome();
		    	moduleAccess = srightsAgent.getStudyRightsForModule(StringUtil.stringToNum(studyId), 
		    			StringUtil.stringToNum(userId), "MILESTONES");
		    	if (!StringUtil.isAccessibleFor(moduleAccess, 'V')){
		    		aBilling.put(INVOICEABLE_KEY, "");
		    		aBilling.put(INVOICEABLEAMT_KEY, "");
		    		aBilling.put(INVOICED_KEY, "");
		    		aBilling.put(INVOICEDAMT_KEY, "");
		    		aBilling.put(UNINVOICED_KEY, "");
		    		aBilling.put(UNINVOICEDAMT_KEY, "");
		    	} else {
					double invoiceableAmt = 0.0, invoicedAmt = 0.0, uninvoicedAmt = 0.0;
					int indx = -1;
					indx = invoiceableStudies.indexOf(studyId);
					if (indx > -1){
						invoiceableAmt = Double.parseDouble((String)invoiceables.get(indx));
						invoiceableAmt = Math.rint(invoiceableAmt * 100)/100;
						aBilling.put(INVOICEABLE_KEY, String.valueOf(invoiceableAmt));
					}else{
						aBilling.put(INVOICEABLE_KEY, "");
					}
					
					indx = -1;
					indx = invoicedStudies.indexOf(studyId);
					if (indx > -1){
						invoicedAmt = Double.parseDouble((String)invoiced.get(indx));
						invoicedAmt = Math.rint(invoicedAmt * 100)/100;
						aBilling.put(INVOICED_KEY, String.valueOf(invoicedAmt));
					}else{
						aBilling.put(INVOICED_KEY, "");
					}
					
					uninvoicedAmt = (invoiceableAmt-invoicedAmt);
					aBilling.put(UNINVOICED_KEY, String.valueOf(uninvoicedAmt));
		    	}
			} catch(Exception e) {
				Rlog.fatal("finBilling", "aBilling.put error: " + e);
			}
			try {
				jArray.put(aBilling);
			} catch(Exception e) {
				Rlog.fatal("finBilling", "jArray.put error: " + e);
			}
		}
		myBillingData = jArray;
    	return myBillingData;
	}

	@Override
	public JSONArray getBillingDrillDownData(String userId, String studyId, String calledFrom){
    	JSONArray jsBillingData = new JSONArray();
    	
    	int moduleAccess = 0, patientAccess = 0;
    	StudyRightsAgentRObj srightsAgent = EJBUtil.getStudyRightsAgentHome();
    	moduleAccess = srightsAgent.getStudyRightsForModule(StringUtil.stringToNum(studyId), 
    			StringUtil.stringToNum(userId), "MILESTONES");
    	if (!StringUtil.isAccessibleFor(moduleAccess, 'V')){
    		return jsBillingData;
    	}

    	patientAccess = srightsAgent.getStudyRightsForModule(StringUtil.stringToNum(studyId), 
    			StringUtil.stringToNum(userId), "STUDYVPDET");
    	
    	PatientDao pdao = new PatientDao();
    	int minPHIRight = 0;
    	UserJB userB = new UserJB();
    	userB.setUserId(StringUtil.stringToNum(userId));
		userB.getUserDetails();
		String grpId = userB.getUserGrpDefault();
		    	
    	String[] arMilestoneType =  new String[5];
    	arMilestoneType[0] = "EM";
    	arMilestoneType[1] = "VM";
    	arMilestoneType[2] = "PM";
    	arMilestoneType[3] = "SM";
    	arMilestoneType[4] = "AM";
    	
    	String[] arMilestoneTypeHeader =  new String[5];
    	arMilestoneTypeHeader[0] = LC.L_Event;/*Event*/
    	arMilestoneTypeHeader[1] = LC.L_Visit;/*Visit*/
    	arMilestoneTypeHeader[2] = LC.L_Patient_Status;/*Patient Status*/	
    	arMilestoneTypeHeader[3] = LC.L_Study_Status;/*Study Status*/
    	arMilestoneTypeHeader[4] = LC.L_Additional;/*Additional*/
    	
    	double totalMileAmount =0.0, totalPrevInvoicedAmount=0.0, totalUninvoicedAmount=0.0;
        ArrayList dataList = new ArrayList();
        
        try{
	    	for (int t = 0; t < arMilestoneType.length;t++){
	    		InvoiceJB InvB = new InvoiceJB(); 
	    		MileAchievedDao mileDao = new MileAchievedDao();
	     		if(arMilestoneType[t].equalsIgnoreCase("EM")){
	     			mileDao = InvB.getAchievedMilestonesEM(studyId, "", "", "", "", "", "rec", "", "All", "");
	     		}else{
	     			if (arMilestoneType[t].equalsIgnoreCase("AM")){
	     	 			mileDao = InvB.getAchievedMilestonesAM(studyId, "", "", "", "", "", "rec", "", "All" , "");
	     			}else{	
	     				mileDao = InvB.getAchievedMilestonesNew(studyId, "", "", "", "", "", "rec", arMilestoneType[t], "", "");
	     			}
	     		}
	     		
	     		MilestoneDao milestoneDao = new MilestoneDao();
	    		milestoneDao = mileDao.getMilestoneDao();
	    		ArrayList arMilestoneIds = new ArrayList();
	    		arMilestoneIds = milestoneDao.getMilestoneIds();
	    		ArrayList arMilestoneRuleDescs = new  ArrayList();
	    		arMilestoneRuleDescs = milestoneDao.getMilestoneRuleDescs();
	    		ArrayList arMilestoneAchievedCounts = new ArrayList();
	    		arMilestoneAchievedCounts = milestoneDao.getMilestoneAchievedCounts();
	    		ArrayList arCountPerMilestone = new ArrayList();
	    		arCountPerMilestone= milestoneDao.getMilestoneCounts();
	    		ArrayList arMileAmounts = new ArrayList();
	    		arMileAmounts =  milestoneDao.getMilestoneAmounts();
	    		ArrayList arMileTotalInvoicedAmounts = new ArrayList();
	    		arMileTotalInvoicedAmounts = milestoneDao.getTotalInvoicedAmounts();
	    		
	    		int milecount=0;
	    		
	    		if (arMilestoneIds != null)
	    		{
	    			milecount = arMilestoneIds.size();
	    		}	
	    		int totalmilecount = milecount;
	    		
	    		String mileDesc="", mileStoneId="", mileAchCount="";
	    		int countPerMilestone =0, mileDetailCount=0, mileInvoiceAmt=0;
	    		double mileAmount = 0.00, mileAmountForGroupMilestones=0.0, calcAmount=0.0, 
	    		totalMilePrevInvoicedAmount=0.0, mileDetailAmountsPrevInvoiced = 0.00,
	    		totalMileUninvoicedAmount=0.0;
	    	
	    		// for each milestone, get the milestone achieved details
	    		for (int i =0; i < milecount ; i++){
	    			mileDesc = (String ) arMilestoneRuleDescs.get(i);
	    			mileStoneId = String.valueOf(((Integer)arMilestoneIds.get(i)).intValue());
	    	
	    			MileAchievedDao ach = new MileAchievedDao();
	    			ArrayList patients = new ArrayList();
	    			ArrayList ids = new ArrayList();
	    			ArrayList enrollingSites = new ArrayList();
	    			ArrayList patFirstNames = new ArrayList();
	    			ArrayList patLastNames = new ArrayList();
	    			ArrayList patientCodes = null;
	    			ArrayList patStudyIds = null;
	    			ArrayList calendars = null;
	    			ArrayList visits = null;
	    			//ArrayList paymentTypes = new ArrayList();
	    			//ArrayList paymentFors = new ArrayList();
	    			ArrayList coverTypes = null;
	    			//ArrayList achievedOn = new ArrayList();
	    			ArrayList arMileDetailAmountsPrevInvoiced = new ArrayList();
	    	
	    			countPerMilestone =  StringUtil.stringToNum((String) arCountPerMilestone.get(i));
	    			mileAchCount = (String) arMilestoneAchievedCounts.get(i);
	    			mileAmount = Double.parseDouble((String) arMileAmounts.get(i));
	    			
	    			Hashtable htAchieved = new Hashtable();
	    			htAchieved = mileDao.getHtAchieved();
	    			totalMilePrevInvoicedAmount = 0;
	    			if (countPerMilestone > 1)
	    			{
	    		 		mileAmountForGroupMilestones = mileAmount/countPerMilestone;
	    		 	}else{
	    		 		mileAmountForGroupMilestones = mileAmount;
	    		 	}
	    			mileInvoiceAmt=(int) mileAmountForGroupMilestones;
	    			mileAmountForGroupMilestones = Double.parseDouble(""+NumberUtil.roundOffNo(mileAmountForGroupMilestones));					
	    	
	    			if (htAchieved.containsKey(mileStoneId)){
	    				ach = (MileAchievedDao) htAchieved.get(mileStoneId);
	    				ids = ach.getId();
	    				patients = ach.getPatient();
	    				mileDetailCount = ids.size();

	    				enrollingSites = ach.getEnrollingSite();
	    				patFirstNames = ach.getPatFirstName();
	    				patLastNames = ach.getPatLastName();
	    				patientCodes = ach.getPatientCode();
	    				patStudyIds = ach.getPatientStudyIds();
	    				calendars = ach.getCalDescs();
	    				visits = ach.getVisitNames();
	    				//paymentTypes = ach.getMilestonePaymentTypes();
	    				coverTypes = ach.getCoverAnalysis();
	    				//paymentFors = ach.getMilestonePaymentFors();
	    				calcAmount = StringUtil.stringToNum(mileAchCount) * mileAmount;
	    				//achievedOn = ach.getAchievedOn();
	    				ArrayList arMileDetailAmountsInvoiced = new ArrayList();
	    				arMileDetailAmountsInvoiced = ach.getAmountInvoiced();

	    				ArrayList arMileDetailAmountsHoldback = new ArrayList();
	    				arMileDetailAmountsHoldback = ach.getAmountHoldback();

	    				for (int indx = 0; indx < arMileDetailAmountsInvoiced.size(); indx++){
	    					double amountInvoiced = Double.parseDouble((String)arMileDetailAmountsInvoiced.get(indx));
	    					double amountHoldback = Double.parseDouble((String)arMileDetailAmountsHoldback.get(indx));

	    					arMileDetailAmountsPrevInvoiced.add(indx, ""+(amountInvoiced + amountHoldback));
	    				}
	    			} else {
	    				mileAchCount = "0";
	    				mileDetailCount = 0;
	    				calcAmount = mileAmount;
	    			}
	    			
	    			if (StringUtil.stringToNum(mileAchCount) > 0){
		    			String strTotalPrevInvAmount = "";
		    			strTotalPrevInvAmount = (String)arMileTotalInvoicedAmounts.get(i);
		    			
		    			if (! StringUtil.isEmpty(strTotalPrevInvAmount)){
		    				 totalMilePrevInvoicedAmount = Double.parseDouble(strTotalPrevInvAmount);
		    			} else {
		    				totalMilePrevInvoicedAmount = 0;
		    			}
		    			totalMileAmount += calcAmount;
		    			totalMilePrevInvoicedAmount = Math.rint(totalMilePrevInvoicedAmount * 100)/100;
		    			totalPrevInvoicedAmount += totalMilePrevInvoicedAmount;
		    			
		    			for (int k = 0; k < mileDetailCount; k++){
		    				mileDetailAmountsPrevInvoiced = Double.parseDouble((String) arMileDetailAmountsPrevInvoiced.get(k));
		    				mileDetailAmountsPrevInvoiced = Double.parseDouble(""+NumberUtil.roundOffNo(mileDetailAmountsPrevInvoiced));
		    				   				
		    				int mileAchieveId = StringUtil.stringToNum(""+ids.get(k));
		
		    				HashMap<String, Object> mstoneMap = new HashMap<String, Object>();
		    				mstoneMap.put("milestoneId",mileStoneId);
		    				mstoneMap.put("mileAchieveId",""+mileAchieveId);
		    				mstoneMap.put("mileType",arMilestoneTypeHeader[t]);
			    			String patientPK = (String)patients.get(k);
			    			minPHIRight =  pdao.getViewPatientPHIRight(EJBUtil.stringToNum(userId),EJBUtil.stringToNum(grpId),EJBUtil.stringToNum(patientPK));
		    				
		    				if (null == enrollingSites)
		    					mstoneMap.put("enrollingSite", "-");
		    				else{
		    					String enrollingSite = (String)enrollingSites.get(k);
		    					if (StringUtil.isEmpty(enrollingSite))
		    						mstoneMap.put("enrollingSite", "-");
		    					else
		    						mstoneMap.put("enrollingSite", enrollingSite);
		    				}

		    				if (null == patFirstNames && null == patLastNames)
		    					mstoneMap.put("patientName", "-");
		    				else{
		    					String patientName = (null == patLastNames)? 
		    							"" : (String)patLastNames.get(k);
		    					
		    					patientName = StringUtil.isEmpty(patientName)? "" : patientName;
		    					
		    					String patFirstName = (null == patFirstNames)? 
		    							"" : StringUtil.isEmpty((String)patFirstNames.get(k))? 
		    									"" : (String)patFirstNames.get(k);
		    					
		    					patientName += (StringUtil.isEmpty(patFirstName)? 
		    									"" : ", " + patFirstName);

			    		    	
		    					patientName = (StringUtil.isEmpty(patientName))? 
		    							"-" : patientName;
		    					if(minPHIRight>=4){
		    						mstoneMap.put("patientName", patientName);
	    						}else if (!StringUtil.isAccessibleFor(patientAccess, 'V')){
			    		    		mstoneMap.put("patientName", "*");
			    		    	} else {
			    		    		mstoneMap.put("patientName", "****");
			    		    	}
		    		    	}

		    				if (null == patientCodes)
		    					mstoneMap.put("patientCode", "-");
							else{
								String patientCode = (String)patientCodes.get(k);
								if (StringUtil.isEmpty(patientCode)){
									mstoneMap.put("patientCode", "-");
								}
								else{
										mstoneMap.put("patientCode", patientCodes.get(k));
									}
							}
		    				
		    				if (null == patStudyIds)
		    					mstoneMap.put("patStudyId", "-");
		    				else
		    					mstoneMap.put("patStudyId", patStudyIds.get(k));
		    				
		    				if (null == calendars)
		    					mstoneMap.put("calendar", "-");
		    				else
		    					mstoneMap.put("calendar", calendars.get(k));
		    				
		    				if (null == visits)
		    					mstoneMap.put("visit", "-");
		    				else
		    					mstoneMap.put("visit", visits.get(k));
		    				
		    				//mstoneMap.put("paymentType", paymentTypes.get(k));
		    				//mstoneMap.put("paymentFor", paymentFors.get(k));
		
		    				String cT = (String)coverTypes.get(k);
		    				mstoneMap.put("coverageType",cT);
		    				mstoneMap.put("descService", mileDesc + " "+ LC.L_Amount +": "+NumberUtil.formatToFloat(mileAmount));
		    				
		    				String detail = "";
		    				if (INVOICEABLE_KEY.equals(calledFrom)){
			             		detail = (NumberUtil.formatToFloat(mileAmountForGroupMilestones)).toString();
		    					mstoneMap.put(INVOICEABLEAMT_KEY, mileInvoiceAmt);
		    					mstoneMap.put(INVOICEABLE_KEY, detail);
		    				}
		
		    				if (INVOICED_KEY.equals(calledFrom)){
			    				if (mileDetailAmountsPrevInvoiced == 0){
			    					//Do not add this hash map to array 
			    					continue;
			    				}
			             		detail = (NumberUtil.formatToFloat(mileDetailAmountsPrevInvoiced)).toString();
		    					mstoneMap.put(INVOICEDAMT_KEY, mileInvoiceAmt);
		    					mstoneMap.put(INVOICED_KEY, detail);
		    				}
		    				
		    				if (UNINVOICED_KEY.equals(calledFrom)){
		    					totalMileUninvoicedAmount = (mileAmountForGroupMilestones-mileDetailAmountsPrevInvoiced);
			    				if (totalMileUninvoicedAmount == 0){
			    					//Do not add this hash map to array 
			    					continue;
			    				}
			             		detail = (NumberUtil.formatToFloat(totalMileUninvoicedAmount)).toString();
		    					mstoneMap.put(UNINVOICEDAMT_KEY, mileInvoiceAmt);
		    					mstoneMap.put(UNINVOICED_KEY, detail);
		    				}
		    				/*if (-1 <= countPerMilestone && countPerMilestone <= 1){
		    					mstoneMap.put("quantity", "1");					
		    				}else{
		    					mstoneMap.put("quantity", "1/"+countPerMilestone);
		    				}
		
		    				mstoneMap.put("prevInvoicedAmount", mileDetailAmountsPrevInvoiced);
		    				mstoneMap.put("mileAmount", mileAmountForGroupMilestones);
		    				
		    				
		    				String calcImage = "<div align='center'><img src='../images/jpg/Budget.gif' title='"+LC.L_Calculate+"' border='0'"
		    					+ " name='calculate_e"+mileAchieveId+"' id='calculate_e"+mileAchieveId+"'/></div>";
		    				mstoneMap.put("calculate", calcImage);
		    				
		    				String eraseImage = "<A title='"+LC.L_Erase+"' border='0'>"+LC.L_Erase+"</A>";
		    				mstoneMap.put("erase", eraseImage);*/
		    				dataList.add(mstoneMap);
		    			}
		    		}
	    		}
	    	}
	    	/*** Total Row ***/
	    	String strTotal="";
	    	HashMap<String, Object> mstoneMap = new HashMap<String, Object>();
	    	mstoneMap.put("isTotal","Y");
			mstoneMap.put("mileType",LC.L_Total);
			if (INVOICEABLE_KEY.equals(calledFrom)){
       			strTotal = (NumberUtil.formatToFloat(totalMileAmount)).toString();
				mstoneMap.put(INVOICEABLE_KEY, strTotal);
			}
			if (INVOICED_KEY.equals(calledFrom)){
       			strTotal = (NumberUtil.formatToFloat(totalPrevInvoicedAmount)).toString();
				mstoneMap.put(INVOICED_KEY, strTotal);
			}
			if (UNINVOICED_KEY.equals(calledFrom)){
		    	totalUninvoicedAmount = totalMileAmount - totalPrevInvoicedAmount;
       			strTotal = (NumberUtil.formatToFloat(totalUninvoicedAmount)).toString();
				mstoneMap.put(UNINVOICED_KEY, strTotal);
			}
			dataList.add(mstoneMap);
	    	//End Data here

	    	Collections.sort ( dataList , new Comparator (){
			
	    		 public int compare ( Object object1 , Object object2)
	    	    	{
	    	    		Integer obj1Value=0;
	    				Integer obj2Value=0;
	    				try {
	    					if(( ( HashMap ) object1 ).get( "invoiceableAmount" ) != null && ( ( HashMap ) object2 ).get( "invoiceableAmount" ) != null){
	    					obj1Value = ( Integer ) ( ( HashMap ) object1 ).get ( "invoiceableAmount" );
	    					obj2Value = ( Integer ) ( ( HashMap ) object2 ).get ( "invoiceableAmount" );
	    					}
	    					else if(( ( HashMap ) object1 ).get( "invoicedAmount" ) != null && ( ( HashMap ) object2 ).get( "invoicedAmount" ) != null){
	    						obj1Value = ( Integer ) ( ( HashMap ) object1 ).get ( "invoicedAmount" );
	    						obj2Value = ( Integer ) ( ( HashMap ) object2 ).get ( "invoicedAmount" );
	    						}
	    					else if(( ( HashMap ) object1 ).get( "uninvoicedAmount" ) != null && ( ( HashMap ) object2 ).get( "uninvoicedAmount" ) != null){
	    						obj1Value = ( Integer ) ( ( HashMap ) object1 ).get ( "uninvoicedAmount" );
	    						obj2Value = ( Integer ) ( ( HashMap ) object2 ).get ( "uninvoicedAmount" );
	    						}
	    				} catch (Exception e) {
	    					e.printStackTrace();
	    				}

	    	            return obj2Value.compareTo ( obj1Value ) ;
	    	        
	    	    }
	    		
	    	} ) ;
	    	for (int iX = 0; iX < dataList.size(); iX++) {
	    		jsBillingData.put(EJBUtil.hashMapToJSON((HashMap) dataList
	    				.get(iX)));
	    	}
        } catch (Exception e){
			Rlog.fatal("finBillingDrillDownData", "jArray.put error: " + e);
		}
    	return jsBillingData;
	}
	
	@Override
	public JSONArray getCollection(String accId, String userId, String studyIds) {
		JSONArray myCollectionData = new JSONArray();

		StudyJB studyJB1 = new StudyJB();
		StudyDao studyDao1 = studyJB1.getStudyAutoRows(userId, accId, studyIds, 1);
		ArrayList arrStudyIds = studyDao1.getStudyAutoIds();
		if (arrStudyIds == null) { return myCollectionData; }

		FinancialsDao finDao = new FinancialsDao(); 
		finDao = getMyInvoiced(studyIds);

		ArrayList invoiced = new ArrayList();
		invoiced = finDao.getInvoiced();
		ArrayList invoicedStudies = new ArrayList();
		invoicedStudies = finDao.getStudies();
		
		finDao = new FinancialsDao();
		finDao = getMyCollected(studyIds);

		ArrayList collected = new ArrayList();
		collected = finDao.getCollected();
		ArrayList collectedStudies = new ArrayList();
		collectedStudies = finDao.getStudies();
		
		JSONArray jArray = new JSONArray();
		
		for (int iX=0; iX < arrStudyIds.size(); iX++) {
			String studyId = (String)arrStudyIds.get(iX);
			StudyDao studyDao = new StudyDao();
			studyDao.getStudy(StringUtil.stringToNum(studyId));
			
			JSONObject aBilling = new JSONObject();
			try{
				aBilling.put(STUDYID_KEY, studyId);
				aBilling.put(STUDYNUMBER_KEY, String.valueOf(studyDao.getStudyNumbers().get(0)));
				
				int moduleAccess = 0;
		    	StudyRightsAgentRObj srightsAgent = EJBUtil.getStudyRightsAgentHome();
		    	moduleAccess = srightsAgent.getStudyRightsForModule(StringUtil.stringToNum(studyId), 
		    			StringUtil.stringToNum(userId), "MILESTONES");
		    	if (!StringUtil.isAccessibleFor(moduleAccess, 'V')){
		    		aBilling.put(INVOICED_KEY, "");
		    		aBilling.put(INVOICEDAMT_KEY, "");
		    		aBilling.put(COLLECTED_KEY, "");
		    		aBilling.put(COLLECTEDAMT_KEY, "");
		    		aBilling.put(OUTSTANDING_KEY, "");
		    		aBilling.put(OUTSTANDINGAMT_KEY, "");
		    	} else {
					double invoicedAmt = 0.0, collectedAmt = 0.0, outstandingAmt = 0.0;
					int indx = -1;
					indx = invoicedStudies.indexOf(studyId);
					if (indx > -1){
						invoicedAmt = Double.parseDouble((String)invoiced.get(indx));
						invoicedAmt = Math.rint(invoicedAmt * 100)/100;
						aBilling.put(INVOICED_KEY, String.valueOf(invoicedAmt));
					}else{
						aBilling.put(INVOICED_KEY, "");
					}

					indx = -1;
					indx = collectedStudies.indexOf(studyId);
					if (indx > -1){
						collectedAmt = Double.parseDouble((String)collected.get(indx));
						collectedAmt = Math.rint(collectedAmt * 100)/100;
						aBilling.put(COLLECTED_KEY, String.valueOf(collectedAmt));
					}else{
						aBilling.put(COLLECTED_KEY, "");
					}
					
					outstandingAmt = (invoicedAmt-collectedAmt);
					aBilling.put(OUTSTANDING_KEY, String.valueOf(outstandingAmt));
		    	}
			} catch(Exception e) {
				Rlog.fatal("finCollection", "aBilling.put error: " + e);
			}
			try {
				jArray.put(aBilling);
			} catch(Exception e) {
				Rlog.fatal("finCollection", "jArray.put error: " + e);
			}
		}
		myCollectionData = jArray;
    	return myCollectionData;
	}

	@Override
	public JSONArray getCollectionDrillDownData(String userId, String studyId, String calledFrom){
    	JSONArray jsCollectionData = new JSONArray();

    	int moduleAccess = 0;
    	ArrayList dataList = new ArrayList();
    	StudyRightsAgentRObj srightsAgent = EJBUtil.getStudyRightsAgentHome();
    	moduleAccess = srightsAgent.getStudyRightsForModule(StringUtil.stringToNum(studyId), 
    			StringUtil.stringToNum(userId), "MILESTONES");
    	if (!StringUtil.isAccessibleFor(moduleAccess, 'V')){
    		return jsCollectionData;
    	}

    	try{
    		ArrayList invoiceIds = new ArrayList();
    		ArrayList reconciledInvoiceIds = new ArrayList();
    		FinancialsDao invoiceFinDao = new FinancialsDao();
    		FinancialsDao reconciledInvoiceFinDao = new FinancialsDao();
    		
    		double prevInvoicedAmount=0.0, collectedAmount=0.0, outstandingAmount=0.0,
    			totalPrevInvoicedAmount=0.0, totalCollectedAmount=0.0, totalOutstandingAmount=0.0;
    		int invoiceAmt=0;

    		if (INVOICED_KEY.equals(calledFrom)){
    			invoiceFinDao.getCollectionDrillDownData(StringUtil.stringToNum(studyId), calledFrom);
	    		invoiceIds = invoiceFinDao.getInvoices();
	    		
	    		for (int iX= 0; iX < invoiceIds.size(); iX++) {
	    			HashMap<String, Object> hMap = new HashMap<String, Object>();
	             	hMap.put(STUDYID_KEY, invoiceFinDao.getStudies().get(iX));
	             	hMap.put("pk_invoice", invoiceIds.get(iX));
	             	hMap.put("invoiceNumber", invoiceFinDao.getInvoiceNumbers().get(iX));
	             	
	             	String detail = (String)invoiceFinDao.getInvoiceDates().get(iX);
	             	//detail = DateUtil.format2DateFormat(detail, "N");
	             	hMap.put("invoiceDate", detail);
	             	
	             	detail = (String)invoiceFinDao.getPaymentDueDates().get(iX);
	             	//detail = DateUtil.format2DateFormat(detail, "N");
	             	hMap.put("paymentDueDate", detail);
	             	prevInvoicedAmount = Double.parseDouble((String)invoiceFinDao.getInvoiced().get(iX));
	             	invoiceAmt = (int) prevInvoicedAmount;
             		totalPrevInvoicedAmount += prevInvoicedAmount;
             		
             		detail = (NumberUtil.formatToFloat(prevInvoicedAmount)).toString();
             		hMap.put(INVOICEDAMT_KEY, invoiceAmt);
             		hMap.put(INVOICED_KEY, detail);
             		
             		dataList.add(hMap);
	    		}
    		} else {
    			invoiceFinDao.getCollectionDrillDownData(StringUtil.stringToNum(studyId), INVOICED_KEY);
    			invoiceIds = invoiceFinDao.getInvoices();

    			if (null != invoiceIds && invoiceIds.size() > 0){
    				reconciledInvoiceFinDao.getCollectionDrillDownData(StringUtil.stringToNum(studyId), calledFrom);
        			reconciledInvoiceIds = reconciledInvoiceFinDao.getInvoices();   			

        			ArrayList revisedInvoiceIds = new ArrayList();
        			ArrayList revisedPaymentDates = new ArrayList();
        			ArrayList revisedCollectedAmts = new ArrayList();
        		
        			String invoiceId = "", reconciledInvoiceId = "";
        			String prevReconciledInvoiceId  = "";
        			double prevCollectedAmt = 0;
        			int reconciledIndx = -1;
        			
        			for (int iX= 0; iX < reconciledInvoiceIds.size(); iX++) {
        				reconciledInvoiceId = (String)reconciledInvoiceIds.get(iX);
        				if (prevReconciledInvoiceId.equals(reconciledInvoiceId)){
        					collectedAmount = Double.parseDouble((String)reconciledInvoiceFinDao.getCollected().get(iX));
        					prevCollectedAmt += collectedAmount;
        					revisedCollectedAmts.set(reconciledIndx, ""+prevCollectedAmt);
        					
        				} else {
        					revisedInvoiceIds.add(reconciledInvoiceId);
        					reconciledIndx = revisedInvoiceIds.size() - 1;
        					revisedPaymentDates.add((String)reconciledInvoiceFinDao.getPaymentDates().get(iX));
        					
        					prevCollectedAmt = Double.parseDouble((String)reconciledInvoiceFinDao.getCollected().get(iX));
        					revisedCollectedAmts.add((String)reconciledInvoiceFinDao.getCollected().get(iX));
        				}
        				prevReconciledInvoiceId = reconciledInvoiceId;
        			}
	    			
		    		for (int iX= 0; iX < invoiceIds.size(); iX++) {
		    			invoiceId = (String)invoiceIds.get(iX);
		    			//reconciledInvoiceId = (String)revisedInvoiceIds.get(iX);
	
		    			HashMap<String, Object> hMap = new HashMap<String, Object>();	
		    			hMap.put(STUDYID_KEY, invoiceFinDao.getStudies().get(iX));
		             	hMap.put("pk_invoice", invoiceIds.get(iX));
		             	hMap.put("invoiceNumber", invoiceFinDao.getInvoiceNumbers().get(iX));
		             	
		             	String detail = (String)invoiceFinDao.getInvoiceDates().get(iX);
		             	//detail = DateUtil.format2DateFormat(detail, "N");
		             	hMap.put("invoiceDate", detail);
		             	
		             	detail = (String)invoiceFinDao.getPaymentDueDates().get(iX);
		             	//detail = DateUtil.format2DateFormat(detail, "N");
		             	hMap.put("paymentDueDate", detail);
		             	
		             	prevInvoicedAmount = Double.parseDouble((String)invoiceFinDao.getInvoiced().get(iX));
	             		totalPrevInvoicedAmount += prevInvoicedAmount;

		    			int foundIndx = revisedInvoiceIds.indexOf(invoiceId);
	             		if (foundIndx > -1){
	             			if (COLLECTED_KEY.equals(calledFrom)){
			             		detail = (String)revisedPaymentDates.get(foundIndx);
			                 	//detail = DateUtil.format2DateFormat(detail, "N");
			                 	hMap.put("paymentDate", detail);
			
			             		collectedAmount = Double.parseDouble((String)revisedCollectedAmts.get(foundIndx));
			             		invoiceAmt = (int) collectedAmount;
			             		if (collectedAmount == 0){
			             			//Do not add this hash map to array 
			             			continue;
			             		}
			             		totalCollectedAmount += collectedAmount;

			             		detail = (NumberUtil.formatToFloat(collectedAmount)).toString();
			             		hMap.put(COLLECTEDAMT_KEY, invoiceAmt);
			             		hMap.put(COLLECTED_KEY, detail);
			             	}
			             	if (OUTSTANDING_KEY.equals(calledFrom)){
			             		collectedAmount = Double.parseDouble((String)revisedCollectedAmts.get(foundIndx));
			             		totalCollectedAmount += collectedAmount;
			             		
			             		outstandingAmount = prevInvoicedAmount - collectedAmount;
			             		invoiceAmt = (int) outstandingAmount;
			             		if (outstandingAmount == 0){
			             			//Do not add this hash map to array 
			             			continue;
			             		}
			             		totalOutstandingAmount += outstandingAmount;

			             		detail = (NumberUtil.formatToFloat(outstandingAmount)).toString();
			             		hMap.put(OUTSTANDINGAMT_KEY, invoiceAmt);
			             		hMap.put(OUTSTANDING_KEY, detail);
			             	}
		             	} else {
		             		if (COLLECTED_KEY.equals(calledFrom)){
			                 	hMap.put("paymentDate", "-");
			
			             		collectedAmount = 0;
			             		invoiceAmt = (int) collectedAmount;
			             		if (collectedAmount == 0){
			             			//Do not add this hash map to array 
			             			continue;
			             		}
			             		hMap.put(COLLECTEDAMT_KEY, invoiceAmt);
			             		hMap.put(COLLECTED_KEY, NumberUtil.formatToFloat(collectedAmount));
			             	}
			             	if (OUTSTANDING_KEY.equals(calledFrom)){
			             		outstandingAmount = prevInvoicedAmount;
			             		invoiceAmt = (int) outstandingAmount;
			             		if (outstandingAmount == 0){
			             			//Do not add this hash map to array 
			             			continue;
			             		}
			             		totalOutstandingAmount += outstandingAmount;

			             		detail = (NumberUtil.formatToFloat(outstandingAmount)).toString();
			             		hMap.put(OUTSTANDINGAMT_KEY, invoiceAmt);
			             		hMap.put(OUTSTANDING_KEY, detail);
			             	}
		             	}
	             		dataList.add(hMap);
		    		}
    			}
	    	}

	    	/*** Total Row ***/
    		String strTotal;
	    	HashMap<String, Object> hMap = new HashMap<String, Object>();
	    	hMap.put("isTotal","Y");
	    	hMap.put("invoiceNumber", LC.L_Total);
	    	if (INVOICED_KEY.equals(calledFrom)){
       			strTotal = (NumberUtil.formatToFloat(totalPrevInvoicedAmount)).toString();
         		hMap.put(INVOICED_KEY, strTotal);
         	}
         	if (COLLECTED_KEY.equals(calledFrom)){
       			strTotal = (NumberUtil.formatToFloat(totalCollectedAmount)).toString();
         		hMap.put(COLLECTED_KEY, strTotal);
         	}
         	if (OUTSTANDING_KEY.equals(calledFrom)){
       			strTotal = (NumberUtil.formatToFloat(totalOutstandingAmount)).toString();
         		hMap.put(OUTSTANDING_KEY, strTotal);
         	}
         	dataList.add(hMap);
         	//End Data Here
         	
         	Collections.sort ( dataList , new Comparator (){
    			
	    		 public int compare ( Object object1 , Object object2)
	    	    	{
	    	    		Integer obj1Value=0;
	    				Integer obj2Value=0;
	    				try {
	    					if(( ( HashMap ) object1 ).get( "invoicedAmount" ) != null && ( ( HashMap ) object2 ).get( "invoicedAmount" ) != null){
	    					obj1Value = ( Integer ) ( ( HashMap ) object1 ).get ( "invoicedAmount" );
	    					obj2Value = ( Integer ) ( ( HashMap ) object2 ).get ( "invoicedAmount" );
	    					}
	    					else if(( ( HashMap ) object1 ).get( "collectedAmount" ) != null && ( ( HashMap ) object2 ).get( "collectedAmount" ) != null){
	    						obj1Value = ( Integer ) ( ( HashMap ) object1 ).get ( "collectedAmount" );
	    						obj2Value = ( Integer ) ( ( HashMap ) object2 ).get ( "collectedAmount" );
	    						}
	    					else if(( ( HashMap ) object1 ).get( "outstandingAmount" ) != null && ( ( HashMap ) object2 ).get( "outstandingAmount" ) != null){
	    						obj1Value = ( Integer ) ( ( HashMap ) object1 ).get ( "outstandingAmount" );
	    						obj2Value = ( Integer ) ( ( HashMap ) object2 ).get ( "outstandingAmount" );
	    						}
	    				} catch (Exception e) {
	    					// TODO Auto-generated catch block
	    					e.printStackTrace();
	    				}

	    	            return obj2Value.compareTo ( obj1Value ) ;
	    	        
	    	    }
	    		
	    	} ) ;
         	for (int iX = 0; iX < dataList.size(); iX++) {
         		jsCollectionData.put(EJBUtil.hashMapToJSON((HashMap) dataList
	    				.get(iX)));
	    	}
	    	//Put Data into Map 
		} catch (Exception e){
			Rlog.fatal("finCollectionDrillDownData", "jArray.put error: " + e);
		}
    	return jsCollectionData;
	}
	
	@Override
	public JSONArray getPayments(String accId, String userId, String studyIds) {
		JSONArray myPaymentsData = new JSONArray();

		StudyJB studyJB1 = new StudyJB();
		StudyDao studyDao1 = studyJB1.getStudyAutoRows(userId, accId, studyIds, 1);
		ArrayList arrStudyIds = studyDao1.getStudyAutoIds();
		if (arrStudyIds == null) { return myPaymentsData; }

		FinancialsDao finDao = new FinancialsDao(); 
		finDao = getMyReceipts(studyIds);

		ArrayList receipts = new ArrayList();
		receipts = finDao.getReceipts();
		ArrayList receiptStudies = new ArrayList();
		receiptStudies = finDao.getStudies();
		
		finDao = new FinancialsDao();
		finDao = getMyDisbursements(studyIds);

		ArrayList disbursed = new ArrayList();
		disbursed = finDao.getDisbursements();
		ArrayList disbursedStudies = new ArrayList();
		disbursedStudies = finDao.getStudies();
		
		JSONArray jArray = new JSONArray();
		
		for (int iX=0; iX < arrStudyIds.size(); iX++) {
			String studyId = (String)arrStudyIds.get(iX);
			StudyDao studyDao = new StudyDao();
			studyDao.getStudy(StringUtil.stringToNum(studyId));
			
			JSONObject aBilling = new JSONObject();
			try{
				aBilling.put(STUDYID_KEY, studyId);
				aBilling.put(STUDYNUMBER_KEY, String.valueOf(studyDao.getStudyNumbers().get(0)));
				
				int moduleAccess = 0;
		    	StudyRightsAgentRObj srightsAgent = EJBUtil.getStudyRightsAgentHome();
		    	moduleAccess = srightsAgent.getStudyRightsForModule(StringUtil.stringToNum(studyId), 
		    			StringUtil.stringToNum(userId), "MILESTONES");
		    	if (!StringUtil.isAccessibleFor(moduleAccess, 'V')){
		    		aBilling.put(RECEIPTS_KEY, "");
		    		aBilling.put(DISBURSEMENTS_KEY, "");
		    		aBilling.put(NETCASH_KEY, "");
		    	} else {
					double receiptAmt = 0.0, disbursedAmt = 0.0, netCashAmt = 0.0;
					int indx = -1;
					indx = receiptStudies.indexOf(studyId);
					if (indx > -1){
						receiptAmt = Double.parseDouble((String)receipts.get(indx));
						receiptAmt = Math.rint(receiptAmt * 100)/100;
						aBilling.put(RECEIPTS_KEY, String.valueOf(receiptAmt));
					}else{
						aBilling.put(RECEIPTS_KEY, "");
					}

					indx = -1;
					indx = disbursedStudies.indexOf(studyId);
					if (indx > -1){
						disbursedAmt = Double.parseDouble((String)disbursed.get(indx));
						disbursedAmt = Math.rint(disbursedAmt * 100)/100;
						aBilling.put(DISBURSEMENTS_KEY, String.valueOf(disbursedAmt));
					}else{
						aBilling.put(DISBURSEMENTS_KEY, "");
					}
					
					netCashAmt = (receiptAmt-disbursedAmt);
					aBilling.put(NETCASH_KEY, String.valueOf(netCashAmt));
		    	}
			} catch(Exception e) {
				Rlog.fatal("finPayments", "aBilling.put error: " + e);
			}
			try {
				jArray.put(aBilling);
			} catch(Exception e) {
				Rlog.fatal("finPayments", "jArray.put error: " + e);
			}
		}
		myPaymentsData = jArray;
    	return myPaymentsData;
	}

	@Override
	public JSONArray getPaymentsDrillDownData(String userId, String studyId, String calledFrom){
    	JSONArray jsPaymentsData = new JSONArray();

    	int moduleAccess = 0;
    	StudyRightsAgentRObj srightsAgent = EJBUtil.getStudyRightsAgentHome();
    	moduleAccess = srightsAgent.getStudyRightsForModule(StringUtil.stringToNum(studyId), 
    			StringUtil.stringToNum(userId), "MILESTONES");
    	if (!StringUtil.isAccessibleFor(moduleAccess, 'V')){
    		return jsPaymentsData;
    	}

    	try{
    		FinancialsDao finDao = new FinancialsDao();
    		finDao.getPaymentsDrillDownData(StringUtil.stringToNum(studyId), calledFrom);
    		
    		ArrayList paymentStudies = finDao.getStudies();
    		double receiptAmount=0.0, disburedAmount=0.0, netCashAmount=0.0,
    			totalReceiptAmount=0.0, totalDisbursedAmount=0.0, totalNetCashAmount=0.0;
    		
    		for (int iX= 0; iX < paymentStudies.size(); iX++) {
    			HashMap<String, Object> hMap = new HashMap<String, Object>();
             	hMap.put(STUDYID_KEY, paymentStudies.get(iX));
             	
             	String detail = (String)finDao.getPaymentDates().get(iX);
             	//detail = DateUtil.format2DateFormat(detail, "N");
             	hMap.put("paymentDate", detail);
             	
             	if (!NETCASH_KEY.equals(calledFrom)){
	             	detail = (String)finDao.getPaymentDesc().get(iX);
	             	detail = StringUtil.isEmpty(detail)? "-" : detail;
	             	hMap.put("paymentDesc", detail);
	             	
	             	detail = (String)finDao.getPaymentNotes().get(iX);
	             	detail = StringUtil.isEmpty(detail)? "-" : detail;
	             	hMap.put("paymentNotes", detail);
             	}

             	if (RECEIPTS_KEY.equals(calledFrom)){
             		receiptAmount = Double.parseDouble((String)finDao.getReceipts().get(iX));
             		totalReceiptAmount += receiptAmount;

             		detail = (NumberUtil.formatToFloat(receiptAmount)).toString();
             		hMap.put(RECEIPTS_KEY, detail);
             	}
             	if (DISBURSEMENTS_KEY.equals(calledFrom)){
             		disburedAmount = Double.parseDouble((String)finDao.getDisbursements().get(iX));
             		if (disburedAmount == 0){
             			//Do not add this hash map to array 
             			continue;
             		}
             		totalDisbursedAmount += disburedAmount;

             		detail = (NumberUtil.formatToFloat(disburedAmount)).toString();
             		hMap.put(DISBURSEMENTS_KEY, detail);
             	}
             	if (NETCASH_KEY.equals(calledFrom)){
             		receiptAmount = Double.parseDouble((String)finDao.getReceipts().get(iX));
             		disburedAmount = Double.parseDouble((String)finDao.getDisbursements().get(iX));
             		netCashAmount = receiptAmount - disburedAmount;
             		if (netCashAmount == 0){
             			//Do not add this hash map to array 
             			//continue;
             		}

             		totalReceiptAmount += receiptAmount;
             		
             		detail = (receiptAmount == 0)? "-" : (NumberUtil.formatToFloat(receiptAmount)).toString();
             		hMap.put(RECEIPTS_KEY, detail);

             		totalDisbursedAmount += disburedAmount;
             		detail = (disburedAmount == 0)? "-" : (NumberUtil.formatToFloat(disburedAmount)).toString();
             		hMap.put(DISBURSEMENTS_KEY, detail);

             		totalNetCashAmount += netCashAmount;

             		detail = (netCashAmount == 0)? "-" : (NumberUtil.formatToFloat(netCashAmount)).toString();
             		hMap.put(NETCASH_KEY, detail);
             	}
             	jsPaymentsData.put(EJBUtil.hashMapToJSON(hMap));
    		}
    		/*** Total Row ***/
    		String strTotal = "";
	    	HashMap<String, Object> hMap = new HashMap<String, Object>();
	    	hMap.put("isTotal","Y");
	    	hMap.put("paymentDate", LC.L_Total);
	    	if (RECEIPTS_KEY.equals(calledFrom)){
       			strTotal = (NumberUtil.formatToFloat(totalReceiptAmount)).toString();
         		hMap.put(RECEIPTS_KEY, strTotal);
         	}
         	if (DISBURSEMENTS_KEY.equals(calledFrom)){
       			strTotal = (NumberUtil.formatToFloat(totalDisbursedAmount)).toString();
         		hMap.put(DISBURSEMENTS_KEY, strTotal);
         	}
         	if (NETCASH_KEY.equals(calledFrom)){
       			strTotal = (NumberUtil.formatToFloat(totalReceiptAmount)).toString();
         		hMap.put(RECEIPTS_KEY, strTotal);

       			strTotal = (NumberUtil.formatToFloat(totalDisbursedAmount)).toString();
         		hMap.put(DISBURSEMENTS_KEY, strTotal);

       			strTotal = (NumberUtil.formatToFloat(totalNetCashAmount)).toString();
         		hMap.put(NETCASH_KEY, strTotal);
         	}
			jsPaymentsData.put(EJBUtil.hashMapToJSON(hMap));
	    	//End Data here
		} catch (Exception e){
			Rlog.fatal("finPaymentsDrillDownData", "jArray.put error: " + e);
		}
    	return jsPaymentsData;
	}
}
