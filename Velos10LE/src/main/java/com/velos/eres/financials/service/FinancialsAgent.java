/*
 * Classname : FinancialsAgent
 * 
 * Version information: 1.0
 *
 * Date: 12/20/2011
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sampada Mhalagi
 */

package com.velos.eres.financials.service;

import javax.ejb.Remote;
import org.json.JSONArray;
import org.json.JSONException;
import com.velos.eres.financials.business.common.FinancialsDao;

@Remote
public abstract interface FinancialsAgent
{
	public FinancialsDao getMyInvoiceables(String studyIds);

	public FinancialsDao getMyInvoiced(String studyIds);

	public FinancialsDao getMyCollected(String studyIds);

	public FinancialsDao getMyReceipts(String studyIds);

	public FinancialsDao getMyDisbursements(String studyIds);

	public JSONArray getBilling(String accId, String userId, String studyIds);

	public JSONArray getBillingDrillDownData(String userId, String studyId, String calledFrom);
  
	public JSONArray getCollection(String accId, String userId, String studyIds);
	
	public JSONArray getCollectionDrillDownData(String userId, String studyId, String calledFrom);

	public JSONArray getPayments(String accId, String userId, String studyIds);
	
	public JSONArray getPaymentsDrillDownData(String userId, String studyId, String calledFrom);
}