package com.velos.eres.bulkupload.service;

import javax.ejb.Remote;
import com.velos.eres.bulkupload.business.BulkTemplateBean;
@Remote
public interface BulkTemplateAgent {
	 	public BulkTemplateBean getBulkTemplateDetails(int pkSpec);    
	    public int setBulkTemplateDetails(BulkTemplateBean specimen);    
	    public int updateBulkTemplate(BulkTemplateBean specimen);
}
