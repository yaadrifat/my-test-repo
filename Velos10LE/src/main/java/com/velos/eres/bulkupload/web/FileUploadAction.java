package com.velos.eres.bulkupload.web;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.velos.eres.bulkupload.business.BulkDao;
import com.velos.eres.bulkupload.business.BulkTemplateDao;
import com.velos.eres.bulkupload.business.bulkDetDao;
import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.MC;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.SessionMaint;
import com.velos.eres.service.util.StringUtil;
import com.aithent.file.uploadDownload.SaveFile;
import java.io.InputStream;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import eu.medsea.mimeutil.MimeType;
import eu.medsea.mimeutil.MimeUtil;
import eu.medsea.mimeutil.detector.MimeDetector;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.PatientDao;
import com.velos.eres.business.common.SettingsDao;
import com.velos.eres.business.common.StudyDao;
import com.velos.eres.business.common.UserSiteDao;
import com.velos.eres.web.common.CommonJB;
import com.velos.eres.web.patProt.PatProtJB;
import com.velos.eres.web.specimen.SpecimenJB;
import com.velos.eres.web.specimenStatus.SpecimenStatusJB;
import com.velos.eres.web.study.StudyJB;
import com.velos.eres.web.user.UserJB;
import com.velos.eres.web.userSite.UserSiteJB;
import com.velos.esch.business.common.EventAssocDao;
import com.velos.esch.business.common.ScheduleDao;
import com.velos.esch.web.eventassoc.EventAssocJB;
public class FileUploadAction extends ActionSupport implements ServletRequestAware {
	private static final long serialVersionUID = 1L;
	private File bulkUpload;
	private String bulkUploadContentType;
    private InputStream fileInputStream;
    private String bulkUploadFileName;
    private HttpServletRequest servletRequest;
	private HttpServletResponse response = null;
	private static final String BULK_PK_VELOS_FIELD="bulkpkfld";
	private static final String BULK_FILE_HEADER="bulkfileheader";
	private static final String BULK_VELOS_FIELD_NAME="bulkfldname";
	private static final String BULK_FIELD_FLAG="bulkfldflag";
	private static final String ORG_MANDATORY_FLAG="mandflag";
	private static final String WHOLE_FILE_DATA="wholefiledata";
	private static final String WHOLE_FILE_ROWS="wholefilerows";
	private static final String FILE_NAME="filename";
	private static final String SAVE_MAPPING="saveMapping";
	private static final String BULK_FILE_ERRORLOG="fileerrlog";
	private static final String BULK_PK="pkbulk";
	private static HttpSession tSession=null;
	private static final String TIMEOUT="timeout";
	public String db="";
	public String tableName="";
	public String columnName=""; 
	public String module="";
	public String pkColumnName="";
	public String errMsg="";
	public String flheader="";
	public int rownum;
	public int colnum;
	public int manflag;
	private int insertRow=0;
	public String fileData="";
    private static final String[] formats = { "yyyy-MM-dd'T'HH:mm:ss'Z'", "yyyy-MM-dd'T'HH:mm:ssZ", "yyyy-MM-dd'T'HH:mm:ss",
        "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd'T'HH:mm:ss.SSSZ", "yyyy-MM-dd HH:mm:ss", "MM/dd/yyyy HH:mm:ss",
        "MM/dd/yyyy'T'HH:mm:ss.SSS'Z'", "MM/dd/yyyy'T'HH:mm:ss.SSSZ", "MM/dd/yyyy'T'HH:mm:ss.SSS", "MM/dd/yyyy'T'HH:mm:ssZ",
        "MM/dd/yyyy'T'HH:mm:ss", "yyyy:MM:dd HH:mm:ss","MM/dd/yyyy","dd-MMM-yy"};
	public FileUploadAction() {
		ActionContext ac = ActionContext.getContext();
		response = (HttpServletResponse)ac.get(ServletActionContext.HTTP_RESPONSE);
	}
	public FileUploadAction(int row,int col,String data)
    {
		rownum=row;
		colnum=col;
		fileData=data;
		
    }
	public FileUploadAction(int row,int col,String data,String err)
    {
		rownum=row;
		colnum=col;
		fileData=data;
		errMsg=err;
		
    }
	public FileUploadAction(int row,int col,int flag,String header,String data)
    {
		rownum=row;
		colnum=col;
		manflag=flag;
		flheader=header;
		fileData=data;
    }
	
    public String execute() {
    	File fileToSave=null;
    	File fileToDb=null;
    	FileUploadAction f1=new FileUploadAction();
    	f1.tSession = servletRequest.getSession(true);
    	SessionMaint sessionmaint=new SessionMaint();
    	if (sessionmaint.isValidSession(tSession)){
        try { 
        	//int pkVal=0;
            String contentType = ServletActionContext.getServletContext().getMimeType(this.bulkUploadFileName);
            
        	tSession.setAttribute(FILE_NAME, this.bulkUploadFileName);
            
            MimeUtil.registerMimeDetector("eu.medsea.mimeutil.detector.MagicMimeMimeDetector"); 
            MimeUtil.registerMimeDetector("eu.medsea.mimeutil.detector.ExtensionMimeDetector");
            //MimeUtil.registerMimeDetector("eu.medsea.mimeutil.detector.OpendesktopMimeDetector");
            
            String extensionByString  = MimeUtil.getExtension(this.bulkUploadFileName);

            if(!EJBUtil.isEmpty(extensionByString)&& extensionByString.equalsIgnoreCase("csv")){
           
            db=servletRequest.getParameter("db");
            tableName=servletRequest.getParameter("tableName");
            columnName=servletRequest.getParameter("columnName");
            module=servletRequest.getParameter("module");
            pkColumnName=servletRequest.getParameter("pkColumnName");
            String userid=servletRequest.getParameter("userId");
            String ipAdd=servletRequest.getParameter("ipAdd");
            Configuration.readReportParam(Configuration.ERES_HOME + "eresearch.xml");
            //String fileDbPath = servletRequest.getSession().getServletContext().getRealPath("/");
            //fileToDb = new File(fileDbPath,this.bulkUploadFileName);
            //FileUtils.copyFile(this.bulkUpload, fileToDb);
            String fileSavePath = Configuration.REPTEMPFILEPATH;
            fileToSave = new File(fileSavePath,this.bulkUploadFileName);
            FileUtils.copyFile(this.bulkUpload, fileToSave);
            String retn=parseCsv(fileSavePath+"/"+this.bulkUploadFileName);
            if(retn.equalsIgnoreCase("success")){}
            else
            {
            addActionError("The selected file is empty!");
            return INPUT;
            }
//            CsvBulkUploadJB BulkUploadJB=new CsvBulkUploadJB();
//            //BulkUploadJB.setFkUser("test");
//            //BulkUploadJB.setFkBulkTamplate("test");
//            BulkUploadJB.setFileName("test");
//            BulkUploadJB.setTotRecord("10");
//            BulkUploadJB.setSucRecord("1");
//            BulkUploadJB.setLastModBy(userid);
//            BulkUploadJB.setIpAdd(ipAdd);
//            pkVal=BulkUploadJB.setBulkUplodDetails();
//            SaveFile.saveFile(db,tableName,columnName,pkColumnName,fileDbPath+this.bulkUploadFileName,pkVal,fileToDb.length());

          
            
            
            }else{
            	addActionError("Please enter a .CSV file!");
                return INPUT;
            }
        } catch (Exception e) { 
            e.printStackTrace();
            addActionError(e.getMessage());
            return INPUT;
        }
        finally
        {
        	//System.out.println("------------Inside finally----------------");
        	try{
        	if(fileToSave!=null && fileToSave.getAbsoluteFile().exists())
        	{
        		//System.out.println("fileToSavefileToSave"+fileToSave.getAbsoluteFile().toString());
        		//FileUtils.forceDelete(fileToSave.getAbsoluteFile());
        	}
        	if(fileToDb!=null && fileToDb.getAbsoluteFile().exists())
        	{
        		//System.out.println("fileToDbfileToDbfileToDb"+fileToDb.getAbsoluteFile().toString());
        		//FileUtils.forceDelete(fileToDb.getAbsoluteFile());
        	}
        	}
        	catch(Exception e){
        		e.printStackTrace();
                addActionError(e.getMessage());
                return INPUT;
        	}
        }
    	}
    	else
    	{
    		return TIMEOUT;
    	}
        return SUCCESS;
    }
    public String bulkDownload()
    {
    	try {
            db=servletRequest.getParameter("db");
            tableName=servletRequest.getParameter("tableName");
            columnName=servletRequest.getParameter("columnName");
            module=servletRequest.getParameter("module");
            pkColumnName=servletRequest.getParameter("pkColumnName");
            
            if (com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD==null) com.aithent.file.uploadDownload.Configuration.readSettings("sch");
    		com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(
            		com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD
                            + "fileUploadDownload.xml", "specimen");
            
    		if (!SaveFile.readFile(db, tableName, columnName, pkColumnName,
    				com.aithent.file.uploadDownload.Configuration.DOWNLOADFOLDER + "/" + "bulkUploadSample.csv",
    				27)) { 
                //System.out.println("FileDoenloadServlet.doGet could not read file");
            }
    		fileInputStream = new FileInputStream(
    				new File(com.aithent.file.uploadDownload.Configuration.DOWNLOADFOLDER + "/" + "bulkUploadSample.csv"));
    	 } catch (Exception e) { 
             e.printStackTrace();
             addActionError(e.getMessage());
             return INPUT;
         }
    	return SUCCESS;
    }
    
    public String Download() throws Exception
    {
    		String fileDownloadPath = servletRequest.getSession().getServletContext().getRealPath("/jsp/help");
    		fileInputStream = new FileInputStream(
    				new File(fileDownloadPath + "/bulkUploadSample.csv"));
        	return SUCCESS;
    }
    
    public String parseCsv(String parseCsvPath)
    { 
    	ArrayList<Integer> pkBulkDet=null; 
    	HashMap<Integer,String> fileHeader=new LinkedHashMap<Integer,String>();
    	ArrayList<Integer> fileRowCol=new ArrayList();
    	ArrayList<BulkUploadJB> wholeFileData=new ArrayList();
    	ArrayList<String> fldName=null; 
    	ArrayList<Integer> fldFlag=null; 
  	  	String setvalueover="0";
  	  	int mndtry=0;
  	    CommonJB commonB=new CommonJB();
  	  	SettingsDao settingsDao=commonB.getSettingsInstance();
  	  	int modname=1;
  	  	String keyword="ACC_SPECIMEN_ORGANIZATION_ASSOC";
  	  	UserJB user = (UserJB) tSession.getValue("currentUser");
  	  	String accountId = user.getUserAccountId();
  	  	settingsDao.retrieveSettings(EJBUtil.stringToNum(accountId),modname,keyword);
  	  	ArrayList setvalueovers=settingsDao.getSettingValue();
  	  	if(setvalueovers.size()>0)
  	  	setvalueover=(setvalueovers.get(0)==null)?"":(setvalueovers.get(0)).toString();
  	  	mndtry =EJBUtil.stringToNum(setvalueover);
        BulkDao bdao=new BulkDao();
        bdao.getBulkPkValue("specimen");
        int pkbulk = bdao.getPkBulk();
        bulkDetDao bdetdao=new bulkDetDao();
        bdetdao.getBulkSupportedflds(pkbulk);
        pkBulkDet=bdetdao.getPkBulkDet();
        fldName=bdetdao.getSupFld();
        fldFlag=bdetdao.getFlag();
        String temp=null;
    	try
    	{
    	 CSVReader reader = new CSVReader(new FileReader(parseCsvPath));
		 String [] nextLine;
		 int flag=0;
		 while ((nextLine = reader.readNext()) != null) {
			int testflag=0;
			int i=0;
		    for(i=0;i<nextLine.length;i++)
			{
		    nextLine[i]=nextLine[i].trim();
		    if(nextLine[i].length()>0){
		    	testflag=1;	
			}
			}
		    if(testflag==1){
			for(i=0;i<nextLine.length;i++)
			{
				nextLine[i]=nextLine[i].trim();
				if(flag==0)
				{
				if(nextLine[i].length()>0)
				fileHeader.put(i,nextLine[i]);
				}
				else
				{
	     		for(Map.Entry<Integer, String> entry : fileHeader.entrySet()) {
	     		if(i==entry.getKey())
	     		wholeFileData.add(new BulkUploadJB(flag,entry.getKey(),nextLine[i]));
	     		}
				}
			//System.out.println("Name: [" + nextLine[i] + "]");
			}
			if(flag>0){
			fileRowCol.add(flag);
			}
			flag++;
		    }
		   }	
		// System.out.println("fileRowColfileRowCol");
    	}
    	 catch (Exception e) { 
             e.printStackTrace();
             addActionError(e.getMessage());
         }
    	 if(fileHeader.isEmpty()){
    		 return INPUT;	 
    	 }
    	 tSession = servletRequest.getSession(true);
    	 tSession.setAttribute(BULK_FILE_HEADER, fileHeader);
    	 tSession.setAttribute(BULK_VELOS_FIELD_NAME, fldName);
    	 tSession.setAttribute(BULK_PK_VELOS_FIELD, pkBulkDet);
    	 tSession.setAttribute(BULK_FIELD_FLAG, fldFlag);
    	 tSession.setAttribute(ORG_MANDATORY_FLAG, mndtry);
    	 tSession.setAttribute(WHOLE_FILE_DATA, wholeFileData);
    	 tSession.setAttribute(WHOLE_FILE_ROWS, fileRowCol);
    	// fileHeader.clear();
    	 return SUCCESS;
    }
    public String fileMapping()
    {
    	int len=0;
    	HashMap<Integer,String> bulkPkName=new HashMap<Integer,String>();
    	ArrayList<Integer> pkBulk=(ArrayList)tSession.getAttribute("bulkpkfld");
    	ArrayList<String> nameBulk=(ArrayList)tSession.getAttribute("bulkfldname");
    	for(int i=0;i<pkBulk.size();i++)
    	{
    		bulkPkName.put(pkBulk.get(i),nameBulk.get(i));
    	}    	
    	TreeMap<String,String> saveMapping=new TreeMap<String,String>();
    	String[] selVelosFld = servletRequest.getParameterValues("fldNm"); //selected form ids
    	if (!(selVelosFld==null)) {
    		len = selVelosFld.length;
    	}
    	for(int i=0;i<len;i++)
    	{
    		String fileFld=servletRequest.getParameter(selVelosFld[i].trim());
    		if(fileFld!=null)
    			saveMapping.put(selVelosFld[i].trim(), fileFld);
    	}
    	servletRequest.setAttribute(SAVE_MAPPING,saveMapping);
    	servletRequest.setAttribute(BULK_PK,bulkPkName);
    	tSession.setAttribute(SAVE_MAPPING, saveMapping);
    	tSession.setAttribute(BULK_PK, bulkPkName);
    	return SUCCESS;
    }
    public String deleteMapping()
    {
    	String[] selVelosFld = servletRequest.getParameterValues("delMap"); //selected form ids
    	if (!(selVelosFld==null)) {
    		//System.out.println("selVelosFldselVelosFld"+selVelosFld);
    		tSession = servletRequest.getSession(true);
    		String userId = (String) tSession.getValue("userId");
    		String ipAdd = (String) tSession.getValue("ipAdd");
    		BulkTemplateDao temDao=new BulkTemplateDao();
        	int del=temDao.deleteMappings(selVelosFld,userId,ipAdd);
        	//System.out.println("deldeldeldeldeldeldel"+del);
    	}
    	return SUCCESS;
    }
    public int validateUpload(int size,String mapName,String autoSpec,String savemapfld,String accId)
    {
    	ArrayList<Integer> rowBulk=(ArrayList)tSession.getAttribute("wholefilerows");
    	HashMap<Integer,String> bulkPkName=(HashMap)tSession.getAttribute("pkbulk");
    	ArrayList<BulkUploadJB> wholeFileData=(ArrayList)tSession.getAttribute("wholefiledata");
    	TreeMap<String,String> saveMapping=(TreeMap)tSession.getAttribute("saveMapping");
    	HashMap<Integer,Integer> bulkPkFlag=new HashMap<Integer,Integer>();
    	ArrayList<Integer> pkBulk=(ArrayList)tSession.getAttribute("bulkpkfld");
    	ArrayList<Integer> flagBulk=(ArrayList)tSession.getAttribute("bulkfldflag");
    	ArrayList<BulkUploadJB> validateSampleStus=new ArrayList();
    	ArrayList<BulkUploadJB> checkErrors=new ArrayList<BulkUploadJB>();
    	ArrayList<BulkUploadJB> insertErrors=new ArrayList<BulkUploadJB>(); 
      	for(int j=0;j<pkBulk.size();j++)
    	{
      		bulkPkFlag.put(pkBulk.get(j),flagBulk.get(j));
    	}
    	String headerValue="";  
    	SpecimenJB specJB=null;
    	SpecimenStatusJB SpecimenStatus=null;
    	CodeDao cdSpecTyp = new CodeDao();
		String userId = (String) tSession.getValue("userId");
		String ipAdd = (String) tSession.getValue("ipAdd");
		UserJB user = (UserJB) tSession.getValue("currentUser");
    	String specId="";
    	int pkSpec=0;
		Date date = new java.util.Date();
		String sysDate = DateUtil.dateToString(date);
		sysDate = sysDate + " " + "00:00:00";
		for(int i=0;i<size;i++){
		specJB=new SpecimenJB();
		SpecimenStatus=new SpecimenStatusJB();
		
		UserSiteDao usd = new UserSiteDao ();
		UserSiteJB usrSite=new UserSiteJB();
		String accntId = user.getUserAccountId();
		usd = usrSite.getSitesWithViewRight(EJBUtil.stringToNum(accntId),EJBUtil.stringToNum(userId));
		
		StudyJB studyJB=new StudyJB();
		ArrayList<Integer> studyPk = null;
		ArrayList<String> studyNumbers = null;
		StudyDao studyDao = studyJB.getStudyValuesForUsers(userId);
		studyPk=studyDao.getStudyIds();
		studyNumbers=studyDao.getStudyNumbers();
		
		PatientDao patientDao= new PatientDao();
		patientDao.getPatientsRows(userId,accId);
	    ArrayList<String> patNames= patientDao.getPatNames();
	    ArrayList<String> patIds= patientDao.getPatIds();
	    
	    ArrayList<Integer> parentSpecs=null;
		ArrayList<String> lastName=null;
		ArrayList<String> firstName=null;
		ArrayList<String> midNameSpec=null;
		BulkDao bulkdao=new BulkDao();	
		String accountId = (String) tSession.getValue("accountId");
		bulkdao.getUserAccount(accountId);
		parentSpecs=bulkdao.getUserAcc();
		firstName=bulkdao.getUserAccFirstName();
		lastName=bulkdao.getUserAccLastName();
		midNameSpec=bulkdao.getUserAccMidName();
		
		if(!StringUtil.isEmpty(autoSpec) && autoSpec.equals("true"))
		{
					specId=specJB.getSpecimenIdAuto();
					specJB.setSpecimenId(specId);
		   			specJB.setFkAccount(accId);
					specId=(specId==null)?"":specId;
		}
    		for (Map.Entry<String,String> entry : saveMapping.entrySet()) 
    		{
    			headerValue=bulkPkName.get(Integer.parseInt(entry.getKey()));
    			if(!wholeFileData.isEmpty()){
    			for(BulkUploadJB t1:wholeFileData)
    			{	
    				if(rowBulk.get(i)==t1.rownum && Integer.parseInt(entry.getValue()) ==t1.colnum)
    				{
						int cdFlag=0;
						String cdPk="";
						int cd=0;
						ArrayList<String> codeVal=null;
						ArrayList<Integer> codepk=null;
						if(headerValue.equals("Specimen ID") && StringUtil.isEmpty(autoSpec))
    					{
						 
    						if(!StringUtil.isEmpty(t1.fileData))
    						{
    							ArrayList parentSpec=new ArrayList();
    							//String accountId = (String) tSession.getValue("accountId");
    							BulkDao bdao=new BulkDao();	
    							bdao.getParentSpecimen(t1.fileData, accountId);
    							parentSpec=bdao.getParentSpec();
    							if(parentSpec.isEmpty()){
    								specId=t1.fileData;
        				   			if(specId.length()<=150){
        				    		specJB.setSpecimenId(specId); 
        				   			specJB.setFkAccount(accId);
        				    		}
        				   			else{
        				   				checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,headerValue+" length Exceed"));
        				   			}	
    							}
    							else
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Specimen Id allready Exists"));
    							}
    					
    						}
    						else{
    							if(bulkPkFlag.get(Integer.parseInt(entry.getKey()))==1)
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Mandatory Field "+headerValue+" is null"));
    							}
    						
    						}
    						break;
    					}
    					if(headerValue.equals("Sample Type"))
    					{    		
    						cdSpecTyp.getCodeValues("specimen_type");
    						codeVal=cdSpecTyp.getCDesc();
    						codepk=cdSpecTyp.getCId();
								
    						if(!StringUtil.isEmpty(t1.fileData))
    						{
    							for(cd=0;cd<codeVal.size();cd++){
    								if(codeVal.get(cd).equalsIgnoreCase(t1.fileData)){
    									cdPk=String.valueOf(codepk.get(cd));
    									cdFlag=1;
    								}
    							}
    							if(cdFlag==1)
    							specJB.setSpecType(cdPk);
    							else
    							{
    							checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"File header "+headerValue+" value "+t1.fileData+" not Exists"));
    							}
    						}
    						else
    						{
    							if(bulkPkFlag.get(Integer.parseInt(entry.getKey()))==1)
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Mandatory Field "+headerValue+" is null"));
    							}
    						}
    						break;
    					}
    					if(headerValue.equals("Organization"))
    					{
    						int manflg=0;
    						manflg=(Integer)tSession.getAttribute("mandflag");
    						codepk = usd.getUserSiteSiteIds();
    						codeVal = usd.getUserSiteNames();
    						if(!StringUtil.isEmpty(t1.fileData))
    						{
    							for(cd=0;cd<codeVal.size();cd++){
    								if(codeVal.get(cd).equalsIgnoreCase(t1.fileData)){
    									cdPk=String.valueOf(codepk.get(cd));
    									cdFlag=1;
    								}
    							}
    							if(cdFlag==1)
    							specJB.setFkSite(cdPk);	
    							else
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"File header "+headerValue+" value "+t1.fileData+" not Exists"));
    							}
    						}
    						else
    						{
    							if(manflg==1)
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Mandatory Field "+headerValue+" is null"));
    							}
    						}
    						break;
    					}
    					if(headerValue.equals("Study Number"))
    					{
    						
    						HashMap<Integer,String> stdPkNum=new HashMap<Integer,String>();
    						if(!studyPk.isEmpty()){
    						for(cd=0;cd<studyPk.size();cd++){
    							stdPkNum.put(studyPk.get(cd), studyNumbers.get(cd));
    						}
    						}
    					
    						if(!StringUtil.isEmpty(t1.fileData))
    						{
    							if(!stdPkNum.isEmpty()){
    							for (Map.Entry<Integer,String> stdVal : stdPkNum.entrySet()) {
    								if(stdVal.getValue().equalsIgnoreCase(t1.fileData)){
    									cdPk=String.valueOf(stdVal.getKey());
    									cdFlag=1;
    								}
    							}
    							
    						}
    							if(cdFlag==1)
        							specJB.setFkStudy(cdPk);
        							else
        							{
        								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"File header "+headerValue+" value "+t1.fileData+" not Exists"));
        							}
    						}
    						else
    						{
    							if(bulkPkFlag.get(Integer.parseInt(entry.getKey()))==1)
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Mandatory Field "+headerValue+" is null"));
    							}
    						}
    					break;
    					}
    					if(headerValue.equals("Patient ID"))
    					{
    						HashMap<String,String> patPkNum=new HashMap<String,String>();
    						if(!patIds.isEmpty()){ 
        						for(cd=0;cd<patIds.size();cd++){
        							patPkNum.put(patIds.get(cd), patNames.get(cd));
        						}
        						}
    						if(!StringUtil.isEmpty(t1.fileData))
    						{
    							if(!patPkNum.isEmpty()){
    							for (Map.Entry<String,String> stdVal : patPkNum.entrySet()) {
    								if(stdVal.getValue().equalsIgnoreCase(t1.fileData)){
    									cdPk=String.valueOf(stdVal.getKey());
    									cdFlag=1;
    								}
    							}
    							
    						}
    						if(cdFlag==1)
        					specJB.setFkPer(cdPk); 
        					else
        					{
        					checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"File header "+headerValue+" value "+t1.fileData+" not Exists"));
        					}
    						}
    						else
    						{
    							if(bulkPkFlag.get(Integer.parseInt(entry.getKey()))==1)
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Mandatory Field "+headerValue+" is null"));
    							}
    						}
    						break;
    					} 

    					if(headerValue.equals("Patient Study ID")) 
    					{
    						int patprotflag=1;
    						int stdprotflag=1;
    						String Stdy="";
    						String Org="";
    						String pat="";
    						String Cal="";
    						String Vst="";
    						String Evt="";
    						int index=0;
    						String headerValueN="";
    						ArrayList<Integer> patprotpk=null;
    						ArrayList<String> patprotstd=null;
    						ArrayList<String> patprotpat=null;
    						ArrayList<String> patprotorg=null;
    						int patprotProt=0;
    						String patprotocolname="";
    						ArrayList<String> pkpat=new ArrayList();
    						ArrayList<String> stdpat=new ArrayList();
    						HashMap<String,String> rowData=new HashMap(); 
    						if(!StringUtil.isEmpty(t1.fileData))
    						{
    						BulkDao bdao=new BulkDao();
        					bdao.getPatProtDet(t1.fileData);
        					patprotpk=bdao.getPatprotid();
    	    				patprotstd=bdao.getPatprotstd();
    	    				patprotpat=bdao.getPatprotpat();
    	    				patprotorg=bdao.getPatprotOrg();
    	    				String patPk="";
    	    				String stdPk="";
    	    				//System.out.println("patprotpkpatprotpk  :"+patprotpk+"patprotstdpatprotstd  :"+patprotstd+"patprotpatpatprotpat  :"+patprotpat+"patprotorgpatprotorg  :"+patprotorg+"patprotProtpatprotProt"+patprotProt);
    	    				if(!patprotpk.isEmpty()){
    						for(BulkUploadJB t2:wholeFileData){
    							if(t2.rownum==t1.rownum){
    								rowData.put(StringUtil.integerToString(t2.colnum),t2.fileData);
    		    					}
    							}
    				   		for (Map.Entry<String,String> entryN : saveMapping.entrySet()) 
    			    		{
    			    			headerValueN=bulkPkName.get(Integer.parseInt(entryN.getKey()));

    			    				if(headerValueN.equals("Study Number"))
        			    			{
    			    				for (Map.Entry<String,String> entryM : rowData.entrySet()) 
    			    				{
    			    					if(entryN.getValue().equals(entryM.getKey())){
    			    						Stdy=entryM.getValue();
    			    					}
    			    				}
    			    				
    			    			}
    			    			if(headerValueN.equals("Patient ID"))
    			    			{
    			    				for (Map.Entry<String,String> entryM : rowData.entrySet()) 
    			    				{
    			    					if(entryN.getValue().equals(entryM.getKey())){
    			    						pat=entryM.getValue();
    			    					}
    			    				}
    			    				
    			    			}
    			      			if(headerValueN.equals("Organization"))
    			    			{
    			    				for (Map.Entry<String,String> entryM : rowData.entrySet()) 
    			    				{
    			    					if(entryN.getValue().equals(entryM.getKey())){
    			    						Org=entryM.getValue();
    			    					}
    			    				}
    			    				
    			    			}
    			      			if(headerValueN.equals("Calendar Name"))
    			    			{
    			    				for (Map.Entry<String,String> entryM : rowData.entrySet()) 
    			    				{
    			    					if(entryN.getValue().equals(entryM.getKey())){
    			    						Cal=entryM.getValue();
    			    					}
    			    				}
    			    				
    			    			}
    			      			if(headerValueN.equals("Visit"))
    			    			{
    			    				for (Map.Entry<String,String> entryM : rowData.entrySet()) 
    			    				{
    			    					if(entryN.getValue().equals(entryM.getKey())){
    			    						Vst=entryM.getValue();
    			    					}
    			    				}
    			    				
    			    			}
    			      			if(headerValueN.equals("Event"))
    			    			{
    			    				for (Map.Entry<String,String> entryM : rowData.entrySet()) 
    			    				{
    			    					if(entryN.getValue().equals(entryM.getKey())){
    			    						Evt=entryM.getValue();
    			    					}
    			    				}
    			    				
    			    			}
    			    		}

     						if(!StringUtil.isEmpty(pat)){
        						HashMap<String,String> patPkNum=new HashMap<String,String>();
        						if(!patIds.isEmpty()){ 
            						for(cd=0;cd<patIds.size();cd++){
            							patPkNum.put(patIds.get(cd), patNames.get(cd));
            						}
            						}
        						if(!patPkNum.isEmpty()){
        							for (Map.Entry<String,String> stdVal : patPkNum.entrySet()) {
        								if(stdVal.getValue().equalsIgnoreCase(pat)){
        									pkpat.add(String.valueOf(stdVal.getKey()));
        								}
        							}	
        						}
        						//System.out.println("pkpatpkpatpkpatpkpat"+pkpat);
        						for(cd=0;cd<patprotpat.size();cd++)
	    						{
	    							if(patprotflag==0)
	    							{
	    								break;
	    							}
	    							for(cdFlag=0;cdFlag<pkpat.size();cdFlag++){
	    								if(patprotpat.get(cd).equals(pkpat.get(cdFlag)))
	    								{
	    									index=patprotpat.indexOf(pkpat.get(cdFlag));
	    									patPk=pkpat.get(cdFlag);
	    									patprotflag=0;
	    									break;
	    									
	    								}
	    							}
	    						}
        	    	    		bdao.getPatProtFkProtocol(StringUtil.integerToString(patprotpk.get(index)));
        	    	    		patprotProt=bdao.getPatprotprot();
        	    	    		bdao.getPatProtProtocolName(StringUtil.integerToString(patprotProt));
        	    	    		patprotocolname=bdao.getPatprotocolname();
    							if(patprotflag==1)
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Patient "+pat+" not match with patient study id "+t1.fileData));    	    						
        						}
    						else
    						{
    						patprotflag=0;	
    						specJB.setFkPer(patprotpat.get(0));
    						bdao.getPatProtFkProtocol(StringUtil.integerToString(patprotpk.get(0)));
    	    	    		patprotProt=bdao.getPatprotprot();
    	    	    		bdao.getPatProtProtocolName(StringUtil.integerToString(patprotProt));
    	    	    		patprotocolname=bdao.getPatprotocolname();
    						}
      						if(!StringUtil.isEmpty(Stdy)){
    							//study id can't be duplicated
    							HashMap<Integer,String> stdPkNum=new HashMap<Integer,String>();
        						//StudyJB studyJB=new StudyJB();
        						//userId = (String) tSession.getValue("userId");
        						//ArrayList<String> studyPk = null;
        						//ArrayList<String> studyNumbers = null;
        						//StudyDao studyDao = studyJB.getStudyValuesForUsers(userId);
        						//studyPk=studyDao.getStudyIds();
        						//studyNumbers=studyDao.getStudyNumbers();
        						if(!studyPk.isEmpty()){
        						for(cd=0;cd<studyPk.size();cd++){
        							stdPkNum.put(studyPk.get(cd), studyNumbers.get(cd));
        						}
        						}
        						if(!stdPkNum.isEmpty()){
        							for (Map.Entry<Integer,String> stdVal : stdPkNum.entrySet()) {
        								if(stdVal.getValue().equalsIgnoreCase(Stdy)){
        									stdpat.add(String.valueOf(stdVal.getKey()));
        								}
        							}	
        						}
        						//System.out.println("stdpatstdpatstdpatstdpat"+stdpat);
    							for(cd=0;cd<patprotstd.size();cd++)
	    						{
	    							if(stdprotflag==0)
	    							{
	    								break;
	    							}
	    							for(cdFlag=0;cdFlag<stdpat.size();cdFlag++){
	    								if(patprotstd.get(cd).equals(stdpat.get(cdFlag)))
	    								{
	    									stdPk=stdpat.get(cdFlag);
	    									stdprotflag=0;
	    									break;
	    									
	    								}
	    							}
	    						}
    							if(stdprotflag==1)
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Study id "+Stdy+" not match with patient study id "+t1.fileData));    	    						
        						
        						}
    						else
    						{ 
    						stdprotflag=0;
    						specJB.setFkStudy(patprotstd.get(0));
    						}
    						if(StringUtil.isEmpty(Org) && !patprotorg.isEmpty())
    						specJB.setFkSite(patprotorg.get(0));  
    						ArrayList eventNames=new ArrayList();
    						ArrayList<Integer> eventPk=new ArrayList();
    						//EVENT,VISIT ARE ADDED TO CALANDER.FK_PTOTOCOL IS PK OF CALANDER AND EVENT,VISIT IS FETCHED BY USING FK_PROTOCOL
    						EventAssocJB eventassocB=new EventAssocJB();
    				        EventAssocDao assocdao = eventassocB.getEventVisitMileStoneGrid(patprotProt);/*YK :: DFIN12 BUG #5954*/
    				   		eventNames = assocdao.getNames();
    				   		eventPk=assocdao.getEvent_ids();
    				   		//System.out.println("eventPkeventPkeventPkeventPkeventPkeventPk"+eventPk);
    				   		//System.out.println("eventNameseventNameseventNames"+eventNames);
    				   		if(!StringUtil.isEmpty(Cal)&&!StringUtil.isEmpty(Vst)&&!StringUtil.isEmpty(Evt)&&!StringUtil.isEmpty(patprotocolname))
    				   		{
    				   		if(patprotocolname.equals(Cal)&&eventNames.contains(Vst)&&eventNames.contains(Evt))	
    				   		{
    				   			for(int evetPkN=0;evetPkN<eventPk.size();evetPkN++)
    				   			{
    				   				//System.out.println("eventPkeventPkeventPkeventPkeventPkeventPk"+eventPk.get(evetPkN));
    				   				if(eventPk.get(evetPkN)>0){
    				   					//System.out.println("eventPkeventPkeventPkeventPkeventPkeventPk222"+eventPk.get(evetPkN));
    				   				specJB.setFkSchEvents(String.valueOf(eventPk.get(evetPkN)));	
    				   				break;
    				   				}
    				   			}
    				   			
    				   		}
    				   		else
    				   		{
    				   			checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Event "+Evt+" not exists in "+t1.fileData+" Patient Study Id"));
    				   		}
    				   		}
    	    				}
    	    				else
    	    				{
    	    					checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Patient Study Id "+t1.fileData+" not exists"));
    	    				}
    	    			
    	    				
    						}
    						else
    						{
    							if(bulkPkFlag.get(Integer.parseInt(entry.getKey()))==1)
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Mandatory Field "+headerValue+" is null"));
    							}
    						}
      			    		/*		   		//Patient id can be duplicate depends upon organization
    						System.out.println("StdyStdy  :"+Stdy+"patpatpat: "+pat+" "+Org);
    						HashMap<String,String> stdPkNum=new HashMap<String,String>();
    						HashMap<String,String> patPkNum=new HashMap<String,String>();
    						HashMap<String,String> sitePkNum=new HashMap<String,String>();
    						userId = (String) tSession.getValue("userId");
    						if(!StringUtil.isEmpty(Stdy)){
        						StudyJB studyJB=new StudyJB();
        						StudyDao studyDao = studyJB.getStudyValuesForUsers(userId);
        						patIds=studyDao.getStudyIds();
        						patNames=studyDao.getStudyNumbers();
        						if(!patIds.isEmpty()){
        						for(cd=0;cd<patIds.size();cd++){
        							stdPkNum.put(patIds.get(cd),patNames.get(cd));
        						}
        						}
    						}
    						if(!StringUtil.isEmpty(pat)){
        					    PatientDao patDao= new PatientDao();
        					    patDao.getPatientsRows(userId,accId);
        					    patNames= patDao.getPatNames();
        					    patIds= patDao.getPatIds();
        						if(!patIds.isEmpty()){ 
            						for(cd=0;cd<patIds.size();cd++){
            							patPkNum.put(patIds.get(cd), patNames.get(cd));
            						}
            						}
    						}
    						if(!StringUtil.isEmpty(Org)){
        						UserSiteDao usd = new UserSiteDao ();
        						UserSiteJB usrSite=new UserSiteJB();
        						String accountId = user.getUserAccountId();
        						usd = usrSite.getSitesWithViewRight(EJBUtil.stringToNum(accountId),EJBUtil.stringToNum(userId));
        						patIds = usd.getUserSiteSiteIds();
        						patNames = usd.getUserSiteNames();
        						if(!patIds.isEmpty()){
                				for(cd=0;cd<patIds.size();cd++){
                					sitePkNum.put(patIds.get(cd),patNames.get(cd));
                				}
                				}
    						}
    						System.out.println("stdPkNumstdPkNum"+stdPkNum);
    						System.out.println("stdPkNumstdPkNum"+patPkNum);
    						System.out.println("stdPkNumstdPkNum"+sitePkNum);
    						if(!StringUtil.isEmpty(pat)&&!StringUtil.isEmpty(Stdy)&& !StringUtil.isEmpty(Org)){
    							if(!patPkNum.isEmpty()&& !stdPkNum.isEmpty()&&!sitePkNum.isEmpty()){
    								for(cd=0;cd<patprotpk.size();cd++){
        								if(patPkNum.containsKey(patprotpat.get(cd))&&stdPkNum.containsKey(patprotstd.get(cd))&& sitePkNum.containsKey(patprotorg.get(cd))){
        									cdPk=String.valueOf(patprotpk.get(cd));
        									cdFlag=1;
        									break;
        								}
        							}
        							
        						}
    						}
    						else if(!StringUtil.isEmpty(pat)&&!StringUtil.isEmpty(Stdy)&& StringUtil.isEmpty(Org)){
    						}
    						else if(!StringUtil.isEmpty(pat)&& StringUtil.isEmpty(Stdy)&& !StringUtil.isEmpty(Org)){        						    	    						
        					}
    						else if(!StringUtil.isEmpty(pat)&& StringUtil.isEmpty(Stdy)&& StringUtil.isEmpty(Org)){        						    	    						
        					}
    						else if(StringUtil.isEmpty(pat)&& !StringUtil.isEmpty(Stdy)&& !StringUtil.isEmpty(Org)){        						    	    						
        					}
    						else if(StringUtil.isEmpty(pat)&& !StringUtil.isEmpty(Stdy)&& StringUtil.isEmpty(Org)){        						    	    						
        					}
    						else if(StringUtil.isEmpty(pat)&& StringUtil.isEmpty(Stdy)&& !StringUtil.isEmpty(Org)){        						    	    						
        					}
    						else if(StringUtil.isEmpty(pat)&& StringUtil.isEmpty(Stdy)&& StringUtil.isEmpty(Org)){        						    	    						
        					}
   						if(!StringUtil.isEmpty(pat)&&StringUtil.isEmpty(Stdy)){
    							ArrayList<String> pkPer=null;
    							ArrayList<String> fkSite=null;
    							bdao.getPatientData(pat);
    							pkPer=bdao.getPkPer();
    							fkSite=bdao.getFkSite();
    							if(!pkPer.isEmpty()){
    							for(cd=0;cd<patprotpat.size();cd++)
	    						{
	    							if(patprotflag==0)
	    							{
	    								break;
	    							}
	    							for(cdFlag=0;cdFlag<pkPer.size();cdFlag++){
	    								if(patprotpat.get(cd).equals(pkPer.get(cdFlag))&& patprotorg.get(cd).equals(fkSite.get(cdFlag)))
	    								{
	    									index=patprotpat.indexOf(pkPer.get(cdFlag));
	    									patPk=pkPer.get(cdFlag);
	    	        					    PatientDao pat1= new PatientDao();
	    	        					    userId = (String) tSession.getValue("userId");
	    	        					    pat1.getPatientsRows(userId,accId);
	    	        					    patNames= pat1.getPatNames();
	    	        					    patIds= pat1.getPatIds();
	    	        						if(!patIds.isEmpty()){ 
	    	            						for(int Pkid=0;Pkid<patIds.size();Pkid++){
	    	            							if(patIds.get(Pkid).equals(patPk)){
	    	            						  		bdao.getPatProtFkProtocol(StringUtil.integerToString(patprotpk.get(index)));
	    	                    	    	    		patprotProt=bdao.getPatprotprot();
	    	                    	    	    		bdao.getPatProtProtocolName(StringUtil.integerToString(patprotProt));
	    	                    	    	    		patprotocolname=bdao.getPatprotocolname();
	    	                    	    	    		specJB.setFkStudy(patprotstd.get(index));
	    	                    	    	    		patprotflag=0;
	    	            								break;
	    	            							}
	    	            						}
	    	            						}
	    	        						break;
	    								}
	    							}
	    						}
    							}
        	    	  
    							if(patprotflag==1)
    								checkErrors.add(new FileUploadAction(t1.rownum,t1.colnum,t1.fileData,"Patient "+pat+" not match with patient study id "+t1.fileData));
    						}
    			
    						else
    						{
    						patprotflag=0;	
    						specJB.setFkPer(patprotpat.get(0));
    						bdao.getPatProtFkProtocol(StringUtil.integerToString(patprotpk.get(0)));
    	    	    		patprotProt=bdao.getPatprotprot();
    	    	    		bdao.getPatProtProtocolName(StringUtil.integerToString(patprotProt));
    	    	    		patprotocolname=bdao.getPatprotocolname();
    						}
    						System.out.println("patprotProtpatprotProt"+patprotProt);
    						System.out.println("patprotocolnamepatprotocolname"+patprotocolname);*/
    					break;
    					}
    					if(headerValue.equals("Specimen Description"))
    					{
    						
    						if(!StringUtil.isEmpty(t1.fileData))
    						specJB.setSpecDesc(t1.fileData);
    						else
    						{
    							if(bulkPkFlag.get(Integer.parseInt(entry.getKey()))==1)
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Mandatory Field "+headerValue+" is null"));
    							}
    						}
    						//System.out.println("headerValueheaderValue"+headerValue);
    						break;
    					}if(headerValue.equals("Sample Status"))
    					{
							cdSpecTyp.getCodeValues("specimen_stat");
    						codeVal=cdSpecTyp.getCDesc();
    						codepk=cdSpecTyp.getCId();
    						if(!StringUtil.isEmpty(t1.fileData)){
    							for(cd=0;cd<codeVal.size();cd++){
    								if(codeVal.get(cd).equalsIgnoreCase(t1.fileData)){
    									cdPk=String.valueOf(codepk.get(cd));
    									cdFlag=1;
    								}
    							}
    							if(cdFlag==1){	
    							validateSampleStus.add(new BulkUploadJB(t1.rownum,StringUtil.stringToNum(cdPk),manflag,headerValue,t1.fileData));
    							}
    							else
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"File header "+headerValue+" value "+t1.fileData+" not Exists"));
    							}
    						
    						}
    						else
    						{
    							if(bulkPkFlag.get(Integer.parseInt(entry.getKey()))==1)
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Mandatory Field "+headerValue+" is null"));
    							}
    						}
    						
    						break;
    					}if(headerValue.equals("Sample Status Date"))
    					{
    						
    						if(!StringUtil.isEmpty(t1.fileData)){
    							String dateN="";
    				            for (String parse : formats) {
						            SimpleDateFormat sdf = new SimpleDateFormat(parse);
						            try {
						                sdf.parse(t1.fileData);
						               
						                dateN=parse;
						                break;
						            } catch (ParseException e) {

						            }
						            }
    				            if(!StringUtil.isEmpty(dateN)){
    				            if(dateN.equals("MM/dd/yyyy"))
    							SpecimenStatus.setSsDate(t1.fileData+" "+"00:00:00");
    				            else if(dateN.equals("MM/dd/yyyy HH:mm:ss"))
    				            	SpecimenStatus.setSsDate(t1.fileData);
    				            }
    							}
    						else
    						{
    							SpecimenStatus.setSsDate(sysDate);
    							if(bulkPkFlag.get(Integer.parseInt(entry.getKey()))==1)
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Mandatory Field "+headerValue+" is null"));
    							}
    						}
    						
    						break;
    					}if(headerValue.equals("Sample Collected Quantity"))
    					{
    						
    						if(!StringUtil.isEmpty(t1.fileData)){
    							float theHeight = Float.parseFloat(t1.fileData);
    							
    							specJB.setSpecBaseOrigQuant(t1.fileData);
    						}
    						else
    						{
    							if(bulkPkFlag.get(Integer.parseInt(entry.getKey()))==1)
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Mandatory Field "+headerValue+" is null"));
    							}
    						}
    						
    						break;
    					}if(headerValue.equals("Sample Collected Quantity Units"))
    					{
    						
    						if(!StringUtil.isEmpty(t1.fileData)){
    							CodeDao cdSpecQuantUnit = new CodeDao();
    							cdSpecQuantUnit.getCodeValues("spec_q_unit");
    							codeVal=cdSpecQuantUnit.getCDesc();
    							codepk=cdSpecQuantUnit.getCId();
    							for(cd=0;cd<codeVal.size();cd++){
    								if(codeVal.get(cd).equalsIgnoreCase(t1.fileData)){
    									cdPk=String.valueOf(codepk.get(cd));
    									cdFlag=1;
    									break;
    								}
    							}
    							if(cdFlag==1){
    								specJB.setSpecBaseOrigQUnit(cdPk);	
    							}
    							else
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"File header "+headerValue+" value "+t1.fileData+" not Exists"));
    							}
    						}
    						else
    						{
    							if(bulkPkFlag.get(Integer.parseInt(entry.getKey()))==1)
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Mandatory Field "+headerValue+" is null"));
    							}
    						} 
    						
    						break;
    					}
    					if(headerValue.equals("Sample Collection date and time"))
    					{
    						
    						if(!StringUtil.isEmpty(t1.fileData)){
    							String dateN="";
    				            for (String parse : formats) {
						            SimpleDateFormat sdf = new SimpleDateFormat(parse);
						            try {
						                sdf.parse(t1.fileData);
						               
						                dateN=parse;
						                break;
						            } catch (ParseException e) {

						            }
						            }
    				            if(!StringUtil.isEmpty(dateN)){
    				            if(dateN.equals("MM/dd/yyyy"))
    				            	specJB.setSpecCollDate(t1.fileData+" "+"00:00:00");
    				            else if(dateN.equals("MM/dd/yyyy HH:mm:ss"))
    				            	specJB.setSpecCollDate(t1.fileData);
    				            }
    						} 
    						else
    						{
    							if(bulkPkFlag.get(Integer.parseInt(entry.getKey()))==1)
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Mandatory Field "+headerValue+" is null"));
    							}
    						}    						
    						
    						break;
    					}
    					if(headerValue.equals("Alternate ID"))
    					{
    					
    						if(!StringUtil.isEmpty(t1.fileData)){
    							specJB.setSpecAltId(t1.fileData);
    						}
    						else
    						{
    							if(bulkPkFlag.get(Integer.parseInt(entry.getKey()))==1)
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Mandatory Field "+headerValue+" is null"));
    							}
    						}
    						
    						break;
    					}if(headerValue.equals("Sample Notes"))
    					{
    					
    						if(!StringUtil.isEmpty(t1.fileData)){
    							specJB.setSpecNote(t1.fileData);
    						} 
    						else
    						{
    							if(bulkPkFlag.get(Integer.parseInt(entry.getKey()))==1)
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Mandatory Field "+headerValue+" is null"));
    							}
    						}
    						
    						break;
    					}if(headerValue.equals("Anatomic Site"))
    					{
    					
    						if(!StringUtil.isEmpty(t1.fileData)){
    							CodeDao cdSpecQuantUnit = new CodeDao();
    							cdSpecQuantUnit.getCodeValues("anatomic_site");
    							codeVal=cdSpecQuantUnit.getCDesc();
    							codepk=cdSpecQuantUnit.getCId();
    							for(cd=0;cd<codeVal.size();cd++){
    								if(codeVal.get(cd).equalsIgnoreCase(t1.fileData)){
    									cdPk=String.valueOf(codepk.get(cd));
    									cdFlag=1;
    									break;
    								}
    							}
    							if(cdFlag==1){
    								specJB.setSpecAnatomicSite(cdPk);	
    							}
    							else
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"File header "+headerValue+" value "+t1.fileData+" not Exists"));
    							}
    							
    						}
    						else
    						{
    							if(bulkPkFlag.get(Integer.parseInt(entry.getKey()))==1)
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Mandatory Field "+headerValue+" is null"));
    							}
    						}
    						
    						break;
    					}if(headerValue.equals("Tissue Side"))
    					{		
    						
    						if(!StringUtil.isEmpty(t1.fileData)){
    							CodeDao cdSpecQuantUnit = new CodeDao();
    							cdSpecQuantUnit.getCodeValues("tissue_side");
    							codeVal=cdSpecQuantUnit.getCDesc();
    							codepk=cdSpecQuantUnit.getCId();
    							for(cd=0;cd<codeVal.size();cd++){
    								if(codeVal.get(cd).equalsIgnoreCase(t1.fileData)){
    									cdPk=String.valueOf(codepk.get(cd));
    									cdFlag=1;
    									break;
    								}
    							}
    							if(cdFlag==1){
    								specJB.setSpecTissueSide(cdPk);	
    							}
    							else
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"File header "+headerValue+" value "+t1.fileData+" not Exists"));
    							}
    						}
    						else
    						{
    							if(bulkPkFlag.get(Integer.parseInt(entry.getKey()))==1)
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Mandatory Field "+headerValue+" is null"));
    							}
    						}
    						
    						break;
    					}if(headerValue.equals("Pathological Status"))
    					{
    						
    						if(!StringUtil.isEmpty(t1.fileData)){
    							CodeDao cdSpecQuantUnit = new CodeDao();
    							cdSpecQuantUnit.getCodeValues("pathology_stat");
    							codeVal=cdSpecQuantUnit.getCDesc();
    							codepk=cdSpecQuantUnit.getCId();
    							for(cd=0;cd<codeVal.size();cd++){
    								if(codeVal.get(cd).equalsIgnoreCase(t1.fileData)){
    									cdPk=String.valueOf(codepk.get(cd));
    									cdFlag=1;
    									break;
    								}
    							}
    							if(cdFlag==1){
    								specJB.setSpecPathologyStat(cdPk);	
    							}
    							else
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"File header "+headerValue+" value "+t1.fileData+" not Exists"));
    							}

    							
    						}
    						else
    						{
    							if(bulkPkFlag.get(Integer.parseInt(entry.getKey()))==1)
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Mandatory Field "+headerValue+" is null"));
    							}
    						}
    						
    						break;
    					}if(headerValue.equals("Expected Amount"))
    					{
    						
    						if(!StringUtil.isEmpty(t1.fileData)){
    							float theHeight = Float.parseFloat(t1.fileData);
    							
    							specJB.setSpecExpectQuant(t1.fileData);
    						}
    						else
    						{
    							if(bulkPkFlag.get(Integer.parseInt(entry.getKey()))==1)
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Mandatory Field "+headerValue+" is null"));
    							}
    						}    						
    						
    						break;
    					}if(headerValue.equals("Expected Amount Unit"))
    					{
    						
    						if(!StringUtil.isEmpty(t1.fileData)){
    							CodeDao cdSpecQuantUnit = new CodeDao();
    							cdSpecQuantUnit.getCodeValues("spec_q_unit");
    							codeVal=cdSpecQuantUnit.getCDesc();
    							codepk=cdSpecQuantUnit.getCId();
    							for(cd=0;cd<codeVal.size();cd++){
    								if(codeVal.get(cd).equalsIgnoreCase(t1.fileData)){
    									cdPk=String.valueOf(codepk.get(cd));
    									cdFlag=1;
    									break;
    								}
    							}
    							if(cdFlag==1){
    								specJB.setSpecExpectedQUnits(cdPk);	
    							}
    							else
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"File header "+headerValue+" value "+t1.fileData+" not Exists"));
    							}
    						}
    						else
    						{
    							if(bulkPkFlag.get(Integer.parseInt(entry.getKey()))==1)
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Mandatory Field "+headerValue+" is null"));
    							}
    						}
    						
    					break;
    						
    					}if(headerValue.equals("Current Amount"))
    					{
    						
    						if(!StringUtil.isEmpty(t1.fileData)){
    							float theHeight = Float.parseFloat(t1.fileData);
    							
    							specJB.setSpecOrgQnty(t1.fileData);
    						}
    						else
    						{
    							if(bulkPkFlag.get(Integer.parseInt(entry.getKey()))==1)
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Mandatory Field "+headerValue+" is null"));
    							}
    						}
    						
    					break;
    					}
    					if(headerValue.equals("Current Amount Unit"))
    					{
    					
    						if(!StringUtil.isEmpty(t1.fileData)){
    							CodeDao cdSpecQuantUnit = new CodeDao();
    							cdSpecQuantUnit.getCodeValues("spec_q_unit");
    							codeVal=cdSpecQuantUnit.getCDesc();
    							codepk=cdSpecQuantUnit.getCId();
    							for(cd=0;cd<codeVal.size();cd++){
    								if(codeVal.get(cd).equalsIgnoreCase(t1.fileData)){
    									cdPk=String.valueOf(codepk.get(cd));
    									cdFlag=1;
    									break;
    								}
    							}
    							if(cdFlag==1){
    								specJB.setSpectQuntyUnits(cdPk);	
    							}
    							else
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"File header "+headerValue+" value "+t1.fileData+" not Exists"));
    							}

    						}
    						else
    						{
    							if(bulkPkFlag.get(Integer.parseInt(entry.getKey()))==1)
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Mandatory Field "+headerValue+" is null"));
    							}
    						}
    						
    					break;
    					}
    					if(headerValue.equals("Pathologist")||headerValue.equals("Surgeon")||headerValue.equals("Collecting Technician")||headerValue.equals("Processing Technician"))
    					{
    						
    						if(!StringUtil.isEmpty(t1.fileData)){
    							//ArrayList<Integer> parentSpec=null;
    							String userName="";
    							int accPk=0;
    							//ArrayList<String> lastName=null;
    							//ArrayList<String> firstName=null;
    							//ArrayList<String> midNameSpec=null;
    							//BulkDao bdao=new BulkDao();	
    							//String accountId = (String) tSession.getValue("accountId");
    							//bdao.getUserAccount(accountId);
    							//parentSpec=bdao.getUserAcc();
    							//firstName=bdao.getUserAccFirstName();
    							//lastName=bdao.getUserAccLastName();
    							//midNameSpec=bdao.getUserAccMidName();
    							if(!parentSpecs.isEmpty())
    							{
    								for(int usercnt=0;usercnt<parentSpecs.size();usercnt++)
    								{
    									if(!firstName.isEmpty())
    									{
    										if(!StringUtil.isEmpty(firstName.get(usercnt)))
    											userName=firstName.get(usercnt);
    									}
    									if(!midNameSpec.isEmpty())
    									{
    										if(!StringUtil.isEmpty(midNameSpec.get(usercnt)))
    											userName=userName+" "+midNameSpec.get(usercnt);
    									}
    									if(!lastName.isEmpty())
    									{
    										if(!StringUtil.isEmpty(lastName.get(usercnt)))
    											userName=userName+" "+lastName.get(usercnt);
    									}
    									
    									if(userName.equalsIgnoreCase(t1.fileData)){
    										cdFlag=1;
    										accPk=parentSpecs.get(usercnt);
    										
    										if(headerValue.equalsIgnoreCase("Pathologist"))
    										specJB.setSpecUserPathologist(StringUtil.integerToString(accPk));
    										if(headerValue.equalsIgnoreCase("Surgeon"))
        										specJB.setSpecUserSurgeon(StringUtil.integerToString(accPk));
    										if(headerValue.equalsIgnoreCase("Collecting Technician"))
        										specJB.setSpecUserCollectingTech(StringUtil.integerToString(accPk));
    										if(headerValue.equalsIgnoreCase("Processing Technician"))
        										specJB.setSpecUserProcessingTech(StringUtil.integerToString(accPk));
    										break;
    									}
    									userName="";
    								}
    								if(cdFlag==1){	
        							}
        							else
        							{
        								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"File header "+headerValue+" value "+t1.fileData+" not Exists"));
        							}
    							}

    						}
    						else
    						{
    							if(bulkPkFlag.get(Integer.parseInt(entry.getKey()))==1)
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Mandatory Field "+headerValue+" is null"));
    							}
    						}
    						
    					break;
    					}  			
    					if(headerValue.equals("Parent Specimen ID"))
    					{
    						
    						if(!StringUtil.isEmpty(t1.fileData)){
    							ArrayList<Integer> parentSpec=null;
    							//String accountId = (String) tSession.getValue("accountId");
    							BulkDao bdao=new BulkDao();	
    							bdao.getParentSpecimen(t1.fileData, accountId);
    							parentSpec=bdao.getParentSpec();
    							if(!parentSpec.isEmpty())
    								specJB.setFkSpec(StringUtil.integerToString(parentSpec.get(0)));
    							else
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"The parent specimen "+t1.fileData+"  not exists at line no. "+t1.rownum));	
    							}
    							
    						}
    						else
    						{
    							if(bulkPkFlag.get(Integer.parseInt(entry.getKey()))==1)
    							{
    								checkErrors.add(new BulkUploadJB(t1.rownum,t1.colnum,t1.fileData,"Mandatory Field "+headerValue+" is null"));
    							}
    						}    						
    						
    					break;
    					}
    				}		
    			}
    		}
    		}
    		
    		if(checkErrors.isEmpty())
    		{
    			userId = (String) tSession.getValue("userId");
    		    ipAdd = (String) tSession.getValue("ipAdd");
				specJB.setCreator(userId);
				specJB.setIpAdd(ipAdd);
    				   pkSpec = specJB.setSpecimenDetails();
    				   if(pkSpec>0)
    				   {
    					   insertRow++;
    					   specJB.setFkAccount(accId);

       									   if(!validateSampleStus.isEmpty())
       									   {
       										     
       										BulkUploadJB validateSampleStusN=validateSampleStus.get(0);	
   					    						
       										   if(validateSampleStusN.manflag==0)
       										   {
       											
       										    SpecimenStatus.setFkSpecimen(pkSpec+"");
      				    							SpecimenStatus.setSsDate(sysDate);
      				    							SpecimenStatus.setFkCodelstSpecimenStat(validateSampleStusN.colnum + "");
      				    							SpecimenStatus.setSsStatusBy(userId);
      				    							SpecimenStatus.setCreator(userId);
      				    							SpecimenStatus.setIpAdd(ipAdd);
      				    							SpecimenStatus.setSpecimenStatusDetails();
       										   }
       										    	   
       										   else
       										   {
       											   	cdSpecTyp = new CodeDao();
       				    							int fkCodelstSpecimenStat = cdSpecTyp.getCodeId("specimen_stat", "Collected");
       				    							SpecimenStatus.setFkSpecimen(pkSpec+"");
       				    							//SpecimenStatus.setSsDate(sysDate);
       				    							SpecimenStatus.setFkCodelstSpecimenStat(fkCodelstSpecimenStat + "");
       				    							SpecimenStatus.setSsStatusBy(userId);
       				    							SpecimenStatus.setCreator(userId);
       				    							SpecimenStatus.setIpAdd(ipAdd);
       				    							SpecimenStatus.setSpecimenStatusDetails();
       										   }
       									   }
       									   else
       									   {
       										 	cdSpecTyp = new CodeDao();
   				    							int fkCodelstSpecimenStat = cdSpecTyp.getCodeId("specimen_stat", "Collected");
   				    							SpecimenStatus.setFkSpecimen(pkSpec+"");
   				    							//SpecimenStatus.setSsDate(sysDate);
   				    							SpecimenStatus.setFkCodelstSpecimenStat(fkCodelstSpecimenStat + "");
   				    							SpecimenStatus.setSsStatusBy(userId);
   				    							SpecimenStatus.setCreator(userId);
   				    							SpecimenStatus.setIpAdd(ipAdd);
   				    							SpecimenStatus.setSpecimenStatusDetails();
       									   }


    				   }
    				   else if(pkSpec==-3)
    					  System.out.println("Error:Specimen Id in file with Row no. "+rowBulk.get(i)+" is allready exist in database!!!");
    				  
						}
    		else
    		{
    			
    			
    			for(int errSize=0;errSize<checkErrors.size();errSize++) 
        		{
    				BulkUploadJB upl=checkErrors.get(errSize);
    				insertErrors.add(upl);
    				
        		}
    		}
    		checkErrors.clear();
    	 	validateSampleStus.clear();
			}
	
				   userId = (String) tSession.getValue("userId");
				   ipAdd = (String) tSession.getValue("ipAdd");
			   	   BulkDao bdao=new BulkDao();
			       bdao.getBulkPkValue("specimen");
			       int pkbulk = bdao.getPkBulk();
				   BulkTemplateJB blkTemJB=new BulkTemplateJB();
				   String fileName=(String)tSession.getAttribute("filename");
				   int temMatpk=0;
				   if(!StringUtil.isEmpty(savemapfld)){
					   blkTemJB.setFkBulkEntity(pkbulk+"");
					   blkTemJB.setTemplateName(savemapfld);
					   blkTemJB.setFkUser(userId);
					   blkTemJB.setIpAdd(ipAdd);
					   blkTemJB.setCreator(userId);
					   temMatpk=blkTemJB.setBulkTemplateDetails();  
					   if(temMatpk==-3)
	    				 System.out.println("template name is allready exists!!!");
				   }	
				   HashMap<Integer,String> fileHeader=(HashMap)tSession.getAttribute("bulkfileheader");
				   int temDetpk=0;
				   int unSuccRec=rowBulk.size()-insertRow;
				   int bkUpPk=0;
				   
				   CsvBulkUploadJB BulkUploadJB=new CsvBulkUploadJB();
				   BulkUploadJB.setFileName(fileName);
				   if(!StringUtil.isEmpty(savemapfld)&&temMatpk>=1){
				   BulkUploadJB.setFkBulkTamplate(temMatpk+"");
				   }
				   BulkUploadJB.setFkUser(userId);
				   BulkUploadJB.setSucRecord(insertRow+"");
				   BulkUploadJB.setTotRecord(rowBulk.size()+"");
				   BulkUploadJB.setUnsucRecord(unSuccRec+"");
				   BulkUploadJB.setIpAdd(ipAdd);
				   BulkUploadJB.setCreator(userId);
				   bkUpPk=BulkUploadJB.setBulkUplodDetails();
				   for (Map.Entry<String,String> entry : saveMapping.entrySet()) 
		    		{
				   if(!StringUtil.isEmpty(savemapfld)){
				   BulkTemplateDetailJB bulkTepDet=new BulkTemplateDetailJB();
				   bulkTepDet.setFileFldName(fileHeader.get(StringUtil.stringToInteger(entry.getValue())));
				   bulkTepDet.setFkBulkEntityDet(entry.getKey());
				   bulkTepDet.setFkBulkTemplate(temMatpk+"");
				   bulkTepDet.setColNum(entry.getValue());
				   bulkTepDet.setIpAdd(ipAdd);
				   bulkTepDet.setCreator(userId);
				   temDetpk=bulkTepDet.setBulkTemplateDetDetails();
					}
				   if(!insertErrors.isEmpty()){
				   int pkBulkErrRow=0;
				   for(int errSize=0;errSize<insertErrors.size();errSize++) {
					   BulkUploadJB upl=insertErrors.get(errSize);
					   if(StringUtil.stringToInteger(entry.getValue())==upl.colnum){
						   BulkErrorJB BulkError=new BulkErrorJB();
						   BulkError.setFkBulkUpload(bkUpPk+"");
						   BulkError.setFileRowNum(upl.rownum+"");
						   BulkError.setFileHeaderRowData(fileHeader.get(upl.colnum));
						   BulkError.setFileErrRowData(upl.fileData);
						   BulkError.setCreator(userId);
						   pkBulkErrRow=BulkError.setBulkErrDetails();
				
						   BulkErrLogJB bulkErrLogJB=new BulkErrLogJB();
						   bulkErrLogJB.setFkBulkErrRow(StringUtil.integerToString(pkBulkErrRow));
						   bulkErrLogJB.setCreator(userId);
						   if(!StringUtil.isEmpty(savemapfld)&& temDetpk>=1){
						   bulkErrLogJB.setFkBulkTemDet(StringUtil.integerToString(temDetpk));
						   }
						   bulkErrLogJB.setErrMsg(upl.errMsg); 
						   bulkErrLogJB.setBulkErrLogDetails();  
					   }
				  
				  
					   }
				   }
		    		}
		

				 //  tSession = servletRequest.getSession(true);
			       tSession.setAttribute(BULK_FILE_ERRORLOG,insertErrors);		   
				   
    		
    	return insertRow;
    }
    public boolean checkIfNumber(String in) {
        try {

            Integer.parseInt(in);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }
    public boolean checkIfFloat(String in) {
        try {

            Float.parseFloat(in);
        
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }
    public InputStream getFileInputStream() {
		return fileInputStream;
	}
    public File getBulkUpload() {
        return bulkUpload;
    }
    public void setBulkUpload(File bulkUpload) {
        this.bulkUpload = bulkUpload;
    }
    public String getBulkUploadContentType() {
        return bulkUploadContentType;
    }
 
    public void setBulkUploadContentType(String bulkUploadContentType) {
        this.bulkUploadContentType = bulkUploadContentType;
    }
 
    public String getBulkUploadFileName() {
        return bulkUploadFileName;
    }
 
    public void setBulkUploadFileName(String bulkUploadFileName) {
        this.bulkUploadFileName = bulkUploadFileName;
    }
 
    public void setServletRequest(HttpServletRequest servletRequest) {
        this.servletRequest = servletRequest;
 
    }
}