package com.velos.eres.bulkupload.web;


import com.velos.eres.bulkupload.business.BulkErrLogBean;
import com.velos.eres.bulkupload.service.BulkErrLogAgent;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

public class BulkErrLogJB {
	private  int pkErrLog;
    private  String fkBulkErrRow;
    private  String fkBulkTemDet;
    private  String errMsg;
    private  String ipAdd;
    private  String creator;
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getIpAdd() {
		return ipAdd;
	}
	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}
	public int getPkErrLog() {
		return pkErrLog;
	}
	public void setPkErrLog(int pkErrLog) {
		this.pkErrLog = pkErrLog;
	}
	public String getFkBulkErrRow() {
		return fkBulkErrRow;
	}
	public void setFkBulkErrRow(String fkBulkErrRow) {
		this.fkBulkErrRow = fkBulkErrRow;
	}
	public String getFkBulkTemDet() {
		return fkBulkTemDet;
	}
	public void setFkBulkTemDet(String fkBulkTemDet) {
		this.fkBulkTemDet = fkBulkTemDet;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	public BulkErrLogJB() {
        Rlog.debug("bulkupload", "BulkErrLogJB() ");
    }
//Parameterized constructor

		public BulkErrLogJB(int pkErr,String fkBulkUpload,String fileRowNum,String fileHeaderRowData,String creator,String ipAdd) {
			setPkErrLog(pkErr);
			setFkBulkErrRow(fkBulkUpload);
			setFkBulkTemDet(fileRowNum);
			setErrMsg(fileHeaderRowData);
			setIpAdd(ipAdd);
			setCreator(creator);
	}


	  /**
     * Retrieves the details of a bulkUpload in BulkUploadStateKeeper Object
     *
     * @return BulkUploadStateKeeper consisting of the bulkUpload details
     * @see BulkUploadStateKeeper
     */

    public BulkErrLogBean createErrRowLogStateKeeper() {
    	Rlog.debug("bulkUpload", "createErrRowLogStateKeeper");
        return new BulkErrLogBean(this.pkErrLog, this.fkBulkErrRow, this.fkBulkTemDet,this.errMsg,this.creator,this.ipAdd);
    }
    public BulkErrLogBean getBulkErrLogDetails() {
    	BulkErrLogBean spsk = null;
        try {
        	BulkErrLogAgent bulkUploadAgent = EJBUtil.getBulkErrLogHome();
            spsk = bulkUploadAgent.getBulkErrLogDetails(this.pkErrLog);
            Rlog.debug("bulkUpload", "In CsvBulkUpload.getBulkErrLogDetails() " + spsk);
        } catch (Exception e) {
            Rlog.fatal("bulkUpload", "Error in getBulkErrLogDetails() in BulkErrLogJB " + e);
        }
        if (spsk != null) {
        	this.pkErrLog = spsk.getPkErrLog();
    	    this.fkBulkErrRow = spsk.getFkBulkErrRow();
    	    this.fkBulkTemDet = spsk.getFkBulkTemDet();
    	    this.errMsg = spsk.getErrMsg();
    	    this.creator = spsk.getCreator();
    	    this.ipAdd = spsk.getIpAdd();
        }
        return spsk;
    }
    public int setBulkErrLogDetails() {
        int output = 0;
        try {
        	BulkErrLogAgent bulkUploadAgent = EJBUtil.getBulkErrLogHome();
            output = bulkUploadAgent.setBulkErrLogDetails(this.createErrRowLogStateKeeper());
            this.setPkErrLog(output);
        } catch (Exception e) {
            Rlog.fatal("bulkUpload", "Error in setBulkErrLogDetails() in BulkErrLogJB "   + e);
            return -2;
        }
        return output;
    }


    public int updateBulkErrLog() {
        int output;
        try {
        	BulkErrLogAgent bulkUploadAgent = EJBUtil.getBulkErrLogHome();
            output = bulkUploadAgent.updateBulkErrLog(this.createErrRowLogStateKeeper());
        } catch (Exception e) {
            Rlog.debug("bulkupload", "Exception setting bulkupload Error details to the BulkErrLogJB" + e.getMessage());
            return -2;
        }
        return output;
    }
}
