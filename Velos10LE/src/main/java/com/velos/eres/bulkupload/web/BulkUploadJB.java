package com.velos.eres.bulkupload.web;

public class BulkUploadJB {
	
	
	public int rownum;
	public int colnum;
	public int manflag;
	public String fileData="";
	public String flheader="";
	
	public BulkUploadJB()
    {
		
    }
	
	public BulkUploadJB(int row,int col,String data)
    {
		rownum=row;
		colnum=col;
		fileData=data;
		
    }
	public BulkUploadJB(int row,int col,String data,String err)
    {
		rownum=row;
		colnum=col;
		fileData=data;
		errMsg=err;
		
    }
	public BulkUploadJB(int row,int col,int flag,String header,String data)
    {
		rownum=row;
		colnum=col;
		manflag=flag;
		flheader=header;
		fileData=data;
    }
	
	
	public int getRownum() {
		return rownum;
	}
	public void setRownum(int rownum) {
		this.rownum = rownum;
	}
	public int getColnum() {
		return colnum;
	}
	public void setColnum(int colnum) {
		this.colnum = colnum;
	}
	public int getManflag() {
		return manflag;
	}
	public void setManflag(int manflag) {
		this.manflag = manflag;
	}
	public String getFileData() {
		return fileData;
	}
	public void setFileData(String fileData) {
		this.fileData = fileData;
	}
	public String getFlheader() {
		return flheader;
	}
	public void setFlheader(String flheader) {
		this.flheader = flheader;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	public String errMsg="";



	
}
