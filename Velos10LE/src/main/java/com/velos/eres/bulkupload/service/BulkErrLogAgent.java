package com.velos.eres.bulkupload.service;

import javax.ejb.Remote;

import com.velos.eres.bulkupload.business.BulkErrLogBean;

@Remote
public interface BulkErrLogAgent {
	public BulkErrLogBean getBulkErrLogDetails(int pkSpec);    
    public int setBulkErrLogDetails(BulkErrLogBean specimen);    
    public int updateBulkErrLog(BulkErrLogBean specimen);

}
