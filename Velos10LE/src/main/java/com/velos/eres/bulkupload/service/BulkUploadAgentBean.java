package com.velos.eres.bulkupload.service;


import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.velos.eres.bulkupload.business.BulkUploadBean;
import com.velos.eres.service.util.Rlog;

@Stateless
@Remote( { BulkUploadAgent.class } )
public class BulkUploadAgentBean implements BulkUploadAgent{
	  @PersistenceContext(unitName = "eres")
	    protected EntityManager em;
	  public BulkUploadBean getBulkUploadDetails(int pkSpec) {
	        if (pkSpec == 0) {
	            return null;
	        } 
	        BulkUploadBean spec = null;
	        try {
	        	spec = (BulkUploadBean) em.find(BulkUploadBean .class, new Integer(pkSpec));	            
	        } catch (Exception e) {
	            Rlog.fatal("bulkupload",
	                    "Error in getBulkUploadDetails() in BulkUploadAgentBean" + e);
	        }
	        return spec;
	    }
	
	  public int setBulkUplodDetails(BulkUploadBean spsk) {    
	        try {
	        	BulkUploadBean  spec = new BulkUploadBean(); 
	        	spec.updateBulkUpload(spsk);        	 
	            em.persist(spec);
	            return spec.getPkBulkUpload();
	        } catch (Exception e) {
	        	  Rlog.fatal("bulkupload",  "Error in setBulkUplodDetails() in BulkUploadAgentBean" + e);
	        }
	        return 0;
	    }
	    public int updateBulkUpload(BulkUploadBean spsk) {
	    	BulkUploadBean appSpec= null;
	        int output;
	        try {	            
	            output = appSpec.updateBulkUpload(spsk);            
	            em.merge(appSpec);
	        } catch (Exception e) {
	        	Rlog.fatal("bulkupload",  "Error in updateBulkUpload() in BulkUploadAgentBean" + e);
	            return -2;
	        }
	        return output;
	    }
}
