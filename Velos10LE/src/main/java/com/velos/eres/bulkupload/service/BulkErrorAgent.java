package com.velos.eres.bulkupload.service;

import com.velos.eres.bulkupload.business.BulkErrBean;
import com.velos.eres.bulkupload.business.BulkUploadBean;
import javax.ejb.Remote;

@Remote
public interface BulkErrorAgent {
	 	public BulkErrBean getBulkErrDetails(int pkSpec);    
	    public int setBulkErrDetails(BulkErrBean specimen);    
	    public int updateBulkErr(BulkErrBean specimen);
}
