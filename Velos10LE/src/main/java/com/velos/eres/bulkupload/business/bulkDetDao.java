package com.velos.eres.bulkupload.business;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.bulkupload.business.BULKSQLS;
import com.velos.eres.service.util.Rlog;

public class bulkDetDao extends CommonDAO implements java.io.Serializable {

	private ArrayList<Integer> pkBulkDet;
	private int fkBulk;
	private ArrayList<String> supFld;
	private ArrayList<String> dataType;
	private ArrayList<Integer> flag;

	public ArrayList getPkBulkDet() {
		return this.pkBulkDet;
	}

	public void setPkBulkDet(ArrayList pkBulkDet) {
		this.pkBulkDet = pkBulkDet;
	}

	public void setPkBulkDet(int pkBulkDet) {
		this.pkBulkDet.add(pkBulkDet);
	}

	public int getFkBulk() {
		return fkBulk;
	}

	public void setFkBulk(int fkBulk) {
		this.fkBulk = fkBulk;
	}
	public ArrayList getSupFld() {
		return this.supFld;
	}

	public void setSupFld(ArrayList cSubType) {
		this.supFld = cSubType;
	}

	public void setSupFld(String sCId) {
		this.supFld.add(sCId);
	}

	public ArrayList getDataType() {
		return this.dataType;
	}

	public void setDataType(ArrayList dataType) {
		this.dataType = dataType;
	}

	public void setDataType(String dataType) {
		this.dataType.add(dataType);
	}

	public ArrayList getFlag() {
		return this.flag;
	}

	public void setFlag(ArrayList flag) {
		this.flag = flag;
	}

	public void setFlag(int flag) {
		this.flag.add(flag);
	}
	public bulkDetDao() {
		pkBulkDet=new ArrayList<Integer>();
		supFld = new ArrayList<String>();
		dataType=new ArrayList<String>();
		flag=new ArrayList<Integer>();
	}

	public boolean getBulkSupportedflds(int fkbulk) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		setFkBulk(fkbulk);
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(BULKSQLS.erbulksurfld);
			pstmt.setInt(1, getFkBulk());
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				setPkBulkDet(rs.getInt("PK_BULK_ENTITY_DETAIL"));
				setSupFld(rs.getString("VELOS_FIELD_NAME"));
				setDataType(rs.getString("DATA_TYPE"));
				setFlag(rs.getInt("MANDATORY_FLAG"));
			}
			return true;
		} catch (SQLException ex) {
			Rlog.fatal("bulkupload",
					"EXCEPTION IN FETCHING FROMBULKUPLOADDetail TABLE " + ex);
			return false;
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (Exception e) {
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
			}
		}
	}

}
