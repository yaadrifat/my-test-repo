package com.velos.eres.bulkupload.web;

import com.velos.eres.bulkupload.business.BulkUploadBean;
import com.velos.eres.bulkupload.service.BulkUploadAgent;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

public class CsvBulkUploadJB {
	    private  int pkBulkUpload;
	    private  String fkUser;
	    private  String fkBulkTamplate;
	    private  String fileName;
	    private  String totRecord;
	    private  String sucRecord;
	    private  String unsucRecord;
	    private  String creator;
	    public String getCreator() {
			return creator;
		}

		public void setCreator(String creator) {
			this.creator = creator;
		}

		public String getUnsucRecord() {
			return unsucRecord;
		}

		public void setUnsucRecord(String unsucRecord) {
			this.unsucRecord = unsucRecord;
		}


		private  String lastModBy;
	    private  String ipAdd;
//  GETTERS AND SETTERS

    /**
	 * @return the pkSpecimen
	 */
	public int getBulkUpload() {
		return pkBulkUpload;
	}

	public int getPkBulkUpload() {
		return pkBulkUpload;
	}

	public void setPkBulkUpload(int pkBulkUpload) {
		this.pkBulkUpload = pkBulkUpload;
	}

	public String getFkUser() {
		return fkUser;
	}

	public void setFkUser(String fkUser) {
		this.fkUser = fkUser;
	}

	public String getFkBulkTamplate() {
		return fkBulkTamplate;
	}

	public void setFkBulkTamplate(String fkBulkTamplate) {
		this.fkBulkTamplate = fkBulkTamplate;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getTotRecord() {
		return totRecord;
	}

	public void setTotRecord(String totRecord) {
		this.totRecord = totRecord;
	}

	public String getSucRecord() {
		return sucRecord;
	}

	public void setSucRecord(String sucRecord) {
		this.sucRecord = sucRecord;
	}

	public String getLastModBy() {
		return lastModBy;
	}

	public void setLastModBy(String lastModBy) {
		this.lastModBy = lastModBy;
	}
	public void setIpAdd(String ipAddn) {
		 this.ipAdd=ipAddn;
	}
	public String getIpAdd() {
		 return ipAdd;
	}
	
    public void CsvBulkUpload(int pkBulkUpload) {
    	setPkBulkUpload(pkBulkUpload);
    }

    /**
     * Default Constructor
     */

    public CsvBulkUploadJB() {
        Rlog.debug("bulkupload", "CsvBulkUploadJB.CsvBulkUploadJB() ");
    }
//Parameterized constructor

		public CsvBulkUploadJB(int  pkBulkUpload,String fkUser,String fkBulkTem,String fileName,String totRec,String sucRecord,
			String  lastModBy, String ipAdd,String unsucRecord,String creator) {
			setPkBulkUpload(pkBulkUpload);
			setFkUser(fkUser);
			setFkBulkTamplate(fkBulkTem);
			setFileName(fileName);
			setTotRecord(totRec);
			setSucRecord(sucRecord);
			setLastModBy(lastModBy);
			setIpAdd(ipAdd);
			setUnsucRecord(unsucRecord);
			setCreator(creator);

	}


	  /**
     * Retrieves the details of a bulkUpload in BulkUploadStateKeeper Object
     *
     * @return BulkUploadStateKeeper consisting of the bulkUpload details
     * @see BulkUploadStateKeeper
     */

    public BulkUploadBean createBulkUploadStateKeeper() {
    	Rlog.debug("bulkUpload", "createBulkUploadStateKeeper");
        return new BulkUploadBean(this.pkBulkUpload, this.fkUser, this.fkBulkTamplate,
        	    this.fileName, this.totRecord, this.sucRecord,
        	    this.lastModBy,this.unsucRecord, this.creator,
        	    this.ipAdd);
    }
    public BulkUploadBean getBulkUploadDetails() {
    	BulkUploadBean spsk = null;
        try {
        	BulkUploadAgent bulkUploadAgent = EJBUtil.getBulkUploadHome();
            spsk = bulkUploadAgent.getBulkUploadDetails(this.pkBulkUpload);
            Rlog.debug("bulkUpload", "In CsvBulkUpload.getBulkUploadDetails() " + spsk);
        } catch (Exception e) {
            Rlog.fatal("bulkUpload", "Error in getBulkUploadDetails()() in SpecimenJB " + e);
        }
        if (spsk != null) {
        	this.pkBulkUpload = spsk.getPkBulkUpload();
    	    this.fkUser = spsk.getFkUser();
    	    this.fkBulkTamplate = spsk.getFkBulkTamplate();
    	    this.fileName = spsk.getFileName();
    	    this.totRecord= spsk.getTotRecord();
    	    this.sucRecord = spsk.getSucRecord();
    	    this.unsucRecord = spsk.getUnSucRecord();
    	    this.lastModBy =spsk.getLastModBy();
       	    this.ipAdd = spsk.getIpAdd() ;
       	    this.creator=spsk.getCreator();
        }
        return spsk;
    }
    public int setBulkUplodDetails() {
        int output = 0;
        try {
        	BulkUploadAgent bulkUploadAgent = EJBUtil.getBulkUploadHome();
            output = bulkUploadAgent.setBulkUplodDetails(this.createBulkUploadStateKeeper());
            this.setPkBulkUpload(output);
        } catch (Exception e) {
            Rlog.fatal("bulkUpload", "Error in setBulkUplodDetails() in CsvBulkUploadJB "   + e);
            return -2;
        }
        return output;
    }


    public int updateBulkUpload() {
        int output;
        try {
        	BulkUploadAgent bulkUploadAgent = EJBUtil.getBulkUploadHome();
            output = bulkUploadAgent.updateBulkUpload(this.createBulkUploadStateKeeper());
        } catch (Exception e) {
            Rlog.debug("bulkupload", "Exception setting bulkupload details to the bulkupload JB" + e.getMessage());
            return -2;
        }
        return output;
    }
	
}// end of class

