package com.velos.eres.bulkupload.web;

import com.velos.eres.bulkupload.business.BulkTemplateBean;
import com.velos.eres.bulkupload.service.BulkTemplateAgent;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

public class BulkTemplateJB {
	
	private  int pkTemplate;
	private  String fkBulkEntity;
    private  String templateName;
    private  String fkUser;
	private  String ipAdd;
	private  String creator;
	
    public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public int getPkTemplate() {
		return pkTemplate;
	}
	public void setPkTemplate(int pkTemplate) {
		this.pkTemplate = pkTemplate;
	}
	public String getFkBulkEntity() {
		return fkBulkEntity;
	}
	public void setFkBulkEntity(String fkBulkEntity) {
		this.fkBulkEntity = fkBulkEntity;
	}
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	public String getFkUser() {
		return fkUser;
	}
	public void setFkUser(String fkUser) {
		this.fkUser = fkUser;
	}

	public String getIpAdd() {
		return ipAdd;
	}
	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}
    public BulkTemplateJB() {
        Rlog.debug("BulkTemplateJB", "BulkTemplateJB.BulkTemplateJB() ");
    }
    
    public BulkTemplateJB(int pkBulkUpload,String fkBulkEntity,String templateName,String fkUser,String lastModBy,String creator,String ipAdd) {
    		setPkTemplate(pkBulkUpload);
    		setFkBulkEntity(fkBulkEntity);
    		setTemplateName(templateName);
    		setFkUser(fkUser);
			setIpAdd(ipAdd);
			setCreator(creator);
	}
    public BulkTemplateBean createBulkTemplateStateKeeper() {
    	Rlog.debug("bulkUploadTemplate", "bulkUplodTemplate.createBulkTemplateStateKeeper");
        return new BulkTemplateBean(this.pkTemplate, this.fkBulkEntity, this.templateName,
        	    this.fkUser,this.creator, this.ipAdd);
    }
    
    public BulkTemplateBean getBulkTemplateDetails() {
    	BulkTemplateBean spsk = null;
        try {
        	BulkTemplateAgent bulkTemplateAgent = EJBUtil.getTemplateHome();
            spsk = bulkTemplateAgent.getBulkTemplateDetails(this.pkTemplate);
            Rlog.debug("bulkTemplate", "In BulkTemplateJB.getBulkTemplateDetails " + spsk);
        } catch (Exception e) {
            Rlog.fatal("bulkTemplate", "Error in getBulkTemplateDetails() in BulkTemplateJB " + e);
        }
        if (spsk != null) {
        	this.pkTemplate = spsk.getPkTemplate();
    	    this.fkBulkEntity = spsk.getFkBulkEntity();
    	    this.templateName = spsk.getTemplateName();
    	    this.fkUser = spsk.getFkUser();
    	    this.ipAdd = spsk.getIpAdd();
    	    this.creator=spsk.getCreator();
        }
        return spsk;
    }
    
    public int setBulkTemplateDetails() {
        int output = 0;
        try {
        	BulkTemplateAgent bulkTemplateAgent = EJBUtil.getTemplateHome();
            output = bulkTemplateAgent.setBulkTemplateDetails(this.createBulkTemplateStateKeeper());
            this.setPkTemplate(output);
        } catch (Exception e) {
            Rlog.fatal("bulkTemplate", "Error in setBulkTemplateDetails() in BulkTemplateJB "   + e);
            return -2;
        }
        return output;
    }
    public int updateBulkTemplate() {
        int output;
        try {
        	BulkTemplateAgent bulkTemplateAgent = EJBUtil.getTemplateHome();
            output = bulkTemplateAgent.updateBulkTemplate(this.createBulkTemplateStateKeeper());
        } catch (Exception e) {
            Rlog.debug("bulkTemplate", "Exception setting bulkTemplate details to the BulkTemplateJB" + e.getMessage());
            return -2;
        }
        return output;
    }
    
}
