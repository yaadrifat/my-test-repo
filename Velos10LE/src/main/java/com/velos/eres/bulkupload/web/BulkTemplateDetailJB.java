package com.velos.eres.bulkupload.web;

import com.velos.eres.bulkupload.business.BulkTemplateDetailBean;
import com.velos.eres.bulkupload.service.BulkTempateDetailAgent;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

public class BulkTemplateDetailJB {

	private  int pkTemplateDet;
	private  String fkBulkTemplate;
    private  String fkBulkEntityDet;
    private  String fileFldName;
    private  String lastModBy;
    private  String ipAdd;
    private  String creator;
    public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	private  String colNum;
    public String getColNum() {
		return colNum;
	}
	public void setColNum(String colNum) {
		this.colNum = colNum;
	}
	public int getPkTemplateDet() {
		return pkTemplateDet;
	}
	public void setPkTemplateDet(int pkTemplateDet) {
		this.pkTemplateDet = pkTemplateDet;
	}
	public String getFkBulkTemplate() {
		return fkBulkTemplate;
	}
	public void setFkBulkTemplate(String fkBulkTemplate) {
		this.fkBulkTemplate = fkBulkTemplate;
	}
	public String getFkBulkEntityDet() {
		return fkBulkEntityDet;
	}
	public void setFkBulkEntityDet(String fkBulkEntityDet) {
		this.fkBulkEntityDet = fkBulkEntityDet;
	}
	public String getFileFldName() {
		return fileFldName;
	}
	public void setFileFldName(String fileFldName) {
		this.fileFldName = fileFldName;
	}
	public String getLastModBy() {
		return lastModBy;
	}
	public void setLastModBy(String lastModBy) {
		this.lastModBy = lastModBy;
	}
	public String getIpAdd() {
		return ipAdd;
	}
	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}
	public BulkTemplateDetailJB() {
        Rlog.debug("BulkTemplateDetailJB", "BulkTemplateDetailJB.BulkTemplateDetailJB() ");
    }
	public BulkTemplateDetailJB(int pkTemplateDet,String fkBulkTemplate,String fkBulkEntityDet,String fileFldName,String lastModBy,String creator,String ipAdd,String colnum,String rownum) {
		setPkTemplateDet(pkTemplateDet);
		setFkBulkTemplate(fkBulkTemplate);
		setFkBulkEntityDet(fkBulkEntityDet);
		setFileFldName(fileFldName);
		setLastModBy(lastModBy);
		setIpAdd(ipAdd);
		setColNum(colnum);
		setCreator(creator);
}
public BulkTemplateDetailBean createBulkTemplateDetStateKeeper() {
	Rlog.debug("bulkTemplateDet", "bulkTemplateDet.createBulkTemplateDetStateKeeper");
    return new BulkTemplateDetailBean(this.pkTemplateDet, this.fkBulkTemplate, this.fkBulkEntityDet,
    	    this.fileFldName,this.lastModBy,this.creator,this.ipAdd,this.colNum);
}

public BulkTemplateDetailBean getBulkTemplateDetDetails() {
	BulkTemplateDetailBean spsk = null;
    try {
    	BulkTempateDetailAgent bulkTempateDetailAgent = EJBUtil.getTemplateDetHome();
        spsk = bulkTempateDetailAgent.getBulkTemplateDetDetails(this.pkTemplateDet);
        Rlog.debug("bulkTemplateDet", "In BulkTemplateJB.getBulkTemplateDetails " + spsk);
    } catch (Exception e) {
        Rlog.fatal("bulkTemplateDet", "Error in getBulkTemplateDetDetails() in BulkTemplateDetailJB " + e);
    }
    if (spsk != null) {
    	this.pkTemplateDet = spsk.getPkTemplateDet();
	    this.fkBulkTemplate = spsk.getFkBulkTemplate();
	    this.fkBulkEntityDet = spsk.getFkBulkEntityDet();
	    this.fileFldName = spsk.getFileFldName();
	    this.lastModBy= spsk.getLastModBy();
	    this.ipAdd = spsk.getIpAdd();
	    this.colNum = spsk.getColNum();
	    this.creator=spsk.getCreator();
    }
    return spsk;
}

public int setBulkTemplateDetDetails() {
    int output = 0;
    try {
    	BulkTempateDetailAgent bulkTemplateAgent = EJBUtil.getTemplateDetHome();
        output = bulkTemplateAgent.setBulkTemplateDetDetails(this.createBulkTemplateDetStateKeeper());
        this.setPkTemplateDet(output);
    } catch (Exception e) {
        Rlog.fatal("bulkTemplateDet", "Error in setBulkTemplateDetDetails() in BulkTemplateDetailJB "   + e);
        return -2;
    }
    return output;
}
public int updateBulktemplateDet() {
    int output;
    try {
    	BulkTempateDetailAgent bulkTemplateAgent = EJBUtil.getTemplateDetHome();
        output = bulkTemplateAgent.updateBulktemplateDet(this.createBulkTemplateDetStateKeeper());
    } catch (Exception e) {
        Rlog.debug("bulkTemplateDet", "Exception setting updateBulktemplateDet details to the BulkTemplateJB" + e.getMessage());
        return -2;
    }
    return output;
}
}
