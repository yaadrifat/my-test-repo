package com.velos.eres.ctrp.service;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

import javax.ejb.Remote;

import com.velos.eres.ctrp.business.CtrpDraftBean;
import com.velos.eres.ctrp.business.CtrpDraftNotificationBean;
import com.velos.eres.ctrp.business.CtrpValidationDao;

@Remote
public interface CtrpDraftAgent {
	public CtrpDraftBean getCtrpDraftBean(Integer id);
	public Integer createCtrpDraft(CtrpDraftBean ctrpDraftBean);
	//Added By Parminder Singh:CTRP-22473
	public Integer createCtrpDraftNotification(CtrpDraftNotificationBean ctrpDraftNotificationBean);
	public int deleteDraftNotiUser(Integer statusId, String userId);
	public Integer updateCtrpDraft(CtrpDraftBean ctrpDraftBean);
	public CtrpDraftBean findByDraftId(Integer findByDraftId);
	public ArrayList findDraftsByStudyIds(Integer accId, String studyIds);
	public CtrpValidationDao runCtrpDraftValidation(Integer ctrpDraftId);
	public int unmarkStudyCtrpReportable(String studyIds);
	public int deleteStudyDrafts(String studyIds,String draftIds);
	//Modified for Bug#8029:Akshi
	public int deleteStudyDrafts(String studyIds,String draftIds,Hashtable<String, String> args);
	//22472
	public int fetchNextDraftNum(int studyId);
	public int deleteCtrpDrftStats(String ctrpDrftStatIds, String draftId);
	public Map<String, String> fetchStudyAutoComplete(String accId,String userId);
}
