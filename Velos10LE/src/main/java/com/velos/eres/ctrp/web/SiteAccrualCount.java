package com.velos.eres.ctrp.web;

public class SiteAccrualCount {

	private String accrualCount;
	private String studyIdentifier;
	private String studySiteIdentifier;
	private String studySiteAccrualCount;

	/**
	 * @return the accrualCount
	 */
	public String getAccrualCount() {
		return accrualCount;
	}

	/**
	 * @param accrualCount
	 *            the accrualCount to set
	 */
	public void setAccrualCount(String accrualCount) {
		this.accrualCount = accrualCount;
	}

	/**
	 * @return the studyIdentifier
	 */
	public String getStudyIdentifier() {
		return studyIdentifier;
	}

	/**
	 * @param studyIdentifier
	 *            the studyIdentifier to set
	 */
	public void setStudyIdentifier(String studyIdentifier) {
		this.studyIdentifier = studyIdentifier;
	}

	/**
	 * @return the studySiteIdentifier
	 */
	public String getStudySiteIdentifier() {
		return studySiteIdentifier;
	}

	/**
	 * @param studySiteIdentifier
	 *            the studySiteIdentifier to set
	 */
	public void setStudySiteIdentifier(String studySiteIdentifier) {
		this.studySiteIdentifier = studySiteIdentifier;
	}

	/**
	 * @return the studySiteAccrualCount
	 */
	public String getStudySiteAccrualCount() {
		return studySiteAccrualCount;
	}

	/**
	 * @param studySiteAccrualCount
	 *            the studySiteAccrualCount to set
	 */
	public void setStudySiteAccrualCount(String studySiteAccrualCount) {
		this.studySiteAccrualCount = studySiteAccrualCount;
	}
}
