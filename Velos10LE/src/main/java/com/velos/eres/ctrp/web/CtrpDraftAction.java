package com.velos.eres.ctrp.web;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.aithent.audittrail.reports.AuditUtils;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.CtrlDao;
import com.velos.eres.business.common.GroupDao;
import com.velos.eres.business.common.StudyINDIDEDAO;
import com.velos.eres.business.common.StudyNIHGrantDao;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.MC;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.SessionMaint;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.service.util.VelosResourceBundle;
import com.velos.eres.web.group.GroupJB;
import com.velos.eres.web.grpRights.GrpRightsJB;
import com.velos.eres.web.user.UserJB;

public class CtrpDraftAction extends ActionSupport  {
	private static final long serialVersionUID = -8607845443424729947L;
	private CtrpDraftJB ctrpDraftJB = null;
	private CtrpDraftSectSponsRespJB ctrpDraftSectSponsRespJB = null;
	private HttpServletRequest request = null;
	private HttpServletResponse response = null;
	private String errorMessage = null;
	private static final String CTRP_DRAFT_BROWSER_TILE = "ctrpDraftBrowserTile";
	private static final String CTRP_DRAFT_NON_INDUSTRIAL_TILE = "ctrpDraftNonIndustrialTile";
	private static final String ACCOUNT_ID = "accountId";
	private static final String USER_ID = "userId";
	private static final String IP_ADD = "ipAdd";
	private static final String E_SIGN = "eSign";
	private static final String DRAFT_ID = "draftId";
	private static final String STUDY_ID = "studyId";
	private static final String MESSAGE="message";
	private static final String CTRP_DRAFT_SEARCH="ctrpDraftSearch";
	private static final String CTRP_UNMARK="ctrpUnmark";
	private static final String CTRP_DRAFT_DELTE="draftDelete";
	private static final String CTRP_DRAFT_STUDIES="studyIds";
	private static final String CTRP_NONINDUSTRY_FLAG="nonIndustryFlag";
	private static final String CTRP_CSV_RESULT="result";
	private static final String CTRP_DRAFT_IDS ="selectedDraftIds";
	public static final String PROTOCOL_STATUS = "protocolStatus";
	public static final String STUDY_STATUS_COUNT = "statusCountVal";
	public static final String STUDY_STATUS_FK = "fKStudytatus";
	public static final String USER_FK = "userFK";
	public static final String DATA_NOT_SAVED = "dataNotSaved";
	public static final String DATA_NOT_DELETED = "dataNotDeleted";
	private Map errorMap = null;
	private Map userDataMap=null;
	private Collection errorAction = null;


	public CtrpDraftAction() {
		ActionContext ac = ActionContext.getContext();
		request = (HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
		response = (HttpServletResponse)ac.get(ServletActionContext.HTTP_RESPONSE);
		ctrpDraftJB = new CtrpDraftJB();
		ctrpDraftSectSponsRespJB = new CtrpDraftSectSponsRespJB();
		ctrpDraftJB.setCtrpDraftSectSponsorRespJB(ctrpDraftSectSponsRespJB);
	}
	
	public void setCtrpDraftJB(CtrpDraftJB ctrpDraftJB) {
		this.ctrpDraftJB = ctrpDraftJB;
	}
	
	public CtrpDraftJB getCtrpDraftJB() {
		return ctrpDraftJB;
	}
	
	public void setCtrpDraftSectSponsRespJB(CtrpDraftSectSponsRespJB ctrpDraftSectSponsRespJB) {
		this.ctrpDraftSectSponsRespJB = ctrpDraftSectSponsRespJB;
	}

	public CtrpDraftSectSponsRespJB getCtrpDraftSectSponsRespJB() {
		return ctrpDraftSectSponsRespJB;
	}
	
	public String getErrorMessage(){
		return this.errorMessage;
	}

	public void setErrorMessage(String errorMessage){
		this.errorMessage = errorMessage;
	}
	
	public String getCtrpDraftBrowser() {
		this.populateJB();
		
		try {
			ctrpDraftJB.setLoggedUser(Integer.parseInt((String)request.getSession(false).getAttribute(USER_ID)));
			ctrpDraftJB.getStatusUserList();
			request.removeAttribute(MESSAGE);
			String studyDraft= request.getParameter(CTRP_DRAFT_SEARCH);
			studyDraft=studyDraft==null?"1":studyDraft;
			String protocolStatus= request.getParameter(PROTOCOL_STATUS);
			protocolStatus=protocolStatus==null?"1":protocolStatus;
			int ctrpDraftSeach =Integer.parseInt(studyDraft);
			ctrpDraftJB.setStudyDraftsInt(ctrpDraftSeach);
			
			String ctrpUnmark=request.getParameter(CTRP_UNMARK);
			ctrpUnmark=ctrpUnmark==null?"":ctrpUnmark;
			String draftDelete=request.getParameter(CTRP_DRAFT_DELTE);
			draftDelete=draftDelete==null?"":draftDelete;
			String studyIds =request.getParameter(CTRP_DRAFT_STUDIES);
			studyIds=studyIds==null?"":"("+studyIds+")";
			String draftIds =request.getParameter("draftIds");
			draftIds=draftIds==null?"":"("+draftIds+")";
			if(!studyIds.equals(""))
			{
				int ret=0;
				if(ctrpUnmark.equals("1"))
				{ret=ctrpDraftJB.unmarkStudyCtrpReportable(studyIds);}
				
				if(draftDelete.equals("1"))
				{ret=ctrpDraftJB.deleteStudyDrafts(studyIds,draftIds,AuditUtils.createArgs(request.getSession(false),"",LC.Ctrp_DraftBrowser));}    //Added for Bug#8029, Akshi
				
				if(ret!=0)
				 {
					 request.setAttribute(MESSAGE, MC.M_Data_SavedSucc);
				 }else{
					 request.setAttribute(MESSAGE, MC.M_DataNtSvd_Succ);
				 }
				
			}
			
			
		} catch (Exception e) {
			System.out.println("Exception in getCtrpDraftBrowser ");
			e.printStackTrace();
		}
		return CTRP_DRAFT_BROWSER_TILE;
	}
		
	/**
	 * Added by Parminder Singh for CTRP-22473
	 * This method is used to delete data from Table
	 * 
	 */
	public String deleteCtrpNotificationUsers() {
		ctrpDraftJB.setStatusCountVal(request.getParameter(STUDY_STATUS_COUNT));
		ctrpDraftJB.setEmailUserId(request.getParameter(USER_FK));
		Integer output = ctrpDraftJB.deleteCtrpNontificationUsers();
		if(output<0)
		{
			Map<String, String> errorMap = new HashMap<String, String>();
			errorMap.put(DATA_NOT_DELETED, MC.M_DataNotDel_Succ);
			this.errorMap=errorMap;
			return INPUT;
		}

		return SUCCESS;
	}
	
	/**
	 * Added by Parminder Singh for CTRP-22473
	 * This method is used for persist data in Table
	 * 
	 */
	public String saveCtrpDraftNotification() {
		ctrpDraftJB.setStatusCountVal(request.getParameter(STUDY_STATUS_COUNT));
		ctrpDraftJB.setEmailUserId(request.getParameter(USER_FK));
		HashMap<String, String> argMap = new HashMap<String, String>();
		String userId = null;
		try {
			userId = (String)request.getSession(false).getAttribute(USER_ID);
		} finally {
			userId = StringUtil.trueValue(userId);
		}
		argMap.put(USER_ID, userId);
		String ipAdd = null;
		try {
			ipAdd = (String)request.getSession(false).getAttribute(IP_ADD);
		} finally {
			ipAdd = StringUtil.trueValue(ipAdd);
		}
		argMap.put(IP_ADD, ipAdd);
		Integer output = ctrpDraftJB.updateCtrpDraftNontification(argMap);
		if(output<0)
		{	Map<String, String> errorMap = new HashMap<String, String>();
			errorMap.put(DATA_NOT_SAVED, MC.M_DataNotSvd_Succ);
			this.errorMap=errorMap;
			return INPUT;
		}	
		return SUCCESS;
	}
	
	public String getCtrpDraftNonIndustrial() {
		String accountId = (String)request.getSession(false).getAttribute(ACCOUNT_ID);
			//YK:Fixed Bug #8617
			String userId = null;
			try {
				userId = (String)request.getSession(false).getAttribute(USER_ID);
			} finally {
				userId = StringUtil.trueValue(userId);
			}
			ctrpDraftJB.setLoggedUser(Integer.parseInt(userId));
			
		if (request.getParameter(DRAFT_ID) != null) {
			ctrpDraftJB.retrieveCtrpDraft(StringUtil.stringToNum(request.getParameter(DRAFT_ID)));
			ctrpDraftJB.retreiveCtrpDocumentInfo(StringUtil.stringToNum(request.getParameter(DRAFT_ID)));
			ctrpDraftJB.retrieveStudy(StringUtil.stringToNum(ctrpDraftJB.getFkStudy()));
		} else if (request.getParameter(STUDY_ID) != null) {
			ctrpDraftJB.retrieveStudy(StringUtil.stringToNum(request.getParameter(STUDY_ID)));
		}
		return CTRP_DRAFT_NON_INDUSTRIAL_TILE;
	}
	
	public String updateCtrpDraftNonIndustrial() {
		ctrpDraftJB.setTrialSubmTypeInt(
				StringUtil.stringToNum(request.getParameter(CtrpDraftJB.FLD_SUBM_TYPE)));
		ctrpDraftJB.setDraftStudyPhase(
				StringUtil.stringToNum(request.getParameter(CtrpDraftJB.FLD_STUDY_PHASE)));
		ctrpDraftJB.setStudyPurpose(
				StringUtil.stringToNum(request.getParameter(CtrpDraftJB.FLD_STUDY_PURPOSE)));
		ctrpDraftJB.setLeadOrgTypeInt(
				StringUtil.stringToNum(request.getParameter(CtrpDraftJB.FLD_LEAD_ORG_TYPE)));
		ctrpDraftJB.setSumm4Type(request.getParameter(CtrpDraftJB.FLD_SUMM4_TYPE));
		ctrpDraftJB.setSumm4TypeInt(
				StringUtil.stringToNum(request.getParameter(CtrpDraftJB.FLD_SUMM4_TYPE_INT)));
		ctrpDraftJB.setTrialStatus(StringUtil.stringToNum(request.getParameter(CtrpDraftJB.FLD_TRIAL_STATUS)));
		
		//22472
		ctrpDraftJB.setCtrpNciProcStatId(
				StringUtil.stringToNum(request.getParameter(CtrpDraftJB.FLD_CTRP_NCI_PROC_STATUS)));
		
		validateCtrpDraftNonIndustrialBasic();
		validateESignForUpdateCtrpDraft();
		if (hasFieldErrors()) {
			Map<String, List<String>> errorMap = getFieldErrors();
			this.errorMap = errorMap;
			return INPUT;
		}
		HashMap<String, String> argMap = new HashMap<String, String>();
		String userId = null;
		try {
			userId = (String)request.getSession(false).getAttribute(USER_ID);
		} finally {
			userId = StringUtil.trueValue(userId);
		}
		argMap.put(USER_ID, userId);
		String ipAdd = null;
		try {
			ipAdd = (String)request.getSession(false).getAttribute(IP_ADD);
		} finally {
			ipAdd = StringUtil.trueValue(ipAdd);
		}
		argMap.put(IP_ADD, ipAdd);
		argMap.put(CtrpDraftJB.STATUS_CODE_ID, StringUtil.trueValue(request.getParameter(CtrpDraftJB.FLD_DRAFT_STATUS)));
		
		//22472
		argMap.put(CtrpDraftJB.FLD_CTRP_NCI_PROC_STATUS, StringUtil.trueValue(request.getParameter(CtrpDraftJB.FLD_CTRP_NCI_PROC_STATUS)));
		argMap.put(CtrpDraftJB.FLD_NCI_TRIAL_STATUS_DATE, StringUtil.trueValue(request.getParameter(CtrpDraftJB.FLD_NCI_TRIAL_STATUS_DATE)));
		
		Integer output = ctrpDraftJB.updateCtrpDraftNonIndustrial(argMap);
		if (output < 0) {
			addActionError(MC.M_Err_SavingData);
			this.errorAction = getActionErrors();
			for (Object errorObj : this.errorAction){
				this.errorMessage = errorObj.toString();
				System.out.println(this.errorMessage);
				break;
			}
			return ERROR;
		}
		if (output == 0) { return NONE; }
		//Ak:Added the Trial Documents functionality
		if (output > 0){
			  Integer trailOutput=0;
			  trailOutput = ctrpDraftJB.setCtrpDraftDocinfo(argMap,output);
				  if (trailOutput < 0) { return ERROR; }	
		}
		this.errorMap = new HashMap();
		return SUCCESS;
	}
	
	public String validateCtrpDraftNonIndustrialBasic() {
		String[] dateFormatParams = { DateUtil.getAppDateFormat() };
		if (ctrpDraftJB.getTrialSubmTypeA().equals(request.getParameter(CtrpDraftJB.FLD_SUBM_TYPE))) {
			if (!StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_AMEND_DATE))) {
				if (!DateUtil.isValidDateFormat(request.getParameter(CtrpDraftJB.FLD_AMEND_DATE))) {
					addFieldError(CtrpDraftJB.FLD_AMEND_DATE, 
							VelosResourceBundle.getMessageString("M_IvdDt_PlsVldFmt", (Object[])dateFormatParams));
				}
			}
		}
		if (!StringUtil.isEmpty((request.getParameter(CtrpDraftJB.FLD_TRIAL_STATUS_DATE)))) {
			if (!DateUtil.isValidDateFormat(request.getParameter(CtrpDraftJB.FLD_TRIAL_STATUS_DATE))) {
				addFieldError(CtrpDraftJB.FLD_TRIAL_STATUS_DATE, 
						VelosResourceBundle.getMessageString("M_IvdDt_PlsVldFmt", (Object[])dateFormatParams));
			}
		}
		if (!StringUtil.isEmpty((request.getParameter(CtrpDraftJB.FLD_TRIAL_START_DATE)))) {
			if (!DateUtil.isValidDateFormat(request.getParameter(CtrpDraftJB.FLD_TRIAL_START_DATE))) {
				addFieldError(CtrpDraftJB.FLD_TRIAL_START_DATE, 
						VelosResourceBundle.getMessageString("M_IvdDt_PlsVldFmt", (Object[])dateFormatParams));
			}
		}
		if (!StringUtil.isEmpty((request.getParameter(CtrpDraftJB.FLD_PRIMARY_COMPLETION_DATE)))) {
			if (!DateUtil.isValidDateFormat(request.getParameter(CtrpDraftJB.FLD_PRIMARY_COMPLETION_DATE))) {
				addFieldError(CtrpDraftJB.FLD_PRIMARY_COMPLETION_DATE, 
						VelosResourceBundle.getMessageString("M_IvdDt_PlsVldFmt", (Object[])dateFormatParams));
			}
		}
		if ("1".equals(request.getParameter("ctrpDraftJB.draftXmlRequired"))) {
			if (!StringUtil.isEmpty(request.getParameter(CtrpDraftSectSponsRespJB.FLD_RESP_PARTY_EXT))) {
				if (((String)(request.getParameter(CtrpDraftSectSponsRespJB.FLD_RESP_PARTY_EXT))).matches(".*[^0-9].*")) {
					addFieldError(CtrpDraftSectSponsRespJB.FLD_RESP_PARTY_EXT, MC.M_Etr_NumValueForExt);
				}
			}
		}
		validateESignForUpdateCtrpDraft();
		if (hasFieldErrors()) {
			Map<String, List<String>> errorMap = getFieldErrors();
			this.errorMap = errorMap;
			return INPUT;
		}
		this.errorMap = new HashMap();
		return SUCCESS;
	}
	
	public String validateCtrpDraftNonIndustrialBusiness() {
		if (ctrpDraftJB == null) { new CtrpDraftJB(); }
		ctrpDraftJB.setTrialSubmTypeInt(
				StringUtil.stringToNum(request.getParameter(CtrpDraftJB.FLD_SUBM_TYPE)));
		ctrpDraftJB.setDraftStudyPhase(
				StringUtil.stringToNum(request.getParameter(CtrpDraftJB.FLD_STUDY_PHASE)));
		ctrpDraftJB.setStudyPurpose(
				StringUtil.stringToNum(request.getParameter(CtrpDraftJB.FLD_STUDY_PURPOSE)));
		ctrpDraftJB.setLeadOrgTypeInt(
				StringUtil.stringToNum(request.getParameter(CtrpDraftJB.FLD_LEAD_ORG_TYPE)));
		ctrpDraftJB.setSumm4Type(request.getParameter(CtrpDraftJB.FLD_SUMM4_TYPE));
		ctrpDraftJB.setSumm4TypeInt(
				StringUtil.stringToNum(request.getParameter(CtrpDraftJB.FLD_SUMM4_TYPE_INT)));
		validateSectSubmTypeForUpdateCtrpDraft();
		validateSectTrialIdsForUpdateCtrpDraft();
		validateSectTrialDetailsForUpdateCtrpDraft();
		validateSectLeadOrgPiForUpdateCtrpDraft();
		validateSectSponsRespForUpdateCtrpDraft();
		validateSectSumm4ForUpdateCtrpDraft();
		//validateSectNihGrantsForUpdateCtrpDraft(); //FIX #8586
		validateSectStatusDatesForUpdateCtrpDraft();//Added for CTRP-20579, Ankit
		//validateSectIndIdeForUpdateCtrpDraft(); //FIX #8586
		validateSectRegulatoryInfoUpdateCtrpDraft();
		validateSecTrialDocUpdateCtrpDraft(); //Ak:Added for Trial Documents
		if (hasFieldErrors()) {
			Map<String, List<String>> errorMap = getFieldErrors();
			this.errorMap = errorMap;
			return INPUT;
		}
		this.errorMap = new HashMap();
		return SUCCESS;
	}
	
	private void validateSectLeadOrgPiForUpdateCtrpDraft() {
		if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_LEAD_ORG_ID)) &&
				StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_LEAD_ORG_FK_SITE))) {
			addFieldError(CtrpDraftJB.FLD_LEAD_ORG_ID, MC.CTRP_PerformThisOperation);
		}
		if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_LEAD_ORG_ID)) &&
				!StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_LEAD_ORG_FK_SITE))){
			Integer fkAddress = StringUtil.stringToInteger(request.getParameter(CtrpDraftJB.FLD_LEAD_ORG_FK_ADD));
			if (fkAddress == null || fkAddress < 0){
				addFieldError(CtrpDraftJB.FLD_LEAD_ORG_ID, MC.CTRP_CompleteAddressReqd);
			}else{
				validateAddressForCtrpDraftNonIndustrial(CtrpDraftJB.FLD_LEAD_ORG_ID,"LEAD_ORG");
			}
		}
		
		if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_PI_ID)) &&
				StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_PI_FK_USER))) {
			addFieldError(CtrpDraftJB.FLD_PI_ID, MC.CTRP_PerformThisOperation);
		}
		if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_PI_ID)) &&
				!StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_PI_FK_USER))){
			Integer fkAddress = StringUtil.stringToInteger(request.getParameter(CtrpDraftJB.FLD_PI_FK_ADD));
			if (fkAddress == null || fkAddress < 0){
				addFieldError(CtrpDraftJB.FLD_PI_ID, MC.CTRP_CompleteAddressReqd);
			}else{
				validateAddressForCtrpDraftNonIndustrial(CtrpDraftJB.FLD_PI_ID,"PI");
			}
		}
	}
	
	private void validateSectSumm4ForUpdateCtrpDraft() {
		if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_SUMM4_ID)) &&
				StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_SUMM4_FK_SITE))) {
			addFieldError(CtrpDraftJB.FLD_SUMM4_ID, MC.CTRP_PerformThisOperation);
		}
		if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_SUMM4_ID)) &&
				!StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_SUMM4_FK_SITE))){
			Integer fkAddress = StringUtil.stringToInteger(request.getParameter(CtrpDraftJB.FLD_SUMM4_FK_ADD));
			if (fkAddress == null || fkAddress < 0){
				addFieldError(CtrpDraftJB.FLD_SUMM4_ID, MC.CTRP_CompleteAddressReqd);
			}else{
				validateAddressForCtrpDraftNonIndustrial(CtrpDraftJB.FLD_SUMM4_ID,"SUMM4");
			}
		}
	}
	private void validateSectNihGrantsForUpdateCtrpDraft() {
		try {
			int studyId = StringUtil.stringToNum(ctrpDraftJB.getFkStudy());
			
			StudyNIHGrantDao nihGrantDao = new StudyNIHGrantDao();
			nihGrantDao.getStudyNIHGrantRecords(studyId);

			ArrayList pkStudyNIhGrants = nihGrantDao.getPkStudyNIhGrants();
			if (pkStudyNIhGrants == null || pkStudyNIhGrants.size() <= 0) {
				addFieldError(CtrpDraftJB.FLD_NIH_GRANTS, MC.CTRP_NIHGrantInfoReqd);
			}
		} catch (Exception e) {
			Rlog.fatal("ctrp",
					"Exception in CtrpDraftAction.validateSectNihGrantsForUpdateCtrpDraft: " + e);
		}
	}
	//Ak:Added for Trial Documents
	private void validateSecTrialDocUpdateCtrpDraft() {
		String thisSubmTypeCodeType = 
			StringUtil.trueValue(request.getParameter(CtrpDraftJB.FLD_SUBM_TYPE));
		if (thisSubmTypeCodeType.equals(ctrpDraftJB.getTrialSubmTypeA())) {
			if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_CTRPDOC_PROTOCOLDOC_DESC))) {
				addFieldError(CtrpDraftJB.FLD_CTRPDOC_PROTOCOLDOC_DESC, MC.M_FieldRequired);
			}
			if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_CTRP_IRBAPPROVAL_DESC))) {
				addFieldError(CtrpDraftJB.FLD_CTRP_IRBAPPROVAL_DESC, MC.M_FieldRequired);
			}
			if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_CTRP_CHANGEMEMODOC_DESC))) {
				addFieldError(CtrpDraftJB.FLD_CTRP_CHANGEMEMODOC_DESC, MC.M_FieldRequired);
			}
		} else if (thisSubmTypeCodeType.equals(ctrpDraftJB.getTrialSubmTypeO())) {
			if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_CTRPDOC_PROTOCOLDOC_DESC))) {
				addFieldError(CtrpDraftJB.FLD_CTRPDOC_PROTOCOLDOC_DESC, MC.M_FieldRequired);
			}
			if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_CTRP_IRBAPPROVAL_DESC))) {
				addFieldError(CtrpDraftJB.FLD_CTRP_IRBAPPROVAL_DESC, MC.M_FieldRequired);
			}
		}
		
		//Start Validations for Non-Industrial Trial Documents format : Added by Raviesh for Bug#8844   
		if (null != StringUtil.stringToInteger(request.getParameter(CtrpDraftJB.FLD_PROT_DOC_PK))){
			if (Integer.parseInt(request.getParameter(CtrpDraftJB.FLD_PROT_DOC_PK))>0 ) {
				String fileName = request.getParameter(CtrpDraftJB.FLD_CTRPDOC_PROTOCOLDOC_DESC);
				fileName = fileName.substring(fileName.lastIndexOf("."));
				
				if( fileName.indexOf(".doc")==-1 &&
						fileName.indexOf(".xls")==-1 &&
						fileName.indexOf(".pdf")==-1 &&
						fileName.indexOf(".wp")==-1)
				{
				 addFieldError(CtrpDraftJB.FLD_CTRPDOC_PROTOCOLDOC_DESC,MC.CTRP_DraftPlsChkDocType);
				}
			}
		}
		if (null != StringUtil.stringToInteger(request.getParameter(CtrpDraftJB.FLD_IRB_APPR_PK))){
			if (Integer.parseInt(request.getParameter(CtrpDraftJB.FLD_IRB_APPR_PK))>0 ) {
				
				String fileName = request.getParameter(CtrpDraftJB.FLD_CTRP_IRBAPPROVAL_DESC);
				fileName = fileName.substring(fileName.lastIndexOf("."));
				if( fileName.indexOf(".doc")==-1 &&
						fileName.indexOf(".xls")==-1 &&
						fileName.indexOf(".pdf")==-1 &&
						fileName.indexOf(".wp")==-1)
				{
				 addFieldError(CtrpDraftJB.FLD_CTRP_IRBAPPROVAL_DESC,MC.CTRP_DraftPlsChkDocType);
				}
			}
		}
		if (null != StringUtil.stringToInteger(request.getParameter(CtrpDraftJB.FLD_PART_SITES_LIST_PK))){
			if (Integer.parseInt(request.getParameter(CtrpDraftJB.FLD_PART_SITES_LIST_PK))>0 ) {
				
				String fileName = request.getParameter(CtrpDraftJB.FLD_CTRP_PARTSITESLIST_DESC);
				fileName = fileName.substring(fileName.lastIndexOf("."));
				
				if( fileName.indexOf(".doc")==-1 &&
						fileName.indexOf(".xls")==-1 &&
						fileName.indexOf(".pdf")==-1 &&
						fileName.indexOf(".wp")==-1)
				{
				 addFieldError(CtrpDraftJB.FLD_CTRP_PARTSITESLIST_DESC,MC.CTRP_DraftPlsChkDocType);
				}
			}
		}
		if (null != StringUtil.stringToInteger(request.getParameter(CtrpDraftJB.FLD_INFORM_CONSENT_PK))){
			if (Integer.parseInt(request.getParameter(CtrpDraftJB.FLD_INFORM_CONSENT_PK))>0 ) {
				
				String fileName = request.getParameter(CtrpDraftJB.FLD_CTRP_INFORMEDCONSENT_DOC_DESC);
				fileName = fileName.substring(fileName.lastIndexOf("."));
				
				if( fileName.indexOf(".doc")==-1 &&
						fileName.indexOf(".xls")==-1 &&
						fileName.indexOf(".pdf")==-1 &&
						fileName.indexOf(".wp")==-1)
				{
				 addFieldError(CtrpDraftJB.FLD_CTRP_INFORMEDCONSENT_DOC_DESC,MC.CTRP_DraftPlsChkDocType);
				}
			}
		}
		if (null != StringUtil.stringToInteger(request.getParameter(CtrpDraftJB.FLD_CTRP_OTHER_DOC_PK))){
			if (Integer.parseInt(request.getParameter(CtrpDraftJB.FLD_CTRP_OTHER_DOC_PK))>0 ) {
				
				String fileName = request.getParameter(CtrpDraftJB.FLD_CTRP_OTHER_DOC_DESC);
				fileName = fileName.substring(fileName.lastIndexOf("."));
				
				if( fileName.indexOf(".doc")==-1 &&
						fileName.indexOf(".xls")==-1 &&
						fileName.indexOf(".pdf")==-1 &&
						fileName.indexOf(".wp")==-1)
				{
				 addFieldError(CtrpDraftJB.FLD_CTRP_OTHER_DOC_DESC,MC.CTRP_DraftPlsChkDocType);
				}
			}
		}
		if (null != StringUtil.stringToInteger(request.getParameter(CtrpDraftJB.FLD_CHANGE_MEMO_DOC_PK))){
			if (Integer.parseInt(request.getParameter(CtrpDraftJB.FLD_CHANGE_MEMO_DOC_PK))>0 ) {
				
				String fileName = request.getParameter(CtrpDraftJB.FLD_CTRP_CHANGEMEMODOC_DESC);
				fileName = fileName.substring(fileName.lastIndexOf("."));
				
				if( fileName.indexOf(".doc")==-1 &&
						fileName.indexOf(".xls")==-1 &&
						fileName.indexOf(".pdf")==-1 &&
						fileName.indexOf(".wp")==-1)
				{
				 addFieldError(CtrpDraftJB.FLD_CTRP_CHANGEMEMODOC_DESC,MC.CTRP_DraftPlsChkDocType);
				}
			}
		}
		if (null != StringUtil.stringToInteger(request.getParameter(CtrpDraftJB.FLD_PROT_HIGHLIGHT_PK))){
			if (Integer.parseInt(request.getParameter(CtrpDraftJB.FLD_PROT_HIGHLIGHT_PK))>0 ) {
				
				String fileName = request.getParameter(CtrpDraftJB.FLD_CTRP_PROTHIGHLIGHT_DOC_DESC);
				fileName = fileName.substring(fileName.lastIndexOf("."));
				
				if( fileName.indexOf(".doc")==-1 &&
						fileName.indexOf(".xls")==-1 &&
						fileName.indexOf(".pdf")==-1 &&
						fileName.indexOf(".wp")==-1)
				{
				 addFieldError(CtrpDraftJB.FLD_CTRP_PROTHIGHLIGHT_DOC_DESC,MC.CTRP_DraftPlsChkDocType);
				}
			}
		}
		//End Validations for Non-Industrial Trial Documents format
		
	}

	// Regulatory Info Section validations :AGodara
	private void validateSectRegulatoryInfoUpdateCtrpDraft(){

		// Date:21-Dec-2011 Bug:8019   @Ankit		
		if (ctrpDraftJB.getDraftXmlRequired()!= null && ctrpDraftJB.getDraftXmlRequired()==1) {			

			if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_OVERSIGHTCOUNTRY))){
			addFieldError(CtrpDraftJB.FLD_OVERSIGHTCOUNTRY, MC.M_FieldRequired);
			}
			if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_OVERSIGHTORGANIZATION))){
				addFieldError(CtrpDraftJB.FLD_OVERSIGHTORGANIZATION, MC.M_FieldRequired);
			}
			
			if (("-1").equalsIgnoreCase(request.getParameter("ctrpDraftJB.fdaIntvenIndicator"))){
				addFieldError("ctrpDraftJB.fdaIntvenIndicator", MC.M_FieldRequired);
			}else if(("1").equalsIgnoreCase(request.getParameter("ctrpDraftJB.fdaIntvenIndicator"))){// checking if select "Yes"
				
				if (("-1").equalsIgnoreCase(request.getParameter("ctrpDraftJB.section801Indicator"))){
					addFieldError("ctrpDraftJB.section801Indicator", MC.M_FieldRequired);
				}else if (("1").equalsIgnoreCase(request.getParameter("ctrpDraftJB.section801Indicator"))){// checking if select "Yes"
					
					if (("-1").equalsIgnoreCase(request.getParameter("ctrpDraftJB.delayPostIndicator"))){
						addFieldError("ctrpDraftJB.delayPostIndicator", MC.M_FieldRequired);
					}	
				} else if(("0").equalsIgnoreCase(request.getParameter("ctrpDraftJB.section801Indicator"))){// checking if select "NO"
					ctrpDraftJB.setDelayPostIndicator(null);
				}
			}else if(("0").equalsIgnoreCase(request.getParameter("ctrpDraftJB.fdaIntvenIndicator"))){// checking if select "NO"
				ctrpDraftJB.setSection801Indicator(null);
				ctrpDraftJB.setDelayPostIndicator(null);
			}
		}else{
			
			ctrpDraftJB.setDelayPostIndicator(null);
			ctrpDraftJB.setFdaIntvenIndicator(null);
			ctrpDraftJB.setOversightCountry(null);
			ctrpDraftJB.setOversightOrganization(null);
			ctrpDraftJB.setSection801Indicator(null);
			ctrpDraftJB.setDelayPostIndicator(null);
			ctrpDraftJB.setDmAppointIndicator(null);
			
		}
	}
	
	private void validateSectSubmTypeForUpdateCtrpDraft() {
		String s1 = null;
		if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_SUBM_TYPE))) {
			addFieldError(CtrpDraftJB.FLD_SUBM_TYPE, MC.M_FieldRequired);
		} else if (ctrpDraftJB.getTrialSubmTypeA().equals(request.getParameter(CtrpDraftJB.FLD_SUBM_TYPE))) {
			if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_AMEND_NUM))) {
				addFieldError(CtrpDraftJB.FLD_AMEND_NUM, MC.M_FieldRequired);
			}
			if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_AMEND_DATE))) {
				addFieldError(CtrpDraftJB.FLD_AMEND_DATE, MC.M_FieldRequired);
			} else {
				s1 = StringUtil.trueValue(request.getParameter(CtrpDraftJB.FLD_AMEND_DATE));
				if (!DateUtil.isValidDateFormat(s1)) {
					String[] strParam = { DateUtil.getAppDateFormat() };
					addFieldError(CtrpDraftJB.FLD_AMEND_DATE, 
							VelosResourceBundle.getMessageString("M_IvdDt_PlsVldFmt", (Object[])strParam));
				}
			}
		}
		s1 = StringUtil.trueValue(request.getParameter(CtrpDraftJB.FLD_AMEND_NUM));
		if (s1.getBytes().length > 20) {
			Integer[] intParam = { 20 };
			addFieldError(CtrpDraftJB.FLD_AMEND_NUM,
					VelosResourceBundle.getMessageString("M_Etr_NumValueForExceedsBytes", (Object[])intParam));
		}

		if (!StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_SUBM_TYPE)) 
			&& (ctrpDraftJB.getTrialSubmTypeO().equals(request.getParameter(CtrpDraftJB.FLD_SUBM_TYPE))
				|| ctrpDraftJB.getTrialSubmTypeA().equals(request.getParameter(CtrpDraftJB.FLD_SUBM_TYPE)))) {
			if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_XML_REQD))) {
				addFieldError(CtrpDraftJB.FLD_XML_REQD, MC.M_FieldRequired);
			}
		}
	}
	
	private void validateSectTrialIdsForUpdateCtrpDraft() {
		String thisSubmTypeCodeType = 
			StringUtil.trueValue(request.getParameter(CtrpDraftJB.FLD_SUBM_TYPE));
		if (thisSubmTypeCodeType.equals(ctrpDraftJB.getTrialSubmTypeA())) {
		  /*if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_NCI_TRIAL_ID))) {
				addFieldError(CtrpDraftJB.FLD_NCI_TRIAL_ID, MC.M_FieldRequired);
			}*/
			if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_ORG_TRIAL_ID))) {
				addFieldError(CtrpDraftJB.FLD_ORG_TRIAL_ID, MC.M_FieldRequired);
			}
		} else if (thisSubmTypeCodeType.equals(ctrpDraftJB.getTrialSubmTypeO())) {
			if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_ORG_TRIAL_ID))) {
				addFieldError(CtrpDraftJB.FLD_ORG_TRIAL_ID, MC.M_FieldRequired);
			}
		}
		/*else if (thisSubmTypeCodeType.equals(ctrpDraftJB.getTrialSubmTypeU())) {
			if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_NCI_TRIAL_ID))) {
				addFieldError(CtrpDraftJB.FLD_NCI_TRIAL_ID, MC.M_FieldRequired);
			}
		}*/
		String s1 = StringUtil.trueValue(request.getParameter(CtrpDraftJB.FLD_NCI_TRIAL_ID));
		if (s1.getBytes().length > 100) {
			Integer[] intParam = { 100 };
			addFieldError(CtrpDraftJB.FLD_NCI_TRIAL_ID, 
					VelosResourceBundle.getMessageString("M_Etr_NumValueForExceedsBytes", (Object[])intParam));
		}
		s1 = StringUtil.trueValue(request.getParameter(CtrpDraftJB.FLD_ORG_TRIAL_ID));
		if (s1.getBytes().length > 100) {
			Integer[] intParam = { 100 };
			addFieldError(CtrpDraftJB.FLD_ORG_TRIAL_ID, 
					VelosResourceBundle.getMessageString("M_Etr_NumValueForExceedsBytes", (Object[])intParam));
		}
		s1 = StringUtil.trueValue(request.getParameter(CtrpDraftJB.FLD_NCT_NUM));
		if (s1.getBytes().length > 20) {
			Integer[] intParam = { 20 };
			addFieldError(CtrpDraftJB.FLD_NCT_NUM,
					VelosResourceBundle.getMessageString("M_Etr_NumValueForExceedsBytes", (Object[])intParam));
		}
		s1 = StringUtil.trueValue(request.getParameter(CtrpDraftJB.FLD_OTHER_TRIAL_ID));
		if (s1.getBytes().length > 100) {
			Integer[] intParam = { 100 };
			addFieldError(CtrpDraftJB.FLD_OTHER_TRIAL_ID, 
					VelosResourceBundle.getMessageString("M_Etr_NumValueForExceedsBytes", (Object[])intParam));
		}
	}
	
	private void validateSectTrialDetailsForUpdateCtrpDraft() {
		if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_STUDY_PHASE))) {
			addFieldError(CtrpDraftJB.FLD_STUDY_PHASE, MC.M_FieldRequired);
		}
		if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_STUDY_TYPE))) {
			addFieldError(CtrpDraftJB.FLD_STUDY_TYPE, MC.M_FieldRequired);
		}
		if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_STUDY_PURPOSE))) {
			addFieldError(CtrpDraftJB.FLD_STUDY_PURPOSE, MC.M_FieldRequired);
		} else if (ctrpDraftJB.getStudyPurposeOtherPk().equals(
				request.getParameter(CtrpDraftJB.FLD_STUDY_PURPOSE))) {
			if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_STUDY_PURPOSE_OTHER))) {
				addFieldError(CtrpDraftJB.FLD_STUDY_PURPOSE_OTHER, MC.M_FieldRequired);
			}
			String s1 = StringUtil.trueValue(request.getParameter(CtrpDraftJB.FLD_STUDY_PURPOSE_OTHER));
			if (s1.getBytes().length > 1000) {
				Integer[] intParam = { 1000 };
				addFieldError(CtrpDraftJB.FLD_STUDY_PURPOSE_OTHER, 
						VelosResourceBundle.getMessageString("M_Etr_NumValueForExceedsBytes", (Object[])intParam));
			}
		}
	}
	
	private void validateSectSponsRespForUpdateCtrpDraft() {
		if (!"1".equals(request.getParameter("ctrpDraftJB.draftXmlRequired"))) {
			return; // For XML not required, this validation is unnecessary
		}
		if (StringUtil.isEmpty(request.getParameter(CtrpDraftSectSponsRespJB.FLD_SPONS_ID)) &&
				StringUtil.isEmpty(request.getParameter(CtrpDraftSectSponsRespJB.FLD_SPONS_FK_SITE))) {
			addFieldError(CtrpDraftSectSponsRespJB.FLD_SPONS_ID, MC.CTRP_PerformThisOperation);
		}
		if (StringUtil.isEmpty(request.getParameter(CtrpDraftSectSponsRespJB.FLD_SPONS_ID)) &&
				!StringUtil.isEmpty(request.getParameter(CtrpDraftSectSponsRespJB.FLD_SPONS_FK_SITE))){
			Integer fkAddress = StringUtil.stringToInteger(request.getParameter(CtrpDraftSectSponsRespJB.FLD_SPONS_FK_ADD));
			if (fkAddress == null || fkAddress < 0){
				addFieldError(CtrpDraftSectSponsRespJB.FLD_SPONS_ID, MC.CTRP_CompleteAddressReqd);
			}else{
				validateAddressForCtrpDraftNonIndustrial(CtrpDraftSectSponsRespJB.FLD_SPONS_ID,"SPONS");
			}
		}
		
		if (StringUtil.isEmpty(request.getParameter(CtrpDraftSectSponsRespJB.FLD_RESP_PARTY))) {
			addFieldError(CtrpDraftSectSponsRespJB.FLD_RESP_PARTY, MC.M_FieldRequired);
		}
		if (StringUtil.isEmpty(request.getParameter(CtrpDraftSectSponsRespJB.FLD_RESP_PARTY_EMAIL))) {
			addFieldError(CtrpDraftSectSponsRespJB.FLD_RESP_PARTY_EMAIL, MC.M_FieldRequired);
		}
		if (StringUtil.isEmpty(request.getParameter(CtrpDraftSectSponsRespJB.FLD_RESP_PARTY_PHONE))) {
			addFieldError(CtrpDraftSectSponsRespJB.FLD_RESP_PARTY_PHONE, MC.M_FieldRequired);
		}
		if ("1".equals(request.getParameter("ctrpDraftJB.draftXmlRequired"))) {
			if (!StringUtil.isEmpty(request.getParameter(CtrpDraftSectSponsRespJB.FLD_RESP_PARTY_EXT))) {
				if (((String)(request.getParameter(CtrpDraftSectSponsRespJB.FLD_RESP_PARTY_EXT))).matches(".*[^0-9].*")) {
					addFieldError(CtrpDraftSectSponsRespJB.FLD_RESP_PARTY_EXT, MC.M_Etr_NumValueForExt);
				}
			}
		}
		if ("1".equals(request.getParameter(CtrpDraftSectSponsRespJB.FLD_SPONS_CONTACT_TYPE))) {
			if (StringUtil.isEmpty(request.getParameter(CtrpDraftSectSponsRespJB.FLD_PER_CONTACT_ID)) &&
					StringUtil.isEmpty(request.getParameter(CtrpDraftSectSponsRespJB.FLD_PER_CONTACT_FK_USER))) {
				addFieldError(CtrpDraftSectSponsRespJB.FLD_PER_CONTACT_ID, MC.CTRP_PerformThisOperation);
			}else{
				if (StringUtil.isEmpty(request.getParameter(CtrpDraftSectSponsRespJB.FLD_PER_CONTACT_ID)) &&
						!StringUtil.isEmpty(request.getParameter(CtrpDraftSectSponsRespJB.FLD_PER_CONTACT_FK_USER))){
					Integer fkAddress = StringUtil.stringToInteger(request.getParameter(CtrpDraftSectSponsRespJB.FLD_PER_CONTACT_FK_ADD));
					if (fkAddress == null || fkAddress < 0){
						addFieldError(CtrpDraftSectSponsRespJB.FLD_PER_CONTACT_ID, MC.CTRP_CompleteAddressReqd);
					}else{
						validateAddressForCtrpDraftNonIndustrial(CtrpDraftSectSponsRespJB.FLD_PER_CONTACT_ID,"PER_CONTACT");
					}
				}
			}
		} else if ("0".equals(request.getParameter(CtrpDraftSectSponsRespJB.FLD_SPONS_CONTACT_TYPE))) {
			if (StringUtil.isEmpty(request.getParameter(CtrpDraftSectSponsRespJB.FLD_GEN_CONTACT_ID))) {
				addFieldError(CtrpDraftSectSponsRespJB.FLD_GEN_CONTACT_ID, MC.M_FieldRequired);
			}
		}
	}
	
	private void validateESignForUpdateCtrpDraft() {
		if (StringUtil.isEmpty(request.getParameter(E_SIGN))) { 
			addFieldError(CtrpDraftJB.FLD_E_SIGN, MC.M_Etr_Esign_ToProc);
		} else if (request.getSession(false).getAttribute(E_SIGN) != null) {
			if (!((String)request.getSession(false).getAttribute(E_SIGN))
					.equals(request.getParameter(E_SIGN))) {
				addFieldError(CtrpDraftJB.FLD_E_SIGN, MC.M_IncorrEsign_EtrAgain);
			}
		}
	}
	
	//Added for CTRP-20579, Ankit
	private void validateSectStatusDatesForUpdateCtrpDraft() {
		String[] dateFormatParams = { DateUtil.getAppDateFormat() };
		String s1 = null;
		
		s1 = StringUtil.trueValue(request.getParameter(CtrpDraftJB.FLD_TRIAL_STATUS));
		if (StringUtil.isEmpty(s1)) {
			addFieldError(CtrpDraftJB.FLD_TRIAL_STATUS, MC.M_FieldRequired);
		}
		else if (s1.equals(ctrpDraftJB.getStatusTypeAdminComplete())) {
			if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_STUDY_STOP_REASON))) {
				addFieldError(CtrpDraftJB.FLD_STUDY_STOP_REASON, MC.M_FieldRequired);
			}
		} else if (s1.equals(ctrpDraftJB.getStatusTypeTempClosedAcc())) {
			if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_STUDY_STOP_REASON))) {
				addFieldError(CtrpDraftJB.FLD_STUDY_STOP_REASON, MC.M_FieldRequired);
			}
		} else if(s1.equals(ctrpDraftJB.getStatusTypeWithdrawn())) {
			if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_STUDY_STOP_REASON))) {
				addFieldError(CtrpDraftJB.FLD_STUDY_STOP_REASON, MC.M_FieldRequired);
			}
		}
		
		s1 = StringUtil.trueValue(request.getParameter(CtrpDraftJB.FLD_TRIAL_STATUS_DATE));
		if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_TRIAL_STATUS_DATE))) {
			addFieldError(CtrpDraftJB.FLD_TRIAL_STATUS_DATE, MC.M_FieldRequired);
		}else if (!DateUtil.isValidDateFormat(s1)) {
			addFieldError(CtrpDraftJB.FLD_TRIAL_STATUS_DATE, 
					VelosResourceBundle.getMessageString("M_IvdDt_PlsVldFmt", (Object[])dateFormatParams));
		}
	
		s1 = StringUtil.trueValue(request.getParameter(CtrpDraftJB.FLD_TRIAL_START_DATE));
		if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_TRIAL_START_DATE))) {
			addFieldError(CtrpDraftJB.FLD_TRIAL_START_DATE, MC.M_FieldRequired);
		}else if (!DateUtil.isValidDateFormat(s1)) {
			addFieldError(CtrpDraftJB.FLD_TRIAL_START_DATE, 
					VelosResourceBundle.getMessageString("M_IvdDt_PlsVldFmt", (Object[])dateFormatParams));
		}
		
		s1 = StringUtil.trueValue(request.getParameter(CtrpDraftJB.FLD_PRIMARY_COMPLETION_DATE));
		if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_PRIMARY_COMPLETION_DATE))) {
			addFieldError(CtrpDraftJB.FLD_PRIMARY_COMPLETION_DATE, MC.M_FieldRequired);
		}else if (!DateUtil.isValidDateFormat(s1)) {
			addFieldError(CtrpDraftJB.FLD_PRIMARY_COMPLETION_DATE, 
					VelosResourceBundle.getMessageString("M_IvdDt_PlsVldFmt", (Object[])dateFormatParams));
		}
	}
	
	private void validateSectIndIdeForUpdateCtrpDraft() {
		try {
			int studyId = StringUtil.stringToNum(ctrpDraftJB.getFkStudy());
			
			StudyINDIDEDAO indIdeDao = new StudyINDIDEDAO();
			indIdeDao.getStudyIndIdeInfo(studyId);

			ArrayList<Integer> idIndIdeList = indIdeDao.getIdIndIdeList();
			if (idIndIdeList == null || idIndIdeList.size() <= 0) {
				addFieldError(CtrpDraftJB.FLD_INDIDE, MC.CTRP_IndIdeInfoReqd);
			}
		} catch (Exception e) {
			Rlog.fatal("ctrp",
					"Exception in CtrpDraftAction.validateSectIndIdeForUpdateCtrpDraft: " + e);
		}
	}
	
	private void validateAddressForCtrpDraftNonIndustrial(String idField, String addressIdentifier) {
		int addressComplete = 1;
		boolean fieldFound = false;
		
		if (addressIdentifier != null){
			synchronized(CtrpDraftJB.class) {
				Field[] fields = CtrpDraftJB.class.getFields();
				for (Field field : fields) {
					String fieldName = field.getName();
					boolean checkMe = false;
					if (fieldName.equals("FLD_"+addressIdentifier+"_STREET")
						|| fieldName.equals("FLD_"+addressIdentifier+"_CITY")
						|| fieldName.equals("FLD_"+addressIdentifier+"_STATE")
						|| fieldName.equals("FLD_"+addressIdentifier+"_ZIP")
						|| fieldName.equals("FLD_"+addressIdentifier+"_COUNTRY")
						|| fieldName.equals("FLD_"+addressIdentifier+"_EMAIL"))
					{
						checkMe = true;
					}else{
						if (fieldName.equals("FLD_"+addressIdentifier+"_PHONE") && 
								(addressIdentifier.equals("PI") || (addressIdentifier.equals("PER_CONTACT"))))
							checkMe = true;
					}
					
					if (checkMe){
						try {
							fieldFound = true;
							String fieldValue = (String) field.get(null);
							
							if (StringUtil.isEmpty(request.getParameter(fieldValue))) {
								if (addressComplete == 1){
									addressComplete = 0;
									addFieldError(idField, MC.CTRP_CompleteAddressReqd);
								}
								addFieldError(fieldValue, MC.M_FieldRequired);							
							}
						} catch(Exception e) {
							System.out.println(e.getMessage());
						}
					}
				}
			}
			if (!fieldFound){
				synchronized(CtrpDraftSectSponsRespJB.class) {
					Field[] fields = CtrpDraftSectSponsRespJB.class.getFields();
					for (Field field : fields) {
						String fieldName = field.getName();
						boolean checkMe = false;
						if (fieldName.equals("FLD_"+addressIdentifier+"_STREET")
							|| fieldName.equals("FLD_"+addressIdentifier+"_CITY")
							|| fieldName.equals("FLD_"+addressIdentifier+"_STATE")
							|| fieldName.equals("FLD_"+addressIdentifier+"_ZIP")
							|| fieldName.equals("FLD_"+addressIdentifier+"_COUNTRY")
							|| fieldName.equals("FLD_"+addressIdentifier+"_EMAIL"))
						{
							checkMe = true;
						}else{
							if (fieldName.equals("FLD_"+addressIdentifier+"_PHONE") && 
									(addressIdentifier.equals("PI") || (addressIdentifier.equals("PER_CONTACT"))))
								checkMe = true;
						}
						
						if (checkMe){
							try {
								fieldFound = true;
								String fieldValue = (String) field.get(null);
								
								if (StringUtil.isEmpty(request.getParameter(fieldValue))) {
									if (addressComplete == 1){
										addressComplete = 0;
										addFieldError(idField, MC.CTRP_CompleteAddressReqd);
									}
									addFieldError(fieldValue, MC.M_FieldRequired);							
								}
							} catch(Exception e) {
								System.out.println(e.getMessage());
							}
						}
					}
				}
			}
		}
	}
	
	/*Added for download CVS Section : Yogendra Pratap Singh*/
	public String ctrpDraftCSVExport(){
		ArrayList<CtrpDraftCSVExportJB> exportList = null ;
		String zipFileName=null;
		String fileName = null;
		List<String> draftNameList = null;
		int docSuccessFlag = 0; 
		try{
		if (ctrpDraftJB == null) { new CtrpDraftJB(); }
		String draftIds = request.getParameter(CTRP_DRAFT_IDS)== null ? "" : request.getParameter(CTRP_DRAFT_IDS);
		//Modified for Bug#8464 | Date: 20 FEB 2012 |By :YPS
		draftIds = ctrpDraftJB.findSelectedDtaftIds(draftIds);
		int nonIndustryFlag = request.getParameter(CTRP_NONINDUSTRY_FLAG)== null ? 0 : Integer.parseInt(request.getParameter(CTRP_NONINDUSTRY_FLAG));
		exportList = ctrpDraftJB.ctrpDraftCSVExport(draftIds,nonIndustryFlag);
		boolean result = ctrpDraftJB.generateCSVDocument(exportList, nonIndustryFlag);
		if(result)
			docSuccessFlag =1;
		//For Enhancement 22495 CTRP Document
		if(docSuccessFlag==1){
			
			// adding down-loaded status
			
			HashMap<String, String> argMap = new HashMap<String, String>();
			String userId = (String)request.getSession(false).getAttribute(USER_ID);
			userId = StringUtil.trueValue(userId);
			argMap.put(USER_ID, userId);
			
			String	ipAdd = (String)request.getSession(false).getAttribute(IP_ADD);
			ipAdd = StringUtil.trueValue(ipAdd);
			argMap.put(IP_ADD, ipAdd);
			
			CodeDao cdDraftStatus = new CodeDao();
			int downloadedStatPK =  cdDraftStatus.getCodeId("ctrpDraftStatus", "DWLD");
			argMap.put(CtrpDraftJB.STATUS_CODE_ID, ""+downloadedStatPK);
			// for system generated
			argMap.put("NOTES", CtrpDraftJB.SYS_GEN_STAT);
			
			CtrpDraftJB ctrpDraftJB = new CtrpDraftJB();
			
			String [] draftIdArr = draftIds.split(",");
			for(String s:draftIdArr){
				ctrpDraftJB.setId(StringUtil.stringToInteger(s));
				ctrpDraftJB.updateStatusHistoryDetails(argMap,CtrpDraftJB.CTRP_DRAFT_STATUS);
			}
			//
			
			draftNameList = ctrpDraftJB.processForZipFile(exportList);
			if(nonIndustryFlag==1)
				fileName =CtrpDraftJBHelper.COMPLETE_BATCH_ULOAD;	
			else
				fileName = CtrpDraftJBHelper.ABBREVIATED_BATCH_ULOAD;
			draftNameList.add(fileName);
			
			if(null!=draftNameList && draftNameList.size()>0){
				zipFileName = ctrpDraftJB.prepareZipFileFromTempDoc(draftNameList);
				request.setAttribute("zipFileName", zipFileName);
			}else{
				request.setAttribute("zipFileName", null);
			}
		}
		request.setAttribute(CTRP_CSV_RESULT, docSuccessFlag);
		
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String openCSVDoc(){
		//If csvFileType ==1 then NonIndustrial  Else Industrial  
		String zipFileName = request.getParameter("zipFileName")== null ? "" : request.getParameter("zipFileName");
		int result = request.getParameter(CTRP_CSV_RESULT)== null ? 0 : Integer.parseInt(request.getParameter(CTRP_CSV_RESULT));
		ctrpDraftJB.openCSVDoc(response, zipFileName);
		request.setAttribute("zipFileName", zipFileName);
		request.setAttribute(CTRP_CSV_RESULT, result);
		return null;
	}

	private void populateJB()
	{
		String userId ="" ;
		char protocolManagementRight = '0';
		HttpSession tSession = request.getSession(false);
		SessionMaint sessionmaint = new SessionMaint();
		if (sessionmaint.isValidSession(tSession)) {

			GrpRightsJB grpRights = (GrpRightsJB) request.getSession(false).getAttribute("GRights");
			if (grpRights == null) { return; }
			String modRight = (String) tSession.getValue("modRight");
			
			
			userId = (String) tSession.getAttribute("userId");
			ctrpDraftJB.setUserIdS(userId);
	    	String accountId = (String) tSession.getValue("accountId");
	    	ctrpDraftJB.setAccountId(accountId);
	    	
	    	int usrId = EJBUtil.stringToNum(userId);
	    	UserJB userB = new UserJB();
	    	userB.setUserId(usrId);
	    	userB.getUserDetails();
	    	
	    	
	    	String defGroup = userB.getUserGrpDefault();
	    	int grpId=EJBUtil.stringToNum(defGroup);
	    	ctrpDraftJB.setGrpId(String.valueOf(grpId));
	    	GroupJB groupB = new GroupJB();
	    	groupB.setGroupId(EJBUtil.stringToNum(defGroup));
	    	groupB.getGroupDetails();
	    	String groupName = groupB.getGroupName();
	    	ctrpDraftJB.setGroupName(groupName);
	    	CtrlDao acmod = new CtrlDao();
	    	 acmod.getControlValues("study_rights","STUDYSUM");  
	         ArrayList aRightSeq = acmod.getCSeq();  
	         String rightSeq = aRightSeq.get(0).toString();  
	         ctrpDraftJB.setRightSeq(rightSeq);
	         int iRightSeq = EJBUtil.stringToNum(rightSeq);
	        
	    	CodeDao  cDao= new CodeDao();
	    	int pmtclsId = cDao.getCodeId("studystat", "prmnt_cls");
	    	ctrpDraftJB.setPmtclsId(String.valueOf(pmtclsId));
	    	String superuserRights = "";

	    	superuserRights = GroupDao.getDefaultStudySuperUserRights(userId);
	    	ctrpDraftJB.setSuperuserRights(superuserRights);
	    	

		}
	}

	public void setErrorMap(Map errorMap) {
		this.errorMap = errorMap;
	}

	public Map getErrorMap() {
		return errorMap;
	}

	public void setErrorAction(Collection errorAction) {
		this.errorAction = errorAction;
	}

	public Collection getErrorAction() {
		return errorAction;
	}
	
	public String markCtrpDraftReadyForNonIndustrial() {
		try {
			//Saving the data to make sure un-saved but validated data is saved 
			if (SUCCESS.equals(this.updateCtrpDraftNonIndustrial())){
				HashMap<String, String> argMap = new HashMap<String, String>();
				String userId = null;
				try {
					userId = (String)request.getSession(false).getAttribute(USER_ID);
				} finally {
					userId = StringUtil.trueValue(userId);
				}
				argMap.put(USER_ID, userId);
				String ipAdd = null;
				try {
					ipAdd = (String)request.getSession(false).getAttribute(IP_ADD);
				} finally {
					ipAdd = StringUtil.trueValue(ipAdd);
				}
				argMap.put(IP_ADD, ipAdd);
				CodeDao cdDraftStatus = new CodeDao();
				int readySubmissionPK =  cdDraftStatus.getCodeId("ctrpDraftStatus", "readySubmission");
				
				argMap.put(CtrpDraftJB.STATUS_CODE_ID, ""+readySubmissionPK);
				int draftId = ctrpDraftJB.getId();
				ctrpDraftJB.retrieveCtrpDraft(draftId);
				Integer output = ctrpDraftJB.markCtrpDraftReadyForSubmission(argMap);
				if (output < 0) {
					request.setAttribute(MESSAGE, MC.M_DataNtSvd_Succ);
					return ERROR;
				}
				
				request.setAttribute(MESSAGE, MC.M_Data_SavedSucc);
				return SUCCESS;
			} else {
				request.setAttribute(MESSAGE, MC.M_DataNtSvd_Succ);
				return ERROR;
			}
		} catch (Exception e){
			request.setAttribute(MESSAGE, MC.M_DataNtSvd_Succ);
			return ERROR;
		}
	}
	
	public Map getUserDataMap() {
		return userDataMap;
	}

	public void setUserDataMap(Map userDataMap) {
		this.userDataMap = userDataMap;
	}
	
	//22472
	public String deleteCtrpStatus() {
		
		String statIds = StringUtil.trueValue(request.getParameter("statusCountVal"));
		String draftId = StringUtil.trueValue(request.getParameter("draftPK"));
		Integer output = ctrpDraftJB.deleteCtrpDrftStats(statIds, draftId);
		if(output<0)
		{
			Map<String, String> errorMap = new HashMap<String, String>();
			errorMap.put(DATA_NOT_DELETED, MC.M_DataNotDel_Succ);
			this.errorMap=errorMap;
			return INPUT;
		}
		return SUCCESS;
	}
	
	public String fetchCtrpDraftStatuses() {
		ctrpDraftJB.setId(StringUtil.stringToInteger(request.getParameter(DRAFT_ID)));
		this.userDataMap = ctrpDraftJB.fetchCtrpDraftStatuses();
		return SUCCESS;
	}
	public String copyCtrpDraft(){
		String userId = (String)request.getSession(false).getAttribute(USER_ID);
		ctrpDraftJB.setLoggedUser(Integer.parseInt(userId));
		ctrpDraftJB.copyCtrpDraft(StringUtil.stringToInteger(request.getParameter(DRAFT_ID)));
		return CTRP_DRAFT_NON_INDUSTRIAL_TILE;
	}
	
	public String fetchStudyAutoComplete(){
			String userId = (String)request.getSession(false).getAttribute(USER_ID);
			String accId = (String)request.getSession(false).getAttribute(ACCOUNT_ID);
			this.userDataMap = ctrpDraftJB.fetchStudyAutoComplete(accId,userId);
			return SUCCESS;
	}
}	
