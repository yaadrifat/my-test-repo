package com.velos.eres.ctrp.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.SortedMap;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletResponse;

import com.velos.eres.business.address.impl.AddressBean;
import com.velos.eres.business.appendix.impl.AppendixBean;
import com.velos.eres.business.codelst.impl.CodelstBean;
import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.StudyINDIDEDAO;
import com.velos.eres.business.common.StudyNIHGrantDao;
import com.velos.eres.business.common.StudyStatusDao;
import com.velos.eres.business.site.impl.SiteBean;
import com.velos.eres.business.study.impl.StudyBean;
import com.velos.eres.business.studyNIHGrant.impl.StudyNIHGrantBean;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.ctrp.business.CTRPDraftDocDao;
import com.velos.eres.ctrp.business.CtrpDraftBean;
import com.velos.eres.ctrp.service.CtrpDraftAgent;
import com.velos.eres.service.addressAgent.AddressAgentRObj;
import com.velos.eres.service.codelstAgent.CodelstAgentRObj;
import com.velos.eres.service.siteAgent.SiteAgentRObj;
import com.velos.eres.service.studyAgent.StudyAgent;
import com.velos.eres.service.studyINDIDEAgent.StudyINDIDEAgentRObj;
import com.velos.eres.service.studyNIHGrantAgent.StudyNIHGrantAgentRObj;
import com.velos.eres.service.studyStatusAgent.StudyStatusAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.JNDINames;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.appendix.AppendixJB;
import com.velos.eres.web.statusHistory.StatusHistoryJB;
import com.velos.eres.web.studyNIHGrant.StudyNIHGrantJB;
import com.velos.eres.web.user.UserJB;

/**
 * A helper class for CtrpDraftJB. The purpose of this class is to reduce the code size of CtrpDraftJB
 * by delegating some methods. This class object requires a reference to a CtrpDraftJB object. When
 * executing a method, this class fills the CtrpDraftJB object with the data retrieved from DB.
 * 
 * @author Isaac Huang
 */
public class CtrpDraftJBHelper {
	private CtrpDraftJB ctrpDraftJB = null;
	
	private static final String EMPTY_STRING = "";
	private static final String STUDY_STAT_TYPE = "studystat";
	private static final String STUDY_STAT_ACTIVE_SUBTYPE = "active";
	private static final String STUDY_STAT_SUBTYPE = "active_cls";
	private static final String STUDY_PERMNT_CLS = "prmnt_cls";//used as study 'Permanent Closure' status
	
	public static final String  COMPLETE_BATCH_ULOAD    =  "CompleteBatchUpload.csv";
	public static final String  ABBREVIATED_BATCH_ULOAD =  "AbbreviatedBatchUpload.csv";
	public static final String LINE_SEPRATOR	= "\r\n";
	private static final String CONTENT_DISPOSITION = "Content-Disposition";
	
	private static String REGEX_COMMA = "[,]+";
	private static final String REPLACE_STRING = "~";
	private static String REPLACE_DUPLICATES="(^\\[|\\]$)";
	private static final String SCH = "sch";
		

	
	@SuppressWarnings("unused")
	private CtrpDraftJBHelper() {}

	public CtrpDraftJBHelper(CtrpDraftJB ctrpDraftJB) {
		this.ctrpDraftJB = ctrpDraftJB;
	}
	
	private StudyAgent getStudyAgent() throws NamingException {
		InitialContext ic = new InitialContext();
		return (StudyAgent)ic.lookup(JNDINames.StudyAgentHome);
	}
	
	private StudyStatusAgentRObj getStudyStatusAgent() throws NamingException {
		InitialContext ica = new InitialContext();
		return (StudyStatusAgentRObj)ica.lookup(JNDINames.StudyStatusAgentHome);
	}
	
	private CtrpDraftAgent getCtrpDraftAgent() throws NamingException {
		InitialContext ic = new InitialContext();
		return (CtrpDraftAgent)ic.lookup(JNDINames.CtrpDraftAgentHome);
	}
	
	private SiteAgentRObj getSiteAgent() throws NamingException {
		InitialContext ic = new InitialContext();
		return (SiteAgentRObj)ic.lookup(JNDINames.SiteAgentHome);
	}

	private AddressAgentRObj getAddressAgent() throws NamingException {
		InitialContext ic = new InitialContext();
		return (AddressAgentRObj)ic.lookup(JNDINames.AddressAgentHome);
	}

	private UserAgentRObj getUserAgent()  throws NamingException {
		InitialContext ic = new InitialContext();
		return (UserAgentRObj)ic.lookup(JNDINames.UserAgentHome);
	}

	private String getCodelstDescriptionByPk(int codePk) {
		CodeDao codeDao = new CodeDao();
		codeDao.getCodeValuesById(codePk);
		ArrayList myArrayList = codeDao.getCDesc();
		if (myArrayList == null || myArrayList.size() < 1) { return EMPTY_STRING; }
		return (String)myArrayList.get(0);
	}
	
	void retrieveStudy(int studyId) {
		StudyAgent studyAgent = null;
		StudyBean studyBean = null;
		UserBean uBean =null;
		
		try {
			studyAgent = this.getStudyAgent();
			studyBean = studyAgent.getStudyDetails(studyId);
		} catch (Exception e) {
			Rlog.fatal("ctrp", "Exception in CtrpDraftJBHelper.retrieveStudy for studyId="+studyId+" :"+e);
			return;
		}
		
		if (ctrpDraftJB.getCtrpDraftSectSponsorRespJB() != null) {
			try {
				if (StringUtil.isEmpty(ctrpDraftJB.getCtrpDraftSectSponsorRespJB().getSponsorId())) {
					ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setSponsorId(studyBean.getStudySponsorIdInfo());
				}
				ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setSponsorContactFromStudy(studyBean.getStudyContact());
			} catch(Exception e) {
				e.printStackTrace();
				Rlog.fatal("ctrp", "Exception in CtrpDraftJBHelper.retrieveStudy sponsorId for studyId="+studyId+" :"+e);
			}
		}
		
		try {
			// Added for CTRP-20579, Ankit
			
			/**Bug# 8618 : 24 April 2012 :Yogendra Pratap */
			UserJB userJB = new UserJB();
			userJB.setUserId(ctrpDraftJB.getLoggedUser());
			uBean = userJB.getUserDetails();
			int userSiteId = (uBean.getUserSiteId()==null) ? 0 : Integer.parseInt(uBean.getUserSiteId());
			
			StudyStatusAgentRObj studyStatusAgent = this.getStudyStatusAgent();
			StudyStatusDao currStudyStatDao = studyStatusAgent.getStudyStatusDesc(studyId, userSiteId, 0, 0);
			
			ctrpDraftJB.setFkStudy(String.valueOf(studyBean.getId()));
			ctrpDraftJB.setStudyNumber(studyBean.getStudyNumber());
			
			//Added for CTRP-22471 : Raviesh
			ctrpDraftJB.setNciTrialId(studyBean.getNciTrialIdentifier());
			ctrpDraftJB.setNctNumber((studyBean.getNctNumber()));
			
			ctrpDraftJB.setSubOrgTrialId(studyBean.getStudyNumber());
			ctrpDraftJB.setStudyTitle(studyBean.getStudyTitle());
			ctrpDraftJB.setStudyPhase(getCodelstDescriptionByPk(
					StringUtil.stringToNum(studyBean.getStudyPhase())));
			ctrpDraftJB.setStudyType(getCodelstDescriptionByPk(
					StringUtil.stringToNum(studyBean.getStudyType())));
			String diseaseSites=studyBean.getDisSite();
			StringBuffer diseaseNames= new StringBuffer();
			if(diseaseSites!=null)
			{
						StringTokenizer diseaseSitesToken = new StringTokenizer(diseaseSites,",");
						while(diseaseSitesToken.hasMoreTokens()){
						diseaseNames.append((getCodelstDescriptionByPk(StringUtil.stringToNum(diseaseSitesToken.nextToken())))).append(", ");
					}
				
			}
			ctrpDraftJB.setStudyDiseaseSite(diseaseSites);
			String diseaseName=diseaseNames.toString().trim();
			if(diseaseName.length()>1)
			{
				diseaseName=diseaseName.substring(0,diseaseName.length()-1);
			}
			ctrpDraftJB.setStudyDiseaseName(diseaseName);
			if (null == ctrpDraftJB.getSumm4TypeInt()){
				ctrpDraftJB.setSumm4TypeInt(
						StringUtil.stringToNum(studyBean.getStudyResType()));
				String summ4Desc = studyBean.getStudyResType()==null?"":getCodelstDescriptionByPk(StringUtil.stringToNum(studyBean.getStudyResType())); 
				ctrpDraftJB.setSumm4Type(summ4Desc);
			}
			if(null == ctrpDraftJB.getSumm4CtrpId()){				
				ctrpDraftJB.setSumm4CtrpId(studyBean.getStudySponsorIdInfo());
			}
			// Added for CTRP-20579, Ankit (Start)
			
			/**Bug# 8618 : 24 April 2012 :Yogendra Pratap */
			//ctrpDraftJB.setStudyActiveDate(studyBean.getStudyActBeginDate());
			//ctrpDraftJB.setStudyEndDate(studyBean.getStudyEndDate());
			
		   ArrayList currentStats =  currStudyStatDao.getCurrentStats();
		   ArrayList currentStatsIds =  currStudyStatDao.getStudyStatus();
		   ArrayList currStatusLst = currStudyStatDao.getDescStudyStats();
		   ArrayList currentStatusDtLst = currStudyStatDao.getStatusCreatedOn();
		   ArrayList statusValidFrmDtLst = currStudyStatDao.getStatStartDates();
		   ArrayList statusFkSite = currStudyStatDao.getSiteIds();
		   ArrayList statusPKId = currStudyStatDao.getIds();
		   
		   SortedMap enrollClsStatDate = new TreeMap();
		   SortedMap activeStatDate = new TreeMap();
		   
		   CodeDao codeDao = new CodeDao();
		   int closedEnrollment= codeDao.getCodeId(STUDY_STAT_TYPE, STUDY_STAT_SUBTYPE);
		   int activeStatusValid= codeDao.getCodeId(STUDY_STAT_TYPE, STUDY_STAT_ACTIVE_SUBTYPE);
		   int permanentClosureStatus = codeDao.getCodeId(STUDY_STAT_TYPE, STUDY_PERMNT_CLS);
		   
		   int len = currentStats.size();
		   String currentVal="";
		   String currentStatus ="";
		   String enrollClosedStatusId ="";
		   String enrollClosedStatusDt ="-";
		   String permanentClosureStatusDt="-";
		   String activeStatusId ="";
		   String activeStatusDt ="-";
		   String statusValidFrmDt = "";
		   int loggedUser =ctrpDraftJB.getLoggedUser();
		   UserAgentRObj userAgent = getUserAgent();
		   UserBean userBean = userAgent.getUserDetails(loggedUser);
		   String loggedUserSite=userBean.getUserSiteId();
		   ArrayList<Integer> elementList = new ArrayList<Integer>();
		   ArrayList<String> studyAciveDate  = new ArrayList<String>();
		   ArrayList<Integer> elmList = new ArrayList<Integer>();
		   ArrayList<String> studyAccClosedDate  = new ArrayList<String>();
		   for (int i=0;i<len;i++) {
		        currentVal = (currentStats.get(i) == null)?"":(currentStats.get(i)).toString();
		        if(loggedUserSite.equalsIgnoreCase(statusFkSite.get(i).toString())){
		        	enrollClosedStatusId = (currentStatsIds.get(i) == null)?"0":(currentStatsIds.get(i)).toString();
			        if(closedEnrollment==(Integer.parseInt(enrollClosedStatusId)))
			        {
			        	enrollClosedStatusDt=((statusValidFrmDtLst.get(i)) == null)?"-":(statusValidFrmDtLst.get(i)).toString();
						 if(enrollClosedStatusDt.length() > 1){
							 enrollClosedStatusDt = DateUtil.dateToString(java.sql.Date.valueOf(enrollClosedStatusDt.substring(0,10)));
							 enrollClsStatDate.put(statusPKId.get(i), enrollClosedStatusDt);
					 	}
			        }
			        if(permanentClosureStatus==(Integer.parseInt(enrollClosedStatusId))){
			        	permanentClosureStatusDt=((statusValidFrmDtLst.get(i)) == null)?"-":(statusValidFrmDtLst.get(i)).toString();
						 if(permanentClosureStatusDt.length() > 1){
							 permanentClosureStatusDt = DateUtil.dateToString(java.sql.Date.valueOf(permanentClosureStatusDt.substring(0,10)));
					 	}
			        	if(elmList.isEmpty()){
							elmList.add(Integer.parseInt(statusPKId.get(i).toString()));
							studyAccClosedDate.add(permanentClosureStatusDt);
				        }else if((elmList.get(0)) < Integer.parseInt(statusPKId.get(i).toString())){
				        	elmList.clear();
				        	studyAccClosedDate.clear();
			        		elmList.add(Integer.parseInt(statusPKId.get(i).toString()));
			        		studyAccClosedDate.add(permanentClosureStatusDt);
				        }
			        }
			        activeStatusId = (currentStatsIds.get(i) == null)?"0":(currentStatsIds.get(i)).toString();
			        if(activeStatusValid==(Integer.parseInt(activeStatusId)))
			        {
			        	activeStatusDt=((statusValidFrmDtLst.get(i)) == null)?"-":(statusValidFrmDtLst.get(i)).toString();
			        	if(activeStatusDt.length() > 1){
			        		activeStatusDt = DateUtil.dateToString(java.sql.Date.valueOf(activeStatusDt.substring(0,10)));
			        		activeStatDate.put(statusPKId.get(i), activeStatusDt);
			        	}
			        	/**Bug# 8618 : 24 April 2012 :Yogendra Pratap */
			        	if(elementList.isEmpty()){
			        		elementList.add(Integer.parseInt(statusPKId.get(i).toString()));
			        		studyAciveDate.add(statusValidFrmDtLst.get(i).toString());
			        	}else if((elementList.get(0)) < Integer.parseInt(statusPKId.get(i).toString())){
			        		elementList.clear();
			        		studyAciveDate.clear();
			        		elementList.add(Integer.parseInt(statusPKId.get(i).toString()));
			        		studyAciveDate.add(statusValidFrmDtLst.get(i).toString());
			        	}
			        }
		        }
		        if ( currentVal.equals("1")) {
					 currentStatus=((currStatusLst.get(i)) == null)?"-":(currStatusLst.get(i)).toString();
					 statusValidFrmDt=((statusValidFrmDtLst.get(i)) == null)?"-":(statusValidFrmDtLst.get(i)).toString();
					 if(statusValidFrmDt.length() > 1){
						 statusValidFrmDt = DateUtil.dateToString(java.sql.Date.valueOf(statusValidFrmDt.substring(0,10)));
				 	}
		        }
    	   }
		   if(enrollClsStatDate.size()>0){
		   enrollClosedStatusDt = enrollClsStatDate.get(enrollClsStatDate.lastKey()).toString() ;
		   }
		   if(activeStatDate.size()>0){
		   activeStatusDt = activeStatDate.get(activeStatDate.lastKey()).toString();
		   }
		   ctrpDraftJB.setCurrStudyStatus(currentStatus);
		   ctrpDraftJB.setStatusValidFrm(statusValidFrmDt);
		   ctrpDraftJB.setActiveStatusValidFrom(activeStatusDt);
		   /**Bug# 8618 : 24 April 2012 :Yogendra Pratap */
		   if(studyAciveDate!=null && studyAciveDate.size()>0){
		   ctrpDraftJB.setStudyActiveDate(DateUtil.dateToString(java.sql.Date.valueOf(studyAciveDate.get(0).substring(0,10))));
		   }
		   if(studyAccClosedDate!=null && studyAccClosedDate.size()>0){
			   ctrpDraftJB.setStudyEndDate(studyAccClosedDate.get(0));
		   }

		   if(StringUtil.isEmpty(ctrpDraftJB.getRecStatusDate()) && !statusValidFrmDt.equals("-")){ctrpDraftJB.setRecStatusDate(statusValidFrmDt);}
		   if(StringUtil.isEmpty(ctrpDraftJB.getOpenAccrualDate())&& !activeStatusDt.equals("-")){ctrpDraftJB.setOpenAccrualDate(activeStatusDt);}
		   if(StringUtil.isEmpty(ctrpDraftJB.getClosedAccrualDate())&& !enrollClosedStatusDt.equals("-")){ctrpDraftJB.setClosedAccrualDate(enrollClosedStatusDt);}
		   ctrpDraftJB.setStudyEnrollClosedDt(enrollClosedStatusDt);
		   //Added for CTRP-20579, Ankit  (End)
		} catch (Exception e) {
			Rlog.fatal("ctrp", "Exception in CtrpDraftJBHelper.retrieveStudy for studyId="+studyId+" :"+e);
			e.printStackTrace();
		}
	}
	
	void retrieveCtrpDraft(int draftId) {
		try {
			CtrpDraftAgent ctrpDraftAgent = this.getCtrpDraftAgent();
			CtrpDraftBean ctrpDraftBean = ctrpDraftAgent.getCtrpDraftBean(draftId);
			
			ctrpDraftJB.setId(ctrpDraftBean.getId());
			int showMe = ctrpDraftJB.checkCtrpDraftStatus();
			if (showMe == 0) 
				ctrpDraftJB.setHideeSign(1);
			else
				ctrpDraftJB.setHideeSign(0);
			ctrpDraftJB.setDraftNum(ctrpDraftBean.getDraftNum());
			ctrpDraftJB.setDraftType(ctrpDraftBean.getCtrpDraftType());
			ctrpDraftJB.setFkStudy(String.valueOf(ctrpDraftBean.getFkStudy()));
			ctrpDraftJB.setTrialSubmTypeInt(ctrpDraftBean.getFkCodelstSubmissionType());
			ctrpDraftJB.setDraftXmlRequired(ctrpDraftBean.getDraftXMLReqd());
			ctrpDraftJB.setAmendNumber(ctrpDraftBean.getAmendNumber());
			ctrpDraftJB.setAmendDate(DateUtil.dateToString(ctrpDraftBean.getAmendDate()));
			ctrpDraftJB.setNciTrialId(ctrpDraftBean.getNciStudyId());
			ctrpDraftJB.setLeadOrgTrialId(ctrpDraftBean.getLeadOrgStudyId());
			ctrpDraftJB.setNctNumber(ctrpDraftBean.getNctNumber());
			ctrpDraftJB.setOtherTrialId(ctrpDraftBean.getOtherNumber());
			ctrpDraftJB.setDraftPilot(ctrpDraftBean.getDraftPilot());
			try { ctrpDraftJB.setDraftStudyPhase(ctrpDraftBean.getFkCodelstPhase()); } catch(Exception e) {}
			ctrpDraftJB.setDraftPilot(ctrpDraftBean.getDraftPilot());
			try { ctrpDraftJB.setDraftStudyType(ctrpDraftBean.getStudyType()); } catch(Exception e) {}
			try { ctrpDraftJB.setStudyPurpose(ctrpDraftBean.getStudyPurpose()); } catch(Exception e) {}
			ctrpDraftJB.setStudyPurposeOther(ctrpDraftBean.getStudyPurposeOther());
			
			ctrpDraftJB.setLeadOrgCtrpId(ctrpDraftBean.getLeadOrgCtrpId() == null ? null :
				String.valueOf(ctrpDraftBean.getLeadOrgCtrpId()));
			ctrpDraftJB.setLeadOrgFkSite(ctrpDraftBean.getFkSiteLeadOrg() == null ? null : 
				String.valueOf(ctrpDraftBean.getFkSiteLeadOrg()));
			ctrpDraftJB.setPiId(ctrpDraftBean.getPiCtrpId() == null ? null :
				String.valueOf(ctrpDraftBean.getPiCtrpId()));
			ctrpDraftJB.setPiFkUser(ctrpDraftBean.getFkUserPI() == null ? null :
				String.valueOf(ctrpDraftBean.getFkUserPI()));
			
			if (ctrpDraftBean.getFkSiteLeadOrg() != null) {
				try {
					SiteAgentRObj siteAgent = getSiteAgent();
					SiteBean siteBean = siteAgent.getSiteDetails(ctrpDraftBean.getFkSiteLeadOrg());
					ctrpDraftJB.setLeadOrgName(siteBean.getSiteName());
				} catch(Exception e) {
					e.printStackTrace();
				}
				try {
					Integer addId = ctrpDraftBean.getFkAddLeadOrg();
					AddressAgentRObj addressAgent = this.getAddressAgent();
					AddressBean addressBean = addressAgent.getAddressDetails(addId);
					ctrpDraftJB.setLeadOrgFkAdd(String.valueOf(addId));
					ctrpDraftJB.setLeadOrgStreet(addressBean.getAddPri());
					ctrpDraftJB.setLeadOrgCity(addressBean.getAddCity());
					ctrpDraftJB.setLeadOrgState(addressBean.getAddState());
					ctrpDraftJB.setLeadOrgZip(addressBean.getAddZip());
					ctrpDraftJB.setLeadOrgCountry(addressBean.getAddCountry());
					ctrpDraftJB.setLeadOrgEmail(addressBean.getAddEmail());
					ctrpDraftJB.setLeadOrgPhone(addressBean.getAddPhone());
					ctrpDraftJB.setLeadOrgTty(addressBean.getAddTty());
					ctrpDraftJB.setLeadOrgFax(addressBean.getAddFax());
					ctrpDraftJB.setLeadOrgUrl(addressBean.getAddUrl());
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
			try { ctrpDraftJB.setLeadOrgTypeInt(ctrpDraftBean.getLeadOrgType()); } catch(Exception e) {}
			if (ctrpDraftBean.getFkUserPI() != null) {
				try {
					UserAgentRObj userAgent = getUserAgent();
					UserBean userBean = userAgent.getUserDetails(ctrpDraftBean.getFkUserPI());
					ctrpDraftJB.setPiFirstName(userBean.getUserFirstName());
					ctrpDraftJB.setPiMiddleName(userBean.getUserMidName());
					ctrpDraftJB.setPiLastName(userBean.getUserLastName());
				} catch(Exception e) {
					e.printStackTrace();
				}
				try {
					Integer addId = ctrpDraftBean.getFkAddPi();
					AddressAgentRObj addressAgent = this.getAddressAgent();
					AddressBean addressBean = addressAgent.getAddressDetails(addId);
					ctrpDraftJB.setPiFkAdd(String.valueOf(addId));
					ctrpDraftJB.setPiStreetAddress(addressBean.getAddPri());
					ctrpDraftJB.setPiCity(addressBean.getAddCity());
					ctrpDraftJB.setPiState(addressBean.getAddState());
					ctrpDraftJB.setPiZip(addressBean.getAddZip());
					ctrpDraftJB.setPiCountry(addressBean.getAddCountry());
					ctrpDraftJB.setPiEmail(addressBean.getAddEmail());
					ctrpDraftJB.setPiPhone(addressBean.getAddPhone());
					ctrpDraftJB.setPiTty(addressBean.getAddTty());
					ctrpDraftJB.setPiFax(addressBean.getAddFax());
					ctrpDraftJB.setPiUrl(addressBean.getAddUrl());
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
			
			if(null != ctrpDraftBean.getSumm4SponsorId()){
				ctrpDraftJB.setSumm4CtrpId(ctrpDraftBean.getSumm4SponsorId());
			} else {
				Integer studyId = ctrpDraftBean.getFkStudy();
				
				StudyAgent studyAgent = this.getStudyAgent();
				StudyBean studyBean = studyAgent.getStudyDetails(studyId);
				
				ctrpDraftBean.setSumm4SponsorId(studyBean.getStudySponsorIdInfo());
			}
			ctrpDraftJB.setSumm4TypeInt(ctrpDraftBean.getSumm4SponsorType());

			String summ4Desc="";
			if(ctrpDraftBean.getSumm4SponsorType() !=null){
				try{
					ctrpDraftJB.setSumm4TypeInt(ctrpDraftBean.getSumm4SponsorType());
					summ4Desc = getCodelstDescriptionByPk(ctrpDraftBean.getSumm4SponsorType());
				} catch(Exception e) {
					e.printStackTrace();
				}
			}else{
				Integer studyId = ctrpDraftBean.getFkStudy();
				
				StudyAgent studyAgent = this.getStudyAgent();
				StudyBean studyBean = studyAgent.getStudyDetails(studyId);
				
				ctrpDraftJB.setSumm4TypeInt(StringUtil.stringToNum(studyBean.getStudyResType()));
				summ4Desc = studyBean.getStudyResType()==null?"":getCodelstDescriptionByPk(StringUtil.stringToNum(studyBean.getStudyResType())); 
			}
			ctrpDraftJB.setSumm4Type(summ4Desc);

			if (ctrpDraftBean.getFkSiteSumm4Sponsor() != null) {
				try {
					SiteAgentRObj siteAgent = getSiteAgent();
					SiteBean siteBean = siteAgent.getSiteDetails(ctrpDraftBean.getFkSiteSumm4Sponsor());
					ctrpDraftJB.setSumm4FkSite(String.valueOf(ctrpDraftBean.getFkSiteSumm4Sponsor()));
					ctrpDraftJB.setSumm4Name(siteBean.getSiteName());
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
			
			if(ctrpDraftBean.getfkAddSumm4Sponsor()!=null){
				try{
					ctrpDraftJB.setSumm4FkAdd(String.valueOf(ctrpDraftBean.getfkAddSumm4Sponsor()));
					AddressAgentRObj summ4AdressAgent = this.getAddressAgent();
					AddressBean summ4AddressBean = summ4AdressAgent.getAddressDetails(ctrpDraftBean.getfkAddSumm4Sponsor());
					ctrpDraftJB.setSumm4Street(summ4AddressBean.getAddPri());
					ctrpDraftJB.setSumm4City(summ4AddressBean.getAddCity());
					ctrpDraftJB.setSumm4State(summ4AddressBean.getAddState());
					ctrpDraftJB.setSumm4Zip(summ4AddressBean.getAddZip());
					ctrpDraftJB.setSumm4Country(summ4AddressBean.getAddCountry());
					ctrpDraftJB.setSumm4Email(summ4AddressBean.getAddEmail());
					ctrpDraftJB.setSumm4Phone(summ4AddressBean.getAddPhone());
					ctrpDraftJB.setSumm4Tty(summ4AddressBean.getAddTty());
					ctrpDraftJB.setSumm4Fax(summ4AddressBean.getAddFax());
					ctrpDraftJB.setSumm4Url(summ4AddressBean.getAddUrl());
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
			try {
				if (ctrpDraftJB.getCtrpDraftSectSponsorRespJB() != null) {
					ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setSponsorFkSite(
							ctrpDraftBean.getFkSiteSponsor());
					ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setSponsorFkAdd(
							ctrpDraftBean.getFkAddSponsor());
					if (ctrpDraftBean.getFkSiteSponsor() != null) {
						SiteAgentRObj siteAgent = getSiteAgent();
						SiteBean siteBean = siteAgent.getSiteDetails(ctrpDraftBean.getFkSiteSponsor());
						ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setSponsorName(siteBean.getSiteName());
						AddressAgentRObj addressAgent = this.getAddressAgent();
						AddressBean addressBean = addressAgent.getAddressDetails(ctrpDraftBean.getFkAddSponsor());
						ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setSponsorFkAdd(ctrpDraftBean.getFkAddSponsor());
						ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setSponsorStreet(addressBean.getAddPri());
						ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setSponsorCity(addressBean.getAddCity());
						ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setSponsorState(addressBean.getAddState());
						ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setSponsorZip(addressBean.getAddZip());
						ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setSponsorCountry(addressBean.getAddCountry());
						ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setSponsorEmail(addressBean.getAddEmail());
						ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setSponsorPhone(addressBean.getAddPhone());
						ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setSponsorTty(addressBean.getAddTty());
						ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setSponsorFax(addressBean.getAddFax());
						ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setSponsorUrl(addressBean.getAddUrl());
					}
					ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setResponsibleParty(
							ctrpDraftBean.getResponsibleParty());
					ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setSponsorContactType(
							ctrpDraftBean.getRpSponsorContactType());
					ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setResponsiblePartyEmail(
							ctrpDraftBean.getRpEmail());
					ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setResponsiblePartyPhone(
							ctrpDraftBean.getRpPhone());
					ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setResponsiblePartyPhoneExt(
							ctrpDraftBean.getRpExtn() == null ? null : 
								String.valueOf(ctrpDraftBean.getRpExtn()));
					if (ctrpDraftJB.getCtrpDraftSectSponsorRespJB().getSponsorContactType() != null 
							&& ctrpDraftJB.getCtrpDraftSectSponsorRespJB().getSponsorContactType() == 1) {
						// sponsor contact type = personal
						ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setPersonalContactId(
								ctrpDraftBean.getRpSponsorContactId());
						if (ctrpDraftBean.getFkUserSponsorPersonal() != null) {
							UserAgentRObj userAgent = getUserAgent();
							UserBean userBean = userAgent.getUserDetails(ctrpDraftBean.getFkUserSponsorPersonal());
							ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setPersonalContactFkUser(
									ctrpDraftBean.getFkUserSponsorPersonal());
							ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setPersonalContactFirstName(
									userBean.getUserFirstName());
							ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setPersonalContactMiddleName(
									userBean.getUserMidName());
							ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setPersonalContactLastName(
									userBean.getUserLastName());

							AddressAgentRObj addressAgent = this.getAddressAgent();
							AddressBean addressBean = addressAgent.getAddressDetails(ctrpDraftBean.getFkAddSponsorPersonal());
							ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setPersonalContactFkAdd(
									ctrpDraftBean.getFkAddSponsorPersonal());
							ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setPersonalContactStreetAddress(addressBean.getAddPri());
							ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setPersonalContactCity(addressBean.getAddCity());
							ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setPersonalContactState(addressBean.getAddState());
							ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setPersonalContactZip(addressBean.getAddZip());
							ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setPersonalContactCountry(addressBean.getAddCountry());
							ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setPersonalContactEmail(addressBean.getAddEmail());
							ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setPersonalContactPhone(addressBean.getAddPhone());
							ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setPersonalContactTty(addressBean.getAddTty());
							ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setPersonalContactFax(addressBean.getAddFax());
							ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setPersonalContactUrl(addressBean.getAddUrl());
						}
					} else if (ctrpDraftJB.getCtrpDraftSectSponsorRespJB().getSponsorContactType() != null 
							&& ctrpDraftJB.getCtrpDraftSectSponsorRespJB().getSponsorContactType() == 0) {
						// sponsor contact type = generic
						ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setGenericContactId(
								ctrpDraftBean.getRpSponsorContactId());
						ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setGenericContactTitle(
								ctrpDraftBean.getSponsorGenericTitle());
					}
				}
			} catch(Exception e) {
				Rlog.fatal("ctrp", 
						"Exception in CtrpDraftJBHelper.retrieveCtrpDraft for spon/resp sec for id="+draftId+" :"+e);
				e.printStackTrace();
			}
			
			//--CTRP-20552-7 -Starts
			try{
				if(ctrpDraftBean.getCtrpDraftType().intValue()==1){
					//------------- Trial Submission ------------//
					ctrpDraftJB.setIndusTrialTypeInt(ctrpDraftBean.getFkCodelstSubmissionType());
					Integer trialOwner = ctrpDraftBean.getFkTrialOwner();
					if (null == trialOwner){
						ctrpDraftJB.setTrialUsrId(null);
					} else{
						ctrpDraftJB.setTrialUsrId(String.valueOf(trialOwner));
					}

					UserAgentRObj userAgent = getUserAgent();
					if (null != ctrpDraftBean.getFkTrialOwner()){
						UserBean userBean = userAgent.getUserDetails(ctrpDraftBean.getFkTrialOwner());
	
						ctrpDraftJB.setTrialFirstName(userBean.getUserFirstName());
						ctrpDraftJB.setTrialLastName(userBean.getUserLastName());

						AddressAgentRObj addressAgent = this.getAddressAgent();
						AddressBean addressBean = addressAgent.getAddressDetails(Integer.parseInt(userBean.getUserPerAddressId()));
						ctrpDraftJB.setTrialEmailId(addressBean.getAddEmail());
					}
					//------------- Trial Identification  ------------//
					ctrpDraftJB.setSubOrgTrialId(ctrpDraftBean.getSubmitOrgStudyId());
					//------------- Trial Details  ------------//
					ctrpDraftJB.setCtrpIntvnTypeInt(ctrpDraftBean.getFkCodelstInterventionType());
					ctrpDraftJB.setInterventionName(ctrpDraftBean.getInterventionName());
					//------------- Lead or Submitting Organizations / Principal Investigator  ------------//
					ctrpDraftJB.setIsNCIDesignated(ctrpDraftBean.getSubmitNCIDesignated());
					//---- Submitting Organization ----//
					ctrpDraftJB.setSubOrgCtrpId(ctrpDraftBean.getSubmitOrgCtrpId() == null ? null :
						String.valueOf(ctrpDraftBean.getSubmitOrgCtrpId()));
					ctrpDraftJB.setSubOrgFkSite(ctrpDraftBean.getFkSiteSubmitOrg() == null ? null : 
						String.valueOf(ctrpDraftBean.getFkSiteSubmitOrg()));
					if (ctrpDraftBean.getFkSiteSubmitOrg() != null) {
						try {
							SiteAgentRObj siteAgent = getSiteAgent();
							SiteBean siteBean = siteAgent.getSiteDetails(ctrpDraftBean.getFkSiteSubmitOrg());
							ctrpDraftJB.setSubOrgName(siteBean.getSiteName());
						} catch(Exception e) {
							e.printStackTrace();
						}
					}
					if (ctrpDraftBean.getFkAddSubmitOrg()!= null) {
						try {
							Integer addId = ctrpDraftBean.getFkAddSubmitOrg();
							AddressAgentRObj subOrgAddressAgent = this.getAddressAgent();
							AddressBean subOrgAddressBean = subOrgAddressAgent.getAddressDetails(addId);
							ctrpDraftJB.setSubOrgFkAdd(String.valueOf(addId));
							ctrpDraftJB.setSubOrgStreet(subOrgAddressBean.getAddPri());
							ctrpDraftJB.setSubOrgCity(subOrgAddressBean.getAddCity());
							ctrpDraftJB.setSubOrgState(subOrgAddressBean.getAddState());
							ctrpDraftJB.setSubOrgZip(subOrgAddressBean.getAddZip());
							ctrpDraftJB.setSubOrgCountry(subOrgAddressBean.getAddCountry());
							ctrpDraftJB.setSubOrgEmail(subOrgAddressBean.getAddEmail());
							ctrpDraftJB.setSubOrgPhone(subOrgAddressBean.getAddPhone());
							ctrpDraftJB.setSubOrgTty(subOrgAddressBean.getAddTty());
							ctrpDraftJB.setSubOrgFax(subOrgAddressBean.getAddFax());
							ctrpDraftJB.setSubOrgUrl(subOrgAddressBean.getAddUrl());
						} catch(Exception e) {
							e.printStackTrace();
						}
					}
					try { ctrpDraftJB.setSubOrgTypeInt(ctrpDraftBean.getSubmitOrgType()); } catch(Exception e) {}
					ctrpDraftJB.setSubOrgProgCode(ctrpDraftBean.getSponsorProgramCode());

					//------------- Industrial Status/Dates (site specific)  ------------//
					ctrpDraftJB.setSiteRecruitTypeInt(ctrpDraftBean.getFkCodelstCtrpStudyStatus());
					ctrpDraftJB.setRecStatusDate(DateUtil.dateToString(ctrpDraftBean.getCtrpStudyStatusDate()));
					ctrpDraftJB.setOpenAccrualDate(DateUtil.dateToString(ctrpDraftBean.getCtrpStudyAccOpenDate()));
					ctrpDraftJB.setClosedAccrualDate(DateUtil.dateToString(ctrpDraftBean.getCtrpStudyAccClosedDate()));
					ctrpDraftJB.setSiteTrgtAccrual(String.valueOf((ctrpDraftBean.getSiteTargetAccrual()==null?"":ctrpDraftBean.getSiteTargetAccrual())));
			
				}

			}catch(Exception e) {
				Rlog.fatal("ctrp", "Exception in CtrpDraftJBHelper.retrieveCtrpDraft for Industial for id="+draftId+" :"+e);
				e.printStackTrace();
			}
			
			
			
			//--CTRP-20552-7 -Ends			

			StatusHistoryJB statusHistoryJB = new StatusHistoryJB();
			int statusId = statusHistoryJB.getLatestStatusId(ctrpDraftJB.getId(), CtrpDraftJB.ER_CTRP_DRAFT);
			statusHistoryJB.setStatusId(statusId);
			statusHistoryJB.getStatusHistoryDetails();
			ctrpDraftJB.setDraftStatus(StringUtil.stringToInteger(statusHistoryJB.getStatusCodelstId()));

			//22472
			StatusHistoryJB nciStatusHistoryJB = new StatusHistoryJB();
			int nciStatusId = nciStatusHistoryJB.getLatestStatusId(ctrpDraftJB.getId(), CtrpDraftJB.ER_CTRP_DRAFT_NCI_STAT);
			nciStatusHistoryJB.setStatusId(nciStatusId);
			nciStatusHistoryJB.getStatusHistoryDetails();
			if(nciStatusHistoryJB.getStatusCodelstId()!=null){
				ctrpDraftJB.setCtrpNciProcStatId(StringUtil.stringToInteger(nciStatusHistoryJB.getStatusCodelstId()));	
			}			
			ctrpDraftJB.setCtrpNciProcStatDt(nciStatusHistoryJB.getStatusStartDate());
			
			// START Regulatory Info section: AGODARA
			try {ctrpDraftJB.setOversightCountry(ctrpDraftBean.getOversightCountry());} catch(Exception e) {}
			try {ctrpDraftJB.setOversightOrganization(ctrpDraftBean.getOversightOrganization());} catch(Exception e) {}
			try {ctrpDraftJB.setFdaIntvenIndicator(ctrpDraftBean.getFdaIntvenIndicator());} catch(Exception e) {}
			try {ctrpDraftJB.setSection801Indicator(ctrpDraftBean.getSection801Indicator());} catch(Exception e) {}
			try {ctrpDraftJB.setDelayPostIndicator(ctrpDraftBean.getDelayPostIndicator());} catch(Exception e) {}
			try {ctrpDraftJB.setDmAppointIndicator(ctrpDraftBean.getDmAppointIndicator());} catch(Exception e) {}
			//END Regulatory Info section: AGODARA
			
			// Added for CTRP-20579, Ankit (Start)
			try { ctrpDraftJB.setTrialStatusInt(ctrpDraftBean.getFkCodelstCtrpStudyStatus()); } catch(Exception e) {}
			ctrpDraftJB.setTrialStatusDate(DateUtil.dateToString(ctrpDraftBean.getCtrpStudyStatusDate()));
			ctrpDraftJB.setStudyStopReason(ctrpDraftBean.getCtrpStudyStopReason());
			ctrpDraftJB.setTrialStartDate(DateUtil.dateToString(ctrpDraftBean.getCtrpStudyStartDate()));
			ctrpDraftJB.setTrialStartType(ctrpDraftBean.getIsStartActual());
			ctrpDraftJB.setPrimaryCompletionDate(DateUtil.dateToString(ctrpDraftBean.getCtrpStudyCompDate()));
			ctrpDraftJB.setPrimaryCompletionType(ctrpDraftBean.getIsCompActual());
			//Added for CTRP-20579, Ankit  (End)
			
			try {
				if (ctrpDraftJB.getCtrpDraftSectSponsorRespJB() != null) {
					ctrpDraftJB.getCtrpDraftSectSponsorRespJB().setSponsorId(
							ctrpDraftBean.getSponsorCtrpId() == null ? null : ctrpDraftBean.getSponsorCtrpId());
				}
			} catch(Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			ctrpDraftJB.setId(0);
			Rlog.fatal("ctrp", "Exception in CtrpDraftJBHelper.retrieveCtrpDraft for id="+draftId+" :"+e);
			e.printStackTrace();
		}
	}
	
	
	public void retreiveCtrpDocumentInfo(int draftId) {
		try{
			CTRPDraftDocDao ctrpDraftDao= null;
			CTRPDraftDocJB ctrpDraftDocJB = new CTRPDraftDocJB();
			ctrpDraftDao=ctrpDraftDocJB.getCTRPDraftDocDetails(draftId);
			ArrayList< Integer> ctrpDrafts=ctrpDraftDao.getCtrpDrafts();
			if(ctrpDraftDao.getCtrpDocids().size() >0){
				CodeDao codeDao= new CodeDao();
				ArrayList<Integer>  ctrpDraftDocTypes = ctrpDraftDao.getCtrpDocTypes();
				ArrayList<Integer>  ctrpDraftStudyAppxs = ctrpDraftDao.getStudyAppendixs();
				ArrayList<Integer>  trialCtrpDraftPKs = ctrpDraftDao.getCtrpDocids();
				for(int i=0; i< ctrpDraftDao.getCtrpDocids().size();i++){	
					AppendixJB appnxJB= new AppendixJB();
					Integer ctrpStudyAppndx=0;
					Integer trialCtrpDraftPK=0;
					ctrpStudyAppndx=ctrpDraftStudyAppxs.get(i);
					trialCtrpDraftPK=trialCtrpDraftPKs.get(i);
					appnxJB.setId(ctrpStudyAppndx);
					AppendixBean appenxBean=appnxJB.getAppendixDetails();
					String studyaddDixUtl=appenxBean.getAppendixUrl_File();
					String draftDocType=codeDao.getCodeSubtype(ctrpDraftDocTypes.get(i));
					if(draftDocType.equalsIgnoreCase(ctrpDraftJB.CDLST_PROTOCOLDOC)){
						ctrpDraftJB.setProtocolDocDesc(studyaddDixUtl);
						ctrpDraftJB.setProtocolDocPK(ctrpStudyAppndx);
						ctrpDraftJB.setTrialProtocolDocPK(trialCtrpDraftPK);
					}if(draftDocType.equalsIgnoreCase(ctrpDraftJB.CDLST_IRBAPPROVAL)){
						ctrpDraftJB.setIRBApprovalDesc(studyaddDixUtl);	
						ctrpDraftJB.setIRBApprovalPK(ctrpStudyAppndx);
						ctrpDraftJB.setTrialIRBApprovalPK(trialCtrpDraftPK);
					}if(draftDocType.equalsIgnoreCase(ctrpDraftJB.CDLST_PARTSITESLIST)){
						ctrpDraftJB.setPartSitesListDesc(studyaddDixUtl);
						ctrpDraftJB.setPartSitesListPK(ctrpStudyAppndx);
						ctrpDraftJB.setTrialPartSitesListPK(trialCtrpDraftPK);
					}if(draftDocType.equalsIgnoreCase(ctrpDraftJB.CDLST_INFORMEDCONSENT)){						 
						ctrpDraftJB.setInformedConsentDesc(studyaddDixUtl);	
						ctrpDraftJB.setInformedConsentPK(ctrpStudyAppndx);
						ctrpDraftJB.setTrialInformedConsentPK(trialCtrpDraftPK);
					}if(draftDocType.equalsIgnoreCase(ctrpDraftJB.CDLST_CHANGEMEMODOC)){
						ctrpDraftJB.setChangeMemoDocDesc(studyaddDixUtl);	
						ctrpDraftJB.setChangeMemoDocPK(ctrpStudyAppndx);
						ctrpDraftJB.setTrialChangeMemoDocPK(trialCtrpDraftPK);
					}if(draftDocType.equalsIgnoreCase(ctrpDraftJB.CDLST_PROTHIGHLIGHT)){
						ctrpDraftJB.setProtHighlightDesc(studyaddDixUtl);	
						 ctrpDraftJB.setProtHighlightPK(ctrpStudyAppndx);
						 ctrpDraftJB.setTrialProtHighlightPK(trialCtrpDraftPK);
					}if(draftDocType.equalsIgnoreCase(ctrpDraftJB.CDLST_OTHER)){
						ctrpDraftJB.setOtherDesc(studyaddDixUtl);	
						ctrpDraftJB.setOtherPK(ctrpStudyAppndx);
						ctrpDraftJB.setTrialOtherPK(trialCtrpDraftPK);
					}if(draftDocType.equalsIgnoreCase(CtrpDraftJB.CDLST_ABBRTRIALTEMPL)){ /*YK:Added for CTRP-20552-7*/
						ctrpDraftJB.setAbbrTrialTempDesc(studyaddDixUtl);	
						ctrpDraftJB.setAbbrTrialTempPK(ctrpStudyAppndx);
						ctrpDraftJB.setTrialAbbrTrialTempPK(trialCtrpDraftPK);
					}
					
				}
			}
			
		}catch(Exception e){
			Rlog.fatal("ctrp", "Exception in CtrpDraftDocJBHelper.retreiveCtrpDocumentInfo for id="+draftId+" :"+e);
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		
	}
	/*Added for download CVS Section : Yogendra Pratap Singh*/
	public CtrpDraftCSVExportJB addNonIndustryCSVExportHeader() {
		CtrpDraftCSVExportJB ctrpDraftCSVExportJB = new CtrpDraftCSVExportJB();
		
		ctrpDraftCSVExportJB.getStudyJB().setStudyNumber("Unique Trial Identifier");//1
		ctrpDraftCSVExportJB.setFkCodelstSubmissionType("Submission Type");//2
		ctrpDraftCSVExportJB.setNciStudyId("NCI Trial Identifier");//Study Title 3
		ctrpDraftCSVExportJB.setDraftXMLReqd("Clinical Trials.gov XML Required?");//DRAFT_XML_REQD 4
		ctrpDraftCSVExportJB.setAmendNumber("Amendment Number");//5
		ctrpDraftCSVExportJB.setAmendDate("Amendment Date");//6
		ctrpDraftCSVExportJB.setLeadOrgStudyId("Lead Organization Trial Identifier");//7
		ctrpDraftCSVExportJB.setNctNumber("NCT");//8
		ctrpDraftCSVExportJB.setOtherNumber("Other Trial Identifier");//9
		ctrpDraftCSVExportJB.setTriailDetailsTitle("Title");//10
		ctrpDraftCSVExportJB.setStudyType("Trial Type");//11
		ctrpDraftCSVExportJB.setStudyPurpose("Primary Purpose");//12
		ctrpDraftCSVExportJB.setStudyPurposeAdditionalQualifier("[Primary Purpose] Additional Qualifier");//13 [Primary Purpose] Additional Qualifier
		//[Primary Purpose] Other Text					//14
		ctrpDraftCSVExportJB.setStudyPurposeOther("[Primary Purpose] Other Text");//14
		ctrpDraftCSVExportJB.setFkCodelstPhase("Phase");//15
		
		ctrpDraftCSVExportJB.setDraftPilot("Pilot Trial?");//16 Need Clarification
		ctrpDraftCSVExportJB.setSponsorCtrpId("[Sponsor] Organization PO-ID");//17 Need Clarification
		ctrpDraftCSVExportJB.setSponsorName("[Sponsor] Organization Name");//18
		
		//For setting the values in AddressJB  get Data on the Basis Of id SPONSOR_FK_ADD
		ctrpDraftCSVExportJB.getSponsorAddressJB().setAddPri("[Sponsor] Street Address");//19
		ctrpDraftCSVExportJB.getSponsorAddressJB().setAddCity("[Sponsor] City");//20
		ctrpDraftCSVExportJB.getSponsorAddressJB().setAddState("[Sponsor] State/Province");//21
		ctrpDraftCSVExportJB.getSponsorAddressJB().setAddZip("[Sponsor] Zip/Postal code");//22
		ctrpDraftCSVExportJB.getSponsorAddressJB().setAddCountry("[Sponsor] Country");//23
		ctrpDraftCSVExportJB.getSponsorAddressJB().setAddEmail("[Sponsor] Email Address");//24
		ctrpDraftCSVExportJB.getSponsorAddressJB().setAddPhone("[Sponsor] Phone");//25
		ctrpDraftCSVExportJB.getSponsorAddressJB().setAddTty("[Sponsor] TTY");//26
		ctrpDraftCSVExportJB.getSponsorAddressJB().setAddFax("[Sponsor] FAX");//27
		ctrpDraftCSVExportJB.getSponsorAddressJB().setAddUrl("[Sponsor] URL");//28
		
		ctrpDraftCSVExportJB.setResponsibleParty("Responsible Party");//29
		//Modified for Bug# 8478 For Responsible Party Contact 
		ctrpDraftCSVExportJB.setResponsiblePartyEmailAddress("Responsible Party Email Address");//Responsible Party Email Address 30
		ctrpDraftCSVExportJB.setResponsiblePartyPhone("Responsible Party Phone ");//Responsible Party Phone  31
		ctrpDraftCSVExportJB.setResponsiblePartyExt("Responsible Party Ext");//Responsible Party Ext 32
		
		ctrpDraftCSVExportJB.setRpSponsorContactType("Sponsor Contact Type");//30
		ctrpDraftCSVExportJB.setSponsorGenericTitle("[Sponsor Contact] Title");//31
		ctrpDraftCSVExportJB.setRpSponsorContactId("[Sponsor Contact] Person PO-ID ");//32
		ctrpDraftCSVExportJB.setSponsorPersonalFirstName("[Sponsor Contact] First Name");//33
		ctrpDraftCSVExportJB.setSponsorPersonalMiddleName("[Sponsor Contact] Middle Name");//34
		ctrpDraftCSVExportJB.setSponsorPersonalLastName("[Sponsor Contact] Last Name");//35
		
		//For setting the values in AddressJB  get Data on the Basis Of id SPONSOR_PERSONAL_FK_ADD
		ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().setAddPri("[Sponsor Contact] Street Address");//36
		ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().setAddCity("[Sponsor Contact] City");//37
		ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().setAddState("[Sponsor Contact] State/Province ");//38
		ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().setAddZip("[Sponsor Contact] Zip/Postal code");//39
		ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().setAddCountry("[Sponsor Contact] Country");//40
		ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().setAddEmail("[Sponsor Contact] Email Address");//41
		ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().setAddPhone("[Sponsor Contact] Phone ");//42
		ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().setAddTty("[Sponsor Contact] TTY");//43
		ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().setAddFax("[Sponsor Contact] FAX");//44
		ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().setAddUrl("[Sponsor Contact] URL");//45
		
		ctrpDraftCSVExportJB.setLeadOrgCtrpId("[Lead Organization] Organization PO-ID");//46
		//get [Lead Organization] Name from ER_site(fk_peradd) table with the primary key of er_add(pk_add)
		ctrpDraftCSVExportJB.setLeadOrgName("[Lead Organization] Name");//LEAD_ORG_FK_SITE 47
		
		//For setting the values in AddressJB  get Data on the Basis Of id LEAD_ORG_FK_ADD
		ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddPri("[Lead Organization] Street Address");//48
		ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddCity("[Lead Organization] City");//49
		ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddState("[Lead Organization] State/Province");//50
		ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddZip("[Lead Organization] Zip/Postal code ");//51
		ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddCountry("[Lead Organization] Country");//52
		ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddEmail("[Lead Organization] Email Address");//53
		ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddPhone("[Lead Organization] Phone");//54
		ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddTty("[Lead Organization] TTY");//55
		ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddFax("[Lead Organization] FAX");//56
		ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddUrl("[Lead Organization] URL");//57
		ctrpDraftCSVExportJB.setLeadOrgType("[Lead Organization] Organization Type");//58
		/*Principal Investigator*/
		ctrpDraftCSVExportJB.setPiCtrpId("[Principal Investigator] Person PO-ID");//59
		//ctrpDraftCSVExportJB.setPiFirstName("[Principal Investigator] First Name"); //60
		//ctrpDraftCSVExportJB.setPiMiddleName("[Principal Investigator] Middle Name");//61
		//ctrpDraftCSVExportJB.setPiLastName("[Principal Investigator] Last Name");//62
		//get user data from the er_user on the basis of PI_FK_USER
		ctrpDraftCSVExportJB.getPrincipalInvestgatorUserJB().setUserFirstName("[Principal Investigator] First Name");//PiFirstName 60
		ctrpDraftCSVExportJB.getPrincipalInvestgatorUserJB().setUserMidName("[Principal Investigator] Middle Name");//PiMiddleName 61
		ctrpDraftCSVExportJB.getPrincipalInvestgatorUserJB().setUserLastName("[Principal Investigator] Last Name");//PiLastName 62
		
		//first get the user data (by using  PI_FK_USER) from er_site after that get foreign key of er_add table and get the data
		ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddPri("[Principal Investigator] Street Address");//63
		ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddCity("[Principal Investigator] City");//64
		ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddState("[Principal Investigator] State/Province ");//65
		ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddZip("[Principal Investigator] Zip/Postal code");//66
		ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddCountry("[Principal Investigator] Country");//67
		ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddEmail("[Principal Investigator] Email Address");//68
		ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddPhone("[Principal Investigator] Phone");//69
		ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddTty("[Principal Investigator] TTY");//70
		ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddFax("[Principal Investigator] FAX");//71
		ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddUrl("[Principal Investigator] URL");//72
		/*Summary 4 Funding*/
		ctrpDraftCSVExportJB.setSumm4SponsorType("Summary 4 Funding Category");//73
		ctrpDraftCSVExportJB.setSumm4SponsorId("[Summary 4 Funding Sponsor/Source] Organization PO-ID");//74
		ctrpDraftCSVExportJB.setSumm4SponsorName("[Summary 4 Funding Sponsor/Source] Organization Name");//75
		//GET Summary For Funding Information On the basis Of SUMM4_SPONSOR_FK_ADD from address table
		ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddPri("[Summary 4 Funding Sponsor/Source] Street Address");//76
		ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddCity("[Summary 4 Funding Sponsor/Source] City");//77
		ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddState("[Summary 4 Funding Sponsor/Source] State/Province");//78
		ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddZip("[Summary 4 Funding Sponsor/Source] Zip/Postal code");//79
		ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddCountry("[Summary 4 Funding Sponsor/Source ] Country");//80
		ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddEmail("[Summary 4 Funding Sponsor/Source ] Email Address");//81
		ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddPhone("[Summary 4 Funding Sponsor/Source ] Phone");//81
		ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddTty("[Summary 4 Funding Sponsor/Source ] TTY");//83
		ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddFax("[Summary 4 Funding Sponsor/Source ] FAX");//84
		ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddUrl("[Summary 4 Funding Sponsor/Source ] URL");//85
		ctrpDraftCSVExportJB.setSponsorProgramCode("Program Code");//86

		//Get ER_STUDY_NIH_GRANT data with help of Fk_study ID 
		ctrpDraftCSVExportJB.getStudyNIHGrantJB().setFundMech("[NIH Grant] Funding Mechanism");//87
		ctrpDraftCSVExportJB.getStudyNIHGrantJB().setInstCode("[NIH Grant] Institute Code");//88
		ctrpDraftCSVExportJB.getStudyNIHGrantJB().setNihGrantSerial("[NIH Grant] Serial Number");//89
		ctrpDraftCSVExportJB.getStudyNIHGrantJB().setProgramCode("[NIH Grant] NCI Division/Program Code");//90
		
		ctrpDraftCSVExportJB.setFkCodelstCtrpStudyStatus("Current Trial Status");          //FK_CODELST_CTRP_STUDY_STATUS 91
		ctrpDraftCSVExportJB.setCtrpStudyStopReason("Why Study Stopped?");//92
		ctrpDraftCSVExportJB.setCtrpStudyStatusDate("Current Trial Status Date");//93
		ctrpDraftCSVExportJB.setCtrpStudyStartDate("Study Start Date");//94
		ctrpDraftCSVExportJB.setIsStartActual("Study Start Date Type");//95
		ctrpDraftCSVExportJB.setCtrpStudyCompDate("Primary Completion Date");//96
		ctrpDraftCSVExportJB.setIsCompActual("Primary Completion Date Type");//97
		
		//Get ER_STUDY_INDIDE data with help of Fk_study ID 
		ctrpDraftCSVExportJB.getStudyINDIDEJB().setTypeIndIde("IND/IDE Type");//98
		ctrpDraftCSVExportJB.getStudyINDIDEJB().setNumberIndIde("IND/IDE Number");//99
		ctrpDraftCSVExportJB.getStudyINDIDEJB().setFkIndIdeGrantor("IND/IDE Grantor");//100
		ctrpDraftCSVExportJB.getStudyINDIDEJB().setFkIndIdeHolder("IND/IDE Holder Type");//101
		ctrpDraftCSVExportJB.getStudyINDIDEJB().setFkProgramCode("[IND/IDE] NIH Institution");//102
		ctrpDraftCSVExportJB.getStudyINDIDEJB().setNciDevisionProgram("[IND/IDE] NCI Division /Program");//103
		ctrpDraftCSVExportJB.getStudyINDIDEJB().setFkAccessType("[IND/IDE] Has Expanded Access?");//104 
		ctrpDraftCSVExportJB.getStudyINDIDEJB().setExpandIndIdeAccess("[IND/IDE] Expanded Access Status");//105
		ctrpDraftCSVExportJB.getStudyINDIDEJB().setExemptINDIDE("[IND/IDE] Exempt Indicator");//106
		
		ctrpDraftCSVExportJB.setOversightCountry("Oversight Authority Country");//107
		ctrpDraftCSVExportJB.setOversightOrganization("Oversight Authority Organization Name");//108
		ctrpDraftCSVExportJB.setFdaIntvenIndicator("FDA Regulatory Information Indicator");//109
		ctrpDraftCSVExportJB.setSection801Indicator("Section 801 Indicator");//110
		ctrpDraftCSVExportJB.setDelayPostIndicator("Delayed Posting Indicator");//111
		ctrpDraftCSVExportJB.setDmAppointIndicator("Data Monitoring Committee Appointed Indicator");//112
		
		//Trail Document Section : FK_CTRP_DRAFT from ER_CTRP_DOCUMENTS table with primary key of ER_CTRP_DRAFT  (PK_CTRP_DRAFT)
		ctrpDraftCSVExportJB.setProtocolDocument("Protocol Document File Name");//113
		ctrpDraftCSVExportJB.setIrbApprovalDocument("IRB Approval Document File Name");//114
		ctrpDraftCSVExportJB.setParticipatingSitesDocument("Participating Sites Document File Name");//115
		ctrpDraftCSVExportJB.setInformedConsentDocument("Informed Consent Document File Name");//116
		ctrpDraftCSVExportJB.setOtherTrialRelatedDocument("Other Trial Related Document File Name");//117
		ctrpDraftCSVExportJB.setChangeMemoDocument("Change Memo Document Name");//118
		ctrpDraftCSVExportJB.setProtocolHighlightDocument("Protocol Highlight Document Name");//119
		return ctrpDraftCSVExportJB;
	}
	/*Added for download CVS Section : Yogendra Pratap Singh*/
	public ArrayList<CtrpDraftCSVExportJB> retriveNonIndustryCSVExportData(ArrayList<CtrpDraftCSVExportJB> exportList , ArrayList<CtrpDraftBean> aList){
		try {
			if(aList!=null && aList.size()>0){
				AddressAgentRObj addressAgentRObj = EJBUtil.getAddressAgentHome();
				UserAgentRObj  userAgentRObj =EJBUtil.getUserAgentHome();
				SiteAgentRObj siteAgentRObj = EJBUtil.getSiteAgentHome();
				StudyNIHGrantAgentRObj studyNIHGrantAgentRObj = EJBUtil.getStudyNIHGrantAgentHome();
				StudyINDIDEAgentRObj studyINDIDEAgentRObj = EJBUtil.getStudyINDIDEAgentHome();
				StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
				
				CodelstAgentRObj cCodelstAgentRObj = EJBUtil.getCodelstAgentHome();
				
				AddressBean sponsorAddressBean = null;
				AddressBean sponsorPersonalAddressBean = null;
				AddressBean leadOrgAddressBean = null;
				AddressBean piAddressBean = null;
				AddressBean summ4SponsorAddressBean = null;
				UserBean principalInvestgatorUserBean = null;
				StudyNIHGrantBean  studyNIHGrantBean = null;
				StudyBean studyBean = null;
				CodelstBean codelstBean = null;
				
				
				for(CtrpDraftBean ctrpDraftBean : aList){
					
					CtrpDraftCSVExportJB ctrpDraftCSVExportJB = new CtrpDraftCSVExportJB();
					if(null!=ctrpDraftBean.getFkStudy())
						studyBean = studyAgent.getStudyDetails(ctrpDraftBean.getFkStudy());
					else
						studyBean = new StudyBean();
					if(null==studyBean)studyBean = new StudyBean();
					ctrpDraftCSVExportJB.getStudyJB().setStudyNumber(replaceNullValues(studyBean.getStudyNumber()));//Study Number 1 Modified For Bug# 8466:Yogendra
					
					//String submissionType = cCodelstAgentRObj.getCodeDescription(ctrpDraftBean.getFkCodelstSubmissionType());// code list data
					//ctrpDraftCSVExportJB.setFkCodelstSubmissionType(replaceNullValues(submissionType));//2
					if(null!=ctrpDraftBean.getFkCodelstSubmissionType())
						codelstBean = cCodelstAgentRObj.getCodelstDetails( ctrpDraftBean.getFkCodelstSubmissionType());// code list data
					else
						codelstBean = new CodelstBean();
					if(null==codelstBean)codelstBean = new CodelstBean();
					ctrpDraftCSVExportJB.setFkCodelstSubmissionType(replaceNullValues( codelstBean.getClstSubType() ));//2 SUBMIT_ORG_TYPE
					//Modified For Bug# 8466:Yogendra
					//ctrpDraftCSVExportJB.setNciStudyId(replaceNullValues(ctrpDraftBean.getFkStudy()));//NCI Trial Identifier  3
					ctrpDraftCSVExportJB.setNciStudyId(replaceNullValues(ctrpDraftBean.getNciStudyId()));//NCI Trial Identifier 3
					String xmlReq = "";
					if(null!=ctrpDraftBean.getDraftXMLReqd())
						xmlReq =replaceNullValues(ctrpDraftBean.getDraftXMLReqd());
					ctrpDraftCSVExportJB.setDraftXMLReqd( (xmlReq.equalsIgnoreCase("") || xmlReq.equalsIgnoreCase("0") )  ?   "No"    :  "Yes" );//DRAFT_XML_REQD 4
					ctrpDraftCSVExportJB.setAmendNumber(replaceNullValues(ctrpDraftBean.getAmendNumber()));//5
					ctrpDraftCSVExportJB.setAmendDate(formatDateString( replaceNullValues(ctrpDraftBean.getAmendDate()) ));//6
					ctrpDraftCSVExportJB.setLeadOrgStudyId(replaceNullValues(ctrpDraftBean.getLeadOrgStudyId()));//7
					ctrpDraftCSVExportJB.setNctNumber(replaceNullValues(ctrpDraftBean.getNctNumber()));//8
					ctrpDraftCSVExportJB.setOtherNumber(replaceNullValues(ctrpDraftBean.getOtherNumber()));//9
					ctrpDraftCSVExportJB.setTriailDetailsTitle(replaceNullValues(studyBean.getStudyTitle()));//10
					//Identifies Study Type. Maps to Study research Type. 1-Interventional and 0-Observational
					String trailType = ""; 
					if(null!=ctrpDraftBean.getStudyType())
						trailType = replaceNullValues(ctrpDraftBean.getStudyType());
					ctrpDraftCSVExportJB.setStudyType(trailType.equalsIgnoreCase("1") ? "Interventional" : "Observational" );//11
					String studyPurpose = null;
					if(null!=ctrpDraftBean.getStudyPurpose())
						studyPurpose =  cCodelstAgentRObj.getCodeDescription(ctrpDraftBean.getStudyPurpose());
					ctrpDraftCSVExportJB.setStudyPurpose(replaceNullValues( studyPurpose ));//12
					ctrpDraftCSVExportJB.setStudyPurposeAdditionalQualifier(trailType.equalsIgnoreCase("1") ? "Other" : "" );//13
					ctrpDraftCSVExportJB.setStudyPurposeOther(replaceNullValues(ctrpDraftBean.getStudyPurposeOther()));//14
					String phase = null;
					if(null!=ctrpDraftBean.getFkCodelstPhase())
						phase = cCodelstAgentRObj.getCodeDescription(ctrpDraftBean.getFkCodelstPhase());// code list data
					ctrpDraftCSVExportJB.setFkCodelstPhase(replaceNullValues( phase ));//15
					ctrpDraftCSVExportJB.setDraftPilot(replaceNullValues(ctrpDraftBean.getDraftPilot()));//16
					ctrpDraftCSVExportJB.setSponsorCtrpId(replaceNullValues(ctrpDraftBean.getSponsorCtrpId()));//17
					SiteBean sponsorSiteBean =null;
					if(ctrpDraftBean.getFkSiteSponsor()!=null){
						SiteAgentRObj siteAgent = getSiteAgent();
						sponsorSiteBean = siteAgent.getSiteDetails(ctrpDraftBean.getFkSiteSponsor());
					}else{
						sponsorSiteBean = new SiteBean();
					}
					ctrpDraftCSVExportJB.setSponsorName(replaceNullValues(sponsorSiteBean.getSiteName()));//18
					//ctrpDraftCSVExportJB.setSponsorName(replaceNullValues(ctrpDraftBean.getFkSiteSponsor()));//18

					//For setting the values in AddressJB  set Data on the Basis Of id SPONSOR_FK_ADD
					if(null!=ctrpDraftBean.getFkAddSponsor())
						sponsorAddressBean = addressAgentRObj.getAddressDetails(ctrpDraftBean.getFkAddSponsor());
					else
						sponsorAddressBean = new AddressBean();
					if(null==sponsorAddressBean)sponsorAddressBean = new AddressBean();
					ctrpDraftCSVExportJB.getSponsorAddressJB().setAddPri(replaceNullValues(sponsorAddressBean.getAddPri()));//19
					ctrpDraftCSVExportJB.getSponsorAddressJB().setAddCity(replaceNullValues(sponsorAddressBean.getAddCity()));//20
					ctrpDraftCSVExportJB.getSponsorAddressJB().setAddState(replaceNullValues(sponsorAddressBean.getAddState()));//21
					ctrpDraftCSVExportJB.getSponsorAddressJB().setAddZip(replaceNullValues(sponsorAddressBean.getAddZip()));//22
					ctrpDraftCSVExportJB.getSponsorAddressJB().setAddCountry(replaceNullValues(sponsorAddressBean.getAddCountry()));//23
					ctrpDraftCSVExportJB.getSponsorAddressJB().setAddEmail(replaceNullValues(sponsorAddressBean.getAddEmail()));//24
					ctrpDraftCSVExportJB.getSponsorAddressJB().setAddPhone(replaceNullValues(sponsorAddressBean.getAddPhone()));//25
					ctrpDraftCSVExportJB.getSponsorAddressJB().setAddTty(replaceNullValues(sponsorAddressBean.getAddTty()));//26
					ctrpDraftCSVExportJB.getSponsorAddressJB().setAddFax(replaceNullValues(sponsorAddressBean.getAddFax()));//27
					ctrpDraftCSVExportJB.getSponsorAddressJB().setAddUrl(replaceNullValues(sponsorAddressBean.getAddUrl()));//28
					String respParty = ""; 
					String responsibleParty ="";
					if(null!=ctrpDraftBean.getResponsibleParty()){
						respParty = replaceNullValues(ctrpDraftBean.getResponsibleParty());
					if(respParty.equalsIgnoreCase("1"))
						responsibleParty = "PI";
					else if(respParty.equalsIgnoreCase("0"))
						responsibleParty = "Sponsor";
					}
					ctrpDraftCSVExportJB.setResponsibleParty(responsibleParty);//29
					//Added for Bug# 8478 For Responsible Party Contact 
					ctrpDraftCSVExportJB.setResponsiblePartyEmailAddress(replaceNullValues(ctrpDraftBean.getRpEmail()));//Responsible Party Email Address 30
					ctrpDraftCSVExportJB.setResponsiblePartyPhone(replaceNullValues(ctrpDraftBean.getRpPhone()));//Responsible Party Phone  31
					String rpExtn = null;
					if(ctrpDraftBean.getRpExtn()!=null)
						rpExtn = ctrpDraftBean.getRpExtn().toString();
					ctrpDraftCSVExportJB.setResponsiblePartyExt(replaceNullValues(rpExtn));//Responsible Party Ext 32
					
					String rpSponContactType = ""; 
					String rpSponsorContactType ="";
					if(null!=ctrpDraftBean.getRpSponsorContactType()){
						rpSponContactType = replaceNullValues(ctrpDraftBean.getRpSponsorContactType());
					if(rpSponContactType.equalsIgnoreCase("1"))
						rpSponsorContactType = "Personal";
					else if(rpSponContactType.equalsIgnoreCase("0"))
						rpSponsorContactType = "Generic";
					}
					ctrpDraftCSVExportJB.setRpSponsorContactType(rpSponsorContactType);//30
					ctrpDraftCSVExportJB.setSponsorGenericTitle(replaceNullValues(ctrpDraftBean.getSponsorGenericTitle()));//31
					ctrpDraftCSVExportJB.setRpSponsorContactId(replaceNullValues(ctrpDraftBean.getRpSponsorContactId()));//32
					UserBean sponsorPersonaluserBean = null;
					if(ctrpDraftBean.getFkUserSponsorPersonal()!=null){
						UserAgentRObj userAgent = getUserAgent();
						sponsorPersonaluserBean = userAgent.getUserDetails(ctrpDraftBean.getFkUserSponsorPersonal());
					}else{
						sponsorPersonaluserBean = new UserBean();
					}
					ctrpDraftCSVExportJB.setSponsorPersonalFirstName(replaceNullValues(sponsorPersonaluserBean.getUserFirstName()));//33
					ctrpDraftCSVExportJB.setSponsorPersonalMiddleName(replaceNullValues(sponsorPersonaluserBean.getUserMidName()));//34
					ctrpDraftCSVExportJB.setSponsorPersonalLastName(replaceNullValues(sponsorPersonaluserBean.getUserLastName()));//35
					//ctrpDraftCSVExportJB.setSponsorPersonalFirstName(replaceNullValues(ctrpDraftBean.getSponsorPersonalFirstName()));//33
					//ctrpDraftCSVExportJB.setSponsorPersonalMiddleName(replaceNullValues(ctrpDraftBean.getFkUserSponsorPersonal()));//34
					//ctrpDraftCSVExportJB.setSponsorPersonalLastName(replaceNullValues(ctrpDraftBean.getSponsorPersonalLastName()));//35
					
					//For setting the values in AddressJB  set Data on the Basis Of id SPONSOR_PERSONAL_FK_ADD
					if(null!=ctrpDraftBean.getFkAddSponsorPersonal())
						sponsorPersonalAddressBean = addressAgentRObj.getAddressDetails(ctrpDraftBean.getFkAddSponsorPersonal());
					else
						sponsorPersonalAddressBean = new AddressBean();
					if(null==sponsorPersonalAddressBean)sponsorPersonalAddressBean = new AddressBean();
					ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().setAddPri(replaceNullValues(sponsorPersonalAddressBean.getAddPri()));//36
					ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().setAddCity(replaceNullValues(sponsorPersonalAddressBean.getAddCity()));//37
					ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().setAddState(replaceNullValues(sponsorPersonalAddressBean.getAddState()));//38
					ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().setAddZip(replaceNullValues(sponsorPersonalAddressBean.getAddZip()));//39
					ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().setAddCountry(replaceNullValues(sponsorPersonalAddressBean.getAddCountry()));//40
					ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().setAddEmail(replaceNullValues(sponsorPersonalAddressBean.getAddEmail()));//41
					ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().setAddPhone(replaceNullValues(sponsorPersonalAddressBean.getAddPhone()));//42
					ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().setAddTty(replaceNullValues(sponsorPersonalAddressBean.getAddTty()));//43
					ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().setAddFax(replaceNullValues(sponsorPersonalAddressBean.getAddFax()));//44
					ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().setAddUrl(replaceNullValues(sponsorPersonalAddressBean.getAddUrl()));//45
					ctrpDraftCSVExportJB.setLeadOrgCtrpId(replaceNullValues( ctrpDraftBean.getLeadOrgCtrpId() ));//46
					//get [Lead Organization] Name from ER_site(fk_peradd) table with the primary key of er_add(pk_add)
					SiteBean leadOrgNameSiteBean =null;
					if(ctrpDraftBean.getFkSiteLeadOrg()!=null){
						SiteAgentRObj siteAgent = getSiteAgent();
						leadOrgNameSiteBean = siteAgent.getSiteDetails(ctrpDraftBean.getFkSiteLeadOrg());
					}else{
						leadOrgNameSiteBean = new SiteBean();
					}
					ctrpDraftCSVExportJB.setLeadOrgName(replaceNullValues(leadOrgNameSiteBean.getSiteName()));//47
					//ctrpDraftCSVExportJB.setLeadOrgName(replaceNullValues(ctrpDraftBean.getLeadOrgName()));//47
					//For setting the values in AddressJB  set Data on the Basis Of id LEAD_ORG_FK_ADD
					if(null!=ctrpDraftBean.getFkAddLeadOrg())
						leadOrgAddressBean = addressAgentRObj.getAddressDetails(ctrpDraftBean.getFkAddLeadOrg());
					else
						leadOrgAddressBean = new AddressBean();
					if(null==leadOrgAddressBean)leadOrgAddressBean = new AddressBean();
					ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddPri(replaceNullValues(leadOrgAddressBean.getAddPri()));//48
					ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddCity(replaceNullValues(leadOrgAddressBean.getAddCity()));//49
					ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddState(replaceNullValues(leadOrgAddressBean.getAddState()));//50
					ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddZip(replaceNullValues(leadOrgAddressBean.getAddZip()));//51
					ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddCountry(replaceNullValues(leadOrgAddressBean.getAddCountry()));//52
					ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddEmail(replaceNullValues(leadOrgAddressBean.getAddEmail()));//53
					ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddPhone(replaceNullValues(leadOrgAddressBean.getAddPhone()));//54
					ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddTty(replaceNullValues(leadOrgAddressBean.getAddTty()));//55
					ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddFax(replaceNullValues(leadOrgAddressBean.getAddFax()));//56
					ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddUrl(replaceNullValues(leadOrgAddressBean.getAddUrl()));//57
					String leadOrgType=null;
					if(ctrpDraftBean.getLeadOrgType()!=null)
						leadOrgType = cCodelstAgentRObj.getCodeDescription(ctrpDraftBean.getLeadOrgType());// code list data
					ctrpDraftCSVExportJB.setLeadOrgType(replaceNullValues(leadOrgType));//58
					/*Principal Investigator*/
					ctrpDraftCSVExportJB.setPiCtrpId(replaceNullValues( ctrpDraftBean.getPiCtrpId() ));//59
					//get user data from the er_user on the basis of PI_FK_USER
					if(null!=ctrpDraftBean.getFkUserPI())
						principalInvestgatorUserBean= userAgentRObj.getUserDetails(ctrpDraftBean.getFkUserPI());
					else 
						principalInvestgatorUserBean = new UserBean();
					if(null==principalInvestgatorUserBean)principalInvestgatorUserBean = new UserBean();
					ctrpDraftCSVExportJB.getPrincipalInvestgatorUserJB().setUserFirstName(replaceNullValues(principalInvestgatorUserBean.getUserFirstName()));//60
					ctrpDraftCSVExportJB.getPrincipalInvestgatorUserJB().setUserMidName(replaceNullValues(principalInvestgatorUserBean.getUserMidName()));//61
					ctrpDraftCSVExportJB.getPrincipalInvestgatorUserJB().setUserLastName(replaceNullValues(principalInvestgatorUserBean.getUserLastName()));//62
					//first get the user data (by using  PI_FK_USER) from er_site after that set foreign key of er_add table and get the data
					if(null!=ctrpDraftBean.getFkAddPi())
						piAddressBean = addressAgentRObj.getAddressDetails(ctrpDraftBean.getFkAddPi());
					else
						piAddressBean = new AddressBean();
					if(null==piAddressBean)piAddressBean = new AddressBean();
					ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddPri(replaceNullValues(piAddressBean.getAddPri()));//63
					ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddCity(replaceNullValues(piAddressBean.getAddCity()));//64
					ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddState(replaceNullValues(piAddressBean.getAddState()));//65
					ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddZip(replaceNullValues(piAddressBean.getAddZip()));//66
					ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddCountry(replaceNullValues(piAddressBean.getAddCountry()));//67
					ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddEmail(replaceNullValues(piAddressBean.getAddEmail()));//68
					ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddPhone(replaceNullValues(piAddressBean.getAddPhone()));//69
					ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddTty(replaceNullValues(piAddressBean.getAddTty()));//70
					ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddFax(replaceNullValues(piAddressBean.getAddFax()));//71
					ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddUrl(replaceNullValues(piAddressBean.getAddUrl()));//72
					/*Summary 4 Funding*/
					String summ4SponsorType = null;
					if(null!=ctrpDraftBean.getSumm4SponsorType())
						summ4SponsorType = cCodelstAgentRObj.getCodeDescription( ctrpDraftBean.getSumm4SponsorType() );// code list data
					ctrpDraftCSVExportJB.setSumm4SponsorType(replaceNullValues(summ4SponsorType));//73
					ctrpDraftCSVExportJB.setSumm4SponsorId(replaceNullValues(ctrpDraftBean.getSumm4SponsorId()));//74
					
					SiteBean siteSumm4SponsorSiteBean =null;
					if(ctrpDraftBean.getFkSiteSumm4Sponsor()!=null){
						SiteAgentRObj siteAgent = getSiteAgent();
						siteSumm4SponsorSiteBean = siteAgent.getSiteDetails(ctrpDraftBean.getFkSiteSumm4Sponsor());
					}else{
						siteSumm4SponsorSiteBean = new SiteBean();
					}
					ctrpDraftCSVExportJB.setSumm4SponsorName(replaceNullValues(siteSumm4SponsorSiteBean.getSiteName()));//75
					//ctrpDraftCSVExportJB.setSumm4SponsorName(replaceNullValues(ctrpDraftBean.getFkSiteSumm4Sponsor()));//75
					
					//GET Summary For Funding Information On the basis Of SUMM4_SPONSOR_FK_ADD from address table
					if(null!=ctrpDraftBean.getfkAddSumm4Sponsor())
						summ4SponsorAddressBean = addressAgentRObj.getAddressDetails(ctrpDraftBean.getfkAddSumm4Sponsor());
					else
						summ4SponsorAddressBean = new AddressBean();
					if(null==summ4SponsorAddressBean)summ4SponsorAddressBean = new AddressBean();
					ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddPri(replaceNullValues(summ4SponsorAddressBean.getAddPri()));//76
					ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddCity(replaceNullValues(summ4SponsorAddressBean.getAddCity()));//77
					ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddState(replaceNullValues(summ4SponsorAddressBean.getAddState()));//78
					ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddZip(replaceNullValues(summ4SponsorAddressBean.getAddZip()));//79
					ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddCountry(replaceNullValues(summ4SponsorAddressBean.getAddCountry()));//80
					ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddEmail(replaceNullValues(summ4SponsorAddressBean.getAddEmail()));//81
					ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddPhone(replaceNullValues(summ4SponsorAddressBean.getAddPhone()));//82
					ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddTty(replaceNullValues(summ4SponsorAddressBean.getAddTty()));//83
					ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddFax(replaceNullValues(summ4SponsorAddressBean.getAddFax()));//84
					ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddUrl(replaceNullValues(summ4SponsorAddressBean.getAddUrl()));//85
					ctrpDraftCSVExportJB.setSponsorProgramCode(replaceNullValues(ctrpDraftBean.getSponsorProgramCode()));//86
					/*CTRP STUDY NIH GRANT DATA */
					//Get ER_STUDY_NIH_GRANT data with help of Fk_study ID 
					//Modified for Bug 8473 : Yogendra Pratap
					StudyNIHGrantDao studyNIHGrantDao = null;
					StudyNIHGrantJB studyNIHGrantB = new StudyNIHGrantJB();
					if(null!=ctrpDraftBean.getFkStudy())
						studyNIHGrantDao = studyNIHGrantB.getStudyNIHGrantRecords(ctrpDraftBean.getFkStudy());
					
					ArrayList pk_NIhGrants = studyNIHGrantDao.getPkStudyNIhGrants();
					ArrayList fund_mechsms = studyNIHGrantDao.getFundMechsms();
					ArrayList inst_codes = studyNIHGrantDao.getInstitudeCodes();
					ArrayList program_codes = studyNIHGrantDao.getProgramCodes();
					ArrayList serial_numbers = studyNIHGrantDao.getNihGrantSerials();
					StringBuffer  tempFundMechsm=new StringBuffer();
					StringBuffer  tempInstCode=new StringBuffer();
					StringBuffer  tempProgramCode=new StringBuffer();
					StringBuffer  tempSerialNumber=new StringBuffer();
					if(null!=pk_NIhGrants && pk_NIhGrants.size()>0){
						for(int k=0;k<fund_mechsms.size();k++){
							Integer fundMach =(Integer)fund_mechsms.get(k);
							String	fundMachDesc = cCodelstAgentRObj.getCodeDescription( fundMach );// code list data
							if(fund_mechsms.get(k)!=null){
							if(k < fund_mechsms.size()-1)
								tempFundMechsm.append(replaceNullValues(fundMachDesc)).append("~");
							else
								tempFundMechsm.append(replaceNullValues(fundMachDesc));
							}
						}
						for(int k=0;k<inst_codes.size();k++){
							Integer inst_cod =(Integer)inst_codes.get(k);
							String	inst_codDesc = cCodelstAgentRObj.getCodeDescription( inst_cod );// code list data
							if(inst_codes.get(k)!=null){
								if(k < inst_codes.size()-1)
									tempInstCode.append(replaceNullValues(inst_codDesc)).append("~");
								else
									tempInstCode.append(replaceNullValues(inst_codDesc));
							}
						}
						for(int k=0;k<program_codes.size();k++){
							Integer program_cod =(Integer)program_codes.get(k);
							String	program_codDesc = cCodelstAgentRObj.getCodeDescription( program_cod );// code list data
							if(program_codes.get(k)!=null){
								if(k < program_codes.size()-1)
									tempProgramCode.append(replaceNullValues(program_codDesc)).append("~");
								else
									tempProgramCode.append(replaceNullValues(program_codDesc));
							}
						}
						for(int k=0;k<serial_numbers.size();k++){
							String serial_num =(String)serial_numbers.get(k);
							if(serial_numbers.get(k)!=null){
								if(k < serial_numbers.size()-1)
									tempSerialNumber.append(replaceNullValues(serial_num)).append("~");
								else
									tempSerialNumber.append(replaceNullValues(serial_num));
							}
						}
					}
					ctrpDraftCSVExportJB.getStudyNIHGrantJB().setFundMech(replaceNullValues(tempFundMechsm.toString()));//87
					ctrpDraftCSVExportJB.getStudyNIHGrantJB().setInstCode(replaceNullValues(tempInstCode.toString()));//88
					ctrpDraftCSVExportJB.getStudyNIHGrantJB().setNihGrantSerial(replaceNullValues(tempSerialNumber.toString()));//89
					ctrpDraftCSVExportJB.getStudyNIHGrantJB().setProgramCode(replaceNullValues(tempProgramCode.toString()));//90
					String ctrpStudyStatus = null;
					if(null!=ctrpDraftBean.getFkCodelstCtrpStudyStatus())
						ctrpStudyStatus = cCodelstAgentRObj.getCodeDescription( ctrpDraftBean.getFkCodelstCtrpStudyStatus() );// code list data
					ctrpDraftCSVExportJB.setFkCodelstCtrpStudyStatus(replaceNullValues( ctrpStudyStatus ));  //FK_CODELST_CTRP_STUDY_STATUS 91
					ctrpDraftCSVExportJB.setCtrpStudyStopReason(replaceNullValues(ctrpDraftBean.getCtrpStudyStopReason()));//92
					ctrpDraftCSVExportJB.setCtrpStudyStatusDate(formatDateString( replaceNullValues(ctrpDraftBean.getCtrpStudyStatusDate()) ));//93
					ctrpDraftCSVExportJB.setCtrpStudyStartDate(formatDateString( replaceNullValues(ctrpDraftBean.getCtrpStudyStartDate()) ));//94
					int isStartActual = 0;
					if(null!= ctrpDraftBean.getIsStartActual())
						isStartActual = ctrpDraftBean.getIsStartActual();
					ctrpDraftCSVExportJB.setIsStartActual(replaceNullValues( (isStartActual==1 ) ? "Actual" :"Anticipated"));//95
					ctrpDraftCSVExportJB.setCtrpStudyCompDate(formatDateString(replaceNullValues(ctrpDraftBean.getCtrpStudyCompDate()) ));//96
					int IsCompActual = 0;
					if(null!=ctrpDraftBean.getIsCompActual())
						IsCompActual = ctrpDraftBean.getIsCompActual();
					ctrpDraftCSVExportJB.setIsCompActual(replaceNullValues((IsCompActual==1) ? "Actual" :"Anticipated"));//97
					
					/*CTRP STUDY INDIDE data from ER_STUDY_INDIDE table with help of fk_study ID */
					StudyINDIDEDAO studyINDIDEDAO = studyINDIDEAgentRObj.getStudyINDIDEInfo(ctrpDraftBean.getFkStudy());
					StringBuilder indIdes = new StringBuilder();
					StringBuilder typeIndIdes = new StringBuilder();
					StringBuilder numberIndIdes = new StringBuilder();
					StringBuilder grantors = new StringBuilder();
					StringBuilder holders = new StringBuilder();
					StringBuilder programCodes = new StringBuilder();
					StringBuilder expandedAccess = new StringBuilder();
					StringBuilder accessTypes = new StringBuilder();
					StringBuilder exemptIndIdes = new StringBuilder();
					if(null!=studyINDIDEDAO){
						ArrayList<Integer>	idIndIdeList =studyINDIDEDAO.getIdIndIdeList();
						ArrayList<Integer>	typeIndIdeList =studyINDIDEDAO.getTypeIndIdeList();
						ArrayList<String>	numberIndIdeList =studyINDIDEDAO.getNumberIndIdeList();
						ArrayList<Integer>	grantorList =studyINDIDEDAO.getGrantorList();
						ArrayList<Integer>	holderList =studyINDIDEDAO.getHolderList();
						ArrayList<Integer>	programCodeList =studyINDIDEDAO.getProgramCodeList();
						ArrayList<Integer>	expandedAccessList =studyINDIDEDAO.getExpandedAccessList();
						ArrayList<Integer>	accessTypeList =studyINDIDEDAO.getAccessTypeList();
						ArrayList<Integer>	exemptIndIdeList =studyINDIDEDAO.getExemptIndIdeList();
						if(null!=idIndIdeList && idIndIdeList.size()>0){
							int i = 1;
							for(Integer  indIde:idIndIdeList){
								if(i!=typeIndIdeList.size())
									indIdes=indIdes.append(replaceNullValues(indIde)).append("~");
								else
									indIdes=indIdes.append(replaceNullValues(indIde));
								i++;
							}
						}
						if(null!=typeIndIdeList && typeIndIdeList.size()>0){
							int i=1;
							for(Integer  typeIndIde:typeIndIdeList){
								//Identifies INDIDE Type. 1-IND, 2-IDE
								if(i!=typeIndIdeList.size())
									typeIndIdes=typeIndIdes.append(( typeIndIde==1 ) ? "IND" : "IDE").append("~");
								else
									typeIndIdes=typeIndIdes.append(( typeIndIde==1 ) ? "IND" : "IDE");
								i++;
							}
						}
						if(null!=numberIndIdeList && numberIndIdeList.size()>0){
							int i=1;
							for(String  numberIndIde:numberIndIdeList){
								if(i!=numberIndIdeList.size())
									numberIndIdes=numberIndIdes.append(replaceNullValues(numberIndIde)).append("~");
								else
									numberIndIdes=numberIndIdes.append(replaceNullValues(numberIndIde));
								i++;
							}
						}
						if(null!=grantorList && grantorList.size()>0){
							int i=1;
							for(Integer  grantor:grantorList){
								String grantorValue = cCodelstAgentRObj.getCodeDescription( grantor );
								if(i!=grantorList.size())
									grantors=grantors.append(replaceNullValues(grantorValue)).append("~");
								else
									grantors=grantors.append(replaceNullValues(grantorValue));
								i++;	
							}
						}
						if(null!=holderList && holderList.size()>0){
							int i=1;
							for(Integer  holder:holderList){
								String holderValue = cCodelstAgentRObj.getCodeDescription( holder );
								if(i!=holderList.size())
									holders=holders.append( replaceNullValues(holderValue) ).append("~");
								else
									holders=holders.append( replaceNullValues(holderValue) );
								i++;
							}
						}
						if(null!=programCodeList && programCodeList.size()>0){
							int i=1;
							for(Integer  programCode:programCodeList){
								String programCodeValue = cCodelstAgentRObj.getCodeDescription( programCode );
								if(i!=programCodeList.size())
									programCodes=programCodes.append( replaceNullValues(programCodeValue) ).append("~");
								else
									programCodes=programCodes.append( replaceNullValues(programCodeValue) );
								i++;
							}
						}
						if(null!=expandedAccessList && expandedAccessList.size()>0){
							int i=1;
							for(Integer  expandedAcc:expandedAccessList){
								if(i!=expandedAccessList.size())
									expandedAccess=expandedAccess.append( (expandedAcc==1 ) ? "Yes" : "No").append("~");
								else
									expandedAccess=expandedAccess.append( (expandedAcc==1 ) ? "Yes" : "No");
								i++;
							}
						}
						if(null!=accessTypeList && accessTypeList.size()>0){
							int i=1;
							for(Integer  accessType:accessTypeList){
								String accessTypeValue = cCodelstAgentRObj.getCodeDescription( accessType );
								if(i!=accessTypeList.size())
									accessTypes=accessTypes.append( replaceNullValues(accessTypeValue) ).append("~");
								else
									accessTypes=accessTypes.append( replaceNullValues(accessTypeValue) );
								i++;
							}
						}
						if(null!=exemptIndIdeList && exemptIndIdeList.size()>0){
							int i=1;
							for(Integer  exemptIndIde:exemptIndIdeList){
								if(i!=exemptIndIdeList.size())
									exemptIndIdes=exemptIndIdes.append( (exemptIndIde==1 ) ? "Yes" : "No" ).append("~");
								else
									exemptIndIdes=exemptIndIdes.append( (exemptIndIde==1 ) ? "Yes" : "No" );
								i++;	
							}
						}
					}
					//ctrpDraftCSVExportJB.getStudyINDIDEJB().setPkStudyINDIDE(indIdes.toString());
					ctrpDraftCSVExportJB.getStudyINDIDEJB().setTypeIndIde(replaceNullValues( typeIndIdes ));//98
					ctrpDraftCSVExportJB.getStudyINDIDEJB().setNumberIndIde(replaceNullValues( numberIndIdes ));//99
					ctrpDraftCSVExportJB.getStudyINDIDEJB().setFkIndIdeGrantor(replaceNullValues( grantors ));//100
					ctrpDraftCSVExportJB.getStudyINDIDEJB().setFkIndIdeHolder(replaceNullValues( holders  ));//101
					ctrpDraftCSVExportJB.getStudyINDIDEJB().setFkProgramCode(replaceNullValues( programCodes ));//102
					ctrpDraftCSVExportJB.getStudyINDIDEJB().setNciDevisionProgram( replaceNullValues( programCodes ));//103
					ctrpDraftCSVExportJB.getStudyINDIDEJB().setFkAccessType(replaceNullValues( expandedAccess ));//104 
					ctrpDraftCSVExportJB.getStudyINDIDEJB().setExpandIndIdeAccess(replaceNullValues( accessTypes ));//105
					ctrpDraftCSVExportJB.getStudyINDIDEJB().setExemptINDIDE(replaceNullValues( exemptIndIdes ));//106
					//Modified For Bug# 8468: Yogendra Pratap
					String oversightCountry  = null;
					if(null!=ctrpDraftBean.getOversightCountry())
						oversightCountry = cCodelstAgentRObj.getCodeDescription( ctrpDraftBean.getOversightCountry() );
					ctrpDraftCSVExportJB.setOversightCountry(replaceNullValues(oversightCountry));//107
								
					String oversightOrganization  = null;
					if(null!=ctrpDraftBean.getOversightOrganization())
						oversightOrganization = cCodelstAgentRObj.getCodeDescription( ctrpDraftBean.getOversightOrganization() );
					ctrpDraftCSVExportJB.setOversightOrganization(replaceNullValues(oversightOrganization));//108
					String fdaIntvenInd = ""; 
					String fdaIntvenIndicator ="";
					if(null!=ctrpDraftBean.getFdaIntvenIndicator()){
						fdaIntvenInd = replaceNullValues(ctrpDraftBean.getFdaIntvenIndicator());
					if(fdaIntvenInd.equalsIgnoreCase("1"))
						fdaIntvenIndicator = "Yes";
					else if(fdaIntvenInd.equalsIgnoreCase("0"))
						fdaIntvenIndicator = "No";
					}
					ctrpDraftCSVExportJB.setFdaIntvenIndicator(fdaIntvenIndicator);//109
					String section801Ind = ""; 
					String section801Indicator ="";
					if(null!=ctrpDraftBean.getSection801Indicator()){
						section801Ind = replaceNullValues(ctrpDraftBean.getSection801Indicator());
					if(section801Ind.equalsIgnoreCase("1"))
						section801Indicator = "Yes";
					else if(section801Ind.equalsIgnoreCase("0"))
						section801Indicator = "No";
					}
					ctrpDraftCSVExportJB.setSection801Indicator(section801Indicator);//110
					String delayPostInd = ""; 
					String delayPostIndicator ="";
					if(null!=ctrpDraftBean.getDelayPostIndicator()){
						delayPostInd = replaceNullValues(ctrpDraftBean.getDelayPostIndicator());
					if(delayPostInd.equalsIgnoreCase("1"))
						delayPostIndicator = "Yes";
					else if(delayPostInd.equalsIgnoreCase("0"))
						delayPostIndicator = "No";
					}
					ctrpDraftCSVExportJB.setDelayPostIndicator(delayPostIndicator);//111
					String dmAppointInd = ""; 
					String dmAppointIndicator ="";
					if(null!=ctrpDraftBean.getDmAppointIndicator()){
						dmAppointInd = replaceNullValues(ctrpDraftBean.getDmAppointIndicator());
					if(dmAppointInd.equalsIgnoreCase("1"))
						dmAppointIndicator = "Yes";
					else if(dmAppointInd.equalsIgnoreCase("0"))
						dmAppointIndicator = "No";
					}
					ctrpDraftCSVExportJB.setDmAppointIndicator(dmAppointIndicator);//112
					
					//Trail Document Section : FK_CTRP_DRAFT from ER_CTRP_DOCUMENTS table with primary key of ER_CTRP_DRAFT (PK_CTRP_DRAFT)
					
					//retreiveCtrpDocumentInfo(ctrpDraftBean.getId());
					
					CTRPDraftDocDao ctrpDraftDao= null;
					CTRPDraftDocJB ctrpDraftDocJB = new CTRPDraftDocJB();
					ctrpDraftDao=ctrpDraftDocJB.getCTRPDraftDocDetails(ctrpDraftBean.getId());
					//ArrayList< Integer> ctrpDrafts=ctrpDraftDao.getCtrpDrafts();
					//Changes By Yogendra For : 22495-CTRP Documents File
					ctrpDraftCSVExportJB.setId(ctrpDraftBean.getId().toString());
					List<AppendixBean> appenxBeanList= new ArrayList<AppendixBean>();
					if(ctrpDraftDao.getCtrpDocids().size() >0){
						CodeDao codeDao= new CodeDao();
						ArrayList<Integer>  ctrpDraftDocTypes = ctrpDraftDao.getCtrpDocTypes();
						ArrayList<Integer>  ctrpDraftStudyAppxs = ctrpDraftDao.getStudyAppendixs();
						ArrayList<Integer>  trialCtrpDraftPKs = ctrpDraftDao.getCtrpDocids();
						for(int i=0; i< ctrpDraftDao.getCtrpDocids().size();i++){	
							try{
								AppendixJB appnxJB= new AppendixJB();
								Integer ctrpStudyAppndx=0;
								Integer trialCtrpDraftPK=0;
								ctrpStudyAppndx=ctrpDraftStudyAppxs.get(i);
								trialCtrpDraftPK=trialCtrpDraftPKs.get(i);
								appnxJB.setId(ctrpStudyAppndx);
								AppendixBean appenxBean=appnxJB.getAppendixDetails();
								//Changes By Yogendra For : 22495-CTRP Documents File
								appenxBeanList.add(appenxBean);
								//Modified for Bug# 8471:Yogendra
								String studyaddDixUtl = appenxBean.getAppendixUrl_File();
								String draftDocType=codeDao.getCodeSubtype(ctrpDraftDocTypes.get(i));
								if(draftDocType.equalsIgnoreCase(CtrpDraftJB.CDLST_PROTOCOLDOC)){
									ctrpDraftCSVExportJB.setProtocolDocument(replaceNullValues(studyaddDixUtl));//113
								}if(draftDocType.equalsIgnoreCase(CtrpDraftJB.CDLST_IRBAPPROVAL)){
									ctrpDraftCSVExportJB.setIrbApprovalDocument(replaceNullValues(studyaddDixUtl));//114
								}if(draftDocType.equalsIgnoreCase(CtrpDraftJB.CDLST_PARTSITESLIST)){
									ctrpDraftCSVExportJB.setParticipatingSitesDocument(replaceNullValues(studyaddDixUtl));//115
								}if(draftDocType.equalsIgnoreCase(CtrpDraftJB.CDLST_INFORMEDCONSENT)){						 
									ctrpDraftCSVExportJB.setInformedConsentDocument(replaceNullValues(studyaddDixUtl));//116
								}if(draftDocType.equalsIgnoreCase(CtrpDraftJB.CDLST_OTHER)){
									ctrpDraftCSVExportJB.setOtherTrialRelatedDocument(replaceNullValues(studyaddDixUtl));//117
								}if(draftDocType.equalsIgnoreCase(CtrpDraftJB.CDLST_CHANGEMEMODOC)){
									ctrpDraftCSVExportJB.setChangeMemoDocument(replaceNullValues(studyaddDixUtl));//118
								}if(draftDocType.equalsIgnoreCase(CtrpDraftJB.CDLST_PROTHIGHLIGHT)){
									ctrpDraftCSVExportJB.setProtocolHighlightDocument(replaceNullValues(studyaddDixUtl));//119
								}
								ctrpDraftCSVExportJB.setDocumentInfoList(appenxBeanList);
							}catch (Exception e) {}
						}
					}
					exportList.add(ctrpDraftCSVExportJB);
				}
			}
		} catch (Exception e) {
			Rlog.fatal("ctrp", "Exception in CtrpDraftDocJBHelper.retreiveCtrpDocumentInforetriveNonIndustryCSVExportData "+e);
			e.printStackTrace();
		}
		return exportList;
	}
	/*Added for download CVS Section : Yogendra Pratap Singh*/
	public FileWriter generateNonIndustryCSVDcoument(ArrayList<CtrpDraftCSVExportJB> exportList,FileWriter fw)  {
		try{
		for(CtrpDraftCSVExportJB ctrpDraftCSVExportJB:exportList ){
			fw.append(ctrpDraftCSVExportJB.getStudyJB().getStudyNumber()).append(",");//1
			fw.append(ctrpDraftCSVExportJB.getFkCodelstSubmissionType()).append(",");//2
			fw.append(ctrpDraftCSVExportJB.getNciStudyId()).append(",");//Study Title 3
			fw.append(ctrpDraftCSVExportJB.getDraftXMLReqd()).append(",");//4
			fw.append(ctrpDraftCSVExportJB.getAmendNumber()).append(",");//5
			fw.append(ctrpDraftCSVExportJB.getAmendDate()).append(",");//6
			fw.append(ctrpDraftCSVExportJB.getLeadOrgStudyId()).append(",");//7
			fw.append(ctrpDraftCSVExportJB.getNctNumber()).append(",");//8
			fw.append(ctrpDraftCSVExportJB.getOtherNumber()).append(",");//9
			fw.append(ctrpDraftCSVExportJB.getTriailDetailsTitle()).append(",");//10
			fw.append(ctrpDraftCSVExportJB.getStudyType()).append(",");//11
			fw.append(ctrpDraftCSVExportJB.getStudyPurpose()).append(",");//12
			fw.append(ctrpDraftCSVExportJB.getStudyPurposeAdditionalQualifier()).append(",");//13
			//[Primary Purpose] Other Text					//14
			fw.append(ctrpDraftCSVExportJB.getStudyPurposeOther()).append(",");//14
			fw.append(ctrpDraftCSVExportJB.getFkCodelstPhase()).append(",");//15
			fw.append(ctrpDraftCSVExportJB.getDraftPilot()).append(",");//16
			fw.append(ctrpDraftCSVExportJB.getSponsorCtrpId()).append(",");//17
			fw.append(ctrpDraftCSVExportJB.getSponsorName()).append(",");//18
			//For getting the values in AddressJB  get Data on the Basis Of id SPONSOR_FK_ADD
			fw.append(ctrpDraftCSVExportJB.getSponsorAddressJB().getAddPri()).append(",");//19
			fw.append(ctrpDraftCSVExportJB.getSponsorAddressJB().getAddCity()).append(",");//20
			fw.append(ctrpDraftCSVExportJB.getSponsorAddressJB().getAddState()).append(",");//21
			fw.append(ctrpDraftCSVExportJB.getSponsorAddressJB().getAddZip()).append(",");//22
			fw.append(ctrpDraftCSVExportJB.getSponsorAddressJB().getAddCountry()).append(",");//23
			fw.append(ctrpDraftCSVExportJB.getSponsorAddressJB().getAddEmail()).append(",");//24
			fw.append(ctrpDraftCSVExportJB.getSponsorAddressJB().getAddPhone()).append(",");//25
			fw.append(ctrpDraftCSVExportJB.getSponsorAddressJB().getAddTty()).append(",");//26
			fw.append(ctrpDraftCSVExportJB.getSponsorAddressJB().getAddFax()).append(",");//27
			fw.append(ctrpDraftCSVExportJB.getSponsorAddressJB().getAddUrl()).append(",");//28
			fw.append(ctrpDraftCSVExportJB.getResponsibleParty()).append(",");//29
			//Modified for Bug# 8478 For Responsible Party Contact 
			fw.append(ctrpDraftCSVExportJB.getResponsiblePartyEmailAddress()).append(",");//Responsible Party Email Address 30
			fw.append(ctrpDraftCSVExportJB.getResponsiblePartyPhone()).append(",");//Responsible Party Phone  31
			fw.append(ctrpDraftCSVExportJB.getResponsiblePartyExt()).append(",");//Responsible Party Ext 32
			
			fw.append(ctrpDraftCSVExportJB.getRpSponsorContactType()).append(",");//30
			fw.append(ctrpDraftCSVExportJB.getSponsorGenericTitle()).append(",");//31
			fw.append(ctrpDraftCSVExportJB.getRpSponsorContactId()).append(",");//32
			fw.append(ctrpDraftCSVExportJB.getSponsorPersonalFirstName()).append(",");//33
			fw.append(ctrpDraftCSVExportJB.getSponsorPersonalMiddleName()).append(",");//34
			fw.append(ctrpDraftCSVExportJB.getSponsorPersonalLastName()).append(",");//35
			//For getting the values in AddressJB  get Data on the Basis Of id SPONSOR_PERSONAL_FK_ADD
			fw.append(ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().getAddPri()).append(",");//36
			fw.append(ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().getAddCity()).append(",");//37
			fw.append(ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().getAddState()).append(",");//38
			fw.append(ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().getAddZip()).append(",");//39
			fw.append(ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().getAddCountry()).append(",");//40
			fw.append(ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().getAddEmail()).append(",");//41
			fw.append(ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().getAddPhone()).append(",");//42
			fw.append(ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().getAddTty()).append(",");//43
			fw.append(ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().getAddFax()).append(",");//44
			fw.append(ctrpDraftCSVExportJB.getPersonalSponsorAddressJB().getAddUrl()).append(",");//45
			fw.append(ctrpDraftCSVExportJB.getLeadOrgCtrpId()).append(",");//46
			//get [Lead Organization] Name from ER_site(fk_peradd) table with the primary key of er_add(pk_add)
			fw.append(ctrpDraftCSVExportJB.getLeadOrgName()).append(",");//47
			//fw.append((ctrpDraftCSVExportJB.getFkAddLeadOrg()));//48
			//For getting the values in AddressJB  get Data on the Basis Of id LEAD_ORG_FK_ADD
			fw.append(ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().getAddPri()).append(",");//48
			fw.append(ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().getAddCity()).append(",");//49
			fw.append(ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().getAddState()).append(",");//50
			fw.append(ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().getAddZip()).append(",");//51
			fw.append(ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().getAddCountry()).append(",");//52
			fw.append(ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().getAddEmail()).append(",");//53
			fw.append(ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().getAddPhone()).append(",");//54
			fw.append(ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().getAddTty()).append(",");//55
			fw.append(ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().getAddFax()).append(",");//56
			fw.append(ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().getAddUrl()).append(",");//57
			fw.append(ctrpDraftCSVExportJB.getLeadOrgType()).append(",");//58
			/*Principal Investigator*/
			fw.append(ctrpDraftCSVExportJB.getPiCtrpId()).append(",");//59
			//ctrpDraftCSVExportJB.getPiFirstName("[Principal Investigator] First Name"));
			//ctrpDraftCSVExportJB.getPiMiddleName("[Principal Investigator] Middle Name"));
			//ctrpDraftCSVExportJB.getPiLastName("[Principal Investigator] Last Name"));
			//get user data from the er_user on the basis of PI_FK_USER
			fw.append(ctrpDraftCSVExportJB.getPrincipalInvestgatorUserJB().getUserFirstName()).append(",");//60
			fw.append(ctrpDraftCSVExportJB.getPrincipalInvestgatorUserJB().getUserMidName()).append(",");//61
			fw.append(ctrpDraftCSVExportJB.getPrincipalInvestgatorUserJB().getUserLastName()).append(",");//62
			//first get the user data (by using  PI_FK_USER) from er_site after that get foreign key of er_add table and get the data
			fw.append(ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().getAddPri()).append(",");//63
			fw.append(ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().getAddCity()).append(",");//64
			fw.append(ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().getAddState()).append(",");//65
			fw.append(ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().getAddZip()).append(",");//66
			fw.append(ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().getAddCountry()).append(",");//67
			fw.append(ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().getAddEmail()).append(",");//68
			fw.append(ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().getAddPhone()).append(",");//69
			fw.append(ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().getAddTty()).append(",");//70
			fw.append(ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().getAddFax()).append(",");//71
			fw.append(ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().getAddUrl()).append(",");//72
			/*Summary 4 Funding*/
			fw.append(ctrpDraftCSVExportJB.getSumm4SponsorType()).append(",");//73
			fw.append(ctrpDraftCSVExportJB.getSumm4SponsorId()).append(",");//74
			fw.append(ctrpDraftCSVExportJB.getSumm4SponsorName()).append(",");//75
			//GET Summary For Funding Information On the basis Of SUMM4_SPONSOR_FK_ADD from address table
			fw.append(ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().getAddPri()).append(",");//76
			fw.append(ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().getAddCity()).append(",");//77
			fw.append(ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().getAddState()).append(",");//78
			fw.append(ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().getAddZip()).append(",");//79
			fw.append(ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().getAddCountry()).append(",");//80
			fw.append(ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().getAddEmail()).append(",");//81
			fw.append(ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().getAddPhone()).append(",");//81
			fw.append(ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().getAddTty()).append(",");//83
			fw.append(ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().getAddFax()).append(",");//84
			fw.append(ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().getAddUrl()).append(",");//85
			fw.append(ctrpDraftCSVExportJB.getSponsorProgramCode()).append(",");//86
			/*CTRP STUDY NIH GRANT DATA */
			//Get ER_STUDY_NIH_GRANT data with help of Fk_study ID 
			fw.append(ctrpDraftCSVExportJB.getStudyNIHGrantJB().getFundMech()).append(",");//87
			fw.append(ctrpDraftCSVExportJB.getStudyNIHGrantJB().getInstCode()).append(",");//88
			fw.append(ctrpDraftCSVExportJB.getStudyNIHGrantJB().getNihGrantSerial()).append(",");//89
			fw.append(ctrpDraftCSVExportJB.getStudyNIHGrantJB().getProgramCode()).append(",");//90
			fw.append(ctrpDraftCSVExportJB.getFkCodelstCtrpStudyStatus()).append(","); //FK_CODELST_CTRP_STUDY_STATUS 91
			fw.append(ctrpDraftCSVExportJB.getCtrpStudyStopReason()).append(",");//92
			fw.append(ctrpDraftCSVExportJB.getCtrpStudyStatusDate()).append(",");//93
			fw.append(ctrpDraftCSVExportJB.getCtrpStudyStartDate()).append(",");//94
			fw.append(ctrpDraftCSVExportJB.getIsStartActual()).append(",");//95
			fw.append(ctrpDraftCSVExportJB.getCtrpStudyCompDate()).append(",");//96
			fw.append(ctrpDraftCSVExportJB.getIsCompActual()).append(",");//97
			//Get ER_STUDY_INDIDE data with help of Fk_study ID 
			fw.append(ctrpDraftCSVExportJB.getStudyINDIDEJB().getTypeIndIde()).append(",");//98
			fw.append(ctrpDraftCSVExportJB.getStudyINDIDEJB().getNumberIndIde()).append(",");//99
			fw.append(ctrpDraftCSVExportJB.getStudyINDIDEJB().getFkIndIdeGrantor()).append(",");//100
			fw.append(ctrpDraftCSVExportJB.getStudyINDIDEJB().getFkIndIdeHolder()).append(",");//101
			fw.append(ctrpDraftCSVExportJB.getStudyINDIDEJB().getFkProgramCode()).append(",");//102
			//Modified For Bug# 8469:Yogendra Pratap
			fw.append(ctrpDraftCSVExportJB.getStudyINDIDEJB().getNciDevisionProgram()).append(",");//103
			fw.append(ctrpDraftCSVExportJB.getStudyINDIDEJB().getFkAccessType()).append(",");//104 
			fw.append(ctrpDraftCSVExportJB.getStudyINDIDEJB().getExpandIndIdeAccess()).append(",");//105
			fw.append(ctrpDraftCSVExportJB.getStudyINDIDEJB().getExemptINDIDE()).append(",");//106
			fw.append(ctrpDraftCSVExportJB.getOversightCountry()).append(",");//107
			fw.append(ctrpDraftCSVExportJB.getOversightOrganization()).append(",");//108
			fw.append(ctrpDraftCSVExportJB.getFdaIntvenIndicator()).append(",");//109
			fw.append(ctrpDraftCSVExportJB.getSection801Indicator()).append(",");//110
			fw.append(ctrpDraftCSVExportJB.getDelayPostIndicator()).append(",");//111
			fw.append(ctrpDraftCSVExportJB.getDmAppointIndicator()).append(",");//112
			//Trail Document Section : FK_CTRP_DRAFT from ER_CTRP_DOCUMENTS table with primary key of ER_CTRP_DRAFT  (PK_CTRP_DRAFT)
			fw.append(replaceNullValues(ctrpDraftCSVExportJB.getProtocolDocument())).append(",");//113
			fw.append(replaceNullValues(ctrpDraftCSVExportJB.getIrbApprovalDocument())).append(",");//114
			fw.append(replaceNullValues(ctrpDraftCSVExportJB.getParticipatingSitesDocument())).append(",");//115
			fw.append(replaceNullValues(ctrpDraftCSVExportJB.getInformedConsentDocument())).append(",");//116
			fw.append(replaceNullValues(ctrpDraftCSVExportJB.getOtherTrialRelatedDocument())).append(",");//117
			fw.append(replaceNullValues(ctrpDraftCSVExportJB.getChangeMemoDocument())).append(",");//118
			fw.append(replaceNullValues(ctrpDraftCSVExportJB.getProtocolHighlightDocument()));//119
			fw.write(LINE_SEPRATOR);//120
		}
		}catch(Exception e){
			Rlog.fatal("ctrp", "Exception in CtrpDraftDocJBHelper.generateNonIndustryCSVDcoument "+e);
			e.printStackTrace();
		}
		return fw;
	}
	
	/*Added for download CVS Section : Yogendra Pratap Singh*/
	public CtrpDraftCSVExportJB addIndustryCSVExportHeader() {
		CtrpDraftCSVExportJB ctrpDraftCSVExportJB = new CtrpDraftCSVExportJB();
		ctrpDraftCSVExportJB.setSubmitOrgStudyId("Local Trial Identifier");//1 SUBMIT_ORG_STUDYID
		ctrpDraftCSVExportJB.setFkCodelstSubmissionType("Submission Type");//2 SUBMIT_ORG_TYPE(Sub Type Of Codelst Table)
		ctrpDraftCSVExportJB.setNciStudyId("NCI Trial Identifier");//3 NCI_STUDYID 
		ctrpDraftCSVExportJB.setSubmitOrgCtrpId("[Submitting Organization] Organization PO-ID");//4 SUBMIT_ORG_CTRPID
		ctrpDraftCSVExportJB.setSubmitOrgName("[Submitting Organization] Name");//5 SUBMIT_ORG_NAME
		//get Address Details with SUBMIT_ORG_FK_ADD
		ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().setAddPri("[Submitting Organization] Street Address");//6
		ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().setAddCity("[Submitting Organization] City");//7
		ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().setAddState("[Submitting Organization] State/Province");//8
		ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().setAddZip("[Submitting Organization] Zip/Postal code");//9
		ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().setAddCountry("[Submitting Organization] Country");//10
		ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().setAddEmail("[Submitting Organization] Email Address");//11
		ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().setAddPhone("[Submitting Organization] Phone");//12
		ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().setAddTty("[Submitting Organization] TTY");//13
		ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().setAddFax("[Submitting Organization] FAX");//14
		ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().setAddUrl("[Submitting Organization] URL");//15
		ctrpDraftCSVExportJB.setSubmitOrgType("[Submitting Organization] Organization Type");//16 SUBMIT_ORG_TYPE
		ctrpDraftCSVExportJB.setSubmitNCIDesignated("Is Submitting Organization a NCI Designated Cancer Center?"); //17 SUBMIT_NCI_DESIGNATED
		ctrpDraftCSVExportJB.setLeadOrgCtrpId("[Lead Organization] CTEP Organization PO-ID"); // 18 LEAD_ORG_CTRPID
		ctrpDraftCSVExportJB.setLeadOrgName("[Lead Organization] Name");// 19 LEAD_ORG_FK_SITE
		//For setting the values in AddressJB  get Data on the Basis Of id LEAD_ORG_FK_ADD
		ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddPri("[Lead Organization] Street Address");//20
		ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddCity("[Lead Organization] City");//21
		ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddState("[Lead Organization] State/Province");//22
		ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddZip("[Lead Organization] Zip/Postal code ");//23
		ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddCountry("[Lead Organization] Country");//24
		ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddEmail("[Lead Organization] Email Address");//25
		ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddPhone("[Lead Organization] Phone");//26
		ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddTty("[Lead Organization] TTY");//27
		ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddFax("[Lead Organization] FAX");//28
		ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddUrl("[Lead Organization] URL");//29
		ctrpDraftCSVExportJB.setLeadOrgType("[Lead Organization] Organization Type");//LEAD_ORG_TYPE 30
		/**Trial Identification */
		ctrpDraftCSVExportJB.setLeadOrgStudyId("Lead Organization Trial Identifier");//LEAD_ORG_STUDYID 31
		ctrpDraftCSVExportJB.setNctTrailIdentifier("NCT Trial Identifier"); //NCI_STUDYID  or SUBMIT_ORG_STUDYID  32
		/**Trial Details */
		ctrpDraftCSVExportJB.setTriailDetailsTitle("Title");//Study Title with fk_study get study title 33
		ctrpDraftCSVExportJB.setStudyType("Trial Type");//STUDY_TYPE 34
		ctrpDraftCSVExportJB.setStudyPurpose("Primary Purpose"); //STUDY_PURPOSE 35
		ctrpDraftCSVExportJB.setStudyPurposeOther("If Primary Purpose is 'Other' describe"); //STUDY_PURPOSE_OTHER 36
		ctrpDraftCSVExportJB.setFkCodelstPhase("Phase"); //37
		ctrpDraftCSVExportJB.setDraftPilot("Pilot Trial?"); //38
		//ctrpDraftCSVExportJB.getSitePrincipalInvestgatorAddressJB()
		ctrpDraftCSVExportJB.setPiCtrpId("[Site Principal Investigator] Person PO-ID");// PI_CTRPID 39
		//get User Details with the help of PI_FK_USER from ER_USER table
		ctrpDraftCSVExportJB.setPiFirstName("[Site Principal Investigator] First Name");//PiFirstName 40
		ctrpDraftCSVExportJB.setPiMiddleName("[Site Principal Investigator] Middle Name");//PiMiddleName 41
		ctrpDraftCSVExportJB.setPiLastName("[Site Principal Investigator] Last Name");//PiLastName 42
		/*
		ctrpDraftCSVExportJB.getPrincipalInvestgatorUserJB().setUserFirstName("[Site Principal Investigator] First Name");//PiFirstName 40
		ctrpDraftCSVExportJB.getPrincipalInvestgatorUserJB().setUserMidName("[Site Principal Investigator] Middle Name");//PiMiddleName 41
		ctrpDraftCSVExportJB.getPrincipalInvestgatorUserJB().setUserLastName("[Site Principal Investigator] Last Name");//PiLastName 42
		 */		
		//get Address details with the help of PI_FK_ADD
		ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddPri("[Site Principal Investigator] Street Address");//43
		ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddCity("[Site Principal Investigator] City");//44
		ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddState("[Site Principal Investigator] State/Province");//45
		ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddZip("[Site Principal Investigator] Zip/Postal code");//46
		ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddCountry("[Site Principal Investigator] Country");//47
		ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddEmail("[Site Principal Investigator] Email Address");//48
		ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddPhone("[Site Principal Investigator] Phone");//49
		ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddTty("[Site Principal Investigator] TTY");//50
		ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddFax("Site [Principal Investigator] FAX");//51
		ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddUrl("[Site Principal Investigator] URL");//52
		ctrpDraftCSVExportJB.setSumm4SponsorType("Summary 4 Funding Sponsor/Source Category");//SUMM4_SPONSOR_TYPE 53
		ctrpDraftCSVExportJB.setSumm4SponsorId("[Summary 4 Funding Sponsor/Source] Organization PO-ID");//SUMM4_SPONSOR_ID 54
		ctrpDraftCSVExportJB.setSumm4SponsorName("[Summary 4 Funding Sponsor/Source] Organization Name");//SUMM4_SPONSOR_NAME 55
		//GET Summary For Funding Information On the basis Of SUMM4_SPONSOR_FK_ADD from address table
		ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddPri("[Summary 4 Funding Sponsor/Source] Street Address");// 56
		ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddCity("[Summary 4 Funding Sponsor/Source] City ");//57
		ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddState("[Summary 4 Funding Sponsor/Source] State/Province");//58
		ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddZip("[Summary 4 Funding Sponsor/Source] Zip/Postal code");//59
		ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddCountry("[Summary 4 Funding Sponsor/Source ] Country");//60
		ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddEmail("[Summary 4 Funding Sponsor/Source ] Email Address");//61
		ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddPhone("[Summary 4 Funding Sponsor/Source ] Phone");//62
		ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddTty("[Summary 4 Funding Sponsor/Source ] TTY");//63
		ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddFax("[Summary 4 Funding Sponsor/Source ] FAX");//64
		ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddUrl("[Summary 4 Funding Sponsor/Source ] URL");//65
		ctrpDraftCSVExportJB.setSponsorProgramCode("[Submitting Site specific] Program Code");//SPONSOR_PROGRAM_CODE 66
		ctrpDraftCSVExportJB.setFkCodelstCtrpStudyStatus("Site Recruitment Status");//FK_CODELST_CTRP_STUDY_STATUS 67
		ctrpDraftCSVExportJB.setCtrpStudyStatusDate("Site Recruitment Status Date");//68
		ctrpDraftCSVExportJB.setCtrpStudyAccOpenDate("Date Opened for Accrual");//69
		ctrpDraftCSVExportJB.setCtrpStudyAccClosedDate("Date Closed for Accrual");//70
		ctrpDraftCSVExportJB.setSiteTargetAccrual("Site Target Accrual");//71
		ctrpDraftCSVExportJB.setFkCodelstDiseaseSite("Disease Name");//72
		ctrpDraftCSVExportJB.setFkCodelstInterventionType("Intervention Type");//73
		ctrpDraftCSVExportJB.setInterventionName("Intervention Name");//74
		//ctrpDraftCSVExportJB.getTrailUserJB FK_TRIAL_OWNER
		ctrpDraftCSVExportJB.getTrailUserJB().setUserFirstName("Trial Owner First Name");//75 
		ctrpDraftCSVExportJB.getTrailUserJB().setUserLastName("Trial Owner Last Name");//76
		//ctrpDraftCSVExportJB.getTrailUserJB().setUserLastName("Trial Owner Email Address");//77
		ctrpDraftCSVExportJB.getTrailAddressJB().setAddEmail("Trial Owner Email Address"); //77
		//Trail Document Section : FK_CTRP_DRAFT from ER_CTRP_DOCUMENTS table with primary key of ER_CTRP_DRAFT  (PK_CTRP_DRAFT)
		ctrpDraftCSVExportJB.setAbbreviatedTrialTemplate("Abbreviated Trial Template Name");//78
		ctrpDraftCSVExportJB.setOtherTrialRelatedDocument("Other Trial Related Document File Name");//79
		return ctrpDraftCSVExportJB;
	}
	/*Added for download CVS Section : Yogendra Pratap Singh*/
	public ArrayList<CtrpDraftCSVExportJB> retriveIndudtryCSVExportData(ArrayList<CtrpDraftCSVExportJB> exportList , ArrayList<CtrpDraftBean> aList){
		try {
			if(aList!=null && aList.size()>0){
				AddressAgentRObj addressAgentRObj = EJBUtil.getAddressAgentHome();
				UserAgentRObj  userAgentRObj =EJBUtil.getUserAgentHome();
				StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
				CodelstAgentRObj cCodelstAgentRObj = EJBUtil.getCodelstAgentHome();
				AddressBean submtgOrgAddressBean = null;
				AddressBean leadOrgAddressBean = null;
				AddressBean piAddressBean = null;
				AddressBean summ4SponsorAddressBean = null;
				AddressBean trailAddressBean = null;
				UserBean userBean = null;
				UserBean trailUserBean = null;
				StudyBean studyBean = null;
				CodelstBean codelstBean = null;
				for(CtrpDraftBean ctrpDraftBean : aList){
					CtrpDraftCSVExportJB ctrpDraftCSVExportJB = new CtrpDraftCSVExportJB();
					
					ctrpDraftCSVExportJB.setSubmitOrgStudyId(replaceNullValues(ctrpDraftBean.getSubmitOrgStudyId()));//1 SUBMIT_ORG_STUDYID
					codelstBean = cCodelstAgentRObj.getCodelstDetails( ctrpDraftBean.getFkCodelstSubmissionType());// code list data
					ctrpDraftCSVExportJB.setFkCodelstSubmissionType(replaceNullValues( codelstBean.getClstSubType() ));//2 SUBMIT_ORG_TYPE
					//Change For Bug# 8487 :Yogendra
					//ctrpDraftCSVExportJB.setNciStudyId(replaceNullValues(ctrpDraftBean.getSubmitOrgStudyId()));// 3 NCI_STUDYID 
					ctrpDraftCSVExportJB.setNciStudyId(replaceNullValues(ctrpDraftBean.getNciStudyId()));// 3 NCI_STUDYID 
					ctrpDraftCSVExportJB.setSubmitOrgCtrpId(replaceNullValues(ctrpDraftBean.getSubmitOrgCtrpId()));//4 SUBMIT_ORG_CTRPID
					SiteBean submitOrgSiteBean =null;
					if(ctrpDraftBean.getFkSiteSubmitOrg()!=null){
						SiteAgentRObj siteAgent = getSiteAgent();
						submitOrgSiteBean = siteAgent.getSiteDetails(ctrpDraftBean.getFkSiteSubmitOrg());
					}else{	submitOrgSiteBean = new SiteBean();	}
					ctrpDraftCSVExportJB.setSubmitOrgName(replaceNullValues(submitOrgSiteBean.getSiteName()));//5 SUBMIT_ORG_NAME
					//ctrpDraftCSVExportJB.setSubmitOrgName(replaceNullValues(ctrpDraftBean.getFkSiteSubmitOrg()));//5 SUBMIT_ORG_NAME
					//SUBMIT_ORG_FK_ADD. Identifies Submitting Organization Address. Foreign Key for ER_ADD
					if(null!=ctrpDraftBean.getFkAddSubmitOrg())
						submtgOrgAddressBean = addressAgentRObj.getAddressDetails(ctrpDraftBean.getFkAddSubmitOrg());
					else
						submtgOrgAddressBean = new AddressBean();
					ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().setAddPri(replaceNullValues(submtgOrgAddressBean.getAddPri()));//6
					ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().setAddCity(replaceNullValues(submtgOrgAddressBean.getAddCity()));//7
					ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().setAddState(replaceNullValues(submtgOrgAddressBean.getAddState()));//8
					ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().setAddZip(replaceNullValues(submtgOrgAddressBean.getAddZip()));//9
					ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().setAddCountry(replaceNullValues(submtgOrgAddressBean.getAddCountry()));//10
					ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().setAddEmail(replaceNullValues(submtgOrgAddressBean.getAddEmail()));//11
					ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().setAddPhone(replaceNullValues(submtgOrgAddressBean.getAddPhone()));//12
					ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().setAddTty(replaceNullValues(submtgOrgAddressBean.getAddTty()));//13
					ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().setAddFax(replaceNullValues(submtgOrgAddressBean.getAddFax()));//14
					ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().setAddUrl(replaceNullValues(submtgOrgAddressBean.getAddUrl()));//15
					String submitOrgTypeValue  = null;
					if(null!=ctrpDraftBean.getSubmitOrgType())
						submitOrgTypeValue = cCodelstAgentRObj.getCodeDescription( ctrpDraftBean.getSubmitOrgType() );
					ctrpDraftCSVExportJB.setSubmitOrgType(replaceNullValues(submitOrgTypeValue));//16 SUBMIT_ORG_TYPE
					int cancerDesignatedCenter = 0;
					if(null!=ctrpDraftBean.getSubmitNCIDesignated())
						cancerDesignatedCenter = ctrpDraftBean.getSubmitNCIDesignated() ;
					ctrpDraftCSVExportJB.setSubmitNCIDesignated(replaceNullValues(( cancerDesignatedCenter==1 ) ? "Yes" : "No" )); //17 SUBMIT_NCI_DESIGNATED
					ctrpDraftCSVExportJB.setLeadOrgCtrpId(replaceNullValues(ctrpDraftBean.getLeadOrgCtrpId())); // 18 LEAD_ORG_CTRPID
					SiteBean leadOrgNameSiteBean =null;
					if(ctrpDraftBean.getFkSiteLeadOrg()!=null){
						SiteAgentRObj siteAgent = getSiteAgent();
						leadOrgNameSiteBean = siteAgent.getSiteDetails(ctrpDraftBean.getFkSiteLeadOrg());
					}else{
						leadOrgNameSiteBean = new SiteBean();
					}
					ctrpDraftCSVExportJB.setLeadOrgName(replaceNullValues(leadOrgNameSiteBean.getSiteName()));//19
					//ctrpDraftCSVExportJB.setLeadOrgName(replaceNullValues(ctrpDraftBean.getLeadOrgName()));// 19 LEAD_ORG_FK_SITE
					
					//LEAD_ORG_FK_ADD.Identifies Lead Organization Address. Foreign Key for ER_ADD
					if(null!=ctrpDraftBean.getFkAddLeadOrg())
						leadOrgAddressBean = addressAgentRObj.getAddressDetails(ctrpDraftBean.getFkAddLeadOrg());
					else
						leadOrgAddressBean = new AddressBean();
					ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddPri(replaceNullValues(leadOrgAddressBean.getAddPri()));//20
					ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddCity(replaceNullValues(leadOrgAddressBean.getAddCity()));//21
					ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddState(replaceNullValues(leadOrgAddressBean.getAddState()));//22
					ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddZip(replaceNullValues(leadOrgAddressBean.getAddZip()));//23
					ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddCountry(replaceNullValues(leadOrgAddressBean.getAddCountry()));//24
					ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddEmail(replaceNullValues(leadOrgAddressBean.getAddEmail()));//25
					ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddPhone(replaceNullValues(leadOrgAddressBean.getAddPhone()));//26
					ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddTty(replaceNullValues(leadOrgAddressBean.getAddTty()));//27
					ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddFax(replaceNullValues(leadOrgAddressBean.getAddFax()));//28
					ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().setAddUrl(replaceNullValues(leadOrgAddressBean.getAddUrl()));//29
					String leadOrgType = null;
					if(null!=ctrpDraftBean.getLeadOrgType())
						leadOrgType = cCodelstAgentRObj.getCodeDescription( ctrpDraftBean.getLeadOrgType() );// code list data
					ctrpDraftCSVExportJB.setLeadOrgType(replaceNullValues(leadOrgType));// 30 LEAD_ORG_TYPE
					
					/**Trial Identification */
					ctrpDraftCSVExportJB.setLeadOrgStudyId(replaceNullValues(ctrpDraftBean.getLeadOrgStudyId()));// 31 LEAD_ORG_STUDYID
					//Change For Bug# 8487 :Yogendra
					//ctrpDraftCSVExportJB.setNctTrailIdentifier(replaceNullValues(ctrpDraftBean.getNciStudyId())); // 32 NCI_STUDYID
					ctrpDraftCSVExportJB.setNctTrailIdentifier(replaceNullValues(ctrpDraftBean.getNctNumber())); // 32 NCT_NUMBER
					/**Trial Details */
					if(null!=ctrpDraftBean.getFkStudy())
						studyBean = studyAgent.getStudyDetails(ctrpDraftBean.getFkStudy());
					else
						studyBean = new StudyBean();
					ctrpDraftCSVExportJB.setTriailDetailsTitle(replaceNullValues(studyBean.getStudyTitle()));// 33 Study Title with fk_study get study title
					
					//ctrpDraftCSVExportJB.setStudyType(replaceNullValues(ctrpDraftBean.getStudyType()));// 34 STUDY_TYPE
					//Identifies Study Type. Maps to Study research Type. 1-Interventional and 0-Observational
					String trailType = "";
					if(null!=ctrpDraftBean.getStudyType())
						trailType = replaceNullValues(ctrpDraftBean.getStudyType());
					ctrpDraftCSVExportJB.setStudyType(trailType.equalsIgnoreCase("1") ? "Interventional" : "Observational" );// 34 STUDY_TYPE
					
					String studyPurpose = null;
					if(null!=ctrpDraftBean.getStudyPurpose())
						studyPurpose = cCodelstAgentRObj.getCodeDescription( ctrpDraftBean.getStudyPurpose() );// code list data
					ctrpDraftCSVExportJB.setStudyPurpose(replaceNullValues(studyPurpose)); // 35 STUDY_PURPOSE
					
					
					ctrpDraftCSVExportJB.setStudyPurposeOther(replaceNullValues(ctrpDraftBean.getStudyPurposeOther())); // 36 STUDY_PURPOSE_OTHER
					
					//
					String fkCodeLstPhase = null;
					if(null!=ctrpDraftBean.getFkCodelstPhase())
						fkCodeLstPhase = cCodelstAgentRObj.getCodeDescription( ctrpDraftBean.getFkCodelstPhase() );// code list data
					ctrpDraftCSVExportJB.setFkCodelstPhase(replaceNullValues(fkCodeLstPhase));//37
					//Identifies Pilot study flag 1-Yes, 0-No
					int draftPilot =0;
					if(null!=ctrpDraftBean.getDraftPilot())
						draftPilot  = ctrpDraftBean.getDraftPilot();
					ctrpDraftCSVExportJB.setDraftPilot(replaceNullValues( ( draftPilot == 1 ) ? "Yes" : "No" ));//38
					
					//ctrpDraftCSVExportJB.getSitePrincipalInvestgatorAddressJB()
					ctrpDraftCSVExportJB.setPiCtrpId(replaceNullValues(ctrpDraftBean.getPiCtrpId()));// 39 PI_CTRPID
					ctrpDraftCSVExportJB.setPiFirstName(replaceNullValues(ctrpDraftBean.getPiFirstName())); //PiFirstName 40
					ctrpDraftCSVExportJB.setPiMiddleName(replaceNullValues("")); //PiMiddleName 41
					ctrpDraftCSVExportJB.setPiLastName(replaceNullValues(ctrpDraftBean.getPiLastName())); //PiLastName 42
					
					//get User Details with the help of PI_FK_USER from ER_USER table
					/*if(null!=ctrpDraftBean.getFkUserPI())
						userBean = userAgentRObj.getUserDetails(ctrpDraftBean.getFkUserPI());
					else
						userBean = new UserBean();
					ctrpDraftCSVExportJB.getPrincipalInvestgatorUserJB().setUserFirstName(replaceNullValues(userBean.getUserFirstName()));//PiFirstName 40
					ctrpDraftCSVExportJB.getPrincipalInvestgatorUserJB().setUserMidName(replaceNullValues(userBean.getUserMidName()));//PiMiddleName 41
					ctrpDraftCSVExportJB.getPrincipalInvestgatorUserJB().setUserLastName(replaceNullValues(userBean.getUserLastName()));//PiLastName 42
					*/
					
					//PI_FK_ADD.Identifies PI Address. Foreign Key for ER_ADD
					if(null!=ctrpDraftBean.getFkAddPi())
						piAddressBean = addressAgentRObj.getAddressDetails(ctrpDraftBean.getFkAddPi());
					else
						piAddressBean = new AddressBean();
					ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddPri(replaceNullValues(piAddressBean.getAddPri()));//43
					ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddCity(replaceNullValues(piAddressBean.getAddCity()));//44
					ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddState(replaceNullValues(piAddressBean.getAddState()));//45
					ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddZip(replaceNullValues(piAddressBean.getAddZip()));//46
					ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddCountry(replaceNullValues(piAddressBean.getAddCountry()));//47
					ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddEmail(replaceNullValues(piAddressBean.getAddEmail()));//48
					ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddPhone(replaceNullValues(piAddressBean.getAddPhone()));//49
					ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddTty(replaceNullValues(piAddressBean.getAddTty()));//50
					ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddFax(replaceNullValues(piAddressBean.getAddFax()));//51
					ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().setAddUrl(replaceNullValues(piAddressBean.getAddUrl()));//52
					String summ4SponsorType = null;
					if(null!=ctrpDraftBean.getSumm4SponsorType())
						summ4SponsorType = cCodelstAgentRObj.getCodeDescription( ctrpDraftBean.getSumm4SponsorType() );// code list data
					ctrpDraftCSVExportJB.setSumm4SponsorType(replaceNullValues(summ4SponsorType));// 53 SUMM4_SPONSOR_TYPE
					
					ctrpDraftCSVExportJB.setSumm4SponsorId(replaceNullValues(ctrpDraftBean.getSumm4SponsorId()));// 54 SUMM4_SPONSOR_ID
					SiteBean siteSumm4SponsorSiteBean =null;
					if(ctrpDraftBean.getFkSiteSumm4Sponsor()!=null){
						SiteAgentRObj siteAgent = getSiteAgent();
						siteSumm4SponsorSiteBean = siteAgent.getSiteDetails(ctrpDraftBean.getFkSiteSumm4Sponsor());
					}else{
						siteSumm4SponsorSiteBean = new SiteBean();
					}
					ctrpDraftCSVExportJB.setSumm4SponsorName(replaceNullValues(siteSumm4SponsorSiteBean.getSiteName()));//55
					//ctrpDraftCSVExportJB.setSumm4SponsorName(replaceNullValues(ctrpDraftBean.getFkSiteSumm4Sponsor()));//55 SUMM4_SPONSOR_NAME
					
					//GET Summary For Funding Information On the basis Of SUMM4_SPONSOR_FK_ADD from address table
					if(null!=ctrpDraftBean.getfkAddSumm4Sponsor())
						summ4SponsorAddressBean = addressAgentRObj.getAddressDetails(ctrpDraftBean.getfkAddSumm4Sponsor());
					else
						summ4SponsorAddressBean = new AddressBean();
					ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddPri(replaceNullValues(summ4SponsorAddressBean.getAddPri()));//56
					ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddCity(replaceNullValues(summ4SponsorAddressBean.getAddCity()));//57
					ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddState(replaceNullValues(summ4SponsorAddressBean.getAddState()));//58
					ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddZip(replaceNullValues(summ4SponsorAddressBean.getAddZip()));//59
					ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddCountry(replaceNullValues(summ4SponsorAddressBean.getAddCountry()));//60
					ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddEmail(replaceNullValues(summ4SponsorAddressBean.getAddEmail()));//61
					ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddPhone(replaceNullValues(summ4SponsorAddressBean.getAddPhone()));//62
					ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddTty(replaceNullValues(summ4SponsorAddressBean.getAddTty()));//63
					ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddFax(replaceNullValues(summ4SponsorAddressBean.getAddFax()));//64
					ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().setAddUrl(replaceNullValues(summ4SponsorAddressBean.getAddUrl()));//65
					
					ctrpDraftCSVExportJB.setSponsorProgramCode(replaceNullValues(ctrpDraftBean.getSponsorProgramCode()));// 66 SPONSOR_PROGRAM_CODE
					String siteRecuritmentStatus = null;
					if(null!=ctrpDraftBean.getFkCodelstCtrpStudyStatus())
						siteRecuritmentStatus = cCodelstAgentRObj.getCodeDescription( ctrpDraftBean.getFkCodelstCtrpStudyStatus() );// code list data
					ctrpDraftCSVExportJB.setFkCodelstCtrpStudyStatus(replaceNullValues( siteRecuritmentStatus ));// 67 FK_CODELST_CTRP_STUDY_STATUS
					ctrpDraftCSVExportJB.setCtrpStudyStatusDate(formatDateString( replaceNullValues(ctrpDraftBean.getCtrpStudyStatusDate()) ));//68
					ctrpDraftCSVExportJB.setCtrpStudyAccOpenDate(formatDateString( replaceNullValues(ctrpDraftBean.getCtrpStudyAccOpenDate()) ));//69
					
					
					ctrpDraftCSVExportJB.setCtrpStudyAccClosedDate(formatDateString( replaceNullValues(ctrpDraftBean.getCtrpStudyAccClosedDate()) ));//70
					
					ctrpDraftCSVExportJB.setSiteTargetAccrual(replaceNullValues(ctrpDraftBean.getSiteTargetAccrual()));//71
					StringBuilder diseaseSiteValue = new StringBuilder();
					String[] diseaseSiteArr = null;
					if(ctrpDraftBean.getFkCodelstDiseaseSite()!=null){
						String[] diseaseSiteArr1 = {ctrpDraftBean.getFkCodelstDiseaseSite()}; 
						if(ctrpDraftBean.getFkCodelstDiseaseSite().indexOf(",") != -1)
							diseaseSiteArr = ctrpDraftBean.getFkCodelstDiseaseSite().split(",");
						else
							diseaseSiteArr = diseaseSiteArr1;
						int i=1;
						for(String diseaseSiteTemp:diseaseSiteArr){
							if (i != diseaseSiteArr.length)
								diseaseSiteValue = diseaseSiteValue.append(cCodelstAgentRObj.getCodeDescription( Integer.parseInt(diseaseSiteTemp) )).append("~");
							else
								diseaseSiteValue = diseaseSiteValue.append(cCodelstAgentRObj.getCodeDescription( Integer.parseInt(diseaseSiteTemp) ));
							i++;
						}
					}
					ctrpDraftCSVExportJB.setFkCodelstDiseaseSite(replaceNullValues(diseaseSiteValue.toString() ));//72
					String interventionType = null;
					if(null!=ctrpDraftBean.getFkCodelstInterventionType())
						interventionType = cCodelstAgentRObj.getCodeDescription( ctrpDraftBean.getFkCodelstInterventionType());
					ctrpDraftCSVExportJB.setFkCodelstInterventionType(replaceNullValues( interventionType ));//73
					ctrpDraftCSVExportJB.setInterventionName(replaceNullValues(ctrpDraftBean.getInterventionName()));//74
					
					//ctrpDraftCSVExportJB.getTrailUserJB FK_TRIAL_OWNER
					if(null!=ctrpDraftBean.getFkTrialOwner())
						trailUserBean = userAgentRObj.getUserDetails(ctrpDraftBean.getFkTrialOwner());
					else
						trailUserBean = new UserBean();
					ctrpDraftCSVExportJB.getTrailUserJB().setUserFirstName(replaceNullValues(trailUserBean.getUserFirstName()));//75 
					ctrpDraftCSVExportJB.getTrailUserJB().setUserLastName(replaceNullValues(trailUserBean.getUserLastName()));//76
					
					int fk_per_add = 0;
					if(trailUserBean.getUserPerAddressId()!=null){
						fk_per_add = Integer.parseInt(trailUserBean.getUserPerAddressId());
						trailAddressBean = addressAgentRObj.getAddressDetails(fk_per_add);
					}else{
						trailAddressBean = new AddressBean();}
					ctrpDraftCSVExportJB.getTrailAddressJB().setAddEmail(replaceNullValues(trailAddressBean.getAddEmail())); //77
					
					//ctrpDraftCSVExportJB.(replaceNullValues(trailUserBean.getUserLastName()));//77
					//Trail Document Section : FK_CTRP_DRAFT from ER_CTRP_DOCUMENTS table with primary key of ER_CTRP_DRAFT (PK_CTRP_DRAFT)
					CTRPDraftDocDao ctrpDraftDao= null;
					CTRPDraftDocJB ctrpDraftDocJB = new CTRPDraftDocJB();
					ctrpDraftDao=ctrpDraftDocJB.getCTRPDraftDocDetails(ctrpDraftBean.getId());
					//ArrayList< Integer> ctrpDrafts=ctrpDraftDao.getCtrpDrafts();
					//Changes By Yogendra For : 22495-CTRP Documents File
					ctrpDraftCSVExportJB.setId(ctrpDraftBean.getId().toString());
					List<AppendixBean> appenxBeanList= new ArrayList<AppendixBean>();
					List<Integer> appenxBeanIdList= new ArrayList<Integer>();
					if(ctrpDraftDao.getCtrpDocids().size() >0){
						CodeDao codeDao= new CodeDao();
						ArrayList<Integer>  ctrpDraftDocTypes = ctrpDraftDao.getCtrpDocTypes();
						ArrayList<Integer>  ctrpDraftStudyAppxs = ctrpDraftDao.getStudyAppendixs();
						ArrayList<Integer>  trialCtrpDraftPKs = ctrpDraftDao.getCtrpDocids();
						for(int i=0; i< ctrpDraftDao.getCtrpDocids().size();i++){	
							try{
								AppendixJB appnxJB= new AppendixJB();
								Integer ctrpStudyAppndx=0;
								Integer trialCtrpDraftPK=0;
								ctrpStudyAppndx=ctrpDraftStudyAppxs.get(i);
								trialCtrpDraftPK=trialCtrpDraftPKs.get(i);
								appnxJB.setId(ctrpStudyAppndx);
								AppendixBean appenxBean=appnxJB.getAppendixDetails();
								//Changes By Yogendra For : 22495-CTRP Documents File
								appenxBeanList.add(appenxBean);
								//Modified for Bug# 8471:Yogendra
								String studyaddDixUtl = appenxBean.getAppendixUrl_File();
								String draftDocType=codeDao.getCodeSubtype(ctrpDraftDocTypes.get(i));
								if(draftDocType.equalsIgnoreCase(CtrpDraftJB.CDLST_ABBRTRIALTEMPL)){ 
									ctrpDraftCSVExportJB.setAbbreviatedTrialTemplate(replaceNullValues(studyaddDixUtl));//78
								}if(draftDocType.equalsIgnoreCase(CtrpDraftJB.CDLST_OTHER)){
									ctrpDraftCSVExportJB.setOtherTrialRelatedDocument(replaceNullValues(studyaddDixUtl));//79
								}
								ctrpDraftCSVExportJB.setDocumentInfoList(appenxBeanList);
							}catch (Exception e) {}
						}
					}
					exportList.add(ctrpDraftCSVExportJB);
				}
			}
		}catch(Exception e){
			Rlog.fatal("ctrp", "Exception in CtrpDraftDocJBHelper.retriveIndudtryCSVExportData "+e);
			e.printStackTrace();

		}
		return exportList;
	}
	/*Added for download CVS Section : Yogendra Pratap Singh*/
	public FileWriter generateIndustryCSVDcoument(ArrayList<CtrpDraftCSVExportJB> exportList,FileWriter fw) throws IOException {
		try{
			for(CtrpDraftCSVExportJB ctrpDraftCSVExportJB:exportList ){
				//fw.append(ctrpDraftCSVExportJB.getId()).append(",");//1
				fw.append(ctrpDraftCSVExportJB.getSubmitOrgStudyId()).append(",");//1 SUBMIT_ORG_STUDYID
				fw.append(ctrpDraftCSVExportJB.getFkCodelstSubmissionType()).append(",");//2 SUBMIT_ORG_TYPE
				fw.append(ctrpDraftCSVExportJB.getNciStudyId()).append(",");// 3 NCI_STUDYID
				fw.append(ctrpDraftCSVExportJB.getSubmitOrgCtrpId()).append(",");//4 SUBMIT_ORG_CTRPID
				fw.append(ctrpDraftCSVExportJB.getSubmitOrgName()).append(",");//5 SUBMIT_ORG_NAME
				//get Address Details with SUBMIT_ORG_FK_ADD
				fw.append(ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().getAddPri()).append(",");//6
				fw.append(ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().getAddCity()).append(",");//7
				fw.append(ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().getAddState()).append(",");//8
				fw.append(ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().getAddZip()).append(",");//9
				fw.append(ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().getAddCountry()).append(",");//10
				fw.append(ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().getAddEmail()).append(",");//11
				fw.append(ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().getAddPhone()).append(",");//12
				fw.append(ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().getAddTty()).append(",");//13
				fw.append(ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().getAddFax()).append(",");//14
				fw.append(ctrpDraftCSVExportJB.getSubmittingOrganizationAddressJB().getAddUrl()).append(",");//15
				fw.append(ctrpDraftCSVExportJB.getSubmitOrgType()).append(",");//16 SUBMIT_ORG_TYPE
				//fw.append(ctrpDraftCSVExportJB.getLeadOrgType("[Submitting Organization] Organization Type")).append(",");//16 SUBMIT_ORG_TYPE
				fw.append(ctrpDraftCSVExportJB.getSubmitNCIDesignated()).append(","); //17 SUBMIT_NCI_DESIGNATED
				fw.append(ctrpDraftCSVExportJB.getLeadOrgCtrpId()).append(","); // 18 LEAD_ORG_CTRPID
				fw.append(ctrpDraftCSVExportJB.getLeadOrgName()).append(",");// 19 LEAD_ORG_NAME
				//fw.append(ctrpDraftCSVExportJB.getOrgazationSiteName().setSiteName("[Lead Organization] Name")).append(",");
				//For setting the values in AddressJB  get Data on the Basis Of id LEAD_ORG_FK_ADD
				fw.append(ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().getAddPri()).append(",");//20
				fw.append(ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().getAddCity()).append(",");//21
				fw.append(ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().getAddState()).append(",");//22
				fw.append(ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().getAddZip()).append(",");//23
				fw.append(ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().getAddCountry()).append(",");//24
				fw.append(ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().getAddEmail()).append(",");//25
				fw.append(ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().getAddPhone()).append(",");//26
				fw.append(ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().getAddTty()).append(",");//27
				fw.append(ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().getAddFax()).append(",");//28
				fw.append(ctrpDraftCSVExportJB.getLeadOrganizationAddressJB().getAddUrl()).append(",");//29
				fw.append(ctrpDraftCSVExportJB.getLeadOrgType()).append(",");//LEAD_ORG_TYPE 30
				/**Trial Identification */
				fw.append(ctrpDraftCSVExportJB.getLeadOrgStudyId()).append(",");// 31 LEAD_ORG_STUDYID
				fw.append(ctrpDraftCSVExportJB.getNctTrailIdentifier()).append(","); // 32 NCI_STUDYID  or SUBMIT_ORG_STUDYID 
				/**Trial Details */
				fw.append(ctrpDraftCSVExportJB.getTriailDetailsTitle()).append(",");// 33 Study Title with fk_study get study title
				fw.append(ctrpDraftCSVExportJB.getStudyType()).append(",");// 34 STUDY_TYPE
				fw.append(ctrpDraftCSVExportJB.getStudyPurpose()).append(","); // 35 STUDY_PURPOSE
				fw.append(ctrpDraftCSVExportJB.getStudyPurposeOther()).append(","); // 36 STUDY_PURPOSE_OTHER
				fw.append(ctrpDraftCSVExportJB.getFkCodelstPhase()).append(",");// 37
				fw.append(ctrpDraftCSVExportJB.getDraftPilot()).append(","); //38
				//fw.append(ctrpDraftCSVExportJB.getSitePrincipalInvestgatorAddressJB()
				fw.append(ctrpDraftCSVExportJB.getPiCtrpId()).append(",");// 39 PI_CTRPID
				//get User Details with the help of PI_FK_USER from ER_USER table
				fw.append(ctrpDraftCSVExportJB.getPiFirstName()).append(",");//40
				fw.append(ctrpDraftCSVExportJB.getPiMiddleName()).append(",");//41
				fw.append(ctrpDraftCSVExportJB.getPiLastName()).append(",");//PiLastName 42
				//get Address details with the help of PI_FK_ADD
				fw.append(ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().getAddPri()).append(",");//43
				fw.append(ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().getAddCity()).append(",");//44
				fw.append(ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().getAddState()).append(",");//45
				fw.append(ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().getAddZip()).append(",");//46
				fw.append(ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().getAddCountry()).append(",");//47
				fw.append(ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().getAddEmail()).append(",");//48
				fw.append(ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().getAddPhone()).append(",");//49
				fw.append(ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().getAddTty()).append(",");//50
				fw.append(ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().getAddFax()).append(",");//51
				fw.append(ctrpDraftCSVExportJB.getPrincipalInvestgatorAddressJB().getAddUrl()).append(",");//52
				fw.append(ctrpDraftCSVExportJB.getSumm4SponsorType()).append(",");// 53 SUMM4_SPONSOR_TYPE
				fw.append(ctrpDraftCSVExportJB.getSumm4SponsorId()).append(",");// 54 SUMM4_SPONSOR_ID
				fw.append(ctrpDraftCSVExportJB.getSumm4SponsorName()).append(",");//55 SUMM4_SPONSOR_NAME
				//GET Summary For Funding Information On the basis Of SUMM4_SPONSOR_FK_ADD from address table
				fw.append(ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().getAddPri()).append(",");//56
				fw.append(ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().getAddCity()).append(",");//57
				fw.append(ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().getAddState()).append(",");//58
				fw.append(ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().getAddZip()).append(",");//59
				fw.append(ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().getAddCountry()).append(",");//60
				fw.append(ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().getAddEmail()).append(",");//61
				fw.append(ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().getAddPhone()).append(",");//62
				fw.append(ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().getAddTty()).append(",");//63
				fw.append(ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().getAddFax()).append(",");//64
				fw.append(ctrpDraftCSVExportJB.getSumm4SponsorAddressJB().getAddUrl()).append(",");//65
				fw.append(ctrpDraftCSVExportJB.getSponsorProgramCode()).append(",");// 66 SPONSOR_PROGRAM_CODE
				fw.append(ctrpDraftCSVExportJB.getFkCodelstCtrpStudyStatus()).append(",");// 67 FK_CODELST_CTRP_STUDY_STATUS
				fw.append(ctrpDraftCSVExportJB.getCtrpStudyStatusDate()).append(",");//68
				fw.append(ctrpDraftCSVExportJB.getCtrpStudyAccOpenDate()).append(",");//69
				fw.append(ctrpDraftCSVExportJB.getCtrpStudyAccClosedDate()).append(",");//70
				fw.append(ctrpDraftCSVExportJB.getSiteTargetAccrual()).append(",");//71
				fw.append(ctrpDraftCSVExportJB.getFkCodelstDiseaseSite()).append(",");//72
				fw.append(ctrpDraftCSVExportJB.getFkCodelstInterventionType()).append(",");//73
				fw.append(ctrpDraftCSVExportJB.getInterventionName()).append(",");//74
				//fw.append(ctrpDraftCSVExportJB.getTrailUserJB FK_TRIAL_OWNER
				fw.append(ctrpDraftCSVExportJB.getTrailUserJB().getUserFirstName()).append(",");// 75
				fw.append(ctrpDraftCSVExportJB.getTrailUserJB().getUserLastName()).append(",");//76
				//fw.append(ctrpDraftCSVExportJB.getTrailUserJB().getUserLastName());//77
				fw.append(ctrpDraftCSVExportJB.getTrailAddressJB().getAddEmail()).append(","); //77
				fw.append(replaceNullValues(ctrpDraftCSVExportJB.getAbbreviatedTrialTemplate())).append(",");//79
				fw.append(replaceNullValues(ctrpDraftCSVExportJB.getOtherTrialRelatedDocument()));//78

				fw.write(LINE_SEPRATOR);
			}
		}catch(Exception ex){
			Rlog.fatal("ctrp", "Exception in CtrpDraftDocJBHelper.generateIndustryCSVDcoument"+ex);
			ex.printStackTrace();
		}
		return fw;
	}
	/*Added for download CVS Section : Yogendra Pratap Singh*/
	public FileWriter getFileWriterForCSVExport(int nonIndustrialFlag) {
		FileWriter fw = null;
		String tempFilePath = null;
		String fileName = null;
		File file = null;
		try{
		if(nonIndustrialFlag==1)
			fileName = COMPLETE_BATCH_ULOAD;	
		else
			fileName = ABBREVIATED_BATCH_ULOAD;
		
		//Changes By Yogendra For : 22495-CTRP Documents File
		if (com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD==null) com.aithent.file.uploadDownload.Configuration.readSettings(SCH);
		com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(
        		com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD
                        + "fileUploadDownload.xml", "study");
		file = new File(com.aithent.file.uploadDownload.Configuration.DOWNLOADFOLDER + "/" + fileName);
		if (file.exists())
			file.delete();
		file.createNewFile();
		fw = new FileWriter(file);
		}catch(Exception e){
			Rlog.fatal("ctrp", "Exception in CtrpDraftDocJBHelper.getFileWriterForCSVExport"+e);
			e.printStackTrace();
		}
		return fw;
	}
	/*Added for download CVS Section : Yogendra Pratap Singh*/
	public String replaceNullValues(Object value){
		String replaceValue = (value ==null) ? "" : (value.toString());
		if(replaceValue==null)
			replaceValue="";
		if(replaceValue.equalsIgnoreCase("null"))
			replaceValue="";
		return replaceSpecialValues(replaceValue);
	}
	/*Added for download CVS Section : Yogendra Pratap Singh*/
	public String formatDateString(String dateString){
		StringBuilder strDate = new StringBuilder();
		if(dateString!=null && dateString!=""){
			String[] dateTempArr = dateString.split(" ");
			String[] strDateArr = dateTempArr[0].split("-");
			strDate  = strDate.append(strDateArr[1]).append("/").append(strDateArr[2]).append("/").append(strDateArr[0]);
			//strDate = Month/Date/Year(MM/dd/yyyy)
		}
		return strDate.toString();
	}
	/*Added for download CVS Section : Yogendra Pratap Singh*/
	public void openCSVDoc(HttpServletResponse response ,String zipFileName)throws IOException{
		InputStream in=null;
		OutputStream output=null;
		String fileName =zipFileName;
		String tempFilePath = null;
		File file = null;
		try{
			//Changes By Yogendra For : 22495-CTRP Documents File
			file = new File(zipFileName);
			if (com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD==null) com.aithent.file.uploadDownload.Configuration.readSettings("sch");
			com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(
            		com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD
                            + "fileUploadDownload.xml", "study");

			tempFilePath = EJBUtil.getActualPath(com.aithent.file.uploadDownload.Configuration.DOWNLOADFOLDER, zipFileName);
			file = new File(tempFilePath );
			response.reset();
			response.resetBuffer();
				response.setContentType("application/zip");
				response.setHeader(CONTENT_DISPOSITION,"attachment;filename="+fileName);
			 in = new FileInputStream( file );
			 output = response.getOutputStream();
				int bufread = 0;
				while ((bufread = in.read()) != -1) {
					output.write(bufread);
				}
			} catch (Exception e) {
				Rlog.fatal("ctrp", "Exception in CtrpDraftDocJBHelper.openCSVDoc when open the ZIP document :"+fileName+" Exception is :"+e);
				e.printStackTrace();
			} finally {
				output.close();
				in.close();
				response.flushBuffer();
			}
		}
	/*For Bug# to replace comma , with other values.*/
	public String replaceSpecialValues(String replaceValue){
		Pattern pattern = Pattern.compile(REGEX_COMMA);
	    // get a matcher object
	    Matcher matcher = pattern.matcher(replaceValue);
	    if(matcher.find())
	    	replaceValue = matcher.replaceAll(REPLACE_STRING);/*YPS :Bug#8847:14 MAR 2012*/
	    if(replaceValue.indexOf("\r\n") != -1 )
	    	replaceValue= replaceValue.replaceAll("\\r\\n", " ");
		return replaceValue;
	}
	//Added for Bug#8464 | Date: 20 FEB 2012 |By :YPS
	public String findSelectedDtaftIds(String draftIds) {
		String seletedDraftIds = new LinkedHashSet<String>(Arrays.asList(draftIds.split(","))).toString().replaceAll(REPLACE_DUPLICATES, "");
		return seletedDraftIds;
	}
	
	//22472
	public void CopyCtrpDraft(Integer draftId) {
		retrieveCtrpDraft(draftId);
	}
}
