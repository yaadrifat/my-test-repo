package com.velos.eres.ctrp.web;

public class PatientRacesAccrual {
	
	private String patientRaces;
	private String studyIdentifier;
	private String studySubjectIdentifier;
	private String race;
	private String patientCount;

	/**
	 * @return the patientRaces
	 */
	public String getPatientRaces() {
		return patientRaces;
	}

	/**
	 * @param patientRaces
	 *            the patientRaces to set
	 */
	public void setPatientRaces(String patientRaces) {
		this.patientRaces = patientRaces;
	}

	/**
	 * @return the studyIdentifier
	 */
	public String getStudyIdentifier() {
		return studyIdentifier;
	}

	/**
	 * @param studyIdentifier
	 *            the studyIdentifier to set
	 */
	public void setStudyIdentifier(String studyIdentifier) {
		this.studyIdentifier = studyIdentifier;
	}

	/**
	 * @return the studySubjectIdentifier
	 */
	public String getStudySubjectIdentifier() {
		return studySubjectIdentifier;
	}

	/**
	 * @param studySubjectIdentifier
	 *            the studySubjectIdentifier to set
	 */
	public void setStudySubjectIdentifier(String studySubjectIdentifier) {
		this.studySubjectIdentifier = studySubjectIdentifier;
	}

	/**
	 * @return the race
	 */
	public String getRace() {
		return race;
	}

	/**
	 * @param race
	 *            the race to set
	 */
	public void setRace(String race) {
		this.race = race;
	}

	/**
	 * @return the patientCount
	 */
	public String getPatientCount() {
		return patientCount;
	}

	/**
	 * @param patientCount the patientCount to set
	 */
	public void setPatientCount(String patientCount) {
		this.patientCount = patientCount;
	}

}
