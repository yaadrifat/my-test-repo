package com.velos.eres.ctrp.service;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.common.CtrpDraftDao;
import com.velos.eres.business.common.StudyDao;
import com.velos.eres.ctrp.business.CtrpDraftBean;
import com.velos.eres.ctrp.business.CtrpDraftNotificationBean;
import com.velos.eres.ctrp.business.CtrpValidationDao;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;

@Stateless
@Remote( { CtrpDraftAgent.class } )
public class CtrpDraftAgentBean implements CtrpDraftAgent {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

	public CtrpDraftBean getCtrpDraftBean(Integer id) {
		if (id == null || id == 0) { return null; }
		CtrpDraftBean ctrpDraftBean = null;
		try {
			ctrpDraftBean = (CtrpDraftBean) em.find(CtrpDraftBean.class, new Integer(id));
		} catch (Exception e) {
			Rlog.fatal("ctrp", "Exception in CtrpDraftAgentBean.getCtrpDraftBean: "+e);
		}
		return ctrpDraftBean;
	}

	public Integer createCtrpDraft(CtrpDraftBean ctrpDraftBean) {
		Integer output = 0;
		try {
			ctrpDraftBean.setDeletedFlag(0); // New draft cannot have deleted_flag = 1
			em.persist(ctrpDraftBean);
			output = ctrpDraftBean.getId();
		} catch (Exception e) {
			Rlog.fatal("ctrp", "Exception in CtrpDraftAgentBean.createCtrpDraft: "+e);
			output = -1;
		}
		return output;
	}
	/**
	 * Added by Parminder Singh for CTRP-22473
	 * This method is used for persist data in Table
	 * 
	 */
	public Integer createCtrpDraftNotification(CtrpDraftNotificationBean ctrpDraftNotificationBean){
		Integer output = 0;
		try {

            try {
            	Query query = em.createNamedQuery("findCtrpDraftNotificationByUserId");
    	        query.setParameter("fk_codelst_studystat",ctrpDraftNotificationBean.getFkCodelstStudyStatus());
    	        query.setParameter("fk_user", ctrpDraftNotificationBean.getFkUser());
    	        ArrayList list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    // User With This status already record exists
                    Rlog.debug("Ctrp",
                                    "CtrpDraftAgentBean.createCtrpDraftNotification :User With This status already record exists");
                    return -3;
                }
            } catch (Exception ex) {
                Rlog.fatal("Ctrp", "Exception thrown:" + ex);
                //found = 0; // New record
            }
			em.persist(ctrpDraftNotificationBean);
			output = ctrpDraftNotificationBean.getId();
		} catch (Exception e) {
			Rlog.fatal("ctrp", "Exception in CtrpDraftNotificationBean.createCtrpDraftNotification: "+e);
			output = -1;
		}
		return output;
	}
	/**
	 * Added by Parminder Singh for CTRP-22473
	 * This method is used for delete data from Table
	 * 
	 */
	public int deleteDraftNotiUser(Integer statusId, String userId)
    {
    	int ret = 0;

    	try {
             CtrpDraftDao ctrpDao=new CtrpDraftDao();
             ret = ctrpDao.deleteDraftNotiUser(statusId,userId);

            return ret;
        } catch (Exception e) {
            Rlog.fatal("ctrp","Exception In delete user in CtrpDraftNotificationBean"
                            + e);
            return -1;
        }


    }

	public Integer updateCtrpDraft(CtrpDraftBean ctrpDraftBean) {
		Integer output = 0;
		if (ctrpDraftBean.getId() < 1) { return output; }
		try {
			CtrpDraftBean originalBean = (CtrpDraftBean) em.find(CtrpDraftBean.class, ctrpDraftBean.getId());
			ctrpDraftBean.setCreator(originalBean.getCreator()); // Keep the original
			if (ctrpDraftBean.getIpAdd() == null) {
				ctrpDraftBean.setIpAdd(originalBean.getIpAdd()); // Keep the original if new is null
			}
			if (ctrpDraftBean.getDeletedFlag() == null) { ctrpDraftBean.setDeletedFlag(0); }
			em.merge(ctrpDraftBean);
			output = ctrpDraftBean.getId();
		} catch (Exception e) {
			Rlog.fatal("ctrp", "Exception in CtrpDraftAgentBean.updateCtrpDraft: "+e);
			output = -1;
		}
		return output;
	}

	public CtrpValidationDao runCtrpDraftValidation(Integer ctrpDraftId) {
		Integer output = 0;
		CtrpValidationDao ctrpValidationDao = new CtrpValidationDao();
		
		//ctrpValidationDao.setDraftId(ctrpDraftId);
		try {
			ctrpValidationDao.setCtrpDraftId(ctrpDraftId);
			ctrpValidationDao.runCtrpDraftValidation();
			//output = ctrpDraftBean.getId();
		} catch (Exception e) {
			Rlog.fatal("ctrp", "Exception in CtrpDraftAgentBean.runCtrpDraftValidation: "+e);
			output = -1;
		}
		//return output;
		
		return ctrpValidationDao;
	}
	
	 public CtrpDraftBean findByDraftId(Integer ctrpDraftId) {
        Query query = em.createNamedQuery("findCtrpDraftByDraftId");
        query.setParameter("draftId", ctrpDraftId.intValue());
        ArrayList list = (ArrayList) query.getResultList();
        if (list == null || list.size() == 0) { return null; }
        return (CtrpDraftBean) list.get(0);
    }

	 public ArrayList findDraftsByStudyIds(Integer accId, String studyIds) {
		Query query = em.createNamedQuery("findCtrpDraftsByStudyIds");
		query.setParameter("fk_account", accId);
		query.setParameter("fkStudys", studyIds);
		ArrayList list = (ArrayList) query.getResultList();
		if (list == null || list.size() == 0) { return null; }
		return list;
	}
	 
	public int unmarkStudyCtrpReportable(String studyIds) {
		int ret=0;
		try {
	    
            Rlog.debug("studyDraft",
                    "In unmarkStudyCtrpReportable in CtrpDraftAgentBean line number 0");
            CtrpDraftDao ctrpDraftDao = new CtrpDraftDao();
            Rlog.debug("studyDraft",
                    "In unmarkStudyCtrpReportable in CtrpDraftAgentBean line number 1");
            ret= ctrpDraftDao.unmarkStudyCtrpReportable(studyIds);
            return ret;

        } catch (Exception e) {
            Rlog.fatal("studyDraft",
                    "Exception In unmarkStudyCtrpReportable in CtrpDraftAgentBean " + e);
            return ret;
        }
	}
	public int deleteStudyDrafts(String studyIds,String draftIds) {
		int ret=0;
		try {	
			
			Rlog.debug("studyDraft",
			"In deleteStudyDrafts in CtrpDraftAgentBean line number 0");
			CtrpDraftDao ctrpDraftDao = new CtrpDraftDao();
			Rlog.debug("studyDraft",
			"In deleteStudyDrafts in CtrpDraftAgentBean line number 1");
			ret= ctrpDraftDao.deleteStudyDrafts(studyIds,draftIds);
			return ret;
			
		} catch (Exception e) {
			Rlog.fatal("studyDraft",
					"Exception In deleteStudyDrafts in CtrpDraftAgentBean " + e);
			return ret;
		}
	}
	public int deleteStudyDrafts(String studyIds,String draftIds,Hashtable<String, String> args) {  //Modified for Bug#8029:Akshi
		int ret=0;
		String condition="";
		try {
			AuditBean audit=null; //Audit Bean for AppDelete
			String pkVal = "";
			String ridVal = "";
            String currdate =DateUtil.getCurrentDate(); /*gets the current date from DateUtil*/
            String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String appModule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the App Module Name from the Hashtable*/
            
        		condition = condition + studyIds;  
        	
        	condition = "FK_STUDY IN ("+condition.substring(1);

        	Hashtable<String, ArrayList<String>> getRowValues = AuditUtils.getRowValues("ER_CTRP_DRAFT",condition,"eres");/*Fetches the RID/PK_VALUE*/
        	ArrayList<String> rowPK  = (ArrayList<String>)getRowValues.get(AuditUtils.ROW_PK_KEY);
    		ArrayList<String> rowRID  = (ArrayList<String>)getRowValues.get(AuditUtils.ROW_RID_KEY);
    		//String getRidValue= AuditUtils.getRidValues("ER_PATPROT","PK_PATPROT="+condition);/*Fetches the RID/PK_VALUE*/ 
            for(int i=0; i<rowPK.size(); i++)
			{
    			pkVal = rowPK.get(i);
    			ridVal = rowRID.get(i);
    			audit = new AuditBean("ER_CTRP_DRAFT",pkVal,ridVal,
        			userID,currdate,appModule,ipAdd,reason);
    			em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
			}
			Rlog.debug("studyDraft",
			"In deleteStudyDrafts in CtrpDraftAgentBean line number 0");
			CtrpDraftDao ctrpDraftDao = new CtrpDraftDao();
			Rlog.debug("studyDraft",
			"In deleteStudyDrafts in CtrpDraftAgentBean line number 1");
			ret= ctrpDraftDao.deleteStudyDrafts(studyIds,draftIds);
			return ret;
			
		} catch (Exception e) {
			Rlog.fatal("studyDraft",
					"Exception In deleteStudyDrafts in CtrpDraftAgentBean " + e);
			return ret;
		}
	}
	//22472
	public int fetchNextDraftNum(int studyId) {
		 
		try {
		
		Query query=em.createNamedQuery("findCtrpDraftsByStudyId");
		 query.setParameter("fkStudy",studyId);
		 ArrayList drafts=(ArrayList)query.getResultList();
		 if (drafts == null || drafts.size()==0) {
			 return 1; 
		 }else{
			 return drafts.size()+1;
		 }
		}catch(Exception e){
			Rlog.fatal("studyDraft",
					"Exception In fetchNextDraftNum in CtrpDraftAgentBean " + e);
			return 1;
		}
	}
	public int deleteCtrpDrftStats(String ctrpDrftStatIds, String draftId){
		int ret=0;
		try {	
			CtrpDraftDao ctrpDraftDao = new CtrpDraftDao();
			ret= ctrpDraftDao.deleteCtrpDrftStats(ctrpDrftStatIds, draftId);
			return ret;
		} catch (Exception e) {
			Rlog.fatal("studyDraft",
					"Exception In deleteCtrpDrftStats in CtrpDraftAgentBean " + e);
			return ret;
		}
	}
	public Map<String, String> fetchStudyAutoComplete(String accId,String userId){
		
		try{
			StudyDao studyDao = new StudyDao();
			studyDao.getStudyAutoRows(userId,accId,null,2);
			ArrayList studyIds = studyDao.getStudyAutoIds();
			ArrayList studyNames = studyDao.getStudyAutoNames();
			HashMap<String, String> studyAutoCompleteData = new HashMap<String, String>();
			for(int i=0;i<studyIds.size();i++){
				studyAutoCompleteData.put(studyIds.get(i).toString(),studyNames.get(i).toString());
			}
			return studyAutoCompleteData;
		}catch(Exception e){
			Rlog.fatal("studyDraft",
					"Exception In fetchStudyAutoComplete in CtrpDraftAgentBean " + e);
			return null;
		}
		
		
		
	}
	
}
