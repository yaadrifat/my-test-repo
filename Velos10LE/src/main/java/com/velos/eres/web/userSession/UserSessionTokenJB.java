package com.velos.eres.web.userSession;

import org.apache.log4j.Logger;

import com.velos.eres.service.userSessionAgent.UserSessionTokenAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.web.user.UserJB;

public class UserSessionTokenJB {
	private static Logger logger = Logger.getLogger(UserSessionTokenJB.class);
	public static final String CSRF_TOKEN = "CSRFToken";
	
	public String getCsrfTokenLiteral() { return CSRF_TOKEN; }
	
	public String createTokenBeforeLogin(String ipAdd) {
		try {
			UserSessionTokenAgentRObj sessTokenRObj = EJBUtil.getUserSessionTokenAgentHome();
			return sessTokenRObj.createTokenBeforeLogin(ipAdd);
		} catch(Exception e) {
			logger.fatal("Error in UserSessionTokenJB.createTokenBeforeLogin:"+e);
			return null;
		}
	}
	
	public String createTokenForUser(Integer fkUser) {
		if (fkUser == null) { return null; }
		try {
			UserSessionTokenAgentRObj sessTokenRObj = EJBUtil.getUserSessionTokenAgentHome();
			return sessTokenRObj.createTokenForUser(fkUser);
		} catch(Exception e) {
			logger.fatal("Error in UserSessionTokenJB.createTokenForUser:"+e);
			return null;
		}
	}
	
	public void clearUserLoginFlag(Integer fkUser) {
		if (fkUser == null) { return; }
		UserJB userJB = new UserJB();
		userJB.setUserId(fkUser);
		userJB.getUserDetails();
		userJB.setUserLoginFlag("0");
		userJB.setUserCurrntSsID(null);
		userJB.updateUser();
	}
	
	public String challengeTokenBeforeLogin(String token, String ipAdd) {
		if (token == null) { return null; }
		try {
			UserSessionTokenAgentRObj sessTokenRObj = EJBUtil.getUserSessionTokenAgentHome();
			return sessTokenRObj.challengeTokenBeforeLogin(token, ipAdd);
		} catch(Exception e) {
			logger.fatal("Error in UserSessionTokenJB.challengeTokenBeforeLogin:"+e);
			return null;
		}
	}
	
	public String challengeTokenForUser(String token, Integer fkUser) {
		if (token == null || fkUser == null) { return null; }
		try {
			UserSessionTokenAgentRObj sessTokenRObj = EJBUtil.getUserSessionTokenAgentHome();
			return sessTokenRObj.challengeTokenForUser(token, fkUser);
		} catch(Exception e) {
			logger.fatal("Error in UserSessionTokenJB.challengeTokenForUser:"+e);
			return null;
		}
	}
	
	public Integer invalidateAllTokensForUser(Integer fkUser) {
		if (fkUser == null) { return 0; }
		try {
			UserSessionTokenAgentRObj sessTokenRObj = EJBUtil.getUserSessionTokenAgentHome();
			return sessTokenRObj.invalidateAllTokensForUser(fkUser);
		} catch(Exception e) {
			logger.fatal("Error in UserSessionTokenJB.invalidateAllTokensForUser:"+e);
			return -1;
		}
	}
}
