/*
 * Classname : 			PatStudyStatJB.java
 * 
 * Version information
 *
 * Date 				06/16/2001
 * 
 * Copyright notice: 	Aithent
 */

package com.velos.eres.web.patStudyStat;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.PatStudyStatDao;
import com.velos.eres.business.patProt.impl.PatProtBean;
import com.velos.eres.business.patStudyStat.impl.PatStudyStatBean;
import com.velos.eres.service.patStudyStatAgent.PatStudyStatAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/**
 * PatStudyStatJB is a client side Java Bean for Patient Study Status' business
 * components. The class implements the Business Delegate pattern to reduce
 * coupling between presentation-tier clients and business services.
 * 
 * <p>
 * In eResearch Architecture, the class resides at the Application layer.
 * </p>
 * 
 * @author Deepali Sachdeva
 * @version %I%, %G%
 */
/*
 * *************************History********************************* Modified by
 * :Sonia Modified On:08/16/2004 Comment: 1. Added new attribute screenNumber,
 * screenedBy, screeningOutcome and respective getter/setters 2. Modified
 * methods for the new attribute 3. Modified JavaDoc comments
 * 
 * Modified by :Sonia Modified On : 08/19/2004 Comment: 1. Added new attribute
 * patProtObj 2. Modified methods for the new attribute
 * 
 * Modified by: Sonia Modified on : 09/02/2004 Comment: Added a new method
 * getPatStudyCurrentStatusWithCodeSubType()
 * 
 * *************************END****************************************
 */
public class PatStudyStatJB {

    /**
     * the Study Status Id
     */
    private int id;

    /**
     * patient study status
     */
    private String patStudyStat;

    /**
     * the Study id
     */

    private String studyId;

    /**
     * the Patient id
     */

    private String patientId;

    /**
     * the status start date
     */

    private String startDate;

    /**
     * the associated Notes
     */
    private String notes;

    /**
     * the status end date
     */

    private String endDate;

    /**
     * creator
     */
    private String creator;

    /**
     * last modified by
     */
    private String modifiedBy;

    /**
     * IP Address
     */
    private String ipAdd;

    /**
     * PatStudyStat Reason
     */
    private String patStudyStatReason;

    /**
     * Screen Number for patient
     */
    private String screenNumber;

    /**
     * User who screened the patient
     */
    private String screenedBy;

    /**
     * Screening Outcome Code
     */

    private String screeningOutcome;
    
 //JM: 01.09.2006 /////////////////   

    
    /**
     * Patient next followup date
     */
    public String ptstNextFlwup = null;
    
        
    /**
     * Patient consent verison
     */
    public String ptstConsentVer;          
    
    

    /**
     * Patient Enrollment Statekeeper
     */

    private PatProtBean patProtObj;
    
    /**
     * Current Status
     */
    
    private String currentStat;//KM
    /**
     * Patient Study Data 
     */
    
    private String patStudyDiagName;
    /**
     * Patient Demographics Reportable
     */
    
    private String dGrphReportable;

    // GETTER SETTER METHODS

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEndDate() {
        return this.endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getStartDate() {
        return this.startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStudyId() {
        return this.studyId;
    }

    public void setStudyId(String study) {
        this.studyId = study;
    }

    public String getPatientId() {
        return this.patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getPatStudyStat() {
        return this.patStudyStat;
    }

    public void setPatStudyStat(String patStudyStat) {
        this.patStudyStat = patStudyStat;
    }

    public String getPatStudyStatReason() {
        return this.patStudyStatReason;
    }

    public void setPatStudyStatReason(String patStudyStatReason) {
        this.patStudyStatReason = patStudyStatReason;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return Last Modified By
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * @return Returns the value of Screen Number.
     */
    public String getScreenNumber() {
        return screenNumber;
    }

    /**
     * Sets the value of Screen Number.
     * 
     * @param screenNumber
     *            The patient screen Number.
     */
    public void setScreenNumber(String screenNumber) {
        this.screenNumber = screenNumber;
    }

    /**
     * @return Returns the value of screenedBy - The user who screened the
     *         patient.
     */
    public String getScreenedBy() {
        return screenedBy;
    }

    /**
     * Sets the value of screenedBy.
     * 
     * @param screenedBy
     *            The id of the user who screened the patient.
     */
    public void setScreenedBy(String screenedBy) {
        this.screenedBy = screenedBy;
    }

    /**
     * @return Returns the value of Screening Outcome code for the patient.
     */
    public String getScreeningOutcome() {
        return screeningOutcome;
    }

    /**
     * Sets the value of Screening Outcome Code.
     * 
     * @param screeningOutcome
     *            Screening Outcome Code for the patient.
     */
    public void setScreeningOutcome(String screeningOutcome) {
        this.screeningOutcome = screeningOutcome;
    }
    
    
    
    //JM: 01.09.2006/////////////////
    

	public String getPtstNextFlwup() {
		return ptstNextFlwup;		
    }

    public void setPtstNextFlwup(String ptstNextFlwup) {    	
        this.ptstNextFlwup = ptstNextFlwup;        
    }
    
    public String getPtstConsentVer() {
        return ptstConsentVer;
    }

    public void setPtstConsentVer(String ptstConsentVer) {
        this.ptstConsentVer = ptstConsentVer;
    }
    
    ///////////////  
    
    
    

    /**
     * Returns the Patient Enrollment StateKeeper.
     * 
     * @return the Patient Enrollment StateKeeper.
     */
public PatProtBean getPatProtObj() {
    return patProtObj;
}

    /**
     * Sets the Patient Enrollment StateKeeper.
     * 
     * @param patProtObj
     *            the Patient Enrollment StateKeeper.
     */
    public void setPatProtObj(PatProtBean patProtObj) {
        this.patProtObj = patProtObj;
    }
    
    //	Added by Manimaran for the September Enhancement (S8).
    
    /**
     * @return Current Status
     */
    public String getCurrentStat() {
    	return this.currentStat;
    }
    
    /**
     * @param currentStat
     *            The Status of the Patient
     */
    public void setCurrentStat(String currentStat) {
    	this.currentStat = currentStat;
    }

    // END OF GETTER SETTER METHODS

    
	public String getPatStudyDiagName() {
		return patStudyDiagName;
	}

	public void setPatStudyDiagName(String patStudyDiagName) {
		this.patStudyDiagName = patStudyDiagName;
	}

	public String getdGrphReportable() {
		return dGrphReportable;
	}

	public void setdGrphReportable(String dGrphReportable) {
		this.dGrphReportable = dGrphReportable;
	}

	/**
     * Contructor to create PatStudyStatJB with patient study status id
     * 
     * @param id
     *            patient study status id
     */
    public PatStudyStatJB(String id) {
        int patStudyStatId = 0;
        patStudyStatId = EJBUtil.stringToNum(id);
        setId(patStudyStatId);
    }

    /** Default constructor */
    public PatStudyStatJB() {
    }

    /**
     * Populates itself with the patient study status details using its
     * attribute : id Calls getPatStudyStatDetails() on the Patient Study Status
     * Session Bean: PatStudyStatAgentBean. <br>
     * The session bean method returns a PatStudyStatStateKeeper object that is
     * used by this method <br>
     * to populate the object's attributes.
     * 
     * @return A PatStudyStatStateKeeper object
     * @see PatStudyStatStateKeeper
     */
    public PatStudyStatBean getPatStudyStatDetails() {
        PatStudyStatBean psk = null;
        try {
            PatStudyStatAgentRObj patStatAgent = EJBUtil
                    .getPatStudyStatAgentHome();

            psk = patStatAgent.getPatStudyStatDetails(this.id);
        } catch (Exception e) {
            Rlog.fatal("patstudystat",
                    "EXception in calling session bean in study status" + e);

        }
        if (psk != null) {
            this.id = psk.getId();
            this.endDate = psk.returnEndDate();
            this.notes = psk.getNotes();
            this.startDate = psk.returnStartDate();
            this.studyId = psk.getStudyId();
            this.patStudyStat = psk.getPatStudyStat();
            this.creator = psk.getCreator();
            this.modifiedBy = psk.getModifiedBy();
            this.ipAdd = psk.getIpAdd();
            this.patientId = psk.getPatientId();
            setPatStudyStatReason(psk.getPatStudyStatReason());
            setScreeningOutcome(psk.getScreeningOutcome());
            setScreenedBy(psk.getScreenedBy());
            setScreenNumber(psk.getScreenNumber());
            
//JM:01.06.2006////////////

            this.ptstNextFlwup = psk.getPtstNextFlwup();
            this.ptstConsentVer  = psk.getPtstConsentVer();
/////////            
            setPatProtObj(psk.getPatProtObj());
            this.currentStat = psk.getCurrentStat();//KM

        }
        return psk;
    }

    /**
     * Creates a new patient study status using the object's attributes. Calls
     * setPatStudyStatDetails() on the <br>
     * PatStudyStatAgent Session Bean
     * 
     * @return success flag
     *         <ul>
     *         <li> id of new patient status</li>
     *         <li> -2 in case of a failure</li>
     *         </ul>
     */

    public int setPatStudyStatDetails() {
        try {
            int ret;
            PatStudyStatAgentRObj patStatAgent = EJBUtil
                    .getPatStudyStatAgentHome();

            ret = patStatAgent.setPatStudyStatDetails(this
                    .createPatStudyStatStateKeeper());
            return ret;
        } catch (Exception e) {
            Rlog.fatal("patstudystat",
                    "in session bean - set study status details" + e);
            return -2;
        }
    }

    /**
     * Updates a Patient Study Status using the object's attributes. Calls
     * updatePatStudyStat() on the <br>
     * PatStudyStatAgent Session Bean
     * 
     * @return success flag
     *         <ul>
     *         <li> 0 if successful</li>
     *         <li> -2 in case of a failure</li>
     *         </ul>
     */
    public int updatePatStudyStat() {
        int ret = 0;
        try {
            PatStudyStatAgentRObj patStatAgent = EJBUtil
                    .getPatStudyStatAgentHome();
            ret = patStatAgent.updatePatStudyStat(this
                    .createPatStudyStatStateKeeper());
            return ret;
        } catch (Exception e) {
            Rlog.fatal("patstudystat",
                    "in session bean - set study status details " + e);
            return -2;
        }
    }

    /**
     * Returns an PatStudyStatStateKeeper object with the current values of the
     * Bean
     * 
     * @return PatStudyStatStateKeeper object
     * @see com.velos.eres.business.patStudyStat.PatStudyStatStateKeeper
     */
    public PatStudyStatBean createPatStudyStatStateKeeper() {

        return new PatStudyStatBean(id, patStudyStat, studyId, patientId,
                startDate, notes, endDate, creator, modifiedBy, ipAdd,
                patStudyStatReason, screenNumber, screenedBy, screeningOutcome,
                ptstNextFlwup, ptstConsentVer,
                patProtObj, currentStat);   	
    	
    	
    	
    }

    public PatStudyStatDao getStudyPatients(String studyId) {
        PatStudyStatDao pssDao = new PatStudyStatDao();
        try {
            Rlog.debug("patstudystat",
                    "PatStudyStatJB.getStudyPatients starting");
            PatStudyStatAgentRObj patStudyStatAgentRObj = EJBUtil
                    .getPatStudyStatAgentHome();

            Rlog.debug("patstudystat",
                    "PatStudyStatJB.getStudyPatients after remote");
            pssDao = patStudyStatAgentRObj.getStudyPatients(studyId);
            Rlog.debug("patstudystat",
                    "PatStudyStatJB.getStudyPatients after Dao");
        } catch (Exception e) {
            Rlog.fatal("patstudystat",
                    "Error in getStudyPatients in PatStudyStatJB " + e);
        }
        return pssDao;
    }

    /**
     * Populates itself with the patient's current study status details Calls
     * getPatStudyCurrentStatus() on the Patient Study Status Session Bean:
     * PatStudyStatAgentBean. <br>
     * The session bean method returns a PatStudyStatStateKeeper object that is
     * used by this method <br>
     * to populate the object's attributes.
     * 
     * @param studyId
     *            Study Id
     * @param patientId
     *            Patient Id
     */
    public void getPatStudyCurrentStatus(String studyId, String patientId) {
        PatStudyStatBean psk = null;
        try {
            PatStudyStatAgentRObj patStatAgent = EJBUtil
                    .getPatStudyStatAgentHome();

            psk = patStatAgent.getPatStudyCurrentStatus(studyId, patientId);
        } catch (Exception e) {
            Rlog.fatal("patstudystat",
                    "EXception in PatStudyStatJB.getPatStudyCurrentStatus" + e);

        }
        if (psk != null) {
            this.id = psk.getId();
            this.endDate = psk.returnEndDate();
            this.notes = psk.getNotes();
            this.startDate = psk.returnStartDate();
            this.studyId = psk.getStudyId();
            this.patStudyStat = psk.getPatStudyStat();
            this.creator = psk.getCreator();
            this.modifiedBy = psk.getModifiedBy();
            this.ipAdd = psk.getIpAdd();
            this.patientId = psk.getPatientId();
            setPatStudyStatReason(psk.getPatStudyStatReason());
            setScreeningOutcome(psk.getScreeningOutcome());
            setScreenedBy(psk.getScreenedBy());
            setScreenNumber(psk.getScreenNumber());
            
            
//JM:01.06.2006///////////
            
            this.ptstNextFlwup = psk.getPtstNextFlwup();
            this.ptstConsentVer  = psk.getPtstConsentVer();
            
            
///////           
            setPatProtObj(psk.getPatProtObj());
            this.currentStat = psk.getCurrentStat();//KM
            

        }

    }

    /**
     * Removes a Patient Study Status
     * 
     * @param statId
     *            Patient Study Status PK
     * @return success flag
     *         <ul>
     *         <li> 0 if successful</li>
     *         <li> -2 in case of a failure</li>
     *         </ul>
     */
    public int reomovePatStudyStat(int statId) {
        int ret = 0;
        try {
            PatStudyStatAgentRObj patStatAgent = EJBUtil
                    .getPatStudyStatAgentHome();

            ret = patStatAgent.reomovePatStudyStat(statId);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("patstudystat",
                    "Exception in PatStudyStatJB.removePatStudyStat " + e);
            return -2;
        }
    }
    
    //Overloaded for INF-18183 : Raviesh
    public int reomovePatStudyStat(int statId,Hashtable<String, String> args) {
        int ret = 0;
        try {
            PatStudyStatAgentRObj patStatAgent = EJBUtil
                    .getPatStudyStatAgentHome();

            ret = patStatAgent.reomovePatStudyStat(statId,args);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("patstudystat",
                    "Exception in PatStudyStatJB.removePatStudyStat " + e);
            return -2;
        }
    }

    /**
     * Populates itself with the patient's current study status details and
     * returns the status code subtype
     * 
     * @param studyId
     *            Study Id
     * @param patientId
     *            Patient Id
     * @return patient study status's code subtype
     */
    public String getPatStudyCurrentStatusWithCodeSubType(String studyId,
            String patientId) {
        String patStudyStat = "";
        String patStatSubType = "";

        CodeDao cdStat = new CodeDao();
        getPatStudyCurrentStatus(studyId, patientId);

        patStudyStat = getPatStudyStat();

        cdStat.getCodeValuesById(EJBUtil.stringToNum(patStudyStat));

        ArrayList arSubType = new ArrayList();
        arSubType = cdStat.getCSubType();

        if (arSubType.size() > 0) {
            patStatSubType = (String) arSubType.get(0);
        }

        return patStatSubType;

    }

    /**
     * 
     * @return pk
     *         <ul>
     *         <li> id of new patient status</li>
     *         <li> -2 in case of a failure</li>
     *         </ul>
     */

    public int getPkForStatusCodelstSubType(int studyId, int patId,
            String codeSubType) {
        try {
            int ret;
            PatStudyStatAgentRObj patStatAgent = EJBUtil
                    .getPatStudyStatAgentHome();

            ret = patStatAgent.getPkForStatusCodelstSubType(studyId, patId,
                    codeSubType);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("patstudystat",
                    "in session bean - getPkForStatusCodelstSubType" + e);
            return -2;
        }
    }
    
    
   //Added by Manimaran to prompt if PatientStudyId being entered is duplicate
    public int getCountPatStudyId(int studyId,String patStudyId) {
        int count = 0;
        try {
        	PatStudyStatAgentRObj patStatAgent = EJBUtil
            .getPatStudyStatAgentHome();
            count = patStatAgent.getCountPatStudyId(studyId,patStudyId);
            
        } catch (Exception e) {
            Rlog.fatal("patstudystat", "Error in getCountPatStudyId() in PatStudyStatJB " + e);
        }
        return count;
    }
    
    /*
     * generates a String of XML for the customer details entry form.<br><br>
     * Note that it is planned to encapsulate this detail in another object.
     */
    public String toXML() {
        return null;
    }// end of method

}// end of class

