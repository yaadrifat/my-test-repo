/**
 * This is the java bean to handle the changes in the form status 
 * @author Salil
 * Date 07/29/2003
 **/

package com.velos.eres.web.formStat;

// importing packages

import com.velos.eres.business.common.FormStatusDao;
import com.velos.eres.business.formStat.impl.FormStatBean;
import com.velos.eres.service.formStatAgent.FormStatAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

public class FormStatJB {

    // this is the primary key for the form Stat table
    private int formStatId;

    // this is the reference to the form library
    private String formLib;

    // this is the refernce to the code list table
    private String codeLst;

    // This is s a refernce to the person changing the status
    private String changedBy;

    // This is the start date of the changed status
    private String stDate;

    // This is the end date of the status
    private String endDate;

    // These are the notes about the change in form status
    private String notes;

    // this store the record type
    private String recordType;

    // This is the refernce to the creatot
    private String creator;

    // This is the refernce to the person who last modified the form status
    private String lastModifiedBy;

    // This is the ip address of the machine that changed the form status
    private String ipAdd;

    // these are the getter functions

    public int getFormStatId() {
        return this.formStatId;
    }

    public String getFormLib() {
        return this.formLib;
    }

    public String getCodeLst() {
        return this.codeLst;
    }

    public String getChangedBy() {
        return this.changedBy;
    }

    public String getStDate() {
        return this.stDate;
    }

    public String getEndDate() {
        return this.endDate;
    }

    public String getNotes() {
        return this.notes;
    }

    public String getRecordType() {
        return this.recordType;
    }

    public String getCreator() {
        return this.creator;
    }

    public String getLastModifiedBy() {
        return this.lastModifiedBy;
    }

    public String getIpAdd() {
        return this.ipAdd;
    }

    // /These are the setters

    public void setFormStatId(int formStatId) {
        this.formStatId = formStatId;
    }

    public void setFormLib(String formLib) {
        this.formLib = formLib;
    }

    public void setCodeLst(String codeLst) {
        this.codeLst = codeLst;
    }

    public void setChangedBy(String changedBy) {
        this.changedBy = changedBy;
    }

    public void setStDate(String stDate) {
        this.stDate = stDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // / The Constructor for the Java Bean

    public void formStat() {
    }

    /*
     * This is to set the form status when it is entered for the first time
     * 
     */

    public int setFormStatDetails() {
        try {

            Rlog.debug("formstat", "Inside setFormStatDetails in FormStatJB");
            FormStatAgentRObj formStatAgentRObj = EJBUtil
                    .getFormStatAgentHome();

            this.setFormStatId(formStatAgentRObj.setFormStatDetails(this
                    .createFormStatStateKeeper()));
            return formStatId;

        } catch (Exception e) {
            Rlog.fatal("formstat",
                    "Exception in setting form status in setFormStatDetails in FormStatJB"
                            + e);
            return 0;
        }

    }

    /*
     * This would update the the form status for an already existing form
     */
    public int updateFormStatDetails() {
        int output;
        try {
            Rlog
                    .debug("formstat",
                            "Inside updateFormStatDetails in FormStatJB");

            FormStatAgentRObj formStatAgentRObj = EJBUtil
                    .getFormStatAgentHome();
            output = formStatAgentRObj.updateFormStatDetails(this
                    .createFormStatStateKeeper());

        } catch (Exception e) {
            Rlog.fatal("formstat",
                    "Exception in updating form status in updateFormStatDetails in FormStatJB"
                            + e);
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * This would get the form status details by getting the details in a
     * statekeeper object
     */
    public FormStatBean getFormStatDetails() {
        FormStatBean fssk = null;
        try {
            Rlog.debug("formstat", "Inside getFormStatDetails in FormStatJB");
            FormStatAgentRObj formStatAgentRObj = EJBUtil
                    .getFormStatAgentHome();

            fssk = formStatAgentRObj.getFormStatDetails(this.formStatId);
        } catch (Exception e) {
            Rlog.fatal("formstat",
                    "Exception in getting statekeeper from getFormStatDetails in FormStatJB"
                            + e);
        }
        if (fssk != null) {
            this.formStatId = fssk.getFormStatId();
            this.formLib = fssk.getFormLib();
            this.codeLst = fssk.getCodeLst();
            this.changedBy = fssk.getChangedBy();
            this.stDate = fssk.getStDate();
            this.endDate = fssk.getEndDate();
            this.lastModifiedBy = fssk.getLastModifiedBy();
            this.notes = fssk.getNotes();
            this.creator = fssk.getCreator();
            this.recordType = fssk.getRecordType();
            this.ipAdd = fssk.getIpAdd();
        }
        return fssk;
    }

    /**
     * This function would help in creating the statekeeper object and would
     * return a formstatstatekeeper object
     */

    public FormStatBean createFormStatStateKeeper() {
        Rlog
                .debug("formstat",
                        "Inside createFormStatStateKeeper in FormStatJB");
        return new FormStatBean(formStatId, formLib, codeLst, changedBy,
                stDate, endDate, notes, recordType, creator, lastModifiedBy,
                ipAdd);
    }

    /**
     * Method to get all the Status of a form param formId
     * 
     * @return FormStatusDao
     */

    public FormStatusDao getFormStatusHistory(int formId) {
        FormStatusDao formStatusDao = new FormStatusDao();

        try {
            Rlog.debug("formstat", "Exception in getFormStatusHistory");
            FormStatAgentRObj formStatAgentRObj = EJBUtil
                    .getFormStatAgentHome();

            formStatusDao = formStatAgentRObj.getFormStatusHistory(formId);
            return formStatusDao;
        } catch (Exception e) {
            Rlog.debug("formstat", "Exception in getFormStatusHistory" + e);
            return null;
        }
    }

}