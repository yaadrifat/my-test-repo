/*
 * Classname : 				InvoiceJB
 *
 * Version information : 	1.0
 *
 * Date 					10/18/2005
 *
 * Copyright notice: 		Velos Inc
 *
 * Author 					Sonia Abrol
 */

package com.velos.eres.web.invoice;

/* IMPORT STATEMENTS */


import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import javax.persistence.Column;

import com.velos.eres.business.common.InvoiceDao;
import com.velos.eres.business.common.MileAchievedDao;
import com.velos.eres.business.invoice.InvoiceBean;
import com.velos.eres.service.invoiceAgent.InvoiceAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for Invoice module.
 *
 * @author Sonia Abrol
 * @version 1.0, 10/18/2005
 */

public class InvoiceJB {
	/**
	 * the Invoice Primary Key
	 */
	private int id;

	/**
	 * the Invoice Number
	 */

	private String invNumber;

	/**
	 * the Invoice addressed to
	 */
	private String invAddressedto;

	/**
	 * the Invoice sent from
	 */
	private String invSentFrom;

	/**
	 * the invoice header
	 */
	private String invHeader;

	/**
	 * the invoice footer
	 */
	private String invFooter;

	/**
	 * Milestone rule status type
	 */
	private String invMileStatusType;

	/**
	 * the invoice interval type
	 */
	private String invIntervalType;

	/**
	 * Invoice interval from
	 */
	private String invIntervalFrom;

	/**
	 * Invoice interval To
	 */
	private String invIntervalTo;

	/**
	 * Invoice Date
	 */
	private String invDate;

	/**
	 * the invoice Notes
	 */
	private String invNotes ;

	/**
	 * the invoice Other user
	 */
	private String invOtherUser;


	/**
	 * the id of the user who created the record
	 */
	private String creator;

	/**
	 * Id of the user who last modified the record
	 */
	private String lastModifiedBy;

	/**
	 * the IP address of the user who created or modified the record
	 */
	private String iPAdd;

	private String invPayDueBy;

	private String invPayUnit;

	private String study;

	private String site;

	private String intnlAccNum;//JM:

	private String invStat;//JM:
	
	private String covTyp;
	
	/**
	 * @param covTyp the covTyp to set
	 */
	public void setCovTyp(String covTyp) {
		this.covTyp = covTyp;
	}

	/**
	 * @return the covTyp
	 */
	public String getCovTyp() {
		return covTyp;
	}
	//Commented by Manimaran for Enh#FIN12

	//private String includeHeader; //KM

	//private String includeFooter;

	//JM: 111306
	public String getIntnlAccNum() {
		return intnlAccNum;
	}

	public void setIntnlAccNum(String internalAccNum) {
		this.intnlAccNum = internalAccNum;
	}


	public String getInvStat() {
		return invStat;
	}

	public void setInvStat(String invStat) {
		this.invStat = invStat;
	}


	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

    public String getStudy() {
		return study;
	}

	public void setStudy(String study) {
		this.study = study;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getInvAddressedto() {
		return invAddressedto;
	}

	public void setInvAddressedto(String invAddressedto) {
		this.invAddressedto = invAddressedto;
	}

	public String getInvDate() {
		return invDate;
	}

	public void setInvDate(Date invDate) {
		setInvDate(DateUtil.dateToString(invDate));
	}



	public void setInvDate(String invDate) {
		this.invDate = invDate;
	}

	public String getInvFooter() {
		return invFooter;
	}

	public void setInvFooter(String invFooter) {
		this.invFooter = invFooter;
	}

	public String getInvHeader() {
		return invHeader;
	}

	public void setInvHeader(String invHeader) {
		this.invHeader = invHeader;
	}

	public String getInvIntervalFrom() {
		return invIntervalFrom;
	}

	public void setInvIntervalFrom(String invIntervalFrom) {
		this.invIntervalFrom = invIntervalFrom;
	}

	public void setInvIntervalFrom(Date invIntervalFrom) {
		setInvIntervalFrom(DateUtil.dateToString(invIntervalFrom));
	}

	public String getInvIntervalTo() {
		return invIntervalTo;
	}

	public void setInvIntervalTo(String invIntervalTo) {
		this.invIntervalTo = invIntervalTo;
	}
	public void setInvIntervalTo(Date invIntervalTo) {
		setInvIntervalTo(DateUtil.dateToString(invIntervalTo));
	}


	public String getInvIntervalType() {
		return invIntervalType;
	}

	public void setInvIntervalType(String invIntervalType) {
		this.invIntervalType = invIntervalType;
	}

	public String getInvMileStatusType() {
		return invMileStatusType;
	}

	public void setInvMileStatusType(String invMileStatusType) {
		this.invMileStatusType = invMileStatusType;
	}

	public String getInvNotes() {
		return invNotes;
	}

	public void setInvNotes(String invNotes) {
		this.invNotes = invNotes;
	}

	public String getInvNumber() {
		return invNumber;
	}

	public void setInvNumber(String invNumber) {
		this.invNumber = invNumber;
	}

	public String getInvOtherUser() {
		return invOtherUser;
	}

	public void setInvOtherUser(String invOtherUser) {
		this.invOtherUser = invOtherUser;
	}

	public String getInvSentFrom() {
		return invSentFrom;
	}

	public void setInvSentFrom(String invSentFrom) {
		this.invSentFrom = invSentFrom;
	}

	public String getIPAdd() {
		return iPAdd;
	}

	public void setIPAdd(String add) {
		iPAdd = add;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getInvPayDueBy() {
		return invPayDueBy;
	}

	public void setInvPayDueBy(String invPayDueBy) {
		this.invPayDueBy = invPayDueBy;
	}


	public String getInvPayUnit() {
		return invPayUnit;
	}

	public String getCalculatedInvPaymentDate() {

		String dt = "";
		String origdate = "";

		int unitdays = 1;
		int totaldays = 0;

		invPayUnit = invPayUnit.trim();

		if (invPayUnit.equals("M"))
		{
			unitdays = 30;

		} else  if (invPayUnit.equals("W"))
		{
			unitdays = 7;

		}
		else if (invPayUnit.equals("D"))
		{
			unitdays = 1;

		}
		else if (invPayUnit.equals("Y"))
		{
			unitdays = 365;

		}

		 
		origdate = getInvDate();

		totaldays = EJBUtil.stringToNum(invPayDueBy) * unitdays;

		dt = DateUtil.calculateNewDate(origdate, totaldays);

		return dt;
	}


	public void setInvPayUnit(String invPayUnit) {
		this.invPayUnit = invPayUnit;
	}

	//Commented by Manimaran for Enh.#FIN12(Req.change)

	/*public String getIncludeHeader() {
		return includeHeader;
	}

	public void setIncludeHeader(String includeHeader) {
		this.includeHeader = includeHeader;
	}

	public String getIncludeFooter() {
		return includeFooter;
	}

	public void setIncludeFooter(String includeFooter) {
		this.includeFooter = includeFooter;
	}*/

	 // END OF GETTER SETTER METHODS


	/**
     * Defines an InvoiceJB object with the specified category Id
     */
    public InvoiceJB(int id) {
        setId(id);
    }

    /**
     * Defines an InvoiceJB object with default values for fields
     */
    public InvoiceJB() {
        Rlog.debug("Invoice", "InvoiceJB.InvoiceJB() ");
    };

    /**
     * Defines an InvoiceJB object with the specified values for the fields
     *
     */
     /** Full Argument constructor*/

    /** Full Argument constructor */
	public InvoiceJB(String creator, int id, String addressedto, String invDate, String footer, String header, String invIntervalFrom, String invIntervalTo, String invIntervalType, String invMileStatusType, String notes, String invNumber, String invOtherUser, String invSentFrom, String iPadd, String lastModifiedBy,
			String invPayDueBy,String invPayUnit ,String study, String site, String intnlAccNum, String invStat ) {

		this.creator = creator;
		this.id = id;
		this.invAddressedto = addressedto;
		this.invDate = invDate;
		this.invFooter = footer;
		this.invHeader = header;
		this.invIntervalFrom = invIntervalFrom;
		this.invIntervalTo = invIntervalTo;
		this.invIntervalType = invIntervalType;
		this.invMileStatusType = invMileStatusType;
		this.invNotes = notes;
		this.invNumber = invNumber;
		this.invOtherUser = invOtherUser;
		this.invSentFrom = invSentFrom;
		this.iPAdd =  iPadd;
		this.lastModifiedBy = lastModifiedBy;
		setInvPayDueBy(invPayDueBy);
		setInvPayUnit(invPayUnit);
		setStudy(study);
		setSite(site);
		setIntnlAccNum(intnlAccNum);
		setInvStat(invStat);

	}

    /** Full Argument constructor */
	public InvoiceJB(String creator, int id, String addressedto, String invDate, String footer, String header, String invIntervalFrom, String invIntervalTo, String invIntervalType, String invMileStatusType, String notes, String invNumber, String invOtherUser, String invSentFrom, String iPadd, String lastModifiedBy,
			String invPayDueBy,String invPayUnit ,String study, String site, String intnlAccNum, String invStat,String covTyp ) {

		this.creator = creator;
		this.id = id;
		this.invAddressedto = addressedto;
		this.invDate = invDate;
		this.invFooter = footer;
		this.invHeader = header;
		this.invIntervalFrom = invIntervalFrom;
		this.invIntervalTo = invIntervalTo;
		this.invIntervalType = invIntervalType;
		this.invMileStatusType = invMileStatusType;
		this.invNotes = notes;
		this.invNumber = invNumber;
		this.invOtherUser = invOtherUser;
		this.invSentFrom = invSentFrom;
		this.iPAdd =  iPadd;
		this.lastModifiedBy = lastModifiedBy;
		setInvPayDueBy(invPayDueBy);
		setInvPayUnit(invPayUnit);
		setStudy(study);
		setSite(site);
		setIntnlAccNum(intnlAccNum);
		setInvStat(invStat);
		setCovTyp(covTyp);

	}


    /**
     * Retrieves the details of an category in InvoiceStateKeeper Object
     *
     * @return InvoiceBean
     * @see InvoiceBean
     */
    public InvoiceBean getInvoiceDetails() {
        InvoiceBean iBean = null;
        try {
            InvoiceAgentRObj invoiceAgentRObj = EJBUtil.getInvoiceAgentHome();
            iBean = invoiceAgentRObj.getInvoiceDetails(this.id);
            Rlog.debug("Invoice", "In InvoiceJB.getInvoiceDetails() " + iBean);
        } catch (Exception e) {
            Rlog.fatal("Invoice", "Error in getInvoiceDetails() in InvoiceJB "
                            + e);
        }
        if (iBean != null) {
        	setInvAddressedto(iBean.getInvAddressedto());
			setInvDate(iBean.getInvDate());
			setInvFooter(iBean.getInvFooter()) ;
			setInvHeader(iBean.getInvHeader()) ;
			setInvIntervalFrom(iBean.getInvIntervalFrom()) ;
			setInvIntervalTo(iBean.getInvIntervalTo());
			setInvIntervalType(iBean.getInvIntervalType()) ;
			setInvMileStatusType(iBean.getInvMileStatusType()) ;
			setInvNotes(iBean.getInvNotes());
			setInvNumber(iBean.getInvNumber()) ;
			setInvOtherUser(iBean.getInvOtherUser()) ;
			setInvSentFrom(iBean.getInvSentFrom()) ;
			setIPAdd(iBean.getIPAdd()) ;
			setLastModifiedBy(iBean.getLastModifiedBy()) ;
			setInvPayDueBy(iBean.getInvPayDueBy());
			setInvPayUnit(iBean.getInvPayUnit());
			setStudy(iBean.getStudy());
			setSite(iBean.getSite());
			setIntnlAccNum(iBean.getIntnlAccNum());//JM:
			setInvStat(iBean.getInvStat());
			setCreator(iBean.getCreator()) ;
			setCovTyp(iBean.getCovTyp());
			//setIncludeHeader(iBean.getIncludeHeader());//KM
			//setIncludeFooter(iBean.getIncludeFooter());
        }
        return iBean;
    }

    public void setInvoiceDetails() {
        int output = 0;
        try {
            InvoiceAgentRObj invoiceAgentRObj = EJBUtil.getInvoiceAgentHome();
            output = invoiceAgentRObj.setInvoiceDetails(this.createInvoiceBean());
            this.setId(output);
            Rlog.debug("Invoice", "InvoiceJB.setInvoiceDetails()");



        } catch (Exception e) {
            Rlog
                    .fatal("Invoice", "Error in setInvoiceDetails() in InvoiceJB "
                            + e);
            e.printStackTrace();
        }
    }

    /**
     * Saves the modified details of the category to the database
     *
     * @return 0 - If update succeeds <BR>
     *         -2 - If update fails
     */
    public int updateInvoice() {
        int output;
        try {
            InvoiceAgentRObj invoiceAgentRObj = EJBUtil.getInvoiceAgentHome();
            output = invoiceAgentRObj.updateInvoice(this.createInvoiceBean());


        } catch (Exception e) {
            Rlog.fatal("Invoice",    "EXCEPTION IN updateInvoice in InvoiceJB BEAN"
                            + e.getMessage());

            return -2;
        }
        return output;
    }

    /**
     * Uses the values of the current InvoiceJB object to create an
     * InvoiceBean Object
     *
     * @return InvoiceBean object
      */
    public InvoiceBean createInvoiceBean() {
        Rlog.debug("Invoice", "InvoiceJB.createInvoiceBean ");

        Date dtInvDate = new Date();
        Date dtIntervalFrom = new Date();
        Date dtIntervalTo = new Date();


        dtInvDate  = DateUtil.stringToDate(invDate, null);
        dtIntervalFrom = DateUtil.stringToDate(invIntervalFrom, null);
        dtIntervalTo = DateUtil.stringToDate(invIntervalTo, null);

        return new InvoiceBean(creator, id, invAddressedto, dtInvDate ,  invFooter, invHeader, dtIntervalFrom,
        		dtIntervalTo, invIntervalType, invMileStatusType, invNotes, invNumber, invOtherUser, invSentFrom, iPAdd, lastModifiedBy,
        		invPayDueBy,invPayUnit,study,site,intnlAccNum,invStat,covTyp);

    }

     /**
     * Deletes Invoice Record
     *
     * @return 0 - If update succeeds <BR>
     *         -2 - If update fails
     */
    public int deleteInvoice() {
        int output;
        try {
            InvoiceAgentRObj invoiceAgentRObj = EJBUtil.getInvoiceAgentHome();
            output = invoiceAgentRObj.deleteInvoice(this.id);
        } catch (Exception e) {
            Rlog.fatal("Invoice",
                    "EXCEPTION IN deleteInvoice"      + e.getMessage());

            return -2;
        }
        return output;
    }

 // Overloaded for INF-18183 ::: AGodara
    public int deleteInvoice(Hashtable<String, String> auditInfo) {
        int output;
        try {
            InvoiceAgentRObj invoiceAgentRObj = EJBUtil.getInvoiceAgentHome();
            output = invoiceAgentRObj.deleteInvoice(this.id,auditInfo);
        } catch (Exception e) {
            Rlog.fatal("Invoice",
                    "EXCEPTION IN deleteInvoice deleteInvoice(Hashtable<String, String> auditInfo)"      + e.getMessage());
            return -2;
        }
        return output;
    }

    /**
     * Gets Invoice Dao with saved invoices for the study
     *
     * @return InvoiceDao
     */
    public InvoiceDao getSavedInvoices(String studyId) {
    	InvoiceDao inv = new InvoiceDao();
        try {
            InvoiceAgentRObj invoiceAgentRObj = EJBUtil.getInvoiceAgentHome();
            inv = invoiceAgentRObj.getSavedInvoices(EJBUtil.stringToNum(studyId));
        } catch (Exception e) {
            Rlog.fatal("Invoice",
                    "EXCEPTION IN getSavedInvoices"      + e.getMessage());

            return inv;
        }
        return inv;
    }
    
    /**
     * Gets Saved Invoices and parameters to be used to apply pagination in invoice browser
     * Added By : Raviesh
     */
    public InvoiceDao getSavedInvoicesWithPagination(String studyId,String orderBy,String orderType,Map getRowsCount,int curPage) {
    	InvoiceDao inv = new InvoiceDao();
        try {
            InvoiceAgentRObj invoiceAgentRObj = EJBUtil.getInvoiceAgentHome();
            inv = invoiceAgentRObj.getSavedInvoicesWithPagination(EJBUtil.stringToNum(studyId),orderBy,orderType, getRowsCount, curPage);
        } catch (Exception e) {
            Rlog.fatal("Invoice",
                    "EXCEPTION IN getSavedInvoicesWithPagination"      + e.getMessage());

            return inv;
        }
        return inv;
    }


    /**
     * Gets Achieved Milestones for a date range and milestone receivable status type
     *
     * @return MileAchievedDao
     */
    public MileAchievedDao getAchievedMilestones(String studyId, String rangeFrom,String rangeTo, String milestoneReceivableStatus,String milestoneType, String site)
    {
    	MileAchievedDao mile = new MileAchievedDao ();
        try {
            InvoiceAgentRObj invoiceAgentRObj = EJBUtil.getInvoiceAgentHome();
            mile = invoiceAgentRObj.getAchievedMilestones(EJBUtil.stringToNum(studyId),rangeFrom,rangeTo, milestoneReceivableStatus,milestoneType,site);
        } catch (Exception e) {
            Rlog.fatal("Invoice",
                    "EXCEPTION IN getAchievedMilestones"      + e.getMessage());

            return mile ;
        }
        return mile ;
    }
    public MileAchievedDao  getAchievedMilestonesForUser(String studyId, String rangeFrom,String rangeTo, String milestoneReceivableStatus,String milestoneType, String user)
    {
    	MileAchievedDao mile = new MileAchievedDao ();
        try {
            InvoiceAgentRObj invoiceAgentRObj = EJBUtil.getInvoiceAgentHome();
            mile = invoiceAgentRObj.getAchievedMilestonesForUser(EJBUtil.stringToNum(studyId),rangeFrom,rangeTo, milestoneReceivableStatus,milestoneType,user);
        } catch (Exception e) {
            Rlog.fatal("Invoice",
                    "EXCEPTION IN getAchievedMilestonesForUser"      + e.getMessage());

            return mile ;
        }
        return mile ;
    }



    /**
     * Gets the total amount invoiced for an invoice
     *
     * @param inv Invoice PK
     *            Description of the Parameter
     */
    public double getTotalAmountInvoiced(int inv)
    {
    	double ret = 0;

        try {
            InvoiceAgentRObj invoiceAgentRObj = EJBUtil.getInvoiceAgentHome();
            ret = invoiceAgentRObj.getTotalAmountInvoiced(inv);
        } catch (Exception e) {
            Rlog.fatal("Invoice",
                    "EXCEPTION IN getTotalAmountInvoiced(int inv) "      + e.getMessage());

            return ret ;
        }
        return ret ;
    }






    /**
     * Gets the invoice number sequence from the er_invoice table
     *
     * @param studyId
     *            Study Id
     *            JM: 111706
     */
    public String getInvoiceNumber() {
    	String invNumber = null;

            try {
                InvoiceAgentRObj invoiceAgentRObj = EJBUtil.getInvoiceAgentHome();
                invNumber = invoiceAgentRObj.getInvoiceNumber();

            } catch (Exception e) {
                Rlog.fatal("Invoice",
                        "EXCEPTION IN getInvoiceNumber() "      + e.getMessage());
                return invNumber ;

            }
            return invNumber ;
        }
	//Yogendra Pratap : Bug# 8764 :Date 28 Mar 2012
    public String replaceValues(String replaceValue,String replace, String replaceWithStr){
    	 if(replaceValue.indexOf(",") != -1 )
 	    	replaceValue= replaceValue.replaceAll(replace, replaceWithStr);
		return replaceValue;
	}
    
    /**
     * Added for FIN-22378 ::: Ankit
     * @return MileAchievedDao
     */    
    public MileAchievedDao getAchievedMilestonesNew(String studyId,String yearFilter,String monthFilterYear,String monthFilter, String rangeFrom,String rangeTo, String milestoneReceivableStatus,String milestoneType, String site,String dateRangeType)
    {
    	MileAchievedDao mile = new MileAchievedDao ();
        try {
            InvoiceAgentRObj invoiceAgentRObj = EJBUtil.getInvoiceAgentHome();
            mile = invoiceAgentRObj.getAchievedMilestonesNew(EJBUtil.stringToNum(studyId),rangeFrom,rangeTo, milestoneReceivableStatus,milestoneType,site,yearFilter,monthFilter,monthFilterYear,dateRangeType);
        } catch (Exception e) {
            Rlog.fatal("Invoice",
                    "EXCEPTION IN getAchievedMilestones"      + e.getMessage());

            return mile ;
        }
        return mile ;
    }
    
    public MileAchievedDao getAchievedMilestonesEM(String studyId,String yearFilter,String monthFilterYear,String monthFilter, String rangeFrom,String rangeTo, String milestoneReceivableStatus, String site, String CovType,String dateRangeType)
    {
    	MileAchievedDao mile = new MileAchievedDao ();
        try {
            InvoiceAgentRObj invoiceAgentRObj = EJBUtil.getInvoiceAgentHome();
            mile = invoiceAgentRObj.getAchievedMilestonesEM(EJBUtil.stringToNum(studyId),rangeFrom,rangeTo, milestoneReceivableStatus,site,CovType,yearFilter,monthFilter,monthFilterYear,dateRangeType);
        } catch (Exception e) {
            Rlog.fatal("Invoice",
                    "EXCEPTION IN getAchievedMilestonesEMandAM"      + e.getMessage());
            return mile ;
        }
        return mile ;
    }

    public MileAchievedDao getAchievedMilestonesAM(String studyId,String yearFilter,String monthFilterYear,String monthFilter, String rangeFrom,String rangeTo, String milestoneReceivableStatus, String site, String CovType,String dateRangeType)
    {
    	MileAchievedDao mile = new MileAchievedDao ();
        try {
            InvoiceAgentRObj invoiceAgentRObj = EJBUtil.getInvoiceAgentHome();
            mile = invoiceAgentRObj.getAchievedMilestonesAM(EJBUtil.stringToNum(studyId),rangeFrom,rangeTo, milestoneReceivableStatus,site,CovType,yearFilter,monthFilter,monthFilterYear,dateRangeType);
        } catch (Exception e) {
            Rlog.fatal("Invoice",
                    "EXCEPTION IN getAchievedMilestonesAM"      + e.getMessage());
            return mile ;
        }
        return mile ;
    }
/**
 * Check the invoice number sequence is existed or not from the er_invoice table
 * @param invNumber
 * Added by Sudhir Shukla for FIN-22382_0926012 Enhancement
 * @return
 */
    public boolean getInvoiceNumber(String invNumber) {
    	boolean flag=false;

            try {
                InvoiceAgentRObj invoiceAgentRObj = EJBUtil.getInvoiceAgentHome();
                flag = invoiceAgentRObj.getInvoiceNumber(invNumber);

            } catch (Exception e) {
                Rlog.fatal("Invoice",
                        "EXCEPTION IN getInvoiceNumber(invNumber) "      + e.getMessage());
                return flag ;

            }
            return flag ;
        }
    
    public int getInvoiceId(String invNumber) {
    	int invId=0;

            try {
                InvoiceAgentRObj invoiceAgentRObj = EJBUtil.getInvoiceAgentHome();
                invId = invoiceAgentRObj.getInvoiceId(invNumber);

            } catch (Exception e) {
                Rlog.fatal("Invoice",
                        "EXCEPTION IN getInvoiceNumber(invNumber) "      + e.getMessage());
                return invId ;

            }
            return invId ;
        }
    
    
    // Added for FIN-21239_09262012 ::: Sudhir Shukla
    public int selectInvoiceReconciledOrNot(Hashtable<String, String> auditInfo) {
        int output;
        try {
            InvoiceAgentRObj invoiceAgentRObj = EJBUtil.getInvoiceAgentHome();
            output = invoiceAgentRObj.selectInvoiceReconciledOrNot(this.id,auditInfo);
        } catch (Exception e) {
            Rlog.fatal("Invoice",
                    "EXCEPTION IN selectInvoiceReconciledOrNot selectInvoiceReconciledOrNot(Hashtable<String, String> auditInfo)"      + e.getMessage());
            return -2;
        }
        return output;
    }
    
    /**
     * Gets the invoice number sequence from the er_invoice table
     *
     * @param studyId
     *            Study Id
     *            JM: 111706
     */
    public int getMaxInvoiceNumber(int studyID) {
    	int invNumber = 0;

            try {
                InvoiceAgentRObj invoiceAgentRObj = EJBUtil.getInvoiceAgentHome();
                invNumber = invoiceAgentRObj.getMaxInvoiceNumber(studyID);

            } catch (Exception e) {
                Rlog.fatal("Invoice",
                        "EXCEPTION IN getInvoiceNumber() "      + e.getMessage());
                return invNumber ;

            }
            return invNumber ;
        }
         //end of class
    
  
}
