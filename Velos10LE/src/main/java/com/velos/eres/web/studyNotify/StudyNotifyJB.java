/*

 * Classname : StudyNotifyJB.java

 * 

 * Version information

 *

 * Date 03/16/2001

 * 

 * Copyright notice: Aithent 

 */

package com.velos.eres.web.studyNotify;

import com.velos.eres.business.studyNotify.impl.StudyNotifyBean;
import com.velos.eres.service.studyNotifyAgent.StudyNotifyAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/**
 * 
 * Client side bean for StudyNotify
 * 
 * @author Sajal
 * 
 */

public class StudyNotifyJB {

    /**
     * 
     * the studyNotify Id
     * 
     */

    private int studyNotifyId;

    /**
     * 
     * the Therapeutic area from codelist
     * 
     */

    private String studyNotifyCodelstTarea;

    /**
     * 
     * the studyNotify User Id
     * 
     */

    private String studyNotifyUserId;

    /**
     * 
     * the studyNotify type
     * 
     */

    private String studyNotifyType;

    /**
     * 
     * the studyNotify keywords
     * 
     */

    private String studyNotifyKeywords;

    /*
     * 
     * creator
     * 
     */

    private String creator;

    /*
     * 
     * last modified by
     * 
     */

    private String modifiedBy;

    /*
     * 
     * IP Address
     * 
     */

    private String ipAdd;

    // GETTER SETTER METHODS

    public int getStudyNotifyId() {

        return this.studyNotifyId;

    }

    public void setStudyNotifyId(int studyNotifyId) {

        this.studyNotifyId = studyNotifyId;

    }

    public String getStudyNotifyCodelstTarea() {

        return this.studyNotifyCodelstTarea;

    }

    public void setStudyNotifyCodelstTarea(String studyNotifyCodelstTarea) {

        this.studyNotifyCodelstTarea = studyNotifyCodelstTarea;

    }

    public String getStudyNotifyUserId() {

        return this.studyNotifyUserId;

    }

    public void setStudyNotifyUserId(String studyNotifyUserId) {

        this.studyNotifyUserId = studyNotifyUserId;

    }

    public String getStudyNotifyType() {

        return this.studyNotifyType;

    }

    public void setStudyNotifyType(String studyNotifyType) {

        this.studyNotifyType = studyNotifyType;

    }

    public String getStudyNotifyKeywords() {

        return this.studyNotifyKeywords;

    }

    public void setStudyNotifyKeywords(String studyNotifyKeywords) {

        this.studyNotifyKeywords = studyNotifyKeywords;

        Rlog.debug("studyNotify",
                "StudyNotifyJB.setstudyNotifyKeywords studyNotifyKeywords "
                        + studyNotifyKeywords);

    }

    /**
     * 
     * @return Creator
     * 
     */

    public String getCreator() {

        return this.creator;

    }

    /**
     * 
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     * 
     */

    public void setCreator(String creator) {

        this.creator = creator;

        Rlog
                .debug("studyNotify", "StudyNotifyJB.setCreator creator "
                        + creator);

    }

    /**
     * 
     * @return Last Modified By
     * 
     */

    public String getModifiedBy() {

        return this.modifiedBy;

    }

    /**
     * 
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     * 
     */

    public void setModifiedBy(String modifiedBy) {

        this.modifiedBy = modifiedBy;

        Rlog.debug("studyNotify", "StudyNotifyJB.setModifiedBy modifiedBy "
                + modifiedBy);

    }

    /**
     * 
     * @return IP Address
     * 
     */

    public String getIpAdd() {

        return this.ipAdd;

    }

    /**
     * 
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     * 
     */

    public void setIpAdd(String ipAdd) {

        this.ipAdd = ipAdd;

        Rlog.debug("studyNotify", "StudyNotifyJB.setIpAdd ipAdd " + ipAdd);

    }

    // END OF GETTER SETTER METHODS

    public StudyNotifyJB(int studyNotifyId) {

        setStudyNotifyId(studyNotifyId);

        Rlog.debug("studyNotify",
                "Constructor of StudyNotifyJB with studyNotifyId ");

    }

    public StudyNotifyJB() {

        Rlog.debug("studyNotify", "Blank constructor of StudyNotifyJB");

    }

    /**
     * 
     * Full arguments constructor.
     * 
     */

    public StudyNotifyJB(int studyNotifyId, String studyNotifyCodelstTarea,

    String studyNotifyUserId, String studyNotifyType,

    String studyNotifyKeywords, String creator, String modifiedBy,

    String ipAdd) {

        setStudyNotifyId(studyNotifyId);

        setStudyNotifyCodelstTarea(studyNotifyCodelstTarea);

        setStudyNotifyUserId(studyNotifyUserId);

        setStudyNotifyType(studyNotifyType);

        setStudyNotifyKeywords(studyNotifyKeywords);

        setCreator(creator);

        setModifiedBy(modifiedBy);

        setIpAdd(ipAdd);

        Rlog
                .debug("studyNotify",
                        "Full arguments constructor of StudyNotifyJB");

    }

    public StudyNotifyBean getStudyNotifyDetails() {
        StudyNotifyBean snsk = null;
        try {

            StudyNotifyAgentRObj studyNotifyAgentRObj = EJBUtil
                    .getStudyNotifyAgentHome();

            snsk = studyNotifyAgentRObj
                    .getStudyNotifyDetails(this.studyNotifyId);

            Rlog.debug("studyNotify",
                    "StudyNotifyJB.getStudyNotifyDetails() StudyNotifyStateKeeper "
                            + snsk);

        } catch (Exception e) {

            Rlog.fatal("studyNotify",
                    "Error in getStudyNotifyDetails() in StudyNotifyJB " + e);

        }

        if (snsk != null) {

            this.studyNotifyId = snsk.getStudyNotifyId();

            this.studyNotifyCodelstTarea = snsk.getStudyNotifyCodelstTarea();

            this.studyNotifyUserId = snsk.getStudyNotifyUserId();

            this.studyNotifyType = snsk.getStudyNotifyType();

            this.studyNotifyKeywords = snsk.getStudyNotifyKeywords();

            this.creator = snsk.getCreator();

            this.modifiedBy = snsk.getModifiedBy();

            this.ipAdd = snsk.getIpAdd();

        }

        return snsk;

    }

    public void setStudyNotifyDetails() {

        try {

            StudyNotifyAgentRObj studyNotifyAgentRObj = EJBUtil
                    .getStudyNotifyAgentHome();

            this
                    .setStudyNotifyId(studyNotifyAgentRObj
                            .setStudyNotifyDetails(this
                                    .createStudyNotifyStateKeeper()));

            Rlog.debug("studyNotify", "StudyNotifyJB.setStudyNotifyDetails()");

        } catch (Exception e) {

            Rlog.fatal("studyNotify",
                    "Error in setStudyNotifyDetails() in StudyNotifyJB " + e);

        }

    }

 // notify subscribers
    public int notifySubscribers(int studyPK){
    	int output = 0;
      try {

            StudyNotifyAgentRObj studyNotifyAgentRObj = EJBUtil
                    .getStudyNotifyAgentHome();

            output = studyNotifyAgentRObj.notifySubscribers(studyPK);

            Rlog.debug("studyNotify", "notifySubscribers()");

        } catch (Exception e) {

            Rlog.fatal("studyNotify",
                    "Error in notifySubscribers() in StudyNotifyJB " + e);
            
            output = -1;
        }
        
        return output;
    	
    }	
    
    	
    public int updateStudyNotify()

    {

        int output;

        try {

            StudyNotifyAgentRObj studyNotifyAgentRObj = EJBUtil
                    .getStudyNotifyAgentHome();

            output = studyNotifyAgentRObj.updateStudyNotify(this
                    .createStudyNotifyStateKeeper());

        }

        catch (Exception e) {
            Rlog.fatal("studyNotify",
                    "EXCEPTION IN SETTING STUDYNOTIFY DETAILS TO  SESSION BEAN "
                            + e);

            return -2;

        }

        return output;

    }

    public StudyNotifyBean createStudyNotifyStateKeeper() {

        return new StudyNotifyBean(studyNotifyId, studyNotifyCodelstTarea,
                studyNotifyUserId, studyNotifyType, studyNotifyKeywords,
                creator, modifiedBy, ipAdd);

    }

    /*
     * 
     * generates a String of XML for the studyNotify details entry form.<br><br>
     * 
     */

    public String toXML()

    {

        Rlog.debug("studyNotify", "StudyNotifyJB.toXML()");

        return new String(
                "<?xml version=\"1.0\"?>"
                        +

                        "<?cocoon-process type=\"xslt\"?>"
                        +

                        "<?xml-stylesheet href=\"/stylesheet/form-html.xsl\" type=\"text/xsl\"?>"
                        +

                        "<?xml-stylesheet href=\"/stylesheet/form-wml.xsl\" type=\"text/xsl\" media=\"wap\"?>"
                        +

                        // to be done "<form action=\"studyNotifysummary.jsp\">"
                        // +

                        "<head>" +

                        // to be done "<title>StudyNotify Summary </title>" +

                        "</head>" +

                        "<form>" +

                        "<input type=\"hidden\" name=\"studyNotifyId\" value=\""
                        + this.getStudyNotifyId() + "\" size=\"10\"/>" +

                        "<input type=\"text\" name=\"studyNotifyCodelstTarea\" value=\""
                        + this.getStudyNotifyCodelstTarea()
                        + "\" size=\"10\"/>" +

                        "<input type=\"text\" name=\" studyNotifyUserId\" value=\""
                        + this.getStudyNotifyUserId() + "\" size=\"50\"/>" +

                        "<input type=\"text\" name=\" studyNotifyType\" value=\""
                        + this.getStudyNotifyType() + "\" size=\"30\"/>" +

                        "<input type=\"text\" name=\" studyNotifyKeywords\" value=\""
                        + this.getStudyNotifyKeywords() + "\" size=\"30\"/>" +

                        "</form>"

        );

    }// end of method

}// end of class

