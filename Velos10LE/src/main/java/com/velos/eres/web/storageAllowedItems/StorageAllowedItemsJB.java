/*
 * Classname : 			StorageAllowedItemsJB
 * 
 * Version information : 	1.0
 *
 * Date 			08/06/2007
 * 
 * Copyright notice: 		Velos Inc
 * 
 * Author 			Khader
 */

package com.velos.eres.web.storageAllowedItems;

/* IMPORT STATEMENTS */

import java.util.Hashtable;
import com.velos.eres.business.common.StorageAllowedItemsDao;
import com.velos.eres.business.storageAllowedItems.impl.StorageAllowedItemsBean;
import com.velos.eres.service.storageAllowedItemsAgent.StorageAllowedItemsAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;


/* END OF IMPORT STATEMENTS */

public class StorageAllowedItemsJB {
	
    private int    pkAllowedItem ;
    private String fkStorage;	
    private String fkCodelstStorageType;
    private String fkCodelstSpecimenType;
    private String allowedItemType;
    
    /**
     * creator
    */
    private String creator;

    /**
     * last modified by
    */
    
    private String modifiedBy;

    /**
     * IP Address
     */
    private String ipAdd;

    
//  GETTERS AND SETTERS
 
    public StorageAllowedItemsJB(int pkAllowedItem) {
        setPkAllowedItems(pkAllowedItem);
    }

    /**
     * Default Constructor
     */
    public StorageAllowedItemsJB() {
        Rlog.debug("storageAllowedItems", "StorageAllowedItemsJB.StorageAllowedItemsJB()");
    }
    
	public StorageAllowedItemsJB(int pkAi,String fkStorage, String fkCodelstStorageType, String fkCodelstSpecimenType,String aiItemType, String creator, String modifiedBy, String ipAdd) {
		setCreator(creator);
		setModifiedBy(modifiedBy);
		setIpAdd(ipAdd);
	}
	
	public String getModifiedBy() {
		return modifiedBy;
	}
	public String getIpAdd() {
		return ipAdd;
	}
	
	/**
	 * @param ipAdd the ipAdd to set
	 */
	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}
	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public int getPkAllowedItems(){
		 return pkAllowedItem;
		
	}
    public void setPkAllowedItems(int pkAllowedItem){
    	this.pkAllowedItem = pkAllowedItem;
	
    	
    }
    public String getFkStorage(){
    	return fkStorage;
    	   	
    }
    public void setFkStorage(String fkStorage){
        this.fkStorage = fkStorage ;
    	
    }
    public String getFkCodelstStorageType(){
    	return fkCodelstStorageType ;
    	
    }
    public void setFkCodelstStorageType(String fkCodelstStorageType){
    	this.fkCodelstStorageType = fkCodelstStorageType;
    	
    }
    public String getFkCodelstSpecimenType(){
    	return fkCodelstSpecimenType;
    	
    }
    public void setFkCodelstSpecimenType(String fkCodelstSpecimenType){
    	this.fkCodelstSpecimenType = fkCodelstSpecimenType;
    	
    }
    public String getAllowedItemType(){
    	return allowedItemType;
    	   	
    }
    public void setAllowedItemType(String allowedItemType){
    	this.allowedItemType = allowedItemType;
    	
    	
    }
	public String getCreator() {
		return creator;
	}

	/**
	 * @param creator the creator to set
	 */
	public void setCreator(String creator) {
		this.creator = creator;
	}

    
    public int setStorageAllowedItemsDetails() {
    	
        int output = 0;
        try {
        	
        	StorageAllowedItemsAgentRObj storageAllowedItemstRObj = EJBUtil.getStorageAllowedItemAgentHome();
           	StorageAllowedItemsBean bean = this.createStorageAllowedItemStateKeeper();
            output = storageAllowedItemstRObj.setStorageAllowedItemsDetails(this.createStorageAllowedItemStateKeeper());
            this.setPkAllowedItems(output);
            Rlog.debug("storage", "StorageJB.setStorageDetails()");

        }catch (Exception e) {
            Rlog
                    .fatal("storage", "Error in setStorageDetails() in StorageJB "
                            + e);
          return -2;
        }
        return output;
    }
    public StorageAllowedItemsBean createStorageAllowedItemStateKeeper() {
    	
        Rlog.debug("storage", "StorageJB.createStorageStateKeeper ");
        return new StorageAllowedItemsBean(this.pkAllowedItem, this.fkStorage, this.fkCodelstStorageType, 
        		this.fkCodelstSpecimenType,
        		this.allowedItemType, 
        		this.creator,  this.modifiedBy,  this.ipAdd);
    }
    public StorageAllowedItemsDao getStorageAllowedValue(int pkStorage) {
    	System.out.println("inside get storage Allowed Value  details ...."+pkStorage);
    	StorageAllowedItemsDao storageAllowedItemDao = new StorageAllowedItemsDao();
    	try {
    		StorageAllowedItemsAgentRObj storageAllowedItemstRObj = EJBUtil.getStorageAllowedItemAgentHome();
            storageAllowedItemDao= storageAllowedItemstRObj.getStorageAllowedItemValue(pkStorage);
            return storageAllowedItemDao;
           
        } catch (Exception e) {
            Rlog.fatal("storage", "Error in getStorageValue(pkStorage) in StorageJB " + e);
        }
        return storageAllowedItemDao;
    }
    // Overloaded for INF-18183 ::: Akshi
    public void deleteStorageAllowedItems(int fkStorage,Hashtable<String, String> args) {
        
        try {
            Rlog.debug("storage", "StorageJB:deleteStorages starting");

            StorageAllowedItemsAgentRObj storageAllowedItems = EJBUtil.getStorageAllowedItemValue();
           storageAllowedItems.deleteStorageAllowedItems(fkStorage,args);

        } catch (Exception e) {
            Rlog.fatal("storage", "StorageJB:deleteStorage-Error " + e);
            
        }
       

    }
       
}// end of class

