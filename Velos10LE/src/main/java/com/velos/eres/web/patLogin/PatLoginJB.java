/*
 * Classname : 				PatLoginJB
 * 
 * Version information : 	1.0
 *
 * Date 					04/04/2007
 * 
 * Copyright notice: 		Velos Inc
 * 
 * Author 					Jnanamay Majumdar
 */

package com.velos.eres.web.patLogin;

/* IMPORT STATEMENTS */


import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.patLogin.impl.PatLoginBean;
import com.velos.eres.service.patLoginAgent.PatLoginAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.Security;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for Patient login module.
 * 
 * @author Jnanamay Majumdar
 * @version 1.0, 04/05/2007
 */

public class PatLoginJB {
	/**
	 * the PK_PORTAL_LOGIN Primary Key
	 */
	private int id;
	
	/**
	 * the PL_ID
	 */
	private String plId;
	

	/**
	 * the PL_ID_TYPE
	 * 
	 */
	private String  plIdType;
	
	
	/**
	 * patient login
	 */
	private String  plLogin;
	
	/**
	 * patient password
	 */
	private String  plPassword;
	
	/**
	 * patient status
	 */
	private String  plStatus;
	
	/**
	 * patient logout time
	 */
	private String  plLogOutTime;
	
	/**
	 * fk_portal
	 */
	private String  fkPortal;
	
	/*
	 * Audit columns
	 */
	private String  creator;
	private String  modifiedBy;
	private String  ipAdd;
	
	/** Flag to indicate if an auto login was created, just used in method validateLoginPassword */
	private boolean isAutoLogin;
	
	
	
//GET SET methos startes here
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPlId() {
		return plId;
	}

	public void setPlId(String plId) {
		this.plId = plId;
	}	
    
	public String getPlIdType() {
		return plIdType;
	}

	public void setPlIdType(String plIdtype) {
		this.plIdType = plIdtype;
	}
	
	public String getPlLogin() {
		return plLogin;
	}

	public void setPlLogin(String plLogin) {		
		
		this.plLogin = plLogin;
	}
	
	public String getPlPassword() {
	    return plPassword;
	}

    public void setPlPassword(String plPassword) {
	    this.plPassword = plPassword;
    }
    public String getPlStatus() {
    	return plStatus;
    }

    public void setPlStatus(String plStatus) {
    	this.plStatus = plStatus;
    }	

    public String getPlLogOutTime() {
  		return plLogOutTime;
  	}

  	public void setPlLogOutTime(String plLogOutTime) {
  		this.plLogOutTime = plLogOutTime;
  	}
  	
  	public String getFkPortal() {
  		return fkPortal;
  	}

  	public void setFkPortal(String fkPortal) {
  		this.fkPortal = fkPortal;
  	}
	  	
  	public String getCreator() {
  		return creator;
  	}

  	public void setCreator(String creator) {
  		this.creator = creator;
  	}	
  	  	
    public String getModifiedBy() {
  		return modifiedBy;
  	}

  	public void setModifiedBy(String modifiedBy) {
  		this.modifiedBy = modifiedBy;
  	}	
  	
  	public String getIpAdd() {
  		return ipAdd;
  	}

  	public void setIpAdd(String ipAdd) {
  		this.ipAdd = ipAdd;
  	}	
  	
  	// END OF GETTER SETTER METHODS
	

	/**
     * Defines an patLoginJB object with the specified Id
     */
    public PatLoginJB(int id) {
        setId(id);
    }

    /**
     * default constructor
     */
    public PatLoginJB() {
        Rlog.debug("patLogin", "PatLoginJB.PatLoginJB() ");
    };

    /**
     * Defines an PatLoginJB object with the specified values for the fields
     * 
     */
     /** Full Argument constructor*/
    
    /** Full Argument constructor */
	public void PatloginJB(int id, String plId, String plIdType, String plLogin, String plPassword, String plStatus, String plLogOutTime, String fkPortal, String plCreator, String plModifiedBy, String plIpAdd )
	{
		this.id = id;
		this.plId = plId;
		this.plIdType = plIdType;
		this.plLogin = plLogin;
		this.plPassword = plPassword;
		this.plStatus = plStatus;
		this.plLogOutTime = plLogOutTime;
		this.fkPortal = fkPortal;
		this.creator = plCreator;
		this.modifiedBy = plModifiedBy;
		this.ipAdd = plIpAdd;
		
		
	}
    

    /**
     * Retrieves the patient login details in PatloginStateKeeper Object
     * 
     * @return patLoginBean 
     */
    public PatLoginBean getPatLoginDetails() {
    	PatLoginBean pBean = null;
        try {
        	PatLoginAgentRObj patLoginAgentRObj = EJBUtil.getPatLoginHome();
            pBean = patLoginAgentRObj.getPatLoginDetails(this.id);
            Rlog.debug("PatLogin", "In PatLoginJB.getPatLoginDetails() " + pBean);
        } catch (Exception e) {
            Rlog.fatal("PatLogin", "Error in getPatLoginDetails() in InvoiceJB "
                            + e);
        }
        if (pBean != null) {
        	setId(pBean.getId());
        	setPlId(pBean.getPlId()); 
        	setPlIdType(pBean.getPlIdType()) ;
        	setPlLogin(pBean.getPlLogin()) ;
        	setPlPassword(pBean.getPlPassword()); 
        	setPlStatus(pBean.getPlStatus()) ;
        	setPlLogOutTime(pBean.getPlLogOutTime()) ;
        	setFkPortal(pBean.getFkPortal());
        	setCreator(pBean.getCreator());
            setModifiedBy(pBean.getModifiedBy());
            setIpAdd(pBean.getIpAdd());
        }
        return pBean;
    }

    public void setPatLoginDetails() {
        int output = 0;
        try {
        	
        	PatLoginAgentRObj patLoginAgentRObj = EJBUtil.getPatLoginHome();
            output = patLoginAgentRObj.setPatLoginDetails(this.createPatLogInBean());
            this.setId(output);
            Rlog.debug("PatLogin", "PatLoginJB.setPatLoginDetails()");
            
          
            
        } catch (Exception e) {
            Rlog.fatal("PatLogin", "Error in setPatLoginDetails() in patLoginJB "
                            + e);
            e.printStackTrace();
        }
    }

    /**
     * Saves the modified details of the patLogin
     * 
     * @return 0 - If update succeeds <BR>
     *         -2 - If update fails
     */
    public int updatePatLogin() {
        int output;
        try {
        	PatLoginAgentRObj patLoginAgentRObj = EJBUtil.getPatLoginHome();
            output = patLoginAgentRObj.updatePatLogin(this.createPatLogInBean());
                      
          
        } catch (Exception e) {
            Rlog.fatal("PatLogin",    "EXCEPTION IN updatePatLogin in  PatLogin BEAN"
                            + e.getMessage());

            return -2;
        }
        return output;
    }

    /**
     * Notify Patient on change of LoginId or Password
     * 
     * 
     *
     */
    public void notifyPatient(String patientPK,String loginName,String password,String user,String portalPK) {
        try {
        	PatLoginAgentRObj patLoginAgentRObj = EJBUtil.getPatLoginHome();
        	patLoginAgentRObj.notifyPatient(patientPK, loginName, password, user, portalPK);
                      
        } catch (Exception e) {
            Rlog.fatal("PatLogin",    "EXCEPTION IN updatePatLogin in  PatLogin BEAN"
                            + e.getMessage());
        }
    }
    
    /**
     * From JB to Bean
     * Uses the values of the current PatLoginJB object to create an
     * PatLogInBean Object
     * 
     * @return PatLogInBean object 
      */
    public PatLoginBean createPatLogInBean() {
        Rlog.debug("PatLogin", "PatLoginJB.createPatLogInBean ");
        
        return new PatLoginBean(id, plId, plIdType, plLogin, (plPassword), plStatus, plLogOutTime, fkPortal, creator, modifiedBy, ipAdd  );
    }

     /**
     * Delete patLogin data
     * 
     * @return 0 - If update succeeds <BR>
     *         -2 - If update fails
     */
    public int deletePatLogin() {
        int output;
        try {
        	PatLoginAgentRObj patLoginAgentRObj = EJBUtil.getPatLoginHome();
            output = patLoginAgentRObj.deletePatLogin(this.id);
        } catch (Exception e) {
            Rlog.fatal("PatLogin",
                    "EXCEPTION IN deletePatLogin"      + e.getMessage());

            return -2;
        }
        return output;
    }
    
        
    public int validateLogin(String loginName) {   
    
    	PatLoginBean pBean = null;         
        int success = 0;

        try {

        	PatLoginAgentRObj patLoginAgentRObj = EJBUtil.getPatLoginHome();

        	pBean = patLoginAgentRObj.validateLogin(loginName);

            Rlog.debug("patlogin", "PatLoginJB.validateLogin()");

        } catch (Exception e) {
            Rlog.fatal("patlogin", "Error in validateLogin()() in PatLoginJB " + e);
            e.printStackTrace();
        }
        if (pBean != null) {
            
            this.id = pBean.getId();
    		this.plId = pBean.getPlId(); 
    		this.plIdType = pBean.getPlIdType();
    		this.plLogin = pBean.getPlLogin();    		    		
    		this.plPassword = pBean.getPlPassword(); 
    		this.plStatus = pBean.getPlStatus();
    		this.plLogOutTime = pBean.getPlLogOutTime();
    		this.fkPortal = pBean.getFkPortal();
    		
    		this.creator = pBean.getCreator();
    		this.modifiedBy = pBean.getModifiedBy();
    		this.ipAdd = pBean.getIpAdd();
    		

        } else {
            success = -1;
        }
        return success;
    }
    
    
   
    public int validateLoginPassword(String loginName,String password,String ipAdd) {   
        
    	PatLoginBean pBean = null;         
        int success = 0;
        int activeStatus = 0;
        //get code for 'Active' login status
        CodeDao cd = new CodeDao();
        activeStatus = cd.getCodeId("patLoginStat","A");

        try {

        	PatLoginAgentRObj patLoginAgentRObj = EJBUtil.getPatLoginHome();

        	pBean = patLoginAgentRObj.validateLoginPassword(loginName,Security.encrypt(password),ipAdd);

            Rlog.debug("patlogin", "PatLoginJB.validateLoginPassword");

        } catch (Exception e) {
            Rlog.fatal("patlogin", "Exception in validateLoginPassword() in PatLoginJB " + e);
            e.printStackTrace();
            pBean = null;
        }

        if (pBean == null) {
        	try {
        		Rlog.debug("patlogin", "PatLoginJB.validateLoginPassword Second try");

        		PatLoginAgentRObj patLoginAgentRObj = EJBUtil.getPatLoginHome();

        		pBean = patLoginAgentRObj.validateLoginPassword(loginName,Security.encryptSHA(password),ipAdd);

        		Rlog.debug("patlogin", "PatLoginJB.validateLoginPassword");

        	} catch (Exception ex) {
        		Rlog.fatal("patlogin", "Exception in validateLoginPassword() in PatLoginJB " + ex);
        		ex.printStackTrace();
        	}
        }
          
        //added check if the login is Active
        if (pBean != null && EJBUtil.stringToNum(pBean.getPlStatus()) == activeStatus) {
            
            this.id = pBean.getId();
    		this.plId = pBean.getPlId(); 
    		this.plIdType = pBean.getPlIdType();
    		this.plLogin = pBean.getPlLogin();    		    		
    		this.plPassword = pBean.getPlPassword(); 
    		this.plStatus = pBean.getPlStatus();
    		this.plLogOutTime = pBean.getPlLogOutTime();
    		this.fkPortal = pBean.getFkPortal();
    		
    		this.creator = pBean.getCreator();
    		this.modifiedBy = pBean.getModifiedBy();
    		this.ipAdd = pBean.getIpAdd();
    		setIsAutoLogin(pBean.getIsAutoLogin());

        } else {
            success = -1;
        }
        return success;
    }
      
    
    public boolean getIsAutoLogin() {
		return isAutoLogin;
	}
	public void setIsAutoLogin(boolean isAutoLogin) {
		this.isAutoLogin = isAutoLogin;
	}
	
	
}
