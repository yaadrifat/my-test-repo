/*
 * Classname : LabJB
 *
 * Version information: 1.0
 *
 * Date: 09/28/2003
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Vishal Abrol
 */

package com.velos.eres.web.lab;

/* IMPORT STATEMENTS */

import java.util.Hashtable;
import com.velos.eres.business.common.LabDao;
import com.velos.eres.business.lab.impl.LabBean;
import com.velos.eres.service.labAgent.LabAgentRObj;
import com.velos.eres.service.labAgent.impl.LabAgentBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for lab
 *
 * @author Vishal Abrol
 * @version 1.0 09/28/2003
 */

public class LabJB {

    /**
     * The primary key:pk_patlabs
     */

    private int id;

    /**
     * The foreign key reference to labs lookup table.
     */
    private String testId;

    /**
     * The foreign key reference to table person
     */
    private String perId;

    /**
     * Test Date
     */
    private String testDate;

    /**
     * Test Result
     */

    private String testResult;

    /**
     * The Test Type
     */
    private String testType;

    /**
     * The Test unit
     */
    private String testUnit;

    /**
     * The Test Range
     */
    private String testRange;

    /**
     * The Test LLN
     */
    private String testLLN;

    /**
     * The Test ULN
     */
    private String testULN;

    /**
     * The Foreing key for er_site
     */

    private String siteId;

    /**
     * The Test Status
     */

    private String testStatus;

    /**
     * The Accession number
     */

    private String accnNum;

    /**
     * The TestGroup
     */

    private String testgroupId;

    /**
     * The TestGroup
     */

    private String abnrFlag;

    /**
     * The foreign key for table er_patprot
     */

    private String patprotId;

    /**
     * The foreign key for table sch_events
     */

    private String eventId;

    /**
     * The creator
     */

    private String creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    private String modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    private String ipAdd;

    private String erName;

    private String erGrade;

    private String stdPhase;

    private String studyId;

  //JM: 07Jul2009: #INVP2.11
    private String specimenId;

    // /////////////////////////////////////////////////////////////////////////////////

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public String getTestId() {
        return testId;
    }

    public void setPerId(String perId) {
        this.perId = perId;
    }

    public String getPerId() {
        return perId;
    }

    public String getTestDate() {
        return testDate;
    }

    public String getTestResult() {
        return testResult;
    }

    public String getTestType() {
        return testType;
    }

    public String getTestUnit() {
        return testUnit;
    }

    public String getTestRange() {
        return testRange;
    }

    public String getTestLLN() {
        return testLLN;
    }

    public String getTestULN() {
        return testULN;
    }

    public String getSiteId() {
        return siteId;
    }

    public String getTestStatus() {
        return testStatus;
    }

    public String getAccnNum() {
        return accnNum;
    }

    public String getTestgroupId() {
        return testgroupId;
    }

    public String getAbnrFlag() {
        return abnrFlag;
    }

    public String getPatprotId() {
        return patprotId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setTestDate(String testDate) {
        this.testDate = testDate;
    }

    public void setTestResult(String testResult) {
        this.testResult = testResult;
    }

    public void setTestType(String testType) {
        this.testType = testType;
    }

    public void setTestUnit(String testUnit) {
        this.testUnit = testUnit;
    }

    public void setTestRange(String testRange) {
        this.testRange = testRange;
    }

    public void setTestLLN(String testLLN) {
        this.testLLN = testLLN;
    }

    public void setTestULN(String testULN) {
        this.testULN = testULN;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public void setTestStatus(String testStatus) {
        this.testStatus = testStatus;
    }

    public void setAccnNum(String accnNum) {
        this.accnNum = accnNum;
    }

    public void setTestgroupId(String testgroupId) {
        this.testgroupId = testgroupId;
    }

    public void setAbnrFlag(String abnrFlag) {
        this.abnrFlag = abnrFlag;
    }

    public void setPatprotId(String patprotId) {
        this.patprotId = patprotId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    /**
     * @return the Creator of the Form Field is returned
     *
     */

    public String getCreator() {
        return this.creator;
    }

    /**
     * @param takes
     *            a string
     */

    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     *
     *
     * @return modifiedBy
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param the
     *            set unique ID of the User
     */

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return the IP Address of the Form Notification request
     */

    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param set
     *            the IP Address of the Form Notification request
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    public void setErName(String erName) {
        this.erName = erName;
    }

    public void setErGrade(String erGrade) {
        this.erGrade = erGrade;
    }

    public String getErName() {
        return erName;
    }

    public String getErGrade() {
        return erGrade;
    }

    public void setStudyId(String studyId) {
        this.studyId = studyId;
    }

    public void setStdPhase(String stdPhase) {
        this.stdPhase = stdPhase;
    }

    public String getStudyId() {
        return studyId;
    }

    public String getStdPhase() {
        return stdPhase;
    }


  //JM: 07Jul2009: #INVP2.11

    public String getSpecimenId() {
        return specimenId;
    }

    public void setSpecimenId(String specimenId) {
    	this.specimenId = specimenId;
    }




    // //////////////////////////////////////////////////////////////////

    // END OF THE GETTER AND SETTER METHODS

    /**
     * Constructor
     *
     * @param lab
     *            Id: Unique Id constructor
     *
     */

    public LabJB(int id) {
        setId(id);
    }

    /**
     * Default Constructor
     */

    public LabJB() {
        Rlog.debug("Lab", "LabJB.LabJB() ");
    }

    public LabJB(int id, String testId, String perId, String testDate,
            String testResult, String testType, String testUnit,
            String testRange, String testLLN, String testULN, String siteId,
            String testStatus, String accnNum, String testgroupId,
            String abnrFlag, String patprotId, String eventId, String creator,
            String modifiedBy, String ipAdd, String erName, String erGrade,
            String stdPhase, String studyId, String specimenId) {
        setId(id);
        setTestId(testId);
        setPerId(perId);
        setTestDate(testDate);
        setTestResult(testResult);
        setTestType(testType);
        setTestUnit(testUnit);
        setTestRange(testRange);
        setTestLLN(testLLN);
        setTestULN(testULN);
        setSiteId(siteId);
        setTestStatus(testStatus);
        setAccnNum(accnNum);
        setTestgroupId(testgroupId);
        setAbnrFlag(abnrFlag);
        setPatprotId(patprotId);
        setEventId(eventId);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setErName(erName);
        setErGrade(erGrade);
        setStdPhase(stdPhase);
        setStudyId(studyId);
        setSpecimenId(specimenId);
    }

    /**
     * Calls getLabDetails of lab Session Bean: LabAgentBean
     *
     * @return LabStatKeeper
     * @see LabAgentBean
     */

    public LabBean getLabDetails() {
        LabBean labsk = null;
        try {
            LabAgentRObj labAgentRObj = EJBUtil.getLabAgentHome();
            labsk = labAgentRObj.getLabDetails(this.id);
            Rlog.debug("lab", "LabJB.getLabDetails() LabStateKeeper " + labsk);
        } catch (Exception e) {
            Rlog.fatal("lab", "Error in Lab getLabDetails" + e);
        }

        if (labsk != null) {
            setId(labsk.getId());
            setTestId(labsk.getTestId());
            setPerId(labsk.getPerId());
            setTestDate(labsk.getTestDate());
            setTestResult(labsk.getTestResult());
            setTestType(labsk.getTestType());
            setTestUnit(labsk.getTestUnit());
            setTestRange(labsk.getTestRange());
            setTestLLN(labsk.getTestLLN());
            setTestULN(labsk.getTestULN());
            setSiteId(labsk.getSiteId());
            setTestStatus(labsk.getTestStatus());
            setAccnNum(labsk.getAccnNum());
            setTestgroupId(labsk.getTestgroupId());
            setAbnrFlag(labsk.getAbnrFlag());
            setPatprotId(labsk.getPatprotId());
            setEventId(labsk.getEventId());
            setCreator(labsk.getCreator());
            setModifiedBy(labsk.getModifiedBy());
            setIpAdd(labsk.getIpAdd());
            setErName(labsk.getErName());
            setErGrade(labsk.getErGrade());
            setStdPhase(labsk.getStdPhase());
            setStudyId(labsk.getStudyId());
            setSpecimenId(labsk.getSpecimenId());


        }

        return labsk;

    }

    /**
     * Calls setLabDetails() of LabId Session Bean: LabAgentBean
     *
     */

    public int setLabDetails() {
        int toReturn;
        try {

            LabAgentRObj labAgentRObj = EJBUtil.getLabAgentHome();

            LabBean tempStateKeeper = new LabBean();

            tempStateKeeper = this.createLabStateKeeper();

            toReturn = labAgentRObj.setLabDetails(tempStateKeeper);
            Rlog.debug("lab", "LabJB.setLabDetails() Outta thewre2"
                    + labAgentRObj + "statekeeper" + tempStateKeeper);
            this.setId(toReturn);
            Rlog.debug("lab", "LabJB.setLabDetails() Outta thewre3");
            Rlog.debug("lab", "LabJB.setLabDetails()");

            return toReturn;

        } catch (Exception e) {
            Rlog.fatal("lab", "Error in setLabDetails() in labJB " + e);
            return -2;
        }
    }

    /**
     *
     * @return a statekeeper object for the labId Record with the current values
     *         of the bean
     */
    public LabBean createLabStateKeeper() {

        return new LabBean(id, testId, perId, testDate, testResult, testType,
                testUnit, testRange, testLLN, testULN, siteId, testStatus,
                accnNum, testgroupId, abnrFlag, patprotId, eventId, creator,
                modifiedBy, ipAdd, erName, erGrade, stdPhase, studyId, specimenId);

    }

    /**
     * Calls updateLab() of Lab Session Bean: LabAgentBean
     *
     * @return The status as an integer
     */
    public int updateLab() {
        int output;
        try {

            LabAgentRObj labAgentRObj = EJBUtil.getLabAgentHome();
            output = labAgentRObj.updateLab(this.createLabStateKeeper());
        } catch (Exception e) {
            Rlog.debug("lab",
                    "EXCEPTION IN SETTING lab DETAILS TO  SESSION BEAN" + e);
            return -2;
        }
        return output;
    }

    public int removeLab() {

        int output;

        try {

            LabAgentRObj labAgentRObj = EJBUtil.getLabAgentHome();
            output = labAgentRObj.removeLab(this.id);
            return output;

        } catch (Exception e) {
            Rlog.fatal("lab", "in LabJB -removeLab() method" + e);
            return -1;
        }

    }

    public int updateClobData(int labId, String result, String notes) {

        int ret;
        try {

            LabAgentRObj labAgentRObj = EJBUtil.getLabAgentHome();
            Rlog.debug("lab", "LabJB.updateClobData after remote");
            ret = labAgentRObj.updateClobData(labId, result, notes);
            Rlog.debug("lab", "LabJB.updateClobData after Dao");
        } catch (Exception e) {
            Rlog.fatal("Lab", "Error in LabJB:updateClobData " + e);
            return -2;
        }
        return ret;
    }

    public LabDao getData(String groupId) {
        LabDao labDao = new LabDao();
        try {

            LabAgentRObj labAgentRObj = EJBUtil.getLabAgentHome();
            Rlog.debug("lab", "LabJB.getData(groupId) after remote");
            labDao = labAgentRObj.getData(groupId);
            Rlog.debug("lab", "LabJB.getData(groupId) after Dao");
            return labDao;
        } catch (Exception e) {
            Rlog.fatal("Lab", "Error in LabJB:getData(groupId) " + e);
        }
        return labDao;
    }

    public LabDao getData() {
        LabDao labDao = new LabDao();
        try {

            LabAgentRObj labAgentRObj = EJBUtil.getLabAgentHome();
            Rlog.debug("lab", "LabJB.getData(groupId) after remote");
            labDao = labAgentRObj.getData();
            Rlog.debug("lab", "LabJB.getData() after Dao");
            return labDao;
        } catch (Exception e) {
            Rlog.fatal("Lab", "Error in LabJB:getData() " + e);
        }
        return labDao;

    }

    public LabDao groupPullDown(String groupId) {
        LabDao labDao = new LabDao();
        try {

            LabAgentRObj labAgentRObj = EJBUtil.getLabAgentHome();
            Rlog.debug("lab", "LabJB.groupPullDown() after remote");
            labDao = labAgentRObj.groupPullDown(groupId);
            Rlog.debug("lab", "LabJB.groupPullDown() after Dao");
            return labDao;
        } catch (Exception e) {
            Rlog.fatal("Lab", "Error in LabJB:groupPullDown() " + e);
        }
        return labDao;
    }

    public int insertLab(LabDao labdao) {

        try {
            Rlog.debug("lab", "LabJB:insertLab starting");

            LabAgentRObj labAgentRObj = EJBUtil.getLabAgentHome();
            labAgentRObj.insertLab(labdao);
        } catch (Exception e) {
            Rlog.fatal("lab", "LabJB:InsertLab-Error " + e);
            return -2;
        }
        return 1;
    }

    public int deleteLab(String[] deleteIds, String tableName, String colName,
            String type) {
        int ret = 0;
        try {
            Rlog.debug("lab", "LabJB:deleteLab starting");

            LabAgentRObj labAgentRObj = EJBUtil.getLabAgentHome();
            labAgentRObj.deleteLab(deleteIds, tableName, colName, type);

        } catch (Exception e) {
            Rlog.fatal("lab", "LabJB:deleteLab-Error " + e);
            return -1;
        }
        return ret;

    }
    
    // Overloaded for INF-18183 ::: AGodara
    public int deleteLab(String[] deleteIds, String tableName, String colName,
            String type,Hashtable<String, String> auditInfo) {
        int ret = 0;
        try {
            LabAgentRObj labAgentRObj = EJBUtil.getLabAgentHome();
            labAgentRObj.deleteLab(deleteIds, tableName, colName, type,auditInfo);
        } catch (Exception e) {
            Rlog.fatal("lab", "LabJB:deleteLab-Error " + e);
            return -1;
        }
        return ret;
    }

    public LabDao retLabUnit(String[] testIds) {
        LabDao labDao = new LabDao();
        try {
            Rlog.debug("lab", "LabJB:retLabUnit starting");

            LabAgentRObj labAgentRObj = EJBUtil.getLabAgentHome();
            labDao = labAgentRObj.retLabUnit(testIds);
            return labDao;
        } catch (Exception e) {

            Rlog.fatal("lab", "LabJB:retLabUnit-Error " + e);

        }
        return labDao;
    }

    public LabDao retLabUnit(String testId) {
        LabDao labDao = new LabDao();
        try {
            Rlog.debug("lab", "LabJB:retLabUnit starting");

            LabAgentRObj labAgentRObj = EJBUtil.getLabAgentHome();
            labDao = labAgentRObj.retLabUnit(testId);
            return labDao;
        } catch (Exception e) {

            Rlog.fatal("lab", "LabJB:retLabUnit-Error " + e);

        }
        return labDao;
    }

    public LabDao getClobData(String testId) {
        LabDao labDao = new LabDao();
        try {
            Rlog.debug("lab", "LabJB:getClobData starting");

            LabAgentRObj labAgentRObj = EJBUtil.getLabAgentHome();
            labDao = labAgentRObj.getClobData(testId);
            return labDao;
        } catch (Exception e) {

            Rlog.fatal("lab", "LabJB:getClobData-Error " + e);

        }
        return labDao;

    }
}
