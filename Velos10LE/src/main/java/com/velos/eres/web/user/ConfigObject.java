/*
 * Classname : ConfigObject
 * 
 * Version information: 1.0
 *
 * Date: 02/17/2005
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Gopu
 */

package com.velos.eres.web.user;

import java.util.ArrayList; 

/**
 * Client Side Object for ConfigObject
 */

public class ConfigObject {
    /**
     * The primary key reference :Stores the PK of the respective module
     */

    public ArrayList primaryKey;
    
    
//  JM: 051806 To bring into consideration the ACCOUNT ID    
    /**
     * Stores the account id
     */
    public ArrayList accountID;
    

    /**
     * The PageName : Stores the respective module
     */

    public ArrayList pageName;
    

    public ArrayList getPrimaryKey() {
        return this.primaryKey;
    }

    public void setPrimaryKey(ArrayList primaryKey) {
        this.primaryKey = primaryKey;
    }
    
//JM: 03Jul2006
    public ArrayList getAccountId() {
        return this.accountID ;
    }

    public void setAccountId(ArrayList accountID) {
        this.accountID = accountID;
    }  
    
    
    

    public ArrayList getPageName() {
        return this.pageName;
    }

    public void setPageName(ArrayList pageName) {
        this.pageName = pageName;
    }
}
