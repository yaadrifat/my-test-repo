/*
 * Classname : FormSecJB
 * 
 * Version information: 1.0
 *
 * Date: 18/07/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sonia Kaura
 */

package com.velos.eres.web.formSec;

/* IMPORT STATEMENTS */
import java.util.Hashtable;

import com.velos.eres.business.common.FormSecDao;
import com.velos.eres.business.formSec.impl.FormSecBean;
import com.velos.eres.service.formSecAgent.FormSecAgentRObj;
import com.velos.eres.service.formSecAgent.impl.FormSecAgentBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for Form Sec
 * 
 * @author Sonia Kaura
 * @version 1.0 24/06/2003
 */

public class FormSecJB {

    /**
     * The form primary key:pk_formsec
     */

    private int formSecId;

    /**
     * The foreign key reference from the Field : FK_FORMLIB
     */
    private String formLibId;

    /**
     * The Name of the Section: FORMSEC_NAME
     */

    private String formSecName;

    /**
     * The Sequence number of the Section: FORMSEC_SEQ
     */
    private String formSecSeq;

    /**
     * Stores section format - tabular / non-tabular fields: FORMSEC_FMT
     * 
     */

    private String formSecFmt;

    /**
     * No. of times fields will be repeated within the section: FORMSEC_REPNO
     * 
     */
    private String formSecRepNo;

    /**
     * 
     * /** This column stores the record type setting: N - New M - Modified D -
     * Deleted : RECORD_TYPE
     */

    private String recordType;

    /**
     * 
     * The ID of the Creator is sotred : CREATOR
     */

    private String creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    private String modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    private String ipAdd;

    private String offlineFlag;

    // /////////////////////////////////////////////////////////////////////////////////

    // GETTER AND SETTER METHODS

    /**
     * 
     * 
     * @return formSecId
     */
    public int getFormSecId() {
        return formSecId;
    }

    /**
     * 
     * 
     * @param formSecId
     */
    public void setFormSecId(int formSecId) {
        this.formSecId = formSecId;
    }

    /**
     * 
     * 
     * @return formLibId
     */
    public String getFormLibId() {
        return formLibId;

    }

    /**
     * 
     * 
     * @param formLibId
     */
    public void setFormLibId(String formLibId) {

        this.formLibId = formLibId;
    }

    /**
     * 
     * 
     * @return formSecName
     */
    public String getFormSecName() {

        return formSecName;

    }

    /**
     * 
     * 
     * @param formSecName
     */
    public void setFormSecName(String formSecName) {

        this.formSecName = formSecName;

    }

    /**
     * 
     * 
     * @return formSecSeq
     */
    public String getFormSecSeq() {

        return formSecSeq;

    }

    /**
     * 
     * 
     * @param formSecSeq
     */
    public void setFormSecSeq(String formSecSeq) {

        this.formSecSeq = formSecSeq;
    }

    /**
     * 
     * 
     * @return formSecFmt
     */
    public String getFormSecFmt() {

        return formSecFmt;

    }

    /**
     * 
     * 
     * @param formSecFmt
     */
    public void setFormSecFmt(String formSecFmt) {

        this.formSecFmt = formSecFmt;

    }

    /**
     * 
     * 
     * @return formSecRepNo
     */
    public String getFormSecRepNo() {

        return formSecRepNo;
    }

    /**
     * 
     * 
     * @param formSecRepNo
     */
    public void setFormSecRepNo(String formSecRepNo) {
        this.formSecRepNo = formSecRepNo;

    }

    /**
     * @return The Creator of the Form Section
     */

    public String getCreator() {
        return this.creator;
    }

    /**
     * 
     * @param to
     *            set the Creator of the Form Section
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return the Record Type : m: modified n:new d: delet
     */
    public String getRecordType() {
        return this.recordType;
    }

    /**
     * @param set
     *            the Record Type to be modified,new or delete
     */
    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param the
     *            set unique ID of the User
     */

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return the IP Address of the Form Section request
     */

    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param set
     *            the IP Address of the Form Section request
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * @return the offline Flag
     */

    public String getOfflineFlag() {
        return this.offlineFlag;
    }

    /**
     * @param set
     *            the offline Flag
     */

    public void setOfflineFlag(String offlineFlag) {
        this.offlineFlag = offlineFlag;
    }

    // //////////////////////////////////////////////////////////////////

    // END OF THE GETTER AND SETTER METHODS

    /**
     * Constructor
     * 
     * @param formSecId:
     *            Unique Id constructor
     * 
     */

    public FormSecJB(int formSecId) {
        setFormSecId(formSecId);
    }

    /**
     * Default Constructor
     */

    public FormSecJB() {
        Rlog.debug("formsection", "FormSecJB.FormNotifyJB() ");
    }

    public FormSecJB(int formSecId, String formLibId, String formSecName,
            String formSecSeq, String formSecFmt, String formSecRepNo,
            String recordType, String creator, String modifiedBy, String ipAdd) {

        setFormSecId(formSecId);
        setFormLibId(formLibId);
        setFormSecName(formSecName);
        setFormSecSeq(formSecSeq);
        setFormSecFmt(formSecFmt);
        setFormSecRepNo(formSecRepNo);
        setRecordType(recordType);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);

    }

    /**
     * Calls getFormSecDetails of FormSec Session Bean: FormSecAgentBean
     * 
     * @return FormSecStatKeeper
     * @see FormSecAgentBean
     */

    public FormSecBean getFormSecDetails() {
        FormSecBean formSecsk = null;
        try {
            FormSecAgentRObj formSecAgentRObj = EJBUtil.getFormSecAgentHome();

            formSecsk = formSecAgentRObj.getFormSecDetails(this.formSecId);
            Rlog.debug("formsection",
                    "FormSecJB.getFormSecDetails() FormSecStateKeeper "
                            + formSecsk);
        } catch (Exception e) {
            Rlog.fatal("formsection", "Error in FormSec getFormSecDetails" + e);
        }

        if (formSecsk != null) {

            this.formSecId = formSecsk.getFormSecId();
            this.formLibId = formSecsk.getFormLibId();
            this.formSecName = formSecsk.getFormSecName();
            this.formSecSeq = formSecsk.getFormSecSeq();
            this.formSecFmt = formSecsk.getFormSecFmt();
            this.formSecRepNo = formSecsk.getFormSecRepNo();
            this.recordType = formSecsk.getRecordType();
            this.creator = formSecsk.getCreator();
            this.modifiedBy = formSecsk.getModifiedBy();
            this.ipAdd = formSecsk.getIpAdd();
            this.offlineFlag = formSecsk.getOfflineFlag();

        }

        return formSecsk;

    }

    /**
     * Calls setFormSecDetails() of Form Section Session Bean: FormSecAgentBean
     * 
     */

    public int setFormSecDetails() {
        int toReturn;
        try {

            FormSecAgentRObj formSecAgentRObj = EJBUtil.getFormSecAgentHome();

            FormSecBean tempStateKeeper = new FormSecBean();
            tempStateKeeper = this.createFormSecStateKeeper();
            toReturn = formSecAgentRObj.setFormSecDetails(tempStateKeeper);
            this.setFormSecId(toReturn);

            Rlog.debug("formsection", "FormSecJB.setFormSecDetails()");

            return toReturn;

        } catch (Exception e) {
            Rlog.fatal("formsection",
                    "Error in setFormSecDetails() in FormSecJB " + e);
            return -2;
        }
    }

    /**
     * 
     * @return a statekeeper object for the FormSec Record with the current
     *         values of the bean
     */
    public FormSecBean createFormSecStateKeeper() {
        Rlog.debug("formsection", "FormSecJB.createFormSecStateKeeper ");

        return new FormSecBean(formSecId, formLibId, formSecName, formSecSeq,
                formSecFmt, formSecRepNo, recordType, creator, modifiedBy,
                ipAdd, offlineFlag);

    }

    /**
     * Calls updateFormSec() of FormSec Session Bean: FormSecAgentBean
     * 
     * @return The status as an integer
     */
    public int updateFormSec() {
        int output;
        try {
            FormSecAgentRObj formSecAgentRObj = EJBUtil.getFormSecAgentHome();

            output = formSecAgentRObj.updateFormSec(this
                    .createFormSecStateKeeper());
        } catch (Exception e) {
            Rlog.debug("formsection",
                    "EXCEPTION IN SETTING formsection DETAILS TO  SESSION BEAN"
                            + e);
            return -2;
        }
        return output;
    }
    
    //Overloaded for INF-18183 ::: Ankit
    public int updateFormSec(Hashtable<String, String> auditInfo) {
        int output;
        try {
            FormSecAgentRObj formSecAgentRObj = EJBUtil.getFormSecAgentHome();

            output = formSecAgentRObj.updateFormSec(this
                    .createFormSecStateKeeper(),auditInfo);
        } catch (Exception e) {
            Rlog.debug("formsection",
                    "EXCEPTION IN SETTING formsection DETAILS TO  SESSION BEAN"
                            + e);
            return -2;
        }
        return output;
    }

    public int removeFormSec() {

        int output;

        try {

            FormSecAgentRObj formSecAgentRObj = EJBUtil.getFormSecAgentHome();
            output = formSecAgentRObj.removeFormSec(this.formSecId);
            return output;

        } catch (Exception e) {
            Rlog
                    .fatal("formsection",
                            "in FormSecJB -removeFormNotify() method");
            return -1;
        }

    }

    /**
     * Method to add the multiple sections to the ER_FORMSEC tabel
     * 
     * @param FormSecDao
     * 
     */

    public int insertNewSections(FormSecDao formSecDao) {

        int ret = 0;
        String formId = "";
        int retVal = 0;
        int formLibId = 0;

        try {

            Rlog.debug("formsection", "FormSecJB.insertNewSections starting");
            FormSecAgentRObj formSecAgentRObj = EJBUtil.getFormSecAgentHome();

            ret = formSecAgentRObj.insertNewSections(formSecDao);

            Rlog.debug("formsection", "FormSecJB. &&&ret$$" + ret);
            formLibId = EJBUtil
                    .stringToNum((String) formSecDao.getFormLibId(0));
            Rlog.debug("formsection", "FormSecJB. &&&FORM$$" + formLibId);

            // This is done through the Triggers now on ER_FORMSEC to avoid
            // extra processing
            /*
             * FormLibJB formJB = new FormLibJB (); formId=
             * formSecDao.getFormLibId(0).toString();
             * 
             * 
             * formJB.setFormLibId(Integer.parseInt(formId));
             * Rlog.debug("formsection","FormSecJB. &&&FORM$$"+
             * formJB.getFormLibId()); formJB.getFormLibDetails();
             * Rlog.debug("formsection","AFTER GETTING FORM DETAILS 1##");
             * formJB.setFormLibXslRefresh("1"); Rlog.debug("formsection","AFTER
             * SETTING REFRESH FLAG 2##"); Rlog.debug("formsection","FormSecJB.
             * RECORD TYPE 111=="+ formSecDao.getRecordType(0).toString());
             * formJB.setRecordType(formSecDao.getRecordType(0).toString());
             * 
             * Rlog.debug("formsection","FormSecJB. CREATOR
             * 222==="+formSecDao.getCreator(0).toString()); formJB.setCreator(
             * formSecDao.getCreator(0).toString()) ;
             * 
             * Rlog.debug("formsection","FormSecJB. IPADD 333=="+
             * formSecDao.getIpAdd(0).toString()); formJB.setIpAdd(
             * formSecDao.getIpAdd(0).toString()) ; formJB.updateFormLib();
             */

            if (ret > 0) {

                Rlog.debug("formsection", "FormSecJB. before updateSectionSeq");
                retVal = formSecDao.updateSectionSeq(formLibId);
                Rlog.debug("formsection", "FormSecJB. retVal" + retVal);

            }
            return retVal;

        } catch (Exception e) {
            Rlog.fatal("formsection", "insertNewSections() in FormSecJB " + e);
            return -2;
        }

    }

    /**
     * 
     * 
     * @param formLibId
     * @return FormSecDao
     */
    public FormSecDao getAllSectionsOfAForm(int formLibId) {
        FormSecDao formSecDao = new FormSecDao();
        try {
            Rlog.debug("formsection", "FormSecJB.getAllCategories starting");
            FormSecAgentRObj formSecAgentRObj = EJBUtil.getFormSecAgentHome();

            formSecDao = formSecAgentRObj.getAllSectionsOfAForm(formLibId);

            return formSecDao;
        } catch (Exception e) {
            Rlog.fatal("formsection",
                    "getAllSectionsOfAForm(int formLibId) in FormSecJB " + e);
        }
        return formSecDao;
    }

    /**
     * Method to update multiple sections to the ER_FORMSEC tabel
     * 
     * @param FormSecDao
     * 
     */

    public int updateNewSections(FormSecDao formSecDao) {

        // FormSecDao formSecDao = new FormSecDao();
        // ArrayList formSecIds= null;
        int ret = 0;
        int retval = 0;
        String formId = "";
        int retvalue = 0;
        int formLibId = 0;
        try {
            Rlog.debug("formsection", "FormSecJB.updateNewSections starting");
            FormSecAgentRObj formSecAgentRObj = EJBUtil.getFormSecAgentHome();

            ret = formSecAgentRObj.updateNewSections(formSecDao);
            formLibId = EJBUtil
                    .stringToNum((String) formSecDao.getFormLibId(0));
            Rlog.debug("formsection", "FormSecJB. &&&FORM$$" + formLibId);
            // formSecIds = formSecDao.getFormSecRepNo(0).toString();;
            // Rlog.debug("formsection","formsecids in JB"+formSecIds);

            // This is done through the Triggers now on ER_FORMSEC to avoid
            // extra processing
            /*
             * Rlog.debug("formsection","FormSecJB.
             * &&&FORM$$"+formSecDao.getFormLibId(0).toString()); FormLibJB
             * formJB = new FormLibJB (); formId=
             * formSecDao.getFormLibId(0).toString();
             * 
             * 
             * formJB.setFormLibId(Integer.parseInt(formId));
             * Rlog.debug("formsection","FormSecJB. &&&FORM$$2"+
             * formJB.getFormLibId()); formJB.getFormLibDetails();
             * formJB.setFormLibXslRefresh("1");
             * formJB.setRecordType(formSecDao.getRecordType(0).toString());
             * formJB.setLastModifiedBy(formSecDao.getModifiedBy(0).toString()) ;
             * formJB.setIpAdd( formSecDao.getIpAdd(0).toString()) ;
             * formJB.updateFormLib();
             */
            Rlog.debug("formsection", "ret" + ret);
            if (ret != -2) {
                // Function to set the browserflag of field to be displayed in
                // browser to 0 when the section repeat flag is greater than 0

                retval = formSecDao.updateBrowserFlagForRepFld(formSecDao);
                Rlog.debug("formsection", "retval" + retval);

            }
            if (retvalue >= 0) {
                Rlog.debug("formsection",
                        "FormSecJB. in update before updateSectionSeq");
                retvalue = formSecDao.updateSectionSeq(formLibId);
                Rlog.debug("formsection", "FormSecJB. in update retVal"
                        + retvalue);

            }
            return retvalue;
        }

        catch (Exception e) {
            Rlog.fatal("formsection", "updateNewSections() in FormSecJB " + e);
            return -2;
        }

    }

    public int updateDefaultFormSec(int pkSection, int pkForm, String user,
            String ipAdd) {
        int ret;
        try {
            Rlog
                    .debug("formsection",
                            "FormLibJB.updateDefaultFormSec starting");
            FormSecAgentRObj formSecAgentRObj = EJBUtil.getFormSecAgentHome();

            ret = formSecAgentRObj.updateDefaultFormSec(pkForm, pkSection,
                    user, ipAdd);

        } catch (Exception e) {
            Rlog.fatal("formsection",
                    "Error in updateDefaultFormSec in FormlibJB " + e);
            return -2;
        }
        return ret;
    }

    /*
     * Method to find whether a duplicate section sequence number exists for a
     * newly added section in form
     */
    public FormSecBean findByFormIdAndSeq(int formLibId, int seq) {
        Rlog.debug("formsection", "Error in formsec formLibId" + formLibId);
        Rlog.debug("formsection", "Error in formsec seq" + seq);
        FormSecBean formSecSk = null;
        try {

            FormSecAgentRObj formSecAgentRObj = EJBUtil.getFormSecAgentHome();

            formSecSk = formSecAgentRObj.findByFormIdAndSeq(formLibId, seq);

        } catch (Exception e) {
            Rlog
                    .fatal("formsection", "Error in formsec findByFormIdAndSeq"
                            + e);
        }

        if (formSecSk != null) {
            this.formSecId = formSecSk.getFormSecId();

            this.formSecSeq = formSecSk.getFormSecSeq();

        }

        return formSecSk;

    }

    /**
     * returns true if field as any inter field action associated with the field
     * 
     * @param sectionId
     * 
     *            
     */

    public boolean sectionHasInterFieldActions(String sectionId)
    {
        try {
            Rlog.debug("formsection",
                            "FormLibJB.sectionHasInterFieldActions starting");
            FormSecAgentRObj formSecAgentRObj = EJBUtil.getFormSecAgentHome();

            return formSecAgentRObj.sectionHasInterFieldActions(sectionId);

        } catch (Exception e) {
            Rlog.fatal("formsection",
                    "Error in sectionHasInterFieldActions in FormSecJB " + e);
            return true;
        }

    	
    }
    
}
