package com.velos.eres.web.submission;

import java.util.ArrayList;
import java.util.Date;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.submission.impl.SubmissionStatusBean;
import com.velos.eres.service.submissionAgent.SubmissionStatusAgent;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

public class SubmissionStatusJB {
    private int id;
    private Integer fkSubmission;
    private Integer fkSubmissionBoard;
    private Integer fkSubmissionStatus;
    private Date submissionStatusDate;
    private Integer submissionEnteredBy;
    private Integer submissionAssignedTo;
    private Integer submissionCompletedBy;
    private String submissionNotes;
    private Integer isCurrent;
    private Integer creator;
    private Integer lastModifiedBy;
    private String ipAdd;
    private Integer fkStudyStat;

	public int createSubmissionStatus() {
        int output = 0;
        try {
            SubmissionStatusAgent submissionStatusAgent = EJBUtil.getSubmissionStatusAgentHome();
            SubmissionStatusBean submissionStatusBean = createEntityBean();
            // set old statuses to 0 if any
            submissionStatusAgent.setCurrentToOld(submissionStatusBean);
            // set new status to 1 
            submissionStatusBean.setIsCurrent(new Integer(1));
            output = submissionStatusAgent.createSubmissionStatus(submissionStatusBean);
        } catch(Exception e) {
            Rlog.fatal("submission", "Error in SubmissionStatusJB.createSubmissionStatus "+e);
            output = -1;
        }
        return output;
    }
    
    public int getSubmissionStatusDetails(int id) {
        int output = 0;
        try {
            SubmissionStatusAgent submissionStatusAgent = EJBUtil.getSubmissionStatusAgentHome();
            SubmissionStatusBean submissionStatusBean = 
                submissionStatusAgent.getSubmissionStatusDetails(id);
            if (submissionStatusBean == null) { return -1; }
            output = submissionStatusBean.getId();
            this.id = submissionStatusBean.getId();
            this.submissionNotes = submissionStatusBean.getSubmissionNotes();
            this.fkSubmission = submissionStatusBean.getFkSubmission();
            this.fkSubmissionBoard = submissionStatusBean.getFkSubmissionBoard();
            this.fkSubmissionStatus = submissionStatusBean.getSubmissionStatus();
            this.submissionAssignedTo = submissionStatusBean.getSubmissionAssignedTo();
            this.submissionStatusDate = submissionStatusBean.getSubmissionStatusDate();
            this.submissionEnteredBy = submissionStatusBean.getSubmissionEnteredBy();
            this.isCurrent = submissionStatusBean.getIsCurrent();
            this.fkStudyStat = submissionStatusBean.getFkStudyStat();
        } catch(Exception e) {
            Rlog.fatal("submission", "Error in SubmissionStatusJB.getSubmissionStatusDetails "+e);
            output = -1;
        }
        return output;
    }
    
    private SubmissionStatusBean createEntityBean() {
        return new SubmissionStatusBean(this.id, this.fkSubmission, this.fkSubmissionBoard,
                this.fkSubmissionStatus, this.submissionStatusDate, this.submissionEnteredBy,
                this.submissionAssignedTo, this.submissionCompletedBy, this.submissionNotes,
                this.isCurrent, this.creator, this.lastModifiedBy, this.ipAdd, this.fkStudyStat);
    }

    private Integer getSubmissionStatus(String subtype) {
        if (subtype == null) { return new Integer(0); }
        CodeDao codeDao = new CodeDao();
        codeDao.getCodeValues("subm_status");
        ArrayList codeSubtypes = codeDao.getCSubType();
        ArrayList codePks = codeDao.getCId();
        for (int iX=0; iX<codeSubtypes.size(); iX++) {
            if (subtype.equals(codeSubtypes.get(iX))) {
                return (Integer)codePks.get(iX);
            }
        }
        return new Integer(0);
    }
    
    public void setSubmissionStatusBySubtype(String subtype) {
        this.fkSubmissionStatus = getSubmissionStatus(subtype);
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getFkSubmission() {
        return String.valueOf(fkSubmission);
    }
    public void setFkSubmission(String fkSubmission) {
        this.fkSubmission = EJBUtil.stringToInteger(fkSubmission);
    }
    public String getFkSubmissionBoard() {
        return String.valueOf(fkSubmissionBoard);
    }
    public void setFkSubmissionBoard(String fkSubmissionBoard) {
        this.fkSubmissionBoard = EJBUtil.stringToInteger(fkSubmissionBoard);
    }
    public Integer getFkSubmissionStatus() {
        return fkSubmissionStatus;
    }
    public void setFkSubmissionStatus(String fkSubmissionStatus) {
        this.fkSubmissionStatus = EJBUtil.stringToInteger(fkSubmissionStatus);
    }
    public Date getSubmissionStatusDate() {
        return submissionStatusDate;
    }
    public void setSubmissionStatusDate(String submissionStatusDate) {
        if (submissionStatusDate == null || submissionStatusDate.length() == 0) {
            this.submissionStatusDate = null;
        } else {
            this.submissionStatusDate = DateUtil.stringToDate(submissionStatusDate);
        }
    }
    public String getSubmissionEnteredBy() {
        return String.valueOf(submissionEnteredBy);
    }
    public void setSubmissionEnteredBy(String submissionEnteredBy) {
        this.submissionEnteredBy = EJBUtil.stringToInteger(submissionEnteredBy);
    }
    public String getSubmissionAssignedTo() {
        return String.valueOf(submissionAssignedTo);
    }
    public void setSubmissionAssignedTo(String submissionAssignedTo) {
        this.submissionAssignedTo = EJBUtil.stringToInteger(submissionAssignedTo);
    }
    public String getSubmissionCompletedBy() {
        return String.valueOf(submissionCompletedBy);
    }
    public void setSubmissionCompletedBy(String submissionCompletedBy) {
        this.submissionCompletedBy = EJBUtil.stringToInteger(submissionCompletedBy);
    }
    public String getSubmissionNotes() {
        return submissionNotes;
    }
    public void setSubmissionNotes(String submissionNotes) {
        this.submissionNotes = submissionNotes;
    }
    public int getIsCurrent() {
        return isCurrent;
    }
    public void setIsCurrent(int isCurrent) {
        this.isCurrent = isCurrent;
    }
    public String getCreator() {
        return String.valueOf(creator);
    }
    public void setCreator(String creator) {
        this.creator = EJBUtil.stringToInteger(creator);
    }
    public String getLastModifiedBy() {
        return String.valueOf(lastModifiedBy);
    }
    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = EJBUtil.stringToInteger(lastModifiedBy);
    }
    public String getIpAdd() {
        return ipAdd;
    }
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }
    public Integer getFkStudyStat() {
		return fkStudyStat;
	}
	public void setFkStudyStat(Integer fkStudyStat) {
		this.fkStudyStat = fkStudyStat;
	}



    /** Gets the one status previous from the current status for a for a submission board and submission. Returns the Pk of the codelst
     * */
     public String getPreviousCurrentSubmissionStatus(int submissionPK, int submissionBoardPK)
    {
    	String retStatus= "";
    	
    	try{
            SubmissionStatusAgent submissionStatusAgent = EJBUtil.getSubmissionStatusAgentHome();
            retStatus = submissionStatusAgent.getPreviousCurrentSubmissionStatus( submissionPK,  submissionBoardPK);
 
     	}
    	 catch (Exception e) {
             Rlog.fatal("submission", "Exception in SubmissionSatusJB.getPreviousCurrentSubmissionStatus "+e);
             retStatus = "";
         }
    	return retStatus;
 
    }

}
