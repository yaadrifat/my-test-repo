

package com.velos.eres.web.formAction;

/* IMPORT STATEMENTS */

import java.util.Hashtable;
import com.velos.eres.business.account.impl.AccountBean;
import com.velos.eres.business.formAction.impl.FormActionBean;
import com.velos.eres.service.formActionAgent.FormActionAgentRObj;
import com.velos.eres.service.formNotifyAgent.FormNotifyAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */


public class FormActionJB {

    private int formActionId;
    private String formActionName;
    private int formActionSourceFormId;
    private int formActionTargetFormId;
    private String formActionSourceFld;
    private String formActionTargetFld;
    private String formActionOperator;
    private int formActionFilledFormId;
    private String formActionMsg;
    // GETTER SETTER METHODS

   
    

    public int getFormActionFilledFormId() {
		return formActionFilledFormId;
	}


	public void setFormActionFilledFormId(int formActionFilledFormId) {
		this.formActionFilledFormId = formActionFilledFormId;
	}


	public int getFormActionId() {
		return formActionId;
	}


	public void setFormActionId(int formActionId) {
		this.formActionId = formActionId;
	}


	public String getFormActionName() {
		return formActionName;
	}


	public void setFormActionName(String formActionName) {
		this.formActionName = formActionName;
	}


	public String getFormActionOperator() {
		return formActionOperator;
	}


	public void setFormActionOperator(String formActionOperator) {
		this.formActionOperator = formActionOperator;
	}


	public String getFormActionSourceFld() {
		return formActionSourceFld;
	}


	public void setFormActionSourceFld(String formActionSourceFld) {
		this.formActionSourceFld = formActionSourceFld;
	}


	public int getFormActionSourceFormId() {
		return formActionSourceFormId;
	}


	public void setFormActionSourceFormId(int formActionSourceFormId) {
		this.formActionSourceFormId = formActionSourceFormId;
	}


	public String getFormActionTargetFld() {
		return formActionTargetFld;
	}


	public void setFormActionTargetFld(String formActionTargetFld) {
		this.formActionTargetFld = formActionTargetFld;
	}


	public int getFormActionTargetFormId() {
		return formActionTargetFormId;
	}


	public void setFormActionTargetFormId(int formActionTargetFormId) {
		this.formActionTargetFormId = formActionTargetFormId;
	}
	
	public String getFormActionMsg() {
		return formActionMsg;
	}

	public void setFormActionMsg(String formActionMsg) {
		this.formActionMsg = formActionMsg;
	}

//	 END OF GETTER SETTER METHODS

	/**
     * 
     * Defines an AccountJB object with the specified Account Id
     * 
     */

    public FormActionJB(int formActionId) {

        setFormActionId(formActionId);

    }
    
    public FormActionJB() {

        Rlog.debug("formAction", "FormActionJB.FormActionJB() ");

    }

    public FormActionJB(int formActionId, String formActionName, String formActionOperator,
            String formActionSourceFld, int formActionSourceFormId, String formActionTargetFld,
            int formActionTargetFormId,int formActionFilledFormId,String formActionMsg) {

    	setFormActionFilledFormId(formActionFilledFormId);
    	setFormActionId(formActionId);
    	setFormActionName(formActionName);
    	setFormActionOperator(formActionOperator);
    	setFormActionSourceFld(formActionSourceFld);
    	setFormActionSourceFormId(formActionSourceFormId);
    	setFormActionTargetFld(formActionTargetFld);
    	setFormActionTargetFormId(formActionTargetFormId);
    	setFormActionMsg(formActionMsg);

        Rlog.debug("FormAction", "FormActionJB.FormActionJB(all parameters)");

    }


    public FormActionBean getFormActionDetails() {

    	FormActionBean fask = null;

        try {

            FormActionAgentRObj formActionAgentRObj = EJBUtil.getFormActionAgentHome();
 
            fask = formActionAgentRObj.getFormActionDetails(this.formActionId);

            Rlog.debug("formAction",
                    "FormActionJB.getFormActionDetails() FormActionStateKeeper " + fask);

        } catch (Exception e) {

            Rlog.fatal("formAction", "Error in getFormActionDetails() in FormActionJB "
                    + e);

        }

        if (fask != null) {

            this.formActionId = fask.getFormActionId();
            this.formActionName = fask.getFormActionName();
            this.formActionOperator = fask.getFormActionOperator();
            this.formActionSourceFormId = fask.getFormActionSourceFormId();
            this.formActionTargetFormId = fask.getFormActionTargetFormId();
            this.formActionSourceFld = fask.getFormActionSourceFld();
            this.formActionTargetFld = fask.getFormActionTargetFld();
            this.formActionFilledFormId =  fask.getFormActionFilledFormId();
        	this.formActionMsg=fask.getFormActionMsg();


        }

        return fask;

    }

    /**
     * 
     * Stores the details of an formAction in the database
     * 
     */

    public void setFormActionDetails()

    {

        try {

            FormActionAgentRObj formActionAgentRObj = EJBUtil.getFormActionAgentHome();

            //this.setFormActionId(formActionAgentRObj.setFormActionDetails(this
            //                    	.createFormActionStateKeeper()));
            formActionAgentRObj.setFormActionDetails(this.createFormActionStateKeeper());
            Rlog.debug("formAction", "FormActionJB.setFormActionDetails()");

        } catch (Exception e) {

            Rlog.fatal("formAction", "Error in setFormActionDetails() in FormActionJB "
                    + e);

        }

    }


    /**
     * 
     * Saves the modified details of the form action to the database
     * 
     * 
     * 
     * @return 0 - If update succeeds <BR>
     * 
     * -2 - If update fails
     * 
     */

    public int updateFormAction()

    {

        int output;

        try {
        	 //System.out.println("into form action jb");
        	FormActionAgentRObj formActionAgentRObj = EJBUtil.getFormActionAgentHome();

            output = formActionAgentRObj.updateFormAction(this
                    .createFormActionStateKeeper());

        }

        catch (Exception e) {

            Rlog.fatal("formAction",
                    "EXCEPTION IN SETTING FORMACTION DETAILS TO  SESSION BEAN "
                            + e);
            return -2;

        }

        return output;

    }

    /**
     * 
     * Uses the values of the current FormActionJB object to create an
     * FormActionStateKeeper Object
     * 
     * 
     * 
     * @return FormActionStateKeeper object with all the details of the account
     * 
     * @see FormActionStateKeeper
     * 
     */

    public FormActionBean createFormActionStateKeeper()
    {

        Rlog.debug("FormAction", "FormActionJB.createFormActionStateKeeper ");

        return new FormActionBean(formActionId,  formActionName,  formActionOperator,
                 formActionSourceFld,  formActionSourceFormId,  formActionTargetFld,
                 formActionTargetFormId, formActionFilledFormId,formActionMsg);

    }

//061008ML for delete form action
    
    public int removeFormAction() {

        int output;

        try {

            FormActionAgentRObj formActionAgentRObj = EJBUtil
                    .getFormActionAgentHome();

            output = formActionAgentRObj.removeFormAction(this.formActionId);
            return output;

        } catch (Exception e) {
            Rlog.fatal("formAction",
                    "in FormActionJB -removeFormAction() method");
            return -1;
        }

    }
// end of 061008ML
 
    // Overloaded for INF-18183 ::: AGodara
    public int removeFormAction(Hashtable<String, String> auditInfo) {

        int output;
        try {
            FormActionAgentRObj formActionAgentRObj = EJBUtil
                    .getFormActionAgentHome();
            output = formActionAgentRObj.removeFormAction(this.formActionId,auditInfo);
            return output;
        } catch (Exception e) {
            Rlog.fatal("formAction",
                    "in FormActionJB method removeFormAction(Hashtable<String, String> auditInfo)");
            return -1;
        }
    }
      
}// end of class

