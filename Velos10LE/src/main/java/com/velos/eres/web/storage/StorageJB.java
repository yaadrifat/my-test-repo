/*
 * Classname : 				StorageJB
 *
 * Version information : 	1.0
 *
 * Date 					06/25/2007
 *
 * Copyright notice: 		Velos Inc
 *
 * Author 					Manimaran
 */

package com.velos.eres.web.storage;

/* IMPORT STATEMENTS */
import java.util.Hashtable;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.StorageDao;
import com.velos.eres.business.storage.impl.StorageBean;
import com.velos.eres.service.storageAgent.StorageAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.specimen.SpecimenJB;
import com.velos.eres.web.specimenStatus.SpecimenStatusJB;
import com.velos.eres.web.storageStatus.StorageStatusJB;

/* END OF IMPORT STATEMENTS */

public class StorageJB {
    private int pkStorage;
    private String storageId;
    private String storageName;
    private String fkCodelstStorageType;
    private String fkStorage;
    private String storageCapNum;
    private String fkCodelstCapUnits;
    //private String storageStatus;--KM-Removal of Status column in table.
    private String storageDim1CellNum;
    private String storageDim1Naming;
    private String storageDim1Order;
    private String storageDim2CellNum;
    private String storageDim2Naming;
    private String storageDim2Order;
    private String storageNotes;
    private String storageAvailUnitCnt;
    private String storageCoordinateX;
    private String storageCoordinateY;
    private String storageLabel;
    private String fkCodelstChildStype;
    private String parentstorageName;

    private String storageTemplateType;
    private String storageAlternateId;
    private String storageUnitClass;
    private String storageKitCategory;

    /**
     * creator
     */
    private String creator;

    /**
     * last modified by
     */
    private String modifiedBy;

    /**
     * IP Address
     */
    private String ipAdd;
    //KM-09182007
    private String accountId;

    private String template;
 
//  Bikash fk Storage kit for invp 2.8.
    private String fkStorageKit;
    private String multiSpecimen;
//  GETTERS AND SETTERS  

	/**
	 * @return the pkStorage
	 */
	public int getPkStorage() {
		return pkStorage;
	}

	/**
	 * @param pkStorage the pkStorage to set
	 */
	public void setPkStorage(int pkStorage) {
		this.pkStorage = pkStorage;
	}

	/**
	 * @return the storageId
	 */
	public String getStorageId() {
		return storageId;
	}


	/**
	 * @param storageId the storageId to set
	 */
	public void setStorageId(String storageId) {
		this.storageId = storageId;
	}


	/**
	 * @return the storageName
	 */
	public String getStorageName() {
		return storageName;
	}


	/**
	 * @param storageName the storageName to set
	 */
	public void setStorageName(String storageName) {
		this.storageName = storageName;
	}

	/**
	 * @return the fkCodelstStorageType
	 */
	public String getFkCodelstStorageType() {
		return fkCodelstStorageType;
	}

	/**
	 * @param fkCodelstStorageType the fkCodelstStorageType to set
	 */
	public void setFkCodelstStorageType(String fkCodelstStorageType) {
		this.fkCodelstStorageType = fkCodelstStorageType;
	}

	/**
	 * @return the fkCodelstChildStype
	 */
	public String getFkCodelstChildStype() {
		return fkCodelstChildStype;
	}

	/**
	 * @param fkCodelstChildStype the fkCodelstChildStype to set
	 */
	public void setFkCodelstChildStype(String fkCodelstChildStype) {
		this.fkCodelstChildStype = fkCodelstChildStype;
	}


	/**
	 * @return the fkStorage
	 */
	public String getFkStorage() {
		return fkStorage;
	}

	/**
	 * @param fkStorage the fkStorage to set
	 */
	public void setFkStorage(String fkStorage) {
		this.fkStorage = fkStorage;
	}

	/**
	 * @return the storageCapNum
	 */
	public String getStorageCapNum() {
		return storageCapNum;
	}

	/**
	 * @param storageCapNum the storageCapNum to set
	 */
	public void setStorageCapNum(String storageCapNum) {
		this.storageCapNum = storageCapNum;
	}

	/**
	 * @return the fkCodelstCapUnits
	 */
	public String getFkCodelstCapUnits() {
		return fkCodelstCapUnits;
	}

	/**
	 * @param fkCodelstCapUnits the fkCodelstCapUnits to set
	 */
	public void setFkCodelstCapUnits(String fkCodelstCapUnits) {
		this.fkCodelstCapUnits = fkCodelstCapUnits;
	}


	/**
	 * @return the storageDim1CellNum
	 */
	public String getStorageDim1CellNum() {
		return storageDim1CellNum;
	}

	/**
	 * @param storageDim1CellNum the storageDim1CellNum to set
	 */
	public void setStorageDim1CellNum(String storageDim1CellNum) {
		this.storageDim1CellNum = storageDim1CellNum;
	}

	/**
	 * @return the storageDim1Naming
	 */
	public String getStorageDim1Naming() {
		return storageDim1Naming;
	}

	/**
	 * @param storageDim1Naming the storageDim1Naming to set
	 */
	public void setStorageDim1Naming(String storageDim1Naming) {
		this.storageDim1Naming = storageDim1Naming;
	}

	/**
	 * @return the storageDim1Order
	 */
	public String getStorageDim1Order() {
		return storageDim1Order;
	}

	/**
	 * @param storageDim1Order the storageDim1Order to set
	 */
	public void setStorageDim1Order(String storageDim1Order) {
		this.storageDim1Order = storageDim1Order;
	}

	/**
	 * @return the storageDim2CellNum
	 */
	public String getStorageDim2CellNum() {
		return storageDim2CellNum;
	}

	/**
	 * @param storageDim2CellNum the storageDim2CellNum to set
	 */
	public void setStorageDim2CellNum(String storageDim2CellNum) {
		this.storageDim2CellNum = storageDim2CellNum;
	}

	/**
	 * @return the storageDim2Naming
	 */
	public String getStorageDim2Naming() {
		return storageDim2Naming;
	}

	/**
	 * @param storageDim2Naming the storageDim2Naming to set
	 */
	public void setStorageDim2Naming(String storageDim2Naming) {
		this.storageDim2Naming = storageDim2Naming;
	}

	/**
	 * @return the storageDim2Order
	 */
	public String getStorageDim2Order() {
		return storageDim2Order;
	}

	/**
	 * @param storageDim2Order the storageDim2Order to set
	 */
	public void setStorageDim2Order(String storageDim2Order) {
		this.storageDim2Order = storageDim2Order;
	}

	/**
	 * @return the storageNotes
	 */
	public String getStorageNotes() {
		return storageNotes;
	}

	/**
	 * @param storageNotes the storageNotes to set
	 */
	public void setStorageNotes(String storageNotes) {
		this.storageNotes = storageNotes;
	}

	/**
	 * @return the storageAvailUnitCnt
	 */
	public String getStorageAvailUnitCnt() {
		return storageAvailUnitCnt;
	}

	/**
	 * @param storageAvailUnitCnt the storageAvailUnitCnt to set
	 */
	public void setStorageAvailUnitCnt(String storageAvailUnitCnt) {
		this.storageAvailUnitCnt = storageAvailUnitCnt;
	}

	/**
	 * @return the storageCoordinateX
	 */
	public String getStorageCoordinateX() {
		return storageCoordinateX;
	}

	/**
	 * @param storageCoordinateX the storageCoordinateX to set
	 */
	public void setStorageCoordinateX(String storageCoordinateX) {
		this.storageCoordinateX = storageCoordinateX;
	}

	/**
	 * @return the storageCoordinateY
	 */
	public String getStorageCoordinateY() {
		return storageCoordinateY;
	}

	/**
	 * @param storageCoordinateY the storageCoordinateY to set
	 */
	public void setStorageCoordinateY(String storageCoordinateY) {
		this.storageCoordinateY = storageCoordinateY;
	}

	/**
	 * @return the storageLabel
	 */
	public String getStorageLabel() {
		return storageLabel;
	}

	/**
	 * @param storageLabel the storageLabel to set
	 */
	public void setStorageLabel(String storageLabel) {
		this.storageLabel = storageLabel;
	}

	/**
	 * @return the creator
	 */
	public String getCreator() {
		return creator;
	}

	/**
	 * @param creator the creator to set
	 */
	public void setCreator(String creator) {
		this.creator = creator;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the ipAdd
	 */
	public String getIpAdd() {
		return ipAdd;
	}

	/**
	 * @param ipAdd the ipAdd to set
	 */
	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}

	//KM-09182007-Account Id added
	/**
     * @return accountId
     */
    public String getAccountId() {
        return this.accountId;
    }


    /**
     * @param accountId
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    //KM
    /**
     * @return template
     */
    public String getTemplate() {
        return this.template;
    }

    /**
     * @param template
     */
    public void setTemplate(String template) {
        this.template = template;
    }

  //JM: 30Jul2009: invp2.15 (part-1)

    public String getStorageTemplateType() {
    	return this.storageTemplateType;

    }

    public void setStorageTemplateType(String storageTemplateType){
    	 this.storageTemplateType = storageTemplateType;
    }

    public String getStorageAlternateId() {
        return this.storageAlternateId;
    }

    public void setStorageAlternateId(String storageAlternateId) {
        this.storageAlternateId = storageAlternateId;
    }

    public String getStorageUnitClass() {
    	return this.storageUnitClass;

    }

    public void setStorageUnitClass(String storageUnitClass){
    	 this.storageUnitClass = storageUnitClass;
    }

//JM: 11Aug2009, invp2.15.3
    public String getStorageKitCategory() {
    	return this.storageKitCategory;

    }

    public void setStorageKitCategory(String storageKitCategory){
    	 this.storageKitCategory = storageKitCategory;
    }
    
    //invp 2.8 added by Bikash
    /**
     *  fk storage kit
     * @return
     */
    public String getFkStorageKit() {
    	return fkStorageKit;
    }
/**
 * 
 * @param fkStorageKit
 */
    public void setFkStorageKit(String fkStorageKit) {
    	this.fkStorageKit = fkStorageKit;
    }
    
    /**
     * @return multiSpecimenStorage
     */
    public String getMultiSpecimenStorage() {
        return this.multiSpecimen;
    }
    
    public void setMultiSpecimenStorage(String multiSpecimen) {
    	this.multiSpecimen = multiSpecimen;
    }

//	 END OF THE GETTER AND SETTER METHODS

	/**
     * Constructor
     *
     * @param pkStorage:
     *            Unique Id constructor
     *
     */

    public StorageJB(int pkStorage) {
        setPkStorage(pkStorage);
    }

    /**
     * Default Constructor
     */

    public StorageJB() {
        Rlog.debug("storage", "StorageJB.StorageJB() ");
    }

	public StorageJB(int pkStorage, String storageId, String storageName, String fkCodelstStorageType, String fkStorage, String storageCapNum, String fkCodelstCapUnits, String storageDim1CellNum, String storageDim1Naming, String storageDim1Order, String storageDim2CellNum, String storageDim2Naming, String storageDim2Order, String storageNotes, String storageAvailUnitCnt, String storageCoordinateX, String storageCoordinateY, String storageLabel,String fkCodelstChildStype, String creator, String modifiedBy, String ipAdd, String accountId, String template, String storageTemplateType, String storageAlternateId, String storageUnitClass, String storageKitCategory,String multiSpecimen) {

		setPkStorage(pkStorage);
		setStorageId(storageId);
		setStorageName(storageName);
		setFkCodelstStorageType(fkCodelstStorageType);
		setFkStorage(fkStorage);
		setStorageCapNum(storageCapNum);
		setFkCodelstCapUnits(fkCodelstCapUnits);
		setStorageDim1CellNum(storageDim1CellNum);
		setStorageDim1Naming(storageDim1Naming);
		setStorageDim1Order(storageDim1Order);
		setStorageDim2CellNum(storageDim2CellNum);
		setStorageDim2Naming(storageDim2Naming);
		setStorageDim2Order(storageDim2Order);
		setStorageNotes(storageNotes);
		setStorageAvailUnitCnt(storageAvailUnitCnt);
		setStorageCoordinateX(storageCoordinateX);
		setStorageCoordinateY(storageCoordinateY);
		setStorageLabel(storageLabel);
		setFkCodelstChildStype(fkCodelstChildStype);
		setCreator(creator);
		setModifiedBy(modifiedBy);
		setIpAdd(ipAdd);
		setAccountId(accountId);
		setTemplate(template);
		setStorageTemplateType(storageTemplateType);
        setStorageAlternateId(storageAlternateId);
        setStorageUnitClass(storageUnitClass);
        setStorageKitCategory(storageKitCategory);
        setMultiSpecimenStorage(multiSpecimen);
	}


	  /**
     * Retrieves the details of an storage in StorageStateKeeper Object
     *
     * @return StorageStateKeeper consisting of the storage details
     * @see StorageStateKeeper
     */
    public StorageBean getStorageDetails() {
    	StorageBean ssk = null;
    	String parentstorage_Name = "";
    	StorageDao sd = new StorageDao();


        try {
            StorageAgentRObj storageAgentRObj = EJBUtil.getStorageAgentHome();
            ssk = storageAgentRObj.getStorageDetails(this.pkStorage);
            Rlog.debug("storage", "In StorageJB.getStorageDetails() " + ssk);
        } catch (Exception e) {
            Rlog
                    .fatal("storage", "Error in getStorageDetails() in StorageJB "
                            + e);
        }
        if (ssk != null) {

        	parentstorage_Name = sd.getParentStorage(this.pkStorage);

        	if (StringUtil.isEmpty(parentstorage_Name))
        	{
        		parentstorage_Name = "";
        	}

        	this.parentstorageName = parentstorage_Name;

        	copyStorageBeanAttributes(ssk);

        }

        return ssk;
    }


    public int setStorageDetails() {
        int output = 0;
        try {
        	StorageAgentRObj storageAgentRObj = EJBUtil.getStorageAgentHome();
            output = storageAgentRObj.setStorageDetails(this
                    .createStorageStateKeeper());

            this.setPkStorage(output);

            CommonDAO cDao = new CommonDAO();
            Rlog.debug("storage:---Notes", "storage data saved:" + output);
            Rlog.debug("storage", "storage this.getContents():" + this.getStorageNotes());
            if(this.getStorageNotes()!=null)
            cDao.updateClob(this.getStorageNotes(),"er_storage","storage_notes"," where pk_storage = " + output);
            Rlog.debug("storage", "StorageJB.setStorageDetails()");
        }catch (Exception e) {
            Rlog
                    .fatal("storage", "Error in setStorageDetails() in StorageJB "
                            + e);
          return -2;
        }
        return output;
    }


    public int updateStorage() {
        int output;
        try {
        	StorageAgentRObj storageAgentRObj = EJBUtil.getStorageAgentHome();
            output = storageAgentRObj.updateStorage(this
                    .createStorageStateKeeper());

            CommonDAO cDao = new CommonDAO();
            Rlog.debug("storage:---Notes", "storage data saved:" + this.getPkStorage());
            Rlog.debug("storage", "storage this.getContents():" + this.getStorageNotes());
            cDao.updateClob(this.getStorageNotes(),"er_storage","storage_notes"," where pk_storage = " + this.getPkStorage());
            Rlog.debug("storage", "StorageJB.updateStorage()");
        } catch (Exception e) {
            Rlog.debug("storage",
                    "EXCEPTION IN SETTING STORAGE DETAILS TO  SESSION BEAN"
                            + e.getMessage());

            return -2;
        }
        return output;
    }

    public StorageBean createStorageStateKeeper() {
        Rlog.debug("storage", "StorageJB.createStorageStateKeeper ");
        return new StorageBean(this.pkStorage, this.storageId, this.storageName, this.fkCodelstStorageType,
        		this.fkStorage, this.storageCapNum, this.fkCodelstCapUnits,
        		this.storageDim1CellNum, this.storageDim1Naming,
        		this.storageDim1Order,  this.storageDim2CellNum,  this.storageDim2Naming,
        		this.storageDim2Order,  this.storageNotes,this.storageAvailUnitCnt,
        		this.storageCoordinateX, this.storageCoordinateY,this.storageLabel,this.fkCodelstChildStype,
        		this.creator,  this.modifiedBy,  this.ipAdd, this.accountId, this.template,
        		this.storageTemplateType, this.storageAlternateId, this.storageUnitClass,
        		this.storageKitCategory ,this.fkStorageKit,this.multiSpecimen);
    }


    public int deleteStorages(String[] deleteIds, int flag, int user) {
        int ret = 0;
        try {
            Rlog.debug("storage", "StorageJB:deleteStorages starting");

            StorageAgentRObj storageAgentRObj = EJBUtil.getStorageAgentHome();
            ret = storageAgentRObj.deleteStorages(deleteIds,flag, user);//KM

        } catch (Exception e) {
            Rlog.fatal("storage", "StorageJB:deleteStorage-Error " + e);
            return -1;
        }
        return ret;

    }
    // Overloaded for INF-18183 ::: Akshi
    public int deleteStorages(String[] deleteIds, int flag, int user, Hashtable<String, String> args) {
        int ret = 0;
        try {
            Rlog.debug("storage", "StorageJB:deleteStorages starting");

            StorageAgentRObj storageAgentRObj = EJBUtil.getStorageAgentHome();
            ret = storageAgentRObj.deleteStorages(deleteIds,flag, user,args);//KM

        } catch (Exception e) {
            Rlog.fatal("storage", "StorageJB:deleteStorage-Error " + e);
            return -1;
        }
        return ret;

    }

    public int getCount(String storageName) {
        int count = 0;
        try {
            Rlog.debug("storage", "StorageJB.getCount starting");
            StorageAgentRObj storageAgentRObj = EJBUtil.getStorageAgentHome();
            Rlog.debug("storage", "StorageJB.getCount after remote");
            count = storageAgentRObj.getCount(storageName);
            Rlog.debug("storage", "StorageJB.getCount after Dao");
        } catch (Exception e) {
            Rlog.fatal("storage", "Error in getCount(storageName) in StorageJB " + e);
        }
        return count;
    }

    //KM-091907
    public int getCountId(String storageId,String accId) {
        int count = 0;
        try {
            Rlog.debug("storage", "StorageJB.getCountId starting");
            StorageAgentRObj storageAgentRObj = EJBUtil.getStorageAgentHome();
            Rlog.debug("storage", "StorageJB.getCountId after remote");
            count = storageAgentRObj.getCountId(storageId,accId);
            Rlog.debug("storage", "StorageJB.getCountId after Dao");
        } catch (Exception e) {
            Rlog.fatal("storage", "Error in getCountId(storageId) in StorageJB " + e);
        }
        return count;
    }

    //BT: For INV 21675
    public int getStorageCount(String fkStorage) {
        int count = 0;
        try {
            Rlog.debug("storage", "StorageJB.getStorageCount starting");
            StorageAgentRObj storageAgentRObj = EJBUtil.getStorageAgentHome();
            Rlog.debug("storage", "StorageJB.getStorageCount after remote");
            count = storageAgentRObj.getStorageCount(fkStorage);
            Rlog.debug("storage", "StorageJB.getStorageCount after Dao");
        } catch (Exception e) {
            Rlog.fatal("storage", "Error in getStorageCount(fkStorage) in StorageJB " + e);
        }
        return count;
    }

    public int getChildCount(String fkStorage) {
        int count = 0;
        try {
            Rlog.debug("storage", "StorageJB.getChildCount starting");
            StorageAgentRObj storageAgentRObj = EJBUtil.getStorageAgentHome();
            Rlog.debug("storage", "StorageJB.getChildCount after remote");
            count = storageAgentRObj.getChildCount(fkStorage);
            Rlog.debug("storage", "StorageJB.getChildCount after Dao");
        } catch (Exception e) {
            Rlog.fatal("storage", "Error in getChildCount(fkStorage) in StorageJB " + e);
        }
        return count;
    }
    
    public String getStorageIdAuto() {

   	 String id = "";
        try {
            Rlog.debug("storage", "StorageJB:getStorageIdAuto ....");

            StorageAgentRObj storageAgentRObj = EJBUtil.getStorageAgentHome();
            id = storageAgentRObj.getStorageIdAuto();

        } catch (Exception e) {
            Rlog.fatal("storage", "StorageJB:getStorageIdAuto -Error " + e);
            return id;
        }
        return id;

    }

    public String getStorageIdAuto(String fkCodelstStorageType, int fkAccount) {
        String id = "";
        try {
            StorageAgentRObj storageAgentRObj = EJBUtil.getStorageAgentHome();
            id = StringUtil.trueValue(storageAgentRObj.getStorageIdAuto(fkCodelstStorageType, fkAccount));
        } catch (Exception e) {
            Rlog.fatal("storage", "In StorageJB:getStorageIdAuto(storageType): " + e);
            return id;
        }
        return id;
    }

    public StorageDao getStorageValue(int pkStorage) {
    	StorageDao storageDao = new StorageDao();
    	try {
            StorageAgentRObj storageAgentRObj = EJBUtil.getStorageAgentHome();
            storageDao= storageAgentRObj.getStorageValue(pkStorage);
            return storageDao;

        } catch (Exception e) {
            Rlog.fatal("storage", "Error in getStorageValue(pkStorage) in StorageJB " + e);
        }
        return storageDao;
    }

	public String getParentstorageName() {
		return parentstorageName;
	}

//JM: 28Dec2007
    public String findAllChildStorageUnitIds(String storageId) {

      	 String retString = "";
           try {
               Rlog.debug("storage", "StorageJB:findAllChildStorageUnitIds ....");

               StorageAgentRObj storageAgentRObj = EJBUtil.getStorageAgentHome();
               retString = storageAgentRObj.findAllChildStorageUnitIds(storageId);

           } catch (Exception e) {
               Rlog.fatal("storage", "StorageJB:findAllChildStorageUnitIds -Error " + e);
               return retString;
           }
           return retString;

       }

//  JM: 22Jan2008
    public String findParentStorageType(String storageId) {
    	 if (storageId == null) { return null; }
      	 String retString = "";
           try {
               Rlog.debug("storage", "StorageJB:findParentStorageType ....");

               StorageAgentRObj storageAgentRObj = EJBUtil.getStorageAgentHome();
               retString = storageAgentRObj.findParentStorageType(storageId);

           } catch (Exception e) {
               Rlog.fatal("storage", "StorageJB:findParentStorageType -Error " + e);
               return retString;
           }
           return retString;

       }


//  Added by Manimaran to copy StorageUnit and Templates.
    public int copyStorageUnitsAndTemplates(String[] strgPks,String[] copyCounts,String[] strgIds,String[] strgNames,String[] templates,String usr, String ipAdd) {
    	int ret = 0;
    	try {
    		Rlog.debug("storage", "StorageJB:copyStorages starting");

    		StorageAgentRObj storageAgentRObj = EJBUtil.getStorageAgentHome();
    		ret = storageAgentRObj.copyStorageUnitsAndTemplates(strgPks,copyCounts,strgIds,strgNames,templates,usr,ipAdd);

    	} catch (Exception e) {
    		Rlog.fatal("storage", "StorageJB:copyStorages-Error " + e);
    		return -1;
    	}
    	return ret;

    }


    /**
     * Searches storages on the basis of arguments passed in Hashtable args
     * */
    public StorageDao searchStorage(Hashtable args,int account)
    {
    	StorageDao sdo = new StorageDao();
    	try {


    		StorageAgentRObj storageAgentRObj = EJBUtil.getStorageAgentHome();
    		sdo = storageAgentRObj.searchStorage(args,account);

    	} catch (Exception e) {
    		Rlog.fatal("storage", "StorageJB:searchStorage-Error " + e);

    	}
    	return sdo;

    }



    /** Storage Search method. arguments are passed in Hastable args
     *  */
    public StorageDao getImmediateChildren(int storagePK)
    {
    	StorageDao sdo = new StorageDao();
    	try {


    		StorageAgentRObj storageAgentRObj = EJBUtil.getStorageAgentHome();
    		sdo = storageAgentRObj.getImmediateChildren(storagePK);

    	} catch (Exception e) {
    		Rlog.fatal("storage", "StorageJB:searchStorage-Exception " + e);

    	}
    	return sdo;

    }

    /**
     * creates a specimen for the filled form, assign it to a new storage and assign the new storage to selected location
     * returns @Hashtable keys:
     * 	newspecimen - value of new specimen pk
     * */
    public Hashtable addFormSpecimenToStorageKit(Hashtable args,String creator, String ipAdd,String account)
    {
    	Hashtable htReturn = new Hashtable();

    	String newlocation = "";
    	String specimenId = "";
    	String specType = "";
    	String specimenStatus = "";
    	String statDate = "";
    	String fkStorageForSpecimen = "";
    	SpecimenJB specJB = new SpecimenJB();
    	StorageJB sjb = new StorageJB();
    	StorageJB sjbNew = new StorageJB();

    	StorageStatusJB newStatB = new StorageStatusJB();
    	SpecimenStatusJB specStatB = new SpecimenStatusJB();

    	String storageId = "";
    	String strCoordinateY ="";
    	String strCoordinateX ="";

    	int newstoragepk = 0;
    	int specPk = 0;
    	String fkper = "";
    	String fkstudy = "";
    	String fkschevents1 = "";
    	int defaultStorageStat = 0;

    	try {
    		//get Parameters
    		newlocation = (String)args.get("newlocation");
    		specimenId = (String)args.get("specimenId");
    		specType = (String)args.get("specType");
    		specimenStatus = (String)args.get("specimenStatus");
    		statDate = (String)args.get("statDate");
    		fkStorageForSpecimen = (String)args.get("fkStorageForSpecimen");
    		strCoordinateY =  (String)args.get("strCoordinateY");
    		strCoordinateX =  (String)args.get("strCoordinateX");

    		fkper =  (String)args.get("fkper");
    		fkstudy =  (String)args.get("fkstudy");
    		fkschevents1 =  (String)args.get("fkschevents1");


			//if specimenId is empty, generate a new is

    		if (StringUtil.isEmpty(specimenId))
    		{
    			specimenId = specJB.getSpecimenIdAuto();
    		}

    		//if both location and KIT is blank do not create any storage

    		if ((!StringUtil.isEmpty(fkStorageForSpecimen)) ||  (! StringUtil.isEmpty(newlocation)))
    		{
	    		//create  new storage and assign it to location


	    		// 	set new storage's values and copy this storage
	    		storageId = sjbNew.getStorageIdAuto();

	    		if (! StringUtil.isEmpty(fkStorageForSpecimen))
	    		{
		    		sjb.setPkStorage(EJBUtil.stringToNum(fkStorageForSpecimen));
		    		sjbNew.copyStorageBeanAttributes(sjb.getStorageDetails());

		    		sjbNew.setPkStorage(0);

		    		sjbNew.setStorageId(storageId);
		    		sjbNew.setFkStorage(newlocation);
		    		sjbNew.setCreator(creator);
		    		sjbNew.setModifiedBy(creator);
		    		sjbNew.setIpAdd(ipAdd);
		    		sjbNew.setAccountId(account);

		    		sjbNew.setStorageCoordinateX(strCoordinateX);
		    		sjbNew.setStorageCoordinateY(strCoordinateY);
		    		sjbNew.setStorageDetails();

		    		newstoragepk = sjbNew.getPkStorage(); //this is where the new specimen will go
	    		}
	    		else
	    		{
	    			if (! StringUtil.isEmpty(newlocation))
	    			{
	    				newstoragepk = EJBUtil.stringToNum(newlocation);
	    			}
	    		}



    		}
    		// create default status for this storage

    		CodeDao cdstoragestat =new CodeDao();
    		defaultStorageStat = cdstoragestat.getCodeId("storage_stat","Occupied");

    		newStatB.setSsStartDate(statDate);
    		newStatB.setFkStorage(String.valueOf(newstoragepk));
         	newStatB.setFkCodelstStorageStat(String.valueOf(defaultStorageStat));

			newStatB.setFkUser(creator);
			newStatB.setIpAdd(ipAdd);
			newStatB.setCreator(creator);
			newStatB.setStorageStatusDetails();

    		//create new specimen
    		specJB.setCreator(creator);
    		specJB.setFkAccount(account);

    		if (newstoragepk > 0)
    		{
    			specJB.setFkStorage(String.valueOf(newstoragepk));
    		}
    		specJB.setFkStrgKitComp(fkStorageForSpecimen);


    		specJB.setSpecimenId(specimenId);

    		specJB.setIpAdd(ipAdd);

    		specJB.setSpecCollDate(statDate);
    		specJB.setSpecType(specType);

    		specJB.setFkPer(fkper);
    		specJB.setFkSchEvents(fkschevents1);
    		specJB.setFkStudy(fkstudy);
    		//specJB.setFkVisit();
    		specJB.setSpecDesc("Created from form response");
    		specJB.setSpecimenDetails();
    		specPk = specJB.getPkSpecimen();

    		//create status for this specimen

    		specStatB.setFkSpecimen(String.valueOf(specPk));

    		specStatB.setSsDate(statDate);
    		specStatB.setFkCodelstSpecimenStat(specimenStatus);
    		specStatB.setSsQuantity("");
    		specStatB.setSsQuantityUnits("");
    		specStatB.setSsAction("");
    		specStatB.setFkStudy(fkstudy);

    		specStatB.setSsStatusBy(creator);
    		specStatB.setCreator(creator);
    		specStatB.setIpAdd(ipAdd);
    		specStatB.setSpecimenStatusDetails();


    		htReturn.put("newspecimen", String.valueOf(specPk));
    	} catch (Exception e) {
    		Rlog.fatal("storage", "StorageJB:addFormSpecimenToStorageKit-Exception " + e);
    		htReturn.put("newspecimen", new String("0"));

    	}
    	return htReturn;

    }


    //

    /**
     * sets values of StorageBean attributes to the bean object
     *
     * @see StorageBean
     */
    public void copyStorageBeanAttributes(StorageBean ssk ) {
    	 if (ssk != null) {

            this.pkStorage = ssk.getPkStorage();
            this.storageId = ssk.getStorageId();
            this.storageName = ssk.getStorageName();
            this.fkCodelstStorageType = ssk.getFkCodelstStorageType();
    		this.fkStorage = ssk.getFkStorage();
    		this.storageCapNum = ssk.getStorageCapNum();
    		this.fkCodelstCapUnits = ssk.getFkCodelstCapUnits();
    		this.storageDim1CellNum = ssk.getStorageDim1CellNum();
    		this.storageDim1Naming = ssk.getStorageDim1Naming();
    		this.storageDim1Order = ssk.getStorageDim1Order();
    		this.storageDim2CellNum = ssk.getStorageDim2CellNum();
    		this.storageDim2Naming = ssk.getStorageDim2Naming();
    		this.storageDim2Order = ssk.getStorageDim2Order();
    		this.storageNotes = ssk.getStorageNotes();
    		this.storageAvailUnitCnt = ssk.getStorageAvailUnitCnt();
    		this.storageCoordinateX = ssk.getStorageCoordinateX();
    		this.storageCoordinateY = ssk.getStorageCoordinateY();
    		this.storageLabel = ssk.getStorageLabel();
    		this.fkCodelstChildStype = ssk.getFkCodelstChildStype();
            this.creator = ssk.getCreator();
            this.modifiedBy = ssk.getModifiedBy();
            this.ipAdd = ssk.getIpAdd();
            this.accountId = ssk.getAccountId();
            this.template = ssk.getTemplate();
            this.multiSpecimen=ssk.getMultiSpecimenStorage();
            this.storageTemplateType = ssk.getStorageTemplateType();
        	this.storageAlternateId = ssk.getStorageAlternateId();
       		this.storageUnitClass = ssk.getStorageUnitClass();
       		this.storageKitCategory = ssk.getStorageKitCategory();

        }

    }

//JM: 21Aug2009:
    public StorageDao getAllChildren(int pkStorageKit) {
    	StorageDao storageDao = new StorageDao();
    	try {
            StorageAgentRObj storageAgentRObj = EJBUtil.getStorageAgentHome();
            storageDao= storageAgentRObj.getAllChildren(pkStorageKit);
            return storageDao;

        } catch (Exception e) {
            Rlog.fatal("storage", "Error in getAllChildren(pkStorageKit) in StorageJB " + e);
        }
        return storageDao;
    }



    public StorageDao getSpecimensAndPatients(int pkStorage, int account) {
        StorageDao storageDao = null;
        try {
            StorageAgentRObj storageAgentRObj = EJBUtil.getStorageAgentHome();
            storageDao = storageAgentRObj.getSpecimensAndPatients(pkStorage, account);
        } catch (Exception e) {
            Rlog.fatal("storage", "Error in getSpecimensAndPatients in StorageJB " + e);
        }
        return storageDao == null ? new StorageDao() : storageDao;
    }

}// end of class

