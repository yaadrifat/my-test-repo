/*

 * Classname : 				AccountJB

 * 

 * Version information : 	1.0

 *

 * Date 					02/19/2001

 * 

 * Copyright notice: 		Velos Inc

 * 

 * Author 					Sajal

 */

package com.velos.eres.web.account;

/* IMPORT STATEMENTS */

import com.velos.eres.business.account.impl.AccountBean;
import com.velos.eres.business.common.AccountDao;
import com.velos.eres.service.accountAgent.AccountAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * 
 * Client side bean for Account module. This class is used for any kind of
 * manipulation for the Account.
 * 
 * @author Sajal
 * 
 * @version 1.0, 02/19/2001
 * 
 */

public class AccountJB {

    /*
     * CLASS METHODS
     * 
     * 
     * 
     * public int getAccId()
     * 
     * public void setAccId(int accId)
     * 
     * public String getAccType()
     * 
     * public void setAccType(String accType)
     * 
     * public String getAccRoleType()
     * 
     * public void setAccRoleType(String accRoleType)
     * 
     * public String getAccName()
     * 
     * public void setAccName(String accName)
     * 
     * public String getAccStartDate()
     * 
     * public void setAccStartDate(String accStartDate)
     * 
     * public String getAccEndDate()
     * 
     * public void setAccEndDate(String accEndDate)
     * 
     * public String getAccStatus()
     * 
     * public void setAccStatus(String accStatus)
     * 
     * public String getAccMailFlag()
     * 
     * public void setAccMailFlag(String accMailFlag)
     * 
     * public String getAccPubFlag()
     * 
     * public void setAccPubFlag(String accPubFlag)
     * 
     * public String getAccNote()
     * 
     * public void setAccNote(String accNote)
     * 
     * public String getAccCreator()
     * 
     * public void setAccCreator(String accCreator)
     * 
     * public String getAccLogo()
     * 
     * public void setAccLogo(String accLogo)
     * 
     * public AccountJB(int accId)
     * 
     * public AccountJB()
     * 
     * public AccountJB(int accId, String accType, String accRoleType, String
     * accName,
     * 
     * String accStartDate, String accEndDate,String accStatus,
     * 
     * String accMailFlag, String accPubFlag, String accNote,
     * 
     * String accCreator, String accLogo)
     * 
     * public AccountStateKeeper getAccountDetails()
     * 
     * public void setAccountDetails()
     * 
     * public int updateAccount()
     * 
     * public AccountStateKeeper createAccountStateKeeper()
     * 
     * 
     * 
     * END OF CLASS METHODS
     */

    /*
     * 
     * account Id
     * 
     */

    private int accId;

    /*
     * 
     * account type
     * 
     */

    private String accType;

    /*
     * 
     * account role type
     * 
     */

    private String accRoleType;

    /*
     * 
     * account name
     * 
     */

    private String accName;

    /*
     * 
     * account start date
     * 
     */

    private String accStartDate;

    /*
     * 
     * account end date
     * 
     */

    private String accEndDate;

    /*
     * 
     * account status
     * 
     */

    private String accStatus;

    /*
     * 
     * account mail flag
     * 
     */

    private String accMailFlag;

    /*
     * 
     * account public flag
     * 
     */

    private String accPubFlag;

    /*
     * 
     * account note
     * 
     */

    private String accNote;

    /*
     * 
     * account creator
     * 
     */

    private String accCreator;

    /*
     * 
     * account logo
     * 
     */

    private String accLogo;

    /*
     * 
     * account max user
     * 
     */

    private String accMaxUsr;
    
    /*
     * 
     * account max Portals
     * 
     */

    private String accMaxPortal; //JM:

    

    /*
     * 
     * account max storage
     * 
     */

    private String accMaxStorage;

    /*
     * 
     * account module rights
     * 
     */

    private String accModRight;

    /*
     * 
     * creator
     * 
     */

    private String creator;

    /*
     * 
     * last modified by
     * 
     */

    private String modifiedBy;

    /*
     * 
     * IP Address
     * 
     */

    private String ipAdd;

    private String ldapEnabled;
    
    private String wsEnabled;
    
    private String accSkin;
    
    private String autoGenStudy;//KM
    
    private String autoGenPatient;
    
    // GETTER SETTER METHODS

    /**
     * 
     * @return Account Id
     * 
     */

    public int getAccId() {

        return this.accId;

    }

    /**
     * 
     * @param accId
     *            The value that is required to be registered as Account Id
     * 
     */

    public void setAccId(int accId) {

        this.accId = accId;

    }

    /**
     * 
     * @return Account Type
     * 
     */

    public String getAccType() {

        return this.accType;

    }

    /**
     * 
     * @param accType
     *            The value that is required to be registered as Account Type
     * 
     */

    public void setAccType(String accType) {

        this.accType = accType;

    }

    /**
     * 
     * @return Account Role Type
     * 
     */

    public String getAccRoleType() {

        return this.accRoleType;

    }

    /**
     * 
     * @param accType
     *            The value that is required to be registered as Account Type
     * 
     */

    public void setAccRoleType(String accRoleType) {

        this.accRoleType = accRoleType;

    }

    /**
     * 
     * @return Name of the Account
     * 
     */

    public String getAccName() {

        return this.accName;

    }

    /**
     * 
     * @param accName
     *            The value that is required to be registered as Account Name
     * 
     */

    public void setAccName(String accName) {

        this.accName = accName;

    }

    /**
     * 
     * @return Account Start Date
     * 
     */

    public String getAccStartDate() {

        return this.accStartDate;

    }

    /**
     * 
     * @param accStartDate
     *            The value that is required to be registered as Account Start
     *            Date
     * 
     */

    public void setAccStartDate(String accStartDate) {

        this.accStartDate = accStartDate;

    }

    /**
     * 
     * @return Account End Date
     * 
     */

    public String getAccEndDate() {

        return this.accEndDate;

    }

    /**
     * 
     * @param accEndDate
     *            The value that is required to be registered as Account End
     *            Date
     * 
     */

    public void setAccEndDate(String accEndDate) {

        this.accEndDate = accEndDate;

    }

    /**
     * 
     * @return Account Status
     * 
     */

    public String getAccStatus() {

        return this.accStatus;

    }

    /**
     * 
     * @param accStatus
     *            The value that is required to be registered as Account Status
     * 
     */

    public void setAccStatus(String accStatus) {

        this.accStatus = accStatus;

    }

    /**
     * 
     * @return Value of Account Mail Flag
     * 
     */

    public String getAccMailFlag() {

        return this.accMailFlag;

    }

    /**
     * 
     * @param accMailFlag
     *            The value that is required to be registered as Account Mail
     *            Flag
     * 
     */

    public void setAccMailFlag(String accMailFlag) {

        this.accMailFlag = accMailFlag;

    }

    /**
     * 
     * @return Value of Account Public Flag
     * 
     */

    public String getAccPubFlag() {

        return this.accPubFlag;

    }

    /**
     * 
     * @param accPubFlag
     *            The value that is required to be registered as Account Public
     *            Flag
     * 
     */

    public void setAccPubFlag(String accPubFlag) {

        this.accPubFlag = accPubFlag;

    }

    /**
     * 
     * @return Account Note
     * 
     */

    public String getAccNote() {

        return this.accNote;

    }

    /**
     * 
     * @param accNote
     *            The value that is required to be registered as Account Note
     * 
     */

    public void setAccNote(String accNote) {

        this.accNote = accNote;

    }

    /**
     * 
     * @return The Id of the Account Creator
     * 
     */

    public String getAccCreator() {

        return this.accCreator;

    }

    /**
     * 
     * @param accCreator
     *            The User Id that is required to be registered as Account
     *            Creator
     * 
     */

    public void setAccCreator(String accCreator) {

        this.accCreator = accCreator;

    }

    /**
     * 
     * @return Account Logo
     * 
     */

    public String getAccLogo() {

        return this.accLogo;

    }

    /**
     * 
     * @param accLogo
     *            The value that is required to be registered as Account Logo
     * 
     */

    public void setAccLogo(String accLogo) {

        this.accLogo = accLogo;

    }

    /**
     * 
     * @return Account Max Users
     * 
     */

    public String getAccMaxUsr() {

        return this.accMaxUsr;

    }

    /**
     * 
     * @param accMaxUsr
     *            The value that is required to be registered as Account Max
     *            Users
     * 
     */

    public void setAccMaxUsr(String accMaxUsr) {

        this.accMaxUsr = accMaxUsr;

    }

    /**
     * 
     * @return Account Max Storage
     * 
     */

    public String getAccMaxStorage() {

        return this.accMaxStorage;

    }
    
    //JM:
    /**
     * 
     * @param accMaxPortal
     *            The value that is required to be registered as Account Max
     *            Portals
     * 
     */

    public void setAccMaxPortal(String accMaxPortals) {

        this.accMaxPortal = accMaxPortals;

    }

    /**
     * 
     * @return Account Max Portal
     * 
     */

    public String getAccMaxPortal() {

        return this.accMaxPortal;

    }
            

    /**
     * 
     * @param accMaxStorage
     *            The value that is required to be registered as Account Max
     *            Storage
     * 
     */

    public void setAccMaxStorage(String accMaxStorage) {

        this.accMaxStorage = accMaxStorage;

    }

    /**
     * 
     * @return Account Module Rights
     * 
     */

    public String getAccModRight() {

        return this.accModRight;

    }

    /**
     * 
     * @param accModRight
     *            The value that is required to be registered as Account Module
     *            Right
     * 
     */

    public void setAccModRight(String accModRight) {

        this.accModRight = accModRight;

    }

    /**
     * 
     * @return Creator
     * 
     */

    public String getCreator() {

        return this.creator;

    }

    /**
     * 
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     * 
     */

    public void setCreator(String creator) {

        this.creator = creator;

    }

    /**
     * 
     * @return Last Modified By
     * 
     */

    public String getModifiedBy() {

        return this.modifiedBy;

    }

    /**
     * 
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     * 
     */

    public void setModifiedBy(String modifiedBy) {

        this.modifiedBy = modifiedBy;

    }

    /**
     * 
     * @return IP Address
     * 
     */

    public String getIpAdd() {

        return this.ipAdd;

    }

    /**
     * 
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     * 
     */

    public void setIpAdd(String ipAdd) {

        this.ipAdd = ipAdd;

    }
    
    
    
    //Added  by Manimaran for the Enh.#GL7


    public void setAutoGenStudy(String autoGenStudy) {

        this.autoGenStudy = autoGenStudy;

    }

    
    public String getAutoGenStudy() {

        return this.autoGenStudy;

    }
    
    
    
    public void setAutoGenPatient(String autoGenPatient) {

        this.autoGenPatient = autoGenPatient;

    }

    
    public String getAutoGenPatient() {

        return this.autoGenPatient;

    }
            

    // END OF GETTER SETTER METHODS

    /**
     * 
     * Defines an AccountJB object with the specified Account Id
     * 
     */

    public AccountJB(int accId) {

        setAccId(accId);

    }
    

    /**
	 * @return the ldapEnabled
	 */
	public String getLdapEnabled() {
		return ldapEnabled;
	}

	/**
	 * @param ldapEnabled the ldapEnabled to set
	 */
	public void setLdapEnabled(String ldapEnabled) {
		this.ldapEnabled = ldapEnabled;
	}
	
	/**
	 * @return the ldapEnabled
	 */
	public String getWSEnabled() {
		return wsEnabled;
	}

	/**
	 * @param ldapEnabled the ldapEnabled to set
	 */
	public void setWSEnabled(String wsEnabled) {
		this.wsEnabled = wsEnabled;
	}

	
    /**
	 * @return the accSkin
	 */
	public String getAccSkin() {
		return accSkin;
	}

	/**
	 * @param ldapEnabled the ldapEnabled to set
	 */
	public void setAccSkin(String accSkin) {
		this.accSkin= accSkin;
	}
	
	
	
	/**
     * 
     * Defines an AccountJB object with default values for fields
     * 
     */

    public AccountJB() {

        Rlog.debug("account", "AccountJB.AccountJB() ");

    };

    /**
     * 
     * Defines an AccountJB object with the specified values for the fields
     * 
     * 
     * 
     */

    public AccountJB(int accId, String accType, String accRoleType,
            String accName,

            String accStartDate, String accEndDate, String accStatus,

            String accMailFlag, String accPubFlag, String accNote,

            String accCreator, String accLogo, String accMaxUsr, String accMaxPortal,

            String accMaxStorage, String accModRight, String creator,

            String modifiedBy, String ipAdd,String ldapEnabled, String wsEnabled,String accSkin) {

        setAccId(accId);

        setAccType(accType);

        setAccRoleType(accRoleType);

        setAccName(accName);

        setAccStartDate(accStartDate);

        setAccEndDate(accEndDate);

        setAccStatus(accStatus);

        setAccMailFlag(accMailFlag);

        setAccPubFlag(accPubFlag);

        setAccNote(accNote);

        setAccCreator(accCreator);

        setAccLogo(accLogo);

        setAccMaxUsr(accMaxUsr);
        
        setAccMaxPortal(accMaxPortal);//JM:

        setAccMaxStorage(accMaxStorage);

        setAccModRight(accModRight);

        setCreator(creator);

        setModifiedBy(modifiedBy);

        setIpAdd(ipAdd);
        
        setLdapEnabled(ldapEnabled);
        
        setWSEnabled(wsEnabled);

        setAccSkin(accSkin);
        
        setAutoGenStudy(autoGenStudy); //KM
        
        setAutoGenPatient(autoGenPatient);
        Rlog.debug("account", "AccountJB.AccountJB(all parameters)");

    }

    /**
     * 
     * Retrieves the details of an account in AccountStateKeeper Object
     * 
     * 
     * 
     * @return AccountStateKeeper consisting of the account details
     * 
     * @see AccountStateKeeper
     * 
     */

    public AccountBean getAccountDetails() {

        AccountBean ask = null;

        try {

            AccountAgentRObj accountAgentRObj = EJBUtil.getAccountAgentHome();

            ask = accountAgentRObj.getAccountDetails(this.accId);

            Rlog.debug("account",
                    "AccountJB.getAccountDetails() AccountStateKeeper " + ask);

        } catch (Exception e) {

            Rlog.fatal("account", "Error in getAccountDetails() in AccountJB "
                    + e);

        }

        if (ask != null) {

            this.accId = ask.getAccId();

            this.accType = ask.getAccType();

            this.accRoleType = ask.getAccRoleType();

            this.accName = ask.getAccName();

            this.accStartDate = ask.getAccStartDate();

            this.accEndDate = ask.getAccEndDate();

            this.accStatus = ask.getAccStatus();

            this.accMailFlag = ask.getAccMailFlag();

            this.accPubFlag = ask.getAccPubFlag();

            this.accNote = ask.getAccNote();

            this.accCreator = ask.getAccCreator();

            this.accLogo = ask.getAccLogo();

            this.accMaxUsr = ask.getAccMaxUsr();

            this.accMaxPortal = ask.getAccMaxPortal();//JM:            

            this.accMaxStorage = ask.getAccMaxStorage();

            this.accModRight = ask.getAccModRight();

            this.creator = ask.getCreator();

            this.modifiedBy = ask.getModifiedBy();

            this.ipAdd = ask.getIpAdd();
            
            this.ldapEnabled=ask.getLdapEnabled();
            
            this.wsEnabled=ask.getWSEnabled();
            
            this.accSkin=ask.getAccSkin();
            
            this.autoGenStudy = ask.getAutoGenStudy(); //KM
            
            this.autoGenPatient = ask.getAutoGenPatient();

        }

        return ask;

    }

    /**
     * 
     * Stores the details of an account in the database
     * 
     */

    public void setAccountDetails()

    {

        try {

            AccountAgentRObj accountAgentRObj = EJBUtil.getAccountAgentHome();

            this.setAccId(accountAgentRObj.setAccountDetails(this
                    .createAccountStateKeeper()));

            Rlog.debug("account", "AccountJB.setAccountDetails()");

        } catch (Exception e) {

            Rlog.fatal("account", "Error in setAccountDetails() in AccountJB "
                    + e);

        }

    }

    /**
     * 
     * Saves the modified details of the account to the database
     * 
     * 
     * 
     * @return 0 - If update succeeds <BR>
     * 
     * -2 - If update fails
     * 
     */

    public int updateAccount()

    {

        int output;

        try {

            AccountAgentRObj accountAgentRObj = EJBUtil.getAccountAgentHome();

            output = accountAgentRObj.updateAccount(this
                    .createAccountStateKeeper());

        }

        catch (Exception e) {

            Rlog.fatal("account",
                    "EXCEPTION IN SETTING ACCOUNT DETAILS TO  SESSION BEAN "
                            + e);
            return -2;

        }

        return output;

    }

    /**
     * 
     * Uses the values of the current AccountJB object to create an
     * AccountStateKeeper Object
     * 
     * 
     * 
     * @return AccountStateKeeper object with all the details of the account
     * 
     * @see AccountStateKeeper
     * 
     */

    public AccountBean createAccountStateKeeper()

    {

        Rlog.debug("account", "AccountJB.createAccountStateKeeper ");

        return new AccountBean(new Integer(accId), accType, (accRoleType),
                accName, accStartDate, accEndDate, accStatus, accMailFlag,
                accPubFlag, accNote, accCreator, accLogo, accMaxUsr,accMaxPortal,
                accMaxStorage, accModRight, creator, modifiedBy, ipAdd,ldapEnabled, wsEnabled,accSkin, autoGenStudy, autoGenPatient);

    }

    public String getCreationDate(int accId) {

        String creation_date = "";

        try {

            Rlog.debug("account", "AccountJB.getCreationDate starting");

            AccountAgentRObj accountAgentRObj = EJBUtil.getAccountAgentHome();
            ;

            Rlog.debug("account", "AccountJB.getCreationDate after remote");

            creation_date = accountAgentRObj.getCreationDate(accId);

            Rlog.debug("account", "AccountJB.getCreationDate after call");

        } catch (Exception e) {

            Rlog.fatal("account",
                    "Error in getCreationDate(int accId) in AccountJB " + e);

        }

        return creation_date;

    }

    public int copyAccount(int accId) {

        int ret = 0;

        try {

            Rlog.debug("account", "AccountJB.copyAccount starting");

            AccountAgentRObj accountAgentRObj = EJBUtil.getAccountAgentHome();

            Rlog.debug("account", "AccountJB.copyAccount after remote");

            ret = accountAgentRObj.copyAccount(accId);

            Rlog.debug("account", "AccountJB.copyAccount after call");

        } catch (Exception e) {

            Rlog.fatal("account",
                    "Error in copyAccount(int accId) in AccountJB " + e);

        }

        return ret;

    }

    public AccountDao getAccounts() {

        AccountDao accountDao = new AccountDao();

        try {

            Rlog.debug("account", "AccountJB.getAccounts starting");

            AccountAgentRObj accountAgentRObj = EJBUtil.getAccountAgentHome();

            Rlog.debug("account", "AccountJB.getAccounts after remote");

            accountDao = accountAgentRObj.getAccounts();

            Rlog.debug("account", "AccountJB.getAccounts after call");

        } catch (Exception e) {

            Rlog.fatal("account", "Error in getAccounts() in AccountJB " + e);

        }

        return accountDao;

    }
    public AccountDao getLoginModuleDetails(int accId) {

        AccountDao accountDao = new AccountDao();

        try {

            AccountAgentRObj accountAgentRObj = EJBUtil.getAccountAgentHome();
            accountDao = accountAgentRObj.getLoginModuleDetails(accId);

        } catch (Exception e) {

            Rlog.fatal("account", "Error in getLoginModuleDetails() in AccountJB " + e);
            e.printStackTrace();

        }

        return accountDao;

    }
    public AccountDao getLoginModule(int loginModule) {

        AccountDao accountDao = new AccountDao();

        try {

            AccountAgentRObj accountAgentRObj = EJBUtil.getAccountAgentHome();
            accountDao = accountAgentRObj.getLoginModule(loginModule);

        } catch (Exception e) {

            Rlog.fatal("account", "Error in getLoginModuleDetails() in AccountJB " + e);
            e.printStackTrace();

        }

        return accountDao;

    }
    
    //JM: Returns the total number of portals present in an user's Account
    
    
    public int getPortalsCount(int accId) {

        int ret = 0;

        try {

            Rlog.debug("account", "AccountJB.getPortalsCount starting");

            AccountAgentRObj accountAgentRObj = EJBUtil.getAccountAgentHome();

            Rlog.debug("account", "AccountJB.getPortalsCount after remote");

            ret = accountAgentRObj.getPortalsCount(accId);

            Rlog.debug("account", "AccountJB.getPortalsCount after call");

        } catch (Exception e) {

            Rlog.fatal("account",
                    "Error in getPortalsCount(int accId) in AccountJB " + e);

        }

        return ret;

    }
    
    

    /*
     * 
     * generates a String of XML for the account details entry form.<br><br>
     * 
     * 
     * 
     * 
     * 
     * public String toXML() {
     * 
     * Rlog.debug("account","AccountJB.toXML()");
     * 
     * return new String ("<?xml version=\"1.0\"?>" + "<?cocoon-process
     * type=\"xslt\"?>" + "<?xml-stylesheet href=\"/stylesheet/form-html.xsl\"
     * type=\"text/xsl\"?>" + "<?xml-stylesheet
     * href=\"/stylesheet/form-wml.xsl\" type=\"text/xsl\" media=\"wap\"?>" +
     * 
     * //to be done "<form action=\"accountsummary.jsp\">" + "<head>" +
     * 
     * //to be done "<title>Account Summary </title>" + "</head>" + "<input
     * type=\"hidden\" name=\"accId\" value=\"" + this.getAccId() + "\"
     * size=\"10\"/>" + "<input type=\"text\" name=\"accountType\" value=\"" +
     * this.getAccType() + "\" size=\"30\"/>" + "<input type=\"text\" name=\"
     * accName\" value=\"" + this.getAccName() + "\" size=\"30\"/>" + "<input
     * type=\"text\" name=\" accStartDate\" value=\"" + this.getAccStartDate()+
     * "\" size=\"12\"/>" + "<input type=\"text\" name=\" accEndDate\"
     * value=\"" + this.getAccEndDate() + "\" size=\"12\"/>" + "<input
     * type=\"text\" name=\" accStatus\" value=\"" + this.getAccStatus() + "\"
     * size=\"200\"/>" + "<input type=\"text\" name=\" accMailFlag\" value=\"" +
     * this.getAccMailFlag()+ "\" size=\"200\"/>" + "<input type=\"text\"
     * name=\" accPubFlag\" value=\"" + this. getAccPubFlag()+ "\"
     * size=\"200\"/>" + "<input type=\"text\" name=\" accNote\" value=\"" +
     * this.getAccNote()+ "\" size=\"2000\"/>" + "<input type=\"text\" name=\"
     * accCreator\" value=\"" + this.getAccCreator() + "\" size=\"10\"/>" + "</form>" );
     * 
     *  }
     * 
     * 
     * 
     */

}// end of class

