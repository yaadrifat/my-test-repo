package com.velos.eres.web.moreDetails;



/* IMPORT STATEMENTS */

import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import com.velos.eres.business.common.MoreDetailsDao;
import com.velos.eres.business.moreDetails.impl.MoreDetailsBean;
import com.velos.eres.service.moreDetailsAgent.MoreDetailsAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* END OF IMPORT STATEMENTS */


public class MoreDetailsJB {

    

    private int id;

    /**
     * The foreign key reference to modules
     */
    private String modId;
    private String modName;

    /**
     * The Id Type
     */
 
  
    private String modElementId;

    /**
     * The Alternate Study Id
     */
    private String modElementData;

    private String creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    private String modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    private String ipAdd;

    // /////////////////////////////////////////////////////////////////////////////////

    // GETTER AND SETTER METHODS

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

   

    /**
	 * @return the modElementData
	 */
	public String getModElementData() {
		return modElementData;
	}

	/**
	 * @param modElementData the modElementData to set
	 */
	public void setModElementData(String modElementData) {
		this.modElementData = modElementData;
	}

	/**
	 * @return the modElementId
	 */
	public String getModElementId() {
		return modElementId;
	}

	/**
	 * @param modElementId the modElementId to set
	 */
	public void setModElementId(String modElementId) {
		this.modElementId = modElementId;
	}

	/**
	 * @return the modId
	 */
	public String getModId() {
		return modId;
	}

	/**
	 * @param modId the modId to set
	 */
	public void setModId(String modId) {
		this.modId = modId;
	}

	/**
	 * @return the modName
	 */
	public String getModName() {
		return modName;
	}

	/**
	 * @param modName the modName to set
	 */
	public void setModName(String modName) {
		this.modName = modName;
	}

	/**
     * @return The Creator of the Form Section
     */

    public String getCreator() {
        return this.creator;
    }

    /**
     * 
     * @param to
     *            set the Creator of the Form Section
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param the
     *            set unique ID of the User
     */

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return the IP Address of the Form Section request
     */

    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param set
     *            the IP Address of the Form Section request
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // //////////////////////////////////////////////////////////////////

    // END OF THE GETTER AND SETTER METHODS

   

    public MoreDetailsJB(int id) {
        setId(id);
    }

    /**
     * Default Constructor
     */

    public MoreDetailsJB() {
        
    }

    public MoreDetailsJB(int id, String modId, String modName,
            String modElementId, String modElementData,String creator, String modifiedBy,
            String ipAdd) {
    	setId(id);
        setModId(modId);
        setModName(modName);
        setModElementId(modElementId);
        setModElementData(modElementData);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }
    

   

    public MoreDetailsBean getMoreDetailsBean() {
        MoreDetailsBean mdb = null;
        try {
            MoreDetailsAgentRObj mdAgentRObj = EJBUtil.getMoreDetailsAgentHome();
            mdb = mdAgentRObj.getMoreDetailsBean(this.id);
            
        } catch (Exception e) {
            Rlog.fatal("moredetails", "Error in  getNoreDetails" + e);
            e.printStackTrace();
        }

        if (mdb != null) {
        	setId(mdb.getId());
            setModId(mdb.getModId());
            setModName(mdb.getModName());
            setModElementId(mdb.getModElementId());
            setModElementData(mdb.getModElementData());
            setCreator(mdb.getCreator());
            setModifiedBy(mdb.getModifiedBy());
            setIpAdd(mdb.getIpAdd());

        }

        return mdb;

    }

   
    public int setMoreDetails() {
        int toReturn;
        try {

            MoreDetailsAgentRObj mdAgentRObj = EJBUtil.getMoreDetailsAgentHome();

            MoreDetailsBean mdb= new MoreDetailsBean();

            mdb = this.createMoreDetailsStateKeeper();

            toReturn = mdAgentRObj.setMoreDetails(mdb);
            this.setId(toReturn);

           

            return toReturn;

        } catch (Exception e) {
            Rlog.fatal("moredetails", "Error in setMoreDetails() in MoreDetailsJB "
                    + e);
            e.printStackTrace();
            return -2;
        }
    }

    
    public MoreDetailsBean createMoreDetailsStateKeeper() {

        return new MoreDetailsBean(id,  modId,modName,
                modElementId,  modElementData,creator,  modifiedBy,
                 ipAdd);

    }

   
    public int updateMoreDetails() {
        int output;
        try {
            MoreDetailsAgentRObj mdAgentRObj = EJBUtil.getMoreDetailsAgentHome();
            output = mdAgentRObj.updateMoreDetails(this
                    .createMoreDetailsStateKeeper());
        } catch (Exception e) {
            Rlog
                    .debug("moredetails",
                            "EXCEPTION IN SETTING DETAILS TO  SESSION BEAN"
                                    + e);
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    public int removeMoreDetails() {

        int output;

        try {

            MoreDetailsAgentRObj mdAgentRObj = EJBUtil.getMoreDetailsAgentHome();
            output = mdAgentRObj.removeMoreDetails(this.id);
            return output;

        } catch (Exception e) {
            Rlog.fatal("moredetails", "in moredetailsJB -removeMoreDetails() method" + e);
            e.printStackTrace();
            return -1;
        }

    }

   

    public int createMultipleMoreDetails(MoreDetailsBean mdb , String defUserGroup ) {

        int ret = 0;

        try {
            
            MoreDetailsAgentRObj mdAgentRObj = EJBUtil.getMoreDetailsAgentHome();
            ret = mdAgentRObj.createMultipleMoreDetails(mdb,  defUserGroup );

            return ret;
        } catch (Exception e) {
            Rlog.fatal("moredetails", "createMultipleMoreDetails in MoreDetailsIdJB " + e);
            e.printStackTrace();
            return -2;
        }

    }

   
    public int updateMultipleMoreDetails(MoreDetailsBean mdb) {

        int ret = 0;

        try {
            
            MoreDetailsAgentRObj mdAgentRObj = EJBUtil.getMoreDetailsAgentHome();
            ret = mdAgentRObj.updateMultipleMoreDetails(mdb);

            return ret;
        } catch (Exception e) {
            Rlog.fatal("moredetails", "updateMultipleMoreDetails() in MoreDetailsJB " + e);
            e.printStackTrace();
            return -2;
        }

    }

    public MoreDetailsDao getMoreDetails(int studyId,String modName, String defUserGroup ) {

        MoreDetailsDao mdDao = new MoreDetailsDao();

        try {
          
            MoreDetailsAgentRObj mdAgentRObj = EJBUtil.getMoreDetailsAgentHome();

            mdDao = mdAgentRObj.getMoreDetails(studyId,modName, defUserGroup );

            return mdDao;
        } catch (Exception e) {
            Rlog.fatal("moredetails", "getMoreDetails() in MoreDetailsJB " + e);
            e.printStackTrace();
            return null;
        }

    }

    public MoreDetailsDao getEventMoreDetails(int studyId,String modName,String defUserGroup,int finDetRight) {
        MoreDetailsDao mdDao = new MoreDetailsDao();

        try {
            MoreDetailsAgentRObj mdAgentRObj = EJBUtil.getMoreDetailsAgentHome();
            mdDao = mdAgentRObj.getEventMoreDetails(studyId,modName,defUserGroup,finDetRight);
            return mdDao;
        } catch (Exception e) {
            Rlog.fatal("moredetails", "getEventMoreDetails() in MoreDetailsJB " + e);
            e.printStackTrace();
            return null;
        }

    }
    
    public void saveMoredetails(String modName,HttpServletRequest req,String userId,String loggedInUsr){
    	


		int success = 0;
		
		try{
			
			//HttpSession tSession = request.getSession(true);
			//String userId = (String) tSession.getAttribute("userId");
			//String ipAdd = (String)tSession.getAttribute("ipAdd");
			//String defUserGroup = (String) tSession.getAttribute("defUserGroup");
			
			ArrayList arRecordType = new ArrayList();
			ArrayList arAlternateId = new ArrayList();
			ArrayList arAlternateValues = new ArrayList();
			ArrayList arId = new ArrayList();

			Enumeration params = req.getParameterNames();
			
			while (params.hasMoreElements()) {
			    
				String param = (String)params.nextElement();
			    
				if (param.startsWith("alternateId") && (!param.endsWith("Checks"))) {
			    	
					String paramVal = req.getParameter(param);
			    	
					if (null != paramVal){
			    		String mdId = param.substring(param.indexOf("alternateId")+("alternateId".length()));
			    		arAlternateId.add(mdId);
			    		arAlternateValues.add(paramVal);
			    		paramVal = req.getParameter("recordType"+mdId);
				    	if (null != paramVal){
				    		arRecordType.add(paramVal);
				    	}
				    	paramVal = req.getParameter("id"+mdId);
				    	if (null != paramVal){
				    		arId.add(paramVal);
				    	}
			      	}
			    }
			}

			MoreDetailsBean mdbMain = new MoreDetailsBean();

			ArrayList alElementData = arAlternateValues;

				int len = alElementData.size();
				
				for ( int j = 0 ; j< len ; j++)
				{
					MoreDetailsBean mdbTemp = new MoreDetailsBean();
					
					if   ( ( !(StringUtil.isEmpty ((String)arAlternateValues.get(j))) &&  "N".equals((String)arRecordType.get(j))) || "M".equals((String)arRecordType.get(j)))
					{
						mdbTemp.setId(StringUtil.stringToNum((String)arId.get(j)));
						mdbTemp.setModId(userId) ;
						mdbTemp.setModName(modName) ;
						mdbTemp.setModElementId((String)arAlternateId.get(j));
						mdbTemp.setModElementData((String)arAlternateValues.get(j));
						mdbTemp.setRecordType((String)arRecordType.get(j));
						if ("N".equals((String)arRecordType.get(j)))
						{
							mdbTemp.setCreator(loggedInUsr);
						}else
						{
							mdbTemp.setModifiedBy(loggedInUsr);
						}
						mdbTemp.setIpAdd(ipAdd);
						mdbMain.setMoreDetailBeans(mdbTemp);
					}
				}
				MoreDetailsJB moreDetails = new MoreDetailsJB();
				
				success = moreDetails.createMultipleMoreDetails(mdbMain,"0");
				
		} catch (Exception e){
			e.printStackTrace();
			success = 0;
		}
		
    	
    }

}
