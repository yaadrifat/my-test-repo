/*
 * Classname : AppendixJB.java
 * 
 * Version information
 *
 * Date 03/02/2001
 * 
 * Copyright notice: Aithent
 */
package com.velos.eres.web.appendix;
import java.util.Hashtable;
import com.velos.eres.business.appendix.impl.AppendixBean;
import com.velos.eres.business.common.AppendixDao;
import com.velos.eres.service.appendixAgent.AppendixAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
/**
 * Client side bean for the Study Appendix
 * 
 * @author Sunita Pahwa
 */
public class AppendixJB {
    /**
     * the Appendix Id
     */
    private int id;
    /**
     * 
     * 
     * appendix type
     * 
     * 
     */
    private String type;
    /**
     * 
     * 
     * the url/file name
     * 
     * 
     */
    private String url_file;
    /**
     * appendix description
     */
    private String desc;
    /**
     * appendix Public Flag
     */
    private String pubflag;
    /**
     * appendix study
     */
    private String study;
    /**
     * appendix studyVerId
     */
    private String studyVerId;
    private String filename;
    private String fileSize;
    /*
     * creator
     */
    private String creator;
    /*
     * last modified by
     */
    private String modifiedBy;
    /*
     * IP Address
     */
    private String ipAdd;
    // GETTER SETTER METHODS
    public int getId()
    {
        return this.id;
    }
    public void setId(int id)
    {
        this.id = id;
    }
    public String getAppendixType()
    {
        return this.type;
    }
    public void setAppendixType(String type)
    {
        this.type = type;
    }
    public String getAppendixPubFlag()
    {
        return this.pubflag;
    }
    public void setAppendixPubFlag(String pubflag)
    {
        this.pubflag = pubflag;
    }
    public String getAppendixUrl_File()
    {
        return this.url_file;
    }
    public void setAppendixUrl_File(String url_file)
    {
        this.url_file = url_file;
    }
    public String getAppendixDescription()
    {
        return this.desc;
    }
    public void setAppendixDescription(String desc)
    {
        this.desc = desc;
    }
    public String getAppendixStudy() {
        return this.study;
    }
    public void setAppendixStudy(String study) {
        this.study = study;
    }
    public String getAppendixStudyVer() {
        return this.studyVerId;
    }
    public void setAppendixStudyVer(String studyVerId) {
        this.studyVerId = studyVerId;
    }
    public String getAppendixSavedFileName()
    {
        return this.filename;
    }
    public void setAppendixSavedFileName(String filename)
    {
        this.filename = filename;
    }
    public String getFileSize()
    {
        return this.fileSize;
    }
    public void setFileSize(String fileSize)
    {
        this.fileSize = fileSize;
    }
    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }
    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }
    /**
     * @return Last Modified By
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }
    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }
    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }
    // END OF GETTER SETTER METHODS
    // CONSTRUCTOR TO CREATE appendix OBJ WITH ID
    public AppendixJB(String id) {
        Rlog.debug("Appendix", "appendix JB Create with appendix id");
        int appId = 0;
        appId = EJBUtil.stringToNum(id);
        setId(appId);
    }
    // DEFAULT CONSTRUCTOR
    public AppendixJB() {
        Rlog.debug("Appendix", "appendix JB : Default Constructor");
    }
    /**
     * 
     * 
     * 
     * 
     * 
     */
    public AppendixBean getAppendixDetails() {
        AppendixBean ask = null;
        try
        {
            AppendixAgentRObj aa_ro = EJBUtil.getAppendixAgentHome();
            ask = aa_ro.getAppendixDetails(this.id);
        }
        catch (Exception e)
        {
            Rlog.debug("Appendix", "EXception in calling session bean");
        }
        if (ask != null)
        {
            this.id = ask.getAppendixId();
            this.type = ask.getAppendixType();
            this.url_file = ask.getAppendixUrl_File();
            this.desc = ask.getAppendixDescription();
            this.pubflag = ask.getAppendixPubFlag();
            this.study = ask.getAppendixStudy();
            this.studyVerId = ask.getAppendixStudyVer();
            this.filename = ask.getAppendixSavedFileName();
            this.fileSize = ask.getFileSize();
            this.creator = ask.getCreator();
            this.modifiedBy = ask.getModifiedBy();
            this.ipAdd = ask.getIpAdd();
        }
        return ask;
    }
    public int setAppendixDetails() {
        int apndxId = 0;
        try {
            AppendixAgentRObj aa_ro = EJBUtil.getAppendixAgentHome();
            apndxId = aa_ro
                    .setAppendixDetails(this.createAppendixStateKeeper());
        }
        catch (Exception e) {
            Rlog.debug("Appendix", "in session bean - set appendix details");
            return -1;
        }
        return apndxId;
    }
    public int removeAppendix() {
        int output;
        try {
            AppendixAgentRObj aa_ro = EJBUtil.getAppendixAgentHome();
            output = aa_ro.removeAppendix(this.id);
            return output;
        }
        catch (Exception e)
        {
            Rlog.debug("Appendix", "in session bean - remove appendix details");
            return -1;
        }
    }
    
    // Overloaded for INF-18183 ::: AGodara
    public int removeAppendix(Hashtable<String, String> auditInfo) {
        int output;
        try {
            AppendixAgentRObj aa_ro = EJBUtil.getAppendixAgentHome();
            output = aa_ro.removeAppendix(this.id,auditInfo);
            return output;
        }
        catch (Exception e)
        {
            Rlog.debug("Appendix", "in remove appendix details- removeAppendix(Hashtable<String, String> auditInfo)");
            return -1;
        }
    }
    public int updateAppendix()
    {
        int output;
        try {
            AppendixAgentRObj appendixAgentRObj = EJBUtil
                    .getAppendixAgentHome();
            output = appendixAgentRObj.updateAppendix(this
                    .createAppendixStateKeeper());
        }
        catch (Exception e) {
            Rlog.debug("Appendix",
                    "EXCEPTION IN SETTING APPENDIX DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }
    // places bean attributes into a CustomerStateHolder.
    public AppendixBean createAppendixStateKeeper()
    {
        return new AppendixBean(id, type, url_file, desc, pubflag, study,
                studyVerId, filename, fileSize, creator, modifiedBy, ipAdd);
    }
    public AppendixDao getByStudyVerId(int studyVerId) {
        AppendixDao appendixDao = new AppendixDao();
        try {
            Rlog.debug("appendix", "AppendixJB.getByStudyVerId starting");
            AppendixAgentRObj appendixAgentRObj = EJBUtil
                    .getAppendixAgentHome();
            appendixDao = appendixAgentRObj.getByStudyVerId(studyVerId);
            Rlog.debug("appendix", "AppendixJB.getByStudyVerId after Dao");
            return appendixDao;
        } catch (Exception e) {
            Rlog.fatal("appendix", "Error in getByStudyVerId in AppendixJB "
                    + e);
            return null;
        }
    }
    public AppendixDao getAppendixValuesPublic(int studyVerId) {
        AppendixDao appendixDao = new AppendixDao();
        try {
            Rlog.debug("appendix",
                    "AppendixJB.getAppendixValuesPublic starting");
            AppendixAgentRObj appendixAgentRObj = EJBUtil
                    .getAppendixAgentHome();
            appendixDao = appendixAgentRObj.getAppendixValuesPublic(studyVerId);
            Rlog.debug("appendix",
                    "AppendixJB.getAppendixValuesPublic after Dao");
            return appendixDao;
        } catch (Exception e) {
            Rlog.fatal("appendix",
                    "Error in getAppendixValuesPublic(int studyVerId) in AppendixJB "
                            + e);
            return null;
        }
    }
    public AppendixDao getByStudyIdAndType(int studyVerId, String type) {
        AppendixDao appendixDao = new AppendixDao();
        try {
            Rlog.debug("appendix", "AppendixJB.getByStudyIdAndType starting");
            AppendixAgentRObj appendixAgentRObj = EJBUtil
                    .getAppendixAgentHome();
            appendixDao = appendixAgentRObj.getByStudyIdAndType(studyVerId,
                    type);
            Rlog.debug("appendix", "AppendixJB.getByStudyIdAndType after Dao");
            return appendixDao;
        } catch (Exception e) {
            Rlog.fatal("appendix",
                    "Error in getByStudyIdAndType(int studyVerId, String type) in AppendixJB "
                            + e);
            return null;
        }
    }
    
    /** gets latest document for version category */
    public AppendixDao getLatestDocumentForCategory(String studyId,String versionCategory)
    {
        AppendixDao appendixDao = new AppendixDao();
        try {
         
            AppendixAgentRObj appendixAgentRObj = EJBUtil
                    .getAppendixAgentHome();
            appendixDao = appendixAgentRObj.getLatestDocumentForCategory(studyId,versionCategory);
            return appendixDao;
        } catch (Exception e) {
            Rlog.fatal("appendix",
                    "Exception in  getLatestDocumentForCategory(String studyId,String versionCategory) in AppendixJB "
                            + e);
            return null;
        }
    	
    }
    /**
     * 
     * 
     * generates a String of XML for the customer details entry form.<br>
     * <br>
     * 
     * 
     * Note that it is planned to encapsulate this detail in another object.
     * 
     * 
     */
    public String toXML()
    {
        return null;
    }// end of method
}// end of class
