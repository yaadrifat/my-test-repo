/*
 * Classname: 				StorageKitJB
 *
 * Version information: 	1.0
 *
 * Date: 					05Aug2009
 *
 * Copyright notice: 		Velos Inc
 *
 * Author: 					Jnanamay
 */

package com.velos.eres.web.storageKit;

/* IMPORT STATEMENTS */

import com.velos.eres.business.storageKit.impl.StorageKitBean;
import com.velos.eres.service.storageKitAgent.StorageKitAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.business.common.StorageKitDao;


/* END OF IMPORT STATEMENTS */

public class StorageKitJB {


    /**
     * Pk storage kit
     */
    private int pkStorageKit;

    /**
     * fk storage
     */
    private String fkStorage;

    /**
     * default specimen type
     */
    private String defSpecimenType;

    /**
     * default Processing type
     */
    private String defProcType;

    /**
     * default Processing sequence
     */
    private String defProcSeq;

    /**
     * default specimen quantity
     */
    private String defSpecQuant;

    /**
     * default specimen quantity unit
     */
    private String defSpecQuantUnit;

    /**
     * kit specimen disposition
     */
    private String kitSpecDisposition;

    /**
     * Kit specimen envt. constraints
     */
    private String kitSpecEnvtCons;

    /**
     * creator
     */
    private String creator;

    /**
     * last modified by
     */
    private String modifiedBy;

    /**
     * IP Address
     */
    private String ipAdd;



//  GETTERS AND SETTERS

    /**
	 * @return the pkStorageKit
	 */
	public int getPkStorageKit() {
		return pkStorageKit;
	}

	/**
	 * @param pkStorageKit the pkStorage kit to set
	 */
	public void setPkStorageKit(int pkStorageKit) {
		this.pkStorageKit = pkStorageKit;
	}

	/**
	 * @return the fkStorage
	 */
	public String getFkStorage() {
		return fkStorage;
	}
	/**
	 * @param fkStorage the fkStorage to set
	 */
	public void setFkStorage(String fkStorage) {
		this.fkStorage = fkStorage;
	}

	/**
	 * @return the defSpecimenType
	 */
	public String getDefSpecimenType() {
		return defSpecimenType;
	}
	/**
	 * @param defSpecimenType the defSpecimenType to set
	 */
	public void setDefSpecimenType(String defSpecimenType) {
		this.defSpecimenType = defSpecimenType;
	}

	/**
	 * @return the defProcType
	 */
	public String getDefProcType() {
		return defProcType;
	}
	/**
	 * @param defProcType the defProcType to set
	 */
	public void setDefProcType(String defProcType) {
		this.defProcType = defProcType;
	}

	/**
	 * @return the defProcSeq
	 */
	public String getDefProcSeq() {
		return defProcSeq;
	}
	/**
	 * @param defProcSeq the defProcSeq to set
	 */
	public void setDefProcSeq(String defProcSeq) {
		this.defProcSeq = defProcSeq;
	}

	/**
	 * @return the defSpecQuant
	 */
	public String getDefSpecQuant() {
		return defSpecQuant;
	}
	/**
	 * @param defSpecQuant the defSpecQuant to set
	 */
	public void setDefSpecQuant(String defSpecQuant) {
		this.defSpecQuant = defSpecQuant;
	}

	/**
	 * @return the defSpecQuantUnit
	 */
	public String getDefSpecQuantUnit() {
		return defSpecQuantUnit;
	}
	/**
	 * @param defSpecQuantUnit the defSpecQuantUnit to set
	 */
	public void setDefSpecQuantUnit(String defSpecQuantUnit) {
		this.defSpecQuantUnit = defSpecQuantUnit;
	}

	/**
	 * @return the kitSpecDisposition
	 */
	public String getKitSpecDisposition() {
		return kitSpecDisposition;
	}
	/**
	 * @param kitSpecDisposition the kitSpecDisposition to set
	 */
	public void setKitSpecDisposition(String kitSpecDisposition) {
		this.kitSpecDisposition = kitSpecDisposition;
	}

	/**
	 * @return the kitSpecEnvtCons
	 */
	public String getKitSpecEnvtCons() {
		return kitSpecEnvtCons;
	}
	/**
	 * @param kitSpecEnvtCons the kitSpecEnvtCons to set
	 */
	public void setKitSpecEnvtCons(String kitSpecEnvtCons) {
		this.kitSpecEnvtCons = kitSpecEnvtCons;
	}

	/**
	 * @return the creator
	 */
	public String getCreator() {
		return creator;
	}

	/**
	 * @param creator the creator to set
	 */
	public void setCreator(String creator) {
		this.creator = creator;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the ipAdd
	 */
	public String getIpAdd() {
		return ipAdd;
	}

	/**
	 * @param ipAdd the ipAdd to set
	 */
	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}

//	 END OF THE GETTER AND SETTER METHODS

	/**
     * Constructor
     *
     * @param pkStorageKit:
     *            Unique Id constructor
     *
     */

    public StorageKitJB(int pkStorageKit) {
    	setPkStorageKit(pkStorageKit);
    }

    /**
     * Default Constructor
     */

    public StorageKitJB() {
        Rlog.debug("storagekit", "StorageKitJB.StorageKitJB() ");
    }

	/**
	 * Paramterised constructor
	 * @param pkStorageKit
	 * @param fkStorage
	 * @param defSpecimenType
	 * @param defProcType
	 * @param defProcSeq
	 * @param defSpecQuant
	 * @param defSpecQuantUnit
	 * @param kitSpecDisposition
	 * @param kitSpecEnvtCons
	 * @param creator
	 * @param modifiedBy
	 * @param ipAdd
	 */
    public StorageKitJB(int pkStorageKit,
    	    String fkStorage,
    	    String defSpecimenType,
    	    String defProcType,
    	    String defProcSeq,
    	    String defSpecQuant,
    	    String defSpecQuantUnit,
    	    String kitSpecDisposition,
    	    String kitSpecEnvtCons,
    	    String creator,
    	    String modifiedBy,
    	    String ipAdd) {


		setPkStorageKit(pkStorageKit);
        setFkStorage(fkStorage);
        setDefSpecimenType(defSpecimenType);
        setDefProcType(defProcType);
        setDefProcSeq(defProcSeq);
        setDefSpecQuant(defSpecQuant);
        setDefSpecQuantUnit(defSpecQuantUnit);
        setKitSpecDisposition(kitSpecDisposition);
        setKitSpecEnvtCons(kitSpecEnvtCons);
        setCreator(creator);
		setModifiedBy(modifiedBy);
		setIpAdd(ipAdd);

	}


	 /**
     * Retrieves the details of an storagekit in StorageKitStateKeeper Object
     *
     * @return StorageKitStateKeeper consisting of the storageKit details
     * @see StorageKitStateKeeper
     */
    public StorageKitBean getStorageKitDetails() {
    	StorageKitBean ssk = null;
    	String parentstorage_Name = "";
    	//StorageDao sd = new StorageDao();


        try {
            StorageKitAgentRObj storageKitAgentRObj = EJBUtil.getStorageKitAgentHome();
            ssk = storageKitAgentRObj.getStorageKitDetails(this.pkStorageKit);
            Rlog.debug("storagekit", "In StorageKitJB.getStorageKitDetails() " + ssk);
        } catch (Exception e) {
            Rlog
                    .fatal("storagekit", "Error in getStorageKitDetails() in StorageKitJB "+ e);
        }
        if (ssk != null) {

        	copyStorageKitBeanAttributes(ssk);

        }

        return ssk;
    }


    public int setStorageKitDetails() {
        int output = 0;
        try {
        	StorageKitAgentRObj storageKitAgentRObj = EJBUtil.getStorageKitAgentHome();
            output = storageKitAgentRObj.setStorageKitDetails(this
                    .createStorageKitStateKeeper());

            this.setPkStorageKit(output);

        }catch (Exception e) {
            Rlog
                    .fatal("storagekit", "Error in setStorageKitDetails() in StorageKitJB "
                            + e);
          return -2;
        }
        return output;
    }


    public int updateStorageKit() {
        int output;
        try {
        	StorageKitAgentRObj storageKitAgentRObj = EJBUtil.getStorageKitAgentHome();
            output = storageKitAgentRObj.updateStorageKit(this.createStorageKitStateKeeper());
        } catch (Exception e) {
            Rlog.debug("storagekit",
                    "EXCEPTION IN SETTING STORAGE Kit DETAILS TO  SESSION BEAN"
                            + e.getMessage());

            return -2;
        }
        return output;
    }


    public StorageKitBean createStorageKitStateKeeper() {
        Rlog.debug("storagekit", "StorageKitJB.createStorageKitStateKeeper ");
        return new StorageKitBean(
        		this.pkStorageKit,
        	    this.fkStorage,
        	    this.defSpecimenType,
        	    this.defProcType,
        	    this.defProcSeq,
        	    this.defSpecQuant,
        	    this.defSpecQuantUnit,
        	    this.kitSpecDisposition,
        	    this.kitSpecEnvtCons,
        	    this.creator,
        	    this.modifiedBy,
        	    this.ipAdd);
    }



    /**
     * sets values of StorageKitBean attributes to the bean object
     *
     * @see StorageKitBean
     */
    public void copyStorageKitBeanAttributes(StorageKitBean ssk ) {
    	 if (ssk != null) {

		 	this.pkStorageKit  = ssk.getPkStorageKit();
		    this.fkStorage  = ssk.getFkStorage();
		    this.defSpecimenType  = ssk.getDefSpecimenType();
		    this.defProcType  = ssk.getDefProcType();
		    this.defProcSeq  = ssk.getDefProcSeq();
		    this.defSpecQuant  = ssk.getDefSpecQuant();
		    this.defSpecQuantUnit  = ssk.getDefSpecQuantUnit();
		    this.kitSpecDisposition  = ssk.getKitSpecDisposition();
		    this.kitSpecEnvtCons  = ssk.getKitSpecEnvtCons();
		    this.creator = ssk.getCreator();
            this.modifiedBy = ssk.getModifiedBy();
            this.ipAdd = ssk.getIpAdd();

        }

    }

  //JM: 21Aug2009:
    public StorageKitDao getStorageKitAttributes(int pkStorageKit) {
    	StorageKitDao stkDao = new StorageKitDao();
    	try {           
            StorageKitAgentRObj storageKitAgentRObj = EJBUtil.getStorageKitAgentHome();
            stkDao= storageKitAgentRObj.getStorageKitAttributes(pkStorageKit);
            return stkDao;

        } catch (Exception e) {
            Rlog.fatal("storagekit", "Error in getStorageKitAttributes(pkStorageKit) in StorageKitJB " + e);
        }
        return stkDao;
    }

  






}// end of class

