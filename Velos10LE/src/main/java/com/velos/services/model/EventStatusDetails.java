package com.velos.services.model;

import java.util.Date;

public class EventStatusDetails extends ServiceObject {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2277955930384247908L;
	
	private EventStatusIdentifier eventStatusIdentifier;
	private EventStatus eventStatus;
	private Date startDate;
	private Date endDate;
	private String notes;
	private User enteredBy;
	public EventStatusIdentifier getEventStatusIdentifier() {
		return eventStatusIdentifier;
	}
	public void setEventStatusIdentifier(EventStatusIdentifier eventStatusIdentifier) {
		this.eventStatusIdentifier = eventStatusIdentifier;
	}
	public EventStatus getEventStatus() {
		return eventStatus;
	}
	public void setEventStatus(EventStatus eventStatus) {
		this.eventStatus = eventStatus;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public User getUser() {
		return enteredBy;
	}
	public void setUser(User enteredBy) {
		this.enteredBy = enteredBy;
	}
	
	

}
