package com.velos.services.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="EventCostIdentifiers")
@XmlAccessorType(XmlAccessType.FIELD)
public class EventCostIdentifiers extends ServiceObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7724877986192393863L;
	
	private List<EventCostIdentifier> eventCostIdentifier;

	public List<EventCostIdentifier> getEventCostIdentifier() {
		return eventCostIdentifier;
	}

	public void setEventCostIdentifier(List<EventCostIdentifier> eventCostIdentifier) {
		this.eventCostIdentifier = eventCostIdentifier;
	}
	
	public void addEventCostIdentifier(EventCostIdentifier eventCostIdentifier){
		this.eventCostIdentifier.add(eventCostIdentifier);
	}

}
