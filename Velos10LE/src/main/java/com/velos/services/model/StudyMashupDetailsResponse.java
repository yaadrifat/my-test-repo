package com.velos.services.model;

import java.util.HashMap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Tarandeep Singh Bali
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="StuydMashupDetailsReponse")
public class StudyMashupDetailsResponse extends ServiceObject {	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4364192840091381363L;
	protected HashMap responeMap;

	public HashMap getResponeMap() {
		return responeMap;
	}

	public void setResponeMap(HashMap responeMap) {
		this.responeMap = responeMap;
	}

}
