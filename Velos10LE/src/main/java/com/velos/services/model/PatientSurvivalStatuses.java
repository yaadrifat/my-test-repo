
package com.velos.services.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Model class representing a list of patient survival status.
 * @author Raman
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="PatientSurvivalStatuses")
public class PatientSurvivalStatuses extends ServiceObject
{

    /**
     * 
     */
    private static final long serialVersionUID = -8643891583943218535L;
    /**
     * Generated serialVersionUID
     */
 
    protected List<PatientSurvivalStatus> patientSurvivalStatus=new ArrayList<PatientSurvivalStatus>();
    
    @NotNull 
    @Size(min=1)
    @Valid
	public List<PatientSurvivalStatus> getPatientSurvivalStatuses() {
        return patientSurvivalStatus;
    }

    public void setPatientSurvivalStatuses(List<PatientSurvivalStatus> patientSurvivalStatus) {
        this.patientSurvivalStatus = patientSurvivalStatus;
    }
    
    public void addPatientSurvivalStatus(PatientSurvivalStatus patientSurvivalStatus)
    {
    	this.patientSurvivalStatus.add(patientSurvivalStatus);
    }

    
}
