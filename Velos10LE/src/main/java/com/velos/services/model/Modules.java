package com.velos.services.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Tarandeep Singh Bali
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Modules")
public class Modules extends ServiceObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2198576508918076360L;
	
	private List<Module> module = new ArrayList<Module>();

	public List<Module> getModule() {
		return module;
	}

	public void setModule(List<Module> module) {
		this.module = module;
	}

	public void addModule(Module module)
	{
	    this.module.add(module); 
	}
	

}
