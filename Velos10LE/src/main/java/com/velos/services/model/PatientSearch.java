package com.velos.services.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="PatientSearch")
@XmlType(name="PatientSearchType")
public class PatientSearch extends ServiceObject{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -182849311570503293L;

	private String patientID= null;
	private String patFirstName= null;
	private String patLastName= null;
	private Date patDateofBirth= null;
	private OrganizationIdentifier patOrganization= null;
	private Code patSurvivalStat= null;
	private Code patSpecialty= null;
	private Code gender= null;
	//private String requestedRecordCount = null;
	private boolean isExactSearch = false;
	private String pageNumber = null;
	private String pageSize = null;
	
	//Added By Aman
	private String patientFacilityID= null;
	private Code race= null;
	private boolean isDistinctFacilityRequired = false;
	
	
	public enum sorting{
		 PatientID,
		 PatFirstName,
		 PatLastName,
		 Age,
		 Gender,
		 SurvivalStatus;
		 
	}
	private sorting sortBy;
	
	public sorting getSortBy() {
		return sortBy;
	}
	public void setSortBy(sorting sortBy) {
		this.sortBy = sortBy;
	}
	public enum ordering{
		ASCENDING ,
		DESCENDING ;
	}
	private ordering sortOrder;
		
	public ordering getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(ordering sortOrder) {
		this.sortOrder = sortOrder;
	}
	public String getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(String pageNumber) {
		this.pageNumber = pageNumber;
	}
	public String getPageSize() {
		return pageSize;
	}
	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}
	public String getPatientID() {
		return patientID;
	}
	public void setPatientID(String patientID) {
		this.patientID = patientID;
	}
	public String getPatFirstName() {
		return patFirstName;
	}
	public void setPatFirstName(String patFirstName) {
		this.patFirstName = patFirstName;
	}
	public String getPatLastName() {
		return patLastName;
	}
	public void setPatLastName(String patLastName) {
		this.patLastName = patLastName;
	}
	public Date getPatDateofBirth() {
		return patDateofBirth;
	}
	public void setPatDateofBirth(Date patDateofBirth) {
		this.patDateofBirth = patDateofBirth;
	}
	
	public OrganizationIdentifier getPatOrganization() {
		return patOrganization;
	}
	public void setPatOrganization(OrganizationIdentifier patOrganization) {
		this.patOrganization = patOrganization;
	}
	public Code getPatSurvivalStat() {
		return patSurvivalStat;
	}
	public void setPatSurvivalStat(Code patSurvivalStat) {
		this.patSurvivalStat = patSurvivalStat;
	}
	public Code getPatSpecialty() {
		return patSpecialty;
	}
	public void setPatSpecialty(Code patSpecialty) {
		this.patSpecialty = patSpecialty;
	}
	public Code getGender() {
		return gender;
	}
	public void setGender(Code gender) {
		this.gender = gender;
	}
/*	public String getRequestedRecordCount() {
		return requestedRecordCount;
	}
	public void setRequestedRecordCount(String requestedRecordCount) {
		this.requestedRecordCount = requestedRecordCount;
	}*/
	public boolean isExactSearch() {
		return isExactSearch;
	}
	public void setExactSearch(boolean isExactSearch) {
		this.isExactSearch = isExactSearch;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getPatientFacilityID() {
		return patientFacilityID;
	}
	public void setPatientFacilityID(String patientFacilityID) {
		this.patientFacilityID = patientFacilityID;
	}
	public Code getRace() {
		return race;
	}
	public void setRace(Code race) {
		this.race = race;
	}
	public boolean isDistinctFacilityRequired() {
		return isDistinctFacilityRequired;
	}
	public void setDistinctFacilityRequired(boolean isDistinctFacilityRequired) {
		this.isDistinctFacilityRequired = isDistinctFacilityRequired;
	}
	
}
