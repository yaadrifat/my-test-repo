/**
 * Created On Sep 8, 2011
 */
package com.velos.services.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kanwaldeep
 *
 */
public class MultipleChoices extends ServiceObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6391896544568739344L;
	private List<MultipleChoice> choice = new ArrayList<MultipleChoice>();
	
	private boolean autoSequence; 
	private boolean hideResponses; 
	private boolean topAlignResponses; 
	private Integer columnCount; 

	/**
	 * @param choice the choice to set
	 */
	public void setChoice(List<MultipleChoice> choice) {
		this.choice = choice;
	}
	
	public void addChoice(MultipleChoice choice)
	{
		this.choice.add(choice); 
	}

	/**
	 * @return the choice
	 */
	public List<MultipleChoice> getChoice() {
		return choice;
	}

	public boolean isAutoSequence() {
		return autoSequence;
	}

	public void setAutoSequence(boolean autoSequence) {
		this.autoSequence = autoSequence;
	}

	public boolean isHideResponses() {
		return hideResponses;
	}

	public void setHideResponses(boolean hideResponses) {
		this.hideResponses = hideResponses;
	}

	public boolean isTopAlignResponses() {
		return topAlignResponses;
	}

	public void setTopAlignResponses(boolean topAlignResponses) {
		this.topAlignResponses = topAlignResponses;
	}

	/**
	 * @param columnCount the columnCount to set
	 */
	public void setColumnCount(Integer columnCount) {
		this.columnCount = columnCount;
	}

	/**
	 * @return the columnCount
	 */
	public Integer getColumnCount() {
		return columnCount;
	} 

}
