package com.velos.services.model;
/**
 * @author Raman
 */
public class CreateMultiPatientStudyStatus extends ServiceObject
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6619065994132618114L;
	protected PatientIdentifier patientIdentifier;
	protected StudyIdentifier studyIdentifier;
	protected PatientEnrollmentDetails patientEnrollmentDetails;
	public PatientEnrollmentDetails getPatientEnrollmentDetails() {
		return patientEnrollmentDetails;
	}
	public void setPatientEnrollmentDetails(
			PatientEnrollmentDetails patientEnrollmentDetails) {
		this.patientEnrollmentDetails = patientEnrollmentDetails;
	}
	/**
	 * @return the patientIdentifier
	 */
	public PatientIdentifier getPatientIdentifier() {
		return patientIdentifier;
	}
	/**
	 * @param patientIdentifier the patientIdentifier to set
	 */
	public void setPatientIdentifier(PatientIdentifier patientIdentifier) {
		this.patientIdentifier = patientIdentifier;
	}
	/**
	 * @return the studyIdentifier
	 */
	public StudyIdentifier getStudyIdentifier() {
		return studyIdentifier;
	}
	/**
	 * @param studyIdentifier the studyIdentifier to set
	 */
	public void setStudyIdentifier(StudyIdentifier studyIdentifier) {
		this.studyIdentifier = studyIdentifier;
	}
	
	
}
