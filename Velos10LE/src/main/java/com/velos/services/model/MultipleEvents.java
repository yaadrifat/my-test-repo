package com.velos.services.model;

import java.util.List;

/**
 * 
 * @author Tarandeep Singh Bali
 *
 */

public class MultipleEvents extends ServiceObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5562387814040454982L;
	
	private List<MultipleEvent> event;

	public List<MultipleEvent> getEvent() {
		return event;
	}

	public void setEvent(List<MultipleEvent> event) {
		this.event = event;
	}
	
	

}
