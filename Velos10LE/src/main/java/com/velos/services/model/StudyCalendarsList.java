package com.velos.services.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="StudyCalendarsList")
public class StudyCalendarsList extends ServiceObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1844769611777533543L;
	protected List<StudyCalendars> studyCalendars = new ArrayList<StudyCalendars>();
	public List<StudyCalendars> getStudyCalendars() {
		return studyCalendars;
	}
	public void setStudyCalendars(List<StudyCalendars> studyCalendars) {
		this.studyCalendars = studyCalendars;
	}
	public void addStudyCalendars(StudyCalendars studyCalendars)
    {
    	this.studyCalendars.add(studyCalendars);
    }
	
}
