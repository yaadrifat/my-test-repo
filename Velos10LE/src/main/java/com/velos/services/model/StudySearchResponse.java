
package com.velos.services.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="StudySearchResponse")

/**
 * Model class for fields in the Study Search Response
 */
public class StudySearchResponse
    extends ServiceObject 
    implements Serializable
{
	/**
     * 
     */
    private static final long serialVersionUID = -356950809796989200L;

    public StudySearchResponse(){
        
    }

    /** Theraputic Area **/
    protected Code therapeuticArea;
    
    protected UserIdentifier principalInvestigator;

    protected String studyNumber;

    protected String studyTitle;

    protected Code division;
    
    protected Code phase;
    
    protected StudyOrganization studyOrganization;
    
    protected String moreStudyDetails;
    
    protected StudyStatus currentStudyStatus;
    
    protected StudyStatus displayedStudyStatus;
    
    //Pagination Fields
    protected Integer pageSize;
    
    protected Integer pageNumber;
	
    protected Integer totalCount;

	/**
	 * @return the therapeuticArea
	 */
	public Code getTherapeuticArea() {
		return therapeuticArea;
	}

	/**
	 * @param therapeuticArea the therapeuticArea to set
	 */
	public void setTherapeuticArea(Code therapeuticArea) {
		this.therapeuticArea = therapeuticArea;
	}

	/**
	 * @return the principalInvestigator
	 */
	public UserIdentifier getPrincipalInvestigator() {
		return principalInvestigator;
	}

	/**
	 * @param principalInvestigator the principalInvestigator to set
	 */
	public void setPrincipalInvestigator(UserIdentifier principalInvestigator) {
		this.principalInvestigator = principalInvestigator;
	}

	/**
	 * @return the studyNumber
	 */
	public String getStudyNumber() {
		return studyNumber;
	}

	/**
	 * @param studyNumber the studyNumber to set
	 */
	public void setStudyNumber(String studyNumber) {
		this.studyNumber = studyNumber;
	}

	/**
	 * @return the studyTitle
	 */
	public String getStudyTitle() {
		return studyTitle;
	}

	/**
	 * @param studyTitle the studyTitle to set
	 */
	public void setStudyTitle(String studyTitle) {
		this.studyTitle = studyTitle;
	}

	/**
	 * @return the division
	 */
	public Code getDivision() {
		return division;
	}

	/**
	 * @param division the division to set
	 */
	public void setDivision(Code division) {
		this.division = division;
	}

	/**
	 * @return the phase
	 */
	public Code getPhase() {
		return phase;
	}

	/**
	 * @param phase the phase to set
	 */
	public void setPhase(Code phase) {
		this.phase = phase;
	}

	/**
	 * @return the studyOrganization
	 */
	public StudyOrganization getStudyOrganization() {
		return studyOrganization;
	}

	/**
	 * @param studyOrganization the studyOrganization to set
	 */
	public void setStudyOrganization(StudyOrganization studyOrganization) {
		this.studyOrganization = studyOrganization;
	}

	/**
	 * @return the moreStudyDetails
	 */
	public String getMoreStudyDetails() {
		return moreStudyDetails;
	}

	/**
	 * @param moreStudyDetails the moreStudyDetails to set
	 */
	public void setMoreStudyDetails(String moreStudyDetails) {
		this.moreStudyDetails = moreStudyDetails;
	}

	/**
	 * @return the currentStudyStatus
	 */
	public StudyStatus getCurrentStudyStatus() {
		return currentStudyStatus;
	}

	/**
	 * @param currentStudyStatus the currentStudyStatus to set
	 */
	public void setCurrentStudyStatus(StudyStatus currentStudyStatus) {
		this.currentStudyStatus = currentStudyStatus;
	}

	/**
	 * @return the displayedStudyStatus
	 */
	public StudyStatus getDisplayedStudyStatus() {
		return displayedStudyStatus;
	}

	/**
	 * @param displayedStudyStatus the displayedStudyStatus to set
	 */
	public void setDisplayedStudyStatus(StudyStatus displayedStudyStatus) {
		this.displayedStudyStatus = displayedStudyStatus;
	}

	/**
	 * @return the pageSize
	 */
	public Integer getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * @return the pageNumber
	 */
	public Integer getPageNumber() {
		return pageNumber;
	}

	/**
	 * @param pageNumber the pageNumber to set
	 */
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	/**
	 * @return the totalCount
	 */
	public Integer getTotalCount() {
		return totalCount;
	}

	/**
	 * @param totalCount the totalCount to set
	 */
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
    
    
}
