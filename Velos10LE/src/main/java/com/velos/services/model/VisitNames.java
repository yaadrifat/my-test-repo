/**
 * Created On Jul 5, 2011
 */
package com.velos.services.model;

import java.util.List;

/**
 * @author Kanwaldeep
 *
 */
public class VisitNames extends ServiceObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8828866364960059725L;
	private List<String> visitName;

	public List<String> getVisitName() {
		return visitName;
	}

	public void setVisitName(List<String> visitName) {
		this.visitName = visitName;
	} 
	
	public void addVisitName(String visitName)
	{
		this.visitName.add(visitName); 
	}

}
