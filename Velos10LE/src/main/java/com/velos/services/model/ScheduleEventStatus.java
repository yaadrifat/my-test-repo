package com.velos.services.model;

import java.util.Date;

/**
 * 
 * @author Tarandeep Singh Bali
 *
 */

public class ScheduleEventStatus extends ServiceObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2169224201335787588L;
	
	protected Date statusValidFrom;
	protected OrganizationIdentifier siteOfService;
	protected Code coverageType;
	protected String reasonForChangeCoverType; 
	protected String notes;
	protected String reasonForChangeFDAAudit;
	public Date getStatusValidFrom() {
		return statusValidFrom;
	}
	public void setStatusValidFrom(Date statusValidFrom) {
		this.statusValidFrom = statusValidFrom;
	}
	public OrganizationIdentifier getSiteOfService() {
		return siteOfService;
	}
	public void setSiteOfService(OrganizationIdentifier siteOfService) {
		this.siteOfService = siteOfService;
	}
	public Code getCoverageType() {
		return coverageType;
	}
	public void setCoverageType(Code coverageType) {
		this.coverageType = coverageType;
	}
	public String getReasonForChangeCoverType() {
		return reasonForChangeCoverType;
	}
	public void setReasonForChangeCoverType(String reasonForChangeCoverType) {
		this.reasonForChangeCoverType = reasonForChangeCoverType;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getReasonForChangeFDAAudit() {
		return reasonForChangeFDAAudit;
	}
	public void setReasonForChangeFDAAudit(String reasonForChangeFDAAudit) {
		this.reasonForChangeFDAAudit = reasonForChangeFDAAudit;
	}	

}
