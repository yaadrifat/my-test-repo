package com.velos.services.model;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * model class for StudyPatientStatus, get patient status on a study 
 * @author Manimaran
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="StudyPatientStatus")
public class StudyPatientStatus extends ServiceObject implements Serializable{

	private static final long serialVersionUID = 1L;
	
	protected PatientStudyStatusIdentifier studyPatStatId;
	protected Code studyPatStatus;
	protected Date statusDate;
	protected String statusNote;
	protected Code statusReason;
	protected boolean isCurrentStatus;
	protected String patStudyId;
	//protected OrganizationIdentifier organizationId; 
	//protected UserIdentifier enrolledBy;
	protected String CUD;
	

	public String getCUD() {
		return CUD;
	}
	public void setCUD(String cUD) {
		CUD = cUD;
	}
	public PatientStudyStatusIdentifier getStudyPatStatId() {
		return studyPatStatId;
	}
	public void setStudyPatStatId(PatientStudyStatusIdentifier studyPatStatId) {
		this.studyPatStatId = studyPatStatId;
	}
	
	
	public Code getStudyPatStatus() {
		return studyPatStatus;
	}
	public void setStudyPatStatus(Code studyPatStatus) {
		this.studyPatStatus = studyPatStatus;
	}
	
	public Date getStatusDate() {
		return statusDate;
	}
	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}
	
	public String getStatusNote() {
		return statusNote;
	}
	public void setStatusNote(String statusNote) {
		this.statusNote = statusNote;
	}
	public Code getStatusReason() {
		return statusReason;
	}
	public void setStatusReason(Code statusReason) {
		this.statusReason = statusReason;
	}
	
	public boolean isCurrentStatus() {
		return isCurrentStatus;
	}
	public void setCurrentStatus(boolean isCurrentStatus) {
		this.isCurrentStatus = isCurrentStatus;
	}
	
	public String getPatStudyId() {
		return patStudyId;
	}
	public void setPatStudyId(String patStudyId) {
		this.patStudyId = patStudyId;
	}
	
	/*
	@NotNull
	public OrganizationIdentifier getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(OrganizationIdentifier organizationId) {
		this.organizationId = organizationId;
	}
   
    @NotNull
    public UserIdentifier getEnrolledBy() {
		return enrolledBy;
	}
	public void setEnrolledBy(UserIdentifier enrolledBy) {
		this.enrolledBy = enrolledBy;
	}
	*/
}