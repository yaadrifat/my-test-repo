package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Groups implements Serializable 
{
	
	private static final long serialVersionUID = -3685161708201790145L;
	
	protected List<GroupIdentifier> group = new ArrayList<GroupIdentifier>();

	public Groups() {
	}

	public void add(GroupIdentifier group) {
		this.group.add(group);
	}

	public void addAll(List<GroupIdentifier> group) {
		this.group = group;
	}

	public List<GroupIdentifier> getGroup() {
		return this.group;
	}

	public void setGroup(List<GroupIdentifier> group) {
		this.group = group;
	}
}
