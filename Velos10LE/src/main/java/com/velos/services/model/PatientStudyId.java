/**
 * 
 */
package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Model class to hold the Patient Study ID as string. It is not a complex data structure.
 * So, it is called ID, not Identifier.
 * 
 * @author ihuang
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="PatientStudyId")
public class PatientStudyId 
	extends ServiceObject
	implements Serializable{
	
	private static final long serialVersionUID = 2940536899672220664L;
	
	protected String patientStudyId;
	
	public String getPatientStudyId() {
		return patientStudyId;
	}

	public void setPatientStudyId(String patientStudyId) {
		this.patientStudyId = patientStudyId;
	}
	
}