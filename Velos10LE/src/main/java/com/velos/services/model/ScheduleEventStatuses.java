package com.velos.services.model;

import java.util.List;

/**
 * 
 * @author Tarandeep Singh Bali
 *
 */

public class ScheduleEventStatuses extends ServiceObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8352400169241522168L;
	
	private List<ScheduleEventStatusIdentifier> scheduleEventStatInfo;
	
	public List<ScheduleEventStatusIdentifier> getScheduleEventStatInfo() {
		return scheduleEventStatInfo;
	}
	
	public void setScheduleEventStatInfo(
			List<ScheduleEventStatusIdentifier> scheduleEventStatInfo) {
		this.scheduleEventStatInfo = scheduleEventStatInfo;
	}

	public void addScheduleEventStatusIdentifier(ScheduleEventStatusIdentifier
			                                     scheduleEventStatInfo){
		
		this.scheduleEventStatInfo.add(scheduleEventStatInfo);
		
	}

}
