package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="MonitorVersion")

public class MonitorVersion implements Serializable{
	
	/**
	 * Object for Monitor Version, having properties for Monitor Version
	 * Namely eSP Build Number, eSP Version number, eReseaech compatibility version
	 * and eSP build date.
	 */
	private static final long serialVersionUID = 1L;
	protected String eSPBuildNumber;
	protected String eSPVersionNumber;
	protected String eResearchCompatibilityVersionNumber;
	protected String eSPBuildDate;
	protected String dateFormat;
	

	public String geteSPBuildNumber() {
		return eSPBuildNumber;
	}

	public void seteSPBuildNumber(String eSPBuildNumber) {
		this.eSPBuildNumber = eSPBuildNumber;
	}

	public String geteSPVersionNumber() {
		return eSPVersionNumber;
	}

	public void seteSPVersionNumber(String eSPVersionNumber) {
		this.eSPVersionNumber = eSPVersionNumber;
	}

	public String geteResearchCompatibilityVersionNumber() {
		return eResearchCompatibilityVersionNumber;
	}

	public void seteResearchCompatibilityVersionNumber(
			String eResearchCompatibilityVersionNumber) {
		this.eResearchCompatibilityVersionNumber = eResearchCompatibilityVersionNumber;
	}

	public String geteSPBuildDate() {
		return eSPBuildDate;
	}

	public void seteSPBuildDate(String eSPBuildDate) {
		this.eSPBuildDate = eSPBuildDate;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}
	
}