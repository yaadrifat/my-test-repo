/**
 * Created On Jan 24, 2012
 */
package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Kanwaldeep
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="StudyPatientFormResponseIdentifier")
public class StudyPatientFormResponseIdentifier extends SimpleIdentifier{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5215366524643098644L;

}
