
package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Data Transfer object carrying Study information. This class contains
 * the fields supported in verison 2.0 by the service operations for a Study.
 * @author dylan
 *
 */
@XmlRootElement(name="Study")
@XmlAccessorType(XmlAccessType.FIELD)
public class Study
    extends ServiceObject 
    implements Serializable
{
	


	public Study(){
		 
	}

    /**
	 * 
	 */
	private static final long serialVersionUID = -3005709574018763377L;



	protected StudyStatuses studyStatuses;

    protected StudyTeamMembers studyTeamMembers;
    
    @XmlElement(name="StudyOrganization")
    protected StudyOrganizations studyOrganizations;

    protected StudySummary studySummary;
    
    protected StudyAttachments studyAttachments;
    
    //added to get OID in services response
    protected StudyIdentifier studyIdentifier; 
  

    /**
     * Returns a list of all {@link StudyStatus} instances for this Study.
     * @return
     */
    //KM-#5693
	//@Size(min=1)
    @NotNull
	@Valid
    public StudyStatuses getStudyStatuses() {
//        if (studyStatuses == null) {
//            studyStatuses = new ArrayList<StudyStatus>();
//        }
        return this.studyStatuses;
    }

    /**
     * Returns a list of all {@link StudyTeamMember} instances for this Study.
     * @return
     */
   //@Size(min=1)
	@NotNull
	@Valid
    public StudyTeamMembers getStudyTeamMembers() {
//        if (studyTeamMembers == null) {
//            studyTeamMembers = new ArrayList<StudyTeamMember>();
//        }
        return this.studyTeamMembers;
    }

	/**
	 * Sets a list of {@link StudyStatus} objects for this Study.
	 * @param studyStatuses
	 */
    public void setStudyStatuses(StudyStatuses studyStatuses) {
		this.studyStatuses = studyStatuses;
	}

    /**
	 * Sets a list of {@link StudyTeamMember} objects for this Study.
	 * @param studyStatuses
	 */
	public void setStudyTeamMembers(StudyTeamMembers studyTeamMembers) {
		this.studyTeamMembers = studyTeamMembers;
	}

    /**
	 * Sets a list of {@link StudyOrganization} objects for this Study.
	 * @param studyStatuses
	 */
	public void setStudyOrganizations(
			StudyOrganizations studyOrganizations) {
		this.studyOrganizations = studyOrganizations;
	}

    /**
     * Returns a list of all {@link StudyTeamMember} instances for this Study.
     * @return
     */
//	@Size(min=1)
	@NotNull
	@Valid
    public StudyOrganizations getStudyOrganizations() {
//        if (studyOrganizations == null) {
//            studyOrganizations = new ArrayList<StudyOrganization>();
//        }
        return this.studyOrganizations;
    }
	
    /**
     * Returns the {@link StudySummary} instance for this Study.
     * @return
     */
	@NotNull
	@Valid
	public StudySummary getStudySummary() {
		return studySummary;
	}

	/**
	 * Sets the {@link StudySummary} for this Study.
	 * @param studySummary
	 */
	public void setStudySummary(StudySummary studySummary) {
		this.studySummary = studySummary;
	}

    public StudyAttachments getStudyAttachments() {
		return studyAttachments;
	}
    
	public void setStudyAttachments(StudyAttachments studyAttachments) {
		this.studyAttachments = studyAttachments;
	}

	/**
	 * Returns the {@link StudyIdentifier} instance for this Study
	 * @return
	 */
	public StudyIdentifier getStudyIdentifier() {
		return studyIdentifier;
	}

	/**Sets the {@link StudyIdentifier} for this Study
	 * @param studyIdentifier
	 */
	public void setStudyIdentifier(StudyIdentifier studyIdentifier) {
		this.studyIdentifier = studyIdentifier;
	}

}
