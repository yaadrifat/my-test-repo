package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="EventSearch")
public class EventSearch extends ServiceObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6559341658943363264L;
	protected String eventNameDescNotes;
	//protected String description;
	protected Code libraryType;
	protected Code costType;
	protected String cptCode;
	protected OrganizationIdentifier facility;
	protected String eventCategory;
	
	public EventSearch(){
		
	}
	

	public String getEventNameDescNotes() {
		return eventNameDescNotes;
	}

	public void setEventNameDescNotes(String eventNameDescNotes) {
		this.eventNameDescNotes = eventNameDescNotes;
	}

	public Code getLibraryType() {
		return libraryType;
	}
	public void setLibraryType(Code libraryType) {
		this.libraryType = libraryType;
	}



	public Code getCostType() {
		return costType;
	}
	public void setCostType(Code costType) {
		this.costType = costType;
	}
	

	public String getCptCode() {
		return cptCode;
	}


	public void setCptCode(String cptCode) {
		this.cptCode = cptCode;
	}


	public OrganizationIdentifier getFacility() {
		return facility;
	}
	public void setFacility(OrganizationIdentifier facility) {
		this.facility = facility;
	}
	public String getEventCategory() {
		return eventCategory;
	}
	public void setEventCategory(String eventCategory) {
		this.eventCategory = eventCategory;
	}
	
	

}
