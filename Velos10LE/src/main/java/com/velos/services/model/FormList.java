/**
 * Created On Dec 2, 2011
 */
package com.velos.services.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kanwaldeep
 *
 */
public class FormList extends ServiceObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2740539440306021105L;
	protected List<FormInfo> formInfo=new ArrayList<FormInfo>();
	protected int formCount;

	public List<FormInfo> getFormInfo() {
		return formInfo;
	}

	public void setFormInfo(List<FormInfo> formInfo) {
		this.formInfo = formInfo;
	}
	
	public void addFormInfo(FormInfo formInfo)
	{
		this.formInfo.add(formInfo);
	}

	/**
	 * @return the formCount
	 */
	public int getFormCount() {
		return formCount;
	}

	/**
	 * @param formCount the formCount to set
	 */
	public void setFormCount(int formCount) {
		this.formCount = formCount;
	}
	
}
