package com.velos.services.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Events")
public class Events extends ServiceObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1826817049164956736L;
	protected List<Event> event = new ArrayList<Event>();

	public List<Event> getEvent() {
		return event;
	}

	public void setEvent(List<Event> event) {
		this.event = event;
	}
	
	public void addEvent(Event event){
		
		this.event.add(event);
	}
	

}
