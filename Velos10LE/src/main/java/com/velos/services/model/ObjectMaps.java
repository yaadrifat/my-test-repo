/**
 * Created On Mar 15, 2013
 */
package com.velos.services.model;

import java.util.List;
import java.util.ArrayList;

import com.velos.services.map.ObjectMap;

/**
 * @author Kanwaldeep
 *
 */
public class ObjectMaps extends ServiceObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5703683000777288582L;
	private List<ObjectMap> objectMap = new ArrayList<ObjectMap>();
	public List<ObjectMap> getObjectMap() {
		return objectMap;
	}

	public void setObjectMap(List<ObjectMap> objectMap) {
		this.objectMap = objectMap;
	} 
	
	public void addObjectMap(ObjectMap objectMap)
	{
		this.objectMap.add(objectMap);
	}
	

}
