/**
 * Created On Sep 7, 2011
 */
package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Kanwaldeep
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Form")
public class StudyFormDesign extends LinkedFormDesign{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8622864709898802775L;
	private StudyIdentifier studyIdentifier;

	public StudyIdentifier getStudyIdentifier() {
		return studyIdentifier;
	}

	public void setStudyIdentifier(StudyIdentifier studyIdentifier) {
		this.studyIdentifier = studyIdentifier;
	} 
}
