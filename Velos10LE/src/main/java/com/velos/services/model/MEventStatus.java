/**
 * Created On Dec 13, 2012
 */
package com.velos.services.model;

/**
 * @author Kanwaldeep
 *
 */
public class MEventStatus extends ServiceObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9185538186621852004L;
	protected ScheduleEventIdentifier eventIdentifier; 
	protected EventStatus eventStatus;
	public ScheduleEventIdentifier getEventIdentifier() {
		return eventIdentifier;
	}
	public void setEventIdentifier(ScheduleEventIdentifier eventIdentifier) {
		this.eventIdentifier = eventIdentifier;
	}
	public EventStatus getEventStatus() {
		return eventStatus;
	}
	public void setEventStatus(EventStatus eventStatus) {
		this.eventStatus = eventStatus;
	} 
}
