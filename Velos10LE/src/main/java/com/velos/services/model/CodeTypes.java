/**
 * Created On Nov 6, 2012
 */
package com.velos.services.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Kanwaldeep
 *
 */
@XmlRootElement(name="CodeTypes")
public class CodeTypes extends ServiceObject{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4281717059856868967L;
	private List<CodeType> type = new ArrayList<CodeType>();

	/**
	 * @param type the type to set
	 */
	public void setType(List<CodeType> type) {
		this.type = type;
	}

	public List<CodeType> getType() {
		return type;
	} 
	
	public void add(CodeType type)
	{
		this.type.add(type); 
	}

}
