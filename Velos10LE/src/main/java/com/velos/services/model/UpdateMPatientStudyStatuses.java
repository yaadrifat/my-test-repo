package com.velos.services.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="UpdateMPatientStudyStatuses")
public class UpdateMPatientStudyStatuses extends ServiceObject
{

	/**
	 * @author Navneet
	 */
	private static final long serialVersionUID = -6502144930816638190L;

	private List<UpdateMStudyPatientStatus> updateMPatientStudyStatus = new ArrayList<UpdateMStudyPatientStatus>();

	public List<UpdateMStudyPatientStatus> getUpdateMPatientStudyStatus() {
		return updateMPatientStudyStatus;
	}

	public void setUpdateMPatientStudyStatus(
			List<UpdateMStudyPatientStatus> updateMPatientStudyStatus) {
		this.updateMPatientStudyStatus = updateMPatientStudyStatus;
	}
	
	public void addUpdateMPatientStudyStatus(UpdateMStudyPatientStatus updateMPatientStudyStatus)
	{
		this.updateMPatientStudyStatus.add(updateMPatientStudyStatus);
	}
	
}
