/**
 * 
 */
package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author dylan
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="UserIdentifier")
public class UserIdentifier 
	extends SimpleIdentifier
	implements Serializable{
	


	/**
	 * 
	 */
	private static final long serialVersionUID = 6460572508742238241L;
	private String userLoginName = null;
	private String firstName = null;
	private String lastName = null;
	private OrganizationIdentifier organizationIdentifier = null;
	private GroupIdentifier groupIdentifier = null;
	
	public UserIdentifier(){
		
	}

	
	public UserIdentifier(
			String userLoginName) {
		
		super();
		this.userLoginName = userLoginName;

	}
	
	public UserIdentifier(
			String firstName, 
			String lastName) {
		
		super();
		this.firstName = firstName;
		this.lastName = lastName;
	}


	public UserIdentifier(String userLoginName, String firstName,
			String lastName) {
		super();
		this.userLoginName = userLoginName;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	public UserIdentifier(String OID, String userLoginName, String firstName, String lastName)
	{
		super(); 
		this.OID = OID; 
		this.userLoginName = userLoginName; 
		this.firstName = firstName; 
		this.lastName = lastName; 
	}

	public String getUserLoginName() {
		return userLoginName;
	}

	public void setUserLoginName(String userId) {
		this.userLoginName = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Override
	public String toString(){
		return userLoginName  + " " + firstName + " " + lastName;
	}


	public OrganizationIdentifier getOrganizationIdentifier() {
		return organizationIdentifier;
	}


	public void setOrganizationIdentifier(
			OrganizationIdentifier organizationIdentifier) {
		this.organizationIdentifier = organizationIdentifier;
	}


	public GroupIdentifier getGroupIdentifier() {
		return groupIdentifier;
	}


	public void setGroupIdentifier(GroupIdentifier groupIdentifier) {
		this.groupIdentifier = groupIdentifier;
	}
	
	
}
