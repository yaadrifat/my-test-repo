/**
 * Created On Mar 15, 2013
 */
package com.velos.services.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kanwaldeep
 *
 */
public class ObjectInfos extends ServiceObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4806976766919125932L;
	private List<ObjectInfo> tableinfo = new ArrayList<ObjectInfo>();
	public List<ObjectInfo> getTableinfo() {
		return tableinfo;
	}
	public void setTableinfo(List<ObjectInfo> tableinfo) {
		this.tableinfo = tableinfo;
	}
	
	

}
