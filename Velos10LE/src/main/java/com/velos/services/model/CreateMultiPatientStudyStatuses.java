package com.velos.services.model;
/**
 * @author Raman
 */
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="CreateMultiPatientStudyStatuses")
public class CreateMultiPatientStudyStatuses extends ServiceObject
{
	private static final long serialVersionUID = -6502144930816638190L;

	private List<CreateMultiPatientStudyStatus> createMPatientStudyStatusList = new ArrayList<CreateMultiPatientStudyStatus>();

	
	/**
	 * @return the createMPatientStudyStatusList
	 */
	public List<CreateMultiPatientStudyStatus> getCreateMPatientStudyStatusList() {
		return createMPatientStudyStatusList;
	}


	/**
	 * @param createMPatientStudyStatusList the createMPatientStudyStatusList to set
	 */
	public void setCreateMPatientStudyStatusList(
			List<CreateMultiPatientStudyStatus> createMPatientStudyStatusList) {
		this.createMPatientStudyStatusList = createMPatientStudyStatusList;
	}


	public void addCreateMPatientStudyStatus(CreateMultiPatientStudyStatus createMPatientStudyStatus)
	{
		this.createMPatientStudyStatusList.add(createMPatientStudyStatus);
	}
	
}
