package com.velos.services.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Data transfer object representing a study IND/IDE record.
 * @author Isaac
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="StudyIndIde")
public class StudyIndIde
    extends ServiceObject
{

	private static final long serialVersionUID = 3794016758238040467L;

	protected String indIdeType;
	
    protected String indIdeNumber;
    
	protected Code grantor;

    protected Code holderType;

    protected Code programCode;

    protected boolean isExpandedAccess;
    
    protected Code expandedAccessType;
    
    protected boolean isExempt;

    
    /**
     * Gets the value of the IND/IDE Type property.
     * 
     * @return either the value "IND" or "IDE"
     *     possible object is
     *     {@link String }
     * 
     */
	@NotNull
	public String getIndIdeType() {
		return indIdeType;
	}

	public void setIndIdeType(String indIdeType) {
		this.indIdeType = indIdeType;
	}
	
    /**
     * Gets the value of the IND/IDE Number property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
	@NotNull
    public String getIndIdeNumber() {
        return indIdeNumber;
    }

    /**
     * Sets the value of the IND/IDE Number property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndIdeNumber(String indIdeNumber) {
        this.indIdeNumber = indIdeNumber;
    }

    /**
     * Gets the value of the grantor property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    @NotNull
    @Valid
    public Code getGrantor() {
        return grantor;
    }

    /**
     * Sets the value of the grantor property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setGrantor(Code grantor) {
        this.grantor = grantor;
    }

    /**
     * Gets the value of the holder type property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    @NotNull
    @Valid
    public Code getHolderType() {
        return holderType;
    }

    /**
     * Sets the value of the holder type property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setHolderType(Code holderType) {
        this.holderType = holderType;
    }
    
    /**
     * Gets the value of the program code property: NIH Institution, NCI Division, or Program Code.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    @Valid
    public Code getProgramCode() {
        return programCode;
    }

    /**
     * Sets the value of the program code property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setProgramCode(Code programCode) {
        this.programCode = programCode;
    }
    
    /**
     * Returns whether this IND/IDE record allows for expanded access.
     * @return
     */
	public boolean isExpandedAccess() {
		return isExpandedAccess;
	}
    
    /**
     * Sets whether this IND/IDE record allows for expanded access.
     * @param isExpandedAccess
     */
	public void setExpandedAccess(boolean isExpandedAccess) {
		this.isExpandedAccess = isExpandedAccess;
	}
	
    /**
     * Gets the value of the expanded access type property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    @Valid
    public Code getExpandedAccessType() {
        return expandedAccessType;
    }

    /**
     * Sets the value of the expanded access type property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setExpandedAccessType(Code expandedAccessType) {
        this.expandedAccessType = expandedAccessType;
    }
    
    /**
     * Returns whether this IND/IDE record is exempt.
     * @return
     */
	public boolean isExempt() {
		return isExempt;
	}
    
    /**
     * Sets whether this IND/IDE record is exempt.
     * @param isExempt
     */
	public void setExempt(boolean isExempt) {
		this.isExempt = isExempt;
	}
	
	/**
	 * Parent Identifier of a Study IND/IDE record is the studyIdentifier
	 * @return studyIdentifier of the enclosing study
	 */
    public StudyIdentifier getParentIdentifier() {
        if (super.parentIdentifier == null || 
                super.parentIdentifier.getId().size() < 1) { return null; }
        for (SimpleIdentifier myId : super.parentIdentifier.getId()) {
            if (myId instanceof StudyIdentifier) {
                return (StudyIdentifier)myId;
            }
        }
        return null;
    }
    
    public void setParentIdentifier(StudyIdentifier studyIdentifer) {
        List<SimpleIdentifier> idList = new ArrayList<SimpleIdentifier>();
        idList.add(studyIdentifer);
        super.parentIdentifier = new ParentIdentifier();
        super.parentIdentifier.setId(idList);
    }
    
}
