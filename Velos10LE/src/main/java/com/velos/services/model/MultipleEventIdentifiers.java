package com.velos.services.model;

import java.util.List;

/**
 * 
 * @author Tarandeep Singh Bali
 *
 */

public class MultipleEventIdentifiers extends ServiceObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5562387814040454982L;
	
	private List<MultipleEventIdentifier> eventIdentifier;

	public List<MultipleEventIdentifier> getEventIdentifier() {
		return eventIdentifier;
	}

	public void setEventIdentifier(List<MultipleEventIdentifier> eventIdentifier) {
		this.eventIdentifier = eventIdentifier;
	}
	
	

}
