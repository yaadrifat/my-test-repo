package com.velos.services.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Isaac
 *
 */
public class StudyIndIdes extends ServiceObject {

	private static final long serialVersionUID = 2364942262852460785L;
	protected List<StudyIndIde> studyIndIde;
	
	@NotNull
	@Size(min=1)
	@Valid
	public List<StudyIndIde> getStudyIndIde() {
		return studyIndIde;
	}
	public void setStudyIndIde(List<StudyIndIde> studyIndIde) {
		this.studyIndIde = studyIndIde;
	} 
	
	/**
	 * Parent Identifier of a Study IND/IDE record is the studyIdentifier
	 * @return studyIdentifier of the enclosing study
	 */
    public StudyIdentifier getParentIdentifier() {
        if (super.parentIdentifier == null || 
                super.parentIdentifier.getId().size() < 1) { return null; }
        for (SimpleIdentifier myId : super.parentIdentifier.getId()) {
            if (myId instanceof StudyIdentifier) {
                return (StudyIdentifier)myId;
            }
        }
        return null;
    }
    
    public void setParentIdentifier(StudyIdentifier studyIdentifer) {
        List<SimpleIdentifier> idList = new ArrayList<SimpleIdentifier>();
        idList.add(studyIdentifer);
        super.parentIdentifier = new ParentIdentifier();
        super.parentIdentifier.setId(idList);
    }
}
