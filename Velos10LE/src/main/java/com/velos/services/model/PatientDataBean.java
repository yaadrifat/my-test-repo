package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
 
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="PatientData")
public class PatientDataBean extends SimpleIdentifier{
	
	/** 
	 * 
	 */
	private static final long serialVersionUID = 4593956806659566042L;

	private String patFirstName= null;
	private String patLastName= null;
	private String patDateofBirth= null;
	private Code patSurvivalStat= null;
	private Code gender= null;
	private PatientIdentifier patientIdentifier = null; 
	//Added By Aman
	private Code race= null;
	private String patientFacilityId= null;
	
	public String getPatFirstName() {
		return patFirstName;
	}
	public void setPatFirstName(String patFirstName) {
		this.patFirstName = patFirstName;
	}
	public String getPatLastName() {
		return patLastName;
	}
	public void setPatLastName(String patLastName) {
		this.patLastName = patLastName;
	}
	public String getPatDateofBirth() {
		return patDateofBirth;
	}
	public void setPatDateofBirth(String patDateofBirth) {
		this.patDateofBirth = patDateofBirth;
	}
	public Code getPatSurvivalStat() {
		return patSurvivalStat;
	}
	public void setPatSurvivalStat(Code patSurvivalStat) {
		this.patSurvivalStat = patSurvivalStat;
	}
	public Code getGender() {
		return gender;
	}
	public void setGender(Code gender) {
		this.gender = gender;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/**
	 * @param patientIdentifier the patientIdentifier to set
	 */
	public void setPatientIdentifier(PatientIdentifier patientIdentifier) {
		this.patientIdentifier = patientIdentifier;
	}
	/**
	 * @return the patientIdentifier
	 */
	public PatientIdentifier getPatientIdentifier() {
		return patientIdentifier;
	}
	
	public Code getRace() {
		return race;
	}
	public void setRace(Code race) {
		this.race = race;
	}
	public String getPatientFacilityId() {
		return patientFacilityId;
	}
	public void setPatientFacilityId(String patientFacilityId) {
		this.patientFacilityId = patientFacilityId;
	}
	
}
