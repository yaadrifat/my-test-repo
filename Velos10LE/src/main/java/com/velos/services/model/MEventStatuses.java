/**
 * Created On Dec 13, 2012
 */
package com.velos.services.model;

import java.util.List;
import java.util.ArrayList; 

/**
 * @author Kanwaldeep
 *
 */
public class MEventStatuses extends ServiceObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -340849994116615579L;
	
	protected List<MEventStatus> eventStatus = new ArrayList<MEventStatus>();

	public List<MEventStatus> getMeventStatus() {
		return eventStatus;
	}

	public void setMeventStatus(List<MEventStatus> meventStatus) {
		this.eventStatus = meventStatus;
	}
	
	

}
