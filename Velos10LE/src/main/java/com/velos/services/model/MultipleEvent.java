package com.velos.services.model;
/**
 * 
 * @author Tarandeep Singh Bali
 *
 */

public class MultipleEvent extends ServiceObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4852741458139679069L;
	
	private EventIdentifier eventIdentifier;
	private EventAttributes eventAttributes;
	public EventIdentifier getEventIdentifier() {
		return eventIdentifier;
	}
	public void setEventIdentifier(EventIdentifier eventIdentifier) {
		this.eventIdentifier = eventIdentifier;
	}
	public EventAttributes getEventAttributes() {
		return eventAttributes;
	}
	public void setEventAttributes(EventAttributes eventAttributes) {
		this.eventAttributes = eventAttributes;
	}

}
