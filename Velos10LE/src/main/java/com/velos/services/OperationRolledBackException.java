/**
 * 
 */
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * @author dylan
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OperationRolledBackException")
public class OperationRolledBackException extends OperationException {

	/**
     * 
     */
    private static final long serialVersionUID = -7904436349667288378L;

    public OperationRolledBackException(Issues issues) {
		super();
		super.issues = issues;
	}

	public OperationRolledBackException(Issues issues, String message) {
		super(message);
        super.issues = issues;
	}

	public Issues getIssues() {
		return issues;
	}

	public void setIssues(Issues issues) {
		super.issues = issues;
	}
	
	
}
