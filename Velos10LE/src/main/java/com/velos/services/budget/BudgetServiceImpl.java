package com.velos.services.budget;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.TeamDao;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.esch.business.budget.impl.BudgetBean;
import com.velos.esch.business.common.BudgetUsersDao;
import com.velos.esch.service.budgetAgent.BudgetAgentRObj;
import com.velos.services.AbstractService;
import com.velos.services.AuthenticationException;
import com.velos.services.AuthorizationException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.ValidationException;
import com.velos.services.authorization.BudgetAuthModule;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.authorization.TeamAuthModule;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.BudgetIdentifier;
import com.velos.services.model.BudgetStatus;
import com.velos.services.model.Code;
import com.velos.services.model.UserIdentifier;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ObjectLocator;

@Stateless
@Remote(BudgetService.class)
public class BudgetServiceImpl extends AbstractService implements BudgetService{
	
	@EJB
	private UserAgentRObj userAgent;
	@EJB
	private ObjectMapService objectMapService;
	@EJB
	private BudgetAgentRObj budgetAgent; 
	@EJB
	GrpRightsAgentRObj groupRightsAgent;
	
	
	@Resource
	private SessionContext sessionContext; 
	
	private static Logger logger = Logger.getLogger(BudgetServiceImpl.class.getName());

	/**
	 * 
	 * @param studyIdentifier
	 * @param budgetIdentifier
	 * @return
	 * @throws OperationException
	 */
	public BudgetStatus getBudgetStatus(BudgetIdentifier budgetIdentifier) throws OperationException{
		BudgetStatus budgetStatus= null;
		try{
			Integer budgetPK = locateBudgetPK(budgetIdentifier);
			
			if(budgetPK == 0 ){
				Issue issue = new Issue(IssueTypes.BUDGET_NOT_FOUND, "For Budget ID : "+budgetIdentifier.getOID());
				addIssue(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
				
			}
			budgetStatus = getBudgetStatus(budgetPK);
		
		} 
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl removeStudyOrganization", e);
			throw new OperationRolledBackException(response.getIssues());
	
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl removeStudyOrganization", t);
			throw new OperationRolledBackException(response.getIssues());
	
		}
		
		return budgetStatus;
	}

	/**
	 * 
	 * @param budgetName
	 * @return
	 * @throws OperationException 
	 */
	private BudgetStatus getBudgetStatus(Integer budgetPK) throws OperationException {
		
		BudgetBean budgetBean = budgetAgent.getBudgetDetails(budgetPK);
		checkBudgetStatusPermissions(budgetBean); 
		BudgetStatus budgetStatus = new BudgetStatus();
		BudgetIdentifier budgetIdentifier = new BudgetIdentifier();
		ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_CALENDAR_BUDGET, budgetPK);
		
		budgetIdentifier.setBudgetName(budgetBean.getBudgetName());
		budgetIdentifier.setBudgetVersion(budgetBean.getBudgetVersion()); 
		budgetIdentifier.setPK(budgetPK);
		budgetIdentifier.setOID(map.getOID());
		
		budgetStatus.setBudgetIdentifier(budgetIdentifier);
		
		CodeCache codeCache = CodeCache.getInstance();
		
		Code budgetStatusCode = 
			codeCache.getSchCodeSubTypeByPK(
					CodeCache.CODE_CALENDAR_BUDGET_STATUS_TYPE, 
					EJBUtil.stringToInteger(budgetBean.getBudgetCodeListStatus()),
					callingUser.getUserId());
		budgetStatus.setBudgetStatus(budgetStatusCode);
		
		UserIdentifier usrBudgetStatusChangedBy = getUserIdFromPKStr(budgetBean.getModifiedBy(), objectMapService, userAgent);
		budgetStatus.setStatusChangedBy(usrBudgetStatusChangedBy);
		budgetStatus.setStatusDate(budgetBean.getLastModifiedDt());
		
		return budgetStatus;
		
	}
	private Integer locateBudgetPK(BudgetIdentifier budgetIdentifier) 
	throws OperationException{
		Integer budgetPK = null; 
		if  (budgetIdentifier == null ||
				((budgetIdentifier.getPK() == null || budgetIdentifier.getPK()<=0) &&(budgetIdentifier.getOID() == null || budgetIdentifier.getOID().length() ==0) && 
						(budgetIdentifier.getBudgetName() == null || budgetIdentifier.getBudgetName().length() == 0) ) ){
			
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid Budget Identifier : OID or Budget Name or Budget PK is required"));
			throw new ValidationException();
		}
		try
		{
			budgetPK =
				ObjectLocator.budgetPKFromIdentifier(callingUser, budgetIdentifier,objectMapService);
		}catch(MultipleObjectsFoundException mobe)
		{
			addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, "Multiple Budgets found"));
			throw new OperationException(); 
		}
		
		
		if (budgetPK == null || budgetPK == 0){
			StringBuffer errorMessage = new StringBuffer("Budget not found for:"); 
			if(budgetIdentifier.getOID() != null && budgetIdentifier.getOID().length() > 0 )
			{
				errorMessage.append(" OID:" + budgetIdentifier.getOID()); 
			}
			else if(budgetIdentifier.getBudgetName() != null && budgetIdentifier.getBudgetName().length() > 0)
			{
				errorMessage.append( " Budget name:" + budgetIdentifier.getBudgetName()); 
				if(budgetIdentifier.getBudgetVersion() != null && budgetIdentifier.getBudgetVersion().length() > 0)
				{
					errorMessage.append(" BudgetVersion:" + budgetIdentifier.getBudgetVersion()); 
				}
			}
			addIssue(new Issue(IssueTypes.BUDGET_NOT_FOUND, errorMessage.toString()));
			throw new OperationException();
		}
		return budgetPK;
	}
	
	
	private void checkGroupProtocolViewPermissions() throws OperationException
	{
		//Get the user's permission for viewing protocols
		GroupAuthModule groupAuth = 
			new GroupAuthModule(callingUser, groupRightsAgent);

		Integer manageProtocolPriv = 
			groupAuth.getAppManageProtocolsPrivileges();

		boolean hasViewManageProt = 
			GroupAuthModule.hasViewPermission(manageProtocolPriv);
		if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
		if (!hasViewManageProt){
			addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User not Authorized to view budgets status and cant access Budgets"));
			throw new AuthorizationException("User not Authorized to view budgets status and cant access Budgets");
		}
	}
	
	private void checkStudyTeamViewPermissions(int studyPK) throws OperationException
	{
		TeamAuthModule teamAuth = 
			new TeamAuthModule(callingUser.getUserId(), studyPK );
		Integer manageStudySetup = teamAuth.getStudySetupPrivileges(); 
		
		boolean hasViewStudyCalPermissions = TeamAuthModule.hasViewPermission(manageStudySetup); 
		if(logger.isDebugEnabled()) logger.debug("user study team - STUDYCAL priv :" + manageStudySetup); 
		if(!hasViewStudyCalPermissions)
		{
			addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User not Authorized to view budgets status and cant access Budgets"));
			throw new AuthorizationException("User not authorized to view budgets status and cant access Budgets");
		}
		
		
	}
	private void checkGroupBudgetViewPermissions() throws OperationException
	{
		//Get the user's permission for viewing budget
		GroupAuthModule groupAuth = 
			new GroupAuthModule(callingUser, groupRightsAgent);

		Integer manageBudgetPriv = 
			groupAuth.getAppBudgetPrivileges();

		boolean hasViewManageBudget = 
			GroupAuthModule.hasViewPermission(manageBudgetPriv);
		if (logger.isDebugEnabled()) logger.debug("user manage budget priv: " + manageBudgetPriv);
		if (!hasViewManageBudget){
			addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User not authorized to view budgets status and cant access Budgets"));
			throw new AuthorizationException("User not Authorized to view Budget Status");
		}
	}
	
	
	private void checkBudgetDetailsPermissions(BudgetBean budgetBean) throws OperationException
	{
		
		if(budgetBean.getBudgetCombinedFlag() != null && budgetBean.getBudgetCombinedFlag().equalsIgnoreCase("Y"))
		{
			Integer studyPK = null; 
			if(budgetBean.getBudgetStudyId() != null){
				studyPK = EJBUtil.stringToInteger(budgetBean.getBudgetStudyId());				
			}
			checkGroupProtocolViewPermissions(); 
			checkStudyTeamViewPermissions(studyPK); 
		}else
		{
			checkGroupBudgetViewPermissions(); 
		}
		
		BudgetAuthModule budgetAuth = new BudgetAuthModule(budgetBean, callingUser);
		Integer budgetPriv = budgetAuth.getBudgetDetailsPriviledges(); 
		if(!BudgetAuthModule.hasViewPermission(budgetPriv)) 
		{
			addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User not authorized to view budgets status and cant access Budgets"));
			throw new AuthorizationException("User not Authorized to view Budget Status");
		}
		
		
	}
	
	
	private void checkBudgetStatusPermissions(BudgetBean budgetBean) throws OperationException
	{

		boolean hasAccess  = false; 
		GroupAuthModule groupAuth = new GroupAuthModule(callingUser, groupRightsAgent); 
		Integer grpBudget =  groupAuth.getAppBudgetPrivileges(); 
		boolean hasViewBudgetGrpRights = GroupAuthModule.hasViewPermission(grpBudget);
		if(hasViewBudgetGrpRights)
		{
			String scope = budgetBean.getBudgetRScope(); 
			if(!StringUtil.isEmpty(scope))
			{
				if(scope.equalsIgnoreCase("A"))
				{
					if(budgetBean.getBudgetAccountId().equalsIgnoreCase(callingUser.getUserAccountId()))
					{
						hasAccess = true; 								
					}
				}else if(scope.equalsIgnoreCase("O"))
				{
					if(budgetBean.getBudgetSiteId().equalsIgnoreCase(callingUser.getUserSiteId()))
					{
						hasAccess = true; 							
					}
				}else if(scope.equalsIgnoreCase("S"))
				{
					// check if user is in studyTeam. 
					String studyID = budgetBean.getBudgetStudyId(); 
					TeamDao teamDao = new TeamDao(); 
					teamDao.findUserInTeam(EJBUtil.stringToNum(budgetBean.getBudgetStudyId()),callingUser.getUserId());
					if (teamDao.getCRows() > 0) {
						hasAccess = true; 							
					}else
					{
						teamDao.getSuperUserTeam(EJBUtil.stringToNum(budgetBean.getBudgetStudyId())); 
						if(teamDao.getUserIds().contains(callingUser.getUserId()))
						{

							hasAccess = true;  									
						}
					}

				}
			}
			
			//individual user access right overrides grpRight and scopeRight - not applicable to combined Budgets
			if(budgetBean.getBudgetCombinedFlag() == null)
			{
				BudgetUsersDao bgtUsersDao = new BudgetUsersDao();	            
	            String userRightStr = bgtUsersDao.getBudgetUserRight(budgetBean.getBudgetId(), callingUser.getUserId());
			    if (!StringUtil.isEmpty(userRightStr)) {
			    	hasAccess = true;  			    
			    }	           
			}		
		}else if(budgetBean.getBudgetCombinedFlag() != null && budgetBean.getBudgetCombinedFlag().equalsIgnoreCase("Y"))
		{
			Integer studyPK = null; 
			if(budgetBean.getBudgetStudyId() != null){
				studyPK = EJBUtil.stringToInteger(budgetBean.getBudgetStudyId());				
			}
			checkGroupProtocolViewPermissions(); 
			checkStudyTeamViewPermissions(studyPK); 
			hasAccess = true; 
			
		}
		
		if(!hasAccess)
		{
			addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User not authorized to view budgets status and cant access Budgets"));
			throw new AuthorizationException("User not Authorized to view Budget Status");
		}
		
		
	}
	
	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		response = new ResponseHolder();
		callingUser = 
			getLoggedInUser(
					sessionContext,
					userAgent);
		return ctx.proceed();

	}
	
}
