/**
 * Created On Nov 6, 2012
 */
package com.velos.services.systemadmin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.CommonDAO;
import com.velos.esch.business.common.SchCodeDao;
import com.velos.services.model.Code;
import com.velos.services.model.CodeType;
import com.velos.services.model.CodeTypes;
import com.velos.services.model.Codes;
import com.velos.services.util.CodeCache;

/**
 * @author Kanwaldeep
 *
 */
public class CodeServiceDAO extends CommonDAO{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2905895089446389972L;
	private static Logger logger = Logger.getLogger(CodeServiceDAO.class); 
	
	public CodeTypes getCodeTypes() throws SQLException
	{
		CodeTypes types = new CodeTypes(); 
		
		String sql = "select CODELST_TYPE_SERVICES,description from ER_CODELSTTYPE";
		ResultSet rs = null; 
		Connection conn = null; 
		PreparedStatement stmt = null;
		
		try{
			
			conn = getConnection(); 
			stmt = conn.prepareStatement(sql); 
			rs = stmt.executeQuery(); 
			
			while(rs.next())
			{
				CodeType type = new CodeType(); 
				type.setCodeType(rs.getString(1));
				type.setDescription(rs.getString(2));
				types.add(type);
			}
		}catch(SQLException sqe)
		{
			logger.error("Failed to retrieve CodeTypes from DB"); 
			throw sqe; 
		}finally
		{
			if(rs != null)
			{
				try{
					rs.close(); 
				}catch(SQLException se)
				{
					logger.error("Unable to close result set", se); 
				}
			}
			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException se)
				{
					logger.error("Unable to close prepared Statement", se); 
				}
				
			}
			if(conn != null)
			{
				returnConnection(conn); 
			}
		}
		
		
		
		return types; 
	}
	
	
	public Codes getCodes(String serviceCodeType, Integer accountID) throws SQLException
	{
		CodeTypes types = new CodeTypes(); 
		
		String sql = "select CODELST_TYPE,schema_name from er_codelsttype where CODELST_TYPE_SERVICES = ?";
		ResultSet rs = null; 
		Connection conn = null; 
		PreparedStatement stmt = null;
		Codes codes = new Codes();
		
		try{
			
			conn = getConnection(); 
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, serviceCodeType);
			rs = stmt.executeQuery(); 
			
			if(rs.next())
			{
				CodeCache codeCache = CodeCache.getInstance();
				
				if(rs.getString("schema_name").equalsIgnoreCase("eres"))
				{
					CodeDao cd = codeCache.getCodes(rs.getString("CODELST_TYPE"), accountID);				 		

					for(int i=0; i < cd.getCId().size(); i++) {
						Code code = new Code();
						code.setType(cd.getCType());
						code.setCode((String)cd.getCSubType().get(i));
						code.setDescription((String)cd.getCDesc().get(i));
						if ("Y".equals((String)cd.getCodeHide().get(i))) {
							code.setHidden(true);
						}
						codes.add(code);
					}
				}else{
					SchCodeDao schcd = codeCache.getSchCodes(rs.getString("CODELST_TYPE"),accountID); 
					for(int j=0; j < schcd.getCId().size(); j++) {
						Code code = new Code();
						code.setType(serviceCodeType);
						code.setCode((String)schcd.getCSubType().get(j));
						code.setDescription((String)schcd.getCDesc().get(j));
						if ("Y".equals((String)schcd.getCodeHide().get(j))) {
							code.setHidden(true);
						}
						codes.add(code); 
					}

				}
				
			}
		}catch(SQLException sqe)
		{
			logger.error("Failed to retrieve CodeTypes from DB"); 
			throw sqe; 
		}finally
		{
			if(rs != null)
			{
				try{
					rs.close(); 
				}catch(SQLException se)
				{
					logger.error("Unable to close result set", se); 
				}
			}
			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException se)
				{
					logger.error("Unable to close prepared Statement", se); 
				}
				
			}
			if(conn != null)
			{
				returnConnection(conn); 
			}
		}
		
		
		
		return codes; 
	}
	
	public boolean isTableNameValid(String tableName) throws SQLException
	{
		PreparedStatement stmt = null; 
		ResultSet rs = null;
		Connection conn = null; 
		boolean istableNameValid = false; 
		try{
			
			String sql = "SELECT table_name FROM all_tables where owner in (?, ?, ?) and table_name = ?"; 
			conn = getConnection(); 
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, "ESCH");
			stmt.setString(2, "EPAT");
			stmt.setString(3, "ERES");
			stmt.setString(4, tableName.toUpperCase());
			rs = stmt.executeQuery();
			
			if(rs.next())
			{
				istableNameValid = true;
			}
			
		}catch(SQLException sqe)
		{
			logger.error("Failed to retrieve CodeTypes from DB"); 
			throw sqe; 
		}finally
		{
			if(rs != null)
			{
				try{
					rs.close(); 
				}catch(SQLException se)
				{
					logger.error("Unable to close result set", se); 
				}
			}
			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException se)
				{
					logger.error("Unable to close prepared Statement", se); 
				}
				
			}
			if(conn != null)
			{
				returnConnection(conn); 
			}
		}
		return istableNameValid;
	}
	

}
