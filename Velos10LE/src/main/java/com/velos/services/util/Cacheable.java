/**
 * 
 */
package com.velos.services.util;

/**
 * @author dylan
 *
 */
public interface Cacheable {

	public void flush();
}
