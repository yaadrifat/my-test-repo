/**
 * Created On Mar 24, 2011
 */
package com.velos.services.mbean;
import org.jboss.system.ServiceMBeanSupport;

import com.velos.services.outbound.MessagingDaemon;



/**
 * @author Kanwaldeep
 *
 */
public class OutBoundService extends ServiceMBeanSupport implements OutBoundServiceMBean{

	  public void init() throws Exception { } 

	  public void startService() throws Exception {
	       MessagingDaemon.startDeamon();
	 
	  } 
	  public void stopService() throws Exception { 
	     MessagingDaemon.stopDeamon();
	  }

}
