/**
 * Created On Mar 24, 2011
 */
package com.velos.services.mbean;

import org.jboss.system.ServiceMBean;

/**
 * @author Kanwaldeep
 *
 */
public interface OutBoundServiceMBean extends ServiceMBean{
	
}
