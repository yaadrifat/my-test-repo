/**
 * Created On May 19, 2011
 */
package com.velos.services.calendar;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;


import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.services.OperationException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.CalendarEvent;
import com.velos.services.model.CalendarEventSummary;
import com.velos.services.model.CalendarEvents;
import com.velos.services.model.CalendarIdentifier;
import com.velos.services.model.CalendarVisit;
import com.velos.services.model.CalendarVisitSummary;
import com.velos.services.model.CalendarVisits;
import com.velos.services.model.Code;
import com.velos.services.model.Cost;
import com.velos.services.model.Costs;
import com.velos.services.model.Duration;
import com.velos.services.model.EventCostIdentifier;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.ParentIdentifier;
import com.velos.services.model.ServiceObject;
import com.velos.services.model.VisitIdentifier;

import com.velos.services.util.CodeCache;
import com.velos.services.util.ServicesUtil;


/**
 * @author Kanwaldeep
 *
 */
public class StudyCalendarDAO extends CommonDAO {	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2859662255612542296L;

	private static Logger logger = Logger.getLogger(StudyCalendarDAO.class); 
	
	public static final String VELSEP = "[VELSEP]";
	public static final String VELSEP_REGX = "\\[VELSEP\\]";
	
	private Integer calendarPK; 
	private Integer studyPK; 
	private boolean addedBeforeOffline; 
	
	public Integer getCalendarPK()
	{
		return calendarPK; 
	}
	
	public Integer getStudyPK()
	{
		return studyPK; 
	}
	
	public boolean isAddedBeforeOffline()
	{
		return addedBeforeOffline; 
	}
	/**
	 * 
	 * @param calendarPK
	 * @param userAccountPK
	 * @param visitPK
	 * @param eventPK
	 * @return
	 * @throws SQLException
	 */
	public CalendarVisits getStudyCalendarVisits(Integer calendarPK, Integer userAccountPK, Integer visitPK, Integer eventPK) throws SQLException
	{
		CalendarVisits visits = new CalendarVisits();
		Connection conn = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try
		{
			CodeCache codeCache = CodeCache.getInstance();		   
		   conn = getConnection(); 
		   stmt = conn.prepareStatement(constructVisitEventSQL(calendarPK, visitPK, eventPK)); 
		   int j = 1; 

		   stmt.setInt(j++, calendarPK);  
		   stmt.setInt(j++, calendarPK); 
		  
		   rs = stmt.executeQuery(); 
		   ObjectMapService  mapService = ServicesUtil.getObjectMapService();	
		   CalendarVisit visit = null ;	
		   CalendarEvent event = null; 
		   int visitSeq = 1; 
		   int eventID = 0; 
		   while(rs.next())
		   { 
			   if(rs.getString("FLAG").equals("V"))
			   {
				   	visit = populateVisit(rs, visitSeq, mapService); 
					visits.setVisit(visit); 
					visitSeq++; 
					
			   }else //event
			   { 
				   if(eventID != rs.getInt("EVENT_ID"))
				   {
					   eventID = rs.getInt("EVENT_ID"); 
					   event = populateEvent(rs, mapService, codeCache, userAccountPK); 
					   visit.getEvents().setEvent(event); 
				   }
				   else
				   {
					   
					   event.getCosts().setCost(populateCost(rs, codeCache, userAccountPK, mapService)); 			   
				   }				   
				
			   }		 
			
		   }		
			
		}finally
		{
			closeResultSet(rs); 
			closeStatement(stmt); 
			returnConnection(conn); 
			
		}
		return visits; 
		
	}
	/**
	 * 
	 * @param userAccountPK
	 * @param visitPK
	 * @param calendarPK
	 * @return
	 * @throws SQLException
	 */
	
	public CalendarVisit getStudyCalendarVisit(Integer userAccountPK, Integer visitPK, Integer calendarPK) throws SQLException
	{
		CalendarVisit visit = null ;		
		Connection conn = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try
		{
		   CodeCache codeCache = CodeCache.getInstance();		   
		   conn = getConnection(); 
		   stmt = conn.prepareStatement(constructVisitEventSQL(calendarPK, visitPK, null)); 
		   
		   stmt.setInt(1, calendarPK); 
		   stmt.setInt(2, visitPK); 		   
		   rs = stmt.executeQuery(); 
		   ObjectMapService  mapService = ServicesUtil.getObjectMapService();
		

		   CalendarEvent event = null; 
		   int visitSeq = 1; 
		   int eventID = 0; 
		   while(rs.next())
		   { 
			   if(rs.getString("FLAG").equals("V"))
			   {
				 
				   if(rs.getInt("FK_VISIT") == visitPK)
				   {
					   ObjectMap calendarMap = mapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY_CALENDAR, rs.getInt("CHAIN_ID")); 
					   CalendarIdentifier calendarIdentifier = new CalendarIdentifier(); 
					   calendarIdentifier.setOID(calendarMap.getOID());
					   calendarIdentifier.setPK(rs.getInt("CHAIN_ID"));
					   ParentIdentifier parentIde = new ParentIdentifier(); 
					   parentIde.addIdentifier(calendarIdentifier); 					 
					   visit = populateVisit(rs, visitSeq, mapService); 
					   visit.getCalendarVisitSummary().setParentIdentifier(parentIde); 
				   }
				   visitSeq++; 
					
			   }else //event
			   { 
				   if(eventID != rs.getInt("EVENT_ID"))
				   {
					   eventID = rs.getInt("EVENT_ID"); 
					   event = populateEvent(rs, mapService, codeCache, userAccountPK); 
					   visit.getEvents().setEvent(event); 
				   }
				   else
				   {					   
					   event.getCosts().setCost(populateCost(rs, codeCache, userAccountPK, mapService)); 			   
				   }				   
				
			   }		 
			
		   }		
			
		}finally
		{
			closeResultSet(rs); 
			closeStatement(stmt); 
			returnConnection(conn); 
			
		}
		return visit; 
		
	}
	/**
	 * 
	 * @param calendarPK
	 * @param visitPK
	 * @param eventPK
	 * @return
	 */
	private String constructVisitEventSQL(Integer calendarPK,Integer visitPK, Integer eventPK)
	{
		StringBuffer preSql = new StringBuffer(); 
		
		if(eventPK == null)
		{
			preSql.append( "SELECT decode(NUM_DAYS,0,0,DISPLACEMENT) as DISPLACEMENT, 0 as EVENT_ID, 0 as ORG_ID, "+
					"fk_protocol as CHAIN_ID,  '' as EVENT_TYPE,  visit_name  as NAME,  '' as NOTES, "+
					"0 as PK_EVENTCOST, 0 as COST,  '' as COST_DESCRIPTION, 0 as DURATION, 'D' as DURATION_UNIT,"+
					"0 as USER_ID,  ''  as LINKED_URI, ''  as FUZZY_PERIOD, "+
					" ''  as MSG_TO,  null as codeStatus, DESCRIPTION  as DESCRIPTION, "+
					"NUM_MONTHS MONTH , "+
					"NUM_WEEKS WEEK , "+
					"NUM_DAYS DAY,  "+
					" DECODE(INSERT_AFTER, 0 , null , (Select b.visit_name from sch_protocol_visit b where b.pk_protocol_visit = s.insert_after)) as INSERT_AFTER_VISIT, "+
					" INSERT_AFTER_INTERVAL, "+
					" INSERT_AFTER_INTERVAL_UNIT, "+
					" 'V' as flag, pk_protocol_visit as FK_VISIT , '' as EVENT_CATEGORY, 0 as EVENT_SEQUENCE, offline_flag, hide_flag, "+
					" '' as Facilityname,"+
					" '' as SiteOfService,"+
					" 0 as costvalue, 0 as costcurrency, 0 as costtype,"+
					" WIN_AFTER_NUMBER, "+
					" WIN_AFTER_UNIT, "+
					" WIN_BEFORE_NUMBER, "+
					" WIN_BEFORE_UNIT "+
					" , 0 as coverageType "+
					",'' as EVENT_CPTCODE" +
					" FROM sch_protocol_visit s"+
					" WHERE  "); 
					preSql.append(" s.fk_protocol= ? "); 

			
			preSql.append(" union "); 
		}		
		
		preSql.append(" SELECT DECODE((SELECT NUM_DAYS FROM SCH_PROTOCOL_VISIT v WHERE v.pk_protocol_visit= a.FK_VISIT),0,0,DISPLACEMENT) AS DISPLACEMENT, "+ 
	    	    " EVENT_ID, ORG_ID, "+
	            " CHAIN_ID, EVENT_TYPE, NAME, NOTES, "+
	            " PK_EVENTCOST, COST, COST_DESCRIPTION, DURATION, DURATION_UNIT,"+
	            " USER_ID, LINKED_URI, FUZZY_PERIOD, "+
	            " MSG_TO,  FK_CODELST_CALSTAT as codeStatus,DESCRIPTION, "+
	            " 0 AS MONTH , "+
	            " 0 AS WEEK , "+
	            " 0 AS DAY, "+
	            " '' AS INSERT_AFTER_VISIT, "+
                " 0 AS INSERT_AFTER_INTERVAL, "+
                " '' AS INSERT_AFTER_INTERVAL_UNIT, "+
	            " 'E' as flag, FK_VISIT,  EVENT_CATEGORY, EVENT_SEQUENCE, offline_flag, hide_flag, "+
	            " (select b.SITE_NAME||'" + VELSEP + "'||b.PK_SITE||'" + VELSEP + "'||b.SITE_ALTID from er_site b where b.pk_site = a.FACILITY_ID) as Facilityname, "+
	            " (select c.SITE_NAME||'" + VELSEP + "'||c.PK_SITE||'" + VELSEP + "'||c.SITE_ALTID from er_site c where c.pk_site = a.SERVICE_SITE_ID) as SiteOfService, "+
	            "  evtcost.eventcost_value costvalue, evtcost.fk_currency costcurrency, evtcost.fk_cost_desc costtype, "+
	            " TO_NUMBER(EVENT_FUZZYAFTER) AS WIN_AFTER_NUMBER, "+
	            " EVENT_DURATIONAFTER AS WIN_AFTER_UNIT, "+
	            " TO_NUMBER(FUZZY_PERIOD) AS WIN_BEFORE_NUMBER , "+
	            " EVENT_DURATIONBEFORE as WIN_BEFORE_UNIT "+
	            " , FK_CODELST_COVERTYPE as coverageType"+
	            ", EVENT_CPTCODE "+
	            " FROM EVENT_ASSOC a , sch_eventcost evtcost WHERE  a.event_id = evtcost.fk_event(+) "); 
		if(eventPK != null) preSql.append(" and a.event_id = ?"); 
		else if(visitPK != null) preSql.append(" and a.fk_visit = ?"); 
		else if(calendarPK != null) preSql.append(" and a.CHAIN_ID= ? "); 
	    
		preSql.append(" AND a.FK_VISIT IS NOT NULL and a.event_type <> 'U' ORDER BY DISPLACEMENT,fk_visit,flag desc, EVENT_SEQUENCE"); 
		
		return preSql.toString(); 
	}
	
	/**
	 * 
	 * @param eventPK
	 * @param userAccountPK
	 * @return
	 * @throws SQLException
	 */
	public CalendarEvent getStudyCalendarEvent(int eventPK, Integer userAccountPK) throws SQLException
	{
		CalendarEvent event = new CalendarEvent() ; 
		Connection conn = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try
		{
		   CodeCache codeCache = CodeCache.getInstance();		   
		   conn = getConnection(); 
		   stmt = conn.prepareStatement(constructVisitEventSQL(null, null, eventPK)); 
		   stmt.setInt(1, eventPK); 
		   rs = stmt.executeQuery(); 
		   if(rs.next())
		   {

			   ObjectMapService  mapService = ServicesUtil.getObjectMapService();
			   event = populateEvent(rs, mapService, codeCache, userAccountPK); 
			   ParentIdentifier eventParentIdentifier = new ParentIdentifier(); 
			   VisitIdentifier eventParentVisitIdentifier = new VisitIdentifier(); 
			   ObjectMap mapVisit = mapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_VISIT, rs.getInt("FK_VISIT")); 
			   eventParentVisitIdentifier.setOID(mapVisit.getOID()); 
			   eventParentVisitIdentifier.setPK(rs.getInt("FK_VISIT"));
			   eventParentIdentifier.addIdentifier(eventParentVisitIdentifier); 

			   ObjectMap calendarMap = mapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY_CALENDAR, rs.getInt("CHAIN_ID")); 
			   CalendarIdentifier calendarIdentifier = new CalendarIdentifier(); 
			   calendarIdentifier.setOID(calendarMap.getOID());
			   calendarIdentifier.setPK(rs.getInt("CHAIN_ID")); 
			   eventParentIdentifier.addIdentifier(calendarIdentifier); 

			   event.setParentIdentifier(eventParentIdentifier); 
			   while(rs.next())
			   {

				   event.getCosts().setCost(populateCost(rs, codeCache, userAccountPK,mapService)); 	

			   }

			 
		   }
		}finally
		{
			closeResultSet(rs); 
			closeStatement(stmt); 
			returnConnection(conn);
			
		}
		
		return event; 
	}
	/**
	 * 
	 * @param rs
	 * @param mapService
	 * @param codeCache
	 * @param userAccountPK
	 * @return
	 * @throws SQLException
	 */
	private CalendarEvent  populateEvent(ResultSet rs, ObjectMapService mapService, CodeCache codeCache, Integer userAccountPK) throws SQLException
	{
		   CalendarEvent event = new CalendarEvent(); 
		   Integer eventID = rs.getInt("EVENT_ID"); 
		   ObjectMap map = mapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY_CALENDAR, eventID); 
		   EventIdentifier eventIdentifier = new EventIdentifier(); 
		   eventIdentifier.setOID(map.getOID());
		   eventIdentifier.setPK(eventID);
		   CalendarEventSummary eventSummary = new CalendarEventSummary(); 
		   eventSummary.setEventIdentifier(eventIdentifier);
		   eventSummary.setEventName(rs.getString("NAME")); 
		   eventSummary.setDescription(rs.getString("DESCRIPTION")); 
		   eventSummary.setEventDuration(new Duration(rs.getInt("DURATION"), rs.getString("DURATION_UNIT"))); 
		   eventSummary.setEventWindowAfter(new Duration(rs.getInt("WIN_AFTER_NUMBER"), rs.getString("WIN_AFTER_UNIT")));
		   eventSummary.setEventWindowBefore(new Duration(rs.getInt("WIN_BEFORE_NUMBER"), rs.getString("WIN_BEFORE_UNIT")));
		   eventSummary.setNotes(rs.getString("NOTES")); 
		   eventSummary.setSequence(rs.getInt("EVENT_SEQUENCE"));
		   eventSummary.setCoverageType(codeCache.getSchCodeSubTypeByPK(CodeCache.CODE_TYPE_COVERAGE_TYPE, rs.getInt("coverageType"), userAccountPK));		 
		   eventSummary.setCPTCode(rs.getString("EVENT_CPTCODE")); 
		   
		   String facility = rs.getString("Facilityname");
		   if(facility != null && facility.length() > 0)
		   {
			   String[] facilityArray = facility.split(VELSEP_REGX); 						   
			   OrganizationIdentifier identifier = new OrganizationIdentifier(); 
			   identifier.setSiteName(facilityArray[0]);
			   
			   ObjectMap mapOrg = mapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, EJBUtil.stringToInteger(facilityArray[1]));						   
			   identifier.setOID(mapOrg.getOID());
			   if(facilityArray.length >= 2)//Bug Fix : 15803, 15880, 15860
			   {
				   identifier.setPK(EJBUtil.stringToInteger(facilityArray[1])); 
			   }
			   eventSummary.setFacility(identifier); 
			  
		   }
		   
		   
		   String siteOfService = rs.getString("SiteOfService"); 
		   if(siteOfService != null && siteOfService.length() > 0)
		   {
			   String[] siteOfServiceArray = siteOfService.split(VELSEP_REGX); 						   
			   OrganizationIdentifier identifier = new OrganizationIdentifier(); 
			   identifier.setSiteName(siteOfServiceArray[0]);
			   
			   ObjectMap mapOrg = mapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, EJBUtil.stringToInteger(siteOfServiceArray[1]));						   
			   identifier.setOID(mapOrg.getOID()); 
			   if(siteOfServiceArray.length >= 2)//Bug Fix : 15803, 15880, 15860
			   {
				   identifier.setPK(EJBUtil.stringToInteger(siteOfServiceArray[1])); 
			   }
			   eventSummary.setSiteOfService(identifier);  
			   
		   }
		   
		   
		   if(rs.getBigDecimal("costvalue") != null)// if cost is null - no need to put costs tags in response
		   {
			   Costs costs = new Costs(); 
			   costs.setCost(populateCost(rs, codeCache, userAccountPK, mapService)); 
			   event.setCosts(costs);						
		   }
		   
		   event.setCalendarEventSummary(eventSummary); 
		   return event; 
	}
	/**
	 * 
	 * @param rs
	 * @param codeCache
	 * @param userAccountPK
	 * @return
	 * @throws SQLException
	 */
	private Cost populateCost(ResultSet rs, CodeCache codeCache,Integer userAccountPK, ObjectMapService mapService) throws SQLException
	{
		Cost cost = new Cost(); 
		cost.setCost(rs.getBigDecimal("costvalue")); 
		Code costType = codeCache.getSchCodeSubTypeByPK(CodeCache.CODE_TYPE_COST_DESC, rs.getInt("costtype"), userAccountPK); 
		cost.setCostType(costType); 
		Code currency = codeCache.getSchCodeSubTypeByPK(CodeCache.CODE_TYPE_CURRENCY, rs.getInt("costcurrency"), userAccountPK); 
		cost.setCurrency(currency); 
		
		EventCostIdentifier costIdentifier = new EventCostIdentifier();
		ObjectMap map =mapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_EVENT_COST, rs.getInt("PK_EVENTCOST")); 
		
		costIdentifier.setOID(map.getOID());
		costIdentifier.setPK(rs.getInt("PK_EVENTCOST"));
		
		cost.setEventCostIdentifier(costIdentifier); 
		
		return cost; 	
	}
	/**
	 * 
	 * @param rs
	 * @param visitSeq
	 * @param mapService
	 * @return
	 * @throws SQLException
	 */
	private CalendarVisit populateVisit(ResultSet rs, Integer visitSeq, ObjectMapService mapService) throws SQLException
	{
		   CalendarVisit visit = new CalendarVisit(); 
		   CalendarVisitSummary visitSummary = new CalendarVisitSummary(); 
		   visitSummary.setVisitName(rs.getString("NAME")); 
		   VisitIdentifier visitIdentifier = new VisitIdentifier(); 				   
		   ObjectMap map = mapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_VISIT, rs.getInt("FK_VISIT")); 
		   visitIdentifier.setOID(map.getOID());
		   visitIdentifier.setPK(rs.getInt("FK_VISIT"));
		   visitSummary.setVisitIdentifier(visitIdentifier); 
			
			boolean noIntervalDefined = false; 
			if(rs.getString("DISPLACEMENT") == null)
			{
				noIntervalDefined = true; 
				
			}else if(rs.getString("INSERT_AFTER_VISIT") == null || rs.getString("INSERT_AFTER_VISIT").length() == 0)
			{
				visitSummary.setAbsoluteInterval(new Duration(rs.getInt("DISPLACEMENT"), "D"));
			}else
			{
	
				visitSummary.setRelativeVisitName(rs.getString("INSERT_AFTER_VISIT")); 
				Duration relativeVisitInterval = new Duration(rs.getInt("INSERT_AFTER_INTERVAL"), rs.getString("INSERT_AFTER_INTERVAL_UNIT")); 
				visitSummary.setRelativeVisitInterval(relativeVisitInterval); 
			}
			
			visitSummary.setVisitSequence(visitSeq++);
			visitSummary.setVisitWindowAfter(new Duration(rs.getInt("WIN_AFTER_NUMBER"), rs.getString("WIN_AFTER_UNIT")))	; 
			visitSummary.setVisitWindowBefore(new Duration(rs.getInt("WIN_BEFORE_NUMBER"), rs.getString("WIN_BEFORE_UNIT"))); 
			visitSummary.setNoIntervalDefined(noIntervalDefined); 
			visitSummary.setVisitDescription(rs.getString("DESCRIPTION")); 
			
			visit.setCalendarVisitSummary(visitSummary); 
			CalendarEvents events = new CalendarEvents(); 
			visit.setEvents(events); 
			return visit; 
	}
	/**
	 * 
	 * @param visitName
	 * @param calendarPK
	 * @return
	 */
	public Integer getVisitPKByName(String visitName, Integer calendarPK)
	{
		PreparedStatement stmt = null ; 
		ResultSet rs = null ; 
		Connection conn = null; 
		Integer visitPK = null;
		try
		{
		
			conn = getConnection(); 
			String sql = "select pk_protocol_visit from sch_protocol_visit where upper(visit_name) = ? and fk_protocol= ?"; 
			stmt  = conn.prepareStatement(sql); 
			stmt.setString(1, visitName.toUpperCase()); 
			stmt.setInt(2, calendarPK); 
			
			rs = stmt.executeQuery(); 
			
			if(rs.next())
			{
				visitPK = rs.getInt(1); 
			}
	
		}catch(SQLException sqe)
		{
			logger.error("Unable to get Visit PK by Name ", sqe); 			 
		}finally
		{
			closeResultSet(rs); 
			closeStatement(stmt); 
			returnConnection(conn); 
		}
		
		return visitPK; 
	}
	
	
	private void closeStatement(PreparedStatement stmt)
	{
		if(stmt != null)
		{
			try{
				stmt.close(); 
			}catch(SQLException sqe)
			{
				
			}
		}
	}
	
	private void closeResultSet(ResultSet rs)
	{
		if(rs != null)
		{
			try
			{
				rs.close(); 
			}catch(SQLException sqe)
			{
				
			}
		}
		
	}

	/**
	 * @param eventName
	 * @param visitPK
	 * @return
	 */
	public Integer getEventPKByEventName(String eventName, Integer visitPK) throws OperationException{
		PreparedStatement stmt = null ; 
		ResultSet rs = null ; 
		Connection conn = null; 
		Integer eventPK = null; 
		try
		{
		
			conn = getConnection(); 
			String sql = "select event_id from event_assoc where upper(name) = ? and fk_visit= ?"; 
			stmt  = conn.prepareStatement(sql); 
			stmt.setString(1, eventName.toUpperCase()); 
			stmt.setInt(2, visitPK); 
			
			rs = stmt.executeQuery(); 
			
			if(rs.next())
			{
				eventPK = rs.getInt(1); 
			}
	
		}catch(SQLException sqe)
		{
			logger.error("Unable to get Visit PK by Name ", sqe); 
			throw new OperationException(sqe); 
		}finally
		{
			closeResultSet(rs); 
			closeStatement(stmt); 
			returnConnection(conn); 
		}
		
		return eventPK; 
	}
	
	/**
	 * 
	 * @param visitPK
	 */
	public void getCalendarStudyPKForVisit(int visitPK) throws OperationException
	{
		PreparedStatement stmt = null ; 
		ResultSet rs = null; 
		Connection conn = null ; 
		
		try
		{
			String sql = "select b.event_id,  b.chain_id, a.OFFLINE_FLAG from sch_protocol_visit a, event_assoc b" +
					" where a.fk_protocol = b.event_id and pk_protocol_visit = ?"; 
			conn = getConnection(); 
			stmt = conn.prepareStatement(sql); 
			stmt.setInt(1, visitPK); 
			rs = stmt.executeQuery();
			while(rs.next())
			{
				calendarPK = rs.getInt(1); 
				studyPK = rs.getInt(2);
				addedBeforeOffline =rs.getInt(3) == 1?true: false; 
				
			}
			
		} catch (SQLException e) {
			new OperationException(e); 
		}finally
		{
			closeResultSet(rs); 
			closeStatement(stmt); 
			returnConnection(conn); 
		}
		
	}
	/**
	 * 
	 * @param eventPK
	 */
	public void getCalendarStudyPKForEvent(int eventPK) throws OperationException
	{
		PreparedStatement stmt = null ; 
		ResultSet rs = null; 
		Connection conn = null ; 
		
		try
		{
			String sql = "select  b.event_id, b.chain_id, a.OFFLINE_FLAG from event_assoc a, event_assoc b" +
					" where a.chain_id = b.event_id and b.event_type = ? and a.event_id = ?"; 
			conn = getConnection(); 
			stmt = conn.prepareStatement(sql); 
			stmt.setString(1, "P"); 
			stmt.setInt(2, eventPK);  
			rs = stmt.executeQuery(); 
			while(rs.next())
			{
				calendarPK = rs.getInt(1); 
				studyPK = rs.getInt(2); 
				addedBeforeOffline =rs.getInt(3) == 1?true: false; 
			}
			
		} catch (SQLException e) {
			throw new OperationException(e); 
		}finally
		{
			closeResultSet(rs); 
			closeStatement(stmt); 
			returnConnection(conn); 
		}
		
	}
	/**
	 * 
	 * @param calendarName
	 * @param studyPK
	 * @return
	 */
	public Integer checkDuplicateCalendarsInStudy(String  calendarName, Integer studyPK) throws OperationException
	{
		PreparedStatement stmt = null ; 
		ResultSet rs = null; 
		Connection conn = null ; 
		Integer count = 0;
		try
		{
			
			String sql = "SELECT COUNT(event_id) FROM EVENT_ASSOC WHERE event_type = 'P' AND LOWER(trim(NAME)) = ? AND chain_id = ?"; 
			conn = getConnection(); 
			stmt = conn.prepareStatement(sql); 
			stmt.setString(1,calendarName.toLowerCase()); 
			stmt.setInt(2,studyPK); 
			  
			rs = stmt.executeQuery(); 
			if(rs.next())
			{
				count = rs.getInt(1); 
			}
			
		} catch (SQLException e) {
			throw new OperationException(e); 
		}finally
		{
			closeResultSet(rs); 
			closeStatement(stmt); 
			returnConnection(conn); 
		}
		return count;
		
	}
	/**
	 * 
	 * @param code
	 * @param codeType
	 * @param accountId
	 * @return
	 */
	public Integer locateCatLibPK(Code code, String codeType, int accountId)
	throws OperationException
	{
		PreparedStatement stmt = null ; 
		ResultSet rs = null; 
		Connection conn = null ; 
		Integer catLibPK= 0;
		try
		{
			
			String sql = "SELECT PK_CATLIB FROM ER_CATLIB WHERE CATLIB_NAME= ? AND CATLIB_TYPE=? "; 
			conn = getConnection(); 
			stmt = conn.prepareStatement(sql);
			
			stmt.setString(1,code.getCode()); 
			stmt.setString(2,codeType);
			//stmt.setInt(3,accountId); 
			  
			rs = stmt.executeQuery(); 
			if(rs.next())
			{
				catLibPK = rs.getInt(1); 
			}
			
		} catch (SQLException e) {
			throw new OperationException(e); 			
		}finally
		{
			closeResultSet(rs); 
			closeStatement(stmt); 
			returnConnection(conn); 
		}
		return catLibPK;
		
	}

	/**
	 * 
	 * @param eventName
	 * @param chain_id
	 * @return
	 */
	public int getCostIDforEventPool(String eventName,int chain_id ) throws OperationException {
		int costID  = 0; 
		Connection conn = null; 
		PreparedStatement stmt = null ; 
		ResultSet rs = null; 
		String sql = "select cost from event_assoc where chain_id = ? and LOWER(trim(Name)) = ? and fk_visit is null"; 
		
		try
		{
			conn = getConnection(); 
			stmt = conn.prepareStatement(sql); 
			stmt.setInt(1, chain_id); 
			stmt.setString(2, eventName.toLowerCase()); 
			
			rs = stmt.executeQuery(); 
			
			while(rs.next())
			{
				costID = rs.getInt(1); 
			}
					
		
		}catch(SQLException sqe)
		{
			throw new OperationException(sqe); 
		}finally
		{
			closeResultSet(rs); 
			closeStatement(stmt); 
			returnConnection(conn); 
		}
		return costID;
	}
	/**
	 * 
	 * @param visitPK
	 * @param sequence
	 * @param tableName
	 * @return
	 */
	public Integer getEventIdForVisitFromSequence(Integer visitPK,
			Integer sequence, String tableName)throws OperationException {
		int eventId  = 0; 
		Connection conn = null; 
		PreparedStatement stmt = null ; 
		ResultSet rs = null; 
		String sql = "select event_id from event_assoc where fk_visit =? and event_sequence =?"; 
		
		try
		{
			conn = getConnection(); 
			stmt = conn.prepareStatement(sql); 
			stmt.setInt(1, visitPK); 
			stmt.setInt(2, sequence);
			//stmt.setString(3, tableName); 
			
			rs = stmt.executeQuery(); 
			
			while(rs.next())
			{
				eventId = rs.getInt(1); 
			}
					
		
		}catch(SQLException sqe)
		{
			throw new OperationException(sqe); 
		}finally
		{
			closeResultSet(rs); 
			closeStatement(stmt); 
			returnConnection(conn); 
		}
		return eventId;
	}
	
	public Integer locateCalendarPK(String calendarName,
			Integer studyPK) throws OperationException{
		
		Integer eventId = 0;
		
		Connection conn = null; 
		PreparedStatement stmt = null ; 
		ResultSet rs = null; 
		String sql = "select event_id from event_assoc where name =? and chain_id =? and event_calassocto = 'P'"; 
		
		try
		{
			conn = getConnection(); 
			stmt = conn.prepareStatement(sql); 
			stmt.setString(1, calendarName); 
			stmt.setInt(2, studyPK);
			//stmt.setString(3, tableName); 
			
			rs = stmt.executeQuery(); 
			
			while(rs.next())
			{
				eventId = rs.getInt(1); 
			}
					
		
		}catch(SQLException sqe)
		{
			throw new OperationException(sqe); 
		}finally
		{
			closeResultSet(rs); 
			closeStatement(stmt); 
			returnConnection(conn); 
		}
		return eventId;
	}
	//Virendra
	public ArrayList<Integer> geEventPKsForVisit(Integer vistPK,
			Integer studyPK, Integer calendarPK) 
			throws OperationException{
		Integer eventId = 0;
		Connection conn = null; 
		PreparedStatement stmt = null ; 
		ResultSet rs = null; 
		String sql = "select event_id from event_assoc where fk_visit = ?  and chain_id = ? and event_type='A'"; 
		ArrayList<Integer> lstEventPks = new ArrayList<Integer>();
		try
		{
			conn = getConnection(); 
			stmt = conn.prepareStatement(sql); 
			stmt.setInt(1, vistPK);
			stmt.setInt(2, calendarPK);
			
			//stmt.setString(3, tableName); 
			
			rs = stmt.executeQuery(); 
			
			while(rs.next())
			{
				eventId = rs.getInt(1);
				if(eventId != null){
					lstEventPks.add(eventId);
				}
			}
					
		
		}catch(SQLException sqe)
		{
			throw new OperationException(sqe); 
		}finally
		{
			closeResultSet(rs); 
			closeStatement(stmt); 
			returnConnection(conn); 
		}
		return lstEventPks;
	}
	
	
	/**
	 * 
	 * @param visitPK
	 * @param sequence
	 * @return
	 */
	public List<CalendarEvent> getEventIDSequenceMapForVisit(Integer visitPK)throws OperationException {
	//NOTE: do not touch Sequence Logic for eSP is based on this. Please review that before making any changes. 
		Connection conn = null; 
		PreparedStatement stmt = null ; 
		ResultSet rs = null; 
		String sql = "select event_id, event_sequence from event_assoc where fk_visit =? order by event_sequence"; 
		List<CalendarEvent> seqMap = new ArrayList<CalendarEvent>(); 
		try
		{
			conn = getConnection(); 
			stmt = conn.prepareStatement(sql); 
			stmt.setInt(1, visitPK); 
		
			rs = stmt.executeQuery(); 
			
			while(rs.next())
			{
				CalendarEvent event = new CalendarEvent(); 
				CalendarEventSummary summary = new CalendarEventSummary(); 
				EventIdentifier identifier = new EventIdentifier(); 
				identifier.setPK(StringUtil.stringToInteger(rs.getString(1)));
				summary.setEventIdentifier(identifier); 
				event.setCalendarEventSummary(summary); 
				seqMap.add(event);
			}
					
		
		}catch(SQLException sqe)
		{
			throw new OperationException(sqe); 
		}finally
		{
			closeResultSet(rs); 
			closeStatement(stmt); 
			returnConnection(conn); 
		}
		return seqMap;
	}
	
	public Integer getMaxSeqForEventsOfVisit(Integer visitPK)throws OperationException {
	
			Connection conn = null; 
			PreparedStatement stmt = null ; 
			ResultSet rs = null; 
			String sql = "select max(EVENT_SEQUENCE) from event_assoc where fk_visit = ? and (DISPLACEMENT <> 0 or DISPLACEMENT IS NULL)"; 
			Integer maxSequence = null;
			try
			{
				conn = getConnection(); 
				stmt = conn.prepareStatement(sql); 
				stmt.setInt(1, visitPK); 
			
				rs = stmt.executeQuery(); 
				
				while(rs.next())
				{
					maxSequence = rs.getInt(1); 
					
				}
						
			
			}catch(SQLException sqe)
			{
				throw new OperationException(sqe); 
			}finally
			{
				closeResultSet(rs); 
				closeStatement(stmt); 
				returnConnection(conn); 
			}
			return maxSequence;
		}
	
	public void updatedDisplacementForEventsInVisit(Integer calendarPK, Integer visitPK, Integer displacement)
	{
		Connection conn = null; 
		PreparedStatement stmt = null; 
		
		String sql = "UPDATE EVENT_ASSOC SET displacement = ? WHERE chain_id = ? AND fk_visit = ?"; 
		try
		{
			conn = getConnection(); 
			stmt = conn.prepareStatement(sql); 
			stmt.setString(1, EJBUtil.integerToString(displacement)); 
			stmt.setInt(2, calendarPK); 
			stmt.setInt(3, visitPK); 

			stmt.executeUpdate(); 
		}catch(SQLException sqe)
		{

		}finally
		{
			closeStatement(stmt); 
			returnConnection(conn); 
		}


		
	}
	
	
    public static boolean ifEventExists(int eventPK) throws OperationException{
    	
    	String sql = "select count(*) as Count from event_assoc where event_id = ?";
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	boolean flag = false;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, eventPK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		if(rs.getInt("Count") ==1)
				   flag = true;
				else 
				   flag =  false;
        	}
        	return flag;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
    
    public static boolean ifVisitExists(int visitPK) throws OperationException{
    	
    	String sql = "select count(*) as Count from sch_protocol_visit where pk_protocol_visit = ?";
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	boolean flag = false;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, visitPK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		if(rs.getInt("Count") ==1)
				   flag = true;
				else 
				   flag =  false;
        	}
        	return flag;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
    
    public static boolean ifCalendarExists(int calPK) throws OperationException{
    	
    	String sql = "select count(*) as Count from event_assoc where event_id = ?";
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	boolean flag = false;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, calPK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		if(rs.getInt("Count") ==1)
				   flag = true;
				else 
				   flag =  false;
        	}
        	return flag;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
    
    //Bug Fix : 16267
    public static boolean ifStudyExists(int studyPK) throws OperationException{
    	
    	String sql = "select count(*) as Count from er_study where pk_study = ?";
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	boolean flag = false;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, studyPK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		if(rs.getInt("Count") ==1)
				   flag = true;
				else 
				   flag =  false;
        	}
        	return flag;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
	
}
