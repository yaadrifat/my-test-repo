/**
 * Created On Jun 8, 2011
 */
package com.velos.services.calendar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.esch.business.common.ProtVisitDao;
import com.velos.esch.business.eventassoc.impl.EventAssocBean;
import com.velos.esch.business.protvisit.impl.ProtVisitBean;
import com.velos.esch.service.eventassocAgent.EventAssocAgentRObj;
import com.velos.esch.service.protvisitAgent.ProtVisitAgentRObj;
import com.velos.services.AbstractService;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ValidationException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.CalendarVisit;
import com.velos.services.model.CalendarVisitSummary;
import com.velos.services.model.CalendarVisits;
import com.velos.services.model.Duration;
import com.velos.services.model.VisitNameIdentifier;
import com.velos.services.ResponseHolder;
import com.velos.services.model.Duration.TimeUnits;

/**
 * @author Kanwaldeep
 *
 */
public class CalendarVisitHelper{
	
	private static Logger logger = Logger.getLogger(CalendarVisitHelper.class); 
	
	private int visitNum; 
	
	public void addVisitsToCalendar(Integer calendarID, CalendarVisits newVisits, int calendarDuration, Map<String, Object> parameters) throws OperationException
	{
		//Get list of saved Visits for this Calendar - used to validate Names and get Relative Visit PK 
		ProtVisitDao dao = ((ProtVisitAgentRObj)parameters.get("ProtVisitAgentRObj")).getProtocolVisits(calendarID); 	
		ProtVisitAgentRObj protVisitAgent = (ProtVisitAgentRObj)parameters.get("ProtVisitAgentRObj"); 
		List visitIDs = dao.getVisit_ids(); 
		List visitNames = dao.getNames();
		List<String> visitNamesUpperCase = new ArrayList<String>(); 
		List visitDisplacements = dao.getDisplacements(); 
		List visitNumOfDays = dao.getDays(); 
		
		for(int j = 0; j < visitNames.size(); j++)
		{
			visitNamesUpperCase.add(visitNames.get(j).toString().toUpperCase()); 
		}
		
		Map<String, CalendarVisit> visitNotSaved = new HashMap<String, CalendarVisit>(); 
		
		// Map for all the events to be added and corresponding costID 
		Map<String, Integer> eventNames =  new HashMap<String, Integer>(); 
		int costMax = 0; 
		for(CalendarVisit visit: newVisits.getVisit())
		{
			if(visit.getCalendarVisitSummary().getVisitName() == null || visit.getCalendarVisitSummary().getVisitName().length() == 0)
			{
				Issue issue = new Issue(IssueTypes.DATA_VALIDATION, "visitName is required to create a Visit"); 
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
				throw new OperationException(issue.toString());
			}
			if(visitNotSaved.containsKey(visit.getCalendarVisitSummary().getVisitName().toUpperCase()))
			{
				Issue issue = new Issue(IssueTypes.DUPLICATE_VISIT_NAME, "For Visit Name : " + visit.getCalendarVisitSummary().getVisitName());
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
				
			}
			visitNotSaved.put(visit.getCalendarVisitSummary().getVisitName().toUpperCase(), visit);
			
			if(visit.getEvents()!= null && visit.getEvents().getEvent() != null &&  visit.getEvents().getEvent().size() > 0)
			{
				CalendarEventHelper eventHelper = new CalendarEventHelper(); 
				costMax = eventHelper.populateEventNames(visit.getEvents(), eventNames, calendarID, parameters, costMax); 	
			}

		}
		
		//get VisitNum
		try
		{
			visitNum = protVisitAgent.getMaxVisitNo(calendarID);	
		}catch(NullPointerException npe)
		{
			// do nothing - Calendar is not created yet
		}		
	
		while(!visitNotSaved.isEmpty())
		{
			visitNotSaved = addVisitsToCalendar(calendarID, visitNotSaved,  visitIDs, visitNamesUpperCase, visitDisplacements, calendarDuration, parameters, newVisits.getVisit(), visitNumOfDays, eventNames); 
		}
		
	}
	/**
	 * @param calendarID
	 * @param visitNotSaved
	 * @param savedVisits
	 */
	private Map<String, CalendarVisit> addVisitsToCalendar(int calendarID,
			Map<String,CalendarVisit> visitList, List visitIDs, List<String> visitNames, List visitDisplacements, int calendarDuration, Map<String, Object> parameters, List<CalendarVisit> calendarVisits, List visitNumofDays, Map<String, Integer> eventNames) throws OperationException{
		Map<String, CalendarVisit> visitNotSaved = new HashMap<String, CalendarVisit>(); 

		for(CalendarVisit visit: visitList.values())
		{

			 if(visit.getCalendarVisitSummary().getRelativeVisitName() == null || visitNames.contains(visit.getCalendarVisitSummary().getRelativeVisitName().toUpperCase()))

			 {
				 int visitPK = addVisitToStudyCalendar(visit, calendarID, visitIDs, visitNames, visitDisplacements, calendarDuration, parameters, visitNumofDays, eventNames);
				 visitIDs.add(visitPK);
				 visitNames.add(visit.getCalendarVisitSummary().getVisitName().toUpperCase()); 
				 		 
			
			 }else if(visitList.containsKey(visit.getCalendarVisitSummary().getRelativeVisitName().toUpperCase()))
			 {
				 //check if relative visit is there in still to add List 				 
				 visitNotSaved.put(visit.getCalendarVisitSummary().getVisitName() , visit); 
			 }
			 else
			 {
				 Issue issue = new Issue(IssueTypes.VISIT_NOT_FOUND, "Relative visit not found :Relative Visit Name : " + visit.getCalendarVisitSummary().getRelativeVisitName());
				 ((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
				 logger.error(issue);
				 throw new OperationException(issue.toString());
			 }
			
				 
		}
		
		return visitNotSaved; 
		
	}

	/**
	 * @param visit
	 * @param calendarID
	 */
	@SuppressWarnings("unchecked")
	private Integer addVisitToStudyCalendar(CalendarVisit visit, int calendarID, List visitIDs, List<String> visitNames, List visitDisplacements, int calendarDuration, Map<String, Object> parameters, List visitNumOfDays, Map<String, Integer> eventNames) throws OperationException{
		
		ProtVisitAgentRObj protVisitAgent = (ProtVisitAgentRObj)parameters.get("ProtVisitAgentRObj"); 
		UserBean callingUser = (UserBean) parameters.get("callingUser"); 
		// check name if it already exists 
		if(visitNames.contains(visit.getCalendarVisitSummary().getVisitName().toUpperCase()))
		{
			Issue issue = new Issue(IssueTypes.VISIT_NAME_ALREADY_EXISTS, "For Visit Name : " + visit.getCalendarVisitSummary().getVisitName());
			((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
			logger.error(issue);
			throw new OperationException(issue.toString());
		}
		//
		ProtVisitBean visitBean = new ProtVisitBean(); 
		visitBean.setName(visit.getCalendarVisitSummary().getVisitName()); 	
		// visit_no is not sequence of visit it appears on screen it is order in which visits are entered.		
		visitBean.setVisit_no(++visitNum); 
		visitBean.setDescription(visit.getCalendarVisitSummary().getVisitDescription()); 
		
		visitBean.setInsertAfter(0);
		visitBean.setInsertAfterInterval(0);
		int days = 0; 
		int displacement = 0; 
		int checkVisit = 0; 
		if(visit.getCalendarVisitSummary().isNoIntervalDefined() != null && visit.getCalendarVisitSummary().isNoIntervalDefined())
		{
			//Bug Fix : 16121
			if(visit.getCalendarVisitSummary().getAbsoluteInterval() != null && visit.getCalendarVisitSummary().getAbsoluteInterval().getValue() != null){
				((ResponseHolder) parameters.get("ResponseHolder"))
				.getIssues().add(new Issue(IssueTypes.DATA_VALIDATION, "Cannot pass AbsoluteInterval tag with NoInternalDefined tag set as TRUE"));
				throw new ValidationException();
			}
			//Bug Fix : 16122
			if(visit.getCalendarVisitSummary().getRelativeVisitInterval() != null && visit.getCalendarVisitSummary().getRelativeVisitInterval().getValue() != null){
				((ResponseHolder) parameters.get("ResponseHolder"))
				.getIssues().add(new Issue(IssueTypes.DATA_VALIDATION, "Cannot pass RelativeVisitInterval tag with NoInternalDefined tag set as TRUE"));
				throw new ValidationException();
			}
			visitBean.setIntervalFlag("1"); 
		}
		else if(visit.getCalendarVisitSummary().getRelativeVisitName() != null && visit.getCalendarVisitSummary().getRelativeVisitName().length() > 0)
		{
			
			// fixed bug #6693 
			 if(visit.getCalendarVisitSummary().getRelativeVisitInterval().getValue() == null || visit.getCalendarVisitSummary().getRelativeVisitInterval().getValue()==0|| visit.getCalendarVisitSummary().getRelativeVisitInterval().getValue()<0)
			 {
				 Issue issue = new Issue(IssueTypes.DATA_VALIDATION, "RelativevisitInterval should be greater than zero"); 
					((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
					throw new OperationException();
			 }
			// get index for relative visit
			int i = visitNames.indexOf(visit.getCalendarVisitSummary().getRelativeVisitName().toUpperCase()); 
			
			visitBean.setInsertAfter((Integer) visitIDs.get(i));
			if(visit.getCalendarVisitSummary().getRelativeVisitInterval() != null)
			{
				if(visit.getCalendarVisitSummary().getRelativeVisitInterval().getUnit() == null)
				{
					((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
							new Issue(
									IssueTypes.DATA_VALIDATION, 
									"Please enter relative visit interval unit"));
					throw new OperationException();
				}
				
				if(!(visit.getCalendarVisitSummary().getRelativeVisitInterval().getUnit().equals(TimeUnits.DAY)))
				{
					((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
							new Issue(
									IssueTypes.DATA_VALIDATION, 
									"Please enter relative visit interval unit as 'DAY'"));
					throw new OperationException();

				}
				
			}
			
			visitBean.setInsertAfterInterval(visit.getCalendarVisitSummary().getRelativeVisitInterval().getValue());
			visitBean.setInsertAfterIntervalUnit(Duration.encodeUnit(visit.getCalendarVisitSummary().getRelativeVisitInterval().getUnit())); 
			
			if(EJBUtil.stringToInteger(visitNumOfDays.get(i).toString()) == 0)
			{
				displacement = 0; 
			}else
			{
				displacement = (Integer) visitDisplacements.get(i) ; 
			}
			
			displacement += visit.getCalendarVisitSummary().getRelativeVisitInterval().getValue(); 
			
			if(displacement  ==  calendarDuration)
			{
				checkVisit =  protVisitAgent.validateDayZeroVisit(calendarDuration, calendarID,1,0 );
			}
			
			if(displacement > calendarDuration || checkVisit > 0)
			{

				Issue issue = new Issue(IssueTypes.VISIT_INTERVAL_EXCEEDS_CALENDAR_DURATION, "For Visit Name : " + visit.getCalendarVisitSummary().getVisitName());
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
			}
			visitDisplacements.add(displacement); 
			visitNumOfDays.add(displacement); 
			visitBean.setDisplacement(Integer.toString(displacement)); 
			visitBean.setDays(displacement); 
			visitBean.setIntervalFlag("0"); 

		}else if(visit.getCalendarVisitSummary().getAbsoluteInterval() != null && visit.getCalendarVisitSummary().getAbsoluteInterval().getValue() != null 
				&& (visit.getCalendarVisitSummary().isNoIntervalDefined() != null && visit.getCalendarVisitSummary().isNoIntervalDefined().equals(Boolean.FALSE)))

		{
			if(visit.getCalendarVisitSummary().getAbsoluteInterval().getUnit() == null )
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(
								IssueTypes.DATA_VALIDATION, 
								"Please enter visit absolute unit"));
				throw new OperationException();

			}
			if(!(visit.getCalendarVisitSummary().getAbsoluteInterval().getUnit().equals(TimeUnits.DAY)))
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(
								IssueTypes.DATA_VALIDATION, 
								"Please enter visit absolute unit as 'DAY'"));
				throw new OperationException();

			}
			//Bug Fix 6555
			days = visit.getCalendarVisitSummary().getAbsoluteInterval().getValue();
			if(days== 0 )
			{
				displacement = 1; 
				checkVisit = protVisitAgent.validateDayZeroVisit(calendarDuration,calendarID,0,0);
				
			}else
			{
				displacement = days; 
			}
			
			if(displacement == calendarDuration)
			{
				checkVisit = protVisitAgent.validateDayZeroVisit(calendarDuration,calendarID,1,0 );
			}
			if(displacement > calendarDuration || checkVisit > 0)
			{

				Issue issue = new Issue(IssueTypes.VISIT_INTERVAL_EXCEEDS_CALENDAR_DURATION, "For Visit Name : " + visit.getCalendarVisitSummary().getVisitName());
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
			}
			visitBean.setDays(days); 
			visitBean.setDisplacement(Integer.toString(displacement));
			visitDisplacements.add(displacement);
			visitNumOfDays.add(days); 
			visitBean.setIntervalFlag("0"); 
			
		}else
		{
			((ResponseHolder) parameters.get("ResponseHolder"))
			.getIssues().add(new Issue(IssueTypes.DATA_VALIDATION, "Either NoIntervalDefined should be true or RelativeVisit and IntervalAfter or AbsoluteInterval is required"));
			throw new ValidationException();
		}
		
		int visitAfterValue = 0; 
		TimeUnits visitAfterUnit = TimeUnits.DAY; 
		if(visit.getCalendarVisitSummary().getVisitWindowAfter() != null)
		{
			visitAfterValue = visit.getCalendarVisitSummary().getVisitWindowAfter().getValue(); 
			visitAfterUnit = visit.getCalendarVisitSummary().getVisitWindowAfter().getUnit(); 	
			
			if(!visitAfterUnit.equals(TimeUnits.DAY)){

				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(
								IssueTypes.DATA_VALIDATION, 
								"Please enter visitWindowAfter unit as 'DAY'"));
				throw new OperationException();

			}
		}
		
		visitBean.setDurationAfter(Integer.toString(visitAfterValue)); 
		visitBean.setDurationUnitAfter(Duration.encodeUnit(visitAfterUnit));
		int visitBeforeValue = 0; 
		TimeUnits visitBeforeUnit = TimeUnits.DAY; 
		if(visit.getCalendarVisitSummary().getVisitWindowBefore() != null)
		{
			visitBeforeValue = visit.getCalendarVisitSummary().getVisitWindowBefore().getValue(); 
			visitBeforeUnit = visit.getCalendarVisitSummary().getVisitWindowBefore().getUnit(); 
			if(!visitBeforeUnit.equals(TimeUnits.DAY)){

				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(
								IssueTypes.DATA_VALIDATION, 
								"Please enter visitWindowBefore unit as 'DAY'"));
				throw new OperationException();

			}
			 
		}
		
		visitBean.setDurationBefore(Integer.toString(visitBeforeValue));
		visitBean.setDurationUnitBefore(Duration.encodeUnit(visitBeforeUnit));
		visitBean.setProtocol_id(calendarID); 
		visitBean.setCreator(EJBUtil.integerToString(callingUser.getUserId()))	; 
		visitBean.setip_add(AbstractService.IP_ADDRESS_FIELD_VALUE); 
	
		int responseSave = protVisitAgent.setProtVisitDetails(visitBean); 
		
	
		if(responseSave < 0)
		{
			if(responseSave == -1)
			{
				Issue issue = new Issue(IssueTypes.VISIT_NAME_ALREADY_EXISTS, "For Visit Name : " + visit.getCalendarVisitSummary().getVisitName());
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
			}
		}
		visitBean.setVisit_id(responseSave); 
		int visitPK = visitBean.getVisit_id(); 
		if(!(visit.getEvents() == null || visit.getEvents().getEvent().isEmpty()))
		{
			CalendarEventHelper helper = new CalendarEventHelper(); 
			helper.addEventsToVisit(visit.getEvents(),calendarID, visitPK, Integer.toString(displacement), parameters, eventNames); 
		}
		VisitNameIdentifier visitIdentifier = new VisitNameIdentifier(); // Bug Fix : 9778 : Tarandeep Singh Bali
		ObjectMap map = ((ObjectMapService) parameters.get("ObjectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_VISIT, visitPK); 
		visitIdentifier.setOID(map.getOID());
		visitIdentifier.setPK(visitPK);
		visitIdentifier.setVisitName(visit.getCalendarVisitSummary().getVisitName()); // Bug Fix : 9778 : Tarandeep Singh Bali
		((ResponseHolder) parameters.get("ResponseHolder")).addObjectCreatedAction(visitIdentifier); 
		return visitPK ; 
	}                   
	
	
	public void visitSummaryIntoBeanForUpdate(CalendarVisitSummary visitSummary, ProtVisitBean visitBean, int calendarID, Map<String, Object> parameters) throws OperationException  {
		
		ProtVisitDao dao = ((ProtVisitAgentRObj)parameters.get("ProtVisitAgentRObj")).getProtocolVisits(calendarID); 	
		ProtVisitAgentRObj protVisitAgent = (ProtVisitAgentRObj)parameters.get("ProtVisitAgentRObj"); 
		EventAssocAgentRObj eventAssocAgent = (EventAssocAgentRObj) parameters.get("EventAssocAgentRObj");
		StudyCalendarDAO studyCalendarDAO = new StudyCalendarDAO(); 
		
		List visitIDs = dao.getVisit_ids(); 
		List visitNames = dao.getNames();
		List<String> visitNamesUpperCase = new ArrayList<String>(); 
		List visitDisplacements = dao.getDisplacements(); 
		List visitNumOfDays = dao.getDays();
		
		for(int j = 0; j < visitNames.size(); j++)
		{
			visitNamesUpperCase.add(visitNames.get(j).toString().toUpperCase()); 
		}
		
		boolean isOffLine= false; 
		
		if(visitBean.getOfflineFlag() != null  && visitBean.getOfflineFlag().equals(1)) isOffLine = true; 
				
		EventAssocBean calendarBean = eventAssocAgent.getEventAssocDetails(calendarID); 
		if(calendarBean == null)
		{
			((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
					new Issue(
							IssueTypes.CALENDAR_NOT_FOUND, 
							"Calendar not found"));
			throw new OperationException();
		}
		
//		Integer studyPK = Integer.parseInt(calendarBean.getChain_id()); 
//		checkStudyTeamUpdatePermissions(studyPK); 
		//pass Calendar Duration 
		int duration= EJBUtil.stringToInteger(calendarBean.getDuration()); 
		
		
		 
		UserBean callingUser = (UserBean) parameters.get("callingUser"); 
		// check name if it already exists 
		//Kanwal fix  - No need to throw exception when it is null, user doesn't want to update leave as it is
		if(visitSummary.getVisitName() != null && visitSummary.getVisitName().length() == 0 )
		{
			Issue issue = new Issue(IssueTypes.DATA_VALIDATION, "Visit Name required");
			((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
			logger.error(issue);
			throw new OperationException(issue.toString());
		}
		// Bug Fix 7459
		if(visitSummary.getVisitName() != null)
		{
			if(isOffLine && !visitSummary.getVisitName().equals(visitBean.getName()))
			{
				Issue issue = new Issue(IssueTypes.DATA_VALIDATION, "Visit Name cannot be updated after Calendar with this visit was activated");
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
			}
			visitNamesUpperCase.remove(visitBean.getName().toUpperCase());
			if(visitNamesUpperCase.contains(visitSummary.getVisitName().toUpperCase()))
			{
				Issue issue = new Issue(IssueTypes.VISIT_NAME_ALREADY_EXISTS, "For Visit Name : " + visitSummary.getVisitName());
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
			}
			visitBean.setName(visitSummary.getVisitName());
		}
		
		 	
		// visit_no is not sequence of visit it appears on screen it is order in which visits are entered.		
		visitBean.setVisit_no(++visitNum); 
		if(visitSummary.getVisitDescription() != null) visitBean.setDescription(visitSummary.getVisitDescription()); 
		
		//visitBean.setInsertAfter(0);
		//visitBean.setInsertAfterInterval(0);
		int days = 0; 
		int displacement = 0; 
		int checkVisit = 0;
		
		if((visitSummary.isNoIntervalDefined() != null && visitSummary.isNoIntervalDefined().equals(Boolean.FALSE) )
				&&( visitSummary.getRelativeVisitInterval() !=null && visitSummary.getRelativeVisitName()!= null && visitSummary.getRelativeVisitName().length() == 0 && visitSummary.getRelativeVisitInterval().getUnit() == null)
				&&( visitSummary.getAbsoluteInterval() !=null && visitSummary.getAbsoluteInterval().getUnit() == null) )
		{
				((ResponseHolder) parameters.get("ResponseHolder"))
				.getIssues().add(new Issue(IssueTypes.DATA_VALIDATION, "Either NoIntervalDefined should be true or RelativeVisit and RelativeVisitInterval or AbsoluteInterval is required"));
				throw new ValidationException();
			
			
		}
		
		if(isOffLine && (visitSummary.isNoIntervalDefined() != null || visitSummary.getRelativeVisitInterval() != null || visitSummary.getAbsoluteInterval() != null))
		{
			((ResponseHolder) parameters.get("ResponseHolder"))
			.getIssues().add(new Issue(IssueTypes.DATA_VALIDATION, "Visit Interval cannot be changed for visits which were part of ACTIVE calendar"));
			throw new ValidationException();
			
		}
		
		if(visitSummary.isNoIntervalDefined() != null && visitSummary.isNoIntervalDefined())
		{
			if((visitSummary.getRelativeVisitName() != null && visitSummary.getRelativeVisitName().length() > 0) || (visitSummary.getAbsoluteInterval() != null && visitSummary.getAbsoluteInterval().getUnit() != null))
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(
								IssueTypes.DATA_VALIDATION, 
								"Interval can be only one of these: 'noIntervalDefined' ,  'relativeVisitInterval' or 'absoluteInterval'"));
				throw new OperationException();
			}
			visitBean.setIntervalFlag("1"); 
			visitBean.setInsertAfter(0); 
			visitBean.setInsertAfterInterval(0);
			visitBean.setInsertAfterIntervalUnit(null);
			visitBean.setDisplacement(null); 
			visitBean.setDays(null); 
			visitBean.setWeeks(0); 
			visitBean.setMonths(0);
			studyCalendarDAO.updatedDisplacementForEventsInVisit(calendarID, visitBean.getVisit_id(), null);
		}
		else if(visitSummary.getRelativeVisitName() != null && visitSummary.getRelativeVisitName().length() > 0)
		{
			if(visitSummary.getAbsoluteInterval() != null && visitSummary.getAbsoluteInterval().getValue() != null && visitSummary.getAbsoluteInterval().getUnit() !=null)
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(
								IssueTypes.DATA_VALIDATION, 
								"Interval can be only one of these: 'noIntervalDefined' ,  'relativeVisitInterval' or 'absoluteInterval'"));
				throw new OperationException();
			}
			// fixed bug #6693 
			//Virendra:# 7297, 7288
			 if(visitSummary.getRelativeVisitInterval() == null || visitSummary.getRelativeVisitInterval() == null ||
					 visitSummary.getRelativeVisitInterval().getValue()==0|| visitSummary.getRelativeVisitInterval().getValue()<0)
			 {
				 Issue issue = new Issue(IssueTypes.DATA_VALIDATION, "RelativevisitInterval should be greater than zero"); 
					((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
					throw new OperationException();
			 }
			// get index for relative visit
			int i = visitNamesUpperCase.indexOf(visitSummary.getRelativeVisitName().toUpperCase()); 
			if(i == -1)
			{
				Issue issue = new Issue(IssueTypes.DATA_VALIDATION, "Relative Visit name "+visitSummary.getRelativeVisitName() + " not found"); 
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
				throw new OperationException(); 
			}
			visitBean.setInsertAfter((Integer) visitIDs.get(i));
			if(visitSummary.getRelativeVisitInterval() != null)
			{
				if(visitSummary.getRelativeVisitInterval().getUnit() == null)
				{
					((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
							new Issue(
									IssueTypes.DATA_VALIDATION, 
									"Unit is required"));
					throw new OperationException();
				}
				if(!(visitSummary.getRelativeVisitInterval().getUnit().equals(TimeUnits.DAY)))
				{
					((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
							new Issue(
									IssueTypes.DATA_VALIDATION, 
									"Please enter visitWindowBefore unit as 'DAY'"));
					throw new OperationException();

				}
				
			}
			
			visitBean.setInsertAfterInterval(visitSummary.getRelativeVisitInterval().getValue());
			visitBean.setInsertAfterIntervalUnit(Duration.encodeUnit(visitSummary.getRelativeVisitInterval().getUnit())); 
			
			if(EJBUtil.stringToInteger(visitNumOfDays.get(i).toString()) == 0)
			{
				displacement = 0; 
			}else
			{
				displacement = (Integer) visitDisplacements.get(i) ; 
			}
			
			displacement += visitSummary.getRelativeVisitInterval().getValue(); 
			
			if(displacement  ==  duration)
			{
				checkVisit =  protVisitAgent.validateDayZeroVisit(duration, calendarID,1,0 );
			}
			
			if(displacement > duration || checkVisit > 0)
			{

				Issue issue = new Issue(IssueTypes.INVALID_VISIT_DURATION,"Visit exceeds the specified Calendar duration");
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
			}
	//		visitDisplacements.add(displacement); 
	//		visitNumOfDays.add(displacement); 
			visitBean.setDisplacement(Integer.toString(displacement)); 
			visitBean.setDays(displacement); 
			visitBean.setIntervalFlag("0");
			visitBean.setWeeks(0); 
			visitBean.setMonths(0); 
			studyCalendarDAO.updatedDisplacementForEventsInVisit(calendarID, visitBean.getVisit_id(), displacement);

		}else if(visitSummary.getRelativeVisitInterval() != null && visitSummary.getRelativeVisitInterval().getValue() != null)
		{
			Issue issue = new Issue(IssueTypes.INVALID_VISIT_DURATION,"For Visit Name : " + visitSummary.getVisitName());
			((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
			logger.error(issue);
			throw new OperationException(issue.toString());
		}else if(visitSummary.getAbsoluteInterval() != null && visitSummary.getAbsoluteInterval().getValue() != null)

		{	
			if(visitSummary.getAbsoluteInterval().getUnit()== null)
			{
				Issue issue = new Issue(IssueTypes.DATA_VALIDATION, "Please provide absoluteInterval unit'");
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
			}
			
			if(!visitSummary.getAbsoluteInterval().getUnit().equals(TimeUnits.DAY))
			{
				Issue issue = new Issue(IssueTypes.DATA_VALIDATION, "Please absoluteInterval unit as 'DAY'");
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
			}
			//Bug Fix 6555
			days = visitSummary.getAbsoluteInterval().getValue();
			if(days== 0 )
			{
				displacement = 1; 
				checkVisit = protVisitAgent.validateDayZeroVisit(duration,calendarID,0,0);
				
			}else
			{
				displacement = days; 
			}
			
			if(displacement == duration)
			{
				checkVisit = protVisitAgent.validateDayZeroVisit(duration,calendarID,1,0 );
			}
			if(displacement > duration || checkVisit > 0)
			{

				Issue issue = new Issue(IssueTypes.INVALID_VISIT_DURATION, "For Visit Name : " + visitSummary.getVisitName());
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
			}
			visitBean.setDays(days); 
			visitBean.setDisplacement(Integer.toString(displacement));
			
	//		visitDisplacements.add(displacement);
	//		visitNumOfDays.add(days); 
			visitBean.setInsertAfter(0); 
			visitBean.setIntervalFlag("0"); 
			visitBean.setInsertAfterInterval(0); 
			visitBean.setWeeks(0); 
			visitBean.setMonths(0); 
			studyCalendarDAO.updatedDisplacementForEventsInVisit(calendarID, visitBean.getVisit_id(), displacement);			
		}
		
		int visitAfterValue = 0; 
		TimeUnits visitAfterUnit = TimeUnits.DAY; 
		if(visitSummary.getVisitWindowAfter() != null)
		{
			visitAfterValue = visitSummary.getVisitWindowAfter().getValue(); 
			visitAfterUnit = visitSummary.getVisitWindowAfter().getUnit(); 	
			if(visitAfterUnit != null){
				if(!visitAfterUnit.equals(TimeUnits.DAY)){

					((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
							new Issue(
									IssueTypes.DATA_VALIDATION, 
							"Please enter visitWindowAfter unit as 'DAY'"));
					throw new OperationException();

				}
			visitBean.setDurationAfter(Integer.toString(visitAfterValue)); 
			visitBean.setDurationUnitAfter(Duration.encodeUnit(visitAfterUnit));
			}else
			{
				visitBean.setDurationAfter(Integer.toString(visitAfterValue)); 
				visitBean.setDurationUnitAfter("D");				
			}
		}
		int visitBeforeValue = 0; 
		TimeUnits visitBeforeUnit = TimeUnits.DAY; 
		if(visitSummary.getVisitWindowBefore() != null)
		{
			visitBeforeValue = visitSummary.getVisitWindowBefore().getValue(); 
			visitBeforeUnit = visitSummary.getVisitWindowBefore().getUnit(); 
			String visitBeforeDurationUnit = "D"; 
			if(visitBeforeUnit != null)
			{
				if(!visitBeforeUnit.equals(TimeUnits.DAY)){

					((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
							new Issue(
									IssueTypes.DATA_VALIDATION, 
							"Please enter visitWindowBefore unit as 'DAY'"));
					throw new OperationException();

				}
				
				visitBeforeDurationUnit = Duration.encodeUnit(visitBeforeUnit); 
			}



				visitBean.setDurationBefore(Integer.toString(visitBeforeValue));
				visitBean.setDurationUnitBefore(visitBeforeDurationUnit);
		}
		//visitBean.setProtocol_id(calendarID); 
		//visitBean.setCreator(EJBUtil.integerToString(callingUser.getUserId()))	; 
		visitBean.setip_add(AbstractService.IP_ADDRESS_FIELD_VALUE);
		
	}
}
