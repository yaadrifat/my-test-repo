/**
 * Created On Sep 8, 2011
 */
package com.velos.services.form;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.services.OperationException;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMapService;
import com.velos.services.map.ObjectMap; 
import com.velos.services.model.DateFieldValidations;
import com.velos.services.model.FieldIdentifier;
import com.velos.services.model.FormDesign;
import com.velos.services.model.FormDesign.LinkedTo;
import com.velos.services.model.FormDesign.SharedWith;
import com.velos.services.model.FormField;
import com.velos.services.model.FormField.Alignment;
import com.velos.services.model.FormField.DataType;
import com.velos.services.model.FormField.FieldType;
import com.velos.services.model.Code;
import com.velos.services.model.FieldValidations;
import com.velos.services.model.FormFields;
import com.velos.services.model.FormIdentifier;
import com.velos.services.model.FormInfo;
import com.velos.services.model.FormList;
import com.velos.services.model.FormSection;
import com.velos.services.model.FormSectionIdentifier;
import com.velos.services.model.FormSections;
import com.velos.services.model.FormType;
import com.velos.services.model.LinkedFormDesign;
import com.velos.services.model.LinkedFormDesign.DisplayType;
import com.velos.services.model.LinkedFormDesign.EntrySettings;
import com.velos.services.model.MultipleChoice;
import com.velos.services.model.MultipleChoices;
import com.velos.services.model.NumberFieldValidations;
import com.velos.services.model.NumberRange;
import com.velos.services.model.StudyFormDesign;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatientFormDesign;
import com.velos.services.model.StudyPatientForms;
import com.velos.services.model.TextFieldValidations;
import com.velos.services.util.CodeCache;
import com.velos.services.util.EnumUtil;


/**
 * @author Kanwaldeep
 *
 */
public class FormDesignDAO extends CommonDAO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8177890666129569926L;
	private static Logger logger = Logger.getLogger(FormDesignDAO.class); 
	
	public FormDesign getLibraryFormDesign(Integer formPK, boolean includeFormatting, UserBean callingUser, ObjectMapService objectMapService, FormDesign formDesign) throws OperationException
	{
		if(formDesign == null)
		{
			 formDesign = new FormDesign(); 
		}		
		StringBuffer sql = new StringBuffer(); 	
		
		sql.append("select d.FORM_NAME, d.FORM_DESC,d.FORM_SHAREDWITH,");
		sql.append(" d.FORM_STATUS,d.FORM_LINKTO, d.FORM_ESIGNREQ,d.FORM_NEXT,");
		sql.append(" a.PK_FORMSEC, a.formsec_name, a.formsec_seq, a.FORMSEC_OFFLINE, a.FORMSEC_REPNO, a.FORMSEC_FMT, b.PK_FORMFLD, b.formfld_seq, b.formfld_mandatory, b.formfld_browserflg,");
		sql.append(" c.pk_field, c.fld_name, c.fld_type, c.fld_datatype,c.FLD_DECIMAL, c.FLD_DESC,");
		sql.append(" c.fld_instructions, c.fld_length, c.fld_type, c.FLD_ISVISIBLE, c.FLD_KEYWORD, c.FLD_DEFRESP, c.FLD_LENGTH, c.FLD_LINESNO, c.FLD_CHARSNO, c.FLD_LKPDATAVAL,");
		sql.append(" c.FLD_LKPDISPVAL, c.FLD_LKPTYPE, c.FLD_ISREADONLY, c.FLD_SAMELINE, c.FLD_SORTORDER, c.FLD_SYSTEMID, c.FLD_UNDERLINE, c.FLD_ISUNIQUE, c.FLD_UNIQUEID,");
		sql.append(" c.FLD_OVERRIDE_MANDATORY, c.FLD_OVERRIDE_RANGE, c.FLD_OVERRIDE_DATE, c.FLD_OVERRIDE_FORMAT, c.FLD_TODAYCHECK, c.FLD_COLCOUNT, c.FLD_BOLD, c.FLD_ITALICS, c.FLD_ALIGN,");
		sql.append(" c.FLD_INSTRUCTIONS, c.FLD_KEYWORD, C.FLD_NAME_FORMATTED, c.FLD_DISPLAY_WIDTH, c.FLD_HIDELABEL, c.FLD_EXPLABEL, c.fld_format,");
		sql.append(" c.FLD_HIDERESPLABEL, c.FLD_SORTORDER , c.FLD_RESPALIGN ,");
		sql.append(" e.FLDRESP_DATAVAL, e.FLDRESP_DISPVAL, e.FLDRESP_ISDEFAULT, e.FLDRESP_OFFLINE, e.FLDRESP_SCORE, e.FLDRESP_SEQ, e.RECORD_TYPE,");
		sql.append(" f.FLDVALIDATE_OP1, f.FLDVALIDATE_VAL1, f.FLDVALIDATE_LOGOP1, f.FLDVALIDATE_OP2, f.FLDVALIDATE_VAL2"); 
		sql.append(" from er_formsec a, er_formfld b, er_fldlib c, er_formlib d, ER_FLDRESP e, ER_FLDVALIDATE f");
		sql.append(" where a.PK_FORMSEC = b.FK_FORMSEC");
		sql.append(" and a.FK_FORMLIB = d.PK_FORMLIB");
		sql.append(" and b.FK_FIELD = c.PK_FIELD");
		sql.append(" and f.FK_FLDLIB(+) = c.PK_FIELD");
		sql.append(" and e.FK_FIELD(+) = c.PK_FIELD");
		sql.append(" and c.RECORD_TYPE <> 'D'");
		sql.append(" and d.PK_FORMLIB = ?");
		sql.append(" UNION");
		sql.append(" select d.FORM_NAME, d.FORM_DESC,d.FORM_SHAREDWITH,");
		sql.append(" d.FORM_STATUS,d.FORM_LINKTO, d.FORM_ESIGNREQ,d.FORM_NEXT,");
		sql.append(" a.PK_FORMSEC, a.formsec_name, a.formsec_seq, a.FORMSEC_OFFLINE, a.FORMSEC_REPNO, a.FORMSEC_FMT, b.PK_FORMFLD,b.formfld_seq, b.formfld_mandatory, b.formfld_browserflg,");
		sql.append(" c.pk_field, c.fld_name, c.fld_type, c.fld_datatype,c.FLD_DECIMAL, c.FLD_DESC,");
		sql.append(" c.fld_instructions, c.fld_length, c.fld_type, c.FLD_ISVISIBLE, c.FLD_KEYWORD, c.FLD_DEFRESP, c.FLD_LENGTH, c.FLD_LINESNO, c.FLD_CHARSNO, c.FLD_LKPDATAVAL,");
		sql.append(" c.FLD_LKPDISPVAL, c.FLD_LKPTYPE, c.FLD_ISREADONLY, c.FLD_SAMELINE, c.FLD_SORTORDER, c.FLD_SYSTEMID, c.FLD_UNDERLINE, c.FLD_ISUNIQUE, c.FLD_UNIQUEID,");
		sql.append(" c.FLD_OVERRIDE_MANDATORY, c.FLD_OVERRIDE_RANGE, c.FLD_OVERRIDE_DATE, c.FLD_OVERRIDE_FORMAT, c.FLD_TODAYCHECK, c.FLD_COLCOUNT, c.FLD_BOLD, c.FLD_ITALICS, c.FLD_ALIGN,");
		sql.append(" c.FLD_INSTRUCTIONS, c.FLD_KEYWORD, C.FLD_NAME_FORMATTED, c.FLD_DISPLAY_WIDTH, c.FLD_HIDELABEL, c.FLD_EXPLABEL, c.fld_format,");
		sql.append(" c.FLD_HIDERESPLABEL, c.FLD_SORTORDER , c.FLD_RESPALIGN ,");
		sql.append(" e.FLDRESP_DATAVAL, e.FLDRESP_DISPVAL, e.FLDRESP_ISDEFAULT, e.FLDRESP_OFFLINE, e.FLDRESP_SCORE, e.FLDRESP_SEQ, e.RECORD_TYPE,");
		sql.append(" f.FLDVALIDATE_OP1, f.FLDVALIDATE_VAL1, f.FLDVALIDATE_LOGOP1, f.FLDVALIDATE_OP2, f.FLDVALIDATE_VAL2"); 
		sql.append(" from er_formsec a, er_formfld b, er_fldlib c, er_formlib d, ER_FLDRESP e, ER_FLDVALIDATE f, er_repformfld g");
		sql.append(" where a.PK_FORMSEC = g.FK_FORMSEC");
		sql.append(" and a.FK_FORMLIB = d.PK_FORMLIB");
		sql.append(" and g.FK_FIELD = c.PK_FIELD");
		sql.append(" and f.FK_FLDLIB(+) = c.PK_FIELD	");
		sql.append(" and g.FK_FORMFLD = b.PK_FORMFLD ");
		sql.append(" and e.FK_FIELD(+) = c.PK_FIELD");
		sql.append(" and c.RECORD_TYPE <> 'D'");
		sql.append(" and d.PK_FORMLIB = ?");
		//sql.append(" order by pk_field, pk_formsec");
		sql.append(" order by pk_formsec, pk_field");
		
		Connection conn = null; 
		PreparedStatement stmt = null; 
		ResultSet rs = null; 
		
		try
		{
			conn = getConnection(); 
			logger.debug(sql.toString()); 			
			stmt = conn.prepareStatement(sql.toString()); 
			stmt.setInt(1, formPK); 
			stmt.setInt(2, formPK); 
			rs = stmt.executeQuery(); 
			int i = 0; 
			String sectionName = ""; 
			String fieldID = ""; 
			FormSection section = null; 
			FormSections sections = new FormSections(); 
			FormFields fields = null; 
			FormField field = null; 
			while(rs.next())
			{
				if(i == 0)
				{
					formDesign.setFormName(rs.getString("FORM_NAME"));
					formDesign.setFormDesc(rs.getString("FORM_DESC"));					
					formDesign.setFormSharedWith(EnumUtil.getEnumType(SharedWith.class,rs.getString("FORM_SHAREDWITH")));
					
					formDesign.setFormLinkedTo(EnumUtil.getEnumType(LinkedTo.class, rs.getString("FORM_LINKTO"))); 
					if( rs.getString("FORM_ESIGNREQ") != null )
					{
						formDesign.setESignRequired(rs.getString("FORM_ESIGNREQ").equals("1")); 
					}else
					{
						formDesign.setESignRequired(false);
					}
					FormIdentifier formIdentifier = new FormIdentifier(); 
					Code formlibStatus = null; 
					if(formDesign.getFormLinkedTo().equals(LinkedTo.LIBRARY))
					{
						formlibStatus = CodeCache.getInstance().getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_FORM_STATUS, 
							rs.getString("FORM_STATUS"),
							EJBUtil.stringToNum(callingUser.getUserAccountId()));
					}else{
						formlibStatus = CodeCache.getInstance().getCodeSubTypeByPK(
								CodeCache.CODE_TYPE_LINKED_FORM_STATUS, 
								rs.getString("FORM_STATUS"),
								EJBUtil.stringToNum(callingUser.getUserAccountId()));
					}
					formDesign.setFormStatus(formlibStatus); 
			
					ObjectMap objectMap  = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_FORM_LIBRARY, formPK);
					formIdentifier.setOID(objectMap.getOID());
					formIdentifier.setPK(formPK);
					formDesign.setFormIdentifier(formIdentifier); 
				
					//		formDesign.setFormNext(formNext)
					formDesign.setSections(sections); 

				}

				if(!(sectionName.equals(rs.getString("formsec_name"))))
				{

					sectionName = rs.getString("formsec_name");	
					section = new FormSection(); 
					section.setSecSequence(rs.getString("formsec_seq"));
					section.setSecName(sectionName); 
					section.setOffline(rs.getInt("FORMSEC_OFFLINE") ==1);
					FormSectionIdentifier sectionIdentifier = new FormSectionIdentifier();
					ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_FORM_SECTION, rs.getInt("PK_FORMSEC")); 
					sectionIdentifier.setOID(map.getOID());
					sectionIdentifier.setPK(rs.getInt("PK_FORMSEC")); 
					section.setSectionIdentifier(sectionIdentifier); 
					section.setTabular(rs.getString("FORMSEC_FMT").equals("T")); 
					section.setRepeated(rs.getInt("FORMSEC_REPNO")); 
					fields = new FormFields(); 
					section.setFields(fields); 
					sections.addSection(section); 

				}
				
				String dataType = rs.getString("fld_datatype") ==null? "":rs.getString("fld_datatype") ; 
				
				if( !dataType.equals(DataType.MULTIPLE_CHOICE_LOOKUP.toString())) //not supporting lookup types. 
				{
					if(!fieldID.equals(rs.getString("pk_field")))  //change comparision with field ID. FieldName can have repeated fields
					
					{
						fieldID = rs.getString("pk_field"); 
						field = new FormField(); 
						field.setName(rs.getString("fld_name")); 
						field.setDesc(rs.getString("FLD_DESC")); 
						field.setFieldType(rs.getString("fld_type")); 
						field.setUniqueID(rs.getString("FLD_UNIQUEID")); 
						field.setSystemID(rs.getString("FLD_SYSTEMID")); 
						//	field.setLength(rs.getInt("fld_length"));
						field.setDataType(rs.getString("fld_datatype"));
						field.setCharacteristics(rs.getString("FLD_ISVISIBLE"));
						field.setInstructions(rs.getString("FLD_INSTRUCTIONS")); 
						field.setSortOrder(rs.getString("FLD_SORTORDER")); 
						field.setReadOnly(rs.getInt("FLD_ISREADONLY")==1?true:false ); 
						field.setKeyword(rs.getString("FLD_KEYWORD"));
						field.setSequence(rs.getInt("formfld_seq")); 



						if(includeFormatting)
						{
							field.setSameline(rs.getInt("FLD_SAMELINE")== 1? true:false);					
							field.setFieldNameFormat(rs.getString("FLD_NAME_FORMATTED"));
							field.setAlign(EnumUtil.getEnumType(Alignment.class, rs.getString("FLD_ALIGN"))); 
							field.setBold(rs.getInt("FLD_BOLD") == 1?true:false); 
							field.setItalics(rs.getInt("FLD_ITALICS") == 1?true:false); 
							field.setHideLabel(rs.getInt("FLD_HIDELABEL") == 1?true:false); 
							field.setExpandLabel(rs.getInt("FLD_EXPLABEL") ==1?true:false); 
							field.setDisplayWidth(rs.getInt("FLD_DISPLAY_WIDTH")); 
						}

						//		field.setFormat(format); 

//						FormFieldIdentifier fieldIdentifier = new FormFieldIdentifier(); 
//						ObjectMap objectMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_FORM_FIELD, rs.getInt("PK_FORMFLD")) ;
//						fieldIdentifier.setOID(objectMap.getOID()); 
						
// Change to FieldIdentifier(PK of er_fldlib) to have different OID for repeated field
						
						FieldIdentifier  fieldIdentifier = new FieldIdentifier(); 
						ObjectMap objectMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_FIELD_LIBRARY, rs.getInt("pk_field")) ;
						fieldIdentifier.setOID(objectMap.getOID());
						fieldIdentifier.setPK(rs.getInt("pk_field"));

						field.setFieldIdentifier(fieldIdentifier); 


						if(field.getFieldType() == FieldType.MULTIPLE_CHOICE)
						{
							MultipleChoices choices = new MultipleChoices();
							choices.setHideResponses(rs.getInt("FLD_HIDERESPLABEL")==1? true:false); //FLD_HIDERESPLABEL
							if(rs.getString("FLD_SORTORDER") != null) 
							{
								choices.setAutoSequence(rs.getString("FLD_SORTORDER").equals("A")?true:false); //FLD_SORTORDER
							}
							if(rs.getString("FLD_RESPALIGN") != null)
							{
								choices.setTopAlignResponses(rs.getString("FLD_RESPALIGN").equals("top")?true:false); //FLD_RESPALIGN
							}
							MultipleChoice choice = new MultipleChoice(); 
							choice.setDataValue(rs.getString("FLDRESP_DATAVAL")); 
							if(rs.getInt("FLDRESP_ISDEFAULT")== 1)
							{
								choice.setDefault(true); 
							}					
							choice.setDisplayValue(rs.getString("FLDRESP_DISPVAL")); 
							choice.setScore(rs.getInt("FLDRESP_SCORE")); 
							choice.setSequence(rs.getInt("FLDRESP_SEQ"));
							if(rs.getString("RECORD_TYPE") != null && rs.getString("RECORD_TYPE").equals("H"))
							{
								choice.setHidden(true); 
							}else
							{
								choice.setHidden(false); 
							}
							choices.addChoice(choice); 

							field.setChoices(choices); 

						}
						//TODO write encoder
						//field.setFormat(rs.getString)					
						if(field.getFieldType() == FieldType.EDIT_BOX)
						{					

							if(field.getDataType() == DataType.EDIT_BOX_TEXT)
							{
								TextFieldValidations validations = new TextFieldValidations(); 
								validations.setMandatory(rs.getInt("formfld_mandatory")==1); 
								validations.setDefaultResponse(rs.getString("FLD_DEFRESP")); 
								validations.setEditBoxLines(rs.getInt("FLD_LINESNO")); 
								validations.setMaxCharAllowed(rs.getInt("FLD_CHARSNO")); 
								validations.setOverrideMandatory(rs.getInt("FLD_OVERRIDE_MANDATORY") == 1); 
								validations.setFieldLength(rs.getInt("FLD_LENGTH")); 
								//field.setValidations(validations); 
								field.setTextValidations(validations);
							}else if(field.getDataType() == DataType.EDIT_BOX_NUMBER)
							{
								NumberFieldValidations validations = new NumberFieldValidations(); 
								validations.setDefaultResponse(rs.getString("FLD_DEFRESP")) ; 
								validations.setEditBoxLines(rs.getInt("FLD_LINESNO")); 
								validations.setFieldLength(rs.getInt("FLD_LENGTH")); 
								validations.setFormat(rs.getString("fld_format")); 
								validations.setMandatory(rs.getInt("formfld_mandatory")==1); 
								validations.setMaxCharAllowed(rs.getInt("FLD_CHARSNO")); 
								validations.setOverrideFormat(rs.getInt("FLD_OVERRIDE_FORMAT") == 1); 
								validations.setOverrideMandatory(rs.getInt("FLD_OVERRIDE_MANDATORY") == 1); 
								validations.setOverrideRange(rs.getInt("FLD_OVERRIDE_RANGE") == 1); 
								NumberRange range = new NumberRange();
								range.setFirstOperator(rs.getString("FLDVALIDATE_OP1")); 
								range.setFirstValue(rs.getString("FLDVALIDATE_VAL1")); 
								range.setLogicOperator(rs.getString("FLDVALIDATE_LOGOP1")); 
								range.setSecondOperator(rs.getString("FLDVALIDATE_OP2")); 
								range.setSecondValue(rs.getString("FLDVALIDATE_VAL2")); 
								validations.setRange(range);
								//field.setValidations(validations); 
								field.setNumberValidations(validations);
							}else if(field.getDataType() == DataType.EDIT_BOX_DATE)
							{
								DateFieldValidations validations = new DateFieldValidations(); 
								String defaultDate = rs.getString("FLD_DEFRESP"); 
								if(defaultDate == null ){
									validations.setDefaultDateToCurrent(false); 
								}else
								{
									validations.setDefaultDateToCurrent(defaultDate.equalsIgnoreCase("[VELSYSTEMDATE]"));
								}
								validations.setFutureDateAllowed(rs.getInt("FLD_TODAYCHECK") == 0); 
								validations.setMandatory(rs.getInt("formfld_mandatory")==1); 
								validations.setOverrideMandatory(rs.getInt("FLD_OVERRIDE_MANDATORY") == 1); 
								validations.setOverrideDateValidation(rs.getInt("FLD_OVERRIDE_DATE") == 1);
								//field.setValidations(validations); 
								field.setDateValidations(validations);
							}
						}else if(field.getFieldType() == FieldType.MULTIPLE_CHOICE)
						{
							FieldValidations validations = new FieldValidations();
							validations.setMandatory(rs.getInt("formfld_mandatory")==1); 
							validations.setOverrideMandatory(rs.getInt("FLD_OVERRIDE_MANDATORY") == 1); 
							field.setValidations(validations);
						}


						fields.addField(field);			
					}else if(field.getFieldType() == FieldType.MULTIPLE_CHOICE)// set multiple choices
					{
						MultipleChoice choice = new MultipleChoice(); 
						choice.setDataValue(rs.getString("FLDRESP_DATAVAL")); 
						if(rs.getString("FLDRESP_ISDEFAULT").equals("1"))
						{
							choice.setDefault(true); 
						}					
						choice.setDisplayValue(rs.getString("FLDRESP_DISPVAL")); 
						choice.setScore(rs.getInt("FLDRESP_SCORE")); 
						choice.setSequence(rs.getInt("FLDRESP_SEQ")); 
						if(rs.getString("RECORD_TYPE") != null && rs.getString("RECORD_TYPE").equals("H"))
						{
							choice.setHidden(true); 
						}else
						{
							choice.setHidden(false); 
						}

						field.getChoices().addChoice(choice); 				
					}	
				}
				i++; 
			}


		}catch(SQLException sqe)
		{
			sqe.printStackTrace(); 
			throw new OperationException(); 

		}finally{
			if(rs != null)
			{
				try
				{
					rs.close(); 
				}catch(SQLException sqe)
				{}
			}

			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException sqe){}
			}

			if(conn != null)
			{
				returnConnection(conn); 
			} 
		}

		
		
		
		return formDesign; 
	}
	
	
	public LinkedFormDesign getAccountPatientFormDesign(Integer formPK, FormType formType, UserBean callingUser, ObjectMapService objectMapService, LinkedFormDesign linkedFormDesign, boolean includeFormatting) throws OperationException
	{
		
		StringBuffer sql = new StringBuffer(); 
		sql.append("select  LF_DATACNT, LF_DISPLAYTYPE, LF_ENTRYCHAR, FK_ACCOUNT, FK_STUDY, LF_LNKFROM, LF_ISIRB from er_linkedforms where FK_FORMLIB = ?");
		sql.append(" and trim(LF_DISPLAYTYPE) = ?"); 
		
		Connection conn = null; 
		PreparedStatement stmt = null; 
		ResultSet rs = null ;
		
		try
		{
			conn = getConnection(); 
			stmt = conn.prepareStatement(sql.toString());
			stmt.setInt(1, formPK);
			String s = formType.getValue(); 
			stmt.setString(2, s); 
			
			rs = stmt.executeQuery(); 
			
			
			if(rs.next())
			{
				linkedFormDesign.setDataCount(rs.getInt("LF_DATACNT"));
				linkedFormDesign.setDisplayType(EnumUtil.getEnumType(DisplayType.class, rs.getString("LF_DISPLAYTYPE"))); 
				linkedFormDesign.setEntrySetting(EnumUtil.getEnumType(EntrySettings.class,rs.getString("LF_ENTRYCHAR"))); 
				linkedFormDesign.setIRBForm(rs.getInt("LF_ISIRB") == 1? true:false); 
//				linkedFormDesign.setSubmissionType(rs.getString("LF_SUBMISSION_TYPE"));
				if(rs.getString("FK_STUDY") != null && rs.getString("FK_STUDY").length() > 0)
				{
					StudyIdentifier studyIdentifier = new StudyIdentifier(); 
					ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, rs.getInt("FK_STUDY")); 
					studyIdentifier.setOID(map.getOID());
					studyIdentifier.setPK( rs.getInt("FK_STUDY"));
					if(linkedFormDesign instanceof StudyPatientFormDesign)
					{
						((StudyPatientFormDesign) linkedFormDesign).setStudyIdentifier(studyIdentifier); 
					}
					
					if(linkedFormDesign instanceof StudyFormDesign)
					{
						((StudyFormDesign) linkedFormDesign).setStudyIdentifier(studyIdentifier); 
					}
				}
				getLibraryFormDesign(formPK, includeFormatting, callingUser, objectMapService, linkedFormDesign); 
			}
			else
			{
				throw new OperationException();  
			} 
						
			
		}catch(SQLException sqe)
		{
			logger.error(sqe); 
			
		}finally
		{
			if(rs != null)
			{
				try
				{
					rs.close(); 
				}catch(SQLException sqe)
				{}
			}
			
			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException sqe){}
			}
			
			if(conn != null)
			{
				returnConnection(conn); 
			} 
		}
		
		
		return linkedFormDesign; 
	}
	
	public StudyFormDesign getStudyFormDesign(Integer formPK, UserBean callingUser, ObjectMapService objectMapService, 
			Integer studyPK, boolean includeFormatting) throws OperationException
	{
		StudyFormDesign linkedFormDesign = new StudyFormDesign();
		StringBuffer sql = new StringBuffer(); 
		sql.append("select  a.LF_DATACNT, a.LF_DISPLAYTYPE, a.LF_ENTRYCHAR, a.FK_ACCOUNT, a.FK_STUDY, a.LF_LNKFROM, a.LF_ISIRB, b.STUDY_NUMBER from er_linkedforms a, er_study b where a.FK_FORMLIB = ? and a.FK_STUDY = b.PK_STUDY(+)");
	//	sql.append(" and ((LF_DISPLAYTYPE = 'SP' and FK_STUDY =?) OR (LF_DISPLAYTYPE = 'PS') OR (LF_DISPLAYTYPE = 'PR'))"); 
		
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try
		{
			conn = getConnection(); 
			stmt = conn.prepareStatement(sql.toString());
			stmt.setInt(1, formPK);
		//	stmt.setInt(2, studyPK); 
			
			rs = stmt.executeQuery(); 
			if(rs.next())
			{
				linkedFormDesign.setDataCount(rs.getInt("LF_DATACNT"));
				linkedFormDesign.setDisplayType(EnumUtil.getEnumType(DisplayType.class, rs.getString("LF_DISPLAYTYPE"))); 
				linkedFormDesign.setEntrySetting(EnumUtil.getEnumType(EntrySettings.class,rs.getString("LF_ENTRYCHAR"))); 
				linkedFormDesign.setIRBForm(rs.getInt("LF_ISIRB") == 1); 
//				linkedFormDesign.setSubmissionType(rs.getString("LF_SUBMISSION_TYPE"));
				if(rs.getString("FK_STUDY") != null && rs.getString("FK_STUDY").length() > 0)
				{
					StudyIdentifier studyIdentifier = new StudyIdentifier(); 
					ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, rs.getInt("FK_STUDY")); 
					studyIdentifier.setOID(map.getOID()); 
					studyIdentifier.setPK(rs.getInt("FK_STUDY"));
					studyIdentifier.setStudyNumber(rs.getString("STUDY_NUMBER")); 
					((StudyFormDesign) linkedFormDesign).setStudyIdentifier(studyIdentifier); 
				}
				getLibraryFormDesign(formPK, includeFormatting, callingUser, objectMapService, linkedFormDesign); 
			}
			else
			{
				throw new OperationException();
			} 
		} catch(SQLException e) {
			logger.error(e); 
		} finally {
			if(rs != null) {
				try { rs.close();  } catch (SQLException e) {}
			}
			if(stmt != null) {
				try{ stmt.close();  }catch(SQLException sqe){}
			}
			if(conn != null) {
				returnConnection(conn); 
			}
		}
		return linkedFormDesign; 
	}
	
	public StudyFormDesign getStudyPatientFormDesign(Integer formPK, UserBean callingUser, ObjectMapService objectMapService, StudyFormDesign linkedFormDesign, Integer studyPK, boolean includeFormatting)
	throws OperationException
	{
		
		StringBuffer sql = new StringBuffer(); 
		sql.append("select  a.LF_DATACNT, a.LF_DISPLAYTYPE, a.LF_ENTRYCHAR, a.FK_ACCOUNT, a.FK_STUDY, a.LF_LNKFROM, a.LF_ISIRB, b.STUDY_NUMBER from er_linkedforms a, er_study b where a.FK_FORMLIB = ? and a.FK_STUDY = b.PK_STUDY(+)");
	//	sql.append(" and ((LF_DISPLAYTYPE = 'SP' and FK_STUDY =?) OR (LF_DISPLAYTYPE = 'PS') OR (LF_DISPLAYTYPE = 'PR'))"); 
		
		Connection conn = null; 
		PreparedStatement stmt = null; 
		ResultSet rs = null ;
		
		try
		{
			conn = getConnection(); 
			stmt = conn.prepareStatement(sql.toString());
			stmt.setInt(1, formPK);
		//	stmt.setInt(2, studyPK); 
			
			rs = stmt.executeQuery(); 
			
			
			if(rs.next())
			{
				linkedFormDesign.setDataCount(rs.getInt("LF_DATACNT"));
				linkedFormDesign.setDisplayType(EnumUtil.getEnumType(DisplayType.class, rs.getString("LF_DISPLAYTYPE"))); 
				linkedFormDesign.setEntrySetting(EnumUtil.getEnumType(EntrySettings.class,rs.getString("LF_ENTRYCHAR"))); 
				linkedFormDesign.setIRBForm(rs.getInt("LF_ISIRB") == 1? true:false); 
//				linkedFormDesign.setSubmissionType(rs.getString("LF_SUBMISSION_TYPE"));
				if(rs.getString("FK_STUDY") != null && rs.getString("FK_STUDY").length() > 0)
				{
					StudyIdentifier studyIdentifier = new StudyIdentifier(); 
					ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, rs.getInt("FK_STUDY")); 
					studyIdentifier.setOID(map.getOID()); 
					studyIdentifier.setPK(rs.getInt("FK_STUDY"));
					studyIdentifier.setStudyNumber(rs.getString("STUDY_NUMBER")); 
					((StudyFormDesign) linkedFormDesign).setStudyIdentifier(studyIdentifier); 
				
				
				}
				getLibraryFormDesign(formPK, includeFormatting, callingUser, objectMapService, linkedFormDesign); 
			}
			else
			{
				throw new OperationException();  
			} 
						
			
		}catch(SQLException sqe)
		{
			logger.error(sqe); 
			
		}finally
		{
			if(rs != null)
			{
				try
				{
					rs.close(); 
				}catch(SQLException sqe)
				{}
			}
			
			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException sqe){}
			}
			
			if(conn != null)
			{
				returnConnection(conn); 
			} 
		}
		
		
		return linkedFormDesign; 
	}	
	/**
	 * @return
	 */
	private MultipleChoices getMultipleChoicesForField(String fieldPK) {
		PreparedStatement stmt = null ; 
		ResultSet rs = null; 
		Connection conn = null; 
		MultipleChoices choices = new MultipleChoices(); 
		
		try
		{
			String sql = "select PK_FLDRESP, FK_FIELD, FLDRESP_SEQ, FLDRESP_DISPVAL, FLDRESP_DATAVAL, FLDRESP_SCORE,FLDRESP_ISDEFAULT, RECORD_TYPE,FLDRESP_OFFLINE from ER_FLDRESP where FK_FIELD = ?"; 
			
			conn = getConnection(); 
			stmt = conn.prepareStatement(sql); 
			stmt.setString(1,fieldPK); 
			
			
			
			
			while(rs.next()){
				MultipleChoice choice = new MultipleChoice(); 
				choice.setDataValue(rs.getString("FLDRESP_DATAVAL")); 
				if(rs.getString("FLDRESP_ISDEFAULT").equals("1"))
				{
					choice.setDefault(true); 
				}
				
				choice.setDisplayValue(rs.getString("FLDRESP_DISPVAL")); 
				choice.setScore(rs.getInt("FLDRESP_SCORE")); 
				choice.setSequence(rs.getInt("FLDRESP_SEQ")); 
				
				choices.addChoice(choice); 				
			}
		}catch(SQLException sqe)
		{
			
		}finally
		{
			if(rs != null)
			{
				try
				{
					rs.close(); 
				}catch(SQLException sqe)
				{}
			}
			
			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException sqe){}
			}
			
			if(conn != null)
			{
				returnConnection(conn); 
			}
			
		}
		
		
		return choices;
	}

	public  Integer getFormPK(String formName, FormType formType, Integer studyID, String accountPK) throws MultipleObjectsFoundException
	{
		PreparedStatement stmt = null; 
		ResultSet rs = null; 
		Connection conn = null ; 
		Integer formPK = null; 
		
		try{
			
			String sql = ""; 
			if(formType.equals(FormType.LIBRARY))
			{
				sql =  "select pk_formlib from er_formlib where form_name = ? and form_linkto = 'L' and fk_account = ?"; 
			}else if(formType.equals(FormType.STUDYPATIENT))
			{
				sql = "select a.FK_FORMLIB as FK_FORMLIB FROM ER_LINKEDFORMS  a, ER_FORMLIB  b" +
						" WHERE b.FORM_NAME = ? and b.FK_ACCOUNT = ?  AND a.FK_FORMLIB = PK_FORMLIB" +
						" AND a.FK_FORMLIB = PK_FORMLIB " +
						" AND ( (LF_DISPLAYTYPE = 'SP' and FK_STUDY =?)" +
						" OR (LF_DISPLAYTYPE = 'PS') OR (LF_DISPLAYTYPE = 'PR'))" +
						" and  a.record_type <> 'D'"; 
			}	
			else if(formType.equals(FormType.PATIENT))
			{
				// sql = "select pk_formlib from er_formlib where form_name = ? and form_linkto = 'A' and fk_account = ?	";
				
				sql = "select a.FK_FORMLIB as FK_FORMLIB FROM ER_LINKEDFORMS  a, ER_FORMLIB  b" +
						" WHERE b.FORM_NAME = ? and b.FK_ACCOUNT = ?  AND a.FK_FORMLIB = PK_FORMLIB" +
						" AND a.FK_FORMLIB = PK_FORMLIB " +
						" AND LF_DISPLAYTYPE = 'PA' " +
						" and  a.record_type <> 'D'";
			}
			else if(formType.equals(FormType.STUDY)) 
			{
				sql = "select a.FK_FORMLIB as FK_FORMLIB FROM ER_LINKEDFORMS  a, ER_FORMLIB  b" +
						" WHERE b.FORM_NAME = ? and b.FK_ACCOUNT = ?  AND a.FK_FORMLIB = PK_FORMLIB" +
						" AND a.FK_FORMLIB = PK_FORMLIB " +
						" AND ( (LF_DISPLAYTYPE = 'S' and FK_STUDY =?)" +
						" OR LF_DISPLAYTYPE = 'SA')" +
						" and  a.record_type <> 'D'"; 
			
			}else
			{
				return null; 
			}
			
			conn = getConnection(); 			

			stmt = conn.prepareStatement(sql); 
			
			stmt.setString(1, formName); 
			stmt.setString(2, accountPK);
			if(studyID != null && studyID > 0) stmt.setInt(3, studyID); 
			
			rs = stmt.executeQuery(); 
			int ctr = 0; 
			if(rs.next())
			{
				formPK = rs.getInt(1);
				ctr++; 
			}
			
			//if(ctr > 1) throw new MultipleObjectsFoundException(); 
			
		}catch(SQLException sqe)
		{
			sqe.printStackTrace(); 
		}finally
		{
			if(rs != null)
			{
				try
				{
					rs.close(); 
				}catch(SQLException sqe)
				{}
			}
			
			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException sqe){}
			}
			
			if(conn != null)
			{
				returnConnection(conn); 
			}
			
		}
		
		return formPK; 
	}

	
	
	
	
	public List<Integer> getLibraryFormListUserHasAccess(Integer userID)
	{
		PreparedStatement stmt = null ; 
		ResultSet rs = null;
		Connection conn = null;
		List<Integer> formList = new ArrayList<Integer>();  
		try
		{
			String sql = " select distinct pk_formlib from er_formlib a, er_objectshare b " +
			" where b.fk_object = a.pk_formlib and b.object_number = ? and a.form_linkto = ? and a.record_type <> ? " +
			" and ((b.objectshare_type =? and b.fk_objectshare_id = ?)" +
			" or  (b.OBJECTSHARE_TYPE =? and b.fk_objectshare_id in(select fk_site from er_usersite where fk_user = ? and usersite_right > ?)))"; 
			conn = getConnection(); 
			stmt = conn.prepareStatement(sql); 
			stmt.setInt(1, 1);  //OrderNumber = 1 for Forms 
			stmt.setString(2, "L"); 
			stmt.setString(3, "D"); 
			stmt.setString(4, "U"); 
			stmt.setInt(5, userID); 
			stmt.setString(6, "O");
			stmt.setInt(7, userID); 
			stmt.setInt(8, 0); 
			
			rs = stmt.executeQuery(); 
			
			while(rs.next())
			{
				formList.add(rs.getInt(1)); 
			}
		}catch(Exception sqe)
		{
			sqe.printStackTrace(); 
		}finally
		{
			if(rs != null)
			{
				try
				{
					rs.close(); 
				}catch(SQLException sqe)
				{}
			}
			
			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException sqe){}
			}
			
			if(conn != null)
			{
				returnConnection(conn); 
			}
			
		}
		
		return formList; 
	}
	
	public List<Integer> getStudyPatientFormListUserHasAccess(UserBean callingUser, Integer studyID, boolean isCalledFromFormResponse)
	{
		PreparedStatement stmt = null; 
		Connection conn = null; 
		ResultSet rs = null; 
		List<Integer> studyPatientFormList = new ArrayList<Integer>(); 
		
		try
		{
			StringBuffer sql = new StringBuffer( "select c.FK_FORMLIB as FK_FORMLIB FROM ER_LINKEDFORMS  a, ER_FORMLIB  b,er_formstat c") ; 
			sql.append(" WHERE b.FK_ACCOUNT = ?  AND a.FK_FORMLIB = PK_FORMLIB AND c.FK_FORMLIB = PK_FORMLIB");  
			sql.append(" AND (");
			if(studyID != null) sql.append("(a.LF_DISPLAYTYPE = 'SP' and a.FK_STUDY =? ) OR "); 
			sql.append("(a.LF_DISPLAYTYPE = 'PS') OR (a.LF_DISPLAYTYPE = 'PR')) ");
			sql.append(" and  a.record_type <> 'D' and nvl(a.lf_hide,0)=0 and c.formstat_enddate IS NULL");
			//Bug Fix 10454  - commenting the check to return only list of Active and Lockdown formDesigns only
//			if(isCalledFromFormResponse)
//			{
				sql.append(" and ((c.fk_codelst_stat = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat')) or"); 
				sql.append(" (c.fk_codelst_stat = (select pk_codelst from er_codelst where codelst_subtyp='L'  and codelst_type='frmstat')) )"); 
//			}
			sql.append(" and a.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=? and usersite_right <> 0)) and ");
			sql.append(" a.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user=? ))");
			if(studyID != null) sql.append("  and a.pk_lf not in (select pkg_util.f_to_number(settings_value) from er_settings where settings_modname='3' AND settings_modnum=? AND settings_keyword='FORM_HIDE')");
			sql.append(" union");  
			sql.append(" select c.FK_FORMLIB as FK_FORMLIB FROM ER_LINKEDFORMS  a, ER_FORMLIB  b,er_formstat c"); 
			sql.append(" WHERE b.FK_ACCOUNT = ?  AND a.FK_FORMLIB = PK_FORMLIB  AND c.FK_FORMLIB = PK_FORMLIB");  
			sql.append(" AND (");
			if(studyID != null) sql.append("(a.LF_DISPLAYTYPE = 'SP' and a.FK_STUDY =? ) OR "); 
			sql.append("(a.LF_DISPLAYTYPE = 'PS') OR (a.LF_DISPLAYTYPE = 'PR')) ");
			sql.append(" and  a.record_type <> 'D' and nvl(a.lf_hide,0)=0 and c.formstat_enddate IS NULL"); 
			//Bug Fix 10454  - commenting the check to return only list of Active and Lockdown formDesigns only
//			if(isCalledFromFormResponse)
//			{
				sql.append(" and ((c.fk_codelst_stat = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat')) or");
				sql.append(" (c.fk_codelst_stat = (select pk_codelst from er_codelst where codelst_subtyp='L'  and codelst_type='frmstat')) )");
//			}
			sql.append(" and (a.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user=? ))");
			sql.append(" or a.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user=? and usersite_right <> 0)))"); 
			if(studyID != null) sql.append(" and  a.pk_lf not in (select pkg_util.f_to_number(settings_value) from er_settings where settings_modname='3' AND settings_modnum=? AND settings_keyword='FORM_HIDE')"); 
			 
			
			conn = getConnection(); 
			stmt = conn.prepareStatement(sql.toString()); 
			int j = 1; 
			stmt.setString(j++, callingUser.getUserAccountId()); 
			if(studyID != null) stmt.setInt(j++, studyID); 
			stmt.setInt(j++, callingUser.getUserId()); 
			stmt.setInt(j++, callingUser.getUserId()); 
			if(studyID != null) stmt.setInt(j++, studyID);
	        stmt.setString(j++, callingUser.getUserAccountId());
	        if(studyID != null) stmt.setInt(j++, studyID);
	        stmt.setInt(j++, callingUser.getUserId());
	        stmt.setInt(j++, callingUser.getUserId());
	        if(studyID != null) stmt.setInt(j++, studyID);
	        
	        rs = stmt.executeQuery(); 
	    	while(rs.next())
			{
				studyPatientFormList.add(rs.getInt(1)); 
			}
		}catch(SQLException sqe)
		{
			logger.error(sqe); 
		}
		finally
		{
			if(rs != null)
			{
				try
				{
					rs.close(); 
				}catch(SQLException sqe)
				{}
			}
			
			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException sqe){}
			}
			
			if(conn != null)
			{
				returnConnection(conn); 
			}			
		}
		
		return studyPatientFormList; 
		
	}
	
	public boolean isFormLinkedToAnyEvent(Integer formPK)
	{
		
		String sql = "select count(fk_form) from esch.sch_event_crf where fk_form = ?"; 
		Connection conn = null; 
		ResultSet rs = null; 
		PreparedStatement stmt = null; 
		
		try{
			
			conn = getConnection(); 
			
			stmt = conn.prepareStatement(sql); 
			
			stmt.setInt(1, formPK); 
			
			rs = stmt.executeQuery(); 
			
			while(rs.next())
			{
				if(rs.getInt(1) > 0) return true; 
				
			}
			
		}catch(SQLException sqe)
		{
			logger.error(sqe); 
		}
		finally
		{
			if(rs != null)
			{
				try
				{
					rs.close(); 
				}catch(SQLException sqe)
				{}
			}
			
			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException sqe){}
			}
			
			if(conn != null)
			{
				returnConnection(conn); 
			}			
		}
		
		
		
		return false; 
	}

	public LinkedFormDesign getPatientFormDesign(Integer formPK,boolean includeFormatting,FormType formType, UserBean callingUser, ObjectMapService objectMapService, LinkedFormDesign linkedFormDesign) throws OperationException
	{
		StringBuffer sql = new StringBuffer(); 
		sql.append("select  a.LF_DATACNT, a.LF_DISPLAYTYPE, a.LF_ENTRYCHAR, a.FK_ACCOUNT, a.LF_LNKFROM, a.LF_ISIRB from er_linkedforms a where a.FK_FORMLIB = ? ");
	//	sql.append(" and ((LF_DISPLAYTYPE = 'SP' and FK_STUDY =?) OR (LF_DISPLAYTYPE = 'PS') OR (LF_DISPLAYTYPE = 'PR'))"); 
	//	sql.append(" and LF_DISPLAYTYPE = 'PA' ");
		Connection conn = null; 
		PreparedStatement stmt = null; 
		ResultSet rs = null ;
		
		try
		{
			conn = getConnection(); 
			stmt = conn.prepareStatement(sql.toString());
			stmt.setInt(1, formPK);
		//	stmt.setInt(2, studyPK); 
			
			rs = stmt.executeQuery(); 
			
			
			if(rs.next())
			{
				linkedFormDesign.setDataCount(rs.getInt("LF_DATACNT"));
				linkedFormDesign.setDisplayType(EnumUtil.getEnumType(DisplayType.class, rs.getString("LF_DISPLAYTYPE"))); 
				linkedFormDesign.setEntrySetting(EnumUtil.getEnumType(EntrySettings.class,rs.getString("LF_ENTRYCHAR"))); 
				linkedFormDesign.setIRBForm(rs.getInt("LF_ISIRB") == 1? true:false); 
				
				getLibraryFormDesign(formPK, includeFormatting, callingUser, objectMapService, linkedFormDesign); 
			}
			else
			{
				//throw new OperationException();  
				return linkedFormDesign =null; 
			} 
								
			
		}catch(SQLException sqe)
		{
			logger.error(sqe); 
			
		}finally
		{
			if(rs != null)
			{
				try
				{
					rs.close(); 
				}catch(SQLException sqe)
				{}
			}
			
			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException sqe){}
			}
			
			if(conn != null)
			{
				returnConnection(conn); 
			} 
		}
		
		
		return linkedFormDesign; 
	}
	 
	/*public LinkedFormDesign getPatientFormDesign(Integer formPK, FormType formType, UserBean callingUser, ObjectMapService objectMapService, LinkedFormDesign linkedFormDesign) throws OperationException
	{
		StringBuffer sql = new StringBuffer(); 
		sql.append("select  a.LF_DATACNT, a.LF_DISPLAYTYPE, a.LF_ENTRYCHAR, a.FK_ACCOUNT, a.LF_LNKFROM, a.LF_ISIRB from er_linkedforms a where a.FK_FORMLIB = ? ");
	//	sql.append(" and ((LF_DISPLAYTYPE = 'SP' and FK_STUDY =?) OR (LF_DISPLAYTYPE = 'PS') OR (LF_DISPLAYTYPE = 'PR'))"); 
	//	sql.append(" and LF_DISPLAYTYPE = 'PA' ");
		Connection conn = null; 
		PreparedStatement stmt = null; 
		ResultSet rs = null ;
		
		try
		{
			conn = getConnection(); 
			stmt = conn.prepareStatement(sql.toString());
			stmt.setInt(1, formPK);
		//	stmt.setInt(2, studyPK); 
			
			rs = stmt.executeQuery(); 
			
			
			if(rs.next())
			{
				linkedFormDesign.setDataCount(rs.getInt("LF_DATACNT"));
				linkedFormDesign.setDisplayType(EnumUtil.getEnumType(DisplayType.class, rs.getString("LF_DISPLAYTYPE"))); 
				linkedFormDesign.setEntrySetting(EnumUtil.getEnumType(EntrySettings.class,rs.getString("LF_ENTRYCHAR"))); 
				linkedFormDesign.setIRBForm(rs.getInt("LF_ISIRB") == 1? true:false); 
				
				getLibraryFormDesign(formPK, false, callingUser, objectMapService, linkedFormDesign); 
			}
			else
			{
				throw new OperationException();  
			} 
						
			
		}catch(SQLException sqe)
		{
			logger.error(sqe); 
			
		}finally
		{
			if(rs != null)
			{
				try
				{
					rs.close(); 
				}catch(SQLException sqe)
				{}
			}
			
			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException sqe){}
			}
			
			if(conn != null)
			{
				returnConnection(conn); 
			} 
		}
		
		
		return linkedFormDesign; 
	
	}

	*/	
	
	
	
	 private int cRows;
	
	public void setCRows(int cRows) {
        this.cRows = cRows;
    }
	
	
	public FormList getListOfStudyPatientForms(int accountId, int patId, int studyId,int userId,Map<String, Object> parameters,int limit,boolean formHasResponses)
    {
       
       StringBuffer sqlBuf = new StringBuffer();
       PreparedStatement pstmt = null;
       Connection conn = null;
       sqlBuf.append("Select PK_FORMLIB,b.FORM_STATUS,FORM_NAME,FORM_DESC,LOWER (FORM_NAME) FRMNAME,");
	   sqlBuf.append(" (select count(*) from er_patforms pf where pf.fk_patprot in ( select pk_patprot from er_patprot pp where pp.fk_per=? and pp.fk_study=?) and pf.FK_FORMLIB=a.FK_FORMLIB and pf.RECORD_TYPE <> 'D') SAVEDCOUNT ");
	   sqlBuf.append(" FROM ER_LINKEDFORMS a, ER_FORMLIB b, er_formstat c");
	   sqlBuf.append(" WHERE b.FK_ACCOUNT = ? AND a.FK_FORMLIB = PK_FORMLIB AND c.FK_FORMLIB = PK_FORMLIB");
	   sqlBuf.append("  AND ((LF_DISPLAYTYPE = 'SP' AND FK_STUDY = ?)OR (LF_DISPLAYTYPE = 'PS')OR (LF_DISPLAYTYPE = 'PR'))");
	   sqlBuf.append(" AND a.record_type <> 'D'AND NVL (a.lf_hide, 0) = 0 AND c.formstat_enddate IS NULL");
	   sqlBuf.append("  AND ( (c.fk_codelst_stat =(SELECT pk_codelst FROM er_codelst WHERE codelst_subtyp = 'A' AND codelst_type = 'frmstat'))");
	   sqlBuf.append(" OR (c.fk_codelst_stat =(SELECT pk_codelst FROM er_codelst WHERE codelst_subtyp = 'L' AND codelst_type = 'frmstat')))");
	   sqlBuf.append(" AND a.fk_formlib NOT IN(SELECT fk_formlib FROM er_formorgacc WHERE fk_site NOT IN(SELECT fk_site FROM er_usersite WHERE fk_user = ? AND usersite_right <> 0))");
	   sqlBuf.append(" AND a.fk_formlib NOT IN (SELECT fk_formlib FROM er_formgrpacc WHERE fk_group NOT IN (SELECT fk_grp FROM er_usrgrp WHERE fk_user = ?))");
	   sqlBuf.append(" UNION");
	   sqlBuf.append(" SELECT PK_FORMLIB,b.FORM_STATUS,FORM_NAME,FORM_DESC,LOWER (FORM_NAME) FRMNAME,");
	   sqlBuf.append(" (select count(*) from er_patforms pf where pf.fk_patprot in ( select pk_patprot from er_patprot pp where pp.fk_per=? and pp.fk_study=?) and pf.FK_FORMLIB=a.FK_FORMLIB) SAVEDCOUNT ");
	   sqlBuf.append(" FROM ER_LINKEDFORMS a, ER_FORMLIB b, er_formstat c");
	   sqlBuf.append(" WHERE b.FK_ACCOUNT = ? AND a.FK_FORMLIB = PK_FORMLIB AND c.FK_FORMLIB = PK_FORMLIB");
	   sqlBuf.append(" AND ((LF_DISPLAYTYPE = 'SP' AND FK_STUDY = ?)OR (LF_DISPLAYTYPE = 'PS')OR (LF_DISPLAYTYPE = 'PR'))");
	   sqlBuf.append(" AND a.record_type <> 'D'AND NVL (a.lf_hide, 0) = 0 AND c.formstat_enddate IS NULL");
	   sqlBuf.append(" AND ( (c.fk_codelst_stat =(SELECT pk_codelst FROM er_codelst WHERE codelst_subtyp = 'A' AND codelst_type = 'frmstat'))");
	   sqlBuf.append(" AND (a.fk_formlib IN (SELECT fk_formlib FROM er_formgrpacc WHERE fk_group IN (SELECT fk_grp FROM er_usrgrp WHERE fk_user = ?))");
	   sqlBuf.append(" OR a.fk_formlib IN(SELECT fk_formlib FROM er_formorgacc WHERE fk_site IN (SELECT fk_site FROM er_usersite WHERE fk_user = ? AND usersite_right <> 0))))");
	   sqlBuf.append(" ORDER BY  FRMNAME ASC");
	   if(formHasResponses)
	   {
		   StringBuffer sqlBuf2=new StringBuffer();
		   sqlBuf2.append("select * from (");
		   sqlBuf2.append(sqlBuf.toString());
		   sqlBuf2.append(") where savedcount>0");
		   sqlBuf=sqlBuf2;
	   }
	   StudyPatientForms spf= new StudyPatientForms();
	   FormList formList=new FormList();
	   try {
		   		conn = getConnection();
		   		Rlog.debug("formDesignDao", "LinkedFormsDao.getListOfStudyPatientForms SQL :  "+sqlBuf.toString());   
		   		pstmt = conn.prepareStatement(sqlBuf.toString());
		   		pstmt.setInt(1, patId);
		   		pstmt.setInt(2, studyId);
		   		pstmt.setInt(3, accountId);
	            pstmt.setInt(4, studyId);
	            pstmt.setInt(5, userId);
	            pstmt.setInt(6, userId);
	            pstmt.setInt(7, patId);
	            pstmt.setInt(8, studyId);
	            pstmt.setInt(9, accountId);
	            pstmt.setInt(10, studyId);
	            pstmt.setInt(11, userId);
	            pstmt.setInt(12, userId);
	            FormInfo formInfo=null;
	            FormIdentifier formIdentifier=null;
	            Code codeFormStatus=null;
	            CodeCache codeCache = CodeCache.getInstance(); 
	            ResultSet rs = pstmt.executeQuery();
	            int rows = 0,currentRow=0;
	            while (rs.next())
	            {
	            	if(limit>currentRow)
	            	{
	            		formInfo= new FormInfo();
	            		formInfo.setFormName(rs.getString("FORM_NAME"));
	            		formInfo.setFormDesc(rs.getString("FORM_DESC"));
		                ObjectMap formObjectMap = ((ObjectMapService)parameters.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_FORM_LIBRARY, rs.getInt("PK_FORMLIB"));
		    			formIdentifier=new FormIdentifier();
		    			formIdentifier.setOID(formObjectMap.getOID());
		    			formIdentifier.setPK(formObjectMap.getTablePK());
		    			formInfo.setFormIdentifier(formIdentifier);
		                codeFormStatus=codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_LINKED_FORM_STATUS, rs.getString("form_status"),StringUtil.stringToNum(((UserBean)parameters.get("callingUser")).getUserAccountId())  );
		                formInfo.setFormStatus(codeFormStatus);
		                formInfo.setDataCount(rs.getInt("SAVEDCOUNT"));
		                currentRow++;
		                formList.addFormInfo(formInfo);
	            	}
	            	rows++;
	            }
	            Rlog.debug("formDesignDao", "LinkedFormsDao.getListOfStudyPatientForms rows :  "+ rows);
	            setCRows(rows);
	            formList.setFormCount(rows);
	            return formList;
	        }
	   catch (SQLException ex)
	    {
		   Rlog.fatal("formDesignDao","LinkedFormsDao.getListOfStudyPatientForms EXCEPTION" + ex);
	    } finally {
	            try {
	                if (pstmt != null)
	                    pstmt.close();
	            } catch (Exception e) {
	            }
	            try {
	                if (conn != null)
	                    conn.close();
	            } catch (Exception e) {
	            }
	
	    }
		return formList;
	}
	public FormList getListOfPatientForms(int accountId, int patId,int userId,Map<String, Object> parameters,int pageNumber,int pageSize,boolean formHasResponses,boolean ignoreDisplayInPatFlag)
    {
		PreparedStatement pstmt = null;
	    Connection conn = null;
	    StringBuffer sqlBuf=new StringBuffer();
		sqlBuf.append(" select a.PK_LF as PK_LF ,c.FK_FORMLIB as FK_FORMLIB , FORM_NAME, FORM_DESC , nvl(LF_DISPLAY_INPAT,0) LF_DISPLAY_INPAT , " +
				"CASE WHEN (nvl(LF_DISPLAY_INPAT,0)=1) " +
				"THEN  (select count(*) from ER_PATFORMS pf where pf.Fk_formlib = a.FK_FORMLIB and pf.Fk_per = ? and pf.RECORD_TYPE <> 'D' AND FK_PATPROT IS NULL)  " +
				"ELSE  (select count(*) from ER_PATFORMS pf where pf.Fk_formlib = a.FK_FORMLIB and pf.Fk_per = ? and pf.RECORD_TYPE <> 'D')  END AS SAVEDCOUNT  " +
				",b.FORM_STATUS as form_status");
    	sqlBuf.append(" FROM ER_LINKEDFORMS  a, ER_FORMLIB  b,er_formstat c ");
    	sqlBuf.append(" WHERE b.FK_ACCOUNT = ?  AND a.FK_FORMLIB = PK_FORMLIB   ");
    	sqlBuf.append(" AND c.FK_FORMLIB = PK_FORMLIB   AND (LF_DISPLAYTYPE = 'PA' or (LF_DISPLAYTYPE = 'PS' and nvl(LF_DISPLAY_INPAT,0)=1) OR (LF_DISPLAYTYPE = 'PR' and nvl(LF_DISPLAY_INPAT,0)=1))    and  a.record_type <> 'D' and nvl(a.lf_hide,0)=0 and c.formstat_enddate IS NULL  ");
    	sqlBuf.append(" and ((c.fk_codelst_stat = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat')) or ");
    	sqlBuf.append(" (c.fk_codelst_stat = (select pk_codelst from er_codelst where codelst_subtyp='L'  and codelst_type='frmstat')) ) ");
    	sqlBuf .append(" and a.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=? and usersite_right <> 0)) and ");
    	sqlBuf .append(" a.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user=? ))");
    	sqlBuf.append(" union  ");
    	sqlBuf.append(" select a.PK_LF as PK_LF ,c.FK_FORMLIB as FK_FORMLIB , FORM_NAME, FORM_DESC , nvl(LF_DISPLAY_INPAT,0) LF_DISPLAY_INPAT , " +
    			"CASE WHEN (nvl(LF_DISPLAY_INPAT,0)=1) " +
    			"THEN  (select count(*) from ER_PATFORMS pf where pf.Fk_formlib = a.FK_FORMLIB and pf.Fk_per = ? and pf.RECORD_TYPE <> 'D' AND FK_PATPROT IS NULL)  " +
    			"ELSE  (select count(*) from ER_PATFORMS pf where pf.Fk_formlib = a.FK_FORMLIB and pf.Fk_per = ? and pf.RECORD_TYPE <> 'D')  END AS SAVEDCOUNT  " +
    			",b.FORM_STATUS as form_status");
    	sqlBuf.append(" FROM ER_LINKEDFORMS  a, ER_FORMLIB  b,er_formstat c  ");
    	sqlBuf.append(" WHERE b.FK_ACCOUNT = ?  AND a.FK_FORMLIB = PK_FORMLIB  ");
    	sqlBuf.append(" AND c.FK_FORMLIB = PK_FORMLIB   AND (LF_DISPLAYTYPE = 'PA' or (LF_DISPLAYTYPE = 'PS'and nvl(LF_DISPLAY_INPAT,0)=1) OR (LF_DISPLAYTYPE = 'PR' and nvl(LF_DISPLAY_INPAT,0)=1)) and  a.record_type <> 'D' and nvl(a.lf_hide,0)=0 ");
    	sqlBuf.append(" and c.formstat_enddate IS NULL ");
    	sqlBuf.append(" and ((c.fk_codelst_stat = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat')) or ");
    	sqlBuf.append(" (c.fk_codelst_stat = (select pk_codelst from er_codelst where codelst_subtyp='L'  and codelst_type='frmstat')) ) ");
    	sqlBuf.append(" and (a.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user=? ))");
    	sqlBuf.append(" or a.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user=? and usersite_right <> 0))) ");
    	sqlBuf.append(" order by  FORM_NAME ");
   	 	StringBuffer sqlBuf1=new StringBuffer();
    	 if (ignoreDisplayInPatFlag) {
   		   sqlBuf1.append("select * from (");
   		   sqlBuf1.append(sqlBuf.toString());
   		   sqlBuf1.append(") where LF_DISPLAY_INPAT <> 1");
   		   sqlBuf=sqlBuf1;
    	 }
    	 StringBuffer sqlBuf2=new StringBuffer();
    	 if(formHasResponses)
    	 {
 		  
 		   sqlBuf2.append("select * from (");
 		   sqlBuf2.append(sqlBuf.toString());
 		   sqlBuf2.append(") where savedcount>0");
 		   sqlBuf=sqlBuf2;
 		 }
    	int uperLimit=pageSize*pageNumber;
    	int lowerLimit=(uperLimit-pageSize)+1;
    	/*sqlBuf2=null;sqlBuf2=new StringBuffer();
    	sqlBuf2.append(" select * from (select x.*, rownum row_num from (" +
    			sqlBuf.toString() +
    			") x ) where row_num between "+lowerLimit+" and " + uperLimit);*/
    	FormList formList=new FormList();
    	try {
	   		conn = getConnection();
	   		Rlog.debug("formDesignDao", "LinkedFormsDao.getListOfPatientForms sql :  "+ sqlBuf.toString());
	        pstmt = conn.prepareStatement(sqlBuf.toString());
		   	 pstmt.setInt(1, patId);
	         pstmt.setInt(2, patId);
	         pstmt.setInt(3, accountId);
	         pstmt.setInt(4, userId);
	         pstmt.setInt(5, userId);
	         pstmt.setInt(6, patId);
	         pstmt.setInt(7, patId);
	         pstmt.setInt(8, accountId);
	         pstmt.setInt(9, userId);
	         pstmt.setInt(10, userId);
	         FormInfo formInfo=null;
            FormIdentifier formIdentifier=null;
            Code codeFormStatus=null;
            CodeCache codeCache = CodeCache.getInstance(); 
            ResultSet rs = pstmt.executeQuery();
            int rows = 1;
            while (rs.next())
            {
            	if(lowerLimit<=rows && rows<=uperLimit)
            	{
            		formInfo= new FormInfo();
            		formInfo.setFormName(rs.getString("FORM_NAME"));
            		formInfo.setFormDesc(rs.getString("FORM_DESC"));
	                ObjectMap formObjectMap = ((ObjectMapService)parameters.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_FORM_LIBRARY, rs.getInt("FK_FORMLIB"));
	    			formIdentifier=new FormIdentifier();
	    			formIdentifier.setOID(formObjectMap.getOID());
	    			formIdentifier.setPK(formObjectMap.getTablePK());
	    			formInfo.setFormIdentifier(formIdentifier);
	                codeFormStatus=codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_LINKED_FORM_STATUS, rs.getString("form_status"),StringUtil.stringToNum(((UserBean)parameters.get("callingUser")).getUserAccountId())  );
	                formInfo.setFormStatus(codeFormStatus);
	                formInfo.setDataCount(rs.getInt("SAVEDCOUNT"));
	                formList.addFormInfo(formInfo);
            	}
            	rows++;
            }
            Rlog.debug("formDesignDao", "LinkedFormsDao.getListOfPatientForms rows :  "+ rows);
            setCRows(rows);
            formList.setFormCount(rows-1);
            return formList;
        }
   catch (SQLException ex)
    {
	   Rlog.fatal("formDesignDao","LinkedFormsDao.getListOfPatientForms EXCEPTION" + ex);
    } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

    }
		
		
		
		return null;
    
    }
	
}
