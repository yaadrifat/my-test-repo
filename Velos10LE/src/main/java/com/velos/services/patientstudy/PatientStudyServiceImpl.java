package com.velos.services.patientstudy;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import com.velos.eres.business.patProt.impl.PatProtBean;
import com.velos.eres.business.person.impl.PersonBean;
import com.velos.eres.business.study.impl.StudyBean;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.patProtAgent.PatProtAgentRObj;
import com.velos.eres.service.personAgent.PersonAgentRObj;
import com.velos.eres.service.studyAgent.StudyAgent;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.userSiteAgent.UserSiteAgentRObj;
import com.velos.eres.service.util.StringUtil;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.Issues;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.authorization.TeamAuthModule;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientProtocolIdentifier;
import com.velos.services.model.PatientStudy;
import com.velos.services.model.PatientStudyId;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.util.ObjectLocator;
/**
 * Session Bean with implementations related to PatientStudies
 * @author Virendra
 *
 */
@Stateless
@Remote(PatientStudyService.class)
public class PatientStudyServiceImpl 
extends AbstractService 
implements PatientStudyService{

@EJB
GrpRightsAgentRObj groupRightsAgent;
@EJB
private UserAgentRObj userAgent;
@Resource 
private SessionContext sessionContext;
@EJB
private StudyAgent studyAgent;
@EJB
private ObjectMapService objectMapService;
@EJB
private PatProtAgentRObj patProtAgent;
@EJB
private PersonAgentRObj patientAgent;
Integer personPk = 0;
@EJB
private UserSiteAgentRObj userSiteAgent;

	
	private static Logger logger = Logger.getLogger(PatientStudyServiceImpl.class.getName());
	/**
	 * returns list of patientStudies with param as PatientIdentifier.
	 */
	public List<PatientStudy> getPatientStudies(PatientIdentifier patientId)
			throws OperationException {
		
		try{
			
			if((patientId==null || (StringUtil.isEmpty(patientId.getOID())
					&& 	(StringUtil.isEmpty(patientId.getPatientId()) 
							|| (patientId.getOrganizationId()==null 
								|| (StringUtil.isEmpty(patientId.getOrganizationId().getOID())
									&& StringUtil.isEmpty(patientId.getOrganizationId().getSiteName())
									&& StringUtil.isEmpty(patientId.getOrganizationId().getSiteAltId())
									&& (patientId.getOrganizationId().getPK()==null || patientId.getOrganizationId().getPK()==0))))
					&&(patientId.getPK()==null || patientId.getPK()==0))))
			{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid patientIdentifier is required to addStudyPatientStatus"));
			throw new OperationException();
			}
			
			try{
			//PersonPk from ObjectLocator
			personPk = ObjectLocator.personPKFromPatientIdentifier(
					callingUser, 
					patientId, 
					objectMapService);
			
			} catch (MultipleObjectsFoundException e) {
				addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, 
						"Multiple Patients found")); 
				throw new OperationException(); 
			}
			
			if(null==personPk || personPk ==0){
				addIssue(new Issue(
							IssueTypes.PATIENT_NOT_FOUND, 
							"Patient not found"));
				throw new OperationException();
			}
		
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);
//			Integer manageProtocolPriv = 
//				groupAuth.getAppManageProtocolsPrivileges();
//			
//			boolean hasViewManageProt = 
//				GroupAuthModule.hasViewPermission(manageProtocolPriv);
//			
//			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
//			if (!hasViewManageProt){
//				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view Patient data"));
//				throw new AuthorizationException("User Not Authorized to view Patient data");
//			}
			
			Integer managePatientsPriv = 
				groupAuth.getAppManagePatientsPrivileges();
			
			boolean hasViewManageProt = 
				GroupAuthModule.hasViewPermission(managePatientsPriv);
			
			if (logger.isDebugEnabled()) logger.debug("user manage patient priv: " + managePatientsPriv);
			if (!hasViewManageProt){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view Patient data"));
				throw new AuthorizationException("User Not Authorized to view Patient data");
			}
			
	        // #6242: Check patient facility rights; this is the max rights among all facilities of this patient
            Integer patFacilityRights = userSiteAgent.getUserPatientFacilityRight(callingUser.getUserId(), personPk);
            if (!GroupAuthModule.hasViewPermission(patFacilityRights)) {
                if (logger.isDebugEnabled()) logger.debug("User not authorized to access data of this patient");
                addIssue(
                        new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION, 
                                "User not authorized to access data of this patient"));
                throw new AuthorizationException("User not authorized to access data of this patient");
            }
            
			ArrayList<Integer> lstPatProtPk = ObjectLocator.getPatProtPkFromPersonPk(personPk, objectMapService);
			ArrayList<PatientStudy> lstPatientStudy = new ArrayList<PatientStudy>();
			for(Integer patProtPk: lstPatProtPk){
				PatProtBean patProtBean = patProtAgent.getPatProtDetails(patProtPk);
				Integer studyPk =  StringUtil.stringToInteger(patProtBean.getPatProtStudyId());
				PatientStudy patientStudy = null;
				try {
					patientStudy = getPatientStudy(studyPk);
				} catch(Exception e) {
					logger.warn("Exception for studyPK="+studyPk+" "+e);
					continue;
				}
				patientStudy.setPatientStudyId(patProtBean.getPatStudyId());
				lstPatientStudy.add(patientStudy);
			}
			if (lstPatientStudy.isEmpty()){
				Issue issue = new Issue(IssueTypes.STUDIES_FOR_PATIENT_NOT_FOUND, "Studies for patient not found");
				addIssue(issue);
				logger.error(issue);
				throw new OperationException();
			}
			return lstPatientStudy;
			
		}
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", t);
			throw new OperationException(t);
		}
	}
	//
	private PatientStudy getPatientStudy(Integer studyPk) throws AuthorizationException{
		
		PatientStudy patStudy = new PatientStudy();
		StudyIdentifier studyIdentifier = new StudyIdentifier();
		
		StudyBean studyBean = studyAgent.getStudyDetails(studyPk);
		String studyNumber = studyBean.getStudyNumber();
		ObjectMap studyObjectMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPk);
		TeamAuthModule teamAuthModule = new TeamAuthModule(callingUser.getUserId(), studyPk);
		
		int studyTeamPrivileges = teamAuthModule.getStudyTeamPrivileges();
		
		if (!TeamAuthModule.hasViewPermission(studyTeamPrivileges)){
			if (logger.isDebugEnabled()) logger.debug("user does not have view permission to view patient study");
			
			addIssue(
					new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, 
							"User does not have view permission to view patient study"));
			throw new AuthorizationException("User does not have view permission to view patient Study");
		}
		int patientManagePrivileges = teamAuthModule.getPatientManagePrivileges();
		//Virendra:#6156, for phi rights(patient privileges for logged user)
		if(!TeamAuthModule.hasViewPermission(patientManagePrivileges)){
			studyIdentifier.setStudyNumber("*");
			studyIdentifier.setOID("*");
		}
		else{
			studyIdentifier.setStudyNumber(studyNumber);
			studyIdentifier.setOID(studyObjectMap.getOID());
			studyIdentifier.setPK(studyPk);
		}
		patStudy.setStudyIdentifier(studyIdentifier);
		
		patStudy.setIsFdaRegulated(studyBean.fdaRegulatedStudy == null ? false : 
			(studyBean.fdaRegulatedStudy == 1 ? true : false));
	return 	patStudy;
	}	

	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		response = new ResponseHolder();
		callingUser = 
			getLoggedInUser(
					sessionContext,
					userAgent);
		return ctx.proceed();

	}
	
	@Override
	public PatientStudyId getPatientStudyId(PatientIdentifier patientId,
			StudyIdentifier studyId) throws OperationException {
		Issues issues = new Issues();
		PatientStudyId patientStudyId = new PatientStudyId();
		try {
			// Do basic input validation
			this.validateForGetPatientStudyId(patientId, studyId);
			
			// Start another try-catch just to catch MultipleObjectsFoundException
			try{
				personPk = ObjectLocator.personPKFromPatientIdentifier(
						callingUser, patientId, objectMapService);
			} catch (MultipleObjectsFoundException e) {
				issues.add(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, "Multiple Patients found")); 
				throw new OperationException(issues); 
			}
			if(null==personPk || personPk ==0){
				issues.add(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Patient not found"));
				throw new OperationException(issues);
			}
			
			// Continue with the rest of authorization checks
			GroupAuthModule groupAuth = new GroupAuthModule(callingUser, groupRightsAgent);
			boolean hasViewManageProt = 
					GroupAuthModule.hasViewPermission(groupAuth.getAppManagePatientsPrivileges());
			if (!hasViewManageProt){
				issues.add(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view Patient data"));
				throw new OperationException("User Not Authorized to view Patient data");
			}
            Integer patFacilityRights = userSiteAgent.getUserPatientFacilityRight(callingUser.getUserId(), personPk);
            if (!GroupAuthModule.hasViewPermission(patFacilityRights)) {
                issues.add(new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION, 
                		"User not authorized to access data of this patient"));
                throw new OperationException("User not authorized to access data of this patient");
            }
            
			Integer studyPKFromReq = locateStudyPK(studyId);
			if(studyPKFromReq == null || studyPKFromReq == 0){
				issues.add(new Issue(IssueTypes.STUDY_NOT_FOUND, 
						"Study not found for StudyIdentifier OID:" + studyId.getOID() + 
						" StudyNumber " + studyId.getStudyNumber()));
				throw new OperationException(issues);
			}
			
			// Main loop to get the Patient Study ID
			boolean patientIsOnStudy = false;
			ArrayList<Integer> lstPatProtPk = ObjectLocator.getPatProtPkFromPersonPk(personPk, objectMapService);
			for(Integer patProtPk: lstPatProtPk){
				PatProtBean patProtBean = patProtAgent.getPatProtDetails(patProtPk);
				Integer studyPk =  StringUtil.stringToInteger(patProtBean.getPatProtStudyId());
				if (studyPk == null || studyPk == 0) { continue; }
				if (studyPk.compareTo(studyPKFromReq) != 0) { continue; }
				getPatientStudy(studyPk); // check study access here
				patientIsOnStudy = true;
				patientStudyId.setPatientStudyId(patProtBean.getPatStudyId());
				break;
			}
			
			if (!patientIsOnStudy){
				issues.add(new Issue(IssueTypes.STUDIES_FOR_PATIENT_NOT_FOUND, "Patient is not on this study"));
				throw new OperationException(issues);
			}
		} catch(AuthorizationException e) {
			issues.add(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, e.getMessage()));
			throw new OperationException(issues);
		} catch(OperationException e) {
			throw e;
		} catch(Throwable t) {
			this.addUnknownThrowableIssue(t);
			t.printStackTrace();
			throw new OperationException(t);
		}
		return patientStudyId;
	}
	
	private void validateForGetPatientStudyId(PatientIdentifier patientId,
			StudyIdentifier studyId) throws OperationException {
		Issues issues = new Issues();
		try {
			if (patientId==null) {
				issues.add(new Issue(IssueTypes.DATA_VALIDATION, "Valid patientIdentifier is required for getPatientStudyId"));
				throw new OperationException(issues);
			}
			if (StringUtil.isEmpty(patientId.getOID()) 
					&& (patientId.getPK() == null || patientId.getPK() < 1)) {
				if (patientId.getOrganizationId() == null) {
					issues.add(new Issue(IssueTypes.DATA_VALIDATION, 
							"Valid organizationIdentifier in patientIdentifier is required for getPatientStudyId"));
					throw new OperationException(issues);
				}
				if (StringUtil.isEmpty(patientId.getOrganizationId().getOID())
						&& (patientId.getOrganizationId().getPK() == null || patientId.getOrganizationId().getPK() < 1)
						&& StringUtil.isEmpty(patientId.getOrganizationId().getSiteName())
						&& StringUtil.isEmpty(patientId.getOrganizationId().getSiteAltId())
						) {
					issues.add(new Issue(IssueTypes.DATA_VALIDATION, 
							"Valid organizationIdentifier in patientIdentifier is required for getPatientStudyId"));
					throw new OperationException(issues);
				}
				if (StringUtil.isEmpty(patientId.getPatientId())) {
					issues.add(new Issue(IssueTypes.DATA_VALIDATION, "Valid patientIdentifier is required for getPatientStudyId"));
					throw new OperationException(issues);
				}
			}
			
			if(studyId ==null || (StringUtil.isEmpty(studyId.getOID()) 
					&& (studyId.getPK()==null || studyId.getPK()<=0) 
					&& StringUtil.isEmpty(studyId.getStudyNumber()))) {
				issues.add(new Issue(IssueTypes.DATA_VALIDATION, "Valid studyIdentifier is required for getPatientStudyId"));
				throw new OperationException(issues);
			}
			
		} catch(OperationException e) {
			throw e;
		} catch(Throwable t) {
			this.addUnknownThrowableIssue(t);
			t.printStackTrace();
			throw new OperationException(t);
		}
	}
	
	private Integer locateStudyPK(StudyIdentifier studyIdentifier) 
			throws OperationException {
		Issues issues = new Issues();
		Integer studyPK = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier,objectMapService);
		if (studyPK == null || studyPK == 0){
			StringBuffer errorMessage = new StringBuffer("Study not found for:"); 
			if(studyIdentifier.getOID() != null && studyIdentifier.getOID().length() > 0) {
				errorMessage.append(" OID: " + studyIdentifier.getOID()); 
			}
			if(studyIdentifier.getStudyNumber() != null && studyIdentifier.getStudyNumber().length() > 0) {
				errorMessage.append( " Study Number: " + studyIdentifier.getStudyNumber()); 
			}
			issues.add(new Issue(IssueTypes.STUDY_NOT_FOUND, errorMessage.toString()));
			throw new OperationException(issues);
		}
		return studyPK;
	}

	public PatientProtocolIdentifier getPatientStudyInfo(Integer patProtId) throws OperationException {
		PatientProtocolIdentifier patProtIdentifier = new PatientProtocolIdentifier();
		Issues issues = new Issues();
		PatientStudyId patientStudyId = new PatientStudyId();
		try {
			// Main loop to get the Patient Study ID
			boolean patientIsOnStudy = false;
			ArrayList<Integer> lstPatProtPk = ObjectLocator.getPatientStudyPKsFromPatProt(patProtId, objectMapService);
			
			Integer studyPk = null;
			
			for(Integer patProtPk: lstPatProtPk){
				PatProtBean patProtBean = patProtAgent.getPatProtDetails(patProtPk);
				
				studyPk = StringUtil.stringToInteger(patProtBean.getPatProtStudyId());
				if (studyPk == null || studyPk == 0) { continue; }

				personPk = StringUtil.stringToInteger(patProtBean.getPatProtPersonId());
				if (personPk == null || personPk == 0) { continue; }
				
				getPatientStudy(studyPk); // check study access here
				patientIsOnStudy = true;
				patientStudyId.setPatientStudyId(patProtBean.getPatStudyId());
				break;
			}
			
			if (!patientIsOnStudy){
				issues.add(new Issue(IssueTypes.STUDIES_FOR_PATIENT_NOT_FOUND, "Patient is not on this study"));
				throw new OperationException(issues);
			}

			// Do basic input validation
			if(studyPk == null || studyPk == 0){
				issues.add(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found"));
				throw new OperationException(issues);
			}

			if(null==personPk || personPk ==0){
				issues.add(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Patient not found"));
				throw new OperationException(issues);
			}
			
			// Continue with the rest of authorization checks
			GroupAuthModule groupAuth = new GroupAuthModule(callingUser, groupRightsAgent);
			boolean hasViewManageProt = GroupAuthModule.hasViewPermission(groupAuth.getAppManagePatientsPrivileges());
			if (!hasViewManageProt){
				issues.add(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view Patient data"));
				throw new OperationException("User Not Authorized to view Patient data");
			}
            Integer patFacilityRights = userSiteAgent.getUserPatientFacilityRight(callingUser.getUserId(), personPk);
            if (!GroupAuthModule.hasViewPermission(patFacilityRights)) {
                issues.add(new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION, 
                		"User not authorized to access data of this patient"));
                throw new OperationException("User not authorized to access data of this patient");
            }
            
            //Set PK as patProtId
            patProtIdentifier.setPK(patProtId);
            
            // Set StudyIdentifier
			StudyBean studyBean = studyAgent.getStudyDetails(studyPk);
            StudyIdentifier studyIdentifier = new StudyIdentifier();
            studyIdentifier.setPK(studyPk);
            studyIdentifier.setStudyNumber(studyBean.getStudyNumber());
            patProtIdentifier.setStudyIdentifier(studyIdentifier);

            //Set PatientIdentifier
			PersonBean patientBean = patientAgent.getPersonDetails(personPk);
            PatientIdentifier patientIdentifier = new PatientIdentifier();
            patientIdentifier.setPK(personPk);
            patientIdentifier.setPatientId(patientBean.getPersonPId());
            patProtIdentifier.setPatientIdentifier(patientIdentifier);
		} catch(AuthorizationException e) {
			issues.add(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, e.getMessage()));
			throw new OperationException(issues);
		} catch(OperationException e) {
			throw e;
		} catch(Throwable t) {
			this.addUnknownThrowableIssue(t);
			t.printStackTrace();
			throw new OperationException(t);
		}
		return patProtIdentifier;
	}
}