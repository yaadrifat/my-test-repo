package com.velos.services.mashup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;

import static javax.ejb.TransactionAttributeType.REQUIRED;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.LinkedFormsDao;
import com.velos.eres.service.accountAgent.AccountAgentRObj;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.linkedFormsAgent.LinkedFormsAgentRObj;
import com.velos.eres.service.patFacilityAgent.PatFacilityAgentRObj;
import com.velos.eres.service.patProtAgent.PatProtAgentRObj;
import com.velos.eres.service.personAgent.PersonAgentRObj;
import com.velos.eres.service.studyAgent.StudyAgent;
import com.velos.eres.service.studySiteAgent.StudySiteAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.userSiteAgent.UserSiteAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.services.AbstractService;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.authorization.TeamAuthModule;
import com.velos.services.form.FormDesignDAO;
import com.velos.services.form.FormDesignService;
import com.velos.services.formresponse.FormResponseService;
import com.velos.services.formresponse.FormResponseServiceDAO;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.FieldID;
import com.velos.services.model.FilterParams;
import com.velos.services.model.Modules;
import com.velos.services.model.Study;
import com.velos.services.model.StudyFilterFormResponse;
import com.velos.services.model.StudyFormDesign;
import com.velos.services.model.StudyFormResponse;
import com.velos.services.model.StudyFormResponseIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyMashup;
import com.velos.services.model.StudyMashupDetailsResponse;
import com.velos.services.model.LinkedFormDesign.DisplayType;
import com.velos.services.model.StudyStatus;
import com.velos.services.model.StudyStatuses;
import com.velos.services.model.StudySummary;
import com.velos.services.patientschedule.PatientScheduleDAO;
import com.velos.services.patientschedule.PatientScheduleServiceImpl;
import com.velos.services.study.StudyService;
import com.velos.services.study.StudyServiceImpl;
import com.velos.services.util.JNDINames;
import com.velos.services.util.ObjectLocator;
import com.velos.webservices.SimpleAuthCallbackHandler;


/**
 * @author Tarandeep Singh Bali
 *
 */
@Stateless
@Remote(StudyMashupService.class)
public class StudyMashupServiceImpl extends AbstractService 
implements StudyMashupService {
	
	@EJB
	private ObjectMapService objectMapService;

	@EJB
	GrpRightsAgentRObj groupRightsAgent;
	@EJB
	private UserAgentRObj userAgent;
	@EJB
	private StudySiteAgentRObj studySiteAgent;
	@EJB
	private UserSiteAgentRObj userSiteAgent;
	@EJB
	private PatFacilityAgentRObj patFacilityAgent;
	@EJB
	private PersonAgentRObj personAgentBean;
	@EJB
	private AccountAgentRObj accountAgent;
	@EJB
	private PatProtAgentRObj patProtAgent;
//	@EJB
//	LinkedFormsAgentRObj linkedFormAgent;
	@Resource 
	private SessionContext sessionContext;
	
	private static Logger logger = Logger.getLogger(StudyMashupServiceImpl.class.getName());
	
	public StudyMashup getStudyMashup(StudyIdentifier studyIdentifier) throws OperationException{
		
		StudyMashup studyMashup = new StudyMashup();
		
		try{
			
			Study study = new Study();
			StudyFormDesign studyFormDesign = new StudyFormDesign();
			//StudyService studyService;
			
			InitialContext ic = null;
			StudyService studyService = null;
			FormDesignService formDesign = null;
			
			try{
				ic = new InitialContext();
				studyService = (StudyService)ic.lookup(JNDINames.StudyServiceImpl);
			}catch(NamingException e){
				e.printStackTrace();
			}
			
			if(studyService != null){
				
				study = studyService.getStudy(studyIdentifier);
			}			

//            if(formDesign != null){
//            	studyFormDesign = formDesign.getStudyFormDesign(formIdentifier, studyIdentifier, formName, includeFormatting)
//            }
				
		
			
			studyMashup.setStudy(study);
			
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyMashupImpl create", e);
			e.setIssues(response.getIssues());
			e.printStackTrace();
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyMashupImpl create", t);
			throw new OperationException(t);
		}
		return studyMashup;
		
	}
	
	public StudyFormResponse getStudyFormResponse(Integer formPK) throws OperationException{
		
		StudyFormResponse studyFormResponse = new StudyFormResponse();
		Integer formResponsePK = 0;
		try{
			InitialContext ic = null;
			FormResponseService formResponseService = null;
			
			try{
				ic = new InitialContext();
				formResponseService = (FormResponseService)ic.lookup(JNDINames.FormResponseServiceImpl);
			}catch(NamingException e){
				e.printStackTrace();
			}
			
			formResponsePK = StudyMashupDAO.getStudyFormResponsePK(formPK);
			if(formResponsePK ==0 || formResponsePK == null){
				addIssue(new Issue(IssueTypes.FORM_RESPONSE_DOES_NOT_EXIST,				
						"Form Response does not exist")); 
					throw new OperationException("Form Response does not exist"); 
			}
			
			StudyFormResponseIdentifier formResponseIdentifier = new StudyFormResponseIdentifier();
			formResponseIdentifier.setPK(formResponsePK);
			
			if(formResponseService != null){
				studyFormResponse = formResponseService.getStudyFormResponse(formResponseIdentifier);  
			}
			
			
			
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyMashupImpl create", e);
			e.setIssues(response.getIssues());
			e.printStackTrace();
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyMashupImpl create", t);
			throw new OperationException(t);
		}
		return studyFormResponse;
	}
	
	public StudyFilterFormResponse getFilterStudyFormResponse(FilterParams filterParams) throws OperationException{
		
		StudyFilterFormResponse formResponse= new StudyFilterFormResponse();
		HashMap<String, Object> map = new HashMap<String, Object>();
		StudyFormResponse studyFormResponse = new StudyFormResponse();
		StudyFormDesign studyFormDesign = new StudyFormDesign();
		Integer formResponsePK = 0;
		Integer formPK = 0;
		try{
			
			InitialContext ic = null;
			InitialContext icDesign = null;
			FormResponseService formResponseService = null;
			FormDesignService formDesignService = null;
			
			try{
				ic = new InitialContext();
				formResponseService = (FormResponseService)ic.lookup(JNDINames.FormResponseServiceImpl);
			}catch(NamingException e){
				e.printStackTrace();
			}
			
			try{
				icDesign = new InitialContext();
				formDesignService = (FormDesignService)ic.lookup(JNDINames.FormDesignServiceImpl);
			}catch(NamingException e){
				e.printStackTrace();
			}
			
			formPK = filterParams.getFormPK();
			
			formResponsePK = StudyMashupDAO.getStudyFormResponsePK(formPK);
			if(formResponsePK ==0 || formResponsePK == null){
				addIssue(new Issue(IssueTypes.FORM_RESPONSE_DOES_NOT_EXIST,				
						"Form Response does not exist")); 
					throw new OperationException("Form Response does not exist"); 
			}
			
			StudyFormResponseIdentifier formResponseIdentifier = new StudyFormResponseIdentifier();
			formResponseIdentifier.setPK(formResponsePK);
			
			if(formDesignService != null){
				
			}
			
			if(formResponseService != null){
				
				studyFormResponse = formResponseService.getStudyFormResponse(formResponseIdentifier);
				if(filterParams.getFieldIDs() != null){
					Integer mapSize = map.size();
					try{
						if(filterParams.getFieldIDs().getFieldID().size() > 0){
							for(int i = 0; i < filterParams.getFieldIDs().getFieldID().size(); i++){
								for(int j = 0; j < studyFormResponse.getFormFieldResponses().getField().size(); j++){
									Integer fieldPK = StudyMashupDAO.getFieldPK(formPK, filterParams.getFieldIDs().getFieldID().get(i));
									Integer fieldResponsePK = studyFormResponse.getFormFieldResponses().getField().get(j).getFieldIdentifier().getPK();
									if(fieldPK.equals(fieldResponsePK)){
										map.put(filterParams.getFieldIDs().getFieldID().get(i), studyFormResponse.getFormFieldResponses().getField().get(j).getValue());
									}
								}
								if(mapSize == map.size()){
									Issue issue = new Issue();
									issue.setType(IssueTypes.INVALID_FILTER_FIELD_ID);
									issue.setMessage("Could not find any field with field ID: " + filterParams.getFieldIDs().getFieldID().get(i) + " Please enter a valid field ID to filter response");								
									map.put(filterParams.getFieldIDs().getFieldID().get(i), issue);
								}
								mapSize = map.size();
							}
						}
					}catch(NullPointerException e){
						addIssue(new Issue(IssueTypes.INVALID_FILTER_FIELD_ID,				
								"Please enter a valid field ID to filter response")); 
							throw new OperationException("Please enter a valid field ID to filter response"); 
					}


				}else{
					map.put("studyFormResponse", studyFormResponse);
				}

				
				
				formResponse.setResponeMap(map);
			}
			
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyMashupImpl create", e);
			e.setIssues(response.getIssues());
			e.printStackTrace();
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyMashupImpl create", t);
			throw new OperationException(t);
		}
		
		return formResponse;
		
	}
	
	
	public StudyMashupDetailsResponse getStudyMashupDetails(StudyIdentifier studyIdentifier, Integer formPK, Modules modules) throws OperationException{
		StudyMashupDetailsResponse studyMashupResponse = new StudyMashupDetailsResponse();
		Integer studyPK=0;
		HashMap responseMap = new HashMap();
		StudySummary studySummary = new StudySummary();
		StudyStatuses studyStatuses = new StudyStatuses();
		StudyFilterFormResponse studyFilterFormResponse = new StudyFilterFormResponse();
		try{
			
			if( studyIdentifier == null || (studyIdentifier.getPK() == null && (StringUtil.isEmpty(studyIdentifier.getOID()))
					&& (StringUtil.isEmpty(studyIdentifier.getStudyNumber()))))
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid StudyIdentifier is required to get Patient Schedule"));
				throw new OperationException();
			}

			studyPK =ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier,objectMapService);

			if (studyPK == null || studyPK==0 || !StudyMashupDAO.ifStudyExists(studyPK)){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Patient study not found"));
				throw new OperationException("Patient study not found");
			}

			if(formPK == 0 || formPK == null || !StudyMashupDAO.ifFormExists(formPK)){
				addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found"));
				throw new OperationException("Form not found");
			}
			
			InitialContext ic = null;
			InitialContext icDesign = null;
			StudyService studyService = null;
			FormDesignService formDesignService = null;
			
			try{
				ic = new InitialContext();
				studyService = (StudyService)ic.lookup(JNDINames.StudyServiceImpl);
			}catch(NamingException e){
				e.printStackTrace();
			}
			
			if(studyService != null){
				
				for(int i = 0; i < modules.getModule().size(); i++){
					if(modules.getModule().get(i).getModuleName().equals("studysummary")){
						studySummary = studyService.getStudySummary(studyIdentifier);
//						for(int j = 0; j < modules.getModule().get(i).getFieldID().getFieldID().size(); j++){
//							if(modules.getModule().get(i).getFieldID().getFieldID().get(i) == "division"){
//								//resp;
//							}
//						}
						responseMap.put("studySummary", studySummary);
					}
					if(modules.getModule().get(i).getModuleName().equals("studystatus")){
						studyStatuses = studyService.getStudyStatuses(studyIdentifier);
						responseMap.put("studyStatuses", studyStatuses);
					}
					if(modules.getModule().get(i).getModuleName().equals("studyformresponse")){
						FilterParams filterParams = new FilterParams();
						filterParams.setFormPK(formPK);
						FieldID fieldID = new FieldID();
						fieldID.setFieldID(modules.getModule().get(i).getFieldID().getFieldID());
						filterParams.setFieldIDs(fieldID);
						studyFilterFormResponse = getFilterStudyFormResponse(filterParams);
						responseMap.put("studyformresponse", studyFilterFormResponse);
					}
				}
			}
			studyMashupResponse.setResponeMap(responseMap);
		    
		   }catch(OperationException e){
				sessionContext.setRollbackOnly();
				if (logger.isDebugEnabled()) logger.debug("StudyMashupImpl create", e);
				e.setIssues(response.getIssues());
				e.printStackTrace();
				throw e;
			}
			catch(Throwable t){
				this.addUnknownThrowableIssue(t);
				if (logger.isDebugEnabled()) logger.debug("StudyMashupImpl create", t);
				throw new OperationException(t);
			}
		return studyMashupResponse;
		}
		
	
	
	
	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		response = new ResponseHolder();
		callingUser = 
			getLoggedInUser(
					sessionContext,
					userAgent);
		return ctx.proceed();

	}

}
