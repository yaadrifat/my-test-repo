package com.velos.services.mashup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.model.FilterParams;
import com.velos.services.model.Modules;
import com.velos.services.model.StudyFilterFormResponse;
import com.velos.services.model.StudyFormResponse;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyMashup;
import com.velos.services.model.StudyMashupDetailsResponse;


/**
 * Remote Interface declaring methods from service implementation 
 * @author Tarandeep Singh Bali
 *
 */

@Remote
public interface StudyMashupService {
	
	public StudyMashup getStudyMashup(StudyIdentifier studyIdentifier) throws OperationException;
	
	public StudyFormResponse getStudyFormResponse(Integer formPK) throws OperationException;
	
	public StudyFilterFormResponse getFilterStudyFormResponse(FilterParams forlFilterParams) throws OperationException;
	
	public StudyMashupDetailsResponse getStudyMashupDetails(StudyIdentifier studyIdentifier, Integer formPK, Modules modules) throws OperationException;
	

}
