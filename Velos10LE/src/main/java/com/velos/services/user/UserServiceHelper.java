package com.velos.services.user;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.SessionContext;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.log4j.Logger;

import com.velos.eres.business.address.impl.AddressBean;
import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.CtrlDao;
import com.velos.eres.business.common.SettingsDao;
import com.velos.eres.business.common.SiteDao;
import com.velos.eres.business.site.impl.SiteBean;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.business.usrGrp.impl.UsrGrpBean;
import com.velos.eres.service.addressAgent.AddressAgentRObj;
import com.velos.eres.service.commonAgent.CommonAgentRObj;
import com.velos.eres.service.siteAgent.SiteAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.usrGrpAgent.UsrGrpAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.Security;
import com.velos.eres.service.util.StringUtil;
import com.velos.services.AbstractService;
import com.velos.services.CodeNotFoundException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.ValidationException;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.NonSystemUser;
import com.velos.services.model.User;
import com.velos.services.patientdemographics.PatientDemographicsHelper;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ObjectLocator;

public class UserServiceHelper
{

	private static Logger logger = Logger.getLogger(PatientDemographicsHelper.class);

	public void validateNonSystemUser(NonSystemUser nonSystemUser,Map<String, Object> parameters) throws ValidationException
	{
		
		List<Issue> validationIssues = new ArrayList<Issue>();
		if(nonSystemUser.getFirstName()== null || nonSystemUser.getFirstName().trim().length() == 0)
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION,	"First Name may not be null"));
		}
		if(nonSystemUser.getLastName()== null || nonSystemUser.getLastName().trim().length() == 0)
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION,	"Last Name may not be null"));
		}
		//Bug#15358 Raman
		if(nonSystemUser.getOrganization() == null ||((nonSystemUser.getOrganization().getOID()== null || nonSystemUser.getOrganization().getOID().length() == 0 )
				&&(nonSystemUser.getOrganization().getSiteAltId() == null || nonSystemUser.getOrganization().getSiteAltId().length() == 0) 
			&& (nonSystemUser.getOrganization().getSiteName() == null || nonSystemUser.getOrganization().getSiteName().length() == 0)
			&& (nonSystemUser.getOrganization().getPK() == null || nonSystemUser.getOrganization().getPK() < 1)))
		{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"Organization may not be null")); 
		}
		
		if (validationIssues.size() > 0)
		{
			((ResponseHolder) parameters.get("ResponseHolder"))
			.getIssues().addAll(validationIssues); 
		if (logger.isDebugEnabled()) logger.debug("Validation Issues found for UserServiceImpl");
			throw new ValidationException();
		}
	}
	
	public int createNonSystemUser(NonSystemUser nonSystemUser,Map<String, Object> parameters) throws OperationException 
	{
		
		UserBean callingUser = (UserBean) parameters.get("callingUser");
		SiteAgentRObj siteAgent = (SiteAgentRObj) parameters.get("siteAgent");
		ObjectMapService objectMapService= (ObjectMapService) parameters.get("objectMapService");
		SessionContext sessionContext = (SessionContext) parameters.get("sessionContext"); 
		
		validateNonSystemUser(nonSystemUser,parameters);
		
		 String userAddId = null;
		 String userId = null;
		 
		AddressBean addressSKUser = new AddressBean();
		UserBean userSK = new UserBean();
				
		addressSKUser.setAddCity(nonSystemUser.getCity());
		addressSKUser.setAddPri(nonSystemUser.getAddress());
		addressSKUser.setAddState(nonSystemUser.getState());
		addressSKUser.setAddZip(nonSystemUser.getZip());
		addressSKUser.setAddCountry(nonSystemUser.getCountry());
		addressSKUser.setAddPhone(nonSystemUser.getPhone());
		
		if(nonSystemUser.getEmail() != null && nonSystemUser.getEmail().length() != 0)
		{	
			try 
			{
				String email = nonSystemUser.getEmail();
				InternetAddress emailAddr = new InternetAddress(email);
				emailAddr.validate();
				addressSKUser.setAddEmail(email);
			} 
			catch (AddressException ex)
			{
				((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
                           new Issue(
                           IssueTypes.DATA_VALIDATION, "Invalid Email Address : " + nonSystemUser.getEmail()));
                         throw new OperationException(); 
			}
		}
	
		userSK.setUserFirstName(nonSystemUser.getFirstName());
		userSK.setUserLastName(nonSystemUser.getLastName());
		
		if((nonSystemUser.getUserStatus() == null) || (nonSystemUser.getUserStatus().toString().length()==0))
		{	
			((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
                     new Issue(
                     IssueTypes.DATA_VALIDATION, "User Status field is required. " ));
                   throw new OperationException();
			    
		}
	
		if(nonSystemUser.getUserStatus().getLegacyString().equals("B"))
		{
			((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
                    new Issue(
                    IssueTypes.DATA_VALIDATION, "User Status can not be 'BLOCKED' " ));
                  throw new OperationException();
		}	
		userSK.setUserStatus(nonSystemUser.getUserStatus().getLegacyString());
		if(nonSystemUser.getJobType() != null && nonSystemUser.getJobType().getCode() != null)
		{
			try
			{	
				userSK.setUserCodelstJobtype(AbstractService.dereferenceCodeStr(nonSystemUser.getJobType(), CodeCache.CODE_TYPE_JOB_TYPE, callingUser));
			}
			catch(CodeNotFoundException e)
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(
							IssueTypes.DATA_VALIDATION, 
							"User Job Type Code Not Found: " ));
				throw new OperationException();
			}
			
		}	
				
		if(nonSystemUser.getPrimarySpecialty() != null && nonSystemUser.getPrimarySpecialty().getCode() != null)
		{
			try
			{			
				userSK.setUserCodelstSpl(AbstractService.dereferenceCodeStr(nonSystemUser.getPrimarySpecialty(), CodeCache.CODE_TYPE_PRIMARY_SPECIALTY, callingUser));
			}
			catch(CodeNotFoundException e)
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(
							IssueTypes.DATA_VALIDATION, 
							"Primary Speciality Code Not Found: " ));
				throw new OperationException();
			}
			
		}
		 userSK.setUserType(ObjectLocator.USER_TYPE_NON_SYSTEM);
		 userSK.setUserAccountId(callingUser.getUserAccountId());
		
	// --- checking site of User sending -----	 
		Integer sitePK = 0;
		try
		{
			 sitePK = ObjectLocator.sitePKFromIdentifier(callingUser, nonSystemUser.getOrganization(), sessionContext, objectMapService);
		}
		catch(MultipleObjectsFoundException moe)
		{
			((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "organizations not found for  OID:"
					+ nonSystemUser.getOrganization().getOID() + " siteAltID:" + nonSystemUser.getOrganization().getSiteAltId() + " siteName:" + nonSystemUser.getOrganization().getSiteName()));
			
			throw new OperationException();
		}
		if(sitePK==null || sitePK == 0)
		{
		((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "organizations  not found for  OID :"
					+ nonSystemUser.getOrganization().getOID() + " siteAltID:" + nonSystemUser.getOrganization().getSiteAltId() + " siteName:" + nonSystemUser.getOrganization().getSiteName()));
			
			throw new OperationException();
		}
		SiteBean sitebn = siteAgent.getSiteDetails(sitePK);
		if(sitebn == null)
		{
			((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "organizations  not found for sitePK :"
					+ nonSystemUser.getOrganization().getPK()));
			
			throw new OperationException();
		}	
			
//	 ---checking organization access rights of callingUser -----		
		SiteDao siteDao = new SiteDao();
		siteDao.getSitesByUser(callingUser.getUserId());
		boolean hasAccessright = false;
		for(int siteCount=0;siteCount<siteDao.getSiteIds().size();siteCount++)
		{
			if(sitePK.equals(siteDao.getSiteIds().get(siteCount)))
			{
				userSK.setUserSiteId((StringUtil.integerToString(sitePK)));
				hasAccessright=true;
				break;
			}
			
		}	
		if(!hasAccessright)
		{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.ORGANIZATION_AUTHORIZATION, "Calling User not authorised to add new user for site OID :"
						+ nonSystemUser.getOrganization().getOID() + " siteAltID:" + nonSystemUser.getOrganization().getSiteAltId() + " siteName:" + nonSystemUser.getOrganization().getSiteName()));
				throw new OperationException();
		}
		 try {
	            AddressAgentRObj addressUserAgentRObj = EJBUtil
	                    .getAddressAgentHome();
	            userAddId = String.valueOf(addressUserAgentRObj
	                    .setAddressDetails(addressSKUser));
	           	           
	            Rlog.debug("UserServiceHelper",
	                    "In UserServiceHelper.createUSer for user address");
	        } catch (Exception e) {
	            Rlog.fatal("accountWrapper",
	                    "Error in UserServiceHelper.createUser for user address "
	                            + e);
	            return -2;
	        }
		 userSK.setUserPerAddressId(userAddId);
		 try
		 {
	            UserAgentRObj userAgentRObj1 = EJBUtil.getUserAgentHome();
	            userId = String.valueOf(userAgentRObj1.setUserDetails(userSK));
	                 
	            Rlog.debug("UserServiceHelper",
	                    "In UserServiceHelper.createUser for user");
	     } catch (Exception e) {
	            Rlog
	                    .fatal("UserServiceHelper",
	                            "Error in UserServiceHelper.createUser for user "
	                                    + e);
	            return -2;
	        } 
		
			 		 
		return (StringUtil.stringToNum(userId));
	}

	public void validateUser(User user, Map<String, Object> parameters) throws ValidationException
	{
		List<Issue> validationIssues = new ArrayList<Issue>();
		if(user.getFirstName()== null || user.getFirstName().trim().length() == 0)
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION,	"First Name may not be null"));
		}
		if(user.getLastName()== null || user.getLastName().trim().length() == 0)
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION,	"Last Name may not be null"));
		}
		if(user.getEmail() == null || user.getEmail().trim().length() == 0)
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION,	"Email may not be null"));
		}	
		/*if(user.getTimeZoneId().getCode() == null || user.getTimeZoneId().getCode().trim().length() == 0)
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Time Zone may not be null"));
		}*/	
		if(user.getOrganization() == null ||((user.getOrganization().getOID()== null || user.getOrganization().getOID().length() == 0 )
				&&(user.getOrganization().getSiteAltId() == null || user.getOrganization().getSiteAltId().length() == 0) 
			&& (user.getOrganization().getSiteName() == null || user.getOrganization().getSiteName().length() == 0)))
		{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"Organization may not be null")); 
		}
		if((user.getUserdefaultgroup()== null) || ((user.getUserdefaultgroup().getOID() == null || user.getUserdefaultgroup().getOID().length() == 0) && (user.getUserdefaultgroup().getGroupName() == null || user.getUserdefaultgroup().getGroupName().trim().length() == 0)))
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "User Default Group may not be null"));
		}	
		if(user.getUserLoginName()== null || user.getUserLoginName().trim().length() == 0)
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION,	"Login Name may not be null"));
		}
		if(user.getUserPassword()== null || user.getUserPassword().trim().length() == 0)
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION,	"Password required "));
		}
		if(user.getUserESign()== null || user.getUserESign().trim().length() == 0)
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION,	"eSign required "));
		}
		if (validationIssues.size() > 0)
		{
			((ResponseHolder) parameters.get("ResponseHolder"))
			.getIssues().addAll(validationIssues); 
		if (logger.isDebugEnabled()) logger.debug("Validation Issues found for UserServiceImpl");
			throw new ValidationException();
		}
		
		
	}
	
	public int createUser(User user,Map<String, Object> parameters) throws OperationException, ParseException
	{
		
		UserBean callingUser = (UserBean) parameters.get("callingUser");
		ObjectMapService objectMapService= (ObjectMapService) parameters.get("objectMapService");
		SessionContext sessionContext = (SessionContext) parameters.get("sessionContext"); 
		UserAgentRObj userAgent = (UserAgentRObj) parameters.get("userAgent");
		validateUser(user,parameters);
	
		 String userAddId = null;
		 String userId = null;
		 String usrGrpId = null;
		 String uTimeZone ="";
		 String logouttime="";
		 String pwdexpires="";
		 String esignexpires="";
		 String siteUrl = "";
		 int rows = 0;
				 
		AddressBean addressSKUser = new AddressBean();
		UserBean userSK = new UserBean();
		UsrGrpBean usrGrpSK = new UsrGrpBean();
				
		addressSKUser.setAddCity(user.getCity());
		addressSKUser.setAddPri(user.getAddress());
		addressSKUser.setAddState(user.getState());
		addressSKUser.setAddZip(user.getZip());
		addressSKUser.setAddCountry(user.getCountry());
		addressSKUser.setAddPhone(user.getPhone());
	  
		// -- default setting for user account 

					CommonAgentRObj commonAgentRObj = EJBUtil.getCommonAgentHome();
					SettingsDao settingsDao = commonAgentRObj.getSettingsInstance();
					int modname=1;
					int accId = (StringUtil.stringToNum(callingUser.getUserAccountId()));
					settingsDao.retrieveSettings((accId),modname);
					ArrayList keywords=settingsDao.getSettingKeyword();
					CodeDao codeDao = new CodeDao();
					codeDao.getTimeZones();
					ArrayList setvalues=settingsDao.getSettingValue();
					HashMap keywordHash = new HashMap();
					for(int i=0;i<keywords.size();i++)
					{	
						keywordHash.put(keywords.get(i).toString(),(setvalues.get(i)==null)?"":setvalues.get(i).toString()) ;
					}
					uTimeZone = String.valueOf(keywordHash.get("ACC_USER_TZ"));
					logouttime =  String.valueOf(keywordHash.get("ACC_SESSION_TIMEOUT"));
					pwdexpires = String.valueOf(keywordHash.get("ACC_DAYS_PWD_EXP"));
					esignexpires = String.valueOf(keywordHash.get("ACC_DAYS_ESIGN_EXP"));
						//ACC_DAYS_PWD_EXP_OVERRIDE
		
	// -----getting the groupPK from ObjectLocator ----
		
		Integer groupPK = null;
		try
		{
			groupPK = ObjectLocator.groupPKFromGroupIdentifier(callingUser, user.getUserdefaultgroup(), objectMapService);
		}
		catch(MultipleObjectsFoundException e)
		{
			((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
			new Issue(IssueTypes.DATA_VALIDATION,"Multiple records found for group :" +user.getUserdefaultgroup().getGroupName()));
			throw new OperationException();
		}
	
		if(groupPK == null)
		{
			((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.GROUP_NOT_FOUND, "Group not found for  OID : "+user.getUserdefaultgroup().getOID()+"  Group not found for  group name : "+user.getUserdefaultgroup().getGroupName()));
			
			throw new OperationException();
		}	
		else
		{
			userSK.setUserGrpDefault(groupPK.toString());
		}	
		
	// --- Validating  email	
		if(user.getEmail() != null && user.getEmail().length() != 0)
		{	
			try 
			{
				String email = user.getEmail();
				InternetAddress emailAddr = new InternetAddress(email);
				emailAddr.validate();
				addressSKUser.setAddEmail(email);
			} 
			catch (AddressException ex)
			{
				
				((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
                           new Issue(
                           IssueTypes.DATA_VALIDATION, "Invalid Email Address : " + user.getEmail()));
                         throw new OperationException(); 
			}
			
		}	
	
	      // --eSign checks -----	
			String eSign = user.getUserESign();
			boolean esignValue = false;
			if(eSign.matches("\\d+"))
			{
				esignValue=true;
			}
			else
			{
				esignValue=false;
			}	
			if(!esignValue)
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(IssueTypes.DATA_VALIDATION,"e-Signature must be numeric. Please enter again. " ));
				throw new OperationException();
			}
			
			if(eSign.trim().length()<4)
			{

				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(IssueTypes.DATA_VALIDATION,"e-Signature must be atleast 4 characters long. Please enter again." ));
				throw new OperationException();
			}	
		
		// ---UserPassword checking ------	
			if(user.getUserPassword().trim().length()< 8)
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(IssueTypes.DATA_VALIDATION," Password must be atleast 8 characters long. Please enter again." ));
				throw new OperationException();
			}	
		// --if userpassword and UserLogin name are same -------- 	
			if(user.getUserPassword().equals(user.getUserLoginName()))
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
									new Issue(IssueTypes.DATA_VALIDATION," Password cannot be same as Login Name. Please enter again. " ));
				throw new OperationException();
			}	
		
		//--- pwd expire date
			Date todayDate=new Date();
		    Calendar cal = Calendar.getInstance();
			cal.setTime(todayDate);
			cal.add(Calendar.DATE, (StringUtil.stringToNum(pwdexpires)));  // number of days to add to pwd expire date
			todayDate = cal.getTime(); 
			userSK.setUserPwdExpiryDate(DateUtil.dateToString(todayDate));
						
			//--- eSign expire date
			Date todayDate1=new Date();
		    Calendar cal1 = Calendar.getInstance();
			cal1.setTime(todayDate1);
			cal1.add(Calendar.DATE, (StringUtil.stringToNum(esignexpires)));  // number of days to add to eSign expire date
			todayDate1 = cal.getTime();
			userSK.setUserESignExpiryDate(DateUtil.dateToString(todayDate1));
		   			
			userSK.setUserFirstName(user.getFirstName());
			userSK.setUserLastName(user.getLastName());
			userSK.setUserSecQues(user.getUserSecurityQuestion());
			userSK.setUserAnswer(user.getUserSecurityAnswer());
			userSK.setUserPhaseInv(user.getUserTrialPhaseInvolvement());
			userSK.setUserWrkExp(user.getUserWorkExperience());
			userSK.setUserPwd(Security.encryptSHA(user.getUserPassword()));
			userSK.setUserESign(user.getUserESign());
			userSK.setUserSessionTime(logouttime);
			userSK.setUserPwdExpiryDays(pwdexpires);
			userSK.setUserESignExpiryDays(esignexpires);
			userSK.setIpAdd(AbstractService.IP_ADDRESS_FIELD_VALUE);
			userSK.setCreator(StringUtil.integerToString(callingUser.getUserId()));		
		
	// --- checks for more than one login name in DB ------		
			int count = 0;
			 try
			 {
		           userAgent = EJBUtil.getUserAgentHome();
		           count = userAgent.getCount(user.getUserLoginName());
		           Rlog.debug("user", "UserServiceHelper.getCount after Dao");
		     }
			 catch (Exception e) {
		            Rlog.fatal("user", "Error in getCount(String userLogin) in UserServiceHelper "
		                    + e);
		        }
			 if(count > 0)
			 {
				 ((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
							new Issue(IssueTypes.DATA_VALIDATION," The Login ID you have entered already exists. Please enter a new Login Id.***** " ));
					throw new OperationException();
			 }
			 else
			 {
				 userSK.setUserLoginName(user.getUserLoginName());
			 }
	
			 // --- checks for UserStatus ----		 
			
			if((user.getUserStatus() == null) || (user.getUserStatus().toString().length()==0))
			{	
				((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
	                     new Issue(
	                     IssueTypes.DATA_VALIDATION, "User Status field is required. " ));
	                   throw new OperationException();
				    
			}
			userSK.setUserStatus(user.getUserStatus().getLegacyString());
			
			if(user.getJobType() != null && user.getJobType().getCode() != null)
			{
				try
				{	
					userSK.setUserCodelstJobtype(AbstractService.dereferenceCodeStr(user.getJobType(), CodeCache.CODE_TYPE_JOB_TYPE, callingUser));
				}
				catch(CodeNotFoundException e)
				{
					((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
							new Issue(
								IssueTypes.DATA_VALIDATION, 
								"User Job Type Code Not Found: " ));
					throw new OperationException();
				}
				
			}	
				
			
		//--- checks for User TimeZone -----
			if(user.getTimeZoneId() != null && user.getTimeZoneId().getCode() != null)
			{
				try
				{	
					userSK.setTimeZoneId(AbstractService.dereferenceTzCodeStr(user.getTimeZoneId(), "timezone", callingUser));
				}
				catch(CodeNotFoundException e)
				{
					((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
							new Issue(
								IssueTypes.DATA_VALIDATION, 
								"Time Zone Code Not Found: " ));
					throw new OperationException();
				}
				
			}
		// ---- default timeZone --------	
			else
			{
				userSK.setTimeZoneId(uTimeZone);
						
			}	
		// --- checking primary specialty -----------	
			
			if(user.getPrimarySpecialty() != null && user.getPrimarySpecialty().getCode() != null)
			{
				try
				{			
					userSK.setUserCodelstSpl(AbstractService.dereferenceCodeStr(user.getPrimarySpecialty(), CodeCache.CODE_TYPE_PRIMARY_SPECIALTY, callingUser));
				}
				catch(CodeNotFoundException e)
				{
					((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
							new Issue(
								IssueTypes.DATA_VALIDATION, 
								"Primary Speciality Code Not Found: " ));
					throw new OperationException();
				}
				
			}
			 userSK.setUserType(ObjectLocator.USER_TYPE_SYSTEM);
			 userSK.setUserAccountId(callingUser.getUserAccountId());
			  Integer sitePK = 0;
				try
				{
					 sitePK = ObjectLocator.sitePKFromIdentifier(callingUser, user.getOrganization(), sessionContext, objectMapService);
				}
				catch(MultipleObjectsFoundException moe)
				{
					((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "organizations not found for  OID:"
							+ user.getOrganization().getOID() + " siteAltID:" + user.getOrganization().getSiteAltId() + " siteName:" + user.getOrganization().getSiteName()));
					
					throw new OperationException();
				}
				if(sitePK == null || sitePK == 0)
				{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "organizations not found for OID :"
							+ user.getOrganization().getOID() + " siteAltID:" + user.getOrganization().getSiteAltId() + " siteName:" + user.getOrganization().getSiteName()));
					
					throw new OperationException();
				}	
			// ---checking organization access rights of callingUser -----					
				
				SiteDao siteDao = new SiteDao();
				siteDao.getSitesByUser(callingUser.getUserId());
				boolean hasAccessright = false;
				for(int siteCount=0;siteCount<siteDao.getSiteIds().size();siteCount++)
				{
					if(sitePK.equals(siteDao.getSiteIds().get(siteCount)))
					{
						userSK.setUserSiteId((StringUtil.integerToString(sitePK)));
						hasAccessright=true;
						break;
					}
					
				}	
				if(!hasAccessright)
				{
						((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.ORGANIZATION_AUTHORIZATION, "User not authorised to add new user for site OID :"
								+ user.getOrganization().getOID() + " siteAltID:" + user.getOrganization().getSiteAltId() + " siteName:" + user.getOrganization().getSiteName()));
						throw new OperationException();
				}
				 try {
			            AddressAgentRObj addressUserAgentRObj = EJBUtil
			                    .getAddressAgentHome();
			            userAddId = String.valueOf(addressUserAgentRObj
			                    .setAddressDetails(addressSKUser));
			           	           
			            Rlog.debug("UserServiceHelper",
			                    "In UserServiceHelper.createUSer for user address");
			        } catch (Exception e) {
			            Rlog.fatal("accountWrapper",
			                    "Error in UserServiceHelper.createUser for user address "
			                            + e);
			            return -2;
			        }
				 userSK.setUserPerAddressId(userAddId);
				 try
				 {
			            UserAgentRObj userAgentRObj1 = EJBUtil.getUserAgentHome();
			            userId = String.valueOf(userAgentRObj1.setUserDetails(userSK));
			                 
			            Rlog.debug("UserServiceHelper",
			                    "In UserServiceHelper.createUser for user");
			     } catch (Exception e) {
			            Rlog.fatal("UserServiceHelper",
			                            "Error in UserServiceHelper.createUser for user "
			                                    + e);
			           throw new OperationException();
			        } 
				
				    usrGrpSK.setUsrGrpUserId(userId);
			        usrGrpSK.setUsrGrpGroupId(groupPK.toString());	       
			        usrGrpSK.setIpAdd(AbstractService.IP_ADDRESS_FIELD_VALUE);
			        usrGrpSK.setCreator((StringUtil.integerToString(callingUser.getUserId())));
			        try {
			            UsrGrpAgentRObj usrGrpAgentRObj = EJBUtil.getUsrGrpAgentHome();
			            usrGrpId = String.valueOf(usrGrpAgentRObj
			                    .setUsrGrpDetails(usrGrpSK));
			            Rlog.debug("accountWrapper",
			                    "In AccountWrapperAgentBean.createUserGroup for user"
			                            + usrGrpId);
			        } catch (Exception e) {
			            Rlog.fatal("accountWrapper",
			                    "Error in AccountWrapperAgentBean.createUserGroup for user "
			                            + e);
			            throw new OperationException();
			        }
			        
			 // --- checks for sending notification to newly created user        
			    	
			        if(user.isSendNotifaction())
					{
						CtrlDao urlCtrl = new CtrlDao();	
						urlCtrl.getControlValues("site_url");
						rows = urlCtrl.getCRows();
						if (rows > 0) {
							siteUrl = (String) urlCtrl.getCValue().get(0);
						}
						 userAgent = EJBUtil.getUserAgentHome();
						 userAgent.notifyUserResetInfo(user.getUserPassword(), siteUrl, user.getUserESign(),StringUtil.stringToNum(userId), user.getUserLoginName(), "ID");
						 userAgent.notifyUserResetInfo(user.getUserPassword(), siteUrl, user.getUserESign(),StringUtil.stringToNum(userId), user.getUserLoginName(), "P");
										
					}
			    	 return (StringUtil.stringToNum(userId));
	}
	
	

}
