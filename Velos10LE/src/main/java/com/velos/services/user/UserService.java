/**
 * 
 */
package com.velos.services.user;

import javax.ejb.Remote;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.Groups;
import com.velos.services.model.NonSystemUser;
import com.velos.services.model.Organizations;
import com.velos.services.model.User;
import com.velos.services.model.UserIdentifier;
import com.velos.services.model.User.UserStatus;
import com.velos.services.model.UserSearch;
import com.velos.services.model.UserSearchResults;
/**
 * @author dylan
 *
 */
@Remote
public interface UserService
{
	public static final String TABLE_NAME="er_user";
	public ResponseHolder changeUserStatus(UserIdentifier userId,UserStatus userStat) throws OperationException;
	public Groups getAllGroups() throws OperationException;
	public Groups getUserGroups(UserIdentifier userId) throws OperationException;
	public ResponseHolder createNonSystemUser(NonSystemUser nonSystemUser) throws OperationException; 
	public Organizations getAllOrganizations() throws OperationException;
	public ResponseHolder killUserSession(UserIdentifier userId) throws OperationException; 
    public ResponseHolder createUser(User user) throws OperationException ;
	public ResponseHolder checkESignature(String eSignature) throws OperationException;
	public UserSearchResults searchUser(UserSearch userSearch) throws OperationException;
}
