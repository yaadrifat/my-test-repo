/**
 * 
 */
package com.velos.services.study;


import javax.ejb.Remote;


/**
 * The SummaryCLOB EJB exists to persist Clob fields from
 * er_study. eResearch persists them in a separate transaction through
 * a stored procedure. This creates issues in the service layer as 
 * all persistence is done in the container-managed transaction. So,
 * instead of going through the stored procedure for these fields, 
 * we create EJBs to handle them.
 * 
 * @author dylan
 *
 */
@Remote
public interface SummaryCLOBService  {
	public static final String MESSAGE_LOG_KEY = "com.velos.services.message";
	
	public void updateCLOB(SummaryCLOB summaryCLOB);
	
}
