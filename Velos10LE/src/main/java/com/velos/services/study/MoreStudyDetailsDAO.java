/*Virendra
 * 
 * 
 * 
 */

package com.velos.services.study;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CommonDAO;
import com.velos.services.OperationException;
import com.velos.services.model.NVPair;
import com.velos.services.model.StudySummary;
import com.velos.services.util.CodeCache;


public class MoreStudyDetailsDAO extends CommonDAO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8410547449723597051L;
	private static Logger logger = Logger.getLogger(MoreStudyDetailsDAO.class);

	private static String studyStatusSql = "select pk_studyid, pk_codelst, codelst_subtyp," +
			" codelst_desc, STUDYID_ID," +
			" codelst_seq,codelst_custom_col " +
			",codelst_custom_col1 from  er_studyid, er_codelst "  +
			" where fk_study = ? and  pk_codelst = FK_CODELST_IDTYPE  and" +
			" codelst_type = 'studyidtype'" +
			"and not exists (select * from er_codelst_hide h where h.codelst_type = 'studyidtype' and h.fk_grp = ? and h.fk_codelst = pk_codelst and h.CODELST_HIDE_TABLE = 1) ";
	
	 public static Map<Integer, NVPair> getStudyMoreStudyDetails(int studyPK, int usersDefaultGroup) 
	 
	 throws 
	 OperationException{

	        PreparedStatement pstmt = null;
			Connection conn = null;
	        try
	        {
	        	conn = getConnection();
				if (logger.isDebugEnabled()) logger.debug("com.velos.services.study.StudyStatusDAO.getStudy sql:" + studyStatusSql);
				
				pstmt = conn.prepareStatement(studyStatusSql);
				pstmt.setInt(1, studyPK);
				pstmt.setInt(2, usersDefaultGroup);
				
				ResultSet rs = pstmt.executeQuery();
				
				Map<Integer, NVPair> MSDMap = new HashMap<Integer, NVPair>();
				
				while (rs.next()) {
					String MSDKey = rs.getString("codelst_subtyp");
					if (MSDKey != null && MSDKey.length() > 0){
						Integer MSDPK = rs.getInt("pk_studyid");
						NVPair item = new NVPair();
						String MSDValue = rs.getString("STUDYID_ID");
						item.setKey(MSDKey);
						item.setValue(MSDValue);
						item.setType(StudySummary.MORE_STUDY_DETAILS_NS);
						MSDMap.put(MSDPK, item);
					}
				}
				return MSDMap;
	        }
	        catch (SQLException ex) {
				logger.fatal("StudyStatusDAO.getStudy EXCEPTION IN FETCHING FROM er_studystatus"
						+ ex);
				throw new OperationException(ex);
			} finally {
				try {
					if (pstmt != null)
						pstmt.close();
				} catch (Exception e) {
				}
				try {
					if (conn != null)
						conn.close();
				} catch (Exception e) {
				}

			}

	 }
	        
	
	

}
