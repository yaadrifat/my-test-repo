/**
 * 
 * Data Access Object responsible for fetching  StudyStatuses
 * 
 * 11/30/2010 DRM - added current stat field
 */
package com.velos.services.study;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.Rlog;
import com.velos.services.OperationException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.Code;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.StudyStatus;
import com.velos.services.model.StudyStatusIdentifier;
import com.velos.services.model.UserIdentifier;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ServicesUtil;


public class StudyStatusDAO extends CommonDAO {

	
	private static final long serialVersionUID = 7548543001098480469L;
	private static Logger logger = Logger.getLogger(StudyStatusDAO.class);
	
	/**Declaration of a Regular expression String 
	 * used as custom splitting expression in Velos.
	 */
	public static final String VELSEP = "[VELSEP]";
	public static final String VELSEP_REGX = "\\[VELSEP\\]";
	
	/**Query String for fetching values related to Study Statuses. 
	 */
	private static String studyStatusSql = "SELECT "
					+"(select USR_LOGNAME||'" + VELSEP + "'|| usr_lastname||'" + VELSEP + "'||usr_firstname"
					+" from er_user where pk_user= FK_USER_DOCBY),"
					+"(select USR_LOGNAME||'" + VELSEP + "'||usr_lastname||'" + VELSEP + "'||usr_firstname "
					+"from er_user where pk_user= STUDYSTAT_ASSIGNEDTO) ,"
					+"(select SITE_NAME||'" + VELSEP + "'||PK_SITE||'" + VELSEP + "'||SITE_ALTID from er_site where pk_site = fk_site),"
					+"STATUS_TYPE," + "PK_STUDYSTAT," + "STUDYSTAT_DATE," 
					+"STUDYSTAT_ENDT," + "STUDYSTAT_MEETDT," 
					+"FK_CODELST_REVBOARD," + "FK_CODELST_STUDYSTAT,"+ "OUTCOME,"+"STUDYSTAT_NOTE,"
					+"STUDYSTAT_VALIDT, CURRENT_STAT, FK_USER_DOCBY, STUDYSTAT_ASSIGNEDTO"
    				+" FROM ER_STUDYSTAT WHERE FK_STUDY =?";
	
	/**Static method, takes studyPk and UserAccountPK as parameters, 
	 * and returns List of Study Statuses 
	 **/
	 public static List<StudyStatus> getStudyStatuses(int studyPK, 
				int userAccountPK1) throws OperationException{

 	        Rlog.debug("studystatus", "In DAO: com.velos.services.study.StudyStatusDAO;");
	        PreparedStatement pstmt = null;
			Connection conn = null;
	        try
	        {
	        	conn = getConnection();
	        	CodeCache codeCache = CodeCache.getInstance();
				if (logger.isDebugEnabled()) logger.debug("com.velos.services.study.StudyStatusDAO.getStudy sql:" + studyStatusSql);
				
				pstmt = conn.prepareStatement(studyStatusSql);
				pstmt.setInt(1, studyPK);
				ResultSet rs = pstmt.executeQuery();
				
				ArrayList<StudyStatus> studyStatus = new ArrayList<StudyStatus>();
				StudyStatus studyStat = null;
				while (rs.next()) {
					
					/*Get StudyStatusCodeType by PK from CodeCache,
					 *taking studystat_type, STATUS_TYPE, 
					 *and userAccountPK as parameters 
					 */
					//Virendra: Fixed Bug No. 5385.
					//Instantiated StudyStatus inside loop 
					//for every new StudyStatus Object
					studyStat = new StudyStatus();
					
					Code studyStatusCodeType = 
						codeCache.getCodeSubTypeByPK(
								CodeCache.CODE_TYPE_STATUS_TYPE, 
								rs.getInt("STATUS_TYPE"),
								userAccountPK1);
					/*Get ObjectMapService to get or create OID for Organization and Study Status */
					
					ObjectMapService  mapService = ServicesUtil.getObjectMapService();
					
					// Kanwal - included StudyStatusIdentifier for Study Status					
					StudyStatusIdentifier studyStatusId = new StudyStatusIdentifier(); 
					ObjectMap map1 = mapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY_STATUS, rs.getInt("PK_STUDYSTAT"));
					studyStatusId.setOID(map1.getOID());
					studyStatusId.setPK(rs.getInt("PK_STUDYSTAT"));
					studyStat.setStudyStatusId(studyStatusId); 
					
					
					/*Get StudyStatusCode by PK from CodeCache,
					 *taking studystat, PK_STUDYSTAT(From ER_STUDYSTAT table, 
					 *and userAccountPK as parameters 
					 */
					Code studyStatusCode = 
						codeCache.getCodeSubTypeByPK(
								CodeCache.CODE_TYPE_STATUS, 
								rs.getInt("FK_CODELST_STUDYSTAT"),
								userAccountPK1);
					
					String usrDocByCombo= rs.getString(1);
					String usrAssignToCombo= rs.getString(2);
					String orgIdCombo = rs.getString(3);
					
					if(usrDocByCombo != null){
						/*splitting name in first name, 
						* last name and login user for User Documented By
						*/
						String usrDocByLogName="";
						String usrDocByLname="";
						String usrDocByFname="";
						
						String[] usrDocByComboArr=usrDocByCombo.split(VELSEP_REGX);
						usrDocByLogName=usrDocByComboArr[0];
						usrDocByLname=usrDocByComboArr[1];
						usrDocByFname=usrDocByComboArr[2];
						
						UserIdentifier usrIdentifier1 = new UserIdentifier(
								usrDocByLogName,usrDocByFname,usrDocByLname);
						usrIdentifier1.setUserLoginName(usrDocByLogName);
						usrIdentifier1.setFirstName(usrDocByFname);
						usrIdentifier1.setLastName(usrDocByLname);
						ObjectMap map2 = mapService.getOrCreateObjectMapFromPK(
						        ObjectMapService.PERSISTENCE_UNIT_USER, rs.getInt("FK_USER_DOCBY"));
						usrIdentifier1.setOID(map2.getOID());
						studyStat.setDocumentedBy(usrIdentifier1);
					
					}
					
					
					if(usrAssignToCombo != null){
						/* Splitting name in first name, last name 
						 * and loginuser for User Assignedto 
						 */
						String usrAssignToLogName="";
						String usrAssignToLname="";
						String usrAssignToFname="";
						String[] usrAssignToComboArr=usrAssignToCombo.split(VELSEP_REGX);
						usrAssignToLogName=usrAssignToComboArr[0];
						usrAssignToLname=usrAssignToComboArr[1];
						usrAssignToFname=usrAssignToComboArr[2];
						/*Creating User identifier for UserAssignetTo,
						 * Sending Param UserAssignedTo LoginName, 
						 * UserAssignedTo FirstName and UserAssignedTo LastName
						 */						
						UserIdentifier usrIdentifier2 = new UserIdentifier(
								usrAssignToLogName,usrAssignToFname,usrAssignToLname);
						usrIdentifier2.setUserLoginName(usrAssignToLogName);
						usrIdentifier2.setFirstName(usrAssignToFname);
						usrIdentifier2.setLastName(usrAssignToLname);
                        ObjectMap map3 = mapService.getOrCreateObjectMapFromPK(
                                ObjectMapService.PERSISTENCE_UNIT_USER, rs.getInt("STUDYSTAT_ASSIGNEDTO"));
                        usrIdentifier2.setOID(map3.getOID());

						studyStat.setAssignedTo(usrIdentifier2);
						
					}
					 
					
					if (orgIdCombo != null){
						String[] orgIdFieldArray = orgIdCombo.split(VELSEP_REGX);
						
						OrganizationIdentifier orgId = 
							new OrganizationIdentifier();
						orgId.setSiteName(orgIdFieldArray[0]);						
						ObjectMap map = mapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.parseInt(orgIdFieldArray[1]));
						orgId.setOID(map.getOID());
						
						if (orgIdFieldArray.length > 2){
							
							orgId.setSiteAltId(orgIdFieldArray[2]);
						}		
						studyStat.setOrganizationId(orgId);
						
					}
					
					/*Get studyStatus ReviewBoard Code by PK from CodeCache,
					 *taking rev_board, FK_CODELST_REVBOARD(from ER_STUDYSTAT Table) , 
					 *and userAccountPK as parameters 
					 */
					Code studyStatusReviewBoardCode = 
						codeCache.getCodeSubTypeByPK(
								CodeCache.CODE_TYPE_REVIEW_BOARD, 
								rs.getInt("FK_CODELST_REVBOARD"),
								userAccountPK1);
					/*Get studyStatus Outcome Code by PK from CodeCache,
					 *taking studystat_out, OUTCOME(from ER_STUDYSTAT Table) , 
					 *and userAccountPK as parameters 
					 */
					Code studyStatusOutcomeCode = 
						codeCache.getCodeSubTypeByPK(
								CodeCache.CODE_TYPE_OUTCOME, 
								rs.getInt("OUTCOME"),
								userAccountPK1);
					
					Boolean isCurrentStat = (rs.getInt("CURRENT_STAT") == 1 ? true : false);
					/*Setting the values in Study Status List  
					 */
					studyStat.setStatusType(studyStatusCodeType);
					studyStat.setStatus(studyStatusCode);
					studyStat.setValidFromDate(rs.getDate("STUDYSTAT_DATE"));
					//studyStat.setValidUntilDate(rs.getDate("STUDYSTAT_ENDT"));//virendra
					studyStat.setMeetingDate(rs.getDate("STUDYSTAT_MEETDT"));
					studyStat.setReviewBoard(studyStatusReviewBoardCode);
					studyStat.setOutcome(studyStatusOutcomeCode);
					studyStat.setNotes(rs.getString("STUDYSTAT_NOTE"));
					//Virendra:Fixed Bug No:5451
					studyStat.setValidUntilDate(rs.getDate("STUDYSTAT_VALIDT"));
					studyStat.setCurrentForStudy(isCurrentStat);
					
					
					studyStatus.add(studyStat);
					
					
					
				}
				return studyStatus;
	        }
	        catch (SQLException ex) {
				logger.fatal("StudyStatusDAO.getStudy " +
						"EXCEPTION IN FETCHING FROM er_studystatus"
						+ ex);
				throw new OperationException(ex);
			} finally {
				try {
					if (pstmt != null)
						pstmt.close();
				} catch (Exception e) {
				}
				try {
					if (conn != null)
						conn.close();
				} catch (Exception e) {
				}

			}

	 }
	        
	
	

}
