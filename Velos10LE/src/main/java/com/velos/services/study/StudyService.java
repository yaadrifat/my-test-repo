/**
 * 
 */
package com.velos.services.study;


import java.util.List;

import javax.ejb.Remote;


import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.Study;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyOrganization;
import com.velos.services.model.StudySearch;
import com.velos.services.model.StudyStatus;
import com.velos.services.model.StudyStatuses;
import com.velos.services.model.StudyStatusIdentifier;
import com.velos.services.model.StudySummary;
import com.velos.services.model.StudyTeamMember;
import com.velos.services.model.StudyTeamMembers;
import com.velos.services.model.UserIdentifier;

/**
 * This interface defines operations for affecting Studies and their
 * sub-components.
 * 
 * In 2.0, this provides operations for creating a study, adding Study Team
 * Members and Organizations, and adding Study Statuses.
 * 
 * Each operation occurs within a transaction. If an operation fails, an
 * {@link com.velos.services.OperationException} is thrown. Details of the error can be found in
 * {@link com.velos.services.OperationException#getIssues()} 
 * 
 * 
 * @since 2.0
 * @author dylan
 * 
 */


@Remote
public interface StudyService {
	
	/**
	 * Returns a {@link com.velos.services.model.Study} object based on the studyIdentifier. This operation is used
	 * to get Summary, Team, Organization and Status information about a Study.
	 * @param studyIdentifier studyIdentifier for locating the Study
	 * @return Study object representing the study
	 * @throws OperationException
	 */
	public Study getStudy(StudyIdentifier studyIdentifier) throws OperationException;
	
	/**
	 * Gets the StudySummary object for the Study identified by the StudyIdentifier.
	 * @param studyIdentifier
	 * @return The StudySummary object associated with the Study
	 * @throws OperationException
	 */
	public StudySummary getStudySummary(StudyIdentifier studyIdentifier) throws OperationException;
	
	/**
	 * Creates a new Study based on the values provided in the {@link com.velos.services.model.Study} object.
	 * Note that a default Status is always created. Statuses send within this Study are
	 * added in addition. 
	 * 
	 * For study summary users and study team members, this operation can create non-system users
	 * at the same time. If a UserIdentifier is sent in one of these positions, and the createNonSystemUser
	 * is parameter is true, then the system will create a new non-system user.
	 * 
	 * Note that an initial status will be created in addition to any statuses sent in through
	 * this method.
	 * 
	 * @param study Study to create
	 * @param createNonSystemUsers If true, and a user in team user is non-system and not matched
	 * by first and last name, system will create a new non-system user. If false, will send error.
	 * 
	 * @return A ResponseHolder instance that may hold information about database persisting 
	 * actions that occurred as a result of this operation.
	 * 
	 * @throws OperationException
	 */
	public ResponseHolder createStudy(Study study, boolean createNonSystemUsers) throws OperationException;
	
	/**
	 * Updates the StudySummary. Values send as null will be nulled out.
	 * 
	 * In this version of eSP, this method requires that a complete {@link StudySummary}
	 * object be provided. Any fields sent in as null will result in a null field.
	 * 
	 * @param studyId Identifies the study to update
	 * @param summary Summary information to update.
	 * @param createNonSystemUsers If true, and a user in team user is non-system and not matched
	 * by first and last name, system will create a new non-system user. If false, will send error.
	 * 
	 * @return A ResponseHolder instance that may hold information about database persisting 
	 * actions that occured as a result of this operation.
	 * 
	 * @throws OperationException
	 */
	public ResponseHolder updateStudySummary(StudyIdentifier studyId, StudySummary summary, boolean createNonSystemUsers) throws OperationException;

	/**
	 * Adds the given study team member to the study. 
	 * 
	 * If the user identified in the {@link StudyTeamMember#setUserId(UserIdentifier)} member
	 * is a hidden user, the method will return an error.
	 * 
	 * In this version of eSP, this method requires that a complete {@link StudyTeamMember}
	 * object be provided. Any fields sent in as null will result in a null field.
	 * 
	 * @param studyId Identifies the study to update
	 * @param studyTeamMember StudyTeamMember to add to the study
	 * @param createNonSystemUsers If true, and a user in team user is non-system and not matched
	 * by first and last name, system will create a new non-system user. If false, will send error.
	 * @return A ResponseHolder instance that may hold information about database persisting 
	 * actions that occurred as a result of this operation.
	 * @throws OperationException
	 */
	public ResponseHolder addStudyTeamMember(StudyIdentifier studyId, StudyTeamMember studyTeamMember, boolean createNonSystemUsers) throws OperationException;
	
	/**
	 * Updates a study team member for the given study. The study team member to be updated
	 * is identified by the provide StudyTeamMember.userId field.
	 * 
	 * Values send as null will be nulled out.
	 * 
	 * @param studyId Identifies the study to update
	 * 
	 * @param studyTeamMember StudyTeamMember to update
	 * 
	 * @return A ResponseHolder instance that may hold information about database persisting 
	 * actions that occurred as a result of this operation.
	 * 
	 * @throws OperationException
	 */
	public ResponseHolder updateStudyTeamMember(StudyIdentifier studyId, StudyTeamMember studyTeamMember) throws OperationException;
	
	/**
	 * Removes the given study team member 
	 * 
	 * @param studyId Identifies the study to update
	 * 
	 * @param teamUserId Identifies the team member to remove
	 * 
	 * @return A ResponseHolder instance that may hold information about database persisting 
	 * actions that occurred as a result of this operation.
	 * 
	 * @throws OperationException
	 */
	public ResponseHolder removeStudyTeamMember(StudyIdentifier studyId, UserIdentifier teamUserId) throws OperationException;
	
	/**
	 * Adds an organization to the study.
	 * 
	 * 
	 * @param studyId Identifies the study to update
	 * 
	 * @param studyOrganization StudyOrganization instance to add to the study
	 * 
	 * @return A ResponseHolder instance that may hold information about database persisting 
	 * actions that occurred as a result of this operation.
	 * 
	 * @throws OperationException
	 */
	public ResponseHolder addStudyOrganization(StudyIdentifier studyId, StudyOrganization studyOrganization) throws OperationException;
	
	/**
	 * Updates a StudyOrganization. The organization to update is identified by the 
	 * {@link StudyOrganization#setOrganizationIdentifier(OrganizationIdentifier)} provided.
	 * 
	 * In this version of eSP, this method requires that a complete {@link StudyOrganization}
	 * object be provided. Any fields sent in as null will result in a null field.
	 * 
	 * @param studyId Identifies the study to update 
	 * 
	 * @param studyOrganization StudyOrganization to update.
	 * 
	 * @return A ResponseHolder instance that may hold information about database persisting 
	 * actions that occurred as a result of this operation.
	 * 
	 * @throws OperationException
	 */
	public ResponseHolder updateStudyOrganization(StudyIdentifier studyId, StudyOrganization studyOrganization) throws OperationException;
	
	/**
	 * Removes a StudyOrganization from a study
	 * 
	 * @param studyId Identifies the study to update 
	 * 
	 * @param organizationId Identifies the organization to remove
	 * 
	 * @return A ResponseHolder instance that may hold information about database persisting 
	 * actions that occurred as a result of this operation.
	 * 
	 * @throws OperationException
	 */
	public ResponseHolder removeStudyOrganization(StudyIdentifier studyId, OrganizationIdentifier organizationId) throws OperationException;
	
	/**
	 * Add a StudyStatus to a study
	 * 
	 * @param studyId Identifies the study to update 
	 * 
	 * @param studyStatus The StudyStatus to add
	 * 
	 * @return A ResponseHolder instance that may hold information about database persisting 
	 * actions that occurred as a result of this operation.
	 * 
	 * @throws OperationException
	 */
	public ResponseHolder addStudyStatus(StudyIdentifier studyId, StudyStatus studyStatus) throws OperationException;
	
	/**
	 * Get the entire list of status for a study
	 *  
	 * @param studyIdentifier Identifies the study to update 
	 * 
	 * @return List of all StudyStatuses for this Study
	 * 
	 * @throws OperationException
	 */
	public StudyStatuses getStudyStatuses(StudyIdentifier studyIdentifier) throws OperationException;
	/**
	 * 
	 * @param studyStatusIdentifier
	 * @return StudyStatus
	 * @throws OperationException
	 */
	public StudyStatus getStudyStatus(StudyStatusIdentifier studyStatusIdentifier) throws OperationException;

	/**
	 * @param studySearch
	 * @return
	 * @throws OperationException 
	 */
	public List<StudySearch> searchStudy(StudySearch studySearch) throws OperationException;
	
	/**
	 * Get the entire list of study team members
	 *  
	 * @param studyIdentifier Identifies the study to update 
	 * 
	 * @return List of all StudyTeamMembers for this Study
	 * 
	 * @throws OperationException
	 */
	public StudyTeamMembers getStudyTeamMembers(StudyIdentifier studyIdentifier) throws OperationException;

}
