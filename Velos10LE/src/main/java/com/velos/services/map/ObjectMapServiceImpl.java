/**
 * 
 */
package com.velos.services.map;

import java.util.UUID;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import com.velos.services.model.ObjectInfo;

/**
 * @author dylan
 *
 */
@Stateless
public class ObjectMapServiceImpl  implements ObjectMapService{
	private static Logger logger = Logger.getLogger("ObjectMapServiceImpl");
	@PersistenceContext(unitName = "eres")
    protected EntityManager em;
	
	public ObjectMap getObjectMapFromId(String id) 
	throws 
		MultipleObjectsFoundException{
		
		return (ObjectMap)em.find(ObjectMap.class, id);

	}
	
	//@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	protected ObjectMap createObjectMap(String tableName, Integer tablePK) {
		ObjectMap objectMap = new ObjectMap();
		UUID uuid = UUID.randomUUID();
		objectMap.setOID(uuid.toString());
		objectMap.setTableName(tableName);
		objectMap.setTablePK(tablePK);
		
		em.persist(objectMap);
		
		//String externalId = uuid.toString();
		return objectMap;
	}

	public ObjectMap getOrCreateObjectMapFromPK(String tableName, Integer tablePK) {
		Query query = em
		.createNamedQuery(ObjectMap.NAMED_QUERY_TABLE_PK);
		query.setParameter(ObjectMap.QUERY_PARAM_TABLE_NAME, tableName);
		query.setParameter(ObjectMap.QUERY_PARAM_TABLE_PK, tablePK);
		ObjectMap map = null; 
		try{
			map =  (ObjectMap)query.getSingleResult();
		
		}catch(NoResultException nre)
		{
			map = createObjectMap(tableName, tablePK); 
			
		}catch(EntityNotFoundException etn)
		{
			map = createObjectMap(tableName, tablePK); 
			
		}catch(NonUniqueResultException nue)
		{
			logger.error("No Unique record found for"+ tableName + " " +tablePK, nue);
			
		}catch(Throwable t){
			logger.error("Error occured while getOrCreateObjectMapFromPK:: TableName "+tableName+ " TablePK "+ tablePK,t);
		}
		return map;

	}
	//added by virendra to retrieve tablePk from OID
	public Integer getObjectPkFromOID(String OID){
		Query query = em
		.createNamedQuery(ObjectMap.NAMED_QUERY_TABLE_PK_FROM_OID);
		query.setParameter(ObjectMap.QUERY_PARAM_OID, OID);
		Integer tablePk = 0;
		try{
			tablePk =  (Integer)query.getSingleResult();
		}
		catch(EntityNotFoundException etn){
			logger.error("No tablePk found for OID"+ OID );
		}
		catch(NonUniqueResultException nue){
			logger.error("No Unique record found for"+ OID );
		}
		catch(Throwable t){
			logger.error("Error occured while retrieving tablePk for OID:: TableName "+OID);
		}
		return tablePk;
		
	}

	public ObjectInfo getObjectInfoFromOID(String OID){
		ObjectMap oMap = null;
		ObjectInfo oInfo = null;
		
		Query query = em.createNamedQuery(ObjectMap.NAMED_QUERY_OBJECTMAP_FROM_OID);
		query.setParameter(ObjectMap.QUERY_PARAM_OID, OID);

		try{
			oMap =  (ObjectMap)query.getSingleResult();
			oInfo = new ObjectInfo();
			oInfo.setTablePk(oMap.getTablePK());
			oInfo.setTableName(oMap.getTableName());
		}
		catch(EntityNotFoundException etn){
			logger.error("No ObjectMap found for OID "+ OID );
		}
		catch(NonUniqueResultException nue){
			logger.error("No Unique record found for OID "+ OID );
		}
		catch(Throwable t){
			logger.error("Error occured while retrieving ObjectMap for OID: "+OID);
		}
		return oInfo;
		
	}
	
}
