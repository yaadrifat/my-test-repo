package com.velos.services.patientdemographics;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.Patients;
import com.velos.services.model.UpdatePatientDemographics;



@Remote
public interface UpdatePatientDemographicsService
{
	
	public ResponseHolder checkAuthForUpdatePatient(PatientIdentifier patId, UpdatePatientDemographics updatePatDemo) throws OperationException;

	public ResponseHolder updatePatientDemographics(PatientIdentifier patId, UpdatePatientDemographics updatePatDemo) throws OperationException;
		
	public ResponseHolder updateMPatientDemographics(Patients patients) throws OperationException;
}
