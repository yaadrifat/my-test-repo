package com.velos.services.patientdemographics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import com.velos.epat.audit.service.AuditRowEpatAgent;
import com.velos.eres.business.common.PatFacilityDao;
import com.velos.eres.business.person.impl.PersonBean;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.patFacilityAgent.PatFacilityAgentRObj;
import com.velos.eres.service.patProtAgent.PatProtAgentRObj;
import com.velos.eres.service.perIdAgent.PerIdAgentRObj;
import com.velos.eres.service.personAgent.PersonAgentRObj;
import com.velos.eres.service.prefAgent.PrefAgentRObj;
import com.velos.eres.service.siteAgent.SiteAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.userSiteAgent.UserSiteAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.CRUDAction;
import com.velos.services.CompletedAction;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.Issues;
import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.ValidationException;
import com.velos.services.authorization.AbstractAuthModule;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.Patients;
import com.velos.services.model.UpdatePatientDemographics;
import com.velos.services.patientdemographics.UpdatePatientDemographicsHelper;
import com.velos.services.patientdemographics.UpdatePatientDemographicsService;
import com.velos.services.util.ObjectLocator;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@Remote({ UpdatePatientDemographicsService.class })
public class UpdatePatientDemographicsServiceImpl extends AbstractService
		implements UpdatePatientDemographicsService {

	@EJB
	GrpRightsAgentRObj groupRightsAgent;

	@EJB
	private PersonAgentRObj personAgent;

	@Resource
	private SessionContext sessionContext;

	@EJB
	private UserAgentRObj userAgent;

	@EJB
	private SiteAgentRObj siteAgent;

	@EJB
	private ObjectMapService objectMapService;

	@EJB
	private PatProtAgentRObj patProtAgent;

	@EJB
	private UserSiteAgentRObj userSiteAgent;

	@EJB
	private PatFacilityAgentRObj patFacilityAgent;

	@EJB
	private PerIdAgentRObj perIdAgentRObj;
	
	@EJB 
	private AuditRowEpatAgent auditRowEpatAgent; 
	
	@EJB
	private PrefAgentRObj prefAgent; 

	private static Logger logger = Logger.getLogger(UpdatePatientDemographicsServiceImpl.class.getName());
	
	private static Integer personPk=null;
	private boolean isOIDSupplied = true;
	private static Integer defOrgPk=null;

	public ResponseHolder updatePatientDemographics(PatientIdentifier patId, UpdatePatientDemographics updatePatDemo) 
	throws OperationException
	{
		try {
			//Authorization method call here
			checkAuthForUpdatePatient(patId, updatePatDemo, false);
		} catch (OperationException e) {
			throw e;
		}

		try
		{
		    personPk = null; 
		    
		    try{
		    	personPk = ObjectLocator.personPKFromPatientIdentifier(
		    			callingUser, 
		    			patId, 
		    			objectMapService);
		    } catch (MultipleObjectsFoundException e) {
		    	addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, 
		    			"Multiple Patients found")); 
		    	throw new OperationException(); 
		    }


		    //if person not found, add an issue
		    if(personPk == null || personPk ==0){
		        addIssue(new Issue(
		                IssueTypes.PATIENT_NOT_FOUND, 
		                "Patient not found for code: "+patId.getPatientId()+ " for given OrganizationId"));
		        throw new OperationException();
		    }
            PersonBean personBean = personAgent.getPersonDetails(personPk);
            if (personBean == null){
                addIssue(new Issue(
                        IssueTypes.PATIENT_NOT_FOUND, 
                        "Patient not found for code: "+patId.getPatientId()+" OID: "+patId.getOID()));
                throw new OperationException();
            }
            if (callingUser.getUserAccountId() == null
                    || !callingUser.getUserAccountId().equals(personBean.getPersonAccount())){
                addIssue(new Issue(
                        IssueTypes.PATIENT_NOT_FOUND, 
                        "Patient not found for OID: "+patId.getOID()));
                throw new OperationException();
            }
			
			if (updatePatDemo != null ){
				validate(updatePatDemo);
			}

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("personAgent", this.personAgent);
			parameters.put("sessionContext", this.sessionContext);
			parameters.put("userAgent", this.userAgent);
			parameters.put("siteAgent", this.siteAgent);
			parameters.put("objectMapService", this.objectMapService);
			parameters.put("patProtAgent", this.patProtAgent);
			parameters.put("userSiteAgent", this.userSiteAgent);
			parameters.put("patFacilityAgent", this.patFacilityAgent);
			parameters.put("callingUser", this.callingUser);
			parameters.put("ResponseHolder", this.response);
			parameters.put("perIdAgentRObj", this.perIdAgentRObj);
			parameters.put("auditRowEpatAgent", this.auditRowEpatAgent);					
			parameters.put("personPk", this.personPk);
			parameters.put("isOIDSupplied", this.isOIDSupplied);
			
			//calling UpdatePatient method of UpdatePatientDemographicsHelper
			UpdatePatientDemographicsHelper updatePatDemoHelper = new UpdatePatientDemographicsHelper();
			int result = updatePatDemoHelper.updatePatientDemographics(updatePatDemo,patId, parameters);
			
			if (result == 0) {
				ObjectMap map = this.objectMapService
						.getOrCreateObjectMapFromPK("person",
								Integer.valueOf(personPk));
				this.response.addAction(new CompletedAction("Patient with OID :" + map.getOID() + " or Patient PK :"+map.getTablePK()+
		    	        " or PatientCode :" + personBean.getPersonPId() + "  updated successfully", CRUDAction.UPDATE));
			} else
			{
				System.out.println("result of update patient demographics is "+ result);
				  ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
					        new Issue(
					        IssueTypes.PATIENT_ERROR_UPDATE_DEMOGRAPHICS));
					      throw new OperationException();

			}
				
		} catch (OperationException e) {
			try {
				this.sessionContext.getUserTransaction().setRollbackOnly();
			} catch (Exception ex) {
				throw new OperationException(ex);
			}
			if (logger.isDebugEnabled())
				logger.debug("PatientDemographicsServiceImpl create", e);
			throw new OperationRolledBackException(this.response.getIssues());
		} catch (Throwable t) {
			try {
				this.sessionContext.getUserTransaction().setRollbackOnly();
			} catch (Exception ex) {
				throw new OperationException(ex);
			}
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled())
				logger.debug("PatientDemographicsServiceImpl create", t);
			throw new OperationRolledBackException(this.response.getIssues());
		}

		return response;
	}
	
	@Override
	public ResponseHolder updateMPatientDemographics(
			Patients patients)
			throws OperationException {
		try
		{
			if(patients==null)
			{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid updatePatientDemographicsList is required")); 
					throw new OperationException(); 
			}
			
			
			//checking calling users group rights for manage patients
			GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
			int managePatients = authModule.getAppManagePatientsPrivileges().intValue();
			boolean hasEditPatientPermissions = GroupAuthModule.hasEditPermission(Integer.valueOf(managePatients));
			if (!hasEditPatientPermissions)
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION,"User is not authorized to edit patient data"));
				throw new AuthorizationException("User is not authorized to edit patient data");
			}
				
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("personAgent", this.personAgent);
			parameters.put("sessionContext", this.sessionContext);
			parameters.put("userAgent", this.userAgent);
			parameters.put("siteAgent", this.siteAgent);
			parameters.put("objectMapService", this.objectMapService);
			parameters.put("patProtAgent", this.patProtAgent);
			parameters.put("userSiteAgent", this.userSiteAgent);
			parameters.put("patFacilityAgent", this.patFacilityAgent);
			parameters.put("callingUser", this.callingUser);
			parameters.put("ResponseHolder", this.response);
			parameters.put("perIdAgentRObj", this.perIdAgentRObj);
			parameters.put("auditRowEpatAgent", this.auditRowEpatAgent);
			parameters.put("prefAgent", this.prefAgent);

			List<UpdatePatientDemographics> patientDemographicsList=patients.getPatient();
			List<UpdatePatientDemographics> patientDemographicsListChecked=new ArrayList<UpdatePatientDemographics>();
			PatientIdentifier patId=null;
			for(UpdatePatientDemographics updatePatDemo:patientDemographicsList)
			{
				try{
					validate(updatePatDemo);
				}
				catch(ValidationException ve)
				{
					//addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid UpdatePatientDemographics is required"));
					continue; 
				}
				/*if(updatePatDemo.getReasonForChange() == null || updatePatDemo.getReasonForChange().length() == 0)
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Reason For Change is required to update PatientDemographics"));
					continue; 
				}*/
				if(updatePatDemo.getPatientIdentifier()!=null)
				{
					patId=updatePatDemo.getPatientIdentifier();
				}
				else
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientIdentifier is required"));
					continue; 
				}
			    personPk = null; 
			    
			    try{
			    	personPk = ObjectLocator.personPKFromPatientIdentifier(callingUser,patId,objectMapService);
			    } 
			    catch (MultipleObjectsFoundException e) 
			    { 
			    	addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND,"Multiple Patients found"));
			    	continue;  
			    }


			    //if person not found, add an issue
			    if(personPk == null || personPk ==0)
			    {
			        addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND,"Patient not found for code: "+patId.getPatientId()+" OID: "+patId.getOID()));
			        continue;
			    }
			    if (logger.isDebugEnabled())
					logger.debug("PatientDemographicsServiceImpl personPk"+personPk);
				
	            PersonBean personBean = personAgent.getPersonDetails(personPk);
	            if (personBean == null)
	            {
	                addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND,"Patient not found for code: "+patId.getPatientId()+" OID: "+patId.getOID()+" PK : " + patId.getPK()));
	                continue;  
	            }
	            if (callingUser.getUserAccountId() == null || !callingUser.getUserAccountId().equals(personBean.getPersonAccount()))
	            {
	                addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND,"Patient not found for code: "+patId.getPatientId()+" OID: "+patId.getOID()+" PK : " + patId.getPK()));
	                continue;  
	            }
	            patId.setPK(personPk);
	            patId.setPatientId(personBean.getPersonPId());
	            OrganizationIdentifier orgId=new OrganizationIdentifier();
	            orgId.setPK(StringUtil.stringToInteger(personBean.getPersonLocation()));
	            orgId.setSiteName(siteAgent.getSiteDetails(StringUtil.stringToInteger(personBean.getPersonLocation())).getSiteName());
	            patId.setOrganizationId(orgId);
	            patId.setOID(objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PERSON, personPk).getOID());
	            //checking organization authorization
				PatFacilityDao patFacilityDao = patFacilityAgent.getPatientFacilities(personPk);
				int facilityCount = (patFacilityDao.getId()).size();
				boolean hasAccessToSite=false;
				Integer userSiteRight=null;
					 
				for (int patFacilityCount = 0; patFacilityCount < facilityCount ; patFacilityCount++)
				{
			    	 String sitePK = patFacilityDao.getPatientSite().get(patFacilityCount).toString();
			    	 			    	 
			    	if(patFacilityDao.getPatAccessFlag(patFacilityCount).equals("7")) //7 means Access is granted for this organization, 0 means revoked
					{
						userSiteRight = Integer.valueOf(userSiteAgent.getRightForUserSite(callingUser.getUserId().intValue(),StringUtil.stringToInteger(sitePK)));
						if (AbstractAuthModule.hasEditPermission(userSiteRight))
					      {
					        hasAccessToSite=true;
							break;
					      }
					 }
				 }
				
			     if(!hasAccessToSite)
			     {
			    	 addIssue(new Issue(IssueTypes.ORGANIZATION_AUTHORIZATION,"User not Authorized to edit Patient to this Organization. OID: " + 
					          patId.getOrganizationId().getOID() + " SiteAltID:" + patId.getOrganizationId().getSiteAltId() + " SiteName:" + patId.getOrganizationId().getSiteName()));
			    	 continue;
				 }
			     //specialty check
			     if(!StringUtil.isEmpty(personBean.getPersonSplAccess()))
			     {
			    	 if((callingUser.getUserCodelstSpl()==null)||(!callingUser.getUserCodelstSpl().equalsIgnoreCase(personBean.getPersonSplAccess())))
			    	 {
			    		 addIssue(new Issue(IssueTypes.SPECIALITY_ACCESS_AUTHORIZATION,"User is not authorized to edit patient data with pk : "+patId.getPK()
			    				 +" OID : "+patId.getOID()
			    				 +" patientId : "+patId.getPatientId()
			    				 +" siteName"+patId.getOrganizationId().getSiteName()));
			    		 continue;
						 //throw new AuthorizationException("User is not authorized to edit patient data");
			    	 }
			     }
			     updatePatDemo.setPatientIdentifier(patId);
			     patientDemographicsListChecked.add(updatePatDemo);
			}
			if(patientDemographicsListChecked.size()>0)
			{
				UpdatePatientDemographicsHelper updatePatientDemographicsHelper=new UpdatePatientDemographicsHelper();
				updatePatientDemographicsHelper.UpdateMPatientDemographics(patientDemographicsListChecked,parameters);
			}
			
		}
		catch (OperationException e) {
			/*try {
				this.sessionContext.getUserTransaction().setRollbackOnly();
			} catch (Exception ex) {
				throw new OperationException(ex);
			};*/
			if (logger.isDebugEnabled())
				logger.debug("PatientDemographicsServiceImpl create", e);
			throw new OperationRolledBackException(this.response.getIssues());
		} catch (Throwable t) {
			/*try {
				this.sessionContext.getUserTransaction().setRollbackOnly();
			} catch (Exception ex) {
				throw new OperationException(ex);
			}*/
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled())
				logger.debug("PatientDemographicsServiceImpl create", t);
			throw new OperationRolledBackException(this.response.getIssues());
		}
		if (response.getIssues() != null && response.getIssues().getIssue() != null 
				&& response.getIssues().getIssue().size() > 0) {
			throw new OperationRolledBackException(this.response.getIssues());
		}
		return response;
	}

	private void checkAuthForUpdatePatient(PatientIdentifier patId, UpdatePatientDemographics updatePatDemo,
			boolean includeResponse) 
	throws OperationException{
		//validating Patient identifier
		if(patId == null)
		{
			Issues issues = new Issues();
			issues.add(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientIdentifier is required"));
	        throw new OperationException(issues);
		}
		
		if (updatePatDemo == null ){
			Issues issues = new Issues();
			issues.add(new Issue(IssueTypes.DATA_VALIDATION, "No Input Data provided"));
	        throw new OperationException(issues);
		} 

		/*** The 2 main parameters are not null. So do further validations ***/
		if(((patId.getOID() == null || patId.getOID().length() == 0) && (patId.getPK() == null || patId.getPK()<=0)) 
				&& ((patId.getPatientId() == null || patId.getPatientId().length() == 0)
						|| (patId.getOrganizationId() == null) ) )
		{
			Issues issues = new Issues();
			issues.add(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientIdentifier with OID or PatientPK or PatientID and OrganizationIdentifier is required"));
	        throw new OperationException(issues);
		}
		
		if(StringUtil.isEmpty(updatePatDemo.getReasonForChange())){
			Issues issues = new Issues();
			issues.add(new Issue(IssueTypes.DATA_VALIDATION, "Reason For Change is required to update PatientDemographics"));
	        throw new OperationException(issues);
		}

		/*** Authorization - START **/
		//checking calling users group rights for manage patients
		GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
		int managePatients = authModule.getAppManagePatientsPrivileges().intValue();
		boolean hasEditPatientPermissions = GroupAuthModule.hasEditPermission(Integer.valueOf(managePatients));
		if (!hasEditPatientPermissions)
		{
			Issues issues = new Issues();
			issues.add(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User is not authorized to edit patient data"));
	        throw new OperationException(issues);
		}

		personPk = null; 
		try{
			personPk = ObjectLocator.personPKFromPatientIdentifier(callingUser, patId, objectMapService);
		} catch (MultipleObjectsFoundException e) {
			Issues issues = new Issues();
			issues.add(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, "Multiple Patients found"));
	        throw new OperationException(issues);
		}

		//if person not found, add an issue
		if(personPk == null || personPk ==0){
			Issues issues = new Issues();
			issues.add(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Patient not found for code: "+patId.getPatientId()+ " for given OrganizationId"));
	        throw new OperationException(issues);
		}

		PersonBean personBean = personAgent.getPersonDetails(personPk);
		if (personBean == null){
			Issues issues = new Issues();
			issues.add(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Patient not found for code: "+patId.getPatientId()+" OID: "+patId.getOID()));
	        throw new OperationException(issues);
		}
		if (callingUser.getUserAccountId() == null
				|| !callingUser.getUserAccountId().equals(personBean.getPersonAccount())){
			Issues issues = new Issues();
			issues.add(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Patient not found for OID: "+patId.getOID()));
	        throw new OperationException(issues);
		}

		//checking organization authorization
		PatFacilityDao patFacilityDao = patFacilityAgent.getPatientFacilities(personPk);
		int facilityCount = (patFacilityDao.getId()).size();
		boolean hasAccessToSite=false;
		Integer userSiteRight=null;

		for (int patFacilityCount = 0; patFacilityCount < facilityCount ; patFacilityCount++)
		{
			String sitePK = patFacilityDao.getPatientSite().get(patFacilityCount).toString();
			if(patFacilityDao.getPatAccessFlag(patFacilityCount).equals("7")) //7 means Access is granted for this organization, 0 means revoked
			{
				userSiteRight = Integer.valueOf(userSiteAgent.getRightForUserSite(callingUser.getUserId().intValue(),EJBUtil.stringToInteger(sitePK)));
				if (AbstractAuthModule.hasEditPermission(userSiteRight))
				{
					hasAccessToSite=true;
					break;
				}
			}
		}

		if(!hasAccessToSite)
		{
	        Issues issues = new Issues();
			issues.add(new Issue(IssueTypes.ORGANIZATION_AUTHORIZATION, "User not Authorized to edit Patient to this Organization. OID: " 
					+ patId.getOrganizationId().getOID() + " SiteAltID:" + patId.getOrganizationId().getSiteAltId() + 
					" SiteName:" + patId.getOrganizationId().getSiteName()));
	        throw new OperationException(issues);
		}
		//Raman Bug#11891 specialty check
		if(!StringUtil.isEmpty(personBean.getPersonSplAccess()))
		{
			if((callingUser.getUserCodelstSpl()==null)||(!callingUser.getUserCodelstSpl().equalsIgnoreCase(personBean.getPersonSplAccess())))
			{
				Issues issues = new Issues();
				issues.add(new Issue(IssueTypes.SPECIALITY_ACCESS_AUTHORIZATION, "User is not authorized to edit patient data"));
		        throw new OperationException(issues);
			}
		}

		if (includeResponse){
			this.response.addAction(new CompletedAction("AuthorizationVerified", CRUDAction.RETRIEVE));
		}
		/*** Authorization - END **/
	}

	public ResponseHolder checkAuthForUpdatePatient(PatientIdentifier patId, UpdatePatientDemographics updatePatDemo) 
	throws OperationException{

		checkAuthForUpdatePatient(patId, updatePatDemo, true) ;
		return this.response;
	}

	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		this.response = new ResponseHolder();
		this.callingUser = getLoggedInUser(this.sessionContext, this.userAgent);
		return ctx.proceed();
	}
}
