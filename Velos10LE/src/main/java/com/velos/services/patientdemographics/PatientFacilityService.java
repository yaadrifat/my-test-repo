package com.velos.services.patientdemographics;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientOrganization;
import com.velos.services.model.PatientOrganizationIdentifier;
import com.velos.services.model.PatientOrganizations;

@Remote
public interface PatientFacilityService
{
			ResponseHolder addPatientFacility(PatientIdentifier patId, PatientOrganization patOrg) throws OperationException;
			ResponseHolder updatePatientFacility(PatientIdentifier patIdentifier,
			OrganizationIdentifier orgID,
			PatientOrganizationIdentifier paramOrganizationIdentifier,
			PatientOrganization paramPatientOrganization) throws OperationException;
			PatientOrganizations getPatientFacilities(PatientIdentifier patientIdentifier) throws OperationException;
			PatientOrganizations getPatientOrganizations(Integer personPK) throws OperationException;
}
