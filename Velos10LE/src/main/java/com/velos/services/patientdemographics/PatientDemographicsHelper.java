/**
 * Created On Jun 28, 2011
 */
package com.velos.services.patientdemographics;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.SessionContext;

import oracle.sql.DATE;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.PatFacilityDao;
import com.velos.eres.business.common.PatientDao;
import com.velos.eres.business.patFacility.impl.PatFacilityBean;
import com.velos.eres.business.person.impl.PersonBean;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.patFacilityAgent.PatFacilityAgentRObj;
import com.velos.eres.service.personAgent.PersonAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.userSiteAgent.UserSiteAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.web.account.AccountJB;
import com.velos.eres.web.user.ConfigDetailsObject;
import com.velos.eres.web.user.ConfigFacade;
import com.velos.services.AbstractService;
import com.velos.services.CodeNotFoundException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.ValidationException;
import com.velos.services.authorization.AbstractAuthModule;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.AccessRights;
import com.velos.services.model.Code;
import com.velos.services.model.Patient;
import com.velos.services.model.PatientDemographics;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientOrganization;
import com.velos.services.model.PatientOrganizations;
import com.velos.services.model.Duration.TimeUnits;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ObjectLocator;

/**
 * @author Kanwaldeep
 *
 */
public class PatientDemographicsHelper {

	private static Logger logger = Logger.getLogger(PatientDemographicsHelper.class); 
	
	public boolean isMandatory(ConfigDetailsObject configDetails, String field)
	{
		boolean isMandatory = false;
		ArrayList fields = configDetails.getPcfField(); 
		ArrayList mandatory = configDetails.getPcfMandatory();
		
		if(fields == null || fields.indexOf(field) == -1)
		{
			
			//default configuration
			if(field.equals("patid")) return true;
			if(field.equals("dob")) return true; 
			if(field.equals("survivestat")) return true; 
			if(field.equals("org")) return true; 
		}else
		{
			if(mandatory.get(fields.indexOf(field)).toString().equals("1")) return true; 
		}
	
		
		return isMandatory; 
	}
	
	/**
	 * @param patient
	 * @throws ValidationException 
	 */
	private void validatePatient(PatientDemographics patient, UserBean callingUser, Map<String, Object> parameters) throws ValidationException {

		ConfigDetailsObject configDetails
		= ConfigFacade.getConfigFacade().populateObject(EJBUtil.stringToInteger(callingUser.getUserAccountId()), "patient");
		AccountJB acctJB = new AccountJB(); 
		acctJB.setAccId(EJBUtil.stringToInteger(callingUser.getUserAccountId())); 
		acctJB.getAccountDetails(); 
		
		boolean isAutoGenerateID = false; 
		
		if(acctJB.getAutoGenPatient() != null && acctJB.getAutoGenPatient().equals("1"))
		{
			isAutoGenerateID = true; 
		}
				
		List<Issue> validationIssues = new ArrayList<Issue>(); 
		if(configDetails != null ){
			if(isAutoGenerateID == false)
			{
				if(isMandatory(configDetails, "patid") &&( patient.getPatientCode() == null
						|| patient.getPatientCode().length() == 0))
				{				
					validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"Patient Code may not be null")); 
				}
			}

			if(isMandatory(configDetails, "survivestat") && (patient.getSurvivalStatus() == null || patient.getSurvivalStatus().getCode() == null))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"Survival Status code may not be null")); 
			}

			if(isMandatory(configDetails, "dod") && (patient.getDeathDate() == null))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"Death Date may not be null")); 
			}

			if(isMandatory(configDetails, "fname") && (patient.getFirstName() == null || patient.getFirstName().length() == 0))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"First Name may not be null")); 
			}

			if(isMandatory(configDetails, "midname") && (patient.getMiddleName() == null || patient.getMiddleName().length() == 0))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"Middle Name may not be null")); 
			}

			if(isMandatory(configDetails, "lname") && (patient.getLastName() == null || patient.getLastName().length() == 0))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"Last Name may not be null")); 
			}

			if(isMandatory(configDetails,"dob") && patient.getDateOfBirth() == null)
			{
				validationIssues.add(new Issue( IssueTypes.DATA_VALIDATION, "Date of Birth may not be null")); 
			}

			if(isMandatory(configDetails,"facilityID") && patient.getPatFacilityId() == null)
			{
				validationIssues.add(new Issue( IssueTypes.DATA_VALIDATION, "Patient FacilityID may not be null")); 
			}

			if(isMandatory(configDetails, "gender") && (patient.getGender() == null || patient.getGender().getCode()== null))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"Gender code may not be null")); 
			}


			if(isMandatory(configDetails, "email") && (patient.geteMail() == null || patient.geteMail().length() == 0))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"Patient Email may not be null")); 
			}

			if(isMandatory(configDetails, "ethnicity") && (patient.getEthnicity() == null || patient.getEthnicity().getCode() == null))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"Ethnicity.code may not be null")); 
			}
			if(isMandatory(configDetails, "race") && (patient.getRace() == null || patient.getRace().getCode() == null))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"Race code may not be null")); 
			}



			if(isMandatory(configDetails, "address1") && (patient.getAddress1() == null || patient.getAddress1().length() == 0))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"Address1 may not be null")); 
			}
			if(isMandatory(configDetails, "address2") && (patient.getAddress2() == null || patient.getAddress2().length() == 0))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"Address2 may not be null")); 
			}	

			if(isMandatory(configDetails, "city") && (patient.getCity() == null || patient.getCity().length() == 0))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"City may not be null")); 
			}
			if(isMandatory(configDetails, "state") && (patient.getState() == null || patient.getState().length() == 0))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"State may not be null")); 
			}
			if(isMandatory(configDetails, "county") && (patient.getCounty() == null || patient.getCounty().length() == 0))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"LastName may not be null")); 
			}
			if(isMandatory(configDetails, "zipcode") && (patient.getZipCode() == null || patient.getZipCode().length() == 0))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"ZipCode may not be null")); 
			}

			if(isMandatory(configDetails, "country") && (patient.getCountry() == null || patient.getCountry().length() == 0))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"Country may not be null")); 
			}

			if(isMandatory(configDetails, "hphone") && (patient.getHomePhone() == null || patient.getHomePhone().length() == 0))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"Home Phone may not be null")); 
			}


			if(isMandatory(configDetails, "wphone") && (patient.getWorkPhone() == null || patient.getWorkPhone().length() == 0))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"Work Phone may not be null")); 
			}
			
		/*	if(isMandatory(configDetails, "org") && (patient.getOrganization() == null || ((patient.getOrganization().getOID()== null || patient.getOrganization().getOID().length() == 0 )
					&&(patient.getOrganization().getSiteAltId() == null || patient.getOrganization().getSiteAltId().length() == 0) 
					&& (patient.getOrganization().getSiteName() == null || patient.getOrganization().getSiteName().length() == 0))))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"Organization may not be null")); 
			}*/
			if(isMandatory(configDetails, "org") && (patient.getOrganization() == null || (((patient.getOrganization().getOID()== null || patient.getOrganization().getOID().length() == 0) && (patient.getOrganization().getPK()== null ||patient.getOrganization().getPK()<=0 ))
					&&(patient.getOrganization().getSiteAltId() == null || patient.getOrganization().getSiteAltId().length() == 0) 
					&& (patient.getOrganization().getSiteName() == null || patient.getOrganization().getSiteName().length() == 0))))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"Organization may not be null")); 
			}
			



			if (validationIssues.size() > 0){
				((ResponseHolder) parameters.get("ResponseHolder"))
				.getIssues().addAll(validationIssues); 
				
			//	response.getIssues().addAll(validationIssues);
				if (logger.isDebugEnabled()) logger.debug("Validation Issues found for PatientDemographics");
				throw new ValidationException();
			}
		}
	}
	
	public int createPatient(Patient patient, Map<String, Object> parameters) throws OperationException
	{
		UserBean callingUser = (UserBean) parameters.get("callingUser"); 
		validatePatient(patient.getPatientDemographics(), callingUser ,parameters); 
		SessionContext sessionContext = (SessionContext) parameters.get("sessionContext"); 
		ObjectMapService objectMapService = (ObjectMapService) parameters.get("objectMapService"); 
		UserAgentRObj userAgent = (UserAgentRObj) parameters.get("userAgent"); 		

		PatientOrganization defaultOrganization = getDefaultOrganization(patient.getPatientOrganizations(), parameters); 
		Integer patientPK = createPatientDemographics(patient.getPatientDemographics(), defaultOrganization, parameters); 
		Integer defaultOrganizationID = null; 
		if(defaultOrganization != null)
		{
			// remove default organization as it is already added by trigger on Person table 
			//Bug Fix 6680
		
			try {
				defaultOrganizationID = ObjectLocator.sitePKFromIdentifier(callingUser, defaultOrganization.getOrganizationID(), sessionContext, objectMapService);
			}catch(MultipleObjectsFoundException moe)
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, "Found multiple organizations for OID:"
						+ defaultOrganization.getOrganizationID().getOID() + " siteAltID:" + defaultOrganization.getOrganizationID().getSiteAltId() + " siteName:" + defaultOrganization.getOrganizationID().getSiteName()));
				
				throw new OperationException();
			}
			
			patient.getPatientOrganizations().getOrganization().remove(defaultOrganization); 
			PatientOrganization orgFromDemographics = new PatientOrganization(); 
			orgFromDemographics.setAccess(AccessRights.GRANTED); 
			orgFromDemographics.setOrganizationID(patient.getPatientDemographics().getOrganization()); 
			orgFromDemographics.setFacilityID(patient.getPatientDemographics().getPatFacilityId()); 
			orgFromDemographics.setRegistrationDate(patient.getPatientDemographics().getRegistrationDate()); 
			orgFromDemographics.setDefault(false);
			patient.getPatientOrganizations().getOrganization().add(orgFromDemographics); 
		}else
		{
			try {
				defaultOrganizationID = ObjectLocator.sitePKFromIdentifier(callingUser, patient.getPatientDemographics().getOrganization(), sessionContext, objectMapService);
			}catch(MultipleObjectsFoundException moe)
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, "Found multiple organizations for OID:"
						+ patient.getPatientDemographics().getOrganization().getOID() + " siteAltID:" + patient.getPatientDemographics().getOrganization().getSiteAltId() + " siteName:" + patient.getPatientDemographics().getOrganization().getSiteName()));
				
				throw new OperationException();
			}
		}
		if(patient.getPatientOrganizations() != null)
		{

			addPatientOrganizations(patient.getPatientOrganizations(), false, patientPK, parameters, defaultOrganizationID); 
		}else
		{
			List<Integer> patOrg = new ArrayList<Integer>(); 
			patOrg.add(defaultOrganizationID); 
			parameters.put("patientOrganizations", patOrg);
		
		}

		return patientPK; 
	}
/**
 * getDefaultOrganization - Checks if PatientOrganizations list has any organization set up a default. 
 */
	private PatientOrganization getDefaultOrganization(PatientOrganizations patientOrganizations, Map<String, Object> parameters) throws OperationException
	{
		PatientOrganization defaultOrg = null;
		if(patientOrganizations != null)
		{
			List<PatientOrganization> organization = patientOrganizations.getOrganization(); 
			boolean hasDefault = false; 

			if(organization != null)
			{
				for(PatientOrganization org: organization)
				{
					if(hasDefault && org.isDefault()) 
					{
						((ResponseHolder) parameters.get("ResponseHolder"))
						.getIssues().add(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, "Found multiple organizations as Default Organization only single Organization can be default"));
						throw new OperationException(); 
					}
					if(org.isDefault())
					{
						hasDefault = true; 
						defaultOrg = org; 
					}
				}
			}
		}
		return defaultOrg; 


	}

	/**
	 * @param patientOrganizations
	 * @throws OperationException 
	 */
	private void addPatientOrganizations(
			PatientOrganizations patientOrganizations, boolean isExistingPatient, Integer patientPK, Map<String, Object>parameters, Integer defaultOrgID) throws OperationException {
		
		List<PatientOrganization> organization = patientOrganizations.getOrganization(); 
		
		List<Integer> existingOrganization = new ArrayList<Integer>(); 
		
		PatFacilityAgentRObj patFacilityAgent = (PatFacilityAgentRObj) parameters.get("patFacilityAgent"); 
		
		if(isExistingPatient)
		{
			PatFacilityDao facilityDAO = patFacilityAgent.getPatientFacilities(patientPK); 
			for(int i = 0; i < facilityDAO.getPatientSite().size() ; i++)
			{				
				existingOrganization.add(EJBUtil.stringToInteger(facilityDAO.getPatientSite().get(i).toString())); 
			}
			
			
		}
		
		if(defaultOrgID != null && defaultOrgID != 0)
		{
			existingOrganization.add(defaultOrgID); 
		}
		for(PatientOrganization org: organization)
		{
			
			int orgPK = 0; 
			UserBean callingUser = (UserBean) parameters.get("callingUser"); 
			SessionContext sessionContext = (SessionContext) parameters.get("sessionContext"); 
			ObjectMapService objectMapService = (ObjectMapService) parameters.get("objectMapService"); 
			UserAgentRObj userAgent = (UserAgentRObj) parameters.get("userAgent"); 		
			try {
				//Navneet
				if(org.getOrganizationID() == null || (((org.getOrganizationID().getOID()== null || org.getOrganizationID().getOID().length() == 0) && (org.getOrganizationID().getPK()== null ||org.getOrganizationID().getPK()<=0 ))
						&&(org.getOrganizationID().getSiteAltId() == null || org.getOrganizationID().getSiteAltId().length() == 0) 
						&& (org.getOrganizationID().getSiteName() == null || org.getOrganizationID().getSiteName().length() == 0)))
				{
					((ResponseHolder) parameters.get("ResponseHolder"))
					.getIssues().add(new Issue(IssueTypes.DATA_VALIDATION, "Organization may not be null or 0"));
					throw new OperationException(); 
					
				}	
				orgPK = ObjectLocator.sitePKFromIdentifier(callingUser, org.getOrganizationID(), sessionContext, objectMapService);
				}catch(MultipleObjectsFoundException moe)
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, "Found multiple organizations for OID:"
						+ org.getOrganizationID().getOID() + " siteAltID:" + org.getOrganizationID().getSiteAltId() + " siteName:" + org.getOrganizationID().getSiteName() + " sitePk :"+org.getOrganizationID().getPK()));
				
				throw new OperationException();
			}
			if(orgPK == 0)
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "Organization not found for OID:"
						+ org.getOrganizationID().getOID() + " siteAltID:" + org.getOrganizationID().getSiteAltId() + " siteName:" + org.getOrganizationID().getSiteName()+ " sitePk :"+org.getOrganizationID().getPK()));

				throw new OperationException(); 
			}
			//BUG Fix 6539 - Kanwal
			UserSiteAgentRObj userSiteAgent = (UserSiteAgentRObj) parameters.get("userSiteAgent"); 
			Integer userSiteRight = userSiteAgent.getRightForUserSite(callingUser.getUserId(),orgPK); 
			if(!AbstractAuthModule.hasNewPermission(userSiteRight))
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(
								IssueTypes.ORGANIZATION_AUTHORIZATION, 
								"User not Authorized to add Patient to this Organization "
									+ org.getOrganizationID().getOID() + " siteAltID:" + org.getOrganizationID().getSiteAltId() + " siteName:" + org.getOrganizationID().getSiteName()+ " sitePk :"+org.getOrganizationID().getPK()));
			throw new OperationException();
			}
			
			if(existingOrganization.contains(orgPK))
			{
				if(isExistingPatient)
				{
					((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.PATIENT_ALREADY_REGISTERED_TO_THIS_ORGANIZATION, "Patient is already registered to this Organization"));
					throw new OperationException();  
				}else
				{
					((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.DUPLICATE_ORGANIZATION, "Duplicate organization found in request"));
					throw new OperationException(); 
				}

			}else
			{
				existingOrganization.add(orgPK); 
				PatFacilityBean facilityBean = new PatFacilityBean(); 
				facilityBean.setIPAdd(AbstractService.IP_ADDRESS_FIELD_VALUE); 
				facilityBean.setIsDefault(org.isDefault()? "1":"0"); 
				facilityBean.setCreator(EJBUtil.integerToString(callingUser.getUserId())); 
				if(org.getAccess() != null )
				{
					facilityBean.setPatAccessFlag(org.getAccess().equals(AccessRights.GRANTED)?"7" :"0"); 
				}else
				{
					//BugFix # 6529  
					((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
							new Issue(
									IssueTypes.DATA_VALIDATION, 
									"Access can be either GRANTED or REVOKED"));
				throw new OperationException();
				}
				facilityBean.setPatientFacilityId(org.getFacilityID()); 

				String registeredBy = ""; 
				try
				{
					if(org.getProvider() != null)
					{
						UserBean bean = ObjectLocator.userBeanFromIdentifier(callingUser, org.getProvider(), userAgent, sessionContext, objectMapService); 
						if(bean == null)
						{
							((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.USER_NOT_FOUND, "Provider not found for OID:"
									+ org.getProvider().getOID() + " FirstName:" + org.getProvider().getFirstName() + " lastName:" + org.getProvider().getLastName() + " UserLoginName:" + org.getProvider().getUserLoginName()));
							throw new OperationException();
						}
						registeredBy =EJBUtil.integerToString( bean.getUserId()); 					
					}
				}catch(MultipleObjectsFoundException mce)
				{
					((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, "Found multiple Users for OID:"
							+ org.getProvider().getOID() + " FirstName:" + org.getProvider().getFirstName() + " lastName:" + org.getProvider().getLastName() + " UserLoginName:" + org.getProvider().getUserLoginName()));
					throw new OperationException();
				}



				facilityBean.setRegisteredBy(registeredBy); 


				facilityBean.setRegisteredByOther(org.getOtherProvider()); 
				facilityBean.setRegDate(org.getRegistrationDate()); 
				facilityBean.setPatientPK(EJBUtil.integerToString(patientPK)); 
				facilityBean.setPatientSite(EJBUtil.integerToString(orgPK));

				if(org.getSpecialtyAccess() != null && org.getSpecialtyAccess().getSpeciality() != null)
				{
					List<Code> specialties = org.getSpecialtyAccess().getSpeciality();
					StringBuffer specialtyStr = new StringBuffer(); 

					for(int x = 0; x < specialties.size();  x++)
					{
						if(x != 0) specialtyStr.append(",");
						try{
							specialtyStr.append(AbstractService.dereferenceCodeStr(specialties.get(x), CodeCache.CODE_TYPE_PRIMARY_SPECIALTY, callingUser)); 
						}catch(CodeNotFoundException cde)
						{
							((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
									new Issue(
											IssueTypes.DATA_VALIDATION, 
											"Specialty Code Not Found: " + specialties.get(x).getCode()));
							throw new OperationException();
						}
					}
					facilityBean.setPatSpecialtylAccess(specialtyStr.toString()); 
				}					

				int saved = patFacilityAgent.setPatFacilityDetails(facilityBean);
				if(saved == 0)
				{

					((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
							new Issue(
									IssueTypes.PATIENT_ERROR_ADD_ORGANIZATION)); 
					throw new OperationException(); 
				}
			}

		}
		
		parameters.put("patientOrganizations", existingOrganization); 

	}

	/**
	 * @param patient
	 * @return
	 * @throws OperationException 
	 */
	private Integer createPatientDemographics(PatientDemographics patient, PatientOrganization defaultOrganization, Map<String, Object> parameters) throws OperationException {
		
		Integer organizationID = 0;
		UserBean callingUser = (UserBean) parameters.get("callingUser"); 
		SessionContext sessionContext = (SessionContext) parameters.get("sessionContext"); 
		ObjectMapService objectMapService = (ObjectMapService)parameters.get("objectMapService"); 
		if(patient.getOrganization() != null)
		{
			try {
				organizationID = ObjectLocator.sitePKFromIdentifier(callingUser, patient.getOrganization(), sessionContext, objectMapService);
			} catch (MultipleObjectsFoundException e) {
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, "Found multiple organizations for OID:"
						+ patient.getOrganization().getOID() + " siteAltID:" + patient.getOrganization().getSiteAltId() + " siteName:" + patient.getOrganization().getSiteName()+ " sitePK:"+patient.getOrganization().getPK()));
				throw new OperationException();
			} 
			
			if(organizationID == null || organizationID == 0)
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
							new Issue(
									IssueTypes.ORGANIZATION_NOT_FOUND, 
									"Organization not found for OID:"
										+ patient.getOrganization().getOID() + " siteAltID:" + patient.getOrganization().getSiteAltId() + " siteName:" + patient.getOrganization().getSiteName()+ " sitePk :"+patient.getOrganization().getPK()));
				throw new OperationException();
			}
			//BUG Fix 6539 - Kanwal
			UserSiteAgentRObj userSiteAgent = (UserSiteAgentRObj) parameters.get("userSiteAgent"); 
			Integer userSiteRight = userSiteAgent.getRightForUserSite(callingUser.getUserId(),organizationID); 
			if(!AbstractAuthModule.hasNewPermission(userSiteRight))
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(
								IssueTypes.ORGANIZATION_AUTHORIZATION, 
								"User not Authorized to add Patient to this Organization "
									+ patient.getOrganization().getOID() + " siteAltID:" + patient.getOrganization().getSiteAltId() + " siteName:" + patient.getOrganization().getSiteName()+ " sitePk :"+patient.getOrganization().getPK()));
			throw new OperationException();
			}
		}
		
		
	
		PersonBean personBean = new PersonBean();
		if(patient.getDateOfBirth() != null)
		{
			personBean.setPersonDb(patient.getDateOfBirth()); 
		}
		java.util.Date date = new java.util.Date();
	if(patient.getDateOfBirth().after(date))
		//Bug Fixed #6691
	{
		Issue issue = new Issue(IssueTypes.DATA_VALIDATION, "Date of birth should not be after present day"); 
		((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
		throw new OperationException();
	}
		// change according to default Organization in PatientOrganizations if none is default then what is sent in patient object will be saved
		if(defaultOrganization != null)
		{
			personBean.setPatientFacilityId(defaultOrganization.getFacilityID()); 
			personBean.setPersonRegDt(defaultOrganization.getRegistrationDate()); 
			try {
				organizationID = ObjectLocator.sitePKFromIdentifier(callingUser, defaultOrganization.getOrganizationID(), sessionContext, objectMapService);
			} catch (MultipleObjectsFoundException e) {
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, "Found multiple organizations for OID:"
						+ patient.getOrganization().getOID() + " siteAltID:" + patient.getOrganization().getSiteAltId() + " siteName:" + patient.getOrganization().getSiteName()));
				throw new OperationException();
			} 
			
			if(organizationID == null || organizationID == 0)
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
							new Issue(
									IssueTypes.ORGANIZATION_NOT_FOUND, 
									"Organization not found for OID:"
										+ defaultOrganization.getOrganizationID().getOID() + " siteAltID:" + defaultOrganization.getOrganizationID().getSiteAltId() + " siteName:" + defaultOrganization.getOrganizationID().getSiteName()+ " sitePk :"+defaultOrganization.getOrganizationID().getPK()));
				throw new OperationException();
			}
			
			UserSiteAgentRObj userSiteAgent = (UserSiteAgentRObj) parameters.get("userSiteAgent"); 
			Integer userSiteRight = userSiteAgent.getRightForUserSite(callingUser.getUserId(),organizationID); 
			if(!AbstractAuthModule.hasNewPermission(userSiteRight))
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(
								IssueTypes.ORGANIZATION_AUTHORIZATION, 
								"User not Authorized to add Patient to this Organization "
									+ defaultOrganization.getOrganizationID().getOID() + " siteAltID:" + defaultOrganization.getOrganizationID().getSiteAltId() + " siteName:" + defaultOrganization.getOrganizationID().getSiteName()+ " sitePk :"+defaultOrganization.getOrganizationID().getPK()));
			throw new OperationException();
			}
			personBean.setPersonLocation(EJBUtil.integerToString(organizationID));
		}else{
			personBean.setPatientFacilityId((patient.getPatFacilityId() == null || patient.getPatFacilityId().length() == 0)? patient.getPatientCode():patient.getPatFacilityId()); 
			personBean.setPersonRegDt(patient.getRegistrationDate()); 
			personBean.setPersonLocation(EJBUtil.integerToString(organizationID)); 
		}
		
		PatientDao patientDao = new PatientDao(); 
		boolean isPatientIDExists = patientDao.patientCodeExists(patient.getPatientCode(), EJBUtil.integerToString(organizationID)); 
		if(isPatientIDExists)
		{
			((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
					new Issue(
							IssueTypes.PATIENT_ID_ALREADY_EXISTS, 
							"Patient ID already exists: " + patient.getPatientCode()));
			throw new OperationException();
		}
// Bug Fix 6754 - removing this validation as it is just warning message in eResearch 		
//		if(patientDao.patientCheck(patient.getFirstName(), patient.getLastName(), personBean.getPersonDob(), callingUser.getUserAccountId(), patient.getPatientCode()))
//		{
//			((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
//					new Issue(
//							IssueTypes.PATIENT_ALREADY_EXISTS, 
//							"Patient already exists for  First Name:" + patient.getFirstName()
//							+ " Last Name:" + patient.getLastName() + " DOB:" + personBean.getPersonDob()));
//			throw new OperationException();
//			
//		}
		
		personBean.setCreator(EJBUtil.integerToString(callingUser.getUserId())); 
		personBean.setIpAdd(AbstractService.IP_ADDRESS_FIELD_VALUE); 
		personBean.setPersonAccount(callingUser.getUserAccountId()); 
		
		personBean.setPersonPId(patient.getPatientCode()); 
		
		
		if(patient.getSurvivalStatus() != null && patient.getSurvivalStatus().getCode() != null)
		{
			try {
				
				String survivalStatusCode = AbstractService.dereferenceCodeStr(patient.getSurvivalStatus(), CodeCache.CODE_TYPE_PATIENT_SURVIVAl_STATUS, callingUser); 
				
				personBean.setPersonStatus(survivalStatusCode);
				CodeCache codeCache = CodeCache.getInstance();
				patient.setSurvivalStatus(codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_PATIENT_SURVIVAl_STATUS, survivalStatusCode, EJBUtil.stringToInteger(callingUser.getUserAccountId()))); 
			} catch (CodeNotFoundException e) {
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(
								IssueTypes.DATA_VALIDATION, 
								"Survival Status Code Not Found: " + patient.getSurvivalStatus().getCode()));
				throw new OperationException();
			}
		}
		//Bug Fix 6586
		if (patient.getSurvivalStatus().getCode().equals("A"))
		{
			if(patient.getDeathDate() != null)
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(IssueTypes.DATA_VALIDATION, "Patient's survival status is given as \"" + patient.getSurvivalStatus().getDescription()+ "\" along with Date of Death")); 
				throw new OperationException(); 
			}
				
			
		}
		
		if(patient.getSurvivalStatus().getCode().equals("dead"))
		{
			if(patient.getDeathDate() == null)
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(IssueTypes.DATA_VALIDATION, "DeathDate is required when Patient's survival status is given as \" "+ patient.getSurvivalStatus().getDescription()+" \"")); 
				throw new OperationException(); 
			}
			
			if(patient.getDeathDate().before(patient.getDateOfBirth()))
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(IssueTypes.DATA_VALIDATION, "DeathDate cannot be before Date of Birth")); 
				throw new OperationException(); 
				
			}
			personBean.setPersonDeathDt(patient.getDeathDate()); 
		}
		
			
		
		personBean.setPersonFname(patient.getFirstName()); 
		personBean.setPersonMname(patient.getMiddleName()); 
		personBean.setPersonLname(patient.getLastName());
		

		
		if(patient.getGender() != null && patient.getGender().getCode() != null)
		{
			try{
				personBean.setPersonGender(AbstractService.dereferenceCodeStr(patient.getGender(), CodeCache.CODE_TYPE_GENDER, callingUser));
			}catch(CodeNotFoundException e)
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(
								IssueTypes.DATA_VALIDATION, 
								"Gender Code Not Found: " + patient.getGender().getCode()));
				throw new OperationException();
			}
		}
		
		personBean.setPersonEmail(patient.geteMail()); 
				
		
		if(patient.getEthnicity() != null && patient.getEthnicity().getCode() != null)
		{
			try{
				personBean.setPersonEthnicity(AbstractService.dereferenceCodeStr(patient.getEthnicity(), CodeCache.CODE_TYPE_ETHNICITY, callingUser)); 
			}catch(CodeNotFoundException e)
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(
							IssueTypes.DATA_VALIDATION, 
							"Ethnicity Code Not Found: " + patient.getEthnicity().getCode()));
				throw new OperationException();
			}
			
		}
		
		if(patient.getRace() != null && patient.getRace().getCode() != null)
		{
			try
			{
				personBean.setPersonRace(AbstractService.dereferenceCodeStr(patient.getRace(), CodeCache.CODE_TYPE_RACE, callingUser));
			}catch(CodeNotFoundException e)
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(
							IssueTypes.DATA_VALIDATION, 
							"Race Code Not Found: " + patient.getRace().getCode()));
				throw new OperationException();
			}
		}
		
		if (patient.getMaritalStatus() != null && patient.getMaritalStatus().getCode() != null) {
			try {
				personBean.setPersonMarital(AbstractService.dereferenceCodeStr(
						patient.getMaritalStatus(), CodeCache.CODE_TYPE_MARITAL_STATUS, callingUser));
			} catch (CodeNotFoundException e) {
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(
							IssueTypes.DATA_VALIDATION, 
							"Marital Status Code Not Found: " + patient.getMaritalStatus().getCode()));
				throw new OperationException();
			}
		}
		
		personBean.setPersonAddress1(patient.getAddress1()); 
		personBean.setPersonAddress2(patient.getAddress2()); 
		personBean.setPersonCity(patient.getCity()); 
		personBean.setPersonState(patient.getState()); 
		personBean.setPersonCounty(patient.getCounty()); 
		personBean.setPersonZip(patient.getZipCode()); 
		personBean.setPersonCountry(patient.getCountry()); 
		
		personBean.setPersonBphone(patient.getWorkPhone()); 
		personBean.setPersonHphone(patient.getHomePhone());
		
		personBean.setPersonNotes(""); 
		
	
		PersonAgentRObj personAgent = (PersonAgentRObj) parameters.get("personAgent"); 
		
		PersonBean personBean2 = null; 
		personBean2 = personAgent.setPersonDetails(personBean); 
		personAgent.flush();
		if(personBean2 == null)
		{
			((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
					new Issue(
							IssueTypes.PATIENT_ERROR_CREATE_DEMOGRAPHICS)); 
			throw new OperationException(); 
						
		}
		return personBean2.getPersonPKId();
	}
	
	

}
