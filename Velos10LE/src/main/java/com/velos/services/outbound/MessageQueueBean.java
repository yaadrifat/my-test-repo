package com.velos.services.outbound;

import com.velos.services.CRUDAction;

/**
 * @author Kanwaldeep
 */
public class MessageQueueBean {
	
	private Integer messagePK; 
	private String tableName; 
	private Integer tablePK; 
	private Integer processedFlag; 
	private String module; 
	private CRUDAction action; 
	private String fieldsUpdated; 
	private String rootFK; 
	private String rootTableName; 
	private Integer accountFK;
	private String createdOn; 
	
	public Integer getMessagePK() {
		return messagePK;
	}
	public void setMessagePK(Integer messagePK) {
		this.messagePK = messagePK;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public Integer getTablePK() {
		return tablePK;
	}
	public void setTablePK(Integer tablePK) {
		this.tablePK = tablePK;
	}
	public Integer getProcessedFlag() {
		return processedFlag;
	}
	public void setProcessedFlag(Integer processedFlag) {
		this.processedFlag = processedFlag;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public CRUDAction getAction() {
		return action;
	}
	public void setAction(String action) {
		if(action.equalsIgnoreCase(MessagingConstants.MESSAGE_ACTION_INSERT))
		{
			this.action = CRUDAction.CREATE; 
			return; 
		}
		if(action.equalsIgnoreCase(MessagingConstants.MESSAGE_ACTION_DELETE))
		{
			this.action = CRUDAction.REMOVE;
			return; 
		}
		//AS only three actions are supported in OutBound Messaging.
		this.action = CRUDAction.UPDATE; 
		
	}
	public String getFieldsUpdated() {
		return fieldsUpdated;
	}
	public void setFieldsUpdated(String fieldsUpdated) {
		this.fieldsUpdated = fieldsUpdated;
	}
	public String getRootFK() {
		return rootFK;
	}
	public void setRootFK(String rootFK) {
		this.rootFK = rootFK;
	}
	public String getRootTableName() {
		return rootTableName;
	}
	public void setRootTableName(String rootTableName) {
		this.rootTableName = rootTableName;
	}
	public Integer getAccountFK() {
		return accountFK;
	}
	public void setAccountFK(Integer accountFK) {
		this.accountFK = accountFK;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the createdOn
	 */
	public String getCreatedOn() {
		return createdOn;
	}
	
	
	

}
