package com.velos.services.outbound;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;

/**
 * MessagingDaemon sets TimerTasks for outbound messaging
 * 
 * @author Kanwaldeep
 *
 */
public class MessagingDaemon extends TimerTask {
	private static  Logger logger = Logger.getLogger(MessagingDaemon.class); 
	
	private int accountNumber; 
	
	private static Timer timer = new Timer(); 
	
	public int getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}
	@Override
	public void run() {	
		logger.debug("Entered run method of Daemon"); 
		MessageProcessor.processMessages(accountNumber); 
	}
	public static void startDeamon() {
		
		try{
			timer = new Timer(); 
			MessagingAccountDAO dao = new MessagingAccountDAO(); 
			Map<Integer, Integer> accountSettingsMap = dao.getDataTimerTaskSetup(); 
		
			Iterator<Integer> accountItr = accountSettingsMap.keySet().iterator(); 
		
			while(accountItr.hasNext())
			{
				int accountNumber = accountItr.next(); 
				MessagingDaemon daemon = new MessagingDaemon(); 
				daemon.setAccountNumber(accountNumber); 
				long scheduleInterval =(accountSettingsMap.get(accountNumber))*1000;
				logger.info("Scheduling timer for account "+accountNumber +" and to run at regular intervals of " + scheduleInterval  + "ms");
				try{
					//Schedule after delay of 1 minute to give server time to start up before request getting fired.
					timer.schedule(daemon,60000, scheduleInterval);
				}catch(Exception e)
				{
					logger.info(e); 
					e.printStackTrace(); 
				}
			
			}	
		}catch(Exception e)
		{
			logger.fatal("Unable to Start Daemon for OutBound Messaging : Please fix problem and re start this service ", e); 
		}

		
	}
	
	public static void stopDeamon()
	{
		timer.cancel(); 
	}
	


	
}