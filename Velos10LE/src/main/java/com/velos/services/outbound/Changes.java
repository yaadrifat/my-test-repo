/**
 * Created On Mar 16, 2011
 */
package com.velos.services.outbound;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;

import com.velos.services.model.ParentIdentifier;

/**
 * @author Kanwaldeep
 *
 */
@XmlRootElement
public class Changes implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1564850541564654953L;
	private Set<Change> Change = new HashSet<Change>();
	private ParentIdentifier parentIdentifier; 
	
	/**
	 * @param {@link com.velos.services.outbound.ParentIdentifier}  
	 */
	public void setParentIdentifier(ParentIdentifier parentIdentifier)
	{
		this.parentIdentifier = parentIdentifier; 
	}
	
	/**
	 * @return {@link com.velos.services.outbound.ParentIdentifier} 
	 */
	public ParentIdentifier getParentIdentifier()
	{
		return parentIdentifier; 
	}
	
	/**
	 * @return Set of {@link com.velos.services.outbound.Change} 
	 */
	public Set<Change> getChange() {
		return Change;
	}

	/**
	 * @param Set of {@link com.velos.services.outbound.Change} 
	 */
	public void setChange(Set<Change> change) {
		Change = change;
	}	
	
}
