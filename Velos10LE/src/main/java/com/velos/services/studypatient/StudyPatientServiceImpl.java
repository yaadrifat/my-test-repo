package com.velos.services.studypatient;

import static javax.ejb.TransactionAttributeType.REQUIRED;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;



import com.velos.epat.audit.service.AuditRowEpatAgent;
import com.velos.eres.audit.service.AuditRowEresAgent;
import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.PatFacilityDao;
import com.velos.eres.business.common.StudyDao;
import com.velos.eres.business.common.StudySiteDao;
import com.velos.eres.business.common.UserSiteDao;
import com.velos.eres.business.patProt.impl.PatProtBean;
import com.velos.eres.business.patStudyStat.impl.PatStudyStatBean;
import com.velos.eres.business.person.impl.PersonBean;
import com.velos.eres.business.site.impl.SiteBean;
import com.velos.eres.business.study.impl.StudyBean;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.patFacilityAgent.PatFacilityAgentRObj;
import com.velos.eres.service.patProtAgent.PatProtAgentRObj;
import com.velos.eres.service.patStudyStatAgent.PatStudyStatAgentRObj;
import com.velos.eres.service.personAgent.PersonAgentRObj;
import com.velos.eres.service.siteAgent.SiteAgentRObj;
import com.velos.eres.service.studyAgent.StudyAgent;
import com.velos.eres.service.studySiteAgent.StudySiteAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.userSiteAgent.UserSiteAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.CRUDAction;
import com.velos.services.CompletedAction;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.authorization.TeamAuthModule;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.Code;
import com.velos.services.model.CreateMultiPatientStudyStatus;
import com.velos.services.model.CreateMultiPatientStudyStatuses;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.Patient;
import com.velos.services.model.PatientEnrollmentDetails;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientStudyStatusIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatient;
import com.velos.services.model.StudyPatientStatuses;
import com.velos.services.model.UpdateMPatientStudyStatuses;
import com.velos.services.model.UpdateMStudyPatientStatus;
import com.velos.services.model.UserIdentifier;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ObjectLocator;
/**
 * Implementation for StuyPatient Service 
 * @author Virendra
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@Remote(StudyPatientService.class)
public class StudyPatientServiceImpl 
extends AbstractService 
implements StudyPatientService{

@EJB
private ObjectMapService objectMapService;
@EJB
GrpRightsAgentRObj groupRightsAgent;
@EJB
private UserAgentRObj userAgent;
@Resource 
private SessionContext sessionContext;
@EJB
private StudySiteAgentRObj studySiteAgent;
@EJB
private PatStudyStatAgentRObj patStudyStatAgent; 
@EJB
private PersonAgentRObj personAgent; 
@EJB
private SiteAgentRObj siteAgent;
@EJB
private PatProtAgentRObj patProtAgent;
@EJB
private UserSiteAgentRObj userSiteAgent;
@EJB
private PatFacilityAgentRObj patFacilityAgent;
@EJB
private StudyAgent studyAgent;

@EJB
private AuditRowEresAgent auditRowEresAgnet;

@EJB
private AuditRowEpatAgent auditRowEpatAgent;

private static Logger logger = Logger.getLogger(StudyPatientServiceImpl.class.getName());
	/**
	 * getStudyPatients with param StudyIdentifier
	 */
	public List<StudyPatient> getStudyPatients(StudyIdentifier studyIdentifier) throws OperationException {
		
		try{
			
			if(studyIdentifier==null || (StringUtil.isEmpty(studyIdentifier.getOID()) 
					&& (studyIdentifier.getPK()==null || studyIdentifier.getPK()<=0) 
					&& StringUtil.isEmpty(studyIdentifier.getStudyNumber())))
			{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to addStudyPatientStatus"));
			throw new OperationException();
			}
			//call to ObjectLocator for StudyPk
			Integer studyPK = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier, objectMapService);
			objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPK);
			
			if(studyPK == null || studyPK ==0 || !StudyPatientDAO.ifStudyExists(studyPK)){//Bug Fix : 16693
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found"));
				throw new OperationException("Study not found");
				
			}
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);
			Integer manageProtocolPriv = 
				groupAuth.getAppManageProtocolsPrivileges();
			
			boolean hasViewManageProt = 
				GroupAuthModule.hasViewPermission(manageProtocolPriv);
			
			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
			if (!hasViewManageProt){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view study data"));
				throw new AuthorizationException("User Not Authorized to view study data");
			}
			
	
			Integer managePatientsPriv = 
				groupAuth.getAppManagePatientsPrivileges();
			
			boolean hasViewManagePat = 
				GroupAuthModule.hasViewPermission(managePatientsPriv);
			
			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + managePatientsPriv);
			if (!hasViewManagePat){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view patient data"));
				throw new AuthorizationException("User Not Authorized to view patient data");
			}
			
			// Handle access control in the same way as the where clause in Patient.getStudyPatientsSQL().
			// Get sitePK and pass it into StudyPatientDAO.
			StudySiteDao studySiteDao = studySiteAgent.getSitesForStatAndEnrolledPat(studyPK, 
			        callingUser.getUserId(), 0); // 0 = accountId, which is not used in the SQL
			ArrayList siteIds = studySiteDao.getSiteIds();
			Integer sitePK = 0;
			ArrayList<StudyPatient> listCompleteStudyPatient = new ArrayList<StudyPatient>();
			//Virendra:#6757
			if (siteIds != null && siteIds.size() > 0) {
				for(int i = 0 ; i< siteIds.size(); i++){
					sitePK = (Integer)siteIds.get(i);
					listCompleteStudyPatient.addAll((ArrayList<StudyPatient>) StudyPatientDAO.
							getStudyPatientByStudyPK(studyPK, sitePK));
				}
			}
			
			if(listCompleteStudyPatient.isEmpty()){
				addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Patient not found"));
				throw new OperationException("Patient not found");
			}
			//Virendra:#6074
			TeamAuthModule teamAuthModule = new TeamAuthModule(callingUser.getUserId(), studyPK);
			int patientManagePrivileges = teamAuthModule.getPatientManagePrivileges();//StudyTeamPrivileges();
			
			if (!TeamAuthModule.hasViewPermission(patientManagePrivileges)){
				if (logger.isDebugEnabled()) logger.debug("user does not have permission to view study patient");
			
				addIssue(
						new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, 
								"User does not have permission to view study patient"));
				throw new AuthorizationException("User does not have permission to view study patient");
			}
			//Hide PHI information if user not having complete patient data view rights 
			Integer managePatientPrivComplete = 
				groupAuth.getAppViewCompletePatientDataPrivileges();
			//Virendra:#6044
			int viewPatientDetailsPrivileges = teamAuthModule.getPatientViewDataPrivileges();
			
			if(managePatientPrivComplete == 3 || 
					(!TeamAuthModule.hasViewPermission(viewPatientDetailsPrivileges)) ){
				return getLstPhiStudyPatient(listCompleteStudyPatient);
			}
			return listCompleteStudyPatient;
		}
		catch(OperationException e){
			//sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", t);
			throw new OperationException(t);
		}
	}
	
	
	private ArrayList<StudyPatient> getLstPhiStudyPatient(
			ArrayList<StudyPatient> lstStudyPat) throws OperationException{
		
		for(int i = 0; i < lstStudyPat.size(); i++ ){
			StudyPatient studyPat = lstStudyPat.get(i);
			studyPat.setStudyPatFirstName("*");
			studyPat.setStudyPatLastName("*");
		}
		return lstStudyPat;
	}


	@TransactionAttribute(REQUIRED)
	public ResponseHolder enrollPatientToStudy(
			PatientIdentifier patientIdentifier, StudyIdentifier studyIdentifier, PatientEnrollmentDetails patientEnrollmentDetails)
	throws OperationException {
		try{
			
			
			if(patientEnrollmentDetails == null)
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "patientEnrollmentDetails is required to addStudyPatientStatus"));
				throw new OperationException();
			}
			if((patientIdentifier==null || (StringUtil.isEmpty(patientIdentifier.getOID())
					&& 	(StringUtil.isEmpty(patientIdentifier.getPatientId()) 
							|| (patientIdentifier.getOrganizationId()==null 
								|| (StringUtil.isEmpty(patientIdentifier.getOrganizationId().getOID())
									&& StringUtil.isEmpty(patientIdentifier.getOrganizationId().getSiteName())
									&& StringUtil.isEmpty(patientIdentifier.getOrganizationId().getSiteAltId())
									&& (patientIdentifier.getOrganizationId().getPK()==null || patientIdentifier.getOrganizationId().getPK()==0))))
					&&(patientIdentifier.getPK()==null || patientIdentifier.getPK()==0))))
			{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid patientIdentifier is required to addStudyPatientStatus"));
			throw new OperationException();
			}
			if(studyIdentifier==null || (StringUtil.isEmpty(studyIdentifier.getOID()) 
							&& (studyIdentifier.getPK()==null || studyIdentifier.getPK()<=0) 
							&& StringUtil.isEmpty(studyIdentifier.getStudyNumber())))
			{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to addStudyPatientStatus"));
			throw new OperationException();
			}
						
			// check users group right to manage Patients
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);
			Integer managePatientPriv = 
				groupAuth.getAppManagePatientsPrivileges();
			boolean hasNewManagePat = 
				GroupAuthModule.hasEditPermission(managePatientPriv);


			if(!hasNewManagePat)
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to edit patient  data"));
				throw new AuthorizationException("User Not Authorized to edit patient data");
			}



			// get StudyPK for checking Team rights for Study
			Integer studyPK = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier, objectMapService); 
			if(studyPK == null || studyPK == 0){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier OID:" + studyIdentifier.getOID() + " StudyNumber " + studyIdentifier.getStudyNumber()));
				throw new OperationException();
			}

			TeamAuthModule teamAuth = new TeamAuthModule(callingUser.getUserId(), studyPK); 
			Integer managePatientEnrollPriv = teamAuth.getPatientManagePrivileges(); 
			boolean hasEnrollPatientPriv = TeamAuthModule.hasNewPermission(managePatientEnrollPriv); 

			if(!hasEnrollPatientPriv)
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized enroll patient to this Study"));
				throw new AuthorizationException("User Not Authorized to enroll patient");
			}
			
			canEnrollToThisStudy(studyPK); 

			Map<String, Object> parameters = new HashMap<String, Object>(); 

			parameters.put("personAgent", personAgent);
			parameters.put("sessionContext", sessionContext); 
			parameters.put("userAgent", userAgent);
			parameters.put("siteAgent", siteAgent);
			parameters.put("objectMapService", objectMapService);
			parameters.put("patProtAgent", patProtAgent) ;
			parameters.put("userSiteAgent", userSiteAgent);
			parameters.put("patFacilityAgent", patFacilityAgent);
			parameters.put("callingUser", callingUser);
			parameters.put("ResponseHolder", response); 
			parameters.put("studySiteAgent", studySiteAgent); 
			parameters.put("patStudyStatAgent", patStudyStatAgent); 
			parameters.put("studyAgent", studyAgent); 

			PatientEnrollmentHandler handler = new PatientEnrollmentHandler(); 
			handler.enrollPatientToStudy(patientIdentifier, studyPK, patientEnrollmentDetails, parameters);
		}catch(OperationException e){
			//sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			//sessionContext.setRollbackOnly(); 
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", t);
			throw new OperationException(t);
		}

		return response;
	}
	
	
	@TransactionAttribute(REQUIRED)
	public ResponseHolder createAndEnrollPatient(Patient patient, StudyIdentifier studyIdentifier, PatientEnrollmentDetails patientEnrollmentDetails)
	throws OperationException
	{

		try{
			
			if(patient == null || patient.getPatientDemographics() == null || patientEnrollmentDetails == null)
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "PatientDemographics, patientEnrollmentDetails are required to create and enroll patient to Study"));
				throw new OperationException();
			}
			if(studyIdentifier==null || (StringUtil.isEmpty(studyIdentifier.getOID()) 
					&& (studyIdentifier.getPK()==null || studyIdentifier.getPK()<=0) 
					&& StringUtil.isEmpty(studyIdentifier.getStudyNumber())))
			{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to addStudyPatientStatus"));
			throw new OperationException();
			}
						
			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent);
			int managePatients = authModule.getAppManagePatientsPrivileges();
			boolean hasNewPatientPermissions = GroupAuthModule.hasNewPermission(managePatients); 
			if(!hasNewPatientPermissions)
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User not authorized to add patient data"));
				throw new AuthorizationException("User not authorized to add patient data");
			}
			
			
			
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);
			Integer managePatientPriv = 
				groupAuth.getAppManagePatientsPrivileges();
			boolean hasNewManagePat = 
				GroupAuthModule.hasEditPermission(managePatientPriv);


			if(!hasNewManagePat)
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to edit patient  data"));
				throw new AuthorizationException("User Not Authorized to edit patient data");
			}



			// get StudyPK for checking Team rights for Study
			Integer studyPK = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier, objectMapService); 
			if(studyPK == null || studyPK == 0){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier OID:" + studyIdentifier.getOID() + " StudyNumber " + studyIdentifier.getStudyNumber()));
				throw new OperationException();
			}

			TeamAuthModule teamAuth = new TeamAuthModule(callingUser.getUserId(), studyPK); 
			Integer managePatientEnrollPriv = teamAuth.getPatientManagePrivileges(); 
			boolean hasEnrollPatientPriv = TeamAuthModule.hasNewPermission(managePatientEnrollPriv); 

			if(!hasEnrollPatientPriv)
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized enroll patient to this Study"));
				throw new AuthorizationException("User Not Authorized to enroll patient");
			}
	
			canEnrollToThisStudy(studyPK);
			canEnrollToThisOrg(studyPK, patient);//BugFix: 10887
			
			Map<String, Object> parameters = new HashMap<String, Object>(); 
			parameters.put("personAgent", personAgent);
			parameters.put("sessionContext", sessionContext); 
			parameters.put("userAgent", userAgent);
			parameters.put("siteAgent", siteAgent);
			parameters.put("objectMapService", objectMapService);
			parameters.put("patProtAgent", patProtAgent) ;
			parameters.put("userSiteAgent", userSiteAgent);
			parameters.put("patFacilityAgent", patFacilityAgent);
			parameters.put("callingUser", callingUser);
			parameters.put("ResponseHolder", response); 
			parameters.put("studySiteAgent", studySiteAgent); 
			parameters.put("patStudyStatAgent", patStudyStatAgent); 
			parameters.put("studyAgent", studyAgent); 

			PatientEnrollmentHandler handler = new PatientEnrollmentHandler(); 
			handler.createAndEnrollPatient(patient, studyPK, patientEnrollmentDetails, parameters); 
		}catch(OperationException e){
			//sessionContext.setRollbackOnly();			
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			//sessionContext.setRollbackOnly(); 	
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", t);
			throw new OperationException(t);
		}

		return response;		
	}
	
	
	private boolean canEnrollToThisStudy(Integer studyPK) throws OperationException
	{
		StudyDao studydao = studyAgent.getUserStudies(EJBUtil.integerToString(callingUser.getUserId()), "dStudy", 0, "active");
		boolean canEnroll = false; 
		for(int i = 0; i < studydao.getStudyIds().size(); i++)
		{
			int studyID = EJBUtil.stringToInteger(studydao.getStudyIds().get(i).toString());
			if(studyID == studyPK)
			{
			   canEnroll = true; 
			   break; 
			}
		}
		
		if(!canEnroll)
		{
			addIssue(
					new Issue(IssueTypes.DATA_VALIDATION, 
							"Patient cannot be enroll to Study provided"));
			throw new AuthorizationException("Patient cannot be enrolled to Study provided");
		}
		 
		return canEnroll; 

	}
	//Bug Fix:10887
	private boolean canEnrollToThisOrg(Integer studyPK, Patient patient) throws OperationException{
		
		boolean canEnroll = false;
		StudySiteDao studySiteDao = studySiteAgent.getStdSitesLSampleSize(studyPK);
		for(int i =0; i < studySiteDao.getStudySiteIds().size(); i++){
					
			String patientSiteName = patient.getPatientDemographics().getOrganization().getSiteName();
			
			if(studySiteDao.getSiteNames().get(i).toString().compareTo(patientSiteName) == 0){
				
				canEnroll = true;
				break;
			}
		
	}
		if(!canEnroll){
			addIssue(
					new Issue(IssueTypes.DATA_VALIDATION, 
							"Patient Organization is not part of the Study Team Organization"));
			throw new AuthorizationException("Patient cannot be enrolled to Study provided");
		}
		
		return canEnroll;
	}
	



	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		response = new ResponseHolder();
		callingUser = 
			getLoggedInUser(
					sessionContext,
					userAgent);
		return ctx.proceed();

	}


	@Override
	public ResponseHolder deleteStudyPatientStatus(
			PatientStudyStatusIdentifier patientStudyStatusIdentifier,String reasonForDelete)
			throws OperationException {
		try {
			
			//--------Checking group authorization-------------//
			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent); 
			Integer patientManagmentAccess = authModule.getAppManagePatientsPrivileges(); 
			if(!GroupAuthModule.hasViewPermission(patientManagmentAccess))
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User is not authorized to view Patient details")); 
				throw new AuthorizationException(); 
			}
			//--------Checking PatientStudyStatusIdentifier for null---------//
			if((patientStudyStatusIdentifier==null) || ((StringUtil.isEmpty(patientStudyStatusIdentifier.getOID())) && (patientStudyStatusIdentifier.getPK()==null || patientStudyStatusIdentifier.getPK()==0)))
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientStudyStatusIdentifier with OID or PK is required.")); 
				throw new OperationException();
			}
			ObjectMap objectMap =null;
			Integer patStudyPK=0;
			if(!StringUtil.isEmpty(patientStudyStatusIdentifier.getOID()))
			{
				objectMap = objectMapService.getObjectMapFromId(patientStudyStatusIdentifier.getOID());
				if(objectMap == null || !(objectMap.getTableName().equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_PATSTUDYSTAT)))
				{
					addIssue(new Issue(IssueTypes.STUDY_PATIENT_STATUS_NOT_FOUND, "Please send valid PatientStudyStatusIdentifier"));
					throw new OperationException();
				}
				patStudyPK=objectMap.getTablePK();
			}
			
			if(patientStudyStatusIdentifier.getPK()!=null && patientStudyStatusIdentifier.getPK()!=0)
			{
				patStudyPK = patientStudyStatusIdentifier.getPK();
			}
			
			
			
			PatStudyStatBean patStudyStatBean=patStudyStatAgent.getPatStudyStatDetails(patStudyPK.intValue());
			if(patStudyStatBean==null)
			{
				addIssue(new Issue(IssueTypes.STUDY_PATIENT_STATUS_NOT_FOUND, "Study Patient status not found."));
				throw new OperationException();
			}
			Integer studyPK=StringUtil.stringToInteger(patStudyStatBean.getStudyId());
			Integer perPK=StringUtil.stringToInteger(patStudyStatBean.getPatientId());
			//---------check patient level access------------//
			Integer userMaxStudyPatientRight = userSiteAgent.getMaxRightForStudyPatient(callingUser.getUserId(), perPK, studyPK);
			if(userMaxStudyPatientRight == 0)
			{
				addIssue(new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION, "User is not authorized to access patient's study site")); 
				throw new OperationException();
			}
			//-----------check Study level access------------//
			TeamAuthModule teamAuth = new TeamAuthModule(callingUser.getUserId(), studyPK); 
			Integer studyPatientManagement = teamAuth.getPatientManagePrivileges(); 
			if(!TeamAuthModule.hasEditPermission(studyPatientManagement))
			{
				addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User is not authorized to edit study patient status")); 
				throw new OperationException(); 
			}
			//-----------Checking if provided StudyPatientStatus is current status or not---//
			String currentStatFlag=patStudyStatBean.getCurrentStat();
			if(currentStatFlag.equals("1"))
			{
				addIssue(new Issue(IssueTypes.ERROR_REMOVING_STUDY_PATIENT_STATUS, "Can not delete current study patient status")); 
				throw new OperationException();
			}
			//-------------Removing study patient status---------//
			//Bug #13802 Raman
			int ret;
			if(StringUtil.isEmpty(reasonForDelete))
			{
				StudyBean sb=studyAgent.getStudyDetails(studyPK.intValue());
				if("1".equals(sb.getFdaRegulatedStudy()))
				{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"reasonForDelete field may not be left blank when study is marked as FDA regulated"));
					throw new OperationException();
				}
				ret=patStudyStatAgent.reomovePatStudyStat(patStudyPK);
			}
			else
			{

				Hashtable<String, String> argsHT = new Hashtable<String, String>();
				argsHT.put("USER_ID_KEY",StringUtil.integerToString(callingUser.getUserId()));
				argsHT.put("IP_ADD_KEY", AbstractService.IP_ADDRESS_FIELD_VALUE);
				argsHT.put("REASON_FOR_DEL",reasonForDelete);
				ret=patStudyStatAgent.reomovePatStudyStat(patStudyPK,argsHT);
				
			}
			if(ret==-1)
			{
				addIssue(new Issue(IssueTypes.ERROR_REMOVING_STUDY_PATIENT_STATUS, "Study Patient status could not be deleted successfully")); 
				throw new OperationException();
			}
			if(ret==-3)
			{
				addIssue(new Issue(IssueTypes.ERROR_REMOVING_STUDY_PATIENT_STATUS, "Study Patient status can not be deleted as this is the only patient study status left")); 
				throw new OperationException();
			}
			//Bug #14482 
			ObjectMap map=objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PATSTUDYSTAT, patStudyPK);
			response.addAction(new CompletedAction("StudyPatientStatus with OID " + map.getOID()+" and PK : "+patStudyPK+" is removed successfully", CRUDAction.REMOVE));
			
			
		}catch(OperationException e){
			//sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("FormResponseServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("FormResponseServiceImpl create", t);
			throw new OperationException(t);
		}
		
		
		
		return response;
	}


	@Override
	public ResponseHolder updateStudyPatientStatus(PatientStudyStatusIdentifier patientStudyStatusIdentifier,PatientEnrollmentDetails patientEnrollmentDetails)
			throws OperationException {
		try {
			
			//--------Checking group authorization-------------//
			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent); 
			Integer patientManagmentAccess = authModule.getAppManagePatientsPrivileges(); 
			if(!GroupAuthModule.hasViewPermission(patientManagmentAccess))
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User is not authorized to view Patient details")); 
				throw new AuthorizationException(); 
			}
			//--------Checking PatientStudyStatusIdentifier for null---------//
			if((patientStudyStatusIdentifier==null) || ((StringUtil.isEmpty(patientStudyStatusIdentifier.getOID())) && (patientStudyStatusIdentifier.getPK()==null || patientStudyStatusIdentifier.getPK()==0)))
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientStudyStatusIdentifier with OID or PK is required.")); 
				throw new OperationException();
			}
			//--------Checking PatientEnrollmentDetails for null---------//
			if(patientEnrollmentDetails==null)
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "PatientEnrollmentDetails is required.")); 
				throw new OperationException();
			}
			ObjectMap objectMap =null;
			Integer patStudyPK=0;
			if(!StringUtil.isEmpty(patientStudyStatusIdentifier.getOID()))
			{
				objectMap = objectMapService.getObjectMapFromId(patientStudyStatusIdentifier.getOID());
				if(objectMap == null || !(objectMap.getTableName().equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_PATSTUDYSTAT)))
				{
					addIssue(new Issue(IssueTypes.STUDY_PATIENT_STATUS_NOT_FOUND, "Please send valid PatientStudyStatusIdentifier"));
					throw new OperationException();
				}
				patStudyPK=objectMap.getTablePK();
			}
			
			if(patientStudyStatusIdentifier.getPK()!=null && patientStudyStatusIdentifier.getPK()!=0)
			{
				patStudyPK = patientStudyStatusIdentifier.getPK();
			}
			PatStudyStatBean patStudyStatBean=patStudyStatAgent.getPatStudyStatDetails(patStudyPK.intValue());
			if(patStudyStatBean==null)
			{
				addIssue(new Issue(IssueTypes.STUDY_PATIENT_STATUS_NOT_FOUND, "Study Patient status not found."));
				throw new OperationException();
			}
			Integer studyPK=StringUtil.stringToInteger(patStudyStatBean.getStudyId());
			Integer perPK=StringUtil.stringToInteger(patStudyStatBean.getPatientId());
			//---------check patient level access------------//
			Integer userMaxStudyPatientRight = userSiteAgent.getMaxRightForStudyPatient(callingUser.getUserId(), perPK, studyPK);
			if(userMaxStudyPatientRight == 0)
			{
				addIssue(new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION, "User is not authorized to access patient's study site")); 
				throw new OperationException();
			}
			//-----------check Study level access------------//
			TeamAuthModule teamAuth = new TeamAuthModule(callingUser.getUserId(), studyPK); 
			Integer studyPatientManagement = teamAuth.getPatientManagePrivileges(); 
			if(!TeamAuthModule.hasEditPermission(studyPatientManagement))
			{
				addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User is not authorized to edit study patient status")); 
				throw new OperationException(); 
			}
			//-----------Checking if provided StudyPatientStatus is current status or not---//
			//String currentStatFlag=patStudyStatBean.getCurrentStat();
			/*if(patStudyStatBean.getCurrentStat().equals("1") && patientEnrollmentDetails.isCurrentStatus() )
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Study Patient Status provided is already the current status.")); 
				throw new OperationException();
			}*/
			
			PatientEnrollmentHandler patientEnrollmentHandler=new PatientEnrollmentHandler();
			//StudyPatientHelper studyPatientHelper=new StudyPatientHelper();
			
			Map<String, Object> parameters = new HashMap<String, Object>(); 
			parameters.put("patStudyStatAgent", patStudyStatAgent);
			parameters.put("personAgent", personAgent);
			parameters.put("callingUser", callingUser);
			parameters.put("response", response);
			parameters.put("studyPK",studyPK);
			parameters.put("perPK", perPK);
			parameters.put("sessionContext", sessionContext);
			parameters.put("objectMapService", objectMapService);
			parameters.put("patFacilityAgent", patFacilityAgent);
			parameters.put("userAgent", userAgent);
			parameters.put("siteAgent", siteAgent);
			parameters.put("studyAgent", studyAgent);
			parameters.put("auditRowEresAgnet", auditRowEresAgnet);
			parameters.put("auditRowEpatAgent", auditRowEpatAgent);
			
			
			patientEnrollmentHandler.updateStudyPatientStatus(patStudyStatBean,patientEnrollmentDetails,parameters);
			//Bug #14481
			ObjectMap map=objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PATSTUDYSTAT, patStudyPK);
			response.addAction(new CompletedAction("StudyPatientStatus with OID " + map.getOID()+" and with Pk : "+patStudyPK+" is updated successfully", CRUDAction.UPDATE));
		}catch(OperationException e){
			//sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("FormResponseServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("FormResponseServiceImpl create", t);
			throw new OperationException(t);
		}
		return response;
	}
	
	
	public StudyPatientStatuses getStudyPatientStatusHistory(StudyIdentifier studyIdentifier, PatientIdentifier patientIdentifier)
	throws OperationException
	{
		StudyPatientStatuses studyPatientStatuses = new StudyPatientStatuses(); 
		try
		{
			if((patientIdentifier==null || (StringUtil.isEmpty(patientIdentifier.getOID())
					&& 	(StringUtil.isEmpty(patientIdentifier.getPatientId()) 
							|| (patientIdentifier.getOrganizationId()==null 
								|| (StringUtil.isEmpty(patientIdentifier.getOrganizationId().getOID())
									&& StringUtil.isEmpty(patientIdentifier.getOrganizationId().getSiteName())
									&& StringUtil.isEmpty(patientIdentifier.getOrganizationId().getSiteAltId())
									&& (patientIdentifier.getOrganizationId().getPK()==null || patientIdentifier.getOrganizationId().getPK()==0))))
					&&(patientIdentifier.getPK()==null || patientIdentifier.getPK()==0))))
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid patientIdentifier is required to get StudyPatientStatusHistory"));
				throw new OperationException();
			}
			if(studyIdentifier==null || (StringUtil.isEmpty(studyIdentifier.getOID()) 
							&& (studyIdentifier.getPK()==null || studyIdentifier.getPK()<=0) 
							&& StringUtil.isEmpty(studyIdentifier.getStudyNumber())))
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to get StudyPatientStatusHistory"));
				throw new OperationException();
			}
			
			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent);
			int managePatients = authModule.getAppManagePatientsPrivileges();
			boolean hasViewPatientPermissions = GroupAuthModule.hasViewPermission(managePatients); 
			if(!hasViewPatientPermissions)
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User not authorized to view patient data"));
				throw new AuthorizationException("User not authorized to view patient data");
			}
			
			
			
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);
			Integer managePatientPriv = 
				groupAuth.getAppManagePatientsPrivileges();
			boolean hasViewManagePat = 
				GroupAuthModule.hasViewPermission(managePatientPriv);


			if(!hasViewManagePat)
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view patient  data"));
				throw new AuthorizationException("User Not Authorized to view patient data");
			}



			// get StudyPK for checking Team rights for Study
			Integer studyPK = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier, objectMapService); 
			if(studyPK == null || studyPK == 0){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier"));
				throw new OperationException();
			}

			TeamAuthModule teamAuth = new TeamAuthModule(callingUser.getUserId(), studyPK); 
			Integer managePatientEnrollPriv = teamAuth.getPatientManagePrivileges(); 
			boolean hasEnrollPatientPriv = TeamAuthModule.hasViewPermission(managePatientEnrollPriv);
			
			if(!hasEnrollPatientPriv)
			{
				addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION,"User Not authorized for patient data for this Study"));
				throw new OperationException(); 
			}
			
			Integer patientPK = ObjectLocator.personPKFromPatientIdentifier(callingUser, patientIdentifier, objectMapService); 
			if(patientPK == null || patientPK == 0)
			{
				addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Patient not found for PatientIdentifier"));
				throw new OperationException();
			}
			
			UserSiteDao userSiteDAO = new UserSiteDao(); 
			
			if(userSiteDAO.getMaxRightForStudyPatient(callingUser.getUserId(), patientPK, studyPK)== 0)
			{
				addIssue(new Issue(IssueTypes.ORGANIZATION_AUTHORIZATION, "User not authorized to create new StudyPatientStatus for this Study and Patient")); 
				throw new OperationException();
			}
			
			StudyPatientDAO studyPatientDAO = new StudyPatientDAO(); 
			studyPatientStatuses.setStudyPatientStatus(studyPatientDAO.getPatStatusHistory(patientPK, studyPK, StringUtil.stringToInteger(callingUser.getUserAccountId()))); 

		}catch(OperationException e){
			if (logger.isDebugEnabled()) logger.debug("StudyPatientServiceImpl getStudPatientStatusHistory", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyPatientServiceImpl getStudPatientStatusHistory", t);
			throw new OperationException(t);
		}
		
		return studyPatientStatuses; 
	}


	/* (non-Javadoc)
	 * @see com.velos.services.studypatient.StudyPatientService#addStudyPatientStatus(com.velos.services.model.PatientEnrollmentDetails, com.velos.services.model.PatientIdentifier, com.velos.services.model.StudyIdentifier)
	 */
	@Override
	public ResponseHolder addStudyPatientStatus(
			PatientEnrollmentDetails studyPatientStatusDetails,
			PatientIdentifier patientIdentifier, StudyIdentifier studyIdentifier)
			throws OperationException {
		try{                                                                                                                                                               
			
			if(studyPatientStatusDetails == null)
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "patientEnrollmentDetails is required to addStudyPatientStatus"));
				throw new OperationException();
			}

			if((patientIdentifier==null || (StringUtil.isEmpty(patientIdentifier.getOID())
												&& 	(StringUtil.isEmpty(patientIdentifier.getPatientId()) 
														|| (patientIdentifier.getOrganizationId()==null 
															|| (StringUtil.isEmpty(patientIdentifier.getOrganizationId().getOID())
																&& StringUtil.isEmpty(patientIdentifier.getOrganizationId().getSiteName())
																&& StringUtil.isEmpty(patientIdentifier.getOrganizationId().getSiteAltId())
																&& (patientIdentifier.getOrganizationId().getPK()==null || patientIdentifier.getOrganizationId().getPK()==0))))
												&&(patientIdentifier.getPK()==null || patientIdentifier.getPK()==0))))
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid patientIdentifier is required to addStudyPatientStatus"));
				throw new OperationException();
			}
			if(studyIdentifier==null || (StringUtil.isEmpty(studyIdentifier.getOID()) 
											&& (studyIdentifier.getPK()==null || studyIdentifier.getPK()<=0) 
											&& StringUtil.isEmpty(studyIdentifier.getStudyNumber())))
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to addStudyPatientStatus"));
				throw new OperationException();
			}
			// check users group right to manage Patients
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);
			Integer managePatientPriv = 
				groupAuth.getAppManagePatientsPrivileges();
			boolean hasNewManagePat = 
				GroupAuthModule.hasViewPermission(managePatientPriv);


			if(!hasNewManagePat)
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to edit patient  data"));
				throw new AuthorizationException("User Not Authorized to edit patient data");
			}

			
			

			// get StudyPK for checking Team rights for Study
			Integer studyPK = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier, objectMapService); 
			if(studyPK == null || studyPK == 0){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier OID:" + studyIdentifier.getOID() + " StudyNumber " + studyIdentifier.getStudyNumber()));
				throw new OperationException();
			}

			TeamAuthModule teamAuth = new TeamAuthModule(callingUser.getUserId(), studyPK); 
			Integer managePatientEnrollPriv = teamAuth.getPatientManagePrivileges(); 
			boolean hasEnrollPatientPriv = TeamAuthModule.hasNewPermission(managePatientEnrollPriv); 

			if(!hasEnrollPatientPriv)
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized enroll patient to this Study"));
				throw new AuthorizationException("User Not Authorized to enroll patient");
			}

			Map<String, Object> parameters = new HashMap<String, Object>(); 

			parameters.put("personAgent", personAgent);
			parameters.put("sessionContext", sessionContext); 
			parameters.put("userAgent", userAgent);
			parameters.put("siteAgent", siteAgent);
			parameters.put("objectMapService", objectMapService);
			parameters.put("patProtAgent", patProtAgent) ;
			parameters.put("userSiteAgent", userSiteAgent);
			parameters.put("patFacilityAgent", patFacilityAgent);
			parameters.put("callingUser", callingUser);
			parameters.put("ResponseHolder", response); 
			parameters.put("studySiteAgent", studySiteAgent); 
			parameters.put("patStudyStatAgent", patStudyStatAgent); 
			parameters.put("studyAgent", studyAgent);

			Integer patientPK =null;
			try {
				patientPK = ObjectLocator.personPKFromPatientIdentifier(callingUser, patientIdentifier, objectMapService);
			} catch (MultipleObjectsFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			UserSiteDao userSiteDAO = new UserSiteDao(); 
			
			if(userSiteDAO.getMaxRightForStudyPatient(callingUser.getUserId(), patientPK, studyPK)== 0)
			{
				addIssue(new Issue(IssueTypes.ORGANIZATION_AUTHORIZATION, "User not authorized to create new StudyPatientStatus for this Study and Patient")); 
				throw new OperationException();
			}

			PatientEnrollmentHandler handler = new PatientEnrollmentHandler(); 
			handler.addStudyPatientStatus(studyPatientStatusDetails, parameters, patientPK, studyPK);

		}catch(OperationException e){
			if (logger.isDebugEnabled()) logger.debug("StudyPatientServiceImpl getStudPatientStatusHistory", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyPatientServiceImpl getStudPatientStatusHistory", t);
			throw new OperationException(t);
		}
		return response;
	}


	@Override
	public PatientEnrollmentDetails getStudyPatientStatus(
			PatientStudyStatusIdentifier patientStudyStatusIdentifier)
			throws OperationException {
		PatientEnrollmentDetails patientEnrollmentDetails=new PatientEnrollmentDetails();
		try
		{
			//--------Checking group authorization-------------//
			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent); 
			Integer patientManagmentAccess = authModule.getAppManagePatientsPrivileges(); 
			if(!GroupAuthModule.hasViewPermission(patientManagmentAccess))
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User is not authorized to view Patient details")); 
				throw new AuthorizationException(); 
			}
			//--------Checking PatientStudyStatusIdentifier for null---------//
			if((patientStudyStatusIdentifier==null) || ((StringUtil.isEmpty(patientStudyStatusIdentifier.getOID())) && (patientStudyStatusIdentifier.getPK()==null || patientStudyStatusIdentifier.getPK()==0)))
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientStudyStatusIdentifier with OID or PK is required.")); 
				throw new OperationException();
			}
			ObjectMap objectMap =null;
			Integer patStudyPK=0;
			if(!StringUtil.isEmpty(patientStudyStatusIdentifier.getOID()))
			{
				objectMap = objectMapService.getObjectMapFromId(patientStudyStatusIdentifier.getOID());
				if(objectMap == null || !(objectMap.getTableName().equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_PATSTUDYSTAT)))
				{
					addIssue(new Issue(IssueTypes.STUDY_PATIENT_STATUS_NOT_FOUND, "Please send valid PatientStudyStatusIdentifier"));
					throw new OperationException();
				}
				patStudyPK=objectMap.getTablePK();
			}
			
			if(patientStudyStatusIdentifier.getPK()!=null && patientStudyStatusIdentifier.getPK()!=0)
			{
				patStudyPK = patientStudyStatusIdentifier.getPK();
			}
			
			PatStudyStatBean patStudyStatBean=patStudyStatAgent.getPatStudyStatDetails(patStudyPK.intValue());
			if(patStudyStatBean==null)
			{
				addIssue(new Issue(IssueTypes.STUDY_PATIENT_STATUS_NOT_FOUND, "Study Patient status not found."));
				throw new OperationException();
			}
			Integer studyPK=StringUtil.stringToInteger(patStudyStatBean.getStudyId());
			Integer perPK=StringUtil.stringToInteger(patStudyStatBean.getPatientId());
			//---------check patient level access------------//
			Integer userMaxStudyPatientRight = userSiteAgent.getMaxRightForStudyPatient(callingUser.getUserId(), perPK, studyPK);
			if(userMaxStudyPatientRight == 0)
			{
				addIssue(new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION, "User is not authorized to access patient's study site")); 
				throw new OperationException();
			}
			//-----------check Study level access------------//
			TeamAuthModule teamAuth = new TeamAuthModule(callingUser.getUserId(), studyPK); 
			Integer studyPatientManagement = teamAuth.getPatientManagePrivileges(); 
			if(!TeamAuthModule.hasViewPermission(studyPatientManagement))
			{
				addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User is not authorized to view study patient status")); 
				throw new OperationException(); 
			}
			CodeDao codeDao=new CodeDao();
			CodeCache codeCache = CodeCache.getInstance();
			int callingUserAccId=StringUtil.stringToNum(callingUser.getUserAccountId());
			PatProtBean patProtBean=patStudyStatBean.getPatProtObj();
			PersonBean personB = personAgent.getPersonDetails(StringUtil.stringToNum(patStudyStatBean.getPatientId()));
			
			Code codeStatus = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_PATPROT_STATUS, patStudyStatBean.getPatStudyStat(),callingUserAccId);
			patientEnrollmentDetails.setStatus(codeStatus);
			Code codeStatusReason=codeCache.getCodeSubTypeByPK(codeDao.getCodeSubtype(StringUtil.stringToInteger(patStudyStatBean.getPatStudyStat())), patStudyStatBean.getPatStudyStatReason(), callingUserAccId);
			patientEnrollmentDetails.setStatusReason(codeStatusReason);
			patientEnrollmentDetails.setCurrentStatus(patStudyStatBean.getCurrentStat().equals("1")?true:false);
			patientEnrollmentDetails.setNotes(patStudyStatBean.getNotes());
			patientEnrollmentDetails.setStatusDate(patStudyStatBean.getStartDate());
			//enrolled
			if(codeStatus.getCode().equalsIgnoreCase("enrolled"))
			{
				patientEnrollmentDetails.setRandomizationNumber(patProtBean.getPatProtRandomNumber());
				UserBean ubEnrolledBy=userAgent.getUserDetails(StringUtil.stringToNum(patProtBean.getPatProtUserId()));
				if(ubEnrolledBy!=null)
				{
					UserIdentifier enrolledBy=new UserIdentifier();
					enrolledBy.setFirstName(ubEnrolledBy.getUserFirstName());
					enrolledBy.setLastName(ubEnrolledBy.getUserLastName());
					enrolledBy.setUserLoginName(ubEnrolledBy.getUserLoginName());
					enrolledBy.setOID((objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_USER,StringUtil.stringToInteger(patProtBean.getPatProtUserId()))).getOID());
					patientEnrollmentDetails.setEnrolledBy(enrolledBy);
				}
			}
			//Informed Consent Signed
			if(codeStatus.getCode().equalsIgnoreCase("infConsent"))
			{
				patientEnrollmentDetails.setInformedConsentVersionNumber(patProtBean.getPtstConsentVer());
			}
			//in follow up
			if(codeStatus.getCode().equalsIgnoreCase("followup"))
			{
				patientEnrollmentDetails.setNextFollowUpDate(patStudyStatBean.getPtstNextFlwupPersistent());
			}
			//Screening
			if(codeStatus.getCode().equalsIgnoreCase("screening"))
			{
				patientEnrollmentDetails.setScreenNumber(patStudyStatBean.getScreenNumber());
				UserBean ubScreenedBy=userAgent.getUserDetails(StringUtil.stringToNum(patStudyStatBean.getScreenedBy()));
				if(ubScreenedBy!=null)
				{
					UserIdentifier screenedBy=new UserIdentifier();
					screenedBy.setFirstName(ubScreenedBy.getUserFirstName());
					screenedBy.setLastName(ubScreenedBy.getUserLastName());
					screenedBy.setUserLoginName(ubScreenedBy.getUserLoginName());
					screenedBy.setOID((objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_USER,StringUtil.stringToInteger(patStudyStatBean.getScreenedBy()))).getOID());
					patientEnrollmentDetails.setScreenedBy(screenedBy);
				}
				Code codeScreeningOutcome=codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_PATPROT_SCREENING_OUTCOME, patStudyStatBean.getScreeningOutcome(), callingUserAccId);
				patientEnrollmentDetails.setScreeningOutcome(codeScreeningOutcome);				
			}
			patientEnrollmentDetails.setPatientStudyId(patProtBean.getPatStudyId());
			
			if(!StringUtil.isEmpty(patProtBean.getEnrollingSite()))
			{
				SiteBean sbEnrollingSite=siteAgent.getSiteDetails(StringUtil.stringToInteger(patProtBean.getEnrollingSite()));
				if(sbEnrollingSite!=null)
				{
					OrganizationIdentifier enrollingSite=new OrganizationIdentifier();
					enrollingSite.setSiteName(sbEnrollingSite.getSiteName());
					enrollingSite.setOID((objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION,StringUtil.stringToInteger(patProtBean.getEnrollingSite()))).getOID());
					patientEnrollmentDetails.setEnrollingSite(enrollingSite);
				}
			}
			
			//AssignedTo
			UserBean ubAssignedTo=userAgent.getUserDetails(StringUtil.stringToNum(patProtBean.getAssignTo()));
			if(ubAssignedTo!=null)
			{
				UserIdentifier assignedTo=new UserIdentifier();
				assignedTo.setFirstName(ubAssignedTo.getUserFirstName());
				assignedTo.setLastName(ubAssignedTo.getUserLastName());
				assignedTo.setUserLoginName(ubAssignedTo.getUserLoginName());
				assignedTo.setOID((objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_USER,StringUtil.stringToInteger(patProtBean.getAssignTo()))).getOID());
				patientEnrollmentDetails.setAssignedTo(assignedTo);
			}
			//Physician
			UserBean ubPhysician=userAgent.getUserDetails(StringUtil.stringToNum(patProtBean.getPatProtPhysician()));
			if(ubPhysician!=null)
			{
				UserIdentifier physician=new UserIdentifier();
				physician.setFirstName(ubPhysician.getUserFirstName());
				physician.setLastName(ubPhysician.getUserLastName());
				physician.setUserLoginName(ubPhysician.getUserLoginName());
				OrganizationIdentifier organizationIdentifier = new OrganizationIdentifier();
				organizationIdentifier.setPK(StringUtil.stringToInteger(ubPhysician.getUserSiteId()));
				physician.setOrganizationIdentifier(organizationIdentifier);
				physician.setOID((objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_USER,StringUtil.stringToInteger(patProtBean.getPatProtPhysician()))).getOID());
				patientEnrollmentDetails.setPhysician(physician);
			}
			//Treatment Location
			Code codeTreatmentLocation=codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_PATPROT_TREATMENT_LOCATION, patProtBean.getTreatmentLoc(), callingUserAccId);
			patientEnrollmentDetails.setTreatmentLocation(codeTreatmentLocation);
			//Treatment Organization
			if(!StringUtil.isEmpty(patProtBean.getpatOrg()))
			{
				SiteBean sbTreatmentOrg=siteAgent.getSiteDetails(StringUtil.stringToInteger(patProtBean.getpatOrg()));
				if(sbTreatmentOrg!=null)
				{
					OrganizationIdentifier TreatmentOrg=new OrganizationIdentifier();
					TreatmentOrg.setSiteName(sbTreatmentOrg.getSiteName());
					TreatmentOrg.setOID((objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION,StringUtil.stringToInteger(patProtBean.getpatOrg()))).getOID());
					patientEnrollmentDetails.setTreatingOrganization(TreatmentOrg);
					
				}
			}
			
			//Evaluation flag
			Code codeEvaluationFlag=codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_PATPROT_EVALUATION_FLAG, patProtBean.getPtstEvalFlag(), callingUserAccId);
			patientEnrollmentDetails.setEvaluationFlag(codeEvaluationFlag);
			//Evaluation Status
			Code codeEvaluationStatus=codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_PATPROT_EVALATION_STATUS, patProtBean.getPtstEval(), callingUserAccId);
			patientEnrollmentDetails.setEvaluationStatus(codeEvaluationStatus);
			//Inevaluation Status
			Code codeInEvaluationStatus=codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_PATPROT_INEVALATION_STATUS, patProtBean.getPtstInEval(), callingUserAccId);
			patientEnrollmentDetails.setInevaluationStatus(codeInEvaluationStatus);
			//Survival Status
			Code codeSurvivalStatus=codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_PATPROT_SURVIVAL_STATUS, patProtBean.getPtstSurvival(), callingUserAccId);
			patientEnrollmentDetails.setSurvivalStatus(codeSurvivalStatus);
			//DOD
			patientEnrollmentDetails.setDateOfDeath(patProtBean.getPtstDeathDatePersistent());
			//COD
			Code codeDeathCause=codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_PATIENT_DEATH_CAUSE, personB.getPatDthCause(), callingUserAccId);
			patientEnrollmentDetails.setCauseOfDeath(codeDeathCause);
			//Specify Cause
			patientEnrollmentDetails.setSpecifyCause(personB.getDthCauseOther());
			//Death Related To Trial
			Code codeDeathRltdToTrial=codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_PATPROT_STUDY_RELATED_DEATH, patProtBean.getPtstDthStdRel(), callingUserAccId);
			patientEnrollmentDetails.setDeathRelatedToTrial(codeDeathRltdToTrial);
			//Reason of Death Related To Trial
			patientEnrollmentDetails.setReasonOfDeathRelatedToTrial(patProtBean.getPtstDthStdRelOther());
			
			
		}catch(OperationException e){
			if (logger.isDebugEnabled()) logger.debug("StudyPatientServiceImpl getStudPatientStatusHistory", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyPatientServiceImpl getStudPatientStatusHistory", t);
			throw new OperationException(t);
		}
		
		return patientEnrollmentDetails;
	}

	@Override
	public ResponseHolder createMPatientStudyStatus(
			CreateMultiPatientStudyStatuses createMPatientStudyStatuses)
			throws OperationException {
		try
		{
		
			System.out.println(" ---inside the createMPatientStudyStatus method----");
			//--------Checking group authorization-------------//
			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent); 
			Integer patientManagmentAccess = authModule.getAppManagePatientsPrivileges(); 
			if(!GroupAuthModule.hasViewPermission(patientManagmentAccess))
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User is not authorized to edit Patient details")); 
				throw new AuthorizationException(); 
			}
			if(createMPatientStudyStatuses == null)
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid CreateMPatientStudyStatuses is required")); 
				throw new OperationException(); 
			}
			if(createMPatientStudyStatuses.getCreateMPatientStudyStatusList()== null)
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid CreateMPatientStudyStatusesList is required")); 
				throw new OperationException(); 
			}
		
			Map<String, Object> parameters = new HashMap<String, Object>(); 

			parameters.put("personAgent", personAgent);
			parameters.put("sessionContext", sessionContext); 
			parameters.put("userAgent", userAgent);
			parameters.put("siteAgent", siteAgent);
			parameters.put("objectMapService", objectMapService);
			parameters.put("patProtAgent", patProtAgent) ;
			parameters.put("userSiteAgent", userSiteAgent);
			parameters.put("patFacilityAgent", patFacilityAgent);
			parameters.put("callingUser", callingUser);
			parameters.put("ResponseHolder", response); 
			parameters.put("studySiteAgent", studySiteAgent); 
			parameters.put("patStudyStatAgent", patStudyStatAgent); 
			parameters.put("studyAgent", studyAgent);
			
			parameters.put("auditRowEresAgnet", auditRowEresAgnet);
			parameters.put("auditRowEpatAgent", auditRowEpatAgent);
			
			
			List<CreateMultiPatientStudyStatus> createMPatientStudyStatusList = createMPatientStudyStatuses.getCreateMPatientStudyStatusList();
			List<CreateMultiPatientStudyStatus> createMPatientStudyStatusListChecked = new ArrayList<CreateMultiPatientStudyStatus>();
		//	List<PatStudyStatBean> patStudyStatB = new ArrayList<PatStudyStatBean>();
			List<PatStudyStatBean> patStudyStatBeanList = new ArrayList<PatStudyStatBean>();
			CreateMultiPatientStudyStatus createMPatientStudyStatus = null;
			PersonBean personBean = null;
			PatStudyStatBean patStudystatBean =null;
			PatProtBean patProtBean=null;
			Map<String, Object> patientData=null;
			Integer studyPK =null;
			Integer patientPK =null;
			ArrayList<Integer> siteToEnroll=null;
			List<Map<String, Object>> patDataList=new ArrayList<Map<String,Object>>();
			PatientEnrollmentDetails studyPatientStatusDetails=null;
			PatientIdentifier patientIdentifier=null; StudyIdentifier studyIdentifier=null;
			for(CreateMultiPatientStudyStatus patientStudyStatus : createMPatientStudyStatusList)
			{
				studyPatientStatusDetails=null;
				studyPatientStatusDetails=patientStudyStatus.getPatientEnrollmentDetails();
				patientIdentifier=patientStudyStatus.getPatientIdentifier(); 
				studyIdentifier=patientStudyStatus.getStudyIdentifier();
				if(studyPatientStatusDetails == null)
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "patientEnrollmentDetails is required to addStudyPatientStatus"));
					//throw new OperationException();
					continue;
				}

				if((patientIdentifier==null || (StringUtil.isEmpty(patientIdentifier.getOID())
														&& 	(StringUtil.isEmpty(patientIdentifier.getPatientId()) 
																|| (patientIdentifier.getOrganizationId()==null 
																	|| (StringUtil.isEmpty(patientIdentifier.getOrganizationId().getOID())
																		&& StringUtil.isEmpty(patientIdentifier.getOrganizationId().getSiteName())
																		&& StringUtil.isEmpty(patientIdentifier.getOrganizationId().getSiteAltId())
																		&& (patientIdentifier.getOrganizationId().getPK()==null || patientIdentifier.getOrganizationId().getPK()==0))))
														&&(patientIdentifier.getPK()==null || patientIdentifier.getPK()==0))))
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid patientIdentifier is required to addStudyPatientStatus"));
					//throw new OperationException();
					continue;
				}
				if(studyIdentifier==null || (StringUtil.isEmpty(studyIdentifier.getOID()) 
													&& (studyIdentifier.getPK()==null || studyIdentifier.getPK()<=0) 
													&& StringUtil.isEmpty(studyIdentifier.getStudyNumber())))
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to addStudyPatientStatus"));
					//throw new OperationException();
					continue;
				}
				// get StudyPK for checking Team rights for Study
				studyPK=null;
				studyPK = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier, objectMapService); 
				if(studyPK == null || studyPK == 0){
					addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier OID:" + studyIdentifier.getOID() + " StudyNumber " + studyIdentifier.getStudyNumber()));
					//throw new OperationException();
					continue;
				}

				TeamAuthModule teamAuth = new TeamAuthModule(callingUser.getUserId(), studyPK); 
				Integer managePatientEnrollPriv = teamAuth.getPatientManagePrivileges(); 
				boolean hasEnrollPatientPriv = TeamAuthModule.hasNewPermission(managePatientEnrollPriv); 

				if(!hasEnrollPatientPriv)
				{
					addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User Not Authorized to add patient study status"));
					//throw new AuthorizationException("User Not Authorized to enroll patient");
					//throw new OperationException();
					continue;
				}

				

				patientPK =null;
				try {
					patientPK = ObjectLocator.personPKFromPatientIdentifier(callingUser, patientIdentifier, objectMapService);
				} catch (MultipleObjectsFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				personBean =null;
				personBean = personAgent.getPersonDetails(patientPK);
				if(personBean==null)
				{
					((ResponseHolder)parameters.get("ResponseHolder")).addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND,"Patient Not found"));
					//throw new OperationException();
					continue;
				}
			
				UserSiteDao userSiteDAO = new UserSiteDao(); 
				
				if(userSiteDAO.getMaxRightForStudyPatient(callingUser.getUserId(), patientPK, studyPK)== 0)
				{
					addIssue(new Issue(IssueTypes.ORGANIZATION_AUTHORIZATION, "User not authorized to create new StudyPatientStatus for this Study and Patient")); 
					//throw new OperationException();
					continue;
				}
				
				
				
				System.out.println("-----------"+studyPK+"-----"+patientPK+"----------");
				patProtBean=null;
				patProtBean=patProtAgent.findCurrentPatProtDetails(studyPK,
						patientPK);
				//Bug #: 15430 Raman
				if(patProtBean.getPatProtId() == 0)
			    {
			     addIssue(new Issue(IssueTypes.PATIENT_NOT_ONSTUDY, "Given patient is not linked to study provided")); 
			     //throw new OperationException();
				 continue;
			    }
				patientData=null;
				patientData = new HashMap<String,Object>(); 
				
				PatFacilityDao patFacilityDao = patFacilityAgent.getPatientFacilitiesSiteIds(patientPK); 
				siteToEnroll=null;
				siteToEnroll = new ArrayList<Integer>(); 
				
				for( int j = 0; j < patFacilityDao.getPatientSite().size(); j++)
				{
					siteToEnroll.add(StringUtil.stringToInteger(patFacilityDao.getPatientSite().get(j).toString())); 
				}
				
				patientData.put("patientOrganizations", siteToEnroll); 
				patientData.put("patientDOB",  personBean.getPersonDb()); 
				patientData.put("patientCode", personBean.getPersonPId()); 
				
				patientData.put("patProtBean", patProtBean);
				patientData.put("patientPK", patientPK);
				patientData.put("studyPK", studyPK);
				patientData.put("patientEnrollmentDetails", studyPatientStatusDetails);
				
				
				
				patDataList.add(patientData);
			}
			PatientEnrollmentHandler handler = new PatientEnrollmentHandler(); 
			String[] values=handler.createMStudyPatientStatus(patDataList, parameters);
			for( int i = 0; i < values.length; i++ )
			{
				int ret=StringUtil.stringToNum(values[i]);
				System.out.println("Returned is "+ret);
				if(ret>0)
				{
					ObjectMap patProtStatusMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PATSTUDYSTAT, ret); 
					PatientStudyStatusIdentifier patStudyStatusIdentifier = new PatientStudyStatusIdentifier(); 
					patStudyStatusIdentifier.setOID(patProtStatusMap.getOID()); 
					patStudyStatusIdentifier.setPK(ret);
					response.addObjectCreatedAction(patStudyStatusIdentifier);
				}
				else
				{
					response.addIssue(new Issue(IssueTypes.ERROR_CREATE_PATIENT_ENROLLMENT, values[i]));
				}
			}
			
		}
		catch(OperationException e){
			if (logger.isDebugEnabled()) logger.debug("StudyPatientServiceImpl getStudPatientStatusHistory", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyPatientServiceImpl getStudPatientStatusHistory", t);
			throw new OperationException(t);
		}
		// TODO Auto-generated method stub
		return response;
	}


	@Override
	public ResponseHolder updateMPatientStudyStatus(
			UpdateMPatientStudyStatuses updateMPatientStudyStatuses)
			throws OperationException {
		try
		{
		
			//--------Checking group authorization-------------//
			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent); 
			Integer patientManagmentAccess = authModule.getAppManagePatientsPrivileges(); 
			if(!GroupAuthModule.hasEditPermission(patientManagmentAccess))
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User is not authorized to edit Patient details")); 
				throw new AuthorizationException(); 
			}

			Integer manageProtocolPriv = 
					authModule.getAppManageProtocolsPrivileges();

			boolean hasEditManageProt = 
				GroupAuthModule.hasEditPermission(manageProtocolPriv);
			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
			if (!hasEditManageProt){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to edit studies "));
				throw new AuthorizationException("User Not Authorized to edit studies");
			}
			
			
			if(updateMPatientStudyStatuses == null)
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid updateMPatientStudyStatusesList is required")); 
				throw new OperationException(); 
			}
			if(updateMPatientStudyStatuses.getUpdateMPatientStudyStatus()== null)
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid updateMPatientStudyStatusesList is required")); 
				throw new OperationException(); 
			}
		
			Map<String, Object> parameters = new HashMap<String, Object>(); 
			parameters.put("patStudyStatAgent", patStudyStatAgent);
			parameters.put("personAgent", personAgent);
			parameters.put("callingUser", callingUser);
			parameters.put("response", response);
			parameters.put("sessionContext", sessionContext);
			parameters.put("objectMapService", objectMapService);
			parameters.put("patFacilityAgent", patFacilityAgent);
			parameters.put("userAgent", userAgent);
			parameters.put("siteAgent", siteAgent);
			parameters.put("studyAgent", studyAgent);
			parameters.put("auditRowEresAgnet", auditRowEresAgnet);
			parameters.put("auditRowEpatAgent", auditRowEpatAgent);
			
			
			List<UpdateMStudyPatientStatus> updateMPatientStudyStatus = updateMPatientStudyStatuses.getUpdateMPatientStudyStatus();
			List<UpdateMStudyPatientStatus> updateMPatientStudyStatusListChecked = new ArrayList<UpdateMStudyPatientStatus>();
			List<PatStudyStatBean> patStudyStatBeanList = new ArrayList<PatStudyStatBean>();
				
			Integer studyPK = 0;
			Integer perPK = 0;
			for(UpdateMStudyPatientStatus updateMPatStdyStatus : updateMPatientStudyStatus)
			{
				ObjectMap objectMap =null;
				Integer patStudyPK=0;
				if(updateMPatStdyStatus.getPatientStudyStatusIdentifier()== null)
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientStudyStatusIdentifier with OID or PK is required.")); 
					//throw new OperationException();
					continue;
				}	
				if(updateMPatStdyStatus.getPatientStudyStatusIdentifier()!= null)
				{	
					if(!StringUtil.isEmpty(updateMPatStdyStatus.getPatientStudyStatusIdentifier().getOID()))
					{
						objectMap = objectMapService.getObjectMapFromId(updateMPatStdyStatus.getPatientStudyStatusIdentifier().getOID());
						if(objectMap == null || !(objectMap.getTableName().equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_PATSTUDYSTAT)))
						{
							addIssue(new Issue(IssueTypes.STUDY_PATIENT_STATUS_NOT_FOUND, "Please send valid PatientStudyStatusIdentifier"));
							//throw new OperationException();
							continue;
						}
						patStudyPK=objectMap.getTablePK();
							
					}
				}
				if(updateMPatStdyStatus.getPatientStudyStatusIdentifier().getPK()!=null && updateMPatStdyStatus.getPatientStudyStatusIdentifier().getPK()!=0)
				{
					patStudyPK = updateMPatStdyStatus.getPatientStudyStatusIdentifier().getPK();
				}
				PatStudyStatBean patStudyStatBean=patStudyStatAgent.getPatStudyStatDetails(patStudyPK.intValue());
				if(patStudyStatBean==null)
				{
					addIssue(new Issue(IssueTypes.STUDY_PATIENT_STATUS_NOT_FOUND, "Study Patient status not found for :"+patStudyPK));
				//	throw new OperationException();
					continue;
					
				}
				studyPK=StringUtil.stringToInteger(patStudyStatBean.getStudyId());
				perPK=StringUtil.stringToInteger(patStudyStatBean.getPatientId());
						
				parameters.put("studyPK",studyPK);
				parameters.put("perPK", perPK);
				parameters.put("patStudyPK", patStudyPK);
				
				//---------check patient level access------------//
				Integer userMaxStudyPatientRight = userSiteAgent.getMaxRightForStudyPatient(callingUser.getUserId(), perPK, studyPK);
				if(userMaxStudyPatientRight == 0)
				{
					addIssue(new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION, "User is not authorized to access patient's study site")); 
					//throw new OperationException();
					continue;
				}
				//-----------check Study level access------------//
				TeamAuthModule teamAuth = new TeamAuthModule(callingUser.getUserId(), studyPK); 
				Integer studyPatientManagement = teamAuth.getPatientManagePrivileges();
				if(!TeamAuthModule.hasEditPermission(studyPatientManagement))
				{
					addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User is not authorized to edit study patient status")); 
					//throw new OperationException(); 
					continue;
				}
			
				updateMPatientStudyStatusListChecked.add(updateMPatStdyStatus);
				patStudyStatBeanList.add(patStudyStatBean);
				
			}
			
			if(updateMPatientStudyStatusListChecked.size()>0 && patStudyStatBeanList.size()>0)
			{	
				PatientEnrollmentHandler patientEnrollmentHandler=new PatientEnrollmentHandler();
				patientEnrollmentHandler.updateMStudyPatientStatus(patStudyStatBeanList,updateMPatientStudyStatusListChecked,parameters);
			}
					
		}
		catch (OperationException e) {
			try {e.printStackTrace();
				//this.sessionContext.getUserTransaction().setRollbackOnly();
			} catch (Exception ex) {
				throw new OperationException(ex);
			};
			if (logger.isDebugEnabled())
				logger.debug("PatientDemographicsServiceImpl create", e);
			throw new OperationRolledBackException(this.response.getIssues());
		} catch (Throwable t) {
			try {
				t.printStackTrace();
				//this.sessionContext.getUserTransaction().setRollbackOnly();
			} catch (Exception ex) {
				throw new OperationException(ex);
			}
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled())
				logger.debug("PatientDemographicsServiceImpl create", t);
			throw new OperationRolledBackException(this.response.getIssues());
		}
		// TODO Auto-generated method stub
		return response;
	}

	
}