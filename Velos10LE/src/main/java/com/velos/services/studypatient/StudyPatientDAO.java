package com.velos.services.studypatient;

import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CommonDAO;
import com.velos.esch.service.util.StringUtil;
import com.velos.services.OperationException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.Code;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientStudyStatusIdentifier;
import com.velos.services.model.StudyPatient;
import com.velos.services.model.StudyPatientStatus;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ServicesUtil;

public class StudyPatientDAO extends CommonDAO{
	
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(StudyPatientDAO.class);
	
	public static String getSQLString(){
		StringBuilder sbSQL = new StringBuilder();
		sbSQL.append(" Select distinct p.rowid as rowcount,erv.fk_per, p.person_lname as mask_person_lname, p.person_fname as mask_person_fname, "); 
		sbSQL.append(" erv.per_code,");
		sbSQL.append(" erv.PATSTUDYSTAT_SUBTYPE, ");
		sbSQL.append(" (select site_name from ER_SITE where pk_site = p.fk_site) site_name,");  
		sbSQL.append(" erv.patprot_enroldt as patprot_enroldt_datesort,"); 
		sbSQL.append(" erv.last_visit_name,erv.cur_visit, erv.cur_visit_date, erv.next_visit as next_visit_datesort, ");
		sbSQL.append(" lower(p.person_lname || p.person_fname) mask_patnamelower,lower(erv.PATPROT_PATSTDID) lowerpatstdid,erv.PATPROT_PATSTDID,");
		sbSQL.append(" assignedto_name,physician_name,enrolledby_name,treatingorg_name");
//		sbSQL.append(" (select tx_name from er_studytxarm where pk_studytxarm=(select fk_studytxarm from er_pattxarm z where Z.FK_PATPROT=erv.PK_PATPROT and pk_pattxarm = (  ") ;
//		sbSQL.append(" select max(pk_pattxarm) from er_pattxarm z where tx_start_date=(select max(tx_start_date) from er_pattxarm y where y.FK_PATPROT=erv.PK_PATPROT ) and Z.FK_PATPROT=erv.PK_PATPROT ) )  )  current_tx_arm,") ;
		sbSQL.append("  from erv_studypat_by_visit erv ,  erv_person p where P.pk_person = erv.fk_per ");
		sbSQL.append(" and erv.fk_study = ? ");
        sbSQL.append(" and exists (select * from ER_PATFACILITY fac where fac.fk_per = erv.fk_per  ");
        sbSQL.append(" and fac.fk_site in ( ? ) and fac.patfacility_accessright > 0 ) ");
		String masterSql = sbSQL.toString();
		return masterSql;
	}
	

	
	public static List<StudyPatient> getStudyPatientByStudyPK(Integer studyPK, Integer sitePK) throws OperationException {
		
		PreparedStatement pstmt = null;
		Connection conn = null;
		ArrayList<StudyPatient> lstStudyPatient = new ArrayList<StudyPatient>();
		
		try{
			
			conn = getConnection();

			//if (logger.isDebugEnabled()) logger.debug(" sql:" + sql);
			StudyPatient studyPatient = null;
			pstmt = conn.prepareStatement(getSQLString());
			pstmt.setInt(1, studyPK);
			pstmt.setInt(2, sitePK);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				studyPatient =  new StudyPatient();
				PatientIdentifier patientIdentifier = new PatientIdentifier();
				//create or get person object map for studyPatient
				ObjectMapService objectMapService = ServicesUtil.getObjectMapService(); 
				ObjectMap obj = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PERSON, rs.getInt("fk_per"));
				String strOID = obj.getOID();
				
				patientIdentifier.setPatientId(rs.getString("per_code"));
				patientIdentifier.setOID(strOID);
				patientIdentifier.setPK( rs.getInt("fk_per"));
				studyPatient.setPatientIdentifier(patientIdentifier);
				
				studyPatient.setStudyPatId(rs.getString("PATPROT_PATSTDID"));
				studyPatient.setStudyPatFirstName(rs.getString("mask_person_fname"));
				studyPatient.setStudyPatLastName(rs.getString("mask_person_lname"));
				lstStudyPatient.add(studyPatient);
			}
			
		}
		catch(Throwable t){
			t.printStackTrace();
			throw new OperationException();
			
		}
		finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}

		}
		return lstStudyPatient;
		
	}
	
	
	public static List<Integer> getAllPatients(Integer userID) throws OperationException
	{
		PreparedStatement stmt = null ;
		ResultSet rs = null; 
		Connection conn = null; 
		ArrayList<Integer> patientList = new ArrayList<Integer>(); 
		
		try
		{
			StringBuffer sql = new StringBuffer(); 
			sql.append("select fk_per from ER_PATFACILITY fac, er_usersite usr" ); 
			sql.append(" where usersite_right>= ? AND usr.fk_site = fac.fk_site"); 
			sql.append(" and fk_user = ?  AND fac.patfacility_accessright > ?"); 
			
			conn = getConnection(); 

			stmt = conn.prepareStatement(sql.toString()); 
			stmt.setInt(1, 4); 
			stmt.setInt(2, userID);
			stmt.setInt(3, 0); 

			rs = stmt.executeQuery(); 

			while(rs.next())
			{
				patientList.add(rs.getInt(1)); 
			}

		}catch(SQLException sqe)
		{
			sqe.printStackTrace(); 
			throw new OperationException(); 
		}
		finally {
			try {
				if (rs != null)
					rs.close();
			} catch (Exception e) {
			}
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}

		}
		
		return patientList; 
	}

	public String getPatientStatusHistoryString(){
		StringBuilder sbSQL = new StringBuilder();
		sbSQL.append("select PK_PATSTUDYSTAT, FK_CODELST_STAT, PATSTUDYSTAT_DATE, ");
		sbSQL.append(" PATSTUDYSTAT_ENDT, PATSTUDYSTAT_NOTE, er_codelst.codelst_desc, ");
		sbSQL.append(" patstudystat_reason, CURRENT_STAT ");
		sbSQL.append(" from er_patstudystat,  er_codelst  ");
		sbSQL.append(" where fk_per = ? and fk_study = ? and FK_CODELST_STAT = pk_codelst order by  PATSTUDYSTAT_DATE DESC,PK_PATSTUDYSTAT DESC  ");
		String masterSql = sbSQL.toString();
		return masterSql;
	}
	
	
	private static String getCurrentPatientStatusSQL(){
		StringBuilder sbSQL = new StringBuilder();
		sbSQL.append("select PK_PATSTUDYSTAT");
		sbSQL.append(" from er_patstudystat ");
		//sbSQL.append(" where fk_per = ? and fk_study = ? and current_stat = 1 and PATSTUDYSTAT_ENDT is null");		
		sbSQL.append(" where fk_per = ? and fk_study = ? and current_stat = 1 ");		
		return sbSQL.toString();
	}

	
	public List<StudyPatientStatus> getPatStatusHistory(Integer patPk, Integer studyPk, Integer accId) throws OperationException {
		
		PreparedStatement pstmt = null;
		Connection conn = null;
		ArrayList<StudyPatientStatus> lstPatientStat = new ArrayList<StudyPatientStatus>();
		
		try{
			boolean isCurrentStatStr = false;
			conn = getConnection();
			CodeCache codeCache = CodeCache.getInstance();
			StudyPatientStatus studyPatientStatus = null;
			pstmt = conn.prepareStatement(getPatientStatusHistoryString());
			pstmt.setInt(1, patPk);
			pstmt.setInt(2, studyPk);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				studyPatientStatus =  new StudyPatientStatus();
				
				ObjectMapService  mapService = ServicesUtil.getObjectMapService();
				PatientStudyStatusIdentifier studyPatStatId = new PatientStudyStatusIdentifier(); 
				ObjectMap map1 = mapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PATSTUDYSTAT, rs.getInt("PK_PATSTUDYSTAT"));
				studyPatStatId.setOID(map1.getOID());
				studyPatStatId.setPK(rs.getInt("PK_PATSTUDYSTAT"));
				studyPatientStatus.setStudyPatStatId(studyPatStatId); 
				
				Code studyStatusCode = 
					codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATPROT_STATUS, 
							rs.getInt("FK_CODELST_STAT"),
							accId);
				
				studyPatientStatus.setStudyPatStatus(studyStatusCode);
				studyPatientStatus.setStatusDate(rs.getDate(3));
				studyPatientStatus.setStatusNote(rs.getString(5));

				Code statusReasonCode = 
					codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_FOLLOWUP, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_ENROLL_APPR, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_ENROLL_PENDING, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_ENROLL_DENIED, 
							rs.getInt("patstudystat_reason"),
							accId);
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_OFFSTUDY, 
							rs.getInt("patstudystat_reason"),
							accId);
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_OFFTREAT, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				//
				if(statusReasonCode == null) 
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_INFCONSENT, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_BEGSTUDYACT, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_SCRFAIL, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_ENROLLED, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_ENDBILLING, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_COMPSTUDY, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_ACTIVESTUDY, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_WITHDRAWN, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_NOTMEETELIGIBLE, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_PHYSDICRETION, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_WITHDREWCON, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				
				//
				
				
					
				studyPatientStatus.setStatusReason(statusReasonCode);
				
				isCurrentStatStr = rs.getInt("CURRENT_STAT") == 1 ? true : false;
				studyPatientStatus.setCurrentStatus(isCurrentStatStr);
				
				lstPatientStat.add(studyPatientStatus);
			}
		}
		catch(Throwable t){
			t.printStackTrace();
			throw new OperationException();
			
		}
		finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}

		}
		return lstPatientStat;
		
	}
	
public static int getCurrentPatientStatus(Integer patPk, Integer studyPk) throws OperationException {
		
		PreparedStatement pstmt = null;
		Connection conn = null;
		int currentStudyPatStatus = 0;
		
		try{
			boolean isCurrentStatStr = false;
			conn = getConnection();
			CodeCache codeCache = CodeCache.getInstance();
			StudyPatientStatus studyPatientStatus = null;
			pstmt = conn.prepareStatement(getCurrentPatientStatusSQL());
			pstmt.setInt(1, patPk);
			pstmt.setInt(2, studyPk);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
			   currentStudyPatStatus = rs.getInt(1); 
			}
				
		}
		catch(Throwable t){
			t.printStackTrace();
			throw new OperationException();
			
		}
		finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}

		}
		return currentStudyPatStatus;
		
	}



public static String[] updateMStudyPatientStatus(List<String> updateSQL,List<String> pKSQL) throws OperationException,SQLException
{
	// TODO Auto-generated method stub
	
	Connection conn = null;
	CallableStatement csmt = null;
	
	try
	{
		conn = getConnection();
		conn.setAutoCommit(false);
			
		String [] updateString = new String[updateSQL.size()];
		updateString = updateSQL.toArray(updateString);
				
		String [] pk_sql = new String[pKSQL.size()];
		pk_sql = pKSQL.toArray(pk_sql);
		
       ArrayDescriptor des = ArrayDescriptor.createDescriptor("ERES.ARRAY_STRING", conn);
       
       ARRAY passing_array = new ARRAY(des,conn,updateString);
       ARRAY pk_sql_to_pass = new ARRAY(des,conn,pk_sql);
         
        csmt = conn.prepareCall("call ERES.SP_UPDATE_MUL_STDY_PAT_STATUS(?,?,?)");
     // Passing an arrays to the procedure -
        csmt.setArray(1, passing_array);
        csmt.setArray(2, pk_sql_to_pass);
        csmt.registerOutParameter(3,OracleTypes.ARRAY,"ERES.ARRAY_STRING");
        csmt.execute();
        
        // Retrieving array from the result set of the procedure after execution -
        ARRAY arr1 = ((OracleCallableStatement)csmt).getARRAY(3);
        String[] receivedArray = (String[])(arr1.getArray());

     	return receivedArray;
	}
	catch(SQLException se){
		se.printStackTrace();
		//conn.rollback();
		throw new OperationException(se.getMessage());
		
	}
	finally {
		try {
			if (csmt != null)
				csmt.close();
		} catch (Exception e) {
		}
		try {
			if (conn != null)
				conn.close();
		} catch (Exception e) {
		}

	}
}

private static final String STR_ZERO = "0";

private String executeConstructedSqls(Connection conn, String person, 
		String patProt, String patStudyStat) {
	PreparedStatement personStmt = null;
	int updateCount = 0;
	try {
		personStmt = conn.prepareStatement(person);
		updateCount = personStmt.executeUpdate();
	} catch (Exception e) {
		logger.fatal("In StudyPatientDAO.executeConstructedSqls, this personStmt failed to run: "+person+" due to "+e);
		updateCount = -1;
	} finally {
		try {
			personStmt.close();
		} catch(Exception e) {}
	}
	if (updateCount < 0) {
		return STR_ZERO;
	}
	
	PreparedStatement patProtStmt = null;
	try {
		patProtStmt = conn.prepareStatement(patProt);
		updateCount = patProtStmt.executeUpdate();
	} catch (Exception e) {
		logger.fatal("In StudyPatientDAO.executeConstructedSqls, this patProtStmt failed to run: "+patProt+" due to "+e);
		updateCount = -1;
	} finally {
		try {
			patProtStmt.close();
		} catch(Exception e) {}
	}
	if (updateCount < 0) {
		return STR_ZERO;
	}
	
	int nextPk = -1;
	PreparedStatement patStudyStatPkStmt = null;
	try {
		patStudyStatPkStmt = conn.prepareStatement(" select SEQ_ER_PATSTUDYSTAT.NEXTVAL from dual ");
		ResultSet rs = patStudyStatPkStmt.executeQuery();
		while(rs.next()) {
			nextPk = rs.getInt(1);
		}
	} catch (Exception e) {
		logger.fatal("In StudyPatientDAO.executeConstructedSqls, could not get SEQ_ER_PATSTUDYSTAT.NEXTVAL:"+e);
		nextPk = -1;
	}
	if (nextPk < 0) {
		return STR_ZERO;
	}
	
	PreparedStatement patStudyStatStmt = null;
	try {
		patStudyStatStmt = conn.prepareStatement(patStudyStat.replace(":1", " ? "));
		patStudyStatStmt.setInt(1, nextPk);
		updateCount = patStudyStatStmt.executeUpdate();
	} catch (Exception e) {
		logger.fatal("In StudyPatientDAO.executeConstructedSqls, this patStudyStatStmt failed to run: "+patStudyStat+" due to "+e);
		updateCount = -1;
	} finally {
		try {
			patStudyStatStmt.close();
		} catch(Exception e) {}
	}
	if (updateCount < 0) {
		return STR_ZERO;
	}
	return String.valueOf(nextPk);
}

private static final String findEnrolledInCodelstSql = " select PK_CODELST from ER_CODELST where "
		+"CODELST_TYPE = 'patStatus' and CODELST_SUBTYP = 'enrolled' ";
private static final String findPreviouslyEnrolledStatusSql = " select PK_PATSTUDYSTAT from ERES.ER_PATSTUDYSTAT where "
		+" FK_PER = ? and FK_STUDY = ? and FK_CODELST_STAT = ? and PK_PATSTUDYSTAT <> ? ";
private static final String findOneOfPostEnrolledStatusesInCodelst = "select CODELST_SUBTYP from ER_CODELST where "
		+ " CODELST_TYPE = 'patStatus' and  CODELST_SUBTYP in ('active', 'followup','offtreat','offstudy','lockdown')"
		+ " and PK_CODELST = ? ";

private String handlePreviouslyEnrolledStatus(Connection conn, String newlyCreatedPk, String p_fkArr) {
	String[] inputParts = p_fkArr.split(",");
	int pkCodelstEnrolled = 0;
	PreparedStatement findEnrolledStmt = null;
	try {
		findEnrolledStmt =  conn.prepareStatement(findEnrolledInCodelstSql);
		ResultSet rs = findEnrolledStmt.executeQuery();
		while(rs.next()) {
			pkCodelstEnrolled = rs.getInt(1);
		}
	} catch(Exception e) {
		pkCodelstEnrolled = 0;
	} finally {
		try { findEnrolledStmt.close(); } catch(Exception e) {}
	}
	
	if (pkCodelstEnrolled < 1) {
		return "The codelist for the patStatus type is defective.";
	}
	
	PreparedStatement findPreviouslyEnrolledStmt = null;
	int pkPatStudyStatOfPreviouslyEnrolled = 0;
	try {
		findPreviouslyEnrolledStmt = conn.prepareStatement(findPreviouslyEnrolledStatusSql);
		findPreviouslyEnrolledStmt.setInt(1, StringUtil.stringToNum(inputParts[1]));
		findPreviouslyEnrolledStmt.setInt(2, StringUtil.stringToNum(inputParts[2]));
		findPreviouslyEnrolledStmt.setInt(3, pkCodelstEnrolled);
		findPreviouslyEnrolledStmt.setInt(4, StringUtil.stringToNum(newlyCreatedPk));
		ResultSet rs = findPreviouslyEnrolledStmt.executeQuery();
		while(rs.next()) {
			pkPatStudyStatOfPreviouslyEnrolled = rs.getInt(1);
		}
	} catch(Exception e) {
		logger.fatal("In StudyPatientDAO.handlePreviouslyEnrolledStatus, faield to run findPreviouslyEnrolledStatusSql due to "+e);
		pkPatStudyStatOfPreviouslyEnrolled = 0;
	} finally {
		try { findPreviouslyEnrolledStmt.close(); } catch(Exception e) {}
	}
	
	// If the newly created status is 'enrolled', it must not have a previous 'enrolled' status
	if (StringUtil.stringToNum(inputParts[0]) == pkCodelstEnrolled
			&& pkPatStudyStatOfPreviouslyEnrolled > 0) {
		return "Cannot add 'enrolled' patient study status because a previous 'enrolled' status already exists for fkPer="
			+inputParts[1]+" and fkStudy="+inputParts[2];
	}
	
	// If the newly created status is one of ('active', 'followup', 'offtreat', 'offstudy', 'lockdown'),
	// then it must have a previously enrolled status
	PreparedStatement findOneOfPostEnrolledStmt = null;
	String subTypeOfOneOfPostEnrolled = null;
	try {
		findOneOfPostEnrolledStmt = conn.prepareStatement(findOneOfPostEnrolledStatusesInCodelst);
		findOneOfPostEnrolledStmt.setInt(1, StringUtil.stringToNum(inputParts[0]));
		ResultSet rs = findOneOfPostEnrolledStmt.executeQuery();
		while(rs.next()) {
			subTypeOfOneOfPostEnrolled = rs.getString(1);
		}
	} catch(Exception e) {
		subTypeOfOneOfPostEnrolled = null;
	} finally {
		try { findOneOfPostEnrolledStmt.close(); } catch(Exception e) {}
	}
	if (subTypeOfOneOfPostEnrolled != null && pkPatStudyStatOfPreviouslyEnrolled < 1) {
		return "Cannot add '"+subTypeOfOneOfPostEnrolled+"' patient study status for fkPer="
				+inputParts[1]+" and fkStudy="+inputParts[2]+" because it does not have a previous 'enrolled' status";
	}
	
	return newlyCreatedPk;
}

private static final String checkCurrentStatusSql = " select CURRENT_STAT from ER_PATSTUDYSTAT where PK_PATSTUDYSTAT = ? ";

private String handleCurrentStatus(Connection conn, String newlyCreatedPk, String p_fkArr, int userId) {
	String[] inputParts = p_fkArr.split(",");
	String retString = null;
	PreparedStatement checkCurrentStatusStmt = null;
	int isCurrentStatus = 0;
	try {
		checkCurrentStatusStmt = conn.prepareStatement(checkCurrentStatusSql);
		checkCurrentStatusStmt.setInt(1, StringUtil.stringToNum(newlyCreatedPk));
		ResultSet rs = checkCurrentStatusStmt.executeQuery();
		while(rs.next()) {
			isCurrentStatus = rs.getInt(1);
		}
	} catch(Exception e) {
		logger.fatal("In StudyPatientDAO.handleCurrentStatus, checkCurrentStatusStmt failed:"+e);
		isCurrentStatus = 0;
	} finally {
		try { checkCurrentStatusStmt.close(); } catch(Exception e) {}
	}
	
	if (isCurrentStatus < 1) {
		return newlyCreatedPk; // No need to update anything for current status; just return
	}
	
	// The newly created status is set as the "current status" => change the end date 
	// and last modified date and last modified by of the previous "current status"
	CallableStatement cstmt = null;
	int outCstmt = 0;
	try {
		cstmt = conn.prepareCall("{call SP_UPDATE_STATDT_PATSTUDYSTAT(?,?,?,?,?)}");
		cstmt.setInt(1, StringUtil.stringToNum(newlyCreatedPk));
		cstmt.setInt(2, StringUtil.stringToNum(inputParts[1]));
		cstmt.setInt(3, StringUtil.stringToNum(inputParts[2]));
		cstmt.setInt(4, userId);
		cstmt.registerOutParameter(5, OracleTypes.INTEGER);
		cstmt.execute();
		cstmt.getInt(5);
	} catch(Exception e) {
		logger.fatal("In StudyPatientDAO.handleCurrentStatus, SP_UPDATE_STATDT_PATSTUDYSTAT faied:"+e);
	} finally {
		try { cstmt.close(); } catch(Exception e) {}
	}
	if (outCstmt < 0) {
		logger.warn("In StudyPatientDAO.handleCurrentStatus, SP_UPDATE_STATDT_PATSTUDYSTAT returned:"+outCstmt);
	}
	
	return newlyCreatedPk;
}


public String[] createMStudyPatientStatus(String[] person, String[] patProt, String[] patStudyStat, String[] p_fkArr, int userId) throws OperationException
{
	String[] retStrings = new String[patStudyStat.length];
	if (patStudyStat == null || patStudyStat.length < 1) {
		return null;
	}
	
	// Get connection unset auto-commit
	Connection conn = null;
	try {
		conn = getConnection();
		conn.setAutoCommit(false);
	} catch (Exception e) {
		logger.fatal("In StudyPatientDAO.createMStudyPatientStatus, failed to get connection:"+e);
		return null;
	}
	
	// Start of main loop
	for (int iX = 0; iX < patStudyStat.length; iX++) {
		// Execute constructed SQLs
		retStrings[iX] = this.executeConstructedSqls(conn, person[iX], patProt[iX], patStudyStat[iX]);
		// retStrings[iX] = "388"; // test data
		String[] inputParts = p_fkArr[iX].split(",");
		
		// If last execution failed, rollback and skip
		if (STR_ZERO.equals(retStrings[iX])) {
			try {
				conn.rollback();
			} catch(Exception e) {
				logger.fatal("In StudyPatientDAO.createMStudyPatientStatus, failed to rollback for pkPer="
					+inputParts[1]+" and pkStudy="+inputParts[2]+" and status code="+inputParts[0]+" due to "+e);
				break;
			}
			retStrings[iX] += "; no action taken on record #"+(iX+1)+".";
			continue;
		}
		
		// Handle previously enrolled status
		retStrings[iX] = this.handlePreviouslyEnrolledStatus(conn, retStrings[iX], p_fkArr[iX]);
		
		// If last execution failed, rollback and skip
		if (StringUtil.stringToNum(retStrings[iX]) < 1) {
			try {
				conn.rollback();
			} catch(Exception e) {
				logger.fatal("In StudyPatientDAO.createMStudyPatientStatus, failed to rollback for pkPer="
					+inputParts[1]+" and pkStudy="+inputParts[2]+" and status code="+inputParts[0]+" due to "+e);
				break;
			}
			retStrings[iX] += "; no action taken on record #"+(iX+1)+".";
			continue;
		}
		
		// Handle current status
		retStrings[iX] = this.handleCurrentStatus(conn, retStrings[iX], p_fkArr[iX], userId);

		// If last execution failed, rollback and skip
		if (StringUtil.stringToNum(retStrings[iX]) < 1) {
			try {
				conn.rollback();
			} catch(Exception e) {
				logger.fatal("In StudyPatientDAO.createMStudyPatientStatus, failed to rollback for pkPer="
					+inputParts[1]+" and pkStudy="+inputParts[2]+" and status code="+inputParts[0]+" due to "+e);
				break;
			}
			retStrings[iX] += "; no action taken on record #"+(iX+1)+".";
			continue;
		}

		// Commit after each iteration
		try {
			conn.commit();
		} catch(Exception e) {
			logger.fatal("In StudyPatientDAO.createMStudyPatientStatus, failed to commit for pkPer="
					+inputParts[1]+" and pkStudy="+inputParts[2]+" and status code="+inputParts[0]+" due to "+e);
		}
	}
	// End of main loop
	
	// Close connection
	try {
		if (conn != null) { conn.close(); }
	} catch(Exception e) {}
	
	return retStrings;
}

public String[] createMStudyPatientStatus_old(String[] person, String[] patProt, String[] patStudyStat, String[] p_fkArr) throws OperationException
{
	Array ret ;
    CallableStatement cstmt = null;
    Connection conn = null;

    try {
        conn = getConnection();
   
        ArrayDescriptor adPerson = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
        ARRAY personSqls = new ARRAY(adPerson, conn, person);
        
        ArrayDescriptor adPatProt = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
        ARRAY patProtSqls = new ARRAY(adPatProt, conn, patProt);
        
        ArrayDescriptor adPatStudyStat = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
        ARRAY patStudyStatSqls = new ARRAY(adPatStudyStat, conn, patStudyStat);
        
        ArrayDescriptor adFkArr = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
        ARRAY fkArr = new ARRAY(adFkArr, conn, p_fkArr);
        
        cstmt = conn.prepareCall("{call SP_CREATE_MUL_PAT_STUDY_STATUS(?,?,?,?,?)}");
        
        cstmt.setArray(1, personSqls);
        cstmt.setArray(2, patProtSqls);
        cstmt.setArray(3, patStudyStatSqls);
        cstmt.setArray(4, fkArr);
        cstmt.registerOutParameter(5,OracleTypes.ARRAY,"ERES.ARRAY_STRING");
        cstmt.execute();
        ret = ((OracleCallableStatement)cstmt).getARRAY(5);
        //String[] receivedArray = (String[])(arr1.getArray());
        String[] values = (String[])ret.getArray();
        

        return values;

    } catch (Throwable t) {
    	t.printStackTrace();
		throw new OperationException();
    } finally {
        try {
            if (cstmt != null)
                cstmt.close();
        } catch (Exception e) {
        }
        try {
            if (conn != null)
                conn.close();
        } catch (Exception e) {
        }
    }
    
}
//Bug Fix : 16693
public static boolean ifStudyExists(int studyPK) throws OperationException{
	
	String sql = "select count(*) as Count from er_study where pk_study = ?";
	PreparedStatement pstmt  = null;
	Connection conn =  null;
	boolean flag = false;
	try{
		conn = getConnection();
		pstmt = conn.prepareStatement(sql);
    	pstmt.setInt(1, studyPK);
    	
    	ResultSet rs = pstmt.executeQuery();
    			
    	while(rs.next()){
    		if(rs.getInt("Count") ==1)
			   flag = true;
			else 
			   flag =  false;
    	}
    	return flag;
	}catch(Throwable t){
		t.printStackTrace();
		throw new OperationException();
	}
	
	finally{
		try{
			if(pstmt!=null){
				pstmt.close();
			}
		}catch(Exception e){
			
		}
		try{
			if(conn!=null){
				conn.close();
			}
		}catch(Exception e){
			
		}
	}
}


}
