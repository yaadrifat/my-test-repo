package com.velos.services.client;


import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.json.JSONObject;

import com.velos.services.OperationException;
import com.velos.services.mashup.StudyMashupService;
import com.velos.services.model.FilterParams;
import com.velos.services.model.Modules;
import com.velos.services.model.StudyFilterFormResponse;
import com.velos.services.model.StudyFormResponse;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyMashup;
import com.velos.services.model.StudyMashupDetailsResponse;
import com.velos.services.util.JNDINames;

/***
 * Client Class/Remote object for Study Mashup Services 
 * @author Tarandeep Singh Bali
 *
 */
public class StudyMashupClient {
	
	/**
	 * Invokes the remote object.
	 * @return remote Study Mashup service
	 * @throws OperationException
	 */
	
	public static StudyMashupService getStudyMashupRemote() throws OperationException{
		
		StudyMashupService StudyMashupRemote = null;
		InitialContext ic;
		
		try{
			ic = new InitialContext();
			StudyMashupRemote = (StudyMashupService)ic.lookup(JNDINames.StudyMashupServiceImpl);
		}		catch(NamingException e){
			throw new OperationException(e);
		}
		return StudyMashupRemote;
	}
	
	
	public static StudyMashup getStudyMashup(StudyIdentifier studyIdentifier)
	throws OperationException{
		StudyMashupService studyMashupService = getStudyMashupRemote(); 
		return studyMashupService.getStudyMashup(studyIdentifier);
	}
	
	public static StudyFormResponse getStudyFormResponse(Integer formPK)
	throws OperationException{
		StudyMashupService studyMashupService = getStudyMashupRemote(); 
		return studyMashupService.getStudyFormResponse(formPK);
	}
	
	public static StudyFilterFormResponse getFilterStudyFormResponse(FilterParams formFilterParams)
	throws OperationException{
		StudyMashupService studyMashupService = getStudyMashupRemote(); 
		return studyMashupService.getFilterStudyFormResponse(formFilterParams);
	}
	
	public static StudyMashupDetailsResponse getStudyMashupDetails(StudyIdentifier studyIdentifier, Integer formPK, Modules modules)
	throws OperationException{
		StudyMashupService studyMashupService = getStudyMashupRemote(); 
		return studyMashupService.getStudyMashupDetails(studyIdentifier, formPK, modules);
	}

//	public static StudyMashupDetailsResponse updateStudyMashup(StudyIdentifier studyIdentifier, JSONObject mashupData)
//	throws OperationException{
//		StudyMashupService studyMashupService = getStudyMashupRemote(); 
//		return studyMashupService.updateStudyMashup(studyIdentifier, mashupData);
//	}
}
