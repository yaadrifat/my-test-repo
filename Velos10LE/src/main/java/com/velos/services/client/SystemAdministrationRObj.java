/**
 * Created On Nov 6, 2012
 */
package com.velos.services.client;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.services.OperationException;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.CodeTypes;
import com.velos.services.model.Codes;
import com.velos.services.model.ObjectInfo;
import com.velos.services.model.ObjectMaps;
import com.velos.services.model.ObjectInfos;
import com.velos.services.model.SimpleIdentifier;
import com.velos.services.systemadmin.CodeListService;
import com.velos.services.util.JNDINames;

/**
 * @author Kanwaldeep
 *
 */
public class SystemAdministrationRObj {
	
	private static CodeListService getCodeListRemote() throws OperationException
	{
		CodeListService codeListService = null; 
		InitialContext ic = null; 
		try
		{
			ic = new InitialContext();
			codeListService = (CodeListService) ic.lookup(JNDINames.CodeListServiceImpl); 
			
		}catch(NamingException ne)
		{
			throw new OperationException(ne); 
		}
		
		return codeListService; 
	}
	
	private static ObjectMapService getObjectMapRemote() throws OperationException{
		ObjectMapService objectMapService = null; 
		InitialContext ic = null; 
		try
		{
			ic = new InitialContext();
			objectMapService = (ObjectMapService) ic.lookup(JNDINames.ObjectMapServiceImpl); 
			
		}catch(NamingException ne)
		{
			throw new OperationException(ne); 
		}
		
		return objectMapService; 
	}
	
	public static Codes getCodeList(String codeListType) throws OperationException
	{
		return getCodeListRemote().getCodeList(codeListType); 
	}

	/**
	 * @return
	 */
	public static CodeTypes getCodeTypes() throws OperationException{
			return getCodeListRemote().getCodeTypes();
	}
	
	public static ObjectMaps getorCreateObjectMap(ObjectInfos tableInfos) throws OperationException
	{
		return getCodeListRemote().getOrCreateObjectMap(tableInfos);
	}

	public static ObjectInfo getObjectInfoFromOID(SimpleIdentifier simpleIdentifier) throws OperationException
	{
		return getObjectMapRemote().getObjectInfoFromOID(simpleIdentifier.getOID());
	}
}
