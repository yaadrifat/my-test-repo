package com.velos.services.client;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.Groups;
import com.velos.services.model.NonSystemUser;
import com.velos.services.model.Organizations;
import com.velos.services.model.User;
import com.velos.services.model.User.UserStatus;
import com.velos.services.model.UserIdentifier;
import com.velos.services.model.UserSearch;
import com.velos.services.model.UserSearchResults;
import com.velos.services.user.UserService;
import com.velos.services.util.JNDINames;
/***
 * Client Class/Remote object for User Services 
 */
public class UserClient
{
	
	private static ResponseHolder response = new ResponseHolder();
	public static ResponseHolder changeUserStatus(UserIdentifier uId, UserStatus uStat)throws OperationException 
	{
		
		UserService userService= getUserRemote();
		if(userService == null){
			Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
		}
		return userService.changeUserStatus(uId, uStat);
	}
	public static Groups getAllGroups() throws OperationException 
	{
		UserService userService= getUserRemote();
		if(userService == null){
			Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
		}
		return userService.getAllGroups();
	}
	
	public static Groups getUserGroups(UserIdentifier userId) throws OperationException
	{
		UserService userService = getUserRemote();
		return userService.getUserGroups(userId);
		
	}
	
	public static ResponseHolder createNonSystemUser(NonSystemUser nonsystemuser)throws OperationException 
	{
		
		UserService userService= getUserRemote();
		if(userService == null){
			Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
		}
		return userService.createNonSystemUser(nonsystemuser);
	}

		
	private static UserService getUserRemote() throws OperationException
	{
		UserService userService = null;
		InitialContext ic;
		try
		{
			ic = new InitialContext();
			userService =(UserService) ic.lookup(JNDINames.UserServiceImpl);
		}
		catch(NamingException e){
			throw new OperationException(e);
		}
		return userService;
	}

	public static  Organizations getAllOrganizations() throws OperationException 
	{
		UserService userService= getUserRemote();
		if(userService == null){
			Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
		}
		return userService.getAllOrganizations();
	}
	
	public static ResponseHolder killUserSession(UserIdentifier userId)throws OperationException 
	{
		
		UserService userService= getUserRemote();
		if(userService == null){
			Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
		}
		return userService.killUserSession(userId);
	}
	public static ResponseHolder createUser(User user)throws OperationException 
	{
		
		UserService userService= getUserRemote();
		if(userService == null){
			Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
		}
		return userService.createUser(user);
	}
	/**
	 * @param eSignature
	 * @return
	 */
	public static ResponseHolder checkESignature(String eSignature) throws OperationException{
	    UserService userService = getUserRemote(); 
	    if(userService == null)
	    {
	    	Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
	    }
		return userService.checkESignature(eSignature);
	}
	
	public static UserSearchResults searchUser(UserSearch userSearch) throws OperationException {
	    UserService userService = getUserRemote(); 
	    if(userService == null)
	    {
	    	Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
	    }
		return userService.searchUser(userSearch);
	}

}