package com.velos.services.searchpatient;


import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;


import com.velos.eres.business.common.UserSiteDao;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.userSiteAgent.UserSiteAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.web.userSite.UserSiteJB;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.authorization.AbstractAuthModule;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.PatientSearch;
import com.velos.services.model.PatientSearchResponse;
import com.velos.services.util.ObjectLocator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
@Stateless
@Remote({SearchPatientService.class})
public class SearchPatientServiceImpl extends AbstractService implements SearchPatientService {

	private static Logger logger = Logger.getLogger(SearchPatientServiceImpl.class.getName());
	
	@Resource
	private SessionContext sessionContext;
	@EJB
	private UserAgentRObj userAgent;
	@EJB
	GrpRightsAgentRObj groupRightsAgent;
	@EJB
	private UserSiteAgentRObj userSiteAgent;
	
	@EJB
	private ObjectMapService objectMapService;

	public PatientSearchResponse searchPatient(PatientSearch paramPatientIdentifier) throws OperationException {
		try{
			Integer siteId = 0;
			int requestedParameterCount = 0;	
			ArrayList arUsrSiteid = new ArrayList();	
			int userId = this.callingUser.getUserId();

			if(this.callingUser != null){
				GroupAuthModule groupAuth = new GroupAuthModule(this.callingUser, this.groupRightsAgent);
				
				Integer managePatientPriv =groupAuth.getAppManagePatientsPrivileges();
				boolean hasViewManagePatient = GroupAuthModule.hasViewPermission(managePatientPriv);
				if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + managePatientPriv);
				if (!hasViewManagePatient){
					if (logger.isDebugEnabled()) logger.debug("Thrwoing exception " + hasViewManagePatient);
					addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User not authorized to view patient data"));					
					throw new AuthorizationException("User not authorized to view patient data");
				} 				
			} else {
				if (logger.isDebugEnabled()) logger.debug("Calling user: " + this.callingUser);
			}
						
			if (paramPatientIdentifier.getPatOrganization() != null) {				
				try {
				      siteId = 
				      ObjectLocator.sitePKFromIdentifier(
				      this.callingUser, 
				      paramPatientIdentifier.getPatOrganization(), 
				      this.sessionContext, 
				      this.objectMapService);
				    if (siteId == null || siteId.intValue() == 0 || !SearchPatientDAO.ifOrgExists(siteId)) {//Bug Fix : 16357
				      addIssue(
				        new Issue(
				        IssueTypes.ORGANIZATION_NOT_FOUND, 
				        "SiteID provided, but site not found " + paramPatientIdentifier.getPatOrganization()));
				      throw new OperationException();
				    }
				}
				catch (MultipleObjectsFoundException e) {
				    addIssue(
				    new Issue(
				    IssueTypes.ORGANIZATION_NOT_FOUND, 
				    "Multiple sites found for " + paramPatientIdentifier.getPatOrganization()));
				    throw new OperationException();
				}
					
				UserSiteJB usrSite = new UserSiteJB();
				UserSiteDao usd = new UserSiteDao();
					
				usd = usrSite.getSitesWithViewRight(EJBUtil.stringToNum(this.callingUser.getUserAccountId()),userId);
				boolean hasAccessToSite = false;
				ArrayList arSiteid = new ArrayList();
				ArrayList arSitedesc = new ArrayList();
				arSiteid = usd.getUserSiteSiteIds();	
				arSitedesc = usd.getUserSiteNames();	
				if (logger.isDebugEnabled()) logger.debug("Sites: " + arSitedesc); 
				for(int cnt = 0 ; cnt < arSiteid.size() ; cnt++)
				{
					if (logger.isDebugEnabled()) logger.debug("Loop values:" + arSitedesc.get(cnt).toString() + " SitePK: " + siteId);
					if(siteId.intValue() != (Integer.valueOf(arSiteid.get(cnt).toString())))  
					{
						continue;											
					}
					hasAccessToSite = true;
					break ;	
				}
				if(!hasAccessToSite){
					addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User do not have access to this Organization"));					
					throw new AuthorizationException("User do not have access to this Organization");
				}
								
				
				if (logger.isDebugEnabled()) logger.debug("Site id: "+ siteId);
				if(siteId.intValue() > 0){
					try{
						if (logger.isDebugEnabled()) logger.debug("userSiteAgent: " + userSiteAgent.getRightForUserSite(userId, siteId));
						Integer userSiteRight = Integer.valueOf(userSiteAgent.getRightForUserSite(userId, siteId ));
					    if (logger.isDebugEnabled()) logger.debug("userSiteRight" + userSiteRight);
						if (!AbstractAuthModule.hasViewPermission(userSiteRight))
					    {
					        if (logger.isDebugEnabled()) logger.debug("User not Authorized to view Patient" + paramPatientIdentifier.getPatOrganization());
					        addIssue(new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION, "User Not Authorized to view Patients in Organization: " + paramPatientIdentifier.getPatOrganization() ));
					        throw new AuthorizationException("User Not Authorized to view Patients in Organization");
					    }
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}
				requestedParameterCount++ ;
				arUsrSiteid.add(siteId);
			} else {
				UserSiteJB usrSite = new UserSiteJB();
				UserSiteDao usd = new UserSiteDao();
				
				usd = usrSite.getSitesWithViewRight(EJBUtil.stringToNum(this.callingUser.getUserAccountId()),userId);
				ArrayList arSiteid = new ArrayList();
				ArrayList arSitedesc = new ArrayList();
				arSiteid = usd.getUserSiteSiteIds();	
				arSitedesc = usd.getUserSiteNames();	
				if (logger.isDebugEnabled()) logger.debug("Sites: " + arSitedesc); 
				arUsrSiteid = arSiteid;
				if(arUsrSiteid.size() == 0){
					addIssue(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND,"No User Organization Not Found" ));
			        if (logger.isDebugEnabled()) logger.debug("User Org not found");
			        throw new OperationException();
				}
			}
			
			Map parameters = new HashMap();
			parameters.put("callingUser", this.callingUser);
			parameters.put("ResponseHolder", this.response);
			parameters.put("arUsrSiteid", arUsrSiteid);
			parameters.put("requestedParameterCount", requestedParameterCount);
			parameters.put("objectMapService", objectMapService);

			SearchPatientDAO objPatientSearchDAO = new SearchPatientDAO();
			return objPatientSearchDAO.searchPatient(paramPatientIdentifier, parameters);	
		} catch (OperationException oe){
			if (logger.isDebugEnabled()) logger.debug("EXception : " + oe);
			oe.setIssues(this.response.getIssues());
		    throw oe;
			
		} catch (Throwable t){
			t.printStackTrace();
			addUnknownThrowableIssue(t);
			throw new OperationException(t);
		}
	}
	@AroundInvoke
	  public Object myInterceptor(InvocationContext ctx) throws Exception {
		if (logger.isDebugEnabled()) logger.debug("MyInterceptor: " );
	    this.response = new ResponseHolder();
	    this.callingUser = 
	      getLoggedInUser(
	      this.sessionContext, 
	      this.userAgent);
	    return ctx.proceed();
	  }
}
