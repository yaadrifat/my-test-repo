package com.velos.services.library;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.esch.business.common.EventdefDao;
import com.velos.esch.web.eventdef.EventdefJB;
import com.velos.services.OperationException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.EventSearch;
import com.velos.services.model.LibraryEvent;
import com.velos.services.model.LibraryEvents;
import com.velos.services.util.ServicesUtil;

public class LibraryServiceDAO extends CommonDAO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4728938754447175606L;
	private static Logger logger = Logger.getLogger(LibraryServiceDAO.class);
	
	public static LibraryEvents searchEvent(EventSearch eventSearch, int eventLibraryType, Map<String, Object> parameters, Integer pageNumber,Integer pageSize) throws OperationException{
		
		Connection conn = null;		
		PreparedStatement pstmt = null;
		String searchClause = "";
		String fromClause = "";
		String strCostType = "";
		String strEventFacilityId = "";
		StringBuffer eventDefSql = new StringBuffer();
		StringBuffer countSql = new StringBuffer();
		
		EventdefJB eventdefB = new EventdefJB();
		EventdefDao eventdefDAO = new EventdefDao();
		UserBean usrBean = (UserBean)parameters.get("callingUser");
		int chainId = 0;
		int costType = 0;
		int facilityID = 0;
		int acctId = usrBean.userAccountId;
		costType = EJBUtil.stringToInteger(parameters.get("costType").toString());
		facilityID = EJBUtil.stringToInteger(parameters.get("facilityID").toString());
//		PreparedStatement pstmt = null;
//		Connection conn = null;
		LibraryEvents libraryEvents = new LibraryEvents();
		ObjectMapService objectMapService = ServicesUtil.getObjectMapService();
		
		try{
			
			if (costType == 0) {
				strCostType = "";
			}
			else {
				strCostType = "" + costType;
			}
			
			if (facilityID == 0) {
				strEventFacilityId = "";
			}
			else {
				strEventFacilityId = "" + facilityID;
			}
			
			if(StringUtil.isEmpty(eventSearch.getEventCategory())){
              			String eventCategory = "";
              			eventSearch.setEventCategory(eventCategory);
			}
            
	        if (!StringUtil.isEmpty(eventSearch.getEventNameDescNotes())){
	              searchClause = " AND ( LOWER(DESCRIPTION) LIKE LOWER('%"+eventSearch.getEventNameDescNotes().trim()+"%') " +
	              		         "OR LOWER(NAME) LIKE LOWER('%"+eventSearch.getEventNameDescNotes().trim()+"%') " +
	              		         "OR LOWER(NOTES) LIKE LOWER('%"+eventSearch.getEventNameDescNotes().trim()+"%') )";
	        }
	        
			if (!StringUtil.isEmpty(eventSearch.getCptCode())) {
				searchClause = searchClause + " AND (LOWER(EVENT_CPTCODE) LIKE LOWER('%"+eventSearch.getCptCode().trim()+"%')) ";
			}
			
			if (!StringUtil.isEmpty(strEventFacilityId)) {
				searchClause = searchClause + " AND (LOWER(FACILITY_ID) LIKE LOWER('%"+strEventFacilityId+"%')) ";
			}
	        
			if (!StringUtil.isEmpty(strCostType)) {
				searchClause = searchClause + " AND (LOWER(fk_cost_desc) LIKE LOWER('%"+strCostType+"%')) and fk_event = event_id ";
				fromClause = fromClause + " , sch_eventcost ";

			}
			
			searchClause = searchClause + " AND EVENT_LIBRARY_TYPE = "+eventLibraryType +" ";
			
			if ((eventSearch.getEventCategory().equals("") || eventSearch.getEventCategory().equals(" "))){
				
				eventDefSql.append("SELECT distinct EVENT_ID, case when length(DESCRIPTION) > 100 then substr(DESCRIPTION,1,100) || '...' " +
						    "else DESCRIPTION end as DESCRIPTION, chain_id category_id,EVENT_TYPE, NAME event_name ," +
						    "(select NAME from EVENT_DEF a where a.event_id = b.chain_id and TRIM(UPPER(EVENT_TYPE)) = 'L' ) category, " +
						    "(select DESCRIPTION from EVENT_DEF a where a.event_id = b.chain_id and TRIM(UPPER(EVENT_TYPE)) = 'L' ) category_desc,event_cptcode, " +
						    "case when length(notes) > 100 then substr(notes,1,100) || '...' else notes end as notes, " +
						    "lst_cost(event_id) as eventcost, lst_additionalcode(event_id,'evtaddlcode') as additionalcode," +
						    "(select site_name from er_site s  where s.pk_site = b.FACILITY_ID) as Facilityname FROM EVENT_DEF b ");
				
				eventDefSql.append(fromClause);
				
				eventDefSql.append(" WHERE TRIM(USER_ID)= "+acctId+" AND TRIM(UPPER(EVENT_TYPE)) = 'E'") ;
				
				countSql.append("select count(*) from (SELECT distinct event_id FROM EVENT_DEF ");
				countSql.append(fromClause);
				countSql.append(" WHERE  TRIM(USER_ID)= "+acctId+" AND TRIM(UPPER(EVENT_TYPE)) IN('L','E')");
			}
			else{
				
				eventdefDAO= eventdefB.getHeaderList(acctId,"L",eventLibraryType);
	            for(int i = 0; i < eventdefDAO.getEvent_ids().size(); i++){
	            	if(eventSearch.getEventCategory().equals(eventdefDAO.getNames().get(i).toString()) == true){
	            		chainId =  EJBUtil.stringToInteger(eventdefDAO.getEvent_ids().get(i).toString());
	            		
	            	}
	            }
				
				eventDefSql.append("SELECT distinct EVENT_ID, case when length(DESCRIPTION) > 100 then substr(DESCRIPTION,1,100) || '...' " +
						    "else DESCRIPTION end as DESCRIPTION, chain_id category_id,EVENT_TYPE, NAME event_name ," +
						    "(select NAME from EVENT_DEF a where a.event_id = b.chain_id and TRIM(UPPER(EVENT_TYPE)) = 'L' ) category, " +
						    "(select DESCRIPTION from EVENT_DEF a where a.event_id = b.chain_id and TRIM(UPPER(EVENT_TYPE)) = 'L' ) category_desc," +
						    "event_cptcode, case when length(notes) > 100 then substr(notes,1,100) || '...' else notes end as notes , " +
						    "lst_cost(event_id) as eventcost,  lst_additionalcode(event_id,'evtaddlcode') as additionalcode," +
						    "(select site_name from er_site s  where s.pk_site = b.FACILITY_ID) as Facilityname FROM EVENT_DEF b ");


				eventDefSql.append(fromClause);
				eventDefSql.append(" WHERE CHAIN_ID= "+chainId+" AND TRIM(UPPER(EVENT_TYPE)) = 'E'" );
				
				//ORDER BY CHAIN_ID";
				countSql.append("select count(*) from (SELECT distinct event_id FROM EVENT_DEF ");
				countSql.append(fromClause);
				countSql.append(" WHERE  CHAIN_ID= "+chainId+" and TRIM(UPPER(EVENT_TYPE)) IN('L','E') ");

			}
			
		    eventDefSql.append(searchClause);
		    countSql.append(searchClause); 
		    countSql.append(" )");
	    	int uperLimit=pageSize*pageNumber;
	    	int lowerLimit=(uperLimit-pageSize)+1;
	    	
		    StringBuffer sqlWithPagination=new StringBuffer();
		    
	    	sqlWithPagination.append(" select * from (select x.*, rownum row_num from (" +
	    			eventDefSql.toString() +
	    			") x ) where rownum between "+lowerLimit+" and " + uperLimit);
		    
		    String mainSql = sqlWithPagination.toString();
		    
			try{
				conn = getConnection();
			    List<LibraryEvent> libraryEventList = new ArrayList<LibraryEvent>();  
				pstmt = conn.prepareStatement(mainSql);
			      
			      ResultSet rs = pstmt.executeQuery();
			      
			      if(rs != null){
			    	  
				      while (rs.next())
				      {
				    	LibraryEvent libraryEvent = new LibraryEvent();
				    	EventIdentifier eventIdentifier = new EventIdentifier();
				    	eventIdentifier.setPK(rs.getInt("EVENT_ID"));
				    	ObjectMap objMap = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_LIBRARY_EVENTS, rs.getInt("EVENT_ID"));
				    	eventIdentifier.setOID(objMap.getOID());
				    	libraryEvent.setLibraryEventIdentifier(eventIdentifier);
				    	libraryEvent.setEventCategory(rs.getString("CATEGORY"));
				    	libraryEvent.setEventName(rs.getString("EVENT_NAME"));
				    	libraryEvent.setDescription(rs.getString("DESCRIPTION"));
				    	libraryEvent.setNotes(rs.getString("NOTES"));
				    	libraryEvent.setCost(rs.getString("EVENTCOST"));
				    	libraryEvent.setAdditionalCode(rs.getString("ADDITIONALCODE"));
				    	libraryEvent.setFacility(rs.getString("FACILITYNAME"));
				    	libraryEvent.setCptCode(rs.getString("event_cptcode")); 
				    	libraryEventList.add(libraryEvent);
				      }
			      }
			      
			      libraryEvents.setLibraryEvent(libraryEventList);
			      return libraryEvents;
				
			}catch(Throwable t){
	    		t.printStackTrace();
	    		throw new OperationException();
	    	}
	    	
	    	finally{
	    		try{
	    			if(pstmt!=null){
	    				pstmt.close();
	    			}
	    		}catch(Exception e){
	    			
	    		}
	    		try{
	    			if(conn!=null){
	    				conn.close();
	    			}
	    		}catch(Exception e){
	    			
	    		}
	    	}
			
		}catch(Throwable t){
			
			t.printStackTrace();
			
			throw new OperationException();
		}
		
		finally{
			try{
				if(pstmt!=null){
					pstmt.close();
				}
			}catch(Exception e){
				
			}
			try{
				if(conn!=null){
					conn.close();
				}
			}catch(Exception e){
				
			}
		}
		
	}
	
    public static boolean ifFacilityExists(int facilityPK) throws OperationException{
    	
    	String sql = "select count(*) as Count from er_site where pk_site = ?";
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	boolean flag = false;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, facilityPK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		if(rs.getInt("Count") ==1)
				   flag = true;
				else 
				   flag =  false;
        	}
        	return flag;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
    
    public static boolean ifEventCatExists(String eventCat) throws OperationException{
    	
    	String sql = "select count(*) as Count from event_def where event_category = ?";
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	boolean flag = false;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setString(1, eventCat);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		if(rs.getInt("Count") >=1)
				   flag = true;
				else 
				   flag =  false;
        	}
        	return flag;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }

}
