package com.velos.services.library;

/**
 * @author - Tarandeep Singh Bali
 */

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.EventSearch;
import com.velos.services.model.LibraryEvents;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ObjectLocator;


@Stateless
@Remote(LibraryService.class)
public class LibraryServiceImpl extends AbstractService 
implements LibraryService{
	
	@EJB
	private ObjectMapService objectMapService;
	
	@EJB
	private UserAgentRObj userAgent;
	
	@Resource 
	private SessionContext sessionContext;
	
	@EJB
	GrpRightsAgentRObj groupRightsAgent;
	
	private static Logger logger = Logger.getLogger(LibraryServiceImpl.class.getName());

	@Override
	public LibraryEvents searchEvent(EventSearch eventSearch, Integer pageNumber, Integer pageSize) throws OperationException {
		

		LibraryEvents libraryEvents = new LibraryEvents();
		int eventLibraryType = 0;
		Integer costType = 0;
		Integer facilityID = 0;
	
		
		try{
			
			GroupAuthModule groupAuthModule = new GroupAuthModule(callingUser, groupRightsAgent);
			Integer manageLibraryPriv = groupAuthModule.getAppEventLibraryPrivileges();			
			if(!GroupAuthModule.hasViewPermission(manageLibraryPriv))
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to View Library Events"));
				throw new AuthorizationException("User Not Authorized to View Library Events");
			}
			
			Map parameters = new HashMap();
			parameters.put("callingUser", this.callingUser);
			
			if(eventSearch.getLibraryType() == null || StringUtil.isEmpty(eventSearch.getLibraryType().getCode())){
	            if (logger.isDebugEnabled()) logger.debug("Event Library is required");
	            addIssue(
	                    new Issue(IssueTypes.DATA_VALIDATION, 
	                            "Please enter a valid Event Library Type"));
	            throw new AuthorizationException("Please enter a valid Event Library Type");
			}
			try{
				eventLibraryType = dereferenceSchCode(eventSearch.getLibraryType(), CodeCache.CODE_EVENT_LIBRARY_TYPE, callingUser);
			}catch (Exception e) {
				// TODO Auto-generated catch block
				if (logger.isDebugEnabled()) logger.debug("Valid Event Library is required");
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Please enter a valid Event Library Type"));
				throw new OperationException("Please enter a valid Event Library Type");
			}
			
			if((eventSearch.getCostType() != null) && (!StringUtil.isEmpty(eventSearch.getCostType().getCode()))){
				try{
				costType = dereferenceSchCode(eventSearch.getCostType(), CodeCache.CODE_TYPE_COST_DESC, callingUser);
				}catch (Exception e) {
					// TODO Auto-generated catch block
					if (logger.isDebugEnabled()) logger.debug("COST TYPE not found");
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Please enter a valid Cost Type"));
					throw new OperationException("Please enter a valid Cost Type");
				}
			}
			
			if(eventSearch.getFacility() != null){// && eventSearch.getFacility().getOID() != null && eventSearch.getFacility().getOID().length() > 0){
				
				try{	
					facilityID = ObjectLocator.sitePKFromIdentifier(callingUser, eventSearch.getFacility(), sessionContext, objectMapService);
				}catch (Exception e) {
					// TODO Auto-generated catch block
					if (logger.isDebugEnabled()) logger.debug("Event Status Code is required");
					addIssue(new Issue(IssueTypes.INVALID_SITE_OF_SERVICE, "Invalid site of service"));
					throw new OperationException("Invalid site of service");
				}
					//Bug Fix : 15221
					if(facilityID == null){
						if (logger.isDebugEnabled()) logger.debug("Facility  not found");
						addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Please enter a valid Facility"));
						throw new OperationException("Please enter a valid Facility");
					}
					if(facilityID == 0){
						if (logger.isDebugEnabled()) logger.debug("Facility  not found");
						addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Please enter a valid Facility"));
						throw new OperationException("Please enter a valid Facility");
					}
					
					if(!LibraryServiceDAO.ifFacilityExists(facilityID)){
						addIssue(new Issue(IssueTypes.DATA_VALIDATION,				
								"Please enter a valid Facility")); 
						throw new OperationException("Please enter a valid Facility"); 
					}
			}
			
			if(eventSearch.getEventCategory() != null){
				if(!LibraryServiceDAO.ifEventCatExists(eventSearch.getEventCategory())){
					addIssue(new Issue(IssueTypes.DATA_VALIDATION,				
							"Please enter a valid Event category")); 
					throw new OperationException("Please enter a valid Event Category"); 
				}
			}
			
			
			parameters.put("costType", costType);
			parameters.put("facilityID", facilityID);
			//Bug Fix : 15246 && 15247
			if(!EJBUtil.isEmpty(EJBUtil.integerToString(pageSize))){
				if(pageSize<0)
		    	{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION,				
							"Please enter a valid Page Size")); 
					throw new OperationException("Please enter a valid Page Size"); 
		    	}
				else if(pageSize == 0){
					pageSize = 100;
				}
			}else if(EJBUtil.isEmpty(EJBUtil.integerToString(pageSize))){
				pageSize = 100;
			}
 
			if(!EJBUtil.isEmpty(EJBUtil.integerToString(pageNumber))){
				if(pageNumber<0)
		    	{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION,				
							"Please enter a valid Page Number")); 
					throw new OperationException("Please enter a valid Page Number"); 
		    	}
				else if(pageNumber == 0){
					pageNumber = 1;
				}
			}else if(EJBUtil.isEmpty(EJBUtil.integerToString(pageNumber))){
				pageNumber = 1;
			}

			
			libraryEvents = LibraryServiceDAO.searchEvent(eventSearch, eventLibraryType, parameters, pageNumber, pageSize);

			
			return libraryEvents;
			
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			e.printStackTrace();
			if (logger.isDebugEnabled()) logger.debug("LibraryServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			t.printStackTrace();
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("LibraryServiceImpl create", t);
			throw new OperationException(t);
		}
	

	}
	
	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		response = new ResponseHolder();
		callingUser = 
			getLoggedInUser(
					sessionContext,
					userAgent);
		return ctx.proceed();

	}


}
