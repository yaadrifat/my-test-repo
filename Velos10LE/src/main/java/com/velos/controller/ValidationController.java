/*
$Id: ValidationController.java,v 1.4 2013/04/01 23:02:18 smhalagi Exp $ */
package com.velos.controller;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.MC;
import com.velos.eres.service.util.SessionMaint;
import com.velos.eres.web.user.UserJB;
import com.velos.validator.DefaultValidator;
import com.velos.validator.FormValidator;
import com.velos.validator.PatStudyValidator;
import com.velos.validator.PatValidator;
import com.velos.validator.StudyValidator;

import java.util.*;

public class ValidationController extends HttpServlet {
    
    private ServletContext context;
    private HashMap accounts = new HashMap();
 
    public void init(ServletConfig config) throws ServletException {
        this.context = config.getServletContext();
        
    }
    
    public  void doGet(HttpServletRequest request, HttpServletResponse  response)
        throws IOException, ServletException {
        
        boolean validateFlag=false; 
        String validateFld="";
        String validateMessage="";
        
        SessionMaint sessionmaint = new SessionMaint();
        HttpSession tSession = request.getSession(false);
        if (!sessionmaint.isValidSession(tSession)) {
        	this.CreateXml(false, validateMessage, response);
        	return;
        }
        
        String targetId = request.getParameter("id");
        targetId=(targetId==null)?"":targetId;
        String dataSet = request.getParameter("dataset");
        dataSet=(dataSet==null)?"":dataSet;
        String module=request.getParameter("module");
        module=(module==null)?"":module;
        
        module=module.toLowerCase();
        
        String[]moduleArray=module.split(":");
        
        if (moduleArray[0].indexOf('~')>=0)
        	validateFld=moduleArray[0].substring(moduleArray[0].indexOf('~')+1);
        else 
        	validateFld=moduleArray[1];
        
        validateFld=validateFld.toLowerCase();
        
        if (module.startsWith("misc"))
        {
        	if (validateFld.equals("userlogname")){
	        	   String usrLogin = (String) tSession.getAttribute("loginname");
	        	   if ((targetId != null) && usrLogin.equals(targetId)) {
	        		   validateMessage=MC.M_Valid_UserName;
	        		   validateFlag = true;
		           } else {
		        	   validateMessage=MC.M_Invalid_UserName;
		        	   validateFlag = false;
		           }
        	} else {
            	dataSet = (String)tSession.getAttribute("userId");
            	DefaultValidator dValidate=new DefaultValidator(validateFld,dataSet,targetId);
            	validateFlag=dValidate.validate();
            	validateMessage=dValidate.getValidationMessage();
            }
        }
        
        if (module.startsWith("patient"))
        {
           PatValidator pValidate=new PatValidator(validateFld,dataSet,targetId);
           validateFlag=pValidate.validate();
           validateMessage=pValidate.getValidationMessage();
        }
        
        if (module.startsWith("study"))
        {
           StudyValidator sValidate=new StudyValidator(validateFld,dataSet,targetId);
           validateFlag=sValidate.validate();
           validateMessage=sValidate.getValidationMessage();
        }
        if (module.startsWith("patstudy"))
        {
           PatStudyValidator psValidate=new PatStudyValidator(validateFld,dataSet,targetId);
           validateFlag=psValidate.validate();
           validateMessage=psValidate.getValidationMessage();
        }
        if (module.startsWith("form"))
        {
           FormValidator formValidate=new FormValidator(validateFld,dataSet,targetId);
           validateFlag=formValidate.validate();
           validateMessage=formValidate.getValidationMessage();
        }
        
             
     this.CreateXml(validateFlag,validateMessage,response);   
    }

    public  void doPost(HttpServletRequest request, HttpServletResponse  response)
        throws IOException, ServletException {

        this.doGet( request,  response);
    }
    private void CreateXml(boolean validateFlag,String validateMessage,HttpServletResponse response)
    {
    
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        try {
        response.getWriter().write("<valid>"+validateFlag);
        if (validateMessage.length()>0) response.getWriter().write("<message>"+validateMessage+"</message>");
        response.getWriter().write("</valid>");
        }catch(IOException ioe) {
         ioe.printStackTrace();   
        }
        
        
        
    }
}


