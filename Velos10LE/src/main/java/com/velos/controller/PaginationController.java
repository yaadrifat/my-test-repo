/*
$Id: PaginationController.java,v 1.32 2013/09/24 07:18:29 tkumar Exp $ */
package com.velos.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.velos.browser.EIRB;
import com.velos.browser.Patient;
import com.velos.browser.Study;
import com.velos.eres.business.common.BrowserDao;
import com.velos.eres.business.common.SettingsDao;
import com.velos.eres.service.util.BrowserRows;
import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.MC;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

public class PaginationController extends HttpServlet {

	private ServletContext context;
	private ArrayList colExp; 
	public void init(ServletConfig config) throws ServletException {
		this.context = config.getServletContext();
		

	}
	
    private void showForbiddenMessage(HttpServletResponse resp) {
        try {
            ServletOutputStream out = resp.getOutputStream();
            out.println("<html><head><title>"+LC.L_Forbidden+"</title></head>");
            out.println("<body>");
            out.println("<h1>"+LC.L_Forbidden+"</h1>");
            out.println("<p>"+MC.M_NotPerm_ToAcesServer+"</p>");
            out.println("</body>");
            out.println("</html>");
        } catch (IOException e) {
        }
    }
    
    public static final String USER_ID = "userId";
    public static final String PP_IGNORE_ALL_RIGHTS = "pp_ignoreAllRights";
    public static final String CHARSET = "UTF-8";

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		
        response.setCharacterEncoding(CHARSET); //Ak:Added to support Unicode for YUI implementation.
        HttpSession session = request.getSession(false);
        
        if (session == null ||
        		(StringUtil.stringToNum((String)session.getAttribute(USER_ID)) < 1 &&
        		session.getAttribute(PP_IGNORE_ALL_RIGHTS) == null)) {
            showForbiddenMessage(response);
            Rlog.fatal("paginate", "Forbidden_error: invalid_session");
            return;
        }
        
		String mainSQL="",countSQL="";
		boolean mask=true;

		String module = request.getParameter("module");
		module = (module == null) ? "" : module;
		
		String dmode = request.getParameter("dmode");
		dmode = (dmode == null) ? "" : dmode;
		
		
 		
		int accountId=StringUtil.stringToNum(StringUtil.trueValue((String)session.getAttribute("accountId")));
		int userId=StringUtil.stringToNum(StringUtil.trueValue((String)session.getAttribute("userId")));
		int grpId=StringUtil.stringToNum(StringUtil.trueValue((String)session.getAttribute("defUserGroup")));
		
		
		module = module.toLowerCase().trim();
	 
		
		int rowsPerPage=0;
		
		// Get Order by information and pre-process if needed
		String orderBy = StringUtil.trueValue(request
				.getParameter("orderBy"));
		String orderType = StringUtil.trueValue(request
				.getParameter("orderType"));
		
		if (orderBy.equals("MASK_PERSON_DOB")) orderBy="NO_OF_DAYS_DIFF";
		
	 
		
		if (module.startsWith("allpatient")) {
			HashMap params = new HashMap();
			params.put("accountId", StringUtil.trueValue((String)session.getAttribute("accountId")));
			params.put("regBy", StringUtil.trueValue(request
					.getParameter("regBy")));
			params.put("patName", StringUtil.trueValue(request
					.getParameter("patName")));
			params.put("pstat", StringUtil.trueValue(request
					.getParameter("pstat")));
			params.put("siteId", StringUtil.trueValue(request
					.getParameter("siteId")));
			params.put("speciality", StringUtil.trueValue(request
					.getParameter("speciality")));
			params.put("patCode", StringUtil.trueValue(request
					.getParameter("patCode")));
			params.put("gender", StringUtil.trueValue(request
					.getParameter("gender")));
			params.put("userId", StringUtil.trueValue((String)session.getAttribute("userId")));
			params.put("age", StringUtil.trueValue(request
					.getParameter("patage")));
			params.put("studyId", StringUtil.trueValue(request
					.getParameter("studyId")));
			String regBy=request.getParameter("regby");
			regBy=StringUtil.trueValue(regBy);
			params.put("regBy", regBy);
			params.put("grpId", StringUtil.trueValue((String)session.getAttribute("defUserGroup")));
			
			 
			Patient pat = new Patient();
			pat.setParamMap(params);
			pat.getAllPatientsSQL();
			mainSQL=pat.getMainSQL();
			countSQL=pat.getCountSQL();
			mask=true;
			
		}
		if (module.startsWith("advsearch")) {
			Study study=new Study();
			study.getAdvSearchSQL(request);
			mainSQL=study.getMainSQL();
			countSQL=study.getCountSQL();
		}
		if (module.startsWith("studypatient")) {
			Patient pat=new Patient();
			pat.getStudyPatientsSQL(request);
			mainSQL=pat.getMainSQL();
			countSQL=pat.getCountSQL();
			mask=true;
		}
		if (module.startsWith("allschedules")) {
			Patient pat=new Patient();
			pat.getAllSchedulesSQL(request);
			mainSQL=pat.getMainSQL();
			countSQL=pat.getCountSQL();
			mask=true;
		}
		if (module.startsWith("preparea")){			
			HashMap params = new HashMap();
			params.put("accountId",
					StringUtil.trueValue((String)session.getAttribute("accountId")));
			params.put("userId", 
					StringUtil.trueValue((String)session.getAttribute("userId")));
			params.put("patStdCode", StringUtil.trueValue(request
					.getParameter("patStdCode")));			
			params.put("siteId", StringUtil.trueValue(request
					.getParameter("dPatSite"))); //AK:Fixed BUG#5985(1stApril11)
			params.put("studyId", StringUtil.trueValue(request
					.getParameter("studyId")));
			params.put("patCode", StringUtil.trueValue(request
					.getParameter("patCode")));			
			params.put("dateChoice", StringUtil.trueValue(request
					.getParameter("dateChoice")));
			params.put("year", StringUtil.trueValue(request
					.getParameter("year")));
			params.put("year1", StringUtil.trueValue(request
					.getParameter("year1")));
			params.put("month", StringUtil.trueValue(request
					.getParameter("month")));
			params.put("fromDate", StringUtil.trueValue(request
					.getParameter("fromDate")));
			params.put("toDate", StringUtil.trueValue(request
					.getParameter("toDate")));
			params.put("grpId", 
					StringUtil.trueValue((String)session.getAttribute("defUserGroup")));
			Patient pat = new Patient();
			pat.setParamMap(params);
			pat.getAllSchedulesWithKitSql(request);
			mainSQL=pat.getMainSQL();
			countSQL=pat.getCountSQL();
			//FIXED BUG 6026
			mask=true;
		}
		if (module.startsWith("ctrpdraftbrowser")) {
			Study study=new Study();
			study.getStudyCtrpDrafts(request);
			mainSQL=study.getMainSQL();
			countSQL=study.getCountSQL();
		}
		if (module.startsWith("ctrpaccrualbrowser")) {
			Study study=new Study();
			study.getCtrpSubjectAccrual(request);
			mainSQL=study.getMainSQL();
			countSQL=study.getCountSQL();
		}
        String tabsubtype = StringUtil.trueValue(request.getParameter("tabsubtype"));
		if (module.startsWith("irb")) {
			
			String user = StringUtil.trueValue((String)session.getAttribute("userId"));
			
			String group = StringUtil.trueValue((String)session.getAttribute("defUserGroup"));
			
			String submissionType = StringUtil.trueValue(request.getParameter("submissionType"));
			String revType = StringUtil.trueValue(request.getParameter("revType"));
			String searchCriteria = StringUtil.htmlEncodeXss(request.getParameter("searchCriteria"));
            String meetingDate = StringUtil.htmlEncodeXss(request.getParameter("meetingDate"));
            String meetingId = StringUtil.htmlEncodeXss(request.getParameter("meetingId"));
            
            String studyPI = StringUtil.htmlEncodeXss(request.getParameter("studyPI"));
            

            if (meetingDate == null || meetingDate.length() == 0 || "null".equals(meetingDate)) {
                if (meetingId != null && meetingId.length() > 0 && !"null".equals(meetingDate)) {
                    meetingDate = meetingId;
                } else {
                    meetingDate = "0";
                }
            }
			
			HashMap params = new HashMap();
			
		     
			params.put("studyPI", studyPI);
			params.put("accountId", StringUtil.trueValue((String)session.getAttribute("accountId")));
			
			params.put("tabsubtype", tabsubtype);
			params.put("user", user);
			
			params.put("group", group);
			params.put("revType", revType);
			params.put("submissionType", submissionType);
			params.put("module", module);
            params.put("searchCriteria", searchCriteria);
			
			if (module.equals("irbongoing")) {
			    params.put("studyStatusSubtype1", "irb_approved"); // !! Keep "irb_approved" with "studyStatusSubtype1" !!
                params.put("studyStatusSubtype2", "active");
			}
			if (module.equals("irbmeeting") || module.equals("irbmeetingtopic")) {
			    params.put("meetingDate", meetingDate);
			}
			
			EIRB eirb=new EIRB();
			
			eirb.setParamMap(params);
			
			eirb.getApplicationSQL();
			
			mainSQL=eirb.getMainSQL();
			countSQL=eirb.getCountSQL();
			mask=false; 
		}

			
			int curPage = -1;
			if (!"undefined".equals(request.getParameter("curPage"))) {
                curPage = StringUtil.stringToNum(StringUtil.trueValue(request.getParameter("curPage")));
			}
			if (curPage <= 0)
				curPage = 1;
			
			SettingsDao sdao = new SettingsDao();  
			sdao.getSettingsValue(module.toUpperCase()+"_ROWS",accountId,grpId,userId);
			
			
			String rPP = StringUtil.trueValue(request
					.getParameter("rowsPerPage"));
			rowsPerPage = StringUtil.stringToNum(rPP);
			if (rowsPerPage == 0) {
			//VA:KLUDGE : this should be fixed We are doing extra hit to DB which can be removed if we retrieve meta string
			//with same hit. However, meta string is retrieved only first time
				rowsPerPage=StringUtil.stringToNum(StringUtil.trueValue(sdao.getOrderedSetting("U,G,A")));
				if (rowsPerPage==0)
				rowsPerPage = Configuration.ROWSPERBROWSERPAGE ;
				 
					
			}
			

			// String rPa=
			// StringUtil.trueValue(request.getParameter("rowsPerPage"));
			// rPa=(rPa.length()==0)?"10":rPa;
			// int rowsPerPage=EJBUtil.stringToNum(rPa);

			
			BrowserRows br = new BrowserRows();

			
			br.getBrowserRows(curPage, rowsPerPage, mainSQL, 50, countSQL, orderBy, orderType,mask);
			
			String meta=request.getParameter("meta");
			meta=(meta==null)?"":meta;
			String metaStr="";
			if (meta.equals("true") || (dmode.length()>0)  )
			{
				metaStr=getMetaData(module,tabsubtype,accountId,grpId,userId);
			}
			
			String jsonData="";
			String exportData="";
			if (dmode.equals("xls"))
				 exportData=br.toExportString("xls",colExp);
			else if (dmode.equals("doc"))
				exportData=br.toExportString("doc",colExp);
			else
				jsonData=br.toJSONString();
			
			
			
			if (metaStr.length()>0 && (jsonData.length()>0))
			{
				jsonData=jsonData.substring(0,jsonData.lastIndexOf("}")  ) + ","+metaStr+"}";
			}
			
			if (jsonData.length()>0)
			response.getWriter().write(jsonData);
			else
				response.getWriter().write(exportData);

	}
	
	 private String getMetaData(String module,String subtype,int accId,int grpId,int userId)
		{
	    	//return  "\"colArray\":\"PERSON_CODE,PLNAME,PERSON_DOB,PERSON_GENDER,PERSON_STATUS,PK_PERSON\"";
	    	/*return  "\"colArray\":[\"PERSON_CODE\",\"PFNAME\",\"PLNAME\",\"PERSON_DOB\",\"PERSON_GENDER\",\"PERSON_STATUS\",\"PK_PERSON\"]," 
	    	  + "\"meta\":[{\"key\":\"PERSON_CODE\", \"label\":\"Person Code\",\"formatter\":\"myCustomFormatter\", \"sortable\":true, \"resizeable\":true},"+
	                               " {\"key\":\"PFNAME\", \"label\":\"Person First Name\", \"sortable\":true, \"resizeable\":true, \"hideable\":true, \"className\":\"hide\"},"+
	                               " {\"key\":\"PLNAME\", \"label\":\"Person Last Name\", \"sortable\":true, \"resizeable\":true, \"hideable\":true},"+
	                                 "{\"key\":\"PERSON_DOB\", \"text\":\"Date of Birth\", \"label\":\"DOB\", \"sortable\":true, \"resizeable\":true,\"hideable\":true},"+
	                                 " {\"key\":\"PERSON_GENDER\", \"text\":\"Gender\" ,\"label\":\"Gender\", \"sortable\":true, \"resizeable\":true,\"hideable\":true},"+
	                               " {\"key\":\"PERSON_STATUS\",\"text\":\"Patient Status\", \"label\":\"Patient Status\", \"sortable\":true, \"resizeable\":true, \"hideable\":true}]";
	                               */
	    	 String metaStr="";
	    	 ArrayList colExpLabels,colExpIds;
	    	 BrowserDao bd = new BrowserDao();
	    	 String skipColNames[] = null;
	    	 if ("irbpend".equals(module) && "irb_saved_tab".equals(subtype)) {
	    	     skipColNames = new String[2];
	    	     skipColNames[0] = "SEND_LINK";
	    	     skipColNames[1] = "SUBMISSION_NOTES";
	    	 } else if ("irbpend".equals(module) && "irb_act_tab".equals(subtype)) {
                 skipColNames = new String[1];
                 skipColNames[0] = "LETTER_LINK";
             } else if ("irbpend".equals(module) && "irb_items_tab".equals(subtype)) {
                 skipColNames = new String[3];
                 skipColNames[0] = "SEND_LINK";
                 skipColNames[1] = "SUBMISSION_NOTES";
                 skipColNames[2] = "LETTER_LINK";
	    	 } else if ("irbpend".equals(module) && "irb_appr_tab".equals(subtype)) {
                 skipColNames = new String[3];
                 skipColNames[0] = "SEND_LINK";
                 skipColNames[1] = "SUBMISSION_NOTES";
             } else if ("irbsub".equals(module) && "irb_pend_tab".equals(subtype)) {
	    	     skipColNames = new String[1];
	    	     skipColNames[0] = "FORMS_LINK";
	    	 } else if ("irbsub".equals(module) && "irb_pend_rev".equals(subtype)) {
	    	     skipColNames = new String[1];
	    	     skipColNames[0] = "FORMS_LINK";
	    	 }
	    	 
	       	 metaStr=bd.getBrdetailString(module,accId,grpId,userId,skipColNames);
	       	 colExpLabels=bd.getColExpLabels();
	       	 colExp=new ArrayList();
	       	 colExp.add(colExpLabels);
	       	 colExpIds=bd.getColnames();
	       	 colExp.add(colExpIds);

	       	 return metaStr;

		}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		this.doGet(request, response);
	}

}
