/**
 * Extends HttpServlet class, used to upload a file to the database. The file can be uploaded only to a Blob type column.
 *
 * @author Sajal
 *
 * @version 1.0
 *
 */
package com.aithent.file.uploadDownload;
// import statements
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Hashtable;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.MC;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * Extends HttpServlet class, used to upload a file to the database. The file
 * can be uploaded only to a Blob type column.
 * 
 */
public class FileUploadServlet extends HttpServlet {
    protected int maxSize = 1024 * 1024 * 8;
    public String[] pkBase;
    public boolean deleteFlag = false;
    public Hashtable table;
    public String path = "", file_name;
    public long uploadFileSize = 0;
    public String ser_file_name;
    public static final String CHARSET = "UTF-8";
    public static final String USER_ID = "userId";
    public static final String PP_IGNORE_ALL_RIGHTS = "pp_ignoreAllRights";
    ServletOutputStream out;
    FileUploadServlet() {
        pkBase = new String[1];
    }
    int page_id, user_id, file_upld_id, field_id = 0;
    /**
     * Overrides the init method of javax.servlet.GenericSevlet. Called by the
     * servlet container to indicate to a servlet that the servlet is being
     * placed into service.
     * 
     * @param sc
     *            a ServletConfig object that contains configuration information
     *            for this servlet
     * 
     */
    public void init(ServletConfig sc) throws ServletException {
        super.init(sc);
        String max = sc.getInitParameter("maxSize");
        try {
            maxSize = Integer.parseInt(max);
        } catch (NumberFormatException nfe) {
            // Do nothing, leave at default value
        }
    }

    /*
     * We will not support the GET method for upload servlets
     */
    protected void doGet(HttpServletRequest req, HttpServletResponse res) 
    		throws ServletException, IOException {
    	showForbiddenMessage(res.getOutputStream());
    }

    /**
     * Overrides the doPost method of HttpServlet. Extracts the parameters sent
     * by the servlet and uploads the specified file with the specified name
     * from the temporary path specified in the XML file to the table specified.
     * 
     * @param httpservletrequest
     *            an HttpServletRequest object that contains the request the
     *            client has made of the servlet
     * 
     * @param httpservletresponse
     *            an HttpServletResponse object that contains the response the
     *            servlet sends to the client
     * 
     */
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        System.out.println("line 1");
        req.setCharacterEncoding(CHARSET);
        HttpSession session = req.getSession(false);
        if (session == null ||
        		(StringUtil.stringToNum((String)session.getAttribute(USER_ID)) < 1 &&
        		session.getAttribute(PP_IGNORE_ALL_RIGHTS) == null)) {
        	showForbiddenMessage(res.getOutputStream());
        	Rlog.fatal("fileUpload", "Forbidden_error: invalid_session");
        	return;
        }
        out = res.getOutputStream();
        String file_type, file_path, file_name;
        file_name = null;
        file_type = null;
        // get the file size
        // create instance of client side java bean
        if (table == null) {
            // AppendixJB ajb=new AppendixJB();
            if (!req.getContentType().toLowerCase().startsWith(
                    "multipart/form-data")) {
                System.out.println("FileUploadServlet.doPost line 1");
                // Since this servlet only handles File Upload, bail out
                // Note: this isn't strictly legal - I should send
                // a custom error message
                return;
            }
            if (req.getContentLength() > maxSize) {
                System.out.println("FileUploadServlet.doPost line 2");
                return;
            }
            int ind = req.getContentType().indexOf("boundary=");
            System.out.println("FileUploadServlet.doPost line 3");
            if (ind == -1) {
                System.out.println("FileUploadServlet.doPost line 4");
                return;
            }
            String boundary = req.getContentType().substring(ind + 9);
            if (boundary == null) {
                System.out.println("FileUploadServlet.doPost line 5");
                return;
            }
            try {
                System.out.println("FileUploadServlet.doPost line 6");
                table = parseMulti(boundary, req.getInputStream(), out);
            } catch (Throwable t) {
                System.out.println("FileUploadServlet.doPost Fatal line 7");
                t.printStackTrace();
                return;
            }
        }
        System.out.println("line 2");
        // Get the data from hashtable
        // The name of DB entry in fileUploadDownload.xml
        String[] db = new String[1];
        // The name of module entry in fileUploadDownload.xml to be used for
        // file upload
        String[] module = new String[1];
        // The name of the table where the file needs to be uploaded
        String[] tableName = new String[1];
        // Column Name of the BLOB field where the file would be saved
        String[] columnName = new String[1];
        // The primary key column name of the table in which the file needs to
        // be uploaded
        String[] pkColumnName = new String[1];
        // The primary key value of the row for which the file needs to be
        // uploaded
        String[] pkValue = new String[1];
        // The URL of the page that should be opened after this servlet has been
        // executed.
        String[] nextPage = new String[1];
        // The max size of the file that can be uploaded
        String[] maxFileSize = new String[1];
        String[] accMaxStorage = new String[1];
        maxFileSize = (String[]) table.get("maxFileSize");
        accMaxStorage = (String[]) table.get("accMaxStorage");
        System.out.println("@@maxFileSize:" + maxFileSize);
        System.out.println("@@accMaxStorage:" + accMaxStorage);
        if (maxFileSize[0] == null)
            maxFileSize[0] = "0";
        if (accMaxStorage[0] == null)
            accMaxStorage[0] = "0";
        db = (String[]) table.get("db");
        module = (String[]) table.get("module");
        // read appendix param
        Configuration.readSettings(db[0]);
        Configuration.readUploadDownloadParam(
                Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml",
                module[0]);
        tableName = (String[]) table.get("tableName");
        System.out.println("tableName" + tableName[0]);
        columnName = (String[]) table.get("columnName");
        System.out.println("columnName" + columnName[0]);
        pkColumnName = (String[]) table.get("pkColumnName");
        System.out.println("line 3");
        // pkValue = (String[])table.get("pkValue");
        pkValue = (String[]) pkBase;
        nextPage = (String[]) table.get("nextPage");
        for (Enumeration fields = table.keys(); fields.hasMoreElements();) {
            String name = (String) fields.nextElement();
            Object obj = table.get(name);
            if (obj instanceof Hashtable) {
                // its a file!
                Hashtable filehash = (Hashtable) obj;
                obj = filehash.get("content");
                byte[] bytes = (byte[]) obj;
                file_name = (String) filehash.get("filename");
                // set the url value
                // else set the file name and save its contents to server
                if (file_name.length() > 0) {
                    if (file_name.lastIndexOf("\\") != -1)
                        file_name = file_name.substring(file_name
                                .lastIndexOf("\\") + 1);
                    this.file_name = file_name;
                    // Check if the file type i.e., '.xxx'exists or not
                    boolean dot_flag = false;
                    if (file_name.indexOf(".") > 0) {
                        dot_flag = true;
                        // Get the type
                        file_type = file_name.substring(file_name
                                .lastIndexOf(".") + 1);
                        // Get the name without type
                        file_name = file_name.substring(0, file_name
                                .lastIndexOf("."));
                    }
                    // Remove spaces
                    file_name = file_name.replace(' ', '_');
                    // append date and time with name
                    Calendar now = Calendar.getInstance();
                    file_name = file_name + now.get(now.DAY_OF_MONTH)
                            + now.get(Calendar.MONTH)
                            + (now.get(now.YEAR) - 1900)
                            + now.get(now.HOUR_OF_DAY) + now.get(now.MINUTE)
                            + now.get(now.SECOND);
                    // Attach the type
                    if (dot_flag == true)
                        file_name = file_name + "." + file_type;
                    ser_file_name = EJBUtil.getActualPath(
                            Configuration.UPLOADFOLDER, file_name);
                    System.out.println(ser_file_name);
                    FileOutputStream file = new FileOutputStream(ser_file_name);
                    file.write(bytes, 0, bytes.length);
                    file.close();
                    // set file name
                }
            }
        } // end of for
        File server_file = new File(ser_file_name);
        uploadFileSize = server_file.length();
        System.out.println("@@uploadFileSize:" + uploadFileSize);
        System.out.println("@@Long.parseLong(maxFileSize[0]):"
                + Long.parseLong(maxFileSize[0]));
        System.out.println("line 4");
        path = "URL=" + nextPage[0];
        table.put("uploadFileSize", Long.toString(uploadFileSize));
        System.out.println("@@@Long.toString(uploadFileSize)"
                + Long.toString(uploadFileSize));
        deleteFlag = false;
        if (uploadFileSize == 0) {
            System.out.println("@@inside uploadFileSize");
            deleteFlag = true;
            path = path + "&incorrectFile=1&outOfSpace=0";
        } else if ((uploadFileSize > Long.parseLong(maxFileSize[0]) && Long
                .parseLong(accMaxStorage[0]) != -1)) {
            System.out.println("@@inside else if");
            deleteFlag = true;
            path = path + "&incorrectFile=0&outOfSpace=1";
        } else {
            System.out.println("@@inside else");
            int pkValueInt = EJBUtil.stringToNum(pkValue[0]);
            // save file in db
            if (pkValueInt >= 0) {
                System.out.println("@@inside pkValueInt>=0");
                SaveFile.saveFile(db[0], tableName[0], columnName[0],
                        pkColumnName[0], ser_file_name, pkValueInt,
                        uploadFileSize);
            }
            // out.flush();
            System.out.println("@@before incorrectFile");
            path = path + "&incorrectFile=0&outOfSpace=0";
        }
        System.out.println("@@path" + path);
        System.out.println("line 5");
        // out.flush();
        // out.close();
        // out.println(path);
    }// end of do post
    /**
     * 
     * 
     * Obtain information on this servlet.
     * 
     * 
     * @return String describing this servlet.
     * 
     * 
     */
    public String getServletInfo() {
        return "File upload servlet -- used to receive files";
    }
    
    protected Hashtable saveFilesInTemp(Hashtable inHash, String userId) {
        Hashtable outHash = new Hashtable();
        int count=0;
        
        for (Enumeration elems = inHash.keys(); elems.hasMoreElements();) {
            Object obj = inHash.get((String) elems.nextElement());
            if (!(obj instanceof Hashtable)) { continue; }
            // its a file!
            count++;
            byte[] bytes = (byte[]) ((Hashtable)obj).get("content");
            String _file_name = (String) ((Hashtable)obj).get("filename");
            if (bytes == null || bytes.length == 0) { continue; }
            if (_file_name == null || _file_name.length() == 0) { continue; }
            
            String _file_type = null;
            // set the url value
            // else set the file name and save its contents to server
            if (_file_name.lastIndexOf("\\") != -1)
                _file_name = _file_name.substring(_file_name
                        .lastIndexOf("\\") + 1);
            // Check if the file type i.e., '.xxx'exists or not
            boolean dot_flag = false;
            if (_file_name.indexOf(".") > 0) {
                dot_flag = true;
                // Get the type
                _file_type = _file_name.substring(_file_name
                        .lastIndexOf(".") + 1);
                // Get the name without type
                _file_name = _file_name.substring(0, _file_name
                        .lastIndexOf("."));
            }
            // Remove spaces
            _file_name = _file_name.replace(' ', '_');
            String _file_uri = _file_name + "";
            // append date and time with name
            Calendar now = Calendar.getInstance();
            _file_name = _file_name + "_" + count + now.get(now.DAY_OF_MONTH)
                    + now.get(Calendar.MONTH)
                    + (now.get(now.YEAR) - 1900)
                    + now.get(now.HOUR_OF_DAY) + now.get(now.MINUTE)
                    + now.get(now.SECOND) + now.get(now.MILLISECOND)
                    + userId;
            // Attach the type
            if (dot_flag == true) {
                _file_name = _file_name + "." + _file_type;
                _file_uri = _file_uri + "." + _file_type;
            }
            String _ser_file_name = EJBUtil.getActualPath(
                    Configuration.UPLOADFOLDER, _file_name);
            String tmpName = (String)((Hashtable)obj).get("name");
            String nameIndex = "";
            if (tmpName.length() > 4) { nameIndex = tmpName.substring(4); }
            
            FileOutputStream file = null;
            try {
                file = new FileOutputStream(_ser_file_name);
                file.write(bytes, 0, bytes.length);
            } catch(IOException e) {
            } finally {
                if (file != null) try { file.close(); } catch(IOException e) {}
            }
            // store ser_file_name and file_name and all request parameters of the same set in outHash
            if ("".equals(nameIndex)) {
                outHash.put("ser_file_name1", _ser_file_name);
                outHash.put("file_name1", _file_uri);
                outHash.put("desc1", (String[])inHash.get("desc"));
            } else {
                outHash.put("ser_file_name"+nameIndex, _ser_file_name);
                outHash.put("file_name"+nameIndex, _file_uri);
                outHash.put("desc"+nameIndex, (String[])inHash.get("desc"+nameIndex));
                outHash.put("verNumber"+nameIndex, (String[])inHash.get("verNumber"+nameIndex));
                outHash.put("versionDate"+nameIndex, (String[])inHash.get("versionDate"+nameIndex));
                outHash.put("dStudyvercat"+nameIndex, (String[])inHash.get("dStudyvercat"+nameIndex));
                outHash.put("dStudyvertype"+nameIndex, (String[])inHash.get("dStudyvertype"+nameIndex));
            }
        }
        
        return outHash;
    }
    
    protected Hashtable saveFileInDB(Hashtable inHash, Hashtable fileHash, int studyApndxId, int fileHashIndex,
            int incorrectFile, int outOfSpace) {
        Hashtable outHash = new Hashtable();
        
        String maxFileSize[] = (String[]) inHash.get("maxFileSize");
        String accMaxStorage[] = (String[]) inHash.get("accMaxStorage");
        String nextPage[] = (String[]) inHash.get("nextPage");
        String db[] = (String[]) inHash.get("db");
        String tableName[] = (String[]) inHash.get("tableName");
        String columnName[] = (String[]) inHash.get("columnName");
        String pkColumnName[] = (String[]) inHash.get("pkColumnName");
        String pkValue[] = new String[1];
        
        if (studyApndxId > 0) {
            pkValue[0] = String.valueOf(studyApndxId);   // new way
        } else {
            pkValue = (String[]) pkBase; // old way
        }
        
        String _ser_file_name = (String) fileHash.get("ser_file_name"+fileHashIndex);
        
        File server_file = new File(_ser_file_name);
        long _uploadFileSize = server_file.length();
        String _path = "URL=" + nextPage[0];
        outHash.put("uploadFileSize", Long.toString(_uploadFileSize));
        boolean _deleteFlag = false;
        if (_uploadFileSize == 0) {
            _deleteFlag = true;
            incorrectFile++;
        } else if ((_uploadFileSize > Long.parseLong(maxFileSize[0]) && Long
                .parseLong(accMaxStorage[0]) != -1)) {
            _deleteFlag = true;
            outOfSpace++;
        } else {
            int pkValueInt = EJBUtil.stringToNum(pkValue[0]);
            // save file in db
            if (pkValueInt >= 0) {
                SaveFile.saveFile(db[0], tableName[0], columnName[0],
                        pkColumnName[0], _ser_file_name, pkValueInt,
                        _uploadFileSize);
            }
        }
        
        outHash.put("incorrectFile", incorrectFile);
        outHash.put("outOfSpace", outOfSpace);
        outHash.put("deleteFlag", _deleteFlag);
        outHash.put("uploadFileSize", _uploadFileSize);
        
        return outHash;
    }
    
    protected void showForbiddenMessage(ServletOutputStream out) {
        try {
            out.println("<html><head><title>"+LC.L_Forbidden+"</title></head>");
            out.println("<body>");
            out.println("<h1>"+LC.L_Forbidden+"</h1>");
            out.println("<p>"+MC.M_NotPerm_ToAcesServer+"</p>");
            out.println("</body>");
            out.println("</html>");
        } catch (IOException e) {
        }
    }
    
    public static final String CLASS_EXT = ".class";
    public static final String JS_EXT  = ".js";
    public static final String JSP_EXT = ".jsp";
    public static final String EAR_EXT = ".ear";
    public static final String JAR_EXT = ".jar";
    public static final String SAR_EXT = ".sar";
    public static final String WAR_EXT = ".war";
    public static final String BAT_EXT = ".bat";
    public static final String EXE_EXT = ".exe";
    public static final String SH_EXT  = ".sh";
    public static final String DOT_DOT = "..";
    public static final String EMPTY_STR = "";
    public static final String SLASH_STR = "/";
    public static final String BACKSLASH_REGEX = "\\\\";
    
    protected String checkFilename(String filename) {
    	if (StringUtil.isEmpty(filename)) {
    		return filename;
    	}
    	if (filename.indexOf(DOT_DOT) > -1) {
    		System.out.println("filename may not have the .. string");
    		throw new IllegalArgumentException("filename may not have the .. string");
    	}
    	
    	// Remove the path in front of the filename
    	filename = filename.replaceAll(BACKSLASH_REGEX, SLASH_STR);
    	try {
    		String[] pathParts = filename.split(SLASH_STR);
    		if (pathParts.length > 0) {
    			filename = pathParts[pathParts.length-1];
    		} else {
    			filename = filename.replaceAll(SLASH_STR, EMPTY_STR);
    		}
    	} catch(Exception e) {
    		filename = filename.replaceAll(SLASH_STR, EMPTY_STR);
    	}
    	
    	// Check forbidden file extensions
    	String filenameLower = filename.toLowerCase();
    	if (filenameLower.endsWith(CLASS_EXT) ||
    			filenameLower.endsWith(JS_EXT)  ||
    			filenameLower.endsWith(JSP_EXT) ||
    			filenameLower.endsWith(EAR_EXT) ||
    			filenameLower.endsWith(JAR_EXT) ||
    			filenameLower.endsWith(SAR_EXT) ||
    			filenameLower.endsWith(WAR_EXT) ||
    			filenameLower.endsWith(BAT_EXT) ||
    			filenameLower.endsWith(EXE_EXT) ||
    			filenameLower.endsWith(SH_EXT)) {
    		filename = filename+".tmp";
    		//throw new IllegalArgumentException("filename may not have application file extension");
    	}
    	return filename;
    }
    /**
     * This method parses the input, and returns a hashtable of either String[]
     * values (for parameters) or Hashtable values (for files uploaded). The
     * values of the entries in the hashtable are name, filename, Content-Type,
     * and Contents. Note that uploads should be capped in size by the calling
     * method, since otherwise a denial of service attack on server memory
     * becomes trivial.
     */
    Hashtable parseMulti(String boundary, ServletInputStream in,
            ServletOutputStream out) throws IOException {
        int buffSize = 1024 * 1024 * 8; // 8K
        Hashtable hash = new Hashtable();
        int result;
        String line;
        String lowerline;
        String boundaryStr = "--" + boundary;
        ByteArrayOutputStream content;
        String filename;
        String contentType;
        String name;
        String value;
        String Detail_id;
        byte[] b = new byte[buffSize];
        result = in.readLine(b, 0, b.length);
        if (result == -1)
            throw new IllegalArgumentException("InputStream truncated");
        line = new String(b, 0, 0, result);
        if (!line.startsWith(boundaryStr))
            throw new IllegalArgumentException("MIME boundary missing: " + line);
        int ind;
        while (true) {
            System.out.println(b.toString());
            filename = null;
            contentType = null;
            Detail_id = null;
            content = new ByteArrayOutputStream();
            name = null;
            // get next line (should be content disposition)
            result = in.readLine(b, 0, b.length);
            if (result == -1)
                return hash;
            line = new String(b, 0, 0, result - 2);
            int filenamePos1 = line.indexOf("filename=\"");
            int filenameEmpty = line.indexOf("filename=\"\"");
            if (filenameEmpty > -1) {
            	filename = "";
            } else {
            	if (filenamePos1 > -1) {
            		String fileline = new String(b, 0, result - 2, CHARSET);
            		int filenamePos2 = fileline.indexOf("\"", filenamePos1+10);
            		filename = fileline.substring(filenamePos1+10, filenamePos2);
            	}
            }
            try {
            	filename = checkFilename(filename);
            } catch (Exception e) {
            	showForbiddenMessage(out);
            	throw new IOException(e);
            }
            lowerline = line.toLowerCase();
            if (!lowerline.startsWith("content-disposition"))
                continue;
            ind = lowerline.indexOf("content-disposition: ");
            int ind2 = lowerline.indexOf(";");
            System.out.println("a" + lowerline);
            if (ind == -1 || ind2 == -1)
                throw new IllegalArgumentException(
                "Content Disposition line misformatted: " + line);
            String disposition = lowerline.substring(ind + 21, ind2);
            if (!disposition.equals("form-data"))
                throw new IllegalArgumentException(
                "Content Disposition of " + disposition + " is not supported");
            // determine what the name is
            int ind3 = lowerline.indexOf("name=\"", ind2);
            int ind4 = lowerline.indexOf("\"", ind3 + 7);
            if (ind3 == -1 || ind4 == -1)
                throw new IllegalArgumentException(
                "Content Disposition line misformatted: " + line);
            name = line.substring(ind3 + 6, ind4);
            // determine filename, if any
            int ind5 = lowerline.indexOf("filename=\"", ind4 + 2);
            int ind6 = lowerline.indexOf("\"", ind5 + 10);
            if (filename == null && ind5 != -1 && ind6 != -1) {
                filename = line.substring(ind5 + 10, ind6);
            }
            // Next line will either be blank, or Content-Type, followed by
            // blank.
            result = in.readLine(b, 0, b.length);
            System.out.println("result:: " + result);
            if (result == -1)
                return hash;
            line = new String(b, 0, 0, result - 2); // -2 to remove \r\n
            lowerline = line.toLowerCase();
            if (lowerline.startsWith("content-type")) {
                int ind7 = lowerline.indexOf(" ");
                if (ind7 == -1)
                    throw new IllegalArgumentException(
                    "Content-Type line misformatted: " + line);
                contentType = lowerline.substring(ind7 + 1);
                // read blank header line
                result = in.readLine(b, 0, b.length);
                if (result == -1)
                    return hash;
                line = new String(b, 0, 0, result - 2); // -2 to remove \r\n
                if (line.length() != 0) {
                    throw new IllegalArgumentException(
                    "Unexpected line in MIMEpart header: " + line);
                }
            } else if (line.length() != 0) {
                throw new IllegalArgumentException(
                "Misformatted line following disposition: " + line);
            }
            // read content, implement readahead by one line
            boolean readingContent = true;
            boolean firstLine = true;
            byte[] buffbytes = new byte[buffSize];
            int buffnum = 0;
            result = in.readLine(b, 0, b.length);
            if (result == -1)
                return hash;
            line = new String(b, 0, 0, result);
            if (!line.startsWith(boundaryStr)) {
                System.arraycopy(b, 0, buffbytes, 0, result);
                buffnum = result;
                result = in.readLine(b, 0, b.length);
                if (result == -1)
                    return hash;
                line = new String(b, 0, 0, result);
                firstLine = false;
                if (line.startsWith(boundaryStr)) {
                    readingContent = false;
                }
            }
            else {
                readingContent = false;
            }
            // out.println("***" + readingContent);
            while (readingContent) {
                content.write(buffbytes, 0, buffnum);
                System.arraycopy(b, 0, buffbytes, 0, result);
                buffnum = result;
                result = in.readLine(b, 0, b.length);
                if (result == -1)
                    return hash;
                line = new String(b, 0, 0, result);
                if (line.startsWith(boundaryStr))
                    readingContent = false;
            }
            if (!firstLine) {
                if (buffnum > 2)
                    content.write(buffbytes, 0, buffnum - 2);
            }
            // now set appropriate variable, populate hashtable
            if (filename == null) {
                if (hash.get(name) == null) {
                    String[] values = new String[1];
                    values[0] = content.toString();
                    hash.put(name, values);
                } else {
                    Object prevobj = hash.get(name);
                    if (prevobj instanceof String[]) {
                        String[] prev = (String[]) prevobj;
                        String[] newStr = new String[prev.length + 1];
                        System.arraycopy(prev, 0, newStr, 0, prev.length);
                        newStr[prev.length] = content.toString();
                        hash.put(name, newStr);
                    } else {
                        throw new IllegalArgumentException(
                                "failure in parseMulti hashtable building code");
                    }
                }
                // out.println(hash.toString());
            } else {
                // Yes, we don't return Hashtable[] for multiple files of same
                // name. AFAIK, that's not allowed.
                Hashtable filehash = new Hashtable(4);
                filehash.put("name", name);
                filehash.put("filename", filename);
                // String Detail_id=
                // filehash.put("Detail_id",in.GetParameter("Detail_id"));
                if (contentType == null)
                    contentType = "application/octet-stream";
                filehash.put("content-type", contentType);
                filehash.put("content", content.toByteArray());
                System.out.println("content" + content.toByteArray());
                hash.put(name, filehash);
                /* out.println(hash.toString()); */
            }
        }
    }
}
