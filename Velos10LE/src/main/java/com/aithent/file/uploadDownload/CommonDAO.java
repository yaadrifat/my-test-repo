/**
 * Base class for Data Access classes. Contains method to make a database connection.
 *
 * @author Sajal
 *
 * @version 1.0
 *
 */

package com.aithent.file.uploadDownload;

/* Import statments */
import java.sql.Connection;
import java.sql.DriverManager;

/* End of Import statments */

/**
 * Base class for Data Access classes. Contains method to make a database
 * connection.
 */

public class CommonDAO {

    /**
     * Makes a connection to the database and returns a Connection object
     * 
     * @return a Connection object
     * 
     */

    public static Connection getConnection(String db) {
        try {
            Configuration.readSettings(db);
        } catch (Exception e) {
            System.out
                    .println("EXCEPTION IN CommonDao.getConnnection IN COMMON DAO"
                            + e);
        }
        Connection conn = null;
        try {
            Class.forName(Configuration.DBDriverName);
            conn = DriverManager.getConnection(Configuration.DBUrl,
                    Configuration.DBUser, Configuration.DBPwd);
            return conn;
        } catch (Exception e) {
            System.out
                    .println("EXCEPTION IN CommonDao.getConnnection :getConnection(): exception:"
                            + e);
            return null;
        }
    }

}