/**
 * Extends HttpServlet class, used to upload a file to the database. The file can be uploaded only to a Blob type column.
 *
 * @author Sajal
 *
 * @version 1.0
 *
 */

package com.aithent.file.uploadDownload;

// import statements
import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.user.UserJB;
import com.velos.esch.web.bgtApndx.BgtApndxJB;

/**
 * Extends HttpServlet class, used to upload a file to the database. The file
 * can be uploaded only to a Blob type column.
 * 
 */
public class BgtApndx extends FileUploadServlet {

    /**
     * 
     */
    private static final long serialVersionUID = -5782460992039044532L;

    public void init(ServletConfig sc) throws ServletException {

    }

    /**
     * Overrides the doPost method of HttpServlet. Extracts the parameters sent
     * by the servlet and uploads the specified file with the specified name
     * from the temporary path specified in the XML file to the table specified.
     * 
     * @param httpservletrequest
     *            an HttpServletRequest object that contains the request the
     *            client has made of the servlet
     * 
     * @param httpservletresponse
     *            an HttpServletResponse object that contains the response the
     *            servlet sends to the client
     * 
     */
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        if (session == null ||
        		(StringUtil.stringToNum((String)session.getAttribute(USER_ID)) < 1 &&
                		session.getAttribute(PP_IGNORE_ALL_RIGHTS) == null)) {
        	showForbiddenMessage(res.getOutputStream());
        	Rlog.fatal("fileUpload", "Forbidden_error: invalid_session");
        	return;
        }

        if (!req.getContentType().toLowerCase().startsWith(
                "multipart/form-data")) {
            // Since this servlet only handles File Upload, bail out
            // Note: this isn't strictly legal - I should send
            // a custom error message
            return;
        }
        int ind = req.getContentType().indexOf("boundary=");
        if (ind == -1) {
            return;
        }
        String boundary = req.getContentType().substring(ind + 9);
        if (boundary == null) {
            return;
        }
        try {
            table = parseMulti(boundary, req.getInputStream(), res.getOutputStream());
        } catch (Throwable t) {
            t.printStackTrace();
            return;
        }

        String[] eSign = new String[1];
        String[] userId = new String[1];
        String[] nextPage = new String[1];
        
        String[] mode   = new String[1];
        String[] ipAdd  = new String[1];

        eSign = (String[]) table.get("eSign");
        userId = (String[]) table.get("userId");
        nextPage = (String[]) table.get("nextPage");
        ipAdd = (String[]) table.get("ipAdd");
        
        mode = (String[]) table.get("mode");
       

        UserJB userB = new UserJB();
        userB.setUserId(EJBUtil.stringToNum(userId[0]));
        userB.getUserDetails();
        
        String actualESign = userB.getUserESign();
        out = res.getOutputStream();

        if (eSign[0].equals(actualESign)) {

            System.out.println("BgtApndx.doPost line 1");
            BgtApndxJB bgtApndxJB = new BgtApndxJB();
            System.out.println("BgtApndx.doPost line 2");
            bgtApndxJB.setCreator(userId[0]);
            bgtApndxJB.setModifiedBy(userId[0]);
            bgtApndxJB.setIpAdd(ipAdd[0]);
            int bgtApndxId = bgtApndxJB.setBgtApndxDetails();
            System.out.println("BgtApndx.doPost line 3");
            pkBase[0] = String.valueOf(bgtApndxId);
            System.out.println("BgtApndx.doPost line 4");

            System.out.println("in side do post of BgtApndxdsdsdsdsds");
            System.out.println(pkBase[0]);
            System.out.println("BgtApndx.doPost line 5");

            super.doPost(req, res);
            if (deleteFlag == false) {
                System.out.println("BgtApndx.doPost line 6");

                String[] desc = (String[]) table.get("desc");
                System.out.println("BgtApndx.doPost line 7");
                String[] type = (String[]) table.get("type");
                System.out.println("BgtApndx.doPost line 8");
                String[] bgtId = (String[]) table.get("bgtId");
                System.out.println("BgtApndx.doPost line 9");
                System.out.println(file_name);
                System.out.println("BgtApndx.doPost line 10");
                bgtApndxJB.setBgtApndxId(bgtApndxId);
                System.out.println("BgtApndx.doPost line 11");
                bgtApndxJB.getBgtApndxDetails();
                System.out.println("BgtApndx.doPost line 12");
                bgtApndxJB.setBgtApndxDesc(desc[0]);
                System.out.println("BgtApndx.doPost line 13");
                bgtApndxJB.setBgtApndxType(type[0]);
                System.out.println("BgtApndx.doPost line 14");
                bgtApndxJB.setBgtApndxBudget(bgtId[0]);
                // Added By Amarnadh for Bugzilla issue #3130
                bgtApndxJB.setIpAdd(ipAdd[0]);
               // modified by Amarnadh to fix the bug #3011
                bgtApndxJB.setCreator(userId[0]);
           
                
                bgtApndxJB.setBgtApndxUri(file_name);
                bgtApndxJB.updateBgtApndx();
            } else {
                bgtApndxJB.bgtApndxDelete(bgtApndxId);
            }

            /*
             * out.println("<HTML>"); out.println("<BODY>"); out.println("<META
             * HTTP-EQUIV=Refresh CONTENT=\"0;"+path+"\""); out.println("</BODY>");
             * out.println("</HTML>");
             */
            res.sendRedirect("../../" + path);
            System.out.println("path>>>>"+path);
        } else {
            /*
             * out.println("<HTML>"); out.println("<BODY>"); out.println("<META
             * HTTP-EQUIV=Refresh
             * CONTENT=\"0;URL=../../eres/jsp/incorrectesign.jsp\"");
             * out.println("</BODY>"); out.println("</HTML>");
             */
            res.sendRedirect("../../velos/jsp/incorrectesign.jsp");
        }

    }

    /**
     * 
     * 
     * Obtain information on this servlet.
     * 
     * 
     * @return String describing this servlet.
     * 
     * 
     */

    public String getServletInfo() {

        return "File upload servlet -- used to receive files";

    }

}
