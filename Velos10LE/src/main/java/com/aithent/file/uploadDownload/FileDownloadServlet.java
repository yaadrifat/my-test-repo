/**
 * Used to Download a file from the database. The file can be downloaded only if it is stored in a Blob type column.
 *
 * @author Sajal
 *
 * @version 1.0
 *
 */

package com.aithent.file.uploadDownload;

// import statements
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.velos.eres.business.common.ObjectMapDao;
import com.velos.eres.business.common.StudyApndxDao;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.MC;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.Security;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.user.UserJB;
import com.velos.eres.web.userReportLog.UserReportLogJBHelper;

/**
 * Extends HttpServlet class, used to Download a file from the database. The
 * file can be downloaded only if it is stored in a Blob type column.
 * 
 */

public class FileDownloadServlet extends HttpServlet {
    /**
     * 
     */
    private static final long serialVersionUID = 8323413786733439206L;

    String downloadFolder;
    
    private static final String USERNAME_STR = "username";
    private static final String PASSWORD_STR = "password";
    private static final String OID_STR = "OID";
    private static final String STUDY_STR = "study";
    private static final String STUDYAPNDX_FILEOBJ_STR = "STUDYAPNDX_FILEOBJ";
    private static final String PK_STUDYAPNDX_STR = "PK_STUDYAPNDX";
    private static final String ERES_STR = "eres", SCH_STR = "sch";
    private static final HashMap<String, Integer> allowedApndxTables = new HashMap<String, Integer>();
    
    static {
    	allowedApndxTables.put("er_studyapndx", 1);
    }

    /**
     * Default constructor that initializes the class variables
     * 
     */
    public FileDownloadServlet() {
        downloadFolder = null;
    }

    /**
     * Calls the Servlet's init(ServletConfig) method.
     * 
     * @param servletconfig
     *            a ServletConfig object containing the servlet's configuration
     *            and initialization parameters
     * 
     */
    public void init(ServletConfig servletconfig) throws ServletException {
        super.init(servletconfig);
    }

    /**
     * Overrides the doGet method of HttpServlet. Extracts the parameters sent
     * by the servlet and downloads the specified file with the specified name
     * at the temporary path specified in the XML file.
     * 
     * @param httpservletrequest
     *            an HttpServletRequest object that contains the request the
     *            client has made of the servlet
     * 
     * @param httpservletresponse
     *            an HttpServletResponse object that contains the response the
     *            servlet sends to the client
     * 
     */
    public void doGet(HttpServletRequest httpservletrequest,
            HttpServletResponse httpservletresponse) throws ServletException, IOException {
    	if (processByOID(httpservletrequest,httpservletresponse)) {
    		return;
    	}
    	processServlet(httpservletrequest,httpservletresponse);
    }
    
    public void doPost(HttpServletRequest httpservletrequest,
            HttpServletResponse httpservletresponse) throws ServletException, IOException {
    	if (processByOID(httpservletrequest,httpservletresponse)) {
    		return;
    	}
    	processServlet(httpservletrequest,httpservletresponse);

    }
    
    /**
     *  Method to process download by document OID. This is used for eSP.
     *  
     *  @return boolean processCompleted
     *  if true, process is completed so do not process further.
     *  if false, proceed to normal processing.
     */
    private boolean processByOID(HttpServletRequest httpservletrequest,
            HttpServletResponse httpservletresponse) throws ServletException, IOException {
    	String username = (String) httpservletrequest.getParameter(USERNAME_STR);
    	String password = (String) httpservletrequest.getParameter(PASSWORD_STR);
    	String oid = (String) httpservletrequest.getParameter(OID_STR);
    	if (username == null || password == null || oid == null) { return false; }
    	UserJB userJB = new UserJB();
    	UserBean userBean = userJB.validateUser(username, password);
    	if (userBean != null && userBean.getUserLoginName() != null) {
    		processByOIDDetails(httpservletrequest, httpservletresponse);
    		return true;
    	}
    	return false;
    }
    
    private void processByOIDDetails(HttpServletRequest httpservletrequest,
            HttpServletResponse httpservletresponse) throws ServletException, IOException {
    	String oid = (String) httpservletrequest.getParameter(OID_STR);
    	ObjectMapDao objectMapDao = new ObjectMapDao();
    	objectMapDao.getObjectMap(oid);
    	String tableName = objectMapDao.getTableName();
    	int tablePk = objectMapDao.getTablePk();
    	System.out.println("In processByOIDDetails: tableName="+tableName+" tablePk="+tablePk);
    	if (allowedApndxTables.get(tableName) == null) {
    		return;
    	}
    	StudyApndxDao studyApndxDao = new StudyApndxDao();
    	studyApndxDao.getUriByPk(tablePk);
    	if (Configuration.FILE_UPLOAD_DOWNLOAD == null) { Configuration.readSettings(SCH_STR); }
        Configuration.readUploadDownloadParam(Configuration.FILE_UPLOAD_DOWNLOAD
        		+"fileUploadDownload.xml", STUDY_STR);
        String filepath = EJBUtil.getActualPath(Configuration.DOWNLOADFOLDER, studyApndxDao.getStudyApndxUri());
        if (!SaveFile.readFile(ERES_STR, tableName, STUDYAPNDX_FILEOBJ_STR, PK_STUDYAPNDX_STR, filepath, tablePk)) {
            System.out.println("FileDownloadServlet.processByOIDDetails could not read file");
        }
        File file = new File(filepath);
        FileInputStream fileinputstream = new FileInputStream(file);
        filepath = StringUtil.decodeString(filepath);
        filepath = checkFilename(filepath);
        String targetFileName = StringUtil.replace(filepath, "/", "");
    	httpservletrequest.setCharacterEncoding(CHARSET);
    	httpservletresponse.setCharacterEncoding(CHARSET);
        httpservletresponse.setContentType(SaveFile.getApplicationType(filepath));
        httpservletresponse.setContentLength((int) file.length());
        httpservletresponse.setHeader("Content-disposition", "inline; filename=\"" + targetFileName + "\"");
        javax.servlet.ServletOutputStream servletoutputstream = httpservletresponse.getOutputStream();
        int j = 0; // used as counter
        int i = 0; // used as counter
        int size = 1024 * 1024 * 8; // defined to download a file in the chunks
        byte abyte0[] = new byte[size]; // a byte array of the size specified
        do {
            i = fileinputstream.read(abyte0);
            if (i != -1) {
                servletoutputstream.write(abyte0, 0, i);
                j += i;
            }
        } while (i == size);
        fileinputstream.close();
        servletoutputstream.flush();
        servletoutputstream.close();
    }
    
    private void showForbiddenMessage(HttpServletResponse resp) {
        try {
            ServletOutputStream out = resp.getOutputStream();
            out.println("<html><head><title>"+LC.L_Forbidden+"</title></head>");
            out.println("<body>");
            out.println("<h1>"+LC.L_Forbidden+"</h1>");
            out.println("<p>"+MC.M_NotPerm_ToAcesServer+"</p>");
            out.println("</body>");
            out.println("</html>");
        } catch (IOException e) {
        }
    }
    
    public static final String CLASS_EXT = ".class";
    public static final String JS_EXT  = ".js";
    public static final String JSP_EXT = ".jsp";
    public static final String EAR_EXT = ".ear";
    public static final String JAR_EXT = ".jar";
    public static final String SAR_EXT = ".sar";
    public static final String WAR_EXT = ".war";
    public static final String BAT_EXT = ".bat";
    public static final String EXE_EXT = ".exe";
    public static final String SH_EXT  = ".sh";
    public static final String DOT_DOT = "..";
    public static final String EMPTY_STR = "";
    public static final String SLASH_STR = "/";
    public static final String BACKSLASH_REGEX = "\\\\";
    
    private String checkFilename(String filename) {
    	if (StringUtil.isEmpty(filename)) {
    		return filename;
    	}
    	if (filename.indexOf(DOT_DOT) > -1) {
    		filename = filename.replaceAll("\\.\\.", "");
    	}
    	
    	// Remove the path in front of the filename
    	filename = filename.replaceAll(BACKSLASH_REGEX, SLASH_STR);
    	try {
    		String[] pathParts = filename.split(SLASH_STR);
    		if (pathParts.length > 0) {
    			filename = pathParts[pathParts.length-1];
    		} else {
    			filename = filename.replaceAll(SLASH_STR, EMPTY_STR);
    		}
    	} catch(Exception e) {
    		filename = filename.replaceAll(SLASH_STR, EMPTY_STR);
    	}
    	
    	// Check forbidden file extensions
    	String filenameLower = filename.toLowerCase();
    	if (filenameLower.endsWith(CLASS_EXT) ||
    			filenameLower.endsWith(JS_EXT)  ||
    			filenameLower.endsWith(JSP_EXT) ||
    			filenameLower.endsWith(EAR_EXT) ||
    			filenameLower.endsWith(JAR_EXT) ||
    			filenameLower.endsWith(SAR_EXT) ||
    			filenameLower.endsWith(WAR_EXT) ||
    			filenameLower.endsWith(BAT_EXT) ||
    			filenameLower.endsWith(EXE_EXT) ||
    			filenameLower.endsWith(SH_EXT)) {
    		filename = filename+".tmp";
    	}
    	return filename;
    }
    
    public static final String USER_ID = "userId";
    public static final String PP_IGNORE_ALL_RIGHTS = "pp_ignoreAllRights";
    public static final String CHARSET = "UTF-8";

    private void processServlet(HttpServletRequest httpservletrequest,
            HttpServletResponse httpservletresponse) throws ServletException, IOException
            {
    	httpservletrequest.setCharacterEncoding(CHARSET);
    	httpservletresponse.setCharacterEncoding(CHARSET);

        // Security check - Make sure the session in request has same ID as the ID stored in cookies
        HttpSession session = httpservletrequest.getSession(false);
        
        if (session == null ||
        		(StringUtil.stringToNum((String)session.getAttribute(USER_ID)) < 1 &&
        		session.getAttribute(PP_IGNORE_ALL_RIGHTS) == null)) {
            showForbiddenMessage(httpservletresponse);
            Rlog.fatal("fileDownload", "Forbidden_error: invalid_session");
            return;
        }
    	System.out.println("Download userId="+session.getAttribute(USER_ID));
        
        String filename1 = httpservletrequest.getParameter("file");
        if (filename1 == null) {
            showForbiddenMessage(httpservletresponse);
            Rlog.fatal("fileDownload", "Forbidden_error: file");
            return;
        }
        
        if (filename1.startsWith("report") &&
        		null == httpservletrequest.getHeader("referer")) {
            showForbiddenMessage(httpservletresponse);
            Rlog.fatal("fileDownload", "Forbidden_error: report");
            return;
        }
        
        String sessionId = (String) session.getId();
        if (filename1.startsWith("dynxml") || filename1.startsWith("dyndoc") ||
                filename1.startsWith("dynexcel") || filename1.startsWith("export")) {
            if (httpservletrequest.getParameter("key") == null) {
                showForbiddenMessage(httpservletresponse);
                Rlog.fatal("fileDownload", "Forbidden_error: adhoc");
                return;
            }
            String assembledKey = "";
            try {
                String incomingKey = httpservletrequest.getParameter("key");
                String [] chunks = new String[incomingKey.length() / 3];
                int index = 0;
                for (int iX=0; iX+2<incomingKey.length(); ) {
                    chunks[index++] = incomingKey.substring(iX,iX+3);
                    iX = iX + 3;
                }
                char[] chs = new char[chunks.length];
                for (int iX=0; iX<chs.length; iX++) {
                    chs[iX] = (char)Integer.parseInt(chunks[iX]);
                }
                assembledKey = new String(chs);
            } catch(Exception e) {
                Rlog.fatal("fileDownload", "Exception "+e);
            }
            String sessId = Security.decrypt(assembledKey);
            if ("error".equals(sessId)) {
                showForbiddenMessage(httpservletresponse);
                Rlog.fatal("fileDownload", "Forbidden_error: assembledKey");
                return;
            }
            if (!sessionId.startsWith(sessId)) {
                showForbiddenMessage(httpservletresponse);
                Rlog.fatal("fileDownload", "Forbidden_error: scheck");
                return;
            }
        }
        
        Cookie[] cookies = httpservletrequest.getCookies();
        if (cookies == null) {
            showForbiddenMessage(httpservletresponse);
            Rlog.fatal("fileDownload", "Forbidden_error: no_cookie");
            return;
        }
        boolean hasGoodCookie = false;
        for (int iX=0; iX<cookies.length; iX++) {
            if ("JSESSIONID".equals(cookies[iX].getName())) {
                String JSessID = cookies[iX].getValue();
                if (JSessID.equals(session.getId())) {
                	hasGoodCookie = true;
                    break;
                }
            }
        }
        if (!hasGoodCookie) {
            showForbiddenMessage(httpservletresponse);
            Rlog.fatal("fileDownload", "Forbidden_error: bad_cookie");
            return;
        }
        // -- End of security check
        
        int j = 0; // used as counter
        int i = 0; // used as counter
        int size = 1024 * 1024 * 8; // defined to download a file in the chunks
        // of this size
        byte abyte0[] = new byte[size]; // a byte array of the size specified
        // above
        /*
         * To read the Upload and Download parameters spoecified in the XML
         * file(fileUploadDownload.xml). These values are stored in the static
         * variables of Configuration.class file. FILE_UPLOAD_DOWNLOAD is a
         * static variable in the Configuration.class file that contains the
         * value of the environment variable FILE_UPLOAD_DOWNLOAD set at the
         * server.
         */

        try {
        	//System.out.println("FileDownloadServlet.doPost before request.setCharacterEncoding();" + httpservletrequest.getCharacterEncoding());
        	 
            String s = httpservletrequest.getParameter("file");
            // String s = httpservletrequest.getPathInfo(); // would contain any
            // extra path
            // information
            // associated with
            // the URL the
            // client sent when
            // it made this
            // request.

            // Decode the file name

            com.velos.eres.service.util.EJBUtil ejbUtil = new com.velos.eres.service.util.EJBUtil();
            s = StringUtil.decodeString(s);
            s = checkFilename(s);
            String targetFileName = StringUtil.replace(s, "/", ""); // SV,
           
            //System.out.println("FileDownloadServlet.doPost after request.setCharacterEncoding();" + httpservletrequest.getCharacterEncoding());

            int pkValue = EJBUtil.stringToNum(httpservletrequest
                    .getParameter("pkValue")); // the primary key value of the
            // row against which the file
            // needs to be downloaded

            String db = httpservletrequest.getParameter("db"); // db entry in
            // the
            // fileuploaddownload.xml
            // to be used
            // for this
            // operation

            String module = httpservletrequest.getParameter("module"); // module
            // entry
            // in
            // the
            // fileuploaddownload.xml
            // to be
            // used
            // for
            // file
            // upload

            String tableName = httpservletrequest.getParameter("tableName"); // table
            // from
            // which
            // the
            // file
            // needs
            // to
            // be
            // downloaded

            String columnName = httpservletrequest.getParameter("columnName"); // blob
            // column
            // where
            // the
            // file
            // has
            // been
            // saved
            // in
            // the
            // database

            String pkColumnName = httpservletrequest
                    .getParameter("pkColumnName"); // the name of Primary Key
            // column of the table where
            // the file is saved
            if (Configuration.FILE_UPLOAD_DOWNLOAD==null) Configuration.readSettings("sch");
            Configuration.readUploadDownloadParam(
                    Configuration.FILE_UPLOAD_DOWNLOAD
                            + "fileUploadDownload.xml", module);

            s = EJBUtil.getActualPath(Configuration.DOWNLOADFOLDER, s); // would
            // get
            // the
            // complete
            // path
            // where
            // the
            // file
            // would
            // be
            // temporarily
            // downloaded

            // read the file from the database and create it on the load folder,
            // so that servlet can read it
            if (!SaveFile.readFile(db, tableName, columnName, pkColumnName, s,
                    pkValue)) {
                System.out
                        .println("FileDoenloadServlet.doGet could not read file");
            }
            File file = new File(s);

            FileInputStream fileinputstream = new FileInputStream(file);
 
            
            
            httpservletresponse.setContentType(SaveFile.getApplicationType(s));

            httpservletresponse.setContentLength((int) file.length());

            // SV, 7/27/04, Fix for bug #1201, following line sets the file name
            // to the decoded file name, so the Browser SaveAs dialog will show
            // file name correctly.
            httpservletresponse.setHeader("Content-disposition", "inline; filename=\"" + targetFileName + "\"");
            
            // Bug 10290: Commenting this out because it causes problem for FF
            //httpservletresponse.setHeader("Content-Type", "charset=UTF-8");

            javax.servlet.ServletOutputStream servletoutputstream = httpservletresponse
                    .getOutputStream();

            do {
                i = fileinputstream.read(abyte0);
                if (i != -1) {
                    servletoutputstream.write(abyte0, 0, i);
                    j += i;
                }
            } while (i == size);
            // System.out.println("FileDownloadServlet.doGet line 14");
            fileinputstream.close();
            // System.out.println("FileDownloadServlet.doGet line 15");
            servletoutputstream.flush();
            // System.out.println("FileDownloadServlet.doGet line 16");
            servletoutputstream.close();
            // System.out.println("FileDownloadServlet.doGet line 17");

            // delete the temporary file from the server - file will not be
            // deleted now
            // file.delete();
        } catch (IOException _ex) {
            try {
                System.out
                        .println("FileDownloadServlet.doGet exception " + _ex);
                String s1 = httpservletrequest.getParameter("errors");
                if (s1 != null)
                    httpservletresponse.sendRedirect(s1);
                else
                    httpservletresponse.sendError(404);
            } catch (IOException _ex2) {
                System.out.println("FileDownloadServlet.doGet exception2 "
                        + _ex2);
            }
        }finally{
        	if(!httpservletrequest.getParameter("moduleName").equalsIgnoreCase("ctrpDraft"))
        		UserReportLogJBHelper.saveLoggingData(httpservletrequest);
        }
    	
            }
}

