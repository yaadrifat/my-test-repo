/**
 * Used to Save the file to the database and extract the file from the database
 *
 * @author Sajal
 *
 * @version 1.0
 *
 */

package com.aithent.file.uploadDownload;

/* import statements */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.StringUtil;

import oracle.jdbc.driver.OracleResultSet;
import oracle.sql.BLOB;

/**
 * Consists of methods to upload a file to a Blob column in a database and to
 * download a file from the Blob column in a database to a specified folder.
 * 
 */

public class SaveFile extends CommonDAO implements java.io.Serializable {

    /**
     * Default blank constructor.
     * 
     */
    public SaveFile() {
    }

    /**
     * Used to upload a file in the blob column in a database from the specified
     * location on the server.
     * 
     * @param tableName
     *            a String that is the name of the table to which the file needs
     *            to be uploaded.
     * 
     * @param columnName
     *            a String that is the name of the column in the table to which
     *            the file needs to be uploaded. This column needs to be of a
     *            Blob type.
     * 
     * @param pkColumnName
     *            a String that is the name of the primary key column in the
     *            table to which the file needs to be uploaded. This column
     *            uniquely identifies a row in that table.
     * 
     * @param filPath
     *            a String that is the complete path and file name of the file
     *            on the server that needs to be uploaded to the database.
     * 
     * @param pkValueInt
     *            an int that is the vale of the primary key column
     *            pkColumnName. The file that needs to be uploaded would be
     *            saved in the 'columnName' column of 'tableName' table.
     * 
     * @param uploadFileSize
     *            a long indicating the size of the file that needs to be
     *            uploaded.
     * 
     * @return <code>true</code> if the file is saved successfully
     *         <code>false</code> otherwise.
     * 
     */
    public static boolean saveFile(String db, String tableName,
            String columnName, String pkColumnName, String filPath,
            int pkValueInt, long uploadFileSize) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        int filekb = 0;
        filekb = (int) uploadFileSize / 1024;
        // set code type
        try {
            
        	conn = getConnection(db);
            conn.setAutoCommit(false);
             
            File file = new File(filPath);
            // fileSize = file.length();
             
            InputStream istr = new FileInputStream(file);
            pstmt = conn.prepareStatement("Update " + tableName + " set "
                    + columnName + " = empty_blob() where " + pkColumnName
                    + " = " + pkValueInt);
             
            pstmt.execute();
            pstmt.close();
            // get the empty blob from the row saved above
            Statement b_stmt = conn.createStatement();
            String sqlStr = "SELECT " + columnName + " from " + tableName
                    + " WHERE " + pkColumnName + " = " + pkValueInt
                    + " FOR UPDATE";
             
            ResultSet savedBlob = b_stmt.executeQuery(sqlStr);
            // Retrieve BLOB streams and load the files
             
            if (savedBlob.next()) {
                // Get the BLOB locator and open output stream for the BLOB
                BLOB filBLOB = ((OracleResultSet) savedBlob).getBLOB(1);
                OutputStream l_blobOutputStream = filBLOB
                        .getBinaryOutputStream();
                byte[] l_buffer = new byte[10 * 1024];
                int l_nread = 0; // Number of bytes read
                while ((l_nread = istr.read(l_buffer)) != -1)
                    // Read from file
                    l_blobOutputStream.write(l_buffer, 0, l_nread); // Write to
                // BLOB
                // Close both streams
                istr.close();
                l_blobOutputStream.close();
            }
            conn.commit();
            savedBlob.close();
            b_stmt.close();
             
            // delete the temporary file that was created on the server
            file.delete();
            return true;
        } catch (Exception ex) {
            System.out.println("EXCEPTION IN SAVING FILE" + ex);
            ex.printStackTrace();
            return false;
        } finally {
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

    /**
     * Used to download a file from the blob column in a database to a specified
     * location on the server.
     * 
     * @param tableName
     *            a String that is the name of the table where the file is
     *            currently stored.
     * 
     * @param columnName
     *            a String that is the name of the column in the table where the
     *            file needs to be downloaded from. This column needs to be of a
     *            Blob type.
     * 
     * @param pkColumnName
     *            a String that is the name of the primary key column in the
     *            table from where the file needs to be downloaded. This column
     *            uniquely identifies a row in that table.
     * 
     * @param filPath
     *            a String that is the complete path where the file needs to be
     *            downloaded. It is the complete path with the file name. The
     *            downloaded file would be of the specified name.
     * 
     * @param pkValueInt
     *            an int that is the vale of the primary key column
     *            pkColumnName. The file that needs to be downloaded from the
     *            'columnName' column of 'tableName' table.
     * 
     * @return <code>true</code> if the file is downloaded successfully
     *         <code>false</code> otherwise.
     * 
     */
    public static boolean readFile(String db, String tableName,
            String columnName, String pkColumnName, String filPath,
            int pkValueInt) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        if (db == null) { db = "eres"; }
        // set code type
        try {
             
        	conn = getConnection(db);
             
            String sqlStr = "Select " + columnName + " from " + tableName
                    + " where " + pkColumnName + " = " + pkValueInt;
            pstmt = conn.prepareStatement(sqlStr);
             
            ResultSet rs = pstmt.executeQuery();
             
            if (rs.next()) {
                 // get binary stream data from result set
                InputStream dbFile = (rs.getBlob(1)).getBinaryStream();
                 // open operating system file for storing the image
                FileOutputStream os = new FileOutputStream(filPath);
                 // fetch-write loop, fetch image data from rs, write to file
                int i;
                while ((i = dbFile.read()) != -1)
                    os.write(i);
                 // Close the destination output stream
                os.close();
            }
            return true;
        } catch (Exception ex) {
            System.out
                    .println("SaveFile.readFile EXCEPTION IN SAVING FILE FROMCONTROL TABLE "
                            + ex);
            ex.printStackTrace();
            return false;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }
    
    /**
     * returs appropriate content type depending on the file name. Handles only commonly used aplications
     * */
    public static String getApplicationType(String fileName)
    {
    	String appType = "application/octet-stream";
    	
    	String fileNameInLowerCase="";
    	
    	
    	
    	if (! StringUtil.isEmpty(fileName))
    		{
    		
    			fileNameInLowerCase=fileName.toLowerCase();
    		
    			if ( fileNameInLowerCase.endsWith(".pdf") )
    			{
    				appType = "application/pdf";
    			}else if  ( fileNameInLowerCase.endsWith(".doc") )
    			{
    				appType = "application/msword";
    			}
    			else if  ( fileNameInLowerCase.endsWith(".xls") )
    			{
    				appType = "application/vnd.ms-excel";
    			}
    			else if  ( fileNameInLowerCase.endsWith(".ppt") )
    			{
    				appType = "application/vnd.ms-powerpoint";
    			}
    			else if  ( fileNameInLowerCase.endsWith(".rtf") )
    			{
    				appType = "application/vnd.ms-rtf";
    			}
    			else if ( fileNameInLowerCase.endsWith(".jpg") || fileNameInLowerCase.endsWith(".gif") || fileNameInLowerCase.endsWith(".jpeg"))
    			{
    				appType = "image/jpeg";
    			}
    			else if ( fileNameInLowerCase.endsWith(".txt") ) {
    			    appType = "text/plain";
    			}
    		}
    	
    	System.out.println("appType returned:" +appType);
    	
    	return appType;
    	
    	
    }
}
