/**
 * Extends HttpServlet class, used to upload a file to the database. The file can be uploaded only to a Blob type column.
 *
 * @author Anu
 *
 * @version 1.0
 *
 */

package com.aithent.file.uploadDownload;

// import statements
import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.studySiteApndx.StudySiteApndxJB;
import com.velos.eres.web.user.UserJB;

/**
 * Extends HttpServlet class, used to upload a file to the database. The file
 * can be uploaded only to a Blob type column.
 *
 */
public class StudySiteApndx extends FileUploadServlet {

    public void init(ServletConfig sc) throws ServletException {
        System.out.println("StudySiteApndx initialized");

    }

    /**
     * Overrides the doPost method of HttpServlet. Extracts the parameters sent
     * by the servlet and uploads the specified file with the specified name
     * from the temporary path specified in the XML file to the table specified.
     *
     * @param httpservletrequest
     *            an HttpServletRequest object that contains the request the
     *            client has made of the servlet
     *
     * @param httpservletresponse
     *            an HttpServletResponse object that contains the response the
     *            servlet sends to the client
     *
     */
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        if (session == null ||
        		(StringUtil.stringToNum((String)session.getAttribute(USER_ID)) < 1 &&
                		session.getAttribute(PP_IGNORE_ALL_RIGHTS) == null)) {
        	showForbiddenMessage(res.getOutputStream());
        	Rlog.fatal("fileUpload", "Forbidden_error: invalid_session");
        	return;
        }

        System.out.println("StudySiteApndx in do post 1");
        if (!req.getContentType().toLowerCase().startsWith(
                "multipart/form-data")) {
            System.out.println("StudySiteApndx in do post 2");
            // Since this servlet only handles File Upload, bail out
            // Note: this isn't strictly legal - I should send
            // a custom error message
            return;
        }
        int ind = req.getContentType().indexOf("boundary=");
        System.out.println("StudySiteApndx in do post 3");
        if (ind == -1) {
            System.out.println("StudySiteApndxin do post 4");
            return;
        }
        System.out.println("StudySiteApndxin do post 5");
        String boundary = req.getContentType().substring(ind + 9);
        System.out.println("StudySiteApndxin do post 6" + boundary);
        if (boundary == null) {
            System.out.println("StudySiteApndxin do post 7");
            return;
        }
        try {
            System.out.println("StudySiteApndxin do post 8");
            table = parseMulti(boundary, req.getInputStream(), res.getOutputStream());
            System.out.println("StudySiteApndxin do post 9");
        } catch (Throwable t) {
            System.out.println("StudySiteApndxin do post 10 excpetion");
            t.printStackTrace();
            return;
        }

        String[] eSign = new String[1];
        String[] userId = new String[1];
        String[] nextPage = new String[1];

        eSign = (String[]) table.get("eSign");
        System.out.println("StudySiteApndxin do post 11 esign" + eSign);
        userId = (String[]) table.get("userId");
        System.out.println("StudySiteApndxin do post 12 userId" + userId);
        nextPage = (String[]) table.get("nextPage");
        System.out.println("StudySiteApndxin do post 13 nextPage" + nextPage);

        UserJB userB = new UserJB();
        System.out.println("StudySiteApndxin do post 14");
        userB.setUserId(EJBUtil.stringToNum(userId[0]));
        System.out.println("StudySiteApndxin do post 15");
        userB.getUserDetails();
        System.out.println("StudySiteApndxin do post 16");
        String actualESign = userB.getUserESign();
        System.out.println("StudySiteApndxin do post 17");
        out = res.getOutputStream();
        System.out.println("StudySiteApndxin do post 18");

        if (eSign[0].equals(actualESign)) {

            System.out.println("StudySiteApndxin do post 19");
            StudySiteApndxJB studySiteApndxJB = new StudySiteApndxJB();
            System.out.println("StudySiteApndxin do post 20");
            //KM-#3634
            studySiteApndxJB.setCreator(userId[0]);
            int studySiteApndxId = studySiteApndxJB.setStudySiteApndxDetails();
            System.out.println("StudySiteApndxin do post 21");

            pkBase[0] = String.valueOf(studySiteApndxId);
            System.out.println("StudySiteApndxin do post 22");

            super.doPost(req, res);
            System.out.println("StudySiteApndxin do post 23");

            if (deleteFlag == false) {
                System.out.println("StudySiteApndxin do post 24");
                String[] studySiteId = (String[]) table.get("studySiteId");

                String[] desc = (String[]) table.get("desc");

                System.out.println("StudySiteApndxin do post 25  desc" + desc);
                String[] type = (String[]) table.get("type");
                System.out.println("StudySiteApndxin do post 26 type" + type);

                studySiteApndxJB.setStudySiteApndxId(studySiteApndxId);
                System.out
                        .println("StudySiteApndxin do post 27 studySiteApndxId"
                                + studySiteApndxId);

                studySiteApndxJB.getStudySiteApndxDetails();
                System.out.println("StudySiteApndxin do post 28");

                studySiteApndxJB.setStudySiteId(studySiteId[0]);
                System.out.println("StudySiteApndxin do post studySiteId[0]"
                        + studySiteId[0]);

                studySiteApndxJB.setAppendixDescription(desc[0]);
                System.out
                        .println("StudySiteApndxin do post 29 desc" + desc[0]);

                studySiteApndxJB.setAppendixType(type[0]);
                System.out
                        .println("StudySiteApndxin do post 30 type" + type[0]);

                studySiteApndxJB.setAppendixName(file_name);
                System.out.println("StudySiteApndxin do post 31 file_name"
                        + file_name);
                // studyApndxJB.setAppendixSavedFileName(ser_file_name);
                studySiteApndxJB.setFileSize((new Long(uploadFileSize))
                        .toString());
                System.out.println("StudySiteApndxin do post 32");
                studySiteApndxJB.setModifiedBy(userId[0]);
                studySiteApndxJB.updateStudySiteApndx();
                System.out.println("StudySiteApndxin do post 33");
            } else {
                System.out.println("StudySiteApndxin do post 34");
                studySiteApndxJB.setStudySiteApndxId(studySiteApndxId);
                System.out.println("StudySiteApndxin do post 35");
                studySiteApndxJB.removeStudySiteApndx();
                System.out.println("StudySiteApndxin do post 36");
            }
            System.out.println("StudySiteApndxin do post 37");
            res.sendRedirect("../../" + path);
        } else {
            System.out.println("StudySiteApndxin do post 38");
            res.sendRedirect("../../velos/jsp/incorrectesign.jsp");
            System.out.println("StudySiteApndxin do post 39");
        }

    }

    /**
     *
     *
     * Obtain information on this servlet.
     *
     *
     * @return String describing this servlet.
     *
     *
     */

    public String getServletInfo() {

        return "File upload servlet -- used to receive files";

    }

}
