dir = Dir.open( "eres" )
content = "whenever oserror exit -1\nwhenever sqlerror exit -1\n"
dir.each { |e|  content += "@eres/"+e+"\n" }
content = content.gsub "@eres/..\n", ""
content = content.gsub "@eres/.\n", ""
content += "spool done_eres.txt\n"
content += "quit"
puts content
