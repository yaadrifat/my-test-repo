dir = Dir.open( "esch" )
content = "whenever oserror exit -1\nwhenever sqlerror exit -1\n"
dir.each { |e|  content += "@esch/"+e+"\n" }
content = content.gsub "@esch/..\n", ""
content = content.gsub "@esch/.\n", ""
content += "spool done_esch.txt\n"
content += "quit"
puts content
