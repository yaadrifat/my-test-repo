<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.business.common.CommonDAO" %>
<%@page import="java.sql.Connection,java.sql.PreparedStatement" %>
<%@page import="java.net.URLEncoder,java.net.URLDecoder" %>
<%@page import="java.util.ArrayList" %>

<%
	String input = null;
	Connection conn = null;
	PreparedStatement pstmt = null;
	ArrayList<String> resultList = new ArrayList<String>();
	try {
		conn = CommonDAO.getConnection();
		StringBuffer sqlSB = new StringBuffer();
		sqlSB.append(" select T1 from T "); 
		pstmt = conn.prepareStatement(sqlSB.toString());
		ResultSet rs = pstmt.executeQuery();
		while(rs.next()) {
			resultList.add(rs.getString("T1"));
		}
	} catch(Exception e) {
		System.out.println("Error occurred: "+e);
	} finally {
		if (pstmt != null) {
			try { pstmt.close(); } finally {}
		}
		if (conn != null) {
			try { conn.close(); } finally {}
		}
	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
Text saved in DB:
<table>
<%  for(String t1: resultList) {%>
<tr><td>
<%      out.println(t1);     %>
</td></tr>
<%  }  %>
</table>
</body>
</html>