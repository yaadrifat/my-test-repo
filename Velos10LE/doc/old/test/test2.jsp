<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.business.common.CommonDAO" %>
<%@page import="java.sql.Connection,java.sql.PreparedStatement" %>
<%@page import="java.net.URLEncoder,java.net.URLDecoder" %>

<%
String input = request.getParameter("freetext");
if (input != null && input.length() > 0) {
	Connection conn = null;
	PreparedStatement pstmt = null;
	try {
		conn = CommonDAO.getConnection();
		StringBuffer sqlSB = new StringBuffer();
		sqlSB.append(" insert into T(T1) values "); 
		sqlSB.append(" ('").append(input).append("') " );
		pstmt = conn.prepareStatement(sqlSB.toString());
		pstmt.execute();
	} catch(Exception e) {
		System.out.println("Error occurred: "+e);
	} finally {
		try { if (pstmt != null) { pstmt.close(); } } finally {}
		try { if (conn != null) { conn.close(); } } finally {}
	}
	response.sendRedirect(response.encodeRedirectURL("test3.jsp"));
}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<form action="test2.jsp" accept-charset="UTF-8" method="POST">
Please enter text here:
<input type="text" id="freetext" name="freetext" size="30"/>
<input type="submit" value="Go"/>
</form>
</body>
</html>