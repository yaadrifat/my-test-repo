Add impact analysis documents for bug fixes in this folder. (For enhancements, use the folder one level above.)

Use the naming convention such as this:
Impact_Analysis_Checklist_Bug5555.xls

