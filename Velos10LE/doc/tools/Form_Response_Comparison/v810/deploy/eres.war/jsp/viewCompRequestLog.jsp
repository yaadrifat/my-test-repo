<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>



 <html>
<head>
<title>View Log</title>

<%@ page import="com.velos.eres.service.util.*,java.util.*,java.io.*, com.velos.eres.business.common.*, java.text.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<head>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</head>

 

<%@ page language = "java" import = "java.util.*,com.velos.eres.business.common.*"%>

<body>
<br>
<form name="logForm" >
<DIV style="overflow:auto;" >
 
 <p>View All Comparison requests: </p>
 
<TABLE border = "1">
<%

	FormCompareLogDao fd = new FormCompareLogDao();
	
	fd.getCompareLog();
	
	ArrayList arCols = new ArrayList();
	
	Hashtable htRows = new Hashtable();
		
	arCols = fd.getColDispNameAr();
	htRows = fd.getHtRows();
	
	String rowStyle = "";
	
	for (int k = 0; k<arCols.size(); k++)
	{
		%>
		
			<TH> <%= (String) arCols.get(k) %></TH>
		<% 		
		
	}
	
	for (int htCtr = 1; htCtr<=htRows.size() ; htCtr++)
	{
		ArrayList arValues = new ArrayList ();
		
		if (htRows.containsKey(Integer.valueOf(htCtr)))
				{
					arValues = (ArrayList) htRows.get(Integer.valueOf(htCtr));
					
					 
					
					if (htCtr==1)
					{
						 
						rowStyle = "style=\"background=yellow;\"";
						
					}
					else
					{
						rowStyle = "";
						
					}
		
				%>
				
				<tr <%=rowStyle %>>
						<%
						
						for (int colCtr = 0; colCtr<arValues.size(); colCtr++)
						{
							if (colCtr == 0)
							{
								%>
								<td> <A href="viewCompLogDetails.jsp?logPk=<%= (String) arValues.get(colCtr) %>" ><%= (String) arValues.get(colCtr) %></A></td>
								<% 
							}
							else
							{
							%>
							
								<td> <%= StringUtil.isEmpty((String) arValues.get(colCtr)) ? "-" : (String) arValues.get(colCtr) %> </td>
							<%
							}
							
						}
						
						
						%>  
					 
				</tr>
				
				<% 
		
			} //containsKey
	}
	
	
%>
	 		     
</TABLE>
</div>

<span ID="span_det" style="overflow:auto;width:100%;height:70%;"></span>

</form>

</body>

</html>

