<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title>Compare</title>

<%@ page import="com.velos.eres.service.util.*,java.util.*,java.io.*, com.velos.eres.business.common.*,com.velos.impex.*,java.text.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<head>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</head>

 

<%@ page language = "java" import = "java.util.*,com.velos.eres.business.common.*"%>

<%

	String mode = request.getParameter("mode");

	if (StringUtil.isEmpty(mode ))
	{
		mode = "I";	
	}

	
%>

<body>
<br>
<DIV>
			
<%  if ( mode.equals("I"))
	{
		%>
		
	
		 <p>Please enter Primary Key for a Patient form (any type of linking)</p>
		 
		<form action="compareFormResponseUtil.jsp" method="post" >
		<table>
		<tr>
			<TD> Enter Form PK:	</TD>
			<TD> <input name="formpk" type="text">
			 <input name="mode" type="hidden" value="F">
			</TD>
		 	
		</tr>
	<tr>
		<td colspan="2"> <BR>Compare form responses added or modified in last 'X' number of Days. Enter a number if you do not want to compare 'All' responses</TD>
	</tr>
		
		<tr>
			 
			<TD> <input name="dateFrom" type="text" value="All">
	 			</TD>
		<td> <Input type="submit"  value="Submit"></Input> </td>	
		</tr>
		
		</table>
		
		
		
		</form>

	 	<% 
			
		
	}
else if  ( mode.equals("F"))
{
	
	String formpk = request.getParameter("formpk");
	String dateFrom = request.getParameter("dateFrom");
	
	Hashtable htParam = new Hashtable();
	
		
	CompareFormResponses cfr = new CompareFormResponses();
	cfr.setFormId(EJBUtil.stringToNum(formpk));
	cfr.setFormType("P");
	
	
	if (! StringUtil.isEmpty(dateFrom) && (!dateFrom.equalsIgnoreCase("All")) &&  (EJBUtil.stringToNum(dateFrom) >= 0))
	{
		
		htParam.put("dateFrom",String.valueOf(EJBUtil.stringToNum(dateFrom)) );
		cfr.setHtParam(htParam);
	}
	
	
	cfr.start();

  
%>
	<BR><BR><BR><BR>
			<P align = "center" class="defComments">
			
			We have recorded your Form Response Comparison request.
			 
			<BR> Please wait while we take you to the Log page
		
			</P>
		  <META HTTP-EQUIV=Refresh CONTENT="1; URL=viewCompRequestLog.jsp"> 

<% }

%> 		
</div>
</body>

</html>

