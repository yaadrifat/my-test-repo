CREATE OR REPLACE PROCEDURE  Delete_Audit_Trigger (
p_TableName varchar2, -- Optional
p_TiggerType varchar2  -- Mandatory AD0 or AD
)
as
  trigger_text  VARCHAR2(32767); -- trigger text
  trigger_Body  VARCHAR2(32767); -- trigger body
  body_Quotes  VARCHAR2(2);
  v_Condition  VARCHAR2(500);
  trigger_Count number;
  column_Count number;
  L_HANDLER UTL_FILE.FILE_TYPE;
 TYPE tableNames IS TABLE OF varchar2(200) INDEX BY varchar2(30);
  tableVars tableNames;

  FUNCTION generate_body (tableName VARCHAR2) RETURN VARCHAR2 AS
  
     TYPE colnames IS TABLE OF varchar2(200) INDEX BY varchar2(30);
     colnameVars colnames;
  
  BEGIN
column_Count:=0;
  body_Quotes:='''';
  trigger_Body :='CREATE  OR REPLACE TRIGGER '||tableName||'_'||upper(p_TiggerType)||' AFTER DELETE ON '||tableName||'        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := ''New User'' ;
 oldUsr := ''New User'' ;
 
 ';
 SELECT count(*) into column_Count  FROM user_tab_cols WHERE table_name=UPPER(tableName) and column_name =Upper('LAST_MODIFIED_BY') order by column_name;
 if (column_Count=1) then
    trigger_Body:=trigger_Body||'
    BEGIN
     SELECT TO_CHAR(pk_user) ||'', '' || usr_lastname ||'', '' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := ''New User'' ;
      END;';
  
 end if;
   SELECT count(*) into column_Count  FROM user_tab_cols WHERE table_name=UPPER(tableName) and column_name =Upper('CREATOR') order by column_name;
 if (column_Count=1) then
    trigger_Body:=trigger_Body||'
    BEGIN
     SELECT TO_CHAR(pk_user) ||'', '' || usr_lastname ||'', '' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := ''New User'' ;
      END;';
  
 end if;
  trigger_Body:=trigger_Body||' 
  Audit_Trail.record_transaction (raid, '||body_Quotes||tableName||body_Quotes||', :OLD.rid, ''D'', usr);';
 
 For t IN(SELECT column_name FROM user_tab_cols WHERE table_name=UPPER(tableName) order by column_name) LOOP
    colnameVars(t.column_name) := t.column_name;

if colnameVars(t.column_name) like 'FK_CODELST%' then

trigger_Body:=trigger_Body||' 
BEGIN
SELECT to_char(PK_CODELST)||'', ''||CODELST_DESC into codeList_desc from SCH_CODELST where PK_CODELST=:OLD.'||colnameVars(t.column_name)||';
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '' 0, '';
END;
       Audit_Trail.column_delete (raid, '||body_Quotes||colnameVars(t.column_name)||body_Quotes||', codeList_desc);';
elsif colnameVars(t.column_name) = 'LAST_MODIFIED_BY' then
   trigger_Body:=trigger_Body||'
       Audit_Trail.column_delete (raid, '||body_Quotes||colnameVars(t.column_name)||body_Quotes||', usr );'; 
elsif colnameVars(t.column_name) = 'CREATOR' then
   trigger_Body:=trigger_Body||'
       Audit_Trail.column_delete (raid, '||body_Quotes||colnameVars(t.column_name)||body_Quotes||', oldUsr );';
elsif colnameVars(t.column_name) = 'LAST_MODIFIED_DATE' or colnameVars(t.column_name) = 'CREATED_ON' then
  trigger_Body:=trigger_Body||'
       Audit_Trail.column_delete (raid, '||body_Quotes||colnameVars(t.column_name)||body_Quotes||',TO_CHAR(:OLD.'||colnameVars(t.column_name)||',PKG_DATEUTIL.f_get_dateformat));';
else
   trigger_Body:=trigger_Body||'
       Audit_Trail.column_delete (raid, '||body_Quotes||colnameVars(t.column_name)||body_Quotes||', :OLD.'||colnameVars(t.column_name)||');';
end if;

  END LOOP;
  trigger_Body:=trigger_Body||'
COMMIT;
END;
/';   
    RETURN trigger_Body;
  END generate_body;
 
BEGIN

if(LENGTH(p_TiggerType)>0) then

if(upper(p_TiggerType) = 'AD' or upper(p_TiggerType) = 'AD0') then

v_Condition := 'SCH_%';
if (LENGTH(p_TableName)>0) then
v_Condition := upper(p_TableName);
end if;
 
 FOR r IN (SELECT table_name,tablespace_name FROM user_tables where TABLE_NAME like v_Condition order by table_name) LOOP
    tableVars(r.table_name) := r.table_name;
    
 --    select count(*) into trigger_Count from USER_TRIGGERS where table_name=upper(tableVars(r.table_name)) and trigger_name = tableVars(r.table_name)||'_'||upper(p_TiggerType); 
 -- if(trigger_Count>0)      then
            trigger_text := generate_body(tableVars(r.table_name));  
            L_HANDLER := UTL_FILE.FOPEN('ESCH', tableVars(r.table_name)||'_'||upper(p_TiggerType)||'.sql', 'W');
            UTL_FILE.PUTF(L_HANDLER, trigger_text);
 --   end if;


  UTL_FILE.FCLOSE(L_HANDLER);
 -- EXECUTE IMMEDIATE trigger_text;  
  END LOOP;
     else
     dbms_output.put_line('Trigger type parameter (second) is not correct, required type e.g ''AD '' OR '' AD0''');
  end if;
  else
     dbms_output.put_line('Please give all mandatory parameters');
  end if;
  
END;
/

