#!/bin/sh
mvn install:install-file -Dfile=spring-ws-2.1.4.RELEASE.jar -DgroupId=org.springframework.ws -DartifactId=spring-ws -Dversion=2.1.4.RELEASE -Dpackaging=jar
mvn install:install-file -Dfile=jboss-messaging-client-5.1.0.jar -DgroupId=jboss -DartifactId=jboss-messaging-client -Dversion=5.1.0 -Dpackaging=jar
mvn install:install-file -Dfile=jboss-javaee-5.1.0.jar -DgroupId=jboss -DartifactId=jboss-javaee -Dversion=5.1.0 -Dpackaging=jar
mvn install:install-file -Dfile=jboss-logging-log4j-5.1.0.jar -DgroupId=jboss -DartifactId=jboss-logging-log4j -Dversion=5.1.0 -Dpackaging=jar
mvn install:install-file -Dfile=jboss-logging-spi-5.1.0.jar -DgroupId=jboss -DartifactId=jboss-logging-spi -Dversion=5.1.0 -Dpackaging=jar
mvn install:install-file -Dfile=jnp-client-5.1.0.jar -DgroupId=jboss -DartifactId=jnp-client -Dversion=5.1.0 -Dpackaging=jar
mvn install:install-file -Dfile=jboss-aop-client-5.1.0.jar -DgroupId=jboss -DartifactId=jboss-aop-client -Dversion=5.1.0 -Dpackaging=jar
mvn install:install-file -Dfile=jboss-remoting-5.1.0.jar -DgroupId=jboss -DartifactId=jboss-remoting -Dversion=5.1.0 -Dpackaging=jar
mvn install:install-file -Dfile=jboss-common-core-5.1.0.jar -DgroupId=jboss -DartifactId=jboss-common-core -Dversion=5.1.0 -Dpackaging=jar
mvn install:install-file -Dfile=trove-1.0.2.jar -DgroupId=gnu -DartifactId=trove -Dversion=1.0.2 -Dpackaging=jar
mvn install:install-file -Dfile=javassist-3.9.0.GA.jar -DgroupId=javassist -DartifactId=javassist -Dversion=3.9.0.GA -Dpackaging=jar
mvn install:install-file -Dfile=jboss-mdr-5.1.0.jar -DgroupId=jboss -DartifactId=jboss-mdr -Dversion=5.1.0 -Dpackaging=jar
mvn install:install-file -Dfile=concurrent-1.0.0.jar -DgroupId=edu.oswego -DartifactId=concurrent -Dversion=1.0.0 -Dpackaging=jar
mvn install:install-file -Dfile=jboss-serialization-5.1.0.jar -DgroupId=jboss -DartifactId=jboss-serialization -Dversion=5.1.0 -Dpackaging=jar
mvn install:install-file -Dfile=tomcat7-servlet-api.jar -DgroupId=javax.servlet -DartifactId=tomcat7-servlet-api -Dversion=3.0.0 -Dpackaging=jar

echo Hit return to exit
read a
