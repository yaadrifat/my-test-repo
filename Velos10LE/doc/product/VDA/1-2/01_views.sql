create or replace view vda_v_eres_deletelog
as
select  PK_APP_DL,
  TABLE_NAME,
  TABLE_PK  ,
  TABLE_RID ,
  DELETED_BY,
  DELETED_ON,
  APP_MODULE,
  IP_ADDRESS,
  REASON_FOR_DELETION  
 from  eres.audit_deletelog
/
 
COMMENT ON TABLE vda_v_eres_deletelog IS 'This view provides data on all delete transactions from Velos eResearch eres database schema'
/

COMMENT ON COLUMN vda_v_eres_deletelog.PK_APP_DL IS 'Primary key'
/

COMMENT ON COLUMN vda_v_eres_deletelog.TABLE_NAME IS 'Name of the database table'
/

COMMENT ON COLUMN vda_v_eres_deletelog.TABLE_PK IS 'The DELETED ROW''s Primary Key'
/

COMMENT ON COLUMN vda_v_eres_deletelog.TABLE_RID IS 'The DELETED ROW''s RID'
/

COMMENT ON COLUMN vda_v_eres_deletelog.DELETED_BY IS 'The PK of the er_user, user who deleted the row'
/

COMMENT ON COLUMN vda_v_eres_deletelog.DELETED_ON IS 'Stores the date of deletion'
/

COMMENT ON COLUMN vda_v_eres_deletelog.APP_MODULE IS 'Stores the Module name'
/

COMMENT ON COLUMN vda_v_eres_deletelog.IP_ADDRESS IS 'Stores the IP address of user'
/

COMMENT ON COLUMN vda_v_eres_deletelog.REASON_FOR_DELETION IS 'Reason why the row was deleted'
/


create or replace view vda_v_esch_deletelog
as
select  PK_APP_DL,
  TABLE_NAME,
  TABLE_PK  ,
  TABLE_RID ,
  DELETED_BY,
  DELETED_ON,
  APP_MODULE,
  IP_ADDRESS,
  REASON_FOR_DELETION  
 from  esch.audit_deletelog
/
 
COMMENT ON TABLE vda_v_esch_deletelog IS 'This view provides data on all delete transactions from Velos eResearch esch database schema'
/

COMMENT ON COLUMN vda_v_esch_deletelog.PK_APP_DL IS 'Primary key'
/

COMMENT ON COLUMN vda_v_esch_deletelog.TABLE_NAME IS 'Name of the database table'
/

COMMENT ON COLUMN vda_v_esch_deletelog.TABLE_PK IS 'The DELETED ROW''s Primary Key'
/

COMMENT ON COLUMN vda_v_esch_deletelog.TABLE_RID IS 'The DELETED ROW''s RID'
/

COMMENT ON COLUMN vda_v_esch_deletelog.DELETED_BY IS 'The PK of the er_user, user who deleted the row'
/

COMMENT ON COLUMN vda_v_esch_deletelog.DELETED_ON IS 'Stores the date of deletion'
/

COMMENT ON COLUMN vda_v_esch_deletelog.APP_MODULE IS 'Stores the Module name'
/

COMMENT ON COLUMN vda_v_esch_deletelog.IP_ADDRESS IS 'Stores the IP address of user'
/

COMMENT ON COLUMN vda_v_esch_deletelog.REASON_FOR_DELETION IS 'Reason why the row was deleted'
/

create or replace view vda_v_epat_deletelog
as
select  PK_APP_DL,
  TABLE_NAME,
  TABLE_PK  ,
  TABLE_RID ,
  DELETED_BY,
  DELETED_ON,
  APP_MODULE,
  IP_ADDRESS,
  REASON_FOR_DELETION  
from  epat.audit_deletelog
/
 
COMMENT ON TABLE vda_v_epat_deletelog IS 'This view provides data on all delete transactions from Velos eResearch epat database schema'
/

COMMENT ON COLUMN vda_v_epat_deletelog.PK_APP_DL IS 'Primary key'
/

COMMENT ON COLUMN vda_v_epat_deletelog.TABLE_NAME IS 'Name of the database table'
/

COMMENT ON COLUMN vda_v_epat_deletelog.TABLE_PK IS 'The DELETED ROW''s Primary Key'
/

COMMENT ON COLUMN vda_v_epat_deletelog.TABLE_RID IS 'The DELETED ROW''s RID'
/

COMMENT ON COLUMN vda_v_epat_deletelog.DELETED_BY IS 'The PK of the er_user, user who deleted the row'
/

COMMENT ON COLUMN vda_v_epat_deletelog.DELETED_ON IS 'Stores the date of deletion'
/

COMMENT ON COLUMN vda_v_epat_deletelog.APP_MODULE IS 'Stores the Module name'
/

COMMENT ON COLUMN vda_v_epat_deletelog.IP_ADDRESS IS 'Stores the IP address of user'
/

COMMENT ON COLUMN vda_v_epat_deletelog.REASON_FOR_DELETION IS 'Reason why the row was deleted'
/

create or replace view vda_v_groups
as
select 
PK_GRP,
  GRP_NAME,
  GRP_DESC,
  RID,
  (SELECT   usr_firstname || ' ' || usr_lastname
   FROM   eres.ER_USER
  WHERE   pk_user = er_grps.CREATOR)
   creator,
(SELECT   usr_firstname || ' ' || usr_lastname
   FROM   eres.ER_USER
  WHERE   pk_user = er_grps.LAST_MODIFIED_BY)
   LAST_MODIFIED_BY,
  LAST_MODIFIED_DATE,
  CREATED_ON ,
  FK_ACCOUNT ,
  IP_ADD,
  decode(GRP_SUPUSR_FLAG,0,'No',1,'Yes') IS_SUPERUSER_GRP  ,
  decode(GRP_SUPBUD_FLAG,0,'No',1,'Yes') IS_BUD_SUPERUSER_GRP       ,
  decode(GRP_HIDDEN,0,'No',1,'Yes') IS_GRP_HIDDEN  ,
  (select codelst_desc from eres.er_codelst where pk_codelst = FK_CODELST_ST_ROLE) GRP_STUDYTEAM_ROLE 
 from  eres.er_grps
/ 

COMMENT ON TABLE vda_v_groups IS 'This view provides access to all groups'
/

COMMENT ON COLUMN vda_v_groups.PK_GRP IS 'The Primary Key to uniquely identify the groups created in the database.'
/

COMMENT ON COLUMN vda_v_groups.GRP_NAME IS 'The group name'
/

COMMENT ON COLUMN vda_v_groups.GRP_DESC IS 'the group description'
/

COMMENT ON COLUMN vda_v_groups.RID IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.'
/

COMMENT ON COLUMN vda_v_groups.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN vda_v_groups.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN vda_v_groups.LAST_MODIFIED_DATE IS 'The date the  record was last modified on (Audit)'
/

COMMENT ON COLUMN vda_v_groups.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN vda_v_groups.FK_ACCOUNT IS 'The account the group is linked with'
/


COMMENT ON COLUMN vda_v_groups.IP_ADD IS 'The IP ADDRESS of the client machine (Audit)'
/


COMMENT ON COLUMN vda_v_groups.IS_SUPERUSER_GRP IS 'Indicates if the group is a super user group'
/

COMMENT ON COLUMN vda_v_groups.IS_BUD_SUPERUSER_GRP IS 'Indicates if the group is a super user group for budgets access'
/

COMMENT ON COLUMN vda_v_groups.IS_GRP_HIDDEN IS 'Indicates if this Group is hidden in Group and User selection Lookups'
/

COMMENT ON COLUMN vda_v_groups.GRP_STUDYTEAM_ROLE IS 'This is the Foreign Key to Study Team ROLE code in er_codelst'
/

create or replace view vda_v_group_users
as
Select
PK_USRGRP,
  FK_USER ,
  FK_GRP  ,
  g.RID                 ,
  g.CREATOR             ,
  g.LAST_MODIFIED_BY    ,
  g.LAST_MODIFIED_DATE  ,
  g.CREATED_ON          ,
  g.IP_ADD,
  usr_lastname,
  usr_firstname,
(select grp_name from eres.er_grps where pk_grp = FK_GRP)  group_name,
u.fk_account
from eres.ER_USRGRP g,eres.er_user u
where pk_user = FK_USER
/

COMMENT ON TABLE vda_v_group_users IS 'This provides access the group-user relationship: all users in a group'
/

COMMENT ON COLUMN vda_v_group_users.PK_USRGRP IS 'The Primary key'
/

COMMENT ON COLUMN vda_v_group_users.FK_USER IS 'The PK of er_user'
/

COMMENT ON COLUMN vda_v_group_users.FK_GRP IS 'The PK of er_grps'
/

COMMENT ON COLUMN vda_v_group_users.RID IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.'
/

COMMENT ON COLUMN vda_v_group_users.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN vda_v_group_users.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN vda_v_group_users.LAST_MODIFIED_DATE IS 'The date the  record was last modified on (Audit)'
/

COMMENT ON COLUMN vda_v_group_users.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN vda_v_group_users.IP_ADD IS 'The IP ADDRESS of the client machine (Audit)'
/  
 
COMMENT ON COLUMN vda_v_group_users.usr_lastname IS 'The last name of the user'
/  

COMMENT ON COLUMN vda_v_group_users.usr_firstname IS 'The first name of the user'
/  
COMMENT ON COLUMN vda_v_group_users.FK_ACCOUNT IS 'The account the study site is linked with'
/
 
 
create or replace view vda_v_er_codelst 
as
select PK_CODELST  ,
  CODELST_TYPE,
  CODELST_SUBTYP,
  CODELST_DESC ,
  CODELST_HIDE ,
  CODELST_SEQ  ,
  CODELST_CUSTOM_COL  ,
  CODELST_CUSTOM_COL1 ,
  CODELST_STUDY_ROLE  
 from eres.er_codelst
/
 
COMMENT ON TABLE vda_v_er_codelst IS 'This view provides access to all the codes (eres schema) used in the application. Mostly, these codes are available in the form of ''Dropdown'' data.'
/

COMMENT ON COLUMN vda_v_er_codelst.PK_CODELST IS 'This uniquely identifies the code. Primary key of the table'
/

COMMENT ON COLUMN vda_v_er_codelst.CODELST_TYPE IS 'The code type to categorize the codes'
/

COMMENT ON COLUMN vda_v_er_codelst.CODELST_SUBTYP IS 'The second level categorization of the code. This is in addition to the code type column and is sometimes used in the application for specific purposes'
/

COMMENT ON COLUMN vda_v_er_codelst.CODELST_DESC IS 'The Description of the code. This value is shown in the application instead of the code'
/

COMMENT ON COLUMN vda_v_er_codelst.CODELST_HIDE IS 'A flag (Y/N) to indicate if the value has to be displayed or not. Default N'
/

COMMENT ON COLUMN vda_v_er_codelst.CODELST_SEQ IS 'This number indicates the sequence in which the values for a particular code type/sub-type will be displayed in the application'
/

COMMENT ON COLUMN vda_v_er_codelst.CODELST_CUSTOM_COL IS 'This column can be used to establish relationships between codes or some specific attributes.'
/

COMMENT ON COLUMN vda_v_er_codelst.CODELST_CUSTOM_COL1 IS 'This column can be used to establish relationships between codes or some specific attributes. In code_type=''studyidtype'', this stores the dropdown html structure'
/

COMMENT ON COLUMN vda_v_er_codelst.CODELST_STUDY_ROLE IS 'This is used to store the Study Team Roles that may have access to the code'
/
 
create or replace view vda_v_studysites
as
 select
 si.PK_STUDYSITES ,
  si.FK_STUDY,
  si.FK_SITE ,
  (select codelst_desc from eres.er_codelst where pk_codelst = si.FK_CODELST_STUDYSITETYPE) STUDYSITE_TYPE  ,
  si.STUDYSITE_LSAMPLESIZE   ,
  decode(si.STUDYSITE_PUBLIC,'0','No',1,'Yes') is_studysite_public        ,
  decode(si.STUDYSITE_REPINCLUDE,'0','No',1,'Yes')  include_in_reports,
  si.RID                     ,
  (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = si.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = si.LAST_MODIFIED_BY) LAST_MODIFIED_BY ,
  si.CREATED_ON              ,
  si.LAST_MODIFIED_ON        ,
  si.IP_ADD                  ,
  si.STUDYSITE_ENRCOUNT      ,
  decode(si.IS_REVIEWBASED_ENR,'0','No',1,'Yes') IS_REVIEWBASED_ENR       ,
  eres.GETUSER(si.ENR_NOTIFYTO) send_enrollment_notification,
  eres.GETUSER(si.ENR_STAT_NOTIFYTO) send_enrollment_approval ,
  st.study_number,(select site_name from eres.er_site where pk_site = si.fk_site) site_name,
  st.fk_account
 from eres.er_studysites si, eres.er_study st
 where st.pk_study = si.fk_study
/

COMMENT ON TABLE vda_v_studysites IS 'This view provides information on the sites or organizations associated with a study'
/

COMMENT ON COLUMN vda_v_studysites.PK_STUDYSITES IS 'Primary key'
/

COMMENT ON COLUMN vda_v_studysites.FK_STUDY IS 'This column identifies the study to which this study site is associated'
/

COMMENT ON COLUMN vda_v_studysites.FK_SITE IS 'This column identifies the site'
/

COMMENT ON COLUMN vda_v_studysites.STUDYSITE_TYPE IS 'This column identifies the study site type'
/

COMMENT ON COLUMN vda_v_studysites.STUDYSITE_LSAMPLESIZE IS 'The local sample size for this study site'
/

COMMENT ON COLUMN vda_v_studysites.is_studysite_public IS 'Flag for  information to be available to the public'
/

COMMENT ON COLUMN vda_v_studysites.include_in_reports IS 'Flag for study site to be included in reports'
/

COMMENT ON COLUMN vda_v_studysites.RID IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.'
/

COMMENT ON COLUMN vda_v_studysites.CREATOR IS 'The user who created the record (Audit)'
/

COMMENT ON COLUMN vda_v_studysites.LAST_MODIFIED_BY IS 'The user who last modified the record(Audit)'
/

COMMENT ON COLUMN vda_v_studysites.LAST_MODIFIED_ON IS 'The date the  record was last modified on (Audit)'
/

COMMENT ON COLUMN vda_v_studysites.CREATED_ON IS 'The date the record was created on (Audit)'
/

COMMENT ON COLUMN vda_v_studysites.IP_ADD IS 'The IP ADDRESS of the client machine (Audit)'
/  

COMMENT ON COLUMN vda_v_studysites.STUDYSITE_ENRCOUNT IS 'The number of current patients enrolled (with any status) for the study-site combination'
/

COMMENT ON COLUMN vda_v_studysites.IS_REVIEWBASED_ENR IS 'Identifies if the site will have review based enrollment.'
/

COMMENT ON COLUMN vda_v_studysites.send_enrollment_notification IS 'Identifies the users who will be notified when an ''enrolled'' status is added'
/

COMMENT ON COLUMN vda_v_studysites.send_enrollment_approval IS 'Identifies the users who will be notified when a patient status with code_subtyp = "enroll_appr" or code_subtyp = "enroll_denied" status is added'
/

COMMENT ON COLUMN vda_v_studysites.STUDY_NUMBER IS 'The Study number of the associated study'
/

COMMENT ON COLUMN vda_v_studysites.SITE_NAME IS 'The name of the associated site'
/

COMMENT ON COLUMN vda_v_studysites.FK_ACCOUNT IS 'The account the study site is linked with'
/

CREATE OR REPLACE VIEW VDA_V_STUDYTEAM_MEMBERS
(
   STUDY_NUMBER,
   STUDY_TITLE,
   USER_SITE_NAME,
   USER_NAME,
   USER_PHONE,
   USER_EMAIL,
   USER_ADDRESS,
   ROLE,
   FK_STUDY,
   CREATED_ON,
   FK_ACCOUNT,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   RID,
   PK_STUDYTEAM,
   STUDYTEAM_STATUS
)
AS
   SELECT   study_number,
            study_title,
            (SELECT   site_name
               FROM   eres.ER_SITE
              WHERE   pk_site = fk_siteid)
               ORGANIZATION,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.ER_USER
              WHERE   pk_user = fk_user)
               user_name,
            add_phone user_phone,
            add_email user_email,
               address
            || NVL2 (address, NVL2 (add_city, ', ', ''), '')
            || add_city
            || NVL2 (add_city, NVL2 (add_state, ', ', ''), '')
            || add_state
            || NVL2 (add_state, NVL2 (add_zipcode, ' - ', ''), '')
            || add_zipcode
               AS user_address,
            eres.F_GET_CODELSTDESC (fk_codelst_tmrole) ROLE,
            fk_study,
            ER_STUDYTEAM.CREATED_ON,
            ER_USER.fk_account,
               (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = ER_STUDYTEAM.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = ER_STUDYTEAM.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            ER_STUDYTEAM.LAST_MODIFIED_DATE,
            ER_STUDYTEAM.RID,
            PK_STUDYTEAM STEAM_RESPONSE_ID,
            STUDYTEAM_STATUS
     FROM   eres.ER_STUDY,
            eres.ER_STUDYTEAM,
            eres.ER_USER,
            eres.ER_ADD
    WHERE       ER_STUDY.pk_study = fk_study
            AND pk_user = fk_user
            AND pk_add = fk_peradd
            AND study_team_usr_type = 'D'
/

create or replace view VDA_V_ACCTFORMS_AUDIT
as
Select PK_FORMAUDITCOL ,
  FK_FILLEDFORM ,
  FK_FORM ,
  form_name,
  FA_SYSTEMID ,
  FA_DATETIME ,
  FA_FLDNAME  ,
  FA_OLDVALUE ,
  FA_NEWVALUE ,
  FA_MODIFIEDBY_NAME  ,
  FA_REASON, b.fk_Account 
  from eres.er_formauditcol,eres.er_formlib b
  where FA_FORMTYPE = 'A' and b.pk_formlib = FK_FORM
/

create or replace view VDA_V_STUDYFORMS_AUDIT
as
Select PK_FORMAUDITCOL ,
  FK_FILLEDFORM ,
  FK_FORM ,
  form_name,
  FA_SYSTEMID ,
  FA_DATETIME ,
  FA_FLDNAME  ,
  FA_OLDVALUE ,
  FA_NEWVALUE ,
  FA_MODIFIEDBY_NAME  ,
  FA_REASON ,
  study_number,
  study_title, b.fk_account 
  from eres.er_formauditcol,eres.er_formlib b,eres.er_studyforms sf,eres.er_study
  where FA_FORMTYPE = 'S' and b.pk_formlib = FK_FORM and sf.pk_studyforms = FK_FILLEDFORM 
  and sf.fk_formlib = FK_FORM
  and pk_study = fk_study
/

create or replace view VDA_V_PATFORMS_AUDIT
as
Select PK_FORMAUDITCOL ,
  FK_FILLEDFORM ,
  FK_FORM ,
  form_name,
  FA_SYSTEMID ,
  FA_DATETIME ,
  FA_FLDNAME  ,
  FA_OLDVALUE ,
  FA_NEWVALUE ,
  FA_MODIFIEDBY_NAME  ,
  FA_REASON ,
  fk_per, fk_patprot,per_code as Patient_id,
  (select patprot_patstdid from eres.er_patprot where pk_patprot = fk_patprot) Patient_study_id,
  b.fk_account 
  from eres.er_formauditcol,eres.er_formlib b,eres.er_patforms pf, eres.er_per p
  where FA_FORMTYPE = 'P' and b.pk_formlib = FK_FORM and pf.pk_patforms = FK_FILLEDFORM 
  and pf.fk_formlib = FK_FORM and p.pk_per = fk_per
 /

 create or replace view vda_v_usersites
as
  SELECT   A.PK_SITE,
            A.FK_CODELST_TYPE,
            A.FK_ACCOUNT,
            A.SITE_NAME,
            A.SITE_PARENT as PARENT_SITE,
            A.SITE_STAT,
            (select SITE_NAME from eres.er_site B where B.PK_SITE = A.SITE_PARENT )AS PARENT_SITE_NAME,
            (Select C.CODELST_DESC from eres.ER_CODELST C where C.PK_CODELST =  A.FK_CODELST_TYPE)SITETYPE,
            PK_USERSITE,
            FK_USER,
            USERSITE_RIGHT,
            A.SITE_ALTID,
            usr.USR_FIRSTNAME || ' ' || usr.USR_LASTNAME as user_name
     FROM   eres.ER_SITE A,
            eres.ER_USERSITE U, eres.ER_USER usr
    WHERE    U.FK_SITE = A.PK_SITE and usr.pk_user = u.fk_user and usersite_right = 1
  /

CREATE OR REPLACE  VIEW VDA_V_PAT_TXARM
(
   PATIENT_ID,
   PATIENT_STUDY_ID,
   TREATMENT_NAME,
   DRUG_INFO,
   TREATMENT_STATUS_DATE,
   TREATMENT_END_DATE,
   TREATMENT_NOTES,
   CREATOR,
   CREATED_ON,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   FK_ACCOUNT,
   RID,
   PK_PATTXARM,
   FK_STUDY,
   FK_PER,
   STUDY_NUMBER,STUDY_TITLE
)
AS
   SELECT   PER_CODE ,
            PATPROT_PATSTDID,
            (SELECT   TX_NAME
               FROM   eres.ER_STUDYTXARM
              WHERE   PK_STUDYTXARM = FK_STUDYTXARM)
               PTARM_TREATMT_NAME,
            TX_DRUG_INFO ,
            TX_START_DATE,
            TX_END_DATE ,
            NOTES PTARM_NOTES,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = pt.CREATOR)
               CREATOR,
            pt.CREATED_ON,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = pt.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            pt.LAST_MODIFIED_DATE,
            p.FK_ACCOUNT,
            pt.RID,
            PK_PATTXARM ,
            fk_study,
            fk_per,
            study_number,
            study_title
     FROM   eres.ER_PATTXARM pt, eres.ER_PATPROT pp, eres.ER_PER p,eres.er_study s
    WHERE   PK_PATPROT = FK_PATPROT AND pk_per = fk_per and
    s.pk_study = fk_study
/

  
CREATE OR REPLACE  VIEW VDA_V_MORE_PATIENT_DETAILS
(
   FIELD_NAME,
   FIELD_VALUE,
   FK_PER,
   CREATED_ON,
   FK_ACCOUNT,
   PATIENT_ID,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   RID,
   PK_PERID,
   SITE_NAME
)
AS
   SELECT   codelst_desc AS field_name,
            perid_id AS field_value,
            fk_per,
            a.CREATED_ON,
            p.fk_account,
            PER_CODE PATIENT_ID,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.er_user
              WHERE   pk_user = A.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.er_user
              WHERE   pk_user = A.LAST_MODIFIED_BY)
               last_modified_by,
            a.LAST_MODIFIED_DATE,
            A.RID,
            PK_PERID ,
            (SELECT   site_name
               FROM   eres.ER_SITE
              WHERE   pk_site = p.fk_site) site_name
     FROM   epat.pat_perid a, eres.ER_PER p, eres.er_codelst c
    WHERE   pk_per = fk_per and c.pk_codelst = fk_codelst_idtype
/

CREATE OR REPLACE  VIEW VDA_V_PAT_SITES
(
   PATIENT_ID,
   SITE_NAME,
   PAT_FACILITY_ID,
   REG_DATE,
   PROVIDER,
   SPECIALTY_GROUP,
   FACILITY_ACCESS,
   FACILITY_DEFAULT,
   CREATED_ON,
   LAST_MODIFIED_DATE,
   CREATOR,
   LAST_MODIFIED_BY,
   FK_ACCOUNT,
   RID,
   PK_PATFACILITY,
   FK_PER 
)
AS
   SELECT   PERSON_CODE ,
            (SELECT   SITE_NAME
               FROM   eres.ER_SITE
              WHERE   PK_SITE = ER_PATFACILITY.FK_SITE)
               SITE_NAME,
            ER_PATFACILITY.PAT_FACILITYID ,
            TRUNC (PATFACILITY_REGDATE) REG_DATE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = PATFACILITY_PROVIDER)
               PROVIDER,
            eres.code_lst_names (PATFACILITY_SPLACCESS) SPECIALTY_GROUP,
            DECODE (PATFACILITY_ACCESSRIGHT, 0, 'Revoked', 'Granted')
               FACILITY_ACCESS,
            eres.F_GET_YESNO (PATFACILITY_DEFAULT) FACILITY_DEFAULT,
            ER_PATFACILITY.CREATED_ON,
            ER_PATFACILITY.LAST_MODIFIED_DATE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = ER_PATFACILITY.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER
              WHERE   PK_USER = ER_PATFACILITY.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            FK_ACCOUNT ,
            ER_PATFACILITY.RID,
            PK_PATFACILITY ,
            fk_per
       FROM      ERES.ER_PATFACILITY,
               EPAT.PERSON
Where ER_PATFACILITY.FK_PER = PERSON.PK_PERSON
/

CREATE OR REPLACE VIEW VDA_V_USERS
(
   USER_TYPE,
   USER_DEFAULT_GROUP_FK ,
   SITE_NAME,
   USER_NAME,
   USER_JOBTYPE,
   USER_ADDRESS,
   USER_CITY,
   USER_STATE,
   USER_ZIP,
   USER_COUNTRY,
   USER_PHONE,
   USER_EMAIL,
   USER_TIMEZONE,
   USER_SPECIALTY,
   USER_WRKEXP,
   USER_PHASEINV,
   USER_DEFAULTGRP,
   USER_USRNAME,
   USER_STATUS,
   FK_ACCOUNT,
   CREATED_ON,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATOR,
   RID,
   PK_USER 
)
AS
   SELECT   eres.F_Get_Usrtype (USR_TYPE) USER_TYPE,
            FK_GRP_DEFAULT USER_DEFAULT_GROUP_FK ,
            (SELECT   SITE_NAME
               FROM   eres.ER_SITE
              WHERE   PK_SITE = FK_SITEID)
               SITE_NAME,
            o.USR_FIRSTNAME || ' ' || o.USR_LASTNAME AS USER_NAME,
            eres.F_Get_Codelstdesc (FK_CODELST_JOBTYPE) USER_JOBTYPE,
            a.ADDRESS as  USER_ADDRESS,
            a.ADD_CITY as USER_CITY,
            a.ADD_STATE as  USER_STATE,
            a.ADD_ZIPCODE as USER_ZIP,
            a.ADD_COUNTRY as USER_COUNTRY,
            a.ADD_PHONE USER_PHONE,
            a.ADD_EMAIL as USER_EMAIL,
            (SELECT   TZ_DESCRIPTION
               FROM   esch.SCH_TIMEZONES
              WHERE   PK_TZ = FK_TIMEZONE)
               USER_TIMEZONE,
            eres.F_Get_Codelstdesc (FK_CODELST_SPL) USER_SPECIALTY,
            USR_WRKEXP USER_WRKEXP,
            USR_PAHSEINV USER_PHASEINV,
            (SELECT   GRP_NAME
               FROM   eres.ER_GRPS
              WHERE   PK_GRP = FK_GRP_DEFAULT)
               USER_DEFAULT_GROUP,
            USR_LOGNAME USER_USRNAME,
            DECODE (USR_STAT,
                    'A', 'Active',
                    'B', 'Blocked',
                    'D', 'Deactivated',
                    'Other')
               USER_STATUS,
             FK_ACCOUNT,
            o.CREATED_ON CREATED_ON,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER i
              WHERE   i.PK_USER = o.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            o.LAST_MODIFIED_DATE LAST_MODIFIED_DATE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   eres.ER_USER i
              WHERE   i.PK_USER = o.CREATOR)
               CREATOR,
                o.RID,
            PK_USER  
     FROM   eres.ER_USER o,eres.ER_ADD a
    WHERE   o.USR_TYPE <> 'X' and a.PK_ADD = o.FK_PERADD
/

CREATE  or REPLACE VIEW VDA_V_LIBRARY_FORMS
(
   FORM_TYPE,
   FORM_NAME,
   FORM_DESC,
   FORM_STATUS,
   FORM_SHARED_WITH,
   FK_ACCOUNT,
   PK_FORMLIB,
   RID,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   FORM_ESIGN_REQUIRED
)
AS
   SELECT   (SELECT   CATLIB_NAME
               FROM   eres.ER_CATLIB
              WHERE   PK_CATLIB = FK_CATLIB)
               FORM_TYPE,
            FORM_NAME FORM_NAME,
            FORM_DESC FORM_DESC,
            eres.F_CODELST_DESC (FORM_STATUS) FORM_STATUS,
            eres.F_GET_SHAREDWITH (FORM_SHAREDWITH) FORM_SHARED_WITH,
            FK_ACCOUNT,
            PK_FORMLIB ,
            RID,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.ER_USER
              WHERE   pk_user = ER_FORMLIB.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   eres.ER_USER
              WHERE   pk_user = ER_FORMLIB.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            LAST_MODIFIED_DATE,
            CREATED_ON,
            decode(nvl(FORM_ESIGNREQ,0),0,'No',1,'Yes') FORM_ESIGN_REQUIRED
     FROM   eres.ER_FORMLIB
    WHERE   form_linkto = 'L' and nvl(record_type,'N') <> 'D'
/


INSERT INTO VDA_TRACK_PATCHES
(
  PATCH_PK       ,
  DB_VER_MJR     ,
  DB_VER_MNR     ,
  DB_PATCH_NAME  ,
  FIRED_ON       ,
  APP_VERSION    
) VALUES(SEQ_VDA_TRACK_PATCHES.nextval,3,1,'01_views.sql',sysdate,'VDA 1.2 Build 1')
/

commit
/ 
