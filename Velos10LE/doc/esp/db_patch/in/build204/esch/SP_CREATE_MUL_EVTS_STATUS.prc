set define off;
CREATE OR REPLACE PROCEDURE ESCH.SP_CREATE_MUL_EVTS_STATUS  (
p_event_id IN NUMBER,
p_notes IN VARCHAR,
p_date IN DATE,
p_user IN NUMBER,
p_status IN NUMBER,
p_ipadd VARCHAR,
p_mode in char,
p_sosId in NUMBER,
p_coverageTypeId in NUMBER,
p_reason_for_change_CT in varchar,
p_oldeventstat in  NUMBER,
p_inotes IN VARCHAR,
p_pkstring in varchar,
p_reason_for_change VARCHAR
)
AS
v_pk_eventstat NUMBER;
v_rid NUMBER;
v_raid NUMBER;

BEGIN


SP_MARKDONE(p_event_id,p_notes,p_date,p_user,p_status,p_ipadd,p_mode,p_sosId,p_coverageTypeId,p_reason_for_change_CT);

SP_EVENTNOTIFY(p_event_id,p_status,p_user,p_ipadd,p_oldeventstat);

select PK_EVENTSTAT into v_pk_eventstat from
(select PK_EVENTSTAT from sch_eventstat where to_number(fk_event) = p_event_id order by EVENTSTAT_ENDDT desc)
where rownum <= 1;

SELECT RID into v_rid FROM esch.SCH_EVENTSTAT where PK_EVENTSTAT = v_pk_eventstat;

select raid into v_raid from 
(select raid from audit_row where rid = v_rid and user_name like '%'||p_user||'%' order by 1 desc)
where rownum <= 1;

update audit_column set remarks = p_reason_for_change where raid = v_raid and remarks is null;

insert into SCH_EVENTSTAT (creator, EVENTSTAT_DT, EVENTSTAT_ENDDT, EVENTSTAT_NOTES, EVENTSTAT, FK_EVENT, ip_add,PK_EVENTSTAT) values (p_user, p_date, NULL,p_inotes,p_status, p_pkstring, p_ipadd, SCH_EVENTSTAT_SEQ.nextval );

END SP_CREATE_MUL_EVTS_STATUS;
/
