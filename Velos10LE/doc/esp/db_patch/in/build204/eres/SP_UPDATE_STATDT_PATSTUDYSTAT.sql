set define off;

CREATE OR REPLACE PROCEDURE ERES."SP_UPDATE_STATDT_PATSTUDYSTAT" (
  p_newPk IN NUMBER,
  p_fkPer IN NUMBER,
  p_fkStudy IN NUMBER,
  p_userId IN NUMBER,
  o_ret OUT NUMBER
)
AS
  v_beginDate DATE;
  v_prev_current_stat NUMBER;
BEGIN

  BEGIN
    select PATSTUDYSTAT_DATE into v_beginDate from ERES.ER_PATSTUDYSTAT
    where PK_PATSTUDYSTAT = p_newPk;
      
    SELECT PK_PATSTUDYSTAT INTO v_prev_current_stat
    FROM ERES.ER_PATSTUDYSTAT
    WHERE FK_STUDY = p_fkStudy and  FK_PER = p_fkPer
      and PATSTUDYSTAT_ENDT is null
      and PATSTUDYSTAT_DATE <= v_beginDate
      and PK_PATSTUDYSTAT <> p_newPk;
        
    UPDATE ERES.ER_PATSTUDYSTAT SET 
      PATSTUDYSTAT_ENDT = v_beginDate,
      LAST_MODIFIED_BY = p_userId,
      LAST_MODIFIED_DATE = sysdate WHERE PK_PATSTUDYSTAT = v_prev_current_stat;
        
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        BEGIN
            SELECT PK_PATSTUDYSTAT
            INTO v_prev_current_stat
            FROM ERES.ER_PATSTUDYSTAT
            WHERE  PK_PATSTUDYSTAT =
            ( select max(i.PK_PATSTUDYSTAT) from ERES.ER_PATSTUDYSTAT  i
                where FK_STUDY = p_fkStudy and  FK_PER = p_fkPer
                 and i.PATSTUDYSTAT_DATE = (
                   select max(ii.PATSTUDYSTAT_DATE) from ERES.ER_PATSTUDYSTAT  ii
                        where FK_STUDY = p_fkStudy and  FK_PER = p_fkPer
                             and ii.PATSTUDYSTAT_DATE <=v_beginDate and ii.PK_PATSTUDYSTAT <> p_newPk)
                 and i.PK_PATSTUDYSTAT <> p_newPk );
            UPDATE ERES.ER_PATSTUDYSTAT SET PATSTUDYSTAT_ENDT = v_beginDate,
            LAST_MODIFIED_BY = p_userId,
            LAST_MODIFIED_DATE = sysdate WHERE PK_PATSTUDYSTAT = v_prev_current_stat;
            EXCEPTION
                 WHEN NO_DATA_FOUND THEN
                 o_ret:=-1;
        END;

        BEGIN
          SELECT PK_PATSTUDYSTAT
          INTO v_prev_current_stat
          FROM ERES.ER_PATSTUDYSTAT
          WHERE       PK_PATSTUDYSTAT =
             ( select max(i.PK_PATSTUDYSTAT) from ERES.ER_PATSTUDYSTAT  i
                where       FK_STUDY = p_fkStudy and  FK_PER = p_fkPer
                 and        i.PATSTUDYSTAT_DATE = (
                    select min(ii.PATSTUDYSTAT_DATE) from ERES.ER_PATSTUDYSTAT  ii
                        Where     FK_STUDY = p_fkStudy and  FK_PER = p_fkPer
                         and        ii.PATSTUDYSTAT_DATE >=v_beginDate
              and ii.PK_PATSTUDYSTAT <> p_newPk) and        i.PK_PATSTUDYSTAT <> p_newPk);
           UPDATE ERES.ER_PATSTUDYSTAT SET PATSTUDYSTAT_ENDT = v_beginDate,
            LAST_MODIFIED_BY = p_userId,
            LAST_MODIFIED_DATE = sysdate  WHERE PK_PATSTUDYSTAT = v_prev_current_stat;
           EXCEPTION
             WHEN NO_DATA_FOUND THEN
             o_ret:=-1;
        END;
      WHEN OTHERS THEN
        o_ret:=-1;
        RAISE_APPLICATION_ERROR (-20102, 'Exception occurred in SP_UPDATE_STATDT_PATSTUDYSTAT:'||SQLERRM);
  END;

  COMMIT;
      
END SP_UPDATE_STATDT_PATSTUDYSTAT;
/
