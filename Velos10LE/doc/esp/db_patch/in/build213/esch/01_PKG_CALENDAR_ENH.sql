set define off;

CREATE OR REPLACE PACKAGE ESCH."PKG_CALENDAR_ENH" AS
PROCEDURE sp_updt_unsch_evts1_enh(p_patId in number, p_patProtId in number, p_calId in number, p_user in number,
                                  p_ipAdd in varchar2,p_eventStatus IN ARRAY_STRING,  p_evstartdate IN ARRAY_STRING,
                                  p_eventNotes IN ARRAY_STRING, p_eventCoverageType IN ARRAY_STRING,
                                  p_eventSOSID IN ARRAY_STRING, p_reasonForCoverageChange IN ARRAY_STRING,
                                  p_evtids IN ARRAY_STRING,
                                  p_evt_validfrom IN ARRAY_STRING, o_scheventids OUT ARRAY_STRING);

 PROCEDURE sp_updt_admin_unsch_evts1_enh(p_calId in number, p_user in number, p_ipAdd in varchar2,
                                         p_eventStatus IN ARRAY_STRING,  p_evstartdate IN ARRAY_STRING,
                                         p_eventNotes IN ARRAY_STRING, p_eventCoverageType IN ARRAY_STRING,
                                         p_eventSOSID IN ARRAY_STRING, p_reasonForCoverageChange IN ARRAY_STRING,
                                         p_evtids IN ARRAY_STRING,p_evt_validfrom IN ARRAY_STRING, o_scheventids OUT ARRAY_STRING);

--pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_Calendar', pLEVEL  => Plog.LDEBUG);
END PKG_CALENDAR_ENH;
/

CREATE OR REPLACE PACKAGE BODY ESCH."PKG_CALENDAR_ENH" AS
PROCEDURE sp_updt_unsch_evts1_enh(p_patId in number, p_patProtId in number, p_calId in number, p_user in number,
                                  p_ipAdd in varchar2,p_eventStatus IN ARRAY_STRING, p_evstartdate IN ARRAY_STRING,
                                  p_eventNotes IN ARRAY_STRING, p_eventCoverageType IN ARRAY_STRING,
                                  p_eventSOSID IN ARRAY_STRING,
                                  p_reasonForCoverageChange IN ARRAY_STRING, p_evtids IN ARRAY_STRING, 
                                  p_evt_validfrom IN ARRAY_STRING, o_scheventids OUT ARRAY_STRING)
as
  /*
     Date : 18Dec 2012
     Author: Tarandeep Singh Bali
     Purpose: insert new records in the sch_events1 table for the event_type='U' i.e.; Unscheduled events but with configurable start date and event status
     status: undegoing change

  */
  v_fk_per number;
  v_patProt number;
  v_calassoc         CHAR (1);
  v_protocol_study   NUMBER;


  v_event_Actual_Date char(20);
  v_ptprot_st_dt char(20);
  v_seq_num number;
  v_days NUMBER;
  v_patprot_schday number;
  v_mindisp number;
  v_mainCnt number;
  v_eventId NUMBER;

  j NUMBER;

  v_seq number;
  v_error varchar2(4000);
  v_patprot_stat number;
  v_curr_disc_status number;
  v_schids ARRAY_STRING;
  v_eventStatus NUMBER;
  v_eventStartDate DATE;
  v_eventNotes varchar2(2000);
  v_eventCoverageType NUMBER;
  v_eventSOSID NUMBER;
  v_reasonForCoverageChange varchar2(2000);
  v_evt_status_valid_from varchar2(2000);

  BEGIN


       --v_event_Actual_Date := to_char(p_evStartDate,PKG_DATEUTIL.F_GET_DATEFORMAT);

        SELECT event_calassocto, chain_id
            INTO v_calassoc, v_protocol_study
             FROM EVENT_ASSOC
            WHERE event_id = p_calId AND UPPER (event_type) = 'P';


    v_mainCnt := p_evtIds.COUNT;
    v_schids := ARRAY_STRING();
    j := 1;

    WHILE j <= v_mainCnt LOOP




   --LOOP
    v_eventId := TO_NUMBER(p_evtIds(j));
    v_eventStatus := TO_NUMBER(p_eventStatus(j));

    --plog.debug(pctx, 'Event Start Date1: '|| p_evStartDate(j));
    v_eventStartDate := TO_DATE(p_evStartDate(j), 'yyyy-mm-dd');
    --plog.debug(pctx, 'Event Start Date2: '|| v_eventStartDate);
    --v_event_Actual_Date :=TO_CHAR(v_eventStartDate,'mm/dd/yyyy');
    --plog.debug(pctx, 'Event Start Date3: '|| v_event_Actual_Date);
    v_event_Actual_Date := to_char(v_eventStartDate,PKG_DATEUTIL.F_GET_DATEFORMAT);

    v_eventNotes := TO_CHAR(p_eventNotes(j));
    --plog.debug(pctx, 'Event Notes: '|| v_eventNotes);

    v_eventCoverageType := TO_NUMBER(p_eventcoveragetype(j));
    v_eventSOSID := TO_NUMBER(p_eventSOSID(j));
    v_reasonForCoverageChange := p_reasonforcoveragechange(j);
    v_evt_status_valid_from := p_evt_validfrom(j);
    --plog.debug(pctx, 'Event Status Valid From: '|| v_evt_status_valid_from);


           select fk_per, pk_patprot, to_char(PATPROT_START,PKG_DATEUTIL.F_GET_DATEFORMAT), patprot_schday, patprot_stat
                  into v_fk_per, v_patProt, v_ptprot_st_dt, v_patprot_schday, v_patprot_stat
           from er_patprot a where a.pk_patprot = p_patProtId and fk_protocol = p_calId;




      if (v_patprot_stat = 0) then
                v_curr_disc_status := 5;
      else
                v_curr_disc_status := 0;

      end if;

         if v_patprot_schday is null then

              SELECT MIN(displacement) INTO v_mindisp FROM EVENT_ASSOC WHERE chain_id=p_calId AND UPPER(event_type)='A'
              AND fk_visit is not null;
              v_days:=v_mindisp;
          else
              v_days := v_patprot_schday;
          end if;



  --begin

  select SCH_EVENTS1_SEQ1.NEXTVAL into v_seq from dual;
    DBMS_OUTPUT.PUT_LINE(j);
    v_schids.extend;
  v_schids(j) := to_char(v_seq);

    INSERT INTO SCH_EVENTS1  ( EVENT_ID, LOCATION_ID, NOTES, DESCRIPTION, BOOKED_BY,
    START_DATE_TIME, END_DATE_TIME, PATIENT_ID, OBJECT_ID, STATUS, TYPE_ID,
    SESSION_ID, VISIT_TYPE_ID, FK_ASSOC,  ISCONFIRMED, BOOKEDON, EVENT_EXEON, CREATOR , LAST_MODIFIED_BY ,
    LAST_MODIFIED_DATE ,CREATED_ON, IP_ADD, FK_PATPROT,ACTUAL_SCHDATE, VISIT,
    FK_VISIT,event_notes, SERVICE_SITE_ID, FACILITY_ID, FK_CODELST_COVERTYPE,
    fk_study, EVENT_SEQUENCE, REASON_FOR_COVERAGECHANGE )
    ((SELECT lpad (to_char(v_seq), 10, '0'), '173', v_eventNotes,
    NAME, '0000000525',
    case when (select no_interval_flag FROM SCH_PROTOCOL_VISIT
    where fk_protocol =  p_calId and pk_protocol_visit = fk_visit) = 1
    then (to_date(v_ptprot_st_dt,PKG_DATEUTIL.F_GET_DATEFORMAT) )
    else ((to_date(v_ptprot_st_dt,PKG_DATEUTIL.F_GET_DATEFORMAT)
    + nvl(PKG_RESCHEDULE.f_get_displacement(p_calId,fk_visit),0)) - 1)
    end ,
    (( to_date(v_ptprot_st_dt,PKG_DATEUTIL.F_GET_DATEFORMAT)
    + PKG_RESCHEDULE.f_get_displacement(p_calId,fk_visit)) - 1) + DURATION,
    lpad (to_char (v_fk_per), 10, '0'), '0000001476', v_curr_disc_status, '165',
    lpad (to_char (p_calId), 10, '0'), FUZZY_PERIOD, EVENT_ID,v_eventStatus,
    to_date(v_ptprot_st_dt,PKG_DATEUTIL.F_GET_DATEFORMAT) , to_date(v_evt_status_valid_from,pkg_dateutil.f_get_dateformat),
    p_user, NULL, NULL , SYSDATE ,p_ipAdd, p_patProtId,
    case when (v_event_Actual_Date is not NULL) then (to_date(v_event_Actual_Date,PKG_DATEUTIL.F_GET_DATEFORMAT))
    when (select no_interval_flag  FROM SCH_PROTOCOL_VISIT
    where fk_protocol =  p_calId and pk_protocol_visit = fk_visit ) = 1
    then null
    else ((to_date(v_ptprot_st_dt,PKG_DATEUTIL.F_GET_DATEFORMAT)
    + nvl(PKG_RESCHEDULE.f_get_displacement(p_calId,fk_visit),0)) - 1)
    end ,
    (select act_num from(select rownum act_num,a.*    from
    ((SELECT pk_protocol_visit, visit_no, displacement
    FROM sch_protocol_visit
    where fk_protocol = p_calId order by   PKG_RESCHEDULE.f_get_displacement(p_calId,pk_protocol_visit) asc, visit_no)) a)b    where pk_protocol_visit = fk_visit) visit
    , fk_visit , notes, v_eventSOSID, assoc.FACILITY_ID, v_eventCoverageType,
    0, assoc.EVENT_SEQUENCE, v_reasonForCoverageChange
    FROM EVENT_ASSOC  assoc
    WHERE CHAIN_ID = p_calId
    and event_id = v_eventId
    and EVENT_TYPE = 'U'
    and (displacement <> 0 or displacement = 0)
    and (DISPLACEMENT >=v_days or displacement = 0)
    ));

    --update the patProt
    Update er_patprot set
    last_modified_by = p_user, last_modified_date = sysdate
    where pk_patprot = p_patProtId;

    pkg_calendar.sp_add_additionaMS(v_eventId);

    commit;
    update sch_eventstat set EVENTSTAT_DT = to_date(v_evt_status_valid_from,pkg_dateutil.f_get_dateformat)
    where pk_eventstat = (select PK_EVENTSTAT from sch_eventstat where fk_event = v_seq);


          pkg_gensch.sp_addmsg_to_tran_per_event (v_eventId, v_fk_per, v_protocol_study, p_user, p_ipAdd, v_patProt);
        pkg_gensch.sp_setmailstat_per_event (v_eventId, v_patprot, p_user, p_ipAdd);



              IF (NVL (v_calassoc, 'P') = 'P')
              THEN
                 Sp_Copyprotcrf (v_patProt, p_user, p_ipAdd);
              END IF;
              COMMIT;


   j := j+1;
   end loop;

     COMMIT;
   o_scheventids := v_schids;
  end;

PROCEDURE sp_updt_admin_unsch_evts1_enh(p_calId in number, p_user in number, p_ipAdd in varchar2,
                                           p_eventStatus IN ARRAY_STRING, p_evstartdate IN ARRAY_STRING,
                                           p_eventNotes IN ARRAY_STRING, p_eventCoverageType IN ARRAY_STRING,
                                           p_eventSOSID IN ARRAY_STRING, p_reasonForCoverageChange IN ARRAY_STRING,
                                           p_evtids IN ARRAY_STRING, p_evt_validfrom IN ARRAY_STRING, o_scheventids OUT ARRAY_STRING)
as
  /*
     Date : 18Dec 2012
     Author: Kanwaldeep Kaur
     Purpose: insert new records in the sch_events1 table for the event_type='U' i.e.; Unscheduled events but with configurable start date and event status
     status: undegoing change

  */
  v_calassoc         CHAR (1);
  v_protocol_study   NUMBER;


  v_event_Actual_Date char(20);
  v_stprot_st_dt char(20);
  v_seq_num number;
  v_days NUMBER;
  v_mindisp number;
  v_mainCnt number;
  v_eventId NUMBER;

  j NUMBER;

  v_seq number;
  v_error varchar2(4000);
  v_schids ARRAY_STRING;
  v_eventStatus NUMBER;
  v_eventStartDate DATE;
  v_eventNotes varchar2(2000);
  v_eventCoverageType NUMBER;
  v_eventSOSID NUMBER;
  v_reasonForCoverageChange varchar2(2000);
  v_eventStatusPK NUMBER;
  v_evt_status_valid_from varchar2(20);

  BEGIN


        --v_event_Actual_Date := to_char(p_evStartDate,PKG_DATEUTIL.F_GET_DATEFORMAT);

        SELECT event_calassocto, chain_id, to_char(event_calschdate,PKG_DATEUTIL.F_GET_DATEFORMAT)
            INTO v_calassoc, v_protocol_study, v_stprot_st_dt
             FROM EVENT_ASSOC
            WHERE event_id = p_calId AND UPPER (event_type) = 'P';


    v_mainCnt := p_evtIds.COUNT;
    v_schids := ARRAY_STRING();
    j := 1;

    WHILE j <= v_mainCnt LOOP




   --LOOP
    v_eventId := TO_NUMBER(p_evtIds(j));
    v_eventStatus := TO_NUMBER(p_eventStatus(j));

    v_eventStartDate := TO_DATE(p_evStartDate(j), 'yyyy-mm-dd');
    --v_event_Actual_Date :=TO_CHAR(v_eventStartDate,'mm/dd/yyyy');
    v_event_Actual_Date := to_char(v_eventStartDate,PKG_DATEUTIL.F_GET_DATEFORMAT);

    v_eventNotes := TO_CHAR(p_eventNotes(j));
    v_eventCoverageType := TO_NUMBER(p_eventcoveragetype(j));
    v_eventSOSID := TO_NUMBER(p_eventSOSID(j));
    v_reasonForCoverageChange := p_reasonforcoveragechange(j);
    v_evt_status_valid_from := p_evt_validfrom(j);


   SELECT MIN(displacement) INTO v_mindisp FROM EVENT_ASSOC WHERE chain_id=p_calId AND UPPER(event_type)='A'
    AND fk_visit is not null;
    v_days:=v_mindisp;

   --begin

  select SCH_EVENTS1_SEQ1.NEXTVAL into v_seq from dual;
    DBMS_OUTPUT.PUT_LINE(j);
    v_schids.extend;
  v_schids(j) := to_char(v_seq);

    INSERT INTO SCH_EVENTS1  ( EVENT_ID, LOCATION_ID, NOTES, DESCRIPTION, BOOKED_BY,
    START_DATE_TIME, END_DATE_TIME, PATIENT_ID, OBJECT_ID, STATUS, TYPE_ID,
    SESSION_ID, VISIT_TYPE_ID, FK_ASSOC,  ISCONFIRMED, BOOKEDON, EVENT_EXEON, CREATOR , LAST_MODIFIED_BY ,
    LAST_MODIFIED_DATE ,CREATED_ON, IP_ADD, FK_PATPROT,ACTUAL_SCHDATE, VISIT,
    FK_VISIT,event_notes, SERVICE_SITE_ID, FACILITY_ID, FK_CODELST_COVERTYPE,
    fk_study, EVENT_SEQUENCE, REASON_FOR_COVERAGECHANGE )
    ((SELECT lpad (to_char(v_seq), 10, '0'), '173', v_eventNotes,
    NAME, '0000000525',
    case when (select no_interval_flag FROM SCH_PROTOCOL_VISIT
    where fk_protocol =  p_calId and pk_protocol_visit = fk_visit) = 1
    then (to_date(v_stprot_st_dt,PKG_DATEUTIL.F_GET_DATEFORMAT) )
    else ((to_date(v_stprot_st_dt,PKG_DATEUTIL.F_GET_DATEFORMAT)
    + nvl(PKG_RESCHEDULE.f_get_displacement(p_calId,fk_visit),0)) - 1)
    end ,
    (( to_date(v_stprot_st_dt,PKG_DATEUTIL.F_GET_DATEFORMAT)
    + PKG_RESCHEDULE.f_get_displacement(p_calId,fk_visit)) - 1) + DURATION,
    lpad (to_char (0), 10, '0'), '0000001476', 0, '165',
    lpad (to_char (p_calId), 10, '0'), FUZZY_PERIOD, EVENT_ID,v_eventStatus,
    to_date(v_stprot_st_dt,PKG_DATEUTIL.F_GET_DATEFORMAT) , to_date(v_evt_status_valid_from,pkg_dateutil.f_get_dateformat),
    p_user, NULL, NULL , SYSDATE ,p_ipAdd, 0,
    case when (v_event_Actual_Date is not NULL) then (to_date(v_event_Actual_Date,PKG_DATEUTIL.F_GET_DATEFORMAT))
    when (select no_interval_flag  FROM SCH_PROTOCOL_VISIT
    where fk_protocol =  p_calId and pk_protocol_visit = fk_visit ) = 1
    then null
    else ((to_date(v_stprot_st_dt,PKG_DATEUTIL.F_GET_DATEFORMAT)
    + nvl(PKG_RESCHEDULE.f_get_displacement(p_calId,fk_visit),0)) - 1)
    end ,
    (select act_num from(select rownum act_num,a.*    from
    ((SELECT pk_protocol_visit, visit_no, displacement
    FROM sch_protocol_visit
    where fk_protocol = p_calId order by   PKG_RESCHEDULE.f_get_displacement(p_calId,pk_protocol_visit) asc, visit_no)) a)b    where pk_protocol_visit = fk_visit) visit
    , fk_visit ,notes, v_eventSOSID, assoc.FACILITY_ID, v_eventCoverageType,
    v_protocol_study, assoc.EVENT_SEQUENCE, v_reasonForCoverageChange
    FROM EVENT_ASSOC  assoc
    WHERE CHAIN_ID = p_calId
    and event_id = v_eventId
    and EVENT_TYPE = 'U'
    and (displacement <> 0 or displacement = 0)
    and (DISPLACEMENT >=v_days or displacement = 0)
    ));

    pkg_calendar.sp_add_additionaMS(v_eventId);

     commit;




    update sch_eventstat set EVENTSTAT_DT = to_date(v_evt_status_valid_from,pkg_dateutil.f_get_dateformat)
    where pk_eventstat = (select PK_EVENTSTAT from sch_eventstat where fk_event = v_seq);



    commit;


   j := j+1;
   end loop;

     COMMIT;
   o_scheventids := v_schids;
  end;

end PKG_CALENDAR_ENH;
/

CREATE OR REPLACE SYNONYM ERES.pkg_calendar_enh FOR pkg_calendar_enh;
CREATE OR REPLACE SYNONYM EPAT.pkg_calendar_enh FOR pkg_calendar_enh;
GRANT EXECUTE, DEBUG ON pkg_calendar_enh TO EPAT;
GRANT EXECUTE, DEBUG ON pkg_calendar_enh TO ERES;
