--------------------------------------------------------
--  File created - Thursday-March-28-2013   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure SP_M_EVENTNOTIFY
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "ESCH"."SP_M_EVENTNOTIFY" (
   P_EVENT          IN ARRAY_STRING,
   --P_EVENTSTAT      IN NUMBER,
   P_CREATOR        NUMBER,
   P_IPADD          VARCHAR2,
   --P_OLDEVENTSTAT   IN NUMBER,
   --P_EVENTSTATPK IN ARRAY_STRING,
   o_scheventids OUT ARRAY_STRING
)
AS

/*
Author : Tarandeep Singh Bali 
11 March 2013
This procedure will move the records to despatcher table to send mails in case of change in event status
*/

 V_EVENTFLAG       NUMBER;
 V_POS1            NUMBER         := 0;
 V_USERS           VARCHAR2 (1000);
 V_MSG             VARCHAR2 (4000);
 V_STR             VARCHAR2 (1000);
 V_PARAMS          VARCHAR2 (20);
 V_CNT             NUMBER;

 V_EVENTNAME       VARCHAR2 (4000);--KM-15Sep09
 V_PATCODE         VARCHAR2 (25);
 V_FPARAM          VARCHAR2 (4000); --KM
 V_SCHDATE         VARCHAR2 (12) ;
 V_VISIT           NUMBER;
 V_PATPROT         NUMBER     ;
 V_MSGTEMPLATE     VARCHAR2 (4000);
 V_OLDEVSTATNAME   VARCHAR2 (300);
 V_NEWEVSTATNAME   VARCHAR2 (300);
 V_EVENTNAME_TRUNC VARCHAR2 (4000);

 v_pat_site NUMBER;
 v_pk_per NUMBER;
 v_study NUMBER;
 v_right NUMBER := 0;
 v_visit_name VARCHAR2(50);
 v_fk_protocol number;
 --pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SP_EVENTNOTIFY', pLEVEL  => Plog.LFATAL);
 pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_Calendar', pLEVEL  => Plog.LDEBUG);
   
 v_mainCnt NUMBER;
 v_event NUMBER;
 --v_eventStat NUMBER;
 --v_oldEventStat NUMBER;
 v_schids ARRAY_STRING;
 j NUMBER;
 P_EVENTSTAT number;
 P_OLDEVENTSTAT number;

BEGIN
  v_mainCnt := p_event.COUNT;
  v_schids := ARRAY_STRING();
    j := 1;
     --plog.debug(pctx, 'MainCount: '|| v_mainCnt);
    WHILE j <= v_mainCnt LOOP 

    v_event := TO_NUMBER(p_event(j));
   -- v_eventStat := TO_NUMBER(p_eventstat(j));
    --v_oldEventStat := TO_NUMBER(p_oldeventstat(j));
    --plog.debug(pctx, 'Event Stat PK: '|| P_EVENTSTATPK(j));
    
    v_schids.extend;
    select max(PK_EVENTSTAT) into v_schids(j) from SCH_EVENTSTAT where fk_event=lpad(to_char(v_event),10,'0');
    
    select eventstat into P_EVENTSTAT from sch_eventstat where pk_eventstat = v_schids(j);
    P_OLDEVENTSTAT := P_EVENTSTAT;
    --v_schids(j) := TO_CHAR(P_EVENTSTATPK(j));
    --v_schids(j) := TO_NUMBER(P_EVENTSTATPK(j));
    plog.debug(pctx, 'Event evschids value: '|| v_schids(j));
    SELECT fk_patprot, ev.description, TO_CHAR(ACTUAL_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT), visit, visit_name, fk_protocol
     INTO v_patprot, v_eventname ,v_schdate ,V_VISIT,v_visit_name, v_fk_protocol
     FROM SCH_EVENTS1 ev,SCH_PROTOCOL_VISIT
    WHERE event_id = LPAD(v_event,10,0)  AND pk_protocol_visit = fk_visit ;

    BEGIN
    SELECT fk_study ,fk_per
    INTO v_study,v_pk_per
    FROM er_patprot WHERE pk_patprot = v_patprot;

    EXCEPTION
         WHEN NO_DATA_FOUND THEN
            --V_EVENTFLAG := 0;
            RETURN;
      END;


      SELECT fk_site
    INTO v_pat_site
    FROM er_per WHERE pk_per =v_pk_per;




   SELECT Pkg_Common.SCH_GETPATCODE (V_PATPROT)
     INTO V_PATCODE
     FROM DUAL;   -- v_patcode is param 1

   --V_VISIT := SCH_GETVISIT (V_PATPROT, lpad (P_EVENT, 10, 0));   -- get visit

   SELECT CODELST_DESC
     INTO V_OLDEVSTATNAME
     FROM SCH_CODELST
    WHERE PK_CODELST = P_OLDEVENTSTAT
      AND CODELST_TYPE = 'eventstatus';

   SELECT CODELST_DESC
     INTO V_NEWEVSTATNAME
     FROM SCH_CODELST
    WHERE PK_CODELST = P_EVENTSTAT
      AND CODELST_TYPE = 'eventstatus';

   V_MSGTEMPLATE := Pkg_Common.SCH_GETMAILMSG ('eventstat');
   --KM-16Sep09
   v_eventname_trunc := SUBSTR(v_eventname,1,500)  || '...';
   V_FPARAM := V_PATCODE ||
               '~' ||
               NVL(TO_CHAR(v_visit_name),'Not Known') ||
               '~' ||
               v_eventname_trunc ||
               '~' ||
               V_SCHDATE ||
               '~' ||
               V_OLDEVSTATNAME ||
               '~' ||
               V_NEWEVSTATNAME;
      V_MSG := Pkg_Common.SCH_GETMAIL (V_MSGTEMPLATE, V_FPARAM);

plog.debug(pctx,'gotmail');

   FOR I IN 1 .. 2
   LOOP
      V_POS1 := 0;
      V_USERS := NULL;
      V_EVENTFLAG := 0;


      IF I = 2 THEN
         V_EVENTNAME := 'All';
      END IF;

      BEGIN
         SELECT ALNOT_FLAG, ALNOT_USERS
           INTO V_EVENTFLAG, V_USERS
           FROM SCH_ALERTNOTIFY
          WHERE FK_study = v_study and  alnot_globalflag = 'G' and alnot_type = 'N' and fk_patprot is null
             and FK_CODELST_AN = P_EVENTSTAT
            AND ALNOT_MOBEVE = V_EVENTNAME and fk_protocol = v_fk_protocol;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            V_EVENTFLAG := 0;
      END;

    plog.debug(pctx,'V_EVENTFLAG' || V_EVENTFLAG);


      IF V_EVENTFLAG = 1 THEN
         /* Get the message */

         /*select msgtxt_short
           into v_msg
           from sch_msgtxt
          where msgtxt_type = 'eventstat' ; */

         /*Move the records to despatcher to send email to the users*/
         V_STR := V_USERS || ',';
         V_PARAMS := V_USERS || ',';


         LOOP
            V_CNT := V_CNT + 1;
            V_POS1 := INSTR (V_STR, ',');
            V_PARAMS := SUBSTR (V_STR, 1, V_POS1 - 1);
            EXIT WHEN V_PARAMS IS NULL;

            --check for email recepient user's rights on patient's organization

            BEGIN
                        SELECT      pkg_user.f_chk_right_for_studysite(v_study ,v_params,v_pat_site )
                        INTO v_right FROM dual;
            EXCEPTION WHEN OTHERS THEN
                        v_right := 0;
                        plog.debug(pctx,'v_right - exce' || v_right);
            END;

            plog.debug(pctx,'V_right' || v_right);

            --KM-#3891
              IF ((v_right >  0) and (P_EVENTSTAT <> P_OLDEVENTSTAT) ) THEN
                        INSERT INTO SCH_DISPATCHMSG
                        (     PK_MSG,    MSG_SENDON,    MSG_STATUS,   MSG_TYPE,   FK_SCHEVENT,      MSG_TEXT,      FK_PAT,
                                       CREATOR,    IP_ADD,   FK_PATPROT      )
                 VALUES(    SCH_DISPATCHMSG_SEQ1.NEXTVAL,  SYSDATE,   0,  'U',v_event,  V_MSG,   V_PARAMS,
                    P_CREATOR,   P_IPADD,  v_patprot   );

                    plog.debug(pctx,'insrted' );
            END IF;

            V_STR := SUBSTR (V_STR, V_POS1 + 1);
         END LOOP;
      END IF;
   END LOOP;
commit;
COMMIT;
j := j+1;
end loop;
     COMMIT;
   o_scheventids := v_schids;
END SP_M_EVENTNOTIFY;

/

