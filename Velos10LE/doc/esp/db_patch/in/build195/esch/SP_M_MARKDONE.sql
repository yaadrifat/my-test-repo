--------------------------------------------------------
--  File created - Wednesday-March-27-2013   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure SP_M_MARKDONE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "ESCH"."SP_M_MARKDONE" (
p_eventIds IN ARRAY_STRING,
p_notes IN ARRAY_STRING,
p_date IN ARRAY_STRING,
p_user IN NUMBER,
--p_status IN NUMBER,
p_ipadd VARCHAR,
p_mode in char,
p_sosId in ARRAY_STRING,
p_coverageTypeId in ARRAY_STRING,
--P_EVENTSTATPK IN ARRAY_STRING,
p_reason_for_change_CT in ARRAY_STRING
) AS

v_eventIds number;
v_date date;
--v_status number;
v_notes varchar2(4000);
v_sosId number;
v_coverageTypeId number;
v_reasonForChange varchar2(4000);
v_mainCnt number;
v_eventDate date;
v_actual_date char(20);
v_checkPK number;
v_eventStatPK number;
j NUMBER;
p_status NUMBER;

 pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_Calendar', pLEVEL  => Plog.LDEBUG);

BEGIN

  

--update sch_events1 with newly inserted status

    v_mainCnt := p_eventIds.COUNT;
    --v_schids := ARRAY_STRING();
    j := 1;
    
    WHILE j <= v_mainCnt LOOP
    
    v_eventIds := TO_NUMBER(p_eventids(j));
    v_notes := TO_CHAR(p_notes(j));
    --v_status := TO_NUMBER(p_status(j));
    v_sosId := TO_NUMBER(p_sosId(j));
    v_coverageTypeId := TO_NUMBER(p_coverageTypeId(j));
    v_reasonForChange := TO_CHAR(p_reason_for_change_ct(j));
    
   v_eventDate := TO_DATE(p_Date(j), 'yyyy-mm-dd');
    
   v_actual_date :=TO_CHAR(v_eventDate,PKG_DATEUTIL.F_GET_DATEFORMAT);
   v_date := TO_DATE(v_actual_date,PKG_DATEUTIL.F_GET_DATEFORMAT);
   
   select max(PK_EVENTSTAT) into v_eventStatPK from SCH_EVENTSTAT where fk_event=lpad(to_char(v_eventIds),10,'0');
   
   select eventstat into p_status from sch_eventstat where pk_eventstat = v_eventStatPK;
   --v_eventStatPK := TO_NUMBER(P_EVENTSTATPK(j));
    --v_date := v_eventDate;
    --v_Date := TO_DATE(p_Date(j), 'yyyy-mm-dd');
    --plog.debug(pctx, 'Event Start Date2: '|| v_eventStartDate);
    --v_event_Actual_Date :=TO_CHAR(v_eventStartDate,'mm/dd/yyyy');
    
    update sch_events1
   set NOTES = v_notes,
       EVENT_EXEON = v_date,
   EVENT_EXEBY = p_user,
   ISCONFIRMED = p_status, LAST_MODIFIED_BY = p_user, LAST_MODIFIED_DATE = sysdate,
   IP_ADD = p_ipadd,
   SERVICE_SITE_ID= v_sosId,
   FK_CODELST_COVERTYPE=v_coverageTypeId,
   REASON_FOR_COVERAGECHANGE = v_reasonForChange
   where EVENT_ID = lpad(to_char(v_eventIds),10,'0');

select max(PK_EVENTSTAT) INTO v_checkPK from SCH_EVENTSTAT where fk_event=lpad(to_char(v_eventIds),10,'0');

  plog.debug(pctx, 'Event Stat PK: '|| v_checkPK);

     update SCH_EVENTSTAT
     set  EVENTSTAT_DT = v_date,
          EVENTSTAT_NOTES =  v_notes , LAST_MODIFIED_BY = p_user, LAST_MODIFIED_DATE = sysdate, IP_ADD = p_ipadd
     where fk_event = lpad(to_char(v_eventIds),10,'0')
     --and PK_EVENTSTAT = (select max(PK_EVENTSTAT) from SCH_EVENTSTAT where fk_event=lpad(to_char(v_eventIds),10,'0'));
     and PK_EVENTSTAT = v_eventStatPK;


COMMIT;
j := j+1;
end loop;


END SP_M_MARKDONE;

/

