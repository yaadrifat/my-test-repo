set define off;

create or replace
TRIGGER ER_STUDYSTAT_AD1_OUT 
AFTER DELETE ON ER_STUDYSTAT 
FOR EACH ROW
DECLARE 
fkaccount NUMBER; 
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SVC_OUTBOUND', pLEVEL  => Plog.LFATAL);
BEGIN
  SELECT fk_account 
  INTO fkaccount
  FROM er_study
  WHERE pk_study = :OLD.fk_study; 
  
  INSERT INTO er_msg_queue(pk_message, 
  tablename, 
  table_pk, 
  module, 
  fk_account,
  processed_flag, 
  action, root_pk,
  root_tablename) 
  values 
  (SEQ_ER_MSG_QUEUE.nextval,
  'er_studystat',
  :OLD.pk_studystat, 
  'study_status',
  fkaccount,
  0,
  'D',
  to_char(:OLD.fk_study),
  'er_study');
  EXCEPTION  WHEN OTHERS THEN
  Plog.FATAL(pCTX,'exception in ER_STUDYSTAT_AD1_OUT ' || SQLERRM);
END;

--------------------------------------------------------------------------------------------------------


create or replace
TRIGGER ER_STUDYSTAT_AI1_OUT 
BEFORE INSERT ON ER_STUDYSTAT 
FOR EACH ROW
DECLARE 
fkaccount NUMBER; 
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SVC_OUTBOUND', pLEVEL  => Plog.LFATAL);
BEGIN
  SELECT fk_account
  INTO fkaccount
  FROM er_user
  WHERE pk_user = :NEW.creator; 
  INSERT INTO er_msg_queue
  (pk_message,
  tablename, 
  table_pk, 
  module, 
  fk_account,
  processed_flag, 
  action,
  root_pk,
  root_tablename) 
  VALUES 
  (SEQ_ER_MSG_QUEUE.nextval,
  'er_studystat',
  :NEW.pk_studystat, 
  'study_status', 
  fkaccount,
  0, 
  'I', 
  to_char(:NEW.fk_study),
  'er_study'); 
  EXCEPTION  WHEN OTHERS THEN
  Plog.FATAL(pCTX,'exception in ER_STUDYSTAT_AI1_OUT ' || SQLERRM);
END;


----------------------------------------------------------------------------------------------------------------
create or replace
TRIGGER ER_STUDYSTAT_AU2_OUT
AFTER UPDATE ON ER_STUDYSTAT
FOR EACH ROW
DECLARE 
fkaccount NUMBER;
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SVC_OUTBOUND', pLEVEL  => Plog.LFATAL);
BEGIN
  SELECT fk_account
  INTO fkaccount
  FROM er_user
  WHERE pk_user = NVL(:NEW.LAST_MODIFIED_BY, :NEW.CREATOR);
  
  INSERT INTO er_msg_queue
  (pk_message,
  tablename, 
  table_pk, 
  module, 
  fk_account,
  processed_flag,
  action,
  root_pk, 
  root_tablename)
  VALUES 
  (SEQ_ER_MSG_QUEUE.nextval, 
  'er_studystat',
  :NEW.pk_studystat,
  'study_status',
  fkaccount,
  0,
  'U',
  to_char(:NEW.fk_study), 
  'er_study'); 
  EXCEPTION WHEN OTHERS THEN
   Plog.FATAL(pCTX,'exception in ER_STUDYSTAT_AU2_OUT ' || SQLERRM);
END;



