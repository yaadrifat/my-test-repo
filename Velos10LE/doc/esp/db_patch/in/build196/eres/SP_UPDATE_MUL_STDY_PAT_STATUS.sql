CREATE OR REPLACE PROCEDURE ERES.SP_UPDATE_MUL_STDY_PAT_STATUS(p_array IN ARRAY_STRING,pk_array IN ARRAY_STRING,s_array OUT ARRAY_STRING,current_status IN VARCHAR2 )
as
v_updatestatus VARCHAR2(1000);
v_current_status VARCHAR2(1000);
v_pk_stat   NUMBER := 0 ;
v_errorPK  NUMBER:= 0;
j NUMBER := 1;

BEGIN
/*
Stored procedure to update the multiple patient study status
*/
s_array := new ARRAY_STRING();
v_current_status := current_status;

IF v_current_status IS NOT NULL THEN
 execute immediate v_current_status;
  END IF;
for i in 1 .. pk_array.count
 loop
 begin
         v_pk_stat := pk_array(i);
         v_updatestatus:=p_array(i);
         IF(v_pk_stat = v_errorPK) THEN
         continue;
          END IF;
        
          execute immediate v_updatestatus;
          
         IF(i <  pk_array.count and v_pk_stat = pk_array(i+1)) THEN
            continue;
            END IF;
            s_array.extend;
            commit;
            v_errorPK :=0;
            s_array(j) := v_pk_stat;
            j := j+1;        
        EXCEPTION 
        WHEN OTHERS THEN
        ROLLBACK;
            s_array.extend;
            v_errorPK := v_pk_stat;
            s_array(j) := 'Error while updating StudyPatientStatus  for ' || v_errorPK;   
            j := j+1;
 end;
end loop;
END SP_UPDATE_MUL_STDY_PAT_STATUS;
/

