DROP TRIGGER ERES.ER_STUDYFORMS_AU2_OUT;

CREATE OR REPLACE TRIGGER ERES.ER_STUDYFORMS_AU2_OUT
AFTER UPDATE
OF FORM_COMPLETED
ON ERES.ER_STUDYFORMS 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
fkaccount NUMBER;
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SVC_OUTBOUND', pLEVEL  => Plog.LFATAL);

BEGIN
 /* Author : Raman */
 SELECT fk_account
  INTO fkaccount
  FROM er_user
  WHERE pk_user = :NEW.CREATOR; 
  IF NVL(:OLD.FORM_COMPLETED,0) != NVL(:NEW.FORM_COMPLETED,0)    
  THEN

 PKG_MSG_QUEUE.SP_POPULATE_FORMRES_STATUS_MSG('ER_STUDYFORMS',
                                                :NEW.PK_STUDYFORMS,
                                               fkaccount,
                                               'U',
                                               to_char(:NEW.FK_FORMLIB),
                                               :NEW.FORM_COMPLETED );    
                                               
    
  END IF;
  EXCEPTION
     WHEN OTHERS THEN
       Plog.FATAL(pCTX,'exception in ERES.ER_STUDYFORMS_AU2_OUT ' || SQLERRM);

END ER_STUDYFORMS_AU0_OUT;
/


