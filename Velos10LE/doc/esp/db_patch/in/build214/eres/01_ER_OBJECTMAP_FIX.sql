SET DEFINE OFF;
SET SERVEROUTPUT ON;

DECLARE
  v_sysid VARCHAR(255);
  CURSOR c1 IS
    select distinct m1.TABLE_NAME, m1.TABLE_PK from ERES.ER_OBJECT_MAP m1, ERES.ER_OBJECT_MAP m2
    where m1.SYSTEM_ID <> M2.SYSTEM_ID and m1.TABLE_NAME = m2.TABLE_NAME
    and m1.TABLE_PK = m2.TABLE_PK group by m1.SYSTEM_ID, m1.TABLE_NAME, m1.TABLE_PK;
  indx_cnt NUMBER;

BEGIN
  FOR item in c1
  LOOP
    select min (m1.SYSTEM_ID) into v_sysid from ERES.ER_OBJECT_MAP m1 where 
    m1.TABLE_NAME = item.TABLE_NAME and m1.TABLE_PK = item.TABLE_PK;
    DBMS_OUTPUT.PUT_LINE('Deleting SYSTEM_ID '||v_sysid);
    delete from ERES.ER_OBJECT_MAP where SYSTEM_ID = v_sysid;
  END LOOP;
  COMMIT;

  select count(*) into indx_cnt 
  from SYS.ALL_INDEXES where INDEX_NAME = 'IDX_REFERENCE';
  
  if (indx_cnt < 1) then 
    execute immediate   
'CREATE UNIQUE INDEX ERES.IDX_REFERENCE ON ERES.ER_OBJECT_MAP '||
'(TABLE_NAME, TABLE_PK) '||
'LOGGING '||
'TABLESPACE ERES_USER '||
'PCTFREE    10 '||
'INITRANS   2 '||
'MAXTRANS   255 '||
'STORAGE    ( '||
'            INITIAL          64K '||
'            NEXT             1M '||
'            MINEXTENTS       1 '||
'            MAXEXTENTS       UNLIMITED '||
'            PCTINCREASE      0 '||
'            BUFFER_POOL      DEFAULT '||
'           ) '||
'NOPARALLEL ';
    DBMS_OUTPUT.PUT_LINE('Constraint IDX_REFERENCE created.');
  else
    DBMS_OUTPUT.PUT_LINE('Constraint IDX_REFERENCE already exists.');
  end if; 

END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,292,921,'01_ER_OBJECTMAP_FIX.sql',sysdate,'9.2.1 eSP4.2.0 B#214');
