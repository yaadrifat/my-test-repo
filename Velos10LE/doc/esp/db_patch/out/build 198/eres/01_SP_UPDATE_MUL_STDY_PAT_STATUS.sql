set define off;

CREATE OR REPLACE PROCEDURE ERES.SP_UPDATE_MUL_STDY_PAT_STATUS(P_ARRAY IN ARRAY_STRING,PK_ARRAY IN ARRAY_STRING,S_ARRAY OUT ARRAY_STRING,CURRENT_STATUS IN VARCHAR2 )
as
v_updatestatus VARCHAR2(1000);
v_current_status VARCHAR2(1000);
v_pk_stat   NUMBER := 0 ;
v_errorpk  NUMBER:= 0;
v_pk_pat_std  NUMBER;
v_pk_study  NUMBER;
v_pk_person  NUMBER;
v_pk_db  NUMBER;
v_cs  NUMBER;
j NUMBER := 1;

BEGIN
/*
Stored procedure to update the multiple patient study status
*/
s_array := new ARRAY_STRING();
v_current_status := current_status;


FOR i in 1 .. pk_array.count
 LOOP
 BEGIN
         v_pk_stat := pk_array(i);
         v_updatestatus:=p_array(i);
         
         IF(v_pk_stat = v_errorpk) THEN
         CONTINUE;
          END IF; 
        
          EXECUTE IMMEDIATE v_updatestatus;
         BEGIN
                SELECT current_stat INTO v_cs FROM ER_PATSTUDYSTAT WHERE pk_patstudystat = v_pk_stat;
                SELECT fk_study INTO v_pk_study FROM ER_PATSTUDYSTAT WHERE pk_patstudystat = v_pk_stat;
                SELECT fk_per INTO v_pk_person FROM ER_PATSTUDYSTAT WHERE pk_patstudystat = v_pk_stat;  
            IF(v_cs=1) THEN
             BEGIN    
                SELECT pk_patstudystat INTO v_pk_pat_std  FROM ER_PATSTUDYSTAT  WHERE fk_study = v_pk_study AND fk_per= v_pk_person  AND   pk_patstudystat <> v_pk_stat  AND current_stat = 1;
                UPDATE ER_PATSTUDYSTAT SET current_stat=0 WHERE pk_patstudystat = v_pk_pat_std;
              
              EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                        v_pk_pat_std :=0;
             END;
           END IF; 
                          
         END;           
         IF(i <  PK_ARRAY.count and v_pk_stat = PK_ARRAY(i+1)) THEN
            CONTINUE;
            END IF;
            s_array.extend;
            COMMIT;
            v_errorpk :=0;
            s_array(j) := v_pk_stat;
            j := j+1;        
        EXCEPTION    
        WHEN OTHERS THEN
        ROLLBACK;
            s_array.extend;
            v_errorpk := v_pk_stat;
            s_array(j) := 'Error while updating StudyPatientStatus  for ' || v_errorpk || SQLERRM;   
            j := j+1;
 END;
END LOOP;
END SP_UPDATE_MUL_STDY_PAT_STATUS;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,204,912,'01_SP_UPDATE_MUL_STDY_PAT_STATUS.sql',sysdate,'9.1.2 eSP4.1.1 B#198');

commit;
