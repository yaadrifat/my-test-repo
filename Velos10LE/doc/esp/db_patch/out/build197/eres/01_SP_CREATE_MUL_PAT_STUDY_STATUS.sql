set define off;


CREATE OR REPLACE PROCEDURE ERES."SP_CREATE_MUL_PAT_STUDY_STATUS" (
    p_personSqls IN ARRAY_STRING,
    p_patProtSqls IN ARRAY_STRING,
    p_patStudyStatSqls IN ARRAY_STRING,
    p_fkAr IN ARRAY_STRING,
    o_ret OUT ARRAY_STRING)

--Author 
--Raman Dhawan

AS
newPk NUMBER;
j NUMBER := 1;
q NUMBER :=1;
v_cdlst_stat VARCHAR2(200);
v_cdlst_enrolled VARCHAR2(200);
v_prev_enrolled_stat VARCHAR2(200);
v_prev_current_stat VARCHAR2(200);
v_current_stat VARCHAR2(200);

v_csv VARCHAR2(200);
v_num NUMBER :=0;
v_arr DBMS_UTILITY.UNCL_ARRAY;

v_fkStat NUMBER;
v_fkPer NUMBER;
v_fkStudy NUMBER;
v_beginDate VARCHAR2(200);
v_curFlag VARCHAR2(200);

v_errorCode VARCHAR2(200);

BEGIN
    
   o_ret  := new ARRAY_STRING();
    for i in 1 .. p_patStudyStatSqls.count
    loop
        BEGIN
            newPk :=0;
            v_csv:= p_fkAr(i);
            DBMS_UTILITY.COMMA_TO_TABLE (regexp_replace(v_csv,'(^|,)','\1x'), v_num, v_arr);
             v_fkStat :=to_number(substr(v_arr(1),2));
             v_fkPer :=to_number(substr(v_arr(2),2));
             v_fkStudy :=to_number(substr(v_arr(3),2));
            -- v_beginDate :=to_number(substr(v_arr(4),2));
           
        --executing person and patprot sql 
           execute immediate p_personSqls(i);
           execute immediate p_patProtSqls(i);
           SELECT codelst_subtyp INTO v_cdlst_stat FROM ER_CODELST WHERE pk_codelst = v_fkStat;
            --getting codelist pk for enrolled status
            v_cdlst_enrolled:= ERES."F_CODELST_ID" ('patStatus','enrolled') ;
            IF  (v_cdlst_stat='enrolled') OR
                (v_cdlst_stat='active') OR
                (v_cdlst_stat='followup') OR
                (v_cdlst_stat='offtreat') OR
                (v_cdlst_stat='offstudy') OR
                (v_cdlst_stat='lockdown')
            THEN
                BEGIN
                        SELECT PK_PATSTUDYSTAT INTO v_prev_enrolled_stat FROM ERES.er_patstudystat  where     FK_STUDY =v_fkStudy and  FK_PER =v_fkPer  and FK_CODELST_STAT=v_cdlst_enrolled;
                         IF  (v_cdlst_stat='enrolled')--Found previos Enrolled Stat
                         THEN
                             v_errorCode:='-3';
                             RAISE INVALID_NUMBER ;
                          END IF;
                    EXCEPTION
                    WHEN NO_DATA_FOUND THEN--NO Previous Enrollment
                     IF  (v_cdlst_stat<>'enrolled')
                      THEN
                         v_errorCode:='-4';
                         RAISE INVALID_NUMBER ;
                       END IF;
                END;
                
            END IF;
            
            select SEQ_ER_PATSTUDYSTAT.NEXTVAL INTO newPk from dual;
            execute immediate p_patStudyStatSqls(i) USING newPk;
            select patstudystat_date,CURRENT_STAT  INTO v_beginDate, v_curFlag from eres.er_patstudystat where pk_patstudystat=newPk;
           --current flag check
            IF(v_curFlag='1')
            THEN
                BEGIN
                SELECT PK_PATSTUDYSTAT   
                    INTO v_prev_current_stat  
                    FROM ERES.ER_PATSTUDYSTAT   
                    WHERE FK_STUDY = v_fkStudy and  FK_PER = v_fkPer   
                        and CURRENT_STAT = '1' 
                        and PK_PATSTUDYSTAT <> newPk;
                 UPDATE ERES.ER_PATSTUDYSTAT SET    CURRENT_STAT   = '0'  WHERE  PK_PATSTUDYSTAT     =v_prev_current_stat;
                  EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                        v_prev_current_stat:=0;
                END;
            END IF;
           
            BEGIN
                SELECT PK_PATSTUDYSTAT  
                INTO v_prev_current_stat 
                FROM ERES.ER_PATSTUDYSTAT  
                WHERE FK_STUDY = v_fkStudy and  FK_PER = v_fkPer  
                    and PATSTUDYSTAT_ENDT is null 
                    and PATSTUDYSTAT_DATE <= v_beginDate 
                    and PK_PATSTUDYSTAT <> newPk;
                UPDATE ERES.ER_PATSTUDYSTAT SET    PATSTUDYSTAT_ENDT   = v_beginDate WHERE  PK_PATSTUDYSTAT     =v_prev_current_stat;
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                              BEGIN
                                SELECT PK_PATSTUDYSTAT  
                                INTO v_prev_current_stat 
                                FROM ERES.ER_PATSTUDYSTAT  
                                WHERE        PK_PATSTUDYSTAT= 
                                     ( select max(i.PK_PATSTUDYSTAT) from ERES.ER_PATSTUDYSTAT  i 
                                            where   FK_STUDY = v_fkStudy and  FK_PER = v_fkPer   
                                             and    i.PATSTUDYSTAT_DATE = ( 
                                                        select max(ii.PATSTUDYSTAT_DATE) from ERES.ER_PATSTUDYSTAT  ii 
                                                            Where     FK_STUDY = v_fkStudy and  FK_PER = v_fkPer  
                                                                 and        ii.PATSTUDYSTAT_DATE <=v_beginDate and ii.PK_PATSTUDYSTAT <> newPk) 
                                             
                                             and       i.PK_PATSTUDYSTAT <> newPk);
                                UPDATE ERES.ER_PATSTUDYSTAT SET    PATSTUDYSTAT_ENDT   = v_beginDate WHERE  PK_PATSTUDYSTAT     =v_prev_current_stat;
                                EXCEPTION
                                     WHEN NO_DATA_FOUND THEN
                                     v_prev_current_stat:=0;
                            END;
                            
                             BEGIN
                                SELECT PK_PATSTUDYSTAT  
                                INTO v_prev_current_stat 
                                FROM ERES.ER_PATSTUDYSTAT 
                                WHERE       PK_PATSTUDYSTAT =
                                 ( select max(i.PK_PATSTUDYSTAT) from ERES.ER_PATSTUDYSTAT  i 
                                    where       FK_STUDY = v_fkStudy and  FK_PER = v_fkPer 
                                     and        i.PATSTUDYSTAT_DATE = ( 
                                        select min(ii.PATSTUDYSTAT_DATE) from ERES.ER_PATSTUDYSTAT  ii 
                                            Where     FK_STUDY = v_fkStudy and  FK_PER = v_fkPer 
                                             and        ii.PATSTUDYSTAT_DATE >=v_beginDate 
                                              and ii.PK_PATSTUDYSTAT <> newPk) and        i.PK_PATSTUDYSTAT <> newPk);
                               UPDATE ERES.ER_PATSTUDYSTAT SET    PATSTUDYSTAT_ENDT   = v_beginDate WHERE  PK_PATSTUDYSTAT     =v_prev_current_stat;
                                EXCEPTION
                                     WHEN NO_DATA_FOUND THEN
                                     v_prev_current_stat:=0;
                            END;
                            
                END;
            
                BEGIN
                    SELECT PK_PATSTUDYSTAT  
                                    INTO v_current_stat 
                                    FROM ERES.ER_PATSTUDYSTAT 
                                    WHERE
                                      PK_PATSTUDYSTAT =
                                       ( select max(i.PK_PATSTUDYSTAT) from  ERES.ER_PATSTUDYSTAT  i 
                                       where      FK_STUDY = v_fkStudy and  FK_PER = v_fkPer 
                                        and    trunc(i.PATSTUDYSTAT_DATE) = ( select max(trunc(ii.PATSTUDYSTAT_DATE)) from ERES.ER_PATSTUDYSTAT  ii 
                                        Where   FK_STUDY = v_fkStudy and  FK_PER = v_fkPer ));
                    UPDATE ERES.ER_PATSTUDYSTAT SET    PATSTUDYSTAT_ENDT   = null  WHERE  PK_PATSTUDYSTAT     =v_current_stat;
                     EXCEPTION
                                         WHEN NO_DATA_FOUND THEN
                                         v_current_stat:=0;
               END;
            COMMIT; 
            o_ret.extend;
            --o_ret(j) := 'Error while creating StudyPatientStatus  '||q||'  ' ||SQLCODE||'   '||SQLERRM;  
            o_ret(j) := newPk ;
            j := j+1;  
            EXCEPTION 
            WHEN OTHERS THEN
            ROLLBACK;
            --o_ret(j) := 'Error while creating StudyPatientStatus  '||q||'  ' ||SQLCODE||'   '||SQLERRM;  
             o_ret.extend;
            o_ret(j) := v_errorCode;
            j := j+1;
             v_errorCode:='0';
 end;
end loop;

END SP_CREATE_MUL_PAT_STUDY_STATUS;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,203,911,'01_SP_CREATE_MUL_PAT_STUDY_STATUS.sql',sysdate,'9.1.1 eSP4.1.1 B#197');

commit;