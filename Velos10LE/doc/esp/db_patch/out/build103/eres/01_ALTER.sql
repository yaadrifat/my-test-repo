CREATE TABLE "ERES"."SVC_SESSION_LOG"
  (
    "PK_SESSION_LOG"  NUMBER(10,0) NOT NULL ENABLE,
    "SUCCESSFLAG" NUMBER(1,0),
    "USER_IDENT"  VARCHAR2(255 CHAR),
    "LOGIN_TIME" TIMESTAMP (6),
    "LOGOUTTIME" TIMESTAMP (6),
    "REMOTE_ADDR" VARCHAR2(255 CHAR),
    PRIMARY KEY ("PK_SESSION_LOG")
  )
  TABLESPACE "ERES_USER" ;

  COMMENT ON TABLE "ERES"."SVC_SESSION_LOG" IS  'Stores session log information for service messages.';
  COMMENT ON COLUMN "ERES"."SVC_SESSION_LOG"."PK_SESSION_LOG" IS  'Primary key for the table.' ;
  COMMENT ON COLUMN "ERES"."SVC_SESSION_LOG"."SUCCESSFLAG" IS  'Indicates whether authentication was successful for this seassion. 1=success, 0=failed' ;
  COMMENT ON COLUMN "ERES"."SVC_SESSION_LOG"."USER_IDENT" IS  'Identity associated with this session. Can be an eResearch user, or identity trusted through brokered trust.' ;
  COMMENT ON COLUMN "ERES"."SVC_SESSION_LOG"."LOGIN_TIME" IS  'Time that the session was opened.' ;
  COMMENT ON COLUMN "ERES"."SVC_SESSION_LOG"."LOGOUTTIME" IS  'Time that the session was closed.' ;
  COMMENT ON COLUMN "ERES"."SVC_SESSION_LOG"."REMOTE_ADDR" IS  'Address of the remote machine.' ;
 
  CREATE TABLE "ERES"."SVC_MESSAGE_LOG"
    (
      "PK_MESSAGE_LOG" NUMBER(10,0) NOT NULL ENABLE,
      "INITIATION_TIME" TIMESTAMP (6),
      "DURATION"     NUMBER(19,0),
      "ENDPOINT"     VARCHAR2(255 CHAR),
      "OPERATION"    VARCHAR2(255 CHAR),
      "FK_SESSION_LOG"   NUMBER(10,0),
      "FK_USER"      NUMBER(10,0),
      "BODY"         VARCHAR2(255 CHAR),
      "MODULE"       VARCHAR2(255 CHAR),
      "ROUTE"        VARCHAR2(255 CHAR),
      "MESSAGE_ID"   VARCHAR2(255 CHAR),
      "SUCCESS_FLAG" NUMBER(1,0),
      "ERROR_TYPE" VARCHAR2(80 CHAR),
      "ERROR_STRING" VARCHAR2(255 CHAR),
      PRIMARY KEY ("PK_MESSAGE_LOG")
    )
  TABLESPACE "ERES_USER" ;

  COMMENT ON TABLE "ERES"."SVC_MESSAGE_LOG" IS  'Stores message log information for service messages. Messages can be incoming or outgoing.';
  COMMENT ON COLUMN "ERES"."SVC_MESSAGE_LOG"."PK_MESSAGE_LOG" IS  'Primary key for the table.' ;
  COMMENT ON COLUMN "ERES"."SVC_MESSAGE_LOG"."INITIATION_TIME" IS  'Time that message was initiated' ;
  COMMENT ON COLUMN "ERES"."SVC_MESSAGE_LOG"."DURATION" IS  'Duration, in milliseconds, between initiation and completion.' ;
  COMMENT ON COLUMN "ERES"."SVC_MESSAGE_LOG"."ENDPOINT" IS  'Identifies the enpoint being communicated to.' ;
  COMMENT ON COLUMN "ERES"."SVC_MESSAGE_LOG"."OPERATION" IS  'Identifies the operation that the message is intented to perform.' ;
  COMMENT ON COLUMN "ERES"."SVC_MESSAGE_LOG"."FK_SESSION_LOG" IS  'Foreign key to the session_log.' ;
  COMMENT ON COLUMN "ERES"."SVC_MESSAGE_LOG"."FK_USER" IS  'Foreign key to the user making this request' ;
  COMMENT ON COLUMN "ERES"."SVC_MESSAGE_LOG"."BODY" IS  'Body of the message. Currently unused.' ;
  COMMENT ON COLUMN "ERES"."SVC_MESSAGE_LOG"."MODULE" IS  'Service module processing the message.' ;
  COMMENT ON COLUMN "ERES"."SVC_MESSAGE_LOG"."ROUTE" IS  'Route information.' ;
  COMMENT ON COLUMN "ERES"."SVC_MESSAGE_LOG"."MESSAGE_ID" IS  'External identifier for the message.' ;
  COMMENT ON COLUMN "ERES"."SVC_MESSAGE_LOG"."SUCCESS_FLAG" IS  'Indicates whether processing of the message completed. 1=success, 0=failure' ;
  COMMENT ON COLUMN "ERES"."SVC_MESSAGE_LOG"."ERROR_STRING" IS  'If an error occured, stores type of error.' ;
  COMMENT ON COLUMN "ERES"."SVC_MESSAGE_LOG"."ERROR_STRING" IS  'If an error occured, stores text information about the error.' ;
