set define off;

CREATE TABLE "ERES"."ER_MSG_QUEUE"
  (
    "PK_MESSAGE"     NUMBER NOT NULL ENABLE,
    "TABLENAME"      VARCHAR2(128 BYTE),
    "TABLE_PK"       NUMBER,
    "MODULE"         VARCHAR2(64 BYTE),
    "FK_ACCOUNT"     NUMBER,
    "PROCESSED_FLAG" NUMBER,
    "ACTION"         CHAR(1 CHAR),
    "ROOT_PK"        VARCHAR2(1024 BYTE),
    "ROOT_TABLENAME" VARCHAR2(4000 BYTE),
    "CREATED_ON" DATE DEFAULT sysdate,
    CONSTRAINT "XPKER_MSG_QUEUE" PRIMARY KEY ("PK_MESSAGE") USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT) TABLESPACE "ERES_USER" ENABLE
  )
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE
  (
    INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT
  )
  TABLESPACE "ERES_USER" ;
COMMENT ON COLUMN "ERES"."ER_MSG_QUEUE"."PK_MESSAGE"
IS
  'This is primary key for this table';
  COMMENT ON COLUMN "ERES"."ER_MSG_QUEUE"."TABLENAME"
IS
  'This is table name for the module which is modified.';
  COMMENT ON COLUMN "ERES"."ER_MSG_QUEUE"."TABLE_PK"
IS
  'This is primary key of table whose record has been modified';
  COMMENT ON COLUMN "ERES"."ER_MSG_QUEUE"."MODULE"
IS
  'This represents module which comes from er_modules for outbound messaging';
  COMMENT ON COLUMN "ERES"."ER_MSG_QUEUE"."FK_ACCOUNT"
IS
  'This is account number for which data is updated';
  COMMENT ON COLUMN "ERES"."ER_MSG_QUEUE"."PROCESSED_FLAG"
IS
  'This represents status of record. ';
  COMMENT ON COLUMN "ERES"."ER_MSG_QUEUE"."ACTION"
IS
  'This represents action taken on record: ''C'' represents Create action, ''D'' - "Delete" and ''U'' - Update.';
  COMMENT ON COLUMN "ERES"."ER_MSG_QUEUE"."ROOT_PK"
IS
  'This is comma separated Primary keys of ROOT tables ';
  COMMENT ON COLUMN "ERES"."ER_MSG_QUEUE"."ROOT_TABLENAME"
IS
  'This is comma separated table names for root , should be in same sequence as Root_PK';
  COMMENT ON COLUMN "ERES"."ER_MSG_QUEUE"."CREATED_ON"
IS
  'This stores timestamp of change';


--------------------------------------------------------------------------------------

CREATE TABLE "ERES"."ER_MSG_QUEUE_HISTORY"
  (
    "PK_MESSAGE"     NUMBER NOT NULL ENABLE,
    "TABLENAME"      VARCHAR2(128 BYTE),
    "TABLE_PK"       NUMBER,
    "MODULE"         VARCHAR2(64 BYTE),
    "FK_ACCOUNT"     NUMBER,
    "PROCESSED_FLAG" NUMBER,
    "ACTION"         CHAR(1 CHAR),
    "ROOT_PK"        VARCHAR2(1024 BYTE),
    "ROOT_TABLENAME" VARCHAR2(4000 BYTE),
    "CREATED_ON" DATE,
    "ERROR_MESSAGE" VARCHAR2(4000 BYTE),
    "MESSAGE_SENT" DATE DEFAULT SYSDATE
  )
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE
  (
    INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT
  )
  TABLESPACE "ERES_USER" ;


------------------------------------------------------------------------------------------

CREATE TABLE "ERES"."ER_MSG_TOPICS"
  (
    "FK_ACCOUNT"            NUMBER,
    "TOPIC_NAME"            VARCHAR2(128 BYTE),
    "TOPIC_JNDI"            VARCHAR2(128 BYTE),
    "ROOT_TABLENAMES"       VARCHAR2(4000 BYTE),
    "TOPIC_CONNECTION_JNDI" VARCHAR2(128 BYTE) DEFAULT 'ConnectionFactory',
    "TOPIC_CONN_USERNAME"   VARCHAR2(64 BYTE),
    "TOPIC_CONN_PASSWORD"   VARCHAR2(64 BYTE)
  )
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE
  (
    INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT
  )
  TABLESPACE "ERES_USER" ;
COMMENT ON COLUMN "ERES"."ER_MSG_TOPICS"."FK_ACCOUNT"
IS
  'This is account foriegn key to provide different TOPICS for different accounts';
  COMMENT ON COLUMN "ERES"."ER_MSG_TOPICS"."TOPIC_NAME"
IS
  'This is name of Topic where the outbound message for this account willl be sent';
  COMMENT ON COLUMN "ERES"."ER_MSG_TOPICS"."TOPIC_JNDI"
IS
  'This is Topic JNDI to send message to.';
  COMMENT ON COLUMN "ERES"."ER_MSG_TOPICS"."ROOT_TABLENAMES"
IS
  'This can have multiple comma separated root tablenames.';
  COMMENT ON COLUMN "ERES"."ER_MSG_TOPICS"."TOPIC_CONNECTION_JNDI"
IS
  'This is Topic Connection factory JNDI';
  COMMENT ON COLUMN "ERES"."ER_MSG_TOPICS"."TOPIC_CONN_USERNAME"
IS
  'This is username to get Connection for connection factory to get durable subscription';
  COMMENT ON COLUMN "ERES"."ER_MSG_TOPICS"."TOPIC_CONN_PASSWORD"
IS
  'This is password to get Connection for connection factory to get durable subscription.';

----------------------------------------------------------------------------------------------------
