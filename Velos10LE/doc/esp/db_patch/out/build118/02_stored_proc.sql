set define off;

create or replace
PACKAGE      "PKG_MSG_QUEUE"
AS
   MODULE_STUDY_STATUS   VARCHAR2 (200) := 'study_status';
   pCTX Plog.LOG_CTX
         := Plog.init (pSECTION => 'SVC_OUTBOUND', pLEVEL => Plog.LFATAL) ;

   PROCEDURE SP_POPULATE_MSG_QUEUE (
      P_TABLE_NAME          ER_MSG_QUEUE.TABLENAME%TYPE,
      P_TABLE_PK            ER_MSG_QUEUE.TABLE_PK%TYPE,
      P_FK_ACCOUNT          ER_MSG_QUEUE.FK_ACCOUNT%TYPE,
      P_ACTION              ER_MSG_QUEUE.ACTION%TYPE,
      P_ROOT_PK             ER_MSG_QUEUE.ROOT_PK%TYPE,
      P_ROOT_TABLENAME      ER_MSG_QUEUE.ROOT_TABLENAME%TYPE,
      O_PK_MESSAGE   OUT  ER_MSG_QUEUE.PK_MESSAGE%TYPE
   );

   PROCEDURE SP_POPULATE_MSG_ADDINFO (
      P_FK_MESSAGE        ER_MSG_QUEUE.PK_MESSAGE%TYPE,
      P_ELEMENT_NAME      ER_MSG_ADDINFO.ELEMENT_NAME%TYPE,
      P_ELEMENT_VALUE     ER_MSG_ADDINFO.ELEMENT_VALUE%TYPE,
      P_COL_NAME          ER_MSG_ADDINFO.COL_NAME%TYPE
   );

   PROCEDURE SP_POPULATE_STUDY_STATUS_MSG (
      P_TABLE_PK              ER_MSG_QUEUE.TABLE_PK%TYPE,
      P_FK_ACCOUNT            ER_MSG_QUEUE.FK_ACCOUNT%TYPE,
      P_ACTION                ER_MSG_QUEUE.ACTION%TYPE,
      P_ROOT_PK               ER_MSG_QUEUE.ROOT_PK%TYPE,
      P_STUDY_STAT_CODEFK     ER_CODELST.PK_CODELST%TYPE
   );
END PKG_MSG_QUEUE;
/

----------------------------------------------------------------------------------------


create or replace
PACKAGE BODY      "PKG_MSG_QUEUE"
AS
   PROCEDURE SP_POPULATE_MSG_QUEUE (    
      P_TABLE_NAME          ER_MSG_QUEUE.TABLENAME%TYPE,
      P_TABLE_PK            ER_MSG_QUEUE.TABLE_PK%TYPE,
      P_FK_ACCOUNT          ER_MSG_QUEUE.FK_ACCOUNT%TYPE,
      P_ACTION              ER_MSG_QUEUE.ACTION%TYPE,
      P_ROOT_PK             ER_MSG_QUEUE.ROOT_PK%TYPE,
      P_ROOT_TABLENAME      ER_MSG_QUEUE.ROOT_TABLENAME%TYPE,
      O_PK_MESSAGE   OUT    ER_MSG_QUEUE.PK_MESSAGE%TYPE
   )
   AS
      V_MODULE   ER_MSG_QUEUE.MODULE%TYPE;
   BEGIN
      SELECT   CASE LOWER (P_TABLE_NAME)
                  WHEN 'er_studystat' THEN MODULE_STUDY_STATUS
               END
        INTO   V_MODULE
        FROM   DUAL;

      SELECT   SEQ_ER_MSG_QUEUE.NEXTVAL INTO O_PK_MESSAGE FROM DUAL;

      INSERT INTO er_msg_queue (pk_message,
                                tablename,
                                table_pk,
                                module,
                                fk_account,
                                processed_flag,
                                action,
                                root_pk,
                                root_tablename)
        VALUES   (O_PK_MESSAGE,
                  P_TABLE_NAME,
                  P_TABLE_PK,
                  V_MODULE,
                  P_FK_ACCOUNT,
                  0,
                  P_ACTION,
                  P_ROOT_PK ,
                  P_ROOT_TABLENAME);
   EXCEPTION
      WHEN OTHERS
      THEN
         Plog.FATAL (
            pCTX,
            'exception in PKG_MSG_QUEUE.SP_POPULATE_MSG_QUEUE ' || SQLERRM
         );
   END SP_POPULATE_MSG_QUEUE;

   PROCEDURE SP_POPULATE_MSG_ADDINFO (
        P_FK_MESSAGE       ER_MSG_QUEUE.PK_MESSAGE%TYPE,
        P_ELEMENT_NAME     ER_MSG_ADDINFO.ELEMENT_NAME%TYPE,
        P_ELEMENT_VALUE    ER_MSG_ADDINFO.ELEMENT_VALUE%TYPE,
        P_COL_NAME         ER_MSG_ADDINFO.COL_NAME%TYPE
   )
   AS
   BEGIN
      INSERT INTO er_msg_addinfo (PK_MSG_ADDINFO,
                                  FK_MSG_QUEUE,
                                  ELEMENT_NAME,
                                  ELEMENT_VALUE,
                                  COL_NAME)
        VALUES   (SEQ_ER_MSG_ADDINFO.NEXTVAL,
                  P_FK_MESSAGE,
                  P_ELEMENT_NAME,
                  P_ELEMENT_VALUE,
                  P_COL_NAME);
   EXCEPTION
      WHEN OTHERS
      THEN
         Plog.FATAL (
            pCTX,
            'exception in PKG_MSG_QUEUE.SP_POPULATE_MSG_ADDINFO ' || SQLERRM
         );
   END SP_POPULATE_MSG_ADDINFO;

   PROCEDURE SP_POPULATE_STUDY_STATUS_MSG (
        P_TABLE_PK              ER_MSG_QUEUE.TABLE_PK%TYPE,
        P_FK_ACCOUNT            ER_MSG_QUEUE.FK_ACCOUNT%TYPE,
        P_ACTION                ER_MSG_QUEUE.ACTION%TYPE,
        P_ROOT_PK               ER_MSG_QUEUE.ROOT_PK%TYPE,
        P_STUDY_STAT_CODEFK     ER_CODELST.PK_CODELST%TYPE
   )
   AS
      v_PKMSG           ER_MSG_QUEUE.PK_MESSAGE%TYPE;
      v_codesubtype     ER_CODELST.CODELST_SUBTYP%TYPE;
      v_codedes         ER_CODELST.CODELST_DESC%TYPE;
      v_TABLENAME       ER_MSG_QUEUE.TABLENAME%TYPE := 'er_studystat';
      v_ROOTTABLE       ER_MSG_QUEUE.ROOT_TABLENAME%TYPE := 'er_study';
   BEGIN
      SP_POPULATE_MSG_QUEUE (v_TABLENAME,
                             P_TABLE_PK,
                             P_FK_ACCOUNT,
                             P_ACTION,
                             P_ROOT_PK,
                             v_ROOTTABLE,
                             v_PKMSG);

      SELECT   CODELST_SUBTYP, CODELST_DESC
        INTO   v_codesubtype, v_codedes
        FROM   ER_CODELST
       WHERE   PK_CODELST = P_STUDY_STAT_CODEFK;

      SP_POPULATE_MSG_ADDINFO (v_PKMSG,
                               'statusSubType',
                               v_codesubtype,
                               'CODELST_SUBTYP');
      SP_POPULATE_MSG_ADDINFO (v_PKMSG,
                               'statusCodeDesc',
                               v_codedes,
                               'CODELST_DESC');
   EXCEPTION
      WHEN OTHERS
      THEN
         Plog.FATAL (
            pCTX,
            'exception in PKG_MSG_QUEUE.SP_POPULATE_STUDY_STATUS_MSG '
            || SQLERRM
         );
   END SP_POPULATE_STUDY_STATUS_MSG;
END PKG_MSG_QUEUE;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,118,8100,'02_stored_proc.sql',sysdate,'8.10.0 eSP2.1 B#118');

COMMIT;
