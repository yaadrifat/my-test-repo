SET DEFINE OFF; 

CREATE OR REPLACE TRIGGER ESCH.SCH_BUDGET_AI1_OUT
AFTER INSERT
ON ESCH.SCH_BUDGET
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
fkaccount NUMBER;
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SVC_OUTBOUND', pLEVEL  => Plog.LFATAL);
/******************************************************************************
   NAME:       SCH_BUDGET_AI1_OUT
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        11/1/2011     Kanwal        1. Created this trigger.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     SCH_BUDGET_AI1_OUT
      Sysdate:         11/1/2011
      Date and Time:   11/1/2011, 10:02:07 AM, and 11/1/2011 10:02:07 AM
******************************************************************************/
BEGIN
    SELECT fk_account
  INTO fkaccount
  FROM er_user
  WHERE pk_user = NVL(:NEW.LAST_MODIFIED_BY, :NEW.CREATOR);

  ERES.PKG_MSG_QUEUE.SP_POPULATE_BUDGET_STATUS_MSG (
      :NEW.pk_budget,
      fkaccount,
      'I',
      :NEW.PK_BUDGET,
      :NEW.FK_CODELST_STATUS
   );
   EXCEPTION WHEN OTHERS THEN
   Plog.FATAL(pCTX,'exception in SCH_BUDGET_AI1_OUT ' || SQLERRM);
END SCH_BUDGET_AI1_OUT;
/

CREATE OR REPLACE TRIGGER ESCH.SCH_BUDGET_AU3_OUT
AFTER UPDATE
OF BUDGET_STATUS
ON ESCH.SCH_BUDGET REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
DECLARE
fkaccount NUMBER;
action char(1) := 'U'; 
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SVC_OUTBOUND', pLEVEL  => Plog.LFATAL);
BEGIN
  SELECT fk_account
  INTO fkaccount
  FROM er_user
  WHERE pk_user = NVL(:NEW.LAST_MODIFIED_BY, :NEW.CREATOR);
  
  IF(NVL(:NEW.BUDGET_DELFLAG, ' ')  = 'Y')
  THEN action:= 'D'; 
  END IF; 

  ERES.PKG_MSG_QUEUE.SP_POPULATE_BUDGET_STATUS_MSG (
      :NEW.pk_budget,
      fkaccount,
      action,
      :NEW.PK_BUDGET,
      :NEW.FK_CODELST_STATUS
   );
   EXCEPTION WHEN OTHERS THEN
   Plog.FATAL(pCTX,'exception in SCH_BUDGET_AU3_OUT ' || SQLERRM);
END SCH_BUDGET_AU3_OUT;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,126,8101,'01_triggers.sql',sysdate,'8.10.1 eSP3.2 B#141');

COMMIT;