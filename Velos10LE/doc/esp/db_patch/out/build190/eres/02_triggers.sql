set define off;


CREATE OR REPLACE TRIGGER ERES.ER_ACCTFORMS_AI0_OUT
AFTER INSERT
ON ERES.ER_ACCTFORMS 
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
DECLARE
fkaccount NUMBER;
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SVC_OUTBOUND', pLEVEL  => Plog.LFATAL);

BEGIN
 /* Author : Raman */
 SELECT fk_account
  INTO fkaccount
  FROM er_user
  WHERE pk_user = :NEW.CREATOR; 

 PKG_MSG_QUEUE.SP_POPULATE_FORMRES_STATUS_MSG('ER_ACCTFORMS',
                                                :NEW.PK_ACCTFORMS,
                                               fkaccount,
                                               'I',
                                               to_char(:NEW.FK_FORMLIB),
                                               :NEW.FORM_COMPLETED );   
  EXCEPTION
     WHEN OTHERS THEN
       Plog.FATAL(pCTX,'exception in ERES.ER_ACCTFORMS_AI0_OUT ' || SQLERRM); 
END ER_ACCTFORMS_AI0_OUT;
/

--------------------------------------------------------------------------------

CREATE OR REPLACE TRIGGER ERES.ER_ACCTFORMS_AU2_OUT
AFTER UPDATE
OF FORM_COMPLETED
ON ERES.ER_ACCTFORMS 
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
DECLARE
fkaccount NUMBER;
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SVC_OUTBOUND', pLEVEL  => Plog.LFATAL);

BEGIN
 /* Author : Raman */
 SELECT fk_account
  INTO fkaccount
  FROM er_user
  WHERE pk_user = :NEW.CREATOR; 

 PKG_MSG_QUEUE.SP_POPULATE_FORMRES_STATUS_MSG('ER_ACCTFORMS',
                                                :NEW.PK_ACCTFORMS,
                                               fkaccount,
                                               'U',
                                               to_char(:NEW.FK_FORMLIB),
                                               :NEW.FORM_COMPLETED );   
  EXCEPTION
     WHEN OTHERS THEN
       Plog.FATAL(pCTX,'exception in ERES.ER_ACCTFORMS_AU2_OUT ' || SQLERRM);
END ER_ACCTFORMS_AU2_OUT;
/

--------------------------------------------------------------------------------

CREATE OR REPLACE TRIGGER ERES.ER_PATFORMS_AI0_OUT
AFTER INSERT
ON ERES.ER_PATFORMS 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
fkaccount NUMBER;
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SVC_OUTBOUND', pLEVEL  => Plog.LFATAL);

BEGIN
 /* Author : Raman */
 SELECT fk_account
  INTO fkaccount
  FROM er_user
  WHERE pk_user = :NEW.CREATOR; 

 PKG_MSG_QUEUE.SP_POPULATE_FORMRES_STATUS_MSG('ER_PATFORMS',
                                                :NEW.PK_PATFORMS,
                                               fkaccount,
                                               'I',
                                               to_char(:NEW.FK_FORMLIB),
                                               :NEW.FORM_COMPLETED );   
  EXCEPTION
     WHEN OTHERS THEN
       Plog.FATAL(pCTX,'exception in ERES.ER_PATFORMS_AI0__OUT ' || SQLERRM); 
     
END ;
/

-----------------------------------------------------------------------------------------

CREATE OR REPLACE TRIGGER ERES.ER_PATFORMS_AU2_OUT
AFTER UPDATE
OF FORM_COMPLETED
ON ERES.ER_PATFORMS 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
fkaccount NUMBER;
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SVC_OUTBOUND', pLEVEL  => Plog.LFATAL);

BEGIN
 /* Author : Raman */
 SELECT fk_account
  INTO fkaccount
  FROM er_user
  WHERE pk_user = :NEW.CREATOR; 

 PKG_MSG_QUEUE.SP_POPULATE_FORMRES_STATUS_MSG('ER_PATFORMS',
                                                :NEW.PK_PATFORMS,
                                               fkaccount,
                                               'U',
                                               to_char(:NEW.FK_FORMLIB),
                                               :NEW.FORM_COMPLETED );   
  EXCEPTION
     WHEN OTHERS THEN
       Plog.FATAL(pCTX,'exception in ERES.ER_PATFORMS_AU2__OUT ' || SQLERRM);
END ER_PATFORMS_AU0_OUT;
/

-------------------------------------------------------------------------------------------
CREATE OR REPLACE TRIGGER ERES."ER_STUDYFORMS_AI0_OUT" AFTER INSERT
ON ERES.ER_STUDYFORMS 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
fkaccount NUMBER;
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SVC_OUTBOUND', pLEVEL  => Plog.LFATAL);

BEGIN
 /* Author : Raman */
 SELECT fk_account
  INTO fkaccount
  FROM er_user
  WHERE pk_user = :NEW.CREATOR; 

 PKG_MSG_QUEUE.SP_POPULATE_FORMRES_STATUS_MSG('ER_STUDYFORMS',
                                                :NEW.PK_STUDYFORMS,
                                               fkaccount,
                                               'I',
                                               to_char(:NEW.FK_FORMLIB),
                                               :NEW.FORM_COMPLETED );   
  EXCEPTION
     WHEN OTHERS THEN
       Plog.FATAL(pCTX,'exception in ERES.ER_STUDYFORMS_AI0_OUT ' || SQLERRM); 
END ER_ER_STUDYFORMS_AI0_OUT;
/
--------------------------------------------------------------------------------------------

CREATE OR REPLACE TRIGGER ERES.ER_STUDYFORMS_AU2_OUT
AFTER UPDATE
OF FORM_COMPLETED
ON ERES.ER_STUDYFORMS 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
fkaccount NUMBER;
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SVC_OUTBOUND', pLEVEL  => Plog.LFATAL);

BEGIN
 /* Author : Raman */
 SELECT fk_account
  INTO fkaccount
  FROM er_user
  WHERE pk_user = :NEW.CREATOR; 

 PKG_MSG_QUEUE.SP_POPULATE_FORMRES_STATUS_MSG('ER_STUDYFORMS',
                                                :NEW.PK_STUDYFORMS,
                                               fkaccount,
                                               'U',
                                               to_char(:NEW.FK_FORMLIB),
                                               :NEW.FORM_COMPLETED );   
  EXCEPTION
     WHEN OTHERS THEN
       Plog.FATAL(pCTX,'exception in ERES.ER_STUDYFORMS_AU2_OUT ' || SQLERRM);

END ER_STUDYFORMS_AU0_OUT;
/
-------------------------------------------------------------------------------------------------

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,201,910,'02_triggers.sql',sysdate,'9.1.0 eSP4.1.1 B#190');

COMMIT;