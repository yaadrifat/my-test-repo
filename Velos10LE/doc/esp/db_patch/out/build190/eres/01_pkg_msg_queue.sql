set define off;
------------------------------------------------------
CREATE OR REPLACE PACKAGE ERES."PKG_MSG_QUEUE"
AS
   MODULE_STUDY_STATUS      VARCHAR2 (200) := 'study_status';
   MODULE_CALENDAR_STATUS   VARCHAR2 (200) := 'calendar_status';
   MODULE_BUDGET_STATUS     VARCHAR2 (200) := 'budget_status';
   MODULE_STUDY_TEAM        VARCHAR2 (200) := 'study_team';
   MODULE_STUDY_SUMMARY     VARCHAR2 (200) := 'study_summary';
   MODULE_FORM_STATUS       VARCHAR2 (200) := 'form_status';

   pCTX Plog.LOG_CTX
         := Plog.init (pSECTION => 'SVC_OUTBOUND', pLEVEL => Plog.LFATAL) ;

    PROCEDURE SP_POPULATE_MSG_QUEUE (
      P_TABLE_NAME          ER_MSG_QUEUE.TABLENAME%TYPE,
      P_TABLE_PK            ER_MSG_QUEUE.TABLE_PK%TYPE,
      P_FK_ACCOUNT          ER_MSG_QUEUE.FK_ACCOUNT%TYPE,
      P_ACTION              ER_MSG_QUEUE.ACTION%TYPE,
      P_ROOT_PK             ER_MSG_QUEUE.ROOT_PK%TYPE,
      P_ROOT_TABLENAME      ER_MSG_QUEUE.ROOT_TABLENAME%TYPE,
      O_PK_MESSAGE   OUT  ER_MSG_QUEUE.PK_MESSAGE%TYPE
    );

    PROCEDURE SP_POPULATE_MSG_ADDINFO (
      P_FK_MESSAGE        ER_MSG_QUEUE.PK_MESSAGE%TYPE,
      P_ELEMENT_NAME      ER_MSG_ADDINFO.ELEMENT_NAME%TYPE,
      P_ELEMENT_VALUE     ER_MSG_ADDINFO.ELEMENT_VALUE%TYPE,
      P_COL_NAME          ER_MSG_ADDINFO.COL_NAME%TYPE
    );

    PROCEDURE SP_POPULATE_STUDY_STATUS_MSG (
      P_TABLE_PK              ER_MSG_QUEUE.TABLE_PK%TYPE,
      P_FK_ACCOUNT            ER_MSG_QUEUE.FK_ACCOUNT%TYPE,
      P_ACTION                ER_MSG_QUEUE.ACTION%TYPE,
      P_ROOT_PK               ER_MSG_QUEUE.ROOT_PK%TYPE,
      P_STUDY_STAT_CODEFK     ER_CODELST.PK_CODELST%TYPE
    );

     PROCEDURE SP_POPULATE_STUDY_SUMMARY_MSG (
       P_TABLE_PK              ER_MSG_QUEUE.TABLE_PK%TYPE,
       P_FK_ACCOUNT            ER_MSG_QUEUE.FK_ACCOUNT%TYPE,
       P_ACTION                ER_MSG_QUEUE.ACTION%TYPE
    );

    PROCEDURE SP_POPULATE_CAL_STATUS_MSG (
      P_TABLE_PK              ER_MSG_QUEUE.TABLE_PK%TYPE,
      P_FK_ACCOUNT            ER_MSG_QUEUE.FK_ACCOUNT%TYPE,
      P_ACTION                ER_MSG_QUEUE.ACTION%TYPE,
      P_ROOT_PK               ER_MSG_QUEUE.ROOT_PK%TYPE,
      P_CAL_STAT_CODEFK       EVENT_ASSOC.FK_CODELST_CALSTAT%TYPE
    );

   PROCEDURE SP_POPULATE_STUDYID_MSG(
        P_TABLE_PK              ER_MSG_QUEUE.TABLE_PK%TYPE,
        P_FK_ACCOUNT            ER_MSG_QUEUE.FK_ACCOUNT%TYPE,
        P_ACTION                ER_MSG_QUEUE.ACTION%TYPE,
        P_ROOT_PK               ER_MSG_QUEUE.ROOT_PK%TYPE,
        P_CODELST_IDTYPEFK      ER_STUDYID.FK_CODELST_IDTYPE%TYPE
   );

    PROCEDURE SP_POPULATE_STUDYTEAM_MSG (
        P_TABLE_PK              ER_MSG_QUEUE.TABLE_PK%TYPE,
        P_FK_ACCOUNT            ER_MSG_QUEUE.FK_ACCOUNT%TYPE,
        P_ACTION                ER_MSG_QUEUE.ACTION%TYPE,
        P_ROOT_PK               ER_MSG_QUEUE.ROOT_PK%TYPE,
        P_USER_PK               ER_USER.PK_USER%TYPE,
        P_CODELST_TMROLEFK      ER_STUDYTEAM.FK_CODELST_TMROLE%TYPE
   );

   PROCEDURE SP_POPULATE_BUDGET_STATUS_MSG (
        P_TABLE_PK              ER_MSG_QUEUE.TABLE_PK%TYPE,
        P_FK_ACCOUNT            ER_MSG_QUEUE.FK_ACCOUNT%TYPE,
        P_ACTION                ER_MSG_QUEUE.ACTION%TYPE,
        P_ROOT_PK               ER_MSG_QUEUE.ROOT_PK%TYPE,
        P_CAL_STAT_CODEFK       SCH_CODELST.PK_CODELST%TYPE
   );

   PROCEDURE SP_POPULATE_FORMRES_STATUS_MSG (
        P_TABLE_NAME          ER_MSG_QUEUE.TABLENAME%TYPE,
        P_TABLE_PK              ER_MSG_QUEUE.TABLE_PK%TYPE,
        P_FK_ACCOUNT            ER_MSG_QUEUE.FK_ACCOUNT%TYPE,
        P_ACTION                ER_MSG_QUEUE.ACTION%TYPE,
        P_ROOT_PK               ER_MSG_QUEUE.ROOT_PK%TYPE,
        P_FORMRES_STATUS        ERES.ER_FORMSLINEAR.FORM_COMPLETED%TYPE
   );

END PKG_MSG_QUEUE;
/
------------------------------------------------------
CREATE OR REPLACE PACKAGE BODY ERES."PKG_MSG_QUEUE"
AS
   PROCEDURE SP_POPULATE_MSG_QUEUE (
      P_TABLE_NAME          ER_MSG_QUEUE.TABLENAME%TYPE,
      P_TABLE_PK            ER_MSG_QUEUE.TABLE_PK%TYPE,
      P_FK_ACCOUNT          ER_MSG_QUEUE.FK_ACCOUNT%TYPE,
      P_ACTION              ER_MSG_QUEUE.ACTION%TYPE,
      P_ROOT_PK             ER_MSG_QUEUE.ROOT_PK%TYPE,
      P_ROOT_TABLENAME      ER_MSG_QUEUE.ROOT_TABLENAME%TYPE,
      O_PK_MESSAGE   OUT    ER_MSG_QUEUE.PK_MESSAGE%TYPE
   )
   AS
      V_MODULE   ER_MSG_QUEUE.MODULE%TYPE;
   BEGIN
      SELECT   CASE LOWER (P_TABLE_NAME)
                  WHEN 'er_studystat' THEN MODULE_STUDY_STATUS
                  WHEN 'event_assoc' THEN MODULE_CALENDAR_STATUS
                  WHEN 'sch_budget' THEN MODULE_BUDGET_STATUS
                  WHEN 'er_studyteam' THEN MODULE_STUDY_TEAM
                  WHEN 'er_study' THEN MODULE_STUDY_SUMMARY
                  WHEN 'er_studyid' THEN MODULE_STUDY_SUMMARY
                  WHEN 'er_patforms' THEN MODULE_FORM_STATUS
               END

        INTO   V_MODULE
        FROM   DUAL;

      SELECT   SEQ_ER_MSG_QUEUE.NEXTVAL INTO O_PK_MESSAGE FROM DUAL;

      INSERT INTO er_msg_queue (pk_message,
                                tablename,
                                table_pk,
                                module,
                                fk_account,
                                processed_flag,
                                action,
                                root_pk,
                                root_tablename)
        VALUES   (O_PK_MESSAGE,
                  P_TABLE_NAME,
                  P_TABLE_PK,
                  V_MODULE,
                  P_FK_ACCOUNT,
                  0,
                  P_ACTION,
                  P_ROOT_PK ,
                  P_ROOT_TABLENAME);
   EXCEPTION
      WHEN OTHERS
      THEN
         Plog.FATAL (
            pCTX,
            'exception in PKG_MSG_QUEUE.SP_POPULATE_MSG_QUEUE ' || SQLERRM
         );
   END SP_POPULATE_MSG_QUEUE;

   PROCEDURE SP_POPULATE_MSG_ADDINFO (
        P_FK_MESSAGE       ER_MSG_QUEUE.PK_MESSAGE%TYPE,
        P_ELEMENT_NAME     ER_MSG_ADDINFO.ELEMENT_NAME%TYPE,
        P_ELEMENT_VALUE    ER_MSG_ADDINFO.ELEMENT_VALUE%TYPE,
        P_COL_NAME         ER_MSG_ADDINFO.COL_NAME%TYPE
   )
   AS
   BEGIN
      INSERT INTO er_msg_addinfo (PK_MSG_ADDINFO,
                                  FK_MSG_QUEUE,
                                  ELEMENT_NAME,
                                  ELEMENT_VALUE,
                                  COL_NAME)
        VALUES   (SEQ_ER_MSG_ADDINFO.NEXTVAL,
                  P_FK_MESSAGE,
                  P_ELEMENT_NAME,
                  P_ELEMENT_VALUE,
                  P_COL_NAME);
   EXCEPTION
      WHEN OTHERS
      THEN
         Plog.FATAL (
            pCTX,
            'exception in PKG_MSG_QUEUE.SP_POPULATE_MSG_ADDINFO ' || SQLERRM
         );
   END SP_POPULATE_MSG_ADDINFO;

   PROCEDURE SP_POPULATE_STUDY_STATUS_MSG (
        P_TABLE_PK              ER_MSG_QUEUE.TABLE_PK%TYPE,
        P_FK_ACCOUNT            ER_MSG_QUEUE.FK_ACCOUNT%TYPE,
        P_ACTION                ER_MSG_QUEUE.ACTION%TYPE,
        P_ROOT_PK               ER_MSG_QUEUE.ROOT_PK%TYPE,
        P_STUDY_STAT_CODEFK     ER_CODELST.PK_CODELST%TYPE
   )
   AS
      v_PKMSG           ER_MSG_QUEUE.PK_MESSAGE%TYPE;
      v_codesubtype     ER_CODELST.CODELST_SUBTYP%TYPE;
      v_codedes         ER_CODELST.CODELST_DESC%TYPE;
      v_TABLENAME       ER_MSG_QUEUE.TABLENAME%TYPE := 'er_studystat';
      v_ROOTTABLE       ER_MSG_QUEUE.ROOT_TABLENAME%TYPE := 'er_study';
   BEGIN
      SP_POPULATE_MSG_QUEUE (v_TABLENAME,
                             P_TABLE_PK,
                             P_FK_ACCOUNT,
                             P_ACTION,
                             P_ROOT_PK,
                             v_ROOTTABLE,
                             v_PKMSG);

      SELECT   CODELST_SUBTYP, CODELST_DESC
        INTO   v_codesubtype, v_codedes
        FROM   ER_CODELST
       WHERE   PK_CODELST = P_STUDY_STAT_CODEFK;

      SP_POPULATE_MSG_ADDINFO (v_PKMSG,
                               'statusSubType',
                               v_codesubtype,
                               'CODELST_SUBTYP');
      SP_POPULATE_MSG_ADDINFO (v_PKMSG,
                               'statusCodeDesc',
                               v_codedes,
                               'CODELST_DESC');
   EXCEPTION
      WHEN OTHERS
      THEN
         Plog.FATAL (
            pCTX,
            'exception in PKG_MSG_QUEUE.SP_POPULATE_STUDY_STATUS_MSG '
            || SQLERRM
         );
   END SP_POPULATE_STUDY_STATUS_MSG;


   PROCEDURE SP_POPULATE_STUDY_SUMMARY_MSG (
        P_TABLE_PK              ER_MSG_QUEUE.TABLE_PK%TYPE,
        P_FK_ACCOUNT            ER_MSG_QUEUE.FK_ACCOUNT%TYPE,
        P_ACTION                ER_MSG_QUEUE.ACTION%TYPE
           )
   AS
      v_PKMSG           ER_MSG_QUEUE.PK_MESSAGE%TYPE;
      v_codesubtype     ER_CODELST.CODELST_SUBTYP%TYPE;
      v_codedes         ER_CODELST.CODELST_DESC%TYPE;
      v_TABLENAME       ER_MSG_QUEUE.TABLENAME%TYPE := 'er_study';
      v_ROOTTABLE       ER_MSG_QUEUE.ROOT_TABLENAME%TYPE := 'er_study';
   BEGIN
      SP_POPULATE_MSG_QUEUE (v_TABLENAME,
                             P_TABLE_PK,
                             P_FK_ACCOUNT,
                             P_ACTION,
                             P_TABLE_PK,
                             v_ROOTTABLE,
                             v_PKMSG);


   EXCEPTION
      WHEN OTHERS
      THEN
         Plog.FATAL (
            pCTX,
            'exception in PKG_MSG_QUEUE.SP_POPULATE_STUDY_SUMMARY_MSG '
            || SQLERRM
         );
   END SP_POPULATE_STUDY_SUMMARY_MSG;

    PROCEDURE SP_POPULATE_CAL_STATUS_MSG (
        P_TABLE_PK              ER_MSG_QUEUE.TABLE_PK%TYPE,
        P_FK_ACCOUNT            ER_MSG_QUEUE.FK_ACCOUNT%TYPE,
        P_ACTION                ER_MSG_QUEUE.ACTION%TYPE,
        P_ROOT_PK               ER_MSG_QUEUE.ROOT_PK%TYPE,
        P_CAL_STAT_CODEFK       EVENT_ASSOC.FK_CODELST_CALSTAT%TYPE
   )
   AS
      v_PKMSG           ER_MSG_QUEUE.PK_MESSAGE%TYPE;
      v_codesubtype     SCH_CODELST.CODELST_SUBTYP%TYPE;
      v_codedes         SCH_CODELST.CODELST_DESC%TYPE;
      v_TABLENAME       ER_MSG_QUEUE.TABLENAME%TYPE := 'event_assoc';
      v_ROOTTABLE       ER_MSG_QUEUE.ROOT_TABLENAME%TYPE := 'event_assoc';
   BEGIN
      SP_POPULATE_MSG_QUEUE (v_TABLENAME,
                             P_TABLE_PK,
                             P_FK_ACCOUNT,
                             P_ACTION,
                             P_ROOT_PK,
                             v_ROOTTABLE,
                             v_PKMSG);

      SELECT   CODELST_SUBTYP, CODELST_DESC
        INTO   v_codesubtype, v_codedes
        FROM   SCH_CODELST
       WHERE   PK_CODELST = P_CAL_STAT_CODEFK;

      SP_POPULATE_MSG_ADDINFO (v_PKMSG,
                               'statusSubType',
                               v_codesubtype,
                               'CODELST_SUBTYP');
      SP_POPULATE_MSG_ADDINFO (v_PKMSG,
                               'statusCodeDesc',
                               v_codedes,
                               'CODELST_DESC');
   EXCEPTION
      WHEN OTHERS
      THEN
         Plog.FATAL (
            pCTX,
            'exception in PKG_MSG_QUEUE.SP_POPULATE_CAL_STATUS_MSG'
            || SQLERRM
         );
   END SP_POPULATE_CAL_STATUS_MSG;


PROCEDURE SP_POPULATE_STUDYID_MSG(
        P_TABLE_PK              ER_MSG_QUEUE.TABLE_PK%TYPE,
        P_FK_ACCOUNT            ER_MSG_QUEUE.FK_ACCOUNT%TYPE,
        P_ACTION                ER_MSG_QUEUE.ACTION%TYPE,
        P_ROOT_PK               ER_MSG_QUEUE.ROOT_PK%TYPE,
           P_CODELST_IDTYPEFK      ER_STUDYID.FK_CODELST_IDTYPE%TYPE
   )
   AS
      v_PKMSG           ER_MSG_QUEUE.PK_MESSAGE%TYPE;
      v_codesubtype     ER_CODELST.CODELST_SUBTYP%TYPE;
      v_codedes         ER_CODELST.CODELST_DESC%TYPE;
      v_TABLENAME       ER_MSG_QUEUE.TABLENAME%TYPE := 'er_studyid';
      v_ROOTTABLE       ER_MSG_QUEUE.ROOT_TABLENAME%TYPE := 'er_study';

   BEGIN
      SP_POPULATE_MSG_QUEUE (v_TABLENAME,
                             P_TABLE_PK,
                             P_FK_ACCOUNT,
                             P_ACTION,
                             P_ROOT_PK,
                             v_ROOTTABLE,
                             v_PKMSG);

   EXCEPTION
      WHEN OTHERS
      THEN
         Plog.FATAL (
            pCTX,
            'exception in PKG_MSG_QUEUE.SP_POPULATE_STUDYID_MSG '
            || SQLERRM
         );
   END SP_POPULATE_STUDYID_MSG;


    PROCEDURE SP_POPULATE_STUDYTEAM_MSG (
        P_TABLE_PK              ER_MSG_QUEUE.TABLE_PK%TYPE,
        P_FK_ACCOUNT            ER_MSG_QUEUE.FK_ACCOUNT%TYPE,
        P_ACTION                ER_MSG_QUEUE.ACTION%TYPE,
        P_ROOT_PK               ER_MSG_QUEUE.ROOT_PK%TYPE,
        P_USER_PK               ER_USER.PK_USER%TYPE,
        P_CODELST_TMROLEFK     ER_STUDYTEAM.FK_CODELST_TMROLE%TYPE
   )
   AS
      v_PKMSG           ER_MSG_QUEUE.PK_MESSAGE%TYPE;
      v_TABLENAME       ER_MSG_QUEUE.TABLENAME%TYPE := 'er_studyteam';
      v_ROOTTABLE       ER_MSG_QUEUE.ROOT_TABLENAME%TYPE := 'er_study';
      v_codesubtype     ER_CODELST.CODELST_SUBTYP%TYPE;
      v_codedes         ER_CODELST.CODELST_DESC%TYPE;

   BEGIN
      SP_POPULATE_MSG_QUEUE (v_TABLENAME,
                             P_TABLE_PK,
                             P_FK_ACCOUNT,
                             P_ACTION,
                             P_ROOT_PK,
                             v_ROOTTABLE,
                             v_PKMSG);

      SP_POPULATE_MSG_ADDINFO (v_PKMSG,
                               'UserIdentifier',
                               P_USER_PK,
                               'FK_USER');
      SELECT   CODELST_SUBTYP, CODELST_DESC
        INTO   v_codesubtype, v_codedes
        FROM   ER_CODELST
       WHERE   PK_CODELST = P_CODELST_TMROLEFK;

      SP_POPULATE_MSG_ADDINFO (v_PKMSG,
                               'teamRoleType',
                               v_codesubtype,
                               'CODELST_SUBTYP');
      SP_POPULATE_MSG_ADDINFO (v_PKMSG,
                               'teamRoleDesc',
                               v_codedes,
                               'CODELST_DESC');


   EXCEPTION
      WHEN OTHERS
      THEN
         Plog.FATAL (
            pCTX,
            'exception in PKG_MSG_QUEUE.SP_POPULATE_STUDYTEAM_MSG'
            || SQLERRM
         );
   END SP_POPULATE_STUDYTEAM_MSG;

   PROCEDURE SP_POPULATE_BUDGET_STATUS_MSG (
        P_TABLE_PK              ER_MSG_QUEUE.TABLE_PK%TYPE,
        P_FK_ACCOUNT            ER_MSG_QUEUE.FK_ACCOUNT%TYPE,
        P_ACTION                ER_MSG_QUEUE.ACTION%TYPE,
        P_ROOT_PK               ER_MSG_QUEUE.ROOT_PK%TYPE,
        P_CAL_STAT_CODEFK       SCH_CODELST.PK_CODELST%TYPE
   )
   AS
      v_PKMSG           ER_MSG_QUEUE.PK_MESSAGE%TYPE;
      v_codesubtype     SCH_CODELST.CODELST_SUBTYP%TYPE;
      v_codedes         SCH_CODELST.CODELST_DESC%TYPE;
      v_TABLENAME       ER_MSG_QUEUE.TABLENAME%TYPE := 'sch_budget';
      v_ROOTTABLE       ER_MSG_QUEUE.ROOT_TABLENAME%TYPE := 'sch_budget';
   BEGIN
      SP_POPULATE_MSG_QUEUE (v_TABLENAME,
                             P_TABLE_PK,
                             P_FK_ACCOUNT,
                             P_ACTION,
                             P_ROOT_PK,
                             v_ROOTTABLE,
                             v_PKMSG);

      SELECT   CODELST_SUBTYP, CODELST_DESC
        INTO   v_codesubtype, v_codedes
        FROM   SCH_CODELST
       WHERE   PK_CODELST = P_CAL_STAT_CODEFK;

      SP_POPULATE_MSG_ADDINFO (v_PKMSG,
                               'statusSubType',
                               v_codesubtype,
                               'CODELST_SUBTYP');
      SP_POPULATE_MSG_ADDINFO (v_PKMSG,
                               'statusCodeDesc',
                               v_codedes,
                               'CODELST_DESC');
   EXCEPTION
      WHEN OTHERS
      THEN
          Plog.FATAL (
            pCTX,
            'exception in PKG_MSG_QUEUE.SP_POPULATE_BUDGET_STATUS_MSG'
            || SQLERRM
         );
   END SP_POPULATE_BUDGET_STATUS_MSG;
   
   PROCEDURE SP_POPULATE_FORMRES_STATUS_MSG (
        P_TABLE_NAME          ER_MSG_QUEUE.TABLENAME%TYPE,
        P_TABLE_PK              ER_MSG_QUEUE.TABLE_PK%TYPE,
        P_FK_ACCOUNT            ER_MSG_QUEUE.FK_ACCOUNT%TYPE,
        P_ACTION                ER_MSG_QUEUE.ACTION%TYPE,
        P_ROOT_PK               ER_MSG_QUEUE.ROOT_PK%TYPE,
        P_FORMRES_STATUS        ERES.ER_FORMSLINEAR.FORM_COMPLETED%TYPE
   )
   AS
      v_PKMSG           ER_MSG_QUEUE.PK_MESSAGE%TYPE;
      v_codesubtype     SCH_CODELST.CODELST_SUBTYP%TYPE;
      v_codedes         SCH_CODELST.CODELST_DESC%TYPE;
      v_lfdisplaytype   ER_LINKEDFORMS.LF_DISPLAYTYPE%TYPE;
      v_displaytype     VARCHAR2 (200);
      v_ROOTTABLE       ER_MSG_QUEUE.ROOT_TABLENAME%TYPE := 'er_formlib';
   BEGIN
      SP_POPULATE_MSG_QUEUE (P_TABLE_NAME,
                             P_TABLE_PK,
                             P_FK_ACCOUNT,
                             P_ACTION,
                             P_ROOT_PK,
                             v_ROOTTABLE,
                             v_PKMSG);
    SELECT   CODELST_SUBTYP, CODELST_DESC
        INTO   v_codesubtype, v_codedes
        FROM   ER_CODELST
       WHERE   PK_CODELST = P_FORMRES_STATUS;
    
    SP_POPULATE_MSG_ADDINFO (v_PKMSG,
                               'statusSubType',
                               v_codesubtype,
                               'CODELST_SUBTYP'); 
    SP_POPULATE_MSG_ADDINFO (v_PKMSG,
                               'statusCodeDesc',
                               v_codedes,
                               'CODELST_DESC');
    SELECT LF_DISPLAYTYPE
        INTO v_lfdisplaytype
        FROM ER_LINKEDFORMS
        WHERE FK_FORMLIB = P_ROOT_PK;
    
    SELECT   CASE LOWER (v_lfdisplaytype)
                  WHEN 's'  THEN  'STUDY LEVEL'
                  WHEN 'sp' THEN  'SPECIFIC STUDY PATIENT LEVEL'
                  WHEN 'a'  THEN  'ACCOUNT LEVEL'
                  WHEN 'sa' THEN  'STUDY LEVEL (ALL)'
                  WHEN 'pa' THEN  'PATIENT LEVEL (ALL)'
                  WHEN 'c'  THEN  'CRF'
                  WHEN 'ps' THEN  'PATIENT LEVEL (ALL STUDIES)'
                  WHEN 'pr' THEN  'PATIENT LEVEL (ALL STUDIES-RESTRICTED)'
             END
        INTO   v_displaytype
        FROM   DUAL;
        
    SP_POPULATE_MSG_ADDINFO (v_PKMSG,
                               'formType',
                               v_displaytype,
                               'LF_DISPLAYTYPE');
        
        
        
                                
   EXCEPTION
      WHEN OTHERS
      THEN
          Plog.FATAL (pCTX,'exception in PKG_MSG_QUEUE.SP_POPULATE_FORMRES_STATUS_MSG'|| SQLERRM);
   END SP_POPULATE_FORMRES_STATUS_MSG;



END PKG_MSG_QUEUE;
/

-------------------------------------------------------
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,201,910,'01_pkg_msg_queue.sql',sysdate,'9.1.0 eSP4.1.1 B#190');

COMMIT;
