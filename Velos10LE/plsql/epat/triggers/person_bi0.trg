CREATE  OR REPLACE TRIGGER PERSON_BI0 BEFORE INSERT ON PERSON        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'PERSON', erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CAUSE_OF_DEATH_OTHER', :NEW.CAUSE_OF_DEATH_OTHER);
       Audit_Trail.column_insert (raid, 'CREATED_ON', :NEW.CREATED_ON);
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
       Audit_Trail.column_insert (raid, 'FK_ACCOUNT', :NEW.FK_ACCOUNT);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_BLOODGRP;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_BLOODGRP', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_CITIZEN;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_CITIZEN', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_EDU;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_EDU', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_EMPLOYMENT;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_EMPLOYMENT', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_ETHNICITY;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_ETHNICITY', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_GENDER;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_GENDER', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_MARITAL;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_MARITAL', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_NATIONALITY;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_NATIONALITY', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_PAT_DTH_CAUSE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_PAT_DTH_CAUSE', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_PRILANG;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_PRILANG', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_PSTAT;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_PSTAT', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_RACE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_RACE', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_RELIGION;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_RELIGION', codeList_desc);
       Audit_Trail.column_insert (raid, 'FK_CODLST_NTYPE', :NEW.FK_CODLST_NTYPE);
       Audit_Trail.column_insert (raid, 'FK_PERSON_MOTHER', :NEW.FK_PERSON_MOTHER);
       Audit_Trail.column_insert (raid, 'FK_SITE', :NEW.FK_SITE);
       Audit_Trail.column_insert (raid, 'FK_TIMEZONE', :NEW.FK_TIMEZONE);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', :NEW.LAST_MODIFIED_DATE);
       Audit_Trail.column_insert (raid, 'PAT_FACILITYID', :NEW.PAT_FACILITYID);
       Audit_Trail.column_insert (raid, 'PERSON_ADDRESS1', :NEW.PERSON_ADDRESS1);
       Audit_Trail.column_insert (raid, 'PERSON_ADDRESS2', :NEW.PERSON_ADDRESS2);
       Audit_Trail.column_insert (raid, 'PERSON_ADD_ETHNICITY', :NEW.PERSON_ADD_ETHNICITY);
       Audit_Trail.column_insert (raid, 'PERSON_ADD_RACE', :NEW.PERSON_ADD_RACE);
       Audit_Trail.column_insert (raid, 'PERSON_AKA', :NEW.PERSON_AKA);
       Audit_Trail.column_insert (raid, 'PERSON_ALTID', :NEW.PERSON_ALTID);
       Audit_Trail.column_insert (raid, 'PERSON_BIRTH_PLACE', :NEW.PERSON_BIRTH_PLACE);
       Audit_Trail.column_insert (raid, 'PERSON_BPHONE', :NEW.PERSON_BPHONE);
       Audit_Trail.column_insert (raid, 'PERSON_CITY', :NEW.PERSON_CITY);
       Audit_Trail.column_insert (raid, 'PERSON_CODE', :NEW.PERSON_CODE);
       Audit_Trail.column_insert (raid, 'PERSON_COUNTRY', :NEW.PERSON_COUNTRY);
       Audit_Trail.column_insert (raid, 'PERSON_COUNTY', :NEW.PERSON_COUNTY);
       Audit_Trail.column_insert (raid, 'PERSON_DEATHDT', :NEW.PERSON_DEATHDT);
       Audit_Trail.column_insert (raid, 'PERSON_DEGREE', :NEW.PERSON_DEGREE);
       Audit_Trail.column_insert (raid, 'PERSON_DOB', :NEW.PERSON_DOB);
       Audit_Trail.column_insert (raid, 'PERSON_DRIV_LIC', :NEW.PERSON_DRIV_LIC);
       Audit_Trail.column_insert (raid, 'PERSON_EMAIL', :NEW.PERSON_EMAIL);
       Audit_Trail.column_insert (raid, 'PERSON_ETHGRP', :NEW.PERSON_ETHGRP);
       Audit_Trail.column_insert (raid, 'PERSON_FNAME', :NEW.PERSON_FNAME);
       Audit_Trail.column_insert (raid, 'PERSON_HPHONE', :NEW.PERSON_HPHONE);
       Audit_Trail.column_insert (raid, 'PERSON_LNAME', :NEW.PERSON_LNAME);
       Audit_Trail.column_insert (raid, 'PERSON_MILVET', :NEW.PERSON_MILVET);
       Audit_Trail.column_insert (raid, 'PERSON_MNAME', :NEW.PERSON_MNAME);
       Audit_Trail.column_insert (raid, 'PERSON_MOTHER_NAME', :NEW.PERSON_MOTHER_NAME);
       Audit_Trail.column_insert (raid, 'PERSON_MULTI_BIRTH', :NEW.PERSON_MULTI_BIRTH);
       Audit_Trail.column_insert (raid, 'PERSON_NOTES', :NEW.PERSON_NOTES);
       Audit_Trail.column_insert (raid, 'PERSON_NOTES_CLOB', :NEW.PERSON_NOTES_CLOB);
       Audit_Trail.column_insert (raid, 'PERSON_ORGOTHER', :NEW.PERSON_ORGOTHER);
       Audit_Trail.column_insert (raid, 'PERSON_PHYOTHER', :NEW.PERSON_PHYOTHER);
       Audit_Trail.column_insert (raid, 'PERSON_PREFIX', :NEW.PERSON_PREFIX);
       Audit_Trail.column_insert (raid, 'PERSON_REGBY', :NEW.PERSON_REGBY);
       Audit_Trail.column_insert (raid, 'PERSON_REGDATE', :NEW.PERSON_REGDATE);
       Audit_Trail.column_insert (raid, 'PERSON_SPLACCESS', :NEW.PERSON_SPLACCESS);
       Audit_Trail.column_insert (raid, 'PERSON_SSN', :NEW.PERSON_SSN);
       Audit_Trail.column_insert (raid, 'PERSON_STATE', :NEW.PERSON_STATE);
       Audit_Trail.column_insert (raid, 'PERSON_SUFFIX', :NEW.PERSON_SUFFIX);
       Audit_Trail.column_insert (raid, 'PERSON_ZIP', :NEW.PERSON_ZIP);
       Audit_Trail.column_insert (raid, 'PK_PERSON', :NEW.PK_PERSON);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/

