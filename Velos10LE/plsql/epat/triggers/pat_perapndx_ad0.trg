CREATE  OR REPLACE TRIGGER PAT_PERAPNDX_AD0 AFTER DELETE ON PAT_PERAPNDX        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'PAT_PERAPNDX', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr );
       Audit_Trail.column_delete (raid, 'FK_PER', :OLD.FK_PER);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr );
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'PERAPNDX_DATE', :OLD.PERAPNDX_DATE);
       Audit_Trail.column_delete (raid, 'PERAPNDX_DESC', :OLD.PERAPNDX_DESC);
      -- Audit_Trail.column_delete (raid, 'PERAPNDX_FILEOBJ', :OLD.PERAPNDX_FILEOBJ);
       Audit_Trail.column_delete (raid, 'PERAPNDX_TYPE', :OLD.PERAPNDX_TYPE);
       Audit_Trail.column_delete (raid, 'PERAPNDX_URI', :OLD.PERAPNDX_URI);
       Audit_Trail.column_delete (raid, 'PK_PERAPNDX', :OLD.PK_PERAPNDX);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
COMMIT;
END;
/
