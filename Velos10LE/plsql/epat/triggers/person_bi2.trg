CREATE OR REPLACE TRIGGER "PERSON_BI2" 
BEFORE INSERT
ON PERSON REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
v_autogen number;
v_patientid varchar2(100);
begin
  if :new.person_code is null then
      select ac_autogen_patient into v_autogen from er_account where pk_account = :new.fk_account;
      if v_autogen = 1 then
        eres.sp_autogen_patient(:new.fk_account,v_patientid) ;
        :new.person_code := v_patientid;

	        if :new.pat_facilityid is null then
	        	:new.pat_facilityid := v_patientid;
	        end if;
      end if;
  end if;

end;
/


