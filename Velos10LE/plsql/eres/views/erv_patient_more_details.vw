/* Formatted on 4/6/2009 4:17:11 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERES.ERV_PATIENT_MORE_DETAILS
(
   FIELD_NAME,
   FIELD_VALUE,
   FK_PER,
   CREATED_ON,
   FK_ACCOUNT,
   PTMPD_PAT_ID,
   PTMPD_PAT_NAME,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   RID,
   PTMPD_RESPONSE_ID
)
AS
   SELECT   (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_idtype)
               AS field_name,
            perid_id AS field_value,
            fk_per,
            a.CREATED_ON,
            fk_account,
            PER_CODE PTMPD_PAT_ID,
            (SELECT   PERSON_FNAME || ' ' || PERSON_LNAME
               FROM   PERSON
              WHERE   PK_PER = PK_PERSON)
               PTMPD_PAT_NAME,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   er_user
              WHERE   pk_user = A.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   er_user
              WHERE   pk_user = A.LAST_MODIFIED_BY)
               last_modified_by,
            a.LAST_MODIFIED_DATE,
            A.RID,
            PK_PERID PTMPD_RESPONSE_ID
     FROM   pat_perid a, ER_PER
    WHERE   pk_per = fk_per;


CREATE OR REPLACE SYNONYM ESCH.ERV_PATIENT_MORE_DETAILS FOR ERES.ERV_PATIENT_MORE_DETAILS;


CREATE OR REPLACE SYNONYM EPAT.ERV_PATIENT_MORE_DETAILS FOR ERES.ERV_PATIENT_MORE_DETAILS;


GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERES.ERV_PATIENT_MORE_DETAILS TO EPAT;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERES.ERV_PATIENT_MORE_DETAILS TO ESCH;


