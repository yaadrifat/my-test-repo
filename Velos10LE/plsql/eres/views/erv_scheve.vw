/* Formatted on 2/9/2010 1:39:32 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERV_SCHEVE
(
   EVENT_ID,
   OBJECT_ID,
   VISIT_TYPE_ID,
   CHILD_SERVICE_ID,
   SESSION_ID,
   OCCURENCE_ID,
   LOCATION_ID,
   NOTES,
   TYPE_ID,
   ROLES,
   SVC_TYPE_ID,
   STATUS,
   SERVICE_ID,
   BOOKED_BY,
   CODELST_DESC,
   DESCRIPTION,
   ATTENDED,
   START_DATE_TIME,
   END_DATE_TIME,
   BOOKEDON,
   ISWAITLISTED,
   ISCONFIRMED,
   OBJECT_NAME,
   LOCATION_NAME,
   SESSION_NAME,
   USER_NAME,
   PATIENT_ID,
   SVC_TYPE_NAME,
   EVENT_NUM,
   VISIT_TYPE_NAME,
   OBJ_LOCATION_ID,
   FK_ASSOC,
   EVENT_EXEON,
   EVENT_EXEBY,
   EVENT_SCHDATE,
   FK_PATPROT,
   VISIT,
   FK_VISIT,
   ACTUAL_SCHDATE,
   VISIT_NAME,
   FK_STUDY,
   EVENT_STATUS_SUBTYPE,
   EVENT_SEQUENCE
)
AS
   SELECT   "EVENT_ID",
            "OBJECT_ID",
            "VISIT_TYPE_ID",
            "CHILD_SERVICE_ID",
            "SESSION_ID",
            "OCCURENCE_ID",
            "LOCATION_ID",
            "NOTES",
            "TYPE_ID",
            "ROLES",
            "SVC_TYPE_ID",
            "STATUS",
            "SERVICE_ID",
            "BOOKED_BY",
            "CODELST_DESC",
            "DESCRIPTION",
            "ATTENDED",
            "START_DATE_TIME",
            "END_DATE_TIME",
            "BOOKEDON",
            "ISWAITLISTED",
            isconfirmed,
            "OBJECT_NAME",
            "LOCATION_NAME",
            "SESSION_NAME",
            "USER_NAME",
            "PATIENT_ID",
            "SVC_TYPE_NAME",
            "EVENT_NUM",
            "VISIT_TYPE_NAME",
            "OBJ_LOCATION_ID",
            "FK_ASSOC",
            "EVENT_EXEON",
            "EVENT_EXEBY",
            "ACTUAL_SCHDATE" AS event_schdate,
            "FK_PATPROT",
            "VISIT",
            fk_visit,
            "ACTUAL_SCHDATE",
            (SELECT   visit_name
               FROM   sch_protocol_visit
              WHERE   pk_protocol_visit = fk_visit)
               AS visit_name,
            fk_study,
            codelst_subtyp event_status_subtype,
            event_sequence
     FROM   SCH_EVENTS1, SCH_CODELST
    WHERE   ISCONFIRMED = pk_codelst;


CREATE SYNONYM ESCH.ERV_SCHEVE FOR ERV_SCHEVE;


CREATE SYNONYM EPAT.ERV_SCHEVE FOR ERV_SCHEVE;


GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_SCHEVE TO EPAT;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_SCHEVE TO ESCH;

