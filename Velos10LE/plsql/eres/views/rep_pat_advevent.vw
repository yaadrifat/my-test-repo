/* Formatted on 2/9/2010 1:39:57 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW REP_PAT_ADVEVENT
(
   PATAE_PAT_ID,
   PATAE_PAT_STUDY,
   PATAE_PAT_NAME,
   PATAE_TYPE,
   PATAE_GRADE,
   PATAE_DESC,
   PATAE_COURSE,
   PATAE_START_DATE,
   PATAE_STOP_DATE,
   PATAE_ENTERED_BY,
   PATAE_ATTRIBUTION,
   PATAE_OUTCOME_TYPE,
   PATAE_DEATH_DATE,
   PATAE_ACTION,
   PATAE_RECOVERYDESC,
   PATAE_OUTCOME_NOTE,
   PATAE_ADDL_INFO,
   PATAE_NOTIFIED,
   PATAE_NOTES,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   PATAE_MEDDRA_CODE,
   PATAE_DICTIONARY,
   RID,
   PATAE_RESPONSE_ID,
   FK_ACCOUNT,
   FK_STUDY,
   PK_PER,
   ADVERSE_EVENT
)
AS
   SELECT   PERSON_CODE PATAE_PAT_ID,
            (SELECT   STUDY_NUMBER
               FROM   ERES.ER_STUDY
              WHERE   PK_STUDY = SCH_ADVERSEVE.FK_STUDY)
               PATAE_PAT_STUDY,
            PERSON_FNAME || ' ' || PERSON_LNAME PATAE_PAT_NAME,
            f_get_codelstdesc (FK_CODLST_AETYPE) PATAE_TYPE,
            AE_GRADE PATAE_GRADE,
            AE_DESC PATAE_DESC,
            AE_TREATMENT_COURSE PATAE_COURSE,
            AE_STDATE PATAE_START_DATE,
            AE_ENDDATE PATAE_STOP_DATE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = AE_ENTERBY)
               PATAE_ENTERED_BY,
            F_GET_CODELSTDESC (AE_RELATIONSHIP) PATAE_ATTRIBUTION,
            F_GETAE_OUTCOME_ADD_INFO (PK_ADVEVE,
                                      AE_OUTDATE,
                                      'outcome',
                                      AE_OUTNOTES)
               PATAE_OUTCOME_TYPE,
            PERSON_DEATHDT PATAE_DEATH_DATE,
            F_GET_CODELSTDESC (FK_CODELST_OUTACTION) PATAE_ACTION,
            F_GET_CODELSTDESC (AE_RECOVERY_DESC) PATAE_RECOVERYDESC,
            AE_OUTNOTES PATAE_OUTCOME_NOTE,
            F_GETAE_OUTCOME_ADD_INFO (PK_ADVEVE,
                                      AE_OUTDATE,
                                      'adve_info',
                                      AE_OUTNOTES)
               PATAE_ADDL_INFO,
            F_Getae_Notify (pk_adveve) patae_notified,
            AE_NOTES PATAE_NOTES,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = SCH_ADVERSEVE.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = SCH_ADVERSEVE.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            SCH_ADVERSEVE.LAST_MODIFIED_DATE,
            SCH_ADVERSEVE.CREATED_ON,
            MEDDRA PATAE_MEDDRA_CODE,
            DICTIONARY PATAE_DICTIONARY,
            SCH_ADVERSEVE.RID,
            PK_ADVEVE PATAE_RESPONSE_ID,
            (SELECT   fk_account
               FROM   ER_STUDY
              WHERE   pk_study = fk_study)
               fk_account,
            fk_study,
            fk_per pk_per,
            ae_name adverse_event
     FROM      ESCH.SCH_ADVERSEVE
            LEFT JOIN
               EPAT.PERSON
            ON SCH_ADVERSEVE.FK_PER = PK_PERSON;


