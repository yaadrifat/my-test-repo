/* Formatted on 2/9/2010 1:39:46 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW REP_ACCNT_ORGS
(
   ACORG_NAME,
   ACORG_TYPE,
   ACORG_PARENT_ORG,
   ACORG_DESC,
   ACORG_SITE_ID,
   ACORG_ADDRESS,
   ACORG_CITY,
   ACORG_STATE,
   ACORG_ZIP,
   ACORG_COUNTRY,
   ACORG_PHONE,
   ACORG_EMAIL,
   ACORG_ACCOUNT,
   CREATOR,
   CREATED_ON,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   RID,
   ACORG_RESPONSE_ID,
   FK_ACCOUNT
)
AS
   SELECT   SITE_NAME ACORG_NAME,
            f_get_codelstdesc (FK_CODELST_TYPE) ACORG_TYPE,
            (SELECT   SITE_NAME
               FROM   ER_SITE S
              WHERE   S.PK_SITE = ER_SITE.SITE_PARENT)
               ACORG_PARENT_ORG,
            SITE_INFO ACORG_DESC,
            SITE_ID ACORG_SITE_ID,
            ADDRESS ACORG_ADDRESS,
            ADD_CITY ACORG_CITY,
            ADD_STATE ACORG_STATE,
            ADD_ZIPCODE ACORG_ZIP,
            ADD_COUNTRY ACORG_COUNTRY,
            ADD_PHONE ACORG_PHONE,
            ADD_EMAIL ACORG_EMAIL,
            (SELECT   AC_NAME
               FROM   ER_ACCOUNT
              WHERE   PK_ACCOUNT = FK_ACCOUNT)
               ACORG_ACCOUNT,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = ER_SITE.CREATOR)
               CREATOR,
            ER_SITE.CREATED_ON,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = ER_SITE.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            ER_SITE.LAST_MODIFIED_DATE,
            ER_SITE.RID,
            ER_SITE.PK_SITE ACORG_RESPONSE_ID,
            FK_ACCOUNT
     FROM      ER_SITE
            LEFT JOIN
               ER_ADD
            ON ER_SITE.FK_PERADD = ER_ADD.PK_ADD;


