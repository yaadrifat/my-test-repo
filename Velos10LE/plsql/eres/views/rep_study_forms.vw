/* Formatted on 2/9/2010 1:40:06 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW REP_STUDY_FORMS
(
   STFRM_STUDY_NUM,
   STFRM_NAME,
   STFRM_DESC,
   STFRM_LINKED_TO,
   STFRM_STATUS,
   CREATOR,
   CREATED_ON,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   FK_ACCOUNT,
   RID,
   STFRM_RESPONSE_ID,
   FK_STUDY
)
AS
   SELECT   s.study_number STFRM_STUDY_NUM,
            b.FORM_NAME STFRM_NAME,
            b.FORM_DESC STFRM_DESC,
            (DECODE (TRIM (lf_displaytype),
                     'S',
                     'Study',
                     'SP',
                     'Patient'))
               AS STFRM_LINKED_TO,
            (SELECT   F_GET_CODELSTDESC (e.fk_codelst_stat)
               FROM   er_formstat e
              WHERE       e.fk_formlib = l.fk_formlib
                      AND e.formstat_enddate IS NULL
                      AND ROWNUM < 2)
               STFRM_STATUS,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = b.CREATOR)
               CREATOR,
            b.CREATED_ON,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = b.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            b.LAST_MODIFIED_DATE,
            s.FK_ACCOUNT fk_ACCOUNT,
            b.RID,
            PK_FORMLIB STFRM_RESPONSE_ID,
            l.fk_study
     FROM   ER_LINKEDFORMS l, ER_STUDY s, er_formlib b
    WHERE       l.FK_FORMLIB = b.PK_FORMLIB
            AND l.fk_study IS NOT NULL
            AND l.record_type <> 'D'
            AND l.fk_study = pk_study;


