/* Formatted on 2/9/2010 1:39:28 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERV_PATSTUDY_EVE_CRF
(
   PATSTUDYSTAT_ID,
   PATSTUDYSTAT_DESC,
   FK_PER,
   PER_CODE,
   FK_STUDY,
   STUDY_NUMBER,
   STUDY_VER_NO,
   FK_ACCOUNT,
   PATSTUDYSTAT_DATE,
   PATPROT_ENROLDT,
   PK_PATPROT,
   PK_PATSTUDYSTAT,
   PER_SITE,
   SCH_EVENTS1_PK,
   EVENT_PROTOCOL_ID,
   EVENT_STATUS_ID,
   EVENT_START_DATE,
   EVENT_ID_ASSOC,
   EVENT_EXEON,
   EVENT_EXEBY,
   EVENT_ACTUAL_SCHDATE,
   VISIT,
   EVENT_STATUS_DESC,
   EVENT_STATUS_CODELST_SUBTYP,
   CRF_STAT_DESC,
   CRF_STAT_CODELST_SUBTYP,
   CRF_NUMBER,
   CRF_NAME,
   CRF_TYPE_FLAG,
   CRF_CURSTAT_ID,
   CRF_PK,
   CRF_CREATION_DATE,
   CRF_STAT_PK,
   CRF_STAT_DATE,
   CRFSTAT_ENTERBY,
   CRFSTAT_REVIEWBY,
   CRFSTAT_REVIEWON,
   CRFSTAT_SENTTO,
   CRFSTAT_SENTFLAG,
   CRFSTAT_SENTBY,
   CRFSTAT_SENTON
)
AS
   SELECT   v.PATSTUDYSTAT_ID,
            v.PATSTUDYSTAT_DESC,
            v.FK_PER,
            v.PER_CODE,
            v.FK_STUDY,
            v.STUDY_NUMBER,
            v.STUDY_VER_NO,
            v.FK_ACCOUNT,
            v.PATSTUDYSTAT_DATE,
            v.PATPROT_ENROLDT,
            v.PK_PATPROT,
            v.PK_PATSTUDYSTAT,
            v.PER_SITE,
            v.SCH_EVENTS1_PK,
            v.EVENT_PROTOCOL_ID,
            v.EVENT_STATUS_ID,
            v.EVENT_START_DATE,
            v.EVENT_ID_ASSOC,
            v.EVENT_EXEON,
            v.EVENT_EXEBY,
            v.EVENT_ACTUAL_SCHDATE,
            v.VISIT,
            v.EVENT_STATUS_DESC,
            v.EVENT_STATUS_CODELST_SUBTYP,
            c.CODELST_DESC,
            c.CODELST_SUBTYP,
            crf.crf_number,
            crf.crf_name,
            crf.crf_flag,
            crf.crf_curstat,
            crf.pk_crf,
            crf.created_on,
            crfstat.pk_crfstat,
            crfstat.created_on,
            crfstat.CRFSTAT_ENTERBY,
            crfstat.CRFSTAT_REVIEWBY,
            crfstat.CRFSTAT_REVIEWON,
            crfstat.CRFSTAT_SENTTO,
            crfstat.CRFSTAT_SENTFLAG,
            crfstat.CRFSTAT_SENTBY,
            crfstat.CRFSTAT_SENTON
     FROM   ERV_PATSTUDY_EVE v,
            SCH_CODELST c,
            SCH_CRF crf,
            SCH_CRFSTAT crfstat
    WHERE       v.SCH_EVENTS1_PK = crf.fk_events1
            AND c.pk_codelst = crfstat.FK_CODELST_CRFSTAT
            AND crf.crf_delflag <> 'Y'
            AND crfstat.FK_CRF = crf.PK_CRF;


CREATE SYNONYM ESCH.ERV_PATSTUDY_EVE_CRF FOR ERV_PATSTUDY_EVE_CRF;


CREATE SYNONYM EPAT.ERV_PATSTUDY_EVE_CRF FOR ERV_PATSTUDY_EVE_CRF;


GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_PATSTUDY_EVE_CRF TO EPAT;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_PATSTUDY_EVE_CRF TO ESCH;

