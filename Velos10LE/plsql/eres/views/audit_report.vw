/* Formatted on 2/9/2010 1:39:08 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW AUDIT_REPORT
(
   RAID,
   RID,
   TABLE_NAME,
   ACTION,
   TIMESTAMP,
   USER_NAME,
   COLUMN_NAME,
   OLD_VALUE,
   NEW_VALUE,
   FK_ACCOUNT
)
AS
   SELECT                                                    /*+ FIRST_ROWS */
         A  .RAID,
            a.RID,
            a.TABLE_NAME,
            DECODE (action,
                    'U',
                    'UPDATE',
                    'D',
                    'DELETE',
                    'I',
                    'INSERT')
               action,
            a.TIMESTAMP,
            a.USER_NAME,
            b.COLUMN_NAME,
            OLD_VALUE,
            new_VALUE,
            c.fk_account
     FROM   audit_row a, audit_column b, er_user c
    WHERE       a.raid = b.raid
            AND a.TABLE_NAME <> 'ER_USER'
            AND ASCII (SUBSTR (TRIM (a.user_name), 1, 1)) BETWEEN 48 AND 57
            AND SUBSTR (
                  a.user_name,
                  1,
                  DECODE (INSTR (a.user_name, ','),
                          0, 10,
                          INSTR (a.user_name, ',') - 1)
               ) = c.pk_user
   /* this will get update transactions of all tables except ER USER */
   UNION ALL
   SELECT                                                    /*+ FIRST_ROWS */
         A  .RAID,
            a.RID,
            a.TABLE_NAME,
            DECODE (action,
                    'U',
                    'UPDATE',
                    'D',
                    'DELETE',
                    'I',
                    'INSERT')
               action,
            a.TIMESTAMP,
               TO_CHAR (c.pk_user)
            || ', '
            || c.usr_lastname
            || ', '
            || c.usr_lastname
               USER_NAME,
            b.COLUMN_NAME,
            OLD_VALUE,
            new_VALUE,
            c.fk_account
     FROM   audit_row a, audit_column b, ER_USER c
    WHERE       a.raid = b.raid
            AND a.user_name = c.pk_user
            AND a.table_name = 'ER_USER'
            AND a.action = 'U'
            AND ASCII (SUBSTR (TRIM (a.user_name), 1, 1)) BETWEEN 48 AND 57
   /* This will get update transaction of ER USER table with User Name from ER User */
   UNION ALL
   SELECT                                                    /*+ FIRST_ROWS */
         A  .RAID,
            a.RID,
            a.TABLE_NAME,
            'INSERT' action,
            a.TIMESTAMP,
            a.USER_NAME,
            ' ' COLUMN_NAME,
            ' ' OLD_VALUE,
            ' ' new_VALUE,
            c.fk_account
     FROM   audit_row a, er_user c
    WHERE       Action = 'I'
            AND table_name <> 'ER_USER'
            AND ASCII (SUBSTR (TRIM (a.user_name), 1, 1)) BETWEEN 48 AND 57
            AND SUBSTR (
                  a.user_name,
                  1,
                  DECODE (INSTR (a.user_name, ','),
                          0, 10,
                          INSTR (a.user_name, ',') - 1)
               ) = c.pk_user
   /* This will get the Insert transactions of all talbles except ER USER*/
   UNION ALL
   SELECT                                                    /*+ FIRST_ROWS */
         A  .RAID,
            a.RID,
            a.TABLE_NAME,
            'INSERT' action,
            a.TIMESTAMP,
            a.USER_NAME,
            ' ' COLUMN_NAME,
            ' ' OLD_VALUE,
            ' ' new_VALUE,
            TO_NUMBER (NULL) fk_account
     FROM   audit_row a
    WHERE       Action = 'I'
            AND table_name = 'ER_USER'
            AND user_name = 'New User'
   UNION ALL
   SELECT                                                    /*+ FIRST_ROWS */
         A  .RAID,
            a.RID,
            a.TABLE_NAME,
            'INSERT' action,
            a.TIMESTAMP,
               TO_CHAR (b.pk_user)
            || ', '
            || b.usr_lastname
            || ', '
            || b.usr_lastname
               USER_NAME,
            ' ' COLUMN_NAME,
            ' ' OLD_VALUE,
            ' ' new_VALUE,
            b.fk_account
     FROM   audit_row a, er_user b
    WHERE       Action = 'I'
            AND table_name = 'ER_USER'
            AND ASCII (SUBSTR (TRIM (a.user_name), 1, 1)) BETWEEN 48 AND 57
            AND a.user_name = b.pk_user;


CREATE SYNONYM ESCH.AUDIT_REPORT FOR AUDIT_REPORT;


CREATE SYNONYM EPAT.AUDIT_REPORT FOR AUDIT_REPORT;


GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON AUDIT_REPORT TO EPAT;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON AUDIT_REPORT TO ESCH;

