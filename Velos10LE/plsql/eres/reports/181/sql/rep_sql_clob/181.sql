SELECT disease_site,
  disease_site_subtyp,
  repsitename,
  enrsitename,
  studyTypes,
  Anatomicsitenull,
  SUM(pat_ds_count) COUNT
FROM
  ( select codelst_desc as disease_site, codelst_subtyp as disease_site_subtyp,
    (select site_name from er_site where pk_site in (:repOrgId)) as repsitename,
     SITE_NAME as enrsitename,
    null as studyTypes,
    pkg_util.f_join(cursor(SELECT ('[Study: '||STUDY_NUMBER ||' & '||'Patient: '||(select PER_CODE from er_per where PK_PER = patprt.fk_per) || ']')
    FROM ER_PATPROT patprt,
      er_patstudystat patst,
      er_study stdy,
      er_studysites stsite
    WHERE stdy.fk_account = :sessAccId
    AND stdy.pk_study     = patprt.fk_study
    AND patprt.FK_PER     = patst.fk_per
    AND stsite.fk_study   =stdy.pk_study
    AND patprt.fk_study   = patst.fk_study
	AND patprt.patprot_stat = 1
    AND FK_CODELST_STAT = (SELECT cdStat1.pk_codelst FROM ER_CODELST cdStat1 WHERE cdStat1.codelst_subtyp = 'enrolled')
    AND PATSTUDYSTAT_DATE BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT)
    AND fk_site_enrolling  = enrSite.pk_site
    AND stsite.FK_SITE             IN (:repOrgId)
    AND (PATPROT_TREATINGORG IS NULL OR NVL(PATPROT_TREATINGORG,0) IN (:trtOrgId))
    AND PATPROT_OTHR_DIS_CODE IS NULL
    AND FK_CODELST_TYPE IN (SELECT cdStudyType.pk_codelst FROM ER_CODELST cdStudyType WHERE cdStudyType.codelst_type ='study_type' and CODELST_CUSTOM_COL1='interventional')
    AND FK_CODELST_PURPOSE IN (SELECT cdStudyPurpose.pk_codelst FROM ER_CODELST cdStudyPurpose WHERE cdStudyPurpose.codelst_type ='studyPurpose' and cdStudyPurpose.CODELST_SUBTYP='treatment')
    ), ';') AS Anatomicsitenull,
	(SELECT count(*)
      FROM ER_PATPROT patprt, er_study stdy, er_studysites stsite
      WHERE  stdy.fk_account = :sessAccId
      AND stdy.pk_study = patprt.fk_study
      AND stsite.fk_study=stdy.pk_study
	  AND patprt.patprot_stat = 1
	  AND EXISTS (select * from er_patstudystat patst
      where patst.fk_per = patprt.FK_PER and patst.fk_study = patprt.fk_study
      AND patst.FK_CODELST_STAT = (SELECT cdStat1.pk_codelst FROM ER_CODELST cdStat1 WHERE cdStat1.codelst_subtyp = 'enrolled')
      AND patst.PATSTUDYSTAT_DATE BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT))
      AND fk_site_enrolling = enrSite.pk_site
      AND stsite.FK_SITE in (:repOrgId)
      AND (PATPROT_TREATINGORG IS NULL OR NVL(PATPROT_TREATINGORG,0) IN (:trtOrgId))
	  AND (cdDisSite.PK_CODELST = f_to_number(patprt.PATPROT_OTHR_DIS_CODE)
      OR ((cdDisSite.PK_CODELST <> f_to_number(patprt.PATPROT_OTHR_DIS_CODE))
      AND (
      (cdDisSite.PK_CODELST in (select t2.pk_codelst from er_codelst t2 where t2.codelst_type = 'disease_site' AND t2.codelst_subtyp = 'leukemiaOther')
      AND f_to_number(patprt.PATPROT_OTHR_DIS_CODE) in (select t2.pk_codelst from er_codelst t2 where t2.codelst_type = 'disease_site' AND t2.codelst_subtyp = 'leukemNotSpecif'))
      )))
      AND FK_CODELST_TYPE IN (SELECT cdStudyType.pk_codelst FROM ER_CODELST cdStudyType WHERE cdStudyType.codelst_type ='study_type' and CODELST_CUSTOM_COL1='interventional')
    AND FK_CODELST_PURPOSE IN (SELECT cdStudyPurpose.pk_codelst FROM ER_CODELST cdStudyPurpose WHERE cdStudyPurpose.codelst_type ='studyPurpose' and cdStudyPurpose.CODELST_SUBTYP='treatment')    ) as pat_ds_count
  FROM er_site enrSite,
  (select a.pk_codelst as pk_codelst, a.codelst_subtyp as codelst_subtyp, a.codelst_desc as codelst_desc
  from er_codelst a WHERE a.codelst_type = 'disease_site' and a.codelst_subtyp not in ('multiplesites','leukemNotSpecif')
  UNION
  select bb.pk_codelst, 'leukemiaOther', bb.codelst_desc
  from er_codelst b, er_codelst bb WHERE b.codelst_type = bb.codelst_type and b.codelst_type ='disease_site' and b.codelst_subtyp = 'leukemNotSpecif' and bb.codelst_subtyp = 'leukemiaOther'
  ) cdDisSite
  WHERE enrSite.PK_SITE in (:enrOrgId)
  )
  group by disease_site, disease_site_subtyp, repsitename, enrsitename, studyTypes, Anatomicsitenull
  order by enrsitename,
  decode(disease_site_subtyp,'lip',1,'esophagus',2,'stomach',3,'smallIntestine',4,'colon',5,'rectum',6,'anus',7,'liver',8,'pancreas',9,'othDigestOrgan',10,
'larynx',11,'lung',12,'oRespIntThorOrg',13,'bonesJoints',14,'softTissue',15, 'melanomaSkin',16,'kaposis',17,'mycosisFungoide',18,'otherSkin',19,'b_female',20,'b_male',21,
'cervix',22,'corpusUteri',23,'ovary',24,'othFemalGenital',25,'prostate',26,'othMaleGenital',27,'urinaryBladder',28,'kidney',29,'otherUrinary',30,'eyeandOrbit',31,
'brain',32,'thyroid',33,'othEndocrineSys',34,'non_hodgkins',35,'hodgkins',36,'multipleMyeloma',37,'lymphoidLeuk',38,'myeloMonocyLeuk',39,'leukemiaOther',40,'othHemapoietic',41,
'unknownSites',42,'illDefinedSites',43) 