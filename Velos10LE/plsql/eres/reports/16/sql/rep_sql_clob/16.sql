select er_study.study_number,
er_site.site_name,
to_char(er_study.study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT)
study_actualdt,
er_study.study_title,
PER_CODE patient_id,
patprot_patstdid,
to_char(er_patprot.PATPROT_ENROLDT,PKG_DATEUTIL.F_GET_DATEFORMAT) pe,
(select name         from ESCH.event_assoc        where
EVENT_ID = er_patprot.fk_protocol) protocol,
to_char(er_patprot.patprot_start,PKG_DATEUTIL.F_GET_DATEFORMAT) ps,
( select codelst_desc
from er_codelst
where pk_codelst = fk_codelst_stat ) patient_status
from er_patprot, er_study,  er_per  , er_site,ER_PATSTUDYSTAT stat
where   er_patprot.PATPROT_STAT = 1
and     er_patprot.fk_study in (:studyId)
and     er_per.PK_PER = er_patprot.fk_per
and     er_per.fk_site in (:orgId)
and     er_per.fk_site = er_site.pk_site
and     er_study.pk_study = er_patprot.fk_study
AND ER_PATPROT.fk_per=stat.fk_per AND ER_PATPROT.fk_study=stat.fk_study
AND  PATSTUDYSTAT_ENDT IS NULL
and     er_patprot.PATPROT_ENROLDT >= TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT)
and     er_patprot.PATPROT_ENROLDT <= TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT)
and fk_codelst_stat in (:patStatusId)