select f.person_code as patientId,
b.CODELST_DESC as advType,
c.STUDY_NUMBER as studyNumber,
c.STUDY_TITLE as studyTitle,
to_char(c. STUDY_ACTUALDT,'MM/dd/yyyy') study_start_dt,
g.site_name site_name,
a.AE_DESC as advDesc,
to_char(a.AE_STDATE,'MM/dd/yyyy') as advSdate,
to_char(a.AE_OUTDATE,'MM/dd/yyyy') as advOutdate,
(select codelst_desc from sch_codelst where pk_codelst = a.AE_RELATIONSHIP) as advRelation,
(select distinct patprot_patstdid from er_patprot where fk_study=c.pk_study and fk_per = a.fk_per) as pat_studyid
from sch_adverseve a,
sch_codelst b,
er_study c,
sch_advInfo d,
sch_codelst e,
person f,
er_site g
where  a.FK_CODLST_AETYPE=b.pk_codelst
and d.FK_CODELST_INFO=e.pk_codelst
and a.FK_PER=f.pk_person
and d.FK_ADVERSE = a.PK_ADVEVE
and a.FK_STUDY = c.pk_study
and e.CODELST_SUBTYP = 'al_death'
and d.ADVINFO_VALUE = 1
and f.FK_SITE = ~1
and c.PK_STUDY= ~2
and a.AE_OUTDATE between '~3' and '~4'
and g.pk_site = ~1
