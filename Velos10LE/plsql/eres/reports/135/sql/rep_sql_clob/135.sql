select PERSON_CODE, PAT_STUDYID, STUDY_NUMBER, PROTOCOL_NAME, PATPROT_ENROLDT,
SITE_NAME, PATPROT_START, VISIT, EVENT_NAME, MONTH, YEAR, START_DATE_TIME,
STATUS,EVENTSTAT_DT,ROLE,
PLAN_DUR,
round(substr(PLAN_DUR,1,2) * 24 + substr(PLAN_DUR,4,2) + substr(PLAN_DUR,7,2) / 60 + substr(PLAN_DUR,10,2) / 3600,2) as PLAN_DUR_HRS,
ACTUAL_DUR,
round(substr(ACTUAL_DUR,1,2) * 24 + substr(ACTUAL_DUR,4,2) + substr(ACTUAL_DUR,7,2) / 60 + substr(ACTUAL_DUR,10,2) / 3600,2) as ACTUAL_DUR_HRS
from (
SELECT PERSON_CODE , PAT_STUDYID, STUDY_NUMBER,
   PROTOCOL_NAME,  TO_CHAR(PATPROT_ENROLDT,PKG_DATEUTIL.F_GET_DATEFORMAT) PATPROT_ENROLDT,
  SITE_NAME,
 TO_CHAR(PATPROT_START,PKG_DATEUTIL.F_GET_DATEFORMAT) PATPROT_START,
    visit_name AS VISIT, EVENT_NAME, MONTH, TO_CHAR(EVENT_SCHDATE,'YYYY') YEAR ,
    TO_CHAR(EVENT_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) START_DATE_TIME,
        (SELECT codelst_desc FROM sch_codelst WHERE pk_codelst = eventstat) AS status,
  TO_CHAR(eventstat_dt,PKG_DATEUTIL.F_GET_DATEFORMAT) eventstat_dt,
        (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_role) AS ROLE,
      (SELECT eventusr_duration
      FROM sch_eventusr WHERE sch_eventusr.fk_event = a.fk_assoc
      AND EVENTUSR = fk_codelst_role ) AS plan_dur,
        evres_track_duration AS actual_dur
 FROM erv_patsch a, esch.sch_events1 b, sch_eventstat c, esch.sch_evres_track d
 WHERE a.event_id = b.event_id
 AND b.event_id = c.fk_event
 AND fk_eventstat = pk_eventstat
 AND a.fk_study IN (:studyId)
 AND EVENT_SCHDATE BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT)
 ORDER BY person_code, EVENT_SCHDATE
)
