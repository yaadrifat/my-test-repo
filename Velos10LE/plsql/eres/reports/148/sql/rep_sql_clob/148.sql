SELECT
 ST.STUDY_NUMBER, ST.STUDY_TITLE,
 (select usr_firstname||' '||usr_lastname from er_user where
 pk_user = NVL(ST.STUDY_PRINV, 0)) STUDY_PRINV,
 (select F_CODELST_DESC(ss.submission_status) from dual) submission_status,
 to_char(ss.submission_status_date,pkg_dateutil.f_get_dateformat) submission_status_date,
 (select to_char(meeting_date,pkg_dateutil.f_get_dateformat) from er_review_meeting
 where pk_review_meeting = NVL(esb.fk_review_meeting,0)) meeting_date,
 PK_SUBMISSION_PROVISO,
 FK_USER_ENTEREDBY,usr_lst(FK_USER_ENTEREDBY) User_enteredby_name,
 PROVISO_DATE, PROVISO
 from er_submission sub
 inner join er_study st
 on SUB.FK_STUDY = ST.PK_STUDY
 inner join er_submission_board esb
 on esb.fk_submission = sub.pk_submission
 inner join er_submission_status ss
 on ss.fk_submission = sub.pk_submission and
 ss.fk_submission_board = esb.pk_submission_board
 left outer join er_submission_proviso p
 on (P.FK_SUBMISSION = SUB.PK_SUBMISSION and p.fk_submission_board = :submissionBoardPK)
 where
 sub.pk_submission = :submissionPK and esb.pk_submission_board = :submissionBoardPK
 and ss.is_current = 1
 order by p.PROVISO_DATE desc
 
