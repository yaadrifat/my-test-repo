 select
  PERSON_CODE                    ,
  PAT_STUDYID,
  PERSON_NAME                    ,
  STUDY_NUMBER                   ,
  SITE_NAME,
 STUDY_TITLE                    ,
 to_char(STUDY_ACTUALDT,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDY_ACTUALDT,
        to_char(PATPROT_ENROLDT,PKG_DATEUTIL.F_GET_DATEFORMAT) PATPROT_ENROLDT,
        PROTOCOLID   ,
  PROTOCOL_NAME                  ,
  to_char(PATPROT_START,PKG_DATEUTIL.F_GET_DATEFORMAT) PATPROT_START,
  EVENT_NAME                     ,
  MONTH                          ,
  to_char(EVENT_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) st ,
  EVENT_STATUS,
  FK_PATPROT, visit_name as VISIT
 from   erv_patsch
where  fk_per in (:patientId)  and
       fk_study in (:studyId) and
        EVENT_STATUS in ('Not Done','To Be Rescheduled') and
       EVENT_SCHDATE between TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT)  and TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT) and
 SCHEVE_STATUS = 0
 Order by EVENT_SCHDATE
