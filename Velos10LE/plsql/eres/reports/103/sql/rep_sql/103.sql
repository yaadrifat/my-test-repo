SELECT study_number AS study_number,
person_code AS patient_id,
patprot_patstdid AS patient_study_id,
person_lname || ', ' || person_fname AS patient_name,
(SELECT codelst_desc FROM er_codelst WHERE pk_codelst = fk_codelst_race) AS race,
(SELECT codelst_desc FROM er_codelst WHERE pk_codelst = fk_codelst_ethnicity) AS ethnicity,
TO_CHAR(patprot_enroldt,PKG_DATEUTIL.F_GET_DATEFORMAT) AS enrolled_on,
(SELECT TO_CHAR(MAX(patstudystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT) FROM er_patstudystat WHERE fk_per = a.fk_per AND fk_study = a.FK_STUDY AND fk_codelst_stat = (SELECT pk_codelst FROM er_codelst WHERE codelst_type ='patStatus' AND codelst_subtyp = 'active')) AS active_on,
(SELECT TO_CHAR(MAX(patstudystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT) FROM er_patstudystat WHERE fk_per = a.fk_per AND fk_study = a.FK_STUDY AND fk_codelst_stat = (SELECT pk_codelst FROM er_codelst WHERE codelst_type ='patStatus' AND codelst_subtyp = 'followup')) AS followup_on,
(SELECT MAX(TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) ||  (SELECT ' - ' || codelst_desc FROM er_codelst WHERE pk_codelst = patstudystat_reason) || DECODE(patstudystat_note,NULL,'',' - ' || patstudystat_note)) FROM er_patstudystat WHERE fk_per = a.fk_per AND fk_study = a.FK_STUDY AND fk_codelst_stat = (SELECT pk_codelst FROM er_codelst WHERE codelst_type ='patStatus' AND codelst_subtyp = 'offtreat')) AS off_treatment_on,
(SELECT MAX(TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) || (SELECT ' - ' || codelst_desc FROM er_codelst WHERE pk_codelst = patstudystat_reason) ||  DECODE(patstudystat_note,NULL,'',' - ' || patstudystat_note)) FROM er_patstudystat WHERE fk_per = a.fk_per AND fk_study = a.FK_STUDY AND fk_codelst_stat = (SELECT pk_codelst FROM er_codelst WHERE codelst_type ='patStatus' AND codelst_subtyp = 'offstudy')) AS off_study_on
FROM er_patprot a, er_study b,EPAT.person c
WHERE a.FK_PER = c.pk_person AND
EXISTS (SELECT * FROM er_usersite WHERE fk_site = c.fk_site AND fk_user = ~1  AND usersite_right > 0)    AND
a.FK_STUDY = b.pk_study AND
patprot_enroldt BETWEEN '~2' AND '~3' AND
patprot_stat = 1
ORDER BY study_number,patient_id