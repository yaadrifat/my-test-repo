select 	std.study_number study_num,
to_char(std.study_actualdt,'mm/dd/yyyy') study_start_dt,
std.study_title study_title,st.site_name site_name,
std.pk_study,
st.pk_site,
(select count(pk_patprot)
from 	ERV_PATSTUDYSTAT b
Where 	b.FK_STUDY = std.pk_study
and 	b.per_site = st.pk_site  and
PATPROT_ENROLDT between '~2'and '~3'
and 	trim(lower(GENDER_CODELST_SUBTYP)) = 'female') gen_female_count,
(select 	count(pk_patprot)
from 	ERV_PATSTUDYSTAT b
Where 	b.FK_STUDY = std.pk_study
and 	b.per_site = st.pk_site
and 	PATPROT_ENROLDT between '~2'and '~3'
and 	trim(lower(GENDER_CODELST_SUBTYP)) = 'male') gen_male_count,
(select 	count(pk_patprot)
from 	ERV_PATSTUDYSTAT b
Where 	b.FK_STUDY = std.pk_study
and 	b.per_site = st.pk_site  and
PATPROT_ENROLDT between '~2'and '~3'
and 	trim(lower(GENDER_CODELST_SUBTYP)) = 'other') gen_other_count,
(select 	count(pk_patprot)
from 	ERV_PATSTUDYSTAT b
Where 	b.FK_STUDY = std.pk_study
and 	b.per_site = st.pk_site
and 	PATPROT_ENROLDT between '~2'and '~3'
and 	trim(lower(GENDER_CODELST_SUBTYP)) = 'unknown') gen_unknown_count,
(select 	count(pk_patprot)
from 	ERV_PATSTUDYSTAT b
Where 	b.FK_STUDY = std.pk_study
and 	b.per_site = st.pk_site  and
	PATPROT_ENROLDT between '~2'and '~3'
and 	nvl(GENDER_CODELST_SUBTYP,'1') = '1') gen_not_entered_count,
(select 	count(pk_patprot)
from 	ERV_PATSTUDYSTAT b
Where 	b.FK_STUDY = std.pk_study
and 	b.per_site = st.pk_site
and 	PATPROT_ENROLDT between '~2'and '~3'
and 	trim(lower(ETHNICITY_CODELST_SUBTYP)) = 'hispanic') race_hispanic_count,
(select 	count(pk_patprot)
from 	ERV_PATSTUDYSTAT b
Where 	b.FK_STUDY = std.pk_study
and 	b.per_site = st.pk_site
and 	PATPROT_ENROLDT between '~2'and '~3'
and 	trim(lower(ETHNICITY_CODELST_SUBTYP)) = 'asian') race_asian_count,
(select 	count(pk_patprot)
from 	ERV_PATSTUDYSTAT b
Where 	b.FK_STUDY = std.pk_study
and 	b.per_site = st.pk_site
and 	PATPROT_ENROLDT between '~2'and '~3'
and 	trim(lower(ETHNICITY_CODELST_SUBTYP)) = 'afr_am') race_afr_am_count,
(select 	count(pk_patprot)
from 	ERV_PATSTUDYSTAT b
Where 	b.FK_STUDY = std.pk_study
and 	b.per_site = st.pk_site
and 	PATPROT_ENROLDT between '~2'and '~3'
and 	trim(lower(ETHNICITY_CODELST_SUBTYP)) = 'caucasian') race_caucasian_count,
(select 	count(pk_patprot)
from 	ERV_PATSTUDYSTAT b
Where 	b.FK_STUDY = std.pk_study
and 	b.per_site = st.pk_site
and 	PATPROT_ENROLDT between '~2'and '~3'
and 	trim(lower(ETHNICITY_CODELST_SUBTYP)) = 'nat_am') race_nat_am_count,
(select 	count(pk_patprot)
from 	ERV_PATSTUDYSTAT b
Where 	b.FK_STUDY = std.pk_study
and 	b.per_site = st.pk_site
and 	PATPROT_ENROLDT between '~2'and '~3'
and 	trim(lower(ETHNICITY_CODELST_SUBTYP)) = 'other') race_other_count,
(select 	count(pk_patprot)
from 	ERV_PATSTUDYSTAT b
Where 	b.FK_STUDY = std.pk_study
and 	b.per_site = st.pk_site
and 	PATPROT_ENROLDT between '~2'and '~3'
and 	nvl(ETHNICITY_CODELST_SUBTYP,'1') = '1') race_not_entered_count,
(select 	count(pk_patprot)
from 	ERV_PATSTUDYSTAT b
Where 	b.FK_STUDY = std.pk_study
and 	b.per_site = st.pk_site
and 	PATPROT_ENROLDT between '~2'and '~3' )  total_count
from 	er_study std,
er_account acc,
er_site st
where 	std.pk_study =  ~1
and 	acc.pk_account = std.fk_account
and 	st.fk_account = acc.pk_account
