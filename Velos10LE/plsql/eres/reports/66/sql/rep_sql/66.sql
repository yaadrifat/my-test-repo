SELECT (SELECT form_name FROM ER_FORMLIB WHERE pk_formlib = fk_formlib) AS form_name,
(SELECT per_code FROM ER_PER WHERE pk_per = fk_per) AS patient_id,
(SELECT to_char(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = f_codelst_id('patStatus' ,'enrolled') ) AS enrolled_date,
(SELECT to_char(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = f_codelst_id('patStatus' ,'active')  AND ROWNUM = 1) AS active_date,
(SELECT to_char(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = f_codelst_id('patStatus' ,'offtreat')  AND ROWNUM = 1) AS offtreat_date,
(SELECT to_char(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = f_codelst_id('patStatus' ,'offstudy')  AND ROWNUM = 1) AS offstudy_date,
(SELECT (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_stat) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND patstudystat_date = (SELECT MAX(patstudystat_date) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study ) AND ROWNUM = 1) AS current_status,
(SELECT COUNT(*) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> 'D') AS tot_forms,
(SELECT COUNT(*) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> 'D' AND form_completed = f_codelst_id('fillformstat' ,'complete')  ) AS comp_formstat,
(SELECT COUNT(*) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> 'D' AND form_completed = f_codelst_id('fillformstat' ,'working')  ) AS work_formstat,
(SELECT COUNT(*) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> 'D' AND form_completed NOT IN (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'fillformstat' AND (codelst_subtyp = 'working' OR codelst_subtyp = 'complete'))) AS othr_formstat,
(SELECT to_char(MAX(created_on),PKG_DATEUTIL.F_GET_DATEtimeFORMAT) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> 'D' ) AS form_createdon,
(SELECT (SELECT usr_firstname || ' ' || usr_lastname FROM ER_USER WHERE pk_user = y.creator) FROM ER_PATFORMS y WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> 'D' AND created_on = (SELECT MAX(created_on) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> 'D' ) AND ROWNUM = 1) AS form_createdby,
(SELECT to_char(MAX(last_modified_date),PKG_DATEUTIL.F_GET_DATEtimeFORMAT) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> 'D' ) AS form_modon,
(SELECT (SELECT usr_firstname || ' ' || usr_lastname FROM ER_USER WHERE pk_user = y.last_modified_by) FROM ER_PATFORMS y WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> 'D' AND last_modified_date = (SELECT MAX(last_modified_date) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> 'D' ) AND ROWNUM = 1) AS form_modby
FROM
(SELECT DISTINCT a.fk_per,fk_formlib,fk_study, pk_patprot
FROM ER_PATPROT a, ER_PATFORMS b
WHERE
pk_patprot = fk_patprot AND
fk_study = ~2 AND a.fk_per = ~1 and
(b.created_on between to_date('~3',PKG_DATEUTIL.F_GET_DATEFORMAT)and to_date('~4',PKG_DATEUTIL.F_GET_DATEFORMAT)  OR b.last_modified_date between to_date('~3',PKG_DATEUTIL.F_GET_DATEFORMAT)and to_date('~4',PKG_DATEUTIL.F_GET_DATEFORMAT)  ) ) x
ORDER BY 2,1
