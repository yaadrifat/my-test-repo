select
       er_user.usr_lastname,
        er_user.usr_firstname,
        '-' team_role,
         er_site.site_name,
          er_add.add_phone,
         er_add.add_email,
         'M' view_modify,
      er_study.study_number,
     to_char(er_study.study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt
        from er_user,
             er_study,
             er_site,
             er_add
       where er_study.study_parentid = ~1
         and er_study.fk_author = er_user.pk_user
         and er_user.fk_siteid = er_site.pk_site
         and er_add.pk_add = er_user.fk_peradd
 union all
      select er_user.usr_lastname,
             er_user.usr_firstname,
             '-' team_role,
             er_site.site_name,
             er_add.add_phone,
             er_add.add_email,
             'V' view_modify,
             er_studyview.studyview_num,
             null study_actualdt
      from  er_studyview ,
            er_user,
             er_site,
             er_add
      where er_studyview.fk_study = ~1
        and  er_studyview.fk_user = pk_user
        and er_user.fk_siteid = er_site.pk_site
        and er_add.pk_add = er_user.fk_peradd
      order by view_modify, usr_firstname,usr_lastname 
