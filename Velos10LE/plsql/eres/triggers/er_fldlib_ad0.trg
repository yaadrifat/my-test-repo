CREATE  OR REPLACE TRIGGER ER_FLDLIB_AD0 AFTER DELETE ON ER_FLDLIB        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_FLDLIB', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr );
       Audit_Trail.column_delete (raid, 'FK_ACCOUNT', :OLD.FK_ACCOUNT);
       Audit_Trail.column_delete (raid, 'FK_CATLIB', :OLD.FK_CATLIB);
       Audit_Trail.column_delete (raid, 'FK_LOOKUP', :OLD.FK_LOOKUP);
       Audit_Trail.column_delete (raid, 'FLD_ALIGN', :OLD.FLD_ALIGN);
       Audit_Trail.column_delete (raid, 'FLD_BOLD', :OLD.FLD_BOLD);
       Audit_Trail.column_delete (raid, 'FLD_CHARSNO', :OLD.FLD_CHARSNO);
       Audit_Trail.column_delete (raid, 'FLD_COLCOUNT', :OLD.FLD_COLCOUNT);
       Audit_Trail.column_delete (raid, 'FLD_COLOR', :OLD.FLD_COLOR);
       Audit_Trail.column_delete (raid, 'FLD_DATATYPE', :OLD.FLD_DATATYPE);
       Audit_Trail.column_delete (raid, 'FLD_DECIMAL', :OLD.FLD_DECIMAL);
       Audit_Trail.column_delete (raid, 'FLD_DEFRESP', :OLD.FLD_DEFRESP);
       Audit_Trail.column_delete (raid, 'FLD_DESC', :OLD.FLD_DESC);
       Audit_Trail.column_delete (raid, 'FLD_DISPLAY_WIDTH', :OLD.FLD_DISPLAY_WIDTH);
       Audit_Trail.column_delete (raid, 'FLD_EXPLABEL', :OLD.FLD_EXPLABEL);
       Audit_Trail.column_delete (raid, 'FLD_FONT', :OLD.FLD_FONT);
       Audit_Trail.column_delete (raid, 'FLD_FONTSIZE', :OLD.FLD_FONTSIZE);
       Audit_Trail.column_delete (raid, 'FLD_FORMAT', :OLD.FLD_FORMAT);
       Audit_Trail.column_delete (raid, 'FLD_HIDELABEL', :OLD.FLD_HIDELABEL);
       Audit_Trail.column_delete (raid, 'FLD_HIDERESPLABEL', :OLD.FLD_HIDERESPLABEL);
       Audit_Trail.column_delete (raid, 'FLD_INSTRUCTIONS', :OLD.FLD_INSTRUCTIONS);
       Audit_Trail.column_delete (raid, 'FLD_ISREADONLY', :OLD.FLD_ISREADONLY);
       Audit_Trail.column_delete (raid, 'FLD_ISUNIQUE', :OLD.FLD_ISUNIQUE);
       Audit_Trail.column_delete (raid, 'FLD_ISVISIBLE', :OLD.FLD_ISVISIBLE);
       Audit_Trail.column_delete (raid, 'FLD_ITALICS', :OLD.FLD_ITALICS);
       Audit_Trail.column_delete (raid, 'FLD_KEYWORD', :OLD.FLD_KEYWORD);
       Audit_Trail.column_delete (raid, 'FLD_LENGTH', :OLD.FLD_LENGTH);
       Audit_Trail.column_delete (raid, 'FLD_LIBFLAG', :OLD.FLD_LIBFLAG);
       Audit_Trail.column_delete (raid, 'FLD_LINESNO', :OLD.FLD_LINESNO);
       Audit_Trail.column_delete (raid, 'FLD_LINKEDFORM', :OLD.FLD_LINKEDFORM);
       Audit_Trail.column_delete (raid, 'FLD_LKPDATAVAL', :OLD.FLD_LKPDATAVAL);
       Audit_Trail.column_delete (raid, 'FLD_LKPDISPVAL', :OLD.FLD_LKPDISPVAL);
       Audit_Trail.column_delete (raid, 'FLD_LKPTYPE', :OLD.FLD_LKPTYPE);
       Audit_Trail.column_delete (raid, 'FLD_NAME', :OLD.FLD_NAME);
       Audit_Trail.column_delete (raid, 'FLD_NAME_FORMATTED', :OLD.FLD_NAME_FORMATTED);
       Audit_Trail.column_delete (raid, 'FLD_OVERRIDE_DATE', :OLD.FLD_OVERRIDE_DATE);
       Audit_Trail.column_delete (raid, 'FLD_OVERRIDE_FORMAT', :OLD.FLD_OVERRIDE_FORMAT);
       Audit_Trail.column_delete (raid, 'FLD_OVERRIDE_MANDATORY', :OLD.FLD_OVERRIDE_MANDATORY);
       Audit_Trail.column_delete (raid, 'FLD_OVERRIDE_RANGE', :OLD.FLD_OVERRIDE_RANGE);
       Audit_Trail.column_delete (raid, 'FLD_REPEATFLAG', :OLD.FLD_REPEATFLAG);
       Audit_Trail.column_delete (raid, 'FLD_RESPALIGN', :OLD.FLD_RESPALIGN);
       Audit_Trail.column_delete (raid, 'FLD_SAMELINE', :OLD.FLD_SAMELINE);
       Audit_Trail.column_delete (raid, 'FLD_SORTORDER', :OLD.FLD_SORTORDER);
       Audit_Trail.column_delete (raid, 'FLD_SYSTEMID', :OLD.FLD_SYSTEMID);
       Audit_Trail.column_delete (raid, 'FLD_TODAYCHECK', :OLD.FLD_TODAYCHECK);
       Audit_Trail.column_delete (raid, 'FLD_TYPE', :OLD.FLD_TYPE);
       Audit_Trail.column_delete (raid, 'FLD_UNDERLINE', :OLD.FLD_UNDERLINE);
       Audit_Trail.column_delete (raid, 'FLD_UNIQUEID', :OLD.FLD_UNIQUEID);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr );
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'PK_FIELD', :OLD.PK_FIELD);
       Audit_Trail.column_delete (raid, 'RECORD_TYPE', :OLD.RECORD_TYPE);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
COMMIT;
END;
/
