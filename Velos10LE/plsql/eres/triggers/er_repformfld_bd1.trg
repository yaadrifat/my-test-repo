CREATE OR REPLACE TRIGGER "ER_REPFORMFLD_BD1" 
BEFORE DELETE
ON ER_REPFORMFLD
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
BEGIN
--delete validation data for repeated fields
delete from er_repfldvalidate
where fk_repformfld = :old.pk_repformfld;
END;
/


