CREATE OR REPLACE TRIGGER ER_FLDLIB_AU0
AFTER UPDATE
OF PK_FIELD
  ,FK_CATLIB
  ,FK_ACCOUNT
  ,FLD_LIBFLAG
  ,FLD_NAME
  ,FLD_DESC
  ,FLD_UNIQUEID
  ,FLD_SYSTEMID
  ,FLD_KEYWORD
  ,FLD_TYPE
  ,FLD_DATATYPE
  ,FLD_INSTRUCTIONS
  ,FLD_LENGTH
  ,FLD_DECIMAL
  ,FLD_LINESNO
  ,FLD_CHARSNO
  ,FLD_DEFRESP
  ,FK_LOOKUP
  ,FLD_ISUNIQUE
  ,FLD_ISREADONLY
  ,FLD_ISVISIBLE
  ,FLD_COLCOUNT
  ,FLD_FORMAT
  ,FLD_REPEATFLAG
  ,FLD_BOLD
  ,FLD_ITALICS
  ,FLD_SAMELINE
  ,FLD_ALIGN
  ,FLD_UNDERLINE
  ,FLD_COLOR
  ,FLD_FONT
  ,RECORD_TYPE
  ,FLD_FONTSIZE
  ,FLD_LKPDATAVAL
  ,FLD_LKPDISPVAL
  ,LAST_MODIFIED_BY
  ,LAST_MODIFIED_DATE
  ,IP_ADD
  ,FLD_TODAYCHECK
  ,FLD_OVERRIDE_MANDATORY
  ,FLD_LKPTYPE
  ,FLD_EXPLABEL
  ,FLD_OVERRIDE_FORMAT
  ,FLD_OVERRIDE_RANGE
  ,FLD_OVERRIDE_DATE
  ,FLD_HIDELABEL
  ,FLD_HIDERESPLABEL
  ,FLD_DISPLAY_WIDTH
  ,FLD_LINKEDFORM
  ,FLD_RESPALIGN
  ,FLD_NAME_FORMATTED
ON ER_FLDLIB REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
  raid NUMBER(10);
   usr VARCHAR2(100);
   old_modby VARCHAR2(100);
   new_modby VARCHAR2(100);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
   usr := Getuser(:NEW.last_modified_by);
  Audit_Trail.record_transaction
    (raid, 'ER_FLDLIB', :OLD.rid, 'U', usr);
  IF NVL(:OLD.pk_field,0) !=
     NVL(:NEW.pk_field,0) THEN
     Audit_Trail.column_update
       (raid, 'PK_FIELD',
       :OLD.pk_field, :NEW.pk_field);
  END IF;
  IF NVL(:OLD.fk_catlib,0) !=
     NVL(:NEW.fk_catlib,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_CATLIB',
       :OLD.fk_catlib, :NEW.fk_catlib);
  END IF;
  IF NVL(:OLD.fk_account,0) !=
     NVL(:NEW.fk_account,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_ACCOUNT',
       :OLD.fk_account, :NEW.fk_account);
  END IF;
  IF NVL(:OLD.fld_libflag,' ') !=
     NVL(:NEW.fld_libflag,' ') THEN
     Audit_Trail.column_update
       (raid, 'FLD_LIBFLAG',
       :OLD.fld_libflag, :NEW.fld_libflag);
  END IF;
  IF NVL(:OLD.fld_name,' ') !=
     NVL(:NEW.fld_name,' ') THEN
     Audit_Trail.column_update
       (raid, 'FLD_NAME',
       :OLD.fld_name, :NEW.fld_name);
  END IF;
  IF NVL(:OLD.fld_desc,' ') !=
     NVL(:NEW.fld_desc,' ') THEN
     Audit_Trail.column_update
       (raid, 'FLD_DESC',
       :OLD.fld_desc, :NEW.fld_desc);
  END IF;
  IF NVL(:OLD.fld_uniqueid,' ') !=
     NVL(:NEW.fld_uniqueid,' ') THEN
     Audit_Trail.column_update
       (raid, 'FLD_UNIQUEID',
       :OLD.fld_uniqueid, :NEW.fld_uniqueid);
  END IF;
  IF NVL(:OLD.fld_systemid,' ') !=
     NVL(:NEW.fld_systemid,' ') THEN
     Audit_Trail.column_update
       (raid, 'FLD_SYSTEMID',
       :OLD.fld_systemid, :NEW.fld_systemid);
  END IF;
  IF NVL(:OLD.fld_keyword,' ') !=
     NVL(:NEW.fld_keyword,' ') THEN
     Audit_Trail.column_update
       (raid, 'FLD_KEYWORD',
       :OLD.fld_keyword, :NEW.fld_keyword);
  END IF;
  IF NVL(:OLD.fld_type,' ') !=
     NVL(:NEW.fld_type,' ') THEN
     Audit_Trail.column_update
       (raid, 'FLD_TYPE',
       :OLD.fld_type, :NEW.fld_type);
  END IF;
  IF NVL(:OLD.fld_datatype,' ') !=
     NVL(:NEW.fld_datatype,' ') THEN
     Audit_Trail.column_update
       (raid, 'FLD_DATATYPE',
       :OLD.fld_datatype, :NEW.fld_datatype);
  END IF;
  IF NVL(:OLD.fld_instructions,' ') !=
     NVL(:NEW.fld_instructions,' ') THEN
     Audit_Trail.column_update
       (raid, 'FLD_INSTRUCTIONS',
       :OLD.fld_instructions, :NEW.fld_instructions);
  END IF;
  IF NVL(:OLD.fld_length,0) !=
     NVL(:NEW.fld_length,0) THEN
     Audit_Trail.column_update
       (raid, 'FLD_LENGTH',
       :OLD.fld_length, :NEW.fld_length);
  END IF;
  IF NVL(:OLD.fld_decimal,0) !=
     NVL(:NEW.fld_decimal,0) THEN
     Audit_Trail.column_update
       (raid, 'FLD_DECIMAL',
       :OLD.fld_decimal, :NEW.fld_decimal);
  END IF;
  IF NVL(:OLD.fld_linesno,0) !=
     NVL(:NEW.fld_linesno,0) THEN
     Audit_Trail.column_update
       (raid, 'FLD_LINESNO',
       :OLD.fld_linesno, :NEW.fld_linesno);
  END IF;
  IF NVL(:OLD.fld_charsno,0) !=
     NVL(:NEW.fld_charsno,0) THEN
     Audit_Trail.column_update
       (raid, 'FLD_CHARSNO',
       :OLD.fld_charsno, :NEW.fld_charsno);
  END IF;
  IF NVL(:OLD.fld_defresp,' ') !=
     NVL(:NEW.fld_defresp,' ') THEN
     Audit_Trail.column_update
       (raid, 'FLD_DEFRESP',
       :OLD.fld_defresp, :NEW.fld_defresp);
  END IF;
  IF NVL(:OLD.fk_lookup,0) !=
     NVL(:NEW.fk_lookup,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_LOOKUP',
       :OLD.fk_lookup, :NEW.fk_lookup);
  END IF;
  IF NVL(:OLD.fld_isunique,0) !=
     NVL(:NEW.fld_isunique,0) THEN
     Audit_Trail.column_update
       (raid, 'FLD_ISUNIQUE',
       :OLD.fld_isunique, :NEW.fld_isunique);
  END IF;
  IF NVL(:OLD.fld_isreadonly,0) !=
     NVL(:NEW.fld_isreadonly,0) THEN
     Audit_Trail.column_update
       (raid, 'FLD_ISREADONLY',
       :OLD.fld_isreadonly, :NEW.fld_isreadonly);
  END IF;
  IF NVL(:OLD.fld_isvisible,0) !=
     NVL(:NEW.fld_isvisible,0) THEN
     Audit_Trail.column_update
       (raid, 'FLD_ISVISIBLE',
       :OLD.fld_isvisible, :NEW.fld_isvisible);
  END IF;
  IF NVL(:OLD.fld_colcount,0) !=
     NVL(:NEW.fld_colcount,0) THEN
     Audit_Trail.column_update
       (raid, 'FLD_COLCOUNT',
       :OLD.fld_colcount, :NEW.fld_colcount);
  END IF;
  IF NVL(:OLD.fld_format,' ') !=
     NVL(:NEW.fld_format,' ') THEN
     Audit_Trail.column_update
       (raid, 'FLD_FORMAT',
       :OLD.fld_format, :NEW.fld_format);
  END IF;
  IF NVL(:OLD.fld_repeatflag,0) !=
     NVL(:NEW.fld_repeatflag,0) THEN
     Audit_Trail.column_update
       (raid, 'FLD_REPEATFLAG',
       :OLD.fld_repeatflag, :NEW.fld_repeatflag);
  END IF;
  IF NVL(:OLD.fld_bold,0) !=
     NVL(:NEW.fld_bold,0) THEN
     Audit_Trail.column_update
       (raid, 'FLD_BOLD',
       :OLD.fld_bold, :NEW.fld_bold);
  END IF;
  IF NVL(:OLD.fld_italics,0) !=
     NVL(:NEW.fld_italics,0) THEN
     Audit_Trail.column_update
       (raid, 'FLD_ITALICS',
       :OLD.fld_italics, :NEW.fld_italics);
  END IF;
  IF NVL(:OLD.fld_sameline,0) !=
     NVL(:NEW.fld_sameline,0) THEN
     Audit_Trail.column_update
       (raid, 'FLD_SAMELINE',
       :OLD.fld_sameline, :NEW.fld_sameline);
  END IF;
  IF NVL(:OLD.fld_align,' ') !=
     NVL(:NEW.fld_align,' ') THEN
     Audit_Trail.column_update
       (raid, 'FLD_ALIGN',
       :OLD.fld_align, :NEW.fld_align);
  END IF;
  IF NVL(:OLD.fld_underline,0) !=
     NVL(:NEW.fld_underline,0) THEN
     Audit_Trail.column_update
       (raid, 'FLD_UNDERLINE',
       :OLD.fld_underline, :NEW.fld_underline);
  END IF;
  IF NVL(:OLD.fld_color,' ') !=
     NVL(:NEW.fld_color,' ') THEN
     Audit_Trail.column_update
       (raid, 'FLD_COLOR',
       :OLD.fld_color, :NEW.fld_color);
  END IF;
  IF NVL(:OLD.fld_font,' ') !=
     NVL(:NEW.fld_font,' ') THEN
     Audit_Trail.column_update
       (raid, 'FLD_FONT',
       :OLD.fld_font, :NEW.fld_font);
  END IF;
  IF NVL(:OLD.record_type,' ') !=
     NVL(:NEW.record_type,' ') THEN
     Audit_Trail.column_update
       (raid, 'RECORD_TYPE',
       :OLD.record_type, :NEW.record_type);
  END IF;
  IF NVL(:OLD.fld_fontsize,0) !=
     NVL(:NEW.fld_fontsize,0) THEN
     Audit_Trail.column_update
       (raid, 'FLD_FONTSIZE',
       :OLD.fld_fontsize, :NEW.fld_fontsize);
  END IF;
  IF NVL(:OLD.fld_lkpdataval,' ') !=
     NVL(:NEW.fld_lkpdataval,' ') THEN
     Audit_Trail.column_update
       (raid, 'FLD_LKPDATAVAL',
       :OLD.fld_lkpdataval, :NEW.fld_lkpdataval);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     Audit_Trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.fld_lkpdispval,' ') !=
     NVL(:NEW.fld_lkpdispval,' ') THEN
     Audit_Trail.column_update
       (raid, 'FLD_LKPDISPVAL',
       :OLD.fld_lkpdispval, :NEW.fld_lkpdispval);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     Audit_Trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;
   IF NVL(:OLD.fld_lkptype,' ') !=
     NVL(:NEW.fld_lkptype,' ') THEN
     Audit_Trail.column_update
       (raid, 'FLD_LKPTYPE',
       :OLD.fld_lkptype, :NEW.fld_lkptype);
  END IF;

 IF NVL(:OLD.LAST_MODIFIED_BY,0) !=
 NVL(:NEW.LAST_MODIFIED_BY,0) THEN
	BEGIN
		SELECT  TO_CHAR(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
		INTO old_modby FROM ER_USER  WHERE pk_user = :OLD.last_modified_by ;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			old_modby := NULL;
	END ;
	BEGIN
		SELECT  TO_CHAR(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
		INTO new_modby   FROM ER_USER   WHERE pk_user = :NEW.LAST_MODIFIED_BY ;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			new_modby := NULL;
	 END ;
	Audit_Trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
 END IF;

  IF NVL(:OLD.FLD_TODAYCHECK,0) !=
     NVL(:NEW.FLD_TODAYCHECK,0) THEN
     Audit_Trail.column_update
       (raid, 'FLD_TODAYCHECK',
       :OLD.FLD_TODAYCHECK, :NEW.FLD_TODAYCHECK);
  END IF;

  IF NVL(:OLD.FLD_OVERRIDE_MANDATORY,0) !=
     NVL(:NEW.FLD_OVERRIDE_MANDATORY,0) THEN
     Audit_Trail.column_update
       (raid, 'FLD_OVERRIDE_MANDATORY',
       :OLD.FLD_OVERRIDE_MANDATORY, :NEW.FLD_OVERRIDE_MANDATORY);
  END IF;

  IF NVL(:OLD.FLD_EXPLABEL,0) !=
     NVL(:NEW.FLD_EXPLABEL,0) THEN
     Audit_Trail.column_update
       (raid, 'FLD_EXPLABEL',
       :OLD.FLD_EXPLABEL, :NEW.FLD_EXPLABEL);
  END IF;
   IF NVL(:OLD.FLD_OVERRIDE_FORMAT,0) !=
     NVL(:NEW.FLD_OVERRIDE_FORMAT,0) THEN
     Audit_Trail.column_update
       (raid, 'FLD_OVERRIDE_FORMAT',
       :OLD.FLD_OVERRIDE_FORMAT, :NEW.FLD_OVERRIDE_FORMAT);
  END IF;
     IF NVL(:OLD.FLD_OVERRIDE_RANGE,0) !=
     NVL(:NEW.FLD_OVERRIDE_RANGE,0) THEN
     Audit_Trail.column_update
       (raid, 'FLD_OVERRIDE_RANGE',
       :OLD.FLD_OVERRIDE_RANGE, :NEW.FLD_OVERRIDE_RANGE);
  END IF;
     IF NVL(:OLD.FLD_OVERRIDE_DATE,0) !=
     NVL(:NEW.FLD_OVERRIDE_DATE,0) THEN
     Audit_Trail.column_update
       (raid, 'FLD_OVERRIDE_DATE',
       :OLD.FLD_OVERRIDE_DATE, :NEW.FLD_OVERRIDE_DATE);
  END IF;
   IF NVL(:OLD.FLD_HIDELABEL,0) !=
     NVL(:NEW.FLD_HIDELABEL,0) THEN
     Audit_Trail.column_update
       (raid, 'FLD_HIDELABEL',
       :OLD.FLD_HIDELABEL, :NEW.FLD_HIDELABEL);
  END IF;
  IF NVL(:OLD.FLD_HIDERESPLABEL,0) !=
     NVL(:NEW.FLD_HIDERESPLABEL,0) THEN
     Audit_Trail.column_update
       (raid, 'FLD_HIDERESPLABEL',
       :OLD.FLD_HIDERESPLABEL, :NEW.FLD_HIDERESPLABEL);
  END IF;
  IF NVL(:OLD.FLD_DISPLAY_WIDTH,0) !=
     NVL(:NEW.FLD_DISPLAY_WIDTH,0) THEN
     Audit_Trail.column_update
       (raid, 'FLD_DISPLAY_WIDTH',
       :OLD.FLD_DISPLAY_WIDTH, :NEW.FLD_DISPLAY_WIDTH);
  END IF;
    IF NVL(:OLD.FLD_LINKEDFORM,0) !=
     NVL(:NEW.FLD_LINKEDFORM,0) THEN
     Audit_Trail.column_update
       (raid, 'FLD_LINKEDFORM',
       :OLD.FLD_LINKEDFORM, :NEW.FLD_LINKEDFORM);
  END IF;
  IF NVL(:OLD.FLD_RESPALIGN,' ') !=
     NVL(:NEW.FLD_RESPALIGN,' ') THEN
     Audit_Trail.column_update
       (raid, 'FLD_RESPALIGN',
       :OLD.FLD_RESPALIGN, :NEW.FLD_RESPALIGN);
  END IF;
 IF NVL(:OLD.FLD_NAME_FORMATTED,' ') !=
     NVL(:NEW.FLD_NAME_FORMATTED,' ') THEN
     Audit_Trail.column_update
       (raid, 'FLD_NAME_FORMATTED',
       :OLD.FLD_NAME_FORMATTED, :NEW.FLD_NAME_FORMATTED);
  END IF;
END;
/


