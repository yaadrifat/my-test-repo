CREATE OR REPLACE TRIGGER ERES.ER_STUDYID_AD1_OUT
AFTER DELETE
ON ERES.ER_STUDYID 
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
DECLARE
fkaccount NUMBER;
pkmsg NUMBER;        

pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SVC_OUTBOUND', pLEVEL  => Plog.LFATAL);
/******************************************************************************
   NAME:       ER_STUDYID_AD1_OUT
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        9/23/2011   Kanwal       1. Created this trigger.
******************************************************************************/
BEGIN
  SELECT fk_account
  INTO fkaccount
  FROM er_user
  WHERE pk_user = NVL(:NEW.LAST_MODIFIED_BY, :NEW.CREATOR);

  PKG_MSG_QUEUE.SP_POPULATE_STUDYID_MSG(
      :NEW.PK_STUDYID,
      fkaccount,
      'D',
      to_char(:NEW.fk_study),
      :NEW.FK_CODELST_IDTYPE
      
   );
  EXCEPTION WHEN OTHERS THEN
   Plog.FATAL(pCTX,'exception in ER_STUDYID_AD1_OUT' || SQLERRM);
END ER_STUDYID_AI1_OUT;
/