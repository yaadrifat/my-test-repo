CREATE OR REPLACE TRIGGER "ER_SETTINGS_AU0"
AFTER UPDATE
ON ER_SETTINGS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
--Created by Manimaran for Audit update
declare
   raid number(10);
   usr varchar2(100);
   old_modby varchar2(100);
   new_modby varchar2(100);
begin
   select seq_audit.nextval into raid from dual;
   usr := getuser(:new.last_modified_by);
   audit_trail.record_transaction(raid, 'ER_SETTINGS', :old.rid, 'U', usr);

  if nvl(:old.SETTINGS_PK,0) !=
     NVL(:new.SETTINGS_PK,0) then
     audit_trail.column_update
       (raid, 'SETTINGS_PK',
       :old.SETTINGS_PK, :new.SETTINGS_PK);
  end if;

  if nvl(:old.SETTINGS_MODNUM,0) !=
     NVL(:new.SETTINGS_MODNUM,0) then
     audit_trail.column_update
       (raid, 'SETTINGS_MODNUM',
       :old.SETTINGS_MODNUM, :new.SETTINGS_MODNUM);
  end if;

  if nvl(:old.SETTINGS_MODNAME,0) !=
     NVL(:new.SETTINGS_MODNAME,0) then
     audit_trail.column_update
       (raid, 'SETTINGS_MODNAME',
       :old.SETTINGS_MODNAME, :new.SETTINGS_MODNAME);
  end if;

  if nvl(:old.SETTINGS_KEYWORD,' ') !=
     NVL(:new.SETTINGS_KEYWORD,' ') then
     audit_trail.column_update
       (raid, 'SETTINGS_KEYWORD',
       :old.SETTINGS_KEYWORD, :new.SETTINGS_KEYWORD);
  end if;

  if nvl(:old.SETTINGS_VALUE,' ') !=
     NVL(:new.SETTINGS_VALUE,' ') then
     audit_trail.column_update
       (raid, 'SETTINGS_VALUE',
       :old.SETTINGS_VALUE, :new.SETTINGS_VALUE);
  end if;

  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update(raid, 'RID',:old.rid, :new.rid);
  end if;

  if nvl(:old.LAST_MODIFIED_BY,0) != NVL(:new.LAST_MODIFIED_BY,0) then
     Begin
	Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
	into old_modby from er_user  where pk_user = :old.last_modified_by ;
     Exception When NO_DATA_FOUND then
	old_modby := null;
     End ;
     Begin
	Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
	into new_modby   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
     Exception When NO_DATA_FOUND then
	new_modby := null;
     End ;
     audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
  end if;

  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update(raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT),to_char(:new.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;

  if nvl(:old.ip_add,' ') != NVL(:new.ip_add,' ') then
     audit_trail.column_update(raid, 'IP_ADD',:old.ip_add, :new.ip_add);
  end if;
end;
/


