CREATE  OR REPLACE TRIGGER ER_FORMNOTIFY_BI0 BEFORE INSERT ON ER_FORMNOTIFY        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_FORMNOTIFY',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON',TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr );
       Audit_Trail.column_insert (raid, 'FK_FLDLIB', :NEW.FK_FLDLIB);
       Audit_Trail.column_insert (raid, 'FK_FLDVALIDATE', :NEW.FK_FLDVALIDATE);
       Audit_Trail.column_insert (raid, 'FK_FORMLIB', :NEW.FK_FORMLIB);
       Audit_Trail.column_insert (raid, 'FN_MSGTEXT', :NEW.FN_MSGTEXT);
       Audit_Trail.column_insert (raid, 'FN_MSGTYPE', :NEW.FN_MSGTYPE);
       Audit_Trail.column_insert (raid, 'FN_SENDTYPE', :NEW.FN_SENDTYPE);
       Audit_Trail.column_insert (raid, 'FN_USERS', :NEW.FN_USERS);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'PK_FN', :NEW.PK_FN);
       Audit_Trail.column_insert (raid, 'RECORD_TYPE', :NEW.RECORD_TYPE);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/
