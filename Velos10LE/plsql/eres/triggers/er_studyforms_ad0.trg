CREATE  OR REPLACE TRIGGER ER_STUDYFORMS_AD0 AFTER DELETE ON ER_STUDYFORMS        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_STUDYFORMS', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr );
       Audit_Trail.column_delete (raid, 'FK_FORMLIB', :OLD.FK_FORMLIB);
       Audit_Trail.column_delete (raid, 'FK_FORMLIBVER', :OLD.FK_FORMLIBVER);
       Audit_Trail.column_delete (raid, 'FK_SPECIMEN', :OLD.FK_SPECIMEN);
       Audit_Trail.column_delete (raid, 'FK_STUDY', :OLD.FK_STUDY);
       Audit_Trail.column_delete (raid, 'FORM_COMPLETED', :OLD.FORM_COMPLETED);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'ISVALID', :OLD.ISVALID);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr );
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'NOTIFICATION_SENT', :OLD.NOTIFICATION_SENT);
       Audit_Trail.column_delete (raid, 'PK_STUDYFORMS', :OLD.PK_STUDYFORMS);
       Audit_Trail.column_delete (raid, 'PROCESS_DATA', :OLD.PROCESS_DATA);
       Audit_Trail.column_delete (raid, 'RECORD_TYPE', :OLD.RECORD_TYPE);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
       Audit_Trail.column_delete (raid, 'STUDYFORMS_FILLDATE', :OLD.STUDYFORMS_FILLDATE);
      -- Audit_Trail.column_delete (raid, 'STUDYFORMS_XML', :OLD.STUDYFORMS_XML);
      
COMMIT;
END;
/
