CREATE OR REPLACE TRIGGER ER_PATLABS_AU0 AFTER UPDATE ON ER_PATLABS FOR EACH ROW
declare
   raid number(10);
   usr varchar2(100);
   old_modby varchar2(100);
   new_modby varchar2(100);
begin
  select seq_audit.nextval into raid from dual;
  usr := getuser(:new.last_modified_by);
  audit_trail.record_transaction(raid, 'ER_PATLABS', :old.rid, 'U', usr);

  if nvl(:old.PK_PATLABS,0) != NVL(:new.PK_PATLABS,0) then
     audit_trail.column_update(raid, 'PK_PATLABS',:old.PK_PATLABS, :new.PK_PATLABS);
  end if;

  if nvl(:old.FK_PER,0) !=
     NVL(:new.FK_PER,0) then
     audit_trail.column_update
       (raid, 'FK_PER',
       :old.FK_PER, :new.FK_PER);
  end if;


  if nvl(:old.FK_TESTID,0) !=
     NVL(:new.FK_TESTID,0) then
     audit_trail.column_update
       (raid, 'FK_TESTID',
       :old.FK_TESTID, :new.FK_TESTID);
  end if;


  IF NVL(:OLD.TEST_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.TEST_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update(raid, 'TEST_DATE',
     to_char(:OLD.TEST_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.TEST_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  if nvl(:old.TEST_RESULT,' ') !=
     NVL(:new.TEST_RESULT,' ') then
     audit_trail.column_update
       (raid, 'TEST_RESULT',
       :old.TEST_RESULT, :new.TEST_RESULT);
  end if;

  if nvl(:old.TEST_TYPE,' ') !=
     NVL(:new.TEST_TYPE,' ') then
     audit_trail.column_update
       (raid, 'TEST_TYPE',
       :old.TEST_TYPE, :new.TEST_TYPE);
  end if;

  if nvl(:old.TEST_UNIT,' ') !=
     NVL(:new.TEST_UNIT,' ') then
     audit_trail.column_update
       (raid, 'TEST_UNIT',
       :old.TEST_UNIT, :new.TEST_UNIT);
  end if;

  if nvl(:old.TEST_RANGE,' ') !=
     NVL(:new.TEST_RANGE,' ') then
     audit_trail.column_update
       (raid, 'TEST_RANGE',
       :old.TEST_RANGE, :new.TEST_RANGE);
  end if;

  if nvl(:old.TEST_LLN,' ') !=
     NVL(:new.TEST_LLN,' ') then
     audit_trail.column_update
       (raid, 'TEST_LLN',
       :old.TEST_LLN, :new.TEST_LLN);
  end if;

  if nvl(:old.TEST_ULN,' ') !=
     NVL(:new.TEST_ULN,' ') then
     audit_trail.column_update
       (raid, 'TEST_ULN',
       :old.TEST_ULN, :new.TEST_ULN);
  end if;

  if nvl(:old.FK_SITE,0) !=
     NVL(:new.FK_SITE,0) then
     audit_trail.column_update
       (raid, 'FK_SITE',
       :old.FK_SITE, :new.FK_SITE);
  end if;

 if nvl(:old.FK_CODELST_TSTSTAT,0) !=
     NVL(:new.FK_CODELST_TSTSTAT,0) then
     audit_trail.column_update
       (raid, 'FK_CODELST_TSTSTAT',
       :old.FK_CODELST_TSTSTAT, :new.FK_CODELST_TSTSTAT);
  end if;

  if nvl(:old.ACCESSION_NUM,' ') !=
     NVL(:new.ACCESSION_NUM,' ') then
     audit_trail.column_update
       (raid, 'ACCESSION_NUM',
       :old.ACCESSION_NUM, :new.ACCESSION_NUM);
  end if;


  if nvl(:old.FK_TESTGROUP,0) !=
     NVL(:new.FK_TESTGROUP,0) then
     audit_trail.column_update
       (raid, 'FK_TESTGROUP',
       :old.FK_TESTGROUP, :new.FK_TESTGROUP);
  end if;

  if nvl(:old.FK_CODELST_ABFLAG,0) !=
     NVL(:new.FK_CODELST_ABFLAG,0) then
     audit_trail.column_update
       (raid, 'FK_CODELST_ABFLAG',
       :old.FK_CODELST_ABFLAG, :new.FK_CODELST_ABFLAG);
  end if;

  if nvl(:old.FK_PATPROT,0) !=
     NVL(:new.FK_PATPROT,0) then
     audit_trail.column_update
       (raid, 'FK_PATPROT',
       :old.FK_PATPROT, :new.FK_PATPROT);
  end if;

   if nvl(:old.FK_EVENT_ID,0) !=
     NVL(:new.FK_EVENT_ID,0) then
     audit_trail.column_update
       (raid, 'FK_EVENT_ID',
       :old.FK_EVENT_ID, :new.FK_EVENT_ID);
  end if;

  if nvl(:old.CUSTOM001,' ') !=
     NVL(:new.CUSTOM001,' ') then
     audit_trail.column_update
       (raid, 'CUSTOM001',
       :old.CUSTOM001, :new.CUSTOM001);
  end if;

  if nvl(:old.CUSTOM002,' ') !=
     NVL(:new.CUSTOM002,' ') then
     audit_trail.column_update
       (raid, 'CUSTOM002',
       :old.CUSTOM002, :new.CUSTOM002);
  end if;
  if nvl(:old.CUSTOM003,' ') !=
     NVL(:new.CUSTOM003,' ') then
     audit_trail.column_update
       (raid, 'CUSTOM003',
       :old.CUSTOM003, :new.CUSTOM003);
  end if;
  if nvl(:old.CUSTOM004,' ') !=
     NVL(:new.CUSTOM004,' ') then
     audit_trail.column_update
       (raid, 'CUSTOM004',
       :old.CUSTOM004, :new.CUSTOM004);
  end if;
  if nvl(:old.CUSTOM005,' ') !=
     NVL(:new.CUSTOM005,' ') then
     audit_trail.column_update
       (raid, 'CUSTOM005',
       :old.CUSTOM005, :new.CUSTOM005);
  end if;
  if nvl(:old.CUSTOM006,' ') !=
     NVL(:new.CUSTOM006,' ') then
     audit_trail.column_update
       (raid, 'CUSTOM006',
       :old.CUSTOM006, :new.CUSTOM006);
  end if;
  if nvl(:old.CUSTOM007,' ') !=
     NVL(:new.CUSTOM007,' ') then
     audit_trail.column_update
       (raid, 'CUSTOM007',
       :old.CUSTOM007, :new.CUSTOM007);
  end if;
  if nvl(:old.CUSTOM008,' ') !=
     NVL(:new.CUSTOM008,' ') then
     audit_trail.column_update
       (raid, 'CUSTOM008',
       :old.CUSTOM008, :new.CUSTOM008);
  end if;
  if nvl(:old.CUSTOM009,' ') !=
     NVL(:new.CUSTOM009,' ') then
     audit_trail.column_update
       (raid, 'CUSTOM009',
       :old.CUSTOM009, :new.CUSTOM009);
  end if;
  if nvl(:old.CUSTOM010,' ') !=
     NVL(:new.CUSTOM010,' ') then
     audit_trail.column_update
       (raid, 'CUSTOM010',
       :old.CUSTOM010, :new.CUSTOM010);
  end if;


  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;

  if nvl(:old.LAST_MODIFIED_BY,0) != NVL(:new.LAST_MODIFIED_BY,0) then
     Begin
	Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
	into old_modby from er_user  where pk_user = :old.last_modified_by ;
     Exception When NO_DATA_FOUND then
	old_modby := null;
     End ;
     Begin
	Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
	into new_modby   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
     Exception When NO_DATA_FOUND then
	new_modby := null;
     End ;
     audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
  end if;

  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update(raid, 'LAST_MODIFIED_DATE',
              to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

  end if;

  if nvl(:old.ip_add,' ') != NVL(:new.ip_add,' ') then
     audit_trail.column_update(raid, 'IP_ADD',:old.ip_add, :new.ip_add);
  end if;

  if nvl(:old.ER_NAME,' ') !=
     NVL(:new.ER_NAME,' ') then
     audit_trail.column_update
       (raid, 'ER_NAME',
       :old.ER_NAME, :new.ER_NAME);
  end if;

  if nvl(:old.ER_GRADE,0) !=
     NVL(:new.ER_GRADE,0) then
     audit_trail.column_update
       (raid, 'ER_GRADE',
       :old.ER_GRADE, :new.ER_GRADE);
  end if;
  if nvl(:old.FK_CODELST_STDPHASE,0) !=
     NVL(:new.FK_CODELST_STDPHASE,0) then
     audit_trail.column_update
       (raid, 'FK_CODELST_STDPHASE',
       :old.FK_CODELST_STDPHASE, :new.FK_CODELST_STDPHASE);
  end if;
  if nvl(:old.FK_STUDY,0) !=
     NVL(:new.FK_STUDY,0) then
     audit_trail.column_update
       (raid, 'FK_STUDY',
       :old.FK_STUDY, :new.FK_STUDY);
  end if;
  if nvl(:old.FK_SPECIMEN,0) !=
     NVL(:new.FK_SPECIMEN,0) then
     audit_trail.column_update
       (raid, 'FK_SPECIMEN',
       :old.FK_SPECIMEN, :new.FK_SPECIMEN);
  end if;
end;
/


