create or replace TRIGGER ER_SPECIMEN_APPNDX_AUO AFTER UPDATE OF RID,IP_ADD,CREATOR,FK_SPECIMEN,CREATED_ON,PK_SPECIMEN_APPNDX,SA_URI,SA_DESC,SA_TYPE ON ERES.ER_SPECIMEN_APPNDX FOR EACH ROW
declare
  raid number(10);
  usr varchar2(2000);
begin
  --KM-#3634
  usr := getuser(:new.last_modified_by);
  select seq_audit.nextval into raid from dual;
  audit_trail.record_transaction
    (raid, 'ER_SPECIMEN_APPNDX', :old.rid, 'U', usr);
  if nvl(:old.pk_specimen_appndx,0) !=
     NVL(:new.pk_specimen_appndx,0) then
     audit_trail.column_update
       (raid, 'PK_SPECIMEN_APPNDX',
       :old.pk_specimen_appndx, :new.pk_specimen_appndx);
  end if;

  if nvl(:old.fk_specimen,0) !=
     NVL(:new.fk_specimen,0) then
     audit_trail.column_update
       (raid, 'FK_SPECIMEN',
       :old.fk_specimen, :new.fk_specimen);
  end if;

  if nvl(:old.sa_desc,' ') !=
     NVL(:new.sa_desc,' ') then
     audit_trail.column_update
       (raid, 'SA_DESC',
       :old.sa_desc, :new.sa_desc);
  end if;

  if nvl(:old.sa_uri,' ') !=
     NVL(:new.sa_uri,' ') then
     audit_trail.column_update
       (raid, 'SA_URI',
       :old.sa_uri, :new.sa_uri);
  end if;

  if nvl(:old.sa_type,' ') !=
     NVL(:new.sa_type,' ') then
     audit_trail.column_update
       (raid, 'SA_TYPE',
       :old.sa_type, :new.sa_type);
  end if;

  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;

  if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :old.last_modified_by, :new.last_modified_by);
  end if;

  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;

  if nvl(:old.ip_add,' ') !=
     NVL(:new.ip_add,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.ip_add, :new.ip_add);
  end if;
end;
/


