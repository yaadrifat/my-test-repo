CREATE OR REPLACE TRIGGER ER_PORTAL_LOGINS_AU0
AFTER UPDATE OF PK_PORTAL_LOGIN, PL_ID, PL_ID_TYPE, PL_LOGIN, PL_PASSWORD, PL_STATUS, PL_LOGOUT_TIME, FK_PORTAL, RID, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD
ON ER_PORTAL_LOGINS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
  usr VARCHAR2(100);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  usr := getuser(:NEW.last_modified_by);
  audit_trail.record_transaction
    (raid, 'ER_PORTAL_LOGINS', :OLD.rid, 'U', usr);

  IF NVL(:OLD.pk_portal_login,0) !=
     NVL(:NEW.pk_portal_login,0) THEN
     audit_trail.column_update
       (raid, 'PK_PORTAL_LOGIN',
       :OLD.pk_portal_login, :NEW.pk_portal_login);
  END IF;

  IF NVL(:OLD.pl_id,0) !=
     NVL(:NEW.pl_id,0) THEN
     audit_trail.column_update
       (raid, 'PL_ID',
       :OLD.pl_id, :NEW.pl_id);
  END IF;

  IF NVL(:OLD.pl_id_type,' ') !=
     NVL(:NEW.pl_id_type,' ') THEN
     audit_trail.column_update
       (raid, 'PL_ID_TYPE',
       :OLD.pl_id_type, :NEW.pl_id_type);
  END IF;

  IF NVL(:OLD.pl_login,' ') !=
     NVL(:NEW.pl_login,' ') THEN
     audit_trail.column_update
       (raid, 'PL_LOGIN',
       :OLD.pl_login, :NEW.pl_login);
  END IF;

  IF NVL(:OLD.pl_password,' ') !=
     NVL(:NEW.pl_password,' ') THEN
     audit_trail.column_update
       (raid, 'PL_PASSWORD',
       :OLD.pl_password, :NEW.pl_password);
  END IF;

  IF NVL(:OLD.pl_status,0) !=
     NVL(:NEW.pl_status,0) THEN
     audit_trail.column_update
       (raid, 'PL_STATUS',
       :OLD.pl_status, :NEW.pl_status);
  END IF;

  IF NVL(:OLD.pl_logout_time,0) !=
     NVL(:NEW.pl_logout_time,0) THEN
     audit_trail.column_update
       (raid, 'PL_LOGOUT_TIME',
       :OLD.pl_logout_time, :NEW.pl_logout_time);
  END IF;

  IF NVL(:OLD.fk_portal,0) !=
     NVL(:NEW.fk_portal,0) THEN
     audit_trail.column_update
       (raid, 'FK_PORTAL',
       :OLD.fk_portal, :NEW.fk_portal);
  END IF;

  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID', :OLD.rid, :NEW.rid);
  END IF;

  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
   BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM ER_USER
     WHERE pk_user = :OLD.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      old_modby := NULL ;
   END ;
   BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM ER_USER
     WHERE pk_user = :NEW.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      new_modby := NULL ;
   END ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  END IF;

  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  IF NVL(:OLD.IP_ADD,' ') !=
     NVL(:NEW.IP_ADD,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.IP_ADD, :NEW.IP_ADD);
  END IF;

END;
/


