CREATE OR REPLACE TRIGGER "ER_FORMFLD_AU3" 
AFTER UPDATE OF LAST_MODIFIED_DATE
ON ER_FORMFLD
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
	v_last_modifiedby NUMBER ;
	v_ipadd VARCHAR2(15) ;
	v_fldlib_id_old NUMBER ;
    v_fldlib_id_new NUMBER ;
	v_fld_tocopy NUMBER(10);
	v_new_repformfld NUMBER(10);

	v_fld_systemid_old VARCHAR2(50) ;
	v_fld_systemid_new VARCHAR2(50) ;
	v_formfld_javascr VARCHAR2(4000) ;
	v_formfld_javascr_init VARCHAR2(4000) ;

    v_fldval_javascr_init VARCHAR2(4000) ;

	v_fk_formsec   NUMBER ;
	v_formsec_fmt CHAR(1);
	v_formfld_seq NUMBER ;
	v_fld_name VARCHAR2(50);
    v_output  NUMBER ;
    v_formfld_xsl   VARCHAR2(4000);
    v_formfld_xsl_init VARCHAR2(4000);
    v_formfld_xsl_1   VARCHAR2(4000);
    v_formfld_xsl_2   VARCHAR2(4000);
    v_formfld_xsl_3   VARCHAR2(4000);
    v_pos_1  NUMBER ;
    v_pos_2  NUMBER ;
    v_pos_3  NUMBER ;
    v_pos_endtd NUMBER;
    len NUMBER ;
	v_strlen NUMBER ;
	v_fld_type CHAR(1);
	v_fld_datatype VARCHAR2(3);
	v_fld_align VARCHAR2(20);
	BEGIN
	-- Select the values from ER_FORMFLD to the variables
		v_fk_formsec := :NEW.FK_FORMSEC ;
		v_formfld_seq := :NEW.FORMFLD_SEQ ;
		v_formfld_xsl_init := :NEW.FORMFLD_XSL ;
		v_formfld_javascr_init :=  :NEW.FORMFLD_JAVASCR;
		v_last_modifiedby := :NEW.LAST_MODIFIED_BY ;
		v_fldlib_id_old  := :NEW.FK_FIELD ;
		v_ipadd := :NEW.IP_ADD ;
		 --We get the the FLD_SYSTEMID of the original Field from the ER_FLDLIB table for using in the XSL
		SELECT FLD_SYSTEMID
		INTO v_fld_systemid_old
		FROM ER_FLDLIB
		WHERE PK_FIELD = v_fldlib_id_old   ;

		-- Fix for repeat fields bugs
		BEGIN
		SELECT
		FLDVALIDATE_JAVASCR
		INTO
		v_fldval_javascr_init
		FROM er_fldvalidate
		WHERE fk_fldlib = v_fldlib_id_old;
		EXCEPTION  WHEN NO_DATA_FOUND THEN
		v_fldval_javascr_init := ' ';
		END;

	     FOR i  IN ( SELECT pk_repformfld, fk_field
		 	 	     FROM ER_REPFORMFLD
					WHERE fk_formfld = :NEW.pk_formfld AND
					record_type <> 'D' ORDER BY repformfld_set, repformfld_seq )
			LOOP
		   --Update the copies of the ER_FLDLIB by taking the data of the original field
		   -- from the fk_field of the ER_FORMFLD
			v_fldlib_id_new := i.fk_field;
		     Pkg_Form.SP_FLD_COPY (v_fldlib_id_old ,v_fldlib_id_new , v_output ) ;
		   	IF  v_output >= 1  THEN
		   	   --We get the the new FLD_SYSTEMID from the ER_FLDLIB table for using in the XSL
			  	SELECT FLD_SYSTEMID , fld_type, fld_datatype,fld_align
				INTO v_fld_systemid_new , v_fld_type, v_fld_datatype,v_fld_align
				FROM ER_FLDLIB
				WHERE PK_FIELD = v_fldlib_id_new   ;
				v_formfld_xsl_1 := REPLACE(v_formfld_xsl_init, v_fld_systemid_old ,v_fld_systemid_new);
	            v_formfld_javascr := REPLACE(v_formfld_javascr_init , v_fld_systemid_old , v_fld_systemid_new);
                      --Find  the Format of the Section
		          SELECT formsec_fmt
          	      INTO v_formsec_fmt
		   	      FROM ER_FORMSEC
			      WHERE  pk_formsec = v_fk_formsec ;
                     --we change the xsl and remove the <label> tags if the section format is tabular
                IF v_formsec_fmt = 'T'  THEN
			   		--modified by sonia abrol, 03/16/05 to use a function to cut the label
			 		 SELECT Pkg_Form.f_cut_fieldlabel(v_formfld_xsl_1,v_fld_type, v_fld_datatype,v_fld_align,v_fldlib_id_new )
			 		 INTO v_formfld_xsl  FROM dual;


				/*
 			        --modified by Sonika on Nov 21, 03 search only for '<td width=''15%'' instead of '<td width=''15%'' ><label' as align has also been added
                    --modified by Sonika on April 19, 04 search only for <td
          		    v_pos_1 := INSTR( v_formfld_xsl_1, '<td') ;
	     	        v_formfld_xsl_2 := SUBSTR(v_formfld_xsl_1 , 0 ,v_pos_1-1);
		            len := LENGTH(v_formfld_xsl_1);
			        v_pos_2 := INSTR(v_formfld_xsl_1 , '</label>');
		            --v_strlen := length( '</label>  &#xa0;&#xa0;&#xa0;</td>' );
                    --Modified by Sonika Talwar on April 22, 04 to remove help icon addition incase of Tabular section
                    --find </td> end tag of label
   		            v_pos_endtd := INSTR(v_formfld_xsl_1 , '</td>',v_pos_2);
                    --# of characters between </label> and </td>
			        v_strlen := LENGTH('</td>');
			        v_strlen := (v_pos_endtd - v_pos_2) + v_strlen;
                    v_formfld_xsl_3 := SUBSTR( v_formfld_xsl_1 , v_pos_2+v_strlen , len-1 ) ;
                    v_formfld_xsl := v_formfld_xsl_2 || v_formfld_xsl_3 ; */
			 ELSE
			       v_formfld_xsl := v_formfld_xsl_1 ;
			 END IF ;
				--Update all the respective data from existing ER_FORMFLD to ER_REPFORMFLD
				UPDATE ER_REPFORMFLD SET
				   REPFORMFLD_XSL  = v_formfld_xsl ,
				   REPFORMFLD_JAVASCR = v_formfld_javascr,
				   REPFORMFLD_SEQ = v_formfld_seq ,
				   RECORD_TYPE = 'M' ,
				   LAST_MODIFIED_BY = v_last_modifiedby ,
				   IP_ADD =  v_ipadd
				WHERE pk_repformfld = i.pk_repformfld ;
				--update repeated field validation javascript
				UPDATE ER_REPFLDVALIDATE SET
				   REPFLDVALIDATE_JAVASCR = REPLACE(v_fldval_javascr_init , v_fld_systemid_old , v_fld_systemid_new),
--		   		   REPLACE (REPFLDVALIDATE_JAVASCR, v_fld_systemid_old , v_fld_systemid_new) ,
				   RECORD_TYPE = 'M' ,
				   LAST_MODIFIED_BY = v_last_modifiedby ,
				   IP_ADD =  v_ipadd
				WHERE fk_repformfld = i.pk_repformfld ;
			 END IF ;
		END LOOP ; --for loop for the er_repformfld
	END ;
/


