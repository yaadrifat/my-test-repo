CREATE OR REPLACE TRIGGER "ER_STUDY_AU_STUDYVER" 
AFTER UPDATE OF STUDY_ACTUALDT
ON ER_STUDY REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE

v_new_pk_study NUMBER;
v_new_last_modified_by NUMBER;
v_new_ip_add VARCHAR2(15);

BEGIN

v_new_pk_study := :NEW.pk_study ;
v_new_last_modified_by := :NEW.last_modified_by;
v_new_ip_add := :NEW.ip_add ;

-- commented for new req for keeping alert notification settings only at study level
--pkg_alnot.set_study_alnot( v_new_pk_study ,v_new_last_modified_by, sysdate , v_new_ip_add );
--
 END;
/


