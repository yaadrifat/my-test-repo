CREATE OR REPLACE TRIGGER "ER_FLDRESP_BD0" 
  BEFORE UPDATE OF record_type  --delete
  ON ER_FLDRESP
  FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);

rtype CHAR;
BEGIN

  rtype:= :NEW.record_type;

 IF(rtype='D') THEN
 			  :NEW.fk_field := NULL; --temporary solution, make the record orphan
 END IF;

END;
/


