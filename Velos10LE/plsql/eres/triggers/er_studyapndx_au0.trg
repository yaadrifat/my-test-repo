CREATE OR REPLACE TRIGGER ER_STUDYAPNDX_AU0
AFTER UPDATE
ON ER_STUDYAPNDX REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  old_modby varchar2(100) ;
  new_modby varchar2(100) ;
  old_study varchar2(1000) ;
  new_study varchar2(1000) ;
  usr varchar2(100);
begin
  select seq_audit.nextval into raid from dual;

  usr := getuser(:new.last_modified_by);
  audit_trail.record_transaction
    (raid, 'ER_STUDYAPNDX', :old.rid, 'U', usr);
  if nvl(:old.pk_studyapndx,0) !=
     NVL(:new.pk_studyapndx,0) then
     audit_trail.column_update
       (raid, 'PK_STUDYAPNDX',
       :old.pk_studyapndx, :new.pk_studyapndx);
  end if;
  if nvl(:old.fk_study,0) !=
     NVL(:new.fk_study,0) then
    Begin
     select study_title into old_study
     from er_study where pk_study = :old.fk_study ;
    Exception When NO_DATA_FOUND then
     old_study := null ;
    End ;
    Begin
     select study_title into new_study
     from er_study where pk_study = :new.fk_study ;
    Exception When NO_DATA_FOUND then
     new_study := null ;
    End ;
     audit_trail.column_update
       (raid, 'FK_STUDY',
       old_study, new_study);
  end if;
  if nvl(:old.studyapndx_type,' ') !=
     NVL(:new.studyapndx_type,' ') then
     audit_trail.column_update
       (raid, 'STUDYAPNDX_TYPE',
       :old.studyapndx_type, :new.studyapndx_type);
  end if;
  if nvl(:old.studyapndx_uri,' ') !=
     NVL(:new.studyapndx_uri,' ') then
     audit_trail.column_update
       (raid, 'STUDYAPNDX_URI',
       :old.studyapndx_uri, :new.studyapndx_uri);
  end if;
  if nvl(:old.studyapndx_desc,' ') !=
     NVL(:new.studyapndx_desc,' ') then
     audit_trail.column_update
       (raid, 'STUDYAPNDX_DESC',
       :old.studyapndx_desc, :new.studyapndx_desc);
  end if;
  if nvl(:old.studyapndx_pubflag,' ') !=
     NVL(:new.studyapndx_pubflag,' ') then
     audit_trail.column_update
       (raid, 'STUDYAPNDX_PUBFLAG',
       :old.studyapndx_pubflag, :new.studyapndx_pubflag);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
    Begin
     select to_char(pk_user) ||', '|| usr_lastname ||', '||usr_firstname
     into old_modby
     from er_user
     where pk_user = :old.last_modified_by ;
    Exception When NO_DATA_FOUND then
     old_modby := null ;
    End ;
    Begin
     select to_char(pk_user) ||', '|| usr_lastname ||', '||usr_firstname
     into new_modby
     from er_user
     where pk_user = :new.last_modified_by ;
    Exception When NO_DATA_FOUND then
     new_modby := null ;
    End ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  end if;
  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
              to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

  end if;
  if nvl(:old.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
    if nvl(:old.studyapndx_file,' ') !=
     NVL(:new.studyapndx_file,' ') then
     audit_trail.column_update
       (raid, 'STUDYAPNDX_FILE',
       :old.studyapndx_file, :new.studyapndx_file);
  end if;
  if nvl(:old.IP_ADD,' ') !=
     NVL(:new.IP_ADD,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.IP_ADD, :new.IP_ADD);
  end if;
end;
/


