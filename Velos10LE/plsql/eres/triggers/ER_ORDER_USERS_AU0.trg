CREATE OR REPLACE TRIGGER ER_ORDER_USERS_AU0  AFTER UPDATE OF PK_ORDER_USER,
USER_LOGINID,
USER_FNAME,
USER_LNAME,
USER_MAILID,
USER_CONTACTNO,
USER_COMMENTS,
CREATOR,
CREATED_ON,
LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,
IP_ADD,
DELETEDFLAG,
RID ON ER_ORDER_USERS REFERENCING OLD AS OLD NEW AS NEW

FOR EACH ROW
DECLARE
   raid NUMBER(10);
   usr VARCHAR(200); 
   old_modified_by VARCHAR2(100);
   new_modified_by VARCHAR2(100);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  
   usr := getuser(:NEW.LAST_MODIFIED_BY);
   
  audit_trail.record_transaction
    (raid, 'ER_ORDER_USERS', :OLD.RID, 'U', usr);
    
  IF NVL(:OLD.PK_ORDER_USER,0) != NVL(:NEW.PK_ORDER_USER,0) THEN
     audit_trail.column_update
       (raid, 'PK_ORDER_USER',
       :OLD.PK_ORDER_USER, :NEW.PK_ORDER_USER);
  END IF;
 IF NVL(:OLD.USER_LOGINID,' ') != NVL(:NEW.USER_LOGINID,' ') THEN
     audit_trail.column_update(raid, 'USER_LOGINID',:OLD.USER_LOGINID, :NEW.USER_LOGINID);
  END IF; 
  IF NVL(:OLD.USER_FNAME,' ') != NVL(:NEW.USER_FNAME,' ') THEN
     audit_trail.column_update
       (raid, 'USER_FNAME',
       :OLD.USER_FNAME, :NEW.USER_FNAME);
  END IF; 
  IF NVL(:OLD.USER_LNAME,' ') != NVL(:NEW.USER_LNAME,' ') THEN
     audit_trail.column_update
       (raid, 'USER_LNAME',
       :OLD.USER_LNAME, :NEW.USER_LNAME);
  END IF;
  IF NVL(:OLD.USER_MAILID,' ') != NVL(:NEW.USER_MAILID,' ') THEN
     audit_trail.column_update
       (raid, 'USER_MAILID',
       :OLD.USER_MAILID, :NEW.USER_MAILID);
  END IF;
  IF NVL(:OLD.USER_CONTACTNO,' ') != NVL(:NEW.USER_CONTACTNO,' ') THEN
     audit_trail.column_update
       (raid, 'USER_CONTACTNO',
       :OLD.USER_CONTACTNO, :NEW.USER_CONTACTNO);
  END IF;
  IF NVL(:OLD.USER_COMMENTS,' ') != NVL(:NEW.USER_COMMENTS,' ') THEN
     audit_trail.column_update
       (raid, 'USER_COMMENTS',
       :OLD.USER_COMMENTS, :NEW.USER_COMMENTS);
  END IF;
   IF NVL(:OLD.CREATOR,0) !=
     NVL(:NEW.CREATOR,0) THEN
     audit_trail.column_update
       (raid, 'CREATOR',
       :OLD.CREATOR, :NEW.CREATOR);
  END IF;  
  IF NVL(:OLD.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
  IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.IP_ADD, :NEW.IP_ADD);     
    END IF;    
  if nvl(:old.LAST_MODIFIED_BY,0) != NVL(:new.LAST_MODIFIED_BY,0) then
    Begin
      Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
      into old_modified_by from er_user  where pk_user = :old.last_modified_by ;
      Exception When NO_DATA_FOUND then
			old_modified_by := null;
    End ;
    Begin
      Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
      into new_modified_by   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
      Exception When NO_DATA_FOUND then
			new_modified_by := null;
    End ;
      audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modified_by, new_modified_by);
    end if;
    IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
	IF NVL(:OLD.DELETEDFLAG,0) != NVL(:NEW.DELETEDFLAG,0) THEN
     audit_trail.column_update(raid, 'DELETEDFLAG',:OLD.DELETEDFLAG, :NEW.DELETEDFLAG);
  END IF;
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.RID, :NEW.RID);     
    END IF;
END ;
/

