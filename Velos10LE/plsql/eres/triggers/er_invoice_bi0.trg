CREATE  OR REPLACE TRIGGER ER_INVOICE_BI0 BEFORE INSERT ON ER_INVOICE        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_INVOICE',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'COVERAGE_TYPE_CRITERIA', :NEW.COVERAGE_TYPE_CRITERIA);
       Audit_Trail.column_insert (raid, 'CREATED_ON',TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr );
       Audit_Trail.column_insert (raid, 'FK_SITE', :NEW.FK_SITE);
       Audit_Trail.column_insert (raid, 'FK_STUDY', :NEW.FK_STUDY);
       Audit_Trail.column_insert (raid, 'INT_ACC_NUM', :NEW.INT_ACC_NUM);
       Audit_Trail.column_insert (raid, 'INV_ADDRESSEDTO', :NEW.INV_ADDRESSEDTO);
       Audit_Trail.column_insert (raid, 'INV_DATE', :NEW.INV_DATE);
       Audit_Trail.column_insert (raid, 'INV_FOOTER', :NEW.INV_FOOTER);
       Audit_Trail.column_insert (raid, 'INV_HEADER', :NEW.INV_HEADER);
       Audit_Trail.column_insert (raid, 'INV_INTERVALFROM', :NEW.INV_INTERVALFROM);
       Audit_Trail.column_insert (raid, 'INV_INTERVALTO', :NEW.INV_INTERVALTO);
       Audit_Trail.column_insert (raid, 'INV_INTERVALTYPE', :NEW.INV_INTERVALTYPE);
       Audit_Trail.column_insert (raid, 'INV_MILESTATUSTYPE', :NEW.INV_MILESTATUSTYPE);
       Audit_Trail.column_insert (raid, 'INV_NOTES', :NEW.INV_NOTES);
       Audit_Trail.column_insert (raid, 'INV_NUMBER', :NEW.INV_NUMBER);
       Audit_Trail.column_insert (raid, 'INV_OTHER_USER', :NEW.INV_OTHER_USER);
       Audit_Trail.column_insert (raid, 'INV_PAYMENT_DUEBY', :NEW.INV_PAYMENT_DUEBY);
       Audit_Trail.column_insert (raid, 'INV_PAYUNIT', :NEW.INV_PAYUNIT);
       Audit_Trail.column_insert (raid, 'INV_SENTFROM', :NEW.INV_SENTFROM);
       Audit_Trail.column_insert (raid, 'INV_STATUS', :NEW.INV_STATUS);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'PK_INVOICE', :NEW.PK_INVOICE);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/
