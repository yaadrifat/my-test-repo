create or replace TRIGGER "ER_STORAGE_STATUS_BIO" BEFORE INSERT ON ER_STORAGE_STATUS
FOR EACH ROW
DECLARE
   raid NUMBER(10);
   erid NUMBER(10);
   usr VARCHAR(2000);
   insert_data CLOB;
BEGIN

   BEGIN
      --KM-#3635
      SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname INTO usr FROM er_user
      WHERE pk_user = :NEW.creator ;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         USR := 'New User' ;
   END ;

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
   :NEW.rid := erid ;
   SELECT seq_audit.NEXTVAL INTO raid FROM dual;

   audit_trail.record_transaction(raid, 'ER_STORAGE_STATUS',erid, 'I',usr);

insert_data:= :NEW.PK_STORAGE_STATUS||'|'||:NEW.FK_STORAGE||'|'||TO_CHAR(:NEW.SS_START_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
:NEW.FK_CODELST_STORAGE_STATUS||'|'||
--:NEW.SS_NOTES||'|'|| --KM-to fix the issue3171
:NEW.FK_USER||'|'||:NEW.SS_TRACKING_NUMBER||'|'||:NEW.FK_STUDY||'|'||
:NEW.RID||'|'||:NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'||TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.IP_ADD;

INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END ;
/


