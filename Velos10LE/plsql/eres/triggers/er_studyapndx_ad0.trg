CREATE  OR REPLACE TRIGGER ER_STUDYAPNDX_AD0 AFTER DELETE ON ER_STUDYAPNDX        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_STUDYAPNDX', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr );
       Audit_Trail.column_delete (raid, 'FK_STUDY', :OLD.FK_STUDY);
       Audit_Trail.column_delete (raid, 'FK_STUDYVER', :OLD.FK_STUDYVER);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr );
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'PK_STUDYAPNDX', :OLD.PK_STUDYAPNDX);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
       Audit_Trail.column_delete (raid, 'STUDYAPNDX_DESC', :OLD.STUDYAPNDX_DESC);
       Audit_Trail.column_delete (raid, 'STUDYAPNDX_FILE', :OLD.STUDYAPNDX_FILE);
       --Audit_Trail.column_delete (raid, 'STUDYAPNDX_FILEOBJ', :OLD.STUDYAPNDX_FILEOBJ);
       Audit_Trail.column_delete (raid, 'STUDYAPNDX_FILESIZE', :OLD.STUDYAPNDX_FILESIZE);
       Audit_Trail.column_delete (raid, 'STUDYAPNDX_PUBFLAG', :OLD.STUDYAPNDX_PUBFLAG);
       Audit_Trail.column_delete (raid, 'STUDYAPNDX_TYPE', :OLD.STUDYAPNDX_TYPE);
       Audit_Trail.column_delete (raid, 'STUDYAPNDX_URI', :OLD.STUDYAPNDX_URI);
COMMIT;
END;
/
