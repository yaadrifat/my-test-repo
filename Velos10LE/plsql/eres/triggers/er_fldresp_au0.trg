CREATE OR REPLACE TRIGGER ER_FLDRESP_AU0
  after update of
  pk_fldresp,
  fk_field,
  fldresp_seq,
  fldresp_dispval,
  fldresp_dataval,
  fldresp_score,
  fldresp_isdefault,
  record_type,
  last_modified_by,
  last_modified_date,
  ip_add,
  rid
  on er_fldresp
  for each row
declare
  raid number(10);

   usr varchar2(100);

   old_modby varchar2(100);

   new_modby varchar2(100);

begin
  select seq_audit.nextval into raid from dual;

   usr := getuser(:new.last_modified_by);

  audit_trail.record_transaction
    (raid, 'ER_FLDRESP', :old.rid, 'U', usr);

  if nvl(:old.pk_fldresp,0) !=
     NVL(:new.pk_fldresp,0) then
     audit_trail.column_update
       (raid, 'PK_FLDRESP',
       :old.pk_fldresp, :new.pk_fldresp);
  end if;
  if nvl(:old.fk_field,0) !=
     NVL(:new.fk_field,0) then
     audit_trail.column_update
       (raid, 'FK_FIELD',
       :old.fk_field, :new.fk_field);
  end if;
  if nvl(:old.fldresp_seq,0) !=
     NVL(:new.fldresp_seq,0) then
     audit_trail.column_update
       (raid, 'FLDRESP_SEQ',
       :old.fldresp_seq, :new.fldresp_seq);
  end if;
  if nvl(:old.fldresp_dispval,' ') !=
     NVL(:new.fldresp_dispval,' ') then
     audit_trail.column_update
       (raid, 'FLDRESP_DISPVAL',
       :old.fldresp_dispval, :new.fldresp_dispval);
  end if;
  if nvl(:old.fldresp_dataval,' ') !=
     NVL(:new.fldresp_dataval,' ') then
     audit_trail.column_update
       (raid, 'FLDRESP_DATAVAL',
       :old.fldresp_dataval, :new.fldresp_dataval);
  end if;
  if nvl(:old.fldresp_score,0) !=
     NVL(:new.fldresp_score,0) then
     audit_trail.column_update
       (raid, 'FLDRESP_SCORE',
       :old.fldresp_score, :new.fldresp_score);
  end if;
  if nvl(:old.fldresp_isdefault,0) !=
     NVL(:new.fldresp_isdefault,0) then
     audit_trail.column_update
       (raid, 'FLDRESP_ISDEFAULT',
       :old.fldresp_isdefault, :new.fldresp_isdefault);
  end if;
  if nvl(:old.record_type,' ') !=
     NVL(:new.record_type,' ') then
     audit_trail.column_update
       (raid, 'RECORD_TYPE',
       :old.record_type, :new.record_type);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ip_add,' ') !=
     NVL(:new.ip_add,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.ip_add, :new.ip_add);
  end if;
 if nvl(:old.LAST_MODIFIED_BY,0) !=
 NVL(:new.LAST_MODIFIED_BY,0) then
	Begin
		Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
		into old_modby from er_user  where pk_user = :old.last_modified_by ;
		Exception When NO_DATA_FOUND then
			old_modby := null;
	End ;
	Begin
		Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
		into new_modby   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
		Exception When NO_DATA_FOUND then
			new_modby := null;
	 End ;
	audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
 end if;

end;
/


