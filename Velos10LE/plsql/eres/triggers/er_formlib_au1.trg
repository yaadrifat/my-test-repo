CREATE OR REPLACE TRIGGER "ER_FORMLIB_AU1" 
AFTER UPDATE OF FORM_STATUS
ON ER_FORMLIB
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
WHEN (
OLD.FORM_LINKTO <> 'L'
      )
DECLARE
v_offline_status Number;
v_active_status Number;
BEGIN
/*
**When form status is changed to offline for editing, Set Formfld_offline to 1 to indicate that the data was there before the form was made off line for editing.
**This is required because we don't have to allow the user to delete the form fields, form sections and field responses
**which were created before changing the status to offline for editing but incase he adds a new field, he should be able to delete
**that.
** Modified by Sonia Abrol, 02/09/06, there was no check if the status is changed or not, so it was fired everytime the form record was updated
*/
--get offline for editing status

if ( :new.form_status <> :old.form_status) then -- execute the logic only if form status is changed
		select pk_codelst
		 into v_offline_status
		 from er_codelst where codelst_type='frmstat' and codelst_subtyp='O';

		--get offline for active status
		select pk_codelst
		 into v_active_status
		 from er_codelst where codelst_type='frmstat' and codelst_subtyp='A';

		if :new.form_status = v_offline_status then
		  pkg_lnkform.sp_set_offline_flag(:new.pk_formlib,1);
		elsif :new.form_status = v_active_status then
		  pkg_lnkform.sp_set_offline_flag(:new.pk_formlib,0);
		end if ;

end if;
END;
/


