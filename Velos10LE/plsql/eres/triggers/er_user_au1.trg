CREATE OR REPLACE TRIGGER "ER_USER_AU1" 
AFTER UPDATE OF FK_SITEID
ON ER_USER REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  v_pk_bud NUMBER(10);
  v_b_right VARCHAR2(10);

  v_old_fk_siteid NUMBER ;
  v_new_fk_siteid NUMBER ;
  v_new_pk_user NUMBER ;
  v_new_creator NUMBER ;
  v_new_ip_add VARCHAR2(15) ;
  v_right_seq NUMBER ;
  v_new_grp_default NUMBER ;

BEGIN

v_old_fk_siteid := :OLD.FK_SITEID ;
v_new_fk_siteid := :NEW.FK_SITEID ;
v_new_pk_user := :NEW.pk_user;
v_new_creator := :NEW.creator;
v_new_ip_add :=  :NEW.ip_add;
v_new_grp_default := :NEW.fk_grp_default ;

/*Select  CTRL_SEQ
   into v_right_seq
   from er_ctrltab
   where  CTRL_KEY   = 'app_rights' and
   CTRL_VALUE     = 'MPATIENTS';

UPDATE er_usersite
  set USERSITE_RIGHT =0
  where fk_user=v_new_pk_user;
   /*
UPDATE er_usersite
  set USERSITE_RIGHT = (
  select to_number(substr(GRP_RIGHTS,v_right_seq,1))
  from er_grps
  where  PK_GRP = v_new_grp_default)
  Where fk_user = v_new_pk_user and
  fk_site = v_new_fk_siteid;
/*
author - sonia sahni
*/
--delete this users record from sch_bgtusers if entered for the old organization
DELETE FROM sch_bgtusers
WHERE fk_user = v_new_pk_user AND
BGTUSERS_TYPE = 'G' AND
fk_budget IN ( SELECT pk_budget FROM
            sch_budget WHERE FK_SITE IS NOT NULL AND
            FK_SITE = v_old_fk_siteid AND
            BUDGET_RIGHTSCOPE = 'O' AND
            BUDGET_SITEFLAG = 1  );

FOR i IN (SELECT DISTINCT b.pk_budget,u.BGTUSERS_RIGHTS
          FROM sch_budget b, sch_bgtusers u
          WHERE b.FK_SITE IS NOT NULL AND
          b.FK_SITE = v_new_fk_siteid AND
          b.BUDGET_RIGHTSCOPE = 'O' AND
          b.BUDGET_SITEFLAG = 1 AND
          u.fk_budget = b.pk_budget AND u.BGTUSERS_TYPE = 'G' )
LOOP
    v_pk_bud :=   i.pk_budget;
    v_b_right := i.BGTUSERS_RIGHTS;
   INSERT INTO sch_bgtusers(PK_BGTUSERS,
                                    FK_BUDGET ,
                                    FK_USER  ,
                                    BGTUSERS_RIGHTS ,
                                    BGTUSERS_TYPE ,
                                    CREATOR,
                                    CREATED_ON ,
                                    IP_ADD )
   VALUES (SEQ_SCH_BGTUSERS.NEXTVAL,
           v_pk_bud,
           v_new_pk_user ,
           v_b_right,
           'G',
           v_new_creator,
           SYSDATE,v_new_ip_add )  ;
END LOOP;




END;
/


