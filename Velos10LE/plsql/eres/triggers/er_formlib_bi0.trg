CREATE  OR REPLACE TRIGGER ER_FORMLIB_BI0 BEFORE INSERT ON ER_FORMLIB        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_FORMLIB',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON', TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
       Audit_Trail.column_insert (raid, 'FK_ACCOUNT', :NEW.FK_ACCOUNT);
       Audit_Trail.column_insert (raid, 'FK_CATLIB', :NEW.FK_CATLIB);
       Audit_Trail.column_insert (raid, 'FLD_FONTSIZE', :NEW.FLD_FONTSIZE);
       Audit_Trail.column_insert (raid, 'FORM_ACTIVATION_JS', :NEW.FORM_ACTIVATION_JS);
       Audit_Trail.column_insert (raid, 'FORM_ALIGN', :NEW.FORM_ALIGN);
       Audit_Trail.column_insert (raid, 'FORM_BGCOLOR', :NEW.FORM_BGCOLOR);
       Audit_Trail.column_insert (raid, 'FORM_BOLD', :NEW.FORM_BOLD);
       Audit_Trail.column_insert (raid, 'FORM_COLOR', :NEW.FORM_COLOR);
       Audit_Trail.column_insert (raid, 'FORM_CUSTOM_JS', :NEW.FORM_CUSTOM_JS);
       Audit_Trail.column_insert (raid, 'FORM_DESC', :NEW.FORM_DESC);
       Audit_Trail.column_insert (raid, 'FORM_ESIGNREQ', :NEW.FORM_ESIGNREQ);
       Audit_Trail.column_insert (raid, 'FORM_FILLFLAG', :NEW.FORM_FILLFLAG);
       Audit_Trail.column_insert (raid, 'FORM_FONT', :NEW.FORM_FONT);
       Audit_Trail.column_insert (raid, 'FORM_ITALICS', :NEW.FORM_ITALICS);
       Audit_Trail.column_insert (raid, 'FORM_KEYWORD', :NEW.FORM_KEYWORD);
       Audit_Trail.column_insert (raid, 'FORM_LINKTO', :NEW.FORM_LINKTO);
       Audit_Trail.column_insert (raid, 'FORM_NAME', :NEW.FORM_NAME);
       Audit_Trail.column_insert (raid, 'FORM_NEXT', :NEW.FORM_NEXT);
       Audit_Trail.column_insert (raid, 'FORM_NEXTMODE', :NEW.FORM_NEXTMODE);
       Audit_Trail.column_insert (raid, 'FORM_SAVEFORMAT', :NEW.FORM_SAVEFORMAT);
       Audit_Trail.column_insert (raid, 'FORM_SHAREDWITH', :NEW.FORM_SHAREDWITH);
       Audit_Trail.column_insert (raid, 'FORM_STATUS', :NEW.FORM_STATUS);
       Audit_Trail.column_insert (raid, 'FORM_UNDERLINE', :NEW.FORM_UNDERLINE);
      -- Audit_Trail.column_insert (raid, 'FORM_VIEWXSL', :NEW.FORM_VIEWXSL);
      -- Audit_Trail.column_insert (raid, 'FORM_XML', :NEW.FORM_XML);
      -- Audit_Trail.column_insert (raid, 'FORM_XSL', :NEW.FORM_XSL);
       Audit_Trail.column_insert (raid, 'FORM_XSLREFRESH', :NEW.FORM_XSLREFRESH);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'PK_FORMLIB', :NEW.PK_FORMLIB);
       Audit_Trail.column_insert (raid, 'RECORD_TYPE', :NEW.RECORD_TYPE);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID); 
 
COMMIT;
END;
/