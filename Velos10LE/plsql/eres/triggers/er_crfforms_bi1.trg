CREATE OR REPLACE TRIGGER "ER_CRFFORMS_BI1"
BEFORE INSERT
ON ER_CRFFORMS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
WHEN (
new.CRFFORMS_FILLDATE is null
      )
declare

  datestr Varchar2(20);
  v_form_xml  sys.xmlType;
  i   NUMBER := 1;
 begin
  v_form_xml := :new.CRFFORMS_XML;


          datestr := v_form_xml.extract('/rowset/er_def_date_01/text()').getStringVal();

       if datestr is not null then
       :new.CRFFORMS_FILLDATE  := to_date(datestr,PKG_DATEUTIL.f_get_dateformat);
    end if ;

 if :new.CRFFORMS_FILLDATE is null then
   :new.CRFFORMS_FILLDATE  := sysdate;
     END IF;
 end;
/


