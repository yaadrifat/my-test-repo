CREATE OR REPLACE TRIGGER "ER_REPFLDVALIDATE_AU0"
AFTER UPDATE
ON ER_REPFLDVALIDATE REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
--Created by Manimaran for Audit update
declare
   raid number(10);
   usr varchar2(100);
   old_modby varchar2(100);
   new_modby varchar2(100);
begin
  select seq_audit.nextval into raid from dual;
  usr := getuser(:new.last_modified_by);
  audit_trail.record_transaction(raid, 'ER_REPFLDVALIDATE', :old.rid, 'U', usr);

  if nvl(:old.PK_REPFLDVALIDATE,0) !=
     NVL(:new.PK_REPFLDVALIDATE,0) then
     audit_trail.column_update
       (raid, 'PK_REPFLDVALIDATE',
       :old.PK_REPFLDVALIDATE, :new.PK_REPFLDVALIDATE);
  end if;

  if nvl(:old.FK_REPFORMFLD,0) !=
     NVL(:new.FK_REPFORMFLD,0) then
     audit_trail.column_update
       (raid, 'FK_REPFORMFLD',
       :old.FK_REPFORMFLD, :new.FK_REPFORMFLD);
  end if;

  if nvl(:old.FK_FIELD,0) !=
     NVL(:new.FK_FIELD,0) then
     audit_trail.column_update
       (raid, 'FK_FIELD',
       :old.FK_FIELD, :new.FK_FIELD);
  end if;

  if nvl(:old.REPFLDVALIDATE_JAVASCR,' ') !=
     NVL(:new.REPFLDVALIDATE_JAVASCR,' ') then
     audit_trail.column_update
       (raid, 'REPFLDVALIDATE_JAVASCR',
       :old.REPFLDVALIDATE_JAVASCR, :new.REPFLDVALIDATE_JAVASCR);
  end if;

  if nvl(:old.RECORD_TYPE,' ') !=
     NVL(:new.RECORD_TYPE,' ') then
     audit_trail.column_update
       (raid, 'RECORD_TYPE',
       :old.RECORD_TYPE, :new.RECORD_TYPE);
  end if;

  if nvl(:old.LAST_MODIFIED_BY,0) != NVL(:new.LAST_MODIFIED_BY,0) then
     Begin
	Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
	into old_modby from er_user  where pk_user = :old.last_modified_by ;
     Exception When NO_DATA_FOUND then
	old_modby := null;
     End ;
     Begin
	Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
	into new_modby   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
     Exception When NO_DATA_FOUND then
	new_modby := null;
     End ;
     audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
  end if;

  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update(raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;

  if nvl(:old.ip_add,' ') != NVL(:new.ip_add,' ') then
     audit_trail.column_update(raid, 'IP_ADD',:old.ip_add, :new.ip_add);
  end if;

  if nvl(:old.rid,0) != NVL(:new.rid,0) then
      audit_trail.column_update(raid, 'RID',:old.rid, :new.rid);
  end if;

end;
/


