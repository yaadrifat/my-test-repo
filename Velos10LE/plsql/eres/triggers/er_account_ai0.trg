CREATE OR REPLACE TRIGGER "ER_ACCOUNT_AI0"
AFTER INSERT
ON ER_ACCOUNT
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE

BEGIN

 -- create account wide empty setting record for calendar budget template

 Insert into ER_SETTINGS
   (SETTINGS_PK, SETTINGS_MODNUM, SETTINGS_MODNAME, SETTINGS_KEYWORD, SETTINGS_VALUE
    )
 values(
   seq_ER_SETTINGS.nextval, :new.pk_Account, 1, 'CALBUD_TEMP', NULL
   );

 END;
/


