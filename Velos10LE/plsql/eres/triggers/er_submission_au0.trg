CREATE OR REPLACE TRIGGER ER_SUBMISSION_AU0
  after update of
  PK_SUBMISSION,
  FK_STUDY,
  SUBMISSION_FLAG,
  SUBMISSION_TYPE,
  SUBMISSION_STATUS,
  CREATED_ON,
  CREATOR,
  LAST_MODIFIED_DATE,
  LAST_MODIFIED_BY,
  RID,
  IP_ADD
  ON ER_SUBMISSION for each row
WHEN (
OLD.rid IS NOT NULL
      )
declare
  raid number(10);
  usr VARCHAR2(500);
begin
  select seq_audit.nextval into raid from dual;

  usr := getuser(NVL(:NEW.LAST_MODIFIED_BY,:OLD.CREATOR));

  audit_trail.record_transaction(raid, 'ER_SUBMISSION', :old.rid, 'U', usr);

  if nvl(:old.PK_SUBMISSION,0) !=
     NVL(:new.PK_SUBMISSION,0) then
     audit_trail.column_update
       (raid, 'PK_SUBMISSION',
       :old.PK_SUBMISSION, :new.PK_SUBMISSION);
  end if;
  if nvl(:old.FK_STUDY,0) !=
     NVL(:new.FK_STUDY,0) then
     audit_trail.column_update
       (raid, 'FK_STUDY',
       :old.FK_STUDY, :new.FK_STUDY);
  end if;
    if nvl(:old.SUBMISSION_FLAG,0) !=
     NVL(:new.SUBMISSION_FLAG,0) then
     audit_trail.column_update
       (raid, 'SUBMISSION_FLAG',
       :old.SUBMISSION_FLAG, :new.SUBMISSION_FLAG);
  end if;
    if nvl(:old.SUBMISSION_TYPE,0) !=
     NVL(:new.SUBMISSION_TYPE,0) then
     audit_trail.column_update
       (raid, 'SUBMISSION_TYPE',
       :old.SUBMISSION_TYPE, :new.SUBMISSION_TYPE);
  end if;
    if nvl(:old.SUBMISSION_STATUS,0) !=
     NVL(:new.SUBMISSION_STATUS,0) then
     audit_trail.column_update
       (raid, 'SUBMISSION_STATUS',
       :old.SUBMISSION_STATUS, :new.SUBMISSION_STATUS);
  end if;
  if nvl(:old.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:OLD.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),
       to_char(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
    if nvl(:old.CREATOR,0) !=
     NVL(:new.CREATOR,0) then
     audit_trail.column_update
       (raid, 'CREATOR',
       :old.CREATOR, :new.CREATOR);
  end if;
  if nvl(:old.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),
       to_char(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
    if nvl(:old.LAST_MODIFIED_BY,0) !=
     NVL(:new.LAST_MODIFIED_BY,0) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :old.LAST_MODIFIED_BY, :new.LAST_MODIFIED_BY);
  end if;
    if nvl(:old.RID,0) !=
     NVL(:new.RID,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.RID, :new.RID);
  end if;
  if nvl(:old.ip_add,' ') !=
     NVL(:new.ip_add,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.ip_add, :new.ip_add);
  end if;


end;
/


