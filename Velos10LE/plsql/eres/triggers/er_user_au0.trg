CREATE OR REPLACE TRIGGER ER_USER_AU0 AFTER UPDATE ON ER_USER FOR EACH ROW
declare
  raid number(10);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction

    (raid, 'ER_USER', :old.rid, 'U', :NEW.last_modified_by);

  if nvl(:old.pk_user,0) !=
     NVL(:new.pk_user,0) then
     audit_trail.column_update
       (raid, 'PK_USER',
       :old.pk_user, :new.pk_user);
  end if;
  if nvl(:old.fk_account,0) !=
     NVL(:new.fk_account,0) then
     audit_trail.column_update
       (raid, 'FK_ACCOUNT',
       :old.fk_account, :new.fk_account);
  end if;
  if nvl(:old.fk_siteid,0) !=
     NVL(:new.fk_siteid,0) then
     audit_trail.column_update
       (raid, 'FK_SITEID',
       :old.fk_siteid, :new.fk_siteid);
  end if;
  if nvl(:old.usr_lastname,' ') !=
     NVL(:new.usr_lastname,' ') then
     audit_trail.column_update
       (raid, 'USR_LASTNAME',
       :old.usr_lastname, :new.usr_lastname);
  end if;
  if nvl(:old.usr_firstname,' ') !=
     NVL(:new.usr_firstname,' ') then
     audit_trail.column_update
       (raid, 'USR_FIRSTNAME',
       :old.usr_firstname, :new.usr_firstname);
  end if;
  if nvl(:old.usr_midname,' ') !=
     NVL(:new.usr_midname,' ') then
     audit_trail.column_update
       (raid, 'USR_MIDNAME',
       :old.usr_midname, :new.usr_midname);
  end if;
  if nvl(:old.usr_wrkexp,' ') !=
     NVL(:new.usr_wrkexp,' ') then
     audit_trail.column_update
       (raid, 'USR_WRKEXP',
       :old.usr_wrkexp, :new.usr_wrkexp);
  end if;
  if nvl(:old.usr_pahseinv,' ') !=
     NVL(:new.usr_pahseinv,' ') then
     audit_trail.column_update
       (raid, 'USR_PAHSEINV',
       :old.usr_pahseinv, :new.usr_pahseinv);
  end if;
  if nvl(:old.usr_sesstime,0) !=
     NVL(:new.usr_sesstime,0) then
     audit_trail.column_update
       (raid, 'USR_SESSTIME',
       :old.usr_sesstime, :new.usr_sesstime);
  end if;
  if nvl(:old.usr_logname,' ') !=
     NVL(:new.usr_logname,' ') then
     audit_trail.column_update
       (raid, 'USR_LOGNAME',
       :old.usr_logname, :new.usr_logname);
  end if;
  if nvl(:old.usr_pwd,' ') !=
     NVL(:new.usr_pwd,' ') then
     audit_trail.column_update
       (raid, 'USR_PWD',
       :old.usr_pwd, :new.usr_pwd);
  end if;
  if nvl(:old.usr_secques,' ') !=
     NVL(:new.usr_secques,' ') then
     audit_trail.column_update
       (raid, 'USR_SECQUES',
       :old.usr_secques, :new.usr_secques);
  end if;
  if nvl(:old.usr_ans,' ') !=
     NVL(:new.usr_ans,' ') then
     audit_trail.column_update
       (raid, 'USR_ANS',
       :old.usr_ans, :new.usr_ans);
  end if;
  if nvl(:old.usr_stat,' ') !=
     NVL(:new.usr_stat,' ') then
     audit_trail.column_update
       (raid, 'USR_STAT',
       :old.usr_stat, :new.usr_stat);
  end if;
  if nvl(:old.fk_codelst_spl,0) !=
     NVL(:new.fk_codelst_spl,0) then
     audit_trail.column_update
       (raid, 'FK_CODELST_SPL',
       :old.fk_codelst_spl, :new.fk_codelst_spl);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.fk_grp_default,0) !=
     NVL(:new.fk_grp_default,0) then
     audit_trail.column_update
       (raid, 'FK_GRP_DEFAULT',
       :old.fk_grp_default, :new.fk_grp_default);
  end if;
  if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :old.last_modified_by, :new.last_modified_by);
  end if;
  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
        to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.fk_peradd,0) !=
     NVL(:new.fk_peradd,0) then
     audit_trail.column_update
       (raid, 'FK_PERADD',
       :old.fk_peradd, :new.fk_peradd);
  end if;
  if nvl(:old.fk_codelst_jobtype,0) !=
     NVL(:new.fk_codelst_jobtype,0) then
     audit_trail.column_update
       (raid, 'FK_CODELST_JOBTYPE',
       :old.fk_codelst_jobtype, :new.fk_codelst_jobtype);
  end if;
  if nvl(:old.usr_code,' ') !=
     NVL(:new.usr_code,' ') then
     audit_trail.column_update
       (raid, 'USR_CODE',
       :old.usr_code, :new.usr_code);
  end if;
  if nvl(:old.usr_pwdxpiry,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.usr_pwdxpiry,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'USR_PWDXPIRY',
       to_char(:old.usr_pwdxpiry,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.usr_pwdxpiry,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.usr_pwddays,0) !=
     NVL(:new.usr_pwddays,0) then
     audit_trail.column_update
       (raid, 'USR_PWDDAYS',
       :old.usr_pwddays, :new.usr_pwddays);
  end if;
  if nvl(:old.usr_pwdremind,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.usr_pwdremind,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'USR_PWDREMIND',
       to_char(:old.usr_pwdremind,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.usr_pwdremind,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.usr_es,' ') !=
     NVL(:new.usr_es,' ') then
     audit_trail.column_update
       (raid, 'USR_ES',
       :old.usr_es, :new.usr_es);
  end if;
  if nvl(:old.usr_esxpiry,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.usr_esxpiry,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'USR_ESXPIRY',
       to_char(:old.usr_esxpiry,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.usr_esxpiry,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.usr_esdays,0) !=
     NVL(:new.usr_esdays,0) then
     audit_trail.column_update
       (raid, 'USR_ESDAYS',
       :old.usr_esdays, :new.usr_esdays);
  end if;
  if nvl(:old.usr_esremind,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.usr_esremind,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'USR_ESREMIND',
       to_char(:old.usr_esremind,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.usr_esremind,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ip_add,' ') !=
     NVL(:new.ip_add,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.ip_add, :new.ip_add);
  end if;
  if nvl(:old.fk_timezone,0) !=
     NVL(:new.fk_timezone,0) then
     audit_trail.column_update
       (raid, 'FK_TIMEZONE',
       :old.fk_timezone, :new.fk_timezone);
  end if;
  if nvl(:old.usr_attemptcnt,0) !=
     NVL(:new.usr_attemptcnt,0) then
     audit_trail.column_update
       (raid, 'USR_ATTEMPTCNT',
       :old.usr_attemptcnt, :new.usr_attemptcnt);
  end if;
  if nvl(:old.usr_loggedinflag,0) !=
     NVL(:new.usr_loggedinflag,0) then
     audit_trail.column_update
       (raid, 'USR_LOGGEDINFLAG',
       :old.usr_loggedinflag, :new.usr_loggedinflag);
  end if;
  if nvl(:old.usr_siteflag,' ') !=
     NVL(:new.usr_siteflag,' ') then
     audit_trail.column_update
       (raid, 'USR_SITEFLAG',
       :old.usr_siteflag, :new.usr_siteflag);
  end if;

  --Added by Manimaran for Enh.#U11
  if nvl(:old.user_hidden,0) !=
     NVL(:new.user_hidden,0) then
     audit_trail.column_update
       (raid, 'USER_HIDDEN',
       :old.user_hidden, :new.user_hidden);
  end if;

 end;
/


