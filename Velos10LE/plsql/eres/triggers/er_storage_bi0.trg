CREATE  OR REPLACE TRIGGER ER_STORAGE_BI0 BEFORE INSERT ON ER_STORAGE        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_STORAGE',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON', TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
       Audit_Trail.column_insert (raid, 'FK_ACCOUNT', :NEW.FK_ACCOUNT);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_CAPACITY_UNITS;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_CAPACITY_UNITS', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_CHILD_STYPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_CHILD_STYPE', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_STORAGE_TYPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_STORAGE_TYPE', codeList_desc);
       Audit_Trail.column_insert (raid, 'FK_STORAGE', :NEW.FK_STORAGE);
       Audit_Trail.column_insert (raid, 'FK_STORAGE_KIT', :NEW.FK_STORAGE_KIT);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'PK_STORAGE', :NEW.PK_STORAGE);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
       Audit_Trail.column_insert (raid, 'STORAGE_ALTERNALEID', :NEW.STORAGE_ALTERNALEID);
       Audit_Trail.column_insert (raid, 'STORAGE_AVAIL_UNIT_COUNT', :NEW.STORAGE_AVAIL_UNIT_COUNT);
       Audit_Trail.column_insert (raid, 'STORAGE_CAP_NUMBER', :NEW.STORAGE_CAP_NUMBER);
       Audit_Trail.column_insert (raid, 'STORAGE_COORDINATE_X', :NEW.STORAGE_COORDINATE_X);
       Audit_Trail.column_insert (raid, 'STORAGE_COORDINATE_Y', :NEW.STORAGE_COORDINATE_Y);
       Audit_Trail.column_insert (raid, 'STORAGE_DIM1_CELLNUM', :NEW.STORAGE_DIM1_CELLNUM);
       Audit_Trail.column_insert (raid, 'STORAGE_DIM1_NAMING', :NEW.STORAGE_DIM1_NAMING);
       Audit_Trail.column_insert (raid, 'STORAGE_DIM1_ORDER', :NEW.STORAGE_DIM1_ORDER);
       Audit_Trail.column_insert (raid, 'STORAGE_DIM2_CELLNUM', :NEW.STORAGE_DIM2_CELLNUM);
       Audit_Trail.column_insert (raid, 'STORAGE_DIM2_NAMING', :NEW.STORAGE_DIM2_NAMING);
       Audit_Trail.column_insert (raid, 'STORAGE_DIM2_ORDER', :NEW.STORAGE_DIM2_ORDER);
       Audit_Trail.column_insert (raid, 'STORAGE_ID', :NEW.STORAGE_ID);
       Audit_Trail.column_insert (raid, 'STORAGE_ISTEMPLATE', :NEW.STORAGE_ISTEMPLATE);
       Audit_Trail.column_insert (raid, 'STORAGE_KIT_CATEGORY', :NEW.STORAGE_KIT_CATEGORY);
       Audit_Trail.column_insert (raid, 'STORAGE_LABEL', :NEW.STORAGE_LABEL);
       Audit_Trail.column_insert (raid, 'STORAGE_NAME', :NEW.STORAGE_NAME);
       Audit_Trail.column_insert (raid, 'STORAGE_NOTES', :NEW.STORAGE_NOTES);
       Audit_Trail.column_insert (raid, 'STORAGE_TEMPLATE_TYPE', :NEW.STORAGE_TEMPLATE_TYPE);
       Audit_Trail.column_insert (raid, 'STORAGE_UNIT_CLASS', :NEW.STORAGE_UNIT_CLASS);
COMMIT;
END;
/
