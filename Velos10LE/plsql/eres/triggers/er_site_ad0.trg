CREATE  OR REPLACE TRIGGER ER_SITE_AD0 AFTER DELETE ON ER_SITE        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
  oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;

            BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR;
      EXCEPTION WHEN NO_DATA_FOUND THEN
       oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_SITE', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON', TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr);
       Audit_Trail.column_delete (raid, 'FK_ACCOUNT', :OLD.FK_ACCOUNT);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_TYPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_TYPE', codeList_desc);
       Audit_Trail.column_delete (raid, 'FK_PERADD', :OLD.FK_PERADD);
       Audit_Trail.column_delete (raid, 'GUID', :OLD.GUID);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'PK_SITE', :OLD.PK_SITE);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
       Audit_Trail.column_delete (raid, 'SITE_ALTID', :OLD.SITE_ALTID);
       Audit_Trail.column_delete (raid, 'SITE_HIDDEN', :OLD.SITE_HIDDEN);
       Audit_Trail.column_delete (raid, 'SITE_ID', :OLD.SITE_ID);
       Audit_Trail.column_delete (raid, 'SITE_INFO', :OLD.SITE_INFO);
       Audit_Trail.column_delete (raid, 'SITE_NAME', :OLD.SITE_NAME);
       Audit_Trail.column_delete (raid, 'SITE_NOTES', :OLD.SITE_NOTES);
       Audit_Trail.column_delete (raid, 'SITE_PARENT', :OLD.SITE_PARENT);
       Audit_Trail.column_delete (raid, 'SITE_RESTRICT', :OLD.SITE_RESTRICT);
       Audit_Trail.column_delete (raid, 'SITE_SEQ', :OLD.SITE_SEQ);
       Audit_Trail.column_delete (raid, 'SITE_STAT', :OLD.SITE_STAT);
COMMIT;
END;
/