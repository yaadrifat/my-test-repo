CREATE OR REPLACE TRIGGER ER_ADD_AU0
AFTER UPDATE
ON ER_ADD REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  old_addtype varchar2(200) ;
  new_addtype varchar2(200) ;
  old_modby varchar2(100) ;
  new_modby varchar2(100) ;
  usr varchar2(100);
begin
  select seq_audit.nextval into raid from dual;

  usr := getuser(:new.last_modified_by);
  audit_trail.record_transaction
    (raid, 'ER_ADD', :old.rid, 'U', usr);
  if nvl(:old.fk_codelst_addtype,0) !=
     NVL(:new.fk_codelst_addtype,0) then
   Begin
     	select trim(codelst_desc) into old_addtype
	from er_codelst where pk_codelst =  :old.fk_codelst_addtype ;
      Exception When NO_DATA_FOUND then
	old_addtype := null;
   End;
   Begin
     	select trim(codelst_desc) into new_addtype
	from er_codelst where pk_codelst =  :new.fk_codelst_addtype ;
      Exception When NO_DATA_FOUND then
	new_addtype := null;
   End;
     audit_trail.column_update
       (raid, 'FK_CODELST_ADDTYPE',
       old_addtype, new_addtype);
  end if;
  if nvl(:old.pk_add,0) !=
     NVL(:new.pk_add,0) then
     audit_trail.column_update
       (raid, 'PK_ADD',
       :old.pk_add, :new.pk_add);
  end if;
  if nvl(:old.address,' ') !=
     NVL(:new.address,' ') then
     audit_trail.column_update
       (raid, 'ADDRESS',
       :old.address, :new.address);
  end if;
  if nvl(:old.add_city,' ') !=
     NVL(:new.add_city,' ') then
     audit_trail.column_update
       (raid, 'ADD_CITY',
       :old.add_city, :new.add_city);
  end if;
  if nvl(:old.add_state,' ') !=
     NVL(:new.add_state,' ') then
     audit_trail.column_update
       (raid, 'ADD_STATE',
       :old.add_state, :new.add_state);
  end if;
  if nvl(:old.add_zipcode,' ') !=
     NVL(:new.add_zipcode,' ') then
     audit_trail.column_update
       (raid, 'ADD_ZIPCODE',
       :old.add_zipcode, :new.add_zipcode);
  end if;
  if nvl(:old.add_country,' ') !=
     NVL(:new.add_country,' ') then
     audit_trail.column_update
       (raid, 'ADD_COUNTRY',
       :old.add_country, :new.add_country);
  end if;
  if nvl(:old.add_county,' ') !=
     NVL(:new.add_county,' ') then
     audit_trail.column_update
       (raid, 'ADD_COUNTY',
       :old.add_county, :new.add_county);
  end if;
  if nvl(:old.add_eff_dt,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.add_eff_dt,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'ADD_EFF_DT',
       to_char(:old.add_eff_dt, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.add_eff_dt, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.add_phone,' ') !=
     NVL(:new.add_phone,' ') then
     audit_trail.column_update
       (raid, 'ADD_PHONE',
       :old.add_phone, :new.add_phone);
  end if;
  if nvl(:old.add_email,' ') !=
     NVL(:new.add_email,' ') then
     audit_trail.column_update
       (raid, 'ADD_EMAIL',
       :old.add_email, :new.add_email);
  end if;
  if nvl(:old.add_pager,' ') !=
     NVL(:new.add_pager,' ') then
     audit_trail.column_update
       (raid, 'ADD_PAGER',
       :old.add_pager, :new.add_pager);
  end if;
  if nvl(:old.add_mobile,' ') !=
     NVL(:new.add_mobile,' ') then
     audit_trail.column_update
       (raid, 'ADD_MOBILE',
       :old.add_mobile, :new.add_mobile);
  end if;
  if nvl(:old.add_fax,' ') !=
     NVL(:new.add_fax,' ') then
     audit_trail.column_update
       (raid, 'ADD_FAX',
       :old.add_fax, :new.add_fax);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
    Begin
     select to_char(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     into old_modby
     from er_user
     where pk_user = :old.last_modified_by ;
    Exception When NO_DATA_FOUND then
    old_modby := null;
   End;
   Begin
     select to_char(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     into new_modby
     from er_user
     where pk_user = :new.last_modified_by ;
    Exception When NO_DATA_FOUND then
    new_modby := null;
   End;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  end if;
  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:old.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT));
   end if;
  if nvl(:old.IP_ADD,' ') !=
     NVL(:new.IP_ADD,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.IP_ADD, :new.IP_ADD);
  end if;

end;
/


