CREATE OR REPLACE TRIGGER ER_USERSITE_AU0
  after update of
  pk_usersite,
  fk_user,
  fk_site,
  usersite_right,
  creator,
  created_on,
  ip_add,
  rid
  on er_usersite
  for each row
declare
  raid number(10);
  usr varchar2(100);

begin
  select seq_audit.nextval into raid from dual;

  usr := getuser(:new.last_modified_by);

  audit_trail.record_transaction
    (raid, 'ER_USERSITE', :old.rid, 'U', usr );

  if nvl(:old.pk_usersite,0) !=
     NVL(:new.pk_usersite,0) then
     audit_trail.column_update
       (raid, 'PK_USERSITE',
       :old.pk_usersite, :new.pk_usersite);
  end if;
  if nvl(:old.fk_user,0) !=
     NVL(:new.fk_user,0) then
     audit_trail.column_update
       (raid, 'FK_USER',
       :old.fk_user, :new.fk_user);
  end if;
  if nvl(:old.fk_site,0) !=
     NVL(:new.fk_site,0) then
     audit_trail.column_update
       (raid, 'FK_SITE',
       :old.fk_site, :new.fk_site);
  end if;
  if nvl(:old.usersite_right,0) !=
     NVL(:new.usersite_right,0) then
     audit_trail.column_update
       (raid, 'USERSITE_RIGHT',
       :old.usersite_right, :new.usersite_right);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :old.last_modified_by, :new.last_modified_by);
  end if;
  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
        to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ip_add,' ') !=
     NVL(:new.ip_add,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.ip_add, :new.ip_add);
  end if;

end;
/


