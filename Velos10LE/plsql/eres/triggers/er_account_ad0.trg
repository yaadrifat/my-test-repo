CREATE  OR REPLACE TRIGGER ER_ACCOUNT_AD0 AFTER DELETE ON ER_ACCOUNT        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
  oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;

            BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR;
      EXCEPTION WHEN NO_DATA_FOUND THEN
       oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_ACCOUNT', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'ACCENDDATE', :OLD.ACCENDDATE);
       Audit_Trail.column_delete (raid, 'AC_AUTOGEN_PATIENT', :OLD.AC_AUTOGEN_PATIENT);
       Audit_Trail.column_delete (raid, 'AC_AUTOGEN_STUDY', :OLD.AC_AUTOGEN_STUDY);
       Audit_Trail.column_delete (raid, 'AC_ENABLELDAP', :OLD.AC_ENABLELDAP);
       Audit_Trail.column_delete (raid, 'AC_ENABLEWS', :OLD.AC_ENABLEWS);
       Audit_Trail.column_delete (raid, 'AC_ENDT', :OLD.AC_ENDT);
       Audit_Trail.column_delete (raid, 'AC_KEY', :OLD.AC_KEY);
      -- Audit_Trail.column_delete (raid, 'AC_LOGO', :OLD.AC_LOGO);
       Audit_Trail.column_delete (raid, 'AC_MAILFLAG', :OLD.AC_MAILFLAG);
       Audit_Trail.column_delete (raid, 'AC_MAXSTORAGE', :OLD.AC_MAXSTORAGE);
       Audit_Trail.column_delete (raid, 'AC_MAXUSR', :OLD.AC_MAXUSR);
       Audit_Trail.column_delete (raid, 'AC_MODRIGHT', :OLD.AC_MODRIGHT);
       Audit_Trail.column_delete (raid, 'AC_NAME', :OLD.AC_NAME);
       Audit_Trail.column_delete (raid, 'AC_NOTE', :OLD.AC_NOTE);
       Audit_Trail.column_delete (raid, 'AC_PUBFLAG', :OLD.AC_PUBFLAG);
       Audit_Trail.column_delete (raid, 'AC_STAT', :OLD.AC_STAT);
       Audit_Trail.column_delete (raid, 'AC_STRTDT', :OLD.AC_STRTDT);
       Audit_Trail.column_delete (raid, 'AC_TYPE', :OLD.AC_TYPE);
       Audit_Trail.column_delete (raid, 'AC_USRCREATOR', :OLD.AC_USRCREATOR);
       Audit_Trail.column_delete (raid, 'CREATED_ON', TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_ROLE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_ROLE', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_SKIN;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_SKIN', codeList_desc);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'MAX_PORTALS', :OLD.MAX_PORTALS);
       Audit_Trail.column_delete (raid, 'PK_ACCOUNT', :OLD.PK_ACCOUNT);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
       Audit_Trail.column_delete (raid, 'SITE_CODE', :OLD.SITE_CODE);
COMMIT;
END;
/