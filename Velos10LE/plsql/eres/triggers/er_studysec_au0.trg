create or replace
TRIGGER ERES.ER_STUDYSEC_AU0
AFTER UPDATE
ON ERES.ER_STUDYSEC REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  old_modby varchar2(100) ;
  new_modby varchar2(100) ;
  old_study varchar2(4000) ;
  new_study varchar2(4000) ;
  usr varchar2(100);
begin
  select seq_audit.nextval into raid from dual;

  usr := getuser(:new.last_modified_by);
  
  --KM-#3634
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.record_transaction
    (raid, 'ER_STUDYSEC', :old.rid, 'U', usr);
  END IF;
  
  if nvl(:old.pk_studysec,0) !=
     NVL(:new.pk_studysec,0) then
     audit_trail.column_update
       (raid, 'PK_STUDYSEC',
       :old.pk_studysec, :new.pk_studysec);
  end if;

  if nvl(:old.fk_study,0) !=
     NVL(:new.fk_study,0) then
   Begin
     select study_title into old_study
     from er_study where pk_study = :old.fk_study ;
     Exception When NO_DATA_FOUND then
      old_study := null ;
    End ;
    Begin
     select study_title into new_study
     from er_study where pk_study = :new.fk_study ;
     Exception When NO_DATA_FOUND then
      new_study := null ;
    End ;
     audit_trail.column_update
       (raid, 'FK_STUDY',
       old_study, new_study);
  end if;
  if nvl(:old.studysec_name,' ') !=
     NVL(:new.studysec_name,' ') then
     audit_trail.column_update
       (raid, 'STUDYSEC_NAME',
       :old.studysec_name, :new.studysec_name);
  end if;

  if nvl(:old.studysec_num,' ') !=
     NVL(:new.studysec_num,' ') then
     audit_trail.column_update
       (raid, 'STUDYSEC_NUM',
       :old.studysec_num, :new.studysec_num);
  end if;
  if nvl(:old.studysec_pubflag,' ') !=
     NVL(:new.studysec_pubflag,' ') then
     audit_trail.column_update
       (raid, 'STUDYSEC_PUBFLAG',
       :old.studysec_pubflag, :new.studysec_pubflag);
  end if;
  if nvl(:old.studysec_seq,0) !=
     NVL(:new.studysec_seq,0) then
     audit_trail.column_update
       (raid, 'STUDYSEC_SEQ',
       :old.studysec_seq, :new.studysec_seq);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
   Begin
     select to_char(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     into old_modby
     from er_user
     where pk_user = :old.last_modified_by ;
     Exception When NO_DATA_FOUND then
      old_modby := null ;
    End ;
    Begin
     select to_char(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     into new_modby
     from er_user
     where pk_user = :new.last_modified_by ;
     Exception When NO_DATA_FOUND then
      new_modby := null ;
    End ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  end if;
  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.IP_ADD,' ') !=
     NVL(:new.IP_ADD,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.IP_ADD, :new.IP_ADD);
  end if;


  if  :old.STUDYSEC_text  !=  :new.STUDYSEC_text then
--JM: commented and modified in the new line

     --audit_trail.column_update  (raid, 'STUDYSEC_TEXT',    dbms_lob.substr(:old.STUDYSEC_CONTENTS ,1,4000), dbms_lob.substr(:new.STUDYSEC_CONTENTS,1,4000));
     audit_trail.column_update  (raid, 'STUDYSEC_TEXT',    dbms_lob.substr(:OLD.STUDYSEC_text ,4000,1), dbms_lob.substr(:NEW.STUDYSEC_text,4000,1));
  end if;

end;
/