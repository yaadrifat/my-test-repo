CREATE  OR REPLACE TRIGGER ER_FLDRESP_AD0 AFTER DELETE ON ER_FLDRESP        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_FLDRESP', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr );
       Audit_Trail.column_delete (raid, 'FK_FIELD', :OLD.FK_FIELD);
       Audit_Trail.column_delete (raid, 'FLDRESP_DATAVAL', :OLD.FLDRESP_DATAVAL);
       Audit_Trail.column_delete (raid, 'FLDRESP_DISPVAL', :OLD.FLDRESP_DISPVAL);
       Audit_Trail.column_delete (raid, 'FLDRESP_ISDEFAULT', :OLD.FLDRESP_ISDEFAULT);
       Audit_Trail.column_delete (raid, 'FLDRESP_OFFLINE', :OLD.FLDRESP_OFFLINE);
       Audit_Trail.column_delete (raid, 'FLDRESP_SCORE', :OLD.FLDRESP_SCORE);
       Audit_Trail.column_delete (raid, 'FLDRESP_SEQ', :OLD.FLDRESP_SEQ);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr );
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'PK_FLDRESP', :OLD.PK_FLDRESP);
       Audit_Trail.column_delete (raid, 'RECORD_TYPE', :OLD.RECORD_TYPE);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
COMMIT;
END;
/
