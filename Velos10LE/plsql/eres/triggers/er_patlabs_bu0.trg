CREATE OR REPLACE TRIGGER "ER_PATLABS_BU0" 
BEFORE UPDATE
ON ER_PATLABS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
BEGIN
	:NEW.LAST_MODIFIED_DATE := SYSDATE;
END;
/


