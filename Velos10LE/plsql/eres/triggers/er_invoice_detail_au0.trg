CREATE OR REPLACE TRIGGER "ER_INVOICE_DETAIL_AU0" 
AFTER UPDATE
ON ER_INVOICE_DETAIL REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  old_codetype VARCHAR2(200) ;
  new_codetype VARCHAR2(200) ;
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
  usr VARCHAR2(100);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  usr := Getuser(:NEW.last_modified_by);

  Audit_Trail.record_transaction
    (raid, 'ER_INVOICE_DETAIL', :OLD.rid, 'U', usr);



  IF NVL(:OLD.pk_invdetail ,0) !=
     NVL(:NEW.pk_invdetail ,0) THEN
     Audit_Trail.column_update
       (raid, 'pk_invdetail ',
       :OLD.pk_invdetail , :NEW.pk_invdetail );
  END IF;

   IF NVL(:OLD.amount_due ,0) !=
     NVL(:NEW.amount_due ,0) THEN
     Audit_Trail.column_update
       (raid, 'amount_due ',
       :OLD.amount_due , :NEW.amount_due );
  END IF;
    IF NVL(:OLD.amount_invoiced ,0) !=
     NVL(:NEW.amount_invoiced ,0) THEN
     Audit_Trail.column_update
       (raid, 'amount_invoiced ',
       :OLD.amount_invoiced , :NEW.amount_invoiced );
  END IF;
   IF NVL(:OLD.amount_holdback ,0) !=
     NVL(:NEW.amount_holdback ,0) THEN
     Audit_Trail.column_update
       (raid, 'amount_holdback ',
       :OLD.amount_holdback , :NEW.amount_holdback );
  END IF;
  IF NVL(:OLD.fk_milestone,0) !=
     NVL(:NEW.fk_milestone,0) THEN
     Audit_Trail.column_update
       (raid, 'fk_milestone',
       :OLD.fk_milestone, :NEW.fk_milestone);
  END IF;

  IF NVL(:OLD.fk_per,0) !=
     NVL(:NEW.fk_per,0) THEN
     Audit_Trail.column_update
       (raid, 'fk_per',
       :OLD.fk_per, :NEW.fk_per);
  END IF;


IF NVL(:OLD.payment_duedate ,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.payment_duedate ,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update
       (raid, 'payment_duedate',
       :OLD.payment_duedate , :NEW.payment_duedate );
  END IF;

  IF NVL(:OLD.fk_study,0) !=
     NVL(:NEW.fk_study,0) THEN
     Audit_Trail.column_update
       (raid, 'fk_study',
       :OLD.fk_study, :NEW.fk_study);
  END IF;

  IF NVL(:OLD.fk_patprot ,0) !=     NVL(:NEW.fk_patprot ,0) THEN
     Audit_Trail.column_update   (raid, 'fk_patprot ',    :OLD.fk_patprot , :NEW.fk_patprot );
  END IF;

  IF NVL(:OLD.fk_inv ,0) !=     NVL(:NEW.fk_inv ,0) THEN
     Audit_Trail.column_update   (raid, 'fk_inv ',    :OLD.fk_inv , :NEW.fk_inv );
  END IF;

  IF NVL(:OLD.FK_MILEACHIEVED,0) !=
     NVL(:NEW.FK_MILEACHIEVED,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_MILEACHIEVED',
       :OLD.FK_MILEACHIEVED, :NEW.FK_MILEACHIEVED);
  END IF;


old_modby := '';
new_modby := '';

  IF NVL(:OLD.last_modified_by,0) !=    NVL(:NEW.last_modified_by,0) THEN
      BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM ER_USER
     WHERE pk_user = :OLD.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      old_modby := NULL ;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM ER_USER
     WHERE pk_user = :NEW.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      new_modby := NULL ;
    END ;
	     Audit_Trail.column_update     (raid, 'LAST_MODIFIED_BY', old_modby, new_modby);
  END IF;

  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update     (raid, 'LAST_MODIFIED_DATE',    :OLD.last_modified_date, :NEW.last_modified_date);
  END IF;

  IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=     NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update (raid, 'CREATED_ON',       :OLD.created_on, :NEW.created_on);
  END IF;

  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN Audit_Trail.column_update    (raid, 'RID',  :OLD.rid, :NEW.rid);
  END IF;

END;
/


