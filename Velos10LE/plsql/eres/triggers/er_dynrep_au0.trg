CREATE OR REPLACE TRIGGER ER_DYNREP_AU0 AFTER UPDATE OF RID,IP_ADD,FK_FORM,FK_USER,FK_STUDY,FK_PERSON,PK_DYNREP,FK_ACCOUNT,DYNREP_DESC,DYNREP_NAME,DYNREP_TYPE,REPORT_TYPE,DYNREP_FILTER,DYNREP_FTRNAME,DYNREP_HDRNAME,DYNREP_ORDERBY,DYNREP_FORMLIST,DYNREP_SHAREWITH,LAST_MODIFIED_BY,DYNREP_USEDATAVAL,DYNREP_USEUNIQUEID,LAST_MODIFIED_DATE ON ER_DYNREP FOR EACH ROW
declare
  raid number(10);
   usr varchar2(100);
   old_modby varchar2(100);
   new_modby varchar2(100);
begin
  select seq_audit.nextval into raid from dual;
   usr := getuser(:new.last_modified_by);
  audit_trail.record_transaction
    (raid, 'ER_DYNREP', :old.rid, 'U', usr);
  if nvl(:old.PK_DYNREP,0) !=
     NVL(:new.PK_DYNREP,0) then
     audit_trail.column_update
       (raid, 'PK_DYNREP',
       :old.PK_DYNREP, :new.PK_DYNREP);
  end if;
  if nvl(:old.FK_FORM,0) !=
     NVL(:new.FK_FORM,0) then
     audit_trail.column_update
       (raid, 'FK_FORM',
       :old.FK_FORM, :new.FK_FORM);
  end if;
 if nvl(:old.DYNREP_NAME,' ') !=
     NVL(:new.DYNREP_NAME,' ') then
     audit_trail.column_update
       (raid, 'DYNREP_NAME',
       :old.DYNREP_NAME, :new.DYNREP_NAME);
  end if;

if nvl(:old.DYNREP_DESC,' ') !=
     NVL(:new.DYNREP_DESC,' ') then
     audit_trail.column_update
       (raid, 'DYNREP_DESC',
       :old.DYNREP_DESC, :new.DYNREP_DESC);
  end if;
if nvl(:old.DYNREP_FILTER,' ') !=
     NVL(:new.DYNREP_FILTER,' ') then
     audit_trail.column_update
       (raid, 'DYNREP_FILTER',
       :old.DYNREP_FILTER, :new.DYNREP_FILTER);
  end if;

if nvl(:old.DYNREP_ORDERBY,' ') !=
     NVL(:new.DYNREP_ORDERBY,' ') then
     audit_trail.column_update
       (raid, 'DYNREP_ORDERBY',
       :old.DYNREP_ORDERBY, :new.DYNREP_ORDERBY);
  end if;

if nvl(:old.FK_USER,0) !=
     NVL(:new.FK_USER,0) then
     audit_trail.column_update
       (raid, 'FK_USER',
       :old.FK_USER, :new.FK_USER);
end if;

 if nvl(:old.fk_account,0) !=
     NVL(:new.fk_account,0) then
     audit_trail.column_update
       (raid, 'FK_ACCOUNT',
       :old.fk_account, :new.fk_account);
end if;

if nvl(:old.DYNREP_HDRNAME,' ') !=
     NVL(:new.DYNREP_HDRNAME,' ') then
     audit_trail.column_update
       (raid, 'DYNREP_HDRNAME',
       :old.DYNREP_HDRNAME, :new.DYNREP_HDRNAME);
  end if;

if nvl(:old.DYNREP_FTRNAME,' ') !=
     NVL(:new.DYNREP_FTRNAME,' ') then
     audit_trail.column_update
       (raid, 'DYNREP_FTRNAME',
       :old.DYNREP_FTRNAME, :new.DYNREP_FTRNAME);
  end if;
if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;

if nvl(:old.LAST_MODIFIED_BY,0) !=
 NVL(:new.LAST_MODIFIED_BY,0) then
	Begin
		Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
		into old_modby from er_user  where pk_user = :old.last_modified_by ;
		Exception When NO_DATA_FOUND then
			old_modby := null;
	End ;
	Begin
		Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
		into new_modby   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
		Exception When NO_DATA_FOUND then
			new_modby := null;
	 End ;
	audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
 end if;

if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;

if nvl(:old.ip_add,' ') !=
     NVL(:new.ip_add,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.ip_add, :new.ip_add);
  end if;

if nvl(:old.REPORT_TYPE,' ') !=
     NVL(:new.REPORT_TYPE,' ') then
     audit_trail.column_update
       (raid, 'REPORT_TYPE',
       :old.REPORT_TYPE, :new.REPORT_TYPE);
  end if;


  if nvl(:old.FK_STUDY,0) !=
     NVL(:new.FK_STUDY,0) then
     audit_trail.column_update
       (raid, 'FK_STUDY',
       :old.FK_STUDY, :new.FK_STUDY);
  end if;


  if nvl(:old.FK_PERSON,0) !=
     NVL(:new.FK_PERSON,0) then
     audit_trail.column_update
       (raid, 'FK_PERSON',
       :old.FK_PERSON, :new.FK_PERSON);
  end if;


if nvl(:old.DYNREP_SHAREWITH,' ') !=
     NVL(:new.DYNREP_SHAREWITH,' ') then
     audit_trail.column_update
       (raid, 'DYNREP_SHAREWITH',
       :old.DYNREP_SHAREWITH, :new.DYNREP_SHAREWITH);
  end if;


if nvl(:old.DYNREP_TYPE,' ') !=
     NVL(:new.DYNREP_TYPE,' ') then
     audit_trail.column_update
       (raid, 'DYNREP_TYPE',
       :old.DYNREP_TYPE, :new.DYNREP_TYPE);
  end if;

if nvl(:old.DYNREP_FORMLIST,' ') !=
     NVL(:new.DYNREP_FORMLIST,' ') then
     audit_trail.column_update
       (raid, 'DYNREP_FORMLIST',
       :old.DYNREP_FORMLIST, :new.DYNREP_FORMLIST);
  end if;

 if nvl(:old.DYNREP_USEUNIQUEID,0) !=
     NVL(:new.DYNREP_USEUNIQUEID,0) then
     audit_trail.column_update
       (raid, 'DYNREP_USEUNIQUEID',
       :old.DYNREP_USEUNIQUEID, :new.DYNREP_USEUNIQUEID);
 end if;
 if nvl(:old.DYNREP_USEDATAVAL,0) !=
     NVL(:new.DYNREP_USEDATAVAL,0) then
     audit_trail.column_update
       (raid, 'DYNREP_USEDATAVAL',
       :old.DYNREP_USEDATAVAL, :new.DYNREP_USEDATAVAL);
 end if;

end;
/


