CREATE OR REPLACE TRIGGER ER_DYNREPFILTER_AU0 AFTER UPDATE OF RID,IP_ADD,CREATOR,FK_FORM,FK_DYNREP,FORM_TYPE,CREATED_ON,DATERANGE_TO,DATERANGE_FROM,DATERANGE_TYPE,FK_PARENTFILTER,PK_DYNREPFILTER,LAST_MODIFIED_BY,DYNREPFILTER_NAME,LAST_MODIFIED_DATE ON ER_DYNREPFILTER FOR EACH ROW
declare
  raid number(10);
   usr varchar2(100);
   old_modby varchar2(100);
   new_modby varchar2(100);
begin
  select seq_audit.nextval into raid from dual;
   usr := getuser(:new.last_modified_by);
  audit_trail.record_transaction
    (raid, 'ER_DYNREPFILTER', :old.rid, 'U', usr);
  if nvl(:old.PK_DYNREPFILTER,0) !=
     NVL(:new.PK_DYNREPFILTER,0) then
     audit_trail.column_update
       (raid, 'PK_DYNREPFILTER',
       :old.PK_DYNREPFILTER, :new.PK_DYNREPFILTER);
  end if;

  if nvl(:old.FK_DYNREP,0) !=
     NVL(:new.FK_DYNREP,0) then
     audit_trail.column_update
       (raid, 'FK_DYNREP',
       :old.FK_DYNREP, :new.FK_DYNREP);
  end if;

 if nvl(:old.RID,0) !=
     NVL(:new.RID,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.RID, :new.RID);
  end if;

if nvl(:old.LAST_MODIFIED_BY,0) !=
 NVL(:new.LAST_MODIFIED_BY,0) then
	Begin
		Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
		into old_modby from er_user  where pk_user = :old.last_modified_by ;
		Exception When NO_DATA_FOUND then
			old_modby := null;
	End ;
	Begin
		Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
		into new_modby   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
		Exception When NO_DATA_FOUND then
			new_modby := null;
	 End ;
	audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
 end if;

if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;

if nvl(:old.IP_ADD,' ') !=
     NVL(:new.IP_ADD,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.IP_ADD, :new.IP_ADD);
  end if;

if nvl(:old.DYNREPFILTER_NAME,' ') !=
     NVL(:new.DYNREPFILTER_NAME,' ') then
     audit_trail.column_update
       (raid, 'DYNREPFILTER_NAME',
       :old.DYNREPFILTER_NAME, :new.DYNREPFILTER_NAME);
  end if;

if nvl(:old.FK_FORM,0) !=
     NVL(:new.FK_FORM,0) then
     audit_trail.column_update
       (raid, 'FK_FORM',
       :old.FK_FORM, :new.FK_FORM);
  end if;

if nvl(:old.FK_PARENTFILTER,0) !=
     NVL(:new.FK_PARENTFILTER,0) then
     audit_trail.column_update
       (raid, 'FK_PARENTFILTER',
       :old.FK_PARENTFILTER, :new.FK_PARENTFILTER);
  end if;

if nvl(:old.FORM_TYPE,' ') !=
     NVL(:new.FORM_TYPE,' ') then
     audit_trail.column_update
       (raid, 'FORM_TYPE',
       :old.FORM_TYPE, :new.FORM_TYPE);
  end if;

if nvl(:old.DATERANGE_TYPE,' ') !=
     NVL(:new.DATERANGE_TYPE,' ') then
     audit_trail.column_update
       (raid, 'DATERANGE_TYPE',
       :old.DATERANGE_TYPE, :new.DATERANGE_TYPE);
  end if;

if nvl(:old.DATERANGE_FROM,' ') !=
     NVL(:new.DATERANGE_FROM,' ') then
     audit_trail.column_update
       (raid, 'DATERANGE_FROM',
       :old.DATERANGE_FROM, :new.DATERANGE_FROM);
  end if;

if nvl(:old.DATERANGE_TO,' ') !=
     NVL(:new.DATERANGE_TO,' ') then
     audit_trail.column_update
       (raid, 'DATERANGE_TO',
       :old.DATERANGE_TO, :new.DATERANGE_TO);
  end if;

end;
/


