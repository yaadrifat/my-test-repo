CREATE OR REPLACE TRIGGER "ER_ER_FLDLIB_BU_LM" BEFORE UPDATE ON ER_FLDLIB
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
begin :new.last_modified_date := sysdate;
 end;
/


