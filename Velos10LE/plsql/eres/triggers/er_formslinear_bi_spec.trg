CREATE OR REPLACE TRIGGER "ER_FORMSLINEAR_BI_SPEC" 
BEFORE INSERT
ON ER_FORMSLINEAR
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
WHEN (
new.FK_SPECIMEN is null
      )
declare

  v_fk_specimen Number;
  v_form_type Varchar2(10);
 begin

 		v_form_type := :new.form_type;

 		if (v_form_type = 'A') then -- account form

 			select fk_specimen
 			into  v_fk_specimen
 			from er_acctforms
 			where pk_acctforms = :new.fk_filledform and fk_formlib = :new.fk_form;

 		elsif (v_form_type = 'S') then --study form
 			select fk_specimen
 			into  v_fk_specimen
 			from er_studyforms
 			where pk_studyforms = :new.fk_filledform and fk_formlib = :new.fk_form;

 		else -- patient form

 			select fk_specimen
 			into  v_fk_specimen
 			from er_patforms
 			where pk_patforms = :new.fk_filledform and fk_formlib = :new.fk_form;

 		end if;

	:new.fk_specimen := v_fk_specimen;
end;
/


