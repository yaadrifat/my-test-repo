CREATE OR REPLACE TRIGGER "ER_PATPROT_AU0"
AFTER UPDATE
ON ER_PATPROT
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  new_usr VARCHAR2(100);
  old_usr VARCHAR2(100);
   usr VARCHAR2(100);
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
  old_codetype VARCHAR2(200) ;
  new_codetype VARCHAR2(200) ;
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  usr := getuser(:NEW.last_modified_by);
 -- Added by Ganapathy on 05/07/2005 for Audit Update

  new_usr:=getuser(:NEW.fk_user);
   old_usr:=getuser(:OLD.fk_user);

  audit_trail.record_transaction
    (raid, 'ER_PATPROT', :OLD.rid, 'U', usr);

  IF NVL(:OLD.pk_patprot,0) !=
     NVL(:NEW.pk_patprot,0) THEN
     audit_trail.column_update
       (raid, 'PK_PATPROT',
       :OLD.pk_patprot, :NEW.pk_patprot);
  END IF;
  IF NVL(:OLD.fk_protocol,0) !=
     NVL(:NEW.fk_protocol,0) THEN
     audit_trail.column_update
       (raid, 'FK_PROTOCOL',
       :OLD.fk_protocol, :NEW.fk_protocol);
  END IF;
  IF NVL(:OLD.fk_study,0) !=
     NVL(:NEW.fk_study,0) THEN
     audit_trail.column_update
       (raid, 'FK_STUDY',
       :OLD.fk_study, :NEW.fk_study);
  END IF;
-- modified  by Ganapathy on 05/07/2005 for Audit Update
  --IF NVL(:OLD.fk_user,0) !=
  --   NVL(:NEW.fk_user,0) THEN

 IF NVL(old_usr,0)!=NVL(new_usr,0) THEN

     audit_trail.column_update
       (raid, 'FK_USER',old_usr,new_usr);
--       :OLD.fk_user, :NEW.fk_user);
END IF;
  --END IF;


  IF NVL(:OLD.patprot_enroldt,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.patprot_enroldt,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'PATPROT_ENROLDT',
       to_char(:OLD.patprot_enroldt,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.patprot_enroldt,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.fk_per,0) !=
     NVL(:NEW.fk_per,0) THEN
     audit_trail.column_update
       (raid, 'FK_PER',
       :OLD.fk_per, :NEW.fk_per);
  END IF;
  IF NVL(:OLD.patprot_start,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.patprot_start,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'PATPROT_START',
       to_char(:OLD.patprot_start,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.patprot_start,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.patprot_notes,' ') !=
     NVL(:NEW.patprot_notes,' ') THEN
     audit_trail.column_update
       (raid, 'PATPROT_NOTES',
       :OLD.patprot_notes, :NEW.patprot_notes);
  END IF;
  IF NVL(:OLD.patprot_stat,0) !=
     NVL(:NEW.patprot_stat,0) THEN
     audit_trail.column_update
       (raid, 'PATPROT_STAT',
       :OLD.patprot_stat, :NEW.patprot_stat);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;

  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM er_user
     WHERE pk_user = :OLD.last_modified_by ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_modby := NULL ;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM er_user
     WHERE pk_user = :NEW.last_modified_by ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_modby := NULL ;
    END ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
              to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

  END IF;
  IF NVL(:OLD.created_on,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.created_on,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'CREATED_ON',
              to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));

  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;
  IF NVL(:OLD.patprot_discdt,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.patprot_discdt,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'PATPROT_DISCDT',
       to_char(:OLD.patprot_discdt,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.patprot_discdt,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.patprot_reason,' ') !=
     NVL(:NEW.patprot_reason,' ') THEN
     audit_trail.column_update
       (raid, 'PATPROT_REASON',
       :OLD.patprot_reason, :NEW.patprot_reason);
  END IF;
  IF NVL(:OLD.patprot_schday,0) !=
     NVL(:NEW.patprot_schday,0) THEN
     audit_trail.column_update
       (raid, 'PATPROT_SCHDAY',
       :OLD.patprot_schday, :NEW.patprot_schday);
  END IF;
    IF NVL(:OLD.FK_TIMEZONE,0) !=
     NVL(:NEW.FK_TIMEZONE,0) THEN
     audit_trail.column_update
       (raid, 'FK_TIMEZONE',
       :OLD.FK_TIMEZONE, :NEW.FK_TIMEZONE);
  END IF;

IF NVL(:OLD.PATPROT_PATSTDID,' ') !=
     NVL(:NEW.PATPROT_PATSTDID,' ') THEN
     audit_trail.column_update
       (raid, 'PATPROT_PATSTDID',
       :OLD.PATPROT_PATSTDID, :NEW.PATPROT_PATSTDID);
  END IF;
IF NVL(:OLD.FK_USERASSTO,0) !=
     NVL(:NEW.FK_USERASSTO,0) THEN
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM er_user
     WHERE pk_user = :OLD.FK_USERASSTO ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_modby := NULL ;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM er_user
     WHERE pk_user = :NEW.FK_USERASSTO ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_modby := NULL ;
    END ;
     audit_trail.column_update
       (raid, 'FK_USERASSTO',
       old_modby, new_modby);
  END IF;
    IF NVL(:OLD.FK_CODELSTLOC,0) !=
     NVL(:NEW.FK_CODELSTLOC,0) THEN
    BEGIN
	SELECT trim(codelst_desc) INTO old_codetype
	  FROM er_codelst
       WHERE pk_codelst =  :OLD.FK_CODELSTLOC 	;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_codetype := NULL;
    END ;
    BEGIN
     SELECT trim(codelst_desc) INTO new_codetype
	 FROM er_codelst
      WHERE pk_codelst =  :NEW.FK_CODELSTLOC ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_codetype := NULL;
    END ;
     audit_trail.column_update
       (raid, 'FK_CODELSTLOC',
       old_codetype, new_codetype);
  END IF;
  IF NVL(:OLD.PATPROT_PHYSICIAN,0) !=
     NVL(:NEW.PATPROT_PHYSICIAN,0) THEN
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM er_user
     WHERE pk_user = :OLD.PATPROT_PHYSICIAN ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_modby := NULL ;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM er_user
     WHERE pk_user = :NEW.PATPROT_PHYSICIAN ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_modby := NULL ;
    END ;
     audit_trail.column_update
       (raid, 'PATPROT_PHYSICIAN',
       old_modby, new_modby);
  END IF;
 IF NVL(:OLD.PATPROT_RANDOM,' ') !=
     NVL(:NEW.PATPROT_RANDOM,' ') THEN
     audit_trail.column_update
       (raid, 'PATPROT_RANDOM',
       :OLD.PATPROT_RANDOM, :NEW.PATPROT_RANDOM);
  END IF;


   IF NVL(:OLD.FK_CODELST_PTST_EVAL_FLAG,0) !=
     NVL(:NEW.FK_CODELST_PTST_EVAL_FLAG,0) THEN
    BEGIN
	SELECT trim(codelst_desc) INTO old_codetype
	  FROM er_codelst
       WHERE pk_codelst =  :OLD.FK_CODELST_PTST_EVAL_FLAG 	;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_codetype := NULL;
    END ;
    BEGIN
     SELECT trim(codelst_desc) INTO new_codetype
	 FROM er_codelst
      WHERE pk_codelst =  :NEW.FK_CODELST_PTST_EVAL_FLAG ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_codetype := NULL;
    END ;
     audit_trail.column_update
       (raid, 'FK_CODELST_PTST_EVAL_FLAG',
       old_codetype, new_codetype);
  END IF;



   IF NVL(:OLD.FK_CODELST_PTST_EVAL,0) !=
     NVL(:NEW.FK_CODELST_PTST_EVAL,0) THEN
    BEGIN
	SELECT trim(codelst_desc) INTO old_codetype
	  FROM er_codelst
       WHERE pk_codelst =  :OLD.FK_CODELST_PTST_EVAL;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_codetype := NULL;
    END ;
    BEGIN
     SELECT trim(codelst_desc) INTO new_codetype
	 FROM er_codelst
      WHERE pk_codelst =  :NEW.FK_CODELST_PTST_EVAL;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_codetype := NULL;
    END ;
     audit_trail.column_update
       (raid, 'FK_CODELST_PTST_EVAL',
       old_codetype, new_codetype);
  END IF;





   IF NVL(:OLD.FK_CODELST_PTST_INEVAL,0) !=
     NVL(:NEW.FK_CODELST_PTST_INEVAL,0) THEN
    BEGIN
	SELECT trim(codelst_desc) INTO old_codetype
	  FROM er_codelst
       WHERE pk_codelst =  :OLD.FK_CODELST_PTST_INEVAL;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_codetype := NULL;
    END ;
    BEGIN
     SELECT trim(codelst_desc) INTO new_codetype
	 FROM er_codelst
      WHERE pk_codelst =  :NEW.FK_CODELST_PTST_INEVAL;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_codetype := NULL;
    END ;
     audit_trail.column_update
       (raid, 'FK_CODELST_PTST_INEVAL',
       old_codetype, new_codetype);
  END IF;



   IF NVL(:OLD.FK_CODELST_PTST_SURVIVAL,0) !=
     NVL(:NEW.FK_CODELST_PTST_SURVIVAL,0) THEN
    BEGIN
	SELECT trim(codelst_desc) INTO old_codetype
	  FROM er_codelst
       WHERE pk_codelst =  :OLD.FK_CODELST_PTST_SURVIVAL;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_codetype := NULL;
    END ;
    BEGIN
     SELECT trim(codelst_desc) INTO new_codetype
	 FROM er_codelst
      WHERE pk_codelst =  :NEW.FK_CODELST_PTST_SURVIVAL;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_codetype := NULL;
    END ;
     audit_trail.column_update
       (raid, 'FK_CODELST_PTST_SURVIVAL',
       old_codetype, new_codetype);
  END IF;




   IF NVL(:OLD.FK_CODELST_PTST_DTH_STDREL,0) !=
     NVL(:NEW.FK_CODELST_PTST_DTH_STDREL,0) THEN
    BEGIN
	SELECT trim(codelst_desc) INTO old_codetype
	  FROM er_codelst
       WHERE pk_codelst =  :OLD.FK_CODELST_PTST_DTH_STDREL;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_codetype := NULL;
    END ;
    BEGIN
     SELECT trim(codelst_desc) INTO new_codetype
	 FROM er_codelst
      WHERE pk_codelst =  :NEW.FK_CODELST_PTST_DTH_STDREL;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_codetype := NULL;
    END ;
     audit_trail.column_update
       (raid, 'FK_CODELST_PTST_DTH_STDREL',
       old_codetype, new_codetype);
  END IF;

 IF NVL(:OLD.DEATH_STD_RLTD_OTHER,' ') !=
     NVL(:NEW.DEATH_STD_RLTD_OTHER,' ') THEN
     audit_trail.column_update
       (raid, 'DEATH_STD_RLTD_OTHER',
       :OLD.DEATH_STD_RLTD_OTHER, :NEW.DEATH_STD_RLTD_OTHER);
  END IF;


 IF NVL(:OLD.INFORM_CONSENT_VER,' ') !=
     NVL(:NEW.INFORM_CONSENT_VER,' ') THEN
     audit_trail.column_update
       (raid, 'INFORM_CONSENT_VER',
       :OLD.INFORM_CONSENT_VER, :NEW.INFORM_CONSENT_VER);
  END IF;


IF NVL(:OLD.next_followup_on,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.next_followup_on,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'NEXT_FOLLOWUP_ON',
       to_char(:OLD.next_followup_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.next_followup_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;


  IF NVL(:OLD.date_of_death,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.date_of_death,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'DATE_OF_DEATH',
       to_char(:OLD.date_of_death,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.date_of_death,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

--JM: 10Jul2008 #3534
IF NVL(:OLD.PATPROT_CONSIGN,0) !=
     NVL(:NEW.PATPROT_CONSIGN,0) THEN
     audit_trail.column_update
       (raid, 'PATPROT_CONSIGN',
       :OLD.PATPROT_CONSIGN, :NEW.PATPROT_CONSIGN);
  END IF;


IF NVL(:OLD.PATPROT_CONSIGNDT,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.PATPROT_CONSIGNDT,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'PATPROT_CONSIGNDT',
       to_char(:OLD.PATPROT_CONSIGNDT,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.PATPROT_CONSIGNDT,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;


IF NVL(:OLD.PATPROT_RESIGNDT1,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.PATPROT_RESIGNDT1,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'PATPROT_RESIGNDT1',
       to_char(:OLD.PATPROT_RESIGNDT1,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.PATPROT_RESIGNDT1,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

IF NVL(:OLD.PATPROT_RESIGNDT2,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.PATPROT_RESIGNDT2,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'PATPROT_RESIGNDT2',
       to_char(:OLD.PATPROT_RESIGNDT2,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.PATPROT_RESIGNDT2,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

IF NVL(:OLD.FK_SITE_ENROLLING,0) !=
     NVL(:NEW.FK_SITE_ENROLLING,0) THEN
     audit_trail.column_update
       (raid, 'FK_SITE_ENROLLING',
       :OLD.FK_SITE_ENROLLING, :NEW.FK_SITE_ENROLLING);
  END IF;

IF NVL(:OLD.PATPROT_TREATINGORG,0) !=
     NVL(:NEW.PATPROT_TREATINGORG,0) THEN
     audit_trail.column_update
       (raid, 'PATPROT_TREATINGORG',
       :OLD.PATPROT_TREATINGORG, :NEW.PATPROT_TREATINGORG);
  END IF;
  

IF NVL(:OLD.PATPROT_DISEASE_CODE,0) !=
     NVL(:NEW.PATPROT_DISEASE_CODE,0) THEN
     audit_trail.column_update
       (raid, 'PATPROT_DISEASE_CODE',
       :OLD.PATPROT_DISEASE_CODE, :NEW.PATPROT_DISEASE_CODE);
  END IF;
  
IF NVL(:OLD.DMGRPH_REPORTABLE,0) !=
     NVL(:NEW.DMGRPH_REPORTABLE,0) THEN
     audit_trail.column_update
       (raid, 'DMGRPH_REPORTABLE',
       :OLD.DMGRPH_REPORTABLE, :NEW.DMGRPH_REPORTABLE);
  END IF;
  
IF NVL(:OLD.PATPROT_OTHR_DIS_CODE,0) !=
     NVL(:NEW.PATPROT_OTHR_DIS_CODE,0) THEN
     audit_trail.column_update
       (raid, 'PATPROT_OTHR_DIS_CODE',
       :OLD.PATPROT_OTHR_DIS_CODE, :NEW.PATPROT_OTHR_DIS_CODE);
  END IF;
  
IF NVL(:OLD.PATPROT_MORE_DIS_CODE1,0) !=
     NVL(:NEW.PATPROT_MORE_DIS_CODE1,0) THEN
     audit_trail.column_update
       (raid, 'PATPROT_MORE_DIS_CODE1',
       :OLD.PATPROT_MORE_DIS_CODE1, :NEW.PATPROT_MORE_DIS_CODE1);
  END IF;
  
IF NVL(:OLD.PATPROT_MORE_DIS_CODE2,0) !=
     NVL(:NEW.PATPROT_MORE_DIS_CODE2,0) THEN
     audit_trail.column_update
       (raid, 'PATPROT_MORE_DIS_CODE2',
       :OLD.PATPROT_MORE_DIS_CODE2, :NEW.PATPROT_MORE_DIS_CODE2);
  END IF;

--JM: 10Jul2008 #3534------------------

END;
/
