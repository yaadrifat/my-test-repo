CREATE OR REPLACE TRIGGER ER_PATFACILITY_AU0
AFTER UPDATE
ON ER_PATFACILITY REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  old_codetype VARCHAR2(200) ;
  new_codetype VARCHAR2(200) ;
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
  usr VARCHAR2(100);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  usr := Getuser(:NEW.last_modified_by);

  Audit_Trail.record_transaction
    (raid, 'ER_PATFACILITY', :OLD.rid, 'U', usr);

  IF NVL(:OLD.PK_PATFACILITY,0) !=
     NVL(:NEW.PK_PATFACILITY,0) THEN
     Audit_Trail.column_update
       (raid, 'PK_PATFACILITY',
       :OLD.PK_PATFACILITY, :NEW.PK_PATFACILITY);
  END IF;
  IF NVL(:OLD.FK_PER,0) !=
     NVL(:NEW.FK_PER,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_PER',
       :OLD.FK_PER, :NEW.FK_PER);
  END IF;

  IF NVL(:OLD.FK_SITE,0) !=
     NVL(:NEW.FK_SITE,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_SITE',
       :OLD.FK_SITE, :NEW.FK_SITE);
  END IF;

  IF NVL(:OLD.PAT_FACILITYID,' ') !=
     NVL(:NEW.PAT_FACILITYID,' ') THEN
     Audit_Trail.column_update
       (raid, 'PAT_FACILITYID',
       :OLD.PAT_FACILITYID, :NEW.PAT_FACILITYID);
  END IF;

IF NVL(:OLD.PATFACILITY_REGDATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.PATFACILITY_REGDATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update
       (raid, 'PATFACILITY_REGDATE',
       to_char(:OLD.PATFACILITY_REGDATE,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.PATFACILITY_REGDATE,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

old_modby := '';
new_modby := '';

  IF NVL(:OLD.last_modified_by,0) !=    NVL(:NEW.last_modified_by,0) THEN
      BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM ER_USER
     WHERE pk_user = :OLD.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      old_modby := NULL ;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM ER_USER
     WHERE pk_user = :NEW.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      new_modby := NULL ;
    END ;
	     Audit_Trail.column_update     (raid, 'LAST_MODIFIED_BY', old_modby, new_modby);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update     (raid, 'LAST_MODIFIED_DATE',
     to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

  END IF;

  IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=     NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update (raid, 'CREATED_ON',
     to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));

  END IF;


------------------

  IF NVL(:OLD.PATFACILITY_PROVIDER,0) !=     NVL(:NEW.PATFACILITY_PROVIDER,0) THEN
	 BEGIN
	      SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     	  INTO old_modby
		       FROM ER_USER
			        WHERE pk_user = :OLD.PATFACILITY_PROVIDER ;
					     EXCEPTION WHEN NO_DATA_FOUND THEN
						       old_modby := NULL ;
	      END ;

    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM ER_USER
     WHERE pk_user = :NEW.PATFACILITY_PROVIDER ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      new_modby := NULL ;
    END ;

    Audit_Trail.column_update      (raid, 'PATFACILITY_PROVIDER',
       old_modby , new_modby );
  END IF;

    IF NVL(:OLD.PATFACILITY_OTHERPROVIDER,' ') !=
     NVL(:NEW.PATFACILITY_OTHERPROVIDER,' ') THEN
     Audit_Trail.column_update
       (raid, 'PATFACILITY_OTHERPROVIDER',
       :OLD.PATFACILITY_OTHERPROVIDER, :NEW.PATFACILITY_OTHERPROVIDER);
  END IF;

---------------
  IF NVL(:OLD.PATFACILITY_SPLACCESS,' ') !=
     NVL(:NEW.PATFACILITY_SPLACCESS,' ') THEN
     Audit_Trail.column_update
       (raid, 'PATFACILITY_SPLACCESS',
       :OLD.PATFACILITY_SPLACCESS, :NEW.PATFACILITY_SPLACCESS);
  END IF;
  IF NVL(:OLD.PATFACILITY_ACCESSRIGHT,0) !=
     NVL(:NEW.PATFACILITY_ACCESSRIGHT,0) THEN
     Audit_Trail.column_update
       (raid, 'PATFACILITY_ACCESSRIGHT',
       :OLD.PATFACILITY_ACCESSRIGHT, :NEW.PATFACILITY_ACCESSRIGHT);
  END IF;
---------------------------------
  IF NVL(:OLD.PATFACILITY_DEFAULT,0) !=     NVL(:NEW.PATFACILITY_DEFAULT,0) THEN
     Audit_Trail.column_update   (raid, 'PATFACILITY_DEFAULT',    :OLD.PATFACILITY_DEFAULT, :NEW.PATFACILITY_DEFAULT);
  END IF;

  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN Audit_Trail.column_update    (raid, 'RID',  :OLD.rid, :NEW.rid);
  END IF;
  -- Ankit Bug-10961 Date- 22-Aug-2012
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;

END;
/