create or replace
TRIGGER ER_INVOICE_AD0 AFTER DELETE ON ER_INVOICE        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 oldUsr := 'New User' ;


    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;

    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR;
      EXCEPTION WHEN NO_DATA_FOUND THEN
       oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_INVOICE', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON', TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr);
       Audit_Trail.column_delete (raid, 'PK_INVOICE', :OLD.PK_INVOICE);
       Audit_Trail.column_delete (raid, 'INV_NUMBER', :OLD.INV_NUMBER);
       Audit_Trail.column_delete (raid, 'INV_ADDRESSEDTO', :OLD.INV_ADDRESSEDTO);
       Audit_Trail.column_delete (raid, 'INV_SENTFROM', :OLD.INV_SENTFROM);
       Audit_Trail.column_delete (raid, 'INV_HEADER', :OLD.INV_HEADER);
       Audit_Trail.column_delete (raid, 'INV_FOOTER', :OLD.INV_FOOTER);
       Audit_Trail.column_delete (raid, 'INV_MILESTATUSTYPE', :OLD.INV_MILESTATUSTYPE);
       Audit_Trail.column_delete (raid, 'INV_INTERVALTYPE', :OLD.INV_INTERVALTYPE);
       Audit_Trail.column_delete (raid, 'INV_INTERVALFROM', :OLD.INV_INTERVALFROM);
       Audit_Trail.column_delete (raid, 'INV_INTERVALTO', :OLD.INV_INTERVALTO);
       Audit_Trail.column_delete (raid, 'INV_DATE', :OLD.INV_DATE);
       Audit_Trail.column_delete (raid, 'INV_NOTES', :OLD.INV_NOTES);
       Audit_Trail.column_delete (raid, 'INV_OTHER_USER', :OLD.INV_OTHER_USER);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
       Audit_Trail.column_delete (raid, 'INV_PAYUNIT', :OLD.INV_PAYUNIT);
       Audit_Trail.column_delete (raid, 'FK_STUDY', :OLD.FK_STUDY);
       Audit_Trail.column_delete (raid, 'FK_SITE', :OLD.FK_SITE);
       Audit_Trail.column_delete (raid, 'INV_PAYMENT_DUEBY', :OLD.INV_PAYMENT_DUEBY);
       Audit_Trail.column_delete (raid, 'INT_ACC_NUM', :OLD.INT_ACC_NUM);
       Audit_Trail.column_delete (raid, 'INV_STATUS', :OLD.INV_STATUS);
       Audit_Trail.column_delete (raid, 'COVERAGE_TYPE_CRITERIA', :OLD.COVERAGE_TYPE_CRITERIA);
COMMIT;
END;